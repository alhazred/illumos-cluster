#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_toplevel.ksh	1.12	08/05/20 SMI"
#
#	scmsgs_toplevel WS [DIRLIST]
# run the top-level scmsgs check.

Ignore_pod() {
	cat << EOF

=head1
NAME

scmsgs_toplevel - check entire source tree for syslog message errors

=head1
SYNOPSIS

scmsgs_toplevel DIRECTORY [SUBDIRS]

=head1
DESCRIPTION

scmsgs_toplevel performs the top-level scmsgs check for Sun Cluster builds.
the required DIRECTORY argument specifies which directory to process, and
the optional SUBDIR arguments limit the processing to the specified
subdirectories of DIRECTORY.

All .c and .cc files, and all shell scripts, under the current directory,
are scanned for syslog messages; any messages found are checked against
the file sc_msgid_expl and the set of explanations found in SCMSGS comments.
See https://n1wiki.sfbay.sun.com/bin/view/BE/NewSCMSGSHowTo .

As a transitional aid, scmsgs_toplevel reads the file
$(SRC)/messageid/sc_msgid_grandfathered.txt
as a list of "grandfathered" syslog messages for
which an explanation will NOT be required.
This feature was added to ensure that the check will fail
only for newly added messages.

Three types of errors are reported:
(1) no explanation provided; (2) msgid collision; and
(3) duplicate explanations provided within SCMSGS comments.
scmsgs_check exits with a status of 1 if any errors are found.

In addition, non-fatal warnings are given for various consistency
checks on the explanations file $(SRC)/messageid/sc_msgid_expl .

Several useful temp files and output files are generated, all with
the prefix "scmsgs-". (See FILES below.)

=head1
EXAMPLE

scmsgs_toplevel . cmd common lib uts

This is how scmsgs_toplevel is called from the build.
The search is limited to the subdirs cmd, common, lib, and uts.

=head1
FILES

=over 4

=item (*)

$(SRC)/messageid/sc_msgid_expl - legacy explanations file

=item (*)

$(SRC)/messageid/sc_msgid_grandfathered.txt - list of syslog messages for
which no explanation will be required

=item (*)

scmsgs-getfiles.tmp - temp file listing names of files scanned

=item (*)

scmsgs-getmsgs.tmp - temp file listing the syslog messages found

=item (*)

scmsgs-merge.tmp - temp file of syslog messages found, with duplicates merged

=item (*)

scmsgs-errs - output file of errors and warnings

=item (*)

scmsgs-summary - counts of errors and warnings

=item (*)

scmsgs-expl - generated explanations file (ala sc_msgid_expl)

=item (*)

scmsgs-unexpl-compact - compact listing of unexplained messages

=item (*)

scmsgs-unref - explanations not matched by any found syslog message

=back

=head1
ENVIRONMENT VARIABLES

=cut

EOF
}

PROGDIR=$(cd `dirname $0`;/bin/pwd)
HERE=`/bin/pwd`

PREFIX=${PREFIX:-scmsgs}
OFILE=$HERE/$PREFIX-expl
# URFILE=$HERE/$PREFIX-unref
# UFLAG="-u $URFILE"
ERRFILE=$PREFIX-errs
# GFILE=${GFILE:-$HERE/messageid/sc_msgid_grandfathered.txt}
UXFILE=$PREFIX-unexpl-compact
SUMMARY=$PREFIX-summary
# PFILE=$HERE/$PREFIX-placeholders
# PFLAG="-P $PFILE"

WS="$1"; shift
DIRLIST="$*"

GFLAG=""
UFLAG=""
PFLAG=""
OFLAG="-o $OFILE"

# if the file of "grandfathered" messages exists, use it.
# if [ -r "$GFILE" ]; then
# 	GFLAG="-g $GFILE"
# fi

(cd $WS;
PREFIX=$PREFIX \
$PROGDIR/scmsgs_check -V -f "" \
	$OFLAG $UFLAG $GFLAG $PFLAG \
	$DIRLIST) > $ERRFILE 2> $SUMMARY
xstat=$?

# produce compact versions of the major results from above
if [ -n "$SCMSGS_DEBUG" ]; then
sed -n -e 's/^error: UNEXPLAINED MSG: \([0-9]*\) \[\(.*\)\].*/\1	:\2/p' \
	< $ERRFILE \
	| sort -u -t: +1 > $UXFILE

grep '^[0-9]' < $URFILE | sort -u -t: +1 > $URFILE-compact
grep '^[0-9]' < $OFILE | sort -u -t: +1 > $OFILE-compact
grep '^[0-9]' < $PFILE | sort -u -t: +1 > $PFILE-compact
fi

cat $SUMMARY

if [ $xstat -ne 0 ]; then
	echo "### (see \"$ERRFILE\" for a detailed listing of scmsgs errors)"
fi

exit $xstat
