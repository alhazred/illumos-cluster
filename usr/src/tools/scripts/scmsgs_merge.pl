#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_merge.pl	1.4	08/05/20 SMI"
#
#	scmsgs_merge [-v] < MSGLIST
#		-v        verbose
# merge duplicate messages on input; print merged messages to stdout.

BEGIN {
	($PROGDIR = ".") unless (($PROGDIR) = ($0 =~ m{^(.*)/}));
	unshift @INC, $PROGDIR;
}

use SCMSGS;

sub dump_entry
{
	my $x = shift;

	print "\n";

	&SCMSGS::print_entry(\*STDOUT, $x);

	print "Location:";

	my @locs = split /\s+/, $x->{location};
	print ("\n\t", join("\n\t", @locs), "\n");
	if ($x->{commentloc} ne '') {
		print "Commentloc:";
		my @clocs = split /\s+/, $x->{commentloc};
		print ("\n\t", join("\n\t", @clocs), "\n");
	}
}

sub main
{
	my $nmerges = 0;
	my $NMSGS = 0;
	$S = new SCMSGS("/dev/fd/0");

	@MSGS = ();
	%MSGS = ();

	foreach $x (@{$S->explist()}) {
		my $msgtext = $x->{msgtext};
		my $p = $MSGS{$msgtext};
		$NMSGS++;
		if ($p) {
# print STDERR "# MERGE msgtext=$msgtext\n";
			&SCMSGS::merge_msgs($p, $x);
			$nmerges++;
			next;
		}
# print STDERR "# ADD msgtext=$msgtext\n";
		push @MSGS, $x;
		$MSGS{$msgtext} = $x;
	}

	my $NDISTINCT = 0;
	foreach $x (@MSGS) {
		$NDISTINCT++;
		&dump_entry($x);
	}

print STDERR
"### scmsgs_merge: $NMSGS messages read, $NDISTINCT distinct\n"
	if $opt_v;
}

$opt_v = $v;

&main;
