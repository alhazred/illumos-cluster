#!/bin/ksh

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)depotctl.ksh	1.4	09/05/14 SMI"
#
# Control an IPS repository on your local machine. This command manages the
# directory for the repository backing store, as well as the pkg.depotd(1M)
# daemon. There are five sub-commands:
#   start:
#	Creates the backing store directory, if it doesn't already exist
#	Starts pkg.depotd, pointing to the backing store diretory
#   restart:
#	Stops pkg.depotd daemons owned by the user and running on the
#		specified (or default) port
#	Starts pkg.depotd, pointing to the backing store diretory
#   stop:
#	Stops pkg.depotd daemons owned by the user and running on the
#		specified (or default) port
#   destroy:
#	Stops pkg.depotd daemons owned by the user and running on the
#		specified (or default) port
#	Deletes the backing store directory
#   cleanse:
#	destroy followed by start.
#
# The user can specify the port on which the pkg.depotd daemon listens with -p.
# If the port is not specified, the default is the user's UID (hashed to be
# between 1024 and 65535).
#
# The user can specify the backing store directory with -d.
# If the directory is not specified, the default is /var/repos/$PORT.
#

#
# The following function is for auto-generating the man page
#
function Ignore_pod {
	cat << EOF

=head1
NAME

depotctl - control an IPS repository on your local machine

=head1
SYNOPSIS

depotctl { start | stop | restart | destroy | cleanse } [-p port] [-d directory]

=head1
DESCRIPTION

I<depotctl> manages an IPS repository on your local system, including the directory for the repository backing store and the pkg.depotd(1M) daemon.

=head1
OPTIONS

  start
	Creates the backing store directory, if it doesn't already exist
	Starts pkg.depotd, pointing to the backing store diretory

  restart
	Stops pkg.depotd daemons owned by the user and running on the
		specified (or default) port
	Starts pkg.depotd, pointing to the backing store diretory

  stop
	Stops pkg.depotd daemons owned by the user and running on the
		specified (or default) port

  destroy
	Stops pkg.depotd daemons owned by the user and running on the
		specified (or default) port
	Deletes the backing store directory

  cleanse
	destroy followed by start.

  -p port
	The port on which the pkg.depotd daemon listens.
	If the port is not specified, the default is the user's UID (hashed
	to be between 1024 and 65535).


  -d directory
	The directory for the backing store for pkg.depotd.
	If the directory is not specified, the default is
	/var/repos/<port>.

=head1
EXAMPLES

  depotctl start

the above example starts a pkg.depotd process listening on the default port
and using the default directory for the backing store.

  depotctl destroy -p 5000 -d /var/mypath/123

the above example stops a pkg.depotd process listening on port 5000 and
deletes the /var/mypath/123 directory.

=head1
SEE ALSO

pkg.depotd(1M)

=head1
EXIT STATUS

I<depotctl> exits with a status of 1 on failure; otherwise 0.

=cut

EOF
}

ID=/usr/bin/id
KILL=/usr/bin/kill
RM=/usr/bin/rm
PGREP=/usr/bin/pgrep
PFILES=/usr/bin/pfiles
PKG_DEPOT=/usr/lib/pkg.depotd
REPO_BASEDIR=/var/repos
USAGE="Usage: $0 { start | stop | restart | destroy | cleanse } [-p port] [-d directory]\n\n\t-p \tspecify the port on which the pkg.depotd should listen\n\t-d \tspecify the directory for the pkg.depotd backing store"

#
# There are four different steps that we take in different combination
# depending on the user action requested
#
START=0
STOP=0
CREATE=0
DESTROY=0

#
# argument one always must be the subcommand
#
case "$1" in
start)
	START=1
	CREATE=1
	;;
restart)
	STOP=1
	START=1
	;;
stop)
	STOP=1
	;;
destroy)
	STOP=1
	DESTROY=1
	;;
cleanse)
	STOP=1
	DESTROY=1
	CREATE=1
	START=1
	;;
*)
	echo "$USAGE"
	exit 1
	;;
esac

shift

#
# Now parse the optional arguments
#
while getopts "p:d:" opt; do
	case $opt in
	p)
		REPO_PORT=$OPTARG
		;;
	d)
		REPO_DIR=$OPTARG
		;;
	*)
		echo "$USAGE"
		exit 1
		;;
	esac
done

#
# Get the UID
#
MYUID=$($ID -u)
if [ $? != 0 ]; then
	printf "Error running id -u\n"
	exit 1
fi

#
# If the user specifies a port, use that. Don't sanity
# check that it's non-privileged; assume the user
# is running the script as root if not.
#
# If no port is specified, use the uid, but ensure that it's set to a
# non-privileged port between 1024 and 65535.
#
if [ -z "$REPO_PORT" ]; then
	REPO_PORT=$MYUID
	REPO_PORT=$(expr $REPO_PORT % 65536)
	if [ "$REPO_PORT" -lt 1024 ]; then
		REPO_PORT=$(expr $REPO_PORT + 1024)
	fi
fi

#
# If the user specifies a REPO_DIR, use that.
# Otherwise, default to our BASEDIR + PORT.
# 
if [ -z "$REPO_DIR" ]; then
	REPO_DIR=$REPO_BASEDIR/$REPO_PORT 
fi

echo "Using repository dir $REPO_DIR and port of $REPO_PORT"

if [ $STOP -eq 1 ]; then
	echo "Stopping all depot servers owned by user $MYUID running on port $REPO_PORT"
	#
	# Stop all pkg.depotd processes owned by the user running this script
	# that are running on the REPO_PORT
	#
	PROC_LIST=$($PGREP -U $MYUID pkg.depotd)
	for PROC in $PROC_LIST; do
		#
		# The pfiles output contains a line like this:
		#         sockname: AF_INET 0.0.0.0  port: 59388
		# The port number is the fifth token
		#
		TEMP_PORT=$($PFILES $PROC | awk '/port:/ { print $5 }')
		if [ "${TEMP_PORT}" -eq "${REPO_PORT}" ]; then
			echo "killing $PROC"
			$KILL -9 $PROC
		fi
	done
fi

if [ $DESTROY -eq 1 ]; then
	#
	# If a repository using that directory exists already, confirm that
	# the user really wants to destroy it. This is necessary because two
	# different uids could be congruent (mod 65536), leading to collisions
	# in the directory path.
	#
	if [ -d "$REPO_DIR" ]; then
		echo "$REPO_DIR is already in use. Really remove it and all its contents? (y/n)"
		read response
		if [ $response = "y" -o $response = "Y" -o $response = "yes" -o $response = "Yes" ]; then
			$RM -rf "$REPO_DIR"
			if [ $? != 0 ]; then
				printf "Error destroying directory %s\n" $REPO_DIR
				exit 1
			fi
		else
			exit 1
		fi
	fi
fi

if [ $CREATE -eq 1 ]; then
	#
	# If a repository using that directory exists already, confirm that
	# the user really wants to create it. This is necessary because two
	# different uids could be congruent (mod 65536), leading to collisions
	# in the directory path.
	#
	if [ -d "$REPO_DIR" ]; then
		echo "$REPO_DIR is already in use. Do you want to continue? (y/n)"
		read response
		if [ $response != "y" -a $response != "Y" -a $response != "yes" -a $response != "Yes" ]; then
			exit 1
		fi
	fi

	echo "Creating repository directory $REPO_DIR"

	#
	# Attempt to create the repository directory.
	# Ignore the failure if the directory already exists because
	# we already checked with the user for that case (directly above).
	# But if the failure is due to permissions, we should error out.
	#
	mkdir -p "$REPO_DIR"
	if [ $? != 0 ]; then
		printf "Error creating repository directory %s\n" $REPO_DIR
		exit 1
	fi
fi

if [ $START -eq 1 ]; then
	echo "Starting depot server on port $REPO_PORT using dir $REPO_DIR"

	#
	# Check if there are any depot processes on that port
	#
	PROC_LIST=$($PGREP -U $MYUID pkg.depotd)
	for PROC in $PROC_LIST; do
		#
		# The pfiles output contains a line like this:
		#         sockname: AF_INET 0.0.0.0  port: 59388
		# The port number is the fifth token
		#
		TEMP_PORT=$($PFILES $PROC | awk '/port:/ { print $5 }')
		if [ "${TEMP_PORT}" -eq "${REPO_PORT}" ]; then
			echo "Error, pkg.depotd process $PROC is already running on port $REPO_PORT"
			exit 1
		fi
	done

	#
	# Start it with nohup so it keeps running if user logs out.
	# Run it in the background because it doesn't daemonize.
	#
	(cd $HOME && nohup $PKG_DEPOT -p $REPO_PORT -d "$REPO_DIR" --rebuild &)

	#
	# Verify that it started correctly
	# (give it a delay to start properly)
	#
	sleep 3
	PROC_LIST=$($PGREP -U $MYUID pkg.depotd)
	if [ $? -eq 0 ]; then
		# We found a proc
		echo "Started correctly"
		exit 0
	fi

	#
	# If we get here, we didn't find any procs, so it failed
	#
	printf "Error starting depot\n" $REPO_DIR;
	exit 1
fi

exit 0
