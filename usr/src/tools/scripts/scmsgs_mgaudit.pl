#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_mgaudit.pl	1.5	08/05/20 SMI"
#
#	scmsgs_mgaudit [-cv] EXPL1 EXPL2 EXPL3 ...
#		-c	check for clashes also
#		-v	verbose
# audits the (generated) EXPL files from multiple gates,
# and warns of collisions.
#
# TODO: add locations to error messages

# BEGIN { unshift @INCL, (($0 =~ m{(.*)/}) ? $1 : "."); }
# use SCMSGS;

sub dump_entry
{
	my ($x, $name) = @_;
	my $nkeys = 0+(keys %$x);
	my $nloc = 0+@{$x->{location}};
	my $msg = $x->{msgtext};
	print STDERR "! $name: LINENO=$LINENO ENTNO=$ENTNO x=$x nkeys=$nkeys nloc=$nloc [$msg]\n";
}


sub read_expl_files
{
	%MSG_LIST_BY_MSGID = ();
	$i = 1;
	select STDERR; $| = 1;
	foreach $fn (@ARGV) {
		if (-d $fn) {
			# TODO: run the scmsgs pipeline to get the messages
			print STDERR "# generating expl file {$i} \"$fn\"...\n";
			&dodir($fn, $i);
			print STDERR "# done\n";
		}
		else {
			print STDERR "# reading {$i} \"$fn\"...";
			&dofile($fn, $i);
			print STDERR "# done\n";
		}
		$i++;
	}
}

sub dofile
{
	my ($fn, $fid) = @_;
	my $list = &read_msg_list($fn);
##	my $p = new SCMSGS($fn);
##	my $list = $p->explist();
	my $k = 0+@{$list};
	push @LISTS, $list;
	print STDERR "$k messages\n";
	foreach $x (@{$list}) {
		$x->{fileno} = $fid;
# print STDERR "id=$x->{msgid}\n";
		push @{$MSG_LIST_BY_MSGID{$x->{msgid}}}, $x;
		my $tmp = $x->{msgtext};
		&_op_sig($tmp);
		push @{$MSG_LIST_BY_SIG{$tmp}}, $x;
	}
}

sub chomper
{
	my $s = shift;
	chomp $s;
	return $s;
}

sub find_srcdir
{
	my ($fn) = @_;
	my $unknown = "***UNKNOWN***";
	my ($wk) = &chomper(`curws $fn`);
	if ($wk =~ m{^$|^\Q$unknown\E$}) {
		return "";
	}

	my @trialdirs = qw(usr/src src);
	foreach $td (@trialdirs) {
		my $path = "$wk/$td";
		if (-d $path) {
			return $path;
		}
	}
	return "";
}

sub maketemp
{
	my $fid = shift;
	my $pid = $$;
	my $tmp = "$TMPDIR/scmcg.$pid.$fid.txt";
	push @TMPFILES, $tmp;
	return $tmp;
}

sub generate
{
	my ($src, $tmp) = @_;
	my @subdirlist = qw(cmd uts lib common);
	my @extlist = grep (-d "$src/$_", @subdirlist);
	my $cmd = "(cd $src; $CHECKER -V -P /dev/null -f \"\" -o $tmp @extlist >/dev/null)";
	print STDERR "# + $cmd\n";
	system($cmd);
	my $xstat = $?>>8;
	print STDERR "# (cmd returned $xstat)\n";
	if ($xstat != 0) { $PROGERRS++; }
	return $xstat;
}

sub dodir
{
	my ($fn, $fid) = @_;
	my $src = &find_srcdir($fn);
	if ($src eq '') {
print STDERR "error: argument \"$fn\" is neither an EXPL file nor a workspace; skipping\n";
		return;
	}
	# print STDERR "# generating EXPL file for $fn...\n";
	my $tmp = &maketemp($fid);
	&generate($src, $tmp);
	print STDERR "# reading {$i} \"$fn\"...";
	&dofile($tmp, $fid);
}

sub print_hits
{
	# show all the different texts along with
	# a list of which files had it.
	my %hithash = ();
	my %msgid_by_text = ();
	my %x_by_text = ();
	foreach $x (@_) {
		push @{$hithash{$x->{msgtext}}},
			$x->{fileno};
		$msgid_by_text{$x->{msgtext}} = $x->{msgid};
		push @{$x_by_text{$x->{msgtext}}}, $x;
		# &dump_entry($x, "print_hits");
	}

	my @outlist = ();
	foreach $text (sort keys %hithash) {
		my @mfids = sort @{$hithash{$text}};
		my $s = "    {@mfids} $msgid_by_text{$text} [$text]\n";
		if ($opt_v) {
			# optionally print source code locations
			foreach $x (@{$x_by_text{$text}}) {
				foreach $loc (@{$x->{location}}) {
					$s .= "    at: {$x->{fileno}} $loc\n";
				}
			}
		}
		push @outlist, $s;
	}
	print STDERR sort @outlist;
}

sub check_collisions
{
	$NCOLLISIONS = 0;
	foreach $msgid (grep @{$MSG_LIST_BY_MSGID{$_}} > 1,
			sort keys %MSG_LIST_BY_MSGID) {
		$NCOLLISIONS++;
		my @hitlist = @{$MSG_LIST_BY_MSGID{$msgid}};
print STDERR "error: MSGID COLLISION on msgid=$msgid\n";
		&print_hits(@hitlist);
	}

}

# operator to generate "signature"
sub _op_sig
{
	$_[0] =~ s/\\n//g;
	$_[0] =~ s/%[ds]/XXX/g;
	$_[0] =~ s/\W+//g;
	$_[0] = lc substr($_[0], 0, 60);
}

sub check_clashes
{
	$NCLASHES = 0;
	foreach $sig (grep @{$MSG_LIST_BY_SIG{$_}} > 1,
			sort keys %MSG_LIST_BY_SIG) {
		my @hitlist = @{$MSG_LIST_BY_SIG{$sig}};
		# let's skip any where all the variants are exactly identical.
		# since they would have already been reported as collisions.
		my %texts;
		foreach $x (@hitlist) { $texts{$x->{msgtext}} = 1; }
		next if (0+(keys %texts) <= 1);

		$NCLASHES++;
		my $msgidstr = join(",", sort map $_->{msgid}, @hitlist);
print STDERR "warning: MSGTEXT CLASH: msgids=$msgidstr\n";
		&print_hits(@hitlist);
	}

}

sub cleanup { unlink @TMPFILES   if @TMPFILES > 0; }

sub CATCH { &cleanup; exit 1; }


# ----- code borrowed from scmsgs_style
sub check_entry
{
	my $x = { %ENTRY };
	push @ENTRIES, $x;
	# &dump_entry(\%ENTRY, "check_ENTRY");
}

sub clear_entry()
{
	%ENTRY = ();
	$ENTRY_LINENO = 0;
	$STATE = 'incomment';
}

# blank lines can occur within an entry,
# but not within a single tag (eg, Explanation or User Action).
sub handle_blank
{
	return if ($STATE eq 'incomment');
	&handle_tagval($_[0]);
}

sub got_entry
{
	return if (keys %ENTRY <= 0);

	# &dump_entry(\%ENTRY, "got_entry");
	$ENTNO++;
	foreach $k (keys %ENTRY) {
		$ENTRY{$k} =~ s/\s*$// unless ref $ENTRY{$k};
	}

	&check_entry();
	&clear_entry();
}

sub handle_other
{
	print "E: unrecognized garbage at line $LINENO\n";
}

# comments, if they exist, pertain to the "next" entry, whose
# header is yet to be read.  thus, a comment marks the end
# of the current entry (if we have seen a header).
sub handle_comment
{
	if ($STATE eq 'gotheader') {
		# comment marks the start of a new entry,
		# so process the current entry.
		&got_entry();
	}

	if ($ENTRY_LINENO == 0) {
		# this is the first comment after an entry ended.
		$ENTRY_LINENO = $LINENO;
	}

	$STATE = 'incomment';

	if ($_[0] =~ m/^#+\s*code:(.*)/) {
		$ENTRY{CODE} = $1;
	}
	elsif ($_[0] =~ m/^#+\s*at:(.*)/) {
		my $loc = $1;
		$loc =~ s/^\s*//;
		$loc =~ s/\s*$//;
		push @{$ENTRY{location}}, $loc;
	}
}

sub handle_header
{
	if ($ENTRY_LINENO == 0) {
		$ENTRY_LINENO = $LINENO;
	}

	$STATE = 'gotheader';

	if ($_[0] !~ m{^(\d+)\s*:?\s*(.*)}) {
		print "E: malformed header at line $LINENO\n";
		return;
	}

	$ENTRY{msgid} = $1;
	$ENTRY{msgtext} = $2;
	$CURTAG = '';
}

sub handle_tag { }

sub handle_tagval { }

sub read_msg_list
{
	my $fn = shift;
	local @ENTRIES = ();

	open(FN, $fn) or warn("can't open \"$fn\"\n");

	&clear_entry();
	$LINENO = 0;
	$ENTNO = 0;

	while (<FN>) {
		$LINENO++;
		chomp;
		if (m/^\s*$/) { &handle_blank($_); }
		elsif (m/^\s*#/) { &handle_comment($_); }
		elsif (m/^\d/) { &handle_header($_); }
		elsif (m/^\w/) { &handle_tag($_); }
		elsif (m/^\s/) { &handle_tagval($_); }
		else { &handle_other($_); }
	}

	if (keys %ENTRY > 0) {
		&got_entry();
	}

	close(FN);

	return \@ENTRIES;
}

# ----- end of borrowed code

sub setsigs
{
	$SIG{INT} = \&CATCH;
	$SIG{TERM} = \&CATCH;
}

sub main
{
	&setsigs;
	&read_expl_files;
	&check_collisions;
	&check_clashes if $opt_c;
	&cleanup;
}


# ---------- start of mainline code ----------

$bebin = "/ws/sc31u-tools/onbld/bin";
$ENV{PATH} = $bebin.":".$ENV{PATH} if ($ENV{PATH} !~ m{(^|:)\Q$bebin\E(:|$)});
$CHECKER = "scmsgs_check";

$TMPDIR="/tmp";
@TMPFILES = ();

$opt_c = $c;
$opt_v = $v;

&main;

print "# $NCOLLISIONS collisions were found\n" if $NCOLLISIONS > 0;
print "# $NCLASHES clashes were found\n" if $NCLASHES > 0;

exit ($NCOLLISIONS > 0);
