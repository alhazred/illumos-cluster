#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_check.ksh	1.12	08/05/20 SMI"
#
#	scmsgs_check [-options] [DIRLIST]
#		-d           debugging
#		-g GFILE     grandfathered explanations
#		-o OFILE     write explanations to OFILE
#		-t TMPDIR    all temp files go here, "." by default.
#		-u UFILE     write unreferenced messages here.
#		-P PFILE     placeholder entries written here.
#
# run a pipeline of scmsgs scripts to generate a list of unexplained
# syslog messages.  this script should be called within a workspace.
#
# the optional DIRLIST is a list of subdirs whose files should be
# (recursively) checked.  if present, this has the effect of limiting
# the search to the given subdirs.  if absent, "." is (recursively) checked.
#

Ignore_pod() {
	cat << EOF

=head1
NAME

scmsgs_check - check for syslog message errors within a source subtree

=head1
SYNOPSIS

scmsgs_check [B<-V>]

=head1
DESCRIPTION

scmsgs_check checks source files for syslog message errors.
All .c, .cc, and .h files, and all shell scripts, under the current directory,
are scanned for syslog messages; any messages found are checked against
the file sc_msgid_expl (no longer used as of 2006.05)
and the set of explanations found in SCMSGS comments.
See https://n1wiki.sfbay.sun.com/bin/view/BE/NewSCMSGSHowTo .

Three types of errors are reported:
(1) no explanation provided; (2) msgid collision; and
(3) duplicate explanations provided within SCMSGS comments.
scmsgs_check exits with a status of 1 if any errors are found.

The optional B<-V> flag causes verbose reporting of warnings,
mainly for use by the scmsgs_toplevel script.

N.B. that scmsgs_check can only check the source files
under the current directory.
In particular, any warnings about "unexplained messages" are based only on
the set of SCMSGS comments in these files.
If a message is shared with a source file that is not under
the current directory, it may be that the other instance has an
explanation in an SCMSGS comment that will not be found.
Therefore, for best results, run scmsgs checks at the top
level thru "dmake scmsgs".

=head1
FILES

=over 4

=item (*)

$(SRC)/messageid/sc_msgid_expl - legacy explanations file

=item (*)

ez-getfiles.tmp - list of files scanned

=item (*)

ez-getmsgs.tmp - list of syslog messages found

=item (*)

ez-merge.tmp - list of syslog messages found, with duplicates merged

=back

=head1
ENVIRONMENT VARIABLES

PREFIX - filename prefix for generated files; "ez" by default.

=cut

EOF
}

usage() {
	echo 1>&2 illegal flag
	echo 1>&2 \
	'usage: scmsgs_check [-d] [-o OFILE] [-u UFILE] [-P PFILE] [DIRLIST]'
	exit 1
}

# ---------- start of mainline code
PROGDIR=$(cd $(dirname $0);/bin/pwd)
GETFILES=$PROGDIR/scmsgs_getfiles
GETMSGS=$PROGDIR/scmsgs_getmsgs
MERGEMSGS=$PROGDIR/scmsgs_merge
AUDITMSGS=$PROGDIR/scmsgs_audit
pref=${PREFIX:-ez}

AVFLAG=
GFLAG=
VFLAG=-v
OFLAG=
UFLAG=
DFLAG=
PFLAG=
FFLAG=
TFLAG=
T="."
TGETFILES=/dev/null
TGETMSGS=/dev/null
TMERGEMSGS=/dev/null

while getopts ":df:g:o:t:u:P:V" flag; do
	case "$flag" in
	d) DFLAG=-d ;;
	f) FFLAG=-f=$OPTARG ;;
	g) GFLAG=-g=$OPTARG ;;
	o) OFLAG=-o=$OPTARG ;;
	t) T=$OPTARG ;;
	u) UFILE=$OPTARG; UFLAG=-u=$UFILE ;;
	P) PFILE=$OPTARG; PFLAG=-P=$PFILE ;;
	V) AVFLAG=-V ;;
	\?) usage ;;
	esac
done
shift `expr $OPTIND - 1`

if [ -n "$SCMSGS_DEBUG" ]; then
	TGETFILES=$T/$pref-getfiles.tmp
	TGETMSGS=$T/$pref-getmsgs.tmp
	TMERGEMSGS=$T/$pref-mergemsgs.tmp
fi

DIRLIST="${@:-.}"

SRC=${SRC:-`curws`/usr/src}
WS=${SRC%/usr/src*}
wsname=$(basename $WS)
echo 1>&2 "### wsname=$wsname"

# if workspace is not virtual, use the -S flag with getfiles (only SCCS)
# if workspace is virtual, use the -L flag with getfiles (only symlinks)
case $wsname in
*+*) SFLAG=-L ;;
*) SFLAG=-S ;;
esac

### # use previously-generated scmsgs-expl file if any.
### # unf this may cause results that are surprising to the users
### # if they've removed any explanations.
### [ -z $FFLAG ] && EXPL_FILE=$SRC/scmsgs-expl
### [ -s $EXPL_FILE ] && FFLAG=-f=$EXPL_FILE

### HERE=`pwd`

$GETFILES $VFLAG $SFLAG $DIRLIST \
	| sed -e 's,^\./,,' | tee $TGETFILES \
	| $GETMSGS $VFLAG -x -L -c | tee $TGETMSGS \
	| $MERGEMSGS $VFLAG | tee $TMERGEMSGS \
	| $AUDITMSGS \
		$DFLAG $GFLAG $AVFLAG $OFLAG $UFLAG $VFLAG $PFLAG $FFLAG
