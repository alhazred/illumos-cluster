#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)build_cds.ksh	1.8	08/05/20 SMI"
#
# Usage: build_cds [-d] [-i image_dir] ws1 [ws2]...
# Usage: build_dvds [-d] [-i image_dir] ws1 [ws2]...
# This script builds CD/DVD images from the specified workspaces,
# using the cdimage/dvdimage scripts contained within the workspaces.
# Supplying the -d option will cause the images to be built from
# the debug build, instead of the default non-debug build.
# The resultant CD/DVD images will be stored in <ws1> if the -i option
# isn't given.
#
# The intention of this script is to know as little as possible about the
# layout and contents of the workspaces supplied to it. This script provides
# some global variables and functions which the cdimage/dvdimage scripts
# should make use of. This script processes the specified workspaces by
# loading their cdimage/dvdimage scripts into it's environment and executing
# their appropriate build_image_<ARCH>_<OS> function.
#
# The cdimage/dvdimage scripts must be located in <ws>/usr/src/pkgdefs and
# their names must have the format <cd image name>.cdimage.ksh or
# <dvd image name>.dvdimage.ksh
# For example the cdimage script to build the Sun Cluster Agents image
# might be named sc_agents.cdimage.ksh and the resultant CD images will
# be cdrom.sc_agents_sparc and cdrom.sc_agents_x86. Note that an equivalent
# dvdimage script called sc_agents.dvdimage.ksh will result in one DVD image,
# dvd.sc_agents, containing both sparc and x86.
# The cdimage/dvdimage scripts contain the actual code to build the CD/DVD
# images in functions named build_image_<ARCH>_<OS>, so for sparc Solaris 9
# the function would be named build_image_sparc_9. These ARCH/OS specific
# functions typically call other functions to handle the areas of the CD/DVD
# that are common to multiple OSes, like the installer and documentation
# navigation files. These individual functions should be made robust enough
# to handle the situation where they are called more than once, which is
# highly likely if each OS function calls a common function.
#

Ignore_pod() {
	cat << EOF

=head1
NAME

build_cds, build_dvds - build CD/DVD images from the specified pre-built
workspaces

=head1
SYNOPSIS

build_cds [B<-d>] [B<-i image_dir>] B<ws1> [B<ws2>]...

build_dvds [B<-d>] [B<-i image_dir>] B<ws1> [B<ws2>]...

=head1
DESCRIPTION

I<build_cds>/I<build_dvds> builds CD/DVD images from the specified pre-built
workspaces. The resultant image is constructed using the cdimage/dvdimage
scripts contained within the workspaces, so I<build_cds> may produce a DVD
image and vice-versa.

See I<nbuild>(1) for details on how to create the CD/DVD images during
the build process.

=head1
OPTIONS

  -d
	Build images from the debug build (default is non-debug build)

  -i image_dir
	Put the resultant CD/DVD images in image_dir (default is ws1)

=head1
EXAMPLES

  build_cds /workspace/clustbld/ws+5.10+sparc

the above example builds a non-debug CD/DVD image in
/workspace/clustbld/ws+5.10+sparc.

  build_dvds -d -i /tmp /workspace/clustbld/ws+5.10+i386

the above example builds a CD/DVD image in /tmp containing debug binaries from
/workspace/clustbld/ws+5.10+i386.

  build_dvds ohac+5.11+i386 ohacds+5.11+i386 ohacgeo+5.11+i386

the above example builds a CD/DVD image containing non-debug binaries from the
three virtual workspaces located in the current directory. The resultant
CD/DVD image will be located in ohac+5.11+i386.

=head1
SEE ALSO

nbuild(1)

=head1
EXIT STATUS

I<build_cds>/I<build_dvds> exits with a status of 1 on failure; otherwise 0.

=cut

EOF
}

#
# Variables
#
IMAGE_SCRIPT_DIR=usr/src/pkgdefs
CDIMAGE_SCRIPT_SUFFIX=.cdimage.ksh
DVDIMAGE_SCRIPT_SUFFIX=.dvdimage.ksh
DEBUG_BUILD=0
DOT_BUILD_LAST_OS=usr/src/.build.last_OS
DOT_BUILD_OS_VERSION=usr/src/.build.OS_version
set -A WORKSPACES
set -A WORKSPACES_ARCH
set -A WORKSPACES_OS

DIRMODE=0755
FILEMODE=0644
XFILEMODE=0755

# Override user's environment
CMDDIR=/usr/bin

BASENAME=${CMDDIR}/basename
CHMOD=${CMDDIR}/chmod
CP="${CMDDIR}/cp -f"
CPIO=${CMDDIR}/cpio
EGREP=${CMDDIR}/egrep
FIND=${CMDDIR}/find
LN=${CMDDIR}/ln
LS=${CMDDIR}/ls
MKDIR="${CMDDIR}/mkdir -p -m ${DIRMODE}"
PKGTRANS=${CMDDIR}/pkgtrans
RM="${CMDDIR}/rm -f"
SED=${CMDDIR}/sed

#
# Functions
#
# Builds the image by loading the specified cdimage/dvdimage script and
# executing the appropriate build_image_arch_os function
# build_image (cdrom/dvd, workspace, script, arch, os, debug)
build_image() {
	CDROM=${1}
	DVD=${1}
	WS=${2}
	SCRIPT=${3}
	ARCH=${4}
	OS=${5}
	DEBUG=${6}

	# Load the cdimage/dvdimage script
	. ${SCRIPT}

	# Build the ARCH/OS image
	build_image_${ARCH}_${OS} || error "Can't find build_image_${ARCH}_${OS} in ${SCRIPT}"
}

# Copies the file and sets the destination file's permissions
# -x is optionally given to make the destination file executable
# copy ([-x], from, to)
copy()
{
	[ "${1}" = "-x" ] && { mode=${XFILEMODE}; shift; } || { mode=${FILEMODE}; }
	from=${1}
	to=${2}$([ -d "$2" ] && { echo "/\c"; ${BASENAME} ${1}; })
	${RM} ${to} || error "Can't overwrite ${to}"
	${CP} ${from} ${to} || error "Can't copy ${from} to ${to}"
	${CHMOD} ${mode} ${to} || error "Can't chmod ${to}"
}

# Prints the supplied error message and exits the script
# error (errmsg)
error() {
	ERROR="ERROR: ${1}\nExiting ${SCRIPT:-${0}}"

	echo "$ERROR"
	exit 1
}

# Prints the usage message and exits the script
# usage ()
usage() {
	USAGE="Usage: $0 [-d] [-i image_dir] ws1 [ws2]...\n \
	where:  -d = build images from the debug build (default is non-debug build)\n \
		-i image_dir = CD/DVD images directory (default is ws1)"

	echo "$USAGE"
	exit 1
}

#
# Main program
#
[ $# -lt 1 ] && usage

# Get options
OPTIND=1
while getopts di: opt
do
	case ${opt} in
		d) DEBUG_BUILD=1;;
		i) IMAGEDIR=${OPTARG};;
		\?) usage;;
	esac
done

# Correct argument count after options
shift $((${OPTIND} - 1))
[ $# -lt 1 ] && usage

# Get the workspaces
while [ $# -gt 0 ]
do
	ws=${1}

	# Check that the workspace is built
	if [ ! -d "${ws}/Codemgr_wsdata" ]
	then
		error "${ws} is not the root directory of a workspace"
	elif [ ! -f "${ws}/${DOT_BUILD_LAST_OS}" -o ! -f "${ws}/${DOT_BUILD_OS_VERSION}" ]
	then
		error "${ws} is not a built workspace"
	fi

	# Extract and store the workspace arch/os info
	if [ "$(cut -d' ' -f1 ${ws}/${DOT_BUILD_OS_VERSION})" = "SunOS" ]
	then
		arch=$(cut -d' ' -f6 ${ws}/${DOT_BUILD_OS_VERSION})
		[ "${arch}" != "sparc" ] && arch=x86
		os=$(cut -d'.' -f2 ${ws}/${DOT_BUILD_LAST_OS})
		wsnum=${#WORKSPACES[*]}
		WORKSPACES[${wsnum}]=${ws}
		WORKSPACES_ARCH[${wsnum}]=${arch}
		WORKSPACES_OS[${wsnum}]=${os}
	else
		error "${ws} contains a build of an unknown arch/os combination"
	fi

	# Check that the workspace contains at least one cdimage/dvdimage script
	if [ ! -f "$(${LS} ${ws}/${IMAGE_SCRIPT_DIR}/*${CDIMAGE_SCRIPT_SUFFIX} ${ws}/${IMAGE_SCRIPT_DIR}/*${DVDIMAGE_SCRIPT_SUFFIX} 2>/dev/null | head -1)" ]
	then
		error "Can't find ${IMAGE_SCRIPT_DIR}/*${CDIMAGE_SCRIPT_SUFFIX} or ${IMAGE_SCRIPT_DIR}/*${DVDIMAGE_SCRIPT_SUFFIX} in ${ws}"
	fi

	shift
done

# Set the images directory to the first workspace specified, if the
# user didn't specify an alternate location
IMAGEDIR=${IMAGEDIR:-${WORKSPACES[0]}}
if [ ! -e "${IMAGEDIR}" ]
then
	${MKDIR} ${IMAGEDIR} || error "Can't create directory ${IMAGEDIR}"
fi

IMAGETYPE=$([ "${DEBUG_BUILD}" -eq 0 ] && echo "non-")debug
echo "CD/DVD ${IMAGETYPE} images will be located in ${IMAGEDIR}"

# Remove any existing images which are about to be rebuilt
wsnum=0
while [ $wsnum -lt ${#WORKSPACES[*]} ]
do
	for script in $(${LS} ${WORKSPACES[${wsnum}]}/${IMAGE_SCRIPT_DIR}/*${CDIMAGE_SCRIPT_SUFFIX} ${WORKSPACES[${wsnum}]}/${IMAGE_SCRIPT_DIR}/*${DVDIMAGE_SCRIPT_SUFFIX} 2>/dev/null)
	do
		if [ -n "$(echo ${script} | ${EGREP} "${CDIMAGE_SCRIPT_SUFFIX}$")" ]
		then
			image=cdrom.$(${BASENAME} ${script} | ${SED} -e "s:${CDIMAGE_SCRIPT_SUFFIX}$::")_${WORKSPACES_ARCH[${wsnum}]}
		else
			image=dvd.$(${BASENAME} ${script} | ${SED} -e "s:${DVDIMAGE_SCRIPT_SUFFIX}$::")
		fi

		if [ -d ${IMAGEDIR}/${image} ]
		then
			echo "Removing old image ${IMAGEDIR}/${image}"
			${RM} -r ${IMAGEDIR}/${image} || error "Can't delete ${IMAGEDIR}/${image}"
		fi
	done

	wsnum=$(($wsnum + 1))
done

# Build the CD/DVD images
wsnum=0
while [ $wsnum -lt ${#WORKSPACES[*]} ]
do
	echo "Processing workspace ${WORKSPACES[${wsnum}]}"
	for script in $(${LS} ${WORKSPACES[${wsnum}]}/${IMAGE_SCRIPT_DIR}/*${CDIMAGE_SCRIPT_SUFFIX} ${WORKSPACES[${wsnum}]}/${IMAGE_SCRIPT_DIR}/*${DVDIMAGE_SCRIPT_SUFFIX} 2>/dev/null)
	do
		if [ -n "$(echo ${script} | ${EGREP} "${CDIMAGE_SCRIPT_SUFFIX}$")" ]
		then
			image=cdrom.$(${BASENAME} ${script} | ${SED} -e "s:${CDIMAGE_SCRIPT_SUFFIX}$::")_${WORKSPACES_ARCH[${wsnum}]}
		else
			image=dvd.$(${BASENAME} ${script} | ${SED} -e "s:${DVDIMAGE_SCRIPT_SUFFIX}$::")
		fi

		echo "Building ${IMAGETYPE} image ${IMAGEDIR}/${image}"
		build_image ${IMAGEDIR}/${image} ${WORKSPACES[${wsnum}]} ${script} ${WORKSPACES_ARCH[${wsnum}]} ${WORKSPACES_OS[${wsnum}]} ${DEBUG_BUILD}
	done

	wsnum=$(($wsnum + 1))
done
