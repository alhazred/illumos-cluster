#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)chk.ksh	1.18	09/04/07 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# chk - Generate report from workspace .ln/.scmsgs files
#

#
# Common utilities for use below
#
FIND=/usr/bin/find
CAT=/bin/cat
RM=/bin/rm
ECHO=/bin/echo
PRINTF=/bin/printf
DATE=$(/bin/date +%d%b%y)
TOUCH=/bin/touch

#
# Convert relative pathname to absolute, canonical pathname;
# i.e., one that does not contain ".", "..", or symlinks.
# Do this conversion to enable path comparisons and use of cd.
#
function get_abs_path {
	typeset f="$1"
	if [ -d "$f" ]; then
		(cd "$f" && /bin/pwd)
	else
		${ECHO} $(cd $(dirname "$f") && /bin/pwd)/$(basename "$f")
	fi
}

#
# Attempt to discover Solaris version of last build in workspace $GATE.
# If found, echo it in the form "Sol_N".  Otherwise echo "unknown".
#
function get_bld_sol_vers {
	typeset bldos="$($CAT $GATE/usr/src/.build.last_OS)"
	case "$bldos" in
		5.*) $ECHO Sol_${bldos##5.} ;;
		*) $ECHO "unknown"
	esac
}

#
# Convert various forms of arg 1, the Solaris version string,
# to the "Sol_N" form, and echo it.
# If arg 1 is the empty string, attempt to discover Solaris version
# of last build.
#
function get_sol_vers {
	typeset osvers="$1"
	case "$osvers" in
		Sol_[0-9]*) $ECHO $osvers ;;
		5.*) $ECHO Sol_${osvers##5.} ;;
		[sS][0-9]*) $ECHO Sol_${osvers##?} ;;
		"") $ECHO $(get_bld_sol_vers) ;;
		*) $ECHO "unknown" ;;
	esac
}

#
# Default to current workspace.
#
GATE=${CODEMGR_WS}
SOL_VER=
MACH=$(uname -p)

CHKTYPE="lint"
JUST_SUMMARY=0

USAGE="$0 [-o <owners>] [-p <parent_ws>] [-n] [-s <Solaris version>] [-a sparc|i386] -w <workspace>"

set -- $(getopt nw:p:l:o:s:t:a: $*)
if [ $? != 0 ]; then
	echo $USAGE
	exit 2
fi

for i in $*
do
	case $i in
		-w) GATE=$(get_abs_path $2); shift 2;;
		-p) PARENT_WS=$(get_abs_path $2); shift 2;;
		-l) HISTORICAL_SUMMARY=$(get_abs_path $2); shift 2;;
		-o) OWNERS=$(get_abs_path $2); shift 2;;
		-n) JUST_SUMMARY=1; shift;;
		-s) osvers=$2; shift 2;;
		-t) CHKTYPE=$2; shift 2;;
		-a) MACH=$2; shift 2;;
		--) shift; break;;
	esac
done

case "$MACH" in
sparc|i386) ;;
*) $ECHO $USAGE; exit 2 ;;
esac

if [ "${GATE}" = "" -o ! -d "${GATE}" ]; then
	echo $USAGE
	exit 2
fi

#
# Convert the Solaris version string provided on command line to "Sol_N"
# form if necessary.  If not provided, attempt to discover Solaris
# version of last build in the workspace.
#
SOL_VER=$(get_sol_vers "$osvers")
if [ "$SOL_VER" = "unknown" ]; then
	${ECHO} "Cannot determine Solaris version.  Re-run with appropriate -s option."
	exit 1
fi

ROOT_PROTO=${ROOT_PROTO:-proto/$SOL_VER}

#
# Make sure the report directory exists.
#
RPTDIR=${GATE}/${ROOT_PROTO}/${CHKTYPE}_${MACH}
SUMMARY=${RPTDIR}/${CHKTYPE}.summary
OWNER_REPORT=${RPTDIR}/${CHKTYPE}.report

if [ ! -d ${RPTDIR} ]; then
	mkdir ${RPTDIR} || exit $?
fi

#
# The owners file resides in the workspace 
#
if [ "$OWNERS" = "" ]; then
	if [ -f $GATE/usr/src/tools/etc/owners ]; then
		OWNERS=$GATE/usr/src/tools/etc/owners
	elif [ -f $GATE/usr/closed/tools/etc/owners ]; then
		OWNERS=$GATE/usr/closed/tools/etc/owners
	fi
fi

if [ ! -f ${OWNERS} ]; then
	echo 'Cannot locate owners specified with -o option'
	exit 2
fi

TMPNAME=/tmp/${CHKTYPE}.$$

#
# The comment below describes how we used to do things.  Starting with
# sc32-gate we designate an owner for orphans by placing a "*" pattern at
# the bottom of the owners file.  And we use the string "EXCLUDED" rather
# than handle a special case, "-".
#
# Owner for those files which don't currently have an owner listed
# in owners file.  Exclude owner is all files which have
# been excluded from ownership.
#
ORPHAN=ORPHAN
EXCLUDED=EXCLUDED

if [ "${PARENT_WS}" = "" ]; then
	PARENT_WS=$(workspace parent ${GATE})
fi

#
# Make sure the workspace specified is accessible
#
[ ! -d ${GATE}/usr/src ] && ${ECHO} Cannot access ${GATE}/usr/src && exit 1
cd ${GATE}/usr/src

#
# Report what we're doing
#
${ECHO} Generating $CHKTYPE report for $GATE
if [ "$OWNERS" != "" ]; then
	${ECHO} "Using ${OWNERS}"
else
	${ECHO} "No owners file exists"
fi
${ECHO} Results in ${RPTDIR}

#
# Default our historical summary
#
if [ "${HISTORICAL_SUMMARY}" != "" ]; then
	if [ "${HISTORICAL_SUMMARY}" = "${SUMMARY}" ]; then
		mv ${SUMMARY} ${SUMMARY}.old
		HISTORICAL_SUMMARY=${SUMMARY}.old
	fi
	${ECHO} "Comparing against ${HISTORICAL_SUMMARY}"
else
	if [ -f ${PARENT_WS}/${ROOT_PROTO}/${CHKTYPE}_${MACH}/${CHKTYPE}.summary ]; then
		HISTORICAL_SUMMARY=${PARENT_WS}/${ROOT_PROTO}/${CHKTYPE}_${MACH}/${CHKTYPE}.summary
	fi
fi

#
# And do it...
#

if [ $JUST_SUMMARY -eq 0  -a  "$OWNERS" != "" ]; then
(
	if [ "${CHKTYPE}" = "lint" ]; then
		${FIND} . -name \*.ln -print
	elif [ "${CHKTYPE}" = "scmsgs" ]; then
		${FIND} . -name \*.scmsgs -print
	fi
) | (
	i=0
	j=0
	# read pattern, owner, trailing stuff
	while read pat ow ws
	do
		case $pat in
		\#*)    continue;;
		*=*)    continue;;
		"")     continue;;
		esac
		#
		# If the owner is '-', then it's
		# an excluded file.
		#
		if [ "$ow" = '-' ]
		then
			excludelist[$j]=$pat
			let j=j+1
			continue
		fi
		#
		# Otherwise, add it to the list of patterns
		# and associated owners
		#
		pattern[$i]=$pat
		owner[$i]=$ow
		let ${ow}=0
		let i=i+1
	done < ${OWNERS}

	#
	# Scan the list of files
	#
	npat=$i
	nexcl=$j
	while read file
	do
		#
		# FlexeLint's output looks like this, where the first line is blank
		# and the second line contains the string "--- Module:" :
		#
		# <blank line>
		# --- Module:   <filename>
		# <line number>: {Error,Warning,Info} blah blah blah
		# <line number>: {Error,Warning,Info} blah blah blah
		# ...
		# <line number>: {Error,Warning,Info} blah blah blah
		#
		# So if there are only two lines, there are no errors, warnings, or info messages.
		#
		lines=$(wc -l < $file)
		if [ "${CHKTYPE}" = "lint" ]; then
			[ $lines -eq 2 ] && continue
			[ $lines -eq 0 ] && continue
		elif [ "${CHKTYPE}" = "scmsgs" ]; then
			[ $lines -eq 0 ] && continue
		fi

		#
		# Check to see if it's excluded
		#
		i=0
		while ((i < nexcl))
		do
			if [ "$file" = "./${excludelist[$i]}" ]
			then
				${TOUCH} ${TMPNAME}.${EXCLUDED}
				${ECHO} $file >> ${TMPNAME}.${EXCLUDED}
				continue 2
			fi
			let i=i+1
		done
		#
		# Scan for the owner of the file
		#
		i=0
		while ((i < npat))
		do
			if [ "$file" = "./${pattern[$i]}" ]
			then
				let val=${owner[$i]}
				let val=${val}+1
				let ${owner[$i]}=${val}
				${TOUCH} ${TMPNAME}.${owner[$i]}
				${ECHO} $file >> ${TMPNAME}.${owner[$i]}
				continue 2
			fi
			let i=i+1
		done

		#
		# If no owner was found, add it to the orphan list
		#
		${TOUCH} ${TMPNAME}.${ORPHAN}
		[ $i -eq $npat ] && ${ECHO} $file >> ${TMPNAME}.${ORPHAN}
	done
)

#
# Generate report and send out notification email if required
#
if [ -f ${TMPNAME}.* ]; then
if [ "${CHKTYPE}" = "lint" ]; then
	for i in ${TMPNAME}.*
	do
		owner=$(expr $i : "${TMPNAME}.\(.*\)")
		#
		# Count errors/warnings/info messages for each file
		# calculating totals as we go.
		#
		terr=0
		twarn=0
		tinfo=0
		ttotal=0
		for j in $(${CAT} $i)
		do
			err=$(grep "Error(" $j | wc -l)
			let err=err
			let terr=terr+err
			warn=$(grep "Warning(" $j | wc -l)
			let warn=warn
			let twarn=twarn+warn
			info=$(grep "Info(" $j | wc -l)
			let info=info
			let tinfo=tinfo+info
			let total=err+warn+info
			let total=total
			[ $total -eq 0 ] && continue
			let ttotal=ttotal+total
			cnts="${err}/${warn}/${info}"
			${PRINTF} "%-5d\t%-14s\t%s\n" ${total} ${cnts} ${j} >> \
			    ${TMPNAME}.report.$owner
		done
		cnts=$terr/$twarn/$tinfo
		${PRINTF} "%-10s\t%5d\t\t%14s\n" $owner $ttotal $cnts >> \
		    ${TMPNAME}_summary
		(
			${ECHO} ===========================================
			${ECHO} "Responsible Manager:\t$owner"
			${ECHO} ===========================================
			${PRINTF} "%-5s\t%-14s\n" Total Errs/Warn/Info
			cnts=$terr/$twarn/$tinfo
			${PRINTF} "%-5d\t%-14s\t%s\n" ${ttotal} ${cnts} ${DATE}
			${ECHO} ===========================================
			# report file may not exist if owner's files had
			# zero errors/warnings/info messages
			if [ -f ${TMPNAME}.report.$owner ]; then
				${CAT} ${TMPNAME}.report.$owner | sort -nr
			fi
			${ECHO} ===========================================
		) > ${OWNER_REPORT}.$owner
	done
	# Remove all the tmp lint files so far except for the summary.
	${RM} ${TMPNAME}.*
elif [ "$CHKTYPE" = "scmsgs" ]; then
	for i in ${TMPNAME}.*
	do
		owner=$(expr $i : "${TMPNAME}.\(.*\)")
		#
		# Count error messages for each file
		#
		ttotal=0
		for j in $(${CAT} $i)
		do
			total=$(cat $j | wc -l)
			let total=total
			[ $total -eq 0 ] && continue
			let ttotal=ttotal+total
			${PRINTF} "%-5d\t%-14s\t%s\n" ${total} ${j} >> \
			${TMPNAME}.report.$owner
		done
		${PRINTF} "%-10s\t%5d\t\t%14s\n" $owner $ttotal >> \
		${TMPNAME}_summary
		(
			${ECHO} ===========================================
			${ECHO} "RM:\t$owner"
			${ECHO} ===========================================
			${PRINTF} "%-8s\n" Messages
			${PRINTF} "%-5d\t\t%s\n" ${ttotal} ${DATE}
			${ECHO} ===========================================
			${CAT} ${TMPNAME}.report.$owner | sort -nr
			${ECHO} ===========================================
		) > ${OWNER_REPORT}.$owner
		${RM} $i ${TMPNAME}.report.$owner
	done
fi
fi

#
# Create summary
#
if [ "$CHKTYPE" = "lint" -a -f ${TMPNAME}_summary ]; then
(
	${ECHO} =======================================================
        ${PRINTF} "%-10s\t%5s\t\t%14s\t%s\n" RM Total Errs/Warn/Info ${DATE}
	${ECHO} =======================================================
	${CAT} ${TMPNAME}_summary | sort +1 -nr
	${ECHO} =======================================================
) > ${SUMMARY}
	${RM} ${TMPNAME}_summary
elif [ "$CHKTYPE" = "scmsgs" -a -f ${TMPNAME}_summary ]; then
(
	${ECHO} =======================================================
	${PRINTF} "%-10s\t%8s\t%s\n" RM Messages ${DATE}
	${ECHO} =======================================================
	${CAT} ${TMPNAME}_summary | sort +1 -nr
	${ECHO} =======================================================
) > ${SUMMARY}
${RM} ${TMPNAME}_summary
fi
fi

#
# Do differences
#
if [ "${HISTORICAL_SUMMARY}" = ""  -o  ! -f "${HISTORICAL_SUMMARY}" ]; then
	${ECHO} "Historical summary file ${HISTORICAL_SUMMARY} does not exist."
	exit 0
fi

if [ ! -f "${SUMMARY}" ]; then
	${ECHO} "Summary file ${SUMMARY} does not exist, so cannot produce diffs report."
	exit 0
fi

#
# Find total list of workspace owners
#
while read W
do
	case "$W" in
		RM* | ===*) continue;;
		Responsible* | Manager*) continue;;
	esac
	W_name=$(expr "$W" : '\([^ 	]*\).*')
	eval _${W_name}=\'"$W"\'
	W_names="${W_names} ${W_name}"
done < ${SUMMARY}

#
# Find total list of historical owners
#
while read H
do
	case "$H" in
		RM* | ===*) continue;;
		Responsible* | Manager*) continue;;
	esac
	H_name=$(expr "$H" : '\([^ 	]*\).*')
	eval ${H_name}=\'"$H"\'
	H_names="${H_names} ${H_name}"
done < ${HISTORICAL_SUMMARY}

#
# Find list of new workspace owners
#
NW_names=""
for i in ${W_names}
do
	for j in ${H_names}
	do
		[ "$j" = "$i" ] && break
	done
	if [ "$j" != "$i" ]; then
		NW_names="${NW_names} $i"
	else
		OW_names="${OW_names} $i"
	fi
done

#
# Find list of new historical workspace owners
#
NH_names=""
for i in ${H_names}
do
	for j in ${W_names}
	do
		[ "$j" = "$i" ] && break
	done
	if [ "$j" != "$i" ]; then
		NH_names="${NH_names} $i"
	else
		OH_names="${OH_names} $i"
	fi
done

#
# Print differences in existing owners lists
#
for i in ${OW_names}
do
	eval W=\${_${i}}
	W_total=$(expr "$W" : '[^ 	]*[ 	]*\([0-9]*\).*')
	eval H=\${${i}}
	H_total=$(expr "$H" : '[^ 	]*[ 	]*\([0-9]*\).*')
	DIFF=$(expr ${W_total} - ${H_total})
	case ${DIFF} in
		-*);;
		0) DIFF="";;
		*) DIFF="+${DIFF}";;
	esac
	str=$(eval echo \"\$\{_${i}\}\")
	${PRINTF} "%s\t%5s\n" "${str}" ${DIFF} >> ${TMPNAME}_diffs
done

#
# Print workspace owners which are new from the parent
#
for i in ${NW_names}
do
	eval W=\${_${i}}
	W_total=+$(expr "$W" : '[^ 	]*[ 	]*\([0-9]*\).*')
	str=$(eval echo \"\$\{_${i}\}\")
	${PRINTF} "%s\t%5s\n" "$str" $W_total >> ${TMPNAME}_diffs
done

#
# Print parent owners which no longer exist in workspace
#
for i in ${NH_names}
do
	eval H=\${${i}}
	H_total=-$(expr "$H" : '[^ 	]*[ 	]*\([0-9]*\).*')
	str=$(eval echo \"\$\{${i}\}\")
	${PRINTF} "%-10s\t%5s\n" $i $H_total >> ${TMPNAME}_diffs
done

( 
  ${ECHO} =========================================================
  ${ECHO} ${CHKTYPE} differences summary - ${DATE}
  ${ECHO} =========================================================
  ${ECHO} "Workspace:\t${SUMMARY}"
  ${ECHO} "Parent:\t\t${HISTORICAL_SUMMARY}"
  ${ECHO} =========================================================
  ${CAT} ${TMPNAME}_diffs
  ${ECHO} =========================================================
) > ${SUMMARY}.diffs
${RM} ${TMPNAME}_diffs
