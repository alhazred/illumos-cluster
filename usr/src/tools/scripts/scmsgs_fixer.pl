#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_fixer.pl	1.7	08/05/20 SMI"
#
#	scmsgs_fixer [-l=LINELEN] [-s=SUF] FILE FILE ...
# fix up the SCMSGS comments in the given source FILEs.
# insertmsgs will output to the file FILE${SUF} in each case.
# the effect will be that all SCMSGS comments in the given FILEs
# will be put into a canonical pretty-printed, properly
# indented form.

=head1
NAME

scmsgs_fixer - fix SCMSGS comments

=head1
SYNOPSIS

scmsgs_fixer [B<-l>=LINELEN] [B<-s>=SUFFIX] FILES ...

=head1
DESCRIPTION

scmsgs_fixer is a script intended to assist in the creation of
well-formatted SCMSGS comments.  It works with .c, .cc, and shell files.

The line length for formatting purposes can be set with
the B<-l>=LINELEN option.
The default LINELEN is 78 chars.

The suffix for output files can be set with
the B<-s>=SUFFIX option.  The default suffix is ".new".

=head1
EXAMPLE

scmsgs_fixer netapp_nas_qd.cc

creates a file netapp_nas_qd.cc.new that is the same as netapp_nas_qd.cc
except that any SCMSGS comments in the file are properly
indented and formatted.

=cut

$opt_s = $s || ".new";
$opt_l = $l || 78;
$PROGDIR = "." unless ($PROGDIR) = ($0 =~ m{(.*)/});

$getmsgs = "$PROGDIR/scmsgs_getmsgs -x 2>/dev/null";
$insertmsgs = "$PROGDIR/scmsgs_insert -f=/dev/null -v -F -l=$opt_l -s=$opt_s";

foreach $arg (@ARGV) {
	exec("$getmsgs $arg | $insertmsgs");
}
