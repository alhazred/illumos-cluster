#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_getmsgs.pl	1.12	08/05/20 SMI"
#
#	scmsgs_getmsgs -L [-OPTIONS] < SRCFILENAMES
#	scmsgs_getmsgs [-OPTIONS] SRCFILE ... 
#		-F=FUNCFILE	get functions from FUNCFILE
#		-L		read filenames from STDIN
#		-c		print code of call
#		-d		debugging
#		-v		verbose
#		-x		extended-form output (like sc_msgid_expl)
#
# scan SRCFILEs for calls to syslog (and various wrapper functions),
# and output info about each call (to stdout).  scmsgs_getmsgs can
# recognize syslog calls and SCMSGS comments in C, C++, and shell files.
# files of other types are ignored.
#
# SRCFILES may either be named on the command line, or --- with
# the -L flag --- read from STDIN.  the latter mode allows scmsgs_getmsgs
# to be used in a pipeline where an earlier stage scans the source tree
# to find candidate source files. (eg, scmsgs_getfiles)
#
# the output is a series of message entries, in
# the same format as used in the explanations file sc_msgid_expl.
# Example:
#
#	477378:	Failed to restart the service.
#	Location:
#		lib/libdsdev/common/scds_fm.c:815
#

# modify @INC so as to get SCMSGS.pm from $PROGDIR.
BEGIN {
	$PROGDIR = "." unless (($PROGDIR) = ($0 =~ m,^(.*)/,));
	unshift @INC, $PROGDIR;
}

use SCMSGS;



# ---------- start of subroutines

sub mkmsgid
{
	my $s = shift;
	$s =~ s/\\n//g;
	return &SCMSGS::STRLOG_MAKE_MSGID($s);
}

# print an scmsg entry to stdout
sub emit
{
	my $s = shift;

	if (keys %COMMENT > 0) {
		$COMMENT{commentloc} = "$FILE:$COMMENT_LINENO+$COMMENT_NLINES";
		if ($COMMENT_LINENO+$COMMENT_NLINES+$MAX_COMMENT_DIST
			<= $CALL_LINENO) {
print STDERR "# warning: SCMSGS comment at $FILE:$COMMENT_LINENO is too far from call at line $CALL_LINENO;\n and will be ignored\n";
			%COMMENT = ();
		}
	}
	else {
		return if $s eq '';
	}

	$COMMENT{msgid} = &mkmsgid($s);
	$COMMENT{msgtext} = $s;
	my $loc = "$FILE:$CALL_LINENO";
	if ($COMMENT{explanation} ne '' || $COMMENT{user_action} ne '') {
		$loc .= "*";
	}
	$COMMENT{location} = $loc;

	&dump_entry(\*STDOUT, \%COMMENT)    unless $COMMENT{skip};

	%COMMENT = ();
	$EMITCOUNT++;
}

# this string contains a shell script that handles flag arguments
# and prints the "message text" of a call to the scds_syslog command.
$SYSLOG_SH_PROTO = <<'EOF';
	argfunc() {
		while getopts ':m:p:t:' opt; do
			if [ $opt = m ]; then
				echo "$OPTARG";
			fi;
		done;
	}
EOF
	;

# this string contains a shell script that handles flag arguments
# and prints the "message text" of a call to the logger command.
$LOGGER_SH_PROTO = <<'EOF';
	argfunc() {
		while getopts ':f:p:t:' opt; do
			if [ "x$opt" = xt ]; then
				tag=$OPTARG;
			fi;
		done;
		shift `expr $OPTIND - 1`;
		echo "$@";
	}
EOF
	;

# the %SHESC and %SHUNESC hashes are used to protect certain special
# characters from expansion when we send a string to the shell.
%SHESC = (
	'$', 'DOLLAR',
	'(', 'LPAR',
	')', 'RPAR',
	'`', 'BTICK',
);

@SHUNESC{values %SHESC} = keys %SHESC;

$ECHARS = join("", keys %SHESC);
$SHUNPAT = join("|", values %SHESC);

sub process_sh_call
{
	my ($args, $proto) = @_;
print STDERR "process_sh_call($args)\n"    if $opt_d;
	$proto =~ s/\n/ /gs;

	$args =~ s/\s*;;\s*$//;
	$args =~ s/[$ECHARS]/%$SHESC{$&}%/g;
	my $cmd = "$proto; argfunc $args";
print STDERR "?? cmd=$cmd\n" if $opt_d;
	my (@s) = `sh -c \Q$cmd\E 2>&1`;
	chomp @s;
	$s = join("", @s);
print STDERR "?? s1=$s\n" if $opt_d;
	$s =~ s/%($SHUNPAT)%/$SHUNESC{$1}/g;
print STDERR "?? s2=$s\n" if $opt_d;

	# there's a big problem with the above code.
	# the problem is related to the treatment of backslashed $ chars.
	# under the old scmsgs process, it was not realized that it was
	# an error for the call to scds_syslog to contain $vars.
	# consequently, the old process tried to preserve the variable
	# references so that "$XYZ" in the call would result in "$XYZ"
	# appearing in the message text.  essentially the process
	# imposed an implicit escaping of any $ character in the string.
	#
	# however, under the new scmsgs process, we realize that it is
	# inherently an error for the call to contain $vars (since it
	# will cause the MSGID to be different at runtime than it was
	# when we run the scmsgs scripts).  we don't want $ chars to
	# appear at all in the call, but the above code tries to maintain
	# compatibility with the old process if they do appear.
	#
	# unfortunately, this doesn't give the right answer if the
	# call contains an EXPLICITLY escaped $ character, ie, if the
	# intent of the call is to display a literal "$".
	#
	# we can't maintain compatibility AND correctness.
	# i'm not sure what's the best solution.  for now, let's
	# insert a hack to fix the case where there's a quoted $ or `.
	# this is definitely not a clean or general solution.
	#
	$s =~ s/\\([\$`])/$1/g;

	# normally we want to ignore any constructs that superfically
	# resemble a syslog call but aren't really.  the symptom of
	# this is usually a message string that doesn't make sense.
	# BUT, if there is a SCMSGS comment associated with this
	# instance, we override this sanity check.
	# we'll let the downstream code figure out what to do with it.
	if (keys %COMMENT == 0) {
		return if $s =~ /^\s*$/;

		# if the message consists of a single variable expansion,
		# skip it.
		return if $s =~ /^\s*\${?\w*}?\s*$/;
	}

	&emit($s);
}

# scan a file of unknown type:
# if the first line looks like a sh script,
# then process the file as such.  otherwise ignore the file.
sub scan_maybesh_file
{
print STDERR "scan_maybesh_file\n" if $opt_d;
	local $fh = shift;
	$_ = <$fh>;
	$LINENO++;
	if (m{^#!\s*/.*bin/.*sh}) {
		&scan_sh_file($fh);
	}
}

sub scan_csh_file { &scan_sh_file(@_); }

sub scan_ksh_file { &scan_sh_file(@_); }

sub qstart
{
	return '';	# for now, we don't do it.
}

sub scan_sh_file
{
print STDERR "scan_sh_file\n"    if $opt_d;
	local $fh = shift;
	my $line = '';
	my @comment;
	my $continuing = 0;
	my $inquote = 0;

	MAJOR:
	while (<$fh>) {
		chomp;
		$LINENO++;

		if (@comment > 0) {
			# we're in the midst of reading an SCMSGS comment.
			if (!m/^\s*#/) {
				# we've found the first line after the comment.
				my $save = $_;
				process_sh_comment(@comment);
				@comment = ();
				$LINENO--;
				$_ = $save;
				redo MAJOR;
			}

			push @comment, $_;
			next;
		}

		if (m/^\s*#\s?SCMSGS\b(.*)$/) {
			# we found the beginning of an SCMSGS comment.
			# switch to comment mode.
			@comment = ("# ".$1);
			if (keys %COMMENT > 0) {
print STDERR "# warning: unused SCMSGS comment at $FILE:$COMMENT_LINENO is being superseded by another SCMSGS comment at line $LINENO;\n and will be discarded.\n";
				%COMMENT = ();
			}
			$COMMENT_LINENO = $LINENO;
			next;
		}

		next if /^\s*#/;

		if ($continuing) {
			if (/\\$/) {
				s/\\$//;
				$line .= $_;
				next;
			}
			$line .= $_;
			$continuing = 0;
		}
		elsif ($inquote) {
			$line .= $_;
			if (&qstart($_) eq '') {
				# this line is balanced, so it does not
				# end the quoted string.
				next;
			}
			# unbalanced - done
			$inquote = '';
		}
		else {
			if ($inquote = &qstart($_)) {
				$CALL_LINENO = $LINENO;
				$line = $_."\n";
				next;
			}

			if (/\\$/) {
				# the first continued line
				$CALL_LINENO = $LINENO;
				s/\\$//;
				$line = $_;
				$continuing = 1;
				next;
			}
			# a normal, non-continued line.
			$CALL_LINENO = $LINENO;
			$line = $_;
		}

		my ($cmd, $args) = ($line =~ m/^\s*(\S*)\s*(.*)/);
		if ($cmd =~ m{^(\S*/)?scds_syslog$}) {
			$CALL_TEXT = $line;
			&process_sh_call($args, $SYSLOG_SH_PROTO);
		}
		elsif ($cmd =~ m{\blogger$} || $cmd =~ /\${LOGGER}$/) {
			$CALL_TEXT = $line;
			&process_sh_call($args, $LOGGER_SH_PROTO);
		}
		$line = '';
	}
}

sub scan_c_file { return &scan_cc_file(@_); }

%MARKERS = (
	"Q", "\"",
	"A", "\"",
	"B", "\"",
	"C", ",",
);

# process_cc_call(calltext, N) --
# calltext is a complete subroutine call.  we want to extract the Nth
# argument, which is assumed to be a C string.  at first glance,
# it seems simple: just split at the commas.  but there are pitfalls.
# - strings may contain escaped double-quote chars.
# - strings may contain embedded commas.
# - preceding expressions may contain function calls, strings, etc.
# to handle all these cases correctly would require both a C lexical
# analyzer and parser.  that's too much trouble, so we make some
# simplifying assumptions.  the following code should work on almost all
# real-world wrapper calls.  at any rate it provides enough latitude
# that all real-world wrapper calls can be reasonably written to work.
#
# this algorithm could be made a whole lot simpler if the developers
# would only agree to follow some simple formatting conventions, but nooo...
# so we're forced to use a complicated set of ad hoc heuristics.
sub process_cc_call
{
print STDERR "process_cc_call(@_)\n"  if $opt_d;
	local($_, $argno) = @_;
	my ($nc) = 0 + keys %COMMENT;
	$argno--;	# convert argno to 0-indexing.

	# special hack noise-rejection filter:
	# if what we think is the argument list does not contain any
	# quoted strings, chances are this is not really a syslog call.
	# (eg, it might be part of a comment, or a subroutine definition.
	# so skip it.
	return if m{^[^\(\)]*\w\s*\([^"\(\)]*\)} & !$nc;

	# step a.
	# strip off outer subroutine call and parens.
	if (/SC_SYSLOG_MESSAGE/) {
		# SC_SYSLOG_MESSAGE is a special case, because it is sometimes
		# called as a sub-expression within some other function call.
		# in order for our processing to work correctly, the other
		# (outer) function call if any has to be stripped away in
		# addition to the SC_SYSLOG_MESSAGE itself.
print STDERR "a1=$_\n" if $opt_d;
		s/.*SC_SYSLOG_MESSAGE\(//;
		s/"\s*\).*/"/;	# XXX what if this matches within the string?
print STDERR "a2=$_\n" if $opt_d;
	}
	else {
		# here we can assume the top-level function call is the
		# one we want to dissect.  strip off function name
		# and outer function call parens.
		# Examples:
		#	syslog(a,b,c) --> a,b,c
		s/^\s*([^\(\)]*)\((.*)\)\s*;/$2/;
	}

	# step b.
	# replace all escaped double-quotes with a special marker.
	# this will allow string literals to be more easily matched as
	# whatever occurs between a pair of double-quotes.
	s/\\"/\001Q/g;
print STDERR "!! b _=$_\n" if $opt_d;

	# step c.
	# replace beginning-double-quote and ending-double-quote
	# with distinct special MARKERS.  this will make it easier
	# do the concatenation of adjacent string literals.
	my $nq = s/"([^"]*)"/\001A$1\001B/g;
print STDERR "!! c _=$_\n" if $opt_d;

	# were there any double-quotes substituted?
	# if not, maybe the code is a subroutine definition or a
	# prototype declaration, or maybe the message argument is a
	# variable rather than a literal string.
	# whatever the cause, there's nothing we can do with this call.
	return if !$nq && !$nc;

	# step d.
	# splice together adjacent string literals.
	# eg, "this" "is" "three" "strings" --> "thisisthreestrings"
	s/\001B\s*\001A//g;
print STDERR "!! d _=$_\n" if $opt_d;

	# step e.
	# get rid of outer whitespace adjacent to double quotes.
	s/\s*\001A/\001A/g;
	s/\001B\s*/\001B/g;
print STDERR "!! e _=$_\n" if $opt_d;

	# step f.
	# split out string literals.
	# odd-numbered parts are inside a string literal.
	my @parts = split /\001[AB]/, $_;
	# replace embedded commas with a special marker,
	# in odd-numbered parts.
	for ($i = 1; $i < @parts; $i += 2) {
		$parts[$i] =~ s/,/\001C/g;
	}
print STDERR "!! f _=$_\n" if $opt_d;

	# step g.
	# now glue the string back together, and split at commas.
	# (the remaining commas are now known not to be embedded
	# in string literals.)
	$_ = join("", @parts);
print STDERR "!! g _=$_\n" if $opt_d;

	# step h.
	# select the N'th argument.
	$_ = (split /,/, $_)[$argno];
print STDERR "!! h _=$_\n" if $opt_d;

	# step i.
	# simulate some commonly used macros
	s/\s*(gettext|SYSTEXT|NOGET)\(\s*(.*)\)\s*/$2/;

	# undo the replacements done in previous steps.
	s/\001(\w)/$MARKERS{$1}/g;

	# remove leading and trailing \n's.
	# this is safe to do because the msgid calculation ignores
	# control chars anyway.  we'd like to discard leading and
	# trailing spaces, but that would alter the msgid value,
	# leading to mismatches between the listed msgid and the
	# value printed by the kernel.
	#
	## remove leading and trailing whitespace and newlines.
	# s/^(\s|\\n)*//;
	# s/(\s|\\n)*$//;
	s/^(\\n)*//;
	s/(\\n)*$//;

	# something still needs to be done with internal newlines.

	return if $_ eq '' && !$nc;
print STDERR "!! i _=$_\n" if $opt_d;

	&emit($_);
}

sub scan_cc_file
{
print STDERR "scan_cc_file\n"  if $opt_d;
	local $fh = shift;
	my $argno;
	my $incall = 0;
	my $bslash = 0;
	my $accept_comma = 0;
	my @comment = ();
	my $line;
	my $saw_slashstar = 0;
	my $saw_dblslash = 0;
	my $in_c_comment = 0;
	# TODO: use state machine instead of individual flags

	MAJOR:
	while (<$fh>) {
		chomp;
		$LINENO++;

		if (@comment > 0) {
			# we're in the midst of reading an SCMSGS comment.
			if (!$in_c_comment) {
				if (!m{^\s*//}) {
				# we've found the first line AFTER a // comment.
				my $save = $_;
				&process_cc_comment(@comment);
				@comment = ();
				$LINENO--;
				$_ = $save;
				redo MAJOR;
				}
				# don't do this.
				# elsif (m{^\s*//\s*$}) {
				# # an empty comment line signals the logical
				# # end of the SCMSGS comment.
				# &process_cc_comment(@comment,$_);
				# @comment = ();
				# next;
				# }
			}

			if ($in_c_comment && m{^(.*?)\*/}) {
				# we've found the comment terminator.
				push @comment, $1;
				&process_c_comment(@comment);
				@comment = ();
				next;
			}
			push @comment, $_;
			next;
		}

		# check for start of SCMSGS comment
		if ($saw_slashstar && m{^\s*\*? ?SCMSGS\b(.*)}
		 || (!$saw_slashstar && m{^\s*// ?SCMSGS\b(.*)})) {
			my $rest;
			# we found it
			if (keys %COMMENT > 0) {
print STDERR "# warning: unused SCMSGS comment at $FILE:$COMMENT_LINENO is being superseded by another SCMSGS comment at line $LINENO;\n and will be discarded.\n";
				%COMMENT = ();
			}
			$in_c_comment = $saw_slashstar;
			if ($saw_slashstar) {
				# a c-style comment
				$COMMENT_LINENO = $LINENO-1;
				$rest = "* ".$1;
				if ($rest =~ s,\*/.*,,) {
					# comment ends on same line, so
					# process it immediately.
					# and continue looping in normal mode.
					@comment = ("", $rest);
					process_c_comment(@comment);
					@comment = ();
					next;
				}
				@comment = ("", $rest);
				$saw_slashstar = 0;
			}
			else {
				# a C++-style comment
				$COMMENT_LINENO = $LINENO;
				$rest = "// ".$1;
				@comment = ($rest);
				if ($saw_dblslash) {
					# if the preceding input line was an
					# empty comment line, adjust the 
					# starting lineno to absorb it.
					$COMMENT_LINENO--;
					unshift @comment, '//';
				}
			}

			next;
		}

		if (m{^\s*//\s*$}) {
			# remember the fact that this could be
			# the start of an SCMSGS comment.
			$saw_dblslash = 1;
			next;
		}
		$saw_dblslash = 0;
		if (m{^\s*/\*\s*$}) {
			# remember the fact that this could be
			# the start of an SCMSGS comment.
			$saw_slashstar = 1;
			next;
		}
		$saw_slashstar = 0;

		next if m/NO_SC_SYSLOG_MESSAGE/;
		next if m{^\s*(//|\*)};

		# the "." before $LOGSUBRPAT is to prevent matching the
		# name at the start of a line, where it is likely to
		# be the function's definition, rather than a call to
		# the function.  it also helps by making the function
		# name the second parenthesized sub-pattern ($2) in
		# both regexes.
		#
		# the initial sub-pattern before "log" is there to ensure
		# we match only the cases where it is a class method
		# rather than a math subroutine.
		if (m/(.\b)($LOGSUBRPAT)\(/o	#balance\)
		 || m/(->|\.)\s*(log)\(/) {	#balance\)
			# pickvar is the argument number of the format string
			$pickvar = $PICKVARS{$2};
			# accept_comma means that the end of the call expr
			# may be a comma rather than a semicolon.
			$accept_comma = $2 eq 'SC_SYSLOG_MESSAGE' ? 1 : 0;
			$CALL_LINENO = $LINENO;
			$bslash = 0;
			$line = '';
			$incall = 1;
			# fallsthru
		}

		if ($incall) {
			if (!$bslash) {
				# not inside a string literal, so we
				# can collapse leading whitespace.
				s/^\s*/ /;
			}
			if (m/\)\s*;\s*(\/|$)/
			 || ($accept_comma && m/\)\s*,\s*(\/|$)/)) {
				$line .= $_;
				# we've gathered the whole subroutine call
				$CALL_TEXT = $line;
				&process_cc_call($line, $pickvar);
				$incall = 0;
				next;
			}
			else {
				# handle backslashed continuation lines.
				# this is a potential bug, if the line
				# happens to end with a triple-backslash?
				$bslash = s/(^|[^\\])\\$/$1/;
				$line .= $_;
				next;
			}
		}
	}
}

sub process_c_comment
{
	return if @_ == 0;
# print STDERR "found c COMMENT:\n", join("\n", @_), "\n";
	$_[-1] =~ s,\*/.*,,;
	foreach (@_) { s,^\s*\*? ?,,; }
	&process_comment;
}

sub process_cc_comment
{
	return if @_ == 0;
# print STDERR "found cc COMMENT:\n", join("\n", @_), "\n";
	foreach (@_) { s,^\s*//\s*,,; }
	&process_comment;
}

sub process_comment
{
	%COMMENT = ();
	my $lasttag = '';
	my %tagsadded;
	$COMMENT_NLINES = 0+@_;

	foreach (@_) {
		if (m/^@(\w+)\s*(.*)/) {
			$lasttag = $1;
			if (!$LEGAL_KEYS{$lasttag}) {
print STDERR "# warning: unrecognized key \"$lasttag\" in SCMSGS comment at $FILE:$COMMENT_LINENO\n";
				# XXX let it pass.
				# not sure if this is the right thing to do.

				# $lasttag = '';
				# next;
			}
			$COMMENT{$lasttag} = $2;
			$tagsadded{$lasttag} = 1;
		}
		else {
			if ($lasttag ne '') {
				$COMMENT{$lasttag} .= "\n"
					if ($COMMENT{$lasttag} ne '');
				$COMMENT{$lasttag} .= $_;
			}
			else {
print STDERR "# warning: stray text after SCMSGS keyword at $FILE:$COMMENT_LINENO; ignored\n"
			if ($_ ne '');
			}
		}
	}

	# comments need to have escape processing done when written,
	# to prevent any slash-star that might be present in the text from
	# prematurely ending the SCMSGS comment.  therefore, we need to
	# undo the escape processing when we read such a comment.
	foreach $tag (qw(explanation user_action)) {
		if (!defined $COMMENT{$tag}) {
print STDERR "# warning: missing $tag in SCMSGS comment at $FILE:$COMMENT_LINENO\n";
			next;
		}
		$COMMENT{$tag} =~ s/\n*$//;
		$COMMENT{$tag} = &SCMSGS::unescapetext($COMMENT{$tag});
		if ($COMMENT{$tag} =~ m/^\s*$/) {
print STDERR "# warning: empty $tag in SCMSGS comment at $FILE:$COMMENT_LINENO\n";
		}
	}
}

sub process_sh_comment
{
	return if @_ == 0;
# print STDERR "found sh COMMENT:\n", join("\n", @_), "\n";
	foreach (@_) { s,^\s*#? ?,,; }
	&process_comment;
}

sub dotype
{
	my $filetype = shift;
print STDERR "dotype type=$filetype\n"    if $opt_d;
	local $fh = shift;
	if ($filetype =~ m/^(c|cc|sh|ksh|csh)$/) {
		my $name = "scan_${filetype}_file";
		&{$name}($fh);
	}
	elsif ($filetype eq '') {
		# could be a shell script
		&scan_maybesh_file($fh);
	}
	if (keys %COMMENT > 0) {
print STDERR "# warning: SCMSGS comment at $FILE:$COMMENT_LINENO had no matching call;\n and will be ignored\n";
		%COMMENT = ();
	}
}

# read a SRCFILE and process it.
sub readsrc
{
print STDERR "readsrc(@_)\n"    if $opt_d;
	my $f = shift;
	$FILE = $f;
	$LINENO = 0;
	$EMITCOUNT = 0;
	open(F, "<$f") || warn("$PROG: can't open \"$f\"");

	my ($filetype) = ($f =~ m{\.([^./]*)$});
	&dotype($filetype, \*F);

	close(F);
	return $EMITCOUNT;
}

sub dump_entry
{
	local ($fh, $x) = @_;

	print $fh "\n" if $opt_x;

	print "## code: $CALL_TEXT\n" if $opt_c;

	if ($opt_x) {
	&SCMSGS::print_entry($fh, $x);

	print &SCMSGS::tagstring($x, "location") if ($x->{location} ne '');
	print &SCMSGS::tagstring($x, "commentloc") if ($x->{commentloc} ne '');
	}
	else {
	print $fh $x->{location}, " ", $x->{msgid}, " ", $x->{msgtext}, "\n";
	}
}

sub dofile
{
	my $fn = shift;
	$FILECOUNT++;
	my $n = &readsrc($fn);
	if ($n > 0) {
		$USEDFILECOUNT++;
	}
	$MSGCOUNT += $n;
}

sub main
{
	local $FILECOUNT = 0;
	local $MSGCOUNT = 0;
	local $USEDFILECOUNT = 0;

	&get_funclist($opt_F);

	if ($opt_L) {
		while (<>) {
			chomp;
			&dofile($_);
		}
	}
	else {
		foreach (@ARGV) {
			&dofile($_);
		}
	}

	print STDERR
"### scmsgs_getmsgs: $MSGCOUNT messages found in $USEDFILECOUNT files, out of $FILECOUNT total files.\n"
		if $opt_v;
}

sub get_funclist
{
	my $fn = shift;
	open(FF, "< $fn") || die("can't open \"$fn\"\n");
	my ($func, $argno);
	while (<FF>) {
		chomp;
		s/^\s*//;
		s/^#.*//;
		next if /^$/;
		next if !(($func, $argno) = (m/(\w+)\s*(\d+)/));
		$PICKVARS{$func} = $argno;
print STDERR "will search for $func([#$argno])\n" if $opt_d;

	}

	close(FF);

	# the "log" function is treated specially,
	# so we don't include it in the $LOGSUBRPAT.
	# "log()" is handled in its own pattern.
	$LOGSUBRPAT = join("|", keys %PICKVARS);
	$PICKVARS{log} = 3;
}


# ---------- start of mainline code
# set conventionally-named option variables from their "-s" equivalents.
$opt_F = $F || "$PROGDIR/scmsgs_funcs.txt";
$opt_L = $L;
$opt_c = $c;
$opt_d = $d;
$opt_v = $v;
$opt_x = $x;

@legal_keys =
qw(explanation user_action skip);
@LEGAL_KEYS{@legal_keys} = (1) x @legal_keys;

$MAX_COMMENT_DIST = 20;	# call must occur within this many lines of comment.

($PROG) = ($0 =~ m,([^/]*)$,);

&main;
