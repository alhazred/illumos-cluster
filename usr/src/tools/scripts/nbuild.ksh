#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
# 
# ident	"@(#)nbuild.ksh	1.44	09/04/07 SMI"
#
#	nbuild [-DFNT] [-abilptv] [VAR=VALUE ...]
# do a virtual build of sun cluster software.
# by default, do a clean build of non-debug.
# see Usage for explanation of flags.
# OPTHOME, SPRO and TEAMWARE may be set in the environment or on the command
# line to override /ws/sc31u-tools or /opt defaults.
#
# examples:
#	nbuild -a		deluxe build of non-debug, native OS
#	nbuild -i		incremental build of non-debug, native OS
#	nbuild -D		clean build of debug and non-debug, native OS
#	nbuild -DFi		incremental debug-only build, native OS
#	nbuild -a OS=5.10	deluxe non-debug build of solaris 10
#

function Usage {
	Die \
'usage: nbuild [-'$allflags'] [VAR=VALUE ...]

non-DEBUG only is the default build type.

	-D	do a build with DEBUG on
	-F	do _not_ do a non-DEBUG build
	-N	do non-virtual build
	-T	test mode
	-a	do all extra steps (lint, check, cdrom/dvd, etc)
	-b	build cdrom/dvd images
	-i	incremental build (no clobber)
	-l	build lint and check targets
	-p	send packages to the IPS repository
	-t	build and use tools in usr/src/tools
	-v	verbose (echo major steps to stderr)
'
}

function Ignore_pod {

$CAT<<EOF

=head1
NAME

nbuild - nightly build script

=head1
SYNOPSYS

nbuild [-DFNTabilptv] [VAR=VALUE ...]

=head1
DESCRIPTION

I<nbuild> is a descendant of the old sun cluster I<nightly>(1)
build script.
it is designed to be easier to use.
unlike I<nightly>, the I<nbuild> script does not require an environment file.
I<nbuild> derives reasonable default values for all parameters, so
no environment file is needed.
on the other hand, the defaults can be overridden by explicitly
specifying VAR=VALUE assignments on the I<nbuild> command line.

the principal parameters of a build are:

=over 4

=item (*)

the path to the workspace to build.

=item (*)

whether this is a virtual build,
and if so the path to the destination (child) workspace.

=item (*)

the OS and architecture to build for.

=item (*)

DEBUG/non-DEBUG

=back

I<nbuild> defaults to doing a virtual build,
where the parent (non-virtual) workspace is whatever workspace
contains the current directory when nbuild is invoked.
(ie, to build a workspace, cd to it and then run nbuild.)
by default, the destination workspace is created as a sibling
of the parent workspace.
the destination workspace is given a name derived from
the parent name and the target OS and architecture.

PARENT + OS + MACH

by default, I<nbuild> builds for the native OS and architecture.
these defaults may be over-ridden by command-line assignments.
for example, "nbuild OS=5.10" builds for solaris 10 instead of
whatever the build machine happens to be running.
if the current workspace is (say) "myworksp", and the host
machine architecture is "sparc", then the default destination 
workspace is "myworksp+5.10+sparc".

by default, a non-DEBUG build is performed.

here is an outline of the steps that nbuild performs.

=over 4

=item (*)

clobber destination workspace (if it exists, and if B<-i> was not specified).

=item (*)

create and/or update virtual workspace if this is a virtual build.

=item (*)

build "install" in non-DEBUG mode
(if B<-F> was not specified).

=item (*)

build "lint" in non-DEBUG mode,
if B<-l> or B<-a> was specified (and B<-F> was not specified).
failure of this step is not fatal.

=item (*)

build packages in non-DEBUG mode
(if B<-F> was not specified).

=item (*)

build CD-ROM/DVD images in non-DEBUG mode, if B<-b> or B<-a> was specified
(and B<-F> was not specified).

=item (*)

build "install" in DEBUG mode (if B<-D> was specified).

=item (*)

build "lint" in DEBUG mode,
if B<-l> or B<-a> was specified (and B<-D> was specified).

=item (*)

build packages in DEBUG mode
(if B<-D> was specified).

=item (*)

build CD-ROM/DVD images in DEBUG mode, if B<-b> or B<-a> was specified
(and B<-D> was specified).

=item (*)

build "check" and "scmsgs" targets, if B<-l> or B<-a> was specified.

=back

=head1
USAGE

B<important:>
running I<nbuild> on a dirty parent (non-virtual) workspace
may result in hard-to-diagnose problems!
the parent workspace must be clean ---
that is, unbuilt, or cleaned with justsccs(1).

I<nbuild> accepts the following command-line flags and arguments.

=over 4

=item (*)

B<-D> - perform a DEBUG build
(in addition to the default non-DEBUG build).

=item (*)

B<-F> - suppress (ie, do NOT perform) the non-DEBUG build.
(by default a non-DEBUG build is performed.)

=item (*)

B<-N> - do a non-virtual build (in the current workspace)
instead of the default virtual build.

=item (*)

B<-T> - test mode.  behavior of this option is unspecified.

=item (*)

B<-a> - do "all" extra steps, including linting and building the CD-ROM/DVD
images.
(specifying B<-a> is equivalent to specifying B<-b> and B<-l> together.)

=item (*)

B<-b> - build CD-ROM/DVD images.
(off by default.)

=item (*)

B<-i> -
do an incremental build instead of the default "clean" build from scratch.
(off by default.)

=item (*)

B<-l> - do linting.
(off by default.)

=item (*)

B<-p> - send packages to the IPS repository.
(off by default.)

=item (*)

B<-t> - build and use tools in usr/src/tools.
(off by default.)

=item (*)

B<-v> - verbose mode.  (nbuild will print a few extra pieces of information
to stdout during the run.)
(off by default.)

=item (*)

VAR=VALUE -
sets the environment variable VAR to VALUE.
some environment variables have a special meaning to
I<nbuild>, or to the sun cluster makefiles,
or to the tools used in the sun cluster makefiles.
see ENVIRONMENT VARIABLES below.

=back

=head1
EXAMPLES

=over 4

=item (*)

nbuild

a plain, non-DEBUG clean build.

=item (*)

nbuild B<-i>

a non-DEBUG incremental build.

=item (*)

nbuild B<-D>

a clean dual build (both non-DEBUG and DEBUG).

=item (*)

nbuild B<-DF>

a clean build of DEBUG only.

=item (*)

nbuild B<-a> OS=5.10

a clean deluxe non-DEBUG build, for solaris 10.

=item (*)

nbuild MAKEFLAGS=k

passes in the variable MAKEFLAGS with the value "k",
which tells dmake to continue building even after encountering errors.

=back

=head1
SEE ALSO

blerf(1), dmake(1), nbmake(1), nbmerge(1)

=head1
FILES

=over 4

=item (*)

/tmp/nbuild.$LOGNAME.* - directory for nbuild temp files

=item (*)

$WS+$OS+$ARCH/log/log.* - directory for nbuild log files
(eg, /workspace/clustbld/quorum+5.9+sparc/log/log.200506271635.01
would be my virtual workspace for the editable workspace "quorum",
building for solaris 9 sparc, for the build started at
2005/06/27 at 4:35 pm.)

=item (*)

$WS+$OS+$ARCH/nbuild-lock - workspace exclusion lock

=item (*)

$PARENT_WS/usr/src/nbuild.rc - this file (if it exists) is sourced in
before command line options get set, to obtain any gate-specific
environment settings.

=back

=head1
ENVIRONMENT VARIABLES

I<nbuild> treats many environment variables specially,
either making use of their inherited value, or setting them
for the benefit of the tools that are called.
some are inheritable directly from the user's environment;
others must be set on the I<nbuild> command line via the
I<VAR=VALUE> mechanism.

in the list below,
environment variables marked with an asterisk are
ones whose values can be directly inherited without
explicitly setting them on the command line.

=over 4

=item (*)

CCTEMPFLAG - set to -temp=$TMPDIR

=item (*)

CODEMGR_WS - set to the virtual workspace by default.

=item (*)

CODEMGR_WS_PREFIX - specifies the prefix of the virtual workspace
to be built.  if CODEMGR_WS_PREFIX is set, then the default location
of the virtual workspace is $CODEMGR_WS_PREFIX+$OS+$MACH.
(CODEMGR_WS_PREFIX has no effect if CODEMGR_WS is also set.)

=item (*)

HOME * - passed thru without change.

=item (*)

IDSTR - string used to create "version string" that identifies the build.
set to the name of the (last component of the)
non-virtual workspace directory by default.

=item (*)

LOGDATE - if present, specifies the date used to create the log directory.

=item (*)

LOGNAME * - passed thru, and used as the default name of the recipient
for mail notifications about the build.

=item (*)

MACH - by default, set to sparc or i386 depending on the natively running OS.

=item (*)

MAKE * - set to dmake by default.

=item (*)

MAKEFLAGS * - flags for make.
the default value is B<S>, which means to stop at the first error.
it can be set to B<k> to keep going after the first error.

=item (*)

MAKETOOLS * - if present, specifies the location of the build tools.

=item (*)

NBMERGEFLAGS - this variable is passed to nbmerge(1) during
creation of the virtual source directory.
NBMERGEFLAGS is normally set to "", which means nbmerge
will exclude files not under SCCS control.
to do otherwise would invite problems relating to building from a
dirty workspace.  however, NBMERGEFLAGS can be set to "C<-u>" to include all
files whether or not they are under SCCS control (see nbmerge(1)).
this feature is provided on a use-at-your-own-risk basis.

=item (*)

OPTHOME * - set to /ws/sc31u-tools or /opt by default.

=item (*)

OS * - set to the version of the natively running OS by default.

=item (*)

PARENT_WS - set to the non-virtual workspace by default.

=item (*)

SOL_VER - I<nbuild> sets this based on the OS.

=item (*)

TEAMWARE * - set to $OPTHOME/teamware by default.

=item (*)

TMPDIR - the directory for compiler tempfiles.
I<nbuild> sets this to a subdirectory of its own temp directory (in /tmp).

=back

=head1
EXIT STATUS

I<nbuild> exits with a status of 1 if any of the outlined steps fails,
with exceptions as noted in the DESCRIPTION; otherwise 0.

=cut

EOF
}

function Build_dual_install {
	task=

	if ! $F_FLAG; then
		# next line is workaround for limitation in Linux ksh
		export RELEASE_BUILD=

		RELEASE_BUILD= \
		Build_combo non-DEBUG \
			|| whyfailed="$whyfailed $task"
	fi
	if $D_FLAG; then
		# next line is workaround for limitation in Linux ksh
		unset RELEASE_BUILD

		Build_combo DEBUG \
			|| whyfailed="$whyfailed $task"
	fi
}

function Build_packages {
	Timed_run "Creating $LABEL packages" \
		$SRC/pkgdefs \
		src_pkgdefs "$DBGSUF" \
		$MAKE -e install
}

function Postproc_src_pkgdefs {
	typeset log=$1
	typeset pkout=$VLOGDIR/pkgdefs-install$DBGSUF.txt

	$CP $log $pkout

	Subheader "Package build errors ($LABEL)"
	egrep -e "$MAKE|ERROR|WARNING" < $pkout \
		| Group_grepv PSTAMP

	# Ignore WARNINGs when checking for build FAILURE.
	# this command is done just to set the correct exit status.
	egrep -e "$MAKE|ERROR" < $pkout | \
		Group_grepv PSTAMP \
		> /dev/null 2>&1
}

function Filter_install_warnings {
	# look for some common errors.
	egrep < $1 -e "(MAXSYMLINKS|No space left on device|Permission denied|No such file or directory)"

	egrep < $1 -i warning: \
		| Group_grepv \
			'^nbuild: ' \
			'^ld: warning: .* section .* has invalid type'
}

function Filter_inst_noise {
	Group_grepv < $1 \
		'^/' \
		'^(Start|Finish|real|user|sys|./bld_awk)' \
		'^nbuild: ' \
		'^Writing' \
		'^echo ' \
		'\.c:$' \
		'\-o (SUNW0sc|SUNW0scds|SUNW0scg)$' \
		'^ld: warning: .* section .* has invalid type' \
		| sort -u
}

function Filter_install_errors {

	# NOTE: warnings are generally not errors, but in this
	# case we consider warnings from dmake to be errors.
	#
	# the following regexp matches either:
	#	dmake: (name of make program followed by colon)
	#	error (the word error delimited by whitespace or colon)
	egrep < $1 -i -e "$MAKE:|[ 	]error([: 	]|\$)" \
		| Group_grepv \
			"Number of error messages: 0" \
			"javadoc: warning - Error "
}

function Build_ips {
	$p_FLAG || return 0;

	Timed_run "Sending $LABEL packages to the IPS repository" \
		$SRC/ipsdefs \
		src_ipsdefs "$DBGSUF" \
		$MAKE -e install
}

function Build_cd_images {
	$b_FLAG || return 0;

	Timed_run "Creating $LABEL cdrom/dvd images" \
		$SRC \
		cdlog "$DBGSUF" \
		build_cds \
		$([ "$LABEL" = "DEBUG" ] && echo "-d") $CODEMGR_WS
}

function Postproc_cdlog {
	[ $rv -eq 0 ] && return 0
	Subheader "CD-ROM/DVD build errors ($LABEL)"
	tail -3 $1
	return 0
}

# Build_combo LABEL -
# LABEL is either DEBUG or non-DEBUG
function Build_combo {
	LABEL=$1

	case "$LABEL" in
	DEBUG) DBGSUF='' ;;
	*)     DBGSUF='-nd' ;;
	esac

	task=Build_install$DBGSUF
	Build_install || return 1
	task=Build_lint_dirs$DBGSUF
	Build_lint_dirs || return 1
	task=Build_packages$DBGSUF
	Build_packages || return 1
	task=Build_ips$DBGSUF
	Build_ips || return 1
	task=Build_cd_images$DBGSUF
	Build_cd_images || return 1

	task=""
	return 0
}

function Build_install {
	Timed_run "Building Sun Cluster source ($LABEL)" \
		$SRC \
		src_install "$DBGSUF" \
		$MAKE -e install
	typeset rv=$?
	typeset pplog=$VLOGDIR/src_install$DBGSUF-postproc.txt
	grep '^==== ' $pplog | Log2Mail
	[ $rv -eq 0 ]
}

function Linecount {
	set -- $(grep '^[^ ]*:' $1 | wc)
	echo $1
}

function Postproc_src_install {
	typeset log=$1
	typeset etmp=$NBTMPDIR/errors
	typeset wtmp=$NBTMPDIR/warnings

	Filter_install_errors $log > $etmp 2>&1
	Filter_install_warnings $log > $wtmp 2>&1

	typeset nerrors=$(Linecount $etmp)
	typeset nwarnings=$(Linecount $wtmp)

	Subheader "Build errors ($LABEL) - $nerrors"
	$CAT $etmp
	Subheader "Build warnings ($LABEL) - $nwarnings"
	$CAT $wtmp

	$i_FLAG || \
		Differences "Build noise differences ($LABEL)" \
			noise$DBGSUF Filter_inst_noise $log

	$CP $log $REFLOG/install$DBGSUF.txt
	return $nerrors
}


function Clean_lint {
	# Remove all .ln files to ensure a full reference file
	$RM -f Nothing_to_remove \
		$(find . -name SCCS -prune -o -type f -name '*.ln' -print)
}

function Lint_dir {
	typeset lintdir=$1

	typeset LINTBASE=$(basename $lintdir)
	Timed_run "Removing old .ln files" \
		$lintdir \
		lint-clean "$DBGSUF" \
		Clean_lint

	Timed_run "$MAKE lint of $LINTBASE" \
		$lintdir \
		src_lint "$DBGSUF" \
		$MAKE -ek lint
}

function Postproc_src_lint {
	typeset log=$1
	typeset LINTOUT=$REFLOG/lint$DBGSUF.txt
	typeset x=$REFLOG/lint-noise$DBGSUF
	typeset oldlnout=$x-ref.txt lnout=$x.txt
	typeset linttmp=$NBTMPDIR/linttmp-$$

	# LINTOUT is the unfiltered lint output
	$CP $log $LINTOUT

	grep "$MAKE:" < $LINTOUT > $linttmp
	if [ -s $linttmp ]; then
		Subheader "$MAKE lint of $LINTBASE ERRORS"
		$CAT $linttmp
	fi

	Filter_lint_noise $LINTOUT > $linttmp
	if [ -s $linttmp ]; then
		Subheader "$MAKE lint of $LINTBASE WARNINGS"
		$CAT $linttmp
	fi

	Differences "lint noise differences $LINTBASE" \
		lint-noise Filter_lint_noise $LINTOUT

	Chk_lint
	return 0
}

function Chk_lint {
	# Create lint reports
	ROOT_PROTO=$ROOT_PROTO$DBGSUF \
	$CHK
	typeset arch=$CODEMGR_WS/$ROOT_PROTO/lint_$MACH
	if [ -f $arch/lint.summary.diffs ]; then
		$CAT $arch/lint.summary.diffs
	elif [ -f $arch/lint.summary ]; then
		$CAT $arch/lint.summary
	fi
}

function Filter_lint_noise {
	Group_grepv < $1 \
		'^(real|user|sys)' \
		'\.c:$' \
		| egrep -i '(warning|lint):' \
		| sort -u
}

function Scmsgs_errors {
	typeset fn=$1
	Subheader "$MAKE scmsgs of $WSBASE ERRORS"
	grep "$MAKE:" $fn
}

function Filter_scmsgs {
	grep : < $1 | grep -v '^nbuild:' | sort -u
}

function Clean_scmsgs {
	# Remove all .scmsgs files to ensure a full reference file
	$RM -f Nothing_to_remove \
		$(find . -name SCCS -prune -o -type f -name '*.scmsgs' -print )
}

function Build_scmsgs {
	typeset dir=$SRC
	typeset grandfile=$SRC/messageid/sc_msgid_grandfathered.txt

	if [ -f "$grandfile" -o ! -d "$SRC/messageid" ]; then
		# the grandfathered file is a list of messages
		# for which we do not require a documented explanation.
		# this file only exists during the transition to the new
		# (automated) scmsgs process.  the messageid directory
		# is deleted after the transition is complete.

		typeset rstat=0

		Timed_run "Removing old scmsgs files" \
			$dir \
			nscmsgs_clean "" \
			$MAKE -e clean_scmsgs

		Timed_run "$MAKE scmsgs of $WSBASE" \
			$dir \
			nscmsgs "" \
			$MAKE -e scmsgs \
		 || rstat=1

		if [ $rstat -ne 0 ]; then
			whyfailed="$whyfailed scmsgs"
		fi

		return $rstat
	fi

	# this is what we do for the old (manual) scmsgs process.
	Timed_run "Removing old .scmsgs files" \
		$dir \
		scmsgs-clean "" \
		Clean_scmsgs

	Timed_run "$MAKE scmsgs of $WSBASE" \
		$dir \
		scmsgs "" \
		$MAKE -ek scmsgs
}

function Postproc_nscmsgs {
	# this is the postprocessing for the new (automated) scmsgs process
	$CP scmsgs-errs $VLOGDIR/scmsgs-errs.txt
	return 0
}

function Postproc_scmsgs {
	# this is the postprocessing for the old (manual) scmsgs process.
	typeset log=$1
	typeset out=$REFLOG/scmsgs.txt

	$CP $log $out
	Scmsgs_errors $out

	Differences "scmsgs noise differences $WSBASE" \
		scmsgs-noise Filter_scmsgs $out

	Chk_scmsgs
	return 0
}

function Chk_scmsgs {
	# this is done for the old (manual) scmsgs process.
	$CHK -t scmsgs
	typeset arch=$CODEMGR_WS/$ROOT_PROTO/scmsgs_$MACH
	if [ -f $arch/scmsgs.summary.diffs ]; then
		$CAT $arch/scmsgs.summary.diffs
	elif [ -f $arch/scmsgs.summary ]; then
		$CAT $arch/scmsgs.summary
	fi
}

function Build_tools {
	$t_FLAG || return 0
	[ -e $TOOLS/Makefile ] || return 0

	typeset destroot=$TOOLS_PROTO

	RELEASE_BUILD= \
	Timed_run "Building tools" \
		$TOOLS \
		tools_install "" \
		$MAKE ROOT=$destroot -e install \
		|| return 1

	# tools build succeeded, so adjust environment
	typeset tp="$destroot/opt/scbld/bin"
	export CTFCONVERT="$tp/$MACH/ctfconvert"
	export CTFMERGE="$tp/$MACH/ctfmerge"
	export PATH="$tp:$tp/$MACH:$PATH"
	export SHMSGPREP="$tp/$MACH/extract_gettext"

	Subheader "New environment settings after building tools"
	echo "CTFCONVERT=${CTFCONVERT}"
	echo "CTFMERGE=${CTFMERGE}"
	echo "PATH=${PATH}"
	echo "SHMSGPREP=${SHMSGPREP}"
}

function Postproc_tools_tools {
	typeset log=$1
	typeset out=$VLOGDIR/tools-install.txt

	$CP $log $out
	Subheader "Tools build errors"
	egrep -e "($MAKE:|[ 	]error[: 	\n])" < $out \
		| Group_grepv warning
}

function Find_pkginfo_dirs {
	typeset d=$1
	[ -d "$d" ] || return
	find $d -name pkginfo.tmpl -print -o -name .del\* -prune \
		| sed -e 's:/pkginfo.tmpl$::' \
		| sort -u
}

function Show_mode_impact {
	[ -z "$whyfailed" ] || return 0

	Header "Impact on file permissions"
	# Get pkginfo files from usr/src/pkgdefs
	pmodes -qvdP $(Find_pkginfo_dirs $SRC/pkgdefs)
}

function Format_time {
	typeset -i et=$1
	((minutes = et / 60))
	echo "${minutes}m"
}

function Report_build_times {
	typeset END_DATE=$(date)

	echo "====" "Build Summary: $BLDSUMMARY"
	echo "====" "Nightly $MAKETYPE build started:  $START_DATE"
	echo "====" "Nightly $MAKETYPE build completed:  $END_DATE"
	echo "====" "Total real build time: $(Format_time $SECONDS)"
}

function Report_corefiles {
	Header "Find core files"
	find $SRC -name core -a -type f -exec file {} \;
	return 0
}

function Build_check {
	MAKEFLAGS= \
	Timed_run "Building check" \
		$SRC \
		src_check "" \
		$MAKE -ek check
}

function Postproc_src_check {
	### straight filter, then save output
	typeset log=$1
	typeset out=$REFLOG/check.txt
	$CP $log $out
	Subheader "cstyle/hdrchk errors"
	egrep -e "\.c:|\.cc:|\.h:" < $out | sort -u
	# check errors are nonfatal, always return 0
	return 0
}

function Build_lint_dirs {
	$l_FLAG || return 0
	Lint_dir $SRC
}

function Make_tools_clobber {
	$t_FLAG || return 0
	[ -e $TOOLS/Makefile ] || return 0

	Timed_run "Make tools clobber" \
		$TOOLS \
		tools_clobber "" \
		$MAKE -ek clobber
	Bg_clobber $TOOLS_PROTO t1
	mkdir -p $TOOLS_PROTO
}

function Postproc_tools_clobber {
	typeset log=$1
	typeset out=$VLOGDIR/tools-clobber.txt

	$CP $log $out

	Subheader "Make tools-clobber ERRORS"
	grep "$MAKE:" $out
	[ $? -ne 0 ]
}

function Make_src_clobber {
	[ -e $SRC/Makefile ] || return 0

	Timed_run "Make src clobber" \
		$SRC \
		src_clobber "" \
		$MAKE -ek clobber

	Proto_clobber
}

# clobber the directory named in the first argument.
# use the second argument as a distinguishing tag
# for the temporary name.
function Bg_clobber {
	typeset xxx="$1" tag="$2"
	typeset xxxdir=$(dirname "$xxx")
	typeset victim="$xxxdir/.nuke-$tag-$$"
	if [ -d "$xxx" ]; then
	$MV "$xxx" "$victim" || $RM -rf "$xxx"
	$RM -rf "$victim" &
	fi
}

function Proto_clobber {
	# remove DEBUG and non-DEBUG proto depending on flags
	$D_FLAG && \
	Bg_clobber "$CODEMGR_WS/$ROOT_PROTO/root_$MACH" p1
	! $F_FLAG && \
	Bg_clobber "$CODEMGR_WS/$ROOT_PROTO-nd/root_$MACH" p2
}

function Postproc_src_clobber {
	typeset log=$1
	typeset out=$VLOGDIR/clobber.txt

	$CP $log $out

	### filter shows elapsed time, then errors.
	### why don't all filters show elapsed time (they should).
	### just record the time at the start of the command.
	### don't emit the header at the start, yet?
	### emit the header including the e.t. at the end.

	Subheader "Make clobber ERRORS"
	grep "$MAKE:" $out
	[ $? -ne 0 ]
}

function Misc_clobber {
	[ -d $SRC ] || return
	# Get back to a clean workspace as much as possible to catch
	# problems that only occur on fresh workspaces.
	# Remove all .make.state* files, libraries, and .o's that may
	# have been omitted from clobber.
	# We should probably blow away temporary directories too.
	Subheader "removing intermediate files"
	(cd $SRC && \
	find . \( -name SCCS -o -name 'interfaces.*' \) -prune \
		-o \( -name '.make.*' \
		-o -name 'lib*.a' -o -name 'lib*.so*' \
		-o -name '*.o' -o -name '.build.*' \) \
	    -exec /bin/rm -f {} \;)
}

function Clobber {
	Misc_clobber
	Make_src_clobber
	Make_tools_clobber
}

function Nbclobber {
	typeset nbclobber=/home/clustbld/bin/nbclobber
	Timed_run "Nbclobber $SRC" \
		$SRC \
		src_nbclobber "" \
		$nbclobber

	Proto_clobber
}

function Nuke {
	typeset ws=$1
	if ! Is_virtws $ws; then
		Warn "workspace $ws is not virtual, so reverting to Clobber"
		Clobber
		return
	fi

	typeset nukedir=$ws/.stalestuff

	# get rid of old nukedir if any
	if [ -d $nukedir ]; then
		Bg_clobber $nukedir n1
	fi

	# move current stuff to new nukedir
	mkdir -p $nukedir
	for d in $(/bin/ls $ws); do
		# note that stalestuff should not show in the
		# ls output because it's a '.' directory
		case $d in
		Codemgr_wsdata|log|nbuild-lock|public) ;;
		wx) ;;
		*) $MV $ws/$d $nukedir
		esac
	done

	# remove nukedir
	Bg_clobber $nukedir n2
}

function Is_virtws {
	typeset d=$1/Codemgr_wsdata
	[ -f "$d/exports.lst" -a -f "$d/profile" ] \
	|| [ -f "$1/exports.lst" -a -f "$1/profile" ]
}

function Symlink_ws {
	typeset ws=$1
	typeset nbmerge=nbmerge

	$nbmerge -u $PARENT_WS/Codemgr_wsdata $ws/Codemgr_wsdata \
			|| return 1
	typeset src
 	[ -d "$PARENT_WS/usr" ] && src=usr
 	[ -d "$PARENT_WS/src" ] && src=src

	NBMERGEFLAGS=${NBMERGEFLAGS:-"-v"}
	[ -f "$PARENT_WS/Codemgr_wsdata/do_not_remove" ] \
			&& NBMERGEFLAGS="$NBMERGEFLAGS -u"
	$nbmerge $NBMERGEFLAGS $PARENT_WS/$src $ws/$src \
			|| return 1

	return 0
}

function Create_virtws {
	Vecho "Creating virtual workspace"

	typeset ws=$1
	if [ -d "$ws/Codemgr_wsdata" ] && ! Is_virtws $ws; then
		Die "error: existing workspace $ws is not virtual -"
	fi

	Timed_run "Updating virtual workspace $ws" \
		$PARENT_WS \
		nbmerge "" \
		Symlink_ws $ws \
		|| Die "error: merge failed, quitting"
}

function Make_tmp {
	Vecho "making dir, NBTMPDIR=$NBTMPDIR"
	mkdir -p $NBTMPDIR || Die "error: tmp dir $NBTMPDIR already existed"
	export TMPDIR=$NBTMPDIR/tmp
	mkdir -p $TMPDIR
	export CCTEMPFLAG=-temp=$TMPDIR
}

function Lock_ws {
	typeset readme=$LOCKDIR/README

	# if necessary, create parent dir needed for locking.
	# ignore errors here since they could be caused by
	# concurrency conditions.
	mkdir -p $(dirname $LOCKDIR) 2>/dev/null

	Vecho "locking ws, LOCKDIR=$LOCKDIR"
	# creating the lock directory should only fail if someone
	# else is already building.
	if ! mkdir $LOCKDIR 2>/dev/null; then
		if [ -d $LOCKDIR ]; then
		Die "error: another build of $WSBASE is already running.\n$($CAT $readme)\nto break the lock, do \"rm -rf $LOCKDIR\""
		else
		Die "error: could not lock $WSBASE for unknown reasons, lock=$LOCKDIR"
		fi
	fi

	# we got the lock
	# stash some helpful identification info here for the benefit
	# of anyone who might want to know who's building.
	LOCKED=true
	echo "build started by user $LOGNAME on host $(hostname) at $(date)" > $readme
}

function Init_logs {
	# create empty workspace if necessary
	[ -d $CODEMGR_WS ] || mkdir -p $CODEMGR_WS

	# make log parent dir
	[ -d $ATLOG ] || mkdir -p $ATLOG

	# make dir for reference logs
	[ -d $REFLOG ] || mkdir -p $REFLOG

	# the redundant quote marks prevent unwanted sccs keyword expansion
	LOGDATE=${LOGDATE:-$(date '+%Y''%m''%d''%H''%M')}

	typeset -Z2 seq
	seq=01

	# create log dir
	typeset B="$ATLOG/log.$LOGDATE"
	VLOGDIR=$B.$seq

	# try to make log dir.  if we fail, advance to
	# next sequence number and try again until we succeed.
	until mkdir $VLOGDIR 2>/dev/null; do
		if [ -z "$seq" ]; then
			# first time thru, try to set seq to
			# highest existing seq
			seq=$(ls -d $VLOGDIR* \
				| tail -1 | sed -ne 's,.*\.\(..\)$,\1,p')
		fi
		(( seq = seq + 1 ))
		VLOGDIR=$B.$seq
		if [ $seq -ge 200 ]; then
			Die "error: can't create any log dir"
		fi
		sleep 2
	done

	Vecho "Creating log dir, VLOGDIR=$VLOGDIR"

	# the log file will be generated under the name $LOG_FN
	# in directory log/log.MMDD
	LOG_FN=$VLOGDIR/log.txt

	# Create mail_msg_file
	mail_msg_file="$VLOGDIR/email.txt"
	touch $mail_msg_file
}


function Show_env {
	Header "NBUILD_ARGS=$NBUILD_ARGS"
	Header "list of environment variables"
	env | sort
}

function Prepare_virtws {
	if ! $i_FLAG; then
		if $NBCLOBBERFLAG && ! $N_FLAG; then
			Nbclobber
		elif $NUKEFLAG && ! $N_FLAG; then
			Nuke $CODEMGR_WS
		else
			Clobber
		fi
	fi

	if ! $N_FLAG; then
		Create_virtws $CODEMGR_WS 2>&1
	else
		Create_nonvirt 2>&1
	fi
}

function Create_nonvirt {
	Timed_run "src bringover" \
		. \
		src_bringover "" \
		Bringover
}

function Bringover {
	# sleep on the parent workspace's lock
	while egrep -s write $PARENT_WS/Codemgr_wsdata/locks
	do
		sleep 120
	done

	$TEAMWARE/bin/bringover -c "nightly update" -p $PARENT_WS \
	    -w $CODEMGR_WS usr < /dev/null
	if [ $? -ne 0 ]
	then
        	echo "trouble with bringover, quitting at $(date)." >> $LOGFILE
		exit 1
	fi

	return 0
}

function Build_env {
	Header "Build script version: @(#)nbuild.ksh	1.44	09/04/07 SMI"
}

function Build_l_targets {
	$l_FLAG || return 0

	Build_check
	Build_scmsgs
	Report_corefiles | Log2Mail
	Show_mode_impact | Log2Mail
}

# print the "universal" path for this workspace.
# try to turn it into something that will be good
# on all platforms.
# (assuming that /net and /home are available on all platforms.)
function Get_universal_path {
	typeset path=$1
	case "$path" in
	\*\**) echo "$path"; return ;;
	/*) ;;
	*) path=$(/bin/pwd)/$path ;;
	esac

	case "$path" in
	/ws/*|/net/*|/home/*|/workspace/*|/project/*) echo "$path" ;;
	*) echo "/net/$(hostname)$path" ;;
	esac
}

function Show_url {
	typeset VIEWER
	if [ -d $2 ]; then
		VIEWER="${WEBBASE:+$WEBBASE/dirview.cgi?}"
	else
		VIEWER="${WEBBASE:+$WEBBASE/txtview.cgi?}"
	fi
	echo "browse $1 at:"
	if [ -n "$VIEWER" ]; then
		echo "$VIEWER$(Get_universal_path $2|sed -e 's/+/%2b/g')"
	else
		echo "$(Get_universal_path $2)"
	fi
}

function Show_blerflink {
	typeset VIEWER="${WEBBASE:+$WEBBASE/blerfview.cgi?}"
	if [ -n "$VIEWER" ]; then
		echo "view blerf summary at:"
		echo "$VIEWER$(Get_universal_path $LOG_FN|sed -e 's/+/%2b/g')"
	else
		echo "view blerf summary by running:"
		echo "\$ $progdir/blerf $(Get_universal_path $LOG_FN)"
	fi
}

function Dummy {
	echo "$@"
	return 0
}

function Send_notification {
	typeset BLDSUMMARY=$1
	typeset MAILER=/usr/bin/mailx
	[ -e $MAILER ] || MAILER=/bin/mail

	if [ "$XSTAT" -ne 0 -a -n "$FAILMAILTO" ]; then
		MAILTO="$MAILTO $FAILMAILTO"
	fi
	$MAILER -s "$BLDSUMMARY Nightly Build of $WSBASE Completed." \
		$MAILTO < $mail_msg_file
}

function Summarize {
	[ -z "$whyfailed" ] && XSTAT=0

	BLDSUMMARY=SUCCESS
	[ -z "$whyfailed" ] || BLDSUMMARY=FAILURE
	whyfailed=${whyfailed:+" because$whyfailed failed"}

	echo "" | Log2Mail
	Report_build_times | Log2Mail
	Header "exit status=$XSTAT$whyfailed" | Log2Mail
	Show_url "logs" $VLOGDIR | Log2Mail
	if [ $XSTAT -ne 0 ]; then
		Show_blerflink | Log2Mail
	fi

	Send_notification $BLDSUMMARY < $mail_msg_file
}

# the output of this routine and everything under it,
# goes to the main log unless otherwise specified.
function Logged_work {
	START_DATE="$(date)"
	SECONDS=0
	whyfailed=

	Header "Nightly $MAKETYPE build of $CODEMGR_WS started on $(hostname) at $START_DATE" | Log2Mail
	echo "VERSION=$VERSION" | Log2Mail

	Show_env
	Prepare_virtws

	Build_env
	Build_tools
	Build_dual_install
	Build_l_targets

	Summarize
}

# ----- misc support routines

# Vecho - write a diagnostic message to stderr
function Vecho {
	$LOGGING_STARTED && echo "$prog:" "$@"
	$v_FLAG || return
	echo 1>&2 "$prog:" "$@"
}

function Warn {
	echo 1>&2 "$prog:" "$@"
}

function Header {
	echo "\n====" "$@" "===="
}

function Subheader {
	echo "\n====" "$@"
}

function Die {
	echo 1>&2 $prog "$@"
	$DYING && exit 3
	export DYING=true
	Cleanup
	# send mail when we die unexpectedly.
# not sure if we want to do this
# 	if [ -n "$NBTMPDIR" -a -d "$NBTMPDIR" ]; then
# 		echo "$@" | Send_notification FAILURE
# 	fi
	exit 2
}

# join arguments as alternatives in a single regular expression
function Joinx {
	perl -e 'print join("|",@ARGV),"\n"' "$@"
}

# Group_grepv REGEXPS
# process stdin, filtering out lines not containing a colon (:),
# and filtering out lines containing any of the given REGEXPS
# return success if NO lines are printed (opposite of normal egrep).
function Group_grepv {
	(grep ':' | egrep -v "$(Joinx $@)") \
		|| return 0
	return 1
}

# return true if the argument is the name of an existing function
function Is_function {
	typeset func=$1
	typeset wh=$(whence -v $func 2>&1 | grep 'is a function')
	[ -n "$wh" ]
}

# Timed_run TITLE DIR IDENT SUF FUNC [ARGS ...]
# this routine combines several common operations related to
# doing a timed run of a build stage.
#
# 1. run a command under time, in a given directory.
#    clone the output to a temp file (it also goes to stdout).
# 2. if there is a postprocessing function for the target,
#    call it on the temp file.  the postprocessing output
#    goes to the mail log only.
# 3. return the combined status of the make and of the postprocessing function.
#
function Timed_run {
	typeset title="$1"; shift
	typeset dir=$1; shift
	typeset jobident=$1; shift
	typeset suf="$1"; shift
	eval typeset target=\$$#
	typeset log=$VLOGDIR/$jobident$suf.txt
	typeset pplog=$VLOGDIR/$jobident$suf-postproc.txt
	typeset func=$1; shift

	typeset xfile=$NBTMPDIR/xstat-$$
	typeset rv
	typeset startsecs=$SECONDS
	Header "$title, started at $(date)" | Log2Mail

	if $T_FLAG; then
		func="Dummy $func"
	fi

	# this output goes to the main log as well as the individual log
	$RM -f $xfile
	typeset vdir=${dir#$CODEMGR_WS/} vlog=${log#$CODEMGR_WS/}
	Vecho "+ (cd $vdir && $func $@) > $vlog" | Log2Mail
	(cd $dir && $func "$@"; echo $? >$xfile) 2>&1 | $TEE $log
	rv=$($CAT $xfile)

	typeset endsecs=$SECONDS
	((et = endsecs - startsecs))
	typeset fet=$(Format_time $et)
	Subheader "$title, ended after $fet"
	Vecho "cmd returned $rv after $fet" | $TEE -a $log | Log2Mail
	Show_url "log" $log | Log2Mail

	# do optional postprocessing.
	# the postprocessing function, if it exists, will be
	# named after the directory and the target.
	typeset pfunc="Postproc_$jobident"
	typeset pprv=0
	if Is_function $pfunc; then
		$RM -f $xfile
		if $T_FLAG; then
			pfunc="Dummy $pfunc"
		fi
		typeset vpplog=${pplog#$CODEMGR_WS/}
		Vecho "+ (cd $vdir && $pfunc $vlog) > $vpplog" | Log2Mail
		(cd $dir && $pfunc $log; echo $? >$xfile) 2>&1 > $pplog
		pprv=$($CAT $xfile)
		Vecho "postproc returned $pprv" | $TEE -a $pplog | Log2Mail
		Show_url "postproc log" $pplog | Log2Mail
	fi
	echo ""|Log2Mail
	[ "$rv" -eq 0  -a  "$pprv" -eq 0 ]
}

# Differences TITLE JOBIDENT FUNC LOG
# this routine encapsulates several common steps taken by
# routines that filter a logfile and then run diff against
# the result of the previous run.
function Differences {
	typeset title="$1" jobident=$2 filter=$3 log=$4
	typeset out=$REFLOG/$jobident.txt oldout=$REFLOG/$jobident-ref.txt
	typeset tmp=$NBTMPDIR/delta-$$

	Subheader "$title"

	# filter the current log to create the new result.
	$filter $log > $tmp

	# if there is no previous result file, fake it
	if [ ! -f $out ]; then
		$CP $tmp $out
	fi
	# previous results become renamed to be the "old" results
	$MV -f $out $oldout

	# the new filtered results become the current results
	$MV $tmp $out

	# diff the current results vs the old.
	diff $oldout $out
}

function Cleanup {
	$LOCKED || return
	Vecho "Cleaning up $LOCKDIR and $NBTMPDIR"
	# remove lock dir and tmp dir
	[ -n "$LOCKDIR" ] && $RM -rf $LOCKDIR
	[ -n "$NBTMPDIR" ] && $RM -rf $NBTMPDIR
}

function Interrupted {
	Cleanup
	exit 2
}

# send input to the pending mail message as well as the regular log
function Log2Mail {
	$TEE -a $mail_msg_file
}

# ----- routines that implement workspace naming conventions
function Get_native_os {
	case "$(uname)" in
	Linux) echo Linux ;;
	*) uname -r ;;
	esac
}

function Get_native_mach {
	case "$(uname -p)" in
	sparc) echo sparc ;;
	*)     echo i386 ;;
	esac
}

# fixup automount reported pathnames to match what the user sees
function am_fix {
	sed -e 's,^/\.automount/\([^/]*\)/root,/net/\1,' \
		-e 's,^[^:/]*:/\.automount/\([^/]*\)/root/,/net/\1/,'
}

function Get_curws {
	typeset wsdir=Codemgr_wsdata
	typeset curdir=${1:-$(/bin/pwd)}
	curdir=$(echo $curdir|am_fix)

	while true; do
		if [ -d "$curdir/$wsdir" ]; then
			echo $curdir
			return 0
		fi
		newdir=$(dirname $curdir)
		if [ "$newdir" = "$curdir" ]; then
			echo '***UNKNOWN***'
			return 1
		fi
		curdir=$newdir
	done
}

function Get_virtws {
	pfile="Codemgr_wsdata/parent"
	curws=$(Get_curws $*)
	if [ $? -ne 0 ]; then
		echo "$curws"
		return 1
	fi

	# get the type of the file
	lspfile=$(ls -l "$curws/$pfile" 2>&1 | sed -ne 's%\(.\).*%\1%p')
	if [ "$lspfile" = l ]; then
		# it's a symlink - we're in the virtual ws already.
		echo "$curws"
		return 0
	fi

	# we're in the non-virtual twin,
	# so generate the virtual twin's name based on the $OS and $MACH.
	echo "$curws" | sed -e 's,\([^/]*\)$,\1'"+$OS+$MACH",
	return 0
}

function Get_sol_ver {
	typeset osver=${1:-$OS}
	osver=${osver:-$(uname -sr)}

	case "$osver" in
	Linux*)
		echo Linux ;;
	SunOS*|[25].[0-9]*)
		echo "$osver" | sed -ne 's,.*[25]\.\([0-9]*\).*,Sol_\1,p' ;;
	*)
		echo unknown ;;
	esac
	return 0
}

# ----- environment-setting routines
#
# Clear most environment variables so that this script
# has positive control of the environment.
# Additional user vars may be specified on the command line.
#
function Clear_env {
	for v in Nothing $(/bin/env | egrep '^[A-Za-z]' | sed -ne 's/=.*//p'); do
		case "$v" in
		MAKE|MAKEFLAGS|MAKETOOLS|HOME|LOGNAME|USER|OS|SPRO|TEAMWARE|OPTHOME|WEBBASE)
			;;
		[TV]_FLAG)
			;;
		*)
			unset $v;;
		esac
	done
}

function Set_common_env {
	export MAKE=${MAKE:-dmake}
	export MAKEFLAGS=${MAKEFLAGS:-S}
	export PARENT_WS=${PARENT_WS:-$(Get_curws)}
	case "$PARENT_WS" in
	\**UNKNOWN*) Die "error: current dir is not within a workspace" ;;
	esac

	# these may depend on user-specified value of OS
	if $N_FLAG; then
		export CODEMGR_WS=${CODEMGR_WS:-$PARENT_WS}
	else
		if [ -n "$CODEMGR_WS_PREFIX" ]; then
			DEFAULT_CODEMGR_WS="$CODEMGR_WS_PREFIX+$OS+$MACH";
		else
			DEFAULT_CODEMGR_WS=$(Get_virtws $PARENT_WS);
		fi
		export CODEMGR_WS=${CODEMGR_WS:-$DEFAULT_CODEMGR_WS}
	fi
	export SOL_VER=${SOL_VER:-$(Get_sol_ver)}

	MAILTO=${LOGNAME:-$USER}
	NBTMPDIR="/tmp/$prog-${LOGNAME:-$USER}-tmp.$$"

	WSBASE=$(basename $CODEMGR_WS)
	# LOCKDIR="/tmp/$prog-lock.$WSBASE-$LOGNAME"

	ATLOG="$CODEMGR_WS/log"
	REFLOG=$ATLOG/ref
	LOCKDIR=$CODEMGR_WS/nbuild-lock

	# OS related settings.
	export ROOT_PROTO=proto/$SOL_VER

	# Reference and archives related settings.  Usually don't change.
	# build environment variables
	export ROOT="$CODEMGR_WS/proto/root_$MACH"
	export SRC="$CODEMGR_WS/usr/src"
	# OK to change IDSTR to your own identifying string
	export IDSTR=${IDSTR:-$(basename $PARENT_WS)}
	export VERSION=${VERSION:-"$IDSTR:$(date '+%Y-''%m-''%d')"}

	export DMAKE_MAX_JOBS=${DMAKE_MAX_JOBS:-4}
	export DMAKE_MODE=${DMAKE_MODE:-parallel}
	export DMAKE_ODIR=$NBTMPDIR

	export ENVLDLIBS1="-L$ROOT/usr/lib -L$ROOT/usr/ccs/lib"
	export ENVCPPFLAGS1="-I$ROOT/usr/include"

	# we export POUND_SIGN to speed up the build process
	# -- prevents evaluation of the Makefile.master definitions.
	export POUND_SIGN="#"

	case "$MAKE-$DMAKE_MODE" in
	*dmake-parallel) MAKETYPE="parallel dmake";;
	*dmake-*)        MAKETYPE="serial dmake";;
	*)               MAKETYPE="special $(basename $MAKE)";;
	esac

	TOOLS=$SRC/tools
	TOOLS_PROTO=$TOOLS/proto
	CHK=${CHK:-chk}
}

function Set_path {
	export PATH="$OPTHOME/scbld/bin:$OPTHOME/scbld/bin/$MACH:$OPTHOME/onbld/bin:$OPTHOME/onbld/bin/$MACH:/usr/ccs/bin:/bin:/usr/bin:$SPRO/bin:$TEAMWARE/bin:/usr/sbin"

	if [ "x$MAKETOOLS" != "x" ]; then
		PATH=$MAKETOOLS:$PATH
	fi
}

function Check_env {
	Vecho "Checking environment..."
	Vecho "PARENT_WS=$PARENT_WS"
	Vecho "CODEMGR_WS=$CODEMGR_WS"
	Vecho "SOL_VER=$SOL_VER"
	case "$PARENT_WS" in
	\**UNKNOWN*) Die "error: current dir is not within a workspace" ;;
	esac
	! $N_FLAG && Is_virtws $PARENT_WS \
		&& Die "error: current dir is within a virtual workspace"
	[ -d "$PARENT_WS" ] \
		|| Die "error: parent workspace does not exist"
}

function Set_before_env {
	Vecho "Initializing environment"
	Clear_env
	Set_path

	export LC_COLLATE=C \
		LC_CTYPE=C \
		LC_MESSAGES=C \
		LC_MONETARY=C \
		LC_NUMERIC=C \
		LC_TIME=C

	export OS=${OS:-$(Get_native_os)}
	export MACH=${MACH:-$(Get_native_mach)}
	export OPTHOME=${OPTHOME:-/ws/sc31u-tools}
	[ -d $OPTHOME/onbld/bin ] || OPTHOME=/opt
	export SPRO=${SPRO:-$OPTHOME/SUNWspro}
	export TEAMWARE=${TEAMWARE:-$OPTHOME/teamware}

	Set_path
}

function Handle_flag {
	typeset f=$1
	case "$f" in
	[$allflags]) eval ${f}_FLAG=true;;
	*) Usage;;
	esac
}

function Set_cmd_line_options {
	# default values for low-level FLAGS
	for f in $(echo $allflags|sed -e 's/./& /g'); do
		eval ${f}_FLAG=false
	done

	# examine arguments
	OPTIND=1
	while getopts $allflags FLAG; do
		Handle_flag $FLAG
	done

	# correct argument count after options
	shift $(expr $OPTIND - 1)

	if $a_FLAG; then
		# -a = -b -l
		b_FLAG=true
		l_FLAG=true
	fi

	# put any remaining assignments into environment
	if [ "$1x" != x ]; then
		export "$@"
	fi
}

# Set_env [FLAGS] [VAR=VALUE ...]
function Set_env {
	Set_before_env
	# source in nbuild.rc from workspace, if any.
	# options set there will be treated like cmd line VAR=VALUE options.
	[ -e $PARENT_WS/usr/src/$prog.rc ] \
		&& . $PARENT_WS/usr/src/$prog.rc
	Set_cmd_line_options "$@"
	Set_after_env
}

function Set_after_env {
	Set_common_env
	# set PATH again in case some variables changed.
	Set_path
	Check_env
}

function Main {
	Lock_ws
	trap Interrupted 1 2 3 9 15
	Make_tmp
	Init_logs

	LOGGING_STARTED=true
	Logged_work > $LOG_FN
	LOGGING_STARTED=false
}

# ----- mainline code
NBUILD_ARGS="$*"
prog=${0##*/}
progdir=$(cd $(/bin/dirname $0) && /bin/pwd)
allflags=DFNTabilptv
v_FLAG=false
XSTAT=1
LOGGING_STARTED=false
LOCKED=false
DYING=false
NUKEFLAG=false
NBCLOBBERFLAG=false
WEBSERVER="galileo.sfbay.sun.com"
WEBBASE=$(/usr/sbin/ping $WEBSERVER 10 >/dev/null 2>&1 && echo "http://$WEBSERVER/gk/builds/web")
CAT=/bin/cat
CP=/bin/cp
MV=/bin/mv
RM=/bin/rm
TEE=/bin/tee

typeset +x CAT MV RM CP TEE

Set_env "$@"
Main
Cleanup
exit $XSTAT



# todo: fix so that mail is sent even if die prematurely
