#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_fill.pl	1.7	08/05/20 SMI"
#
#	scmsgs_fill [-l=LLEN] [-e] < COMMENT > NEWCOMMENT
# reformat a comment to the given line length.
# the level of indentation and style of the comment is
# taken from the first line of the input.

=head1
NAME

scmsgs_fill - indent and fill an SCMSGS comment

=head1
SYNOPSIS

scmsgs_fill

=head1
DESCRIPTION

scmsgs_fill is intended to assist in the creation of well-formatted
SCMSGS comments.  It works with .c, .cc, and shell files.
The input should be a series of lines constituting an SCMSGS
comment (in the style of .c, .cc, or shell).  The output will be
a properly formatted comment, whose indentation will be the
same as the first line of the input.  The point is that the input
can be badly indented, and scmsgs_fill takes care of the indentation
and formatting.

It is intended to be called as an editor filter.  Eg,
if there is an SCMSGS comment from lines 123 to 150,
it might be called as:

:123,150! scmsgs_fill

which would replace lines 123-150 with a well-formatted and -indented
SCMSGS comment.

In emacs, you could do the equivalent by marking the region from
lines 123-150, then doing:

C-u M-| scmsgs_fill

=head1
EXAMPLE

:123,150! scmsgs_fill

(from within vi)

=cut

BEGIN {
	$PROGDIR = "." unless (($PROGDIR) = ($0 =~ m,^(.*)/,));
	unshift @INC, $PROGDIR;
}

use SCMSGS;

# ---------- start of subroutines


#
# remove_intitial_fluff($s)
#	$s - the leading fluff which may or may not be present on each line.
# @LINES is global array of comment data.
sub remove_initial_fluff
{
	my $s = shift;
	foreach (@LINES) {
		s,^\s*(\Q$s\E\s*)?,,;
	}

	for ($i = 0; $i < @LINES && $LINES[$i] eq ''; $i++) { }

	# delete initial SCMSGS word
	$LINES[$i] =~ s/^\s*SCMSGS\s*//;
}

sub reformat
{
	my ($initial, $mid, $final) = @_;
	my @output;
	# width of comment line, exclusive of indent and comment fluff.
	my $width = $opt_l - &indentof($IWS) - length($mid);
	my %TAGS;

	# identify tags and their values within the comment.
	my ($k, $v);
	my $i = 0;
	foreach (@LINES) {
		$i++;
		next if $_ eq '';
		if (m{^\@(\S+)\s*(.*)}) {
			$k = $1;
			if (defined $TAGS{$k}) {
				print STDERR<<EOF;
$ARGV:$i multiply defined tag '$k'
EOF
			}
			$TAGS{$k} = $2;
			next;
		}
		if (!$LEGAL_TAGS{$k}) {
			print STDERR <<EOF;
$ARGV:$i unsupported tag '$k'
EOF
		}
		$TAGS{$k} .= ' ' . $_;
	}

	foreach $k (@LEGAL_TAGS) {
		if (!defined $TAGS{$k}) {
			print STDERR<<EOF;
$ARGV:$i missing tag '$k'
EOF
		}
	}

	# spit out the comment.
	if ($initial ne '') { print $IWS.$initial."\n"; }
	print $IWS.$mid."SCMSGS"."\n";
	foreach $k (sort keys %TAGS) {
# print STDERR "--> tag= $TAGS{$k}\n";
		print $IWS.$mid."@".$k."\n";
		my $text = $TAGS{$k};
		$text = &SCMSGS::escapetext($text)    if $opt_e;
		foreach $s (&fill_tag($text, $width)) {
			print $IWS.$mid.$s."\n";
		}
	}
	if ($final ne '') { print $IWS.$final."\n"; }
}

sub fill_tag
{
	my ($s, $width) = @_;
	$s =~ s/^\s*//;
	my @r = ();
	my $line;
	my @s = split /\s+/, $s;

	while (@s > 0) {
		$line = shift @s;
		while (@s > 0 && length($line) + 1 + length($s[0]) <= $width) {
			$line .= " ".(shift @s);
		}
		push @r, $line;
	}
	return @r;
}

sub common_comment
{
	my ($initial, $mid, $final) = @_;
	my $ifluff = $mid;
	$ifluff =~ s/\s*//g;
	&remove_initial_fluff($ifluff);
	&reformat($initial, $mid, $final);
}

sub c_comment
{
	# remove initial and final comment delimiters.
	$LINES[0] =~ s,^\s*/\*\s*,,;
	$LINES[-1] =~ s,^\s*\*\/,,;

	&common_comment("/*", " * ", " */");
}

sub cpp_comment
{
	&common_comment("//", "// ", "//");
}

sub shell_comment
{
	# remove comment fluff.
	&common_comment("", "# ", "");
}

sub indentof
{
	my $s = shift;
	my $r = 0;

	foreach (split //, $s) {
		if ($_ eq "\t") { $r += 8 - $r%8; }
		else { $r++; }
	}

	return $r;
}

sub main
{
	@LINES = <>;

	chomp @LINES;

	($IWS, $ITOKEN) = ($LINES[0] =~ m{^(\s*)(/\*|//|#)});

	if ($ITOKEN eq '/*')
		{ &c_comment; }
	elsif ($ITOKEN eq '//')
		{ &cpp_comment; }
	elsif ($ITOKEN eq '#')
		{ &shell_comment; }
	else {
		print STDERR <<EOF;
$PROG: unrecognized comment type \"$ITOKEN\" - allowed comment types are
/* C */
// C++
# shell
EOF
	}
}


# ---------- start of mainline code
($PROG) = ($0 =~ m{([^/]*)$});
$opt_l = $l || 78;
$opt_e = $e;

@LEGAL_TAGS = qw(explanation user_action);
@LEGAL_TAGS{@LEGAL_TAGS} = (1) x @LEGAL_TAGS;

&main;
