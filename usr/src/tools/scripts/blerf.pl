#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)blerf.pl	1.16	08/05/20 SMI"
#
#	blerf [-w=N] [log.txt]
# locate errors in a (potentially huge) nightly build output.
#

=head1
NAME

blerf - build log error finder

=head1
SYNOPSYS

blerf B<[-w=N]> < log.txt

=head1
DESCRIPTION

I<blerf> scans a build log (such as produced by
I<nbuild>(1), or the older script I<nightly>(1),
or plain I<make>(1) or I<dmake>(1),
and extracts the most useful information about
build errors (un-ignored "Error code"
messages as reported by I<make>(1) or I<dmake>(1))
in the log.

I<blerf> also reports other types of build errors not
produced by I<make> or I<dmake>, if the build log was
produced by I<nbuild> (as opposed to the older script I<nightly>).

I<blerf> reports only the first error found, if the log
is a plain I<make> or I<dmake> log; or the first
error (if any) in each section, if the log is a multi-section
log such as produced by I<nightly> or I<nbuild>.

the output includes the following information for each reported error:

=over 4

=item (*)

the line number of the "Error code" message in the log.

=item (*)

the current section header, if this is a multi-section log.

=item (*)

a list of directories recently entered by I<make> or I<dmake>.

=item (*)

about 20 lines of context before the "Error code", or more.
I<blerf> tries to recognize what might constitute a
"command" in the output, and will expand the window if
the preceding 20 lines do not contain what looks like a command.

=item (*)

the directory/target stack of I<make> or I<dmake>.

=back

=head1
USAGE

the optional B<-w=N> argument specifies that a minimum window of N
lines will be printed before the "Error code" line.
the default N is 20.

=head1
EXIT STATUS

I<blerf> always exits with a status of 0.

=cut

sub main
{
	$LAST_HEADER='unknown';
	$BEFORE_DISPLAY_WINDOW = ($w || 20) + 1;
	$MAX_BEFORE_WINDOW=2*$BEFORE_DISPLAY_WINDOW+10;
	if ($MAX_BEFORE_WINDOW < 50) { $MAX_BEFORE_WINDOW = 50; }
	$DIR_WINDOW=4;

	$LINENO = 0;
	@DIRLIST = ();
	$NERR = 0;

	while (<>) {
		$LINENO++;

		# maintain up to $MAX_BEFORE_WINDOW lines of context
		# in @BEFORE, to be displayed when we find an error.
		push @BEFORE, $_;
		shift @BEFORE    if (@BEFORE > $MAX_BEFORE_WINDOW);

		if (m/^==== /) {
			next if (m/, ended after/);
			# section header as printed by nbuild or nightly.ksh
			chomp;
			&do_section_header;
			next;
		}

		if (m,^/\S*$, && m,.*/usr/src/(.*),) {
			# a line consisting of just one word,
			# starting with a slash.
			# and containing .../usr/src/... .
			# lines of this form are presumed to be the output of
			# "pwd" when the build enters a directory.
			&do_direntry($1);
			next;
		}

		if (m/^\*+ (Error) code (\d+)$/
		 || m/^d*make: (warning|.*error)/i) {
			&do_make_error($_);
			$LINENO--;
			# redo because the current value of $_ after
			# do_make_error is a line we must process in this loop.
			redo;
		}

		if (m/^nbuild: cmd returned (\d+)/ && $1 != 0) {
			&do_build_error($1);
			$LINENO--;
			redo;
		}
	}

	print "($NERR errors found)\n";
	exit 0;
}

sub do_section_header
{
	@DIRLIST = ();
	@BEFORE = ();
	$LAST_HEADER = $_;
}

sub context_size
{
	# the @BEFORE array contains up to $MAX_BEFORE_WINDOW lines of the
	# build output that came before the current (error) line.
	# we would like to display only $BEFORE_DISPLAY_WINDOW lines, to
	# reduce the amount of noise.  but it's useful for the display to
	# contain the echo of the last "command" that was run before the
	# error, and even a few lines before that.  so we scan the $BEFORE
	# array looking for "commands".
	my $lastgood = 0;

	for (my $i = 0; $i < @BEFORE; $i++) {
		if ($BEFORE[$i] =~ m,^[/=\w].*[^.\s]\s*$,) {
			$lastgood = $i-5;
		}
	}

	my $nshift = @BEFORE-$BEFORE_DISPLAY_WINDOW;
	if ($nshift > $lastgood) {
		$nshift = $lastgood;
	}

	return @BEFORE-$nshift;
}

sub do_make_error
{
	my %POSTERRORDIRS = ();
	$NERR++;

	print "Error found at line $LINENO, section header:\n",
		$LAST_HEADER, "\n\n";

	print "Recently visited dirs:\n",
		join("\n", @DIRLIST), "\n\n";

	&printbefore(&context_size());

	print "\n\nTarget stack:\n";

	my $errcmd;

	# the caller gives us the current input line as an argument,
	# and we want to process it in the first iteration of the
	# while-loop below.
	$_ = $_[0];

	while (1) {
		if (m/^d*make: (warning|.*error)/i) {
# dmake: Warning: Target `install' not remade because of errors
# dmake: Warning: Don't know how to make target `getCVMMaster_SRDF'
# dmake: Warning: Command failed for target `datarep'
			s/don't//i;
			m/['`](\S+)['`]/i;
			$target = $1;
# print STDERR "* _=$_";
# print STDERR "* target=$target\n";
		}
		if (m/^The following command caused the error:/) {
			$errcmd = <>;
			$LINENO++;
			chomp $errcmd;
			$errcmd = " => $errcmd";
		}
		if (m/^current working dir.* (\S+)$/i) {
			$dir = $1;
			next   if ($testing
				&& ($POSTERRORDIRS{$dir}
				    || $POSTERRORDIRS{"$dir/$target"}));
			$dir =~ s,.*/usr/src/,,
				|| $dir =~ s,.*/usr/src$,\.,;
			print "$dir [$target$errcmd]\n";
			$errcmd = "";
		}
		if (m{^/\S*$}) {
			$POSTERRORDIRS{$&} = 1;
		}
		if (m/^==== /) {
			print "\n\n\n\n";
			# found a section header or subheader.
			# if it's just the ending subheader of
			# this section, just eat it.
			next if (m/, ended after/);
			# the caller will redo on $_.
			return;
		}
	}
	continue {
		# read next line
		$_ = <>;
		last if !$_;
		$LINENO++;
	}

	# EOF, so just quit.
	exit 0;
}

sub printbefore
{
	my $windowsize = shift;

	my $nshift = @BEFORE-$windowsize;

	for (my $i = $nshift; --$i >= 0;) {
		shift @BEFORE;
	}

	print "Context:\n";
	my $l = $LINENO-@BEFORE;
	while (@BEFORE > 0) {
		$l++;
		printf("%6d  %s", $l, shift @BEFORE);
	}
}

sub do_build_error
{
	$NERR++;
	print "Error found at line $LINENO, section header:\n";
	print $LAST_HEADER, "\n\n";

	&printbefore($BEFORE_DISPLAY_WINDOW);

	# skip to end of this section
	while (<>) {
		$LINENO++;
		if (m/^==== /) {
			# another section.
			print "\n\n\n\n";
			return;
		}
	}

	exit 0;
}

sub do_direntry
{
	push @DIRLIST, $_[0];
	shift @DIRLIST    if (@DIRLIST > $DIR_WINDOW);
}

$testing = 1;
&main;
