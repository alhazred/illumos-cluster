#! /usr/bin/perl
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.	 All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)nbmerge.pl	1.8	08/05/20 SMI"
#
#	nbmerge [-OPTIONS] SRCDIR DSTDIR

sub usage
{
	die(
qq{usage: nbmerge [-$OPTIONS] SRCDIR DSTDIR
	-A - use absolute symlinks (opposite of -r)
	-D - don't create dummy mkmerge export and profile files
	-d - delete before linking (should not be needed)
	-h - emit help message
	-l - use hard links instead of symlinks
	-n - don't execute, just print what it would do
	-r - use relative symlinks (opposite of -A)
	-U - unconditional
	-u - handle un-sccs-ed files also
	-v - verbose - report actions
	-V - extra verbose - report non-actions\n});
}

=head1
NAME

nbmerge - create virtual workspace for nbuild

=head1
SYNOPSIS

nbmerge [-OPTION ...] SRCDIR DSTDIR

=head1
DESCRIPTION

the purpose of I<nbmerge> is to create the so-called "virtual workspaces"
used by the nightly build script nbuild(1).
nbmerge is so named because it is similar in function to
the mkmerge(1) tool.
SRCDIR is assumed to be a subdir of an editable workspace,
and DSTDIR the corresponding subdir within a virtual workspace.

nbmerge traverses the directory SRCDIR, and uses symlinks
to make directory DSTDIR a clone of SRCDIR.
in the process, nbmerge creates all the requisite
subdirectories in DSTDIR.
(the parent directory of DSTDIR must already exist,
although DSTDIR itself can be present or not.)

it is assumed that SRCDIR is a teamware workspace or
part of one, under SCCS control.
therefore, by default nbmerge does not clone the SCCS
directories under SRCDIR, and ignores any files that
are not under SCCS control.
in contrast, the justsccs tool removes files not under
SCCS control.
the intent is the same: to create a clean workspace.

to clone all files including non-SCCS-ed files,
use the C<-u> option.  note that the C<-u> option still
excludes SCCS directories proper.
to clone the SCCS directories, use the C<-U> option.

=head1
USAGE

I<nbmerge> accepts the following flags.
several of the flags are analogous to ones in mkmerge.
(each flag must be specified as a separate argument.)

=over 4

=item (*)

C<-A> - absolute - use
absolute symbolic links (the default) instead of relative.

=item (*)

C<-D> - dummy - by default, nbmerge creates dummy files
"exports.lst" and "profile",
for compatibility with mkmerge.  the C<-D> option suppresses the
creation of these files.

=item (*)

C<-d> - delete - delete target files before trying to link.

=item (*)

C<-h> - help - print usage message.

=item (*)

C<-l> - hard link - use hard links instead of symlinks.

=item (*)

C<-n> - do not actually create any files or directories, just print the
creations that would otherwise be done.

=item (*)

C<-r> - relative - use relative symbolic links instead of absolute.

=item (*)

C<-U> - completely unconditional - take all files and directories, including
SCCS subdirs and files not under SCCS control.  use the C<-U> option
if you want to clone the whole source tree.  (note: nbmerge
never takes .snapshot dirs.)

=item (*)

C<-u> - uncontrolled - take all files and directories, including
files not under SCCS control, but excluding SCCS directories.

=item (*)

C<-v> - verbose - print directory-level actions.

=item (*)

C<-V> - more verbose - print file- and directory-level actions.

=back

=head1
EXAMPLE

cd /workspace/clustbld;
nbmerge myworksp/usr myworksp+5.9+sparc/usr

in this example, nbmerge populates the "usr" subdir of (virtual)
workspace "myworksp+5.9+sparc", with absolute symlinks into the workspace
"myworksp".  the directory "myworksp+5.9+sparc" must have already been in
existence.  the subdir "usr" may or may not have existed beforehand.

=head1
SEE ALSO

justsccs(1), mkmerge(1), nbuild(1), sccs(1)

=head1
EXIT STATUS

nbmerge exits with status 0 if all operations succeeded.

=cut

use File::Find;
use Getopt::Std;

($PROGNAME) = ($0 =~ m{([^/]*)$});
$OPTIONS = "ADdhlnrUuVv";

sub abspath
{
	my $d = shift;

	my $pwd = `cd $d; pwd`;
	my $x = $?>>8;
	if ($x != 0) { die("can't get absolute path of \"$d\"\n"); }
	chomp $pwd;

	$pwd =~ s#//*#/#g;
	$pwd =~ s#/$##  if $pwd ne '/';
	return $pwd;
}

sub find_common_ancestor
{
	my ($wsrc, $wdst) = @_;

	# now $wsrc and $wdst are both absolute.
	# we want to find the maximal common ancestor dir if any.
	my $common = '';
	while ($wsrc ne '' && $wdst ne '') {
		my ($isrc) = ($wsrc =~ m{^(/[^/]*)});
		my ($idst) = ($wdst =~ m{^(/[^/]*)});
		last if $idst eq '' || $isrc ne $idst;
		$common .= $idst;
		$wsrc = substr($wsrc, length $idst);
		$wdst = substr($wdst, length $idst);
	}

	if ($wsrc =~ m{^/} && $wdst =~ m{^/}) {
		$common .= '/';
		$wsrc = substr($wsrc, 1);
		$wdst = substr($wdst, 1);
	}

	# factor the common part out of the actual argument strings.
	$_[0] = $wsrc;
	$_[1] = $wdst;
	return $common;
}

sub crosspath
{
	my $srcrel = shift;

	my $dots = "$srcrel/";
	$dots =~ s#[^/]*/$##;	# toss final component
	$dots =~ s#[^/]+#..#g;	# replace remaining components by ..

print STDERR "crosspath($srcrel) => $dots$DST_to_SRC/$srcrel\n";
	return "$dots$DST_to_SRC/$srcrel";
}

sub wanted
{
	my @x = stat($_);
	if (-d _) {
		# normally we don't make the target directory here,
		# because it might turn out that the source
		# directory doesn't contain anything worthwhile.
		# instead, target directories will be lazily,
		# when we find we have to create an actual link.
		#
		# but if -U was specified, we can make the dir now.
		if ($_ eq '.snapshot') {
			# never take .snapshot dirs
			$File::Find::prune = 1;
			return;
		}

		if ($opt_U) {
			&domkdir("-p", $File::Find::name);
			return;
		}

		if ($_ eq 'SCCS' || /^\../) {
print STDERR "(skipping $_/)\n"   if $opt_v > 1;
			$File::Find::prune = 1;
			return;
		}
		return;
	}

	# a non-directory file.
	# if it's not under SCCS control, or if it's a .-file
	# skip it unless user asked for it.
	if ((!$opt_U && /^\../)
	 || (!$opt_u && !-r "SCCS/s.$_")) {
print STDERR "(skipping $File::Find::name)\n"    if $opt_v > 1;
		return;
	}

	# make a link to it.
	my $srcrel = substr($File::Find::name, $BASELEN);
	if ($opt_A) {
		&dolink($File::Find::name, "$ABSDST/$srcrel");
	}
	else {
		&dolink(&crosspath($srcrel), "$ABSDST/$srcrel");
	}
}

sub dolink
{
	$NFILES++;
	# check that the parent dir is there, and if not, make it.
	my ($dirname) = ($_[1] =~ m{(.*)/});
	if (!-d $dirname) { &domkdir("-p", $dirname); }

print STDERR "+ symlink @_\n"   if $opt_v > 1;
	if ($opt_n) { return 0; }

	# if the user specified -d, the assumption is we're refreshing
	# an already-populated DESTDIR, and we unlink pre-emptively
	# before doing the new link.
	if ($opt_d) {
		&dounlink($_[1]);
	}
	# if the user did not specify -d, the assumption is we're
	# dealing with a previously-empty DESTDIR.  however, we could
	# be wrong about that.  if try to be adaptive.  at first, we
	# are optimistic, and naively assume the link or symlink will
	# work without removing the destination beforehand.
	# if we get a lot of link failures under this policy, then
	# we switch to doing unlinks first.  if that produces even
	# more failures, we switch back to optimistic mode.
	#
	# at least, that's the theory.
	elsif (!$OPTIMISTIC) {
		&dounlink($_[1]);
		$OPTIMISTIC = 1 if $UNLINKFAIL > $LINKFAIL+25;
	}

	# note that link and symlink return 1 on success, 0 on failure.
	my $r = $opt_l ? link($_[0], $_[1]) : symlink($_[0], $_[1]);
	$NLINK++;
	return 0 if $r;
	$NLINKFAIL++;

	$LINKFAIL++;
	$OPTIMISTIC = 0 if $LINKFAIL > $UNLINKFAIL+25;

	# an error occurred.  remove the dest file and try again.
	&dounlink($_[1]);
	$r = $opt_l ? link($_[0], $_[1]) : symlink($_[0], $_[1]);
	$NLINK++;
	return 0 if $r;
	$NLINKFAIL++;

	# unlinking didn't help, so this failure doesn't count.
	$LINKFAIL--;

	# give up.
	warn("$PROGNAME: $LINK @_ failed: $!\n");
	$NFAIL++;
	return -1;
}

sub dounlink
{
	$NUNLINK++;
	return 0 if unlink($_[0]);
	$NUNLINKFAIL++;
	return 1;
}

sub domkdir
{
	$NMKDIR++;
print STDERR "+ mkdir @_\n"   if $opt_v;
	if ($opt_n) { return 0; }

	# note, this is deliberately a "rm" not "rmdir" or "rm -rf".
	# the intention is to remove an errant file if there happens
	# to be one with the name we want for our directory.
	# if a directory already exists with this name, we don't care.
	if ($opt_d) { system("/bin/rm", "-f", @_); }

	system("/bin/mkdir", @_);
	my $x = $?>>8;
	if ($x != 0) {
		$NMKDIRFAIL++;
		return -1;
	}
	return 0;
}

sub dofind
{
	my $src = shift;
	$BASELEN = (length $src) + 1;
	print STDERR "(entering $src)\n"   if $opt_v;
	&File::Find::find(\&wanted, $src);
	print STDERR "(leaving $src)\n"   if $opt_v;
}

sub main
{
	$ARGERR = 0;
	if (!getopts($OPTIONS)) { $ARGERR++; }

	if ($opt_h) { $ARGERR++; }

	if (@ARGV < 2) {
		print STDERR "$PROGNAME: insufficient arguments\n";
		$ARGERR++;
	}

	if (@ARGV > 2) {
		print STDERR "$PROGNAME: excess arguments\n";
		$ARGERR++;
	}

	if ($ARGERR) { &usage; }

	if ($opt_U) { $opt_u = 1; }
	$opt_A = !$opt_r;
	if ($opt_n) { $opt_v = 1; }
	if ($opt_V) { $opt_v = 2; }
	if ($opt_l) {
		$opt_A = 1;
		$LINK = "link";
	}
	else {
		$LINK = "symlink";
	}

	$SRCDIR = shift @ARGV;
	$DSTDIR = shift @ARGV;

	if (!-d $DSTDIR) {
		if (&domkdir($DSTDIR) < 0)
			{ die("$PROG: fatal error - can't create $DSTDIR\n"); }
	}
	if (! $opt_D) {
		# create dummies of profile and exports.lst, like mkmerge
		print STDERR "(creating $DSTDIR/{exports.lst,profile})\n"
				if $opt_v > 1;
		system("cp /dev/null $DSTDIR/exports.lst && cp /dev/null $DSTDIR/profile");
	}

	$ABSSRC = &abspath($SRCDIR);
	$ABSDST = &abspath($DSTDIR);

	$COMMONSRC = $ABSSRC;
	$COMMONDST = $ABSDST;

	$COMMON = &find_common_ancestor($COMMONSRC, $COMMONDST);
print STDERR "(common=$COMMON from=$COMMONSRC to=$COMMONDST)\n"   if $opt_v;

	if ($COMMONSRC eq '') {
		die("$PROGNAME: error, DSTDIR cannot be a subdir of SRCDIR\n");
	}
	if ($COMMONDST eq '') {
		die("$PROGNAME: error, SRCDIR cannot be a subdir of DSTDR\n");
	}

	if (!$opt_A) {
		my $DST_to_COMMON = $COMMONDST;
		$DST_to_COMMON =~ s#[^/]+#..#g;
		$DST_to_SRC = "$DST_to_COMMON/$COMMONSRC";
# print STDERR "DST_to_SRC=$DST_to_SRC";
	}

	$OPTIMISTIC = 1;
	$NFAIL = $NFILES = 0;
	$NMKDIRFAIL = $NMKDIR = 0;
	$NLINKFAIL = $NLINK = 0;
	$NUNLINKFAIL = $NUNLINK = 0;

	&dofind($ABSSRC);

	print STDERR "(",
		"failcounts: FILES=$NFAIL/$NFILES",
		" DIRS=$NMKDIRFAIL/$NMKDIR",
		" links=$NLINKFAIL/$NLINK",
		" unlinks=$NUNLINKFAIL/$NUNLINK",
		")\n"
					    if $opt_v || $NFAIL > 0;

	exit ($NFAIL>0);
}


&main;

