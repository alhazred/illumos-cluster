#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_getfiles.pl	1.3	08/05/20 SMI"
#
#	scmsgs_getfiles [-LS] SUBDIRS ...
#		-L	take only files that are symlinks (for virt ws)
#		-S	take only files that are under SCCS control (non-virt)
# print a list of files (within the given SUBDIRS) that could
# contain syslog messages.

use File::Find;

sub visit
{
	# ignore SCCS directories and deleted directories.
	if (/^(SCCS|\.del.*)/) {
		$File::Find::prune = 1;
		return;
	}

	# ignore README's, comma-files, makefiles, tidfiles
	return if (m/^,.*|^README|^Make.*|_tids$/ || -d $_);

	my ($ext) = (m/\.([^.]*)$/);

	# accept the file if it's a .c or a .cc;
	# or if it could be a shell file.
	if ($ext eq ""
	 || $ext =~ m/^(cc?|.*sh)$/) {
		return if ($opt_L && !(-l $_));
		return if ($opt_S && !(-r "SCCS/s.$_"));
		print $File::Find::name, "\n";
		$COUNT++;
	}
}

$opt_S = $S;
$opt_L = $L;
$opt_v = $v;

$COUNT = 0;
&find(\&visit, @ARGV);
print STDERR "### nfiles=$COUNT\n"    if $opt_v;
exit 0;
