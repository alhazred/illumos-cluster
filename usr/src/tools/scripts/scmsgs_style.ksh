#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_style.ksh	1.8	08/05/20 SMI"
#
#	scmsgs_style [OPTIONS]
#	scmsgs_style [OPTIONS] FILES
#		-v			verbose
#		-A ANTIDICT		specify antidictionary
#		-D DICT			specify dictionary
#
# run scmsgs style checks on source files.
# if no FILES are specified, do the current directory

Ignore_pod() {
	cat << EOF

=head1
NAME

scmsgs_style - check source files for style errors in syslog messages

=head1
SYNOPSIS

scmsgs_style [B<-v>] [B<-A> ANTIDICT] [B<-D> DICT] [FILES]

=head1
DESCRIPTION

scmsgs_style checks for style errors in syslog messages.
It flags all the "common errors in syslog messages"
( https://n1wiki.sfbay.sun.com/bin/view/BE/CommonMistakesInSyslogMessages ),
and most of the errors identified in the oft-ignored
"error message writing checklist"
( http://galileo.sfbay.sun.com/messageid-writing-checklist.html ).

Notably, the Explanation and User Action fields are spell-checked
using ispell(1).

If no FILE arguments are given,
scmsgs_style will operate on all .c, .cc, and shell files
under the current directory.

The optional B<-v> flag enables verbose output, including a
summary listing of all misspelled words found in all entries.

The optional B<-D> flag specifies a supplementary dictionary in place of the
standard one.  Words found in the supplementary dictionary are assumed
correct and are not sent to ispell(1) for spell-checking.

The optional B<-A> flag specifies an anti-dictionary in place of the
standard one.  Words found in the anti-dictionary are assumed wrong,
and will garner a special warning when found in the text.

=head1
EXAMPLE

scmsgs_style B<-v> *.c

runs scmsgs style checks on all .c files in the current directory.

=head1
FILES

=over 4

=item (*)

$PROGDIR/scmsgs_dict.txt - standard supplementary dictionary

=item (*)

$PROGDIR/scmsgs_antidict.txt - standard anti-dictionary

=item (*)

$PROGDIR/scmsgs_staudit - script to check EXPL_FILE

=back

=head1
ENVIRONMENT VARIABLES

=head1
SEE ALSO

ispell(1)

=cut

EOF
}

Getfiles() {
	ABSSRC=$(/bin/pwd)

	WS=${ABSSRC%/usr/src*}
	wsname=$(basename $WS)

	case "$wsname" in
	*+*) SFLAG=-L ;;
	*) SFLAG=-S ;;
	esac

	$PROGDIR/scmsgs_getfiles $VFLAG $SFLAG . \
		| sed -e 's,^\./,,'
}

PROGDIR=$(dirname $0)
PREFIX=sty
DFLAG=
VFLAG=
DICTFLAG=
ANTIDICTFLAG=

while getopts ':dvA:D:' flag; do
	case "$flag" in
	d) DFLAG=-d ;;
	v) VFLAG=-v ;;
	A) ANTIDICTFLAG="-A=$OPTARG" ;;
	D) DICTFLAG="-D=$OPTARG" ;;
	*) echo unknown flag "$OPTARG"; exit 1;;
	esac
done

shift `expr $OPTIND - 1`

if [ $# -eq 0 ]; then
	Getfiles
else
	echo "$@"|tr ' ' '\12'
fi \
	| $PROGDIR/scmsgs_getmsgs -x -L -c $VFLAG \
	| $PROGDIR/scmsgs_staudit $DICTFLAG $ANTIDICTFLAG $DFLAG $VFLAG

