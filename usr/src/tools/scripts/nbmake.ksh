#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
# 
# ident	"@(#)nbmake.ksh	1.19	09/04/07 SMI"
#
#	nbmake [-Dv] [-- MAKE_FLAGS] MAKE_ARGS
# do a make within sun cluster tree.
# OPTHOME, SPRO and TEAMWARE may be set in the environment or on the command
# line to override /ws/sc31u-tools or /opt defaults.
#

function Ignore_pod {
	cat << EOF

=head1
NAME

nbmake - run dmake with an nbuild-like environment

=head1
SYNOPSIS

nbmake [B<-D>] [B<-v>] [B<--> MAKE_FLAGS] MAKE_ARGS

=head1
DESCRIPTION

I<nbmake> creates an environment similar to that created by
I<nbuild>(1), then calls I<dmake>(1) in the current workspace.
this eliminates the need for the multi-step process of
doing a cd, running I<ws>(1), doing another cd, and running I<dmake>.
instead, if you're in your workspace, you can just run I<nbmake>.

the C<-D> flag to I<nbmake> forces a debug build.  the default
is non-debug.

the C<-v> (verbose) flag enables an initial dump of
the environment before calling dmake.

to pass flags to I<dmake> itself,
use C<--> to mark the end of the I<nbmake> flags.

whatever further arguments you specify to nbmake, are passed to I<dmake>.

as with I<nbuild>, you do not need to specify an environment file.
but unlike I<nbuild>, I<nbmake> builds directly in the
current workspace - NOT the corresponding virtual workspace.
if you want to use I<nbmake> to build a virtual workspace,
then you must first cd to somewhere within the virtual workspace.

=head1
EXAMPLES

  nbmake OS=5.10

the above example builds the current directory, non-debug, for solaris 10.

  nbmake -D -- -k

the above example builds the current directory, debug, and passes
the C<-k> flag to dmake.

=head1
EXIT STATUS

whatever dmake returns.

=cut

EOF
}

function Vecho {
	$v_FLAG || return
	echo "$prog: $@"
}

function Usage {
	Die 'Usage: nbmake [-D] [-v] [-- MAKE_FLAGS] [MAKE_ARGS]'
}

function Warn {
	echo 1>&2 "$prog:" "$@"
}

function Subheader {
	echo "\n====" "$@"
}

function Die {
	echo "$@"
	Cleanup
	exit 2
}

function Cleanup {
	# nothing to do
	: imdone
}

# join arguments as alternatives in a single regular expression
function Joinx {
	perl -e 'print join("|",@ARGV),"\n"' "$@"
}

# Group_grepv REGEXPS
# process stdin, filtering out lines not containing a colon (:),
# and filtering out lines containing any of the given REGEXPS
# return success if NO lines are printed (opposite of normal egrep).
function Group_grepv {
	(grep ':' | egrep -v "$(Joinx $@)") \
		|| return 0
	return 1
}

# return true iff the argument is the name of an existing function
function Is_function {
	typeset func=$1
	typeset wh=$(whence -v $func 2>&1 | grep 'is a function')
	[ -n "$wh" ]
}

function Interrupted {
	Warn interrupted
	Cleanup
	exit 2
}

# ----- routines that implement workspace naming conventions
function Get_native_os {
	case "$(uname)" in
	Linux) echo Linux ;;
	*) uname -r ;;
	esac
}

function Get_native_mach {
	case "$(uname -p)" in
	sparc) echo sparc ;;
	*)     echo i386 ;;
	esac
}

# fixup automount reported pathnames to match what the user sees
function am_fix {
	sed -e 's,^/\.automount/\([^/]*\)/root,/net/\1,' \
		-e 's,^[^:/]*:/\.automount/\([^/]*\)/root/,/net/\1/,'
}

function Get_curws {
	typeset wsdir=Codemgr_wsdata
	typeset curdir=${1:-$(/bin/pwd)}
	curdir=$(echo $curdir|am_fix)

	while true; do
		if [ -d "$curdir/$wsdir" ]; then
			echo $curdir
			return 0
		fi
		newdir=$(dirname $curdir)
		if [ "$newdir" = "$curdir" ]; then
			echo '***UNKNOWN***'
			return 1
		fi
		curdir=$newdir
	done
}

function Get_virtws {
	typeset pfile="Codemgr_wsdata/parent"
	typeset curws=$(Get_curws $*)
	if [ $? -ne 0 ]; then
		echo "$curws"
		return 1
	fi

	# get the type of the file
	lspfile=$(ls -l "$curws/$pfile" 2>&1 | sed -ne 's%\(.\).*%\1%p')
	if [ "$lspfile" = l ]; then
		# it's a symlink - we're in the virtual ws already.
		echo "$curws"
		return 0
	fi

	# we're in the non-virtual twin,
	# so generate the virtual twin's name based on the $OS and $MACH.
	echo "$curws" | sed -e 's,\([^/]*\)$,\1'"+$OS+$MACH",
	return 0
}

function Get_sol_ver {
	typeset osver=${1:-$OS}
	osver=${osver:-$(uname -sr)}

	case "$osver" in
	Linux*)
		echo Linux ;;
	SunOS*|[25].[0-9]*)
		echo "$osver" | sed -ne 's,.*[25]\.\([0-9]*\).*,Sol_\1,p' ;;
	*)
		echo unknown ;;
	esac
	return 0
}

# ----- environment-setting routines
#
# Clear most environment variables so that this script
# has positive control of the environment.
# Additional user vars may be specified on the command line.
#
function Clear_env {
	for v in Nothing $(/bin/env | egrep '^[A-Za-z]' | sed -ne 's/=.*//p'); do
		case "$v" in
		MAKE|MAKEFLAGS|MAKETOOLS|HOME|LOGNAME|USER|OS|SPRO|TEAMWARE|OPTHOME)
			;;
		[TV]_FLAG)
			;;
		*)
			unset $v;;
		esac
	done
}

function Set_common_env {
	export MAKE=${MAKE:-dmake}
	export MAKEFLAGS=${MAKEFLAGS:-S}
	export PARENT_WS=${PARENT_WS:-$(Get_curws)}

	# these may depend on user-specified value of OS
	export CODEMGR_WS=${CODEMGR_WS:-$PARENT_WS}
	export SOL_VER=${SOL_VER:-$(Get_sol_ver)}

	MAILTO=$LOGNAME
	TMPDIR="/tmp/$prog-tmp.$$"

	WSBASE=$(basename $CODEMGR_WS)
	LOCKDIR="/tmp/$prog-lock.$WSBASE-$LOGNAME"

	ATLOG="$CODEMGR_WS/log"
	REFLOG=$ATLOG/ref

	# OS related settings.
	export ROOT_PROTO=proto/$SOL_VER

	# Reference and archives related settings.  Usually don't change.
	# build environment variables
	export ROOT="$CODEMGR_WS/proto/root_$MACH"
	export SRC="$CODEMGR_WS/usr/src"
	# OK to change IDSTR to your own identifying string
	export IDSTR=${IDSTR:-$(basename $PARENT_WS)}
	export VERSION="$IDSTR:$(date '+%Y-%m-%d')"

	export DMAKE_MAX_JOBS=${DMAKE_MAX_JOBS:-4}
	export DMAKE_MODE=${DMAKE_MODE:-parallel}

	export ENVLDLIBS1="-L$ROOT/usr/lib -L$ROOT/usr/ccs/lib"
	export ENVCPPFLAGS1="-I$ROOT/usr/include"

	# we export POUND_SIGN to speed up the build process
	# -- prevents evaluation of the Makefile.master definitions.
	export POUND_SIGN="#"

	case "$MAKE-$DMAKE_MODE" in
	*dmake-parallel) MAKETYPE="parallel dmake";;
	*dmake-*)        MAKETYPE="serial dmake";;
	*)               MAKETYPE="special $(basename $MAKE)";;
	esac

	TOOLS=$SRC/tools
	TOOLS_PROTO=$TOOLS/proto
	CHK=${CHK:-chk}
}

function Set_path {
	export PATH="$OPTHOME/scbld/bin:$OPTHOME/scbld/bin/$MACH:$OPTHOME/onbld/bin:$OPTHOME/onbld/bin/$MACH:/usr/ccs/bin:/bin:/usr/bin:$SPRO/bin:$TEAMWARE/bin:/usr/sbin"

	if [ -n "$MAKETOOLS" ]; then
		PATH=$MAKETOOLS:$PATH
	fi
	if [ -d "$TOOLS_PROTO" ]; then
		typeset tp=$TOOLS_PROTO/opt/scbld/bin
		export CTFCONVERT=$tp/$MACH/ctfconvert
		export CTFMERGE=$tp/$MACH/ctfmerge
		export PATH=$tp:$tp/$MACH:$PATH
		export SHMSGPREP=$tp/$MACH/extract_gettext
	fi
}

function Check_env {
	Vecho "Checking environment..."
	Vecho '"@(#)nbmake.ksh	1.19	09/04/07 SMI"'
	Vecho "PARENT_WS=$PARENT_WS"
	Vecho "CODEMGR_WS=$CODEMGR_WS"
	Vecho "SOL_VER=$SOL_VER"
	Vecho "MACH=$MACH"
	case "$CODEMGR_WS" in
	\**UNKNOWN*) Die "error: current dir is not within a workspace" ;;
	esac
}

function Set_before_env {
	# Vecho "Initializing environment"
	Clear_env
	Set_path

	export LC_COLLATE=C \
		LC_CTYPE=C \
		LC_MESSAGES=C \
		LC_MONETARY=C \
		LC_NUMERIC=C \
		LC_TIME=C

	export OS=${OS:-$(Get_native_os)}
	export MACH=${MACH:-$(Get_native_mach)}
	export OPTHOME=${OPTHOME:-/ws/sc31u-tools}
	[ -d $OPTHOME/onbld/bin ] || OPTHOME=/opt
	export SPRO=${SPRO:-$OPTHOME/SUNWspro}
	export TEAMWARE=${TEAMWARE:-$OPTHOME/teamware}

	Set_path
}

function Initial_dump {
	env|sort
	echo ''
}

function Handle_flag {
	case "$1" in
	D) D_FLAG=true;;
	F) D_FLAG=false;;
	v) v_FLAG=true;;
	*) argerr=true;;
	esac
}

function EnvMake {
	Set_before_env
	# source in nbuild.rc from workspace, if any.
	# options set there will be treated like cmd line VAR=VALUE options.
	[ -e $CODEMGR_WS/usr/src/$prog.rc ] \
		&& . $CODEMGR_WS/usr/src/$prog.rc
	OPTIND=1
	while getopts $allflags FLAG; do
		Handle_flag $FLAG
	done
	$argerr && Usage
	shift $(expr $OPTIND - 1)
	if ! $D_FLAG; then
		export RELEASE_BUILD=
	fi
	trap Interrupted 1 2 3 9 15
	Set_after_env

	$v_FLAG && Initial_dump

	# any remaining arguments are passed to make
	$MAKE -e "$@"
	XSTAT=$?
}

function Set_after_env {
	Set_common_env
	# set PATH again in case some variables changed.
	Set_path
	Check_env
}

# ----- mainline code
NBUILD_ARGS="$*"
prog=$(basename $0)
allflags=DFvq
D_FLAG=false
F_FLAG=false
v_FLAG=false
argerr=false
XSTAT=1

EnvMake "$@"
Cleanup

exit $XSTAT
