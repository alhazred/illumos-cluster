#! /usr/bin/perl -s
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)scmsgs_audit.pl	1.10	08/05/20 SMI"
#
#	scmsgs_audit [-OPTIONS] < MSGS_LIST
#		-d			debug
#		-f=EXPL_FILE		explanations file
#		-g=GRANDFATHERED_FILE	file of grandfathered messages
#		-o=NEW_EXPL_FILE	write new scmsgs file
#		-u=NEW_UNREF_FILE	write unreferenced entries to this file
#		-U			treat unreferenced entries as errs
#		-P=PH_FILE		write placeholder entries to this file
#		-v			verbose
#		-V			more verbose
#
# read in explanation entries from EXPL_FILE, and
# messages from STDIN.  run various consistency checks
# and report the results.  among the inconsistencies reported:
#	- mismatches between id and message text in EXPL_FILE
#	- probable missing Explanation fields in EXPL_FILE entries
#	- inconsistent format in EXPL_FILE
#	- duplicate messages in EXPL_FILE
#	- explanations in EXPL_FILE with no message in STDIN
#	- messages in STDIN with no explanation in EXPL_FILE
#	- duplicate messages in STDIN
# optionally write a new, explanations file to NEW_EXPL_FILE, with
# information integrated from both EXPL_FILE and STDIN.

# add $PROGDIR to @INC so that SCMSGS.pm can be found.
BEGIN {
	($PROGDIR = ".") unless (($PROGDIR) = ($0 =~ m{^(.*)/}));
	unshift @INC, $PROGDIR;
}

# ---------- start of subroutines

use SCMSGS;

# # operator to un-escape escaped doublequotes
# sub _op_dblquote { $_[0] =~ s/\\"/"/g; }

# operator to standardize whitespace
sub _op_ws
{
	$_[0] =~ s/^\s*(.*?)\s*$/$1/;
	$_[0] =~ s/\s+/ /g;
}

# operator to do a few simple spellcheck-type corrections
sub _op_spell
{
	# i before e, except after c.
	$_[0] =~ s/cie/cei/g;
}

# operator to generate "signature"
sub _op_sig
{
	$_[0] =~ s/\\n//g;
	$_[0] =~ s/%[ds]/XXX/g;
	$_[0] =~ s/\W+//g;
	$_[0] = lc substr($_[0], 0, 60);
}

# operator to remove first word if it looks like a "tag".
sub _op_untag
{
	$_[0] =~ s/^\[?\$?[A-Z_]+\]?\s*([A-Z])/\1/;
}

# apply all combinations of two operators
# return the results, uniquified and sorted in order of string length
# (longest strings first).
sub gen_variants
{
	my $msgtext = shift;
	my ($x, $y);
	my %results = ();
	foreach $p (@OP_SUBS) {
		$x = $msgtext;
		&{$p}($x);
		foreach $q (@OP_SUBS) {
			next if ($p eq $q);
			$y = $x;
			&{$q}($y);
			$results{$y} = 1 if length $y >= 16;
		}
	}
	return sort {length($b) - length($a)} keys %results;
}

sub mkmsgid
{
	my $s = shift;
	$s =~ s/\\n//g;
	return &SCMSGS::STRLOG_MAKE_MSGID($s);
}

#
# read_expl_file() --
# read in the explanations file.
# at the end,
# $ORIG_EXPL_LIST is a reference to the complete list of objects read in;
# @EXPL_LIST is the list of objects with duplicates (by msgtext) removed;
# %EXPL_LIST is a hash of explanation msgtext to the corresponding object.
# %EXPL_LOCS_BY_TEXT is a hash of msgtext to array of line numbers
# within the explanations file of the entries in which the
# msgtext was found.
#
sub read_expl_file
{
	my $fn = shift;
	my ($x, $msgtext);
	@EXPL_LIST = ();
	%EXPL_LIST = ();
	$EXPL_LIST = [];

	return if $fn eq '';

	my $fp = new SCMSGS($fn);

	$ORIG_EXPL_LIST = $fp->explist();

	foreach $x (@$ORIG_EXPL_LIST) {
		$msgtext = $x->{msgtext};

		push @{$EXPL_LOCS_BY_TEXT{$msgtext}}, $x->{LINENO};
		if ($EXPL_LIST{$msgtext}) {
			# if the same msgtext was seen already in another
			# explanation entry, combine the entries.
			&SCMSGS::merge_msgs($EXPL_LIST{$msgtext}, $x);
			next;
		}

		$EXPL_LIST{$msgtext} = $x;
		push @EXPL_LIST, $x;

		if ($opt_V) {
		if ($x->{MISSX}) {
		my $loc = $EXPL_FILE.":".$x->{LINENO};
print "warning: probable missing tag at $loc\n";
		}

		if (&SCMSGS::is_incomplete($x)) {
		my $loc = $EXPL_FILE.":".$x->{LINENO};
print "warning: incomplete entry $loc\n";
		}
		}
	}

	$EXPL_LIST = \@EXPL_LIST;
}

#
# read_grandfathered() --
# read the file of grandfathered messages.
# messages in this list will be allowed to go unexplained.
#
sub read_grandfathered
{
	my $fn = shift;

	my $fp = new SCMSGS($fn);

	$GF_LIST = $fp->explist();

	foreach $x (@$GF_LIST) {
		$msgtext = $x->{msgtext};
		$GF_TEXT{$msgtext} = 1;
	}
}

#
# check_hybrid_clashes() --
# check for clashes among messages in the hybrid list.  a clash is
# where multiple messages have message texts that are essentially
# the same, although they may not be 100% identical.
# as a side-effect, sets the %EXPL_CLASH hash.
#
sub check_hybrid_clashes
{
	# first generate signatures from the msgtext of each explanation entry,
	# and group the entries by signature.
	my %EXPL_BY_SIG = ();
	$EXPL_CLASH_COUNT = 0;
	foreach $x (@HYBRID_LIST) {
		my $msgtext = $x->{msgtext};
		my $sig = $msgtext;
		&_op_sig($sig);
		push @{$EXPL_BY_SIG{$sig}}, $x;
	}

	# report groups that have more than one element.
	foreach $sig (keys %EXPL_BY_SIG) {
		my $xp = $EXPL_BY_SIG{$sig};
		if (@$xp > 1) {
			my $msgids = join(",", sort map($_->{msgid}, @$xp));
			if ($opt_V) {
print "warning: MSG CLASH: msgids=$msgids\n";
				foreach $x (@$xp) {
					my $s = &textloc($x->{msgtext});
					print "    $s\n";
				}
			}
			foreach $x (@$xp) {
				$EXPL_CLASH{$x->{msgtext}} = $msgids;
			}
			$EXPL_CLASH_COUNT++;
		}
	}
}

#
# hash_msg_variants() --
# create a hash that will allow us to quickly identify if a variant
# of a message from the MSG list will match something in the EXPL file.
#
sub hash_msg_variants
{
	foreach $x (@MSG_LIST) {
		my $msgtext = $x->{msgtext};
		foreach $v (&gen_variants($msgtext))
			{ $MSG_VARIANTS{$v} = $msgtext; }
	}
}

#
# check_expl_matches() --
# take note of all the straight matches between expl and msg.
# we have to do this before we try testing variants of the
# explanations or messages, to prevent the variants from
# changing the msgtext of already-correct explanation entries.
#
sub check_expl_matches
{
	foreach $x (@EXPL_LIST) {
		my $msgtext = $x->{msgtext};
		if ($MSG_LOCS_BY_TEXT{$msgtext}) {
			# a matching message entry has been found.
			# merge its info into the expl entry.
			&SCMSGS::merge_expl_msg($x, $MSG_LIST{$msgtext});
			$EXPL_FOUND_MSG{$msgtext} = 1;
		}
	}
}

#
# check_expl_mismatches() --
# check for msgid/msgtext mismatches in expl file
# if necessary and possible, modify the msgtext
# to match the id.
#
sub check_expl_mismatches
{
	my ($x, $v, $msgtext, $msgid, $cmsgid, $ctext);

	MAJOR:
	foreach $x (@EXPL_LIST) {
		$msgtext = $x->{msgtext};
		$msgid = $x->{msgid};
		$cmsgid = &mkmsgid($msgtext);

		# does the computed id match the listed id?
		# if so, assume the entry is OK.
		next if ($msgid eq $cmsgid);

		# the computed id did not match the listed msgid.
		# if the msgtext matches a syslog text, we assume the
		# msgtext must be correct and the msgid is wrong.
		if ($EXPL_FOUND_MSG{$msgtext}) {
			$EXPL_FOUND_MSG{$msgtext} = 1;
			$EXPL_FIXED_ID{$msgtext} = 1;
			$x->{msgid} = $cmsgid;
			if ($opt_V) {
print "CORRECTION: MSGID on expl, msgid=$msgid -> $cmsgid [$msgtext]\n";
			}
			next;
		}

		# try comparing all variants of the listed msgtext vs
		# all variants of the syslog messages.
		foreach $v (grep $MSG_VARIANTS{$_}, @variants) {
			$ctext = $MSG_VARIANTS{$v};
			next   if $EXPL_FOUND_MSG{$ctext};

			&alter_expl($x, $ctext);
			next MAJOR;
		}

		# does the listed id match a syslog message?
		# if so, assume the syslog message is the correct msgtext.
		$ctext = $MSG_TEXT_BY_ID{$msgid};
		if ($ctext ne ''
		 && !$EXPL_FOUND_MSG{$ctext}) {
			&alter_expl($x, $ctext);
			next;
		}

		$EXPL_UNFIXED{$msgtext} = 1;
		$x->{msgid} = $cmsgid;
		if ($opt_V) {
print "CORRECTION: $msgid --> $cmsgid [$msgtext]\n";
		}
	}
}

#
# check_expl_dup() --
# check the list of explanations for dups - entries that have
# identical message text.
#
sub check_expl_dup
{
	my @texts = sort keys %EXPL_LOCS_BY_TEXT;
	# $EXPL_DISTINCT = 0 + @texts;

	foreach $msgtext (@texts) {
		my $p = $EXPL_LOCS_BY_TEXT{$msgtext};
		my $n = 0+@$p;
		if ($n > 1) {
print "warning: EXPL DUPx$n: [$msgtext]\n";
print "  [", join(", ", @$p), "]\n";
		}
	}
}


#
# read_msg_file() --
# read in the messages list from STDIN.
# at the end,
# $ORIG_MSG_LIST is a reference to the full list of objects read in;
# @MSG_LIST is the list of objects with duplicates removed;
# %MSG_LIST is a hash of text to the corresponding objects;
#
sub read_msg_file
{
	my $mf =    new SCMSGS("STDIN", \*STDIN);
	@MSG_LIST = ();
	%MSG_LIST = ();

	$ORIG_MSG_LIST = $MSG_LIST = $mf->explist();
# print STDERR "kount=",0+@$MSG_LIST,"\n";

	foreach $x (@$ORIG_MSG_LIST) {
		my $msgid = $x->{msgid};
		my $msgtext = $x->{msgtext};

		# location info is treated specially, stored in a
		# separate hash.  it's going to be printed as comments,
		# not as part of the entries proper.
		push @{$MSG_LOCS_BY_TEXT{$msgtext}},
				split /\s+/, $x->{location};
		# delete $x->{location};

		# we assume that external preprocessing
		# has removed any message dups by this point.
		$MSG_LIST{$msgtext} = $x;
		push @MSG_LIST, $x;
		$MSG_TEXT_BY_ID{$msgid} = $msgtext;
	}

	$MSG_LIST = \@MSG_LIST;
}

#
# check_msg_file_dups()--
# check the input list of syslog messages for dups (by message text).
#
sub check_msg_file_dups
{
	foreach $msgtext (sort keys %MSG_LOCS_BY_TEXT) {
		my $p = $MSG_LOCS_BY_TEXT{$msgtext};
		my $n = 0+@{$p};
		if ($n > 1) {
print "warning: MSG DUPx$n: [$msgtext]\n",
"    ", join("\n    ", &compact_locs(@$p)), "\n";
		}
	}
}

#
# check_msg_dupex() --
# check the list of syslog messages for ones with duplicate SCMSGS comments.
#
sub check_msg_dupex
{
	$NCOMMENTDUPS = 0;
	foreach $msgtext (sort keys %MSG_LOCS_BY_TEXT) {
		# check the list of locations for multiple *'s.
		my $p = $MSG_LOCS_BY_TEXT{$msgtext};
		my @commentedlocs = grep m{\*}, @{$p};
		if (@commentedlocs > 1) {
			my $n = 0+@commentedlocs;
print "error: SCMSGS COMMENT DUPx$n: [$msgtext]\n",
"    ", join("\n    ", &compact_locs(@commentedlocs)), "\n";
			$NCOMMENTDUPS++;
		}
	}
}

#
# check_expl_unused() --
# check the EXPL file list for explanation entries that are unused, ie,
# entries whose message was not found among the input syslog messages.
#
sub check_expl_unused
{
	my $msgtext;

	MAJOR:
	foreach $x (@EXPL_LIST) {
		$msgtext = $x->{msgtext};

		# if we found a match, great.
		if ($MSG_LOCS_BY_TEXT{$msgtext}) {
			$EXPL_FOUND_MSG{$msgtext} = 1;
			next;
		}

		# if not, try variants of messages to see if any
		# of them will match.  if so, modify the msgtext of the
		# expl entry.
		foreach $v (grep $MSG_VARIANTS{$_}, &gen_variants($msgtext)) {
			$ctext = $MSG_VARIANTS{$v};
			next if $EXPL_FOUND_MSG{$ctext};

			&alter_expl($x, $ctext);

			$VARIANT_FOUND_MSG{$msgtext} = 1;
			next MAJOR;
		}

		$EXPL_UNUSED{$msgtext} = 1;
	}

	if ($opt_V) {
	foreach $msgtext (sort keys %EXPL_UNUSED) {
print "warning: EXPL UNUSED: [$msgtext]\n";
	}
	}
}

#
# check_msg_unexplained()
# identify input syslog messages that have no explanation entry.
#
sub check_msg_unexplained
{
	my $msgtext;

	$UNEXPLAINED_COUNT = 0;
	$EXPLAINED_BY_GF = 0;
	$EXPLAINED_BY_COMMENT = 0;
	$EXPLAINED_BY_EXPL = 0;

	foreach $x (@MSG_LIST) {
		if ($x->{explanation}) {
			# the message was explained in an SCMSGS comment
			$EXPLAINED_BY_COMMENT++;
			next;
		}
		$msgtext = $x->{msgtext};
		if ($EXPL_LOCS_BY_TEXT{$msgtext}) {
			# the message was found in the EXPL file
			$EXPLAINED_BY_EXPL++;
			if ($opt_d) {
			my $msgid = &mkmsgid($msgtext);
print "MSG EXPLAINED BY EXPL_FILE: $msgid [$msgtext]\n";
			}
			next;
		}
		if ($GF_TEXT{$msgtext}) {
			# the message was grandfathered.
			$EXPLAINED_BY_GF++;
			my ($s) = &textloc($msgtext);
print "warning: GRANDFATHERED MSG: $s\n";
			next;
		}

		# there was no explanation
		$UNEXPLAINED_COUNT++;

		if ($opt_v) {
			my ($s) = &textloc($msgtext);
print "error: UNEXPLAINED MSG: $s\n";
		}
	}
}

sub summarize
{
	local $| = 1;

	my $EXPL_UAXMISS = 0+(grep &SCMSGS::is_incomplete($_), @EXPL_LIST);

	my $NEXPL = 0+@$ORIG_EXPL_LIST;
	my $NMSGS = 0+@$ORIG_MSG_LIST;
	my $NEXPL_FIXED_ID = 0+(keys %EXPL_FIXED_ID);
	my $NEXPL_FIXED_TEXT = 0+(keys %EXPL_FIXED_TEXT);
	my $NEXPL_UNFIXED = 0+(keys %EXPL_UNFIXED);
	my $NWRONG = $NEXPL_FIXED_ID+$NEXPL_FIXED_TEXT+$NEXPL_UNFIXED;
	# my $VARIANT_FOUND_MSG = 0+(keys %VARIANT_FOUND_MSG);
	my $EXPL_DISTINCT = 0+@EXPL_LIST;
	my $UNREFS = 0+(keys %EXPL_UNUSED);
	my $MSGID_CHANGED_COUNT = $NWRONG-$NEXPL_FIXED_TEXT;
	my $NGF = 0+@$GF_LIST;
	my $NFATAL = $UNEXPLAINED_COUNT + $NCOLLISIONS + $NCOMMENTDUPS;

	print STDERR <<EOF;
#===== SUMMARY =====
EOF

	if ($opt_f ne '') {
	print STDERR <<EOF;
#* $NEXPL total entries read from EXPL_FILE: $EXPL_DISTINCT distinct;
#  $EXPL_UAXMISS entries were missing either user action or explanation;
#  $UNREFS entries were apparently not referenced;
#  $NWRONG entries had incorrect MSGID:
#    $NEXPL_FIXED_TEXT corrected text, $MSGID_CHANGED_COUNT corrected msgid.
EOF
	}

	if ($opt_g ne '') {
	print STDERR <<EOF;
#* $NGF total entries were read from GRANDFATHERED list.
EOF
	}

	print STDERR <<EOF;
#* $NMSGS messages read from STDIN:
#    $EXPLAINED_BY_COMMENT messages were explained in SCMSGS comments;
#    $EXPLAINED_BY_EXPL messages were explained in EXPL_FILE;
#    $EXPLAINED_BY_GF messages were grandfathered.
#* $PLACEHOLDERS entries were placeholders.
#* $NFATAL fatal errors:
#    $UNEXPLAINED_COUNT messages had no explanation entry;
#    $NCOLLISIONS msgid collisions were found;
#    $NCOMMENTDUPS messages with duplicate SCMSGS comments were found.
EOF
}

#
# filedate() --
# return the mod date of a file in canonical form.
#
sub filedate
{
	my $fn = shift;
	return &candate((stat($fn))[9]);
}

#
# candate() --
# return date string in a canonical format YYYY.MM.DD.hh.mm
#
sub candate
{
	my $t = shift;
	my ($sec, $min, $hr, $mday, $mon, $year, @junk) = localtime($t);
	return sprintf("%04d.%02d.%02d,%02d:%02d",
		$year+1900, $mon+1, $mday, $hr, $min);
}

sub dump_entry
{
	local ($fh, $x) = @_;

	my $msgid = $x->{msgid};
	my $msgtext = $x->{msgtext};
	my $errs = '';

	# before printing the entry, print a comment listing the
	# location(s) of the syslog message, and other info.
	my $locs = $MSG_LOCS_BY_TEXT{$msgtext};

	print $fh "\n";
	if (!$locs) {
		# an unreferenced entry.
		$errs .= "U";
	}
	else {
		# print comment indicating where this entry is referenced.
		foreach $loc (&compact_locs(@$locs))
			{ print $fh "# at: $loc\n"; }
	}

	if ($EXPL_CLASH{$msgtext}) {
		print $fh "# clash: msgids=$EXPL_CLASH{$msgtext}\n";
		$errs .= "C";
	}

	if ($EXPL_COLLIDED{$msgid}) {
		print $fh "# collision: msgids=$EXPL_COLLIDED{$msgid}\n";
		$errs .= "K";
	}

	if ($msgtext =~ m/^\s|\s$/) {
		$SPACERR_COUNT++;
		$errs .= "S";
	}

	my @keys = sort keys %$x;
	if (grep m/(explanation|user_action)-/, @keys) {
		$DUPERR_COUNT++;
		$errs .= "D";
	}
	if (&SCMSGS::is_incomplete($x)) {
		$XERR_COUNT++;
		$errs .= "X";
	}
	if ($errs) {
		$ERROR_ENTRIES++;
		print $fh "# >>>ERRORS: $errs\n";
	}

	&SCMSGS::print_entry($fh, $x);
}

# dump a group of explanation file entries.
sub dump_sect
{
	my ($fh, $list) = @_;
	foreach $x (sort {$a->{msgid}-$b->{msgid}} @$list) {
		&dump_entry($fh, $x);
	}
}

#
# create_hybrid_list() --
# creates hybrid (combined) list of message entries from the EXPL file and
# from the input list of syslog messages.  as a side-effect, also create
# the list of placeholders.  if the -P flag is given, placeholders get
# written to a different output file.
#
sub create_hybrid_list
{
	my $xx;

	# add all unexplained messages into @EXPL_LIST.
	# this doesn't do anything??
	push @EXPL_LIST, values %MSG_UNEXPLAINED;

	# &check_expl_clashes();

	@PH_LIST = ();
	$PLACEHOLDERS = 0;

	# create hybrid list of messages.
	# for each message in MSG_LIST, if it has an SCMSGS comment, use it;
	# else use the explanation from EXPL_LIST if any.
	@HYBRID_LIST = ();
	foreach $x (@MSG_LIST) {
		if ($x->{explanation} ne '') {
			if (&SCMSGS::is_placeholder_entry($x))
				{ push @PH_LIST, $x; next; }
			push @HYBRID_LIST, $x; next;
		}
		$xx = $EXPL_LIST{ $x->{msgtext} } || $x;
		if (&SCMSGS::is_placeholder_entry($xx))
			{ push @PH_LIST, $xx; next; }
		push @HYBRID_LIST, $xx;
	}

	$PLACEHOLDERS = 0+@PH_LIST;

	## if -P was not specified, placeholder entries go to NEW_EXPL_FILE.
	# push @HYBRID_LIST, @PH_LIST if $opt_P eq '';
}

sub write_expl_file
{
	my ($EXPL_FILE, $ofn) = @_;

	open (OFN, ">$ofn")
		|| die("error $! -- can't create NEW_EXPL_FILE \"$ofn\"\n");

	my $expl_file_date = &filedate($EXPL_FILE);
	my $curdate = &candate(time);

	print OFN <<EOF;
$COM this file was generated by $PROG -- DO NOT EDIT!!
$COM GENDATE=$curdate EXPL_FILE=$EXPL_FILE ($expl_file_date)
$COM
$COM Error code key:
$COM	X - missing explanation
$COM	C - inter-message clash
$COM	K - msgid collision
$COM	D - duplicate entry
$COM	U - unreferenced entry
$COM	S - msgtext has leading or trailing spaces
$COM
EOF

	&dump_sect(\*OFN, \@HYBRID_LIST);

	my $UAX = $XERR_COUNT;
	my $N = 0+@HYBRID_LIST;
	my $MX = 0+@unexpl;
	print OFN <<EOF;

$COM $N entries.
$COM $ERROR_ENTRIES entries have errors.
$COM $XERR_COUNT entries are missing explanation or user action (X).
$COM $DUPERR_COUNT entries have dups (D).
$COM $EXPL_CLASH_COUNT entries have msgtext clashes with others entries (C).
$COM $EXPL_COLLISION_COUNT entries have MSGID collisions (K).
$COM $SPACERR_COUNT entries have msgtext with leading or trailing spaces (S).
EOF
	if ($opt_P eq '') {
		&write_placeholders(\*OFN);
	}

	close (OFN);

	if ($opt_u ne '') {
		# find unreferenced messages from EXPL_FILE
		@UNREF_LIST = grep $MSG_LOCS_BY_TEXT{$_->{msgtext}} eq '',
				@EXPL_LIST;

		open(UFILE, ">$opt_u")
		 || warn("$PROG: can't create file \"$opt_u\" for unreferenced entries\n");
		print UFILE <<EOF;
$COM this file was generated by $PROG -- DO NOT EDIT!!
$COM it contains unreferenced explanations.
$COM GENDATE=$curdate EXPL_FILE=$EXPL_FILE ($expl_file_date)
EOF
		&dump_sect(\*UFILE, \@UNREF_LIST);

		$N = @UNREF_LIST;
		print UFILE<<EOF;
# $N unreferenced entries.
EOF

		close(UFILE);
	}

	# if -P was specified, write placeholder entries to a separate file.
	if ($opt_P ne '') {
		open(PHFILE, ">$opt_P") || warn("$PROG: can't create file \"$opt_P\" for placeholder entries\n");
		&write_placeholders(\*PHFILE);
		close(PHFILE);
	}
}

sub write_placeholders
{
	my $F = shift;

#	print $F <<EOF;
#$COM this file was generated by $PROG -- DO NOT EDIT!!
#$COM it contains placeholder explanations.
#$COM GENDATE=$curdate EXPL_FILE=$EXPL_FILE ($expl_file_date)
#EOF
	&SCMSGS::set_print_prefix("## ");
	&dump_sect($F, \@PH_LIST);
	&SCMSGS::set_print_prefix("");

	$N = 0+@PH_LIST;
	print $F <<EOF;
$COM $N placeholder entries.
EOF
}

#
# check_hybrid_collisions() --
# check the hybrid list for msgid collisions and duplicate explanations.
# this has to be done after the hybrid list has been created, of course.
# we need to check BOTH the list from the EXPL file and the list of
# input syslog messages for collisions.
#
sub check_hybrid_collisions
{
	my (%x_by_msgid);
	my (%x_by_text);

	$EXPL_COLLISION_COUNT = 0;
	$EXPL_DUP_COUNT = 0;

	# for each msgid, and each message text, create a hash
	# pointing to the set of matching message objects.
	foreach $x (@HYBRID_LIST) {
		push @{$x_by_msgid{$x->{msgid}}}, $x;
		push @{$x_by_text{$x->{msgtext}}}, $x;
	}

	# now check the sets of message objects.  each one "should be"
	# a singleton.
	my %complained_msgid;
	my %complained_text;
	foreach $x (@HYBRID_LIST) {
		my ($msgtext) = $x->{msgtext};
		my ($msgid) = $x->{msgid};
		my ($mp) = $x_by_msgid{$msgid};
# print STDERR "! checking $msgid [$msgtext]\n";
		if (!$complained_msgid{$msgid}
		 && @{$mp} > 1) {
			# don't complain more than once about a given collision
			$complained_msgid{$msgid} = 1;
			if ($opt_v) {
				my %texts;
				foreach $y (@{$mp}) {
					$texts{$y->{msgtext}} = 1;
				}
print "error: MSGID COLLISION: msgid=$msgid\n";
				foreach $m (keys %texts) {
					my ($s) = &textloc($m);
					print "    $s\n";
				}
			}
			$EXPL_COLLISION_COUNT++;
			$EXPL_COLLIDED{$msgid} = join(",",
				sort(map "$_->{msgid}", @{$mp}));
		}

		my $tp = $x_by_text{$msgtext};
		if (!$complained_text{$msgtext} && @$tp > 1) {
			if ($opt_v) {
				my @locs = map $_->{LINENO}, @$tp;
print "warning: EXPL DUP: [$msgtext]\n";
print "  at lines: ", join(", ", @locs), "\n";
			}
			$EXPL_DUP_COUNT++;
		}
	}
	$NCOLLISIONS = $EXPL_DUP_COUNT + $EXPL_COLLISION_COUNT;
}

# make all changes necessary to correct the text of an explanation entry.
sub alter_expl
{
	my ($x, $ctext) = @_;
	my $msgtext = $x->{msgtext};
	my $msgid = $x->{msgid};
	my $cmsgid = &mkmsgid($ctext);

	$EXPL_LOCS_BY_TEXT{$ctext} = $EXPL_LOCS_BY_TEXT{$msgtext};
	$EXPL_LIST{$ctext} = $EXPL_LIST{$msgtext};
	$EXPL_FIXED_TEXT{$msgtext} = 1;
	$EXPL_FOUND_MSG{$ctext} = 1;

	# a variant of an expl matched a msg variant
	# if a variant matched, modify the
	# stored copy --- the original msgtext was wrong.
	$x->{msgtext} = $ctext;
	$x->{msgid} = $cmsgid;
	if ($opt_V) {
print "CORRECTION: $msgid [$msgtext] --> $cmsgid [$ctext]\n";
	}
}

# compact adjacent locations FILE:LINENO that have the same
# FILE, into FILE:LINE1,LINE2,.., to save space.
sub compact_locs
{
	my @r = ();
	my $lastf = '';
	my ($file, $lineno);
	foreach $loc (@_) {
		next if !(($file, $lineno) = ($loc =~ m/(.*):(.*)/));
		if ($file ne $lastf) {
			push @r, $loc;
			$lastf = $file;
			next;
		}
		$r[-1] .= ",$lineno";
	}
	return @r;
}

sub textloc
{
	my $m = shift;
	my $id = &mkmsgid($m);
	my $p = $MSG_LOCS_BY_TEXT{$m};
# print STDERR "! p=$p x=@{$p}\n";
	my $s = "$id [$m]";
	my $n = 0+@$p;
	if ($n > 0) { $s .= " at $p->[0]"; }
	if ($n > 1) { my $n1 = $n-1; $s .= " and $n1 other locations"; }
	return $s;
}

sub main
{
	$DUPERR_COUNT = 0;
	$XERR_COUNT = 0;
	$SPACERR_COUNT = 0;
	$PLACEHOLDERS = 0;

	&read_expl_file($EXPL_FILE);
	&read_grandfathered($opt_g) if ($opt_g ne '');
	&read_msg_file();

	&hash_msg_variants();

	&check_expl_matches();
	&check_expl_mismatches();
	&check_expl_dup()   if $opt_V;

	# since msg dups are so common, let's not treat them as errors.
	# &check_msg_file_dups()   if $opt_V;
	&check_msg_dupex();

	&check_expl_unused();
	&check_msg_unexplained();

	&create_hybrid_list();
	&check_hybrid_clashes();
	&check_hybrid_collisions();

	&write_expl_file($EXPL_FILE, $opt_o)    if ($opt_o ne '');

	&summarize();

	exit ($UNEXPLAINED_COUNT > 0 || $NCOLLISIONS > 0 || $NCOMMENTDUPS > 0);
}


# --------- start of mainline code

# get list of subr names beginning with "_op_".
@OP_SUBNAMES = grep m/^_op_/ && defined &{"main::$_"}, keys %main:: ;
@OP_SUBS = map \&{$_}, @OP_SUBNAMES;

$COM = "###";
$SECCOM = "######";

$opt_d = $d;
$opt_f = $f;
$opt_g = $g;
$opt_v = $v || $V;
$opt_V = $V;
$opt_o = $o;
$opt_u = $u;
$opt_U = $U; # TODO
$opt_P = $P;

($PROG) = ($0 =~ m/([^\/]*)$/);

$EXPL_FILE = $opt_f;

if ($opt_o ne '' && $opt_o eq $EXPL_FILE) {
	# safety check - don't overwrite original EXPL_FILE
	warn("$PROG: NEW_EXPL_FILE would overwrite EXPL_FILE; ignoring -o\n");
	$opt_o = '';
}

&main;
