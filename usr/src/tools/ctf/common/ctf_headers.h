/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CTF_HEADERS_H
#define	_CTF_HEADERS_H

#pragma ident	"@(#)ctf_headers.h	1.4	08/05/20 SMI"

/*
 * Because the tools are executed on the system where they are built,
 * the tools need to include the headers installed on the build system,
 * rather than those in the ON prototype used by Sun Cluster.  However,
 * some of the headers required by the tools are part of the ON source
 * tree, but not delivered as part of Solaris.  These include the following:
 *
 * /ws/on10-gate/usr/src/lib/libctf/common/libctf.h
 * /ws/on10-gate/usr/src/uts/common/sys/ctf_api.h
 * /ws/on10-gate/usr/src/uts/common/sys/ctf.h
 *
 * To address the problem, these headers have been copied in the tools
 * directory as followed:
 *
 * $(SRC)/tools/ctf/common/libctf.h
 * $(SRC)/tools/ctf/common/sys/ctf_api.h
 * $(SRC)/tools/ctf/common/sys/ctf.h
 *
 * and then we have done two things:
 * 1) Created this header with a specific order of inclusion for the
 *    ctf headers.  Because the <libctf.h> header includes <sys/ctf_api.h>
 *    which in turn includes <sys/ctf.h> we need to include these in
 *    reverse order to guarantee that we get the correct versions of
 *    the headers, and that we are not using a downlevel version of headers
 *    which could be installed on the system.
 * 2) In $(SRC)/tools/ctf/Makefile.ctf, the -I includes the directory
 *    where the ctf headers have been copied: ($SRC)/tools/ctf/common.
 */

#include <sys/ctf.h>
#include <sys/ctf_api.h>
#include <libctf.h>

#endif /* _CTF_HEADERS_H */
