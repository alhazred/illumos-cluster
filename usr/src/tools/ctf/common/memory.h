/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MEMORY_H
#define	_MEMORY_H

#pragma ident	"@(#)memory.h	1.4	08/05/20 SMI"

/*
 * Routines for memory management
 */

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

void *xmalloc(size_t);
void *xcalloc(size_t);
char *xstrdup(const char *);
char *xstrndup(char *, size_t);
void *xrealloc(void *, size_t);

#ifdef __cplusplus
}
#endif

#endif /* _MEMORY_H */
