#!/usr/perl5/bin/perl -wn
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)typedef_bug.pl	1.3	08/05/20 SMI"
#

#
# This script translates an input list of entries describing types impacted
# by the typedef bug 4432619 into a C description of these entries. The C
# output is to be compiled with CTF binaries.
#
# The input list is passed on the standard input or in some files specified as
# the arguments of the script. The -n option of Perl is used to make it iterate
# over each line of the standard input or of the specified files. The C output
# file is written on the standard output.
#
# INPUT:
#
# Each typedef bug entry in the input is composed of four fields separated by
# a semi-colon (;) and some optional spaces. An entry can be defined on multiple
# lines separated by a backslash and a newline. In that case, the entry is
# composed all the lines concatenated without the leading and trailing spaces of
# each lines
#
# For example, the following multi-lines entry:
#
# 	line-content \
# 		line-continuation-1 \
#		line-continuation-2
#
# is interpreted like the entry:
#
# 	line-contentline-continuation-1line-continuation-2
#
# The format of an entry is:
#
# <type name> ; <member name> ; <current type> ; <target type>
#
# This means that in type <type name> the member <member name> of type
# <current type> should have its type changed to <target type>.
#
# <type name> generally references a struct or an union, and <member name>
# indicates the member impacted in that struct or union. An empty <member name>
# means that the type impacted has no member, so in that case the type is not
# a struct or a union but a typedef. When an anonymouns struct or union is
# impacted then that it is referenced through a typedef pointing to that struct
# or union.
#
# <current type> describes the current type of the member. Its value can be:
#
#	PPPS(name): a pointer to a pointer to a pointer to the struct "name"
#	PPS(name) : a pointer to a pointer to the struct "name"
#	PS(name)  : a pointer to the struct "name"
#	PI(name)  : a pointer to the intrinsic "name"
#	PT(name)  : a pointer to the typedef "name"
#	PF(name)  : a pointer to the function returning the type "name"
#	S(name)   : the struct "name"
#	I(name)   : the intrinsic "name"
#	AAI(name) : an array of arrays of intrinsic "name"
#
# The special name "unsigned 64" can be used to designate an intrinsic and it
# will match the intrinsic types "unsigned long" and "unsigned long long". This
# is used for types which are different on 32-bit and 64-bit compilations.
#
# <target type> describes the target type of the member. Its value can be:
#
#	PPPT(name): a pointer to a pointer to a pointer to the typedef "name"
#	PPT(name) : a pointer to a pointer to the typedef "name"
#	PT(name)  : a pointer to the typedef "name"
#	T(name)   : the typedef "name"
#	AAT(name) : an array of arrays of typedef "name"
#
# Examples:
#
#	as ; a_objectdir ; PPS(vnode) ; PPT(vnode_t)
#
# This entry means that in "struct as", the member "a_objectdir" which type
# is a "pointer to a pointer to a struct vnode" must have its type changed
# to a "pointer to a pointer to a typedef vnode_t".
#
#	tdelta_t ;; AAI(long long) ; AAT(hrtime_t)
#
# This entry has no member so "tdelta_t" is a typedef and this means the
# "typedef tdelta_t" which type is an "array of arrays of long long" must have
# its type changed to an "array of arrays of typedef hrtime_t".
#
# OUTPUT:
#
# The output is a C code which does:
#
# - include the file "../typedef_bug.h"
#
# - define an enum entry TYPE_* for each type used in a typedef bug entry;
#   TYPE_MAX is the last entry of this enum.
#
# - define the array type_name[TYPE_MAX]; this array has an entry for each
#   type TYPE_* which is the name of the type.
#
# - define the array type_cache[TYPE_MAX]; this array has an entry for each
#   type TYPE_*, its content is initially null and will be populated at
#   run-time.
#
# - define the array typedef_bugs[]; this array has an entry for each typedef
#   bug which is a C description of the text input entry.
#

require 5.005;
use strict;
use diagnostics;
use locale;

sub store_type($)
{
	my $t = shift;
	my $enum;

	if (! exists $::types{$t}) {
		$enum = uc $t;
		$enum =~ s/[^A-Z0-9_]/_/g;
		$::types{$t} = $enum;
	}
}

BEGIN
{
	local %::types = ();
	local @::bugs = ();
	local $::lineno = 0;
	local $::line = "";
}

sub process_line()
{
	my ($str, $mem, $cur, $tgt);
	my ($tgt_type, $tgt_spec);

	my $CUR_PATTERN = 'PPPS|PPS|PS|PI|PT|PF|S|I|AAI';
	my $TGT_PATTERN = 'PPPT|PPT|PT|T|AAT';

	$::lineno++;

	# remove newline, comment, starting and ending spaces
	chop;
	s/\#.*$//;
	s/^\s+//;
	s/\s+$//;

	if (! $_) {
		# empty line
		return;
	}

	$::line .= $_;

	if (/\\$/) {
		# line continuation
		chop $::line;
		$::line =~ s/\s+$//;
		return;
	}

	@_ = split /\s*;\s*/, $::line;
	if (scalar @_ != 4) {
		print STDERR "invalid line $::lineno\n";
		exit 1;
	}

	$::line = "";
	$str = $_[0];
	$mem = $_[1];
	$cur = $_[2];
	$tgt = $_[3];

	#
	# Process the type name.
	#
	# For example: proc => TYPE_PROC
	#
	store_type $str;
	$str = "TYPE_" . $::types{$str};

	#
	# Process the member name.
	#
	# For example: p_agenttp => "p_agenttp"
	#
	if ($mem) {
		$mem = '"' . $mem . '"';
	} else {
		$mem = 'MEMBER_NONE';
	}

	#
	# Process the current type description.
	#
	# For example: PS(vnode) => CUR_PS("vnode")
	#
	if (! ($cur =~ s/^($CUR_PATTERN)\(([^\)]+)\)$/CUR_$1(\"$2\")/)) {
		print STDERR "invalid current type $cur at line $::lineno\n";
		exit 1;
	}

	#
	# Process the target type description.
	#
	# For example: PT(vnode_t) => TGT_PT(VNODE_T)
	#
	if (! ($tgt =~ /^($TGT_PATTERN)\(([^\)]+)\)$/)) {
		print STDERR "invalid target type $tgt at line $::lineno\n";
		exit 1;
	}
	$tgt_spec = $1;
	chop $tgt_spec;		# remove the leading 'T'
	$tgt_type = $2;
	store_type $tgt_type;
	$tgt = 'TGT_' . $tgt_spec . 'T(' . $::types{$tgt_type} . ')';

	#
	# If the target type is not a typedef but a reference to a typedef
	# (for example a pointer to typedef). Then we need to define a type
	# for this reference.
	#
	# For example, if the target is PT(vnode_t) (a pointer to the typedef
	# vnode_t) then we will define VNODE_T_P. If the target is PPT(vnode_t)
	# then we will define VNODE_T_PP.
	#
	if ($tgt_spec) {
		store_type $tgt_type . '_' . $tgt_spec
	}

	# store the new bug entry
	push @::bugs, "$str, $mem, $cur, $tgt";
}

END
{
	my @stypes;
	my $str;

	@stypes = sort { $a cmp $b } keys %::types;

	print "/*\n" .
	    " * AUTOMATICALLY GENERATED FILE - DO NOT EDIT\n" .
	    " * This file has been generated by the script typedef_bug.pl\n" .
	    " */\n\n";

	#
	# include the header file
	#
	print "#include \"../typedef_bug.h\"\n\n";

	#
	# print the enum defining types (TYPE_*)
	#
	print "enum {\n";
	map { print "\tTYPE_$::types{$_},\n" } @stypes;
	print "\tTYPE_MAX\n};\n\n";

	#
	# print the array type_name[] containing the name of each type
	#
	$str = "char *type_name[TYPE_MAX] = {\n";
	map { $str .= "\t\"$_\",\n"; } @stypes;
	chop $str;
	chop $str;
	print "$str\n};\n\n";

	#
	# declare the array type_cache[] for storing the tdesc_t of each type
	#
	print "tdesc_t *type_cache[TYPE_MAX] = { NULL };\n\n";

	#
	# print the array typedef_bugs[] containing the bug entries
	#
	print "struct typedef_bug_desc typedef_bugs[] = {\n";
	map { print "\t{ $_ },\n" } sort { $a cmp $b } @::bugs;
	print "\t{ -1 }\n};\n\n";
}

#
# Main Loop
#
process_line;
