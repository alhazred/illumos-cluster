/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)st_parse.c	1.13	08/10/09 SMI"

/*
 * This file is a sewer.
 */

#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <strings.h>
#include <setjmp.h>
#include <ctype.h>
#include <sys/ctf.h>

#include "ctftools.h"
#include "memory.h"
#include "list.h"

#define	HASH(NUM)	((int)(NUM & (BUCKETS - 1)))
#define	BUCKETS		128

#define	TYPEPAIRMULT	10000
#define	MAKETYPEID(file, num)	((file) * TYPEPAIRMULT + num)
#define	TYPEFILE(tid)		((tid) / TYPEPAIRMULT)
#define	TYPENUM(tid)		((tid) % TYPEPAIRMULT)

#define	BASENAME_BUFSIZE	32

#define	expected(a, b, c) _expected(a, b, c, __LINE__)

static int faketypenumber = 100000000;

static tdesc_t *hash_table[BUCKETS];
static tdesc_t *name_table[BUCKETS];

static tdesc_t *tdp_void  = NULL;
static tdesc_t *tdp_voidp = NULL;

list_t *typedbitfldmems;

static void reset(void);
static jmp_buf	resetbuf;

static char *soudef(char *cp, stabtype_t type, tdesc_t **rtdp);
static void enumdef(char *cp, tdesc_t **rtdp);
static int compute_sum(const char *w);
static char *number(char *cp, int *n);
static char *name(char *cp, char **w);
static char *id(char *cp, int *h);
static char *whitesp(char *cp);
static void addhash(tdesc_t *tdp, int num);
static int tagadd(char *w, int h, tdesc_t *tdp);
static char *tdefdecl(char *cp, int h, tdesc_t **rtdp);
static char *intrinsic(char *cp, tdesc_t **rtdp);
static char *arraydef(char *cp, tdesc_t **rtdp);

extern int debug_level;
int debug_parse = DEBUG_PARSE;

/*PRINTFLIKE3*/
static void
parse_debug(int level, char *cp, char *fmt, ...)
{
	va_list ap;
	char buf[1024];
	char tmp[32];
	int i;

	if (level > debug_level || !debug_parse)
		return;

	if (cp != NULL) {
		for (i = 0; i < 30; i++) {
			if (cp[i] == '\0')
				break;
			if (!iscntrl(cp[i]))
				tmp[i] = cp[i];
		}
		tmp[i] = '\0';
		(void) snprintf(buf, sizeof (buf), "%s [cp='%s']\n", fmt, tmp);
	} else {
		strcpy(buf, fmt);
		strcat(buf, "\n");
	}

	va_start(ap, fmt);
	vadebug(level, buf, ap);
	va_end(ap);
}

/* Report unexpected syntax in stabs. */
static void
_expected(
	char *who,	/* what function, or part thereof, is reporting */
	char *what,	/* what was expected */
	char *where,	/* where we were in the line of input */
	int line)
{
	fprintf(stderr, "%s, expecting \"%s\" at \"%s\"\n", who, what, where);
	fprintf(stderr, "code line: %d, file %s\n", line,
	    (curhdr ? curhdr : "NO FILE"));
	reset();
}

/*ARGSUSED*/
void
parse_init(tdata_t *td)
{
	int i;

	for (i = 0; i < BUCKETS; i++) {
		hash_table[i] = NULL;
		name_table[i] = NULL;
	}

	if (typedbitfldmems != NULL) {
		list_free(typedbitfldmems, NULL, NULL);
		typedbitfldmems = NULL;
	}
}

void
parse_finish(tdata_t *td)
{
	td->td_nextid = ++faketypenumber;
}

static tdesc_t *
unres_new(int tid)
{
	tdesc_t *tdp;

	tdp = xcalloc(sizeof (*tdp));
	tdp->t_type = TYPEDEF_UNRES;
	tdp->t_id = tid;

	return (tdp);
}

char *
read_tid(char *cp, tdesc_t **tdpp)
{
	tdesc_t *tdp;
	int tid;

	cp = id(cp, &tid);

	assert(tid != 0);

	if (*cp == '=') {
		if (!(cp = tdefdecl(cp + 1, tid, &tdp)))
			return (NULL);
		if (tdp->t_id && tdp->t_id != tid) {
			tdesc_t *ntdp = xcalloc(sizeof (*ntdp));

			ntdp->t_type = TYPEDEF;
			ntdp->t_tdesc = tdp;
			tdp = ntdp;
		}
		addhash(tdp, tid);
	} else if ((tdp = lookup(tid)) == NULL)
		tdp = unres_new(tid);

	*tdpp = tdp;
	return (cp);
}

static iitype_t
parse_fun(char *cp, iidesc_t *ii)
{
	iitype_t iitype;
	tdesc_t *tdp;
	tdesc_t **args = NULL;
	char *w;
	int h;
	int nargs = 0;
	int va = 0;

	/*
	 * name:P		prototype
	 * name:F		global function
	 * name:f		static function
	 * name:YIf		template function instantiation
	 */
	switch (*cp++) {
	case 'P':
		iitype = II_NOT; /* not interesting */
		break;

	case 'F':
		iitype = II_GFUN;
		break;

	case 'f':
		iitype = II_SFUN;
		break;

	case 'Y':
		if (strncmp("If", cp, 2) != 0)
			expected("parse_fun", "YIf", cp - 1);
		cp += 2;

		/* skipping template actual list */
		while (*cp != '@') {
			cp = name(cp, &w);
			free(w);
			cp = id(cp, &h);
			cp++;
		}
		cp++;

		if (strncmp(";g;", cp, 3) != 0)
			expected("parse_fun", ";g;", cp - 1);
		cp += 3;

		/* skipping template mangled name */
		cp = name(cp, &w);
		free(w);

		/*
		 * The stabs documentation indicates that we should have
		 * an 'F', however Sun Studio sometimes generates an 'f'.
		 */
		if (*cp != 'F' && *cp != 'f')
			expected("parse_fun", "[Ff]", cp);
		cp++;

		iitype = II_GFUN;
		break;

	default:
		expected("parse_fun", "[PfFY]", cp - 1);
	}

	if (!(cp = read_tid(cp, &tdp)))
		return (-1);

	if (*cp)
		args = xmalloc(sizeof (tdesc_t *) * FUNCARG_DEF);

	while (*cp && *++cp) {
		if (*cp == '0') {
			va = 1;
			continue;
		}

		nargs++;
		if (nargs > FUNCARG_DEF)
			args = xrealloc(args, sizeof (tdesc_t *) * nargs);
		if (!(cp = read_tid(cp, &args[nargs - 1])))
			return (-1);
		/*
		 * C++ functions with default arguments have these arguments
		 * specified after the type. This is an integer or the name
		 * of a function.
		 */
		while (*cp && (*cp != ';'))
			cp++;	/* skip default arguments */
	}

	ii->ii_type = iitype;
	ii->ii_dtype = tdp;
	ii->ii_nargs = nargs;
	ii->ii_args = args;
	ii->ii_vargs = va;

	parse_debug(3, "parse_fun", "new function %s with %d args\n",
	    ii->ii_name, ii->ii_nargs);

	return (iitype);
}

static iitype_t
parse_sym(char *cp, iidesc_t *ii)
{
	tdesc_t *tdp;
	iitype_t iitype;
	char *gname;

	/*
	 * name:G		global variable
	 * name:S		static variable
	 * name:l		C++: literal
	 * name:E		C++: external data
	 * name:U		C++: class declaration
	 *
	 */
	switch (*cp++) {
	case 'G':
		iitype = II_GVAR;
		break;
	case 'S':
		iitype = II_SVAR;
		break;
	case 'p':
		iitype = II_PSYM;
		break;
	case '(':
		cp--;
		/*FALLTHROUGH*/
	case 'r':
	case 'V':
	case 'l':
	case 'E':
		iitype = II_NOT; /* not interesting */
		break;
	case 'U':
		iitype = II_SOU;
		break;
	default:
		expected("parse_sym", "[GlprSVUE(]", cp - 1);
	}

	/*
	 * Globalized symbols should appear with the stab 'S' (static). However
	 * Sun Studio sometimes makes them appear with the stabs 'G' (global),
	 * so we have to check global names in both cases.
	 */
	if (noglobal && (iitype == II_GVAR || iitype == II_SVAR) &&
	    (strncmp(ii->ii_name, global_prefix, global_prefix_len) == 0)) {
		/*
		 * This symbol was globalized, remove the global prefix
		 * from its name to restore the original name.
		 */
		iitype = II_SVAR;
		gname = ii->ii_name;
		ii->ii_name = xstrdup(ii->ii_name + global_prefix_len);
		parse_debug(4, NULL, "undo globalization stabs %s "
		    "=> %s", gname, ii->ii_name);
		free(gname);
	}

	if (iitype == II_SOU) {
		int tid;

		/* C++: class declaration (U) */
		cp = id(cp, &tid);
		tdp = lookup(tid);
		if (tdp == NULL) {
			tdp = xcalloc(sizeof (*tdp));
			tdp->t_name = ii->ii_name ? xstrdup(ii->ii_name) : NULL;
			tdp->t_type = FORWARD;
			addhash(tdp, tid);
		} else {
			/* we can have several 'U' statements */
			parse_debug(4, NULL, "parse_sym: type %d already "
			    "defines %s", tid,
			    tdp->t_name ? tdp->t_name : "(anon)");
			assert(streq(ii->ii_name, tdp->t_name));
		}
	} else {
		if (!(cp = read_tid(cp, &tdp)))
			return (-1);
	}

	ii->ii_type = iitype;
	ii->ii_dtype = tdp;

	return (iitype);
}

static iitype_t
parse_type(char *cp, iidesc_t *ii)
{
	tdesc_t *tdp, *ntdp;
	int tid;

	if (*cp++ != 't')
		expected("parse_type", "t (type)", cp - 1);

	cp = id(cp, &tid);
	if ((tdp = lookup(tid)) == NULL) {
		if (*cp++ != '=')
			expected("parse_type", "= (definition)", cp - 1);

		(void) tdefdecl(cp, tid, &tdp);

		if (tdp->t_id == tid) {
			assert(tdp->t_type != TYPEDEF);
			assert(!lookup(tdp->t_id));

			if (!streq(tdp->t_name, ii->ii_name)) {
				ntdp = xcalloc(sizeof (*ntdp));
				ntdp->t_name = xstrdup(ii->ii_name);
				ntdp->t_type = TYPEDEF;
				ntdp->t_tdesc = tdp;
				tdp->t_id = faketypenumber++;
				tdp = ntdp;
			}
		} else if (tdp->t_id == 0) {
			assert(tdp->t_type == FORWARD ||
			    tdp->t_type == INTRINSIC);

			if (tdp->t_name && !streq(tdp->t_name, ii->ii_name)) {
				ntdp = xcalloc(sizeof (*ntdp));
				ntdp->t_name = xstrdup(ii->ii_name);
				ntdp->t_type = TYPEDEF;
				ntdp->t_tdesc = tdp;
				tdp->t_id = faketypenumber++;
				tdp = ntdp;
			}
		} else if (tdp->t_id != tid) {
			ntdp = xcalloc(sizeof (*ntdp));
			ntdp->t_name = xstrdup(ii->ii_name);
			ntdp->t_type = TYPEDEF;
			ntdp->t_tdesc = tdp;
			tdp = ntdp;
		}

		if (tagadd(ii->ii_name, tid, tdp) < 0)
			return (-1);
	}

	ii->ii_type = II_TYPE;
	ii->ii_dtype = tdp;

	if ((tdp_void == NULL) && (strcmp(ii->ii_name, "void") == 0)) {
		/* keep a reference to the void type */
		parse_debug(4, NULL, "void type is %d", tdp->t_id);
		tdp_void = tdp;
	}

	return (II_TYPE);
}

static iitype_t
parse_sou(char *cp, iidesc_t *idp)
{
	tdesc_t *rtdp;
	int tid;

	if (*cp++ != 'T')
		expected("parse_sou", "T (sou)", cp - 1);

	cp = id(cp, &tid);
	if (*cp++ != '=')
		expected("parse_sou", "= (definition)", cp - 1);

	parse_debug(1, NULL, "parse_sou: declaring '%s'", idp->ii_name ?
	    idp->ii_name : "(anon)");
	if ((rtdp = lookup(tid)) != NULL) {
		if (idp->ii_name != NULL) {
			if (rtdp->t_name != NULL &&
			    strcmp(rtdp->t_name, idp->ii_name) != 0) {
				tdesc_t *tdp;

				tdp = xcalloc(sizeof (*tdp));
				tdp->t_name = xstrdup(idp->ii_name);
				tdp->t_type = TYPEDEF;
				tdp->t_tdesc = rtdp;
				addhash(tdp, tid); /* for *(x,y) types */
				parse_debug(3, NULL, "    %s defined as %s(%d)",
				    idp->ii_name, tdesc_name(rtdp), tid);
			} else if (rtdp->t_name == NULL) {
				rtdp->t_name = xstrdup(idp->ii_name);
				addhash(rtdp, tid);
			}
		}
	} else {
		rtdp = xcalloc(sizeof (*rtdp));
		rtdp->t_name = idp->ii_name ? xstrdup(idp->ii_name) : NULL;
		addhash(rtdp, tid);
	}

	switch (*cp++) {
	case 's':
		(void) soudef(cp, STRUCT, &rtdp);
		break;
	case 'u':
		(void) soudef(cp, UNION, &rtdp);
		break;
	case 'Y':
		/*
		 * C++ Specification:
		 *
		 *  Yc: class
		 *  YC: nested class
		 *  Ys: structure
		 *  YS: nested structure
		 *  Yu: union
		 *  YU: nested union
		 *  Yn: namespace
		 *
		 */
		switch (*cp++) {
		case 'c':
		case 'C':
			(void) soudef(cp, CPP_CLASS, &rtdp);
			break;
		case 'u':
		case 'U':
			(void) soudef(cp, CPP_UNION, &rtdp);
			break;
		case 's':
		case 'S':
			(void) soudef(cp, CPP_STRUCT, &rtdp);
			break;
		case 'n':
			/* we handle a namespace like a struct */
			rtdp->t_size = 0;
			rtdp->t_type = STRUCT;
			break;
		default:
			expected("parse_sou", "<tag type Y[cCsSuUn]>", cp - 2);
			break;
		}
		break;
	case 'e':
		enumdef(cp, &rtdp);
		break;
	default:
		expected("parse_sou", "<tag type s/u/e/Y>", cp - 1);
		break;
	}

	idp->ii_type = II_SOU;
	idp->ii_dtype = rtdp;
	return (II_SOU);
}

static iitype_t
parse_cpp(char *cp, iidesc_t *idp)
{
	tdesc_t *rtdp;
	iitype_t iitype;
	stabtype_t stype;
	int tid;
	char *w;

	if (*cp++ != 'Y')
		expected("parse_cpp", "<tag type Y>", cp - 1);

	/*
	 * The Y symbol descriptors are the C++ specific symbol descriptors.
	 *
	 * name:YR		RTTI
	 *
	 * name:YTc		class template
	 * name:YTC		nested class template
	 * name:YTs		struct template
	 * name:YTS		nested struct template
	 *
	 * name:YIc		instantiated template from a class
	 * name:YIC		instantiated template from a nested class
	 * name:YIs		instantiated template from a struct
	 * name:YIS		instantiated template from a nested struct
	 */
	switch (*cp++) {
	case 'R':
		iitype = II_NOT; /* not interesting */
		break;

	case 'T':
		/* we need to define this type name for differential mangling */
		iitype = II_SOU;
		if (*cp != 'c' && *cp != 'C' && *cp != 's' && *cp != 'S') {
			expected("parse_cpp", "<tag type YT[cCsS]>", cp - 3);
		}
		cp++;
		cp = id(cp, &tid);
		assert(lookup(tid) == NULL);
		rtdp = xcalloc(sizeof (*rtdp));
		rtdp->t_name = xstrdup(idp->ii_name);
		rtdp->t_size = 0;
		rtdp->t_type = STRUCT;
		addhash(rtdp, tid);
		idp->ii_dtype = rtdp;
		break;

	case 'I':
		iitype = II_SOU;
		switch (*cp++) {
		case 'c':
		case 'C':
			stype = CPP_CLASS;
			break;
		case 's':
		case 'S':
			stype = CPP_STRUCT;
			break;
		default:
			expected("parse_cpp", "<tag type YI[cCsS]>", cp - 3);
		}
		cp = id(cp, &tid);

		if (strncmp(";@;", cp, 3) != 0)
			expected("parse_cpp", ";@;", cp - 1);
		cp += 3;

		parse_debug(1, NULL, "parse_cpp: declaring '%s'",
		    idp->ii_name ? idp->ii_name : "(anon)");

		/*
		 * The type can already exist because it can have been
		 * previously declared with a class declaration (U).
		 */
		rtdp = lookup(tid);
		if (rtdp == NULL) {
			rtdp = xcalloc(sizeof (*rtdp));
			rtdp->t_name = idp->ii_name ? xstrdup(idp->ii_name) :
				NULL;
			addhash(rtdp, tid);
		}

		/*
		 * We need to parse actual arguments because they can contain
		 * declarations of some new types.
		 */
		while (*cp != '@') {
			tdesc_t *tdp;

			cp = name(cp, &w);
			free(w);
			if (!(cp = read_tid(cp, &tdp)))
				return (-1);
			cp++;
		}
		cp++;

		if (strncmp(";g;", cp, 3) != 0)
			expected("parse_cpp", ";g;", cp - 1);
		cp += 3;

		(void) soudef(cp, stype, &rtdp);
		idp->ii_dtype = rtdp;
		break;

	default:
		expected("parse_cpp", "<tag type Y[RTI]>", cp - 2);
	}

	idp->ii_type = iitype;
	return (iitype);
}

int
parse_stab(stab_t *stab, char *cp, iidesc_t **iidescp)
{
	iidesc_t *ii = NULL;
	iitype_t (*parse)(char *, iidesc_t *);
	int rc;

	/*
	 * set up for reset()
	 */
	if (setjmp(resetbuf))
		return (-1);

	cp = whitesp(cp);
	ii = iidesc_new(NULL);
	cp = name(cp, &ii->ii_name);

	switch (stab->n_type) {
	case N_FUN:
		parse = parse_fun;
		break;

	case N_LSYM:
	case N_ESYM:
	case N_ISYM:
		if (*cp == 't')
			parse = parse_type;
		else if (*cp == 'T')
			parse = parse_sou;
		else if (*cp == 'Y')
			parse = parse_cpp;
		else
			parse = parse_sym;
		break;

	case N_GSYM:
	case N_LCSYM:
	case N_PSYM:
	case N_ROSYM:
	case N_RSYM:
	case N_STSYM:
		if (*cp == 'Y')
			parse = parse_cpp;
		else
			parse = parse_sym;
		break;
	default:
		parse_debug(1, cp, "Unknown stab type %#x", stab->n_type);
		bzero(&resetbuf, sizeof (resetbuf));
		return (-1);
	}

	rc = parse(cp, ii);
	bzero(&resetbuf, sizeof (resetbuf));

	if (rc < 0 || ii->ii_type == II_NOT) {
		iidesc_free(ii, NULL);
		return (rc);
	}

	*iidescp = ii;

	return (1);
}

/*
 * Check if we have this node in the hash table already
 */
tdesc_t *
lookup(int h)
{
	int bucket = HASH(h);
	tdesc_t *tdp = hash_table[bucket];

	while (tdp != NULL) {
		if (tdp->t_id == h)
			return (tdp);
		tdp = tdp->t_hash;
	}
	return (NULL);
}

static char *
whitesp(char *cp)
{
	char c;

	for (c = *cp++; isspace(c); c = *cp++)
		;
	--cp;
	return (cp);
}

static char *
name(char *cp, char **w)
{
	char *new, *orig, *end, c;
	int len;
	int h;
	tdesc_t *ptdp = NULL;

	orig = cp;
	c = *cp++;
	if (c == ':')
		*w = NULL;
	else if (isalpha(c) || strchr("_.$", c)) {

		for (c = *cp++; isalnum(c) || strchr(" _.$", c); c = *cp++)
			;
		end = cp;
		len = end - orig;

		if (c == '(') {
			/*
			 * Some C++ stabs use differential mangling:
			 *
			 * class foo::bar  => __1nDfooDbar_
			 * foo::bar::buz() => __1cDfooDbarDbuz6M_v_
			 * differential    => ...c........Dbuz6M_v.
			 * differential    => cDbuz6M_v
			 *
			 * If __1nDfooDbar_ is type (0,20), the differential
			 * name for __1cDfooDbarDbuz6M_v_ is cDbuz6M_v(0,20)
			 */
			cp = id(cp - 1, &h);
			c = *cp++;
			ptdp = lookup(h);
			assert(ptdp != NULL);
			assert(ptdp->t_name != NULL);
			/*
			 * The first character is holding the type of this
			 * mangled name and the remaining part of the name is
			 * to be appended to the base mangled name.
			 */
			len += strlen(ptdp->t_name) - 1;
			parse_debug(4, orig, "differential mangling: base=%s",
			    ptdp->t_name);
		}

		if (c != ':') {
			reset();
		}
		new = xmalloc(len);

		if (ptdp != NULL) {
			/*
			 * If differential mangling, prepend mangled base name,
			 * change the type and move to the last character ('_').
			 * Then append the difference and '_'.
			 */
			(void) strcpy(new, ptdp->t_name);
			new[3] = *orig++;
			new += strlen(new) - 1;
			assert(*new == '_');
		}

		while (orig < end - 1)
			*new++ = *orig++;

		if (ptdp != NULL)
			*new++ = '_';
		*new = '\0';
		*w = new - (len - 1);

		if (ptdp != NULL) {
			parse_debug(4, NULL, "differential mangling: "
			    "result=%s len=%d", *w, len);
		}
	} else
		reset();

	return (cp);
}

/*
 * Read the name of a member of C++ class or struct. The name is always
 * prefixed with a ppp-code indicating if the member is public, private or
 * protected and the name of the member is available after the ppp-code.
 *
 * If the class or struct is not anonymous then the member name after the
 * ppp-code is differentially mangled without any type number because it
 * implicitly references the name of the class or struct it is part of.
 *
 * For example, the stab for:
 *
 * class bar {
 *	int foo;
 * };
 *
 * will be: __1nDbar_:T(0,19)=Yc4bar;;AcDfoo:(0,3),0,32;
 *
 * Here the name of the member "foo" appears as "AcDfoo" where "A" is the
 * ppp-code and "cDfoo" is the differential name compared with the name of
 * the class. So the complete differential name is "cDfoo(0,19)" hence the
 * full name of the member is __1cDbarDfoo_ (i.e. bar::foo).
 *
 * However we just want the short name of the member (i.e. foo) and not its
 * full name (i.e. bar::foo) so we are not going to rebuild the full mangled
 * name but we will just read the short name. The name of the member is like
 * this:      AcDfoo
 *            ^^^\ /
 *            ||| |
 *            ||| +- field name
 *            ||+--- field name size (encoded number)
 *            |+---- mangling type
 *            +----- ppp-code
 *
 * So we are going to skip the ppp-code, the mangling type and the field name
 * size and then just read the field name.
 */
static char *
member_name(tdesc_t *tdp, char *cp, char **w)
{
	/* always skip the ppp-code */
	cp++;

	if (tdp->t_name != NULL) {
		/*
		 * This is not an anonymous structure: skip mangling
		 * type and encoded number. The encoded number indicates
		 * the length of the string and uses a base 26: a lower
		 * case letter indicates that more digits are to follow,
		 * an upper case letter is the last digit of the number.
		 */
		cp++;
		while (('a' <= *cp) && (*cp <= 'z'))
			cp++;
		if ((*cp < 'A') || (*cp > 'Z'))
			expected("member_name", "[A-Z]", cp);
		cp++;
	}

	return (name(cp, w));
}

static char *
number(char *cp, int *n)
{
	char *next;

	*n = (int)strtol(cp, &next, 10);
	if (next == cp)
		expected("number", "<number>", cp);
	return (next);
}

static char *
id(char *cp, int *h)
{
	int n1, n2;

	if (*cp == '(') {	/* SunPro style */
		cp++;
		cp = number(cp, &n1);
		if (*cp++ != ',')
			expected("id", ",", cp - 1);
		cp = number(cp, &n2);
		if (*cp++ != ')')
			expected("id", ")", cp - 1);
		*h = MAKETYPEID(n1, n2);
	} else if (isdigit(*cp)) { /* gcc style */
		cp = number(cp, &n1);
		*h = n1;
	} else {
		expected("id", "(/0-9", cp);
	}
	return (cp);
}

static int
tagadd(char *w, int h, tdesc_t *tdp)
{
	tdesc_t *otdp;

	tdp->t_name = w ? xstrdup(w) : NULL;
	if (!(otdp = lookup(h)))
		addhash(tdp, h);
	else if (otdp != tdp) {
		warning("duplicate entry\n");
		warning("  old: %s %d (%d,%d)\n", tdesc_name(otdp),
		    otdp->t_type, TYPEFILE(otdp->t_id), TYPENUM(otdp->t_id));
		warning("  new: %s %d (%d,%d)\n", tdesc_name(tdp),
		    tdp->t_type, TYPEFILE(tdp->t_id), TYPENUM(tdp->t_id));
		return (-1);
	}

	return (0);
}

static char *
tdefdecl_fun(char *cp, int h, tdesc_t **rtdp, int proto)
{
	tdesc_t *ntdp;

	cp = tdefdecl(cp, h, &ntdp);
	*rtdp = xcalloc(sizeof (**rtdp));
	(*rtdp)->t_type = FUNCTION;
	(*rtdp)->t_size = 0;
	(*rtdp)->t_id = h;
	(*rtdp)->t_fndef = xcalloc(sizeof (fndef_t));
	/*
	 * The 6.1 compiler will sometimes generate incorrect stabs for
	 * function pointers (it'll get the return type wrong).  This
	 * causes merges to fail.  We therefore treat function pointers
	 * as if they all point to functions that return int.  When
	 * 4432549 is fixed, the lookupname() call below should be
	 * replaced with `ntdp'.
	 */
	(*rtdp)->t_fndef->fn_ret = lookupname("int");
	if (proto) {
		/* parse prototype information */
		while (*cp != '#')
			cp = read_tid(cp, &ntdp);
		cp++;
	}

	return (cp);
}

static char *
tdefdecl_ptr(char *cp, int h, tdesc_t **rtdp)
{
	tdesc_t *ntdp = NULL;

	cp = tdefdecl(cp, h, &ntdp);
	if (ntdp == NULL)
		expected("tdefdecl_ptr", "id", cp);

	if (!ntdp->t_id)
		ntdp->t_id = faketypenumber++;

	*rtdp = xcalloc(sizeof (**rtdp));
	(*rtdp)->t_type = POINTER;
	(*rtdp)->t_size = 0;
	(*rtdp)->t_id = h;
	(*rtdp)->t_tdesc = ntdp;

	if ((tdp_voidp == NULL) && (ntdp == tdp_void)) {
		/* keep a reference to the void* type */
		parse_debug(4, NULL, "tdefdecl: void* type is %d",
		    (*rtdp)->t_id);
		tdp_voidp = *rtdp;
	}

	return (cp);
}

static char *
tdefdecl(char *cp, int h, tdesc_t **rtdp)
{
	tdesc_t *ntdp;
	char *w;
	int c, h2;
	char type;

	parse_debug(3, cp, "tdefdecl h=%d", h);

	/* Type codes */
	switch (type = *cp) {
	case 'b': /* integer */
	case 'R': /* fp */
		cp = intrinsic(cp, rtdp);
		break;
	case '(': /* equiv to another type */
		cp = id(cp, &h2);
		ntdp = lookup(h2);

		if (ntdp != NULL && *cp == '=') {
			if (ntdp->t_type == FORWARD && *(cp + 1) == 'x') {
				/*
				 * The 6.2 compiler, and possibly others, will
				 * sometimes emit the same stab for a forward
				 * declaration twice.  That is, "(1,2)=xsfoo:"
				 * will sometimes show up in two different
				 * places.  This is, of course, quite fun.  We
				 * want CTF to work in spite of the compiler,
				 * so we'll let this one through.
				 */
				char *c2 = cp + 2;
				char *nm;

				if (!strchr("sue", *c2++)) {
					expected("tdefdecl/x-redefine", "[sue]",
					    c2 - 1);
				}

				c2 = name(c2, &nm);
				if (strcmp(nm, ntdp->t_name) != 0) {
					terminate("Stabs error: Attempt to "
					    "redefine type (%d,%d) as "
					    "something else: %s\n",
					    TYPEFILE(h2), TYPENUM(h2),
					    c2 - 1);
				}
				free(nm);

				h2 = faketypenumber++;
				ntdp = NULL;
			} else {
				terminate("Stabs error: Attempting to "
				    "redefine type (%d,%d)\n", TYPEFILE(h2),
				    TYPENUM(h2));
			}
		}

		if (ntdp == NULL) {  /* if that type isn't defined yet */
			if (*cp != '=') {
				/* record it as unresolved */
				parse_debug(3, NULL, "tdefdecl unres type %d",
				    h2);
				*rtdp = calloc(sizeof (**rtdp), 1);
				(*rtdp)->t_type = TYPEDEF_UNRES;
				(*rtdp)->t_id = h2;
				break;
			} else
				cp++;

			/* define a new type */
			cp = tdefdecl(cp, h2, rtdp);
			if ((*rtdp)->t_id && (*rtdp)->t_id != h2) {
				ntdp = calloc(sizeof (*ntdp), 1);
				ntdp->t_type = TYPEDEF;
				ntdp->t_tdesc = *rtdp;
				*rtdp = ntdp;
			}

			addhash(*rtdp, h2);

		} else { /* that type is already defined */
			if (ntdp->t_type != TYPEDEF || ntdp->t_name != NULL) {
				*rtdp = ntdp;
			} else {
				parse_debug(3, NULL,
				    "No duplicate typedef anon for ref");
				*rtdp = ntdp;
			}
		}
		break;
	case 'Y': /* C++: pointer to class member */
		cp++;
		switch (*cp++) {
		case 'D': /* pointer to a class data member */
			/* skip class type */
			cp = id(cp, &h2);
			cp = tdefdecl_ptr(cp, h, rtdp);
			break;
		case 'M': /* pointer to a class member function */
			if (*cp == 'K')
				/* const function */
				cp++;
			if (*cp == 'B')
				/* volatile function */
				cp++;

			/* skip class type */
			cp = id(cp, &h2);

			/*
			 * We have to parse the content pointed by the pointer:
			 * this is a function with prototype info. Then we
			 * create a pointer to that function.
			 */
			ntdp = NULL;
			cp = tdefdecl_fun(cp, faketypenumber++, &ntdp, 1);
			if (ntdp == NULL)
				expected("tdefdecl/YM", "id", cp);

			assert(ntdp->t_id);
			*rtdp = xcalloc(sizeof (**rtdp));
			(*rtdp)->t_type = POINTER;
			(*rtdp)->t_size = 0;
			(*rtdp)->t_id = h;
			(*rtdp)->t_tdesc = ntdp;
			break;
		default:
			expected("tdefdecl/Y", "[MD]", cp - 1);
		}
		break;
	case '&': /* C++: Reference */
	case '*':
		cp = tdefdecl_ptr(cp + 1, h, rtdp);
		break;
	case 'g': /* C++: function with prototype info  */
		cp = tdefdecl_fun(cp + 1, h, rtdp, 1);
		break;
	case 'f':
		cp = tdefdecl_fun(cp + 1, h, rtdp, 0);
		break;
	case 'a':
	case 'z':
		cp++;
		if (*cp++ != 'r')
			expected("tdefdecl/[az]", "r", cp - 1);
		*rtdp = xcalloc(sizeof (**rtdp));
		(*rtdp)->t_type = ARRAY;
		(*rtdp)->t_id = h;
		cp = arraydef(cp, rtdp);
		break;
	case 'x':
		c = *++cp;
		if (c != 's' && c != 'u' && c != 'e')
			expected("tdefdecl/x", "[sue]", cp - 1);
		cp = name(cp + 1, &w);

		ntdp = xcalloc(sizeof (*ntdp));
		ntdp->t_type = FORWARD;
		ntdp->t_name = w;
		/*
		 * We explicitly don't set t_id here - the caller will do it.
		 * The caller may want to use a real type ID, or they may
		 * choose to make one up.
		 */

		*rtdp = ntdp;
		break;

	case 'B': /* volatile */
		cp = tdefdecl(cp + 1, h, &ntdp);

		if (!ntdp->t_id)
			ntdp->t_id = faketypenumber++;

		*rtdp = xcalloc(sizeof (**rtdp));
		(*rtdp)->t_type = VOLATILE;
		(*rtdp)->t_size = 0;
		(*rtdp)->t_tdesc = ntdp;
		(*rtdp)->t_id = h;
		break;

	case 'k': /* const */
		cp = tdefdecl(cp + 1, h, &ntdp);

		if (!ntdp->t_id)
			ntdp->t_id = faketypenumber++;

		*rtdp = xcalloc(sizeof (**rtdp));
		(*rtdp)->t_type = CONST;
		(*rtdp)->t_size = 0;
		(*rtdp)->t_tdesc = ntdp;
		(*rtdp)->t_id = h;
		break;

	case 'K': /* restricted */
		cp = tdefdecl(cp + 1, h, &ntdp);

		if (!ntdp->t_id)
			ntdp->t_id = faketypenumber++;

		*rtdp = xcalloc(sizeof (**rtdp));
		(*rtdp)->t_type = RESTRICT;
		(*rtdp)->t_size = 0;
		(*rtdp)->t_tdesc = ntdp;
		(*rtdp)->t_id = h;
		break;

	case 'u':
	case 's':
		cp++;

		*rtdp = xcalloc(sizeof (**rtdp));
		(*rtdp)->t_name = NULL;
		cp = soudef(cp, (type == 'u') ? UNION : STRUCT, rtdp);
		break;
	default:
		expected("tdefdecl", "<type code>", cp);
	}
	return (cp);
}

static char *
intrinsic(char *cp, tdesc_t **rtdp)
{
	intr_t *intr = xcalloc(sizeof (intr_t));
	tdesc_t *tdp;
	int width, fmt, i;

	switch (*cp++) {
	case 'b':
		intr->intr_type = INTR_INT;
		if (*cp == 's')
			intr->intr_signed = 1;
		else if (*cp != 'u')
			expected("intrinsic/b", "[su]", cp);
		cp++;

		if (strchr("cbv", *cp))
			intr->intr_iformat = *cp++;

		cp = number(cp, &width);
		if (*cp++ != ';')
			expected("intrinsic/b", "; (post-width)", cp - 1);

		cp = number(cp, &intr->intr_offset);
		if (*cp++ != ';')
			expected("intrinsic/b", "; (post-offset)", cp - 1);

		cp = number(cp, &intr->intr_nbits);
		break;

	case 'R':
		intr->intr_type = INTR_REAL;
		for (fmt = 0, i = 0; isdigit(*(cp + i)); i++)
			fmt = fmt * 10 + (*(cp + i) - '0');

		if (fmt < 1 || fmt > CTF_FP_MAX)
			expected("intrinsic/R", "number <= CTF_FP_MAX", cp);

		intr->intr_fformat = fmt;
		cp += i;

		if (*cp++ != ';')
			expected("intrinsic/R", ";", cp - 1);
		cp = number(cp, &width);

		intr->intr_nbits = width * 8;
		break;
	}

	tdp = xcalloc(sizeof (*tdp));
	tdp->t_type = INTRINSIC;
	tdp->t_size = width;
	tdp->t_name = NULL;
	tdp->t_intr = intr;
	parse_debug(3, NULL, "intrinsic: size=%d", width);
	*rtdp = tdp;

	return (cp);
}

static tdesc_t *
bitintrinsic(tdesc_t *template, int nbits)
{
	tdesc_t *newtdp = xcalloc(sizeof (tdesc_t));

	newtdp->t_name = xstrdup(template->t_name);
	newtdp->t_id = faketypenumber++;
	newtdp->t_type = INTRINSIC;
	newtdp->t_size = template->t_size;
	newtdp->t_intr = xmalloc(sizeof (intr_t));
	bcopy(template->t_intr, newtdp->t_intr, sizeof (intr_t));
	newtdp->t_intr->intr_nbits = nbits;

	return (newtdp);
}

static char *
offsize(char *cp, mlist_t *mlp)
{
	int offset, size;

	if (*cp == ',')
		cp++;
	cp = number(cp, &offset);
	if (*cp++ != ',')
		expected("offsize/2", ",", cp - 1);
	cp = number(cp, &size);
	if (*cp++ != ';')
		expected("offsize/3", ";", cp - 1);
	mlp->ml_offset = offset;
	mlp->ml_size = size;
	return (cp);
}

static tdesc_t *
find_intrinsic(tdesc_t *tdp)
{
	for (;;) {
		switch (tdp->t_type) {
		case TYPEDEF:
		case VOLATILE:
		case CONST:
		case RESTRICT:
			tdp = tdp->t_tdesc;
			break;

		default:
			return (tdp);
		}
	}
}

static char *
soudef(char *cp, stabtype_t type, tdesc_t **rtdp)
{
	mlist_t *mlp, **prev;
	char *w;
	char buf[BASENAME_BUFSIZE];
	int n;
	int h;
	int size;
	tdesc_t *tdp, *itdp;

	cp = number(cp, &size);
	(*rtdp)->t_size = size;
	(*rtdp)->t_type = type; /* s or u */

	prev = &((*rtdp)->t_members);

	if ((type == CPP_CLASS) || (type == CPP_UNION) ||
	    (type == CPP_STRUCT)) {

		if (type == CPP_UNION)
			(*rtdp)->t_type = UNION;
		else
			/* we handle a class like a struct */
			(*rtdp)->t_type = STRUCT;

		parse_debug(4, cp, "soudef: starting class");

		/* skip class name */
		while ((*cp++) != ';')
			;

		/*
		 * Parse base classes: base classes of a derived class are
		 * listed after the class name. Each base name starts with the
		 * ppp-code indicating if the inheritance is private, protected
		 * or public.
		 *
		 * A base class is represented as a member of the struct
		 * representing the derived class. This member has the type of
		 * the base class, and its name is "_base_n" where n is the
		 * index of the class in the base class list.
		 */
		n = 1;
		while (*cp != ';') {

			mlp = xcalloc(sizeof (*mlp));
			*prev = mlp;

			/* skip ppp-code */
			cp++;

			/* read offset (in bytes) and convert to bits */
			cp = number(cp, &mlp->ml_offset);
			mlp->ml_offset *= 8;

			/* read type of the base class */
			cp = id(cp, &h);
			tdp = lookup(h);

			assert(tdp != NULL);
			assert(tdp->t_name != NULL);

			parse_debug(4, NULL, "soudef: base class %d: %s\n",
			    n, tdp->t_name);

			(void) snprintf(buf, BASENAME_BUFSIZE, "_base_%d", n);
			mlp->ml_name = xstrdup(buf);
			mlp->ml_size = tdp->t_size * 8;

			/* a base class is a defined class or struct */
			itdp = find_intrinsic(tdp);
			assert((itdp->t_type != INTRINSIC) &&
			    (itdp->t_type != TYPEDEF_UNRES));
			mlp->ml_type = tdp;

			/* cp is now pointing to next base */
			prev = &mlp->ml_next;
			n++;
		}

		/* skip ';' */
		cp++;
	}

	/*
	 * An '@' here indicates a bitmask follows.   This is so the
	 * compiler can pass information to debuggers about how structures
	 * are passed in the v9 world.  We don't need this information
	 * so we skip over it.
	 */
	if (cp[0] == '@') {
		cp += 3;
	}

	parse_debug(3, cp, "soudef: %s size=%d", tdesc_name(*rtdp),
	    (*rtdp)->t_size);

	/* now fill up the fields */
	while ((*cp != '\0') && (*cp != ';')) { /* signifies end of fields */
		mlp = xcalloc(sizeof (*mlp));
		*prev = mlp;

		if ((type == CPP_CLASS) || (type == CPP_STRUCT) ||
		    (type == CPP_UNION)) {
			cp = member_name(*rtdp, cp, &w);
		} else {
			cp = name(cp, &w);
		}

		mlp->ml_name = w;
		cp = id(cp, &h);
		/*
		 * find the tdesc struct in the hash table for this type
		 * and stick a ptr in here
		 */
		tdp = lookup(h);
		if (tdp == NULL) { /* not in hash list */
			parse_debug(3, NULL, "      defines %s (%d)", w, h);
			if (*cp++ != '=') {
				tdp = unres_new(h);
				parse_debug(3, NULL,
				    "      refers to %s (unresolved %d)",
				    (w ? w : "anon"), h);
			} else {
				cp = tdefdecl(cp, h, &tdp);

				if (tdp->t_id && tdp->t_id != h) {
					tdesc_t *ntdp = xcalloc(sizeof (*ntdp));

					ntdp->t_type = TYPEDEF;
					ntdp->t_tdesc = tdp;
					tdp = ntdp;
				}

				addhash(tdp, h);
				parse_debug(4, cp,
				    "     soudef now looking at    ");
				cp++;
			}
		} else {
			parse_debug(3, NULL, "      refers to %s (%d, %s)",
			    w ? w : "anon", h, tdesc_name(tdp));

			/* C++ can have multiple forward references */
			if (*cp++ == '=') {
				tdesc_t *ntdp;

				cp = tdefdecl(cp, h, &ntdp);
				assert((ntdp->t_type == FORWARD) &&
				    (streq(tdp->t_name, ntdp->t_name)));
				free(ntdp);
				parse_debug(4, cp,
				    "     soudef (forward) now looking at    ");
			}
		}

		cp = offsize(cp, mlp);

		itdp = find_intrinsic(tdp);
		if (itdp->t_type == INTRINSIC) {
			if (mlp->ml_size != itdp->t_intr->intr_nbits) {
				parse_debug(4, cp, "making %d bit intrinsic "
				    "from %s", mlp->ml_size, tdesc_name(itdp));
				/*
				 * Because of bug 4818400 with Sun Studio 8,
				 * all volatile types will have a size of zero.
				 * In that case use the size of the referenced
				 * type.
				 */
				if (mlp->ml_size == 0) {
					parse_debug(4, cp,
					    "adjusting zero bit size to %d "
					    "from %s",
					    itdp->t_intr->intr_nbits,
					    itdp->t_name);
					mlp->ml_type = itdp;
					mlp->ml_size = itdp->t_intr->intr_nbits;
				} else {
					parse_debug(4, cp,
					    "making %d bit intrinsic from %s",
					    mlp->ml_size, itdp->t_name);
					mlp->ml_type = bitintrinsic(itdp,
					    mlp->ml_size);
				}
			} else
				mlp->ml_type = tdp;
		} else if (itdp->t_type == TYPEDEF_UNRES) {
			list_add(&typedbitfldmems, mlp);
			mlp->ml_type = tdp;
		} else {
			mlp->ml_type = tdp;
		}

		/* cp is now pointing to next field */
		prev = &mlp->ml_next;
	}

	/*
	 * Parse virtual pointer: the virtual pointer (if any) is indicated
	 * in a dedicated field after the description of the class.  We
	 * represent the virtual pointer as a member of the struct representing
	 * the class.  This member has the type "void *" and the name "_vptr".
	 *
	 */
	if ((*cp == ';') && ((type == CPP_CLASS) || (type == CPP_STRUCT) ||
	    (type == CPP_UNION))) {

		cp++;

		/*
		 * Before parsing information of the virtual pointer, we have
		 * to skip three fields: member functions, static data members
		 * and friends.
		 */
		n = 0;
		while ((*cp != '\0') && (n < 3)) {
			if (*cp == ';')
				n++;
			cp++;
		}

		if ((*cp != '\0') && (*cp != ';')) {

			parse_debug(4, cp, "soudef: virtual pointer");

			/* algorithm number (ignored) */
			cp = number(cp, &n);
			if (*cp++ != ' ')
				expected("soudef", "<space>", cp - 1);

			/* virtual pointer offset */
			cp = number(cp, &n);
			n *= 8;

			/* create the "_vptr" member */
			mlp = xcalloc(sizeof (*mlp));
			mlp->ml_name = "_vptr";
			mlp->ml_offset = n;
			parse_debug(4, NULL, "      _vptr at offset %d", n);

			if (tdp_voidp == NULL) {
				assert(tdp_void != NULL);
				tdp_voidp = xcalloc(sizeof (tdesc_t));
				tdp_voidp->t_type = POINTER;
				tdp_voidp->t_size = 0;
				tdp_voidp->t_id = faketypenumber++;
				tdp_voidp->t_tdesc = tdp_void;
				parse_debug(4, NULL, "soudef: void* type is %d",
				    tdp_voidp->t_id);
			}
			mlp->ml_type = tdp_voidp;
			mlp->ml_size = tdp_voidp->t_size;

			/* insert the "_vptr" member at the right position */
			prev = &((*rtdp)->t_members);
			while ((*prev != NULL) && ((*prev)->ml_offset < n)) {
				prev = &(*prev)->ml_next;
			}
			mlp->ml_next = *prev;
			*prev = mlp;
		}
	}

out:
	return (cp);
}

static char *
arraydef(char *cp, tdesc_t **rtdp)
{
	int start, end, h;

	cp = id(cp, &h);
	if (*cp++ != ';')
		expected("arraydef/1", ";", cp - 1);

	(*rtdp)->t_ardef = xcalloc(sizeof (ardef_t));
	(*rtdp)->t_ardef->ad_idxtype = lookup(h);

	cp = number(cp, &start); /* lower */
	if (*cp++ != ';')
		expected("arraydef/2", ";", cp - 1);

	if (*cp == 'S') {
		/* variable length array - treat as null dimensioned */
		cp++;
		if (*cp++ != '-')
			expected("arraydef/fpoff-sep", "-", cp - 1);
		cp = number(cp, &end);
		end = start;
	} else {
		/* normal fixed-dimension array */
		cp = number(cp, &end);  /* upper */
	}

	if (*cp++ != ';')
		expected("arraydef/3", ";", cp - 1);
	(*rtdp)->t_ardef->ad_nelems = end - start + 1;
	cp = tdefdecl(cp, h, &((*rtdp)->t_ardef->ad_contents));

	parse_debug(3, cp, "defined array idx type %d %d-%d next ",
	    h, start, end);

	return (cp);
}

static void
enumdef(char *cp, tdesc_t **rtdp)
{
	elist_t *elp, **prev;
	char *w;

	(*rtdp)->t_type = ENUM;
	(*rtdp)->t_emem = NULL;

	prev = &((*rtdp)->t_emem);
	while (*cp != ';') {
		elp = xcalloc(sizeof (*elp));
		elp->el_next = NULL;
		*prev = elp;
		cp = name(cp, &w);
		elp->el_name = w;
		cp = number(cp, &elp->el_number);
		parse_debug(3, NULL, "enum %s: %s=%d", tdesc_name(*rtdp),
		    elp->el_name, elp->el_number);
		prev = &elp->el_next;
		if (*cp++ != ',')
			expected("enumdef", ",", cp - 1);
	}
}

tdesc_t *
lookup_name(tdesc_t **hash, const char *name)
{
	int bucket = compute_sum(name);
	tdesc_t *tdp, *ttdp = NULL;

	for (tdp = hash[bucket]; tdp != NULL; tdp = tdp->t_next) {
		if (tdp->t_name != NULL && strcmp(tdp->t_name, name) == 0) {
			if (tdp->t_type == STRUCT || tdp->t_type == UNION ||
			    tdp->t_type == ENUM || tdp->t_type == INTRINSIC)
				return (tdp);
			if (tdp->t_type == TYPEDEF)
				ttdp = tdp;
		}
	}
	return (ttdp);
}

tdesc_t *
lookupname(const char *name)
{
	return (lookup_name(name_table, name));
}

void
change_typename(hash_t *iihash, iidesc_t *iidescp, char *name)
{
	int bucket;
	tdesc_t *tdp, *prev;

	tdp = iidescp->ii_dtype;

	/* remove iidesc_t from the iidesc hash table */
	hash_remove(iihash, iidescp);

	/*
	 * Remove tdesc_t from the name hash table "name_table". Note that we
	 * don't remove the tdesc_t from the type hash table "hash_table"
	 * because its tid will not change.
	 */
	assert(tdp->t_name != NULL);
	bucket = compute_sum(tdp->t_name);
	if (name_table[bucket] == tdp) {
		name_table[bucket] = tdp->t_next;
	} else {
		for (prev = name_table[bucket]; prev != NULL;
		    prev = prev->t_next) {
			if (prev->t_next == tdp)
				break;
		}
		if (prev != NULL) {
			prev->t_next = tdp->t_next;
		}
	}

	/*
	 * tdp and iidescp should have the same name. However because of bug
	 * 4693451 this may not be true for nested enum with compilers older
	 * than Sun Studio 8.
	 */
	assert((tdp->t_type == ENUM) ||
	    strcmp(iidescp->ii_name, tdp->t_name) == 0);

	free(iidescp->ii_name);
	free(tdp->t_name);

	if (name == NULL) {
		iidescp->ii_name = NULL;
		tdp->t_name = NULL;
	} else {
		iidescp->ii_name = xstrdup(name);
		tdp->t_name = xstrdup(name);
	}

	/* re-insert iidesc_t into the iidesc hash table */
	hash_add(iihash, iidescp);

	/*
	 * Re-insert tdesc_t into the name hash table. addhash() expects to
	 * insert tdesc_t into at least one of the hash table name_table or
	 * hash_table. Here tdesc_t is already in hash_table and addhash()
	 * only adds to name_table types which have a name. So we should only
	 * call addhash() if the new name is not NULL.
	 */
	if (name != NULL)
		addhash(tdp, tdp->t_id);
}

/*
 * Add a node to the hash queues.
 */
static void
addhash(tdesc_t *tdp, int num)
{
	int hash = HASH(num);
	tdesc_t *ttdp;
	char added_num = 0, added_name = 0;

	/*
	 * If it already exists in the hash table don't add it again
	 * (but still check to see if the name should be hashed).
	 */
	ttdp = lookup(num);

	if (ttdp == NULL) {
		tdp->t_id = num;
		tdp->t_hash = hash_table[hash];
		hash_table[hash] = tdp;
		added_num = 1;
	}

	if (tdp->t_name != NULL) {
		ttdp = lookupname(tdp->t_name);
		if (ttdp == NULL) {
			hash = compute_sum(tdp->t_name);
			tdp->t_next = name_table[hash];
			name_table[hash] = tdp;
			added_name = 1;
		}
	}
	if (!added_num && !added_name) {
		terminate("stabs: broken hash\n");
	}
}

static int
compute_sum(const char *w)
{
	char c;
	int sum;

	for (sum = 0; (c = *w) != '\0'; sum += c, w++)
		;
	return (HASH(sum));
}

static void
reset(void)
{
	longjmp(resetbuf, 1);
}

void
check_hash(void)
{
	tdesc_t *tdp;
	int i, h;

	printf("checking hash\n");
	for (i = 0; i < BUCKETS; i++) {
		if (hash_table[i]) {
			for (tdp = hash_table[i]->t_hash;
			    tdp && tdp != hash_table[i];
			    tdp = tdp->t_hash) {
				if ((h = HASH(tdp->t_id)) != i)
					terminate("invalid hash id %d "
					    "(hash=%d) in bucket %d\n",
					    tdp->t_id, h, i);
				continue;
			}
			if (tdp) {
				terminate("cycle in hash bucket %d\n", i);
				return;
			}
		}

		if (name_table[i]) {
			for (tdp = name_table[i]->t_next;
			    tdp && tdp != name_table[i];
			    tdp = tdp->t_next) {
				assert(tdp->t_name != NULL);
				if ((h = compute_sum(tdp->t_name)) != i)
					terminate("invalid hash name %s "
					    "(hash=%d) in bucket %d\n",
					    tdp->t_name, h, i);
				continue;
			}
			if (tdp) {
				terminate("cycle in name bucket %d\n", i);
				return;
			}
		}
	}
	printf("done\n");
}

/*ARGSUSED1*/
static int
resolve_typed_bitfields_cb(mlist_t *ml, void *private)
{
	tdesc_t *tdp = ml->ml_type;

	debug(3, "Resolving typed bitfields (member %s)\n",
	    (ml->ml_name ? ml->ml_name : "(anon)"));

	while (tdp) {
		switch (tdp->t_type) {
		case INTRINSIC:
			if (ml->ml_size != tdp->t_intr->intr_nbits) {
				debug(3, "making %d bit intrinsic from %s",
				    ml->ml_size, tdesc_name(tdp));
				/*
				 * Because of bug 4818400 with Sun Studio 8,
				 * all volatile types will have a size of zero.
				 * In that case use the size of the referenced
				 * type.
				 */
				if (ml->ml_size == 0) {
					debug(3,
					    "adjusting zero bit size to %d "
					    "from %s",
					    tdp->t_intr->intr_nbits,
					    tdp->t_name);
					ml->ml_type = tdp;
					ml->ml_size = tdp->t_intr->intr_nbits;
				} else {
					debug(3,
					    "making %d bit intrinsic from %s",
					    ml->ml_size, tdp->t_name);
					ml->ml_type = bitintrinsic(tdp,
					    ml->ml_size);
				}
			} else {
				debug(3, "using existing %d bit %s intrinsic",
				    ml->ml_size, tdesc_name(tdp));
				ml->ml_type = tdp;
			}
			return (1);

		case POINTER:
		case TYPEDEF:
		case VOLATILE:
		case CONST:
		case RESTRICT:
			tdp = tdp->t_tdesc;
			break;

		default:
			return (1);
		}
	}

	terminate("type chain for bitfield member %s has a NULL", ml->ml_name);
	/*NOTREACHED*/
	return (0);
}

void
resolve_typed_bitfields(void)
{
	(void) list_iter(typedbitfldmems,
	    (int (*)())resolve_typed_bitfields_cb, NULL);
}
