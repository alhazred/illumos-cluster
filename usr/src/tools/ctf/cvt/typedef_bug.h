/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TYPEDEF_BUG_H
#define	_TYPEDEF_BUG_H

#pragma ident	"@(#)typedef_bug.h	1.3	08/05/20 SMI"

#include "ctftools.h"

#define	NONE	0
#define	MEMBER_NONE	((char *)0)
#define	TYPE_UNDEFINED	((tdesc_t *)-1)

#define	ARRAY_DEPTH_MAX 3

#define	CUR_PPPS(type)	{ POINTER, 3, STRUCT, type }
#define	CUR_PPS(type)	{ POINTER, 2, STRUCT, type }
#define	CUR_PS(type)	{ POINTER, 1, STRUCT, type }
#define	CUR_PI(type)	{ POINTER, 1, INTRINSIC, type }
#define	CUR_PT(type)	{ POINTER, 1, TYPEDEF, type }
#define	CUR_PF(rtype)	{ POINTER, 1, FUNCTION, rtype }
#define	CUR_S(type)	{ NONE, 0, STRUCT, type }
#define	CUR_I(type)	{ NONE, 0, INTRINSIC, type }
#define	CUR_AAI(type)	{ ARRAY, 2, INTRINSIC, type }

#define	TGT_PPPT(type)	{ POINTER, 3, TYPE_ ## type ## _PPP, TYPE_ ## type }
#define	TGT_PPT(type)	{ POINTER, 2, TYPE_ ## type ## _PP,  TYPE_ ## type }
#define	TGT_PT(type)	{ POINTER, 1, TYPE_ ## type ## _P,   TYPE_ ## type }
#define	TGT_T(type)	{ NONE,    0, TYPE_ ## type, TYPE_ ## type }
#define	TGT_AAT(type)	{ ARRAY,   2, TYPE_ ## type ## _AA,  TYPE_ ## type }

struct arr_desc {
	tdesc_t *index;
	uint_t nelems;
};

struct cur_desc {
	int depth_type;
	int depth;
	int kind;
	char *name;
};

struct tgt_desc {
	int depth_type;
	int depth;
	int type;
	int base;
};

struct typedef_bug_desc {
	int type;
	char *member;
	struct cur_desc current;
	struct tgt_desc target;
};

extern char *type_name[];
extern tdesc_t *type_cache[];
extern struct typedef_bug_desc typedef_bugs[];

#endif /* _TYPEDEF_BUG_H */
