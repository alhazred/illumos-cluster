#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.18	08/10/09 SMI"

include ../../Makefile.ctf

STRIPFLAG=

.KEEP_STATE:
.PARALLEL:

PROG=ctfconvert ctfmerge

GENSRCS= \
	alist.c \
	barrier.c \
	ctf.c \
	fifo.c \
	hash.c \
	iidesc.c \
	input.c \
	list.c \
	memory.c \
	merge.c \
	output.c \
	stack.c \
	strtab.c \
	symbol.c \
	tdata.c \
	traverse.c \
	util.c

CVTSRCS=$(GENSRCS) \
	ctfconvert.c \
	dwarf.c \
	stabs.c \
	fixup_tdescs.c \
	st_parse.c \
	typedef_bug.c

CVTOBJS=$(CVTSRCS:%.c=%.o)
CVTLINTFILES = $(CVTSRCS:%.c=%.ln)

MRGSRCS=$(GENSRCS) \
	ctfmerge.c
MRGOBJS=$(MRGSRCS:%.c=%.o)
MRGLINTFILES = $(MRGSRCS:%.c=%.ln)

SRCS=$(CVTSRCS) $(MRGSRCS) $(CMPSRCS)
OBJS=$(SRCS:%.c=%.o)
LINTFILES=$(SRCS:%.c=%.ln)

DWARFSRCLIBDIR	= ../../dwarf

DWARFLDFLAGS	= \
	-L$(DWARFSRCLIBDIR)/$(MACH) \
	'-R$$ORIGIN/../lib:$$ORIGIN/../../lib/$(MACH)' \
	-ldwarf
DWARFCPPFLAGS	= -I$(DWARFSRCLIBDIR)

LDLIBS		+= -lz -lelf
CPPFLAGS	+= -D_REENTRANT
CFLAGS		= -W0,-xc99=%all $(CTF_FLAGS)
LINTFLAGS	+= -mnux

ctfconvert	:= LDFLAGS += $(DWARFLDFLAGS) -ldemangle
#
# Some build machines are using Solaris 9 while some are using 10.
# Solaris 9 requires some libthread link options (-mt -lpthread) while
# Solaris 10 does not need this anymore. When all build machines are
# upgraded to Solaris 10 then these options can be removed.
#
ctfmerge	:= LDFLAGS += -mt -lpthread

dwarf.o dwarf.ln	:= CPPFLAGS += $(DWARFCPPFLAGS)
