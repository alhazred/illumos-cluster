/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fixup_tdescs.c	1.8	08/10/09 SMI"

/*
 * Workarounds for stabs generation bugs in the compiler and general needed
 * fixups.
 */

#include <stdio.h>
#include <strings.h>
#include <assert.h>

#include "ctf_headers.h"
#include "ctftools.h"
#include "hash.h"
#include "memory.h"
#include "typedef_bug.h"

typedef struct iitd_find {
	tdesc_t *tgt;
	iidesc_t *ret;
} iitd_find_t;

static int
iitd_find_cb(void *entry, void *data)
{
	iidesc_t *src = (iidesc_t *)entry;
	iitd_find_t *find = (iitd_find_t *)data;

	if (src->ii_dtype != find->tgt)
		return (0);

	find->ret = src;

	return (-1);
}

/*
 * The C compiler and the C++ compiler do not generate stabs with the same
 * name for an embedded struct (i.e. for a struct defined inside another
 * struct). The C++ compiler will use a context defined name while the C
 * compiler will use a globally defined name.  If such structure is used by
 * C code and by C++ code, then we will have the same stabs with two different
 * names and this will create duplicated CTF.  For example, the C struct
 * lwp_watch is defined inside struct _klwp; the C compiler will create a stab
 * named "lwp_watch" while the C++ compiler will create an equivalent stab
 * named "_klwp::lwp_watch".  These nodes are equivalent although they don't
 * have exact same name.
 *
 * The following function renames the specified structures and replaces the
 * C++ stabs name with the C stabs name.
 */
static void
fix_typename(tdata_t *td)
{
	/*
	 * curs[i] is the current type name and tgts[i] is the corresponding
	 * target type name. tgts[i] must match the string in curs[i] after
	 * the last colon-colon (::).
	 */
	char *curs[] = {
		"_klwp::lwp_watch", "_klwp::mstate",
		"_kthread::tsd_thread", "_kthread::__tu",
		"_kthread::__tdu", "_kthread::rprof",
		"_kthread::__tdu::__tds", "_kthread::__tu::__ts",
		"ciputctrl_str::_ciput_un",
		"ciputctrl_str::_ciput_un::_ciput_str",
		"kfpu_t::_kfpu_u"
	};
	char *tgts[] = {
		"lwp_watch", "mstate",
		"tsd_thread", "__tu",
		"__tdu", "rprof",
		"__tds", "__ts",
		"_ciput_un",
		"_ciput_str",
		"_kfpu_u"
	};
	int i;
	iitd_find_t find;
	iidesc_t ii;
	tdesc_t *str;

	for (i = 0; i < sizeof (curs) / sizeof (curs[0]); i++) {
		if (!(str = lookupname(curs[i])) ||
		    (str->t_type != STRUCT && str->t_type != UNION))
			continue;
		debug(4, "Changing typename %s to %s\n", str->t_name, tgts[i]);

		ii.ii_type = II_SOU;
		ii.ii_name = str->t_name;

		find.tgt = str;
		find.ret = NULL;

		(void) hash_match(td->td_iihash, &ii, iitd_find_cb, &find);

		if (find.ret == NULL)
			terminate("Can't find iidesc_t for type %d\n",
			    str->t_id);

		change_typename(td->td_iihash, find.ret, tgts[i]);
	}
}

/*
 * The include file flock.h defines l_end as l_len, so if you have a structure
 * having a member named l_end it will be renamed to l_len. This can create
 * duplicated types if such struct is sometimes used with flock.h and sometimes
 * not.  To avoid this problem, this function renames the member "l_len" to
 * "l_end" for the specified structures.
 */
static void
fix_flock_h()
{
	char *strs[] = {
		"repl_pxfs::lock_info_t",
		"repl_pxfs_v1::lock_info_t"
	};
	tdesc_t *str;
	mlist_t *ml;
	int i;

	for (i = 0; i < sizeof (strs) / sizeof (strs[0]); i++) {
		if (!(str = lookupname(strs[i])) || str->t_type != STRUCT)
			continue;

		for (ml = str->t_members; ml; ml = ml->ml_next) {
			if (streq(ml->ml_name, "l_len"))
				break;
		}
		if (ml == NULL)
			continue;

		free(ml->ml_name);
		ml->ml_name = xstrdup("l_end");

		debug(3, "Changing l_len to l_end in struct %s\n", strs[i]);
	}
}

/*
 * Due to 4432619, compilers will sometimes incorrectly generate some stabs
 * using typedef types.  Given a "struct foo", and a corresponding "typedef
 * struct foo foo_t".  In some cases, the compiler will sometimes omit the
 * "typedef foo_t" and replace it with its definition (i.e. "struct foo").
 * Regardless of correctness, this breaks merges, as it occurs inconsistently
 * by file.  The following routines know how to recognize this bug in a specific
 * set of cases.  There is no general way to solve this problem without a fix to
 * the compiler.  In general, cases should only be added to these routines to
 * fix merging problems.
 *
 * Cases are described in the file typedef_bug.txt, and the Perl script
 * typedef_bug.pl translates that file into typedef_bug.c which defines the
 * arrays type_name, type_cache and typedef_bugs.
 */

static tdesc_t *
get_type(int type)
{
	tdesc_t *td;

	td = type_cache[type];

	if (td == NULL) {
		td = lookupname(type_name[type]);
		if (td == NULL)
			td = TYPE_UNDEFINED;

		type_cache[type] = td;
	}

	return (td);
}

static void
fix_typedef(tdata_t *td)
{
	struct arr_desc sarray[ARRAY_DEPTH_MAX];
	struct typedef_bug_desc *bug;
	tdesc_t *str, *tgt, *p;
	tdesc_t **curp;
	mlist_t *ml;
	int depth, i;

	for (bug = &typedef_bugs[0]; bug->type != -1; bug++) {

		str = get_type(bug->type);

		if (str == TYPE_UNDEFINED)
			continue;

		if (bug->member == MEMBER_NONE) {
			/* the bug impacts a typedef */
			if (str->t_type != TYPEDEF)
				continue;
			curp = &str->t_tdesc;
		} else {
			/*
			 * The bug impacts a struct/union member. The impacted
			 * struct or union is directly referenced if it has a
			 * name or it is referenced through a typedef if it is
			 * anonymous.
			 */
			if (str->t_type == TYPEDEF) {
				/* anonymous struct referenced via a typedef */
				str = str->t_tdesc;
				if (str->t_name != NULL)
					continue;
			}
			if ((str->t_type != STRUCT) && (str->t_type != UNION))
				continue;

			for (ml = str->t_members; ml; ml = ml->ml_next) {
				if (streq(ml->ml_name, bug->member))
					break;
			}
			if (ml == NULL)
				continue;
			curp = &ml->ml_type;
		}

		p = *curp;
		depth = bug->current.depth;

		switch (bug->current.depth_type) {

		case NONE:
			i = 0;
			break;

		case POINTER:
			for (i = 0; i < depth; i++) {
				if (p->t_type != POINTER)
					break;
				p = p->t_tdesc;
			}
			break;

		case ARRAY:
			assert(depth <= ARRAY_DEPTH_MAX);
			for (i = 0; i < depth; i++) {
				if (p->t_type != ARRAY)
					break;
				sarray[i].index = p->t_ardef->ad_idxtype;
				sarray[i].nelems = p->t_ardef->ad_nelems;
				p = p->t_ardef->ad_contents;
			}
			break;

		default:
			terminate("Unknown typedef bug depth type: %d\n",
			    bug->current.depth_type);
		}

		if ((i < depth) || (p->t_type != bug->current.kind))
			continue;

		if (p->t_type == FUNCTION) {
			/* FUNCTION */
			if (!streq(p->t_fndef->fn_ret->t_name,
				bug->current.name))
				continue;
		} else {
			/* STRUCT or INTRINSIC or TYPEDEF */
			if (!streq(p->t_name, bug->current.name)) {
				if (strcmp(bug->current.name, "unsigned 64") ||
				    (!streq(p->t_name, "unsigned long") &&
				    !streq(p->t_name, "unsigned long long")))
					continue;
			}
		}

		tgt = type_cache[bug->target.type];

		if (tgt == NULL) {
			/* need to build target type */
			tgt = get_type(bug->target.base);

			if (tgt != TYPE_UNDEFINED) {
				if (tgt->t_type != TYPEDEF) {
					tgt = TYPE_UNDEFINED;
				} else if (bug->target.depth_type == POINTER) {

					i = 0;
					while (i < bug->target.depth) {
						p = xcalloc(sizeof (*p));
						p->t_type = POINTER;
						p->t_id = td->td_nextid++;
						p->t_tdesc = tgt;
						tgt = p;
						i++;
					}

				} else if (bug->target.depth_type == ARRAY) {

					i = bug->target.depth - 1;
					assert(i < ARRAY_DEPTH_MAX);
					while (i >= 0) {
						p = xcalloc(sizeof (*p));
						p->t_type = ARRAY;
						p->t_id = td->td_nextid++;
						p->t_ardef = xcalloc(
							sizeof (ardef_t));
						p->t_ardef->ad_nelems =
							sarray[i].nelems;
						p->t_ardef->ad_idxtype =
							sarray[i].index;
						p->t_ardef->ad_contents = tgt;
						tgt = p;
						i--;
					}
				}
			}
			type_cache[bug->target.type] = tgt;
		}

		if (tgt == TYPE_UNDEFINED)
			/* target type doesn't exist */
			continue;

		*curp = tgt;

		debug(3, "Fixed %s:%s typedef %s (depth = %d => %d)\n",
		    type_name[bug->type],
		    (bug->member == MEMBER_NONE)? "" : bug->member,
		    type_name[bug->target.base], depth, bug->target.depth);
	}

}

/*
 * Fix stabs generation bugs.  These routines must be run before the
 * post-conversion merge
 */
void
cvt_fixstabs(tdata_t *td)
{
	fix_typedef(td);
}

struct match {
	tdesc_t *m_ret;
	const char *m_name;
};

static int
matching_iidesc(iidesc_t *iidesc, struct match *match)
{
	if (!streq(iidesc->ii_name, match->m_name))
		return (0);

	if (iidesc->ii_type != II_TYPE && iidesc->ii_type != II_SOU)
		return (0);


	match->m_ret = iidesc->ii_dtype;
	return (-1);
}

static tdesc_t *
lookup_tdesc(tdata_t *td, const char *name)
{
	struct match match = { NULL, name };
	iter_iidescs_by_name(td, name, (int (*)())matching_iidesc, &match);
	return (match.m_ret);
}

/*
 * The cpu structure grows, with the addition of a machcpu member, if
 * _MACHDEP is defined.  This means that, for example, the cpu structure
 * in unix is different from the cpu structure in genunix.  As one might
 * expect, this causes merges to fail.  Since everyone indirectly contains
 * a pointer to a CPU structure, the failed merges can cause massive amounts
 * of duplication.  In the case of unix uniquifying against genunix, upwards
 * of 50% of the structures were unmerged due to this problem.  We fix this
 * by adding a cpu_m member.  If machcpu hasn't been defined in our module,
 * we make a forward node for it.
 */
static void
fix_small_cpu_struct(tdata_t *td, size_t ptrsize)
{
	tdesc_t *cput, *cpu;
	tdesc_t *machcpu;
	mlist_t *ml, *lml;
	mlist_t *cpum;
	int foundcpucyc = 0;

	/*
	 * We're going to take the circuitous route finding the cpu structure,
	 * because we want to make sure that we find the right one.  It would
	 * be nice if we could verify the header name too.  DWARF might not
	 * have the cpu_t, so we let this pass.
	 */
	if ((cput = lookup_tdesc(td, "cpu_t")) != NULL) {
		if (cput->t_type != TYPEDEF)
			return;
		cpu = cput->t_tdesc;
	} else {
		cpu = lookup_tdesc(td, "cpu");
	}

	if (cpu == NULL)
		return;

	if (!streq(cpu->t_name, "cpu") || cpu->t_type != STRUCT)
		return;

	for (ml = cpu->t_members, lml = NULL; ml;
	    lml = ml, ml = ml->ml_next) {
		if (strcmp(ml->ml_name, "cpu_cyclic") == 0)
			foundcpucyc = 1;
	}

	if (foundcpucyc == 0 || lml == NULL ||
	    strcmp(lml->ml_name, "cpu_m") == 0)
		return;

	if ((machcpu = lookup_tdesc(td, "machcpu")) == NULL) {
		machcpu = xcalloc(sizeof (*machcpu));
		machcpu->t_name = xstrdup("machcpu");
		machcpu->t_id = td->td_nextid++;
		machcpu->t_type = FORWARD;
	} else if (machcpu->t_type != STRUCT) {
		return;
	}

	debug(3, "Adding cpu_m machcpu %s to cpu struct\n",
	    (machcpu->t_type == FORWARD ? "forward" : "struct"));

	cpum = xmalloc(sizeof (*cpum));
	cpum->ml_offset = lml->ml_offset + (ptrsize * NBBY);
	cpum->ml_size = 0;
	cpum->ml_name = xstrdup("cpu_m");
	cpum->ml_type = machcpu;
	cpum->ml_next = NULL;

	lml->ml_next = cpum;
}

void
cvt_fixups(tdata_t *td, size_t ptrsize)
{
	fix_typename(td);
	fix_flock_h();
	fix_small_cpu_struct(td, ptrsize);
}
