/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)extract_gettext.c	1.13	08/05/20 SMI"

/*
 * extract_gettext
 *
 * This program reads a shell script from stdin,
 * extracts all occurances of gettext and their strings, then
 * rewrites them to stdout in a form usable by xgettext(1).
 *
 * Note that any gettext strings spanning multiple lines are
 * concatenated to form one contiguous text string. This is only
 * possible because the english text is also reformatted similarly
 * before being output. See gettext() and sc_print_para() in
 * usr/src/cmd/scinstall/lib/sc_common.ksh
 */

#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <ctype.h>

char *ggetline(char *line, int maxline);

int
main(int argc, char **argv)
{
	register char *ptr, *s;
	char buffer[BUFSIZ];
	char *comment = NULL;
	int c, quote;

	/* Get args */
	opterr = 0;
	while ((c = getopt(argc, argv, "c:")) != EOF) {
		switch (c) {
		case 'c':
			comment = optarg;
			break;

		default:
			break;
		}
	}

	/* Get first line */
	ptr = ggetline(buffer, sizeof (buffer));

	/* Process current line */
	while (ptr) {
		/* Skip leading spaces */
		while (isspace(*ptr))
			++ptr;

		/* Search for comment block */
		if ((*ptr == '#') && (comment != NULL)) {
			/* Is this a -c comment? */
			if ((s = strstr(ptr, comment)) != NULL) {
				if (isspace(*(s + strlen(comment)))) {
					printf("/*\n");

					while (*ptr == '#') {
						/* Print comment text */
						printf(" * %s", ++ptr);
						ptr = ggetline(buffer, \
							sizeof (buffer));

						/* Skip leading spaces */
						while (isspace(*ptr))
							++ptr;
					}

					printf(" */\n");
					continue;
				}
			}
		/* Search for gettext string */
		} else if ((s = strstr(ptr, "gettext")) != NULL) {
			if (isspace(*(s + strlen("gettext")))) {
				ptr = s + strlen("gettext ");
				quote = 0;

				/* Search for beginning quote */
				while (ptr && !quote) {
					if ((s = strpbrk(ptr, "\"\'\\")) \
					    == NULL) {
						/* Not found */
						break;
					} else if (*s == '\\') {
						ptr = ggetline(buffer, \
							sizeof (buffer));
					} else {
						/* Print gettext(" */
						ptr = s;
						quote = *ptr++;
						printf("gettext(\"");
					}
				}

				/* Search for ending quote */
				while (ptr && quote) {
					if ((s = strpbrk(ptr, "\"\'\\")) \
					    == NULL) {
						/*
						 * Print remainder of line
						 * without '\n'
						 */
						*(ptr + strlen(ptr) - 1) \
						    = '\0';
						printf("%s", ptr);

						/* Skip blank lines */
						while (ptr = ggetline(buffer, \
						    sizeof (buffer))) {
							if (*ptr != '\n') {
								break;
							}
						}

						/* Skip leading spaces */
						if (isspace(*ptr)) {
							putchar(' ');

							while (isspace(*++ptr));
						}
					} else {
						/* Print selected text */
						c = *s;
						*s = '\0';
						printf("%s", ptr);
						ptr = ++s;

						/*
						 * Is this really the
						 * ending quote?
						 */
						if (c == quote) {
							/* Print "); */
							quote = 0;
							printf("\"); \n");
						} else {
							if (c == '\"') {
								putchar('\\');
							}

							putchar(c);

							if (c == '\\') {
								putchar(*ptr++);
							}
						}
					}
				}

				/* Process remainder of line  */
				continue;
			}
		}

		/* Get next line */
		ptr = ggetline(buffer, sizeof (buffer));
	}

	return (0);
}


/* Gets a line from stdin */
char *
ggetline(char *line, int maxline)
{
	if (fgets(line, maxline, stdin) != NULL) {
		/* Line too long? */
		if (line[strlen(line) - 1] != '\n') {
			fprintf(stderr, "line too long\n");
			exit(1);
		}
		return (line);
	}
	return (NULL);
}
