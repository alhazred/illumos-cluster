//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clexec_clconf.cc	1.5	08/07/17 SMI"

#include <h/naming.h>
#include <h/remote_exec.h>
#include <sys/cladm_debug.h>
#include <sys/clconf_int.h>
#include <nslib/ns.h>
#include <sys/sol_version.h>

#if (SOL_VERSION >= __s10)
#ifdef _KERNEL
#include <sys/zone.h>
#else
#include <zone.h>
#endif // _KERNEL
#define	SHUTDOWN_USER_CMD	"/usr/sbin/init 0"
#endif // SOL_VERSION >= __s10

// Maximum number of retry attempts.
#define	MAX_RETRY	12

// Sleep Interval between retry attempts.
#define	SLEEP_INTERVAL	5
#define	MICROSEC	1000000

//
// This function invokes the cl_exec interface to remote execute
// a task on another node in the cluster.
// This function also supports a "virtual cluster" of zones. If this
// function is invoked from a non-global zone, the assumption is
// that this non-global zone is part of the virtual cluster. The task
// is then executed on the non-global zone that is part of the same
// virtual cluster running on another physical node of the cluster.
//
extern "C" clconf_errnum_t
clconf_exec_program_with_opts(const char *program, nodeid_t id,
    boolean_t *clust_memb, boolean_t realtime)
{
#if defined(_KERNEL) || !defined(_KERNEL_ORB)

	CORBA::Object_var		obj_v;
	CORBA::Exception		*exp;
	Environment			env;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	bool				check_if_online = true;

#if (SOL_VERSION >= __s10) && !defined(_KERNEL)

	//
	// Build the namespace from "cl_exec.<nodeid>.<zonename>"
	//
	char		zonename[ZONENAME_MAX];
	char		name[ZONENAME_MAX + 12];
	zoneid_t	zoneid = getzoneid();

	if (zoneid == GLOBAL_ZONEID) {
		os::sprintf(name, "cl_exec.%d", id);
	} else {
		if (getzonenamebyid(zoneid, zonename,
		    sizeof (zonename)) < 0) {
			return (CL_ZONE_ERROR);
		}
		os::sprintf(name, "cl_exec.%d.%s", id, zonename);
	}

#else	// (SOL_VERSION >= __s10) && !defined(_KERNEL)

	char		name[20];
	os::sprintf(name, "cl_exec.%d", id);

#endif	// (SOL_VERSION >= __s10) && !defined(_KERNEL)

	os::sc_syslog_msg	msg_handle("", program, NULL);
	uint_t			index;

	for (index = 0; index < MAX_RETRY; index++) {

#if (SOL_VERSION >= __s10)
		//
		// A special check for scshutdown.
		// In case of scshutdown for inside a virtual cluster,
		// the CZMM shutdown reconfiguration is done before
		// the actual shutdown command is done on the zones.
		// clconf code checks whether the zone is online and
		// then only runs the command on it. Since the CZMM
		// has shutdown the virtual cluster membership, so
		// clconf will get the zeroed membership, and hence
		// would not execute the command on the zones of the
		// virtual cluster.
		//
		if ((os::strcmp(program, SHUTDOWN_USER_CMD) == 0) &&
		    clconf_inside_virtual_cluster()) {
			check_if_online = false;
		}
#endif

		if (check_if_online) {
			//
			// Check if the node is a current member of the cluster,
			// before executing the user program.
			//
			if (!(conf_is_cluster_member(id))) {
				if (clust_memb != NULL) {
					*clust_memb = B_FALSE;
				}
				return (CL_NO_CLUSTER);
			} else {
				if (clust_memb != NULL) {
					*clust_memb = B_TRUE;
				}
			}
		}

		if (index != 0) {
			env.clear();
			os::usecsleep((os::usec_t)SLEEP_INTERVAL * MICROSEC);
		}

		// Get the object from the nameserver.
		obj_v = ctx_v->resolve(name, env);

		if ((exp = env.exception()) != NULL) {
#ifdef	_KERNEL
			CLEXEC_EXCEPTION(env, "clconf_do_execution",
			    "resolve");
#endif

			if (naming::not_found::_exnarrow(exp) != NULL) {
				//
				// The object is not bound in the nameserver.
				// Try again.
				//
#ifdef	_KERNEL
				CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
				    ("clconf_do_execution %s narrow\n", name));
#endif
				continue;
			}
		}
		//
		// Narrow the object down to type 'remote_exec'.
		//
		remote_exec::cl_exec_var cl_exec_v =
		    remote_exec::cl_exec::_narrow(obj_v);

		if (CORBA::is_nil(cl_exec_v)) {
			//
			// Some other object has bound as cl_exec!!
			// Try again.
			//
#ifdef	_KERNEL
			CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
			    ("clconf_do_execution:%s No cl_exec\n", name));
#endif
			continue;
		}

		// Execute the user program.
		int	result;
		char	*stdout_str = NULL;
		char	*stderr_str = NULL;
		int	ret_val = 0;

#ifdef	_KERNEL
		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_GREEN,
		    ("clconf_do_execution:%s exec %s %d\n",
		    name, program, (int)realtime));
#endif

		ret_val = cl_exec_v->exec_program_get_output(result,
		    stdout_str, stderr_str, program, remote_exec::DEF, 0, env);

#ifdef	_KERNEL
		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_GREEN,
		    ("clconf_do_execution:%s exec %s retval %d result %d\n",
		    name, program, ret_val, result));
#endif
		if (env.exception() != NULL) {
#ifdef	_KERNEL
			CLEXEC_EXCEPTION(env, "clconf_do_execution", name);
#endif
			//
			// The node hosting the cl_exec server object may have
			// gone down between the time we got a reference to it
			// and did an inovation on this object. Try again.
			//
			continue;
		}
		if (ret_val != 0) {
			//
			// SCMSGS
			// @explanation
			// The task execution by the cl_execd interface failed.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    NO_SC_SYSLOG_MESSAGE("Executing %s failed, "
			    "retval = %d"), program, ret_val);
			return (CL_EXEC_ERROR);
		}
		//
		// syslog stdout.
		//
		if (stdout_str != NULL) {
			msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    NO_SC_SYSLOG_MESSAGE("%s: %s"), "stdout",
			    stdout_str);
			delete [] stdout_str;
		}
		//
		// syslog stderr.
		//
		if (stderr_str != NULL) {
			msg_handle.log(SC_SYSLOG_NOTICE, MESSAGE,
			    NO_SC_SYSLOG_MESSAGE("%s: %s"), "stderr",
			    stderr_str);
			delete [] stderr_str;
		}
		return (CL_NOERROR);
	}

	ASSERT(index >= MAX_RETRY);

#endif // !defined(_KERNEL) && defined(_KERNEL_ORB)

	return (CL_INVALID_OBJ);
}
