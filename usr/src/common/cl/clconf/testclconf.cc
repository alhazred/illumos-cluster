/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testclconf.cc	1.8	08/05/20 SMI"

#include <stdio.h>
#include <sys/clconf_int.h>
#include <sys/clconf_property.h>
#include <clconf/ccr_cctree.h>

int
print_callback(ccr_node *node, void *)
{
	if (node->get_value() != NULL) {
		char *fname = node->get_full_name();
		printf("%s\t%s\n", fname, node->get_value());
		delete [] fname;
	}
	return (1);
}


main()
{
	clpl_refresh();
	clpl_read("../clprtlist");
	clpl_read("clprtlist");
	extern ccr_node *clplroot;
	clplroot->traverse(print_callback, NULL);

	printf("\n\n");

	clconf_obj_t *ada = clpl_get_object(CL_ADAPTER, "hme");
	if (ada == NULL)
		return (0);

	// print out all the properties.
	clconf_iter_t *piter = clpl_obj_get_properties(ada);
	clconf_obj_t *prt;
	while ((prt = clconf_iter_get_current(piter)) != NULL) {
		clconf_propdesc_t dsc = clpl_prop_get_description(prt);
		printf("%s %i\n", dsc.name, dsc.type);
		clconf_iter_advance(piter);
	}
	clconf_iter_release(piter);

	printf("\n\n");
	clplroot->traverse(print_callback, NULL);
}
