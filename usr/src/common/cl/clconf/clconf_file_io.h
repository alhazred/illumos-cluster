/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLCONF_FILE_IO_H
#define	_CLCONF_FILE_IO_H

#pragma ident	"@(#)clconf_file_io.h	1.17	08/05/20 SMI"

#include <ccr/persistent_table.h>
#include <clconf/clconf_io.h>
#include <clconf/clnode.h>

// The io interface class that reads the configuration data using
// CCR FILE interface.
class clconf_file_io : public clconf_io {
public:
	// Initialize the clconf tree. It will read from CCR, construct
	// the tree. Returns true on success, and false if fail.
	bool initialize();

	void shutdown();

	clconf_file_io(const char *);
	~clconf_file_io();

	// Open a read CCR session.
	clconf_io::status open();
	// Close a read CCR session.
	void close();
	// Read one element from CCR.
	clconf_io::status read_one(char **, char **);

	// Readonly interface. No write.
	bool begin_transaction();
	bool commit_transaction();
	void abort_transaction();
	static int append_node(ccr_node *nd, void *vp);
	bool write(const char *, const char *);
	void *create_seq();
	void delete_seq(void *);
	int write_seq(void *);
	int32_t get_version();

private:
	readonly_persistent_table *read_tb;
	updatable_persistent_table *write_tb;

	// Stuff to support multiple threads calling initialize
	char *filename;
	char *write_filename;
	bool is_initialized;
	os::mutex_t lck;

	clconf_file_io(const clconf_file_io &);
	clconf_file_io & operator = (const clconf_file_io &);

};

#endif	/* _CLCONF_FILE_IO_H */
