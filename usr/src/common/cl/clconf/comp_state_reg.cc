//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)comp_state_reg.cc	1.17	08/05/20 SMI"

#include <h/component_state.h>
#include <clconf/comp_state_reg.h>
#include <clconf/comp_state_proxy.h>
#include <orb/infrastructure/orb_conf.h>
#include <h/naming.h>
#include <nslib/ns.h>


//
// Static members
//
component_state_reg	*component_state_reg::the_component_state_reg = NULL;

static os::mutex_t		reg_lock;

//
// constructor
//
component_state_reg::component_state_reg() :
	McServerof<component_state::registry>(),
	proxy_count(0)
{
	ASSERT(the_component_state_reg == NULL);
}

//
// destructor - clear the registry prior to exiting
//
component_state_reg::~component_state_reg()
{
	component_state_proxy	*csp;

	ASSERT(reg_lock.lock_held());

	ASSERT(the_component_state_reg == this);
	the_component_state_reg = NULL;

	// clean up the hashtable
	table_lock.lock();

	string_hashtable_t<component_state_proxy *>::iterator
	    iter(proxy_hashtab);

	while ((csp = iter.get_current()) != NULL) {
		iter.advance();		// advance before removing from table
		csp->proxy_lock();
		(void) _remove_proxy(csp);
		csp->proxy_unlock();
	}

	table_lock.unlock();
}

//
// IDL Interfaces
// get_state
void
component_state_reg::get_state(const char *cmp_name,
    component_state::component_state_t_out cmp_state, Environment &e)
{
	ASSERT(cmp_name != NULL);

	table_lock.lock();
	component_state_proxy	*csp = proxy_hashtab.lookup(cmp_name);
	if (csp == NULL) {
		table_lock.unlock();
		e.exception(
		    new component_state::registry::component_does_not_exist);
		cmp_state = (component_state::component_state_t *)NULL;
		return;
	}

	// Get the information from the proxy while holding the lock
	// but dont write it directly in the out parameter.
	// Writing into the out parameter will cause some memory allocation
	// to be done by the class String_field
	// We dont want to allocate memory while holding the csp->lock()

	// This need only be done for the state_string, which can change
	// depending if the state of the proxy changes.
	// We get back a constant pointer to static data, therefore we dont
	// have to copy the string while holding the lock
	// the proxy object cannot be deleted while we are in get_state,
	// because it need be removed from the hashtable before being deleted
	// and we're holding the table lock

	// The only data on the proxy that can change is the state which
	// affects which static string is returned by the get_state_string()
	// method.  Therefore those are the only 2 members that we must examine
	// under a lock from the csp

	const char	*state_string;

	// Now do the memory allocation
	cmp_state = new component_state::component_state_t;

	csp->proxy_lock();
	(*cmp_state).state = (short)csp->get_state();
	state_string = csp->get_state_string();
	csp->proxy_unlock();

	(*cmp_state).component_name = csp->get_name();
	(*cmp_state).component_type = csp->get_type();
	(*cmp_state).state_string = state_string;

	table_lock.unlock();
}

//
// get_state_list
//
void
component_state_reg::get_state_list(
    component_state::component_state_list_t_out state_list, Environment &)
{
	table_lock.lock();

	unsigned int		state_index;
	component_state_proxy	*csp;

	// Same message about allocation while holding the csp lock applis
	// to this method. (See comment in get_state())
	const char	*state_string;

	// Get an iterator to the hash table
	string_hashtable_t<component_state_proxy *>::iterator i(proxy_hashtab);

	// Allocate space for the out structure
	state_list = new component_state::component_state_list_t
	    (proxy_count, proxy_count);

	for (state_index = 0; (csp = i.get_current()) != NULL;
	    i.advance(), state_index++) {
		csp->proxy_lock();
		(*state_list)[state_index].state = (short)csp->get_state();
		state_string = csp->get_state_string();
		csp->proxy_unlock();

		(*state_list)[state_index].component_name = csp->get_name();
		(*state_list)[state_index].component_type = csp->get_type();
		(*state_list)[state_index].state_string = state_string;
	}

	table_lock.unlock();
}


//
// Create the registry
// Will create the registry (so it can be accessed via "the").
int
component_state_reg::create_registry()
{
	reg_lock.lock();

	ASSERT(the_component_state_reg == NULL);
	the_component_state_reg = new component_state_reg;

	reg_lock.unlock();

	return (0);
}

//
// Initialize the registry
// The component state registry lives on the global nameserver.  Once csr is
// started for each node in the cluster, and only local proxies will register
// with a particular csr.
//
int
component_state_reg::initialize_registry()
{
	Environment			e;
	naming::naming_context_var	ctxp;
	component_state::registry_var	csr_ref;
	char				reg_name[16];

	if (os::snprintf(reg_name, 16UL, "csr-node-%u",
	    orb_conf::local_nodeid()) >= 15) {
		reg_name[15] = '\0';
	}

	ctxp = ns::root_nameserver();

	reg_lock.lock();
	csr_ref = the()->get_objref();

	ctxp->rebind(reg_name, csr_ref, e);

	reg_lock.unlock();

	if (e.exception()) {
		e.exception()->print_exception("component registry bind");
		return (EIO);
	}

	return (0);
}


// shutdown the registry, causes the registry to get unref'd
//
int
component_state_reg::shutdown_registry()
{
	int	rslt = 0;

	// Currently we do shutdown_registry so late in the shutdown sequence
	// that we cannot support any more remote invocations.
	// Workaround is to not unbind/delete. return success
#if 0
	Environment			e;
	naming::naming_context_var	ctxp;
	char				reg_name[16];

	if (os::snprintf(reg_name, 16UL, "csr-node-%u",
	    orb_conf::local_nodeid()) >= 15) {
		reg_name[15] = '\0';
	}

	ctxp = ns::root_nameserver();

	reg_lock.lock();
	ctxp->unbind(reg_name, e);
	if (e.exception()) {
		e.exception()->print_exception("component registry unbind");
		rslt = EIO;
	}
	// unreferenced will cause the object to be deleted
	reg_lock.unlock();
#endif
	return (rslt);
}

//
// Add a proxy to the registry
//
int
component_state_reg::add_proxy(component_state_proxy *csp)
{
	// No duplicates should be possible
	const char	*name = csp->get_name();

	table_lock.lock();

	if (proxy_hashtab.lookup(name) != NULL) {
		table_lock.unlock();
		return (1);
	}

	proxy_hashtab.add(csp, name);
	proxy_count++;

	table_lock.unlock();

	return (0);
}

//
// Remove a proxy from the registry
// does locking
//
int
component_state_reg::remove_proxy(component_state_proxy *csp)
{
	int		rslt;

	table_lock.lock();
	rslt = _remove_proxy(csp);
	table_lock.unlock();

	return (rslt);
}

//
// Remove a proxy from the registry
// does no-locking is a private interface for local context callers
//
int
component_state_reg::_remove_proxy(component_state_proxy *csp)
{
	int	rslt	= 0;

	if (proxy_hashtab.remove(csp->get_name()) != NULL)
		proxy_count--;
	else
		rslt = 1;

	return (rslt);
}

//
// unreferenced
// - clear the "the" pointer
//
void
component_state_reg::_unreferenced(unref_t cookie)
{
	reg_lock.lock();
	if (_last_unref(cookie)) {
		delete this;
	}
	reg_lock.unlock();
}
