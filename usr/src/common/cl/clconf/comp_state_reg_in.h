/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  comp_state_reg_in.h
 *
 */

#ifndef _COMP_STATE_REG_IN_H
#define	_COMP_STATE_REG_IN_H

#pragma ident	"@(#)comp_state_reg_in.h	1.5	08/05/20 SMI"

//
// Get a reference to the "the" component state registry
//
inline component_state_reg *
component_state_reg::the()
{
	return (the_component_state_reg);
}

#endif	/* _COMP_STATE_REG_IN_H */
