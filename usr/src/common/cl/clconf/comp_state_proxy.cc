//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)comp_state_proxy.cc	1.17	08/05/20 SMI"
//

#include <sys/os.h>
#include <clconf/comp_state_proxy.h>
#include <clconf/comp_state_reg.h>
#include <orb/infrastructure/orbthreads.h>
#include <sys/sc_syslog_msg.h>

// constructor
component_state_proxy::component_state_proxy()
{
}


// destructor
component_state_proxy::~component_state_proxy()
{
}

//
// get_type
// maps the static enum of the type of component to a string
const char *
component_state_proxy::get_type()
{
	// static vector of type strings
	//
	// this is where we map map the current proxy to a type string
	// when a query has come into the csr.
	// each proxy subclass will have implemented the method _type()
	//
	// The strings in  this static map are defined in
	// src/sys/rsrc_tag.h
	//
	static const char *type_map[LAST_COMPONENT_TYPE] = {
		SC_SYSLOG_TRANSPORT_PATH_TAG,
		SC_SYSLOG_TRANSPORT_ADAPTER_TAG,
	};

	return (type_map[_type()]);
}

//
// register_with_csr - will register this proxy with the csr
// and post a ADDED event to syslog
// returns 1 if the registration was unsuccessful and returns 0 if successful.
//
// We do not hold any locks while doing this, so it is OK even if we have
// to wait for memory during add_proxy or posting to syslog.
//
int
component_state_proxy::register_with_csr()
{
	component_state_reg	*csr = component_state_reg::the();

	ASSERT(!proxy_lock_held());
	if (csr != NULL) {
		if (csr->add_proxy(this)) {
			ASSERT(0);
		} else {
			// Post the event to the syslog
			post_event_to_syslog(ADDED, NULL);
			return (0);
		}
	}
	return (1);
}

//
// unregister_from_csdr - will remove the proxy from the csr
// and post a REMOVED event to syslog
// returns 1 if the unregister was unsuccessful and returns 0 if successful.
//
// We do not hold any locks while doing this, so it is OK even if we have
// to wait for memory during add_proxy or posting to syslog.
//
int
component_state_proxy::unregister_from_csr()
{
	component_state_reg	*csr = component_state_reg::the();

	ASSERT(!proxy_lock_held());
	if (csr != NULL) {
		if (csr->remove_proxy(this)) {
			ASSERT(0);
		} else {
			post_event_to_syslog(REMOVED, NULL);
			return (0);
		}
	}
	return (1);
}

//
// post event to syslog - will post the current event to syslog
// get_state_fmtstring() determines unique ID for each component's
// message
//
void
component_state_proxy::post_event_to_syslog(int event, const char *msg)
{
	const char 		*adpname = get_extname();
	os::sc_syslog_msg	event_msg(get_type(), get_name(), NULL);

	if (msg != NULL) {
		(void) event_msg.log(SC_SYSLOG_NOTICE, event,
		    get_state_fmtstring(), adpname);
	}
}
