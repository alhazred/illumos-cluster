/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  clnode_in.h
 *
 */

#ifndef _CLNODE_IN_H
#define	_CLNODE_IN_H

#pragma ident	"@(#)clnode_in.h	1.15	08/05/20 SMI"

inline void
clnode::hold()
{
	cnt->hold();
}

inline void
clnode::rele()
{
	cnt->rele();
}

inline uint_t
clnode::get_incn()
{
	return (cnt->incn_num);
}

inline void
clnode::set_parent(ccr_node *n)
{
	parent = n;
	cnt = ((clnode *)n)->cnt;
}

#ifdef _KERNEL_ORB
inline void
cl_current_tree::set_oldroot_to_root()
{
	root_lock.lock();
	old_root = root;
	root_lock.unlock();
}
#endif

#endif	/* _CLNODE_IN_H */
