/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLNODE_H
#define	_CLNODE_H

#pragma ident	"@(#)clnode.h	1.37	08/06/30 SMI"

#include <clconf/ccr_cctree.h>
#include <sys/refcnt.h>
#include <clconf/clconf_io.h>

#include <sys/sol_version.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#endif

#ifndef UNODE
#if SOL_VERSION >= __s10

#ifdef _KERNEL
#include <sys/zone.h>
#else
#include <zone.h>
#endif	// _KERNEL

#define	CLUSTER_NAME_MAX ZONENAME_MAX

#endif	// SOL_VERSION >= __s10
#endif	// UNODE

// This file defines the data structures used to access the CCR
// infrastructure table. The data structures are based on the generic
// ones defined in ccr_cctree.h, with locks and reference counting infos
// added.
//
// For a clconf tree we have a root node, from which we
// can traverse the tree and get all the info.
//
// We have a reference count. Every clconf tree is reference counted.
// The current (most recent) clconf tree is created with a refcount of 1.
// When it's replaced by some other clconf tree as current its refcount is
// decremented by 1. A process increment the refcount before it accesses
// the data in the tree, and decrement it when it's done. When the refcount
// goes to 0 the tree is destroyed. The reference counting is needed to
// keep the trees around as long as someone may still access them.
//
// We have an incarnation number that is
// incremented everytime the current tree changes. The incarnation number
// is there to coordinate writes to CCR. When a process want to change
// CCR, it gets a copy of the current tree, modifies it, and call
// write_ccr to commit the changes. If however, the CCR changed since it
// obtained the copy of the tree, the commit will fail because the
// incn_number doesn't match. This prevents a process to overwrite
// the changes made by another process.
//
// We have a lock to protect the information described above.
// However the clconf trees are not protected by locks.
// This is OK because reading the tree (through the
// clconf interfaces) doesn't change the tree (by using iterators). Writing
// to the tree is always achieved by modifying a copy of the tree (which is
// local to the thread that's doing the modification) and then
// swap the new tree with the old tree.


// forward declarations
class clnode;

// Maintains information about the current clconf tree.
// Also keeps a reference to the old clconf tree during the transition from
// old tree to new tree until the topology manager callback is done.
// Any reference to the old tree after the did update or recovery callback
// can lead to error as we might be referring to freed memory.
class cl_current_tree {
public:
	static int initialize();
	static void shutdown();

	static cl_current_tree &the();
	cl_current_tree &cl_tree(const char *);

	//
	// Reads in a clconf tree from CCR. Returns true if a new tree was
	// read in. If the return code is false the errorcode is set to
	// true if there was an error. The error code is false the tree was
	// not read because there was no need to.
	//
	bool read_ccr(bool *errorcodep);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	int add_cluster(const char *cluster_namep);
	uint32_t get_num_virtual_clusters();
	char **get_virtual_cluster_names();
#ifndef _KERNEL_ORB
	int refresh_zc_clconf_trees();
#endif	// !_KERNEL_ORB
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	// Writes the proposed tree to CCR.
	bool write_ccr(clnode *proposed_tree);

#ifndef _KERNEL
	bool write_nonclust_ccr(clnode *proposed_tree);
#endif
	bool lock_ccr(clnode *);

	void unlock_ccr();

	// set the clconf_io.
	void set_io_interface(clconf_io *);

	// Returns the root of the current clconf tree.
	clnode *get_root();
#ifdef _KERNEL_ORB
	// Returns the root of the old clconf tree.
	clnode *get_old_root();

	void destroy_old_root();

	void set_oldroot_to_root();
#endif

	// Set the pointer to the root of current tree.
	// The new root will have an incn_num that's one bigger that
	// the previous tree.
	void set_root(clnode *);

	cl_current_tree(const char *);
	~cl_current_tree();
#ifdef DEBUGGER_PRINT
	void debugger_print();
	clnode *mdb_get_root();
#endif

private:
	// Methods cl_current_tree::get_root() and cl_current_tree::read_ccr()
	// compete for the same lock. cl_current_tree::read_ccr() does memory
	// allocation using new, which can block leading to deadlock between
	// two threads calling these methods. This is undesirable if we want
	// to support calling these functions in clock interrupt context.
	// So the lock has been split in two locks so that one thread does block
	// indefinitely if the other is blocked in malloc.

	// Pointer to the clconf_io interface through which we can
	// access the configuration data.
	clconf_io	*ccr_ptr;
	os::mutex_t	ccr_read_lock;   // protects ccr_ptr during ccr reads

	clnode		*root;		// the root of the current clconf tree.
	os::mutex_t	root_lock;	// protects root
#ifdef _KERNEL_ORB
	clnode		*old_root;	// Also protected by root_lock
#endif

	char		*cluster_namep;

	int32_t		current_version; // version for global cluster

	static cl_current_tree *the_cl_current_tree;
};

// The refcount and incarnation for a clconf tree.
class clrefcnt : public refcnt {
	friend class cl_current_tree;
	friend class clnode;
public:
	clrefcnt(clnode *);
	~clrefcnt();
private:
	uint_t incn_num;	// the incarnation number of the tree.
	clnode *root;		// the root of the clconf tree.
};

// The node of a clconf tree. It contains a pointer to the refcount object for
// this tree (one refcount per tree)
//
class clnode : public ccr_node {
	friend class cl_current_tree; // need to access cnt.
public:
	// Add/remove a child from the specified group. The clconf tree
	// has some 'group' nodes. Add to a group means make a clnode
	// the direct child of the specified group node. For example, in
	// "cluster.nodes.1" the clnode corresponding to "nodes" is a
	// group node.
	clconf_errnum_t add_to_group(const char *grpname, clnode *node);
	clconf_errnum_t remove_from_group(const char *grpname, clnode *node);

	// Override the parent's methods.
	ccr_node *dup();
	ccr_node *create_node(const char *);

	// Inc/dec the refcount.
	void hold();
	void rele();

	// Set up the parent pointer.
	void set_parent(ccr_node *);

	// Give the root node a chance to do something special. When copying
	// a tree/branch, the parameter passed in is the corresponding
	// node on the tree being copied. When creating a new tree, the
	// parameter is NULL.
	//
	// When copying a whole tree or creating a new tree
	// it'll create a new clrefcnt object.
	// When copying a branch, it'll points cnt to that of the old tree.
	void initroot(ccr_node *);

	// Get the incarnation number.
	uint_t get_incn();

	// Constructors.
	clnode(clnode *);
	clnode(const char *);
#ifdef DEBUGGER_PRINT
	void get_nodes(char **, int &);
#endif
private:
	// Helper function. Setup the name(id) of this node to the next
	// available one in the parent's name space. We only call this
	// function on the nodes that represent clconf_obj (clconf_node for
	// instance). The name for those clnodes are always a number.
	void setup_name();

	clrefcnt *cnt;		// Pointer to the refcnt for the whole tree.
};

#include <clconf/clnode_in.h>


#endif	/* _CLNODE_H */
