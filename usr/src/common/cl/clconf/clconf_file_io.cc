/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clconf_file_io.cc	1.27	08/05/20 SMI"

#include <h/ccr.h>
#include <clconf/clconf_file_io.h>
#include <sys/clconf_int.h>
#include <clconf/clnode.h>

bool
clconf_file_io::initialize()
{
	bool	error;

	lck.lock();
	if (is_initialized) {
		lck.unlock();
		return (true);
	}
	if (cl_current_tree::initialize() != 0) {
		lck.unlock();
		return (false);
	}

	cl_current_tree::the().set_io_interface(this);
	if (!cl_current_tree::the().read_ccr(&error)) {
		lck.unlock();
		return (false);
	}
	is_initialized = true;
	lck.unlock();
	return (true);
}


void
clconf_file_io::shutdown()
{
	lck.lock();
	if (!is_initialized) {
		lck.unlock();
		return;
	}

	// Frees up the hold on the current tree. This should delete
	// the tree assuming everyone who did a hold did a rele.
	cl_current_tree::the().set_root(NULL);

	is_initialized = false;
	lck.unlock();
}

clconf_file_io::clconf_file_io(const char *fn) : read_tb(NULL),
    write_tb(NULL),
    is_initialized(false)
{
	filename = os::strdup(fn);
	write_filename = new char[os::strlen(filename) + 5]; // room for suffix
	os::sprintf(write_filename, "%s.new", filename);

}

clconf_file_io::~clconf_file_io()
{
	ASSERT(read_tb == NULL);	// caller should do close first.
	ASSERT(write_tb == NULL);	// caller should do close first.
	delete[] filename;
	filename = NULL;
	delete[] write_filename;
	write_filename = NULL;
}

clconf_io::status
clconf_file_io::open()
{
	read_tb = new readonly_persistent_table;
	write_tb = new updatable_persistent_table;

	if (read_tb->initialize(filename) != 0) {
		delete read_tb;
		read_tb = NULL;
		return (C_FAIL);
	}

	if (write_tb->initialize(write_filename) != 0) {
		delete write_tb;
		write_tb = NULL;
		return (C_FAIL);
	}

	return (C_SUCCESS);
}

void
clconf_file_io::close()
{
	if (read_tb != NULL) {
		read_tb->close();
		delete read_tb;
		read_tb = NULL;
	}
	if (write_tb != NULL) {
		(void) write_tb->close();
		delete write_tb;
		write_tb = NULL;
	}
}

//
// read_one
//
// This function returns a pair of pointers to the next entry key/value.
// The caller is responsible for deleting the two buffers in argument
// after their consumption (note the "pointer to a char pointer" arguments).
//
clconf_io::status
clconf_file_io::read_one(char **key, char **value)
{
	table_element_t *telp;
	int retval;

	if (read_tb == NULL) {
		return (C_FAIL);
	}

	if ((telp = read_tb->next_element(retval)) == NULL) {
		return (C_FAIL);
	}

	*key = telp->key;
	*value = telp->data;

	telp->key = NULL;
	telp->data = NULL;
	delete telp;
	return (C_SUCCESS);
}

int32_t
clconf_file_io::get_version()
{
	return (0);
}

bool
clconf_file_io::begin_transaction()
{
	return (false);
}

bool
clconf_file_io::commit_transaction()
{
	ASSERT(0);
	return (false);
}

void
clconf_file_io::abort_transaction()
{
	ASSERT(0);
}

// Create an element_seq
void *
clconf_file_io::create_seq()
{
	return (new ccr::element_seq(0));
}

// Delete an element_seq
void
clconf_file_io::delete_seq(void *seq)
{
	delete(ccr::element_seq *)seq;
}

int
clconf_file_io::append_node(ccr_node *nd, void *vp)
{
	if (nd->is_leaf()) {
		ccr::element_seq *esp = (ccr::element_seq *) vp;
		unsigned int len = esp->length();
		esp->length(len+1);
		// const char * ensures that a copy is made of the string
		// We do not have to take a copy of the full_name
		// because we have already alloc'ed memory in
		// get_full_name() that needs to be freed with element_seq.
		// Since get_value() does not allocate memory we have
		// to make a copy of it by using const char *
		(*esp)[len].key = nd->get_full_name();
		(*esp)[len].data = (const char *)nd->get_value();
	}
	return (1);
}

bool
clconf_file_io::write(const char *, const char *)
{
	return (0);
}

int
clconf_file_io::write_seq(void *seq)
{
	ccr::element_seq *esp = (ccr::element_seq *)seq;
	int err;

	for (uint_t i = 0; i < esp->length(); i++) {
		err = write_tb->insert_element((*esp)[i].key, (*esp)[i].data);
		if (err)
			return (1);
	}
	return (0);
}
