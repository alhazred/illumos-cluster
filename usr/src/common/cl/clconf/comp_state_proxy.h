/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  comp_state_proxy.h
 *
 */

#ifndef _COMP_STATE_PROXY_H
#define	_COMP_STATE_PROXY_H

#pragma ident	"@(#)comp_state_proxy.h	1.13	08/05/20 SMI"

#include <sys/os.h>
#include <sys/sc_syslog_msg.h>
#include <sys/threadpool.h>

typedef enum proxy_state_stat_enum {
	UNDEF = 0,	// Not defined
	TRANSIENT,	// Transient state; not to be logged in syslog
	FINAL		// Important state to be logged in syslog
} proxy_state_stat_t;

//
// The Component State Proxy
//
class component_state_proxy {
public:
	enum component_type_t {
	    COMP_PATHEND = 0,		// Sample usage
	    COMP_ADAPTER,
	    LAST_COMPONENT_TYPE
	};

	// constructor - initialize the on_queue_flag
	component_state_proxy();

	// destructor
	virtual ~component_state_proxy();

	// Abstract methods

	// Get component name
	// this is the unique internal name of a component within a type
	virtual const char *get_name() = 0;

	// this is the unique external name of a component within a type
	virtual const char *get_extname() = 0;

	// Get component type
	// the type of component
	virtual const char *get_type();

	// external state. (Mapped from internal state)
	virtual sc_state_code_t get_state() = 0;

	// state string (mapped from internal state)
	virtual const char *get_state_string() = 0;

	// state format string (mapped from internal state) for syslog use
	virtual const char *get_state_fmtstring() = 0;

	// Locking can be redefined by tha subclass if we want to use a
	// different lock.  Otherwise the locking on component_state_proxy
	// will use a single mutex in this object to protect all operations
	// on this object.
	virtual void proxy_lock();
	virtual void proxy_unlock();
	virtual bool proxy_lock_held();

	// register with csr
	// This method will register this proxy with the csr, and
	// send a ADDED event to clust-log.
	int register_with_csr();

	// unregister from csr
	// this method will un-register this proxy from the csr, and
	// send a REMOVED event to clust-log.
	int unregister_from_csr();

protected:
	//
	// Each subclass must implemnt _type to return the type of
	// component that it is monitoring
	virtual component_type_t _type() = 0;

	// Lock used for access to this object (can be overriden)
	// by re-writing the virtual lock/unlock/lock_held methods
	os::mutex_t	csp_lock;

	// post_event_to_syslog
	// will post the current event to syslog
	void post_event_to_syslog(int event, const char *msg);
}; // component_state_proxy

#include <clconf/comp_state_proxy_in.h>

#endif	/* _COMP_STATE_PROXY_H */
