/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLCONF_CCR_H
#define	_CLCONF_CCR_H

#pragma ident	"@(#)clconf_ccr.h	1.39	08/09/27 SMI"

#include <h/ccr.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <clconf/clconf_io.h>
#include <clconf/clnode.h>
#include <sys/sol_version.h>

#define	CLCONF_TABLE "infrastructure"

// The callback that registers with CCR to get events about updates
// to the infrastructure table in CCR.
class clconf_infr_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update(
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_update_zc(
		    const char *clusterp,
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_recovery(
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_recovery_zc(
		    const char *clusterp,
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void _unreferenced(unref_t);
};

//
// A generic callback that registers with CCR to get events
// about updates to a particular CCR table.
//
class clconf_ccr_table_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update(
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_update_zc(
		    const char *clusterp,
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_recovery(
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_recovery_zc(
		    const char *clusterp,
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void _unreferenced(unref_t);

	clconf_ccr_table_callback_impl(
	    clconf_ccr_callback_handler, const char*);
private:
	// The handler to be invoked by did_update() & did_recovery()
	clconf_ccr_callback_handler ccr_cb_hdlr;
	char *tnamep;
};

// The callback that registers with CCR to get events about all
// CCR changes, used by scsymon.
class clconf_ccr_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update(
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_update_zc(
		    const char *clusterp,
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_recovery(
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void did_recovery_zc(
		    const char *clusterp,
		    const char *table_namep,
		    ccr::ccr_update_type,
		    Environment &);
	void _unreferenced(unref_t);

	clconf_ccr_callback_impl(clconf_ccr_callback_handler);
private:
	// The handler to be invoked by did_update() & did_recovery()
	clconf_ccr_callback_handler ccr_cb_hdlr;
};

// The io interface that goes through CCR to access configuration data.
class clconf_ccr : public clconf_io {
	friend class cl_current_tree;
public:
	static clconf_ccr &the();
	clconf_ccr &vc_clconf(const char *);

	// Initialize the clconf tree. It will read from CCR, construct
	// the tree and register callback with CCR. Returns true on
	// success, false if fail.
	static int initialize();
	int initialize_int();

	static void shutdown();
	void shutdown_int();

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// Read the virtual cluster configuration data from the CCR.
	// If in kernel, also register the infrastructure callbacks
	// for the virtual clusters.
	//
	int read_vc_data();

	//
	// Reads in data for a virtual cluster into the clconf tree.
	// Returns true on success, and false on failure.
	//
	bool read_single_vc_data(const char *);

	// Common function used by the above two methods
	bool read_vc_data_common(const char *, uint32_t);

	//
	// Clean up the virtual cluster clconf objects and data.
	//
	void cleanup_vc_data();

	// Do the cleanup for a single virtual cluster
	void cleanup_single_vc_data(const char *);

	// Common function used by the above two methods
	void cleanup_vc_data_common(const char *);

#ifdef	_KERNEL
	//
	// clconf gets notified that CCR is recoverd.
	// clconf needs this callback to determine if one or more
	// zone clusters were added/removed while this node was down.
	// As part of this call, clconf updates its data structures
	// synchronously, if required.
	//
	void ccr_is_refreshed_callback_for_clconf();
#endif	// _KERNEL

	//
	// Checks to see if the zone clusters that clconf knows about are
	// the same as those present in CCR.
	// When executed in userland, the method also makes sure that
	// the clconf trees of the zone clusters have
	// the latest data from infrastructure tables in CCR.
	//
	// Returns 0 on success, error code on failure.
	//
	int check_zone_cluster_list();
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#ifdef _KERNEL_ORB
	// Register callback with infrastructure file. Returns true on success.
	bool register_infr_callback();
	void unregister_infr_callback();
#endif

	//
	// Register callback with a particular CCR file.
	// Returns true on success.
	//
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	bool register_ccr_table_callback(
	    const char *, const char *, ccr::callback_ptr);
	void unregister_ccr_table_callback(
	    const char *, const char *, ccr::callback_ptr);
#else
	bool register_ccr_table_callback(const char *, ccr::callback_ptr);
	void unregister_ccr_table_callback(const char *, ccr::callback_ptr);
#endif	// (SOL_VERSION >= __s10) && defined(UNODE)

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	void cldir_callback(const char *, const char *, clconf_ccr_update_t);
	void vc_dir_callback(const char *, const char *, clconf_ccr_update_t);
	void vc_dir_callback_cleanup(const char *);
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

	// Register callback for any CCR changes.
	clconf_errnum_t register_ccr_callback(clconf_ccr_callback_handler);
	clconf_errnum_t unregister_ccr_callback();

	clconf_ccr(const char *);
	~clconf_ccr();

	// Get the ccr directory pointer.
	bool lookup_dir();

	// Open a read CCR session.
	clconf_io::status open();
	// Close a read CCR session.
	void close();
	// Read one element from CCR.
	clconf_io::status read_one(char **key, char **value);

	// for write.
	bool begin_transaction();
	bool commit_transaction();
	void abort_transaction();
	// write one element to CCR.
	bool write(const char *key, const char *value);
	// write a sequence to CCR.
	int write_seq(void *seq);

	// Helper functions to create and delete a sequence.
	// These functions hide the implementation of the sequence,
	// which is why they return a void *.
	void *create_seq();
	void delete_seq(void *seq);
	// The callback function for ccr_node::traverse. The nd is the
	// ccr_node to be printed out to the CCR. The sequence is passed
	// in seq.
	static int append_node(ccr_node *nd, void *seq);

	int32_t  get_version();

#if (SOL_VERSION >= __s10)
	int32_t	get_cluster_id(char *clusterp, uint32_t &clid);
	int32_t	get_cluster_name(uint32_t clid, char **clusterp);
#endif // (SOL_VERSION >= __s10)

private:

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	bool register_cldir_callback();
	void unregister_cldir_callback();
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

#ifndef _KERNEL_ORB
	os::mutex_t &get_clconf_ccr_lock(const char *);
#endif	// _KERNEL_ORB

	ccr::directory_ptr dir_ptr;
	ccr::readonly_table_ptr read_ptr;
	ccr::updatable_table_ptr write_ptr;

#ifdef _KERNEL_ORB
	// cb for infrastructure table changes
	ccr::callback_ptr infr_cb_p;
#endif	// _KERNEL_ORB

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	// cb for cluster directory table changes
	ccr::callback_ptr cldir_cb_p;
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

	//
	// cb corresp to any ccr change.
	// If non-nil, indicates that a callback handler is registered.
	//
	ccr::callback_ptr ccr_cb_p;

	char *cluster_namep;

	static clconf_ccr *the_clconf_ccr;

	// Lock for single-threading the execution of (un)register_ccr_cb.
	static os::mutex_t clconf_ccr_cb_lock;
#ifndef	_KERNEL_ORB
	static os::mutex_t clconf_ccr_lock;
#endif
	//
	// This lock will be used to protect
	// the ccr::directory_ptr dir_ptr
	// variable
	//
	os::rwlock_t dir_lock;
};

#endif	/* _CLCONF_CCR_H */
