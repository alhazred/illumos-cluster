/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clconf_ccr.cc	1.84	08/09/27 SMI"

#include <clconf/clconf_ccr.h>
#include <cmm/cmm_ns.h>
#include <sys/cl_assert.h>
#include <sys/cl_comm_support.h>

#ifdef _KERNEL_ORB
#include <tm/kernel_tm.h>
#endif /* _KERNEL_ORB */

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
#include <cmm/membership_engine_impl.h>
#include <orb/infrastructure/clusterproc.h>
void cldir_callback(const char *, const char *, clconf_ccr_update_t);
clconf_ccr_callback_handler cldir_callback_fn = cldir_callback;
void vc_dir_callback(const char *, const char *, clconf_ccr_update_t);
clconf_ccr_callback_handler vc_dir_callback_fn = vc_dir_callback;
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <sys/hashtable.h>
#include <sys/vc_int.h>

//
// For each virtual cluster, there is a clconf object created.
// The locks are similar to what we have for the physical cluster
// clconf object. Each clconf object is associated with its own
// clconf tree.
// The hashtable stores the clconf objects for all virtual clusters.
// This hashtable is populated when the clconf initializes in global zone only.
// Inside non-global zones (virtual clusters as well as non-cluster-wide zones),
// these pointers are NULL.
//
struct vc_clconf_data {
	clconf_ccr	*the_vc_clconf_ccrp;
	os::mutex_t	vc_clconf_ccr_cb_lock;
#ifndef _KERNEL_ORB
	os::mutex_t	vc_clconf_ccr_lock;
#endif	// _KERNEL_ORB
};
typedef struct vc_clconf_data vc_clconf_data_t;
string_hashtable_t<vc_clconf_data_t *> *vc_clconf_data_tablep = NULL;

#ifdef _KERNEL
//
// When a virtual cluster is created, the cluster_directory table
// is first updated and clconf gets a callback. After that the infrastructure
// table for the virtual cluster is created. So in order for clconf to
// know that the infrastructure table of the virtual cluster is ready,
// clconf needs to register a callback for the directory table of the
// virtual cluster. Once the infrastructure table is created (which is a
// transaction on the virtual cluster directory table), clconf gets a callback,
// reads the infrastructure table as part of the callback and unregisters
// the virtual cluster directory table callback as it is no longer needed.
//
struct vc_dir_cb {
	ccr::callback_ptr vc_dir_cb_p;
	char *cl_namep;
};
SList<struct vc_dir_cb> *vc_dir_cb_listp = NULL;
os::mutex_t *vc_dir_cb_list_lockp = NULL;
#endif	// _KERNEL
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#ifndef	_KERNEL_ORB
os::mutex_t clconf_ccr::clconf_ccr_lock;
#endif
os::mutex_t clconf_ccr::clconf_ccr_cb_lock;
clconf_ccr *clconf_ccr::the_clconf_ccr = NULL;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
//
// Get the name of the cluster in which the clconf code is running.
// Returns DEFAULT_CLUSTER for global zone and non-cluster-wide zones.
// Returns zonename for cluster-wide zones (zone clusters).
// Caller is responsible for freeing up the memory allocated
// for the cluster name.
// Returns NULL in case of failure to get the cluster name.
//
char *
get_clname_for_clconf()
{
	int			retval = 0;
	Environment		e;
	CORBA::Exception	*exp = NULL;
	uint_t			clid;
	char			*zonenamep = new char[ZONENAME_MAX];
	ccr::directory_ptr	dir_ptr;

	if (getzoneid() == GLOBAL_ZONEID) {
		(void) os::strcpy(zonenamep, DEFAULT_CLUSTER);
		return (zonenamep);
	}

	// Get a reference to the directory object
	naming::naming_context_var ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
#ifdef DEBUG
		os::warning("can't find local name server");
#endif
		return (NULL);
	}
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
#ifdef DEBUG
		os::warning("ccr_directory not found in name server");
#endif
		e.clear();
		return (NULL);
	}
	dir_ptr = ccr::directory::_narrow(obj);

	// Get current zonename
#ifdef _KERNEL_ORB
	zone_t *zonep = zone_find_by_id(getzoneid());
	if ((zonep == NULL) || (zonep->zone_name == NULL)) {
		if (zonep != NULL) {
			zone_rele(zonep);
		}
		return (NULL);
	}
	(void) os::strcpy(zonenamep, zonep->zone_name);
	zone_rele(zonep);
#else	// _KERNEL_ORB
	if (getzonenamebyid(getzoneid(), zonenamep, ZONENAME_MAX) == -1) {
			return (NULL);
	}
#endif	// _KERNEL_ORB

	// Get the cluster id
	clid = dir_ptr->zc_getidbyname(zonenamep, e);
	if ((exp = e.exception()) != NULL) {
		//
		// Should be able to convert current zone name to
		// a valid cluster id.
		//
		ASSERT(0);
		e.clear();
		return (NULL);
	}
	if (clid == BASE_NONGLOBAL_ID) {
		// Native non-global zone.
		(void) os::strcpy(zonenamep, DEFAULT_CLUSTER);
	}
	return (zonenamep);
}
#else	// (SOL_VERSION >= __s10) && !defined(UNODE)
char *
get_clname_for_clconf()
{
	char *zonenamep = os::strdup(DEFAULT_CLUSTER);
	return (zonenamep);
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#ifndef _KERNEL_ORB
#if (SOL_VERSION >= __s10)
//
// Get the clconf lock (which is used in userland clconf code).
// Assumption here is that only a global zone entity queries
// info about virtual clusters. Since the code using this is clconf,
// we can make this assumption. The alternative is we have access checks
// here, but it would involve a bunch of IDL calls and function calls,
// unnecessarily slowing down stuff.
//
os::mutex_t &
clconf_ccr::get_clconf_ccr_lock(const char *cl_namep)
{
	if (getzoneid() == GLOBAL_ZONEID) {
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			return (clconf_ccr::clconf_ccr_lock);
		} else {
			return (vc_clconf_data_tablep->lookup(cl_namep)->
			    vc_clconf_ccr_lock);
		}
	} else {
		return (clconf_ccr::clconf_ccr_lock);
	}
}
#else	// (SOL_VERSION >= __s10)
os::mutex_t &
clconf_ccr::get_clconf_ccr_lock(const char *cl_namep)
{
	return (clconf_ccr::clconf_ccr_lock);
}
#endif	// (SOL_VERSION >= __s10)
#endif	// _KERNEL_ORB

//
// Get the clconf object of the cluster we are in.
// There is a special case here for kernel threads.
// If a kernel thread running in the context of a non-global zone
// calls this method, it gets the clconf object it is entitled to.
// That is, if the thread is for a non-cluster-wide zone,
// then it gets the kernel global zone clconf object.
// Else if the thread is for a cluster-wide zone, it gets the clconf
// object for the virtual cluster which is stored in the kernel.
//
clconf_ccr &
clconf_ccr::the()
{
	ASSERT(the_clconf_ccr != NULL);

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	if (getzoneid() != GLOBAL_ZONEID) {
		char *cl_namep = get_clname_for_clconf();
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			// Non-cluster-wide zone
			return (*the_clconf_ccr);
		}
		// virtual cluster clconf object
		vc_clconf_data_t *datap =
		    vc_clconf_data_tablep->lookup(cl_namep);
		ASSERT(datap != NULL);
		ASSERT(datap->the_vc_clconf_ccrp != NULL);
		return (*(datap->the_vc_clconf_ccrp));
	}
#endif
	return (*the_clconf_ccr);
}

//
// Get the clconf object for a particular cluster.
// Anyone calling this method from a non-global zone context would
// get the clconf object for the cluster the zone is a part of.
// If the caller is in global zone, then it can get the clconf object
// of any virtual cluster or of the global zone cluster.
//
clconf_ccr &
clconf_ccr::vc_clconf(const char *cl_namep)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	if (getzoneid() != GLOBAL_ZONEID) {
		//
		// Cannot obtain the clconf object of another cluster
		// from inside a virtual cluster.
		//
		ASSERT(os::strcmp(cl_namep, cluster_namep) == 0);
		return (clconf_ccr::the());
	} else {
		// Global zone
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			return (clconf_ccr::the());
		} else {
			// request for the clconf object of a virtual cluster
			vc_clconf_data_t *datap =
			    vc_clconf_data_tablep->lookup(cl_namep);
			ASSERT(datap != NULL);
			ASSERT(datap->the_vc_clconf_ccrp != NULL);
			return (*(datap->the_vc_clconf_ccrp));
		}
	}
#else	// (SOL_VERSION >= __s10) && !defined(UNODE)
	return (clconf_ccr::the());
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)
}

// static
int
clconf_ccr::initialize()
{
	int err;

#ifdef	_KERNEL_ORB
	ASSERT(the_clconf_ccr == NULL);
#else
	clconf_ccr_lock.lock();
	if (the_clconf_ccr != NULL) {
		clconf_ccr_lock.unlock();
		return (0);
	}
#endif

	char *cl_namep = get_clname_for_clconf();
	if (cl_namep == NULL) {
#ifdef DEBUG
		os::warning("could not get current cluster name\n");
#endif
#ifndef	_KERNEL_ORB
		clconf_ccr_lock.unlock();
#endif
		return (ENOENT);
	}

	the_clconf_ccr = new clconf_ccr(cl_namep);
	if (the_clconf_ccr == NULL) {
#ifndef	_KERNEL_ORB
		clconf_ccr_lock.unlock();
#endif
		delete [] cl_namep;
		return (ENOMEM);
	}

	// initialize_int() will drop the lock;
	// XXX is it ok to initialize zone cluster clconf data below
	// XXX without holding the clconf ccr lock?
	err = the_clconf_ccr->initialize_int();
	if (err) {
		clconf_ccr::shutdown();
		delete [] cl_namep;
		return (err);
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If we are running in global zone, then create and initialize
	// the clconf objects for the configured virtual clusters.
	//
	if (getzoneid() == GLOBAL_ZONEID) {
		vc_clconf_data_tablep =
		    new string_hashtable_t<vc_clconf_data_t *>;
		ASSERT(vc_clconf_data_tablep);
#ifdef _KERNEL
		vc_dir_cb_listp = new SList<struct vc_dir_cb>;
		ASSERT(vc_dir_cb_listp);
		vc_dir_cb_list_lockp = new os::mutex_t;
		ASSERT(vc_dir_cb_list_lockp);
#endif	// _KERNEL

		err = the_clconf_ccr->read_vc_data();
		if (err) {
#ifdef DEBUG
			os::warning(
			    "failed to read virtual cluster data from CCR\n");
#endif
			delete [] cl_namep;
			return (err);
		}
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	delete [] cl_namep;
	return (err);
}

#ifdef _KERNEL_ORB
int
clconf_ccr::initialize_int()
{
	bool error;
	if (!lookup_dir()) {
		return (ENOENT);
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If we are running in global zone and if this clconf object is
	// for a virtual cluster, then add cluster to the list of clconf trees.
	//
	if (getzoneid() == GLOBAL_ZONEID) {
		if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) != 0) {
			// New clconf for a virtual cluster
			int err =
			    cl_current_tree::the().add_cluster(cluster_namep);
			if (err) {
				return (err);
			}
		}
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	// Get the clconf tree for this cluster
	cl_current_tree &tree = cl_current_tree::the().cl_tree(cluster_namep);
	tree.set_io_interface(this);

	/*
	 * Infrastructure callback is registered only in kernel/unode mode.
	 * In userland we just have to update the clconf tree when
	 * infrastructure file is modified. This can be achieved by not
	 * using callbacks(see clnode.cc). In kernel, we not only update
	 * the clconf tree but also update the topology manager, path
	 * manager, and quorum subsystem. So we need the callback in kernel.
	 */
	if (!register_infr_callback()) {
#ifdef DEBUG
#if (SOL_VERSION >= __s10)
		os::warning("unable to register clconf callback with CCR "
		    "for cluster %s", cluster_namep);
#else
		os::warning("unable to register clconf callback with CCR ");
#endif	// (SOL_VERSION >= __s10)
#endif	// DEBUG
		return (EIO);
	}

	if (!tree.read_ccr(&error)) {
#ifdef DEBUG
#if (SOL_VERSION >= __s10)
		os::warning("failed to read clconf from ccr for cluster %s",
		    cluster_namep);
#else
		os::warning("failed to read clconf from ccr for cluster %s");
#endif	// (SOL_VERSION >= __s10)
#endif	// DEBUG
		return (EIO);
	}
	// No need to release the old root here because it should be NULL.

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	//
	// If this clconf object being initialized is for the global zone,
	// and if we are running in kernel, then register a callback
	// for the cluster_directory table.
	//
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0) {
		if (!register_cldir_callback()) {
#ifdef DEBUG
			os::warning("unable to register %s callback with CCR",
			    CLUSTER_DIR);
#endif
			return (EIO);
		}
	}
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

	CLCONF_DBG(("clconf_ccr::initialize successful\n"));
	return (0);
}

#else	// _KERNEL_ORB
int
clconf_ccr::initialize_int()
{
	bool error;
	os::mutex_t &lock = get_clconf_ccr_lock(cluster_namep);

	ASSERT(lock.lock_held());
	if (!lookup_dir()) {
		lock.unlock();
		return (ENOENT);
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If we are running in global zone and if this clconf object is
	// for a virtual cluster, then add cluster to the list of clconf trees.
	//
	if (getzoneid() == GLOBAL_ZONEID) {
		if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) != 0) {
			// New clconf for a virtual cluster
			int err =
			    cl_current_tree::the().add_cluster(cluster_namep);
			if (err) {
				lock.unlock();
				return (err);
			}
		}
	}
#endif

	// Get the clconf tree for this cluster
	cl_current_tree &tree = cl_current_tree::the().cl_tree(cluster_namep);
	tree.set_io_interface(this);

	if (!tree.read_ccr(&error)) {
#ifdef DEBUG
#if (SOL_VERSION >= __s10)
		os::warning("failed to read clconf from ccr for cluster %s",
		    cluster_namep);
#else
		os::warning("failed to read clconf from ccr for cluster %s");
#endif	// (SOL_VERSION >= __s10)
#endif	// DEBUG
		lock.unlock();
		return (EIO);
	}

	// No need to release the old root here because it should be NULL.

	CLCONF_DBG(("clconf_ccr::initialize successful\n"));
	lock.unlock();
	return (0);
}
#endif	// _KERNEL_ORB

void
clconf_ccr::shutdown_int()
{
#ifdef _KERNEL_ORB
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// For zone clusters, there is no need to unregister
	// the infrastructure callback. This is because, by the time
	// clconf comes to know about the deletion of a zone cluster,
	// the infrastructure table for the zone cluster is already
	// gone and so are the callbacks registered for the table.
	//
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0) {
		if (is_not_nil(infr_cb_p)) {
			unregister_infr_callback();
			ASSERT(CORBA::is_nil(infr_cb_p));
		}
	} else {
		CORBA::release(infr_cb_p);
		infr_cb_p = nil;
	}
#else	// (SOL_VERSION >= __s10) && !defined(UNODE)
	if (is_not_nil(infr_cb_p)) {
		unregister_infr_callback();
		ASSERT(CORBA::is_nil(infr_cb_p));
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)
#endif	// _KERNEL_ORB

	dir_lock.wrlock();
	CORBA::release(dir_ptr);
	dir_ptr = nil;
	dir_lock.unlock();

	// If ccr_cb_p is not nil, release it.
	if (is_not_nil(ccr_cb_p)) {
		// Unregister will do the release.
		(void) unregister_ccr_callback();
	}

	ASSERT(CORBA::is_nil(ccr_cb_p));
	ASSERT(CORBA::is_nil(read_ptr));
	ASSERT(CORBA::is_nil(write_ptr));

	//
	// Frees up the hold on the current tree. This should delete
	// the tree assuming everyone who did a hold did a rele.
	//
	cl_current_tree::the().cl_tree(cluster_namep).set_root(NULL);
}

// static
void
clconf_ccr::shutdown()
{
	if (the_clconf_ccr == NULL) {
		return;
	}

#ifndef	_KERNEL_ORB
	clconf_ccr_lock.lock();
#endif	// _KERNEL_ORB

	CLCONF_DBG(("clconf_ccr::shutdown\n"));

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If we are running in global zone, then the virtual cluster
	// clconf data should be cleaned up.
	//
	if (getzoneid() == GLOBAL_ZONEID) {
		the_clconf_ccr->cleanup_vc_data();
#ifdef _KERNEL
		// Unregister the cluster directory callback, if registered
		if (is_not_nil(the_clconf_ccr->cldir_cb_p)) {
			the_clconf_ccr->unregister_cldir_callback();
			ASSERT(CORBA::is_nil(the_clconf_ccr->cldir_cb_p));
		}
#endif	// _KERNEL
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	the_clconf_ccr->shutdown_int();
	delete the_clconf_ccr;
	the_clconf_ccr = NULL;
#ifndef	_KERNEL_ORB
	clconf_ccr_lock.unlock();
#endif
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
//
// Read the virtual cluster configuration data from CCR.
//
int
clconf_ccr::read_vc_data()
{
	bool			error;
	Environment		e;
	CORBA::Exception	*exp = NULL;
	CORBA::StringSeq	*cl_namesp = NULL;
	CORBA::StringSeq	*cl_idsp = NULL;
	char			**namesp = NULL;
	uint_t			num_clusters;
	uint_t			index;

	dir_lock.rdlock();
	dir_ptr->zc_get_names_and_ids(cl_namesp, cl_idsp, e);
	dir_lock.unlock();
	exp = e.exception();
	if (exp != NULL) {
		ASSERT(ccr::invalid_zc_access::_exnarrow(exp) != NULL);
		if (cl_namesp) {
			delete cl_namesp;
		}
		if (cl_idsp) {
			delete cl_idsp;
		}
		e.clear();
		return (EIO);
	}
	ASSERT(cl_namesp->length() == cl_idsp->length());

	//
	// The data about cluster names and ids obtained from
	// directory object contains the global cluster too.
	// Here we are interested in virtual clusters only.
	//
	num_clusters = cl_namesp->length();
	namesp = new char*[num_clusters];
	ASSERT(namesp != NULL);
	for (index = 0; index < num_clusters; index++) {
		namesp[index] = (char *)(*cl_namesp)[index];
	}

	for (index = 0; index < num_clusters; index++) {
		char *cl_namep = namesp[index];
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			// This routine does not work for global cluster
			continue;
		}
		uint32_t cl_id = (uint32_t)os::atoi((char *)(*cl_idsp)[index]);
		error = read_vc_data_common(cl_namep, cl_id);
	}

	delete [] namesp;
	delete cl_namesp;
	delete cl_idsp;
	return (0);
}

//
// Reads in data for a single virtual cluster.
// Returns true on success, and false on failure.
//
bool
clconf_ccr::read_single_vc_data(const char *cl_namep)
{
	Environment		e;
	CORBA::Exception	*exp = NULL;
	uint32_t		cl_id;

	dir_lock.rdlock();
	ASSERT(os::strcmp(cl_namep, DEFAULT_CLUSTER) != 0);
	ASSERT(is_not_nil(dir_ptr));

	cl_id = dir_ptr->zc_getidbyname(cl_namep, e);
	dir_lock.unlock();
	exp = e.exception();
	if (exp != NULL) {
		if (ccr::no_such_zc::_exnarrow(exp) != NULL) {
#ifdef DEBUG
			os::warning("unable to get cluster id for cluster %s\n",
			    cl_namep);
#endif
			e.clear();
			return (false);
		} else {
			ASSERT(0);
			e.clear();	/*lint !e527 */
			return (false);	/*lint !e527 */
		}
	}

	if (!read_vc_data_common(cl_namep, cl_id)) {
		return (false);
	}

	return (true);
}

//
// Creates a clconf object for a particular virtual cluster
// and puts an entry for the clconf object in the table.
// If the entry already exists, the method does not overwrite
// the entry and returns false.
// Returns true on success, and false on failure
//
bool
clconf_ccr::read_vc_data_common(const char *cl_namep, uint32_t cl_id)
{
	bool retval = true;

	if (vc_clconf_data_tablep->lookup(cl_namep) != NULL) {
		// Entry exists in table
		return (false);
	}

	// Entry does not exist in table
	vc_clconf_data_t *datap = new vc_clconf_data_t;
	if (datap == NULL) {
		return (false);
	}
#ifndef _KERNEL_ORB
	datap->vc_clconf_ccr_lock.lock();
#endif

	clconf_ccr *clconfp = new clconf_ccr(cl_namep);
	if (clconfp == NULL) {
		return (false);
	}

	datap->the_vc_clconf_ccrp = clconfp;
	vc_clconf_data_tablep->add(datap, cl_namep);

	// initialize_int() will drop the lock;
	retval = clconfp->initialize_int();
	if (retval) {
		clconfp->shutdown_int();
	}
	return (retval);
}

//
// Clean up the clconf objects and data for all virtual clusters.
//
void
clconf_ccr::cleanup_vc_data()
{
	// If there is no zone cluster clconf data, then simply return.
	if (vc_clconf_data_tablep == NULL) {
		return;
	}

	vc_clconf_data_t *datap = NULL;
	string_hashtable_t<vc_clconf_data_t *>::iterator
	    iter(*vc_clconf_data_tablep);
	for (; (datap = iter.get_current()) != NULL; iter.advance()) {
		cleanup_vc_data_common(
		    datap->the_vc_clconf_ccrp->cluster_namep);
	}
#ifdef _KERNEL
	delete vc_dir_cb_listp;
	vc_dir_cb_listp = NULL;
	delete vc_dir_cb_list_lockp;
	vc_dir_cb_list_lockp = NULL;
#endif	// _KERNEL
	delete vc_clconf_data_tablep;
	vc_clconf_data_tablep = NULL;
}

//
// Cleans up the clconf object and data for a single virtual cluster
//
void
clconf_ccr::cleanup_single_vc_data(const char *cl_namep)
{
	cleanup_vc_data_common(cl_namep);
}

//
// Removes the virtual cluster clconf object from the table of clconf objects
//
void
clconf_ccr::cleanup_vc_data_common(const char *cl_namep)
{
	vc_clconf_data_t *clconfp = vc_clconf_data_tablep->remove(cl_namep);
#ifndef _KERNEL_ORB
	os::mutex_t &lock = clconfp->vc_clconf_ccr_lock;
	lock.lock();
#endif
	clconfp->the_vc_clconf_ccrp->shutdown_int();
	delete (clconfp->the_vc_clconf_ccrp);
	clconfp->the_vc_clconf_ccrp == NULL;
#ifndef _KERNEL_ORB
	lock.unlock();
#endif
	delete clconfp;
}

#ifdef _KERNEL
//
// clconf gets notified that CCR is recoverd.
// clconf needs this callback to determine if one or more
// zone clusters were added/removed while this node was down.
// As part of this call, clconf updates its data structures
// synchronously, if required.
//
void
clconf_ccr::ccr_is_refreshed_callback_for_clconf()
{
	//
	// Ignore return value. check_zone_cluster_list() will print
	// debug statements in case of any error.
	//
	(void) clconf_ccr::the().check_zone_cluster_list();
}
#endif	// _KERNEL

//
// Checks to see if the zone clusters that clconf knows about are the same as
// those present in CCR.
// When executed in userland, the method also makes sure that the clconf trees
// of the zone clusters have the latest data from infrastructure tables in CCR.
//
// Returns 0 on success, error code on failure.
//
int
clconf_ccr::check_zone_cluster_list()
{
	bool			error;
	Environment		e;
	CORBA::Exception	*exp = NULL;
	CORBA::StringSeq	*cl_namesp = NULL;
	CORBA::StringSeq	*cl_idsp = NULL;
	uint_t			num_clusters;
	uint_t			index;
	uint32_t		cl_id;

	if (getzoneid() != GLOBAL_ZONEID) {
		// This operation is not allowed in non-global zones
		CLCONF_DBG(("check_zone_cluster_list : called in "
		    "non-global zone, returning EPERM\n"));
		return (EPERM);
	}

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));

	//
	// First check if any zone cluster exists in CCR,
	// but not in the list of clconf trees.
	// If so, add it to the list of clconf trees.
	//
	dir_ptr->zc_get_names_and_ids(cl_namesp, cl_idsp, e);
	dir_lock.unlock();
	exp = e.exception();
	if (exp != NULL) {
		ASSERT(ccr::invalid_zc_access::_exnarrow(exp) != NULL);

		// In global zone, we should not get this exception.
		ASSERT(0);

		if (cl_namesp) {
			delete cl_namesp;
		}
		if (cl_idsp) {
			delete cl_idsp;
		}
		e.clear();
		CLCONF_DBG(("check_zone_cluster_list : querying CCR for "
		    "zc names and ids raised exception, returning EIO\n"));
		return (EIO);
	}
	CL_PANIC(cl_namesp->length() == cl_idsp->length());
	num_clusters = cl_namesp->length();
	for (index = 0; index < num_clusters; index++) {
		char *cl_namep = (char *)(*cl_namesp)[index];
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			//
			// The data about cluster names and ids obtained from
			// directory object contains the global cluster too.
			// Here we are interested in zone clusters only.
			//
			continue;
		}
		cl_id = (uint32_t)os::atoi((char *)(*cl_idsp)[index]);
		//
		// This call will check if the zone cluster is present in
		// the list of clconf trees. If not, it will create the tree
		// for the zone cluster and read the infrastructure for it.
		//
		error = read_vc_data_common(cl_namep, cl_id);
	}

	//
	// Now check if the list of clconf trees has any zone cluster
	// that does not exist in the CCR. If so, remove it from the list
	// of clconf trees.
	//
	vc_clconf_data_t *datap = NULL;
	string_hashtable_t<vc_clconf_data_t *>::iterator
	    iter(*vc_clconf_data_tablep);
	while ((datap = iter.get_current()) != NULL) {
		iter.advance();
		char *iter_cl_namep = datap->the_vc_clconf_ccrp->cluster_namep;
		bool cl_exists = false;
		for (index = 0; index < num_clusters; index++) {
			if (os::strcmp(iter_cl_namep,
			    (char *)(*cl_namesp)[index]) == 0) {
				cl_exists = true;
				break;
			}
		}
		if (!cl_exists) {
			// Stale zone cluster clconf tree; remove it
			cleanup_single_vc_data(iter_cl_namep);
		}
	}

	delete cl_namesp;
	delete cl_idsp;

#ifndef	_KERNEL_ORB
	//
	// In userland, clconf does not get callbacks from CCR when
	// zone clusters are added/removed or zone cluster infrastructure table
	// is modified. So make sure that the clconf trees of the zone clusters
	// have the latest data from infrastructure tables in CCR.
	//
	int refresh_err = cl_current_tree::the().refresh_zc_clconf_trees();
	return (refresh_err);

#else	// !_KERNEL_ORB

	// In kernel
	return (0);

#endif	// !_KERNEL_ORB
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

//
// Makes a clconf object register for CCR callbacks for the infrastructure
// table of the cluster that the clconf object corresponds to.
// Returns true if successfully registered, false otherwise.
//
#ifdef _KERNEL_ORB
bool
clconf_ccr::register_infr_callback()
{
	Environment 		e;
	clconf_io::status	result;

	while ((result = open()) == C_RETRY);
	if (result == C_FAIL) {
		CLCONF_DBG(("fail to register callback with CCR "
		    "for cluster %s\n", cluster_namep));
		return (false);
	}

	// callback should only be registered once.
	ASSERT(CORBA::is_nil(infr_cb_p));
	infr_cb_p = (new clconf_infr_callback_impl())->get_objref();
	read_ptr->register_callbacks(infr_cb_p, e);
	ASSERT(e.exception() == NULL);
	close();
	CLCONF_DBG(("registered infrastructure callback %p with CCR "
	    "for cluster %s\n", infr_cb_p, cluster_namep));
	return (true);
}

//
// Unregisters the infrastructure table callback for the clconf object
// of a particular cluster.
//
void
clconf_ccr::unregister_infr_callback()
{
	Environment 		e;
	clconf_io::status	result = C_SUCCESS;

	//
	// For virtual clusters, there is no need to unregister
	// the infrastructure callback. This is because, by the time
	// clconf comes to know about the deletion of a virtual cluster,
	// the infrastructure table for the virtual cluster is already
	// gone and so are the callbacks registered for the table.
	//
	ASSERT(os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0);

	while ((result = open()) == C_RETRY);
	if (result == C_FAIL) {
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		    "CCR", NULL);

		bool panic = true;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
		if (getzoneid() != GLOBAL_ZONEID) {
			panic = false;
		}
#endif
		//
		// SCMSGS
		// @explanation
		// Failed to open table infrastructure in
		// unregistered clconf callback with CCR.
		// Table infrastructure not found.
		// @user_action
		// Check the table infrastructure.
		//
		if (panic) {
			(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
			    "clconf: Failed to open "
			    "table infrastructure in "
			    "unregister_infr_callback");
		} else {
			(void) syslog_msg.log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "clconf: Failed to open "
			    "table infrastructure in "
			    "unregister_infr_callback");
		}
	}

	read_ptr->unregister_callbacks(infr_cb_p, e);
	ASSERT(e.exception() == NULL);
	CORBA::release(infr_cb_p);
	infr_cb_p = nil;
	close();
	CLCONF_DBG(("unregistered clconf callback with CCR for cluster %s\n",
	    cluster_namep));
}
#endif	// _KERNEL_ORB

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
//
// If the CLUSTER_DIR file is changed (which could be the result of
// the creation/deletion of a cluster or the recovery of the CLUSTER_DIR file),
// then we get a callback from CCR here.
// The "table_name" is the name of the cluster created/deleted.
//
void
cldir_callback(const char *cluster_namep,
    const char *table_namep, clconf_ccr_update_t update)
{
	clconf_ccr::the().cldir_callback(cluster_namep, table_namep, update);
}

//
// If a new virtual cluster is added, then this routine registers a directory
// callback object for the directory table of the virtual cluster, so that
// when the infrastructure file is created for the virtual cluster, a callback
// is received (directory table callback clients are called whenever any table
// mentioned in the directory changes), and the infrastructure table contents
// can be read into the clconf tree. This routine also notifies CZMM about
// a new virtual cluster being added.
//
// If a virtual cluster is removed, then this routine unregisters
// the infrastructure callback object for the virtual cluster, and then removes
// the part of the clconf tree that contains the virtual cluster infrastructure
// information. This routine also notifies CZMM about a new virtual cluster
// being removed.
//
// If the cluster directory is recovered, then it signifies that the CCR
// has been sync'ed. In this case, this routine reads in virtual cluster
// configuration data from CCR, and registers infrastructure callbacks
// for all virtual clusters. This routine also notifies CZMM that the CCR is
// sync'ed and the clconf has read in the virtual cluster configuration data.
//
void
clconf_ccr::cldir_callback(const char *clust_namep,
    const char *table_namep, clconf_ccr_update_t update)
{
	bool error;
	const char *cl_namep = table_namep;
	struct vc_dir_cb *dir_cbp = NULL;

	// The cluster directory callbacks are for global cluster only
	ASSERT(os::strcmp(clust_namep, DEFAULT_CLUSTER) == 0);

	CLCONF_DBG(("kernel clconf %s updated\n", CLUSTER_DIR));

	// XXXCCR hack before ccr moves into kernel.
	(void) clconf_ccr::the().lookup_dir();

	if (update == CCR_TABLE_ADDED) {
		// The global cluster cannot be added or removed dynamically
		ASSERT(os::strcmp(cl_namep, DEFAULT_CLUSTER) != 0);

		//
		// A new virtual cluster has been added.
		// The infrastructure file does not exist now; it would be
		// created later. But when it is created, the directory table
		// of the virtual cluster would be updated. So register for
		// callbacks for the directory table of the virtual cluster,
		// so that you can know when the infrastructure file is created
		// for the virtual cluster.
		//
		// Two virtual clusters cannot be created simultaneously.
		// Only after one virtual cluster is created completely
		// (which includes creation of the infrastructure table
		// for the virtual cluster and the synchronous callback
		// received by clconf for the creation of infrastructure
		// table), can another virtual cluster be created.
		// So we can safely use only one directory callback object.
		//
		vc_dir_cb_list_lockp->lock();
		SList<struct vc_dir_cb>::ListIterator iter(*vc_dir_cb_listp);
		while ((dir_cbp = iter.get_current()) != NULL) {
			if (os::strcmp(dir_cbp->cl_namep, cl_namep) == 0) {
				// Found in list
				CLCONF_DBG(("stale vc dir callback found "
				    "for cluster %s\n", cl_namep));
				vc_dir_cb_list_lockp->unlock();
				return;
			}
			iter.advance();
		}
		dir_cbp = new struct vc_dir_cb;
		if (dir_cbp == NULL) {
			CLCONF_DBG(("failed to allocate memory for "
			    "vc dir callback for cluster %s\n", cl_namep));
#ifdef DEBUG
			os::warning("failed to allocate memory\n");
#endif
		}
		dir_cbp->cl_namep = os::strdup(cl_namep);
		dir_cbp->vc_dir_cb_p = (new clconf_ccr_table_callback_impl(
		    vc_dir_callback_fn, DIR))->get_objref();
		vc_dir_cb_listp->append(dir_cbp);
		if (!clconf_ccr::the().register_ccr_table_callback(
		    cl_namep, DIR, dir_cbp->vc_dir_cb_p)) {
			//
			// Failed to register a callback with the directory
			// table for the virtual cluster. Continue to notify
			// CZMM about the new virtual cluster.
			// But note that clconf would not be able to know when
			// the infrastructure file of the virtual cluster
			// is created.
			//
#ifdef DEBUG
			os::warning("failed to register callbacks with "
			    "%s table for cluster %s", DIR, cl_namep);
#endif
			vc_dir_cb_listp->erase(dir_cbp);
			delete [] (dir_cbp->cl_namep);
			CORBA::release(dir_cbp->vc_dir_cb_p);
			delete dir_cbp;
		}
		dir_cbp = NULL;
		vc_dir_cb_list_lockp->unlock();

		membership_engine_impl *enginep =
		    membership_engine_impl::the();
		ASSERT(enginep != NULL);
		enginep->clconf_callback_to_membership(
		    cl_namep, ZONE_CLUSTER_ADDED);

	} else if (update == CCR_TABLE_REMOVED) {
		// The global cluster cannot be added or removed dynamically
		ASSERT(os::strcmp(cl_namep, DEFAULT_CLUSTER) != 0);

		//
		// There could be cases where the directory callback object
		// is still present when the cluster is removed. This could
		// happen, say, in this sequence of events :
		// (1) while the virtual cluster was being created,
		// the cluster_directory table was updated, clconf received
		// a callback, registered a callback for the vc directory
		// (2) there was a failure in creating the infrastructure table,
		// so clconf does not know about this.
		// (3) the virtual cluster creation failed, and so the changes
		// are rolled back. clconf gets a callback for
		// the cluster_directory table, but it has to remove the stale
		// vc directory callback object.
		//
		// The cleanup code frees the passed in string.
		// So we duplicate the string here.
		//
		char *cleanup_cl_namep = os::strdup(cl_namep);
		CL_PANIC(cleanup_cl_namep != NULL);
		vc_dir_callback_cleanup(cleanup_cl_namep);

		// A virtual cluster has been deleted.
		clconf_ccr::the().cleanup_single_vc_data(cl_namep);

		membership_engine_impl *enginep =
		    membership_engine_impl::the();
		ASSERT(enginep != NULL);
		enginep->clconf_callback_to_membership(
		    cl_namep, ZONE_CLUSTER_REMOVED);

	} else {
		ASSERT(update == CCR_RECOVERED);
		//
		// The cluster directory table has been recovered.
		// This happens only the first time the node joins the cluster.
		// The cluster directory table is recovered first.
		// If zone clusters were added when this node was down,
		// then we cannot start refreshing clconf data structures
		// here as the CCR on this node would not have created
		// the new zone cluster tables till now.
		// Instead, we ignore this recovery callback.
		// Once CCR finishes its refresh, it will callback
		// into clconf, and then clconf can refresh its data structures.
		//
	}
}

//
// A callback has been received due to the update of the directory
// table for a newly added virtual cluster. If the update of the directory
// table is due to the creation of an infrastructure table for the newly added
// virtual cluster, then read in the infrastructure table from CCR into clconf
// tree, and then unregister the directory callback for the virtual cluster.
//
void
vc_dir_callback(const char *cluster_namep,
    const char *table_namep, clconf_ccr_update_t update)
{
	clconf_ccr::the().vc_dir_callback(cluster_namep, table_namep, update);
}

void
vc_dir_callback_cleanup_thread(void *cl_namep)
{
	clconf_ccr::the().vc_dir_callback_cleanup((const char *)cl_namep);
}

void
clconf_ccr::vc_dir_callback_cleanup(const char *cl_namep)
{
	struct vc_dir_cb *dir_cbp = NULL;

	// Unregister the directory table callback
	vc_dir_cb_list_lockp->lock();
	SList<struct vc_dir_cb>::ListIterator iter(*vc_dir_cb_listp);
	while ((dir_cbp = iter.get_current()) != NULL) {
		if (os::strcmp(dir_cbp->cl_namep, cl_namep) == 0) {
			// Found in list, remove it
			vc_dir_cb_listp->erase(dir_cbp);
			delete [] (dir_cbp->cl_namep);
			unregister_ccr_table_callback(
			    cl_namep, DIR, dir_cbp->vc_dir_cb_p);
			CORBA::release(dir_cbp->vc_dir_cb_p);
			dir_cbp->vc_dir_cb_p = nil;
			delete dir_cbp;
			dir_cbp = NULL;
			CLCONF_DBG(("unregistered vc dir callback "
			    "for cluster %s\n", cl_namep));
			break;
		}
		iter.advance();
	}
	vc_dir_cb_list_lockp->unlock();

	delete [] cl_namep;
}

//
// Once the infrastructure table of a new virtual cluster is created,
// we receive the callback here. We then read the contents of the infrastructure
// table into the clconf tree. After that, we unregister the directory callback
// object for the virtual cluster, as it is no longer needed.
//
void
clconf_ccr::vc_dir_callback(const char *cl_namep,
    const char *table_namep, clconf_ccr_update_t update)
{
	Environment		e;
	CORBA::Exception	*exp;
	uint32_t		cluster_id;

	ASSERT(os::strcmp(cl_namep, DEFAULT_CLUSTER) != 0);
	if (os::strcmp(table_namep, CLCONF_TABLE) == 0) {
		bool error = read_single_vc_data(cl_namep);

		//
		// Unregister the directory table callback.
		// The cleanup thread will free up the memory
		// for the cluster name allocated here.
		//
		char *cleanup_cl_namep = os::strdup(cl_namep);
		CL_PANIC(cleanup_cl_namep != NULL);
		if (clnewlwp(vc_dir_callback_cleanup_thread,
		    (void *)cleanup_cl_namep, orb_conf::rt_maxpri(),
		    orb_conf::rt_classname(), NULL) != 0) {
			CLCONF_DBG(("failed to create thread that would "
			    "handle unregistration of %s table callback for "
			    "cluster %s\n", DIR, cl_namep));
			//
			// cleanup thread creation failed.
			// delete the memory for cluster name.
			//
			delete [] cleanup_cl_namep;
		}
	}
}

// Registers for cluster directory callbacks
bool
clconf_ccr::register_cldir_callback()
{
	// callback should only be registered once.
	ASSERT(CORBA::is_nil(cldir_cb_p));
	cldir_cb_p = (new clconf_ccr_table_callback_impl(
	    cldir_callback_fn, CLUSTER_DIR))->get_objref();

	if (!register_ccr_table_callback(
	    DEFAULT_CLUSTER, CLUSTER_DIR, cldir_cb_p)) {
		return (false);
	}
	return (true);
}

// Unregisters the cluster directory callback object
void
clconf_ccr::unregister_cldir_callback()
{
	// callback should already be there
	ASSERT(is_not_nil(cldir_cb_p));
	unregister_ccr_table_callback(
	    DEFAULT_CLUSTER, CLUSTER_DIR, cldir_cb_p);
	CORBA::release(cldir_cb_p);
	cldir_cb_p = nil;
}
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

#if (SOL_VERSION >= __s10) && !defined(UNODE)
// Registers for CCR callbacks for any CCR table
bool
clconf_ccr::register_ccr_table_callback(
    const char *cl_namep, const char *table_namep, ccr::callback_ptr cb_p)
{
	Environment 			e;
	ccr::readonly_table_ptr		tab_ptr;

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));
	tab_ptr = dir_ptr->lookup_zc(cl_namep, table_namep, e);
	dir_lock.unlock();
	if (e.exception()) {
#ifdef DEBUG
		os::warning("failed to register callback as table %s "
		    "for cluster %s is not found", table_namep, cl_namep);
#endif	// DEBUG
		ASSERT(CORBA::is_nil(tab_ptr));
		e.clear();
		return (false);
	}

	tab_ptr->register_callbacks(cb_p, e);
	ASSERT(e.exception() == NULL);

	CORBA::release(tab_ptr);
	tab_ptr = ccr::readonly_table::_nil();
	CLCONF_DBG(("registered %s callback %p with CCR for cluster %s\n",
	    table_namep, cb_p, cl_namep));
	return (true);
}

#else	// (SOL_VERSION >= __s10) && !defined(UNODE)
// Registers for CCR callbacks for any CCR table
bool
clconf_ccr::register_ccr_table_callback(
    const char *table_namep, ccr::callback_ptr cb_p)
{
	Environment 			e;
	ccr::readonly_table_ptr		tab_ptr;

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));
	tab_ptr = dir_ptr->lookup(table_namep, e);
	dir_lock.unlock();
	if (e.exception()) {
#ifdef DEBUG
		os::warning("failed to register callback as table %s "
		    "is not found", table_namep);
#endif	// DEBUG
		ASSERT(CORBA::is_nil(tab_ptr));
		e.clear();
		return (false);
	}

	tab_ptr->register_callbacks(cb_p, e);
	ASSERT(e.exception() == NULL);

	CORBA::release(tab_ptr);
	tab_ptr = ccr::readonly_table::_nil();
	CLCONF_DBG(("registered %s callback %p with CCR\n",
	    table_namep, cb_p));
	return (true);
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#if (SOL_VERSION >= __s10) && !defined(UNODE)
// Unregisters a previously registered callback object for any CCR table
void
clconf_ccr::unregister_ccr_table_callback(
    const char *cl_namep, const char *table_namep, ccr::callback_ptr cb_p)
{
	Environment 			e;
	ccr::readonly_table_ptr		tab_ptr;

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));
	tab_ptr = dir_ptr->lookup_zc(cl_namep, table_namep, e);
	dir_lock.unlock();
	if (e.exception()) {
#ifdef DEBUG
		os::warning("failed to unregister callback as table %s "
		    "for cluster %s is not found", table_namep, cl_namep);
#endif	// DEBUG
		ASSERT(CORBA::is_nil(tab_ptr));
		e.clear();
		return;
	}

	tab_ptr->unregister_callbacks(cb_p, e);
	ASSERT(e.exception() == NULL);

	CORBA::release(tab_ptr);
	tab_ptr = ccr::readonly_table::_nil();
	CLCONF_DBG(("unregistered %s callback with CCR for cluster %s\n",
	    table_namep, cl_namep));
}

#else	// (SOL_VERSION >= __s10) && !defined(UNODE)
// Unregisters a previously registered callback object for any CCR table
void
clconf_ccr::unregister_ccr_table_callback(
    const char *table_namep, ccr::callback_ptr cb_p)
{
	Environment 			e;
	ccr::readonly_table_ptr		tab_ptr;

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));
	tab_ptr = dir_ptr->lookup(table_namep, e);
	dir_lock.unlock();
	if (e.exception()) {
#ifdef DEBUG
		os::warning("failed to unregister callback as table %s "
		    "is not found", table_namep);
#endif	// DEBUG
		ASSERT(CORBA::is_nil(tab_ptr));
		e.clear();
		return;
	}

	tab_ptr->unregister_callbacks(cb_p, e);
	ASSERT(e.exception() == NULL);

	CORBA::release(tab_ptr);
	tab_ptr = ccr::readonly_table::_nil();
	CLCONF_DBG(("unregistered %s callback with CCR\n", table_namep));
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

//
// clconf_ccr::register_ccr_callback()
//
// Registers the given fn as the one to be invoked by the CCR
// callback corresp to any changes in the CCR.
// Note that CCR here means CCR for the cluster we are in (physical/virtual).
//
clconf_errnum_t
clconf_ccr::register_ccr_callback(clconf_ccr_callback_handler ccr_hdlr)
{
	Environment		e;
	CORBA::Exception	*exp;
	clconf_errnum_t	err = CL_NOERROR;

	clconf_ccr_cb_lock.lock();

	//
	// Is there a callback already registered, or is the handler
	// being passed in NULL? Either case, return the error code.
	//
	if (is_not_nil(ccr_cb_p)) {
		// Callback should be registered only once.
		err = CL_ALREADY_REGISTERED;
	} else if (ccr_hdlr == NULL) {
		err = CL_NULL_CALLBACK;
	} else {
		// Register the callback fn.
		ccr_cb_p = (new clconf_ccr_callback_impl(ccr_hdlr))->
		    get_objref();

		dir_lock.rdlock();
		ASSERT(is_not_nil(dir_ptr));
		dir_ptr->register_callback(ccr_cb_p, e);
		dir_lock.unlock();
		if ((exp = e.exception()) != NULL) {
			CORBA::release(ccr_cb_p);
			ccr_cb_p = nil;
			CL_PANIC(ccr::callback_exists::_exnarrow(exp) == NULL);
			err = CL_CCR_ERROR;
			e.clear();
		} else {
			CLCONF_DBG(("registered ccr callback %p with CCR\n",
			    ccr_cb_p));
		}
	}

	clconf_ccr_cb_lock.unlock();
	return (err);
}

//
// clconf_ccr::unregister_ccr_callback()
//
// Unregisters  a previously registered CCR callback handler.
//
clconf_errnum_t
clconf_ccr::unregister_ccr_callback()
{
	Environment		e;
	CORBA::Exception	*exp;
	clconf_errnum_t	err = CL_NOERROR;

	clconf_ccr_cb_lock.lock();

	if (CORBA::is_nil(ccr_cb_p)) {
		err = CL_NOT_REGISTERED;
	} else {

		dir_lock.rdlock();
		ASSERT(is_not_nil(dir_ptr));
		dir_ptr->unregister_callback(ccr_cb_p, e);
		dir_lock.unlock();
		CLCONF_DBG(("unregistered ccr callback %p "
		    "with CCR\n", ccr_cb_p));

		//
		// Reset hdlr etc to indicate hdlr exists irrespective
		// of whether the unregister was successful or not, because
		// this can only fail if there is a problem accessing the
		// CCR directory, which is a pretty bad scenario, and certainly
		// callbacks for the directory will not be operational anyway.
		//
		CORBA::release(ccr_cb_p);
		ccr_cb_p = nil;

		if ((exp = e.exception()) != NULL) {
			CL_PANIC(ccr::no_such_callback::_exnarrow(exp) == NULL);
			err = CL_CCR_ERROR;
			e.clear();
		}
	}

	clconf_ccr_cb_lock.unlock();
	return (err);
}

clconf_ccr::clconf_ccr(const char *cl_namep) :
    dir_ptr(nil), read_ptr(nil), write_ptr(nil),
#ifdef _KERNEL_ORB
    infr_cb_p(nil),
#endif	// _KERNEL_ORB
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
    cldir_cb_p(nil),
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)
    ccr_cb_p(nil)
{
	cluster_namep = os::strdup(cl_namep);
}

clconf_ccr::~clconf_ccr()
{
	delete [] cluster_namep;
}

//
// Gets a readonly table pointer for the CLCONF_TABLE of a particular cluster
//
clconf_io::status
clconf_ccr::open()
{
#if (SOL_VERSION < __s10) || defined(UNODE)
	// Prior to S10 or for unode, there are no virtual clusters.
	ASSERT(os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0);
#endif

	ASSERT(CORBA::is_nil(read_ptr));

	Environment e;

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	read_ptr = dir_ptr->lookup_zc(cluster_namep, CLCONF_TABLE, e);
#else
	read_ptr = dir_ptr->lookup(CLCONF_TABLE, e);
#endif
	dir_lock.unlock();
	if (e.exception()) {
#ifdef DEBUG
#if (SOL_VERSION >= __s10) && !defined(UNODE)
		os::warning("table infrastructure for cluster %s "
		    "is not found", cluster_namep);
#else
		os::warning("table infrastructure is not found");
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)
#endif	// DEBUG
		ASSERT(CORBA::is_nil(read_ptr));
		e.clear();
		return (C_FAIL);
	}
	read_ptr->atfirst(e);
	if (e.exception() != NULL) {
		CL_PANIC(ccr::table_modified::_exnarrow(e.exception()));
		e.clear();
		close();
		return (C_RETRY);
	}
	return (C_SUCCESS);
}

void
clconf_ccr::close()
{
	ASSERT(is_not_nil(read_ptr));
	CORBA::release(read_ptr);
	read_ptr = nil;
}

int32_t
clconf_ccr::get_version()
{
	Environment e;
	int32_t version;

	dir_lock.rdlock();
	ccr::readonly_table_var tmp_ptr =
	    dir_ptr->lookup_zc(cluster_namep, CLCONF_TABLE, e);
	dir_lock.unlock();
	if (e.exception()) {
		ASSERT(CORBA::is_nil(tmp_ptr));
		version = -1;
		e.clear();
	} else {
		version = tmp_ptr->get_gennum(e);
	}

	return (version);
}

#if (SOL_VERSION >= __s10)
//
// Get the cluster id of a given virtual cluster.
// Return values :
// ENOENT :
//	If such a cluster does not exist.
// EACCES :
//	If there is an attempt to violate zone boundary.
//	Global zone is allowed to query any cluster id.
// 0 : success
//
int32_t
clconf_ccr::get_cluster_id(char *clusterp, uint32_t& clid)
{
	int			retval = 0;
	Environment 		e;
	CORBA::Exception	*exp	= NULL;
	ASSERT(clusterp);

	dir_lock.rdlock();
	clid = dir_ptr->zc_getidbyname(clusterp, e);
	dir_lock.unlock();
	if ((exp = e.exception()) != NULL) {
		if (ccr::invalid_zc_access::_exnarrow(exp)) {
			retval = EACCES;
		} else if (ccr::no_such_zc::_exnarrow(exp)) {
			retval = ENOENT;
		}
		e.clear();
	}
	return (retval);
}

//
// Get the cluster name, given the cluster id.
// Return values :
// ENOENT : if such a cluster does not exist
// EACCES :
//	if there is an attempt to violate zone boundary.
//	Global zone is allowed to query any cluster name.
// 0 : success
//
int32_t
clconf_ccr::get_cluster_name(uint_t clid, char **clnamep)
{
	int			retval = 0;
	Environment 		e;
	CORBA::Exception	*exp	= NULL;

	dir_lock.rdlock();
	*clnamep = dir_ptr->zc_getnamebyid(clid, e);
	dir_lock.unlock();
	if ((exp = e.exception()) != NULL) {
		if (ccr::invalid_zc_access::_exnarrow(exp)) {
			retval = EACCES;
		} else if (ccr::no_such_zc::_exnarrow(exp)) {
			retval = ENOENT;
		}
		e.clear();
	}
	return (retval);
}
#endif // (SOL_VERSION >= __s10)

//
// read_one
//
// This function returns a pair of pointers to the next entry key/value.
// The caller is responsible for deleting the two buffers in argument
// after their consumption (note the "pointer to a char pointer" arguments).
//
// The read_ptr is already set to the CLCONF_TABLE of the appropriate
// zone cluster.
//
clconf_io::status
clconf_ccr::read_one(char **key, char **value)
{
	ASSERT(is_not_nil(read_ptr));
	ccr::table_element *outelem = NULL;
	char *tmp_key = NULL;
	char *tmp_value = NULL;
	Environment e;
	os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG, "CCR", NULL);

	read_ptr->next_element(outelem, e);
	if (e.exception() != NULL) {
		if (ccr::table_modified::_exnarrow(e.exception())) {
			e.clear();
			return (C_RETRY);
		} else {
			// either system_error or no more element
			e.clear();
			return (C_FAIL);
		}
	}

	ASSERT(outelem != NULL);

	tmp_key = new char[strlen(outelem->key) + 1];
	if (tmp_key == NULL) {
		return (C_FAIL);
	}
	tmp_value = new char[strlen(outelem->data) + 1];
	if (tmp_value == NULL) {
		delete [] tmp_key;
		return (C_FAIL);
	}
	(void) strcpy(tmp_key, outelem->key);
	(void) strcpy(tmp_value, outelem->data);
	delete outelem;

	*key = tmp_key;
	*value = tmp_value;
	return (C_SUCCESS);
}

bool
clconf_ccr::begin_transaction()
{
	Environment e;

	ASSERT(CORBA::is_nil(write_ptr));

	dir_lock.rdlock();
	ASSERT(is_not_nil(dir_ptr));

	write_ptr = dir_ptr->begin_transaction(CLCONF_TABLE, e);
	dir_lock.unlock();
	if (e.exception()) {
		e.exception()->print_exception(
		    "clconf_ccr::begin_transaction\n");
		ASSERT(CORBA::is_nil(write_ptr));
		return (false);
	}
	// We'll write a whole new table out.
	write_ptr->remove_all_elements(e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "clconf_ccr::begin_transaction\n");
		CORBA::release(write_ptr);
		write_ptr = nil;
		e.clear();
		return (false);
	}
	return (true);
}

bool
clconf_ccr::commit_transaction()
{
	ASSERT(is_not_nil(write_ptr));
	Environment e;
	write_ptr->commit_transaction(e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "clconf_ccr::commit_transaction\n");
		CORBA::release(write_ptr);
		write_ptr = nil;
		e.clear();
		return (false);
	}
	CORBA::release(write_ptr);
	write_ptr = nil;
	return (true);
}

void
clconf_ccr::abort_transaction()
{
	ASSERT(is_not_nil(write_ptr));
	Environment e;
	write_ptr->abort_transaction(e);
	ASSERT(!e.exception());
	CORBA::release(write_ptr);
	write_ptr = nil;
}

bool
clconf_ccr::write(const char *key, const char *value)
{
	ASSERT(is_not_nil(write_ptr));
	Environment e;
	write_ptr->add_element(key, value, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "clconf_ccr::write\n");
		CORBA::release(write_ptr);
		write_ptr = nil;
		e.clear();
		return (false);
	}
	return (true);
}

// Create an element_seq
void *
clconf_ccr::create_seq()
{
	return (new ccr::element_seq(0));
}

// Delete an element_seq
void
clconf_ccr::delete_seq(void *seq)
{
	delete(ccr::element_seq *)seq;
}

// Append the leaf node to the element_seq
int
clconf_ccr::append_node(ccr_node *nd, void *vp)
{
	// Write leaf nodes out.
	if (nd->is_leaf()) {
		ccr::element_seq *esp = (ccr::element_seq *) vp;
		unsigned int len = esp->length();
		esp->length(len+1);
		// const char * ensures that a copy is made of the string
		// We do not have to take a copy of the full_name
		// because we have already alloc'ed memory in
		// get_full_name() that needs to be freed with element_seq.
		// Since get_value() does not allocate memory we have
		// to make a copy of it by using const char *
		(*esp)[len].key = nd->get_full_name();
		(*esp)[len].data = (const char *)nd->get_value();
	}
	return (1);
}

// Write out the sequence to the CCR
// Return -1 if there is a failure.
int
clconf_ccr::write_seq(void *seq)
{
	ccr::element_seq *esp = (ccr::element_seq *)seq;
	ASSERT(is_not_nil(write_ptr));
	Environment e;
	write_ptr->add_elements(*esp, e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}
	return (0);
}

//
// Updates the reference to the CCR directory object
//
bool
clconf_ccr::lookup_dir()
{
	Environment e;
	dir_lock.wrlock();
	CORBA::release(dir_ptr);
	dir_ptr = nil;

	naming::naming_context_var ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
#ifdef DEBUG
		os::warning("can't find local name server");
#endif
		dir_lock.unlock();
		return (false);
	}
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
#ifdef DEBUG
		os::warning("ccr_directory not found in name server");
#endif
		e.clear();
		dir_lock.unlock();
		return (false);
	}

	dir_ptr = ccr::directory::_narrow(obj);
	dir_lock.unlock();
	return (true);
}

/*
 * These callbacks are available only in kernel or unode mode.
 */
#ifdef _KERNEL_ORB
void
clconf_infr_callback_impl::did_update(
				const char *table_namep,
				ccr::ccr_update_type,
				Environment &e)
{
	bool	error;

	ASSERT(os::strcmp(table_namep, CLCONF_TABLE) == 0);
	CLCONF_DBG(("kernel clconf updated\n"));

	// XXXCCR hack before ccr moves into kernel.
#ifdef _KERNEL
	(void) clconf_ccr::the().lookup_dir();
#endif // _KERNEL

	if (cl_current_tree::the().read_ccr(&error)) {
		kernel_tm_callback_funcp();
		cl_current_tree::the().destroy_old_root();
	}

	//
	// Call the quorum algorithm's callback routine since the
	// quorum configuration may have changed.
	//
	quorum::quorum_algorithm_var	qm_v;
	qm_v = cmm_ns::get_quorum_algorithm(NULL);
	qm_v->quorum_table_updated(e);
	ASSERT(!e.exception());
	e.clear();
}

void
clconf_infr_callback_impl::did_update_zc(
				const char *clusterp,
				const char *table_namep,
				ccr::ccr_update_type,
				Environment &e)
{
	bool	error;

	ASSERT(os::strcmp(clusterp, DEFAULT_CLUSTER) != 0);
	ASSERT(os::strcmp(table_namep, CLCONF_TABLE) == 0);
	CLCONF_DBG(("kernel clconf updated\n"));

	// XXXCCR hack before ccr moves into kernel.
#ifdef _KERNEL
	(void) clconf_ccr::the().vc_clconf(clusterp).lookup_dir();
#endif // _KERNEL

	cl_current_tree &tree = cl_current_tree::the().cl_tree(clusterp);
	if (tree.read_ccr(&error)) {
		tree.destroy_old_root();
	}
}

void
clconf_infr_callback_impl::did_recovery(
			    const char *table_namep,
			    ccr::ccr_update_type,
			    Environment &e)
{
	bool	error;

	// XXXCCR hack before ccr moves into kernel.
#ifdef _KERNEL
	(void) clconf_ccr::the().lookup_dir();
#endif

	ASSERT(os::strcmp(table_namep, CLCONF_TABLE) == 0);
	if (cl_current_tree::the().read_ccr(&error)) {
		CLCONF_DBG(("clconf recovered\n"));
		kernel_tm_callback_funcp();
		cl_current_tree::the().destroy_old_root();
	}

	//
	// Call the quorum algorithm's callback routine since the
	// quorum configuration may have changed.
	//
	quorum::quorum_algorithm_var	qm_v;
	qm_v = cmm_ns::get_quorum_algorithm(NULL);
	qm_v->quorum_table_updated(e);
	ASSERT(!e.exception());
	e.clear();
}

void
clconf_infr_callback_impl::did_recovery_zc(
			    const char *clusterp,
			    const char *table_namep,
			    ccr::ccr_update_type,
			    Environment &e)
{
	bool	error;

	ASSERT(os::strcmp(clusterp, DEFAULT_CLUSTER) != 0);
	ASSERT(os::strcmp(table_namep, CLCONF_TABLE) == 0);
	CLCONF_DBG(("did_recovery_zc() for infr table called, "
	    "cluster %s\n", clusterp));

	// XXXCCR hack before ccr moves into kernel.
#ifdef _KERNEL
	(void) clconf_ccr::the().vc_clconf(clusterp).lookup_dir();
#endif // _KERNEL

	cl_current_tree &tree = cl_current_tree::the().cl_tree(clusterp);
	if (tree.read_ccr(&error)) {
		tree.destroy_old_root();
		CLCONF_DBG(("did_recovery_zc() for infr table succeeded, "
		    "cluster %s\n", clusterp));
	} else {
		if (error) {
			CLCONF_DBG(("did_recovery_zc() for infr table "
			    "failed, cluster %s\n", clusterp));
		} else {
			CLCONF_DBG(("did_recovery_zc(): infr table "
			    "already latest, cluster %s\n", clusterp));
		}
	}
}

void
#ifdef DEBUG
clconf_infr_callback_impl::_unreferenced(unref_t cookie)
#else
clconf_infr_callback_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}
#endif // _KERNEL_ORB

void
clconf_ccr_table_callback_impl::did_update(
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	//
	// If this callback object is for CLUSTER_DIR or DIR file,
	// then the table_name returned as part of callback is
	// the name of the directory/file created/removed.
	//
	// If this callback object is for any other file,
	// then the table_name returned as part of callback
	// should match the filename corresponding to this
	// callback object.
	//
	if ((os::strcmp(tnamep, CLUSTER_DIR) != 0) &&
	    (os::strcmp(tnamep, DIR) != 0)) {
		ASSERT(os::strcmp(tnamep, table_namep) == 0);
	}

	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(DEFAULT_CLUSTER, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
clconf_ccr_table_callback_impl::did_update_zc(
			    const char *clusterp,
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	//
	// If this callback object is for CLUSTER_DIR or DIR file,
	// then the table_name returned as part of callback is
	// the name of the directory/file created/removed.
	//
	// If this callback object is for any other file,
	// then the table_name returned as part of callback
	// should match the filename corresponding to this
	// callback object.
	//
	if ((os::strcmp(tnamep, CLUSTER_DIR) != 0) &&
	    (os::strcmp(tnamep, DIR) != 0)) {
		ASSERT(os::strcmp(tnamep, table_namep) == 0);
	}

	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(clusterp, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
clconf_ccr_table_callback_impl::did_recovery(
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	//
	// If this callback object is for CLUSTER_DIR or DIR file,
	// then the table_name returned as part of callback is
	// the name of the directory/file created/removed.
	//
	// If this callback object is for any other file,
	// then the table_name returned as part of callback
	// should match the filename corresponding to this
	// callback object.
	//
	if ((os::strcmp(tnamep, CLUSTER_DIR) != 0) &&
	    (os::strcmp(tnamep, DIR) != 0)) {
		ASSERT(os::strcmp(tnamep, table_namep) == 0);
	}

	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(DEFAULT_CLUSTER, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
clconf_ccr_table_callback_impl::did_recovery_zc(
			    const char *clusterp,
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	//
	// If this callback object is for CLUSTER_DIR or DIR file,
	// then the table_name returned as part of callback is
	// the name of the directory/file created/removed.
	//
	// If this callback object is for any other file,
	// then the table_name returned as part of callback
	// should match the filename corresponding to this
	// callback object.
	//
	if ((os::strcmp(tnamep, CLUSTER_DIR) != 0) &&
	    (os::strcmp(tnamep, DIR) != 0)) {
		ASSERT(os::strcmp(tnamep, table_namep) == 0);
	}

	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(clusterp, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
#ifdef DEBUG
clconf_ccr_table_callback_impl::_unreferenced(unref_t cookie)
#else
clconf_ccr_table_callback_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete [] tnamep;
	delete this;
}

clconf_ccr_table_callback_impl::clconf_ccr_table_callback_impl
	(clconf_ccr_callback_handler ccr_hdlr, const char *table_namep) :
	ccr_cb_hdlr(ccr_hdlr)
{
	tnamep = os::strdup(table_namep);
	ASSERT(tnamep != NULL);
}

void
clconf_ccr_callback_impl::did_update(
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(DEFAULT_CLUSTER, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
clconf_ccr_callback_impl::did_update_zc(
			    const char *clusterp,
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(clusterp, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
clconf_ccr_callback_impl::did_recovery(
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(DEFAULT_CLUSTER, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
clconf_ccr_callback_impl::did_recovery_zc(
			    const char *clusterp,
			    const char *table_namep,
			    ccr::ccr_update_type ccr_op,
			    Environment &e)
{
	// Invoke callback handler if one is registered.
	if (ccr_cb_hdlr != NULL) {
		(*ccr_cb_hdlr)(clusterp, table_namep,
		    (clconf_ccr_update_t)ccr_op);
	}
	e.clear();
}

void
#ifdef DEBUG
clconf_ccr_callback_impl::_unreferenced(unref_t cookie)
#else
clconf_ccr_callback_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

clconf_ccr_callback_impl::clconf_ccr_callback_impl(
    clconf_ccr_callback_handler ccr_hdlr) : ccr_cb_hdlr(ccr_hdlr)
{
}
