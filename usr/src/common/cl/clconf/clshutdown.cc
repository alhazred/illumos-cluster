//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clshutdown.cc	1.12	09/02/08 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <nslib/ns.h>
#include <h/cmm.h>
#include <cmm/cmm_ns.h>
#include <orb/infrastructure/orb_conf.h>
#include <vm/versioned_protocol.h>
#include <sys/vm_util.h>

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <h/membership.h>
#include <cmm/membership_client.h>
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <zone.h>
#endif

#if (SOL_VERSION < __s10) || defined(UNODE)
//
// In S9 and earlier releases,
// this function deactivates failfast on the specified node.
//
// Will return :
// (1) ENOENT if the failfast admin object for the node is not
//	registered in the name server (maybe node is not up)
// (2) EIO if an exception was returned
// (3) EEXIST if the failfasts were already disabled
// (4) 0 otherwise
//
int
clconf_disable_failfast(nodeid_t nodeid)
{
	Environment e;
	ff::failfast_admin_var ff_admin_v = cmm_ns::get_ff_admin(nodeid);

	if (CORBA::is_nil(ff_admin_v)) {
		return (ENOENT);
	}

	bool retval = ff_admin_v->disable_failfast(e);

	if (e.exception()) {
		return (EIO);
	}

	if (!retval) {
		return (EEXIST);
	}

	return (0);
}

//
// In S9 and earlier releases,
// this function reactivates failfast on the specified node.
//
// Will return :
// (1) ENOENT if the failfast admin object for the node is not
//	registered in the name server (maybe node is not up)
// (2) EIO if an exception was returned,
//	or if enabling failfast per domain is not allowed
// (3) EEXIST if the failfasts were already disabled
// (4) 0 otherwise
//
int
clconf_enable_failfast(nodeid_t nodeid)
{
	Environment e;
	ff::failfast_admin_var ff_admin_v = cmm_ns::get_ff_admin(nodeid);

	if (CORBA::is_nil(ff_admin_v)) {
		return (ENOENT);
	}

	bool retval = ff_admin_v->enable_failfast(e);

	if (e.exception()) {
		return (EIO);
	}

	if (!retval) {
		return (EEXIST);
	}

	return (0);
}
#endif	// (SOL_VERSION < __s10) || defined(UNODE)

//
// Disables path monitoring on the specified node.  Will return ENOENT
// if the failfast admin object for the node is not registered in the
// name server (maybe node is not up). Otherwise it will return EIO if an
// exception was returned.  Otherwise it will return the EEXIST if the
// monitoring was already disabled, or 0 otherwise
//
int
clconf_disable_path_monitoring(nodeid_t nodeid)
{
	Environment e;
	cmm::control_ptr ccp = cmm_ns::get_control("Kernel CMM", nodeid);

	if (CORBA::is_nil(ccp)) {
		return (ENOENT);
	}

	bool retval = ccp->disable_monitoring(e);

	if (e.exception()) {
		return (EIO);
	}

	if (!retval) {
		return (EEXIST);
	}

	return (0);
}

//
// Re-enables path monitoring on the specified node.  Will return ENOENT if
// the failfast admin object for the node is not registered in the
// name server (maybe node is not up). Otherwise it will return EIO if an
// exception was returned.  Otherwise it will return the EEXIST if the
// monitoring was already enabled, or 0 otherwise
//
int
clconf_enable_path_monitoring(nodeid_t nodeid)
{
	Environment e;
	cmm::control_ptr ccp = cmm_ns::get_control("Kernel CMM", nodeid);

	if (CORBA::is_nil(ccp)) {
		return (ENOENT);
	}

	bool retval = ccp->enable_monitoring(e);

	if (e.exception()) {
		return (EIO);
	}

	if (!retval) {
		return (EEXIST);
	}

	return (0);
}

// Signal a cluster shutdown to the cmm module of the specified node.
// Will return:
//    o EIO    if an exception or an error is catched
//    o EEXIST if the cluster shutdown is already in progress
//    o ENOENT if the specified node does not exist
//    o EACCES if the cluster is not ready to perform this operation
//    o 0 otherwise
int
clconf_cluster_shutdown(nodeid_t nodeid)
{
	Environment		e;
	bool			retval;
	CORBA::Exception	*exp;


#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zoneid_t zoneid = getzoneid();
	if (zoneid != GLOBAL_ZONEID) {
		uint_t clid;
		char cluster_name[ZONENAME_MAX];
		if (getzonenamebyid(
		    zoneid, cluster_name, ZONENAME_MAX) == -1) {
			(void) fprintf(stderr, "Failed to get cluster name\n");
			return (ENOENT);
		}

		// Check to see if the virtual cluster exists
		int error = clconf_get_cluster_id(cluster_name, &clid);
		if (error) {
			ASSERT((error == ENOENT) || (error == EACCES));
			(void) fprintf(stderr,
			    "Failed to get cluster id, error %d\n", error);
			return (error);
		}

		char membership_name[MEMBERSHIP_NAME_MAX_LEN];
		if (construct_membership_name(membership_name,
		    membership::ZONE_CLUSTER, cluster_name, NULL)) {
			ASSERT(0);
		}
		membership::manager_var mem_manager_v =
		    cmm_ns::get_membership_manager_ref_zc(
		    cluster_name, membership_name);
		if (CORBA::is_nil(mem_manager_v)) {
			return (ENOENT);
		}
		retval = mem_manager_v->start_shutdown(e);
		if (!retval) {
			return (EIO);
		} else {
			return (0);
		}
	}
#endif

	version_manager::vp_version_t rv;
	version_manager::vm_admin_var vm_adm = vm_util::get_vm(nodeid);

	if (CORBA::is_nil(vm_adm)) {
		return (ENOENT);
	}

	vm_adm->get_running_version("cmm_control", rv, e);

	if ((exp = e.exception()) != NULL) {
		if (version_manager::too_early::_exnarrow(exp)) {
			//
			// This can only happen when there is rolling upgrade
			// in progress.
			//
			return (EACCES);
		}
		(void) fprintf(stderr,\
		    "cluster_shutdown has raised"\
		    " the exception %s\n", e.exception()->_name());
		return (EIO);
	}

	if ((rv.major_num >= 1) && (rv.minor_num >= 1)) {

		cmm::control_ptr ccp =
		    cmm_ns::get_control("Kernel CMM", nodeid);

		if (CORBA::is_nil(ccp)) {
			return (ENOENT);
		}

		retval = ccp->cluster_shutdown(e);

		if (e.exception()) {
			(void) fprintf(stderr,\
			    "cluster_shutdown has raised"\
			    " the exception %s\n", e.exception()->_name());
			return (EIO);
		}

		if (!retval) {
			return (EEXIST);
		}

		return (0);

	} else {
		return (EACCES);
	}
}

//
// Enable the cluster wide panic on the specified node.
// Return Value:
//    o EIO	if an exception or an error is caught
//    o ENOENT  if the specified node does not exist
//    o EEXIST  if the cluster-wide panic is already enabled
//    o EBADRQC if the cluster has non-debug cl_comm binary
//    o 0 Otherwise
// If the cluster node has non-debug bits this function is effectively a no-op.
//
int
clconf_enable_cluster_wide_panic(nodeid_t nodeid)
{
	Environment	env;
	bool		retval;
	CORBA::Exception *exp = NULL;
	cmm::control_ptr cmmp = cmm_ns::get_control("Kernel CMM", nodeid);

	if (CORBA::is_nil(cmmp)) {
		return (ENOENT);
	}

	retval = cmmp->cw_enable_panic(env);

	//
	// A non-zero return value here means that the cluster-wide panic
	// has already been enabled or this is not a debug binary and this
	// is an invalid operation.
	//
	if (!retval) {
#ifdef DEBUG
		return (EEXIST);
#else
		return (EBADRQC);
#endif
	}

	if ((exp = env.exception()) != NULL) {
		(void) fprintf(stderr,\
		    "clconf_enable_cluster_wide_panic has raised"\
		    " the exception %s\n", exp->_name());
		return (EIO);
	}

	return (0);
}

//
// Disable the cluster wide panic on the specified node.
// Return Value:
//    o EIO    if an exception or an error is catched
//    o ENOENT if the specified node does not exist
//    o EEXIST  if the cluster-wide panic is already disabled
//    o EBADRQC if the cluster has non-debug cl_comm binary
//    o 0 Otherwise
// If the cluster node has non-debug bits this function is effectively a no-op.
//
int
clconf_disable_cluster_wide_panic(nodeid_t nodeid)
{
	Environment	env;
	bool		retval;
	CORBA::Exception *exp = NULL;
	cmm::control_ptr cmmp = cmm_ns::get_control("Kernel CMM", nodeid);

	if (CORBA::is_nil(cmmp)) {
		return (ENOENT);
	}

	retval = cmmp->cw_disable_panic(env);

	//
	// A non-zero return value here means that the cluster-wide panic
	// has already been disabled or this is not a debug binary and this
	// is an invalid operation.
	//
	if (!retval) {
#ifdef DEBUG
		return (EEXIST);
#else
		return (EBADRQC);
#endif
	}

	if ((exp = env.exception()) != NULL) {
		(void) fprintf(stderr,\
		    "clconf_disable_cluster_wide_panic has raised"\
		    " the exception %s\n", exp->_name());
		return (EIO);
	}
	return (0);
}
