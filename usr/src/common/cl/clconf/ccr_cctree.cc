/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ccr_cctree.cc	1.37	08/08/01 SMI"

#include <clconf/ccr_cctree.h>
#include <sys/clconf_int.h>

#ifdef	CLCONF_DBGBUF
uint32_t clconf_dbg_size = 10000;
dbg_print_buf clconf_dbg(clconf_dbg_size);
#endif

// A helper function. It checks whether the name contains illegal
// charaters. Returns true if name is ok.
bool
ccr_node::name_ok(const char *str, char ch)
{
	char c;
	while ((c = *str++) != 0) {
		if (c == ch) {
			return (false);
		}
	}
	return (true);
}

ccr_node *
ccr_node::dup()
{
	return (new ccr_node(this));
}

ccr_node *
ccr_node::create_node(const char *n)
{
	return (new ccr_node(n));
}

ccr_node::ccr_node(ccr_node *c) :
	parent(NULL),
	_SList::ListElem((void *)this)
{
	name = os::strdup(c->name);
	value = os::strdup(c->value);
}

ccr_node::ccr_node(const char *n) :
	parent(NULL),
	value(NULL),
	_SList::ListElem((void *)this)
{
	ASSERT(n != NULL);
	ASSERT(ccr_node::name_ok(n));
	name = os::strdup(n);
}

ccr_node::~ccr_node()
{
	ASSERT(children.empty());
	delete[] name;
	delete[] value;
	parent = NULL;
}

// Traverse the tree.
void
ccr_node::traverse(int (*callback)(ccr_node *, void *), void *arg)
{
	// current points to the node in the original tree that we
	// are copying.
	ccr_node *current = this;
	current->atfirst();

	// This loop traverses the tree. We access nodes (callback on them)
	// on the way up the tree (depth first traverse).
	while (true) {	//lint !e716 !e774
		ccr_node *nxt = current->get_current_child();
		if (nxt != NULL) {
			// Note that we only access a node once on the
			// way down, so we initialize the node here.
			nxt->atfirst();
			// Advance the current node, so that next time
			// we get to it we'll have the right child.
			current->advance();
			current = nxt;
		} else {
			// On the way up.
			ccr_node *prnt = current->get_parent();
			if (callback(current, arg) == 0) {
				return;
			}
			if (current == this) {
				// Back to where we started, done.
				break;
			}
			current = prnt;
		}
	}
}

//
// Deep copy of a tree.
// Note: copy_tree is MT safe because the tree's state is not changed during
//	 the deep copy. Therefore, locks are not needed to protect it.
//
ccr_node *
ccr_node::copy_tree()
{
	ccr_node		*nxt, *new_nxt;
	clconfiter		*cur_iter;
	SList<clconfiter>	iter_list;	// keep track of iters

	// new_current points to the newly created node.
	ccr_node *new_current = this->dup();
	new_current->initroot(this);

	// Get the list of this node's children.
	if ((cur_iter = this->get_iterator()) != NULL) {
		// Add it into the iter_list.
		iter_list.append(cur_iter);
	} else {
		// This node has no children.
		return (new_current);
	}

	//
	// This loop performs a preorder tree traversal.
	//
	// Each time we visit a node, we will create a corresponding node
	// in the newly created tree.
	//
	// Then we will check if this node has children or not. If the node
	// has children, we will save its iterator into the end of iter_list
	// and go one level down to visit its children, starting from the
	// leftmost child. If the node has no child, which means it's a leaf
	// node, we will visit its next brother if there is any.
	//
	// After we finish visiting all the children, grandchildren, etc.
	// of a node, we need to take the current iterator out of the
	// iter_list and go one level up to visit that node's next brother
	// if there is any.
	//
	// The reason we use an iterator here is because it won't change the
	// state of a tree while traversing it. The iter_list is a stack-like
	// list that keeps track of the iterators that we have gotten on the
	// way down the tree.
	//
	for (;;) {
		// Get a node from the iterator we are visiting
		nxt = cur_iter->get_current();
		if (nxt != NULL) {
			//
			// On the way down, create new nodes. Note that we
			// only access a node once on the way down, so we
			// initialize the node here.
			//
			new_nxt = nxt->dup();
			(void) new_current->add(new_nxt);

			//
			// Advance the current iterator to adjust the
			// _current pointer of this iterator. So next
			// time we get back to it we'll have its right
			// brother.
			//
			cur_iter->advance();
			// Step down to the next level of the newly created
			// tree.
			new_current = new_nxt;

			// Get the children of the current node
			if ((cur_iter = nxt->get_iterator()) != NULL) {
				// add to the end of iter_list
				iter_list.append(cur_iter);
				//
				// The node we just visited has children,
				// We will go one level down to visit its
				// children.
				//
				continue;
			}
			//
			// If we are here, the node we just visited has no
			// children, which means its a leaf node.
			//
		} else {
			//
			// We are done visiting all the nodes in this
			// iterator(cur_iter). Erase it from the iter_list.
			//
			(void) iter_list.erase(cur_iter);
			delete cur_iter;
		}

		//
		// If we are here, either nxt == NULL, which means we are
		// done visiting all the nodes in the cur_iter, or
		// cur_iter == NULL, which means we have just visited a
		// leaf node.
		//

		//
		// We need to go one level up to the iterator saved at the
		// end of the iter_list.
		//
		iter_list.atlast();
		cur_iter = iter_list.get_current();
		if (cur_iter == NULL) {
			//
			// There are no more saved iterators in the iter_list,
			// so we are done visiting all the nodes in the tree.
			//
			break;
		} else {
			//
			// We stepped one level up in the orignal tree.
			// We also need to step one level up in the newly
			// created tree before continue.
			//
			new_current = new_current->get_parent();
		}
	}
	ASSERT(new_current != NULL);

	return (new_current);
}

// The callback that when used with traverse will delete a tree.
int
delete_tree_call_back(ccr_node *n, void *)
{
	ccr_node *prt = n->get_parent();
	if (prt != NULL) {
		(void) prt->remove(n);
	}
	delete n;
	return (1);
}

// Deep delete of a tree. Note that after this call the node
// is deleted and shouldn't be accessed anymore.
void
ccr_node::delete_tree()
{
	traverse(delete_tree_call_back, NULL);
}

clconfiter *
ccr_node::get_iterator()
{
	if (children.empty()) {
		return (NULL);
	}
	return (new clconfiter(children));
}

void
ccr_node::init_iter(clconfiter *iter)
{
	iter->reinit(children);
}

void
ccr_node::atfirst()
{
	children.atfirst();
}

void
ccr_node::advance()
{
	children.advance();
}

unsigned int
ccr_node::get_num_children()
{
	unsigned int num_children = 0;
	clconfiter iter(children);
	for (; iter.get_current() != NULL; iter.advance()) {
		num_children++;
	}
	return (num_children);
}

ccr_node *
ccr_node::get_current_child()
{
	return (children.get_current());
}

bool
ccr_node::is_leaf()
{
	return (children.empty());
}

ccr_node *
ccr_node::get_named_child(const char *n)
{
	ccr_node *current = NULL;
	clconfiter iter(children);
	while ((current = iter.get_current()) != NULL &&
	    os::strcmp(n, current->name) != 0) {
		iter.advance();
	}
	return (current);
}

ccr_node *
ccr_node::get_named_child_create(const char *n)
{
	if (!ccr_node::name_ok(n)) {
		return (NULL);
	}

	ccr_node *child = get_named_child(n);
	if (child == NULL) {
		// Create the child.
		child = create_node(n);
		if (!add(child)) {
			delete child;
			return (NULL);
		}
	}
	return (child);
}

const char *
ccr_node::get_name()
{
	return (name);
}

// Get the full name of a node.
char *
ccr_node::get_full_name()
{
	SList<ccr_node> stack;

	ccr_node *current = this;

	// Get the full path.
	do {
		stack.prepend(current);
		current = current->get_parent();
	} while (current != NULL);

	// Calculate the name length. Note that the length of the full
	// name is the sum of names on the path plus the number of
	// separators.
	size_t length = 0;
	for (stack.atfirst(); (current = stack.get_current()) != NULL;
	    stack.advance()) {
		length += (os::strlen(current->name) + 1);
	}

	// Construct the name.
	char *full_name = new char[length];

	char *pname = full_name;
	if ((current = stack.reapfirst()) != NULL) {
		(void) os::strcpy(pname, current->name);
		pname += os::strlen(current->name);
	}
	while ((current = stack.reapfirst()) != NULL) {
		(void) os::strcpy(pname, ".");
		pname++;
		(void) os::strcpy(pname, current->name);
		pname += os::strlen(current->name);
	}
	ASSERT(pname == &(full_name[length - 1]));
	full_name[length - 1] = 0;

	return (full_name);
}

const char *
ccr_node::get_value()
{
	return (value);
}

bool
ccr_node::check_name(const char *n)
{
	if (!ccr_node::name_ok(n)) {
		return (false);
	}
	ccr_node *current;
	clconfiter iter(children);
	while ((current = iter.get_current()) != NULL) {
		if (os::strcmp(current->name, n) == 0) {
			return (false);
		}
		iter.advance();
	}
	return (true);
}

void
ccr_node::set_parent(ccr_node *n)
{
	parent = n;
}

bool
ccr_node::add(ccr_node *node)
{
	if (check_name(node->name)) {
		children.append(node);
		node->set_parent(this);
		return (true);
	}
	return (false);
}

bool
ccr_node::remove(ccr_node *node)
{
	return (children.erase(node));
}

bool
ccr_node::set_name(const char *n)
{
	if (!ccr_node::name_ok(n)) {
		return (false);
	}

	if (os::strcmp(name, n) == 0) {
		// Same name, nothing to do.
		return (true);
	}

	if (parent == NULL || parent->check_name(n)) {
		delete[] name;
		name = os::strdup(n);
		return (true);
	}
	return (false);
}

bool
ccr_node::set_value(const char *v)
{
	if (is_leaf()) {
		// Leaf node.
		delete[] value;
		value = os::strdup(v);
		return (true);
	}
	return (false);
}

ccr_node *
ccr_node::get_parent()
{
	return (parent);
}

#ifdef DEBUG
int
ccr_node::print(ccr_node *node, void *)
{
	if (node->is_leaf()) {
		char *fname = node->get_full_name();

		if (node->get_value() != NULL) {
			CLCONF_DBG(("%s\t%s\n", fname, node->get_value()));
		} else {
			CLCONF_DBG(("%s\t\n", fname));
		}
		delete [] fname;
	}
	return (1);
}
#endif

// helper function. Copy one field from ptr to str. Returns false when
// ptr reaches the end.
bool
ccr_node::get_next_str(char *&ptr, char *str, char sep)
{
	if (ptr[0] == 0) {
		return (false);
	}
	while ((*str++ = *ptr++) != sep) {
		// check whether we reached the end of the NULL terminating
		// string.
		if (*(ptr-1) == 0) {
			ptr--;
			break;
		}
	}
	str--;
	*str = 0;
	return (true);
}

// Get a child by the relative name. Create child if not found.
// The name given is one that starts from the this. That is, it contains
// the name of this node.
ccr_node *
ccr_node::get_child_by_name_create(const char *n)
{
	char str[CL_MAX_LEN + 1];

	char *ptr = (char *)n;
	ccr_node *node = this;
	(void) ccr_node::get_next_str(ptr, str); // Get to the next element.

	while (get_next_str(ptr, str)) {
		node = node->get_named_child_create(str);
	}
	return (node);
}

// Get a child by the relative name. Create child if not found.
// The name given doesn't contain the name of this node.
ccr_node *
ccr_node::get_child_by_name_create_ex(const char *n)
{
	char str[CL_MAX_LEN + 1];

	char *ptr = (char *)n;
	ccr_node *node = this;

	while (ccr_node::get_next_str(ptr, str)) {
		node = node->get_named_child_create(str);
	}
	return (node);
}

// Get a child by the relative name.
// The name given is one that starts from the this. That is, it contains
// the node of this node.
ccr_node *
ccr_node::get_child_by_name(const char *n)
{
	char str[CL_MAX_LEN + 1];

	char *ptr = (char *)n;
	ccr_node *node = this;
	(void) ccr_node::get_next_str(ptr, str); // Get to the next element.

	while (get_next_str(ptr, str)) {
		node = node->get_named_child(str);
		if (node == NULL) {
			return (NULL);
		}
	}
	return (node);
}

// Nothing special to do for the root here.
void
ccr_node::initroot(ccr_node *)
{
}
