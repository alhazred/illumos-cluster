/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clnode.cc	1.60	08/06/30 SMI"

#include <clconf/clnode.h>
#ifndef DEBUGGER_PRINT
#include <clconf/clconf_ccr.h>
#include <clconf/clconf_file_io.h>
#endif // DEBUGGER_PRINT
#include <sys/cl_assert.h>

#if (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)
#include <sys/cladm_int.h>
#endif	// (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <sys/hashtable.h>
#include <clconf/clconf_ccr.h>
//
// For each virtual cluster, there is a clconf tree object created.
// The hashtable stores the clconf trees for all virtual clusters.
// This hashtable is populated when the clconf initializes in global zone only.
// Inside non-global zones (virtual clusters as well as non-cluster-wide zones),
// these pointers are NULL.
//
string_hashtable_t<cl_current_tree *> *vc_treesp = NULL;
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

cl_current_tree *cl_current_tree::the_cl_current_tree = NULL;
extern SList<clconf_path_t> *clconf_path_list;

// Get the name of the cluster in which the clconf code is running
extern char *get_clname_for_clconf();

// static
int
cl_current_tree::initialize()
{
	int the_err = 0;

#ifdef	_KERNEL_ORB
	ASSERT(the_cl_current_tree == NULL);
#else
	clconf_ccr::clconf_ccr_lock.lock();
	if (the_cl_current_tree != NULL) {
		clconf_ccr::clconf_ccr_lock.unlock();
		return (0);
	}
#endif	// _KERNEL_ORB

	char *cl_namep = get_clname_for_clconf();
	if (cl_namep == NULL) {
#ifdef DEBUG
		os::warning("could not get current cluster name\n");
#endif
		return (ENOENT);
	}

	the_cl_current_tree = new cl_current_tree(cl_namep);
	if (the_cl_current_tree == NULL) {
		the_err = ENOMEM;
	} else {
		clconf_path_list = new SList<clconf_path_t>;
		if (clconf_path_list == NULL) {
			the_err = ENOMEM;
		}
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If no error has happened till now,
	// and if we are running in global zone, then allocate memory
	// for the hashtable that stores virtual cluster clconf trees.
	//
	if ((the_err == 0) && (getzoneid() == GLOBAL_ZONEID)) {
		vc_treesp = new string_hashtable_t<cl_current_tree *>;
		if (vc_treesp == NULL) {
			the_err = ENOMEM;
		}
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#ifndef	_KERNEL_ORB
	clconf_ccr::clconf_ccr_lock.unlock();
#endif
	delete [] cl_namep;
	return (the_err);
}

// static
void
cl_current_tree::shutdown()
{
	// XXX
	// XXX Need an assertion here to check that all virtual cluster
	// XXX clconf trees have been freed up and removed.
	// XXX

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	if (vc_treesp != NULL) {
		delete vc_treesp;
		vc_treesp = NULL;
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	if (clconf_path_list != NULL) {
		delete clconf_path_list;
		clconf_path_list = NULL;
	}
	if (the_cl_current_tree != NULL) {
		delete the_cl_current_tree;
		the_cl_current_tree = NULL;
	}
}

//
// Get the clconf tree of the cluster we are in.
// There is a special case here for kernel threads.
// If a kernel thread running in the context of a non-global zone
// calls this method, it gets the clconf tree it is entitled to.
// That is, if the thread is for a non-cluster-wide zone,
// then it gets the kernel global zone clconf tree.
// Else if the thread is for a cluster-wide zone, it gets the clconf
// tree for the virtual cluster which is stored in the kernel.
//
cl_current_tree &
cl_current_tree::the()
{
	ASSERT(the_cl_current_tree != NULL);

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	if (getzoneid() != GLOBAL_ZONEID) {
		char *cl_namep = get_clname_for_clconf();
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			// Non-cluster-wide zone
			return (*the_cl_current_tree);
		}
		// virtual cluster clconf tree
		cl_current_tree *treep = vc_treesp->lookup(cl_namep);
		ASSERT(treep != NULL);
		return (*treep);
	}
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

	return (*the_cl_current_tree);
}

//
// Get the clconf tree for a particular cluster.
// Anyone calling this method from a non-global zone context would
// get the clconf tree for the cluster the zone is a part of.
// If the caller is in global zone, then it can get the clconf tree
// of any virtual cluster or of the global zone cluster.
//
cl_current_tree &
cl_current_tree::cl_tree(const char *cl_namep)
{
	ASSERT(this == the_cl_current_tree);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	if (getzoneid() != GLOBAL_ZONEID) {
		//
		// Cannot obtain the tree of another cluster
		// from inside a virtual cluster.
		//
		ASSERT(os::strcmp(cl_namep, cluster_namep) == 0);
		return (cl_current_tree::the());
	} else {
		// Global zone
		if (os::strcmp(cl_namep, DEFAULT_CLUSTER) == 0) {
			return (cl_current_tree::the());
		} else {
			// request for the tree of a virtual cluster
			cl_current_tree *treep = vc_treesp->lookup(cl_namep);
			ASSERT(treep != NULL);
			return (*treep);
		}
	}
#else	// (SOL_VERSION >= __s10) && !defined(UNODE)
	return (cl_current_tree::the());
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifndef	_KERNEL_ORB
//
// This method makes sure that the clconf trees of the zone clusters
// have the latest data from infrastructure tables in CCR.
//
int
cl_current_tree::refresh_zc_clconf_trees()
{
	int retval = 0;
	if (getzoneid() != GLOBAL_ZONEID) {
		return (EPERM);
	}

	cl_current_tree *treep = NULL;
	string_hashtable_t<cl_current_tree *>::iterator iter(*vc_treesp);
	for (; (treep = iter.get_current()) != NULL; iter.advance()) {
		bool error;
		if (!treep->read_ccr(&error)) {
			if (error) {
				// Could not refresh a zc clconf tree
				retval = EIO;
			}
		}
	}
	return (retval);
}
#endif	// !_KERNEL_ORB

//
// Returns the number of zone clusters configured in CCR.
// (Note that base cluster is not considered as a zone cluster)
// Returns 0 if called in non-global zone.
//
uint32_t
cl_current_tree::get_num_virtual_clusters()
{
	if (getzoneid() != GLOBAL_ZONEID) {
		return (0);
	} else {
		uint32_t count = 0;
		cl_current_tree *treep = NULL;
		string_hashtable_t<cl_current_tree *>::iterator
		    iter(*vc_treesp);
		for (; (treep = iter.get_current()) != NULL; iter.advance()) {
			count++;
		}
		return (count);
	}
}

//
// Returns the names of zone clusters configured in CCR.
// (Note that base cluster is not considered as a zone cluster)
// The names are returned as an array of strings.
// The array is NULL terminated, that is, the last index in the array
// of pointers contains NULL.
// The caller needs to free the memory allocated.
// Returns NULL if called in non-global zone.
//
char **
cl_current_tree::get_virtual_cluster_names()
{
	if (getzoneid() != GLOBAL_ZONEID) {
		return (NULL);
	} else {
		char **namesp = NULL;
		unsigned int index = 0;
		cl_current_tree *treep = NULL;

		uint32_t num_clusters = get_num_virtual_clusters();
		namesp = new char*[num_clusters + 1];

		string_hashtable_t<cl_current_tree *>::iterator
		    iter(*vc_treesp);
		for (; (treep = iter.get_current()) != NULL; iter.advance()) {
			namesp[index++] = os::strdup(treep->cluster_namep);
		}
		ASSERT(index == num_clusters);
		namesp[index] = NULL;	// Null terminate the array
		return (namesp);
	}
}

//
// Creates and adds a clconf tree object to the table of clconf trees.
//
int
cl_current_tree::add_cluster(const char *cl_namep)
{
	cl_current_tree *treep = vc_treesp->lookup(cl_namep);
	ASSERT(treep == NULL);
	treep = new cl_current_tree(cl_namep);
	if (treep == NULL) {
		return (ENOMEM);
	}
	vc_treesp->add(treep, cl_namep);
	return (0);
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

//
// Get the root of the current clconf tree. This is the entry point to
// access info in the clconf so we inc the reference count here.
//
clnode *
cl_current_tree::get_root()
{
#ifndef _KERNEL_ORB
	bool	error;

	/*
	 * Update the clconf tree if the infrastructure file is changed.
	 * This is done only in user mode.
	 * In kernel mode read_ccr should be done in the callback and not
	 * here because, get_root() can be called from clock/interrupt
	 * context and read_ccr does blocking memory allocations.
	 */

	(void) read_ccr(&error);
#endif

	root_lock.lock();
	clnode *ret = root;
	if (ret != NULL) {
		ret->hold();
	}
	root_lock.unlock();
	return (ret);
}

#ifdef _KERNEL_ORB
//
// old root should only be used during topology manager callback.
// Any attempt to get old_root after the callback would return NULL
// since there should not be a reference to (and any use of) old_root
// after the topology manager callback.
//
clnode *
cl_current_tree::get_old_root()
{
	root_lock.lock();
	clnode *ret = old_root;
	if (ret != NULL)
		ret->hold();
	root_lock.unlock();
	return (ret);
}

void
cl_current_tree::destroy_old_root()
{
	root_lock.lock();
	ASSERT(old_root != NULL);
	old_root->rele();
	old_root = NULL;
	root_lock.unlock();
}
#endif

void
cl_current_tree::set_io_interface(clconf_io *in)
{
	ccr_ptr = in;
}

cl_current_tree::cl_current_tree(const char *cl_namep) :
    ccr_ptr(NULL), root(NULL),
#ifdef _KERNEL_ORB
    old_root(NULL),
#endif
    current_version(-1)
{
	cluster_namep = os::strdup(cl_namep);
}

cl_current_tree::~cl_current_tree()
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If we are running in global zone,
	// and if this clconf tree is for a virtual cluster,
	// then remove the tree from the table of clconf trees.
	//
	if ((getzoneid() == GLOBAL_ZONEID) &&
	    (os::strcmp(cluster_namep, DEFAULT_CLUSTER) != 0)) {
		cl_current_tree *treep = vc_treesp->remove(cluster_namep);
		ASSERT(treep == this);
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)
	delete [] cluster_namep;
}

//
// clcurrent_tree::read_ccr()
//
// Construct clconf tree based on configuration data.
//
//	ccr_read_lock() is used to prevent two threads from simultaneously
//	updating the read ptr( internal to ccr_ptr). ccr_ptr->open() set
//	read ptr to beginning of ccr information. Subsequent reads by either
//	reader will advance the ptr and return incorrect output to both readers.
//
// Return values:
//	true  : A new tree was successfully read in.
//	false : New tree was not read in
//		*errorp = true : there was an error
//		*errorp = false : there was no need to reread the tree as it
//				was up to date already.
//
bool
cl_current_tree::read_ccr(bool *errorp)
{
	clconf_io::status result;
	clnode *clp;
	int32_t new_version;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	// Get the cluster id
	uint32_t cl_id = 0;
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) != 0) {
#ifdef _KERNEL
		int32_t err =
		    clconf_ccr::the().get_cluster_id(cluster_namep, cl_id);
		ASSERT(err == 0);
#else	// _KERNEL
		cz_id_t czid;
		czid.clid = 0;
		(void) os::strcpy(czid.name, cluster_namep);
		if (int ret = os::cladm(CL_CONFIG, CL_GET_ZC_ID, &czid) != 0) {
#ifdef DEBUG
			os::warning("could not get current cluster id, "
			    "ret val = %d\n", ret);
#endif
			return (false);
		}
		cl_id = czid.clid;
#endif	// _KERNEL
		ASSERT(cl_id != 0);
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	ccr_read_lock.lock();
	ASSERT(ccr_ptr != NULL);

	new_version = ccr_ptr->get_version();
	// negative versions are not valid and they will be changed after
	// CCR recovery is done.
	if ((new_version == current_version) && (new_version >= 0)) {
		ccr_read_lock.unlock();
		//
		// Since we have not really read the CCR, return false.
		// However set the error code to false to indicate that
		// there was no error.
		//
		*errorp = false;
		return (false);
	}

	do {
		clp = new clnode("cluster");
		clp->initroot(NULL);

		while ((result = ccr_ptr->open()) == clconf_io::C_RETRY);
		if (result == clconf_io::C_FAIL) {
#ifdef DEBUG
			os::warning("Can't open the ccr file for cluster %s",
			    cluster_namep);
#endif
			ccr_read_lock.unlock();

			*errorp = true;
			return (false);
		}

		// read CCR.
		char *key = NULL;
		char *value = NULL;
		while ((result = ccr_ptr->read_one(&key, &value))
		    == clconf_io::C_SUCCESS) {
			(void) clp->get_child_by_name_create(key)->
			    set_value(value);
			delete [] key;
			delete [] value;
		}
		ccr_ptr->close();

		if (result == clconf_io::C_RETRY) {
			clp->delete_tree();
		}

	} while (result == clconf_io::C_RETRY);

	current_version = new_version;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// For a virtual cluster clconf tree, put in the cluster id
	// onto the tree. Since the cluster id is 32 bit,
	// a string of 30 chars should be more than enough.
	//
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) != 0) {
		char id_str[30];
		int num_chars = os::itoa(cl_id, id_str);
		ASSERT((num_chars > 0) && (num_chars < 30));
		(void) clp->get_child_by_name_create(
		    "cluster.properties.cluster_id")->set_value(id_str);
	}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	ccr_read_lock.unlock();

	set_root(clp);
#ifdef DEBUG
	clp->traverse(ccr_node::print, NULL);
#endif
	return (true);
}

#ifndef _KERNEL
/*
 * The following function is to write an updated clconf tree
 * to the CCR. This is used only in non-cluster mode when
 * updating the CCR with the new private IP address range.
 */
bool
cl_current_tree::write_nonclust_ccr(clnode *proposed)
{
	clconf_io::status result;

	// Open the CCR file for writing
	while ((result = ccr_ptr->open()) == clconf_io::C_RETRY);
	if (result == clconf_io::C_FAIL) {
		os::warning("Can't open the ccr file");
		return (false);
	}

	// write CCR.
	int err;
	void *seq = ccr_ptr->create_seq();
	if (seq != NULL) {
		proposed->traverse(clconf_file_io::append_node, seq);
		err = ccr_ptr->write_seq(seq);
		if (err != 0) {
			ccr_ptr->delete_seq(seq);
			ccr_ptr->close();
			return (false);
		}
		ccr_ptr->delete_seq(seq);
	}

	// Close the CCR file
	ccr_ptr->close();

	return (true);
}
#endif

// Replace the current tree with the proposed one.
// We do not keep the old_root in user land and hence we do the rele()
// on the root object before the new value is assigned to it.
// If the proposed root is NULL, then we do not save the root in old_root
// and release the root object.
void
cl_current_tree::set_root(clnode *proposed)
{
	root_lock.lock();
	if (root == NULL) {
		root = proposed;
	} else {
		if (proposed != NULL) {
			proposed->cnt->incn_num = root->cnt->incn_num+1;
		}
#ifdef _KERNEL_ORB
		if (proposed == NULL) {
			root->rele();
			old_root = NULL;
		} else
			old_root = root;
#else
		root->rele();
#endif
		root = proposed;
	}
	root_lock.unlock();

	if (proposed == NULL) {
		delete this;
	}
}

#ifndef DEBUGGER_PRINT
//
// cl_current_tree::lock_ccr
//
// This method "locks" the CCR infrastructure table for writing the contents
// of the proposed tree passed to it. Locking is in essence done by starting
// a CCR transaction. It is assumed that the proposed tree has been obtained
// by modifying the copy of a tree that was previously read from the CCR. This
// method also makes checks to verify that no updates have taken place to the
// CCR infrastructre table since the time the original tree (from which the
// proposed tree has been derived) was read and now. If one or more updates
// have taken place, the CCR lock is dropped (i.e., the transaction is
// aborted) and false is returned. True is returned otherwise.
//
// If this method returns with success, the subsequent write CCR must be called
// with the same proposed tree. The subsequent write_ccr method will drop the
// CCR lock after updating the CCR. If for some reason the caller decides not
// to write to the CCR, it must drop the CCR lock by calling unlock_ccr
// explicitly.
//
bool
cl_current_tree::lock_ccr(clnode *proposed)
{
	bool	error;

	//
	// Start a CCR transaction. This will prevent any other infrastructure
	// reads or writes from taking place while we prepare to write to the
	// CCR.
	//
	if (!ccr_ptr->begin_transaction()) {
		return (false);
	}

	//
	// Refresh the current tree by rereading the CCR. The proposed tree
	// will be compared with the current tree to detect if the proposed
	// tree has become out of date in the next step. Note that this read
	// must happen after the transaction has been started for the said
	// comparison to be valid.
	//
	if (!read_ccr(&error)) {
		if (error) {
			//
			// Error reading the CCR
			//
			ccr_ptr->abort_transaction();
			return (false);
		}
	}

	//
	// Now make sure that the proposed tree that we are about to write
	// to the CCR has not been already rendered out of date by other
	// competing CCR updates. If the proposed tree is truly a derivative
	// of the current tree then its incarnation number will be the same
	// as the incarnation number of the current tree.
	//
	bool good_proposed_tree = false;
	root_lock.lock();
	ASSERT(root->cnt->incn_num >= proposed->cnt->incn_num);
	if (root->cnt->incn_num == proposed->cnt->incn_num) {
		good_proposed_tree = true;
	}
	root_lock.unlock();

	if (good_proposed_tree) {
		return (true);
	}

	//
	// Tree is stale. Lock CCR operation must fail. Abort the trasaction
	// that was started at the beginning of this method.
	//
	ccr_ptr->abort_transaction();

	return (false);
}

//
// cl_current_tree::unlock_ccr
//
// The unlock_ccr method aborts the transaction started by a previous call to
// lock_ccr. This call is only necessary if a write_ccr does not follow the
// call to lock_ccr.
//
void
cl_current_tree::unlock_ccr()
{
	ccr_ptr->abort_transaction();
}

//
// cl_current_tree::write_ccr()
//
// This method writes the given proposed tree to the CCR infrastructure table.
// The CCR must have been locked by a preceding call to lock_ccr with the same
// proposed tree. This method drops the above mentioned lock upon return
// irrespective of the result.
//
bool
cl_current_tree::write_ccr(clnode *proposed)
{
#ifdef _KERNEL
	os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG, "CCR", NULL);
	//
	// SCMSGS
	// @explanation
	// Routine write_ccr that writes a clconf tree out to CCR should not
	// be called from kernel.
	// @user_action
	// No action required. This is informational message.
	//
	(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
	    "clconf: Write_ccr routine shouldn't be called from kernel");
#endif
	//
	// Should never have to write the current tree to ccr. The current
	// tree should be the same as in ccr.
	//
	CL_PANIC(proposed != root);

	//
	// Make sure we have not been asked to write a stale tree. This assert
	// can not fail if as prescribed a successful call to lock_ccr was made
	// with the same proposed tree before this call to write_ccr.
	//
	root_lock.lock();
	ASSERT(root->cnt->incn_num == proposed->cnt->incn_num);
	root_lock.unlock();

	//
	// Write the nodes into the sequence and attempt to committ.
	//
	void *seq = ccr_ptr->create_seq();
	if (seq != NULL) {
		proposed->traverse(clconf_ccr::append_node, seq);
		int err = ccr_ptr->write_seq(seq);
		ccr_ptr->delete_seq(seq);
		if (err == 0) {
			bool success = ccr_ptr->commit_transaction();
			//
			// Commit transaction has already dropped the CCR
			// transaction lock irrespective of the outcome.
			//
			return (success);
		}
	}

	//
	// Something went wrong during either creating the sequence or while
	// writing to it. Abort the transaction and return with failure.
	//

	ccr_ptr->abort_transaction();

	return (false);
}
#endif // DEBUGGER_PRINT

clrefcnt::clrefcnt(clnode *r) : incn_num(1), root(r)
{
}

clrefcnt::~clrefcnt()
{
	root->delete_tree();
	root = NULL;
}

clnode::clnode(clnode *c) : ccr_node(c), cnt(NULL)
{
}

clnode::clnode(const char *c) : ccr_node(c), cnt(NULL)
{
}

ccr_node *
clnode::dup()
{
	return ((ccr_node *)new clnode(this));
}

ccr_node *
clnode::create_node(const char *n)
{
	return ((ccr_node *)new clnode(n));
}

void
clnode::initroot(ccr_node *oldroot)
{
	cnt = new clrefcnt(this);
	if (oldroot != NULL) {
		cnt->incn_num = ((clnode *)oldroot)->cnt->incn_num;
	}
}

void
clnode::setup_name()
{
	if (name != NULL && os::strcmp(name, "-1") != 0) {
		// name is already setup.
		return;
	}

	// Name is really the id for an entity. We need to set it up. We will
	// use the least unused id under this parent.
	ccr_node *c;
	clconfiter *iter = parent->get_iterator();
	int unused = 0;
	bool found;
	// We need this outerloop because the names are not guaranteed to
	// be in sorted order.
	do {
		unused++;
		found = true;
		while ((c = iter->get_current()) != NULL) {
			int v = os::atoi(c->get_name());
			if (v == unused) {
				found = false;
				// rewind the iterator
				parent->init_iter(iter);
				break;
			}
			iter->advance();
		}
	} while (!found);
	delete iter;
	delete[] name;

	char ch[32];
	(void) os::itoa(unused, ch);
	name = os::strdup(ch);
}

clconf_errnum_t
clnode::add_to_group(const char *grpname, clnode *node)
{
	if (get_named_child_create(grpname)->add(node)) {
		node->setup_name();
		return (CL_NOERROR);
	}
	return (CL_INVALID_ID);
}

clconf_errnum_t
clnode::remove_from_group(const char *grpname, clnode *node)
{
	clnode *parent_node = (clnode *)node->get_parent();

	if (parent_node != get_named_child(grpname)) {
		return (CL_INVALID_TREE);
	}
	node->delete_tree();

	if (parent_node->is_leaf()) {
		(void) remove(parent_node);
		delete parent_node;
	}
	return (CL_NOERROR);
}
