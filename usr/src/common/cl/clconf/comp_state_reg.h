/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  comp_state_reg.h
 *
 */

#ifndef _COMP_STATE_REG_H
#define	_COMP_STATE_REG_H

#pragma ident	"@(#)comp_state_reg.h	1.8	08/05/20 SMI"

#include <h/component_state.h>
#include <orb/object/adapter.h>
#include <clconf/comp_state_proxy.h>
#include <sys/hashtable.h>
#include <sys/clconf_int.h>

//
// The Component State Registry
//
// One of these objects exists on each node, they keep track of the the
// component state proxie's and field requests from users about component
// status.
//
// The two IDL interfaces are:
//	get_state
//		given a component name will return status or an exception
//	get_state_list
//		will return a sequence of status.  (For all the state proxies
//		on this node.
//
//
class component_state_reg : public McServerof<component_state::registry> {
public:
	// Constructor
	component_state_reg();

	// Destructor
	~component_state_reg();

	//
	// IDL Interfaces
	//
	// Get the state of a named component
	void get_state(const char *name,
	    component_state::component_state_t_out state, Environment &e);

	// Get a list of states for all named component on this node
	void get_state_list(
	    component_state::component_state_list_t_out state_list,
	    Environment &e);

	// deal with _unref
	void _unreferenced(unref_t);

	// Initialization is done by a 2 step process.
	// Early in orb initialization we need to call create_registry()
	// which will create the implementation object.  This is neccesary so
	// we can provide services to kernel proxies that get created early in
	// the boot process.
	//
	// Then later we can call intialize_registry() which will bind the
	// registry on the root nameserver in order for it to be availble to
	// cluster clients
	//
	static int	create_registry();
	static int	initialize_registry();

	//
	// shutdown the registry
	// will remove the registry from the nameserver
	// and clean up the "the" pointer
	static int	shutdown_registry();

	// get "the" state registry (static instance)
	//
	// NOTE: It is illegal use of the "the()" method to get a pointer to
	// the impl object and then call get_objref() on that pointer.
	// If the registry has not been bound the the root nameserver and you
	// get a obj_ref() to it (which then is released) you can cause
	// premature deletion of the registry object.
	static component_state_reg *the();

	// Add a proxy with the registry
	int add_proxy(component_state_proxy *csp);

	// Remove proxy from registry
	int remove_proxy(component_state_proxy *csp);

private:
	// Hash table that will hold pointers to the state proxy's objects
	os::mutex_t					table_lock;
	string_hashtable_t<component_state_proxy *>	proxy_hashtab;
	uint_t						proxy_count;

	// "the" component_state_reg
	static component_state_reg	*the_component_state_reg;

	// Remove proxy from registry
	int _remove_proxy(component_state_proxy *csp);


}; // component_state_reg

#include <clconf/comp_state_reg_in.h>

#endif	/* _COMP_STATE_REG_H */
