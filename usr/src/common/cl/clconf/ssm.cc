//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ssm.cc	1.70	08/05/20 SMI"

#include <stdlib.h>
#include <orb/infrastructure/orb.h>
#include <sys/ssm.h>
#include <nslib/ns.h>
#include <h/ccr.h>
#include <h/network.h>
#include <sys/cladm.h>
#include <sys/rm_util.h>
#include <clconf/clpdt.h>
#include <cl_net/netlib.h>

#if SOL_VERSION >= __s10
#define	SCTP_RGM_SUPPORT
#endif

static os::mutex_t ssm_init_mutex;

//
// Effects: Returns an object reference to the PDT server primary, else
//  returns NULL.
//
network::PDTServer_ptr
get_pdt_server()
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	controlp;
	CORBA::Object_var		nobjv;

	//
	// Get a reference to PDTServer object.
	//
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));
	controlp = rm_ref->get_control(network::PDTServer::NS_NAME, e);
	if (e.exception()) {
		e.clear();
		return (NULL);
	}

	//
	// Return the object reference of the current primary.
	//
	nobjv = controlp->get_root_obj(e);
	if (e.exception()) {
		e.exception()->print_exception("get_pdt_server: "
		    "Can't find root object\n");
		e.clear();
		return (NULL);
	}

	//
	// Coerce corba object to PDTServer object
	//
	network::PDTServer_ptr pdtsp = network::PDTServer::_narrow(nobjv);

	ASSERT(!CORBA::is_nil(pdtsp));

	return (pdtsp);
}

//
// Effects: This routine initializes the SSM API.  Each routine in this
//   API calls this routine first to ensure that cluster networking is
//   setup and accessible.
//   Returns the object reference to the PDT server.
//
static network::PDTServer_ptr
ssm_initialize(void)
{
	//
	// The reference to the PDT server that we'll be returning
	// is cached in the static variable pdt_server after the
	// first successful call to get_pdt_server().  The flag
	// ssm_initialized controls whether a lookup is needed or
	// has already succeeded.  Since this routine could be
	// called before cl_net is loaded (such as when the ssmw
	// unode module is loaded before cl_net), get_pdt_server()
	// may return NULL, requiring a retry on a subsequent call,
	// but since the ORB is already up and we don't want to
	// call ORB::initialize more than once, there is a separate
	// orb_up flag to indicate if the ORB needs to be initialized.
	// The mutex lock ensures correct operation if two or more
	// threads call this routine at the same time.
	//
	static bool ssm_initialized = false;
	static bool orb_up = false;
	static network::PDTServer_ptr pdt_server;

	ssm_init_mutex.lock();

	if (!ssm_initialized) {
		//
		// Initialize the ORB.
		//
		if (!orb_up) {
			if (ORB::initialize() != 0) {
				ssm_init_mutex.unlock();
				return (NULL);
			} else
				orb_up = true;
		}

		pdt_server = get_pdt_server();

		//
		// If we got a pointer to the pdt server, set the
		// flag to true to ensure that we skip this conditional
		// the next time around.  If we don't get a pointer it
		// is unlikely we'll get one later, except in the case
		// of unode where the cl_net module could be loaded
		// between one call to us and the next.
		//
		if (!CORBA::is_nil(pdt_server))
			ssm_initialized = true;
	}

	ssm_init_mutex.unlock();

	return (pdt_server);
}

//
// Effects: Convert the service "ssm_svc" to the "sap."  Returns
//   SCHA_SSM_SUCCESS if the conversion occurs successfully.
// Parameters:
//  - ssm_svc (IN): structure that define a service
//  - sap (OUT): service access point
// Exceptions:
//  - SCHA_SSM_NO_SERVICE:  if "ssm_svc" is NULL.
//  - SCHA_SSM_INVALID_PROTOCOL:   if protocol is unknown.  Legal
//				values are for TCP and UDP.
//
static scha_ssm_exceptions
ssm_service_to_sap(ssm_service_t *ssm_svc, network::service_sap_t& sap)
{
	//
	// If service is NULL, return SCHA_SSM_NO_SERVICE
	//
	if (ssm_svc == NULL) {
		return (SCHA_SSM_NO_SERVICE);
	}

	//
	// Check if it is a valid protocol.
	// In not, return SCHA_SSM_INVALID_PROTOCOL.
	//
	switch (ssm_svc->protocol) {
	case IPPROTO_TCP:
	case IPPROTO_UDP:
#ifdef SCTP_RGM_SUPPORT
	case IPPROTO_SCTP:
#endif
	case 0: // Means any protocol //
		break;
	default:
		return (SCHA_SSM_INVALID_PROTOCOL);
	}

	//
	// Convert to our internal structure
	//
	sap.lport = ssm_svc->port;
	sap.protocol = (uchar_t)ssm_svc->protocol;
	bcopy(&ssm_svc->ipaddr, &sap.laddr, sizeof (sap.laddr));

	return (SCHA_SSM_SUCCESS);
}

static scha_ssm_exceptions
ssm_group_to_group(ssm_group_t		*g_name, 	// OUT
		network::group_t&	group)		// OUT
{
	//
	// Check if group name is NULL
	//
	if (g_name == NULL) {
		return (SCHA_SSM_NO_GROUP);
	}

	//
	// Initialize members of group
	//
	(void) strcpy(group.resource, g_name->resource);

	return (SCHA_SSM_SUCCESS);
}

static scha_ssm_exceptions
ssm_policy_to_pdtpolicy(scha_ssm_policy_t	policy, 	// IN
		    network::lb_policy_t&	pdtpolicy_kind)	// OUT
{
	scha_ssm_exceptions	result = SCHA_SSM_SUCCESS;

	//
	// Check if the load balancing policy is valid. If yes, set
	// pdtpolicy_kind, else return SCHA_SSM_INVALID_POLICY.
	//
	switch (policy) {
	case scha_ssm_lb_weighted:
		pdtpolicy_kind = network::LB_WEIGHTED;
		break;
	case scha_ssm_lb_perf:
		pdtpolicy_kind = network::LB_PERF;
		break;
	case scha_ssm_lb_user:
		pdtpolicy_kind = network::LB_USER;
		break;
	case scha_ssm_lb_st:
		pdtpolicy_kind = network::LB_ST;
		break;
	case scha_ssm_lb_sw:
		pdtpolicy_kind = network::LB_SW;
		break;
	default:
		result = SCHA_SSM_INVALID_POLICY;
	} // end switch

	return (result);
}

//
// PUBLIC ROUTINES
//

extern "C" {

scha_ssm_exceptions
ssm_create_scal_srv_grp(ssm_group_t *g_name, scha_ssm_policy_t policy)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::lb_policy_t	pdtpolicy_kind;
	network::group_t	group;
	network::ret_value_t	ret_value;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_policy_to_pdtpolicy(policy, pdtpolicy_kind)) !=
	    SCHA_SSM_SUCCESS) {
		return (ret);
	}

	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Tell the PDT server about the new scalable service group.
	//
	ret_value = pdt_server->create_scal_srv_grp(group, pdtpolicy_kind, e);

	if (e.exception()) {
		//
		// exception means a system error
		//
		e.exception()->print_exception("ssm_create_scal_srv_grp");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	switch (ret_value) {
	case network::GROUP_EXISTS:
		return (SCHA_SSM_GROUP_EXISTS);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::NOSUCH_GROUP:
	case network::INVALID_SERVICE_GROUP:
	case network::SERVICE_EXISTS:
	case network::NOSUCH_SERVICE:
	case network::NODE_EXISTS:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::INTERNAL_ERROR:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_del_scal_srv_grp(ssm_group_t *g_name)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::ret_value_t	ret_value;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Tell the PDT server to remove the scalable service.
	//
	ret_value = pdt_server->delete_scal_srv_grp(group, e);

	if (e.exception()) {
		//
		// exception means a system error
		//
		e.exception()->print_exception("ssm_del_scal_srv_grp");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	//
	// Check for error where the group is not known, and
	// raise SCHA_SSM_INVALID_SERVICE_GROUP.
	//
	switch (ret_value) {
	case network::INVALID_SERVICE_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::SERVICE_EXISTS:
		return (SCHA_SSM_SERVICE_EXISTS);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::GROUP_EXISTS:
	case network::NOSUCH_GROUP:
	case network::NOSUCH_SERVICE:
	case network::NODE_EXISTS:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::INTERNAL_ERROR:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_add_nodeid(ssm_group_t *g_name, nodeid_t node)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::ret_value_t	ret_value;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Check if the nodeid is valid. If it less than 0 or greater
	// than NODEID_MAX return SCHA_SSM_INVALID_NODEID.
	//
	if (node == 0 || node > NODEID_MAX) {
		return (SCHA_SSM_INVALID_NODEID);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Tell PDT server about the service instance can be run on
	// the specified node for every service belonging to the
	// specified service group.
	//
	ret_value = pdt_server->add_nodeid(group, node, e);

	if (e.exception()) {
		e.exception()->print_exception("ssm_add_nodeid");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	//
	// If the service doesnt exist, return SCHA_SSM_NO_SUCH_SERVICE.
	// If the node already exists in the node list for the service
	// return SCHA_SSM_NODE_EXISTS.
	//
	switch (ret_value) {
	case network::NODE_EXISTS:
		return (SCHA_SSM_NODE_EXISTS);
	case network::NOSUCH_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::GROUP_EXISTS:
	case network::INVALID_SERVICE_GROUP:
	case network::SERVICE_EXISTS:
	case network::NOSUCH_SERVICE:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::INTERNAL_ERROR:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_remove_nodeid(ssm_group_t *g_name, nodeid_t node)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::ret_value_t	ret_value;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Check if the nodeid is valid. If it less than 0 or greater
	// than NODEID_MAX return SCHA_SSM_INVALID_NODEID.
	//
	if (node == 0 || node > NODEID_MAX) {
		return (SCHA_SSM_INVALID_NODEID);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Inform the PDT server to remove the specified node from the
	// config_list of all the services belonging to the specified
	// group.
	//
	ret_value = pdt_server->remove_nodeid(group, node, e);

	if (e.exception()) {
		//
		// exception means a system error
		//
		e.exception()->print_exception("ssm_remove_nodeid");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	//
	// If the group doesnt exist, return SCHA_SSM_INVALID_SERVICE_GROUP.
	// If the node doesnt exist in the nodelist for the
	// service, return SCHA_SSM_NO_NODEID.
	//

	switch (ret_value) {
	case network::NOSUCH_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::NO_NODEID:
		return (SCHA_SSM_NO_NODEID);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::GROUP_EXISTS:
	case network::INVALID_SERVICE_GROUP:
	case network::SERVICE_EXISTS:
	case network::NOSUCH_SERVICE:
	case network::NODE_EXISTS:
	case network::INVALID_PROTOCOL:
	case network::INTERNAL_ERROR:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_add_scal_service(ssm_group_t *g_name, ssm_service_t *ssm_svc)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::ret_value_t	ret_value;
	network::service_sap_t	sap;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	if ((ret = ssm_service_to_sap(ssm_svc, sap)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Tell the PDT server to add the IP address to the specified
	// group.
	//
	ret_value = pdt_server->add_scal_service(group, sap, e);

	if (e.exception()) {
		e.exception()->print_exception("ssm_add_scal_service");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	switch (ret_value) {
	case network::INVALID_SERVICE_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::SERVICE_CONFLICTS:
		return (SCHA_SSM_SERVICE_CONFLICTS);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::GROUP_EXISTS:
	case network::NOSUCH_GROUP:
	case network::NOSUCH_SERVICE:
	case network::NODE_EXISTS:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::INTERNAL_ERROR:
	case network::SERVICE_EXISTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_rem_scal_service(ssm_group_t *g_name, ssm_service_t *ssm_svc)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::ret_value_t	ret_value;
	network::service_sap_t	sap;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	if ((ret = ssm_service_to_sap(ssm_svc, sap)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Tell the PDT server to remove the service from the specified
	// group.
	//
	ret_value = pdt_server->rem_scal_service(group, sap, e);
	if (e.exception()) {
		e.exception()->print_exception("ssm_rem_scal_service");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	switch (ret_value) {
	case network::INVALID_SERVICE_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::NOSUCH_SERVICE:
		return (SCHA_SSM_NOSUCH_SERVICE);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::NOSUCH_GROUP:
	case network::INTERNAL_ERROR:
	case network::GROUP_EXISTS:
	case network::SERVICE_EXISTS:
	case network::NODE_EXISTS:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_get_scal_services(ssm_group_t *g_name, ssm_service_t **ret_svc,
	int *ret_cnt)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::grpinfoseq_t_var ginfo;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Tell the PDT server to return list of services
	//
	pdt_server->gns_service(ginfo, group, e);

	if (e.exception()) {
		e.exception()->print_exception("ssm_get_scal_service");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	*ret_svc = NULL;
	*ret_cnt = 0;

	//
	// Since a group name is passed to the PDT server, we should only
	// get one item back
	//
	if (ginfo.length() == 1) {
		//
		// Make sure we got the right group
		//
		ASSERT(strcmp(g_name->resource,
		    ginfo[(unsigned)0].srv_group.resource) == 0);

		network::srvinfoseq_t& sinfo = ginfo[(unsigned)0].srv_obj;
		ssm_service_t *ssm_svcp;

		if (sinfo.length() == 0)
			return (SCHA_SSM_SUCCESS);

		//
		// Build the array of ssm_service_t
		//
		ssm_svcp = (ssm_service_t *)malloc(sinfo.length() *
		    sizeof (ssm_service_t));

		if (ssm_svcp == NULL)
			return (SCHA_SSM_NO_MEMORY);

		for (uint_t i = 0; i < sinfo.length(); i++) {
			ssm_svcp[i].protocol = sinfo[i].srv_sap.protocol;
			ssm_svcp[i].port = sinfo[i].srv_sap.lport;
			bcopy(&sinfo[i].srv_sap.laddr, &ssm_svcp[i].ipaddr,
			    sizeof (ssm_svcp[i].ipaddr));
		}

		*ret_svc = ssm_svcp;
		*ret_cnt = (int)sinfo.length();

		return (SCHA_SSM_SUCCESS);
	}

	return (SCHA_SSM_INVALID_SERVICE_GROUP);
}

scha_ssm_exceptions
ssm_config_sticky_srv_grp(ssm_group_t *g_name, int sticky_timeout,
	boolean_t sticky_udp, boolean_t sticky_weak,
	boolean_t sticky_generic)
{
	Environment			e;
	network::PDTServer_ptr		pdt_server;
	network::group_t		group;
	network::sticky_config_t	st;
	network::ret_value_t		ret_value;
	scha_ssm_exceptions		ret;

	//
	// Check if the timeout value is in range. Since rgm checks it
	// before calling us, we should always be fine. But just in case.
	//
	if (sticky_timeout < SSM_MIN_STICKY_TIMEOUT)
		return (SCHA_SSM_INVALID_STICKY_TIMEOUT);

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Setup the sticky config structure
	//
	st.st_timeout = sticky_timeout;
	st.st_flags = ((sticky_udp? network::ST_UDP: 0) |
	    (sticky_weak? network::ST_WEAK: 0) |
	    (sticky_generic? network::ST_GENERIC:0));

	//
	// Tell the PDT server to remove the service from the specified
	// group.
	//
	ret_value = pdt_server->config_sticky_srv_grp(group, st, e);
	if (e.exception()) {
		e.exception()->print_exception("ssm_rem_scal_service");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	switch (ret_value) {
	case network::NOSUCH_GROUP:
	case network::INVALID_SERVICE_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::NOSUCH_SERVICE:
	case network::INTERNAL_ERROR:
	case network::GROUP_EXISTS:
	case network::SERVICE_EXISTS:
	case network::NODE_EXISTS:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_config_rr_srv_grp(ssm_group_t *g_name, boolean_t rr_enabled,
int conn_threshold)
{
	Environment			e;
	network::PDTServer_ptr		pdt_server;
	network::group_t		group;
	network::rr_config_t		rr;
	network::ret_value_t		ret_value;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Setup the RR config structure
	//
	rr.rr_flags = rr_enabled ? network::LB_RR : 0;
	rr.rr_conn_threshold = conn_threshold;

	//
	// Tell the PDT server to configure RR load balancing property
	// for the service group.
	//
	ret_value = pdt_server->config_rr_srv_grp(group, rr, e);
	if (e.exception()) {
		e.exception()->print_exception("ssm_rem_scal_service");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	switch (ret_value) {
	case network::NOSUCH_GROUP:
	case network::INVALID_SERVICE_GROUP:
		return (SCHA_SSM_INVALID_SERVICE_GROUP);
	case network::SUCCESS:
		return (SCHA_SSM_SUCCESS);
	case network::NOSUCH_SERVICE:
	case network::INTERNAL_ERROR:
	case network::GROUP_EXISTS:
	case network::SERVICE_EXISTS:
	case network::NODE_EXISTS:
	case network::NO_NODEID:
	case network::INVALID_PROTOCOL:
	case network::SERVICE_CONFLICTS:
	default:
		return (SCHA_SSM_INTERNAL_ERROR);
	}
}

scha_ssm_exceptions
ssm_is_scalable_service(ssm_service_t *ssm_svc, int *result)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::service_sap_t	sap;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_service_to_sap(ssm_svc, sap)) != SCHA_SSM_SUCCESS)
		return (ret);

	*result = pdt_server->is_scal_service(sap, e);

	if (e.exception()) {
		//
		// exception means a system error
		//
		e.exception()->print_exception("ssm_is_scalable_service");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_is_scalable_service_group(ssm_group_t *g_name, int *result)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	*result = pdt_server->is_scal_service_group(group, e);

	if (e.exception()) {
		//
		// exception means a system error
		//
		e.exception()->print_exception("ssm_is_scalable_service_group");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_set_primary_gifnode(ssm_service_t *ssm_svc, nodeid_t node)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::service_sap_t	sap;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Check if the nodeid is valid. If it less than 0 or greater
	// than NODEID_MAX return SCHA_SSM_INVALID_NODEID.
	//
	if (node == 0 || node > NODEID_MAX) {
		return (SCHA_SSM_INVALID_NODEID);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_service_to_sap(ssm_svc, sap)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Inform the PDT server to set this node as the
	// primary gif node
	//
	pdt_server->set_primary_gifnode(sap, node, e);

	if (e.exception()) {
		//
		// Extract any user exceptions first.  We map ENOENT
		// here to SCHA_SSM_NO_SERVICE so that the RGM can distinguish
		// between a system error, and simply an attempt to
		// attach an IP address that has no associated service.
		//
		// (see bugid: 4203412)
		//
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			switch (ex_op->error) {
			case ENOENT:
				e.clear();
				return (SCHA_SSM_NO_SERVICE);
			default:
				break;
			}
		}
		e.exception()->print_exception("ssm_set_primary_gifnode");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_attach_gifnode(ssm_group_t *g_name, ssm_service_t *ssm_svc, nodeid_t node)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::service_sap_t	sap;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Check if the nodeid is valid. If it less than 0 or greater
	// than NODEID_MAX return SCHA_SSM_INVALID_NODEID.
	//
	if (node == 0 || node > NODEID_MAX) {
		return (SCHA_SSM_INVALID_NODEID);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	if ((ret = ssm_service_to_sap(ssm_svc, sap)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Inform the PDT server to set this node as the
	// primary gif node
	//
	pdt_server->attach_gifnode(sap, node, e);

	if (e.exception()) {
		//
		// Extract any user exceptions first.  We map ENOENT
		// here to SCHA_SSM_NO_SERVICE so that the RGM can distinguish
		// between a system error, and simply an attempt to
		// attach an IP address that has no associated service.
		//
		// (see bugid: 4203412)
		//
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			switch (ex_op->error) {
			case ENOENT:
				e.clear();
				return (SCHA_SSM_NO_SERVICE);
			default:
				break;
			}
		}
		e.exception()->print_exception("ssm_attach_gifnode");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_detach_gifnode(ssm_group_t *g_name, ssm_service_t *ssm_svc, nodeid_t node)
{
	Environment		e;
	network::PDTServer_ptr	pdt_server;
	network::group_t	group;
	network::service_sap_t	sap;
	scha_ssm_exceptions		ret;

	//
	// Get the PDT server.
	//
	pdt_server = ssm_initialize();
	if (CORBA::is_nil(pdt_server)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}

	//
	// Check if the nodeid is valid. If it less than 0 or greater
	// than NODEID_MAX return SCHA_SSM_INVALID_NODEID.
	//
	if (node == 0 || node > NODEID_MAX) {
		return (SCHA_SSM_INVALID_NODEID);
	}

	//
	// Convert arguments.
	//
	if ((ret = ssm_group_to_group(g_name, group)) != SCHA_SSM_SUCCESS)
		return (ret);

	if ((ret = ssm_service_to_sap(ssm_svc, sap)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Inform the PDT server to set this node as the
	// primary gif node
	//
	pdt_server->detach_gifnode(sap, node, e);

	if (e.exception()) {
		//
		// Extract any user exceptions first.  We map ENOENT
		// here to SCHA_SSM_NO_SERVICE so that the RGM can distinguish
		// between a system error, and simply an attempt to
		// attach an IP address that has no associated service.
		//
		// (see bugid: 4203412)
		//
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			switch (ex_op->error) {
			case ENOENT:
				e.clear();
				return (SCHA_SSM_NO_SERVICE);
			default:
				break;
			}
		}
		e.exception()->print_exception("ssm_detach_gifnode");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_are_weights_set(ssm_group_t *g_name, boolean_t *result) {

	Environment		e;
	network::PDTServer_ptr pdts = NULL;
	network::group_t	gname;
	scha_ssm_exceptions	ret;

	if ((ret = ssm_group_to_group(g_name, gname)) != SCHA_SSM_SUCCESS)
		return (ret);

	//
	// Get the PDT server.
	//
	pdts = ssm_initialize();
	if (CORBA::is_nil(pdts)) {
		return (SCHA_SSM_MOD_NOTLOADED);
	}
	network::lbobj_i_ptr lbobj = pdts->get_lbobj(gname, e);
	if (e.exception()) {
		e.exception()->print_exception("ssm_are_weights_set()");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}
	if (CORBA::is_nil(lbobj)) {
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	network::dist_info_t_var dist_infov =
	    lbobj->get_distribution(e);
	if (e.exception()) {
		e.exception()->print_exception("ssm_are_weights_set()");
		e.clear();
		return (SCHA_SSM_INTERNAL_ERROR);
	}

	*result = (boolean_t)dist_infov->initialized;
	return (SCHA_SSM_SUCCESS);
}


//
// Whenever new error codes are defined or existing ones are changed
// in scha_ssm.h, this routine must be updated to ensure that the
// messages defined are consistent.
//
const char *
ssm_err_code_to_mesg(scha_ssm_exceptions ssm_err_code)
{
	switch (ssm_err_code) {
	case SCHA_SSM_NO_SERVICE:
		return ("no scalable service was specified");
	case SCHA_SSM_INVALID_PROTOCOL:
		return ("protocol was neither UDP nor TCP");
	case SCHA_SSM_SUCCESS:
		return ("request completed successfully");
	case SCHA_SSM_NO_GROUP:
		return ("no scalable services networking"
		    " group was specified");
	case SCHA_SSM_INVALID_POLICY:
		return ("load balancing policy was invalid");
	case SCHA_SSM_INTERNAL_ERROR:
		return ("internal cluster networking"
		    " error occurred");
	case SCHA_SSM_GROUP_EXISTS:
		return ("scalable services networking group"
		    " already exists");
	case SCHA_SSM_INVALID_SERVICE_GROUP:
		return ("scalable services networking group"
		    " does not exist");
	case SCHA_SSM_SERVICE_CONFLICTS:
		return ("scalable service conflicts with a "
		    "service in another group");
	case SCHA_SSM_INVALID_NODEID:
		return ("node id was invalid");
	case SCHA_SSM_NO_NODEID:
		return ("node does not exist in the scalable"
		    " service's configuration list");
	case SCHA_SSM_NOSUCH_SERVICE:
		return ("scalable service does not exist");
	case SCHA_SSM_INVALID_ADDRESS:
		return ("IP address was invalid");
	case SCHA_SSM_INVALID_PORTNUM:
		return ("port number was invalid");
	case SCHA_SSM_NO_LBSTRING:
		return ("load balancing weights were invalid");
	case SCHA_SSM_NODE_EXISTS:
		return ("node already exists in the scalable"
		    " service's configuration list");
	case SCHA_SSM_NO_MEMORY:
		return ("not enough memory");
	case SCHA_SSM_CANNOT_ADD_NODEID:
		return ("unexpected error while adding node id");
	case SCHA_SSM_COMM_ERROR:
		return ("unexpected communication error occurred");
	case SCHA_SSM_SEQ_ERROR:
		return ("unexpected sequence error occurred");
	case SCHA_SSM_MOD_NOTLOADED:
		return ("scalable services infrastructure has not been "
		    "initialized");
	case SCHA_SSM_SERVICE_EXISTS:
		return ("scalable service exists in the group");
	case SCHA_SSM_INVALID_STICKY_TIMEOUT:
		return ("invalid client affinity timeout value");
	default:
		return ("unknown error occurred");
	}
}

} // end extern "C"
