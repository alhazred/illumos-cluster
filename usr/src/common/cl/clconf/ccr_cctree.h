/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CCR_CCTREE_H
#define	_CCR_CCTREE_H

#pragma ident	"@(#)ccr_cctree.h	1.32	08/08/01 SMI"

#include <sys/list_def.h>
#include <sys/os.h>
#include <orb/debug/haci_debug.h>

#if defined(HACI_DBGBUFS) && !defined(NO_CLCONF_DBGBUF)
#define	CLCONF_DBGBUF
#endif

#ifdef	CLCONF_DBGBUF
extern dbg_print_buf clconf_dbg;
#define	CLCONF_DBG(args) HACI_DEBUG(ENABLE_CLCONF_DBG, clconf_dbg, args)
#else
#define	CLCONF_DBG(args)
#endif

// forward declaration.
class ccr_node;

// The iterator we use to iterate throught clconf nodes.
// CSTYLED
typedef IntrList<ccr_node, _SList>::ListIterator clconfiter;

//
// The word "node" in this file means a node of the tree - it has
// nothing to do with cluster nodes...
//

//
//
// This class is designed to produce a tree-structured hierarchy using
// '.' separated node names. The intent is to add structure to the CCR
// tables in such a fashion that they are extensible in future releases
// w/o invalidating previous readers of the tables.
//

//
// Note that this class doesn't attempt to directly read the CCR; we'll
// add some routines to do that with the read and write functions...
//


class ccr_node : public _SList::ListElem {
public:
	//
	// [Hint: static member ccr_node::separator
	// must be defined in the program]
	//
	static const char separator = '.';	// The separator between names.

	// Traverse the tree starting from this. Calls callback and passes it
	// the current node pointer and the user specified arg. It stops when
	// it is done with the tree or when the callback returns zero.
	void traverse(int (*callback)(ccr_node *, void *arg), void *arg);

	// does a deep copy of the node and it's children.
	ccr_node *copy_tree();

	// does a deep delete of the node and it's children
	void delete_tree();

	// Is it a leaf node?
	bool is_leaf();

	// returns an iterator to iterate through the children.
	clconfiter *get_iterator();

	// Reinit the iterator passed in.
	void init_iter(clconfiter *);

	// Set the children iterator to the first.
	void atfirst();

	// Return the number of children of this node.
	unsigned int get_num_children();

	// returns the current child of this node.
	ccr_node *get_current_child();

	// Advance the children iterator.
	void advance();

	// Set up the parent pointer. It may be overloaded if a child class
	// want to setup some more info based on the parent.
	virtual void set_parent(ccr_node *);

	// returns a child by relative name of this node. Returns
	// NULL if not found. The name doesn't contain this node (parent)
	// The name is just one level deep (doesn't contain seperators).
	ccr_node *get_named_child(const char *);

	// returns a child by relative name of this node. Create if
	// necessary. Returns NULL if the name is bad. The name doesn't
	// contain this node (parent).
	ccr_node *get_named_child_create(const char *);

	// Get a child by the relative name. Create child if not found.
	// The name given is one that starts from this (including this).
	// The name may contain seperators and as a result we may create
	// multiple children of different depth.
	ccr_node *get_child_by_name_create(const char *);

	// Same as get_child_by_name_create except that the name given
	// doesn't include this.
	ccr_node *get_child_by_name_create_ex(const char *);

	// Get a child by the relative name. Returns NULL if not found.
	// The name given is one that starts from this (include this).
	ccr_node *get_child_by_name(const char *);

	// returns the parent, NULL if this is root.
	ccr_node *get_parent();

	// returns the name of this node
	const char *get_name();

	// returns the full name of this node. The full name starts
	// from the root node and may contain seperator. For example,
	// "cluster.nodes.1" is the full name for node_1.
	char *get_full_name();

	// Returns the value stored in this node.
	const char *get_value();

	// Check if the specified name could be given to a child. It
	// checks for name conflicts. Returns true if the name is fine,
	// false if the name is illegal.
	bool check_name(const char *);

	// Adds a tree (w/ perhaps only one node) onto existing tree...
	// will fail if name of node is duplicated. Returns true for
	// success, false for fail.
	bool add(ccr_node *);

	// Removes the specified node (and any children) from the tree.
	// Returns true if the node is found in the tree. Returns false
	// if it's not in the tree. It just remove the node from the
	// tree. It won't try to delete any children.
	bool remove(ccr_node *);

	// Renames a node. Fails if this would produce duplicate
	// name with other siblings. Returns true for
	// success, false for fail.
	bool set_name(const char *);

	// sets the value of the specified node. Error if node
	// isn't a leaf node. Returns true for success, false for fail.
	bool set_value(const char *);

	// Create a duplicate of this node.
	virtual ccr_node *dup();

	// Give the root node a chance to do something special. When copying
	// a tree/branch, the parameter passed in is the corresponding
	// node on the tree being copied. When creating a new tree, the
	// parameter is NULL.
	virtual void initroot(ccr_node *);

	// Create a new node the specified name.
	virtual ccr_node *create_node(const char *);

	// Destructor. It will free name and value, and assert that
	// there is no children around.
	virtual ~ccr_node();

	// Checks whether the name contains illegal char. The first
	// parameter is the name, the second is the illegal char to
	// check for.
	static bool name_ok(const char *, char ch = ccr_node::separator);

	// Create a new node the specified name.
	ccr_node(const char *);

	// Print out a debug message.
#ifdef DEBUG
	static int print(ccr_node *, void *arg);
#endif

	// create a new node with the same name and value as in the one
	// passed in.
	ccr_node(ccr_node *);

	// helper function. Copy the next field (between separators) from ptr
	// to buf. Returns false when ptr reaches the end.
	static bool get_next_str(char *&ptr, char *buf,
	    char sep = ccr_node::separator);
#ifdef DEBUGGER_PRINT
	void get_nodes(char **, int &);
	uint_t get_count();
#endif

protected:
	IntrList<ccr_node, _SList> children;
	ccr_node *parent;

	// We copy the name and free it when the node is deleted.
	char *name;

	// The value of this node. It's NULL for non-leaf node.
	char *value;

	// Disable.
	ccr_node(const ccr_node &);
	ccr_node & operator = (const ccr_node &);
};

#endif	/* _CCR_CCTREE_H */
