//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clconf.cc	1.176	09/02/10 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/rsrc_tag.h>
#include <sys/time.h>
#include <clconf/clnode.h>
#include <clconf/clconf_ccr.h>
#include <clconf/clconf_file_io.h>
#include <cmm/cmm_ns.h>
#include <tm/user_tm.h>
#include <sys/rsrc_tag.h>
#include <h/ccr.h>
#include <h/component_state.h>		/* Query interface */
#include <h/repl_pxfs.h>
#include <sys/cl_assert.h>
#include <vm/versioned_protocol.h>
#include <sys/vm_util.h>
#include <sys/quorum_int.h>
#include <sys/cl_comm_support.h>

#ifdef _KERNEL
#include <netinet/in.h>
#include <orb/ip/ipconf.h>
#include <sys/cladm_debug.h>
#endif

#ifdef _KERNEL_ORB
#include <orb/transport/path_manager.h>
#else
#include <orb/infrastructure/orb.h>
#endif

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <h/membership.h>
#include <sys/vc_int.h>
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

//
// Common internal function used to get quorum status
//
static int cluster_get_quorum_status_common(
    quorum_status_t **, boolean_t get_immediate_status);

//
// Routines in clconf code try to query the current quorum status
// from the quorum algorithm object in kernel.
// While querying this status, it is possible that a CMM reconfiguration
// is in progress. In that scenario, the query (IDL method) raises
// a CORBA exception. On seeing this exception raised, the clconf code,
// that is querying the quorum status, sleeps for a certain period and
// then retries the query operation.
//
// This global variable is the sleep period used in such a case.
// The default value of this sleep period is 2 seconds.
// It can be changed for :
// (i) kernel clconf using mdb or /etc/system
// (ii) userland clconf using mdb
// On the next query, the clconf code will pick up the changed value.
//
unsigned int clconf_sleep_period_for_quorum_status_query = 2; // seconds

clconf_errnum_t (*clconf_exec_program_with_opts_ptr)(const char *program,
    nodeid_t id, boolean_t *clust_memb, boolean_t realtime);

#ifndef	linux
#define	in_addr_solaris	in_addr
#endif

/*
 * IMPORTANT : Part of this file is currently duplicated in
 * usr/src/common/cl/mdbmacros/clconf/mdb_clconf.cc. Make sure that
 * any changes to this file are also changed in that file if applicable.
 */

/* The endpoint fo a path */
typedef struct clconf_endpoint {
	nodeid_t nodeid;
	int adapterid;
	int portid;
} clconf_endpoint_t;

/* The path object */
/* end[0] is Local and end[1] is Remote. Use enum clconf_endtype_t as index */
struct clconf_path {
	clconf_endpoint_t end[2];
	uint_t quantum;
	uint_t timeout;
};

nodeid_t
clconf_get_local_nodeid()
{
	return (orb_conf::local_nodeid());
}

//
// clconf_get_clustername
//
// Returns clustername for the cluster.
// Caller needs to release the return string
//

char *
clconf_get_clustername()
{
	clconf_cluster_t *cl = clconf_cluster_get_current();

	const char *nm = clconf_obj_get_name((clconf_obj_t *)cl);

	if (nm != NULL) {
		char *clustername = os::strdup(nm);
		clconf_obj_release((clconf_obj_t *)cl);
		return (clustername);
	} else {
		clconf_obj_release((clconf_obj_t *)cl);
		return (NULL);
	}
}

//
// clconf_get_nodename
//
//  Returns nodename for the given nodeid.
//  Space for nodename must be allocated by the client.
//  Call this function if you want one that does not blocking
//  memory allocation calls.
//
void
clconf_get_nodename(nodeid_t nodeid, char *nodename)
{
	const char *nm = NULL;

	CL_PANIC((nodeid >= 1) && (nodeid <= NODEID_MAX));

	clconf_cluster_t *cl = clconf_cluster_get_current();

	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl, CL_NODE,
	    (int)nodeid);

	nm = clconf_obj_get_name((clconf_obj_t *)nd);

	if (nm != NULL) {
		(void) strcpy(nodename, nm);
	} else {
		nodename[0] = '\0';
	}

	clconf_obj_release((clconf_obj_t *)cl);
}


/* Private helper function. Convert a group name to an obj type */
clconf_objtype_t
clconf_name_to_objtype(const char *name)
{
	if (os::strcmp(name, "cluster") == 0) {
		return (CL_CLUSTER);
	} else if (os::strcmp(name, "nodes") == 0) {
		return (CL_NODE);
	} else if (os::strcmp(name, "adapters") == 0) {
		return (CL_ADAPTER);
	} else if (os::strcmp(name, "ports") == 0) {
		return (CL_PORT);
	} else if (os::strcmp(name, "blackboxes") == 0) {
		return (CL_BLACKBOX);
	} else if (os::strcmp(name, "cables") == 0) {
		return (CL_CABLE);
	} else if (os::strcmp(name, "quorum_devices") == 0) {
		return (CL_QUORUM_DEVICE);
	} else {
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		name, NULL);
		//
		// SCMSGS
		// @explanation
		// An invalid group name has been encountered while converting
		// a group name to clconf_obj type. Valid group names are
		// "cluster", "nodes", "adapters", "ports", "blackboxes",
		// "cables", and "quorum_devices".
		// @user_action
		// This is an unrecoverable error, and the cluster needs to be
		// rebooted. Also contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "clconf: Invalid group name");
	}
	return (CL_CLUSTER);
}

/* helper function. Convert objtype to group name */
void
clconf_objtype_to_name(clconf_objtype_t type, char *name)
{
	switch (type) {
	case CL_CLUSTER:
		(void) os::strcpy(name, "cluster");
		break;
	case CL_NODE:
		(void) os::strcpy(name, "nodes");
		break;
	case CL_ADAPTER:
		(void) os::strcpy(name, "adapters");
		break;
	case CL_PORT:
		(void) os::strcpy(name, "ports");
		break;
	case CL_BLACKBOX:
		(void) os::strcpy(name, "blackboxes");
		break;
	case CL_CABLE:
		(void) os::strcpy(name, "cables");
		break;
	case CL_QUORUM_DEVICE:
		(void) os::strcpy(name, "quorum_devices");
		break;
	default:
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		    name, NULL);
		//
		// SCMSGS
		// @explanation
		// An invalid clconf_obj type has been encountered while
		// converting an clconf_obj type to group name. Valid objtypes
		// are "CL_CLUSTER", "CL_NODE", "CL_ADAPTER", "CL_PORT",
		// "CL_BLACKBOX", "CL_CABLE", "CL_QUORUM_DEVICE".
		// @user_action
		// This is an unrecoverable error, and the cluster needs to be
		// rebooted. Also contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "clconf: Invalid clconf_obj type");
	}
}

/* initialize a static iterator */
clconf_iter_t *
clconf_siter_init(clconf_siter_t *siter, clconf_obj_t *obj,
    clconf_objtype_t childtype)
{
	// Makesure that our static iterator is big enough to contain the
	// real thing.
	ASSERT(sizeof (clconf_siter_t) >= sizeof (clconfiter));
	siter->ptr = NULL;
	clconfiter *iter = (clconfiter *)siter;

	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	char grpname[64];
	clconf_objtype_to_name(childtype, grpname);
	ccr_node *cr = node->get_named_child(grpname);
	if (cr == NULL) {
		return (NULL);
	}
	cr->init_iter(iter);
	return ((clconf_iter_t *)iter);
}

/* Advance the iterator */
void
clconf_iter_advance(clconf_iter_t *iter)
{
	ASSERT(iter != NULL);
	((clconfiter *)iter)->advance();
}

/* Get the current element, returns NULL when there is no more elements */
clconf_obj_t *
clconf_iter_get_current(clconf_iter_t *iter)
{
	if (iter == NULL) {
		return (NULL);
	}
	return ((clconf_obj_t *)((clconfiter *)iter)->get_current());
}

/* Get the number of elements in the iterator */
int
clconf_iter_get_count(clconf_iter_t *iter)
{
	if (iter == NULL) {
		return (0);
	}

	_SList::ListElem *cr;
	int count;

	cr = ((clconfiter *)iter)->get_current();
	for (count = 0; cr != NULL; cr = cr->next()) {
		count++;
	}
	return (count);
}

/* Release the iterator */
void
clconf_iter_release(clconf_iter_t *iter)
{
	delete ((clconfiter *)iter);
}

/* Helper function */
clconf_iter_t *
clconf_obj_get_iter(clconf_obj_t *obj, const char *grpname)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *cr = node->get_named_child(grpname);
	if (cr == NULL) {
		return (NULL);
	}
	return ((clconf_iter_t *)cr->get_iterator());
}

/*
 * Get the object's id as string. Every object has an id that distinguish
 * it from its siblings of the same type. It returns NULL if the id is not set.
 */
const char *
clconf_obj_get_idstring(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	return (((clnode *)obj)->get_name());
}

/*
 * Get the object's id. Every object has an id that distinguish it from
 * its siblings of the same type. It returns -1 if the id is not set.
 */
int
clconf_obj_get_id(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	return (os::atoi(((clnode *)obj)->get_name()));
}

/*
 * Set the object's id. Returns CL_INVALID_ID if the id is conflict with others
 */
clconf_errnum_t
clconf_obj_set_id(clconf_obj_t *obj, int id)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	char str[32];
	(void) os::itoa(id, str);
	if (!node->set_name(str)) {
		return (CL_INVALID_ID);
	}
	return (CL_NOERROR);
}

/*
 * Get the object's type.
 */
clconf_objtype_t
clconf_obj_get_objtype(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	if (node->get_parent() == NULL) {
		return (CL_CLUSTER);
	}
	return (clconf_name_to_objtype(node->get_parent()->get_name()));
}

// Callback to set the states.
int
clconf_obj_state_callback(ccr_node *c, void *state)
{
	// If the name is a number, it means we are on a node instead
	// of a group. We can set the state then.
	const char *nm = c->get_name();
	uint_t len = (uint_t)os::strlen(nm);
	for (uint_t i = 0; i < len; i++) {
		if (nm[i] < '0' || nm[i] > '9') {
			// not a number.
			return (1);
		}
	}

	ccr_node *st = c->get_named_child_create("state");
	ASSERT(st != NULL);
	(void) st->set_value((char *)state);

	return (1);
}

/*
 * Returns state of object. Returns NULL if state was never set.
 */
const char *
clconf_obj_get_state(clconf_obj_t *obj)
{
	if (obj == NULL) {
		return (NULL);
	}
	ccr_node *node = (ccr_node *)obj;
	node = node->get_named_child("state");
	if (node == NULL) {
		return (NULL);
	}
	return (node->get_value());
}

/*
 * Enable the clconf object.
 */
clconf_errnum_t
clconf_obj_enable(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *prt = node->get_parent();
	if (prt != NULL && !clconf_obj_enabled((clconf_obj_t *)
	    prt->get_parent())) {
		// The parent is disabled, fail.
		return (CL_PARENT_DISABLED);
	}
	(void) node->get_named_child_create("state")->set_value("enabled");
	return (CL_NOERROR);
}

/*
 * Disable the clconf object. This function disables the object
 * regardless of the state of either its parents or children.
 * The children (if any) will be left in the same state so that when
 * this is re-enabled, the state is preserved.
 */
clconf_errnum_t
clconf_obj_disable(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	(void) node->get_named_child_create("state")->set_value("disabled");
	return (CL_NOERROR);
}

/*
 * Whether this object is enabled. Returns 1 if true 0 if false.
 */
int
clconf_obj_enabled(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *st = node->get_named_child("state");
	if (st != NULL) {
		const char *v = st->get_value();
		if (os::strcmp(v, "disabled") == 0) {
			return (0);
		} else if (os::strcmp(v, "enabled") == 0) {
			return (1);
		} else
			return (0);	// Any other state is disabled!
	}
	// Default is enabled.
	return (1);
}

/*
 * Get the child with the specified type and id. Returns NULL if not found.
 */
clconf_obj_t *
clconf_obj_get_child(clconf_obj_t *obj, clconf_objtype_t type, int id)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	char tyname[20];
	clconf_objtype_to_name(type, tyname);

	char idstr[32];
	(void) os::itoa(id, idstr);

	ccr_node *child = node->get_named_child(tyname);
	if (child == NULL) {
		return (NULL);
	}
	child = child->get_named_child(idstr);
	return ((clconf_obj_t *)child);
}

/*
 * Get the parent. Returns NULL if used on a clconf_cluster.
 */
clconf_obj_t *
clconf_obj_get_parent(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clconf_objtype_t objtype = clconf_obj_get_objtype(obj);
	CL_PANIC(objtype != CL_UNKNOWN_OBJ);
	if (objtype == CL_CLUSTER) {
		return (NULL);
	}
	clnode *node = (clnode *)obj;
	return ((clconf_obj_t *)node->get_parent()->get_parent());
}

/*
 * Get the specified property. Returns NULL if no such property.
 */
const char *
clconf_obj_get_property(clconf_obj_t *obj, const char *property_name)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *cr = node->get_named_child("properties");
	if (cr == NULL) {
		return (NULL);
	}
	ccr_node *prpt = cr->get_named_child(property_name);
	if (prpt == NULL) {
		return (NULL);
	}
	return (prpt->get_value());
}

/*
 * Returns the properties this object has. Returns NULL if it doesn't have
 * any properties.
 */
clconf_iter_t *
clconf_obj_get_properties(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "properties"));
}

/*
 * Set the specified property. Create the property if it doesn't exist.
 * If property_value is NULL we will delete the property.
 * Returns CL_BADNAME if the property name is invalid.
 */
clconf_errnum_t
clconf_obj_set_property(clconf_obj_t *obj, const char *property_name,
    const char *property_value)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	if (property_value != NULL) {
		// setting a property.
		ccr_node *prts = node->get_named_child_create("properties");
		ASSERT(prts != NULL);
		ccr_node *prt = prts->get_named_child_create(property_name);
		if (prt == NULL) {
			return (CL_BADNAME);
		}
		(void) prt->set_value(property_value);
	} else {
		// removing a property.
		ccr_node *prts = NULL;
		ccr_node *prt = NULL;
		prts = node->get_named_child("properties");
		if (prts != NULL) {
			prt = prts->get_named_child(property_name);
		}
		if (prt != NULL) {
			(void) prts->remove(prt);
		}
	}
	return (CL_NOERROR);
}

/*
 * Returns name of object. Returns NULL if name was never set.
 */
const char *
clconf_obj_get_name(clconf_obj_t *obj)
{
	if (obj == NULL) {
		return (NULL);
	}
	ccr_node *node = (ccr_node *)obj;
	node = node->get_named_child("name");
	if (node == NULL) {
		return (NULL);
	}
	return (node->get_value());
}

/*
 * Set the name of the object.
 */
void
clconf_obj_set_name(clconf_obj_t *obj, const char *name)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	(void) node->get_named_child_create("name")->set_value(name);
}

/*
 * Returns the internal name of object. The internal name is the key string
 * used in CCR to identify this object. For example, "cluster.nodes.1"
 * is the internal name for node 1.
 */
char *
clconf_obj_get_int_name(clconf_obj_t *obj)
{
	return (((clnode *)obj)->get_full_name());
}

/*
 * Releases a string referenced by name.  The name is typically
 * the one obtained through clconf_obj_get_int_name() but can also be
 * any memory allocated with new.
 */
void
clconf_free_string(char *name)
{
	delete [] name;
}

/*
 * checks the object to make sure it's properly filled out,
 * but doesn't check relationships to other objects. Returns
 * CL_INVALID_OBJ if the check fails and returns the detailed
 * info in the errorbuff.
 */
clconf_errnum_t
clconf_obj_check(clconf_obj_t *, char *, uint_t)
{
	/* Call other module to do the checking XXX */
	return (CL_NOERROR);
}

/*
 * Every clconf tree has a hold count. The tree will be deleted only when the
 * hold count reaches zero.
 */

/*
 * Obtain a hold on the tree. This garantees the tree doesn't go away.
 * When the receiver is done, it should do a release.
 */
void
clconf_obj_hold(clconf_obj_t *obj)
{
	((clnode *)obj)->hold();
}

/*
 * Release the hold we have on the object.
 */
void
clconf_obj_release(clconf_obj_t *obj)
{
	((clnode *)obj)->rele();
}

/*
 * deep copy an existing object and return with hold count = 1
 */
clconf_obj_t *
clconf_obj_deep_copy(clconf_obj_t *obj)
{
	clconf_obj_hold(obj);
	clnode *node = (clnode *)obj;
	// copy_tree will construct a new tree with a seperate clrefcnt
	// object that has count 1.
	clnode *new_tree = (clnode *)node->copy_tree();
	clconf_obj_release(obj);
	return ((clconf_obj_t *)new_tree);
}

/*
 * delete the object along with all its children.
 */
void
clconf_obj_deep_delete(clconf_obj_t *obj)
{
	((clnode *)obj)->delete_tree();
}

/*
 * Returns the incarnation number for the tree this object is in.
 */
uint_t
clconf_obj_get_incarnation(clconf_obj_t *obj)
{
	clnode *nd = (clnode *)obj;
	return (nd->get_incn());
}

/*
 * ***********************************************************************
 * Interfaces with a cluster object.
 * ***********************************************************************
 */

// Gets the name of the cluster the clconf code is running in
extern char *get_clname_for_clconf();

/*
 * Get the current cluster object.
 */
clconf_cluster_t *
clconf_cluster_get_current()
{
#ifdef _KERNEL_ORB
	/*
	 * Here we need to wait for ORB::initialize
	 * to finish initialization and construction
	 * of clconf.
	 */
	while (cl_wait_until_orb_is_up_funcp ==
	    NULL) {
		// Sleep for 1 second
		os::usecsleep((os::usec_t)MICROSEC);
	}
	cl_wait_until_orb_is_up_funcp();
#endif	// _KERNEL_ORB
	clconf_cluster_t *clp =
	    (clconf_cluster_t *)cl_current_tree::the().get_root();
	ASSERT(clp != NULL);
	return (clp);
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
/*
 * Get the cluster object for a virtual cluster.
 * Can be used in global zone only.
 * Returns NULL if called in non-global zone.
 */
clconf_cluster_t *
clconf_cluster_get_vc_current(const char *cl_namep)
{
#ifndef	_KERNEL_ORB
	if (getzoneid() == GLOBAL_ZONEID) {
		//
		// In global zone user mode, check for updates to
		// clconf trees for zone clusters.
		// Zone clusters could have been added/removed;
		// or infrastructure table of a zone cluster could have changed.
		// This function refreshes all the zone cluster clconf trees.
		//
		if (clconf_ccr::the().check_zone_cluster_list() != 0) {
			//
			// The clconf trees for the zone clusters could be
			// stale. So return NULL.
			//
			return (NULL);
		}
	}
#endif	// !_KERNEL_ORB

	clconf_cluster_t *clp =
	    (clconf_cluster_t *)cl_current_tree::the().
	    cl_tree(cl_namep).get_root();
	return (clp);
}
#endif

#ifdef _KERNEL_ORB
/*
 * Get the old cluster object.
 */
clconf_cluster_t *
clconf_cluster_get_old()
{
	clconf_cluster_t *cl =
	    (clconf_cluster_t *)cl_current_tree::the().get_old_root();
	ASSERT(cl != NULL);
	return (cl);
}

void
clconf_cluster_set_oldroot_to_root()
{
	cl_current_tree::the().set_oldroot_to_root();
}
#endif

/*
 * Create an empty cluster object.
 */
clconf_cluster_t *
clconf_cluster_create()
{
	return ((clconf_cluster_t *)new clnode("-1"));
}

/*
 * Determines if transition is feasible. Returns an error code indicating
 * why the transition is not feasible and the detailed info in errorbuf.
 * The error codes are:
 * CL_BUF_TOO_SMALL, the errorbuf is not big enough to contain the message.
 * CL_INVALID_OBJ, the proposed clconf_cluster contains invalid objects.
 * CL_INVALID_TREE, the proposed cluster is not valid.
 * (Called from user land or in UNODE mode.)
 */
#ifndef _KERNEL
clconf_errnum_t
clconf_cluster_check_transition(clconf_cluster_t *, clconf_cluster_t *proposed,
    char *errorbuf, uint_t len, boolean_t *cmm_reconfig_needed)
{
	clconf_errnum_t error = CL_NOERROR;

	//
	// If the caller is interested in finding out if a cmm reconf
	// will be needed, then initialize the corresp flag to false.
	//
	if (cmm_reconfig_needed != NULL)
		*cmm_reconfig_needed = B_FALSE;

	/*
	 * tm_ic_verify() can return a collection of errors in the
	 * error code by using bit masks in the return value.
	 *		XXXTM - Need to beef this up
	 * TM Library returns several error codes.
	 */
	int		ret;
	if ((ret = user_tm::the().verify(proposed, errorbuf, len)) != 0) {
		if (ret & (TM_ERR_EDED | TM_ERR_EDTR | TM_ERR_EDPT)) {
			error = CL_INVALID_CABLE_ENDPOINT;
		} else if (ret & TM_ERR_BUFF) {
			error = CL_BUF_TOO_SMALL;
		} else {
			error = CL_INVALID_TREE;
		}
	}

	if (error == CL_NOERROR) {
		/*
		 * Call the quorum algorithm's check routine since the
		 * quorum configuration may have changed.
		 */
		quorum_table_t				*qt;
		quorum::node_config_list_t		nodelist;
		quorum::quorum_device_config_list_t	devicelist;
		quorum::quorum_config_error_t		q_error;
		quorum::quorum_algorithm_var		qm_v;
		Environment 				e;
		uint_t					i;

		//
		// Construct the proposed nodelist and devicelist by first
		// generating the proposed quorum table.
		//
		if (clconf_get_quorum_table(proposed, &qt) != 0)
			return (CL_INVALID_TREE);

		nodelist.length(qt->nodes.listlen);
		devicelist.length(qt->quorum_devices.listlen);
		for (i = 0; i < qt->nodes.listlen; i++) {
			nodelist[i].nid = qt->nodes.list[i].nodenum;
			for (uint_t j = 0; j < MHIOC_RESV_KEY_SIZE; j++)
				nodelist[i].reservation_key.key[j] =
				    qt->nodes.list[i].reservation_key.key[j];
			nodelist[i].votes_configured = qt->nodes.list[i].votes;
		}
		for (i = 0; i < qt->quorum_devices.listlen; i++) {
			devicelist[i].qid = qt->quorum_devices.list[i].qid;
			devicelist[i].votes_configured =
				qt->quorum_devices.list[i].votes;
			devicelist[i].gdevname = os::strdup((const char *)
				qt->quorum_devices.list[i].gdevname);
			devicelist[i].nodes_with_enabled_paths =
				qt->quorum_devices.list[i].
				nodes_with_enabled_paths;
			devicelist[i].nodes_with_configured_paths =
				qt->quorum_devices.list[i].
				nodes_with_configured_paths;
		}
		clconf_release_quorum_table(qt);

		qm_v = cmm_ns::get_quorum_algorithm(NULL);
		q_error = qm_v->quorum_check_transition(nodelist,
		    devicelist, e);
		if (e.exception()) {
			error = CL_INVALID_TREE;
			e.clear();
		} else {
			switch (q_error) {
			case quorum::QUORUM_CONFIG_NO_CHANGE:
				error = CL_NOERROR;
				break;
			case quorum::QUORUM_CONFIG_SAFE_CHANGE:
				error = CL_NOERROR;
				if (cmm_reconfig_needed != NULL)
					*cmm_reconfig_needed = B_TRUE;
				break;
			case quorum::QUORUM_CONFIG_MULTIPLE_CHANGES:
				error = CL_QUORUM_CONFIG_MULTIPLE_CHANGES;
				break;
			case quorum::QUORUM_CONFIG_LARGE_CHANGE:
				error = CL_QUORUM_CONFIG_LARGE_CHANGE;
				break;
			case quorum::QUORUM_CONFIG_WILL_LOSE_QUORUM:
				error = CL_QUORUM_CONFIG_WILL_LOSE_QUORUM;
				break;
			case quorum::QUORUM_CONFIG_ILLEGAL:
				error = CL_QUORUM_CONFIG_ILLEGAL;
				break;
			default:
				error = CL_INVALID_TREE;
			}
		}
	}

	return (error);
}
#endif // _KERNEL

/*
 * Commit the proposed clconf tree to the CCR. Only does so when the proposed
 * tree is valid. Returns:
 * CL_BUF_TOO_SMALL, the errorbuf is not big enough to contain the message.
 * CL_TREE_OUTDATED, the clconf_cluster pointed by proposed is no longer
 *     current. This means the changes proposed are based on outdated
 *     information. The caller should sync up with the current clconf_cluster
 *     and redo the changes.
 * CL_INVALID_TREE, the proposed cluster is not valid.
 * CL_NODE_ISOLATED, if the proposed cluster results in node isolation.
 */
#ifndef _KERNEL
clconf_errnum_t
clconf_cluster_commit(clconf_cluster_t *proposed,
    char *errorbuf, uint_t len)
{
	boolean_t cmm_reconf_needed = B_FALSE;

	//
	// First obtain a transaction lock and verify whether the proposed
	// tree is based on the most current cluster tree.
	//
	if (!cl_current_tree::the().lock_ccr((clnode *)proposed)) {
		return (CL_TREE_OUTDATED);
	}

	//
	// Check the proposed tree from transport configuration and quorum
	// point of view.
	//
	clconf_errnum_t error = clconf_cluster_check_transition(NULL,
	    proposed, errorbuf, len, &cmm_reconf_needed);

	if (error != CL_NOERROR) {
		cl_current_tree::the().unlock_ccr();

		return (error);
	}

	//
	// Now perform sanity checks based on dynamic transport path status.
	// ic_update checks the dynamic path state and returns a non-zero
	// value if performing the update will result in removing the last
	// online path to any node. Otherwise it updates the IP addresses
	// etc for the new clconf tree.
	//
	if (user_tm::the().ic_update(proposed) != 0) {
		cl_current_tree::the().unlock_ccr();

		return (CL_NODE_ISOLATED);
	}

	//
	// Everything looks cool. Try to go ahead and do the write. Write_ccr
	// drops the CCR transaction lock obtained by the lock_ccr call above
	// irrespective of the success status. So there is no need for an
	// explicit unlock_ccr.
	//
	if (!cl_current_tree::the().write_ccr((clnode *)proposed)) {
		//
		// A failure here means an error was seen while creating
		// the ccr sequence or an exception occured while trying
		// to commit the transaction. Returning CL_TREE_OUTDATED
		// here with the hope that the operation will succeed when
		// the caller retries.
		//
		return (CL_TREE_OUTDATED);
	}

	//
	// Successful in writing to the CCR. If CMM reconfig was indicated
	// by the check transition function above, initiate it now.
	//
	if (cmm_reconf_needed) {
		clconf_cluster_reconfigure();
	}

	return (CL_NOERROR);
}
#endif

#ifndef _KERNEL
/*
 * This function is called by the non-cluster mode cli (and therefore
 * works on in non-cluster mode) used to modify the private IP address
 * range of the cluster. It writes to a new file (usually
 * /etc/cluster/ccr/global/infrastructure.new). This new file is then
 * used to overwrite the existing infrastructure file.
 */
int
clconf_modify_ip(char *filename, char *netnum, char *c_netmask,
    char *netmask, char *subnet_netmask, char *maxnodes,
    char *maxprivnets)
{
	clconf_file_io *infra;
	clconf_cluster_t *infra_tree;
	int err;

	infra = new clconf_file_io(filename);

	// Initialize clconf tree and read in the contents of the file
	if (!infra->initialize())
		return (1);

	// Initialize the topology manager
	if ((err = user_tm::initialize()) != 0)
		return (err);

	// Get pointer to current tree
	infra_tree = clconf_cluster_get_current();
	if (infra_tree == NULL)
		return (1);

	// Set the private_net_number, private_netmask and the
	// private_subnet_netmask properties of the cluster based on
	// the input. Also set the maxnodes and maxprivatenets
	// properties. All of these properties refer to the new
	// private IP address range.
	err = clconf_obj_set_property((clconf_obj_t *)infra_tree,
		"private_net_number", netnum);
	if (err != CL_NOERROR)
		return (err);
	err = clconf_obj_set_property((clconf_obj_t *)infra_tree,
		"cluster_netmask", c_netmask);
	if (err != CL_NOERROR)
		return (err);
	err = clconf_obj_set_property((clconf_obj_t *)infra_tree,
		"private_netmask", netmask);
	if (err != CL_NOERROR)
		return (err);
	err = clconf_obj_set_property((clconf_obj_t *)infra_tree,
		"private_subnet_netmask", subnet_netmask);
	if (err != CL_NOERROR)
		return (err);
	err = clconf_obj_set_property((clconf_obj_t *)infra_tree,
		"private_maxnodes", maxnodes);
	if (err != CL_NOERROR)
		return (err);
	err = clconf_obj_set_property((clconf_obj_t *)infra_tree,
		"private_maxprivnets", maxprivnets);
	if (err != CL_NOERROR)
		return (err);

	// Based on the new IP address range provided above, Update
	// the IP address assigned to the adapters using the topology
	// manager
	if ((err = user_tm::the().modify_ip_address_range(infra_tree)) != 0)
		return (err);

	// Once all the updates are made to the clconf tree in memory,
	// update the CCR with the modified clconf tree
	if ((err = clconf_cluster_write_nonclust_ccr(infra_tree)) != 0)
		return (err);

	return (0);
}

/*
 * Write the updated clconf tree to the ccr in the non-cluster mode
 * This is used is used to update the infrastructure table with the
 * new IP address range and new IP addresses calculated by the
 * topology manager. This cannot be used in cluster mode.
 */
clconf_errnum_t
clconf_cluster_write_nonclust_ccr(clconf_cluster_t *proposed)
{
	if (cl_current_tree::the().write_nonclust_ccr((clnode *)proposed)) {
		return (CL_NOERROR);
	} else {
		return (CL_CCR_ERROR);
	}
}
#endif

/* Get the iterators */
clconf_iter_t *
clconf_cluster_get_nodes(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "nodes"));
}

clconf_iter_t *
clconf_cluster_get_cables(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "cables"));
}

clconf_iter_t *
clconf_cluster_get_bbs(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "blackboxes"));
}

clconf_iter_t *
clconf_cluster_get_quorum_devices(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "quorum_devices"));
}

/*
 * Add components to a cluster.
 * Returns CL_BADNAME if the component added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_cluster_add_node(clconf_cluster_t *cl, clconf_node_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("nodes", (clnode *)n));
}

clconf_errnum_t
clconf_cluster_add_cable(clconf_cluster_t *cl, clconf_cable_t *c)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("cables", (clnode *)c));
}


clconf_errnum_t
clconf_cluster_add_bb(clconf_cluster_t *cl, clconf_bb_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("blackboxes", (clnode *)n));
}

clconf_errnum_t
clconf_cluster_add_quorum_device(clconf_cluster_t *cl, clconf_quorum_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("quorum_devices", (clnode *)n));
}


/*
 * Remove components from a cluster.
 * CL_INVALID_TREE if the component to be removed is not found in the cluster.
 */
clconf_errnum_t
clconf_cluster_remove_node(clconf_cluster_t *cl, clconf_node_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_NODE);
	return (((clnode *)cl)->remove_from_group("nodes", (clnode *)n));
}



clconf_errnum_t
clconf_cluster_remove_cable(clconf_cluster_t *cl, clconf_cable_t *c)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)c) == CL_CABLE);
	return (((clnode *)cl)->remove_from_group("cables", (clnode *)c));
}


clconf_errnum_t
clconf_cluster_remove_bb(clconf_cluster_t *cl, clconf_bb_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_BLACKBOX);
	return (((clnode *)cl)->remove_from_group("blackboxes",
	    (clnode *)n));
}

clconf_errnum_t
clconf_cluster_remove_quorum_device(clconf_cluster_t *cl, clconf_quorum_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_QUORUM_DEVICE);
	return (((clnode *)cl)->remove_from_group("quorum_devices",
	    (clnode *)n));
}

// Helper function

const char *
clconf_cluster_get_cmm_property_str(clconf_cluster_t *cl, const char *prtname,
    const char *default_value)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	ccr_node *cm = ((clnode *)cl)->get_named_child("cmm");
	if (cm == NULL) {
		return (default_value);
	}
	ccr_node *pts = cm->get_named_child("properties");
	if (pts == NULL) {
		return (default_value);
	}
	ccr_node *pt = pts->get_named_child(prtname);
	if (pt == NULL) {
		return (default_value);
	}
	const char *prt = pt->get_value();
	if (prt == NULL || prt[0] == 0) {
		// Return default value.
		return (default_value);
	}
	return (prt);
}

int
clconf_cluster_get_cmm_property(clconf_cluster_t *cl, const char *prtname,
    int default_value)
{
	const char *v = clconf_cluster_get_cmm_property_str(cl, prtname, NULL);
	if (v == NULL) {
		return (default_value);
	}
	return (os::atoi(v));
}

int
clconf_cluster_get_msgretries(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "msgretries", 3));
}

int
clconf_cluster_get_orb_return_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_return_timeout",
	    30000 /* milliseconds */));
}

int
clconf_cluster_get_orb_stop_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_stop_timeout",
	    30000 /* milliseconds */));
}

int
clconf_cluster_get_orb_abort_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_abort_timeout",
	    30000 /* milliseconds */));
}

int
clconf_cluster_get_orb_step_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_step_timeout",
	    120000 /* milliseconds */));
}

int
clconf_cluster_get_failfast_grace_time(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "failfast_grace_time",
	    10000 /* milliseconds */));
}

int
clconf_cluster_get_failfast_panic_delay(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "failfast_panic_delay",
	    30000 /* milliseconds */));
}

// This is in addition to the failfast_panic_delay

int
clconf_cluster_get_failfast_proxy_panic_delay(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl,
	    "proxy_failfast_panic_delay", 5000 /* milliseconds */));
}

int
clconf_cluster_get_node_fenceoff_timeout(clconf_cluster_t *cl)
{
	//
	// fenceoff_timeout is the timeout to decide that a
	// non-communicating node is dead and that it is safe to take over HA
	// services from that node. This is used to protect against data
	// corruption in case the cluster gets partitioned and the different
	// partitions take different amounts of time to run the quorum
	// algorithm to decide which partition should survive. This timeout
	// includes the time it takes a partitioned node to start a
	// reconfiguration, run the quorum algorithm and decide the outcome
	// of the quorum race. The winner of the quorum race waits for
	// node_fenceoff_timeout before declaring the non-communicating node
	// dead. If a node has not finished the quorum algorithm in this time,
	// it fails fast.
	//
	return (clconf_cluster_get_cmm_property(cl, "fenceoff_timeout",
	    300000 /* milliseconds */));
}

int
clconf_cluster_get_boot_delay(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "boot_delay",
		60000 /* milliseconds */));
}

int
clconf_cluster_get_node_halt_timeout(clconf_cluster_t *cl)
{
	//
	// halt_timeout is the failfast timeout used by a shutting down node,
	// enabled right before it sends the shutting down message to other
	// nodes in the cluster. It is also the timeout used by the other nodes
	// for safely declaring the shutting down node to be in the dead state
	// after getting the shutting down message from that node.
	//
	return (clconf_cluster_get_cmm_property(cl, "halt_timeout",
	    5000 /* milliseconds */));
}


const char *
clconf_cluster_get_failfast_mode(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property_str(cl, "failfast_mode",
	    "PANIC"));
}

/* Returns NULL if the node is not found */
const char *
clconf_cluster_get_nodename_by_nodeid(clconf_cluster_t *cl, nodeid_t id)
{
	clconf_iter_t *iter = clconf_cluster_get_nodes(cl);
	clconf_obj_t *obj;

	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		CL_PANIC(clconf_obj_get_objtype(obj) == CL_NODE);
		if ((nodeid_t)clconf_obj_get_id(obj) == id) {
			clconf_iter_release(iter);
			return (clconf_obj_get_name(obj));
		}
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);
	return (NULL);
}

/*
 * Gets node id given the nodename
 * If nodename is NULL then it returns the local nodeid
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 */
nodeid_t
clconf_cluster_get_nodeid_by_nodename(char *nodename)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter;
	clconf_node_t		*node;
	nodeid_t nid;
	const char *name;

	if (nodename == NULL) {
		return (clconf_get_local_nodeid());
	}

	nid = (nodeid_t)NODEID_UNKNOWN;

	cl = clconf_cluster_get_current();
	ASSERT(cl != NULL);
	iter = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {
		name = clconf_obj_get_name((clconf_obj_t *)node);
		if (name != NULL && strcmp(name, nodename) == 0) {
			nid = (nodeid_t)clconf_obj_get_id(
			    (clconf_obj_t *)node);
			break;
		}
	}

	clconf_iter_release(iter);
	clconf_obj_release((clconf_obj_t *)cl);
	return (nid);
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
/*
 * Returns the number of virtual clusters configured.
 * Returns 0 if called in non-global zone.
 */
unsigned int
clconf_cluster_get_num_clusters()
{
#ifndef	_KERNEL_ORB
	//
	// In global zone user mode, check for updates to clconf trees
	// for zone clusters. Zone clusters could have been added/removed;
	// or infrastructure table of a zone cluster could have changed.
	// This function refreshes all the zone cluster clconf trees.
	//
	if (getzoneid() == GLOBAL_ZONEID) {
		if (clconf_ccr::the().check_zone_cluster_list() != 0) {
			//
			// The clconf trees for the zone clusters could be
			// stale. So return zero.
			//
			return (0);
		}
	}
#endif	// !_KERNEL_ORB

	// Now the clconf is refreshed. Get the number of zone clusters
	return (cl_current_tree::the().get_num_virtual_clusters());
}

/*
 * Returns the names of virtual clusters configured.
 * The names are returned as an array of strings.
 * The array is NULL terminated, that is, the last index in the array
 * of pointers contains NULL.
 * The caller is responsible for freeing up the memory for the names returned.
 * Returns NULL if called in non-global zone.
 */
char **
clconf_cluster_get_cluster_names()
{
#ifndef	_KERNEL_ORB
	//
	// In global zone user mode, check for updates to clconf trees
	// for zone clusters. Zone clusters could have been added/removed;
	// or infrastructure table of a zone cluster could have changed.
	// This function refreshes all the zone cluster clconf trees.
	//
	if (getzoneid() == GLOBAL_ZONEID) {
		if (clconf_ccr::the().check_zone_cluster_list() != 0) {
			//
			// The clconf trees for the zone clusters could be
			// stale. So return NULL.
			//
			return (NULL);
		}
	}
#endif	// !_KERNEL_ORB

	// Now the clconf is refreshed. Get the names of zone clusters
	return (cl_current_tree::the().get_virtual_cluster_names());
}

/*
 * Gets node id given the nodename in a zone cluster.
 * If nodename is NULL then it returns the local nodeid
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 */
nodeid_t
clconf_zc_get_nodeid_by_nodename(char *zcname, char *nodename)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter;
	clconf_node_t		*node;
	nodeid_t nid;
	const char *name;

	if (nodename == NULL) {
		return (clconf_get_local_nodeid());
	}

	if (zcname == NULL) {
		cl = clconf_cluster_get_current();
	} else {
		cl = clconf_cluster_get_vc_current(zcname);
	}

	ASSERT(cl != NULL);

	nid = (nodeid_t)NODEID_UNKNOWN;

	iter = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {
		name = clconf_obj_get_name((clconf_obj_t *)node);
		if (name != NULL && strcmp(name, nodename) == 0) {
			nid = (nodeid_t)clconf_obj_get_id(
			    (clconf_obj_t *)node);
			break;
		}
	}

	clconf_iter_release(iter);
	clconf_obj_release((clconf_obj_t *)cl);
	return (nid);
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

/*
 * Gets node id given the ip address, if the given ip address is the
 * private hostname of a node.
 * The ip address is specified in in_addr_t.
 *
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 */
nodeid_t
clconf_cluster_get_nodeid_for_private_ipaddr(in_addr_t ipaddr)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter;
	clconf_node_t		*node;
	struct in_addr		cipaddr;
	nodeid_t nid;
	nodeid_t id;

	nid = (nodeid_t)NODEID_UNKNOWN;

	cl = clconf_cluster_get_current();
	ASSERT(cl != NULL);
	iter = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {

		id = (nodeid_t)clconf_obj_get_id((clconf_obj_t *)node);
		if ((clconf_get_user_ipaddr(id, &cipaddr) == 0) &&
		    (ipaddr == cipaddr._S_un._S_addr)) {
			nid = id;
			break;
		}
	}

	clconf_iter_release(iter);
	clconf_obj_release((clconf_obj_t *)cl);
	return (nid);
}


/*
 * Gets node id given the ip address.
 * The ip address is specified in in_addr_t.
 *
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 *	returns nodeid (nid) and adapter id (adpid) as side-effects.
 *	User provides allocated ptrs for nid and adpid
 *	On success these ptrs are set to appropriate values.
 *	On failure nid contains NODEID_UNKNOWN and adpid is set to 0.
 */
nodeid_t
clconf_cluster_get_nodeid_for_ipaddr(nodeid_t *nid, int *adpid,
					in_addr_t ipaddr)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter1, *iter2;
	clconf_node_t		*node;
	clconf_adapter_t	*adp;
	const char		*cl_ipaddr;
	in_addr_t		cipaddr;

	ASSERT(nid != NULL);
	ASSERT(adpid != NULL);
	ASSERT(ipaddr != 0);

	*nid = (nodeid_t)NODEID_UNKNOWN;
	*adpid = 0;

	cl = clconf_cluster_get_current();
	ASSERT(cl != NULL);
	iter1 = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {

	    iter2 = clconf_node_get_adapters(node);

	    for (; (adp = (clconf_adapter_t *)
		clconf_iter_get_current(iter2)) != NULL;
		clconf_iter_advance(iter2)) {

		if ((cl_ipaddr = clconf_obj_get_property
		    ((clconf_obj_t *)adp, "ip_address")) == NULL)
		    continue;

		cipaddr = os::inet_addr(cl_ipaddr);
		if (cipaddr == (in_addr_t)-1) {
			continue;
		}

		if (ipaddr == cipaddr) {
		    *nid = (nodeid_t)clconf_obj_get_id((clconf_obj_t *)node);
		    *adpid = clconf_obj_get_id((clconf_obj_t *)adp);
		    clconf_iter_release(iter2);
		    clconf_iter_release(iter1);
		    clconf_obj_release((clconf_obj_t *)cl);
		    return (*nid);
		}
	    }
	    clconf_iter_release(iter2);
	}
	clconf_iter_release(iter1);
	clconf_obj_release((clconf_obj_t *)cl);
	return (*nid);
}

//
// Returns true if 'id' is a current member of the cluster, false if not.
// Returns false if an error occurs.
// Has to be defined as boolean_t because it is now referenced in a header
// file in sys.
// For zone clusters, the 'id' is considered to be a current member
// of the cluster if the zone cluster is running on the base node
// corresponding to nodeid 'id'. In other words, the zone (of the zone cluster)
// on the base node with nodeid 'id' should be part of the membership of
// the zone cluster.
//
boolean_t
conf_is_cluster_member(nodeid_t id)
{
	quorum_status_t *clust_memb;
	boolean_t state = B_FALSE;

	if (cluster_get_quorum_status(&clust_memb) != 0)
		return (B_FALSE);

	for (uint32_t i = 0; i < clust_memb->num_nodes; i++) {
		if ((clust_memb->nodelist[i].nid == id) &&
		    (clust_memb->nodelist[i].state == QUORUM_STATE_ONLINE)) {
			state = B_TRUE;
			break;
		}
	}

	cluster_release_quorum_status(clust_memb);
	return (state);
}

//
// Request the Cluster Membership Monitor to run a reconfiguration.
// This may be called after adding a quorum device to allow all
// nodes to have the same view of the quorum device. Otherwise, nodes
// that do not have access to the quorum device may not know which node
// is "mastering" the quorum device.
//
void
clconf_cluster_reconfigure(void)
{
	Environment e;
	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	cmm_cntl_v->reconfigure(e);
	e.clear();
}


//
// Execute the program 'program' on the node 'id'.
//
// Returns CL_NO_CLUSTER if the node is not a current cluster member.
// Returns CL_INVALID_OBJ if it can not execute the program.
// Returns CL_NO_ERROR if there's no error.
//
// This call provides at-least-once semantics.  The program that is  specified
// may end up being run on the node multiple times, so it is very important
// that it be idempotent.
//
clconf_errnum_t
clconf_do_execution(const char *program, nodeid_t id, boolean_t *clust_memb,
    boolean_t realtime, boolean_t foreground, boolean_t log)
{
		clconf_errnum_t clconf_err;
#ifdef	_KERNEL
		if (clconf_exec_program_with_opts_ptr == NULL) {
			return (CL_INVALID_OBJ);
		}

		clconf_err = (*clconf_exec_program_with_opts_ptr)(program, id,
		    clust_memb, realtime);
#else
		clconf_err = clconf_exec_program_with_opts(program, id,
		    clust_memb, realtime);
#endif	// _KERNEL

	return (clconf_err);
}

/*
 * Properties that we can access through clconf_obj_get_property.
 */

/*
 * ***********************************************************************
 * Interfaces with a quorum device object.
 * ***********************************************************************
 */

/*
 * Create a cluster quorum_device object.
 */
clconf_quorum_t *
clconf_quorum_device_create()
{
	return ((clconf_quorum_t *)new clnode("-1"));
}

/*
 * ***********************************************************************
 * Interfaces with a node object.
 * ***********************************************************************
 */

/*
 * Create a cluster node object.
 */
clconf_node_t *
clconf_node_create()
{
	return ((clconf_node_t *)new clnode("-1"));
}

/* Get the adapter iterator */
clconf_iter_t *
clconf_node_get_adapters(clconf_node_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_NODE);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "adapters"));
}

/*
 * Add an adapter to a node.
 * Returns CL_BADNAME if the adapter added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_node_add_adapter(clconf_node_t *n, clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_NODE);
	return (((clnode *)n)->add_to_group("adapters", (clnode *)ada));
}

/*
 * Remove an adapter from a node.
 * CL_INVALID_TREE if the adapter to be removed is not found in the node.
 */
clconf_errnum_t
clconf_node_remove_adapter(clconf_node_t *n, clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_NODE);
	return (((clnode *)n)->remove_from_group("adapters", (clnode *)ada));
}


/*
 * Get the private hostname of this node. The default would be
 * "<nodename>-priv"
 */
const char *
clconf_node_get_private_hostname(clconf_node_t *nd)
{
	const char *ret = clconf_obj_get_property((clconf_obj_t *)nd,
	    "private_hostname");
	if (ret == NULL) {
		// construct the hostname.
		const char *ndname = clconf_obj_get_name((clconf_obj_t *)nd);
		char *hstname = new char[os::strlen(ndname) +
		    os::strlen("-priv") + 1];
		os::sprintf(hstname, "%s-priv", ndname);

		// add the private hostname to the property.
		clconf_errnum_t err = clconf_obj_set_property(
		    (clconf_obj_t *)nd, "private_hostname", hstname);
		CL_PANIC(err == CL_NOERROR);
		ret = hstname;
	}
	return (ret);
}

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 */



/*
 * ***********************************************************************
 * Interfaces with an adapter object.
 * ***********************************************************************
 */

/*
 * Create a cluster adapter object.
 */
clconf_adapter_t *
clconf_adapter_create()
{
	return ((clconf_adapter_t *)new clnode("-1"));
}

/*
 * Returns the name of the device
 */
const char *
clconf_adapter_get_device_name(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	return (clconf_obj_get_property((clconf_obj_t *)ada, "device_name"));
}

/*
 * Some devices have two drivers, one for the device and one for the DLPI
 * driver implemented as a layered driver.
 * The latter is indicated with a "dlpi_device_name" property in the adapter
 * We expect that the instance number of the device and the DLPI device are
 * the same.
 * Returns the name of the DLPI device
 */
const char *
clconf_adapter_get_dlpi_device_name(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	//
	// First get the dlpi_device_name if one exists on the adapter
	// If none, then get the device name itself
	//
	const char *dn;
	dn = clconf_obj_get_property((clconf_obj_t *)ada, "dlpi_device_name");
	if (dn == NULL) {
		dn = clconf_adapter_get_device_name(ada);
	}
	return (dn);
}

/*
 * Returns the instance number of the device
 * Returns -1, if one does not exist
 */
int
clconf_adapter_get_device_instance(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	const char *prpt = clconf_obj_get_property((clconf_obj_t *)ada,
	    "device_instance");
	if (prpt != NULL) {
		return (os::atoi(prpt));
	}
	return (-1);
}

/*
 * Returns the private VLAN-ID associated with the adapter.
 * Returns 0, if one does not exist.
 */
int
clconf_adapter_get_vlan_id(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	const char *prpt = clconf_obj_get_property((clconf_obj_t *)ada,
	    "vlan_id");
	if (prpt != NULL) {
		return (os::atoi(prpt));
	}
	return (0);
}

/*
 * A VLAN device has a unique name associated with it. If there is a
 * VLAN-ID property present, then the adapter name is the VLAN device
 * name.
 */
const char *
clconf_adapter_get_vlan_device_name(clconf_adapter_t *ada)
{
	const char *dn;
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	if (clconf_adapter_get_vlan_id(ada) == 0) {
		// It's not a VLAN device.
		return (NULL);
	}
	dn = clconf_obj_get_name((clconf_obj_t *)ada);
	ASSERT(dn);
	return (dn);
}

/* Get the port iterator */
clconf_iter_t *
clconf_adapter_get_ports(clconf_adapter_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_ADAPTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "ports"));
}

/*
 * Add a port to an adapter.
 * Returns CL_BADNAME if the port added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_adapter_add_port(clconf_adapter_t *ada, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	return (((clnode *)ada)->add_to_group("ports", (clnode *)pt));
}

/*
 * Remove a port from an adapter. Returns
 * CL_INVALID_TREE if the port to be removed is not found in the adapter.
 */
clconf_errnum_t
clconf_adapter_remove_port(clconf_adapter_t *ada, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	return (((clnode *)ada)->remove_from_group("ports", (clnode *)pt));
}

/*
 * Get/set the transport type.
 */
const char *
clconf_adapter_get_transport_type(clconf_adapter_t *a)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)a) == CL_ADAPTER);
	return (clconf_obj_get_property((clconf_obj_t *)a, "transport_type"));
}

void
clconf_adapter_set_transport_type(clconf_adapter_t *ada, const char *type)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	(void) clconf_obj_set_property((clconf_obj_t *)ada, "transport_type",
	    type);
}

void
clconf_adapter_set_device_name(clconf_adapter_t *ada, const char *devn)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	(void) clconf_obj_set_property((clconf_obj_t *)ada, "device_name",
	    devn);
}

void
clconf_adapter_set_device_instance(clconf_adapter_t *ada, int inst)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	char n[32];
	(void) os::itoa(inst, n);
	(void) clconf_obj_set_property((clconf_obj_t *)ada, "device_instance",
	    n);
}

/*
 * ***********************************************************************
 * Interfaces with an port object.
 * ***********************************************************************
 */

/*
 * Create a cluster port object.
 */
clconf_port_t *
clconf_port_create()
{
	return ((clconf_port_t *)new clnode("-1"));
}

/*
 * ***********************************************************************
 * Interfaces with an black box object.
 * ***********************************************************************
 */

/*
 * Create a cluster bb object.
 */
clconf_bb_t *
clconf_bb_create()
{
	return ((clconf_bb_t *)new clnode("-1"));
}

/*
 * Set and get type.
 */
void
clconf_bb_set_type(clconf_bb_t *bb, const char *type)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	(void) clconf_obj_set_property((clconf_obj_t *)bb, "type", type);
}

const char *
clconf_bb_get_type(clconf_bb_t *bb)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	return (clconf_obj_get_property((clconf_obj_t *)bb, "type"));
}

#ifndef _KERNEL
/*
 * Set and get transport type.
 */
void
clconf_bb_set_transport_type(clconf_bb_t *bb, const char *trtype)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	(void) clconf_obj_set_property((clconf_obj_t *)bb, "transport_type",
					trtype);
}

const char *
clconf_bb_get_transport_type(clconf_bb_t *bb)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	return (clconf_obj_get_property((clconf_obj_t *)bb, "transport_type"));
}

/*
 * Set and get vlan id.
 */
void
clconf_bb_set_vlan_id(clconf_bb_t *bb, const int vid)
{
	char vlan_id_str[16];	/* vlanid id <= 4096 */
	ASSERT(vid <= 4096);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);

	if (vid == 0) {
		(void) clconf_obj_set_property((clconf_obj_t *)bb, "vlan_id",
						NULL);
	} else {
		(void) snprintf(vlan_id_str, 16UL, "%d", vid);
		(void) clconf_obj_set_property((clconf_obj_t *)bb, "vlan_id",
						vlan_id_str);
	}
}

const int
clconf_bb_get_vlan_id(clconf_bb_t *bb)
{
	const char *vlan_id_str;
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	vlan_id_str = clconf_obj_get_property((clconf_obj_t *)bb, "vlan_id");
	if (vlan_id_str == NULL)
		return (-1);
	return (os::atoi(vlan_id_str));
}
#endif

/* Get the port iterator */
clconf_iter_t *
clconf_bb_get_ports(clconf_bb_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_BLACKBOX);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "ports"));
}

/*
 * Add a port to a black box.
 * Returns CL_BADNAME if the port added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_bb_add_port(clconf_bb_t *bb, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	return (((clnode *)bb)->add_to_group("ports", (clnode *)pt));
}

/*
 * Remove a port from the black box. Returns
 * CL_INVALID_TREE if the port to be removed is not found in the bb.
 */
clconf_errnum_t
clconf_bb_remove_port(clconf_bb_t *bb, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	return (((clnode *)bb)->remove_from_group("ports", (clnode *)pt));
}

/*
 * ***********************************************************************
 * Interfaces with an cable object.
 * ***********************************************************************
 */

/*
 * Create a cluster cable object.
 */
clconf_cable_t *
clconf_cable_create()
{
	return ((clconf_cable_t *)new clnode("-1"));
}

/*
 * Get and set port. End number can only be 1 or 2.
 */
clconf_port_t *
clconf_cable_get_endpoint(clconf_cable_t *cbp, int endnum)
{
	CL_PANIC(endnum == 1 || endnum == 2);
	const char *enddes = clconf_obj_get_property((clconf_obj_t *)cbp,
	    (endnum == 1 ? "end1" : "end2"));
	if (enddes == NULL)
		return (NULL);
	clconf_obj_t *cl = clconf_obj_get_parent((clconf_obj_t *)cbp);

	ccr_node *ret = ((clnode *)cl)->get_child_by_name(enddes);
	return ((clconf_port_t *)ret);
}

clconf_errnum_t
clconf_cable_set_endpoint(clconf_cable_t *cbp, int endnum, clconf_port_t *pt)
{
	char *fname = ((clnode *)pt)->get_full_name();

	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	CL_PANIC(endnum == 1 || endnum == 2);
	(void) clconf_obj_set_property((clconf_obj_t *)cbp,
	    (endnum == 1 ? "end1" : "end2"), fname);

	delete [] fname;
	return (CL_NOERROR);
}

/* Functions and data structures related to paths */

SList<clconf_path_t> *clconf_path_list = NULL;

/* This should be put into a C++ header file */
SList<clconf_path_t> &
clconf_path_get_all()
{
	ASSERT(clconf_path_list != NULL);
	return (*clconf_path_list);
}

void
clconf_path_remove_all()
{
	clconf_path_t *p;
	while ((p = clconf_path_get_all().reapfirst()) != NULL) {
#ifdef _KERNEL_ORB
		(void) pm_remove_path_funcp(p);
#endif
		delete p;
	}
}

void
clconf_path_set_endpoint(clconf_path_t *p, clconf_endtype_t et,
    clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	clconf_obj_t *ada = clconf_obj_get_parent((clconf_obj_t *)pt);
	clconf_obj_t *nd = clconf_obj_get_parent(ada);
	p->end[et].nodeid = (nodeid_t)clconf_obj_get_id(nd);
	p->end[et].adapterid = clconf_obj_get_id(ada);
	p->end[et].portid = clconf_obj_get_id((clconf_obj_t *)pt);
}

clconf_path_t *
clconf_path_create(clconf_port_t *p1, clconf_port_t *p2, uint_t q, uint_t t)
{
	clconf_path_t *p = new clconf_path_t;
	clconf_path_set_endpoint(p, CL_LOCAL, p1);
	clconf_path_set_endpoint(p, CL_REMOTE, p2);
	p->quantum = q;
	p->timeout = t;

	// Make sure p1 is a local port and p2 is remote.
	CL_PANIC(p->end[CL_LOCAL].nodeid == orb_conf::local_nodeid());
	CL_PANIC(p->end[CL_REMOTE].nodeid != orb_conf::local_nodeid());
	return (p);
}

clconf_path_t *
clconf_path_dup(clconf_path_t *p)
{
	clconf_path_t *pt = new clconf_path_t;
	(*pt) = (*p);
	return (pt);
}

void
clconf_path_release(clconf_path_t *p)
{
	delete p;
}

//
// Returns the associated bitmask for a property.
// If a new adapter property is added, and this function is not modified
// then AP_NONE is returned, which may be fine for some adapter properties
// which cannot be changed dynamically.
//

uint_t
clconf_get_pcode(const char *pname)
{
	if (strcmp("ip_address", pname) == 0)
		return (AP_IP_ADDRESS);
	if (strcmp("netmask", pname) == 0)
		return (AP_NETMASK);
	if (strcmp("adapter_id", pname) == 0)
		return (AP_ADAPTER_ID);
	if (strcmp("bandwidth", pname) == 0)
		return (AP_BANDWIDTH);
	if (strcmp("nw_bandwidth", pname) == 0)
		return (AP_NW_BANDWIDTH);
	if (os::strstr(pname, "_heartbeat_timeout") != NULL)
		return (AP_HBT_TIMEOUT);
	if (os::strstr(pname, "_heartbeat_quantum") != NULL)
		return (AP_HBT_QUANTUM);
	if (strcmp("ports", pname) == 0)
		return (AP_PORTS);
	return (AP_NONE);
}

clconf_adapter_t *
clconf_path_get_adapter(clconf_path_t *p, clconf_endtype_t et,
    clconf_cluster_t *cl)
{
	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl, CL_NODE,
	    (int)p->end[et].nodeid);
	return ((clconf_adapter_t *)clconf_obj_get_child(nd, CL_ADAPTER,
	    p->end[et].adapterid));
}

/*
 * Returns the adapter name as a string in a freshly allocated buffer.
 * Caller should free up the memory after its usage is done.
 */
char *
clconf_get_adaptername(clconf_adapter_t *ap)
{
	const char *objname;
	char *adpname;

	objname = clconf_obj_get_name((clconf_obj_t *)ap);
	ASSERT(objname != NULL);
	adpname = new char[strlen(objname) + 1];
	ASSERT(adpname != NULL);
	(void) sprintf(adpname, "%s", objname);

	return (adpname);
}

clconf_port_t *
clconf_path_get_endpoint(clconf_path_t *p, clconf_endtype_t et,
    clconf_cluster_t *cl)
{
	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl,
	    CL_NODE, (int)p->end[et].nodeid);
	clconf_obj_t *ada = clconf_obj_get_child(nd, CL_ADAPTER,
	    p->end[et].adapterid);
	clconf_obj_t *pt = clconf_obj_get_child(ada, CL_PORT,
	    p->end[et].portid);
	return ((clconf_port_t *)pt);
}

/*
 * Constructs an internal pathname with local nodeid, adapterid and remote
 * nodeid, adapterid given.
 * Caller should free up the memory after it is used.
 * If arguments passed are 1,1,2,1 then the following string is returned
 * cluster.nodes.1.adapters.1.nodes.2.adapters.1
 * (Always lower of the two nodeids will be the first node in the string)
 */
char *
clconf_get_internal_pathname(nodeid_t n1, uint_t a1, nodeid_t n2, uint_t a2)
{
	char *fmtstr = "cluster.nodes.%u.adapters.%u.nodes.%u.adapters.%u";
	// maximum uint is 4294967296, which is of length 10.
	size_t size = strlen(fmtstr) + 4*10;
	char *name = new char[size];
	nodeid_t tn = n2;
	uint_t ta = a2;

	if (n1 > n2) {
		n2 = n1;
		n1 = tn;
		a2 = a1;
		a1 = ta;
	}
	os::sprintf(name, fmtstr, n1, a1, n2, a2);
	return (name);
}


/*
 * Constructs the path name from the given path and cluster  objects and returns
 * it as a string. Caller should free up the memory after its usage is
 * done. pathname is constructed as local_adaptername - remote_adaptername
 * local_adaptername is local_nodename:adaptername
 * remote_adaptername is remote_nodename:adaptername
 */
char *
clconf_get_pathname(clconf_path_t *p, clconf_cluster_t *cl)
{
	char *path_name, *adpname;
	const char *nodename;
	char lpe_name[64], rpe_name[64];

	clconf_adapter_t *adp = clconf_path_get_adapter(p, CL_LOCAL, cl);
	ASSERT(adp);
	nodename = clconf_obj_get_name(clconf_obj_get_parent(
	    (clconf_obj_t *)adp));
	adpname = clconf_get_adaptername(adp);
	ASSERT((nodename != NULL) && (adpname != NULL));
	if (os::snprintf(lpe_name, 64UL, "%s:%s", nodename, adpname) >= 63)
		lpe_name[63] = '\0';
	delete [] adpname;

	adp = clconf_path_get_adapter(p, CL_REMOTE, cl);
	ASSERT(adp);
	nodename = clconf_obj_get_name(clconf_obj_get_parent(
	    (clconf_obj_t *)adp));
	adpname = clconf_get_adaptername(adp);
	ASSERT((nodename != NULL) && (adpname != NULL));
	if (os::snprintf(rpe_name, 64UL, "%s:%s", nodename, adpname) >= 63)
		rpe_name[63] = '\0';
	delete [] adpname;

	path_name = new char [strlen(lpe_name)+strlen(rpe_name)+4];
	ASSERT(path_name != NULL);
	(void) sprintf(path_name, "%s - %s", lpe_name, rpe_name);
	return (path_name);
}

void
clconf_path_set_quantum(clconf_path_t *p, uint_t q)
{
	p->quantum = q;
}

void
clconf_path_set_timeout(clconf_path_t *p, uint_t t)
{
	p->timeout = t;
}

uint_t
clconf_path_get_quantum(clconf_path_t *p)
{
	return (p->quantum);
}

uint_t
clconf_path_get_timeout(clconf_path_t *p)
{
	return (p->timeout);
}

/* Add a path to the current clconf tree */
void
clconf_cluster_add_path(clconf_path_t *p)
{
	clconf_path_get_all().append(p);
#ifdef _KERNEL_ORB
	(void) pm_add_path_funcp(p);
#endif
}

/* remove a path */
void
clconf_cluster_remove_path(clconf_path_t *p)
{
	(void) clconf_path_get_all().erase(p);
#ifdef _KERNEL_ORB
	(void) pm_remove_path_funcp(p);
#endif
	delete p;
}

/* Get a path */
clconf_path_t *
clconf_cluster_get_path(int local_adapterid, nodeid_t remote_nodeid,
    int remote_adapterid)
{
	clconf_path_t *p;
	SList<clconf_path_t>::ListIterator iter(clconf_path_get_all());
	while ((p = iter.get_current()) != NULL) {
		if (p->end[CL_LOCAL].adapterid == local_adapterid &&
		    p->end[CL_REMOTE].nodeid == remote_nodeid &&
		    p->end[CL_REMOTE].adapterid == remote_adapterid) {
			return (p);
		}
		iter.advance();
	}
	return (NULL);
}

nodeid_t
clconf_path_get_remote_nodeid(clconf_path_t *p)
{
	return (p->end[CL_REMOTE].nodeid);
}

int
clconf_path_get_adapterid(clconf_path_t *p, clconf_endtype_t ed)
{
	return (p->end[ed].adapterid);
}

int
clconf_path_get_portid(clconf_path_t *p, clconf_endtype_t ed)
{
	return (p->end[ed].portid);
}

// Compares two paths. Return true if they are the same, false if not.
// Both paths should be local paths. We only compare the topology part
// of the path. Quantum and timeout are not included.
bool
clconf_path_equal(clconf_path_t *p1, clconf_path_t *p2)
{
	if (p1 == p2)
		return (true);

	if (p1 == NULL || p2 == NULL)
		return (false);

	CL_PANIC(p1->end[0].nodeid == p2->end[0].nodeid);

	return (p1->end[0].adapterid == p2->end[0].adapterid &&
	    p1->end[0].portid == p2->end[0].portid &&
	    p1->end[1].nodeid == p2->end[1].nodeid &&
	    p1->end[1].adapterid == p2->end[1].adapterid &&
	    p1->end[1].portid == p2->end[1].portid);
}

clconf_path_t *
clconf_get_path(clconf_port_t *p1, clconf_port_t *p2)
{
	// Construct a path using p1 and p2.
	clconf_path_t pth;
	clconf_path_set_endpoint(&pth, CL_LOCAL, p1);
	clconf_path_set_endpoint(&pth, CL_REMOTE, p2);

	// Search the current path list to find the specified path.
	clconf_path_t *p;
	SList<clconf_path_t>::ListIterator iter(clconf_path_get_all());
	while ((p = iter.get_current()) != NULL) {
		if (clconf_path_equal(p, &pth)) {
			return (p);
		}
		iter.advance();
	}
	return (NULL);
}

//
// Get local node reservation key.
//
int
clconf_get_local_key(uint64_t *the_keyvalp)
{
	clconf_cluster_t	*clconfp = NULL;
	clconf_iter_t		*iterp;
	clconf_obj_t		*objp;
	nodeid_t		local_nid;
	nodeid_t		current_nid;
	const char		*keystrp = NULL;
	uint64_t		keyval = 0;

	clconfp = clconf_cluster_get_current();
	ASSERT(clconfp != NULL);
	iterp = clconf_cluster_get_nodes(clconfp);
	local_nid = orb_conf::local_nodeid();
	//
	// Search local node key string in ccr configuration.
	//
	for (; (objp = clconf_iter_get_current(iterp)) != NULL;
	    clconf_iter_advance(iterp)) {
		current_nid = (nodeid_t)clconf_obj_get_id(objp);
		if (current_nid == local_nid) {
			keystrp = clconf_obj_get_property(
			    objp, "quorum_resv_key");
			break;
		}
	}

	clconf_iter_release(iterp);
	clconf_obj_release((clconf_obj_t *)clconfp);

	if (keystrp == NULL) {
		return (1);
	}

	//
	// The key is entered in the CCR as hex digits.
	// Allow preceding "0x".
	//
	if ((keystrp[0] == '0') && (keystrp[1] == 'x')) {
		keystrp = &(keystrp[2]);
	}
	// Convert hex key to decimal value
	for (int i = 0; i < 2 * MHIOC_RESV_KEY_SIZE; i++) {
		char c = keystrp[i];
		if ((c >= '0') && (c <= '9')) {
			keyval = (keyval << 4) + (uchar_t)(c - '0');
		} else if ((c >= 'A') && (c <= 'F')) {
			keyval = (keyval << 4) + 0xa
				+ (uchar_t)(c - 'A');
		} else if ((c >= 'a') && (c <= 'f')) {
			keyval = (keyval << 4) + 0xa
				+ (uchar_t)(c - 'a');
		} else {
			break;
		}
	}
	*the_keyvalp = keyval;
	return (0);
}

//
// Convert the quorum configuration info in cl_in into the quorum_table_t
// data. A NULL value for cl_ini stands for the current cluster configuration.
// Returns 0 if successful and ENOMEM if cannot allocate memory.
//
int
clconf_get_quorum_table(clconf_cluster_t *cl_in, quorum_table_t **qtp)
{
	clconf_cluster_t	*cl = cl_in;
	quorum_table_t		*qt;
	nodeid_t		max_nid;
	uint_t			max_qid = 0;
	nodeid_t		nid;
	uint_t			qid;
	clconf_iter_t		*iter;
	clconf_obj_t		*obj;
	os::sc_syslog_msg	*syslog_msgp;
	char			nodename[8];

	//
	// If no clconf_cluster_t is passed in, use the current clconf.
	//
	if (cl_in == NULL) {
		cl = clconf_cluster_get_current();
	}

	os::sprintf(nodename, "%u", orb_conf::local_nodeid());
	syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		nodename, NULL);

	if (syslog_msgp == NULL) {
		if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		}
		return (ENOMEM);
	}

	qt = new quorum_table_t;
	*qtp = qt;

	if (qt == NULL) {
		//
		// SCMSGS
		// @explanation
		// Could not allocate memory while converting the quorum
		// configuration information into quorum table.
		// @user_action
		// This is an unrecoverable error, and the cluster needs to be
		// rebooted. Also contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: No memory to read quorum configuration table");
		delete syslog_msgp;
		if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		}
		return (ENOMEM);
	} else {
		//
		// Initialize pointers so that the table can be released
		// correctly in case of lack of memory.
		//
		qt->nodes.list = NULL;
		qt->quorum_devices.list = NULL;
	}

	/*
	 * Set max_nid to the largest configured node#.
	 */
	max_nid = NODEID_UNKNOWN;
	iter = clconf_cluster_get_nodes(cl);
	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		if (clconf_obj_get_id(obj) > (int)max_nid) {
			max_nid = (nodeid_t)clconf_obj_get_id(obj);
		}
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);
	CL_PANIC(max_nid != NODEID_UNKNOWN);

	/*
	 * Set max_qid to the largest configured quorum device#.
	 */
	max_qid = 0;
	iter = clconf_cluster_get_quorum_devices(cl);
	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		if (clconf_obj_get_id(obj) > (int)max_qid) {
			max_qid = (nodeid_t)clconf_obj_get_id(obj);
		}
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);
	if (max_qid > MAX_QUORUM_DEVICES) {
		clconf_release_quorum_table(qt);
		//
		// SCMSGS
		// @explanation
		// Found the quorum device ID being invalid while converting
		// the quorum configuration information into quorum table.
		// @user_action
		// Check the quorum configuration information.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: Quorum device ID %ld is invalid. The largest "
			"supported ID is %ld", max_qid, MAX_QUORUM_DEVICES);
		delete syslog_msgp;
		if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		}
		return (EINVAL);
	}

	qt->nodes.list = new node_info_t[max_nid];
	qt->quorum_devices.list = new quorum_device_info_t[max_qid];
	if ((qt->nodes.list == NULL) || (qt->quorum_devices.list == NULL)) {
		clconf_release_quorum_table(qt);
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: No memory to read quorum configuration table");
		delete syslog_msgp;
		if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		}
		return (ENOMEM);
	}
	qt->nodes.listlen = max_nid;
	qt->quorum_devices.listlen = max_qid;

	/*
	 * Fill the node info with default values to handle holes in node IDs.
	 */
	for (nid = 1; nid <= max_nid; nid++) {
		node_info_t	*nodeinfop = &qt->nodes.list[nid-1];
		char		*key = (char *)nodeinfop->reservation_key.key;

		nodeinfop->nodenum = nid;
		nodeinfop->votes = 0;
		bzero(key, (size_t)MHIOC_RESV_KEY_SIZE);
	}

	/* Set up the vote/reservation_key for the nodes */
	iter = clconf_cluster_get_nodes(cl);
	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		const char	*votestr;
		const char	*keystr;
		uint64_t	keyval = 0;
		int		i;

		nid = (nodeid_t)clconf_obj_get_id(obj);

		votestr = clconf_obj_get_property(obj, "quorum_vote");
		if (votestr == NULL) {
		    clconf_release_quorum_table(qt);
		    //
		    // SCMSGS
		    // @explanation
		    // Found the quorum vote field being incorrect while
		    // converting the quorum configuration information into
		    // quorum table.
		    // @user_action
		    // Check the quorum configuration information.
		    //
		    (void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: No valid quorum_vote field for node %u", nid);
		    delete syslog_msgp;
		    if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		    }
		    return (EINVAL);
		} else
		    qt->nodes.list[nid-1].votes = (uint32_t)os::atoi(votestr);

		keystr = clconf_obj_get_property(obj, "quorum_resv_key");
		if (keystr == NULL) {
		    clconf_release_quorum_table(qt);
		    //
		    // SCMSGS
		    // @explanation
		    // Found the quorum_resv_key field being incorrect while
		    // converting the quorum configuration information into
		    // quorum table.
		    // @user_action
		    // Check the quorum configuration information.
		    //
		    (void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: No valid quorum_resv_key field "
			"for node %u", nid);
		    delete syslog_msgp;
		    if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		    }
		    return (EINVAL);
		} else {
			//
			// The key is entered in the CCR as hex digits.
			// Allow preceding "0x".
			//
			if ((keystr[0] == '0') && (keystr[1] == 'x'))
			    keystr = &(keystr[2]);
			for (i = 0; i < 2 * MHIOC_RESV_KEY_SIZE; i++) {
			    char c = keystr[i];
			    if ((c >= '0') && (c <= '9'))
				keyval = (keyval << 4) + (uchar_t)(c - '0');
			    else if ((c >= 'A') && (c <= 'F'))
				keyval = (keyval << 4) + 0xa
						+ (uchar_t)(c - 'A');
			    else if ((c >= 'a') && (c <= 'f'))
				keyval = (keyval << 4) + 0xa
						+ (uchar_t)(c - 'a');
			    else
				break;
			}
		}
		for (i = MHIOC_RESV_KEY_SIZE-1; i >= 0; i--) {
			qt->nodes.list[nid-1].reservation_key.key[i] =
				(char)(keyval & 0xff);
			keyval >>= 8;
		}
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);

	/*
	 * Fill the quorum device info with default values to handle holes
	 * in quorum device IDs.
	 */
	for (qid = 1; qid <= max_qid; qid++) {
		quorum_device_info_t	*qinfo;

		qinfo = &qt->quorum_devices.list[qid-1];
		qinfo->qid = qid;
		qinfo->gdevname = NULL;
		qinfo->type = NULL;
		qinfo->votes = 0;
		qinfo->nodes_with_enabled_paths = 0;
		qinfo->nodes_with_configured_paths = 0;
		qinfo->access_mode = NULL;
	}

	iter = clconf_cluster_get_quorum_devices(cl);
	while ((obj = clconf_iter_get_current(iter)) != NULL) {

		quorum_device_info_t		*qinfo;
		const char			*votestr;
		const char			*propvalue;
		char				pathnm[30];

		qid = (uint_t)clconf_obj_get_id(obj);
		ASSERT((qid > 0) && (qid <= max_qid));

		qinfo = &qt->quorum_devices.list[qid-1];

		qinfo->qid = qid;

		votestr = clconf_obj_get_property(obj, "votecount");
		if (votestr == NULL) {
		    clconf_release_quorum_table(qt);
		    //
		    // SCMSGS
		    // @explanation
		    // Found the votecount field for the quorum device being
		    // incorrect while converting the quorum configuration
		    // information into quorum table.
		    // @user_action
		    // Check the quorum configuration information.
		    //
		    (void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: No valid votecount field for quorum"
			" device %d", qid);
		    delete syslog_msgp;
		    if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		    }
		    return (EINVAL);
		} else
		    qinfo->votes = (uint32_t)os::atoi(votestr);

		qinfo->gdevname = NULL;

		//
		// Read the global device name for the device.
		//
		propvalue = clconf_obj_get_property(obj, "gdevname");
		if (propvalue == NULL) {
		    clconf_release_quorum_table(qt);
		    //
		    // SCMSGS
		    // @explanation
		    // Found the gdevname field for the quorum device being
		    // incorrect while converting the quorum configuration
		    // information into quorum table.
		    // @user_action
		    // Check the quorum configuration information.
		    //
		    (void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"clconf: No valid gdevname field for quorum"
			" device %d", qid);
		    delete syslog_msgp;
		    if (cl_in == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
		    }
		    return (EINVAL);
		} else {
			qinfo->gdevname = os::strdup(propvalue);
			if (qinfo->gdevname == NULL) {
				clconf_release_quorum_table(qt);
				(void) syslog_msgp->log(SC_SYSLOG_WARNING,
				    MESSAGE, "clconf: No memory to read quorum "
				    "configuration table");
				delete syslog_msgp;
				if (cl_in == NULL) {
					clconf_obj_release((clconf_obj_t *)cl);
				}
				return (ENOMEM);
			}
		}

		//
		// Find nodes with enabled/configured paths to the device.
		//
		qinfo->nodes_with_enabled_paths = 0;
		qinfo->nodes_with_configured_paths = 0;
		uint_t num_configured_paths = 0;
		for (nid = 1; nid <= max_nid; nid++) {
			os::sprintf(pathnm, "path_%u", nid);
			propvalue = clconf_obj_get_property(obj, pathnm);
			if (propvalue == NULL)
				continue;
			/* Configured paths include disabled/enabled. */
			qinfo->nodes_with_configured_paths |= 1LL << (nid-1);
			num_configured_paths++;

			if (os::strcmp(propvalue, "enabled") == 0) {
				qinfo->nodes_with_enabled_paths |=
					1LL << (nid-1);
			}
		}

		//
		// Read the access mode for the device
		//
		qinfo->access_mode = NULL;
		propvalue = clconf_obj_get_property(obj, "access_mode");
		if (propvalue != NULL) {
			qinfo->access_mode = os::strdup(propvalue);
			if (qinfo->access_mode == NULL) {
				clconf_release_quorum_table(qt);
				//
				// SCMSGS
				// @explanation
				// When quorum configuration information
				// was converted into the quorum table,
				// memory was not allocated correctly
				// to store a field in the quorum device
				// information.
				// @user_action
				// Insufficient memory might lead to other
				// problems; free up memory if possible.
				//
				(void) syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "clconf: Memory allocation failure.");
				delete syslog_msgp;
				if (cl_in == NULL) {
					clconf_obj_release((clconf_obj_t *)cl);
				}
				return (ENOMEM);
			}
		}

		//
		// Read the type for the device.
		//
		propvalue = clconf_obj_get_property(obj, "type");
		if (propvalue != NULL) {
			qinfo->type = os::strdup(propvalue);
		} else {
			//
			// If access_mode is specified but type is not,
			// then we use the access_mode as the type too.
			//
			// When neither access_mode nor type is specified,
			// we use either SCSI2 or SCSI3 for backward
			// compatibility. In such a case, the number of
			// configured paths is taken into consideration
			// while deciding whether to use SCSI2 or SCSI3.
			//
			if (qinfo->access_mode != NULL) {
				// access_mode is specified, but type isn't
				qinfo->type = os::strdup(qinfo->access_mode);
			} else if (num_configured_paths == 2) {
				qinfo->type = os::strdup(QD_SCSI2);
			} else {
				qinfo->type = os::strdup(QD_SCSI3);
			}
		}

		if (qinfo->type == NULL) {
			// Memory allocation failure
			clconf_release_quorum_table(qt);
			(void) syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "clconf: Memory allocation failure.");
			delete syslog_msgp;
			if (cl_in == NULL) {
				clconf_obj_release((clconf_obj_t *)cl);
			}
			return (ENOMEM);
		}

		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);

	if (cl_in == NULL) {
		clconf_obj_release((clconf_obj_t *)cl);
	}

	delete syslog_msgp;
	return (0);
}

//
// Release the quorum table allocated through clconf_get_quorum_table().
//
void
clconf_release_quorum_table(quorum_table_t *qt)
{
	if (qt == NULL) {
		return;
	}
	if (qt->nodes.list != NULL) {
		delete [] qt->nodes.list;
	}
	if (qt->quorum_devices.list != NULL) {
		for (uint_t i = 0; i < qt->quorum_devices.listlen; i++) {
			if (qt->quorum_devices.list[i].gdevname != NULL) {
				delete [] qt->quorum_devices.list[i].gdevname;
			}
			if (qt->quorum_devices.list[i].type != NULL) {
				delete [] qt->quorum_devices.list[i].type;
			}
			if (qt->quorum_devices.list[i].access_mode != NULL) {
				delete []
				    qt->quorum_devices.list[i].access_mode;
			}
		}
		delete [] qt->quorum_devices.list;
	}
	delete qt;
}

//
// Get the quorum status from the quorum algorithm. A pointer to a pointer
// to the quorum_status_t is passed in. The function allocates the
// memory to hold the status struct. The caller must invoke
// cluster_release_quorum_status() to release the memory allocated by this
// function.
//
// Get the quorum status of nodes and quorum devices as reported
// by the quorum algorithm.
// Note that the information in the quorum algorithm object is the quorum
// information that was valid during the previous CMM reconfiguration.
//
int
cluster_get_quorum_status(quorum_status_t **qpp)
{
	return (cluster_get_quorum_status_common(qpp, B_FALSE));
}

//
// Get the quorum status from the quorum algorithm. A pointer to a pointer
// to the quorum_status_t is passed in. The function allocates the
// memory to hold the status struct. The caller must invoke
// cluster_release_quorum_status() to release the memory allocated by this
// function.
//
// Get the quorum status of nodes as reported by the quorum algorithm object;
// but for the quorum devices, check whether each quorum device
// is accessible or not at present.
// Since the quorum algorithm impl object holds quorum information
// that was true during the previous CMM reconfiguration,
// the quorum information for quorum devices returned in this case
// could be different from that of the quorum algorithm impl object.
//
int
cluster_get_immediate_quorum_status(quorum_status_t **qpp)
{
	return (cluster_get_quorum_status_common(qpp, B_TRUE));
}

//
// Get the quorum status from the quorum algorithm. A pointer to a pointer
// to the quorum_status_t is passed in. The function allocates the
// memory to hold the status struct. The caller must invoke
// cluster_release_quorum_status() to release the memory allocated by this
// function.
//
// If get_immediate_status is B_FALSE, then get the quorum status
// of nodes and quorum devices as reported by the quorum algorithm.
// Note that the information in the quorum algorithm object is the quorum
// information that was valid during the previous CMM reconfiguration.
//
// If get_immediate_status is B_TRUE, then get the quorum status
// of nodes as reported by the quorum algorithm object;
// but for the quorum devices, check whether each quorum device
// is accessible or not at present.
// Since the quorum algorithm impl object holds quorum information
// that was true during the previous CMM reconfiguration,
// the quorum information for quorum devices returned in this case
// could be different from that of the quorum algorithm impl object.
//
static int
cluster_get_quorum_status_common(
    quorum_status_t **qpp, boolean_t get_immediate_status)
{
	quorum::quorum_status_t		*qp;
	uint_t				i, j;
	Environment			e;
	quorum::quorum_algorithm_var	qm_v;

	(*qpp) = new quorum_status_t;

	if ((*qpp) == NULL)
		return (ENOMEM);

	(*qpp)->nodelist = NULL;
	(*qpp)->quorum_device_list = NULL;

#if (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)
	if (clconf_inside_virtual_cluster()) {
		//
		// If we are running in a zone cluster, then we get
		// a snapshot of the zone cluster membership using
		// membership subsystem APIs.
		//
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;
		char cl_name[CLUSTER_NAME_MAX];
		if (getzonenamebyid(getzoneid(), cl_name,
		    CLUSTER_NAME_MAX) < 0) {
			os::warning("could not get current zone name");
			return (EINVAL);
		}
		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		ASSERT(!CORBA::is_nil(membership_api_v));
		membership_api_v->get_cluster_membership(
		    membership, seqnum, cl_name, e);
		//
		// Since we are inside a zone cluster and querying
		// membership for our own cluster, there should be
		// no exception raised.
		//
		ASSERT(e.exception() == NULL);
		if (e.exception()) {
			os::warning(
			    "could not get current cluster membership\n");
			return (EINVAL);
		}

		uint_t current_votes = 0;
		uint_t total_configured_votes = 0;
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (orb_conf::node_configured(nid)) {
				++total_configured_votes;
			}
			if (membership.members[nid] != INCN_UNKNOWN) {
				// Node is online in virtual cluster
				++current_votes;
			}
		}
		ASSERT(current_votes <= total_configured_votes);
		(*qpp)->current_votes = current_votes;
		(*qpp)->configured_votes = total_configured_votes;
		(*qpp)->votes_needed_for_quorum = 1;
		(*qpp)->num_nodes = total_configured_votes;
		(*qpp)->num_quorum_devices = 0;

		(*qpp)->nodelist = new quorum_node_status_t[(*qpp)->num_nodes];
		(*qpp)->quorum_device_list =
		    new quorum_device_status_t[(*qpp)->num_quorum_devices];
		if (((*qpp)->nodelist == NULL) ||
		    ((*qpp)->quorum_device_list == NULL)) {
			cluster_release_quorum_status((*qpp));
			return (ENOMEM);
		}

		nodeid_t nid;
		for (i = 0, nid = 1; nid <= NODEID_MAX; nid++) {
			if (!orb_conf::node_configured(nid)) {
				continue;
			}
			(*qpp)->nodelist[i].nid = nid;
			for (j = 0; j < MHIOC_RESV_KEY_SIZE; j++) {
				(*qpp)->nodelist[i].reservation_key.key[j] = 0;
			}
			(*qpp)->nodelist[i].votes_configured = 1;
			if (membership.members[nid] != INCN_UNKNOWN) {
				(*qpp)->nodelist[i].state = QUORUM_STATE_ONLINE;
			} else {
				(*qpp)->nodelist[i].state
				    = QUORUM_STATE_OFFLINE;
			}
			i++;
		}
		ASSERT(i == (*qpp)->num_nodes);
		return (0);
	}
#endif	// (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)

	qm_v = cmm_ns::get_quorum_algorithm(NULL);

	if (get_immediate_status) {
		//
		// Get the immediate/current quorum status.
		// This IDL method will raise a retry_needed exception
		// if a CMM reconfiguration is in progress and hence
		// the IDL method could not get the quorum status.
		// In that case, sleep for the globally specified sleep period
		// (clconf_sleep_period_for_quorum_status_query - in seconds)
		// and retry again.
		//
		while (1) {
			qm_v->quorum_get_immediate_status(qp, e);
			CORBA::Exception *exp = e.exception();
			if (exp == NULL) {
				// Success
				break;
			}
			if (quorum::retry_needed::_exnarrow(exp)) {
				//
				// Clear exception and retry after sleeping for
				// the number of seconds as specified by
				// the global variable for the sleep period.
				//
				e.clear();
				unsigned int sleep_period =
				    clconf_sleep_period_for_quorum_status_query;
				if (sleep_period < 1) {
					// Sleep for at least 1 second
					sleep_period = 1;
				}
				// Sleep for 'sleep_period' seconds
				os::usecsleep(
				    (os::usec_t)sleep_period * MICROSEC);
			} else {
				//
				// Got an unknown exception that is not raised
				// directly by the IDL method.
				//
				e.clear();
				delete (*qpp);
				*qpp = NULL;
				return (EINVAL);
			}
		}

	} else {
		//
		// Get the quorum status as recorded in
		// the quorum algorithm object.
		//
		qm_v->quorum_get_status(qp, e);
		if (e.exception()) {
			e.clear();
			delete (*qpp);
			*qpp = NULL;
			return (EINVAL);
		}
	}

	(*qpp)->current_votes = qp->current_votes;
	(*qpp)->configured_votes = qp->configured_votes;
	(*qpp)->votes_needed_for_quorum = qp->votes_needed_for_quorum;
	(*qpp)->num_nodes = qp->nodelist.length();
	(*qpp)->num_quorum_devices = qp->quorum_device_list.length();

	(*qpp)->nodelist = new quorum_node_status_t[(*qpp)->num_nodes];
	(*qpp)->quorum_device_list =
		new quorum_device_status_t[(*qpp)->num_quorum_devices];
	if (((*qpp)->nodelist == NULL) ||
	    ((*qpp)->quorum_device_list == NULL)) {
		cluster_release_quorum_status((*qpp));
		return (ENOMEM);
	}

	for (i = 0; i < (*qpp)->num_nodes; i++) {
		(*qpp)->nodelist[i].nid = qp->nodelist[i].config.nid;
		for (j = 0; j < MHIOC_RESV_KEY_SIZE; j++)
			(*qpp)->nodelist[i].reservation_key.key[j] =
				qp->nodelist[i].config.reservation_key.key[j];
		(*qpp)->nodelist[i].votes_configured =
			qp->nodelist[i].config.votes_configured;
		(*qpp)->nodelist[i].state =
			(quorum_state_t)qp->nodelist[i].state;
	}

	for (i = 0; i < (*qpp)->num_quorum_devices; i++) {
		const char *name = (const char *)qp->quorum_device_list[i].
			config.gdevname;

		(*qpp)->quorum_device_list[i].qid =
			qp->quorum_device_list[i].config.qid;
		(*qpp)->quorum_device_list[i].votes_configured =
			qp->quorum_device_list[i].config.votes_configured;
		(*qpp)->quorum_device_list[i].gdevname = os::strdup(name);
		(*qpp)->quorum_device_list[i].nodes_with_enabled_paths =
			qp->quorum_device_list[i].config.
			nodes_with_enabled_paths;
		(*qpp)->quorum_device_list[i].nodes_with_configured_paths =
			qp->quorum_device_list[i].config.
			nodes_with_configured_paths;
		(*qpp)->quorum_device_list[i].state =
			(quorum_state_t)qp->quorum_device_list[i].state;
		(*qpp)->quorum_device_list[i].reservation_owner =
			qp->quorum_device_list[i].reservation_owner;
	}

	delete qp;
	return (0);
}

//
// Release the struct allocated through cluster_get_quorum_status().
//
void
cluster_release_quorum_status(quorum_status_t *qp)
{
	if (qp == NULL)
		return;
	if (qp->nodelist != NULL)
		delete [] qp->nodelist;
	for (uint_t i = 0; i < qp->num_quorum_devices; i++) {
		if (qp->quorum_device_list[i].gdevname) {
			delete [] qp->quorum_device_list[i].gdevname;
		}
	}
	if (qp->quorum_device_list != NULL)
		delete [] qp->quorum_device_list;
	delete qp;
}

//
// Remove a quorum device.
//
clconf_errnum_t
clconf_quorum_remove(char *qd_pathname, nodeid_t connected_nodeid,
    char *type, clconf_obj_t *qdev)
{
	quorum::device_type_registry_var tr_v;
	Environment e;

	//
	// Construct the properties list. We need this because remove
	// is called by scconf after all information has been removed
	// from the CCR. We provide that information through a property
	// list instead.
	//
	quorum::qd_property_seq_t	qdprops;
	clconf_iter_t *priterp = clconf_obj_get_properties(qdev);
	clconf_obj_t *tmp_obj;
	uint_t i = 0;
	if (priterp != NULL) {
		for (; (tmp_obj = clconf_iter_get_current(priterp)) != NULL;
		    clconf_iter_advance(priterp)) {
			qdprops.length(i+1);
			qdprops[i].name = os::strdup(clconf_obj_get_idstring(
			    tmp_obj));
			qdprops[i].value = os::strdup(clconf_obj_get_property(
			    qdev, qdprops[i].name));
			i++;
		}
		clconf_iter_release(priterp);
	}

	tr_v = cmm_ns::get_device_type_registry(connected_nodeid);
	if (CORBA::is_nil(tr_v)) {
		return (CL_QD_REMOVE_FAILED);
	}

	quorum::quorum_device_type_var qd_v = tr_v->get_quorum_device(type, e);

	if (CORBA::is_nil(qd_v) || e.exception()) {
		e.clear();
		return (CL_QD_REMOVE_FAILED);
	}

	quorum::quorum_error_t q_error =
		qd_v->quorum_remove(qd_pathname, qdprops, e);
	if ((e.exception()) || (q_error == quorum::QD_EIO)) {
		e.clear();
		return (CL_QD_REMOVE_FAILED);
	}

	return (CL_NOERROR);
}

//
// Scrub the quorum device in question.
//
clconf_errnum_t
clconf_quorum_device_scrub(char *qd_pathname, nodeid_t connected_nodeid,
    char *type, clconf_obj_t *qdev)
{
	quorum::device_type_registry_var tr_v;
	Environment e;
	clconf_errnum_t	error = CL_NOERROR;

	//
	// To scrub the disk, obtain a handle to the
	// type_registry object on connected_nodeid (one of the
	// ported nodes), then get a reference to the disk,
	// and call its quorum_scrub()
	// method.
	//

	tr_v = cmm_ns::get_device_type_registry(connected_nodeid);
	if (CORBA::is_nil(tr_v)) {
		return (CL_NO_DEV_REGISTRY);
	}

	quorum::quorum_device_type_var qd_v = tr_v->get_quorum_device(type, e);
	ASSERT(e.exception() == NULL);
	e.clear();
	if (CORBA::is_nil(qd_v)) {
		return (CL_QD_NOT_REGISTERED);
	}

	//
	// Construct the properties list. We need this because scrub
	// is called by scconf before all information has been written
	// to the CCR. We provide that information through a property
	// list instead.
	//
	quorum::qd_property_seq_t	qdprops;
	clconf_iter_t *priterp = clconf_obj_get_properties(qdev);
	clconf_obj_t *tmp_obj;
	uint_t i = 0;
	if (priterp != NULL) {
		for (; (tmp_obj = clconf_iter_get_current(priterp)) != NULL;
		    clconf_iter_advance(priterp)) {
			qdprops.length(i+1);
			qdprops[i].name = os::strdup(clconf_obj_get_idstring(
			    tmp_obj));
			qdprops[i].value = os::strdup(clconf_obj_get_property(
			    qdev, qdprops[i].name));
			i++;
		}
		clconf_iter_release(priterp);
	}

	//
	// Call open using the reference.
	// This will also perform any necessary initialization.
	// Passing "true" to quorum_open will scrub the device.
	//
	quorum::quorum_error_t q_error =
	    qd_v->quorum_open(qd_pathname, true, qdprops, e);
	ASSERT(e.exception() == NULL);
	e.clear();
	if (q_error != quorum::QD_SUCCESS) {
		//
		// Call quorum_close() that will cleanup any complete
		// or partial actions done by quorum_open().
		// We ignore the return value, as we do not need to do anything
		// further after trying to close the quorum device object.
		//
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);
		return (CL_QD_OPEN_VALIDATION_FAIL);
	}

	//
	// Try reading keys from the device
	// This is a sanity check to exercise device type
	// specific functionality and make sure
	// quorum device works at a minimum level
	//
	Environment			env_rr;
	quorum::reservation_list_t	resvlist;

	resvlist.listsize = NODEID_MAX;
	resvlist.listlen = 0;
	resvlist.keylist.length(NODEID_MAX);

	if (resvlist.keylist.length() != NODEID_MAX) {
		// Memory allocation failure

		//
		// Call quorum_close() that will cleanup any complete
		// or partial actions done by quorum_open().
		// We ignore the return value, as we do not need to do anything
		// further after trying to close the quorum device object.
		//
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);

		return (CL_MEMALLOC_FAIL);
	}

	quorum::quorum_error_t rr_error = quorum::QD_SUCCESS;

	rr_error = qd_v->quorum_read_reservations(resvlist, env_rr);
	ASSERT(env_rr.exception() == NULL);
	env_rr.clear();
	if (rr_error == quorum::QD_EIO) {
		//
		// Call quorum_close() that will cleanup any complete
		// or partial actions done by quorum_open().
		// We ignore the return value, as we do not need to do anything
		// further after trying to close the quorum device object.
		//
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);

		return (CL_QD_READ_RES_VALIDATION_FAIL);
	}
	rr_error = quorum::QD_SUCCESS;
	resvlist.listlen = 0;
	rr_error = qd_v->quorum_read_keys(resvlist, env_rr);
	ASSERT(env_rr.exception() == NULL);
	env_rr.clear();
	if (rr_error == quorum::QD_EIO) {
		//
		// Call quorum_close() that will cleanup any complete
		// or partial actions done by quorum_open().
		// We ignore the return value, as we do not need to do anything
		// further after trying to close the quorum device object.
		//
		(void) qd_v->quorum_close(e);
		// quorum_close() does not raise any exceptions
		ASSERT(e.exception() == NULL);

		return (CL_QD_READ_KEYS_VALIDATION_FAIL);
	}

	// Close the disk.
	q_error = qd_v->quorum_close(e);
	e.clear(); // Ignore exceptions from close.

	return (error);
}


#ifndef _KERNEL_ORB
int
clconf_lib_init()
{
	int err;
	if ((err = ORB::initialize()) != 0) {
		return (err);
	}

	// initialize clconf
	if ((err = cl_current_tree::initialize()) != 0) {
		return (err);
	}

	// read in clconf tree
	if ((err = clconf_ccr::initialize()) != 0) {
		return (err);
	}

	if ((err = user_tm::initialize()) != 0) {
		return (err);
	}

	return (0);
}

void
clconf_lib_shutdown()
{
	ORB::shutdown();
}
#else
#ifndef _KERNEL
int
clconf_lib_init()
{
	return (user_tm::initialize());
}
#endif

#endif

clconf_errnum_t
clconf_register_ccr_callback(clconf_ccr_callback_handler hdlr)
{
	return (clconf_ccr::the().register_ccr_callback(hdlr));
}

clconf_errnum_t
clconf_unregister_ccr_callback()
{
	return (clconf_ccr::the().unregister_ccr_callback());
}

/*
 * Get values for ccr entries
 */
#ifdef _KERNEL_ORB

/*
 * This function exists only in the kernel because we dont want to link its
 * users with the libclconf library.  The user (nsswitch) cannot be linked with
 * libclconf, because we dont make a libclconf that is 64 bit.  Therefore I've
 * left the user-level implementation of this function in
 * cluster/src/lib/nsswitch/cluster/common/nss-cluster.c and the kernel level
 * implmenetation in clconf.
 */
clconf_errnum_t
clconf_get_ccr_entry(char *table_name, char *entry_name,
	char *outbuf, size_t outbuf_len)
{
	/*
	 * Executing in the kernel
	 */

	Environment	e;
	char		*value	= NULL;
	char		*cl_namep = NULL;

	cl_namep = get_clname_for_clconf();
	if (cl_namep == NULL) {
		return (CL_BADNAME);
	}

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		delete [] cl_namep;
		return (CL_BADNAME);
	}
	ASSERT(!CORBA::is_nil(obj));

	ccr::directory_var ccr_dir = ccr::directory::_narrow(obj);
	ASSERT(!CORBA::is_nil(ccr_dir));

	ccr::readonly_table_var table_ref =
	    ccr_dir->lookup_zc(cl_namep, table_name, e);
	if (e.exception()) {
		delete [] cl_namep;
		return (CL_BADNAME);
	}
	ASSERT(!CORBA::is_nil(table_ref));

	value = table_ref->query_element(entry_name, e);
	if (e.exception()) {
		delete value;
		delete [] cl_namep;
		return (CL_BADNAME);
	}

	/* Copy the returned value to the passed in buffer */
	(void) strncpy(outbuf, value, outbuf_len - 1);
	outbuf[outbuf_len - 1] = '\0';

	delete value;
	delete [] cl_namep;
	return (CL_NOERROR);
}

/*
 * Returns information about the children of a specified node.
 * The node is identified by the first three arguments.
 * path_len		The number of hops in the clconf tree from the
 *			root node to reach the specified node
 * path_node_types	Node types on the way to the node in question
 * path_node_indices	Node indices on the way to the node in question
 * The fourth argument is the child type of interest.
 * The number of children of the above type and the maximum index of
 * any child is retuned in the integers pointed to by the last two
 * arguments.
 *
 * As an example, to obtain information about the number of ports on
 * node 3's adapter 2, the first four arguments should be passed as:
 * path_len = 2, path_node_types = {CL_NODE, CL_ADAPTER},
 * path_node_indices = {3, 2}, child_type = CL_PORT
 */
clconf_errnum_t
clconf_get_clconf_child_stats(
    int			path_len,
    clconf_objtype_t	*path_node_types,
    int			*path_node_indices,
    clconf_objtype_t	child_type,
    int			*child_count_p,
    int			*max_child_index_p)
{
	clconf_cluster_t	*cl;
	clconf_obj_t		*obj;
	clconf_siter_t		siter;
	clconf_iter_t		*iter;
	int			child_count;
	int			max_child_index;

	if (path_len < 0 || path_len > MAX_CLCONF_PATH_LEN) {
		return (CL_BADNAME);
	}

	/* First a hold on the cluster object */
	cl = clconf_cluster_get_current();
	if (cl == NULL) {
		return (CL_NO_CLUSTER);
	}

	/* Traverse the clconf tree until we reach the node of interest */
	obj = (clconf_obj_t *)cl;
	for (int i = 0; i < path_len; i++) {
		obj = clconf_obj_get_child(obj, path_node_types[i],
		    path_node_indices[i]);
		if (obj == NULL) {
			clconf_obj_release((clconf_obj_t *)cl);
			return (CL_BADNAME);
		}
	}

	/* Obtain the list of children of the desired type */
	iter = clconf_siter_init(&siter, obj, child_type);
	if (iter == NULL) {
		clconf_obj_release((clconf_obj_t *)cl);
		return (CL_BADNAME);
	}

	/*
	 * Iterate over the children list to count the children and
	 * to compute the maximum child id
	 */
	child_count	= 0;
	max_child_index	= 0;
	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		int id = clconf_obj_get_id(obj);
		if (id > max_child_index) {
			max_child_index = id;
		}
		child_count++;
		clconf_iter_advance(iter);
	}

	*child_count_p		= child_count;
	*max_child_index_p	= max_child_index;

	/* Free the clconf tree, static iterator not to be freed */
	clconf_obj_release((clconf_obj_t *)cl);

	return (CL_NOERROR);
}
#endif

/*
 * Get the network address for the cluster. This is the private net number
 * assigned to the cluster.
 */
static struct in_addr_solaris	pvtnet_addr;

const struct in_addr *
clconf_get_cluster_network_addr()
{
	const char *ccrent_private_net_number;
	clconf_cluster_t *cl;

	if (pvtnet_addr._S_un._S_un_b.s_b1 == 0) /* first time ? */ {
		/*
		 * read this info from the ccr
		 */
		cl = clconf_cluster_get_current();
		ccrent_private_net_number =
		    clconf_obj_get_property((clconf_obj_t *)cl,
		    "private_net_number");
		clconf_obj_release((clconf_obj_t *)cl);
		if (ccrent_private_net_number == NULL) {
			/*
			 * Use a default value
			 */
			ccrent_private_net_number = CL_DEFAULT_NET_NUMBER;
		}
		ASSERT(ccrent_private_net_number != NULL);

		pvtnet_addr._S_un._S_addr =
			    os::inet_addr(ccrent_private_net_number);
		if (pvtnet_addr._S_un._S_addr == (in_addr_t)(-1)) {
			pvtnet_addr._S_un._S_addr = 0;
			return (NULL);
		}
	}

	ASSERT(pvtnet_addr._S_un._S_addr > 0);
	return ((const struct in_addr *)&pvtnet_addr);
}

/*
 * Get the logical IP address for the specified node. This is
 * determined as a property of the node.
 */
int
clconf_get_user_ipaddr(nodeid_t nodeid, struct in_addr *addr)
{
	const char 		*ccrent_user_ipaddr = NULL;
	clconf_cluster_t 	*cl;
	const struct in_addr	*mynet = NULL;

	if (nodeid < 1 || nodeid > NODEID_MAX)
		return (-1);

	/*
	 * read the userip network number info from the ccr
	 */
	if (flex_ip_address()) {
		cl = clconf_cluster_get_current();
		ccrent_user_ipaddr = clconf_obj_get_property(
				(clconf_obj_t *)cl,
				"private_user_net_number");
		clconf_obj_release((clconf_obj_t *)cl);
		if (ccrent_user_ipaddr == NULL) {
			return (-1);
		}

		// Convert the host part to host order for bitwise
		// operations
		addr->_S_un._S_addr =
			os::inet_addr(ccrent_user_ipaddr) | ntohl(nodeid);
		if (addr->_S_un._S_addr == (in_addr_t)(-1)) {
			addr->_S_un._S_addr = 0;
			return (-1);
		}

		return (0);
	} else {
		mynet = clconf_get_cluster_network_addr();
		if (mynet == NULL)
			return (-1);

		addr->_S_un._S_un_w.s_w1 = mynet->_S_un._S_un_w.s_w1;
		addr->_S_un._S_un_b.s_b3 = LOGICAL_PERNODE_B3;
		addr->_S_un._S_un_b.s_b4 = (uint8_t)nodeid;
		return (0);
	}

}

/*
 * Get the logical base IP address
 */
int
clconf_get_user_network(struct in_addr *addr)
{
	const char 		*ccrent_user_ipaddr = NULL;
	clconf_cluster_t 	*cl;
	const struct in_addr	*mynet = NULL;

	/*
	 * read the userip network number info from the ccr
	 */
	if (flex_ip_address()) {
		cl = clconf_cluster_get_current();
		ccrent_user_ipaddr = clconf_obj_get_property(
				(clconf_obj_t *)cl,
				"private_user_net_number");
		clconf_obj_release((clconf_obj_t *)cl);
		if (ccrent_user_ipaddr == NULL) {
			return (-1);
		}

		// Convert the host part to host order
		addr->_S_un._S_addr = os::inet_addr(ccrent_user_ipaddr);
		if (addr->_S_un._S_addr == (in_addr_t)(-1)) {
			addr->_S_un._S_addr = 0;
			return (-1);
		}

		return (0);
	} else {
		mynet = clconf_get_cluster_network_addr();
		if (mynet == NULL)
			return (-1);

		addr->_S_un._S_un_w.s_w1 = mynet->_S_un._S_un_w.s_w1;
		addr->_S_un._S_un_b.s_b3 = LOGICAL_PERNODE_B3;
		addr->_S_un._S_un_b.s_b4 = (uint8_t)0;
		return (0);
	}

}

/*
 * Get the netmask for the pernode IP addresses.
 */
int
clconf_get_user_netmask(struct in_addr *addr)
{
	const char *ccrent_user_netmask;
	clconf_cluster_t *cl;

	if (flex_ip_address()) {
		/*
		 * read the userip network number info from the ccr
		 */
		cl = clconf_cluster_get_current();
		ccrent_user_netmask =
			clconf_obj_get_property((clconf_obj_t *)cl,
					    "private_user_netmask");
		clconf_obj_release((clconf_obj_t *)cl);
		if (ccrent_user_netmask == NULL) {
			return (-1);
		}
		addr->_S_un._S_addr = os::inet_addr(ccrent_user_netmask);
		if (addr->_S_un._S_addr == (in_addr_t)(-1)) {
			addr->_S_un._S_addr = 0;
			return (-1);
		}
		return (0);
	} else {
		addr->_S_un._S_addr = htonl(
		    (uint32_t)LOGICAL_PERNODE_NETWORK_NETMASK);
		return (0);
	}
}

/*
 * Determine whether we are using the flexible IP Addressing scheme
 */
int
flex_ip_address()
{
	const char *ccrent_private_subnet_netmask;
	clconf_cluster_t *cl;

	cl = clconf_cluster_get_current();
	ccrent_private_subnet_netmask =
	    clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_subnet_netmask");
	clconf_obj_release((clconf_obj_t *)cl);
	if (ccrent_private_subnet_netmask == NULL)
		return (0);
	else
		return (1);
}


#ifdef	_KERNEL
/*
 * Map the specified logical ip to the physical ip of the adapter that's
 * hosting the logical ip.  If we can't find the logical_ip_address in our
 * mapping then we return the logical_ip_address
 */
in_addr_t
clconf_get_physical_ip(in_addr_t logical_ip_address)
{
	if (ipconf_is_initialized_funcp()) {
		return (ipconf_get_physical_ip_funcp(logical_ip_address));
	} else {
		return (logical_ip_address);
	}
}
#endif	// _KERNEL

#ifndef _KERNEL


// is_path returns true if the name passed as the argument is a path.
// Otherwise it returns false.
// A path is defined as one which contains the string ".adapters." twice
// and as the one which does not end with an 'l' or 'h'

bool
is_path(const char *name)
{
	size_t end = strlen(name)-1;
	const char *tmp;

	if (((tmp = strstr(name, ".adapters.")) != NULL) &&
	    (strstr(tmp, ".adapters.") != NULL) &&
	    (name[end] != 'l') && (name[end] != 'h'))
		return (true);
	return (false);
}

// is_pathend returns true if the name passed as the argument is a pathend.
// Otherwise it returns false.
// A pathend is defined as one which contains the string ".adapters." twice
// and as the one which ends with ".l" or ".h"
bool
is_pathend(const char *name)
{
	const char *tmp;
	size_t end = strlen(name)-1;

	if (((tmp = strstr(name, ".adapters.")) != NULL) &&
	    (strstr(tmp, ".adapters.") != NULL) && (name[end-1] == '.') &&
	    ((name[end] == 'l') || (name[end] == 'h')))
		return (true);
	return (false);
}

// This is an internal function used, rightnow, only by
// cladmin_get_component_state routine to construct a component state.
static cladmin_component_state_t *
make_state(sc_state_code_t state, char *ss, char *type, const char *iname)
{
	cladmin_component_state_t *cmp_state = new cladmin_component_state_t;

	cmp_state->int_name = os::strdup(iname);
	cmp_state->type = os::strdup(type);
	cmp_state->state_string = os::strdup(ss);
	cmp_state->state = state;
	return (cmp_state);
}

//
// User level API for getting status of component
//
// Will get the status of a named component (name is both type.name)
//
cladmin_component_state_t *
cladmin_get_component_state(const char *cmp_name)
{
	// Searching from left to right, find the token *.nodes.
	// the following token has to be the nodeid that owns the component
	// we want status of

	const char	*nodes_token	= NULL;
	const char	*begin		= NULL;
	const char	*end		= NULL;
	size_t	len		= 0;
	char	*nodeid_str	= NULL;
	char			csr_name[32];

	nodes_token = strstr(cmp_name, ".nodes.");
	if (nodes_token == NULL)
		return (NULL);

	begin = nodes_token + strlen(".nodes.");
	end = strchr(begin, '.');

	if ((begin == end) || (end == NULL))
		return (NULL);

	// If the component is a path
	if (is_path(cmp_name)) {
		char *pe1 = new char[os::strlen(cmp_name)+3];
		char *pe2 = new char[os::strlen(cmp_name)+3];
		cladmin_component_state_t	*pe1_state, *pe2_state, *pstate;

		// Construct the two pathend names associated with this path
		os::sprintf(pe1, "%s.l", cmp_name);
		os::sprintf(pe2, "%s.h", cmp_name);

		// Get the state of each path
		pe1_state = cladmin_get_component_state(pe1);
		pe2_state = cladmin_get_component_state(pe2);

		// If state of both pathends is NULL then the state
		// of path is returned as NULL. If only one of them
		// is NULL then state of path is returned as SC_STATE_FAULTED.
		// If the two pathend states match, then
		// state of the path is set to that state, otherwise
		// of the path is set to SC_STATE_WAIT.

		if ((pe1_state == NULL) && (pe2_state == NULL)) {
			pstate = NULL;
		} else if ((pe1_state == NULL) || (pe2_state == NULL)) {
			pstate = make_state(SC_STATE_FAULTED, "faulted",
				SC_SYSLOG_TRANSPORT_PATH_TAG, cmp_name);
		} else {
		    if (pe1_state->state == pe2_state->state)
			pstate = make_state(pe1_state->state,
			    pe1_state->state_string, pe1_state->type, cmp_name);
		    else
			pstate = make_state(SC_STATE_WAIT, "waiting",
				    SC_SYSLOG_TRANSPORT_PATH_TAG, cmp_name);
		}
		cladmin_free_component_state(pe1_state);
		cladmin_free_component_state(pe2_state);
		delete [] pe1;
		delete [] pe2;
		return (pstate);
	}

	len = (size_t)(end - begin);
	nodeid_str = new char[len + 1];
	if (nodeid_str == NULL) {
		return (NULL);
	}
	(void) strncpy(nodeid_str, begin, len);
	nodeid_str[len] = '\0';

	Environment			e;
	component_state::registry_var	csr_ref;
	naming::naming_context_var	ctxp		= ns::root_nameserver();
	cladmin_component_state_t	*cmp_state	= NULL;
	component_state::component_state_t_var	state_ref;

	// Get the csr for the specified node
	if (os::snprintf(csr_name, 32UL, "csr-node-%s", nodeid_str) >= 31) {
		csr_name[31] = '\0';
	}
	CORBA::Object_var obj_ref = ctxp->resolve(csr_name, e);
	if (e.exception()) {
		e.clear();
		return (NULL);
	}

	ASSERT(!CORBA::is_nil(obj_ref));
	csr_ref = component_state::registry::_narrow(obj_ref);

	ASSERT(!CORBA::is_nil(csr_ref));
	csr_ref->get_state(cmp_name, state_ref, e);
	if (e.exception()) {
		e.clear();
		return (NULL);
	}

	// Create a state structure and copy into it the result of the query
	cmp_state = make_state((sc_state_code_enum)(*state_ref).state,
				(*state_ref).state_string,
				(*state_ref).component_type,
				(*state_ref).component_name);
	return (cmp_state);
}

// extract_node_id extracts nodeid number from the pathname.
// epoint argument is used to determine whether to extract the low nodeid
// or the high nodeid. If the epoint is 'l' that means we already have low
// nodeid and we need to extract the high node id and vice-versa.

int
extract_node_id(char *name, char epoint)
{
	char *begin, *end;
	int oth_nodeid;

	// Extract low node id if the current node id is high
	begin = strstr(name, ".nodes.");
	begin = begin + strlen(".nodes.");

	// If the current end point node id is low, then get the high node id
	if (epoint == 'l') {
		begin = strstr(begin, ".nodes.");
		begin = begin + strlen(".nodes.");
	}
	end = strstr(begin, ".");
	char *nodeid_str = new char [(uint_t)(end-begin+1)];
	ASSERT(nodeid_str != NULL);
	(void) strncpy(nodeid_str, begin, (size_t)(end-begin));
	nodeid_str[end-begin] = '\0';
	oth_nodeid = os::atoi(nodeid_str);
	delete []nodeid_str;
	return (oth_nodeid);
}

// get_path_state returns a pointer to the state structure of the other
// pathend.
// arguments passed are
// name - name of the path constructed from the known path end.
// ostate_list contains the list of components, one of which is the
// other pathend state.
//
// get_path_state searches the list to find the other pathend by comparing
// the component names in the ostate_list with the name passed as the first
// argument.

component_state::component_state_t *
get_path_state(char *name, component_state::component_state_list_t *ostate_list)
{
	uint_t oth_state_index;

	for (oth_state_index = 0; oth_state_index < (*ostate_list).length();
	    oth_state_index++) {
		component_state::component_state_t *state =
		    &((*ostate_list)[oth_state_index]);

		char *oth_node_name = (*state).component_name;
		if ((is_pathend(oth_node_name)) &&
		    (strncmp(name, oth_node_name, strlen(name)) == 0))
			return (state);
	}
	return (NULL);
}

// Get a list of all state from each proxy in the cluster
// This is done by querying each csr for its list of proxies
cladmin_component_state_list_t *
cladmin_get_component_state_list()
{
	Environment			e;
	clconf_iter_t			*iter	= NULL;
	clconf_cluster_t		*cl	= NULL;
	clconf_obj_t			*obj	= NULL;
	int				nodeid	= 0;
	naming::naming_context_var	ctxp	= ns::root_nameserver();
	component_state::registry_var	csr_ref;
	char				csr_name[32];
	uint_t				cmp_count	= 0;
	cladmin_component_state_list_t	*cmp_list	= NULL;
	component_state::component_state_list_t_var  vstate_list[NODEID_MAX+1];
	uint_t				state_index = 0;

	// Get the list of current nodes in cluster
	cl = clconf_cluster_get_current();
	iter = clconf_cluster_get_nodes(cl);

	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		clconf_iter_advance(iter);

		CL_PANIC(clconf_obj_get_objtype(obj) == CL_NODE);
		nodeid = clconf_obj_get_id(obj);
		if (os::snprintf(csr_name, 32UL, "csr-node-%u", nodeid) >= 31) {
			csr_name[31] = '\0';
		}

		CORBA::Object_var obj_ref = ctxp->resolve(csr_name, e);
		if (e.exception()) {
			// If there is no csr on a specific node, continue
			e.clear();
			continue;
		}

		ASSERT(!CORBA::is_nil(obj_ref));
		csr_ref = component_state::registry::_narrow(obj_ref);

		ASSERT(!CORBA::is_nil(csr_ref));
		csr_ref->get_state_list(vstate_list[nodeid], e);
		if (e.exception()) {
			e.clear();
			continue;
		}

		// Keep track of how many proxies we have in each node
		// (only have the state of the proxy)
		cmp_count += (*(vstate_list[nodeid])).length();
	}

	clconf_iter_release(iter);

	cmp_list = new cladmin_component_state_list_t;
	cmp_list->list = new cladmin_component_state_t[cmp_count];
	cmp_list->listlen = 0;

	// Go back through the list of state sequences and copy every
	// entry we find.
	for (nodeid = 0; nodeid <= NODEID_MAX; nodeid++) {
		component_state::component_state_list_t	*state_list =
		    vstate_list[nodeid];

		if (state_list == NULL)
			continue;

		for (state_index = 0; state_index < (*state_list).length();
		    state_index++) {
			component_state::component_state_t *state =
			    &((*state_list)[state_index]);

			cladmin_component_state_t *cmp_state =
			    &(cmp_list->list[cmp_list->listlen]);

			char *_name = (*state).component_name;
			const char *_type = (*state).component_type;
			const char *_state_string = (*state).state_string;
			sc_state_code_t _state =
			    (sc_state_code_t)(*state).state;

			size_t end = strlen(_name)-1;
			const char *wait_str = "waiting";
			const char *fault_str = "faulted";

			if (*_name == '\0')
				continue;

			if (is_pathend(_name)) {
				char epoint = _name[end];
				_name[end-1] = '\0';

				// epoint contains either 'l' or 'h'
				// It is asserted by is_pathend()
				int oth_nodeid = extract_node_id(_name, epoint);

				component_state::component_state_list_t
				    *oth_state_list = vstate_list[oth_nodeid];
				component_state::component_state_t *
				    oth_state = NULL;

				if (oth_state_list != NULL) {
					// get_path_state state structure of the
					// other pathend.
					oth_state = get_path_state(_name,
					    oth_state_list);
				}

				if (oth_state != NULL) {
				    if ((*oth_state).state != _state)
						_state = SC_STATE_WAIT;

				    // make the other pathend name NULL
				    // so that we will skip it when we
				    // encounter it again.
				    (void)
				    os::strcpy((*oth_state).component_name, "");
				} else
					_state = SC_STATE_FAULTED;

				if (_state == SC_STATE_WAIT)
					_state_string = wait_str;
				else if (_state == SC_STATE_FAULTED)
					_state_string = fault_str;
			}

			cmp_state->int_name = os::strdup(_name);
			cmp_state->type = os::strdup(_type);
			cmp_state->state_string = os::strdup(_state_string);
			cmp_state->state = _state;
			cmp_list->listlen++;
		}
	}

	return (cmp_list);
}

//
// Helper to delete the state structure
//
void
cladmin_free_component_state(cladmin_component_state_t *state_ptr)
{
	if (state_ptr == NULL)
		return;

	delete []state_ptr->int_name;
	delete []state_ptr->type;
	delete []state_ptr->state_string;
	delete state_ptr;
}

//
// Helper to delete the state structure list
void
cladmin_free_component_state_list(cladmin_component_state_list_t *list_ptr)
{
	if (list_ptr == NULL)
		return;

	for (uint_t ind = 0; ind < list_ptr->listlen; ind++) {
		delete []list_ptr->list[ind].int_name;
		delete []list_ptr->list[ind].type;
		delete []list_ptr->list[ind].state_string;
	}

	delete []list_ptr->list;
	delete list_ptr;
}

#endif // !_KERNEL

// Read the cluster shutdown flag from the cmm module of the specified node.
// cluster_shutdown_flag holds the expected value.
//
// Will return:
//    o EIO    if an exception or an error is catched
//    o ENOENT if the specified node does not exist
//    o EACCES if the cluster is not ready to perform this operation
//    o 0 otherwise
int
clconf_get_cluster_shutdown(nodeid_t nodeid, boolean_t *cluster_shutdown_flag)
{
	Environment e;
#ifndef _KERNEL
	os::sc_syslog_msg	*syslog_msgp;
	char			nodename[8];
	os::sprintf(nodename, "%u", orb_conf::local_nodeid());
	syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
	    nodename, NULL);
#endif // !_KERNEL
	cmm::control_ptr ccp = cmm_ns::get_control("Kernel CMM", nodeid);
	version_manager::vp_version_t rv;
	version_manager::vm_admin_var vm_adm = vm_util::get_vm(nodeid);
	vm_adm->get_running_version("cmm_control", rv, e);

	if (e.exception()) {
#ifndef _KERNEL
		//
		// SCMSGS
		// @explanation
		// An exception occurred when retrieving
		// the running version of CMM.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine if a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "exception %s raised while attempting to get "\
		    "running version of CMM.\n", e.exception()->_name());
		delete syslog_msgp;
#else
		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
		    ("clcomm_get_running_version has raised"\
		    " the error %s\n",  e.exception()->_name()));
#endif // !_KERNEL
		return (EIO);
	}

	if ((rv.major_num >= 1) && (rv.minor_num >= 1)) {
		if (CORBA::is_nil(ccp))
			return (ENOENT);

		*cluster_shutdown_flag =
		(boolean_t)ccp->get_cluster_shutdown(e);

		if (e.exception()) {
#ifndef _KERNEL
			//
			// SCMSGS
			// @explanation
			// An exception occurred when retrieving
			// the status of the cluster shutdown flag in CMM.
			// @user_action
			// Contact your authorized Sun service provider
			// to determine if a workaround or patch is available.
			//
			(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "exception %s raised while attempting to get "\
			    "the status of cluster shutdown flag in CMM.\n",
			    e.exception()->_name());
#else
			CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
			    ("cluster_shutdown has raised"\
			    " the exception %s\n", e.exception()->_name()));
#endif // !_KERNEL
			return (EIO);
		}

		return (0);
	}
	else
		return (EACCES);
}

#if (SOL_VERSION >= __s10)

//
// Get the cluster id of the Cluster-Wide Zone given the name of the
// cluster-wide zone.
// Will return:
// ENOENT if the specified cluster-wide zone does not exist.
// EACCES if there is no privilege to get the cluster id of the given
// cluster-wide zone.
// 0 upon success, will also return the cluster id of the given
// cluster-wide zone.
//
int
clconf_get_cluster_id(char *clusterp, uint_t *clidp)
{
	return (clconf_ccr::the().get_cluster_id(clusterp, *clidp));
}

//
// Get the name of the Cluster-Wide Zone given the cluster id of the
// cluster-wide zone.
// Will return:
// ENOENT if the specified cluster-wide zone does not exist.
// EACCES if there is no privilege to get the name of the given
// cluster-wide zone.
// 0 upon success, will also return the name of the given
// cluster-wide zone. The caller of this interface has to release
// the memory allocated to store the name of the cluster-wide zone.
//
int
clconf_get_cluster_name(uint_t clid, char **clusterp)
{
	return (clconf_ccr::the().get_cluster_name(clid, clusterp));
}

#ifndef UNODE
//
// Returns B_TRUE if running inside a zone cluster,
// else returns B_FALSE. In other words, if we are running in
// global zone or a native non-global zone, then return B_FALSE.
//
boolean_t
clconf_inside_virtual_cluster()
{
	uint32_t cl_id;
	zoneid_t zoneid;
	char cluster_name[CLUSTER_NAME_MAX];
	int error;

	zoneid = getzoneid();
	if (zoneid == GLOBAL_ZONEID) {
		// Inside global zone
		return (B_FALSE);
	}

#ifdef _KERNEL_ORB
	zone_t *zonep = zone_find_by_id(zoneid);
	ASSERT(zonep);
	ASSERT(zonep->zone_name);
	if ((zonep == NULL) || (zonep->zone_name == NULL)) {
		os::warning("could not get current zone name");
		if (zonep) {
			zone_rele(zonep);
		}
		return (B_FALSE);
	}
	(void) os::strcpy(cluster_name, zonep->zone_name);
	zone_rele(zonep);
#else	// _KERNEL_ORB
	if (getzonenamebyid(zoneid, cluster_name, CLUSTER_NAME_MAX) < 0) {
		os::warning("could not get current zone name");
		return (B_FALSE);
	}
#endif	// _KERNEL_ORB

	error = clconf_get_cluster_id(cluster_name, &cl_id);
	if (error) {
		ASSERT(0);	// Cannot happen as query for current zone
		return (B_FALSE);
	}

	if (cl_id == BASE_NONGLOBAL_ID) {
		// Inside native non-global zone
		return (B_FALSE);
	} else {
		ASSERT(cl_id >= MIN_CLUSTER_ID);
		return (B_TRUE);
	}
}
#endif	// UNODE
#endif	// (SOL_VERSION >= __s10)

//
// Is Europa farm management enabled ?
//
boolean_t
clconf_is_farm_mgt_enabled()
{
	boolean_t ret;
	const char *p;
	clconf_cluster_t *cl = clconf_cluster_get_current();

	p = clconf_obj_get_property((clconf_obj_t *)cl, "type");
	if ((p == NULL) || (strcmp(p, "europa") != 0)) {
		ret = B_FALSE;
	} else {
		ret = B_TRUE;
	}
	clconf_obj_release((clconf_obj_t *)cl);
	return (ret);
}
