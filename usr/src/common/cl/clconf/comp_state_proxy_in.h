/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  comp_state_proxy_in.h
 *
 */

#ifndef _COMP_STATE_PROXY_IN_H
#define	_COMP_STATE_PROXY_IN_H

#pragma ident	"@(#)comp_state_proxy_in.h	1.7	08/05/20 SMI"

//
// Component State Proxy
//

inline void
component_state_proxy::proxy_lock()
{
	ASSERT(csp_lock.lock_not_held());
	csp_lock.lock();
}

inline void
component_state_proxy::proxy_unlock()
{
	ASSERT(csp_lock.lock_held());
	csp_lock.unlock();
}

inline bool
component_state_proxy::proxy_lock_held()
{
	return (csp_lock.lock_held());
}

#endif	/* _COMP_STATE_PROXY_IN_H */
