/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clconf_property.cc	1.24	08/08/01 SMI"

#include <sys/clconf_property.h>
#include <clconf/clconf_file_io.h>

// The property tree.
ccr_node *clplroot;

// The helper functions defined in clconf.cc
extern void clconf_objtype_to_name(clconf_objtype_t type, char *name);

/*
 * Release the property tree, prepare to get a new tree.
 */
void
clpl_refresh()
{
	// Only keep one tree.
	if (clplroot != NULL) {
		clplroot->delete_tree();
	}
	clplroot = new ccr_node("objects");
}

/*
 * Reads in information from the specified file.
 */
void
clpl_read(char *filename)
{
	if (clplroot == NULL) {
		clpl_refresh();
	}

	ASSERT(clplroot != NULL);

	clconf_file_io plio(filename);

	char *cluster_namep = NULL;
#if (SOL_VERSION >= __s10)
	cluster_namep = new char[CLUSTER_NAME_MAX];
	ASSERT(cluster_namep != NULL);
	// This file is only compiled into userland library
	if (getzonenamebyid(getzoneid(), cluster_namep, CLUSTER_NAME_MAX) < 0) {
#ifdef DEBUG
		os::warning("clpl_read can't get cluster name");
#endif	// DEBUG
		delete [] cluster_namep;
		return;
	}
#else	// (SOL_VERSION >= __s10)
	cluster_namep = os::strdup(DEFAULT_CLUSTER);
#endif	// (SOL_VERSION >= __s10)

	if (!plio.open()) {
#ifdef DEBUG
		os::warning("clpl_read can't open file %s", filename);
#endif
		delete [] cluster_namep;
		return;
	}
	delete [] cluster_namep;

	// read CCR.
	char *key = NULL;
	char *value = NULL;
	while (plio.read_one(&key, &value) == clconf_io::C_SUCCESS) {
		if (!((key[0] == '\0') || (key[0] == '#'))) {
			//
			// Only process a non-empty, non-commented line.
			//
			(void) clplroot->get_child_by_name_create_ex(key)->
			    set_value(value);
		}
		delete [] key;
		delete [] value;
	}
	plio.close();
}

// Helper function. Get the object group.
ccr_node *
clpl_get_group(clconf_objtype_t ot)
{
	if (clplroot == NULL) {
		return (NULL);
	}
	char tn[CL_MAX_LEN+1];
	clconf_objtype_to_name(ot, tn);
	return (clplroot->get_named_child(tn));
}

/*
 * Get valid objects for a certain type.
 * obj_type: the type of objects to get, eg CL_ADAPTER.
 */
clconf_iter_t *
clpl_get_objects(clconf_objtype_t ot)
{
	ccr_node *cr = clpl_get_group(ot);
	if (cr == NULL) {
		return (NULL);
	}
	return ((clconf_iter_t *)cr->get_iterator());
}

/*
 * Get a object of the specified type and name. For example,
 * clpl_get_object(CL_ADAPTER, "hme") to get the hme adapter.
 */
clconf_obj_t *
clpl_get_object(clconf_objtype_t ot, const char *nm)
{
	ccr_node *cr = clpl_get_group(ot);
	if (cr == NULL) {
		return (NULL);
	}

	ccr_node *ret = cr->get_named_child(nm);
	if (ret == NULL) {
		ret = cr->get_named_child(CLPL_GENERIC);
	}
	return ((clconf_obj_t *)ret);
}

/* Get the list of properties for an object */
clconf_iter_t *
clpl_obj_get_properties(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	ccr_node *nd = (ccr_node *)obj;

	// The property of a specific object should contain all the properties
	// of the generic object.
	if (os::strcmp(nd->get_name(), CLPL_GENERIC) != 0) {
		ccr_node *g_obj = nd->get_parent()->get_named_child(
		    CLPL_GENERIC);

		clconfiter *g_iter = g_obj->get_iterator();
		ccr_node *g_chld;
		while ((g_chld = g_iter->get_current()) != NULL) {
			if (nd->check_name(g_chld->get_name())) {
				// We don't have the generic property, add it.
				(void) nd->add(g_chld->copy_tree());
			}
			g_iter->advance();
		}
		delete g_iter;
	}
	return ((clconf_iter_t *)nd->get_iterator());
}

/* Get the object/property's name */
const char *
clpl_get_name(clconf_obj_t *obj)
{
	return (((ccr_node *)obj)->get_name());
}

/* Get the property's type */
clpl_type
clpl_get_type(clconf_obj_t *obj)
{
	ccr_node *node = (ccr_node *)obj;
	ccr_node *cr = node->get_named_child("type");
	if (cr == NULL) {
		char *fname = node->get_full_name();

		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		    "CCR", NULL);
		//
		// SCMSGS
		// @explanation
		// Could not find the type of property in the configuration
		// file.
		// @user_action
		// Check the configuration file.
		//
		(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "clconf: Your configuration file is incorrect! The "
		    "type of property %s is not found", fname);

		delete [] fname;
	}
	const char *tn = cr->get_value();
	clpl_type ret;
	switch (tn[0]) {
	case 's':
		ret = CL_STRING;
		break;
	case 'i':
		ret = CL_INT;
		break;
	case 'e':
		ret = CL_ENUM;
		break;
	case 'b':
		ret = CL_BOOLEAN;
		break;
	default:
		{
			os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
			    "CCR", NULL);
			//
			// SCMSGS
			// @explanation
			// Found the unrecognized property type in the
			// configuration file.
			// @user_action
			// Check the configuration file.
			//
			(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
			    "clconf: Unrecognized property type");
		}
		/*NOTREACHED*/
	}
	return (ret);
}

/* Get the property's min */
int
clpl_get_min(clconf_obj_t *obj)
{
	ccr_node *node = (ccr_node *)obj;
	ccr_node *cr = node->get_named_child("min");
	if (cr == NULL) {
		return (0);
	}
	return (os::atoi(cr->get_value()));
}

/* Get the property's max */
int
clpl_get_max(clconf_obj_t *obj)
{
	ccr_node *node = (ccr_node *)obj;
	ccr_node *cr = node->get_named_child("max");
	if (cr == NULL) {
		return (0);
	}
	return (os::atoi(cr->get_value()));
}

/* Get the property's default value */
const char *
clpl_get_default(clconf_obj_t *obj)
{
	ccr_node *node = (ccr_node *)obj;
	ccr_node *cr = node->get_named_child("default");
	if (cr == NULL) {
		return (NULL);
	}
	return (cr->get_value());
}

/* Is the property a required one? Returns 1 if yes 0 if no */
unsigned char
clpl_is_required(clconf_obj_t *obj)
{
	ccr_node *node = (ccr_node *)obj;
	ccr_node *cr = node->get_named_child("required");
	if (cr == NULL) {
		return (1);
	}
	const char *v = cr->get_value();
	if (v[0] == '0') {
		return (0);
	}
	ASSERT(v[0] == '1');
	return (1);
}

/*
 * Get the property's enum list. For CL_ENUM property only. It returns a
 * string with ':' as deliminator between the enum values
 */
const char *
clpl_get_enumlist(clconf_obj_t *obj)
{
	ccr_node *node = (ccr_node *)obj;
	ccr_node *cr = node->get_named_child("values");
	if (cr == NULL) {
		return (NULL);
	}
	return (cr->get_value());
}

/* Get the description for the property */
clconf_propdesc_t
clpl_prop_get_description(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clconf_propdesc_t pdesc;
	pdesc.name = clpl_get_name(obj);
	pdesc.type = clpl_get_type(obj);
	pdesc.min = clpl_get_min(obj);
	pdesc.max = clpl_get_max(obj);
	pdesc.defaultv = clpl_get_default(obj);
	pdesc.required = clpl_is_required(obj);
	pdesc.enumlist = clpl_get_enumlist(obj);
	return (pdesc);
}
