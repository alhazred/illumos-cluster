/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLCONF_IO_H
#define	_CLCONF_IO_H

#pragma ident	"@(#)clconf_io.h	1.16	08/08/01 SMI"

#include <orb/invo/common.h>
#include <sys/sol_version.h>
#include <ccr/common.h>

// This is the interface to read CCR.
class clconf_io {
public:

	enum status { C_FAIL, C_RETRY, C_SUCCESS };

	// Read one (key, value) pair. Returns true if successful, false if
	// the end is reached.
	virtual status read_one(char **key, char **value) = 0;

	// Open a read CCR session. Returns true if it's successful.
	virtual status open() = 0;

	// Close a read CCR session.
	virtual void close() = 0;

	// for write.
	virtual bool begin_transaction() = 0;
	virtual bool commit_transaction() = 0;
	virtual void abort_transaction() = 0;

	// write one element to CCR.
	virtual bool write(const char *key, const char *value) = 0;

	// Create and destroy a sequence
	virtual void *create_seq() = 0;
	virtual void delete_seq(void *) = 0;

	// Write a sequence to CCR
	virtual int write_seq(void *seq) = 0;

	virtual int32_t get_version() = 0;
};


#endif	/* _CLCONF_IO_H */
