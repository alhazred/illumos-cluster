/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)testwrite.cc	1.26	08/05/20 SMI"

#include <sys/clconf_int.h>
#include <clconf/clconf_ccr.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define	MAX_ERRBUF	256
#define	ADD_NODE	1
#define	ADD_BB		2
#define	ADD_ADP		3
#define	ADD_QUORUM_DEVICE	4

#define	ENABLE		1
#define	DISABLE		2

static void	usage(char *);
static int	test_init(void);
static void	test_fini(void);
static int	add_node(void);
static int	add_quorum_device(int);
static int	add_bb(void);
static int	add_adp(int);
static int	rem_node(int);
static int	rem_quorum_device(int);
static int	rem_bb(int);
static int	rem_adp(int, int);
static int	change_node_state(int, int);
static int	change_bb_state(int, int);
static int	change_adp_state(int, int, int);
static int	change_aport_state(int, int, int, int);
static int	change_bport_state(int, int, int);

int		testclconf(int, char **);

clconf_cluster_t	*cl = NULL;
clconf_cluster_t	*new_cl = NULL;

static void
usage(char *prog)
{
	os::printf("Usage: %s -N | -B  | -Q -v <vote> add\n", prog);
	os::printf("Usage: %s -n <nid> | -b <bbid> | -q <qid> rem \n", prog);
	os::printf("Usage: %s -n <nid> | -b <bbid> enb | dsb \n", prog);
	os::printf("Usage: %s -n <nid> -A adp_add\n", prog);
	os::printf("Usage: %s -n <nid> -a <adp_id> adp_rem\n", prog);
	os::printf("Usage: %s -n <nid> -a <adpid> adp_enb | adp_dsb\n", prog);
	os::printf("Usage: %s -n <nid> -a <adpid> -p <adp_pid> "
		"aport_enb | aport_dsb\n", prog);
	os::printf("Usage: %s -b <bbid> -i <bb_pid> bport_enb | bport_dsb\n",
		prog);
	os::printf("Usage: %s -I <ip_address>\n", prog);
	os::printf("Where the actions are:\n");
	os::printf("\tadd, rem: add or remove Node/Blackbox/QuorumDevice\n");
	os::printf("\tenb, dsb: enable or disable Node/Blackbox\n");
	os::printf("\tadp_add, adp_rem: add or remove Adapter\n");
	os::printf("\tadp_enb, adp_dsb: enable or disable Adapter\n");
	os::printf("\taport_enb, aport_dsb: enable or disable Adapter-Port\n");
	os::printf("\tbport_enb, bport_dsb: enable or disable Blackbox-Port\n");
	os::printf("\t-I prints the nodeid hosting ip_address\n");
	os::printf("Where the tags are:\n");
	os::printf("\tnid is the ID of the Node\n");
	os::printf("\tbbid is the ID of the Blackbox\n");
	os::printf("\tqid is the ID of the Quorum Device\n");
	os::printf("\tvote is the vote count of the Quorum Device\n");
	os::printf("\tadpid is the ID of the Adapter\n");
	os::printf("\tadp_pid is the ID of the Adapter-Port\n");
	os::printf("\tbb_pid is the ID of the Blackbox-Port\n");
}

int
testclconf(int argc, char **argv)
{
	int		arg = 0;
	int		nid = 0, bbid = 0, adpid = 0, bb_pid = 0, adp_pid = 0,
			qid = 0, votecount = 0;
	char		*cmd = NULL;
	int		err = 0;
	int		add_action = 0;

	if (argc < 3) {
		usage(argv[0]);
		return (1);
	}

	// We need to reset the value of optind to 1.
	// This is because our model involves calling -
	//	unode_load xx 1 testclconf testclconf -n 4 add
	// The first time around optind is 1.  However, now testclconf
	// is loaded and it "remembers" the value of optind.
	//
	// The next time, when we run unode_load, it uses the "remembered"
	// value of optind and we do not parse our args correctly.

	optind = 1;

	// Examine command line options
	while ((arg = getopt(argc, argv, "QNBAn:b:a:i:p:q:v:I:?")) != EOF) {
		switch (arg) {
		case 'Q':
			if (add_action)
				err = 1;	// cannot have 2 add_actions
			else
				add_action = ADD_QUORUM_DEVICE;
			os::printf("add_action (%d)\n", add_action);
			break;
		case 'N':
			if (add_action)
				err = 1;	// cannot have 2 add_actions
			else
				add_action = ADD_NODE;
			os::printf("add_action (%d)\n", add_action);
			break;
		case 'B':
			if (add_action)
				err = 1;	// cannot have 2 add_actions
			else
				add_action = ADD_BB;
			break;
		case 'A':
			if (add_action)
				err = 1;	// cannot have 2 add_actions
			else
				add_action = ADD_ADP;
			break;
		case 'n':
			errno = 0;
			nid = atoi(optarg);
			if (errno || (nid <= 0))
				err = 1;
			break;
		case 'q':
			errno = 0;
			qid = atoi(optarg);
			if (errno || (qid <= 0))
				err = 1;
			break;
		case 'v':
			errno = 0;
			votecount = atoi(optarg);
			if (errno || (votecount < 0))
				err = 1;
			break;
		case 'b':
			errno = 0;
			bbid = atoi(optarg);
			if (errno || (bbid <= 0))
				err = 1;
			break;
		case 'a':
			errno = 0;
			adpid = atoi(optarg);
			if (errno || (adpid <= 0))
				err = 1;
			break;
		case 'p':
			errno = 0;
			adp_pid = atoi(optarg);
			if (errno || (adp_pid <= 0))
				err = 1;
			break;
		case 'i':
			errno = 0;
			bb_pid = atoi(optarg);
			if (errno || (bb_pid <= 0))
				err = 1;
			break;
		case 'I':
			if (optarg) {
			    nodeid_t	n_id;
			    int		adp_id;
			    clconf_cluster_get_nodeid_for_ipaddr
				(&n_id, &adp_id, os::inet_addr(optarg));
			    os::printf("Nodeid hosting IP (%s) is %u and "
				"adpid (%d)\n",
				optarg, n_id, adp_id);
				return (0);
			} else
				err = 1;
			break;
		case '?':
			err = 1;
			break;
		default:
			os::printf("Invalid option\n");
			err = 1;
			break;
		}

		if (err) {
			usage(argv[0]);
			return (err);
		}
	}

	if (argc <= optind) {
		usage(argv[0]);
		return (2);
	}

	cmd = argv[optind];

	if (test_init() != 0) {
		os::printf("Error reading CCR cluster tree\n");
		return (6);
	}

	if (strcmp(cmd, "add") == 0) {
	    if ((add_action != ADD_NODE) && (add_action != ADD_QUORUM_DEVICE) &&
		(add_action != ADD_BB)) {
		err = 3;
	    } else {
		if (add_action == ADD_NODE) {
		    os::printf("Add Node\n");
		    if (add_node() != 0)
			err = 5;
		} else if (add_action == ADD_QUORUM_DEVICE) {
		    os::printf("Add Quorum Device\n");
		    if (add_quorum_device(votecount) != 0)
			err = 5;
		} else {
		    os::printf("Add Blackbox\n");
		    if (add_bb() != 0)
			err = 5;
		}
	    }
	} else if (strcmp(cmd, "rem") == 0) {
	    if (((nid == 0) && (qid == 0) && (bbid == 0)) ||
		((nid > 0) && (qid > 0)) ||
		((nid > 0) && (bbid > 0)) ||
		((qid > 0) && (bbid > 0))) {
		err = 3;
	    } else {
		if (nid > 0) {
		    os::printf("Remove node (%d)\n", nid);
		    if (rem_node(nid) != 0)
			err = 5;
		} else if (qid > 0) {
		    os::printf("Remove quorum device (%d)\n", qid);
		    if (rem_quorum_device(qid) != 0)
			err = 5;
		} else {
		    os::printf("Remove blackbox (%d)\n", bbid);
		    if (rem_bb(bbid) != 0)
			err = 5;
		}
	    }
	} else if (strcmp(cmd, "enb") == 0) {
	    if (((nid == 0) && (bbid == 0)) ||
		((nid > 0) && (bbid > 0))) {
		err = 3;
	    } else {
		os::printf("Enable %s (%d)\n",
		    (nid > 0) ? "node" : "blackbox",
		    (nid > 0) ? nid : bbid);
		if (nid > 0) {
		    if (change_node_state(nid, ENABLE) != 0)
			err = 5;
		} else {
		    if (change_bb_state(bbid, ENABLE) != 0)
			err = 5;
		}
	    }
	} else if (strcmp(cmd, "dsb") == 0) {
	    if (((nid == 0) && (bbid == 0)) ||
		((nid > 0) && (bbid > 0))) {
		err = 3;
	    } else {
		os::printf("Disable %s (%d)\n",
		    (nid > 0) ? "node" : "blackbox",
		    (nid > 0) ? nid : bbid);
		if (nid > 0) {
		    if (change_node_state(nid, DISABLE) != 0)
			err = 5;
		} else {
		    if (change_bb_state(bbid, DISABLE) != 0)
			err = 5;
		}
	    }
	} else if (strcmp(cmd, "adp_add") == 0) {
	    if ((add_action != ADD_ADP) || (nid == 0)) {
		err = 3;
	    } else {
		os::printf("Add Adapter in node (%d)\n", nid);
		if (add_adp(nid) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "adp_rem") == 0) {
	    if ((nid == 0) || (adpid == 0)) {
		err = 3;
	    } else {
		os::printf("Remove Adapter - node (%d), adp (%d)\n",
		    nid, adpid);
		if (rem_adp(nid, adpid) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "adp_enb") == 0) {
	    if ((nid == 0) || (adpid == 0)) {
		err = 3;
	    } else {
		os::printf("Enable Adapter - node (%d), adp (%d)\n",
		    nid, adpid);
		if (change_adp_state(nid, adpid, ENABLE) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "adp_dsb") == 0) {
	    if ((nid == 0) || (adpid == 0)) {
		err = 3;
	    } else {
		os::printf("Disable Adapter - node (%d), adp (%d)\n",
		    nid, adpid);
		if (change_adp_state(nid, adpid, DISABLE) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "aport_enb") == 0) {
	    if ((nid == 0) || (adpid == 0) || (adp_pid == 0)) {
		err = 3;
	    } else {
		os::printf("Enable Adapter-Port - node (%d), adp (%d), "
		    "port(%d)\n", nid, adpid, adp_pid);
		if (change_aport_state(nid, adpid, adp_pid, ENABLE) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "aport_dsb") == 0) {
	    if ((nid == 0) || (adpid == 0) || (adp_pid == 0)) {
		err = 3;
	    } else {
		os::printf("Disable Adapter-Port - node (%d), adp (%d), "
		    "port(%d)\n", nid, adpid, adp_pid);
		if (change_aport_state(nid, adpid, adp_pid, DISABLE) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "bport_enb") == 0) {
	    if ((bbid == 0) || (bb_pid == 0)) {
		err = 3;
	    } else {
		os::printf("Enable Blackbox-Port - blackbox (%d), port (%d)\n",
		    bbid, bb_pid);
		if (change_bport_state(bbid, bb_pid, ENABLE) != 0)
			err = 5;
	    }
	} else if (strcmp(cmd, "bport_dsb") == 0) {
	    if ((bbid == 0) || (bb_pid == 0)) {
		err = 3;
	    } else {
		os::printf("Disable Blackbox-Port - blackbox (%d), port (%d)\n",
		    bbid, bb_pid);
		if (change_bport_state(bbid, bb_pid, DISABLE) != 0)
			err = 5;
	    }
	} else {
	    err = 4;
	}

	test_fini();

	if (err == 3) {
		os::printf("%s: Invalid argument values\n", argv[0]);
		usage(argv[0]);
	} else if (err == 4) {
		os::printf("Invalid action (%s)\n", cmd);
		usage(argv[0]);
	} else if (err == 5) {
		os::printf("%s: Error completing action (%s)\n", argv[0], cmd);
	}

	return (err);
}

static int
test_init(void)
{

	if ((cl = clconf_cluster_get_current()) == NULL) {
		os::printf("Error in clconf_cluster_get_current()\n");
		return (1);
	}

	if ((new_cl = (clconf_cluster_t *)clconf_obj_deep_copy((clconf_obj_t *)
		cl)) == NULL) {
		os::printf("Error in clconf_obj_deep_copy()\n");
		return (1);
	}

	return (0);
}

static void
test_fini(void)
{

	if (cl)
		clconf_obj_release((clconf_obj_t *)cl);

	if (new_cl)
		clconf_obj_release((clconf_obj_t *)new_cl);

#ifdef DO_WE_NEED_THIS
	clconf_siter_t	siter;
	clconf_obj_t	*obj;

	// Get a new clconf tree.
	cl = clconf_cluster_get_current();
	iter = clconf_siter_init(&siter, (clconf_obj_t *)cl, CL_NODE);
	for (; (obj = clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {
		os::printf("node: %s\n", clconf_obj_get_name(obj));
	}
	clconf_obj_release((clconf_obj_t *)cl);
#endif	// DO_WE_NEED_THIS
}

/*
 * Add a Node
 */
static int
add_node(void)
{

	int	i;
	char	errorbuf[MAX_ERRBUF];

#ifdef _KERNEL_ORB
	int num_adps = 4;
#else
	int num_adps = 1;
#endif
	clconf_errnum_t err, cl_err;

	const char *clname = clconf_obj_get_name((clconf_obj_t *)cl);
	clconf_node_t *nd = clconf_node_create();
	err = clconf_cluster_add_node(new_cl, nd);
	if (err != CL_NOERROR) {
		os::warning("error: %d\n", err);
	}
	char buff[30];
	(void) sprintf(buff, "node-%d", clconf_obj_get_id((clconf_obj_t *)nd));
	clconf_obj_set_name((clconf_obj_t *)nd, buff);
	(void) clconf_obj_set_property((clconf_obj_t *)nd, "quorum_vote", "0");
	//
	// XXX: Use a cluster ID of 0xabcdef12 for now. This should be
	// replaced by the actual clusterID field in the infrastructure
	// table.
	//
	(void) sprintf(buff, "abcdef12%8llx\n",
		clconf_obj_get_id((clconf_obj_t *)nd));
	(void) clconf_obj_set_property((clconf_obj_t *)nd, "quorum_resv_key",
	    buff);

	// Get the transport_type for the cluster.
	clconf_obj_t *tmpnode = clconf_obj_get_child((clconf_obj_t *)new_cl,
	    CL_NODE, (int)orb_conf::node_number());
	clconf_iter_t *tmpiter =
	    clconf_node_get_adapters((clconf_node_t *)tmpnode);
	const char *trtype =
	    clconf_adapter_get_transport_type((clconf_adapter_t *)
	    clconf_iter_get_current(tmpiter));
	clconf_iter_release(tmpiter);

	// Add adapters.
	for (i = 0; i < num_adps; i++) {
		clconf_adapter_t *ada = clconf_adapter_create();
		err = clconf_node_add_adapter(nd, ada);
		if (err != CL_NOERROR) {
			os::warning("error: %d add_adapter %d\n", err, i);
		}
		(void) sprintf(buff, "adp-%d",
		    clconf_obj_get_id((clconf_obj_t *)ada));
		clconf_obj_set_name((clconf_obj_t *)ada, buff);
		clconf_adapter_set_transport_type(ada, trtype);
		// To test SCI adp calculation by the TM, use ifndef SCI
#ifndef SCI
		(void) clconf_obj_set_property((clconf_obj_t *)ada,
		    "device_name", "sci");
#else
		(void) clconf_obj_set_property((clconf_obj_t *)ada,
		    "device_name", "adp");
#endif /* SCI */
		(void) sprintf(buff, "%d",
		    clconf_obj_get_id((clconf_obj_t *)ada));
		(void) clconf_obj_set_property((clconf_obj_t *)ada,
		    "device_instance", buff);
#ifdef _KERNEL_ORB

		(void) sprintf(buff, "/tmp/%s/node-%d/transport.%d", clname,
		    clconf_obj_get_id((clconf_obj_t *)nd), i+1);
		(void) clconf_obj_set_property((clconf_obj_t *)ada, "door_name",
		    buff);
#else
		// The following tests real cluster. The properties should
		// be filled in according to the real infrastructure.
		(void) clconf_obj_set_property((clconf_obj_t *)ada,
		    "physical_address",
		    "/iommu@f,e0000000/sbus@f,e0001000/SUNW,hme@1,8c00000");
		(void) clconf_obj_set_property((clconf_obj_t *)ada,
		    "adapter_id",
		    "71");
#endif
		// Adapter Ports
		clconf_port_t *pt = clconf_port_create();
		err = clconf_adapter_add_port(ada, pt);
		if (err != CL_NOERROR) {
			os::warning("error: %d add_port to adapter %d\n",
			    err, i);
		}
		(void) sprintf(buff, "%d",
		    clconf_obj_get_id((clconf_obj_t *)pt));
		clconf_obj_set_name((clconf_obj_t *)pt, buff);
		clconf_obj_enable((clconf_obj_t *)pt);
	}

	// Add cables.
	clconf_iter_t *iter = clconf_cluster_get_bbs(new_cl);
	clconf_obj_t *bb;
	for (i = 1; (bb = clconf_iter_get_current(iter)) != NULL; i++,
	    clconf_iter_advance(iter)) {
		// Get the port on the correct adapter.
		clconf_obj_t *ada1 = clconf_obj_get_child(
		    (clconf_obj_t *)nd, CL_ADAPTER, i);
		if (ada1 == NULL) {
			break;
		}
		clconf_port_t *p1 = (clconf_port_t *)clconf_obj_get_child(
		    ada1, CL_PORT, 1);
		ASSERT(p1 != NULL);

		// Create a port on the blackbox.
		clconf_port_t *p2 = clconf_port_create();
		(void) clconf_bb_add_port((clconf_bb_t *)bb, p2);
		(void) sprintf(buff, "%d",
		    clconf_obj_get_id((clconf_obj_t *)p2));
		clconf_obj_set_name((clconf_obj_t *)p2, buff);
		clconf_obj_enable((clconf_obj_t *)p2);

		// create the cable.
		clconf_cable_t *cb1 = clconf_cable_create();
		err = clconf_cable_set_endpoint(cb1, 1, p1);
		ASSERT(err == CL_NOERROR);
		err = clconf_cable_set_endpoint(cb1, 2, p2);
		ASSERT(err == CL_NOERROR);

		// Add the cable to the cluster.
		err = clconf_cluster_add_cable(new_cl, cb1);
		ASSERT(err == CL_NOERROR);
	}
	clconf_iter_release(iter);

	// Enable the node and all adapters.
	if ((cl_err = clconf_obj_enable((clconf_obj_t *)new_cl)) != 0) {
	    os::printf("clconf_obj_enable() failed (err = %d)\n", cl_err);
	    return (1);
	}
	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Add a Quorum Device
 */
static int
add_quorum_device(int votecount)
{

	char	errorbuf[MAX_ERRBUF];

	clconf_errnum_t err, cl_err;

	clconf_quorum_t *qd = clconf_quorum_device_create();
	err = clconf_cluster_add_quorum_device(new_cl, qd);
	if (err != CL_NOERROR) {
		os::warning("error: %d\n", err);
	}
	char buff[30];
	(void) sprintf(buff, "%d", votecount);
	(void) clconf_obj_set_property((clconf_obj_t *)qd, "votecount", buff);
	(void) clconf_obj_set_property((clconf_obj_t *)qd, "gdevname",
		"/dev/did/rdsk/d99s2");

	// Enable the quorum device
	if ((cl_err = clconf_obj_enable((clconf_obj_t *)new_cl)) != 0) {
	    os::printf("clconf_obj_enable() failed (err = %d)\n", cl_err);
	    return (1);
	}

	cl_err = clconf_cluster_check_transition(NULL,
	    new_cl, errorbuf, MAX_ERRBUF, NULL);

	os::printf("clconf_cluster_check_transition() returned %d\n", cl_err);

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Add a Blackbox
 */
static int
add_bb(void)
{
	os::printf("Not yet supported\n");
	return (0);
}

/*
 * Add an Adapter
 */
static int
add_adp(int /* nid */)
{
	os::printf("Not yet supported\n");
	return (0);
}

/*
 * Remove Node
 */
static int
rem_node(int nid)
{
	char	errorbuf[MAX_ERRBUF];
	clconf_node_t *nd;
	clconf_errnum_t cl_err;

	if ((nd = (clconf_node_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_NODE, nid)) == NULL) {
		os::printf("Node (%d) does not exist!\n", nid);
		return (1);
	}

	/*
	 * For now we are working with Node Disable.  This is because,
	 * when we remove node, we should also get rid of the
	 * affected cables
	 */
	// clconf_errnum_t err = clconf_cluster_remove_node(new_cl, nd);
	if ((cl_err = clconf_obj_disable((clconf_obj_t *)nd)) != 0) {
	    os::printf("clconf_obj_disable() failed (err = %d)\n", cl_err);
	    return (1);
	}
	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Remove a quorum device
 */
static int
rem_quorum_device(int qid)
{
	char	errorbuf[MAX_ERRBUF];
	clconf_quorum_t *qd;
	clconf_errnum_t cl_err;

	if ((qd = (clconf_quorum_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_QUORUM_DEVICE, qid)) == NULL) {
		os::printf("Quorum device (%d) does not exist!\n", qid);
		return (1);
	}

	/*
	 * For now we are working with Node Disable.  This is because,
	 * when we remove node, we should also get rid of the
	 * affected cables
	 */
	cl_err = clconf_cluster_remove_quorum_device(new_cl, qd);
	if (cl_err != 0) {
	    os::printf("clconf_cluster_remove_quorum_device() failed "
		"(err = %d)\n", cl_err);
	    return (1);
	}

	cl_err = clconf_cluster_check_transition(NULL,
	    new_cl, errorbuf, MAX_ERRBUF, NULL);

	os::printf("clconf_cluster_check_transition() returned %d\n", cl_err);

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}


/*
 * Remove a Blackbox
 */
static int
rem_bb(int bbid)
{
	clconf_bb_t	*bb;
	char		errorbuf[MAX_ERRBUF];
	clconf_errnum_t	cl_err;

	if ((bb = (clconf_bb_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_BLACKBOX, bbid)) == NULL) {
		os::printf("Blackbox (%d) does not exist!\n", bbid);
		return (1);
	}

	/*
	 * For now we are working with BB Disable.  This is because,
	 * when we remove BB, we should also get rid of the
	 * affected cables
	 */
	// clconf_errnum_t err = clconf_cluster_remove_bb(new_cl, bb);
	if ((cl_err = clconf_obj_disable((clconf_obj_t *)bb)) != 0) {
	    os::printf("clconf_obj_disable() failed (err = %d)\n", cl_err);
	    return (1);
	}

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Remove an Adapter
 */
static int
rem_adp(int nid, int adpid)
{
	clconf_node_t *nd;
	clconf_adapter_t *adp;
	char	errorbuf[MAX_ERRBUF];
	clconf_errnum_t cl_err;

	if ((nd = (clconf_node_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_NODE, nid)) == NULL) {
		os::printf("Node (%d) does not exist!\n", nid);
		return (1);
	}

	if ((adp = (clconf_adapter_t *)clconf_obj_get_child
	    ((clconf_obj_t *)nd, CL_ADAPTER, adpid)) == NULL) {
		os::printf("Node (%d) Adp (%d) does not exist!\n", nid, adpid);
		return (1);
	}

	/*
	 * Use this, but remove affected cables
	 */
	// clconf_node_remove_adapter(nd, adp);
	if ((cl_err = clconf_obj_disable((clconf_obj_t *)adp)) != 0) {
	    os::printf("clconf_obj_disable(() failed (err = %d)\n", cl_err);
	    return (1);
	}

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}
	return (0);
}

/*
 * Enable/Disable a Node
 */
static int
change_node_state(int nid, int action)
{
	char		errorbuf[MAX_ERRBUF];
	clconf_node_t	*nd;
	clconf_iter_t	*iter1, *iter2;
	clconf_adapter_t *adp;
	clconf_port_t	*aport;
	clconf_errnum_t	cl_err;

	if ((nd = (clconf_node_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_NODE, nid)) == NULL) {
		os::printf("Node (%d) does not exist!\n", nid);
		return (1);
	}

	/*
	 * NOTE - clconf_obj_disable(node) will disable the node,
	 * all its adapters and their ports.
	 * However, clconf_obj_enable(node) will enable only the node
	 * and leave the adapters and their ports in their previous state.
	 */
	if (action == ENABLE) {
		if ((cl_err = clconf_obj_enable((clconf_obj_t *)nd)) != 0) {
		    os::printf("clconf_obj_enable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
		iter1 = clconf_node_get_adapters(nd);
		for (; (adp = (clconf_adapter_t *)
		    clconf_iter_get_current(iter1)) != NULL;
		    clconf_iter_advance(iter1)) {
		    if ((cl_err = clconf_obj_enable((clconf_obj_t *)adp)) !=
			0) {
			os::printf("clconf_obj_enable() failed (err = %d)\n",
			    cl_err);
			return (1);
		    }
		    iter2 = clconf_adapter_get_ports(adp);
		    for (; (aport = (clconf_port_t *)
			clconf_iter_get_current(iter2)) != NULL;
			clconf_iter_advance(iter2)) {
			if ((cl_err = clconf_obj_enable(
			    (clconf_obj_t *)aport)) != 0) {
			    os::printf("clconf_obj_enable() failed "
				"(err = %d)\n", cl_err);
			    return (1);
			}
		    }
		}
	} else if (action == DISABLE) {
		if ((cl_err = clconf_obj_disable((clconf_obj_t *)nd)) != 0) {
		    os::printf("clconf_obj_disable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else {
		os::printf("Illegal action specified (%d)\n", action);
		return (1);
	}
	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Enable/Disable a Blackbox
 */
static int
change_bb_state(int bbid, int action)
{
	clconf_bb_t	*bb;
	clconf_port_t	*bport;
	clconf_iter_t	*iter;
	char		errorbuf[MAX_ERRBUF];
	clconf_errnum_t cl_err;

	if ((bb = (clconf_bb_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_BLACKBOX, bbid)) == NULL) {
		os::printf("Blackbox (%d) does not exist!\n", bbid);
		return (1);
	}

	/*
	 * NOTE - clconf_obj_disable(blackbox) will disable the blackbox and
	 * all its ports.
	 * However, clconf_obj_enable(blackbox) will enable only the blackbox
	 * and leave the ports in their previous state.
	 */
	if (action == ENABLE) {
		if ((cl_err = clconf_obj_enable((clconf_obj_t *)bb)) != 0) {
		    os::printf("clconf_obj_enable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
		iter = clconf_bb_get_ports(bb);
		for (; (bport = (clconf_port_t *)
		    clconf_iter_get_current(iter)) != NULL;
		    clconf_iter_advance(iter)) {
		    if ((cl_err = clconf_obj_enable((clconf_obj_t *)bport)) !=
			0) {
			os::printf("clconf_obj_enable() failed (err = %d)\n",
			    cl_err);
			return (1);
		    }
		}
	} else if (action == DISABLE) {
		if ((cl_err = clconf_obj_disable((clconf_obj_t *)bb)) != 0) {
		    os::printf("clconf_obj_disable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else {
		os::printf("Illegal action specified (%d)\n", action);
		return (1);
	}

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Enable/Disable an Adapter
 */
static int
change_adp_state(int nid, int adpid, int action)
{
	clconf_node_t		*nd;
	clconf_adapter_t	*adp;
	clconf_port_t		*aport;
	clconf_iter_t		*iter;
	char			errorbuf[MAX_ERRBUF];
	clconf_errnum_t		cl_err;

	if ((nd = (clconf_node_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_NODE, nid)) == NULL) {
		os::printf("Node (%d) does not exist!\n", nid);
		return (1);
	}

	if ((adp = (clconf_adapter_t *)clconf_obj_get_child
	    ((clconf_obj_t *)nd, CL_ADAPTER, adpid)) == NULL) {
		os::printf("Node (%d) Adp (%d) does not exist!\n", nid, adpid);
		return (1);
	}

	/*
	 * NOTE - clconf_obj_disable(adapter) will disable the adapter and
	 * all its ports.
	 * However, clconf_obj_enable(adapter) will enable only the adapter
	 * and leave the ports in their previous state.
	 */
	if (action == ENABLE) {
		if ((cl_err = clconf_obj_enable((clconf_obj_t *)adp)) != 0) {
		    os::printf("clconf_obj_enable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
		iter = clconf_adapter_get_ports(adp);
		for (; (aport = (clconf_port_t *)
		    clconf_iter_get_current(iter)) != NULL;
		    clconf_iter_advance(iter)) {
		    if ((cl_err = clconf_obj_enable((clconf_obj_t *)aport)) !=
			0) {
			os::printf("clconf_obj_enable() failed (err = %d)\n",
			    cl_err);
			return (1);
		    }
		}
	} else if (action == DISABLE) {
		if ((cl_err = clconf_obj_disable((clconf_obj_t *)adp)) != 0) {
		    os::printf("clconf_obj_disable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else {
		os::printf("Illegal action specified (%d)\n", action);
		return (1);
	}

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Enable/Disable an Adapter-Port
 */
static int
change_aport_state(int nid, int adpid, int adp_pid, int action)
{
	clconf_node_t *nd;
	clconf_adapter_t *adp;
	clconf_port_t *aport;
	char	errorbuf[MAX_ERRBUF];
	clconf_errnum_t cl_err;

	if ((nd = (clconf_node_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_NODE, nid)) == NULL) {
		os::printf("Node (%d) does not exist!\n", nid);
		return (1);
	}

	if ((adp = (clconf_adapter_t *)clconf_obj_get_child
	    ((clconf_obj_t *)nd, CL_ADAPTER, adpid)) == NULL) {
		os::printf("Node (%d) Adp (%d) does not exist!\n", nid, adpid);
		return (1);
	}

	if ((aport = (clconf_port_t *)clconf_obj_get_child
	    ((clconf_obj_t *)adp, CL_PORT, adp_pid)) == NULL) {
		os::printf("Node (%d) Adp (%d) Port (%d) does not exist!\n",
			nid, adpid, adp_pid);
		return (1);
	}

	if (action == ENABLE) {
		if ((cl_err = clconf_obj_enable((clconf_obj_t *)aport)) != 0) {
		    os::printf("clconf_obj_enable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else if (action == DISABLE) {
		if ((cl_err = clconf_obj_disable((clconf_obj_t *)aport)) != 0) {
		    os::printf("clconf_obj_disable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else {
		os::printf("Illegal action specified (%d)\n", action);
		return (1);
	}

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

/*
 * Enable/Disable a Blackbox-Port
 */
static int
change_bport_state(int bbid, int bb_pid, int action)
{
	clconf_bb_t	*bb;
	clconf_port_t	*bport;
	char		errorbuf[MAX_ERRBUF];
	clconf_errnum_t cl_err;

	if ((bb = (clconf_bb_t *)clconf_obj_get_child(
	    (clconf_obj_t *)new_cl, CL_BLACKBOX, bbid)) == NULL) {
		os::printf("Blackbox (%d) does not exist!\n", bbid);
		return (1);
	}

	if ((bport = (clconf_port_t *)clconf_obj_get_child(
	    (clconf_obj_t *)bb, CL_PORT, bb_pid)) == NULL) {
		os::printf("Blackbox (%d) Port (%d) does not exist!\n",
			bbid, bb_pid);
		return (1);
	}

	if (action == ENABLE) {
		if ((cl_err = clconf_obj_enable((clconf_obj_t *)bport)) != 0) {
		    os::printf("clconf_obj_enable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else if (action == DISABLE) {
		if ((cl_err = clconf_obj_disable((clconf_obj_t *)bport)) != 0) {
		    os::printf("clconf_obj_disable() failed (err = %d)\n",
			cl_err);
		    return (1);
		}
	} else {
		os::printf("Illegal action specified (%d)\n", action);
		return (1);
	}

	if ((cl_err = clconf_cluster_commit(new_cl, errorbuf, MAX_ERRBUF)) !=
	    0) {
	    os::printf("clconf_cluster_commit() failed (err = %d)\n", cl_err);
	    return (1);
	}

	return (0);
}

// This file should never be compiled in kernel.
#ifdef _KERNEL_ORB
int
unode_init(void)
{
	os::printf("testclconf loaded\n");
	return (0);
}
#else
int
main(int argc, char *argv[])
{
	if (clconf_lib_init() != 0) {
		exit(1);
	}
	return (testclconf(argc, argv));
}
#endif // _KERNEL_ORB
