//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hashtab.cc	1.35	06/05/03 SMI"

// Hash Table common routines for hashtables

// This code is derived from the Mach Packet Filter (MPF)
// implemented in C and distributed as part of the Mach 3.0
// distribution.
// It has been completely restructured in an object oriented manner
// and reimplemented in C++.

/*
 * Mach Operating System
 * Copyright (c) 1993 Carnegie Mellon University
 * All Rights Reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 *
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 *
 * Carnegie Mellon requests users of this software to return to
 *
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 *
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */
/*
 * Berkeley Packet Filter Definitions from Berkeley
 */

/*
 * Copyright (c) 1990-1991 The Regents of the University of California.
 * All rights reserved.
 *
 * This code is derived from the Stanford/CMU enet packet filter,
 * (net/enet.c) distributed as part of 4.3BSD, and code contributed
 * to Berkeley by Steven McCanne and Van Jacobson both of Lawrence
 * Berkeley Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by the University of
 *      California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *      @(#)bpf.h       7.1 (Berkeley) 5/7/91
 *
 * @(#) $Header: bpf.h,v 2.2 93/08/10 15:11:16 mrt Exp $ (LBL)
 */


#include <sys/hashtab_def.h>
#include <sys/os.h>
#include <orb/infrastructure/orb.h>

_ListHashTable::_ListHashTable(uint_t num_buckets, char n[] /* = NULL */) :
	maxkey_count(HASH_KEY_COUNT), ref_count(0), hashtab_size(num_buckets)
{
	ASSERT(hashtab_size > 0);

#if defined(_KERNEL)
	table = new (os::NO_SLEEP) hash_list_t[hashtab_size];
#else
	table = new hash_list_t[hashtab_size];
#endif

	if (table == NULL) {
		//
		// SCMSGS
		// @explanation
		// The system attempted unsuccessfully to allocate a hash
		// table. There was insufficient memory.
		// @user_action
		// Install more memory, increase swap space, or reduce peak
		// memory consumption.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clutil: Can't allocate hash table");
	}

	id(n);
}

void
_ListHashTable::id(char n[])
{
	if ((n == NULL) || (os::strlen(n) >= 32)) {
		name[0] = 0;
	} else {
		(void) os::strcpy(name, n);
	}
}

_ListHashTable::hash_list_t&
_ListHashTable::hash(uint_t nkeys, uint_t keys[])
{
	ASSERT(nkeys <= HASH_KEY_COUNT);
	uint_t hash_index = 0;
	while (nkeys--) {
		hash_index += *keys++;
	}
	return (table[hash_index % hashtab_size]);
}

void *
_ListHashTable::_add(void *entry, uint_t nkeys, uint_t keys[],
		os::mem_alloc_type flag)
{
	ASSERT(entry != NULL);
	hash_list_t& bucket = hash(nkeys, keys);
	/*CSTYLED*/
	hash_entry_t *he = new (flag) hash_entry_t(entry, nkeys, keys);
	if (he != NULL) {
		bucket.prepend(he);
		ref_count++;
	}
	return (he);
}

_ListHashTable::hash_entry_t *
_ListHashTable::elookup(hash_list_t& bucket, uint_t nkeys, uint_t keys[])
{
	// Using an iterator will allow multiple elookups to happen in parallel
	hash_list_t::ListIterator iter(bucket);
	uint_t	 hash_index;
	hash_entry_t	*he_ptr;

	while ((he_ptr = iter.get_current()) != NULL) {
		for (hash_index = 0; hash_index < nkeys; hash_index++) {
			if (keys[hash_index] != he_ptr->keys[hash_index]) {
				break;
			}
		}

		if (hash_index == nkeys) {
			return (he_ptr);
		}
		iter.advance();
	}
	return (NULL);
}

void *
_ListHashTable::_remove(uint_t nkeys, uint_t keys[])
{
	hash_list_t& bucket = hash(nkeys, keys);
	hash_entry_t *he_ptr = elookup(bucket, nkeys, keys);

	if (he_ptr) {
		void *e = he_ptr->_entry_ptr;

		(void) bucket.erase(he_ptr);
		delete he_ptr;
		ref_count--;
		return (e);
	}
	return (NULL);
}


void *
_ListHashTable::_lookup(uint_t nkeys, uint_t keys[])
{
	hash_list_t& bucket = hash(nkeys, keys);
	hash_entry_t *he_ptr = elookup(bucket, nkeys, keys);
	if (he_ptr)
		return (he_ptr->_entry_ptr);
	return (NULL);
}

void
_ListHashTable::dumpTable()
{
	for (uint_t t = 0; t < hashtab_size; t++) {
		hash_list_t::ListIterator iter(table[t]);
		hash_entry_t *he_ptr;

		os::printf("Bucket %d : \n", t);
		while ((he_ptr = iter.get_current()) != NULL) {
			for (uint_t hash_index = 0; hash_index < HASH_KEY_COUNT;
			    hash_index++) {
				os::printf("%d ", he_ptr->keys[hash_index]);
			}
			os::printf("\n");
			iter.advance();
		}
	}
}


//
// Iterate through table, and for each element found pass its entry and
// callback_data to the callback function callbackf.  callbackf can stop
// the iteration before all elements have been processed by returning false.
//
// Callers are responsible for ensuring that the state of the hash table is
// not modified during the iteration.
//
void
_ListHashTable::hash_iterate(hash_callback_t callbackf, void *callback_data)
{
	for (uint_t t = 0; t < hashtab_size; t++) {
		hash_list_t::ListIterator iter(table[t]);
		hash_entry_t *he_ptr;

		while ((he_ptr = iter.get_current()) != NULL) {
			iter.advance();

			// Pass element's entry to callback function.
			// If function returns false, then end iteration.
			if (!callbackf(he_ptr->_entry_ptr, callback_data)) {
				return;
			}
		}
	}
}


//
// Iterate through table, and for each element found remove it from the table,
// and pass its entry and callback_data to the callback function callbackf.
// callbackf can stop the iteration before all elements have been
// processed by returning false.
//
// Callers are responsible for ensuring that the state of the hash table is
// not modified during the iteration.
//
void
_ListHashTable::hash_iterate_remove(
			hash_callback_t callbackf,
			void *callback_data)
{
	for (uint_t t = 0; t < hashtab_size; t++) {
		// We do not use an iterator here as things can get removed
		hash_list_t& bucket = table[t];
		hash_entry_t *he_ptr;

		while ((he_ptr = bucket.reapfirst()) != NULL) {
			void *entryp = he_ptr->_entry_ptr;	// save entry

			delete he_ptr;				// delete elem
			ref_count--;

			// Pass element's entry to callback function.
			// If function returns false, then end iteration.
			if (! callbackf(entryp, callback_data)) {
				return;
			}
		}
	}
}

//
// Advance to the next non NULL element.
//
void
_ListHashTable::Iterator::advance()
{
	if (_current != NULL)
		_current = _current->next();
	while ((_current == NULL) && (_index < (_hashtab_size - 1))) {
		_index++;
		_current = table[_index].first();
	}
}
