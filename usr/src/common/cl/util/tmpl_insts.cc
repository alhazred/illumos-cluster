//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)tmpl_insts.cc	1.21	08/05/20 SMI"

//
// This file contains template instance declarations that allow
// us to link template code without using a compiler generated
// template repository.  All code, other than this file, is compiled
// without template generation, this way the only instance of a
// instance class is generated from the declarations in this file.
//
// Some formatting is a bit particular due to cstyle complaints.  Make
// sure you carefully cstyle any modifications.
//

#include <unistd.h>
#include <errno.h>
#include <sys/os.h>

#include <sys/nodeset.h>
#include <orb/infrastructure/orb.h>
#include <orb/idl_datatypes/sequence.h>

template class _FixedSeq_<int>;
template class _FixedSeq_<unsigned int>;
template class _FixedSeq_<unsigned char>;
template class _FixedSeq_<unsigned short>;

/*
 * The following templates should be moved to their appropriate component
 * template file. They have remained here as there are still some
 * unresolved dependencies within cl_orb that need more investigation.
 * Moving these templates results in many undefined symbols when cl_orb
 * is loaded. Future work should focus on resolving those issues.
 */
#include <h/data_container.h>
template class proxy<data_container::data_stub>;
#include <nslib/data_container_handler.h>
template class impl_spec<data_container::data, data_container_handler>;

#include <orb/object/adapter.h>

/*
 * An attempt was made to move the replica templates to the ha_tmpl_insts.cc
 * file. In addition, the HAFRMWK_IDL_FILES were moved from cl_orb to
 * cl_haci. This was still not sufficient as components within cl_orb (orbtest
 * and fault_functions) still have dependencies on the ha framework. As
 * a result these templates have remained in the Orb.
 */
#include <h/replica.h>
template class _NormalSeq_<replica::priority_info>;
template class _NormalSeq_<replica::prov_dependency>;
template class _NormalSeq_<replica::service_state_info>;
template class _NormalSeq_<replica::repl_prov_info>;
template class _NormalSeq_<replica::service_info>;

#include <h/replica_int.h>
template class _NormalSeq_<replica_int::init_service_info>;
template class _FixedSeq_<replica_int::rma_service_state>;

template class _Ix_InterfaceSeq_ < replica::checkpoint,
    _A_field_<replica::checkpoint, replica::checkpoint *>,
    replica::checkpoint * >;

template class _Ix_InterfaceSeq_ < replica_int::cb_coord_control,
    _A_field_ < replica_int::cb_coord_control, replica_int::cb_coord_control *>,
    replica_int::cb_coord_control * >;

template class proxy<replica::checkpoint_stub>;
template class proxy<replica::repl_prov_reg_stub>;
template class proxy<replica::repl_prov_stub>;
template class proxy<replica::rm_admin_stub>;
template class proxy<replica::rma_public_stub>;
template class proxy<replica::service_admin_stub>;
template class proxy<replica::service_state_callback_stub>;
template class proxy<replica::trans_state_stub>;
template class proxy<replica::transaction_id_stub>;
template class proxy<replica_int::handler_repl_prov_stub>;
template class proxy<replica_int::reconnect_object_stub>;
template class proxy<replica_int::rm_service_admin_stub>;
template class proxy<replica_int::rm_stub>;
template class proxy<replica_int::rma_admin_stub>;
template class proxy<replica_int::rma_reconf_stub>;
template class proxy<replica_int::rma_repl_prov_stub>;
template class proxy<replica_int::rmm_stub>;
template class proxy<replica_int::tid_factory_stub>;
template class proxy<replica_int::cb_coord_control_stub>;

#include <h/repl_rm.h>
template class proxy<repl_rm::rm_ckpt_stub>;

/*
 * End of the addition of old templates.
 */


template class _Ix_InterfaceSeq_ < CORBA::Object, _A_field_ < CORBA::Object,
    CORBA::Object * >, CORBA::Object * >;

#include <orb/object/proxy.h>
#include <h/orbmsg.h>
#include <h/repl_sample.h>
#include <h/addrspc.h>

template class proxy<CORBA::Object_stub>;

#include <h/perf.h>
template class proxy<orbmsg::message_mgr_stub>;
template class proxy<perf::cbk_stub>;
template class proxy<perf::group_stub>;
template class proxy<perf::kstats_stub>;
template class proxy<perf::stats_stub>;
template class proxy<addrspc::addrspace_stub>;

#include <orb/object/adapter.h>
template class Anchor<orbmsg::message_mgr_stub>;
template class McServerof<addrspc::addrspace>;
template class McServerof<perf::stats>;
template class McNoref<orbmsg::message_mgr>;

#include <repl/service/replica_tmpl.h>
#include <sys/list_def.h>
#include <orb/refs/refcount.h>

#include <orb/xdoor/rxdoor_defs.h>
template class ref_jobs<rxdoor_ref>;
template class ref_jobs<translate_ack>;

#include <h/version.h>
template class McNoref<idlversion>;

template class proxy<idlversion_stub>;
template class Anchor<idlversion_stub>;
template class _FixedSeq_<inf_version_tbl_entry>;

template class _ManagedSeq_<_FixedSeq_<unsigned>, unsigned>;

template class _ManagedSeq_<_NormalSeq_<replica::repl_prov_info>,
    replica::repl_prov_info>;

template class _NormalSeq_<_FixedSeq_<unsigned char> >;
template class _NormalSeq_<_NormalSeq_<_FixedSeq_<unsigned char> > >;

template class _T_var_<_FixedSeq_<unsigned>, _FixedSeq_<unsigned>*>;
template class _T_var_<_NormalSeq_<replica::repl_prov_info>,
    _NormalSeq_<replica::repl_prov_info>*>;

#ifdef _FAULT_INJECTION
template class hash_table_t<FaultTriggers::trigger_t, 1U>;
template class hash_table_t<unsigned, 4U>;
#endif

#include <orb/object/schema.h>
template class schema_list<InterfaceDescriptor>;
template class schema_list<ExceptDescriptor>;

#ifdef _FAULT_INJECTION

#include <h/orbtest.h>
#include <h/orphan_fi.h>
#include <h/unref_fi.h>
#include <h/flowcontrol_fi.h>

template class proxy<orbtest::test_ckpt_stub>;
template class proxy<orbtest::test_mgr_stub>;
template class proxy<orbtest::test_obj_stub>;
template class proxy<orbtest::test_params_stub>;
template class proxy<orphan_fi::common_stub>;
template class proxy<orphan_fi::server_stub>;
template class proxy<orphan_fi::testobj_stub>;
template class proxy<flowcontrol_fi::node_stub>;
template class proxy<flowcontrol_fi::server_stub>;
template class proxy<unref_fi::server_stub>;
template class proxy<unref_fi::testobj_stub>;

#include <h/mc_sema.h>
template class proxy<mc_sema::server_stub>;
template class proxy<mc_sema::sema_stub>;
#include <orb/object/adapter.h>
template class McServerof<mc_sema::sema>;
template class McServerof<mc_sema::server>;

#include <h/ref_stress.h>
template class _NormalSeq_<ref_stress::test_param>;
template class proxy<ref_stress::client_stub>;
template class proxy<ref_stress::rsrc_balancer_stub>;
template class proxy<ref_stress::server_stub>;
template class proxy<ref_stress::testobj_stub>;

#include <h/ha_stress.h>
template class proxy<ha_stress::client_stub>;

#include <h/test_kproxy.h>
template class proxy<test_kproxy::server_stub>;

#endif // _FAULT_INJECTION
