//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)run_test.cc 1.5	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/os.h>
#include <sys/hashtab_def.h>
#include <sys/list_def.h>

const NUM_ENTRIES = 99999;

//
// Shadows each entry. For checking whether entry has been callbacked/disposed.
//
class shadow {
public:
	shadow() : _callbacked(false), _disposed(false) {}

	void	callbacked() { _callbacked = true; }
	bool	is_callbacked() { return (_callbacked); }

	void	disposed() { _disposed = true; }
	bool	is_disposed() { return (_disposed); }

	void		entry_id(uint32_t i) { _entry_id = i; }
	uint32_t	entry_id() { return (_entry_id); }

private:
	bool		_callbacked;
	bool		_disposed;
	uint32_t	_entry_id;
};

//
// Hash and lists entry
//
class entry {
public:
	entry(uint32_t i, shadow* ptr) : _id(i), _shadowp(ptr) {}
	~entry() {
		if (_shadowp->is_disposed()) {
			printf("FAIL: entry (id = %d) is disposed more than "
				"once\n", id());
		}
		_shadowp->disposed();
	}

	uint32_t id() { return (_id); }

	static bool callback(entry *entryp, void *cb_data) {
		if (entryp->_shadowp->is_callbacked()) {
			printf("FAIL: entry (id = %d) is callbacked more than "
				"once\n", entryp->id());
		}
		entryp->_shadowp->callbacked();

		int *datap = (int *)cb_data;
		++*datap;			// increment callback data

		return (true);			// continue iteration
	}

private:
	uint32_t	_id;
	shadow* _shadowp;
};

//
// IntrList entry templates
//
template<class L> class intrlist_elem : public entry, public L::ListElem {
public:
	intrlist_elem(uint32_t i, shadow* ptr) :
		entry(i, ptr), L::ListElem(this) {}

	static bool callback(intrlist_elem<L> *entryp, void *cb_data) {
		// Pass to entry::callback().
		return (entry::callback(entryp, cb_data));
	}
};


void test_hash();
void test_dlist();
void test_slist();
void test_intr_dlist();
void test_intr_slist();


main()
{
	test_hash();
	test_dlist();
	test_slist();
	test_intr_dlist();
	test_intr_slist();
}


void
test_hash()
{
	const	CBDATA_INIT = 1000;
	hash_table_t<entry, 1> hashtab;
	int	cbdata = CBDATA_INIT;
	shadow	shadows[NUM_ENTRIES];
	uint32_t	id;
	int		i;

	printf("\n=== Hash Table Test\n");

	//
	// Set up hash table.
	//
	for (i = 0, id = 0; i < NUM_ENTRIES; ++i, id += 2) {
		shadows[i].entry_id(id);
		entry *entryp = new entry(id, &shadows[i]);
		hashtab.add(entryp, &id);
	}
	if (hashtab.refcount() != NUM_ENTRIES) {
		printf("FAIL: refcount at start is %d instead of %d\n",
			hashtab.refcount(), NUM_ENTRIES);
	}

	//
	// Test iterate()
	//
	hashtab.iterate(entry::callback, &cbdata);

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_callbacked()) {
			printf("FAIL: entry (id = %d) wasn't callbacked\n",
				shadows[i].entry_id());
		}
	}
	if (hashtab.refcount() != NUM_ENTRIES) {
		printf("FAIL: refcount after iterate() is %d instead of %d\n",
			hashtab.refcount(), NUM_ENTRIES);
	}
	if (cbdata != (CBDATA_INIT + NUM_ENTRIES)) {
		printf("FAIL: callback data after iterate() is %d instead of "
			"%d\n", cbdata, CBDATA_INIT + NUM_ENTRIES);
	}

	//
	// Test dispose()
	//
	hashtab.dispose();

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_disposed()) {
			printf("FAIL: entry (id = %d) wasn't disposed\n",
				shadows[i].entry_id());
		}
	}
	if (hashtab.refcount() != 0) {
		printf("FAIL: refcount after dispose() is %d instead of %d\n",
			hashtab.refcount(), 0);
	}
}


void
test_dlist()
{
	const	CBDATA_INIT = 100;
	DList<entry> list;
	int	cbdata = CBDATA_INIT;
	shadow	shadows[NUM_ENTRIES];
	uint32_t	id;
	int		i;

	printf("\n=== DList Test\n");

	//
	// Set up list.
	//
	for (i = 0, id = 1; i < NUM_ENTRIES; ++i, id += 2) {
		shadows[i].entry_id(id);
		entry *entryp = new entry(id, &shadows[i]);
		list.prepend(entryp);
	}

	//
	// Test iterate()
	//
	list.iterate(entry::callback, &cbdata);

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_callbacked()) {
			printf("FAIL: entry (id = %d) wasn't callbacked\n",
				shadows[i].entry_id());
		}
	}
	if (cbdata != (CBDATA_INIT + NUM_ENTRIES)) {
		printf("FAIL: callback data after iterate() is %d instead of "
			"%d\n", cbdata, CBDATA_INIT + NUM_ENTRIES);
	}

	//
	// Test dispose()
	//
	list.dispose();

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_disposed()) {
			printf("FAIL: entry (id = %d) wasn't disposed\n",
				shadows[i].entry_id());
		}
	}
}


void
test_slist()
{
	const	CBDATA_INIT = 10;
	SList<entry> list;
	int	cbdata = CBDATA_INIT;
	shadow	shadows[NUM_ENTRIES];
	uint32_t	id;
	int		i;

	printf("\n=== SList Test\n");

	//
	// Set up list.
	//
	for (i = 0, id = 1; i < NUM_ENTRIES; ++i, ++id) {
		shadows[i].entry_id(id);
		entry *entryp = new entry(id, &shadows[i]);
		list.append(entryp);
	}
	if (list.count() != NUM_ENTRIES) {
		printf("FAIL: count at start is %d instead of %d\n",
			list.count(), NUM_ENTRIES);
	}

	//
	// Test iterate()
	//
	list.iterate(entry::callback, &cbdata);

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_callbacked()) {
			printf("FAIL: entry (id = %d) wasn't callbacked\n",
				shadows[i].entry_id());
		}
	}
	if (list.count() != NUM_ENTRIES) {
		printf("FAIL: count after iterate() is %d instead of %d\n",
			list.count(), NUM_ENTRIES);
	}
	if (cbdata != (CBDATA_INIT + NUM_ENTRIES)) {
		printf("FAIL: callback data after iterate() is %d instead of "
			"%d\n", cbdata, CBDATA_INIT + NUM_ENTRIES);
	}

	//
	// Test dispose()
	//
	list.dispose();

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_disposed()) {
			printf("FAIL: entry (id = %d) wasn't disposed\n",
				shadows[i].entry_id());
		}
	}
	if (list.count() != 0) {
		printf("FAIL: count after dispose() is %d instead of %d\n",
			list.count(), 0);
	}
}


void
test_intr_dlist()
{
	const	CBDATA_INIT = 1;
	typedef	intrlist_elem<_DList> listelem_t;

	IntrList<listelem_t, _DList> list;
	int	cbdata = CBDATA_INIT;
	shadow	shadows[NUM_ENTRIES];
	uint32_t	id;
	int		i;

	printf("\n=== IntrList (_DList::ListElem) Test\n");

	//
	// Set up list.
	//
	for (i = 0, id = NUM_ENTRIES; i < NUM_ENTRIES; ++i, --id) {
		shadows[i].entry_id(id);
		listelem_t *elemp = new listelem_t(id, &shadows[i]);
		list.prepend(elemp);
	}

	//
	// Test iterate()
	//
	list.iterate(listelem_t::callback, &cbdata);

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_callbacked()) {
			printf("FAIL: entry (id = %d) wasn't callbacked\n",
				shadows[i].entry_id());
		}
	}
	if (cbdata != (CBDATA_INIT + NUM_ENTRIES)) {
		printf("FAIL: callback data after iterate() is %d instead of "
			"%d\n", cbdata, CBDATA_INIT + NUM_ENTRIES);
	}

	//
	// Test dispose()
	//
	list.dispose();

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_disposed()) {
			printf("FAIL: entry (id = %d) wasn't disposed\n",
				shadows[i].entry_id());
		}
	}
}


void
test_intr_slist()
{
	const	CBDATA_INIT = -1000;
	typedef	intrlist_elem<_SList> listelem_t;

	IntrList<listelem_t, _SList> list;
	int	cbdata = CBDATA_INIT;
	shadow	shadows[NUM_ENTRIES];
	uint32_t	id;
	int		i;

	printf("\n=== IntrList (_SList::ListElem) Test\n");

	//
	// Set up list.
	//
	for (i = 0, id = ~0; i < NUM_ENTRIES; ++i, --id) {
		shadows[i].entry_id(id);
		listelem_t *elemp = new listelem_t(id, &shadows[i]);
		list.prepend(elemp);
	}
	if (list.count() != NUM_ENTRIES) {
		printf("FAIL: count at start is %d instead of %d\n",
			list.count(), NUM_ENTRIES);
	}

	//
	// Test iterate()
	//
	list.iterate(listelem_t::callback, &cbdata);

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_callbacked()) {
			printf("FAIL: entry (id = %d) wasn't callbacked\n",
				shadows[i].entry_id());
		}
	}
	if (list.count() != NUM_ENTRIES) {
		printf("FAIL: count after iterate() is %d instead of %d\n",
			list.count(), NUM_ENTRIES);
	}
	if (cbdata != (CBDATA_INIT + NUM_ENTRIES)) {
		printf("FAIL: callback data after iterate() is %d instead of "
			"%d\n", cbdata, CBDATA_INIT + NUM_ENTRIES);
	}

	//
	// Test dispose()
	//
	list.dispose();

	for (i = 0; i < NUM_ENTRIES; ++i) {
		if (! shadows[i].is_disposed()) {
			printf("FAIL: entry (id = %d) wasn't disposed\n",
				shadows[i].entry_id());
		}
	}
	if (list.count() != 0) {
		printf("FAIL: count after dispose() is %d instead of %d\n",
			list.count(), 0);
	}
}
