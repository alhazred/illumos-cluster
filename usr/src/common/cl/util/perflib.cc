//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)perflib.cc	1.13	08/05/20 SMI"

#include <sys/perflib.h>
#include <sys/os.h>
#include <sys/sysmacros.h>

// Check if enough space in string whatever is being added to it
// Added one in check to be safe about string terminator
#define	PACKER_CHECK_EXPAND(sz) {\
	if ((head + (sz) + 1) >= size) {\
		size = roundup(head + (sz) + 1, CHUNK);\
		char *p = new char[size];\
		if (s != NULL) {\
			(void) os::strcpy(p, s);\
			delete [] s;\
		}\
		s = p;\
	}\
}

// Check if enough space in string to hold number (plus sign and trailing space)
packer &packer::operator<<(int x) {
	PACKER_CHECK_EXPAND(12)
	os::sprintf(s+head, "%d ", x);
	ASSERT(os::strlen(s+head) <= 12);
	head += os::strlen(s+head);
	return (*this);
}

packer &packer::operator<<(unsigned int x) {
	PACKER_CHECK_EXPAND(11)
	ASSERT(os::strlen(s+head) <= 11);
	os::sprintf(s+head, "%u ", x);
	head += os::strlen(s+head);
	return (*this);
}

packer &packer::operator<<(longlong_t x) {
	PACKER_CHECK_EXPAND(23)
	os::sprintf(s+head, "%ll ", x);
	ASSERT(os::strlen(s+head) <= 23);
	head += os::strlen(s+head);
	return (*this);
}

packer &packer::operator<<(u_longlong_t x) {
	PACKER_CHECK_EXPAND(22)
	os::sprintf(s+head, "%llu ", x);
	ASSERT(os::strlen(s+head) <= 22);
	head += os::strlen(s+head);
	return (*this);
}

packer &packer::operator<<(double x) {
	PACKER_CHECK_EXPAND(12)
	os::sprintf(s+head, "%7.3f ", x);
	ASSERT(os::strlen(s+head) <= 12);
	head += os::strlen(s+head);
	return (*this);
}

packer &packer::operator<<(char *x) {
// Check if space for x in string (including {} and space)
	size_t n = os::strlen(x) + 3;
	PACKER_CHECK_EXPAND(n);
	os::sprintf(s+head, "{%s} ", x);
	head += n;
	return (*this);
}

char *packer::str() {
	char *p = s;
	s = NULL;
	size = 0;
	head = 0;
	return (p);
}
