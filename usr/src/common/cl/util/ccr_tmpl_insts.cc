//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ccr_tmpl_insts.cc	1.2	08/05/20 SMI"

#include <h/ccr.h>
#include <h/ccr_trans.h>
#include <orb/object/adapter.h>

template class McServerof<ccr::callback>;
template class McServerof<ccr::directory>;
template class McServerof<ccr::readonly_table>;
template class McServerof<ccr_data::data_server>;
template class McServerof<ccr_data::updatable_table_copy>;

#include <repl/service/replica_tmpl.h>
template class mc_checkpoint<ccr_trans::ckpt>;
template class mc_replica_of<ccr::updatable_table>;
template class mc_replica_of<ccr_trans::handle>;
template class mc_replica_of<ccr_trans::manager>;
template class repl_server<ccr_trans::ckpt>;
template class _ManagedSeq_<_NormalSeq_<ccr::table_element>,
    ccr::table_element>;
template class _T_var_<_NormalSeq_<ccr::table_element>,
    _NormalSeq_<ccr::table_element>*>;

#include <h/component_state.h>
template class McServerof<component_state::registry>;

template class _NormalSeq_<ccr::table_element>;

template class _FixedSeq_<ccr_data::consis>;
template class _Ix_InterfaceSeq_ < ccr::callback, _A_field_ < ccr::callback,
    ccr::callback * >, ccr::callback * >;
template class _Ix_InterfaceSeq_ < ccr_data::updatable_table_copy,
    _A_field_ < ccr_data::updatable_table_copy,
    ccr_data::updatable_table_copy * >, ccr_data::updatable_table_copy * >;

template class proxy<ccr::callback_stub>;
template class proxy<ccr::directory_stub>;
template class proxy<ccr::readonly_table_stub>;
template class proxy<ccr::updatable_table_stub>;
template class proxy<ccr_data::data_server_stub>;
template class proxy<ccr_data::updatable_table_copy_stub>;
template class proxy<ccr_trans::ckpt_stub>;
template class proxy<ccr_trans::handle_stub>;
template class proxy<ccr_trans::manager_stub>;

template class _NormalSeq_<component_state::component_state_t>;

template class _T_var_<_NormalSeq_<component_state::component_state_t>,
    _NormalSeq_<component_state::component_state_t> *>;
template class _T_var_ < component_state::component_state_t,
    component_state::component_state_t *>;

template class _ManagedSeq_<_NormalSeq_<component_state::component_state_t>,
    component_state::component_state_t>;

template class proxy<component_state::registry_stub>;

//
// clevents
//
#include <h/clevent.h>

template class proxy<clevent::clevent_comm_stub>;
