//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)rgm_tmpl_insts.cc	1.7	08/07/22 SMI"

#include <h/rgm.h>

template class _NormalSeq_<rgm::r_state>;
template class _NormalSeq_<rgm::rg_state>;
template class _NormalSeq_<rgm::idl_nameval>;
template class _NormalSeq_<rgm::idl_nameval_node>;
template class _NormalSeq_<rgm::lni_mappings>;
template class _NormalSeq_<rgm::lni_t>;
template class _NormalSeq_<rgm::zone_state>;

template class proxy<rgm::rgm_comm_stub>;

#include <h/quantum_leap.h>
template class proxy<quantum_leap::ql_obj_stub>;
template class proxy<quantum_leap::ql_rgm_obj_stub>;

#include <h/ifconfig_proxy.h>
template class proxy<ifconfig_proxy::ifconfig_server_stub>;

#include <h/rtreg_proxy.h>
template class proxy<rtreg_proxy::rtreg_server_stub>;

#include <h/pnm_mod.h>

template class _FixedSeq_<pnm_mod::inaddr_t>;
template class _NormalSeq_<pnm_mod::pnm_adp_status>;
template class _NormalSeq_<pnm_mod::pnm_adapterinfo>;

template class proxy<pnm_mod::pnm_server_stub>;

#include <h/ns_test.h>
template class proxy<ns_test::ns_test_obj_stub>;
template class proxy<ns_test::ns_test_local_obj_stub>;
