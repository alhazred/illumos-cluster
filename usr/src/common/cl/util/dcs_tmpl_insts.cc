//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dcs_tmpl_insts.cc	1.3	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <repl/service/replica_tmpl.h>

#include <h/dc.h>
template class _NormalSeq_<mdc::service_property>;
template class _NormalSeq_<mdc::device_service_info>;
template class _FixedSeq_<mdc::dev_range>;
template class _FixedSeq_<mdc::node_preference>;

#include <h/repl_dc.h>
template class proxy<mdc::dc_config_stub>;
template class proxy<mdc::dc_mapper_stub>;
template class proxy<mdc::device_server_stub>;
template class proxy<mdc::device_service_manager_stub>;
template class proxy<mdc::reservation_client_stub>;
template class proxy<repl_dc::repl_dc_ckpt_stub>;
template class proxy<repl_dc::repl_dev_server_ckpt_stub>;
template class McServerof<mdc::device_server>;
template class McServerof<mdc::device_service_manager>;
template class mc_replica_of<mdc::device_server>;
template class _ManagedSeq_<_NormalSeq_<mdc::device_service_info>,
    mdc::device_service_info>;

template class _NormalSeq_<repl_dc_data::dc_contig_chunk_info>;
template class _NormalSeq_<repl_dc_data::dc_instance_alloc_info>;
template class _FixedSeq_<repl_dc_data::dc_map_file_ent_info>;
template class _NormalSeq_<repl_dc_data::dc_map_file_info>;
template class _NormalSeq_<repl_dc_data::dc_min_alloc_info>;
template class _T_var_<_NormalSeq_<mdc::device_service_info>,
    _NormalSeq_<mdc::device_service_info>*>;
template class repl_server<repl_dc::repl_dc_ckpt>;

template class repl_server<repl_dc::repl_dev_server_ckpt>;

template class mc_replica_of<mdc::dc_config>;
template class mc_replica_of<mdc::dc_mapper>;

#include <h/sol.h>
template class _FixedSeq_<sol::aclent_t>;
template class proxy<solobj::cred_stub>;

#include <solobj/solobj_handler.h>
template class impl_spec<solobj::cred, Solobj_handler_cred>;
