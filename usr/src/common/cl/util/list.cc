//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)list.cc	1.38	08/05/20 SMI"

#include <sys/list_def.h>

// Rules for _DList/SList class
// _first and _last point to the first and last list elements
// _current points to the current position of the cursor.
// _current is NULL => cursor is at end of list
// When the list is empty, all three (current, first, last) are NULL.
// When first element is added (through any op), all three are set to it
//	i.e., current is at first element of list.
// XX Wouldnt it be better to set current to element being added (not just first
// XX Also it may be better to avoid using the inbuilt iterator and instead
// use the ListIterator template. It will make the lists more efficient

// find_elem
// Internal function used to find the list element containing t.
// If the same pointer was added twice to the list, the behavior is undefined
_DList::ListElem *
_DList::find_elem(void *t)
{
	ListElem *le;

	// Optimize for the case where list currently points to elem
	if ((_current != NULL) && (_current->_elem == t))
		return (_current);

	for (le = _first; (le != NULL); le = le->_next) {
		if (le->_elem == t)
			break;
	}
	return (le);
}

// append
void
_DList::_append(ListElem *le)
{
	le->ptr_assert();

	le->_next = NULL;

	if ((le->_prev = _last) != NULL) {
		// If list was non-empty
		_last->_next = le;
		if (_current == NULL) {
			_current = le;
			// current was at end of list - le is now end of list
		}
	} else {
		ASSERT(empty());
		_first = _current = le;
	}
	_last = le;
}

// prepend
void
_DList::_prepend(ListElem *le)
{
	le->ptr_assert();

	le->_prev = NULL;

	if ((le->_next = _first) != NULL) {
		// list was non-empty
		_first->_prev = le;
	} else {
		ASSERT(empty());
		_last = _current = le;
	}
	_first = le;
}

//
// _erase_internal (common code to two types of erase on _DLists)
// delete ListElem if del is true.
//
bool
_DList::_erase_internal(ListElem *le, bool del)
{
	ListElem	*pe;	// prior element
	ListElem	*ne;	// next element

	if (le == NULL) {
		return (false);
	}
	pe = le->_prev;
	ne = le->_next;
	if (pe != NULL) {
		pe->_next = ne;
	} else {
		_first = ne;
	}
	if (ne != NULL) {
		ne->_prev = pe;
	} else {
		_last = pe;
	}
	if (_current == le) {
		_current = ne;
	}
	if (del) {
		delete le;
	} else {
		le->ptr_reset();
	}
	return (true);
}

// reapfirst
// Deletes ListElem if del is true
void *
_DList::_reapfirst(bool del)
{
	ListElem *le;

	if ((le = _first) == NULL) {
		return (NULL);
	}
	// inline erase(el, del);
	void *el = le->_elem;
	_first = le->_next;
	if (_first == NULL) {
		_current = _last = NULL;
	} else {
		_first->_prev = NULL;
		if (_current == le) {
			_current = _first;
		}
	}
	if (del) {
		delete le;
	} else {
		le->ptr_reset();
	}
	return (el);
}

// insert_b
void
_DList::_insert_b(ListElem *le)
{
	le->ptr_assert();
	if (_current == _first) { // Handles prepend to empty list also
		_prepend(le);
		return;
	}

	if (_current == NULL) {
		// if inserting into existing list with current as NULL
		// Leave _current at NULL (i.e., at end of list)
		_append(le);
		_current = NULL;
		return;
	}

	ListElem *pe = _current->_prev;

	pe->_next = le;
	_current->_prev = le;

	le->_next = _current;
	le->_prev = pe;
}

// insert_a
void
_DList::_insert_a(ListElem *le)
{
	le->ptr_assert();

	if ((_current == _last) || (_current == NULL)) {
		// Handles empty list and where _current is at or after last
		_append(le);
		return;
	}

	ListElem *ne = _current->_next;

	_current->_next = le;
	ne->_prev = le;
	le->_prev = _current;
	le->_next = ne;
}

// split
void
_DList::split(_DList& l)
{
	// Assumes argument l is empty
	// Returns suffix in l and leaves prefix
	// If current is NULL, returns empty list in l

	ASSERT(l.empty());
	if (_current != NULL) {
		l._first = l._current = _current;
		l._last = _last;
		_last = _current->_prev;
		_current->_prev = NULL;
	} else {
		// do nothing. l is already empty
		ASSERT(l._first == NULL);
		ASSERT(l._current == NULL);
		ASSERT(l._last == NULL);
	}

	if (_last != NULL) {
		_current = _first;
		_last->_next = NULL;
	} else {
		_first = _current = NULL;
	}
}

// backsplit
void
_DList::backsplit(_DList& l)
{
	// Assumes argument l is empty
	// Returns prefix in l and leaves tail
	// If current is NULL, returns full list in l and makes empty

	ASSERT(l.empty());
	l._first = l._current = _first;
	if (_current != NULL) {
		l._last = _current->_prev;
		_first = _current;
		_current->_prev = NULL;
	} else {
		l._last = _last;
		_last = _first = NULL;
		ASSERT(empty());
	}

	if (l._last != NULL) {
		l._last->_next = NULL;
	} else {
		l._first = l._current = NULL;
	}
}

// concat
void
_DList::concat(_DList& l)
{
	// Sets l to empty list after moving it to tail of this
	// Sets current to first element of l
	// If l is empty sets current to NULL (end of list)

	if (l.empty()) {
		_current = NULL;
	} else {
		if (_last != NULL) {
			_last->_next = l._first;
			l._first->_prev = _last;
		} else {
			_first = l._first;
		}
		_current = l._first;
		_last = l._last;
		l._first = l._last = l._current = NULL;
	}
}

// count
uint_t
_DList::count()
{
	ListElem *le;
	uint_t cnt = 0;

	for (le = _first; le != NULL; le = le->_next) {
		cnt++;
	}
	return (cnt);
}

//
// Iterate through list, and for each ListElem found pass its entry and
// callback_data to the callback function callbackf. callbackf can stop
// the iteration before all ListElems have been processed by returning false.
// If the iteration stops in this way, the function returns the element where
// it stopped, otherwise it returns NULL.
//
// This function can be used when one or more iterations are needed (in
// parallel or serially) without changing the state (_first, _last, and
// _current) of the list. Compare this to the other iterator functions,
// e.g. atfirst(), advance(), etc. However, callers are responsible for
// ensuring that the state of the list is not modified during the iteration,
// e.g. by using locks.
//
void *
_DList::list_iterate(void *callbackf_in, void *callback_data)
{
	ListElem *ptr;

	list_callback_t callbackf =
		(list_callback_t)callbackf_in; //lint !e611

	for (ptr = _first; ptr != NULL; ptr = ptr->_next) {
		if (!callbackf(ptr->_elem, callback_data)) {
			// Return the element that is causing iteration to stop.
			return (ptr->_elem);
		}
	}
	return (NULL);
}


// Rules for _SList class
// _first and _last point to the first and last list elements
// _current points to the current position of the cursor.
// _current is NULL => cursor is at end of list
// When the list is empty, all three (current, first, last) are NULL.
// When first element is added (through any op), all three are set to it
//	i.e., current is at first element of list.

// append
void
_SList::_append(ListElem *le)
{
	le->ptr_assert();
	le->_next = NULL;

	if (_last != NULL) {
		_last->_next = le;
	} else {
		ASSERT(empty());
		_first = _current = le;
	}
	if (_current == NULL) {
		_current = le;
		// current was at end of list - le is now end of list
	}
	_last = le;
}


// prepend
void
_SList::_prepend(ListElem *le)
{
	le->ptr_assert();
	if ((le->_next = _first) == NULL) {
		ASSERT(empty());
		_last = _current = le;
	}
	_first = le;
}

// erase
// This version takes as input a (void *) and find the ListElem
// delete ListElem containing elem t also
bool
_SList::_erase_elem(void *t)
{
	ListElem *le, *pe, *ne;

	le = find_elem(t, &pe);
	if (le == NULL) {
		return (false);
	}
	ne = le->_next;
	if (pe != NULL) {
		pe->_next = ne;
	} else {
		_first = ne;
	}
	if (ne == NULL) {
		_last = pe;
	}
	if (_current == le) {
		_current = ne;
	}
	delete le;
	return (true);
}

//
// _erase_listelem
// This version takes as input a pointer to a listElem (e.g, IntrLists)
// Does not delete the ListElem
//
bool
_SList::_erase_listelem(ListElem *le)
{
	ListElem	*pe;	// prior element
	ListElem	*ne;	// next element

	ASSERT(le != NULL);	// For IntrLists this cannot be NULL

	pe = find_prev(le);

	//
	// pe can be NULL for two reasons, le does not exist in list
	// or le == _first. Return false if le does not exist in list
	//
	if ((pe == NULL) && (_first != le)) {
		return (false);
	}
	ne = le->_next;
	if (pe != NULL) {
		pe->_next = ne;
	} else {
		_first = ne;
	}
	if (ne == NULL) {
		_last = pe;
	}
	if (_current == le) {
		_current = ne;
	}
	le->ptr_reset();
	return (true);
}

// reapfirst
// Deletes ListElem if del is true
void *
_SList::_reapfirst(bool del)
{
	ListElem *le;

	if ((le = _first) == NULL) {
		return (NULL);
	}
	// inline erase(el, del);
	void *el = le->_elem;
	_first = le->_next;
	if (_first == NULL) {
		_current = _last = NULL;
	} else if (_current == le) {
		_current = _first;
	}
	if (del) {
		delete le;
	} else {
		le->ptr_reset();
	}
	return (el);
}

// find_prev
_SList::ListElem *
_SList::find_prev(ListElem *le)
{
	ListElem *pe;

	// Return NULL if le is first element
	if (_first == le)
		return (NULL);

	for (pe = _first; pe != NULL; pe = pe->_next) {
		if (pe->_next == le)
			break;
	}
	return (pe);
}

// insert_b
void
_SList::_insert_b(ListElem *le)
{
	le->ptr_assert();
	if (_current == _first) { // Handles prepend to empty list also
		_prepend(le);
		return;
	}

	if (_current == NULL) {
		// if inserting into existing list with current as NULL
		// Leave _current at NULL (i.e., at end of list)
		_append(le);
		_current = NULL;
		return;
	}

	ListElem *pe = find_prev(_current);
	ASSERT(pe->_next == _current);

	pe->_next = le;
	le->_next = _current;
}

// insert_a
void
_SList::_insert_a(ListElem *le)
{
	le->ptr_assert();
	if ((_current == _last) || (_current == NULL)) {
		// Handles empty list, and where _current is at or after _last
		_append(le);
		return;
	}

	le->_next = _current->_next;
	_current->_next = le;
}

// find_elem
// pe returns a pointer to the previous element if t is found in the list
// if t is not found on list, pe is undefined
_SList::ListElem *
_SList::find_elem(void *t, ListElem **pe)
{
	ListElem *le;

	if (pe != NULL) {
		*pe = NULL;
		for (le = _first; le != NULL; le = le->_next) {
			if (le->_elem == t) {
				break;
			}
			*pe = le;
		}
	} else {
		// Optimize for the case where list currently points to elem
		if ((_current != NULL) && (_current->_elem == t))
			return (_current);

		for (le = _first; le != NULL; le = le->_next) {
			if (le->_elem == t)
				break;
		}
	}
	return (le);
}


// split
void
_SList::split(_SList& l)
{
	// Assumes argument l is empty
	// Returns suffix in l and leaves prefix
	// If current is NULL, returns empty list in l

	ASSERT(l.empty());
	if (_current != NULL) {
		l._first = l._current = _current;
		l._last = _last;
		_last = find_prev(_current);
	} else {
		// do nothing. l is already empty
		ASSERT(l._first == NULL);
		ASSERT(l._current == NULL);
		ASSERT(l._last == NULL);
	}

	if (_last != NULL) {
		_current = _first;
		_last->_next = NULL;
	} else {
		_first = _current = NULL;
	}
}

// backsplit
void
_SList::backsplit(_SList& l)
{
	// Assumes argument l is empty
	// Returns prefix in l and leaves tail
	// If current is NULL, returns full list in l and makes empty

	ASSERT(l.empty());
	l._first = l._current = _first;
	if (_current != NULL) {
		l._last = find_prev(_current);
		_first = _current;
	} else {
		l._last = _last;
		_last = _first = NULL;
		ASSERT(empty());
	}

	if (l._last != NULL) {
		l._last->_next = NULL;
	} else {
		l._first = l._current = NULL;
	}
}

// concat
void
_SList::concat(_SList& l)
{
	// Sets l to empty list after moving it to tail of this
	// Sets current to first element of l
	// If l is empty sets current to NULL (end of list)

	if (l.empty()) {
		_current = NULL;
	} else {
		if (_last != NULL) {
			_last->_next = l._first;
		} else {
			_first = l._first;
		}
		_current = l._first;
		_last = l._last;
		l._first = l._last = l._current = NULL;
		ASSERT(l.empty());
	}
}

// count
uint_t
_SList::count()
{
	ListElem *le;
	uint_t cnt = 0;

	for (le = _first; le != NULL; le = le->_next) {
		cnt++;
	}
	return (cnt);
}

//
// Iterate through list, and for each ListElem found pass its entry and
// callback_data to the callback function callbackf. callbackf can stop
// the iteration before all ListElems have been processed by returning false.
//
// This function can be used when one or more iterations are needed (in
// parallel or serially) without changing the state (_first, _last, and
// _current) of the list. Compare this to the other iterator functions,
// e.g. atfirst(), advance(), etc. However, callers are responsible for
// ensuring that the state of the list is not modified during the iteration,
// e.g. by using locks.
//
void *
_SList::list_iterate(void *callbackf_in, void *callback_data)
{
	ListElem *ptr;

	list_callback_t callbackf =
		(list_callback_t)callbackf_in; //lint !e611

	for (ptr = _first; ptr != NULL; ptr = ptr->_next) {
		if (!callbackf(ptr->_elem, callback_data)) {
			// Return the element that is causing iteration to stop.
			return (ptr->_elem);
		}
	}
	return (NULL);
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <sys/list_def_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
