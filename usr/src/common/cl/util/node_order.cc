/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)node_order.cc	1.3	08/05/20 SMI"

#include <sys/node_order.h>

node_order::node_order(void)
{
	nodeid = NODEID_UNKNOWN;
}

node_order::node_order(nodeid_t nid)
{
	set(nid);
}

// cmp can only be used on valid nodeids
int
node_order::cmp(nodeid_t a, nodeid_t b)
{
	ASSERT(a > 0 && a <= NODEID_MAX);
	ASSERT(b > 0 && b <= NODEID_MAX);
	if (a == b)
		return (0);
	if (a > b)
		return (1);
	return (-1);
}

int
node_order::cmp(nodeid_t a)
{
	ASSERT(a > 0 && a <= NODEID_MAX);
	ASSERT(valid());
	if (nodeid == a)
		return (0);
	if (nodeid < a)
		return (1);
	return (-1);
}

int
node_order::cmp(node_order &a)
{
	return (cmp(a.current()));
}
