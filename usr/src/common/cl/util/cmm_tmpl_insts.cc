//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cmm_tmpl_insts.cc	1.8	08/07/17 SMI"

#include <h/cmm.h>
template class proxy<cmm::automaton_stub>;
template class proxy<cmm::callback_registry_stub>;
template class proxy<cmm::callback_stub>;
template class proxy<cmm::comm_stub>;
template class proxy<cmm::control_stub>;
template class proxy<cmm::kernel_ucmm_agent_stub>;
template class proxy<cmm::remote_ucmm_proxy_stub>;
template class proxy<cmm::userland_cmm_stub>;

#include <orb/object/adapter.h>

template class McServerof<cmm::automaton>;
template class McServerof<cmm::callback>;
template class McServerof<cmm::callback_registry>;
template class McServerof<cmm::comm>;
template class McServerof<cmm::control>;
template class McServerof<cmm::kernel_ucmm_agent>;
#include <h/ff.h>
template class McServerof<ff::failfast>;
template class McServerof<ff::failfast_admin>;

template class proxy<ff::failfast_admin_stub>;
template class proxy<ff::failfast_stub>;

#include <h/remote_exec.h>
template class proxy<remote_exec::cl_exec_stub>;

#include <h/membership.h>
template class McServerof<membership::client>;
template class proxy<membership::client_stub>;
template class McServerof<membership::api>;
template class proxy<membership::api_stub>;
template class McServerof<membership::engine>;
template class proxy<membership::engine_stub>;
template class McServerof<membership::manager>;
template class proxy<membership::manager_stub>;
template class _NormalSeq_<membership::membership_manager_info>;
