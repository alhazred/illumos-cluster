//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mc_sema_impl.cc	1.22	08/05/27 SMI"

#include	<nslib/ns.h>
#include	<sys/mc_sema_impl.h>
#include	<sys/cl_comm_support.h>

//
// mc_sema_server_impl
//

//
// Constructor will initialize the count of the sync object
//
// Initialize table.
//
mc_sema_server_impl::mc_sema_server_impl() :
	McServerof<mc_sema::server>()
{
	// Intiialize the table
	for (uint_t count = 0; count < MAX_MC_SEMA; count++) {
		// No need to use the lock during constructor
		sema_table[count].count = 0;
		sema_table[count].in_use = false;
	}

	// Mark the first slot as in use -- Kludge for a bad programming
	// error where id 0 means no id
	sema_table[0].in_use = true;
}

//
// IDL Members of sema server
//
// create_sema is invoked by the client when an init is invoked and no cookie
// was provided to the constructor
// create will find an empty slot on the table and allocate it.
// On a succesful allocation the cookie is returned, otherwise 0 is returned
//
mc_sema::cookie
mc_sema_server_impl::create_sema(Environment &)
{
	uint_t	count;

	// Find an unused entry in the table
	for (count = 0; count < MAX_MC_SEMA; count++) {
		sema_table[count].lck.lock();
		if (!sema_table[count].in_use)
			break;
		sema_table[count].lck.unlock();
	}

	// If we reached the end, we didn't find an open slot
	if (count == MAX_MC_SEMA) {
		return (0);
	}

	// We found an open slot.. and are still holding its lock
	// Allocate a new internal sema type
	sema_table[count].count = 0;
	sema_table[count].in_use = true;
	sema_table[count].lck.unlock();

	return (count);
}


//
// destroy is invoked by the client to de-allocate the slot on the table
// destroy will also release any clients that are sleeping on this semaphore
//
void
mc_sema_server_impl::destroy(mc_sema::cookie id, Environment &e)
{
	// Raise exception if over the limit
	if (id >= MAX_MC_SEMA) {
		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return;
	}

	// Examine the entry
	sema_table[id].lck.lock();

	// Make sure this entry is in use
	if (!sema_table[id].in_use) {
		sema_table[id].lck.unlock();
		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return;
	}

	// mark the slot open
	sema_table[id].in_use = false;

	mc_sema::sema_ptr	client_ref;

	// Walk through all the clients waiting and wake them
	while (is_not_nil(client_ref = sema_table[id].client_l.reapfirst())) {
		client_ref->wakeup(e);
		CORBA::release(client_ref);

		if (e.exception() != NULL) {
			// The mc_sema object is about to be destroyed
			// and we are waking up all clients. If there
			// are any exceptions we neglect them.
			// There should be only client dead exceptions
			// In case there are others we need to understand them
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()) ||
			    CORBA::INV_OBJREF::_exnarrow(e.exception()));
			e.clear();
		}
	}

	sema_table[id].lck.unlock();
	ASSERT(!e.exception());
}


//
// init will set the value of the sema count
//
// It will then examine the list of waiting clients and wakeup as many
// as it can due to the new value of the count
//
void
mc_sema_server_impl::init(uint32_t value, mc_sema::cookie id, Environment &e)
{
	// Raise exception if over the limit
	if (id >= MAX_MC_SEMA) {
		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return;
	}

	// Examine the entry
	sema_table[id].lck.lock();

	// Make sure that this entry is in use
	if (!sema_table[id].in_use) {
		sema_table[id].lck.unlock();

		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return;
	}

	// Now set the value of the count to the
	sema_table[id].count = value;

	mc_sema::sema_ptr	client_ref;

	// Release any clients that may have been waiting.  (Up the allowable
	// count value)
	while (sema_table[id].count != 0) {
		// Get the client reference and wake him up
		client_ref = sema_table[id].client_l.reapfirst();
		if (CORBA::is_nil(client_ref)) {
			// No more clients, just break
			break;
		}

		client_ref->wakeup(e);
		CORBA::release(client_ref);

		if (e.exception() != NULL) {
			// Exception - client dead
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()) ||
			    CORBA::INV_OBJREF::_exnarrow(e.exception()));

			// Dont decrement... try again with next client
			e.clear();
			continue;
		} // Exception

		// Decrement count on successful wakeup
		sema_table[id].count--;
	} // count != 0

	sema_table[id].lck.unlock();
	ASSERT(!e.exception());
}


//
// server side of the sema_p operation
// The count will be decremented, if its already 0, then the sema_p
// operation will return true, and the client should go to sleep
// at a later time the server will receive a sema_v operation on this mc_sema
// object (cookie), and then it will wake up the client.
//
bool
mc_sema_server_impl::sema_p(
	mc_sema::sema_ptr client_ref,
	mc_sema::cookie id,
	Environment &e)
{
	// Raise exception if over the limit
	if (id >= MAX_MC_SEMA) {
		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return (false);
	}

	// Examine the entry
	sema_table[id].lck.lock();

	// Make sure that this entry is in use
	if (!sema_table[id].in_use) {
		sema_table[id].lck.unlock();

		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return (false);
	}

	// Examine if the count is already 0
	if (sema_table[id].count == 0) {
		// Add this client to the end of the client list
		mc_sema::sema_ptr	client_ptr;

		client_ptr = mc_sema::sema::_duplicate(client_ref);
		ASSERT(!CORBA::is_nil(client_ptr));

		// Save this client reference in our list
		sema_table[id].client_l.append(client_ptr);

		sema_table[id].lck.unlock();

		// Returning true should make client go to sleep
		return (true);
	}

	ASSERT(sema_table[id].count > 0);

	// Decrement the count
	sema_table[id].count--;

	sema_table[id].lck.unlock();

	// Returning false will keep the client from going to sleep
	return (false);
}

//
// The sema_v operation on a  mc_sema object (server side)
// increments the count and wakes any waiting client. (only one!)
//
void
mc_sema_server_impl::sema_v(mc_sema::cookie id, Environment &e)
{
	// Raise exception if over the limit
	if (id >= MAX_MC_SEMA) {
		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return;
	}

	// Examine the entry
	sema_table[id].lck.lock();

	// Make sure that this entry is in use
	if (!sema_table[id].in_use) {
		sema_table[id].lck.unlock();

		// Set the exception
		e.exception(new mc_sema::invalid_cookie);
		return;
	}

	// If there are clients waiting, just wake one up
	// (and dont change the count)
	mc_sema::sema_ptr	client_ref;


	// Nested invocations require their own Environment
	Environment	env;

	//
	// Keep on waking clients up if we get exceptions
	// until we run out of clients
	//
	while (is_not_nil(client_ref = sema_table[id].client_l.reapfirst())) {
		client_ref->wakeup(env);
		CORBA::release(client_ref);

		if (env.exception() == NULL) {
			// No error, we woke up some client
			sema_table[id].lck.unlock();
			return;
		}
		//
		// In case of any exception, client was not woken up
		// so try next client.
		//
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()) ||
		    CORBA::INV_OBJREF::_exnarrow(env.exception()));
		env.clear();
	}
	//
	// In case there are no waiting clients or all clients are dead
	// Increase the count on the semaphore
	//
	sema_table[id].count++;

	sema_table[id].lck.unlock();
	ASSERT(!e.exception());
}


//
// no_op - dummy operation used by the client to test server existence.
//
void
mc_sema_server_impl::no_op(mc_sema::cookie, Environment &)
{
}

void
#ifdef DEBUG
mc_sema_server_impl::_unreferenced(unref_t c)
#else
mc_sema_server_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(c));
	delete this;
}

//
// End User mc sema object implementation
//

#if defined(_KERNEL_ORB)

int
mc_sema_impl::register_server()
{
	Environment e;
	naming::naming_context_var lctxp =
	    local_nameserver_funcp((nodeid_t)NULL);
	naming::naming_context_ptr ctxp;
	mc_sema::server_var server_ref;
	CORBA::Object_var obj_ref;

	ASSERT(!CORBA::is_nil(lctxp));

	obj_ref = lctxp->resolve("mc_sema_server", e);
	if (!e.exception() && !CORBA::is_nil(obj_ref)) {
		return (0);
	}

	// Clear any exceptions in previous invocations
	e.clear();

	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		if (node == orb_conf::local_nodeid()) {
			continue;
		}

		ctxp = local_nameserver_funcp(node);
		if (CORBA::is_nil(ctxp)) {
			continue;
		}
		obj_ref = ctxp->resolve("mc_sema_server", e);

		CORBA::release(ctxp);
		if (e.exception() || CORBA::is_nil(obj_ref)) {
			e.clear();
			continue;
		}

		// we found the server_ref
		// Make this a ptr to the mc_sema_server
		server_ref = mc_sema::server::_narrow(obj_ref);
		ASSERT(!CORBA::is_nil(server_ref));
		lctxp->rebind("mc_sema_server", server_ref, e);
		if (e.exception()) {
			e.exception()->print_exception("mc_sema_server rebind");
			return (1);
		}
		return (0);
	}
	os::panic("clutil: Could not find mc_sema_server on any node");
	return (1);
}

int
mc_sema_impl::get_root_to_local()
{
	Environment e;
	naming::naming_context_ptr gctxp = root_nameserver_funcp();
	mc_sema::server_var server_ref;
	CORBA::Object_var obj_ref;
	int retry_count = 1;

	//
	// We will try to get the reference to the global
	// nameserver 5 times. We need to retry as the
	// global nameserver initialization and this
	// happens in parallel.
	//
	while (CORBA::is_nil(gctxp)) {
		if (retry_count == 5) {
			break;
		}
		retry_count++;
		gctxp = root_nameserver_funcp();
	}

	//
	// After retries also if the reference is NIL,
	// panic the node for debug code. For non debug
	// code, return 1.
	// TODO:
	// 1. Verify that the return value is proper
	// 2. If we are hitting the panic often, then
	// either increase the retry count, else add
	// a sleep.
	//
	ASSERT(!CORBA::is_nil(gctxp));
	if (CORBA::is_nil(gctxp)) {
		return (1);
	}

	obj_ref = gctxp->resolve("mc_sema_server", e);
	CORBA::release(gctxp);
	if (!e.exception() && !CORBA::is_nil(obj_ref)) {
		naming::naming_context_ptr lctxp =
		    local_nameserver_funcp((nodeid_t)NULL);

		server_ref = mc_sema::server::_narrow(obj_ref);
		lctxp->rebind("mc_sema_server", server_ref, e);
		CORBA::release(lctxp);
		if (e.exception()) {
			e.clear();
		}
	} else {
		e.clear();
	}

	return (0);
}
#endif


// Constructor
//
// If we have a cookie to a mc_sema object pass it to the constructor
// it will then use this cookie when communicating with server
// If no cookie is set, then the object will do a create_sema call on
// the server when init() is invoked.
//
mc_sema_impl::mc_sema_impl(mc_sema::cookie id) :
	McServerof<mc_sema::sema>(),
	_state(NO_WAITER),
	_id(id)
{
	// Get a reference to the server object
	Environment	e;

	naming::naming_context_ptr ctxp =
	    local_nameserver_funcp((nodeid_t)NULL);
	ASSERT(!CORBA::is_nil(ctxp));
	CORBA::Object_var obj_ref = ctxp->resolve("mc_sema_server", e);
	CORBA::release(ctxp);

	if (e.exception()) {
		e.exception()->print_exception("Getting mc_sema_server");
		_server_ref = mc_sema::server::_nil();
		return;
	}

	// Make this a ptr to the mc_sema_server
	_server_ref = mc_sema::server::_narrow(obj_ref);
	ASSERT(!CORBA::is_nil(_server_ref));
}


//
// init
// Initialize the value of the mc_sema to a value
//
// The client will communicate with the server to set the value
// if no cookie was set on the constructor, init will intially call create_sema
//
void
mc_sema_impl::init(uint32_t value, Environment &e)
{
	_lck.lock();

	if (_id == 0) {
		if (create() != 0) {
#ifdef DEBUG
			os::printf("Error creating mc sema\n");
#endif
			_lck.unlock();
			return;
		}
	}

	ASSERT(_id != 0);
	ASSERT(!CORBA::is_nil(_server_ref));

	// Request to the server to intitialize this mc sema object
	_server_ref->init(value, _id, e);
	if (e.exception()) {
		e.exception()->print_exception("Getting mc_sema_server");
	}

	_lck.unlock();
}


//
// destroy
// must be called to remove the mc_sema object from the server
//
void
mc_sema_impl::destroy(Environment &e)
{
	_lck.lock();

	ASSERT(_id != 0);
	ASSERT(!CORBA::is_nil(_server_ref));

	// Request to the server to destroy this mc sema object
	_server_ref->destroy(_id, e);
	if (e.exception()) {
		e.exception()->print_exception("Getting mc_sema_server");
	}

	_lck.unlock();
}

// Specify the maximum time the client will sleep
// without checking for server existence
const os::usec_t	mc_sema_max_sleep = 30000000;

//
// sema_p operation on a mc_sema object
//
// The server will tell the client to sleep if the server cannot
// immediately grant the semaphore to the client. The client checks the
// existence of the server if not woken up within a reasonable time frame.
//
// XXX - The semaphore must become HA in order to recover from server failures
//
void
mc_sema_impl::sema_p(Environment &e)
{
	_lck.lock();

	ASSERT(_id != 0);
	ASSERT(!CORBA::is_nil(_server_ref));

	bool			rslt;
	mc_sema::sema_var	client_ref;

	// Get a reference to us
	client_ref = get_objref();
	ASSERT(!CORBA::is_nil(client_ref));

	// Only one requestor is allowed at one time
	while (_state != NO_WAITER) {
		_cv.wait(&_lck);
	}
	// Show that this thread has requested the semaphore
	_state = ONE_REQUESTER;

	// Request server to sema_p on this mc sema object
	rslt = _server_ref->sema_p(client_ref, _id, e);
	if (e.exception()) {
		e.exception()->print_exception("Getting mc_sema_server");

		// Cannot recover from semaphore failure
		os::panic("clutil: mc_sema failure");
	}

	// Examine results if they were true, then we need to go to sleep
	// waiting for the server to wake us up
	if (rslt) {
		// Show that this thread is waiting for the semaphore
		_state = ONE_WAITER;

		//
		// Block until the state shows that the client node
		// has heard from the server, and a grant of the semaphore is
		// pending the unblocking of the client.
		//
		while (_state != ONE_PENDING) {
			os::systime	time_out;
			time_out.setreltime(mc_sema_max_sleep);
			(void) _cv.timedwait(&_lck, &time_out);

			if (_state == ONE_PENDING) {
				break;
			}

			// Test to see if the server still exists
			_server_ref->no_op(_id, e);
			if (e.exception()) {
				//
				// The only possible exceptions are
				// COMM_FAILURE or INV_OBJREF.
				//
				e.exception()->
				    print_exception("Test mc_sema_server");

				// Cannot recover from semaphore failure
				os::panic("clutil: mc_sema failure");
			}

		}
	}
	// Show that no thread is actively trying to request the semaphore
	_state = NO_WAITER;

	//
	// Signal one thread that wants to attempt a sema_p operation.
	// There is no defined order about which thread will be woken up.
	//
	_cv.signal();

	_lck.unlock();
}

//
// sema_v operation on mc_sema
//
// communicates with server to do a sema_v
//
// This method cannot hold the lock while performing the V operation,
// because the server could perform a wakeup on a waiting thread. The
// wakeup would need to acquire the lock on this object.
//
void
mc_sema_impl::sema_v(Environment &e)
{
	ASSERT(_id != 0);
	ASSERT(!CORBA::is_nil(_server_ref));

	// Request to the server to sema_v on this mc sema object
	_server_ref->sema_v(_id, e);
	if (e.exception()) {
		e.exception()->print_exception("Getting mc_sema_server");
	}
}

//
// wakeup
//
// invoked by the mc_sema server to wake up the client
//
void
mc_sema_impl::wakeup(Environment &)
{
	_lck.lock();

	// Let the waiting thread know that it can proceed
	ASSERT(_state == ONE_WAITER);
	_state = ONE_PENDING;

	//
	// Signal all waiting threads.
	// We must wake up the thread that sent the sema_p to the server.
	// By waking up all threads we guarantee that we signal the
	// thread that must be woken up. Multiple threads can be waiting.
	//
	_cv.broadcast();

	_lck.unlock();
}


//
// get cookie set by the mc_sema server
//
mc_sema::cookie
mc_sema_impl::get_cookie(Environment &)
{
	mc_sema::cookie	rslt;

	_lck.lock();

	ASSERT(_id != 0);
	rslt = _id;

	_lck.unlock();

	return (rslt);
}


// create
//
// implementation specific function to request a new mc_sema object
// be created on the server
//
int
mc_sema_impl::create()
{
	ASSERT(_id == 0);
	ASSERT(!CORBA::is_nil(_server_ref));

	Environment	e;
	mc_sema::cookie	rslt;

	// Request from the server to create a new mc_sema object
	rslt = _server_ref->create_sema(e);
	if (e.exception()) {
		e.exception()->print_exception("Getting mc_sema_server");
		rslt = 0;
	}

	// Save the result of the create
	_id = rslt;

	if (rslt != 0) {
		return (0);
	} else {
		return (1);
	}
}

void
#ifdef DEBUG
mc_sema_impl::_unreferenced(unref_t c)
#else
mc_sema_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(c));
	delete this;
}
