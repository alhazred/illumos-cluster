//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ns_tmpl_insts.cc	1.2	08/05/20 SMI"


#include <h/naming.h>
template class _NormalSeq_<naming::binding>;

template class proxy<naming::binding_iterator_stub>;
template class proxy<naming::naming_context_stub>;

#include <orb/object/adapter.h>

template class Anchor<naming::naming_context_stub>;
template class McServerof<naming::naming_context>;
template class McNoref<naming::naming_context>;

#include <repl/service/replica_tmpl.h>

template class mc_replica_of<naming::naming_context>;

#include <nslib/binding_iter_handler.h>
template class impl_spec<naming::binding_iterator, binding_iter_handler>;

template class _ManagedSeq_<_NormalSeq_<naming::binding>, naming::binding>;
template class _T_var_<_NormalSeq_<naming::binding>,
    _NormalSeq_<naming::binding>*>;

template class impl_spec<naming::binding_iterator, binding_iter_handler>;

#include <h/repl_ns.h>

template class proxy<repl_ns::ns_replica_stub>;
