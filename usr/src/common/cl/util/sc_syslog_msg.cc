//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sc_syslog_msg.cc	1.4	08/05/20 SMI"

//
// This file is part of a work-around for
// 4517304 sc_syslog_msg_log can unexpectedly fork if syslogd dies
//
// If the ORB is ever enhanced such that it can permit clients to fork,
// then this file (along with the source code for
// sc_syslog_msg_set_syslog_tag_no_fork and sc_syslog_msg_initialize_no_fork)
// can be removed.
//

#include <sys/sc_syslog_msg.h>

// We are introducing new versions of sc_syslog_msg_set_syslog_tag and
// sc_syslog_msg_initialize as a workaround for 4517304. These new
// versions call openlog() with the LOG_CONS flag cleared. This has the
// effect of preventing vsyslog from forking in the event of problems
// with syslogd.
//
// The intent is that programs which link against libclcomm will use
// the new versions of these functions, while programs which do not
// will use the original versions in libclos. This is done by using
// a "pragma weak" alias in libclos (uos_misc.cc) for these functions.
//
// The purpose of the LOG_CONS flag is to request that syslog messages be
// printed to the console in case syslogd dies, isn't started, or has other
// problems. A side effect of this workaround is that no messages will be
// printed to the console in the event of an unhealthy syslogd.
//
extern "C" sc_syslog_msg_status_t
sc_syslog_msg_set_syslog_tag(const char *tag)
{
	return (sc_syslog_msg_set_syslog_tag_no_fork(tag));
}

extern "C" sc_syslog_msg_status_t
sc_syslog_msg_initialize(
			sc_syslog_msg_handle_t *handle,
			const char *resource_type_specific_tag,
			const char *name)
{
	return (sc_syslog_msg_initialize_no_fork(handle,
	    resource_type_specific_tag, name));
}
