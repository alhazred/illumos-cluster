//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)quorum_tmpl_insts.cc	1.2	08/05/20 SMI"

#include <h/quorum.h>
template class _NormalSeq_<quorum::quorum_device_config_t>;
template class _NormalSeq_<quorum::quorum_device_status_t>;
template class _FixedSeq_<quorum::node_status_t>;
template class _FixedSeq_<quorum::node_config_t>;
template class _FixedSeq_<quorum::quorum_result_t>;
template class _FixedSeq_<quorum::registration_key_t>;
template class _NormalSeq_<quorum::qd_property>;

template class proxy<quorum::quorum_algorithm_stub>;
template class proxy<quorum::device_type_registry_stub>;
template class proxy<quorum::quorum_device_type_stub>;

#include <orb/object/adapter.h>

template class McServerof<quorum::quorum_algorithm>;
template class McServerof<quorum::device_type_registry>;
template class McServerof<quorum::quorum_device_type>;

#include <h/quorum.h>

#include <h/dpm.h>
template class _NormalSeq_<disk_path_monitor::dpm_disk_status_t>;
template class _NormalSeq_<disk_path_monitor::dpm_disk_t>;
template class proxy<disk_path_monitor::dpm_stub>;
template class proxy<disk_path_monitor::dpm_policy_stub>;
