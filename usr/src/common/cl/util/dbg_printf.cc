//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dbg_printf.cc	1.56	08/08/19 SMI"

#include <sys/dbg_printf.h>
#include <sys/time.h>
#ifdef _KERNEL
#include <sys/cmn_err.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <dlfcn.h>
#include <unode/unode_config.h>
#include <orb/infrastructure/orb_conf.h>
#endif

#ifdef _KERNEL

#ifdef _LP64
#define	HEADER_LEN	34UL
#else
#define	HEADER_LEN	26UL
#endif

#else  // Unodes and User

#define	HEADER_LEN	26UL

#endif

const uint_t dbg_print_buf::dbgpadlen = (uint_t)(256UL + HEADER_LEN);

SList<dbg_print_buf> *dbg_print_buf::dbg_print_buf_listp = NULL;

os::mutex_t dbg_print_buf::dbgbuf_list_lock;

// Constructor
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
dbg_print_buf::dbg_print_buf(uint_t size, un_dbgout_t output, bool alloc_mem) :
	buf(NULL),
	buf_end(NULL),
	c_ptr(NULL),
	buf_size(size),
	name_checked(false),
	print_mode(output),
	filep(-1),
	sfilep(NULL),
	mmap_buf(NULL),
	mmap_buf_end(NULL)
#else
dbg_print_buf::dbg_print_buf(uint_t size, un_dbgout_t, bool alloc_mem) :
	buf(NULL),
	buf_end(NULL),
	c_ptr(NULL),
	buf_size(size)
#endif
{
	if (alloc_mem && size) {
		buf_end = buf = new char[size];
		bzero(buf, (size_t)size);
	}

#ifndef _KERNEL
	// Only first instance of dbg_print_buf would instantiate the
	// dbg_print_buf_listp.
	//
	dbg_print_buf::dbgbuf_list_lock.lock();
	if (!dbg_print_buf_listp) {
		dbg_buf_list_init();
	}

	// Add this instance to dbg_print_buf_listp.
	dbg_print_buf::dbg_print_buf_listp->append(this);

	dbg_print_buf::dbgbuf_list_lock.unlock();
#endif // _KERNEL
}

dbg_print_buf::dbg_print_buf(dbgbuf_info_t *binfo) :
	c_ptr(binfo),
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	buf_size(binfo->bsize),
	name_checked(false),
	print_mode(binfo->uprint),
	filep(-1),
	sfilep(NULL),
	mmap_buf(NULL),
	mmap_buf_end(NULL)
#else
	buf_size(binfo->bsize)
#endif
{
	if (buf_size) {
		buf_end = buf = new char[buf_size];
		bzero(buf, (size_t)buf_size);
	} else {
		buf_end = buf = NULL;
	}

	c_ptr->bptr = buf;

#ifndef _KERNEL
	// Only first instance of dbg_print_buf would instantiate the
	// dbg_print_buf_listp.
	//
	dbg_print_buf::dbgbuf_list_lock.lock();
	if (!dbg_print_buf_listp) {
		dbg_buf_list_init();
	}

	// Add this instance to dbg_print_buf_listp.
	dbg_print_buf::dbg_print_buf_listp->append(this);

	dbg_print_buf::dbgbuf_list_lock.unlock();
#endif // _KERNEL
}

// Copy constructor.
dbg_print_buf::dbg_print_buf(const dbg_print_buf &dbuf) :
	c_ptr(NULL), // Buffers copied from C-buffers are always C++ buffers.
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	buf_size(dbuf.buf_size),
	name_checked(false),
	print_mode(dbuf.print_mode),
	filep(-1),
	sfilep(NULL),
	mmap_buf(NULL),
	mmap_buf_end(NULL)
#else
	buf_size(dbuf.buf_size)
#endif
{
	if (buf_size) {
		buf = new char[buf_size];
		// copy from dbuf to buf
		(void) os::strncpy(buf, dbuf.buf, (size_t)buf_size - 1);
		buf[buf_size - 1] = '\0';
		buf_end = buf + (dbuf.buf_end - dbuf.buf);
	} else {
		buf_end = buf = NULL;
	}

#ifndef _KERNEL
	// Only first instance of dbg_print_buf would instantiate the
	// dbg_print_buf_listp.
	//
	dbg_print_buf::dbgbuf_list_lock.lock();
	if (!dbg_print_buf_listp) {
		dbg_buf_list_init();
	}

	// Add this instance to dbg_print_buf_listp.
	dbg_print_buf::dbg_print_buf_listp->append(this);

	dbg_print_buf::dbgbuf_list_lock.unlock();
#endif // _KERNEL
}

//
// Instantiate single instance of dbg_print_buf_listp.
//
void
dbg_print_buf::dbg_buf_list_init()
{
	ASSERT(dbg_print_buf_listp == NULL);
	dbg_print_buf_listp = new SList<dbg_print_buf>();
}

dbg_print_buf::~dbg_print_buf()
{
	lock();
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	if (sfilep != NULL) {
		ASSERT(filep == -1);
		(void) fclose(sfilep);
	} else if (filep != -1) {
		ASSERT(mmap_buf != NULL);
		(void) munmap(mmap_buf, (size_t)buf_size);
		(void) close(filep);
	}
	mmap_buf = mmap_buf_end = NULL;	// for lint
#endif

#ifndef _KERNEL
	dbg_print_buf::dbgbuf_list_lock.lock();

	// Erase this instance from dbg_print_buf_listp.
	(void) dbg_print_buf_listp->erase(this);

	dbg_print_buf::dbgbuf_list_lock.unlock();
#endif // _KERNEL

	if (c_ptr != NULL) {
		c_ptr->bptr = NULL;
		c_ptr->optr = NULL;
	}
	c_ptr = NULL;
	delete [] buf;
	buf_end = NULL;		// for lint
}

void
dbg_print_buf::dump_str() {
	if (buf != NULL) {
		os::printf("%s\n%s\n", buf, buf_end);
	} else {
		os::printf("No debug buffer was allocated\n");
	}
}

inline void
dbg_print_buf::update_tail(uint_t pad)
{
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	mmap_buf_end += os::strlen(buf_end);
	buf_end += os::strlen(buf_end);
	if (buf_end > (buf + buf_size - pad)) {
		mmap_buf_end = mmap_buf;
#else
	buf_end += pad;
	//
	// NULL terminate the string.  This cannot be done in
	// dbprintf_va because it has to be done while holding the lock
	// and we don't want to put bcopy inside lock() - unlock().
	//
	*buf_end = '\0';
	if (buf_end >= (buf + buf_size - dbgpadlen)) {
#endif
		buf_end = buf;
	}
}

void
dbg_print_buf::dbprintf(char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);		//lint !e40
	dbprintf_va(true, fmt, ap);
	va_end(ap);
}

void
dbg_print_buf::dbprintf_cont(char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);		//lint !e40
	dbprintf_va(false, fmt, ap);
	va_end(ap);
}

void
dbg_print_buf::dbprintf_va(bool print_header, char *fmt, va_list &ap)
{
	// update_tail is done in the last so that we know the next time we
	// here we have enough space to print 256 bytes
	//
	// The system prepends the thread id and high resolution time
	// (nanoseconds are dropped and so are the upper digits)
	// to the specified string.
	// The unit for timestamp is 10 microseconds.
	// It wraps around every 10000 seconds.
	// Ex: gethrtime() = X ns = X/1000 us = X/10000 10 micro sec.
	//
	int	micro_time = (int)((os::gethrtime() / 10000) % 1000000000);

	size_t header_len = HEADER_LEN;

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	lock();
#else
	char	*prev_buf_end = NULL;
	//
	// Currently we log at the most approx. dbgpadlen bytes each time
	//
	char	tmp_buf[dbg_print_buf::dbgpadlen];
	size_t	tmp_buf_len = 0;
#endif

	if (buf_size == 0) {
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		unlock();
#endif
		return;
	}

#if defined(_KERNEL)
	lock();
#endif
	if (buf == NULL) {
		buf_end = buf = new char[buf_size];
		bzero(buf, (size_t)buf_size);
	}
#if defined(_KERNEL)
	unlock();
#endif

	ASSERT(buf_end != NULL);

	if (print_header) {
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		// unode
		os::sprintf(buf_end, "th %8x tm %9d: ", os::threadid(),
		    micro_time);
#elif defined(_KERNEL)
#ifdef	_LP64
		os::sprintf(tmp_buf, "th %16llx tm %9d: ", os::threadid(),
		    micro_time);
#else
		// kernel
		os::sprintf(tmp_buf, "th %8x tm %9d: ", os::threadid(),
		    micro_time);
#endif
#else
		// user
		os::sprintf(tmp_buf, "th %8x tm %9d: ", os::threadid(),
		    micro_time);
#endif
	} else {
		header_len = 0;
	}

	// NULL terminate on overflow
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	// unode
	if (::vsnprintf(buf_end + header_len, 256UL, fmt, ap) >= 255) {
		buf_end[255 + header_len] = '\0';
	}
#else
	// kernel and user
	if (::vsnprintf(tmp_buf + header_len, 256UL, fmt, ap) >= 255) {
		tmp_buf[255 + header_len] = '\0';
	}
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	//
	// Save buffer content to file (if user requested it).
	//
	if (!name_checked) {
		Dl_info	info;

		if (dladdr(this, &info) == 0) {
			//
			// dladdr() shouldn't fail since if we're here
			// that means this object exists, unless we were
			// dynamically allocated from C code, in which case
			// we can retrieve the name of the encoding struct.
			//
			ASSERT(c_ptr != NULL);
			if (dladdr(c_ptr, &info) == 0) {
				//
				// Do not need syslog because this is
				// a unode only message.
				//
				os::panic("dbg_print_buf: dladdr failed: %s\n",
				    strerror(errno));
			}
		}

		switch (unode_config::the().lookup_dbg_buf(info.dli_sname,
		    print_mode != UNODE_DBGOUT_NONE)) {
		case unode_config::UNINITIALIZED:
			break;			// we'll check back later
		case unode_config::NOT_FOUND:
			name_checked = true;
			break;
		case unode_config::FOUND: {
			// User wants this buffer to be saved to file.
			char	filename[PATH_MAX + 1];

			if (os::snprintf(filename, (size_t)PATH_MAX + 1,
			    "%s/%s.dbg", unode_config::the().node_dir(),
			    info.dli_sname) >= PATH_MAX) {
				// NULL terminate on overflow
				filename[PATH_MAX] = '\0';
			}

			switch (print_mode) {
			case (UNODE_DBGOUT_ALL):
				sfilep = fopen(filename, "a");
				if (sfilep == NULL) {
					perror("dbg_print_buf: fopen");
					break;
				}
				if (unode_config::the().is_verbose()) {
					os::printf("Debug buffer '%s' "
					    "saved to %s\n", info.dli_sname,
					    filename);
				}

				setbuf(sfilep, NULL);	// don't buffer

				// Ensure fd gets closed on "reboots".
				(void) fcntl(fileno(sfilep), F_SETFD,
				    FD_CLOEXEC);

				(void) fprintf(sfilep, "\nNew Incarnation %u\n",
				    orb_conf::local_incarnation());

				name_checked = true;

				break;
			case (UNODE_DBGOUT_MMAP):
				filep = open(filename, O_RDWR|O_CREAT, 00644);
				if (filep == -1) {
					perror("dbg_print_buf: open");
					break;
				}
				(void) fcntl(filep, F_SETFD, FD_CLOEXEC);
				(void) write(filep, buf, (size_t)buf_size);
				mmap_buf = (char *)mmap(0, (size_t)buf_size,
				    PROT_READ|PROT_WRITE, MAP_SHARED, filep,
				    0L);
				ASSERT(mmap_buf != MAP_FAILED);
				mmap_buf_end = mmap_buf + (buf_end - buf);
				name_checked = true;
				break;
			case (UNODE_DBGOUT_NONE):
				break;
			default:
				//
				// Do not need syslog because this is
				// a unode only message.
				//
				os::panic("Invalid print_mode value %d"
				    "in dbprintf for unode\n", print_mode);
				break;
			}
			break;
		    }
		default:
			ASSERT(0);
			break;
		}
	}

	if (filep != -1) {
		(void) os::strcpy(mmap_buf_end, buf_end);
	}

	if (sfilep != NULL) {
		if (fputs(buf_end, sfilep) == EOF) {
			perror("dbg_print_buf: fputs");
		}
	}
#endif // _UNODE

	//
	// Lock the buffer and update the tail
	//
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	update_tail(dbgpadlen);
	unlock();
#else
	tmp_buf_len = strlen(tmp_buf);
	lock();
	prev_buf_end = buf_end;
	update_tail((uint_t)tmp_buf_len);
	unlock();
	bcopy(tmp_buf, prev_buf_end, tmp_buf_len);
#endif
}

//
// This method  erases all the debug buffer instances from the
// dbg_print_buf_list. This is called from ORB::shutdown.
//
// static
void
dbg_print_buf::dbg_buf_erase_list()
{
	dbgbuf_list_lock.lock();
	if (dbg_print_buf_listp == NULL) {
		dbgbuf_list_lock.unlock();
		return;
	}
	dbg_print_buf_listp->erase_list();
	dbgbuf_list_lock.unlock();
}

os::mutex_t c_dbgbuf_lock;

/*
 * print_to_dbgbuf prints the supplied "printf-type-arguments" to the
 * dbg_print_buf pointed to by binfo.  If the buffer has not yet been
 * created, it creates it.  The fields optr and bptr in binfo will be
 * filled in in the constructor for the dbgbuf that takes a dbg_info_t
 * as it's argument.
 */
extern "C" void
print_to_dbgbuf(dbgbuf_info_t *binfo, char *fmt, ...)
{
	if (binfo->bsize == 0)
		return;

	ASSERT((binfo->uprint == UNODE_DBGOUT_ALL) ||
	    (binfo->uprint == UNODE_DBGOUT_MMAP) ||
	    (binfo->uprint == UNODE_DBGOUT_NONE));

	c_dbgbuf_lock.lock();
	if (binfo->optr == NULL) {
		// we have our own new operator, so suppress lint complaint.
		binfo->optr = (void *)new dbg_print_buf(binfo); //lint !e15
	}

	ASSERT(binfo->optr != NULL);
	ASSERT(binfo->bptr != NULL);

	c_dbgbuf_lock.unlock();

	va_list ap;
	va_start(ap, fmt);		//lint !e40
	((dbg_print_buf *)(binfo->optr))->dbprintf_va(true, fmt, ap);
	va_end(ap);
}

/*
 * destroy_dbgbuf deletes a dbgbuf associated with binfo, if there is one.
 */
extern "C" void
destroy_dbgbuf(dbgbuf_info_t *binfo)
{
	c_dbgbuf_lock.lock();

	delete (dbg_print_buf *)(binfo->optr);

	ASSERT(binfo->optr == NULL);
	ASSERT(binfo->bptr == NULL);

	c_dbgbuf_lock.unlock();
}
