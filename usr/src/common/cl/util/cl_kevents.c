/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_kevents.c	1.22	09/03/12 SMI"

#include <unistd.h>
#include <sys/varargs.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/kmem.h>
#include <sys/cmn_err.h>
#include <sys/clconf_int.h>
#include <sys/sysevent.h>
#include <sys/cl_eventdefs.h>
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <sys/zone.h>
#include <sys/vc_int.h>
#include <sys/os.h>
#include <sys/kmem.h>
#endif /* SOL_VERSION >= __s10 */

#define	EVENTS_PER_PERIOD 50	/* event throttling threshold */
#define	PERIOD_UNIT 20		/* unit is second */
#define	MAX_EVENTS_PER_PUBLISHER 10  /* in one unit period */

/*
 * Cluster event publication wrapper for use within the kernel.
 */

#ifdef _KERNEL

cl_event_error_t
sc_publish_event_common(char *subclassp, char *publisherp,
    cl_event_severity_t severity, cl_event_initiator_t initiator,
    char *clidp, char *clnamep, char *nodenamep, va_list ap)
{
	char *ev_subclassp;
	char ev_publisher[MAX_PUB_LEN];
	uint32_t ev_severity;
	uint32_t ev_initiator;

	char *value_namep;
	data_type_t value_type;

	sysevent_attr_list_t *attr_listp = NULL;
	sysevent_t *evp;
	sysevent_id_t eid;
	sysevent_value_t ev_value;

	int err;
	static time_t time_queue[EVENTS_PER_PERIOD];
	static char publisher_queue[EVENTS_PER_PERIOD][MAX_PUB_LEN];
	static int front = 0;
	static int size = 0;
	static int event_throttle_flag = 0;
	int time_diff = 0;

	timespec_t now;
	struct timeval tod;

	ev_subclassp = subclassp;
	(void) strcpy(ev_publisher, SUNW_KERN_PUB);
	(void) strcat(ev_publisher, publisherp);
	ev_severity = (uint32_t)severity;
	ev_initiator = (uint32_t)initiator;

	/* Process the argument list */


	/*
	 * Skip over reserved fields that are ignored for now...
	 */

	/*lint -e40 -e718 -e746 */
	(void) va_arg(ap, int);
	(void) va_arg(ap, int);
	(void) va_arg(ap, int);
	/*lint +e40 +e718 +e746 */

	/*
	 *  Build the attribute list...
	 */

	/*
	 *  First the reserved attributes...
	 *
	 *  - Cluster ID
	 *  - Cluster Name
	 *  - Node Name
	 *  - Time Stamp
	 *  - Event Version
	 *  - Severity
	 *  - Initiator
	 */

	ev_value.value_type = SE_DATA_TYPE_STRING;
	ev_value.value.sv_string = clidp;
	err = sysevent_add_attr(&attr_listp, CL_CLUSTER_ID,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add cluster id");
		return (CL_EVENT_EATTR);
	}

	ASSERT(attr_listp != NULL);

	ev_value.value_type = SE_DATA_TYPE_STRING;
	ev_value.value.sv_string = clnamep;
	err = sysevent_add_attr(&attr_listp, CL_CLUSTER_NAME,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add cluster name");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	ev_value.value_type = SE_DATA_TYPE_STRING;
	ev_value.value.sv_string = nodenamep;
	err = sysevent_add_attr(&attr_listp, CL_EVENT_NODE,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add node name");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	ev_value.value_type = SE_DATA_TYPE_UINT32;
	ev_value.value.sv_uint32 = CL_EVENT_VERSION_NUMBER;
	err = sysevent_add_attr(&attr_listp, CL_EVENT_VERSION,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add event version");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	/* Set timestamp */

	gethrestime(&now);
	tod.tv_sec = now.tv_sec;
	tod.tv_usec = now.tv_nsec / (NANOSEC / MICROSEC);

	ev_value.value_type = SE_DATA_TYPE_UINT32;
	ev_value.value.sv_uint32 = (uint32_t)tod.tv_sec;
	err = sysevent_add_attr(&attr_listp, CL_EVENT_TS_SEC,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add timestamp");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	ev_value.value_type = SE_DATA_TYPE_UINT32;
	ev_value.value.sv_uint32 = (uint32_t)tod.tv_usec;
	err = sysevent_add_attr(&attr_listp, CL_EVENT_TS_USEC,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add timestamp");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	/* Add initiator id and severity values */

	ev_value.value_type = SE_DATA_TYPE_UINT32;
	ev_value.value.sv_uint32 = ev_severity;
	err = sysevent_add_attr(&attr_listp, CL_EVENT_SEVERITY,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add event severity");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	ev_value.value_type = SE_DATA_TYPE_UINT32;
	ev_value.value.sv_uint32 = ev_initiator;
	err = sysevent_add_attr(&attr_listp, CL_EVENT_INITIATOR,
	    &ev_value, SE_NOSLEEP);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: failed to add event initiator id");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_EATTR);
	}

	time_diff = tod.tv_sec - time_queue[front];
	if (size == EVENTS_PER_PERIOD && time_diff < PERIOD_UNIT) {
		/* event storm is detected, turn on event throttling */
		if (event_throttle_flag == 0) {
		    ev_value.value_type = SE_DATA_TYPE_STRING;
		    ev_value.value.sv_string =
			"Event storm detected, turn on the event throttling";
		    err = sysevent_add_attr(&attr_listp,
			CL_EVENT_CONTROL_MSG, &ev_value, SE_NOSLEEP);
		    event_throttle_flag = 1;
		} else {
		    int current = front;
		    int count = 0;
		    for (int i = 0; i < size - 1; i++) {
			if (strcmp(publisher_queue[current], publisherp) == 0) {
			    count++;
			    if (count >= MAX_EVENTS_PER_PUBLISHER &&
					severity == CL_EVENT_SEV_INFO) {
				sysevent_free_attr(attr_listp);
				return (CL_EVENT_EATTR);
			    }
			}
			current++;
			if (current == EVENTS_PER_PERIOD - 1) {
			    current = 0;
			}
		    }
		}
	} else if (event_throttle_flag != 0) {
		/* turn off the throttling */
		ev_value.value_type = SE_DATA_TYPE_STRING;
		ev_value.value.sv_string = "Turn off the event throttling";
		err = sysevent_add_attr(&attr_listp, CL_EVENT_CONTROL_MSG,
			&ev_value, SE_NOSLEEP);
		event_throttle_flag = 0;
	}


	/*
	 * Process event attributes. Each attribute specification is
	 * in a form of a triple, consisting of a value name, value
	 * type and the actual value. Processing is terminated when
	 * we encounter a NULL attribute.
	 *
	 */

	while (1) {

		/*lint -e516 */
		value_namep = va_arg(ap, char *);
		if (value_namep == NULL)
			break;

		if (strlen(value_namep) > MAX_ATTR_NAME) {
			sysevent_free_attr(attr_listp);
			return (CL_EVENT_EINVAL);
		}

		value_type = va_arg(ap, data_type_t);
		if (value_type == NULL)
			break;

		ev_value.value_type = value_type;

		switch (value_type) {
		case SE_DATA_TYPE_BYTE : {
			ev_value.value.sv_byte = va_arg(ap, uchar_t);
			break;
		}
		case SE_DATA_TYPE_INT16 : {
			ev_value.value.sv_int16 = va_arg(ap, int16_t);
			break;
		}
		case SE_DATA_TYPE_UINT16 : {
			ev_value.value.sv_uint16 = va_arg(ap, uint16_t);
			break;
		}
		case SE_DATA_TYPE_INT32 : {
			ev_value.value.sv_int32 = va_arg(ap, int32_t);
			break;
		}
		case SE_DATA_TYPE_UINT32 : {
			ev_value.value.sv_uint32 = va_arg(ap, uint32_t);
			break;
		}
		case SE_DATA_TYPE_INT64 : {
			ev_value.value.sv_int64 = va_arg(ap, int64_t);
			break;
		}
		case SE_DATA_TYPE_UINT64 : {
			ev_value.value.sv_uint64 = va_arg(ap, uint64_t);
			break;
		}
		case SE_DATA_TYPE_STRING : {
			ev_value.value.sv_string = va_arg(ap, char *);
			break;
		}
		case SE_DATA_TYPE_BYTES : {
			uchar_t *value;
			value = va_arg(ap, uchar_t *);
			ev_value.value.sv_bytes.data = value;
			ev_value.value.sv_bytes.size = sizeof (value);
			break;
		}
		case SE_DATA_TYPE_TIME : {
			ev_value.value.sv_time = va_arg(ap, hrtime_t);
			break;
		}
		case DATA_TYPE_UNKNOWN:
		case DATA_TYPE_BOOLEAN:
		case DATA_TYPE_INT16_ARRAY:
		case DATA_TYPE_UINT16_ARRAY:
		case DATA_TYPE_INT32_ARRAY:
		case DATA_TYPE_UINT32_ARRAY:
		case DATA_TYPE_INT64_ARRAY:
		case DATA_TYPE_UINT64_ARRAY:
		case DATA_TYPE_STRING_ARRAY:
		default:
			sysevent_free_attr(attr_listp);
			return (CL_EVENT_EINVAL);
		} /* switch */

		err = sysevent_add_attr(&attr_listp,
		    value_namep, &ev_value, SE_NOSLEEP);
		if (err != 0) {
			cmn_err(CE_WARN, "cl_kevent: cannot add attr <%s>",
			    value_namep);
			sysevent_free_attr(attr_listp);
			return (CL_EVENT_EATTR);
		}
		/*lint +e516 */

	} /* while */

	/* Allocate event structure */

	evp = sysevent_alloc(EC_CLUSTER, ev_subclassp, ev_publisher,
	    KM_NOSLEEP);
	if (evp == NULL) {
		cmn_err(CE_WARN, "cl_kevent: cannot allocate event memory");
		sysevent_free_attr(attr_listp);
		return (CL_EVENT_ENOMEM);
	}

	/* Attach attribute list to event structure */

	err = sysevent_attach_attributes(evp, attr_listp);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: cannot attach attributes (rv %d)",
		    err);
		sysevent_free(evp);
		return (CL_EVENT_EPOST);
	}

	if (size < EVENTS_PER_PERIOD) {
		(void) strcpy(publisher_queue[size], publisherp);
		time_queue[size++] = tod.tv_sec;
	} else {
		(void) strcpy(publisher_queue[front], publisherp);
		time_queue[front++] = tod.tv_sec;
		if (front == EVENTS_PER_PERIOD) {
			front = 0;
		}
	}

	/* Publish event... */
	err = log_sysevent(evp, SE_NOSLEEP, &eid);
	if (err != 0) {
		cmn_err(CE_WARN, "cl_kevent: publication error (rv %d)", err);
		sysevent_free(evp);
		return (CL_EVENT_EPOST);
	}

	sysevent_free(evp);
	return (0);
}

/*
 * Common code called by sc_publish_event and sc_publish_zc_event interfaces.
 */
cl_event_error_t
sc_publish_event_int(char *subclassp, char *publisherp,
    cl_event_severity_t severity, cl_event_initiator_t initiator, va_list ap)
{
	char *clidp;
	char *clnamep;
	char *nodenamep;

	clconf_cluster_t  *clp;
	clconf_node_t *ndp;
	nodeid_t nodeid;

	int retval = 0;

	ASSERT(subclassp != NULL);

	if (strlen(subclassp) > MAX_SUBCLASS_LEN) {
		return (CL_EVENT_EINVAL);
	}

	ASSERT(publisherp != NULL);

	if ((strlen(SUNW_KERN_PUB) + strlen(publisherp)) > MAX_PUB_LEN) {
		return (CL_EVENT_EINVAL);
	}

	/* Get cluster id, cluster name and node name */

	clp = clconf_cluster_get_current();
	ASSERT(clp != NULL);

	clnamep = (char *)clconf_obj_get_name((clconf_obj_t *)clp);
	ASSERT(clp != NULL);

	nodeid = clconf_get_nodeid();
	ndp = (clconf_node_t *)clconf_obj_get_child((clconf_obj_t *)clp,
			CL_NODE, (int)nodeid);
	ASSERT(ndp != NULL);
	nodenamep = (char *)clconf_obj_get_name((clconf_obj_t *)ndp);
	ASSERT(nodenamep != NULL);

	clidp = (char *)clconf_obj_get_property((clconf_obj_t *)clp,
	    "cluster_id");
	ASSERT(clidp != NULL);

	clconf_obj_release((clconf_obj_t *)clp);

	retval = sc_publish_event_common(subclassp, publisherp, severity,
	    initiator, clidp, clnamep, nodenamep, ap);

	return (retval);
}

/*
 * Interface used by the kernel to publish events for the base cluster
 */
/* ARGSUSED */
/* VARARGS4 */
cl_event_error_t
sc_publish_event(char *subclass, char *publisher,
    cl_event_severity_t severity, cl_event_initiator_t initiator, ...)
{
	va_list ap;
	int retval;

	/*lint -e40 */
	va_start(ap, initiator);
	/*lint +e40 */

	retval = sc_publish_event_int(subclass, publisher, severity,
	    initiator, ap);

	va_end(ap);
	return (retval);
}

#if SOL_VERSION >= __s10

/*
 * Interface used by the kernel to publish events for a zone cluster
 */
/* ARGSUSED */
/* VARARGS4 */
cl_event_error_t
sc_publish_zc_event(char *zc_namep, char *subclassp, char *publisherp,
    cl_event_severity_t severity, cl_event_initiator_t initiator, ...)
{
	va_list ap;


	char *clidp;
	char *clnamep;
	char *nodenamep = "";

	clconf_cluster_t  *clp;
	clconf_node_t *ndp;
	nodeid_t nodeid;
	uint32_t zcid;

	int retval = 0;

	ASSERT(subclassp != NULL);

	if (strcmp(zc_namep, "global") == 0) {
		/* global zone */
		/*lint -e40 */
		va_start(ap, initiator);
		/*lint +e40 */
		retval = sc_publish_event_int(subclassp, publisherp, severity,
		    initiator, ap);
		va_end(ap);
		return (retval);
	}

	if (strlen(subclassp) > MAX_SUBCLASS_LEN) {
		return (CL_EVENT_EINVAL);
	}

	ASSERT(publisherp != NULL);

	if ((strlen(SUNW_KERN_PUB) + strlen(publisherp)) > MAX_PUB_LEN) {
		return (CL_EVENT_EINVAL);
	}

	/* Get zone cluster id, cluster name and node name */

	ASSERT(zc_namep != NULL);
	retval = clconf_get_cluster_id(zc_namep, &zcid);
	if (retval != 0) {
		return (CL_EVENT_EINVAL);
	}
	if (zcid < MIN_CLUSTER_ID) {
		return (CL_EVENT_EINVAL);
	}
	clp = clconf_cluster_get_vc_current(zc_namep);
	ASSERT(clp != NULL);
	clnamep = (char *)clconf_obj_get_name((clconf_obj_t *)clp);
	ASSERT(clnamep != NULL);

	nodeid = clconf_get_nodeid();
	ndp = (clconf_node_t *)clconf_obj_get_child((clconf_obj_t *)clp,
			CL_NODE, (int)nodeid);
	if (ndp != NULL) {
		nodenamep = (char *)clconf_obj_get_name((clconf_obj_t *)ndp);
		ASSERT(nodenamep != NULL);
	}

	clidp = (char *)kmem_alloc((size_t)64, KM_NOSLEEP);
	if (clidp == NULL) {
		cmn_err(CE_WARN, "cl_kevent: cannot allocate memory "
		    "for zone cluster id");
		clconf_obj_release((clconf_obj_t *)clp);
		return (CL_EVENT_ENOMEM);
	}
	(void) sprintf(clidp, "%d", zcid);

	clconf_obj_release((clconf_obj_t *)clp);

	/* Process the argument list */


	/*lint -e40 */
	va_start(ap, initiator);
	/*lint +e40 */

	retval = sc_publish_event_common(subclassp, publisherp, severity,
	    initiator, clidp, clnamep, nodenamep, ap);
	va_end(ap);
	kmem_free(clidp, 64);

	return (retval);
}
#endif /* SOL_VERSION >= __s10 */

#endif /* _KERNEL */
