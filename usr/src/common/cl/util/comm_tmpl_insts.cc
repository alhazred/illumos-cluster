//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)comm_tmpl_insts.cc	1.3	08/05/20 SMI"


//
// This file contains template instance declarations that allow
// us to link template code without using a compiler generated
// template repository.  All code, other than this file, is compiled
// without template generation, this way the only instance of a
// instance class is generated from the declarations in this file.
//
// Some formatting is a bit particular due to cstyle complaints.  Make
// sure you carefully cstyle any modifications.
//

#include <unistd.h>
#include <errno.h>
#include <sys/os.h>

#include <sys/nodeset.h>
#include <orb/infrastructure/orb.h>
#include <orb/idl_datatypes/sequence.h>

#include <orb/object/adapter.h>
#include <h/network.h>
#include <repl/service/replica_tmpl.h>
#include <h/replica_int.h>

template class McServerof<network::cl_net_ns_if>;
template class McServerof<network::mcobj>;
template class mc_checkpoint<network::ckpt>;
template class mc_replica_of<network::PDTServer>;
template class mc_replica_of<network::perf_mon_policy>;
template class mc_replica_of<network::user_policy>;
template class mc_replica_of<network::weighted_lbobj>;
template class proxy<network::weighted_lbobj_stub>;

#include <h/network.h>
template class _NormalSeq_<network::grpinfo_t>;
template class _NormalSeq_<network::macinfo_t>;
template class _NormalSeq_<network::srvinfo_t>;
template class _NormalSeq_<network::fwd_t>;
template class _FixedSeq_<network::cid_t>;
template class _FixedSeq_<network::node_status>;
template class _FixedSeq_<network::inaddr_t>;
template class _FixedSeq_<network::print_state_cmd_t>;
template class _FixedSeq_<network::gns_bind_t>;
template class _FixedSeq_<network::hashinfo_t>;

template class _Ix_InterfaceSeq_ < network::mcobj, _A_field_ < network::mcobj,
    network::mcobj * >, network::mcobj * >;

template class proxy<network::PDTServer_stub>;
template class proxy<network::ckpt_stub>;
template class proxy<network::lbobj_i_stub>;
template class proxy<network::cl_net_ns_if_stub>;
template class proxy<network::mcnet_node_stub>;
template class proxy<network::mcobj_stub>;
template class proxy<network::user_policy_stub>;
template class proxy<network::perf_mon_policy_stub>;

template class _ManagedSeq_<_NormalSeq_<network::macinfo_t>,
    network::macinfo_t>;
template class _ManagedSeq_<_NormalSeq_<network::srvinfo_t>,
    network::srvinfo_t>;
template class _T_var_<_NormalSeq_<network::macinfo_t>,
    _NormalSeq_<network::macinfo_t>*>;
template class _T_var_<_NormalSeq_<network::srvinfo_t>,
    _NormalSeq_<network::srvinfo_t>*>;
template class _T_var_<network::bind_t, network::bind_t*>;
template class _T_var_<network::fwd_t, network::fwd_t*>;

template class mc_replica_of<network::mcnet_node>;
template class repl_server<network::ckpt>;

#include <h/pnm_mod.h>
template class _FixedSeq_<pnm_mod::inaddr_t>;
template class _NormalSeq_<pnm_mod::pnm_adp_status>;
template class _NormalSeq_<pnm_mod::pnm_adapterinfo>;

template class proxy<pnm_mod::pnm_server_stub>;
