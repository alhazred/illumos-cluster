//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)threadpool.cc	1.72	08/10/13 SMI"

#include <sys/os.h>
#include <sys/threadpool.h>
#ifndef CLCOMM_COMPAT
#include <sys/mc_probe.h>
#endif
#include <orb/infrastructure/clusterproc.h>
#ifndef CLCOMM_COMPAT
#include <orb/invo/corba.h>
#include <orb/infrastructure/orb.h>
#endif
#ifdef _KERNEL
#include <sys/disp.h>
#endif

static const int SHUTDOWN_GENERATION = -1;

#ifdef THREADPOOL_DBGBUF
dbg_print_buf	threadpool_dbg(8192);
#endif // THREADPOOL_DBGBUF

#define	DEFAULT_THREADPOOL_PRI	60

// Worker thread specific data will contain a pointer to its threadpool_worker_t
os::tsd		threadpool_worker_t::worker;

//
// threadpool_worker_t methods
//

//
// deferred_task_handler - This is the main dispatch loop for a worker thread.
// This method cannot be a method on the threadpool class because the
// worker thread can be transfered between threadpool's.
//
void
threadpool_worker_t::deferred_task_handler()
{
	defer_task	*taskp;
	bool		need_new;

	ASSERT(my_taskp == NULL);

	os::mutex_t	*lockp = threadpoolp->get_lock();
	lockp->lock();

	//
	// Successfully created worker thread.
	// The thread total (thr_total) was incremented in add_server_thread.
	// The thread in creation count is adjusted here under the lock,
	// because until this point the thread is effectively idle
	// and is counted as such. Now the thread begins work.
	//
	ASSERT(threadpoolp->thr_in_creation > 0);
	threadpoolp->thr_in_creation--;

	//
	// Loop forever as long as the thread generation
	// has not changed.
	//
	while (my_gen == threadpoolp->generation) {
		if ((taskp = threadpoolp->pending.reapfirst()) == NULL) {
			//
			// No pending tasks.
			// Tell threadpool that worker is idle.
			//
			threadpoolp->worker_idle(this);

			// Take appropriate action for worker state
			if (my_state == WORK) {
				//
				// A task has been assigned to this worker.
				// This case is by far the most frequent.
				//
				taskp = my_taskp;
				my_taskp = NULL;

			} else if (my_state == SURPLUS) {
				// Threadpool told worker thread to go away.
				break;

			} else if (my_state == CHANGE) {
				// The thread generation changed
				break;

			} else {
				// Encountered invalid state
				ASSERT(0);
				continue;
			}
		} else {
			//
			// Decrement count of tasks still in queue. Must be
			// equal to pending.count() + blocked_waiting.count()
			//
			threadpoolp->count--;
		}
#ifndef CLCOMM_COMPAT
		MC_PROBE_0(tr_receive_gotMsg, "mc threadpool", "");
#endif

		//
		// This will create a new thread if this is the last one.
		// This check is too weak and the threadpool can
		// deadlock because there are insuffient threads.
		// Flow control implementation will address this issue.
		// If thr_idle goes to zero
		// and if add_server_thread runs out of memory
		// and does not create a thread and all the existing threads
		// are stuck, we could deadlock.
		// We could use a polling thread as a workaround.
		//
		need_new = threadpoolp->dynamic_threadpool &&
		    (threadpoolp->thr_idle == 0) &&
		    (threadpoolp->thr_in_creation == 0) &&
		    (threadpoolp->thr_total < threadpoolp->thr_max);

		threadpoolp->unlock();

		if (need_new) {
			(void) threadpoolp->add_server_thread(1);
		}
#ifndef CLCOMM_COMPAT
		MC_PROBE_0(tr_receive_toHandle, "mc threadpool", "");
#endif

		THR_DBG(("%s Process: %p des %d idle %d avg %d tot %d count "
		    "%d\n", threadpoolp->id, my_taskp,
		    threadpoolp->thr_desired, threadpoolp->thr_idle,
		    threadpoolp->avg_thr_idle, threadpoolp->thr_total,
		    threadpoolp->count));

		taskp->execute();

#ifndef CLCOMM_COMPAT
		MC_PROBE_0(tr_receive_Handled, "mc threadpool", "");
#endif

		//
		// Clear SIGINT sent to this thread. If signal forwarding
		// is enabled, then this thread may have received a SIGINT.
		// The signal is relevant to the task that was being
		// processed by this thread at that time. Now the thread has
		// completed that task and should clear signal posted to it
		// before it starts processing the next task.
		//
		clclearlwpsig();

		threadpoolp->lock();
	}
	//
	// Notify threadpool about thread exit.
	//
	threadpoolp->worker_exit(this);
}

//
// threadpool class methods
//

//
// threadpool constructor
//
// First set all the variables to defaults and use the set/change methods
// to change the defaults. The set/change methods handle some implementation
// details that are not handled by initializating the variables directly
//
threadpool::threadpool(bool dynamic, int cnt, const char *name, int maxcnt,
    size_t stacksize) :
	id(NULL),
	thr_total(0),
	thr_idle(0),
	thr_desired(0),
	thr_max(maxcnt),
	avg_thr_idle(0),
	arr_index(0),
	sum_arr(0),
	thr_in_creation(0),
	generation(0),
	thr_stacksize(stacksize),
	thread_pri(DEFAULT_THREADPOOL_PRI),
	thread_clname(NULL),
	dynamic_threadpool(false),
	blocked_processing(0),
	count(0)
{
	int loop_index;

	(void) modify(dynamic, cnt, name);
	ASSERT(thr_max >= thr_desired);

	for (loop_index = 0; loop_index < MAX_THREADS_AVG; loop_index++) {
		arr_thr_idle[loop_index] = 0;
	}
}

//
// modify - used to set/change threadpool properties.
//	dynamic	- true means number of threads changes automatically
//	cnt	- specifies the number of threads to change
//	name	- name of threadpool
int
threadpool::modify(bool dynamic, int cnt, const char *name)
{
	set_name(name);
	set_dynamic_threads(dynamic);
	ASSERT(cnt >= 0);
	int tcnt = change_num_threads(cnt);
	if (tcnt < cnt) {
		THR_DBG(("%s Created fewer threads than requested "
		    "%d created instead of %d\n",
		    id, tcnt, cnt));
		//
		// When the number of threads created is less than expected,
		// dynamic thread pools will self correct if at least one
		// thread was created.
		//
		if (!dynamic_threadpool || (tcnt == 0)) {
			//
			// Need to fix this by either retrying or posting as
			// task. For now we panic as most threads are
			// created during startup and if we cannot get
			// memory it usually points to worse memory problems
			//
#ifndef CLCOMM_COMPAT
			//
			// SCMSGS
			// @explanation
			// There was insufficient memory to create the desired
			// number of threads.
			// @user_action
			// Install more memory, increase swap space, or reduce
			// peak memory consumption.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clutil: Created insufficient threads in "
			    "threadpool");
#endif
		}
		return (ENOMEM);
	}
	return (0);
}

//
// set_sched_props - change the priority and scheduling
// class for the threads in this threadpool.
// When the scheduling class is not specified, use the default.
// This only affects newly created threads. Properies of an existing
// thread changes only when that thread returns from the current task.
//
// Returns 0 on success and the error number on failure (invalid class name).
//
int
threadpool::set_sched_props(int pri, const char *cname)
{
	if (cname != NULL) {
		//
		// Only attempt to get the scheduling class
		// when the scheduling class is specified.
		//
		id_t	cid;
		int	error = getcid((char *)cname, &cid);
		if (error != 0) {
#ifndef CLCOMM_COMPAT
			//
			// SCMSGS
			// @explanation
			// An attempt to change the thread scheduling class
			// failed, because the scheduling class was not
			// configured.
			// @user_action
			// Configure the system to support the desired thread
			// scheduling class.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "clutil: Scheduling class %s not configured",
			    cname);
#endif
			return (error);
		}
	}
	lock();
	thread_pri = pri;
	delete [] thread_clname;
	thread_clname = os::strdup(cname);
	if (thr_total != 0) {
		//
		// Threads exist with old scheduling information.
		// Cause current lwp's to respawn with new scheduling
		// priority and class.
		//
		respawn();
	}
	unlock();
	return (0);
}

threadpool::~threadpool()
{
	//
	// This is to allow destruction of a threadpool without having done
	// shutdown. Can happen due to wrong order of destructors or failed
	// modloads.
	//
	shutdown();
	delete [] id;
	id = NULL;
	delete [] thread_clname;
	thread_clname = NULL;
}

// Function : thread_handler
//
// Wrapper function to specify in thread_create to call	deferred_task_handler
//
// static
void
threadpool::thread_handler(threadpool_worker_t *thrp)
{
	// Record the worker thread control object in thread specific data
	threadpool_worker_t::worker.set_tsd((uintptr_t)thrp);

	// Process work
	thrp->deferred_task_handler();

	// thread exits
	delete thrp;
}

//
// Function: defer_processing
//
// Enqueue a task for the threadpool to handle later.
// Normally when no idle worker threads are present,
// the task is appended to the end of the task queue.
// When prepend is true, the task is prepended to the start
// of the task queue.
//
void
threadpool::defer_processing(defer_task *taskp, bool prepend)
{
	ASSERT(taskp != NULL);
	lock();
	//
	// A task should only be added to a threadpool when there is some
	// thread to process it. The code actually supports this as
	// a later change_num_threads will cause the queue to be processed.
	// However, this is a useful assert as we do not anticipate this
	// situation in our current product.
	//
	ASSERT(thr_desired > 0);

#ifndef CLCOMM_COMPAT
	if (generation == SHUTDOWN_GENERATION) {
		if (!ORB::is_shutdown()) {
			//
			// Currently the shutdown is not clean enough to make
			// this an unconditional panic
			//

			//
			// SCMSGS
			// @explanation
			// During shutdown this operation is not allowed.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clutil: Adding deferred task after threadpool "
			    "shutdown id %s", ((id == NULL) ? "" : id));
		}
		unlock();
		taskp->task_done();
		return;
	}
#endif
	if (dynamic_threadpool) {
		//
		// Last MAX_THREADS_AVG values of idle threads is stored in a
		// circular buffer. Sum of these MAX_THREADS_AVG values is also
		// maintained and used when finding the average
		// value.
		//

		// Array index must be within MAX_THREADS_AVG
		ASSERT(arr_index < MAX_THREADS_AVG);
		sum_arr -= arr_thr_idle[arr_index];

		// Check that sum does not fall below zero
		ASSERT(sum_arr >= 0);
		arr_thr_idle[arr_index] = thr_idle;
		sum_arr += thr_idle;
		arr_index++;
		arr_index %= MAX_THREADS_AVG;
		avg_thr_idle = sum_arr / MAX_THREADS_AVG;

	}
	if (blocked_processing != 0) {
		//
		// The threadpool is currently blocked.
		// Append task to blocked list.
		//
		ASSERT(pending.empty());

		if (prepend) {
			blocked_waiting.prepend(taskp);
		} else {
			blocked_waiting.append(taskp);
		}

		THR_DBG(("%s Defer blocked: %p des %d idle %d avg %d tot %d "
		    "count %d\n", id, taskp, thr_desired, thr_idle,
		    avg_thr_idle, thr_total, count));

		// Increment count of queued tasks
		count++;
	} else {
		//
		// Threadpool currently not blocked.
		//
		ASSERT(blocked_waiting.empty());

		//
		// Try to get another worker thread.
		// The worker thread must be taken off
		// the list in order to be multi-thread safe.
		//
		threadpool_worker_t	*thrp = idle_list.reapfirst();
		if (thrp != NULL) {
			//
			// We found an idle worker thread.
			// Show that the thread has been woken up with work.
			//
			thrp->my_state = threadpool_worker_t::WORK;

			//
			// By giving a worker thread its task here,
			// we guarantee that the worker thread
			// will have work. This approach also
			// reduces unnecessary worker thread wakeups
			// caused by races for work tasks.
			//
			thrp->my_taskp = taskp;

			// Wake worker thread
			wakeup_worker(thrp);

			THR_DBG(("%s Defer no_wait: %p des %d idle %d avg %d"
			    " tot %d count %d\n",
			    id, taskp, thr_desired, thr_idle,
			    avg_thr_idle, thr_total, count));
		} else {
			// No idle worker thread.
			// Must defer the work.
			//
			if (prepend) {
				pending.prepend(taskp);
			} else {
				pending.append(taskp);
			}

			// Increment count of queued tasks
			count++;

			THR_DBG(("%s Defer wait: %p des %d idle %d avg %d"
			    " tot %d count %d\n",
			    id, taskp, thr_desired, thr_idle,
			    avg_thr_idle, thr_total, count));
		}
	}
	unlock();
}

//
// Function: add_server_thread
//
// Add new threads to the threadpool.
// All created threads are part of threadpool_worker_t instance
// which manages worker thread for a threadpool.
//
// Returns the number of threads added successfully.
//
//
int
threadpool::add_server_thread(int thread_count)
{
	int i, err;
	threadpool_worker_t	*thrp;

	// Need to add threads to the driver to deliver the callback to
	for (i = 0; i < thread_count; i++) {
		lock();

		// Do not create threads after shutdown
		if (generation == SHUTDOWN_GENERATION) {
			unlock();
			break;
		}

		// Do not create more threads than cap specified
		if (thr_total >= thr_max) {
			unlock();
			break;
		}

		//
		// We do a non-blocking allocation, because the caller has to
		// handle memory failures from thread_create. There
		// seems to be little point in blocking the server thread in
		// this case.
		//
		/*CSTYLED*/
		thrp = new (os::NO_SLEEP) threadpool_worker_t(this, generation);
		if (thrp == NULL) {
			unlock();
			THR_DBG(("%s add_server_thread: no memory for "
			    "server thread args\n", id));
			break;
		}
		//
		// Increment thr_total here to catch case where shutdown
		// is called before thread has a chance to startup
		//
		thr_total++;
		ASSERT(thr_total > 0);

		// Account for threads in the middle of thread creation.
		thr_in_creation++;
		ASSERT(thr_in_creation > 0);

		//
		// Take local copies before dropping lock
		// In case these change after dropping the lock but before the
		// thread_handler gets to run, set_sched_props would have
		// changed the generation number and we will exit and recreate
		// the thread.
		//
		int	pri = thread_pri;
		char	*clname = thread_clname;
		unlock();
#ifdef _KERNEL
		err = clnewlwp((void (*)(void *))thread_handler, thrp, pri,
		    clname, NULL);
#else
		err = clnewlwp_stack((void (*)(void *))thread_handler, thrp,
		    pri, clname, NULL, (size_t)thr_stacksize);
#endif
		if (err != 0) {
			THR_DBG(("%s thread/lwp create failed\n", id));
			lock();

			ASSERT(thr_total != 0);
			thr_total--;

			ASSERT(thr_in_creation != 0);
			thr_in_creation--;

			threadidle.broadcast();
			unlock();

			delete thrp;
			break;
		}
	}
	return (i);
}

//
// worker_exit - a worker thread is terminating. Adjust threadpool state.
//
void
threadpool::worker_exit(threadpool_worker_t *thrp)
{
	ASSERT(lock_held());

	// Reduce count of worker threads
	ASSERT(thr_total > 0);
	thr_total--;
	ASSERT(thr_idle <= thr_total);

	// Synchronize with both shutdown and any blocked threads.
	threadidle.broadcast();
	unlock();

	//
	// When generation changed and not shutting down, create new lwp
	// to replace the terminating worker thread.
	//
	if ((generation != thrp->my_gen) &&
	    (generation != SHUTDOWN_GENERATION)) {
		//
		// If we cannot create threads this early during boot
		// the node probably has insufficient memory anyway.
		//
		if (add_server_thread(1) == 0) {
#ifndef CLCOMM_COMPAT
			//
			// SCMSGS
			// @explanation
			// There was insufficient memory to support this
			// operation.
			// @user_action
			// Install more memory, increase swap space, or reduce
			// peak memory consumption.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clutil: Could not create lwp during respawn");
#endif
		}
	}
}

//
// worker_idle - the worker thread found no work.
// The threadpool must decide what to do with the worker thread.
//
void
threadpool::worker_idle(threadpool_worker_t *thrp)
{
	ASSERT(lock_held());

	//
	// Check if need to exit thread.
	//
	// Changing value of thr_desired to lower values will
	// automatically cause the excess threads to exit next
	// time through the loop.
	//
	// In case of static threadpools, avg_thr_idle is set to
	// thr_desired + 1, and the value does not change in
	// response to differing work loads.
	// The following check does cause worker threads in static
	// threadpools to exit when parameters are explicitly changed.
	//
	// We check both the current number of idle threads
	// (thr_idle) and the moving average, so that we
	// have some hysteresis during periods of peak demand.
	//
	if ((avg_thr_idle > thr_desired) &&
	    (thr_idle > thr_desired)) {
		//
		// There are more threads than required.
		// Tell the worker thread that it is surplus.
		//
		thrp->my_state = threadpool_worker_t::SURPLUS;
		return;
	}

	// Increment number of idle threads
	thr_idle++;
	ASSERT(thr_idle > 0);
	ASSERT(thr_idle <= thr_total);

	//
	// Signal any waiters that we are idle.
	// This is cheap when there are no waiters
	// and usually there are no waiters.
	//
	threadidle.broadcast();

	// Show that the worker should be sleeping on queue
	thrp->my_state = threadpool_worker_t::SLEEP;

	// Place the idle thread at the start of the queue
	idle_list.prepend(thrp);

	//
	// Wait to be woken up for more work.
	//
	// In order to be multi-thread safe,
	// the waiting thread must be removed
	// from the list before being woken up.
	//
	// Solaris occasionally incorrectly
	// wakes up a thread from cv_wait, in which case
	// the worker thread will still be on the idle
	// worker thread list and we go back to sleep.
	//
	do {
		thrp->threadcv.wait(get_lock());
	} while (thrp->my_state == threadpool_worker_t::SLEEP);
}

//
// Function: respawn
//
// Changes the generation number and wakes up all the threads
// In deferred_task_handler, threads will notice the change in generation
// number and exit while creating a new thread.
// This is used when we change the properties of the threads/lwps such as
// priority or scheduling class.
// Assumes it is called with lock held. XX If this needs to be a
// public interface we may want a flag to indicate whether to grab the
// lock or is already held
//
void
threadpool::respawn(void)
{
	ASSERT(lock_held());
	ASSERT(generation != SHUTDOWN_GENERATION);
	generation++;
	generation_change();
	// Leave task lists unchanged - dont discard any tasks
}

//
// Function: quiesce
//
// This routine is an attempt to wait for the threadpool to reach a particular
//	state. It is still a work-in-progress and does not work for the
//	general case.
//
void
threadpool::quiesce(threadpool::fence_t typ)
{
	switch (typ) {
	case QFENCE : {
		//
		// XXX This does not work if the number of threads is greater
		// than one, as the fence may succeed before some other threads
		// are done processing their tasks.
		//
		ASSERT(!dynamic_threadpool);
		ASSERT(thr_desired == 1);
		ASSERT(generation != SHUTDOWN_GENERATION);

		//
		// Create a fence task, add to the queue and wait for it to
		// reach the head of the queue.
		// fence_task is not dynamically allocated - hence implements
		// a task_done method that does nothing.
		//
		fence_task	fta(false);
		defer_processing(&fta);
		fta.wait();
		break;
	}
	case QBLOCK_EMPTY :
		//
		// Waits for processing to complete and all threads return to
		// idle state. Meanwhile it adds new work to a blocked queue
		// It also moves any existing pending work not already being
		// processed to the blocked queue.
		// This does an implicit block_processing() call and a
		// matching unblock_processing() call is needed to resume task
		// processing.
		//
		block_processing();

		// Fall through to next case to do wait for empty
	case QEMPTY :
		//
		// This does not actually stop adding work but returns when
		// it finds the threadpool in a state with no more work to do.
		// Use block_processing or QBLOCK_EMPTY when you want to block
		// new work.
		//
		lock();

		//
		// In kernel we need to wait for all threads to be idle
		// before exiting from threadpool object.
		//
		// In userland processes we may have a scenario wherein process
		// exit is called before some of the threads got a chance to
		// startup. This case is handled by NOT waiting for in_creation
		// threads. This scenario is not possible in kernel
		// because scheduler will make sure all created threads are
		// quiesced before shutting down.
		//
#ifdef _KERNEL
		while (!pending.empty() || (thr_total != thr_idle)) {
#else
		while (!pending.empty() || (thr_total !=
		    (thr_idle + thr_in_creation))) {
#endif
			threadidle.wait(get_lock());
		}
		unlock();
		break;
	default :
		ASSERT(!"Invalid quiesce type to threadpool");
		break;
	}
}

//
// Block - block_processing() method will checkpoint the current state of the
//	task list and redirect all future tasks added to a waiting list
//	This will prevent any new tasks from getting executed.
//	Threads currently processing tasks are not aborted, they become idle
//	eventually.
//	Use quiesce(QBLOCK_EMPTY) or quiesce(QEMPTY) if need to wait
//	for the current	tasks to be finished executing
//
void
threadpool::block_processing()
{
	lock();
	if (blocked_processing++ == 0) {
		blocked_waiting.concat(pending);
		threadidle.broadcast();	// signal waiters for pending empty
	}
	ASSERT(pending.empty());
	ASSERT(blocked_processing != 0); // check for wraparound
	unlock();
}

//
// Unblock - unblock_processing() method resumes processing the tasks, if
//	this is the last unblock. The method moves tasks from blocked list to
//	pending list. Then it wakes up idle threads to process tasks
//	that can now be processed.
//
//	Block/unblock can nest - If multiple blocks are done, the
//	corresponding number of unblocks must be done. Only the last unblock
//	reenables task processing.
//
void
threadpool::unblock_processing()
{
	lock();
	ASSERT(blocked_processing > 0);
	blocked_processing--;
	if (blocked_processing == 0) {
		// No remaining blocks. Resume processing the tasks
		pending.concat(blocked_waiting);
		ASSERT(blocked_waiting.empty());

		while (!pending.empty()) {
			//
			// There is more pending work.
			//
			// Try to get another worker thread.
			// The worker thread must be taken off
			// the list in order to be multi-thread safe.
			//
			threadpool_worker_t	*thrp = idle_list.reapfirst();
			if (thrp != NULL) {
				//
				// We found an idle worker thread.
				// Show thread has been woken up with work.
				//
				thrp->my_state = threadpool_worker_t::WORK;

				//
				// By giving a worker thread its task here,
				// we guarantee that the worker thread
				// will have work. This approach also
				// reduces unnecessary worker thread wakeups
				// caused by races for work tasks.
				//
				thrp->my_taskp = pending.reapfirst();
				ASSERT(thrp->my_taskp != NULL);

				// Adjust count of pending or blocked work
				ASSERT(count > 0);
				count--;

				// Wake up worker thread
				wakeup_worker(thrp);
			} else {
				//
				// All the worker threads are busy.
				// The worker threads will process this
				// new work when they finish current work.
				//
				// There are forms of threadpool "blocking"
				// that do not stop current work in progress.
				// So it is possible for the unblock to
				// occur when all of the worker threads are
				// busy.
				//
				break;
			}
		}
	}
	unlock();
}

//
// Function: generation_change
//
// Wake up idle worker threads because of thread generation change.
// It is safe to call this method multiple times from shutdown.
//
void
threadpool::generation_change()
{
	ASSERT(lock_held());

	threadpool_worker_t	*thrp;
	while ((thrp = idle_list.reapfirst()) != NULL) {
		//
		// We found an idle worker thread.
		// Show thread has been woken up for generation change.
		//
		thrp->my_state = threadpool_worker_t::CHANGE;

		// Wake it up in order to make it go away.
		//
		wakeup_worker(thrp);
	}
}

//
// Function: shutdown
//
// Change the generation number, wake up all threads and wait for all to exit.
// Any pending tasks in the task lists are discarded by calling task_done()
// Note that shutdown is also called from the destructor, so this routine should
// be safe w.r.t. to being called two times.
//
void
threadpool::shutdown(void)
{
	lock();
	generation = SHUTDOWN_GENERATION;
	generation_change();

	//
	// XX In user programs, it is probably safer to do a thr_join instead
	// of wait.
	//
	//
	// In kernel we need to wait for all threads to be idle
	// before exiting from threadpool object.
	//
	// In userland processes we may have a scenario wherein process
	// exit is called before some of the threads got a chance to
	// startup. This case is handled by NOT waiting for in_creation
	// threads. This scenario is not possible in kernel
	// because scheduler will make sure all created threads are
	// quiesced before shutting down.
	//
#ifdef _KERNEL
	while ((thr_total) != 0) {
#else
	while ((thr_total - thr_in_creation) != 0) {
#endif
		threadidle.wait(get_lock());
	}
	defer_task	*taskp;
	while ((taskp = pending.reapfirst()) != NULL) {
		taskp->task_done();
	}
	while ((taskp = blocked_waiting.reapfirst()) != NULL) {
		taskp->task_done();
	}
	// Reset count of tasks still in queue
	count = 0;

	threadidle.broadcast();	// signal waiters in quiesce
	unlock();
}

//
// Set a name for the threadpool for ease of debugging
//
void
threadpool::set_name(const char *name)
{
	if (name == NULL) {
		name = "(unnamed threadpool)";
	}
	if (id)
		delete [] id;	// delete old name
	id = os::strdup(name);
}

//
// Change the dynamic threadpool property
//
// If disabling dynamic threads, should set avg_thr_idle to thr_desired+1.
// This will cause any extra threads create to exit lazyily when
// done in deferred_task_handler. No point actively waiting for
// the threads to exit here.
//
void
threadpool::set_dynamic_threads(bool flag)
{
	lock();
	if (!flag) {
		avg_thr_idle = thr_desired + 1;
	}
	dynamic_threadpool = flag;
	unlock();
}

//
// change_num_threads
//	Increase or Decrease (or pass 0 for no change) the number threads
//	in the threadpool.
//	Returns the number of change in number of threads.
//	This adjusts thr_desired (which is the preferred number of threads in
//	the threadpool). It always ensures there is a minimum of 1 thread.
//
int
threadpool::change_num_threads(int delta)
{
	if (0 == delta) {
		// Do not want any change.
		return (0);
	}

	lock();
	int old_desired = thr_desired;

	//
	// The desired number of threads can temporarily drop
	// below zero when thread transfers are pending.
	//
	thr_desired += delta;
	THR_DBG(("%s thr_desired %d\n", id, thr_desired));

	// Do not allow thr_desired to exceed thr_max
	if (thr_desired > thr_max) {
	    thr_desired = thr_max;
	}

	//
	// Adjust avg_thr_idle - see set_dynamic_threads above for comments
	// about lazy thread destruction.
	//
	if (!dynamic_threadpool) {
		avg_thr_idle = thr_desired + 1;

		if (thr_total > avg_thr_idle) {
			//
			// The system was about to destroy more threads than
			// were needed. By adjusting the counts not to destroy
			// those threads, we effectively create the needed
			// threads.
			//
			delta = thr_desired - old_desired;
			unlock();
			return (delta);
		}
	}
	delta = thr_desired - old_desired;
	unlock();

	int delta_achieved;
	if ((delta > 0) &&
	    ((delta_achieved = add_server_thread(delta)) < delta)) {
		THR_DBG(("%s Created %d instead of %d server threads\n",
		    id, delta_achieved, delta));
		lock();
		if (!dynamic_threadpool) {
			//
			// Non-dynamic threadpool only creates thread by
			// explicit commands.
			// Readjust counts to reflect threads actually created.
			//
			ASSERT(delta_achieved >= 0);
			ASSERT(delta_achieved <= delta);
			int	delta_diff = delta - delta_achieved;
			thr_desired -= delta_diff;
			avg_thr_idle -= delta_diff;
			ASSERT(thr_desired + 1 == avg_thr_idle);
		}
		delta = thr_desired - old_desired;
		unlock();
	}
	return (delta);
}

//
// change_max_threads
//	Increase or Decrease (or pass 0 for no change) thr_max value
//	in the threadpool.
//	Only makes the change if thr_desired <= new thr_max
//
bool
threadpool::change_max_threads(int delta)
{
	// Do not allow new value of max to be <= 0
	ASSERT((delta >= 0) ? true : (thr_max > (0-delta)));

	lock();
	int new_max = thr_max + delta;
	if (thr_desired > new_max) {
		//
		// Cannot change thr_max if already have more threads
		// use change_num_threads to change thr_desired
		// We allow changing thr_desired even if thr_total is higher
		// This will prevent the further creation of threads
		// NOTE: this implies we cannot ASSERT that thr_total <= thr_max
		// but this is useful functionality
		//
		unlock();
		return (false);
	}
	thr_max = new_max;
	unlock();
	return (true);
}

//
// transfer_worker_threads - asynchronously transfers the specified number
// of threads from the source threadpool to this threadpool.
//
// The caller is responsible for knowing if there are enough threads
// to satisfy this request.
//
// A zero thread transfer request is a no-op.
//
void
threadpool::transfer_worker_threads(int num_transfers, threadpool *from_poolp)
{
	//
	// Enqueue one transfer task per desired worker thread.
	// If all workers threads are currently working,
	// place the task at the front of the threadpool task queue.
	//
	for (int i = 0; i < num_transfers; i++) {
		from_poolp->defer_processing(new transfer_task(this), true);

	}
}

//
// Remove any pending defer_tasks with an address tag
// that matches the specified address
//
void
threadpool::remove_pending_with_address_tag(void *match_addrp)
{
	defer_task *iter_taskp = NULL;

	lock();
	task_list_t::ListIterator iter(pending);
	while ((iter_taskp = iter.get_current()) != NULL) {

		//
		// The correct way to erase from the list while iterating
		// over it is to advance the iterator before doing the erase.
		//
		iter.advance();

		if (match_addrp == iter_taskp->get_address_tag()) {
			// Remove the task from pending list.
			(void) pending.erase(iter_taskp);

			//
			// Removal from list does not free up memory.
			// Free up the memory allocated for the task structure.
			//
			delete iter_taskp;
		}
	}
	unlock();
}

//
// defer_task class methods
//

defer_task::~defer_task()
{
}

// Default implementation for all tasks (assumes task was allocated from heap)
void
defer_task::task_done()
{
	delete this;
}

uint_t
defer_task::get_defer_task_tag()
{
	return (DEFAULT_TAG);
}

//
// The default value is NULL
//
void *
defer_task::get_address_tag()
{
	return (NULL);
}

//
// transfer_task class methods
//

transfer_task::transfer_task(threadpool *new_threadpoolp)
{
	threadpoolp = new_threadpoolp;
}

transfer_task::~transfer_task()
{
	threadpoolp = NULL;		// Make lint happy
}

//
// execute - transfers the worker thread to a new threadpool
//
void
transfer_task::execute()
{
	int		loop_index;

	// Obtain the worker thread control object from thread specific data
	threadpool_worker_t	*thrp =
	    (threadpool_worker_t *)threadpool_worker_t::worker.get_tsd();

	//
	// Currently only support transferring worker threads between
	// threadpool's with the same priority and scheduling class.
	// The threadpool creates new threads when this information changes,
	// so there is little point in supporting transfers between
	// threadpool's that differ in this regard.
	//
	ASSERT(thrp->threadpoolp->thread_pri == threadpoolp->thread_pri);

	ASSERT(thrp->my_state == threadpool_worker_t::WORK);

	thrp->threadpoolp->lock();

	// Reduce worker thread count in old threadpool
	ASSERT(thrp->threadpoolp->thr_total > 0);
	thrp->threadpoolp->thr_total--;
	thrp->threadpoolp->thr_desired--;
	thrp->threadpoolp->avg_thr_idle--;
	if (thrp->threadpoolp->avg_thr_idle < 0) {
		thrp->threadpoolp->avg_thr_idle = 0;
	}
	//
	// Every value in last MAX_THREADS_AVG idle values gets decremented by 1
	// so that decreasing of average by 1 gets reflected
	// properly
	//
	for (loop_index = 0; loop_index < MAX_THREADS_AVG; loop_index++) {
		if (thrp->threadpoolp->arr_thr_idle[loop_index] > 0) {
			thrp->threadpoolp->arr_thr_idle[loop_index]--;
			thrp->threadpoolp->sum_arr--;
		}
	}

	// Synchronize with both shutdown and any blocked threads.
	thrp->threadpoolp->threadidle.broadcast();

	thrp->threadpoolp->unlock();

	// Transfer worker thread to new threadpool
	thrp->threadpoolp = threadpoolp;

#ifdef DEBUG
	// A different lock will be used with this condition variable.
	thrp->threadcv.reset();
#endif

	threadpoolp->lock();

	// Increase worker thread count in new threadpool
	ASSERT(threadpoolp->thr_total < threadpoolp->thr_max);
	threadpoolp->avg_thr_idle++;
	//
	// Every value in last MAX_THREADS_AVG idle values gets incremented by 1
	// so that increasing of average by 1 gets reflected
	// properly
	//
	for (loop_index = 0; loop_index < MAX_THREADS_AVG; loop_index++) {
		thrp->threadpoolp->arr_thr_idle[loop_index]++;
		thrp->threadpoolp->sum_arr++;
	}
	threadpoolp->thr_desired++;
	threadpoolp->thr_total++;
	ASSERT(threadpoolp->thr_total > 0);

	threadpoolp->unlock();

	delete this;
}

//
// fence_task class methods
//
// Fence task is used to wake up the caller when this item reaches the head
// of the queue.
// Note that this is precise only if there is one thread in the threadpool.
//
void
fence_task::execute()
{
	waitcv.signal();
}

// Fence task is sometimes allocated on the task and implements task_done method
void
fence_task::task_done()
{
	if (waitcv.get_state() == os::notify_t::WAITING) {
		//
		// This implies the task is being deleted without going
		// through execute(). signal the waiter to wake up
		// For single-thread threadpools, the semantics are still
		// preserved by doing this. We are usually deleting this task
		// only after the previous tasks are done
		//
		waitcv.signal();
	}
	if (on_heap) {
		delete this;
	}
}

//
// task class methods
//

//
// task destructor
//
// Lint complains about neither zero'ing nor freeing
// the pointer members: method and arg. This is safe for this class.
//
//lint -e1540
work_task::~work_task()
{
}
//lint +e1540

//
// Generic work_task method to call arbitrary static method with one arg
// Deletes itself (by calling task_done) after calling the appropriate method.
//
void
work_task::execute()
{
	(*method)(arg);
	task_done();
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <sys/threadpool_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
