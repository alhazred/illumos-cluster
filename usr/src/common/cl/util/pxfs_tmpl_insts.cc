//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_tmpl_insts.cc	1.5	09/03/03 SMI"

//
// PXFS
//

#include <orb/object/adapter.h>
#include <repl/service/replica_tmpl.h>
#include <h/px_aio.h>

template class proxy<pxfs_aio::aio_callback_stub>;

#include <h/bulkio.h>
template class _FixedSeq_<bulkio::file_hole>;

template class _ManagedSeq_<_FixedSeq_<bulkio::file_hole>, bulkio::file_hole>;

template class _T_var_<_FixedSeq_<bulkio::file_hole>,
    _FixedSeq_<bulkio::file_hole>*>;


template class proxy<bulkio::aio_write_obj_stub>;
template class proxy<bulkio::in_aio_stub>;
template class proxy<bulkio::in_pages_stub>;
template class proxy<bulkio::in_uio_stub>;
template class proxy<bulkio::inout_pages_stub>;
template class proxy<bulkio::inout_uio_stub>;
template class proxy<bulkio::pages_write_obj_stub>;
template class proxy<bulkio::uio_write_obj_stub>;
template class proxy<bulkio_holder::holder_stub>;
template class proxy<bulkio::in_aio_pages_stub>;

template class McServerof<bulkio_holder::holder>;

template class McServerof<pxfs_aio::aio_callback>;

#include <h/pxfs.h>
template class _NormalSeq_<fs::unixdir::direntry_t>;

template class _T_var_<fs::bind_info, fs::bind_info*>;
template class _T_var_<fs::secattr, fs::secattr*>;

template class McServerof<fs::dircache>;
template class McServerof<fs::dirprov>;
template class McServerof<fs::file>;
template class McServerof<fs::filesystem>;
template class McServerof<fs::fobjcache>;
template class McServerof<fs::fobjprov>;
template class McServerof<fs::fs_collection>;
template class McServerof<fs::fsmgr_client>;
template class McServerof<fs::fsmgr_server>;
template class McServerof<fs::io>;
template class McServerof<fs::memcache>;
template class McServerof<fs::mempager>;
template class McServerof<fs::mount_client>;
template class McServerof<fs::pxfs_llm_callback>;
template class McServerof<fs::special>;
template class McServerof<fs::symbolic_link>;
template class McServerof<fs::unixdir>;

template class mc_replica_of<fs::symbolic_link>;
template class mc_replica_of<fs::unixdir>;

template class proxy<fs::dc_callback_stub>;
template class proxy<fs::device_stub>;
template class proxy<fs::dircache_stub>;
template class proxy<fs::dirprov_stub>;
template class proxy<fs::file_stub>;
template class proxy<fs::filesystem_stub>;
template class proxy<fs::fobj_stub>;
template class proxy<fs::fobjcache_stub>;
template class proxy<fs::fobjprov_stub>;
template class proxy<fs::fs_collection_stub>;
template class proxy<fs::fsmgr_client_stub>;
template class proxy<fs::fsmgr_server_stub>;
template class proxy<fs::io_stub>;
template class proxy<fs::memcache_stub>;
template class proxy<fs::mempager_stub>;
template class proxy<fs::mount_client_died_stub>;
template class proxy<fs::mount_client_stub>;
template class proxy<fs::mount_server_stub>;
template class proxy<fs::pxfs_llm_callback_stub>;
template class proxy<fs::special_stub>;
template class proxy<fs::symbolic_link_stub>;
template class proxy<fs::unixdir_stub>;

template class mc_replica_of<fs::dc_callback>;
template class mc_replica_of<fs::dirprov>;
template class mc_replica_of<fs::file>;
template class mc_replica_of<fs::filesystem>;
template class mc_replica_of<fs::fobjprov>;
template class mc_replica_of<fs::fsmgr_server>;
template class mc_replica_of<fs::io>;
template class mc_replica_of<fs::mempager>;
template class mc_replica_of<fs::mount_client_died>;
template class mc_replica_of<fs::mount_server>;
template class mc_replica_of<fs::special>;

#include <h/repl_pxfs.h>
template class _FixedSeq_<repl_pxfs::lock_info_t>;

template class proxy<repl_pxfs::ha_mounter_stub>;
template class proxy<repl_pxfs::mount_replica_stub>;

template class mc_checkpoint<repl_pxfs::mount_replica>;

template class repl_server<repl_pxfs::mount_replica>;

#include <h/pxfs_v1.h>

template class _NormalSeq_<pxfs_v1::unixdir::direntry_t>;
template class _T_var_<pxfs_v1::secattr, pxfs_v1::secattr *>;

template class proxy<pxfs_v1::file_stub>;
template class proxy<pxfs_v1::filesystem_stub>;
template class proxy<pxfs_v1::fobj_client_stub>;
template class proxy<pxfs_v1::fobj_stub>;
template class proxy<pxfs_v1::fobjplus_stub>;
template class proxy<pxfs_v1::fsmgr_client_stub>;
template class proxy<pxfs_v1::fsmgr_server_stub>;
template class proxy<pxfs_v1::io_stub>;
template class proxy<pxfs_v1::pxfs_llm_callback_stub>;
template class proxy<pxfs_v1::special_stub>;
template class proxy<pxfs_v1::symbolic_link_stub>;
template class proxy<pxfs_v1::unixdir_stub>;

template class McServerof<pxfs_v1::file>;
template class McServerof<pxfs_v1::filesystem>;
template class McServerof<pxfs_v1::fobj_client>;
template class McServerof<pxfs_v1::fsmgr_client>;
template class McServerof<pxfs_v1::fsmgr_server>;
template class McServerof<pxfs_v1::io>;
template class McServerof<pxfs_v1::pxfs_llm_callback>;
template class McServerof<pxfs_v1::special>;
template class McServerof<pxfs_v1::symbolic_link>;
template class McServerof<pxfs_v1::unixdir>;

template class mc_replica_of<pxfs_v1::file>;
template class mc_replica_of<pxfs_v1::filesystem>;
template class mc_replica_of<pxfs_v1::fsmgr_server>;
template class mc_replica_of<pxfs_v1::io>;
template class mc_replica_of<pxfs_v1::special>;
template class mc_replica_of<pxfs_v1::symbolic_link>;
template class mc_replica_of<pxfs_v1::unixdir>;

#include <h/repl_pxfs_v1.h>

template class _FixedSeq_<repl_pxfs_v1::lock_info_t>;
template class _NormalSeq_<repl_pxfs_v1::fobj_ckpt_t>;

template class proxy<repl_pxfs_v1::fs_replica_stub>;

template class mc_checkpoint<repl_pxfs_v1::fs_replica>;
template class repl_server<repl_pxfs_v1::fs_replica>;

//
// The impl_spec template builds a handler as part of the template.
// This means that the template has to know the handler.
//
// XXX - the interim solution was to move the PXFS bulkio code
// to the cl_comm module. This is improper for a variety of reasons.
// 1) The PXFS bulkio code should be in the PXFS kernel module.
// 2) The cl_comm module already exceeds the maximum size supported
// by dtrace and must be reduced in size.
// This probably means that we need to support a tmpl_insts file for PXFS.
//
#include <pxfs/bulkio/bulkio_handler.h>

template class impl_spec<bulkio::in_uio, bulkio_handler_inuio>;
template class impl_spec<bulkio::aio_write_obj, bulkio_handler_inoutaiowrite>;
template class impl_spec<bulkio::in_aio, bulkio_handler_inaio>;
template class impl_spec<bulkio::in_aio_pages, bulkio_handler_in_aio_pages>;
template class impl_spec<bulkio::in_pages, bulkio_handler_in_pages>;
template class impl_spec<bulkio::inout_pages, bulkio_handler_inout_pages>;
template class impl_spec<bulkio::inout_uio, bulkio_handler_inoutuio>;
template class impl_spec<bulkio::pages_write_obj,
    bulkio_handler_inoutpageswrite>;
template class impl_spec<bulkio::uio_write_obj, bulkio_handler_inoutuiowrite>;
