//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ha_tmpl_insts.cc	1.2	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/replica_int.h>

template class Anchor<replica_int::rmm_stub>;
template class McServerof<replica::repl_prov>;
template class McServerof<replica::trans_state>;
template class McServerof<replica::transaction_id>;
template class McServerof<replica_int::handler_repl_prov>;
template class McServerof<replica_int::reconnect_object>;
template class McServerof<replica_int::rma_admin>;
template class McServerof<replica_int::rma_reconf>;
template class McServerof<replica_int::rma_repl_prov>;
template class McServerof<replica_int::tid_factory>;

template class McNoref<replica_int::rmm>;

#include <repl/service/replica_tmpl.h>
#include <h/repl_ns.h>
template class mc_checkpoint<repl_ns::ns_replica>;
template class repl_server<repl_ns::ns_replica>;

#include <h/repl_rm.h>
template class mc_checkpoint<repl_rm::rm_ckpt>;

template class mc_replica_of<replica::repl_prov_reg>;
template class mc_replica_of<replica_int::rm>;
template class mc_replica_of<replica_int::rm_service_admin>;
template class mc_replica_of<replica_int::cb_coord_control>;

template class repl_server<repl_rm::rm_ckpt>;
