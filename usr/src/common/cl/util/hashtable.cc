/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hashtable.cc	1.23	08/05/20 SMI"

#include <sys/hashtable.h>

#if defined(DEBUG) && !defined(_KERNEL)
#include <orb/infrastructure/orb.h>
#endif

base_hash_table::base_hash_table(uint_t size,
    uint_t (*func)(uint64_t, uint_t), base_equal_key_function equal,
    base_free_key_function free_func,
    base_dup_key_function dup_func):
	hash_size(size),
	hashfunc(func),
	refcount(0),
	status(false),
	equalfunc(equal),
	freefunc(free_func),
	dupfunc(dup_func)
{
	// func cannot be NULL
	ASSERT(func != NULL);

	hash_buckets = new hash_entry_ptr[size];

	if (hash_buckets != NULL) {
		bzero(hash_buckets, size * sizeof (hash_entry_ptr));
		status = true;
	}
}

base_hash_table::~base_hash_table()
{
#if defined(DEBUG) && !defined(_KERNEL)
	// User land versions do not shutdown cleanly. So if the empty()
	// check fails after ORB is shutdown, we do not fail the ASSERT
	ASSERT(empty() || ORB::is_shutdown());
#endif
	hashfunc = NULL;
	equalfunc = NULL;
	freefunc = NULL;
	dupfunc = NULL;
	delete [] hash_buckets;
	hash_buckets = NULL;
}

void
base_hash_table::dispose(bool destroy)
{
	for (uint_t i = 0; i < hash_size; i++) {
		hash_entry_ptr *pp = &hash_buckets[i];
		hash_entry_ptr p;

		while ((p = *pp) != NULL) {
			// pp is pointer to whatever points to p
			*pp = p->he_next;
			if (destroy) {
				// Free the key if necessary
				freefunc(p->he_key);
				// Now delete the entry
				delete p;
			} else {
				// Just for safety NULL out next pointer
				p->he_next = NULL;
			}
		}
		ASSERT(hash_buckets[i] == NULL);
	}
	refcount = 0;
}

inline base_hash_table::hash_entry_ptr *
base_hash_table::hash(uint64_t key)
{
	ASSERT((*hashfunc)(key, hash_size) < hash_size);
	return (&hash_buckets[(*hashfunc)(key, hash_size)]);
}

uintptr_t
base_hash_table::_lookup(uint64_t key)
{
	// Optimize in case hash table is empty
	if (empty())
		return ((uintptr_t)0);

	hash_entry_ptr p = *(hash(key));

	while (p != NULL) {
		if (equalfunc(key, p->he_key)) {
			return (p->he_entry);
		}
		p = p->he_next;
	}
	return ((uintptr_t)0);
}

void
base_hash_table::_add(uintptr_t entry, uint64_t key, hash_entry_ptr hashp)
{
	hash_entry_ptr *headp = hash(key);

	ASSERT(entry != (uintptr_t)0);
	hashp->he_entry = entry;
	hashp->he_key = dupfunc(key);
	hashp->he_next = *headp;
	*headp = hashp;
	refcount++;
	ASSERT(refcount != 0);	// check for wraparound
}

uintptr_t
base_hash_table::_remove(uint64_t key, bool destroy)
{
	hash_entry_ptr *pp = hash(key);
	hash_entry_ptr p;
	uintptr_t entry;

	while ((p = *pp) != NULL) {
		if (equalfunc(key, p->he_key)) {
			// pp is pointer to whatever points to p
			*pp = p->he_next;
			entry = p->he_entry;
			if (destroy) {
				// Free the key storage if necessary
				freefunc((uint64_t)p->he_key);
				// Free the hash table entry.
				delete p;
			} else {
				// Just for safety NULL out next pointer
				p->he_next = NULL;
			}
			ASSERT(refcount > 0);
			refcount--;
			return (entry);
		}
		pp = &p->he_next;
	}
	return (NULL);
}

// Advance to the next non NULL element.
void
base_hash_table::iterator::advance()
{
	if (_current != NULL)
		_current = _current->he_next;
	while ((_current == NULL) && _index < (_table->hash_size - 1)) {
		_index++;
		_current = _table->hash_buckets[_index];
	}
}

void
base_hash_table::iterator::reinit(base_hash_table *h)
{
	_index = 0;
	_table = h;
	if ((_current = *(h->hash_buckets)) == NULL) {
		advance();
	}
}

// The default hash function does % on hash size
uint_t
default_hashfunc(uint64_t key, uint_t hsize)
{
	return (((uint_t)key) % hsize);
}

//
// The default equality function compares keys.  Returns true if
// the keys are identical, else returns false.
//
bool
default_equalfunc(uint64_t key1, uint64_t key2)
{
	return (key1 == key2);
}

//
// Effects:  Nothing to free because it's just a 64-bit key.
//
void
default_freefunc(uint64_t)
{
}

//
// Effects:  Nothing to do because caller doesn't wish to allocate storage
//   for a key.  Just return it.
//
uint64_t
default_dupfunc(uint64_t key)
{
	return (key);
}

base_string_hash_table::base_string_hash_table(uint_t size,
    uint_t (*func)(const char *, uint_t)) :
	hash_size(size),
	hashfunc(func),
	refcount(0),
	status(false)
{
	// func cannot be NULL
	ASSERT(func != NULL);

	hash_buckets = new hash_entry_ptr[size];
	if (hash_buckets != NULL) {
		bzero(hash_buckets, size * sizeof (hash_entry_ptr));
		status = true;
	}
}

base_string_hash_table::~base_string_hash_table()
{
	ASSERT(empty());
	hashfunc = NULL;
	delete [] hash_buckets;
	hash_buckets = NULL;
}

void
base_string_hash_table::dispose()
{
	for (uint_t i = 0; i < hash_size; i++) {
		hash_entry_ptr *pp = &hash_buckets[i];
		hash_entry_ptr p;

		while ((p = *pp) != NULL) {
			// pp is pointer to whatever points to p
			*pp = p->he_next;
			delete [] p->he_key;
			delete p;
		}
		ASSERT(hash_buckets[i] == NULL);
	}
	refcount = 0;
}

inline base_string_hash_table::hash_entry_ptr *
base_string_hash_table::hash(const char *key)
{
	ASSERT((*hashfunc)(key, hash_size) < hash_size);
	return (&hash_buckets[(*hashfunc)(key, hash_size)]);
}

uintptr_t
base_string_hash_table::_lookup(const char *key)
{
	// Optimize in case hash table is empty
	if (empty())
		return ((uintptr_t)0);

	hash_entry_ptr p = *(hash(key));

	while (p != NULL) {
		if (strcmp(p->he_key, key) == 0)
			return (p->he_entry);
		p = p->he_next;
	}
	return ((uintptr_t)0);
}

void
base_string_hash_table::_add(uintptr_t entry, const char *key, bool *allocated)
{
	hash_entry_ptr hashp = new hash_entry;
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (hashp != NULL);
	}
	if (hashp == NULL) {
		return;
	}
	hash_entry_ptr *headp = hash(key);

	ASSERT(entry != (uintptr_t)0);
	hashp->he_entry = entry;
	hashp->he_key = os::strdup(key);
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (hashp->he_key != NULL);
	}
	ASSERT(hashp->he_key != NULL);
	hashp->he_next = *headp;
	*headp = hashp;
	refcount++;
	ASSERT(refcount != 0);	// check for wraparound
}

uintptr_t
base_string_hash_table::_remove(const char *key)
{
	hash_entry_ptr *pp = hash(key);
	hash_entry_ptr p;
	uintptr_t entry;

	while ((p = *pp) != NULL) {
		if (strcmp(p->he_key, key) == 0) {
			// pp is pointer to whatever points to p
			*pp = p->he_next;
			entry = p->he_entry;
			delete [] p->he_key;
			delete p;
			ASSERT(refcount > 0);
			refcount--;
			return (entry);
		}
		pp = &p->he_next;
	}
	return ((uintptr_t)0);
}

// See if can use base_hash_table's iterators

// Advance to the next non NULL element.
void
base_string_hash_table::iterator::advance()
{
	if (_current != NULL)
		_current = _current->he_next;
	while ((_current == NULL) && _index < (_table->hash_size - 1)) {
		_index++;
		_current = _table->hash_buckets[_index];
	}
}

void
base_string_hash_table::iterator::reinit(base_string_hash_table *h)
{
	_index = 0;
	_table = h;
	if ((_current = *(h->hash_buckets)) == NULL) {
		advance();
	}
}

// The default_string_hash function
uint_t
default_string_hashfunc(const char *key, uint_t hsize)
{
	//
	// The algorithm that this function implements is attributed to
	// P.J. Weinberger's C Compiler, as referenced in
	// "Compilers: Principles, Techniques, and Tools" by Alfred V. Aho,
	// Ravi Sethi, and Jeffrey D. Ullman (Addison-Wesley, 1986,
	// Copyright 1986 by Bell Telephone Laboratories, Incorporated),
	// page 436.
	//
	uint_t 	h = (uint_t)os::strlen(key);
	uint_t 	c;

	for (uchar_t *p = (uchar_t *)key; (c = *p) != '\0'; p++) {
		uint_t g;

		h = (h << 4) + c;
		if ((g = (h & (uint_t)0xf0000000)) != 0) {
			h ^= (g >> 24);
			h ^= g;
		}
	}
	return (h % hsize);
}

base_obj_hash_table::base_obj_hash_table(uint_t size,
    uint_t (*func)(CORBA::Object_ptr, uint_t)) :
	hash_size(size),
	hashfunc(func),
	refcount(0),
	status(false)
{
	// func cannot be NULL
	ASSERT(func != NULL);

	hash_buckets = new hash_entry_ptr[size];
	if (hash_buckets != NULL) {
		bzero(hash_buckets, size * sizeof (hash_entry_ptr));
		status = true;
	}
}

base_obj_hash_table::~base_obj_hash_table()
{
	ASSERT(empty());
	hashfunc = NULL;
	delete [] hash_buckets;
	hash_buckets = NULL;
}

void
base_obj_hash_table::dispose()
{
	for (uint_t i = 0; i < hash_size; i++) {
		hash_entry_ptr *pp = &hash_buckets[i];
		hash_entry_ptr p;

		while ((p = *pp) != NULL) {
			// pp is pointer to whatever points to p
			*pp = p->he_next;
			CORBA::release(p->he_key);
			delete p;
		}
		ASSERT(hash_buckets[i] == NULL);
	}
	refcount = 0;
}

inline base_obj_hash_table::hash_entry_ptr *
base_obj_hash_table::hash(CORBA::Object_ptr key)
{
	ASSERT((*hashfunc)(key, hash_size) < hash_size);
	return (&hash_buckets[(*hashfunc)(key, hash_size)]);
}

uintptr_t
base_obj_hash_table::_lookup(CORBA::Object_ptr key)
{
	// Optimize in case hash table is empty
	if (empty())
		return ((uintptr_t)0);

	hash_entry_ptr p = *(hash(key));

	while (p != NULL) {
		if (key->_equiv(p->he_key))
			return (p->he_entry);
		p = p->he_next;
	}
	return ((uintptr_t)0);
}

void
base_obj_hash_table::_add(uintptr_t entry, CORBA::Object_ptr key,
	bool *allocated)
{
	hash_entry_ptr hashp = new hash_entry;
	if (allocated != NULL) {
		// set memory allocation status
		*allocated = (hashp != NULL);
	}
	if (hashp == NULL) {
		return;
	}
	hash_entry_ptr *headp = hash(key);

	ASSERT(entry != (uintptr_t)0);
	hashp->he_entry = entry;
	hashp->he_key = CORBA::Object::_duplicate(key);
	hashp->he_next = *headp;
	*headp = hashp;
	refcount++;
	ASSERT(refcount != 0);	// check for wraparound
}

uintptr_t
base_obj_hash_table::_remove(CORBA::Object_ptr key)
{
	hash_entry_ptr *pp = hash(key);
	hash_entry_ptr p;
	uintptr_t entry;

	while ((p = *pp) != NULL) {
		if (key->_equiv(p->he_key)) {
			// pp is pointer to whatever points to p
			*pp = p->he_next;
			entry = p->he_entry;
			CORBA::release(p->he_key);
			delete p;
			ASSERT(refcount > 0);
			refcount--;
			return (entry);
		}
		pp = &p->he_next;
	}
	return ((uintptr_t)0);
}

// See if can use base_hash_table's iterators

// Advance to the next non NULL element.
void
base_obj_hash_table::iterator::advance()
{
	if (_current != NULL)
		_current = _current->he_next;
	while ((_current == NULL) && _index < (_table->hash_size - 1)) {
		_index++;
		_current = _table->hash_buckets[_index];
	}
}

void
base_obj_hash_table::iterator::reinit(base_obj_hash_table *h)
{
	_index = 0;
	_table = h;
	if ((_current = *(h->hash_buckets)) == NULL) {
		advance();
	}
}

// The default_obj_hash function
uint_t
default_obj_hashfunc(CORBA::Object_ptr key, uint_t hsize)
{
	return (key->_hash(hsize));
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <sys/hashtable_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
