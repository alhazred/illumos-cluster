//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)vm_tmpl_insts.cc	1.2	08/05/20 SMI"

#include <h/version_manager.h>
template class _NormalSeq_<version_manager::ucc>;
template class _FixedSeq_<version_manager::vp_version_t>;
template class _NormalSeq_<version_manager::vp_info_t>;

template class _Ix_InterfaceSeq_ < version_manager::vm_lca,
    _A_field_ < version_manager::vm_lca, version_manager::vm_lca * >,
    version_manager::vm_lca * >;

template class proxy<version_manager::vm_admin_stub>;
template class proxy<version_manager::inter_vm_stub>;
template class proxy<version_manager::vm_lca_stub>;
template class proxy<version_manager::upgrade_callback_stub>;
template class proxy<version_manager::custom_vp_query_stub>;

#include <orb/object/adapter.h>

template class Anchor<version_manager::inter_vm_stub>;

template class McServerof<version_manager::custom_vp_query>;
template class McServerof<version_manager::vm_admin>;
template class McServerof<version_manager::vm_lca>;
template class McServerof<version_manager::upgrade_callback>;

template class McNoref<version_manager::inter_vm>;
