/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_COMM_H
#define	_VM_COMM_H

#pragma ident	"@(#)vm_comm.h	1.27	08/05/20 SMI"

// Abbreviations:
// btstrp - bootstrap
// no - node
// np - node pair
// cl - cluster
// coord - coordinator
// rv - running version

#include <h/version_manager.h>
#include <h/cmm.h>
#include <vm/vp_error.h>
#include <vm/versioned_protocol.h>
#include <vm/vm_coord.h>
#include <sys/types.h>
#include <sys/os.h>
#include <sys/clconf.h>
#include <orb/object/adapter.h>
#include <orb/member/Node.h>
#include <orb/member/callback_membership.h>
#include <orb/buffers/marshalstream.h>
#include <sys/uadmin.h>

// Constants
const long VM_SHUTDOWN_WAIT = 15 * 1000000;	// vm_comm::force_node_shutdown

// forward declaration
class vm_admin_impl;
class vm_coord;
class vm_stream;

extern "C" void vm_comm_handle_messages(recstream *r);

class vm_comm : public McNoref <version_manager::inter_vm> {
public:
	static int	initialize();
	int		initialize_int();
	static void	shutdown();

	// send_cl_to_coord accepts an encoded vm_stream and related
	// info and forwards it to the coordinator
	bool		send_cl_to_coord(sol::nodeid_t, cmm::seqnum_t,
			    const version_manager::vm_stream_container_t &,
			    Environment &);

	// send_cl_rv is used by the vm_comm on the coordinator node to forward
	// the running versions of cl vps to nodes that need them.
	void		send_cl_rv(cmm::seqnum_t, membership_bitmask_t,
			    const version_manager::vm_stream_container_t &,
			    Environment &);

	// Called from the vm_admin_impl to check on the current incn of
	// a remote node
	bool		current_pairwise_incn(sol::nodeid_t,
			    sol::incarnation_num &);

	// Called if a step is canceled
	void		cmm_step_cancel(cmm::seqnum_t);

	// Called from first step.  Retrieves the vm_stream from vm_admin_impl,
	// sends messages to all other nodes, waits to receive messages, and
	// and then calls into the coordinator to calculate running versions.
	void		calculate_btstrp_cl(callback_membership *,
			    cmm::seqnum_t, os::systime *);

	// Called from 2nd step, sends the list of running versions calculated
	// in the previous step into the vm_admin_impl.
	void		commit_btstrp_cl();

	// Called after disqualification from the cmm steps to generate the
	// cl message and allocate a coordinator if necessary
	void		generate_cl_message(bool coord_local);

	// Called from steps to send the info to the coordinator if necessary
	void		send_cl_message(callback_membership *, cmm::seqnum_t);

	// Called from steps to run coordinator if necessary
	void		calculate_cl(callback_membership *, cmm::seqnum_t,
			    os::systime *);

	// Called from steps to publish the cl running versions
	void 		commit_cl_rv();

	// Initiate the switch of btstrp_cl rvs and the calculation of
	// cl upgrade versions (uvs).
	cmm::seqnum_t	start_upgrade_commit();

	// Finish an upgrade commit request
	void		finish_upgrade_commit();

	//
	// Returns true if an upgrade commit is needed.  A side effect of this
	// is a CMM reconfiguration so all nodes will have up-to-date
	// information.
	//
	bool		is_upgrade_needed(version_manager::vp_state &,
			    nodeset &);

	//
	// Called during the CMM reconfiguration to signal the vm_comm that it
	// is now safe to proceed with calling is_upgrade_needed().
	//
	void		signal_vm_comm(cmm::seqnum_t);

	void		lock_vps_for_upgrade_commit();
	void		unlock_vps_after_upgrade_commit();

	// Message handler for VM_MSG message type
	static void 	handle_messages(recstream *recp);

	// Called by transports early in path formation handshake
	// Unpacks vers_str generating list of names and version
	// ranges, which it passes to bootstrap coordinator.  Returns
	// whatever boolean the bootstrap coordinator returns.
	bool		compatible_btstrp_np(nodeid_t remote_node,
			    incarnation_num remote_incn,
			    size_t vers_strlen, char *,
			    bool remote_rejected);

	// Other functions for the transport
	char		*get_btstrp_np_string(char *, size_t &);
	static size_t	max_btstrp_np_string();

	// Accessor for syslog messages.
	os::sc_syslog_msg &get_msg();

	// External accessors for the different objects
	static vm_comm		&the();
	static vm_coord		&the_vm_coord();
	static vm_admin_impl	&the_vm_admin_impl();

	void 		lock();
	void 		unlock();
	int 		lock_held();

	bool		are_vps_locked();

	//
	// Called during CMM reconfiguration to determine if a cv.broadcast()
	// is needed.
	//
	bool		is_signal_needed();

	cmm::seqnum_t	get_upgrade_seq();

	nodeid_t get_coord_nodeid();

	void		wait_until_vps_are_unlocked();

#ifdef DEBUGGER_PRINT
	int mdb_dump_vps(const char*);
	vm_stream *mdb_get_btstrp_np_message();
	vm_stream *mdb_get_btstrp_cl_message();
	vm_stream *mdb_get_cl_message();
	vm_stream *mdb_get_cl_rv_message();
	vm_stream *mdb_get_btstrp_cl_rv_message();
#endif

private:
	os::mutex_t	*get_lock();

	// Returns true if the cmm has canceled the sequence specified in the
	// argument
	bool		canceled(cmm::seqnum_t);

	// Called from calculate_btstrp_cl to send the bootstrap cluster
	// info to the relevant nodes
	void		send_btstrp_cl_message(callback_membership *,
			    cmm::seqnum_t, os::systime *, bool);

	// Force a cmm reconfiguration
	void		force_return_transition();
	// Force this node out of the cluster
	void		force_node_shutdown();

	// non-static implementation of handle_messages for VM_MSG type
	void		handle_messages_int(recstream *recp);

	typedef struct bootstrap_header_t {
		cmm::seqnum_t		current_seqnum;
		cmm::seqnum_t		upgrade_seqnum;
		uint32_t		qvotes;
		uint32_t		length;
	} bootstrap_header;

	// The biggest btstrp_np message we can send
	static const size_t		max_btstrp_np_strlen;

	// pointer to the comm object for retrieval
	static vm_comm			*the_vm_comm;

	// True if the coordinator has told us we need to leave the cluster
	bool				shutdown_requested;

	// The stream specifying our VP_BTSTRP_NODEPAIR vps, returned from
	// vm_admin_impl::initialize_int and traded at the transport level.
	vm_stream			*btstrp_np_message;
	// True if the coordinator have determined the remote node can
	// communicate with us.
	bool				btstrp_np_ok[NODEID_MAX+1];
	// The incn of the node the maps to btstrp_np_ok (the incn
	// we last heard from)
	cmm::membership_t		btstrp_np_mbrshp;

	// The stream specifying our VP_BTSTRP_CLUSTER vps, returned from
	// vm_admin_impl::new_btstrp_cl_message and traded in the 1st step
	vm_stream			*btstrp_cl_message;

	// Used to record which nodes we've sent the full btstrp_cl_message to.
	callback_membership 		*btstrp_cl_mbrshp;
	// The sequence number in which we received the most recent
	// btstrp_cl_message from the given node.
	cmm::seqnum_t			btstrp_cl_seqs[NODEID_MAX+1];
	// The number of btstrp_cl_messages we are still expecting from
	// other nodes
	uint32_t			btstrp_cl_needed;

	// The stream specifying the running versions for VP_BTSTRP_CLUSTER
	// vps, returned from the coordinator in the first step.
	vm_stream			*btstrp_cl_rv_message;

	// The stream specifying our VP_CLUSTER vps, returned from
	// vm_admin_impl::new_cl_message and sent to the coordinator
	vm_stream			*cl_message;
	// True if cl_message has been sent to the current coordinator
	bool				cl_sent_to_coord;

	// The nodeid and incarnation number of the coordinator
	ID_node				coord_node;

	// The stream specifying the running versions for VP_CLUSTER
	// vps, returned from the coordinator
	vm_stream			*cl_rv_message;

	// The current cmm reconfiguration sequence number
	cmm::seqnum_t			current_seq;
	// The sequence number of the most recent canceled sequence
	cmm::seqnum_t			canceled_seq;
	// The sequence number of the most recent upgrade commit
	cmm::seqnum_t			upgrade_seq;
	// The sequence number of the last time is_upgrade_needed was called.
	cmm::seqnum_t			last_iun_seqnum;
	//
	// Flag set to true when is_upgrade_needed() is called.  It is used
	// to determine when the CMM reconfiguration should broadcast against
	// a condition variable.
	//
	bool				signal_needed;

	// true if a node has requested that we attempt an upgrade
	bool				try_upgrade;

	// Pointers to the member objects
	vm_coord			*the_coord;
	vm_admin_impl			*the_admin_impl;

	// The lock and condition variable.  The lock protects the entire
	// version_manager subsystem.  The lock in the quorum algorithm is
	// grabbed while holding this lock.
	os::mutex_t			vm_comm_lock;
	os::condvar_t			vm_cv;

	// This is used to block callers during upgrade commit
	os::condvar_t			vp_lock_cv;

	//
	// True if vps should not be changed. We don't want to stop the
	// CMM steps; instead, we are careful to not let anyone see the
	// changes that are made in the CMM steps.
	// This is used for CMM reconfigurations while upgrade commit is
	// in progress.
	//
	bool				vps_locked;

	// For syslog messages.
	os::sc_syslog_msg msg;

	// The constructor and destructor
	vm_comm(const char *);
	~vm_comm();

	vm_comm(const vm_comm &);
	vm_comm &operator = (vm_comm &);

#ifndef LIBVM
	// for message logging
	void log_diagnostic(vm_stream*);
#endif
};

#ifndef	NOINLINES
#include <vm/vm_comm_in.h>
#endif	/* NOINLINES */

#endif	/* _VM_COMM_H */
