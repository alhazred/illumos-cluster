/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VERSION_PROT_OBJ_H
#define	_VERSION_PROT_OBJ_H

#pragma ident	"@(#)version_prot_obj.h	1.10	08/05/20 SMI"

#include <sys/list_def.h>
#include <sys/refcnt.h>

class version_prot_obj;
class versioned_protocol;
class coord_vp;

//
// Enumerated types
//
enum vp_type {
	VP_UNINITIALIZED = 0,
	VP_BTSTRP_NODE = 1,		// Never goes off-node
	VP_BTSTRP_NODEPAIR = 2,
	VP_BTSTRP_CLUSTER = 3,
	VP_NODE = 4,			// Not yet used
	VP_NODEPAIR = 5,		// Not yet used
	VP_CLUSTER = 6,
	VP_TYPE_ARRAY_SIZE = 7
};

// A vp_rev_depend is just a pointer to a versioned_protocol that can be put
// into a list.  It is used to create a list of reverse dependencies in
// the versioned_protocol class.

class vp_rev_depend :
	public _SList::ListElem,
	public refcnt {
public:
	// Constructor
	vp_rev_depend(version_prot_obj *);

	// Destructor
	virtual ~vp_rev_depend();

	// Return the appropriate pointer type
	versioned_protocol *get_vpp();
	coord_vp *get_cvpp();
	version_prot_obj *get_vpop();

private:
	virtual void refcnt_unref();

	version_prot_obj *vpop;		// The undirected graph edge is simply
					// a pointer to another versioned
					// protocol object.

	vp_rev_depend(const vp_rev_depend &);
	vp_rev_depend &operator = (vp_rev_depend &);
};

//
// a version_prot_obj is an abstract object that versioned_protocol and
// coord_vp both inherit from and contains some common code and interfaces.
// It also exists so that vp_rev_depend, vp_rhs_version, and vp_set_key
// objects can all point to a common object and therefore be reused in both
// vm_admin_impl and vm_coord.
//

class version_prot_obj : public refcnt {
public:
	version_prot_obj();
	virtual ~version_prot_obj();

	// get_vpp and get_cvpp exist as a sanity check, allowing the caller
	// to ASSERT at the time a version_prot_obj is used that it is either
	// a versioned_protocol object or coord_vp object, and to retrieve
	// a pointer cast to the correct type.
	virtual versioned_protocol *get_vpp() = 0;
	virtual coord_vp *get_cvpp() = 0;

	// Get the stored name of the object.
	virtual char *get_name() = 0;
	virtual vp_type get_vp_type() = 0;

	// These add or remove a reverse dependency
	void add_vp_rev_depend(version_prot_obj *);
	void rm_vp_rev_depend(version_prot_obj *);

	// Retrieve or set the group id, which is used during the connected
	// components algorithms.
	int16_t get_group_id();
	void set_group_id(int16_t);

	// Retrieve or set the ordinal, which is also used during the
	// connected components algorithm.
	uint16_t get_ordinal();
	void set_ordinal(uint16_t);

protected:

	// Edges pointing backwards, the reverse of vp_rhs_versions
	IntrList <vp_rev_depend, _SList> vp_rev_dependlist;

private:
	int16_t group_id;		// Numerical identifier to indicate
					// which connected component this
					// version_prot_obj belongs to.
					// If the identifer is zero, then
					// this version_prot_obj has no
					// dependencies on or to it.
	uint16_t ordinal;		// Alphabetical ordinal of this
					// version_prot_obj in the
					// same connected subgroup from
					// 0 to N-1, where N = number of
					// vp's in the same connected subgroup.

	version_prot_obj(const version_prot_obj &);
	version_prot_obj &operator = (version_prot_obj &);
};

#ifndef	NOINLINES
#include <vm/version_prot_obj_in.h>
#endif	// NOINLINES

#endif	/* _VERSION_PROT_OBJ_H */
