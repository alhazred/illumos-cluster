/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_DEPENDENCY_IN_H
#define	_VP_DEPENDENCY_IN_H

#pragma ident	"@(#)vp_dependency_in.h	1.12	08/05/20 SMI"

// Constructor
//
inline
vp_dependency::vp_dependency() : _SList::ListElem(this),
	bad(false)
{
}

//
// Destructor
//
inline
vp_dependency::~vp_dependency()
{
	if (!rhs_list.empty()) {
		rhs_list.atfirst();
		rhs_list.dispose();
	}
}

inline bool
vp_dependency::is_bad()
{
	return (bad);
}

inline void
vp_dependency::mark_bad()
{
	bad = true;
}

inline void
vp_dependency::clear_bad()
{
	bad = false;
}

inline void
vp_dependency::swap_into_lhs(version_range & lhs)
{
	lhs_versions.swap(lhs);
}

inline version_range &
vp_dependency::get_lhs()
{
	return (lhs_versions);
}

inline IntrList <vp_rhs_version, _SList> &
vp_dependency::get_rhs_list()
{
	return (rhs_list);
}

#endif	/* _VP_DEPENDENCY_IN_H */
