/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _COORD_VP_IN_H
#define	_COORD_VP_IN_H

#pragma ident	"@(#)coord_vp_in.h	1.12	08/05/20 SMI"

inline void
coord_vp::node_entry::get_consistent_versions(version_range &vr)
{
	vr.reinit(consistent_versions);
}

inline IntrList <vp_dependency, _SList> &
coord_vp::node_entry::get_vp_dependency_list()
{
	return (vp_dependlist);
}

inline vp_type
coord_vp::get_vp_type()
{
	return (the_type);
}

inline _SList::ListElem *
coord_vp::get_bfs_elem()
{
	return (&bfs_elem);
}

inline bool
coord_vp::is_empty(nodeid_t n)
{
	return (node_entries[n] == NULL);
}

inline void
coord_vp::set_vp_type(vp_type t)
{
	the_type = t;
}

inline IntrList <vp_dependency, _SList> &
coord_vp::get_vp_dependency_list(nodeid_t n)
{
	ASSERT(node_entries[n] != NULL);
	return (node_entries[n]->get_vp_dependency_list());
}

inline void
coord_vp::get_consistent_versions(nodeid_t n, version_range &vr)
{
	if (node_entries[n] != NULL) {
		node_entries[n]->get_consistent_versions(vr);
	} else {
		vr.empty();
		vr.insert_triad(1, 0, 0);
	}
}

inline void
coord_vp::get_uv(version_manager::vp_version_t &v)
{
	v.major_num = uv.major_num;
	v.minor_num = uv.minor_num;
}

inline void
coord_vp::set_uv(version_manager::vp_version_t &v)
{
	uv.major_num = v.major_num;
	uv.minor_num = v.minor_num;
}

inline void
coord_vp::get_rv(version_manager::vp_version_t &v)
{
	v.major_num = rv.major_num;
	v.minor_num = rv.minor_num;
}

inline void
coord_vp::set_rv(version_manager::vp_version_t &v)
{
	rv.major_num = v.major_num;
	rv.minor_num = v.minor_num;
}

inline void
coord_vp::get_highest_version(version_manager::vp_version_t &v)
{
	v.major_num = highest_uv.major_num;
	v.minor_num = highest_uv.minor_num;
}

inline void
coord_vp::set_highest_version(version_manager::vp_version_t &v)
{
	highest_uv.major_num = v.major_num;
	highest_uv.minor_num = v.minor_num;
}

inline vm_coord&
coord_vp::get_vm_coord()
{
	return (*this_vm_coord);
}
#endif	/* _COORD_VP_IN_H */
