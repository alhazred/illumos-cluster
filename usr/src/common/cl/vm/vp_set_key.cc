//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)vp_set_key.cc	1.9	08/05/20 SMI"

#include <vm/vp_set_key.h>
#include <vm/coord_vp.h>

void
vp_set_key::group_entry::compute_scores()
{
	ASSERT(scores == NULL);

	if (all_versions.get_count() == 0)
		return;

	scores = new uint32_t[all_versions.get_count()];
	scores[0] = 1;

	for (uint16_t i = 1; i < all_versions.get_count(); i++) {
		vp_triad &T = all_versions.get_triad(i);
		vp_triad &Tlast = all_versions.get_triad(i - 1);
		scores[i] = scores[i - 1] +
		    (Tlast.get_minor_max() - Tlast.get_minor_min()) + 1;
		if (T.get_major() != Tlast.get_major())
			scores[i] += 3;
	}
}

void
vp_set_key::node_entry::score_and_sort(vp_set_key *vpsk)
{
	IntrList<vp_set, _SList> tmp_list;
	vp_set *vpsp;

	if (scored_and_sorted)
		return;

	while ((vpsp = solution_universe.reapfirst()) != NULL) {
		vpsk->calculate_score(*vpsp);
		vpsp->add_in_sort_order(tmp_list);
	}
	ASSERT(solution_universe.empty());

	solution_universe.concat(tmp_list);
	scored_and_sorted = true;
}

void
vp_set_key::node_entry::print_su()
{
	vp_set *vpsp;

	VM_DBGC(("(%sscored and sorted)\n", scored_and_sorted ? "" : "not "));

	solution_universe.atfirst();
	while ((vpsp = solution_universe.get_current()) != NULL) {
		vpsp->dbprint();
		VM_DBGC(("\n"));
		solution_universe.advance();
	}
}

//
// try_nodeset
// Check the versions supported for the dependency group on the nodes in the
// nodeset that is passed in. If force is set to false, then if the nodeset is
// a subset of a previously checked nodeset just return the existing values.
// If use_uv is set to true, calculate upgrade versions and set uv for each of
// the coord_vp's in the dependency group. If calc_only is set to false,
// calculate and set new running versions for the coord_vp's. Otherwise, if
// calc_only is set to true, set the highest_version of the coord_vp's, which
// is only used for the is_upgrade_needed functionality.
//
bool
vp_set_key::try_nodeset(const nodeset &ns, bool use_uv, bool calc_only,
    bool force)
{
	vp_set *vps, *vps2, *new_vps;
	IntrList<vp_set, _SList> multiverse;

	ASSERT(!ns.is_empty());

	// Only look at last_calc if this is not an is_upgrade_needed call.
	if (!calc_only) {
		if (force) {
			if (last_calc.equal(ns))
				return (true);
		} else {
			if (last_calc.superset(ns))
				return (true);
		}

		last_calc.empty();
	}

	nodeid_t n = ns.next_node(NODEID_UNKNOWN);

	ASSERT(n != NODEID_UNKNOWN);

	// Copy the first list into the multiverse
	node_entries[n].score_and_sort(this);

	dbprint();

	IntrList<vp_set, _SList> &su = node_entries[n].get_solution_universe();

	su.atfirst();
	while ((vps = su.get_current()) != NULL) {
		new_vps = new vp_set(*vps);
		new_vps->assign_node(n);
		multiverse.append(new_vps);
		su.advance();
	}

	VM_DBGC(("Starting calculation:\n"));

	new_vps = new vp_set(_num_elems);

	while ((vps = multiverse.reapfirst()) != NULL) {
		n = ns.next_node(vps->get_node());
		vps->dbprint();
		VM_DBGC(("\n"));
		if (n == NODEID_UNKNOWN)
			break;
		IntrList<vp_set, _SList> &su_n =
		    node_entries[n].get_solution_universe();
		su_n.atfirst();
		while ((vps2 = su_n.get_current()) != NULL) {
			vps->intersection(*vps2, *new_vps);
			if (new_vps->is_valid()) {
				new_vps->assign_node(n);
				calculate_score(*new_vps);
				new_vps->add_in_sort_order(multiverse);
				//
				// lint warns about memory leak.
				// add_in_sort_order consumes new_vps
				// so there is no leak here. The
				// warning shows up when building
				// libvm.
				//
				new_vps = new vp_set(_num_elems); //lint !e423
			}
			su_n.advance();
		}
		delete vps;
	}

	delete new_vps;
	version_manager::vp_version_t vpv;

	if (vps != NULL) {
		for (uint16_t i = 0; i < _num_elems; i++) {
			vps->get_vr(i).highest_version(vpv.major_num,
			    vpv.minor_num);
			if (use_uv)
				get_cvpp(i)->set_uv(vpv);
			else if (!calc_only)
				get_cvpp(i)->set_rv(vpv);

			// Always set the highest calculated version.
			get_cvpp(i)->set_highest_version(vpv);
		}
		if (!calc_only) {
			last_calc.set(ns);
		}
		multiverse.dispose();
		return (true);
	} else {
		ASSERT(multiverse.empty());
		return (false);
	}
}

//
// is_upgrade_needed
// Called from vm_coord::is_upgrade needed. This calls try_nodeset to calculate
// the highest supported versions for the dependency group. Then check to see
// if the highest supported version is the current running version.
//
bool
vp_set_key::is_upgrade_needed(version_manager::vp_state &s, const nodeset &ns,
    nodeset &minimal_nodes)
{
	bool nodeset_found = true;
	version_manager::vp_version_t	current, possible;
	bool		upgrade_needed = false;

	// Get the current highest version supported.
	nodeset_found = try_nodeset(ns, false, true, true);

	//
	// We must have been able to find at least one valid nodeset.
	// Otherwise, this cluster could not have formed.
	//
	CL_PANIC(nodeset_found);

	//
	// For each coord_vp in the dependency group, check whether the current
	// running version is less than the highest calculated version.
	//
	s = version_manager::ALL_VERSIONS_MATCH;
	minimal_nodes.empty();
	for (uint16_t i = 0; i < _num_elems; i++) {
		get_cvpp(i)->get_rv(current);
		get_cvpp(i)->get_highest_version(possible);

		version_range vr;
		for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
			if (!ns.contains(n))
				continue;

			get_cvpp(i)->calc_potential_versions(vr, n, ns);

			uint16_t	node_major_num;
			uint16_t	node_minor_num;
			vr.highest_version(node_major_num, node_minor_num);
			if ((possible.major_num < node_major_num) ||
			    ((possible.major_num == node_major_num) &&
			    (possible.minor_num < node_minor_num))) {
				//
				// This node could support a higher version than
				// the highest cluster-wide version. Set the
				// vp_state to reflect that there are mismatched
				// versions.
				//
				s = version_manager::MISMATCHED_VERSIONS;
			} else if ((possible.major_num == node_major_num) &&
			    (possible.minor_num == node_minor_num)) {
				//
				// This node's vp spec file is at the minimum
				// for the highest cluster-wide version. If
				// after we look at all nodes, the vp's don't
				// match, then this node is one of the nodes
				// preventing further upgrades.
				//
				minimal_nodes.add_node(n);
			} else {
				//
				// Somehow a node can't support the highest
				// version that try_nodeset just told us all
				// nodes could support.
				//
				ASSERT(0);
			}
		}

		if ((current.major_num < possible.major_num) ||
		    ((current.major_num == possible.major_num) &&
		    (current.minor_num < possible.minor_num))) {
			upgrade_needed = true;
		}
	}

	if (s == version_manager::MISMATCHED_VERSIONS) {
		VM_DBG(("vp_set_key::is_upgrade_needed returns "
		    "MISMATCHED_VERSIONS\n"));
	} else {
		VM_DBG(("vp_set_key::is_upgrade_needed returns "
		    "ALL_VERSIONS_MATCH\n"));
	}
	return (upgrade_needed);
}

void
vp_set_key::dbprint()
{
	ASSERT(_num_elems != 0);

	VM_DBG(("cl_set_key %d:\nElements:\n", get_vpop(0)->get_group_id()));

	VM_DBGC(("ord #\tname (all defined versions)"));

	version_manager::vp_version_t	current;
	for (uint16_t i = 0; i < _num_elems; i++) {
		get_cvpp(i)->get_rv(current);
		VM_DBGC(("\n%u\t%s: (%u,%u): ", i, get_vpop(i)->get_name(),
		    current.major_num, current.minor_num));
		group_entries[i].print_all_versions();
	}

	VM_DBGC(("\n"));

	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (node_entries[n].get_solution_universe().empty())
			continue;

		node_entries[n].print_su();
	}
}
