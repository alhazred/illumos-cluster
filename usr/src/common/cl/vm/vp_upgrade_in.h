/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_UPGRADE_IN_H
#define	_VP_UPGRADE_IN_H

#pragma ident	"@(#)vp_upgrade_in.h	1.9	08/05/20 SMI"

//
// Constructor - Create a empty vp_upgrade ojbect.
//
inline
vp_upgrade::vp_upgrade() : _SList::ListElem(this)
{
}

//
// Constructor - Create an vp_upgrade ojbect to the specified versioned protocol
//
inline
vp_upgrade::vp_upgrade(version_range & lhs, version_range & rhs) :
	_SList::ListElem(this),
	left(lhs),
	right(rhs)
{
}

//
// Destructor
//
inline
vp_upgrade::~vp_upgrade()
{
}

//
// Return the current lefthand side of this upgrade
//
inline version_range &
vp_upgrade::get_lhs()
{
	return (left);
}

//
// Return the current righthand side of this upgrade
//
inline version_range &
vp_upgrade::get_rhs()
{
	return (right);
}

#endif	/* _VP_UPGRADE_IN_H */
