/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VERSION_RANGE_IN_H
#define	_VERSION_RANGE_IN_H

#pragma ident	"@(#)version_range_in.h	1.14	08/05/20 SMI"

// Constructor - Create an empty version_range instance
inline
version_range::version_range() :
	count(0),
	size(0),
	triad_array(NULL)
{
}

inline
version_range::version_range(uint16_t s) :
	count(0),
	size(s)
{
	triad_array = new vp_triad[s];
	ASSERT(triad_array);
}

// This constructor assigns the description attribute
inline
version_range::version_range(uint16_t a, uint16_t b, uint16_t c, char *d) :
	count(1),
	size(3)
{
	triad_array = new vp_triad[3];
	ASSERT(triad_array);
	triad_array[0].assign(a, b, c);
	triad_array[0].assign_description(d);
	ASSERT(validate());
}

inline
version_range::version_range(const version_range &v) :
	count(v.count),
	size(v.count+2)
{
	triad_array = new vp_triad[size];
	ASSERT(triad_array);
	bcopy(v.triad_array, triad_array, vp_triad_size * count);
	ASSERT(validate());
}

// Destructor
inline
version_range::~version_range()
{
	// Free any allocated memory
	delete_versions();
	// And rezero the values
	size = count = 0;
}

// Is the protocol the empty set?
// A version_range object is defined to be empty if its array of triads
// is empty.
inline bool
version_range::is_empty()
{
	return (count == 0);
}

//
// Initialize the version range object as specified by another
// version_range object.  It's up to the caller to make sure that
// valid arguments are passed it.
//
inline void
version_range::reinit(const version_range &B)
{
	reinit(B.count, B.triad_array);
}

inline void
version_range::swap(version_range &B)
{
	uint16_t t;
	vp_triad *a;

	t = B.size;  B.size = size;   size = t;
	t = B.count; B.count = count; count = t;

	a = B.triad_array; B.triad_array = triad_array; triad_array = a;
}

inline void
version_range::empty()
{
	delete_versions();
	count = 0;
	size = 0;
}

//
// Returns the number of valid triads in the sorted array of triads
//
inline uint16_t
version_range::get_count() const
{
	return (count);
}

inline bool
version_range::contains(const version_manager::vp_version_t &v)
{
	return (index_of(v) != -1);
}

inline vp_triad &
version_range::get_triad(uint16_t id)
{
	ASSERT(id < count);

	return (triad_array[id]);
}

inline void
version_range::subtract_equals(const version_range &B)
{
	version_range tmp_vr;

	subtraction(B, tmp_vr);
	swap(tmp_vr);
}

inline void
version_range::intersect_equals(const version_range &B)
{
	version_range tmp_vr;

	intersection(B, tmp_vr);
	swap(tmp_vr);
}

inline void
version_range::lowest_version(uint16_t &maj, uint16_t &min)
{
	if (count) {
		maj = triad_array[0].get_major();
		min = triad_array[0].get_minor_min();
	} else {
		maj = min = 0;
	}
}

inline void
version_range::highest_version(uint16_t &maj, uint16_t &min)
{
	if (count) {
		maj = triad_array[count-1].get_major();
		min = triad_array[count-1].get_minor_max();
	} else {
		maj = min = 0;
	}
}

// Less than operator
inline bool
version_range::lt(const version_range &B)
{
	// We need to have valid triads
	ASSERT(count != 0 && B.count != 0);
	// Okay compare the largest triad of 'A' to the smallest triad in 'B'
	return (triad_array[count - 1].lt(B.triad_array[0]));
}

// Greater than operator
inline bool
version_range::gt(const version_range &B)
{
	// We need to have valid triads
	ASSERT(count != 0 && B.count != 0);
	// Okay compare the smallest triad of 'A' to the largest triad in 'B'
	return (triad_array[0].gt(B.triad_array[B.count - 1]));
}

inline bool
version_range::insert_triad(uint16_t maj, uint16_t min_min, uint16_t min_max)
{
	vp_triad t(maj, min_min, min_max);
	return (insert_triad(t));
}

// This method assigns the description attribute
inline bool
version_range::insert_triad(uint16_t maj, uint16_t min_min,
    uint16_t min_max, char *desc)
{
	vp_triad t(maj, min_min, min_max);
	t.assign_description(desc);
	return (insert_triad(t));
}

inline void
version_range::insert_version(const version_manager::vp_version_t &v)
{
	vp_triad t(v.major_num, v.minor_num, v.minor_num);
	(void) insert_triad(t);
}

inline bool
version_range::merge_equals(const version_range &B)
{
	version_range tmp_vr;
	bool retval;

	retval = merge(B, tmp_vr);
	swap(tmp_vr);
	return (retval);
}

//
// Private methods
//

//
// Delete the sorted array of triads if necessary.
//
inline void
version_range::delete_versions()
{
	if (triad_array != NULL) {
		delete [] triad_array;
		triad_array = (vp_triad *)NULL;
	}
}

#endif	/* _VERSION_RANGE_IN_H */
