/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VERSION_RANGE_H
#define	_VERSION_RANGE_H

#pragma ident	"@(#)version_range.h	1.14	08/05/20 SMI"

#include <sys/os.h>
#include <h/version_manager.h>
#include <vm/vp_triad.h>

//
// Version Manager version_range Class Definition
//
// A version_range object holds an array of triad objects.
// This array is dynamically allocated and can increase or decrease throughout
// the life of the version_range object.  For efficiency, if the array must be
// increased, then the array is overallocated, leaving extra array elements in
// case they are needed in the future.  Because of this, we also keep track
// of the allocated array size and how full it is.
//
// Each triad should be strictly less than and not adjacent to the next highest
// triad.  The algorithms assume these properties for performance reasons (it
// allows all of the low-level algorithms to be O(n).  The validate method
// checks this property and is run in a number of ASSERTs at the end of code
// that manipulates the triad array.
//
class version_range {

// Give vm_stream access to the triad_array
friend class vm_stream;
public:
	version_range();
	version_range(uint16_t size);
	version_range(uint16_t, uint16_t, uint16_t, char*);
	version_range(const version_range &);
	~version_range();

	// A version_range is empty if there is not a single triad in the
	// sorted array of triads.
	bool is_empty();

	// Initialize the version_range as specified by a version_range object.
	void reinit(const version_range &);

	// Initialize the version_range as specified by an array of triads.
	void reinit(uint16_t s, vp_triad *t);

	// Assign the triad array of 'this' to the argument and vice versa.
	void swap(version_range &);

	// Free up the existing triad array
	void empty();

	// Return the current size of version_range object.
	uint16_t get_count() const;

	// Returns true if v is defined in the range
	bool contains(const version_manager::vp_version_t &v);

	// Returns the index of the triad in which v is defined, or -1 if
	// the version is not in the array.
	int index_of(const version_manager::vp_version_t &v);

	vp_triad &get_triad(uint16_t);

	// Subtract one version_range from another.  The difference can be
	// expressed as another version_range object.
	void subtraction(const version_range &B, version_range &result);

	// Assign the result of subtraction to 'this'
	void subtract_equals(const version_range &B);

	// Intersect one version_range object with another.  The intersection
	// can be expressed as another version_range object.
	void intersection(const version_range &B, version_range &result);

	// Assign the result of intersection to 'this'
	void intersect_equals(const version_range &B);

	// Return the lowest version in the first triad in the triad array.
	void lowest_version(uint16_t &, uint16_t &);

	// Return the highest version in the last valid triad in the triad
	// array.
	void highest_version(uint16_t &, uint16_t &);

	// Less than operator
	bool lt(const version_range &B);

	// Greater than operator
	bool gt(const version_range &B);

	// Insert this triad into the sorted array of triads.  Returns true
	// if the triad overlaps with the existing array, false otherwise.
	bool insert_triad(vp_triad t);
	bool insert_triad(uint16_t, uint16_t, uint16_t);
	bool insert_triad(uint16_t, uint16_t, uint16_t, char*);

	// insert the version into the sorted array of triads
	void insert_version(const version_manager::vp_version_t &);

	// Merge two version range objects' array of triads.
	// Returns true if there are overlapping triads, false otherwise
	bool merge(const version_range &B, version_range &result);

	// Assign the result of merge to 'this'
	bool merge_equals(const version_range &B);

	// Print the version_range into the version manager debug buffer,
	// if any
	void dbprint();

private:
	// Validate the triads in the sorted array of triads.
	// No two triads should represent the same major/minor pair.
	bool validate();

	// Grow the sorted array of triads by 'growby' elements.
	void grow(uint16_t growby = 2);

	// Safely free the sorted array of triads, if it exists.
	void delete_versions();

	// Number of allocated slots in the sorted array of triads.
	uint16_t size;

	// Number of valid triads held in the sorted array of triads.
	uint16_t count;

	// List of triads implemented as an array of 'size' elements.
	vp_triad *triad_array;

	version_range &operator = (version_range &);
};

#ifndef	NOINLINES
#include <vm/version_range_in.h>
#endif	// NOINLINES

#endif	/* _VERSION_RANGE_H */
