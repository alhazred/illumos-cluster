/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vp_error.cc	1.3	08/05/20 SMI"

#include <vm/vp_error.h>

const char*
vp_error_to_string(vp_error_t vperr)
{
	const char *str;
	switch (vperr) {
	case VP_SUCCESS:
		str = "Success.";
		break;
	case VP_SYNTAX:
		str = "An unknown line in one of the versioned protocol files "
			"has been encountered.";
		break;
	case VP_DUPMAJMIN:
		str = "The same major and minor pair of one of the versioned "
			"protocol has already been encountered.";
		break;
	case VP_NOTCUSTOM:
		str = "A custom callback name is specified but the "
			"\"Custom\" attribute is not set.";
		break;
	case VP_NOCUSTOMCB:
		str = "A versioned protocol file attribute is set to "
			"\"Custom\" but the custom callback name is "
			"not specified.";
		break;
	case VP_NOTBOOTSTRAP:
		str = "The attribute \"Bootstrap\" must be set for the "
			"versioned protocol files of mode \"Nodepair\" "
			"or \"Node\".";
		break;
	case VP_NOVERSIONS:
		str = "One versioned protocol file did not contain "
			"any valid major/minor numbers. For a versioned "
			"protocol file with an attribute set to \"Custom\", "
			"all potential versions might have been discarded "
			"by the custom callback for this versioned protocol.";
		break;
	case VP_CANNOTOPENDIR:
		str = "The directory containing the versioned protocol files "
			"cannot be opened.";
		break;
	case VP_INVALIDUPGRADE:
		str = "An upgrade line is semantically incorrect "
			"because the lefthand side is not strictly "
			"less than the righthand side of the upgrade. "
			"This error catches the case where the line "
			"tries to upgrade to an earlier version "
			"(e.g. upgrade from 3.0 to 2.0).";
		break;
	case VP_DUPUPGRADE:
		str = "The lefthand side of one of the upgrade line has "
			"already been seen in another upgrade line.";
		break;
	case VP_INVALIDDEP:
		str = "One of the versioned_protocol has a dependency "
			"on a different versioned protocol file that "
			"has an inappropriate type.";
		break;
	case VP_CANNOTOPENFILE:
		str = "A versioned protocol file is found but cannot "
			"be opened.";
		break;
	case VP_DUPNAME:
		str = "Two versioned protocol files with the same name "
			"were found.";
		break;
	case VP_NOVPFILES:
		str = "Not a single versioned protocol was found.";
		break;
	case VP_INCONSISTGRP:
		str = "A connected subgroup of versioned_protocols "
			"did not have a self-consistent set of versions.";
		break;
	case VP_NOCUSTOMQ:
		str = "The query to the custom interface of a "
			"versioned protocol failed.";
		break;
	case VP_TRDATTOOLONG:
		str = "The size of the bootstrap node pair data to be sent "
			"to the other nodes exceeds the size of the "
			"transport's capability.";
		break;
	case VP_UNEXPECTED:
		str = "Unexpected error.";
		break;
	default:
		str = "Unrecognized error number.";
		break;
	}
	return (str);
}
