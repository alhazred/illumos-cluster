/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_LCA_IMPL_IN_H
#define	_VM_LCA_IMPL_IN_H

#pragma ident	"@(#)vm_lca_impl_in.h	1.8	08/05/20 SMI"

//
// vm_lca_impl_in.h implements inline methods for the
// vm_lca_impl class.
//

inline static vm_lca_impl &
vm_lca_impl::the()
{
	ASSERT(the_vm_lcap != NULL);
	return (*the_vm_lcap);
}

inline static void
vm_lca_impl::register_lca()
{
	the_vm_lcap->register_lca_with_cb_coord();
}

#endif	/* _VM_LCA_IMPL_IN_H */
