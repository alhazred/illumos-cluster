//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)vp_triad.cc 1.8     08/05/20 SMI"

#include <vm/vp_triad.h>

// Subtraction method
//
// The result of a subtraction will be specified in either just 'this' or
// 'this' and 'remainder'.  We need remainder when 't' is in the middle of
// 'this', effectively splitting the result into two parts.  For example,
// let 'this' be the major/minor numbers 1.0 thru 1.9 and 't' be 1.5 thru
// 1.7 (represented by the triads [1, 0, 9] and [1, 5, 7] respectively).
// Then the subtraction of 't' from 'this' will result in the two triads:
// 1.0 thru 1.4 and 1.8 thru 1.9 (represented by [1, 0, 4] and [1, 8, 9]).
// In other words [1, 0, 9] - [1, 5, 7] = [1, 0, 4] and [1, 8, 9].
//
// If 'this' is not split by t, the answer will be returned in 'this' and
// remainder will be zeroed.  'this' may also be zeroed if there were no
// values remaining.  If 'this' is split, the lower triad will always be
// returned in 'this' and the higher triad will always be in remainder.
// The caller can determine whether a remainder was returned by calling
// is_empty() on it.
//
// Input is valid if all arguments are non-negative short integer values,
// minor_min <= minor_max, and major_num > 0.
//
// The return value is 0 if the highest version in both triads is the same,
// -1 if the highest version in "this" is lower than the highest version in
// t, and 1 if the highest version in "this" is higher than the highest
// version in t.  This result is used by the version_range subtraction
// operator.  The other anachronisms in the semantics of this method resulted
// from what was convenient at the version_range level.
//

int
vp_triad::subtraction(const vp_triad &t, vp_triad &remainder)
{
	// Reset the second result to NULL
	remainder.zero();

	// Subtracting something with itself yields zero.
	if (this == &t) {
		zero();
		return (0);
	}

	// If the major numbers do not match then we cannot subtract d2 from d1
	// So just return the entire set in d1.
	if (triad.major_num > t.triad.major_num) {
		return (1);
	} else if (triad.major_num < t.triad.major_num) {
		return (-1);
	}

	// Okay, do the subtraction
	if (triad.minor_min > t.triad.minor_max) {
		return (1);
	} else if (triad.minor_max < t.triad.minor_min) {
		return (-1);
	} else if (triad.minor_min >= t.triad.minor_min) {
		if (triad.minor_max < t.triad.minor_max) {
			zero();
			return (-1);
		} else if (triad.minor_max > t.triad.minor_max) {
			assign(triad.major_num, t.triad.minor_max + 1,
			    triad.minor_max);
			return (1);
		} else {
			ASSERT(triad.minor_max == t.triad.minor_max);
			zero();
			return (0);
		}
	} else {
		if (triad.minor_max < t.triad.minor_max) {
			assign(triad.major_num, triad.minor_min,
			    t.triad.minor_min - 1);
			return (-1);
		} else if (triad.minor_max > t.triad.minor_max) {
			remainder.assign(triad.major_num,
			    t.triad.minor_max + 1, triad.minor_max);
			assign(triad.major_num, triad.minor_min,
			    t.triad.minor_min - 1);
			return (1);
		} else {
			ASSERT(triad.minor_max == t.triad.minor_max);
			assign(triad.major_num, triad.minor_min,
			    t.triad.minor_min - 1);
			return (0);
		}
	}
}

// Intersection operator
//
// The result of intersecting two triads will be specified in exactly one triad.
//
// Input is valid if all arguments are non-negative short integer values,
// minor_min <= minor_max, and major_num > 0.
//
// The return value is 0 if the highest version in both triads is the same,
// -1 if the highest version in "this" is lower than the highest version in
// t, and 1 if the highest version in "this" is higher than the highest
// version in t.  This result is used by the version_range subtraction
// operator.
int
vp_triad::intersection(const vp_triad & t, vp_triad &result)
{
	// Intersecting something with itself yields itself
	if (this == &t) {
		result.assign(*this);
		return (0);
	}

	// The major numbers do not match so make result an empty triad
	if (triad.major_num > t.triad.major_num) {
		result.zero();
		return (1);
	} else if (triad.major_num < t.triad.major_num) {
		result.zero();
		return (-1);
	} else if (triad.minor_max < t.triad.minor_min) {
		result.zero();
		return (-1);
	} else if (triad.minor_min > t.triad.minor_max) {
		result.zero();
		return (1);
	} else {
	// The minor numbers do intersect so calculate the intersection
	// Range:
		result.triad.major_num = t.triad.major_num;
		if (triad.minor_min < t.triad.minor_min)
			result.triad.minor_min = t.triad.minor_min;
		else
			result.triad.minor_min = triad.minor_min;
		if (triad.minor_max > t.triad.minor_max) {
			result.triad.minor_max = t.triad.minor_max;
			return (1);
		} else if (triad.minor_max < t.triad.minor_max) {
			result.triad.minor_max = triad.minor_max;
			return (-1);
		} else {
			ASSERT(triad.minor_max == t.triad.minor_max);
			result.triad.minor_max = t.triad.minor_max;
			return (0);
		}
	}
}
