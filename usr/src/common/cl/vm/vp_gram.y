/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * #pragma ident   "@(#)vp_gram.y 1.10     08/05/20 SMI"
 */

/*
 *
 * Yacc grammar for the SunCluster 3.1 Version Manager Versioned Protocol
 * Specification file format, defining the parser "vp_parse()"
 *
 * Versioned Protocol Overview:
 * ---------------------------
 *
 * VP (Versioned Protocol) is the file format for declaring the eligible
 * versions of a particular component of SunCluster on a particular node.
 * Some versions have dependencies on other versioned protocols and their
 * particular versions.  This file will be checked for syntax and semantic
 * correctness and then internalized by the Version Manager. If either a
 * semantic or syntax error is encountered, the parser will halt and attempt
 * to output an appropriate message.  If the file is correct, then the Version
 * Manager will attempt to discover exactly the major and minor numbers
 * for each associated versioned protocol that satisifies every dependency
 * on every versioned protocol.  If no such major and minor numbers exist,
 * then the Version Manager will respond appropriately.
 *
 * The VP file format follows: (A leading '*' depicts an optional line; a
 * leading '+' depicts that at least one such line is required.)
 * The first five lines are required to be present exactly once.
 *
 *	File_format_version = integer;
 *	Name = C-style token;
 *	Description = quoted ascii-string;
 *	Source = ascii-string, no newlines;
 *	Mode = one of "Cluster", "Nodepair", or "Node";
 *	*Attributes: If included, must have one or more of these values:
 *		"Bootstrap", "Custom", "Running", and/or "Persistent"
 *		in a comma separated list followed by a semicolon;
 *	*Custom_callback_name = A C-style token that should only be
 *		specified if and only if the "Custom" attribute is listed.;
 *	+Version : version-line;
 *	*Upgrade : upgrade-line;
 */

/*
 * A version line serves two purposes.  It specifies a particular version the
 * versioned protocol supports on this node, and it lists a set of dependencies
 * on other versioned protocols for that version.  There must be at least one
 * version line in a single vp specification file.
 *
 * A version-line has the following structure: ( Sections enclosed by [ ] are
 * optional.)
 *	version-line : version_range  [ -> dependency-set ]
 *	version_range : tuple [ , tuple ...]
 *	tuple : MAJOR.MINORMIN [ - MINORMAX ] [ ( description ) ] ,
 *		where MAJOR, MINORMIN, and MINORMAX are unsigned integers,
 *		description is a ascii-string, no newlines.
 *	dependency-set : dependency [ & dependency ]
 *	dependency : ascii-string ( dep_version_range )
 *		The string is the name of another versioned protocol specified
 *		in a different file.
 *	dep_version_range : dep_tuple [ , dep_tuple ...]
 *	dep_tuple : MAJOR.MINORMIN [ - dep_max ]	, where MAJOR and
 *							MINORMIN are integers.
 *	dep_max : MINORMAX | *		, where MINORMAX is an integer and '*'
 *					represents the maximum minor number.
 *					Usage of '*' is optional.
 */

/*
 * An upgrade line serves a single purpose.  It specifies the version in the
 * first version range can only be upgraded to the versions in the second
 * version range.  There can be multiple upgrade lines in a single versioned
 * protocol specification file.  Upgrade from a later version to an earlier
 * version is strictly prohibited (i.e. Trying to upgrade from version 4.0
 * to the earlier version 3.0 is prohibited.)
 *
 * An upgrade-line has the following structure:
 *	upgrade-line : version_range -> version-range
 */

/*
 * The parser implementation is very straightforward.  If the file matches
 * the grammar indicated, the vm_interface functions are used to construct
 * a half-formed versioned_protocol object (which is completed when all files
 * have been parsed).  If the file doesn't match the grammar, a syntax error
 * is returned.
 *
 * On syntax errors, this interface will leak anything allocated and put into
 * a token value union which is still in the internal array yy_yyv that serves
 * as the stack.  This isn't a problem for now because we'll panic in that case
 * anyway.  To fix it, we'll just need the lexer to make a list of anything it
 * allocates, and be careful to remove things from the list as we use them, and
 * delete the elements in the list if we hit an error.
 */
%{
/*
 * The parser to handle the versioned protocol specification files.
 */
#include <vm/vp_error.h>
#include <vm/vp_interface.h>
#include <vm/vm_yy_scanner_impl.h>
#include <vm/vm_dbgbuf.h>

/*
 * Don't expand the token depth.  This means that if we exceed the hard-coded
 * token depth, we'll return a syntax error.  This is fine because:
 *
 * A)  The hard coded depth is 150, which is quite deep, and
 * B)  We'll always parse files before we ship them, so if we ever run
 *     into this problem, we can just increase YYMAXDEPTH to a higher number
 *     and use patch dependencies to ensure the parser with the higher value
 *     is used to parse the offending file.
 */

#define	YYEXPAND(a) (0)

/*
 * Be nice to other kernel parsers by changing the names of our symbols to
 * something other than the yacc defaults.  These should eventually be hidden
 * from other kernel modules using symbol map files (as should a huge number
 * of our other symbols).
 */
#define	yyparse		vm_yy_parse
#define	yylex		vm_yy_lex
#define	yyerror		vm_yy_error
#define	yy_yys		vm_yy_yys
#define	yy_yyv		vm_yy_yyv
#define	yyact		vm_yy_act
#define	yychar		vm_yy_char
#define	yychk		vm_yy_chk
#define	yydebug		vm_yy_debug
#define	yydef		vm_yy_def
#define	yyerrflag	vm_yy_errflag
#define	yyexca		vm_yy_exca
#define	yylval		vm_yy_lval
#define	yynerrs		vm_yy_nerrs
#define	yypact		vm_yy_pact
#define	yypgo		vm_yy_pgo
#define	yyps		vm_yy_ps
#define	yypv		vm_yy_pv
#define	yyr1		vm_yy_r1
#define	yyr2		vm_yy_r2
#define	yys		vm_yy_s
#define	yystate		vm_yy_state
#define	yytmp		vm_yy_tmp
#define	yyv		vm_yy_v
#define	yyval		vm_yy_val
#define	yytoks		vm_yy_toks
#define	yyreds		vm_yy_reds

#define	YYDEBUG 0

/*
 * Workaround to deal with this generated code in vm_yy.tab.cc when
 * _KERNEL is defined:
 *
 * #ifdef __STDC__
 * #include <stdlib.h>
 * #include <string.h>
 * #define YYCONST const
 * #else
 * #include <malloc.h>
 * #include <memory.h>
 * #define YYCONST
 * #endif
 *
 * We don't want to include the above user header files when _KERNEL
 * is defined because we end up with multiple declarations of some
 * of the routines in these header files.
 * Error: Only one of a set of overloaded functions can be extern "C"
 * Trick the preprocessor into believing they were already included.
 */

#ifdef	_KERNEL
#define	_STDLIB_H
#define	_STRING_H
#define	_MALLOC_H
#define	_MEMORY_H
#endif	/* _KERNEL */

int vm_yy_lex(void);
static void vm_yy_error(const char *);

%}

%union {
	int ival;			// An integer value
	vp_type vpmode;			// A mode enum
	char *name;			// A cword or string
	void *class_p;			// A pointer to some C++ class
	struct vp_yy_triad vptri;	// A whole triad
}

%token ERRTOK 499

%token FILE_FORMAT_VERSION 500
%token NAME 501
%token DESCRIPTION 502
%token SOURCE 503
%token MODE 504
%token CLUSTER 505
%token NODEPAIR 506
%token NODE 507
%token ATTRIBUTES 508
%token BOOTSTRAP 509
%token CUSTOM 510
%token RUNNING 511
%token PERSISTENT 512
%token CUSTOM_CALLBACK_NAME 513
%token VERSION 514
%token UPGRADE 515

%token COLON 516
%token SEMI 517
%token ASSIGN 518
%token ARROW 519
%token AMP 520
%token LP 521
%token RP 522
%token DOT 523
%token COMMA 524
%token STAR 525
%token DASH 526

%token <ival> INUM 527
%token <name> STRING 528
%token <name> CWORD 529

%type <vptri>	tuple dependency_tuple
%type <class_p>	version_range dependency_version_range dependency
		dependency_set vline uline
%type <ival>	ff_version attributes attrlist theattrs
%type <vpmode>	mode themodes
%type <name>	name desc source ccb

%start vp_spec_file
%%

vp_spec_file	: headers versions upgrades
		;

headers		: ff_version name desc source mode attributes ccb
			{
				assign_vp(vpp, $1, $2, $3, $4, $7, $5, $6);
			}
		;

ff_version	: FILE_FORMAT_VERSION ASSIGN INUM SEMI
			{
				if ($3 != 1) {
					VM_DBG(("Unknown file version %d\n",
					    $3));
					YYABORT;
				}
				$$ = $3;
			}
		;

name		: NAME ASSIGN CWORD SEMI
			{
				$$ = $3;
			}
		;

desc		: DESCRIPTION ASSIGN STRING SEMI
			{
				$$ = $3;
			}
		;

source		: SOURCE ASSIGN STRING SEMI
			{
				$$ = $3;
			}
		;

mode		: MODE ASSIGN themodes SEMI
			{
				$$ = $3;
			}
		;

attributes	: /* empty */
			{
				$$ = 0;
			}
		|  ATTRIBUTES COLON attrlist SEMI
			{
				$$ = $3;
			}
		;

ccb		: /* empty */
			{
				$$ = NULL;
			}
		|  CUSTOM_CALLBACK_NAME ASSIGN CWORD SEMI
			{
				$$ = $3;
			}
		;

themodes	: CLUSTER
			{
				$$ = VP_CLUSTER;
			}
		| NODEPAIR
			{
				$$ = VP_NODEPAIR;
			}
		| NODE
			{
				$$ = VP_NODE;
			}
		;

theattrs	: BOOTSTRAP
			{
				$$ = VP_BOOTSTRAP;
			}
		| CUSTOM
			{
				$$ = VP_CUSTOM;
			}
		| RUNNING
			{
				$$ = VP_RUNNING;
			}
		| PERSISTENT
			{
				$$ = VP_PERSISTENT;
			}
		;

attrlist	: theattrs
			{
				$$ = $1;
			}
		| attrlist COMMA theattrs
			{
				$$ = $1 | $3;
			}
		;

versions	: /* empty */
		| versions vline
			{
				insert_vp_dependency(vpp, $2);
			}
		;

vline		: VERSION COLON version_range SEMI
			{
				$$ = new_vp_dependency(NULL);
				assign_lhs($$, $3);
			}
		| VERSION COLON version_range ARROW dependency_set SEMI
			{
				assign_lhs($5, $3);
				$$ = $5;
			}
		;

upgrades	: /* empty */
		| upgrades uline
			{
				insert_vp_upgrade(vpp, $2);
			}
		;

uline		: UPGRADE COLON version_range ARROW version_range SEMI
			{
				$$ = new_vp_upgrade($3, $5);
			}
		;

dependency_set	: dependency_set AMP dependency
			{
				insert_rhs($1, $3);
				$$ = $1;
			}
		| dependency
			{
			/*
			 * Create a vp_dependency instance with an empty
			 * lefthand side.  (The lefthand side will be
			 * assigned by the 'version_description' yacc
			 * grammar rule.)
			 */
				$$ = new_vp_dependency($1);
			}
		;

dependency	: CWORD LP dependency_version_range RP
			{
				$$ = new_vp_rhs_version($1, $3);
			}
		;

version_range	: version_range COMMA tuple
			{
			/*
			 * Add this tuple into our version_range instance
			 */
				insert_triad($1, &$3);
				$$ = $1;
			}
		| tuple
			{
			/*
			 * Create the version_range instance now that all the
			 * triads have been defined.
			 */
				$$ = new_version_range(&$1);
			}
		;

tuple		: INUM DOT INUM
			{
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = (uint16_t)$3;
			}
		| INUM DOT INUM DASH INUM
			{
				if ($3 > $5) {
					VM_DBG(("Invalid triad %d.%d-%d\n",
					    $1, $3, $5));
					YYABORT;
				}
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = (uint16_t)$5;
			}
		| INUM DOT INUM LP STRING RP
			{
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = (uint16_t)$3;
				$$.description = $5;
			}
		| INUM DOT INUM DASH INUM LP STRING RP
			{
				if ($3 > $5) {
					VM_DBG(("Invalid triad %d.%d-%d\n",
					    $1, $3, $5));
					YYABORT;
				}
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = (uint16_t)$5;
				$$.description = $7;
			}
		;

dependency_version_range
		: dependency_version_range COMMA dependency_tuple
			{
				insert_triad($1, &$3);
				$$ = $1;
			}
		| dependency_tuple
			{
				$$ = new_version_range(&$1);
			}
		;

dependency_tuple
		: INUM DOT INUM
			{
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = (uint16_t)$3;
			}
		| INUM DOT INUM DASH INUM
			{
				if ($3 > $5)
					YYABORT;
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = (uint16_t)$5;
			}
		| INUM DOT INUM DASH STAR
			{
				$$.major_num = (uint16_t)$1;
				$$.minor_min = (uint16_t)$3;
				$$.minor_max = MAXSHORT;
			}
		;

%%

static vm_yy_scanner_impl *vm_yy_scanner = NULL;

/* Call the lexer's lex() */
int
vm_yy_lex() {
	ASSERT(vm_yy_scanner);
	return (vm_yy_scanner->lex());
}

/*
 * Static pointer to allow the parser to find the current versioned_protocol
 * class.
 */

static void *vpp = NULL;

/*
 * Parses one versioned protocol specification file (vp spec file).
 * The vp spec file is checked for syntax and semantic errors.  If the
 * vp spec file is valid, then the information contained within is internalized
 * into the versioned_protocol object, 'vpp'.
 */
vp_error_t
process_vp_file(versioned_protocol **my_vpp, char *filename)
{
	int retval;
	vm_yy_scanner = new vm_yy_scanner_impl;

	/* Reset the vm_yy_in to access the inputted vp spec file */
	if (!vm_yy_scanner->initialize(filename)) { /* open failed */
		VM_DBG(("Couldn't open vp file %s\n", filename));
		return (VP_CANNOTOPENFILE);
	}

	*my_vpp = new versioned_protocol();
	vpp = (void *) *my_vpp;
	ASSERT(vpp);

	VM_DBG(("Parsing vp file %s\n", filename));

	/* Parse the versioned protocol spec file */
	retval = vm_yy_parse();

	/* Close the file pointer */
	vm_yy_scanner->done();
	delete vm_yy_scanner;
	vm_yy_scanner = NULL;

	if (retval != 0) {
		VM_DBG(("Syntax error in vp file %s\n", filename));
		delete *my_vpp;
		*my_vpp = NULL;
		return (VP_SYNTAX);
	}

	VM_DBG(("Parsed %s successfully\n", filename));
	return (VP_SUCCESS);
}

static void
vm_yy_error(const char *estr)
{
	ASSERT(vm_yy_scanner);
	vm_yy_scanner->vm_yy_error(estr);
}
