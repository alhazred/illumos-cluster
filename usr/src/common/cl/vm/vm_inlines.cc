//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)vm_inlines.cc 1.5     08/05/20 SMI"

// Compile all inline functions

#ifdef NOINLINES
#define	inline
#include <vm/coord_cluster_info.h>
#include <vm/coord_cluster_info_in.h>
#include <vm/coord_vp.h>
#include <vm/coord_vp_in.h>
#include <vm/ucc_group.h>
#include <vm/ucc_group_in.h>
#include <vm/version_prot_obj.h>
#include <vm/version_prot_obj_in.h>
#include <vm/version_range.h>
#include <vm/version_range_in.h>
#include <vm/versioned_protocol.h>
#include <vm/versioned_protocol_in.h>
#include <vm/vm_admin_impl.h>
#include <vm/vm_admin_impl_in.h>
#include <vm/vm_comm.h>
#include <vm/vm_comm_in.h>
#include <vm/vm_coord.h>
#include <vm/vm_coord_in.h>
#include <vm/vm_lca_impl.h>
#include <vm/vm_lca_impl_in.h>
#include <vm/vm_stream.h>
#include <vm/vm_stream_in.h>
#include <vm/vm_yy_scanner_impl.h>
#include <vm/vm_yy_scanner_impl_in.h>
#include <vm/vp_constraint.h>
#include <vm/vp_constraint_in.h>
#include <vm/vp_dependency.h>
#include <vm/vp_dependency_in.h>
#include <vm/vp_rhs_version.h>
#include <vm/vp_rhs_version_in.h>
#include <vm/vp_set.h>
#include <vm/vp_set_in.h>
#include <vm/vp_set_key.h>
#include <vm/vp_set_key_in.h>
#include <vm/vp_triad.h>
#include <vm/vp_triad_in.h>
#include <vm/vp_upgrade.h>
#include <vm/vp_upgrade_in.h>
#undef	inline		// in case code is added below that uses inline
#endif  // NOINLINES
