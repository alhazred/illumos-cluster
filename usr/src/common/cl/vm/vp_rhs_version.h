/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_RHS_VERSION_H
#define	_VP_RHS_VERSION_H

#pragma ident	"@(#)vp_rhs_version.h	1.14	08/05/20 SMI"

#include <sys/os.h>
#include <sys/list_def.h>
#include <vm/version_range.h>
#include <vm/version_prot_obj.h>

//
// Version Manager vp_rhs_version Class Definition
//
// The vp_rhs_version object represents a single dependency in the right-
// hand-side of a Version: line in a vp spec file.  The version_range
// represents the versions listed and the name and vpop represent the vp
// that is depended on.  The dependency will start out with just the string,
// but in link() the string is deleted and is replaced by the vpop, which is
// set to vpop_to.
//
class vp_rhs_version : public _SList::ListElem {
public:
	// Constructor
	vp_rhs_version(char *name, version_range &rhs);

	// Destructor
	~vp_rhs_version();

	// set rhs_vpop and create the reverse dependency
	void link(version_prot_obj *vpop_from, version_prot_obj *vpop_to);

	// clear rhs_vpop and remove the reverse dependency
	void unlink(version_prot_obj *vpop_from);

	// Return the single right-hand side.
	version_range &get_vr();

	// Return the versioned protocol's name.
	char *get_name() const;

	// Return the versioned_protocol pointers (from version_prot_obj)
	versioned_protocol *get_vpp();

	coord_vp *get_cvpp();

	// Return the version_prot_obj pointer
	version_prot_obj * get_vpop();

private:
	char *rhs_name;			// The name of the versioned protocol
					// on the right-hand side of the
					// dependency.
	version_prot_obj *rhs_vpop;	// A pointer to the right-hand versioned
					// protocol.
	version_range rhs_versions;	// The particular major/minor number
					// pair(s).

	vp_rhs_version(const vp_rhs_version &);
	vp_rhs_version &operator = (vp_rhs_version &);
};

#ifndef	NOINLINES
#include <vm/vp_rhs_version_in.h>
#endif	// NOINLINES

#endif	/* _VP_RHS_VERSION_H */
