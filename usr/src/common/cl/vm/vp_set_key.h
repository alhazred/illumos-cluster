/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_SET_KEY_H
#define	_VP_SET_KEY_H

#pragma ident	"@(#)vp_set_key.h	1.13	08/05/20 SMI"

#include <sys/os.h>
#include <sys/nodeset.h>
#include <vm/version_prot_obj.h>
#include <vm/version_range.h>
#include <vm/vp_set.h>

// A vp_set_key contains an array (of configurable size) of version_prot_obj
// pointers.  It serves as the "key" to a collection of vp_sets.  Each version
// range in a vp_set is associated with the version_prot_obj in the vp_set_key
// that has the same index.

class vp_set_key {
public:
	// The group_entry object stores information for a single element
	// in a connected subgroup.  The index of each group_entry is the
	// ordinal of the vp in the group.
	class group_entry {
	public:
		group_entry();
		~group_entry();

		// Retrieve a pointer to the stored version_prot_obj
		version_prot_obj *get_vpop();

		// Given a version range, calculate the score for the highest
		// version in that range using all_versions and scores.
		uint32_t get_score(version_range &);

		// Set the value of the stored version_prot_obj
		void set_vpop(version_prot_obj *);

		// Merge the values in the argument into all_versions
		void merge_all_versions(version_range &);

		// dump the contents of all_versions to the debug buffer
		void print_all_versions();

		// Compute scores to match the triads in all_versions
		void compute_scores();
	private:
		version_prot_obj	*vpop;		// A pointer to the
							// associated vpop
		version_range		all_versions;	// Stores every version
							// defined for the vpop
							// on any node
		uint32_t		*scores;	// After compute_scores
							// is called, scores
							// is an array of
							// uint32_t, with each
							// element storing the
							// score of the lowest
							// version in the
							// cooresponding triad
							// of all_versions
	};

	// node_entry objects store the vp_sets associated with each node
	// during a multiverse calculation in vm_coord.
	class node_entry {
	public:
		node_entry();
		~node_entry();

		// Get a reference to the solution universe list
		IntrList<vp_set, _SList> &get_solution_universe();

		// Score each vp_set and then sort the su based on the scores
		// calculated
		void score_and_sort(vp_set_key *);

		// Dump info about the solution universe to the debug buffer.
		void print_su();
	private:
		// The solution_universe is a list of vp_sets representing
		// the valid version combinations for the vps in the group
		// the vp_set_key refers to.
		IntrList<vp_set, _SList> solution_universe;

		// scored_and_sorted is true if score_and_sort has already
		// been called, false otherwise.  It is used to avoid
		// duplicate sorting.
		bool scored_and_sorted;
	};

	vp_set_key();
	vp_set_key(uint16_t s);
	~vp_set_key();

	// Reset the number of array elements to the argument
	void reset(uint16_t);

	// return the number of array elements.
	uint16_t num_elems();

	// Take the vp_set and score it using the information in the
	// group_elems
	void calculate_score(vp_set &);

	// Set the pointer of the array index specified by the first argument
	// to the second argument
	void set_vpop(uint16_t, version_prot_obj *);

	// Merge the version range into all_versions in the group_elem
	// indicated by the first argument
	void merge_all_versions(uint16_t, version_range &);

	// Retrieve a reference to the solution universe for the node
	// indicated by the argument
	IntrList<vp_set, _SList> &get_solution_universe(nodeid_t);

	// score and sort the solution universe for the node indicated by the
	// argument
	void score_and_sort(nodeid_t);

	// Compute the scores in every group_elem
	void compute_scores();

	//
	// Returns true if running or upgrade versions (picked with the
	// second argument) can be calculated for every node in the nodeset.
	// If force is true, versions are recalculated even if they were
	// successfully calculated for a superset of the nodeset, otherwise
	// they aren't recalculated in that case.
	//
	// If calc_only is set, just set the highest_version of each coord_vp,
	// not the rv or uv.
	//
	bool try_nodeset(const nodeset &, bool use_uv, bool calc_only,
	    bool force);

	//
	// Returns true if the cluster membership in ns could upgrade
	// the protocol. Also record whether there are mismatched vp spec
	// files on the nodes and which nodes have the minimal software for
	// the cluster-wide upgrade.
	//
	bool	is_upgrade_needed(version_manager::vp_state &, const nodeset &,
		nodeset &);

	// return the vpop in the group_elem indicated by the argument,
	// cast to the appropriate type
	versioned_protocol *get_vpp(uint16_t);
	coord_vp *get_cvpp(uint16_t);
	version_prot_obj *get_vpop(uint16_t);

	// Output the contents of the key to the debug buffer
	void dbprint();
private:
	uint16_t		_num_elems;	// Size of group_entries array
	group_entry		*group_entries; // The array of group_entries

	// A node_entry for each possible node
	node_entry		node_entries[NODEID_MAX + 1];

	// When not empty, the nodeset of the last successful version
	// calculation
	nodeset			last_calc;

	vp_set_key &operator = (vp_set_key &);
};

#ifndef	NOINLINES
#include <vm/vp_set_key_in.h>
#endif	// NOINLINES

#endif	/* _VP_SET_KEY_H */
