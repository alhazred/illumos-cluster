/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vm_util.cc	1.8	08/05/20 SMI"

#include <sys/vm_util.h>
#include <nslib/ns.h>
#include <h/replica.h>
#include <sys/os.h>

const char vm_util::vm_bind_name[] = "version_manager";

//
// Library function to return a reference to the VM from a requested node.
// If the nodeid is NODEID_UNKNOWN, the VM from the local node is returned.
//
version_manager::vm_admin_ptr
vm_util::get_vm(nodeid_t n)
{
	Environment e;
	static version_manager::vm_admin_ptr	the_vm = nil;
	CORBA::Object_var vm_obj;

	if ((n == NODEID_UNKNOWN) || (n == orb_conf::local_nodeid())) {

		if (!CORBA::is_nil(the_vm)) {
			return (version_manager::vm_admin::_duplicate(the_vm));
		}

		vm_obj = ns::wait_resolve_local(vm_bind_name, e);
		ASSERT(!e.exception());
		the_vm = version_manager::vm_admin::_narrow(vm_obj);
		ASSERT(!CORBA::is_nil(the_vm));

		return (version_manager::vm_admin::_duplicate(the_vm));

	} else {
		//
		// We don't use wait_resolve_local in the remote case
		// because we don't want to poll across nodes unnecessarily.
		//
		naming::naming_context_var ctxp = ns::local_nameserver(n);

		if (CORBA::is_nil(ctxp)) {
			return (nil);
		}

		vm_obj = ctxp->resolve(vm_bind_name, e);
		if (e.exception()) {
			return (nil);
		}
		version_manager::vm_admin_ptr remote_vm =
		    version_manager::vm_admin::_narrow(vm_obj);

		return (remote_vm);
	}
}
