/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VERSION_PROT_OBJ_IN_H
#define	_VERSION_PROT_OBJ_IN_H

#pragma ident	"@(#)version_prot_obj_in.h	1.13	08/05/20 SMI"

//
// Constructor - Create an vp_edge ojbect to the specified versioned protocol
//
inline
vp_rev_depend::vp_rev_depend(version_prot_obj *my_vpop) :
	_SList::ListElem(this),
	vpop(my_vpop)
{
	ASSERT(my_vpop);
}

inline versioned_protocol *
vp_rev_depend::get_vpp()
{
	if (vpop == NULL)
		return (NULL);

	return (vpop->get_vpp());
}

inline coord_vp *
vp_rev_depend::get_cvpp()
{
	if (vpop == NULL)
		return (NULL);

	return (vpop->get_cvpp());
}

inline version_prot_obj *
vp_rev_depend::get_vpop()
{
	return (vpop);
}

inline
version_prot_obj::version_prot_obj() :
	group_id(-1),
	ordinal(0)
{
}

inline int16_t
version_prot_obj::get_group_id()
{
	return (group_id);
}

inline void
version_prot_obj::set_group_id(int16_t g)
{
	group_id = g;
}

inline uint16_t
version_prot_obj::get_ordinal()
{
	return (ordinal);
}

inline void
version_prot_obj::set_ordinal(uint16_t o)
{
	ordinal = o;
}

#endif	/* _VERSION_PROT_OBJ_IN_H */
