/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_DEPENDENCY_H
#define	_VP_DEPENDENCY_H

#pragma ident	"@(#)vp_dependency.h	1.15	08/05/20 SMI"

#include <sys/os.h>
#include <sys/list_def.h>
#include <vm/version_range.h>
#include <vm/vp_rhs_version.h>

//
// Version Manager vp_dependency Class Definition
//
// The vp_dependency object contains the dependency information found in the
// vp spec file.
// 	Each dependency has a lefthand side and a righthand side, "lhs" and
// "rhs" respectively.  (In the dependency A -> B, A would be the lhs and B
// would be the rhs.)
// 	The lhs contains those particular major/minor number pairs that depend
// on some other vp(s).  The lhs is a single version range object.
//	The rhs represents the vp(s) that the lhs is dependent on.  However,
// unlike the lhs, the rhs consists of a linked list of vp_rhs_version
// instance(s).  All vp_rhs_versions must be satisfied to satisfy the dependency
// as a whole.
// 	Why do we do it this way?  It allows us to handle a dependency
// that reads "Some version of vp A depends on some version of vp B and
// some version of vp C."  Both B and C must be set to a particular version
// to satify the dependency on A.
//
// Rather than have the list be a member and then duplicate all of the
// accessor functions, we just inherit from the list.
//
// A dependency is marked bad when the system has decided that none of the
// version values in the lhs are safe to pick.  This can be because the rhs
// lists a vp spec file that isn't included on the system, and the
// vp_rhs_version's vr doesn't include 1.0, or because there is a dependency
// on a node-mode vp that doesn't contain the running version picked during
// initialization.  Bad dependencies are skipped in all contexts other than
// informational queries.
//

class vp_dependency : public _SList::ListElem {
public:
	// Constructor
	vp_dependency();

	// Desctructor
	~vp_dependency();

	// Returns true if the dependency has been marked bad, false otherwise.
	bool is_bad();

	// Sets or clears the bad flag
	void mark_bad();
	void clear_bad();

	// Assign the left-hand side of the dependency, taking the
	// triad array from the argument.
	void swap_into_lhs(version_range & lhs);

	// Return the left-hand side of the dependency.
	version_range & get_lhs();

	IntrList <vp_rhs_version, _SList> &get_rhs_list();

private:
	bool bad;
	// The left-hand side of the dependency.
	// A single version_range object will hold the major/minor number
	// pair(s) that have a dependency on at least one other vp.
	version_range lhs_versions;

	// The right-hand side of the dependency is a list of vp_rhs_version
	// objects, each of which records the vp and the range of versions
	// supported
	IntrList <vp_rhs_version, _SList> rhs_list;

	vp_dependency(const vp_dependency &);
	vp_dependency &operator = (vp_dependency &);
};

#ifndef	NOINLINES
#include <vm/vp_dependency_in.h>
#endif	// NOINLINES

#endif	/* _VP_DEPENDENCY_H */
