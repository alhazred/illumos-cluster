//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)coord_vp.cc 1.21     08/05/20 SMI"

#include <vm/coord_vp.h>
#include <vm/vp_rhs_version.h>
#include <vm/vm_coord.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vm_stream.h>
#include <vm/vm_coord.h>

coord_vp::node_entry::node_entry(coord_vp *cvp):
	creator_cvp(cvp)
{
}

coord_vp::node_entry::~node_entry()
{
	ASSERT(vp_dependlist.empty());
	creator_cvp = NULL;
}

//
// unlink and delete the vp_rhs_versions and delete the vp_dependencies
//
void
coord_vp::node_entry::clear(coord_vp *cvp)
{
	consistent_versions.empty();

	vp_dependency *vpd;
	vp_rhs_version *vrhs;

	while ((vpd = vp_dependlist.reapfirst()) != NULL) {
		while ((vrhs = vpd->get_rhs_list().reapfirst()) != NULL) {
			vrhs->unlink(cvp);
			delete vrhs;
		}
		delete vpd;
	}
}

//
// unpack the entries for this vp from the stream
//
void
coord_vp::node_entry::set_info(coord_vp *cvp, vm_stream &vms)
{
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	coord_vp *the_cvp;

	// We're new or have been cleared
	ASSERT(consistent_versions.is_empty());
	ASSERT(vp_dependlist.empty());

	while ((vpd = vms.get_vp_dependency()) != NULL) {

		// There are versions.  Print them to the debug buffer
		ASSERT(!vpd->get_lhs().is_empty());
		// If the type is BTSTRP_NODEPAIR, there are no rhs_versions
		ASSERT((cvp->get_vp_type() != VP_BTSTRP_NODEPAIR) ||
		    vpd->get_rhs_list().empty());
		vpd->get_lhs().dbprint();
		VM_DBGC((" -> "));

		// grow the list of consistent_versions
		(void) consistent_versions.merge_equals(vpd->get_lhs());
		IntrList<vp_rhs_version, _SList>::ListIterator
		    rhsi(vpd->get_rhs_list());

		// Link up the vp_rhs_versions
		while ((vrhs = rhsi.get_current()) != NULL) {
			VM_DBGC(("%s ", vrhs->get_name()));
			vrhs->get_vr().dbprint();
			VM_DBGC((", "));
			//
			// get_coord_vp will create a new coord_vp
			// object on-the-fly if one isn't found.
			//
			the_cvp = creator_cvp->get_vm_coord().get_coord_vp(
			    vrhs->get_name(), VP_UNINITIALIZED);

			ASSERT(the_cvp != NULL);
			vrhs->link(cvp, the_cvp);
			// Link also grabs a reference, so release the one
			// we got in get_coord_vp.
			the_cvp->rele();
			rhsi.advance();
		}
		VM_DBGC(("\n"));
		vp_dependlist.append(vpd);
	}
}

//
// Set the version_range to the set of versions we could
// potentially support, given the nodeset and the fact that this
// object corresponds to node.
//
void
coord_vp::node_entry::calc_potential_btstrp_cl(version_range &vr, nodeid_t n,
	const nodeset &ns)
{
	vr.empty();

	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	version_range tmp_vr1, tmp_vr2;
	bool depend_ok;

	// Go through the dependencies
	vp_dependlist.atfirst();
	while ((vpd = vp_dependlist.get_current()) != NULL) {
		depend_ok = true;

		// Go through the vp_rhs_versions.
		vpd->get_rhs_list().atfirst();
		while ((vrhs = vpd->get_rhs_list().get_current()) != NULL) {
			// This is a btstrp_cl, so we can only depend on
			// btstrp_np vps.
			ASSERT(vrhs->get_cvpp()->get_vp_type() ==
			    VP_BTSTRP_NODEPAIR);

			// Get the list of active running versions.
			vrhs->get_cvpp()->active_btstrp_np_rvs(tmp_vr1, n, ns);

			// Subtract the versions in the vp_rhs_version from
			// the active_btstrp_np versions.  If there are versions
			// active that aren't in the former, we can't use this
			// dependency.
			tmp_vr1.subtraction(vrhs->get_vr(), tmp_vr2);
			if (!tmp_vr2.is_empty()) {
				depend_ok = false;
				break;
			}
			vpd->get_rhs_list().advance();
		}
		// if the dependency was ok, merge the lhs into vr.
		if (depend_ok) {
			(void) vr.merge_equals(vpd->get_lhs());
		}
		vp_dependlist.advance();
	}
}

void
coord_vp::node_entry::connected_components_bfs(
	IntrList <coord_vp, _SList> &vqueue, int16_t group, uint16_t &gcount)
{
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	version_manager::vp_version_t vpv;

	consistent_versions.empty();

	IntrList<vp_dependency, _SList>::ListIterator depi(vp_dependlist);
	while ((vpd = depi.get_current()) != NULL) {
		vpd->clear_bad();
		IntrList<vp_rhs_version, _SList>::ListIterator
		    rhsi(vpd->get_rhs_list());
		while ((vrhs = rhsi.get_current()) != NULL) {
			ASSERT(vrhs->get_cvpp() != NULL);

			if (vrhs->get_cvpp()->get_vp_type() ==
			    VP_BTSTRP_CLUSTER) {
				vrhs->get_cvpp()->get_rv(vpv);
				if (!vrhs->get_vr().contains(vpv))
					vpd->mark_bad();
				rhsi.advance();
				continue;
			}

			ASSERT(vrhs->get_cvpp()->get_vp_type() == VP_CLUSTER);
			if (vrhs->get_cvpp()->get_group_id() == -1) {
				vrhs->get_cvpp()->set_group_id(group);
				vrhs->get_cvpp()->set_ordinal(gcount);
				gcount++;
				vqueue.append(vrhs->get_cvpp()->get_bfs_elem());
			} else {
				ASSERT(group ==
				    vrhs->get_cvpp()->get_group_id());
			}

			rhsi.advance();
		}

		if (!vpd->is_bad()) {
			(void) consistent_versions.merge_equals(vpd->get_lhs());
		}

		depi.advance();
	}
}

bool
coord_vp::node_entry::cl_check_rv(version_manager::vp_version_t &vpv,
	bool use_uv)
{
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	version_manager::vp_version_t vrhs_rv;

	// Find the relevant dependency
	IntrList<vp_dependency, _SList>::ListIterator depi(vp_dependlist);
	while ((vpd = depi.get_current()) != NULL) {
		if (vpd->get_lhs().contains(vpv))
			break;
		depi.advance();
	}

	if (vpd == NULL)
		return (false);

	// Verify the dependencies for the relevant vp_dependency
	IntrList<vp_rhs_version, _SList>::ListIterator
	    rhsi(vpd->get_rhs_list());
	while ((vrhs = rhsi.get_current()) != NULL) {
		ASSERT(vrhs->get_cvpp() != NULL);
		// If the type of the dependency is btstrp_cl, we need to load
		// the rv even if we are checking uvs, because the versions of
		// btstrp_cl are chosen independently and uvs aren't used.
		if (use_uv &&
		    (vrhs->get_cvpp()->get_vp_type() != VP_BTSTRP_CLUSTER))
			vrhs->get_cvpp()->get_uv(vrhs_rv);
		else
			vrhs->get_cvpp()->get_rv(vrhs_rv);
		// If one has never been calculated, it will have to be 1.0
		if (vrhs_rv.major_num == 0) {
			vrhs_rv.major_num = 1;
			vrhs_rv.minor_num = 0;
		}
		if (!vrhs->get_vr().contains(vrhs_rv))
			return (false);
		rhsi.advance();
	}

	return (true);
}
//
// Constructor
//
coord_vp::coord_vp(const char *vname, vp_type t, vm_coord *tvmcp) :
	_SList::ListElem(this),
	cvp_name(os::strdup(vname)),
	the_type(t),
	this_vm_coord(tvmcp),
	bfs_elem(this)
{
	ASSERT(cvp_name);

	rv.major_num = rv.minor_num = 0;
	uv.major_num = uv.minor_num = 0;
	highest_uv.major_num = 1;
	highest_uv.minor_num = 0;

	for (nodeid_t n = 0; n <= NODEID_MAX; n++)
		node_entries[n] = NULL;
}

//
// Destructor
//
coord_vp::~coord_vp()
{
#ifdef	DEBUG
	for (nodeid_t n = 0; n <= NODEID_MAX; n++) {
		ASSERT(node_entries[n] == NULL);
	}
#endif	// DEBUG
	delete [] cvp_name;
	cvp_name = NULL;
	ASSERT(cvp_name == NULL);
	this_vm_coord = NULL;
}

//
// From version_prot_obj
//

char *
coord_vp::get_name()
{
	return (cvp_name);
}

versioned_protocol *
coord_vp::get_vpp()
{
	ASSERT(0);
	return (NULL);
}

coord_vp *
coord_vp::get_cvpp()
{
	return (this);
}

//
// Add to stream the VP information for given nodeid.
// (Nothing todo if given nodeid not found in the coord_vp list.)
//
void
coord_vp::put_stream(nodeid_t nid, vm_stream *vmsp)
{
	node_entry *entry = node_entries[nid];
	//
	// If found info for targeted node.
	//
	if (entry != NULL) {
		// Set header.
		vmsp->put_header(get_name(), (uint16_t)get_vp_type(), rv, uv);

		vp_dependency *vpd;
		IntrList<vp_dependency, _SList>::ListIterator
			depi(entry->get_vp_dependency_list());

		// For every dependency.
		while ((vpd = depi.get_current()) != NULL) {

			// Left hand.
			vmsp->put_version_range(vpd->get_lhs());

			vp_rhs_version *vrhs;
			IntrList<vp_rhs_version, _SList>::ListIterator
				rhsi(vpd->get_rhs_list());

			// For every right hand.
			while ((vrhs = rhsi.get_current()) != NULL) {
				vmsp->put_vp_rhs_version(vrhs);
				rhsi.advance();
			}
			// Terminate the list.
			vmsp->put_vp_rhs_version(NULL);

			depi.advance();
		}
		// Terminate list.
		version_range tmp_vr;
		vmsp->put_version_range(tmp_vr);

	}
}

//
// This method calculates the highest nodepair running version between
// the local (l) and remote (r) nodes and returns it in the_rv.  If there
// is no compatible version, it returns 0.0.
//
// It can only be called on VP_BTSTRP_NODEPAIR vps.
//

void
coord_vp::calc_highest_nodepair(nodeid_t l, nodeid_t r,
	version_manager::vp_version_t &the_rv)
{
	version_range l_vr, r_vr, inter_vr;

	ASSERT(the_type == VP_BTSTRP_NODEPAIR);

	if (node_entries[l] != NULL) {
		node_entries[l]->get_consistent_versions(l_vr);
	} else {
		(void) l_vr.insert_triad(1, 0, 0);
	}

	if (node_entries[r] != NULL) {
		node_entries[r]->get_consistent_versions(r_vr);
	} else {
		(void) r_vr.insert_triad(1, 0, 0);
	}

	l_vr.intersection(r_vr, inter_vr);

	inter_vr.highest_version(the_rv.major_num, the_rv.minor_num);
}

//
// Unpack the information in the stream into a node_entry cooresponding
// to n.  The current running version reported from n will be in vpv.
//
void
coord_vp::set_node_info(nodeid_t n, version_manager::vp_version_t &n_rv,
	version_manager::vp_version_t &n_uv, uint32_t rva, vm_stream &vms)
{
	VM_DBG(("Setting info for %s on node %u", cvp_name, n));

	if (rva & RV_LOAD) {
		ASSERT(!(rva & RV_VERIFY));
		ASSERT((n_rv.major_num > rv.major_num) ||
		    ((n_rv.major_num == rv.major_num) &&
		    (n_rv.minor_num >= rv.minor_num)));
		rv.major_num = n_rv.major_num;
		rv.minor_num = n_rv.minor_num;
		VM_DBGC((" new rv %u.%u", rv.major_num, rv.minor_num));
	}

	if (rva & RV_VERIFY) {
		ASSERT(!(rva & RV_LOAD));
		// If the old version was 1.0, we might have deleted the
		// coord_vp when the reference count went to 0, so we just
		// restore 1.0 here.
		if (rv.major_num == 0) {
			ASSERT(n_rv.major_num == 1 && n_rv.minor_num == 0);
			rv.major_num = n_rv.major_num;
			rv.minor_num = n_rv.minor_num;
		}
		ASSERT((rv.major_num == n_rv.major_num) &&
		    (rv.minor_num == n_rv.minor_num));
	}

	if (rva & UV_LOAD) {
		ASSERT(!(rva & UV_VERIFY) && !(rva & UV_CLEAR));
		ASSERT(the_type == VP_CLUSTER);
		uv.major_num = n_uv.major_num;
		uv.minor_num = n_uv.minor_num;
		VM_DBGC((" new uv %u.%u", uv.major_num, uv.minor_num));
	}

	if (rva & UV_VERIFY) {
		ASSERT(!(rva & UV_LOAD) && !(rva & UV_CLEAR));
		ASSERT(the_type == VP_CLUSTER);
		// If the old version was 1.0, we might have deleted the
		// coord_vp when the reference count went to 0, so we just
		// restore 1.0 here.
		if (uv.major_num == 0) {
			ASSERT(n_uv.major_num == 1 && n_uv.minor_num == 0);
			uv.major_num = n_uv.major_num;
			uv.minor_num = n_uv.minor_num;
		}
		ASSERT((uv.major_num == n_uv.major_num) &&
		    (uv.minor_num == n_uv.minor_num));
	}

	if (rva & UV_CLEAR) {
		ASSERT(!(rva & UV_LOAD) && !(rva & UV_VERIFY));
		ASSERT(the_type == VP_CLUSTER);
		ASSERT(n_uv.major_num == 0 && n_uv.minor_num == 0);
		uv.major_num = 0;
		uv.minor_num = 0;
		VM_DBGC((" new uv %u, %u (rezero)", uv.major_num,
		    uv.minor_num));
	}

	VM_DBGC(("\n"));

	ASSERT(node_entries[n] == NULL);

	node_entries[n] = new node_entry(this);
	// Take hold (released whenever the node_entry object is deleted)
	hold();

	node_entries[n]->set_info(this, vms);
}

void
coord_vp::clear_node(nodeid_t n)
{
	hold();
	if (node_entries[n] != NULL) {
		node_entries[n]->clear(this);
		delete node_entries[n];
		node_entries[n] = NULL;
		// Release the hold() associated with node_entries[n]
		rele();
	}
	rele();
}

//
// Clear out entries for nodes not in the nodeset and reset the group_id and
// ordinal
//
void
coord_vp::clear_by_nodeset(const nodeset &ns, bool force, bool need_upgrade)
{
	hold();

	if (need_upgrade) {
		if (the_type == VP_BTSTRP_CLUSTER)
			rv.major_num = rv.minor_num = 0;
		else if (the_type == VP_CLUSTER) {
			uv.major_num = uv.minor_num = 0;
			if (force)
				rv.major_num = rv.minor_num = 0;
		}
	}

	last_calc.empty();
	set_ordinal(0);
	set_group_id(-1);

	// Don't clear out non-matching entries for btstrp_np vps, because
	// they might be due to new connections just forming
	if (force || the_type != VP_BTSTRP_NODEPAIR) {
		for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
			if (ns.contains(n) || (node_entries[n] == NULL))
				continue;
			node_entries[n]->clear(this);
			delete node_entries[n];
			node_entries[n] = NULL;
			// Release the hold() associated with node_entries[n]
			rele();
		}
	}

	rele();
}

uint16_t
coord_vp::connected_components_bfs(int16_t group)
{
	vp_rev_depend *vrd;
	coord_vp *cvp;
	IntrList <coord_vp, _SList> vqueue;
	uint16_t gcount = 0;
	nodeid_t n;

	ASSERT(get_group_id() == -1);
	set_group_id(group);
	set_ordinal(gcount);
	gcount++;

	vqueue.append(get_bfs_elem());

	while ((cvp = vqueue.reapfirst()) != NULL) {
		for (n = 1; n <= NODEID_MAX; n++) {
			if (cvp->node_entries[n] != NULL)
				cvp->node_entries[n]->
				    connected_components_bfs(vqueue, group,
				    gcount);
		}

		IntrList<vp_rev_depend, _SList>::ListIterator
		    rdi(cvp->vp_rev_dependlist);

		while ((vrd = rdi.get_current()) != NULL) {
			if (vrd->get_cvpp()->get_group_id() == -1) {
				vrd->get_cvpp()->set_group_id(group);
				vrd->get_cvpp()->set_ordinal(gcount);
				gcount++;
				vqueue.append(vrd->get_cvpp()->get_bfs_elem());
			} else {
				ASSERT(group ==
				    vrd->get_cvpp()->get_group_id());
			}
			rdi.advance();
		}
	}

	if (gcount == 1)
		set_group_id(0);

	return (gcount);
}

void
coord_vp::cl_get_best_subset(nodeset &ns, bool use_uv)
{
	ASSERT(the_type == VP_CLUSTER);

	// If the major_num is zero, this is a new vp, but we only call this
	// code when some nodes are already initialized, so the running version
	// of this new vp must be 1.0 until we commit.
	if (use_uv && (uv.major_num == 0)) {
		uv.major_num = 1;
		uv.minor_num = 0;
	} else if (rv.major_num == 0) {
		rv.major_num = 1;
		rv.minor_num = 0;
	}

	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (!ns.contains(n))
			continue;

		if (!cl_check_node_rv(n, use_uv))
			ns.remove_node(n);
	}
}

bool
coord_vp::cl_check_node_rv(nodeid_t n, bool use_uv)
{
	version_manager::vp_version_t *goal;

	if (use_uv)
		goal = &uv;
	else
		goal = &rv;

	if (node_entries[n] == NULL) {
		return ((goal->major_num == 1) && (goal->minor_num == 0));
	} else {
		return (node_entries[n]->cl_check_rv(*goal, use_uv));
	}
}

//
// Calculate the highest upgradeable version found in the current nodes
// in the cluster based on the current running version.
//
// The calculation to determine the version takes into account :
// * the nodes currently in the cluster
// * the versions found in the vp spec file
// * any versions that are invalidated by the custom interface
// * the current running version
// * the possible versions that the running version can upgrade to
//
void
coord_vp::calc_highest_version(const nodeset &ns,
    version_manager::vp_version_t &ahv)
{
	version_range tmp_vr1;
	version_manager::vp_version_t cur_ahv;

	// Make sure this is a btstrp_cl vp or a cl vp.
	ASSERT((the_type == VP_BTSTRP_CLUSTER) || (the_type == VP_CLUSTER));

	// Set the ahv to 1.0.
	ahv.major_num = 1;
	ahv.minor_num = 0;

	// Go through each of the nodes.
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (!ns.contains(n))
			continue;
		// Get the potential versions for that node.
		calc_potential_versions(tmp_vr1, n, ns);
		// Get the highest upgradeable version for that node.
		tmp_vr1.highest_version(cur_ahv.major_num, cur_ahv.minor_num);
		VM_DBG(("For node %u and vp %s, the highest potential "
		    "upgradeable version is %u.%u\n",
		    n, get_name(), cur_ahv.major_num, cur_ahv.minor_num));
		// Keep track of the highest version that we've seen.
		if (cur_ahv.major_num > ahv.major_num ||
		    (cur_ahv.major_num == ahv.major_num &&
		    cur_ahv.minor_num > ahv.minor_num)) {
			ahv.major_num = cur_ahv.major_num;
			ahv.minor_num = cur_ahv.minor_num;
		}
	}
}

//
// Attempt to set an rv for this vp using the specified nodeset,
// taking into account whether we are committing.  If the force flag
// is true, compute the rv exactly.  If it is false, and we have
// already found an rv for a nodeset that is a superset of the one
// specified, just return true.  Otherwise recompute. Only set internal
// version if calc_only is set.
//
// Return true if the nodeset allows a single running version, false
// otherwise.
//
bool
coord_vp::try_nodeset(const nodeset &ns, bool new_rv, bool use_uv,
    bool calc_only, bool force)
{
	version_range pot_vr, tmp_vr1;

	// Make sure this is a btstrp_cl vp or a cl vp not in a connected
	// subgroup
	ASSERT((the_type == VP_BTSTRP_CLUSTER) ||
	    (the_type == VP_CLUSTER && get_group_id() == 0));

	//
	// Only consider last_calc if this is not an is_upgrade_needed call
	// to avoid having past state affect the is_upgrade_needed result.
	//
	if (!calc_only) {
		// Check to see if we already calculated this combination
		if (force) {
			if (last_calc.equal(ns)) {
				return (true);
			}
		} else {
			if (last_calc.superset(ns)) {
				return (true);
			}
		}

		// Only change the last_calc if this is not a calc_only.
		last_calc.empty();
	}

	// Pick the appropriate running version to calculate into.
	version_manager::vp_version_t *goal;
	if (use_uv)
		goal = &uv;
	else if (calc_only)
		goal = &highest_uv;
	else
		goal = &rv;

	// If we don't want new running versions, but the current running
	// version isn't set, that means that some node has added this
	// rv, so it must conform to 1.0 or be rejected.

	if (!new_rv && (goal->major_num == 0)) {
		goal->major_num = 1;
		goal->minor_num = 0;
	}

	// Go through each of the nodes
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (!ns.contains(n))
			continue;

		// Get the potential versions for that node.
		calc_potential_versions(tmp_vr1, n, ns);

		// If there is a specific goal version, verify the
		// potential versions contains the goal.
		if (!new_rv) {
			if (!tmp_vr1.contains(*goal)) {
				return (false);
			}
		} else {
			// Otherwise, intersect the potential versions from the
			// node into pot_vr.
			if (pot_vr.is_empty()) {
				pot_vr.swap(tmp_vr1);
			} else {
				pot_vr.intersect_equals(tmp_vr1);
			}
			if (pot_vr.is_empty()) {
				return (false);
			}
		}
	}

	// If there is no goal, set the rv to the highest of the potential
	// versions
	if (new_rv) {
		pot_vr.highest_version(goal->major_num, goal->minor_num);
	}

	// Always set the highest calculated version.
	pot_vr.highest_version(highest_uv.major_num, highest_uv.minor_num);
	VM_DBG(("try_nodeset %s: Setting highest calculated version to "
	    "%u.%u\n",
	    get_name(), highest_uv.major_num, highest_uv.minor_num));
	set_highest_version(highest_uv);

	// Record the successful calculation nodeset if this isn't calc_only.
	if (!calc_only) {
		last_calc.set(ns);
	}
	return (true);
}

//
// is_upgrade_needed
// Called from vm_coord::is_upgrade_needed. Calls try_nodeset with the calc_only
// option set to true to calculate the highest possible version. Then checks
// if the highest possible version is also the running version.
//
bool
coord_vp::is_upgrade_needed(version_manager::vp_state &s, const nodeset &ns,
    nodeset &minimal_nodes)
{
	bool	nodeset_found = true;
	bool	upgrade_needed = false;

	// Calculate the highest possible version.
	nodeset_found = try_nodeset(ns, true, false, true, true);

	//
	// We must have been able to find at least one valid nodeset.
	// Otherwise this cluster could not have formed.
	//
	CL_PANIC(nodeset_found);

	//
	// Check if the highest possible version for the cluster is also the
	// highest possible version for each of the nodes.
	//
	s = version_manager::ALL_VERSIONS_MATCH;
	minimal_nodes.empty();
	version_range vr;
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (!ns.contains(n))
			continue;

		vr.empty();
		if (node_entries[n] == NULL) {
			(void) vr.insert_triad(1, 0, 0);
		} else {
			node_entries[n]->get_consistent_versions(vr);
		}

		uint16_t	node_major_num;
		uint16_t	node_minor_num;
		vr.highest_version(node_major_num, node_minor_num);
		if ((highest_uv.major_num < node_major_num) ||
		    ((highest_uv.major_num == node_major_num) &&
		    (highest_uv.minor_num < node_minor_num))) {
			//
			// This node could support a higher version than the
			// highest cluster-wide version. Set the vp_state to
			// reflect that there are mismatched versions.
			//
			s = version_manager::MISMATCHED_VERSIONS;
		} else if ((highest_uv.major_num == node_major_num) &&
		    (highest_uv.minor_num == node_minor_num)) {
			//
			// This node's vp spec file is at the minimum for
			// the highest cluster-wide version. If after we
			// look at all nodes, the vp's don't match, then
			// this node is one of the nodes preventing further
			// upgrades.
			//
			minimal_nodes.add_node(n);
		} else {
			//
			// Somehow a node can't support the highest version that
			// try_nodeset just told us all nodes could support.
			//
			ASSERT(0);
		}
	}

	//
	// If the highest possible version is greater than the current running
	// version, return true. Otherwise, return false.
	//
	VM_DBG(("coord_vp::is_upgrade_needed: rv=%u.%u, huv=%u.%u\n",
	    rv.major_num, rv.minor_num, highest_uv.major_num,
	    highest_uv.minor_num));
	if ((rv.major_num < highest_uv.major_num) ||
	    ((rv.major_num == highest_uv.major_num) &&
	    (rv.minor_num < highest_uv.minor_num))) {
		upgrade_needed = true;
	}

	if (s == version_manager::MISMATCHED_VERSIONS) {
		VM_DBG(("coord_vp::is_upgrade_needed returns "
		    "MISMATCHED_VERSIONS\n"));
	} else {
		VM_DBG(("coord_vp::is_upgrade_needed returns "
		    "ALL_VERSIONS_MATCH\n"));
	}
	return (upgrade_needed);
}

//
// Convenience accessor for node_entry::calc_potential_versions and
// get_consistent_versions
//
void
coord_vp::calc_potential_versions(version_range &vr, nodeid_t n,
	const nodeset &ns)
{
	ASSERT(the_type == VP_BTSTRP_CLUSTER || the_type == VP_CLUSTER);

	if (node_entries[n] == NULL) {
		// If the node isn't defined, return 1.0.
		vr.empty();
		(void) vr.insert_triad(1, 0, 0);
	} else {
		if (the_type == VP_BTSTRP_CLUSTER)
			node_entries[n]->calc_potential_btstrp_cl(vr, n, ns);
		else
			// We already calculated the set of potential versions
			// and put it in consistent_versions.  It doesn't
			// vary by nodeset.
			node_entries[n]->get_consistent_versions(vr);
	}
}

//
// If this is a btstrp_np vp, calculate all the running versions active
// between the node and the nodes in the set and return them in the
// version_range.
//
void
coord_vp::active_btstrp_np_rvs(version_range &vr, nodeid_t n, const nodeset &ns)
{
	version_manager::vp_version_t vpv;

	ASSERT(the_type == VP_BTSTRP_NODEPAIR);

	ASSERT(ns.contains(n));

	vr.empty();

	// Loop through the nodes.
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		// Note that we don't skip node n.  This is intentional.
		// By having each node maintain a virtual nodepair running
		// version with itself, we help ensure that nodes are always
		// compatible with other nodes that have identical version
		// spec files installed.
		//
		// If n weren't included and the nodeset ns happened to contain
		// no other node other than n, we wouldn't return any active
		// btstrp_np running versions.  The rvs are used to eliminate
		// possibilities by evaluating the dependencies, so no rvs
		// means that any btstrp_np dependency will be satisfied.
		//
		// The btstrp_np rvs of a node with itself will be the same
		// rvs as the node will have with an identical node, so by
		// returning them here we ensure that no btstrp_cl rvs will be
		// picked that prevent a node from communicating at the
		// btstrp_np level with a like-versioned peer.

		if (!ns.contains(i))
			continue;

		// Put the highest nodepair rv into vr.
		calc_highest_nodepair(n, i, vpv);
		ASSERT(vpv.major_num != 0);
		vr.insert_version(vpv);
	}
}

//
// On unreferenced, ASSERT all the node_entries are NULL (each
// would hold a reference to 'this') and that there are no reverse
// dependencies either.  Then remove ourselves from the list in the
// coordinator.
//
void
coord_vp::refcnt_unref()
{
#ifdef	DEBUG
	for (nodeid_t n = 0; n <= NODEID_MAX; n++) {
		ASSERT(node_entries[n] == NULL);
	}

	ASSERT(vp_rev_dependlist.empty());
#endif

	// We don't want to forget running versions that we have calculated,
	// so we only remove the object when it receives unreferenced when
	// the running versions aren't important.  This means we could get
	// to a zero reference state.
	//
	// The shutdown path sets the rv and uv to 0.0 and also does a hold
	// and release to ensure we wind up back in refcnt_unref, in case we
	// were at 0 references already.
	if ((rv.major_num == 0 || (rv.major_num == 1 && rv.minor_num == 0)) &&
	    (uv.major_num == 0 || (uv.major_num == 1 && uv.minor_num == 0))) {
		get_vm_coord().remove_cvp_from_list(this);

		VM_DBG(("Deleting coord_vp %s (type %d) after unref\n",
		    cvp_name, the_type));
		delete this;
	}
}
