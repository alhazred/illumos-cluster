/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _COORD_VP_H
#define	_COORD_VP_H

#pragma ident	"@(#)coord_vp.h	1.15	08/05/20 SMI"

#include <sys/list_def.h>
#include <h/version_manager.h>
#include <vm/version_range.h>
#include <vm/version_prot_obj.h>
#include <vm/versioned_protocol.h>
#include <vm/coord_cluster_info.h>
#include <sys/nodeset.h>

class vm_stream;
class vm_coord;

//
// coord_vp is a container for coord_vp::node_entry objects, and contains all
// of the information about a vp the coordinator has received from every node.
// The name and type and some other fields are stored at this level.
//

class coord_vp :
	public _SList::ListElem,
	public version_prot_obj {
public:

	//
	// coord_vp::node_entry contains the dependency entries for a coord_vp
	// cooresponding to one node.  The node is specified by the location
	// of the pointer in the coord_vp object.
	//

	class node_entry {
	public:
		node_entry(coord_vp *);
		virtual ~node_entry();

		// Unlink and delete the vp_rhs_version and vp_dependency
		// objects.
		void clear(coord_vp *);

		// Unpack the vm_stream into the object.
		void set_info(coord_vp *, vm_stream &);

		// Set the version_range to the set of versions we could
		// potentially support, given the nodeset and the fact that this
		// object corresponds to node.
		void calc_potential_btstrp_cl(version_range &, nodeid_t node,
		    const nodeset &);

		// Run the connected components algorithm on the entry.
		void connected_components_bfs(IntrList <coord_vp, _SList> &,
		    int16_t group, uint16_t &gcount);

		// Copy the consistent_versions version_range into the argument
		void get_consistent_versions(version_range &);

		// Verify (by following the dependencies) that the node
		// can support the given running version.  If the second
		// argument is true, check the upgrade version as opposed
		// to the running version.
		bool cl_check_rv(version_manager::vp_version_t &vpv, bool);

		// Direct accessor for the dependency list.
		IntrList <vp_dependency, _SList> &get_vp_dependency_list();

	private:
		// List of dependencies from the vm_stream.
		IntrList <vp_dependency, _SList> vp_dependlist;

		// All of the versions listed on the left hand side of
		// vp_dependencies.
		version_range			consistent_versions;

		coord_vp			*creator_cvp;

		node_entry(const node_entry &);
		node_entry &operator = (node_entry &);
	};

	// Constructor, always pass in the name and type
	coord_vp(const char *vname, vp_type t, vm_coord *tvmcp);

	// Destructor
	virtual ~coord_vp();

	// Return the name of the vp (from version_prot_obj)
	virtual char *get_name();

	// Return the type of the vp.  Can be VP_UNINITIALIZED
	virtual vp_type get_vp_type();

	// (Re)set the type of the vp.
	void set_vp_type(vp_type);

	// From version_prot_obj
	versioned_protocol *get_vpp();
	coord_vp *get_cvpp();

	// Calculate the highest running version supportable between nodes
	// lnode and rnode, given the current node_entries, and put
	// the result in vpv.  The type must be VP_BTSTRP_NODEPAIR.
	void calc_highest_nodepair(nodeid_t lnode, nodeid_t rnode,
	    version_manager::vp_version_t &vpv);

	// Return/set uv in/with v
	void get_uv(version_manager::vp_version_t &v);
	void set_uv(version_manager::vp_version_t &v);

	// Return/set rv in/with v
	void get_rv(version_manager::vp_version_t &v);
	void set_rv(version_manager::vp_version_t &v);

	// Return/set/calc the highest possible version the cluster supports.
	void	get_highest_version(version_manager::vp_version_t &v);
	void	set_highest_version(version_manager::vp_version_t &v);
	void	calc_highest_version(const nodeset &ns,
	    version_manager::vp_version_t &v);

	// Retrieve the info for this vp from the stream and put it into a
	// node_entry object.
	void set_node_info(nodeid_t, version_manager::vp_version_t &,
	    version_manager::vp_version_t &, uint32_t, vm_stream &);

	// Delete the info associated with node
	void clear_node(nodeid_t node);

	// Clear out entries for nodes not in nodeset.
	void clear_by_nodeset(const nodeset &, bool force = false,
	    bool need_upgrade = false);

	// Run the connected components algorithm
	uint16_t connected_components_bfs(int16_t);

	// Returns true if there is no node_entry for the specified node.
	bool is_empty(nodeid_t);

	// ListElem for the bfs algorithm
	_SList::ListElem *get_bfs_elem();

	// Returns the dependency list for the specified node.
	IntrList <vp_dependency, _SList> &get_vp_dependency_list(nodeid_t);

	//
	// Attempt to set an rv for this vp using the specified nodeset,
	// taking into account whether we are committing.  If the force flag
	// is true, compute the rv exactly.  If it is false, and we have
	// already found an rv for a nodeset that is a superset of the one
	// specified, just return true. Otherwise recompute.
	//
	// If calc_only is true, just compute the highest possible version and
	// don't set rv or uv.
	//
	// Return true if the nodeset allows a single running version, false
	// otherwise.
	//
	bool try_nodeset(const nodeset &, bool new_rv, bool use_uv,
	    bool calc_only, bool force);

	// If this is a btstrp_np vp, calculate all the running versions active
	// between the node and the nodes in the set and return them in the
	// version_range.
	void active_btstrp_np_rvs(version_range &, nodeid_t, const nodeset &);

	// Convenience accessor for node_entry::calc_potential_btstrp_cl
	void calc_potential_versions(version_range &, nodeid_t,
	    const nodeset &);

	// If a running version (or upgrade version if the second argument is
	// true) has already been calculated, find the subset of nodes defined
	// in nodeset that are compatible with it, and return it in the same
	// nodeset parameter (it is an inout).
	void cl_get_best_subset(nodeset &, bool use_uv);

	// A wrapper around node_entry::cl_check_rv to handle the 1.0 case.
	bool cl_check_node_rv(nodeid_t, bool);

	// Check whether an upgrade commit would have any effect.
	bool	is_upgrade_needed(version_manager::vp_state &, const nodeset &,
		nodeset &);

	vm_coord	&get_vm_coord();

	// Add vp info in stream for given node
	void put_stream(nodeid_t, vm_stream *);

	// Get version_range for given node id. Returns default version
	// 1.0 if no version range found for this node.
	void get_consistent_versions(nodeid_t, version_range &);

private:
	// From refcnt by way of version_prot_obj
	virtual void refcnt_unref();

	char *cvp_name;			// Unique identifier for vp
	vp_type the_type;		// Internode policy to handle
					// differences between version between
					// nodes.

	// The array of coord_vp_node objects.  If we haven't heard from a
	// node, the entry will be NULL.  Otherwise it will point to a
	// coord_vp_node object with that node's data.
	node_entry *node_entries[NODEID_MAX+1];

	//
	// The nodeset used the last time a call to try_nodeset successfully
	// calculated a set of running or upgrade versions.
	// May also contain empty().
	//
	nodeset				last_calc;

	// 0.0, the running version calculated in try_nodest, or the running
	// version reported by the other nodes, depending on what stage of
	// cluster initialization we are in.  Only used for non-nodepair vps.
	version_manager::vp_version_t	rv;

	// 0.0 or the running version calculated in try_nodeset if we are
	// in the middle of an upgrade (only used for VP_CLUSTER)
	version_manager::vp_version_t	uv;

	// Potential upgrade versions.
	version_manager::vp_version_t	highest_uv;

	vm_coord	*this_vm_coord;

	_SList::ListElem bfs_elem;

	coord_vp(const coord_vp &);
	coord_vp &operator = (coord_vp &);
};

#ifndef	NOINLINES
#include <vm/coord_vp_in.h>
#endif	// NOINLINES

#endif	/* _COORD_VP_H */
