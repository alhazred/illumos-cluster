/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VM_ADMIN_IMPL_H
#define	_VM_ADMIN_IMPL_H

#pragma ident	"@(#)vm_admin_impl.h	1.28	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

//
// The vm_admin_impl object stores, manipulates, and provides access to
// the versioned_protocol objects.  It is also the front-end of the version
// manager used by clients, as it implements the version_manager::vm_admin
// interface and is registered in the local nameserver.
//
// There is one vm_admin_impl object on each node.  It resides in the kernel.
//

#include <h/version_manager.h>
#include <h/cmm.h>
#include <orb/object/adapter.h>
#include <sys/clconf.h>
#include <sys/os.h>
#include <sys/types.h>
#include <vm/versioned_protocol.h>
#include <vm/vm_comm.h>
#include <vm/vm_lca_impl.h>
#include <vm/vm_admin_int.h>

class vm_stream;
class vp_set_key;

class vm_admin_impl : public McServerof<version_manager::vm_admin>,
	public vm_admin_int {

public:
	vm_admin_impl();
	~vm_admin_impl();

	void	_unreferenced(unref_t);

	// Interfaces from vm_admin.

	void	get_running_version(const version_manager::vp_name_t,
		    version_manager::vp_version_t &, Environment &);
	void	get_nodepair_running_version(const version_manager::vp_name_t,
		    sol::nodeid_t, sol::incarnation_num,
		    version_manager::vp_version_t &, Environment &);
	void	do_btstrp_cl_callbacks();
	bool    get_streams(version_manager::node_streams_seq_out,
		    Environment &);
	void	get_vp_list(version_manager::vp_info_seq_out,
		    Environment &);
	void	get_vp_list_by_type(version_manager::vp_info_seq_out,
		    vp_type t, Environment &);
	void	finish_upgrade_commit();
	void	register_upgrade_callbacks(
		    const version_manager::ucc_seq_t &ucc_seq,
		    version_manager::upgrade_callback_ptr cb_p,
		    const version_manager::vp_version_t &highest_version,
		    version_manager::vp_version_t &running_version,
		    Environment &e);
	void	unregister_upgrade_callbacks(
		    version_manager::upgrade_callback_ptr cb_p,
		    Environment &e);
	void	upgrade_commit(version_manager::vp_state &s,
		    membership_bitmask_t &outdated_nodes, Environment &e);
	bool	is_upgrade_needed(version_manager::vp_state &s,
		    membership_bitmask_t &outdated_nodes, Environment &e);

	// Debug hook used by query_vm.
	void	debug_request(const char *, Environment &);
	void	debug_request_io(const char *, CORBA::String_out out_string,
		    Environment &);

	// Internal post-construction initialization.
	int initialize_int(vm_stream *&);

	//
	// Generates the bootstrap cluster message.  Called during the first
	// cmm callback step from vm_comm.  If the first argument is not NULL,
	// and the message is being replaced, the old vm_stream will be
	// deleted.  True is returned if the message has been replaced, false
	// otherwise.
	//
	bool new_btstrp_cl_message(vm_stream *&);

	//
	// Generates the non-bootstrap message.  Similar to
	// new_btstrp_cl_message
	//
	bool new_cl_message(vm_stream *&);

	//
	// Set the running versions of vps with the specified type to the
	// versions listed in the vm_stream.  upgrading is used to choose the
	// version in the stream, and rnode is used to specify a nodeid if
	// the type is nodepair mode.
	//
	virtual void set_rvs(vm_stream &, vp_type type,
		    nodeid_t rnode = NODEID_UNKNOWN);

	// Return a stream of custom versions for all custom callbacks.
	vm_stream *get_cv_streams();

	// Return a stream listing all version protocol names.
	vm_stream *get_all_vp_streams();

	//
	// Returns the value of the change flag.
	//
	bool get_change_flag();

#ifdef DEBUGGER_PRINT
	int mdb_dump_vps(const char*);
#endif

private:
	//
	// Run local consistency on a connected subgroup, as indicated by a
	// vp_set_key.
	//
	bool local_consistency(vp_set_key &);

	//
	// Put entries for the versioned_protocols of the specified type
	// into a new vm_stream and return it.
	//
	vm_stream *get_stream_by_type(vp_type);

	//
	// The lock functions actually use the mutex in vm_comm.  The lock
	// protects the variables below (other than the_vm_admin_impl).
	//
	void lock();
	void unlock();
	int lock_held();

	// Don't copy or assign.
	vm_admin_impl(const vm_admin_impl &);
	vm_admin_impl &operator = (vm_admin_impl &);
};

#ifndef	NOINLINES
#include <vm/vm_admin_impl_in.h>
#endif	// NOINLINES

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _VM_ADMIN_IMPL_H */
