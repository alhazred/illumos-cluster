/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VP_INTERFACE_H
#define	_VP_INTERFACE_H

#pragma ident	"@(#)vp_interface.h	1.10	08/05/20 SMI"

#include <vm/vp_triad.h>
#include <vm/version_range.h>
#include <vm/vp_rhs_version.h>
#include <vm/vp_dependency.h>
#include <vm/vp_upgrade.h>
#include <vm/versioned_protocol.h>
#include <vm/vp_error.h>

vp_error_t process_vp_file(versioned_protocol **vpp, char *filename);

/*
 * The interface header file so the rules in vm_yy.tab.cc (generated from
 * vp_gram.y by yacc) can create the appropriate C++ classes to be used in
 * the local consistency algorithm.  It may be possible, with a little work,
 * to do these operations directly inside vp_gram.y, but this is probably
 * cleaner anyway.
 */
extern "C" {
	struct vp_yy_triad {
		uint16_t major_num;
		uint16_t minor_min;
		uint16_t minor_max;
		char *description;
	};
	/* Constructors */
	void *new_version_range(struct vp_yy_triad *);
	void *new_vp_rhs_version(void *, void *);
	void *new_vp_dependency(void *);
	void *new_vp_upgrade(void *, void *);

	/* Triads */
	void insert_triad(void *vrange_p, struct vp_yy_triad *triadp);

	/* Version line */
	void assign_lhs(void *vpd_p, void *lhs_v_p);
	void insert_rhs(void *vpd_p, void *rhs_v_p);
	void insert_vp_dependency(void *vpp, void *vpd);

	/* Upgrade line */
	void insert_vp_upgrade(void *vpp, void *vpu);

	/* Versioned protocol */
	void assign_vp(void *vpp, int ffv, char *name, char *desc,
	    char *src, char *ccb, vp_type t, int attr);
}

#endif	/* _VP_INTERFACE_H */
