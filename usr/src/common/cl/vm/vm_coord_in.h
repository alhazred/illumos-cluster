/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VM_COORD_IN_H
#define	_VM_COORD_IN_H

#pragma ident	"@(#)vm_coord_in.h	1.12	08/05/20 SMI"

inline nodeid_t
vm_coord::local_nodeid()
{
	return (nodeid);
}

inline uint32_t
vm_coord::current_node_votes(nodeid_t n)
{
	return (cci.current_node_votes(n));
}

#endif	/* _VM_COORD_IN_H */
