//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)vm_stream.cc 1.13     08/05/20 SMI"

#include <vm/vm_stream.h>
#include <vm/versioned_protocol.h>
#include <vm/vp_dependency.h>
#include <vm/vp_rhs_version.h>

vm_stream::~vm_stream()
{
	free_buffer();
	ASSERT(_buffer == NULL);
}

//
// Reinitialize the buffer and pointers with an external string.
//
// If no_copy is true, we set _buffer to s without copying.  The
// caller is responsible for ensuring that the data are not corrupted
// while the vm_stream is in use.
//
// If dbwr is false, we set delete_buffer_when_replacing to false
// and in the future we will not delete _buffer.  The caller is responsible.
// for disposing of s at the appropriate time.  If dbwr is true, we will
// delete the buffer (with '[]').
//
// If dbwr is set to false, the vm_stream implementation will also avoid
// altering the buffer.
//

void
vm_stream::reinit(size_t l, char *s, bool no_copy, bool dbwr)
{
	ASSERT(l > sizeof (vm_stream_header));
	ASSERT(no_copy || !dbwr);

	// Get rid of the old buffer
	free_buffer();

	// Reset the other variables
	expected_vps = current_vp = 0;
	read_offset = sizeof (vm_stream_header);
	write_offset = _size = l;
	delete_buffer_when_replacing = dbwr;

	if (no_copy) {
		_buffer = s;
	} else {
		_buffer = new char[l];
		ASSERT(_buffer);
		bcopy(s, _buffer, l);
	}

	ASSERT(get_stream_type());
}

void
vm_stream::put_versioned_protocol(versioned_protocol *vpp)
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	if (vpp == NULL) {
		put_string(NULL);
		return;
	}

	vpp->rebuild_potential_versions();
	ASSERT(!vpp->get_potential_versions().is_empty());

	version_manager::vp_version_t rv, uv;
	rv.major_num = rv.minor_num = uv.major_num = uv.minor_num = 0;

	if (vpp->get_vp_type() != VP_BTSTRP_NODEPAIR) {
		(void) vpp->get_running_version(rv);
		vpp->get_upgrade_version(uv);
	}

	put_header(vpp->get_name(), (uint16_t)vpp->get_vp_type(), rv, uv);

	vp_dependency *vpd;

	IntrList<vp_dependency, _SList>::ListIterator
	    depi(vpp->get_vp_dependency_list());

	// Add each dependency.  An actual entry will only be made if
	// the left-hand side of the dependency intersects with
	// potential_versions
	while ((vpd = depi.get_current()) != NULL) {
		intersect_and_put_vp_dependency(vpd,
		    vpp->get_potential_versions());
		depi.advance();
	}

	// Terminate vp_dependency list
	intersect_and_put_vp_dependency(NULL, vpp->get_potential_versions());
}

void
vm_stream::reserve_for_write(size_t needed, bool exactly)
{
	size_t addto = 0, growby;

	if (get_stream_type() == VP_STREAM_1) {
		growby = vp_stream_growby;
	} else {
		ASSERT(get_stream_type() == RV_STREAM_1);
		growby = rv_stream_growby;
	}

	if (_size - write_offset >= needed)
		return;

	// Reserve extra room to avoid frequent resizes
	if (exactly) {
		addto = needed;
	} else if ((expected_vps - current_vp) > 0) {
		addto = (expected_vps - current_vp) * growby;
	}

	if (addto < needed)
		addto = needed + growby;

	_size = write_offset + addto;

	char *tmpchr = new char[_size];
	ASSERT(tmpchr);

	if (_buffer != NULL) {
		if (write_offset != 0) {
			bcopy(_buffer, tmpchr, write_offset);	//lint !e669
		}
		free_buffer();
	}

	ASSERT(_buffer == NULL);
	_buffer = tmpchr;
	delete_buffer_when_replacing = true;
}

void
vm_stream::put_string(const char *s)
{
	size_t slen;

	if (s == NULL)
		slen = 0;
	else
		slen = os::strlen(s);

	reserve_for_write(slen + 1);

	if (slen) {
		ASSERT(s != NULL);
		bcopy(s, write_point(), slen + 1);
	} else
		*(write_point()) = 0;

	advance_write_point(slen + 1);
}

char *
vm_stream::get_string_ref()
{
	char *retval = read_point();

	size_t slen = os::strlen(retval);

	// XXX if security ever becomes an issue, this might have to
	// be changed to something that helps strip out bad messages.
	// As it is, this should prevent buffer overflows (by bringing
	// down the node).
	CL_PANIC(read_offset + slen < write_offset);

	advance_read_point(slen + 1);

	ASSERT(write_offset >= read_offset);

	if (slen == 0)
		return (NULL);
	else
		return (retval);
}

void
vm_stream::put_header(const char *vp_name, uint16_t vt,
	version_manager::vp_version_t &rv, version_manager::vp_version_t &uv)
{
	current_vp++;

	put_string(vp_name);

	reserve_for_write(sizeof (vt) + sizeof (rv) + sizeof (uv));

	bcopy(&vt, write_point(), sizeof (vt));
	advance_write_point(sizeof (vt));

	bcopy(&rv, write_point(), sizeof (rv));
	advance_write_point(sizeof (rv));

	bcopy(&uv, write_point(), sizeof (uv));
	advance_write_point(sizeof (uv));
}

char *
vm_stream::get_header(uint16_t &vt, version_manager::vp_version_t &rv,
	version_manager::vp_version_t &uv)
{
	char *retval = get_string_ref();

	if (retval == NULL) {
		// No more headers
		return (NULL);
	}

	ASSERT((write_offset - read_offset) >= (sizeof (vt) + sizeof (rv) +
	    sizeof (uv)));

	bcopy(read_point(), &vt, sizeof (vt));
	advance_read_point(sizeof (vt));

	bcopy(read_point(), &rv, sizeof (rv));
	advance_read_point(sizeof (rv));

	bcopy(read_point(), &uv, sizeof (uv));
	advance_read_point(sizeof (uv));

	return (retval);
}

void
vm_stream::intersect_and_put_vp_dependency(vp_dependency *vpd,
	version_range &vr)
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	version_range tmp_vr;

	if (vpd == NULL) {
		put_version_range(tmp_vr);
		return;
	}

	if (vpd->is_bad()) {
		return;
	}

	vr.intersection(vpd->get_lhs(), tmp_vr);

	if (tmp_vr.is_empty()) {
		// Don't need to add anything for this vp_dependency
		return;
	}

	put_version_range(tmp_vr);

	vp_rhs_version *vrhs;
	IntrList<vp_rhs_version, _SList>::ListIterator
	    rhsi(vpd->get_rhs_list());

	while ((vrhs = rhsi.get_current()) != NULL) {
		// Add each relevant vp_rhs_version
		put_vp_rhs_version(vrhs);
		rhsi.advance();
	}

	// Terminate the list
	put_vp_rhs_version(NULL);
}

vp_dependency *
vm_stream::get_vp_dependency()
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	version_range tmp_vr;

	get_version_range(tmp_vr);

	if (tmp_vr.is_empty()) {
		return (NULL);
	}

	vp_dependency *retval = new vp_dependency();

	retval->swap_into_lhs(tmp_vr);

	vp_rhs_version *vrhs;

	while ((vrhs = get_vp_rhs_version()) != NULL) {
		retval->get_rhs_list().append(vrhs);
	}

	return (retval);
}


void
vm_stream::put_vp_rhs_version(vp_rhs_version *vrhs)
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	if (vrhs == NULL) {
		put_string(NULL);
		return;
	}

	if (vrhs->get_vpop() == NULL) {
		// This was a good dependency, but the vpop is missing, which
		// means we have no entry for this vp on the machine.  ASSERT
		// that the vr contains 1.0 and skip the entry.
#ifdef  DEBUG
		version_manager::vp_version_t vpv;
		vpv.major_num = 1;
		vpv.minor_num = 0;
		ASSERT(vrhs->get_vr().contains(vpv));
#endif
		return;
	}

	if (vrhs->get_vpop()->get_vp_type() == VP_BTSTRP_NODE) {
		// Don't advertise node-mode vps off node
		return;
	}

	put_string(vrhs->get_vpop()->get_name());

	put_version_range(vrhs->get_vr());
}

vp_rhs_version *
vm_stream::get_vp_rhs_version()
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	char *vname = get_string_ref();

	if (vname == NULL)
		return (NULL);

	// Copy the reference into its own string
	vname = os::strdup(vname);
	ASSERT(vname);

	version_range tmp_vr;

	get_version_range(tmp_vr);
	ASSERT(!tmp_vr.is_empty());

	return (new vp_rhs_version(vname, tmp_vr));
}

void
vm_stream::put_version_range(version_range &vr)
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	size_t length_in_stream = vp_triad_version_size * (vr.get_count() + 1);

	reserve_for_write(length_in_stream);

	if (vr.get_count() > 0) {
		for (uint16_t f = 0; f < vr.get_count(); f++) {
			bcopy(vr.triad_array[f].get_triad_version(),
			    write_point(), vp_triad_version_size);
			advance_write_point(vp_triad_version_size);
		}
	}

	bzero(write_point(), vp_triad_version_size);
	advance_write_point(vp_triad_version_size);
}

void
vm_stream::get_version_range(version_range &vr)
{
	ASSERT(get_stream_type() == VP_STREAM_1);

	vp_triad t;

	vr.empty();

	do {
		bcopy(read_point(), t.get_triad_version(),
		    vp_triad_version_size);

		if (t.get_major() != 0)
			(void) vr.insert_triad(t);

		advance_read_point(vp_triad_version_size);
	} while (t.get_major() != 0);
}
