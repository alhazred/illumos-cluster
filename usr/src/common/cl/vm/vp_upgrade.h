/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_UPGRADE_H
#define	_VP_UPGRADE_H

#pragma ident	"@(#)vp_upgrade.h	1.9	08/05/20 SMI"

#include <sys/os.h>
#include <sys/list_def.h>
#include <vm/version_range.h>
#include <vm/vp_error.h>

//
// Version Manager vp_upgrade Class Definition
//
// The vp_upgrade object is used to ensure that an upgrade from one version to
// another is supported.
//
// The vp_upgrade object holds two version_range objects.  The version
// range on the lefthand side holds those particular versions that can only
// be upgraded to the versions that are contained in the version_range on the
// righthand side.
//
class vp_upgrade : public _SList::ListElem {
public:
	// Constructor
	vp_upgrade();

	// Constructor
	vp_upgrade(version_range &lhs, version_range &rhs);

	// Destructor
	~vp_upgrade();

	// Return the lefthand side
	version_range &get_lhs();

	// Return the righthand side
	version_range &get_rhs();

private:
	version_range left;	// The lefthand side
	version_range right;	// The righthand side

	vp_upgrade(const vp_upgrade &);
	vp_upgrade &operator = (vp_upgrade &);
};

#ifndef	NOINLINES
#include <vm/vp_upgrade_in.h>
#endif	// NOINLINES

#endif	/* _VP_UPGRADE_H */
