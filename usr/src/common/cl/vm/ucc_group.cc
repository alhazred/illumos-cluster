//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)ucc_group.cc 1.3     08/05/20 SMI"

#include	<vm/ucc_group.h>
#include	<vm/vm_admin_impl.h>
#include	<repl/repl_mgr/repl_mgr_debug.h>
//
// ucc_group.cc implements the container objects needed to upgrade groups of
// ucc concurrently.  All ucc's in ucc_list must be upgraded concurrently.
//

// Class ucc_group
ucc_group::ucc_group() : _SList::ListElem(this)
{
	size = 0;
	root = true;
	index = 0;
	visited = false;
}

// Constructor with a single ucc in its ucc_list and all the freeze information.
ucc_group::ucc_group(version_manager::ucc *u) : _SList::ListElem(this)
{
	uint_t i;
	char *serv = NULL;

	size = 1;
	root = true;
	index = 0;
	visited = false;

	ucc_list.append(u);
	// Add all of u's HA services to the freeze list
	for (i = 0; i < u->freeze.length(); i++) {
		serv = (char *)u->freeze[i];
		freeze_list.add(serv, serv);
	}
}

ucc_group::~ucc_group()
{
	version_manager::ucc * cur;
	ucc_group **	ug;

	// Remove ucc_list; however, do not delete the actual ucc's themselves
	// as they are housed in the ucc_hashtable.
	cur = ucc_list.reapfirst();
	while (cur != NULL) {
		cur = ucc_list.reapfirst();
	}
	// Remove before_ugs and also remove the pointer elements themselves.
	while ((ug = before_ugs.reapfirst()) != NULL) {
		delete ug;
	}
	// Remove freeze
	freeze_list.dispose();
	// Remove thaw
	thaw_list.dispose();
}

//
// First, increase the indices of each ucc_group inside the Before subtree.
// However, leave 'this' ucc_group's index value unmodified.
//
void
ucc_group::incr_subtree_indices(IntrList<ucc_group, _SList> & uglist,
    uint_t amount)
{
	ucc_group	*cur_ug = NULL;
	ucc_group	*cand_ug = NULL;
	IntrList<ucc_group, _SList>	unmod_ug_list;
	bool	check_erase;
	uint_t	orig_size = uglist.count();
	uint_t	new_depth = 0;

	//
	// Mark everything in the uglist as not visited.  We're going to use
	// the visited field to determine if we've increased the indices before.
	//
	uglist.atfirst();
	while ((cur_ug = uglist.get_current()) != NULL) {
		uglist.advance();
		cur_ug->set_visited(false);
	}
	// Seed the unmodified_ug_list with each of the before ucc_groups.
	atfirst_bef_ug();
	while ((cur_ug = get_next_bef_ug()) != NULL) {
		check_erase = uglist.erase(cur_ug);
		ASSERT(check_erase);
		unmod_ug_list.append(cur_ug);
	}
	unmod_ug_list.atfirst();
	//
	// Start the traversal.
	// Increase each ucc_group inside the before subtree exactly once.
	//
	while ((cur_ug = unmod_ug_list.get_current()) != NULL) {
		unmod_ug_list.advance();
		ASSERT(!cur_ug->get_visited());
		//
		// Increase this ucc_group's index and adjust the maximum depth
		// if necessary.
		//
		cur_ug->set_visited(true);
		new_depth = cur_ug->get_index() + amount;
		cur_ug->set_index(new_depth);
		// Append any unseen before ucc_groups to the unmodified list.
		cur_ug->atfirst_bef_ug();
		while ((cand_ug = cur_ug->get_next_bef_ug()) != NULL) {
			check_erase = uglist.erase(cand_ug);
			//
			// If we can remove the candidate ucc_group successfully
			// from the uglist, then its index has not been
			// increased yet so append it so it can be processed.
			//
			if (check_erase)
				unmod_ug_list.append(cand_ug);
		}
	}
	// Restore the uglist after the traversal is finished.
	uglist.concat(unmod_ug_list);
	ASSERT(unmod_ug_list.empty());
	ASSERT(uglist.count() == orig_size);
}

// Returns true if the ucc_group this contains the ucc u.
bool
ucc_group::contains_ucc(version_manager::ucc *u)
{
	version_manager::ucc * cur;

	ASSERT(u);
	ucc_list.atfirst();
	while ((cur = ucc_list.get_current()) != NULL) {
		if (strcmp((const char *)u->ucc_name,
		    (const char *)cur->ucc_name) == 0)
			return (true);
		ucc_list.advance();
	}
	return (false);
}

// If the edge is not already added, add a 'before' edge to our ucc_group.
// The edge's presence signals we need to upgrade 'child' before 'this'.
void
ucc_group::add_before_edge(ucc_group * child)
{
	ucc_group **	cand_ug = NULL;
	ucc_group **	ugp = NULL;

	ASSERT(child);
	// The child is not a root as 'this' now depends on it.
	child->set_root(false);

	// First check to make sure that this edge has not been previously
	// added.
	before_ugs.atfirst();
	while ((cand_ug = before_ugs.get_current()) != NULL) {
		// This edge from 'this' to 'child'  has been created already
		// so do a no-op.
		if (*cand_ug == child)
			return;
		before_ugs.advance();
	}
	// Add the edge.
	ugp = new ucc_group *;
	ASSERT(ugp);
	*ugp = child;
	before_ugs.append(ugp);
}

// If the edge exists to the ucc_group 'to_be_removed', then remove it and
// return true.  Otherwise, return false.
bool
ucc_group::remove_before_edge(ucc_group * to_be_removed)
{
	ucc_group **	cand_ug = NULL;
	bool	check_erase;

	ASSERT(to_be_removed);
	// Search the before_ugs for to-be-removed ucc.
	before_ugs.atfirst();
	while ((cand_ug = before_ugs.get_current()) != NULL) {
		before_ugs.advance();
		if (*cand_ug == to_be_removed) {
			// Remove it and return true.
			check_erase = before_ugs.erase(cand_ug);
			ASSERT(check_erase);
			delete cand_ug;
			return (true);
		}
	}
	// We searched the entire before_ugs but did not find the ucc_group so
	// return false.
	return (false);
}

// Get the next ucc_group in this ucc_group's before list and then
// advance the current pointer in before_ugs
ucc_group *
ucc_group::get_next_bef_ug()
{
	ucc_group ** ug_p = NULL;
	ug_p = before_ugs.get_current();
	if (ug_p == NULL) {
		return (NULL);
	} else {
		before_ugs.advance();
		return (*ug_p);
	}
}

// Combine the information from ucc A into ucc 'this'
void
ucc_group::combine_groups(ucc_group * A)
{
	ucc_group	** cand_ug = NULL;
	version_manager::ucc	* con_ucc = NULL;
	char 	*freeze_entry = NULL;

	ASSERT(A);
	// Return if we're trying to combine something with itself
	if (this == A)
		return;

	// Add all of A's before edges to 'this'.  add_before_edge() will
	// avoid adding duplicate entries.
	while ((cand_ug = A->before_ugs.reapfirst()) != NULL) {
		(*cand_ug)->atfirst_concurrent();
		add_before_edge(*cand_ug);
	}
	// Add all A's concurrent ucc's to 'this'.  We'll avoid adding duplicate
	// entries.
	while ((con_ucc = A->ucc_list.reapfirst()) != NULL) {
		if (!contains_ucc(con_ucc)) {
			add_ucc(con_ucc);
		}
	}
	// Add all freeze entries of A's to 'this'.  We'll avoid adding
	// duplicate entries.
	string_hashtable_t<char *>::iterator frz_iter(A->freeze_list);
	while ((freeze_entry = (char *)frz_iter.get_current()) != NULL) {
		frz_iter.advance();
		if (!in_freeze_list(freeze_entry)) {
			// Add freeze_entry to 'this'.
			freeze_list.add(freeze_entry, freeze_entry);
		}
	}
}

// Returns true if the service is part of the 'freeze' hashtable
bool
ucc_group::in_freeze_list(char *name)
{
	char *cand = NULL;
	cand = freeze_list.lookup(name);
	if (cand != NULL)
		return (true);
	else
		return (false);
}

// Simply return the next concurrent ucc in the list and nothing more.
version_manager::ucc *
ucc_group::get_next_concurrent()
{
	version_manager::ucc * cur_ug = NULL;
	cur_ug = ucc_list.get_current();
	ucc_list.advance();
	if (cur_ug == NULL)
		return (NULL);
	else
		return (cur_ug);
}

// Get the current ucc in ucc_list and its associated versioned protocol and the
// version that we're upgrading too.
version_manager::ucc *
ucc_group::get_next_con_ucc(version_manager::vp_version_t & new_rv)
{
	version_manager::ucc * next_ucc = NULL;

	next_ucc = ucc_list.get_current();
	ucc_list.advance();
	if (next_ucc != NULL) {
		// Find the versioned protocol via the version manager.
		// All vps should be on every node since these are cluster
		// mode vps and there is currently an upgrade commit occurring.
		vm_comm::the().lock();
		versioned_protocol *vpp =
		    vm_comm::the_vm_admin_impl().get_vp_by_name(
		    next_ucc->vp_name);
		ASSERT(vpp);
		// Ask the vm to return the version that we're upgrading too.
		(void) vpp->get_running_version(new_rv);
		vm_comm::the().unlock();
	}
	return (next_ucc);
}

// Add a HA service to the thaw hashtable.  If the service is already in the
// hashtable, then this is a no-op.
void
ucc_group::add_thaw(char *serv)
{
	char *found = NULL;
	found = thaw_list.lookup(serv);
	if (found == NULL)
		// Add this service
		thaw_list.add(serv, serv);
}

// Freeze all the following services, if the service is not already frozen.
void
ucc_group::freeze_services()
{
	VM_DBG(("Freezing the HA services.\n"));
	char *service_name;
	string_hashtable_t<char *>::iterator frz_iter(freeze_list);

	replica::rm_admin_var rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

	Environment e;
	while ((service_name = frz_iter.get_current()) != NULL) {
		VM_DBG(("Freezing service: %s\n", service_name));
		rm_ref->freeze_for_upgrade(service_name, e);
		if (e.exception()) {
			//
			// The only possible exception is unknown service, since
			// the VM and RM primaries are co-located. An unknown
			// service exception is valid since the service might
			// have died or shutdown, but the uccs are still
			// present. Just ignore the error.
			//
			ASSERT(replica::unknown_service::
			    _exnarrow(e.exception()));
			VM_DBG(("Service %s was not frozen. "
			    "unknown_service exception returned.\n",
			    service_name));
			e.clear();
		}
		frz_iter.advance();
	}
}

// Thaw all the follwing services.
void
ucc_group::thaw_services()
{
	VM_DBG(("Thawing the HA services.\n"));
	char *service_name;
	string_hashtable_t<char *>::iterator thaw_iter(thaw_list);

	replica::rm_admin_var rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

	Environment e;
	while ((service_name = thaw_iter.get_current()) != NULL) {
		VM_DBG(("Thawing service: %s\n", service_name));
		rm_ref->unfreeze_after_upgrade(service_name, e);
		if (e.exception()) {
			//
			// There are two possible exceptions:
			//   dependent_service_frozen
			//   unknown_service
			//
			// unknown_service is valid as in freeze_services above.
			//
			// dependent_service_frozen means that the code to
			// generate the ucc_group lists has an error, since a
			// dependent service should have been unfrozen first.
			//
			CL_PANIC(replica::unknown_service::
			    _exnarrow(e.exception()));
			VM_DBG(("Service %s was not unfrozen. "
			    "unknown_service exception returned.\n",
			    service_name));
			e.clear();
		}
		thaw_iter.advance();
	}
}
