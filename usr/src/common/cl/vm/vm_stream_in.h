/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VM_STREAM_IN_H
#define	_VM_STREAM_IN_H

#pragma ident	"@(#)vm_stream_in.h	1.14	08/05/20 SMI"

inline
vm_stream::vm_stream() :
	delete_buffer_when_replacing(false),
	expected_vps(0),
	current_vp(0),
	_buffer(NULL),
	write_offset(0),
	read_offset(0),
	_size(0)
{
}

inline
vm_stream::vm_stream(stream_type t, uint16_t ex_vps, cmm::seqnum_t rv_seq,
    cmm::seqnum_t uv_seq) :
	delete_buffer_when_replacing(true),
	expected_vps(ex_vps),
	current_vp(0),
	write_offset(sizeof (vm_stream_header)),
	read_offset(sizeof (vm_stream_header))
{
	if (t == VP_STREAM_1) {
		_size = read_offset + ex_vps * vp_stream_growby;
	} else {
		ASSERT(t == RV_STREAM_1);
		_size = read_offset + ex_vps * rv_stream_growby;
	}

	vm_stream_header vmsh;

	_buffer = new char[_size];
	ASSERT(_buffer);

	vmsh.the_type = (uint16_t)t;
	vmsh.rv_seqnum = rv_seq;
	vmsh.uv_seqnum = uv_seq;
	bcopy(&vmsh, _buffer, sizeof (vm_stream_header));
}

inline bool
vm_stream::valid_stream(size_t l, char *s)
{
	uint16_t the_type = NULL;

	if (l < sizeof (vm_stream_header))
		return (false);

	vm_stream_header *vmshp = (vm_stream_header *)s;
	bcopy(&(vmshp->the_type), &the_type, sizeof (vmshp->the_type));

	if (the_type != VP_STREAM_1 && the_type != RV_STREAM_1)
		return (false);

	return (true);
}

inline uint16_t
vm_stream::get_stream_type()
{
	uint16_t the_type;

	ASSERT(write_offset >= sizeof (vm_stream_header));

	vm_stream_header *vmshp = (vm_stream_header *)_buffer;
	bcopy(&(vmshp->the_type), &the_type, sizeof (vmshp->the_type));

	ASSERT(the_type == VP_STREAM_1 || the_type == RV_STREAM_1);

	return (the_type);
}

inline cmm::seqnum_t
vm_stream::get_rv_seqnum()
{
	cmm::seqnum_t rv_seq = 0;

	ASSERT(write_offset >= sizeof (vm_stream_header));

	vm_stream_header *vmshp = (vm_stream_header *)_buffer;
	bcopy(&(vmshp->rv_seqnum), &rv_seq, sizeof (vmshp->rv_seqnum));

	return (rv_seq);
}

inline cmm::seqnum_t
vm_stream::get_uv_seqnum()
{
	cmm::seqnum_t uv_seq = 0;

	ASSERT(write_offset >= sizeof (vm_stream_header));

	vm_stream_header *vmshp = (vm_stream_header *)_buffer;
	bcopy(&(vmshp->uv_seqnum), &uv_seq, sizeof (vmshp->uv_seqnum));

	return (uv_seq);
}

inline char *
vm_stream::write_point()
{
	ASSERT(write_offset < _size);
	return (_buffer + write_offset);
}

inline void
vm_stream::advance_write_point(size_t add_to)
{
	ASSERT(write_offset + add_to <= _size);
	write_offset += add_to;
}

inline char *
vm_stream::read_point()
{
	ASSERT(read_offset < write_offset);
	return (_buffer + read_offset);
}

inline void
vm_stream::advance_read_point(size_t add_to)
{
	ASSERT(read_offset + add_to <= write_offset);
	read_offset += add_to;
}

inline void
vm_stream::read_from_beginning()
{
	read_offset = sizeof (vm_stream_header);
}

inline size_t
vm_stream::written_length()
{
	return (write_offset);
}

inline void
vm_stream::free_buffer()
{
	if (delete_buffer_when_replacing)
		delete [] _buffer;
	_buffer = NULL;
	delete_buffer_when_replacing = false;
}

#endif	/* _VM_STREAM_IN_H */
