/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_STREAM_H
#define	_VM_STREAM_H

#pragma ident	"@(#)vm_stream.h	1.14	08/05/20 SMI"

//
// vm_stream is just a sugar coating around a character array.  Pieces of
// records of versioned_protocols can be added to it with the put_ functions
// and retrieved with the get_ functions.
//
// XXX describe message format here.
//

#include <h/version_manager.h>
#include <h/cmm.h>

class versioned_protocol;
class vp_dependency;
class vp_rhs_version;
class version_range;

typedef struct vm_stream_header_t {
	uint16_t the_type;
	cmm::seqnum_t	rv_seqnum;
	cmm::seqnum_t	uv_seqnum;
} vm_stream_header;

class vm_stream {
// vm_comm and vm_admin_impl needs special access
friend class vm_comm;
friend class vm_admin_impl;

public:
	enum stream_type {
		UNINITIALIZED_STREAM = 0,
		VP_STREAM_1 = 1,
		RV_STREAM_1 = 2
	};

	vm_stream();
	vm_stream(stream_type, uint16_t ex_vps, cmm::seqnum_t rv_seq,
	    cmm::seqnum_t uv_seq);
	~vm_stream();

	// Return the type of this stream
	uint16_t get_stream_type();

	// Accessor functions for the sequence numbers in the stream header
	cmm::seqnum_t	get_rv_seqnum();
	cmm::seqnum_t	get_uv_seqnum();

	// Returns true if the stream represented by the arguments is
	// valid (by examining the header information)
	static bool valid_stream(size_t l, char *s);

	// Add a versioned protocol to a VP_STREAM
	void put_versioned_protocol(versioned_protocol *);

	// Get a vp_dependency from a VP_STREAM
	vp_dependency *get_vp_dependency();

	// Retrieve a versioned_protocol header from the stream.  The
	// return value is the name of the versioned protocol, and is a
	// pointer into the string inside the buffer.
	char *get_header(uint16_t &, version_manager::vp_version_t &,
	    version_manager::vp_version_t &);

	// Add a running version header to the stream.  The name and type
	// of the vp and the current and upgrade running versions are specified.
	void put_header(const char *, uint16_t,
	    version_manager::vp_version_t &, version_manager::vp_version_t &);

	// Reset the read point to the beginning of the message.
	void read_from_beginning();

	// Replace the current buffer with the character string
	void reinit(size_t, char *, bool no_copy = false, bool dbwr = false);

	// put/get a version_range to/from a VP_STREAM
	void get_version_range(version_range &);
	void put_version_range(version_range &);

	void put_vp_rhs_version(vp_rhs_version *);

#ifdef DEBUGGER_PRINT
	int mdb_dump_stream(stream_type);
	char *mdb_get_header(uint16_t &, version_manager::vp_version_t &,
	    version_manager::vp_version_t &);
	int mdb_retreive_triads();
	char *mdb_get_string_ref();
	int mdb_get_dependency();
	char *get_buffer();
	size_t get_size();
#endif

private:
	// Make sure there is enough space after the write pointer to write
	// a message of size s.  If exactly is true, add no more
	// space past s if space needs to be added.
	void reserve_for_write(size_t s, bool exactly = false);

	// Return a pointer to the current write point.
	char *write_point();

	// Advance the write point by the specified amount.
	void advance_write_point(size_t);

	// Return a pointer to the read point
	char *read_point();

	// Advance the read point by the specified amount.
	void advance_read_point(size_t);

	// return the total length of the written message.
	size_t written_length();

	// Insert the specified string into the stream and advance the
	// write point past the end of the string.
	void put_string(const char *);

	// Return a pointer to the current string in the buffer and advance
	// the read point.  The caller must copy the string if they need it
	// past the destruction or reinitialization of the vm_stream.
	char *get_string_ref();

	// Intersect the lhs of the vp_dependency with the version range and
	// add it if the result isn't empty.  If vp_dependency is NULL, add
	// a terminator.  Only works on VP_STREAMs
	void intersect_and_put_vp_dependency(vp_dependency *, version_range &);

	// put and get a vp_rhs_version to/from a VP_STREAM
	vp_rhs_version *get_vp_rhs_version();

	// Free up the buffer, if there is one.
	void free_buffer();

	// If true, the buffer will be deleted when it is freed or replaced.
	bool		delete_buffer_when_replacing;

	// expected_vps is the number of entries we expect in the stream,
	// current_vps is the number entered so far.
	uint16_t	expected_vps, current_vp;

	// A pointer to the character array
	char 		*_buffer;

	// New entries are added starting at _buffer + write_offset.  They
	// are read from _buffer + read_offset.  _size is the length of the
	// current _buffer
	size_t		write_offset, read_offset, _size;
};

// How much per vp to grow a VP_STREAM
const int vp_stream_growby = 100;
// How much per rv to grow an RV_STREAM
const int rv_stream_growby = 30;

#ifndef	NOINLINES
#include <vm/vm_stream_in.h>
#endif	// NOINLINES

#endif	/* _VM_STREAM_H */
