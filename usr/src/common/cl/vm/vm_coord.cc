/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vm_coord.cc	1.34	08/05/20 SMI"

#include <h/version_manager.h>
#include <vm/vm_coord.h>
#include <vm/coord_vp.h>
#include <orb/infrastructure/orb_conf.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vp_dependency.h>
#include <vm/vp_rhs_version.h>
#include <vm/vm_stream.h>
#include <vm/vp_set_key.h>
#include <vm/vp_set.h>
#include <vm/vp_constraint.h>
#include <vm/vm_admin_int.h>
#ifndef LIBVM
#include <vm/vm_comm.h>
#include <vm/vm_admin_impl.h>
#endif

// XXX syslog
// The syslogs that occur when nodes are being shut down (one below, one
// in vm_comm) don't give specific information about what vps were incompatible.
// This isn't always an easy question to answer.  If more specific syslogs are
// desired, code will have to be added to the sub-functions called from
// calculate_btstrp_cl and calculate_cl.
#ifdef LIBVM
vm_coord::vm_coord(nodeid_t this_nodeid, uint32_t needed_votes,
	vm_admin_int *amd_int) :
	cci(needed_votes),
	highest_set_key(0),
	cl_set_keys(NULL),
	nodeid(this_nodeid)
{
	for (int j = 0; j < VP_TYPE_ARRAY_SIZE; j++) {
		cvp_typecount[j] = 0;
	}
	the_vm_admin_int = amd_int;
}
#else
vm_coord::vm_coord() :
	highest_set_key(0),
	cl_set_keys(NULL),
	nodeid(orb_conf::local_nodeid())
{
	for (int j = 0; j < VP_TYPE_ARRAY_SIZE; j++) {
		cvp_typecount[j] = 0;
	}
}
#endif

vm_coord::~vm_coord()
{
	ASSERT(cl_set_keys == NULL);
	nodeset ns;

	// Clear the information corresponding to each node from each coord_vp.
	for (int i = 0; i < VP_TYPE_ARRAY_SIZE; i++) {
		clear_list_not_in_nodeset(ns, (vp_type)i, true, true);
	}
} //lint !e1740

//
// Add entries for the vps specified in the stream
//
bool
vm_coord::decode_vm_stream(nodeid_t rnode, vp_type the_type, vm_stream &vms,
	uint32_t rva)
{
	uint16_t u16_type;
	version_manager::vp_version_t rv, uv;
	coord_vp *cvp;
	char *vpn;

	vms.read_from_beginning();

	while ((vpn = vms.get_header(u16_type, rv, uv)) != NULL) {
		VM_DBG(("%s (%u.%u) (%u.%u)\n", vpn,
		    rv.major_num, rv.minor_num,
		    uv.major_num, uv.minor_num));
		ASSERT(the_type == (vp_type)u16_type);

#ifdef LIBVM
		//
		// Discard vps not known by local vm_admin.
		//
		if (the_vm_admin_int->get_vp_by_name(vpn) == NULL) {

			//
			// Skip stream data for current vp
			//
			vp_dependency *vpd;
			while ((vpd = vms.get_vp_dependency()) != NULL) {
			}
			continue;
		}
#endif

		cvp = get_coord_vp(vpn, the_type);
		if (cvp == NULL) {
			// Type mismatch on name
			VM_DBG((" ... node %u thinks it is type %d\n", rnode,
			    the_type));
			return (false);
		}
		cvp->set_node_info(rnode, rv, uv, rva, vms);
		// Release hold taken in get_coord_vp
		cvp->rele();
	}
	return (true);
}

//
// btstrp_np_handshake takes a vm_stream specifying the bootstrap nodepair
// versioned protocols on another node and returns a stream of running versions
// for the pair of nodes, or NULL if some running version could not be picked.
//
// If rnode is the local node, it just creates the appropriate coord_vp
// objects and returns.
//
vm_stream *
vm_coord::btstrp_np_handshake(nodeid_t rnode, vm_stream *vmsp)
{
	ASSERT(rnode != NODEID_UNKNOWN && rnode <= NODEID_MAX);

	clear_list_by_node(rnode, VP_BTSTRP_NODEPAIR);

	if (!decode_vm_stream(rnode, VP_BTSTRP_NODEPAIR, *vmsp, RV_NONE))
		return (NULL);

	if (rnode == local_nodeid())
		return (NULL);

	// Syslog relating to false return value is handled in
	// get_rv_stream_by_type
	return (get_rv_stream_by_type(VP_BTSTRP_NODEPAIR, rnode));
}

//
// Called from vm_comm to store the versioned protocol info from the
// specified node.
//
void
vm_coord::set_info(vp_type v_type, nodeid_t rnode, vm_stream *vmsp,
	uint32_t qvotes)
{
	uint32_t rva = RV_NONE;
	if (v_type == VP_BTSTRP_CLUSTER) {
		cci.set_btstrp_node_info(rnode, vmsp, qvotes, rva);
	} else {
		ASSERT(v_type == VP_CLUSTER);
		ASSERT(qvotes == 0);
		cci.set_node_info(rnode, vmsp, rva);
	}

	if (vmsp != NULL) {
		clear_list_by_node(rnode, v_type);

		if (!decode_vm_stream(rnode, v_type, *vmsp, rva)) {
			// XXX Add code in here to disconnect from the offending
			// node.  With luck, a partition will form that
			// resolves the problem.
			// XXX syslog
			// Should also syslog an error in this case
			CL_PANIC(0);
		}
	}
}

//
// Called from vm_comm to calculate running versions for
// btstrp_cl vps, given the membership passed in.  The coordinator
// will indicate whether a reconfiguration or the shutdown of this
// node is necessary.
//
vm_stream *
vm_coord::calculate_btstrp_cl(cmm::membership_t &m, cmm::seqnum_t current_seq,
	cmm::seqnum_t upgrade_seq, bool &retry_calc, bool &shutdown_node)
{
	nodeset all_ni, all_cl, current_ns;
	bool calc_success;
	vm_stream *btstrp_cl_rv_msg = NULL;
	uint32_t lowest_votes, current_votes;

	retry_calc = shutdown_node = false;

	// Initialize the data structures in the cci.
	cci.reinitialize(m, false);

	cci.all_current_members(all_cl);
	cci.all_non_initializing_members(all_ni);

	// panic if we are not a member of the current cluster
	CL_PANIC(all_cl.contains(local_nodeid()));

	bool this_node_initial = !all_ni.contains(local_nodeid());
	bool all_nodes_initial = all_ni.is_empty();
	bool need_upgrade = (upgrade_seq > cci.get_btstrp_cl_rv_seq());

	VM_DBG(("local_nd = %u all_ni = %llx all_cl = %llx thi=%d, ani=%d"
	    " nu=%d\n", local_nodeid(), all_ni.bitmask(),
	    all_cl.bitmask(), this_node_initial, all_nodes_initial,
	    need_upgrade));

	// We shouldn't be initializing the cluster and upgrading at the
	// same time.
	ASSERT(!all_nodes_initial || !need_upgrade);

	// We should either be initializing or have valid running versions
	// stored in the relevant coord_vp objects.
	ASSERT(all_nodes_initial || (cci.get_btstrp_cl_rv_seq() != 0));

	// Get rid of old values and initialize data structures
	clear_list_not_in_nodeset(all_cl, VP_BTSTRP_NODEPAIR);
	// Clear out the rvs in the btstrp_cl coord_vps if we're going to be
	// recalculating them
	clear_list_not_in_nodeset(all_cl, VP_BTSTRP_CLUSTER,
	    need_upgrade || all_nodes_initial);

	// Initialize current_ns and calc_success to safe values in case
	// no algorithms are run.
	current_ns.set(all_ni);
	calc_success = true;

	if (need_upgrade) {
		// Calculate new running versions for existing members of the
		// cluster.
		VM_DBG(("Performing btstrp_cl commit calculation\n"));

		calc_success = btstrp_cl_try_nodeset(all_ni, need_upgrade,
		    true);
		// Panic if we couldn't find new running versions for the
		// non-initializing nodes
		CL_PANIC(calc_success);

		cci.set_btstrp_cl_rv_seq(upgrade_seq);
	}

	if (this_node_initial) {
		current_ns.set(all_cl);

		// Try getting running versions for the entire membership
		calc_success = btstrp_cl_try_nodeset(current_ns,
		    all_nodes_initial, true);

		if (!calc_success) {

			// Not all nodes can be members of this cluster, so try
			// nodesets with decreasing numbers of quorum votes.
			VM_DBG(("btstrp_cl: Not all nodes compatible\n"));

			if (!cci.verify_quorum_votes()) {
				retry_calc = true;
				return (NULL);
			}
			current_votes = cci.total_votes();

			if (all_nodes_initial) {
				lowest_votes = cci.needed_votes();
			} else {
				lowest_votes = cci.ninit_votes();
			}

			while (!calc_success &&
			    (current_votes >= lowest_votes)) {
				cci.init_node_subset(current_votes);
				do {
					cci.next_subset(current_ns);
					if (current_ns.is_empty())
						break;
					VM_DBG(("Trying nodeset %llx\n",
					    current_ns.bitmask()));
					calc_success = btstrp_cl_try_nodeset(
					    current_ns, all_nodes_initial,
					    false);
				} while (!calc_success);
				current_votes--;
			}

			// Finalize values if they are changing
			if (all_nodes_initial && calc_success) {
				calc_success = btstrp_cl_try_nodeset(current_ns,
				    all_nodes_initial, true);
				ASSERT(calc_success);
				cci.set_btstrp_cl_rv_seq(current_seq);
			}
		} else {
			VM_DBG(("calc_btstrp_cl: All nodes compatible\n"));
			if (all_nodes_initial)
				cci.set_btstrp_cl_rv_seq(current_seq);
		}
	}

	// Either we're initializing or the calculation should have been
	// successful (because we're already running)

	ASSERT(all_nodes_initial || calc_success);

	if (current_ns.contains(local_nodeid())) {
		ASSERT(calc_success);
		VM_DBG(("calc_btstrp_cl: This node in successful nodeset"
		    " %llx\n", current_ns.bitmask()));

		// If the currently calculated running versions are not
		// in the local vm_admin_impl (because we calculated new ones,
		// because this node is initializing, or because we calculated
		// them already but for whatever reason we didn't get them
		// installed during the previous reconfiguration, we need to
		// send them
		nodeset current_rv;
		cci.get_btstrp_cl_rv_nodeset(current_rv);
		if (!current_rv.contains(local_nodeid())) {
			btstrp_cl_rv_msg =
			    get_rv_stream_by_type(VP_BTSTRP_CLUSTER);
		}
	} else if (calc_success) {
		ASSERT(this_node_initial);
		// We're out
		shutdown_node = true;
		VM_DBG(("calc_btstrp_cl: This node not in successful"
		    " nodeset %llx\n", current_ns.bitmask()));
		// XXX syslog
		// There is already an existing cluster, or a new one will
		// be formed, with the membership defined in current_ns.  This
		// node is incompatible with that cluster and will be shut
		// down.  The incompatibilities are in the btstrp_cl mode
		// vps
	} else {
		ASSERT(all_nodes_initial);
		// No one is in, reconfigure until membership changes.
		VM_DBG(("No successful nodeset with quorum found\n"));
		// XXX syslog
		// No cluster with consistent running versions and quorum
		// could be found, so we will reconfigure until a change
		// in membership yields a change in this situation.  The
		// incompatibilities are in the btstrp_cl mode vps.  We will
		// hit this situation every time the cmm reconfiguration times-
		// out, but since it can only happen when the cluster is first
		// forming, it is probably safe to print out a syslog every
		// time, as we won't be doing much else cluster-wise.
		retry_calc = true;
	}

	return (btstrp_cl_rv_msg);
}

//
// Called from vm_comm to calculate running versions for
// cl vps, given the membership passed in.  The coordinator
// will indicate whether a reconfiguration is necessary,
// which nodes should receive the resulting stream, and
// which nodes should be shut down.
//
vm_stream *
vm_coord::calculate_cl(cmm::membership_t &m, cmm::seqnum_t current_seq,
	cmm::seqnum_t upgrade_seq, nodeset &send_to, nodeset &good_nodes,
	bool &retry_calc)
{
	nodeset all_ni, all_cl;
	bool calc_success = true;
	vm_stream *cl_rv_msg = NULL;
	uint32_t lowest_votes, current_votes;

	retry_calc = false;

	cci.reinitialize(m, true);

	cci.all_current_members(all_cl);
	cci.all_non_initializing_members(all_ni);
	VM_DBG(("cci.cl_rv_seq = %llu\n", cci.get_cl_rv_seq()));

	// panic if we are not a member of the current cluster
	CL_PANIC(all_cl.contains(local_nodeid()));

	bool all_nodes_initial = all_ni.is_empty();
	bool any_node_initial = !all_cl.equal(all_ni);
	bool upgrading = (upgrade_seq > cci.get_cl_rv_seq());

	ASSERT(!all_nodes_initial || !upgrading);

	good_nodes.set(all_cl);

	if (all_nodes_initial) {

		clear_list_not_in_nodeset(good_nodes, VP_CLUSTER, false);

		cl_make_keys(all_cl);

		calc_success = cl_try_nodeset(good_nodes, false, true);

		if (!calc_success) {
			VM_DBG(("cl: Not all nodes compatible\n"));

			if (!cci.verify_quorum_votes()) {
				retry_calc = true;
				send_to.empty();
				good_nodes.empty();
				cl_delete_keys();
				return (NULL);
			}

			current_votes = cci.total_votes();

			lowest_votes = cci.needed_votes();

			while (!calc_success &&
			    (current_votes >= lowest_votes)) {
				cci.init_node_subset(current_votes);
				do {
					cci.next_subset(good_nodes);
					if (good_nodes.is_empty())
						break;
					VM_DBG(("Trying nodeset %llx\n",
					    good_nodes.bitmask()));
					calc_success = cl_try_nodeset(
					    good_nodes, false, false);
				} while (!calc_success);
				current_votes--;
			}
			// Finalize values if they are changing
			if (all_nodes_initial && calc_success) {
				cci.set_cl_rv_seq(current_seq);
				calc_success = cl_try_nodeset(good_nodes,
				    false, true);
				ASSERT(calc_success);
			}
		} else {
			VM_DBG(("cl: All nodes compatible\n"));
			cci.set_cl_rv_seq(current_seq);
		}
		cl_delete_keys();

	} else if (any_node_initial) {
		cl_get_best_nodeset(good_nodes, false);
	}

	if (upgrading) {
		if (upgrade_seq > cci.get_cl_uv_seq()) {
			clear_list_not_in_nodeset(good_nodes, VP_CLUSTER, true);
			// The current uvs, if any, aren't new enough, so
			// calculate new ones
			cl_make_keys(good_nodes);
			cci.set_cl_uv_seq(current_seq);
			VM_DBG(("Performing cl commit calculation\n"));
			calc_success =
			    cl_try_nodeset(good_nodes, true, true);
			// panic if we are upgrading but couldn't find a valid
			// set of new versions
			CL_PANIC(calc_success);
			cl_delete_keys();
		} else {
			// There are already uvs calculated, and we need to
			// determine which nodes match.
			cl_get_best_nodeset(good_nodes, true);
		}
	}

	// If good_nodes is not a superset of the non-initializing nodes,
	// something has gone wrong in the distributed state, so panic.
	CL_PANIC(good_nodes.superset(all_ni));

	// At this point good_nodes is the set of nodes that are compatible,
	// and we have computed rvs and uvs if necessary.  Now we need to
	// compute send_to and retry_calc

	// send_to should be the nodes that don't have the current rvs and
	// uvs

	send_to.set(all_cl);

	nodeset tmp_ns;
	cci.get_cl_rv_nodeset(tmp_ns);
	if (upgrading) {
		nodeset tmp_ns2;
		cci.get_cl_uv_nodeset(tmp_ns2);
		tmp_ns.intersect(tmp_ns2);
	}
	send_to.remove_nodes(tmp_ns);

	if (all_nodes_initial && !calc_success) {
		VM_DBG(("cl: No successful nodeset with quorum found\n"));
		send_to.empty();
		good_nodes.empty();
		// XXX syslog
		// No cluster with consistent running versions and quorum
		// could be found, so we will reconfigure until a change
		// in membership yields a change in this situation.  The
		// incompatibilities are in the cl mode vps.  We will
		// hit this situation every time the cmm reconfiguration times-
		// out, but since it can only happen when the cluster is first
		// forming, it is probably safe to print out a syslog every
		// time, as we won't be doing much else cluster-wise.
		// Note that this will only be logged on the node that
		// hosts the coordinator (which also hosts the rm primary).
		retry_calc = true;
	} else {
		ASSERT(calc_success);
		if (!send_to.is_empty())
			cl_rv_msg = get_rv_stream_by_type(VP_CLUSTER);
	}

	return (cl_rv_msg);
}

//
// vm_coord::is_upgrade_needed()
//
// Go through all of the versioned protocols that are type VP_CLUSTER or type
// VP_BTSTRP_CLUSTER and check if the highest version which was computed the
// last time try_nodeset was called, is the current running version.
//
bool
vm_coord::is_upgrade_needed(version_manager::vp_state &s,
    nodeset &minimal_nodes)
{
	nodeset cur_cl;
	coord_vp *cvp;
	version_manager::vp_state s_tmp = version_manager::ALL_VERSIONS_MATCH;
	nodeset min_tmp;

	s = version_manager::ALL_VERSIONS_MATCH;
	minimal_nodes.empty();

	//
	// cci should contain a valid list of the nodes that were in the
	// cluster at the last cmm reconfig, which should be the same nodes
	// that are in the cluster now.
	//
	cci.all_current_members(cur_cl);

	clear_list_not_in_nodeset(cur_cl, VP_CLUSTER, false);
	cl_make_keys(cur_cl);

	bool upgrade_needed = false;

	//
	// For each versioned protocol, either bootstrap cluster or cluster,
	// call is_upgrade_needed on the coord_vp or vp_set_keys object.
	// If the return value of any is_upgrade_needed call is "true", then
	// this call should also return true. If the vp_state of any of the
	// is_upgrade_needed calls is "MISMATCHED_VERSIONS", this function
	// should also return MISMATCHED_VERSIONS and also add any minimal
	// nodes for the vp to the overall minimal_nodes nodeset.
	//

	cvp_typelist[VP_BTSTRP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_BTSTRP_CLUSTER].get_current()) != NULL) {
		if (cvp->is_upgrade_needed(s_tmp, cur_cl, min_tmp)) {
			VM_DBG(("is_upgrade_needed: true for %s\n",
			    cvp->get_name()));
			upgrade_needed = true;
		} else {
			VM_DBG(("is_upgrade_needed: false for %s\n",
			    cvp->get_name()));
		}
		if (s_tmp == version_manager::MISMATCHED_VERSIONS) {
			s = version_manager::MISMATCHED_VERSIONS;
			minimal_nodes.add_nodes(min_tmp);
		}
		min_tmp.empty();
		cvp_typelist[VP_BTSTRP_CLUSTER].advance();
	}

	cvp_typelist[VP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_CLUSTER].get_current()) != NULL) {
		//
		// Only calculate once for each dependency group. Arbitrarily,
		// we will only calculate for the coord_vp that has ordinal
		// 0, so just skip the others.
		//
		if (cvp->get_ordinal() != 0) {
			VM_DBG(("Skipping is_upgrade_needed for %s\n",
			    cvp->get_name()));
			cvp_typelist[VP_CLUSTER].advance();
			continue;
		}

		if (cvp->get_group_id() == 0) {
			if (cvp->is_upgrade_needed(s_tmp, cur_cl, min_tmp)) {
				VM_DBG(("is_upgrade_needed: true for %s\n",
				    cvp->get_name()));
				upgrade_needed = true;
			} else {
				VM_DBG(("is_upgrade_needed: false for %s\n",
				    cvp->get_name()));
			}
			if (s_tmp == version_manager::MISMATCHED_VERSIONS) {
				s = version_manager::MISMATCHED_VERSIONS;
				minimal_nodes.add_nodes(min_tmp);
			}
			min_tmp.empty();
		} else {
			if (cl_set_keys[cvp->get_group_id()].
			    is_upgrade_needed(s_tmp, cur_cl, min_tmp)) {
				VM_DBG(("is_upgrade_needed: true for %s\n",
				    cvp->get_name()));
				upgrade_needed = true;
			} else {
				VM_DBG(("is_upgrade_needed: false for %s\n",
				    cvp->get_name()));
			}
			if (s_tmp == version_manager::MISMATCHED_VERSIONS) {
				s = version_manager::MISMATCHED_VERSIONS;
				minimal_nodes.add_nodes(min_tmp);
			}
			min_tmp.empty();
		}

		cvp_typelist[VP_CLUSTER].advance();
	}

	cl_delete_keys();

	VM_DBG(("vm_coord returning minimal_nodes %llx\n",
	    minimal_nodes.bitmask()));
	return (upgrade_needed);
}

//
// Add the coord_vp to our lists
//
void
vm_coord::put_cvp_in_list(coord_vp *cvp)
{
	coord_vp *c;
	int r;
	bool inserted = false;

	vp_type t = cvp->get_vp_type();

	// Place the coord_vp in the list in sort-order
	cvp_typelist[t].atfirst();
	while ((c = cvp_typelist[t].get_current()) != NULL) {
		r = os::strcmp(c->get_name(), cvp->get_name());
		if (r > 0) {
			cvp_typelist[t].insert_b(cvp);
			inserted = true;
			cvp_typecount[t]++;
			break;
		} else if (r < 0) {
			cvp_typelist[t].advance();
		} else {
			// Shouldn't already be in list.
			ASSERT(0);
		}
	}

	// Put it at the end of the list if it's last
	if (!inserted) {
		cvp_typelist[t].append(cvp);
		cvp_typecount[t]++;
	}
}

//
// Remove the coord_vp from our lists
//
void
vm_coord::remove_cvp_from_list(coord_vp *cvp)
{
	ASSERT(cvp);
	bool check_erase;
	check_erase = cvp_typelist[cvp->get_vp_type()].erase(cvp);
	ASSERT(check_erase);
	cvp_typecount[cvp->get_vp_type()]--;
}

//
// Return the coord_vp matching name cvn and type t.  If t was in the
// uninitialized list, move it to list t.  If there was no matching
// coord_vp, create one, put it in list t, and return it.  Always return
// with an extra hold (or with the starting hold if we're creating it).
//
// This will return NULL if t was not VP_UNINITIALIZED and if there
// was already a coord_vp with the same name cvn but a different type
// than t.
//
coord_vp *
vm_coord::get_coord_vp(const char *cvn, vp_type t)
{
	coord_vp *the_cvp;
	int r = -1, i, j;

	// Go through every list, starting with the one for t
	IntrList<coord_vp, _SList>::ListIterator cvps;
	for (i = 0; i < VP_TYPE_ARRAY_SIZE; i++) {
		j = (i + t) % VP_TYPE_ARRAY_SIZE;
		cvps.reinit(cvp_typelist[j]);
		while ((the_cvp = cvps.get_current()) != NULL) {
			r = strcmp(the_cvp->get_name(), cvn);
			if (r >= 0) {
				break;
			}
			cvps.advance();
		}
		if (r == 0) {
			// We found it
			ASSERT(the_cvp);
			ASSERT(strcmp(the_cvp->get_name(), cvn) == 0);
			break;
		} else {
			the_cvp = NULL;
		}
	}

	if (the_cvp != NULL) {
		// We found it
		ASSERT(the_cvp);
		ASSERT(strcmp(the_cvp->get_name(), cvn) == 0);
		if (j == VP_UNINITIALIZED) {
			if (t != VP_UNINITIALIZED) {
				// The type was previously VP_UNINITIALIZED,
				// set to a better type if we can.
				remove_cvp_from_list(the_cvp);
				VM_DBG(("Setting type of coord_vp %s to %d\n",
				    cvn, t));
				the_cvp->set_vp_type(t);
				put_cvp_in_list(the_cvp);
			}
		} else {
			if (t != VP_UNINITIALIZED) {
				// Two different types associated with the
				// same name.
				if (j != t) {
					VM_DBG(("VP name conflict:  Some nodes "
					    "think %s is type %d but...\n", cvn,
					    j));
					return (NULL);
				}
			}
		}
		// The hold() is released or made use of by the caller
		the_cvp->hold();
	} else {
		// hold() implicit on creation
		the_cvp = new coord_vp(cvn, t, this);
		VM_DBG(("Creating coord_vp %s (type %d)\n", cvn, t));

		put_cvp_in_list(the_cvp);
	}
	return (the_cvp);
}

//
// Remove any data associated with the node in the cooresponding
// list of coord_vps
//
void
vm_coord::clear_list_by_node(nodeid_t n, vp_type t)
{
	coord_vp *cvp;

	// Clear out the old entries for this node
	cvp_typelist[t].atfirst();
	while ((cvp = cvp_typelist[t].get_current()) != NULL) {
		cvp->clear_node(n);
		// Don't advance if we deleted the underlying object.  We use
		// this idiom because any object in the list could get deleted,
		// not just the object cvp points to.
		if (cvp == cvp_typelist[t].get_current())
			cvp_typelist[t].advance();
	}
}

//
// Clear the last recorded nodesets for vps of type t
//
void
vm_coord::clear_list_not_in_nodeset(const nodeset &ns, vp_type t,
	bool need_upgrade, bool force)
{
	coord_vp *cvp;

	cvp_typelist[t].atfirst();
	while ((cvp = cvp_typelist[t].get_current()) != NULL) {
		cvp->clear_by_nodeset(ns, force, need_upgrade);
		// Don't advance if we deleted the underlying object.  We use
		// this idiom because any object in the list could get deleted,
		// not just the object cvp points to.
		if (cvp == cvp_typelist[t].get_current())
			cvp_typelist[t].advance();
	}
}

void
vm_coord::cl_make_keys(const nodeset &all_cl)
{
	coord_vp *cvp;

	// First, read through the VP_CLUSTER list discarding out-of-date
	// nodes and resetting the group_ids and ordinals.  Also clear the
	// upgrade version.

	cl_set_keys = new vp_set_key[(size_t)(cvp_typecount[VP_CLUSTER]/2 + 1)];

	highest_set_key = 1;
	int f;

	cvp_typelist[VP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_CLUSTER].get_current()) != NULL) {
		if (cvp->get_group_id() == -1) {
			f = cvp->connected_components_bfs(highest_set_key);
			if (f > 1) {
				cl_set_keys[highest_set_key].reset((uint16_t)f);
				highest_set_key++;
			}
		}
		if (cvp->get_group_id() != 0) {
			cl_set_keys[cvp->get_group_id()].
			    set_vpop(cvp->get_ordinal(), cvp);
		}
		cvp_typelist[VP_CLUSTER].advance();
	}

	highest_set_key--;

	if (highest_set_key > 0) {
		for (int i = 1; i <= highest_set_key; i++) {
			compute_solution_universe(cl_set_keys[i], all_cl);
		}
	}
}

void
vm_coord::cl_delete_keys()
{
	if (cl_set_keys != NULL) {
		delete [] cl_set_keys;
		cl_set_keys = NULL;
	}
}

void
vm_coord::compute_solution_universe(vp_set_key &key, const nodeset &ns)
{
	IntrList <vp_set, _SList> tmp_su;
	vp_set *vps;
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	bool node_bad;

#ifdef  DEBUG
	int16_t group_id = key.get_cvpp(0)->get_group_id();
	ASSERT(group_id > 0);
#endif

	int nele = key.num_elems();

	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (!ns.contains(n))
			continue;

		node_bad = false;

		vps = new vp_set((uint16_t)nele);
		ASSERT(vps);

		for (uint16_t m = 0; m < nele; m++) {
			coord_vp *cand_cvp = key.get_cvpp(m);
			cand_cvp->calc_potential_versions(vps->get_vr(m), n,
			    ns);
			key.merge_all_versions(m, vps->get_vr(m));
		}

		vps->set_valid();

		IntrList<vp_set, _SList> &su = key.get_solution_universe(n);
		ASSERT(su.empty());

		su.append(vps);

		vp_constraint vpc((uint16_t)nele);

		for (uint16_t m = 0; m < nele; m++) {
			coord_vp *cand_cvp = key.get_cvpp(m);
			ASSERT(cand_cvp->get_group_id() == group_id);
			vpc.set_index(m);

			if (cand_cvp->is_empty(n))
				continue;

			IntrList<vp_dependency, _SList>::ListIterator
			    depi(cand_cvp->get_vp_dependency_list(n));

			while ((vpd = depi.get_current()) != NULL) {
				if (vpd->is_bad() ||
				    vpd->get_rhs_list().empty()) {
					depi.advance();
					continue;
				}

				vpc.empty();
				vpc.get_vr(m).reinit(vpd->get_lhs());

				IntrList<vp_rhs_version, _SList>::ListIterator
				    rhsi(vpd->get_rhs_list());
				while ((vrhs = rhsi.get_current()) != NULL) {
					ASSERT(vrhs->get_cvpp() != NULL);
					if (vrhs->get_cvpp()->get_vp_type() !=
					    VP_CLUSTER) {
						rhsi.advance();
						continue;
					}
					ASSERT(vrhs->get_cvpp()->
					    get_group_id() == group_id);
					vpc.get_vr(vrhs->get_cvpp()->
					    get_ordinal()).reinit(
					    vrhs->get_vr());
					rhsi.advance();
				}

				while ((vps = su.reapfirst()) != NULL) {
					vpc.apply_to_vp_set(vps, tmp_su);
				}

				if (tmp_su.empty()) {
					node_bad = true;
					break;
				}

				ASSERT(su.empty());
				su.concat(tmp_su);

				depi.advance();
			}

			if (node_bad)
				break;
		}
	}
	key.compute_scores();
}

//
// Generates VP stream for given nodeid / vp type.
// (Returns an empty stream if there is no match.)
//
vm_stream *
vm_coord::get_stream_by_node_and_type(nodeid_t nid, vp_type type)
{
	cmm::seqnum_t rv_seq = 0, uv_seq = 0;

	if (type == VP_BTSTRP_CLUSTER) {
		rv_seq = cci.get_btstrp_cl_rv_seq();
	} else if (type == VP_CLUSTER) {
		rv_seq = cci.get_cl_rv_seq();
		uv_seq = cci.get_cl_uv_seq();
	}

	vm_stream *vmsp;
	vmsp = new vm_stream(vm_stream::VP_STREAM_1,
	    (uint16_t)cvp_typecount[type], rv_seq, uv_seq);

	//
	// For every coord_vp of the targeted type.
	//
	coord_vp *cvp;
	cvp_typelist[type].atfirst();
	while ((cvp = cvp_typelist[type].get_current()) != NULL) {
		// Add to stream.
		cvp->put_stream(nid, vmsp);
		cvp_typelist[type].advance();
	}
	// Terminate stream.
	vmsp->put_versioned_protocol(NULL);

	return (vmsp);
}

//
// get_rv_stream_by_type creates a vm_stream with a list of running versions.
// These are just concatenated vp headers, terminated with a null string.
//
vm_stream *
vm_coord::get_rv_stream_by_type(vp_type t, nodeid_t rnode)
{
	coord_vp *cvp;
	version_manager::vp_version_t rv, uv;
	version_range empty_vr;
	vm_stream *vmsp;

	cmm::seqnum_t rv_seq = 0, uv_seq = 0;

	if (t == VP_BTSTRP_CLUSTER) {
		rv_seq = cci.get_btstrp_cl_rv_seq();
	} else if (t == VP_CLUSTER) {
		rv_seq = cci.get_cl_rv_seq();
		uv_seq = cci.get_cl_uv_seq();
	}

	vmsp = new vm_stream(vm_stream::RV_STREAM_1,
	    (uint16_t)cvp_typecount[t], rv_seq, uv_seq);

	ASSERT(vmsp != NULL);

	cvp_typelist[t].atfirst();
	while ((cvp = cvp_typelist[t].get_current()) != NULL) {
		if (t == VP_BTSTRP_NODEPAIR) {
			ASSERT(rnode != NODEID_UNKNOWN && rnode <= NODEID_MAX);
			cvp->calc_highest_nodepair(local_nodeid(),
			    rnode, rv);
			uv.major_num = uv.minor_num = 0;

			if (rv.major_num == 0) {
				//
				// Found NP incompatibility.
				//
				delete vmsp;
#ifndef LIBVM
				version_range remote_vr;
				cvp->get_consistent_versions(rnode, remote_vr);
				log_diagnostic(cvp->get_name(), rnode,
				    &remote_vr);
#endif
				return (NULL);
			}
		} else {
			ASSERT(rnode == NODEID_UNKNOWN);
			cvp->get_rv(rv);
			cvp->get_uv(uv);
		}
		ASSERT(rv.major_num != 0);
		vmsp->put_header(cvp->get_name(), (uint16_t)t, rv, uv);
		cvp_typelist[t].advance();
	}

	// Terminate the list with a null header
	vmsp->put_header(NULL, (uint16_t)t, rv, uv);

	return (vmsp);
}

#ifndef LIBVM
//
// Log diagnostic.
//
// Get an incompatible version range for a given versioned protocol
// and log a message that informs as much as possible about the
// versions incompatibility with the local node.
//
void
vm_coord::log_diagnostic(char *vp_name, nodeid_t remote_nid,
    version_range *remote_vr) {

	char *msg_1 = NULL;
	char *msg_2 = NULL;

	//
	// If the local node knows about the given versioned
	// protocol file then we can be more verbose about
	// the versions incompatibility.
	//
	versioned_protocol *vp = vm_comm::the_vm_admin_impl().
		get_vp_by_name(vp_name);

	if (vp != NULL) {

		version_range local_vr = vp->get_potential_versions();

		build_description_string(vp, &local_vr, &msg_1);
		if (remote_vr != NULL) {
			build_description_string(vp, remote_vr, &msg_2);
		}
	}

	if (remote_nid != 0) {
		//
		// Log a node pair incompatibility notice message.
		//

		//
		// SCMSGS
		// @explanation
		// This is an informational message from the cluster version
		// manager and may help diagnose what software component is
		// failing to find a compatible version during a rolling
		// upgrade. This error may also be due to attempting to boot a
		// cluster node in 64-bit address mode when other nodes are
		// booted in 32-bit address mode, or vice versa.
		// @user_action
		// This message is informational; no user action is needed.
		// However, if this message is for a core component, one or
		// more nodes may shut down in order to preserve system
		// integrity. Verify that any recent software installations
		// completed without errors and that the installed packages or
		// patches are compatible with the rest of the installed
		// software.
		//
		(void) vm_comm::the().get_msg().log(
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "The %s protocol for this node%s "
		    "is not compatible with the %s protocol for node %u%s.",
		    vp_name,
		    (msg_1 != NULL) ? msg_1 : "",
		    vp_name,
		    remote_nid,
		    (msg_2 != NULL) ? msg_2 : "");
	} else {
		//
		// Log a cluster incompatibility notice message.
		//

		//
		// SCMSGS
		// @explanation
		// The cluster version manager exchanges version information
		// between nodes running in the cluster and has detected an
		// incompatibility. This is usually the result of performing a
		// rolling upgrade where one or more nodes has been installed
		// with a software version that the other cluster nodes do not
		// support. This error may also be due to attempting to boot a
		// cluster node in 64-bit address mode when other nodes are
		// booted in 32-bit address mode, or vice versa.
		// @user_action
		// Verify that any recent software installations completed
		// without errors and that the installed packages or patches
		// are compatible with the rest of the installed software.
		// Save the /var/adm/messages file. Check the messages file
		// for earlier messages related to the version manager which
		// may indicate which software component is failing to find a
		// compatible version.
		//
		(void) vm_comm::the().get_msg().log(
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "The %s protocol for this node%s "
		    "is not compatible with the %s protocol for the rest of "
		    "the cluster%s.",
		    vp_name,
		    (msg_1 != NULL) ? msg_1 : "",
		    vp_name,
		    (msg_2 != NULL) ? msg_2 : "");
	}


	//
	// Clean up.
	//
	if (msg_1 != NULL) {
		delete [] msg_1;
		msg_1 = NULL;
	}
	if (msg_2 != NULL) {
		delete [] msg_2;
		msg_2 = NULL;
	}
}

//
// Helper for vm_coord::log_diagnostic.
//
// This function returns a list of version description strings between
// brackets. The candidate versions are passed as parameters. The function
// uses the specified versioned protocol to perform the translation.
//
void
vm_coord::build_description_string(versioned_protocol *vp, version_range *vr,
    char **msg)
{
	//
	// Description strings (if any) associated to versions
	// are accessible through the local node list of dependencies
	// for the given versioned protocol.
	//
	IntrList<vp_dependency, _SList>::ListIterator
		depi(vp->get_vp_dependency_list());
	vp_dependency *local_dependency;

	int first = 1;
	char *_msg = new char[1];
	*_msg = '\0';

	//
	// Go through candidate versions to translation.
	//
	//lint -e668
	for (uint16_t i = 0; i < vr->get_count(); i++) {

		//
		// Go through dependencies list for local node.
		//
		depi.reinit(vp->get_vp_dependency_list());
		while ((local_dependency = depi.get_current()) != NULL) {
			for (uint16_t j = 0; j < local_dependency->get_lhs().
				get_count(); j++) {

				char *desc =  local_dependency->get_lhs().
					get_triad(j).get_description();

				//
				// If found a match (and description exists).
				//
				if ((vr->get_triad(i).
				    eq(local_dependency->get_lhs().
					get_triad(j))) && (desc != NULL)) {

					//
					// Concatenate description.
					//
					// Add 4 to size allocated to
					// potentially give space for:
					// '\0', plus " (" or ", "], plus ")".
					//
					size_t size = strlen(_msg) +
						strlen(desc) + 4;
					char *msg_tmp = new char[size];
					(void) strcpy(msg_tmp, _msg);
					(void) strcat(msg_tmp, (first == 1) ?
					    " (" : ", ");
					(void) strcat(msg_tmp, desc);
					delete [] _msg;
					_msg = msg_tmp;
					first++;
				}
			}
			depi.advance();
		}
	}
	(void) strcat(_msg, (first != 1) ? ")": "");
	//lint +e668
	*msg = _msg;
}
#endif

//
// Attempt to calculate btstrp_cl running versions for the
// nodeset specified.
//
bool
vm_coord::btstrp_cl_try_nodeset(const nodeset &ns, bool new_rvs, bool force)
{
	bool retval = true;
	coord_vp *cvp;

	cvp_typelist[VP_BTSTRP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_BTSTRP_CLUSTER].get_current()) != NULL) {
		// If force is true, go ahead and run try_nodeset on the
		// remaining coord_vps before returning.  If force is false
		// and one coord_vp fails, return immediately.
		if (!cvp->try_nodeset(ns, new_rvs, false, false, force)) {
			retval = false;
		}
		if (!retval && !force) {
			break;
		}
		cvp_typelist[VP_BTSTRP_CLUSTER].advance();
	}
	return (retval);
}

//
// Attempt to calculate cl running versions for the nodeset specified.
//
bool
vm_coord::cl_try_nodeset(const nodeset &ns, bool use_uv, bool force)
{
	bool retval = true;
	coord_vp *cvp;

	cvp_typelist[VP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_CLUSTER].get_current()) != NULL) {
		if (cvp->get_ordinal() != 0) {
			cvp_typelist[VP_CLUSTER].advance();
			// XXX ASSERT that calc has been done would be good.
			continue;
		}

		if (cvp->get_group_id() == 0) {
			if (!cvp->try_nodeset(ns, true, use_uv, false, force))
				retval = false;
		} else {
			if (!cl_set_keys[cvp->get_group_id()].
			    try_nodeset(ns, use_uv, false, force))
				retval = false;
		}
		if (!retval && !force)
			break;
		cvp_typelist[VP_CLUSTER].advance();
	}

	return (retval);
}

void
vm_coord::cl_get_best_nodeset(nodeset &good_nodes, bool use_uv)
{
	coord_vp *cvp;
	nodeset tmp_ns;

	cvp_typelist[VP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_CLUSTER].get_current()) != NULL) {
		cvp->cl_get_best_subset(good_nodes, use_uv);
		cvp_typelist[VP_CLUSTER].advance();
	}
}

//
// Based on the current set of nodes in the cluster, the upgrade paths
// available, and the current running version, find the highest version that
// can be potentially upgraded to for the specified versioned protocol.
// The versioned protocol must be btstrp cluster or non-btstrp cluster.
//
void
vm_coord::get_highest_version(
    version_manager::vp_version_t &hv,
    char *vp_name)
{
	nodeset current_cl;
	coord_vp *cvp;

	//
	// cci should contain a valid list of the nodes that were in the
	// cluster at the last cmm reconfig, which should be the same nodes
	// that are in the cluster now.
	//
	cci.all_current_members(current_cl);
	clear_list_not_in_nodeset(current_cl, VP_CLUSTER, false);
	cl_make_keys(current_cl);
	// Search the bootstrap cluster coord_vp objects for vp_name.
	cvp_typelist[VP_BTSTRP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_BTSTRP_CLUSTER].get_current()) != NULL) {
		cvp_typelist[VP_BTSTRP_CLUSTER].advance();
		if (strcmp(cvp->get_name(), vp_name) != 0) {
			continue;
		}
		VM_DBG(("Getting highest version for btstrp vp %s\n",
		    vp_name));
		cvp->calc_highest_version(current_cl, hv);
		VM_DBG(("Afterwards, vp %s highest version = %u.%u.\n",
		    vp_name, hv.major_num, hv.minor_num));
		cl_delete_keys();
		return;
	}
	// Search the non-bootstrap cluster coord_vp objects.
	cvp_typelist[VP_CLUSTER].atfirst();
	while ((cvp = cvp_typelist[VP_CLUSTER].get_current()) != NULL) {
		cvp_typelist[VP_CLUSTER].advance();
		if (strcmp(cvp->get_name(), vp_name) != 0) {
			continue;
		}
		VM_DBG(("Getting highest version for non-btstrp vp "
		    "%s\n", vp_name));
		cvp->calc_highest_version(current_cl, hv);
		VM_DBG(("Afterwards, vp %s highest version = %u.%u.\n",
		    vp_name, hv.major_num, hv.minor_num));
		cl_delete_keys();
		return;
	}
	// If we get here, then we cannot find the vp.
	VM_DBG(("Cannot find the vp %s as a btstrp cl or cl vp so returning "
	    "version 1.0.\n", vp_name));
	hv.major_num = 1;
	hv.minor_num = 0;
	cl_delete_keys();
}
