/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_INTERNAL_CUSTOM_H
#define	_VM_INTERNAL_CUSTOM_H

#pragma ident	"@(#)vm_internal_custom.h	1.9	08/05/20 SMI"

//
// vm_internal_custom is an implementation of the custom version interface.
// It is created by vm_comm::initialize.  This can serve as a home for the
// customized version logic of any versioned_protocol using the Custom
// attribute.  Right now it just has the logic for picking the kernel bit
// architecture.
//

#include <h/version_manager.h>
#include <h/cmm.h>
#include <orb/object/adapter.h>
#include <sys/os.h>
#include <sys/types.h>

class vm_internal_custom : public McServerof<version_manager::custom_vp_query> {
public:
	vm_internal_custom();
	~vm_internal_custom();

	void _unreferenced(unref_t);

	// Interfaces from custom_vp_query

	void get_valid_versions(char * const,
	    const version_manager::vp_version_t &,
	    version_manager::vp_version_seq_out, Environment &);

	static int initialize();

	static void shutdown();

	static const char custom_bind_name[];

private:
	vm_internal_custom(const vm_internal_custom &);
	vm_internal_custom &operator = (vm_internal_custom &);
};

#endif	/* _VM_INTERNAL_CUSTOM_H */
