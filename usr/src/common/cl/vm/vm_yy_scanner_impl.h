/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_YY_SCANNER_IMPL_H
#define	_VM_YY_SCANNER_IMPL_H

#pragma ident	"@(#)vm_yy_scanner_impl.h	1.9	08/05/20 SMI"

//
// It turns out it wasn't tractable to make lex work in the kernel, so this
// is a small C++ custom lexer for vp specification files, adapted from the
// C-language lexer in librtreg.
//

#include <sys/os.h>
#include <vm/vp_interface.h>

// An enum of contexts for keywords
enum vm_yy_context {
	look_keyword,	// Look for a header keyword
	look_mode,	// Look for a Mode line keyword
	look_attr,	// Look for an Attributes line keyword
	look_cword,	// Just look for an arbitrary C language token
	look_none	// Don't look for any kind of keyword
};

class vm_yy_scanner_impl {
public:
	vm_yy_scanner_impl();
	~vm_yy_scanner_impl();

	typedef struct KW {
		const char *kw;		// The string to match against
		size_t len;		// The length of that string
		int tok;		// The token to return to the parser
		vm_yy_context con;	// The context to switch into next
	} KW;

	// Initialize the lexer and open the filename parameter
	bool initialize(char *);

	// Called by the parser to get the next token
	int lex();

	// prints an error string.  Also called by the parser.
	void vm_yy_error(const char *);

	// Close the file and delete data structures.
	void done();

private:
	int key_tok();		// Find a keyword token
	int quoted_tok();	// Find a string token
	int cword_tok();	// Find a c-language word token
	int int_tok();		// Find an integer value
	int maketok(int);	// Return a token indicated by the parameter
	int fgetc();		// Get a character from the input file
	int ungetc(int);	// unget a character from the input file
	bool scan_cword();	// Find the last character in a c-language word

	os::rfile f;		// The file we're lexing
	char *tokbuf;		// A buffer for tokens
	uint16_t tokbufloc;	// The length of the buffer
	vm_yy_context con;	// The keyword context we're in
	int linecount, charcount, oldcharcount;		// counts for debugging
};

#ifndef	NOINLINES
#include <vm/vm_yy_scanner_impl_in.h>
#endif	// NOINLINES

#endif	/* _VM_YY_SCANNER_IMPL_H */
