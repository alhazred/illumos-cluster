//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)vp_set.cc 1.12     08/05/20 SMI"

#include <vm/vp_set.h>

//
// Instersection operator
//
// For each version_range in the vp_set 'A', intersect it with its associated
// version_range object in vp_set 'B'.
// For readability,
// 	Let 'A' = 'this', so 'result' = 'A' n 'B'
void
vp_set::intersection(const vp_set &B, vp_set &result)
{
	ASSERT(size == B.size && result.size == size);

	result.valid = true;

	// Intersect each version_range object, one by one.
	for (int i = 0; i < size; i++) {
		vr_list[i].intersection(B.vr_list[i], result.vr_list[i]);
		if (result.vr_list[i].is_empty()) {
			result.valid = false;
		}
	}
}

//
// merge simply calls merge on each of the corresponding version_ranges.
//
void
vp_set::merge_equals(const vp_set &B)
{
	ASSERT(size == B.size);

	valid = true;

	for (int i = 0; i < size; i++) {
		(void) vr_list[i].merge_equals(B.vr_list[i]);
		if (vr_list[i].is_empty()) {
			valid = false;
		}
	}
}

void
vp_set::add_in_sort_order(IntrList<vp_set, _SList> &vpl)
{
	vp_set *vpsp;

	ptr_assert();

	vpl.atfirst();
	while ((vpsp = vpl.get_current()) != NULL) {
		if ((vpsp->score > score) ||
		    ((vpsp->score == score) && (vpsp->last_node > last_node))) {
			vpl.advance();
			continue;
		}
		break;
	}

	// insert_b will append when current == NULL, so all cases are handled
	// correctly
	vpl.insert_b(this);
}

void
vp_set::dbprint()
{
	if (score != 0)
		VM_DBGC(("%u, %u:", score, last_node));

	for (int i = 0; i < size; i++) {
		VM_DBGC(("\t"));
		vr_list[i].dbprint();
	}
}
