/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_TRIAD_IN_H
#define	_VP_TRIAD_IN_H

#pragma ident	"@(#)vp_triad_in.h	1.10	08/05/20 SMI"

// Constructor
// Builds an empty triad.
// Empty is defined to be having a major number of '0'.
inline
vp_triad::vp_triad() :
	description(NULL)
{
	triad.major_num = triad.minor_min = triad.minor_max = 0;
}

// Constructor
// Builds a specified triad.
// It is the caller's responsibility to validate the input.
// Input is valid if all passed in arguments are positive short integer values
// and if min_min <= min_max.
inline
vp_triad::vp_triad(uint16_t maj, uint16_t min_min, uint16_t min_max) :
	description(NULL)
{
	triad.major_num = maj;
	triad.minor_min = min_min;
	triad.minor_max = min_max;
	ASSERT(triad.minor_min <= triad.minor_max);
	ASSERT(triad.major_num != 0);
}

// Destructor
inline
vp_triad::~vp_triad()
{
	if (description != NULL) {
		delete [] description;
		description = NULL;
	}
}

// Returns true if the triad is empty.  An empty triad has a major number
// of 0.
inline bool
vp_triad::is_empty()
{
	return (triad.major_num == 0);
}

// Returns true if the triad is valid.
inline bool
vp_triad::is_valid()
{
	return ((triad.major_num > 0) && (triad.minor_min <= triad.minor_max));
}

// Resets the triad to all zeros
inline void
vp_triad::zero()
{
	triad.major_num = triad.minor_min = triad.minor_max = 0;
}

// Resets a triad to the specified values.  It is the caller's responsibility
// to validate the input.  Input is valid if all arguments are positive short
// integer values and that min_min <= min_max.
inline void
vp_triad::assign(uint16_t maj, uint16_t min_min, uint16_t min_max)
{
	ASSERT(min_min <= min_max);

	triad.major_num = maj;
	triad.minor_min = min_min;
	triad.minor_max = min_max;
}

inline void
vp_triad::assign_description(char *desc)
{
	if (desc != NULL) {
		if (description != NULL) {
			delete [] description;
			description = NULL;
		}
		description = desc;
	}
}

inline void
vp_triad::assign(const vp_triad &t)
{
	if (this != &t) {
		triad.major_num = t.triad.major_num;
		triad.minor_min = t.triad.minor_min;
		triad.minor_max = t.triad.minor_max;
	}
}

// Unlink triad descriptor
inline char*
vp_triad::unlink_description() {
	char *desc = description;
	description = NULL;
	return (desc);
}

// Get triad descriptor
inline char*
vp_triad::get_description() {
	return (description);
}

//
// Return 0 if the specified version is contained in the triad,
// -1 if the versions in the triad are lower than v, and 1 if the
// versions in the triad are higher than v.
//
inline int
vp_triad::compare(const version_manager::vp_version_t &v)
{
	ASSERT(is_valid());

	if (v.major_num == triad.major_num) {
		if (v.minor_num >= triad.minor_min &&
		    v.minor_num <= triad.minor_max) {
			return (0);
		} else if (v.minor_num < triad.minor_min) {
			return (1);
		} else {
			ASSERT(v.minor_num > triad.minor_max);
			return (-1);
		}
	} else if (v.major_num < triad.major_num) {
		return (1);
	} else {
		return (-1);
	}
}

//
// Specialty function for version_range::merge.  If 'this' intersects
// with or is adjacent to t, t is added into this, otherwise 'this' is
// left unchanged.  This function assumes and ASSERTs that the lowest
// value in 'this' is less than or equal to the lowest value in t.
//
// The return value is 0 if the highest value in 'this' is lower than
// and not adjacent to the lowest value in t, and therefore they won't
// be combined.  It is -1 if the two triads intersect, and 1 if they
// don't intersect and are adjacent.
//
inline int
vp_triad::merge(const vp_triad &t)
{
	int retval = 1;

	if (triad.major_num != t.triad.major_num) {
		ASSERT(triad.major_num < t.triad.major_num);
		retval = 0;
	} else {
		ASSERT(triad.minor_min <= t.triad.minor_min);

		if (triad.minor_max + 1 < t.triad.minor_min) {
			retval = 0;
		} else {
			if (triad.minor_max + 1 != t.triad.minor_min)
				retval = -1;

			if (triad.minor_max < t.triad.minor_max)
				triad.minor_max = t.triad.minor_max;
		}
	}
	return (retval);
}

// Equality operator
//
// Two triads are equal if and only if all three values are equal.
inline bool
vp_triad::eq(const vp_triad &t)
{
	return ((t.triad.major_num == triad.major_num) &&
	    (t.triad.minor_max == triad.minor_max) &&
	    (t.triad.minor_min == triad.minor_min));
}

// Less Than operator
//
// A triad is less than another triad if :
// 	its major number is less than the other's major number, or
// 	the major numbers are equal and its minor maximum is less than the
// 	other's minor minimum.
//
// The 'less than' operator is undefined when the triads intersect so
// if the triads intersect with each other, then false is returned.
inline bool
vp_triad::lt(const vp_triad &t)
{
	return ((t.triad.major_num > triad.major_num) ||
	    ((t.triad.major_num == triad.major_num) &&
		(triad.minor_max < t.triad.minor_min)));
}

// Greater Than operator
//
// A triad is greater than another triad if :
// 	its major number is greater than the other major number, or
// 	the major numbers are equal and its minor minimum is greater than the
// 	other's minor maximum.
//
// The 'greater than' operator is undefined when the triads intersect so
// if the triads intersect with each other, then false is returned.
inline bool
vp_triad::gt(const vp_triad &t)
{
	return ((t.triad.major_num < triad.major_num) ||
	    ((t.triad.major_num == triad.major_num) &&
		(t.triad.minor_max < triad.minor_min)));
}

inline bool
vp_triad::adj(const vp_triad &t)
{
	return ((t.triad.major_num == triad.major_num) &&
	    ((triad.minor_max == t.triad.minor_min - 1) ||
		(t.triad.minor_max == triad.minor_min - 1)));
}

inline bool
vp_triad::lower(const vp_triad &t)
{
	return ((triad.major_num < t.triad.major_num) ||
	    (t.triad.major_num == triad.major_num) &&
	    (triad.minor_min < t.triad.minor_min));
}

//
// Return the major number
//
inline uint16_t
vp_triad::get_major() const
{
	return (triad.major_num);
}

//
// Return the minor maximum number
//
inline uint16_t
vp_triad::get_minor_max() const
{
	return (triad.minor_max);
}

//
// Return the minor minimum number
//
inline uint16_t
vp_triad::get_minor_min() const
{
	return (triad.minor_min);
}

inline void
vp_triad::dbprint()
{
	if (triad.minor_min == triad.minor_max) {
		VM_DBGC(("%u.%u", triad.major_num, triad.minor_min));
	} else {
		VM_DBGC(("%u.%u-%u", triad.major_num, triad.minor_min,
		    triad.minor_max));
	}
}

//
// Assign the minor maximum value.
//
inline void
vp_triad::assign_minor_max(uint16_t min_max)
{
	ASSERT(min_max >= triad.minor_min);
	triad.minor_max = min_max;
}

//
// Assign the minor minimum value.
//
inline void
vp_triad::assign_minor_min(uint16_t min_min)
{
	ASSERT(min_min <= triad.minor_max);
	triad.minor_min = min_min;
}

//
// Return triad version information
//
inline vm_triad_version *
vp_triad::get_triad_version()
{
	return (&triad);
}

#endif	/* _VP_TRIAD_IN_H */
