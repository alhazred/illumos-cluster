/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VM_YY_SCANNER_IMPL_IN_H
#define	_VM_YY_SCANNER_IMPL_IN_H

#pragma ident	"@(#)vm_yy_scanner_impl_in.h	1.8	08/05/20 SMI"

inline
vm_yy_scanner_impl::vm_yy_scanner_impl() :
	tokbuf(NULL),
	tokbufloc(0),
	con(look_keyword),
	linecount(1),
	charcount(1),
	oldcharcount(0)
{
}

inline
vm_yy_scanner_impl::~vm_yy_scanner_impl()
{
	if (tokbuf) {
		delete tokbuf;
		tokbuf = NULL;
	}
}

inline void
vm_yy_scanner_impl::done()
{
	f.close();
	if (tokbuf) {
		delete tokbuf;
		tokbuf = NULL;
	}
}

inline int
vm_yy_scanner_impl::fgetc()
{
	int c = f.fgetc();
	if (c == '\n') {
		oldcharcount = charcount;
		charcount = 1;
		linecount++;
	} else {
		charcount++;
	}
	return (c);
}

inline int
vm_yy_scanner_impl::ungetc(int c)
{
	int r = f.ungetc(c);
	if (c == '\n') {
		charcount = oldcharcount;
		linecount--;
	} else {
		charcount--;
	}
	return (r);
}

#endif	/* _VM_YY_SCANNER_IMPL_IN_H */
