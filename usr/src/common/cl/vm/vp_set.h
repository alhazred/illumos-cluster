/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_SET_H
#define	_VP_SET_H

#pragma ident	"@(#)vp_set.h	1.13	08/05/20 SMI"

#include <sys/list_def.h>
#include <sys/os.h>
#include <vm/version_range.h>

//
// Version Manager vp_set Class Definition
//
// A vp_set object holds an array of version_range objects.  The array size
// once defined will not change.  Each version_range object represents a
// version protocol.  Each version protocol will be assigned a static and known
// index position into the array.  This way when two vp_set objects are
// compared, only the version_range objects with the same index will be
// compared.
//

class vp_set : public _SList::ListElem {
friend class vp_constraint;
public:
	vp_set();
	vp_set(uint16_t s);
	vp_set(const vp_set &t);
	~vp_set();

	// Intersection
	void intersection(const vp_set &B, vp_set &result);

	// merge the values of this and the argument and put the result
	// into this.
	void merge_equals(const vp_set &);

	// Retrieve the version range associated with the argument
	version_range &get_vr(uint16_t);

	// Clear out all of the member version ranges
	void empty();

	// A valid vp_set is valid only when all version_range objects
	// in the list are not empty.  These get or set the valid flag.
	bool is_valid();
	void set_valid();

	// Set the score to the argument
	void assign_score(uint32_t);

	// Set and get the last_node variable.
	void assign_node(nodeid_t);
	nodeid_t get_node();

	// Add this to the list in the argument in sort order.  This assumes
	// the list is already sorted and this is not currently in a list.
	// Sort order uses decreasing score as the first criterion and
	// decreasing nodeid (last_node) as the second.
	void add_in_sort_order(IntrList<vp_set, _SList> &);

	// Retrieve the number of vps represented in this vp_set.
	uint16_t get_size();

	// Dump info about this vp_set to the vm debug buffer.
	void dbprint();

private:
	uint16_t size;		// Array size of version_range objects
	version_range *vr_list;	// Array of version_range objects
	bool valid;		// Set to false iffi:
				//	Any version_range object is empty,
				//	Otherwise, set to true.

	nodeid_t last_node;	// Used to store the node that was examined
				// last when using this vp_set in the
				// multiverse calculation.
	uint32_t score;		// The score calculated for this vp_set, based
				// on the scoring information stored in the
				// associated vp_set_key.

	vp_set &operator = (vp_set &);
};

#ifndef	NOINLINES
#include <vm/vp_set_in.h>
#endif	// NOINLINES

#endif	/* _VP_SET_H */
