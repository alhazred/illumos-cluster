/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vm_comm.cc	1.39	08/05/27 SMI"

#include <vm/vm_comm.h>
#include <vm/vm_coord.h>
#include <vm/vm_stream.h>
#include <vm/vm_internal_custom.h>
#include <vm/vm_admin_impl.h>
#include <vm/vm_dbgbuf.h>
#include <sys/vm_util.h>
#include <repl/rmm/rmm.h>
#include <cmm/cmm_ns.h>
#include <nslib/ns.h>
#include <sys/node_order.h>
#include <orb/object/adapter.h>
#include <orb/flow/resource.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb_conf.h>

#ifdef	VM_DBGBUF
uint32_t vm_dbg_size = 12 * 1024;
dbg_print_buf vm_dbg(vm_dbg_size);
#endif

vm_comm		*vm_comm::the_vm_comm = NULL;
const size_t	vm_comm::max_btstrp_np_strlen = 16*1024;

vm_comm::vm_comm(const char *name) :
	McNoref<version_manager::inter_vm>(XDOOR_VM),
	shutdown_requested(false),
	btstrp_np_message(NULL),
	btstrp_cl_message(NULL),
	btstrp_cl_mbrshp(lone_membership::the()),
	btstrp_cl_needed(0),
	btstrp_cl_rv_message(NULL),
	cl_message(NULL),
	cl_sent_to_coord(false),
	coord_node(NODEID_UNKNOWN, INCN_UNKNOWN),
	cl_rv_message(NULL),
	current_seq(0),
	canceled_seq(0),
	upgrade_seq(0),
	last_iun_seqnum(0),
	signal_needed(false),
	try_upgrade(false),
	the_coord(new vm_coord()),
	the_admin_impl(new vm_admin_impl()),
	vps_locked(false),
	msg(SC_SYSLOG_FRAMEWORK_TAG, name, NULL)
{
	btstrp_cl_mbrshp->hold();
	// These could be made CL_PANICs, but there isn't much done
	// in the constructors other than memory allocation, which is blocking
	// in the kernel.
	ASSERT(the_coord != NULL);
	ASSERT(the_admin_impl != NULL);

	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		btstrp_np_ok[i] = false;
		btstrp_np_mbrshp.members[i] = INCN_UNKNOWN;
		btstrp_cl_seqs[i] = 0;
	}
}

vm_comm::~vm_comm()
{
	// Destroy with lock held.
	lock();

	ASSERT(the_coord != NULL);
	delete the_coord;
	the_coord = NULL;			//lint !e423

	delete btstrp_np_message;
	btstrp_np_message = NULL;

	delete btstrp_cl_message;
	btstrp_cl_message = NULL;

	ASSERT(btstrp_cl_mbrshp != NULL);
	btstrp_cl_mbrshp->rele();
	btstrp_cl_mbrshp = NULL;

	delete btstrp_cl_rv_message;
	btstrp_cl_rv_message = NULL;

	delete cl_message;
	cl_message = NULL;

	delete cl_rv_message;
	cl_rv_message = NULL;

	ASSERT(the_admin_impl == NULL);
}

//
// Called from ORB::initialize to create the version manager subsystem.
// Note: the error numbers returned should be errno values which will
// be returned to the cladmin system call.
//
int
vm_comm::initialize()
{
	int err;

	//
	// This will be true only the first time ORB::initialize() is called.
	// We could be called a second time if ORB::initialize() returns an
	// error and cladmin() is called again.
	//
	if (the_vm_comm == NULL) {
		char nodename[28];
		os::sprintf(nodename, "VersionManager.%u",
		    orb_conf::node_number());
		the_vm_comm = new vm_comm(nodename);
		ASSERT(the_vm_comm != NULL);
	}

	// Initialize the vm_internal_custom object.
	if ((err = vm_internal_custom::initialize()) != 0) {
		//
		// SCMSGS
		// @explanation
		// This message can occur when the system is booting if
		// incompatible versions of cluster software are installed.
		// @user_action
		// Verify that any recent software installations completed
		// without errors and that the installed packages or patches
		// are compatible with the rest of the installed software.
		// Also contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) the_vm_comm->msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Error initializing an internal component of the "
		    "version manager (error %d).", err);
		return (err);
	}

	// Perform internal initialization (including parsing the spec files
	// and local consistency checks).
	if ((err = the_vm_comm->initialize_int()) != VP_SUCCESS) {
#ifdef	DEBUG
		//
		// The_vm_comm->initialize_int() logs error information
		// in VM debug buffer. In addition, in debug mode we print
		// a detailed message in the console so that developers
		// can quickly identify development mistakes (such as
		// malformed vp files).
		//
		NO_SC_SYSLOG_MESSAGE((void) the_vm_comm->msg.log(
		    SC_SYSLOG_WARNING,
		    MESSAGE,
		    "Error during vm_comm initialization: %s "
		    "See VM debug buffer for more details",
		    vp_error_to_string(err)));
#endif
		if (err != VP_NOVERSIONS) {
			//
			// VP_VERSIONS is the result of local consistency
			// failure, no valid version range found for one of
			// the local vps (e.g., custom vp returns an empty
			// range). VP_NOVERSIONS error code will be propagated
			// appropriately upwards.
			//
			// Other "unexpected" error cases, such as
			// invalid vp file syntax must be logged.
			//

			//
			// SCMSGS
			// @explanation
			// This message can occur when the system is booting
			// if incompatible versions of cluster software are
			// installed.
			// @user_action
			// Verify that any recent software installations
			// completed without errors and that the installed
			// packages or patches are compatible with the rest of
			// the installed software. Also contact your
			// authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			(void) the_vm_comm->msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "Error initializing the cluster version manager "
			    "(error %d).", err);
		}
		// Must return an errno value.
		return (EINVAL);
	}
	return (0);
}

//
// Perform non-static portion of initialization
//
int
vm_comm::initialize_int()
{
	lock();

	// Call initialize_int on the_admin_impl, triggering parsing and
	// local_consistency
	int err = the_admin_impl->initialize_int(btstrp_np_message);

	if (err != VP_SUCCESS) {
		ASSERT(btstrp_np_message == NULL);
	} else {
		// If we were successful, verify that the btstrp_np_message
		// fits in max_btstrp_np_string
		ASSERT(btstrp_np_message != NULL);

		if (btstrp_np_message->written_length() + sizeof (uint16_t) >
		    max_btstrp_np_string()) {
			VM_DBG(("%lu byte btstrp_np vm_stream can't fit in "
			    "max %lu byte transport message\n",
			    btstrp_np_message->written_length() +
			    sizeof (uint16_t),
			    max_btstrp_np_string()));
			// XXX syslog
			// The btstrp_np message exceeds the maximum allowed.
			delete btstrp_np_message;
			err = VP_TRDATTOOLONG;
		} else {
			// It fits, so give it to the coordinator.
			VM_DBG(("vm_comm: local btstrp_np stream"
			    "(length %lu)\n",
			    btstrp_np_message->written_length()));

			btstrp_np_mbrshp.members[orb_conf::local_nodeid()] =
			    orb_conf::local_incarnation();
			// Also set the string in the coordinator
			(void) the_coord->btstrp_np_handshake(
			    orb_conf::local_nodeid(), btstrp_np_message);
		}
	}

	// the_admin_impl into the local nameserver regardless of whether the
	// above checks were successful.  It will be unbound on shutdown.

	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	version_manager::vm_admin_var vmai = the_admin_impl->get_objref();

	Environment e;
	ctxp->bind(vm_util::vm_bind_name, vmai, e);

	// If we didn't manage to bind vm_admin_impl into the local nameserver,
	// components looking for version info will fail.
	if (e.exception() && err == 0) {
		err = EEXIST;
	}

	unlock();
	return (err);
}

//
// Static shutdown method, called from ORB::shutdown
//
void
vm_comm::shutdown()
{
	if (the_vm_comm == NULL)
		return;

	ASSERT(the_vm_comm->the_admin_impl != NULL);

	the_vm_comm->the_admin_impl = NULL;

	// Unbind vm_admin_impl...it will delete itself in _unreferenced
	Environment e;
	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	ctxp->unbind(vm_util::vm_bind_name, e);
	ASSERT(!e.exception());

	delete the_vm_comm;
	the_vm_comm = NULL;

	vm_internal_custom::shutdown();
}

//
// Receive messages for the cl coordinator
//
bool
vm_comm::send_cl_to_coord(sol::nodeid_t src_node, cmm::seqnum_t snum,
	const version_manager::vm_stream_container_t &vms_c, Environment &)
{
	bool retval = false;

	lock();

	// If the sequence number in the message is the current sequence
	// number not only is this message from the current incarnation of
	// the remote node (otherwise the sequence number couldn't be what
	// it is set to) but the btstrp_cl_mbrshp is also the current
	// membership.

	if (snum == current_seq) {
		// If we're in the same sequence, the coord_node must be
		// us, because the only way it could not be is if we have
		// rebooted.
		ASSERT(coord_node.ndid == orb_conf::local_nodeid());
		ASSERT(vms_c.length() != 0);
		vm_stream *vmsp = new vm_stream();
		vmsp->reinit((size_t)vms_c.length(), (char *)vms_c.buffer(),
		    true, false);
		the_coord->set_info(VP_CLUSTER, src_node, vmsp);
		delete vmsp;
		retval = true;
	} else {
		VM_DBG(("Discarding cl stream from node %u\n", src_node));
	}

	unlock();
	return (retval);
}

//
// Used by the vm_comm on the coordinator node to send the cl vp running
// versions to nodes that need them.
//
void
vm_comm::send_cl_rv(cmm::seqnum_t seqnum, membership_bitmask_t gn,
	const version_manager::vm_stream_container_t &vms_c, Environment &)
{
	nodeset good_nodes(gn);

	lock();

	if (seqnum == current_seq) {
		if (cl_rv_message != NULL) {
			delete cl_rv_message;
			cl_rv_message = NULL;
		}

		//
		// If we are not a compatible node, we will use the
		// stream anyway to log a verbose incompatibility
		// message.
		//
		cl_rv_message = new vm_stream();
		cl_rv_message->reinit((size_t)vms_c.length(),
		    (char *)vms_c.buffer());

		if (!good_nodes.contains(orb_conf::local_nodeid())) {

			//
			// The coordinator has decided that this node is
			// not compatible with the rest of the cluster and
			// must shut down.  The incompatibilities are at
			// the cl mode level.
			//
			shutdown_requested = true;
#ifndef LIBVM
			log_diagnostic(cl_rv_message);
#endif
			// commit_cl_rv with shutdown_requested
			// set to true asserts that cl_rv_message is NULL
			if (cl_rv_message != NULL) {
				delete cl_rv_message;
				cl_rv_message = NULL;
			}
		}
	}

	unlock();
}

#ifndef LIBVM
void
//
// This function gets an rv stream, and for every vp in that stream,
// checks its running version against the potential versions for the
// same vp on the local node (asking the vm admin). In case of
// incompatibility, its triggers a diagnostic log.
//
vm_comm::log_diagnostic(vm_stream *the_stream)
{
	char *vpn;
	uint16_t u16_type;
	version_manager::vp_version_t tmprv, tmpuv;

	//
	// Go through running versions.
	//
	the_stream->read_from_beginning();
	while ((vpn = the_stream->get_header(u16_type, tmprv, tmpuv))
	    != NULL) {

		//
		// Get local node potential versions.
		//
		versioned_protocol *vp = vm_comm::the_admin_impl->
			get_vp_by_name(vpn);
		//
		// If the vp file is not known locally, we use
		// default 1.0 version.
		//
		version_range local_vr;
		if (vp == NULL) {
			(void) local_vr.insert_triad(1, 0, 0);
		} else {
			local_vr.reinit(vp->get_potential_versions());
		}

		//
		// In case of incompatibility.
		//
		if (!local_vr.contains(tmprv)) {
			version_range vr;
			vr.insert_version(tmprv);
			the_coord->log_diagnostic(vpn, 0, &vr);
		}
	}
}
#endif

void
vm_comm::cmm_step_cancel(cmm::seqnum_t sn)
{
	lock();
	ASSERT(canceled_seq <= sn);
	canceled_seq = sn;
	vm_cv.signal();
	unlock();
}

//
// Send either the full or the short message to every other node in the
// membership
//
void
vm_comm::send_btstrp_cl_message(callback_membership *m, cmm::seqnum_t seqnum,
	os::systime *timeout_time, bool new_btstrp_cl)
{
	node_order			no;
	bool				send_retry_needed;
	ID_node				dest_node;
	Environment			e;
	bootstrap_header		bh;
	os::condvar_t::wait_result	wait_res = os::condvar_t::SIGNALED;

	ASSERT(lock_held());

	ASSERT(btstrp_cl_mbrshp != NULL);

	if (new_btstrp_cl) {
		btstrp_cl_mbrshp->rele();
		btstrp_cl_mbrshp = lone_membership::the();
		btstrp_cl_mbrshp->hold();
	}

	// XXX Note that the calls into the quorum algorithm put the vm lock
	// ahead of the quorum lock in ordering.  If the quorum algorithm
	// ever needs to query the version manager, we will have to take
	// this into account.

	// Retrieve the number of quorum votes this node currently owns
	bh.qvotes = the_coord->current_node_votes(orb_conf::local_nodeid());

	bh.current_seqnum = seqnum;

	if (try_upgrade) {
		VM_DBG(("Upgrade commit requested for seq %llu\n", seqnum));
		ASSERT(seqnum > upgrade_seq);
		upgrade_seq = seqnum;
		ASSERT(vps_locked);
		try_upgrade = false;
	}

	bh.upgrade_seqnum = upgrade_seq;

	//
	// Set the local message on the local node first. The following two
	// calls have to be finished without possible interruptions by CMM
	// reconfigurations.
	//
	// vm_comm::calculate_btstrp_cl->vm_admin_impl::new_btstrp_cl_message->
	// ->vm_admin_int::new_btstrp_cl_message()
	// and
	// vm_comm::calculate_btstrp_cl->vm_comm::send_btstrp_cl_message->
	// vm_coord->set_info() for the local node.
	//
	// See bug 4855311 for more details.
	//
	vm_stream *btcl_ptr = NULL;
	if (new_btstrp_cl) {
		btcl_ptr = btstrp_cl_message;
	}
	// Set the local message and quorum vote info in the coord.
	the_coord->set_info(VP_BTSTRP_CLUSTER, orb_conf::local_nodeid(),
	    btcl_ptr, bh.qvotes);
	//
	// We set the local message whenever it changes, so here we just update
	// the sanity control structure to have the current sequence number.
	//
	btstrp_cl_seqs[orb_conf::local_nodeid()] = seqnum;

	//
	// Send messages to all remote relevant nodes.
	//
	// We first try to send messages to all the other relevant nodes.
	// We will retry any failures in a loop until we're notified
	// that we're canceled.
	//
	for (no.lowest(); no.valid(); no.higher()) {

		// Skip nodes that aren't part of the membership
		if (m->membership().members[no.current()] == INCN_UNKNOWN)
			continue;

		if (no.current() == orb_conf::local_nodeid()) {
			//
			// We have already set the local message. No need to
			// do anything here.
			//
			continue;
		}

		//
		// If we've already sent the message to the remote node,
		// just send the short confirmation message.
		//
		if (m->membership().members[no.current()] !=
			btstrp_cl_mbrshp->membership().members[no.current()]) {
			bh.length =
			    (uint32_t)btstrp_cl_message->written_length();
			VM_DBG(("Giving full message to %u %u\n", no.current(),
			    m->membership().members[no.current()]));
		} else {
			bh.length = 0;
			VM_DBG(("Giving short message to %u %u\n",
			    no.current(),
			    m->membership().members[no.current()]));
		}

		dest_node.ndid = no.current();
		dest_node.incn = m->membership().members[no.current()];

		do {
			ASSERT(canceled_seq <= seqnum);

			if (canceled(seqnum))
				return;

			unlock();

			resources_datagram rd(&e, 0,
			    (uint_t)sizeof (bh) + bh.length, dest_node,
			    VM_MSG);

			sendstream *ssp = orb_msg::reserve_resources(&rd);

			if (ssp == NULL) {
				VM_DBG(("Couldn't get sendstream for node "
				    "%u %u\n", dest_node.ndid,
				    dest_node.incn));
				ASSERT(e.exception());
				ASSERT(CORBA::COMM_FAILURE::
				    _exnarrow(e.exception()));
				e.clear();
				// Wait until we're officially canceled
				// by the CMM
				lock();
				while (!canceled(seqnum) && (wait_res !=
				    os::condvar_t::TIMEDOUT)) {
					wait_res = vm_cv.timedwait(get_lock(),
					    timeout_time);
				}
				if (wait_res == os::condvar_t::TIMEDOUT) {
					VM_DBG(("Didn't get canceled in "
					    "send_btstrp_cl_message\n"));
				}
				return;
			}

			ssp->put_bytes(&bh, sizeof (bh));

			if (bh.length != 0) {
				ssp->put_bytes(btstrp_cl_message->_buffer,
				    bh.length);
			}

			send_retry_needed = orb_msg::send_datagram(ssp);

			if (send_retry_needed) {
				VM_DBG(("Retry needed for node %u %u\n",
				    dest_node.ndid, dest_node.incn));
			}
			lock();

			// We don't need to wait because send_datagram only
			// fails when a path fails.  The node-to-node failure
			// is in reserve_resources (and is caused by the
			// list of endpoints to the node having a 1->0)
			// transition.
		} while (send_retry_needed);
	}

	btstrp_cl_mbrshp->rele();
	btstrp_cl_mbrshp = m;
	btstrp_cl_mbrshp->hold();
}

//
// Wait until we've received btstrp_cl messages from all other nodes and then
// call into the coordinator.  Trigger reconfiguration or note the need to
// shut down the node if necessary.
//
// XXX Note that the force_return_transition below could be replaced by
// returning an exception to the calling step.
//
void
vm_comm::calculate_btstrp_cl(callback_membership *m, cmm::seqnum_t seqnum,
	os::systime *timeout_time)
{
	node_order no;
	os::condvar_t::wait_result wait_res = os::condvar_t::SIGNALED;
	bool new_btstrp_cl;

	lock();

	current_seq = seqnum;

	new_btstrp_cl =
	    the_admin_impl->new_btstrp_cl_message(btstrp_cl_message);

	send_btstrp_cl_message(m, seqnum, timeout_time, new_btstrp_cl);

	ASSERT(lock_held());

	if (canceled(seqnum)) {
		unlock();
		return;
	}

	btstrp_cl_needed = 0;

	// Figure out how many messages still need to be received

	for (no.highest(); no.valid(); no.lower()) {
		if (m->membership().members[no.current()] != INCN_UNKNOWN) {
			ASSERT(btstrp_cl_seqs[no.current()] <= current_seq);
			// If a node is in the cmm's membership we should
			// have had a pairwise entry for it already.
			ASSERT(
			    btstrp_np_mbrshp.members[no.current()] >=
			    m->membership().members[no.current()]);

			// If we haven't heard from the node yet, note as
			// such
			if (btstrp_cl_seqs[no.current()] != current_seq) {
				btstrp_cl_needed++;
			}
		}
	}

	VM_DBG(("calculate_btstrp_cl: btstrp_cl_needed = %u\n",
	    btstrp_cl_needed));

	// Wait until we're either canceled or have received all the
	// messages we need.

	while (btstrp_cl_needed > 0) {
		wait_res = vm_cv.timedwait(get_lock(), timeout_time);
		if (wait_res == os::condvar_t::TIMEDOUT) {
			VM_DBG(("Didn't get canceled in "
			    "calculate_btstrp_cl\n"));
			force_return_transition();
		}
		if (canceled(seqnum) || wait_res == os::condvar_t::TIMEDOUT) {
			unlock();
			return;
		}
	}

	VM_DBG(("calculate_btstrp_cl: all messages received\n"));

	// Verify that all the data we need are in good shape
	for (no.highest(); no.valid(); no.lower()) {
		// We don't care about inactive nodes
		if (m->membership().members[no.current()] == INCN_UNKNOWN) {
			continue;
		}

		// If we've received a new pairwise connection that
		// replaced the info for an active member, we'll be
		// receiving a step cancel soon.  We can't go ahead
		// and calculate because we need the correct pairwise
		// info for that node, and it has been replaced.
		// To act properly, we wait here for the cancel to take
		// place.  That way if we hang here we know something
		// has gone wrong.

		if (btstrp_np_mbrshp.members[no.current()] >
		    m->membership().members[no.current()]) {
			wait_res = os::condvar_t::SIGNALED;
			while (!canceled(seqnum) && (wait_res !=
			    os::condvar_t::TIMEDOUT)) {
				wait_res = vm_cv.timedwait(get_lock(),
				    timeout_time);
			}
			if (wait_res == os::condvar_t::TIMEDOUT) {
				VM_DBG(("Didn't get canceled in "
				    "calculate_btstrp_cl\n"));
			}
			unlock();
			return;
		}

		// We've grabbed the lock and aren't canceled and we've
		// received all the relevant messages, so this assertion
		// should hold
		ASSERT(btstrp_cl_seqs[no.current()] == current_seq);
	}

	// If we get here, we haven't returned on a cancel condition and
	// all messages have been received and verified, so it is safe to
	// run the bootstrap calculation.  We won't drop the lock during
	// the calculation so that we can be sure the pairwise information
	// doesn't change.

	if (btstrp_cl_rv_message != NULL) {
		delete btstrp_cl_rv_message;
		btstrp_cl_rv_message = NULL;
	}

	bool retry_calc = false;
	cmm::membership_t cmm_m = m->membership();

	// Calculate the running versions in the coordinator.
	btstrp_cl_rv_message = the_coord->calculate_btstrp_cl(cmm_m,
	    seqnum, upgrade_seq, retry_calc, shutdown_requested);

	if (retry_calc) {
		ASSERT(btstrp_cl_rv_message == NULL);
		ASSERT(!shutdown_requested);
		// Wait to be canceled so we don't retry in tight loop.
		VM_DBG(("Coordinator requesting retry of btstrp_cl calc.\n"));

		while (!canceled(seqnum) &&
		    (wait_res != os::condvar_t::TIMEDOUT)) {
			wait_res = vm_cv.timedwait(get_lock(), timeout_time);
		}
		if (wait_res == os::condvar_t::TIMEDOUT) {
			force_return_transition();
		}
	} else if (shutdown_requested) {
		//
		// Bootstrap cluster incompatibility
		//
#ifndef LIBVM
		btstrp_cl_rv_message = the_coord->get_rv_stream_by_type(
		    VP_BTSTRP_CLUSTER);
		log_diagnostic(btstrp_cl_rv_message);
		delete btstrp_cl_rv_message;
		btstrp_cl_rv_message = NULL;
#endif
	}

	unlock();
}

//
// Pass the btstrp_cl rvs to vm_admin_impl, or shut down the node.
//
void
vm_comm::commit_btstrp_cl()
{
	lock();

	if (shutdown_requested) {
		ASSERT(btstrp_cl_rv_message == NULL);
		VM_DBG(("Coordinator requesting shutdown of node\n"));
		force_node_shutdown();
		unlock();
		return;
	}

	if (btstrp_cl_rv_message != NULL) {
		the_admin_impl->set_rvs(*btstrp_cl_rv_message,
		    VP_BTSTRP_CLUSTER);
		delete btstrp_cl_rv_message;
		btstrp_cl_rv_message = NULL;

		if (vps_locked) {
			//
			// Perform upgrade_callback on the local
			// replica manager provider and other bootstrap cluster
			// vps.
			//
			the_admin_impl->do_btstrp_cl_callbacks();
		}
	}


	unlock();
}

//
// Call into vm_admin_impl to generate a new cl_message
//
void
vm_comm::generate_cl_message(bool coord_local)
{
	bool new_cl;

	lock();

	new_cl = the_admin_impl->new_cl_message(cl_message);

	if (new_cl)
		cl_sent_to_coord = false;

	if (coord_local) {
		if (coord_node.ndid != orb_conf::local_nodeid()) {
			// We're not actually allocating any memory.  Just
			// noting that we're the coordinator so we know we
			// will be getting coordinator messages
			cl_sent_to_coord = false;
			coord_node.ndid = orb_conf::local_nodeid();
			coord_node.incn = orb_conf::local_incarnation();
		} else {
			ASSERT(coord_node.incn ==
			    orb_conf::local_incarnation());
		}
	}
	unlock();
}

//
// Send the cl_message to the coordinator if necessary
//
void
vm_comm::send_cl_message(callback_membership *m, cmm::seqnum_t seqnum)
{
	Environment e;
	nodeid_t rm_primary;
	version_manager::inter_vm_ptr coordp = NULL;
	bool send_ok;

	rm_primary = rmm::the().primary_location();

	lock();

	if ((coord_node.ndid != rm_primary) ||
	    (coord_node.incn != m->membership().members[rm_primary])) {
		coord_node.ndid = rm_primary;
		coord_node.incn = m->membership().members[rm_primary];
		cl_sent_to_coord = false;
		ASSERT(coord_node.incn != INCN_UNKNOWN);
	}

	ASSERT(cl_message != NULL);

	if (!cl_sent_to_coord) {
		version_manager::vm_stream_container_t tmp_message(
		    (uint_t)cl_message->written_length(),
		    (uint_t)cl_message->written_length(),
		    (unsigned char*)cl_message->_buffer, false);
		if (coord_node.ndid == orb_conf::local_nodeid()) {
			unlock();
			send_ok = send_cl_to_coord(orb_conf::local_nodeid(),
			    seqnum, tmp_message, e);
			ASSERT(!e.exception() && send_ok);
			cl_sent_to_coord = true;
			lock();
		} else {
			unlock();
			coordp = new Anchor<version_manager::inter_vm_stub>
			    (coord_node.ndid, XDOOR_VM,
			    version_manager::inter_vm::_interface_descriptor());
			ASSERT(coordp != NULL);
			send_ok = coordp->send_cl_to_coord(
			    orb_conf::local_nodeid(), seqnum, tmp_message, e);
			CORBA::release(coordp);
			lock();
			if (e.exception()) {
				ASSERT(CORBA::COMM_FAILURE::
				    _exnarrow(e.exception()));
				// If COMM_FAILURE, we must be canceled
				// because we have already reached step 3.
				ASSERT(canceled(seqnum));
			} else if (!send_ok) {
				// If send_ok is false, there must have already
				// been a reconfiguration, because we queried
				// for the location of the coordinator during
				// this step.
				ASSERT(canceled(seqnum));
			} else {
				cl_sent_to_coord = true;
			}
		}
	}
	unlock();
}

//
// Call into the coordinator to calculate cl rvs.
//
// XXX Note that the force_return_transition below could be replaced by
// returning an exception to the calling step.
//
void
vm_comm::calculate_cl(callback_membership *m, cmm::seqnum_t seqnum,
	os::systime *timeout_time)
{
	vm_stream *coord_rv_message;
	nodeset send_to, good_nodes;
	bool retry_calc = false;
	os::condvar_t::wait_result wait_res = os::condvar_t::SIGNALED;
	Environment e;
	node_order no;
	version_manager::inter_vm_ptr remote_vmp;

	lock();

	if (canceled(seqnum) || (coord_node.ndid != orb_conf::local_nodeid())) {
		unlock();
		return;
	}

	// Get the rvs and good nodes from the coordinator
	cmm::membership_t cmm_m = m->membership();
	coord_rv_message = the_coord->calculate_cl(cmm_m, seqnum,
	    upgrade_seq, send_to, good_nodes, retry_calc);

	if (retry_calc) {
		ASSERT(good_nodes.is_empty());
		ASSERT(send_to.is_empty());
		ASSERT(coord_rv_message == NULL);
		// Wait to be canceled so we don't retry in tight loop.
		VM_DBG(("Coordinator requesting retry of calc_cl.\n"));
		while (!canceled(seqnum) &&
		    (wait_res != os::condvar_t::TIMEDOUT)) {
			wait_res = vm_cv.timedwait(get_lock(), timeout_time);
		}
		if (wait_res == os::condvar_t::TIMEDOUT) {
			force_return_transition();
		}
	} else if (send_to.is_empty()) {
		ASSERT(coord_rv_message == NULL);
	} else {
		ASSERT(coord_rv_message != NULL);

		// Send the current message to every host in send_to
		for (no.lowest(); no.valid(); no.higher()) {

			// Skip nodes that aren't in send_to
			if (!send_to.contains(no.current())) {
				continue;
			}

			version_manager::vm_stream_container_t tmp_message(
			    (uint_t)coord_rv_message->written_length(),
			    (uint_t)coord_rv_message->written_length(),
			    (unsigned char *)coord_rv_message->_buffer, false);

			if (no.current() == orb_conf::local_nodeid()) {
				unlock();
				send_cl_rv(seqnum, good_nodes.bitmask(),
				    tmp_message, e);
				ASSERT(!e.exception());
				lock();
				continue;
			}

			ASSERT(no.current() != orb_conf::local_nodeid());
			remote_vmp = new Anchor<version_manager::inter_vm_stub>
			    (no.current(), XDOOR_VM,
			    version_manager::inter_vm::_interface_descriptor());
			ASSERT(remote_vmp != NULL);
			unlock();
			remote_vmp->send_cl_rv(seqnum, good_nodes.bitmask(),
			    tmp_message, e);
			CORBA::release(remote_vmp);
			lock();
			if (e.exception()) {
				ASSERT(CORBA::COMM_FAILURE::
				    _exnarrow(e.exception()));
				ASSERT(canceled(seqnum));
				VM_DBG(("Canceled in calc_cl\n"));
				// Don't bother sending to the rest,
				// they'll get
				// it the next time around
				unlock();
				return;
			}
		}
		delete coord_rv_message;
	}

	unlock();
}

//
// Call into the vm_admin_impl to commit the cl rvs.
//
void
vm_comm::commit_cl_rv()
{
	lock();

	if (shutdown_requested) {
		ASSERT(cl_rv_message == NULL);
		VM_DBG(("Coordinator requesting shutdown of node\n"));
		force_node_shutdown();
		unlock();
		return;
	}

	if (cl_rv_message != NULL) {
		the_admin_impl->set_rvs(*cl_rv_message, VP_CLUSTER);
		delete cl_rv_message;
		cl_rv_message = NULL;		//lint !e423
	}

	unlock();
}

cmm::seqnum_t
vm_comm::start_upgrade_commit()
{
	cmm::seqnum_t	last_upgrade_seq;
	lock();
	try_upgrade = true;
	last_upgrade_seq = upgrade_seq;
	force_return_transition();
	unlock();

	return (last_upgrade_seq);
}

void
vm_comm::finish_upgrade_commit()
{
	lock();
	ASSERT(!try_upgrade);
	the_admin_impl->finish_upgrade_commit();
	unlock();
}

nodeid_t
vm_comm::get_coord_nodeid() {
	return (coord_node.ndid);
}

void
vm_comm::lock_vps_for_upgrade_commit()
{
	lock();
	vps_locked = true;
	unlock();
}

void
vm_comm::unlock_vps_after_upgrade_commit()
{
	lock();
	if (vps_locked) {
		vps_locked = false;
		vp_lock_cv.broadcast();
	}
	unlock();
}

//
// External interface for cl_orb
//
extern "C" void
vm_comm_handle_messages(recstream *r)
{
	vm_comm::handle_messages(r);
}

//
// Receive a VM_MSG
//
void
vm_comm::handle_messages(recstream *r)
{
	the().handle_messages_int(r);
}

//
// Internal handle_messages
//
void
vm_comm::handle_messages_int(recstream *r)
{
	ID_node	&src_node = r->get_src_node();

	ASSERT(r->get_msgtype() == VM_MSG);

	lock();

	bootstrap_header bh;

	ASSERT(r->span() >= sizeof (bh));

	r->get_bytes((void *)&bh, sizeof (bh));

	// Discard old messages and duplicates
	if ((src_node.incn != btstrp_np_mbrshp.members[src_node.ndid]) ||
	    (bh.current_seqnum <= btstrp_cl_seqs[src_node.ndid])) {
		VM_DBG(("Old or dup message from %u %u seq %llu\n",
		    src_node.ndid, src_node.incn, bh.current_seqnum));
		// This is from an old node or a duplicate -- discard
		unlock();
		r->done();
		return;
	}

	VM_DBG(("Received btstrp_cl message from %u %u seq %llu, "
	    "upgrade_seq %llu, full message %c\n", src_node.ndid,
	    src_node.incn, bh.current_seqnum, bh.upgrade_seqnum,
	    (bh.length ? 'y' : 'n')));

	// This seqnum might be higher than the current_seq, because
	// current_seq is set in this same step.  However, we'll ASSERT
	// after current_seq is properly set that none of these messages
	// contained higher sequence numbers
	btstrp_cl_seqs[src_node.ndid] = bh.current_seqnum;

	// If the seqnum matches the current seqnum, we decrement
	// btstrp_cl_needed.  We know that once we're watching
	// btstrp_cl_needed in calc_cluster_bootstrap, current_seq
	// will be set to the proper value.

	if (bh.current_seqnum == current_seq)
		btstrp_cl_needed--;

	vm_stream *vmsp = NULL;

	// If there is a full message, unpack the stream
	if (bh.length != 0) {
		ASSERT(r->span() >= bh.length);
		char *c = new char[bh.length];
		r->get_bytes(c, bh.length);
		vmsp = new vm_stream();
		vmsp->reinit((size_t)bh.length, c, true, true);
	}

	// Send the header info (and possibly a stream) into the coordinator
	the_coord->set_info(VP_BTSTRP_CLUSTER, src_node.ndid, vmsp, bh.qvotes);

	// Delete the vm_stream if we allocated one.
	if (vmsp != NULL) {
		delete vmsp;
		vmsp = NULL;
	}

	// If the reported commit_seqnum from the other node is higher than
	// our own, replace ours with theirs.
	if (bh.upgrade_seqnum > upgrade_seq) {
		upgrade_seq = bh.upgrade_seqnum;
	}

	vm_cv.signal();
	unlock();

	r->done();
}

//
// Called by transports early in path formation handshake
// Unpacks vers_str generating list of names and version
// ranges, which it passes to bootstrap coordinator.  Returns
// whatever boolean the bootstrap coordinator returns.
//
bool
vm_comm::compatible_btstrp_np(nodeid_t n, incarnation_num i,
	size_t len, char *str, bool remote_rejected)
{
	bool retval;

	lock();
	ASSERT(btstrp_np_mbrshp.members[n] <= i);

	if (btstrp_np_mbrshp.members[n] == i) {
		// We've already heard from this node
		// Don't VM_MSG because we could flood the buffer.
		retval = btstrp_np_ok[n];
		unlock();
		return (retval);
	}

	VM_DBG(("remote node contact: btstrp_np msg length %lu, "
	    "node %u (%u)\n", len, n, i));

	if (!vm_stream::valid_stream(len, str)) {
		// If the stream is not of the expected type, return false
		// but don't set btstrp_np_ok[n] to false or change
		// btstrp_np_mbrshp.  This gives the other node another
		// chance to send a message in a format we can understand.
		unlock();
		return (false);
	}

	// This is a new incn
	btstrp_np_mbrshp.members[n] = i;

	if (remote_rejected) {
		// The remote node's coordinator has determined that we are
		// incompatible with it
		VM_DBG(("remote node %u %u rejected us\n", n, i));
		//
		// SCMSGS
		// @explanation
		// This is an informational message from the cluster version
		// manager and may help diagnose which systems have
		// incompatible versions with eachother during a rolling
		// upgrade. This error may also be due to attempting to boot a
		// cluster node in 64-bit address mode when other nodes are
		// booted in 32-bit address mode, or vice versa.
		// @user_action
		// This message is informational; no user action is needed.
		// However, one or more nodes may shut down in order to
		// preserve system integrity. Verify that any recent software
		// installations completed without errors and that the
		// installed packages or patches are compatible with the
		// software installed on the other cluster nodes.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Nodes %u and %u have incompatible versions and will "
		    "not communicate properly.", orb_conf::local_nodeid(), n);
		btstrp_np_ok[n] = false;
		unlock();
		return (false);
	}

	// Unpack the message into a stream and send it to the coordinator
	vm_stream bnp_stream, *bnprv_stream;

	bnp_stream.reinit(len, str, true, false);
	bnprv_stream = the_coord->btstrp_np_handshake(n, &bnp_stream);

	btstrp_np_ok[n] = (bnprv_stream != NULL);

	if (!btstrp_np_ok[n]) {
		VM_DBG(("Rejecting connection from %u %u\n", n, i));
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Nodes %u and %u have incompatible versions and will "
		    "not communicate properly.", orb_conf::local_nodeid(), n);
	} else {
		// Set the running versions for the remote node
		the_admin_impl->set_rvs(*bnprv_stream, VP_BTSTRP_NODEPAIR, n);
		delete bnprv_stream;
	}

	unlock();
	return (btstrp_np_ok[n]);
}

//
// Pass the btstrp_np string to the transport.  The first two bytes are
// the type of the stream. If the caller has passed a non NULL buffer,
// the btstrp_np string is copied to that buffer if there is enough
// space available. If the caller has passed a NULL buffer, this method
// allocates enough memory for the string and copies the string in the
// allocated buffer. Freeing this buffer will be caller's responsibility.
// Upon successful copy, the second inout parameter is set to the length
// of the string and a pointer to the buffer is returned. Upon failure,
// the length parameter is reset to zero and a NULL is returned.
//
char *
vm_comm::get_btstrp_np_string(char *c, size_t &s)
{
	lock();

	ASSERT(btstrp_np_message != NULL);

	if (c == NULL) {
		ASSERT(s == 0);
		s = btstrp_np_message->written_length();
		c = new char[s];
		ASSERT(c != NULL);
	} else if (s < btstrp_np_message->written_length()) {
		s = 0;
		c = NULL;
	} else {
		s = btstrp_np_message->written_length();
	}

	if (c != NULL) {
		bcopy(btstrp_np_message->_buffer, c,
		    btstrp_np_message->written_length());
	}

	unlock();
	return (c);
}

size_t
vm_comm::max_btstrp_np_string()
{
	return (max_btstrp_np_strlen);
}

//
// Force the cmm to reconfigure
//
void
vm_comm::force_return_transition()
{
	Environment e;

	ASSERT(lock_held());

	unlock();

	VM_DBG(("Forcing CMM reconfiguration.\n"));
	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	ASSERT(!CORBA::is_nil(cmm_cntl_v));
	cmm_cntl_v->reconfigure(e);
	ASSERT(!e.exception());

	lock();
}

//
// Force the node to shut down using uadmin.
//
void
vm_comm::force_node_shutdown()
{
	uintptr_t mdep = NULL;

	ASSERT(lock_held());
	unlock();
	VM_DBG(("Forcing node to shutdown via uadmin().\n"));
	//
	// SCMSGS
	// @explanation
	// A node is attempting to join the cluster but it is either using an
	// incompatible software version or is booted in a different mode
	// (32-bit vs. 64-bit).
	// @user_action
	// Ensure that all nodes have the same clustering software installed
	// and are booted in the same mode.
	//
	(void) the_vm_comm->msg.log(SC_SYSLOG_WARNING, MESSAGE,
	    "Node %u attempting to join cluster has incompatible cluster "
	    "software.", orb_conf::local_nodeid());

	//
	// Sleep allows the VM incompatibility error message to display to the
	// console; otherwise, uadmin kills the system too quickly to display
	// any useful message to the user.
	//
	os::usecsleep(VM_SHUTDOWN_WAIT);

	//
	// A_REBOOT with AD_HALT shuts down the node without
	// waiting for any OS threads to exit.  The expected
	// behaviour of uadmin would be for this node to
	// be brought down to the OBP.
	//
	(void) uadmin(A_REBOOT, AD_HALT, mdep);
	//
	// uadmin should not return from A_REBOOT, but if it fails to shutdown
	// the node, then the CL_PANIC will guarantee that the node dies.
	//
	CL_PANIC(0);
}

//
// Returns true if an upgrade commit is needed.
//
// To correctly determine if a commit is needed, all nodes require up-to-date
// rvs and uvs information.  A CMM reconfig is invoked and must complete to
// guarantee that all nodes have exchanged up-to-date vm_streams to ensure a
// correct answer.
//
bool
vm_comm::is_upgrade_needed(version_manager::vp_state &s, nodeset &n)
{
	Environment	e;
	bool		ret;
	cmm::seqnum_t	old_seq;

	VM_DBG(("vm_comm::is_upgrade_needed()\n"));
	ASSERT(lock_held());
	// The completion of the CMM reconfiguration must signal the vm_comm.
	signal_needed = true;

	// Remember the last is_upgrade_needed sequence number
	old_seq = last_iun_seqnum;
	//
	// Trigger a CMM reconfiguration.  Only after the CMM
	// reconfig is finished exchanging up-to-date vp information is it
	// correct to call vm_coord::is_upgrade_needed().
	//
	// Start the CMM reconfig.
	force_return_transition();
	// Wait til the CMM reconfiguration completes.
	do {
		VM_DBG(("Waiting for the CMM reconfiguration to finish...\n"));
		vm_cv.wait(&vm_comm_lock);
	} while (old_seq == last_iun_seqnum);
	//
	// At this point, it's safe to determine if an upgrade commit is
	// needed.
	//
	ret = the_vm_coord().is_upgrade_needed(s, n);
	signal_needed = false;
	return (ret);
}

//
// Called by the CMM reconfiguration step mark_joined_step to
// indicate that the CMM configuration has finished and that it's safe to
// continue with vm_coord::is_upgrade_needed().
//
void
vm_comm::signal_vm_comm(cmm::seqnum_t seq)
{
	ASSERT(lock_held());
	last_iun_seqnum = seq;
	vm_cv.broadcast();
}
