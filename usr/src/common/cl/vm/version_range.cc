//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)version_range.cc	1.15	08/05/20 SMI"

#include <vm/version_range.h>
#include <vm/vm_dbgbuf.h>

//
// Initialize the version range object as specified by an array of triads.
// reinit will reuse the existing array if it can, but it does not reserve
// extra space if it cannot.  If you want extra space, call grow() with
// appropriate arguments first.
//
void
version_range::reinit(uint16_t s, vp_triad * t)
{
	if (s > size) {
		delete_versions();
		triad_array = new vp_triad[s];
		size = s;
	}

	ASSERT((s == 0) || (triad_array && t));
	count = s;

	if (count) {
		ASSERT(triad_array);
		for (uint16_t i = 0; i <= (count - 1); i++) {
			triad_array[i].assign(t[i]);
		}
	}
	ASSERT(validate());
}

//
// returns true if v is defined in the range
//
int
version_range::index_of(const version_manager::vp_version_t &v)
{
	if (count == 0)
		return (-1);

	int cur = count/2, lower = 0, upper = count, cmp;

	while (lower < upper) {
		cmp = triad_array[cur].compare(v);
		if (cmp == 0) {
			return (cur);
		} else if (cmp < 0) {
			lower = cur + 1;
		} else {
			upper = cur;
		}
		cur = (lower + upper)/2;
	}
	return (-1);
}

//
// Subtraction operator
// For readability,
//	Let 'A' = 'this', so 'result' = 'A' - 'B'
//
// Result will contain every major/minor number combination defined in the
// triad array in 'this' and not defined in the triad array in B.
//
void
version_range::subtraction(const version_range &B, version_range &result)
{
	uint16_t i = 0, j = 0;
	int max_cmp;
	version_range &A = *this;

	result.empty();

	// The algorithm is O(N) in the number of triads in both 'this' (A)
	// and B.  In the outer loop we walk through each triad in A, and in
	// the inner loop we walk the triads in B until the highest version
	// in the triad we're looking at in B exceeds the highest version in
	// the current triad in A.
	while (i < A.count) {

		// If we're running low on space in the result, grow it
		// proportionally to the number of triads in B left to examine.
		// The algorithm below examines one triad from A and loops
		// through triads in B attempting to subtract.  The worst
		// possible case is that every triad in B splits the triad
		// from A, in which case we will need (#triads in B) + 1.
		// If we are out of triads in B, we only need room for each
		// remaining triad in A.  Note the statements that ASSERT
		// the memory is available before the assignments or
		// manipulation of reference variables.

		if (result.count + 1 >= result.size) {
			if (j < B.count) {
				result.grow(B.count - j + 1);
			} else if (result.count >= result.size) {
				result.grow(A.count - i);
			}
		}

		// Start by copying the triad from A into the current triad
		// in result.
		ASSERT(result.count < result.size);
		(result.triad_array[result.count]).assign(A.triad_array[i]);

		if (j == B.count) {
			// If we've already reached the end of B, we just use
			// the outer loop to copy the triads from A into result.
			result.count++;
			i++;
		} else while (j < B.count) {
			ASSERT((result.count + 1) < result.size);
			vp_triad &R = result.triad_array[result.count];
			vp_triad &Rnext = result.triad_array[result.count+1];

			// Do the subtraction, leaving the results in the
			// current triad in R and the one adjacent to it.
			max_cmp = R.subtraction(B.triad_array[j], Rnext);

			// If Rnext wasn't empty, the current triad in B split
			// the current triad in A in half.  Because B is sorted,
			// we know that no higher triad in B will intersect
			// with the lower part of the split triad, so we can
			// safely put that into 'result' and move on.
			if (!Rnext.is_empty()) {
				ASSERT(max_cmp != 0);
				result.count++;
			}

			// If A had the higher triad, we need to look at the
			// next triad in B.
			if (max_cmp >= 0) {
				j++;
			}

			// If B had the higher triad, or if there are no more
			// triads in B, we need to look at the next triad in A,
			// which means incrementing i and then breaking out
			// of the inner while loop.  We'll be copying the next
			// triad in A into result, so we need to make sure to
			// have result.count point to a blank one, saving the
			// current highest array if it isn't empty (which it
			// will be if the highest triad in B completely
			// intersected with the highest in A.
			if ((max_cmp <= 0) || (j == B.count)) {
				if (!(result.triad_array[result.count]).
				    is_empty())
					result.count++;
				i++;
				break;
			}
		}
	}
	ASSERT(result.validate());
}

//
// Intersection
//
// When this is run, result will contain every major/minor pair that was
// listed in both 'this' and B.
//
void
version_range::intersection(const version_range &B, version_range &result)
{
	uint16_t i = 0, j = 0;
	version_range &A = *this;

	// Empty out result
	result.empty();

	// The algorithm walks A and B simultaneously, incrementing past
	// whichever triad is lower.

	while ((i < A.count) && (j < B.count)) {
		if (result.count == result.size) {
			// Extend by a good guess
			result.grow(A.count - i);
		}
		ASSERT(result.count < result.size);

		vp_triad &R = result.triad_array[result.count];

		// Do the underlying triad intersection
		int max_cmp =
		    (A.triad_array[i]).intersection(B.triad_array[j], R);

		if (!R.is_empty()) {
			// If there was a value, store it.
			result.count++;
		}
		// If A's highest triad was higher, or the highest value in
		// both triads was equal, use the next triad in B.
		if (max_cmp >= 0) {
			j++;
		}
		// If B's highest triad was higher, or the highest value in
		// both triads was equal, use the next triad in A.
		if (max_cmp <= 0) {
			i++;
		}
	}
	ASSERT(result.validate());
}

//
// Insert this triad into the sorted array of triads.
//
// Note that the argument is passed by value.  This is done purposely
// because the value of t can be modified by this function
//
// The return value will be false if the triad does not overlap with
// any other triad in 'this'.  If there is overlap, true will be
// returned.  In either case the triad will be included in 'this'.
//
bool
version_range::insert_triad(vp_triad t)
{
	int l, u = count;
	bool retval = false;

	ASSERT(t.is_valid());
	ASSERT(count <= size);

	// We could also do a binary search here, but for now we'll just
	// do a linear search for the insertion starting point.  We search
	// from the top because we often expect triads to be added in
	// increasing order
	//
	// When this is done, l will point to the triad that is strictly
	// less than t, and u will point to the triad that is strictly greater
	// than t.  If t is lower than all the triads in 'this', l will be -1,
	// and if it is greater than all triads, u will be count.
	for (l = count-1; l >= 0; l--) {
		if (t.lt(triad_array[l]))
			u = l;
		if (t.gt(triad_array[l]))
			break;
	}

	if (u - l != 1) {
		// If l and u aren't adjacent, t overlaps with at least one
		// triad in this, so we will return VP_DUPMAJMIN.
		retval = true;

		// We know t overlaps with l + 1.  minor_min in l + 1 might
		// be lower than minor_min in t, in which case we need to
		// copy it in.
		if (t.get_minor_min() > triad_array[l + 1].get_minor_min()) {
			t.assign_minor_min(triad_array[l + 1].get_minor_min());
		}

		// We know t overlaps with u - 1.  minor_max in u - 1 might
		// be higher than minor_max in t, in which case we need to
		// copy it in.
		// u - 1 might be the same triad as l + 1, which is fine.
		if (t.get_minor_max() < triad_array[u - 1].get_minor_max()) {
			t.assign_minor_max(triad_array[u - 1].get_minor_max());
		}
	}

	// If l is adjacent to t, we decrement l and put its lower minor
	// number into t, so that when we insert t later we will get the
	// right result.
	if ((l >= 0) && t.adj(triad_array[l])) {
		t.assign_minor_min(triad_array[l].get_minor_min());
		l--;
	}

	// If u is adjacent to t, we increment u and put its higher minor
	// number into t
	if ((u < count) && t.adj(triad_array[u])) {
		t.assign_minor_max(triad_array[u].get_minor_max());
		u++;
	}

	// We need enough room for the existing triads in 'this' (count),
	// minus the number of triads we'll remove (u - l - 1), plus room
	// for adding t (1).  The worst case is we aren't removing any
	// triads, and need room for one more, so grow with no arguments
	// will always give us that.
	if ((count - (u - l) + 2) >= size) {
		grow();
	}

	// If t isn't higher than all triads in 'this', and if we aren't
	// removing exactly 1 triad that intersects with or is adjacent to
	// t, we need to move the higher triads to leave just one slot for t.
	// bcopy is safe for moving overlapping chunks of memory.
	if ((u < count) && (u - l != 2)) {
		bcopy(triad_array + u, triad_array + l + 2,
		    vp_triad_size * (count - (unsigned int)u));
	}

	// Update to the new count.
	count = count - (uint16_t)(u - l) + 2;

	ASSERT(l + 1 < count);

	// Insert t into the array.
	triad_array[l+1].assign(t);
	triad_array[l+1].assign_description(t.unlink_description());

	ASSERT(validate());

	return (retval);
}

//
// Merge two version_range objects together.
//
// For readability,
//	Let 'A' = 'this', so 'result' = 'A' merge 'B'
//
// merge returns false if the intersection of 'this' and B is empty.
// if there is an intersection, the merge will complete successfully, but
// the return value will be true.
//
bool
version_range::merge(const version_range &B, version_range &result)
{
	uint16_t i = 0, j = 0;
	version_range &A = *this;
	vp_triad *n;
	bool retval = false;

	// Empty out result
	result.empty();
	result.grow(A.count);

	// We walk both lists simultaneously, as we did in the intersection.
	while ((i < A.count) || (j < B.count)) {
		if (result.count == result.size) {
			// Extend by a good guess
			result.grow(A.count - i + B.count - j);
		}

		// Make n point to the lower of the two current triads, and
		// increment the list we picked it from.
		if ((i == A.count) || ((j < B.count) &&
		    (B.triad_array[j]).lower(A.triad_array[i]))) {
			n = &(B.triad_array[j]);
			j++;
		} else {
			ASSERT((j == B.count) ||
			    !(B.triad_array[j]).lower(A.triad_array[i]));
			n = &(A.triad_array[i]);
			i++;
		}

		ASSERT(n != NULL);

		// If this is the first time through the loop, just put
		// n into the first slot in result and go through the next
		// loop iteration.
		if (result.count == 0) {
			(result.triad_array[0]).assign(*n);
			result.count++;
			continue;
		}

		// Otherwise, triad merge n into the current highest triad
		// in result.  If they overlap or are adjacent, the merge
		// function itself will add n to result.count-1.  If they
		// don't, 'case 0' increments count and assigns n into the
		// next slot.  If merge indicated the two triads intersected,
		// we note that the return value will be VP_DUPMAJMIN, in case
		// the caller cares.
		switch ((result.triad_array[result.count-1]).merge(*n)) {
		case 0:
			(result.triad_array[result.count]).assign(*n);
			result.count++;
			break;
		case -1:
			retval = true;
			break;
		default:
			break;
		}
	}

	ASSERT(result.validate());
	return (retval);
}

//
// This is a debugging facility for printing a version range into the
// VM debug buffer
//
void
version_range::dbprint()
{
	VM_DBGC(("("));
	for (int i = 0; i < count; i++) {
		triad_array[i].dbprint();
		if (i != (count - 1))
			VM_DBGC((", "));
	}
	VM_DBGC((")"));
}

//
// Private methods
//

//
// Resize to increase the sorted array of triads.
// The array is increased by slightly more than is needed so in case we need
// to insert another triad, the space will already be allocated.  This reduces
// the number of times that this function is invoked.  In short, we are
// using preallocation here for efficiency.
//
void
version_range::grow(uint16_t growby)
{
	size += growby;

	if (size > 0) {
		vp_triad *new_versions = new vp_triad[size];
		ASSERT(new_versions);

		// Copy the data over.
		if (count > 0) {
			ASSERT(triad_array);
			for (uint16_t i = 0; i < count; i++) {
				// zero-copy for description string (if any)
				new_versions[i].assign(triad_array[i]);
				new_versions[i].assign_description(
				    triad_array[i].unlink_description());
			}
		}

		// Now free the memory.
		delete_versions();

		ASSERT(triad_array == NULL);
		// finish off by assigning to the new bigger array of triads.
		triad_array = new_versions;
	}
}

//
// Validates the input of triads list 't'.
// A valid array of triads consists of triads that are strictly increasing
// and don't intersect.
//
bool
version_range::validate()
{
	for (uint16_t i = 0; i < (count - 1); i++)
		if (!triad_array[i].lt(triad_array[i+1]) ||
		    triad_array[i].adj(triad_array[i+1]) ||
		    !triad_array[i].is_valid())
			return (false);

	if ((count != 0) && !triad_array[count - 1].is_valid())
		return (false);

	return (true);
}
