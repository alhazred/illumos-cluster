/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_SET_IN_H
#define	_VP_SET_IN_H

#pragma ident	"@(#)vp_set_in.h	1.12	08/05/20 SMI"

//
// Constructor - Create a valid vp_set object without allocating the vr_list.
//
inline
vp_set::vp_set() :
	_SList::ListElem(this),
	size(0),
	valid(true),
	vr_list(NULL),
	last_node(NODEID_UNKNOWN),
	score(0)
{
}

//
// Constructor - Create a vp_set object and allocates the vr_list.  valid is
// initialized to false because every version range will start out empty.
//
inline
vp_set::vp_set(uint16_t s) :
	_SList::ListElem(this),
	size(s),
	valid(false),
	vr_list(new version_range[s]),
	last_node(NODEID_UNKNOWN),
	score(0)
{
	ASSERT(s != 0);
	ASSERT(vr_list);
}

//
// Copy Constructor - Create a completely specified vp_set object
//
inline
vp_set::vp_set(const vp_set &v) :
	_SList::ListElem(this),
	size(v.size),
	valid(v.valid),
	last_node(v.last_node),
	score(v.score)
{
	vr_list = new version_range[size];
	ASSERT(vr_list);
	for (int i = 0; i < size; i++)
		vr_list[i].reinit(v.vr_list[i]);
}

//
// Destructor
//
inline
vp_set::~vp_set()
{
	if (vr_list != NULL) {
		delete [] vr_list;
		vr_list = (version_range *)NULL;
	}
	valid = true;
	size = 0;
}

inline version_range &
vp_set::get_vr(uint16_t i)
{
	ASSERT(i < size);
	return (vr_list[i]);
}

inline void
vp_set::empty()
{
	if (size != 0)
		for (int i = 0; i < size; i++)
			vr_list[i].empty();
}

//
// Is this vp_set object holding valid version_range objects?
//
inline bool
vp_set::is_valid()
{
	return (valid);
}

inline void
vp_set::set_valid()
{
	if (size != 0) {
		valid = true;
		for (int i = 0; i < size; i++)
			if (vr_list[i].is_empty())
				valid = false;
	}
}

inline void
vp_set::assign_score(uint32_t s)
{
	score = s;
}

inline void
vp_set::assign_node(nodeid_t n)
{
	last_node = n;
}

inline nodeid_t
vp_set::get_node()
{
	return (last_node);
}

inline uint16_t
vp_set::get_size()
{
	return (size);
}

#endif	/* _VP_SET_IN_H */
