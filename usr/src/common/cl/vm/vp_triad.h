/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_TRIAD_H
#define	_VP_TRIAD_H

#pragma ident	"@(#)vp_triad.h	1.10	08/05/20 SMI"

#include <sys/os.h>
#include <h/version_manager.h>
#include <vm/vm_dbgbuf.h>

//
// Version Manager triad Class Definition
//
// triad objects are simply three uint16_ts representing the major,
// the minimum minor, and the maximum minor value pairs.  For convenience,
// the minor numbers are represented as a range, and this is interpretted
// as if each minor number between the minimum and maximum had been
// explicitly specified.
//

typedef struct vm_triad_version_t {
	uint16_t major_num;	// Version major number
	uint16_t minor_max;	// Version minor number maximum
	uint16_t minor_min;	// Version minot number minimum
} vm_triad_version;

class vp_triad {
public:
	vp_triad();
	vp_triad(uint16_t, uint16_t, uint16_t);
	~vp_triad();

	// A triad is "empty" if its major number is zero.
	// Return 'true' if triad is empty.
	bool is_empty();

	// A triad is valid if ((major_num > 0) && (minor_min <= minor_max))
	bool is_valid();

	// Reset the triad to all zeros
	void zero();

	// Reset the triad to a specified value.
	void assign(uint16_t, uint16_t, uint16_t);
	void assign_description(char*);

	// Get/Unlink triad descriptor
	char *get_description();
	char *unlink_description();

	// Reset the triad to the values in another triad (assignment)
	void assign(const vp_triad &);

	// Return 0 if the specified version is contained in the triad,
	// -1 if the versions in the triad are lower than v, and 1 if the
	// versions in the triad are higher than v.
	int compare(const version_manager::vp_version_t &v);

	// Subtract one triad from another.  The difference is returned
	// in "this" and in remainder
	int subtraction(const vp_triad &t, vp_triad &remainder);

	// Intersect one triad with another.  The intersection can be
	// expressed in a single triad.  The return value will be 0 if both
	// triads have the same highest version, -1 if the highest version in
	// 'this' is lower than the highest version in t, and -1 if it is
	// higher.
	int intersection(const vp_triad &t, vp_triad &i);

	// Specialty function for version_range::merge.  If 'this' intersects
	// with or is adjacent to t, t is added into this, otherwise 'this' is
	// left unchanged.  This function assumes and ASSERTs that the lowest
	// value in 'this' is less than or equal to the lowest value in t.
	//
	// The return value is 0 if the highest value in 'this' is lower than
	// and not adjacent to the lowest value in t, and therefore they won't
	// be combined.  It is 1 if the two triads intersect, and -1 if they
	// don't intersect and are adjacent.
	int merge(const vp_triad &t);

	// Compare two triads together to determine if they are equal.
	bool eq(const vp_triad &t);

	// Compare two triads together to determine if this < t
	bool lt(const vp_triad &t);

	// Compare two triads together to determine if this > t
	bool gt(const vp_triad &t);

	// Returns true if "this" is immediately adjacent to the argument,
	// false otherwise
	bool adj(const vp_triad &t);

	// True if the lowest version in 'this' is lower than the argument
	bool lower(const vp_triad &t);

	// Return the major number
	uint16_t get_major() const;

	// Return the minor maximum number
	uint16_t get_minor_max() const;

	// Return the minor minimum number
	uint16_t get_minor_min() const;

	// Assign the minor maximum number
	void assign_minor_max(uint16_t min_max);

	// Assign the minor minimum number
	void assign_minor_min(uint16_t min_min);

	// Print the triad into the vm debug buffer, if any
	void dbprint();

	// Get Triad version information
	vm_triad_version *get_triad_version();

private:
	vm_triad_version triad; // version information

	//
	// The description attribute is not manipulated by
	// most of the inline vp_triad methods which are
	// dedicated to version major/minor numbers
	// arithmetic.
	//
	char *description;	// Version description

	vp_triad &operator = (vp_triad &);
};

//
// Constants
//
const size_t vp_triad_size = sizeof (vp_triad);
const size_t vp_triad_version_size = sizeof (vm_triad_version);

#ifndef	NOINLINES
#include <vm/vp_triad_in.h>
#endif	// NOINLINES

#endif	/* _VP_TRIAD_H */
