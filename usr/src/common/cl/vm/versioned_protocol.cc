//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)versioned_protocol.cc	1.27	09/02/18 SMI"

#include <vm/versioned_protocol.h>
#include <sys/os.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vp_rhs_version.h>
#include <vm/vm_admin_int.h>
#include <vm/vm_stream.h>

#ifndef LIBVM
#include <nslib/ns.h>
#include <vm/vm_comm.h>
#endif

//
// Constructor - Create a valid versioned_protocol object without allocating
// the vr_list.
//
versioned_protocol::versioned_protocol() :
	_SList::ListElem(this),
	vp_file_version(0),
	vp_name(NULL),
	vp_description(NULL),
	vp_source(NULL),
	vp_custom_callback(NULL),
	the_type(VP_UNINITIALIZED),
	vp_attributes(0),
	running_version(NULL)
{
	upgrade_version.major_num = upgrade_version.minor_num = 0;
}

//
// Constructor - Create a validated versioned protocol object.
//
versioned_protocol::versioned_protocol(int ffv, char *name, char *description,
	char *source, char *ccb, vp_type vt, int attributes) :
	_SList::ListElem(this),
	vp_file_version((uint16_t)ffv),
	vp_name(os::strdup(name)),
	vp_description(os::strdup(description)),
	vp_source(os::strdup(source)),
	the_type(vt),
	vp_attributes(attributes)
{
	ASSERT(vp_name);
	ASSERT(vp_description);
	ASSERT(vp_source);

	if (ccb != NULL) {
		vp_custom_callback = os::strdup(ccb);
		ASSERT(vp_custom_callback);
	} else {
		vp_custom_callback = NULL;
	}
}

//
// Destructor
//
versioned_protocol::~versioned_protocol()
{
	// ASSERT that our count is back down to 1, for ourselves
	ASSERT(verify_count(1));

	vp_dependlist.dispose();
	vp_rev_dependlist.dispose();
	vp_upgradelist.dispose();

	if (vp_name) {
		delete [] vp_name;
		vp_name = NULL;
	}
	if (vp_description) {
		delete [] vp_description;
		vp_description = NULL;
	}
	if (vp_source) {
		delete [] vp_source;
		vp_source = NULL;
	}
	if (vp_custom_callback) {
		delete [] vp_custom_callback;
		vp_custom_callback = NULL;
	}
	if (running_version) {
		delete [] running_version;
		running_version = NULL;
	}
}

// Unlink the forward dependencies and remove their corresponding reverse
// dependencies, in preparation for deletion.
void
versioned_protocol::unlink_dependencies()
{
	vp_dependency *vpd;
	vp_rhs_version *vrhs;

	IntrList<vp_dependency, _SList>::ListIterator depi(vp_dependlist);
	while ((vpd = depi.get_current()) != NULL) {
		IntrList<vp_rhs_version, _SList>::ListIterator
		    rhsi(vpd->get_rhs_list());
		while ((vrhs = rhsi.get_current()) != NULL) {
			vrhs->unlink(this);
			rhsi.advance();
		}
		depi.advance();
	}
}

versioned_protocol *
versioned_protocol::get_vpp()
{
	return (this);
}

coord_vp *
versioned_protocol::get_cvpp()
{
	ASSERT(0);
	return (NULL);
}

//
// Set version to the current running version. If no version has been set,
// return false.
//
bool
versioned_protocol::get_running_version(version_manager::vp_version_t &v,
	sol::nodeid_t n)
{
#ifdef	DEBUG
	if (the_type != VP_BTSTRP_NODEPAIR && the_type != VP_NODEPAIR) {
		ASSERT(n == 0);
	} else {
		ASSERT(n != NODEID_UNKNOWN && n <= NODEID_MAX);
	}
#endif
#ifdef _KERNEL_ORB
	ASSERT(vm_comm::the().lock_held());
	//
	// We could be running CMM version 0(old version). We always return
	// the highest version supported in this case as the CMM steps that
	// execute the VM initialization are disabled. We also have to
	// consider the case when we just transitioned to CMM version 1.
	// This transition happens in the CMM BEGIN state. The VM is
	// initialized after CMM_STEP_2 is complete. From the BEGIN state
	// till CMM_STEP_2, we have to return the highest supported version.
	// version_changed will be reset in CMM_STEP_3.
	//
	bool use_old_version = cmm_impl::the().is_old_version();
	bool version_change = cmm_impl::the().version_changed();
	if (use_old_version || version_change) {
		consistent_versions.highest_version(v.major_num, v.minor_num);
	} else {
		v.major_num = running_version[n].major_num;
		v.minor_num = running_version[n].minor_num;
	}
#else
	v.major_num = running_version[n].major_num;
	v.minor_num = running_version[n].minor_num;
#endif // _KERNEL_ORB

	if (v.major_num == 0) {
		return (false);
	} else {
		return (true);
	}
}

//
// Assign data members.
//

//
// Set the running version.
//
void
versioned_protocol::set_running_version(
    const version_manager::vp_version_t &v,
    sol::nodeid_t n, vm_stream* s)
{
#ifdef	DEBUG
	if (the_type != VP_BTSTRP_NODEPAIR && the_type != VP_NODEPAIR) {
		ASSERT(n == 0);
	} else {
		ASSERT(n != NODEID_UNKNOWN && n <= NODEID_MAX);
	}
#endif
#ifdef _KERNEL_ORB
	ASSERT(vm_comm::the().lock_held());
#endif // _KERNEL_ORB
	ASSERT(running_version != NULL);
	ASSERT(v.major_num != 0);

	running_version[n].major_num = v.major_num;
	running_version[n].minor_num = v.minor_num;

	// Query for new custom versions if necessary
	if (the_type != VP_BTSTRP_NODEPAIR && the_type != VP_BTSTRP_NODE) {
		int f = custom_query(s);
		// Panic if custom query fails
		CL_PANIC(f == VP_SUCCESS);
	}
}

void
versioned_protocol::assign_vp(int ffv, char *name, char *description,
	char *source, char *ccb, vp_type vt, int attributes)
{
	ASSERT(name);
	ASSERT(source);
	ASSERT(description);

	// Free any existing memory
	if (vp_name)
		delete [] vp_name;
	if (vp_description)
		delete [] vp_description;
	if (vp_source)
		delete [] vp_source;
	if (vp_custom_callback)
		delete [] vp_custom_callback;
	// Allocate and copy data members
	vp_name = name;
	ASSERT(vp_name);
	vp_description = description;
	ASSERT(vp_description);
	vp_source = source;
	ASSERT(vp_source);

	// May be NULL
	vp_custom_callback = ccb;

	// Assign the file format version, type, and attributes data members
	vp_file_version = (uint16_t)ffv;

	// We allow the unsupported values for now and clean them up in the
	// second stage.
	switch (vt) {
	case VP_NODE:
		if (attributes & VP_BOOTSTRAP)
			the_type = VP_BTSTRP_NODE;
		else
			the_type = VP_NODE;
		break;
	case VP_NODEPAIR:
		if (attributes & VP_BOOTSTRAP)
			the_type = VP_BTSTRP_NODEPAIR;
		else
			the_type = VP_NODEPAIR;
		break;
	case VP_CLUSTER:
		if (attributes & VP_BOOTSTRAP)
			the_type = VP_BTSTRP_CLUSTER;
		else
			the_type = VP_CLUSTER;
		break;
	default:
		// Shouldn't be possible...there would be a syntax error
		// before we reached this point
		ASSERT(0);
		break;
	}

	vp_attributes = attributes;
}

//
// This is where we do the following:
//
// 1) Check for a number of error conditions in the file
// 2) Take the dependency list and link put the direct versioned_protocol
//    pointers, discarding the names
// 3) Deal with missing dependencies, if they exist
// 4) Insert reverse dependency entries
// 5) Call the custom interface the first time, if necessary.
//
int
versioned_protocol::verify_and_link(vm_admin_int &vm_admin)
{
	int r;

	if (the_type == VP_NODE || the_type == VP_NODEPAIR) {
		VM_DBG(("vp '%s' has type %d but no Bootstrap attribute\n",
		    vp_name, the_type));
		// XXX syslog
		// See VM_DBG above
		return (VP_NOTBOOTSTRAP);
	}

	if (is_custom() && !vp_custom_callback) {
		VM_DBG(("vp '%s' is marked Custom but has no callback name\n",
		    vp_name));
		// XXX syslog
		// See VM_DBG above
		return (VP_NOCUSTOMCB);
	}

	if (vp_custom_callback && !is_custom()) {
		VM_DBG(("vp '%s' isn't marked Custom but has a callback name\n",
		    vp_name));
		// XXX syslog
		// See VM_DBG above
		return (VP_NOTCUSTOM);
	}
	version_range tmp_vr, every_upgrade, every_left_upgrade;

	versioned_protocol *rhs_vp;
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	vp_upgrade *vup;

	//
	// This is where we
	// o initially set consistent_versions,
	// o check for problems with dependencies (such as a dependency on a
	//   missing vp),
	// o set the direct pointer rather than the name in the vp_dependency
	//   objects, and
	// o insert reverse dependencies
	//
	IntrList<vp_dependency, _SList>::ListIterator depi(vp_dependlist);
	while ((vpd = depi.get_current()) != NULL) {
		ASSERT(!vpd->is_bad());

		IntrList<vp_rhs_version, _SList>::ListIterator
		    rhsi(vpd->get_rhs_list());
		while ((vrhs = rhsi.get_current()) != NULL) {
			ASSERT(vrhs->get_name() && !vrhs->get_vpp());

			// Get a pointer to the vp the vp_rhs_version is
			// referring to
			rhs_vp = vm_admin.get_vp_by_name(vrhs->get_name());

			if (rhs_vp == NULL) {
				// If 1.0 is defined in the dependency, it is
				// ok for the vp spec file to be missing.
				// If 1.0 isn't defined, we mark the dependency
				// bad.
				uint16_t vmaj, vmin;
				vrhs->get_vr().lowest_version(vmaj, vmin);
				if (!(vmaj == 1 && vmin == 0)) {
					// We don't use bad_dependency here
					// because we're still building
					// consistent_versions.
					vpd->mark_bad();
				}
			} else {
				// A versioned protocol can't depend on itself.
				if (rhs_vp == this) {
					VM_DBG(("vp %s depends on itself\n",
					    vp_name));
					// XXX Syslog
					// See VM_DBG message
					return (VP_INVALIDDEP);
				}
				// Link up and create the reverse dependency
				r = check_dependency_type(
				    rhs_vp->get_vp_type());
				if (r != VP_SUCCESS) {
					// Syslog handled from
					// check_dependency_type
					return (r);
				}
				// link up the forward and reverse dependencies
				vrhs->link(this, rhs_vp);
			}
			rhsi.advance();
		}

		if (consistent_versions.merge_equals(vpd->get_lhs())) {
			VM_DBG(("vp '%s' has the same version in "
			    "two Version lines\n", vp_name));
			// XXX syslog
			// See VM_DBG above
			return (VP_DUPMAJMIN);
		}

		if (vpd->is_bad()) {
			(void) inconsistent_versions.
			    merge_equals(vpd->get_lhs());
		}
		depi.advance();
	}

	// Take the inconsistent_versions out of consistent_versions
	consistent_versions.subtract_equals(inconsistent_versions);

	// If none of our version lines was valid, we bomb out.
	if (consistent_versions.is_empty()) {
		VM_DBG(("vp '%s' has no valid versions not invalidated by "
		    "bad dependencies\n", vp_name));
		// There are no valid versions that can be picked.  At this
		// point, this can only be due to dependencies on files that
		// are missing.
#ifndef LIBVM
		vm_comm::the_vm_coord().log_diagnostic(vp_name, 0, NULL);
#endif
		return (VP_NOVERSIONS);
	}

	//
	// This verifies the contents of each upgrade lines are consistent
	// with the other upgrade and version lines.
	//
	IntrList<vp_upgrade, _SList>::ListIterator upi(vp_upgradelist);
	while ((vup = upi.get_current()) != NULL) {

		// Check the consistency of the line itself
		if (!vup->get_lhs().lt(vup->get_rhs())) {
			VM_DBG(("vp '%s': The versions on the left in an "
			    "Upgrade line are not strictly less than the "
			    "versions on the right\n", vp_name));
			// XXX syslog
			// See VM_DBG above
			return (VP_INVALIDUPGRADE);
		}

		// Check the consistency with other lines
		if (every_left_upgrade.merge_equals(vup->get_lhs())) {
			VM_DBG(("vp '%s' has the same version in two Upgrade"
			    " lines\n", vp_name));
			// XXX syslog
			// See VM_DBG above
			return (VP_DUPUPGRADE);
		}

		// Merge left and right sides into every_upgrade
		(void) vup->get_lhs().merge(vup->get_rhs(), tmp_vr);
		(void) every_upgrade.merge_equals(tmp_vr);

		upi.advance();
	}

	// Check for upgrade versions not defined in version lines
	tmp_vr.reinit(every_upgrade);
	tmp_vr.subtract_equals(consistent_versions);
	tmp_vr.subtract_equals(inconsistent_versions);

	if (!tmp_vr.is_empty()) {
		VM_DBG(("vp '%s' has at least one version in an Upgrade line "
		    "not listed in a Version line\n", vp_name));
		// XXX syslog
		// See VM_DBG above
		return (VP_INVALIDUPGRADE);
	}

	// Allocate the array of running versions
	size_t rvsize = 1;
	if (the_type == VP_BTSTRP_NODEPAIR || the_type == VP_NODEPAIR)
		rvsize = NODEID_MAX+1;

	running_version = new version_manager::vp_version_t[rvsize];
	bzero(running_version, sizeof (version_manager::vp_version_t) * rvsize);

	int f;

	// Perform the first custom query, if required
	if ((f = custom_query(vm_admin.get_cv_stream())) != VP_SUCCESS) {
		// Syslog is handled in custom_query
		return (f);
	}

	//
	// Build the potential_versions based on the contents of
	// consistent_versions and custom_versions.  These will first be
	// used in the local_consistency algorithm.
	//
	rebuild_potential_versions();

	if (potential_versions.is_empty()) {
		// The custom query eliminated all the potential versions.
		// This suggests an incompatibility between the installed
		// spec files and the running state.

#ifndef LIBVM
		vm_comm::the_vm_coord().log_diagnostic(vp_name, 0, NULL);
#endif
		return (VP_NOVERSIONS);
	}

	return (VP_SUCCESS);
}

//
// Sets the value of a bootstrap node versioned_protocol to the highest
// consistent version, and then marks any dependency not consistent
// with that value as bad.
//
void
versioned_protocol::pick_node_mode_rv()
{
	ASSERT(the_type == VP_BTSTRP_NODE);

	version_range tmp_vr;
	version_manager::vp_version_t &rv = running_version[0];
	//
	// If we are part of a connected subgroup, rebuild the potential
	// versions, which may have changed as a result of the local
	// consistency check.
	//
	if (get_ordinal() != 0)
		rebuild_potential_versions();

	ASSERT(!potential_versions.is_empty());

	potential_versions.highest_version(rv.major_num, rv.minor_num);

	VM_DBG(("Setting running version of %s to %u.%u\n", vp_name,
	    rv.major_num, rv.minor_num));

	vp_rev_depend *vrd;
	vp_dependency *vpd;
	vp_rhs_version *vrhs;

	//
	// This clause marks the dependencies on this node mode vp that
	// don't include the running version picked as bad.  We do this
	// because we never transmit info about node mode vps off-node,
	// and instead handle the dependencies locally at this point.  We'll
	// never pick another version for the node vps, so the dependencies
	// incompatible with the current rv are safe to forget forever.
	//
	IntrList<vp_rev_depend, _SList>::ListIterator rdi(vp_rev_dependlist);
	while ((vrd = rdi.get_current()) != NULL) {
		IntrList<vp_dependency, _SList>::ListIterator
		    depi(vrd->get_vpp()->vp_dependlist);

		while ((vpd = depi.get_current()) != NULL) {
			if (vpd->is_bad()) {
				depi.advance();
				continue;
			}
			IntrList<vp_rhs_version, _SList>::ListIterator
			    rhsi(vpd->get_rhs_list());
			while ((vrhs = rhsi.get_current()) != NULL) {
				if (vrhs->get_vpp() != this) {
					rhsi.advance();
					continue;
				}
				if (!vrhs->get_vr().contains(rv)) {
					bad_dependency(vpd);
				}
				// Either way, we're done with this dependency
				break;
			}
			depi.advance();
		}
		rdi.advance();
	}
}

//
// This relies on all the potentially connected components not being in
// lists when it is run, as it puts the objects in a queue.  If that isn't
// the case, the code will ASSERT when an element already in a list is placed
// on vqueue.
//
uint16_t
versioned_protocol::connected_components_bfs(int16_t group)
{
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	vp_rev_depend *vrd;
	versioned_protocol *w;
	IntrList <versioned_protocol, _SList> vqueue;
	uint16_t gcount = 0;

	ASSERT(get_group_id() == -1);
	set_group_id(group);
	set_ordinal(gcount);
	gcount++;

	// Start with this object in the queue
	vqueue.append(this);

	// loop while the queue isn't empty
	while ((w = vqueue.reapfirst()) != NULL) {
		// Follow, mark and queue all the non-bad forward dependencies
		IntrList<vp_dependency, _SList>::ListIterator
		    depi(w->vp_dependlist);
		while ((vpd = depi.get_current()) != NULL) {
			IntrList<vp_rhs_version, _SList>::ListIterator
			    rhsi(vpd->get_rhs_list());
			while ((vrhs = rhsi.get_current()) != NULL) {
				if (!vrhs->get_vpp()) {
					rhsi.advance();
					continue;
				}
				if (vrhs->get_vpp()->get_group_id() == -1) {
					vrhs->get_vpp()->set_group_id(group);
					vrhs->get_vpp()->set_ordinal(gcount);
					gcount++;
					vqueue.append(vrhs->get_vpp());
				} else {
					ASSERT(group ==
					    vrhs->get_vpp()->get_group_id());
				}
				rhsi.advance();
			}
			depi.advance();
		}

		// Follow, mark, and queue all the reverse dependencies
		IntrList<vp_rev_depend, _SList>::ListIterator
		    rdi(w->vp_rev_dependlist);
		while ((vrd = rdi.get_current()) != NULL) {
			if (vrd->get_vpp()->get_group_id() == -1) {
				vrd->get_vpp()->set_group_id(group);
				vrd->get_vpp()->set_ordinal(gcount);
				gcount++;
				vqueue.append(vrd->get_vpp());
			} else {
				ASSERT(group == vrd->get_vpp()->get_group_id());
			}
			rdi.advance();
		}
	}

	// Assign the special group id if we're the only member
	if (gcount == 1)
		set_group_id(0);

	return (gcount);
}

char *
versioned_protocol::get_name()
{
	return (vp_name);
}

void
versioned_protocol::debug_print()
{
	VM_DBG(("Versioned protocol entry:\n"));
	VM_DBGC(("Name: %s\n", vp_name));
	VM_DBGC(("Description: %s\n", vp_description));
	VM_DBGC(("Source: %s\n", vp_source));
	if (vp_custom_callback)
		VM_DBGC(("Custom callback name: %s\n", vp_custom_callback));

	switch (the_type) {
	case VP_BTSTRP_NODE:
		VM_DBGC(("Bootstrap node mode\n"));
		break;
	case VP_BTSTRP_NODEPAIR:
		VM_DBGC(("Bootstrap nodepair mode\n"));
		break;
	case VP_BTSTRP_CLUSTER:
		VM_DBGC(("Bootstrap cluster mode\n"));
		break;
	case VP_NODE:
		VM_DBGC(("Node mode\n"));
		break;
	case VP_NODEPAIR:
		VM_DBGC(("Nodepair mode\n"));
		break;
	case VP_CLUSTER:
		VM_DBGC(("Cluster mode\n"));
		break;
	case VP_UNINITIALIZED:
		VM_DBGC(("Mode is uninitialized\n"));
		break;
	default:
	case VP_TYPE_ARRAY_SIZE:
		VM_DBGC(("Mode has unrecognised value\n"));
		break;
	}

	VM_DBGC(("Attributes: "));
	if (vp_attributes & VP_BOOTSTRAP)
		VM_DBGC(("Bootstrap "));
	if (vp_attributes & VP_CUSTOM)
		VM_DBGC(("Custom "));
	if (vp_attributes & VP_RUNNING)
		VM_DBGC(("Running "));
	if (vp_attributes & VP_PERSISTENT)
		VM_DBGC(("Persistent "));

	VM_DBGC(("\n"));

	VM_DBGC(("Potential versions: "));
	potential_versions.dbprint();
	VM_DBGC(("\n"));

	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	vp_upgrade *vup;
	vp_rev_depend *vrd;

	IntrList<vp_dependency, _SList>::ListIterator depi(vp_dependlist);
	while ((vpd = depi.get_current()) != NULL) {
		VM_DBGC(("%s dependency line: ", vpd->is_bad() ? "Bad" : ""));
		vpd->get_lhs().dbprint();
		VM_DBGC((" ->\n"));
		IntrList<vp_rhs_version, _SList>::ListIterator
		    rhsi(vpd->get_rhs_list());
		while ((vrhs = rhsi.get_current()) != NULL) {
			if (vrhs->get_vpp())
				VM_DBGC(("\t%s ",
				    vrhs->get_vpp()->get_name()));
			else {
				ASSERT(vrhs->get_name());
				VM_DBGC(("\tUnknown %s ", vrhs->get_name()));
			}
			vrhs->get_vr().dbprint();
			VM_DBGC(("\n"));
			rhsi.advance();
		}
		depi.advance();
	}

	IntrList<vp_upgrade, _SList>::ListIterator upi(vp_upgradelist);
	while ((vup = upi.get_current()) != NULL) {
		VM_DBGC(("Upgrade Line: "));
		vup->get_lhs().dbprint();
		VM_DBGC((" -> "));
		vup->get_rhs().dbprint();
		VM_DBGC(("\n"));
		upi.advance();
	}

	IntrList<vp_rev_depend, _SList>::ListIterator rdi(vp_rev_dependlist);
	while ((vrd = rdi.get_current()) != NULL) {
		VM_DBGC(("Reverse dependency on %s\n",
		    vrd->get_vpp()->get_name()));
		rdi.advance();
	}

	VM_DBGC(("Group id: %d, Ordinal: %u\n\n", get_group_id(),
	    get_ordinal()));
}

//
// Private methods
//

int
versioned_protocol::check_dependency_type(vp_type vt)
{
	switch (the_type) {
	case VP_BTSTRP_NODE:
		VM_DBG(("vp '%s' is Node mode but contains a dependency\n",
		    vp_name));
		// XXX syslog
		// See VM_DBG above
		return (VP_INVALIDDEP);
	case VP_BTSTRP_NODEPAIR:
		if (vt != VP_BTSTRP_NODE) {
			VM_DBG(("vp '%s' is Nodepair mode but has a dependency"
			    " on a vp that is not Node Mode\n", vp_name));
			// XXX syslog
			// See VM_DBG above
			return (VP_INVALIDDEP);
		}
		break;
	case VP_BTSTRP_CLUSTER:
		if ((vt != VP_BTSTRP_NODE) && (vt != VP_BTSTRP_NODEPAIR)) {
			VM_DBG(("vp '%s' is Bootstrap Cluster mode but has a "
			    " dependency on a vp that is not Node or Nodepair "
			    "mode\n", vp_name));
			// XXX syslog
			// See VM_DBG above
			return (VP_INVALIDDEP);
		}
		break;
	case VP_CLUSTER:
		if ((vt != VP_BTSTRP_NODE) && (vt != VP_BTSTRP_CLUSTER) &&
		    (vt != VP_CLUSTER)) {
			VM_DBG(("vp '%s' is Cluster mode but has a dependency"
			    " on a vp that is Nodepair mode\n", vp_name));
			// XXX syslog
			// See VM_DBG above
			return (VP_INVALIDDEP);
		}
		break;
	case VP_UNINITIALIZED:
	case VP_NODE:
	case VP_NODEPAIR:
	case VP_TYPE_ARRAY_SIZE:
	default:
		ASSERT(0);
		break;
	}
	return (VP_SUCCESS);
}

//
// custom_query calls the custom object to retrieve a list of versions
// the component feels comfortable supporting or upgrading to.  The
// return values report errors in the process of making the query.
// The semantic checks on the versions retrieved are done later in
// rebuild_potential_versions.
//
// Note that we use running_version[0] in every case.  If this is not a
// nodepair mode vp, running_version[0] will contain the
// current running version, or 0.0 if we haven't set one yet.  If
// it is a nodepair vp, it will always contain 0.0, because there
// is no nodeid 0.  In any case, the only custom call for btstrp_np
// vps is in initialize_int, when the entire array is filled with
// zeros, and non-bootstrap nodepair vps are not yet implemented.
//
int
versioned_protocol::custom_query(vm_stream* custom)
{
	CORBA::Object_var q_obj;
	Environment e;

	// If this object isn't a custom object, just return VP_SUCCESS;
	if (!is_custom())
		return (VP_SUCCESS);

	version_manager::vp_version_t &rv = running_version[0];

#ifdef	DEBUG
	if (the_type == VP_BTSTRP_NODEPAIR || the_type == VP_BTSTRP_NODE)
		ASSERT(rv.major_num == 0);
#endif

	// If there is no upgrade path from the current running version
	// there is no need to make the query, so check the upgrade lines
	// against the current running version.

	vp_upgrade *vup;

	if (rv.major_num != 0) {
		bool upgrade_path = false;

		IntrList<vp_upgrade, _SList>::ListIterator upi(vp_upgradelist);
		while ((vup = upi.get_current()) != NULL) {
			if (vup->get_lhs().contains(rv)) {
				upgrade_path = true;
				break;
			}
			upi.advance();
		}

		if (!upgrade_path)
			return (VP_SUCCESS);
	}

#ifdef LIBVM

	//
	// If no custom versions stream passed then we
	// assume that consistent_versions can be custom version.
	//
	if (custom == NULL) {
		custom_versions.reinit(consistent_versions);
		return (VP_SUCCESS);
	}

	//
	// Look in stream for custom version.
	//
	vm_stream &vms = *custom;
	char *vpn;
	uint16_t u16_type;
	version_manager::vp_version_t tmprv, tmpuv;
	bool found = false;
	version_range vers;

	vms.read_from_beginning();
	while ((vpn = vms.get_header(u16_type, tmprv, tmpuv)) != NULL) {
		if (strcmp(vpn, get_name()) == 0) {
			vms.get_version_range(vers);
			found = true;
			break;
		}
	}

	if (!found) {
		//
		// If we did not find a match, then we
		// assume that consistent_versions can be
		// custom version.
		//
		custom_versions.reinit(consistent_versions);
		return (VP_SUCCESS);
	}

	//
	// Otherwise, we take the intersection of the
	// versions found with the vp consistent versions.
	//
	vers.intersect_equals(consistent_versions);
	if (vers.is_empty()) {
		//
		// There are no valid version that can
		// be picked up.
		//
		return (VP_NOVERSIONS);
	}

	//
	// Set custom versions.
	//
	custom_versions.reinit(vers);
	return (VP_SUCCESS);

#else

	// Retrieve the custom object from the local nameserver (its not safe to
	// use the canned resolve function because that waits for the object
	// to be registered if it isn't there).

	naming::naming_context_var ctxp = ns::local_nameserver();

	ASSERT(!CORBA::is_nil(ctxp));
	q_obj = ctxp->resolve(vp_custom_callback, e);

	if (e.exception()) {
		VM_DBG(("vp '%s': Couldn't resolve custom query object "
		    "%s in local_nameserver\n", vp_name,
		    vp_custom_callback));
		// XXX syslog
		// See VM_DBG above
		e.clear();
		return (VP_NOCUSTOMQ);
	}

	version_manager::custom_vp_query_var cusq =
	    version_manager::custom_vp_query::_narrow(q_obj);

	if (CORBA::is_nil(cusq)) {
		VM_DBG(("vp '%s': Couldn't narrow custom query object "
		    "%s to custom_vp_query\n", vp_name, vp_custom_callback));
		// XXX syslog
		// See VM_DBG above
		return (VP_NOCUSTOMQ);
	}

	version_manager::vp_version_seq *c_versions;

	//
	// Make the query and handle the exceptions
	//
	// XXX It isn't clear how the custom query should work for non-
	// bootstrap nodepair protocols if we implement them. The guess
	// is that we will do a separate call for each of the current active
	// versions to determine what it can be upgraded to.  We'll need
	// some sort of list to store the results in as well.

	cusq->get_valid_versions(vp_name, running_version[0], c_versions, e);

	if (e.exception()) {
		if (version_manager::unknown_vp::_exnarrow(e.exception())) {
			VM_DBG(("Query object %s doesn't know about vp %s\n",
			    vp_custom_callback, vp_name));
			// XXX syslog
			// See VM_DBG above
		} else {
			VM_DBG(("Query object %s (vp %s) returning exception\n",
			    vp_custom_callback, vp_name));
			// XXX syslog
			// See VM_DBG above
		}
		e.clear();
		return (VP_NOCUSTOMQ);
	}

	// Assign the answers (which are value pairs) into custom_versions
	// (which stores triads).
	custom_versions.empty();

	version_manager::vp_version_seq &c_vref = *c_versions;

	for (uint32_t i = 0; i < c_vref.length(); i++) {
		if (c_vref[i].major_num == 0)
			continue;
		custom_versions.insert_version(c_vref[i]);
	}

	delete c_versions;
	return (VP_SUCCESS);

#endif
} //lint !e715

//
// This method puts all of the versions we are prepared to support or upgrade
// to given the current running version (if any).  If we haven't chosen a
// running version yet, this is simply the intersection of the consistent
// versions and the custom versions.  If there is already a running version,
// it is the intersection of those with the right-hand-side of the appropriate
// upgrade entry, with the addition of the current running version (which
// we can support tautologically).
//
// There is extra work required to determine the versions of bootstrap cluster
// mode vps, as described and implemented below.
//
void
versioned_protocol::rebuild_potential_versions()
{
	version_range tmp_vr;
	version_manager::vp_version_t &rv = running_version[0];

#ifdef	DEBUG
	// As elsewhere, we use running_version[0] for BTSTRP_NODEPAIR
	// because 0.0 is what we want anyway.  For VP_BTSTRP_NODE, we're
	// asserting that rebuild_potential_versions is never called after
	// the running_version is first picked.
	if (the_type == VP_BTSTRP_NODEPAIR || the_type == VP_BTSTRP_NODE)
		ASSERT(rv.major_num == 0);
#endif
	if (is_custom()) {
		consistent_versions.intersection(custom_versions,
		    potential_versions);
	} else {
		potential_versions.reinit(consistent_versions);
	}

	// If we don't have a current rv set, we aren't in an upgrade
	// situation so we're finished.
	if (rv.major_num == 0) {
		return;
	}

	vp_upgrade *vup;

	IntrList<vp_upgrade, _SList>::ListIterator upi(vp_upgradelist);
	while ((vup = upi.get_current()) != NULL) {
		if (vup->get_lhs().contains(rv)) {
			potential_versions.intersection(vup->get_rhs(), tmp_vr);
			break;
		}
		upi.advance();
	}

	// If there were no upgrade lines, tmp_vr might be empty, which is fine.
	// We'll add the current running version in the next line.
	potential_versions.swap(tmp_vr);

	// We can always "upgrade" to the current version
	ASSERT(consistent_versions.contains(rv));
	potential_versions.insert_version(rv);

	// If we aren't a bootstrap cluster mode vp, we're done.
	if (the_type != VP_BTSTRP_CLUSTER)
		return;

	// Go through reverse dependencies and find the vp_dependency that is
	// currently active.  If there is a forward dependency for 'this' in
	// that line, intersect the listed versions with potential_versions.
	// If not, leave potential_versions as it is.
	//
	// We need this because bootstrap cluster vps and cluster vps are
	// upgraded asynchronously, so the potential versions need of the
	// former need to be compatible with the current running versions of
	// the latter.

	vp_rev_depend *vrd;
	vp_dependency *vpd;
	vp_rhs_version *vrhs;

	IntrList<vp_rev_depend, _SList>::ListIterator rdi(vp_rev_dependlist);
	while ((vrd = rdi.get_current()) != NULL) {
		ASSERT(vrd->get_vpp()->get_vp_type() == VP_CLUSTER);

		version_manager::vp_version_t &rdrv =
		    vrd->get_vpp()->running_version[0];

		// If the running version for any of the CLUSTER vps has not
		// been set, none of them will be set, and it's safe to skip
		// examining the rest of them.
		if (rdrv.major_num == 0)
			return;

		IntrList<vp_dependency, _SList>::ListIterator
		    depi(vrd->get_vpp()->vp_dependlist);

		while ((vpd = depi.get_current()) != NULL) {
			if (!vpd->get_lhs().contains(rdrv)) {
				depi.advance();
				continue;
			}

			if (vpd->is_bad()) {
				depi.advance();
				continue;
			}

			IntrList<vp_rhs_version, _SList>::ListIterator
			    rhsi(vpd->get_rhs_list());
			while ((vrhs = rhsi.get_current()) != NULL) {
				if (vrhs->get_vpp() != this) {
					rhsi.advance();
					continue;
				}
				// ASSERT that the current running version is
				// compatible.
				ASSERT(vrhs->get_vr().contains(rv));
				potential_versions.
				    intersect_equals(vrhs->get_vr());
				break;
			}
			// If we got here, we found the right vp_dependency and
			// can go on to the next reverse dependency.
			break;
		}
		rdi.advance();
	}
}

void
versioned_protocol::refcnt_unref()
{
	// The reference count on a versioned_protocol (as opposed to a
	// coord_vp) should never drop to zero, because it always maintains
	// a reference to itself.
	ASSERT(0);
}
