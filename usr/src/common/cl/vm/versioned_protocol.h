/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VERSIONED_PROTOCOL_H
#define	_VERSIONED_PROTOCOL_H

#pragma ident	"@(#)versioned_protocol.h	1.24	08/05/20 SMI"

#include <sys/list_def.h>
#include <h/version_manager.h>
#include <vm/version_prot_obj.h>
#include <vm/vp_dependency.h>
#include <vm/vp_upgrade.h>
#include <vm/vp_error.h>
#include <cmm/cmm_impl.h>

class vm_admin_int;

// Attribute types - Kept in a bitfield
enum vp_attributes {
	VP_BOOTSTRAP = 1,
	VP_CUSTOM = 2,
	VP_RUNNING = 4,
	VP_PERSISTENT = 8
};

//
// Version Manager versioned_protocol Class Definition
//
// The versioned protocol object internalizes the information contained in
// each version protocol specification file.  For brevity, we'll use "vp" to
// mean "versioned protocol".  There is one vp object per vp spec file.
// Each vp object will be created with the information contained in the file.
// The information will be verified and validated before the object will be
// manipulated.  After all vp objects are correctly created, then each vp
// object will be analyzed to link it with related vps.
//
class versioned_protocol :
	public _SList::ListElem,
	public version_prot_obj {
public:
	// Constructor
	versioned_protocol();

	// Constructor
	versioned_protocol(int ffv, char *name, char *description,
	    char *source, char *ccb, vp_type m, int attributes);

	// Destructor
	~versioned_protocol();

	// Undo the dependency work done in verify_and_link
	void unlink_dependencies();

	// from version_prot_obj
	versioned_protocol	*get_vpp();
	coord_vp		*get_cvpp();

	// Assign the versioned protocol information based on the vp spec file.
	void assign_vp(int ffv, char *name, char *description,
	    char *source, char *ccb, vp_type m, int attributes);

	// Insert the dependency that is based on the vp spec file.
	void insert_vp_dependency(vp_dependency &d);

	// Insert the upgrade that is based on the vp spec file.
	void insert_vp_upgrade(vp_upgrade &u);

	// Perform the second part of initialization after all spec files have
	// been parsed.
	int verify_and_link(vm_admin_int &);

	// Sets the value of a bootstrap node versioned_protocol to the highest
	// consistent version, and then marks any dependency not consistent
	// with that value as bad.
	void pick_node_mode_rv();

	// Run the connected components algorithm starting with this vp.
	uint16_t connected_components_bfs(int16_t);

	// Return the running version associated with this vp.  If the
	// vp is nodepair mode, return the value corresponding to node n.
	bool get_running_version(version_manager::vp_version_t &,
	    sol::nodeid_t n = NODEID_UNKNOWN);

	// Set the running version for the specified node
	void set_running_version(const version_manager::vp_version_t &,
	    sol::nodeid_t, vm_stream*);

	//
	// XXX Temporary until running versions obtained from cluster
	// will be available at the time versioned protocol is
	// initialized. Called by vm_admin_int.
	//
	void set_running_version_int(version_manager::vp_version_t &);

	void get_upgrade_version(version_manager::vp_version_t &);
	void set_upgrade_version(version_manager::vp_version_t &);

	// Return the versioned protocol object's file format version.
	uint16_t get_vp_file_format_version();

	// Return the versioned protocol object's name. (from version_prot_obj)
	virtual char *get_name();

	// Return the versioned protocol object's description.
	char *get_vp_description();

	// Return the versioned protocol object's source.
	char *get_vp_source();

	// Return the versioned protocol object's custom callback method.
	char *get_vp_custom_callback();

	// Return the versioned protocol object's type.
	virtual vp_type get_vp_type();

	// Return the versioned protocol object's attributes.
	int get_vp_attributes();

	// Return a reference to the potential_versions object
	version_range &get_potential_versions();

	// remove any version not found in the argument from
	// consistent_versions and place them in inconsistent_versions.
	// Then rebuild potential_versions.  The argument versions left in
	// the argument are undefined.
	void reset_consistent_versions(version_range &);

	// Return the current dependency, if it exists.
	IntrList <vp_dependency, _SList> &get_vp_dependency_list();

	// Return the current upgrade, if it exists.
	IntrList <vp_upgrade, _SList> &get_vp_upgrade_list();

	// After a dependency is marked bad or a change in custom_versions,
	// or during an upgrade, rebuild the list of potential_versions.
	void rebuild_potential_versions();

	// print a summary of the object to stdout
	void debug_print();

#ifdef DEBUGGER_PRINT
	int mdb_dump_state(const char*);
#endif

	// returns true if the Custom attribute is defined
	bool is_custom();

	// Contains the response from the custom version query
	version_range custom_versions;

private:
	// Check the dependency for sanity
	int check_dependency_type(vp_type);

	// Perform the custom query for this vp, given that the current
	// running version is the argument.
	int custom_query(vm_stream*);

	// Mark the dependency bad and remove its lhs versions from
	// consistent_versions
	void bad_dependency(vp_dependency *);

	// ASSERT(0).  The internal refcnt on a versioned_protocol should never
	// drop to zero.
	virtual void refcnt_unref();

	// Variables from the spec file headers
	uint16_t vp_file_version;	// The file format version information
	char *vp_name;			// Unique identifier for vp
	char *vp_description;		// Brief description of vp
	char *vp_source;		// Delivery vehicle of vp
	char *vp_custom_callback;	// If defined, it's used to specify
					// that the facility using this protocol
					// will cooperate with the version
					// manager to set the running version.
	vp_type the_type;		// Internode policy to handle
					// differences between version between
					// nodes.
	int vp_attributes;		// Bit field that holds vp attributes

	// Contains the versions consistent with all other vps in the
	// connected subgroup
	version_range consistent_versions;

	// All versions defined in Version lines that have been found to be
	// inconsistent with other versioned protocols, or with the chosen
	// running versions of bootstrap node mode vps.
	version_range inconsistent_versions;

	// The intersection of consistent_versions and custom_versions
	version_range potential_versions;

	// An array of running versions, either of length 1 or NODEID_MAX+1
	// depending on whether or not this is a nodepair mode vp.
	version_manager::vp_version_t *running_version;

	// The upgrade version is only used for VP_CLUSTER vps, so we only
	// need one of them
	version_manager::vp_version_t upgrade_version;

	// List of dependencies contained in the vp spec file
	IntrList <vp_dependency, _SList> vp_dependlist;

	// List of possible upgrade paths
	IntrList <vp_upgrade, _SList> vp_upgradelist;

	versioned_protocol(const versioned_protocol &);
	versioned_protocol &operator = (versioned_protocol &);
};

#ifndef	NOINLINES
#include <vm/versioned_protocol_in.h>
#endif	// NOINLINES

#endif	/* _VERSIONED_PROTOCOL_H */
