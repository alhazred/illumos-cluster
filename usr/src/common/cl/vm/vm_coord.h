/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_COORD_H
#define	_VM_COORD_H

#pragma ident	"@(#)vm_coord.h	1.23	08/05/20 SMI"

#include <sys/list_def.h>
#include <orb/member/Node.h>
#include <h/cmm.h>
#include <vm/versioned_protocol.h>
#include <vm/coord_cluster_info.h>

class coord_vp;
class vm_stream;
class vp_set_key;

//
// The vm_coord determines which nodes can be members of a compatible cluster,
// and calculates running and upgrade versions when necessary.  In addition
// to the information stored in the coord_cluster_info object (which is a
// member object of vm_coord) it stores the versioned protocol information
// from each node in lists of coord_vp objects.
//

class vm_coord {
public:
#ifdef LIBVM
	vm_coord(nodeid_t local_nodeid, uint32_t needed_votes, vm_admin_int*);
#else
	vm_coord();
#endif
	~vm_coord();

	// Query the local quorum algorithm for the current number of
	// quorum votes owned by the specified node
	uint32_t	current_node_votes(nodeid_t n);

	// Decode the btstrp_np stream in the arguments and calculate
	// running versions to support between this node and n.  Return
	// NULL if valid versions can't be chosen.
	vm_stream	*btstrp_np_handshake(nodeid_t n, vm_stream *);

	// Called from vm_comm to set the btstrp_cl message from the
	// specified node.
	void		set_info(vp_type, nodeid_t, vm_stream *,
			    uint32_t qvotes = 0);

	// Called from vm_comm to calculate running versions for
	// btstrp_cl vps, given the membership passed in.  The coordinator
	// will indicate whether a reconfiguration or the shutdown of this
	// node is necessary.
	vm_stream	*calculate_btstrp_cl(cmm::membership_t &,
			    cmm::seqnum_t current_seq,
			    cmm::seqnum_t upgrade_seq, bool &retry_algorithm,
			    bool &shutdown_node);

	// Called from vm_comm to calculate running versions for
	// cl vps, given the membership passed in.  The coordinator
	// will indicate whether a reconfiguration is necessary,
	// which nodes should receive the resulting stream, and
	// which nodes should be shut down.
	vm_stream	*calculate_cl(cmm::membership_t &,
			    cmm::seqnum_t current_seq,
			    cmm::seqnum_t upgrade_seq, nodeset &send_to,
			    nodeset &good_nodes, bool &retry_algorithm);

	// Add the coord_vp to our lists
	void		put_cvp_in_list(coord_vp *);

	// Remove the coord_vp from our lists
	void		remove_cvp_from_list(coord_vp *);

	// Retrieve a coord_vp object by name and type, creating it if
	// necessary.  Returns it with an extra hold().  Will only return
	// NULL if vp_type is not VP_UNINITIALIZED and there is a type
	// inconsistency in the database.  A new coord_vp will be created if
	// necessary.
	coord_vp	*get_coord_vp(const char *, vp_type);

	bool		is_upgrade_needed(version_manager::vp_state &,
			    nodeset &);

	//
	// Return the highest version based on the current set of nodes in the
	// cluster.  The highest version is the version that can be
	// potentially upgraded to based on the current running version, the
	// upgrade paths available and the cluster membership.
	// An example : if all nodes have versions 1.0, 1.1, and 1.2 and one
	// node also has 2.0 and there is a valid upgrade path from each of
	// these versions to the next subsequent version, then the highest
	// version is 2.0.
	//
	void		get_highest_version(
			    version_manager::vp_version_t & hv,
			    char *vp_name);

	nodeid_t	local_nodeid();

	// generate stream for given nodeid/vp_type
	vm_stream * get_stream_by_node_and_type(nodeid_t, vp_type);

	// Returns a vm_stream of running version entries, or NULL if those
	// entries could not be calculated.
	vm_stream	*get_rv_stream_by_type(vp_type t,
			    nodeid_t rnode = NODEID_UNKNOWN);
#ifndef LIBVM
	// for message logging
	void log_diagnostic(char*, nodeid_t, version_range*);
#endif

private:
	// Add entries for the vps specified in the stream
	bool		decode_vm_stream(nodeid_t, vp_type, vm_stream &,
			    uint32_t);

	// Remove any data associated with the node in the cooresponding
	// list of coord_vps
	void		clear_list_by_node(nodeid_t, vp_type);

	// Clear the last recorded nodesets for vps of type t
	void		clear_list_not_in_nodeset(const nodeset &, vp_type t,
			    bool need_upgrade = false, bool force = false);

	// Attempt to calculate btstrp_cl running versions for the
	// nodeset specified.
	bool		btstrp_cl_try_nodeset(const nodeset &,
			    bool committing, bool force);

	// Construct vp_set_keys for the current VP_CLUSTER coord_vps.
	void		cl_make_keys(const nodeset &);

	// Delete any keys created in cl_make_keys
	void		cl_delete_keys();

	// Use one of the keys constructed in cl_make_keys to generate a
	// solution universe for the nodes in nodeset
	void		compute_solution_universe(vp_set_key &,
			    const nodeset &);

	// Attempt to calculate cl running versions for the nodeset specified.
	bool		cl_try_nodeset(const nodeset &, bool use_uv,
			    bool force);

	// Return all of the nodes that can support the current running versions
	// (or upgrade versions if the second argument is true) in the
	// nodeset.
	void		cl_get_best_nodeset(nodeset &, bool);

	// List of known vps by type
	IntrList<coord_vp, _SList> cvp_typelist[VP_TYPE_ARRAY_SIZE];

	// The number of elements in each cvp_typelist element
	uint32_t	cvp_typecount[VP_TYPE_ARRAY_SIZE];

	// per-node status information
	coord_cluster_info	cci;

	// The vp_set_keys, which only live during the life of cl_calculate
	int16_t			highest_set_key;
	vp_set_key		*cl_set_keys;

	nodeid_t		nodeid;

	vm_coord(const vm_coord &);
	vm_coord &operator = (vm_coord &);
#ifdef LIBVM
	// reference to local vm_admin
	vm_admin_int	*the_vm_admin_int;
#else
	// for message logging
	void build_description_string(versioned_protocol*, version_range*,
	    char**);
#endif
};

#ifndef	NOINLINES
#include <vm/vm_coord_in.h>
#endif	// NOINLINES

#endif	/* _VM_COORD_H */
