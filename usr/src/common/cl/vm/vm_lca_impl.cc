//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)vm_lca_impl.cc	1.15	08/08/01 SMI"

//
// vm_lca_impl.cc implements the vm lca interfaces of the rolling upgrade
// framework.
//

#include <vm/vm_lca_impl.h>
#include <vm/vm_comm.h>
#include <h/naming.h>
#include <nslib/ns.h>

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#endif

vm_lca_impl *vm_lca_impl::the_vm_lcap = NULL;

//
// Called by orb::initialize() at cluster node boot time to create a vm
// lca object on the node.
//
int
vm_lca_impl::initialize()
{
	CL_PANIC(the_vm_lcap == NULL);

	the_vm_lcap = new vm_lca_impl;
	ASSERT(the_vm_lcap != NULL);

	// Bind with local name server
	Environment	e;
	naming::naming_context_var	ctxp = ns::local_nameserver_c1();
	version_manager::vm_lca_var	vmlca_v = the_vm_lcap->get_objref();
	ctxp->bind("vm_lca", vmlca_v, e);
	if (e.exception()) {
		e.exception()->print_exception("vm_lca bind:");
		// Object will delete itself from _unreferenced
		the_vm_lcap = NULL;
		e.clear();
		return (1);
	}
	return (0);
}

// Shutdown XX Not implemented yet.
void
vm_lca_impl::shutdown(Environment &)
{
	CL_PANIC(0);
}

// Constructor
vm_lca_impl::vm_lca_impl() :
    registered(false),
    num_unfinished_cl_callbacks(0),
    num_unfinished_btstrp_cl_callbacks(0),
    last_cl_callback_task_id(0),
    vps_locked(false)
{
}

// Destructor
vm_lca_impl::~vm_lca_impl()
{
}

// Unreference method
void
#ifdef DEBUG
vm_lca_impl::_unreferenced(unref_t cookie)
#else
vm_lca_impl::_unreferenced(unref_t)
#endif
{
#ifdef DEBUG
	ASSERT(_last_unref(cookie));
#endif
	// Should never reach here.
	CL_PANIC(0);

	delete this;
}

//
// Method to upgrade the version of reference to the cb_coord_control object.
// Since this method may be invoked on a version 0 reference, we need to
// use _generic_method to do it.
//
void
vm_lca_impl::upgrade_cb_coord_control_ref(uint16_t new_idl_version)
{
	VM_DBG(("vm_lca_impl: in upgrade_cb_coord_control_ref, requested "
	    "new IDL version is %u.\n", new_idl_version));

	Environment	e;

	uint8_t data_str[28 + 2 + 1];

	// Generate the function name and put that in the data_str.
	(void) os::snprintf((char *)data_str, MAXOUTSTRING,
	    "upgrade_cb_coord_control_ref");

	//
	// new_idl_version is defined as uint16_t, in order to store it in an
	// array of uint8_t, we need to cut it in half and store them
	// separately.
	//

	// First half.
	data_str[29] = new_idl_version >> 8;
	// Second half.
	data_str[30] = new_idl_version & 255;

	//
	// Lock to protect cbcc_v, we don't want anyone to use cbcc_v when
	// we are upgrading it.
	//
	rwlck.wrlock();
	CORBA::octet_seq_t	data(31, 31, (uint8_t *)data_str, false);
	CORBA::object_seq_t	objs(1, 1);
	objs[0] = nil;

	cbcc_v->_generic_method(data, objs, e);
	ASSERT(!e.exception());
	ASSERT(objs[0] != nil);

	replica_int::cb_coord_control_ptr tmp_cbcc_p =
	    replica_int::cb_coord_control::_narrow(objs[0]);

	VM_DBG(("vm_lca_impl: cbcc_v upgraded from IDL version %u to %u\n",
	    replica_int::cb_coord_control::_version(cbcc_v),
	    replica_int::cb_coord_control::_version(tmp_cbcc_p)));

	cbcc_v = replica_int::cb_coord_control::_duplicate(tmp_cbcc_p);

	rwlck.unlock();
}

//
// Method to register with the cb_coordinator object in the Replica Framework.
//
void
vm_lca_impl::register_lca_with_cb_coord()
{
	VM_DBG(("vm_lca_impl: in register_lca_with_cb_coord\n"));
	Environment	e;
	replica::rm_admin_var   rm_admin_v = rm_util::get_rm();
	replica_int::rm_var the_rm_v = replica_int::rm::_narrow(rm_admin_v);
	ASSERT(!CORBA::is_nil(the_rm_v));

	version_manager::vm_lca_ptr	lca_p = get_objref();

	FAULTPT_HA_FRMWK(FAULTNUM_VM_LCA_BEFORE_REGISTER_LCA,
	    FaultFunctions::generic);

	rwlck.rdlock();
	cbcc_v = the_rm_v->register_lca(lca_p, e);
	CL_PANIC(!e.exception());
	ASSERT(!CORBA::is_nil(cbcc_v));
	CORBA::release(lca_p);
	rwlck.unlock();

	lck.lock();
	CL_PANIC(!registered);
	registered = true;
	lck.unlock();

	FAULTPT_HA_FRMWK(FAULTNUM_VM_LCA_AFTER_REGISTER_LCA,
	    FaultFunctions::generic);
}

#ifdef DEBUG
//
// Print the current ucc's contained in the lca to the buffer.
// This method is called by vm_admin_impl::debug_request().
//
void
vm_lca_impl::debug_print(char **debug_string)
{
	lck.lock();
	string_hashtable_t<version_manager::ucc *>::iterator
	    u_iter(bstrp_cl_ucc_hashtable);
	version_manager::ucc * cur_ucc = NULL;

	// Allocate if debug_string is currently empty.
	if (*debug_string == NULL) {
		*debug_string = new char[MAXOUTSTRING];
	}
	//
	// Print out the uccs contained in the lca into the debug_string by
	// appending each line.  (We have to use os::snprintf() because
	// os::strcat() is not a method in the os class.)
	//
	(void) os::snprintf(*debug_string + os::strlen(*debug_string),
	    MAXOUTSTRING,
	    "uccs associated w/ bstrp cl vps on this local lca :\n");
	while ((cur_ucc = u_iter.get_current()) != NULL) {
		u_iter.advance();
		(void) os::snprintf(*debug_string + os::strlen(*debug_string),
		    MAXOUTSTRING,
		    "btstrp cl ucc:%s, vp:%s\n",
		    (const char *)cur_ucc->ucc_name,
		    (const char *)cur_ucc->vp_name);
	}
	(void) os::snprintf(*debug_string + os::strlen(*debug_string),
	    MAXOUTSTRING, "\n");
	lck.unlock();
}
#endif

//
// Method for registering a group of uccs that are associated with the
// same vp and callback objects.
//
void
vm_lca_impl::register_uccs(
    const version_manager::ucc_seq_t &new_ucc_seq,
    version_manager::upgrade_callback_ptr callbackp,
    bool is_btstrp_cl_vp,
    Environment &e)
{
	uint_t num_uccs = new_ucc_seq.length();
	version_manager::upgrade_callback_ptr cb_p = nil;
	CL_PANIC(num_uccs > 0);

	if (!is_btstrp_cl_vp) {
		CL_PANIC(registered);
		//
		// These uccs are associated with cluster mode vp, need to
		// register with the cb_coordinator.
		//
		rwlck.rdlock();
		for (uint_t i = 0; i < num_uccs; i++) {
			cbcc_v->register_ucc(new_ucc_seq[i], e);
			//
			// XXX Need to handle the version_manager::retry_needed
			// exception.
			//
			if (e.exception()) {
				//
				// We don't need to unregister the uccs that
				// we have already registered. The client is
				// expected to reregister. Plus we won't store
				// the callback info until all uccs requested
				// have successfully registered.
				//
				rwlck.unlock();
				return;
			}

			FAULTPT_VM(FAULTNUM_VM_LCA_REGISTER_CL_UCC,
				FaultFunctions::generic);
		}
		rwlck.unlock();
	}

	//
	// Store the names of those uccs and their associated callback obj ref
	//
	lck.lock();
	for (uint_t i = 0; i < num_uccs; i++) {
		VM_DBG(("vm_lca_impl: in register_uccs, registering "
		    "%s\n", (const char *)new_ucc_seq[i].ucc_name));
		if ((cb_p = (version_manager::upgrade_callback_ptr)
		    callback_hashtable.lookup(
		    (const char *)new_ucc_seq[i].ucc_name)) != nil) {
			if (!cb_p->_equiv(callbackp) && !e.exception()) {
				//
				// But the new ucc is associcated with a
				// different callback object than the existing
				// one, record exception if we haven't. We will
				// continue regardless.
				//
				e.exception(new version_manager::ucc_redefined);
				lck.unlock();
				return;
			} else {
				//
				// This entry already exists and is identical
				// to the existing one. Skip it.
				//
				continue;
			}
		}

		// Insert this entry into the hashtable.
		cb_p = version_manager::upgrade_callback::_duplicate(callbackp);
		callback_hashtable.add(cb_p, new_ucc_seq[i].ucc_name);
#ifdef DEBUG
		//
		// For debugging support, save the ucc associated with
		// btstrp cluster vps.
		//
		if (is_btstrp_cl_vp) {
			version_manager::ucc *ucc_p =
			    new version_manager::ucc(new_ucc_seq[i]);
			bstrp_cl_ucc_hashtable.add(ucc_p,
			    (const char *)new_ucc_seq[i].ucc_name);
		}
#endif
		VM_DBG(("vm_lca_impl: in register_uccs, added %s\n",
		    (const char *)new_ucc_seq[i].ucc_name));
	}
	lck.unlock();
}

// Method to unregister a group of uccs.
void
vm_lca_impl::unregister_uccs(const version_manager::string_seq_t &ucc_name_seq,
    Environment &)
{
	VM_DBG(("vm_lca_impl: in unregister_uccs, %u \n",
	    ucc_name_seq.length()));
	version_manager::upgrade_callback_ptr cb_p = nil;
	uint_t	seq_size = ucc_name_seq.length();

	lck.lock();
	for (uint_t i = 0; i < seq_size; i++) {
		VM_DBG(("vm_lca_impl: in unregister_uccs, %s, i = %u\n",
		    (const char *)ucc_name_seq[i], i));
		cb_p = (version_manager::upgrade_callback_ptr)
		    callback_hashtable.remove((const char *)ucc_name_seq[i]);
#ifdef DEBUG
		// For debugging support, remove the ucc associated with
		// btstrp cluster vps.
		(void) bstrp_cl_ucc_hashtable.remove(
		    (const char *)ucc_name_seq[i]);
#endif
		CORBA::release(cb_p);
		cb_p = nil;
	}
	lck.unlock();
}

//
// Method for unregistering UCCs associated with the specified callback object
// from this LCA only.
//
void
vm_lca_impl::unregister_uccs(version_manager::upgrade_callback_ptr cb_p)
{
	version_manager::upgrade_callback_ptr cur_cb_p;
	char	*key = NULL;
	uint_t	bucket_num;

	lck.lock();

	string_hashtable_t<version_manager::upgrade_callback_ptr>::iterator
	    u_iter(callback_hashtable);

	//
	// We need to iterate through the entire list to remove all UCCs
	// that were associated with the specified callback object.
	//
	while ((cur_cb_p = u_iter.get_current_key_and_bucket(key, bucket_num))
	    != NULL) {
		ASSERT(key);
		//
		// Since we may remove this entry, we need to advance
		// the current point before the possible removal.
		//
		u_iter.advance();

		//
		// Check if the reference stored in this entry points to the
		// same object as the one specified by the caller.
		//
		if (cb_p->_equiv(cur_cb_p)) {
			//
			// They are references to the same object, so we need to
			// remove this entry.
			//
			VM_DBG(("vm_lca_impl: in unregister_uccs, found ucc "
			    "%s to remove.\n", key));
			cur_cb_p = callback_hashtable.remove(key);
			CORBA::release(cur_cb_p);
			cur_cb_p = nil;
		}
		key = NULL;
	}
	lck.unlock();
}

//
// Called by cb_coord_control or version_manager to start a sequence of
// callbacks on this lca.  Return true if there are callbacks being processed
// on this lca. Else return false. cluster mode callbacks are invoked
// from the cb_coordinator while bootstrap cluster callbacks are
// invoked from the version manager in CMM step vm_bootstrap_commit_step.
//
// If the callback_task_id is zero, this is a bootstrap cluster vp callback.
//
bool
vm_lca_impl::do_callback_task(
    const version_manager::string_seq_t &ucc_seq,
    const version_manager::vp_version_seq &new_version_seq,
    uint32_t callback_task_id,
    Environment &)
{
	bool		retval = false;
	// If callback_task_id is zero, this is a bootstrap cluster call back.
	bool		is_btstrp_cl = (callback_task_id == 0);

	uint_t	num_ucc = ucc_seq.length();
	CL_PANIC(num_ucc == new_version_seq.length());

	FAULTPT_VM(FAULTNUM_VM_LCA_BEFORE_DO_CB_TASK, FaultFunctions::generic);

	lck.lock();
	ASSERT(num_unfinished_btstrp_cl_callbacks == 0);

	if (!is_btstrp_cl) {
		// This is a callback task for non-bootstrap cluster vps.
		ASSERT(callback_task_id > 0);
		ASSERT(callback_task_id == (last_cl_callback_task_id + 1) ||
		    callback_task_id == last_cl_callback_task_id ||
		    // New node joining in the middle of upgrade_commit.
		    last_cl_callback_task_id == 0);

		if (last_cl_callback_task_id == callback_task_id) {
/*
			FAULTPT_VM(FAULTNUM_VM_LCA_CL_CB_TASK_RETRY,
				FaultFunctions::generic);
*/
			// This is a retry from the new RM.
			if (num_unfinished_cl_callbacks != 0) {
				//
				// We are still processing callbacks.
				//
				retval = true;
			}
			//
			// Else, we have already processed this task.
			//
			lck.unlock();
			return (retval);
		}
		// Only cluster callbacks involve mutiple steps.
		last_cl_callback_task_id = callback_task_id;
		ASSERT(num_unfinished_cl_callbacks == 0);
	}

	FAULTPT_VM(FAULTNUM_VM_LCA_CB_TASK_BEFORE_LAUNCH,
		FaultFunctions::generic);

	//
	// Allocate threads to handle the actual callbacks,
	// one callback per thread.
	//
	uint_t num_unfinished_callbacks = 0;
	for (uint_t i = 0; i < num_ucc; i++) {
		version_manager::upgrade_callback_ptr cb_p =
		    (version_manager::upgrade_callback_ptr)
			callback_hashtable.lookup((const char *)ucc_seq[i]);
		if (CORBA::is_nil(cb_p)) {
			VM_DBG(("vm_lca_impl: do nothing to ucc %s\n",
			    (char const *)ucc_seq[i]));
			//
			// No work to do since no one has registered this
			// ucc on this lca.
			//
		} else {
			VM_DBG(("vm_lca_impl: Launch thread to do callback "
			    "for ucc %u is %s\n", i, (const char *)ucc_seq[i]));

			//
			// Note that some of these threads may try to
			// call callback_thread_done() while we are starting
			// others, they will wait until the lock is released.
			//
			callback_task *ct = new callback_task(ucc_seq[i],
			    new_version_seq[i], cb_p, is_btstrp_cl);
			common_threadpool::the().defer_processing(ct);

			num_unfinished_callbacks++;
		}
	}

	FAULTPT_VM(FAULTNUM_VM_LCA_CB_TASK_AFTER_LAUNCH,
		FaultFunctions::generic);

	if (is_btstrp_cl) {
		num_unfinished_btstrp_cl_callbacks = num_unfinished_callbacks;
		while (num_unfinished_btstrp_cl_callbacks > 0) {

			FAULTPT_VM(FAULTNUM_VM_LCA_UNFINISHED_BTSTRP_CL_CB,
				FaultFunctions::generic);

			// Wait for all the upgrade callback to complete.
			cv.wait(&lck);
		}
	} else {
		num_unfinished_cl_callbacks = num_unfinished_callbacks;
		if (num_unfinished_cl_callbacks > 0) {

			FAULTPT_VM(FAULTNUM_VM_LCA_UNFINISHED_CL_CB,
				FaultFunctions::generic);

			// There are callback threads being launched.
			retval = true;
		}
	}
	lck.unlock();

	return (retval);
}

//
// Called by callback_task after it has completed its callback.
//
void
vm_lca_impl::callback_thread_done(bool is_btstrp_cl)
{
	FAULTPT_VM(FAULTNUM_VM_LCA_CB_THREAD_DONE, FaultFunctions::generic);

	lck.lock();
	VM_DBG(("vm_lca_impl: in callback_thread_done, %u, %u\n",
	    num_unfinished_btstrp_cl_callbacks, num_unfinished_cl_callbacks));
	if (is_btstrp_cl) {
		ASSERT(num_unfinished_btstrp_cl_callbacks > 0);
		if (--num_unfinished_btstrp_cl_callbacks == 0) {

			FAULTPT_VM(FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_1,
				FaultFunctions::generic);

			//
			// We have finished all the callback tasks requested
			// from the version manager during cmm_reconfiguration.
			// We can inform the do_callback_task thread about
			// the completion.
			//
			cv.broadcast();

			FAULTPT_VM(FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_2,
				FaultFunctions::generic);
		}
		lck.unlock();
	} else {
		ASSERT(num_unfinished_cl_callbacks > 0);
		if (--num_unfinished_cl_callbacks == 0) {
			//
			// We have finished the current callback task requested
			// by the cb_coordinator. we can inform it about the
			// completion now.
			// Note that as soon as we drop the lock,
			// do_callback_task() could be called before we
			// finish notifying the cb_coordinator.
			// This can happen if there is a failover
			// (otherwise the cb_coordinator waits
			// for us to signal we are done).
			//
			uint32_t task_id = last_cl_callback_task_id;
			lck.unlock();
			Environment e;

			FAULTPT_VM(FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_3,
				FaultFunctions::generic);

			rwlck.rdlock();
			cbcc_v->callback_task_done(task_id, e);
			ASSERT(!e.exception());
			rwlck.unlock();

			FAULTPT_VM(FAULTNUM_VM_LCA_NOTIFY_COORD_CB_TASK_DONE_4,
				FaultFunctions::generic);
		} else
			lck.unlock();
	}
}

//
// Get the last cluster vp callback task id on this vm_lca. Called by
// the cb_coordinator after RM failover.
//
void
vm_lca_impl::get_last_cl_callback_task_id(uint32_t &task_id, Environment &)
{
	lck.lock();
	task_id = last_cl_callback_task_id;
	lck.unlock();
}

//
// Called by the cb_coordinator at the beginning of an upgrade commit to
// lock vps on all LCAs.
//
void
vm_lca_impl::lock_vps_for_upgrade_commit(Environment &)
{
	VM_DBG(("vm_lca_impl: in lock_vps_for_upgrade_commit \n"));
	vm_comm::the().lock_vps_for_upgrade_commit();
}

//
// Called by the cb_coordinator at the end of an upgrade commit to
// unlock vps on all LCAs.
//
void
vm_lca_impl::unlock_vps_after_upgrade_commit(Environment &)
{
	VM_DBG(("vm_lca_impl: in unlock_vps_after_upgrade_commit \n"));
	vm_comm::the().unlock_vps_after_upgrade_commit();
}

//
// Called by the cb_coordinator during upgrade_commit to commit the
// current uvs.
//
void
vm_lca_impl::invoke_finish_upgrade_commit_on_vm(Environment &)
{
	VM_DBG(("vm_lca_impl: in invoke_finish_upgrade_commit_on_vm \n"));
	vm_comm::the().finish_upgrade_commit();
	lck.lock();
	// reset the last_cl_callback_task_id
	last_cl_callback_task_id = 0;
	lck.unlock();
}

// Callback task class that deals with individual ucc callback.

// Constructor
vm_lca_impl::callback_task::callback_task(const char *callback_ucc_name,
    const version_manager::vp_version_t &version,
    version_manager::upgrade_callback_ptr cb_p,
    bool is_btstrp_cl) :
	callback_p(version_manager::upgrade_callback::_duplicate(cb_p)),
	btstrp_cl(is_btstrp_cl)
{
	ucc_name = os::strdup(callback_ucc_name);
	ASSERT(ucc_name != NULL);

	vp_version.major_num = version.major_num;
	vp_version.minor_num = version.minor_num;
}

// Destructor
vm_lca_impl::callback_task::~callback_task()
{
	delete [] ucc_name;
}

// Execution function
void
vm_lca_impl::callback_task::execute()
{
	Environment	e;

	callback_p->do_callback(ucc_name, vp_version, e);
	if (e.exception()) {
		VM_DBG(("vm_lca_impl: do_callback returns an exception for UCC "
		    "%s with version %u.%u\n", ucc_name,
		    vp_version.major_num,
		    vp_version.minor_num));
		ASSERT(CORBA::INV_OBJREF::_exnarrow(e.exception()));
		e.clear();
	}

	// Tell the lca that this callback has completed.
	(vm_lca_impl::the()).callback_thread_done(btstrp_cl);

	delete this;
}
