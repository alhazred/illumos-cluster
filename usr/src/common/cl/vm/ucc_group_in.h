/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_UCC_GROUP_IN_H
#define	_UCC_GROUP_IN_H

#pragma ident	"@(#)ucc_group_in.h	1.3	08/05/20 SMI"

// Add the ucc u to this ucc_group.
inline void
ucc_group::add_ucc(version_manager::ucc * u)
{
	ASSERT(u);
	size++;
	ucc_list.append(u);
}

// Accessor function to return the size of the ucc_group.
inline uint_t
ucc_group::get_size()
{
	return (size);
}

// Accessor function to return the index of this ucc_group in the upgrade order
inline uint_t
ucc_group::get_index()
{
	return (index);
}

// Set the index of this ucc_group
inline void
ucc_group::set_index(uint_t p)
{
	index = p;
}

// Accessor function to return if this ucc_group is a root in the tree
inline bool
ucc_group::is_root()
{
	return (root);
}

// Accessor function to return if we've visited this node
inline bool
ucc_group::get_visited()
{
	return (visited);
}

// Set this ucc_group as visited if val is 'true', unvisited if 'false'.
inline void
ucc_group::set_visited(bool val)
{
	visited = val;
}

// Accessor function to return is this ucc_group is a leaf.
inline bool
ucc_group::is_leaf()
{
	if (before_ugs.count() == 0)
		return (true);
	else
		return (false);
}

// Rewind this ucc_group's before list to the beginning
inline void
ucc_group::atfirst_bef_ug()
{
	before_ugs.atfirst();
}

// Rewind this ucc_group's concurrent list to its beginning
inline void
ucc_group::atfirst_concurrent()
{
	ucc_list.atfirst();
}

// Set the root value
inline void
ucc_group::set_root(bool v)
{
	root = v;
}

#endif	/* _UCC_GROUP_IN_H */
