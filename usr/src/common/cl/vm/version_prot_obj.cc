//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)version_prot_obj.cc 1.8     08/05/20 SMI"

#include <vm/version_prot_obj.h>

//
// Destructor
//
vp_rev_depend::~vp_rev_depend()
{
	vpop = NULL;
}

// Indicate this reverse dependency should be cleaned up
void
vp_rev_depend::refcnt_unref()
{
	vpop = NULL;
}

version_prot_obj::~version_prot_obj()
{
}

//
// Insert the vp_rev_depend into the list.
//
void
version_prot_obj::add_vp_rev_depend(version_prot_obj *n)
{
	vp_rev_depend *vrdp;

	// Reset the current pointer so it is at the beginning of the list
	vp_rev_dependlist.atfirst();

	// Find the vp_rev_depend that points to n, if there is one
	while ((vrdp = vp_rev_dependlist.get_current()) != NULL) {
		if (vrdp && vrdp->get_vpop() == n) {
			break;
		} else {
			vp_rev_dependlist.advance();
		}
	}

	// If we found one that points to n, increment its count
	if (vrdp != NULL) {
		ASSERT(vrdp->get_vpop() == n);
		// Hold is released in rm_vp_rev_depend()
		vrdp->hold();
	} else {
		// Otherwise create a new one and add it to the list (it
		// is constructed with a reference count of 1)
		vrdp = new vp_rev_depend(n);
		vp_rev_dependlist.prepend(vrdp);
	}
}

//
// Remove the vp_rev_depend from the list.
//
void
version_prot_obj::rm_vp_rev_depend(version_prot_obj *n)
{
	vp_rev_depend *vrdp;

	// Reset the current pointer so it is at the beginning of the list
	vp_rev_dependlist.atfirst();

	// Find the vp_rev_depend that points to n
	while ((vrdp = vp_rev_dependlist.get_current()) != NULL) {
		if (vrdp && vrdp->get_vpop() == n) {
			break;
		} else {
			vp_rev_dependlist.advance();
		}
	}

	ASSERT(vrdp && (vrdp->get_vpop() == n));
	// Decrement the reference count (taken in add_vp_rev_depend())
	vrdp->rele();

	// If the count has dropped to refcnt_unref will set the vpop to
	// NULL, in which case delete the reverse dependency from the list.
	if (vrdp->get_vpop() == NULL) {
		(void) vp_rev_dependlist.erase(vrdp);
		delete vrdp;
	}
}
