/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * #pragma ident   "@(#)vm_yy_scanner_impl.cc 1.9     08/05/20 SMI"
 */

// XXX Add comments

#include <vm/vm_yy_scanner_impl.h>
#include <vm/vm_yy.tab.h>
#include <vm/vm_dbgbuf.h>

// Defined in the parser
extern YYSTYPE vm_yy_lval;

// The maximum token or string size we can absorb.
static const int tokbufsize = 257;

// The keyword tables

#define	KEYANDLEN(s) s, sizeof (s)-1

static const vm_yy_scanner_impl::KW vm_yy_scanner_keys[] = {
{ KEYANDLEN("File_format_version"),	FILE_FORMAT_VERSION,	look_none },
{ KEYANDLEN("Name"),			NAME,			look_cword },
{ KEYANDLEN("Description"),		DESCRIPTION,		look_none },
{ KEYANDLEN("Source"),			SOURCE,			look_none },
{ KEYANDLEN("Mode"),			MODE,			look_mode },
{ KEYANDLEN("Attributes"),		ATTRIBUTES,		look_attr },
{ KEYANDLEN("Custom_callback_name"),	CUSTOM_CALLBACK_NAME,	look_cword },
{ KEYANDLEN("Version"),			VERSION,		look_cword },
{ KEYANDLEN("Upgrade"),			UPGRADE,		look_none },
{ 0, 0,					0,			look_none } };

static const vm_yy_scanner_impl::KW vm_yy_scanner_modes[] = {
{ KEYANDLEN("Cluster"),			CLUSTER,		look_none },
{ KEYANDLEN("Nodepair"),		NODEPAIR,		look_none },
{ KEYANDLEN("Node"),			NODE,			look_none },
{ 0, 0,					0,			look_none } };

static const vm_yy_scanner_impl::KW vm_yy_scanner_attrs[] = {
{ KEYANDLEN("Bootstrap"),		BOOTSTRAP,		look_attr },
{ KEYANDLEN("Custom"),			CUSTOM,			look_attr },
{ KEYANDLEN("Running"),			RUNNING,		look_attr },
{ KEYANDLEN("Persistent"),		PERSISTENT,		look_attr },
{ 0, 0,					0,			look_none } };

bool
vm_yy_scanner_impl::initialize(char *fname)
{
	ASSERT(tokbuf == NULL);

	// Leave in uninitialized state if open fails
	if (f.open(fname)) {
		tokbuf = new char[tokbufsize];
		tokbufloc = 0;
		linecount = 1;
		charcount = 1;
		con = look_keyword;
		return (true);
	} else {
		VM_DBG(("Couldn't open vp file %s\n", fname));
		return (false);
	}
}

int
vm_yy_scanner_impl::maketok(int t)
{
	if (t == ERRTOK)
		tokbufloc = 0;
	return (t);
}

void
vm_yy_scanner_impl::vm_yy_error(const char *erstr)
{
	VM_DBG(("Line %d, char %d: Parser error '%s'\n", linecount,
	    charcount, erstr));
}

bool
vm_yy_scanner_impl::scan_cword()
{
	ASSERT(tokbufloc == 1);
	int c = tokbuf[0];
	vm_yy_lval.name = NULL;

	if (!(os::ctype::is_alpha(c) || c == '_')) {
		VM_DBG(("Line %d, char %d: Expected C-word starts with "
		    "char %d\n", linecount, charcount, c));
		return (false);
	}

	while (os::ctype::is_alpha(c = fgetc()) || os::ctype::is_digit(c) ||
	    c == '_') {
		if (tokbufloc + 1 < tokbufsize) {
			tokbuf[tokbufloc] = (char)c;
			tokbufloc++;
		} else {
			char t = tokbuf[20];
			tokbuf[20] = 0;
			VM_DBG(("Line %d, char %d: C-word token %s... exceeds "
			    "buffer size %d\n", linecount, charcount, tokbuf,
			    tokbufsize));
			tokbuf[20] = t;
			return (false);
		}
	}

	(void) ungetc(c);
	tokbuf[tokbufloc] = 0;
	return (true);
}

int
vm_yy_scanner_impl::cword_tok()
{
	if (!scan_cword())
		return (maketok(ERRTOK));

	vm_yy_lval.name = os::strdup(tokbuf);
	tokbufloc = 0;
	return (CWORD);
}

int
vm_yy_scanner_impl::key_tok()
{
	const KW *keytab = NULL;

	switch (con) {
	case look_keyword:
		keytab = vm_yy_scanner_keys;
		break;
	case look_mode:
		keytab = vm_yy_scanner_modes;
		break;
	case look_attr:
		keytab = vm_yy_scanner_attrs;
		break;
	case look_cword:
	case look_none:
	default:
		ASSERT(0);
		break;
	}

	ASSERT(keytab != NULL);

	if (!scan_cword())
		return (maketok(ERRTOK));

	int ix;

	for (ix = 0; keytab[ix].kw; ix++) {
		if (tokbufloc == keytab[ix].len && os::strncasecmp(tokbuf,
		    keytab[ix].kw, (size_t)tokbufloc) == 0) {
			break;
		}
	}

	if (!keytab[ix].kw) {
		VM_DBG(("Line %d, char %d: Keyword %s not found in scan "
		    "context %d\n", linecount, charcount, tokbuf, con));
		return (maketok(ERRTOK));
	}

	con = keytab[ix].con;

	tokbufloc = 0;

	return (keytab[ix].tok);
}

int
vm_yy_scanner_impl::int_tok()
{
	ASSERT(tokbufloc == 1);
	int c;
	vm_yy_lval.ival = 0;

	ASSERT(os::ctype::is_digit(tokbuf[0]));

	while (os::ctype::is_digit(c = fgetc())) {
		if (tokbufloc + 1 < tokbufsize) {
			tokbuf[tokbufloc] = (char)c;
			tokbufloc++;
		} else {
			char t = tokbuf[20];
			tokbuf[20] = 0;
			VM_DBG(("Line %d, char %d: Integer %s... bigger than "
			    "buffer %d!\n", linecount, charcount, tokbuf,
			    tokbufsize));
			tokbuf[20] = t;
			return (maketok(ERRTOK));
		}
	}

	(void) ungetc(c);

	tokbuf[tokbufloc] = 0;
	vm_yy_lval.ival = os::atoi(tokbuf);
	tokbufloc = 0;
	return (INUM);
}

int
vm_yy_scanner_impl::quoted_tok()
{
	ASSERT(tokbufloc == 0);
	int c;

	vm_yy_lval.name = NULL;

	while ((c = fgetc()) != '"') {
		if ((c == -1) || (c == 0)) { // EOF or NULL
			return (maketok(ERRTOK));
		}
		if (tokbufloc + 1 < tokbufsize) { // truncate long strings
			tokbuf[tokbufloc] = (char)c;
			tokbufloc++;
		} else if (tokbufloc + 1 == tokbufsize) {
			VM_DBG(("Line %d, char %d: String too long for "
			    "buffer -- truncating\n", linecount, charcount));
		}
	}
	if (tokbufloc > 0) {
		tokbuf[tokbufloc] = 0;
		vm_yy_lval.name = os::strdup(tokbuf);
	}
	tokbufloc = 0;
	return (STRING);
}

int
vm_yy_scanner_impl::lex()
{
	ASSERT(tokbufloc == 0);

	// Skip white space...too big for inline function and only used here
	int c;

	while (1) {
		while ((c = fgetc()) == ' ' || c == '\t' || c == '\n')
			;

		if (c != '#')
			break;

		while ((c = fgetc()) != '\n')
			;
	}

	int d;

	switch (c) {
	case ';':
		con = look_keyword;
		return (maketok(SEMI));
	case '=':
		return (maketok(ASSIGN));
	case '"':
		return (quoted_tok());
	case ':':
		return (maketok(COLON));
	case '&':
		return (maketok(AMP));
	case '(':
		return (maketok(LP));
	case ')':
		return (maketok(RP));
	case ',':
		return (maketok(COMMA));
	case '.':
		return (maketok(DOT));
	case '-':
		d = fgetc();
		if (d == '>') {
			return (maketok(ARROW));
		} else {
			(void) ungetc(d);
			return (maketok(DASH));
		}
	case '*':
		return (maketok(STAR));
	case 0:
		VM_DBG(("Line %d, char %d: Unexpectedly found NULL character\n",
		    linecount, charcount));
		return (maketok(ERRTOK));
	case -1:
		return (0);
	default:
		tokbuf[0] = (char)c;
		tokbufloc++;
		if (os::ctype::is_digit(tokbuf[0]))
			return (int_tok());

		switch (con) {
		case look_keyword:
		case look_mode:
		case look_attr:
			return (key_tok());
		case look_cword:
			return (cword_tok());
		case look_none:
		default:
			VM_DBG(("Line %d, char %d: unexpected char %d\n",
			    linecount, charcount));
			return (maketok(ERRTOK));
		}
	}
}
