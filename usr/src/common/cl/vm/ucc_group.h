/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_UCC_GROUP_H
#define	_UCC_GROUP_H

#pragma ident	"@(#)ucc_group.h	1.3	08/05/20 SMI"

#include <sys/list_def.h>
#include <repl/repl_mgr/repl_mgr_prov_impl.h>
#include <vm/versioned_protocol.h>

//
// ucc_group is a container for the ucc's that must be concurrently upgraded
// with each other.  This allows us to upgrade multiple uccs as a group in a
// concurrent manner.
//
// Some terminology :
// A root ucc_group is a group of uccs where every ucc in the group does NOT
// appear in any other ucc's before list.
// A leaf ucc_group is a group of uccs where every ucc in the group has an empty
// before list.  A ucc in a lead ucc_group does not have any dependencies on
// any other ucc being upgraded beforehand.
// NOTE : It is possible for a ucc_group to be both a root and a leaf.
//

class ucc_group : public _SList::ListElem {
friend class cb_coordinator;

public:
	// Rewind this ucc_group's concurrent list to its beginning
	void	atfirst_concurrent();

	// Simply return the next concurrent ucc in the list and no more.
	version_manager::ucc *	get_next_concurrent();

	// Get the next concurrent ucc in the concurrent list, that ucc's
	// associated versioned protocol, and the version its being upgraded to.
	version_manager::ucc *	get_next_con_ucc(
	    version_manager::vp_version_t &new_rv);

	// Return the number of uccs in this ucc_group
	uint_t	get_size();

	// Constructor
	ucc_group();

	// Constructor
	ucc_group(version_manager::ucc *);

	// Destructor
	~ucc_group();

private:
	//
	// Increase the index of every ucc_group inside the before subtree.
	// This is used during the ucc_group optimization.
	//
	void	incr_subtree_indices(IntrList<ucc_group, _SList> &, uint_t);

	// Returns true if the ucc is contained in this ucc_group
	bool	contains_ucc(version_manager::ucc *u);

	// Add the ucc to the ucc_group
	void	add_ucc(version_manager::ucc * u);

	// Returns the ucc_group index in the upgrade order
	uint_t	get_index();

	// Set the index value
	void	set_index(uint_t p);

	// Returns if this ucc_group is a root
	bool	is_root();

	// Returns true if this ucc_group has been visited, else false.
	bool	get_visited();

	// Set as visited if val is 'true', unvisited if 'false'.
	void	set_visited(bool);

	// Returns true if this ucc_group is a leaf
	bool	is_leaf();

	// If the edge is not already added, add the 'before' edge.
	void	add_before_edge(ucc_group * child);

	// If the edge exists to the ucc_group, remove it and return true.
	bool	remove_before_edge(ucc_group * to_be_removed);

	// Rewind this ucc_group's before list to its beginning
	void	atfirst_bef_ug();

	// Get the next before ucc_group
	ucc_group *	get_next_bef_ug();

	// Combine the information from ucc A into ucc 'this'
	void	combine_groups(ucc_group * A);

	// Returns true if the service is part of the 'freeze' hashtable.
	bool	in_freeze_list(char *name);

	// Add a HA service to the thaw hashtable - if the service is
	// already in the hashtable, then this is a no-op.
	void	add_thaw(char *service);

	// Freeze all HA services associated with this ucc group
	void	freeze_services();

	// Thaw all HA services associated with this ucc group
	void	thaw_services();

	// Set the root value.
	void	set_root(bool v);

	// Flag set to true if this ucc_group is a root
	bool	root;

	// The number of ucc's beloging to this ucc_group
	uint_t	size;

	// The index (from 1 to size) of this ucc_group in the upgrade order
	// when a commit is invoked.  For example, a ucc_group with a index
	// value of 3 would be the third ucc_group to be upgraded.
	uint_t	index;

	// Flag set if this ucc_group has been traversed before.
	// This flag is used during post order traversal during
	// cb_coordinator::construct_ucc_group_list().
	bool	visited;

	// The uccs belonging to this ucc_group.  They must be upgraded
	// concurrently.
	SList<version_manager::ucc> ucc_list;

	// Pointers to ucc_groups that must be upgraded before this ucc_group.
	SList<ucc_group *> before_ugs;

	// A hash of all the HA services that need to be frozen as part of this
	// ucc_group.  Even if multiple ucc's in this ucc_group have the same
	// HA services, there will be only one entry of that HA service in
	// this list.
	string_hashtable_t <char *> freeze_list;

	// A hash of HA services to be thawed when this ucc_group has finished
	// its upgrade work.
	string_hashtable_t <char *> thaw_list;

};

#ifndef NOINLINES
#include <vm/ucc_group_in.h>
#endif	// NOINLINES

#endif /* _UCC_GROUP_H */
