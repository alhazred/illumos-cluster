/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_CONSTRAINT_IN_H
#define	_VP_CONSTRAINT_IN_H

#pragma ident	"@(#)vp_constraint_in.h	1.10	08/05/20 SMI"

//
// Constructor - Create an vp_constraint object with a specified vp_set
//
inline
vp_constraint::vp_constraint(uint16_t size) :
	vps(size),
	the_index(-1)
{
}

//
// Destructor
//
inline
vp_constraint::~vp_constraint()
{
}

//
// Return the version_range object's index in the 'vr_list'.
//
inline uint16_t
vp_constraint::get_index()
{
	ASSERT(the_index != -1);
	return ((uint16_t)the_index);
}

inline void
vp_constraint::set_index(uint16_t i)
{
	the_index = i;
}

inline void
vp_constraint::empty()
{
	vps.empty();
}

inline version_range &
vp_constraint::get_vr(uint16_t vr_num)
{
	return (vps.get_vr(vr_num));
}

#endif	/* _VP_CONSTRAINT_IN_H */
