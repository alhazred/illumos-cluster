//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)vp_constraint.cc 1.6     08/05/20 SMI"

#include <vm/vp_constraint.h>

//
// Instersection operator
//
// For each version_range in 'this->vps', intersect it with its associated
// version_range object in vp_set 'B'.
// If any version range in result winds up being empty, mark result invalid.
//
void
vp_constraint::intersect_with_vp_set(const vp_set &B, vp_set &result)
{
	ASSERT(vps.size == B.size && result.size == vps.size);

	result.valid = true;

	// Intersect each version_range object, one by one.
	for (uint16_t i = 0; i < vps.size; i++) {
		// A constraint may have an empty version_range object in its
		// array.  In a constraint, empty is defined as "all versions".
		// So, if the constraint's version_range is empty, we just
		// copy the version range from B to result.
		if (vps.get_vr(i).is_empty()) {
			result.get_vr(i).reinit(B.vr_list[i]);
		} else {
			vps.get_vr(i).intersection(B.vr_list[i],
			    result.get_vr(i));
		}
		if (result.get_vr(i).is_empty()) {
			result.valid = false;
		}
	}
}

//
// Apply 'this' to vp_set B and put whatever vp_sets are generated into result.
// The constraint represents a particular set of major/minor number pairs on
// particular version protocol.  By applying the constraint, we are throwing
// certain major/minor number pairs out of B.  These values will end up in
// I if they are relevant to the computation.
//
// B must not be on a list when this method is called.  This function
// 'consumes' B in the sense that B will either be reused or deleted.
void
vp_constraint::apply_to_vp_set(vp_set *B, IntrList <vp_set, _SList> &result)
{
	vp_set *I;

	// Assert that B is not in a list.
	ASSERT(B != NULL);
	B->ptr_assert();

	ASSERT(vps.size == B->size);

	// Allocate the vp_set objects
	I = new vp_set(B->size);
	ASSERT(I);

	// Do the intersection and check the results.
	intersect_with_vp_set(*B, *I);

	if (!I->get_vr((uint16_t)the_index).is_empty()) {

	// The constraint effects this solution set, so see if the
	// intersection of all the version_ranges are valid and if so,
		if (I->is_valid())
			// ... save the valid intersections
			result.prepend(I);
		else
			// ... or remove it and free memory
			delete I;

		version_range D;

		// Now, store whatever values of the indexed version_range
		// that weren't part of the constraint in D.
		B->get_vr((uint16_t)the_index).
		    subtraction(vps.get_vr((uint16_t)the_index), D);

		// If D isn't empty, we'll put those remaining values back
		// into 'this' and add ourselves back into the result list.
		// Otherwise we'll delete ourselves.
		if (!D.is_empty()) {
			B->get_vr((uint16_t)the_index).swap(D);
		} else {
			delete B;
			return;
		}
	} else {
		delete I;
	}
	result.prepend(B);
}
