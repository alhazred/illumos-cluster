/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vm_internal_custom.cc	1.18	08/08/01 SMI"

#include <vm/vm_internal_custom.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vm_comm.h>
#include <nslib/ns.h>
#include <h/ccr.h>
#include <sys/vm_util.h>

static void read_quorum_version(uint16_t &major_num, uint16_t &minor_num);

const version_manager::vp_name_t quorum_vpname = "quorum";
const char vm_internal_custom::custom_bind_name[] = "vm_internal_custom";

vm_internal_custom::vm_internal_custom()
{
}

vm_internal_custom::~vm_internal_custom()
{
}

//
// This just does a string_cmp on the name argument and either puts the
// valid list of versions into vvso, or returns an exception.  To add a new
// implementation to this object, you would add a new os::strcmp clause and
// implement your custom version picking code inside it.  Then you would
// specify the bind name ("vm_internal_custom") as the Custom_callback_name
// in your versioned_protocol specification file.
//
// The code below ASSERTs that v.major_num is 0 because there is no Upgrade
// line in the kernel_architecture spec file.  If you have upgrade lines,
// this interface will also be called in the case of an upgrade, and you
// will have to supply the list of versions that can be safely upgraded to
// from this version.
//
void
vm_internal_custom::get_valid_versions(char * const name,
	const version_manager::vp_version_t &v,
	version_manager::vp_version_seq_out vvso,
	Environment &e)
{
	VM_DBG(("vm_internal_custom queried for version of %s\n", name));
	if (os::strcmp(name, "kernel_architecture") == 0) {
		vvso = new version_manager::vp_version_seq(1, 1);
		version_manager::vp_version_seq &vvs = *vvso;
		ASSERT(v.major_num == 0);
#if defined(_LP64)
		vvs[0].major_num = 3;
#else
		vvs[0].major_num = 2;
#endif
		vvs[0].minor_num = 0;
	} else if (os::strcmp(name, quorum_vpname) == 0) {
		//
		// Read the CCR and make sure that there isn't a QD type
		// which isn't supported by the entire cluster.
		//
		// As of the Quorum API putback, we don't need to worry about
		// versioning, but once additional types are added, we will
		// need to do checking here.
		//
		version_manager::vp_version_t quorum_version;
		read_quorum_version(quorum_version.major_num,
		    quorum_version.minor_num);

		// The highest version supported in this release;
		version_manager::vp_version_t highest_quorum_version;
		highest_quorum_version.major_num = 1;
		highest_quorum_version.minor_num = 2;

		if (quorum_version.minor_num == 1) {
			//
			// There was a "type" entry in the infrastructure
			// file, so we can only support Quroum API version
			// and higher.
			//
			vvso = new version_manager::vp_version_seq(2, 2);
			version_manager::vp_version_seq &vvs = *vvso;
			vvs[0].major_num = 1;
			vvs[0].minor_num = 1;
			vvs[1] = highest_quorum_version;
		} else {
			//
			// There is no version specific information in the
			// CCR, so we can support any quorum version.
			//
			vvso = new version_manager::vp_version_seq(2, 2);
			version_manager::vp_version_seq &vvs = *vvso;
			vvs[0].major_num = 1;
			vvs[0].minor_num = 0;
			vvs[1] = highest_quorum_version;
		}
	} else {
		// XXX need? vvso = new version_manager::vp_version_seq(0,0);
		e.exception(new version_manager::unknown_vp);
	}
}

void
#ifdef  DEBUG
vm_internal_custom::_unreferenced(unref_t cookie)
#else
vm_internal_custom::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

// Initialize creates the object and binds it into the nameserver.
int
vm_internal_custom::initialize()
{
	vm_internal_custom *t;

	t = new vm_internal_custom();

	if (t == NULL)
		return (ENOMEM);

	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	version_manager::custom_vp_query_var cvq = t->get_objref();

	Environment e;

	ctxp->bind(custom_bind_name, cvq, e);

	if (e.exception()) {
		e.exception()->print_exception("bind:");
		e.clear();
		VM_DBG(("Couldn't register vm_internal_custom object\n"));
		// Object will delete itself from _unreferenced when
		// cvq is destructed
		return (EIO);
	}

	VM_DBG(("Registered vm_internal_custom object\n"));
	return (0);
}

void
vm_internal_custom::shutdown()
{
	Environment e;
	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	ctxp->unbind(custom_bind_name, e);
	e.clear();
}

//
// read_quorum_version
//
// Read the ccr infrastructure file and determine which levels of the
// quorum algorithm this node can support.
//
void
read_quorum_version(uint16_t &major_num, uint16_t &minor_num)
{
	clconf_iter_t *qdevsi, *currqdevsi;
	clconf_cluster_t *clconf = (clconf_cluster_t *)NULL;
	clconf_obj_t *obj;

	clconf = clconf_cluster_get_current();
	ASSERT(clconf != NULL);

	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);

	major_num = 1;
	minor_num = 0;
	while ((obj = clconf_iter_get_current(currqdevsi)) != NULL) {
		//
		// Need to go through all quorum devices and set the
		// version to the highest minor number corresponding
		// to the version in which support for that type was
		// added.
		//
		if (clconf_obj_get_property(obj, "type") != NULL) {
			VM_DBG(("read_quorum_version: CCR has a QD with "
			    "a declared type. The lowest version this node "
			    "can support is 1.1.\/n"));
			//
			// If this node has a quorum_server type of
			// quorum device configured, then its running
			// version must be set 1.2. This is to avoid
			// accidental downgrade in the case where some
			// cluster nodes were upgraded to 1.2 while
			// some nodes were out of the cluster, a
			// quorum server type of qd ws added and then
			// the entire cluster rebooted.
			//
			const char *dev_typep = clconf_obj_get_name(obj);
			if (os::strcmp(dev_typep, "quorum_server") == 0) {
				minor_num = 2;
			} else if (minor_num < 1) {
				//
				// Set minor_num to 1 only if we
				// haven't set it higher in some previous
				// iteration. Type are scsi2, scsi3,
				// netapp_nas
				//
				minor_num = 1;
			}
		}
		clconf_iter_advance(currqdevsi);
	}
	clconf_iter_release(qdevsi);
	clconf_obj_release((clconf_obj_t *)clconf);
}
