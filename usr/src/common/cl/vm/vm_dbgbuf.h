/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_DBGBUF_H
#define	_VM_DBGBUF_H

#pragma ident	"@(#)vm_dbgbuf.h	1.9	08/05/20 SMI"

#include <orb/debug/haci_debug.h>

//
// This constant is used by internal interface % query_vm.  It is the maximum
// length of the output buffer when the -d print_uccs option is used.
//
const size_t MAXOUTSTRING = 1024;

#if defined(HACI_DBGBUFS) && !defined(NO_VM_DBGBUF)
#define	VM_DBGBUF(a)
#endif

#ifdef VM_DBGBUF
extern dbg_print_buf vm_dbg;
#define	VM_DBG(a) HACI_DEBUG(ENABLE_VM_DBG, vm_dbg, a)
#define	VM_DBGC(a) HACI_DEBUG_CONT(ENABLE_VM_DBG, vm_dbg, a)
#else
#define	VM_DBG(a)
#define	VM_DBGC(a)
#endif

#endif	/* _VM_DBGBUF_H */
