/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VM_COMM_IN_H
#define	_VM_COMM_IN_H

#pragma ident	"@(#)vm_comm_in.h	1.16	08/05/20 SMI"

inline vm_comm &
vm_comm::the()
{
	ASSERT(the_vm_comm != NULL);
	return (*the_vm_comm);
}

inline vm_coord &
vm_comm::the_vm_coord()
{
	ASSERT(the_vm_comm != NULL);
	return (*(the_vm_comm->the_coord));
}

inline vm_admin_impl &
vm_comm::the_vm_admin_impl()
{
	ASSERT(the_vm_comm != NULL);
	ASSERT(the_vm_comm->the_admin_impl != NULL);
	return (*(the_vm_comm->the_admin_impl));
}

// The caller should grab lock before calling this function
inline void
vm_comm::wait_until_vps_are_unlocked()
{
	ASSERT(lock_held());
	while (vps_locked) {
		vp_lock_cv.wait(get_lock());
	}
}

inline void
vm_comm::lock()
{
	vm_comm_lock.lock();
}

inline void
vm_comm::unlock()
{
	vm_comm_lock.unlock();
}

inline int
vm_comm::lock_held()
{
	return (vm_comm_lock.lock_held());
}

inline bool
vm_comm::are_vps_locked()
{
	ASSERT(lock_held());

	return (vps_locked);
}

inline cmm::seqnum_t
vm_comm::get_upgrade_seq()
{
	ASSERT(lock_held());

	return (upgrade_seq);
}	

inline os::mutex_t *
vm_comm::get_lock()
{
	return (&vm_comm_lock);
}

inline bool
vm_comm::canceled(cmm::seqnum_t s)
{
	return (canceled_seq >= s);
}

inline bool
vm_comm::current_pairwise_incn(sol::nodeid_t n, sol::incarnation_num &i)
{
	ASSERT(lock_held());
	return (btstrp_np_mbrshp.members[n] == i);
}

//
// Accessor for syslog messages.
//
inline os::sc_syslog_msg &
vm_comm::get_msg()
{
	return (msg);
}

inline bool
vm_comm::is_signal_needed()
{
	return (signal_needed);
}

#endif	/* _VM_COMM_IN_H */
