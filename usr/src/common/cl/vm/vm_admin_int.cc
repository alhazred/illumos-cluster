/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vm_admin_int.cc	1.3	08/05/20 SMI"

#include <vm/vm_admin_int.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vp_set_key.h>
#include <vm/vp_rhs_version.h>
#include <vm/vp_set.h>
#include <vm/vp_constraint.h>
#include <vm/vp_error.h>
#include <sys/os.h>
#include <sys/node_order.h>
#include <vm/vm_stream.h>

#if !defined(_KERNEL) && defined(_KERNEL_ORB)
#include <unode/unode_config.h>
#endif

#if	defined(VM_DBGBUF) && defined(LIBVM)
uint32_t vm_dbg_size = 12 * 1024;
dbg_print_buf vm_dbg(vm_dbg_size);
#endif

vp_error_t process_vp_file(versioned_protocol **vpp, char *filename);

#ifndef LIBVM
vm_admin_int::vm_admin_int() :
	num_vps(0),
	btstrp_cl_rv_seq(0),
	cl_rv_seq(0),
	cl_uv_seq(0),
	vp_array(NULL),
	need_btstrp_cl_message(true),
	need_cl_message(true),
	change_flag(false)
{
	// Set the directories where we'll look for vp spec files
#ifdef  _KERNEL
	vp_dirs[0] = "/etc/cluster/vp";
	vp_dirs[1] = "/usr/cluster/lib/vp";
	vp_dirs[2] = 0;
#elif defined(_KERNEL_ORB)
	// unode
	vp_dirs[0] = os::strdup(unode_config::the().vp_dir());
	vp_dirs[1] = 0;
	vp_dirs[2] = 0;
#endif
	custom_stream = NULL;
	vplist_stream = NULL;
}

#else
vm_admin_int::vm_admin_int(char **vp_paths, cmm::seqnum_t seq,
    vm_stream *custom, vm_stream *vplist) :
	num_vps(0),
	// XXX temporary - until we get real seq nums from cluster.
	btstrp_cl_rv_seq(seq),
	cl_rv_seq(seq),
	cl_uv_seq(0),
	vp_array(NULL),
	need_btstrp_cl_message(true),
	need_cl_message(true),
	change_flag(false)
{
	vp_dirs[0] = vp_paths[0];
	vp_dirs[1] = vp_paths[1];
	vp_dirs[2] = 0;

	custom_stream = custom;
	vplist_stream = vplist;
}
#endif

vm_admin_int::~vm_admin_int()
{
	int n;

	// Unlink the dependency relationships between the versioned_protocols
	for (n = 0; n < num_vps; n++) {
		(vp_array[n])->unlink_dependencies();
	}

	// Get rid of the sorted vp array
	delete [] vp_array;

	// Delete the objects themselves, destroying the lists at the same time
	for (n = 0; n < VP_TYPE_ARRAY_SIZE; n++) {
		vp_typelist[n].dispose();
	}

	if (custom_stream != NULL) {
		delete custom_stream;
		custom_stream = NULL;
	}
	if (vplist_stream != NULL) {
		delete vplist_stream;
		vplist_stream = NULL;
	}
}

//
// Perform post-construction initialization, from parsing the spec files
// through local_consistency, picking the node-mode versions, and forming
// the btstrp_np_message, passed back through the argument.
//
int
vm_admin_int::initialize_int(vm_stream *&btstrp_np_message)
{
	char fname[PATH_MAX];
	os::rdir vp_dir;
	os::dirent_t *d;
	char *c;
	int f;
	versioned_protocol *v = NULL, *cur;
	IntrList<versioned_protocol, _SList> vp_sortlist;

	ASSERT(btstrp_np_message == NULL);

	// Go through each of the vp directories
	for (int vd = 0; vp_dirs[vd]; vd++) {
		if (!vp_dir.open(vp_dirs[vd])) {
			VM_DBG(("Couldn't open directory %s\n", vp_dirs[vd]));
			vp_sortlist.dispose();
			vp_dir.close();
			// XXX syslog
			// One of the directories that should exist for reading
			// vp spec files from is missing.
			return (VP_CANNOTOPENDIR);
		}
		// Read through each file in the directory
		while ((d = vp_dir.read()) != NULL) {
			// Ignore files not ending in '.vp'
			c = strrchr(d->d_name, '.');
			if (c == NULL || os::strcmp(c, ".vp"))
				continue;

			// Create the full path to the file
			f = os::snprintf(fname, (size_t)PATH_MAX, "%s/%s",
			    vp_dirs[vd], d->d_name);
			// If there was room, parse.
			if (f <= PATH_MAX) {
				f = process_vp_file(&v, fname);
			} else {
				f = VP_CANNOTOPENFILE;
			}
			// If there wasn't room in fname for the full path, or
			// if there was an error parsing, clean up and return.
			if (f) {
				// Clean up and return on error
				vp_sortlist.dispose();
				vp_dir.close();
				// XXX syslog
				// The vp spec file with name fname failed
				// to parse
				return (f);
			}
#ifdef LIBVM
			//
			// Check vp file name against list of vp file names
			// to take into account.
			//
			if (vplist_stream != NULL) {
				bool found = false;
				char *vpn;
				uint16_t u16_type;
				version_manager::vp_version_t tmprv, tmpuv;

				vplist_stream->read_from_beginning();

				while ((vpn = vplist_stream->
				    get_header(u16_type, tmprv, tmpuv))
				    != NULL) {
					if (strcmp(vpn, v->get_name()) == 0) {
						found = true;
						break;
					}
				}

				//
				// Discard current vp if name not found.
				//
				if (!found) {
					delete (v);
					v = NULL;
					continue;
				}
			}
#endif
			// Insert the new versioned_protocol object into the
			// vp_sortlist in sorted order
			ASSERT(v != NULL);
			vp_sortlist.atfirst();
			while ((cur = vp_sortlist.get_current()) != NULL) {
				f = os::strcmp(cur->get_name(), v->get_name());
				if (f > 0) {
					vp_sortlist.insert_b(v);
					v = NULL;
					break;
				} else if (f < 0) {
					vp_sortlist.advance();
				} else {
					VM_DBG(("Found two vps with name %s\n",
					    cur->get_name()));
					vp_sortlist.dispose();
					vp_dir.close();
					// XXX syslog
					// Two vp spec files have the same
					// string in their Name: field
					return (VP_DUPNAME);
				}
			}
			if (v != NULL)
				vp_sortlist.append(v);
			v = NULL;
			num_vps++;
		}
		vp_dir.close();
	}

	if (!num_vps) {
		vp_sortlist.dispose();
		return (VP_NOVPFILES);
	}

	int n = 0;

	// Allocate the vp_array and place the sorted vps into it (for
	// fast retrieval).
	vp_array = new versioned_protocol*[num_vps];
	vp_sortlist.atfirst();
	while ((cur = vp_sortlist.reapfirst()) != NULL) {
		ASSERT(n < num_vps);
		vp_array[n] = cur;
		n++;
	}
	ASSERT(n == num_vps);


	// Perform the second stage of vp initialization
	for (n = 0; n < num_vps; n++) {
		if ((f = vp_array[n]->verify_and_link(*this)) != 0) {
			// Note that errors found here are sysloged from
			// inside verify_and_link
			return (f);
		}
	}

	// temporary room for every key
	vp_set_key *tmp_keys = new vp_set_key[num_vps/2 + 1];

	int16_t highest_vp_set = 1;

	// This is a little tricky.  If the group_id isn't set yet, we
	// run the connected components algorithm on the node.  When the
	// algorithm is finished, anything connected to that node (by
	// definition later in the array) will be marked.  The function returns
	// the number of vps in the connected subgroup, which we can use to
	// initialize the corresponding vp_set_key.  Note that the algorithm
	// assumes that none of the connected components are in lists when
	// it is called, which is safe because we reaped them above to put
	// them in the array.  (It places them in a queue to avoid recursion.)
	//
	// Once we know the vp is part of a connected set, we set the
	// appropriate pointer in the tmp_keys (if necessary) and also then
	// add the vp to the appropriate type list (which is safe, because
	// we won't run the algorithm again so the list element is available.)
	//
	for (n = 0; n < num_vps; n++) {
		if (vp_array[n]->get_group_id() == -1) {
			f = vp_array[n]->
			    connected_components_bfs(highest_vp_set);
			if (f > 1) {
				tmp_keys[highest_vp_set].reset((uint16_t)f);
				highest_vp_set++;
			}
		}
		if (vp_array[n]->get_group_id() != 0) {
			tmp_keys[vp_array[n]->get_group_id()].
			    set_vpop(vp_array[n]->get_ordinal(),
			    vp_array[n]);
		}
		vp_typelist[vp_array[n]->get_vp_type()].append(vp_array[n]);
	}

	highest_vp_set--;

	f = VP_SUCCESS;

	// Print debugging information on each key, run local_consistency,
	// and delete the key.
	if (highest_vp_set > 0) {
		for (n = 1; n <= highest_vp_set; n++) {

			VM_DBG(("\nConnected subgroup %d:\n", n));

			for (uint16_t m = 0; m < tmp_keys[n].num_elems();
			    m++) {
				VM_DBGC(("\t%u %s\n",
				    tmp_keys[n].get_vpp(m)->get_ordinal(),
				    tmp_keys[n].get_vpp(m)->get_name()));
			}

			if (!local_consistency(tmp_keys[n])) {
				f = VP_INCONSISTGRP;
			}
		}
	}

	delete [] tmp_keys;

	// Note that errors caught here are sysloged inside local_consistency
	if (f != VP_SUCCESS)
		return (f);

	IntrList<versioned_protocol, _SList> &bnp = vp_typelist[VP_BTSTRP_NODE];
	// Pick the running versions of the bootstrap node mode vps
	IntrList<versioned_protocol, _SList>::ListIterator node_vps(bnp);

	while ((v = node_vps.get_current()) != NULL) {
		v->pick_node_mode_rv();
		node_vps.advance();
	}

	btstrp_np_message = get_stream_by_type(VP_BTSTRP_NODEPAIR);

	return (VP_SUCCESS);
}

//
// Run local consistency on a connected subgroup, as indicated by a
// vp_set_key
//
bool
vm_admin_int::local_consistency(vp_set_key &key)
{
	IntrList <vp_set, _SList> solution_universe, tmp_su;
	vp_set *vps;
	vp_dependency *vpd;
	vp_rhs_version *vrhs;
	int m;

	int16_t group_id = key.get_vpp(0)->get_group_id();
	ASSERT(group_id > 0);

	int nele = key.num_elems();

	// Make the initial solution universe
	vps = new vp_set((uint16_t)nele);
	ASSERT(vps);

	// Initialze first vp_set to combined contents of potential_versions,
	// which for this algorithm was initialized at the end of
	// versioned_protocol::verify_and_link()
	for (m = 0; m < nele; m++) {
		versioned_protocol *cand_vp = key.get_vpp((uint16_t)m);
		ASSERT(!cand_vp->get_potential_versions().is_empty());
		vps->get_vr((uint16_t)m).
		    reinit(cand_vp->get_potential_versions());
	}

	solution_universe.append(vps);

	// A constraint object
	vp_constraint vpc((uint16_t)nele);

	// Go through the list of vps in the group
	for (m = 0; m < nele; m++) {
		versioned_protocol *cand_vp = key.get_vpp((uint16_t)m);
		ASSERT(cand_vp->get_group_id() == group_id);
		vpc.set_index((uint16_t)m);
		IntrList<vp_dependency, _SList>::ListIterator
		    depi(cand_vp->get_vp_dependency_list());

		// Go through the list of dependency sets in the vp
		while ((vpd = depi.get_current()) != NULL) {
			if (vpd->is_bad() || vpd->get_rhs_list().empty()) {
				depi.advance();
				continue;
			}

			vpc.empty();
			vpc.get_vr((uint16_t)m).reinit(vpd->get_lhs());

			IntrList<vp_rhs_version, _SList>::ListIterator
			    rhsi(vpd->get_rhs_list());

			// Go through the list of dependencies in the set and
			// set the version_range in the constraint
			while ((vrhs = rhsi.get_current()) != NULL) {
				if (!vrhs->get_vpp()) {
					rhsi.advance();
					continue;
				}
				ASSERT(vrhs->get_vpp()->get_group_id() ==
				    group_id);
				vpc.get_vr(vrhs->get_vpp()->get_ordinal()).
				    reinit(vrhs->get_vr());
				rhsi.advance();
			}

			// Constraint is now constructed, apply it to the
			// solution universe

			while ((vps = solution_universe.reapfirst()) != NULL) {
				vpc.apply_to_vp_set(vps, tmp_su);
			}

			// Bomb out if we don't have any solutions left
			if (tmp_su.empty()) {
				VM_DBG(("Connected subgroup %d is "
				    "inconsistent\n", group_id));
				// XXX syslog
				// There is no set of running versions that
				// will satisfy the dependencies for the
				// versioned protocol defined in 'key'.
				//
				// The names of these protocols can be
				// retrieved by calling
				// key.get_vpp(n)->get_name();
				return (false);
			}

			ASSERT(solution_universe.empty());
			solution_universe.concat(tmp_su);

			depi.advance();
		}
	}

	// Solution universe now contains only consistent versions
	// Aggregate them and reset every_valid_version to take dependencies
	// into account.

	vp_set aggr(key.num_elems());

	while ((vps = solution_universe.reapfirst()) != NULL) {
		aggr.merge_equals(*vps);
		delete vps;
	}

	for (m = 0; m < key.num_elems(); m++) {
		key.get_vpp((uint16_t)m)->reset_consistent_versions(
		    aggr.get_vr((uint16_t)m));
	}

	return (true);
}

//
// Add each of the versioned protocols of type t in our list to a new
// vm_stream and return it.  The versioned_protocols are added in sort
// order, and the some parts of the design rely on this.
//
vm_stream *
vm_admin_int::get_stream_by_type(vp_type t)
{
	versioned_protocol *v;

	cmm::seqnum_t rv_seq = 0, uv_seq = 0;

	if (t == VP_BTSTRP_CLUSTER) {
		rv_seq = btstrp_cl_rv_seq;
	} else if (t == VP_CLUSTER) {
		rv_seq = cl_rv_seq;
		uv_seq = cl_uv_seq;
	}

	vm_stream *vmsp = new vm_stream(vm_stream::VP_STREAM_1,
	    (uint16_t)vp_typelist[t].count(), rv_seq, uv_seq);

	ASSERT(vmsp != NULL);

	IntrList<versioned_protocol, _SList> &vpl = vp_typelist[t];
	IntrList<versioned_protocol, _SList>::ListIterator vpli(vpl);

	while ((v = vpli.get_current()) != NULL) {
		vmsp->put_versioned_protocol(v);
		vpli.advance();
	}

	// Terminate
	vmsp->put_versioned_protocol(NULL);

	return (vmsp);
}

//
// Set the running versions for vps of the specified type based on the
// information in the stream.  Committing is used to pick which entry
// in the stream to use.  rnode is used to specify a remote node if
// type is nodepair.
//
void
vm_admin_int::set_rvs(vm_stream &vms, vp_type type, nodeid_t rnode)
{
	versioned_protocol *v;
	uint16_t u16_type;
	version_manager::vp_version_t rv, uv, cur_rv;
	char *vpn;
	int rcmp;
	bool load_rvs = true, load_uvs = false;

	VM_DBG(("vm_admin_int setting rvs of type %d", type));

	if (type != VP_BTSTRP_NODEPAIR && type != VP_NODEPAIR) {
		ASSERT(rnode == 0);
		VM_DBGC((": "));
	} else {
		ASSERT(rnode != NODEID_UNKNOWN && rnode <= NODEID_MAX);
		VM_DBGC((" from node %u: ", rnode));
	}

	//
	// Make sure it makes sense to be receiving this vm_stream in this
	// state, and if necessary move to the next state.
	//

	if (type == VP_BTSTRP_CLUSTER) {
		// We should only receive btstrp_cl streams when the rvs
		// have been recalculated
#ifndef LIBVM
		// Disabled temporarily until we can get real
		// rv_seqnum from cluster.
		//
		ASSERT(vms.get_rv_seqnum() > btstrp_cl_rv_seq);
#endif
		btstrp_cl_rv_seq = vms.get_rv_seqnum();
	}

	// XXX Note that the code below gives the callback coordinator
	// another option for how to finish up an upgrade commit.  It can
	// change over to the new running versions locally and then
	// force a reconfiguration, which will cause the coordinator
	// to report the new versions to all nodes.  The code below
	// will ensure that running versions wind up getting copied over.
	// This is safe as long as the callback coordinator holds the
	// proper locks from every node before switching over the first
	// one (which is mandatory for version sanity anyway).
	if (type == VP_CLUSTER) {
		// This ASSERT handles the case where a reconfiguration
		// occurs after the rvs have been switched on one node but
		// before they have been switched on this node.  The code
		// handles this case by switching over at this point.
		ASSERT(cl_rv_seq == 0 || vms.get_rv_seqnum() == cl_rv_seq ||
		    vms.get_rv_seqnum() == cl_uv_seq);

		if (vms.get_rv_seqnum() > cl_rv_seq) {
			cl_rv_seq = vms.get_rv_seqnum();
		} else {
			ASSERT(vms.get_rv_seqnum() == cl_rv_seq);
			load_rvs = false;
		}

		// We load the uvs whenever the sequence numbers aren't
		// equal.  This handles the transition back to zero correctly,
		// if it is driven from here rather than from the callback
		// coordinator.
		//
		if (vms.get_uv_seqnum() != cl_uv_seq) {
			cl_uv_seq = vms.get_uv_seqnum();
			load_uvs = true;
		}
	}

	IntrList<versioned_protocol, _SList> &vpl = vp_typelist[type];
	IntrList<versioned_protocol, _SList>::ListIterator vpli(vpl);

	vms.read_from_beginning();

	// All the versioned_protocols should be in the list because we
	// registered them.  Skip over the ones we don't care about.

	while ((v = vpli.get_current()) != NULL) {
		vpn = vms.get_header(u16_type, rv, uv);
		ASSERT(vpn != NULL);
		ASSERT(type == (vp_type)u16_type);
		if ((rcmp = os::strcmp(vpn, v->get_name())) != 0) {
			// The entry is missing, so skip over it, but ASSERT
			// the versions listed are 1.0.
			ASSERT(rcmp < 0);
			if (load_uvs) {
				ASSERT(uv.major_num == 1 && uv.minor_num == 0);
			}
			if (load_rvs) {
				ASSERT(rv.major_num == 1 && rv.minor_num == 0);
			}
			continue;
		}
		if (load_rvs) {
			(void) v->get_running_version(cur_rv, rnode);
			if ((cur_rv.major_num < rv.major_num) ||
			    ((cur_rv.major_num == rv.major_num) &&
			    (cur_rv.minor_num < rv.minor_num))) {
				//
				// On this node, the running version is
				// increasing to a new value.  As such, we must
				// keep track that the version has increased so
				// that any multiple sequential upgrades for
				// bootstrap cluster vps are handled correctly
				// during a commit.  See bug 4855309 for more
				// details.
				//
				VM_DBGC(("%s rv= from %u.%u to %u.%u, ",
				    vpn,
				    cur_rv.major_num, cur_rv.minor_num,
				    rv.major_num, rv.minor_num));
				set_change_flag(true);
			} else {
				VM_DBGC(("%s rv=%u.%u, ", vpn, rv.major_num,
				    rv.minor_num));
			}
			v->set_running_version(rv, rnode, custom_stream);
		}
		if (load_uvs) {
			(void) v->get_running_version(cur_rv);
			if ((cur_rv.major_num < uv.major_num) ||
			    ((cur_rv.major_num == uv.major_num) &&
			    (cur_rv.minor_num < uv.minor_num))) {
				//
				// On this node, the running version can be
				// increased to a new value.  As such, we must
				// keep track that the version can be increased
				// so that any multiple sequential upgrades for
				// non-bootstrap cluster vps are handled
				// correctly during a commit.
				//
				VM_DBGC(("%s rv=%u.%u, uv=%u.%u, ", vpn,
				    cur_rv.major_num, cur_rv.minor_num,
				    uv.major_num, uv.minor_num));
				set_change_flag(true);
			} else {
				VM_DBGC(("%s uv=%u.%u, ", vpn, uv.major_num,
				    uv.minor_num));
			}
			v->set_upgrade_version(uv);
		}
		vpli.advance();
	}
	VM_DBGC(("\n"));

	// When the running versions are replaced, we need to send a new
	// message during the next reconfiguration including the versions we
	// can upgrade to on the next commit, so here we record the fact that
	// the message needs regenerating.  This will also cause the sequence
	// numbers included in the messages to be updated.

	if (type == VP_BTSTRP_CLUSTER) {
		need_btstrp_cl_message = true;
		// Changing VP_BTSTRP_CLUSTER rvs also changes the VP_CLUSTER
		// running versions we could potentially upgrade to.
		need_cl_message = true;
	} else if (type == VP_CLUSTER) {
		need_cl_message = true;
	}
}

//
// Generates the bootstrap cluster message.If the first argument is
// not NULL, and the message is being replaced, the old vm_stream will
// be deleted.  True is returned if the message has been replaced,
// false otherwise
//
bool
vm_admin_int::new_btstrp_cl_message(vm_stream *&btstrp_cl_message)
{
	if (need_btstrp_cl_message) {
		if (btstrp_cl_message != NULL) {
			delete btstrp_cl_message;
		}

		btstrp_cl_message = get_stream_by_type(VP_BTSTRP_CLUSTER);

		need_btstrp_cl_message = false;
		return (true);
	} else {
		return (false);
	}
}

//
// Generates the non-bootstrap message.  Similar to
// new_btstrp_cl_message
//
bool
vm_admin_int::new_cl_message(vm_stream *&cl_message)
{
	if (need_cl_message) {
		if (cl_message != NULL) {
			delete cl_message;
		}

		cl_message = get_stream_by_type(VP_CLUSTER);
		ASSERT(cl_message != NULL);

		need_cl_message = false;

		return (true);
	} else {
		return (false);
	}
}

//
// Retrieve a running version through the vm_admin interface.
//
void
vm_admin_int::get_running_version(const version_manager::vp_name_t name,
	version_manager::vp_version_t &v, Environment &e)
{

	versioned_protocol *vpp = get_vp_by_name(name);

	if (!vpp) {
		e.exception(new version_manager::not_found);
		return;
	}

	if (vpp->get_vp_type() == VP_BTSTRP_NODEPAIR ||
	    vpp->get_vp_type() == VP_NODEPAIR) {
		e.exception(new version_manager::wrong_mode);
		return;
	}

	if (!vpp->get_running_version(v)) {
		e.exception(new version_manager::too_early);
		return;
	}
}

void
vm_admin_int::set_running_version(const version_manager::vp_name_t name,
    version_manager::vp_version_t &v)
{
	versioned_protocol *vpp = get_vp_by_name(name);
	vpp->set_running_version_int(v);
}
//
// Retrieve a nodepair running version through the vm_admin interface.
//
void
vm_admin_int::get_nodepair_running_version(
	const version_manager::vp_name_t name,
	sol::nodeid_t n, sol::incarnation_num,
	version_manager::vp_version_t &v, Environment &e)
{

	versioned_protocol *vpp = get_vp_by_name(name);

	if (!vpp) {
		e.exception(new version_manager::not_found);
		return;
	}

	if (vpp->get_vp_type() != VP_BTSTRP_NODEPAIR &&
	    vpp->get_vp_type() != VP_NODEPAIR) {
		e.exception(new version_manager::wrong_mode);
		return;
	}

	if (!vpp->get_running_version(v, n)) {
		e.exception(new version_manager::too_early);
		return;
	}
}

//
// Get a list of the vps specified on this system
//
void
vm_admin_int::get_vp_list_int(version_manager::vp_info_seq_out o)
{
	uint16_t i;

	o = new version_manager::vp_info_seq(num_vps, num_vps);

	version_manager::vp_info_seq  &vio = *o;

	vio.length(num_vps);

	for (i = 0; i < num_vps; i++) {
		vio[i].vp_name = os::strdup(vp_array[i]->get_name());
		vio[i].vp_desc = os::strdup(vp_array[i]->get_vp_description());
		vio[i].vp_source = os::strdup(vp_array[i]->get_vp_source());
	}
}

//
// Perform a binary search for a vp with the specified name
//
versioned_protocol *
vm_admin_int::get_vp_by_name(const char *name)
{
	if (!vp_array)
		return (NULL);

	// Use the vp_array to perform a binary search for the vp in question
	int cur = num_vps/2, lower = 0, upper = num_vps, cmp;

	while (lower < upper) {
		cmp = strcmp(name, vp_array[cur]->get_name());
		if (cmp == 0) {
			return (vp_array[cur]);
		} else if (cmp > 0) {
			lower = cur + 1;
		} else {
			upper = cur;
		}
		cur = (lower + upper)/2;
	}
	return (NULL);
}

vm_stream* vm_admin_int::get_cv_stream()
{
	return (custom_stream);
}

// Return the change flag.
bool
vm_admin_int::get_change_flag()
{
	return (change_flag);
}

// Return the change flag.
void
vm_admin_int::set_change_flag(bool B)
{
	change_flag = B;
}
