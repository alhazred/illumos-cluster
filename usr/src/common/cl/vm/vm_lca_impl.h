/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VM_LCA_IMPL_H
#define	_VM_LCA_IMPL_H

#pragma ident	"@(#)vm_lca_impl.h	1.10	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <h/replica_int.h>
#include <repl/repl_mgr/cb_coord_control_impl.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <sys/rm_util.h>
#include <sys/threadpool.h>
#include <sys/os.h>
#include <vm/vm_dbgbuf.h>

//
// vm_lca_impl.h contains class definiation for the Rolling Upgrade version
// manager local callback agent interface.
//

class vm_lca_impl : public McServerof<version_manager::vm_lca> {

public:
	// constructor
	vm_lca_impl();

	// destructor
	~vm_lca_impl();

	void	_unreferenced(unref_t);

	//
	// Upgrade the reference to the cb_coord_control after the
	// replica manager is upgraded.
	//
	void	upgrade_cb_coord_control_ref(uint16_t new_idl_version);

	// Callback task processes individual UCC's callback.
	class callback_task : public defer_task {
	public:
		callback_task(const char *callback_ucc_name,
		    const version_manager::vp_version_t &version,
		    version_manager::upgrade_callback_ptr cb_p,
		    bool is_btsrp_cluster_mode);
		~callback_task();

		void execute();

	private:
		char	*ucc_name;
		version_manager::vp_version_t	vp_version;
		version_manager::upgrade_callback_var	callback_p;
		bool	btstrp_cl;
	};

	//
	// Called from a callback thread after it has finished its assigned
	// callback task.
	//
	void callback_thread_done(bool is_btstrp_cl);

	// Called by orb::shutdown() to shutdown this object.
	void	shutdown(Environment &);

	// Called from vm_admin
	void	register_uccs(const version_manager::ucc_seq_t &new_ucc,
		    version_manager::upgrade_callback_ptr callbackp,
		    bool is_btstrp_cl_vp, Environment &e);

	// Unregister UCCs associated with the specified callback object.
	void	unregister_uccs(
		    version_manager::upgrade_callback_ptr cb_p);

	//
	// Static methods
	//
	// Called from orb::initialize() to instantiate a vm_lca object per
	// domain during node boot time.
	//
	static	int	initialize();

	// method to access the vm_lca object globally
	static vm_lca_impl &	the();

	// register with RM. Called from cmm callback step10
	static void	register_lca();

	// IDL interfaces

	//
	// The cb_coordinator invokes this function on all lcas to start
	// callbacks for cluster mode vps. Each lca will allocate
	// number of threads, one for each callback to execute the actual
	// callback.
	//
	bool	do_callback_task(
		    const version_manager::string_seq_t &ucc_seq,
		    const version_manager::vp_version_seq &new_version_seq,
		    uint32_t callback_task_id,
		    Environment &e);

	//
	// Called by the cb_coordinator recovery code during upgrade commit to
	// find out the last callback activity and status on this lca.
	//
	void	get_last_cl_callback_task_id(uint32_t &task_id, Environment &e);

	void	lock_vps_for_upgrade_commit(Environment &e);
	void	unlock_vps_after_upgrade_commit(Environment &e);
	void	invoke_finish_upgrade_commit_on_vm(Environment &e);

	// Unregister a group of uccs.
	void    unregister_uccs(
		    const version_manager::string_seq_t &ucc_name_seq,
		    Environment &e);

#ifdef DEBUG
	// Debugging hooks to support query_vm
	void	debug_print(char **);
#endif

private:
	// reference to associated cb_coord_control
	replica_int::cb_coord_control_var	cbcc_v;

	// There is one vm lca per cluster node.
	static vm_lca_impl *the_vm_lcap;

	// Indicate whether this LCA has registered with the cb_coordinator
	bool	registered;

	//
	// Number of non-bootstrap cluster vp callback threads that are still
	// in progress.
	//
	uint_t	num_unfinished_cl_callbacks;

	// Number of in-progress bootstrap cluster vp callback threads.
	uint_t	num_unfinished_btstrp_cl_callbacks;

	// The id of the last callback received on this lca.
	uint32_t	last_cl_callback_task_id;

	// Indicate whether we have locked vps on the local version manager.
	bool		vps_locked;

	// Protects all data members except num_unfinished_cl_callbacks
	os::mutex_t	lck;
	os::condvar_t	cv;

	// Lock that protects cbcc_v
	os::rwlock_t	rwlck;

	// Hashtable keeps track of callbacks associated with cluster mode vp.
	string_hashtable_t<version_manager::upgrade_callback_ptr>
	    callback_hashtable;

#ifdef DEBUG
	// Debugging support to track all uccs associated with bstrp cluster vps
	string_hashtable_t<version_manager::ucc *>	bstrp_cl_ucc_hashtable;
#endif

	// Register this lca with the cb_coordinator
	void	register_lca_with_cb_coord();
};

#include <vm/vm_lca_impl_in.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _VM_LCA_IMPL_H */
