/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)vm_admin_impl.cc	1.47	08/05/20 SMI"

#include <vm/vm_admin_impl.h>
#include <vm/vm_comm.h>
#include <vm/vm_coord.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vp_set_key.h>
#include <vm/vp_rhs_version.h>
#include <vm/vp_set.h>
#include <vm/vp_constraint.h>
#include <sys/vm_util.h>
#include <sys/clconf.h>
#include <sys/os.h>
#include <sys/node_order.h>
#include <orb/infrastructure/orb_conf.h>
#include <vm/vm_stream.h>
#include <vm/vm_lca_impl.h>
#include <repl/rmm/rmm.h>
#include <repl/repl_mgr/cb_coord_control_impl.h>
#ifndef	_KERNEL
#include <unode/unode_config.h>
#endif

vm_admin_impl::vm_admin_impl()
{
}

vm_admin_impl::~vm_admin_impl()
{
}

//
// Perform post-construction initialization, from parsing the spec files
// through local_consistency, picking the node-mode versions, and forming
// the btstrp_np_message, passed back through the argument.
//
int
vm_admin_impl::initialize_int(vm_stream *&btstrp_np_message)
{
	ASSERT(lock_held());

	return (vm_admin_int::initialize_int(btstrp_np_message));
}

//
// Run local consistency on a connected subgroup, as indicated by a
// vp_set_key.
//
bool
vm_admin_impl::local_consistency(vp_set_key &key)
{
	ASSERT(lock_held());
	return (vm_admin_int::local_consistency(key));
}

//
// Add each of the versioned protocols of type t in our list to a new
// vm_stream and return it.  The versioned_protocols are added in sort
// order, and the some parts of the design rely on this.
//
vm_stream *
vm_admin_impl::get_stream_by_type(vp_type t)
{
	ASSERT(lock_held());
	return (vm_admin_int::get_stream_by_type(t));
}

void
#ifdef	DEBUG
vm_admin_impl::_unreferenced(unref_t cookie)
#else
vm_admin_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Generates the bootstrap cluster message.  Called during the first
// cmm callback step from vm_comm.  If the first argument is not NULL,
// and the message is being replaced, the old vm_stream will be
// deleted.  True is returned if the message has been replaced, false
// otherwise.
//
bool
vm_admin_impl::new_btstrp_cl_message(vm_stream *&btstrp_cl_message)
{
	ASSERT(lock_held());
	return (vm_admin_int::new_btstrp_cl_message(btstrp_cl_message));
}

//
// Generates the non-bootstrap message.  Similar to
// new_btstrp_cl_message.
//
bool
vm_admin_impl::new_cl_message(vm_stream *&cl_message)
{
	ASSERT(lock_held());
	return (vm_admin_int::new_cl_message(cl_message));
}

//
// Set the running versions for vps of the specified type based on the
// information in the stream.  Committing is used to pick which entry
// in the stream to use.  rnode is used to specify a remote node if
// type is nodepair.
//
void
vm_admin_impl::set_rvs(vm_stream &vms, vp_type type, nodeid_t rnode)
{
	ASSERT(lock_held());
	// We're doing an upgrade commit, so set the change_flag to true.
	vm_admin_int::set_rvs(vms, type, rnode);
}



//
// Retrieve a running version through the vm_admin interface.
//
void
vm_admin_impl::get_running_version(const version_manager::vp_name_t name,
	version_manager::vp_version_t &v, Environment &e)
{
	CORBA::Exception	*exp;
	const uint_t		max_retries = 15;
	const uint_t		sleep_interval = 1;
	uint_t			retries = 0;

	lock();

	//
	// If Rolling upgrade is in progress from SC3.2, version manager
	// is fully enabled after the upgrade completes. During the interval,
	// when the CMM commits to use the new version but the CMM
	// reconfiguration has not completed the version manager callback
	// steps, we may get a too_early exception when trying to get a
	// running version. We add retries here to handle this case.
	//
	do {
		vm_admin_int::get_running_version(name, v, e);
		if (((exp = e.exception()) == NULL) ||
		    (version_manager::too_early::_exnarrow(exp) == NULL)) {
			break;
		}
		VM_DBG(("Got too_early exception while trying to get running "
		    "version for vp %s\n", name));
		retries++;
		if (retries >= max_retries) {
			break;
		}
		e.clear();
		// Sleep for seconds equal to SLEEP_INTERVAL.
		unlock();
		os::usecsleep((os::usec_t)(sleep_interval*MICROSEC));
		lock();
	} while (1);

	unlock();
}

//
// Retrieve a nodepair running version through the vm_admin interface.
//
void
vm_admin_impl::get_nodepair_running_version(
	const version_manager::vp_name_t name,
	sol::nodeid_t n, sol::incarnation_num i,
	version_manager::vp_version_t &v, Environment &e)
{
	lock();

	if (i != INCN_UNKNOWN && !vm_comm::the().current_pairwise_incn(n, i)) {
		unlock();
		e.exception(new version_manager::old_incarnation);
		return;
	}

	vm_admin_int::get_nodepair_running_version(name, n, i, v, e);

	unlock();
}

//
// Return a stream of custom versions for all custom callbacks.
//
vm_stream*
vm_admin_impl::get_cv_streams()
{
	vm_stream *vmsp;
	// We use a VP stream to store custom versions.
	vmsp = new vm_stream(vm_stream::VP_STREAM_1,
	    (uint16_t)num_vps, 0, 0); //lint !e747

	version_manager::vp_version_t set_ver;
	set_ver.major_num = 0;
	set_ver.minor_num = 0;

	uint16_t i;
	//
	// For every vp.
	//
	for (i = 0; i < num_vps; i++) {
		if (vp_array[i]->is_custom()) {
			//
			// If custom, then store custom versions in stream.
			//
			vmsp->put_header(
			    vp_array[i]->get_name(),
			    (uint16_t)vp_array[i]->get_vp_type(),
			    set_ver, set_ver);

			vmsp->put_version_range(vp_array[i]->custom_versions);
			// Terminate the list.
			vmsp->put_vp_rhs_version(NULL);
			version_range tmp_vr;
			vmsp->put_version_range(tmp_vr);
		}
	}
	// Terminate stream.
	vmsp->put_versioned_protocol(NULL);
	return (vmsp);
}

//
// Return a stream listing all version protocol names.
//
vm_stream*
vm_admin_impl::get_all_vp_streams()
{
	vm_stream *vmsp;
	// We use a RV stream to store custom versions.
	vmsp = new vm_stream(vm_stream::RV_STREAM_1,
	    (uint16_t)num_vps, 0, 0); //lint !e747

	version_manager::vp_version_t set_ver;
	set_ver.major_num = 0;
	set_ver.minor_num = 0;

	uint16_t i;
	//
	// For every vp.
	//
	for (i = 0; i < num_vps; i++) {

		vmsp->put_header(
		    vp_array[i]->get_name(),
		    (uint16_t)vp_array[i]->get_vp_type(),
		    set_ver, set_ver);
	}

	// Terminate stream.
	vmsp->put_header(NULL, (uint16_t)0, set_ver, set_ver);
	return (vmsp);
}

bool
vm_admin_impl::get_streams(version_manager::node_streams_seq_out streams,
    Environment &)
{
	Environment e; //lint !e527
	bool send_ok; //lint !e527

	nodeid_t coordid = vm_comm::the().get_coord_nodeid();

	//
	// forward request to primary admin (if needed).
	//
	if (coordid != orb_conf::local_nodeid()) {

		version_manager::vm_admin_ptr vma_v = vm_util::get_vm(coordid);
		if (CORBA::is_nil(vma_v)) {
			streams = new version_manager::node_streams_seq;
			return (false);
		}

		send_ok = vma_v->get_streams(streams, e);

		if (!e.exception() && send_ok) {
			return (true);
		} else {
			e.clear();
			streams = new version_manager::node_streams_seq;
			return (false);
		}
	}

	lock();

	streams = new version_manager::node_streams_seq(NODEID_MAX + 1,
	    NODEID_MAX + 1);
	version_manager::node_streams_seq &vs = *streams;
	vs.length(NODEID_MAX + 1);

	vs.load(
	    NODEID_MAX + 1,
	    NODEID_MAX + 1,
	    version_manager::node_streams_seq::allocbuf(NODEID_MAX + 1),
	    true); // release = true

	vp_type thetypes[] = {VP_BTSTRP_NODEPAIR, VP_BTSTRP_CLUSTER,
				    VP_CLUSTER};
	uint16_t ntypes = 3;

	cmm::cluster_state_t cstate;
	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	cmm_cntl_v->get_cluster_state(cstate, e);
	if (e.exception()) {
		e.clear();
		unlock();
		return (false);
	}

	//
	// Store VP_BTSTRP_NODEPAIR, VP_BTSTRP_CLUSTER
	// and VP_CLUSTER streams for every node.
	//
	for (nodeid_t outer = 1; outer <= NODEID_MAX; ++outer) {

		vs[outer].length(0);

		//
		// If node in cluster.
		//
		if (cstate.members.members[outer] != INCN_UNKNOWN) {

			//
			// For every stream type we are interested in.
			//
			for (uint16_t inner = 0; inner < ntypes; ++inner) {

				vm_stream *stream =
					vm_comm::the_vm_coord().
					get_stream_by_node_and_type(outer,
					    thetypes[inner]);
				// Previous call always return a new stream.
				//
				// If first stream to add.
				//
				if (vs[outer].length() == 0) {
					vs[outer].load(
					    VP_TYPE_ARRAY_SIZE,
					    VP_TYPE_ARRAY_SIZE,
					    version_manager::streams_seq::
					    allocbuf(VP_TYPE_ARRAY_SIZE),
					    true); // release = true

					for (uint32_t in = 0;
					in < VP_TYPE_ARRAY_SIZE;
					++in) {
						vs[outer][in].
							length(0);
					}
				}

				uint32_t size = (uint32_t)
					stream->written_length();

				char *buffer = new char[size];
				bcopy(stream->_buffer, buffer,
				    (size_t)size); //lint !e668

				vs[outer][thetypes[inner]].load(
				    size, size, (unsigned char*)buffer,
				    true); // release = true

				// Free the stream.
				delete stream;
				stream = NULL;
			}
		}
	}

	//
	// Store all custom version ranges (use sequence 0 - stream 0).
	//
	vs[0].load(VP_TYPE_ARRAY_SIZE, VP_TYPE_ARRAY_SIZE,
	    version_manager::streams_seq::allocbuf(VP_TYPE_ARRAY_SIZE),
	    true); // release = true

	for (uint32_t in = 0; in < VP_TYPE_ARRAY_SIZE; ++in) {
		vs[0][in].length(0);
	}

	vm_stream *stream = get_cv_streams();
	uint32_t size = (uint32_t)stream->written_length();

	char *buffer = new char[size];
	bcopy(stream->_buffer, buffer,
	    (size_t)size); //lint !e668

	vs[0][0].load(size, size, (unsigned char*)buffer,
	    true); // release = true

	// Free the stream.
	delete stream;
	stream = NULL;

	//
	// Store all vp names (use sequence 0 - stream 1).
	//
	stream = get_all_vp_streams();
	size = (uint32_t)stream->written_length();

	char *buffer1 = new char[size];
	bcopy(stream->_buffer, buffer1,
	    (size_t)size); //lint !e668

	vs[0][1].load(size, size, (unsigned char*)buffer1,
	    true); // release = true

	// Free the stream.
	delete stream;
	stream = NULL;

	unlock();
	return (true);
}

//
// Get a list of the vps specified on this system.
//
void
vm_admin_impl::get_vp_list(version_manager::vp_info_seq_out o, Environment &)
{
	lock();
	get_vp_list_int(o);
	unlock();
} //lint !e1746

//
// Get a list of the vps by type specified on this system.
//
void
vm_admin_impl::get_vp_list_by_type(version_manager::vp_info_seq_out o,
	vp_type specified_type, Environment &e)
{
	version_manager::vp_info_seq *all_vps_seq;
	uint_t	i;
	uint_t	count = 0;

	// Get all the vps, regardless of their type.
	get_vp_list(all_vps_seq, e);
	// Only return the vps of the specified type.
	version_manager::vp_info_seq &all_vps = *all_vps_seq;
	o = new version_manager::vp_info_seq(num_vps, num_vps);
	version_manager::vp_info_seq & vio = *o;
	for (i = 0; i < all_vps.length(); i++) {
		if ((get_vp_by_name(all_vps[i].vp_name))->get_vp_type() ==
		    specified_type) {
			// Copy the contents.
			vio[count].vp_name = os::strdup(all_vps[i].vp_name);
			vio[count].vp_desc = os::strdup(all_vps[i].vp_desc);
			vio[count].vp_source = os::strdup(all_vps[i].vp_source);
			count++;
		}
	}
	//
	// Reinitialize vio to its proper size.  It's okay if the count is zero
	// since we'll just return an empty sequence.
	//
	vio.length(count);
	delete all_vps_seq;
}

//
// Get the version_manager subsystem to perform a debug operation.
// The content is specified using the request_string.
//
void
vm_admin_impl::debug_request(const char *request_string, Environment &)
{
	int				n;
	Environment			e;

	if (os::strcmp(request_string, "dump_state") == 0) {
		lock();
		VM_DBG(("Number of vps = %u\n", num_vps));
		for (n = 0; n < num_vps; n++)
			vp_array[n]->debug_print();
		unlock();
	} else if (os::strcmp(request_string, "upgrade_commit") == 0) {
		version_manager::vp_state	s;
		membership_bitmask_t		outdated_nodes;
		upgrade_commit(s, outdated_nodes, e);
		if (e.exception()) {
			e.exception()->print_exception("upgrade_commit");
			e.clear();
		}
	} else if (os::strncmp(request_string, "unreg",
	    os::strlen("unreg")) == 0) {
		//
		// Unregister this ucc.
		// uccs are kept in two separate locations depending on their
		// associated vp.  uccs with bootstrap cluster mode vps are
		// registered in the local node's lca.  uccs with non-bootstrap
		// cluster mode vps are registered in the cb_coordinator.  At
		// debug_request time, we are not sure which vp the ucc is
		// associated with.  So we call unregister on both.  It's okay
		// if the ucc is not found in a particular location, the
		// removal simply no-ops and returns.
		//
		char *ucc_name = NULL;
		ucc_name = os::strdup(request_string + 6);
		// Attempt removal of the ucc from the cb_coordinator.
		repl_mgr_impl *repl_mgr_p = repl_mgr_impl::get_rm();
		ASSERT(repl_mgr_p);
		repl_mgr_p->get_cb_coordinator()->remove_ucc(ucc_name);
		// Attempt removal of the ucc from this lca.

		version_manager::string_seq_t ucc_name_seq(1, 1);
		ucc_name_seq[0] = os::strdup(ucc_name);
		vm_lca_impl::the().unregister_uccs(ucc_name_seq, e);
	}
}

//
// Get the version_manager subsystem to print into an IO buffer.
// The content is specified using the request_string.
//
void
vm_admin_impl::debug_request_io(const char *request_string, CORBA::String_out
    out_string, Environment &)
{
	char *debug_string = NULL;
	if (os::strcmp(request_string, "print_uccs") == 0) {
		// Print the cluster mode uccs that reside on the cb_coord.
		repl_mgr_impl *repl_rm_p = repl_mgr_impl::get_rm();
		ASSERT(repl_rm_p);

#ifdef	DEBUG
		repl_rm_p->get_cb_coordinator()->debug_print(&debug_string);
		//
		// Print the btstrp cluster mode uccs that reside locally on
		// the lca.
		//
		vm_lca_impl::the().debug_print(&debug_string);
		//
		// Print the ucc_group list that would be constructed if an
		// upgrade commit occurred now.
		//
		repl_rm_p->get_cb_coordinator()->debug_print_uglist(
		    &debug_string);
#endif	// DEBUG

		// Construct the out_string.
		out_string = debug_string;
	}
}

void
vm_admin_impl::finish_upgrade_commit()
{
	versioned_protocol		*v;
	version_manager::vp_version_t	vpv;

	ASSERT(lock_held());

	// Clear the change flag.
	vm_admin_int::set_change_flag(false);

	if (cl_uv_seq != 0) {
		VM_DBG(("Upgrading to rvs computed in sequence %llu\n",
		    cl_uv_seq));
		ASSERT(cl_uv_seq > cl_rv_seq);
		cl_rv_seq = cl_uv_seq;
		cl_uv_seq = 0;
		IntrList<versioned_protocol, _SList> &vpl =
		    vp_typelist[VP_CLUSTER];
		IntrList<versioned_protocol, _SList>::ListIterator
		    vpli(vpl);
		while ((v = vpli.get_current()) != NULL) {
			v->get_upgrade_version(vpv);
			ASSERT(vpv.major_num != 0);
			VM_DBG(("Setting rv of %s to %u.%u\n",
			    v->get_name(), vpv.major_num,
			    vpv.minor_num));
			v->set_running_version(vpv, 0, custom_stream);
			vpv.major_num = vpv.minor_num = 0;
			v->set_upgrade_version(vpv);
			vpli.advance();
		}
		//
		// Now that we've changed the running versions, we
		// need to generate a new cl_message to send to the
		// coordinator.
		//
		need_cl_message = true;
		//
		// Changing VP_CLUSTER rvs also changes the btstrp_cl
		// running versions we could potentially upgrade to.
		//
		need_btstrp_cl_message = true;
	} else {
		VM_DBG(("No uvs to upgrade to, rvs calculatd in %llu\n",
		    cl_rv_seq));
	}
}

void
vm_admin_impl::register_upgrade_callbacks(
    const version_manager::ucc_seq_t &ucc_seq,
    version_manager::upgrade_callback_ptr cb_p,
    const version_manager::vp_version_t &highest_version,
    version_manager::vp_version_t &running_version,
    Environment &e)
{
	Environment env;
	uint_t num_uccs = ucc_seq.length();
	ASSERT(num_uccs > 0);

	if (ucc_seq.length() == 0) {
		e.exception(new version_manager::invalid_ucc);
		return;
	}

	// Check if all uccs have the same vp name specified.
	for (uint_t i = 1; i < num_uccs; i++) {
		if (os::strcmp(ucc_seq[i].vp_name, ucc_seq[0].vp_name) != 0) {
			e.exception(new version_manager::invalid_ucc());
			return;
		}
	}

again:
	lock();
	vm_comm::the().wait_until_vps_are_unlocked();

	versioned_protocol *vpp =
	    get_vp_by_name((const char *)ucc_seq[0].vp_name);

	if (!vpp) {
		unlock();
		e.exception(new version_manager::unknown_vp);
		return;
	}

	if (vpp->get_vp_type() != VP_CLUSTER &&
	    vpp->get_vp_type() != VP_BTSTRP_CLUSTER) {
		unlock();
		e.exception(new version_manager::invalid_ucc);
		return;
	}

	if (!vpp->get_running_version(running_version)) {
		unlock();
		e.exception(new version_manager::too_early);
		return;
	}
	unlock();

	VM_DBG(("in register_upgrade_callbacks, vp_name is %s\n, highest is "
	    "%u.%u, running is %u.%u\n", (const char *)ucc_seq[0].vp_name,
	    highest_version.major_num, highest_version.minor_num,
	    running_version.major_num, running_version.minor_num));

	//
	// If the upgrade version is less than or equal to the current
	// version, we are done.
	//
	if ((highest_version.major_num == running_version.major_num &&
	    highest_version.minor_num <= running_version.minor_num) ||
	    (highest_version.major_num < running_version.major_num)) {
		return;
	}

	// Register the list of UCCs with the cb_coordinator.
	vm_lca_impl::the().register_uccs(ucc_seq, cb_p,
	    (vpp->get_vp_type() == VP_BTSTRP_CLUSTER), env);
	CORBA::Exception *ex = env.exception();
	if (ex != NULL) {
		if (version_manager::retry_needed::_exnarrow(ex) != NULL) {
			env.clear();
			VM_DBG(("in register_upgrade_callbacks, retry\n"));
			goto again;
		}
		// Transfer the exception to the return environment.
		e.exception(env.release_exception());
		return;
	}
	//
	// Get the running version again in case there was a commit
	// before we finished registering the callback object.
	// This at least guarantees that the running version is
	// the latest or a callback is generated (or possibly both).
	//
	// XXX There is a minor memory leak in this case since the callback
	// object won't be unregistered if we are now at the highest version
	// for all VPs (so no further upgrade commits). Ideally, we would
	// check for this case and ungregister the UCC.
	//
	lock();
	// If it wasn't too early last time, it should succeed again.
	bool ok = vpp->get_running_version(running_version);
	ASSERT(ok);
	unlock();
}

void
vm_admin_impl::unregister_upgrade_callbacks(
    version_manager::upgrade_callback_ptr cb_p,
    Environment &)
{
	vm_lca_impl::the().unregister_uccs(cb_p);
} //lint !e715

void
vm_admin_impl::upgrade_commit(version_manager::vp_state &s,
    membership_bitmask_t &outdated_nodes, Environment &e)
{
	// Get a reference to the replica manager.
	replica::rm_admin_var   rm_admin_v = rm_util::get_rm();
	replica_int::rm_var the_rm_v = replica_int::rm::_narrow(rm_admin_v);

	ASSERT(!CORBA::is_nil(the_rm_v));

	//
	// Use the _generic_method to get a highest version reference to the
	// rm. This is necessary since upgrade_commit() is supported in
	// replica_manager.vp 1.1 and onwards. If the the cluster is running
	// 1.0 of replica_manager.vp, we will not be able to invoke
	// upgrade_commit() using the old versioned rm referece.
	//
	// Generate the function name and put that in the data_str.
	//

	CORBA::octet_seq_t	data(33, 33,
	    (uint8_t *)"get_highest_version_root_obj_ref", false);
	CORBA::object_seq_t	objs(0);

	//
	// Note that this IDL invocation resuses the environment variable
	// passed to us. This is only works because vm_admin_impl is not an
	// HA interface.
	//
	the_rm_v->_generic_method(data, objs, e);
	if (!e.exception()) {
		ASSERT(objs.length() == 1);
		ASSERT(objs[0] != nil);
		// Unpack the rm reference.
		the_rm_v = replica_int::rm::_narrow(objs[0]);
		the_rm_v->upgrade_commit(s, outdated_nodes, e);
	}
}

//
// is_upgrade_needed()
// Returns true if an upgrade commit is needed.  The actual calculation must be
// done on the VM primary (that is always co-located with the RM primary)
// because only the primary has all the correct clusterwide information.
// For more details see bug 4872631.
//
bool
vm_admin_impl::is_upgrade_needed(version_manager::vp_state &s,
    membership_bitmask_t &outdated_nodes, Environment &e)
{
	nodeset	minimal_nodes; //lint !e527
	bool upgrade_needed = false; //lint !e527
	bool calc_done = false;
	nodeid_t	rm_primary;
	version_manager::vm_admin_var	vm_v;

	// Determine the VM primary node.
	rm_primary = rmm::the().primary_location();

	if (rm_primary == orb_conf::local_nodeid()) {
		//
		// Run the calculation on the VM primary node only.
		// Grab the VM lock so only one invocation is inside the VM
		// at any one time.
		//
		lock();
		upgrade_needed = vm_comm::the().is_upgrade_needed(s,
		    minimal_nodes);
		outdated_nodes = minimal_nodes.bitmask();
		unlock();
	} else {
		// Send the request to the VM primary node.
		VM_DBG(("vm_admin_impl: Sending is_upgrade_needed() "
		    "request to the VM primary node %u\n", rm_primary));
		while (!calc_done) {
			vm_v = vm_util::get_vm(rm_primary);
			if (CORBA::is_nil(vm_v)) {
				//
				// The VM primary might have died, so find the
				// new VM primary and try again.  It's okay
				// if this node itself becomes the new primary
				// because we will just send this request to
				// ourselves and process it correctly.
				//
				rm_primary = rmm::the().primary_location();
				VM_DBG(("vm_admin_impl: Resending "
				    "is_upgrade_needed() request to the "
				    "new VM primary node %u\n", rm_primary));
				continue;
			}
			// Do calculation on the VM primary node.
			upgrade_needed = vm_v->is_upgrade_needed(s,
			    outdated_nodes, e);
			// Check for any exceptions.
			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					// The VM primary died, so try again.
					e.clear();
					rm_primary = rmm::the().
					    primary_location();
					VM_DBG(("vm_admin_impl: COMM_FAILURE "
					    "exception : Resending "
					    "is_upgrade_needed() request to "
					    "the new VM primary node %u\n",
					    rm_primary));
					continue;
				} else if (CORBA::VERSION::_exnarrow(
				    e.exception())) {
					//
					// The VM primary has not been upgraded
					// yet to support the is_upgrade_needed
					// method. This means that the primary
					// is 3.1, 3.1U1, or 3.1U2.  It is the
					// caller of is_upgrade_needed who
					// should handle this specific
					// exception.
					//
					VM_DBG(("vm_admin_impl: The VM primary "
					    "has not been upgraded yet.\n"));
					//
					// Set the outdated nodes to be just
					// the VM primary.
					//
					nodeset primary_only;
					primary_only.add_node(rm_primary);
					outdated_nodes = primary_only.bitmask();
					calc_done = true;
				} else {
					// Unexpected error.
					VM_DBG(("vm_admin_impl: Unexpected "
					    "exception. \n"));
					calc_done = true;
				}
			} else {
				calc_done = true;
			}
		}
	}
	return (upgrade_needed);
}

//
// Perform upgrade callbacks on btstrp cl vps.
// Caller: vm_comm::commit_btstrp_cl().
//
void
vm_admin_impl::do_btstrp_cl_callbacks()
{
	ASSERT(lock_held());

	Environment		e;
	versioned_protocol	*v;
	bool			retval;
	IntrList<versioned_protocol, _SList> &vpl =
	    vp_typelist[VP_BTSTRP_CLUSTER];
	IntrList<versioned_protocol, _SList>::ListIterator vpli(vpl);
	uint_t	size = vp_typelist[VP_BTSTRP_CLUSTER].count();
	version_manager::string_seq_t ucc_name_seq(size, size);
	version_manager::vp_version_seq version_seq(size, size);

	//
	// Get a list of bootstrap cluster vps and their current running
	// versions.
	// Bootstrap cluster UCCs use the same name as their associated
	// bootstrap cluster vps.
	//
	for (uint_t i = 0; (v = vpli.get_current()) != NULL;
	    i++, vpli.advance()) {
		ucc_name_seq[i] = os::strdup(v->get_name());
		retval = v->get_running_version(version_seq[i]);
		CL_PANIC(retval);
	}

	//
	// Ask the LCA to perform callbacks on bootstrap cluster vps
	// if necessary. do_callback_task() doesn't return untill
	// all callbacks are completed.
	//
	retval = vm_lca_impl::the().do_callback_task(ucc_name_seq,
	    version_seq, 0, e);
	CL_PANIC(!e.exception() && !retval);
}

//
// Accessor function to return the value of the change flag.
//
bool
vm_admin_impl::get_change_flag()
{
	return (vm_admin_int::get_change_flag());
}
