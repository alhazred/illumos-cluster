//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)coord_cluster_info.cc 1.15     08/05/20 SMI"

#include <vm/coord_cluster_info.h>
#include <cmm/cmm_ns.h>
#include <vm/vm_dbgbuf.h>
#include <vm/vm_stream.h>

void
coord_cluster_info::set_btstrp_node_info(nodeid_t node, vm_stream *vmsp,
	uint32_t qvotes, uint32_t &rva)
{
	quorum_votes[node] = qvotes;

	rva = RV_NONE;

#ifdef	DEBUG
	reinit_since_last_change = false;
#endif

	if (vmsp == NULL)
		return;

	cmm::seqnum_t	node_btstrp_cl_rv_seq = vmsp->get_rv_seqnum();

	if (node_btstrp_cl_rv_seq == 0) {
		btstrp_initializing_nodes.add_node(node);
	} else {
		btstrp_initializing_nodes.remove_node(node);
	}

	if (node_btstrp_cl_rv_seq == current_btstrp_cl_rv_seq) {
		has_current_btstrp_cl_rv.add_node(node);
		if (node_btstrp_cl_rv_seq != 0)
			rva = RV_VERIFY;
	} else if (node_btstrp_cl_rv_seq > current_btstrp_cl_rv_seq) {
		has_current_btstrp_cl_rv.empty();
		has_current_btstrp_cl_rv.add_node(node);
		current_btstrp_cl_rv_seq = node_btstrp_cl_rv_seq;
		rva = RV_LOAD;
	} else {
		ASSERT(node_btstrp_cl_rv_seq < current_btstrp_cl_rv_seq);
		has_current_btstrp_cl_rv.remove_node(node);
	}
}

//
// The coordinator must digest the information from some node's vm stream
// message.  A major task is to determine if the message holds up-to-date
// and relevent information.  The sequence numbers act as time stamps, allowing
// comparison with internal state to determine what should be done.
//
// NOTE : This method only applies to non-bootstrap cluster mode vps.
//
void
coord_cluster_info::set_node_info(nodeid_t node, vm_stream *vmsp,
	uint32_t &rva)
{
	rva = RV_NONE;

#ifdef	DEBUG
	reinit_since_last_change = false;
#endif

	if (vmsp == NULL)
		return;

	// Obtain the sequence numbers for the running and upgrade versions
	// from the vm_stream.  The sequence numbers are set by the CMM
	// configuration number when the running and upgrade versions were
	// determined.  Please note that the sequence numbers are different than
	// the actual running and upgrade version numbers themselves.
	//
	// Running version (rv) sequence numbers start at 0 and strictly
	// increase.  They only are reset to 0 when the entire cluster reboots.
	// Upgrade versions (uv) sequence numbers start at 0 and increase.  They
	// will be reset to 0 whenever an upgrade commits or when there is no
	// upgrade version available.
	cmm::seqnum_t	node_cl_rv_seq = vmsp->get_rv_seqnum();
	cmm::seqnum_t	node_cl_uv_seq = vmsp->get_uv_seqnum();

	VM_DBG(("set_node_info 1: vm_stream rv seq %llu, "
	    "vm_stream uv seq %llu, current rv seq %llu, "
	    "current uv seq %llu\n",
	    node_cl_rv_seq, node_cl_uv_seq, current_cl_rv_seq,
	    current_cl_uv_seq));

	// Check to see if this node is initializing.
	if (node_cl_rv_seq == 0) {
		initializing_nodes.add_node(node);
	} else {
		initializing_nodes.remove_node(node);
	}

	// Check the running version sequence number
	if (node_cl_rv_seq == current_cl_rv_seq) {
		// Both the incoming msg and this node's internal rv sequence
		// match which indicates that we've got the most up-to-date
		// information about the rv's.
		has_current_cl_rv.add_node(node);
		if (node_cl_rv_seq != 0) {
			// To be safe, verify that the rv's match by setting
			// the verify flag.
			rva = RV_VERIFY;
		}
	} else if (node_cl_rv_seq > current_cl_rv_seq) {
		// The node's internal rv is out-of-date when compared to the
		// incoming msg.  Reset this node's state by assigning a new
		// rv and set the load flag.
		has_current_cl_rv.empty();
		has_current_cl_rv.add_node(node);
		current_cl_rv_seq = node_cl_rv_seq;
		rva = RV_LOAD;
	} else {
		ASSERT(node_cl_rv_seq < current_cl_rv_seq);
		// The incoming msg from node shows that it holds out-of-date
		// rv information so keep track of this.
		has_current_cl_rv.remove_node(node);
	}
	VM_DBG(("set_node_info 2: vm_stream rv seq %llu, "
	    "vm_stream uv seq %llu, current rv seq %llu, "
	    "current uv seq %llu\n",
	    node_cl_rv_seq, node_cl_uv_seq, current_cl_rv_seq,
	    current_cl_uv_seq));
	ASSERT(current_cl_rv_seq >= node_cl_rv_seq);

	// Check the upgrade version sequence number
	if (node_cl_uv_seq == current_cl_uv_seq) {
		// Both the incoming msg and this node's internal uv sequence
		// match which indicates that we've got the most up-to-date
		// information about the uv's.
		if (current_cl_uv_seq > current_cl_rv_seq) {
			// The uv seq is still up-to-date so verify that the uv
			// itself is the same.
			has_current_cl_uv.add_node(node);
			if (node_cl_uv_seq != 0) {
				// To be safe, verify that the uv's match by
				// setting the verify flag.
				rva |= UV_VERIFY;
			}
		} else {
			ASSERT(current_cl_uv_seq <= current_cl_rv_seq);
			if (node_cl_uv_seq != 0) {
				// The node's uv is older than its rv, so
				// rezero the upgrade version.
				rva |= UV_CLEAR;
				current_cl_uv_seq = 0;
				has_current_cl_uv.empty();
			} else {
				// The uv seqs are both 0, so we need to check
				// if the node does indeed have the most
				// up-to-date information.
				ASSERT(node_cl_uv_seq == 0 &&
				    current_cl_uv_seq == 0);
				if (current_cl_rv_seq == node_cl_rv_seq) {
					// The message's uv seq is zero; the
					// node that sent the message is the
					// most up-to-date and needs to be kept
					// track of.
					has_current_cl_uv.add_node(node);
				} else {
					// The incoming msg from node shows that
					// it holds out-of-date uv information
					// so keep track of this.
					has_current_cl_uv.remove_node(node);
				}
			}
		}
	} else if (node_cl_uv_seq > current_cl_uv_seq) {
		// The node's internal uv is out-of-date when compared to the
		// incoming msg.
		if (node_cl_uv_seq > current_cl_rv_seq) {
			// Reset this node's state by assigning a new
			// uv and set the load flag.
			has_current_cl_uv.empty();
			has_current_cl_uv.add_node(node);
			current_cl_uv_seq = node_cl_uv_seq;
			rva |= UV_LOAD;
		} else {
			ASSERT(node_cl_uv_seq <= current_cl_rv_seq);
			// The incoming msg from node shows that it holds
			// out-of-date uv information so keep track of this.
			has_current_cl_uv.remove_node(node);
		}
	} else {
		// The incoming msg from node shows that it is
		// out-of-date.
		ASSERT(node_cl_uv_seq < current_cl_uv_seq);

		if (current_cl_uv_seq <= current_cl_rv_seq) {
			// Since the internal rv is older than the internal
			// uv, we need to rezero the uv.
			rva |= UV_CLEAR;
			current_cl_uv_seq = 0;
			has_current_cl_uv.empty();
			// If the message holds that the uv seq is zero, that
			// means that the node that sent the message is the
			// most up-to-date and needs to be kept track of.
			if (node_cl_uv_seq == 0)
				has_current_cl_uv.add_node(node);
		} else {
			ASSERT(current_cl_uv_seq > current_cl_rv_seq);
			// The incoming msg from node shows that it holds
			// out-of-date uv information so keep track of this.
			has_current_cl_uv.remove_node(node);
		}
	}

	VM_DBG(("set_node_info 3: vm_stream rv seq %llu, "
	    "vm_stream uv seq %llu, current rv seq %llu, "
	    "current uv seq %llu; rva=%u\n",
	    node_cl_rv_seq, node_cl_uv_seq, current_cl_rv_seq,
	    current_cl_uv_seq, rva));

}

//
// Compute the internal variables that depend on the current membership.
//
void
coord_cluster_info::reinitialize(cmm::membership_t &cm, bool the_mode)
{
	nodeset *init_nodes;

	num_init_nodes = 0;
	_total_votes = 0;
	_total_ninit_votes = 0;

	non_btstrp = the_mode;

	// Set init_nodes to the right flag map based on non_btstrp.  It will
	// be used in the computations below.
	if (non_btstrp) {
		init_nodes = &initializing_nodes;
	} else {
		init_nodes = &btstrp_initializing_nodes;
	}

	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (cm.members[n] != INCN_UNKNOWN) {
			current_membership.add_node(n);
			if (init_nodes->contains(n)) {
				num_init_nodes++;
			} else {
				_total_ninit_votes += quorum_votes[n];
			}
			_total_votes += quorum_votes[n];
		} else {
			current_membership.remove_node(n);
			quorum_votes[n] = 0;
			init_nodes->remove_node(n);
		}
	}

	init_key = 0;
	_wanted_votes = 0;
	delete_maps();

#ifndef LIBVM

	// Query the quorum algorithm for the current minimum number of votes.
	Environment e;
	_needed_votes = qa_ref->minimum_cluster_quorum(e);
	ASSERT(!e.exception());

#endif
	// _needed_votes is unchanged for libvm from when cci created.

#ifdef	DEBUG
	reinit_since_last_change = true;
#endif
}

//
// Prepare to deliver nodesets with qvotes_wanted quorum votes.  Start with
//
void
coord_cluster_info::init_node_subset(uint32_t qvotes_wanted)
{
	if (_wanted_votes == 0)
		create_maps();

	ASSERT(qvotes_wanted >= _total_ninit_votes);
	// Start with init_key containing a 1 for every node in init_map,
	// and qvotes
	init_key = 1;
	left_shift(init_key, num_init_nodes);
	init_key -= 1;
	init_key_votes = qvote_map[num_init_nodes];

	ASSERT(qvotes_wanted <= _total_votes);
	_wanted_votes = qvotes_wanted;
}

//
// Initialize init_map and qvote_map
//
void
coord_cluster_info::create_maps()
{
	int i = 1, j;
	nodeid_t n_tmp;
	nodeset *init_nodes;

	uint8_t num_init_array_size = num_init_nodes + 1;

	ASSERT(reinit_since_last_change);
	ASSERT(init_map == NULL);

	// set init_nodes to the appropriate flag map
	if (non_btstrp) {
		init_nodes = &initializing_nodes;
	} else {
		init_nodes = &btstrp_initializing_nodes;
	}

	init_map = new nodeid_t[num_init_array_size];
	qvote_map = new uint32_t[num_init_array_size];
	ASSERT(init_map && qvote_map);

	init_map[0] = 0;	// unused

	// Start by setting nodes in nodeid order
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if (current_membership.contains(n) && init_nodes->contains(n)) {
			init_map[i] = n;
			i++;
		}
	}

	ASSERT(i == num_init_array_size);

	// Slowsort the array in descending order by quorum_votes[].
	for (i = 1; i < (num_init_array_size - 1); i++) {
		for (j = i + 1; j < num_init_array_size; j++) {
			if (quorum_votes[init_map[i]] <
			    quorum_votes[init_map[j]]) {	//lint !e661
				n_tmp = init_map[i];
				init_map[i] = init_map[j];	//lint !e661
				init_map[j] = n_tmp;		//lint !e661
			}
		}
	}

	// Set up the qvote_map
	qvote_map[0] = _total_ninit_votes;
	for (i = 1; i < num_init_array_size; i++) {
		qvote_map[i] = quorum_votes[init_map[i]] + qvote_map[i - 1];
	}
}

//
// Call into the quorum algorithm and verify that the number of quorum votes
// there matches what the nodes reported.
//
bool
coord_cluster_info::verify_quorum_votes()
{
	ASSERT(reinit_since_last_change);

#ifndef LIBVM
	nodeid_t n;
	Environment e;
	uint32_t qv;


	for (n = 1; n <= NODEID_MAX; n++) {
		if (current_membership.contains(n)) {
			qv = qa_ref->current_node_votes(n, e);
			ASSERT(!e.exception());
			if (qv != quorum_votes[n])
				return (false);
		}
	}
#endif
	return (true);
}

//
// Return the highest bit (between 1 and the_max inclusive) set in k.
//
uint8_t
coord_cluster_info::highest_in_key(uint64_t k, uint8_t the_max)
{
	uint64_t comp_key = 1;

	if (k == 0)
		return (0);

	uint64_t tmp_key;

	// Perform a binary search on k.
	uint8_t cur = the_max/2, lower = 0, upper = the_max;

	while (lower < upper) {
		tmp_key = k;
		right_shift(tmp_key, cur);
		// If tmp_key is 1, we've shifted correctly.
		if (tmp_key == comp_key) {
			return (cur + 1);
		} else if (tmp_key == 0) {
			upper = cur;
		} else {
			lower = cur + 1;
		}
		cur = (lower + upper)/2;
	}

	// init_key wasn't 0, so there should be a highest defined bit
	ASSERT(0);
	return (0);
}

//
// Return the lowest bit (between 1 and the_max inclusive) set in k.
//
uint8_t
coord_cluster_info::lowest_in_key(uint64_t k, uint8_t the_max)
{
	uint64_t comp_key = 0x8000000000000000;

	if (k == 0)
		return (0);

	uint64_t tmp_key;

	// Perform a binary search on k.
	uint8_t cur = (the_max + 1)/2, lower = 1, upper = the_max + 1;

	while (lower < upper) {
		tmp_key = k;
		// If tmp_key is 2^63, we've shifted correctly.
		left_shift(tmp_key, 64 - cur);
		if (tmp_key == comp_key) {
			return (cur);
		} else if (tmp_key == 0) {
			lower = cur + 1;
		} else {
			upper = cur;
		}
		cur = (lower + upper)/2;
	}

	// init_key wasn't 0, so there should be a lowest defined node
	ASSERT(0);
	return (0);
}

//
// Return the next nodeset that owns _wanted_votes quorum votes
//
void
coord_cluster_info::next_subset(nodeset &ns)
{
	uint8_t		lowest, highest, i;
	uint64_t	decode_key, mask;
	uint32_t	tmp_votes;
#ifdef	DEBUG
	uint64_t	old_key;
#endif

	ASSERT(_wanted_votes != 0);

	// If init_key is already 0, just return empty
	if (init_key == 0) {
		ns.empty();
		return;
	}

	// loop on init_key  init_key is always decreasing, which ensures
	// termination.  When the nodes in init_key have _wanted_votes, leave
	// the loop.
	do {
		// Check for out-of-possibilities termination condition
		if (init_key == 0)
			break;

		// Check for not-enough-votes termination condition.
		highest = highest_in_key(init_key, num_init_nodes);

		if (qvote_map[highest] < _wanted_votes) {
			init_key = 0;
			init_key_votes = qvote_map[0];
			break;
		}

#ifdef	DEBUG
		old_key = init_key;
#endif

		// set lowest to the lowest set bit
		lowest = lowest_in_key(init_key, num_init_nodes);

		// try removing lowest set bit and see if that results in an
		// excess of votes.  If so, remove it and loop.
		tmp_votes = init_key_votes -
		    (qvote_map[lowest] - qvote_map[lowest - 1]);
		if (tmp_votes > _wanted_votes) {
			mask = 1;
			left_shift(mask, lowest - 1);
			init_key &= ~mask;
			init_key_votes = tmp_votes;
			ASSERT(old_key > init_key);
			continue;
		}

		// If we currently don't have enough votes, and the last 1
		// in the string is set, remove all the lowest 1's so we
		// don't look through all those combinations.  Then loop.
		if ((init_key_votes < _wanted_votes) && (init_key & 1)) {
			lowest = lowest_in_key(~init_key, num_init_nodes);
			mask = 1;
			left_shift(mask, lowest - 1);
			mask -= 1;
			init_key &= ~mask;
			init_key_votes -= qvote_map[lowest - 1] - qvote_map[0];
			ASSERT(old_key > init_key);
			continue;
		}

		// By default, do the safe thing (subtract 1 from
		// init_key and recompute votes).
		init_key--;
		init_key_votes -= qvote_map[lowest] -
		    2 * qvote_map[lowest - 1] + qvote_map[0];
		ASSERT(old_key > init_key);

	} while (init_key_votes != _wanted_votes);

	if (init_key_votes == _wanted_votes) {
		// We have a good combination.  Start by setting ns to the
		// non-initializing nodes.
		ns.set(current_membership);
		if (non_btstrp) {
			ns.remove_nodes(initializing_nodes);
		} else {
			ns.remove_nodes(btstrp_initializing_nodes);
		}
		i = 1;
		decode_key = init_key;
		// Shift a copy of init_key in a loop to determine what nodes
		// are set, and add the set nodes to ns.
		while (decode_key != 0) {
			ASSERT(i <= num_init_nodes);
			if (decode_key & 1) {
				ns.add_node(init_map[i]);
			}
			i++;
			decode_key >>= 1;
		}
	} else {
		ASSERT(init_key == 0);
		ns.empty();
	}
}
