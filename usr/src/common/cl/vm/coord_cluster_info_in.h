/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _COORD_CLUSTER_INFO_IN_H
#define	_COORD_CLUSTER_INFO_IN_H

#pragma ident	"@(#)coord_cluster_info_in.h	1.13	08/05/20 SMI"

#ifdef LIBVM
inline
coord_cluster_info::coord_cluster_info(uint32_t needed_votes) :
	non_btstrp(false),
	current_btstrp_cl_rv_seq(0),
	current_cl_rv_seq(0),
	current_cl_uv_seq(0),
#ifdef	DEBUG
	reinit_since_last_change(false),
#endif
	_total_votes(0),
	_total_ninit_votes(0),
	_needed_votes(needed_votes),
	_wanted_votes(0),
	init_key(0),
	num_init_nodes(0),
	init_map(NULL),
	qvote_map(NULL)
{
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		quorum_votes[n] = 0;
	}
}

#else

inline
coord_cluster_info::coord_cluster_info() :
	non_btstrp(false),
	current_btstrp_cl_rv_seq(0),
	current_cl_rv_seq(0),
	current_cl_uv_seq(0),
#ifdef	DEBUG
	reinit_since_last_change(false),
#endif
	_total_votes(0),
	_total_ninit_votes(0),
	_needed_votes(0),
	_wanted_votes(0),
	init_key(0),
	num_init_nodes(0),
	init_map(NULL),
	qvote_map(NULL)
{
	qa_ref = cmm_ns::get_quorum_algorithm(NULL);
	// If we didn't get a reference, better to panic now while we're
	// initializing than when we're in the cmm steps
	CL_PANIC(!CORBA::is_nil(qa_ref));
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		quorum_votes[n] = 0;
	}
}

#endif  // LIBVM

inline
coord_cluster_info::~coord_cluster_info()
{
#ifndef LIBVM
	CORBA::release(qa_ref);
#endif
	qa_ref = nil;

	delete_maps();
}

inline uint32_t
coord_cluster_info::current_node_votes(nodeid_t n)
{
	ASSERT(n != NODEID_UNKNOWN && n <= NODEID_MAX);

#ifdef LIBVM
	return (_current_node_votes[n]);
#else
	Environment e;
	uint32_t q;


	q = qa_ref->current_node_votes(n, e);
	ASSERT(!e.exception());
	return (q);
#endif
}

inline void
coord_cluster_info::delete_maps()
{
	if (init_map != NULL) {
		delete [] init_map;
		init_map = NULL;
	}

	if (qvote_map != NULL) {
		delete [] qvote_map;
		qvote_map = NULL;
	}
}

inline void
coord_cluster_info::all_current_members(nodeset &ns)
{
	ns.set(current_membership);
}

inline void
coord_cluster_info::all_non_initializing_members(nodeset &ns)
{
	ASSERT(reinit_since_last_change);

	ns.set(current_membership);

	if (non_btstrp) {
		ns.remove_nodes(initializing_nodes);
	} else {
		ns.remove_nodes(btstrp_initializing_nodes);
	}
}

inline uint32_t
coord_cluster_info::total_votes()
{
	ASSERT(reinit_since_last_change);
	return (_total_votes);
}

inline uint32_t
coord_cluster_info::ninit_votes()
{
	ASSERT(reinit_since_last_change);
	return (_total_ninit_votes);
}

inline uint32_t
coord_cluster_info::needed_votes()
{
	ASSERT(_needed_votes != 0);
	return (_needed_votes);
}

inline void
coord_cluster_info::get_btstrp_cl_rv_nodeset(nodeset &ns)
{
	ns.set(has_current_btstrp_cl_rv);
}

inline void
coord_cluster_info::get_cl_rv_nodeset(nodeset &ns)
{
	ns.set(has_current_cl_rv);
}

inline void
coord_cluster_info::get_cl_uv_nodeset(nodeset &ns)
{
	ns.set(has_current_cl_uv);
}

inline cmm::seqnum_t
coord_cluster_info::get_btstrp_cl_rv_seq()
{
	return (current_btstrp_cl_rv_seq);
}

inline void
coord_cluster_info::set_btstrp_cl_rv_seq(cmm::seqnum_t s)
{
	has_current_btstrp_cl_rv.empty();
	current_btstrp_cl_rv_seq = s;
}

inline cmm::seqnum_t
coord_cluster_info::get_cl_rv_seq()
{
	return (current_cl_rv_seq);
}

inline void
coord_cluster_info::set_cl_rv_seq(cmm::seqnum_t s)
{
	has_current_cl_rv.empty();
	current_cl_rv_seq = s;
}

inline cmm::seqnum_t
coord_cluster_info::get_cl_uv_seq()
{
	return (current_cl_uv_seq);
}

inline void
coord_cluster_info::set_cl_uv_seq(cmm::seqnum_t s)
{
	has_current_cl_uv.empty();
	current_cl_uv_seq = s;
}

// uint64_t inconvenience shifting routines
inline void
left_shift(uint64_t &arg, uint8_t amt)
{
	// XXX #ifdef 32/64 bit?
	while (amt > 31) {
		arg <<= 31;
		amt -= 31;
	}
	arg <<= amt;
}

inline void
right_shift(uint64_t &arg, uint8_t amt)
{
	// XXX #ifdef 32/64 bit?
	while (amt > 31) {
		arg >>= 31;
		amt -= 31;
	}
	arg >>= amt;
}

#endif	/* _COORD_CLUSTER_INFO_IN_H */
