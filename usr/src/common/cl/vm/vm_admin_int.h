/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VM_ADMIN_INT_H
#define	_VM_ADMIN_INT_H

#pragma ident	"@(#)vm_admin_int.h	1.3	08/05/20 SMI"

//
// The vm_admin_int object stores, manipulates, and provides access to
// the versioned_protocol objects.  It is the back-end of the
// vm_admin_impl used by clients.
// nameserver.
//

#include <sys/clconf.h>
#include <sys/os.h>
#include <sys/types.h>
#include <vm/versioned_protocol.h>

class vm_stream;
class vp_set_key;

class vm_admin_int {
public:
#ifndef LIBVM
	vm_admin_int();
#else
	vm_admin_int(char **vp_paths, cmm::seqnum_t seq,
	    vm_stream*, vm_stream*);
#endif
	virtual ~vm_admin_int();

	// Interfaces for vm_admin

	virtual void	get_running_version(const version_manager::vp_name_t,
			    version_manager::vp_version_t &, Environment &);
	virtual void	get_nodepair_running_version(
			    const version_manager::vp_name_t,
			    sol::nodeid_t, sol::incarnation_num,
			    version_manager::vp_version_t &, Environment &);
	void	get_vp_list_int(version_manager::vp_info_seq_out);

	// Internal post-construction initialization
	virtual int	initialize_int(vm_stream *&);

	// Generates the bootstrap cluster message. If the first
	// argument is not NULL, and the message is being replaced,
	// the old vm_stream will be deleted.  True is returned if the
	// message has been replaced, false otherwise.
	virtual bool 	new_btstrp_cl_message(vm_stream *&);

	// Generates the non-bootstrap message.  Similar to
	// new_btstrp_cl_message
	virtual bool 	new_cl_message(vm_stream *&);

	// Given a name, returns a pointer to the corresponding
	// versioned_protocol object, or NULL
	versioned_protocol *get_vp_by_name(const char *);

	// Run local consistency on a connected subgroup, as indicated by a
	// vp_set_key
	virtual bool local_consistency(vp_set_key &);

	// Put entries for the versioned_protocols of the specified type
	// into a new vm_stream and return it
	virtual vm_stream *get_stream_by_type(vp_type);

	virtual void set_rvs(vm_stream &, vp_type type,
	    nodeid_t rnode = NODEID_UNKNOWN);

	// XXX temporary until we get this info from cluster.
	void	set_running_version(const version_manager::vp_name_t name,
				    version_manager::vp_version_t &v);

	// Accessor for custom versions stream.
	vm_stream* get_cv_stream();

	// Set/get the change_flag.
	void	set_change_flag(bool B);
	bool	get_change_flag();

protected:

	// The array of vp spec file directory names
	char *vp_dirs[3];

	// The number of vps we parsed
	uint16_t num_vps;

	// The sequence numbers associated with the current running versions.
	// These start as 0 and are set by the coordinator when the versions
	// are set.  We send them whenever we report the current rvs so the
	// coodinator knows when they were computed.
	cmm::seqnum_t		btstrp_cl_rv_seq;
	cmm::seqnum_t		cl_rv_seq;
	cmm::seqnum_t		cl_uv_seq;

	// Custom versions stream. This stream is used only by the LIBVM code.
	// During initialization, and for other operations (set_rvs), vm_admin
	// passes it to versionned protocol objects, that use it to retreive
	// custom running versions.
	vm_stream* custom_stream;

	// Vp list stream. This stream is used only by the LIBVM code.
	// Libvm limit the version check to the intersection of the VPs
	// on the CD and the cluster. This stream contains the list of
	// vp names got from the cluster.
	vm_stream* vplist_stream;

	// List of current vps by type
	IntrList<versioned_protocol, _SList> vp_typelist[VP_TYPE_ARRAY_SIZE];

	// Array of vp pointers sorted by name
	versioned_protocol	**vp_array;

	// Booleans encoding whether messages to the coordinator need to
	// be recomputed.
	bool need_btstrp_cl_message;
	bool need_cl_message;

	//
	// The change_flag is set to true when some vp's running version has
	// been increased by an upgrade commit.
	//
	bool change_flag;

	// Don't copy or assign
	vm_admin_int(const vm_admin_int &);
	vm_admin_int &operator = (vm_admin_int &);
};


#endif /* _VM_ADMIN_INT_H */
