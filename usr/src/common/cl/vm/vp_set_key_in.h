/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_SET_KEY_IN_H
#define	_VP_SET_KEY_IN_H

#pragma ident	"@(#)vp_set_key_in.h	1.11	08/05/20 SMI"

inline
vp_set_key::group_entry::group_entry() :
	vpop(NULL),
	scores(NULL)
{
}

inline
vp_set_key::group_entry::~group_entry()
{
	vpop = NULL;
	all_versions.empty();

	delete [] scores;
	scores = NULL;
}

inline version_prot_obj *
vp_set_key::group_entry::get_vpop()
{
	return (vpop);
}

inline uint32_t
vp_set_key::group_entry::get_score(version_range &vr)
{
	version_manager::vp_version_t vpv;
	int triad_index;

	ASSERT(scores);

	vr.highest_version(vpv.major_num, vpv.minor_num);

	triad_index = all_versions.index_of(vpv);
	ASSERT((triad_index != -1) &&
	    (triad_index < all_versions.get_count()) && scores);

	return (scores[triad_index] + (vpv.minor_num -
	    all_versions.get_triad(triad_index).get_minor_min()));
}

inline void
vp_set_key::group_entry::set_vpop(version_prot_obj *vp)
{
	vpop = vp;
}

inline void
vp_set_key::group_entry::merge_all_versions(version_range &vr)
{
	if (scores != NULL) {
		delete [] scores;
		scores = NULL;
	}

	all_versions.merge_equals(vr);
}

inline void
vp_set_key::group_entry::print_all_versions()
{
	all_versions.dbprint();
}

inline
vp_set_key::node_entry::node_entry() :
	scored_and_sorted(false)
{
}

inline
vp_set_key::node_entry::~node_entry()
{
	solution_universe.dispose();
}

inline IntrList<vp_set, _SList> &
vp_set_key::node_entry::get_solution_universe()
{
	return (solution_universe);
}

inline
vp_set_key::vp_set_key() :
	_num_elems(0),
	group_entries(NULL)
{
}

//
// Destructor
//
inline
vp_set_key::~vp_set_key()
{
	reset(0);
	ASSERT(_num_elems == 0);
	ASSERT(group_entries == NULL);
}

inline void
vp_set_key::reset(uint16_t s)
{
	uint16_t n;

	if (_num_elems != s) {
		if (_num_elems != 0) {
			delete [] group_entries;
		}
		if (s != 0) {
			group_entries = new group_entry[s];
			ASSERT(group_entries);
		} else {
			group_entries = NULL;
		}
		_num_elems = s;
	}
}

inline uint16_t
vp_set_key::num_elems()
{
	return (_num_elems);
}

inline void
vp_set_key::calculate_score(vp_set &set)
{
	uint32_t score = 0;

	ASSERT(group_entries);
	ASSERT(set.is_valid());
	ASSERT(set.get_size() == _num_elems);

	for (uint16_t i = 0; i < _num_elems; i++)
		score += group_entries[i].get_score(set.get_vr(i));

	set.assign_score(score);
}

inline void
vp_set_key::set_vpop(uint16_t i, version_prot_obj *vpop)
{
	ASSERT(i < _num_elems);
	ASSERT(group_entries);
	group_entries[i].set_vpop(vpop);
}

inline void
vp_set_key::merge_all_versions(uint16_t i, version_range &vr)
{
	ASSERT(i < _num_elems);
	ASSERT(group_entries);
	group_entries[i].merge_all_versions(vr);
}

inline IntrList<vp_set, _SList> &
vp_set_key::get_solution_universe(nodeid_t n)
{
	return (node_entries[n].get_solution_universe());
}

inline void
vp_set_key::score_and_sort(nodeid_t n)
{
	node_entries[n].score_and_sort(this);
}

inline versioned_protocol *
vp_set_key::get_vpp(uint16_t i)
{
	ASSERT(i < _num_elems);
	ASSERT(group_entries);
	version_prot_obj *vpop = group_entries[i].get_vpop();
	if (vpop == NULL)
		return (NULL);

	return (vpop->get_vpp());
}

inline coord_vp *
vp_set_key::get_cvpp(uint16_t i)
{
	ASSERT(i < _num_elems);
	ASSERT(group_entries);
	version_prot_obj *vpop = group_entries[i].get_vpop();
	if (vpop == NULL)
		return (NULL);

	return (vpop->get_cvpp());
}

inline version_prot_obj *
vp_set_key::get_vpop(uint16_t i)
{
	ASSERT(i < _num_elems);
	ASSERT(group_entries);
	return (group_entries[i].get_vpop());
}

inline void
vp_set_key::compute_scores()
{
	ASSERT(group_entries);

	for (uint16_t i = 0; i < _num_elems; i++)
		group_entries[i].compute_scores();
}

#endif	/* _VP_SET_KEY_IN_H */
