//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)vp_interface.cc 1.11     08/05/20 SMI"

#include <vm/vp_interface.h>

void *
new_version_range(struct vp_yy_triad *yytriad)
{
	return ((void *) new version_range(yytriad->major_num,
	    yytriad->minor_min, yytriad->minor_max, yytriad->description));
}

void *
new_vp_rhs_version(void *name, void *vr)
{
	ASSERT(name && vr);
	vp_rhs_version *retval = new vp_rhs_version((char *)name,
	    *((version_range *)vr));
	ASSERT(retval);
	delete (version_range *)vr;
	return ((void *)retval);
}

void *
new_vp_dependency(void *rhsv)
{
	vp_dependency *vpd = new vp_dependency();
	if (rhsv != NULL)
		vpd->get_rhs_list().append((vp_rhs_version *)rhsv);
	return ((void *)vpd);
}

void *
new_vp_upgrade(void *lhsv, void *rhsv)
{
	ASSERT(lhsv && rhsv);
	return (void *) new vp_upgrade(*((version_range *)lhsv),
	    *((version_range *)rhsv));
}

void *
new_versioned_protocol()
{
	return ((void *) new versioned_protocol());
}

void
insert_triad(void *vrange_p, struct vp_yy_triad *vy)
{
	(void) ((version_range *)vrange_p)->insert_triad(vy->major_num,
	    vy->minor_min, vy->minor_max, vy->description);
}

void
assign_lhs(void *vpd_p, void *vrange_p)
{
	((vp_dependency *)vpd_p)->swap_into_lhs(*((version_range *)vrange_p));
	delete (version_range *)vrange_p;
}

void
insert_rhs(void *vpd_p, void *rhs_v_p)
{
	((vp_dependency *)vpd_p)->get_rhs_list().
	    append((vp_rhs_version *)rhs_v_p);
}

void
insert_vp_upgrade(void * vpp, void * vpu)
{
	((versioned_protocol *)vpp)->insert_vp_upgrade(*((vp_upgrade *)vpu));
}

void
assign_vp(void *vpp, int ffv, char *name, char *desc, char *src, char *ccb,
    vp_type t, int attr)
{
	((versioned_protocol *)vpp)->assign_vp(ffv, name, desc, src, ccb, t,
	    attr);
}

void
insert_vp_dependency(void *vpp, void *vpd)
{
	((versioned_protocol *)vpp)->
	    insert_vp_dependency(*((vp_dependency *)vpd));
}
