/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_VP_ERROR_H
#define	_VP_ERROR_H

#pragma ident	"@(#)vp_error.h	1.13	08/05/20 SMI"

typedef int vp_error_t;

/*
 * Here are the error codes needed for the Version Manager component needed
 * to support Rolling Upgrade.
 *
 * These error codes will be returned when syntatic and semantic
 * errors are encountered during the parsing of the versioned protocol
 * specification files.
 */
#define	VP_SUCCESS	0	/* Success */
#define	VP_SYNTAX	1
				/*
				 * An unknown line in the vp spec file has been
				 * encountered.
				 */
#define	VP_DUPMAJMIN	2
				/*
				 *  The same major and minor pair of this
				 * versioned protocol has already been
				 * encountered.
				 */
#define	VP_NOTCUSTOM	3
				/*
				 * The custom callback name is specified
				 * but the attribute is not set with "Custom".
				 */
#define	VP_NOCUSTOMCB	4
				/*
				 * The attribute is set with "Custom" but the
				 * custom callback name is not specified.
				 */
#define	VP_NOTBOOTSTRAP	5
				/*
				 * The attribute "Bootstrap" must be set if the
				 * the mode is either "Nodepair" or "Node."
				 */
#define	VP_NOVERSIONS	6
				/*
				 * The versioned protocol file did not contain
				 * any valid major/minor numbers for this
				 * versioned protocol.
				 */
#define	VP_CANNOTOPENDIR	7
				/*
				 * The directory containing the vp spec files
				 * cannot be opened.
				 */
#define	VP_INVALIDUPGRADE	8
				/*
				 * The upgrade line is semantically incorrect
				 * because the lefthand side is not strictly
				 * less than the righthand side of the upgrade.
				 * This error catches the case where the line
				 * tries to upgrade to an earlier version.
				 * (e.g. upgrade from 3.0 to 2.0)
				 */
#define	VP_DUPUPGRADE	9
				/*
				 * The lefthand side of the upgrade line has
				 * already been seen in another upgrade line.
				 */
#define	VP_INVALIDDEP	10
				/*
				 * The versioned_protocol has a dependency
				 * on a different vp that has an inappropriate
				 * type.
				 */
#define	VP_CANNOTOPENFILE 11
				/*
				 * A file is found but cannot be opened.
				 */
#define	VP_DUPNAME	12
				/*
				 * Two vps with the same name were found.
				 */
#define	VP_NOVPFILES	13
				/*
				 * Not a single versioned_protocol was found.
				 */
#define	VP_INCONSISTGRP	14
				/*
				 * A connected subgroup of versioned_protocols
				 * did not have a self-consistent set of
				 * versions.
				 */
#define	VP_NOCUSTOMQ	15
				/*
				 * The query to the custom interface of a
				 * versioned protocol failed.
				 */
#define	VP_TRDATTOOLONG	16
				/*
				 * The size of the btstrp_np data to be sent
				 * to the other nodes exceeds the size of the
				 * transport's capability.
				 */
#define	VP_UNEXPECTED	17
				/*
				 * The catch all unexpected error.
				 */

/* return a specific error string based on the error code */
const char *vp_error_to_string(vp_error_t);

#endif	/* _VP_ERROR_H */
