/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_CONSTRAINT_H
#define	_VP_CONSTRAINT_H

#pragma ident	"@(#)vp_constraint.h	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <vm/vp_set.h>

//
// Version Manager vp_constraint Class Definition
//
// A vp_constraint object holds a single vp_set instance.  This vp_set instance
// contains exactly those versioned protocols that are part of the same
// connected component.  In other words, all versioned protocols that have
// dependencies to each other are represented in this vp_set instance.
// The versioned protocol that has the dependency on it is marked by the
// index field.  The index denotes that versioned protocol's position in the
// array of version_range instances. That versioned protocol's name can be
// obtained using the same index value into the array of versioned protocol
// names.
//
// Rather than having a vp_set member and duplicating all of the accessor
// functions, we just inherit from vp_set.
//

class vp_constraint {
public:
	vp_constraint(uint16_t size);
	~vp_constraint();

	// Return the index to the version protocol with a dependency on it.
	uint16_t get_index();

	// Set the index
	void set_index(uint16_t);

	// Calls vps.empty()
	void empty();

	// Calls vps.get_vr()
	version_range &get_vr(uint16_t);

	// Version of vp_set intersection that takes into account the
	// meaning of an empty version_range in vps (it means the full set
	// of possible versions.
	void intersect_with_vp_set(const vp_set &, vp_set &);

	// Apply the constraint to the vp_set passed in and put the result
	// vp_sets into the list (if any).
	void apply_to_vp_set(vp_set *, IntrList <vp_set, _SList> &);

private:
	int the_index;	// Index into the version_range array to the
			// version protocol that carries a dependency.

	vp_set vps;	// The vp_set used to encode the constraint

	vp_constraint(const vp_constraint &);
	vp_constraint &operator = (vp_constraint &);
};

#ifndef	NOINLINES
#include <vm/vp_constraint_in.h>
#endif	// NOINLINES

#endif	/* _VP_CONSTRAINT_H */
