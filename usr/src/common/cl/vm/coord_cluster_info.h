/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_COORD_CLUSTER_INFO_H
#define	_COORD_CLUSTER_INFO_H

#pragma ident	"@(#)coord_cluster_info.h	1.16	08/05/20 SMI"

#include <sys/nodeset.h>
#include <h/cmm.h>
#include <cmm/cmm_ns.h>

class vm_stream;

//
// The rv_action enum defines a set of flags used to determine the correct
// actions that should be taken when loading running or upgrade versions.
// If the flag set (passed in uint32_t) is empty, no action should be taken
// with the rvs or uvs.  If RV_LOAD is set, the rvs from the stream should
// be copied into the cooresponding coord_vp objects.  If RV_VERIFY is set,
// the code will ASSERT that the existing rvs match the ones in the stream.
// UV_LOAD and UV_VERIFY are analogous.  UV_CLEAR will rezero the upgrade
// version back to 0.0.
//
// At most one RV_ (excluding RV_NONE) and at most one UV_ value may be set.
// (e.g. UV_CLEAR cannot be set when either UV_LOAD or UV_VERIFY is set.)
// (e.g. RV_LOAD cannot be set when RV_VERIFY is set.)
//

enum rv_action {
	RV_NONE = 0,
	RV_LOAD = 1,
	RV_VERIFY = 2,
	UV_LOAD = 4,
	UV_VERIFY = 8,
	UV_CLEAR = 16
};

//
// The coord_cluster_info class stores information about each node that
// isn't stored in the coord_vp objects.  This information includes
//
// o Membership info
// o How many quorum votes each node owns.
// o The sequence numbers associated with the running and upgrade versions
//   in the coord_vps, and which nodes already have those versions.  The
//   sequence numbers come from the CMM sequence number.  The sequence numbers
//   are an indicator when the running and upgrade versions
//   were determined.  Events on other nodes may cause the running and
//   upgrade versions to be recalculated.  When this happens the sequence
//   numbers are increased, and these changes are communicated via the vm_stream
//   messages to the appropriate nodes.
//   The sequence number of running versions are strictly increasing; they are
//   rezeroed only when the entire cluster goes down.  The sequence number of
//   upgrade versions are also strictly increasing but are rezeroed during an
//   upgrade commit when the running version's are set to the upgrade version.
// o A reference to the quorum algorithm
// o Variables derived from this information.
//
// It is used during the btstrp_cl and cl running and upgrade version
// calculations to determine what actions should be taken.
//

class coord_cluster_info {
public:

#ifdef LIBVM
	coord_cluster_info(uint32_t needed_votes);
#else
	coord_cluster_info();
#endif
	~coord_cluster_info();

	// Query the local quorum algorithm for the current number of
	// quorum votes owned by the specified node
	uint32_t	current_node_votes(nodeid_t n);

	// Set the btstrp_cl rv seqnum and the number of quorum votes for node
	void		set_btstrp_node_info(nodeid_t node, vm_stream *vmsp,
			    uint32_t qvotes, uint32_t &);

	// Set the cl rv and uv seqnums for node
	void		set_node_info(nodeid_t node, vm_stream *vmsp,
			    uint32_t &);

	// Recompute private data structures to match the nodes in the
	// callback_membership and the flags passed into the set_node_info
	// functions.  If non_btstrp is false, assume bootstrap, otherwise
	// assume non-bootstrap.
	void		reinitialize(cmm::membership_t &, bool non_btstrp);

	// Sets the nodeset to the set of members passed to reinitialize
	void		all_current_members(nodeset &);

	// Sets the nodeset to the set of members that have initializing set
	// to false, given the membership and mode passed into reinitialize
	void		all_non_initializing_members(nodeset &);

	// Return the total number of votes owned by all members passed into
	// reinitialize
	uint32_t	total_votes();

	// Return the minimum number of votes needed for quorum
	uint32_t	needed_votes();

	// Return the number of votes owned by non-initialzing members
	uint32_t	ninit_votes();

	// Call into the quorum algorithm and verify that the local knowledge
	// of quorum votes matches the votes passed from each node.
	bool		verify_quorum_votes();

	// Prepare to get subsets of nodes with the number of quorum votes
	// specified in the argument
	void		init_node_subset(uint32_t);

	// Set the nodeset to the next subset with the number of quorum votes
	// given in init_node_subset, or empty if there are no more subsets
	// All non-initializing nodes will be included in every subset.
	void		next_subset(nodeset &);

	// These return the set of nodes that have up-to-date rv and uv
	// entries
	void		get_btstrp_cl_rv_nodeset(nodeset &ns);
	void		get_cl_rv_nodeset(nodeset &ns);
	void		get_cl_uv_nodeset(nodeset &ns);

	// These functions get and set the sequence numbers associated with
	// the current rvs and uvs
	cmm::seqnum_t	get_btstrp_cl_rv_seq();
	void		set_btstrp_cl_rv_seq(cmm::seqnum_t);
	cmm::seqnum_t	get_cl_rv_seq();
	void		set_cl_rv_seq(cmm::seqnum_t);
	cmm::seqnum_t	get_cl_uv_seq();
	void		set_cl_uv_seq(cmm::seqnum_t);
private:
	// create or delete the init_map and qvote_map
	void		create_maps();
	void		delete_maps();

	// Return the lowest (or highest) set bit in the uint64_t argument.
	// the second argument specifies the highest possible bit that could
	// be set.
	uint8_t		lowest_in_key(uint64_t, uint8_t);
	uint8_t		highest_in_key(uint64_t, uint8_t);

	// A reference to the quorum algorithm object
	quorum::quorum_algorithm_ptr qa_ref;

	// false if btstrp, true if non-btstrp, set in reinitialize
	bool		non_btstrp;

	// A nodeset of the nodes that had non unknown incns in the
	// callback_membership passed into reinitialize
	nodeset		current_membership;

	// These nodesets record whether a node currently has rvs associated
	// with sequence number 0 (which means they are initializing).
	nodeset		btstrp_initializing_nodes;
	nodeset		initializing_nodes;

	// The nodeset and seqnum associated with btstrp_cl rvs.
	nodeset		has_current_btstrp_cl_rv;
	cmm::seqnum_t	current_btstrp_cl_rv_seq;

	// The nodeset and seqnum associated with cl rvs.
	nodeset		has_current_cl_rv;
	cmm::seqnum_t	current_cl_rv_seq;

	// The nodeset and seqnum associated with cl uvs (upgrade versions)
	nodeset		has_current_cl_uv;
	cmm::seqnum_t	current_cl_uv_seq;

	// The number of quorum votes each node reported in set_btstrp_node_info
	uint32_t	quorum_votes[NODEID_MAX+1];

	// A flag that allows us to ASSERT that certain calls are only made
	// if there has been a reinitialize since the last set_*_node_info
#ifdef	DEBUG
	bool		reinit_since_last_change;
#endif

	// The following are calculated in reinitialize

	// The sum of quorum_votes entries for nodes in current_membership
	uint32_t	_total_votes;
	// The sum of quorum_votes entries for nodes in non_initializing_nodes
	uint32_t	_total_ninit_votes;
	// The number of votes needed for quorum, reported by the quorum alg.
	uint32_t	_needed_votes;

	// The number of votes passed into the last init_node_subset
	uint32_t	_wanted_votes;

#ifdef LIBVM
	uint32_t	_current_node_votes[NODEID_MAX+1];
#endif

	// The a key to thecombination of nodes last passed back from
	// next_subset
	uint64_t	init_key;
	// The number of votes owned by the nodes specified by init_key
	uint32_t	init_key_votes;
	// The number of nodes that are initializing, or the number of nodes
	// specified in current_membership but not non_initializing_nodes
	uint8_t		num_init_nodes;

	// Both maps are size num_init_nodes + 1.

	// A map from init_key to the nodeids of initializing nodes.  Bit
	// n (where the lowest bit is 1) of init_key corresponds to nodeid
	// init_map[n].  The nodes in init_map are sorted from highest to
	// lowest number of quorum votes.  init_map[0] is unused.
	nodeid_t	*init_map;

	// qvote_map[n] will contain the number of votes owned by nodes
	// init_map[1] through init_map[n], as well as all votes of
	// non-initing nodes. So:
	//
	// (qvote_map[n] - qvote_map[n-1]) == quorum_votes[init_map[n]]
	//
	// qvote_map[0] is therefore _total_ninit_votes and is used.
	uint32_t	*qvote_map;

	coord_cluster_info(const coord_cluster_info &);
	coord_cluster_info &operator = (coord_cluster_info &);
};

#ifndef	NOINLINES
#include <vm/coord_cluster_info_in.h>
#endif	// NOINLINES

#endif	/* _COORD_CLUSTER_INFO_H */
