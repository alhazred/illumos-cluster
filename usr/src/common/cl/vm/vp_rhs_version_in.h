/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VP_RHS_VERSION_IN_H
#define	_VP_RHS_VERSION_IN_H

#pragma ident	"@(#)vp_rhs_version_in.h	1.15	08/05/20 SMI"

//
// Constructor - Create a fully specified vp_rhs_version object.
// Assumes n and rhs were created specifically for it.
//
inline
vp_rhs_version::vp_rhs_version(char *n, version_range &rhs) :
	_SList::ListElem(this),
	rhs_vpop(NULL),
	rhs_name(n)
{
	rhs_versions.swap(rhs);
	ASSERT(rhs_name);
}

inline
vp_rhs_version::~vp_rhs_version()
{
	ASSERT(rhs_vpop == NULL);
	if (rhs_name == NULL) {
		delete [] rhs_name;
		rhs_name = NULL;
	}
}

//
// Assign the versioned protocol portion of the right-hand side of the
// dependency and create a reverse dependency in that object to the first
// argument
//
inline void
vp_rhs_version::link(version_prot_obj *vpop_from, version_prot_obj *vpop_to)
{
	if (rhs_name != NULL) {
		delete [] rhs_name;
		rhs_name = NULL;
	}
	rhs_vpop = vpop_to;

	// Add reverse dependency from vpop_from to vpop_to
	vpop_to->add_vp_rev_depend(vpop_from);

	// Add hold for forward dependency to vpop_to.  (Released in unlink())
	vpop_to->hold();
}

//
// Opposite of link, except that we also clear out the string if it never was
//
inline void
vp_rhs_version::unlink(version_prot_obj *vpop_from)
{
	if (rhs_name != NULL) {
		delete [] rhs_name;
		rhs_name = NULL;
	}
	if (rhs_vpop != NULL) {
		// Remove reverse dependency
		rhs_vpop->rm_vp_rev_depend(vpop_from);
		// Remove our hold (taken in link())
		rhs_vpop->rele();
		rhs_vpop = NULL;
	}
}

//
// Return the right-hand side of the dependency.  This can be expressed in a
// single version_range object.
//
inline version_range &
vp_rhs_version::get_vr()
{
	return (rhs_versions);
}

//
// Return the versioned protocol's name that constitutes the right-hand side
// of this dependency.  Note that this will be NULL unless rhs_vpp is NULL,
// because we discard the string.  We can't just helpfully call get_name()
// on rhs_vpp in that case because of the circularity it would cause in the
// include files.
//
inline char *
vp_rhs_version::get_name() const
{
	if (rhs_name)
		return (rhs_name);
	else if (rhs_vpop)
		return (rhs_vpop->get_name());
	else
		return (NULL);
}

inline versioned_protocol *
vp_rhs_version::get_vpp()
{
	if (rhs_vpop == NULL)
		return (NULL);

	return (rhs_vpop->get_vpp());
}

inline coord_vp *
vp_rhs_version::get_cvpp()
{
	if (rhs_vpop == NULL)
		return (NULL);

	return (rhs_vpop->get_cvpp());
}

inline version_prot_obj *
vp_rhs_version::get_vpop()
{
	return (rhs_vpop);
}

#endif	/* _VP_RHS_VERSION_IN_H */
