/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VERSIONED_PROTOCOL_IN_H
#define	_VERSIONED_PROTOCOL_IN_H

#pragma ident	"@(#)versioned_protocol_in.h	1.22	08/05/20 SMI"

//
// Insert the vp_dependency object into the vp_dependlist.  This is not kept
// sorted as it is the parser's responsibility to validate the correctness of
// each dependency.
//
inline void
versioned_protocol::insert_vp_dependency(vp_dependency & d)
{
	vp_dependlist.append(&d);
}

//
// Insert the vp_upgrade object into the vp_upgradelist.
//
inline void
versioned_protocol::insert_vp_upgrade(vp_upgrade & u)
{
	vp_upgradelist.append(&u);
}

//
// XXX Temporary until running versions obtained from cluster will be
// available at the time versioned protocol is initialized.
//
inline void
versioned_protocol::set_running_version_int(version_manager::vp_version_t &v)
{
	running_version[0].major_num = v.major_num;
	running_version[0].minor_num = v.minor_num;
}

inline void
versioned_protocol::set_upgrade_version(version_manager::vp_version_t &v)
{
	ASSERT(the_type == VP_CLUSTER);

	upgrade_version.major_num = v.major_num;
	upgrade_version.minor_num = v.minor_num;
}

inline void
versioned_protocol::get_upgrade_version(version_manager::vp_version_t &v)
{
	// We return the upgrade_version for any vp, but it will be 0.0
	// for vps that aren't VP_CLUSTER, because we never allow the
	// upgrade_version to be set.
	v.major_num = upgrade_version.major_num;
	v.minor_num = upgrade_version.minor_num;
}

//
// Return the versioned protocol's file format version
//
inline uint16_t
versioned_protocol::get_vp_file_format_version()
{
	return (vp_file_version);
}

//
// Return the version protocol object's description
//
inline char *
versioned_protocol::get_vp_description()
{
	return (vp_description);
}

//
// Return the version protocol object's source
//
inline char *
versioned_protocol::get_vp_source()
{
	return (vp_source);
}

//
// Return the version protocol object's custom callback method
//
inline char *
versioned_protocol::get_vp_custom_callback()
{
	return (vp_custom_callback);
}

//
// Return the version protocol object's mode
//
inline vp_type
versioned_protocol::get_vp_type()
{
	return (the_type);
}

//
// Return the version protocol object's attributes
//
inline int
versioned_protocol::get_vp_attributes()
{
	return (vp_attributes);
}

inline version_range &
versioned_protocol::get_potential_versions()
{
	return (potential_versions);
}

inline void
versioned_protocol::reset_consistent_versions(version_range &vr)
{
	version_range tmp_vr;

#ifdef	DEBUG
	// Verify nothing in vr isn't in consistent_versions
	vr.subtraction(consistent_versions, tmp_vr);
	ASSERT(tmp_vr.is_empty());
#endif
	// set consistent_versions to the values in vr;
	consistent_versions.reinit(vr);

	// subtract the new consistent_versions from the old one and
	// place the removed versions into inconsistent_versions.
	vr.subtraction(consistent_versions, tmp_vr);
	inconsistent_versions.merge_equals(tmp_vr);
}

inline IntrList <vp_dependency, _SList> &
versioned_protocol::get_vp_dependency_list()
{
	return (vp_dependlist);
}

inline IntrList <vp_upgrade, _SList> &
versioned_protocol::get_vp_upgrade_list()
{
	return (vp_upgradelist);
}

inline bool
versioned_protocol::is_custom()
{
	return (vp_attributes & VP_CUSTOM);
}

inline void
versioned_protocol::bad_dependency(vp_dependency *vpd)
{
	vpd->mark_bad();

	inconsistent_versions.merge_equals(vpd->get_lhs());
	consistent_versions.subtract_equals(vpd->get_lhs());
}

#endif	/* _VERSIONED_PROTOCOL_IN_H */
