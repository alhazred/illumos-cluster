/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)usymtrace.c	1.16	08/05/20 SMI"

#ifndef linux

#include <ucontext.h>
#include <sys/frame.h>
#include <dlfcn.h>
#include <sys/procfs_isa.h>
#include <stdio.h>
#include <sys/stack.h>
#include <sys/procfs_isa.h>
#include <synch.h>

/* type of demangling function */
typedef int demf_t(const char *, char *, size_t);

#if defined(sparc) || defined(__sparc)
#define	FRAME_PTR_REGISTER REG_SP
#endif

#if defined(i386) || defined(__i386) || defined(__amd64)
#define	FRAME_PTR_REGISTER EBP
#endif

static struct frame *
csgetframeptr()
{
	ucontext_t u;
	struct frame *fp;

	(void) getcontext(&u);

	fp = (struct frame *)
	    ((char *)u.uc_mcontext.gregs[FRAME_PTR_REGISTER] +
	    STACK_BIAS);

	/* make sure to return parents frame pointer.... */

	return ((struct frame *)((ulong_t)fp->fr_savfp + STACK_BIAS));
}


static void
cswalkstack(struct frame *fp, int (*operate_func)(void *, void *, demf_t *),
    void *usrarg, demf_t *demf)
{

	while (fp != NULL && fp->fr_savpc != NULL) {

		if (operate_func((void *)fp->fr_savpc, usrarg, demf) != 0)
			break;
		/*
		 * watch out - libthread stacks look funny at the top
		 * so they may not have their STACK_BIAS set
		 */

		fp = (struct frame *)((ulong_t)fp->fr_savfp +
		    (fp->fr_savfp?(ulong_t)STACK_BIAS:0));
	}
}

static int
csprintaddress(void *pc, void *usrarg, demf_t *demf)
{
	Dl_info info;
	char *func;
	char *lib;
	char buf[1024];

	if (dladdr(pc, & info) == 0) {
		func = "??";
		lib = "??";
	} else {
		lib = (char *)info.dli_fname;
		func = (char *)info.dli_sname;
		if (demf) {
			if ((*demf)(func, buf, sizeof (buf)) == 0)
				func = buf;
		}
	}

	(void) fprintf((FILE *)usrarg,
		"%s:%s+0x%lx\n",
		lib,
		func,
		(uintptr_t)pc - (uintptr_t)info.dli_saddr);

	return (0);
}

void
symtracedump()
{
	const char *libdem = "libdemangle.so.1";
	void *handle;
	static demf_t *demf;
	/*
	 * DEFAULTMUTEX is macro that can be used to initialize mutex to a
	 * default value. However lint does not seem understand this.
	 * From /usr/include/sys/mutex.h:
	 * #define DEFAULTMUTEX    {{{0, 0}, {USYNC_THREAD}, MUTEX_MAGIC}, \
	 *				{{{0, 0, 0, 0, 0, 0, 0, 0}}}, 0}
	 */
	static mutex_t symlck = DEFAULTMUTEX; /*lint !e708 */
	static int tried = 0;

	/* load libdemangle if we can and need to (only try this once) */
	if (!tried) {
		(void) mutex_lock(&symlck);
		if (!tried && !dlopen(libdem, RTLD_NOLOAD)) {
			tried = 1;
			if ((handle = dlopen(libdem, RTLD_LAZY)) != NULL) {
				demf = (demf_t *)dlsym(handle,
				    "cplus_demangle"); /*lint !e611 */
				/*
				 * lint override above is to prevent lint from
				 * complaining about "suspicious cast".
				 */
			}
		}
		(void) mutex_unlock(&symlck);
	}

	cswalkstack(csgetframeptr(), csprintaddress, (void *)stdout, demf);
}

#else /* linux */

#include <assert.h>
#include <linux/unistd.h>

void
symtracedump()
{
	assert(0);
}

_syscall0(int, gettid)

#endif /* linux */
