/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains library functions used for
 * the RBAC-authorization checking for the CLI-tools in SunCluster
 * These functions are placed into libclos.
 * cl_auth.h is its interface.
 */

#pragma ident   "@(#)cl_auth.c 1.7     08/05/20 SMI"

#include <sys/os.h>

#include <stdio.h>
#include <auth_attr.h>
#include <user_attr.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#include <libintl.h>
#include <stdlib.h>
#include <pthread.h>
#include <strings.h>

#include <sys/cl_auth.h>

static int cl_auth_is_initialized = 0;
static uid_t cl_auth_uid;
static uid_t cl_auth_euid;
/*
 * Suppress lint warning about "union initialization"
 * PTHREAD_MUTEX_INITIALIZER is defined in /usr/include/
pthread.h
 */
/*lint -e708 */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
/*lint +e708 */

/*
 * void cl_auth_init()
 *
 * Initializes internal uid/euid copies
 */
void cl_auth_init() {

#ifndef linux
	(void) pthread_mutex_lock(&mutex);

	if (!cl_auth_is_initialized) {

		/* Save our initial UID info */
		cl_auth_uid = getuid();
		cl_auth_euid = geteuid();

		cl_auth_is_initialized = 1;
	}

	(void) pthread_mutex_unlock(&mutex);
#endif
}


/*
 * void cl_auth_demote()
 *
 * sets euid of user to their uid
 */

void cl_auth_demote(void) {

#ifndef linux
	cl_auth_init();

	/* try to demote ourselves back to user privileges */
	(void) seteuid(cl_auth_uid);
#endif
}

/*
 * void cl_auth_promote(void)
 *
 * It restores the EUID privileges to the executable,
 * and sets the UID privileges to the EUID privileges,
 * allowing access (amongst other things) to the
 * ORB. After this function, the command basically
 * can run as "root" if the profile is set up with
 * suid root.
 */

void cl_auth_promote(void) {

#ifndef linux
	cl_auth_init();

	/*
	 * promote this command to the
	 * uid, euid that it needs to get the job done
	 */

	(void) seteuid(cl_auth_euid);
	(void) setuid(cl_auth_euid);
#endif
}

/*
 * This function checks a particular authorization
 * for the user of the current process. It returns
 * 1 if the user is authorized, otherwise 0.
 */
int cl_auth_is_authorized(const char *auth_name) {

#ifdef linux
	/* On linux, no authentication implemented - user always authorized */
	return (1);
#else
	struct passwd *pwd = NULL;
	char *uname = NULL;

	/* make sure we are initialized */
	cl_auth_init();

	/* Get user name */
	pwd = getpwuid(cl_auth_uid);
	uname = pwd -> pw_name;

	/* Check authorization */
	return (chkauthattr(auth_name, uname));
#endif
}

/*
 * This function is used to check pemission
 * for an option of a command. It internally
 * calls cl_auth_is_authorized(). This function
 * exits with the error code passed to it as an
 * argument.
 */
void cl_auth_check_command_opt_exit(const char *command_name,
				    const char *command_option,
				    const char *auth_name,
				    int cl_auth_err) {

	if (!cl_auth_is_authorized(auth_name)) {

		/* I18N */
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

		if (command_option != NULL) {

			if (strlen(command_option) > 2) {

				(void) fprintf(stderr, "%s:  %s %s\n",
				    command_name,
				    dgettext(TEXT_DOMAIN,
					"Not authorized "
					"to use options"),
				    command_option);
			} else {
				(void) fprintf(stderr, "%s:  %s %s\n",
				    command_name,
				    dgettext(TEXT_DOMAIN,
					"Not authorized "
					"to use option"),
				    command_option);
			}

		} else {
			(void) fprintf(stderr, "%s:  %s\n", command_name,
			    dgettext(TEXT_DOMAIN,
				"Not authorized to use this command."));
		}

		exit(cl_auth_err);
	}
}

/*
 * uid_t cl_auth_get_initial_uid(void)
 *
 *     This function returns the initial uid that was
 *     saved before promoting or demoting. Users that
 *     need to know the original uid, should make use
 *     of this method.
 */
uid_t cl_auth_get_initial_uid(void) {
	return (cl_auth_uid);
}
