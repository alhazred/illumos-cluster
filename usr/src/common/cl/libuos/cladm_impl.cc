//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cladm_impl.cc	1.6	08/07/16 SMI"

#include <sys/os.h>
#include <stdarg.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/syslog.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/os_compat.h>
#ifndef linux
#include <door.h>
#include <sys/cladm_int.h>
#endif
#include "cladm_impl.h"

//
// 4829869 Need interposition support for _cladm
//
#ifndef linux
extern "C" int _cladm(int fac, int cmd, void *arg);
#endif

//
// XXX Maximum arg size passed in / expected out.
//
#define	MAXARGS 32 // XXX how big can this get?

extern "C" int
cladm_impl(int fac, int cmd, void *arg)
{
#ifdef linux
	return (-1);
#else
#ifdef DEBUG
	char *unode_dir = getenv("UNODE_DIR");
	char *cluster_name = getenv("CLUSTER_NAME");
	char *node_id = getenv("NODE_ID");
	char unode_path[MAXPATHLEN];
	int unode_gateway_fd;
	int error = 0;
	int args[MAXARGS];
	int *argp = args;
	int *facp = &args[0];
	int *cmdp = &args[1];

	if (node_id == NULL)
#endif
		return (_cladm(fac, cmd, arg));
#ifdef DEBUG
	*facp = fac;
	*cmdp = cmd;

	//
	// Copy input argument into local storage so we can prepend
	// the facility and command in the door invocation.
	//
	if (arg) {
		bcopy(arg, (void *)&args[2],
		    sizeof (args) - (2 * sizeof (int)));
	}

	//
	// We default to /tmp/{cluster}/node-{nodeid}
	//
	sprintf(unode_path, "%s/%s/node-%s/cladm",
	    (unode_dir) ? unode_dir : "/tmp",
	    (cluster_name) ? cluster_name : "ucluster",
	    node_id);

	//
	// Open up the cladm door
	//
	if ((unode_gateway_fd = open(unode_path, O_RDONLY)) < 0) {
		error = errno;
	} else {

		door_arg_t params;
		//
		// XXX A few cladm calls expect that we're in the kernel
		// XXX and can simply copyin/copyout information.  This
		// XXX should be changed, but in the mean time we simply
		// XXX rewrap the arguments and send the strings out.
		//
		struct name_buf {
			int fac;
			int cmd;
			int nodeid;
			int len;
			char name[MAXPATHLEN];
		} name_buf, *name_bufp;

		params.desc_ptr = NULL;
		params.desc_num = 0;

		//
		// XXX Currently CL_GET_NODE_NAME is the only command which
		// we call that requires special treatment.  Getting the
		// cluster name has the same problem and needs to be fixed
		// as well.
		//
		if (fac == CL_CONFIG && cmd == CL_GET_NODE_NAME) {
			clnode_name_t *in_args = (clnode_name_t *)arg;

			//
			// Fill in facility and command parameters
			//
			name_buf.fac = fac;
			name_buf.cmd = cmd;

			//
			// Populate nodeid/len/string
			//
			name_buf.nodeid = in_args->nodeid;
			name_buf.len = MAXPATHLEN;
			bcopy((void *)in_args->name, (void *)name_buf.name,
			    strlen(in_args->name) + 1);

			//
			// Populate params structure
			//
			params.data_ptr = (char *)&name_buf;
			params.data_size = sizeof (name_buf);
			params.rbuf = (char *)&name_buf;
			params.rsize = sizeof (name_buf);
		} else {
			//
			// Use standard arguments array
			//
			params.data_ptr = (char *)&args;
			params.data_size = sizeof (args);
			params.rbuf = (char *)&args;
			params.rsize = sizeof (args);
		}

		//
		// Send the cladm command to unode
		//
		if (door_call(unode_gateway_fd, &params) == -1) {
			error = errno;
			close(unode_gateway_fd);
			goto out;
		}

		//
		// Close up shop until next time
		//
		if (close(unode_gateway_fd) < 0) {
			error = errno;
			goto out;
		}

		//
		// Extract error/exit return valued and results
		//
		facp = &((int *)(params.data_ptr))[0];
		cmdp = &((int *)(params.data_ptr))[1];
		argp = &((int *)(params.data_ptr))[2];

		//
		// errno is passed back in storage for facility.
		// exit code is in command, but effective is returned as -1
		//
		if (*cmdp != 0) {
			error = *facp;
		} else {
			//
			// Door handle passed back in CL_GET_NS_DATA
			// XXX Need to fix this to handle CL_GET_LOCAL_NS
			// XXX Need to generally fix CL_GET_NODE_NAME as well
			//
			if (fac == CL_CONFIG && cmd == CL_GET_NS_DATA) {
				cllocal_ns_data_t *ns_data =
				    (cllocal_ns_data_t *)arg;
				bcopy(&((int *)(params.data_ptr))[2],
				    ns_data->tid, sizeof (ns_data->tid));
				if (params.desc_num == 1) {
					door_info_t info;
					if (door_info(params.desc_ptr->
					    d_data.d_desc.d_descriptor,
					    &info) < 0) {
						error = errno;
						goto out;
					}
					ns_data->filedesc =
					    params.desc_ptr->
					    d_data.d_desc.d_descriptor;
				}
			} else if (fac == CL_CONFIG &&
			    cmd == CL_GET_NODE_NAME) {
				clnode_name_t *out_args = (clnode_name_t *)arg;
				name_bufp = (struct name_buf *)facp;
				out_args->nodeid = name_bufp->nodeid;
				if (name_bufp->len < out_args->len)
					out_args->len = name_bufp->len;
				bcopy((void *)&name_bufp->name,
				    (void *)out_args->name, out_args->len);
			} else {
				bcopy((void *)argp, arg,
				    params.data_size - (2 * sizeof (int)));
			}
		}
	}

out:
	if (error) {
		errno = error;
		return (-1);
	} else {
		return (0);
	}
#endif /* DEBUG */
#endif /* linux */
}
