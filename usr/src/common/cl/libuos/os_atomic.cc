/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)os_atomic.cc	1.11	08/05/20 SMI"

// adapted from atomic.cc, where this is available only in kernel land.

#include <sys/os.h>

/*
 * Standard implementations of various atomic primitives
 * for old platforms that can't do them in hardware.
 *
 * Platforms that implement *all* of these routines
 * should not link with this file (it's all dead code).
 *
 * Platforms that implement some subset of these routines
 * should link with this file to get the rest of them.
 * The emulated versions are all defined with #pragma weak
 * so that the platform's implementation wins if it exists.
 */

os::mutex_t atomic_lock[ATOMIC_HASH_SIZE];


// prototypes for functions defined in this file

extern "C" {

void emul_atomic_add_16(uint16_t *, int16_t);
void emul_atomic_add_32(uint32_t *, int32_t);
void emul_atomic_add_64(uint64_t *, int64_t);
uint16_t emul_atomic_add_16_nv(uint16_t *, int16_t);
uint32_t emul_atomic_add_32_nv(uint32_t *, int32_t);
uint64_t emul_atomic_add_64_nv(uint64_t *, int64_t);
uint32_t emul_cas32(uint32_t *, uint32_t, uint32_t);
uint64_t emul_cas64(uint64_t *, uint64_t, uint64_t);
void *emul_casptr(void **, void *, void *);

};


#pragma weak atomic_add_16 = emul_atomic_add_16
#pragma weak atomic_add_32 = emul_atomic_add_32
#pragma weak atomic_add_64 = emul_atomic_add_64
#pragma weak atomic_add_16_nv = emul_atomic_add_16_nv
#pragma weak atomic_add_32_nv = emul_atomic_add_32_nv
#pragma weak atomic_add_64_nv = emul_atomic_add_64_nv
#pragma weak cas32 = emul_cas32
#pragma weak cas64 = emul_cas64
#pragma weak casptr = emul_casptr

// Disabling lint Info errors for "Loss of sign" in various places

void
emul_atomic_add_16(uint16_t *target, int16_t delta)
{
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	*target += delta;	//lint !e732
	lp->unlock();
}

void
emul_atomic_add_32(uint32_t *target, int32_t delta)
{
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	*target += delta;	//lint !e737
	lp->unlock();
}

void
emul_atomic_add_64(uint64_t *target, int64_t delta)
{
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	*target += delta;	//lint !e737
	lp->unlock();
}

uint16_t
emul_atomic_add_16_nv(uint16_t *target, int16_t delta)
{
	uint16_t new_val;
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	new_val = (*target += delta);	//lint !e732
	lp->unlock();
	return (new_val);
}

uint32_t
emul_atomic_add_32_nv(uint32_t *target, int32_t delta)
{
	uint32_t new_val;
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	new_val = (*target += delta);	//lint !e737
	lp->unlock();
	return (new_val);
}

uint64_t
emul_atomic_add_64_nv(uint64_t *target, int64_t delta)
{
	uint64_t new_val;
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	new_val = (*target += delta);	//lint !e737
	lp->unlock();
	return (new_val);
}

uint32_t
emul_cas32(uint32_t *target, uint32_t cmp, uint32_t new_val)
{
	uint32_t old;
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	if ((old = *target) == cmp)
		*target = new_val;
	lp->unlock();
	return (old);
}

uint64_t
emul_cas64(uint64_t *target, uint64_t cmp, uint64_t new_val)
{
	uint64_t old;
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	if ((old = *target) == cmp)
		*target = new_val;
	lp->unlock();
	return (old);
}

void *
emul_casptr(void **target, void *cmp, void *new_val)
{
	void *old;
	os::mutex_t *lp = ATOMIC_LOCK(target);

	lp->lock();
	if ((old = *target) == cmp)
		*target = new_val;
	lp->unlock();
	return (old);
}
