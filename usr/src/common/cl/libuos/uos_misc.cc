//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)uos_misc.cc	1.82	08/12/17 SMI"

#include <sys/os.h>
#include <stdarg.h>
#include <syslog.h>
#include <time.h>
#include <sys/syslog.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <libgen.h>
#include <libuos/cladm_impl.h>
#include <signal.h>
#include <stdlib.h>
#ifdef linux
#include <rpc/rpc.h>
#include <netdb.h>
#endif

extern "C" int
cladm(int fac, int cmd, void *arg)
{
	return (cladm_impl(fac, cmd, arg));
}

int
os::cladm(int fac, int cmd, void *arg)
{
	return (::cladm(fac, cmd, arg));
}

#ifdef linux
typedef struct signame {
	const char *sigstr;
	const int   signum;
} signame_t;

//
// Mapping for signum and signal
// The following signal names are supported by some versions of Linux.
// These also match with the output from sys_siglist[] which holds
// the signal description on Linux.
//
static signame_t signames[] = {{"HUP", SIGHUP},
				{"INT", SIGINT},
				{"QUIT", SIGQUIT},
				{"ILL", SIGILL},
				{"TRAP", SIGTRAP},
				{"ABRT", SIGABRT},
				{"IOT", SIGIOT},
				{"FPE", SIGFPE},
				{"KILL", SIGKILL},
				{"BUS", SIGBUS},
				{"SEGV", SIGSEGV},
				{"SYS", SIGSYS},
				{"PIPE", SIGPIPE},
				{"ALRM", SIGALRM},
				{"TERM", SIGTERM},
				{"USR1", SIGUSR1},
				{"USR2", SIGUSR2},
				{"CLD", SIGCLD},
				{"CHLD", SIGCHLD},
				{"PWR", SIGPWR},
				{"WINCH", SIGWINCH},
				{"URG", SIGURG},
				{"POLL", SIGPOLL},
				{"IO", SIGPOLL},
				{"STOP", SIGSTOP},
				{"TSTP", SIGTSTP},
				{"CONT", SIGCONT},
				{"TTIN", SIGTTIN},
				{"TTOU", SIGTTOU},
				{"VTALRM", SIGVTALRM},
				{"PROF", SIGPROF},
				{"XCPU", SIGXCPU},
				{"XFSZ", SIGXFSZ},
};

#define	SIGCNT (sizeof (signames) / sizeof (signame_t))

// convert string to long
static int
str2long(const char *p, long *val)
{
	char *q;
	int error;
	int saved_errno = errno;

	errno = 0;
	*val = strtol(p, &q, 10);

	error = ((errno != 0 || q == p || *q != '\0') ? -1 : 0);
	errno = saved_errno;

	return (error);
}
#endif

//
//  str2sig
//  On Solaris, str2sig exists.
//  On Linux, str2sig does not exist and we need to provide it.
//
int os::str2sig(const char *s, int *sigp)
{
#ifndef linux
	return (::str2sig(s, sigp));
#else
	const signame_t *sp;

	if (*s >= '0' && *s <= '9') {
		long val;

		if (str2long(s, &val) == -1) {
			return (-1);
		}

		for (sp = signames; sp < &signames[SIGCNT]; sp++) {
			if (sp->signum == val) {
				*sigp = sp->signum;
				return (0);
			}
		}
		return (-1);
	} else {
		for (sp = signames; sp < &signames[SIGCNT]; sp++) {
			if (strcmp(sp->sigstr, s) == 0) {
				*sigp = sp->signum;
				return (0);
			}
		}
		return (-1);
	}
#endif
}

//
// This file has a couple of lint overrides for Warning 40 when using va_start
// This is because our lint does not understand the compiler builtin variable
// __builtin_va_alist.
//

#define	SC_MESSAGE_LEN (4 * BUFSIZ)

// static function declarations
static sc_syslog_msg_status_t sc_syslog_msg_log_no_args(sc_syslog_msg_handle_t,
				int, sc_event_type_t, const char *, va_list);

static sc_syslog_msg_status_t sc_syslog_msg_set_syslog_tag_log_opts(
    const char *tag, int log_opts);

static sc_syslog_msg_status_t sc_syslog_msg_initialize_log_opts(
    sc_syslog_msg_handle_t *handle, const char *type_specific_tag,
    const char *name, int log_opts);

// Returnt the internet address given a string of a decimal internet addres
// Returned in network byte order
in_addr_t
os::inet_addr(const char *cp)
{
	return (::inet_addr(cp));
}

void
os::printf(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	(void) ::vfprintf(stderr, fmt, adx);
	va_end(adx);
}

int
os::snprintf(char *s, size_t n, const char *fmt, ...)
{
	va_list adx;
	int ret;

	va_start(adx, fmt);		//lint !e40
	ret = ::vsnprintf(s, n, fmt, adx);
	va_end(adx);
	return (ret);
}

void
os::prom_printf(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	(void) ::vfprintf(stderr, fmt, adx);
	va_end(adx);
}

#if defined(linux) && defined(_KERNEL_ORB)
extern os::watchdog_t watchdog;
#endif
void
os::abort()
{
#if !defined(linux) || !defined(_KERNEL_ORB)
	// Provide symbolic stack trace
	::symtracedump();
	// Abort the userland process
	::abort();
#else
	// ORB/linux, we have to panic the node.
	watchdog.panic();
#endif
}

void
os::panic(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	(void) ::fprintf(stderr, "==> PANIC: ");
	(void) ::vfprintf(stderr, fmt, adx);
	(void) ::fprintf(stderr, "\n");
	va_end(adx);

	os::abort();
}

void
os::warning(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	(void) ::fprintf(stderr, "==> WARNING: ");
	(void) ::vfprintf(stderr, fmt, adx);
	(void) ::fprintf(stderr, "\n");
	va_end(adx);
}

int
os::create_dir(char *dirpath)
{
	int retval = 0;
	retval = mkdir(dirpath, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH
	    | S_IXOTH);
	return (retval);
}

int
os::rm_dir(char *dirpath) {

	int retval = 0;
	retval = rmdir(dirpath);
	return (retval);
}

//
// Convenience function to create a file. Return 0 on success.
// Non-zero integer on failure. Creation mode is O_EXCL, so EEXIST is
// returned if the file already exists.
//
int
file_create(char *path)
{
	return (open(path, O_CREAT | O_EXCL, S_IRWXU));
}

int
os::file_rename(char *from_path, char *to_path)
{
	int retval = 0;

	if (rename(from_path, to_path) != 0)
		retval = errno;

	return (retval);
}

int
os::file_unlink(char *filename)
{
	int retval = 0;

	if (unlink(filename) != 0)
		retval = errno;

	return (retval);
}

int
os::file_link(char *from_path, char *to_path)
{
	int retval = 0;

	retval = unlink(to_path);
	if ((retval != 0) && (errno != ENOENT)) {
		return (errno);
	}
	if ((retval = link(from_path, to_path)) != 0) {
		retval = errno;
	}
	return (retval);
}

//
// file_copy
//
// Duplicates file src_file to dest_file
//
#define	FILE_BUFF	40960

int
os::file_copy(char *src_file, char *dest_file)
{
	int	src_fd = 0;
	int	dest_fd = 0;
	int	srccnt, dstcnt;
	int	retval = 0;
	static char file_buff[FILE_BUFF];
	char *ptr;

	if ((src_fd = open(src_file, O_RDONLY))  == -1) {
		retval = errno;
		goto done;
	}

	if ((dest_fd = open(dest_file, O_CREAT|O_WRONLY|O_TRUNC, 0755)) == -1) {
		retval = errno;
		goto done;
	}

	while ((srccnt = read(src_fd, file_buff, FILE_BUFF)) > 0) {
		ptr = file_buff;
		while (srccnt) {
			if ((dstcnt = write(dest_fd, ptr, srccnt)) < 0) {
				retval = errno;
				goto done;
			}
			srccnt -= dstcnt;
			ptr += dstcnt;
		}
	}

done:
	if (src_fd)
		close(src_fd);
	if (dest_fd)
		close(dest_fd);

	return (retval);
}

//
// Convenience function to check if the given file exists
//
bool
os::file_exists(char *path)
{
	if (access(path, F_OK) == 0) {
		return (true);
	} else {
		return (false);
	}
}

void
os::log_syslog(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	syslog(LOG_NOTICE, fmt, adx);
	closelog();
}

void *
os::bsearch(const void *key, const void *base, size_t nel, size_t size,
    int (*compar)(const void *, const void *))
{
	return (::bsearch(key, base, nel, size, compar));
}

void
os::usecsleep(os::usec_t sleeptime)
{
	struct timespec ts;

	os::envchk::check_nonblocking(os::envchk::NB_INTR | os::envchk::NB_INVO,
	    "nanosleep");
	ts.tv_sec = sleeptime / MICROSEC;
	ts.tv_nsec = (sleeptime % MICROSEC) * 1000;

	while (ts.tv_sec > 0 || ts.tv_nsec > 0) {
		int rv = nanosleep(&ts, NULL);
		if (rv == 0)
			return;
	}
}

void
os::systime::setreltime(os::usec_t offset)
{
	struct timeval	now;

	if (gettimeofday(&now, NULL) == -1) {
		os::panic("gettimeofday failed\n");
	}

	offset += now.tv_usec;
	_time.tv_sec = now.tv_sec + (offset / MICROSEC);
	_time.tv_nsec = (offset % MICROSEC) * 1000;
}

bool
os::systime::is_past()
{
	struct timeval	now;

	if (gettimeofday(&now, NULL) == -1) {
		os::panic("gettimeofday failed\n");
	}

	if (_time.tv_sec < now.tv_sec ||
	    ((_time.tv_sec == now.tv_sec) &&
	    (_time.tv_nsec / 1000 < now.tv_usec))) {
		return (true);
	} else return (false);
}

bool
os::systime::is_future()
{
	struct timeval	now;

	if (gettimeofday(&now, NULL) == -1) {
		os::panic("gettimeofday failed\n");
	}

	if (_time.tv_sec > now.tv_sec ||
	    ((_time.tv_sec == now.tv_sec) &&
	    (_time.tv_nsec / 1000 > now.tv_usec))) {
		return (true);
	} else return (false);
}

bool
os::systime::is_before(os::systime &other)
{
	if (_time.tv_sec < other._time.tv_sec)
		return (true);
	else if (_time.tv_sec == other._time.tv_sec)
		return (_time.tv_nsec < other._time.tv_nsec);
	return (false);
}

char *
os::tod::ctime_r(const time_t *clk, char *cbuf, int buflen)
{
#ifdef linux
	return (strncpy(cbuf, ctime(clk), buflen));
#else
	return (::ctime_r(clk, cbuf, buflen));
#endif
}

/*
 * Find highest one bit set.
 *	Returns bit number + 1 of highest bit that is set, otherwise returns 0.
 * High order bit is 31 (or 63 in _LP64 kernel).
 */
int
os::highbit(ulong_t i)
{
	register int h = 1;

	if (i == 0)
		return (0);
#ifdef _LP64
	if (i & 0xffffffff00000000ULL) {
		h += 32; i >>= 32;
	}
#endif
	if (i & 0xffff0000ULL) {
		h += 16; i >>= 16;
	}
	if (i & 0xff00) {
		h += 8; i >>= 8;
	}
	if (i & 0xf0) {
		h += 4; i >>= 4;
	}
	if (i & 0xc) {
		h += 2; i >>= 2;
	}
	if (i & 0x2) {
		h += 1;
	}
	return (h);
}

/*
 * Find lowest one bit set.
 *	Returns bit number + 1 of lowest bit that is set, otherwise returns 0.
 * Low order bit is 0.
 */
int
os::lowbit(ulong_t i)
{
	register int h = 1;

	if (i == 0)
		return (0);

#ifdef _LP64
	if (!(i & 0xffffffff)) {
		h += 32; i >>= 32;
	}
#endif
	if (!(i & 0xffff)) {
		h += 16; i >>= 16;
	}
	if (!(i & 0xff)) {
		h += 8; i >>= 8;
	}
	if (!(i & 0xf)) {
		h += 4; i >>= 4;
	}
	if (!(i & 0x3)) {
		h += 2; i >>= 2;
	}
	if (!(i & 0x1)) {
		h += 1;
	}
	return (h);
}

//
// assertfail - handles ASSERT statement failures.
//	Prints a diagnostic message, dumps the call stack symbolically,
//	and aborts the user program.
//
extern "C" int
assertfail(const char *assert_string, const char *file_name, int line_number)
{
	(void) fprintf(stderr, "Assertion failed: %s,\n\tfile: %s, line: %d\n",
	    assert_string, file_name, line_number);

	//
	// silence complaint from compiler about return value
	//
	if (0)
		return (1);

	/* Provide symbolic stack trace */
	symtracedump();
	abort();
}

#ifdef _KERNEL_ORB
//
// Global definition used for unode.
//
int (*assertfunc)(const char *, const char *, int) = assertfail;
#endif

// os::dupb and allocb would be nice to support in user land and could be
// useful in unode emulation of mblk handling
mblk_t *
os::dupb(mblk_t *, uint_t, os::mem_alloc_type)
{
	ASSERT(!"os::dupb not supported in user land");
	return (NULL);
}

mblk_t *
os::allocb(uint_t, uint_t, os::mem_alloc_type)
{
	ASSERT(!"os::allocb not supported in user land");
	return (NULL);
}

os::sc_syslog_msg::sc_syslog_msg(const char	*resource_type_specific_tag,
				const char	*resource_name,
				void	*)
{
	if (sc_syslog_msg_initialize(&msg_handle, resource_type_specific_tag,
				resource_name)
				!= SC_SYSLOG_MSG_STATUS_GOOD) {
		// XXX what to do?
	}
}

sc_syslog_msg_status_t
os::sc_syslog_msg::log(int	priority,
		sc_event_type_t	event_type,
		const char	*format,
		...)
{
	va_list	ap;
	sc_syslog_msg_status_t status;

	va_start(ap, format);		//lint !e40
	status = sc_syslog_msg_log_no_args(msg_handle, priority, event_type,
				format, ap);
	va_end(ap);

	return (status);
}

os::sc_syslog_msg::~sc_syslog_msg()
{
	sc_syslog_msg_done(&msg_handle);
	msg_handle = NULL;		// for lint
}

/*
 * Following struct and static variables are used by
 * sc_syslog_msg implementation only.
 */
typedef struct {
	char resource_type_tag[BUFSIZ];
	char resource_name[BUFSIZ];
} msg_handle_struct;

// suppress lint warning about union initialization
//lint -save -e708
static pthread_mutex_t sc_syslog_msg_mutex = PTHREAD_MUTEX_INITIALIZER;
//lint -restore
static bool log_is_opened = false;
static bool atexit_is_registered = false;
static char *cached_syslog_tag = NULL;

//
// _sc_syslog_close_atexit
//
// This function is called back when libclos.so is dynamically closed,
// or on process termination for libclos.a. It releases log resources.
//
static void
_sc_syslog_close_atexit(void)
{
	if (log_is_opened) {
		closelog();
	}

	if (cached_syslog_tag != NULL) {
		free(cached_syslog_tag);
		cached_syslog_tag = NULL;
	}
}

//
// _sc_syslog_register_atexit_and_cache_tag
//
// This function registers the atexit callback and caches the syslog
// tag passed as parameter. It returns
//
// true : success.
// false: failure.
//
static bool
_sc_syslog_register_atexit_and_cache_tag(const char *the_tag)
{
	char *tmp_tag = NULL;

	if (!atexit_is_registered) {
		if (atexit(_sc_syslog_close_atexit) != 0) {
			perror("atexit");
			return (false);
		}
		atexit_is_registered = true;
	}

	tmp_tag = strdup(the_tag);
	if (tmp_tag == NULL) {
		perror("strdup");
		return (false);
	}

	if (cached_syslog_tag != NULL) {
		free(cached_syslog_tag);
	}

	cached_syslog_tag = tmp_tag;

	return (true);
}

//
// As a workaround for Bug 4517304, the function sc_syslog_msg_set_syslog_tag
// is replaced by the equivalent function _sc_syslog_msg_set_syslog_tag.
// The intent of the new version is to avoid the possibility of forking by
// syslog(3c) or vsyslog(3c). The original name is bound to the new name by
// a "pragma weak" declaration. The new version is in libclcomm, so that it is
// linked to only by programs that use the ORB.
//
// The common code between the forking and non-forking versions of the function
// is factored out into a new static function
// sc_syslog_msg_set_syslog_tag_log_opts, which takes an
// extra log_opts argument and passes it along to the openlog() call.
//
// The non-forking version (sc_syslog_msg_set_syslog_tag_no_fork) calls
// openlog() with the LOG_CONS flag cleared.
//
// The new functions in libclcomm are defined in sc_syslog_msg.cc
//
// The purpose of the LOG_CONS flag is to request that syslog messages be
// printed to the console in case syslogd dies, isn't started, or has other
// problems. A side effect of this workaround is that no messages will be
// printed to the console in the event of an unhealthy syslogd.
//
static sc_syslog_msg_status_t
sc_syslog_msg_set_syslog_tag_log_opts(const char *tag, int log_opts)
{
	sc_syslog_msg_status_t result = SC_SYSLOG_MSG_STATUS_GOOD;

	if (tag == NULL) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (pthread_mutex_lock(&sc_syslog_msg_mutex) != 0) {
		perror("pthread_mutex_lock");
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (log_is_opened) {
		/*
		 * Close log before re-opening.
		 */
		closelog();
	}
	if (!_sc_syslog_register_atexit_and_cache_tag(tag)) {
		result = SC_SYSLOG_MSG_STATUS_ERROR;
	} else {
		openlog(cached_syslog_tag, log_opts, LOG_DAEMON);
		log_is_opened = true;
	}
	if (pthread_mutex_unlock(&sc_syslog_msg_mutex) != 0) {
		perror("pthread_mutex_unlock");
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	return (result);
}

extern "C" sc_syslog_msg_status_t
_sc_syslog_msg_set_syslog_tag(const char *tag)
{
	return (sc_syslog_msg_set_syslog_tag_log_opts(tag, LOG_CONS));
}

#pragma weak sc_syslog_msg_set_syslog_tag = _sc_syslog_msg_set_syslog_tag

#ifdef BUG_4517304
extern "C" sc_syslog_msg_status_t
sc_syslog_msg_set_syslog_tag_no_fork(const char *tag)
{
	return (sc_syslog_msg_set_syslog_tag_log_opts(tag, 0));
}
#endif /* BUG_4517304 */

//
// As a workaround for Bug 4517304, the function sc_syslog_msg_initialize
// is replaced by the equivalent function _sc_syslog_msg_initialize.
// The intent of the new version is to avoid the possibility of forking by
// syslog(3c) or vsyslog(3c). The original name is bound to the new name by
// a "pragma weak" declaration. The new version is in libclcomm, so that it is
// linked to only by programs that use the ORB.
//
// The common code between the forking and non-forking versions of the function
// is factored out into a new static function sc_syslog_msg_initialize_log_opts,
// which takes an extra log_opts argument and passes it along to the openlog()
// call.
//
// The non-forking version (sc_syslog_msg_initialize_no_fork) calls
// openlog() with the LOG_CONS flag cleared.
//
// The new functions in libclcomm are defined in sc_syslog_msg.cc
//
// The purpose of the LOG_CONS flag is to request that syslog messages be
// printed to the console in case syslogd dies, isn't started, or has other
// problems. A side effect of this workaround is that no messages will be
// printed to the console in the event of an unhealthy syslogd.
//
static sc_syslog_msg_status_t
sc_syslog_msg_initialize_log_opts(
			sc_syslog_msg_handle_t	*handle,
			const char	*type_specific_tag,
			const char	*name,
			int		log_opts)
{
	msg_handle_struct *ihandle = NULL;
	sc_syslog_msg_status_t status = SC_SYSLOG_MSG_STATUS_GOOD;

	*handle = NULL;
	if (type_specific_tag == NULL || name == NULL) {
		goto failure;
	}

	if ((strlen(type_specific_tag) + strlen(name)) >= BUFSIZ) {
		status = SC_SYSLOG_MSG_STATUS_ERROR;
		goto failure;
	}

	ihandle = (msg_handle_struct *)calloc(1UL, sizeof (msg_handle_struct));
	if (ihandle == NULL) {
		status = SC_SYSLOG_MSG_STATUS_ERROR;
		//
		// SCMSGS
		// @explanation
		// There is not enough swap space on the system.
		// @user_action
		// Add more swap space. See swap(1M) for more details.
		//
		syslog(LOG_ERR,
			"Out of memory (memory allocation failed):%s.%s",
			type_specific_tag, name);
		goto failure;
	}

	(void) os::strcpy(ihandle->resource_type_tag, type_specific_tag);
	(void) os::strcpy(ihandle->resource_name, name);

	if (pthread_mutex_lock(&sc_syslog_msg_mutex) != 0) {
		perror("pthread_mutex_lock");
		status = SC_SYSLOG_MSG_STATUS_ERROR;
		goto failure;
	}

	/*
	 * openlog should be called only once.
	 */
	if (!log_is_opened) {
		if (!_sc_syslog_register_atexit_and_cache_tag(
		    type_specific_tag)) {
			status = SC_SYSLOG_MSG_STATUS_ERROR;
		} else {
			openlog(cached_syslog_tag, log_opts, LOG_DAEMON);
			log_is_opened = true;
		}
	}

	if (pthread_mutex_unlock(&sc_syslog_msg_mutex) != 0) {
		perror("pthread_mutex_unlock");
		status = SC_SYSLOG_MSG_STATUS_ERROR;
		goto failure;
	}

	if (status != SC_SYSLOG_MSG_STATUS_ERROR) {
		*handle = (sc_syslog_msg_handle_t)ihandle;
		return (SC_SYSLOG_MSG_STATUS_GOOD);
	}

failure:
	if (ihandle) {
		free(ihandle);
	}
	return (status);
}

extern "C" sc_syslog_msg_status_t
_sc_syslog_msg_initialize(
			sc_syslog_msg_handle_t	*handle,
			const char	*type_specific_tag,
			const char	*name)
{
	return (sc_syslog_msg_initialize_log_opts(handle, type_specific_tag,
	    name, LOG_CONS));
}

#pragma weak sc_syslog_msg_initialize = _sc_syslog_msg_initialize

#ifdef BUG_4517304
extern "C" sc_syslog_msg_status_t
sc_syslog_msg_initialize_no_fork(
			sc_syslog_msg_handle_t	*handle,
			const char	*type_specific_tag,
			const char	*name)
{
	return (sc_syslog_msg_initialize_log_opts(handle, type_specific_tag,
	    name, 0));
}
#endif /* BUG_4517304 */

extern "C" sc_syslog_msg_status_t
sc_syslog_msg_log(
		sc_syslog_msg_handle_t	handle,
		int			priority,
		sc_event_type_t		event_type,
		const char		*format,
		...)
{
	va_list ap;
	sc_syslog_msg_status_t status = SC_SYSLOG_MSG_STATUS_GOOD;

#ifdef linux
	va_start(ap, format);
	vsyslog(priority, format, ap);
#else	/* linux */
	va_start(ap, (void)format);		//lint !e40
	status = sc_syslog_msg_log_no_args(handle, priority, event_type,
						format, ap);
#endif	/* linux */
	va_end(ap);

	return (status);
}

static sc_syslog_msg_status_t
sc_syslog_msg_log_no_args(
		sc_syslog_msg_handle_t	handle,
		int		priority,
		sc_event_type_t	event_type,
		const char	*format,
		va_list		ap)
{
	msg_handle_struct *ihandle;
	char *event_type_str;
	char message[SC_MESSAGE_LEN];
	sc_syslog_msg_status_t status = SC_SYSLOG_MSG_STATUS_GOOD;

	message[0] = '\0';
	ihandle = (msg_handle_struct *)handle;

	/*
	 * buffer "message" should contain information in following form:
	 * ID[<type_tag>.<resource_name>.<event type>]:
	 */

	switch ((int)event_type) {
		case MESSAGE:
			event_type_str = "MESSAGE";
			break;

		case ADDED:
			event_type_str = "ADDED";
			break;

		case REMOVED:
			event_type_str = "REMOVED";
			break;

		case OWNERSHIP_CHANGED:
			event_type_str = "OWNERSHIP_CHANGED";
			break;

		case PROPERTY_CHANGED:
			event_type_str = "PROPERTY_CHANGED";
			break;

		case STATE_CHANGED:
			event_type_str = "STATE_CHANGED";
			break;

		default:
			event_type_str = "MESSAGE";
	}


	// return if NULL or empty format string is passed
	if (!format || format[0] == '\0') {
		return (status);
	}

	// This file is only used for user and unode environments.  In
	// userland, we write to syslog. In unode, we simply send the output to
	// standard out and avoid using the host machine's syslog.

#ifndef _KERNEL_ORB
	vsyslog(priority, format, ap);
#else
	(void) vsnprintf(message, (size_t)SC_MESSAGE_LEN, format, ap);
	if (priority == SC_SYSLOG_PANIC) {
		os::panic("Priority %d: %s\n", priority, message);
	} else {
		(void) printf("Priority %d: %s\n", priority, message);
	}
#endif

	return (status);
} //lint !e550

extern "C" void
sc_syslog_msg_done(sc_syslog_msg_handle_t *handle)
{
	msg_handle_struct *ihandle = (msg_handle_struct *)*handle;
	if (ihandle) {
		free(ihandle);
		*handle = NULL;
	}
}

#ifdef linux
//
// Test if fil is a directory.
// returns 1 if yes, 0 if not
//
static int
isDirectory(const char *fil)
{
	struct stat filestat;
	int ret;

	ret = stat(fil, &filestat);
	if (ret != 0) {
		return (0);
	}
	if (S_ISDIR(filestat.st_mode)) {
		return (1);
	}
	return (0);
}

//
// Implementation of "mkdir -p"
// param path the directory to create
// returns 0 on success, -1 on failure
//
// This is a miminum implementation:
// - filename is not expanded.
// - network paths are not considered.
//
extern "C" int
mkdirp(const char *path, mode_t mode)
{
	int len;
	int pos;
	int ret = 0;
	char *lpath;

	lpath = strdup(path);
	if (lpath == NULL) {
		return (-1);
	}
	len = strlen(lpath);
	pos = 1; /* skip heading '/' */
	while (pos <= len) {
		if ((lpath[pos] == '/') ||
			(pos == len)) {
			lpath[pos] = '\0';
			if (!isDirectory(lpath)) {
				if (mkdir(lpath, mode) != 0)
					ret = -1;
			}
			lpath[pos] = '/';
		}
		pos++;
	}
	free(lpath);
	return (ret);
}

extern "C" hrtime_t
gethrtime()
{
	struct timeval tp;

	(void) gettimeofday(&tp, NULL);
	return ((tp.tv_sec * (hrtime_t)NANOSEC) +
		(tp.tv_usec * (hrtime_t)MILLISEC));
}

extern "C" void
freehostent(struct hostent *hp)
{
	char **p;

	ASSERT(hp != NULL);
	if (hp == NULL) {
		return;
	}

	if (hp->h_addr_list != NULL) {
		for (p = hp->h_addr_list; *p != NULL; p++) {
			if (*p != NULL) {
				free(*p);
			}
		}
		free(hp->h_addr_list);
	}
	if (hp->h_name != NULL) {
		free(hp->h_name);
	}
	if (hp != NULL) {
		free(hp);
	}
}

extern "C" struct hostent *
getipnodebyname(const char *name, int af, int flags, int *error_num)
{
	int count = 0;
	struct addrinfo hints;
	struct addrinfo *res, *ptr;
	struct sockaddr_in *saddr;
	struct hostent *hp;
	char **p;

	memset(&hints, 0, sizeof (hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_flags =  flags;
	*error_num = getaddrinfo(name, NULL, &hints, &res);
	if (*error_num != 0)
		return (NULL);

	hp = (struct hostent *)malloc(sizeof (struct hostent));
	if (hp == NULL) {
		*error_num = ENOMEM;
		return (NULL);
	}
	hp->h_name = (char *)strdup(name);
	hp->h_aliases = NULL;
	hp->h_addrtype = af;
	hp->h_length = (af == AF_INET) ? 4 : 16;

	for (ptr = res; ptr != NULL; ptr = ptr->ai_next) {
		count ++;
	}

	hp->h_addr_list = (char **)malloc(sizeof (char *) * (count + 1));
	if (hp->h_addr_list == NULL) {
		freehostent(hp);
		*error_num = ENOMEM;
		return (NULL);
	}

	p = hp->h_addr_list;
	for (ptr = res; ptr != NULL; ptr = ptr->ai_next) {
		saddr = (struct sockaddr_in *)ptr->ai_addr;
		*p = (char *)malloc(hp->h_length);
		if (*p == NULL) {
			freehostent(hp);
			*error_num = ENOMEM;
			return (NULL);
		}
		if (af == AF_INET) {
			bcopy(&(saddr->sin_addr), *p, hp->h_length);
		} else { /* AF_INET6 */
			if (ptr->ai_family == AF_INET) {
				IN6_INADDR_TO_V4MAPPED(&(saddr->sin_addr),
					(struct in6_addr *)*p);
			} else {
				bcopy(&(saddr->sin_addr), *p, hp->h_length);
			}
		}
		p++;
	}
	*p = NULL;

	freeaddrinfo(res);

	return (hp);
}
#endif

//
// 5018264 Linux: Can't pass NULL param to rpc calls
//
// The Linux RPC code (in some Linux versions) does
// not support NULL pointer in RPC arguments/results.
// As a fix for Bug 5018264, the function xdr_string is replaced by
// the linux_xdr_string define below, to support NULL parameters.
// The .x files have been modified to redefine xdr_string by
// linux_xdr_string using a pre-processor macro
//
#ifdef linux

extern "C" bool_t
linux_xdr_string(XDR *xdrs, char **cpp, uint_t maxsize)
{
	char *sp = *cpp;	/* sp is the actual string pointer */
	uint_t size;
	uint_t nodesize;
	bool_t res;

	/*
	 * first deal with the length since xdr strings are counted-strings
	 */
	switch (xdrs->x_op) {
	case XDR_FREE:
		if (sp == NULL) {
			return (TRUE);		/* already free */
		}
		/* fall through... */
	case XDR_ENCODE:
		size = (sp != NULL) ? strlen(sp) : 0;
		break;
	case XDR_DECODE:
		break;
	}
	if (!xdr_u_int(xdrs, &size)) {
		return (FALSE);
	}
	if (size > maxsize) {
		return (FALSE);
	}
	nodesize = size + 1;
	if (nodesize == 0) {
		/*
		 * This means an overflow.  It a bug in the caller which
		 * provided a too large maxsize but nevertheless catch it
		 * here.
		 */
		return (FALSE);
	}

	/*
	 * now deal with the actual bytes
	 */
	switch (xdrs->x_op) {
	case XDR_DECODE:
		if (sp == NULL) {
			*cpp = sp = (char *)mem_alloc(nodesize);
		}
		if (sp == NULL) {
			(void) fprintf(stderr, "xdr_string: out of memory\n");
			return (FALSE);
		}
		sp[size] = 0;
		/* fall into ... */

	case XDR_ENCODE:
		return (xdr_opaque(xdrs, sp, size));

	case XDR_FREE:
		mem_free(sp, nodesize);
		*cpp = NULL;
		return (TRUE);
	}
	return (FALSE);
}
#endif /* linux */

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <sys/os_in.h>
#include <sys/uos_in.h>
#undef	inline		// in case code is added below that uses inline
#endif	// NOINLINES
