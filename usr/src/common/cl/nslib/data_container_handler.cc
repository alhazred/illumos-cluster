//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)data_container_handler.cc 1.10	08/05/20 SMI"

#include <nslib/data_container_impl.h>


data_container_handler_kit *data_container_handler_kit::
    the_data_container_handler_kit = NULL;

//
// Marshal and unmarshal routines for the data_container_handler
//
CORBA::Object_ptr
data_container_handler_kit::
    unmarshal(service &se, generic_proxy::ProxyCreator, CORBA::TypeId)
{
	// Unmarshal a received object and create a local stub for it.
	return (data_container_impl::unmarshal(se));
}

void
data_container_handler_kit::unmarshal_cancel(service& se)
{
	data_container_impl::unmarshal_cancel(se);
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
data_container_handler_kit::initialize()
{
	ASSERT(the_data_container_handler_kit == NULL);
	the_data_container_handler_kit = new data_container_handler_kit();
	if (the_data_container_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
data_container_handler_kit::shutdown()
{
	if (the_data_container_handler_kit != NULL) {
		delete the_data_container_handler_kit;
		the_data_container_handler_kit = NULL;
	}
}

//
// This method returns reference to data_container_handler_kit instance.
//
data_container_handler_kit&
data_container_handler_kit::the()
{
	ASSERT(the_data_container_handler_kit != NULL);
	return (*the_data_container_handler_kit);
}


//
// data_container_handler methods
//

data_container_handler::~data_container_handler()
{
}

void
data_container_handler::marshal(service& b, CORBA::Object_ptr obj)
{
	// Marshal an object that is sent as an operation argument.
	((data_container_impl *)obj)->marshal(b);
}
