/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NS_TRANS_STATES_H
#define	_NS_TRANS_STATES_H

#pragma ident	"@(#)ns_trans_states.h	1.9	08/05/20 SMI"

#include <h/naming.h>
#include <repl/service/replica_handler.h>
#include <repl/service/transaction_state.h>

class ns_op_state : public transaction_state {
public:
	ns_op_state();

	~ns_op_state();

	//
	// Creates a new "ns_op_state" object and registers
	// it with the ha framework.
	//
	static void register_new_state(Environment &env);

	//
	// If the environment variable indicates a retry operation,
	// then this function true or else it returns false.
	//
	static bool retry(Environment &env);

	//
	// Cleanup routine when the state is unreferenced because
	// both the client and the primary are dead.
	//
	void orphaned(Environment &);

	//
	// Called when the transaction is committed on the primary
	// (i.e., when the primary calls add_commit(), or when the HA
	// framework knows that the invocation has completed on the
	// primary.
	//
	void committed();
};

class ns_new_context_state : public transaction_state {
public:
	ns_new_context_state();

	ns_new_context_state(naming::naming_context_ptr nctx);

	~ns_new_context_state();

	naming::naming_context_ptr get_context();

	static void register_new_state(naming::naming_context_ptr nctx,
	    Environment &env);

	static naming::naming_context_ptr retry(Environment &env);

	void orphaned(Environment &);

	void committed();

private:

	// CORBA ref of the context created on the secondary
	naming::naming_context_ptr ctx;
};

#endif	/* _NS_TRANS_STATES_H */
