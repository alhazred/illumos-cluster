//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)ns_trans_states.cc 1.10	08/05/20 SMI"

#include <nslib/ns_trans_states.h>


ns_op_state::ns_op_state()
{
}

ns_op_state::~ns_op_state()
{
}

void
ns_op_state::register_new_state(Environment &env)
{
	//
	// If this is being invoked while dumping state to a new
	// secondary, don't register any state with the framework.
	//
	if (secondary_ctx::extract_from(env) == NULL)
		return;

	ns_op_state *state = new ns_op_state();
	state->register_state(env);
	env.clear();
}

bool
ns_op_state::retry(Environment &env)
{
	primary_ctx *ctxp = primary_ctx::extract_from(env);
	ns_op_state *state = (ns_op_state *)ctxp->get_saved_state();
	if (state != NULL) {
		// This is a retry.
		return (true);
	}
	return (false);
}

void
ns_op_state::orphaned(Environment &)
{
	// Client died.  Simply return, rollback will be handled by the
	// _unreferenced() call.
}

void
ns_op_state::committed()
{
	// Not used
}



ns_new_context_state::ns_new_context_state(naming::naming_context_ptr nctx)
{
	ctx = nctx; // This is a soft reference
}

ns_new_context_state::~ns_new_context_state()
{
	ctx = NULL;
}

naming::naming_context_ptr
ns_new_context_state::get_context()
{
	return (ctx);
}

void
ns_new_context_state::register_new_state(naming::naming_context_ptr nctx,
    Environment &env)
{
	//
	// If this is being invoked while dumping state to a new
	// secondary, don't register any state with the framework.
	//
	if (secondary_ctx::extract_from(env) == NULL)
		return;

	ns_new_context_state *state = new ns_new_context_state(nctx);
	state->register_state(env);
	env.clear();
}

naming::naming_context_ptr
ns_new_context_state::retry(Environment &env)
{
	naming::naming_context_ptr octx = naming::naming_context::_nil();

	primary_ctx *ctxp = primary_ctx::extract_from(env);
	ns_new_context_state *state =
	    (ns_new_context_state *)ctxp->get_saved_state();
	if (state != NULL) {
		// This is a retry.
		octx = state->get_context();
	}
	return (octx);
}

void
ns_new_context_state::orphaned(Environment &)
{
	// Client died.  Simply return, rollback will be handled by the
	// _unreferenced() call.
}

void
ns_new_context_state::committed()
{
	// Not used
}
