//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)binding_iter_handler.cc 1.7	08/05/20 SMI"

#include <nslib/binding_iter_handler.h>
#include <nslib/binding_iter_impl.h>
#include <nslib/naming_context_impl.h>

binding_iter_handler_kit *binding_iter_handler_kit::
    the_binding_iter_handler_kit = NULL;

//
// binding_iter_handler_kit constructor and destructor.
//
binding_iter_handler_kit::binding_iter_handler_kit() :
    handler_kit(NAMING_ITER_HKID)
{
}

binding_iter_handler_kit::~binding_iter_handler_kit()
{
}

//
// Marshal and unmarshal routines for the binding_iter_handler
//

CORBA::Object_ptr
binding_iter_handler_kit::
    unmarshal(service &se, generic_proxy::ProxyCreator, CORBA::TypeId)
{
	// Unmarshal a received object and create a local stub for it.
	return (binding_iterator_impl::unmarshal(se));
}

void
binding_iter_handler_kit::unmarshal_cancel(service &se)
{
	binding_iterator_impl::unmarshal_cancel(se);
}

//
// This method returns reference to binding_iter_handler_kit instance.
//
binding_iter_handler_kit&
binding_iter_handler_kit::the()
{
	ASSERT(the_binding_iter_handler_kit != NULL);
	return (*the_binding_iter_handler_kit);
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
binding_iter_handler_kit::initialize()
{
	ASSERT(the_binding_iter_handler_kit == NULL);
	the_binding_iter_handler_kit = new binding_iter_handler_kit();
	if (the_binding_iter_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
binding_iter_handler_kit::shutdown()
{
	if (the_binding_iter_handler_kit != NULL) {
		delete the_binding_iter_handler_kit;
		the_binding_iter_handler_kit = NULL;
	}
}

//
// binding_iter_handler methods
//
binding_iter_handler::binding_iter_handler(CORBA::Object_ptr)
{
}


binding_iter_handler::~binding_iter_handler()
{
}

hkit_id_t
binding_iter_handler::handler_id()
{
	return (binding_iter_handler_kit::the().get_hid());
}

void
binding_iter_handler::marshal(service &b, CORBA::Object_ptr obj)
{
	// Marshal an object that is sent as an operation argument
	((binding_iterator_impl *)obj)->marshal(b);
}
