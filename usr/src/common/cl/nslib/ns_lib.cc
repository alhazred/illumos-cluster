//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//
// This file contains the implementation of ns_lib class
// (utility routines for the name server)
//
#pragma ident	"@(#)ns_lib.cc 1.19	08/10/13 SMI"

#ifndef _KERNEL
#include <string.h>
#endif

#include <nslib/ns_lib.h>

dbg_print_buf ns_dbg(0x10000);

//
// Throw the appropriate exception based on the specified error.
//
void
ns_lib::throw_exception(Environment &env, nc_error error)
{
	switch (error) {
	case nc_missing_node:
	{
		naming::not_found *ex = new naming::not_found;
		ex->why = naming::missing_node;
		ex->rest_of_name = (char *)NULL;
		env.exception(ex);
		break;
	}
	case nc_not_context:
	{
		naming::not_found *ex = new naming::not_found;
		ex->why = naming::not_context;
		ex->rest_of_name = (char *)NULL;
		env.exception(ex);
		break;
	}

	case nc_not_object:
	{
		naming::not_found *ex = new naming::not_found;
		ex->why = naming::not_object;
		ex->rest_of_name = (char *)NULL;
		env.exception(ex);
		break;
	}

	case nc_invalid_name:
		env.exception(new naming::invalid_name);
		break;

	case nc_already_bound:
		env.exception(new naming::already_bound);
		break;

	default:
		//
		// Can only get here as a result of a programming error.
		//
		NS_PRINTF(NS_BIND, ("throw_exception: Unknown error: %d\n",
		    error));
		break;
	}
}

// inline
//
// Skip over all slashes ("/") found in the specified string
//
char *
ns_lib::skip_slash(const char *in_str)
{
	char *string = (char *)in_str;

	/* skip over slashes */
	while (*string == '/')
		string++;
	return (string);
}

//
// Return the first component in the specified string
//
char *
ns_lib::get_comp(const char *in_str, int &comp_cnt)
{
	int found_comp = 0;
	char *string = (char *)in_str;

	while (*string != '/' && *string != '\0') {
		found_comp++;
		string++;
	}

	if (found_comp)
		comp_cnt++;
	return (string);
}

//
// Return the number of components (separated by slashes) in the
// specified string
//
int
ns_lib::name_length(const char *in_str)
{
	if ((in_str == NULL) || (*in_str == '\0')) {
		return (0);
	}

	int comp_cnt = 0;
	char *string;

	/* skip over slashes */
	string = skip_slash(in_str);

	string = get_comp(string, comp_cnt);
	while (*string != '\0') {
		string = skip_slash(string);
		string = get_comp(string, comp_cnt);
	}
	return (comp_cnt);
}

//
// Find and return the nth component, specified by "comp_index", in the string
//
char *
ns_lib::get_name_component(const char *in_str, int comp_index)
{
	char *string;
	char *sstring = NULL;
	int comp_cnt = 0;
	size_t comp_size;
	char *ret_comp = NULL;

	/* skip over slashes */
	sstring = skip_slash(in_str);
	string = get_comp(sstring, comp_cnt);
	while (*string != '\0') {
		if ((comp_cnt - 1) == comp_index) {
			break;
		}
		sstring = skip_slash(string);
		string = get_comp(sstring, comp_cnt);
	}
	if ((comp_cnt - 1) == comp_index && string > sstring) {
		comp_size = (size_t)(string - sstring);
		ret_comp = os::strncpy(new char[comp_size + 1],
				sstring, comp_size);
		ret_comp[comp_size] = '\0';
	}
	// XXX - Can ret_comp be NULL?
	return (ret_comp);
}

//
// Returns part of the name ("sub_name").  "start" specifies
// starting "component" and "len" specifies the number of components.
//
char *
ns_lib::sub_name(const char *in_str, int start, int len)
{
	int i;
	char *tmp_str = NULL;
	char *string;

	if (len == 0)
		return (NULL);

	// Allocate space for the component + "/" and the NULL character
	string = new char[os::strlen(in_str) + 2];
	ASSERT(string != NULL);
	string[0] = '\0'; // Required since strcat() appends to the end

	for (i = start; i < start + len; i++) {
		tmp_str = ns_lib::get_name_component(in_str, i);

		// XXX string can be null. We don't have a check for it !
		//lint -e668
		(void) strcat(string, tmp_str);
		(void) strcat(string, "/");
		//lint +e668

		delete tmp_str;
	}

	i = (int)os::strlen(string);
	if (string[i - 1] == '/')
		string[i - 1] = '\0';    // remove last "/"

	return (string);
}

//
// Return the last component in the string
//
char *
ns_lib::get_last_comp(const char *cname)
{
	int length = name_length(cname);
	return (sub_name(cname, length - 1, 1));
}


//
// XXX - DEBUG only
//
char *
ns_lib::error2char(CORBA::Exception *ex)
{
	char *str1;

	str1 = new char[80];

	if (naming::not_found::_exnarrow(ex) != NULL) {
		os::sprintf(str1, " NS: Not_found ");
	} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
		os::sprintf(str1, " NS: Invalid_name ");
	} else if (naming::already_bound::_exnarrow(ex) != NULL) {
		os::sprintf(str1, " NS: Already_bound ");
	} else if (CORBA::SystemException::_exnarrow(ex) != NULL) {
		os::sprintf(str1, " NS: System_error ");
	} else {
		os::sprintf(str1, " NS: U passed wrong Code!!");
	}
	return (str1);
}
