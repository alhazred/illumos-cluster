//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)naming_context_impl.cc	1.38	08/11/24 SMI"

#include <nslib/ns.h>
#include <nslib/naming_context_impl.h>
#include <nslib/binding_iter_impl.h>
#include <sys/rsrc_tag.h>
#include <sys/vm_util.h>

#ifdef _KERNEL_ORB
#include <h/repl_ns.h>
#include <nslib/ns_trans_states.h>
//lint -e766
#include <orb/fault/fault_injection.h>
//lint +e766
#endif

int ns_debug =  NS_BIND_CTX | NS_CKPT | NS_DUMP_STATE;

//
// Global readers/writer lock which protects the private data members
// of the naming context class and held across the checkpoint calls.
//
os::rwlock_t naming_context_ii::ctx_lock;

//
// Lint is throwing message 1776 for all NS_PRINTF
// and print_exception functions. Hence supressing
// it for the whole file.
//
// Message:
// Converting string literals to char * is deprecated
// (Context)  -- A string literal, according to Standard C++
// is typed an array of const char.  This message is issued
// when such a literal is assigned to a non-const pointer.
//

//lint -e1776



//
// Internal implementation constructor
// XXX - impl and not an ptr
//
naming_context_ii::naming_context_ii(naming::naming_context *ncp,
    bool is_replicated, bool lf, uint32_t perm, uint32_t own)
{
	//lint -e1732 -e1733
	b_store = (binding_store *)new volatile_store(ncp);
	//lint +e1732 +e1733
	nctxp = ncp;
	replicated = is_replicated;

	if (replicated) {
		nameserver_type = GLOBAL_NS;
	} else {
		nameserver_type = LOCAL_NS;
	}

	//
	// This value should be set to false only for
	// context 1 (ALL_CONTEXT) of the local and
	// the global name server.
	//
	// This value should be set to false for the real
	// root of the local and global  name server.
	//
	// Sub contexts resciding in the conetxtsof
	// the cz/globalzone should also set this value
	// to false.
	//
	look_further = lf;

	ns_perm = perm;
	owner = own;
}

naming_context_ii::~naming_context_ii()
{
	nctxp = NULL;
	delete b_store; //lint !e1551
}


#ifdef _KERNEL_ORB

// Checkpoint accessor function
// This must be defined here rather than inline because it requires
// the full definition of repl_ns_server. How can we avoid this?
repl_ns::ns_replica_ptr
naming_context_repl_impl::get_checkpoint()
{
	//lint -e1774
	return ((repl_ns_server*)get_provider())->
	    get_checkpoint_repl_ns_ns_replica();
	//lint +e1774
}

//
// Constructor for primary replicated CORBA implementation
//
naming_context_repl_impl::naming_context_repl_impl(repl_ns_server *serverp,
    bool lf, uint32_t perm, uint32_t own) :
    naming_context_ii(this, true, lf, perm, own),
    mc_replica_of<naming::naming_context>(serverp)
{
	//lint -e1506 Actually calling a virtual function in the base class
	// and not the corresponding function in the derived class.
	_handler()->set_cookie((void *)this);
	//lint +e1506
}


//
// Constructor for the secondary replicated CORBA implementation.
//
naming_context_repl_impl::naming_context_repl_impl(
    naming::naming_context_ptr primary_obj,
    bool lf, uint32_t perm, uint32_t own) :
    naming_context_ii(this, true, lf, perm, own),
    mc_replica_of<naming::naming_context>(primary_obj)
{
	//lint -e1506 Actually calling a virtual function in the base class
	// and not the corresponding function in the derived class.
	_handler()->set_cookie((void *)this);
	//lint +e1506
}

void
naming_context_repl_impl::_unreferenced(unref_t arg)
{
	NS_PRINTF(NS_UNREF, ("repl impl unref: %p\n", this));

	naming_context_ii::context_lock();
	if (_last_unref(arg)) {
		//
		// Delete this context if it exists on the unbound
		// context list
		//
		dump_info::delete_unbound_ctx(this);
		delete this;
	}
	naming_context_ii::context_unlock();
}

#endif

//
// This function checks if a client has the permission
// to perform the requested operation.
//
// Parameters:
// 	op_perm:
// 		The Operation permission being requested.
// 		e.g If its a bind operation that the client
// 		is trying to do, in the bind function should
// 		call this function with op_perm set to NS_BIND_PERM
//	op_type:
//		The type of operation. The oparations can be of the
//		following type:
//			NS_NO_OP		No operation
//			NS_READ_OP		Read operation
//			NS_WRITE_OP		Write operation
//			NS_READWRITE_OP		Read and Write operation
//		Currently there is no NS_READWRITE_OP
//	env:
//		The environment variable that is passed as a part
//		of the IDL invocation. This is required in order
//		to determine which zone cluster (local/global)
//		requested the operation.
//
// Return Value:
// 	true:	Has permission to perform the opertaion
// 	false:	Does not have permission to do the operation
//
bool naming_context_ii::has_permission(uint32_t op_perm, uint32_t op_type,
    Environment &env)
{

#if (SOL_VERSION < __s10) || defined(UNODE)
	return (true);
#else
	uint32_t 			clid;
	version_manager::vp_version_t 	ns_version;

	//
	// Check that version 2.0 of the name server software
	// is running. If previous version is running, no
	// need to do security access check.
	//
	ns_version = ns::get_ns_version();
	if (ns_version.major_num < 2) {
		//
		// Older version is running
		// Return true as no security parameters
		// existed in the old version
		//
		return (true);
	}

	clid = env.get_cluster_id();

	if ((clid == owner) && (ns_perm & op_perm)) {
		return (true);
	}

	if ((clid == GLOBAL_ZONEID) && (ns_perm & op_perm)) {
		return (true);
	}

	if ((clid == 2) && (ns_perm & op_perm)) {
		return (true);
	}

	if ((ns_perm & NS_ALLOW_ALL_ZONE_PERM) && (ns_perm & op_perm)) {
		return (true);
	}

	if (((ns_perm & NS_ALLOW_ALL_ZONE_READ_PERM) &&
	    (op_type == NS_READ_OP)) && (ns_perm & op_perm)) {
		return (true);
	}

	// Does not have permission
	return (false);
#endif
} //lint !e1764 !e1762

void
naming_context_impl::_unreferenced(unref_t arg)
{
	NS_PRINTF(NS_UNREF, ("normal impl unref: %p\n", this));

	naming_context_ii::context_lock();
	if (_last_unref(arg)) {
		delete this;
	}
	naming_context_ii::context_unlock();
}

CORBA::Object_ptr
naming_context_impl::resolve(const char *nm, Environment& _env)
{
	ASSERT(nm != NULL);
	return (naming_context_ii::resolve(nm, _env));
}

CORBA::Object_ptr
naming_context_root::resolve(const char *nm, Environment& _env)
{
	ASSERT(nm != NULL);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifdef _KERNEL

	version_manager::vp_version_t ns_version;
	ASSERT(ns::local_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// resolve in the real root of the local name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "gbl_ns"
	//
	ns_version = ns::get_ns_version();
	if ((ns::local_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 			num[30];
		int 			len = 0;
		int			n = os::atoi(nm);
		uint32_t 		clid = _env.get_cluster_id();

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strcmp(nm, "gbl_ns") != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and "gbl_ns"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_root::resolve "
			    "nm = %s len = %d clid = %d env = %p "
			    "Throwing CTX_EACCESS\n",
			    nm, len, clid, &_env));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return (CORBA::Object::_nil());
		}

		//
		// In the real root context of the local nameserver,
		// if some client is trying to look up a zone cluster
		// context, make sure that the client ran from that
		// zone cluster. All clients will be allowed to lookup
		// the ALL_CLUSTER (c1) context.
		//

		if ((len == 0) && (clid != GLOBAL_ZONEID) && (clid != 2) &&
		    (n != 1) && (n != (int)clid)) {
			//
			// len == 0 means the string is made up of only
			// numbers. The client is trying to resolve a
			// virtual context.
			// zid != GLOBAL_ZONEID means that the client is
			// not running in the global zone.
			// clid != 2 means that the client is not running
			// from a native local zone.
			// n != 1 means that the client is not trying to
			// resolve the ALL_CLUSTER context (c1)
			// n != clid means that the client is not trying
			// to resolve its own zone cluster context.
			// Effectively, the client is trying to access
			// some other context.
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_root::resolve "
			    "nm = %s len = %d clid = %d n = %d env = %p "
			    "Throwing NS_EPERM\n",
			    nm, len, clid, n, &_env));
			_env.system_exception(
			    CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
			return (CORBA::Object::_nil());

		} else if ((len == 0) && (clid == 2) && (n != 0) && (n != 1)) {
			//
			// len == 0 means the string is made up of only
			// numbers. The client is trying to resolve a
			// virtual context.
			// clid == 2 means it is a native local zone. Native
			// local zones can access only context 0 and 1. They
			// can not access the contexts of zone clusters.
			// n != 0 means that the client is not trying to
			// resolve the ALL_CLUSTER context (c0)
			// n != 1 means that the client is not trying to
			// resolve the ALL_CLUSTER context (c1)
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_root::resolve "
			    "native local zone nm = %s len = %d "
			    "clid = %d n = %d  env = %p "
			    "Throwing NS_EPERM\n",
			    nm, len, clid, n, &_env));
			_env.system_exception(
			    CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
			return (CORBA::Object::_nil());
		}
	}
#endif
#endif
	return (naming_context_ii::resolve(nm, _env));
}

void
naming_context_ii::set_nameserver_type(int ns_type)
{
	nameserver_type = ns_type;
}

int
naming_context_ii::get_nameserver_type()
{
	return (nameserver_type);
} //lint !e1762

void
naming_context_ii::set_look_further(bool status)
{
	look_further = status;
}

bool
naming_context_ii::get_look_further()
{
	return (look_further);
} //lint !e1762

#ifdef _KERNEL_ORB

//
// Initialize the name space by creating default contexts
// Defined only for _KERNEL_ORB
//
void
naming_context_ii::init_namespace(naming_context_ii *root_ctx,
    const int ns_type, Environment &env)
{
	naming::naming_context_var sctx;
	CORBA::Object_ptr nsobj = nil;

	sctx = root_ctx->bind_new_context("services", env);
	if (env.exception()) {
		env.exception()->print_exception("name_server failed to "
		    "create services context\n");
		env.clear();
	}

	//
	// Need to initializa the contexts for the local nameserver
	// only. For the root/global name server, no need to initialize
	// the context here
	//
	if (ns_type == LOCAL_NS) {
		//
		// Objects bound in this context will be available to
		// the global zone (physical cluster) only
		//
		nsobj = root_ctx->bind_new_context_v1("0", true,
		    NS_ALLOW_ALL_OPP_PERM, GLOBAL_ZONEID, env);
		if (env.exception() || CORBA::is_nil(nsobj)) {
			env.exception()->print_exception(
			    "name_server failed to create 0 context\n");
			env.clear();
			CORBA::release(nsobj);
		} else {
			ns::local_nmsrvr_c0 =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
			NS_PRINTF(NS_DUMP_ALL, ("init_nameserver: "
			    "ns::local_nmsrvr_c0 = %p CORBA\n",
			    ns::local_nmsrvr_c0));
		}

		//
		// Objects bound in this context will be available to
		// all zones (local and global).
		//
		nsobj = root_ctx->bind_new_context_v1("1", false,
		    NS_ALLOW_ALL_OPP_PERM | NS_ALLOW_ALL_ZONE_READ_PERM,
		    GLOBAL_ZONEID, env);
		if (env.exception() || CORBA::is_nil(nsobj)) {
			env.exception()->print_exception(
			    "name_server failed to create  1 context\n");
			env.clear();
			CORBA::release(nsobj);
		} else {
			ns::local_nmsrvr_c1 =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
			NS_PRINTF(NS_DUMP_ALL, ("init_nameserver: "
			    "ns::local_nmsrvr_c1 = %p CORBA\n",
			    ns::local_nmsrvr_c1));
		}
	} // End of if (ns_type == LOCAL_NS)

	if (ns_type == GLOBAL_NS) {

		version_manager::vp_version_t ns_version;

		ns_version = ns::get_ns_version();

		if (ns_version.major_num < 2) {
			// Old version is running.
			NS_PRINTF(NS_DUMP_ALL, ("init_namespace: "
			    "old version running ctx_0\n"));
			//
			// Objects bound in this context will be available to
			// the global zone (physical cluster) only
			//
			nsobj = root_ctx->bind_new_context("0", env);

		} else {
			// New version is running.
			NS_PRINTF(NS_DUMP_ALL, ("init_namespace: "
			    "new version running ctx_0\n"));
			//
			// Objects bound in this context will be available to
			// the global zone (physical cluster) only
			//
			nsobj = root_ctx->bind_new_context_v1("0", true,
			    NS_ALLOW_ALL_OPP_PERM, GLOBAL_ZONEID, env);
		}

		if (env.exception() || CORBA::is_nil(nsobj)) {
			env.exception()->print_exception(
			    "name_server failed to create 0 global context\n");
			env.clear();
			CORBA::release(nsobj);
		} else {
			ns::global_nmsrvr_c0 =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
		}

		if (ns_version.major_num < 2) {
			// Old version is running.
			NS_PRINTF(NS_DUMP_ALL, ("init_namespace: "
			    "old version running ctx_1\n"));
			//
			// Objects bound in this context will be available to
			// all clusters.
			//
			nsobj = root_ctx->bind_new_context("1", env);
		} else {
			// New version is running.
			NS_PRINTF(NS_DUMP_ALL, ("init_namespace: "
			    " new version running ctx_1\n"));
			//
			// Objects bound in this context will be available to
			// all clusters.
			//
			nsobj = root_ctx->bind_new_context_v1("1", false,
			    NS_ALLOW_ALL_OPP_PERM | NS_ALLOW_ALL_ZONE_READ_PERM,
			    GLOBAL_ZONEID, env);
		}

		if (env.exception() || CORBA::is_nil(nsobj)) {
			env.exception()->print_exception(
			    "name_server failed to create  1 global context\n");
			env.clear();
			CORBA::release(nsobj);
		} else {
			ns::global_nmsrvr_c1 =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
		}
	} // End of if (ns_type == GOBAL_NS)
}

//
// This function is patterend on naming_context_ii::init_namespace()
// function. This should be used only during RU scenario in
// repl_ns_server::become_primary()
//
// Call this function from become_primary only when old version
// of nameserver is running.
//
// This function has the ability to run also under new software.
// This is added keeping the future in mind.
//
void
naming_context_ii::create_virtual_root(naming_context_ii *root_ctx,
    const int ns_type, Environment &env)
{
	CORBA::Exception *ex;
	CORBA::Object_ptr nsobj = CORBA::Object::_nil();
	version_manager::vp_version_t ns_version;

	if (ns_type == GLOBAL_NS) {
		ns_version = ns::get_ns_version();
		if (ns_version.major_num < 2) {
			// Old version is running.
			NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
			    "old version running ctx_0\n"));
			//
			// Objects bound in this context will be available to
			// the global zone (physical cluster) only
			//
			nsobj = root_ctx->bind_new_context("0", env);

		} else {
			// New version is running.
			NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
			    "new version running ctx_0\n"));
			//
			// Objects bound in this context will be available to
			// the global zone (physical cluster) only
			//
			nsobj = root_ctx->bind_new_context_v1("0", true,
			    NS_ALLOW_ALL_OPP_PERM, GLOBAL_ZONEID, env);
		}
		if (((ex = env.exception()) != NULL) ||
		    (CORBA::is_nil(nsobj))) {
			if (naming::already_bound::_exnarrow(ex)) {
				//
				// This is safe to ignore. It just means
				// that context 0 already exists and hence
				// no need to recreate it.
				//
				NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
				    "Exception: already_bound "
				    "global context 0 - Ignore\n"));
				env.exception()->print_exception(
				    "create_virtual_root: "
				    "Exception: already_bound "
				    "global context 0 - Ignore\n");
			} else {
				NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
				    "name_server failed to create 0 "
				    "global context\n"));
				env.exception()->print_exception(
				    "create_virtual_root: "
				    "name_server failed to create 0 "
				    "global context\n");
				CL_PANIC(0);
			}
			env.clear();
			if (!CORBA::is_nil(nsobj)) {
				CORBA::release(nsobj);
			}
		} else {
			ns::global_nmsrvr_c0 =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
		}

		if (ns_version.major_num < 2) {
			// Old version is running.
			NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
			    "old version running ctx_1\n"));
			//
			// Objects bound in this context will be available to
			// all clusters.
			//
			nsobj = root_ctx->bind_new_context("1", env);
		} else {
			// New version is running.
			NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
			    "new version running ctx_1\n"));
			//
			// Objects bound in this context will be available to
			// all clusters.
			//
			nsobj = root_ctx->bind_new_context_v1("1", false,
			    NS_ALLOW_ALL_OPP_PERM | NS_ALLOW_ALL_ZONE_READ_PERM,
			    GLOBAL_ZONEID, env);
		}

		if (((ex = env.exception()) != NULL) ||
		    (CORBA::is_nil(nsobj))) {
			if (naming::already_bound::_exnarrow(ex)) {
				//
				// This is safe to ignore. It just means
				// that context 0 already exists and hence
				// no need to recreate it.
				//
				NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
				    "Exception: already_bound global context "
				    "1 - Ignore\n"));
				env.exception()->print_exception(
				    "create_virtual_root: "
				    "Exception: already_bound global context "
				    "1 - Ignore\n");
			} else {
				NS_PRINTF(NS_DUMP_ALL, ("create_virtual_root: "
				    "name_server failed to create 1 "
				    "global context\n"));
				env.exception()->print_exception(
				    "create_virtual_root: "
				    "name_server failed to create 1 "
				    "global context\n");
				CL_PANIC(0);
			}
			env.clear();
			if (!CORBA::is_nil(nsobj)) {
				CORBA::release(nsobj);
			}
		} else {
			ns::global_nmsrvr_c1 =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
		}
	} // End of if (ns_type == GOBAL_NS)
}

#endif

//
// The lookup routine iterates through each component in the specified
// pathname except possibly the last one based on the value of
// "process_last_comp".  Each component in the pathname can represent a local
// naming context (implemented by this name server), remote naming
// context (implemented by some other name server) or an object.
// The lookup proceeds as long as each component represents a local
// naming context.
//
// The binding associated with each component is retrieved and used
// to search the next component.  The binding returned to the caller
// may contain an object, local context or a remote context.
// For remote contexts, the part of the pathname that is yet to be
// looked up is returned in "rest_of_name"
//

nc_error
naming_context_ii::lookup(const char *nm, int process_last_comp,
    binding *&ret_binding, char *&rest_of_name)
{
	int i, last_component, length;
	char *comp;

	length = ns_lib::name_length(nm);
	if (length == 0)
		return (nc_invalid_name);

	last_component = length - 1;
	if (process_last_comp)
		last_component++;

	binding *store_binding = NULL;
	binding_store *bstore = b_store;

	// Lookup each component
	for (i = 0; i < last_component; i++) {
		binding *child_binding;

		comp = ns_lib::get_name_component(nm, i);
		NS_PRINTF(NS_LOOKUP, ("lookup: comp: %s\n", comp));

		//
		// Retrieve binding associated with component
		//
		ctx_lock.wrlock();
		child_binding = bstore->retrieve_binding(comp);
		ctx_lock.unlock();

		delete [] comp;
		if (child_binding == NULL) {
			if (store_binding != NULL)
				store_binding->release();
			NS_PRINTF(NS_LOOKUP, ("lookup: name not found\n"));
			return (nc_missing_node);
		}

		//
		// Obtain the binding_type
		//
		switch (child_binding->get_binding_type()) {
		case binding::bt_local_ctx:
			//
			// The binding represents a local naming context,
			// so obtain a pointer to the binding store,
			// reset child_binding and continue.
			//
			if (store_binding != NULL) {
				store_binding->release();
			}
			bstore = child_binding->get_store_obj();
			store_binding = child_binding;
			NS_PRINTF(NS_LOOKUP, ("lookup: local context\n"));
			break;

		case binding::bt_remote_ctx:
			//
			// The binding represents a remote context, we
			// cannot proceed further, return the binding
			//
			ret_binding = child_binding;
			rest_of_name =
			    ns_lib::sub_name(nm, i + 1, (length - (i + 1)));

			NS_PRINTF(NS_LOOKUP, ("lookup: remote context\n"));
			if (store_binding != NULL)
				store_binding->release();
			return (nc_ok);

		case binding::bt_obj:
			//
			// This is an object (leaf), not a context and hence
			// we cannot proceed further so release parent binding.
			//
			if (store_binding != NULL) {
				store_binding->release();
			}

			//
			// Return the binding if this is the last component
			// or else return an error.
			//
			NS_PRINTF(NS_LOOKUP,
			    ("lookup: object i: %d, length: %d\n", i, length));

			if (i == (length - 1)) {
				store_binding = child_binding;
			} else {
				return (nc_not_context);
			}
			break;

		default:
			//
			// Should never happen.
			//
			{
			os::sc_syslog_msg syslog_msg(
			    SC_SYSLOG_NAMING_SERVICE_TAG, nm, NULL);
			//
			// SCMSGS
			// @explanation
			// During a name server lookup an unknown binding type
			// was encountered.
			// @user_action
			// No action required. This is informational message.
			//
			(void) syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "lookup: unknown binding type <%d>",
			    child_binding->get_binding_type());
			}
			return (nc_cannot_proceed);
		}
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_LOOKUP, FaultFunctions::generic);
#endif

	ret_binding = store_binding;
	rest_of_name = NULL;
	return (nc_ok);
}

nc_error
naming_context_ii::lookup_local_c1(const char *nm, int process_last_comp,
    binding *&ret_binding, char *&rest_of_name)
{
	int i, last_component, length;
	char *comp;

	length = ns_lib::name_length(nm);
	if (length == 0)
		return (nc_invalid_name);

	last_component = length - 1;
	if (process_last_comp)
		last_component++;

	binding *store_binding = NULL;

#ifdef _KERNEL_ORB
	binding_store *bstore = ns::b_store_local_c1;
#else
	binding_store *bstore = b_store;
#endif
	// Lookup each component
	for (i = 0; i < last_component; i++) {
		binding *child_binding;

		comp = ns_lib::get_name_component(nm, i);
		NS_PRINTF(NS_LOOKUP, ("lookup: comp: %s\n", comp));

		//
		// Retrieve binding associated with component
		//
		ctx_lock.wrlock();
		child_binding = bstore->retrieve_binding(comp);
		ctx_lock.unlock();

		delete [] comp;
		if (child_binding == NULL) {
			if (store_binding != NULL)
				store_binding->release();
			NS_PRINTF(NS_LOOKUP, ("lookup: name not found\n"));
			return (nc_missing_node);
		}

		//
		// Obtain the binding_type
		//
		switch (child_binding->get_binding_type()) {
		case binding::bt_local_ctx:
			//
			// The binding represents a local naming context,
			// so obtain a pointer to the binding store,
			// reset child_binding and continue.
			//
			if (store_binding != NULL) {
				store_binding->release();
			}
			bstore = child_binding->get_store_obj();
			store_binding = child_binding;
			NS_PRINTF(NS_LOOKUP, ("lookup: local context\n"));
			break;

		case binding::bt_remote_ctx:
			//
			// The binding represents a remote context, we
			// cannot proceed further, return the binding
			//
			ret_binding = child_binding;
			rest_of_name =
			    ns_lib::sub_name(nm, i + 1, (length - (i + 1)));

			NS_PRINTF(NS_LOOKUP, ("lookup: remote context\n"));
			if (store_binding != NULL)
				store_binding->release();
			return (nc_ok);

		case binding::bt_obj:
			//
			// This is an object (leaf), not a context and hence
			// we cannot proceed further so release parent binding.
			//
			if (store_binding != NULL) {
				store_binding->release();
			}

			//
			// Return the binding if this is the last component
			// or else return an error.
			//
			NS_PRINTF(NS_LOOKUP,
			    ("lookup: object i: %d, length: %d\n", i, length));

			if (i == (length - 1)) {
				store_binding = child_binding;
			} else {
				return (nc_not_context);
			}
			break;

		default:
			//
			// Should never happen.
			//
			{
			os::sc_syslog_msg syslog_msg(
			    SC_SYSLOG_NAMING_SERVICE_TAG, nm, NULL);
			//
			// SCMSGS
			// @explanation
			// During a name server lookup an unknown binding type
			// was encountered.
			// @user_action
			// No action required. This is informational message.
			//
			(void) syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "lookup_c1: unknown binding type <%d>",
			    child_binding->get_binding_type());
			}
			return (nc_cannot_proceed);
		}
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_LOOKUP, FaultFunctions::generic);
#endif

	ret_binding = store_binding;
	rest_of_name = NULL;
	return (nc_ok);
} //lint !e1762

//
// Determine context type
//

binding::binding_type
naming_context_ii::get_context_type(naming::naming_context_ptr obj)
{
	if (obj->_handler()->is_local() && obj->_handler()->get_cookie()) {
		return (binding::bt_local_ctx);
	} else {
		return (binding::bt_remote_ctx);
	}
} //lint !e1762

//
// bind - Associate the object with the given name in this context
//
void
naming_context_ii::bind(const char *nm, CORBA::Object_ptr obj,
    Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_BIND, ("bind: name = %s\n", nm));

#ifdef _KERNEL_ORB
	// This is a debug facility
	if ((os::strcmp(nm, "ns_debug_obj") == 0) &&
	    (!CORBA::is_nil(ns::global_nmsrvr))) {
		ns::print_global_nameserver(ns::global_nmsrvr_obj, "root", 0);
	}
#endif

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif

	// Check the nameserver security parameters
	if (!has_permission(NS_BIND_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, ("naming_context_ii::bind: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return;
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		NS_PRINTF(NS_BIND, ("bind: name = %s error = %d\n", nm, error));
		ns_lib::throw_exception(_env, error);
		return;
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_BIND, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			NS_PRINTF(NS_BIND, ("bind: NULL binding\n"));
			bstore = b_store;
		} else {
			NS_PRINTF(NS_BIND, ("bind: LOCAL context\n"));
			bstore = a_binding->get_store_obj();
		}
		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_BIND_BEFORE_STORE_BINDING,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->store_binding(binding_nm,
		    binding::bt_obj, obj, false);
		if (error != nc_ok) {
			ctx_lock.unlock();
			NS_PRINTF(NS_BIND, ("error = %d name = %s\n",
			    error, binding_nm));
			delete [] binding_nm;
			ns_lib::throw_exception(_env, error);
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774
				get_ckpt()->ckpt_binding(lctx, binding_nm, obj,
				    binding::bt_obj, true, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_BIND_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		NS_PRINTF(NS_BIND, ("bind: Non NULL (REMOTE) binding\n"));
		//
		// The binding represents a remote context, so forward
		// the bind call to the appropriate name server.
		//
		naming::naming_context_ptr rctxp;
		Environment e;
		CORBA::Exception *ex;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());

		NS_PRINTF(NS_BIND, ("bind: rest of name :%s:\n", rest_of_name));
		rctxp->bind(rest_of_name, obj, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL)
		a_binding->release();
	if (rest_of_name != NULL)
		delete [] rest_of_name;
} 

//
// rebind - Associate the object with the given name in this context,
//	    overriding the previous binding, if any.
//
void
naming_context_ii::rebind(const char *nm, CORBA::Object_ptr obj,
    Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_REBIND, ("rebind: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif

	// Check the nameserver security parameters
	if (!has_permission(NS_REBIND_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, ("naming_context_ii::rebind: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return;
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		NS_PRINTF(NS_REBIND, ("rebind: lookup failed\n"));
		return;
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_REBIND, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_REBIND_BEFORE_STORE_BINDING,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->store_binding(binding_nm,
				binding::bt_obj, obj, true);
		if (error != nc_ok) {
			ctx_lock.unlock();
			delete [] binding_nm;
			ns_lib::throw_exception(_env, error);
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_binding(lctx, binding_nm, obj,
				    binding::bt_obj, true, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_REBIND_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		//
		// The binding represents a remote context, so forward
		// the bind call to the appropriate name server.
		//
		naming::naming_context_ptr rctxp;
		Environment e;
		CORBA::Exception *ex;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());
		NS_PRINTF(NS_REBIND,
			("rebind: rest of name :%s:\n", rest_of_name));

		rctxp->rebind(rest_of_name, obj, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL)
		a_binding->release();
	if (rest_of_name != NULL)
		delete [] rest_of_name;
}


void
naming_context_ii::bind_context(const char *nm, naming::naming_context_ptr obj,
    Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_BIND_CTX, ("bind_ctx: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_BIND_CTX_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, (
		    "naming_context_ii::bind_context: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return;
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return;
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_BIND_CTX, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		binding_nm = ns_lib::get_last_comp(nm);

		// Determine if this is a local or remote context
		binding::binding_type btype = get_context_type(obj);
		NS_PRINTF(NS_BIND_CTX, ("bind_ctx: btype = %d\n", btype));

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_BIND_CTX_BEFORE_STORE_BINDING,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->store_binding(binding_nm, btype, obj, false);
		if (error != nc_ok) {
			ctx_lock.unlock();
			delete [] binding_nm;
			ns_lib::throw_exception(_env, error);
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_binding(lctx, binding_nm, obj,
				    btype, true, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_BIND_CTX_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		//
		// The binding represents a remote context, so forward
		// the bind call to the appropriate name server.
		//
		naming::naming_context_ptr rctxp;
		Environment e;
		CORBA::Exception *ex;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());
		NS_PRINTF(NS_BIND_CTX,
			("bind_ctx: rest of name :%s:\n", rest_of_name));

		rctxp->bind_context(rest_of_name, obj, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL)
		a_binding->release();
	if (rest_of_name != NULL)
		delete [] rest_of_name;
}

void
naming_context_ii::rebind_context(const char *nm,
    naming::naming_context_ptr obj, Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_REBIND_CTX, ("rebind_ctx: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_REBIND_CTX_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, (
		    "naming_context_ii::rebind_context: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return;
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return;
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_REBIND_CTX, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		binding_nm = ns_lib::get_last_comp(nm);

		// Determine if this is a local or remote context
		binding::binding_type btype = get_context_type(obj);
		NS_PRINTF(NS_REBIND_CTX, ("rebind_ctx: btype = %d\n", btype));

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_REBIND_CTX_BEFORE_STORE_BINDING,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->store_binding(binding_nm, btype, obj, true);
		if (error != nc_ok) {
			ctx_lock.unlock();
			delete [] binding_nm;
			ns_lib::throw_exception(_env, error);
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_binding(lctx, binding_nm, obj,
				    btype, true, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_REBIND_CTX_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		//
		// The binding represents a remote context, so forward
		// the bind call to the appropriate name server.
		//
		naming::naming_context_ptr rctxp;
		Environment e;
		CORBA::Exception *ex;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());
		NS_PRINTF(NS_REBIND_CTX,
			("rebind_ctx: rest of name :%s:\n", rest_of_name));

		rctxp->rebind_context(rest_of_name, obj, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL)
		a_binding->release();
	if (rest_of_name != NULL)
		delete [] rest_of_name;
}

CORBA::Object_ptr
naming_context_ii::resolve(const char *nm, Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name = NULL;
	binding			*a_binding;
	CORBA::Object_ptr 	obj = CORBA::Object::_nil();
	int			retry_count = 0;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_RESOLVE, ("resolve: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif

	// Check the nameserver security parameters
	if (!has_permission(NS_RESOLVE_PERM, NS_READ_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, ("naming_context_ii::resolve: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}

	//
	// A NULL name implies the current context
	//
	if (ns_lib::name_length(nm) == 0) {
		naming::naming_context_ptr rctx = 0;
		if (replicated) {
#ifdef _KERNEL_ORB
			rctx = ((naming_context_repl_impl *)
			    this->nctxp)->get_objref(); //lint !e1774
#endif
		} else {
			rctx = ((naming_context_impl *)
			    this->nctxp)->get_objref(); //lint !e1774
		}
		return (rctx);
	}

	//
	// Lookup the binding associated with the name by processing
	// the last component.
	//

	error = lookup(nm, true, a_binding, rest_of_name);
	// NS_PRINTF(NS_RESOLVE, ("name %s error = %d\n", nm, error));

	int ns_type = get_nameserver_type();

	if (error == nc_missing_node) {
		//
		// Check if we are already in the context
		// c1 (ALL_CONTEXT). If so, then no need
		// to look further. Return from here.
		// If we are in context c0 or context
		// of the local zone, look further
		// in the c1 context.
		//
		if (!get_look_further()) {
			ns_lib::throw_exception(_env, error);
			return (CORBA::Object::_nil());
		}

		//
		// We are not inside C1. Hence get the
		// context c1 and search for the object
		// there
		//

		rest_of_name = NULL;
		a_binding = NULL;
		naming::naming_context_var ctxp;
		Environment e;
		CORBA::Exception *ex;
#ifdef _KERNEL_ORB
		naming_context_repl_impl *rimpl = NULL;
#endif

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		switch (ns_type) {

		case LOCAL_NS:
			retry_count = 1;
			ctxp = ns::local_nameserver_c1();
			while (CORBA::is_nil(ctxp)) {
				if (retry_count == 5) {
					break;
				}
				retry_count++;
				ctxp = ns::local_nameserver_c1();
			}

			//
			// After 5 retries also, if its nil, then panic
			// the system in case of debug code. We may decide
			// to add a sleep of 1 - 2 sec if we see numerous
			// panics in future.
			// In case of non debug code, return nil
			//
			ASSERT(!CORBA::is_nil(ctxp));
			if (CORBA::is_nil(ctxp)) {
				return (CORBA::Object::_nil());
			}

			// Call resolve in the ALL_CONTEXT
			obj = ctxp->resolve(nm, e);
			break;

		case GLOBAL_NS:
#ifdef _KERNEL_ORB
			retry_count = 1;
			ctxp = ns::root_nameserver_c1();
			while (CORBA::is_nil(ctxp)) {
				if (retry_count == 5) {
					break;
				}
				retry_count++;
				ctxp = ns::root_nameserver_c1();
			}

			//
			// After 5 retries also, if its nil, then panic
			// the system in case of debug code. We may decide
			// to add a sleep of 1 - 2 sec if we see numerous
			// panics in future.
			// In case of non debug code, return nil
			//
			ASSERT(!CORBA::is_nil(ctxp));
			if (CORBA::is_nil(ctxp)) {
				return (CORBA::Object::_nil());
			}

			//
			// Call resolve in the ALL_CONTEXT
			//
			// Do not make a idl call as it is a nested
			// invocation and may cause a deadlock.
			// Since we are already on the name server primary,
			// use the C++ object to do the resolve
			//
			rimpl = (naming_context_repl_impl *)
			    ctxp->_handler()->get_cookie();
			ASSERT(rimpl != NULL);
			if (rimpl == NULL) {
				return (CORBA::Object::_nil());
			}
			obj = rimpl->resolve(nm, e);
#endif
			break;

		case NONE_NS:
		default:
			//
			// Nothing to do
			//
			ASSERT(0);
			return (CORBA::Object::_nil()); //lint !e527

		}
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
			return (CORBA::Object::_nil());
		}
		return (obj);

	}

	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return (CORBA::Object::_nil());
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_RESOLVE, FaultFunctions::generic);
#endif

	//
	// If "rest_of_name" is NULL, it implies that all the components
	// in the pathname were successfully looked up and the returned
	// binding represents one of the following:
	//
	// 	a) An object
	//	b) A local context
	//	c) A remote context
	//
	// So, just return the object reference contained in the binding.
	//
	// If "rest_of_name" isn't NULL, forward the call to the remote
	// name server using the object reference contained in the binding.
	//
	if (rest_of_name == NULL) {
		if (a_binding == NULL) {
			os::panic("RESOLVE: NULL binding\n");
		}
		// Increment ref count since this is an out parameter.
		// The ctx_lock is held while obtaining the object pointer
		// and duplicating it to protect against other threads e.g.
		// rebind(), calling release() and deleting the object.
		ctx_lock.wrlock();
		obj = CORBA::Object::_duplicate(a_binding->get_obj());
		ctx_lock.unlock();
	} else {
		Environment e;
		CORBA::Exception *ex;
		naming::naming_context_ptr rctxp;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		NS_PRINTF(NS_RESOLVE, ("resolve: remote resolve\n"));
		//
		// The binding represents a remote context, so forward
		// the call to the appropriate name server.
		//
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());
		NS_PRINTF(NS_RESOLVE,
			("resolve: rest of name :%s:\n", rest_of_name));

		obj = rctxp->resolve(rest_of_name, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
			obj = CORBA::Object::_nil();
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL)
		a_binding->release();
	if (rest_of_name != NULL)
		delete [] rest_of_name;
	return (obj);
}

void
naming_context_ii::unbind(const char *nm, Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_UNBIND, ("unbind: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_UNBIND_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, ("naming_context_ii::unbind: NS_EPERM "
		"name = %s perm = 0x%x owner = %d replicated = %d "
		"env_clid = %d\n",
		nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return;
	}
	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return;
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_UNBIND, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
			NS_PRINTF(NS_UNBIND, ("unbind: NULL binding\n"));
		} else {
			bstore = a_binding->get_store_obj();
			NS_PRINTF(NS_UNBIND, ("unbind: local context\n"));
		}

		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_UNBIND_BEFORE_DELETE_BINDING,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->delete_binding(binding_nm);
		if (error != nc_ok) {
			ctx_lock.unlock();
			ns_lib::throw_exception(_env, error);
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_unbind(lctx, binding_nm, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_UNBIND_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
		// Now, Delete the binding name
		delete [] binding_nm;
	} else {
		naming::naming_context_ptr rctxp;
		Environment e;
		CORBA::Exception *ex;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		//
		// The binding represents a remote context, so forward
		// the call to the appropriate name server.
		//
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());
		NS_PRINTF(NS_UNBIND,
		    ("unbind: rest of name :%s:\n", rest_of_name));

		rctxp->unbind(rest_of_name, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL)
		a_binding->release();
	if (rest_of_name != NULL)
		delete [] rest_of_name;
}


naming::naming_context_ptr
naming_context_ii::bind_new_context(const char *nm, Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;
	naming::naming_context_ptr ctx = naming::naming_context::_nil();

	NS_PRINTF(NS_BIND_NEWCTX, ("bind_newctx: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_BIND_NEW_CTX_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, (
		    "naming_context_ii::bind_new_context: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return (ctx);
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return (ctx);
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		//
		// Allocate a new context
		//
		binding_store *nstore = NULL;

		if (replicated) {
#ifdef _KERNEL_ORB
			repl_ns_server *repl_srvr =
			    repl_ns_server::get_repl_server();

			naming_context_repl_impl *nc_rimpl =
			    new naming_context_repl_impl(repl_srvr);
			ctx = nc_rimpl->get_objref();
			nstore = nc_rimpl->b_store;
#endif
		} else {
			naming_context_impl *nc_impl =
			    new naming_context_impl();
			ctx = nc_impl->get_objref();
			nstore = nc_impl->b_store;
		}

		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->bind_new_context(binding_nm, ctx,
		    nstore, false);
		if (error != nc_ok) {
			NS_PRINTF(NS_BIND_NEWCTX,
			    ("bind_newctx: could not create new context\n"));
			ctx_lock.unlock();
			delete [] binding_nm;
			CORBA::release(ctx);
			ns_lib::throw_exception(_env, error);
			ctx = naming::naming_context::_nil();
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_context(lctx, ctx, binding_nm,
				    _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		//
		// The binding represents a remote context, so forward
		// the call to the appropriate name server.
		//
		Environment e;
		CORBA::Exception *ex;
		naming::naming_context_ptr rctxp;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif

		NS_PRINTF(NS_BIND_NEWCTX,
			("bind_newctx: remote context: %s\n", rest_of_name));
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());

		ctx = rctxp->bind_new_context(rest_of_name, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
			ctx = naming::naming_context::_nil();
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL) {
		a_binding->release();
	}
	if (rest_of_name != NULL) {
		delete [] rest_of_name;
	}
	return (ctx);
}

naming::naming_context_ptr
naming_context_ii::bind_new_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment &_env)
{
	nc_error		 error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;
	naming::naming_context_ptr ctx = naming::naming_context::_nil();

	NS_PRINTF(NS_BIND_NEWCTX, ("bind_newctx_v1: name = %s lf = %d perm = %d"
	    " own = %d\n", nm, lf, perm, own));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_BIND_NEW_CTX_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, (
		    "naming_context_v1::bind_new_context_v1: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return (ctx);
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return (ctx);
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX, FaultFunctions::generic);
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		//
		// Allocate a new context
		//
		binding_store *nstore = NULL;

		if (replicated) {
#ifdef _KERNEL_ORB
			repl_ns_server *repl_srvr =
			    repl_ns_server::get_repl_server();

			naming_context_repl_impl *nc_rimpl =
			    new naming_context_repl_impl(repl_srvr,
			    lf, perm, own);
			ctx = nc_rimpl->get_objref();
			nstore = nc_rimpl->b_store;
#endif
		} else {
			naming_context_impl *nc_impl =
			    new naming_context_impl(lf, perm, own);
			ASSERT(nc_impl != NULL);
			if (os::strcmp(nm, "0") == 0) {
				NS_PRINTF(NS_DUMP_ALL, (
				    "bind_newctx_v1: name = %s "
				    "local_ctx_0 = %p\n", nm, nc_impl));
			} else if (os::strcmp(nm, "1") == 0) {
				NS_PRINTF(NS_DUMP_ALL, (
				    "bind_newctx_v1: name = %s "
				    "local_ctx_1 = %p\n", nm, nc_impl));
			}
			ctx = nc_impl->get_objref();
			nstore = nc_impl->b_store;
		}

		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX,
		    FaultFunctions::generic);
#endif

		ctx_lock.wrlock();
		error = bstore->bind_new_context(binding_nm, ctx,
		    nstore, false);
		if (error != nc_ok) {
			NS_PRINTF(NS_BIND_NEWCTX,
			    ("bind_newctx: could not create new context\n"));
			ctx_lock.unlock();
			delete [] binding_nm;
			CORBA::release(ctx);
			ns_lib::throw_exception(_env, error);
			ctx = naming::naming_context::_nil();
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_context_v1(lctx, ctx,
				    binding_nm, lf, perm, own, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		//
		// The binding represents a remote context, so forward
		// the call to the appropriate name server.
		//
		Environment e;
		CORBA::Exception *ex;
		naming::naming_context_ptr rctxp;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		NS_PRINTF(NS_BIND_NEWCTX,
			("bind_newctx: remote context: %s\n", rest_of_name));
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());

		ctx = rctxp->bind_new_context_v1(rest_of_name,
		    lf, perm, own, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
			ctx = naming::naming_context::_nil();
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL) {
		a_binding->release();
	}
	if (rest_of_name != NULL) {
		delete [] rest_of_name;
	}
	return (ctx);
}

naming::naming_context_ptr
naming_context_ii::rebind_existing_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment &_env)
{
	nc_error 		error;
	char			*rest_of_name;
	binding			*a_binding;
	uint32_t		env_clid = 0;
	naming::naming_context_ptr ctx = naming::naming_context::_nil();

	NS_PRINTF(NS_BIND_NEWCTX, ("rebind_exctx: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_REBIND_EX_CTX_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, (
		    "naming_context_ii::rebind_existing_context_v1: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return (ctx);
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return (ctx);
	}

#ifdef _KERNEL_ORB
	//
	// TODO: Add new fault point
	// FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX, FaultFunctions::generic);
	//
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		//
		// Allocate a new context
		//
		binding_store *nstore = NULL;

		if (replicated) {
#ifdef _KERNEL_ORB
			repl_ns_server *repl_srvr =
			    repl_ns_server::get_repl_server();

			naming_context_repl_impl *nc_rimpl =
			    new naming_context_repl_impl(repl_srvr,
			    lf, perm, own);
			ctx = nc_rimpl->get_objref();
			nstore = nc_rimpl->b_store;
#endif
		} else {
			naming_context_impl *nc_impl =
			    new naming_context_impl(lf, perm, own);
			ctx = nc_impl->get_objref();
			nstore = nc_impl->b_store;
		}

		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		//
		// TODO: Add new fault point
		// FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX,
		//    FaultFunctions::generic);
		//
#endif

		ctx_lock.wrlock();
		error = bstore->rebind_existing_context(binding_nm, ctx,
		    nstore, true);
		if (error != nc_ok) {
			NS_PRINTF(NS_BIND_NEWCTX,
			    ("rebind_exctx: could not rebind context %s\n",
			    nm));
			ctx_lock.unlock();
			delete [] binding_nm;
			CORBA::release(ctx);
			ns_lib::throw_exception(_env, error);
			ctx = naming::naming_context::_nil();
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_context_v1(lctx, ctx,
				    binding_nm, lf, perm, own, _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();

#ifdef _KERNEL_ORB
			FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT,
			    FaultFunctions::generic);
#endif
		}
	} else {
		//
		// The binding represents a remote context, so forward
		// the call to the appropriate name server.
		//
		Environment e;
		CORBA::Exception *ex;
		naming::naming_context_ptr rctxp;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif
		NS_PRINTF(NS_BIND_NEWCTX,
			("rebind_exctx: remote context: %s\n", rest_of_name));
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());

		ctx = rctxp->rebind_existing_context_v1(rest_of_name,
		    lf, perm, own, e);
		if ((ex = e.exception()) != NULL) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
			ctx = naming::naming_context::_nil();
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL) {
		a_binding->release();
	}
	if (rest_of_name != NULL) {
		delete [] rest_of_name;
	}
	return (ctx);
}

//
// This will unbind an existing context.
//
bool
naming_context_ii::unbind_existing_context_v1(const char *nm, Environment &_env)
{
	nc_error		error;
	char			*rest_of_name;
	binding			*a_binding;
	bool			ret_val = true;
	uint32_t		env_clid = 0;

	NS_PRINTF(NS_BIND_NEWCTX, ("unbind_exctx: name = %s\n", nm));

#if (SOL_VERSION  >= __s10) && !defined(UNODE)
	env_clid = _env.get_cluster_id();
#endif
	// Check the nameserver security parameters
	if (!has_permission(NS_UNBIND_EX_CTX_PERM, NS_WRITE_OP, _env)) {
		NS_PRINTF(NS_DUMP_ALL, (
		    "naming_context_ii::unbind_existing_context_v1: NS_EPERM "
		    "name = %s perm = 0x%x owner = %d replicated = %d "
		    "env_clid = %d\n",
		    nm, ns_perm, owner, replicated, env_clid));
		_env.system_exception(CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
		return (false);
	}

	//
	// Lookup the context which contains the object
	// and return its binding.
	//
	error = lookup(nm, false, a_binding, rest_of_name);
	if (error != nc_ok) {
		ns_lib::throw_exception(_env, error);
		return (false);
	}

#ifdef _KERNEL_ORB
	//
	// TODO: Add new fault point
	// FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX, FaultFunctions::generic);
	//
#endif

	// Determine if the binding represents a local or remote context.
	if (a_binding == NULL || a_binding->is_local_context()) {
		binding_store *bstore;
		char *binding_nm;

		//
		// A NULL binding implies that the name represents
		// the current context which is local.
		//
		if (a_binding == NULL) {
			bstore = b_store;
		} else {
			bstore = a_binding->get_store_obj();
		}

		binding_nm = ns_lib::get_last_comp(nm);

#ifdef _KERNEL_ORB
		//
		// TODO: Add new fault point
		// FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_BEFORE_BIND_NEWCTX,
		//    FaultFunctions::generic);
		//
#endif

		ctx_lock.wrlock();
		error = bstore->unbind_existing_context(binding_nm, true);
		if (error != nc_ok) {
			NS_PRINTF(NS_BIND_NEWCTX,
			    ("bind_newctx: could not delete context %s\n",
			    binding_nm));
			ctx_lock.unlock();
			ns_lib::throw_exception(_env, error);
			ret_val = false;
		} else {
			// Checkpoint this operation if replicated
			if (replicated) {
#ifdef _KERNEL_ORB
				//lint -e1774
				naming::naming_context_ptr lctx =
				    ((naming_context_repl_impl *)
				    bstore->get_ctx())->get_objref();
				//lint +e1774

				get_ckpt()->ckpt_unbind(lctx, binding_nm,
				    _env);
				_env.clear();
				CORBA::release(lctx);
#endif
			}
			ctx_lock.unlock();
#ifdef _KERNEL_ORB
			//
			// TODO: Add new fault point
			// FAULTPT_NS(FAULTNUM_NS_BIND_NEWCTX_AFTER_CKPT,
			//    FaultFunctions::generic);
			//
#endif
		}
		// Now, Delete the binding name
		delete [] binding_nm;
	} else {
		//
		// The binding represents a remote context, so forward
		// the call to the appropriate name server.
		//
		Environment e;
		CORBA::Exception *ex;
		naming::naming_context_ptr rctxp;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

		e.set_zone_id(_env.get_zone_id());
		e.set_zone_uniqid(_env.get_zone_uniqid());
		e.set_cluster_id(_env.get_cluster_id());

#endif

		NS_PRINTF(NS_BIND_NEWCTX,
			("unbind_exctx: remote context: %s\n", rest_of_name));
		rctxp = naming::naming_context::_narrow(a_binding->get_obj());

		ret_val = rctxp->unbind_existing_context_v1(rest_of_name, e);
		if (((ex = e.exception()) != NULL) || (!ret_val)) {
			if (ex->is_system_exception()) {
				//
				// Cannot get to context!  Return the
				// 'missing_node' exception.
				//
				e.clear();
				ns_lib::throw_exception(_env, nc_missing_node);
			} else {
				//
				// We can forward the exception onto the caller
				// because this nested call is of the same type,
				// and all the exceptions it could return are
				// already defined in the IDL file.
				//
				_env.exception(e.release_exception());
			}
			ret_val = false;
		}
		CORBA::release(rctxp);
	}

	if (a_binding != NULL) {
		a_binding->release();
	}
	if (rest_of_name != NULL) {
		delete [] rest_of_name;
	}

	return (ret_val);
}


//
// This routine returns a given number of bindings in this context.
// If there are more bindings then a binding_iterator object is created
// and the reference to this iterator is returned to the caller.
//
void
naming_context_ii::list(uint32_t how_many, naming::binding_list_out bl,
    naming::binding_iterator_out bi, Environment &_env)
{
	uint32_t total_bindings, ret_bindings;
	binding **bindings = NULL;
	naming::binding_list *out_list, *iter_list;
	naming::binding_iterator_ptr bi_ptr = naming::binding_iterator::_nil();
	nc_error error;

	NS_PRINTF(NS_LIST, ("list:\n"));

	total_bindings = b_store->get_binding_count();

	if (how_many < total_bindings) {
		ret_bindings = how_many;
	} else {
		ret_bindings = total_bindings;
	}
	NS_PRINTF(NS_LIST,
	    ("list: tot: %d ret: %d\n", total_bindings, ret_bindings));

	if (ret_bindings) {
		bindings = new binding * [total_bindings];
		ctx_lock.wrlock();
		error = b_store->get_all_bindings(bindings, total_bindings);
		ctx_lock.unlock();
		if (error != nc_ok) {
			delete [] bindings;
			bl = (naming::binding_list *)NULL;
			bi = bi_ptr;
			ns_lib::throw_exception(_env, error);
			return;
		}
	}

#ifdef _KERNEL_ORB
	FAULTPT_NS(FAULTNUM_NS_LIST, FaultFunctions::generic);
#endif

	//
	// Allocate an out array and copy the data into it.
	//
	if (ret_bindings == 0) {
		out_list = new naming::binding_list();
	} else {
		uint32_t i;
		binding *a_binding;

		out_list =
		    new naming::binding_list(ret_bindings, ret_bindings);

		for (i = 0; i < ret_bindings; i++) {
			a_binding = bindings[i];
			//
			// Now, get binding info for this binding and
			// then release the binding
			//
			if (a_binding != NULL) {
				a_binding->get_binding((*out_list)[i]);
				bindings[i] = NULL;
				a_binding->release();
			}
		}

		//
		// Create a binding iterator object if the context has
		// additional bindings.
		//
		if (ret_bindings < total_bindings) {
			uint32_t j, rem = total_bindings - ret_bindings;
			iter_list = new naming::binding_list(rem, rem);

			for (i = 0, j = ret_bindings; j < total_bindings;
			    i++, j++) {
				a_binding = bindings[j];
				//
				// Now, get binding info for this binding and
				// then release the binding
				//
				if (a_binding != NULL) {
					a_binding->get_binding((*iter_list)[i]);
					bindings[j] = NULL;
					a_binding->release();
				}
			}

			binding_iterator_impl *bi_impl;

			bi_impl = new binding_iterator_impl(0, rem, iter_list);
			bi_ptr = bi_impl->get_objref();
		}
	}
	if (bindings != NULL)
		delete [] bindings;

	bl = out_list;
	bi = bi_ptr;
}    


#ifdef _KERNEL_ORB

//
// Replicated implementation
//

void
naming_context_repl_impl::bind(const char *nm, CORBA::Object_ptr obj,
    Environment &_env)
{
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(!CORBA::is_nil(obj));
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// binds in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::bind nm = %s "
			    "Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return;
		}
	}

	if (ns_op_state::retry(_env) == (bool)true) {
		return;
	}
	naming_context_ii::bind(nm, obj, _env);
}

void
naming_context_repl_impl::rebind(const char *nm, CORBA::Object_ptr obj,
    Environment &_env)
{
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(!CORBA::is_nil(obj));
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// rebinds in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::rebind nm = %s "
			    "Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return;
		}
	}

	if (ns_op_state::retry(_env) == (bool)true) {
		return;
	}
	naming_context_ii::rebind(nm, obj, _env);
}

void
naming_context_repl_impl::bind_context(const char *nm,
    naming::naming_context_ptr obj, Environment &_env)
{
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(!CORBA::is_nil(obj));
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// bind_context in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::bind_context nm = %s "
			    "obj = %p Throwing CTX_EACCESS\n", nm, obj));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return;
		}
	}

	if (ns_op_state::retry(_env) == (bool)true) {
		return;
	}
	naming_context_ii::bind_context(nm, obj, _env);
}

void
naming_context_repl_impl::rebind_context(const char *nm,
    naming::naming_context_ptr obj, Environment &_env)
{
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(!CORBA::is_nil(obj));
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// rebind_context in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::rebind_context nm = %s"
			    " obj = %p Throwing CTX_EACCESS\n", nm, obj));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return;
		}
	}

	if (ns_op_state::retry(_env) == (bool)true) {
		return;
	}
	naming_context_ii::rebind_context(nm, obj, _env);
}

CORBA::Object_ptr
naming_context_repl_impl::resolve(const char *nm, Environment &_env)
{
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifdef _KERNEL

	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// resolve in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::resolve nm = %s "
			    "Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return (CORBA::Object::_nil());
		}

		//
		// In the real root context of the global nameserver,
		// if some client is trying to look up a zone cluster
		// context, make sure that the client ran from that
		// zone cluster. All clients will be allowed to lookup
		// the ALL_CLUSTER (c1) context.
		//

		uint32_t 		clid = _env.get_cluster_id();

		if ((len == 0) && (clid != GLOBAL_ZONEID) && (clid != 2) &&
		    (n != 1) && (n != (int)clid)) {
			//
			// len == 0 means the string is made up of only
			// numbers. The client is trying to resolve a
			// virtual context.
			// clid != GLOBAL_ZONEID means that the client is
			// not running in the global zone.
			// clid != 2 means it is not a native local zone
			// n != 1 means that the client is not trying to
			// resolve the ALL_CLUSTER context (c1)
			// n != clid means that the client is not trying
			// to resolve its own zone cluster context.
			// Effectively, the client is trying to access
			// some other context.
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::resolve "
			    "nm = %s len = %d clid = %d "
			    "n = %d  env = %p "
			    "Throwing NS_EPERM\n",
			    nm, len, clid, n, &_env));
			_env.system_exception(
			    CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
			return (CORBA::Object::_nil());

		} else if ((len == 0) && (clid == 2) && (n != 0) && (n != 1)) {
			//
			// len == 0 means the string is made up of only
			// numbers. The client is trying to resolve a
			// virtual context.
			// clid == 2 means it is a native local zone. Native
			// local zones can access only context 0 and 1. They
			// can not access the contexts of zone clusters.
			// n != 0 means that the client is not trying to
			// resolve the ALL_CLUSTER context (c0)
			// n != 1 means that the client is not trying to
			// resolve the ALL_CLUSTER context (c1)
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::resolve "
			    "native local zone nm = %s len = %d "
			    "clid = %d n = %d  env = %p "
			    "Throwing NS_EPERM\n",
			    nm, len, clid, n, &_env));
			_env.system_exception(
			    CORBA::NS_EPERM(0, CORBA::COMPLETED_NO));
			return (CORBA::Object::_nil());
		}
	}

#endif
#endif
	return (naming_context_ii::resolve(nm, _env));
}

void
naming_context_repl_impl::unbind(const char *nm, Environment &_env)
{
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// unbinds in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::unbind nm = %s "
			    "Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return;
		}
	}

	if (ns_op_state::retry(_env) == (bool)true) {
		return;
	}
	naming_context_ii::unbind(nm, _env);
}

naming::naming_context_ptr
naming_context_repl_impl::bind_new_context(const char *nm, Environment &_env)
{
	naming::naming_context_ptr ctx;
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// binding new context in the real root of the
	// global name server. Some critical objects
	// are allowed  to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::bind_new_context "
			    "nm = %s Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return (naming::naming_context::_nil());
		}
	}

	ctx = ns_new_context_state::retry(_env);
	if (!CORBA::is_nil(ctx)) {
		return (naming::naming_context::_duplicate(ctx));
	} else {
		return (naming_context_ii::bind_new_context(nm, _env));
	}
}

naming::naming_context_ptr
naming_context_repl_impl::bind_new_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment &_env)
{
	naming::naming_context_ptr ctx;
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// binds in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::bind_new_context_v1 "
			    "nm = %s Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return (naming::naming_context::_nil());
		}
	}

	ctx = ns_new_context_state::retry(_env);
	if (!CORBA::is_nil(ctx)) {
		return (naming::naming_context::_duplicate(ctx));
	} else {
		return (naming_context_ii::bind_new_context_v1(nm,
		    lf, perm, own, _env));
	}
}

naming::naming_context_ptr
naming_context_repl_impl::rebind_existing_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment &_env)
{
	naming::naming_context_ptr ctx;
	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// rebinds in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, ("naming_context_repl_impl::"
			    "rebind_existing_context_v1"
			    " nm = %s "
			    "Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return (naming::naming_context::_nil());
		}
	}

	ctx = ns_new_context_state::retry(_env);
	if (!CORBA::is_nil(ctx)) {
		return (naming::naming_context::_duplicate(ctx));
	} else {
		return (naming_context_ii::rebind_existing_context_v1(nm,
		    lf, perm, own, _env));
	}
}

bool
naming_context_repl_impl::unbind_existing_context_v1(const char *nm,
    Environment &_env)
{
	naming::naming_context_ptr ctx;

	version_manager::vp_version_t ns_version;

	ASSERT(nm != NULL);
	ASSERT(ns::global_nmsrvr_obj != NULL);

	//
	// If new version is running, do not allow
	// unbinds in the real root of the global name
	// server. Some critical objects are allowed
	// to be bound though.
	// The critical objects are all the zone cluster
	// ids. They are always integers (0, 1, 3 ...)
	// Allow strings like "services" and "local_ns_ ..."
	//
	ns_version = ns::get_ns_version();
	if ((ns::global_nmsrvr_obj == this) && (ns_version.major_num >= 2)) {

		char 		num[30];
		int 		len = 0;
		int 		n = os::atoi(nm);

		(void) os::itoa(n, num, 10);

		//
		// If len == 0, means that both nm and num are the
		// same string and hence nm was made up of all
		// numerals.
		//
		len = os::strcmp(nm, num);

		if ((len != 0) && (os::strcmp(nm, "services") != 0) &&
		    (os::strncmp(nm, "local_ns_", (size_t)9) != 0)) {
			//
			// Return if its a string and the string is not
			// "services" and the string does not start with
			// "local_ns_"
			// If nm is a number, let it through.
			//
			// Since the client does not have permission
			// to bind, no object was bound and hence we
			// return CORBA::COMPLETED_NO
			//
			NS_PRINTF(NS_DUMP_ALL, (
			    "naming_context_repl_impl::"
			    "unbind_existing_context_v1"
			    " nm = %s Throwing CTX_EACCESS\n", nm));
			_env.system_exception(
			    CORBA::CTX_EACCESS(0, CORBA::COMPLETED_NO));
			return (false);
		}
	}

	ctx = ns_new_context_state::retry(_env);
	if (!CORBA::is_nil(ctx)) {
		return (false);
	} else {
		return (
		    naming_context_ii::unbind_existing_context_v1(nm, _env));
	}
}

//
// Method to dump all name server state to the secondary
//
void
naming_context_ii::dump_state(repl_ns::ns_replica_ptr ckpt,
    Environment &_env)
{
	//lint -e1774
	naming::naming_context_ptr lctx =
	    ((naming_context_repl_impl *)b_store->get_ctx())->get_objref();
	//lint +e1774

	if (repl_ns::ns_replica::_version(ckpt) == 0) {
		NS_PRINTF(NS_CKPT, ("naming_context_ii::dump_state: "
		    "lctx = %p\n", lctx));
		ckpt->ckpt_root_context(lctx, _env);
	} else {
		NS_PRINTF(NS_CKPT, ("naming_context_ii::dump_state: "
		    "lctx = %p lf = %d perm = 0x%x own = %d\n",
		    lctx, look_further, ns_perm, owner));
		ckpt->ckpt_root_context_v1(lctx,
		    look_further, ns_perm, owner, _env);
	}
	if (_env.exception() == NULL) {
		b_store->dump_state(ckpt, _env);
	} else {
		_env.clear();
	}
	CORBA::release(lctx);
}

//
// Checkpoint methods invoked on the secondaries
//
void
naming_context_ii::ckpt_binding(const char *nm, CORBA::Object_ptr obj,
    int32_t bind_type, bool is_rebind, Environment &env)
{
	char *binding_nm = new char[os::strlen(nm) + 1];
	(void) os::strcpy(binding_nm, nm);
	int error = b_store->store_binding(binding_nm,
	    (binding::binding_type)bind_type, obj, is_rebind);
	ASSERT(error == nc_ok);

	ns_op_state::register_new_state(env);
}


void
naming_context_ii::ckpt_context(naming::naming_context_ptr ctx,
    const char *nm, Environment &env)
{
	naming_context_repl_impl *rimpl = new naming_context_repl_impl(ctx,
	    false, NS_ALLOW_ALL_OPP_PERM, GLOBAL_ZONEID);

	char *binding_nm = new char[os::strlen(nm) + 1];
	(void) os::strcpy(binding_nm, nm);

	binding_store *nstore = rimpl->b_store;

	//
	// Force the creation of a new context since this context could
	// have existed before this provider became a spare.  See comment
	// in ::become_spare().
	//
	int error = b_store->bind_new_context(binding_nm, ctx, nstore, true);
	ASSERT(error == nc_ok);

	ns_new_context_state::register_new_state(ctx, env);
}

void
naming_context_ii::ckpt_context_v1(naming::naming_context_ptr ctx,
    const char *nm, bool lf, uint32_t perm, uint32_t own, Environment &env)
{
	naming_context_repl_impl *rimpl = new
	    naming_context_repl_impl(ctx, lf, perm, own);

	char *binding_nm = new char[os::strlen(nm) + 1];
	(void) os::strcpy(binding_nm, nm);

	binding_store *nstore = rimpl->b_store;

	//
	// Force the creation of a new context since this context could
	// have existed before this provider became a spare.  See comment
	// in ::become_spare().
	//
	int error = b_store->bind_new_context(binding_nm, ctx, nstore, true);
	ASSERT(error == nc_ok);

	ns_new_context_state::register_new_state(ctx, env);
}

void
naming_context_ii::ckpt_unbind(const char *nm, Environment &env)
{
	int error = b_store->delete_binding(nm);
	ASSERT(error == nc_ok);

	ns_op_state::register_new_state(env);
}

void
naming_context_ii::ckpt_unbound_context(naming::naming_context_ptr ctx)
{
	naming_context_repl_impl *rimpl = new naming_context_repl_impl(
	    ctx, false, NS_ALLOW_ALL_OPP_PERM, GLOBAL_ZONEID);

	NS_PRINTF(NS_CKPT,
	    ("secondary ckpt_unbound_context: "
	    "handler: %p impl: %p lf: %d perm: 0x%x own: %d\n",
	    ctx->_handler(), rimpl, false,
	    NS_ALLOW_ALL_OPP_PERM, GLOBAL_ZONEID));

	dump_info::add_unbound_ctx(rimpl);
}

void
naming_context_ii::ckpt_unbound_context_v1(naming::naming_context_ptr ctx,
    bool lf, uint32_t perm, uint32_t own)
{
	naming_context_repl_impl *rimpl =
	    new naming_context_repl_impl(ctx, lf, perm, own);

	NS_PRINTF(NS_CKPT,
	    ("secondary ckpt_unbound_context_v1: "
	    "handler: %p impl: %p lf: %d perm: 0x%x own: %d\n",
	    ctx->_handler(), rimpl, lf, perm, own));

	dump_info::add_unbound_ctx(rimpl);
}

//lint -e1038
dump_info::unbnd_ctx *unbound_headp = NULL;
//lint +e1038
int unbound_count = 0;

//
// Add a context at the end of the list pointed to by
// unbound_headp.
//
void
dump_info::add_to_list(naming_context_repl_impl *rimpl)
{
	NS_PRINTF(NS_CKPT,
		("add_to_list: rlimpl: %p\n", rimpl));

	ASSERT(rimpl != NULL);

	unbnd_ctx *dlistp = new unbnd_ctx;
	dlistp->rimpl = rimpl;
	dlistp->next = NULL;

	//
	// List is not empty. Append the context at
	// the end of the list.
	//
	if (unbound_headp != NULL) {
		ASSERT(unbound_count > 0);
		unbnd_ctx *tmp_ctx = unbound_headp;
		while (tmp_ctx->next != NULL) {
			tmp_ctx = tmp_ctx->next;
		}
		tmp_ctx->next = dlistp;
	//
	// List is empty.
	//
	} else {
		ASSERT(unbound_count == 0);
		unbound_headp = dlistp;
	}

	unbound_count++;
}

void
dump_info::add_unbound_ctx(naming_context_repl_impl *rimpl)
{
	NS_PRINTF(NS_CKPT_UNBND, ("add_unbound_ctx - rimpl\n"));

	dump_info::add_to_list(rimpl);
}

//
// Add unbound contexts to this list so that they can be dumped out
// to new secondaries.
//
void
dump_info::add_unbound_ctx(CORBA::Object_ptr cobj)
{
	NS_PRINTF(NS_CKPT_UNBND, ("add_unbound_ctx - cobj\n"));

	ASSERT(!CORBA::is_nil(cobj));

	naming_context_repl_impl *rimpl =
	    (naming_context_repl_impl *)cobj->_handler()->get_cookie();

	NS_PRINTF(NS_CKPT,
	    ("add_unbound_ctx: handler: %p impl: %p\n",
	    cobj->_handler(), rimpl));
	if (rimpl) {
		dump_info::add_to_list(rimpl);
	}
}

//
// Delete an unbound context from this list since its being delete
// as a result of the _unreferenced call.
//
void
dump_info::delete_unbound_ctx(naming_context_repl_impl *rimpl)
{
	unbnd_ctx *listp, **listpp;

	for (listpp = &unbound_headp; *listpp != NULL;
	    listpp = &listp->next) {
		listp = *listpp;
		if (listp->rimpl == rimpl) {
			*listpp = listp->next;
			delete listp;
			unbound_count--;
			NS_PRINTF(NS_CKPT_UNBND, ("delete_unbound_ctx\n"));
			break;
		}
	}
} //lint !e818

//
// Dump out all local unbound contexts (implemented by the name server)
// on the list to the new secondary.
//
void
dump_info::dump_unbound_list(repl_ns::ns_replica_ptr ckpt, Environment &_env)
{
	NS_PRINTF(NS_DUMP_STATE,
	    ("dump_state: unbound list: %d\n", unbound_count));

	unbnd_ctx *unbound_listp = unbound_headp;
	while (unbound_listp != NULL) {

		NS_PRINTF(NS_DUMP_STATE, ("dump_state: unbound context\n"));

		naming::naming_context_ptr tctx =
		    unbound_listp->rimpl->get_objref();

		NS_PRINTF(NS_DUMP_STATE,
		    ("dump_unbound_list: handler: %p impl: %p\n",
		    tctx->_handler(), unbound_listp->rimpl));

		if (repl_ns::ns_replica::_version(ckpt) == 0) {
			ckpt->ckpt_unbound_context(tctx, _env);
		} else {
			bool lf = unbound_listp->rimpl->get_look_further();
			uint32_t perm = unbound_listp->rimpl->get_permission();
			uint32_t own = unbound_listp->rimpl->get_owner();
			ckpt->ckpt_unbound_context_v1(
			    tctx, lf, perm, own, _env);
		}

		if (_env.exception() != NULL) {
			_env.clear();
			CORBA::release(tctx);
			break;
		}
		CORBA::release(tctx);
		unbound_listp = unbound_listp->next;
	}
}

//
// Called from repl_ns_server::become_spare() to delete all entries
// in the unbound list as this provider is being converted to a spare.
// Note that new entries could be added to the list while it is being
// processed by this function. This happens if there is more than one
// level of contexts below the root context. Contexts are added to the
// unbound-contexts list when their binding is going to be deleted.
//
void
dump_info::cleanup_unbound_list()
{
	unbnd_ctx *listp = unbound_headp;
	unbnd_ctx *next_ptr;

	NS_PRINTF(NS_CKPT_UNBND, ("cleanup_unbound_list\n"));

	//
	// Delete entries in the unbound-context list pointed to
	// by unbound_headp. Deletion of a context results in the
	// deletion of all bindings under that context. If a binding
	// is a name-context binding, then the context is placed on
	// the unbound-context list. Hence, new contexts could be
	// added to this list while it is being processed here. All
	// such contexts are added at the end of the list and get
	// processed here. The list should be empty at the end of
	// this while loop.
	//
	while (listp != NULL) {
		delete listp->rimpl;
		next_ptr = listp->next;
		delete listp;
		unbound_headp = listp = next_ptr;
		unbound_count --;
	}
	ASSERT(unbound_headp == NULL);
	ASSERT(unbound_count == 0);
	NS_PRINTF(NS_CKPT_UNBND, ("cleanup_unbound_list done\n"));
}

#endif

/*
nc_error
naming_context_impl::lookup(const char *nm, int process_last_comp,
	    binding *&ret_binding, char *&rest_of_name);
{
	naming_context_ii::lookup(
	    nm, process_last_comp, ret_binding, rest_of_name);
}
*/

//lint +e1776
