//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)volatile_binding.cc 1.13	08/05/20 SMI"


#include <nslib/store.h>
#include <nslib/naming_context_impl.h>


volatile_binding::volatile_binding(char *name, CORBA::Object_ptr obj,
    binding::binding_type bind_type) : refcnt_lock()
{
	vbname = name;
	cobj = CORBA::Object::_duplicate(obj);
	bt = bind_type;
	store_obj = NULL;
	ref_count = 1;
}

volatile_binding::volatile_binding(char *name, CORBA::Object_ptr nctx,
    binding_store *store) : refcnt_lock()
{
	vbname = name;
	cobj = CORBA::Object::_duplicate(nctx);
	bt = binding::bt_local_ctx;
	store_obj = store;
	ref_count = 1;
}

// XXX - Check if mutex destructor gets called?
volatile_binding::~volatile_binding()
{
	ASSERT(ref_count == 0);

	NS_PRINTF(NS_BINDING,
	    ("volatile_binding: DESTRUCTOR  name: %s\n", vbname));
	delete [] vbname;
	CORBA::release(cobj);
	cobj = NULL;
	store_obj = NULL;
}

//
// Increment binding reference count
//
void
volatile_binding::hold()
{
	refcnt_lock.lock();
	ref_count++;
	refcnt_lock.unlock();
}

//
// Decrement binding reference count and delete when
// the binding has no references.
//
void
volatile_binding::release()
{
	NS_PRINTF(NS_BINDING,
	    ("volatile_binding: release: %s ref: %d\n", vbname, ref_count));

	refcnt_lock.lock();
	ASSERT(ref_count != 0);
	ref_count--;
	refcnt_lock.unlock();

	//
	// If the binding is going to be deleted, add an entry to the
	// linked list of unbound contexts.
	//
	if (ref_count == 0) {
#ifdef _KERNEL_ORB
		if (bt == binding::bt_local_ctx) {
			dump_info::add_unbound_ctx(cobj);
		}
#endif
		delete this;
	}
}

//
// Caller is reponsible for incrementing reference count
// on this object, if necessary.
//
CORBA::Object_ptr
volatile_binding::get_obj()
{
	return (cobj);
}

binding::binding_type
volatile_binding::get_binding_type()
{
	return (bt);
}

binding_store *
volatile_binding::get_store_obj()
{
	return (store_obj);
}

char *
volatile_binding::get_name()
{
	return (vbname);
}

void
volatile_binding::set_obj(CORBA::Object_ptr obj)
{
	if (cobj != NULL) {
		NS_PRINTF(NS_BINDING,
		    ("volatile_binding: obj is not NULL so release it\n"));
		CORBA::release(cobj);
	}
	cobj = CORBA::Object::_duplicate(obj);
}

void
volatile_binding::update_binding(char *name, binding::binding_type bind_type,
    CORBA::Object_ptr obj)

{
	ASSERT(vbname != NULL);
	delete [] vbname;
	vbname = name;
	bt = bind_type;
	set_obj(obj);
}

void
volatile_binding::update_ctx_binding(char *name, CORBA::Object_ptr ctx,
    binding_store *store)
{
	ASSERT(vbname != NULL);
	delete [] vbname;
	vbname = name;
	set_obj(ctx);
	store_obj = store;
}

void
volatile_binding::get_binding(naming::binding &bi)
{
	bi.binding_name = os::strcpy(new char[os::strlen(vbname) + 1], vbname);
	if (bt == binding::bt_obj)
		bi.bind_type = naming::nobject;
	else
		bi.bind_type = naming::ncontext;
}
