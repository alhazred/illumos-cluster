/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  data_container_impl_in.h
 *
 */

#ifndef _DATA_CONTAINER_IMPL_IN_H
#define	_DATA_CONTAINER_IMPL_IN_H

#pragma ident	"@(#)data_container_impl_in.h	1.6	08/05/20 SMI"

inline
data_container_impl::data_container_impl() :
	_strdata(NULL)
{
}

inline
data_container_impl::data_container_impl(char *state) :
	_strdata(state)
{
}

inline
data_container_impl::~data_container_impl()
{
	delete [] _strdata;
}

//
// Methods required to marshal and unmarshal state of this object
//
inline void
data_container_impl::marshal(service& b)
{
	b.put_string(_strdata);
}

inline static CORBA::Object_ptr
data_container_impl::unmarshal(service& b)
{
	char *mstr = b.get_string();
	return ((new data_container_impl(mstr))->get_objref());
}

inline void
data_container_impl::unmarshal_cancel(service& b)
{
	// Do the same as unmarshal but just neglect the returned data
	(void) b.get_string_cancel();
}

#endif	/* _DATA_CONTAINER_IMPL_IN_H */
