/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NAMING_CONTEXT_IMPL_H
#define	_NAMING_CONTEXT_IMPL_H

#pragma ident	"@(#)naming_context_impl.h	1.23	08/07/22 SMI"

#include <nslib/store.h>
#include <h/naming.h>
#include <orb/object/adapter.h>

#define	NONE_NS		0
#define	GLOBAL_NS	1
#define	LOCAL_NS	2

// Nameserver Operation Types
#define	NS_NO_OP		0
#define	NS_READ_OP		1
#define	NS_WRITE_OP		2
#define	NS_READWRITE_OP		3

#if defined(UNODE) || (SOL_VERSION < __s10)
#define	GLOBAL_ZONEID	0
#endif

//
// Name Server kernel security permissions
//
// Logic:
//
// 1. Check the owner of the context and the caller. If
// they are same then proceed to step 10. Else proceed to step 2.
//
// 2. Check if the caller is the global zone (0). If so, then
// proceed to step 10. Else proceed to step 3.
//
// 3. Check if the caller is a native local zone (2). If so, then
// proceed to step 10. Else proceed to step 4.
//
// 4. Check if all zones are allowed to unrestrictedly operate
// on the context. If so, the proceed to step 10, else go to next
// step 5.
//
// 5. Check if only read permission is allowed from all zones,
// and the operation is a read operation. If so then proceed to
// step 10, else fail the operation.
//
//
// 10. Check the appropriate operation permission. If the check
// passes go pass the operation. If not fail the operation.
//
// ----------------- Operation Permissions ------------------
// NS_BIND_PERM:
// 	Allow bind operation for objects in this context
// NS_REBIND_PERM:
// 	Allow rebind operation for objects in this context.
// NS_BIND_CTX_PERM:
// 	Allow the binding of an object with the given context
// 	in this context.
// NS_REBIND_CTX_PERM:
// 	Allow the rebinding of an object with the given context
// 	in this context.
// NS_RESOLVE_PERM:
// 	Allow resolving/looking up of a context or object
// 	in this context.
// NS_UNBIND_PERM:
// 	Allow unbinding an object in this context
// NS_BIND_NEW_CTX_PERM:
// 	Allow sub context creation in this context
// NS_REBIND_EX_CTX_PERM:
// 	Allow the rebinding of an existing sub context in
// 	this context.
// NS_UNBIND_EX_CTX_PERM:
// 	Allow unbinding an existing context from this context
//
//
#define	NS_BIND_PERM			0x00000001
#define	NS_REBIND_PERM			0x00000002
#define	NS_BIND_CTX_PERM		0x00000004
#define	NS_REBIND_CTX_PERM		0x00000008
#define	NS_RESOLVE_PERM			0x00000010
#define	NS_UNBIND_PERM			0x00000020
#define	NS_BIND_NEW_CTX_PERM		0x00000040
#define	NS_REBIND_EX_CTX_PERM		0x00000080
#define	NS_UNBIND_EX_CTX_PERM		0x00000100

//
// -------------- Zone Access Permissions ----------------
//
#define	NS_ALLOW_LOCAL_ZONE_PERM	0x00010000
#define	NS_ALLOW_ALL_ZONE_PERM		0x00020000
#define	NS_ALLOW_ALL_ZONE_READ_PERM	0x00040000

//
// ------------ Common Permissions -----------------------
//
#define	NS_ALLOW_ALL_OPP_PERM		(NS_BIND_PERM | \
	NS_REBIND_PERM | NS_BIND_CTX_PERM | NS_REBIND_CTX_PERM | \
	NS_RESOLVE_PERM | NS_UNBIND_PERM | NS_BIND_NEW_CTX_PERM | \
	NS_REBIND_EX_CTX_PERM | NS_UNBIND_EX_CTX_PERM)

#define	NS_ALLOW_ALL_PERM		0xffffffff

//
// Declaration of naming context internal implementation object
//
class naming_context_ii {
public:
	naming_context_ii(naming::naming_context *ncp,
	    bool is_replicated,
	    bool lf = false,
	    uint32_t perm = NS_ALLOW_ALL_OPP_PERM,
	    uint32_t own = GLOBAL_ZONEID);

	virtual ~naming_context_ii();

	static void context_lock();

	static void context_unlock();

#ifdef _KERNEL_ORB
	static void init_namespace(naming_context_ii *root_ctx,
	    const int ns_type, Environment &env);
	static void create_virtual_root(naming_context_ii *root_ctx,
	    const int ns_type, Environment &env);
#endif
	void bind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void rebind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void bind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	void rebind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	CORBA::Object_ptr resolve(const char *nm, Environment& _env);

	void unbind(const char *nm, Environment& _env);

	naming::naming_context_ptr bind_new_context(const char *nm,
	    Environment& _env);

	naming::naming_context_ptr bind_new_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	naming::naming_context_ptr rebind_existing_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	bool unbind_existing_context_v1(const char *nm, Environment& _env);

	void list(uint32_t how_many, _Ix_T_out(naming::binding_list) bl,
		naming::binding_iterator_out bi, Environment& _env);

	//
	// New functions introduced in the zone cluster project
	//

#ifdef _KERNEL_ORB
	//
	// Checkpoint helper routines
	//
	void ckpt_binding(const char *nm, CORBA::Object_ptr obj,
		int32_t bind_type, bool is_rebind, Environment &env);

	void ckpt_context(naming::naming_context_ptr ctx, const char *nm,
	    Environment &env);

	void ckpt_context_v1(naming::naming_context_ptr ctx, const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment &env);

	void ckpt_unbind(const char *nm, Environment &env);

	static void ckpt_unbound_context(naming::naming_context_ptr ctx);

	static void ckpt_unbound_context_v1(naming::naming_context_ptr ctx,
	    bool lf, uint32_t perm, uint32_t own);

	void dump_state(repl_ns::ns_replica_ptr ckpt, Environment &_env);

	//
	// Return a CORBA pointer (no CORBA::release required)
	// to the checkpoint interface.
	//
	virtual repl_ns::ns_replica_ptr get_ckpt() = 0;

#endif

	//
	// The kernel security check
	//
	bool has_permission(uint32_t op_perm, uint32_t op_type,
	    Environment &env);

	void set_nameserver_type(int ns_type);
	int get_nameserver_type();

	void set_look_further(bool status);
	bool get_look_further();

	uint32_t get_owner();
	void set_owner(uint32_t own);

	uint32_t get_permission();
	void set_permission(uint32_t perm);

	bool get_replicated();
	binding_store* get_store();

protected:
	// Disallowed operations
	naming_context_ii();

private:
	static os::rwlock_t ctx_lock;

	//
	// To be used for accessing the security
	// and owner fields only
	//
	os::rwlock_t  sec_lock;

	bool replicated;

	// Soft reference to the implementation
	naming::naming_context *nctxp;

	// pointer to the actual store where bindings of the
	// context reside.
	binding_store  *b_store;

	//
	// The name server type (LOCAL_NS, GLOBAL_NS, NONE_NS)
	//
	int nameserver_type;

	//
	// 1. This is set to false  for context 1 of the
	// local name server and the global name server.
	// 2. This is set to false for the real root of the
	// local and global name server.
	// 3. All sub contexts should have the value as
	// false.
	// 3. All zone cluster contexts should have this value
	// as true.
	// 4. The context 0 of the global zone should have it
	// as true for both the local and global nameserver.
	//
	bool look_further;

	//
	// Permissions for this particular object/context
	// in the name server
	//
	uint32_t	ns_perm;

	//
	// The original cluster which created this
	// object/context
	//
	uint32_t	owner;


	nc_error lookup(const char *nm, int process_last_comp,
	    binding *&ret_binding, char *&rest_of_name);

	//
	// For looking up in context c1 (ALL_CLUSTER) of the
	// local name server
	//
	nc_error lookup_local_c1(const char *nm, int process_last_comp,
	    binding *&ret_binding, char *&rest_of_name);

	binding::binding_type get_context_type(naming::naming_context_ptr obj);
};


#ifdef _KERNEL_ORB

#include <repl/service/replica_tmpl.h>
#include <nslib/repl_ns_server.h>


// Forward declaration
class repl_ns_server;

//
//  class  naming_context_repl_impl:  This class should be used to
//	   instantiate a replicated naming_context that does not
//	   require a "well-known" door-id.
//
class naming_context_repl_impl :
	public mc_replica_of<naming::naming_context>,
	private naming_context_ii {
public:
	naming_context_repl_impl();

	naming_context_repl_impl(repl_ns_server *serverp,
	    bool lf = false,
	    uint32_t perm = NS_ALLOW_ALL_OPP_PERM,
	    uint32_t own = GLOBAL_ZONEID);

	naming_context_repl_impl(naming::naming_context_ptr primary_obj,
	    bool lf = false,
	    uint32_t perm = NS_ALLOW_ALL_OPP_PERM,
	    uint32_t own = GLOBAL_ZONEID);

	virtual ~naming_context_repl_impl();

	void _unreferenced(unref_t arg);

	void bind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void rebind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void bind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	void rebind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	CORBA::Object_ptr resolve(const char *nm, Environment& _env);

	void unbind(const char *nm, Environment& _env);

	naming::naming_context_ptr bind_new_context(const char *nm,
	    Environment& _env);

	naming::naming_context_ptr bind_new_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	naming::naming_context_ptr rebind_existing_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	bool unbind_existing_context_v1(const char *nm, Environment& _env);

	void list(uint32_t how_many, _Ix_T_out(naming::binding_list) bl,
	    naming::binding_iterator_out bi, Environment& _env);

	void cz_set_nameserver_type(int ns_type, Environment& _env);
	void cz_set_look_further(bool status, Environment& _env);

	void set_nameserver_type(int ns_type);
	int get_nameserver_type();

	void set_look_further(bool status);
	bool get_look_further();

	uint32_t get_owner();
	void set_owner(uint32_t own);

	uint32_t get_permission();
	void set_permission(uint32_t perm);

	bool get_replicated();
	binding_store* get_store();
	//
	// Return a CORBA pointer (no CORBA::release required)
	// to the checkpoint interface.
	//
	repl_ns::ns_replica_ptr get_ckpt();

	// Checkpoint accessor function.
	repl_ns::ns_replica_ptr	get_checkpoint();
};

//
// Class to save info about unbound contexts used while dumping
// state to secondaries
//
class dump_info {
public:
	static void add_unbound_ctx(naming_context_repl_impl *rimpl);
	static void add_unbound_ctx(CORBA::Object_ptr cobj);
	static void delete_unbound_ctx(naming_context_repl_impl *rimpl);
	static void dump_unbound_list(repl_ns::ns_replica_ptr ckpt,
	    Environment& _env);
	static void cleanup_unbound_list();

	struct unbnd_ctx {
		naming_context_repl_impl *rimpl;
		struct unbnd_ctx *next;
	};
private:
	static void add_to_list(naming_context_repl_impl *rimpl);
};

#endif


//
//  class  naming_context_impl:  This class should be used to
//	   instantiate a naming_context that does not require a
//	   "well-known" door-id.
//
class naming_context_impl :
	public McServerof<naming::naming_context>, private naming_context_ii {
public:
	naming_context_impl(bool lf = false,
	    uint32_t perm = NS_ALLOW_ALL_OPP_PERM,
	    uint32_t own = GLOBAL_ZONEID);

	virtual ~naming_context_impl();

	void _unreferenced(unref_t arg);

	void bind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void rebind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void bind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	void rebind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	CORBA::Object_ptr resolve(const char *nm, Environment& _env);

	void unbind(const char *nm, Environment& _env);

	naming::naming_context_ptr bind_new_context(const char *nm,
	    Environment& _env);

	naming::naming_context_ptr bind_new_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	naming::naming_context_ptr rebind_existing_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	bool unbind_existing_context_v1(const char *nm, Environment& _env);

	void list(uint32_t how_many, _Ix_T_out(naming::binding_list) bl,
	    naming::binding_iterator_out bi, Environment& _env);

	void cz_set_nameserver_type(int ns_type, Environment& _env);
	void cz_set_look_further(bool status, Environment& _env);

	void set_nameserver_type(int ns_type);
	int get_nameserver_type();

	void set_look_further(bool status);
	bool get_look_further();

	uint32_t get_owner();
	void set_owner(uint32_t own);

	uint32_t get_permission();
	void set_permission(uint32_t perm);

	bool get_replicated();
	binding_store* get_store();
#ifdef _KERNEL_ORB
	//
	// Return a CORBA pointer (no CORBA::release required)
	// to the checkpoint interface.
	//
	repl_ns::ns_replica_ptr get_ckpt();
#endif

};


//
//  class  naming_context_root:  This class should be used to
//	   instantiate a naming_context that requires a "well-known"
//	   door-id (eg: machine (local) name server).  This implementation
//	   class will not receive any unreference calls.
//
class naming_context_root :
	public McNoref<naming::naming_context>, private naming_context_ii {
public:

	naming_context_root();

#ifdef _KERNEL_ORB
	naming_context_root(xdoor_id xd,
	    bool lf = false,
	    uint32_t perm = NS_ALLOW_ALL_OPP_PERM,
	    uint32_t own = GLOBAL_ZONEID);
#endif
	virtual ~naming_context_root();

	// static void init_namespace(naming_context_root *);

	void bind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void rebind(const char *nm, CORBA::Object_ptr obj, Environment& _env);

	void bind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	void rebind_context(const char *nm, naming::naming_context_ptr obj,
	    Environment& _env);

	CORBA::Object_ptr resolve(const char *nm, Environment& _env);

	void unbind(const char *nm, Environment& _env);

	naming::naming_context_ptr bind_new_context(const char *nm,
	    Environment& _env);

	naming::naming_context_ptr bind_new_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	naming::naming_context_ptr rebind_existing_context_v1(const char *nm,
	    bool lf, uint32_t perm, uint32_t own, Environment& _env);

	bool unbind_existing_context_v1(const char *nm, Environment& _env);

	void list(uint32_t how_many, _Ix_T_out(naming::binding_list) bl,
	    naming::binding_iterator_out bi, Environment& _env);

	void cz_set_nameserver_type(int ns_type, Environment& _env);
	void cz_set_look_further(bool status, Environment& _env);
#ifdef _KERNEL_ORB
	//
	// Return a CORBA pointer (no CORBA::release required)
	// to the checkpoint interface.
	//
	repl_ns::ns_replica_ptr get_ckpt();

#endif
	void set_nameserver_type(int ns_type);
	int get_nameserver_type();

	void set_look_further(bool status);
	bool get_look_further();

	uint32_t get_owner();
	void set_owner(uint32_t own);

	uint32_t get_permission();
	void set_permission(uint32_t perm);

	bool get_replicated();
	binding_store* get_store();

};

#include <nslib/naming_context_impl_in.h>

#endif	/* _NAMING_CONTEXT_IMPL_H */
