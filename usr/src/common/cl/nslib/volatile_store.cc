//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)volatile_store.cc	1.26	08/10/13 SMI"


#include <nslib/store.h>
#include <nslib/ns_lib.h>

//
// Lint is throwing message 1776 for all NS_PRINTF
// and print_exception functions. Hence supressing
// it for the whole file.
//
// Message:
// Converting string literals to char * is deprecated
// (Context)  -- A string literal, according to Standard C++
// is typed an array of const char.  This message is issued
// when such a literal is assigned to a non-const pointer.
//

//lint -e1776



volatile_store::volatile_store(naming::naming_context *ncp)
{
	blist_hdrp = NULL;
	bindings_count = 0;
	ctxp = ncp;
}  

volatile_store::~volatile_store()
{
	bnode *blistp, *next_blistp;

	NS_PRINTF(NS_STORE, ("volatile_store: destructor\n")); //lint !e1551

	blistp = blist_hdrp;
	for (;;) {
		if (blistp == NULL)
			break;
		blistp->vbinding->release(); //lint !e1551
		bindings_count--;
		next_blistp = blistp->next;
		delete blistp;
		blistp = next_blistp;
	}
	blist_hdrp = NULL;
	ctxp = NULL;
}

// Returns a soft reference to the naming context obj
naming::naming_context *
volatile_store::get_ctx()
{
	return (ctxp);
}

//
// Find the binding with the given name and return it after
// incrementing the reference count.
//
volatile_binding *
volatile_store::find_binding(char *name)
{
	bnode *blistp;
	volatile_binding *vb = NULL;
	char *binding_name;

	for (blistp = blist_hdrp; blistp != NULL; blistp = blistp->next) {
		binding_name = blistp->vbinding->get_name();
		if (os::strcmp(binding_name, name) == 0) {
			vb = blistp->vbinding;
			// Return a held binding
			vb->hold();
			break;
		}
	}
	return (vb);
} //lint !e818

//
// Add the specified binding to the store and increment bindings_count.
//
void
volatile_store::add_binding(volatile_binding *vb)
{
	struct bnode *blistp = new bnode;

	NS_PRINTF(NS_STORE,
	    ("volatile_store: add_binding blistp: %p\n", blistp));

	blistp->vbinding = vb;
	blistp->next = blist_hdrp;
	blist_hdrp = blistp;
	bindings_count++;
}

//
// Retrieve binding associated with the specified name and return
// it to the caller.
//
binding *
volatile_store::retrieve_binding(char *name)
{
	volatile_binding *vb = find_binding(name);
	return (vb);
}

//
// Determine if a binding with the specified name exists.  If the
// binding exists, update it with the new information.  If it doesn't
// exist, allocate the appropriate type of volatile binding and add it
// to the store.
//
nc_error
volatile_store::store_binding(char *name, binding::binding_type bind_type,
    CORBA::Object_ptr obj, bool rebind)
{
	NS_PRINTF(NS_STORE,
	    ("volatile_store: store_binding name: %s this: %p\n", name, this));

	volatile_binding *result = find_binding(name);
	if (result && !rebind) {
		result->release();
		return (nc_already_bound);
	}

	volatile_binding *new_binding = NULL;

	switch (bind_type) {
	    case binding::bt_local_ctx:
	    case binding::bt_remote_ctx:
	    case binding::bt_obj:
		if (result == NULL) {
			new_binding =
			    new volatile_binding(name, obj, bind_type);
		}
		break;
	    case binding::bt_none:
	    default:
		{
		//
		// Should never happen.
		//
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_NAMING_SERVICE_TAG,
		    NULL, NULL);
		//
		// SCMSGS
		// @explanation
		// During a name server binding store an unknown binding type
		// was encountered.
		// @user_action
		// No action required. This is informational message.
		//
		(void) syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "store_binding: <%s> bad bind type <%d>", name, bind_type);
		}
		return (nc_cannot_proceed);
	}

	if (result) {
		result->update_binding(name, bind_type, obj);
		result->release();
	} else {
		add_binding(new_binding);
	}

	return (nc_ok);
}

//
// Will drop the binding passed as a parameter
// from the bstore. This function will not free
// any memory. If the binding doesn't exist in
// the store then "nc_missing_node" error
// is returned.
//
nc_error
volatile_store::drop_binding(const char *name)
{
	char *binding_nm;
	bnode *blistp, **blistpp;
	nc_error error = nc_missing_node;

	for (blistpp = &blist_hdrp; *blistpp != NULL; blistpp = &blistp->next) {
		blistp = *blistpp;
		binding_nm = blistp->vbinding->get_name();
		if (os::strcmp(name, binding_nm) == 0) {
			*blistpp = blistp->next;
			bindings_count--;
			error = nc_ok;
			break;
		}
	}
	return (error);
}

//
// Delete the binding with the specified name from the store
// If the binding doesn't exist in the store then "nc_missing_node"
// error is returned.
//
nc_error
volatile_store::delete_binding(const char *name)
{
	char *binding_nm;
	bnode *blistp, **blistpp;
	nc_error error = nc_missing_node;

	for (blistpp = &blist_hdrp; *blistpp != NULL; blistpp = &blistp->next) {
		blistp = *blistpp;
		binding_nm = blistp->vbinding->get_name();
		if (os::strcmp(name, binding_nm) == 0) {
			*blistpp = blistp->next;
			blistp->vbinding->release();
			delete blistp;
			bindings_count--;
			error = nc_ok;
			break;
		}
	}
	return (error);
}

//
// Delete all the bindings from the store
// Returns nc_ok as successful.
//
nc_error
volatile_store::delete_all_binding()
{
	bnode *blistp, **blistpp;
	nc_error error = nc_ok;

	blistpp = &blist_hdrp;
	while (*blistpp != NULL) {
		blistp = *blistpp;
		*blistpp = blistp->next;
		blistp->vbinding->release();
		delete blistp;
		bindings_count--;
	}
	return (error);
}

//
// Determine if a binding with the specified name exists.  If it
// doesn't, allocate a new volatile binding for the context and add
// it to the volatile store.
//
nc_error
volatile_store::bind_new_context(char *name, CORBA::Object_ptr nctx,
    binding_store *store, bool force)
{
	volatile_binding *result = find_binding(name);

	if (result != NULL && !force) {
		result->release();
		return (nc_already_bound);
	}

	if (result != NULL) {
		result->update_ctx_binding(name, nctx, store);
		result->release();
	} else {
		result = new volatile_binding(name, nctx, store);
		add_binding(result);
	}

	return (nc_ok);
}

nc_error
volatile_store::rebind_existing_context(char *name, CORBA::Object_ptr nctx,
    binding_store *store, bool force)
{
	//
	// Find the binding you are looking for
	// It will return a held volatile_binding
	//
	volatile_binding *vb_ctx = find_binding(name);

	if (vb_ctx == NULL) {
		if (force) {
			//
			// The binding does not exist. Create
			// a binding and return a held binding
			//
			vb_ctx = new volatile_binding(name, nctx, store);
			add_binding(vb_ctx);
			return (nc_ok);
		} else {
			//
			// The binding does not exist and the
			// "force" flag is not set. Hence,
			// return error.
			//
			return (nc_invalid_name);
		}
	}

	//
	// The binding context exists. Delete the
	// child bindings of the traget context first
	// and then rebind the target binding context.
	// As a result of the rebind operation, the child
	// bindings of the target bindings will not be
	// rebinded. They will cease to exist.
	//

	//
	// vb_ctx is the context which you want to rebind
	// Traverse the bindings in this contex and delete
	// them before rebinding vb_ctx.
	//
	binding_store *child_store = vb_ctx->get_store_obj();
	ASSERT(child_store);
	//
	// Ignoring the return value as in the
	// current implementation, the function
	// will always return NC_OK
	//
	(void) child_store->delete_all_binding();

	//
	// Now rebind the binding context.
	//
	vb_ctx->update_ctx_binding(name, nctx, store);
	vb_ctx->release();

	return (nc_ok);
}

//
// Determine if a binding with the specified name exists.  If it
// does not exist, return gracefully or return error, depending
// on the "force" flag. If the binding exists, then first unbind
// its child bindings from the volatile_store of the target binding
// and then unbind the target binding.
//
nc_error
volatile_store::unbind_existing_context(char *name, bool force)
{
	nc_error		error;

	//
	// Find the binding you are looking for
	// It will return a held volatile_binding
	//
	volatile_binding *vb_ctx = find_binding(name);

	//
	// If the context does not exist and the
	// force flag is set, then return success.
	// i.e Ignore the fact that the context
	// never existed.
	// If the context does not exist and the
	// force falg is not set, then return
	// error thatthenode never existed.
	//
	if (vb_ctx == NULL) {
		if (force) {
			return (nc_ok);
		} else {
			return (nc_invalid_name);
		}
	}

	//
	// vb_ctx is the context which you want to delete
	// Traverse the bindings in this contex and delete
	// them before releasing vb_ctx.
	//

	binding_store *child_store = vb_ctx->get_store_obj();
	ASSERT(child_store);
	//
	// Ignoring the return value as in the
	// current implementation, the function
	// will always return NC_OK
	//
	(void) child_store->delete_all_binding();

	//
	// Release the target binding since it was
	// incremented in find_binding
	//
	vb_ctx->release();

	//
	// Now delete the target binding. This is safe now
	// as all the child bindings have been deleted
	// The assumption is that there is only one level
	// of child bindings.
	//
	error = delete_binding(name);

	return (error);
}

//
// Return the count of all bindings in this store.
//
uint32_t
volatile_store::get_binding_count()
{
	return (bindings_count);
}


//
// Allocate memory and return all bindings in this store after
// incrementing their reference counts.  The caller is responsible
// for freeing up the allocated memory.
//
nc_error
volatile_store::get_all_bindings(binding **&bindings, uint32_t tot_bindings)
{
	uint32_t i;
	bnode *blistp;

	//
	// Initialize each entry in the array to hold all the
	// bindings in this store.
	//
	for (i = 0, blistp = blist_hdrp;
	    blistp != NULL && i < tot_bindings; blistp = blistp->next, i++) {
		//
		// Return a held binding
		//
		blistp->vbinding->hold();
		bindings[i] = blistp->vbinding;
	}
	return (nc_ok);
}

#ifdef _KERNEL_ORB

#include <h/repl_ns.h>
#include "ns_trans_states.h"
#include "naming_context_impl.h"

//lint -e1038
volatile_store::bnode *bindings_headp = NULL;
volatile_store::bnode *bindings_tailp = NULL;
//lint +e1038
int dump_bindings_count;

int
volatile_store::dump_all_bindings(repl_ns::ns_replica_ptr ckpt,
    Environment &_env)
{
	int error = 0;
	bnode *blistp;

	for (blistp = blist_hdrp; blistp != NULL; blistp = blistp->next) {
		volatile_binding *vb = blistp->vbinding;
		ASSERT(vb != NULL);
		binding::binding_type bind_type = vb->get_binding_type();

		naming::naming_context_ptr lctx = ((naming_context_repl_impl *)
		    this->get_ctx())->get_objref(); //lint !e1774

		if (bind_type != binding::bt_local_ctx) {
			NS_PRINTF(NS_DUMP_STATE,
			    ("dump_all_bindings: ckpt_binding %s\n",
			    vb->get_name()));

			ckpt->ckpt_binding(lctx, vb->get_name(),
			    vb->get_obj(), bind_type, true, _env);
			if (_env.exception() != NULL) {
				CORBA::release(lctx);
				_env.clear();
				error = 1;
				break;
			}
		} else {
			//lint -e1774
			naming::naming_context_ptr ctx =
			    (naming::naming_context_ptr) vb->get_obj();
			//lint +e1774

			if (repl_ns::ns_replica::_version(ckpt) == 0) {
				NS_PRINTF(NS_DUMP_STATE,
				    ("dump_all_bindings: ckpt_context %s\n",
				    vb->get_name()));
				ckpt->ckpt_context(
				    lctx, ctx, vb->get_name(), _env);
			} else {
				//lint -e1774
				bool look_further = ((naming_context_repl_impl*)
				    (vb->get_store_obj()->get_ctx()))
				    ->get_look_further();
				uint32_t permission =
				    ((naming_context_repl_impl*)
				    (vb->get_store_obj()->get_ctx()))
				    ->get_permission();
				uint32_t owner = ((naming_context_repl_impl*)
				    (vb->get_store_obj()->get_ctx()))
				    ->get_owner();
				//lint +e1774
				NS_PRINTF(NS_DUMP_STATE,
				    ("dump_all_bindings: "
				    "ckpt_context %s lf %d perm 0x%x own %d\n",
				    vb->get_name(), look_further,
				    permission, owner));
				ckpt->ckpt_context_v1(lctx, ctx, vb->get_name(),
				    look_further, permission, owner, _env);
			}

			if (_env.exception() != NULL) {
				CORBA::release(lctx);
				_env.clear();
				error = 1;
				break;
			}

			//
			// Allocate a new bnode structure and add it
			// to the end of the list.
			//
			struct bnode *dlistp = new bnode;

			dlistp->vbinding = vb;
			dlistp->next = NULL;

			if (bindings_headp == NULL)
				bindings_headp = dlistp;
			//
			// Now set the tail pointer appropriately
			//
			if (bindings_tailp != NULL)
				bindings_tailp->next = dlistp;
			bindings_tailp = dlistp;
		}
		dump_bindings_count++;
		CORBA::release(lctx);
	}

	return (error);
}

void
volatile_store::cleanup_dump_state()
{
	struct bnode *nlistp;

	// Now, delete all bnode structures
	while (bindings_headp != NULL) {
		nlistp = bindings_headp->next;
		delete bindings_headp;
		bindings_headp = nlistp;
	}
	ASSERT(bindings_headp == NULL);
	bindings_tailp = NULL;
} //lint !e1762

void
volatile_store::dump_state(repl_ns::ns_replica_ptr ckpt, Environment &_env)
{
	int error;

	NS_PRINTF(NS_DUMP_STATE,
	    ("dump_state: root context: %d num bindings\n", bindings_count));

	ASSERT(bindings_headp == NULL && bindings_tailp == NULL);
	dump_bindings_count = 0;

	error = this->dump_all_bindings(ckpt, _env);
	if (error != 0) {
		cleanup_dump_state();
		return;
	}

	// Process the dump binding list which represents local contexts

	struct bnode *listp = bindings_headp;
	while (listp != NULL) {
		volatile_binding *vb = listp->vbinding;

		NS_PRINTF(NS_DUMP_STATE,
		    ("dump_state: dump bindings for context: %s\n",
		    vb->get_name()));
		ASSERT(vb != NULL);

		//lint -e1774
		volatile_store *nstore = (volatile_store *)vb->get_store_obj();
		//lint +e1774
		error = nstore->dump_all_bindings(ckpt, _env);
		if (error != 0)
			break;

		listp = listp->next;
	}

	//
	// Now, dump the unbound context list
	//
	dump_info::dump_unbound_list(ckpt, _env);

	cleanup_dump_state();

	NS_PRINTF(NS_DUMP_STATE,
	    ("dump_state: dumped: %d bindings\n", dump_bindings_count));
}
#endif

volatile_store::bnode*
volatile_store::get_blist()
{
	return (blist_hdrp);
}

//lint +e1776
