/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _REPL_NS_SERVER_H
#define	_REPL_NS_SERVER_H

#pragma ident	"@(#)repl_ns_server.h	1.18	08/10/22 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include "naming_context_impl.h"
#include <orb/object/adapter.h>
#include <h/repl_ns.h>
#include <h/replica.h>
#include <repl/service/replica_tmpl.h>

//
// States which define whether a protocl
// switch is required or not.
//
// REPL_NS_CONVERSION_INIT:
// 	The initial state. At this point in time, we
// 	are yet to figure out what state we should be
// 	in i.e. The 3 states below
//
// REPL_NS_CONVERSION_NOT_REQUIRED:
// 	The state in which the highest version of the
// 	name server software is running on all nodes
//
// REPL_NS_CONVERSION_REQUIRED:
// 	In a rolling upgrade scenario, on a
// 	scversions commit, conversion from old to
// 	new format needs to be done.
//
// REPL_NS_CONVERSION_DONE:
// 	In a rolling upgrade scenario, the conversion
// 	from old to new version of name server software
// 	has already been done.
//
#define	REPL_NS_CONVERSION_INIT			0
#define	REPL_NS_CONVERSION_NOT_REQUIRED		1
#define	REPL_NS_CONVERSION_REQUIRED		2
#define	REPL_NS_CONVERSION_DONE			3

//
// This data structure is used for building a table for supporting the
// mapping from version protocol spec file version number to the various
// IDL interface versions it represents. The table will be a two
// dimensional array indexed by major/minor vp version.
//
typedef struct {
	int naming_context_vers; // Naming Interface naming::naming_context
	int ns_replica_vers;	 // Checkpoint  Interface repl_ns::ns_replica
} ns_version_map_t;

const int	NS_VP_MAX_MAJOR = 2;
const int	NS_VP_MAX_MINOR = 1;

// The current major and minor number for nameserver
const int	NS_VP_MAJOR = 2;
const int	NS_VP_MINOR = 0;


//lint -e1748

// Forward declaration
class naming_context_repl_impl;

class repl_ns_server : public repl_server<repl_ns::ns_replica> {
public:
	repl_ns_server(const char *nm, const char *id);

	~repl_ns_server();

	static repl_ns_server *get_repl_server();

	static void initialize(char *nm);

	// Required functions for replica framework
	void become_secondary(Environment &);

	void add_secondary(replica::checkpoint_ptr sec_chkpt, const char *,
	    Environment &_environment);

	void remove_secondary(const char *, Environment &);

	void freeze_primary_prepare(Environment &);

	void freeze_primary(Environment &);

	void unfreeze_primary(Environment &);

	void become_primary(const replica::repl_name_seq &, Environment &);

	void become_spare(Environment &);

	void shutdown(Environment &);

	CORBA::Object_ptr get_root_obj(Environment &);

	// Checkpoint operations
	void ckpt_root_context(naming::naming_context_ptr robj,
	    Environment &);

	void ckpt_binding(naming::naming_context_ptr pctx, const char *nm,
	    CORBA::Object_ptr obj, int32_t bind_type, bool rebind,
	    Environment &env);

	void ckpt_context(naming::naming_context_ptr pctx,
	    naming::naming_context_ptr ctx, const char *nm, Environment &env);

	void ckpt_unbind(naming::naming_context_ptr pctx, const char *nm,
	    Environment &env);

	void ckpt_unbound_context(naming::naming_context_ptr pctx,
	    Environment &env);

	//
	// Checkpoint operations introduced as a part of
	// the zone cluster project
	//

	void ckpt_root_context_v1(naming::naming_context_ptr robj,
	    bool lf,
	    uint32_t perm,
	    uint32_t own,
	    Environment &);

	void ckpt_context_v1(naming::naming_context_ptr pctx,
	    naming::naming_context_ptr ctx,
	    const char *nm,
	    bool lf,
	    uint32_t perm,
	    uint32_t own,
	    Environment &env);

	void ckpt_unbound_context_v1(naming::naming_context_ptr pctx,
	    bool lf,
	    uint32_t perm,
	    uint32_t own,
	    Environment &env);

	// Checkpoint accessor function
	repl_ns::ns_replica_ptr	get_checkpoint_repl_ns_ns_replica();

	void ckpt_service_version(unsigned short, unsigned short,
	    bool convert, Environment &);

	//
	// To be used only in upgrade_callback to rebind the
	// real root of the global name server in the local
	// name server on the secondary nodes
	//
	void ckpt_root_rebind(naming::naming_context_ptr robj,
	    Environment &);

	//
	// The callback function invoked by the version manager
	// during a rolling upgrade commit.
	//
	void upgrade_callback(const version_manager::vp_version_t &,
	    Environment &);

	// Set the initial version number
	void set_init_version(const version_manager::vp_version_t &);

	static version_manager::vp_version_t get_current_version();
	static version_manager::vp_version_t get_pending_version();

private:
	//
	// Disallowed operations
	//
	repl_ns_server();

	//
	// These are both protected by version_lock.
	// current_version is the versioned protocol number the version
	// manager has told us we should be running as.
	// pending_version is set when a secondary gets a callback before
	// the primary sends a checkpoint so a failover during upgrade
	// commit is processed correctly.
	//
	static version_manager::vp_version_t	current_version;
	static version_manager::vp_version_t	pending_version;

	//
	// Provides locking between calls to upgrade callbacks and
	// become_primary, where we need to update the current
	// version.
	//
	static os::rwlock_t    version_lock;

	static int	conversion_state;

	naming_context_repl_impl *root_ctxp;

	// Checkpoint proxy
	repl_ns::ns_replica_ptr	_ckpt_proxy;
};

class ns_upgrade_callback:
	public McServerof<version_manager::upgrade_callback>
{
public:
	ns_upgrade_callback(repl_ns_server&);
	~ns_upgrade_callback();

	void _unreferenced(unref_t unref);

	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &);

private:
	repl_ns_server	&repl_ns_server_ref;
};

//lint +e1748

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _REPL_NS_SERVER_H */
