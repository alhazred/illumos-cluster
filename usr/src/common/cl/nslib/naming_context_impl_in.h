/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  naming_context_impl_in.h
 *
 */

#ifndef _NAMING_CONTEXT_IMPL_IN_H
#define	_NAMING_CONTEXT_IMPL_IN_H

#pragma ident	"@(#)naming_context_impl_in.h	1.14	08/07/05 SMI"

//
// naming_context_ii
//
inline void
naming_context_ii::context_lock()
{
	ctx_lock.wrlock();
}

inline void
naming_context_ii::context_unlock()
{
	ctx_lock.unlock();
}

inline uint32_t
naming_context_ii::get_owner()
{
	return (owner);
}

inline void
naming_context_ii::set_owner(uint32_t own)
{
	owner = own;
}

inline uint32_t
naming_context_ii::get_permission()
{
	return (ns_perm);
}

inline void
naming_context_ii::set_permission(uint32_t perm)
{
	ns_perm = perm;
}

inline bool
naming_context_ii::get_replicated()
{
	return (replicated);
}

inline binding_store*
naming_context_ii::get_store()
{
	return (b_store);
}


//
// naming_context_impl
//
inline
naming_context_impl::naming_context_impl(bool lf, uint32_t perm,
    uint32_t own): naming_context_ii(this, false, lf, perm, own)
{
	//lint -e1506 Actually calling a virtual function in the base class
	// and not the corresponding function in the derived class.
// tdas	_handler()->set_cookie((void *)this);
	//lint +e1506
}

inline
naming_context_impl::~naming_context_impl()
{
}

inline void
naming_context_impl::bind(const char *nm, CORBA::Object_ptr obj,
    Environment& _env)
{
	naming_context_ii::bind(nm, obj, _env);
}


inline void
naming_context_impl::rebind(const char *nm, CORBA::Object_ptr obj,
    Environment& _env)
{
	naming_context_ii::rebind(nm, obj, _env);
}

inline void
naming_context_impl::bind_context(const char *nm,
    naming::naming_context_ptr obj, Environment& _env)
{
	naming_context_ii::bind_context(nm, obj, _env);
}

inline void
naming_context_impl::rebind_context(const char *nm,
    naming::naming_context_ptr obj, Environment& _env)
{
	naming_context_ii::rebind_context(nm, obj, _env);
}

inline void
naming_context_impl::unbind(const char *nm, Environment& _env)
{
	naming_context_ii::unbind(nm, _env);
}

inline naming::naming_context_ptr
naming_context_impl::bind_new_context(const char *nm, Environment& _env)
{
	return (naming_context_ii::bind_new_context(nm, _env));
}

inline naming::naming_context_ptr
naming_context_impl::bind_new_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment& _env)
{
	return (naming_context_ii::bind_new_context_v1(nm,
	    lf, perm, own, _env));
}

inline naming::naming_context_ptr
naming_context_impl::rebind_existing_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment& _env)
{
	return (naming_context_ii::rebind_existing_context_v1(nm,
	    lf, perm, own, _env));
}

inline bool
naming_context_impl::unbind_existing_context_v1(const char *nm,
    Environment &_env)
{
	return (naming_context_ii::unbind_existing_context_v1(nm, _env));
}

inline void
naming_context_impl::list(uint32_t how_many, _Ix_T_out(naming::binding_list) bl,
    naming::binding_iterator_out bi, Environment& _env)
{
	naming_context_ii::list(how_many, bl, bi, _env);
}

inline void
naming_context_impl::cz_set_nameserver_type(int ns_type, Environment& _env)
{
	set_nameserver_type(ns_type);
}

inline void
naming_context_impl::set_nameserver_type(int ns_type)
{
	naming_context_ii::set_nameserver_type(ns_type);
}

inline int
naming_context_impl::get_nameserver_type()
{
	return (naming_context_ii::get_nameserver_type());
}

inline void
naming_context_impl::cz_set_look_further(bool status, Environment& _env)
{
	set_look_further(status);
}

inline void
naming_context_impl::set_look_further(bool status)
{
	naming_context_ii::set_look_further(status);
}

inline bool
naming_context_impl::get_look_further()
{
	return (naming_context_ii::get_look_further());
}

inline uint32_t
naming_context_impl::get_owner()
{
	return (naming_context_ii::get_owner());
}

inline void
naming_context_impl::set_owner(uint32_t own)
{
	naming_context_ii::set_owner(own);
}

inline uint32_t
naming_context_impl::get_permission()
{
	return (naming_context_ii::get_permission());
}

inline void
naming_context_impl::set_permission(uint32_t perm)
{
	naming_context_ii::set_permission(perm);
}

inline bool
naming_context_impl::get_replicated()
{
	return (naming_context_ii::get_replicated());
}

inline binding_store*
naming_context_impl::get_store()
{
	return (naming_context_ii::get_store());
}


#ifdef _KERNEL_ORB

inline repl_ns::ns_replica_ptr
naming_context_impl::get_ckpt()
{
	ASSERT(0);
	return (repl_ns::ns_replica::_nil());
}
#endif


//
// naming_context_repl_impl
//

#ifdef _KERNEL_ORB

inline
naming_context_repl_impl::~naming_context_repl_impl()
{
	// XXX - Check
}

inline repl_ns::ns_replica_ptr
naming_context_repl_impl::get_ckpt()
{
	return (get_checkpoint());
}

inline void
naming_context_repl_impl::list(uint32_t how_many,
    _Ix_T_out(naming::binding_list) bl, naming::binding_iterator_out bi,
    Environment& _env)
{
	naming_context_ii::list(how_many, bl, bi, _env);
}

inline void
naming_context_repl_impl::cz_set_nameserver_type(int ns_type, Environment& _env)
{
	set_nameserver_type(ns_type);
}

inline void
naming_context_repl_impl::set_nameserver_type(int ns_type)
{
	naming_context_ii::set_nameserver_type(ns_type);
}

inline int
naming_context_repl_impl::get_nameserver_type()
{
	return (naming_context_ii::get_nameserver_type());
}

inline void
naming_context_repl_impl::cz_set_look_further(bool status, Environment& _env)
{
	set_look_further(status);
}

inline void
naming_context_repl_impl::set_look_further(bool status)
{
	naming_context_ii::set_look_further(status);
}

inline bool
naming_context_repl_impl::get_look_further()
{
	return (naming_context_ii::get_look_further());
}

inline uint32_t
naming_context_repl_impl::get_owner()
{
	return (naming_context_ii::get_owner());
}

inline void
naming_context_repl_impl::set_owner(uint32_t own)
{
	naming_context_ii::set_owner(own);
}

inline uint32_t
naming_context_repl_impl::get_permission()
{
	return (naming_context_ii::get_permission());
}

inline void
naming_context_repl_impl::set_permission(uint32_t perm)
{
	naming_context_ii::set_permission(perm);
}

inline bool
naming_context_repl_impl::get_replicated()
{
	return (naming_context_ii::get_replicated());
}

inline binding_store*
naming_context_repl_impl::get_store()
{
	return (naming_context_ii::get_store());
}


#endif


//
// naming_context_root
//

#ifdef _KERNEL_ORB

inline
naming_context_root::naming_context_root(xdoor_id xd,
    bool lf, uint32_t perm, uint32_t own) :
    McNoref<naming::naming_context>(xd),
    naming_context_ii(this, false, lf, perm, own)
{
	//lint -e1506 Actually calling a virtual function in the base class
	// and not the corresponding function in the derived class.
	// TODO:	_handler()->set_cookie((void *)this);
	//lint +e1506
}
#endif

inline
naming_context_root::~naming_context_root()
{
}

inline void
naming_context_root::bind(const char *nm, CORBA::Object_ptr obj,
    Environment& _env)
{
	naming_context_ii::bind(nm, obj, _env);
}


inline void
naming_context_root::rebind(const char *nm, CORBA::Object_ptr obj,
    Environment& _env)
{
	naming_context_ii::rebind(nm, obj, _env);
}

inline void
naming_context_root::bind_context(const char *nm,
    naming::naming_context_ptr obj, Environment& _env)
{
	naming_context_ii::bind_context(nm, obj, _env);
}

inline void
naming_context_root::rebind_context(const char *nm,
    naming::naming_context_ptr obj, Environment& _env)
{
	naming_context_ii::rebind_context(nm, obj, _env);
}

inline void
naming_context_root::unbind(const char *nm, Environment& _env)
{
	naming_context_ii::unbind(nm, _env);
}

inline naming::naming_context_ptr
naming_context_root::bind_new_context(const char *nm, Environment& _env)
{
	return (naming_context_ii::bind_new_context(nm, _env));
}

inline naming::naming_context_ptr
naming_context_root::bind_new_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment& _env)
{
	return (naming_context_ii::bind_new_context_v1(nm,
	    lf, perm, own, _env));
}

inline naming::naming_context_ptr
naming_context_root::rebind_existing_context_v1(const char *nm,
    bool lf, uint32_t perm, uint32_t own, Environment& _env)
{
	return (naming_context_ii::rebind_existing_context_v1(nm,
	    lf, perm, own, _env));
}

inline bool
naming_context_root::unbind_existing_context_v1(const char *nm,
    Environment &_env)
{
	return (naming_context_ii::unbind_existing_context_v1(nm, _env));
}

inline void
naming_context_root::list(uint32_t how_many, _Ix_T_out(naming::binding_list) bl,
    naming::binding_iterator_out bi, Environment& _env)
{
	naming_context_ii::list(how_many, bl, bi, _env);
}

#ifdef _KERNEL_ORB

inline repl_ns::ns_replica_ptr
naming_context_root::get_ckpt()
{
	ASSERT(0);
	return (repl_ns::ns_replica::_nil());
}

#endif

inline void
naming_context_root::cz_set_nameserver_type(int ns_type, Environment& _env)
{
	set_nameserver_type(ns_type);
}

inline void
naming_context_root::set_nameserver_type(int ns_type)
{
	naming_context_ii::set_nameserver_type(ns_type);
}

inline int
naming_context_root::get_nameserver_type()
{
	return (naming_context_ii::get_nameserver_type());
}

inline void
naming_context_root::cz_set_look_further(bool status, Environment& _env)
{
	set_look_further(status);
}

inline void
naming_context_root::set_look_further(bool status)
{
	naming_context_ii::set_look_further(status);
}

inline bool
naming_context_root::get_look_further()
{
	return (naming_context_ii::get_look_further());
}

inline uint32_t
naming_context_root::get_owner()
{
	return (naming_context_ii::get_owner());
}

inline void
naming_context_root::set_owner(uint32_t own)
{
	naming_context_ii::set_owner(own);
}

inline uint32_t
naming_context_root::get_permission()
{
	return (naming_context_ii::get_permission());
}

inline void
naming_context_root::set_permission(uint32_t perm)
{
	naming_context_ii::set_permission(perm);
}

inline bool
naming_context_root::get_replicated()
{
	return (naming_context_ii::get_replicated());
}

inline binding_store*
naming_context_root::get_store()
{
	return (naming_context_ii::get_store());
}



#endif	/* _NAMING_CONTEXT_IMPL_IN_H */
