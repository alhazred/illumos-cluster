//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)data_container_impl.cc 1.11	08/05/20 SMI"


#include <sys/os.h>
#include <orb/invo/invo.h>
#include <nslib/data_container_impl.h>

//
// The data_container_impl implementation marshals and unmarshals its
// state (represented by a sequence of bytes) when the object is
// bound into or resolved from the name server.
//

void
data_container_impl::_unreferenced(unref_t)
{
	delete this;
}

//
// Set the implementation with the specified data which
// must not exceed 255 bytes.
//
void
data_container_impl::set_data(const char *state, Environment &_environment)
{
	size_t len = 0;

	//
	// Throw an exception if the data is NULL or too long.
	//
	if (state == NULL || (len = os::strlen(state)) > 255) {
		_environment.exception(new data_container::invalid_data);
		return;
	}

	//
	// Replace any existing data with new data.
	//
	if (_strdata != NULL)
		delete [] _strdata;
	_strdata = os::strcpy(new char[len + 1], state);
}

//
// Retrieve data which represents the state of this object.
//
char *
data_container_impl::obtain_data(Environment &)
{
	if (_strdata == NULL)
		return (NULL);
	else
		return (os::strcpy(new char[strlen(_strdata) + 1], _strdata));
}
