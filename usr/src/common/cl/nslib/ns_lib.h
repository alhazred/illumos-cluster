/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NS_LIB_H
#define	_NS_LIB_H

#pragma ident	"@(#)ns_lib.h	1.13	08/10/13 SMI"

//
// This file contains the ns_lib class that spcifies some of the
// utility routines that can be used by the clients of naming service.
// These routines are also used by the naming context implementation.
//

#include <h/naming.h>

extern int ns_debug;

// #define's to debug individual functions

#define	NS_RESOLVE	0x00001
#define	NS_LIST		0x00002

#define	NS_BIND		0x00004
#define	NS_REBIND	0x00008
#define	NS_BIND_CTX	0x00010
#define	NS_REBIND_CTX	0x00020
#define	NS_NEWCTX	0x00040
#define	NS_BIND_NEWCTX	0x00080
#define	NS_UNBIND	0x00100

#define	NS_LOOKUP	0x00200
#define	NS_STORE	0x00400
#define	NS_BINDING	0x00800

#define	NS_BINDING_IMPL	0x01000
#define	NS_BINDING_MRSH	0x02000
#define	NS_NEXT_ONE	0x04000
#define	NS_NEXT_N	0x08000

#define	NS_DUMP_STATE	0x10000
#define	NS_CKPT		0x20000
#define	NS_CKPT_UNBND	0x40000
#define	NS_UNREF	0x80000

#include <sys/dbg_printf.h>

extern dbg_print_buf ns_dbg;

#define	NS_PRINTF(func, args)		\
	if ((ns_debug & func))		\
		(ns_dbg).dbprintf args

//
// Naming context errors
//
enum nc_error {
	nc_ok,
	nc_missing_node,
	nc_not_object,
	nc_not_context,
	nc_invalid_name,
	nc_already_bound,
	nc_cannot_proceed
};

class  ns_lib {
public:
	static int name_length(const char *in_str);
	static char *get_last_comp(const char *in_str);
	static char *get_name_component(const char *in_str, int index);
	static char *sub_name(const char *n_name, int start, int len);
	static void throw_exception(Environment &env, nc_error error);
	static char *error2char(CORBA::Exception *ex);

private:
	static char *skip_slash(const char *string);
	static char *get_comp(const char *string, int &comp_cnt);
};

#endif	/* _NS_LIB_H */
