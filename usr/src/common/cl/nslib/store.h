/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _STORE_H
#define	_STORE_H

#pragma ident	"@(#)store.h	1.24	08/06/24 SMI"


//
//  This file contains the class declarations. These classes are
//  used to implement the  naming_context server.
//
//  class binding:
//	This is an abstract class that specifies the interface of a
//	name-to-object binding.
//
//  class volatile_binding:
//	This class specifies the structure for the implementation
//	of name-to-object binding in main memory (volatile store).
//	It inherits from binding and supports the virtual functions
//	of the binding class.
//
//  class binding_store:
//	This is an abstract class. This specifies all the interface
//	methods for a binding store that can store/access/manipulate
//	the name-to-object bindings.
//
//  class volatile_store:
//	This is derived from binding_store class.
//	This is a specific implementation of binding store in which
//	the name-object binding reside in a volatile store
//	(i.e. main memory).
//

#include "ns_lib.h"
#include <h/naming.h>

#ifdef _KERNEL_ORB
#include <h/repl_ns.h>
#endif

class binding;
class volatile_binding;
class binding_store;
class volatile_store;

//
// Name to object binding
//
class binding {

public:
	enum binding_type {
		bt_none, bt_obj, bt_local_ctx, bt_remote_ctx
	};

	binding() {};
	virtual ~binding() {};

	virtual CORBA::Object_ptr get_obj()			= 0;
	virtual binding_type   get_binding_type()		= 0;
	virtual binding_store *get_store_obj()			= 0;
	virtual char *get_name()				= 0;
	virtual void update_binding(char *name,
				binding::binding_type bind_type,
				CORBA::Object_ptr obj)		= 0;
	virtual void update_ctx_binding(char *name,
				CORBA::Object_ptr ctx,
				binding_store *store)		= 0;
	virtual void hold()					= 0;
	virtual void release()					= 0;
	virtual bool is_local_context()			= 0;
	virtual void get_binding(naming::binding &bi)		= 0;
};


class volatile_binding : public binding {

public:
	volatile_binding(char *name, CORBA::Object_ptr obj,
			binding::binding_type bind_type);
	volatile_binding(char *name, CORBA::Object_ptr nctx,
			binding_store *store);

	~volatile_binding();

	CORBA::Object_ptr get_obj();
	binding_type get_binding_type();
	binding_store *get_store_obj();
	char *get_name();
	void update_binding(char *name,
			binding::binding_type bind_type,
			CORBA::Object_ptr obj);
	void update_ctx_binding(char *name,
			CORBA::Object_ptr ctx,
			binding_store *store);
	void hold();
	void release();

	bool is_local_context() {
		return (get_binding_type() == binding::bt_local_ctx);
	}

	void get_binding(naming::binding &bi);

protected:
	//
	// disallowed operations
	//
	volatile_binding();
	volatile_binding(const volatile_binding &that);
	volatile_binding& operator = (const volatile_binding &that);

private:
	void set_obj(CORBA::Object_ptr obj);

	char			*vbname;
	binding::binding_type	bt;
	CORBA::Object_ptr	cobj;
	binding_store		*store_obj;
	int			ref_count;
	os::mutex_t		refcnt_lock;
};



class binding_store {

public:
	binding_store() {};
	virtual ~binding_store() {};

	virtual naming::naming_context *get_ctx()			= 0;

	virtual binding *retrieve_binding(char *name)			= 0;
	virtual nc_error store_binding(char *name,
				binding::binding_type bt,
				CORBA::Object_ptr obj,
				bool rebind)				= 0;
	virtual nc_error delete_binding(const char *name)		= 0;
	virtual nc_error delete_all_binding()				= 0;
	virtual nc_error bind_new_context(char *name,
				CORBA::Object_ptr nctx,
				binding_store *store,
				bool force)				= 0;
	virtual nc_error rebind_existing_context(char *name,
				CORBA::Object_ptr nctx,
				binding_store *store,
				bool force)				= 0;
	virtual nc_error unbind_existing_context(char *name,
				bool force)				= 0;

	virtual uint32_t get_binding_count()				= 0;
	virtual nc_error get_all_bindings(binding **&bindings,
				uint32_t total_bindings)		= 0;

#ifdef _KERNEL_ORB
	virtual void dump_state(repl_ns::ns_replica_ptr ckpt,
				Environment &_env)			= 0;
#endif
};



class volatile_store : public binding_store {

public:
	volatile_store(naming::naming_context *ncp);
	~volatile_store();

	naming::naming_context *get_ctx();
	binding *retrieve_binding(char *name);
	nc_error store_binding(char *name, binding::binding_type bt,
				CORBA::Object_ptr obj, bool rebind);
	nc_error drop_binding(const char *name);
	nc_error delete_binding(const char *name);
	nc_error delete_all_binding();
	nc_error bind_new_context(char *name, CORBA::Object_ptr nctx,
				binding_store *store, bool force);
	nc_error rebind_existing_context(char *name, CORBA::Object_ptr nctx,
				binding_store *store, bool force);
	nc_error unbind_existing_context(char *name, bool force);

	uint32_t get_binding_count();
	nc_error get_all_bindings(binding **&bindings, uint32_t total_bindings);

#ifdef _KERNEL_ORB
	void dump_state(repl_ns::ns_replica_ptr ckpt, Environment &_env);
#endif

	struct bnode {
		volatile_binding  *vbinding;
		struct bnode  *next;
	};

	bnode *get_blist();

	void add_binding(volatile_binding *vb);
protected:
	volatile_store();

private:
	bnode *blist_hdrp;
	uint32_t bindings_count;
	naming::naming_context *ctxp;

	volatile_binding *find_binding(char *name);

#ifdef _KERNEL_ORB
	int dump_all_bindings(repl_ns::ns_replica_ptr ckpt, Environment &_env);
	void cleanup_dump_state();
#endif
};

#endif	/* _STORE_H */
