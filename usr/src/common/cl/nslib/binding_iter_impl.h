/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _BINDING_ITER_IMPL_H
#define	_BINDING_ITER_IMPL_H

#pragma ident	"@(#)binding_iter_impl.h	1.10	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/naming.h>
#include <nslib/binding_iter_handler.h>

//
// Binding iterator class (with its own object handler)
//

class binding_iterator_impl :
    public impl_spec<naming::binding_iterator, binding_iter_handler> {
public:
	binding_iterator_impl();

	binding_iterator_impl(uint32_t binding_index, uint32_t num_bindings,
	    naming::binding_list *iter_list);

	~binding_iterator_impl();

	void _unreferenced(unref_t);

	void marshal(service &b);

	static CORBA::Object_ptr unmarshal(service &b);

	static void unmarshal_cancel(service &b);

	bool next_one(naming::binding_out, Environment &_env);

	void next_n(uint32_t how_many, naming::binding_list_out bl,
	    Environment &_env);

private:
	uint32_t valid_binding_index;
	uint32_t total_bindings;
	naming::binding_list *bindings_list;
};

#endif	/* _BINDING_ITER_IMPL_H */
