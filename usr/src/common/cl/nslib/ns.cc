//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ns.cc 	class representing a name server library
//

#pragma ident	"@(#)ns.cc	1.86	08/08/14 SMI"

#include <nslib/ns.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/object/adapter.h>
#include <nslib/data_container_handler.h>

#ifdef _KERNEL_ORB
#include <nslib/naming_context_impl.h>
#endif

#ifndef _KERNEL
#include <door.h>
#include <sys/cladm_int.h>
#endif

#include <nslib/ns_lib.h>

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifdef	_KERNEL
#include <sys/clconf_int.h>
#include <sys/zone.h>
#else
#include <zone.h>
#endif
#endif

#if (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)
#include <sys/brand.h>
#include <libzonecfg.h>
#endif

#if defined(UNODE) || (SOL_VERSION < __s10)
#define	GLOBAL_ZONEID	0
#endif

static const char local_nameserver_template[] = "local_ns_%d";

//
// This lock is used to gaurd the
// root/global nameserver reference.
//
static os::mutex_t gns_lock;

//
// This lock is used to guard the
// local nameserver reference.
//
static os::mutex_t llock;

//
// This lock will be used to protect only the
// ccr directory object which is cached in the
// name server.
//
static os::mutex_t ns_dir_lock;

naming::naming_context_ptr
    ns::local_nmsrvr = naming::naming_context::_nil();
naming::naming_context_ptr
    ns::local_nmsrvr_c0 = naming::naming_context::_nil();
naming::naming_context_ptr
    ns::local_nmsrvr_c1 = naming::naming_context::_nil();

naming::naming_context_ptr
    ns::global_nmsrvr = naming::naming_context::_nil();
naming::naming_context_ptr
    ns::global_nmsrvr_c0 = naming::naming_context::_nil();
naming::naming_context_ptr
    ns::global_nmsrvr_c1 = naming::naming_context::_nil();

#ifdef	_KERNEL_ORB

naming_context_root* ns::local_nmsrvr_obj = NULL;
naming_context_impl* ns::local_nmsrvr_obj_c0 = NULL;
naming_context_impl* ns::local_nmsrvr_obj_c1 = NULL;

naming_context_repl_impl* ns::global_nmsrvr_obj = NULL;
naming_context_repl_impl* ns::global_nmsrvr_obj_c0 = NULL;
naming_context_repl_impl* ns::global_nmsrvr_obj_c1 = NULL;

binding_store* ns::b_store_local_c1 = NULL;
binding_store* ns::b_store_root_c1  = NULL;
#endif

//
// Lint is throwing message 1776 for all NS_PRINTF
// and print_exception functions. Hence supressing
// it for the whole file.
//
// Message:
// Converting string literals to char * is deprecated
// (Context)  -- A string literal, according to Standard C++
// is typed an array of const char.  This message is issued
// when such a literal is assigned to a non-const pointer.
//

//lint -e1776


//
// External interface for cl_orb
//
extern "C" naming::naming_context_ptr
root_nameserver()
{
	return (ns::root_nameserver());
}

//
//
// Return an object reference to the global name server
//
//
naming::naming_context_ptr
ns::root_nameserver()
{
	int				ret;
	uint_t				clid = 0;
	char				clid_str[30];
	version_manager::vp_version_t 	ns_version;
	bool				native_zone_flag = false;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	ret = ns::get_cz_id();
	if (ret < 0) {
		CL_PANIC(0);
	}

	if (ret == 2) {
		native_zone_flag = true;
	}

	clid = (uint_t)ret;
	if (ret == 0 || ret == 1 || ret == 2) {
		clid = 0;
	}
#endif
	if (!root_nameserver_init(native_zone_flag)) {
		return (naming::naming_context::_nil());
	}

	if (CORBA::is_nil(global_nmsrvr)) {
		//
		// The global nameserver is not yet
		// initialized. This assert can be removed
		// later on as its the responsibility
		// of the clients calling this function
		// to handle the error.
		//
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}

	os::sprintf(clid_str, "%d", clid);

	if (clid > 0) {
		//
		// The call has come from a CZ.
		// From the global nameserver root, extract
		// the context and return it to the caller.
		//
		CORBA::Object_ptr nsobj = NULL;
		Environment e;
		naming::naming_context_ptr cz_ctx_p;

		ASSERT(!CORBA::is_nil(global_nmsrvr));

		nsobj = global_nmsrvr->resolve(clid_str, e);
		if (e.exception() || CORBA::is_nil(nsobj)) {
			e.clear();
			CORBA::release(nsobj);
			return (naming::naming_context::_nil());
		} else {
			cz_ctx_p =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
		}
		ASSERT(!CORBA::is_nil(cz_ctx_p));
		return (cz_ctx_p);
	}

	ns_version = ns::get_ns_version();
	if (ns_version.major_num < 2) {
		//
		// Old Version of the name server software is runnig
		// Return the real root of the global nameserver
		//
		ASSERT(!CORBA::is_nil(global_nmsrvr));
		return (naming::naming_context::_duplicate(global_nmsrvr));
	}

	//
	// New version of the name server software is running.
	// The call came from the global zone or a native local zone.
	// Return the context of the physical cluster
	//
	ASSERT(!CORBA::is_nil(global_nmsrvr_c0));
	return (naming::naming_context::_duplicate(global_nmsrvr_c0));
}

//
// This function will initialize the global name
// server.
//
// native_zone_flag is set to true when the call
// is coming from the native zone. The caller
// function has to first detect that the call
// came from a native zone and then only pass true.
// By default it is set to false.
// When CR 6725782 is fixed, remove this parameter
// and directly use the ns::is_native_zone function
//
// Return Value:
// 	true: 	On success
// 	false:	On failure
//
bool
ns::root_nameserver_init(bool native_zone_flag)
{
	Environment 			e;
	CORBA::Exception 		*ex;
	CORBA::Object_ptr 		nsobj = nil;
	version_manager::vp_version_t 	ns_version;

#if (SOL_VERSION >= __s10)
	zoneid_t 	zid;
#else
	int		zid;
#endif

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zid = getzoneid();
#else
	zid = 0;
#endif

	//
	// If call came from native zone, treat it
	// like the global zone.
	//
	if (native_zone_flag) {
		zid = 0;
	}

	//
	// This assert catches calls to root_nameserver
	// before calling ORB::initialize().
	//
	ASSERT(ORB::is_initialized() || ORB::is_initializing());

#ifdef _KERNEL_ORB

	int 				retry_count = 0;

	//
	// No need to grab the gns_lock as we are going
	// to read the variable.
	// TODO: Explore if lock should be held
	//
	while (!root_ns_initialized) {
		if (retry_count == 40) {
			break;
		}
		retry_count++;
		os::printf("Global NS not yet initialized ... sleeping"
		    " Count %d\n", retry_count);
		os::usecsleep(500000L);   // Sleep for half sec
	}

	//
	// We have waited for 20 sec. If the initialization is not
	// complete, we assert in debug code. For non debug code,
	// we return a false.
	// TODO: If we are asserting to frequently, that means that
	// we need to increase the retry.
	//
	if (!root_ns_initialized) {
		ASSERT(0);
		return (false); //lint !e527
	}
#endif

	if (CORBA::is_nil(global_nmsrvr)) {
		gns_lock.lock();
		if (CORBA::is_nil(global_nmsrvr)) {
			naming::naming_context_var lctxp;

			lctxp = ns::local_nameserver_root();
			ASSERT(!CORBA::is_nil(lctxp));

			//
			// Now lookup the global name server in the
			// local name server.
			//
			nsobj = lctxp->resolve("gbl_ns", e);
			while (nsobj == NULL) {
				//
				// XXX for some reason nsobj returns empty
				//
				os::sc_syslog_msg syslog_msg(
				    SC_SYSLOG_NAMING_SERVICE_TAG, "naming",
				    NULL);
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "nsobj == NULL.  Retrying ...\n");
				os::usecsleep(100000L);
				nsobj = lctxp->resolve("gbl_ns", e);
			}

			ASSERT(!CORBA::is_nil(nsobj));

			if (e.exception() || CORBA::is_nil(nsobj)) {
				ASSERT(0);
				e.clear(); //lint !e527
				global_nmsrvr = naming::naming_context::_nil();
				global_nmsrvr_c0 =
				    naming::naming_context::_nil();
				global_nmsrvr_c1 =
				    naming::naming_context::_nil();
				CORBA::release(nsobj);
				gns_lock.unlock();
				return (false);
			} else {
				global_nmsrvr =
				    naming::naming_context::_narrow(nsobj);
				CORBA::release(nsobj);
			}
		}
		gns_lock.unlock();
	}

	//
	// Find the running version of the
	// name server software
	//
	ns_version = ns::get_ns_version();

	//
	// At this point the global_nmsrvr should
	// be initialized.
	//
	ASSERT(!CORBA::is_nil(global_nmsrvr));

	//
	// Retrieve the c0 context only if its the global zone and the
	// new version is running
	//
	if ((CORBA::is_nil(global_nmsrvr_c0)) &&
	    (ns_version.major_num >= 2) &&
	    (zid == GLOBAL_ZONEID)) {
		gns_lock.lock();
		//
		// If the new version is running, then
		// only try to retrieve the 0 context
		//
		if (CORBA::is_nil(global_nmsrvr_c0)) {
			nsobj = global_nmsrvr->resolve("0", e);
			if (((ex = e.exception()) != NULL) ||
			    CORBA::is_nil(nsobj)) {
				NS_PRINTF(NS_DUMP_ALL, (
				    "root_nameserver_init: "
				    "Failed to get contex c0 in name server "
				    " Environment = %p\n", &e));
				if (naming::not_found::_exnarrow(ex) != NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: not_found\n"));
				} else if (
				    naming::cannot_proceed::_exnarrow(ex) !=
				    NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: cannot_proceed\n"));
				} else if (naming::invalid_name::_exnarrow(ex)
				    != NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: invalid_name\n"));
				} else if (CORBA::CTX_EACCESS::_exnarrow(ex)
				    != NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: "
					    "CORBA::CTX_EACCESS\n"));
				} else if (CORBA::NS_EPERM::_exnarrow(ex) !=
				    NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: CORBA::NS_EPERM \n"));
				}
				//
				// We want to catch problems quickly
				// in debug code. Hence the assert
				//
				ASSERT(0);
				e.clear(); //lint !e527
				global_nmsrvr_c0 =
				    naming::naming_context::_nil();
				global_nmsrvr_c1 =
				    naming::naming_context::_nil();
				CORBA::release(nsobj);
				gns_lock.unlock();
				return (false);
			} else {
				global_nmsrvr_c0 =
				    naming::naming_context::_narrow(nsobj);
				CORBA::release(nsobj);
			}
		}
		gns_lock.unlock();
		ASSERT(!CORBA::is_nil(global_nmsrvr_c0));
	}

	//
	// Retrieve the c1 for all zones when the new version is running
	//
	if ((CORBA::is_nil(global_nmsrvr_c1)) && (ns_version.major_num >= 2)) {
		gns_lock.lock();
		//
		// If the new version is running, then
		// only try to retrieve the 1 context
		//
		if (CORBA::is_nil(global_nmsrvr_c1)) {
			nsobj = global_nmsrvr->resolve("1", e);
			if (((ex = e.exception()) != NULL) ||
			    CORBA::is_nil(nsobj)) {
				NS_PRINTF(NS_DUMP_ALL, (
				    "root_nameserver_init: "
				    "Failed to get contex c1 in name server "
				    "Environment = %p\n", &e));
				if (naming::not_found::_exnarrow(ex) != NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: not_found\n"));
				} else if (
				    naming::cannot_proceed::_exnarrow(ex) !=
				    NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: cannot_proceed\n"));
				} else if (naming::invalid_name::_exnarrow(ex)
				    != NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: invalid_name\n"));
				} else if (CORBA::CTX_EACCESS::_exnarrow(ex)
				    != NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: "
					    "CORBA::CTX_EACCESS\n"));
				} else if (CORBA::NS_EPERM::_exnarrow(ex) !=
				    NULL) {
					NS_PRINTF(NS_DUMP_ALL, (
					    "root_nameserver_init "
					    "Exception: CORBA::NS_EPERM\n"));
				}
				//
				// We want to catch problems quickly
				// in debug code. Hence the assert
				//
				ASSERT(0);
				e.clear(); //lint !e527
				global_nmsrvr_c1 =
				    naming::naming_context::_nil();
				CORBA::release(nsobj);
				gns_lock.unlock();
				return (false);
			} else {
				global_nmsrvr_c1 =
				    naming::naming_context::_narrow(nsobj);
				CORBA::release(nsobj);
			}
		}
		gns_lock.unlock();
		ASSERT(!CORBA::is_nil(global_nmsrvr_c1));
	}

	// Success
	return (true);
}

//
// External interface for cl_orb
//
extern "C" naming::naming_context_ptr
root_nameserver_root()
{
	return (ns::root_nameserver_root());
}
//
//
// Return an object reference to the global name server
//
naming::naming_context_ptr
ns::root_nameserver_root()
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifndef _KERNEL
	//
	// If call comes from a clusterised zone
	// then return nil. A cz does not have access
	// to global zone name server related context.
	//
	int id = ns::get_cz_id();
	if ((id != 0) && (id != 2)) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}
#endif
#endif
	//
	// Initialize the root nameserver
	// if not already initialized.
	//
	if (!root_nameserver_init()) {
		return (naming::naming_context::_nil());
	}

	if (CORBA::is_nil(global_nmsrvr)) {
		return (naming::naming_context::_nil());
	} else {
		return (naming::naming_context::_duplicate(global_nmsrvr));
	}

}

naming::naming_context_ptr
ns::root_nameserver_c0()
{
	version_manager::vp_version_t ns_version;

	ns_version = ns::get_ns_version();
	if (ns_version.major_num < 2) {
		//
		// If you have hit this panic, it means that
		// you are not RU compliant. You are trying
		// to invoke use the new interface while the
		// old version of the name server software
		// is running.
		//
		CL_PANIC(0);
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If call comes from a clusterised zone
	// then return nil. A cz does not have access
	// to global zone name server related context.
	//
	int id = ns::get_cz_id();
	if ((id != 0) && (id != 2)) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}
#endif
	//
	// Initialize the root nameserver
	// if not already initialized.
	//
	if (!root_nameserver_init()) {
		return (naming::naming_context::_nil());
	}

	if (CORBA::is_nil(global_nmsrvr_c0)) {
		return (naming::naming_context::_nil());
	} else {
		return (naming::naming_context::_duplicate(global_nmsrvr_c0));
	}

}

naming::naming_context_ptr
ns::root_nameserver_c1()
{
	version_manager::vp_version_t ns_version;

	ns_version = ns::get_ns_version();
	if (ns_version.major_num < 2) {
		//
		// If you have hit this panic, it means that
		// you are not RU compliant. You are trying
		// to invoke use the new interface while the
		// old version of the name server software
		// is running.
		//
		CL_PANIC(0);
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifndef _KERNEL
	//
	// If call comes from a clusterised zone
	// then return nil. A cz does not have access
	// to global zone name server related context.
	//
	int id = ns::get_cz_id();
	if ((id != 0) && (id != 2)) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}
#endif
#endif
	//
	// Initialize the root nameserver
	// if not already initialized.
	//
	if (!root_nameserver_init()) {
		return (naming::naming_context::_nil());
	}

	if (CORBA::is_nil(global_nmsrvr_c1)) {
		return (naming::naming_context::_nil());
	} else {
		return (naming::naming_context::_duplicate(global_nmsrvr_c1));
	}

}

//
// External interface for cl_orb
//
extern "C" naming::naming_context_ptr
local_nameserver(nodeid_t node)
{
	if (node == (nodeid_t)NULL) {
		return (ns::local_nameserver(NODEID_UNKNOWN));
	} else {
		return (ns::local_nameserver(node));
	}
}

//
// Return an object reference to the local name server.
//
naming::naming_context_ptr
ns::local_nameserver(nodeid_t n)
{
	int		ret;
	uint_t		clid = 0;
	char		clid_str[30];
	bool		native_zone_flag = false;

	//
	// This assert catches calls to local_nameserver before calling
	// ORB::initialize().
	//
	ASSERT(ORB::is_initialized() || ORB::is_initializing());

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	ret = ns::get_cz_id();
	if (ret < 0) {
		CL_PANIC(0);
	}

	if (ret == 2) {
		native_zone_flag = true;
	}

	clid = (uint_t)ret;
	if (ret == 0 || ret == 1 || ret == 2) {
		clid = 0;
	}
#endif
	os::sprintf(clid_str, "%d", clid);

	//
	// If we're looking for the local nameserver on another node, handle
	// that case here.
	//
	if ((n != NODEID_UNKNOWN) && (n != orb_conf::local_nodeid())) {
		char local_nameserver_name[24]; // "local_ns_NNNNN\0"
		Environment e;

		naming::naming_context_var ctxp = root_nameserver_root();
		if (CORBA::is_nil(ctxp)) {
			return (naming::naming_context::_nil());
		}
		(void) os::snprintf(local_nameserver_name, 24UL,
		    local_nameserver_template, n);
		CORBA::Object_var cobj =
		    ctxp->resolve(local_nameserver_name, e);
		if (e.exception()) {
			e.clear();
			return (naming::naming_context::_nil());
		}

		// Store the root context of the local name server
		naming::naming_context_var lctxp =
		    naming::naming_context::_narrow(cobj);

		//
		// Find the context 0 of the local name server
		// and return it if the call came from the global
		// zone or a native local zone.
		// If the call came from a cz, then return the
		// context of the cz
		//
		cobj = lctxp->resolve(clid_str, e);
		if (e.exception()) {
			e.clear();
			return (naming::naming_context::_nil());
		}
		return (naming::naming_context::_narrow(cobj));
	}

	//
	// Initialize the local name server
	//
	if (!local_nameserver_init(native_zone_flag)) {
#ifdef _KERNEL_ORB
		//
		// Local nameserver initialization
		// failed in the kernel. Something is
		// seriously wrong with the system.
		// Panic the node.
		//
		CL_PANIC(0);
#else
		//
		// Failed to cache the local nameserver
		// reference in userland. Return nil
		// The client is responsible for
		// handling the error.
		//
		return (naming::naming_context::_nil());
#endif
	}

	if (clid > 0) {
		//
		// You are in a CZ
		// The local name server reference should be available
		// by this time. From the local nameserver, get the
		// context of the cz
		//
		CORBA::Object_ptr nsobj = NULL;
		Environment e;
		naming::naming_context_ptr cz_ctx_p;

		nsobj = local_nmsrvr->resolve(clid_str, e);
		if (e.exception() || CORBA::is_nil(nsobj)) {
			e.clear();
			CORBA::release(nsobj);
			return (naming::naming_context::_nil());
		} else {
			cz_ctx_p =
			    naming::naming_context::_narrow(nsobj);
			CORBA::release(nsobj);
		}
		ASSERT(!CORBA::is_nil(cz_ctx_p));
		return (cz_ctx_p);
	}

	ASSERT(!CORBA::is_nil(local_nmsrvr_c0));
	// You are in the global zone or in a native local zone
	return (naming::naming_context::_duplicate(local_nmsrvr_c0));
}

//
// This function will initialize the local nameserver
// on this node. In case of kernel, it will create the
// local name server. In case of userland, it will
// cache the reference of the local nameserver from
// the kernel.
// If the local nameserver is already initialized,
// this function just resturns success.
//
// native_zone_flag is set to true when the call
// is coming from the native zone. The caller
// function has to first detect that the call
// came from a native zone and then only pass true.
// By default it is set to false.
// When CR 6725782 is fixed, remove this parameter
// and directly use the ns::is_native_zone function
//
// Return Value:
// 	true: 	On success
// 	false:	On failure
//
bool
ns::local_nameserver_init(bool native_zone_flag)
{
#if !defined(_KERNEL_ORB)

#if (SOL_VERSION >= __s10)
	zoneid_t 	zid;
#else
	int		zid;
#endif

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zid = getzoneid();
#else
	zid = 0;
#endif
	//
	// If call came from a native zone, set the
	// zid to 0. Treat it like the global zone.
	//
	if (native_zone_flag) {
		zid = 0;
	}
#endif // end of !defined(_KERNEL_ORB)

	//
	// This assert catches calls to local_nameserver
	// before calling ORB::initialize().
	//
	ASSERT(ORB::is_initialized() || ORB::is_initializing());
#ifdef _KERNEL_ORB
	if (CORBA::is_nil(local_nmsrvr)) {
		//
		// Use a lock on the outside chance that there are
		// two threads racing to initialize local namserver.
		// In the normal case (when local namserver isn't nil),
		// we use the fast path and avoid this locking.
		//
		llock.lock();
		if (CORBA::is_nil(local_nmsrvr)) {
			local_nmsrvr = ns::create_local_nameserver();

			//
			// Check that context 0 got created in the
			// local name server
			//
			if (CORBA::is_nil(local_nmsrvr_c0)) {
				NS_PRINTF(NS_BIND_CTX,
				    ("local_nameserver_init: "
				    "C0 context is nil\n"));
				llock.unlock();
				return (false);
			}

			//
			// Check that context 1 got created in the
			// local name server
			//
			if (CORBA::is_nil(local_nmsrvr_c1)) {
				NS_PRINTF(NS_BIND_CTX,
				    ("local_nameserver_init: "
				    "C1 context is nil\n"));
				llock.unlock();
				return (false);
			}
		}
		llock.unlock();
	}

	ASSERT(!CORBA::is_nil(local_nmsrvr));
	ASSERT(!CORBA::is_nil(local_nmsrvr_c0));
	ASSERT(!CORBA::is_nil(local_nmsrvr_c1));
#else
	// ----  Userland ----
	if (CORBA::is_nil(local_nmsrvr)) {
		//
		// Use a lock on the outside chance that there are
		// two threads racing to initialize local namserver.
		// In the normal case (when local namserver isn't nil),
		// we use the fast path and avoid this locking.
		//
		llock.lock();
		if (CORBA::is_nil(local_nmsrvr)) {

			cllocal_ns_data_t d;
			door_info_t info;

			// Get Solaris door and type ID.
			if (os::cladm(CL_CONFIG, CL_GET_NS_DATA, &d) != 0) {
				NS_PRINTF(NS_BIND_CTX,
				    ("local_nameserver_init: "
				    "Could not get door\n"));
				llock.unlock();
				return (false);
			}

			// Check to be sure file descriptor is a door.
			if (door_info(d.filedesc, &info) < 0) {
				NS_PRINTF(NS_BIND_CTX,
				    ("local_nameserver_init: "
				    "Not a door\n"));
				llock.unlock();
				return (false);
			}

			// Create a partially initialized proxy object.
			Anchor<naming::naming_context_stub> *objp =
			    new Anchor<naming::naming_context_stub>(d.filedesc,
				NULL);

			// Get the interface descriptor for the name server.
			InterfaceDescriptor *infp =
			    OxSchema::schema()->descriptor(d.tid,
				objp->_handler());
			// Couldn't get the deep type for the proxy object.
			if (infp == NULL) {
				delete objp;
				NS_PRINTF(NS_BIND_CTX,
				    ("local_nameserver_init: "
				    "Couldnot get deep type\n"));
				llock.unlock();
				return (false);
			}
			// Set the deep type.
			objp->_deep_type(infp);
			// Initialize the local name server reference.
			local_nmsrvr = objp;
		}
		llock.unlock();
	}
	ASSERT(!CORBA::is_nil(local_nmsrvr));

	//
	// Initialize the context 0 and 1 of the
	// local name server in userland.
	//
	CORBA::Object_ptr nsobj = NULL;
	Environment e;

	//
	// Retrieve the c0 context only for the global zone and native
	// local zones
	//
	if ((CORBA::is_nil(local_nmsrvr_c0)) && (zid == GLOBAL_ZONEID)) {
		//
		// Use a lock on the outside chance that there are
		// two threads racing to initialize local namserver.
		// In the normal case (when local namserver isn't nil),
		// we use the fast path and avoid this locking.
		//
		llock.lock();

		if (CORBA::is_nil(local_nmsrvr_c0)) {
			// Lookup the context 0 in the name server root
			nsobj = local_nmsrvr->resolve("0", e);
			if (e.exception() || CORBA::is_nil(nsobj)) {
				e.clear();
				local_nmsrvr = naming::naming_context::_nil();
				CORBA::release(nsobj);
				NS_PRINTF(NS_DUMP_ALL,
				    ("local_nameserver_init: "
				    "Could not get C0\n"));
				llock.unlock();
				return (false);
			} else {
				local_nmsrvr_c0 =
				    naming::naming_context::_narrow(nsobj);
				CORBA::release(nsobj);
			}
		}
		llock.unlock();
		ASSERT(!CORBA::is_nil(local_nmsrvr_c0));
	}

	if (CORBA::is_nil(local_nmsrvr_c1)) {
		//
		// Use a lock on the outside chance that there are
		// two threads racing to initialize local namserver.
		// In the normal case (when local namserver isn't nil),
		// we use the fast path and avoid this locking.
		//
		llock.lock();

		if (CORBA::is_nil(local_nmsrvr_c1)) {

			nsobj = local_nmsrvr->resolve("1", e);
			if (e.exception() || CORBA::is_nil(nsobj)) {
				e.clear();
				local_nmsrvr = naming::naming_context::_nil();
				local_nmsrvr_c0 =
				    naming::naming_context::_nil();
				CORBA::release(nsobj);
				NS_PRINTF(NS_DUMP_ALL,
				    ("local_nameserver_init: "
				    "Could not get C1\n"));
				llock.unlock();
				return (false);
			} else {
				local_nmsrvr_c1 =
				    naming::naming_context::_narrow(nsobj);
				CORBA::release(nsobj);
			}
		}
		llock.unlock();
	}
	ASSERT(!CORBA::is_nil(local_nmsrvr_c1));
#endif
	// Success
	return (true);
} //lint !e715

#if (SOL_VERSION >= __s10) && !defined(UNODE)

//
// Will return the cz id corresponding to the
// zone. For a native local zone, it will return 2
// For the global zone, it will return 0
// In case of error, will return -1
//
// This function should be used in the name server
// sub system only. Do not use it from other
// sub systems.
//
int
ns::get_cz_id()
{
	zoneid_t			zid;
	char				name[ZONENAME_MAX];
	uint_t				clid;
	bool				init_flag = false;

	Environment			e;
	CORBA::Exception 		*ex;
	CORBA::Object_var		obj;
	static ccr::directory_var	ccr_directory_v;

	zid = getzoneid();
	if (zid == 0) {
		//
		// This is the global zone. The
		// cluster id is zero.
		//
		return (0);
	}

	//
	// The call is coming from a local zone
	// Lookup the cluster_directory file and
	// find out the corresponding cluster id.
	// For a native local zone (non cz), the
	// cluster id is 0.
	//
	if (CORBA::is_nil(ccr_directory_v)) {
		ns_dir_lock.lock();
		if (CORBA::is_nil(ccr_directory_v)) {
			if (CORBA::is_nil(local_nmsrvr_c1)) {
				init_flag = local_nameserver_init();
				if (!init_flag) {
					// Error
					return (-1);
				}
			}
			ASSERT(!CORBA::is_nil(local_nmsrvr_c1));
			if (CORBA::is_nil(local_nmsrvr_c1)) {
				NS_PRINTF(NS_BIND_CTX,
				    ("get_cz_id: "
				    "Context not initialised yet\n"));
				ns_dir_lock.unlock();
				return (-1);
			}

			obj = local_nmsrvr_c1->resolve("ccr_directory", e);
			if (e.exception()) {
				NS_PRINTF(NS_BIND_CTX,
				    ("get_cz_id: "
				    "Failed to get ccr_directory\n"));
				e.clear();
				ns_dir_lock.unlock();
				return (-1);
			}
			ccr_directory_v = ccr::directory::_narrow(obj);
		}
		ns_dir_lock.unlock();
	}

	// By this time the reference should be present
	ASSERT(!CORBA::is_nil(ccr_directory_v));

	// Get the zone name.
#ifdef _KERNEL
	zone_t *zonep = zone_find_by_id(zid);
	(void) os::strcpy(name, zonep->zone_name);
	zone_rele(zonep);
#else
	if (getzonenamebyid(zid, name, (size_t)ZONENAME_MAX) == -1) {
		// error in getting name
		return (-1);
	}
#endif

	clid = ccr_directory_v->zc_getidbyname(name, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::invalid_zc_access::_exnarrow(ex)) {
			NS_PRINTF(NS_BIND_CTX, ("get_cz_id: "
			    "Failed to get cluster id "
			    "Exception: invalid_zc_access\n"));
			ASSERT(0);
		} else if (ccr::no_such_zc::_exnarrow(ex)) {
			e.clear();
			//
			// The target zone is not present in the
			// cluster_directory file. This means that
			// the target zone is a native local zone.
			// Such zones will be treated like the global
			// zone and hence a 2 will be returned.
			//
			// ccr_directory_v->zc_getidbyname returns 2
			// for a native local zone.
			//
			return (2);
		} else {
			NS_PRINTF(NS_BIND_CTX, ("get_cz_id: "
			    "Failed to get cluster id "
			    "Exception: UNKNOWN e = %p\n", &e));
		}
		e.clear();
		return (-1);
	}

	// Return the cluster id
	return ((int)clid);
}

//
// Will return the cz id corresponding to the
// zone name.  For a native local zone, it will return 0
// For the global zone, it will return 0
// In case of error, will return -1
//
// This function should be used in the name server
// sub system only. Do not use it from other
// sub systems.
//
int
ns::get_cz_id_by_name(const char *cz_name)
{ //lint !e578

	char				name[ZONENAME_MAX];
	uint_t				clid;

	Environment			e;
	CORBA::Exception 		*ex;
	CORBA::Object_var		obj;
	static ccr::directory_var	ccr_directory_v;

	if (cz_name == NULL) {
		// cz name can not be null
		ASSERT(0);
		return (-1); //lint !e527
	}

	if (os::strcmp(cz_name, "global") == 0) {
		//
		// This is the global zone. The
		// cluster id is zero.
		//
		return (0);
	}

	//
	// Lookup the cluster_directory file and
	// find out the corresponding cluster id.
	// For a native local zone (non cz), the
	// cluster id is 0.
	//
	if (CORBA::is_nil(ccr_directory_v)) {
		ns_dir_lock.lock();
		if (CORBA::is_nil(ccr_directory_v)) {
			if (CORBA::is_nil(local_nmsrvr_c1)) {
				(void) local_nameserver_init();
			}
			ASSERT(!CORBA::is_nil(local_nmsrvr_c1));
			if (CORBA::is_nil(local_nmsrvr_c1)) {
				NS_PRINTF(NS_BIND_CTX,
				    ("get_cz_id_by_name: "
				    "Context not initialised yet\n"));
				ns_dir_lock.unlock();
				return (-1);
			}

			obj = local_nmsrvr_c1->resolve("ccr_directory", e);
			if (e.exception()) {
				NS_PRINTF(NS_BIND_CTX,
				    ("get_cz_id_by_name`: "
				    "Failed to get ccr_directory\n"));
				e.clear();
				ns_dir_lock.unlock();
				return (-1);
			}
			ccr_directory_v = ccr::directory::_narrow(obj);
		}
		ns_dir_lock.unlock();
	}

	// By this time the reference should be present
	ASSERT(!CORBA::is_nil(ccr_directory_v));

	// Get the cz id for the corresponding name
	(void) os::strcpy(name, cz_name);
	clid = ccr_directory_v->zc_getidbyname(name, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::invalid_zc_access::_exnarrow(ex)) {
			NS_PRINTF(NS_BIND_CTX, ("get_cz_id_by_name: "
			    "Failed to get cluster id "
			    "Exception: invalid_zc_access\n"));
			ASSERT(0);
		} else if (ccr::no_such_zc::_exnarrow(ex)) {
			e.clear();
			//
			// The target zone is not present in the
			// cluster_directory file. This means that
			// the target zone is a native local zone.
			// Such zones will be treated like the global
			// zone and hence a 2  will be returned.
			//
			return (2);
		} else {
			NS_PRINTF(NS_BIND_CTX, ("get_cz_id_by_name: "
			    "Failed to get cluster id "
			    "Exception: UNKNOWN\n"));
		}
		e.clear();
		return (-1);
	}

	// Return the cluster id
	return ((int)clid);
}

//
// This will be used to get the local nameserver context of the cluster
// specified as argument. It can be the base cluster or a zone cluster.
// If this method is called from the global zone, then the query is allowed
// for the base cluster as well as any zone cluster.
// If this method is invoked from a zone cluster or a native zone,
// and if the cluster specified as argument is different from the cluster
// in which the caller resides, then nil is returned.
//
naming::naming_context_ptr
ns::local_nameserver_cz(const char *cz_name, nodeid_t n)
{ //lint !e578
	zoneid_t			zid;
	int				clid;
	CORBA::Object_ptr		obj_p;
	naming::naming_context_var 	cz_ctx_v;
	Environment			e;
	CORBA::Exception 		*exp;
	char				clid_str[30];
	int				ret;
	int				curr_clid;

	if (cz_name == NULL) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}

	clid = ns::get_cz_id_by_name(cz_name);
	if (clid < 0) {
		//
		// Unable to find cluster id
		//
		return (naming::naming_context::_nil());
	}

	// Store the cluster id as a string
	os::sprintf(clid_str, "%d", clid);

	//
	// Check that a non-global zone does not get access
	// to a cluster different from the cluster it belongs to.
	//
	zid = getzoneid();
	if (zid != GLOBAL_ZONEID) {
		ret = ns::get_cz_id();
		if (ret < 0) {
			CL_PANIC(0);
		}
		curr_clid = ret;
		if (ret == 0 || ret == 1 || ret == 2) {
			curr_clid = 0;
		}

		if (curr_clid != clid) {
			//
			// Queried cluster does not match
			// the cluster that the current non-global zone
			// belongs to. Not allowed.
			//
			return (naming::naming_context::_nil());
		}
	}

	//
	// If we're looking for the local nameserver on another node, handle
	// that case here.
	//
	if ((n != NODEID_UNKNOWN) && (n != orb_conf::local_nodeid())) {
		char local_nameserver_name[24]; // "local_ns_NNNNN\0"
		naming::naming_context_var ctx_v = root_nameserver_root();
		if (CORBA::is_nil(ctx_v)) {
			return (naming::naming_context::_nil());
		}
		(void) os::snprintf(local_nameserver_name, 24UL,
		    local_nameserver_template, n);
		CORBA::Object_var cobj_v =
		    ctx_v->resolve(local_nameserver_name, e);
		if (e.exception()) {
			e.clear();
			return (naming::naming_context::_nil());
		}

		// Store the root context of the local name server
		naming::naming_context_var lctx_v =
		    naming::naming_context::_narrow(cobj_v);

		cobj_v = lctx_v->resolve(clid_str, e);
		if (e.exception()) {
			e.clear();
			return (naming::naming_context::_nil());
		}
		return (naming::naming_context::_narrow(cobj_v));
	}

	//
	// Initialize the local name server
	// if its not already initialized
	//
	(void) local_nameserver_init();

	if (CORBA::is_nil(local_nmsrvr)) {
		ASSERT(0);
		NS_PRINTF(NS_RESOLVE, ("local_nameserver_cz: "
		    "local_nmsrvr is nil. clid %d\n", clid)); //lint !e527
		return (naming::naming_context::_nil());
	}

	obj_p = local_nmsrvr->resolve(clid_str, e);
	if ((exp = e.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("local_nameserver_cz: "
			    "Exception: not_found clid %d\n",
			    clid));
		} else if (naming::cannot_proceed::_exnarrow(exp) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("local_nameserver_cz: "
			    "Exception: cannot_proceed clid %d\n",
			    clid));
		} else if (naming::invalid_name::_exnarrow(exp) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("local_nameserver_cz: "
			    "Exception: invalid_name clid %d\n",
			    clid));
		} else {
			NS_PRINTF(NS_RESOLVE, ("local_nameserver_cz: "
			    "Exception: unknown clid %d\n",
			    clid));
		}
		e.clear();
		//
		// Caller should check for the return value
		//
		return (naming::naming_context::_nil());
	}

	cz_ctx_v = naming::naming_context::_narrow(obj_p);

	if (CORBA::is_nil(cz_ctx_v)) {
		return (naming::naming_context::_nil());
	}

	// Return the context
	return (cz_ctx_v);
}

//
// This will be used to get the root/global context of a cz
// from the global zone only. If this function is
// invoked from a cz or local zone, then nil is returned.
//
naming::naming_context_ptr
ns::root_nameserver_cz(const char *cz_name)
{ //lint !e578
	zoneid_t			zid;
	int				clid;
	CORBA::Object_ptr		obj_p;
	naming::naming_context_var 	cz_ctxp;
	Environment			e;
	CORBA::Exception 		*ex;
	char				clid_str[30];

	zid = getzoneid();
	if (zid != GLOBAL_ZONEID) {
		//
		// This function should be
		// invoked from the global zone only
		//
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}

	if (cz_name == NULL) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}

	clid = ns::get_cz_id_by_name(cz_name);
	if (clid < 0) {
		//
		// Unable to find cluster id
		//
		return (naming::naming_context::_nil());
	}

	if ((clid == GLOBAL_ZONEID) && (os::strcmp(cz_name, "global") != 0)) {
		//
		// The target zone being queried for is not a
		// clusterized zone. This is not allowed.
		//
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}

	// Store the cluster id as a string
	os::sprintf(clid_str, "%d", clid);

	//
	// Initialize the local name server
	// if its not already initialized
	//
	if (!root_nameserver_init()) {
		return (naming::naming_context::_nil());
	}

	if (CORBA::is_nil(global_nmsrvr)) {
		ASSERT(0);
		NS_PRINTF(NS_RESOLVE, ("global_nameserver_cz: "
		    "global_nmsrvr is nil. clid %d\n", clid)); //lint !e527
		return (naming::naming_context::_nil());
	}


	obj_p = global_nmsrvr->resolve(clid_str, e);
	if ((ex = e.exception()) != NULL) {
		NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: occured clid %d e = %p\n",
			    clid, &e));
		if (naming::not_found::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: not_found clid %d\n",
			    clid));
		} else if (naming::cannot_proceed::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: cannot_proceed clid %d\n",
			    clid));
		} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: invalid_name clid %d\n",
			    clid));
		} else if (CORBA::CTX_EACCESS::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: CTX_EACCESS clid %d\n",
			    clid));
		} else if (CORBA::NS_EPERM::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: NS_EPERM clid %d\n",
			    clid));
		} else {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: unknown clid %d\n",
			    clid));
		}
		e.clear();
		//
		// Caller should check for the return value
		//
		return (naming::naming_context::_nil());
	}

	cz_ctxp = naming::naming_context::_narrow(obj_p);

	if (CORBA::is_nil(cz_ctxp)) {
		return (naming::naming_context::_nil());
	}

	// Return the context
	return (cz_ctxp);
}

/*
naming::naming_context_ptr
ns::root_nameserver_cz()
{
	zoneid_t			zid;
	char				name[ZONENAME_MAX];
	uint_t				clid;
	int				ret;
	CORBA::Object_ptr		obj_p;
	naming::naming_context_var 	cz_ctxp;
	Environment			e;
	CORBA::Exception 		*ex;
	char				clid_str[30];

	zid = getzoneid();
#ifdef _KERNEL
	zone_t *zonep = zone_find_by_id(zid);
	(void) os::strcpy(name, zonep->zone_name);
	zone_rele(zonep);

	ret = clconf_get_cluster_id(name, &clid);
	if (ret == ENOENT) {
		//
		// The name is not a valid cluster
		// Can be only 1334 zone
		// Return the context 0
		//
		return (ns::root_nameserver_c0());
	} else if (ret == EACCES) {
		ASSERT(0);
	}

	ASSERT(ret == 0);
#else
	// Userland
	if (getzonenamebyid(zid, name, ZONENAME_MAX) == -1) {
		// error in getting name
		return (naming::naming_context::_nil());
	}
	cz_id_t czid;
	czid.clid = 0;
	(void) os::strcpy(czid.name, name);
	if ((ret = os::cladm(CL_CONFIG, CL_GET_ZC_ID, &czid)) != 0) {
		os::printf("Failed to get cluster id, ret val = %d\n", ret);
		ASSERT(0);
	}
	clid = czid.clid;

	//
	// If its the global zone or a 1334 zone
	// return physical cluster context
	//
	if (clid == 0 || clid == 1 || clid == 1) {
		return (ns::root_nameserver_c0());
	}
#endif
	//
	// A valid clusterized zone made this call
	// Get the root of the local name server and
	// extract the context corresponding to the cz.
	//

	naming::naming_context_var ctxp = ns::root_nameserver_root();

#ifdef _KERNEL
	//
	// The global nameserver root should have been
	// created and initialized by now. If you are
	// hitting the assert, you are trying to use
	// the global nameserver before it is initialized.
	//
	ASSERT(!CORBA::is_nil(ctxp));
#endif
	if (CORBA::is_nil(ctxp)) {
		//
		// TODO: Put code to reinitialize
		// You must do this for userland
		// Write now keep an assert so that
		// you will know that this can really happen
		//
		ASSER(0);
		return (naming::naming_context::_nil());
	}

	os::sprintf(clid_str, "%d", clid);
	obj_p = ctxp->resolve(clid_str, e);
	if ((ex = e.exception()) != NULL) {
		if (naming::not_found::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: not_found clid %d\n",
			    clid));
		} else if (naming::cannot_proceed::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: cannot_proceed clid %d\n",
			    clid));
		} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: invalid_name clid %d\n",
			    clid));
		} else {
			NS_PRINTF(NS_RESOLVE, ("root_nameserver_cz: "
			    "Exception: unknown clid %d\n",
			    clid));
		}
		e.clear();
		//
		// Caller should check for the return value
		//
		return (naming::naming_context::_nil());
	}

	cz_ctxp = naming::naming_context::_narrow(obj_p);

	if (CORBA::is_nil(cz_ctxp)) {
		return (naming::naming_context::_nil());
	}

	return (cz_ctxp);
}
*/

#endif	// end of #if (SOL_VERSION >= __s10) && !defined(UNODE)

#if (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)
//
//
// This function will tell if the zone is a local
// native zone or not. If this is called from the
// global zone, then this will treat it as a
// native zone
//
// Return Value:
// 	true: For global zone and native local zones
// 	false: For any other branded zone
//
// DO NOT USE THIS FUNCTION TILL CR 6725782 is fixed
//
bool
ns::is_native_zone()
{
	int			error;
	char			zone_name[ZONENAME_MAX];
	char			zone_brand[MAXNAMELEN];
	zoneid_t		zid;
	bool			retval = false;

	// DO NOT USE THIS FUNCTION TILL CR 6725782 is fixed
	ASSERT(0);

	zid = getzoneid(); //lint !e527

	if (zid == GLOBAL_ZONEID) {
		// Global zone is always native
		return (true);
	}

	if (getzonenamebyid(zid, zone_name, sizeof (zone_name)) < 0) {
		NS_PRINTF(NS_DUMP_ALL, ("ns:is_native_zone: "
		    "Unable to get zone name with zid = %d\n", zid));
		ASSERT(0);
		return (false); //lint !e527
	}


	error = zone_get_brand(zone_name, zone_brand, sizeof (zone_brand));
	if (error != Z_OK) {
		NS_PRINTF(NS_DUMP_ALL, ("ns:is_native_zone: "
		    "Unable to get zone brand zid = %d zone_name = %s\n",
		    zid, zone_name));
		ASSERT(0);
		return (false); //lint !e527
	}

	if (os::strcmp(zone_brand, NATIVE_BRAND_NAME) == 0) {
		// Native local zone
		retval = true;
	} else {
		// Some branded zone
		retval = false;
	}

	return (retval);
}

#endif // end of #if (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)

//
// External interface for cl_orb
//
extern "C" naming::naming_context_ptr
local_nameserver_root(nodeid_t node)
{
	if (node == (nodeid_t)NULL) {
		return (ns::local_nameserver_root(NODEID_UNKNOWN));
	} else {
		return (ns::local_nameserver_root(node));
	}
}

//
// Return an object reference to the local name server.
//
naming::naming_context_ptr
ns::local_nameserver_root(nodeid_t)
{
	//
	// This function is called from inside a zone
	// cluster. Do not prevent access to this
	// function from a zone cluster. It is safe
	// as the name server kernel security parameters
	// and special checks prevent misuse of this
	// context.
	//

	//
	// Initialize the local nameserver
	// if not already initialized.
	//
	(void) local_nameserver_init();

	if (CORBA::is_nil(local_nmsrvr)) {
		return (naming::naming_context::_nil());
	} else {
		return (naming::naming_context::_duplicate(local_nmsrvr));
	}
}

naming::naming_context_ptr
ns::local_nameserver_c0(nodeid_t)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// If call comes from a clusterised zone
	// then return nil. A cz does not have access
	// to global zone name server related context.
	//
	int id = ns::get_cz_id();
	if ((id != 0) && (id != 2)) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}
#endif
	//
	// Initialize the local nameserver
	// if not already initialized.
	//
	(void) local_nameserver_init();

	if (CORBA::is_nil(local_nmsrvr_c0)) {
		return (naming::naming_context::_nil());
	} else {
		return (naming::naming_context::_duplicate(local_nmsrvr_c0));
	}
}

naming::naming_context_ptr
ns::local_nameserver_c1(nodeid_t)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifndef _KERNEL
	//
	// If call comes from a clusterised zone
	// then return nil. A cz does not have access
	// to global zone name server related context.
	//
	int id = ns::get_cz_id();
	if ((id != 0) && (id != 2)) {
		ASSERT(0);
		return (naming::naming_context::_nil()); //lint !e527
	}
#endif
#endif
	//
	// Initialize the local nameserver
	// if not already initialized.
	//
	(void) local_nameserver_init();

	if (CORBA::is_nil(local_nmsrvr_c1)) {
		return (naming::naming_context::_nil());
	} else {
		return (naming::naming_context::_duplicate(local_nmsrvr_c1));
	}
}

CORBA::Object_ptr
ns::wait_resolve(const char *nsrvr, Environment &e)
{
	naming::naming_context_var ctxp = root_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));
	return (wait_resolve_private(nsrvr, ctxp, e));
}

CORBA::Object_ptr
ns::wait_resolve_local(const char *nsrvr, Environment &e)
{
	naming::naming_context_var ctxp = local_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));
	return (wait_resolve_private(nsrvr, ctxp, e));
}

CORBA::Object_ptr
ns::wait_resolve_private(const char *nsrvr, naming::naming_context_ptr ctxp,
    Environment &e)
{
	CORBA::Object_ptr the_obj;
#ifdef	DEBUG
	int i = 0;
#endif

	for (;;) {
		the_obj = ctxp->resolve(nsrvr, e);

		if (e.exception()) {
			if (naming::not_found::_exnarrow(e.exception())) {
				// retry
				ASSERT(CORBA::is_nil(the_obj));
				e.clear();
			} else {
				// return exception
				break;
			}
		} else {
			// return the object, nil or not
			break;
		}
		os::usecsleep(100000L);
#ifdef	DEBUG
		i++;
		if ((i % 300) == 0) {
			NS_PRINTF(NS_RESOLVE,
			    ("ns::wait_resolve hasn't found %s "
			    "for %d seconds\n", nsrvr, i / 10));
		}
#endif
	}

	return (the_obj);
}

//
// Will return the running version for the naming interface
version_manager::vp_version_t
ns::get_ns_version()
{
	Environment 				env;
	CORBA::Exception			*ex;
	version_manager::vp_version_t		ns_version;
	version_manager::vm_admin_var 		vm_adm_v;

#ifdef _KERNEL
	//
	// For Kernel use the current version in
	// repl_ns_server (checkpoint class)
	//
	ns_version = repl_ns_server::get_current_version();
	return (ns_version);
#endif

	//
	// For user land use the version provided
	// by the version manager framework.
	//

	// get vm_admin object
	vm_adm_v = vm_util::get_vm(); //lint !e527
	if (CORBA::is_nil(vm_adm_v)) {
		// Something has gone wrong, panic
		CL_PANIC(0);
	}

	vm_adm_v->get_running_version("naming", ns_version, env);
	if ((ex = env.exception()) != NULL) {
		// Something has gone wrong
		if (version_manager::old_incarnation::_exnarrow(ex)) {
			env.exception()->print_exception(
			    "get_ns_version: Exception: old_incarnation\n");
			ASSERT(0);
		} else if (version_manager::not_found::_exnarrow(ex)) {
			env.exception()->print_exception(
			    "get_ns_version: Exception: not_found\n");
			ASSERT(0);
		} else if (version_manager::wrong_mode::_exnarrow(ex)) {
			env.exception()->print_exception(
			    "get_ns_version: Exception: wrong_mode\n");
			ASSERT(0);
		} else if (version_manager::too_early::_exnarrow(ex)) {
			env.exception()->print_exception(
			    "get_ns_version: Exception: too_early\n");
			ASSERT(0);
		} else {
			// should never come here
			ASSERT(0);
		}
		//
		// TODO:
		// If we hit this asserts above a number of times, we need
		// to decide what to do when we are unable to get
		// the running version.
		//
		env.clear(); //lint !e527
	}

	return (ns_version);
}

#ifdef _KERNEL_ORB

//
// Initialize the global name server.
//
int
ns::init_nameserver()
{
	Environment e;
	char local_nameserver_name[24]; // "local_ns_NNNNN\0"

	//
	// Create the global HA name server.
	// Make sure you hold the lock.
	//
	gns_lock.lock();
	(void) create_global_nameserver(e);
	gns_lock.unlock();
	if (e.exception() != NULL) {
		return (EIO);
	}

	while (1) {
		if (!CORBA::is_nil(global_nmsrvr)) {
			break;
		}
		os::usecsleep(200000L);
	}

	naming::naming_context_var lctxp = ns::local_nameserver_root();

	lctxp->bind("gbl_ns", global_nmsrvr, e);

	if (e.exception() != NULL) {
		e.exception()->print_exception("init_nameserver: Can't bind "
			"global nmsrvr");
		e.clear();
		return (EIO);
	}

	(void) os::snprintf(local_nameserver_name, 24UL,
	    local_nameserver_template, orb_conf::local_nodeid());

	global_nmsrvr->rebind(local_nameserver_name, lctxp, e);

	if (e.exception() != NULL) {
		e.exception()->print_exception("init_nameserver: Can't bind "
			"local nameserver into global nameserver");
		e.clear();
		return (EIO);
	}

	NS_PRINTF(NS_DUMP_ALL, ("ns::init_nameserver: COMPLETE\n"));

	// Success
	return (0);
}

//
// Create the local name server and associate it with
// a well known xdoor <node, XDOOR_LOCAL_NS>
//
naming::naming_context_ptr
ns::create_local_nameserver()
{
	Environment e;
	naming_context_root *local_ctx;
	naming::naming_context_ptr lctxp;

	local_ctx = new naming_context_root(XDOOR_LOCAL_NS,
	    false, NS_ALLOW_ALL_OPP_PERM | NS_ALLOW_ALL_ZONE_READ_PERM,
	    GLOBAL_ZONEID);
	NS_PRINTF(NS_DUMP_ALL, ("create_local_nameserver: "
	    "local_ctx = %p\n", local_ctx));
	local_nmsrvr_obj = local_ctx;

	lctxp = local_ctx->get_objref();

	//
	// Initialize the namespace by creating various
	// contexts in the root context.
	//
	naming_context_ii::init_namespace((naming_context_ii *)local_ctx,
	    LOCAL_NS, e);

	return (lctxp);
}

//
// Create the global name server.
//
naming::naming_context_ptr
ns::create_global_nameserver(Environment &)
{
	repl_ns_server::initialize("repl_name_server");

	return (naming::naming_context::_nil());
}

//
// is_root_ns_initialized
//
// returns value of root_ns_initialized
//
bool ns::root_ns_initialized = false;

bool
ns::is_root_ns_initialized()
{
	return (root_ns_initialized);
}

void
ns::set_root_ns_initialized()
{
	root_ns_initialized = true;
}

void
ns::unset_root_ns_initialized()
{
	root_ns_initialized = false;
}

//
// This function is used to bind the context of a cz
// only. This should not be used to bind any subcontext
// of a cz.
// This function will not unbind context 0 and 1
//
// Parameter:
// 	ctx_name: It is the context name
//		  The virtual cluster id or cz id
// 	ns_type:  The name server type
//		  NONE_NS, LOCAL_NS, GLOBAL_NS
//
// Return Value:
// 	true:	  On success
// 	false:	  On faliure
//
bool
ns::bind_cz_context(const char *ctx_name, const int ns_type,
    bool lf, uint32_t perm, uint32_t own)
{
	Environment			e;
	CORBA::Exception 		*ex;
	CORBA::Object_ptr 		nsobj_p = nil;
	naming::naming_context_ptr	nsctx_p = nil;
	naming::naming_context_var	ctx_v;
	version_manager::vp_version_t 	ns_version;

	ASSERT(ctx_name);
	if (ctx_name == NULL) {
		NS_PRINTF(NS_BIND_CTX, ("bind_cz_context: "
		    "Context name is null\n"));
		return (false);
	}

	if (ns_type == NONE_NS) {
		NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	if ((ns_type != LOCAL_NS) && (ns_type != GLOBAL_NS)) {
		NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s ns_type %d\n",
	    ctx_name, ns_type));

	ns_version = ns::get_ns_version();
	if ((ns_version.major_num < 2) && (ns_type == GLOBAL_NS)) {
		//
		// Old version is running. Nothing to do.
		// The assumption is that there are no
		// zone clusters yet.
		//
		CL_PANIC(0);
		return (true); //lint !e527
	}

	//
	// If someone tries to bind context ALL_CONTEXT,
	// PHYSICAL_CONTEXT or a 1334 zone type context
	// return true without doing anything.
	//
	if ((os::strcmp(ctx_name, "0") == 0) ||
	    (os::strcmp(ctx_name, "1") == 0) ||
	    (os::strcmp(ctx_name, "2") == 0)) {
		return (true);
	}

	if (ns_type == LOCAL_NS) {
		ctx_v = ns::local_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("bind_cz_context %s "
			    "Failed to get local name server\n", ctx_name));
			return (false);
		}
	} else if (ns_type == GLOBAL_NS) {
		ctx_v = ns::root_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("bind_cz_context %s "
			    "Failed to get root name server\n", ctx_name));
			return (false);
		}
	}

	nsobj_p = ctx_v->bind_new_context_v1(ctx_name, lf, perm, own, e);
	if ((ex = e.exception()) != NULL) {
		NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
		    "Failed to create contex in name server %d\n",
		    ctx_name, ns_type));
		if (naming::not_found::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
			    "Exception: not_found ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::cannot_proceed::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
			    "Exception: cannot_proceed ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
			    "Exception: invalid_name ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::already_bound::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
			    "Exception: already_bound ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::CTX_EACCESS::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
			    "Exception: CORBA::CTX_EACCESS ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::NS_EPERM::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("bind_cz_context %s "
			    "Exception: CORBA::NS_EPERM ns_type %d\n",
			    ctx_name, ns_type));
		}
		e.clear();
		return (false);
	}
	nsctx_p = naming::naming_context::_narrow(nsobj_p);
	nsctx_p->cz_set_nameserver_type(ns_type, e);
	if (e.exception()) {
		e.clear();
		CORBA::release(nsobj_p);
		return (false);
	}

	CORBA::release(nsobj_p);
	CORBA::release(nsctx_p);

	// Success
	return (true);
}

//
// This function is used to rebind the context of a cz
// only. This should not be used to rebind any subcontext
// of a cz.
// This function will not rebind context 0 and 1
//
// Parameter:
// 	ctx_name: It is the context name
//		  The virtual cluster id or cz id
// 	ns_type:  The name server type
//		  NONE_NS, LOCAL_NS, GLOBAL_NS
//	lf:	  Look further. If set will look in
//		  the ALL_CONTEXT
//	perm:	  Permission for the context
//	own:	  Owner of the context
//
// Return Value:
// 	true:	  On success
// 	false:	  On faliure
//
bool
ns::rebind_cz_context(const char *ctx_name, const int ns_type,
    bool lf, uint32_t perm, uint32_t own)
{
	Environment			e;
	CORBA::Exception 		*ex;
	naming::naming_context_var	ctx_v;
	version_manager::vp_version_t	ns_version;
	naming::naming_context_ptr	ns_ctx;

	ns_ctx = naming::naming_context::_nil();

	ASSERT(ctx_name);
	if (ctx_name == NULL) {
		NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context: "
		    "Context name is null\n"));
		return (false);
	}

	if (ns_type == NONE_NS) {
		NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	if ((ns_type != LOCAL_NS) && (ns_type != GLOBAL_NS)) {
		NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s ns_type %d\n",
	    ctx_name, ns_type));

	//
	// Get the name server version and verify if the
	// latest version is running
	//
	ns_version = ns::get_ns_version();
	if ((ns_version.major_num < 2) && (ns_type == GLOBAL_NS)) {
		//
		// Old version is running. This function should
		// not get called.
		//
		CL_PANIC(0);
		return (true); //lint !e527
	}

	//
	// If someone tries to rebind context ALL_CONTEXT,
	// PHYSICAL_CONTEXT or a 1334 zone type context
	// return true without doing anything.
	//
	if ((os::strcmp(ctx_name, "0") == 0) ||
	    (os::strcmp(ctx_name, "1") == 0) ||
	    (os::strcmp(ctx_name, "2") == 0)) {
		return (true);
	}

	if (ns_type == LOCAL_NS) {
		ctx_v = ns::local_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("rebind_cz_context %s "
			    "Failed to get local name server\n", ctx_name));
			return (false);
		}
	} else if (ns_type == GLOBAL_NS) {
		ctx_v = ns::root_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("rebind_cz_context %s "
			    "Failed to get root name server\n", ctx_name));
			return (false);
		}
	}

	ns_ctx = ctx_v->rebind_existing_context_v1(ctx_name, lf, perm, own, e);
	if (((ex = e.exception()) != NULL) || (CORBA::is_nil(ns_ctx))) {
		NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
		    "Failed to create contex in name server %d\n",
		    ctx_name, ns_type));
		if (naming::not_found::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
			    "Exception: not_found ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::cannot_proceed::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
			    "Exception: cannot_proceed ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
			    "Exception: invalid_name ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::already_bound::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
			    "Exception: already_bound ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::CTX_EACCESS::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
			    "Exception: CORBA::CTX_EACCESS ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::NS_EPERM::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("rebind_cz_context %s "
			    "Exception: CORBA::NS_EPERM ns_type %d\n",
			    ctx_name, ns_type));
		}
		e.clear();
		return (false);
	}

	return (true);
}

//
// This function is used to unbind the context of a cz
// only. This should not be used to bind any subcontext
// of a cz.
// This function will not unbind context 0 and 1
//
// Parameter:
// 	ctx_name: It is the context name
//		  The virtual cluster id or cz id
// 	ns_type:  The name server type
//		  NONE_NS, LOCAL_NS, GLOBAL_NS
//
// Return Value:
//	true:	  On success
// 	false:	  On faliure
//
bool
ns::unbind_cz_context(const char *ctx_name, const int ns_type)
{
	Environment			e;
	CORBA::Exception 		*ex;
	naming::naming_context_var	ctx_v;
	version_manager::vp_version_t	ns_version;
	bool				ret_flag;

	ASSERT(ctx_name);
	if (ctx_name == NULL) {
		NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context: "
		    "Context name is null\n"));
		return (false);
	}

	if (ns_type == NONE_NS) {
		NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	if ((ns_type != LOCAL_NS) && (ns_type != GLOBAL_NS)) {
		NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	ns_version = ns::get_ns_version();
	if ((ns_version.major_num < 2) && (ns_type == GLOBAL_NS)) {
		//
		// Old version is running. This function should
		// not get called.
		//
		CL_PANIC(0);
		return (true); //lint !e527
	}

	NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s ns_type %d\n",
	    ctx_name, ns_type));

	//
	// If someone tries to bind context ALL_CONTEXT,
	// PHYSICAL_CONTEXT or a 1334 zone type context
	// return true without doing anything.
	//
	if ((os::strcmp(ctx_name, "0") == 0) ||
	    (os::strcmp(ctx_name, "1") == 0) ||
	    (os::strcmp(ctx_name, "2") == 0)) {
		return (true);
	}

	if (ns_type == LOCAL_NS) {
		ctx_v = ns::local_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("unbind_cz_context %s "
			    "Failed to get local name server\n", ctx_name));
			return (false);
		}
	} else if (ns_type == GLOBAL_NS) {
		ctx_v = ns::root_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("unbind_cz_context %s "
			    "Failed to get root name server\n", ctx_name));
			return (false);
		}
	}

	ret_flag = ctx_v->unbind_existing_context_v1(ctx_name, e);
	if (((ex = e.exception()) != NULL) || (!ret_flag)) {
		NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
		    "Failed to create contex in name server %d\n",
		    ctx_name, ns_type));
		if (naming::not_found::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
			    "Exception: not_found ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::cannot_proceed::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
			    "Exception: cannot_proceed ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
			    "Exception: invalid_name ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::already_bound::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
			    "Exception: already_bound ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::CTX_EACCESS::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
			    "Exception: CORBA::CTX_EACCESS ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::NS_EPERM::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("unbind_cz_context %s "
			    "Exception: CORBA::NS_EPERM ns_type %d\n",
			    ctx_name, ns_type));
		}
		e.clear();
		return (false);
	}

	return (true);
}

//
// This function is used to check if the context of a cz
// is present. This should not be used to check for sub
// contexts.
//
// This function will return true for context 0 and 1
//
// Parameter:
// 	ctx_name: It is the context name
//		  The virtual cluster id or cz id
// 	ns_type:  The name server type
//		  NONE_NS, LOCAL_NS, GLOBAL_NS
//
// Return Value:
//	true:	On success
// 	false:	On faliure
//
bool
ns::check_cz_context(const char *ctx_name, const int ns_type)
{
	Environment			e;
	CORBA::Exception 		*ex;
	naming::naming_context_var	ctx_v;
	CORBA::Object_ptr		obj_p =  CORBA::Object::_nil();

	ASSERT(ctx_name);
	if (ctx_name == NULL) {
		NS_PRINTF(NS_BIND_CTX, ("check_cz_context: "
		    "Context name is null\n"));
		return (false);
	}

	if (ns_type == NONE_NS) {
		NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	if ((ns_type != LOCAL_NS) && (ns_type != GLOBAL_NS)) {
		NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s : "
		    "ns_type = %d\n", ctx_name, ns_type));
		return (false);
	}

	NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s ns_type %d\n",
	    ctx_name, ns_type));

	//
	// If someone tries to check context ALL_CONTEXT,
	// PHYSICAL_CONTEXT or a 1334 zone type context
	// return true without doing anything.
	//
	if ((os::strcmp(ctx_name, "0") == 0) ||
	    (os::strcmp(ctx_name, "1") == 0) ||
	    (os::strcmp(ctx_name, "2") == 0)) {
		return (true);
	}

	if (ns_type == LOCAL_NS) {
		ctx_v = ns::local_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("check_cz_context %s "
			    "Failed to get local name server\n", ctx_name));
			return (false);
		}
	} else if (ns_type == GLOBAL_NS) {
		ctx_v = ns::root_nameserver_root();
		ASSERT(!CORBA::is_nil(ctx_v));
		if (CORBA::is_nil(ctx_v)) {
			NS_PRINTF(NS_BIND_CTX,
			    ("check_cz_context %s "
			    "Failed to get root name server\n", ctx_name));
			return (false);
		}
	}

	obj_p = ctx_v->resolve(ctx_name, e);
	if ((ex = e.exception()) != NULL) {
		NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s "
		    "Failed to get contex in name server %d\n",
		    ctx_name, ns_type));
		if (naming::not_found::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s "
			    "Exception: not_found ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::cannot_proceed::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s "
			    "Exception: cannot_proceed ns_type %d\n",
			    ctx_name, ns_type));
		} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s "
			    "Exception: invalid_name ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::CTX_EACCESS::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s "
			    "Exception: CORBA::CTX_EACCESS ns_type %d\n",
			    ctx_name, ns_type));
		} else if (CORBA::NS_EPERM::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("check_cz_context %s "
			    "Exception: CORBA::NS_EPERM ns_type %d\n",
			    ctx_name, ns_type));
		}
		e.clear();
		return (false);
	} else if (CORBA::is_nil(obj_p)) {
		return (false);
	}

	// The cz contextx exists
	return (true);
}

//
// This function will be used to create the cz
// context in the global name server at  boot time.
// Do not use it for any other purpose.
//
// Return Value:
// 	true:	If all contexts were created successfully
// 	false: In case of failure
//
// The node will panic if context creation fails due
// to some exception.
//
bool
ns::init_global_context(naming_context_ii *root_ctx, Environment &e)
{
	CORBA::Exception 		*ex;
	CORBA::Object_var		obj;
	ccr::element_seq_var 		seq_v;
	ccr::directory_var 		ccr_dir_v;
	ccr::readonly_table_ptr 	tabptr;
	naming::naming_context_var	local_ctx_v;
	int32_t				num_elem = 0;
	CORBA::Object_ptr 		nsobj_p = nil;
	naming::naming_context_ptr	nsctx_p = nil;
	version_manager::vp_version_t 	ns_version;

	//
	// Check the version of name server
	// software running. If old version,
	// then there is nothing to do.
	//
	ns_version = ns::get_ns_version();
	if (ns_version.major_num < 2) {
		//
		// Old version is running. Nothing to do.
		// The assumption is that there are no
		// zone clusters yet.
		//
		return (true);
	} else {
		// New version is running
		NS_PRINTF(NS_BIND_CTX, ("ns::init_global_context "
		    "global_nmsrvr = %p NEW VERSION \n",
		    global_nmsrvr));
	}

	local_ctx_v = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(local_ctx_v));
	if (CORBA::is_nil(local_ctx_v)) {
		NS_PRINTF(NS_BIND_CTX,
		    ("init_global_context: "
		    "Failed to get local name server e = %p\n", &e));
		ASSERT(0);
		return (false); //lint !e527
	}

	obj = local_ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		NS_PRINTF(NS_BIND_CTX,
		    ("init_global_context: "
		    "Failed to get ccr_directory e = %p\n", &e));
		ASSERT(0);
		e.clear(); //lint !e527
		return (false);
	}
	ccr_dir_v = ccr::directory::_narrow(obj);

	tabptr = ccr_dir_v->lookup_zc("global", "cluster_directory", e);
	if (e.exception()) {
		NS_PRINTF(NS_BIND_CTX,
		    ("init_global_context: "
		    "Failed to get cluster_directory e = %p\n", &e));
		ASSERT(0);
		e.clear(); //lint !e527
		return (false);
	}

	num_elem = tabptr->get_num_elements(e);
	if (e.exception()) {
		NS_PRINTF(NS_BIND_CTX,
		    ("init_global_context: "
		    "Failed to get num elements. e = %p\n", &e));
		ASSERT(0);
		e.clear(); //lint !e527
		return (false);
	}

	tabptr->atfirst(e);
	if (e.exception()) {
		NS_PRINTF(NS_BIND_CTX,
		    ("init_global_context: "
		    "Failed to rewind. e = %p\n", &e));
		ASSERT(0);
		e.clear(); //lint !e527
		return (false);
	}

	tabptr->next_n_elements((uint32_t)num_elem, seq_v, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("init_global_context: "
			    "Failed to get elements "
			    "Exception: table_modified\n"));
		} else if (ccr::system_error::_exnarrow(ex) != NULL) {
			NS_PRINTF(NS_BIND_CTX, ("init_global_context: "
			    "Failed to get elements "
			    "Exception: system_error\n"));
		} else {
			NS_PRINTF(NS_BIND_CTX, ("init_global_context: "
			    "Failed to get elements "
			    "Exception: UNKNOWN\n"));
		}
		ASSERT(0);
		e.clear(); //lint !e527
		return (false);
	}

	num_elem = (int32_t)seq_v->length();

	for (unsigned int i = 0; i < (unsigned int)num_elem; i++) {
		// No need to create context for physical cluster
		//lint -e1023
		//lint -e1703
		//lint -e62
		//lint -e732
		if (os::strcmp("global", (char *)seq_v[i].key) == 0) {
			continue;
		}

		uint32_t cluster_id = (uint_t)os::atoi(seq_v[i].data);

		nsobj_p =
		    root_ctx->bind_new_context_v1((char *)seq_v[i].data,
		    true, NS_ALLOW_ALL_OPP_PERM, cluster_id, e);

		if ((ex = e.exception()) != NULL) {
			if (naming::already_bound::_exnarrow(ex) != NULL) {
				NS_PRINTF(NS_BIND_CTX,
				    ("init_global_context: %s "
				    "Failed to create cz glbl ctx "
				    "Exception: already_bound ... IGNORE\n",
				    (char *)seq_v[i].data));
				//
				// Ignore this error as some other node in
				// the cluster has bound the context. This
				// is fine.
				//
				e.clear();
				continue;
			} else if (naming::not_found::_exnarrow(ex) != NULL) {
				NS_PRINTF(NS_BIND_CTX,
				    ("init_global_context: %s "
				    "Failed to create cz glbl ctx "
				    "Exception: not_found\n",
				    (char *)seq_v[i].data));
			} else if (naming::cannot_proceed::_exnarrow(ex)
			    != NULL) {
				NS_PRINTF(NS_BIND_CTX,
				    ("init_global_context: %s "
				    "Failed to create cz glbl ctx "
				    "Exception: cannot_proceed\n",
				    (char *)seq_v[i].data));
			} else if (naming::invalid_name::_exnarrow(ex)
			    != NULL) {
				NS_PRINTF(NS_BIND_CTX,
				    ("init_global_context: %s "
				    "Failed to create cz glbl ctx "
				    "Exception: invalid_name\n",
				    (char *)seq_v[i].data));
			} else {
				NS_PRINTF(NS_BIND_CTX,
				    ("init_global_context: %s "
				    "Failed to create cz glbl ctx "
				    "Exception: UNKNOWN\n",
				    (char *)seq_v[i].data));
			}
		//lint +e1023
		//lint +e1703
		//lint +e62
		//lint +e732

			//
			// Failed to create the CZ context in the global
			// name server. This is a critical error.
			// Panic the node.
			//
			CL_PANIC(0);

			//
			// This will never be reached. Exception should
			// not be cleared before the CL_PANIC as it will
			// not be possible to find out the exception from
			// the dump.
			//
			e.clear(); //lint !e527
		}
		nsctx_p = naming::naming_context::_narrow(nsobj_p);

		CORBA::release(nsobj_p);
		CORBA::release(nsctx_p);
	}

	// All contexts were bound successfully
	return (true);
}

//
// This function is meant to print all the
// global nameserver contexts and binding
// to the name server debug buffer
//
void
ns::print_global_nameserver(naming_context_repl_impl *ctx,
    char *cname, uint32_t depth)
{
	char ctx_name[512];
	ASSERT(cname != NULL);
	nc_error error;

	if (ctx == NULL) {
		return;
	}

	NS_PRINTF(NS_BIND_CTX, ("Context Name :%s (%p) replicated %d "
	    "ns_type %d lf %d perm %d own %d\n",
	    cname,
	    ctx,
	    ctx->get_replicated(),
	    ctx->get_nameserver_type(),
	    ctx->get_look_further(),
	    ctx->get_permission(),
	    ctx->get_owner()));

	uint32_t binding_count = ctx->get_store()->get_binding_count();
	binding **bindings = new binding*[binding_count];
	error = ctx->get_store()->get_all_bindings(bindings, binding_count);
	ASSERT(error == nc_ok);

	for (uint32_t index = 0; index < binding_count; index++) {
		switch (bindings[index]->get_binding_type()) {
		case binding::bt_obj:
			NS_PRINTF(NS_BIND_CTX,
			    ("Context Name :%s (%p) Obj_Name :%s (%p)\n",
			    cname, ctx, bindings[index]->get_name(),
			    bindings[index]));
			bindings[index]->release();
			break;
		case binding::bt_local_ctx:
		case binding::bt_remote_ctx:
			(void) os::strcpy(ctx_name, cname);
			(void) strcat(ctx_name, "/");
			(void) strcat(ctx_name, bindings[index]->get_name());
			//lint -e1774
			print_global_nameserver((naming_context_repl_impl*)
			    (bindings[index]->get_store_obj()->get_ctx()),
			    ctx_name, depth + 1);
			//lint +e1774
			bindings[index]->release();
			break;
		default:
			//
			// TODO:
			// This is experimental. Need to find out
			// if there are bindings with bt_none
			ASSERT(0);
			bindings[index]->release(); //lint !e527
		} // End of switch
	} // End of for

	// Free the memory
	delete [] bindings;
}

//
// This function migrates the nameserver objects from version 1.0
// to version 2.0. This gets called during a scversions -c command
// during a rolling upgrade. On the primary, this function gets
// executed from the upgrade_callback routine of the repl_ns_server.
// On the secondary, the version number checkpoint will execute this
// function.
//
// This function takes the root of the global nameserver. It is the
// C++ object resciding on the current node. Then it will traverse
// the objects and try to find out the Context 0 and 1. Then in the
// next traversal, it will move the bindings and contexts from the
// real root, to context 0. Some special contexts are not migrated.
//
void
ns::migrate_global_nameserver(naming_context_repl_impl *root_ctx)
{
	nc_error 			nc_err;
	naming_context_repl_impl	*ctx_0 = NULL;
	naming_context_repl_impl	*ctx_1 = NULL;
//	naming_context_repl_impl	*ctx_2 = NULL;

	ASSERT(root_ctx != NULL);

	binding_store *store_ptr = root_ctx->get_store();
	volatile_store::bnode *bnode_lst =
	    ((volatile_store*)root_ctx->get_store())->get_blist(); //lint !e1774
	volatile_store::bnode *tmp_bnode_lst = bnode_lst;

	ASSERT(store_ptr != NULL);
	ASSERT(bnode_lst != NULL);

	//
	// Get the pointers to the context 0 and 1
	// We need to get it here as we do not
	// want to use the cached references.
	//
	while ((ctx_0 == NULL || ctx_1 == NULL) && (tmp_bnode_lst != NULL)) {
		volatile_binding *tmp_vb = tmp_bnode_lst->vbinding;
		char *name = tmp_vb->get_name();
		if (os::strcmp(name, "0") == 0) {
			binding_store *tmp_store = tmp_vb->get_store_obj();
			//lint -e1774
			ctx_0 = (naming_context_repl_impl*)tmp_store->get_ctx();
			//lint +e1774
			ASSERT(ctx_0 != NULL);

			// Change the look_further value here
			ctx_0->set_look_further(true);

			// Set the kernel security parameters
			ctx_0->set_permission(NS_ALLOW_ALL_OPP_PERM);

		} else if (os::strcmp(name, "1") == 0) {
			binding_store *tmp_store = tmp_vb->get_store_obj();
			//lint -e1774
			ctx_1 = (naming_context_repl_impl*)tmp_store->get_ctx();
			//lint +e1774
			ASSERT(ctx_1 != NULL);

			// Set the value of context 1 look_further to false
			ctx_1->set_look_further(false);

			// Set the kernel security parameters
			ctx_1->set_permission(NS_ALLOW_ALL_OPP_PERM |
			    NS_ALLOW_ALL_ZONE_READ_PERM);
		}
		tmp_bnode_lst = tmp_bnode_lst->next;
	}

	ASSERT(ctx_0 != NULL);
	ASSERT(ctx_1 != NULL);

	NS_PRINTF(NS_DUMP_ALL, (
	    "migrate_global_nameserver: Context Name :%s (%p) replicated %d "
	    "ns_type %d lf %d perm %d own %d\n",
	    "ROOT",
	    root_ctx,
	    root_ctx->get_replicated(),
	    root_ctx->get_nameserver_type(),
	    root_ctx->get_look_further(),
	    root_ctx->get_permission(),
	    root_ctx->get_owner()));

	NS_PRINTF(NS_DUMP_ALL, (
	    "migrate_global_nameserver: Context Name :%s (%p) replicated %d "
	    "ns_type %d lf %d perm %d own %d\n",
	    "0",
	    ctx_0,
	    ctx_0->get_replicated(),
	    ctx_0->get_nameserver_type(),
	    ctx_0->get_look_further(),
	    ctx_0->get_permission(),
	    ctx_0->get_owner()));

	NS_PRINTF(NS_DUMP_ALL, (
	    "migrate_global_nameserver: Context Name :%s (%p) replicated %d "
	    "ns_type %d lf %d perm %d own %d\n",
	    "1",
	    ctx_1,
	    ctx_1->get_replicated(),
	    ctx_1->get_nameserver_type(),
	    ctx_1->get_look_further(),
	    ctx_1->get_permission(),
	    ctx_1->get_owner()));

	//
	// Rewind the pointers to point to the begining
	// of the bnode list for the root context
	//
	tmp_bnode_lst = bnode_lst;

	// Get the store and bnode list for c0 context (PHYSICAL_CLUSTER)
	binding_store *store_c0 = ctx_0->get_store();
	ASSERT(store_c0 != NULL);

	// Get the store and bnode list for c1 context (ALL_CLUSTER)
	binding_store *store_c1 = ctx_1->get_store();
	ASSERT(store_c1 != NULL);

	while (tmp_bnode_lst != NULL) {
		volatile_binding *tmp_vb = tmp_bnode_lst->vbinding;
		char *name = tmp_vb->get_name();

	//
	//	if (os::strcmp(name, "2") == 0) {
	//		// Add the binding to the new location
	//		((volatile_store*)store_c1)->add_binding(tmp_vb);

			// Drop the binding from the root context
	//		nc_error rr =
	//		    ((volatile_store*)store_ptr)->drop_binding(name);

	//		NS_PRINTF(NS_BIND_CTX, ("Moved Objet 2 rr = %d\n", rr));
	//		tmp_bnode_lst = tmp_bnode_lst->next;
	//		continue;
	//	}

		//
		// Following objects need to remain in the real root
		// of the global name server
		//
		if ((os::strcmp(name, "0") == 0) ||
		    (os::strcmp(name, "1") == 0) ||
		    (os::strcmp(name, "services") == 0) ||
		    (os::strncmp(name, "local_ns_", (size_t)9) == 0)) {
			tmp_bnode_lst = tmp_bnode_lst->next;
			continue;
		}

		//
		// Following objects need to be moved to the PHYSICAL_CLUSTER
		// context. i.e Context 0
		//

		// Add the binding to the new location
		((volatile_store*)store_c0)->add_binding(tmp_vb); //lint !e1774

		// Drop the binding from the root context
		//lint -e1774
		nc_err = ((volatile_store*)store_ptr)->drop_binding(name);
		//lint +e1774
		if (nc_err != nc_ok) {
			NS_PRINTF(NS_DUMP_ALL, (
			    "migrate_global_nameserver: Unable to drop binding"
			    " %s nc_err = %d\n", name, nc_err));
			ASSERT(0);
		}

		tmp_bnode_lst = tmp_bnode_lst->next;
	}
}

#endif // _KERNEL_ORB

//lint +e1776
