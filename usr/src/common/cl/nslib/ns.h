/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
/*
 *  ns.h		class representing a name server library
 *
 */

#ifndef _NS_H
#define	_NS_H

#pragma ident	"@(#)ns.h	1.38	08/07/19 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <sys/vm_util.h>
#include <h/naming.h>
#include <nslib/store.h>
#include <h/ccr.h>

extern "C" naming::naming_context_ptr local_nameserver(nodeid_t);
extern "C" naming::naming_context_ptr local_nameserver_root(nodeid_t);
extern "C" naming::naming_context_ptr root_nameserver();
extern "C" naming::naming_context_ptr root_nameserver_root();

#define	NS_DUMP_ALL 0xffffffff

// Forward declaration
class naming_context_root;
class naming_context_impl;
class naming_context_repl_impl;

class ns {
	friend class naming_context_ii;
	friend class naming_context_root;
	friend class naming_context_impl;
	friend class naming_context_repl_impl;
	friend class repl_ns_server;
public:
	// get global name server
	static naming::naming_context_ptr root_nameserver();
	static bool root_nameserver_init(bool native_zone_flag = false);

	static naming::naming_context_ptr root_nameserver_root();
	static naming::naming_context_ptr root_nameserver_c0();
	static naming::naming_context_ptr root_nameserver_c1();

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	static naming::naming_context_ptr root_nameserver_cz(const char *);
#endif

	// get local name server
	static naming::naming_context_ptr local_nameserver(nodeid_t n =
	    NODEID_UNKNOWN);

	static naming::naming_context_ptr local_nameserver_root(nodeid_t n =
	    NODEID_UNKNOWN);
	static naming::naming_context_ptr local_nameserver_c0(nodeid_t n =
	    NODEID_UNKNOWN);
	static naming::naming_context_ptr local_nameserver_c1(nodeid_t n =
	    NODEID_UNKNOWN);

	static bool local_nameserver_init(bool native_zone_flag = false);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	static naming::naming_context_ptr local_nameserver_cz(
	    const char *, nodeid_t n = NODEID_UNKNOWN);
#endif

	// create global name server
	static int init_nameserver();

	static CORBA::Object_ptr wait_resolve(const char *, Environment &);

	static CORBA::Object_ptr wait_resolve_local(const char *,
		    Environment &);

	// Will return the running version
	static version_manager::vp_version_t get_ns_version();

#ifdef _KERNEL_ORB
	static bool is_root_ns_initialized();

	static void set_root_ns_initialized();

	static void unset_root_ns_initialized();

	static bool bind_cz_context(const char *, const int,
	    bool lf, uint32_t perm, uint32_t own);

	static bool rebind_cz_context(const char *, const int,
	    bool lf, uint32_t perm, uint32_t own);

	static bool unbind_cz_context(const char *, const int);

	static bool check_cz_context(const char *, const int);

	static bool init_global_context(naming_context_ii *root_ctx,
	    Environment &env);

	static void print_global_nameserver(naming_context_repl_impl *ctx,
	    char *cname, uint32_t depth);

	static void migrate_global_nameserver(naming_context_repl_impl *ctx);

#endif

private:
	static CORBA::Object_ptr wait_resolve_private(const char *,
		    naming::naming_context_ptr, Environment &);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// This function should be used only
	// by the name server software. This is
	// because in certain circumstances, this
	// function would try to initialize the
	// local nameserver.
	//
	static int get_cz_id();
	static int get_cz_id_by_name(const char *);
#endif

#if (SOL_VERSION >= __s10) && !defined(_KERNEL_ORB)
	// Only s10 and ahead userland
	static bool is_native_zone();
#endif

#ifdef _KERNEL_ORB
	// create global name server
	static naming::naming_context_ptr create_global_nameserver(
		Environment &);

	// create local name server
	static naming::naming_context_ptr create_local_nameserver();

	static bool		root_ns_initialized;

	static binding_store  *b_store_local_c1;
	static binding_store  *b_store_root_c1;

	static naming_context_root	*local_nmsrvr_obj;
	static naming_context_impl	*local_nmsrvr_obj_c0;
	static naming_context_impl	*local_nmsrvr_obj_c1;

	static naming_context_repl_impl *global_nmsrvr_obj;
	static naming_context_repl_impl *global_nmsrvr_obj_c0;
	static naming_context_repl_impl *global_nmsrvr_obj_c1;

#endif
	static naming::naming_context_ptr local_nmsrvr;
	static naming::naming_context_ptr local_nmsrvr_c0;
	static naming::naming_context_ptr local_nmsrvr_c1;

	static naming::naming_context_ptr global_nmsrvr;
	static naming::naming_context_ptr global_nmsrvr_c0;
	static naming::naming_context_ptr global_nmsrvr_c1;
};

#endif	/* _NS_H */
