//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)repl_ns_server.cc	1.29	08/10/22 SMI"

#include <nslib/repl_ns_server.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/vm_util.h>
#include <nslib/ns.h>

version_manager::vp_version_t   repl_ns_server::current_version;
version_manager::vp_version_t   repl_ns_server::pending_version;
int repl_ns_server::conversion_state = REPL_NS_CONVERSION_INIT;
os::rwlock_t repl_ns_server::version_lock;
//
// The version map for name server
// The RAC on Zones project uses version 2.0
//
ns_version_map_t ns_vp_to_idl[NS_VP_MAX_MAJOR +1][NS_VP_MAX_MINOR + 1] = {
	{{0, 0}, {0, 0}},  // Version 0.0 defined for indexing convinience
	{{0, 0}, {0, 0}},  // Version 1.0 (1.1 is defined for indexing)
	{{1, 1}, {0, 0}}   // Version 2.0 for naming and checkpoint interface
};

repl_ns_server *repl_srvr;

//
// Lint is throwing message 1776 for all NS_PRINTF
// and print_exception functions. Hence supressing
// it for the whole file.
//
// Message:
// Converting string literals to char * is deprecated
// (Context)  -- A string literal, according to Standard C++
// is typed an array of const char.  This message is issued
// when such a literal is assigned to a non-const pointer.
//

//lint -e1776


// Implement all checkpoint methods

repl_ns_server::repl_ns_server(const char *nm, const char *id) :
    repl_server<repl_ns::ns_replica>(nm, id),
    _ckpt_proxy(NULL)
{
	root_ctxp = NULL;

	//
	// Initialize the version to the
	// default (first) version.
	//
	current_version.major_num = 1;
	current_version.minor_num = 0;
	pending_version.major_num = 1;
	pending_version.minor_num = 0;

	// Set the initial state here.
	conversion_state = REPL_NS_CONVERSION_INIT;
}

repl_ns_server::~repl_ns_server()
{
	root_ctxp = NULL;
	_ckpt_proxy = NULL;
}

//
//
//
repl_ns_server *
repl_ns_server::get_repl_server()
{
	return (repl_srvr);
}

//
// Called from startup
//
void
repl_ns_server::initialize(char *nm)
{
	char id[20];
	Environment e;
	version_manager::vp_version_t 	ns_version;

	os::sprintf(id, "%u", orb_conf::node_number());

	repl_srvr = new repl_ns_server(nm, id);

	//
	// Register for Upgrade Callbacks.
	//

	//
	// Get a pointer to the local version manager.
	//
	version_manager::vm_admin_var vmgr_v =
	    vm_util::get_vm(NODEID_UNKNOWN);
	if (CORBA::is_nil(vmgr_v)) {
		CL_PANIC(!"NS: Could not get vm_admin object");
	}

	// Build a UCC for support of version upgrade callbacks
	version_manager::ucc_seq_t ucc_seq(1, 1);
	version_manager::string_seq_t freeze_seq(1, 1);

	ucc_seq[0].ucc_name = os::strdup("naming");
	ucc_seq[0].vp_name = os::strdup("naming");
	freeze_seq[0] = os::strdup("repl_name_server");
	ucc_seq[0].freeze = freeze_seq;

	// Create a version upgrade callback object.
	ns_upgrade_callback *cbp =
	    new ns_upgrade_callback(*repl_srvr);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();
	version_manager::vp_version_t	callback_limit;
	version_manager::vp_version_t	cur_version;

	cur_version.major_num = 1;
	cur_version.minor_num = 0;

	//
	// Register the callback object with the Version Manager. The
	// tmp_version will be returned.  The version lock is not held
	// since the replica is not yet registered with the HA framework
	// so there can not be a call to become_primary. The current version
	// is returned regardless.
	//
	// If the running version is less than the callback_limit
	// a callback will be registered, otherwise a callback is not
	// registered (currently no way to tell).  The current running
	// version is returned regardless.
	//
	callback_limit.major_num = NS_VP_MAJOR;
	callback_limit.minor_num = NS_VP_MINOR;
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v,
	    callback_limit, cur_version, e);
	if (e.exception()) {
		e.exception()->print_exception("NameServer: "
		    "Failed register_upgrade_callbacks()");
		e.clear();
		NS_PRINTF(NS_CKPT, ("repl_ns_server::initialize: "
		    "Failed to register_upgrade_callbacks() "
		    "Major=%d Minor=%d\n",
		    callback_limit.major_num, callback_limit.minor_num));
	}

	// Establish the current version in the provider (repl_ns_server)
	repl_srvr->set_init_version(cur_version);

	// Now register with the resource manager
	repl_srvr->register_with_rm(1, true, e);
	if (e.exception() != NULL) {
		e.exception()->print_exception("repl_ns_server: "
		    "Exception invoking registering with RM");
		NS_PRINTF(NS_CKPT, ("repl_ns_server::initialize: "
		    "Exception invoking registering with RM\n"));
		e.clear();

		os::sc_syslog_msg syslog_msg(SC_SYSLOG_NAMING_SERVICE_TAG,
		    "naming", NULL);
		//
		// SCMSGS
		// @explanation
		// The Nameserver repl server failed to register with the
		// cluster HA framework.
		// @user_action
		// This is an unrecoverable error, and the cluster needs to be
		// rebooted. Also contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "Nameserver failed to register with cluster "
		    "HA framework\n");
	}

	ns_version = ns::get_ns_version();

	version_lock.wrlock();
	if (ns_version.major_num < 2) {
		//
		// Old version is running. Conversion is going to
		// be required once scversions commit is called.
		//
		conversion_state = REPL_NS_CONVERSION_REQUIRED;
	} else {
		// Running the latest version. Conversion not required.
		conversion_state = REPL_NS_CONVERSION_NOT_REQUIRED;
	}
	NS_PRINTF(NS_CKPT, (
	    "repl_ns_server::initialize: conversion_state = %d\n",
	    conversion_state));
	version_lock.unlock();
} //lint !e818

//
// become primary is called:
// - when service first starts up
// - when the old primary dies and service fails over to this node
// on failover case must initiate recovery
//
void
repl_ns_server::become_primary(const replica::repl_name_seq &,
    Environment &env)
{
	version_manager::vm_admin_var   vm_v;
	replica::checkpoint_var		tmp_ckpt_v;
	CORBA::type_info_t		*typ = NULL;

	//
	// The following environment is for checkpointing the
	// version number to the secondary.
	//
	Environment		ckpt_environment;

	//
	// Create a primary context so the provider can send
	// checkpoints while the service is frozen.
	// XXX change the primary_ctx::invoke_env type.
	//
	//lint -e64
	//lint -e24
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT,
	    ckpt_environment);
	//lint +e24
	//lint +e64

	version_lock.wrlock();

	// Callback may have occured when this replica was a secondary
	if (pending_version.major_num != 1) {
		current_version = pending_version;
	}

	NS_PRINTF(NS_DUMP_ALL, ("repl_ns_server::become_primary (%p): "
	    "Current version is %d.%d\n", this,
	    current_version.major_num, current_version.minor_num));

	// First, initialize the checkpoint proxy.
	typ = repl_ns::ns_replica::_get_type_info(
	    ns_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].ns_replica_vers);
	ASSERT(typ != NULL);

	tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_ns::ns_replica::_narrow(tmp_ckpt_v);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	//
	// We are not the first (initial) primary
	// We were a secondary
	//
	if (root_ctxp != NULL) {
		NS_PRINTF(NS_DUMP_ALL, ("repl_ns_server::become_primary (%p): "
		    "Current version is %d.%d roo_ctxp = %p\n", this,
		    current_version.major_num, current_version.minor_num,
		    root_ctxp));
		//
		// If there was an unprocessed upgrade callback pending,
		// finish the work and send a checkpoint to notify the
		// other secondaries.
		//
		if (pending_version.major_num != 1) {
			pending_version.major_num = 1;
			NS_PRINTF(NS_DUMP_ALL, (
			    "repl_ns_server::become_primary (%p): "
			    "pending_version changed root_ctxp = %p\n",
			    this, root_ctxp));

			naming::naming_context_ptr tmp_global_nmsrvr =
			    ns::global_nmsrvr;

			// Update the server reference.
			typ = naming::naming_context::_get_type_info(
			    ns_vp_to_idl[current_version.major_num]
			    [current_version.minor_num].naming_context_vers);
			ASSERT(typ != NULL);
			ns::global_nmsrvr = root_ctxp->get_objref(typ);
			ns::global_nmsrvr_obj = root_ctxp;
			CORBA::release(tmp_global_nmsrvr);

			// Checkpoint the current version number.
			_ckpt_proxy->ckpt_service_version(
			    current_version.major_num,
			    current_version.minor_num,
			    true, ckpt_environment);
			ckpt_environment.clear();
		}

		//
		// Initialize the namespace by creating context
		// 0 and 1 in the root context.
		// We can reach this only in a RU scenario where the
		// nodes were running the old software and context
		// 0 and 1 has not already been created.
		//
		naming_context_ii::create_virtual_root(
		    (naming_context_ii *)root_ctxp, GLOBAL_NS, env);


		// Release the lock here
		version_lock.unlock();

		//
		// Show that the context is no longer there,
		// so that the destructor will see that everything is clean.
		//
		ckpt_environment.trans_ctxp = NULL;

		return;
	} // End of (root_ctxp != NULL)


	//
	// We are on the primary
	// root_ctxp == NULL
	//

	// Create a new naming_context_repl_impl object
	root_ctxp = new naming_context_repl_impl(get_repl_server(),
	    false,
	    NS_ALLOW_ALL_OPP_PERM | NS_ALLOW_ALL_ZONE_READ_PERM,
	    GLOBAL_ZONEID);

	// Update the server reference.
	typ = naming::naming_context::_get_type_info(
	    ns_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].naming_context_vers);
	ASSERT(typ != NULL);
	ns::global_nmsrvr = root_ctxp->get_objref(typ);
	ns::global_nmsrvr_obj = root_ctxp;

	//
	// If an old primary failed while an upgrade callback was in
	// progress and before the service version was checkpointed, we
	// do the service version checkpointing now.
	//
	if (pending_version.major_num != 1) {
		pending_version.major_num = 1;
		NS_PRINTF(NS_DUMP_ALL, ("repl_ns_server::become_primary (%p): "
		    "pending_version changed new root_ctxp = %p\n",
		    this, root_ctxp));
		_ckpt_proxy->ckpt_service_version(
		    current_version.major_num,
		    current_version.minor_num,
		    true, ckpt_environment);
		ckpt_environment.clear();
	}

	// XXX - Keep the object ref
	naming::naming_context_ptr robj = root_ctxp->get_objref(typ);
	if (repl_ns::ns_replica::_version(tmp_ckpt_v) == 0) {
		NS_PRINTF(NS_DUMP_ALL, ("repl_ns_server::become_primary "
		    "global_nmsrvr_obj = %p robj = %p \n",
		    ns::global_nmsrvr_obj, robj));
		root_ctxp->get_checkpoint()->ckpt_root_context(robj,
		    ckpt_environment);
	} else {
		NS_PRINTF(NS_DUMP_ALL, ("repl_ns_server::become_primary "
		    "global_nmsrvr_obj = %p "
		    "robj = %p lf = %d perm = 0x%x own = %d\n",
		    ns::global_nmsrvr_obj, robj,
		    root_ctxp->get_look_further(),
		    root_ctxp->get_permission(),
		    root_ctxp->get_owner()));
		root_ctxp->get_checkpoint()->ckpt_root_context_v1(robj,
		    root_ctxp->get_look_further(),
		    root_ctxp->get_permission(),
		    root_ctxp->get_owner(),
		    ckpt_environment);
	}
	ckpt_environment.clear();

	version_lock.unlock();

	ASSERT(!CORBA::is_nil(ns::global_nmsrvr));

	//
	// Initialize the namespace by creating various contexts
	// in the root context.
	//
	naming_context_ii::init_namespace(
	    (naming_context_ii *)root_ctxp, GLOBAL_NS, env);

	ns::set_root_ns_initialized();

	//
	// For existing zone clusters, create their
	// global name server contexts.
	//
	if (!ns::init_global_context((naming_context_ii *)root_ctxp, env)) {
		//
		// This is a unrecoverable error.
		// Something is seriously worng.
		//
		CL_PANIC(0);
	}

	//
	// Show that the context is no longer there,
	// so that the destructor will see that everything is clean.
	//
	ckpt_environment.trans_ctxp = NULL;
}

void
repl_ns_server::freeze_primary_prepare(Environment &)
{
}

void
repl_ns_server::freeze_primary(Environment &)
{
}

void
repl_ns_server::unfreeze_primary(Environment &)
{
}

void
repl_ns_server::become_secondary(Environment &)
{
	NS_PRINTF(NS_CKPT, ("Become secondary\n"));

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

void
repl_ns_server::add_secondary(replica::checkpoint_ptr sec_ckpt, const char *,
    Environment &_env)
{
	repl_ns::ns_replica_var ckpt = repl_ns::ns_replica::_narrow(sec_ckpt);
	ASSERT(!CORBA::is_nil(ckpt));

	if (root_ctxp != NULL) {

		NS_PRINTF(NS_CKPT, ("add_secondary: dump_state secondary\n"));

		// Dump all state to the new secondary.
		((naming_context_ii *)root_ctxp)->dump_state(ckpt, _env);
		_env.clear();
	}
}

void
repl_ns_server::remove_secondary(const char *, Environment &)
{
}

void
repl_ns_server::become_spare(Environment &)
{
	//
	// Normally all state created in the server must be deleted.
	// The state then get re-created when an add_secondary happens.
	//
	// The only internal state that the name server must cleanup
	// is the unbound context list.  All other internal state is
	// that is checkpointed result in rebind operations which in
	// turn updates the stale state.
	//
	// XXX - If some operation (e.g., unbind) deletes state on the
	// 	primary before this provider gets promoted from spare
	//	to a secondary	, then become_spare() needs to delete
	//	all internal state.
	//
	// XXX - Need to set root_ctxp to be NULL since add_secondary()
	// 	will not create a new shadow object.
	//
	//	As a result of this, we must add code to delete all the
	//	 bindings associated with the previous root_ctxp before
	//	 setting it to be NULL

	//
	// Delete the root context first. This causes the contexts directly
	// underneath the root context to be placed on the unbound_list.
	// In clustering code today, there's only one such context below
	// the root context. It's the services context.
	// So cleanup_unbound_list() cleans up the services context.
	//
	// The deletion of a context results in the deletion of its binding
	// store which in turn causes all name-object bindings in that context
	// to be deleted.
	//
	delete root_ctxp;
	dump_info::cleanup_unbound_list();
	root_ctxp = NULL; /*lint !e423 */
}

void
repl_ns_server::shutdown(Environment &env)
{
	env.exception(new replica::service_busy);

	//
	// Since we are returning exception, we will remain primary,
	// so don't release the _ckpt_proxy.
	//
}

CORBA::Object_ptr
repl_ns_server::get_root_obj(Environment &)
{
	ASSERT(root_ctxp != NULL);
	return (root_ctxp->get_objref());
}



//
// Checkpoint operations
//

//
// Checkpoint a new naming_context_repl object to existing
// secondaries.
//
void
repl_ns_server::ckpt_root_context(naming::naming_context_ptr robj,
    Environment &)
{
	NS_PRINTF(NS_CKPT, ("ckpt_root_context :%p:\n", robj));

	if (root_ctxp != NULL) {
		return;
	}

	root_ctxp = new naming_context_repl_impl(robj, false,
	    NS_ALLOW_ALL_OPP_PERM | NS_ALLOW_ALL_ZONE_READ_PERM,
	    GLOBAL_ZONEID);
	ns::global_nmsrvr_obj = root_ctxp;

	// TODO: Check if lock needs to be grabbed
	ns::global_nmsrvr = naming::naming_context::_duplicate(robj);
	ASSERT(!CORBA::is_nil(ns::global_nmsrvr));

	NS_PRINTF(NS_DUMP_ALL, ("ckpt_root_context: global_nmsrvr = %p "
	    "root_ctxp = %p global_nmsrvr_obj = %p\n",
	    ns::global_nmsrvr, root_ctxp, ns::global_nmsrvr_obj));

	ns::set_root_ns_initialized();
}

//
// Checkpoint a binding which represents an object reference
//
void
repl_ns_server::ckpt_binding(naming::naming_context_ptr pctx, const char *nm,
    CORBA::Object_ptr obj, int32_t bind_type, bool rebind, Environment &env)
{
	naming_context_repl_impl *rctx_impl =
	    (naming_context_repl_impl *)pctx->_handler()->get_cookie();
	ASSERT(rctx_impl != NULL);

	NS_PRINTF(NS_CKPT, ("ckpt_binding: name: %s\n", nm));
	((naming_context_ii *)rctx_impl)->ckpt_binding(nm, obj, bind_type,
	    rebind, env);

	if (os::strcmp(nm, "ns_debug_obj") == 0) {
		ns::print_global_nameserver(ns::global_nmsrvr_obj, "root", 0);
	}
}

//
// Checkpoint a binding which represents an local context
//
void
repl_ns_server::ckpt_context(naming::naming_context_ptr pctx,
    naming::naming_context_ptr ctx, const char *nm, Environment &env)
{
	naming_context_repl_impl *rctx_impl =
	    (naming_context_repl_impl *)pctx->_handler()->get_cookie();
	ASSERT(rctx_impl != NULL);

	NS_PRINTF(NS_CKPT, ("ckpt_context: name: %s\n", nm));
	((naming_context_ii *)rctx_impl)->ckpt_context(ctx, nm, env);
}

//
// Checkpoint an unbind operation
//
void
repl_ns_server::ckpt_unbind(naming::naming_context_ptr pctx,
    const char *nm, Environment &env)
{
	naming_context_repl_impl *rctx_impl =
	    (naming_context_repl_impl *)pctx->_handler()->get_cookie();
	ASSERT(rctx_impl != NULL);

	NS_PRINTF(NS_CKPT, ("ckpt_unbind: name: %s\n", nm));
	((naming_context_ii *)rctx_impl)->ckpt_unbind(nm, env);
}

//
// Checkpoint an unbound context
//
void
repl_ns_server::ckpt_unbound_context(naming::naming_context_ptr ctx,
    Environment &)
{
	NS_PRINTF(NS_CKPT, ("ckpt_unbound context: ctx: %p\n", ctx));
	naming_context_ii::ckpt_unbound_context(ctx);
}


//
// Checkpoint operations modified as a part of the
// zone cluster project.
//

//
// Checkpoint a new naming_context_repl object to existing
// secondaries.
// perm: The permission bits
// own : The owner zone which registered the context
//
void
repl_ns_server::ckpt_root_context_v1(naming::naming_context_ptr robj,
    bool lf, uint32_t perm, uint32_t own, Environment &)
{
	NS_PRINTF(NS_CKPT,
	    ("ckpt_root_context_v1 :%p: lf = %d perm = 0x%x own = %d\n",
	    robj, lf, perm, own));

	if (root_ctxp != NULL) {
		return;
	}

	root_ctxp = new naming_context_repl_impl(robj, lf, perm, own);
	ns::global_nmsrvr_obj = root_ctxp;

	// TODO: Check if lock needs to be grabbed
	ns::global_nmsrvr = naming::naming_context::_duplicate(robj);
	ASSERT(!CORBA::is_nil(ns::global_nmsrvr));
	NS_PRINTF(NS_CKPT, (
	    "ckpt_root_context_v1 :global_nmsrvr = %p root_ctxp = %p "
	    "global_nmsrvr_obj = %p\n",
	    ns::global_nmsrvr, root_ctxp, ns::global_nmsrvr_obj));

	ns::set_root_ns_initialized();
}

//
// Checkpoint a binding which represents an local context
//
void
repl_ns_server::ckpt_context_v1(naming::naming_context_ptr pctx,
    naming::naming_context_ptr ctx,
    const char *nm,
    bool lf,
    uint32_t perm,
    uint32_t own,
    Environment &env)
{
	naming_context_repl_impl *rctx_impl =
	    (naming_context_repl_impl *)pctx->_handler()->get_cookie();
	ASSERT(rctx_impl != NULL);

	NS_PRINTF(NS_CKPT,
	    ("ckpt_context_v1: name: %s lf: %d perm: 0x%x own: %d\n",
	    nm, lf, perm, own));
	((naming_context_ii *)rctx_impl)->ckpt_context_v1(ctx,
	    nm, lf, perm, own, env);
}

//
// Checkpoint an unbound context
//
void
repl_ns_server::ckpt_unbound_context_v1(naming::naming_context_ptr ctx,
    bool lf, uint32_t perm, uint32_t own, Environment &)
{
	NS_PRINTF(NS_CKPT, ("ckpt_unbound_context_v1: ctx: %p\n", ctx));
	naming_context_ii::ckpt_unbound_context_v1(ctx, lf, perm, own);
}




// Checkpoint accessor function.
repl_ns::ns_replica_ptr
repl_ns_server::get_checkpoint_repl_ns_ns_replica()
{
	return (_ckpt_proxy);
}

//
// Checkpoint the new service version
//
// If convert is false, then do not migrate
//
void
repl_ns_server::ckpt_service_version(unsigned short new_major,
    unsigned short new_minor, bool convert, Environment &)
{
	version_lock.wrlock();
	NS_PRINTF(NS_CKPT, ("in ckpt_service_version Version %d.%d\n",
	    new_major, new_minor));

	current_version.major_num = new_major;
	current_version.minor_num = new_minor;
	pending_version.major_num = 1;

	//
	// No need to release the ns::global_nmsrvr as
	// in ckpt_root_rebind we have already got the
	// new version of the reference.
	// Clear out the object reference for the c0
	// and the c1 context of the global nameserver
	//
	if (!CORBA::is_nil(ns::global_nmsrvr_c0)) {
		CORBA::release(ns::global_nmsrvr_c0);
		ns::global_nmsrvr_c0 = naming::naming_context::_nil();
	}
	if (!CORBA::is_nil(ns::global_nmsrvr_c1)) {
		CORBA::release(ns::global_nmsrvr_c1);
		ns::global_nmsrvr_c1 = naming::naming_context::_nil();
	}

	// Migrate the objects
	if ((conversion_state == REPL_NS_CONVERSION_REQUIRED) &&
	    (convert)) {
		NS_PRINTF(NS_CKPT, ("ckpt_service_version (%p): "
		    "Conversion routine called\n", this));
		ns::migrate_global_nameserver(ns::global_nmsrvr_obj);
		conversion_state = REPL_NS_CONVERSION_DONE;
	}

	version_lock.unlock();
}

//
// To be used only in upgrade_callback to rebind the
// real root of the global name server in the local
// name server on the secondary nodes
//
void
repl_ns_server::ckpt_root_rebind(naming::naming_context_ptr robj,
    Environment &e)
{
	NS_PRINTF(NS_CKPT, ("ckpt_root_rebind :%p:\n", robj));

	ASSERT(!CORBA::is_nil(robj));

	//
	// This should be called only in Rolling Upgrade
	// scenario. Hence the root_ctxp can not be NULL
	//
	ASSERT(root_ctxp != NULL);

	if (!CORBA::is_nil(ns::global_nmsrvr)) {
		CORBA::release(ns::global_nmsrvr);
	}
	ns::global_nmsrvr = robj;
	ASSERT(!CORBA::is_nil(ns::global_nmsrvr));

	NS_PRINTF(NS_DUMP_ALL, ("ckpt_root_rebind: global_nmsrvr = %p "
	    "root_ctxp = %p global_nmsrvr_obj = %p\n",
	    ns::global_nmsrvr, root_ctxp, ns::global_nmsrvr_obj));

	naming::naming_context_var lctxp = ns::local_nameserver_root();
	ASSERT(!CORBA::is_nil(lctxp));
	lctxp->rebind("gbl_ns", ns::global_nmsrvr, e);
	if (e.exception() != NULL) {
		e.exception()->print_exception("ckpt_root_rebind: "
		    "Can't rebind ns::global nmsrvr");
		CL_PANIC(0);
	}
}

//
// The callback function invoked by the version manager
// during a rolling upgrade commit.
//
void
repl_ns_server::upgrade_callback(
    const version_manager::vp_version_t &version,
    Environment &e)
{
	CORBA::type_info_t *typ;

	NS_PRINTF(NS_CKPT, ("repl_ns_server::upgrade_callback (%p): "
	    "Entry current_version = %d.%d pending_version = %d.%d "
	    "version = %d.%d\n",
	    this,
	    current_version.major_num, current_version.minor_num,
	    pending_version.major_num, pending_version.minor_num,
	    version.major_num, version.minor_num));


	//
	// Note that upgrade callbacks are not synchronized with
	// calls to become_primary(), add_secondary(), etc.
	// Getting this lock makes sure the replica state doesn't change.
	//
	version_lock.wrlock();
	if (current_version.major_num > version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num >= version.minor_num)) {

		NS_PRINTF(NS_CKPT, ("repl_ns_server::upgrade_callback (%p): "
		    "major = %d minor = %d No Change\n", this,
		    current_version.major_num, current_version.minor_num));

		// Version isn't changing so just return.
		version_lock.unlock();
		return;
	}

	//
	// If this replica is not the primary, set the pending version
	// to be used if there is a failover before the primary checkpoints
	// a new version.
	//
	if (CORBA::is_nil(_ckpt_proxy)) {
		NS_PRINTF(NS_CKPT, ("repl_ns_server::upgrade_callback (%p): "
		    "current_version = %d.%d version = %d.%d "
		    "_ckpt_proxy = %p\n", this,
		    current_version.major_num, current_version.minor_num,
		    version.major_num, version.minor_num, _ckpt_proxy));

		// If this replica is a secondary, set the pending version.
		if (root_ctxp != NULL) {
			pending_version = version;
			NS_PRINTF(NS_CKPT,
			    ("repl_ns_server::upgrade_callback (%p): "
			    "root_ctxp = %p\n", this, root_ctxp));
		} else {
			// Replica must be a spare.
			current_version = version;
			NS_PRINTF(NS_CKPT,
			    ("repl_ns_server::upgrade_callback (%p): "
			    "current_version = %d.%d "
			    "root_ctxp is NULL - SPARE\n",
			    this, current_version.major_num,
			    current_version.minor_num));
		}
		version_lock.unlock();
		return;
	}

	//
	// We are the primary and the version has changed.
	// Set the current version.
	//
	current_version = version;

	NS_PRINTF(NS_CKPT, ("repl_ns_server::upgrade_callback (%p): "
	    "current_version = %d.%d We are PRIMARY\n", this,
	    current_version.major_num, current_version.minor_num));

	//
	// Switch the checkpoint interface to the new protocol. Save
	// the current _ckpt_proxy and release it after we get a new one
	//
	repl_ns::ns_replica_ptr old_ckpt_p = _ckpt_proxy;

	typ = repl_ns::ns_replica::_get_type_info(
	    ns_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].ns_replica_vers);
	ASSERT(typ != NULL);

	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_ns::ns_replica::_narrow(tmp_ckpt_v);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	CORBA::release(old_ckpt_p);


	//
	// Release the reference to the root of the global nameserver
	// and then again retrieve the reference inorder to use the
	// new namesever protocol (version)
	//
	ASSERT(root_ctxp != NULL);
	NS_PRINTF(NS_CKPT, ("repl_ns_server::upgrade_callback (%p):\n", this));
	naming::naming_context_ptr tmp_global_nmsrvr = ns::global_nmsrvr;
	typ = naming::naming_context::_get_type_info(
	    ns_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].naming_context_vers);
	ASSERT(typ != NULL);
	ns::global_nmsrvr = root_ctxp->get_objref(typ);
	ns::global_nmsrvr_obj = root_ctxp;
	CORBA::release(tmp_global_nmsrvr);

	//
	// Release the reference to C0 and C1 context as it is
	// running the old version. The nameserver software will
	// automatically retrieve the new reference when a client
	// will try to access it.
	//
	if (!CORBA::is_nil(ns::global_nmsrvr_c0)) {
		CORBA::release(ns::global_nmsrvr_c0);
		ns::global_nmsrvr_c0 = naming::naming_context::_nil();
	}

	if (!CORBA::is_nil(ns::global_nmsrvr_c1)) {
		CORBA::release(ns::global_nmsrvr_c1);
		ns::global_nmsrvr_c1 = naming::naming_context::_nil();
	}

	Environment env;
	naming::naming_context_var lctxp = ns::local_nameserver_root();
	ASSERT(!CORBA::is_nil(lctxp));
	lctxp->rebind("gbl_ns", ns::global_nmsrvr, env);
	if (env.exception() != NULL) {
		env.exception()->print_exception(
		    "repl_ns_server::upgrade_callback: "
		    "Can't rebind global nmsrvr");
		// Panic the node
		CL_PANIC(0);
	}

	//
	// If there are more references in future,
	// this is the place to get new references
	//

	//
	// Migrate existing objects from the real
	// root needs to be migrated into the virtual root.
	//
	ns::migrate_global_nameserver(ns::global_nmsrvr_obj);
	conversion_state = REPL_NS_CONVERSION_DONE;

	//
	// Create and add a primary context so the provider can send
	// checkpoints while the service is frozen.
	//
	//lint -e64
	//lint -e24
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, e);
	//lint +e24
	//lint +e64

	//
	// Refresh the real root reference of the global
	// nameserver on the secondaries.
	//
	_ckpt_proxy->ckpt_root_rebind(
	    naming::naming_context::_duplicate(ns::global_nmsrvr), e);

	// Checkpoint the current version number.
	_ckpt_proxy->ckpt_service_version(
	    current_version.major_num,
	    current_version.minor_num,
	    true, e);

	e.trans_ctxp = NULL;
	version_lock.unlock();
}

void
repl_ns_server::set_init_version(const version_manager::vp_version_t &version)
{
	version_lock.wrlock();
	if (current_version.major_num < version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num < version.minor_num)) {
		current_version = version;
	}
	version_lock.unlock();
} //lint !e1762

version_manager::vp_version_t
repl_ns_server::get_current_version()
{
	return (current_version);
}

version_manager::vp_version_t
repl_ns_server::get_pending_version()
{
	return (pending_version);
}

//
// ----------- ns_upgrade_callback class methods -------------
//


ns_upgrade_callback::ns_upgrade_callback(repl_ns_server& replns)
	:repl_ns_server_ref(replns)
{
}

ns_upgrade_callback::~ns_upgrade_callback()
{
}

void
#ifdef DEBUG
ns_upgrade_callback::_unreferenced(unref_t cookie)
#else
ns_upgrade_callback::_unreferenced(unref_t)
#endif
{
	// This object does not support multiple 0->1 transitions.
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Callback routine registered with version manager.
//
void
ns_upgrade_callback::do_callback(const char *,
    const version_manager::vp_version_t &new_version, Environment &e)
{
	NS_PRINTF(NS_CKPT, ("ns_upgrade_callback::do_callback()\n"));

	//
	// Call the repl ns server to upgrade the version
	// and do any transformations required while switching
	// over to the new version
	//
	repl_ns_server_ref.upgrade_callback(new_version, e);
}

//lint +e1776
