/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  data_container_impl.h
 *
 */

#ifndef _DATA_CONTAINER_IMPL_H
#define	_DATA_CONTAINER_IMPL_H

#pragma ident	"@(#)data_container_impl.h	1.9	08/05/20 SMI"

//
// The data container implementation which implements the data_container::data
// IDL interface and uses a special data_container_handler.
//

#include <orb/object/adapter.h>
#include <h/data_container.h>
#include <nslib/data_container_handler.h>

class data_container_impl :
	public impl_spec<data_container::data, data_container_handler> {
public:
	data_container_impl();
	~data_container_impl();
	void _unreferenced(unref_t);
	void marshal(service& b);

	static CORBA::Object_ptr unmarshal(service& b);
	static void unmarshal_cancel(service& b);

	void set_data(const char *state, Environment &_environment);
	char *obtain_data(Environment &_environment);

private:
	data_container_impl(char *state);

	char *_strdata;
};

#include <nslib/data_container_impl_in.h>

#endif	/* _DATA_CONTAINER_IMPL_H */
