/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DATA_CONTAINER_HANDLER_H
#define	_DATA_CONTAINER_HANDLER_H

#pragma ident	"@(#)data_container_handler.h	1.13	08/05/20 SMI"


//
// Named data handler used to marshal and unmarshal data
//
#include <orb/handler/counter_handler.h>

class data_container_handler_kit : public handler_kit {
public:
	data_container_handler_kit();
	~data_container_handler_kit();

	CORBA::Object_ptr unmarshal(service &, generic_proxy::ProxyCreator,
	    CORBA::TypeId);

	void unmarshal_cancel(service &);

	static data_container_handler_kit &the();
	static int initialize();
	static void shutdown();

private:
	static data_container_handler_kit *the_data_container_handler_kit;
};


class data_container_handler : public counter_handler {
public:
	data_container_handler(CORBA::Object_ptr);
	virtual		~data_container_handler();

	hkit_id_t handler_id();
	void marshal(service&, CORBA::Object_ptr);
};

#include <nslib/data_container_handler_in.h>

#endif	/* _DATA_CONTAINER_HANDLER_H */
