/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _BINDING_ITER_HANDLER_H
#define	_BINDING_ITER_HANDLER_H

#pragma ident	"@(#)binding_iter_handler.h	1.10	08/05/20 SMI"

#include <orb/handler/counter_handler.h>

class binding_iter_handler_kit : public handler_kit {
public:
	binding_iter_handler_kit();
	~binding_iter_handler_kit();

	CORBA::Object_ptr unmarshal(service&, generic_proxy::ProxyCreator,
	    CORBA::TypeId);

	void unmarshal_cancel(service &);

	static binding_iter_handler_kit &the();
	static int initialize();
	static void shutdown();
private:
	static binding_iter_handler_kit *the_binding_iter_handler_kit;

};

class binding_iter_handler : public counter_handler {
public:
	binding_iter_handler();
	binding_iter_handler(CORBA::Object_ptr);
	~binding_iter_handler();

	hkit_id_t handler_id();
	void marshal(service &, CORBA::Object_ptr);
};

#endif	/* _BINDING_ITER_HANDLER_H */
