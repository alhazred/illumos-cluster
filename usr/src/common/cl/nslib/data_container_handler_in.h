/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  data_container_handler_in.h
 *
 */

#ifndef _DATA_CONTAINER_HANDLER_IN_H
#define	_DATA_CONTAINER_HANDLER_IN_H

#pragma ident	"@(#)data_container_handler_in.h	1.8	08/05/20 SMI"

#include <nslib/data_container_impl.h>

//
// data_container_handler constructor and desctructor
//

inline
data_container_handler_kit::data_container_handler_kit() :
	handler_kit(DATA_CONTAINER_HKID)
{
}

inline
data_container_handler_kit::~data_container_handler_kit()
{
}

//
// data_container_handler constructor and destructor
//
inline
data_container_handler::data_container_handler(CORBA::Object_ptr)
{
}

inline hkit_id_t
data_container_handler::handler_id()
{
	return (data_container_handler_kit::the().get_hid());
}

#endif	/* _DATA_CONTAINER_HANDLER_IN_H */
