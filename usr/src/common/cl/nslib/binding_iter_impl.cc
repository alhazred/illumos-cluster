//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)binding_iter_impl.cc 1.11	08/05/20 SMI"

#include <h/naming.h>
#include <nslib/ns_lib.h>
#include <nslib/binding_iter_impl.h>

//
// Binding_iterator implementation:
//
// Maintains a pointer to the binding store of the context
// and an index to the current element (that is to be returned
// next for a "next_one()" or "next_n()" call).
//
// The present implementation of binding_iterator_impl is simple.
// It obtains info about all bindings in the context and saves the binding
// pointers in an array.  Additions/deletions of bindings from this context
// are not seen by the caller when using the iterator.  It also saves
// an index to the next binding to be returned for the ("next_one"
// or "next_n") methods.
//

binding_iterator_impl::binding_iterator_impl(uint32_t binding_index,
    uint32_t num_bindings, naming::binding_list *iter_list)
{
	valid_binding_index = binding_index;
	total_bindings = num_bindings;
	bindings_list = iter_list;
}

//lint -e1740
binding_iterator_impl::~binding_iterator_impl()
{
	NS_PRINTF(NS_BINDING_IMPL, ("destructor:\n"));
	delete bindings_list;
}
//lint +e1740

void
binding_iterator_impl::_unreferenced(unref_t)
{
	NS_PRINTF(NS_BINDING_IMPL, ("unreferenced:\n"));
	delete this;
}


bool
binding_iterator_impl::next_one(naming::binding_out b_out, Environment &)
{
	naming::binding *binding_info;

	NS_PRINTF(NS_NEXT_ONE, ("next_one:\n"));

	binding_info = new naming::binding();
	if (bindings_list == NULL) {
		b_out = binding_info;
		return (false);
	}

	char *nm = (*bindings_list)[valid_binding_index].binding_name;

	ASSERT(nm != NULL);
	(*binding_info).binding_name =
	    os::strcpy(new char [os::strlen(nm) + 1], nm);
	(*binding_info).bind_type =
	    (*bindings_list)[valid_binding_index].bind_type;

	valid_binding_index++;

	if (valid_binding_index == total_bindings) {
		delete bindings_list;
		bindings_list = NULL;
	}

	b_out = binding_info;
	return (true);
}

// XXX - Check??
void
binding_iterator_impl::next_n(uint32_t how_many,
    naming::binding_list_out bl, Environment &)
{
	uint32_t valid_bindings, ret_bindings;
	naming::binding_list *out_list;

	NS_PRINTF(NS_NEXT_N, ("next_n:\n"));

	if (bindings_list == NULL) {
		bl = new naming::binding_list();
		return;
	}

	valid_bindings = total_bindings - valid_binding_index;

	if (how_many < valid_bindings)
		ret_bindings = how_many;
	else
		ret_bindings = valid_bindings;

	NS_PRINTF(NS_NEXT_N, ("next_n: valid_index: %d, total_bindings\n",
	    valid_binding_index, total_bindings));

	ASSERT(ret_bindings != 0);
	if (ret_bindings == 0) {
		out_list = new naming::binding_list();
	} else {
		out_list = new naming::binding_list(ret_bindings, ret_bindings);

		for (uint32_t i = 0; i < ret_bindings; i++) {
			char *nm =
			    (*bindings_list)[valid_binding_index].binding_name;

			ASSERT(nm != NULL);

			(*out_list)[i].binding_name =
			    os::strcpy(new char [os::strlen(nm) + 1], nm);
			(*out_list)[i].bind_type =
			    (*bindings_list)[valid_binding_index].bind_type;

			valid_binding_index++;
		}
	}

	if (valid_binding_index == total_bindings) {
		delete bindings_list;
		bindings_list = NULL;
	}

	bl = out_list;
}

//
//
//
void
binding_iterator_impl::marshal(service &b)
{
	NS_PRINTF(NS_BINDING_MRSH, ("marshal:\n"));

	b.put_unsigned_long(valid_binding_index);
	b.put_unsigned_long(total_bindings);
	for (uint32_t i = valid_binding_index; i < total_bindings; i++) {
		b.put_string((*bindings_list)[i].binding_name);
		b.put_long((*bindings_list)[i].bind_type);
	}
}

CORBA::Object_ptr
binding_iterator_impl::unmarshal(service &b)
{
	NS_PRINTF(NS_BINDING_MRSH, ("unmarshal:\n"));

	uint32_t valid_index = b.get_unsigned_long();
	uint32_t num_bindings = b.get_unsigned_long();
	naming::binding_list *iter_list =
	    new naming::binding_list(num_bindings, num_bindings);

	for (uint32_t i = valid_index; i < num_bindings; i++) {
		(*iter_list)[i].binding_name = b.get_string();
		(*iter_list)[i].bind_type = naming::binding_type(b.get_long());
	}

	binding_iterator_impl *bi_impl;

	bi_impl = new binding_iterator_impl(valid_index, num_bindings,
	    iter_list);
	return (bi_impl->get_objref());
}

void
binding_iterator_impl::unmarshal_cancel(service &b)
{
	uint32_t valid_index = b.get_unsigned_long();
	uint32_t num_bindings = b.get_unsigned_long();

	for (uint32_t i = valid_index; i < num_bindings; i++) {
		b.get_string_cancel();
		(void) b.get_long();
	}
}
