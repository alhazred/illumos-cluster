/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _NS_INTERFACE_H
#define	_NS_INTERFACE_H

#pragma ident	"@(#)ns_interface.h	1.4	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Public routines
 *
 * All public routines use an implementation defined context in
 * the name server to store, lookup or remove names.
 */

extern int cl_bind(char *name, char *data);

/*
 * Create an object with the information contained in "data" and
 * associate it with "name".
 *
 * Returns:
 *	0 on success
 *	The following errors on failure:
 *
 *	EINVAL - Invalid name specified
 *	EEXIST - Name already exists
 *	EIO    - Cannot communicate with the name server (system error)
 */


extern int cl_rebind(char *name, char *data);

/*
 * Create an object with the information contained in "data" and
 * forcefully associate it with "name".
 *
 * Returns:
 *	0 on success
 *	The following errors on failure:
 *
 *	EINVAL - Invalid name specified
 *	EIO    - Cannot communicate with the name server (system error)
 */


extern int cl_resolve(char *name, char **data);

/*
 * Lookup the object associated with "name" and return a pointer
 * to information contained in "data".
 *
 * NOTE:  The caller is responsible for freeing up the memory associated
 *        with "data"
 *
 * Returns:
 *	0 on success
 *	The following errors on failure
 *
 *	ENOENT - Name does not exist
 *	EINVAL - Invalid name specified
 *	EIO    - Cannot communicate with the name server (system error)
 */


extern int cl_unbind(char *name);

/*
 * Remove the object associated with "name" and the information
 * contained in it.
 *
 * Returns:
 *	0 on success
 *	The following errors on failure
 *
 *	ENOENT - Name does not exist
 *	ENOENT - Name not found
 *	EIO    - Cannot communicate with the name server (system error)
 */

#ifdef	__cplusplus
}
#endif

#endif	/* _NS_INTERFACE_H */
