//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma	ident	"@(#)ns_interface.cc 1.11	08/05/20 SMI"

#include <string.h>
#include <sys/errno.h>

#include <h/naming.h>
#include <h/data_container.h>
#include <sys/os.h>
#include <sys/rsrc_tag.h>
#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <nslib/data_container_impl.h>
#include <nslib/ns_interface.h>
#include <nslib/ns_lib.h>

static int _bind_data(char *name, char *data, bool rebind);
static int _resolve_data(char *name, char **data);
static int _unbind_data(char *name);
static int _get_error(CORBA::Exception *ex);

static int _initialized = 0;


// Public routines

extern "C" int
cl_bind(char *name, char *data)
{
	return (_bind_data(name, data, false));
}

extern "C" int
cl_rebind(char *name, char *data)
{
	return (_bind_data(name, data, true));
}

extern "C" int
cl_resolve(char *name, char **data)
{
	return (_resolve_data(name, data));
}

extern "C" int
cl_unbind(char *name)
{
	return (_unbind_data(name));
}


//
// Private routines
//
naming::naming_context_ptr
_get_nameserver()
{
	int error;
	naming::naming_context_ptr nptr;

	if (!_initialized) {
		if ((error = ORB::initialize()) != 0) {
			os::sc_syslog_msg syslog_msg(
			    SC_SYSLOG_NAMING_SERVICE_TAG, "naming", NULL);
			//
			// SCMSGS
			// @explanation
			// could not initialize ORB.
			// @user_action
			// Please make sure the nodes are booted in cluster
			// mode.
			//
			(void) syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "ns: Could not initialize ORB: %d", error);
			exit(1);
		}
		_initialized = 1;
	}

	nptr = ns::root_nameserver();
	ASSERT(nptr);
	return (nptr);
}

static int
_get_error(CORBA::Exception *ex)
{
	int retval;

	if (naming::not_found::_exnarrow(ex) != NULL) {
		retval = ENONET;
	} else if (naming::invalid_name::_exnarrow(ex) != NULL) {
		retval = EINVAL;
	} else if (naming::already_bound::_exnarrow(ex) != NULL) {
		retval = EEXIST;
	} else {
		retval = EIO;
	}
	return (retval);
}

static int
_bind_data(char *name, char *data, bool rebind)
{
	int retval = 0;
	Environment e;
	CORBA::Exception *ex;
	naming::naming_context_var nptr;

	nptr = _get_nameserver();

	data_container_impl *str = new data_container_impl();
	data_container::data_ptr obj = str->get_objref();

	obj->set_data(data, e);

	if (rebind) {
		nptr->rebind(name, obj, e);
	} else {
		nptr->bind(name, obj, e);
	}
	if ((ex = e.exception()) != NULL) {
		retval = _get_error(ex);
		NS_PRINTF(NS_BIND, ("ns: rebind/bind of %s failed error: %d\n",
		    name, retval));
		if (retval == ENOENT)
			retval = EINVAL;
		e.clear();
	}
	CORBA::release(obj);
	return (retval);
}

static int
_resolve_data(char *name, char **data)
{
	int retval = 0;
	Environment e;
	CORBA::Exception *ex;
	CORBA::Object_ptr obj;
	naming::naming_context_var nptr;

	nptr = _get_nameserver();

	obj = nptr->resolve(name, e);
	if ((ex = e.exception()) != NULL) {
		retval = _get_error(ex);
		NS_PRINTF(NS_RESOLVE, ("ns: Resolve of %s failed error: %d\n",
		    name, retval));
		e.clear();
		return (retval);
	}

	data_container::data_ptr dobj = data_container::data::_narrow(obj);
	CORBA::release(obj);

	if (CORBA::is_nil(dobj)) {
		retval = EINVAL;
	} else {
		*data = dobj->obtain_data(e);
	}
	CORBA::release(dobj);
	return (retval);
}

static int
_unbind_data(char *name)
{
	int retval = 0;
	Environment e;
	CORBA::Exception *ex;
	naming::naming_context_var nptr;

	nptr = _get_nameserver();

	nptr->unbind(name, e);
	if ((ex = e.exception()) != NULL) {
		retval = _get_error(ex);
		NS_PRINTF(NS_BIND, ("ns: Unbind of %s failed error: %d\n",
		    name, retval));
		e.clear();
	}
	return (retval);
}
