/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RM_STATE_MACH_IN_H
#define	_RM_STATE_MACH_IN_H

#pragma ident	"@(#)rm_state_mach_in.h	1.27	08/05/20 SMI"

//
// rm_state_mach_in.h contains inline functions for rm_state_machine.
//

inline int
reconf_noprint(const char *, replica_int::rma_reconf_ptr,
    const replica::service_num_t &)
{
	return (0);
}

inline void
rm_state_machine::state_mutex_lock()
{
	state_mutex.lock();
}

inline void
rm_state_machine::state_mutex_unlock()
{
	state_mutex.unlock();
}

inline bool
rm_state_machine::state_lock_held()
{
	return ((bool) state_mutex.lock_held());
}

inline
void
rm_state_machine::signal_external_state_change()
{
	CL_PANIC(state_mutex.lock_held());
	external_cv.broadcast();
}

inline
void
rm_state_machine::wait_for_external_state_change()
{
	CL_PANIC(state_mutex.lock_held());
	external_cv.wait(&state_mutex);
}

// Called by other threads to notify this state machine that there may be work
// to do.
inline void
rm_state_machine::signal_state_machine()
{
	is_stable_state = false; // Knock it out of stable state if it is there
	cv.signal();
}

//
// Called by the state machine thread when it is in a stable state. It waits
// for notification from unreferenced or an incoming invocation to tell it that
// there is work to do.
//
inline void
rm_state_machine::block_in_stable_state()
{
	CL_PANIC(state_mutex.lock_held());
	is_stable_state = true;
	signal_external_state_change();
	while (is_stable_state) {
		cv.wait(&state_mutex);
	}
}

inline bool
rm_state_machine::is_stable()
{
	return (is_stable_state);
}

inline rm_state_machine::sm_service_state
rm_state_machine::get_state() const
{
	return (current_state);
}

inline bool
rm_state_machine::max_secondaries_active()
{
	return (s->max_secondaries_active());
}

//
// Returns true if we're processing a switchover
//
inline bool
rm_state_machine::switchover()
{
	return ((cur_primary_p->get_sm_prov_state() &
	    rm_repl_service::prov_reg_impl::
	    SM_PRIMARY_TO_MAKE_SECONDARY) != 0);
}

inline bool
rm_state_machine::service_shutdown()
{
	CL_PANIC(state_lock_held());
	return (s->is_deleted());
}

inline rm_repl_service::prov_reg_impl *
rm_state_machine::find_sec_to_remove()
{
	rm_repl_service::prov_reg_impl	*p = get_repl_prov(
	    rm_repl_service::prov_reg_impl::SM_SEC_TO_BE_REMOVED);
	RM_DBG(("rm_sm %s: find_sec_to_remove() p = %p\n", s->id(), p));
	return (p);
}

inline rm_repl_service::prov_reg_impl *
rm_state_machine::find_sec_to_commit()
{
	rm_repl_service::prov_reg_impl	*p = get_repl_prov(
	    rm_repl_service::prov_reg_impl::SM_SEC_NEED_COMMIT);
	return (p);
}

inline rm_repl_service::prov_reg_impl *
rm_state_machine::get_primary()
{
	return (cur_primary_p);
}

inline void
rm_state_machine::store_recovery_data(
    rm_repl_service::service_recovery_data *data_p)
{
	get_states_list.append(data_p);
}

inline bool
rm_state_machine::must_freeze_for_upgrade()
{
	return (s->is_frozen_for_upgrade());
}

#endif	/* _RM_STATE_MACH_IN_H */
