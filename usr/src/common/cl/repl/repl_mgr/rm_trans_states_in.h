/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_TRANS_STATES_IN_H
#define	_RM_TRANS_STATES_IN_H

#pragma ident	"@(#)rm_trans_states_in.h	1.10	08/05/20 SMI"

//
// rm_trans_states_in.h implements inline methods for the
// rm_trans_states class.
//

inline
trans_state_register_service::trans_state_register_service()
{
}

inline
trans_state_register_service::~trans_state_register_service()
{
}

inline void
trans_state_register_service::orphaned(Environment &)
{
}

inline void
trans_state_register_service::committed()
{
}

inline
trans_state_get_control::trans_state_get_control(
    service_admin_impl *service_admin_p) : sai(service_admin_p)
{
}

inline
trans_state_get_control::~trans_state_get_control()
{
}

inline void
trans_state_get_control::orphaned(Environment &)
{
}

inline void
trans_state_get_control::committed()
{
}

inline service_admin_impl *
trans_state_get_control::get_return_value()
{
	return (sai);
}

inline
trans_state_add_rma::trans_state_add_rma(repl_mgr_impl *obj,
    replica_int::rma_reconf_ptr r) :
	rm_repl_obj(obj), rma_p(replica_int::rma_reconf::_duplicate(r))
{
}

inline
trans_state_add_rma::~trans_state_add_rma()
{
	CORBA::release(rma_p);
	rm_repl_obj = NULL; // For lint...
}

inline void
trans_state_add_rma::orphaned(Environment &e)
{
	// Just treat it as a retry.
	rm_repl_obj->add_rma(rma_p, e);
}

inline void
trans_state_add_rma::committed()
{
	//
	// It is possible that a commit is invoked on a RM provider that
	// has just been promoted to the new RM primary after RM failover. By
	// the time the committed() is invoked, this new RM primary has already
	// completed recovery and taken care of uncommitted add_rma() if
	// there are any so the committed() there is redundant. We should
	// simply ignore it on the primary. See bug 4878186 for more
	// information.
	//
	if (!repl_mgr_impl::get_rm()->is_primary_mode()) {
		rm_repl_obj->add_rma_done_ckpt();
	}
}

inline
trans_state_register_lca::trans_state_register_lca(
    cb_coord_control_impl *impl_obj)
{
	cbcc_p = impl_obj;
}

inline
trans_state_register_lca::~trans_state_register_lca()
{
}

inline void
trans_state_register_lca::orphaned(Environment &)
{
	ASSERT(cbcc_p);
	cbcc_p = NULL;
}

inline void
trans_state_register_lca::committed()
{
}

inline cb_coord_control_impl *
trans_state_register_lca::get_control_impl()
{
	return (cbcc_p);
}

#endif	/* _RM_TRANS_STATES_IN_H */
