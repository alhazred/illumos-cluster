/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SERVICE_ADMIN_IMPL_IN_H
#define	_SERVICE_ADMIN_IMPL_IN_H

#pragma ident	"@(#)service_admin_impl_in.h	1.13	08/05/20 SMI"

//
// service_admin_impl_in.h contains inline functions for
// service_admin_impl.
//

inline
service_admin_impl::sa_state_count::sa_state_count() :
    service_count(0)
{
}

inline
service_admin_impl::sa_state_count::~sa_state_count()
{
}

inline void
service_admin_impl::sa_state_count::set_state_count(
    const service_state_count sa_cnt)
{
	service_count = sa_cnt;
}

inline const service_admin_impl::sa_state_count &
service_admin_impl::get_sa_state_count()
{
	return (service_admin_state_count);
}

inline	const service_state_count	&
service_admin_impl::sa_state_count::get_count()
{
	return (service_count);
}

inline trans_state_reg_rma_repl_prov	*
service_admin_impl::get_saved_primary_reg_rma_repl_prov_state(
    Environment &e)
{
	primary_ctx			*ctxp
	    = primary_ctx::extract_from(e);
	trans_state_reg_rma_repl_prov	*st
	    = (trans_state_reg_rma_repl_prov *) ctxp->get_saved_state();

	return (st);
}

inline trans_state_shutdown_service	*
service_admin_impl::get_saved_primary_shutdown_service_state(
    Environment &e)
{
	primary_ctx			*ctxp
	    = primary_ctx::extract_from(e);
	trans_state_shutdown_service	*st
	    = (trans_state_shutdown_service *) ctxp->get_saved_state();

	return (st);
}

inline const rm_repl_service &
service_admin_impl::get_service()
{
	return (s);
}

inline bool
service_admin_impl::was_forced_shutdown()
{
	return (forced_shutdown);
}
#endif	/* _SERVICE_ADMIN_IMPL_IN_H */
