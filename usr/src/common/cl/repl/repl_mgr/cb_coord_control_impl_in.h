/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CB_COORD_CONTROL_IMPL_IN_H
#define	_CB_COORD_CONTROL_IMPL_IN_H

#pragma ident	"@(#)cb_coord_control_impl_in.h	1.4	08/05/20 SMI"

inline bool
cb_coordinator::is_upgrade_commit_in_progress()
{
	ASSERT(rwlck.lock_held());

	return (upgrade_commit_progress > version_manager::NOT_STARTED);
}

inline void
cb_coordinator::wrlock_rwlock()
{
	rwlck.wrlock();
}

inline void
cb_coordinator::rdlock_rwlock()
{
	rwlck.rdlock();
}

inline void
cb_coordinator::unlock_rwlock()
{
	rwlck.unlock();
}

inline version_manager::vm_lca_ptr
cb_coord_control_impl::get_lca()
{
	return (version_manager::vm_lca::_duplicate(lca_v));
}

inline bool
cb_coord_control_impl::is_callback_in_progress()
{
	ASSERT(rwlck.read_held());
	return (callback_in_progress != 0);
}

inline repl_rm::rm_ckpt_ptr
cb_coord_control_impl::get_checkpoint()
{
	return (((repl_mgr_prov_impl *)get_provider())->
	    get_checkpoint_rm_ckpt());
}

inline void
cb_coord_control_impl::rdlock_rwlock()
{
	rwlck.rdlock();
}

inline void
cb_coord_control_impl::unlock_rwlock()
{
	rwlck.unlock();
}




#endif	/* _CB_COORD_CONTROL_IMPL_IN_H */
