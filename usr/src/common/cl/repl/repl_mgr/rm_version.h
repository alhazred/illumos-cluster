/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_VERSION_H
#define	_RM_VERSION_H

#pragma ident	"@(#)rm_version.h	1.5	08/05/20 SMI"

//
// Define a structure for mapping versions of various idl interfaces
// in the replica framework to VP spec files. This code supports
// versions 1.0, 1.1, 1.2 and 1.3. Rows are indexed by minor_num of current
// running version.
//
typedef struct rm_vp {
	int	rm_admin;
	int	rm_ckpt;
} rm_vp_map;

extern rm_vp_map rm_ver_map[2][4];
//
// Returns the current version of rm_admin interfaces based of the
// running version of the RM
//
#define	RM_ADMIN_VERS(rv)	\
		rm_ver_map[(rv).major_num][(rv).minor_num].rm_admin

//
// Returns the current version of rm_admin interfaces based of the
// running version of the RM
//
#define	RM_CKPT_VERS(rv)	\
		rm_ver_map[(rv).major_num][(rv).minor_num].rm_ckpt


#endif	/* _RM_VERSION_H */
