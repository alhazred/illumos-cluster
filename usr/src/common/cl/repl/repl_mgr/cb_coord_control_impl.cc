//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cb_coord_control_impl.cc	1.20	08/06/23 SMI"

//
// cb_coord_control_impl.cc implements the callback interface
// of the rolling upgrade framework.
//

#include <repl/repl_mgr/cb_coord_control_impl.h>
#include <repl/repl_mgr/repl_mgr_debug.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <repl/repl_mgr/rm_version.h>
#include <vm/vm_comm.h>
#include <vm/vm_admin_impl.h>
#include <vm/vm_lca_impl.h>
#include <vm/vm_dbgbuf.h>

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#include <orb/fault/fault_functions.h>
#endif

// Class cb_coordinator

// Constructor
cb_coordinator::cb_coordinator() :
    last_upgrade_seq(0),
    num_lcas_doing_callback(0),
    upgrade_commit_progress(version_manager::NOT_STARTED)
{
	RM_DBG(("rm_cb:  in cb_coordinator::cb_coordinator()\n"));
}

// Destructor
cb_coordinator::~cb_coordinator()
{
	RM_DBG(("rm_cb:  in cb_coordinator::~cb_coordinator()\n"));
	rwlck.wrlock();
	ucc_hashtable.dispose();
	cb_coord_control_list.dispose();
	rwlck.unlock();
}

// Dumpstate method. Called from repl_mgr_prov_impl::add_secondary().
void
cb_coordinator::add_sec_cb_ckpts(repl_rm::rm_ckpt_ptr ckpt_p, Environment &e)
{
	RM_DBG(("rm_cb:  in cb_coordinator::add_sec_cb_ckpts()\n"));

	uint32_t	seq_size = 0;

	rwlck.rdlock();

	// Build lca and cbcntl sequences.
	seq_size = cb_coord_control_list.count();
	version_manager::lca_seq_t	lca_seq(seq_size, seq_size);
	replica_int::cbcntl_seq_t	cbcntl_seq(seq_size, seq_size);

	SList<cb_coord_control_impl>::ListIterator
	    lca_iter(cb_coord_control_list);
	cb_coord_control_impl *cb_cntl = NULL;

	//
	// Get the current running version of the replica manager. No need
	// to use lock to protect reading of the running version here. The
	// version is only used to get a massaged reference to cb_cntl for
	// constructing secondary on the joining node. The reference will
	// be released in the end.
	//
	repl_mgr_prov_impl *rm_provp = repl_mgr_impl::get_rm()->get_rm_prov();
	version_manager::vp_version_t	v;
	rm_provp->get_running_version(v);

	// Iterate thru the cb_coord_control_list.
	for (uint_t i = 0; (cb_cntl = lca_iter.get_current()) != NULL;
	    lca_iter.advance(), i++) {
		//
		// Store references to lca and its associated cb_coord_control
		// object into lca_seq and cbcntl_seq respectively.
		//
		lca_seq[i] = cb_cntl->get_lca();
		cbcntl_seq[i] = cb_cntl->get_objref(
		    replica_int::cb_coord_control::
		    _get_type_info(RM_ADMIN_VERS(v)));
	}

	// Build ucc sequence.
	seq_size = ucc_hashtable.count();
	version_manager::ucc_seq_t	u_seq(seq_size, seq_size);

	string_hashtable_t<version_manager::ucc *>::iterator
	    u_iter(ucc_hashtable);
	version_manager::ucc *ucc_p = NULL;

	// Iterate through ucc_hashtable.
	for (uint_t i = 0; (ucc_p = u_iter.get_current()) != NULL;
	    u_iter.advance(), i++) {
		// Store ucc into u_seq.
		u_seq[i] = *ucc_p;
	}

	//
	// Ckpt the current upgrade_commit_progress. We want to make sure
	// that we checkpoint upgrade_commit_progress before doing
	// ckpt_dump_cb_coordinator(). This is because ckpt_dump_cb_coordinator
	// checks upgrade_commit_progress and does lock_vps_for_upgrade_commit
	// if necessary.
	//
	if (is_upgrade_commit_in_progress()) {
		ckpt_p->ckpt_set_upgrade_commit_progress(
		    upgrade_commit_progress, e);
		CORBA::Exception	*ex;
		if ((ex = e.exception()) != NULL) {
			//
			// XXX Will we get a version exception here?
			//
			// If we get a version exception here, the joining node
			// is mostly running in an old version, it's
			// ok as it will panic as soon as its vm_lca attempts to
			// register with the cb_coordinator.
			//
			CL_PANIC(CORBA::VERSION::_exnarrow(ex) ||
			    CORBA::COMM_FAILURE::_exnarrow(ex));
			e.clear();
		}
	}

	//
	// Send one dumpstate ckpt with all content of this cb_coordinator
	// object.
	//
	ckpt_p->ckpt_dump_cb_coordinator(lca_seq, cbcntl_seq, u_seq, e);
	if (e.exception()) {
		RM_DBG(("rm_cb:  add_sec_cb_ckpts(), got exception\n"));
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
		e.clear();
	}

	rwlck.unlock();
}

//
// Method for registering a ucc. Called from cb_coord_control object.
// When this method is called on RM secondaries, we can skip the
// validating part. Boolean do_check is used for this purpose.
//
void
cb_coordinator::register_ucc(const version_manager::ucc &new_ucc,
    Environment &e)
{
	RM_DBG(("rm_cb: Got request to register ucc with name %s\n",
	    (const char *)(new_ucc.ucc_name)));

	rwlck.wrlock();

	// Verify ucc on RM primary.
	if (repl_mgr_impl::get_rm()->is_primary_mode()) {
		if (is_upgrade_commit_in_progress()) {
			//
			// An upgrade commit is already in progress. Normally,
			// register_upgrade_callback() requests will be blocked
			// at reading of VPs as we lock vps before doing
			// upgrade_commit. However, there is a small window in
			// which a register_ucc comes through before we have a
			// chance to lock vps. In this case, we have to tell the
			// caller to retry and hopefully it will be blocked
			// in its retry.
			//
			rwlck.unlock();
			e.exception(new version_manager::retry_needed());
			return;
		}

		// Verify that the ucc is valid to register.
		bool valid = verify_ucc_registration(new_ucc, e);
		// Check the return value and for any exceptions.
		if (!valid || e.exception()) {
			rwlck.unlock();
			return;
		}

		RM_DBG(("rm_cb: Successfully validated ucc with name %s so "
		    "registering it on the primary and secondaries\n",
		    (const char *)(new_ucc.ucc_name)));
	}

	// Add the validated ucc.
	version_manager::ucc	*ucc_p = new version_manager::ucc(new_ucc);
	ucc_hashtable.add(ucc_p, new_ucc.ucc_name);

	if (repl_mgr_impl::get_rm()->is_primary_mode()) {
		// Checkpoint registration of the new ucc.
		repl_rm::rm_ckpt_var ckpt_v =
		    repl_mgr_impl::get_rm()->get_checkpoint();
		ckpt_v->ckpt_register_ucc(new_ucc, e);
	}

	rwlck.unlock();
}

// Unregister UCCs that are no longer needed after an upgrade commit.
void
cb_coordinator::unregister_uccs(Environment &e)
{
	rwlck.wrlock();

	RM_DBG(("rm_cb: in unregister_uccs()\n"));

	//
	// The cb_coord primary first selects which uccs need to be
	// unregistered and cleaned up.  The selection and clean up occurs
	// locally on this node.  The selected uccs are then sent to the
	// other nodes via the lca checkpointing so cleanup occurs across the
	// cluster.
	//
	version_manager::string_seq_t  ucc_name_seq;
	select_and_remove_uccs(ucc_name_seq);

	Environment			env;
	cb_coord_control_impl		*cb_cntl = NULL;
	version_manager::vm_lca_var	lca_v;
	SList<cb_coord_control_impl>::ListIterator
	    lca_iter(cb_coord_control_list);
	for (cb_cntl = NULL; (cb_cntl = lca_iter.get_current()) != NULL;
	    lca_iter.advance()) {
		lca_v = cb_cntl->get_lca();
		lca_v->unregister_uccs(ucc_name_seq, env);
		if (env.exception() &&
		    CORBA::COMM_FAILURE::_exnarrow(env.exception())) {
			env.clear();
		}
	}

	// Do ckptpoint
	repl_rm::rm_ckpt_var ckpt_v = repl_mgr_impl::get_rm()->get_checkpoint();
	ckpt_v->ckpt_unregister_uccs(ucc_name_seq, e);
	CL_PANIC(!e.exception());

	rwlck.unlock();
}

// Method for addition of a cb_coord_control object.
void
cb_coordinator::add_cb_coord_control(cb_coord_control_impl *new_controlp)
{
	RM_DBG(("rm_cb: in add_cb_coord_control()\n"));

	// Lock the cb_coord_control_list for write.
	rwlck.wrlock();

	CL_PANIC(!cb_coord_control_list.exists(new_controlp));
	cb_coord_control_list.append(new_controlp);

	rwlck.unlock();
}

// Method for removal of a cb_coord_control object.
void
cb_coordinator::remove_cb_coord_control(cb_coord_control_impl *dead_controlp)
{
	rwlck.wrlock();

	//
	// is_callback_in_progress() expects the read lock to be held
	// on the corresponding cb_coord_impl_objet.
	//
	dead_controlp->rdlock_rwlock();
	if (repl_mgr_impl::get_rm()->is_primary_mode() &&
	    dead_controlp->is_callback_in_progress()) {
		dead_controlp->unlock_rwlock();
		//
		// The dead lca was in the middle of processing callbacks,
		// since it's dead, there's no need to wait for it to
		// complete its callback task anymore.
		//
		lck.lock();

		ASSERT(num_lcas_doing_callback > 0);

		if (--num_lcas_doing_callback == 0) {
			cv.broadcast();
		}
		lck.unlock();
	} else {
		dead_controlp->unlock_rwlock();
	}

	bool	ret = cb_coord_control_list.erase(dead_controlp);
	CL_PANIC(ret);

	rwlck.unlock();
}

//
// Called by cb_coord_control after its associated lca has completed the
// given callback task.
//
void
cb_coordinator::callback_task_done_on_one_lca(uint32_t callback_task_id)
{
	lck.lock();
	ASSERT(num_lcas_doing_callback > 0);
	if (--num_lcas_doing_callback == 0) {

		FAULTPT_VM(FAULTNUM_VM_CB_COORD_CB_TASK_DONE_1,
			FaultFunctions::generic);

		RM_DBG(("rm_cb: Callback task id %d done on all lcas that "
		    "were processing callbacks\n", callback_task_id));
		cv.broadcast();

		FAULTPT_VM(FAULTNUM_VM_CB_COORD_CB_TASK_DONE_2,
			FaultFunctions::generic);
	}
	lck.unlock();
}

//
// Perform lock vps on the newly joined RM secondary if necessary.
// ckpt_dump_cb_coordinator().
//
// Caller:	repl_mgr_prov_impl::ckpt_dump_cb_coordinator()
// Parameters:	None
// Return:	void
//
void
cb_coordinator::lock_vps_on_new_sec()
{
	CL_PANIC(!repl_mgr_impl::get_rm()->is_primary_mode());

	rwlck.rdlock();

	if (is_upgrade_commit_in_progress() &&
	    upgrade_commit_progress < version_manager::CLEANUP) {
		//
		// If there is an upgrade commit in progress, we need to
		// do lock vps on the new joining node. However, we don't
		// need to do the lock_vps if the upgrade commit progress
		// is CLEANUP which means the upgrade commit is about to finish
		// and the vps will be unlocked soon.
		//
		vm_comm::the().lock_vps_for_upgrade_commit();
	}
	rwlck.unlock();
}

//
// Recovery code, called by repl_mgr_impl::recover() on the new RM primary
// after RM failover.
//
void
cb_coordinator::recover(Environment &)
{
	RM_DBG(("rm_cb: in recover()\n"));
	rwlck.rdlock();

	if (is_upgrade_commit_in_progress()) {
		//
		// There was an upgrade_commit thread in progress on the
		// old RM primary, we need to resume it.
		//

		RM_DBG(("rm_cb: need to resume upgrade_commit\n"));

		if (upgrade_commit_progress < version_manager::CLEANUP) {
			//
			// If the progress is CLEANUP_UCCS, we should have
			// destroyed the ucc group list on the old primary so
			// we don't need to reconstruct them.
			//
			construct_ucc_group_list();
		}

		// Allocate a thread to process the upgrade commit task
		upgrade_commit_task *ut = new upgrade_commit_task(true);
		common_threadpool::the().defer_processing(ut);
	}

	rwlck.unlock();
}

//
// Upgrade commit method. Launch an upgrade commit thread.
// Caller: repl_mgr_impl::upgrade_commit
//
void
cb_coordinator::upgrade_commit(version_manager::vp_state &state,
    version_manager::membership_bitmask_t &outdated_nodes, Environment &)
{
	upgrade_commit_task	*ut = NULL;

	//
	// Grab the read lock because we are going to set upgrade commit
	// progress.
	//
	rwlck.wrlock();
	if (!is_upgrade_commit_in_progress()) {
		//
		// Check with the version manager to see if we need to do an
		// upgrade_commit.
		//
		Environment	env;
		if (!vm_comm::the_vm_admin_impl().is_upgrade_needed(state,
		    outdated_nodes, env)) {
			ASSERT(!env.exception());
			rwlck.unlock();
			return;
		} else {
			//
			// Since an upgrade is needed, it is necessary to
			// reset the state and outdated_nodes variables back
			// to their original values.
			//
			state = version_manager::ALL_VERSIONS_MATCH;
			outdated_nodes = 0;
		}

		// Construct ucc group list for callbacks.
		construct_ucc_group_list();

		FAULTPT_VM(FAULTNUM_VM_CONSTRUCT_UCC_GROUP,
			FaultFunctions::generic);

		//
		// Set upgrade commit progress on the local cb_coordinator
		// before unlock to prevent other thread from doing another
		// upgrade commit.
		//
		set_local_upgrade_commit_progress(
		    version_manager::CALCULATE_NEW_RVS_UVS);
		rwlck.unlock();

		FAULTPT_VM(FAULTNUM_VM_CALCULATE_NEW_RVS_UVS,
			FaultFunctions::generic);

		// Upgrade the replica manager first if necessary.
		repl_mgr_prov_impl		*rm_provp =
		    repl_mgr_impl::get_rm()->get_rm_prov();
		version_manager::vp_version_t	v;
		rm_provp->get_running_version(v);
		if (v.major_num == 1 && v.minor_num == 0) {
			RM_DBG(("rm_cb: upgrade from 1.0 to 1.1 first\n"));
			//
			// Since the ckpt_start_upgrade_commit() only exists in
			// replica_manager vp 1.1 and onwards, we need to make
			// sure that the multi-ckpt interface is upgraded to
			// at least 1.1 or above.
			//
			vm_comm::the().lock_vps_for_upgrade_commit();

			lck.lock();
			//
			// This will trigger a cmm reconfiguration to upgrade
			// non-btstrp cl vps to next possible versions and
			// perform upgrade callback on the replica manager and
			// other bootstrap cluster vps if necessary.
			//
			last_upgrade_seq =
			    vm_comm::the().start_upgrade_commit();

			cmm::seqnum_t start_upgrade_seq = last_upgrade_seq;

			do {
				//
				// Wait for the new rvs and uvs to be
				// calculated.
				//
				cv.wait(&lck);
			} while (start_upgrade_seq == last_upgrade_seq);
			lck.unlock();

			//
			// As long as an idl invocation is in progress in the
			// replica manager, no new nodes can join the cluster.
			// So we don't have to worry that a new node with
			// replica_manager vp version 1.0 software which doesn't
			// support the ckpt_set_upgrade_commit_progress()
			// ckpt interface joins in the middle of the above
			// operations. Once the above start_upgrade_commit() is
			// finished, we are gauranteed by the version manager
			// that no new nodes with replica_manager vp version 1.0
			// software can join the cluster anymore.
			//

			// Verify that we have been upgraded successfully.
			rm_provp->get_running_version(v);
			ASSERT(v.major_num == 1 && v.minor_num == 1);
		}

		// Allocate a thread to process all the callbacks
		ut = new upgrade_commit_task(false);
		common_threadpool::the().defer_processing(ut);

		FAULTPT_VM(FAULTNUM_VM_ALLOCATE_UPGD_COMMIT_TASK,
			FaultFunctions::generic);
	} else {
		rwlck.unlock();

		//
		// There's already an upgrade_commit in progress, simply
		// return. Do nothing.
		//
		// Before returning, we need to reset the state and
		// outdated_nodes variables back to their original values.
		// See bug 4925020 for more details.
		//
		state = version_manager::ALL_VERSIONS_MATCH;
		outdated_nodes = 0;

		RM_DBG(("rm_cb: in upgrade_commit(), nothing to do\n"));
	}
}

// Set upgrade commit progress on the local cb_coordinator.
void
cb_coordinator::set_local_upgrade_commit_progress(
    version_manager::commit_progress new_progress)
{
	ASSERT(rwlck.write_held());

	RM_DBG(("rm_cb: in set_local_upgrade_commit_progress, "
	    "new progress %d.\n", new_progress));

	upgrade_commit_progress = new_progress;
}

//
// Called from CMM step -- signal_cbc_rmm_cleanup_step.
// After the cb_coordinator issues start_upgrade_commit() to the version
// manager, it waits for the calculation of new rvs and uvs to complete.
// This function is called by the CMM step to send a notification back
// to the cb_coordinator.
// Parameter:	cur_upgrade_seq -- indicates the current upgrade_seq stored
//		in the version_manager. The value of upgrade_seq gets changed
//		only after caculations of new rvs and uvs triggered by
//		start_upgrade_commit() finish. We use this value to determine
//		if we receive a signal prematurely or not.
// Return:	void
//
void
cb_coordinator::signal_cb_coordinator(cmm::seqnum_t cur_upgrade_seq)
{
	if (repl_mgr_impl::get_rm()->is_primary_mode()) {
		lck.lock();
		if (cur_upgrade_seq > last_upgrade_seq) {
			RM_DBG(("rm_cb: signal_cb_coordinator need to "
			    "signal()\n"));
			last_upgrade_seq = cur_upgrade_seq;
			cv.broadcast();
		}
		lck.unlock();
	}
}

// Helper function: To make idl invocation on all LCAs.
void
cb_coordinator::iterate_over(void (version_manager::vm_lca::*mfn)(
    Environment &e),
    const char *const fn_name)
{
	Environment env;

	RM_DBG(("rm_cb: in iterate_over of %s\n", fn_name));

	rwlck.rdlock();

	cb_coord_control_impl		*cb_cntl = NULL;
	version_manager::vm_lca_ptr	lca_p;
	SList<cb_coord_control_impl>::ListIterator
	    lca_iter(cb_coord_control_list);

	for (cb_cntl = NULL; (cb_cntl = lca_iter.get_current()) != NULL;
	    lca_iter.advance()) {
		lca_p = cb_cntl->get_lca();
		(lca_p->*mfn)(env);
		if (env.exception()) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()));
			env.clear();
		}
		CORBA::release(lca_p);

		FAULTPT_VM(FAULTNUM_VM_CB_COORD_ITERATE_OVER_LCA,
			FaultFunctions::generic);
	}
	rwlck.unlock();
}

#ifdef DEBUG
//
// This is a debug hook used by query_vm to call into the cb_coordinator
// to aid in testing and debugging.  It is called by
// vm_admin_impl::debug_request() which is the reason why it outputs to VM_DBG
// or to stdout.  The motivation to print the outstring is to support automated
// tests that will use debug_print to verify proper behaviour.
//
void
cb_coordinator::debug_print(char **debug_string)
{
	uint_t	i = 0;
	version_manager::ucc	*ucc_p = NULL;
	string_hashtable_t<version_manager::ucc *>::iterator ui(ucc_hashtable);

	// Allocate if debug_string is currently empty.
	if (*debug_string == NULL) {
		*debug_string = new char[MAXOUTSTRING];
	}
	//
	// Print out the uccs contained in the cb_coordinator into the
	// debug_string by appending each line.  (We have to use os::snprintf()
	// because os::strcat() is not a method in the os class.)
	//
	(void) os::snprintf(*debug_string, MAXOUTSTRING,
	    "uccs associated with cluster vps on the cb_coord : \n");
	while ((ucc_p = ui.get_current()) != NULL) {
		ui.advance();
		(void) os::snprintf(*debug_string + os::strlen(*debug_string),
		    MAXOUTSTRING,
		    "non-btstrp cl ucc:%s; vp:%s",
		    (const char *)ucc_p->ucc_name,
		    (const char *)ucc_p->vp_name);
		// Print the Before list.
		for (i = 0; i < ucc_p->upgrade_before.length(); i++) {
			if (i == 0) {
				(void) os::snprintf(*debug_string +
				    os::strlen(*debug_string),
				    MAXOUTSTRING,
				    "; Bef : %s",
				    (const char *)ucc_p->upgrade_before[i]);
			} else {
				(void) os::snprintf(*debug_string +
				    os::strlen(*debug_string),
				    MAXOUTSTRING,
				    ", %s",
				    (const char *)ucc_p->upgrade_before[i]);
			}
		}
		// Print the Concurrent list.
		for (i = 0; i < ucc_p->upgrade_concurrently.length(); i++) {
			if (i == 0) {
				(void) os::snprintf(*debug_string +
				    os::strlen(*debug_string),
				    MAXOUTSTRING,
				    "; Con : %s", (const char *)
				    ucc_p->upgrade_concurrently[i]);
			} else {
				(void) os::snprintf(*debug_string +
				    os::strlen(*debug_string),
				    MAXOUTSTRING,
				    ", %s", (const char *)
				    ucc_p->upgrade_concurrently[i]);
			}
		}
		// Print the Freeze list.
		for (i = 0; i < ucc_p->freeze.length(); i++) {
			if (i == 0) {
				(void) os::snprintf(*debug_string +
				    os::strlen(*debug_string),
				    MAXOUTSTRING,
				    "; Frz : %s", (const char *)
				    ucc_p->freeze[i]);
			} else {
				(void) os::snprintf(*debug_string +
				    os::strlen(*debug_string),
				    MAXOUTSTRING,
				    ", %s", (const char *)
				    ucc_p->freeze[i]);
			}
		}
		(void) os::snprintf(*debug_string + os::strlen(*debug_string),
		    MAXOUTSTRING, "\n");
	}
}

//
// This is a debug hook used by query_vm to call into the cb_coordinator
// construct, print to the outstring variable, and then destroy the ucc_group
// list.  Please note that this cannot be called during an upgrade commit as it
// is protected via locks.
//
void
cb_coordinator::debug_print_uglist(char **debug_string)
{
	ucc_group * cur_ug = NULL;
	version_manager::ucc	*ucc_p;
	uint_t	ug_id = 0;

	// Allocate if debug_string is currently empty.
	if (*debug_string == NULL) {
		*debug_string = new char[MAXOUTSTRING];
	}

	// Construct the list.
	rwlck.wrlock();
	construct_ucc_group_list();
	//
	// Print out the order of uccs into the
	// debug_string by appending each line.  (We have to use os::snprintf()
	// because os::strcat() is not a method in the os class.)
	//
	ucc_group_list.atfirst();
	(void) os::snprintf(*debug_string + os::strlen(*debug_string),
	    MAXOUTSTRING, "ucc_groups in callback order :\n");
	while ((cur_ug = ucc_group_list.get_current()) != NULL) {
		ucc_group_list.advance();
		// Print out each ucc in this ucc_group.
		(void) os::snprintf(*debug_string + os::strlen(*debug_string),
		    MAXOUTSTRING, "ucc_group %u: ", ++ug_id);
		cur_ug->atfirst_concurrent();
		while ((ucc_p = cur_ug->get_next_concurrent()) != NULL) {
			(void) os::snprintf(*debug_string +
			    os::strlen(*debug_string),
			    MAXOUTSTRING, "%s ", (const char *)ucc_p->ucc_name);
		}
		(void) os::snprintf(*debug_string + os::strlen(*debug_string),
		    MAXOUTSTRING, "\n");
	}
	// Destroy the list.
	destroy_ucc_group_list();
	rwlck.unlock();
}
#endif // DEBUG

//
// Verify the ucc's correctness before registration into the callback
// coordinator.
// If the ucc is valid for registration, the return value will be
// 'true' and there will be no exception present in the environment.
//
bool
cb_coordinator::verify_ucc_registration(const version_manager::ucc &new_ucc,
    Environment &e)
{
	version_manager::ucc	*ucc_p = NULL;
	version_manager::ucc	*con_ucc = NULL;
	version_manager::ucc	*cand_con = NULL;
	uint_t	subtree_num = 0;
	uint_t	num_bef_ucc = 0;
	uint_t	num_con_ucc = 0;
	uint_t	num_bef_con = 0;
	uint_t	i = 0;
	uint_t	j = 0;
	uint_t	k = 0;
	char *invalid_ucc = NULL;
	SList<version_manager::ucc> con_list;

	RM_DBG(("rm_cb: Primary node starting validation of ucc with name %s\n",
	    (const char *)(new_ucc.ucc_name)));
	//
	// Check to see if this ucc has already been registered.
	// ucc's are inserted into a hashtable and keyed by name.
	//
	if ((ucc_p = ucc_hashtable.lookup(new_ucc.ucc_name)) != NULL) {
		if (ucc_isequal(new_ucc, *ucc_p)) {
			//
			// An identical ucc has been registered previously, so
			// simply return false.	 Returning false without an
			// exception will prevent this ucc from being
			// registered twice.
			//
			RM_DBG(("rm_cb: ucc with name %s already registered\n",
			    (const char *)(new_ucc.ucc_name)));
			return (false);
		} else {
			//
			// Another ucc with the same name but differing content
			// (in the 'upgrade_before', 'upgrade_concurrently', or
			// 'freeze' data fields) has been previously
			// registered.  This is an error - all ucc's with
			// the same name must have identical content.
			//
			RM_DBG(("rm_cb: ucc with name %s already registered"
			    " but with different content\n",
			    (const char *)(new_ucc.ucc_name)));
			e.exception(new version_manager::ucc_redefined());
			return (false);
		}
	}
	//
	// The ucc has not been previously registered, so run a series of
	// validation checks.
	// NOTE : The list of HA services listed in the 'freeze' field
	// 	are assumed to be valid and correct for freezing and thawing.

	// Check 1:
	// Ensure that the ucc's specified in the 'upgrade_before' and
	// 'upgrade_concurrently' have been properly registered.  If not,
	// then return an exception.
	// First search all names in the 'upgrade_before' list.
	//
	num_bef_ucc = new_ucc.upgrade_before.length();
	for (i = 0; i < num_bef_ucc; i++) {
		if (ucc_hashtable.lookup(new_ucc.upgrade_before[i]) != NULL) {
			continue;
		}
		// This ucc was not previously registered.
		RM_DBG(("rm_cb: ucc with name %s defines 'before' ucc %s that "
		    "has not been previously registered\n",
		    (const char *)(new_ucc.ucc_name),
		    (const char *)(new_ucc.upgrade_before[i])));
		e.exception(new version_manager::invalid_ucc_dependencies());
		return (false);
	}
	// Search all names in the 'upgrade_concurrently' list.
	num_con_ucc = new_ucc.upgrade_concurrently.length();
	for (i = 0; i < num_con_ucc; i++) {
		if (ucc_hashtable.lookup(new_ucc.upgrade_concurrently[i]) !=
		    NULL) {
			continue;
		}
		// This ucc was not previously registered.
		RM_DBG(("rm_cb: ucc with name %s defines 'concurrently'"
		    " ucc %s that has not been previously registered\n",
		    (const char *)(new_ucc.ucc_name),
		    (const char *)(new_ucc.upgrade_concurrently[i])));
		e.exception(new version_manager::invalid_ucc_dependencies());
		return (false);
	}
	//
	// Check 2:
	// Ensure that the 'upgrade_concurrently' and 'upgrade_before'
	// lists are mutually exclusive.
	//
	for (i = 0; i < num_bef_ucc; i++) {
		for (j = 0; j < num_con_ucc; j++) {
			if (strcmp(new_ucc.upgrade_before[i],
			    new_ucc.upgrade_concurrently[j]) != 0) {
				continue;
			}
			//
			// The same ucc is erroneously in both
			// the before and concurrent list.
			//
			RM_DBG(("rm_cb: ucc with name %s has a ucc %s "
			    "erroneously in both the 'concurrently' and "
			    "'before' lists\n",
			    (const char *)(new_ucc.ucc_name),
			    (const char *)(new_ucc.upgrade_before[i])));
			e.exception(new version_manager::
			    invalid_ucc_dependencies());
			return (false);
		}
	}
	//
	// Check 3:
	// Ensure that the insertion of new_ucc will not introduce a cycle
	// into our tree.  An invalid ucc will introduce a cycle which is
	// defined to be the condition where the same ucc 'new_ucc' has to be
	// upgraded both before and concurrently with some other ucc 'b'.
	// Needless to say, this condition is impossible to satisfy.  We test
	// that every ucc in the concurrent list is not found in any of
	// new_ucc's before subtrees.
	//
	for (i = 0; i < num_bef_ucc; i++) {
		if (!contained_inside(ucc_hashtable.lookup(new_ucc.
		    upgrade_before[i]), new_ucc.upgrade_concurrently,
		    &invalid_ucc)) {
			continue;
		}
		//
		// new_ucc is invalid as it incorrectly introduces a
		// cycle into our tree.
		//
		RM_DBG(("rm_cb: ucc with name %s cannot be upgraded before ucc "
		    "%s and concurrently with ucc %s (a cycle would be "
		    "created)\n", (const char *)(new_ucc.ucc_name),
		    (const char *)(new_ucc.upgrade_before[i]),
		    (const char *)(invalid_ucc)));
		delete invalid_ucc;
		e.exception(new version_manager::invalid_ucc_dependencies());
		return (false);
	}
	//
	// Check 4:
	// Ensure that all the concurrent ucc's can indeed be upgraded
	// concurrently.  Two ucc's cannot be upgraded concurrently
	// if one is part of another's before subtree.
	//
	for (i = 0; i < num_con_ucc; i++) {
		con_ucc = ucc_hashtable.lookup(new_ucc.upgrade_concurrently[i]);
		ASSERT(con_ucc);
		subtree_num = con_ucc->upgrade_before.length();
		//
		// Check 4a:
		// Analyze all the before subtrees...
		//
		for (j = 0; j < subtree_num; j++) {
			if (!contained_inside(ucc_hashtable.lookup(
			    con_ucc->upgrade_before[j]),
			    new_ucc.upgrade_concurrently, &invalid_ucc)) {
				continue;
			}
			// new_ucc has an invalid list of concurrent uccs.
			RM_DBG(("rm_cb: ucc with name %s has an invalid "
			    "upgrade_concurrently list.  The ucc %s cannot be "
			    "upgraded concurrently with %s.\n",
			    (const char *)(new_ucc.ucc_name),
			    (const char *)(con_ucc->ucc_name), invalid_ucc));
			delete invalid_ucc;
			e.exception(new version_manager::
			    invalid_ucc_dependencies());
			return (false);
		}
		//
		// Check 4b:
		// Analyze the concurrent ucc's.
		// This check can be a little confusing.  The concurrent list
		// of new_ucc needs to be validated.  We check each of the
		// before subtrees of each of the ucc's that are concurrent
		// with new_ucc.
		//
		ASSERT(con_list.empty());
		get_concurrent(con_list, con_ucc);
		con_list.atfirst();
		while ((cand_con = con_list.get_current()) != NULL) {
			con_list.advance();
			//
			// We need to check the before subtree of the cand_con
			// ucc and not the cand_con ucc itself.
			//
			num_bef_con = cand_con->upgrade_before.length();
			for (k = 0; k < num_bef_con; k++) {
				if (!contained_inside(ucc_hashtable.
				    lookup(cand_con->upgrade_before[k]),
				    new_ucc.upgrade_concurrently,
				    &invalid_ucc)) {
					continue;
				}
				// new_ucc has an invalid concurrent list.
				RM_DBG(("rm_cb: ucc with name %s has an "
				    "invalid upgrade_concurrently list.  The "
				    "ucc %s cannot be upgraded concurrently "
				    "with %s.\n",
				    (const char *)(new_ucc.ucc_name),
				    (const char *)(con_ucc->ucc_name),
				    invalid_ucc));
				delete invalid_ucc;
				e.exception(new version_manager::
				    invalid_ucc_dependencies());
				// Empty the SList.
				con_list.erase_list();
				ASSERT(con_list.empty());
				return (false);
			}
		}
		// Empty the SList.
		con_list.erase_list();
		ASSERT(con_list.empty());
	}
	//
	// Finished with all the checks; the ucc is valid.
	// Return true with no exceptions.
	//
	RM_DBG(("rm_cb: Primary node successfully validated ucc with name %s\n",
	    (const char *)(new_ucc.ucc_name)));
	return (true);
}

//
// Returns true if and only if the data fields in both ucc's are equal.
// The relevent data fields are 'upgrade_before', 'upgrade_concurrently' and
// 'freeze'.
//
bool
cb_coordinator::ucc_isequal(version_manager::ucc a, version_manager::ucc b)
{
	char	*a_ptr = NULL;
	char	*b_ptr = NULL;
	uint_t	num_a = 0;
	uint_t	num_b = 0;
	uint_t	i = 0;
	uint_t	j = 0;

	//
	// the data fields to check are :
	//   'upgrade_before'
	//   'upgrade_concurrently'
	//   'freeze'
	//
	// Check every entry in 'upgrade_before' ...
	//
	num_a = a.upgrade_before.length();
	num_b = b.upgrade_before.length();
	if (num_a != num_b)
		return (false);
	else {
		for (i = 0; i < num_a; i++) {
			a_ptr = a.upgrade_before[i];
			for (j = 0; j < num_b; j++) {
				b_ptr = b.upgrade_before[j];
				if (strcmp(a_ptr, b_ptr) == 0)
					break;
			}
			if (j == num_b)
				return (false);
		}
	}
	// Check every entry in 'upgrade_concurrently' ...
	num_a = a.upgrade_concurrently.length();
	num_b = b.upgrade_concurrently.length();
	if (num_a != num_b)
		return (false);
	else {
		for (i = 0; i < num_a; i++) {
			a_ptr = a.upgrade_concurrently[i];
			for (j = 0; j < num_b; j++) {
				b_ptr = b.upgrade_concurrently[j];
				if (strcmp(a_ptr, b_ptr) == 0)
					break;
			}
			if (j == num_b)
				return (false);
		}
	}
	// Check every entry in 'freeze' ...
	num_a = a.freeze.length();
	num_b = b.freeze.length();
	if (num_a != num_b)
		return (false);
	else {
		for (i = 0; i < num_a; i++) {
			a_ptr = a.freeze[i];
			for (j = 0; j < num_b; j++) {
				b_ptr = b.freeze[j];
				if (strcmp(a_ptr, b_ptr) == 0)
					break;
			}
			if (j == num_b)
				return (false);
		}
	}
	// They are identical.
	return (true);
}

//
// Returns true if any of the uccs inside ucc_names are contained inside the
// subtree of 'u'. Ucc's that must be concurrent with 'u' are part of this
// subtree.
//
bool
cb_coordinator::contained_inside(version_manager::ucc *u,
    CORBA::StringSeq ucc_names, char ** found_ucc)
{
	version_manager::ucc *cur, *before, *visited_ucc;
	SList<version_manager::ucc> unvisited;
	SList<version_manager::ucc> visited;
	uint_t	length;
	uint_t	i = 0;
	bool	skip = false;

	RM_DBG(("rm_cb: Searching the subtree starting at ucc %s...\n",
	    (const char *)(u->ucc_name)));

	ASSERT(u);
	// Initialize and start the unvisited with a single ucc 'u'.
	cur = before = visited_ucc = NULL;
	unvisited.append(u);
	length = ucc_names.length();

	// Search the before subtrees of 'u'.
	while ((cur = unvisited.get_current()) != NULL) {
		//
		// Track which ucc's we've already seen so we don't process them
		// more than once.  This is to prevent infinite loops.
		//
		skip = false;
		visited.atfirst();
		while ((visited_ucc = visited.get_current()) != NULL) {
			visited.advance();
			if (strcmp(visited_ucc->ucc_name, cur->ucc_name) == 0) {
				// We've visited this ucc already so skip it.
				skip = true;
				break;
			}
		}
		if (!skip) {
			//
			// Match the concurrent names with this ucc inside u's
			// subtree.
			//
			for (i = 0; i < length; i++) {
				if (strcmp(cur->ucc_name, ucc_names[i]) ==
				    0) {
					//
					// The caller is responsible for
					// freeing this memory.
					//
					RM_DBG(("rm_cb: Found ucc %s inside "
					    "ucc %s's subtree\n", (const char *)
					    cur->ucc_name, (const char *)
					    u->ucc_name));
					*found_ucc = os::strdup(
					    cur->ucc_name);
					// Cleanup both lists.
					unvisited.erase_list();
					visited.erase_list();
					ASSERT(unvisited.empty());
					ASSERT(visited.empty());
					return (true);
				}
			}
			// Add cur's before ucc's to the subtree.
			for (i = 0; i < cur->upgrade_before.length(); i++) {
				before = ucc_hashtable.lookup(
				    cur->upgrade_before[i]);
				ASSERT(before);
				unvisited.append(before);
			}
			// Add any ucc that is concurrent with cur.
			get_concurrent(unvisited, cur);
		}
		// Now reap the top of the unvisited and advance the pointer.
		(void) unvisited.erase(cur);
		visited.append(cur);
		unvisited.atfirst();
	}
	// No ucc's from ucc_names were found in u's before subtree.
	ASSERT(unvisited.empty());
	// Remove the visited list.
	visited.erase_list();
	ASSERT(visited.empty());
	RM_DBG(("rm_cb: Could not find any of the ucc's inside ucc %s's "
	    "subtree\n", (const char *)u->ucc_name));
	return (false);
}

//
// Append to the list all ucc must be upgraded concurrently with 'u'.
// Duplicate entries in list are not allowed to be appended.
// The ucc 'u' will not be included in the list.
//
void
cb_coordinator::get_concurrent(SList <version_manager::ucc> & list,
    version_manager::ucc * u)
{
	version_manager::ucc * cur_ucc = NULL;
	version_manager::ucc * cand_u = NULL;
	SList<version_manager::ucc> unvisited;
	SList<version_manager::ucc> visited;
	uint_t	i, cur_ucc_len;
	bool	check_erase;

	RM_DBG(("rm_cb: Building a list of all ucc's that are concurrent with "
	    "ucc %s\n", (const char *)u->ucc_name));

	ASSERT(u);
	unvisited.append(u);
	unvisited.atfirst();
	while ((cur_ucc = unvisited.get_current()) != NULL) {
		unvisited.advance();
		//
		// Add all the ucc's in the concurrent list, if its not
		// already there.
		//
		cur_ucc_len = cur_ucc->upgrade_concurrently.length();
		for (i = 0; i < cur_ucc_len; i++) {
			cand_u = ucc_hashtable.lookup(
			    cur_ucc->upgrade_concurrently[i]);
			ASSERT(cand_u);
			// Ignore if cand_u is u.
			if (strcmp(cand_u->ucc_name, u->ucc_name) == 0)
				continue;
			// Add it to the list if its not already there.
			if (!found_in_list(list, cand_u))
				list.append(cand_u);
			//
			// Add it to the unvisited list if its not already
			// there are we have not seen it before.
			//
			if (!found_in_list(visited, cand_u) &&
			    !found_in_list(unvisited, cand_u))
				unvisited.append(cand_u);
		}
		//
		// Add any ucc's that has 'u' in its concurrent list, if its not
		// already there.
		//
		string_hashtable_t<version_manager::ucc *>::iterator
		    ui(ucc_hashtable);
		while ((cand_u = ui.get_current()) != NULL) {
			ui.advance();
			// Ignore if cand_u is u.
			if (strcmp(cand_u->ucc_name, u->ucc_name) == 0)
				continue;
			if ((found_ucc_name(cand_u->upgrade_concurrently,
			    cur_ucc->ucc_name))) {
				// Add it to the list if its not already there.
				if (!found_in_list(list, cand_u))
					list.append(cand_u);
				//
				// Add it to the unvisited list if its not
				// already there are we have not seen it before.
				//
				if (!found_in_list(visited, cand_u) &&
				    !found_in_list(unvisited, cand_u))
					unvisited.append(cand_u);
			}
		}
		// Move cand_u from unvisited to visited.
		check_erase = unvisited.erase(cur_ucc);
		ASSERT(check_erase);
		visited.append(cur_ucc);
		// Rewind unvisited to the front.
		unvisited.atfirst();
	}
	// Destroy the temporary unvisited.
	visited.erase_list();
	ASSERT(visited.empty());

	RM_DBG(("rm_cb: Finished building the list\n"));
}

// This returns true if the ucc name is found inside a list of ucc names.
bool
cb_coordinator::found_ucc_name(CORBA::StringSeq ucc_list, char *ucc_name)
{
	uint_t	i = 0;
	uint_t	length = 0;
	length = ucc_list.length();

	for (i = 0; i < length; i++) {
		if (strcmp(ucc_list[i], ucc_name) == 0)
			return (true);
	}
	// Did not find ucc_name so return false.
	return (false);
}

// Returns true if the ucc is found in the list of uccs.
bool
cb_coordinator::found_in_list(SList <version_manager::ucc> & list,
    version_manager::ucc * u)
{
	version_manager::ucc * cand_u = NULL;
	list.atfirst();
	while ((cand_u = list.get_current()) != NULL) {
		list.advance();
		if (strcmp(u->ucc_name, cand_u->ucc_name) == 0)
			return (true);
	}
	return (false);
}

// Destroys the entire ucc_group_list and each ucc_group list element.
void
cb_coordinator::destroy_ucc_group_list()
{
	lck.lock();
	ucc_group_list.dispose();
	lck.unlock();
}
//
// Returns a pointer to the ucc_group that contains the ucc u.
// If u is not contained in any ucc_group, then return NULL.
//
ucc_group *
cb_coordinator::belongs_to_group(version_manager::ucc * u)
{
	ucc_group	* ug;
	ucc_group_list.atfirst();
	while ((ug = ucc_group_list.get_current()) != NULL) {
		ucc_group_list.advance();
		if (ug->contains_ucc(u))
			return (ug);
	}
	return (NULL);
}

//
// Return the next ucc_group to upgrade given as input the last ucc that was
// successfully upgraded on all nodes.  If there are no more ucc_groups to be
// upgraded, then return NULL.
// In addition, freeze and thaw the appropriate HA services.
//
ucc_group *
cb_coordinator::get_next_ucc_group(uint_t last)
{
	ucc_group * cand;
	uint_t	place = 0;

	FAULTPT_VM(FAULTNUM_VM_GET_NEXT_UCC_GROUP_1, FaultFunctions::generic);

	if (last > ucc_group_list.count()) {
		// There are no more ucc groups and nothing to thaw.
		RM_DBG(("rm_cb: No more ucc groups to return, and no HA "
		    "services to thaw.\n"));
		return (NULL);
	}
	//
	// We need to thaw any HA services since we are guaranteed that all
	// work to upgrade the last ucc has completed on all nodes.
	//
	ucc_group_list.atfirst();
	if (last != 0) {
		while ((cand = ucc_group_list.get_current()) != NULL) {
			ucc_group_list.advance();
			if (last == ++place) {

				FAULTPT_VM(FAULTNUM_VM_THAW_SERVICES_1,
					FaultFunctions::generic);

				//
				// We must thaw any HA services because we
				// know that all upgrade work has completed.
				//
				cand->thaw_services();

				FAULTPT_VM(FAULTNUM_VM_THAW_SERVICES_2,
					FaultFunctions::generic);

				break;
			}
		}
	}
	if (last == ucc_group_list.count()) {
		ASSERT(ucc_group_list.get_current() == NULL);
		// There are no more ucc groups since this was the last.
		RM_DBG(("rm_cb: The last ucc_group was processed and all HA "
		    "services were thawed.\n"));
		return (NULL);
	}
	//
	// Since this is not the last group in out list, freeze any HA services
	// and return the next ucc group.
	//
	cand = ucc_group_list.get_current();
	ASSERT(cand != NULL);
	ASSERT(cand->get_size() != 0);

	FAULTPT_VM(FAULTNUM_VM_FREEZE_SERVICES_1, FaultFunctions::generic);

	cand->freeze_services();

	FAULTPT_VM(FAULTNUM_VM_FREEZE_SERVICES_2, FaultFunctions::generic);

	RM_DBG(("rm_cb: Returning the next ucc group to upgrade (last=%d)\n",
	    last + 1));
	return (cand);
}

//
// Construct the ucc_group_list from the ucc_hashtable information.
// When a commit has been invoked and all locks have been grabbed, the
// hash table of validated registered ucc needs to be converted into a
// list of ucc groups.
// This method is called on the cb coordinator when an upgrade is
// committed.  If during the upgrade commit, the cb_coordinator fails over,
// then the new primary must invoke this method to convert its hashtable into
// the list of ucc_groups.  Since this list can be constructed on any
// secondary, we can omit any checkpointing.
// This is also the place where the freeze/thaw information which we'll
// need during get_next_ucc_group() is built.
//
void
cb_coordinator::construct_ucc_group_list()
{
	// Make sure rwlck grabbed before reading ucc_hashtable.
	ASSERT(rwlck.lock_held());

	//
	// If ucc_group_list is already constructed, then to be on the safe
	// side, simply delete it and then recreate it.
	//
	if (ucc_group_list.count() != 0) {
		destroy_ucc_group_list();
	}
	ASSERT(ucc_group_list.count() == 0);

	// Make sure that there is something in the hashtable.
	if (ucc_hashtable.count() == 0) {
		return;
	}

	//
	// Step 1:
	// Create the ucc_group_list by iterating through each of the ucc's
	// in the hashtable and processing each one one-at-a-time.
	//
	convert_hash_to_list();

	//
	// Step 2:
	// Order the list of ucc_groups by analyzing the dependencies.
	// Afterwards, ucc_group_list will be an ascending sorted list of
	// ucc_groups.
	//
	order_ucc_groups();

	//
	// Step 3:
	// Iterate through the ucc_group_list and label each ucc_group object
	// with its freeze/thaw information.
	//
	mark_thaw_ucc_groups();
}

//
// Convert the hashtable of ucc's to a list of ucc_groups.  Each ucc_group
// contains the uccs that are to be upgraded together.  The algorithm
// processes one ucc at-at-time in any order, and creates a list of ucc_groups.
// The algorithm was designed to output the same ucc_groups (although possibly
// in a different order) regardless of the input order of uccs.  The reason
// for this : so failover of the cb_coordinator will yield a list of ucc_groups
// containing the exact information on the chosen secondary.
// The upgrade order of the ucc_group list is determined in order_ucc_list()
// which is called after convert_hash_to_list(); that is why the order of the
// ucc_group list is not important now.
//
void
cb_coordinator::convert_hash_to_list()
{
	version_manager::ucc * u = NULL;
	version_manager::ucc * cand_u = NULL;
	ucc_group * ug = NULL;
	ucc_group * cand_ug = NULL;
	uint_t	i;
	uint_t	num_uccs;
	bool	check_erase;

	ASSERT(rwlck.lock_held());

	RM_DBG(("rm_cb: Starting convert_hash_to_list().\n"));
	string_hashtable_t<version_manager::ucc *>::iterator ui(ucc_hashtable);
	while ((u = ui.get_current()) != NULL) {
		// u is now a valid ucc in the hashtable.
		if ((ug = belongs_to_group(u)) == NULL) {
			//
			// The ucc 'u' does not belong to any ucc_group
			// so create a new one and continue processing.
			//
			ug = new ucc_group(u);
			ASSERT(ug);
			ucc_group_list.append(ug);
		}
		// Process each of the before ucc's of u.
		num_uccs = u->upgrade_before.length();
		for (i = 0; i < num_uccs; i++) {
			// Get the before ucc.
			cand_u = ucc_hashtable.lookup(u->upgrade_before[i]);
			ASSERT(cand_u);
			// Determine if cand_u belongs to any existing group.
			if ((cand_ug = belongs_to_group(cand_u)) == NULL) {
				//
				// It does not belong to any group, so create
				// a new one.
				//
				cand_ug = new ucc_group(cand_u);
				ASSERT(cand_ug);
				ucc_group_list.append(cand_ug);
			}
			ASSERT(ug != cand_ug);
			// Add the before edge from ug to cand_ug.
			ug->atfirst_concurrent();
			cand_ug->atfirst_concurrent();
			ug->add_before_edge(cand_ug);
		}
		// Process each of the concurrent ucc's of u.
		num_uccs = u->upgrade_concurrently.length();
		for (i = 0; i < num_uccs; i++) {
			// Get the concurrent ucc.
			cand_u = ucc_hashtable.lookup(
			    u->upgrade_concurrently[i]);
			ASSERT(cand_u);
			if ((cand_ug = belongs_to_group(cand_u)) == NULL) {
				//
				// The ucc cand_u does not belong to any
				// ucc_group so create a new one and continue
				// processing.
				//
				cand_ug = new ucc_group(cand_u);
				ASSERT(cand_ug);
				ucc_group_list.append(cand_ug);
			}
			//
			// The concurrent ucc belongs to another
			// ucc_group, so combine the information inside
			// ug into cand_ug and prepare to delete ug.
			// If cand_ug and ug point to the same ucc_group,
			// then we've already combine them together so there
			// is nothing to do.
			//
			if (cand_ug != ug) {
				cand_ug->combine_groups(ug);
				check_erase = ucc_group_list.erase(ug);
				ASSERT(check_erase);
				//
				// At this point, there might be some
				// ucc_groups that point to 'ug'.  Since
				// ug will be deleted, we have to check
				// and if needed, update any pointers
				// to cand_ug and then delete ug.
				//
				update_before_ugs(cand_ug, ug);
				delete ug;
				//
				// Reset ug to be the ucc_group that we
				// just combined cand_ug into.
				//
				ug = cand_ug;
			}
		}
		ui.advance();
	}
	RM_DBG(("rm_cb: Finished convert_hash_to_list()\n"));
}

//
// Return an optimized, ascending order list of ucc_groups based on their
// index values.
//
// Determine a valid order to upgrade the ucc_groups.  To determine each
// ucc_group's place in the upgrade order, use a three step process.
// The first step is to push all the root ucc_groups onto a temporary stack.
// The second step is a loop to pop the first ucc_group off the stack and start
// a traversal until the stack is exhausted.  Though a stack is actually
// utilized, logically a post order tree traversal is being done on a series of
// tree structures.  After step 2, we'll know the depth (contained in the index
// data member) of each ucc_group.  The depth is the maximum traversal depth of
// each ucc_group in the logical trees of ucc_groups. The depth
// preserves the Before dependencies and allows us to order the ucc_groups.
// The third step is an optimization to compress ucc_group_list by combining all
// ucc_groups with equal indices into a single ucc_group.  The optimization
// allows us to parallelize the callbacks into the smallest number of ucc_groups
// and yields the most efficient order when an upgrade commit is done.
//
void
cb_coordinator::order_ucc_groups()
{
	ucc_group * ug = NULL;
	ucc_group * cand_ug = NULL;
	bool	check_erase;
	uint_t	cur_depth = 0;
	uint_t	incr = 0;
	IntrList<ucc_group, _SList>	the_stack;
	IntrList<ucc_group, _SList>	finished_list;
	uint_t	num_ug = 0;

	RM_DBG(("rm_cb: Starting order_ucc_groups()\n"));
	num_ug = ucc_group_list.count();
	//
	// Step 1 - Push all root ucc_groups onto stack.
	//
	ucc_group_list.atfirst();
	while ((ug = ucc_group_list.get_current()) != NULL) {
		ucc_group_list.advance();
		if (ug->is_root()) {
			//
			// We should start here.  Take this root ucc_group and
			// place it on our stack.  All roots get placed at the
			// front (top) of the_stack.  Logically, we are
			// pushing the root of our trees onto our stack.
			//
			check_erase = ucc_group_list.erase(ug);
			ASSERT(check_erase);
			the_stack.prepend(ug);
			ug->set_index(0);
		}
	}
	//
	// Step 2 - Loop to pop off a ucc_group and start post order traversal.
	// At the start, the_stack should contain at least one root ucc_group.
	//
	ASSERT(!the_stack.empty());
	the_stack.atfirst();
	while ((ug = the_stack.get_current()) != NULL) {
		// Set the current depth based on the current ucc_group.
		cur_depth = ug->get_index();
		if (ug->is_leaf() || ug->get_visited()) {
			//
			// The post order traversal either found a leaf in our
			// tree or we're finished with all ug's children and are
			// now processing ug itself.  Either way, pop it off
			// and move it to our finished list.
			//
			the_stack.advance();
			check_erase = the_stack.erase(ug);
			ASSERT(check_erase);
			// Put back into the temporary finished_list.
			finished_list.append(ug);
		} else {
			//
			// The post order traversal must process all the
			// children ucc_groups before it processes the parent.
			// Logically, this means push all the children onto
			// the stack. So mark this ucc_group as visited and
			// insert its children on the stack.  Each child must
			// be inserted exactly once into the_stack otherwise an
			// infinite loop would occur.  We determine the child's
			// eligiblity according to which list it resides in.
			//
			ug->set_visited(true);
			ug->atfirst_bef_ug();
			while ((cand_ug = ug->get_next_bef_ug()) != NULL) {
				check_erase = ucc_group_list.erase(cand_ug);
				if (check_erase) {
					//
					// We've never seens this child before
					// so put it into the_stack and mark
					// its index with the current depth.
					//
					the_stack.prepend(cand_ug);
					cur_depth++;
					cand_ug->set_index(cur_depth);
					cur_depth--;
					continue;
				}
				//
				// If the child is not in ucc_group_list, then
				// this ucc_group has been added to the_stack
				// on an earlier iteration.
				//
				cur_depth++;
				check_erase = the_stack.erase(cand_ug);
				if (check_erase) {
					//
					// If the child hasn't been popped off
					// the_stack yet, make it the next
					// ucc_group to be processed so we
					// follow the postorder tree traversal.
					//
					the_stack.prepend(cand_ug);
					//
					// If we found a deeper traversal, then
					// update the index.  Likewise, track
					// the maximum depth of any traversal.
					//
					if (cur_depth > cand_ug->get_index()) {
						cand_ug->set_index(cur_depth);
					}
				} else if (cur_depth > cand_ug->get_index()) {
					//
					// The child has been added to the_stack
					// but then popped off.  It now resides
					// in finished_list.  If we've found a
					// deeper traversal, we need to update
					// its index and update its children's
					// indices.  Likewise, track the
					// maximum depth of any traversal.
					//
					incr = cur_depth - cand_ug->get_index();
					cand_ug->set_index(cur_depth);
					cand_ug->incr_subtree_indices(
					    finished_list, incr);
				}
				cur_depth--;
			}
		}
		the_stack.atfirst();
	}

	//
	//  Step 3 - Optimize the finished list by combining all ucc_groups
	//  objects according to their index value and then sorting them into
	//  ascending order.  This will be the order that the uccs must be
	//  processed in.
	//
	ASSERT(the_stack.empty());
	ASSERT(ucc_group_list.empty());
	ucc_group_list.concat(finished_list);
	ASSERT(finished_list.empty());
	ASSERT(ucc_group_list.count() == num_ug);
	optimize_ug_list(ucc_group_list);
	RM_DBG(("rm_cb: Finished order_ucc_groups()\n"));
}

//
// Optimize the ucc_list by combining all the ucc_groups with equal depth into
// a single ucc_group.  In ucc_list, the ucc_groups will be sorted in the
// proper order of processing.
//
void
cb_coordinator::optimize_ug_list(IntrList<ucc_group, _SList> &uglist)
{
	ucc_group *cur_ug = NULL;
	ucc_group *cand_ug = NULL;
	bool	check_erase;
	uint_t	max_depth = 0;
	uint_t	depth = 0;

	// Only iterate through a non-empty list.
	if (uglist.empty())
		return;
	// Find the list's maximum depth (index).
	uglist.atfirst();
	while ((cur_ug = uglist.get_current()) != NULL) {
		uglist.advance();
		if (cur_ug->get_index() > max_depth) {
			max_depth = cur_ug->get_index();
		}
	}
	// Iterate through the list combining all ucc_groups of the same depth.
	for (depth = 0; depth <= max_depth; depth++) {
		// Find the first ucc_group that has the selected depth.
		uglist.atfirst();
		while ((cur_ug = uglist.get_current()) != NULL) {
			uglist.advance();
			if (cur_ug->get_index() == depth)
				break;
		}
		ASSERT(cur_ug);
		//
		// Iterate through the remainder of the list to locate others
		// with the same depth.
		//
		while ((cand_ug = uglist.get_current()) != NULL) {
			uglist.advance();
			if (cand_ug->get_index() == depth) {
				//
				// We need to combine cand_ug's information
				// into cur_ug and then remove cand_ug from
				// the list.
				//
				check_erase = uglist.erase(cand_ug);
				ASSERT(check_erase);
				cur_ug->combine_groups(cand_ug);
				delete cand_ug;
			}
		}
		//
		// Place cur_ug at the beginning of the list.  This way the
		// uglist will be sorted.  The ucc_group with the maximum index
		// should be the first in the list.
		//
		check_erase = uglist.erase(cur_ug);
		ASSERT(check_erase);
		uglist.prepend(cur_ug);
	}
}

//
// Traverse the ordered list of ucc_groups and determine when the HA service is
// safe to thaw.
//
void
cb_coordinator::mark_thaw_ucc_groups()
{
	uint_t	idx = 1;
	ucc_group	* cur_ug = NULL;

	RM_DBG(("rm_cb: Marking the ordered list of ucc_group objects with "
	    "its thaw information.\n"));
	ucc_group_list.atfirst();
	while ((cur_ug = ucc_group_list.get_current()) != NULL) {
		// Process each ucc_group one-at-a-time.
		mark_thaw(cur_ug, idx);
		ucc_group_list.advance();
		idx++;
	}
}

//
// Determine if any of this ucc_group's services needs to be thawed.  A service
// needs thawing if it does not appear in any latter ucc_group of the same
// tree.  When we encounter another tree (a root ucc_group), we know that we
// need to thaw all services.
//
void
cb_coordinator::mark_thaw(ucc_group * cur_ug, uint_t index)
{
	ucc_group	*next_ug = NULL;
	bool		should_thaw = true;
	bool		reset_cur = true;
	char		*serv = NULL;

	ASSERT(cur_ug);
	ASSERT(index > 0 && index <= ucc_group_list.count());

	string_hashtable_t<char *>::iterator frz_iter(cur_ug->freeze_list);
	while ((serv = frz_iter.get_current()) != NULL) {
		should_thaw = true;
		//
		// If this cur_ug is a root of the tree, then thaw every
		// service.  Otherwise, determine which services, if any,
		// should be thawed.
		//
		if (!cur_ug->is_root()) {
			//
			// The last ucc to freeze this service is responsible
			// for thawing it.
			// So check to see if a ucc in another later ucc_group
			// needs to freeze the same service.  If so, then the
			// service needs to remain frozen inbetween them.  If
			// not, then mark the service to be thawed after this
			// ucc_group is processed.
			//
			reset_cur = get_ug(index + 1);
			ASSERT(reset_cur);
			while ((next_ug = ucc_group_list.get_current())
			    != NULL) {
				ucc_group_list.advance();
				if (next_ug->in_freeze_list(serv)) {
					//
					// We found another ucc_group
					// that freezes the same service
					// in the same tree, so we need
					// to keep this service frozen.
					//
					should_thaw = false;
					break;
				}
				if (next_ug->is_root()) {
					//
					// next_ug is the last of ucc_group in
					// this tree so we don't need to
					// search the next tree.  We'll do the
					// conservative thing and thaw the
					// service here.
					//
					break;
				}
			}
		}
		if (should_thaw)
			cur_ug->add_thaw(serv);
		frz_iter.advance();
	}
	//
	// Reset the ucc_group_list current pointer back to its original
	// position, pointing at cur_ug.
	//
	ucc_group_list.atfirst();
	while ((next_ug = ucc_group_list.get_current()) != NULL) {
		if (cur_ug == next_ug) {
			RM_DBG(("rm_cb: Finished marking the order list of "
			    "ucc_group objects with its thaw information.\n"));
			return;
		}
		ucc_group_list.advance();
	}
	// XXX - Should never get here!!!
	RM_DBG(("rm_cb: Internal problem in cb_coordinator::mark_thaw()\n"));
	ASSERT(false);
}

//
// Manipulate the ucc_group_list so its current pointer is located on the
// i'th ucc_group and return true.  If there is no i'th group, then return
// false.
//
bool
cb_coordinator::get_ug(uint_t i)
{
	uint_t	j;
	if (i > ucc_group_list.count())
		return (false);
	// Rewind to the first ucc_group.
	ucc_group_list.atfirst();
	// Find the i'th ucc_group in the ordered ucc_group_list.
	for (j = 1; j < i; j++)
		ucc_group_list.advance();
	return (true);
}

//
// Check each ucc_group in ucc_group_list to ensure that if it contains a
// pointer to the soon-to-be-deleted ucc_group 'toss', that that pointer
// will be updated as appropriate.
//
void
cb_coordinator::update_before_ugs(ucc_group * keep, ucc_group * toss)
{
	ucc_group * cur_ug = NULL;

	ASSERT(keep);
	ASSERT(toss);
	ASSERT(keep != toss);
	// Start the beginning of the ucc group list ...
	ucc_group_list.atfirst();
	while ((cur_ug = ucc_group_list.get_current()) != NULL) {
		ucc_group_list.advance();
		// Ignore these ucc_groups.
		if (cur_ug == keep || cur_ug == toss)
			continue;
		//
		// Now cur_ug might have one of its entry in 'before_ugs'
		// point to the soon-to-be-deleted ucc_group 'toss'.  If we do
		// remove an edge, then we will add an edge to 'keep' if
		// one does not already exist.
		// This will preserve the correct order of dependencies.
		//
		if (cur_ug->remove_before_edge(toss)) {
			cur_ug->add_before_edge(keep);
		}
	}
}

//
// Select, remove, and return a list of all unneeded uccs after an upgrade
// commit.  If the running version is at its highest version based on the
// current set of nodes in the cluster, then all uccs associated with the vp
// will be removed.
//
// Uccs associated with bootstrap cluster vps are located in local LCAs.
// Uccs associated with non-bootstrap cluster vps are located in the
// cb_coordinator primary and secondaries.
//
// Note that only the uccs associated with non-bootstrap cluster vps are
// actually removed from the hashtable.  The bootstrap associated uccs are
// placed in the remove_list and then passed back to be removed by the lca.
//
void
cb_coordinator::select_and_remove_uccs(
    version_manager::string_seq_t & remove_list)
{
	version_manager::ucc *cur_ucc;
	ucc_group *cur_ug;
	version_manager::vp_version_t rv;
	version_manager::vp_version_t hv;
	versioned_protocol *vpp;
	uint_t	num_rem = remove_list.length();
	version_manager::vp_info_seq *bstrp_cl_vp_list;
	uint_t	i;
	bool	got_rv = false;
	Environment e;

	//
	// Non-bootstrap uccs:
	// Remember that only uccs that are associated with non-bootstrap
	// vps are contained in the cb_coordinator hashtable.  We have to
	// process the bootstrap cluster ones separately.
	// When a ucc is removed from the hashtable, its removal doesn't
	// have to be checkpointed across all cb_coord secondaries.  This is
	// because the subsequent removal off uccs inside the lca will take
	// care of this.
	//
	ucc_group_list.atfirst();
	while ((cur_ug = ucc_group_list.get_current()) != NULL) {
		ucc_group_list.advance();
		cur_ug->atfirst_concurrent();
		while ((cur_ucc = cur_ug->get_next_concurrent()) != NULL) {
			//
			// Find the versioned protocol and its running version.
			// Protect the versions with the vm_admin lock
			// which is really the vm_comm lock.
			//
			vm_comm::the().lock();
			vpp = vm_comm::the_vm_admin_impl().
			    get_vp_by_name(cur_ucc->vp_name);
			ASSERT(vpp);
			got_rv = vpp->get_running_version(rv);
			ASSERT(got_rv);
			//
			// Based on the current cluster membership, get the
			// highest version available for vpp.
			//
			vm_comm::the_vm_coord().
			    get_highest_version(hv, cur_ucc->vp_name);
			vm_comm::the().unlock();
			//
			// Only remove the ucc's that are equal to or less
			// than the highest version.
			//
			if ((rv.major_num < hv.major_num) ||
			    (rv.major_num == hv.major_num &&
			    rv.minor_num < hv.minor_num)) {
				continue;
			}
			ASSERT(rv.major_num == hv.major_num &&
			    rv.minor_num == hv.minor_num);
			// Add the to-be-removed ucc to our list.
			remove_list.length(++num_rem);
			RM_DBG(("rm_cb : Selected the ucc %s (cl vp) for "
			    "removal\n", (const char *)cur_ucc->ucc_name));
			remove_list[num_rem - 1] = os::strdup(cur_ucc->
			    ucc_name);
			// Scrub all traces of this ucc from the hashtable.
			scrub_ucc(cur_ucc);
		}
	}
	//
	// Bootstrap uccs:
	// Now iterate through all the bootstrap cluster vps.
	// All uccs associated with bootstrap cluster vps share the vp's name
	// and do not have any dependencies.
	//
	vm_comm::the_vm_admin_impl().get_vp_list_by_type(bstrp_cl_vp_list,
	    VP_BTSTRP_CLUSTER, e);
	version_manager::vp_info_seq &bcvl = *bstrp_cl_vp_list;
	for (i = 0; i < bcvl.length(); i++) {
		vm_comm::the().lock();
		vpp = vm_comm::the_vm_admin_impl().get_vp_by_name(
		    bcvl[i].vp_name);
		ASSERT(vpp);
		got_rv = vpp->get_running_version(rv);
		ASSERT(got_rv);
		vm_comm::the_vm_coord().get_highest_version(hv,
		    vpp->get_name());
		vm_comm::the().unlock();
		//
		// Only remove the ucc's that are equal to or less than the
		// highest version.
		//
		if ((rv.major_num < hv.major_num) ||
		    (rv.major_num == hv.major_num &&
		    rv.minor_num < hv.minor_num)) {
			continue;
		}
		ASSERT(rv.major_num == hv.major_num &&
		    rv.minor_num == hv.minor_num);
		//
		// Add the to-be-removed ucc to our list.  Remember that its
		// the responsibility of the lca to remove the uccs associated
		// with btstrp cluster vps so its sufficient to add it to our
		// list.  Remember also that bootstrap ucc's name is identical
		// to vp's name.
		//
		remove_list.length(++num_rem);
		RM_DBG(("rm_cb : Selected the ucc %s (bootstrap cl vp) for "
		    "removal\n", (const char *)vpp->get_name()));
		remove_list[num_rem - 1] = os::strdup(vpp->get_name());
	}
}

//
// Remove the named ucc if it exists.
// Called by the internal command query_vm and remove_uccs().
//
void
cb_coordinator::remove_ucc(const char *ucc_name)
{
	version_manager::ucc * ucc_p = NULL;
	ucc_p = ucc_hashtable.lookup(ucc_name);
	if (ucc_p == NULL)
		return;
	scrub_ucc(ucc_p);
}

//
// Remove the uccs specified in the list.  It's called by
// repl_mgr_prov::ckpt_unregister_uccs().  It's the caller's responsibility to
// ensure the correctness of the list of uccs.
void
cb_coordinator::remove_uccs(const version_manager::string_seq_t & ucc_name_seq)
{
	uint_t	i;

	for (i = 0; i < ucc_name_seq.length(); i++) {
		remove_ucc((const char *)ucc_name_seq[i]);
	}
}

//
// Remove ucc 'u' from the hashtable and all dependencies to it on other uccs.
// Called by unregister_ucc().
//
void
cb_coordinator::scrub_ucc(version_manager::ucc *u)
{
	version_manager::ucc *cand_u;
	string_hashtable_t<version_manager::ucc *>::iterator ui(ucc_hashtable);
	uint_t	i;
	uint_t	num_dep;

	// First, remove dependencies to ucc 'u'.
	while ((cand_u = ui.get_current()) != NULL) {
		ui.advance();
		if (cand_u == u) {
			continue;
		}
		// Check all cand_u's Before dependencies.
		num_dep = cand_u->upgrade_before.length();
		for (i = 0; i < num_dep; i++) {
			if (strcmp(cand_u->upgrade_before[i], u->ucc_name)
			    == 0) {
				//
				// We've found a dependency referring to 'u' and
				// must remove it.  If we are not at the last
				// element in the array, then simply copy the
				// last element into the slot occupied by 'u'.
				//
				if (i != num_dep - 1) {
					cand_u->upgrade_before[i] = os::strdup(
					    cand_u->upgrade_before[
					    num_dep - 1]);
				}
				cand_u->upgrade_before.length(--num_dep);
			}
		}
		// Check all cand_u's Concurrent dependencies.
		num_dep = cand_u->upgrade_concurrently.length();
		for (i = 0; i < num_dep; i++) {
			if (strcmp(cand_u->upgrade_concurrently[i], u->ucc_name)
			    == 0) {
				//
				// We've found a dependency referring to 'u'.
				// We must remove it.  If we are not at the last
				// element in the array, then simply copy the
				// last element into the slot occupied by 'u'.
				//
				if (i != num_dep - 1) {
					cand_u->upgrade_concurrently[i] = os::
					    strdup(cand_u->upgrade_concurrently[
					    num_dep - 1]);
				}
				cand_u->upgrade_concurrently.
				    length(--num_dep);
			}
		}
	}
	RM_DBG(("rm_cb: Scrubbing ucc %s (cl vp) from cb_coord hashtable.\n",
	    (const char *)u->ucc_name));
	// Second, remove the ucc itself and free its memory.
	(void) ucc_hashtable.remove(u->ucc_name);
}

// Class cb_coord_control_impl methods.

// Primary constructor.
cb_coord_control_impl::cb_coord_control_impl(
    repl_mgr_prov_impl *rm_prov_inp,
    const version_manager::vm_lca_ptr lca_in_p,
    cb_coordinator *cbc_inp) :
    mc_replica_of<replica_int::cb_coord_control>(rm_prov_inp),
    lca_v(version_manager::vm_lca::_duplicate(lca_in_p)),
    callback_in_progress(0)
{
	RM_DBG(("rm_cb: Created cb_coord_control_impl '%p' "
	    "on RM primary\n", this));

	cbcp = cbc_inp;
}

// Secondary constructor.
cb_coord_control_impl::cb_coord_control_impl(
    replica_int::cb_coord_control_ptr cbprov_in_p,
    const version_manager::vm_lca_ptr lca_in_p,
    cb_coordinator *cbc_inp) :
    mc_replica_of<replica_int::cb_coord_control>(cbprov_in_p),
    lca_v(version_manager::vm_lca::_duplicate(lca_in_p)),
    callback_in_progress(0)
{
	RM_DBG(("rm_cb: Created cb_coord_control_impl '%p' "
	    "on RM secondary\n", this));

	cbcp = cbc_inp;
}

cb_coord_control_impl::~cb_coord_control_impl()
{
	cbcp = NULL;
}

void
#ifdef DEBUG
cb_coord_control_impl::_unreferenced(unref_t cookie)
#else
cb_coord_control_impl::_unreferenced(unref_t)
#endif
{
	RM_DBG(("rm_cb: unreferenced called for cb_coord_control_impl "
	    "'%p'\n", this));

#ifdef DEBUG
	ASSERT(_last_unref(cookie));
#endif
	//
	// Remove us from the cb_coordinator object.
	//
	cbcp->remove_cb_coord_control(this);

	delete this;
}

//
// IDL method for ucc registration. Called from LCAs.
//
void
cb_coord_control_impl::register_ucc(const version_manager::ucc &new_ucc,
    Environment &e)
{
	FAULTPT_VM(FAULTNUM_VM_CB_COORD_REGISTER_UCC_1,
		FaultFunctions::generic);

	//
	// Pass the reqest to cb_coordinator
	//
	// cb_coordinator::register_ucc is idempotent because it simply ignores
	// a registration request of a ucc which has exact the same content
	// as an existing one. No exception will be thrown either. For this
	// reason, we don't have to make this a mini-transaction.
	//
	cbcp->register_ucc(new_ucc, e);

	FAULTPT_VM(FAULTNUM_VM_CB_COORD_REGISTER_UCC_2,
		FaultFunctions::generic);
}

//
// Ask the associated lca to perform callbacks on the listed uccs.
// Return true if the new callbacks are acutally being processed on the lca,
// false otherwise.
//
bool
cb_coord_control_impl::do_callback_task(
    const version_manager::string_seq_t &ucc_seq,
    const version_manager::vp_version_seq &new_version_seq,
    const uint32_t task_id)
{
	Environment	e;
	bool		retval = true;

	//
	// We hold the write lock here before calling do_callback_task
	// to make sure do_callback_task() does not
	// happen at the same time as callback_task_done().
	// Note: If comm failure is returned, we could still get a call to
	// callback_task_done which might be blocked waiting for us to
	// release the lock.
	//
	rwlck.wrlock();
	retval = lca_v->do_callback_task(ucc_seq, new_version_seq, task_id, e);
	if (e.exception()) {
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
		e.clear();
		retval = false;
	}

	ASSERT(callback_in_progress == 0);
	if (retval)
		callback_in_progress = task_id;
	rwlck.unlock();

	return (retval);
}

//
// Called from the associated lca after it has finished its most recent
// callback task.
//
void
cb_coord_control_impl::callback_task_done(uint32_t callback_task_id,
    Environment &)
{
	rwlck.wrlock();
	if (callback_in_progress == callback_task_id) {
		//
		// Inform the cb_coordinator that our associated lca has
		// completed its callback tasks. There's no need to checkpoint
		// this information as the new RM primary will talk to all lcas
		// to collect data to do the recovery.
		//
		cbcp->callback_task_done_on_one_lca(callback_task_id);
		callback_in_progress = 0;
	} else {
		//
		// This is a new RM which has not recovered from previous
		// upgrade_commit() call on the old RM. Discard this
		// info.
		//
		RM_DBG(("rm_cb: callback_task_done(%u) ignored\n",
		    callback_task_id));
	}
	rwlck.unlock();
}

// _Generic_method
void
cb_coord_control_impl::_generic_method(CORBA::octet_seq_t &data,
    CORBA::object_seq_t &objs, Environment &e)
{
	RM_DBG(("rm_cb: %p in cb_coord_control::_generic_method.\n", this));

	if (data.length() == 0) {
		//
		// The data field specifies the actual action, return exception
		// it's empty.
		//
		e.system_exception(CORBA::VERSION(1, CORBA::COMPLETED_NO));
		return;
	}

	if (os::strncmp((const char *)data.buffer(),
	    "upgrade_cb_coord_control_ref", (ulong_t)29) == 0) {

		//
		// This is a request to upgrade_cb_coord_control_ref().
		//

		//
		// Reconstruct the new idl version. The data is constructed as
		// "<function_name><first 8 bits of new_idl_version><second
		// 8 bits of new_idl_version>".
		//
		uint16_t new_idl_version = 0;
		uint8_t  *buffer = data.buffer();
		new_idl_version = ((uint8_t)buffer[29] << 8) +
		    (uint8_t)buffer[30];
		ASSERT(new_idl_version > 0);

		ASSERT(objs.length() == 1);

		RM_DBG(("rm_cb: processing upgrade_cb_coord_control_ref with "
		    "requested new idl version %d\n", new_idl_version));

		version_manager::vp_version_t rv;
		rv.major_num = 1; // XXX Update this when major_num changes
		rv.minor_num = new_idl_version;

		// Return a reference with the specified IDL version.
		objs[0] = get_objref(
		    replica_int::cb_coord_control::_get_type_info(
		    RM_ADMIN_VERS(rv)));
	} else {
		e.system_exception(CORBA::VERSION(1, CORBA::COMPLETED_NO));
	}
}

// Upgrade commit task class

// Constructor
upgrade_commit_task::upgrade_commit_task(bool is_retry):
    retry(is_retry),
    vps_locked(false)
{
}

// Destructor
upgrade_commit_task::~upgrade_commit_task()
{
}

//
// Deferred task that performs the actual upgrade_commit(). An upgrade commit
// can contain multiple upgrades. This is necessary because some vps can
// only be upgraded by one version at a time. So if a vp is running in v1 mode
// but can potentially run in v3, we have to do two upgrades, 1->2, then 2->3.
//
// This is executed on RM primary only. Since RM doesn't allow switchover,
// there is not issue with killing this thread during switchover.
//
void
upgrade_commit_task::execute()
{
	RM_DBG(("rm_cb: in callback_task::execute()\n"));

	Environment			env;
	cb_coord_control_impl		*cb_cntl = NULL;
	version_manager::vm_lca_var	lca_v;
	uint32_t			last_task_id;
	uint32_t			cur_callback_task_id = 0;
	replica_int::rm_var	rm_v = repl_mgr_impl::get_rm()->get_objref();
	cb_coordinator	*cbcp = repl_mgr_impl::get_rm()->get_cb_coordinator();
	SList<cb_coord_control_impl>::ListIterator	lca_iter;
	ucc_group			*ug = NULL;
	version_manager::ucc		*ucc = NULL;
	uint_t				size, i;
	version_manager::string_seq_t   ucc_name_seq;
	version_manager::vp_version_seq new_version_seq;
	cmm::seqnum_t			start_upgrade_seq;
	bool				has_any_rv_increased = false;

	//
	// No need to grab lock before reading upgrade_commit_progress here,
	// This is because we are the only thread that changes the value of
	// upgrade_commit_progress.
	//
	switch (cbcp->upgrade_commit_progress) {
	case version_manager::CALCULATE_NEW_RVS_UVS:
AGAIN:
		//
		// This step sends a request to the version manager to
		// ask it to calculate and commit new running versions for all
		// btstrp cl vps and calculate new upgrade versions for all
		// non-btstrp cl vps.
		//
		if (retry) {
			retry = false;

			FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_1_A,
				FaultFunctions::generic);
		} else {
			rm_v->set_upgrade_commit_progress(
			    version_manager::CALCULATE_NEW_RVS_UVS, env);
			    CL_PANIC(!env.exception());
		}

		if (!vps_locked) {
			//
			// Lock all the vps. This is needed only at the very
			// beginning of an upgrade commit. In retry case,
			// it's very hard for us to figure out whether we
			// have already locked the vps or not. So it's possible
			// we do an duplicate vps locking here. But it's ok.
			//
			cbcp->iterate_over(&version_manager::vm_lca::
			    lock_vps_for_upgrade_commit,
			    "lock_vps_for_upgrade_commit");
			vps_locked = true;
		}

		//
		// This will trigger a cmm reconfiguration to calculate new
		// rvs for all btstrp cl vps to next possible versions and
		// perform upgrade callback on the replica manager and other
		// bootstrap cluster vps if necessary. The cmm reconfiguration
		// will also calculate new uvs for all non-btstrp cl vps.
		//
		cbcp->lck.lock();
		cbcp->last_upgrade_seq = vm_comm::the().start_upgrade_commit();

		// Record the starting upgrade_seq.
		start_upgrade_seq = cbcp->last_upgrade_seq;

		FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_1_B,
			FaultFunctions::generic);

		do {
			// Wait for the new rvs and uvs to be calculated.
			cbcp->cv.wait(&cbcp->lck);
		} while (start_upgrade_seq == cbcp->last_upgrade_seq);

		FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_1_C,
			FaultFunctions::generic);

		cbcp->lck.unlock();
		// FALLTHROUGH

	case version_manager::COMMIT_UVS:
		// This step tells version manager to commit the current uvs.

		if (retry) {
			//
			// If we are retrying this step, we must have locked vps
			// on the old primary.
			//
			vps_locked = true;
			retry = false;
			//
			// Since this is a retry, there is no way to know
			// whether the original cb_coord primary died after
			// setting the flag to false, but before completing the
			// last cmm reconfiguration.  So set the flag to true
			// to make sure that we always do a final
			// cmm reconfiguration.
			//
			has_any_rv_increased = true;

			FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_2_A,
				FaultFunctions::generic);
		} else {
			CL_PANIC(vps_locked);
			rm_v->set_upgrade_commit_progress(
			    version_manager::COMMIT_UVS, env);
			CL_PANIC(!env.exception());
			//
			// If the vm_admin_impl detects that any running version
			// was increased, then we'll set the
			// has_any_rv_increased to true.  Thus we know that we
			// might have another sequential upgrade possible.
			//
			if (vm_comm::the_vm_admin_impl().get_change_flag()) {
				has_any_rv_increased = true;
			}
		}

		FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_2_B,
			FaultFunctions::generic);

		//
		// Invoke finish_upgrade_commit on all version managers
		// to commit the upgrade version.
		//
		cbcp->iterate_over(&version_manager::vm_lca::
		    invoke_finish_upgrade_commit_on_vm,
		    "invoke_finish_upgrade_commit_on_vm");

		FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_2_C,
			FaultFunctions::generic);

		// FALLTHROUGH

	case version_manager::DO_CL_CALLBACKS:
		//
		// This step sends callbacks on all LCAS for non-btstrp
		// cluster vps.
		//

		if (retry) {
			// This is a retry on the new RM primary.

			//
			// If we are retrying this step, we must have locked vps
			// on the old primary.
			//
			vps_locked = true;
			//
			// Since this is a retry, there is no way to know
			// whether the original cb_coord primary died after
			// setting the flag to false, but before completing the
			// last cmm reconfiguration.  So set the flag to true
			// to make sure that we always do a final
			// cmm reconfiguration.
			//
			has_any_rv_increased = true;

			//
			// Need to find out the callback progress done by the
			// old RM primary and resume from that point.
			//

			//
			// We need to hold on to the lock here to
			// prevent new lca from being added while we are
			// doing recovery.
			//
			cbcp->rwlck.rdlock();

			// Retrieve last callback task id from all lcas.
			lca_iter.reinit(cbcp->cb_coord_control_list);

			for (cb_cntl = NULL;
			    (cb_cntl = lca_iter.get_current()) != NULL;
			    lca_iter.advance()) {
				lca_v = cb_cntl->get_lca();
				lca_v->get_last_cl_callback_task_id(
				    last_task_id, env);
				if (env.exception()) {
					// This lca node has died, ignore it.
					ASSERT(CORBA::COMM_FAILURE::_exnarrow(
					    env.exception()));
					env.clear();
				} else if ((last_task_id - 1) >
				    cur_callback_task_id) {
					cur_callback_task_id = last_task_id - 1;
				}

				FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_3_A,
					FaultFunctions::generic);
			}
			cbcp->rwlck.unlock();

			RM_DBG(("rm_cb: cur_callback_task_id is set to %d\n",
			    cur_callback_task_id));
			retry = false;
		} else {
			ASSERT(vps_locked);
			rm_v->set_upgrade_commit_progress(
			    version_manager::DO_CL_CALLBACKS, env);
			CL_PANIC(!env.exception());
		}
		//
		// Perform the actual callbacks...
		//
		while (((ug = cbcp->get_next_ucc_group(cur_callback_task_id++))
		    != NULL)) {
			// ...but only if at least one running
			// version has been increased or if this is a retry step
			// caused by a VM primary death.  Else, continue
			// getting the next ucc group by completing the while-
			// loop.  Exhausting the while loop allows the freeze
			// and thawing of HA services to complete.
			// See RFE 4917702 for more details.
			//
			if (!has_any_rv_increased) {
				continue;
			}
			// Get the total number of uccs in this group.
			size = ug->get_size();
			CL_PANIC(size > 0);
			ucc_name_seq.length(size);
			new_version_seq.length(size);

			//
			// Fill up ucc_name_seq and their associated
			// new_version_seq.
			//
			ug->atfirst_concurrent();
			for (i = 0; i < size; i++) {
				ucc = ug->get_next_con_ucc(new_version_seq[i]);
				CL_PANIC(ucc != NULL);
				ucc_name_seq[i] = os::strdup(ucc->ucc_name);
				RM_DBG(("rm_cb: ucc_name_seq %d is %s, version "
				    "is %d.%d, callback id is %d\n", i,
				    (char *)ucc->ucc_name,
				    new_version_seq[i].major_num,
				    new_version_seq[i].minor_num,
				    cur_callback_task_id));
			}

			cbcp->rwlck.rdlock();
			lca_iter.reinit(cbcp->cb_coord_control_list);
			// Invoke callback on all lcas.
			for (; (cb_cntl = lca_iter.get_current()) != NULL;
			    lca_iter.advance()) {
				//
				// The sequence here is supposed to be as
				// follows:
				// 1. invoke do_callback_task();
				// 2. num_lcas_doing_callback++ if
				//    do_callback_task() return 1 which mean
				//    the LCA is processing this callback.
				// 3. After the LCA finishs processing the
				//    callback, it sends a notification back
				//    which enventually calls
				//    num_lcas_doing_callback--.
				//
				// If we use the above sequence, we want to
				// make sure that step 3 does not happen before
				// step 2 by holding on to the lock to
				// num_lcas_doing_callback all the way through
				// from step1 to step3. However, the lock is
				// a mutex lock while do_callback_task() is
				// an IDL invocation. It is not desirable to
				// hold on to a mutex lock while doing an IDL
				// invocation. To get around this problem,
				// we first increase num_lcas_doing_callback
				// and decrease it if do_callback_task() returns
				// 0 which means the LCA does not need to
				// process this callback task at all.
				//
				cbcp->lck.lock();
				cbcp->num_lcas_doing_callback++;
				cbcp->lck.unlock();

				if (!cb_cntl->do_callback_task(ucc_name_seq,
				    new_version_seq, cur_callback_task_id)) {
					//
					// The LCA does not need to process
					// this callback.
					//
					cbcp->lck.lock();
					cbcp->num_lcas_doing_callback--;
					cbcp->lck.unlock();
				}

				FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_3_B,
					FaultFunctions::generic);
			}
			cbcp->rwlck.unlock();

			cbcp->lck.lock();
			while (cbcp->num_lcas_doing_callback > 0) {
				//
				// At least one lca is processing callbacks.
				// Wait for all lcas to finish processing their
				// callback task.
				//
				RM_DBG(("rm_cb: wait for %d lcas to finish "
				    "processing callbacks task id %d\n",
				    cbcp->num_lcas_doing_callback,
				    cur_callback_task_id));
				cbcp->cv.wait(&cbcp->lck);

				FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_3_C,
					FaultFunctions::generic);

			} // XXX Else, should we unregister this ucc?
			cbcp->lck.unlock();

			FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_3_D,
				FaultFunctions::generic);
		}

		//
		// We must check for sequential upgrades at this point.
		// If any running version has changed between AGAIN and here,
		// then all rv and uv needs to be recomputed as there could be
		// another upgrade possible.  We invoke that computation by
		// forcing a cmm reconfiguration.
		//
		if (has_any_rv_increased) {
			cur_callback_task_id = 0;
			has_any_rv_increased = false;
			goto AGAIN;
		}
		// FALLTHROUGH

	case version_manager::CLEANUP:	//lint !e616
		//
		// This step unlocks vps, cleans up UCCS that do not
		// exit in any lcas, and destroys the ucc_group list.
		// A UCC can be unregistered from LCA
		// as soon as its callback task is done and no longer needed
		// by the service writer. So here we need to talk to all LCAs
		// to get their lists of register uccs and remove those do not
		// exist in any LCAs from cb_coordinator's ucc list.
		//

		if (retry) {
			//
			// If we are retrying this step, we may or may not
			// have unlocked vps on all nodes. But it's ok to
			// assume that vps are locked and retry everything as
			// function unlock_vps_after_upgrade_commit() is
			// idempotent.
			//
			vps_locked = true;
			retry = false;
		} else {
			CL_PANIC(vps_locked);
			rm_v->set_upgrade_commit_progress(
			    version_manager::CLEANUP, env);
			CL_PANIC(!env.exception());
		}

		//
		// Unregister UCCs that are associated with VPs that
		// have been upgraded to the highest version.
		//
		rm_v->unregister_uccs(env);
		CL_PANIC(!env.exception());

		FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_4_A,
			FaultFunctions::generic);

		// Destroy the ucc_group list
		cbcp->destroy_ucc_group_list();

		FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_4_B,
			FaultFunctions::generic);

		// Unlock all vps.
		cbcp->iterate_over(&version_manager::vm_lca::
		    unlock_vps_after_upgrade_commit,
		    "unlock_vps_after_upgrade_commit");
		vps_locked = false;

	case version_manager::NOT_STARTED:	//lint !e616
		if (retry) {
			retry = false;
		} else {
			FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_0_A,
				FaultFunctions::generic);

			//
			// Set the progress to NOT_STARTED to finish the current
			// upgrade commit.
			//
			rm_v->set_upgrade_commit_progress(
			    version_manager::NOT_STARTED, env);
			CL_PANIC(!env.exception());

			FAULTPT_VM(FAULTNUM_VM_UPGD_COMMIT_TASK_0_B,
				FaultFunctions::generic);
		}
		break;

	default:
		CL_PANIC(0);
	}

	CL_PANIC(!retry && !vps_locked);
}
