/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_state_mach.cc	1.94	08/05/20 SMI"

//
// rm_state_mach.cc implements the (non-Corba) rm_state_machine class.
//

#include <repl/repl_mgr/repl_mgr_debug.h>
#include <repl/repl_mgr/rm_repl_service.h>
#include <repl/repl_mgr/rm_state_mach.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <orb/infrastructure/orb.h>
#include <orb/member/cmm_callback_impl.h>
#include <repl/repl_mgr/rm_repl_service_in.h>
#include <repl/repl_mgr/rm_state_mach_in.h>
#include <repl/rma/rma.h>

#include <orb/fault/fault_injection.h>

// Mutex and condvar for state changes
os::mutex_t rm_state_machine::state_mutex;
os::condvar_t rm_state_machine::external_cv;

rm_state_machine::rm_state_machine(rm_repl_service *const svc,
    const sm_service_state &init_state) :
	current_state(init_state), cur_primary_p(NULL), s(svc),
	first_time_up(true), is_stable_state(false)
{
	ASSERT(s);

	initialize();
} // ctor

void
rm_state_machine::initialize()
{
	rm_handlers[SERVICE_REGISTERED]	= &rm_state_machine::service_registered;
	rm_handlers[SERVICE_RECOVERING]	= &rm_state_machine::service_recovering;
	rm_handlers[SERVICE_UNINIT]	= &rm_state_machine::service_uninit;
	rm_handlers[SERVICE_UP]		= &rm_state_machine::service_up;
	rm_handlers[REMOVE_SECONDARIES]	= &rm_state_machine::remove_secondaries;
	rm_handlers[PRIMARY_FROZEN]	= &rm_state_machine::primary_frozen;
	rm_handlers[SERVICE_FROZEN_CLEAN] =
	    &rm_state_machine::service_frozen_clean;
	rm_handlers[NEW_SECONDARY]	= &rm_state_machine::new_secondary;
	rm_handlers[PRIMARY_DEACTIVATED] =
	    &rm_state_machine::primary_deactivated;
	rm_handlers[DISCONNECTED]	= &rm_state_machine::disconnected;
	rm_handlers[NO_PRIMARY]		= &rm_state_machine::no_primary;
	rm_handlers[PRIMARY_SELECTED]	= &rm_state_machine::primary_selected;
	rm_handlers[PRIMARY_PREPARED]	= &rm_state_machine::primary_prepared;
	rm_handlers[RECONNECTED]	= &rm_state_machine::reconnected;
	rm_handlers[SERVICE_FROZEN_DIRTY] =
	    &rm_state_machine::service_frozen_dirty;
	rm_handlers[CLIENTS_FROZEN]	= &rm_state_machine::clients_frozen;
	rm_handlers[SERVICE_SUICIDE]	= &rm_state_machine::service_suicide;
	rm_handlers[PRIMARY_DEAD]	= &rm_state_machine::primary_dead;
	rm_handlers[SERVICE_DOWN]	= &rm_state_machine::service_down;
	rm_handlers[FROZEN_FOR_UPGRADE]	= &rm_state_machine::frozen_for_upgrade;
	rm_handlers[EXIT_STATE]		= &rm_state_machine::exit_state;
} // initialize

#ifdef RM_DBGBUF
const char *
rm_state_machine::state_name(sm_service_state state)
{
	switch (state) {
	case SERVICE_REGISTERED:
		return ("service_registered");
	case SERVICE_RECOVERING:
		return ("service_recovering");
	case SERVICE_UNINIT:
		return ("service_uninit");
	case SERVICE_UP:
		return ("service_up");
	case REMOVE_SECONDARIES:
		return ("remove_secondaries");
	case PRIMARY_FROZEN:
		return ("primary_frozen");
	case SERVICE_FROZEN_CLEAN:
		return ("service_frozen_clean");
	case NEW_SECONDARY:
		return ("new_secondary");
	case PRIMARY_DEACTIVATED:
		return ("primary_deactivated");
	case DISCONNECTED:
		return ("disconnected");
	case NO_PRIMARY:
		return ("no_primary");
	case PRIMARY_SELECTED:
		return ("primary_selected");
	case PRIMARY_PREPARED:
		return ("primary_prepared");
	case RECONNECTED:
		return ("reconnected");
	case SERVICE_FROZEN_DIRTY:
		return ("service_frozen_dirty");
	case CLIENTS_FROZEN:
		return ("clients_frozen");
	case SERVICE_SUICIDE:
		return ("service_suicide");
	case PRIMARY_DEAD:
		return ("primary_dead");
	case SERVICE_DOWN:
		return ("service_down");
	case FROZEN_FOR_UPGRADE:
		return ("frozen_for_upgrade");
	case EXIT_STATE:
		return ("exit_state");
	default:
		CL_PANIC(0);
		return ("");
	}
}
#endif // RM_DBGBUF

void
rm_state_machine::execute()
{
	sm_service_state state = current_state;

	while (state != EXIT_STATE) {
		CL_PANIC(state_lock_held());

		RM_DBG(("rm_sm %s: state `%s(%x)', current primary \"%s\"\n",
		    s->id(), state_name(state), state,
		    (cur_primary_p == NULL) ? "No primary" :
			cur_primary_p->get_rp_desc()));

		// Store the current state for other threads to examine.
		current_state = state;

		// Process the current state. This returns the next desired
		// state.
		const rm_handler_t	state_handler = rm_handlers[state];

		state = (this->*state_handler)();

		if (state != current_state) {
			broadcast_state_change();
		}
	}

	RM_DBG(("rm_sm %s: state `%s(%x)'\n",
	    s->id(), state_name(state), state));
} // execute

// ====================


//
// service_registered()
//
// We notify the RMAs of our existence and move to SERVICE_UNINIT.
//
// Possible returns are
//	SERVICE_UNINIT
//
rm_state_machine::sm_service_state
rm_state_machine::service_registered()
{
	// need to special handle the case for add_service since its
	// parameters are different. We can make iterate_over more general
	// and not have any requiremnt on the type of functions it calls, but
	// it's probably not worth the effort if add_service is the only
	// exception.
	s->iterate_services("add_service");
	return (SERVICE_UNINIT);
}


//
// service_recovering()
//
// We figure out which state the old RM was in and return
// that state (continue reconfiguration if one was in
// progress, or block in a stable state)
//
rm_state_machine::sm_service_state
rm_state_machine::service_recovering()
{
	sm_service_state return_state = SERVICE_UNINIT;

	RM_DBG(("rm_sm: %s recovery entering service_recovering state\n",
	    s->id()));

	//
	// Get each provider's current state.  Note that this list can be
	// shorter than the list of providers for the service:  if we get a
	// COMM FAILURE when querying the provider for its state, we mark it
	// down but don't add it to the prov_last_state_list.
	// The query may discover a service is being shutdown and may mark the
	// service as deleted. That's why we do it before the check for
	// is_deleted() below.
	//
	SList<rm_repl_service::provider_recovery_data> prov_last_state_list;
	query_providers(&prov_last_state_list);

	//
	// Call become_spare() on all spares, to avoid a provider having a
	// partial state dump.
	//
	spare_all_spares();

	if (s->is_deleted()) {
		//
		// We didn't query RMA's services that were marked as deleted
		// because of checkpoints.  However, we did query those that
		// were in the middle of shutdown.
		// We go to SERVICE_UP if we found a primary provider. It will
		// quickly go to SERVICE_DOWN after it finds that it has been
		// shutdown.
		//
		return_state = (cur_primary_p == NULL) ? SERVICE_DOWN :
		    SERVICE_UP;
	} else {
		//
		// If there was a switchover in progress, then reinstate the
		// state machine flag.
		//
		if (s->switchover_info != NULL) {
			//
			// Found switchover_info object which only gets created
			// in the bottom service during switchover. So we know
			// that there was a switchover in progress on the old
			// RM primary.
			//
			CL_PANIC(s->deps_down.empty() &&
			    s->is_doing_switchover());

			const char *primary_from =
			    s->switchover_info->get_switchover_from();
			const char *primary_to =
			    s->switchover_info->get_switchover_to();

			ASSERT(primary_from != NULL);

			RM_DBG(("rm_sm: %s recovery checking for "
			    "switchovers in progress. cur_primary_p %p, "
			    "source is %s, and dest is %s\n", s->id(),
			    cur_primary_p, primary_from,
			    ((primary_to == NULL) ? "NULL" : primary_to)));

			//
			// Reinstating the PRIMARY -> SEC goal on the primary
			// if it is still the original one.
			//
			rm_repl_service::prov_reg_impl	*provp = NULL;

			if ((cur_primary_p != NULL) &&
			    (os::strcmp(cur_primary_p->get_rp_desc(),
			    primary_from) == 0)) {
				//
				// The prov_reg_impl list below does not
				// contain current primary provider. see
				// prog_reg_impl::get_deps_up_tree().
				//
				RM_DBG(("rm_sm %s: posting switch req "
				    "to %s  prov_desc %s\n", s->id(),
				    cur_primary_p->get_service()->id(),
				    cur_primary_p->get_rp_desc()));
				cur_primary_p->set_sm_prov_state(
				    rm_repl_service::prov_reg_impl::
				    SM_PRIMARY_TO_MAKE_SECONDARY);
				//
				// Mark state change request in the
				// providers.  Depdendent services'
				// primary will be on the same node as
				// ours. So get list provs depending
				// on our current primary and mark
				// them for state change.
				//
				prov_reg_list *prov_listp =
					cur_primary_p->get_deps_up_tree();

				SList<rm_repl_service::prov_reg_impl>::
				    ListIterator iter(prov_listp);

				while ((provp = iter.get_current()) != NULL) {
					iter.advance();

					RM_DBG(("rm_sm %s: posting switch req "
					    "to %s  prov_desc %s\n", s->id(),
					    provp->get_service()->id(),
					    provp->get_rp_desc()));

					provp->set_sm_prov_state(
					    rm_repl_service::prov_reg_impl::
					    SM_PRIMARY_TO_MAKE_SECONDARY);
					//
					// The provider could be marked down
					// and waiting to be cleaned up
					// soon. We can overwrite the FAILED
					// state because any invocation on
					// that provider will return
					// COMM_FAILURE and the provider will
					// be marked for cleanup.
				}
				delete prov_listp;
			}
			//
			// If we were promoting a secondary, and that secondary
			// is still an valid secondary, reinstate the
			// SEC -> PRI goal.
			//
			if ((primary_to != NULL) &&
			    ((provp = s->get_repl_prov(primary_to)) != NULL) &&
			    provp->is_valid_sec()) {
				//
				// If we were promoting a secondary,
				// reinstate the goal.
				//
				provp->set_sm_prov_state(rm_repl_service::
				    prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY);
			}
		} // doing switchover

		return_state = get_next_state(&prov_last_state_list);
	}
	//
	// Delete prov_last_state_list elements since we are done using them
	// in to obtain next state in get_next_state().
	//
	prov_last_state_list.dispose();
	//
	// Recovery data is added to get_states_list in copy_service_data().
	// This can be deleted here.
	//
	get_states_list.dispose();

	return (return_state);
}

//
// service_uninit()
//
// We wait until we need to do one of the following:
//	-- add a provider that can run
//	-- shutdown
//
// Possible returns are
//	NO_PRIMARY
//	SERVICE_DOWN
//
rm_state_machine::sm_service_state
rm_state_machine::service_uninit()
{
	sm_service_state return_state = SERVICE_UNINIT;

	cleanup_providers();
	if (check_other_states(&s->deps_down, SYNC_COMMITTED_PRIMARY) &&
	    (select_new_primary() != NULL)) {
		return_state = NO_PRIMARY;
	} else if (service_shutdown()) {
		return_state = SERVICE_DOWN;
	} else if (!s->deps_down.empty()) {
		//
		// This service depends on some other services. If ANY not
		// all of them are down, this service has to go down too.
		//
		rm_repl_service *deps_s;
		SList<rm_repl_service>::ListIterator iter(s->deps_down);
		while ((deps_s = iter.get_current()) != NULL) {
			iter.advance();
			if (deps_s->smp->check_state(SYNC_SERVICE_DOWN)) {
				//
				// The service is down as one of the services
				// it depends on is down. Go to service_down
				// state.
				//
				return_state = SERVICE_DOWN;

				break;
			}
		}
	}
	if (return_state == SERVICE_UNINIT) {
		// Nothing to do.
		// Clear out any request fields that didn't trigger any work.
		clear_sm_prov_states();
		// Hang out waiting for work.
		block_in_stable_state();
	}

	return (return_state);
}

//
// service_up()
//
// We wait until we need to do one of the following:
//	-- begin failover
//	-- begin switchover
//	-- add a secondary
//	-- remove a secondary
//
// Possible returns are
//	PRIMARY_FROZEN
//	PRIMARY_DEAD
//	SERVICE_DOWN
//
rm_state_machine::sm_service_state
rm_state_machine::service_up()
{
	CL_PANIC(cur_primary_p->is_primary());

	sm_service_state return_state = SERVICE_UP;

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_SERVICE_UP,
		FaultFunctions::generic);

	//
	// If a switchover is in progress, hold off removal of secondaries.
	// It's possible that user has specified a spare provider to switchover
	// to. We need to convert that provider to a secondary before making
	// it a primary. Holding off removal of secondaries can prevent that
	// new secondary from being converted back to a spare before the actual
	// switchover is started.
	//
	if (!s->is_doing_switchover()) {
		// We must remove secondaries before converting them to spares.
		return_state = identify_secs_to_remove();
	}

	if (return_state == SERVICE_UP) {
		// Convert secondaries to spares, shutdown spares and delete
		// dead providers.
		cleanup_providers();
		if (service_shutdown()) {
			//
			// After mark_down, all new invocations from remote
			// clients will be failed.
			//
			s->iterate_over(&replica_int::rma_reconf::mark_down,
			    "mark_down");

			cur_primary_p->mark_down();
			cur_primary_p = NULL;
			return_state = SERVICE_DOWN;
		} else if (primary_is_dead()) {
			cur_primary_p->mark_down();
			return_state = PRIMARY_DEAD;
		} else if (switchover()) {
			//
			// Above needs to freeze first.  We know they will
			// freeze because the switchover request assigns them a
			// switchover task.
			//
			wait_for_others(&s->deps_up, SYNC_FROZEN |
			    SYNC_PRIMARY_QUIESCED | SYNC_SERVICE_DOWN);

			return_state = freeze_primary();

		} else if (find_sec_to_add() != NULL) {
			//
			// We are just adding a secondary. No need to sync with
			// others.
			return_state = freeze_primary();

		} else if (must_freeze_for_upgrade()) {

			RM_DBG(("rm_sm: Freeze primary for svc upgrade\n"));

			signal_dependent_services();
			//
			// Wait for dependent services to freeze
			// first.  The freeze_for_upgrade request will
			// have signaled them as well.
			//
			wait_for_others(&s->deps_up,
			    SYNC_FROZEN | SYNC_PRIMARY_QUIESCED |
			    SYNC_SERVICE_DOWN);

			return_state = freeze_primary();

		} else {
			// Nothing to do.
			// Clear out any request fields that didn't trigger
			// any work.
			clear_sm_prov_states();
			// Hang out waiting for work.

			// FAULT POINT
			// Inject a fault after signaling all the threads
			// waiting on the root object cv.
			FAULTPT_HA_FRMWK(
			    FAULTNUM_HA_FRMWK_RM_ROOT_OBJ_BCAST,
			    FaultFunctions::generic);

			// FAULT POINT
			// Inject a fault after signaling all the threads
			// waiting on the root object cv.
			FAULTPT_HA_FRMWK(
			    FAULTNUM_HA_FRMWK_RM_RECONFIG_EXIT,
			    FaultFunctions::generic);

			block_in_stable_state();
		}
	}

	return (return_state);
} // service_up

void
rm_state_machine::cleanup_providers()
{
	rm_repl_service::prov_reg_impl *rp;

	// Walk through all the providers and see if they need some cleanup.
	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	while ((rp = rp_iter.get_current()) != NULL) {
		uint_t smp_state = rp->get_sm_prov_state();
		// Advance here because rp might get deleted.
		rp_iter.advance();

		if (rp == cur_primary_p)
			continue;

		// Convert secondary to spare
		if (((smp_state & rm_repl_service::prov_reg_impl::
		    SM_SEC_TO_BE_SPARED)) != 0) {
			spare_sec(rp);
			rp->clear_sm_prov_state(rm_repl_service::
			    prov_reg_impl::SM_SEC_TO_BE_SPARED);
		}

		// Shutdown spare
		if (((smp_state & rm_repl_service::prov_reg_impl::
		    SM_SPARE_TO_BE_SHUTDOWN)) != 0) {
			shutdown_spare(rp);
			rp->clear_sm_prov_state(rm_repl_service::
			    prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN);
		}

		// Check if it has failed
		if (((smp_state &
		    rm_repl_service::prov_reg_impl::SM_FAILED))
		    != 0) {
			rp->clear_sm_prov_state(
			    rm_repl_service::prov_reg_impl::
			    SM_FAILED);
			rp->mark_down();
		}

		// Maybe it can be deleted now.
		if (rp != cur_primary_p) {
			rp->try_delete();
		}
	}
}

//
// Only possible return is SERVICE_FROZEN_CLEAN.
//
rm_state_machine::sm_service_state
rm_state_machine::primary_frozen()
{
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_FREEZE_SERVICE,
		FaultFunctions::generic);

	s->iterate_over(&replica_int::rma_reconf::freeze_service_invos,
	    "freeze_service_invos");

	return (SERVICE_FROZEN_CLEAN);
} // primary_frozen

//
// service_frozen_clean() -- proceed with making a failed primary to secondary
// in order to re-select a new primary, or initiating a switchover, or adding
// a secondary
//
// Possible returns from switchover:
//	PRIMARY_DEAD
//	SERVICE_SUICIDE (doesn't exist yet)
//	PRIMARY_SHUTDOWN (doesn't exist yet)
//	PRIMARY_DEACTIVATED
//
// Possible returns from service_frozen_clean() when
// add_secondary() is called:
//	PRIMARY_DEAD
//	SERVICE_SUICIDE
//	PRIMARY_SHUTDOWN (doesn't exist yet)
//	CLIENTS_FROZEN
//		secondary_failed exception, primary okay
//		secondary okay, primary okay, all secondaries added
//	NEW_SECONDARY
//
// Note that adding secondaries isn't done with a loop construct per se:
// NEW_SECONDARY state will return this state and we'll check to see
// if there are more secondaries to add.
rm_state_machine::sm_service_state
rm_state_machine::service_frozen_clean()
{
	rm_repl_service::prov_reg_impl *sec;
	sm_service_state	return_state;

	CL_PANIC(cur_primary_p);

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_SERVICE_FROZEN_CLEAN,
		FaultFunctions::generic);
	//
	// First of all, check whether we got a failed primary. This can happen
	// if a new selected primary candidate failed to become a primary
	// in start_new_primary(). If yes, we need to make it a secondary and
	// continue to select a new primary.
	//
	// Next, check whether we are here to do a swichover or adding a
	// secondary. The order of the checks is very important. For
	// example, we could have entered the state machine to do a swichover,
	// and we need to make the new_primary_p a secondary first.
	//
	// Finally, if we find there is nothing to do, we must have added all
	// the secondaries and are ready to unfreeze the primary.
	//
	uint_t failed_sm_state;
	failed_sm_state = (rm_repl_service::prov_reg_impl::SM_FAILED_PRIMARY |
	    rm_repl_service::prov_reg_impl::SM_PRIMARY_TO_MAKE_SECONDARY);
	if ((cur_primary_p->get_sm_prov_state() & failed_sm_state)
	    == failed_sm_state) {
		//
		// The current primary failed to become primary so we need to
		// convert it back to secondary before reselecting a new
		// primary.
		//
		return_state = make_primary_a_secondary();
	} else if ((sec = find_sec_to_add()) != NULL) {
		// Add a secondary.
		return_state = add_secondary(sec);
		// If add_secondary() returned user exception
		if (sec->is_down()) {
			// if we were trying to make a secondary out
			// of a spare and switchover to this provider,
			// need to back out!
			// Just enter this state again.
			// Secondary could also be down if primary
			// went down, and we got COMM FAILURE when
			// calling become_spare() on secondary.
			CL_PANIC((return_state == SERVICE_FROZEN_CLEAN) ||
			    (return_state == PRIMARY_DEAD));
		}
	} else if (switchover()) {
		// Dependency sync point: wait for above to
		// reach PRIMARY_DEACTIVATED.
		//
		// We know they will deactivate because the switchover
		// request assigns them a new_primary_p too.
		wait_for_others(&s->deps_up,
		    SYNC_PRIMARY_QUIESCED | SYNC_SERVICE_DOWN);

#ifdef FAULTPT_HA_FRMWK
		// Fault point to test 4372770.
		if (!s->deps_up.empty()) {
			FAULTPT_HA_FRMWK(
			    FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_SWITCHOVER,
			    FaultFunctions::generic);
		}
#endif // FAULTPT_HA_FRMWK

		//
		// XXX should return next state, based on user
		// exceptions.
		//
		return_state = make_primary_a_secondary();

	} else {
		// We added one or more secondaries

		FAULTPT_HA_FRMWK(
		    FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_UNFREEZE_PRIMARY,
		    FaultFunctions::generic);

		// Inform dependent services about the changes
		signal_dependent_services();

		// We can unfreeze only if we are not frozen_for_upgrade
		if (must_freeze_for_upgrade()) {
			return_state = FROZEN_FOR_UPGRADE;
		} else {
			return_state = unfreeze_primary();
		}
	}

	return (return_state);
} // service_frozen_clean

//
// frozen_for_upgrade
//
// State in which the service stays until it is unfrozen. Break out of
// the loop only if the primary has died. This allows the new primary
// to be chosen, and we go back to FROZEN_FOR_UPGRADE state.
//
rm_state_machine::sm_service_state
rm_state_machine::frozen_for_upgrade()
{
	rm_repl_service::prov_reg_impl *sec;

	while (must_freeze_for_upgrade()) {

		cleanup_providers();

		RM_DBG(("rm_sm: %s In f_f_u, svc unf=%d\n",
			s->id(), must_freeze_for_upgrade()));

		FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_SERVICE_FROZEN_FOR_UPGRADE,
		    FaultFunctions::generic);
		//
		// If a switchover is in progress, hold off removal of
		// secondaries.  It's possible that user has specified
		// a spare provider to switchover to. We need to
		// convert that provider to a secondary before making
		// it a primary. Holding off removal of secondaries
		// can prevent that new secondary from being converted
		// back to a spare before the actual switchover is
		// started.
		//
		if (!s->is_doing_switchover()) {
			if (identify_secs_to_remove() == REMOVE_SECONDARIES) {
				return (REMOVE_SECONDARIES);
			}
		}
		if (primary_is_dead()) {
			cur_primary_p->mark_down();
			return (PRIMARY_DEAD);
		}
		if (switchover()) {
			// dependent services are already frozen
			return (SERVICE_FROZEN_CLEAN);
		}
		if ((sec = find_sec_to_add()) != NULL) {
			return (add_secondary(sec));
		}

		// Wait for work
		block_in_stable_state();

		RM_DBG(("rm_sm: %s woken in upg_unf, svc unf=%d\n", s->id(),
		    must_freeze_for_upgrade()));
	}
	signal_dependent_services();
	return (unfreeze_primary());
}

//
// new_secondary()
//
// Only possible return is SERVICE_FROZEN_CLEAN.
// If secondary fails here, it will be marked down and flagged as
// needing to be removed.
//
rm_state_machine::sm_service_state
rm_state_machine::new_secondary()
{
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_NEW_SECONDARY,
		FaultFunctions::generic);

	rm_repl_service::prov_reg_impl *new_sec
	    = find_sec_to_commit();
	commit_secondary(new_sec);

	return (SERVICE_FROZEN_CLEAN);
} // new_secondary

//
// primary_deactivated()
//
// Calls disconnect_clients().
// Only possible return is DISCONNECTED
//
rm_state_machine::sm_service_state
rm_state_machine::primary_deactivated()
{
	CL_PANIC((switchover() || primary_is_dead()) &&
	    cur_primary_p->is_primary());

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_DISCONNECT,
		FaultFunctions::generic);

	s->iterate_over(&replica_int::rma_reconf::disconnect_clients,
	    "disconnect_clients");

	return (DISCONNECTED);
} // primary_deactivated

//
// disconnected() -- calls become_secondary_wrapup()
//
// Possible returns are
//	NO_PRIMARY
//	PRIMARY_DEAD
//
// cur_primary_p is nulled if return is NO_PRIMARY.
//
rm_state_machine::sm_service_state
rm_state_machine::disconnected()
{
	CL_PANIC((switchover() || primary_is_dead()) &&
	    cur_primary_p->is_primary());

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_SEC_WRAPUP,
		FaultFunctions::generic);

	sm_service_state return_state = become_secondary_wrapup();

	if (return_state == NO_PRIMARY) {
		cur_primary_p = NULL;

		wait_for_others(&s->deps_down,
		    SYNC_PRIMARY_QUIESCED | SYNC_SERVICE_DOWN);
	}

	return (return_state);
} // disconnected

//
// no_primary()
//
// There are three ways to get here:
//
//	This is our initial state, new repl_prov reg: task is
//	to bring the service up for the first time.
//
//	Primary died:  task is to bring the service
//	up with a different primary.
//
//	Switchover has successfully made the current primary
//	a secondary:  task is to bring up the new primary.
//
// Possible returns are
//	PRIMARY_PREPARED (selected new primary if failover)
//	SERVICE_UNINIT (no eligible primary and we have never started)
//	SERVICE_DOWN (no eligible primary)
//	PRIMARY_DEAD (failure from become_primary_prepare())
//
rm_state_machine::sm_service_state
rm_state_machine::no_primary()
{
	CL_PANIC(!cur_primary_p);

	rm_repl_service::prov_reg_impl		*p = NULL;
	sm_service_state			return_state;

	// XXX not really sure why this fault point is useful, but
	// it corresponds to previous version of reconfiguration.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_NO_PRIMARY,
		FaultFunctions::generic);

	wait_for_others(&s->deps_up, SYNC_NO_PRIMARY|SYNC_SERVICE_DOWN);

	//
	// Clear SM_PRIMARY_TO_MAKE_SECONDARY on all providers.
	// Ideally, we want to clear this flag on the old primary provider
	// only, but since we have already removed the information about the
	// old primary provider in the previous states(DISCONNECTED or
	// PRIMARY_DEAD), we have to clear the flag on all providers here.
	// See bug 4809076 for more information.
	//
	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; (p = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		p->clear_sm_prov_state(rm_repl_service::
		    prov_reg_impl::SM_PRIMARY_TO_MAKE_SECONDARY);
	} // for

	cur_primary_p = select_new_primary();

	if (cur_primary_p == NULL) {
		if (first_time_up) {
			return_state = SERVICE_UNINIT;
		} else {
			//
			// Service object isn't cleaned up until it
			// is unregistered or shutdown, and all
			// references are released.
			//
			return_state = SERVICE_DOWN;

			s->iterate_over(&replica_int::rma_reconf::mark_down,
			    "mark_down");
			// Shut all the replicas down.
			s->mark_dead();
			s->shutdown_providers();
			s->incr_state_count();
		}
	} else {
		return_state = PRIMARY_SELECTED;
	}

	while ((p = find_sec_to_remove()) != NULL) {
		// No need to remove now.
		p->clear_sm_prov_state(rm_repl_service::
		    prov_reg_impl::SM_SEC_TO_BE_REMOVED);
	}

	return (return_state);
} // no_primary

//
// primary_selected()
//
// Calls become_primary_prepare().
// Only possible return is RECONNECTED.
//
rm_state_machine::sm_service_state
rm_state_machine::primary_selected()
{
	CL_PANIC(cur_primary_p && !cur_primary_p->is_down());
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_PRIMARY_PREPARE,
		FaultFunctions::generic);

	return (become_primary_prepare());
} // primary_selected

//
// primary_prepared()
//
// Calls connect_to_primary().
// Only possible return is RECONNECTED.
//
rm_state_machine::sm_service_state
rm_state_machine::primary_prepared()
{
	CL_PANIC(cur_primary_p && !cur_primary_p->is_down());

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_RECONNECT,
		FaultFunctions::generic);

	connect_to_primary();

	return (RECONNECTED);
} // primary_prepared

//
// reconnected()
//
// Calls become_primary() on new primary.
// Possible returns
//	SERVICE_FROZEN_DIRTY
//		become_primary() was completely successful, or
//		secondary_failed exception, primary okay
//	PRIMARY_DEAD (comm failure on primary)
//	PRIMARY_DEACTIVATED (user repl_prov_failed exception on primary)
//	SERVICE_SUICIDE (doesn't exist yet)
//
//
rm_state_machine::sm_service_state
rm_state_machine::reconnected()
{
	CL_PANIC(cur_primary_p && !cur_primary_p->is_down());

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_START_PRIMARY,
		FaultFunctions::generic);

	// XXX We're not yet handling the service_failed user exceptions
	// on become_primary()

	sm_service_state return_state = start_new_primary();

	return (return_state);
} // reconnected

//
// service_frozen_dirty()
//
// Calls unfreeze_primary().
// Possible returns
//	CLIENTS_FROZEN (unfreeze_primary() succeeded)
//	PRIMARY_DEAD (comm failure on primary)
//	PRIMARY_DEACTIVATED (user repl_prov_failed exception on primary)
//	SERVICE_SUICIDE (doesn't exist yet)
//
rm_state_machine::sm_service_state
rm_state_machine::service_frozen_dirty()
{
	CL_PANIC(cur_primary_p && cur_primary_p->is_primary());

	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_RM_UNFREEZE_PRIMARY,
	    FaultFunctions::generic);

	sm_service_state return_state = CLIENTS_FROZEN;
	// XXX We're not yet handling the service_failed by repl_prov user
	// exception on become_primary().

	// Wait for above services to leave no_primary. They will not leave
	// no_primary if this service's primary dies. So we must check for that
	// too.
	while (!primary_is_dead() &&
	    !check_other_states(&s->deps_up,
	    (SYNC_TRYING_PRIMARY |
	    SYNC_COMMITTED_PRIMARY |
	    SYNC_SERVICE_DOWN |
	    SYNC_SERVICE_UNINIT))) {
		cv.wait(&state_mutex);
	}

	if (primary_is_dead()) {
		return_state = PRIMARY_DEAD;
		cur_primary_p->mark_down();
	} else if (must_freeze_for_upgrade()) {
		return_state = FROZEN_FOR_UPGRADE;
	} else {
		return_state = unfreeze_primary();
	}

	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_RM_BEFORE_MOVE_TO_SFD,
	    FaultFunctions::generic);

	return (return_state);
} // service_frozen_dirty

//
// clients_frozen()
//
// Calls unfreeze_service_invos().
// Only possible return is SERVICE_UP.
//
rm_state_machine::sm_service_state
rm_state_machine::clients_frozen()
{
	CL_PANIC(cur_primary_p && cur_primary_p->is_primary());

	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_RM_UNFREEZE_SERVICE,
		FaultFunctions::generic);

	s->iterate_over(&replica_int::rma_reconf::unfreeze_service_invos,
	    "unfreeze_service_invos");

	return (SERVICE_UP);
} // clients_frozen

//
// service_suicide()
//
// User exception on primary tells us the service wants to shut
// itself down.
// Calls mark_down on the service.
// Only possible return is SERVICE_DOWN.
//
rm_state_machine::sm_service_state
rm_state_machine::service_suicide()
{
#ifndef LATER
	//
	// SCMSGS
	// @explanation
	// Unimplemented feature was activated.
	// @user_action
	// Contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "HA: rm_state_machine::service_suicide() not yet implemented");
#endif
	CL_PANIC(!cur_primary_p);
	s->iterate_over(&replica_int::rma_reconf::mark_down, "mark_down");
	return (SERVICE_DOWN);
} // service_frozen_suicide

//
// primary_dead()
//
// If this is the initial state, we were called by
// the reg_impl callback handler in the per-service thread;
// otherwise, we got here from a failure while we were
// moving through the state machine.
//
// Calls wait_on_cmm_for_frozen().
// Only possible return is NO_PRIMARY.
//
rm_state_machine::sm_service_state
rm_state_machine::primary_dead()
{
	// cur_primary_p can be NULL when we've entered this state
	// from service_recovery().
	if (cur_primary_p) {
		CL_PANIC(cur_primary_p->is_down());
		cur_primary_p = NULL;
	}

	wait_on_cmm_for_frozen();
	wait_for_others(&s->deps_down,
	    SYNC_PRIMARY_QUIESCED|SYNC_SERVICE_DOWN);

	return (NO_PRIMARY);
} // primary_dead

rm_state_machine::sm_service_state
rm_state_machine::service_down()
{
	sm_service_state	return_state = SERVICE_DOWN;
	rm_repl_service::prov_reg_impl *p;

	CL_PANIC(cur_primary_p == NULL);

	// The order of these checks is important.
	if ((p = find_sec_to_remove()) != NULL) {
		// We can't remove them now that there is no primary.
		p->clear_sm_prov_state(
		    rm_repl_service::prov_reg_impl::SM_SEC_TO_BE_REMOVED);
	} else {
		cleanup_providers();

		// For every service that depends on this and is in
		// service_uninit state, we need to kick it into service_down
		// state since that won't get any event that does that.
		rm_repl_service *dep_s;
		SList<rm_repl_service>::ListIterator iter(s->deps_up);
		while ((dep_s = iter.get_current()) != NULL) {
			iter.advance();
			rm_state_machine *sm = dep_s->get_state_machine();
			if (sm->get_state() ==
			    rm_state_machine::SERVICE_UNINIT) {
				sm->signal_state_machine();
			}
		}

		if (s->can_exit_state_machine()) {
			// Keep it in stable state. This won't change any more.
			is_stable_state = true;

			// We can go away now...
			return_state = EXIT_STATE;
		} else {
			// Nothing to do.
			// Clear out any request fields that didn't trigger any
			// work.
			clear_sm_prov_states();

			// Hang out waiting for work.
			block_in_stable_state();
		}
	}
	return (return_state);
}

// ====================

// get the first repl_prov in our list whose SM state is the specified
// state
rm_repl_service::prov_reg_impl *	// lint
rm_state_machine::get_repl_prov(
    const rm_repl_service::prov_reg_impl::rcf_state_private s_arg)
{
	rm_repl_service::prov_reg_impl		*p = NULL;
	CL_PANIC(state_lock_held());

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; (p = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		if (p->get_sm_prov_state() & (uint_t)s_arg) {
			break;
		}
	} // for

	return (p);
}

// Clear sm_prov_state for all providers in the service.
// This is called because we have checked for work to do and found that there
// is no more. The requests indicated by the states being cleared are being
// ignored because they were invalid, or because they didn't require any work.
// We assert that we only clear states that we expect to ignore.
void
rm_state_machine::clear_sm_prov_states()
{
	rm_repl_service::prov_reg_impl		*p = NULL;

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; (p = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		p->clear_sm_prov_state(
		    rm_repl_service::prov_reg_impl::
		    SM_SEC_TO_BE_SPARED|
		    rm_repl_service::prov_reg_impl::SM_SPARE_TO_ADD);
	} // for
}

//
// returns true if exception was due to comm failure (which is
// the only kind of failure we can handle).
//
bool
rm_state_machine::inval_rp_state_exception(Environment &e)
{
	ASSERT(e.exception());
	RM_DBG(("rm_sm: inval_rp_state_exception\n"));
	bool comm_failure = false;
	replica::invalid_repl_prov_state *ue;
	if ((ue = replica::invalid_repl_prov_state::_exnarrow(e.exception()))
			!= NULL) {
		// invocation was on object other than primary.

		//
		// SCMSGS
		// @explanation
		// The system did not perform this operation on the primary
		// object.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: repl_mgr: exception invalid_repl_prov_state %d",
		    ue->curr_state);
	} else if (CORBA::COMM_FAILURE::_exnarrow(e.exception()) != NULL) {
		comm_failure = true;
	} else {
		//
		// SCMSGS
		// @explanation
		// An unrecoverable failure occurred in the HA framework.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: repl_mgr: exception occurred while invoking RMA");
	}
	e.clear();
	return (comm_failure);
}

//
// To be called after we invoke an operation on the primary.  The failure of
// the primary may be detected by an exception on the invocation or because it
// is marked as failed by an unreferenced coming in.
// The environment passed in should be the one that was passed to
// the invocation on the primary.
// The state parameter is the next state of the state machine if the primary
// has failed.
//
// XXX When we implement the primary_failed exception, this
// needs to be expanded to deal with it.
//
void
rm_state_machine::update_primary_state(sm_service_state *failed_state,
    Environment &e)
{
	bool primary_down = false;
	if (e.exception()) {
		primary_down = inval_rp_state_exception(e);
	} else {
		primary_down = primary_is_dead();
	}
	if (primary_down) {
		cur_primary_p->mark_down();
		*failed_state = PRIMARY_DEAD;
		return;
	}
}

// The primary is dead if it has been marked as failed and we are not
// in the process of shutting it down.
bool
rm_state_machine::primary_is_dead()
{
	return ((cur_primary_p->get_sm_prov_state() &
	    (rm_repl_service::prov_reg_impl::SM_FAILED)) != 0);
}

//
// Inner function to invoke add_secondary on the primary.
//
rm_state_machine::sm_service_state
rm_state_machine::invoke_add_secondary(
    const replica_int::rma_repl_prov_ptr primary_ptr,
    const replica::checkpoint_ptr sec_chkpt,
    const char *prov_desc)
{
	Environment	e;
	sm_service_state return_state = NEW_SECONDARY;

	state_mutex.unlock();
	primary_ptr->add_secondary(sec_chkpt, prov_desc, e);
	state_mutex.lock();

	//
	// If the add_secondary failed because of a failure on the secondary,
	// we want to move to SERVICE_FROZEN_CLEAN, because we won't be able
	// to add this secondary.
	//
	// Otherwise, get the next state from update_primary_state, which will
	// check for primary failures, and update the return state.
	//
	if (replica::invalid_version::_exnarrow(e.exception()) != NULL) {
		return_state = SERVICE_FROZEN_CLEAN;
		e.clear();
	} else {
		update_primary_state(&return_state, e);
	}

	return (return_state);
}

//
// add_secondary() -- wrapper for add_secondary() invocation on
// current primary.
// Assumes primary has reg_impl allocated for itself.
// Assumes proposed secondary is currently a spare.
//
// Possible results:
//	--	returns NEW_SECONDARY (primary is up), and proposed
//		secondary is now a secondary.  Calls all succeeded.
//	--	returns PRIMARY_DEAD (primary failed in one of
//		the calls to it),
//		proposed secondary is spare, since we deactivated it.
//	--	returns PRIMARY_DEAD (primary failed in one of
//		the calls to it),
//		proposed secondary is down, which we noticed when we
//		tried to deactivate it.
//	--	SERVICE_FROZEN_CLEAN, other results when user
//		exceptions are supported in the future.
rm_state_machine::sm_service_state
rm_state_machine::add_secondary(
    rm_repl_service::prov_reg_impl *const sec_rp)
{
	ASSERT(sec_rp);
	CL_PANIC(sec_rp->is_spare());

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_RM_ADD_SECONDARY_1,
		FaultFunctions::generic);

	RM_DBG(("rm_sm %s: entering add_secondary with "
	    "proposed secondary \"%s\" reg_impl `%p' "
	    "ckpt ptr `%p', "
	    "primary reg_impl `%p'\n",
	    s->id(),
	    sec_rp->get_rp_desc(), sec_rp, sec_rp->get_sec_chkpt(),
	    cur_primary_p));

	sm_service_state return_state;
	const replica_int::rma_repl_prov_ptr primary_ptr
	    = cur_primary_p->get_rma_repl_prov_ptr();

	return_state = invoke_add_secondary(
	    primary_ptr,
	    sec_rp->get_sec_chkpt(),
	    sec_rp->get_rp_desc());
	if (return_state == NEW_SECONDARY) {
		sec_rp->set_repl_prov_state(replica::AS_SECONDARY);
		// We need to be able to find this secondary
		// when we're in NEW_SECONDARY state.
		sec_rp->set_sm_prov_state(
		    rm_repl_service::prov_reg_impl::
		    SM_SEC_NEED_COMMIT);
		s->incr_state_count();
	} else if (return_state == PRIMARY_DEAD) {
		sec_rp->set_repl_prov_state(replica::AS_SECONDARY);
		CL_PANIC(cur_primary_p->is_down());
		sec_rp->set_sm_prov_state(
		    rm_repl_service::prov_reg_impl::
		    SM_SEC_TO_BE_SPARED);
	} else {
		//
		// New with Rolling Upgrade is the possbility that a user
		// exception (prov_failed) can be returned. This is currently
		// the only possible user exception, so we assume that here.
		//
		// For now, just leave the spare as a failed secondary.
		// Later, we will mimic the FAILED_PRIMARY behavior, and
		// unset the FAILED_SECONDARY flag so that it can become
		// a secondary later if conditions change.
		//
		sec_rp->set_sm_prov_state(rm_repl_service::prov_reg_impl::
		    SM_FAILED_SECONDARY);

		sec_rp->clear_sm_prov_state(
		    rm_repl_service::prov_reg_impl::SM_SPARE_TO_ADD |
		    rm_repl_service::prov_reg_impl::SM_SPARE_TO_MAKE_SEC);

		//
		// XXX when new user exceptions are supported,
		// there will be other possible return states,
		// such as SERVICE_FROZEN_CLEAN (primary reports
		// that sec was unreachable.)
		// This is also returned if dependencies aren't satisfied.
		//
		CL_PANIC(return_state == SERVICE_FROZEN_CLEAN);
	}

	RM_DBG(("rm_sm %s: add_secondary returning with "
	    "proposed secondary \"%s\" in state `%x'\n",
	    s->id(), sec_rp->get_rp_desc(), sec_rp->get_repl_prov_state()));

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_RM_ADD_SECONDARY_2,
		FaultFunctions::generic);

	return (return_state);
}

//
// Inner function to invoke commit_secondary on a new secondary.
// Called from NEW_SECONDARY state.
// If secondary fails, it's marked down and flagged as needing to
// be removed.
//
void
rm_state_machine::commit_secondary(
    rm_repl_service::prov_reg_impl *sec)
{
	CL_PANIC(!cur_primary_p->is_down() && sec->is_secondary());

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_RM_COMMIT_SECONDARY,
		FaultFunctions::generic);

	Environment	e;

	state_mutex.unlock();
	sec->get_rma_repl_prov_ptr()->commit_secondary(e);
	state_mutex.lock();
	if (e.exception()) {
		const bool rp_down = inval_rp_state_exception(e);
		if (rp_down) {
			sec->set_sm_prov_state(rm_repl_service::
			    prov_reg_impl::SM_SEC_TO_BE_REMOVED);
			sec->mark_down();
		}
	}
	sec->clear_sm_prov_state(
	    rm_repl_service::prov_reg_impl::SM_SEC_NEED_COMMIT);
}

//
// connect_to_primary()
//
// Calls connect_to_primary() on all the rma's
//
void
rm_state_machine::connect_to_primary()
{
	const replica_int::rma_repl_prov_ptr primary_ptr
	    = cur_primary_p->get_rma_repl_prov_ptr();
	CL_PANIC(!CORBA::is_nil(primary_ptr));

	state_mutex.unlock();

	RM_DBG(("rm_sm %s: "
	    "calling connect_to_primary for primary prov_ptr `%p'\n",
	    s->id(), primary_ptr));

	repl_mgr_impl::rcf_list &rma_list
	    = repl_mgr_impl::get_rcf_list();
	rma_list.lock();
	replica_int::rma_reconf_ptr		p;

	replica_int::primary_info	info;
	info.sid		= s->get_number();
	info.primary		= replica_int::rma_repl_prov::_duplicate(
	    primary_ptr);

#if defined(FAULT_HA_FRMWK)
	int fault_count = 0;
#endif

	//
	// If we get a COMM FAILURE exception, the rma_reconf
	// object will be erased from the list.
	//
	for (rma_list.atfirst(); (p = rma_list.current()) != NULL; ) {
		Environment	e;

#if defined(FAULT_HA_FRMWK)
		void *fault_argp;
		uint32_t fault_argsize;

		if (fault_triggered(
		    FAULTNUM_RM_STATE_MACH_CONNECT_PRIMARY,
		    &fault_argp,
		    &fault_argsize)) {

			uint_t fnum = FAULTNUM_RM_STATE_MACH_CONNECT_PRIMARY;

			FaultFunctions::ha_rm_reboot_arg_t *arg =
			    (FaultFunctions::ha_rm_reboot_arg_t *) fault_argp;

			if ((arg->nodeid == orb_conf::node_number()) &&
			    (arg->sid == FaultFunctions::tsd_svc_id())) {

				fault_count++;
				if (fault_count > 1) {
					FaultFunctions::wait_for_arg_t wait_arg;
					wait_arg.op = arg->op;
					wait_arg.nodeid = arg->nodeid;

					FaultFunctions::reboot(fnum, &wait_arg,
					    (uint32_t)sizeof (wait_arg));
				}
			}
		}
#endif

		RM_DBG(("rm_sm %s: "
		    "calling connect_to_primary on reconf ptr `%p'\n",
		    s->id(), p));
		p->connect_to_primary(info, e);
		if (e.exception()) {
			RM_DBG(("rm_sm %s: got exception on "
			    "rma_reconf `%p'\n,"
			    "current primary `%p'\n",
			    s->id(), p,
			    cur_primary_p));
			// will panic if not comm failure
			s->rma_reconf_exception(p, e);
		} else {
			rma_list.advance();
		}
	}
	rma_list.unlock();
	state_mutex.lock();
	RM_DBG(("rm_sm %s: returning from connect_to_primary "
	    "primary reg_impl`%p', prov_ptr `%p'\n",
	    s->id(), cur_primary_p, primary_ptr));
}

//
// fill_sec_ckpt_seqs() -- allocate a sequence of secondary interface
// (checkpoint) objects.
// If this is a first-time startup, we choose secondaries from
// from spares; otherwise, we find existing secondaries.
// This code assumes that if it's a first-time startup, we
// won't find any providers that are already secondaries:  they
// must be spares or down.
//
void
rm_state_machine::fill_sec_ckpt_seq(replica::secondary_seq &sec_seq)
{
	uint_t	nsecs = 0;
	rm_repl_service::prov_reg_impl *p = NULL;

	if (first_time_up) {
		//
		// The number of secondaries we will bring up for the first
		// time will be no more than the desired one.
		//
		while ((nsecs < s->get_desired_numsecondaries()) &&
		    ((p = find_best_spare()) != NULL)) {
			p->set_repl_prov_state(replica::AS_SECONDARY);
			commit_secondary(p);
			// Only use if the commit succeeded.
			if (p->is_secondary()) {
				sec_seq[nsecs++] = replica::checkpoint::
				    _duplicate(p->get_sec_chkpt());
				RM_DBG(("rm_sm %s: secondary %s\n",
				    s->id(), p->get_rp_desc()));
			}
		}
	} else {
		SList<rm_repl_service::prov_reg_impl>::ListIterator
		    rp_iter(s->repl_prov_list);
		//
		// If a switchover to a spare is in progress or the old primary
		// died in the middle of shift_secs(), we can possibly have
		// more secondaries than the desired one. We need to include
		// all of them into the list.
		//
		for (; ((p = rp_iter.get_current()) != NULL &&
		    (nsecs < MAX_SECONDARIES));
		    rp_iter.advance()) {
			if (p != cur_primary_p && p->is_valid_sec() &&
			    check_secondary_dependency(p)) {
				sec_seq[nsecs++] = replica::checkpoint::
				    _duplicate(p->get_sec_chkpt());
				RM_DBG(("rm_sm %s: secondary %s\n",
				    s->id(), p->get_rp_desc()));
			}
		} // for
	}
	sec_seq.length(nsecs);

	RM_DBG(("rm_sm %s: "
	    "fill_sec_ckpt_seq found %d secondaries for "
	    "primary reg_impl `%p', rma_prov `%p'\n",
	    s->id(), nsecs, cur_primary_p,
	    cur_primary_p->get_rma_repl_prov_ptr()));
}


//
// fill_sec_name_seq() -- allocate a sequence of secondary names.
// Called from reconfiguration code.
// We find existing secondaries, whether this is first-time
// startup or not (in fill_sec_chkp_seq(), we made spares
// into secondaries).
//
void
rm_state_machine::fill_sec_name_seq(replica::repl_name_seq &name_seq)
{
	uint_t					jindex	= 0;
	rm_repl_service::prov_reg_impl	*p	= NULL;

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	//
	// If a switchover to a spare is in progress or the old primary
	// died in the middle of shift_secs(), we can possibly have
	// more secondaries than the desired one. We need to include
	// all of them into the list.
	//
	for (; ((p = rp_iter.get_current()) != NULL &&
	    (jindex < MAX_SECONDARIES));
	    rp_iter.advance()) {
		if ((p != cur_primary_p) && (p->is_valid_sec())) {
			name_seq[jindex++] =
			    rm_repl_service::init_str(p->get_rp_desc());
		}
	} // for

	name_seq.length(jindex);

	RM_DBG(("rm_sm %s: "
	    "fill_sec_name_seq found %d secondaries for "
	    "primary reg_impl `%p', rma_prov `%p'\n",
	    s->id(), jindex, cur_primary_p,
	    cur_primary_p->get_rma_repl_prov_ptr()));
}

//
// Called from service_up(). Check if there are any secondaries that need to
// be removed. If yes, the next state will be REMOVE_SECONDARIES. Otherwise,
// the next state is SERVICE_UP
//
rm_state_machine::sm_service_state
rm_state_machine::identify_secs_to_remove()
{
	Environment				e;
	sm_service_state			return_state = SERVICE_UP;
	uint32_t				actual_numsecs = 0;
	rm_repl_service::prov_reg_impl		*rp, *rpbelow;

	//
	// Walk through the provider list to see if there are any providers
	// flagged to be removed or depending on a spare provider.
	// If either of the above condition is met, flag this provider to be
	// coverted to a spare if needed and set the return_state to
	// REMOVE_SECONDARIES.
	//
	rp = rpbelow = NULL;

	// walk through each provider in this service
	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; (rp = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		if (!rp->is_secondary()) {
			continue;
		}
		// This provider is a secondary
		if ((rp->get_sm_prov_state() &
		    rm_repl_service::prov_reg_impl::SM_SEC_TO_BE_REMOVED)
		    != 0) {
			// This provider is flagged to be removed
			return_state = REMOVE_SECONDARIES;
		} else {
			if (rp->get_sm_prov_state() & rm_repl_service::
			    prov_reg_impl::SM_SEC_TO_BE_SPARED) {
				//
				// This provider has already been removed from
				// the primary's secondary list and yet to be
				// converted to a spare.
				//
				continue;
			}
			//
			// This is a valid secondary without any flags
			// indicating whether it needs to be converted to a
			// spare. But if it is on top of a spare provider,
			// it needs to be converted to a spare,
			//
			// Walk through providers on which this provider
			// depends to see if any of them are spares. If yes,
			// this provider needs to be removed and converted
			// to a spare as well.
			//
			SList<rm_repl_service::prov_reg_impl>::ListIterator
			    rpbelow_iter(rp->deps_down);
			for (; (rpbelow = rpbelow_iter.get_current()) != NULL;
			    rpbelow_iter.advance()) {
				if (rpbelow->is_valid_sec()) {
					continue;
				}

				//
				// This lower level provider is either
				// a spare or a secondary to be spared, so
				// that this provider needs to be removed
				// and spared as well.
				//
				rp->set_sm_prov_state(rm_repl_service::
				    prov_reg_impl::SM_SEC_TO_BE_REMOVED
				    | rm_repl_service::prov_reg_impl::
				    SM_SEC_TO_BE_SPARED);
				RM_DBG(("rm_sm %s: identified secondary"
				    " reg_impl %p, rma_prov `%p' to "
				    "remove.\n", s->id(), rp,
				    rp->get_rma_repl_prov_ptr()));
				return_state = REMOVE_SECONDARIES;

				//
				// No need to check out other lower level
				// providers
				//
				break;
			}

			if (rpbelow == NULL) {
				//
				// All direct lower level providers are valid
				// secondaries. This secondary will remain as
				// a secondary.
				//
				actual_numsecs++;
			}
		}
	}

	//
	// If the actual number of secondaries is more than the desired number
	// of secondaries, identify the extra secondaries to remove.
	//
	int32_t extra_numsecs;
	extra_numsecs = (int32_t)(actual_numsecs -
			    s->get_desired_numsecondaries());
	for (int i = 0; i < extra_numsecs; i++) {
		rp = find_best_sec_to_remove();
		//
		// Since we are holding the mutex_lock, no secondary can be
		// removed at this point. We should expect an extra secondary
		// be found.
		//
		CL_PANIC(rp);
		rp->set_sm_prov_state(
		    rm_repl_service::prov_reg_impl::SM_SEC_TO_BE_REMOVED
		    | rm_repl_service::prov_reg_impl::SM_SEC_TO_BE_SPARED);
		return_state = REMOVE_SECONDARIES;
	}

	if (return_state == REMOVE_SECONDARIES) {
		// Notify dependent services about the changes
		signal_dependent_services();
	}

	return (return_state);
}

//
// Find the best secondary to remove. If nothing found, return NULL.
// TThe factors that determine which secondary to be removed first are as
// follow in order:
//	1. The secondary can bring up the least number of services in future
//	2. The secondary with the lowest priority or tree priority.
//
rm_repl_service::prov_reg_impl *
rm_state_machine::find_best_sec_to_remove()
{
	rm_repl_service::prov_reg_impl *rp, *found_rp = NULL;

	RM_DBG(("rm_sm %s: find_best_sec_to_remove()\n", s->id()));

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; (rp = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		if (rp->is_valid_sec()) {
			if (!found_rp ||
			    found_rp->has_better_overall_pri_than(rp)) {
				found_rp = rp;
			}
		}
	}

	return (found_rp);
}

//
// remove_secondaries()
//
// We look at the rcf_state_private of all the replicas and perform the
// operations accordingly. We remove secondaries from the primary, making
// secondaries spare, and shutting spares down in this step.
//
rm_state_machine::sm_service_state
rm_state_machine::remove_secondaries()
{
	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_RM_REMOVE_SECONDARIES_1,
		FaultFunctions::generic);

	Environment	e;
	rm_repl_service::prov_reg_impl *sec	= NULL;
	sm_service_state	return_state	= SERVICE_UP;

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; ((sec = rp_iter.get_current()) != NULL); rp_iter.advance()) {

		if (!sec->is_secondary() || !(sec->get_sm_prov_state() &
		    rm_repl_service::prov_reg_impl:: SM_SEC_TO_BE_REMOVED)) {
			continue;
		}

		RM_DBG(("rm_sm %s: "
		    "down reg_impl %p, rma_prov `%p' is a secondary:\n"
		    "calling remove_secondary() on primary_ptr `%p'\n",
		    s->id(), sec, sec->get_rma_repl_prov_ptr(),
		    cur_primary_p->get_rma_repl_prov_ptr()));

		if (return_state == SERVICE_UP) {
			state_mutex.unlock();
			cur_primary_p->get_rma_repl_prov_ptr()->
			    remove_secondary(sec->get_sec_chkpt(),
			    sec->get_rp_desc(), e);
			state_mutex.lock();

			// FAULT POINT
			FAULTPT_HA_FRMWK(
				FAULTNUM_HA_FRMWK_RM_REMOVE_SECONDARIES_2,
				FaultFunctions::generic);

			update_primary_state(&return_state, e);
			if (return_state == SERVICE_UP) {
				s->incr_state_count();
				//
				// If the service is frozen for upgrade then
				// stay there rather than going to UP
				//
				if (must_freeze_for_upgrade()) {
					return_state = FROZEN_FOR_UPGRADE;
				}
			}

			//
			// The primary may have died before, during or after the
			// remove_secondary() call. But we have no way of
			// telling when exactly the primary has died.
			// So at this point, it is only safe to assume that
			// this secondary has been removed from the primary's
			// secondary list and needs to be converted to a spare,
			// regardless of the primary's state.
			//
			sec->clear_sm_prov_state(
			    rm_repl_service::prov_reg_impl::
			    SM_SEC_TO_BE_REMOVED);
		} else {
			//
			// The service primary has died, we want to preserve
			// these secodaries to allow them to participate in
			// the selection of new primary. Note: we need to clear
			// both of the flags below, else this provider will
			// not be considered as a valid secondary in the
			// new primary selection process.
			//
			sec->clear_sm_prov_state(
			    rm_repl_service::prov_reg_impl::
			    SM_SEC_TO_BE_REMOVED);

			sec->clear_sm_prov_state(
			    rm_repl_service::prov_reg_impl::
			    SM_SEC_TO_BE_SPARED);
		}
	}

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_RM_REMOVE_SECONDARIES_3,
		FaultFunctions::generic);

	return (return_state);
}

//
// spare_sec()
//
void
rm_state_machine::spare_sec(rm_repl_service::prov_reg_impl *sec)
{
	CL_PANIC(sec->is_secondary() || sec->is_down());

	Environment	e;

	state_mutex.unlock();
	sec->get_rma_repl_prov_ptr()->become_spare(e);
	state_mutex.lock();
	if (e.exception()) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			sec->mark_down();
		} else {
			CL_PANIC(0);
		}
	} else {
		sec->set_repl_prov_state(replica::AS_SPARE);
	}
}

//
// shutdown_spare()
//
void
rm_state_machine::shutdown_spare(
    rm_repl_service::prov_reg_impl *spare)
{
	Environment	e;
	CL_PANIC(spare->is_spare() || spare->is_down());
	CL_PANIC(spare->deps_up.empty());

	// When the node receives the shutdown call it will release its
	// reference to reg_impl object. So after this point, the
	// reg_impl may be unreferenced and deleted at any time.

	if (s->is_dead()) {
		state_mutex.unlock();
		// This is part of the cleanup after the service has died
		spare->get_rma_repl_prov_ptr()->shutdown(
		    replica::SERVICE_CLEANUP, e);
	} else if (s->is_deleted()) {
		state_mutex.unlock();
		// This is part of the service shutdown
		spare->get_rma_repl_prov_ptr()->shutdown(
		    replica::SERVICE_SHUTDOWN, e);
	} else {
		state_mutex.unlock();
		// This is to remove the provider only
		spare->get_rma_repl_prov_ptr()->shutdown(
		    replica::PROVIDER_REMOVAL, e);
	}
	state_mutex.lock();
	if (e.exception()) {
		//
		// The node we are shutting down is already died, which is just
		// fine. Panick if the exception is not due to communication
		// failure.
		//
		CL_PANIC(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
	}
	spare->mark_down();

	// Cleanup the dependency list so that the bottom providers can be
	// shutdown immediately.
	spare->clear_deps_down();
}

//
// Executed from SERVICE_UP state, to begin switchover or
// to add a secondary.
//
rm_state_machine::sm_service_state
rm_state_machine::freeze_primary()
{
	ASSERT(cur_primary_p);

	Environment	e;
	sm_service_state return_state	= PRIMARY_FROZEN;

	RM_DBG(("rm_sm %s: freeze_primary()\n", s->id()));

	s->iterate_over(&replica_int::rma_reconf::freeze_service_prepare,
	    "freeze_service_prepare");
	state_mutex.unlock();
	cur_primary_p->get_rma_repl_prov_ptr()->freeze_primary(e);
	state_mutex.lock();
	update_primary_state(&return_state, e);

	return (return_state);
}

//
rm_state_machine::sm_service_state
rm_state_machine::make_primary_a_secondary()
{
	Environment	e;
	sm_service_state return_state	= PRIMARY_DEACTIVATED;

	RM_DBG(("rm_sm %s: make_primary_a_secondary\n", s->id()));

	bool primary_has_failed = (cur_primary_p->get_sm_prov_state() &
	    rm_repl_service::prov_reg_impl::SM_FAILED_PRIMARY) != 0;
	state_mutex.unlock();
	cur_primary_p->get_rma_repl_prov_ptr()->become_secondary(
	    primary_has_failed, e);
	state_mutex.lock();
	if (replica::repl_prov_failed::_exnarrow(e.exception())) {
		RM_DBG(("rm_sm %s: repl_prov_failed exception\n",
		    s->id()));
		cur_primary_p->set_sm_prov_state(
		    cur_primary_p->get_sm_prov_state() |
		    rm_repl_service::prov_reg_impl::SM_FAILED_PRIMARY);
	} else if (replica::become_secondary_failed::_exnarrow(
	    e.exception())) {
		RM_DBG(("rm_sm %s: become_secondary_failed exception\n",
		    s->id()));

		// The primary can't become a secondary and has to remain
		// a primary. We need to set the sm_prov_state to make
		// sure that it will be chosen as the primary in the following
		// reconfiguration. We need to set it such that the bottom
		// service will choose the same replica as the primary again
		// and all the top level services will follow.

		repl_service_list *tree = s->get_component_tree();

		// Find one service that doesn't depend on anyone and
		// make sure that it will choose the primary we desire.
		// Also clear the sm_prov_state for all the services.
		SList<rm_repl_service>::ListIterator iter(tree);
		rm_repl_service *bottom_s = NULL;
		rm_repl_service *current_s = NULL;
		while ((current_s = iter.get_current()) != NULL) {
			// Clear other sm_prov_state first.
			SList<rm_repl_service::prov_reg_impl>::ListIterator
			    rp_iter(current_s->repl_prov_list);
			rm_repl_service::prov_reg_impl *rp = NULL;
			while ((rp = rp_iter.get_current()) != NULL) {
				rp_iter.advance();
				rp->clear_sm_prov_state(rm_repl_service::
				    prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY);
			}

			if (bottom_s == NULL &&
			    current_s->deps_down.empty()) {
				bottom_s = current_s;
			}
			iter.advance();
		}
		CL_PANIC(bottom_s != NULL);

		// Make sure that the bottom service we choose will choose
		// the current primary.
		bottom_s->smp->cur_primary_p->set_sm_prov_state(
		    rm_repl_service::prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY |
		    bottom_s->smp->cur_primary_p->get_sm_prov_state());

		delete tree;
	} else {
		update_primary_state(&return_state, e);
	}

	return (return_state);
}

rm_state_machine::sm_service_state
rm_state_machine::become_secondary_wrapup()
{
	Environment					e;
	sm_service_state return_state		= NO_PRIMARY;

	RM_DBG(("rm_sm %s: become_secondary_wrapup\n", s->id()));

	state_mutex.unlock();
	cur_primary_p->get_rma_repl_prov_ptr()->become_secondary_wrapup(e);
	state_mutex.lock();
	update_primary_state(&return_state, e);
	// XXX why this test?
	if (return_state == NO_PRIMARY) {
		cur_primary_p->set_repl_prov_state(replica::AS_SECONDARY);
	}

	return (return_state);
}

rm_state_machine::sm_service_state
rm_state_machine::become_primary_prepare()
{
	CL_PANIC(cur_primary_p->is_secondary() || cur_primary_p->is_down() ||
	    first_time_up);

	Environment				e;
	sm_service_state return_state	= PRIMARY_PREPARED;

	RM_DBG(("rm_sm %s: become_primary_prepare\n", s->id()));

	// We are now committed to this primary.
	replica::secondary_seq	secs(MAX_SECONDARIES, MAX_SECONDARIES);
	fill_sec_ckpt_seq(secs);
	if (first_time_up) {
		cur_primary_p->set_repl_prov_state(replica::AS_SECONDARY);
		// fill_sec_ckpt_seq() was the last thing to care about a
		// first time startup.
		first_time_up = false;
	}

	state_mutex.unlock();
	cur_primary_p->get_rma_repl_prov_ptr()->become_primary_prepare(
	    secs, e);
	state_mutex.lock();
	update_primary_state(&return_state, e);

	return (return_state);
}

// Called only during failover, not switchover.
void
rm_state_machine::wait_on_cmm_for_frozen()
{
	// Get the hxdoor_service associated with this ha service on this node.
	hxdoor_service *hs = rma::the().lookup_service(s->get_number());
	ASSERT(hs);

	//
	// We must drop the state lock here to avoid deadlock where the
	// RMM part of CMM reconfiguration needs to get the state lock to make
	// progress.
	//
	state_mutex.unlock();
	hs->wait_on_cmm_to_disconnect_dead_primary();
	state_mutex.lock();

	RM_DBG(("rm_sm %s: returned from wait_for_frozen on cmm\n", s->id()));
}

//
// select_new_primary()
//
// Called when initial state is NO_PRIMARY or a switchover failed and
// we executed PRIMARY_DEAD to get to NO_PRIMARY, or this is a
// failover.
// If we're at the bottom, and have a primary selected, we use that one, rather
// than selecting a new one.
//
rm_repl_service::prov_reg_impl *
rm_state_machine::select_new_primary()
{
	rm_repl_service::prov_reg_impl *p = NULL;

	// choose new primary
	if (s->deps_down.empty()) {
		if (s->is_lower_service_shutdown()) {
			//
			// Our lower level service has been shutdown by the
			// old RM primary and removed itself from our deps_down
			// list. But still we can't live without the lower
			// level service up and running. So we have to go
			// down too. See bug 4693284 for more information.
			//
			CL_PANIC(cur_primary_p == NULL);
		} else if (cur_primary_p != NULL) {
			//
			// cur_primary_p could have been set by another
			// service's state machine from
			// find_elig_primary_from_secs.
			//
			p = cur_primary_p;
		} else {
			p = first_time_up ? find_elig_primary() :
			    find_elig_primary_from_secs();
		}
	} else {

		// Wait for below to pick a new primary and start it up.
		wait_for_others(&s->deps_down,
		    SYNC_COMMITTED_PRIMARY|SYNC_SERVICE_DOWN);

		// Make sure that the service we depend on is up.
		bool service_is_down = false;
		rm_repl_service *deps_s;
		SList<rm_repl_service>::ListIterator iter(s->deps_down);
		while ((deps_s = iter.get_current()) != NULL) {
			iter.advance();
			if (deps_s->smp->check_state(SYNC_SERVICE_DOWN)) {
				service_is_down = true;
				break;
			}
		}

		// Find our primary based on what the below services selected.
		if (!service_is_down) {
			SList<rm_repl_service::prov_reg_impl>::ListIterator
			    rp_iter(s->repl_prov_list);
			for (; ((p = rp_iter.get_current()) != NULL);
			    rp_iter.advance()) {
				if (check_primary_dependency(p)) {
					break;
				}
			}
		}
	}

	RM_DBG(("rm_sm %s: select_new_primary first_time_up `%d'\n",
	    s->id(), first_time_up));

	return (p);
}

rm_state_machine::sm_service_state
rm_state_machine::start_new_primary()
{
	CL_PANIC((cur_primary_p->is_secondary()));

	RM_DBG(("rm_sm %s: "
	    "start_new_primary called with "
	    "cur_primary_p reg_impl `%p', prov_ptr `%p'\n",
	    s->id(), cur_primary_p, cur_primary_p->get_rma_repl_prov_ptr()));

	sm_service_state return_state = SERVICE_FROZEN_DIRTY;
	// allocate sequence of secondaries (checkpoint objects).
	// if this is a first time startup, secondaries are spares.
	replica::repl_name_seq	sec_names(MAX_SECONDARIES, MAX_SECONDARIES);
	fill_sec_name_seq(sec_names);

	Environment			e;
	state_mutex.unlock();
	cur_primary_p->get_rma_repl_prov_ptr()->become_primary(sec_names, e);
	state_mutex.lock();

	FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_FAIL_START_NEW_PRIMARY,
	    FaultFunctions::generic);

	if (replica::repl_prov_failed::_exnarrow(e.exception())) {
		RM_DBG(("rm_sm %s: repl_prov_failed exception\n",
		    s->id()));
		cur_primary_p->set_sm_prov_state(
		    rm_repl_service::prov_reg_impl::
		    SM_PRIMARY_TO_MAKE_SECONDARY|
		    rm_repl_service::prov_reg_impl::SM_FAILED_PRIMARY);
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		return_state = SERVICE_FROZEN_CLEAN;
	} else {
		update_primary_state(&return_state, e);
	}
	if (return_state == SERVICE_FROZEN_DIRTY) {
		// Now that a provider has successfully become primary, we
		// clear the SM_FAILED_PRIMARY state on all other providers
		// so that they get tried again next time through. Eventually,
		// they will be shutdown or repaired by the administrator.
		rm_repl_service::prov_reg_impl *p;
		SList<rm_repl_service::prov_reg_impl>::ListIterator
		    rp_iter(s->repl_prov_list);
		for (; ((p = rp_iter.get_current()) != NULL);
		    rp_iter.advance()) {
			p->clear_sm_prov_state(rm_repl_service::
			    prov_reg_impl::SM_FAILED_PRIMARY);
		}
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		s->incr_state_count();
	}

	return (return_state);
}


rm_state_machine::sm_service_state
rm_state_machine::unfreeze_primary()
{
	RM_DBG(("rm_sm %s: "
	    "calling unfreeze_primary, "
	    "primary reg_impl `%p', prov_ptr `%p'\n",
	    s->id(), cur_primary_p, cur_primary_p->get_rma_repl_prov_ptr()));

	Environment			e;
	state_mutex.unlock();
	cur_primary_p->get_rma_repl_prov_ptr()->unfreeze_primary(e);
	state_mutex.lock();

	sm_service_state return_state	= CLIENTS_FROZEN;
	cur_primary_p->clear_sm_prov_state(rm_repl_service::
	    prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY);

	update_primary_state(&return_state, e);

	return (return_state);
}

rm_state_machine::sm_service_state
rm_state_machine::exit_state()
{
	CL_PANIC(0);
	return (EXIT_STATE);
}

//
// find_elig_primary() -- find an eligible primary from list of spares.  Called
// from operations where the service is coming up for the first time.
//
rm_repl_service::prov_reg_impl *
rm_state_machine::find_elig_primary()
{
	rm_repl_service::prov_reg_impl *p = find_best_spare();
	CL_PANIC(first_time_up);

	if ((p != NULL) && check_primary_dependency(p)) {
		return (p);
	} else {
		return (NULL);
	}
}

rm_state_machine::repl_prov_combo_score::repl_prov_combo_score() :
	selected_as_primary(false), num_running(0), num_start(0), priority(0)
{
}

bool
rm_state_machine::repl_prov_combo_score::better_than(
    repl_prov_combo_score *other)
{
	if (selected_as_primary && !other->selected_as_primary)
		return (true);
	if (other->selected_as_primary && !selected_as_primary)
		return (false);
	if (num_running > other->num_running)
		return (true);
	if (num_running < other->num_running)
		return (false);
	if (num_start > other->num_start)
		return (true);
	if (num_start < other->num_start)
		return (false);
	if (priority < other->priority)
		// Note: The smaller number, the higher priority.
		return (true);
	return (false);
}

//
// Find an eligible primary from those that are currently secondaries.
// Called from reconfig code only.
//
rm_repl_service::prov_reg_impl *
rm_state_machine::find_elig_primary_from_secs()
{
	CL_PANIC(cur_primary_p == NULL);

	// We need to look at all services in the component tree and find
	// the combination of primaries that will provide the highest
	// availability. We care about the following in decreasing priority:
	// 1 the provider has been selected through change_repl_prov_status()
	// 2 number of services that can be kept running.
	// 3 number of services that can be started.
	// 4 priority of the primaries that get to run.
	//
	// We need to select from all possible valid combinations, accounting
	// for dependencies. To find all combinations, we take all services
	// in our dependency component which don't depend on other services.
	// From these, we take all repl_provs which are secondaries. Then we
	// iterate through all combinations of these repl_provs.
	repl_prov_combo_score best_so_far;
	repl_service_list base_services;
	uint_t	ncombos;	// number of combinations.
	uint_t	best_combo_so_far = 0;

	// Get the bottommost services which need to participate in this
	// selection.
	ncombos = get_base_services(&base_services);

	// If they are all in NO_PRIMARY, then we were the last one to
	// arrive and pick the primary for everyone in the list.
	// Otherwise, some other service will be the last one. We wait for
	// it to pick a primary for us.
	if (check_other_states(&base_services, SYNC_NO_PRIMARY)) {

		for (uint_t i = 0; i < ncombos; i++) {
			repl_prov_combo_score combo_val;
			evaluate_combo(&base_services, i, &combo_val);
			if (combo_val.better_than(&best_so_far)) {
				RM_DBG(("rm_sm %s: combo %d is better\n",
				    s->id(), i));
				best_so_far = combo_val;
				best_combo_so_far = i;
			}
		}

		RM_DBG(("rm_sm %s: find_elig_primary_from_secs: "
		    "ncombos = %d, bottom_list len = %d.\n\tbest = %d\n",
		    s->id(), ncombos, base_services.count(),
		    best_combo_so_far));

		// Set the current primary for all bottom services.
		// By setting them all here, we make sure that only one service
		// runs this code, so we don't have to worry about them all
		// reaching the same conclusion.
		uint_t combo_index = best_combo_so_far;
		rm_repl_service *serv;
		for (base_services.atfirst();
		    (serv = base_services.get_current()) != NULL;
		    base_services.advance()) {
			uint_t nsec = serv->num_valid_secs();
			RM_DBG(("rm_sm %s: find_elig_primary_from_secs: "
			    "index = %d, serv=%p.%s %p, nsec = %d\n", s->id(),
			    combo_index, serv, serv->id(),
			    serv->smp->cur_primary_p, nsec));
			CL_PANIC(serv->smp->cur_primary_p == NULL);
			serv->smp->cur_primary_p =
			    serv->get_indexed_sec(combo_index % nsec);
			combo_index /= nsec;
		}
		// Signal all services that might be waiting for us to pick
		// their primary. We must do this explicitly, since the
		// automatic signalling mechanism only works for things that
		// depend on us and our relationship with these other services
		// is indirect.
		// Note that we must signal all bottom level services, not
		// just those in the base_services list because some may not
		// have been added to our version of the base_services list
		// because their last secondary may have died.
		signal_bottom_services();
	} else {
		CL_PANIC(check_other_states(&base_services,
		    SYNC_PRIMARY_QUIESCED));
		// Wait for at least one of the others to move on. Then we
		// will know our primary.
		do {
			RM_DBG(("rm_sm %s: waiting for a primary to "
			    " be selected for this service\n", s->id()));
			cv.wait(&state_mutex);
		} while (check_other_states(&base_services,
		    SYNC_PRIMARY_QUIESCED));
	}

	return (cur_primary_p);
}

// Get the set of services at the bottom of the dependency tree that are
// in the NO_PRIMARY state, or are about to enter it.
// When we do a switchover, we switchover everthing in the dependency, so all
// bottom services will be in or approaching NO_PRIMARY state.
// In the failover case, we are guaranteed that all services which have
// colocated primaries will fail together and that they will be in the
// NO_PRIMARY state. There may be some services in the dependency tree
// which are at the bootom, but are not colocated. This means that
// whatever dependant service caused it to be in the dependency tree is
// not currently active. When we get a failover, such bottom services may not
// also failover, since their primary node may be fine.
// The current service is in NO_PRIMARY state when it makes this call, because
// of the dependency synchroniation on entering this state, we are guraanteed
// that all the services we want to return are either in NO_PRIMARY state
// themselves or they are about to enter NO_PRIMARY.
uint_t
rm_state_machine::get_base_services(SList<rm_repl_service> *base_services)
{
	rm_repl_service *serv;
	uint_t ncombos = 0;

	// Get all services in our component which don't depend on anything.
	repl_service_list *deps = s->get_component_tree();
	for (deps->atfirst(); (serv = deps->get_current()) != NULL;
	    deps->advance()) {
		uint_t sec_count;
		if (serv->deps_down.empty() &&
		    serv->smp->check_state(SYNC_PRIMARY_QUIESCED) &&
		    ((sec_count = serv->num_valid_secs()) != 0)) {
			base_services->append(serv);
			// Keep track of the number of possible combinations.
			if (ncombos == 0)
				ncombos = 1;
			ncombos *= sec_count;
		}
	}
	delete deps;
	return (ncombos);
}


void
rm_state_machine::signal_bottom_services()
{
	rm_repl_service *serv;

	// Get all services in our component which don't depend on anything.
	repl_service_list *deps = s->get_component_tree();
	for (deps->atfirst(); (serv = deps->get_current()) != NULL;
	    deps->advance()) {
		if (serv != s && serv->deps_down.empty()) {
			RM_DBG(("rm_sm %s: signal_bottom_services: "
			    "signaling %s\n", s->id(), serv->id()));
			serv->smp->cv.signal();
		}
	}
	delete deps;
}

void
rm_state_machine::signal_dependent_services()
{
	//
	// Notify dependent services about the changes by signalling their
	// state machine.
	//
	rm_repl_service *servp;
	for (s->deps_up.atfirst();
	    (servp = s->deps_up.get_current()) != NULL;
	    s->deps_up.advance()) {
		servp->smp->signal_state_machine();
	}
}

void
rm_state_machine::evaluate_combo(SList<rm_repl_service> *sl, uint_t combo_num,
    repl_prov_combo_score *val)
{
	rm_repl_service *serv;
	SList<rm_repl_service::prov_reg_impl> fullpl;
	SList<rm_repl_service::prov_reg_impl> more_pl;
	rm_repl_service::prov_reg_impl *p, *p1;

	// Find the full list of providers which depend on these.
	// Initialize with the current selection of bottommost providers.
	// Create the list of providers for this combination.
	uint_t combo_index = combo_num;
	for (sl->atfirst(); (serv = sl->get_current()) != NULL;
	    sl->advance()) {
		uint_t nsec = serv->num_valid_secs();
		more_pl.append(serv->get_indexed_sec(combo_index % nsec));
		combo_index /= nsec;
	}

	while (!more_pl.empty()) {
		RM_DBG(("rm_sm %s: more_pl length = %d\n",
		    s->id(), more_pl.count()));
		// Add each new prov to the fullpl list.
		SList<rm_repl_service::prov_reg_impl> tmp_pl;

		while ((p = more_pl.reapfirst()) != NULL) {
			fullpl.append(p);

			// Keep track of any dependencies that aren't in the
			// list.
			for (p->deps_up.atfirst();
			    (p1 = p->deps_up.get_current()) != NULL;
			    p->deps_up.advance()) {
				if (!fullpl.exists(p1)) {
					// Haven't added this one yet. Add it.
					tmp_pl.append(p1);
				}
			}
		}

		// Move tmp_pl to more_pl for next time through.
		more_pl.concat(tmp_pl);
		CL_PANIC(tmp_pl.empty());
	}

	// copy list
	for (fullpl.atfirst(); (p = fullpl.get_current()) != NULL;
	    fullpl.advance()) {
		more_pl.append(p);
	}

	// Remove providers whose deps_down aren't satisfied.
	// While at it, empty list more_pl
	while ((p = more_pl.reapfirst()) != NULL) {
		for (p->deps_down.atfirst();
		    (p1 = p->deps_down.get_current()) != NULL;
		    p->deps_down.advance()) {
			if (!fullpl.exists(p1)) {
				(void) fullpl.erase(p);
			}
		}
	}

	// Now we have the full set of providers whose dependencies can be
	// satisfied by this choice. Calculate the score.
	// While at it, empty list fullpl
	while ((p = fullpl.reapfirst()) != NULL) {
		RM_DBG(("rm_sm %s: full_list: p = %p(%d)\n",
		    s->id(), p, p->get_repl_prov_state()));
		if ((p->is_valid_sec() || p->is_primary()) &&
		    ((p->get_sm_prov_state() & rm_repl_service::
		    prov_reg_impl::SM_FAILED_PRIMARY) == 0)) {
			val->num_running++;
			if (p->deps_down.empty()) {
				val->priority += p->get_priority();
			}
		} else if (p->is_spare() &&
		    p->get_service()->smp->first_time_up) {
			val->num_start++;
			if (p->deps_down.empty()) {
				val->priority += p->get_priority();
			}
		}
		if ((p->get_sm_prov_state() & rm_repl_service::
		    prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY) != 0) {
			val->selected_as_primary = true;
		}
	}
	RM_DBG(("rm_sm %s: "
	    "evaluate combo %d returned %d, %d, %d, %d\n", s->id(), combo_num,
	    val->selected_as_primary, val->num_running,
	    val->num_start, val->priority));
}

bool
rm_state_machine::check_primary_dependency(
    rm_repl_service::prov_reg_impl *proposed_primary)
{
	bool retval = true;

	if (first_time_up) {
		if (!proposed_primary->is_spare() ||
		    (proposed_primary->get_sm_prov_state() & rm_repl_service::
		    prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN) != 0)
			retval = false;
	} else {
		if (!proposed_primary->is_valid_sec()) {
			retval = false;
			RM_DBG(("rm_sm %s: "
			    "check_primary_dependency(%p) failed state = %d\n",
			    s->id(), proposed_primary,
			    proposed_primary->get_repl_prov_state()));
		}
	}

	// Check dependencies to make sure everything below the proposed
	// primary is a primary.
	rm_repl_service::prov_reg_impl *pd;
	SList<rm_repl_service::prov_reg_impl>::ListIterator
		iter(proposed_primary->deps_down);
	for (; retval && ((pd = iter.get_current()) != NULL); iter.advance()) {
		if (!pd->is_primary()) {
			RM_DBG(("rm_sm %s: "
			    "check_primary_dependency(%p) failed for %p(%d)\n",
			    s->id(), proposed_primary, pd,
			    pd->get_repl_prov_state()));

			// Dependencies aren't satisfied
			retval = false;
			break;
		}
	}

	RM_DBG(("rm_sm %s: check_primary_dependency(%p) first_time = %d"
	    " returning %d\n",
	    s->id(), proposed_primary, first_time_up, retval));
	return (retval);
}

bool
rm_state_machine::check_secondary_dependency(
    rm_repl_service::prov_reg_impl *proposed_sec)
{
	bool retval = true;

	// Check dependencies to make sure everything below the proposed
	// secondary is a secondary.
	rm_repl_service::prov_reg_impl *pd;
	for (proposed_sec->deps_down.atfirst();
	    (pd = proposed_sec->deps_down.get_current()) != NULL;
	    proposed_sec->deps_down.advance()) {
		if (!pd->is_valid_sec()) {
			// Dependencies aren't satisfied
			RM_DBG(("rm_sm %s: "
			    "check_secondary_dependency(%p) failed for "
			    "%p(%d)\n", s->id(), proposed_sec, pd,
			    pd->get_repl_prov_state()));
			retval = false;
			break;
		}
	}
	return (retval);
}

// Find a spare to add as a secondary
rm_repl_service::prov_reg_impl *
rm_state_machine::find_sec_to_add()
{
	rm_repl_service::prov_reg_impl *p = find_best_spare();

	if ((p != NULL) &&
	    (!max_secondaries_active() || (p->get_sm_prov_state() &
	    rm_repl_service::prov_reg_impl::SM_SPARE_TO_MAKE_SEC))) {
		RM_DBG(("rm_sm %s: find_sec_to_add found %p\n", s->id(), p));
		return (p);
	}
	RM_DBG(("rm_sm %s: find_sec_to_add found no secondaries.\n",
	    s->id()));
	return (NULL);
}

// Find the best spare for use as a primary or secondary
// Incomplete for now since we don't consider the impact on dependencies above.
// Check dependencies to make sure everything below the new secondary is a
// secondary.  Return NULL if nothing found.
rm_repl_service::prov_reg_impl *
rm_state_machine::find_best_spare()
{
	rm_repl_service::prov_reg_impl *p, *found_p;
	p = found_p = NULL;

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; ((p = rp_iter.get_current()) != NULL); rp_iter.advance()) {
		if (!p->is_spare())
			continue;
		uint_t smp_state = p->get_sm_prov_state();
		// If this spare has already been chosen as primary, is being
		// shutdown or has died then skip it.
		if ((p == cur_primary_p) || (smp_state & (rm_repl_service::
		    prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN|
		    rm_repl_service::prov_reg_impl::SM_FAILED))) {
			continue;
		}

		// If it has already failed to become a secondary, skip it.
		if (smp_state & rm_repl_service::prov_reg_impl::
		    SM_FAILED_SECONDARY) {
			continue;
		}

		// If the providers it depends on are primary, then we must be
		// looking for a primary and this is the best one. Unless it
		// has already rejected an attempt to become primary.
		if (!p->deps_down.empty() && ((smp_state & rm_repl_service::
		    prov_reg_impl::SM_FAILED_PRIMARY) == 0) &&
		    check_primary_dependency(p)) {
			CL_PANIC(first_time_up);
			return (p);
		}
		// See if its dependencies allow it to be a secondary.
		// If not, then it is useless.
		if (!check_secondary_dependency(p)) {
			continue;
		}
		// If it was requested to be a secondary, use it.
		if (smp_state & rm_repl_service::
		    prov_reg_impl::SM_SPARE_TO_MAKE_SEC) {
			return (p);
		}

		if (!found_p || p->has_better_overall_pri_than(found_p)) {
			found_p = p;
		}
	}

	return (found_p);
}

//
// check_state - return true if this state machine is in the desired state.
// The argument "wanted_states" represents a collection of states,
// where each bit in the word represents a state according to the
// ordinal position in the enum sm_service_state.
//
bool
rm_state_machine::check_state(int wanted_states)
{
	RM_DBG(("rm_sm %s: check_state %x curr = %x (result = %x)\n",
	    s->id(), wanted_states, (1 << current_state),
	    ((1 << current_state) & wanted_states)));

	return (((1 << current_state) & wanted_states) != 0);
}

//
// check_other_states - returns false if any state machine
// in the list of "others" is not in one of the state machine
// "wanted_states".
//
bool
rm_state_machine::check_other_states(SList<rm_repl_service> *others,
    int wanted_states)
{
	rm_repl_service *servp;

	for (others->atfirst(); (servp = others->get_current()) != NULL;
	    others->advance()) {
		// Check if the service is in the right state.
		if (!servp->smp->check_state(wanted_states)) {
			return (false);
		}
	}
	return (true);
}

//
// wait_for_others - wait until all of the services specified in "others"
// are in one of the "wanted_states".
//
void
rm_state_machine::wait_for_others(SList<rm_repl_service> *others,
    int wanted_states)
{
	while (!check_other_states(others, wanted_states)) {
		cv.wait(&state_mutex);
	}
}

//
// We have made a state change and want to notify the threads for other
// interested services.  The interested parties are dependencies up and down.
// The idea here is to minimize the number of other threads which are woken up
// on each state change by just signaling those that may care.
// Note that this may not be worth the effort - we could consider just using a
// single condvar with a broadcast.
//
void
rm_state_machine::broadcast_state_change()
{
	CL_PANIC(state_mutex.lock_held());

	rm_repl_service *servp;
	SList<rm_repl_service>::ListIterator up_iter(s->deps_up);
	while ((servp = up_iter.get_current()) != NULL) {
		servp->smp->cv.signal();
		up_iter.advance();
	}
	SList<rm_repl_service>::ListIterator down_iter(s->deps_down);
	while ((servp = down_iter.get_current()) != NULL) {
		servp->smp->cv.signal();
		down_iter.advance();
	}
}

// ------------------------------------------------------------
// recovery methods

//
// Make RMAs consistent with specified state.
//
void
rm_state_machine::make_consistent_iterate_over(
    const replica_int::rma_service_state state)
{
	void (replica_int::rma_reconf::*mfn)(
	    replica::service_num_t, Environment &) = NULL;
	char *fn_name = NULL;

	state_mutex.unlock();

	switch (state) {
	case replica_int::RMA_SERVICE_FREEZE:
		mfn = &replica_int::rma_reconf::freeze_service_invos;
		fn_name = "freeze_service_invos";
		break;
	case replica_int::RMA_SERVICE_UNFREEZE:
		mfn = &replica_int::rma_reconf::unfreeze_service_invos;
		fn_name = "unfreeze_service_invos";
		break;
	case replica_int::RMA_SERVICE_DISCONNECT:
		mfn = &replica_int::rma_reconf::disconnect_clients;
		fn_name = "disconnect_clients";
		break;
	case replica_int::RMA_SERVICE_MARK_DOWN:
		mfn = &replica_int::rma_reconf::mark_down;
		fn_name = "mark_down";
		break;
	case replica_int::RMA_SERVICE_ADD:
		// Add service has different arguments, special handle it.
		mfn = NULL;
		fn_name = "add_service";
		break;
	case replica_int::RMA_SERVICE_SHUTDOWN:
		mfn = &replica_int::rma_reconf::shutdown_service;
		fn_name = "shutdown_service";
		break;
	default:
		CL_PANIC(0);
	}

	rm_repl_service::service_recovery_data	*data_p;

	for (get_states_list.atfirst();
	    (data_p = get_states_list.get_current()) != NULL;
	    get_states_list.advance()) {
		if (data_p->get_last_state() != state) {
			Environment	e;
			if (mfn != NULL) {
				(data_p->get_rcf()->*mfn)(
				    s->get_number(), e);
			} else {
				CL_PANIC(os::strcmp("add_service", fn_name)
				    == 0);
				data_p->get_rcf()->add_service(
				    s->get_number(), s->get_name(), e);
			}
			if (e.exception()) {
				RM_DBG(("rm_sm %s: got exception on "
				    "rma_reconf `%p'\n,"
				    "current primary `%p'\n",
				    s->id(), data_p->get_rcf(),
				    cur_primary_p));
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception()) != NULL) {
					//
					// Ignore. We will clean it up later
					// when we use the regular iterate_over.
					//
					e.clear();
				} else {
					CL_PANIC(0);
				}
			} else {
				data_p->set_last_state(state);
			}
		}
	}
	state_mutex.lock();

	RECONF_DBG(("rm_sm %s: returned from %s\n", s->id(), fn_name));
}

//
// It's okay to traverse the list of providers without holding the
// state lock, since we'll synchronize with dependencies before
// leaving SERVICE_RECOVERING.
//
void
rm_state_machine::query_providers(
	SList<rm_repl_service::provider_recovery_data> *prd_list)
{
	rm_repl_service::prov_reg_impl		*p = NULL;

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; ((p = rp_iter.get_current()) != NULL); rp_iter.advance()) {
		Environment				e;
		replica_int::rma_repl_prov_state	state;
		replica_int::repl_prov_exception	rp_exception;

		state_mutex.unlock();
		p->get_rma_repl_prov_ptr()->get_state(state, rp_exception, e);
		state_mutex.lock();
		if (e.exception()) {
			RM_DBG(("rm_sm: %s query_providers \"%s\" is dead\n",
			    s->id(), p->get_rp_desc()));

			const bool rp_down = inval_rp_state_exception(e);
			if (rp_down) {
				p->mark_down();
			}
		} else {
			assign_provider_state(p, state, rp_exception);

			rm_repl_service::provider_recovery_data *data_p
			    = new rm_repl_service::provider_recovery_data(
			    p, state, rp_exception);

			prd_list->append(data_p);
		}
	} // for
	RM_DBG(("rm_sm: %s recovery exiting "
	    "query_providers(), prd_list count `%d'\n", s->id(),
	    prd_list->count()));
}

//
// Use provider recovery data state to assign one of
//	AS_PRIMARY
//	AS_SECONDARY
//	AS_SPARE
//	AS_DOWN
// to prov_reg_impl object.
//
void
rm_state_machine::assign_provider_state(
	rm_repl_service::prov_reg_impl	*rp,
	const replica_int::rma_repl_prov_state state,
	const replica_int::repl_prov_exception rpe)
{
	Environment e;

	switch (state) {
	case (replica_int::RMA_CONSTRUCTED):
		CL_PANIC(rp->is_spare());
		break;
	case (replica_int::RMA_ADD_SECONDARY):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);

		// XXX is state count increment necessary in recovery?
		s->incr_state_count();
		break;
	case (replica_int::RMA_COMMIT_SECONDARY):
		rp->set_repl_prov_state(replica::AS_SECONDARY);

		//
		// check if it still receives ckpt from the primary. It is
		// possible that this secondary has been removed from primary's
		// secondary list but yet to be converted to spare. We
		// should set proper flag if that's the case.
		//
		bool is_ckpted;
		is_ckpted = rp->get_rma_repl_prov_ptr()->is_receiving_ckpt(e);
		if (!e.exception()) {
			if (!is_ckpted) {
				//
				// This secondary is invalid, needing to be
				// converted to a spare.
				//
				rp->set_sm_prov_state(
				    rm_repl_service::prov_reg_impl::
				    SM_SEC_TO_BE_SPARED);
			}
		} else {
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				//
				// No need to do anything, the node is dead
				// anyway.
				//
				e.clear();
			} else {
				CL_PANIC(0);
			}
		}
		break;
	case (replica_int::RMA_REMOVE_SECONDARY):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		break;
	case (replica_int::RMA_FREEZE_PRIMARY):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		break;
	case (replica_int::RMA_UNFREEZE_PRIMARY):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		break;
	case (replica_int::RMA_BECOME_PRIMARY_PREPARE):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_SECONDARY);
		break;
	case (replica_int::RMA_BECOME_PRIMARY):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		break;
	case (replica_int::RMA_BECOME_SECONDARY):
		cur_primary_p = rp;
		cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);
		break;
	case (replica_int::RMA_BECOME_SECONDARY_WRAPUP):
		rp->set_repl_prov_state(replica::AS_SECONDARY);
		break;
	case (replica_int::RMA_BECOME_SPARE):
		//
		// become_spare() is normally called on secondary.
		// However, we'll also call it from recovery to
		// make sure the provider didn't get a partial
		// dump during add_secondary().
		//
		CL_PANIC(rp->is_spare());
		break;
	case (replica_int::RMA_SHUTDOWN):
		if (rpe == replica_int::PROV_SERVICE_BUSY) {
			//
			// This is the current primary, since only that can
			// raise an exception.
			// The shutdown that was performed may be part of
			// an uncompleted shutdown request, or of an earlier
			// one. We let the shutdown retry sort things out.
			//
			cur_primary_p = rp;
			cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);

		} else if ((s->flags & rm_repl_service::SHUTDOWN_INTENTION) !=
		    0 && s->shutdown_primary == rp) {
			//
			// A shutdown was in progress with this provider as
			// primary. It didn't get an exception, so it
			// succeeded.
			//
			cur_primary_p = rp;
			cur_primary_p->set_repl_prov_state(replica::AS_PRIMARY);

			// Marks it deleted.
			s->mark_shutdown_succeeded();
		} else {
			// Shutdown was on a spare.
			rp->mark_down();
		}
		break;
	default:
		CL_PANIC(0);
		break;
	}

	RM_DBG(("rm_sm %s: "
	    "assign_provider_state on reg_impl`%p' \"%s\":  state returned "
	    "from query is `%d', assigned state is `%d'\n",
	    s->id(), rp, rp->get_rp_desc(), state, rp->get_repl_prov_state()));
}

// Return the primary's last data.
rm_repl_service::provider_recovery_data *
rm_state_machine::last_primary_data(
    SList<rm_repl_service::provider_recovery_data> *prd_list)
{
	rm_repl_service::provider_recovery_data *data_p;

	for (prd_list->atfirst();
	    ((data_p = prd_list->get_current()) != NULL);
	    prd_list->advance()) {
		if (data_p->get_rp() == cur_primary_p)
			return (data_p);
	}
	// Not Reached
	CL_PANIC(0);
	return (NULL);
}

//
// Given two states among the rma_reconf objects, figure out which state
// to move to, and call the rma_reconf method on the RMAs that need
// to move to that state. It is OK for both states to be the same.
// Returns the next state machine state.
//
// This is only called when we don't know the current primary. If there is
// no primary, then either the service is either starting or stopping or the
// primary died. We only need to worry about RMA states that involve starting
// or stopping. If the RMA states indicate that the primary was up when
// the primary RM died, then we simply return PRIMARY_DEAD state. Any partial
// RMA iterate over operations do not need to be recovered.
//
// Legal state combinations:
//
rm_state_machine::sm_service_state
rm_state_machine::make_rmas_consistent(
    const replica_int::rma_service_state	state_a,
    const replica_int::rma_service_state	state_b)
{
	CL_PANIC(!get_states_list.empty());

	RM_DBG(("rm_sm: %s recovery entering "
	    "make_rmas_consistent(), state_a `%d', state_b `%d'\n",
	    s->id(), state_a, state_b));

	if (state_a == replica_int::RMA_SERVICE_SHUTDOWN ||
	    state_b == replica_int::RMA_SERVICE_SHUTDOWN) {
		CL_PANIC(s->is_deleted());
		first_time_up = false;
		make_consistent_iterate_over(
		    replica_int::RMA_SERVICE_SHUTDOWN);
		return (SERVICE_DOWN);
	}
	if (state_a == replica_int::RMA_SERVICE_MARK_DOWN ||
	    state_b == replica_int::RMA_SERVICE_MARK_DOWN) {
		make_consistent_iterate_over(
		    replica_int::RMA_SERVICE_MARK_DOWN);
		first_time_up = false;
		return (SERVICE_DOWN);
	}
	if (state_a == replica_int::RMA_SERVICE_ADD ||
	    state_b == replica_int::RMA_SERVICE_ADD) {
		if (state_a == replica_int::RMA_SERVICE_CONNECT ||
		    state_b == replica_int::RMA_SERVICE_CONNECT) {
			first_time_up = false;
			return (PRIMARY_DEAD);
		} else {
			make_consistent_iterate_over(
			    replica_int::RMA_SERVICE_ADD);
			first_time_up = true;
			return (SERVICE_UNINIT);
		}
	}

	first_time_up = false;
	return (PRIMARY_DEAD);
}

rm_state_machine::sm_service_state
rm_state_machine::get_consistent_rma_state()
{
	CL_PANIC(!get_states_list.empty());
	rm_repl_service::service_recovery_data	*data_p = NULL;
	replica_int::rma_service_state		state_a;
	replica_int::rma_service_state		state_b;
	replica_int::rma_service_state		this_state;
	bool	a_valid = false;
	bool	b_valid = false;

	RM_DBG(("rm_sm: %s get_consistent_rma_state\n", s->id()));

	for (get_states_list.atfirst();
	    ((data_p = get_states_list.get_current()) != NULL);
	    get_states_list.advance()) {
		this_state = data_p->get_last_state();
		if (!a_valid) {
			a_valid = true;
			state_a = this_state;
		} else if (!b_valid) {
			if (this_state != state_a) { //lint !e644
				b_valid = true;
				state_b = this_state;
			}
		}
		// At most two states...
		CL_PANIC(state_a == this_state ||
		    state_b == this_state); //lint !e644
	}
	if (!b_valid) {
		if (a_valid) {
			state_b = state_a;
		} else {
			state_a = state_b = replica_int::RMA_SERVICE_NONE;
		}
	}

	if (state_a == replica_int::RMA_SERVICE_NONE &&
	    state_b == replica_int::RMA_SERVICE_NONE) {
		//
		// No RMAs have heard of the service.  This means that either
		// the service was just registered and no RMAs were notified or
		// the service was just shutdown and all RMAs were shutdown.
		//
		return ((s->is_deleted()) ? SERVICE_DOWN : SERVICE_REGISTERED);
	}
	return (make_rmas_consistent(state_a, state_b));
}

//
// Return the state service_recovery() should return.
// First we look at the state of the RMAs and then that of the providers.
//
rm_state_machine::sm_service_state
rm_state_machine::get_next_state(
    SList<rm_repl_service::provider_recovery_data> *prd_list)
{
	sm_service_state	next_state = SERVICE_UNINIT;

	RM_DBG(("rm_sm: %s recovery entering get_next_state()\n", s->id()));

	//
	// Let the services that this service depends upon begin the
	// recovery process first and wait until it has finished.
	// This is required so we can resume recovery for this service
	// and make correct decisions based upon a valid state of service
	// we depend on.
	//
	if (!s->deps_down.empty()) {
		wait_for_others(&s->deps_down, SYNC_NOT_RECOVERING);
	}
	if (cur_primary_p == NULL) {
		next_state = get_consistent_rma_state();

		if (next_state == PRIMARY_DEAD && !s->deps_down.empty() &&
		    !check_other_states(&s->deps_down, SYNC_NOT_WAY_UP)) {
			//
			// There is a need to avoid race conditions with
			// services that we depend on. In this case,
			// a service that we depend on is recovering,
			// so we adjust our state accordingly.
			//
			next_state = NO_PRIMARY;
		}
	} else {
		first_time_up = false;
		rm_repl_service::provider_recovery_data *lastprimarydata;
		replica_int::rma_repl_prov_state lastprimarystate;
		replica_int::repl_prov_exception lastprimaryexception;

		//
		// Retrieve last primary's saved data in order to recover the
		// next state of the state machine.
		//
		lastprimarydata = last_primary_data(prd_list);
		ASSERT(lastprimarydata != NULL);

		lastprimarystate = lastprimarydata->get_last_state();
		lastprimaryexception = lastprimarydata->get_rp_exception();

		RM_DBG(("rm_sm %s: get_next_state() lastprimarystate is %d, "
		    "lastprimaryexception is %d.\n", s->id(), lastprimarystate,
		    lastprimaryexception));

		switch (lastprimarystate) {
		case replica_int::RMA_ADD_SECONDARY:
			make_consistent_iterate_over(
			    replica_int::RMA_SERVICE_FREEZE);
			next_state = SERVICE_FROZEN_CLEAN;
			//
			// SERVICE_FROZEN_CLEAN will transition to
			// frozen_for_upgrade if necessary.
			//
			break;

		case replica_int::RMA_REMOVE_SECONDARY:
			//
			// The state indicates that the primary had
			// finished removing a secondary. The
			// remove_secondaries operation would have
			// taken the service to frozen_for_upgrade
			// state. That means the service is still
			// frozen and should be directly taken to that
			// state.
			//
			if (must_freeze_for_upgrade()) {
				next_state = FROZEN_FOR_UPGRADE;
			} else {
				next_state = SERVICE_UP;
			}
			break;

		case replica_int::RMA_FREEZE_PRIMARY:
			make_consistent_iterate_over(
			    replica_int::RMA_SERVICE_FREEZE);
			next_state = SERVICE_FROZEN_CLEAN;

			break;

		case replica_int::RMA_UNFREEZE_PRIMARY:
			//
			// It's possible that some of the rmas has received
			// freeze_service_prepare. This step will call
			// unfreeze_service on all those rmas and make
			// the view consistent.
			//
			make_consistent_iterate_over(
			    replica_int::RMA_SERVICE_UNFREEZE);

			next_state = SERVICE_UP;
			break;

		case replica_int::RMA_BECOME_PRIMARY_PREPARE:
			//
			// In this case the service may be recovering from a
			// primary failure and may have just selected a new
			// primary.  Note that RMA_SERVICE_CONNECT occurs right
			// after a become_primary_prepare, and we have no
			// guarantee that it was called with this primary or a
			// failed one. For that reason, we return
			// PRIMARY_PREPARED state even if we have connected.
			// The RMA will either recognize that this is the same
			// as the primary it already has and do nothing, or it
			// will not have a primary (since the other would have
			// died and been discarded) and it will do the
			// connect.
			//
			next_state = PRIMARY_PREPARED;
			break;

		case replica_int::RMA_BECOME_PRIMARY:
			if (lastprimaryexception ==
			    replica_int::PROV_REPL_PROV_FAILED) {
				//
				// If there is exception stored, the cur_primary
				// must have failed to become a primary when
				// the last RM primary was alive
				//
				// We need to mark it as a failed primary
				// and move to SERVICE_FROZEN_CLEAN to convert
				// it to a secondary
				//
				cur_primary_p->set_sm_prov_state(
				    rm_repl_service::prov_reg_impl::
				    SM_PRIMARY_TO_MAKE_SECONDARY|
				    rm_repl_service::prov_reg_impl::
				    SM_FAILED_PRIMARY);
				next_state = SERVICE_FROZEN_CLEAN;
			} else {
				CL_PANIC(lastprimaryexception ==
				    replica_int::PROV_NONE);
				if (must_freeze_for_upgrade()) {
					next_state = FROZEN_FOR_UPGRADE;
				} else {
					next_state = SERVICE_FROZEN_DIRTY;
				}
			}
			break;

		case replica_int::RMA_BECOME_SECONDARY:
			if (lastprimaryexception ==
			    replica_int::PROV_REPL_PROV_FAILED) {
				//
				// The become_secondary() call on the old
				// RM primary got replica::repl_prov_failed
				// exception. We need to mark the cur_primary
				// a failed one.
				//
				cur_primary_p->set_sm_prov_state(
				    rm_repl_service::prov_reg_impl::
				    SM_PRIMARY_TO_MAKE_SECONDARY |
				    rm_repl_service::prov_reg_impl::
				    SM_FAILED_PRIMARY);
			} else if (lastprimaryexception ==
			    replica_int::PROV_BECOME_SECONDARY_FAILED) {
				//
				// The primary had refused to become a secondary
				// in the become_secondary() call on the old RM
				// primary so it has to remain a primary. We
				// need to set it such that the bottom service
				// will choose the same replica as the primary
				// again and all the top level services will
				// follow.
				//
				repl_service_list *tree =
				    s->get_component_tree();
				//
				// Find one service that doesn't depend on
				// anyone and make sure that it will choose
				// the primary we desire.
				//
				SList<rm_repl_service>::ListIterator iter(tree);
				for (rm_repl_service *current_s = NULL;
				    (current_s = iter.get_current()) != NULL;
				    iter.advance()) {
					if (!current_s->deps_down.empty()) {
						// not a bottom service, skip
						continue;
					}

					// current_s is a bottom service

					// Clear other sm_prov_state first.
					SList<rm_repl_service::prov_reg_impl>::
					    ListIterator rp_iter(
					    current_s->repl_prov_list);
					rm_repl_service::prov_reg_impl *rp;
					for (rp = NULL; (rp =
					    rp_iter.get_current()) != NULL;
					    rp_iter.advance()) {
						rp->clear_sm_prov_state(
						    rm_repl_service::
						    prov_reg_impl::
						    SM_SEC_TO_MAKE_PRIMARY);
					}

					//
					// Make sure that the bottom service
					// will choose its current primary.
					//
					current_s->smp->cur_primary_p->
					    set_sm_prov_state(
					    rm_repl_service::prov_reg_impl::
					    SM_SEC_TO_MAKE_PRIMARY |
					    current_s->smp->cur_primary_p->
					    get_sm_prov_state());
				}

				delete tree;
			}

			make_consistent_iterate_over(
			    replica_int::RMA_SERVICE_DISCONNECT);
			next_state = DISCONNECTED;
			break;

		case replica_int::RMA_BECOME_SECONDARY_WRAPUP:
		case replica_int::RMA_BECOME_SPARE:
		case replica_int::RMA_CONSTRUCTED:
		case replica_int::RMA_COMMIT_SECONDARY:
			CL_PANIC(0);
			break;

		case replica_int::RMA_SHUTDOWN:
			//
			// Shutdowns are issued when the service is up.
			// We don't know how recent this shutdown is.
			// We could check if we had one outstandng, but we
			// can't tell if shutdown reported by the provider was
			// the last one issued by the old RM (since it might
			// have died before it sent the message).
			// If it returned without an exception, then we would
			// have already marked the service as deleted, and
			// wouldn't be here. In this case we assume that we got
			// a service_busy exception and allow the service to
			// stay up. If necessary, a shutdown retry will be
			// performed and will sort things out.
			//
			next_state = SERVICE_UP;
			break;

		default:
			CL_PANIC(0);
			break;
		}
	}

	RM_DBG(("rm_sm: %s recovery exiting "
	    "get_next_state(), next state `%s'\n", s->id(),
	    state_name(next_state)));

	return (next_state);
}

//
// Called during recovery to ensure that all spares are stateless
// (none has a partial dump from a primary that went down
// during add_secondaryi(), for example).
//
void
rm_state_machine::spare_all_spares()
{
	rm_repl_service::prov_reg_impl	*p		= NULL;

	SList<rm_repl_service::prov_reg_impl>::ListIterator
	    rp_iter(s->repl_prov_list);
	for (; ((p = rp_iter.get_current()) != NULL); rp_iter.advance()) {
		if (p->is_spare()) {
			Environment	e;
			p->get_rma_repl_prov_ptr()->become_spare(e);
			if (e.exception()) {
				const bool rp_down
				    = inval_rp_state_exception(e);
				if (rp_down) {
					p->mark_down();
				}
			}
		}
	}
}
