//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dependency_mgr_impl.cc	1.19	08/05/20 SMI"

//
// dependency_mgr_impl.cc implements the dependency manager.
//

#include <sys/os.h>

#include <repl/repl_mgr/dependency_mgr_impl.h>
#include <repl/repl_mgr/rm_repl_service.h>

//
// constructor -- creates a reference to name server object and
// binds the impl object to the name server.
//
dependency_mgr_impl::dependency_mgr_impl()
#ifdef DEBUG
	: depth(-1)
#endif
{
	dependency_table.id("Dependencytab");
}

dependency_mgr_impl::~dependency_mgr_impl()
{
	// XXX In order to support deleting the dependency manager, we'd
	// have to clear out the hash table, etc.
}

// Look up an item and create a dependency_info if none exists
// CSTYLED
dependency_info * /*lint !e1038*/
dependency_mgr_impl::lookup_create(rm_repl_service *key, bool create_it)
{
	ASSERT(mutex.lock_held());
	dependency_info *di = dependency_table.lookup((uint32_t *)&key);
	if (di == NULL && create_it) {
		// Create only if flag is set.
		di = new dependency_info();
		di->key = key;
		(void) dependency_table.add(di, (uint32_t *)&(di->key));
	}
	return (di);
}

//
// Indicate that "item" depends on "depends_on_item"
//
void
dependency_mgr_impl::add_dependency(rm_repl_service *item,
    rm_repl_service *depends_on_item, Environment &_environment)
{
	mutex.lock();
	dependency_info *item1 = lookup_create(item, true);
	dependency_info *item2 = lookup_create(depends_on_item, true);
	// Check for cycles
	repl_service_list il;
	get_more_relatives(item1, &il, true, false, true);
	if (il.exists(depends_on_item)) {
		_environment.exception(new replica_int::cycle());
		mutex.unlock();
		return;
	}
	// Look through the lists.
	if (item1->this_depends_on.exists(item2)) {
		_environment.exception(new replica_int::exists());
		mutex.unlock();
		return;
	}
	item1->this_depends_on.append(item2);
	item2->depend_on_this.append(item1);
	sanity();
	mutex.unlock();
}

// Remove a dependency from the table.
void
dependency_mgr_impl::remove_dependency(rm_repl_service *item,
    rm_repl_service *depends_on_item, Environment &_environment)
{
	int count = 0;
	mutex.lock();
	dependency_info *item1 = lookup_create(item, false);
	dependency_info *item2 = lookup_create(depends_on_item, false);
	if (item1 == NULL || item2 == NULL) {
		// Item doesn't exist
		_environment.exception(new replica_int::doesnt_exist());
		mutex.unlock();
		return;
	}
	// Look through the lists.
	if (item1->this_depends_on.erase(item2)) {
		count++;
	}

	if (item2->this_depends_on.erase(item1)) {
		count++;
	}

	// Verify that delete operated correctly
	if (count == 0) {
		// Dependency doesn't exist
		_environment.exception(new replica_int::doesnt_exist());
	} else if (count != 2) {
		os::panic("Multiple entries in dependency table\n");
	}
	mutex.unlock();
}

//
// Generate a topologically sorted list.  If up is true (false), the list
// consists of the transitive closure of everything that depends on
// the specified item (that the specified item depends on), with
// the topmost (bottommost) element first.
//
void
dependency_mgr_impl::topological(dependency_info *item,
    repl_service_list *ilp, bool up)
{
	DList<dependency_info>::ListIterator li(up ? item->depend_on_this :
		item->this_depends_on);
	dependency_info *cur;

	for (; (cur = li.get_current()) != NULL; li.advance()) {
		if (!ilp->exists(cur->key)) {
			topological(cur, ilp, up);
		}
		if (!ilp->exists(cur->key)) {
			ilp->append(cur->key);
		}
	}
}

// Get the ancestors/dependents of the specified item, adding these to the
// list of items.  Up and down specify if we want ancestors and/or dependents.
// Recurse specifies if the routine should execute recursively; otherwise
// it will return just direct ancestors/dependents.
void
dependency_mgr_impl::get_more_relatives(dependency_info *item,
    repl_service_list *ilp, bool up, bool down,
    bool recurse)
{
	dependency_info *cur;
	if (up) {
		DList<dependency_info>::ListIterator li(item->depend_on_this);
		// Iterate through all things that depend on us.
		for (; (cur = li.get_current()) != NULL; li.advance()) {
			// If the item is new add it to the list
			if (!ilp->exists(cur->key)) {
				ilp->append(cur->key);
				if (recurse) {
					get_more_relatives(cur, ilp, up,
						down, true);
				}
			}
		}
	}
	if (down) {
		DList<dependency_info>::ListIterator li(item->this_depends_on);
		for (; (cur = li.get_current()) != NULL; li.advance()) {
			if (!ilp->exists(cur->key)) {
				ilp->append(cur->key);
				if (recurse) {
					get_more_relatives(cur, ilp, up,
						down, true);
				}
			}
		}
	}
}

// Lookup part of the dependency tree
repl_service_list *
dependency_mgr_impl::lookup(rm_repl_service *item, enum dependency_mode mode)
{
	repl_service_list *ilp;

	mutex.lock();
	dependency_info *item_info = lookup_create(item, false);
	ilp = new repl_service_list;
	if (item_info != NULL) {
		switch (mode) {
			case this_depends_on:
				topological(item_info, ilp, false);
				break;
			case this_directly_depends_on:
				get_more_relatives(item_info, ilp, false,
					true, false);
				break;
			case depend_on_this:
				topological(item_info, ilp, true);
				break;
			case directly_depend_on_this:
				get_more_relatives(item_info, ilp, true,
					false, false);
				break;
			case component:
				ilp->append(item);
				get_more_relatives(item_info, ilp, true, true,
					true);
				break;
			default:
				os::panic("Bad dependency lookup mode\n");
				break;
		}
	} else {
		// Return empty list
	}
	mutex.unlock();
	return (ilp);
}

// Delete all dependencies associated with item and remove it from
// the hash table.
void
dependency_mgr_impl::remove_dependencies(rm_repl_service *key)
{
	mutex.lock();
	dependency_info *item1 = lookup_create(key, false);
	if (item1 == NULL) {
		// Nothing to delete
		mutex.unlock();
		return;
	}

	// Look through the lists.

	// For each entry item2 in iter, erase item1 from item2's list
	// and erase item2 from item1's list.
	dependency_info *item2;

	DList<dependency_info>::ListIterator iter1(item1->this_depends_on);
	for (; (item2 = iter1.get_current()) != NULL; ) {
		(void) item2->depend_on_this.erase(item1);
		// Advance before deleting so the iterator won't get messed up.
		iter1.advance();
		(void) item1->this_depends_on.erase(item2);
	}

	// Do same thing for depend_on_this
	DList<dependency_info>::ListIterator iter2(item1->depend_on_this);
	for (; (item2 = iter2.get_current()) != NULL; ) {
		(void) item2->this_depends_on.erase(item1);
		// Advance before deleting so the iterator won't get messed up.
		iter2.advance();
		(void) item1->depend_on_this.erase(item2);
	}

	// Remove entry from hash table
	(void) dependency_table.remove((uint32_t *)&key);

	// Delete the entry
	delete item1;

	mutex.unlock();
}

//
// Utility function to dump the dependency table.
//
void
dependency_mgr_impl::dump()
{
	mutex.lock();
	hash_table_t<dependency_info, KEY_SIZE>::HashIterator
		iter(&dependency_table);
	dependency_info *item;
	for (; (item = iter.get_current()) != NULL; iter.advance()) {
		dependency_info *cur;
		DList<dependency_info>::ListIterator li(item->this_depends_on);
		for (; (cur = li.get_current()) != NULL; li.advance()) {
			os::printf("%x depends on %x\n", item->key, cur->key);
		}
		DList<dependency_info>::ListIterator li2(item->depend_on_this);
		for (; (cur = li2.get_current()) != NULL; li2.advance()) {
			os::printf("%x is depended on by %x\n", item->key,
				cur->key);
		}
	}
	mutex.unlock();
}

//
// Utility function to dump a list.
//
void
dependency_mgr_impl::dump(repl_service_list *il)
{
	repl_service_list::ListIterator li(il);
	rm_repl_service *ptr;
	for (; (ptr = li.get_current()) != NULL; li.advance()) {
		os::printf("%p\n", ptr);
	}
}

#ifdef DEBUG
//
// Check the sanity of the dependency manager structures.  In particular,
// make sure there are no cycles
//
void
dependency_mgr_impl::sanity()
{
	ASSERT(mutex.lock_held());
	hash_table_t<dependency_info, KEY_SIZE>::HashIterator
		iter(&dependency_table);
	dependency_info *item;

	depth = 0;

	// Clear the depth fields
	for (; (item = iter.get_current()) != NULL; iter.advance()) {
		item->depth = -1;
	}

	// Assign depth ordering
	hash_table_t<dependency_info, KEY_SIZE>::HashIterator
		iter2(&dependency_table);
	for (; (item = iter2.get_current()) != NULL; iter2.advance()) {
		if (item->depth == -1) {
			dfs(item);
		}
	}

	// Check for cycles
	hash_table_t<dependency_info, KEY_SIZE>::HashIterator
		iter3(&dependency_table);
	for (; (item = iter3.get_current()) != NULL; iter3.advance()) {
		DList<dependency_info>::ListIterator li(item->this_depends_on);
		dependency_info *cur;
		for (; (cur = li.get_current()) != NULL; li.advance()) {
			ASSERT(item->depth > cur->depth);
		}
	}
}

//
// Do a depth first search starting at item to find cycles
// The depth field is set to a sequential value.
//
void
dependency_mgr_impl::dfs(dependency_info *item)
{
	// Loop through everything this depends on and do dfs if not visisted.
	DList<dependency_info>::ListIterator li(item->this_depends_on);
	dependency_info *cur;
	ASSERT(item->depth == -1);
	item->depth = 0;	// Visited, but no depth assigned
	for (; (cur = li.get_current()) != NULL; li.advance()) {
		if (cur->depth == -1) {
			dfs(cur);
		}
	}
	ASSERT(item->depth == 0);
	item->depth = ++depth;
}

#endif
