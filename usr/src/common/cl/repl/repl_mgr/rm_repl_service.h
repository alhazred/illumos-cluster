/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_REPL_SERVICE_H
#define	_RM_REPL_SERVICE_H

#pragma ident	"@(#)rm_repl_service.h	1.88	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

//
// rm_repl_service.h contains the class definition for the
// (non-Corba) rm_repl_service class.
//

#include <sys/os.h>
#include <sys/list_def.h>

#include <orb/object/adapter.h>
#include <h/replica_int.h>
#include <h/repl_rm.h>

#include <repl/service/replica_tmpl.h>
#include <repl/repl_mgr/service_admin_impl.h>
#include <repl/repl_mgr/repl_mgr_debug.h>

#include <repl/repl_mgr/repl_mgr_prov_impl.h>

#define	MAX_SECONDARIES	(NODEID_MAX - 1)

class rm_state_machine;

//
// rm_repl_service class contains all the information the RM has
// about a service:  its state, list of all repl_prov registration
// objects (and everything the RM knows about each of their
// corresponding repl_provs).
//
// rm_repl_service methods are called by service_admin interface
// routines to accomplish all operations on the service and its
// repl_provs.
//
// This class also implements reconfiguration and switchover.
//
// Note that it is reference-counted:  count is incremented for
// each service_admin object creation, and for each repl_prov
// registration object creation.
//

class repl_service_list;
class prov_reg_list;

class rm_repl_service {
	friend class rm_state_machine;
public:

	//
	// status for methods that do invocations on rma_repl_provs
	//
	enum op_status {
		SERVICE_OP_SUCCESS,
		SERVICE_OP_FAILURE
	};

	//
	// contains all the information RM stores about each
	// rma_repl_prov.  This object is created during repl_prov
	// registration, and a reference is returned to the caller.
	// _unreferenced is executed on the RM if the registered
	// repl_prov fail.
	//
	class prov_reg_impl :
	    public mc_replica_of<replica::repl_prov_reg> {
	public:
		//
		// rcf_state_private is used by state machine to
		// identify secondaries to remove, etc.
		//
		enum rcf_state_private {
			SM_PRIMARY_TO_MAKE_SECONDARY	= 0x001,
			SM_SEC_TO_MAKE_PRIMARY		= 0x002,
			SM_SPARE_TO_MAKE_SEC		= 0x004,
			SM_SEC_NEED_COMMIT		= 0x008,
			SM_SEC_TO_BE_REMOVED		= 0x010,
			SM_SEC_TO_BE_SPARED		= 0x020,
			SM_SPARE_TO_BE_SHUTDOWN		= 0x040,
			SM_SPARE_TO_ADD			= 0x080,
			SM_FAILED			= 0x100,
			SM_FAILED_PRIMARY		= 0x200,
			SM_FAILED_SECONDARY		= 0x400
		};

		~prov_reg_impl();

		// primary constructor
		prov_reg_impl(repl_mgr_prov_impl *rm_prov,
		    const replica_int::rma_repl_prov_ptr r_prov_ptr,
		    const char *desc,
		    const replica::checkpoint_ptr chkpt,
		    const uint_t pri,
		    const replica::repl_prov_state rs,
		    rm_repl_service *s);

		// secondary constructor
		prov_reg_impl(replica::repl_prov_reg_ptr reg_ptr,
		    const replica_int::rma_repl_prov_ptr r_prov_ptr,
		    const char *desc,
		    const replica::checkpoint_ptr chkpt,
		    const uint_t priority,
		    const replica::repl_prov_state rs,
		    rm_repl_service *s);


		void	_unreferenced(unref_t);

		replica::repl_prov_reg_ptr	get_new_reference();

		void	set_repl_prov_state(
		    const replica::repl_prov_state new_state);

		void	set_sm_prov_state(const uint_t new_state);

		void	clear_sm_prov_state(const uint_t clear_states);

		const char	*get_rp_desc() const;

		rm_repl_service	*get_service() const;

		replica_int::rma_repl_prov_ptr
		    get_rma_repl_prov_ptr() const;

		uint32_t	get_priority();

		void	set_priority(uint32_t);

		//
		// Compare our overall priority with other providers.
		// Note the overall priority is determined by only the
		// total number of dependent services but also the priority
		// value associated with the provider.
		//
		bool	has_better_overall_pri_than(prov_reg_impl *);

		const replica::checkpoint_ptr	get_sec_chkpt() const;

		replica::repl_prov_state	get_repl_prov_state() const;

		uint_t	get_sm_prov_state() const;

		bool	is_primary() const;
		bool	is_secondary() const;
		bool	is_down() const;
		bool	is_spare() const;

		// Is this ready to become primary.
		bool	is_valid_sec() const;

		void	mark_down();

		// see if we can delete this object now.
		void	try_delete();

		//
		// Get the list of providers that directly or indirectly
		// depend on this provider
		//
		prov_reg_list	*get_deps_up_tree();
		//
		// Get the list of providers that this provider directly or
		// indirectly depends on.
		//
		prov_reg_list	*get_deps_down_tree();

		//
		// Helper function. Remove the dependency relationship between
		// this and the ones on the deps_up/deps_down list.
		//
		void	clear_deps_up();
		void	clear_deps_down();

		// Providers that directly depend on this.
		SList<prov_reg_impl>	deps_up;

		// Providers that this directly depends on.
		SList<prov_reg_impl>	deps_down;

		// Checkpoint accessor function.
		repl_rm::rm_ckpt_ptr	get_checkpoint();

	private:
		void	delete_repl_prov_on_sec();

		//
		// Helper function. Iterate through all the deps_up of this
		// and mark them AS_DOWN if they haven't been marked AS_DOWN.
		//
		void	mark_deps_up_tree_down();

		// Corba reference to repl_prov
		replica_int::rma_repl_prov_ptr 	r_prov;

		char	*r_prov_desc;

		// secondary interface (checkpoint_ptr) object
		replica::checkpoint_ptr	s_chkpt;

		// requested priority for this provider
		uint32_t	priority;

		//
		// repl_prov current external state --
		// AS_SECONDARY, AS_PRIMARY, AS_DOWN, etc.
		//
		replica::repl_prov_state	r_prov_state;

		//
		// internal state --
		// SM_SEC_TO_BE_REMOVED, etc.
		//
		uint_t	sm_prov_state;

		// svc needed for _unreferenced()
		rm_repl_service		*svc;

		bool	got_unref;

		// Disallow
		prov_reg_impl();
		prov_reg_impl(const prov_reg_impl &);
		prov_reg_impl &operator = (const prov_reg_impl &);
	}; // prov_reg_impl

	//
	// class definitions for the recovery
	// data gathered during recovery of an RM (during become_primary()).
	//

	class service_recovery_data {
	public:
		service_recovery_data(replica_int::rma_reconf_ptr rcf_p,
		    replica_int::rma_service_state s_state);
		~service_recovery_data();
		replica_int::rma_reconf_ptr get_rcf();
		replica_int::rma_service_state get_last_state();
		void set_last_state(
		    const replica_int::rma_service_state s_state);

	private:
		replica_int::rma_reconf_ptr	reconf_obj;
		replica_int::rma_service_state	last_state;

		service_recovery_data();

		service_recovery_data &operator = (
		    const service_recovery_data &);
	};

	class provider_recovery_data {
	public:
		provider_recovery_data(
		    rm_repl_service::prov_reg_impl *p,
		    replica_int::rma_repl_prov_state p_state,
		    replica_int::repl_prov_exception p_e);

		~provider_recovery_data();

		rm_repl_service::prov_reg_impl		*get_rp();

		replica_int::rma_repl_prov_state	get_last_state();

		replica_int::repl_prov_exception	get_rp_exception();

	private:
		provider_recovery_data();

		provider_recovery_data &operator = (
		    const provider_recovery_data &);

		rm_repl_service::prov_reg_impl		*prov;
		replica_int::rma_repl_prov_state	last_state;
		replica_int::repl_prov_exception	rp_exception;
	};

	rm_repl_service(const char *nm, const replica::service_num_t num,
	    const uint32_t new_desired_numsecondaries, uint_t initial_state,
	    const replica::service_state sstate);

	~rm_repl_service();

	//
	// Lock/unlock all services in a dependency tree. Lock tree returns
	// the pointer to the dependency tree. The same pointer must be
	// passed into unlock_tree. Unlock_tree will free the pointer.
	//
	repl_service_list	*lock_tree();
	void			unlock_tree(repl_service_list *);

	// Wait for all services in a dependency tree to be stable.
	void	wait_for_tree_stable(repl_service_list *);

	// Lock/unlock service.
	void	lock_service();
	void	unlock_service();

	//
	// called from service_admin method of same name.
	//
	prov_reg_impl	*register_rma_repl_prov(
	    replica_int::rma_repl_prov_ptr repl_prov,
	    const char *prov_desc,
	    const replica::checkpoint_ptr sec_chkpt,
	    uint_t priority,
	    service_admin_impl *sap,
	    const replica::prov_dependencies &this_depends_on,
	    repl_rm::rm_ckpt_ptr,
	    Environment &_env);

	//
	// called from service_admin method of same name.
	//
	void	change_repl_prov_status(
	    prov_reg_impl *rp,
	    const replica::change_op op,
	    repl_service_list *treep,
	    repl_rm::rm_ckpt_ptr,
	    Environment &e);

	//
	// called from service_admin method of same name.
	// Returns a Solaris error code.
	//
	uint32_t	shutdown_service(service_admin_impl *,
	    trans_state_shutdown_service	*,
	    repl_rm::rm_ckpt_ptr,
	    bool is_forced_shutdown,
	    Environment &);

	//
	// called from service_admin method of same name.
	//
	CORBA::Object_ptr	get_root_obj(Environment &e);

	// called from checkpoint code -- called only on secondaries
	prov_reg_impl		*create_rma_repl_prov_on_sec(
	    replica_int::rma_repl_prov_ptr prov, const char *prov_desc,
	    const replica::prov_dependencies &this_depends_on,
	    replica::checkpoint_ptr chkpt,
	    uint_t priority,
	    replica::repl_prov_reg_ptr new_reg_obj);

	// called from add_secondary() code
	void	add_sec_rma_repl_prov_ckpts(repl_rm::rm_ckpt_ptr ckpt,
	    Environment &e);

	void	incr_service_refs();
	void	decr_service_refs();

	void	iterate_over(void (replica_int::rma_reconf::*mfn)(
	    replica::service_num_t sid, Environment &_environment),
	    const char *const fn_name);

	void	iterate_services(const char *fn_name);

	// Called during registration to add a dependency.
	void	add_dep_down(rm_repl_service *dep);

	repl_service_list *get_component_tree();

	bool	is_busy() const;
	bool	is_uninitialized() const;
	bool	is_down() const;
	bool	is_up() const;

	const char	*get_name() const;
	const char	*get_depends() const;

	const replica::service_num_t	&get_number() const;
	const replica::service_state	get_state() const;
	replica::service_state		map_service_state() const;

	static char	*init_str(const char *s);

	// Return a reference to the current primary's repl_prov
	replica_int::rma_repl_prov_ptr	get_primary_ref();

	// Returns true if this provider is primary.
	bool	is_primary(replica_int::rma_repl_prov_ptr);

	void	main_loop();

	//
	// return the registration object for the repl_prov specified
	// by the Corba object.
	// can return NULL.
	//
	const prov_reg_impl *get_repl_prov(
	    const replica_int::rma_repl_prov_ptr repl_prov_arg);

	void	mark_deleted();
	bool	is_deleted();
	void	mark_dead();
	bool	is_dead();
	void	mark_shutdown_intention(prov_reg_impl *);
	void	mark_shutdown_succeeded();
	void	mark_shutdown_committed();
	void	mark_shutdown_failed();
	bool	shutdown_intention_marked();
	void	mark_lower_service_shutdown();
	bool	is_lower_service_shutdown();
	void	wrapup_shutdown(service_admin_impl *);

	void	chk_s_down(Environment &_environment);
	void	chk_s_busy(Environment &_environment);
	void	chk_s_uninit(Environment &_environment);
	void	chk_get_root_obj_s_state(Environment &_environment);

	void	chk_rp_invalid(
	    const char *prov_desc,
	    Environment &_environment);

	void	chk_rp_reg(
	    const char *prov_desc,
	    Environment &_environment);

	// Returns true if we can exit the state machine of the service.
	bool	can_exit_state_machine();

	//
	// Major changes to the service increment its state count:
	// used to check if service admin object is out-of-date.
	//
	service_state_count get_service_state_count();

	void	update_service_admin_count(service_admin_impl *sap);

	void	reconf_complete_wait(Environment &environment);

	// Returns true if the service is stable.
	bool	is_stable();

	// Wait for the service to be stable.
	void	wait_for_stable();

	// Wait for the service to be down.
	void	wait_for_down();

	// Lookup a repl_prov from its name. Only return it if it is alive.
	prov_reg_impl	*get_repl_prov(const char *prov_desc);

	rm_state_machine	*get_state_machine();

	// Generate CORBA string lists of dependencies.
	void	get_deps_down(replica::service_dependencies *seq);
	void	get_deps_up(replica::service_dependencies *seq);

	void	list_to_seq(SList<rm_repl_service> *list,
	    replica::service_dependencies *seq);

	void	remove_repl_prov(prov_reg_impl *new_rp);

	// must be public, since it's called from repl_mgr_impl in recovery
	void	rma_reconf_exception(
	    const replica_int::rma_reconf_ptr p,
	    Environment &e);

	// is this RM primary?
	bool	rm_is_primary() const;

	// called during become_spare() on a secondary RM
	void	become_spare_delete_state();

	void	remove_dependencies();

	// Whether this rm_repl_service has a dedicated thread running.
	bool	need_thread();

	// Clear the NEED_THREAD flag.
	void	thread_created();

	// Retrieve the desired number of secondaries
	uint32_t	get_desired_numsecondaries() const;

	// Set the desired number of secondaries
	void	set_desired_numsecondaries(uint32_t);

	void	shift_secs();
	void	shift_secs_from_bottom();
	void	add_to_sorted_list(prov_reg_list *, prov_reg_impl *);

	bool	is_bottom_service();

	// Upgrade freeze interfaces
	void	freeze_for_upgrade();
	void	mark_freeze_for_upgrade();
	bool	is_frozen_for_upgrade() const;
	void	freeze_dependent_for_upgrade();

	void	unfreeze_after_upgrade(Environment &env);
	void	mark_unfreeze_after_upgrade();
	bool	frozen_for_upgrade_by_req() const;
	void	unfreeze_dependent_after_upgrade();

#ifdef	RM_DBGBUF
	const char	*id();
#endif

	// Tell every primary in a dependency tree to become a secondary.
	void	trigger_switchover(const char *primary_from,
		const char *primary_to);

	// Tell every secondary that the switchover is complete.
	// needs to be public for access by service_admin
	void    switchover_complete();

	void	set_doing_switchover(bool);
	bool	is_doing_switchover() const;

	SList<prov_reg_impl>	repl_prov_list;

private:

	prov_reg_impl	*get_repl_prov(
	    const prov_reg_impl::rcf_state_private);

	//
	// allocate new repl_prov_reg object and add it to per-service
	// list.
	//
	prov_reg_impl	*add_repl_prov(
	    replica_int::rma_repl_prov_ptr rma_repl_prov_ptr,
	    const char *prov_desc,
	    const replica::checkpoint_ptr sec_chkpt,
	    uint_t priority,
	    replica::repl_prov_state init_s,
	    SList<prov_reg_impl> *deps);

	SList<prov_reg_impl>	*repl_prov_dependencies(
	    const replica::prov_dependencies &this_depends_on,
	    Environment &_env);

	void	mark_service_down();

	op_status	case_reg_primary(
	    prov_reg_impl *const rp,
	    const replica::service_admin_ptr sa_ptr);

	op_status	case_reg_secondary_or_spare(
	    prov_reg_impl *const rp,
	    const replica::service_admin_ptr sa_ptr);

	void	incr_state_count(service_admin_impl *sap);

	// doesn't update service_admin_object
	void	incr_state_count();

	bool	max_secondaries_active();

	// Number of secondaries ready to become primary.
	uint_t	num_valid_secs();

	// Return the n'th secondary which is ready to become primary.
	prov_reg_impl	*get_indexed_sec(uint_t n);


	// Return true if dependent services can also switchover
	bool	switchover_dependency_check(repl_service_list *);

	//
	// Used before moving to the service_down state to make sure that
	// all secondaries and spares get shutdown.
	//
	void	shutdown_providers();

	// Used by lock_tree().
	bool	can_lock_tree(repl_service_list *);

	// Used by wait_for_all_stable().
	bool	check_tree_stable(repl_service_list *);

	// Common code for marking dependent servcies as FREEZE_FOR_UPGRADE.
	void	mark_dependent_freeze_for_upgrade();

	//
	// Every rm_repl_service must keep a private count of
	// reference objects.
	//
	uint_t	count;

	char	*const name;

	replica::service_num_t	number;
	service_state_count	state_count;

	enum flag_values {
		SERVICE_EXCLUSIVE = 0x01,
		NEED_THREAD = 0x02,
		DEAD = 0x04,
		DELETED = 0x08,
		SHUTTING_DOWN = 0x10,
		SHUTDOWN_INTENTION = 0x20,
		SHUTDOWN_COMMITTED = 0x40,
		LOWER_SERVICE_SHUTDOWN = 0x80,
		FREEZE_FOR_UPGRADE = 0x100,
		FROZEN_FOR_UPGRADE_BY_REQ = 0x200
	};

	uint_t	flags;

	//
	// The primary that is currently being shutdown. This is maintained
	// on the secondary when service shutdowns are in progress. It is
	// used during recovery to help us recover our state.
	//
	prov_reg_impl	*shutdown_primary;

	// The state machine for this service
	rm_state_machine	*smp;

	// Services that directly depend on this.
	SList<rm_repl_service>	deps_up;

	// Services that this directly depends on.
	SList<rm_repl_service>	deps_down;

	// Desired number of secondaries
	uint32_t	desired_numsecondaries;

	//
	// Indicate whether there is a switchover in progress. This flag
	// is set in all services of a dependency tree.
	//
	bool	doing_switchover;

	class switchover_info_t {

	public:
		switchover_info_t(const char*, const char*);
		~switchover_info_t();

		const char	*get_switchover_from() const;
		const char	*get_switchover_to() const;

	private:
		//
		// Specifies the current primary which we are being switched
		// from.
		//
		char	*switchover_from_primary;

		//
		// Specifies the provider that is being switched to, if this is
		// specified in the call to change_repl_prov_status.
		//
		char	*switchover_to_primary;
	};

	//
	// Useful switchover information needed by recovery. Only gets created
	// in the bottom service of a dependency tree.
	//
	switchover_info_t	*switchover_info;

#ifdef RM_DBGBUF
	char	*debug_id;
#endif

	rm_repl_service();
	rm_repl_service(const rm_repl_service&);
	rm_repl_service &operator = (const rm_repl_service &);
}; // rm_repl_service

//
// List of prov_reg_impl.  Has a destructor that does an erase_list so callers
// don't need to empty the list before deleting.
//
class prov_reg_list : public SList<rm_repl_service::prov_reg_impl> {
public:
	~prov_reg_list();
};

//
// List of rm_repl_service.  Has a destructor that does an erase_list so
// callers don't need to empty the list before deleting.
//
class repl_service_list : public SList<rm_repl_service> {
public:
	~repl_service_list();
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _RM_REPL_SERVICE_H */
