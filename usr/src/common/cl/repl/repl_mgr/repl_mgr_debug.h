/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_MGR_DEBUG_H
#define	_REPL_MGR_DEBUG_H

#pragma ident	"@(#)repl_mgr_debug.h	1.18	08/05/20 SMI"

#include <orb/debug/haci_debug.h>

#if defined(HACI_DBGBUFS) && !defined(NO_RM_DBGBUF)
#define	RM_DBGBUF
#endif

// used for general RM tracing
#ifdef RM_DBGBUF
extern dbg_print_buf	rm_dbg_buf;
#define	RM_DBG(a)	HACI_DEBUG(ENABLE_RM_DBG, rm_dbg_buf, a)
#else
#define	RM_DBG(a)
#endif

#if defined(DBGBUFS) && !defined(NO_RECONF_DBGBUF)
#define	RECONF_DBGBUF
#endif

// used for rm_repl_service::iterate_over() method
// Separated so that we can split the messages out in the future if necessary
#ifdef RECONF_DBGBUF
#define	RECONF_DBG(a)	RM_DBG(a)
#else
#define	RECONF_DBG(a)
#endif

#endif	/* _REPL_MGR_DEBUG_H */
