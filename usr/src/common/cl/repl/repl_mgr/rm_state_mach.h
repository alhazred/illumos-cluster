/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RM_STATE_MACH_H
#define	_RM_STATE_MACH_H

#pragma ident	"@(#)rm_state_mach.h	1.37	08/05/20 SMI"

#include <sys/list_def.h>
#include <repl/repl_mgr/rm_repl_service.h>

//
// rm_state_machine class - defines the state of an HA service,
// and is used by the HA framework to control an HA service.
// The HA framework uses one rm_state_machine class object
// to support one HA service.
//
class rm_state_machine {
public:
	//
	// This enum identifies the possible states of the HA service
	// state machine.
	//
	enum sm_service_state {
		SERVICE_REGISTERED,
		SERVICE_RECOVERING,
		SERVICE_UNINIT,
		SERVICE_UP,
		REMOVE_SECONDARIES,
		PRIMARY_FROZEN,
		SERVICE_FROZEN_CLEAN,
		NEW_SECONDARY,
		PRIMARY_DEACTIVATED,
		DISCONNECTED,
		NO_PRIMARY,
		PRIMARY_SELECTED,
		PRIMARY_PREPARED,
		RECONNECTED,
		SERVICE_FROZEN_DIRTY,
		CLIENTS_FROZEN,
		PRIMARY_DEAD,
		SERVICE_SUICIDE,
		SERVICE_DOWN,
		FROZEN_FOR_UPGRADE,
		EXIT_STATE,		// dummy state
		MAX_STATE = EXIT_STATE,
		NSTATE = MAX_STATE + 1
	};

	//
	// This enum defines collections of states for efficient comparisons.
	// The ordinal bit position corresponds to the
	// sm_service_state ordinal enum value.
	//
	enum aggregate_state {
		SYNC_STABLE = 1 << SERVICE_UP |
		    1 << SERVICE_DOWN |
		    1 << SERVICE_UNINIT,

		SYNC_FROZEN = 1 << PRIMARY_FROZEN |
		    1 << SERVICE_FROZEN_CLEAN |
		    1 << NEW_SECONDARY |
		    1 << FROZEN_FOR_UPGRADE,

		SYNC_PRIMARY_QUIESCED = 1 << NO_PRIMARY |
		    1 << DISCONNECTED |
		    1 << PRIMARY_DEAD |
		    1 << SERVICE_UNINIT,

		SYNC_NO_PRIMARY = 1 << NO_PRIMARY |
		    1 << SERVICE_UNINIT,

		//
		// Once a lower level service enters SERVICE_FROZEN_DIRTY state,
		// it waits for its dependent services to enter one of
		// the states below before leaving SERVICE_FROZEN_DIRTY state.
		// This is to ensure that state machines of all services in the
		// dependency tree can be recovered correctly in case the RM
		// primary fails over at this point.
		//
		// See bug 4823195 for more information.
		//
		SYNC_TRYING_PRIMARY = 1 << PRIMARY_PREPARED |
		    1 << RECONNECTED,

		SYNC_COMMITTED_PRIMARY = 1 << SERVICE_FROZEN_DIRTY |
		    1 << SERVICE_UP |
		    1 << FROZEN_FOR_UPGRADE,

		SYNC_SERVICE_DOWN = 1 << SERVICE_DOWN,

		SYNC_SERVICE_UNINIT = 1 << SERVICE_UNINIT,

		// Want any state other than SERVICE_RECOVERING
		SYNC_NOT_RECOVERING = ~(1 << SERVICE_RECOVERING),

		//
		// Looking for a state that does not represent
		// one of the states on the way up to operational status.
		//
		SYNC_NOT_WAY_UP = ~(1 << NO_PRIMARY |
		    1 << PRIMARY_SELECTED |
		    1 << PRIMARY_PREPARED |
		    1 << RECONNECTED |
		    1 << SERVICE_FROZEN_DIRTY)
	};

	bool check_state(int wanted_states);

	bool check_other_states(SList<rm_repl_service> *others,
	    int wanted_states);

	void wait_for_others(SList<rm_repl_service> *others,
	    int wanted_states);

	//
	// The following statics are used by clients who don't care
	// about a specific service.
	//
	static void state_mutex_lock();
	static void state_mutex_unlock();
	static inline bool state_lock_held();
	static void wait_for_external_state_change();
	static void signal_external_state_change();

	//
	// These are non-static and thus are always called with respect to a
	// specific service.
	//
	void signal_state_machine();
	bool is_stable();
	inline rm_repl_service::prov_reg_impl	*get_primary();

	void query_providers(
	    SList<rm_repl_service::provider_recovery_data> *prd_list);

	sm_service_state make_rmas_consistent(
	    const replica_int::rma_service_state	state_a,
	    const replica_int::rma_service_state	state_b);

	sm_service_state get_consistent_rma_state();

	sm_service_state get_state() const;

	typedef sm_service_state  (rm_state_machine::*rm_handler_t)();

	rm_state_machine(rm_repl_service *const svc,
	    const sm_service_state &init_state);

	void execute();
	static bool inval_rp_state_exception(Environment &e);

	//
	// Called during become_primary(), to store data
	// obtained in a call to rma_reconf::get_states().
	//
	void store_recovery_data(
	    rm_repl_service::service_recovery_data *data_p);

private:

	// Class used for comparing combinations when looking for best primary.
	class repl_prov_combo_score {
	public:
		repl_prov_combo_score();
		bool better_than(repl_prov_combo_score *);

		bool selected_as_primary;
		uint_t num_running;
		uint_t num_start;
		uint_t priority;
	};

	void initialize();
#ifdef RM_DBGBUF
	static const char *state_name(sm_service_state state);
#endif

	//
	// called when the associated state is entered.
	//
	sm_service_state	service_registered();
	sm_service_state	service_recovering();
	sm_service_state	service_uninit();
	sm_service_state	service_up();
	sm_service_state	remove_secondaries();
	sm_service_state	primary_frozen();
	sm_service_state	service_frozen_clean();
	sm_service_state	frozen_for_upgrade();
	sm_service_state	new_secondary();
	sm_service_state	primary_deactivated();
	sm_service_state	disconnected();
	sm_service_state	no_primary();
	sm_service_state	primary_selected();
	sm_service_state	primary_prepared();
	sm_service_state	reconnected();
	sm_service_state	service_frozen_dirty();
	sm_service_state	clients_frozen();
	sm_service_state	service_suicide();
	sm_service_state	primary_dead();
	sm_service_state	service_down();
	sm_service_state	service_down_frozen_for_upgrade();
	sm_service_state	exit_state();	// should never execute;
						// exists only to provide
						// easy way to break out
						// of execute loop.

	// rest of state_machine methods are "helper" routines

	bool	service_shutdown();
	void	update_primary_state(sm_service_state *failed_state,
		    Environment &e);
	bool 	primary_is_dead();
	void 	cleanup_providers();

	rm_repl_service::prov_reg_impl *find_sec_to_remove();
	rm_repl_service::prov_reg_impl *find_sec_to_commit();

	sm_service_state identify_secs_to_remove();
	bool 	identify_secs_to_add();

	rm_repl_service::prov_reg_impl *find_best_sec_to_remove();

	void	 spare_sec(rm_repl_service::prov_reg_impl *);
	void	 shutdown_spare(rm_repl_service::prov_reg_impl *);

	sm_service_state freeze_primary();
	sm_service_state unfreeze_primary();
	sm_service_state make_primary_a_secondary();
	sm_service_state become_primary_prepare();
	sm_service_state become_secondary_wrapup();
	sm_service_state start_new_primary();
	rm_repl_service::prov_reg_impl *select_new_primary();
	bool switchover();
	void commit_secondary(rm_repl_service::prov_reg_impl *sec);
	void wait_on_cmm_for_frozen();
	void fill_sec_ckpt_seq(replica::secondary_seq &sec_seq);
	void fill_sec_name_seq(replica::repl_name_seq &name_seq);
	void connect_to_primary();
	bool must_freeze_for_upgrade();

	sm_service_state invoke_add_secondary(
	    const replica_int::rma_repl_prov_ptr primary_ptr,
	    const replica::checkpoint_ptr sec_chkpt,
	    const char *prov_desc);

	sm_service_state add_secondary(
	    rm_repl_service::prov_reg_impl *const sec_rp);

	rm_repl_service::prov_reg_impl *find_sec_to_add();
	rm_repl_service::prov_reg_impl *find_best_spare();
	rm_repl_service::prov_reg_impl *find_elig_primary();
	rm_repl_service::prov_reg_impl *find_elig_primary_from_secs();
	uint_t get_base_services(SList<rm_repl_service> *base_services);
	void signal_bottom_services();
	void signal_dependent_services();

	void evaluate_combo(SList<rm_repl_service> *sl,
	    uint_t combo_num, repl_prov_combo_score *val);

	void deactivate_secondary(rm_repl_service::prov_reg_impl *rp);
	bool max_secondaries_active();

	rm_repl_service::prov_reg_impl *get_repl_prov(
	    const rm_repl_service::prov_reg_impl::rcf_state_private);

	// Clear a bitmask of requested states.
	void clear_sm_prov_states();

	// Helper functions:
	bool check_primary_dependency(
	    rm_repl_service::prov_reg_impl *proposed_primary);

	bool check_secondary_dependency(
	    rm_repl_service::prov_reg_impl *proposed_sec);

	void block_in_stable_state();
	void broadcast_state_change();

	void recovery_reconf_exception(
	    rm_repl_service::service_recovery_data	*data_p,
	    Environment &e);

	void make_consistent_iterate_over(
	    const replica_int::rma_service_state state);

	rm_repl_service::provider_recovery_data*	last_primary_data(
	    SList<rm_repl_service::provider_recovery_data> *);

	sm_service_state get_next_state(
	    SList<rm_repl_service::provider_recovery_data> *prd_list);

	void spare_all_spares();

	void assign_provider_state(
	    rm_repl_service::prov_reg_impl *rp,
	    const replica_int::rma_repl_prov_state state,
	    const replica_int::repl_prov_exception rpe);

	// Disallow
	rm_state_machine &operator = (const rm_state_machine &);
	rm_state_machine(const rm_state_machine &);
	rm_state_machine();

	// Mutex and condvar for state changes
	static os::mutex_t	state_mutex;
	static os::condvar_t	external_cv;

	os::condvar_t	cv;

	sm_service_state	current_state;

	// The primary when we entered the state machine. can be NULL.
	rm_repl_service::prov_reg_impl	*cur_primary_p;

	// The service this state machine is operating on
	rm_repl_service		*s;

	rm_handler_t		rm_handlers[NSTATE];

	bool	first_time_up;

	bool	is_stable_state;

	// used by HA RM for temporary storage of recovery information
	SList<rm_repl_service::service_recovery_data>	get_states_list;
};

#endif	/* _RM_STATE_MACH_H */
