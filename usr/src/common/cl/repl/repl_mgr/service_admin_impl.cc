/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)service_admin_impl.cc	1.86	08/11/24 SMI"

//
// service_admin_impl.cc implements the service control interface
// for administration of a replicated service.  It provides
// a front-end for the rm_repl_service class.  A service may have
// any number of control interface objects pointing to it (this
// is one of the reasons rm_repl_service is reference counted).
//
// Note that some interface methods can return a service_changed
// exception, which informs the caller that his view of the
// service is out-of-date.
//
// XXX service_state_count code has not been rigorously tested.
//

#include <repl/repl_mgr/service_admin_impl.h>
#include <repl/repl_mgr/rm_repl_service.h>
#include <repl/repl_mgr/rm_state_mach.h>
#include <repl/repl_mgr/rs_trans_states.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <repl/repl_mgr/rm_state_mach_in.h>


// primary constructor
service_admin_impl::service_admin_impl(
    repl_mgr_prov_impl *rm_prov_impl,
    rm_repl_service *const svc) :
    mc_replica_of<replica_int::rm_service_admin>
/*CSTYLED*/
    (rm_prov_impl), s(*svc), forced_shutdown(false)
{
	CL_PANIC(rm_state_machine::state_lock_held());
	s.incr_service_refs();
	set_state_count(s.get_service_state_count());
}

// secondary constructor
service_admin_impl::service_admin_impl(
    replica_int::rm_service_admin_ptr sa_ptr,
    rm_repl_service *const svc) :
    mc_replica_of<replica_int::rm_service_admin>
/*CSTYLED*/
    (sa_ptr), s(*svc)
{
	s.incr_service_refs();
	// not particularly meaningful on secondary, where service
	// state count is 0, but we should initialize it to something
	set_state_count(s.get_service_state_count());
}

service_admin_impl::~service_admin_impl()
{
}

void
service_admin_impl::set_state_count(const service_state_count cnt)
{
	service_admin_state_count.set_state_count(cnt);
}

//
// assumes ignore_state_change flag has been checked:  caller wants the
// service_admin operation to fail if his view is out-of-date.
//
void
service_admin_impl::chk_state_change(Environment &_environment)
{
	sa_state_count sa_cnt = get_sa_state_count();
	CL_PANIC(sa_cnt.get_count() <= s.get_service_state_count());
	if ((sa_cnt.get_count() != s.get_service_state_count())) {
		replica::service_admin::service_changed	*sc = new
		    replica::service_admin::service_changed;
		_environment.exception(sc);

		RM_DBG(("service_admin_impl: service changed "
		    "exception, `%p'\n", this));
	}
}

void
service_admin_impl::_unreferenced(unref_t cookie)
{
	rm_state_machine::state_mutex_lock();

	SList<service_admin_impl> &service_admin_list
	    = repl_mgr_impl::get_sai_list();
	CL_PANIC(service_admin_list.exists(this));

	if (_last_unref(cookie)) {
		s.decr_service_refs();
		(void) service_admin_list.erase(this);
		delete this;
	}

	rm_state_machine::state_mutex_unlock();
}

void
service_admin_impl::perform_reg_rma_repl_prov_checks(
    const char *prov_desc, Environment &_env)
{
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: register_rma_repl_prov "
		    "deleted_service exception, sa `%p'\n", this);
		_env.exception(new replica::deleted_service));
	}

	if (!(_env.exception())) {
		s.chk_s_down(_env);
	}

	if (!(_env.exception())) {
		s.chk_rp_invalid(prov_desc, _env);
	}
}

//
// service_admin_impl(replica::service_admin::get_service_info)
// return service_info data on the service administered by this
// control interface.
//
// since this is just a read of the service, we don't checkpoint.
//
replica::service_info*
service_admin_impl::get_service_info(Environment &_env)
{
	rm_state_machine::state_mutex_lock();

	// Check whether the service is deleted.
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: get_service_info "
		    "deleted_service exception, sa `%p'\n", this));
		_env.exception(new replica::deleted_service);
		rm_state_machine::state_mutex_unlock();
		return (nil);
	}

	replica::service_info *si_out = new replica::service_info;

	si_out->sid 		= s.get_number();
	si_out->service_name 	= s.get_name();
	si_out->s_state 	= s.get_state();
	si_out->desired_numsecondaries = s.get_desired_numsecondaries();

	s.get_deps_down(&si_out->this_depends_on);
	s.get_deps_up(&si_out->depend_on_this);

	s.update_service_admin_count(this);
	rm_state_machine::state_mutex_unlock();

	RM_DBG(("service_admin_impl:  processed "
	    "invocation get_service_info, sa `%p'.\n", this));

	return (si_out);
}

//
// service_admin_impl(
// replica_int::rm_service_admin::register_rma_repl_prov)
// register a replica provider and return a reference to a registration
// object, whose _unreferenced call will be executed in the RM (so
// the RM can do failover of a primary, for example).
//
// Note that the specified repl_prov can be functioning as a primary
// or secondary by the time this method returns.
// Also note that the orphan processing routine will set the impl_obj
// to NULL if the client has died:  doing any checks on a stale pointer
// (unreferenced could have been processed during recovery) can be
// dangerous.
//
void
service_admin_impl::register_rma_repl_prov(
    replica_int::rma_repl_prov_ptr rma_prov,
    const char *prov_desc,
    const replica::prov_dependencies &this_depends_on,
    replica::checkpoint_ptr chkpt,
    uint32_t priority,
    replica::repl_prov_reg_out cb,
    Environment &_env)
{
	CL_PANIC(!_env.exception());
	cb = replica::repl_prov_reg::_nil();

	rm_repl_service::prov_reg_impl *impl_obj = NULL;

	RM_DBG(("sa_ri:  got request for repl_prov "
	    "registration, rma_repl_prov \"%s\" `%p', sa `%p'\n",
	    prov_desc, rma_prov, this));

	rm_state_machine::state_mutex_lock();
	// Lock service and everything in its dependency tree.
	repl_service_list *tree = s.lock_tree();

	trans_state_reg_rma_repl_prov	*st
	    = get_saved_primary_reg_rma_repl_prov_state(_env);

	// An exception here means this is an orphaned message. Just return.
	if (_env.exception() != NULL) {
		CL_PANIC(st == NULL);
		_env.clear();
		s.unlock_tree(tree);
		rm_state_machine::state_mutex_unlock();
		return;
	}

	// The client is alive. We don't need to worry about _unreferenced
	// corrupt our data from now on since we already hold the lock.

	if (st) {
		// A retry.

		impl_obj = st->get_reg_impl();
		ASSERT(impl_obj != NULL);
		CL_PANIC(s.repl_prov_list.exists(impl_obj));

		RM_DBG(("sa_ri: retry for "
		    "register_rma_repl_prov, rma_prov `%p'\n",
		    rma_prov));
	} else {
		// A new invocation.

		//
		// Perform some checks on the service and the new provider
		// Possible exceptions here are: invalid_repl_prov
		// failed_service, and deleted_service.
		//
		perform_reg_rma_repl_prov_checks(prov_desc, _env);

		//
		// If no exception returned so far, try to register this
		// provider.
		//
		if (!(_env.exception())) {
			repl_rm::rm_ckpt_ptr rs_ckpt_p = get_checkpoint();
			impl_obj = s.register_rma_repl_prov(rma_prov,
			    prov_desc, chkpt, priority, this,
			    this_depends_on, rs_ckpt_p, _env);
			CORBA::release(rs_ckpt_p);

			// possible exceptions is: dependency_not_satisfied
		}
	}

	if (_env.exception() == NULL) {
		ASSERT(impl_obj != NULL);

		cb = impl_obj->get_objref();
		add_commit(_env);

		//
		// Shift secondaries distribution from the bottom in the
		// dependency tree to which this service belongs.
		//
		// If the joining provider belongs to a bottom service,
		// it may have higher priority than existing providers.
		//
		// If the joining provider belongs to a dependent service,
		// its bottom provider will be able to bring up more services
		// in future, therefore, a higher priority.
		//
		s.shift_secs_from_bottom();
	} else {
		CL_PANIC(impl_obj == NULL);
		RM_DBG(("service_admin_impl:  unsuccessful repl_prov "
		"registration, rma_repl_prov `%p', sa `%p'\n",
		    rma_prov, this));
	}

	if (s.is_down()) {
		//
		// The service should be in either UP or UNINIT state
		// after the registration. Otherwise, the registration must
		// have encoutered some problem.
		//
		RM_DBG(("service_admin_impl: service down. Unsuccessful "
		    "repl_prov registration, rma_repl_prov '%p', sa '%p'.\n"));
		_env.exception(new replica::failed_service);
	}

	s.unlock_tree(tree);
	rm_state_machine::state_mutex_unlock();

	RM_DBG(("service_admin_impl:  processed "
	    "invocation register_rma_repl_prov, sa `%p'\n", this));
}

//
// service_admin_impl(replica::service_admin::change_repl_prov_status)
// change the status of a repl_prov:  can cause switchover.
//
// change_ops:
//	SC_SET_PRIMARY
//	SC_SET_SECONDARY
//	SC_SET_SPARE
//	SC_REMOVE_REPL_PROV
//
// Note that this method does not send a checkpoint.  Note also
// that the result from a retry might return an exception, where
// the non-retry wouldn't (e.g., the recovering RM might notice
// the provider is down, or even process an unreferenced on the
// provider before the retry comes in):  the difference in timing
// caused some problems for the FI tests, which didn't expect an
// exception.
//
void
service_admin_impl::change_repl_prov_status(const char *repl_prov_desc,
    replica::change_op change, bool ignore_state_change,
    Environment &_environment)
{
	rm_state_machine::state_mutex_lock();
	repl_service_list *treep = s.lock_tree();

	Environment		e;
	repl_rm::rm_ckpt_ptr	rs_ckpt_p = get_checkpoint();

	// Check whether the service is deleted.
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: change_repl_prov_status "
		    "deleted_service exception, sa `%p'\n", this));
		e.exception(new replica::deleted_service);
	} else {
		s.chk_s_down(e);
	}

	rm_repl_service::prov_reg_impl *rp;
	if (!e.exception()) {
		if ((rp = s.get_repl_prov(repl_prov_desc)) == NULL) {
			e.exception(new replica::invalid_repl_prov);
			RM_DBG(("rm_rs %s: change_repl_prov_status, "
			    "\"%s\" not registered\n", s.id(), repl_prov_desc));
		} else if (s.is_doing_switchover()) {
			//
			// This is a retry. The recovery code in the
			// rm_state_machine should have resumed and completed
			// the switchover request.
			//
			goto cleanup;
		}
	}

	if (!e.exception() && !ignore_state_change) {
		chk_state_change(e);
	}

	if (!e.exception()) {
		//
		// We need to pass _environment instead of local e because
		// change_repl_prov_status does checkpoint invocations.
		//
		s.change_repl_prov_status(rp, change, treep, rs_ckpt_p,
		    _environment); //lint !e644
	}

cleanup:
	//
	// If we were doing a switchover, then by this time it is complete,
	// so we should reset all the variables.
	//
	if (s.is_doing_switchover()) {
		CL_PANIC(!_environment.exception());
		s.switchover_complete();
		rs_ckpt_p->ckpt_switchover_service(NULL, NULL, s.get_number(),
		    _environment);
		CL_PANIC(!_environment.exception());

		if (!e.exception()) {
			//
			// We haven't gotten any exceptions so far, need to
			// check if the switchover has successfully completed.
			//
			if (!((change == replica::SC_SET_PRIMARY) &&
			    rp->is_primary()) &&
			    !((change == replica::SC_SET_SECONDARY) &&
			    !rp->is_primary())) {
				e.exception(new replica::failed_to_switchover);
			}
		}
	}

	CORBA::release(rs_ckpt_p);

	CORBA::Exception	*ex;
	if ((ex = e.release_exception()) != NULL) {
		// propagate the exception
		_environment.exception(ex);
	}
	s.unlock_tree(treep);
	rm_state_machine::state_mutex_unlock();

	RM_DBG(("service_admin_impl:  processed "
	    "invocation change_repl_prov_status, "
	    "repl_prov_desc %s , sa `%p'\n", repl_prov_desc, this));
}

// Change the priorities of a list of providers altogether.
void
service_admin_impl::change_repl_prov_priority(
    const replica::prov_priority_list &prov_list, Environment &_environment)
{
	rm_state_machine::state_mutex_lock();
	repl_service_list *tree = s.lock_tree();

	// Check whether the service is deleted.
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: change_repl_prov_priority "
		    "deleted_service exception, sa `%p'\n", this));
		_environment.exception(new replica::deleted_service);
	} else if (!s.is_bottom_service()) {
		//
		// Priority can only be set to providers that belong to bottom
		// service.
		//
		_environment.exception(new replica::dependent_prov);
	} else {
		//
		// Going through the list to check if all the priorities
		// spcified have valid value
		//
		for (unsigned int i = 0; i < prov_list.length(); i++) {
			if ((const char *)prov_list[i].prov_desc == NULL) {
				_environment.exception(new
				    replica::invalid_repl_prov);
				break;
			}
		}
	}

	if (_environment.exception()) {
		s.unlock_tree(tree);
		rm_state_machine::state_mutex_unlock();
		return;
	}

	s.chk_s_down(_environment);

	if (!_environment.exception()) {
		rm_repl_service::prov_reg_impl *rp = NULL;
		bool	need_shift_secs = false;

		for (unsigned int i = 0; i < prov_list.length(); i++) {
			if ((rp = s.get_repl_prov(prov_list[i].prov_desc))
			    == NULL) {
				RM_DBG(("rm_rs %s: change_repl_prov_priority,"
				" \"%s\" not registered\n", s.id(),
				(const char *)prov_list[i].prov_desc));

				// ignore it
				continue;
			}

			if (rp->get_priority() != prov_list[i].priority) {
				RM_DBG(("service_admin_impl: set `%s' priority "
				    "to %d, sa `%p'\n",
				    (const char *)prov_list[i].prov_desc,
				    prov_list[i].priority, this));

				rp->set_priority(prov_list[i].priority);

				//
				// We have changed at least one provider's
				// priority, will need to shift secondary
				// distribution later.
				//
				need_shift_secs = true;

				get_checkpoint()->
				    ckpt_change_repl_prov_priority(
				    s.get_number(), prov_list[i].prov_desc,
				    prov_list[i].priority, _environment);

				CL_PANIC(_environment.exception() == NULL);
			}
		}

		if (need_shift_secs) {
			//
			// Since some providers' priority has been changed in
			// this service, we need to shift rearrange secondary
			// distribution.
			//
			s.shift_secs();
		}
	}

	s.unlock_tree(tree);
	rm_state_machine::state_mutex_unlock();

	RM_DBG(("service_admin_impl:  processed "
	    "invocation change_repl_prov_priority, "
	    "sa `%p' \n", this));
}

// Change the desired number of secondaries
void
service_admin_impl::change_desired_numsecondaries(
    uint32_t new_desired_numsecondaries, Environment &_environment)
{
	rm_state_machine::state_mutex_lock();
	repl_service_list *tree = s.lock_tree();

	// Check whether the service is deleted.
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: change_desired_numsecondaries "
		    "deleted_service exception, sa `%p'\n", this));
		_environment.exception(new replica::deleted_service);
	} else if (new_desired_numsecondaries > MAX_SECONDARIES) {
		RM_DBG(("service_admin_impl: "
		    "change_desired_numsecondaries "
		    "invalid_numsecs exception, sa `%p'\n", this));

		_environment.exception(new replica::invalid_numsecs);
	}

	if (_environment.exception()) {
		s.unlock_tree(tree);
		rm_state_machine::state_mutex_unlock();
		return;
	}

	s.chk_s_down(_environment);

	if (!_environment.exception() &&
	    (s.get_desired_numsecondaries() != new_desired_numsecondaries)) {
		RM_DBG(("service_admin_impl: change desired numsecs from "
		    "%d to %d.\n", s.get_desired_numsecondaries(),
		    new_desired_numsecondaries));

		s.set_desired_numsecondaries(new_desired_numsecondaries);

		get_checkpoint()->ckpt_change_desired_numsecondaries(
		    s.get_number(), new_desired_numsecondaries, _environment);
	}

	s.unlock_tree(tree);
	rm_state_machine::state_mutex_unlock();

	RM_DBG(("service_admin_impl: processed "
	    "invocation change_desired_numsecondaries, "
	    "sa '%p', new desired_numsecondaries: %d\n", this,
	    new_desired_numsecondaries));
}

//
// service_admin_impl(replica::service_admin::get_root_obj)
// Can return one of the following:
//	-- nil obj and exception (service is down or uninit)
//	-- nil obj and no exception (primary can't return obj, but is up
//	-- non-nil obj and no exception
//
// No checkpoint done in this invocation.
//
CORBA::Object_ptr
service_admin_impl::get_root_obj(Environment &_env)
{
	rm_state_machine::state_mutex_lock();
	s.lock_service();

	// Check whether the service is deleted.
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: get_root_obj "
		    "deleted_service exception, sa `%p'\n", this));
		_env.exception(new replica::deleted_service);
		s.unlock_service();
		rm_state_machine::state_mutex_unlock();
		return (CORBA::Object::_nil());
	}

	CORBA::Object_ptr root_obj = CORBA::Object::_nil();

	// first make sure service isn't down or uninit
	s.chk_get_root_obj_s_state(_env);

	if (!(_env.exception())) {
		// service method does any retries that
		// are required because of primary failures
		root_obj = s.get_root_obj(_env);
	}

	CL_PANIC(!(s.is_up() && (_env.exception())));

	RM_DBG(("service_admin_impl:  processed "
	    "invocation get_root_obj "
	    "sa `%p', service name `%s', returning obj `%p'\n",
	    this, s.get_name(), root_obj));

	s.unlock_service();
	rm_state_machine::state_mutex_unlock();

	if (_env.exception()) {
		return (CORBA::Object::_nil());
	}
	return (root_obj);
}

//
// service_admin_impl(replica::service_admin::shutdown_service)
//
// We checkpoint intention in the repl_service code; committed() sets
// the service deleted flag.
//
// If recovery rolled forward with the shutdown, just return (unless
// caller didn't want to ignore state change).
//
// Returns a Solaris error code.
//
uint32_t
service_admin_impl::shutdown_service(bool is_forced_shutdown,
    Environment &_environment)
{
	rm_state_machine::state_mutex_lock();

	s.lock_service();

	trans_state_shutdown_service	*st
	    = get_saved_primary_shutdown_service_state(_environment);
	_environment.clear();

	if (st == NULL && s.is_deleted()) {
		RM_DBG(("service_admin_impl: shutdown_service "
		    "deleted_service exception, sa `%p'\n", this);
		_environment.exception(new replica::deleted_service));
	} else {
		forced_shutdown = is_forced_shutdown;
		repl_rm::rm_ckpt_ptr rs_ckpt_p = get_checkpoint();
		uint32_t err_code = s.shutdown_service(this, st, rs_ckpt_p,
		    is_forced_shutdown, _environment);
		CORBA::release(rs_ckpt_p);
		if (err_code != 0) {
			return (err_code);
		}
		if (_environment.exception() == NULL) {
			add_commit(_environment);
		}
	}
	s.unlock_service();
	rm_state_machine::state_mutex_unlock();
	RM_DBG(("service_admin_impl:  processed shutdown_service\n"));
	return (0);
}

//
// service_admin_impl(replica::service_admin::get_repl_provs)
// return a sequence of repl_prov_info structs.
//
// no checkpoint is done in this invocation.
//
void
service_admin_impl::get_repl_provs(replica::repl_prov_seq_out repl_provs,
    Environment &_env)
{
	rm_state_machine::state_mutex_lock();

	// Check whether the service is deleted.
	if (s.is_deleted()) {
		RM_DBG(("service_admin_impl: get_repl_provs "
		    "deleted_service exception, sa `%p'\n", this));
		_env.exception(new replica::deleted_service);
		rm_state_machine::state_mutex_unlock();
		return;
	}

	// lint -- "max" renamed to "maxcount" to eliminate hiding
	const uint_t maxcount = s.repl_prov_list.count();
	const uint_t len = maxcount;

	replica::repl_prov_seq *rps_tmp =
	    new replica::repl_prov_seq(maxcount, len);
	replica::repl_prov_seq &rps_ref = *rps_tmp;
	rm_repl_service::prov_reg_impl	*rpr_i = NULL;
	uint_t					j = 0;
	SList<rm_repl_service::prov_reg_impl>::ListIterator iter
	    (s.repl_prov_list);
	for (;
	    (rpr_i = iter.get_current()) != NULL;
	    iter.advance()) {
		rps_ref[j].curr_state
		    = rpr_i->get_repl_prov_state();
		rps_ref[j].repl_prov_desc
		    = rpr_i->get_rp_desc();
		rps_ref[j].priority = rpr_i->get_priority();
		rm_repl_service::prov_reg_impl *prov;
		unsigned int length = 0;
		for (rpr_i->deps_down.atfirst();
		    (prov = rpr_i->deps_down.get_current()) != NULL;
		    rpr_i->deps_down.advance()) {
			rps_ref[j].this_depends_on.length(length+1);
			rps_ref[j].this_depends_on[length].service =
				prov->get_service()->get_name();
			rps_ref[j].this_depends_on[length].repl_prov_desc =
				prov->get_rp_desc();
			length++;
		}
		j++;
	}

	CL_PANIC(maxcount == j);
	s.update_service_admin_count(this);
	rm_state_machine::state_mutex_unlock();

	replica::repl_prov_seq_out out(rps_tmp);
	repl_provs = out;
} // get_repl_provs

repl_rm::rm_ckpt_ptr
service_admin_impl::get_checkpoint()
{
	return ((repl_mgr_prov_impl*)get_provider())->get_checkpoint_rm_ckpt();
}
