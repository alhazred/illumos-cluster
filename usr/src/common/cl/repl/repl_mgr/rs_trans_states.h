/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RS_TRANS_STATES_H
#define	_RS_TRANS_STATES_H

#pragma ident	"@(#)rs_trans_states.h	1.9	08/05/20 SMI"

//
// rs_trans_states.h contains class definitions for the replica
// manager repl_service transaction state interface.
//
// needs to be a separate file from rm_trans_states because of
// inclusion of rm_repl_service.h
//

#include <repl/service/transaction_state.h>
#include <repl/repl_mgr/rm_repl_service.h>
#include <repl/repl_mgr/service_admin_impl.h>

class trans_state_reg_rma_repl_prov : public transaction_state {
public:
	trans_state_reg_rma_repl_prov(
	    rm_repl_service::prov_reg_impl *);
	~trans_state_reg_rma_repl_prov();	// null
	void orphaned(Environment &);
	void committed();
	rm_repl_service::prov_reg_impl *get_reg_impl();
	// called during orphaned
	void reset_reg_impl();
private:
	// return value
	// Nulled out by orphaned
	rm_repl_service::prov_reg_impl *reg_impl_obj;

	// Disallow
	trans_state_reg_rma_repl_prov();
};

class trans_state_shutdown_service : public transaction_state {
public:

	enum shutdown_state {
		COMMITTED_SUCCESS,	// Succeeded and completed
		COMMITTED_FAILED,	// Failed
		SUCCEEDED,		// Succeeded and not completed
		STARTED			// Started request to primary
	};

	trans_state_shutdown_service(rm_repl_service *, shutdown_state st,
	    service_admin_impl *sa, bool was_forced_shutdown = false);
	~trans_state_shutdown_service();	// null
	void orphaned(Environment &);
	void committed();

	shutdown_state state;

private:
	rm_repl_service	*s;
	service_admin_impl *sai;	// service admin doing the shutdown
	bool forced_shutdown;

	// Disallow
	trans_state_shutdown_service();
};

#include <repl/repl_mgr/rs_trans_states_in.h>

#endif	/* _RS_TRANS_STATES_H */
