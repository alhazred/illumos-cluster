/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RS_TRANS_STATES_IN_H
#define	_RS_TRANS_STATES_IN_H

#pragma ident	"@(#)rs_trans_states_in.h	1.11	08/05/20 SMI"

//
// rs_trans_states_in.h implements inline methods for the
// rs_trans_states class.
//
// needs to be a separate file from rm_trans_states because of
// inclusion of rm_repl_service.h
//

#include <repl/repl_mgr/rm_repl_service_in.h>

inline
trans_state_reg_rma_repl_prov::trans_state_reg_rma_repl_prov(
    rm_repl_service::prov_reg_impl *rp) : reg_impl_obj(rp)
{
}

inline
trans_state_reg_rma_repl_prov::~trans_state_reg_rma_repl_prov()
{
}

inline void
trans_state_reg_rma_repl_prov::orphaned(Environment &)
{
	reset_reg_impl();
}

inline void
trans_state_reg_rma_repl_prov::committed()
{
}

// called during orphaned
inline void
trans_state_reg_rma_repl_prov::reset_reg_impl()
{
	ASSERT(reg_impl_obj);
	reg_impl_obj = NULL;
}

inline rm_repl_service::prov_reg_impl *
trans_state_reg_rma_repl_prov::get_reg_impl()
{
	return (reg_impl_obj);
}

inline
trans_state_shutdown_service::trans_state_shutdown_service(
    rm_repl_service *svc, trans_state_shutdown_service::shutdown_state st,
    service_admin_impl *sa, bool was_forced_shutdown) :
	s(svc), sai(sa), state(st), forced_shutdown(was_forced_shutdown)
{
}

inline
trans_state_shutdown_service::~trans_state_shutdown_service()
{
}

inline void
trans_state_shutdown_service::orphaned(Environment &e)
{
	sai->shutdown_service(forced_shutdown, e);
}

inline void
trans_state_shutdown_service::committed()
{
	if (state == STARTED) {
		state = COMMITTED_FAILED;
		s->mark_shutdown_failed();
	} else {
		CL_PANIC(state == SUCCEEDED);
		s->mark_shutdown_committed();
		state = COMMITTED_SUCCESS;
	}
}

#endif	/* _RS_TRANS_STATES_IN_H */
