/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_MGR_IMPL_H
#define	_REPL_MGR_IMPL_H

#pragma ident	"@(#)repl_mgr_impl.h	1.69	08/05/20 SMI"

//
// repl_mgr_impl.h contains class definitions for the rm interface
// portion of the replica management framework.
//

#include <orb/object/adapter.h>
#include <h/replica_int.h>
#include <h/repl_rm.h>
#include <repl/service/replica_tmpl.h>
#include <repl/service/periodic_thread.h>
#include <repl/repl_mgr/repl_mgr_prov_impl.h>
#include <repl/repl_mgr/cb_coord_control_impl.h>

// entry point for per-service thread
static void *
start_routine(void *arg);

// The starting number for service ids outside of ha framework.
// RMM uses at least one sid.
#define	STARTING_SERVICE_ID	(10)
#define	INVALID_SERVICE_ID	(0)

// forward declarations
class repl_mgr_prov_impl;
class rm_repl_service;
class service_admin_impl;

class repl_mgr_impl : public mc_replica_of<replica_int::rm> {
public:
	//
	// Locked list of rma_reconf objects:  these objects
	// are added to the RM's list via add_rma(), and are
	// accessed during reconfiguration by rm_repl_service
	// class methods.  The RM's static method get_rcf_list()
	// returns a reference to this list.
	//
	// This list can be locked when the rm_repl_service
	// lock is held.
	//
	class rcf_list {
	public:
		void	lock();
		void	unlock();
		int	lock_held();

		void	locked_append(replica_int::rma_reconf_ptr p);
		bool	exists(const replica_int::rma_reconf_ptr p);

		void	atfirst();

		replica_int::rma_reconf_ptr current();
		replica_int::rma_reconf_ptr reapfirst();

		bool	empty();

		void	advance();

		void	erase(const replica_int::rma_reconf_ptr p);

	private:
		os::mutex_t	mutex;
		SList<replica_int::rma_reconf>	list;
	};

	// primary constructor
	repl_mgr_impl(repl_mgr_prov_impl *rm_prov);

	// secondary constructor
	repl_mgr_impl(replica_int::rm_ptr rm_prov,
	    repl_mgr_prov_impl *rm_prov_impl);

	~repl_mgr_impl();

	void _unreferenced(unref_t arg);

	void handle_become_spare();

	//
	// get_rcf_list() provides a a way for rm_repl_service
	// methods to access the RM's rma_reconf list.  Note
	// that there's at most only one instance of the RM
	// on a node.
	//
	static rcf_list &get_rcf_list();

	//
	// get_sai_list() provides a a way for service_admin
	// methods to access the RM's service_admin list.  Note
	// that there's at most only one instance of the RM
	// on a node.
	//
	static SList<service_admin_impl> &get_sai_list();

	//
	// Provides a way for rm_repl_service to delete itself from the
	// service list.
	//
	void erase_service(rm_repl_service *);

	void	read_services(
	    replica_int::init_service_seq &service_seq);

	bool	check_all_stable();

	void	wait_for_all_stable();

	void	wait_for_below_stable(const replica::service_dependencies &,
	    Environment &);

	// Return the RM pointer
	static repl_mgr_impl *get_rm();
	repl_mgr_prov_impl *get_rm_prov();

	void set_primary_mode();
	bool is_primary_mode();

	// Called on the primary during state change.
	void freeze_primary();
	void unfreeze_primary();
	void recover();

	// called during checkpoint on secondary
	void create_service_on_sec(
	    const char *new_service,
	    const replica::service_dependencies &this_depends_on,
	    replica::service_num_t sid,
	    uint32_t desired_numsecondaries,
	    bool is_deleted,
	    const replica::service_state sstate);

	void set_service_num_on_sec(replica::service_num_t sid);

	service_admin_impl *create_sai_on_sec(
	    replica::service_num_t sid,
	    replica_int::rm_service_admin_ptr sai);

	void create_rma_reconf_on_sec(
	    replica_int::rma_reconf_ptr client_rma);

	// Called after dumpstate completes.
	void dumpstate_complete();

	//
	// When shutting down a dead service, there is a cleanup task created
	// for the service. After the cleanup is done we need to remove the
	// task.
	//
	rm_repl_service *remove_cleanup_task(replica::service_num_t sid);

	// called during add_secondary() on RM

	// checkpoint all the rma reconf objects
	void add_sec_reconf_ckpts(repl_rm::rm_ckpt_ptr ckpt, Environment &e);

	// checkpoint all the services
	void add_sec_service_ckpts(repl_rm::rm_ckpt_ptr ckpt, Environment &e);

	// checkpoint all the service admin objects
	void add_sec_sai_ckpts(repl_rm::rm_ckpt_ptr ckpt, Environment &e);

	// checkpoint all the providers
	void add_sec_rma_repl_prov_ckpts(repl_rm::rm_ckpt_ptr ckpt,
		Environment &e);

	// checkpoint cb_coordinator object and all cb_coord_control objects
	void add_sec_cb_ckpts(repl_rm::rm_ckpt_ptr ckpt, Environment &e);

	// checkpoint service states in reconf
	void add_reconf_service_states_ckpts(repl_rm::rm_ckpt_ptr ckpt,
		Environment &e);

	// Idl interfaces
	void register_service(const char *new_service,
	    const replica::service_dependencies &this_depends_on,
	    replica::service_num_t &sid, uint32_t desired_numsecondaries,
	    replica::service_state sstate,
	    Environment &_environment);

	void get_services(replica::rm_service_list_out sl,
	    Environment &_environment);

	replica::service_admin_ptr get_control(const char *service_name,
	    Environment &_environment);

	void	add_rma(replica_int::rma_reconf_ptr client_rma,
		    Environment &_env);

	void	cleanup_dead(Environment &);

	void 	freeze_for_upgrade(const char *service, Environment &_env);
	void 	unfreeze_after_upgrade(const char *service, Environment &_env);
	void	get_services_frozen_for_upgrade(
		    replica::service_frozen_seq_out svcs, Environment &_env);

	//
	// This is called by the VM(version manager) in order to create a
	// cb_coord_control object and the linkage between it and the LCA(local
	// callback agent) object.  There is one pair of cb_coord_control and
	// lca object per node in the cluster.
	//
	replica_int::cb_coord_control_ptr  register_lca(
	    version_manager::vm_lca_ptr lcaobj_p, Environment &e);

	// Perform rolling upgrade commit
	void	upgrade_commit(version_manager::vp_state &s,
		    version_manager::membership_bitmask_t &outdated_nodes,
		    Environment &e);

	void	set_upgrade_commit_progress(
		    version_manager::commit_progress new_progress,
		    Environment &e);

	// Unregister UCCs registered with cb_coordinator
	void	unregister_uccs(Environment &e);

	void	_generic_method(CORBA::octet_seq_t &data,
		    CORBA::object_seq_t &objs, Environment &e);

	//
	// Called by ckpt_register_lca() to create cb_coord_control
	// object on RM secondaries.
	//
	cb_coord_control_impl	*register_lca_on_sec(
	    version_manager::vm_lca_ptr lca_p,
	    replica_int::cb_coord_control_ptr cbcntl_p);

	//
	// Returns true when check_service_dependencies gets a definite
	// answer. Returns false when it can't get a valid answer because
	// one service it depends isn't stable yet.
	//
	bool check_service_dependencies(
	    const replica::service_dependencies &depends_on,
	    Environment &e);

	rm_repl_service *add_service(const char *nm,
	    const replica::service_dependencies &depends_on,
	    replica::service_num_t sid,
	    uint32_t desired_numsecondaries,
	    const replica::service_state sstate);

	rm_repl_service *get_service(const replica::service_num_t num);

	rm_repl_service *get_service_incl_deleted(
	    const replica::service_num_t sid);

	//
	// return pointer to service object in service_list
	// whose name is nm
	//
	rm_repl_service *get_service(const char *nm);

	// Checkpoint has arrived indicating that an add_rma() has begun.
	void add_rma_started_ckpt(replica_int::rma_reconf_ptr rma_p);

	void add_rma_done_ckpt();

	class add_service_recovery_data {
	public:
		add_service_recovery_data(
		    rm_repl_service		*s,
		    replica_int::rma_reconf_ptr rcf_p);

		~add_service_recovery_data();

		replica_int::rma_reconf_ptr &get_rcf();

		rm_repl_service		    *get_service();
	private:
		rm_repl_service			*svc;
		replica_int::rma_reconf_ptr	reconf_obj;

		// Disallow
		add_service_recovery_data();
	};

	// Returns true if the primary is frozen.
	bool	primary_is_frozen();

	//
	// Used when an unreferenced is received while the primary is frozen.
	// Holds a temporary reference to an object until the primary is
	// unfrozen.
	//
	void	hold_tmp_reference(replica::repl_prov_reg_ptr);

	void	unref_freeze_delay();

	// Checkpoint accessor function.
	repl_rm::rm_ckpt_ptr	get_checkpoint();

	// return the pointer to the cb_coordinator
	cb_coordinator	*get_cb_coordinator();

	void	add_rma(const char *sec_name);
	void	empty_reconf_list();
private:

	void fill_sid_seq(replica_int::service_num_seq &num_seq);

	void copy_service_data(replica_int::service_num_seq &sid_seq,
	    replica_int::rma_service_state_seq &state_seq,
	    replica_int::rma_reconf_ptr rcf_p);

	void start_service_threads();

	trans_state_register_service	*
	    get_saved_primary_reg_service_state(Environment &);

	trans_state_get_control	*
	    get_saved_primary_get_control_state(Environment &);

	trans_state_add_rma	*
	    get_saved_primary_add_rma_state(Environment &);

	bool dependent_frozen_for_upgrade(const
	    replica::service_dependencies &);

	//
	// The task that's used to cleanup hxdoor_service on all nodes
	// for the services that are dead and shutdown. It's run periodically.
	//
	class periodic_cleanup_task : public periodic_thread::periodic_job {
		friend class repl_mgr_impl;
	public:
		// Add/remove a service to the to be cleaned up list.
		void append(rm_repl_service *);
		rm_repl_service *remove(replica::service_num_t);

		//
		// Called by the periodic_thread when it wants to run this
		// task.
		//
		void execute();

		//
		// Tell the periodic_thread how often we want to run.
		// Return value in unit of 10 seconds.
		//
		uint_t get_period();

		~periodic_cleanup_task();
	private:
		void lock();
		void unlock();

		// The list of services to be cleaned up.
		SList<rm_repl_service> services;

		// protects the list.
		os::mutex_t lck;
	};

	periodic_cleanup_task cleanup_task;

	//
	// rm_repl_service object is created and appended
	// to service_list during service registration
	//
	SList<rm_repl_service>	service_list;

	// locked list of rma_reconf objects
	rcf_list		rma_reconf_list;

	//
	// When we receive an add_rma checkpoint we keep track of the reference
	// here. During recovery we check if init_services has completed on
	// this. If so, we add it to the list for recovery. If not, we allow
	// the add_rma() retry to complete things.
	//
	replica_int::rma_reconf_ptr adding_rma;

	//
	// wrapper for thread_create:  create the per-service
	// thread for service svc
	//
	void rm_service_thread_create(rm_repl_service *svc);

	bool check_all_recovered();
	void wait_for_recovery_completion();

	void recovery_finish(
	    SList<add_service_recovery_data> *add_s_list);

	void finish_add_service(
	    SList<add_service_recovery_data> *add_s_list);

	//
	// Find the next service id to be assigned to a registering
	// service.
	//
	replica::service_num_t		get_next_service_id();

	//
	// last service number assigned:  next service to register
	// will increment cur_num
	//
	replica::service_num_t		cur_num;

	// Replica data

	// set in constructor and in become_primary()
	bool			primary_mode;
	repl_mgr_prov_impl	*prov_impl;

	// list of service_admin_impl objects that have been created
	SList<service_admin_impl> sai_list;

	//
	// The primary is frozen. This means that the deferred_ref_list
	// should be used for unreferenced notifications on repl_prov_reg_impl
	// objects.
	//
	bool	primary_frozen;

	//
	// List of replica::repl_prov_reg objects that
	// received unreferenced while primary_frozen is true. This list is
	// emptied and the references released when the RM is unfrozen.
	//
	SList<replica::repl_prov_reg>	deferred_ref_list;

	// callback coordinator
	cb_coordinator			cb_coord;

#ifdef _KERNEL
	//
	// The bufcall id and time id. A value of zero tells us that there
	// is nothing scheduled. At most one of them can be non-zero at
	// one time. They are protected by the state_mutex.
	//
	bufcall_id_t bufcallid;
	timeout_id_t timeoutid;

	// The function bufcall calls.
	static void bufcallback(void *);

	// Create threads for all the services that don't have threads yet.
	void create_service_threads();
#endif // _KERNEL

	// Disallow
	repl_mgr_impl();
};

#include <repl/repl_mgr/repl_mgr_impl_in.h>

#endif	/* _REPL_MGR_IMPL_H */
