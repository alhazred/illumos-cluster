/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CB_COORD_CONTROL_IMPL_H
#define	_CB_COORD_CONTROL_IMPL_H

#pragma ident	"@(#)cb_coord_control_impl.h	1.12	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

//
// cb_coord_control_impl.h contains class definitions for callback interface
// in the rolling upgrade framework.
//

#include <repl/repl_mgr/repl_mgr_prov_impl.h>
#include <vm/ucc_group.h>
#include <sys/hashtable.h>
#include <sys/list_def.h>

// The forward definition needed by the cb_coordinator class.
class cb_coord_control_impl;
class upgrade_commit_task;

//
// Callback coordinator class. This object is replicated on all RM secondaries
// and it keeps track of registered lcas (local callback agent) and
// UCCs (upgrade callback class).
//
class cb_coordinator {
	friend upgrade_commit_task;
public:
	// Perform upgrade commit.
	void	upgrade_commit(version_manager::vp_state &s,
		    version_manager::membership_bitmask_t &outdated_nodes,
		    Environment &e);
	void	set_local_upgrade_commit_progress(
		    version_manager::commit_progress new_progress);

	// Register an individual ucc.
	void	register_ucc(const version_manager::ucc &new_ucc,
	    Environment &e);

	// Unregister UCCs
	void	unregister_uccs(Environment &e);

	// Add a new cb_coord_control that is associated with a new node.
	void	add_cb_coord_control(cb_coord_control_impl *new_cbcntlp);

	// Remove the cb_coord_control that is associated with a dead node.
	void	remove_cb_coord_control(cb_coord_control_impl *dead_cbcntlp);

	//  A lca has completed its most recent batched callback task.
	void	callback_task_done_on_one_lca(uint32_t callback_task_id);

	// Indicate whether there is an upgrade commit in progress.
	bool	is_upgrade_commit_in_progress();

	// Perform lock_vps on the newly joined RM secondary if necessary.
	void	lock_vps_on_new_sec();

	//
	// Signal the upgrade commit callback task thread after new rvs and uvs
	// have been calculated by the version manager during upgrade commit.
	//
	void	signal_cb_coordinator(cmm::seqnum_t cur_upgrade_seq);

	// Called by repl_mgr_impl::recover during RM recovery.
	void	recover(Environment &e);

	// Dumpstate method.
	void	add_sec_cb_ckpts(repl_rm::rm_ckpt_ptr ckpt, Environment &e);

	// Helper functions for using the rwlock.
	void	wrlock_rwlock();
	void	rdlock_rwlock();
	void	unlock_rwlock();

	// Constructor
	cb_coordinator();
	// Destructor
	~cb_coordinator();

#ifdef DEBUG
	//
	// Debug hook used by query_vm to print out the current ucc hashtable
	// to the outstring.
	void	debug_print(char **);

	//
	// Debug hook used by query_vm to construct, print to the outstring,
	// and then destroy the current ucc_group list.
	//
	void	debug_print_uglist(char **);
#endif

	// Remove the named ucc if it exists.
	void	remove_ucc(const char *ucc_name);

	//
	// Remove the list of uccs.  If the ucc does not exist, then skip and
	// go on to the next ucc.
	//
	void	remove_uccs(const version_manager::string_seq_t & ucc_list);
private:
	// UCC Hashtable.
	string_hashtable_t<version_manager::ucc *>	ucc_hashtable;

	//
	// This lock protects variables last_upgrade_seq,
	// num_lcas_doing_callback.
	//
	os::mutex_t	lck;
	os::condvar_t	cv;

	//
	// This lock protects read/write of cb_coord_control_impl,
	// ucc_hashtable and upgrade_commit_progress.
	// It can be treated like a mutex lock at most places, the reason
	// we use read write lock here is that some places we need to
	// make idl invocations or send ckpt with lock held.
	//
	os::rwlock_t	rwlck;

	//
	// Most recent upgrade_seq in the version manager.  It is protected by
	// upgrade_lck.
	//
	cmm::seqnum_t	last_upgrade_seq;

	//
	// cb_coord_control_impl list, each cb_coord_control_impl is
	// associated with a lca in cluster.
	//
	SList<cb_coord_control_impl>	cb_coord_control_list;

	//
	// Total number of lcas that are currently processing callback
	// request, this variable only gets used on RM primary.
	//
	uint_t	num_lcas_doing_callback;

	// Flag keeps track of current upgrade commit progress.
	version_manager::commit_progress	upgrade_commit_progress;

	// Helper function to invoke an idl function on all lca.
	void    iterate_over(void (version_manager::vm_lca::*mfn)(
		    Environment &e), const char *const fn_name);

	//
	// The list of ucc_groups is constructed via construct_ucc_group_list()
	// when a commit is started or when a failover occurs.
	// The ucc_group_list is constructed directly from the ucc_hashtable.
	// Having a list of ucc_groups accomodates the case where multiple
	// concurrent uccs must be upgraded at the same time.  This way we
	// can specify that an entire group of ucc's must be upgraded together.
	// This	list must be checkpointed on secondaries.
	//
	IntrList<ucc_group, _SList>	ucc_group_list;

	//
	// Methods used for accessing ucc groups.
	// Verify that the ucc's correctness before registration.
	//
	bool	verify_ucc_registration(const version_manager::ucc &new_ucc,
	    Environment &e);

	// Construct the ucc_group_list from ucc_hashtable.
	void	construct_ucc_group_list();

	// Remove the complete ucc_group_list.
	void	destroy_ucc_group_list();

	//
	// Select, remove and return a list of all unneeded uccs (associated
	// with both bootstrap and non-bootstrap cluster vps) after an
	// upgrade commit.  NOTE: Please see the .cc file for more details on
	// the removal.
	//
	void	select_and_remove_uccs(
	    version_manager::string_seq_t & out_list);

	//
	// A supportive method called by select_and_remove_uccs() that removes
	// the ucc 'u' from the hashtable and then searches through the
	// remaining uccs to remove any dependencies (Before, Concurrently)
	// to 'u'.
	//
	void	scrub_ucc(version_manager::ucc *u);

	// Get the next ucc to upgrade.
	ucc_group	*get_next_ucc_group(uint_t last);

	//
	// Returns true if and only if the data fields in both ucc's are
	// identical.
	//
	bool	ucc_isequal(version_manager::ucc a, version_manager::ucc b);

	//
	// Returns true if any of the uccs inside ucc_names are contained inside
	// the subtree of 'u'.  Ucc's that must be concurrent with 'u' are
	// part of this subtree.
	//
	bool	contained_inside(version_manager::ucc * u,
	    CORBA::StringSeq ucc_names, char ** found_ucc);

	//
	// This is a support method for contained_inside().
	// Append to the list all uccs that are concurrent to u.  The list is
	// guaranteed not to have multiple entries and does not include 'u'
	// itself.
	//
	void	get_concurrent(SList <version_manager::ucc> & list,
	    version_manager::ucc * u);

	//
	// This is a supporting method for construct_ucc_group_list().
	// This returns true if the ucc name is found inside a list of
	// ucc names.
	//
	bool	found_ucc_name(CORBA::StringSeq ucc_list, char *ucc_name);

	// Returns true if the ucc u is found in list.
	bool	found_in_list(SList<version_manager::ucc> & list,
	    version_manager::ucc * u);

	//
	// Returns a pointer to the ucc_group that contains the ucc u.
	// If u is not contained in any ucc_group, then return NULL.
	//
	ucc_group	*belongs_to_group(version_manager::ucc * u);

	//
	// First step in construct_ucc_group_list().  This places
	// all concurrent ucc's into the same ucc_group instance.  That way
	// all the concurrent ucc's can be upgraded together.
	//
	void	convert_hash_to_list();

	//
	// The second step in construct_ucc_group_list().
	// Order the upgrade list of ucc_groups by analyzing the before
	// dependencies.
	//
	void	order_ucc_groups();

	//
	// The third step in construct_ucc_group_list().
	// Traverse the ordered ucc_groups and label each ucc_group with
	// its thaw information.  Please note the actual thaw does not occur
	// here, rather that information is determined and stored in this step.
	//
	void	mark_thaw_ucc_groups();

	//
	// This is a supporting method for order_ucc_groups().
	// Optimize a ucc_group list by combining all ucc_groups with the
	// same index value and by sorting the final ucc_groups in ascending
	// order.
	//
	void	optimize_ug_list(IntrList<ucc_group, _SList> & ug_list);

	//
	// This is a supporting method for mark_thaw_ucc_groups().
	// Process a single ucc_group to see if it has any HA services that
	// can be marked as thawed.
	//
	void	mark_thaw(ucc_group * ug, uint_t index);

	//
	// This is a supporting method for mark_thaw().
	// Manipulate the ordered ucc_group_list so its current pointer is
	// residing on the i'ths ucc_group and return true.
	// If there is no i'th ucc_group, return false.
	//
	bool	get_ug(uint_t i);

	//
	// This is a supporting method for convert_hash_to_list().
	// Check the ucc_group_list and update any before ugs pointer lists
	// that point to the soon-to-be-deleted ucc_group.
	//
	void	update_before_ugs(ucc_group *, ucc_group *);
};

//
// There is the Callback Coordinator control class.  There is one of these
// (HA) objects for each vm_lca object in order to detect node failures via
// unreference delivery.
//
class cb_coord_control_impl :
    public mc_replica_of<replica_int::cb_coord_control> {

public:
	// Primary constructor.
	cb_coord_control_impl(repl_mgr_prov_impl *rm_prov_inp,
	    const version_manager::vm_lca_ptr lca_in_p,
	    cb_coordinator *cbc_inp);

	// Secondary constructor.
	cb_coord_control_impl(replica_int::cb_coord_control_ptr cbprov_in_p,
	    const version_manager::vm_lca_ptr lca_in_p,
	    cb_coordinator *cbc_inp);

	// Destructor.
	~cb_coord_control_impl();

	void	_unreferenced(unref_t);

	version_manager::vm_lca_ptr	get_lca();

	// Checkpoint accessor function.
	repl_rm::rm_ckpt_ptr	get_checkpoint();

	// Dumpstate method.
	void	add_sec_cb_ckpts(repl_rm::rm_ckpt_ptr ckpt_p, Environment &e);

	// Invoke callbacks on the associated lca.
	bool	do_callback_task(
		    const version_manager::string_seq_t &ucc_seq,
		    const version_manager::vp_version_seq &new_version_seq,
		    const uint32_t task_id);

	bool	is_callback_in_progress();

	// IDL interfaces

	//
	// This is called from vm_lca_impl to register a ucc associated with
	// a non-bootstrap vp protocol.
	//
	void	register_ucc(
		    const version_manager::ucc &new_ucc, Environment &e);

	//
	// This is called from vm_lca_impl to notify that callback tasks on
	// that LCA have been completed.
	//
	void	callback_task_done(uint32_t, Environment &e);

	void	_generic_method(CORBA::octet_seq_t &data,
		    CORBA::object_seq_t &objs, Environment &e);

	// Helper functions for using the rwlock.
	void	rdlock_rwlock();
	void	unlock_rwlock();

private:
	// Pointer to the cb_coordinator object.
	cb_coordinator	*cbcp;

	// Corba reference to lca.
	version_manager::vm_lca_var	lca_v;

	// This lock protects all local variables.
	os::rwlock_t	rwlck;

	//
	// Save the task ID for an outstanding call to do_callback_task().
	// This is only used on the primary to match with calls to
	// callback_task_done().
	//
	uint32_t	callback_in_progress;
};

class upgrade_commit_task : public defer_task {
public:
	upgrade_commit_task(bool is_retry);
	~upgrade_commit_task();
	void execute();
private:
	//
	// This indiates whether we are restarting on a new RM primary and
	// doing the first retry. The purpose of having this flag is to avoid
	// unecessary duplicated ckpt on a retrying RM.
	//
	bool	retry;

	// Indicate whether we have locked vps on all nodes.
	bool	vps_locked;
};

#include <repl/repl_mgr/cb_coord_control_impl_in.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* _CB_COORD_CONTROL_IMPL_H */
