/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _REPL_MGR_IMPL_IN_H
#define	_REPL_MGR_IMPL_IN_H

#pragma ident	"@(#)repl_mgr_impl_in.h	1.25	08/05/20 SMI"

//
// repl_mgr_impl_in.h contains inline functions for repl_mgr_impl and
// repl_mgr_impl::rcf_list class.
//

#include <repl/repl_mgr/rm_state_mach.h>

class repl_mgr_prov_impl;
class rm_repl_service;

inline bool
repl_mgr_impl::primary_is_frozen()
{
	return (primary_frozen);
}

inline void
repl_mgr_impl::rcf_list::lock()
{
	mutex.lock();
}

inline void
repl_mgr_impl::rcf_list::unlock()
{
	mutex.unlock();
}

inline int
repl_mgr_impl::rcf_list::lock_held()
{
	return (mutex.lock_held());
}

inline void
repl_mgr_impl::rcf_list::locked_append(replica_int::rma_reconf_ptr p)
{
	list.append(p);
}

inline void
repl_mgr_impl::rcf_list::atfirst()
{
	ASSERT(lock_held());
	list.atfirst();
}

inline replica_int::rma_reconf_ptr
repl_mgr_impl::rcf_list::current()
{
	ASSERT(lock_held());
	return (list.get_current());
}

inline replica_int::rma_reconf_ptr
repl_mgr_impl::rcf_list::reapfirst()
{
	return (list.reapfirst());
}

inline bool
repl_mgr_impl::rcf_list::empty()
{
	return (list.empty());
}

inline void
repl_mgr_impl::rcf_list::advance()
{
	ASSERT(lock_held());
	list.advance();
}

inline void
repl_mgr_impl::rcf_list::erase(const replica_int::rma_reconf_ptr p)
{
	ASSERT(lock_held());
	list.erase(p);
}

inline bool
repl_mgr_impl::rcf_list::exists(const replica_int::rma_reconf_ptr p)
{
	ASSERT(lock_held());
	return (list.exists(p));
}

// called from become_primary() only
inline void
repl_mgr_impl::set_primary_mode()
{
	primary_mode = true;
}

inline bool
repl_mgr_impl::is_primary_mode()
{
	return (primary_mode);
}

inline repl_mgr_prov_impl *
repl_mgr_impl::get_rm_prov()
{
	return (prov_impl);
}


inline
repl_mgr_impl::add_service_recovery_data::add_service_recovery_data(
    rm_repl_service		*s,
    replica_int::rma_reconf_ptr rcf_p) :
    svc(s),
    reconf_obj(replica_int::rma_reconf::_duplicate(rcf_p))
{
}

inline
repl_mgr_impl::add_service_recovery_data::~add_service_recovery_data()
{
	CORBA::release(reconf_obj);
}

inline replica_int::rma_reconf_ptr &
repl_mgr_impl::add_service_recovery_data::get_rcf()
{
	return (reconf_obj);
}

inline rm_repl_service	*
repl_mgr_impl::add_service_recovery_data::get_service()
{
	return (svc);
}

// can return NULL state
inline trans_state_register_service	*
repl_mgr_impl::get_saved_primary_reg_service_state(
    Environment &e)
{
	primary_ctx			*ctxp
	    = primary_ctx::extract_from(e);
	trans_state_register_service	*st
	    = (trans_state_register_service *) ctxp->get_saved_state();

	return (st);
}

// can return NULL state
inline trans_state_get_control	*
repl_mgr_impl::get_saved_primary_get_control_state(
    Environment &e)
{
	primary_ctx			*ctxp
	    = primary_ctx::extract_from(e);
	trans_state_get_control	*st
	    = (trans_state_get_control *) ctxp->get_saved_state();

	return (st);
}

// can return NULL state
inline trans_state_add_rma	*
repl_mgr_impl::get_saved_primary_add_rma_state(
    Environment &e)
{
	primary_ctx			*ctxp
	    = primary_ctx::extract_from(e);
	trans_state_add_rma	*st
	    = (trans_state_add_rma *) ctxp->get_saved_state();

	return (st);
}

inline void
repl_mgr_impl::periodic_cleanup_task::lock()
{
	lck.lock();
}

inline void
repl_mgr_impl::periodic_cleanup_task::unlock()
{
	lck.unlock();
}

inline cb_coordinator *
repl_mgr_impl::get_cb_coordinator()
{
	return (&cb_coord);
}

inline repl_rm::rm_ckpt_ptr
repl_mgr_impl::get_checkpoint()
{
	return ((repl_mgr_prov_impl*)(get_provider()))->
	    get_checkpoint_rm_ckpt();
}

#endif	/* _REPL_MGR_IMPL_IN_H */
