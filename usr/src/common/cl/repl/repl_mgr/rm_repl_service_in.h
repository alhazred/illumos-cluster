/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_REPL_SERVICE_IN_H
#define	_RM_REPL_SERVICE_IN_H

#pragma ident	"@(#)rm_repl_service_in.h	1.54	08/05/20 SMI"

#include <repl/repl_mgr/rm_state_mach.h>

//
// rm_repl_service_in.h contains inline functions for rm_repl_service
// and its nested classes.
//

// secondary constructor is in .cc file because inlines were
// making code too large

inline
rm_repl_service::prov_reg_impl::~prov_reg_impl()
{
	// Note that svc might have just been deleted.
	RM_DBG(("rm_rs : destructing prov_reg_impl %p, "
	    "releasing rma_prov ref %p, checkpoint ref %p\n",
	    this, r_prov, s_chkpt));

	ASSERT(!CORBA::is_nil(r_prov));
	CORBA::release(r_prov);
	r_prov = nil;

	ASSERT(!CORBA::is_nil(s_chkpt));
	CORBA::release(s_chkpt);
	s_chkpt = nil;

	delete r_prov_desc;
	svc = NULL;
}

inline void
rm_repl_service::prov_reg_impl::set_repl_prov_state(
    const replica::repl_prov_state new_state)
{
	CL_PANIC(rm_state_machine::state_lock_held());
	r_prov_state = new_state;
}

inline rm_repl_service *
rm_repl_service::prov_reg_impl::get_service() const
{
	return (svc);
}

inline const replica::checkpoint_ptr
rm_repl_service::prov_reg_impl::get_sec_chkpt() const
{
	return (s_chkpt);
}

inline bool
rm_repl_service::prov_reg_impl::is_down() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (r_prov_state == replica::AS_DOWN);
}

inline bool
rm_repl_service::prov_reg_impl::is_spare() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (r_prov_state == replica::AS_SPARE);
}

//
// service ref count decremented *only* in handle unreferenced.
//
inline void
rm_repl_service::prov_reg_impl::mark_down()
{
	CL_PANIC(rm_state_machine::state_lock_held());

	set_sm_prov_state(0);
	set_repl_prov_state(replica::AS_DOWN);
}

// returns an exception if service down when woken up
inline void
rm_repl_service::reconf_complete_wait(Environment &_environment)
{
	CL_PANIC(rm_state_machine::state_lock_held());
	CL_PANIC(!(_environment.exception()));
	wait_for_stable();
	chk_s_down(_environment);
}


// called from checkpoint code on secondary
inline void
rm_repl_service::mark_shutdown_intention(prov_reg_impl *primary)
{
	flags |= SHUTDOWN_INTENTION;
	shutdown_primary = primary;
}

inline bool
rm_repl_service::shutdown_intention_marked()
{
	return ((flags & rm_repl_service::SHUTDOWN_INTENTION) != 0);
}

// called from remove_depedencies()
inline void
rm_repl_service::mark_lower_service_shutdown()
{
	//
	// Lower service has been shut down first. This can happen on
	// dead services.
	//
	flags |= LOWER_SERVICE_SHUTDOWN;
}

inline bool
rm_repl_service::is_lower_service_shutdown()
{
	return ((flags & LOWER_SERVICE_SHUTDOWN) != 0);
}

inline void
rm_repl_service::mark_freeze_for_upgrade()
{
	flags |= FREEZE_FOR_UPGRADE;
}

inline bool
rm_repl_service::is_frozen_for_upgrade() const
{
	return (flags & FREEZE_FOR_UPGRADE);
}

inline bool
rm_repl_service::frozen_for_upgrade_by_req() const
{
	return (flags & FROZEN_FOR_UPGRADE_BY_REQ);
}

inline void
rm_repl_service::mark_unfreeze_after_upgrade()
{
	flags &= ~FREEZE_FOR_UPGRADE;
}

inline uint32_t
rm_repl_service::get_desired_numsecondaries() const
{
	return (desired_numsecondaries);
}

//
// map_service_state
//
// Map service state into one convenient state if the service is
// down. We do not expose states S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ and
// S_DOWN_FROZEN_FOR_UPGRADE to rest of the code. They are used only
// to tell a registering service if its previous instance was frozen
// for upgrade when it went down. All other state transitions for a
// service that is in either of these states are identical if the
// service was simply down and not frozen. The frozen-ness of a
// service is considered only in a few rm_state_machine states and it
// is desirable not to introduce mostly duplicate code to represent
// these states in the state machine.
//
inline replica::service_state
rm_repl_service::map_service_state() const
{
	replica::service_state sstate = get_state();

	if (sstate == replica::S_DOWN) {
		if (frozen_for_upgrade_by_req()) {
			sstate = replica::S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ;
		} else if (is_frozen_for_upgrade()) {
			sstate = replica::S_DOWN_FROZEN_FOR_UPGRADE;
		}
	}
	return (sstate);
}

//
// inline methods for the RM recovery classes.
//
inline
rm_repl_service::service_recovery_data::service_recovery_data(
    replica_int::rma_reconf_ptr rcf_p,
    replica_int::rma_service_state s_state) :
    reconf_obj(replica_int::rma_reconf::_duplicate(rcf_p)),
    last_state(s_state)
{
}

inline
rm_repl_service::service_recovery_data::~service_recovery_data()
{
	CORBA::release(reconf_obj);
	reconf_obj = nil;
}

inline replica_int::rma_reconf_ptr
rm_repl_service::service_recovery_data::get_rcf()
{
	return (reconf_obj);
}

inline replica_int::rma_service_state
rm_repl_service::service_recovery_data::get_last_state()
{
	return (last_state);
}

inline void
rm_repl_service::service_recovery_data::set_last_state(
    const replica_int::rma_service_state s_state)
{
	last_state = s_state;
}

inline
rm_repl_service::provider_recovery_data::provider_recovery_data(
    prov_reg_impl *p, replica_int::rma_repl_prov_state p_state,
    replica_int::repl_prov_exception p_e) :
    prov(p), last_state(p_state), rp_exception(p_e)
{
}

inline
rm_repl_service::provider_recovery_data::~provider_recovery_data()
{
	prov = NULL;
}

inline
rm_repl_service::prov_reg_impl *
rm_repl_service::provider_recovery_data::get_rp()
{
	return (prov);
}

inline
replica_int::rma_repl_prov_state
rm_repl_service::provider_recovery_data::get_last_state()
{
	return (last_state);
}

inline
replica_int::repl_prov_exception
rm_repl_service::provider_recovery_data::get_rp_exception()
{
	return (rp_exception);
}

inline service_state_count
rm_repl_service::get_service_state_count()
{
	return (state_count);
}

inline void
rm_repl_service::update_service_admin_count(service_admin_impl *sap)
{
	ASSERT(sap);
	sap->set_state_count(state_count);
}

inline bool
rm_repl_service::is_busy() const
{
	return (get_state() != replica::S_UNINIT);
}

inline bool
rm_repl_service::is_uninitialized() const
{
	return (get_state() == replica::S_UNINIT);
}

//
// incr_state_count() -- increment the service state count
// without updating the service admin obj.
//
inline void
rm_repl_service::incr_state_count()
{
	++state_count;
	CL_PANIC(state_count != 0);
}

inline
rm_repl_service::~rm_repl_service()
{
	CL_PANIC(count == 0);
	CL_PANIC(repl_prov_list.empty());
	CL_PANIC(deps_down.empty());

	CL_PANIC(shutdown_primary == NULL);
	shutdown_primary = NULL;
	delete[] name;
	delete smp;
#ifdef RM_DBGBUF
	delete[] debug_id;
#endif
}

inline bool
rm_repl_service::is_primary(replica_int::rma_repl_prov_ptr rp)
{
	return (smp->get_primary()->get_rma_repl_prov_ptr()->_equiv(rp));
}

inline replica_int::rma_repl_prov_ptr
rm_repl_service::get_primary_ref()
{
	prov_reg_impl	*p = smp->get_primary();

	ASSERT(p != NULL);
	return (replica_int::rma_repl_prov::_duplicate(
	    p->get_rma_repl_prov_ptr()));
}

inline void
rm_repl_service::chk_get_root_obj_s_state(Environment &_environment)
{
	CL_PANIC(!(_environment.exception()));
	chk_s_uninit(_environment);
	if (!(_environment.exception())) {
		chk_s_down(_environment);
	}
}

// busy service is anything but uninitialized
inline void
rm_repl_service::chk_s_busy(Environment &_environment)
{
	if (is_busy()) {
		replica::service_busy	*sb = new
		    replica::service_busy;
		_environment.exception(sb);
		RM_DBG(("rm_rs %s: busy service exception\n", id()));
	}
}

inline bool
rm_repl_service::need_thread()
{
	return ((flags & NEED_THREAD) != 0);
}

inline void
rm_repl_service::thread_created()
{
	flags &= ~NEED_THREAD;
}

inline bool
rm_repl_service::is_bottom_service()
{
	return (deps_down.empty());
}

inline void
rm_repl_service::mark_deleted()
{
	flags |= DELETED;
}

inline bool
rm_repl_service::is_deleted()
{
	return ((flags & DELETED) != 0);
}

inline void
rm_repl_service::mark_dead()
{
	flags |= DEAD;
}

inline bool
rm_repl_service::is_dead()
{
	return ((flags & DEAD) != 0);
}

inline void
rm_repl_service::set_doing_switchover(bool new_value)
{
	doing_switchover = new_value;
}

inline bool
rm_repl_service::is_doing_switchover() const
{
	return (doing_switchover);
}

inline const char*
rm_repl_service::switchover_info_t::get_switchover_from() const
{
	return (switchover_from_primary);
}

inline const char*
rm_repl_service::switchover_info_t::get_switchover_to() const
{
	return (switchover_to_primary);
}

#endif	/* _RM_REPL_SERVICE_IN_H */
