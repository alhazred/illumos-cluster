/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_MGR_PROV_IMPL_H
#define	_REPL_MGR_PROV_IMPL_H

#pragma ident	"@(#)repl_mgr_prov_impl.h	1.28	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

//
// repl_mgr_prov_impl.h contains class definitions for the replica
// manager provider (repl_server) interface.
//

#include <orb/object/adapter.h>
#include <h/repl_rm.h>
#include <repl/service/replica_tmpl.h>

// Replica Manager Manager (RMM) instantiates this class.

// forward declarations
class repl_mgr_impl;
class trans_state_register_service;
class trans_state_reg_rma_repl_prov;
class trans_state_shutdown_service;
class trans_state_get_control;
class trans_state_add_rma;

class repl_mgr_prov_impl : public repl_server<repl_rm::rm_ckpt> {
public:
	repl_mgr_prov_impl(const char *id);
	~repl_mgr_prov_impl();

	// Required functions for replica framework

	// become_secondary() is never called on the RM.
	void become_secondary(Environment &_environment);
	void add_secondary(replica::checkpoint_ptr sec_chkpt,
	    const char *secondary_name, Environment &_environment);
	void remove_secondary(const char *secondary_name,
	    Environment &_environment);
	void freeze_primary_prepare(Environment &_environment);
	void freeze_primary(Environment &_environment);
	void unfreeze_primary(Environment &_environment);
	void become_primary(const replica::repl_name_seq &secondary_names,
	    Environment &_environment);
	void become_spare(Environment &_environment);
	void shutdown(Environment &_environment);
	CORBA::Object_ptr get_root_obj(Environment &_environment);

	void upgrade_callback(const version_manager::vp_version_t &v);

	void get_running_version(version_manager::vp_version_t &v);

	// Idl checkpoint interfaces

	void ckpt_register_service(const char *new_service,
	    const replica::service_dependencies &this_depends_on,
	    replica::service_num_t sid, uint32_t desired_numsecondaries,
	    bool is_deleted, replica::service_num_t new_cur_num,
	    replica::service_state sstate,
	    Environment &_environment);

	void ckpt_change_desired_numsecondaries(replica::service_num_t sid,

	    uint32_t new_desired_numsecondaries, Environment &);
	void ckpt_get_control(replica::service_num_t sid,
	    replica_int::rm_service_admin_ptr sai,
	    Environment &_environment);

	void ckpt_add_rma(replica_int::rma_reconf_ptr client_rma,
	    Environment &_environment);

	void ckpt_reg_rma_repl_prov(replica::service_num_t sid,
	    replica_int::rma_repl_prov_ptr prov, const char *prov_desc,
	    const replica::prov_dependencies &this_depends_on,
	    replica::checkpoint_ptr chkpt,
	    uint_t priority,
	    replica::repl_prov_reg_ptr new_reg_obj,
	    Environment &_environment);

	void ckpt_change_repl_prov_priority(replica::service_num_t sid,
	    const char *prov_desc, uint32_t new_priority, Environment &);
	void ckpt_switchover_service(const char *primary_from,
	    const char *primary_to, replica::service_num_t sid,
	    Environment &_environment);

	//
	// Version 0 checkpoint method to checkpoint a service shutdown.
	//
	void ckpt_shutdown_service(replica::service_num_t sid,
	    replica::repl_prov_reg_ptr primary, replica::service_admin_ptr sa,
	    Environment &_environment);
	//
	// Version 1 checkpoint method to checkpoint a service shutdown.
	//
	void ckpt_shutdown_service_v1(replica::service_num_t sid,
	    replica::repl_prov_reg_ptr primary, replica::service_admin_ptr sa,
	    bool was_forced_shutdown, Environment &_environment);
	void ckpt_shutdown_succeeded(replica::service_num_t sid,
	    replica::service_admin_ptr sa, Environment &_environment);


	void ckpt_cleanup_dead_service(replica::service_num_t sid,
	    Environment &_environment);

	void ckpt_rm_obj(replica_int::rm_ptr rm_obj,
	    Environment &_environment);
	void ckpt_dumpstate_complete(
	    Environment &_environment);
	void ckpt_register_lca(version_manager::vm_lca_ptr lca_p,
	    replica_int::cb_coord_control_ptr cbcntl_p,
	    Environment &_environment);
	void ckpt_register_ucc(const version_manager::ucc &u,
	    Environment &_environment);
	void ckpt_dump_cb_coordinator(const version_manager::lca_seq_t &lca_seq,
	    const replica_int::cbcntl_seq_t &cbcntl_seq,
	    const version_manager::ucc_seq_t &u_seq,
	    Environment &_environment);

	void ckpt_freeze_for_upgrade(const char *service,
	    Environment &_environment);

	void ckpt_unfreeze_after_upgrade(const char *service,
	    Environment &_environment);

	void ckpt_set_upgrade_commit_progress(
	    version_manager::commit_progress new_progress,
	    Environment &e);

	void ckpt_unregister_uccs(
	    const version_manager::string_seq_t &ucc_name_seq,
	    Environment &e);

	void	ckpt_service_states_to_reconf(
		    const replica_int::init_service_seq &services,
		    Environment &e);
	// Checkpoint accessor function.
	repl_rm::rm_ckpt_ptr	get_checkpoint_rm_ckpt();

private:

	trans_state_register_service	*
	    get_saved_sec_reg_service_state(Environment &e);
	trans_state_get_control	*
	    get_saved_sec_get_control_state(Environment &e);
	trans_state_add_rma	*
	    get_saved_sec_add_rma_state(Environment &e);
	trans_state_reg_rma_repl_prov	*
	    get_saved_sec_reg_rma_repl_prov_state(Environment &e);
	trans_state_shutdown_service *
	    get_saved_sec_shutdown_service_state(Environment &e);

	// Pointer to the root HA object for this service.
	repl_mgr_impl *rm_repl_obj;

	// True if checkpoints should be treated as dumpstate
	// checkpoints (no transaction state should be created).
	// Significant only on a secondary.
	bool dumpstate;

	// Checkpoint proxy
	repl_rm::rm_ckpt_ptr	_ckpt_proxy;

	// Update running_version and register upgrade_callbacks if necessary
	void register_upgrade_callbacks();

	//
	// Current running version. Change of running_version is protected
	// by state_machine_mutex. Whether to use mutex to read the
	// running_version should be treated case by case.
	//
	version_manager::vp_version_t	running_version;

	char	vp_name[16];
};

class rm_upgrade_callback :
    public McServerof<version_manager::upgrade_callback> {
public:
	rm_upgrade_callback(repl_mgr_prov_impl *rm_prov);
	~rm_upgrade_callback();

	void _unreferenced(unref_t cookie);

	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &e);
private:
	repl_mgr_prov_impl *rm_provp;
};


#include <repl/repl_mgr/repl_mgr_prov_impl_in.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _REPL_MGR_PROV_IMPL_H */
