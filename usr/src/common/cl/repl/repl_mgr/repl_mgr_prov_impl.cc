/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_mgr_prov_impl.cc	1.37	08/05/20 SMI"

//
// repl_mgr_prov_impl.cc implements the rm repl_server interface
// portion of the replica management framework.
//

#include <sys/os.h>
#include <sys/vm_util.h>
#include <repl/repl_mgr/repl_mgr_prov_impl.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <repl/repl_mgr/rm_repl_service_in.h>
#include <repl/repl_mgr/rm_trans_states.h>
#include <repl/repl_mgr/rs_trans_states.h>
#include <repl/repl_mgr/rm_state_mach_in.h>
#include <repl/repl_mgr/rm_version.h>
#include <repl/rmm/rmm.h>
#include <repl/rma/rma.h>
#include <vm/vm_lca_impl.h>
#include <orb/infrastructure/orb_conf.h>

#ifdef RM_DBGBUF
uint32_t rm_dbg_size = 100000;
dbg_print_buf rm_dbg_buf(rm_dbg_size);
#endif

repl_mgr_prov_impl::repl_mgr_prov_impl(const char *id) :
	repl_server<repl_rm::rm_ckpt>("ha repl mgr", id),
	rm_repl_obj(NULL),
	dumpstate(true), _ckpt_proxy(NULL)
{
	os::sprintf(vp_name, "replica_manager");

	running_version.major_num = 0;
	running_version.minor_num = 0;
}

repl_mgr_prov_impl::~repl_mgr_prov_impl()
{
	rm_repl_obj = NULL;

	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = NULL;
}

// --------------------------------------------------------------------

// Idl repl_prov interface methods

void
repl_mgr_prov_impl::become_primary(const replica::repl_name_seq &secondaries,
    Environment &_environment)
{
	RM_DBG(("repl_mgr_prov_impl:  got become_primary() request\n"));

	if (rm_repl_obj == NULL) {
		//
		// New cluster boot, initialize our running version and
		// register upgrade callback if necessary. Secondaries will
		// call register_upgrade_callbacks as part of ckpt_rm_obj()
		// which is sent by primary in either become_primary() or
		// add_secondary().
		//
		// We don't need to do register_upgrade_callbacks on a
		// recovering RM because it should have called
		// register_upgrade_callbacks() during ckpt_rm_obj().
		//
		register_upgrade_callbacks();
	}
	//
	// Initialize the checkpoint proxy to the appropriate version
	// corresponding to the current version of RM.
	//
	replica::checkpoint_var	tmp_ckpt_v = set_checkpoint(
	    repl_rm::rm_ckpt::_get_type_info(RM_CKPT_VERS(running_version)));

	_ckpt_proxy = repl_rm::rm_ckpt::_narrow(tmp_ckpt_v);

	CL_PANIC(!CORBA::is_nil(_ckpt_proxy));

	char my_prov_name[CL_MAX_LEN + 1];
	os::sprintf(my_prov_name, "%d", orb_conf::local_nodeid());
	//
	// If we weren't a secondary, create an implementation object
	// for the rm replica object. This is the case when the
	// cluster is forming for.
	// Then checkpoint its creation.
	//
	if (rm_repl_obj == NULL) {
		// new cluster boot
		rm_repl_obj = new repl_mgr_impl(this);
		ASSERT(rm_repl_obj);

		if (RM_CKPT_VERS(running_version) >= 2) {
			// Add RMA for ourselves.
			rm_repl_obj->add_rma(my_prov_name);
			//
			// Create and store references to reconf
			// objects on secondaries
			//
			uint_t num_sec = secondaries.length();
			for (uint_t i = 0; i < num_sec; i++) {
				rm_repl_obj->add_rma(
				    (const char *)secondaries[i]);
			}
		}
		//
		// The RMM will bind it to the name server after
		// doing a get_root_obj() on this primary.
		//
		replica_int::rm_var rm_ref = rm_repl_obj->get_objref(
		    replica_int::rm::_get_type_info(
		    RM_ADMIN_VERS(running_version)));

		RM_DBG(("repl_mgr_prov_impl:  sending first "
		    "ckpt, ckpt_rm_obj()\n"));

		repl_rm::rm_ckpt_var	ckpt_v = get_checkpoint_rm_ckpt();
		ckpt_v->ckpt_rm_obj(rm_ref, _environment);

		ckpt_v->ckpt_dumpstate_complete(_environment);
		RM_DBG(("repl_mgr_prov_impl:  finished initialization "
		    "in become_primary()\n"));
	} else {
		// We were a secondary, Create threads for each of
		// the services and recover each service here

		// running_version should have been set via ckpt_rm_obj();
		ASSERT(running_version.major_num > 0);

		RM_DBG(("repl_mgr_prov_impl:  recovery \n"));

		rm_repl_obj->set_primary_mode();

		if (RM_CKPT_VERS(running_version) >= 2) {
			//
			// Create and store references to reconf
			// objects on all providers. First add one for
			// ourselves.
			//
			ASSERT(repl_mgr_impl::get_rcf_list().empty());
			rm_repl_obj->add_rma(my_prov_name);

			uint_t num_sec = secondaries.length();
			for (uint_t i = 0; i < num_sec; i++) {
				rm_repl_obj->add_rma(
				    (const char *)secondaries[i]);
			}
		}
		// Recover the cb_coordinator
		rm_repl_obj->get_cb_coordinator()->recover(_environment);

		// For each rm_repl_service object, start up a thread.
		rm_repl_obj->recover();
	}

	RM_DBG(("repl_mgr_prov_impl:  finished handling "
	    "become_primary() request\n"));
}

void
repl_mgr_prov_impl::add_secondary(
    replica::checkpoint_ptr sec_ckpt,
    const char *name,
    Environment &e)
{
	RM_DBG(("repl_mgr_prov_impl:  got add_secondary(%s) request\n", name));

	// checkpoint the RM object
	repl_rm::rm_ckpt_var ckpt = repl_rm::rm_ckpt::_narrow(sec_ckpt);

	ASSERT(!CORBA::is_nil(ckpt));

	//
	// Both ckpt_rm_obj and add_sec_cb_ckpts read the current running
	// version before sending out ckpt messages. We don't need to protect
	// the reading and ckpting here as the running_version is only used to
	// get a massaged reference. (See bug 4788072.) Those references are
	// released at the end of add_secondary. So the typeid based on
	// the running_version doesn't really matter.
	//
	replica_int::rm_var rm_ref = rm_repl_obj->get_objref(
	    replica_int::rm::_get_type_info(RM_ADMIN_VERS(running_version)));

	ckpt->ckpt_rm_obj(rm_ref, e);
	if (e.exception()) {
		RM_DBG(("rm_prov_impl:  ckpt_rm_obj(), "
		    "in add_secondary() got exception\n"));
		e.clear();
	}
	if (RM_CKPT_VERS(running_version) < 2) {
		// checkpoint all the rma reconf objects
		rm_repl_obj->add_sec_reconf_ckpts(ckpt, e);
	}

	// checkpoint all the services
	rm_repl_obj->add_sec_service_ckpts(ckpt, e);

	// checkpoint all the service admin objects
	rm_repl_obj->add_sec_sai_ckpts(ckpt, e);

	// checkpoint all the providers
	rm_repl_obj->add_sec_rma_repl_prov_ckpts(ckpt, e);

	// checkpoint cb_coordinator object and cb_coord_control objects
	rm_repl_obj->add_sec_cb_ckpts(ckpt, e);

	if (RM_CKPT_VERS(running_version) >= 2) {
		//
		// Name of secondary is its node id. We use this to
		// get a reference to the rma_reconf object on the
		// joining node.
		//
		rm_repl_obj->add_rma(name);

		// checkpoint rma reconf service states.
		rm_repl_obj->add_reconf_service_states_ckpts(ckpt, e);
	}
	// checkpoint dumpstate completion
	ckpt->ckpt_dumpstate_complete(e);
	if (e.exception()) {
		RM_DBG(("rm_prov_impl:  ckpt_dumpstate_complete(), "
		    "in add_secondary() got exception\n"));
		e.clear();
	}

	RM_DBG(("repl_mgr_prov_impl:  finished add_secondary() "
	    "request\n"));
}

void
repl_mgr_prov_impl::freeze_primary_prepare(Environment &)
{
}

void
repl_mgr_prov_impl::freeze_primary(Environment &)
{
	RM_DBG(("repl_mgr_prov_impl:  got freeze_primary() request\n"));
	rm_repl_obj->freeze_primary();
}

void
repl_mgr_prov_impl::unfreeze_primary(Environment &)
{
	RM_DBG(("repl_mgr_prov_impl:  got unfreeze_primary() request\n"));
	rm_repl_obj->unfreeze_primary();
}

void
repl_mgr_prov_impl::become_secondary(Environment &)
{
	CL_PANIC(0);
}

void
repl_mgr_prov_impl::remove_secondary(const char *, Environment &)
{
	RM_DBG(("repl_mgr_prov_impl:  got remove_secondary() request\n"));
}

void
repl_mgr_prov_impl::become_spare(Environment &)
{
	RM_DBG(("rm_pi:  got become_spare() request, "
	    "rm_repl_obj `%p'\n", rm_repl_obj));

	// set the dumpstate flag
	dumpstate = true;
	if (rm_repl_obj != NULL) {
		RM_DBG(("rm_pi:  become_spare() calling "
		    "handle_become_spare, "
		    "rm_repl_obj `%p'\n", rm_repl_obj));
		rm_repl_obj->handle_become_spare();
		RM_DBG(("rm_pi:  become_spare() completed "
		    "handle_become_spare\n"));
		delete rm_repl_obj;
		rm_repl_obj = NULL; //lint !e423 lint complains memory leak
	}
	RM_DBG(("rm_pi:  completed become_spare() request, "
	    "rm_repl_obj `%p'\n", rm_repl_obj));
}

void
repl_mgr_prov_impl::shutdown(Environment &)
{
	CL_PANIC(0);
}

CORBA::Object_ptr
repl_mgr_prov_impl::get_root_obj(Environment &)
{
	CL_PANIC(rm_repl_obj != NULL);
	//
	// Need to return a reference corresponding to the current running
	// version.
	//
	return (rm_repl_obj->get_objref(replica_int::rm::_get_type_info(
	    RM_ADMIN_VERS(running_version))));

}

//
// Upgrade commit callback function.
// Caller: rm_callback_task::do_callback()
//
void
repl_mgr_prov_impl::upgrade_callback(const version_manager::vp_version_t &v)
{
	rm_state_machine::state_mutex_lock();

	RM_DBG(("rm_prov_impl: in upgrade_callback.\n"));
	if (running_version.major_num == v.major_num &&
	    running_version.minor_num == v.minor_num) {
		// Version hasn't changed so just return
		rm_state_machine::state_mutex_unlock();
		return;
	}

	RM_DBG(("rm_prov_impl: version has been changed from %d.%d to "
	    "%d.%d. %s\n", running_version.major_num, running_version.minor_num,
	    v.major_num, v.minor_num, vp_name));

	// The version has been changed.
	running_version = v;

	if (rm_repl_obj->is_primary_mode()) {
		// Upgrade our multicheckpoint proxy

		//
		// Keep the reference to the old ckpt_proxy first.
		// Note we don't want to do duplicate here.
		//
		replica::checkpoint_ptr old_ckpt_p = _ckpt_proxy;

		// Get a reference to the upgraded ckpt_proxy.
		replica::checkpoint_var	new_ckpt_v =
		    set_checkpoint(repl_rm::rm_ckpt::_get_type_info(
		    RM_CKPT_VERS(running_version)));

		_ckpt_proxy = repl_rm::rm_ckpt::_narrow(new_ckpt_v);
		ASSERT(!CORBA::is_nil(_ckpt_proxy));

		//
		// Release the reference to the old ckpt_proxy. This
		// should result in the deletion of the old ckpt_proxy.
		//
		CORBA::release(old_ckpt_p);
	} else {
		if (RM_CKPT_VERS(running_version) >= 2) {
			rm_repl_obj->empty_reconf_list();
		}
	}

	rm_state_machine::state_mutex_unlock();

	// Need to update the reference in the local name server.
	(void) rmm::the().register_provider();

	// Upgrade the reference to the cb_coord_control held by the lca.
	vm_lca_impl::the().upgrade_cb_coord_control_ref(v.minor_num);
}

//
// Register upgrade callbacks with the version manager during provider
// initialization time.
//
void
repl_mgr_prov_impl::register_upgrade_callbacks()
{
	RM_DBG(("in register_upgrade_callbacks %s\n", vp_name));
	Environment	e;

	// Get a pointer to the local version manager.
	version_manager::vm_admin_var vmgr_v = vm_util::get_vm(NODEID_UNKNOWN);

	rm_upgrade_callback *cbp = new rm_upgrade_callback(this);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();

	//
	// VP replica_manager is a bootstrap cluster vp, so it uses it's
	// vp name as ucc name.
	//
	version_manager::ucc_seq_t ucc_seq(1, 1);
	ucc_seq[0].ucc_name = os::strdup(vp_name);
	ucc_seq[0].vp_name = os::strdup(vp_name);

	// The highest version we support.
	version_manager::vp_version_t	highest_version;
	highest_version.major_num = 1;
	highest_version.minor_num = 3;

	//
	// If this is executed on a joining node when there is an
	// upgrade_commit in progress on the cluster, the lock of vps
	// on the joining node happens after this step, during
	// vm_lca_impl::register_with_cb_coordinator(). So the following
	// register_upgrade_callbacks should be able to go through even though
	// the vps locks are supposed to be held in cluster.
	//
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v, highest_version,
	    running_version, e);

	ASSERT(!e.exception());

	RM_DBG(("rm_prov_impl: register_upgrade_callbacks, running_verison is "
	    "%d.%d\n", running_version.major_num, running_version.minor_num));
}

// --------------------------------------------------------------------

// Idl checkpoint interface methods

//
// Checkpoint the creation of a new repl_mgr object on the
// primary RM.
void
repl_mgr_prov_impl::ckpt_rm_obj(replica_int::rm_ptr rm_ref, Environment &)
{
	CL_PANIC(rm_repl_obj == NULL && dumpstate);

	rm_repl_obj = new repl_mgr_impl(rm_ref, this); //lint !e423 !e672
	ASSERT(rm_repl_obj);

	// Set running_version and register upgrade callbacks if necessary.
	register_upgrade_callbacks();

	RM_DBG(("rm_prov_impl: rm_obj ckpt completed normally\n"));
}

// can return NULL state
inline trans_state_register_service *
repl_mgr_prov_impl::get_saved_sec_reg_service_state(
    Environment &e)
{
	CL_PANIC(!dumpstate);
	secondary_ctx			*ctxp
	    = secondary_ctx::extract_from(e);
	trans_state_register_service	*st
	    = (trans_state_register_service *) ctxp->get_saved_state();

	return (st);
}

void
repl_mgr_prov_impl::ckpt_register_service(const char *new_service,
    const replica::service_dependencies &this_depends_on,
    replica::service_num_t sid,
    uint32_t desired_numsecondaries,
    bool is_deleted,
    replica::service_num_t new_cur_num,
    replica::service_state sstate,
    Environment &e)
{
	trans_state_register_service	*st = NULL;

	if (!dumpstate) {
		st	= get_saved_sec_reg_service_state(e);
		CL_PANIC(st == NULL);
	}

	ASSERT(rm_repl_obj);

	rm_repl_obj->create_service_on_sec(new_service,
	    this_depends_on, sid, desired_numsecondaries,
	    is_deleted, sstate);

	// set the new cur_num for next service id
	rm_repl_obj->set_service_num_on_sec(new_cur_num);

	if (!dumpstate) {
		st	= new trans_state_register_service();

		st->register_state(e);
		if (e.exception()) {
			RM_DBG(("rm_prov_impl:  client died in "
			    "register_service ckpt\n"));
			e.clear();
		} else {
			RM_DBG(("rm_prov_impl:  "
			    "register_service ckpt completed normally\n"));
		}
	} else {
		RM_DBG(("rm_prov_impl:  "
		    "register_service dumpstate ckpt completed normally\n"));
	}
}

// Function to checkpoint the desired number of secondaries on RM secondaries.
void
repl_mgr_prov_impl::ckpt_change_desired_numsecondaries(
    replica::service_num_t sid, uint32_t new_desired_numsecondaries,
    Environment &)
{
	ASSERT(rm_repl_obj);
	CL_PANIC(!rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service_incl_deleted(sid);

	ASSERT(s != NULL);

	s->set_desired_numsecondaries(new_desired_numsecondaries);
	rm_state_machine::state_mutex_unlock();
}

// can return NULL state
inline trans_state_get_control *
repl_mgr_prov_impl::get_saved_sec_get_control_state(
    Environment &e)
{
	CL_PANIC(!dumpstate);
	secondary_ctx			*ctxp
	    = secondary_ctx::extract_from(e);
	trans_state_get_control	*st
	    = (trans_state_get_control *) ctxp->get_saved_state();

	return (st);
}

void
repl_mgr_prov_impl::ckpt_get_control(replica::service_num_t sid,
    replica_int::rm_service_admin_ptr sai, Environment &e)
{
	trans_state_get_control	*st = NULL;

	if (!dumpstate) {
		st	= get_saved_sec_get_control_state(e);
		CL_PANIC(st == NULL);
	}

	ASSERT(rm_repl_obj);

	service_admin_impl *sec_sai
	    = rm_repl_obj->create_sai_on_sec(sid, sai);

	if (!dumpstate) {
		st	= new trans_state_get_control(sec_sai);

		st->register_state(e);

		if (e.exception()) {
			RM_DBG(("rm_prov_impl:  client died in "
			    "get_control ckpt\n"));
			e.clear();
		}
	} else {
		RM_DBG(("rm_prov_impl:  "
		    "get_control dumpstate ckpt for service `%d' "
		    "completed normally\n", sid));
	}
}

// can return NULL state
inline trans_state_add_rma *
repl_mgr_prov_impl::get_saved_sec_add_rma_state(
    Environment &e)
{
	CL_PANIC(!dumpstate);
	secondary_ctx			*ctxp
	    = secondary_ctx::extract_from(e);
	trans_state_add_rma	*st
	    = (trans_state_add_rma *) ctxp->get_saved_state();

	return (st);
}

void
repl_mgr_prov_impl::ckpt_add_rma(
    replica_int::rma_reconf_ptr client_rma,
    Environment &e)
{
	trans_state_add_rma	*st = NULL;

	ASSERT(rm_repl_obj);

	if (!dumpstate) {
		st	= new trans_state_add_rma(rm_repl_obj, client_rma);

		CL_PANIC(get_saved_sec_add_rma_state(e) == NULL);
		st->register_state(e);
		// Track the RMA that we are adding so that we can use it
		// during recovery if necessary.
		rm_repl_obj->add_rma_started_ckpt(client_rma);
		RM_DBG(("rm_prov_impl:  add_rma ckpt completed\n"));
	} else {
		rm_repl_obj->add_rma_started_ckpt(client_rma);
		rm_repl_obj->add_rma_done_ckpt();
		RM_DBG(("rm_prov_impl:  "
		    "add_rma dumpstate ckpt completed normally\n"));
	}
}

void
repl_mgr_prov_impl::ckpt_service_states_to_reconf(
    const replica_int::init_service_seq &services,
    Environment &e)
{
	RM_DBG(("rm_prov_impl: ckpt_service_states_to_reconf called\n"));
	// This is a local invocation so can reuse environment.
	rma::the().get_reconf()->init_services(services, e);
}


// can return NULL state
inline trans_state_reg_rma_repl_prov *
repl_mgr_prov_impl::get_saved_sec_reg_rma_repl_prov_state(
    Environment &e)
{
	CL_PANIC(!dumpstate);
	secondary_ctx			*ctxp
	    = secondary_ctx::extract_from(e);
	trans_state_reg_rma_repl_prov	*st
	    = (trans_state_reg_rma_repl_prov *) ctxp->get_saved_state();

	return (st);
}

void
repl_mgr_prov_impl::ckpt_reg_rma_repl_prov(replica::service_num_t sid,
    replica_int::rma_repl_prov_ptr prov, const char *prov_desc,
    const replica::prov_dependencies &this_depends_on,
    replica::checkpoint_ptr chkpt,
    uint32_t priority,
    replica::repl_prov_reg_ptr new_reg_obj,
    Environment &e)
{
	trans_state_reg_rma_repl_prov	*st = NULL;

	if (!dumpstate) {
		st	= get_saved_sec_reg_rma_repl_prov_state(e);
		CL_PANIC(st == NULL);
	}

	ASSERT(rm_repl_obj);

	CL_PANIC(!rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service_incl_deleted(sid);
	ASSERT(s);

	rm_repl_service::prov_reg_impl *rp
	    = s->create_rma_repl_prov_on_sec(
	    prov, prov_desc, this_depends_on, chkpt, priority,
	    new_reg_obj);

	rm_state_machine::state_mutex_unlock();

	ASSERT(rp);

	if (!dumpstate) {
		st	= new trans_state_reg_rma_repl_prov(rp);

		st->register_state(e);

		if (e.exception()) {
			RM_DBG(("rm_prov_impl:  client died in "
			    "reg_rma_repl_prov ckpt\n"));
			e.clear();
		} else {
			RM_DBG(("rm_prov_impl:  "
			    "reg_rma_repl_prov ckpt completed normally\n"));
		}
	} else {
		RM_DBG(("rm_prov_impl:  "
		    "reg_rma_repl_prov dumpstate ckpt completed normally\n"));
	}
}

void
repl_mgr_prov_impl::ckpt_change_repl_prov_priority(replica::service_num_t sid,
    const char *prov_desc, uint32_t new_priority, Environment &)
{
	ASSERT(rm_repl_obj);
	CL_PANIC(!rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service_incl_deleted(sid);
	ASSERT(s != NULL);

	rm_repl_service::prov_reg_impl *rp;
	rp = s->get_repl_prov(prov_desc);
	ASSERT(rp != NULL);

	rp->set_priority(new_priority);
	rm_state_machine::state_mutex_unlock();
}

// can return NULL state
inline trans_state_shutdown_service *
repl_mgr_prov_impl::get_saved_sec_shutdown_service_state(
    Environment &e)
{
	CL_PANIC(!dumpstate);
	secondary_ctx			*ctxp = secondary_ctx::extract_from(e);
	trans_state_shutdown_service	*st =
	    (trans_state_shutdown_service *) ctxp->get_saved_state();

	return (st);
}

//
// Checkpoint the tree switchover.
//
// During service_recovery, the RM examines the repl_service to see if there
// was a switchover in progress. If so, then the RM sets the state machine
// state to SM_PRIMARY_TO_MAKE_SECONDARY to preserve the intent to switchover.
//
void
repl_mgr_prov_impl::ckpt_switchover_service(const char *primary_from,
    const char *primary_to, replica::service_num_t sid, Environment &)
{
	repl_mgr_impl	*rmp = repl_mgr_impl::get_rm();
	ASSERT(rmp);

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service(sid);
	ASSERT(s != NULL);

	//
	// If we are starting a switchover, primary_from will
	// have the name of the primary we are switching from.
	// If we are ending a switchover, primary_from will
	// be NULL.
	//
	if (primary_from != NULL) {
		s->trigger_switchover(primary_from, primary_to);
	} else {
		s->switchover_complete();
	}

	rm_state_machine::state_mutex_unlock();
}

// We can receive multiple of these for a single shutdown operation. Each time
// we update the primary that is being used for the shudown.
// During recovery, we check if that primary has successfully completed
// the shutdown.
void
repl_mgr_prov_impl::ckpt_shutdown_service(
    replica::service_num_t sid,
    replica::repl_prov_reg_ptr primary,
    replica::service_admin_ptr sa,
    Environment &e)
{
	trans_state_shutdown_service	*st = NULL;

	CL_PANIC(!dumpstate && !rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service(sid);

	CL_PANIC(!s->is_deleted());

	st = get_saved_sec_shutdown_service_state(e);
	if (st != NULL) {
		CL_PANIC(st->state == trans_state_shutdown_service::STARTED);
	} else {
		// we want to store the service pointer so that commit() can
		// mark the service as deleted.
		st = new trans_state_shutdown_service(s,
		    trans_state_shutdown_service::STARTED,
		    (service_admin_impl *)sa->_handler()->get_cookie(),
		    false);
		st->register_state(e);
	}

	// Update the shutdown intention with the latest primary.
	s->mark_shutdown_intention(
	    (rm_repl_service::prov_reg_impl *)primary->
	    _handler()->get_cookie());

	rm_state_machine::state_mutex_unlock();

	RM_DBG(("rm_prov_impl: shutdown_service ckpt for sid %d\n",
	    sid));
}

//
// We can receive multiple of these for a single shutdown operation. Each time
// we update the primary that is being used for the shudown.
// During recovery, we check if that primary has successfully completed
// the shutdown.
//
void
repl_mgr_prov_impl::ckpt_shutdown_service_v1(
    replica::service_num_t sid,
    replica::repl_prov_reg_ptr primary,
    replica::service_admin_ptr sa,
    bool is_forced_shutdown,
    Environment &e)
{
	trans_state_shutdown_service	*shutdown_service_statep = NULL;

	ASSERT(!dumpstate);
	ASSERT(!rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *repl_servicep = rm_repl_obj->get_service(sid);

	ASSERT(!repl_servicep->is_deleted());

	shutdown_service_statep = get_saved_sec_shutdown_service_state(e);
	if (shutdown_service_statep != NULL) {
		ASSERT(shutdown_service_statep->state ==
		    trans_state_shutdown_service::STARTED);
	} else {
		//
		// we want to store the service pointer so that commit() can
		// mark the service as deleted.
		//
		shutdown_service_statep = new trans_state_shutdown_service(
		    repl_servicep,
		    trans_state_shutdown_service::STARTED,
		    (service_admin_impl *)sa->_handler()->get_cookie(),
		    is_forced_shutdown);
		shutdown_service_statep->register_state(e);
	}
	//
	// Update the shutdown intention with the latest primary.
	//
	repl_servicep->mark_shutdown_intention(
	    (rm_repl_service::prov_reg_impl *)primary->
	    _handler()->get_cookie());

	if (is_forced_shutdown) {
		//
		// At this point, we know for sure that this service
		// will be shutdown. This is because:
		// 1. This is a forced shutdown request which can't
		//    fail.
		// 2. This request will be retried even in the orphaned
		//    case. See trans_state_shutdown_service::orphaned()
		//
		// So we can safely set the desired number of secondaries
		// to 0. This is necessary because we don't want those
		// spares to be converted back to secondaries in case of
		// RM failover, especially after we have called shutdown()
		// on the primary.
		//
		ASSERT(rm_repl_obj);
		CL_PANIC(!rm_repl_obj->is_primary_mode());

		repl_servicep = rm_repl_obj->get_service_incl_deleted(sid);

		ASSERT(repl_servicep != NULL);

		repl_servicep->set_desired_numsecondaries(0);
	}

	RM_DBG(("rm_prov_impl: shutdown_service_v1 ckpt for sid %d\n",
		sid));
	rm_state_machine::state_mutex_unlock();
}

// We can receive one of these for a single shutdown operation. It may
// or may not be preceeded by ckpt_shutdown_service() checkpoints.
// We mark the service as deleted and update the service state
void
repl_mgr_prov_impl::ckpt_shutdown_succeeded(
    replica::service_num_t sid,
    replica::service_admin_ptr sa,
    Environment &e)
{
	trans_state_shutdown_service	*st = NULL;

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service(sid);
	ASSERT(s);

	st = get_saved_sec_shutdown_service_state(e);
	if (st != NULL) {
		CL_PANIC(st->state == trans_state_shutdown_service::STARTED);
		st->state = trans_state_shutdown_service::SUCCEEDED;
	} else {
		// we want to store the service pointer so that commit() can
		// mark the service as deleted.
		st = new trans_state_shutdown_service(s,
		    trans_state_shutdown_service::SUCCEEDED,
		    (service_admin_impl *)sa->_handler()->get_cookie(),
		    false);
		st->register_state(e);
	}
	RM_DBG(("rm_prov_impl: shutdown_succeeeded ckpt for sid %d\n",
	    sid));
	s->mark_shutdown_succeeded();
	rm_state_machine::state_mutex_unlock();
}

void
repl_mgr_prov_impl::ckpt_cleanup_dead_service(replica::service_num_t sid,
    Environment &)
{
	rm_repl_service *s = rm_repl_obj->remove_cleanup_task(sid);
	ASSERT(s != NULL);
	CL_PANIC(s->get_number() == sid && s->can_exit_state_machine());
	RM_DBG(("rm_prov_impl: %s ckpt_cleanup_dead_service\n", s->id()));
	delete s;
}

void
repl_mgr_prov_impl::ckpt_dumpstate_complete(
    Environment &)
{
	ASSERT(rm_repl_obj);
	CL_PANIC(!rm_repl_obj->is_primary_mode() && dumpstate);

	dumpstate = false;

	// Give the secondary RM a chance to do some cleanup. Because we used
	// several checkpoints to dump states, the state of a service is
	// stable during the dumping. Now that the dump state has completed
	// we need to do some further processing based on services' states.
	rm_repl_obj->dumpstate_complete();

	RM_DBG(("rm_prov_impl:  "
	    "dumpstate_complete ckpt completed normally\n"));
}

repl_rm::rm_ckpt_ptr
repl_mgr_prov_impl::get_checkpoint_rm_ckpt()
{
	CL_PANIC(!CORBA::is_nil(_ckpt_proxy));
	return (repl_rm::rm_ckpt::_duplicate(_ckpt_proxy));
}

//
// checkpoint and dumpstate method called in
// repl_mgr_impl::register_lca() or repl_mgr_prov_impl::add_secondary()
//
void
repl_mgr_prov_impl::ckpt_register_lca(
    version_manager::vm_lca_ptr new_lca_p,
    replica_int::cb_coord_control_ptr cbcntl_p,
    Environment &e)
{
	trans_state_register_lca	*st = NULL;

	ASSERT(rm_repl_obj);

	CL_PANIC(!rm_repl_obj->is_primary_mode());

	cb_coord_control_impl *cbccp =
	    rm_repl_obj->register_lca_on_sec(new_lca_p, cbcntl_p);
	CL_PANIC(cbccp);

	if (!dumpstate) {
		//
		// This is a checkpoint method called during mini-transaction,
		// Create a transaction object to associate with it.
		//
		st = new trans_state_register_lca(cbccp);

		st->register_state(e);
		if (e.exception()) {
			RM_DBG(("rm_prov_impl:  client died in "
			    "register_lca ckpt\n"));
			e.clear();
		} else {
			RM_DBG(("rm_prov_impl:  "
			    "register_lca ckpt completed normally\n"));
		}
	} else {
		RM_DBG(("rm_prov_impl:  "
		    "register_lca dumpstate completed normally\n"));
	}
}

// checkpoint method called in cb_coord_control_impl::register_ucc() only
void
repl_mgr_prov_impl::ckpt_register_ucc(const version_manager::ucc &new_ucc,
    Environment &e)
{
	// register ucc on RM secondaries, no need to do check.
	rm_repl_obj->get_cb_coordinator()->register_ucc(new_ucc, e);

	CL_PANIC(!e.exception() && !dumpstate);
}

//
// Dumpstate method for dumping the content of the cb_coordinator object to
// the joining secondary.
//
void
repl_mgr_prov_impl::ckpt_dump_cb_coordinator(
    const version_manager::lca_seq_t &lca_seq,
    const replica_int::cbcntl_seq_t &cbcntl_seq,
    const version_manager::ucc_seq_t &u_seq,
    Environment &e)
{
	cb_coord_control_impl	*cbccp = NULL;
	uint_t			len = 0;

	CL_PANIC(dumpstate);

	// dump lca list
	len = lca_seq.length();
	CL_PANIC(len == cbcntl_seq.length());
	for (uint_t i = 0; i < len; i++) {
		cbccp = rm_repl_obj->register_lca_on_sec(lca_seq[i],
		    cbcntl_seq[i]);
		CL_PANIC(cbccp);
	}

	// dump ucc list
	len = u_seq.length();
	for (uint_t i = 0; i < len; i++) {
		rm_repl_obj->get_cb_coordinator()->register_ucc(u_seq[i], e);
		CL_PANIC(!e.exception());
	}

	//
	// If there is an upgrade commit in progress, the cb_coordinator
	// will need to lock vps on the joining new RM secondary node.
	//
	rm_repl_obj->get_cb_coordinator()->lock_vps_on_new_sec();
}

//
// Checkpoint method to mark service frozen for upgrade.
//
void
repl_mgr_prov_impl::ckpt_freeze_for_upgrade(const char *svc_name,
    Environment &)
{
	ASSERT(rm_repl_obj);
	CL_PANIC(!rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service(svc_name);
	ASSERT(s != NULL);
	s->freeze_for_upgrade();

	rm_state_machine::state_mutex_unlock();
}

//
// Checkpoint method to mark service unfrozen after upgrade.
//
void
repl_mgr_prov_impl::ckpt_unfreeze_after_upgrade(const char *svc_name,
    Environment &env)
{
	ASSERT(rm_repl_obj);
	CL_PANIC(!rm_repl_obj->is_primary_mode());

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = rm_repl_obj->get_service(svc_name);
	ASSERT(s != NULL);
	s->unfreeze_after_upgrade(env);
	// exception can only be returned on RM primary
	CL_PANIC(!env.exception());

	rm_state_machine::state_mutex_unlock();
}

// Checkpoint method to set the progress of an upgrade commit on RM secondaries.
void
repl_mgr_prov_impl::ckpt_set_upgrade_commit_progress(
    version_manager::commit_progress new_progress,
    Environment &)
{
	ASSERT(rm_repl_obj);
	ASSERT(!rm_repl_obj->is_primary_mode());

	cb_coordinator	*cbcp = rm_repl_obj->get_cb_coordinator();
	cbcp->wrlock_rwlock();
	cbcp->set_local_upgrade_commit_progress(new_progress);
	cbcp->unlock_rwlock();
}

// Checkpoint method to cleanup UCCS on RM secondaries
void
repl_mgr_prov_impl::ckpt_unregister_uccs(
    const version_manager::string_seq_t &ucc_name_seq,
    Environment &)
{
	ASSERT(rm_repl_obj);
	CL_PANIC(!rm_repl_obj->is_primary_mode());

	cb_coordinator	*cbcp = rm_repl_obj->get_cb_coordinator();
	cbcp->wrlock_rwlock();
	cbcp->remove_uccs(ucc_name_seq);
	cbcp->unlock_rwlock();
}

// Upgrade callback object for replica manager.

// Constructor
rm_upgrade_callback::rm_upgrade_callback(repl_mgr_prov_impl *rm_prov):
    rm_provp(rm_prov)
{
}

// Destructor
rm_upgrade_callback::~rm_upgrade_callback()
{
	rm_provp = NULL;
}

// Unreference method
void
#ifdef DEBUG
rm_upgrade_callback::_unreferenced(unref_t cookie)
#else
rm_upgrade_callback::_unreferenced(unref_t)
#endif
{
	RM_DBG(("rm_upgrade_callback:  in _unreferenced \n"));
	// This object does not support multiple 0->1 transitions.
	ASSERT(_last_unref(cookie));

	delete this;
}

//
// Upgrade callback method.
// Caller: vm_lca_impl::callback_task
//
void
rm_upgrade_callback::do_callback(const char *ucc_name,
    const version_manager::vp_version_t &new_version,
    Environment &)
{
	RM_DBG(("rm_upgrade_callback: in do_callback\n"));
	ASSERT(os::strcmp(ucc_name, "replica_manager") == 0);
	rm_provp->upgrade_callback(new_version);
}
