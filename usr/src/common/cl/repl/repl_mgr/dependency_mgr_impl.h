/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DEPENDENCY_MGR_IMPL_H
#define	_DEPENDENCY_MGR_IMPL_H

#pragma ident	"@(#)dependency_mgr_impl.h	1.14	08/05/20 SMI"

#include <sys/hashtab_def.h>
#include <h/replica_int.h>
#include <repl/repl_mgr/rm_repl_service.h>

//
// dependency_mgr_impl.h contains class definitions for the dependency
// manager.
//

// Hash table keys are in uint32_t units, for some reason
#define	KEY_SIZE (sizeof (rm_repl_service *)/sizeof (uint32_t))

/*CSTYLED*/
class dependency_mgr_impl {
public:
	// Lookup modes
	enum dependency_mode {this_depends_on, this_directly_depends_on,
		depend_on_this, directly_depend_on_this, component};

	void add_dependency(rm_repl_service *item,
	    rm_repl_service *depends_on_item, Environment &_environment);
	void remove_dependency(rm_repl_service *item,
	    rm_repl_service *depends_on_item, Environment &_environment);
	void remove_dependencies(rm_repl_service *item);
	repl_service_list *lookup(rm_repl_service *item, dependency_mode mode);

	dependency_mgr_impl();
	~dependency_mgr_impl();
	void dump();
	void dump(repl_service_list *il);
private:
	// Hash table referenced by names, holding list of depends_on and
	// depended_on

	struct dependency_info {
		rm_repl_service *key;

		// list of things we depend on.
		DList<dependency_info> this_depends_on;

		// list of things that depend on us
		DList<dependency_info> depend_on_this;
#ifdef DEBUG
		// Used for sanity checking
		int depth;
#endif
	};

	// Mutex to protect the dependency manager
	// XXX Can probably go away.
	os::mutex_t	mutex;

	typedef hash_table_t<dependency_info, KEY_SIZE>
		dependency_hash_table_t;

	dependency_hash_table_t	dependency_table;

	// Lookup an entry in the hash table and optionally create it
	dependency_info *lookup_create(rm_repl_service *key, bool create);

	// Walk the graph
	void get_more_relatives(dependency_info *item,
		repl_service_list *ilp, bool up,
		bool down, bool recurse);
	// Generate a topologically sorted list
	void topological(dependency_info *item, repl_service_list *ilp,
	    bool up);

	// Sanity check the graph and helper depth first search
#ifdef DEBUG
	int depth;
	void sanity();
	void dfs(dependency_info *info);
#else
	void sanity() {}
#endif

};

#endif	/* _DEPENDENCY_MGR_IMPL_H */
