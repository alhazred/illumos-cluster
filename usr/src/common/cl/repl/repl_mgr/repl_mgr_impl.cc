/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repl_mgr_impl.cc	1.131	08/05/20 SMI"

//
// repl_mgr_impl.cc implements the replicated rm interface
// portion of the replica management framework.
//

#include <orb/fault/fault_injection.h>

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/clusterproc.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <repl/repl_mgr/repl_mgr_prov_impl.h>
#include <repl/repl_mgr/rm_repl_service.h>
#include <repl/repl_mgr/rm_trans_states.h>
#include <repl/repl_mgr/rm_repl_service_in.h>
#include <repl/repl_mgr/rm_state_mach_in.h>
#include <repl/repl_mgr/service_admin_impl_in.h>
#include <repl/repl_mgr/cb_coord_control_impl.h>
#include <repl/rma/rma.h>
#include <repl/rmm/rmm.h>
#include <repl/repl_mgr/rm_version.h>

#ifdef _KERNEL
// For the bufcall stuff.
#include <sys/ddi.h>
extern proc_t *proc_cluster;
#endif

//
// static rm pointer is used by rm_repl_service class to
// access rma_reconf_list and its lock, and service_admin obj list,
// via static methods get_rcf_list(), and get_sai_list() respectively.
//
static repl_mgr_impl		*rmp = NULL;
//
// Rows are indexed by major_num of current running version.
// Columns are indexed by the minor_num of current running version.
//
rm_vp_map rm_ver_map[2][4] = {
	{{0, 0}, {0, 0}, {0, 0}, {0, 0}}, // row 0
	// row 1:
	// col0: version 1.0: rm_admin=0, ckpt=0
	// col1: version 1.1: rm_admin=1, rm_ckpt=1
	// col2: version 1.2: rm_admin=1, rm_ckpt=2
	// col2: version 1.3: rm_admin=1, rm_ckpt=3
	{{0, 0}, {1, 1}, {1, 2}, {1, 3}},
    };

// stack size tunable
uint_t rm_thread_stacksize = 0;

//
// constructor for primary RM replica object.
//
repl_mgr_impl::repl_mgr_impl(repl_mgr_prov_impl *rm_prov_impl) :
	mc_replica_of<replica_int::rm>(rm_prov_impl),
	adding_rma(nil),
	cur_num(STARTING_SERVICE_ID),
	primary_mode(true),
	prov_impl(rm_prov_impl),
	primary_frozen(true)
{
	rmp = this;
#ifdef _KERNEL
	bufcallid = 0;
	timeoutid = 0;
#endif
	RM_DBG(("repl_mgr_impl:  primary constructor\n"));
}

//
// constructor for secondary RM replica object.
//
repl_mgr_impl::repl_mgr_impl(replica_int::rm_ptr rm_prov,
    repl_mgr_prov_impl *rm_prov_impl) :
	mc_replica_of<replica_int::rm>(rm_prov),
	adding_rma(nil),
	cur_num(STARTING_SERVICE_ID),
	primary_mode(false),
	prov_impl(rm_prov_impl),
	primary_frozen(true)
{
	RM_DBG(("repl_mgr_impl:  secondary constructor\n"));
	rmp = this;
#ifdef _KERNEL
	bufcallid = 0;
	timeoutid = 0;
#endif
}

repl_mgr_impl::~repl_mgr_impl()
{
	prov_impl = NULL;
	CL_PANIC(CORBA::is_nil(adding_rma));
	adding_rma = nil;

#ifdef _KERNEL
	rm_state_machine::state_mutex_lock();
	// Unschedule what's scheduled.
	CL_PANIC(bufcallid == 0 || timeoutid == 0);
	if (bufcallid != 0) {
		unbufcall(bufcallid);
		bufcallid = 0;
	}
	if (timeoutid != 0) {
		(void) untimeout(timeoutid);
		timeoutid = 0;
	}

	rm_state_machine::state_mutex_unlock();
#endif
}

// Called during state transition for the RM service.
void
repl_mgr_impl::freeze_primary()
{
	rm_state_machine::state_mutex_lock();
	wait_for_all_stable();

	//
	// The framework does not disable unreferenced on objects in this
	// service until after freeze_primary returns. This is fine if the
	// object that is unreferenced is deleted immediately in the context of
	// the _unreferenced() call (we do this for service_admin_impl
	// objects), because the framework waits for these unreferenced calls
	// to drain out before proceeding. However, when we receive
	// unreferenced for a prov_reg_impl object we defer deleting the
	// object until the per-service thread has performed any recovery
	// triggered by that _unreferenced.
	// Because the per-service threads are not managed by the framework
	// (they are private to the RM primary), this deletion can end up
	// happening any time after the freeze_primary() call returns.  This
	// can lead to a situation where an object is deleted in the middle of
	// add_secondary(), when we are supposed to be checkpointing all
	// objects in the service (see bugid 4318082). It is illegal to delete
	// objects in add_secondary() since the deletion triggers a multicast
	// checkpoint to all secondaries and the list of current secondaries is
	// undefined at that time.
	// We deal with this by not telling the per-service threads about
	// unreferenced notifications that come in after the call to
	// freeze_primary(). Instead we create new references to these objects
	// and store them in the deferred_ref_list. We release these references
	// in unfreeze_primary() and that generates new unreferenced
	// notifications for the objects.
	//
	// Note that it is possible that some invocations will come in after
	// this call returns. These are guaranteed to drain out by the time
	// an add_secondary is called, leaving the services in a stable
	// state.
	//

	primary_frozen = true;
	CL_PANIC(deferred_ref_list.empty());

	rm_state_machine::state_mutex_unlock();

	// Remove the cleanup task.
	periodic_thread::the().remove_job(&cleanup_task);
}

//
// unfreeze_primary - the primary is no longer frozen.
// Re-enable processing of deferred unreferences.
//
void
repl_mgr_impl::unfreeze_primary()
{
	replica::repl_prov_reg_ptr	repl_prov_reg_p;

	rm_state_machine::state_mutex_lock();
	//
	// It is now safe to process unreferenced on replica::repl_prov_reg
	// objects. So don't put references on the deferred_ref_list any more.
	// Also release any references already on the list.
	//
	primary_frozen = false;
	while ((repl_prov_reg_p = deferred_ref_list.reapfirst()) != nil) {
		CORBA::release(repl_prov_reg_p);
	}
	rm_state_machine::state_mutex_unlock();

	// Add the cleanup task.
	periodic_thread::the().add_job(&cleanup_task);
}

//
// Called only in become_primary().  No locks required.
// Note that deleted services are skipped, since shutdown
// has completed on them.
//
void
repl_mgr_impl::fill_sid_seq(
    replica_int::service_num_seq &num_seq)
{
	uint_t 			j = 0;
	rm_repl_service		*s;

	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (!s->is_deleted()) {
			num_seq[j++] = s->get_number();
		}
	} // for
	num_seq.length(j);
}

void
repl_mgr_impl::copy_service_data(
	replica_int::service_num_seq &sid_seq,
	replica_int::rma_service_state_seq &state_seq,
	replica_int::rma_reconf_ptr rcf_p)
{
	ASSERT(rm_state_machine::state_lock_held());

	const uint_t	seq_len = sid_seq.length();
	RM_DBG(("repl_mgr_impl:  recovery method copy_service_data\n"));

	CL_PANIC(seq_len && seq_len == state_seq.length());

	for (uint_t i = 0; i < seq_len; i++) {
		// If service is deleted, it doesn't participate in recovery.
		rm_repl_service *s = get_service(sid_seq[i]);
		ASSERT(s);
		replica_int::rma_service_state state = state_seq[i];
		rm_repl_service::service_recovery_data *data_p
		    = new rm_repl_service::service_recovery_data(rcf_p, state);
		RM_DBG(("repl_mgr_impl:  recovery "
		    "copying data to service `%s' state "
		    "machine rcf = %p, state = %p\n", s->get_name(),
		    rcf_p, state));
		// append the state machine's get_states_list
		s->get_state_machine()->store_recovery_data(data_p);
	}
}

//
// Called from recovery code and bufcallback.  We create one thread for
// each service that doesn't have one, even if it is deleted
// (thread will destroy itself if ref count has gone to 0).
// Each thread we create here will try to grab the state_lock,
// but no one will succeed, and enter its state machine,
// until the thread running become_primary() goes into its
// wait for all stable.
//
void
repl_mgr_impl::start_service_threads()
{
	CL_PANIC(rm_state_machine::state_lock_held());
	CL_PANIC(primary_mode);
	RM_DBG(("repl_mgr_impl: start_service_threads\n"));

	rm_repl_service *s;
	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (s->need_thread()) {
			rm_service_thread_create(s);
		}
	}
}

//
// called by repl_mgr_prov_impl in become_primary() on this RM.
// First, gather information from RMAs on service state and
// pass the information to each service's state machine.
// Then start a thread for each service, and wait for all
// services to reconfigure (get to a stable state, with the
// service up if possible).  Finally, clean up/deallocate temporary
// recovery data, and make sure all reconf objects know about every
// service.
//
void
repl_mgr_impl::recover()
{
	RM_DBG(("repl_mgr_impl:  starting recovery\n"));

	const uint_t number_of_services = service_list.count();
	if (number_of_services > 0) {
		RM_DBG(("repl_mgr_impl:  recovery service "
		    "list has `%d' services\n", number_of_services));
		replica_int::service_num_seq sid_seq(number_of_services);

		// iterate through the service list and obtain
		// sids for all services that aren't deleted.
		fill_sid_seq(sid_seq);
		replica_int::rma_reconf_ptr rcf_p;

		// out param
		replica_int::rma_service_state_seq *state_seq;

		// list routines expect list to be locked
		rma_reconf_list.lock();
		// get_service(), etc. expect state mutex to be held
		rm_state_machine::state_mutex_lock();
		rma_reconf_list.atfirst();
		while ((rcf_p = rma_reconf_list.current()) != NULL) {
			Environment e;
			//
			// We use the local out variable to keep lint happy.
			// There is no extra overhead, since the compiler would
			// create one anyway to cast the out parameter.
			//
			replica_int::rma_service_state_seq_out
			    state_o(state_seq);
			RM_DBG(("rm_recovery:Querying rma_reconf %p.\n",
			    rcf_p));

			rcf_p->get_service_states(sid_seq, state_o, e);
			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					rma_reconf_list.erase(rcf_p);
					CORBA::release(rcf_p);

					RM_DBG(("rm_recovery: rma_reconf_"
					    "exception, released ref `%p'\n",
					    rcf_p));
				} else {
					CL_PANIC(0);
				}
			} else {
				copy_service_data(sid_seq, *state_seq, rcf_p);
				//
				// state_seq is allocated by unmarshal.
				// we don't need it anymore.
				//
				delete state_seq;
				rma_reconf_list.advance();
			}
		}

		rma_reconf_list.unlock();

		// If we are adding an rma, see if it has info on the services.
		// If so, then add it to the list we use for recovery.
		if (!CORBA::is_nil(adding_rma)) {
			Environment e;
			// We use the local out variable to keep lint happy.
			// There is no extra overhead, since the compiler would
			// create one anyway to cast the out parameter.
			replica_int::rma_service_state_seq_out
			    state_o(state_seq);

			adding_rma->get_service_states(sid_seq, state_o, e);
			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					// This will get cleaned up later.

					RM_DBG(("rm_recovery: invocation failed"
					    "on adding_rma`%p'\n", adding_rma));
				} else {
					CL_PANIC(0);
				}
			} else {
				// If any services were heard of, then use
				// this rma.
				uint_t seq_len = sid_seq.length();
				uint_t i;
				for (i = 0; i < seq_len; i++) {
					if ((*state_seq)[i] !=
					    replica_int::RMA_SERVICE_NONE) {
						break;
					}
				}
				if (i != seq_len) {
					// We will use the data from this RMA
					// in recovery.
					copy_service_data(sid_seq, *state_seq,
					    adding_rma);
					//
					// state_seq is allocated by unmarshal.
					// we don't need it anymore.
					//
					delete state_seq;
					// Commit the addition of this rma.
					add_rma_done_ckpt();
				}
			}
		}

		// start threads.
		start_service_threads();

		rm_state_machine::state_mutex_unlock();
	}
	RM_DBG(("repl_mgr_impl:  exiting recovery\n"));
}

//
// The following methods are called from add_secondary()
//
// During these we invoke checkpoints to the new secondary. We assert that
// the only exception that we can get from these checkpoints is COMM_FAILURE,
// which can happen if the secondary dies. In this case, we simply clear these
// exceptions and proceed rather than bailing out early.
//

//
// checkpoint all the services
//
void
repl_mgr_impl::add_sec_service_ckpts(
    repl_rm::rm_ckpt_ptr ckpt_p, Environment &e)
{
	RM_DBG(("rm_ri:  starting add_sec_service_ckpts()\n"));

	rm_repl_service	*s;

	// We need to protect both list at the same time.
	rm_state_machine::state_mutex_lock();
	cleanup_task.lock();

	// Dump every thing on the service_list.
	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		replica::service_dependencies 	this_depends_on;
		s->get_deps_down(&this_depends_on);

		RM_DBG(("rm_ri:  add_sec_service_ckpts() %s state %d\n",
		    s->id(), s->map_service_state()));

		ckpt_p->ckpt_register_service(s->get_name(), this_depends_on,
		    s->get_number(), s->get_desired_numsecondaries(),
		    s->is_deleted(), cur_num, s->map_service_state(), e);

		if (e.exception()) {
			RM_DBG(("rm_ri:  add_sec_service_ckpts(), "
			    "got exception checkpointing service %s\n",
			    s->id()));
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				e.clear();
			} else {
				CL_PANIC(0);
			}
		}
	}

	// Dump every thing on the cleanup_task.
	iter.reinit(cleanup_task.services);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		CL_PANIC(s->is_deleted());

		replica::service_dependencies 	this_depends_on;
		s->get_deps_down(&this_depends_on);

		RM_DBG(("rm_ri:  add_sec_service_ckpts() %s\n", s->id()));

		ckpt_p->ckpt_register_service(s->get_name(), this_depends_on,
		    s->get_number(), s->get_desired_numsecondaries(),
		    s->is_deleted(), cur_num, s->map_service_state(), e);

		if (e.exception()) {
			RM_DBG(("rm_ri:  add_sec_service_ckpts(), "
			    "got exception checkpointing service %s\n",
			    s->id()));
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				e.clear();
			} else {
				CL_PANIC(0);
			}
		}

	}
	cleanup_task.unlock();
	rm_state_machine::state_mutex_unlock();

	RM_DBG(("rm_ri:  exiting add_sec_service_ckpts()\n"));
}

// checkpoint all the rma reconf objects
void
repl_mgr_impl::add_sec_reconf_ckpts(
    repl_rm::rm_ckpt_ptr ckpt, Environment &e)
{
	replica_int::rma_reconf_ptr	reconf_obj;

	RM_DBG(("rm_ri:  starting add_sec_reconf_ckpts()\n"));

	// lock only because rma_list methods assert we're locked
	rma_reconf_list.lock();
	for (rma_reconf_list.atfirst();
	    (reconf_obj = rma_reconf_list.current()) != NULL;
	    rma_reconf_list.advance()) {
		RM_DBG(("rm_ri:  add_sec_reconf_ckpts(), "
		    "checkpointing reconf_obj %p\n",
		    reconf_obj));
		ckpt->ckpt_add_rma(reconf_obj, e);
		if (e.exception()) {
			RM_DBG(("rm_ri:  add_sec_reconf_ckpts(), "
			    "got exception checkpointing reconf_obj %p\n",
			    reconf_obj));
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				e.clear();
			} else {
				CL_PANIC(0);
			}
		}
	}
	rma_reconf_list.unlock();
	RM_DBG(("rm_ri:  exiting add_sec_reconf_ckpts()\n"));
}

// checkpoint all the service admin objects
void
repl_mgr_impl::add_sec_sai_ckpts(repl_rm::rm_ckpt_ptr ckpt, Environment &e)
{
	service_admin_impl	*sai;

	RM_DBG(("rm_ri:  starting add_sec_sai_ckpts()\n"));

	SList<service_admin_impl>::ListIterator iter(sai_list);
	for (; (sai = iter.get_current()) != NULL; iter.advance()) {
		replica_int::rm_service_admin_var sai_ref = sai->get_objref();
		RM_DBG(("rm_ri: add_sec_sai_ckpts(), "
		    "checkpointing sai %p\n", sai));
		ckpt->ckpt_get_control(sai->get_service().get_number(),
		    sai_ref, e);
		if (e.exception()) {
			RM_DBG(("rm_ri:  add_sec_sai_ckpts(), "
			    "got exception checkpointing sai %p\n", sai));
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				e.clear();
			} else {
				CL_PANIC(0);
			}
		}
	}

	RM_DBG(("rm_ri:  exiting add_sec_sai_ckpts()\n"));
}

// checkpoint all the providers
void
repl_mgr_impl::add_sec_rma_repl_prov_ckpts(
    repl_rm::rm_ckpt_ptr ckpt, Environment &e)
{
	rm_repl_service	*s;

	RM_DBG(("rm_ri:  starting add_sec_repl_prov_ckpts()\n"));

	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		s->add_sec_rma_repl_prov_ckpts(ckpt, e);
	}

	RM_DBG(("rm_ri:  exiting add_sec_repl_prov_ckpts()\n"));
}

// checkpoint cb_coordinator object and all cb_coord_control objects
void
repl_mgr_impl::add_sec_cb_ckpts(repl_rm::rm_ckpt_ptr ckpt_p, Environment &e)
{
	RM_DBG(("rm_ri:  starting add_sec_cb_ckpts()\n"));

	cb_coord.add_sec_cb_ckpts(ckpt_p, e);

	RM_DBG(("rm_ri:  exiting add_sec_cb_ckpts()\n"));
}

// checkpoint service states maintained by RMA reconf
void
repl_mgr_impl::add_reconf_service_states_ckpts(repl_rm::rm_ckpt_ptr ckpt_p,
    Environment &e)
{
	RM_DBG(("rm_ri:  starting add_reconf_service_state_ckpts()\n"));

	rma_reconf_list.lock();
	rm_state_machine::state_mutex_lock();
	cleanup_task.lock();

	ASSERT(check_all_stable());

	// Take a snapshot of the services' state.
	replica_int::init_service_seq service_seq(service_list.count() +
	    cleanup_task.services.count());

	read_services(service_seq);
	cleanup_task.unlock();

	rm_state_machine::state_mutex_unlock();

	ckpt_p->ckpt_service_states_to_reconf(service_seq, e);

	rma_reconf_list.unlock();

	RM_DBG(("rm_ri:  exiting add_reconf_service_state_ckpts()\n"));
}

// End of add_secondary() calls.
// ------------------------------------------------------------

// Probably not necessary to lock anything in this method.
void
repl_mgr_impl::handle_become_spare()
{
	CL_PANIC(!is_primary_mode());

	RM_DBG(("rm_ri:  starting handle_become_spare() "
	    "rm_repl_obj `%p'\n", this));

	rma_reconf_list.lock();
	rm_state_machine::state_mutex_lock();
	//
	// delete rma_reconf objects. Special handling is not
	// necessary for version 1.2 - the list will be empty.
	//
	replica_int::rma_reconf_ptr rcf_p = NULL;
	while ((rcf_p = rma_reconf_list.reapfirst()) != NULL) {
		CORBA::release(rcf_p);
		RM_DBG(("rm_ri: "
		    "become_spare(), released rma_reconf ref `%p'\n",
		    rcf_p));
	}
	CL_PANIC(rma_reconf_list.empty());

	// delete service_admin objects
	service_admin_impl *saip = NULL;
	while ((saip = sai_list.reapfirst()) != NULL) {
		RM_DBG(("rm_ri: "
		    "become_spare(), deleting sai pointer `%p'\n",
		    saip));
		delete saip;
	}
	CL_PANIC(sai_list.empty());

	// delete services and all their contents
	rm_repl_service	*s = NULL;
	while ((s = service_list.reapfirst()) != NULL) {
		s->become_spare_delete_state();
		RM_DBG(("rm_ri: "
		    "become_spare(), deleting service pointer `%p'\n",
		    s));
		// Destructor will remove deps_down linkage.
	}
	rma_reconf_list.unlock();

	rm_state_machine::state_mutex_unlock();
}

// End of calls made by repl_server methods
// --------------------------------------------------------------

// Return a pointer to the RM.
repl_mgr_impl *
repl_mgr_impl::get_rm()
{
	CL_PANIC(rmp);
	return (rmp);
}

//
// get_rcf_list() -- static method that returns a (non-Corba)
// reference to the RM's reconf list.  Used by rm_repl_service class
// methods that invoke rma_reconf interface routines:  a list of
// rma_reconf objects is stored in the RM.
//
repl_mgr_impl::rcf_list &
repl_mgr_impl::get_rcf_list()
{
	ASSERT(rmp);
	return (rmp->rma_reconf_list);
}

// get_sai_list() -- static method that returns a (non-Corba)
// reference to the RM's service_admin list.  Used by service_admin class
// _unreferenced() method.
//
SList<service_admin_impl> &
repl_mgr_impl::get_sai_list()
{
	ASSERT(rmp);
	return (rmp->sai_list);
}

//
// _unreferenced - ignore unreferenced for now, since it is OK to receive them.
// We create and release a reference in become_primary() and then in
// get_root_obj(), so that leaves a window during boot where we have no
// references.
// If we ever implement a completely clean shutdown, then we may
// need to keep track of whether we are unreferenced.
//
void
repl_mgr_impl::_unreferenced(unref_t cookie)
{
	// Show that unreferenced has been processed
	(void) _last_unref(cookie);
}


//
// get_service() -- search list of rm_repl_service objects by string
// name and return a pointer to service object, or null if service
// isn't in the list. It returns NULL if the service is deleted.
//
rm_repl_service *
repl_mgr_impl::get_service(const char *nm)
{
	CL_PANIC(rm_state_machine::state_lock_held());
	rm_repl_service		*s;

	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (!(os::strcmp(nm, s->get_name()))) {
			if (!s->is_deleted()) {
				break;
			}
		}
	} // for
	return (s);
}


//
// get_service_incl_deleted() -- search list of rm_repl_service
// objects by service
// number and return a pointer to service object, or null if service
// isn't in the list. It returns pointer, even if the service
// is deleted.
// Note: Check if this can be called from the primary ??
// XXX should probably be combined with method below
//
rm_repl_service *
repl_mgr_impl::get_service_incl_deleted(const replica::service_num_t sid)
{
	CL_PANIC(rm_state_machine::state_lock_held());
	rm_repl_service	*s;
	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (sid == s->get_number()) {
			break;
		}
	} // for

	// get_service_incl_deleted is only called when someone still has a
	// reference to the rm_repl_service, in which case it won't be on
	// the cleanup list. Note in the case where we don't find the service,
	// the cleanup_task.remove doesn't change anything and is just like a
	// find.
	CL_PANIC(cleanup_task.remove(sid) == NULL);

	return (s);
} // get_service_incl_deleted

//
// get_service() -- search list of rm_repl_service objects by service
// number and return a pointer to service object, or null if service
// isn't in the list. It returns NULL if the service is deleted.
//
rm_repl_service *
repl_mgr_impl::get_service(const replica::service_num_t sid)
{
	CL_PANIC(rm_state_machine::state_lock_held());
	rm_repl_service	*s;
	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (sid == s->get_number()) {
			if (!s->is_deleted()) {
				break;
			}
		}
	} // for
	return (s);
} // get_service

//
// hold_tmp_reference - Used when an unreferenced is received while
// the primary is frozen.  Holds a temporary reference to an object
// until the primary is unfrozen.
//
void
repl_mgr_impl::hold_tmp_reference(replica::repl_prov_reg_ptr repl_prov_reg_p)
{
	deferred_ref_list.append(repl_prov_reg_p);
}

//
// start_routine() -- entry point for newly-created per-service
// thread.  Calls service main loop, which returns only when all
// of the following are true:
//	(1) service has been deleted (currently, only by call to
//	    unregister_service())
//	(2) all references to the service object have been released.
// Return from main loop destroys the service object and the
// service thread.
//
// XXX should probably return void
//
static void *
start_routine(void *arg)
{
	rm_repl_service *const		s = (rm_repl_service *) arg;

	ASSERT(s);

	RM_DBG((
	    "rm_ri:  start_routine,  new service `%s', "
	    "service ptr `%p'\n",
	    s->id(), s));

#if defined(_FAULT_INJECTION)
	// Set the sid in TSD so that Fault Injection cann tell what sid
	// we're a service-tread for
	FaultFunctions::tsd_svc_id(s->get_number());
#endif // _FAULT_INJECTION

	rm_state_machine::state_mutex_lock();

	s->main_loop();

	// Remove it from the list. Delete it if it's cleanly shutdown,
	// otherwise post a job to the cleanup queue.
	rmp->erase_service(s);

	rm_state_machine::signal_external_state_change();
	rm_state_machine::state_mutex_unlock();

	return (NULL);
}

//
// See if the proposed dependencies are satisfied. Return value:
// true + exception when dependency isn't satisified.
// true + no exception when dependency is satisified and all services are
// stable.
// false when one service it depends on isn't stable.
//
bool
repl_mgr_impl::check_service_dependencies(
	const replica::service_dependencies &dep, Environment &e)
{
	ASSERT(rm_state_machine::state_lock_held());

	for (uint_t i = 0; i < dep.length(); i++) {
		rm_repl_service *s;

		if ((s = get_service(dep[i])) == NULL) {
			e.exception(new replica::invalid_dependency());
			return (true);
		} else {
			if (!s->is_stable()) {
				return (false);
			}
			if (s->is_down()) {
				e.exception(new replica::dependency_failed());
				return (true);
			}
		}
	}
	return (true);
}

//
// add_service() -- allocate an rm_repl_service object, with the specificed
// service number; start its service thread if we are primary; and add it
// to the RM's list of service objects.
//
rm_repl_service *
repl_mgr_impl::add_service(const char *nm,
	const replica::service_dependencies &dep,
	replica::service_num_t sid,
	uint32_t desired_numsecondaries,
	const replica::service_state sstate)
{
	ASSERT(rm_state_machine::state_lock_held());

	rm_repl_service *new_s = new rm_repl_service(nm,
	    sid, desired_numsecondaries,
	    primary_mode ? rm_state_machine::SERVICE_REGISTERED :
	    rm_state_machine::SERVICE_RECOVERING, sstate);

	RM_DBG(("rm_ri: allocated new service %s, service ptr %p, %d\n",
	    new_s->id(), new_s, sstate));

	for (uint_t i = 0; i < dep.length(); i++) {
		rm_repl_service *serv = get_service(dep[i]);
		ASSERT(serv != NULL);
		new_s->add_dep_down(serv);
	}

	// start the per-service thread only if we're the primary
	if (primary_mode) {
		rm_service_thread_create(new_s);
	}

	service_list.append(new_s);

	return (new_s);
}

//
// rm_service_thread_create() -- wrapper to create thread of the
// per-service thread.
//
// If we fail to create the thread, we use bufcall (or timeout when bufcall
// fails) to delay the thread creation until later time when more system
// resource may be available.
//
// We still return with success when the thread creation fails, knowing
// that the service will be in a non-stable state and any request on
// the service will block waiting for it to be stable.
//
void
repl_mgr_impl::rm_service_thread_create(
    rm_repl_service *new_s)
{
	CL_PANIC(new_s->need_thread());
#ifdef _KERNEL
	size_t stksize;

	// lint complains about _defaultstksize being undefined.
	// it is defnied at link time, hence the override.
	//lint -e40 -e574 -e737
	if (rm_thread_stacksize < 2*DEFAULTSTKSZ)
		stksize = 2*DEFAULTSTKSZ;
	else
		stksize = rm_thread_stacksize;
#if SOL_VERSION < __s9
	if ((thread_create(0, stksize, (void(*)(void))start_routine,
	    (caddr_t)new_s, 0UL, proc_cluster, TS_RUN, 60)) != NULL) {
#else
	if (clnewlwp_stack((void(*)(void *))start_routine, (void *)new_s,
	    60, NULL, NULL, stksize) == 0) {
#endif
		// Thread successfully created, clear the NEED_THREAD flag.
		new_s->thread_created();
		return;
	}

	RM_DBG(("repl_mgr_impl:  failed to create thread (%d)\n",
	    new_s->get_number()));

	// At least one of them should be zero.
	CL_PANIC(bufcallid == 0 || timeoutid == 0);

	// We need to try again later to create the thread. Only schedule
	// another job when there isn't one already scheduled.
	if (bufcallid == 0 && timeoutid == 0) {
		bufcallid = bufcall(DEFAULTSTKSZ, BPRI_HI, bufcallback, NULL);
		if (bufcallid == 0) {
			timeoutid = timeout(bufcallback, NULL,
			    drv_usectohz(500000UL));
			//
			// If timeout fails there is nothing much we can do.
			// We'd better panic this node since otherwise we
			// may deadlock the whole cluster.
			//
			if (timeoutid == 0) {
				//
				// SCMSGS
				// @explanation
				// The system could not create the needed
				// thread, because there is inadequate memory.
				// @user_action
				// There are two possible solutions. Install
				// more memory. Alternatively, reduce memory
				// usage.
				//
				(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC,
				    MESSAGE,
				    "HA: rm_service_thread_create failed");
			}
		}
	}
	//lint restore

#else // _KERNEL
	if (os::thread::create(NULL, (size_t)0, start_routine, new_s,
	    (long)THR_DETACHED, NULL)) {
		//
		// SCMSGS
		// @explanation
		// The system could not create the needed thread, because
		// there is inadequate memory.
		// @user_action
		// There are two possible solutions. Install more memory.
		// Alternatively, reduce memory usage.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: repl_mgr_impl: thr_create failed");
	} // thr_create

	// Thread successfully created, clear the NEED_THREAD flag.
	new_s->thread_created();
#endif // _KERNEL
}

#ifdef _KERNEL
// The callback called from bufcall.
void
repl_mgr_impl::bufcallback(void *)
{
	RM_DBG(("repl_mgr_impl: bufcallback\n"));
	repl_mgr_impl::get_rm()->create_service_threads();
}

// Called from bufcallback to try to create the threads again.
void
repl_mgr_impl::create_service_threads()
{
	rm_state_machine::state_mutex_lock();
	if (bufcallid != 0) {
		CL_PANIC(timeoutid == 0);
		bufcallid = 0;
	} else {
		CL_PANIC(timeoutid != 0);
		timeoutid = 0;
	}
	start_service_threads();
	rm_state_machine::state_mutex_unlock();
}
#endif // _KERNEL

// ----------------------------------------------------
// utility methods that execute on secondaries
void
repl_mgr_impl::create_service_on_sec(
    const char *new_service,
    const replica::service_dependencies &this_depends_on,
    replica::service_num_t sid,
    uint32_t desired_numsecondaries,
    bool is_deleted,
    const replica::service_state sstate)
{
	CL_PANIC(!primary_mode);

	// lock only because add_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = add_service(new_service, this_depends_on, sid,
	    desired_numsecondaries, sstate);

	ASSERT(s);

	// if this is called during add_secondary() on the RM,
	// the secondary needs to know if the service is deleted or dead.
	if (is_deleted) {
		// If it's shutdown on the primary we need to do the
		// proper things here, since we won't get any other events.
		s->mark_shutdown_succeeded();
	}

	rm_state_machine::state_mutex_unlock();
}

void
repl_mgr_impl::set_service_num_on_sec(replica::service_num_t sid)
{
	cur_num = sid;
}

service_admin_impl *
repl_mgr_impl::create_sai_on_sec(
    replica::service_num_t sid,
    replica_int::rm_service_admin_ptr sai)
{
	CL_PANIC(!primary_mode);

	// lock only because get_service expects lock to be held
	rm_state_machine::state_mutex_lock();

	rm_repl_service *s = get_service_incl_deleted(sid);
	CL_PANIC(s);

	// create shadow obj
	service_admin_impl *sec_sai = new service_admin_impl(sai, s);

	sai_list.append(sec_sai);
	rm_state_machine::state_mutex_unlock();

	return (sec_sai);
}

void
repl_mgr_impl::create_rma_reconf_on_sec(
    replica_int::rma_reconf_ptr client_rma)
{
	rma_reconf_list.lock();
	//
	// Deletes of rma objects are not check pointed to the secondaries.
	// So this is a chance to delete any references to RMAs on
	// dead nodes.
	//
	replica_int::rma_reconf_ptr rcf_p;
	rma_reconf_list.atfirst();
	while ((rcf_p = rma_reconf_list.current()) != NULL) {
		rma_reconf_list.advance();
		if (rcf_p->_handler()->server_dead()) {
			rma_reconf_list.erase(rcf_p);
			CORBA::release(rcf_p);
		}
	}

	CL_PANIC(!rma_reconf_list.exists(client_rma));

	rma_reconf_list.locked_append(
	    replica_int::rma_reconf::_duplicate(client_rma));
	rma_reconf_list.unlock();
}

void
repl_mgr_impl::dumpstate_complete()
{
	// Go through the service_list and call mark_shutdown_committed
	// on the services that are shutdown. We can't do this earlier
	// since before dumpstate completes we don't know whether
	// there is any service_admin that points to the shutdown
	// services.
	rm_repl_service *s;
	rm_state_machine::state_mutex_lock();
	SList<rm_repl_service>::ListIterator iter(service_list);
	while ((s = iter.get_current()) != NULL) {
		// advance first since we may delete the service from
		// the list in the loop.
		iter.advance();
		if (s->is_deleted()) {
			// This will delete s when appropriate.
			s->mark_shutdown_committed();
		}
	}
	rm_state_machine::state_mutex_unlock();
}

rm_repl_service *
repl_mgr_impl::remove_cleanup_task(replica::service_num_t sid)
{
	return (cleanup_task.remove(sid));
}

// -----------------------------------------------------------
// executed on primary only
//
// read_services() --	allocate services_up sequence;
//				if service up, write data to
//				service_up_info struct;
//
// This method is exited with all services locked.
//
// Note this method returns all the services including the shutdown ones.
// So it should only be used when dumping services to a rma.
//
void
repl_mgr_impl::read_services(replica_int::init_service_seq &service_seq)
{
	ASSERT(rm_state_machine::state_lock_held());

	uint_t i = 0;
	rm_repl_service	*s;
	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		service_seq[i].sid = s->get_number();
		service_seq[i].name = s->get_name();
		service_seq[i].state = s->get_state();
		if ((service_seq[i].state == replica::S_UP) ||
		    (service_seq[i].state == replica::S_FROZEN_FOR_UPGRADE) ||
		    (service_seq[i].state ==
		    replica::S_FROZEN_FOR_UPGRADE_BY_REQ)) {
			service_seq[i].primary = s->get_primary_ref();
		} else {
			service_seq[i].primary = nil;
		}
		RM_DBG(("rm_ri: add_rma adding service %s, state %d "
		    "to list of up services\n", s->id(), s->get_state()));
		i++;
	}

	iter.reinit(cleanup_task.services);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		service_seq[i].sid = s->get_number();
		service_seq[i].name = s->get_name();
		// Once a service is on the cleanup_task, the state machine
		// exits and no longer has valid state. Just set to S_DOWN.
		service_seq[i].state = replica::S_DOWN;
		service_seq[i].primary = nil;
		RM_DBG(("rm_ri: add_rma adding service %s "
		    "to list of down services\n", s->id()));
		i++;
	}

	service_seq.length(i);
	RM_DBG(("rm_ri: add_rma put %d service(s) in service list\n", i));
}

// Wait for all services to be in a stable state, so that we can perform
// operations that act on multiple services.
bool
repl_mgr_impl::check_all_stable()
{
	ASSERT(rm_state_machine::state_lock_held());

	bool stable = true;

	rm_repl_service		*s;

	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (!s->is_stable()) {
			RM_DBG(("rm_ri: check_all_stable() "
			    "failed on %s\n", s->id()));
			stable = false;
			break;
		}
	}
	return (stable);
}

// Wait for all services to be in a stable state, so that we can perform
// operations that act on multiple services.
void
repl_mgr_impl::wait_for_all_stable()
{
	ASSERT(rm_state_machine::state_lock_held());

	// Look up the dependency tree each time, since it might change.
	while (!check_all_stable()) {
		rm_state_machine::wait_for_external_state_change();
	}
}

// Wait for all below services to be in a stable state.
void
repl_mgr_impl::wait_for_below_stable(const replica::service_dependencies &dep,
    Environment &e)
{
	ASSERT(rm_state_machine::state_lock_held());
	ASSERT(!e.exception());

	while (!e.exception() && !check_service_dependencies(dep, e)) {
		rm_state_machine::wait_for_external_state_change();
	}
}


// --------------------------------------------------------------------

// Idl interface methods


//
// first step in the bringup of a service -- register it
// with the RM.
//
// checkpoint only in the non-error case (retry in error case
// will return same error as first-time, so can be handled
// same as non-retry).
// Note that checkpoint is sent before we call add_service()
// on the RMAs:  recovering RM will have called add_service()
// on any RMA that hadn't gotten the call before the RM crashed.
// no add_commit() needed, since we don't care if add_service()
// was called during original invocation.
//
void
repl_mgr_impl::register_service(const char *new_service,
    const replica::service_dependencies &this_depends_on,
    replica::service_num_t &sid, uint32_t desired_numsecondaries,
    const replica::service_state frozen_state,
    Environment &_environment)
{
	RM_DBG(("rm_ri:  got request to register "
	    "service `%s'\n",
	    new_service));
	rm_state_machine::state_mutex_lock();
	trans_state_register_service	*st
	    = get_saved_primary_reg_service_state(_environment);

	if (st) {
		// we don't care whether it's committed, since
		// RM would have called rm_repl_service::add_service()
		// during recovery.
		rm_repl_service *s = get_service(new_service);
		ASSERT(s);
		// XXX should probably make sure dependencies
		// have been added to the dependency list by
		// some kind of assertion.
		sid = s->get_number();
		RM_DBG(("rm_ri: retry for "
		    "register_service of service %s\n",
		    s->id()));
	} else {
		// determine whether rm_repl_service already exists
		rm_repl_service *s = get_service(new_service);
		if (s) {
			RM_DBG(("repl_mgr_impl: service %s already "
			    "exists\n", s->id()));

			// Wait for the service to stablize so we can
			// tell its state.
			s->wait_for_stable();

			replica::service_already_exists *sae = new
			    replica::service_already_exists(
			    s->map_service_state(), s->get_number());

			_environment.exception(sae);
		} else {
			// Wait for all below services to be stable, so
			// that we can do dependency checking. If any of
			// the below service isn't there, we will return
			// an exception.
			wait_for_below_stable(this_depends_on,
			    _environment);

			if ((s = get_service(new_service)) != NULL) {
				//
				// Method wait_for_below_stable may have
				// dropped the lock. We need to check whether
				// the service is registered again.
				//
				RM_DBG(("repl_mgr_impl: service %s already "
				    "exists\n", s->id()));
				s->wait_for_stable();

				replica::service_already_exists	*sae = new
				    replica::service_already_exists(
				    s->map_service_state(), s->get_number());

				_environment.exception(sae);

			} else if (desired_numsecondaries > MAX_SECONDARIES) {
				RM_DBG(("repl_mgr_impl: invalid "
				    "desired_numsecondaries \n"));
				_environment.exception(
				    new replica::invalid_numsecs());

			} else if ((sid = get_next_service_id()) ==
			    INVALID_SERVICE_ID) {
				RM_DBG(("repl_mgr_impl: too many services\n"));
				replica::too_many_services *tmsp = new
				    replica::too_many_services();
				_environment.exception(tmsp);

			} else if (!_environment.exception() &&
			    (frozen_state == replica::S_UP) &&
			    dependent_frozen_for_upgrade(this_depends_on)) {
				//
				// wait_for_below_stable() above
				// should have returned an exception
				// if a dependent service was not
				// found.
				//
				RM_DBG(("repl_mgr_impl: dep. svc frozen\n"));
				replica::dependent_service_frozen *dep_frzp =
					new replica::dependent_service_frozen;
				_environment.exception(dep_frzp);

			} else if (!_environment.exception()) {
				repl_rm::rm_ckpt_var rs_ckpt_v
				    = get_checkpoint();

				// FAULT POINT
				FAULTPT_HA_FRMWK(
				    FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_1,
				    FaultFunctions::generic);

				rs_ckpt_v->ckpt_register_service(
				    new_service,
				    this_depends_on,
				    sid,
				    desired_numsecondaries,
				    false, // is_deleted
				    cur_num,
				    frozen_state,
				    _environment);

				// FAULT POINT
				FAULTPT_HA_FRMWK(
				    FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_2,
				    FaultFunctions::generic);

				s = add_service(new_service, this_depends_on,
				    sid, desired_numsecondaries, frozen_state);
				// Wait for the service thread to perform the
				// add service. While we are waiting it is
				// possible that another thread will register a
				// provider and cause the service to go to
				// SERVICE_UP or SERVICE_DOWN, for that reason,
				// we wait for any stable state.
				s->wait_for_stable();
			}
		}
	} // retry with exception or new invo

	rm_state_machine::state_mutex_unlock();
	RM_DBG(("rm_ri:  done register service `%s'\n", new_service));
} // register_service

//
// get a list of services that are known to the RM and not deleted:  for each
// service, get service_info struct data.
// Note that this information is a snapshot of what the RM knows about the
// service, and can be out-of-date by the time the invocation returns.
//
void
repl_mgr_impl::get_services(replica::rm_service_list_out sl, Environment &)
{
	rm_state_machine::state_mutex_lock();
	const uint_t len = service_list.count();

	replica::rm_service_list *sl_tmp =
	    new replica::rm_service_list(len);
	replica::rm_service_list	&sl_ref = *sl_tmp;
	uint_t 			j = 0;
	rm_repl_service	*s;
	SList<rm_repl_service>::ListIterator iter(service_list);
	for (; (s = iter.get_current()) != NULL; iter.advance()) {
		if (s->is_deleted()) {
			continue;
		}
		sl_ref[j].sid 		= s->get_number();
		sl_ref[j].service_name	= s->get_name();
		sl_ref[j].s_state	= s->get_state();

		s->get_deps_down(&sl_ref[j].this_depends_on);
		s->get_deps_up(&sl_ref[j].depend_on_this);
		j++;
	}
	rm_state_machine::state_mutex_unlock();

	sl_tmp->length(j);
	replica::rm_service_list_out out(sl_tmp);
	sl = out;
}

//
// get a control interface object for the specified service:
// the reference to this object is used to do further administration
// on the service, such as registering a repl_prov, shutting down
// the service, etc.
//
// checkpoint only in the non-error case (retry in error case
// will return same error as first-time, so can be handled
// same as non-retry).
//
replica::service_admin_ptr
repl_mgr_impl::get_control(const char *service_name, Environment &_environment)
{
	rm_state_machine::state_mutex_lock();
	service_admin_impl 	*sai = NULL;
	replica_int::rm_service_admin_ptr sai_ref = nil;

	rm_repl_service *const	s   = get_service(service_name);

	trans_state_get_control	*st
	    = get_saved_primary_get_control_state(_environment);
	if (st) {
		// retry
		sai	= st->get_return_value();
		ASSERT(sai);
		CL_PANIC(sai_list.exists(sai));
		ASSERT(s);
		RM_DBG(("rm_ri: retry for "
		    "get_control of service %s, sai `%p'\n",
		    s->id(), sai));
		sai_ref = sai->get_objref();
	} else {
		if (s == NULL) {
			_environment.exception(new replica::unknown_service);
		} else {
			s->incr_service_refs();	// Prevent from being deleted.

			if (!s->is_deleted()) {
				sai = new service_admin_impl(get_rm_prov(), s);
				ASSERT(sai);
				sai_list.append(sai);
				sai_ref = sai->get_objref();

				// FAULT POINT
				FAULTPT_HA_FRMWK(
					FAULTNUM_HA_RM_CKPT_GET_CONTROL_1,
					FaultFunctions::generic);

				repl_rm::rm_ckpt_var ckpt_v = get_checkpoint();
				ckpt_v->ckpt_get_control(s->get_number(),
				    sai_ref, _environment);

				// FAULT POINT
				FAULTPT_HA_FRMWK(
					FAULTNUM_HA_RM_CKPT_GET_CONTROL_2,
					FaultFunctions::generic);

			} else {
				_environment.exception(
				    new replica::unknown_service);
				RM_DBG(("get_control: %s shutdown\n",
				    service_name));
			}
			s->decr_service_refs();	// release hold
		}
	} // retry with exception or new invo
	rm_state_machine::state_mutex_unlock();
	// lint -- can't tell whether sai has been set at this point
	return (sai_ref);
} // get_control

//
// repl_mgr_impl(replica_int::rm::add_rma)
// give the RM a reference to an rma_reconf object:  the RM
// stores one such object for each RMA (one for each node in
// the cluster), and uses it to invoke each RMA during
// reconfiguration.
// Note that the invoking RMA is responsible for ensuring that
// this is called only once for each incarnation of itself.
//
void
repl_mgr_impl::add_rma(replica_int::rma_reconf_ptr client_rma,
	Environment &_environment)
{
	RM_DBG(("rm_ri: add_rma()\n"));

	trans_state_add_rma	*trans_statep =
	    get_saved_primary_add_rma_state(_environment);

	if (trans_statep) {
		if (trans_statep->is_committed()) {
			return;
		}
		RM_DBG(("rm_ri: retry for add_rma, reconf_ptr `%p'\n",
		    client_rma));
		//
		// If the recovering RM found that init_services was called on
		// this RMA, then it would have committed it and set adding_rma
		// to nil.
		// Otherwise, the recovering RM has ignored this RMA, so
		// we fall through.
		//
		if (CORBA::is_nil(adding_rma)) {
			return;
		}
	}
	//
	// This tricky piece of locking allows us to get both the state lock
	// and the rma lock and guarantee that all services are in a stable
	// state.
	// We must get the rma_reconf_list lock first, since it it held across
	// invocations. The state lock is used to protect the state changes
	// that we care about. We must drop the rma_reconf_list lock when we
	// are waiting for a state change, since it may be needed for the
	// states to stabilize.
	//
	rma_reconf_list.lock();
	rm_state_machine::state_mutex_lock();

	while (!check_all_stable()) {
		rma_reconf_list.unlock();
		rm_state_machine::wait_for_external_state_change();

		//
		// It is safe to drop the state mutex and acquire it
		// again, since we are going to recheck everything.
		//
		rm_state_machine::state_mutex_unlock();
		rma_reconf_list.lock();
		rm_state_machine::state_mutex_lock();
	}
	cleanup_task.lock();

	// Take a snapshot of the services' state.
	replica_int::init_service_seq service_seq(service_list.count() +
	    cleanup_task.services.count());

	read_services(service_seq);

	//
	// Now that we have a snapshot of the services' state, we can
	// drop the state lock. This allows state changes to occur, but
	// by holding on to the rma lock across the init_services()
	// invocation below, we guarantee that no updates that are
	// relevant to the rma are done until we have finished adding
	// this one.
	//
	rm_state_machine::state_mutex_unlock();
	cleanup_task.unlock();

	// add to list and checkpoint before init_services() is called.
	rma_reconf_list.locked_append(
	    replica_int::rma_reconf::_duplicate(client_rma));

	repl_rm::rm_ckpt_var rm_ckpt_v = get_checkpoint();

	// FAULT POINT
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_RM_CKPT_ADD_RMA_1,
		FaultFunctions::generic);

	// Only checkpoint if this is not a retry.
	if (trans_statep == NULL) {
		rm_ckpt_v->ckpt_add_rma(client_rma, _environment);
		_environment.clear();

		// FAULT POINT
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_RM_CKPT_ADD_RMA_2,
			FaultFunctions::generic);

	}

	// Need a new environment for a nested invocation.
	Environment	e;
	client_rma->init_services(service_seq, e);
	if (e.exception()) {
		RM_DBG(("rm_ri: exception from init_services\n"));
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			e.clear();

			//
			// if the RMA client went down, the secondary will find
			// out if it becomes a primary and tries to use it.
			// This creates a small waste of memory, but machines
			// shouldn't be rebooted very often.
			// XXX alternative is to send another checkpoint
			// indicating failure?  Doesn't seem like a big
			// problem, since the client could go down at any time,
			// and we're not checkpointing that to the
			// secondaries.
			//
			rma_reconf_list.erase(client_rma);
			CORBA::release(client_rma);

			RM_DBG(("rm_ri: rma_reconf_exception, released ref "
			    "`%p'\n", client_rma));
		} else {
			CL_PANIC(0);
		}
	} else {
		RM_DBG(("rm_ri: returned from init_services\n"));
	}
	add_commit(_environment); // Before dropping lock.

	rma_reconf_list.unlock();

	// Allow external requests to lock trees.
	rm_state_machine::state_mutex_lock();
	rm_state_machine::signal_external_state_change();
	rm_state_machine::state_mutex_unlock();

	// FAULT POINT
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_REPL_MGR_IMPL_ADD_RMA,
		FaultFunctions::generic);
	RM_DBG(("rm_ri: add_rma() done\n"));
} // add_rma

void
repl_mgr_impl::add_rma_started_ckpt(replica_int::rma_reconf_ptr rma_p)
{
	CL_PANIC(CORBA::is_nil(adding_rma));
	adding_rma = replica_int::rma_reconf::_duplicate(rma_p);
}

void
repl_mgr_impl::add_rma_done_ckpt()
{
	CL_PANIC(!CORBA::is_nil(adding_rma));
	create_rma_reconf_on_sec(adding_rma);
	CORBA::release(adding_rma);
	adding_rma = nil;
}

//
// add_rma
//	Obtains rma reconf reference from the rmm
//
void
repl_mgr_impl::add_rma(const char *sec_name)
{
	replica_int::rma_reconf_ptr rcf_p = nil;

	nodeid_t nid = (nodeid_t)os::atoi(sec_name);
	rma_reconf_list.lock();

	if (nid == orb_conf::local_nodeid()) {
		ASSERT(rma_reconf_list.empty());
		//
		// Get RMA reconf reference on this node
		//
		rcf_p = rma::the().get_reconf();
	} else {
		// Check if we have a reference to this node.
		replica_int::rma_reconf_ptr reconf_p;
		remote_handler *hdlp = NULL;
		rxdoor *xdp = NULL;

		for (rma_reconf_list.atfirst();
		    (reconf_p = rma_reconf_list.current()) != NULL;
		    rma_reconf_list.advance()) {
			hdlp = (remote_handler *)reconf_p->_handler();
			xdp = (rxdoor *)(hdlp->get_xdoorp());
			// xdp == null for local reference
			if (xdp && (xdp->get_server_node().ndid == nid)) {
				//
				// If we have a reconf pointer for
				// this node, the node must have
				// rebooted.
				//
				ASSERT(xdp->server_dead());
				//
				// we have a reference to dead node
				// remove it from the list;
				//
				RM_DBG(("rm_ri: add_rma(%s) removed dead"
				    " reference\n", sec_name));
				rma_reconf_list.erase(reconf_p);
				CORBA::release(reconf_p);
				break;
			}
		}
		//
		// We do not have a reference to this node. Get one from
		// the RMM.
		//
		// lint complains about rmm::the() being undefined.
		//
		//lint -e10 -e64  -e530 -e746 -e1013 -e1015 -e1055
		rcf_p = rmm::the().get_rma_reconf_for_node(nid);
		//lint restore
		ASSERT(!CORBA::is_nil(rcf_p));
	}

	RM_DBG(("rm_ri: add_rma(%s) %p\n", sec_name, rcf_p));
	rma_reconf_list.locked_append(rcf_p);
	rma_reconf_list.unlock();
}

//
// empty_reconf_list
//
// Called on RM secondary during upgrade_callback
//
void
repl_mgr_impl::empty_reconf_list()
{
	replica_int::rma_reconf_ptr rcf_p = NULL;
	while ((rcf_p = rma_reconf_list.reapfirst()) != NULL) {
		CORBA::release(rcf_p);
		RM_DBG(("rm_ri: "
		    "empty_reconf_list(), released rma_reconf ref `%p'\n",
		    rcf_p));
	}
}

void
repl_mgr_impl::erase_service(rm_repl_service *s)
{
	// Only called during shutdown, after we can exit the state machine.
	CL_PANIC(s->can_exit_state_machine());

	if (is_primary_mode()) {
		RM_DBG(("rm_ri: %s erasing service on primary\n", s->id()));
		CL_PANIC(s->is_stable());
	} else {
		RM_DBG(("rm_ri: %s erasing service on secondary\n", s->id()));
	}

	bool er = service_list.erase(s);
	CL_PANIC(er);

	//
	// Move it to the cleanup list. When the primary RM finishes
	// the cleanup it'll send a checkpoint to secondaries to
	// remove it from the cleanup list.
	//
	cleanup_task.append(s);
	RM_DBG(("rm_ri: %s move service to cleanup list\n", s->id()));
}

// Called from repl_mgr_impl::periodic_cleanup_task::execute. Try and see
// if we can clean any dead services up.
void
repl_mgr_impl::cleanup_dead(Environment &e)
{
	rm_repl_service *s;
	rma_reconf_list.lock();
	rm_state_machine::state_mutex_lock();
	// It's ok not to get the lock for the cleanup_task because this
	// is the only place where we call cleanup_task::remove on the primary.
	// Other threads only add to cleanup_task list and it won't disturb
	// us.

	SList<rm_repl_service>::ListIterator iter(cleanup_task.services);

	while ((s = iter.get_current()) != NULL) {
		CL_PANIC(s->can_exit_state_machine());

		// advance first since we may delete s from the list.
		iter.advance();
		version_manager::vp_version_t	rv;

		prov_impl->get_running_version(rv);

		if (RM_ADMIN_VERS(rv) == 0) {
			//
			// Block marshals. After this step we are
			// guaranteed that there is no hxdoor in
			// transit.
			//
			RM_DBG(("rm_ri Calling block_marshals\n"));
			s->iterate_over(
			    &replica_int::rma_reconf::block_marshals,
			    "block_marshals");

		} else {
			RM_DBG(("rm_ri Calling shutdown_check_prepare\n"));
			s->iterate_over(
			    &replica_int::rma_reconf::shutdown_check_prepare,
			    "shutdown_check_prepare");
		}
		//
		// Ask all the rmas about whether they have any hxdoor for this
		// service. If the answer is no for all of them then we can
		// clean the service up.
		//
		replica_int::rma_reconf_ptr p;

		bool can_cleanup = true;
		for (rma_reconf_list.atfirst();
		    (p = rma_reconf_list.current()) != NULL;
		    rma_reconf_list.advance()) {
			Environment local_env;

			// Check whether we can shutdown and reenable
			// marshal. Note that even though once we get
			// a can't answer we know we can't cleanup, we
			// still need to call shutdown_ready for the rest
			// of rmas because we need to reenable marshal.
			if (!p->shutdown_ready(s->get_number(), local_env) &&
			    local_env.exception() == NULL) {
				can_cleanup = false;
			}
			CL_PANIC(local_env.exception() == NULL ||
			    CORBA::COMM_FAILURE::_exnarrow(
			    local_env.exception()));
		}

		if (can_cleanup) {
			RM_DBG(("rm_ri: %s cleanup dead\n", s->id()));
			// Do the cleanup. Note that it's always possible
			// that we cleanup the service on the rmas and then die
			// without sending the checkpoint. It's ok since the
			// shutdown_service operation on the rma is idempotent.
			s->iterate_over(
			    &replica_int::rma_reconf::shutdown_service,
			    "shutdown_service");

			// Checkpoint the cleanup to the secondaries.
			// We don't need to get the state_machine lock
			// since we add a job to the cleanup_task with
			// the lock held and the service's state doesn't
			// change afterwards.
			repl_rm::rm_ckpt_var ckpt_v =  get_checkpoint();
			ckpt_v->ckpt_cleanup_dead_service(s->get_number(), e);

			// Remove the service from the list and free
			// memory.
			(void) cleanup_task.remove(s->get_number());
			delete s;
		} else {
			RM_DBG(("rm_ri: %s dirty dead\n", s->id()));
		}
	}

	rm_state_machine::state_mutex_unlock();
	rma_reconf_list.unlock();
}

//
// This is called by the version manager in order to create a
// cb_coord_control object and the linkage between it and the LCA(local
// callback agent) object.  There is one pair of cb_coord_control and lca
// object per node in the cluster.
//
replica_int::cb_coord_control_ptr
repl_mgr_impl::register_lca(version_manager::vm_lca_ptr new_lca_p,
    Environment &e)
{
	RM_DBG(("rm_ri: repl_mgr_impl::register_lca\n"));

	//
	// No need to grab rm_state_macine::state_mutex_lock because
	// the cb_coordinator has it's own lock.
	//
	cb_coord_control_impl			*impl_objp = NULL;
	replica_int::cb_coord_control_ptr	cbcntl_p = nil;

	primary_ctx	*ctxp = primary_ctx::extract_from(e);
	trans_state_register_lca	*st
	    = (trans_state_register_lca *) ctxp->get_saved_state();

	if (st) {
		// A retry
		impl_objp = st->get_control_impl();
		ASSERT(impl_objp != NULL);
		cbcntl_p = impl_objp->get_objref();
	} else {
		// A new invocation

		// Create a new cb_coord_control object to link with the lca.
		impl_objp = new cb_coord_control_impl(get_rm()->get_rm_prov(),
		    new_lca_p, &cb_coord);

		cb_coord.add_cb_coord_control(impl_objp);

		cbcntl_p = impl_objp->get_objref();

		FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_BEFORE_CKPT_REGISTER_LCA,
		    FaultFunctions::generic);

		// Checkpoint the creation of the new cb_coord_control object
		repl_rm::rm_ckpt_var ckpt_v =  get_checkpoint();
		ckpt_v->ckpt_register_lca(new_lca_p, cbcntl_p, e);
		ASSERT(!e.exception());

		FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_AFTER_CKPT_REGISTER_LCA,
		    FaultFunctions::generic);
	}

	return (cbcntl_p);
}

//
// Called by ckpt_register_lca to create cb_coord_control object
// on RM secondaries.
//
cb_coord_control_impl *
repl_mgr_impl::register_lca_on_sec(version_manager::vm_lca_ptr new_lca_p,
    replica_int::cb_coord_control_ptr cbcntl_p)
{
	//
	// Create a new cb_coord_control_impl to link with the given lca
	// on RM secondaries.
	//
	cb_coord_control_impl	*impl_objp = new cb_coord_control_impl(
					cbcntl_p, new_lca_p, &cb_coord);
	cb_coord.add_cb_coord_control(impl_objp);

	return (impl_objp);
}

//
// get_next_service_id
//
// Find the next usable service id.
// Returns INVALID_SERVICE_ID if it cannot allocate one.
//
// The service id ranges from STARTING_SERVICE_ID up to 2^16-1. It's possible
// that the id gets wrapped around so we want to make sure that we allocate
// the new registering service a service id that is not been used by any one
// in the the cluster.
//
replica::service_num_t
repl_mgr_impl::get_next_service_id()
{
	ASSERT(rm_state_machine::state_lock_held());

	replica::service_num_t orig_cur_num, svc_num;

	//
	// Allocate numbers from STARTING_SERVICE_ID upto 2^16-1. If
	// cur_num == (2^16-1), then cur_num + 1 = 0, due to
	// wraparound.
	//
	orig_cur_num = cur_num;

	//
	// If we don't find an unused id for the new service, we will
	// return INVALID_SERVICE_ID which is 0.
	//
	svc_num = INVALID_SERVICE_ID;

	do {
		//
		// Attempt to find an unused service id for the new registering
		// service. If all service Id's have been taken, return
		// INVALID_SERVICE_ID.
		//
		if (cur_num == INVALID_SERVICE_ID) {
			//
			// cur_num has wrapped around to 0. It needs to
			// be reinitialized to STARTING_SERVICE_ID
			//
			RM_DBG(("rm_ri: repl_mgr_impl::get_next_service_id "
			    "wraparound\n"));
			cur_num = STARTING_SERVICE_ID;
		}

		//
		// check if the id has being taken by either an alive or a dead
		// service.
		//
		rm_repl_service	*s;
		SList<rm_repl_service>::ListIterator iter(service_list);
		for (; (s = iter.get_current()) != NULL; iter.advance()) {
			if (cur_num == s->get_number()) {
				RM_DBG(("rm_ri: "
				    "repl_mgr_impl::get_next_service_id "
				    "found %d in live service list.\n",
				    cur_num));
				break;
			}
		} // for
		if (s != NULL) {
			// This id has been taken. move to next id.
			cur_num++;
			continue;
		}

		iter.reinit(cleanup_task.services);
		for (; (s = iter.get_current()) != NULL; iter.advance()) {
			if (cur_num == s->get_number()) {
				RM_DBG(("rm_ri: "
				    "repl_mgr_impl::get_next_service_id "
				    "found %d in dead service list.\n",
				    cur_num));
				break;
			}
		}
		if (s != NULL) {
			// This id has been taken, move to next id.
			cur_num++;
			continue;
		}

		//
		// the cur_num has not been taken, we can assign this to
		// the new service.
		//
		svc_num = cur_num;

		// increase the cur_num by one for future use
		cur_num++;

		RM_DBG(("rm_ri: "
		    "repl_mgr_impl::get_next_service_id "
		    "found svc_num %d, cur_num is %d.\n", svc_num,
		    cur_num));
		break;
	} while (cur_num != orig_cur_num); // See if we have looped back or not.

	return (svc_num);
}

bool
repl_mgr_impl::dependent_frozen_for_upgrade(const replica::service_dependencies
	&svc_depends_on)
{
	rm_repl_service *sp;

	for (uint_t i = 0; i < svc_depends_on.length(); i++) {
		sp = get_service(svc_depends_on[i]);
		ASSERT(sp);
		if (sp->is_frozen_for_upgrade()) {
			return (true);
		}
	}
	return (false);
}

void
repl_mgr_impl::periodic_cleanup_task::append(rm_repl_service *s)
{
	// Should always append a job with the state mutex held. This
	// is because as soon as we put something here, we could send
	// out a cleanup checkpoint to the secondaries. Since at all the
	// other places where we change the service's state we hold the
	// state mutex, and after we put a service here the service's
	// state no longer changes, getting the state mutex here
	// guarantees that all the checkpoints are received by the
	// secondaries in the right order.
	CL_PANIC(s->is_deleted());
#ifdef DEBUG
	// State machine related asserts can only be done on primary.
	if (repl_mgr_impl::get_rm()->is_primary_mode()) {
		CL_PANIC(s->is_stable() && s->is_down());
	}
#endif // DEBUG
	lck.lock();
	services.append(s);
	lck.unlock();
}

rm_repl_service *
repl_mgr_impl::periodic_cleanup_task::remove(replica::service_num_t sid)
{
	rm_repl_service *s = NULL;
	lck.lock();
	SList<rm_repl_service>::ListIterator iter(services);
	while ((s = iter.get_current()) != NULL) {
		// advance first since we may delete s from the list.
		iter.advance();

		if (s->get_number() == sid) {
			(void) services.erase(s);
			break;
		}
	}
	lck.unlock();
	return (s);
}

// Run once every 300 seconds. Note that get_period returns a number
// with unit of 10 seconds.
uint_t
repl_mgr_impl::periodic_cleanup_task::get_period()
{
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	//
	// This is in unode test mode.
	//
	// In unode mode, the thread gets executed every 10 seconds.
	// so that the tests don't have to wait for too long for
	// the cleanup thread to wake up.
	//
	return (1);
#else
	// In real cluster mode
	return (30);
#endif
}

repl_mgr_impl::periodic_cleanup_task::~periodic_cleanup_task()
{
	services.dispose();
}

// Called by the periodic_thread when it wants to run this task.
// It calls the idl function cleanup_dead on the ha rm service.
void
repl_mgr_impl::periodic_cleanup_task::execute()
{
	Environment e;
	replica_int::rm_var rm_ref = repl_mgr_impl::get_rm()->get_objref();
	rm_ref->cleanup_dead(e);
	CL_PANIC(!e.exception());
}

//
// freeze_for_upgrade
//
// Called by the Version Manager to freeze HA service for upgrade.
// This will also be called by the framework if the RM primary
// fails. The method is idempotent and will do nothing if the service
// is already frozen.
//
void
repl_mgr_impl::freeze_for_upgrade(const char *svc_name, Environment &e)
{
	rm_state_machine::state_mutex_lock();

	rm_repl_service	*s = get_service(svc_name);
	if (s == NULL) {
		rm_state_machine::state_mutex_unlock();
		e.exception(new replica::unknown_service);
		return;
	}

	s->freeze_for_upgrade();

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_CKPT_FREEZE_FOR_UPGRADE_1,
		FaultFunctions::generic);

	//
	// The checkpoint is sent after the freeze_for_upgrade has
	// been done on the service primary. This is intentional in
	// order to handle scenarios like the following: The RM
	// primary node dies while this invocation in ongoing. The
	// recovering RM would find frozen_for_upgrade flag set for
	// the service, and the RM would take the service to
	// frozen_for_upgrade directly without having actually called
	// freeze_primary or freeze_service on other nodes.
	//
	// If the failure happens before checkpoint has been sent,
	// then the repl_mgr_impl::freeze_for_upgrade() invocation
	// itself would be retried on the new RM primary and would
	// cause service primary and sec/clients to be frozen.
	//
	repl_rm::rm_ckpt_var ckpt_v =  get_checkpoint();
	ckpt_v->ckpt_freeze_for_upgrade(svc_name, e);

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_CKPT_FREEZE_FOR_UPGRADE_2,
		FaultFunctions::generic);

	//
	// If the failure happens after the checkpoint has been sent,
	// then we know that the primary was frozen as well as
	// freeze_service_invos was also called on all other rmas. So
	// it is ok to go directly to frozen_for_upgrade() state.
	//
	rm_state_machine::state_mutex_unlock();
}

//
// unfreeze_after_upgrade
//
// Called by the Version Manager to unfreeze HA service after upgrade.
// This will also be called by the framework if the RM primary
// fails. The method is idempotent and will do nothing if the service
// is not frozen.
//
void
repl_mgr_impl::unfreeze_after_upgrade(const char *svc_name, Environment &e)
{
	rm_state_machine::state_mutex_lock();

	rm_repl_service	*s = get_service(svc_name);
	if (s == NULL) {
		e.exception(new replica::unknown_service);
		// service does not exist
		rm_state_machine::state_mutex_unlock();
		return;
	}
	if (!s->is_frozen_for_upgrade()) {
		// service not frozen
		rm_state_machine::state_mutex_unlock();
		return;
	}

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_CKPT_UNFREEZE_AFTER_UPGRADE_1,
		FaultFunctions::generic);

	repl_rm::rm_ckpt_var ckpt_v =  get_checkpoint();
	ckpt_v->ckpt_unfreeze_after_upgrade(svc_name, e);

	// FAULT POINT
	FAULTPT_HA_FRMWK(FAULTNUM_HA_RM_CKPT_UNFREEZE_AFTER_UPGRADE_2,
		FaultFunctions::generic);
	s->unfreeze_after_upgrade(e);

	if (e.exception()) {
		CL_PANIC(replica::dependent_service_frozen::_exnarrow(
		    e.exception()));
		rm_state_machine::state_mutex_unlock();
		return;
	}
	rm_state_machine::state_mutex_unlock();
}

//
// get_services_frozen_for_upgrade
//
// Returns list of services frozen for upgrade. This does not include
// dependent services that are frozen implicitly by the framework
// in order to freeze a bottom level service.
//
void
repl_mgr_impl::get_services_frozen_for_upgrade(
    replica::service_frozen_seq_out svcs,
    Environment &)
{
	rm_state_machine::state_mutex_lock();
	uint_t num_svcs = service_list.count();

	rm_repl_service		*s;
	SList<rm_repl_service>::ListIterator iter(service_list);
	uint_t			i;
	char **int_seq =  new char *[num_svcs];
	for (i = 0; (s = iter.get_current()) != NULL; iter.advance()) {
		if (s->frozen_for_upgrade_by_req()) {
			int_seq[i] = os::strdup(s->get_name());
			i++;
		}
	} // for

	svcs = new replica::service_frozen_seq(i, i, int_seq, true);

	rm_state_machine::state_mutex_unlock();
}

// Perform upgrade commit in the cluster. Called from version_manager.
void
repl_mgr_impl::upgrade_commit(version_manager::vp_state &s,
    version_manager::membership_bitmask_t &outdated_nodes, Environment &e)
{
	// Redirect the request to the cb_coordinator.
	cb_coord.upgrade_commit(s, outdated_nodes, e);
}

// Set upgrade_commit_progress on the local cb_coordinator and ckpt it.
void
repl_mgr_impl::set_upgrade_commit_progress(
    version_manager::commit_progress new_progress,
    Environment &e)
{
	cb_coord.wrlock_rwlock();
	cb_coord.set_local_upgrade_commit_progress(new_progress);
	get_checkpoint()->ckpt_set_upgrade_commit_progress(new_progress, e);
	cb_coord.unlock_rwlock();
}

// Cleanup UCCs that are no longer needed
void
repl_mgr_impl::unregister_uccs(Environment &e)
{
	cb_coord.unregister_uccs(e);
}

// Generic method.
void
repl_mgr_impl::_generic_method(CORBA::octet_seq_t &data,
    CORBA::object_seq_t &objs, Environment &e)
{
	if (data.length() == 0) {
		//
		// The data field specifies the actual action, return exception
		// it's empty.
		//
		e.system_exception(CORBA::VERSION(1, CORBA::COMPLETED_NO));
		return;
	}

	if (os::strncmp((const char *)data.buffer(),
	    "get_highest_version_root_obj_ref", (ulong_t)33) == 0) {
		//
		// This is a request to get a highest version root obj
		// reference.
		//
		objs.length(1);
		objs[0] = get_objref();
	} else {
		e.system_exception(CORBA::VERSION(1, CORBA::COMPLETED_NO));
	}
}
