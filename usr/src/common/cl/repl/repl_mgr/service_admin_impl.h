/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SERVICE_ADMIN_IMPL_H
#define	_SERVICE_ADMIN_IMPL_H

#pragma ident	"@(#)service_admin_impl.h	1.41	08/05/20 SMI"

//
// service_admin_impl.h contains class definitions for the service_admin
// interface portion of the replica management framework.
//
#include <orb/object/adapter.h>
#include <h/repl_rm.h>
#include <repl/service/replica_tmpl.h>
#include <repl/repl_mgr/repl_mgr_prov_impl.h>

class rm_repl_service;
class trans_state_reg_rma_repl_prov;
class trans_state_shutdown_service;

typedef uint_t	service_state_count;

class service_admin_impl :
    public mc_replica_of<replica_int::rm_service_admin> {
public:
	~service_admin_impl();

	// primary constructor
	service_admin_impl(repl_mgr_prov_impl *rm_prov,
	    rm_repl_service *const svc);

	// secondary constructor
	service_admin_impl(replica_int::rm_service_admin_ptr,
	    rm_repl_service *const svc);

	void _unreferenced(unref_t);

	//
	// determine whether this control interface has an out-of-date
	// view of the service.
	//
	void chk_state_change(Environment &_environment);

	void set_state_count(const service_state_count cnt);

	// Idl interfaces
	CORBA::Object_ptr get_root_obj(Environment &_environment);
	void get_repl_provs(replica::repl_prov_seq_out repl_provs,
	    Environment &_environment);
	replica::service_info* get_service_info(Environment &_environment);
	void change_repl_prov_status(const char *repl_prov_desc,
	    replica::change_op change, bool ignore_state_change,
	    Environment &_environment);
	void change_repl_prov_priority(
	    const replica::prov_priority_list &prov_list,
	    Environment &_environment);
	void change_desired_numsecondaries(uint32_t new_desired_numsecondaries,
	    Environment &_environment);
	uint32_t shutdown_service(bool is_forced_shutdown,
	    Environment &_environment);
	/* rm_service_admin */
	void register_rma_repl_prov(replica_int::rma_repl_prov_ptr prov,
	    const char *prov_desc,
	    const replica::prov_dependencies &prov_depends_on_desc,
	    replica::checkpoint_ptr chkpt,
	    uint32_t priority, replica::repl_prov_reg_out reg_cb,
	    Environment &_environment);

	class sa_state_count {
	public:
		sa_state_count();
		~sa_state_count();
		void set_state_count(const service_state_count sa_cnt);
		const service_state_count	&get_count();
	private:
		service_state_count	service_count;
	};
	const sa_state_count &get_sa_state_count();

	// used by checkpoint method in add_secondary()
	const rm_repl_service &get_service();

	// Checkpoint accessor function.
	repl_rm::rm_ckpt_ptr	get_checkpoint();

	//
	// called by trans_state_shutdown_service to find out whether
	// it was a forced shutdown in progress.
	//
	bool was_forced_shutdown();
private:
	//
	// pointer to corresponding rm_repl_service object is
	// needed because administrative operations are actually
	// done in rm_repl_service methods.
	//
	rm_repl_service 		&s;
	sa_state_count			service_admin_state_count;
	bool				forced_shutdown;

	void perform_reg_rma_repl_prov_checks(
	    const char *prov_desc, Environment &_env);

	trans_state_reg_rma_repl_prov	*
	    get_saved_primary_reg_rma_repl_prov_state(Environment &);
	trans_state_shutdown_service	*
	    get_saved_primary_shutdown_service_state(Environment &);
};

#include <repl/repl_mgr/service_admin_impl_in.h>

#endif	/* _SERVICE_ADMIN_IMPL_H */
