/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_repl_service.cc	1.164	08/05/20 SMI"

//
// rm_repl_service.cc implements the (non-Corba) rm_repl_service class.
//

#include <repl/repl_mgr/rm_repl_service.h>
#include <repl/repl_mgr/rm_repl_service_in.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <repl/repl_mgr/rs_trans_states.h>
#include <repl/repl_mgr/rm_state_mach.h>
#include <repl/repl_mgr/rm_state_mach_in.h>
#include <orb/infrastructure/orb.h>
#include <sys/vm_util.h>
#include <repl/repl_mgr/rm_version.h>

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_functions.h>
#endif
//
// During reconfiguration, each RMA is invoked by the RM to perform
// certain local operations (e.g., freeze the service on the local
// node).
//
// XXX Should probably be moved to a different file.
void
rm_repl_service::iterate_over(void (replica_int::rma_reconf::*mfn)(
	replica::service_num_t sid, Environment &_environment),
#if defined(FAULT_HA_FRMWK) || defined(RECONF_DBGBUF)
	const char *const fn_name)
#else
	const char *const)
#endif
{
	rm_state_machine::state_mutex_unlock();

	repl_mgr_impl::rcf_list		&rma_list
		= repl_mgr_impl::get_rcf_list();

	bool obtained_reconf_lock = false;
	if (!rma_list.lock_held()) {
		rma_list.lock();
		obtained_reconf_lock = true;
	}
	replica_int::rma_reconf_ptr		p;

#if defined(FAULT_HA_FRMWK)
	int fault_count = 0;
#endif

	for (rma_list.atfirst(); (p = rma_list.current()) != NULL; ) {
		Environment e;

#if defined(FAULT_HA_FRMWK)
		void *fault_argp;
		uint32_t fault_argsize;

		if (fault_triggered(
		    FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER,
		    &fault_argp,
		    &fault_argsize)) {
			uint_t fnum = FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER;

			FaultFunctions::ha_rm_reboot_arg_t *arg =
			    (FaultFunctions::ha_rm_reboot_arg_t *) fault_argp;

			if (arg->nodeid == orb_conf::node_number() &&
			    (os::strcmp(fn_name, arg->fn_name) == 0) &&
			    (os::strcmp(get_name(), "ha_sample_server") == 0)) {

				fault_count++;
				if (fault_count > 1) {
					FaultFunctions::wait_for_arg_t
					    wait_arg;
					wait_arg.op = arg->op;
					wait_arg.nodeid = arg->nodeid;

					FaultFunctions::reboot(fnum, &wait_arg,
					    (uint32_t)sizeof (wait_arg));
				}
			}
		}
#endif

		(p->*mfn)(get_number(), e);
		if (e.exception()) {
			RECONF_DBG(("rm_rs: got exception on "
			    "rma_reconf `%p' for service `%s'\n", p,
			    get_name()));
			rma_reconf_exception(p, e);
		} else {
			rma_list.advance();
		}
	}
	if (obtained_reconf_lock) {
		rma_list.unlock();
	}

	rm_state_machine::state_mutex_lock();
	RECONF_DBG(("rm_rs %s: returned from %s\n", id(), fn_name));
}

//
// Special handler for the case for add_service since its
// parameters are different. We could make iterate_over more general
// and not have this duplication, or dependencies any requiremnt on
// the type of functions it calls, but it's probably not worth the
// effort if add_service is the only exception.
//
// Used by rm_state_machine::service_registered in rm_state_mach.cc
//
void
rm_repl_service::iterate_services(const char *fn_name)
{
	rm_state_machine::state_mutex_unlock();

	repl_mgr_impl::rcf_list		&rma_list
		= repl_mgr_impl::get_rcf_list();

	bool obtained_reconf_lock = false;
	if (!rma_list.lock_held()) {
		rma_list.lock();
		obtained_reconf_lock = true;
	}
	replica_int::rma_reconf_ptr		p;

#if defined(FAULT_HA_FRMWK)
	int fault_count = 0;
#endif

	for (rma_list.atfirst(); (p = rma_list.current()) != NULL; ) {
		Environment e;

#if defined(FAULT_HA_FRMWK)
		void *fault_argp;
		uint32_t fault_argsize;

		if (fault_triggered(
		    FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER,
		    &fault_argp,
		    &fault_argsize)) {
			uint_t fnum = FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER;

			FaultFunctions::ha_rm_reboot_arg_t *arg =
			    (FaultFunctions::ha_rm_reboot_arg_t *) fault_argp;

			if (arg->nodeid == orb_conf::node_number() &&
			    (os::strcmp(fn_name, arg->fn_name) == 0) &&
			    (os::strcmp(get_name(), "ha_sample_server") == 0)) {

				fault_count++;
				if (fault_count > 1) {
					FaultFunctions::wait_for_arg_t
					    wait_arg;
					wait_arg.op = arg->op;
					wait_arg.nodeid = arg->nodeid;

					FaultFunctions::reboot(fnum, &wait_arg,
					    (uint32_t)sizeof (wait_arg));
				}
			}
		}
#endif

		p->add_service(get_number(), get_name(), e);
		if (e.exception()) {
			RECONF_DBG(("rm_rs: got exception on "
			    "rma_reconf `%p' for service `%s'\n", p,
			    get_name()));
			rma_reconf_exception(p, e);
		} else {
			rma_list.advance();
		}
	}
	if (obtained_reconf_lock) {
		rma_list.unlock();
	}

	rm_state_machine::state_mutex_lock();
	RECONF_DBG(("rm_rs %s: returned from %s\n", id(), fn_name));
}

//
// rma_reconf_exception() -- determine whether the exception returned
// from an rma_reconf invocation was due to a COMM FAILURE:  if
// so, erase from our list and release the object.
//
// XXX Should probably be moved to a different file.
void
rm_repl_service::rma_reconf_exception(
    const replica_int::rma_reconf_ptr p,
    Environment &e)
{
	ASSERT(e.exception());

	RM_DBG(("rm_rs %s: rma_reconf_exception\n", id()));
	if (CORBA::COMM_FAILURE::_exnarrow(e.exception()) != NULL) {
		//
		// RMA down? We don't care about reconf objects,
		// so just remove it from our list and release.
		//
		repl_mgr_impl::rcf_list	&rma_list
			= repl_mgr_impl::get_rcf_list();
		CL_PANIC(rma_list.lock_held());
		rma_list.erase(p);
		CORBA::release(p);

		RM_DBG(("rm_rs %s: "
		    "rma_reconf_exception, released ref `%p'\n",
		    id(), p));
	} else {
		//
		// SCMSGS
		// @explanation
		// An unrecoverable failure occurred in the HA framework.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: repl_mgr: exception while invoking RMA reconf object");
	}
	e.clear();
}

rm_repl_service::rm_repl_service(const char *nm,
    const replica::service_num_t num,
    const uint32_t new_desired_numsecondaries,
    uint_t initial_state, const replica::service_state sstate) :
	count(0), name(init_str(nm)), number(num), state_count(0),
	flags(NEED_THREAD), shutdown_primary(NULL),
	desired_numsecondaries(new_desired_numsecondaries),
	doing_switchover(false), switchover_info(NULL)
{
	//
	// A registering service may need to come up in frozen state
	// in two cases. One is where the previous incarnation of the
	// service went down while being frozen. The second is the
	// case where a service it depends upon is frozen for upgrade.
	//
	if ((sstate == replica::S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ) ||
	    (sstate == replica::S_FROZEN_FOR_UPGRADE_BY_REQ)) {

		flags |= FROZEN_FOR_UPGRADE_BY_REQ | FREEZE_FOR_UPGRADE;

	} else if (sstate == replica::S_DOWN_FROZEN_FOR_UPGRADE ||
	    sstate == replica::S_FROZEN_FOR_UPGRADE) {

		flags |= FREEZE_FOR_UPGRADE;
	}

	smp = new rm_state_machine(this,
	    (rm_state_machine::sm_service_state)initial_state);

#ifdef RM_DBGBUF
	debug_id = new char [os::strlen(nm) + 10];
	os::sprintf(debug_id, "\'%s(%d)\'", nm, num);
#endif
	RM_DBG(("%p rm_rs created\n", this));

	//
	// Note: we do not need to do shift_secs() as we did for
	// set_desired_numsecondaries() because we don't have any
	// providers yet.
	//
}

const replica::service_state
rm_repl_service::get_state() const
{
	//
	// Note: S_DOWN_FROZEN_FOR_UPGRADE and
	// S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ are used only to return
	// them to a restarting service. They do not participate in
	// any decision making states.
	//
	replica::service_state state;

	CL_PANIC(rm_state_machine::state_lock_held());

	switch (smp->get_state()) {
	case rm_state_machine::SERVICE_UNINIT:
		state = replica::S_UNINIT;
		break;
	case rm_state_machine::SERVICE_UP:
		state = replica::S_UP;
		break;
	case rm_state_machine::SERVICE_DOWN:
		state = replica::S_DOWN;
		break;
	case rm_state_machine::EXIT_STATE:
		state = replica::S_DOWN;
		break;
	case rm_state_machine::FROZEN_FOR_UPGRADE:
		state = (frozen_for_upgrade_by_req()) ?
		    replica::S_FROZEN_FOR_UPGRADE_BY_REQ :
		    replica::S_FROZEN_FOR_UPGRADE;
		break;
	default:
		CL_PANIC(!smp->is_stable());
		state = replica::S_UNSTABLE;
		break;
	}
	return (state);
}

// Returns true if the service is stable.
bool
rm_repl_service::is_stable()
{
	return (((flags & SHUTTING_DOWN) == 0) && smp->is_stable());
}

// Wait for the service to be stable.
void
rm_repl_service::wait_for_stable()
{
	incr_service_refs();
	while (!is_stable()) {
		rm_state_machine::wait_for_external_state_change();
	}
	decr_service_refs();
}

// Wait for the service to be down.
void
rm_repl_service::wait_for_down()
{
	while (!(is_stable() && is_down())) {
		rm_state_machine::wait_for_external_state_change();
	}
}

// Return the dependency tree containing this service.
repl_service_list *
rm_repl_service::get_component_tree()
{
	repl_service_list *tree = new repl_service_list;

	tree->append(this);
	SList<rm_repl_service>::ListIterator iter(tree);
	rm_repl_service *comp;
	while ((comp = iter.get_current()) != NULL) {
		rm_repl_service *dep;
		SList<rm_repl_service>::ListIterator dep_up(comp->deps_up);
		while ((dep = dep_up.get_current()) != NULL) {
			if (!tree->exists(dep)) {
				tree->append(dep);
			}
			dep_up.advance();
		}

		SList<rm_repl_service>::ListIterator dep_down(comp->deps_down);
		while ((dep = dep_down.get_current()) != NULL) {
			if (!tree->exists(dep)) {
				tree->append(dep);
			}
			dep_down.advance();
		}

		iter.advance();
	}

	return (tree);
}

// Lock all the services in the dependency tree. This is used to serialize
// service_admin requests for all services in the tree. Returns the dependency
// tree for future unlock.
// XXX This serialization is overly strict.  We should eventually
// allow some operations to be processed concurrently. For example,
// we could allow concurrent shutdown or repl_prov registration for services
// that don't depend on each other.
repl_service_list *
rm_repl_service::lock_tree()
{
	ASSERT(rm_state_machine::state_lock_held());

	rm_repl_service *s;
	repl_service_list *tree;

	// Wait for all services to be stable and unlocked.
	// Look up the dependency tree each time, since it might change.
	while (!can_lock_tree(tree = get_component_tree())) {
		delete tree;
		rm_state_machine::wait_for_external_state_change();
	}

	// Mark all services as locked.
	for (tree->atfirst(); (s = tree->get_current()) != NULL;
	    tree->advance()) {
		CL_PANIC((s->flags & SERVICE_EXCLUSIVE) == 0);
		s->flags |= SERVICE_EXCLUSIVE;
	}
	return (tree);
}

bool
rm_repl_service::can_lock_tree(repl_service_list *tree)
{
	bool can_lock = true;
	rm_repl_service *s;

	// Check if all services are stable and unlocked.
	for (tree->atfirst(); (s = tree->get_current()) != NULL;
	    tree->advance()) {
		if ((s->flags & SERVICE_EXCLUSIVE) != 0 || !s->is_stable()) {
			RM_DBG(("rm_rs %s: can_lock_tree() - failed on %s"
			    "(s->flags & SERVICE_EXCLUSIVE) = %x, "
			    "s->is_stable() = %d\n",
			    id(), s->id(),
			    (s->flags & SERVICE_EXCLUSIVE), s->is_stable()));
			can_lock = false;
			break;
		}
	}
	return (can_lock);
}

// Undo work done by lock_tree().
void
rm_repl_service::unlock_tree(repl_service_list *tree)
{
	ASSERT(rm_state_machine::state_lock_held());
	rm_repl_service *s;

	for (tree->atfirst(); (s = tree->get_current()) != NULL;
	    tree->advance()) {
		CL_PANIC((s->flags & SERVICE_EXCLUSIVE) != 0);
		s->flags &= ~SERVICE_EXCLUSIVE;
	}
	delete tree;
	rm_state_machine::signal_external_state_change();
}

// Wait for the dependency tree to be stable.
void
rm_repl_service::wait_for_tree_stable(repl_service_list *treep)
{
	while (!check_tree_stable(treep)) {
		rm_state_machine::wait_for_external_state_change();
	}
}

bool
rm_repl_service::check_tree_stable(repl_service_list *tree)
{
	rm_repl_service *s;

	ASSERT(rm_state_machine::state_lock_held());

	// Check if all services are stable.
	for (tree->atfirst(); (s = tree->get_current()) != NULL;
	    tree->advance()) {
		// We only do this to trees that we have locked.
		CL_PANIC((s->flags & SERVICE_EXCLUSIVE) != 0);
		if (!s->is_stable()) {
			return (false);
		}
	}
	return (true);
}

// Lock a service. This is used to serialize service_admin requests for a
// specific service in the tree.
void
rm_repl_service::lock_service()
{
	ASSERT(rm_state_machine::state_lock_held());

	// Wait for the service to be stable and unlocked.
	while ((flags & SERVICE_EXCLUSIVE) != 0 || !is_stable()) {
		rm_state_machine::wait_for_external_state_change();
	}

	flags |= SERVICE_EXCLUSIVE;
}

// Unlock a service. This is used to wake up threads blocked in lock_service()
// which serializes service_admin requests for a specific service in the tree.
void
rm_repl_service::unlock_service()
{
	CL_PANIC((flags & SERVICE_EXCLUSIVE) != 0);

	flags &= ~SERVICE_EXCLUSIVE;
	rm_state_machine::signal_external_state_change();
}

//
// Called during become_spare() on RM.
// Don't bother with reference counts.
// Today we don't call become_spare() on an RM, unless the
// RMM detects an incomplete dump from the primary, or we're
// rebooting the node (in which case we don't have service
// state).
//
void
rm_repl_service::become_spare_delete_state()
{
	prov_reg_impl	*rp = NULL;
	while ((rp = repl_prov_list.reapfirst()) != NULL) {
		RM_DBG(("rm_ri: "
		    "become_spare(), deleting reg_impl pointer `%p'\n",
		    rp));
		rp->clear_deps_down();
		rp->clear_deps_up();
		delete rp;
	}

	// Empty the service deps_up and deps_down lists.
	deps_up.erase_list();
	deps_down.erase_list();

	// so that rm_repl_service destructor won't assert
	count = 0;
}

void
rm_repl_service::mark_shutdown_succeeded()
{
	flags &= ~SHUTDOWN_INTENTION;
	shutdown_primary = NULL;
	mark_deleted();
	remove_dependencies();
}

void
rm_repl_service::mark_shutdown_committed()
{
	CL_PANIC(is_deleted());
	flags |= SHUTDOWN_COMMITTED;
	if (can_exit_state_machine()) {
		repl_mgr_impl::get_rm()->erase_service(this);
	}
}

void
rm_repl_service::mark_shutdown_failed()
{
	flags &= ~SHUTDOWN_INTENTION;
	shutdown_primary = NULL;
}

//
// set_desired_numsecondaries() -- set the desired number of secondaries
// This can be invoked on RM primary as well as RM secondaries.
//
void
rm_repl_service::set_desired_numsecondaries(
    uint32_t new_desired_numsecondaries)
{
	CL_PANIC(new_desired_numsecondaries <= MAX_SECONDARIES);

	if (desired_numsecondaries == new_desired_numsecondaries) {
		return;
	}

	desired_numsecondaries = new_desired_numsecondaries;
	RM_DBG(("rm_rs %s desired_numsecondaries changed to: %d\n",
	    id(), desired_numsecondaries));

	if (rm_is_primary()) {
		//
		// This is invoked on the RM primary, so we need to notify the
		// state machine about the changes.
		//
		smp->signal_state_machine();

		if (deps_up.empty()) {
			//
			// This is the top service in a dependency tree or a
			// single level service with no dependency.
			//
			wait_for_stable();
		} else {
			//
			// This service has dependent services on top of it, we
			// need to wait for the whole tree to become stable.
			//
			// Changing desired number of secondaries of a lower
			// level service may affect the actual number of
			// secondaries of its dependent services. So we need to
			// wait for the whole tree to become stable.
			//
			repl_service_list *tree = get_component_tree();

			wait_for_tree_stable(tree);

			delete tree;
		}
	}
}

//
// get_repl_prov() -- return pointer to registration object
// corresponding to the specified rma_repl_prov_ptr.
//
const rm_repl_service::prov_reg_impl *	// lint
rm_repl_service::get_repl_prov(
    const replica_int::rma_repl_prov_ptr repl_prov_arg)
{
	prov_reg_impl		*p = NULL;

	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; ((p = rp_iter.get_current()) != NULL); rp_iter.advance()) {
		if (repl_prov_arg->_equiv(p->get_rma_repl_prov_ptr())) {
			break;
		}
	} // for

	return (p);
}

//
// get_repl_prov() -- return pointer to registration object
// corresponding to the specified provider name.
// This is only used by the service_admin code. We only return a value if
// the provider is alive. If it is not alive, then we are going to delete it
// soon.
//
rm_repl_service::prov_reg_impl *
rm_repl_service::get_repl_prov(const char *prov_desc)
{
	if (prov_desc == NULL) {
		return (NULL);
	}

	prov_reg_impl	*p = NULL;

	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; ((p = rp_iter.get_current()) != NULL); rp_iter.advance()) {
		if (os::strcmp(prov_desc, p->get_rp_desc()) == 0 &&
		    !p->is_down()) {
			// Note that we continue if the provider is down
			// because there may be multiple registrations with
			// the same name (the second registration would only
			// be allowed if the first was dead).
			break;
		}
	} // for

	return (p);
}

//
// max_secondaries_active() -- determine if we have the maximum number
// of active secondaries as specified by the desired number of secondaries.
//
bool
rm_repl_service::max_secondaries_active()
{
	prov_reg_impl	*p = NULL;
	uint32_t	sec_count = 0;

	if (desired_numsecondaries == 0) {
		return (true);
	}

	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; (p = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		if (p->is_valid_sec()) {
			if (++sec_count == desired_numsecondaries) {
				return (true);
			}
		}
	} // for

	return (false);
}

// called from add_secondary() code
void
rm_repl_service::add_sec_rma_repl_prov_ckpts(
    repl_rm::rm_ckpt_ptr ckpt, Environment &e)
{
	prov_reg_impl	*p = NULL;

	RM_DBG(("rm_rs %s starting add_sec_rma_repl_prov_ckpts(), "
	    "%d provs to checkpoint\n", id(), repl_prov_list.count()));

	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; ((p = rp_iter.get_current()) != NULL); rp_iter.advance()) {
		replica::repl_prov_reg_var prov_ref = p->get_new_reference();

		// XXX this code was pulled from
		// service_admin::get_repl_provs().
		// should be made into a common routine.
		replica::prov_dependencies this_depends_on;
		prov_reg_impl *prov;
		unsigned int length = 0;
		SList<prov_reg_impl>::ListIterator deps_down_iter(p->deps_down);
		for (; (prov = deps_down_iter.get_current()) != NULL;
		    deps_down_iter.advance()) {
			this_depends_on.length(length+1);
			this_depends_on[length].service =
				prov->get_service()->get_name();
			this_depends_on[length].repl_prov_desc =
				prov->get_rp_desc();
			length++;
		}

		RM_DBG(("rm_rs:  checkpointing reg_impl %s %p\n",
		    p->get_rp_desc(), p));
		ckpt->ckpt_reg_rma_repl_prov(get_number(),
		    p->get_rma_repl_prov_ptr(), p->get_rp_desc(),
		    this_depends_on,
		    p->get_sec_chkpt(), p->get_priority(),
		    prov_ref, e);
		if (e.exception()) {
			RM_DBG(("rm_ri:  add_sec_rma_repl_prov_ckpts(), "
			    "got exception checkpointing reg_impl %p\n", p));
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				e.clear();
			} else {
				CL_PANIC(0);
			}
		}
	} // for
	RM_DBG(("rm_rs:  exiting add_sec_rma_repl_prov_ckpts()\n"));
}

//
// create new prov_reg_impl object on PRIMARY and link it
//
rm_repl_service::prov_reg_impl *
rm_repl_service::add_repl_prov(
    replica_int::rma_repl_prov_ptr rma_repl_prov,
    const char *prov_desc,
    const replica::checkpoint_ptr sec_chkpt,
    uint_t priority,
    replica::repl_prov_state init_s,
    SList<prov_reg_impl> *deps)
{
	CL_PANIC(rm_is_primary());

	prov_reg_impl *new_rp = new prov_reg_impl(
	    repl_mgr_impl::get_rm()->get_rm_prov(), rma_repl_prov, prov_desc,
	    sec_chkpt, priority, init_s, this);
	ASSERT(new_rp);

	// Set up the per-repl_prov dependency linkage.
	prov_reg_impl *p;
	for (deps->atfirst(); (p = deps->get_current()) != NULL;
	    deps->advance()) {
		p->deps_up.append(new_rp);
		new_rp->deps_down.append(p);
	}

	incr_service_refs();
	repl_prov_list.append(new_rp);
	return (new_rp);
}

//
// create new prov_reg_impl object on SECONDARY and link it
// (called from checkpoint code)
//
rm_repl_service::prov_reg_impl *
rm_repl_service::create_rma_repl_prov_on_sec(
    replica_int::rma_repl_prov_ptr prov, const char *prov_desc,
    const replica::prov_dependencies &this_depends_on,
    replica::checkpoint_ptr sec_chkpt,
    uint_t priority,
    replica::repl_prov_reg_ptr new_reg_obj)
{
	ASSERT(rm_state_machine::state_lock_held());

	RM_DBG(("rm_rs %s: secondary processing ckpt "
	    "to create new reg_impl `%s', "
	    "rma_repl_prov `%p'\n",
	    id(), prov_desc, prov));

	SList<prov_reg_impl> *deps = NULL;
	Environment	e;

	if (!is_deleted()) {
		//
		// Check dependencies and generate the deps list.
		//
		// If this service has already been marked as deleted, no need
		// to get the dependencies as we are going to remove all
		// dependencies in shutdown_providers() anyway.
		//
		deps = repl_prov_dependencies(this_depends_on, e);
	}

	// XXX this probably isn't correct, because secondary
	// could have just gotten unref on something this prov
	// depends on.  But what should we do instead:  just not
	// create the object on the secondary?
	// We're relying on the fact that, in the add_secondary()
	// case, the primary is dumping objects in the order they
	// were created; otherwise we'd get the exception often.
	CL_PANIC(!e.exception());

	CL_PANIC(get_repl_prov(prov) == NULL);

	prov_reg_impl *new_rp = new prov_reg_impl(new_reg_obj, prov, prov_desc,
	    sec_chkpt, priority, replica::AS_SPARE, this);
	ASSERT(new_rp);

	RM_DBG(("rm_rs %s: created new reg_impl on "
	    "secondary RM, rma_repl_prov `%p'\n", id(), prov));

	if (!is_deleted()) {
		//
		// Set up the per-repl_prov dependency linkage.
		//
		// No need to do this for a service that is already marked
		// as deleted since we are going to remove the dependencies
		// in shutdown_providers() anyway.
		//
		prov_reg_impl *p;

		ASSERT(deps);
		for (deps->atfirst(); (p = deps->get_current()) != NULL;
		    deps->advance()) {
			p->deps_up.append(new_rp);
			new_rp->deps_down.append(p);
		}

		// Delete the list returned by repl_prov_dependencies()
		deps->erase_list();
		delete deps;
	}

	incr_service_refs();
	repl_prov_list.append(new_rp);

	return (new_rp);
}

//
// This function does the dependency work for register_rma_repl_prov.
// It generates a list of repl_prov dependencies from the dependency
// strings, and it checks that the dependencies are currently satisfied.
// If the dependencies are not satisfied, it returns an exception
// The caller of this method is responsible for freeing memory used by the
// list returned. The caller must erase and delete the list but not elements
// in the list.
//
SList<rm_repl_service::prov_reg_impl> *
rm_repl_service::repl_prov_dependencies(
    const replica::prov_dependencies &this_depends_on,
    Environment &_env)
{
	SList<prov_reg_impl> *deps
	    = new SList<prov_reg_impl>;
	repl_mgr_impl *rmp = repl_mgr_impl::get_rm();

	ASSERT(rm_state_machine::state_lock_held());

	// Convert the dependency strings into pointers for later use.
	for (unsigned int i = 0; i < this_depends_on.length(); i++) {
		rm_repl_service *serv = rmp->get_service(
			this_depends_on[i].service);
		if (serv == NULL) {
			// XXX This should be a different exception, since
			// the service should exist or the register_service
			// would have failed.
			RM_DBG(("rm_rs %s: Service dependency not "
			    "satisfied for %s\n",
			    id(), (const char *)this_depends_on[i].service));
			_env.exception(new replica::dependency_not_satisfied);
			break;
		}
		prov_reg_impl *rp = serv->get_repl_prov(
			this_depends_on[i].repl_prov_desc);
		if (rp == NULL) {
			RM_DBG(("rm_rs %s: Prov dependency not "
			    "satisfied for %s %s\n",
			    id(), (const char *)this_depends_on[i].service,
			    (const char *)this_depends_on[i].repl_prov_desc));
			_env.exception(new replica::dependency_not_satisfied);
			break;
		}
		deps->append(rp);

		if (rm_is_primary() &&
		    ((serv->get_state() == replica::S_DOWN) ||
		    (rp->get_repl_prov_state() == replica::AS_DOWN) ||
		    (rp->get_sm_prov_state() & rm_repl_service::
		    prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN) != 0)) {
			RM_DBG(("rm_rs %s: Dependency not "
			    "satisfied on primary because of service "
			    "or prov dep state\n",
			    id()));
			_env.exception(new replica::dependency_not_satisfied);
			break;
		}
	}

	if (_env.exception() != NULL) {
		deps->erase_list();
		delete deps;
		deps = NULL;
	}
	return (deps);
}

//
// register_rma_repl_prov()
//
// Returns pointer to newly allocated prov_reg_impl obj.
//
rm_repl_service::prov_reg_impl *
rm_repl_service::register_rma_repl_prov(
    replica_int::rma_repl_prov_ptr	rma_repl_prov_ptr,
    const char				*prov_desc,
    const replica::checkpoint_ptr	sec_chkpt,
    uint_t priority,
    service_admin_impl *sap,
    const replica::prov_dependencies	&this_depends_on,
    repl_rm::rm_ckpt_ptr		rs_ckpt,
    Environment				&_env)
{
	ASSERT(rm_state_machine::state_lock_held());

	SList<prov_reg_impl> *deps;

	// Check dependencies and generate the deps list.
	deps = repl_prov_dependencies(this_depends_on, _env);

	if (_env.exception() != NULL) {
		return (NULL);
	}

#ifdef DEBUG
	// The client checks that the provider dependency is a subset
	// of the service dependency. Assert that here.
	prov_reg_impl *dep_rp;
	SList<prov_reg_impl>::ListIterator rp_iter(deps);
	while ((dep_rp = rp_iter.get_current()) != NULL) {
		rp_iter.advance();
		bool found = false;
		rm_repl_service *dep_s;
		SList<rm_repl_service>::ListIterator s_iter(deps_down);
		while (!found && (dep_s = s_iter.get_current()) != NULL) {
			s_iter.advance();
			if (dep_s == dep_rp->get_service()) {
				found = true;
			}
		}
		if (!found) {
			RM_DBG(("rm_rs %s %s: bad dependency on %s %s\n",
			    id(), prov_desc, dep_rp->get_service()->id(),
			    dep_rp->get_rp_desc()));
			CL_PANIC(0);
		}
	}
#endif // DEBUG

	CL_PANIC(get_repl_prov(prov_desc) == NULL);
	CL_PANIC(get_repl_prov(rma_repl_prov_ptr) == NULL);

	RM_DBG(("rm_rs %s: got request to register rma_repl_prov `%p'\n",
	    id(), rma_repl_prov_ptr));

	// new_rp_state is the initial repl_prov_state of
	// rma_repl_prov_ptr: spare
	// XXX Do we need this. Can't we just initialize to spare in the
	// constructor?
	replica::repl_prov_state new_rp_state = replica::AS_SPARE;

	// allocate the reg_impl object;
	RM_DBG(("rm_rs %s: register_repl_prov adding `%p'\n",
	    id(), rma_repl_prov_ptr));

	prov_reg_impl *new_rp = add_repl_prov(rma_repl_prov_ptr,
	    prov_desc, sec_chkpt, priority, new_rp_state, deps);
	ASSERT(new_rp);

	replica::repl_prov_reg_var prov_ref = new_rp->get_new_reference();

	// FAULT POINT
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_1,
		FaultFunctions::generic);

	// checkpoint the object creation
	rs_ckpt->ckpt_reg_rma_repl_prov(get_number(),
	    rma_repl_prov_ptr, new_rp->get_rp_desc(), this_depends_on,
	    sec_chkpt, priority, prov_ref, _env);

	// FAULT POINT
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_2,
		FaultFunctions::generic);

	deps->erase_list();
	delete deps;

	incr_state_count(sap);

	new_rp->set_sm_prov_state(prov_reg_impl::SM_SPARE_TO_ADD);
	wait_for_stable();

	return (new_rp);
}

void
rm_repl_service::add_dep_down(rm_repl_service *dep)
{
	deps_down.append(dep);
	dep->deps_up.append(this);
}

//
// get_root_obj() -- get the "root object" from the current
// primary, if there is one.
// Note that the service is guaranteed to be "up" and have
// a primary that we haven't yet gotten a COMM_FAILURE on when
// this method is called.
//
CORBA::Object_ptr
rm_repl_service::get_root_obj(Environment &e)
{
	CL_PANIC(!(e.exception()));

	CORBA::Object_ptr root_obj = nil;
	bool retry_needed = true;

	while (retry_needed && !e.exception()) {
		retry_needed = false;
		//
		// If the service is frozen for upgrade, it will still
		// have a primary.
		//
		replica::service_state sstate = get_state();

		RM_DBG(("rm_rs %s: get_root_obj state = %d\n", id(), sstate));
		CL_PANIC(is_up() ||
		    (sstate == replica::S_FROZEN_FOR_UPGRADE) ||
		    (sstate == replica::S_FROZEN_FOR_UPGRADE_BY_REQ));

		prov_reg_impl *primaryp = smp->get_primary();
		//
		// Place a hold on the current primary by getting a new
		// reference. This guarantees that it won't receive
		// _unreferenced() while the get_root_obj() invocation is in
		// progress. Also, the fact that we have the service locked
		// prevents any other operations from changing the primary, so
		// we are guaranteed that it will stay the same.
		//
		replica::repl_prov_reg_ptr primary_ref;
		primary_ref = primaryp->get_new_reference();
		rm_state_machine::state_mutex_unlock();
		Environment local_env;
		root_obj = primaryp->get_rma_repl_prov_ptr()->
		    get_root_obj(local_env);
		rm_state_machine::state_mutex_lock();

		// Release the hold.
		CORBA::release(primary_ref);
		if (local_env.exception()) {
			if (CORBA::COMM_FAILURE::_exnarrow(
			    local_env.exception())) {
				// The primary died, need to retry.
				retry_needed = true;

				//
				// Tell the state machine that there's a
				// problem. This means that we don't have to
				// wait for an unreferenced to come in.
				//
				primaryp->set_sm_prov_state(
					prov_reg_impl::SM_FAILED);
				// Wait for reconfiguration to complete.
				reconf_complete_wait(e);
				CL_PANIC(!e.exception() ||
				    replica::failed_service::_exnarrow(
				    e.exception()));
			} else {
				CL_PANIC(0);
			}
		}
	}

	RM_DBG(("rm_rs %s: returning root obj `%p'\n", id(), root_obj));

	return (root_obj);
}

//
// num_valid_secs - returns number of secondaries ready to become primary.
//
uint_t
rm_repl_service::num_valid_secs()
{
	uint_t				num_secs = 0;
	rm_repl_service::prov_reg_impl	*prov_regp;
	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; (prov_regp = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		//
		// Note that we must check for SM_FAILED_PRIMARY here, because
		// only care about things that can be made primary. We
		// can't move that check into is_valid_sec() because that is
		// also used in the algorithm to select secondaries.
		//
		// XXX There should be a clearer distinction.
		//
		if (prov_regp->is_valid_sec() &&
		    ((prov_regp->get_sm_prov_state() &
		    prov_reg_impl::SM_FAILED_PRIMARY) == 0)) {
			num_secs++;
		}
	}
	return (num_secs);
}

// Return the n'th secondary which is ready to become primary.
rm_repl_service::prov_reg_impl *
rm_repl_service::get_indexed_sec(uint_t n)
{
	uint_t num_secs = 0;
	rm_repl_service::prov_reg_impl *p;

	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; (p = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		//
		// Note that we must check for SM_FAILED_PRIMARY here because
		// it we only care about things that can be made primary. We
		// can't move that check into is_valid_sec() because that is
		// also used in the algorithm to select secondaries.
		// XXX There should be a clearer distinction.
		//
		if (p->is_valid_sec() &&
		    ((p->get_sm_prov_state() &
		    prov_reg_impl::SM_FAILED_PRIMARY) == 0)) {
			if (num_secs++ == n) {
				break;
			}
		}
	}
	return (p);
}

//
// trigger_switchover - tell all dependent primaries to become secondaries.
//
// The flag doing_switchover will be set to all services in the dependency tree
// but the switchover_info will only be created in the bottom level service.
// since that is the service the change_repl_prov_status is called on. In
// case of service recovery, the bottom level service will propagate the
// information to the entire tree.
//
void
rm_repl_service::trigger_switchover(const char *primary_from,
    const char *primary_to)
{
	ASSERT(rm_state_machine::state_lock_held());

	RM_DBG(("rm_rs %s: in trigger_switchover.\n", id()));

	//
	// Store switchover_from_primary and switchover_to_primary in
	// the bottom service
	//
	switchover_info = new switchover_info_t(primary_from, primary_to);

	//
	// Set doing_switchover flag for all services in the dependency
	// tree. This tells the associated rm_state_machine to hold off
	// removal of secondaries until the switchover is completely over.
	//
	repl_service_list	*treep = get_component_tree();
	rm_repl_service	*repl_servicep = NULL;
	for (treep->atfirst();
	    (repl_servicep = treep->get_current()) != NULL;
	    treep->advance()) {
		repl_servicep->set_doing_switchover(true);
	}
	if (!rm_is_primary()) {
		//
		// This is executed on RM secondaries as part of
		// ckpt_switchover_service(). There is no need to
		// do the following work.
		//
		delete treep;
		return;
	}
	RM_DBG(("rm_rs %s: posting switch req to tree\n", id()));
	//
	// All services in the dependency tree must participate.
	//

	if (primary_to != NULL) {
		//
		// If the user has specified which provider to become the
		// new primary, we need to make sure the new primary candidate
		// and all its dependent providers are valid secondaries, Else,
		// we need to convert them to secondaries.
		//
		prov_reg_impl	*provp = get_repl_prov(primary_to);
		prov_reg_list *prov_deps_up_list =
				    provp->get_deps_up_tree();
		prov_deps_up_list->prepend(provp);

		for (prov_deps_up_list->atfirst();
		    (provp = prov_deps_up_list->get_current()) != NULL;
		    prov_deps_up_list->advance()) {
			CL_PANIC(!provp->is_primary());
			if (provp->is_spare()) {
				//
				// This provider is currently a spare
				// we need to convert it to a secondary.
				//
				provp->set_sm_prov_state(
				    prov_reg_impl::SM_SPARE_TO_MAKE_SEC);
			}
		}

		delete prov_deps_up_list;
		// Wait until all the conversions have completed
		wait_for_tree_stable(treep);
	}

	//
	// Now, we can safely trigger the switchover by setting flag
	// SM_PRIMARY_TO_MAKE_SECONDARY on all primaries in the dependency
	// tree.
	//

	for (treep->atfirst();
	    (repl_servicep = treep->get_current()) != NULL;
	    treep->advance()) {
		RM_DBG(("rm_rs %s: posting switch req to %s\n",
		    id(), repl_servicep->id()));

		if (repl_servicep->smp->get_primary() != NULL) {
			repl_servicep->smp->get_primary()->set_sm_prov_state(
			    prov_reg_impl::SM_PRIMARY_TO_MAKE_SECONDARY);
		}
	}
	delete treep;
}

//
// switchover_complete
//
// Tells all of the RM secondaries that the switchover has been completed
// and that they can reset the doing_switchover flag to false for all of the
// services in the given tree.
// Returns true on success, false on failure.
//
void
rm_repl_service::switchover_complete()
{
	ASSERT(rm_state_machine::state_lock_held());

	RM_DBG(("rm_rs %s: in switchover_complete.\n", id()));

	bool	rm_primary = rm_is_primary();

	//
	// Reset doing_switchover to false in all services in the dependency
	// tree.
	//
	rm_repl_service		*repl_servicep = NULL;
	repl_service_list	*treep = get_component_tree();
	for (treep->atfirst();
	    (repl_servicep = treep->get_current()) != NULL;
	    treep->advance()) {
		repl_servicep->set_doing_switchover(false);
		if (rm_primary) {
			//
			// At the beginning of switchover, we set flag
			// doing_switchover to true which tells the rm
			// state machine to hold off removal of secondaries.
			// This is necessary because in case we are switching
			// over to a spare provider, we have to convert it
			// to a secondary first and we don't want it to be
			// converted back to a spare before the actual
			// switchover takes place.
			//
			// Now we have finished switchover and we may end up
			// having one extra secondary if we were switching over
			// to a spare. So after resetting flag doing_switchover
			// to false, we need to inform the state machine about
			// the change and have it remove the extra secondary if
			// there is one.
			//
			repl_servicep->get_state_machine()->
			    signal_state_machine();
		}
	}

	if (rm_primary) {
		wait_for_tree_stable(treep);
		//
		// After a switchover operation, we need to check the
		// secondaries distribution in the dependency tree and shift
		// them if needed.
		//
		// Usually, a primary provider has the highest priority among
		// the providers in a service, however, its priority may
		// become lower than the priorities of other providers if
		// a system administrator changes it on the fly. In that case,
		// the primary provider will remain intact as moving primary
		// around is an expensive operation that should be avoided
		// whenever possible.
		//
		// When a primary is switched away to another provider, by
		// default, the old primary will be converted to a secondary.
		// This may cause an improper secondaries distribution if the
		// old primary has a lower priority as described above.
		// Therefore, we need to perform some check after a switchover
		// operation and re-distribute secondaries if necessary.
		shift_secs();
	}

	delete switchover_info;
	switchover_info = NULL;

	delete treep;
}

//
// switchover_dependency_check - returns true when dependencies
// allow a switchover.
//
// Currently, just check to see if all of the dependent primaries have
// secondaries. Cannot switchover when there is nothing to switch to.
//
// XXX - more sophisticated checks are possible. This seems an improvement.
//
bool
rm_repl_service::switchover_dependency_check(repl_service_list *treep)
{
	//
	// All services in the dependency tree must participate in switchover.
	//
	rm_repl_service		*repl_servicep;
	for (treep->atfirst();
	    (repl_servicep = treep->get_current()) != NULL;
	    treep->advance()) {

		rm_repl_service::prov_reg_impl	*dep_primaryp =
		    repl_servicep->smp->get_primary();

		if (dep_primaryp != NULL &&
		    dep_primaryp->get_service()->num_valid_secs() == 0) {
			//
			// This dependent primary has no secondaries.
			// Cannot switchover.
			//
			return (false);
		}
	}
	return (true);
}

//
// change_repl_prov_status - decides whether a status change is allowed.
// If the status change is possible, this method initiates the change.
//
// The status to change to are (change_ops):
//	SC_SET_PRIMARY
//	SC_SET_SECONDARY
//	SC_SET_SPARE
//	SC_REMOVE_REPL_PROV
//
void
rm_repl_service::change_repl_prov_status(
    prov_reg_impl		*prov_regp,
    const replica::change_op	op,
    repl_service_list		*treep,
    repl_rm::rm_ckpt_ptr	rs_ckpt,
    Environment			&e)
{
	ASSERT(rm_state_machine::state_lock_held());

	uint_t	goal = 0;

	replica::repl_prov_state	cur_state =
	    prov_regp->get_repl_prov_state();

	//
	// Switch based on the desired final status
	//
	switch (op) {
	case replica::SC_SET_PRIMARY:
		// We only allow switchovers from bottom most service.
		if (!prov_regp->deps_down.empty()) {
			e.exception(new replica::dependency_not_satisfied());
			RM_DBG(("rm_rs %s: switchover:not bottom of "
			    "tree\n", id()));
			break;
		}
		switch (cur_state) {
		case replica::AS_SPARE:
			//
			// The same as switchover to a secondary except that
			// there is an extra step converting this provider and
			// all its dependent providers to secondaries in
			// trigger_switchover() below.
			//
		case replica::AS_SECONDARY:
			//
			// Only allow switchover if all dependent providers
			// exist. Also, if the destination provider or any of
			// its dependent provider is a spare, convert it to
			// a secondary first.
			//
			if (!switchover_dependency_check(treep)) {
				e.exception(new replica::depends_on);
				RM_DBG(("rm_rs %s: "
				    "switchover:dependent primary "
				    "cannot switchover\n",
				    id()));
				break;
			}
			goal = prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY;
			trigger_switchover(smp->get_primary()->get_rp_desc(),
			    prov_regp->get_rp_desc());
			//
			// propagate the trigger_switchover to the RM
			// secondaries.
			//
			ASSERT(switchover_info != NULL);
			rs_ckpt->ckpt_switchover_service(
			    switchover_info->get_switchover_from(),
			    switchover_info->get_switchover_to(),
			    get_number(), e);
			CL_PANIC(!e.exception());
			break;

		case replica::AS_PRIMARY:
			// Already primary. Do nothing.
			break;

		case replica::AS_DOWN:
			e.exception(new replica::invalid_repl_prov_state(
				replica::AS_DOWN));
			break;
		}
		break;

	case replica::SC_SET_SECONDARY:
		switch (cur_state) {
		case replica::AS_SPARE:
			goal = prov_reg_impl::SM_SPARE_TO_MAKE_SEC;
			break;

		case replica::AS_SECONDARY:
			// Already secondary. Do nothing.
			break;

		case replica::AS_PRIMARY:
			//
			// We only allow switchovers from bottom most service.
			//
			if (!prov_regp->deps_down.empty()) {
				e.exception(new
				    replica::dependency_not_satisfied());
				RM_DBG(("rm_rs %s: "
				    "switchover:not bottom of tree\n", id()));
				break;
			}
			//
			// Only allow primary to become secondary if there
			// already is at least one secondary that can become
			// a primary
			//
			if (num_valid_secs() == 0) {
				e.exception(new replica::depends_on);
				RM_DBG(("rm_rs %s: "
				    "switchover:no secondary to takeover\n",
				    id()));
				break;
			}
			//
			// Only allow primary to become secondary if all
			// dependent primaries have at least one secondary.
			//
			if (!switchover_dependency_check(treep)) {
				e.exception(new replica::depends_on);
				RM_DBG(("rm_rs %s: "
				    "switchover:dependent primary "
				    "cannot switchover\n",
				    id()));
				break;
			}
			//
			// Tell everyone in the tree to become a secondary.
			// The state machine will pick a primary, possible
			// the same one.
			//
			goal = prov_reg_impl::SM_PRIMARY_TO_MAKE_SECONDARY;
			trigger_switchover(smp->get_primary()->get_rp_desc(),
			    NULL);
			//
			// propagate the trigger_switchover to the RM
			// secondaries.
			//
			ASSERT(switchover_info != NULL);
			rs_ckpt->ckpt_switchover_service(
			    switchover_info->get_switchover_from(),
			    switchover_info->get_switchover_to(),
			    get_number(), e);
			CL_PANIC(!e.exception());
			break;

		case replica::AS_DOWN:
			e.exception(new replica::invalid_repl_prov_state(
			    replica::AS_DOWN));
			break;
		}
		break;

	case replica::SC_SET_SPARE:
		switch (cur_state) {
		case replica::AS_SPARE:
			// Already spare. Do nothing.
			break;

		case replica::AS_SECONDARY:
			//
			// A dependent secondary provider can't reside on a
			// spare provider. But we don't need to worry whether
			// or not this provider has any dependent secondaries
			// on top of it because once this provider gets
			// converted to spare, the service it belongs to will
			// deliver an event to all the dependent services,
			// telling them to convert the corresponding
			// secondaries to spares as well. Moreover, a
			// dependent service should not have control over
			// the service that is below it to make any changes.
			//
			goal = prov_reg_impl::SM_SEC_TO_BE_SPARED |
			    prov_reg_impl::SM_SEC_TO_BE_REMOVED;
			break;

		case replica::AS_PRIMARY: {
			e.exception(new replica::invalid_repl_prov_state(
			    replica::AS_PRIMARY));
			break;
		}

		case replica::AS_DOWN:
			e.exception(new replica::invalid_repl_prov_state(
			    replica::AS_DOWN));
			break;
		}
		break;

	case replica::SC_REMOVE_REPL_PROV:
		switch (cur_state) {
		case replica::AS_PRIMARY:
			e.exception(new replica::invalid_repl_prov_state(
				replica::AS_PRIMARY));
			break;

		case replica::AS_SECONDARY:
			//
			// Check for dependency violations.
			// XXX This is a policy issue.
			// We don't allow a secondary
			// to be removed if others spares depend on it. An
			// alternative would be to have this request force all
			// the dependant secondaries to be removed. That would
			// probably be more consistent.
			//
			if (!prov_regp->deps_up.empty()) {
				e.exception(new replica::depends_on);
				break;
			}
			if (!e.exception()) {
				//
				// Convert secondary to spare,
				// then shut it down.
				//
				goal = prov_reg_impl::SM_SEC_TO_BE_SPARED |
				    prov_reg_impl::SM_SEC_TO_BE_REMOVED |
				    prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN;
			}
			break;

		case replica::AS_SPARE:
			//
			// Check for dependency violations.
			// XXX This is a policy issue.
			// We don't allow a spare
			// to be removed if others spares depend on it. An
			// alternative would be to have this request force all
			// the dependant spare to be removed.
			//
			if (!prov_regp->deps_up.empty()) {
				e.exception(new replica::depends_on);
				break;
			}

			goal = prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN;
			break;

		case replica::AS_DOWN:
			e.exception(new replica::invalid_repl_prov_state(
				replica::AS_DOWN));
			break;
		}
		break;

	default:
		CL_PANIC(0);
		break;
	}

	if (e.exception() == NULL && goal != 0) {
		//
		// The system wants to make a change and can make that change
		//
		RM_DBG(("rm_rs %s: change_repl_prov_status goal = %x\n",
		    id(), goal));
		prov_regp->set_sm_prov_state(goal);

		//
		// Wait for the tree to stabilize. By waiting for the whole
		// tree, we make sure that requests to switchover a bottom
		// level service do not return until the switchovers of
		// dependant services complete.
		//
		wait_for_tree_stable(treep);

		//
		// Removal of a dependent provider will reduce the
		// the total number of services that its lower level provider
		// can bring up, therefore, changing its overall priority.
		// For this reason, after a removal of a dependent provider,
		// it is necessary to check and shift the secondary distribution
		// of the whole dependency tree.
		//
		if ((op == replica::SC_REMOVE_REPL_PROV) &&
		    !deps_down.empty()) {
			//
			// Removal of a dependent provider will change the
			// whole dependency tree's priority distribution.
			// So we need to shift the secondaries from the
			// bottom service.
			//
			shift_secs_from_bottom();
		}
	}
}

//
// Handles the request to shut the service down. We consult the primary about
// whether it's ok to be shutdown.
// Returns a Solaris error code.
//
uint32_t
rm_repl_service::shutdown_service(service_admin_impl *sap,
    trans_state_shutdown_service	*st, // primary state
    repl_rm::rm_ckpt_ptr		rs_ckpt,
    bool				is_forced_shutdown,
    Environment &e)
{
	CL_PANIC(e.exception() == NULL);
	bool retry_needed;
	replica::service_admin_var sa = sap->get_objref();
	version_manager::vp_version_t	running_version;
	version_manager::vm_admin_var	vm_v = vm_util::get_vm();
	//
	// Initialize to lowest possible version in case of error
	//
	running_version.major_num = 0;
	running_version.minor_num = 0;

	if (!CORBA::is_nil(vm_v)) {
		Environment ex;
		vm_v->get_running_version("replica_manager", running_version,
		    ex);
		ASSERT(!ex.exception());
	} else {
		RM_DBG(("rm_repl_service:version manager null reference"));
	}

	if (st != NULL) {
		// This is a retry.
		// If we received a ckpt_shutdown_succeeded
		// checkpoint then the shutdown was approved on the old
		// primary.
		switch (st->state) {
		case trans_state_shutdown_service::COMMITTED_SUCCESS:
			// We are done. Return success.
			return (0);
		case trans_state_shutdown_service::COMMITTED_FAILED:
			// We tried to shutdown a primary, but was an
			// exception. Fall through and try again.
			// We should get the same exception again, though we
			// may get a different one if the service went down.
			// Either one is correct.
			break;
		case trans_state_shutdown_service::SUCCEEDED:
			// A checkpoint was received indicating success, but
			// the commit was not received. wrapup_shutdown() will
			// make sure we are really done.
			wrapup_shutdown(sap);
			return (0);
		case trans_state_shutdown_service::STARTED:
			//
			// We began a request to a primary, but didn't receive
			// a ckpt indicating whether it had succeeded or not.
			//
			// rm_state_machine::assign_provider_state in the
			// RM recovery code checks for possible exception from
			// the previous primary's RMA after finding out that
			// a shutdown was in progress with that provider as
			// primary. If there was no exception, it means that
			// the shutdown had succeeded on the primary node
			// before the RM's failover. In that case, the recovery
			// code goes ahead and calls s->mark_shutdown_succeeded
			// on the new RM primary.
			// See bug 4625850.
			//
			if (is_deleted()) {
				//
				// mark_shutdown_succeeded was called by the
				// RM's recovery code after it discovered that
				// the shutdown of the primary had succeeded
				// and the primary is still alive.
				// So we don't need to invoke shutdown on the
				// primary again, just simply ckpt this to
				// all RM secondaries.
				//
				rs_ckpt->ckpt_shutdown_succeeded(get_number(),
				    sa, e);
				CL_PANIC(!e.exception());
				wrapup_shutdown(sap);
				return (0);
			}
			// The invoked primary is either dead or
			// it may have returned an exception (the recovery code
			// can't tell if the exception was from an older
			// shutdown request or this one). In either case we
			// fall through and retry.
			// First we clear the shutdown_primary pointer that
			// was set on the secondary. All other paths will
			// have already cleared it and we asset that it is
			// NULL when we delete the service.
			shutdown_primary = NULL;
			break;
		default:
			CL_PANIC(0);
		}
	}
	//
	// Check if the service is frozen for upgrade. We cannot
	// shutdown in that case.
	//
	if (is_frozen_for_upgrade() && !is_forced_shutdown) {
		e.exception(new replica::service_frozen_for_upgrade);
	}
	//
	// Check for service down before checking for dependencies. This
	// allows us to shutdown a dead service even if things depend on it.
	//
	if (!e.exception()) {
		chk_s_down(e);
		CL_PANIC(!e.exception() ||
		    replica::failed_service::_exnarrow(e.exception()));
	}
	// Throw depends_on (service_dependencies depend_on_this) exception if
	// we try to remove the service while something depends on it.
	// We only need to do this once, since no new registrations can occur
	// while we have the service locked.
	if (!e.exception() && !deps_up.empty()) {
		e.exception(new replica::depends_on);
	}

	if (!e.exception()) {
		chk_s_uninit(e);
	}

	if (!e.exception() && is_forced_shutdown) {
		//
		// Remove all secondaries by setting the desired number of
		// secondaries to zero.
		//
		// Forced shutdown shares the same code path as the normal
		// shutdown. In normal shutdown, after shutdown() returns
		// successfully on the primary, we convert all secondaries to
		// spares directly without calling remove_secondaries() on the
		// primary. This is fine as long as at the time the shutdown
		// happens there are no references to the service primary and
		// therefore no active invocations on the primary. Otherwise,
		// the primary might continue to send more ckpts to a secondary
		// which has been shutdown and will never hear back from it.
		// That's what exactly can happen to a forced shutdown when we
		// shutdown a service without waiting for all client
		// references to go away. This will result in cluster hang when
		// we call process_to() on the primary to force out all
		// ckpts in the orb and transport before shutting it down. The
		// process_to() may never complete because all the secondaries
		// have gone without the primary's knowledge. To solve this
		// problem, we need to call remove_secondaries() on the primary
		// before converting those secondaries to spares. The easiest
		// way to do it is to set the desired number of secondaries to
		// zero. We don't have to worry about the lack of HAness after
		// setting the desired number of secondaries to zero since
		// the service is going away soon anyways.
		//
		// Note that we have to call set_desired_numsecondaries()
		// before turning on SHUTTING_DOWN flag, otherwise,
		// wait_for_tree_stable() will never return.
		//
		set_desired_numsecondaries(0);
	}

	// Try calling shutdown on the primary until we get a valid answer (no
	// exception, service_busy exception or failed_service exception).
	retry_needed = true;
	while (retry_needed && !e.exception()) {
		retry_needed = false;

		// Setting the SHUTTING_DOWN causes any other operations on the
		// service to block, by making is_stable() fail. Note that this
		// is stronger than locking the service, since that only blocks
		// service_admin requests.
		flags |= SHUTTING_DOWN;

		prov_reg_impl *primaryp = smp->get_primary();
		// Place a hold on the current primary by getting a new
		// reference. This guarantees that it won't receive
		// _unreferenced() while the shutdown() invocation is in
		//
		// We have to call set_desired_numsecondaries() before
		// setting the SHUTTING_DOWN flag, otherwise, the
		// wait_for_tree_stable() will never complete.
		//
		// progress. Also, the SHUTTING_DOWN flag prevents any other
		// operations from changing the primary, so we are guaranteed
		// that it will stay the same.
		replica::repl_prov_reg_ptr primary_ref;
		primary_ref = primaryp->get_new_reference();

		// FAULT POINT
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_1,
			FaultFunctions::generic);
		//
		// Checkpoint the intention to shutdown this primary.
		// Secondaries need to know which primary we were attempting to
		// shutdown so that when they become primary they can
		// distinguish between shutdown() requests that were part of a
		// service shutdown and those cause by shutting down a specific
		// provider. Each one of these shutdown checkpoints overrides
		// the previous one, and secondaries should only keep track of
		// the last one (the earlier ones can be assumed to be dead).
		//
		if (RM_CKPT_VERS(running_version) >= 3) {
			//
			// Checkpoint the shutdown of the service.
			// This will reset the number of secondaries to zero
			// when this is a forced shutdown.
			//
			rs_ckpt->ckpt_shutdown_service_v1(get_number(),
			    primary_ref, sa, is_forced_shutdown, e);
		} else {
			rs_ckpt->ckpt_shutdown_service(get_number(),
			    primary_ref, sa, e);
			if (is_forced_shutdown) {
				//
				// At this point, we know for sure that this
				// service will be shutdown. This is because:
				// 1. This is is a forced shutdown request
				//    which can't fail.
				// 2. After we have done
				//    ckpt_shutdown_service(), this request
				//    will be retried even in the orphaned case.
				//    as has been done in
				//    trans_state_shutdown_service::orphaned()
				//
				// So we can safely ckpt the setting of desired
				// number of secondaries to 0. This is necessary
				// because we don't want those spares to be
				// converted back to secondaries in case of RM
				// failover, especially after we have called
				// shutdown() on the primary.
				//
				rs_ckpt->ckpt_change_desired_numsecondaries(
				    get_number(), 0, e);
			}
		}
		ASSERT(!e.exception());

		FAULTPT_HA_FRMWK(
		    FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_2,
		    FaultFunctions::generic);

		rm_state_machine::state_mutex_unlock();
		Environment local_env;
		if (is_forced_shutdown) {
			uint32_t err_code =
			    primaryp->get_rma_repl_prov_ptr()->shutdown(
			    replica::FORCED_SERVICE_SHUTDOWN, local_env);
			if (err_code != 0) {
				CORBA::release(primary_ref);
				return (err_code);
			}
		} else {
			(void) primaryp->get_rma_repl_prov_ptr()->shutdown(
			    replica::SERVICE_SHUTDOWN, local_env);
		}
		rm_state_machine::state_mutex_lock();
		// Clear the flag before waiting for stable state.
		flags &= ~SHUTTING_DOWN;
		// Release the hold.
		CORBA::release(primary_ref);
		if (local_env.exception()) {
			if (CORBA::COMM_FAILURE::_exnarrow(
			    local_env.exception())) {
				// The primary died, need to retry.
				retry_needed = true;

				// Tell the state machine that there's a
				// problem. This means that we don't have to
				// wait for an unreferenced to come in.
				primaryp->set_sm_prov_state(
				    prov_reg_impl::SM_FAILED);
				// Wait for reconfiguration to complete.
				reconf_complete_wait(e);
				CL_PANIC(!e.exception() ||
				    replica::failed_service::_exnarrow
				    (e.exception()));
			} else {
				//
				// forced shutdown of the primary shouldn't
				// return any other exceptions.
				//
				CL_PANIC(!is_forced_shutdown);

				//
				// Not a comm_failure, we need to put
				// whatever exception into the original
				// environment.
				//
				e.exception(local_env.release_exception());
				CL_PANIC(replica::service_busy::_exnarrow(
				    e.exception()) != NULL);
			}
		}
	}

	// Need to signal an external change because is_stable() could have
	// failed due to the SHUTTING_DOWN flag being set. Actually, this may
	// not be really needed since we will signal when we unlock the
	// service, however it is clearer to do it here too.
	rm_state_machine::signal_external_state_change();

	CORBA::Exception *exp = e.exception();
	bool _is_dead = (replica::failed_service::_exnarrow(exp) != NULL);
	bool success = (exp == NULL || _is_dead ||
	    replica::uninitialized_service::_exnarrow(exp));

	if (success) {
		e.clear();

		// FAULT POINT
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_3,
			FaultFunctions::generic);

		// Checkpoint that we are about to tell the state machine to
		// wrap up the shutdown. In the case where the service was
		// down or uninit, this may be the first checkpoint for this
		// operation.
		rs_ckpt->ckpt_shutdown_succeeded(get_number(), sa, e);
		e.clear();

		// FAULT POINT
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_4,
			FaultFunctions::generic);

		// Recovery will set the DELETED flag if we received the
		// ckpt_shutdown_succeeded checkpoint.
		mark_deleted();

		wrapup_shutdown(sap);
	} else {
		RM_DBG(("rm_rs %s: shutdown failed\n", id()));
		CL_PANIC(replica::service_busy::_exnarrow(exp) ||
		    replica::depends_on::_exnarrow(exp) ||
		    replica::service_frozen_for_upgrade::_exnarrow(exp));
	}

	return (0);
}

void
rm_repl_service::wrapup_shutdown(service_admin_impl *sap)
{
	CL_PANIC(is_deleted());

	// Set this flag so that we can exit the state machine.
	flags |= SHUTDOWN_COMMITTED;

	// Shut all the replicas down.
	shutdown_providers();
	incr_state_count(sap);

	// Remove dependencies so that lower services can be shutdown
	// immediately.
	remove_dependencies();

	// We need to wait for the state machine to bring the service down
	// before we proceeds. This is so that we can guarantee when
	// a shutdown_service call completes, all the replica providers
	// are properly cleaned up. Since the state machine is in the stable
	// state, we need to give it a kick.
	smp->signal_state_machine();
	wait_for_down();

	RM_DBG(("rm_rs %s: successfully shutdown\n", id()));
}

//
// chk_rp_invalid
//
//   Validates a given replica provider
//
void
rm_repl_service::chk_rp_invalid(const char *prov_desc,
    Environment &_environment)
{
	prov_reg_impl	*prov = get_repl_prov(prov_desc);

	// Return invalid repl prov exception if prov is a dup
	if (prov != NULL) {
		RM_DBG(("rm_rs %s: repl_prov state %d\n",
		    id(), prov->get_repl_prov_state()));
		//
		// If the prov was on a dead node it will be unreferenced soon.
		// If the node is not dead, then return 'already_exists'
		// exception.
		//
		if (!prov->get_rma_repl_prov_ptr()->_handler()->server_dead()) {
			_environment.exception(new
			    replica::repl_prov_already_exists);
			RM_DBG(("rm_rs %s: duplicate repl prov %s already "
			    "registered\n", id(), prov_desc));
		} else {
			RM_DBG(("%p rm_rs %s: repl_prov node dead\n",
			    prov, id()));
		}
	}
}

void
rm_repl_service::remove_dependencies()
{
	CL_PANIC(rm_state_machine::state_lock_held());

	rm_repl_service *serv;
	while ((serv = deps_down.reapfirst()) != NULL) {
		(void) serv->deps_up.erase(this);
		// The below service may have been shutdown already and it
		// could be just waiting for its dependendants to go away.
		if (serv->deps_up.empty() && rm_is_primary()) {
			serv->get_state_machine()->signal_state_machine();
		}
	}

	//
	// When we are shutting down a dead service, it could have services
	// depending on it. We need to remove those dependencies.
	//
	while ((serv = deps_up.reapfirst()) != NULL) {
		(void) serv->deps_down.erase(this);
		//
		// If the RM primary dies after remove_dependencies() is called,
		// the dependent service can be mistakenly treated as a bottom
		// service and brought online by the new RM. To avoid this,
		// we need to mark this event on all RM secondaries so that
		// the new RM will know and deal with this corner case
		// properly. See bug 4693284 for more information.
		//
		// remove_dependecies() gets call on all secondaries by
		// ckpt method ckpt_shutdown_succeeded(). If the RM primary
		// dies before the ckpt is received on secondaries, this
		// ckpt will be resent by the new RM primary.
		//
		serv->mark_lower_service_shutdown();
	}
}

// Called when the service is declared down either because it can't
// get a primary or a shutdown_service() was performed.
void
rm_repl_service::shutdown_providers()
{
	prov_reg_impl *rp;

	SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
	for (; (rp = rp_iter.get_current()) != NULL; rp_iter.advance()) {
		// Clean up the dependencies with this provider. We can't
		// assert that deps_up is empty, because in the case where
		// the last providers returned repl_prov_failed during
		// become_primary, the bottom provider is shutdown first.
		// The bottom service goes first because select_new_primary
		// let it go first, and when it fails to find any primary it
		// will do the shutdown first.
		rp->clear_deps_down();
		rp->clear_deps_up();

		rp->clear_sm_prov_state(
		    prov_reg_impl::SM_SEC_TO_MAKE_PRIMARY|
		    prov_reg_impl::SM_SPARE_TO_MAKE_SEC|
		    prov_reg_impl::SM_SEC_NEED_COMMIT|
		    prov_reg_impl::SM_SEC_TO_BE_REMOVED);
		switch (rp->get_repl_prov_state()) {
		case replica::AS_SECONDARY:
			rp->set_sm_prov_state(
			    prov_reg_impl::SM_SEC_TO_BE_SPARED |
			    prov_reg_impl::SM_SPARE_TO_BE_SHUTDOWN);
			break;
		case replica::AS_SPARE:
			rp->set_sm_prov_state(prov_reg_impl::
			    SM_SPARE_TO_BE_SHUTDOWN);
			break;
		case replica::AS_PRIMARY:
			// Do nothing. The state machine will do the right
			// thing.
			break;
		case replica::AS_DOWN:
			break;
		default:
			CL_PANIC(0);
		}
	}
}

//
// Get the list of providers that directly or indirectly depend on this
// provider. Caller is responsible for releasing the list that is returned.
//
prov_reg_list *
rm_repl_service::prov_reg_impl::get_deps_up_tree()
{
	prov_reg_list *tree = new prov_reg_list;

	tree->append(this);
	SList<prov_reg_impl>::ListIterator iter(tree);
	prov_reg_impl *prov;
	while ((prov = iter.get_current()) != NULL) {
		prov_reg_impl *dep;
		SList<prov_reg_impl>::ListIterator dep_up(prov->deps_up);
		while ((dep = dep_up.get_current()) != NULL) {
			if (!tree->exists(dep)) {
				tree->append(dep);
			}
			dep_up.advance();
		}
		iter.advance();
	}

	// remove ourself
	(void) tree->erase(this);

	return (tree);
}

//
// Get the list of providers that this provider directly or indirectly depends
// on. Caller is responsible for releasing the list that is returned.
//
prov_reg_list *
rm_repl_service::prov_reg_impl::get_deps_down_tree()
{
	prov_reg_list *tree = new prov_reg_list;

	tree->append(this);
	SList<prov_reg_impl>::ListIterator iter(tree);
	prov_reg_impl *prov;
	while ((prov = iter.get_current()) != NULL) {
		prov_reg_impl *dep;
		SList<prov_reg_impl>::ListIterator dep_down(prov->deps_down);
		while ((dep = dep_down.get_current()) != NULL) {
			if (!tree->exists(dep)) {
				tree->append(dep);
			}
			dep_down.advance();
		}
		iter.advance();
	}

	// remove ourself
	(void) tree->erase(this);

	return (tree);
}

// Helper function. Remove the dependency relationship between
// this and the ones on the deps_up/deps_down list.
void
rm_repl_service::prov_reg_impl::clear_deps_up()
{
	prov_reg_impl *dep_rp;
	while ((dep_rp = deps_up.reapfirst()) != NULL) {
		(void) dep_rp->deps_down.erase(this);
	}
}

void
rm_repl_service::prov_reg_impl::clear_deps_down()
{
	prov_reg_impl *dep_rp;
	while ((dep_rp = deps_down.reapfirst()) != NULL) {
		(void) dep_rp->deps_up.erase(this);
	}
}

//
// Helper function. Iterate through all the deps_up of this
// and mark them AS_DOWN if they haven't been marked AS_DOWN.
//
void
rm_repl_service::prov_reg_impl::mark_deps_up_tree_down()
{
	prov_reg_impl *dep_rp;
	prov_reg_list *depsup_tree;

	depsup_tree = get_deps_up_tree();
	for (depsup_tree->atfirst();
	    (dep_rp = depsup_tree->get_current()) != NULL;
	    depsup_tree->advance()) {
		if (dep_rp->get_repl_prov_state() != replica::AS_DOWN) {
			dep_rp->set_repl_prov_state(replica::AS_DOWN);
		}
	}
}

// A repl_prov_impl can be deleted when the following conditions
// have been met.
// 1 It has received unreferenced.
// 2 It as been marked down - this implies that the state machine has dealt
//   with its demise.
// 3 Everything that depends on this repl_prov has been cleaned up.
// 4 This is not a former secondary which needs to be removed.
//
void
rm_repl_service::prov_reg_impl::try_delete()
{
	CL_PANIC(rm_state_machine::state_lock_held());
	RM_DBG(("rm_rs:%s try_del unref=%d, state=%d depsup=%d sec_to_rem=%d\n",
		svc->id(), got_unref, r_prov_state, deps_up.empty(),
		(get_sm_prov_state() & SM_SEC_TO_BE_REMOVED)));

	if ((got_unref) && (r_prov_state == replica::AS_DOWN) &&
	    (get_sm_prov_state() & SM_SEC_TO_BE_REMOVED) == 0) {
			svc->remove_repl_prov(this);
			clear_deps_down();

			// Since we don't check for deps_up.empty(),
			// make sure that our deps_up are indeed marked AS_DOWN
			// before we call clear_deps_up().
			mark_deps_up_tree_down();
			clear_deps_up();
			delete this;
	}
}

replica::repl_prov_reg_ptr
rm_repl_service::prov_reg_impl::get_new_reference()
{
	replica::repl_prov_reg_ptr ref = get_objref();
	got_unref = false;
	return (ref);
}


//
// Delete the reg_impl object on a secondary RM.  Note that we
// don't wait for the deps_up list to be empty,
//
void
rm_repl_service::prov_reg_impl::delete_repl_prov_on_sec()
{
	CL_PANIC(rm_state_machine::state_lock_held());
	RM_DBG(("rm_rs %s: "
	    "secondary RM deleting prov_reg_impl `%s', "
	    "deps_up count `%d'\n",
	    svc->id(),
	    get_rp_desc(),
	    deps_up.count()));

	svc->remove_repl_prov(this);
	clear_deps_down();

	// There is no garantee which unreferenced is delivered first.
	// So we need to do the necessary cleanup here.
	clear_deps_up();

	// guaranteed to be last unref on secondary
	delete this;
}

void
rm_repl_service::prov_reg_impl::_unreferenced(unref_t cookie)
{
	rm_state_machine::state_mutex_lock();
	ASSERT(svc);

	RM_DBG(("rm_rs %s: "
	    "unreferenced called for prov_reg_impl `%s', "
	    "state `%d'\n",
	    svc->id(), get_rp_desc(),
	    get_repl_prov_state()));

	if (!svc->rm_is_primary()) {
		//
		// This is a secondary.
		// A secondary always gets unreferenced by a checkpoint.
		// A secondary will always accept an unreference.
		//
		CL_PANIC(_last_unref(cookie));

		delete_repl_prov_on_sec();

	} else if (_last_unref(cookie)) {
		repl_mgr_impl *rmp = repl_mgr_impl::get_rm();
		if (rmp->primary_is_frozen()) {
			RM_DBG(("rm_rs %s: RM pri frozen\n", svc->id()));
			rmp->hold_tmp_reference(get_objref());
		} else {
			got_unref = true;
			svc->get_state_machine()->signal_state_machine();
			switch (get_repl_prov_state()) {
			case replica::AS_PRIMARY:
				set_sm_prov_state(SM_FAILED);
				break;
			case replica::AS_SECONDARY:
				set_sm_prov_state(
				    SM_SEC_TO_BE_REMOVED|SM_FAILED);
				break;
			case replica::AS_SPARE:
				set_sm_prov_state(SM_FAILED);
				break;
			case replica::AS_DOWN:
				// State machine has already dealt with this.
				// Maybe we can delete it now.
				break;
			default:
				CL_PANIC(0);
				break;
			}
		}
	}
	rm_state_machine::state_mutex_unlock();
}

void
rm_repl_service::main_loop()
{
	RM_DBG(("rm_rs %s: entering main loop\n", id()));
	CL_PANIC(!need_thread());
	smp->execute();

	RM_DBG(("rm_rs %s: exiting main loop\n", id()));
}

// Remove repl_prov from the list and from the dependency structure.
void
rm_repl_service::remove_repl_prov(prov_reg_impl *rp)
{
	// Tear down the per repl_prov dependency linkage.

	CL_PANIC(repl_prov_list.exists(rp));
	(void) repl_prov_list.erase(rp);
	decr_service_refs();
}

//
// Convert deps_up list into a service_dependencies sequence.
//
void
rm_repl_service::get_deps_up(replica::service_dependencies *seq)
{
	list_to_seq(&deps_up, seq);
}

//
// Convert deps_down list into a service_dependencies sequence.
//
void
rm_repl_service::get_deps_down(replica::service_dependencies *seq)
{
	list_to_seq(&deps_down, seq);
}

//
// Lookup the dependencies and convert into a service_dependencies list.
//
void
rm_repl_service::list_to_seq(SList<rm_repl_service> *list,
    replica::service_dependencies *seq)
{
	// Count the number of entries in the list, in order to initialize
	// the CORBA sequence.
	uint_t	dep_len = list->count();

	seq->length(dep_len);

	// Iterate through the dependency list and convert to strings.
	rm_repl_service *serv;
	unsigned int i = 0;
	for (list->atfirst();
	    (serv = list->get_current()) != NULL; list->advance()) {
		(*seq)[i++] = (const char *)serv->get_name();
	}
}

//
// destructor for repl_service_list to erase the list. This is needed to avoid
// because we create lots of temporary lists and it is difficult to remember to
// do an erase_list before deleting each one.
// This will only delete the ListElems. The rm_repl_services should not be
// deleted.
//
repl_service_list::~repl_service_list()
{
	erase_list();
}

// primary constructor
rm_repl_service::prov_reg_impl::prov_reg_impl(
    repl_mgr_prov_impl *rm_prov_impl,
    const replica_int::rma_repl_prov_ptr r_prov_ptr,
    const char *desc,
    const replica::checkpoint_ptr chkpt,
    const uint_t pri,
    const replica::repl_prov_state rs,
    rm_repl_service *s) :
    mc_replica_of<replica::repl_prov_reg>(rm_prov_impl),
    r_prov(replica_int::rma_repl_prov::_duplicate(r_prov_ptr)),
    r_prov_desc(rm_repl_service::init_str(desc)),
    s_chkpt(replica::checkpoint::_duplicate(chkpt)),
    priority(pri), r_prov_state(rs),
    sm_prov_state(0),
    svc(s), got_unref(false)
{
}

// secondary constructor
rm_repl_service::prov_reg_impl::prov_reg_impl(
    replica::repl_prov_reg_ptr reg_ptr,
    const replica_int::rma_repl_prov_ptr r_prov_ptr,
    const char *desc,
    const replica::checkpoint_ptr chkpt,
    const uint_t pri,
    const replica::repl_prov_state rs,
    rm_repl_service *s) :
    mc_replica_of<replica::repl_prov_reg>(reg_ptr),
    r_prov(replica_int::rma_repl_prov::_duplicate(r_prov_ptr)),
    r_prov_desc(rm_repl_service::init_str(desc)),
    s_chkpt(replica::checkpoint::_duplicate(chkpt)),
    priority(pri), r_prov_state(rs),
    sm_prov_state(0),
    svc(s), got_unref(false)
{
}

bool
rm_repl_service::rm_is_primary() const
{
	return (repl_mgr_impl::get_rm()->is_primary_mode());
}

void
rm_repl_service::prov_reg_impl::set_sm_prov_state(
    const uint_t new_state)
{
	ASSERT(rm_state_machine::state_lock_held());

	if (new_state != sm_prov_state) {
		svc->get_state_machine()->signal_state_machine();
	}
	sm_prov_state = new_state;
}

// clear sm_prov_state.
void
rm_repl_service::prov_reg_impl::clear_sm_prov_state(
    const uint_t clear_states)
{
	set_sm_prov_state(sm_prov_state & ~clear_states);
}

const char *
rm_repl_service::prov_reg_impl::get_rp_desc() const
{
	return (r_prov_desc);
}

//
// Return the priority of this provider.
//
// Note: Only providers that belong to a bottom level service can have
// priorities associated with them. The priority of a provider belonging to a
// non-bottom service will be that of its bottom level provider. This will
// be referred to as the tree priority of that (non-bottom level) provider.
// If a provider depends on multiple bottom level services, then its tree
// priority will be calculated as the maximum of the priorties of the bottom
// level services.
//
uint32_t
rm_repl_service::prov_reg_impl::get_priority()
{
	if (deps_down.empty()) {
		// We are bottom privider.
		return (priority);
	}

	prov_reg_impl *p;
	int32_t tree_priority = 0;
	int32_t found_tree_priority = -1;

	prov_reg_list *depdown_tree = get_deps_down_tree();

	//
	// Find out the highest priority among all the bottom providers this
	// provider depends on and assign this to this provider.
	// XXX Note: right now, multi-dependency is not fully supported and
	// used. We shouldn't find multiple bottom providers and loop more
	// than once.
	//
	for (depdown_tree->atfirst();
	    (p = depdown_tree->get_current()) != NULL;
	    depdown_tree->advance()) {
		if (p->deps_down.empty()) {
			tree_priority = (int32_t)p->get_priority();
			if ((found_tree_priority > tree_priority) ||
			    (found_tree_priority == -1)) {
				//
				// If this is the first time we find a bottom
				// provider or the current provider has the
				// highest priority. We will use this bottom
				// provider's priority.
				//
				found_tree_priority = tree_priority;
			}
		}
	}

	CL_PANIC(found_tree_priority != -1);
	return ((uint32_t)found_tree_priority);
}

//
// Set priority. Note we allow two providers having identical priority.
//
void
rm_repl_service::prov_reg_impl::set_priority(uint32_t new_priority)
{
	CL_PANIC(deps_down.empty());
	priority = new_priority;
}

//
// Return true if we have higher overall priority than the other providers.
//
// The factors that determine the overall priority are as follows in the
// order given:
//	1. Number of dependent services the provider can bring up in future.
//	2. The associated priority or tree priority.
//
bool
rm_repl_service::prov_reg_impl::has_better_overall_pri_than(
    prov_reg_impl *other)
{
	uint_t total_depsup, other_total_depsup;
	prov_reg_list *depsup_tree;

	// Get the total number providers that are depending on us.
	depsup_tree = get_deps_up_tree();
	total_depsup = depsup_tree->count();
	delete depsup_tree;

	// Get the total number of providers that are depending on us.
	depsup_tree = other->get_deps_up_tree();
	other_total_depsup = depsup_tree->count();
	delete depsup_tree;

	// Compare the overall priorities
	if (total_depsup > other_total_depsup) {
		// We can bring up more services, we have higher priority
		return (true);
	} else if ((total_depsup == other_total_depsup) &&
		    (get_priority() < other->get_priority())) {
		//
		// We can bring up the same number of services as the other
		// one, but we have higher priority associated with us.
		//
		return (true);
	} else {
		return (false);
	}
}

replica_int::rma_repl_prov_ptr
rm_repl_service::prov_reg_impl::get_rma_repl_prov_ptr() const
{
	return (r_prov);
}

replica::repl_prov_state
rm_repl_service::prov_reg_impl::get_repl_prov_state() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (r_prov_state);
}

uint_t
rm_repl_service::prov_reg_impl::get_sm_prov_state() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (sm_prov_state);
}

bool
rm_repl_service::prov_reg_impl::is_primary() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (r_prov_state == replica::AS_PRIMARY);
}

bool
rm_repl_service::prov_reg_impl::is_valid_sec() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (r_prov_state == replica::AS_SECONDARY && (sm_prov_state &
	    (SM_SEC_TO_BE_SPARED | SM_SEC_TO_BE_REMOVED)) == 0);
}

bool
rm_repl_service::prov_reg_impl::is_secondary() const
{
	CL_PANIC(rm_state_machine::state_lock_held());
	return (r_prov_state == replica::AS_SECONDARY);
}

repl_rm::rm_ckpt_ptr
rm_repl_service::prov_reg_impl::get_checkpoint()
{
	return ((repl_mgr_prov_impl*)get_provider())->
	    get_checkpoint_rm_ckpt();
}

//
// Destructor for rm_repl_service::prov_reg_list to erase the list. This is
// needed to avoid memory leak because we create lots of temporary lists and
// it is difficult to remember to do an erase_list before deleting each one.
// This will only delete the LIstElems
//
prov_reg_list::~prov_reg_list()
{
	erase_list();
}

bool
rm_repl_service::is_down() const
{
	return (get_state() == replica::S_DOWN);
}

bool
rm_repl_service::is_up() const
{
	return (get_state() == replica::S_UP);
}

const char *
rm_repl_service::get_name() const
{
	return (name);
}

const replica::service_num_t &
rm_repl_service::get_number() const
{
	return (number);
}

//
// incr_state_count() -- the service's state has changed in some
// important way:  increment it and copy the current state_count
// to the invoking object's count.
//
void
rm_repl_service::incr_state_count(service_admin_impl *sap)
{
	++state_count;
	CL_PANIC(state_count != 0);
	ASSERT(sap);
	update_service_admin_count(sap);
}

char *
rm_repl_service::init_str(const char *s)
{
	return (s ? (os::strcpy(new char [os::strlen(s) + 1], s)) : NULL);
}

// called only by rm_repl_service_mgr::get_control() and add_repl_prov()
void
rm_repl_service::incr_service_refs()
{
	count++;
	CL_PANIC(count != 0);
}

void
rm_repl_service::decr_service_refs()
{
	CL_PANIC(count > 0);
	count--;

	// Note that can_exit_state_machine implies that the service is
	// already shutdown.
	if (can_exit_state_machine()) {
		if (rm_is_primary()) {
			smp->signal_state_machine();
		} else {
			repl_mgr_impl::get_rm()->erase_service(this);
		}
	}
}

bool
rm_repl_service::can_exit_state_machine()
{
	bool rc = false;
	if ((count == 0) && ((flags & SHUTDOWN_COMMITTED) != 0) &&
	    deps_up.empty()) {
		CL_PANIC(is_deleted());
		rc = true;
	}
	return (rc);
}

rm_state_machine *
rm_repl_service::get_state_machine()
{
	return (smp);
}

void
rm_repl_service::chk_s_down(Environment &_environment)
{
	if (is_down()) {
		replica::failed_service	*fse = new replica::failed_service;
		_environment.exception(fse);
		RM_DBG(("rm_rs %s: failed service exception\n", id()));
	}
}

void
rm_repl_service::chk_s_uninit(Environment &_environment)
{
	if (is_uninitialized()) {
		replica::uninitialized_service	*us = new
		    replica::uninitialized_service;
		_environment.exception(us);
		RM_DBG(("rm_rs %s: uninitialized service exception\n", id()));
	}
}

//
// shift_secs() -- Verify if the secondaries distribution of this service is
// in line with the priority rules. If not, shift them accordingly. This
// operation may also affect some or all dependent services of this service.
//
// In order to shift secondaries without affecting the availability of the
// service during this process, we need to perform the shift in two phases
// described below.
//
// Phase 1:
// Increase the desired number of secondaries by a certain number so that all
// spares with higher priorities will be converted to secondaries while those
// secondaries with lower priorities still remain as secondaries.
//
// Phase 2:
// Descrease the desired number of secondaries to the original value so that
// secondaries with lower priorities will be converted to spares.
//
void
rm_repl_service::shift_secs()
{
	uint32_t		numsecs_to_shift = 0;
	rm_repl_service 	*servp = NULL;
	uint32_t		cur_desired_num_secs;

	cur_desired_num_secs = get_desired_numsecondaries();
	//
	// If the current desired numsecondaries is already the maxmium,
	// we don't need to caculate the number of secondaries to shift
	// as there won't be any spares in this service.
	// Otherwise, we need to compare the priorities of secondaries with
	// those of spares and compute the number of secondaries to be
	// converted to spares.
	//
	if (cur_desired_num_secs < MAX_SECONDARIES) {
		prov_reg_impl	*prov_regp;
		prov_reg_list 	*sorted_prov_list = new prov_reg_list;
		//
		// Go through the provider list.
		// Sort the secondaries and spares by their overall priorities.
		//
		SList<prov_reg_impl>::ListIterator rp_iter(repl_prov_list);
		for (; (prov_regp = rp_iter.get_current()) != NULL;
		    rp_iter.advance()) {
			if (!prov_regp->is_primary()) {
				//
				// This is either a secondary or a spare,
				// add it to sorted_prov_list which is
				// sorted by the overall priority.
				//
				add_to_sorted_list(sorted_prov_list,
				    prov_regp);
			}
		}

		//
		// Since providers in the sorted_prov_list are sorted from the
		// one with the best priority to the one with the worst
		// priority, if we take the first cur_desired_num_secs number of
		// providers from the list, we know that they have to be
		// secondaries based on the priority rules. If any of them
		// is a spare, we need to swap it with a secondary that has
		// a worse priority. So the total number of spares among the
		// first cur_desired_num_secs number of providers in the sorted
		// list is the number of providers we need to shift.
		//
		uint32_t num_visited = 0;
		for (sorted_prov_list->atfirst();
		    (((prov_regp = sorted_prov_list->get_current()) != NULL) &&
		    (++num_visited <= cur_desired_num_secs));
		    sorted_prov_list->advance()) {
			if (!prov_regp->is_valid_sec()) {
				// this provider is a spare
				numsecs_to_shift++;
			}
		}
		delete sorted_prov_list;
	}

	// Increase the desired number of secondaries if necessary.
	if (numsecs_to_shift != 0) {
		//
		// Increase the desired number of secondaries in
		// order to convert those spares with higher priority
		// into secondaries.
		//
		set_desired_numsecondaries(
		    cur_desired_num_secs + numsecs_to_shift);
	}

	//
	// Ask those direct dependent services to shift their secondaries if
	// needed.
	//
	for (deps_up.atfirst(); (servp = deps_up.get_current()) != NULL;
	    deps_up.advance()) {
		servp->shift_secs();
	}

	// Decrease the number of secondaries back to the current value
	if (numsecs_to_shift != 0) {
		set_desired_numsecondaries(cur_desired_num_secs);
	}
}

//
// Helper function -- called by shift_secs().
// Add the given new_provp into the given sorted_prov_list which is sorted
// by providers' overall priorities, from the best to the worst.
//
void
rm_repl_service::add_to_sorted_list(prov_reg_list *sorted_prov_list,
    prov_reg_impl *new_provp)
{
	prov_reg_impl	*cur_provp;
	for (sorted_prov_list->atfirst();
	    (cur_provp = sorted_prov_list->get_current()) != NULL;
	    sorted_prov_list->advance()) {
		// We don't expect a primary provider in the list
		CL_PANIC(!cur_provp->is_primary());

		//
		// We also want to make sure not putting a secondary after a
		// spare if they have the same overall priority.
		//
		if (cur_provp->is_valid_sec()) {
			//
			// If cur_provp is a secondary, we insert new_provp
			// before cur_provp only when new_provp has a better
			// overall priority than cur_provp.
			//
			if (new_provp->has_better_overall_pri_than(cur_provp)) {
				sorted_prov_list->insert_b(new_provp);
				break;
			}
		} else if (!cur_provp->has_better_overall_pri_than(new_provp)) {
			//
			// If cur_provp is a spare, we always insert new_provp
			// before cur_provp as long as new_provp has the same
			// or better overall priority than cur_provp.
			//
			sorted_prov_list->insert_b(new_provp);
			break;
		}
	}
	if (cur_provp == NULL) {
		//
		// new_provp has the worst overall priority or the list
		// is empty
		//
		sorted_prov_list->append(new_provp);
	}
}

//
// Shift_secs_from_bottom() -- Shift the secondaries starting from the
// bottom services in the dependency tree. This function is called by
// service_adm_impl::register_rma_repl_prov(). When a dependent provider
// registers, it also changes the priority of its bottom provider.
// So a shift of secondaries from the bottom service is necessary.
//
void
rm_repl_service::shift_secs_from_bottom()
{
	repl_service_list *tree;
	rm_repl_service	*s;

	tree = get_component_tree();
	for (tree->atfirst(); ((s = tree->get_current()) != NULL);
		tree->advance()) {
		if (s->deps_down.empty()) {
			// This is a bottom service
			s->shift_secs();
		}
	}

	delete tree;
}

rm_repl_service::switchover_info_t::switchover_info_t(const char *primary_from,
    const char *primary_to)
{
	ASSERT(primary_from != NULL);
	switchover_from_primary = (char *)os::strdup(primary_from);
	if (primary_to != NULL) {
		switchover_to_primary = (char *)os::strdup(primary_to);
	} else {
		switchover_to_primary = NULL;
	}
}

rm_repl_service::switchover_info_t::~switchover_info_t()
{
	ASSERT(switchover_from_primary != NULL);
	delete [] switchover_from_primary;
	switchover_from_primary = NULL;
	if (switchover_to_primary != NULL) {
		delete [] switchover_to_primary;
		switchover_to_primary = NULL;
	}
}

//
// freeze_dependent_for_upgrade
//
// Helper fundtion to freeze dependent services.
//
void
rm_repl_service::freeze_dependent_for_upgrade()
{
	ASSERT(rm_state_machine::state_lock_held());

	if (deps_up.empty()) {
		return;
	}
	rm_repl_service *deps_up_s;
	SList<rm_repl_service>::ListIterator iter(deps_up);

	for (; (deps_up_s = iter.get_current()) != NULL;
	    iter.advance()) {

		RM_DBG(("rm_rs %s: marking %s for freeze\n", id(),
		deps_up_s->id()));
		deps_up_s->mark_freeze_for_upgrade();
		deps_up_s->freeze_dependent_for_upgrade();
	}
}

//
// freeze_for_upgrade
//
// Freeze given service for upgrade. This function will also freeze
// services depending upon this service. This is to prevent invocation
// to dependent services from blocking indefinitely. Invocation to
// this service and those to the dependent services will block until
// the service is unfrozen.
//
void
rm_repl_service::freeze_for_upgrade()
{
	RM_DBG(("rm_rs %s: freeze_for_upgrade\n", id()));

	//
	// It is ok to update this flag without locking the tree
	// because state machine transitions depend on the
	// FREEZE_FOR_UPGRADE flag. The read/write of this flag itself is
	// under the state machine lock.
	//
	flags |= FROZEN_FOR_UPGRADE_BY_REQ;
	//
	// If the service is already frozen for upgrade (or marked
	// such on secondary), the dependent services will also be
	// frozen (or marked such on secondary). We need not call
	// mark_dependent_freeze_for_upgrade() again because we do not
	// allow upper service to unfreeze if the lower one is frozen.
	//
	if (flags & FREEZE_FOR_UPGRADE) {
		return;
	}

	//
	// The service may be recovering or performing some other transition.
	// Wait for it to become stable and for us to have exclusive access.
	//
	repl_service_list *dependent_svc_listp = NULL;

	if (rm_is_primary()) {
		dependent_svc_listp = lock_tree();
	}

	flags |= FREEZE_FOR_UPGRADE;
	//
	// Freeze all services that depend on us.
	// This is to make sure that any invocations
	// to them do not wait for us to unfreeze and
	// are returned proper exception.
	//
	freeze_dependent_for_upgrade();

	if (rm_is_primary()) {
		smp->signal_state_machine();
		// Wait for the service to go to stable state.
		wait_for_stable();
		// depsup cannot be NULL on rm_primary
		unlock_tree(dependent_svc_listp);

	}
}

void
rm_repl_service::unfreeze_dependent_after_upgrade()
{
	ASSERT(rm_state_machine::state_lock_held());

	if (deps_up.empty()) {
		return;
	}

	rm_repl_service *depsp;
	SList<rm_repl_service>::ListIterator iter(deps_up);
	iter.reinit(deps_up);
	for (; (depsp = iter.get_current()) != NULL; iter.advance()) {
		//
		// If the service was explicitly frozen for
		// upgrade client do not unfreeze it (and its
		// dependent services.) This service should be
		// unfrozen explicitly.
		//
		if (depsp->frozen_for_upgrade_by_req()) {
			RM_DBG(("rm_rs %s: frozen by req no unfreeze %s\n",
			    id(), depsp->id()));
			continue;
		} else {
			RM_DBG(("rm_rs %s: marking %s for unfreeze\n",
			    id(), depsp->id()));
			depsp->mark_unfreeze_after_upgrade();
			depsp->unfreeze_dependent_after_upgrade();
		}
	}
}

//
// unfreeze_after_upgrade
//
// Unfreeze services that were frozen for upgrade. If dependent
// services were frozen by the framework, they will be unfrozen. If
// dependent services were frozen for upgrade explicitly, they (and
// services depending upon them) will not be unfrozen.
//
void
rm_repl_service::unfreeze_after_upgrade(Environment &env)
{
	RM_DBG(("rm_rs %s: unfreeze_after_upgrade\n", id()));

	if ((flags & FREEZE_FOR_UPGRADE) == 0) {
		// service is not frozen for upgrade
		return;
	}

	//
	// Reset FROZEN_FOR_UPGRADE_BY_REQ bit indicating that this
	// service can be unfrozen. Whether it will be unfrozen
	// depends on the status of service it depends upon. If the
	// lower level service is frozen, then when it is unfrozen it
	// can unfreeze this service.
	//
	flags &= ~FROZEN_FOR_UPGRADE_BY_REQ;

	//
	// The service may be recovering or performing some other transition.
	// Wait for it to become stable and for us to have exclusive access.
	//
	repl_service_list *dependent_svc_listp = NULL;

	if (rm_is_primary()) {
		dependent_svc_listp = lock_tree();
	}

	rm_repl_service *depsp;
	SList<rm_repl_service>::ListIterator iter;

	if (rm_is_primary()) {
		//
		// Check if service we depend upon is frozen. If yes,
		// then return an exception
		//
		if (!deps_down.empty()) {
			iter.reinit(deps_down);
			depsp = iter.get_current();
			if (depsp->is_frozen_for_upgrade()) {
				env.exception(new
				    replica::dependent_service_frozen);
				unlock_tree(dependent_svc_listp);
				return;
			}
		}
	}

	flags &= ~FREEZE_FOR_UPGRADE;

	unfreeze_dependent_after_upgrade();

	if (rm_is_primary()) {
		smp->signal_state_machine();
		// Wait for the service to go to stable state.
		wait_for_stable();

		unlock_tree(dependent_svc_listp);
	}
}

#ifdef RM_DBGBUF
const char *
rm_repl_service::id()
{
	return ((const char *)debug_id);
}
#endif
