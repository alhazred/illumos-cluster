/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RM_TRANS_STATES_H
#define	_RM_TRANS_STATES_H

#pragma ident	"@(#)rm_trans_states.h	1.9	08/05/20 SMI"

//
// rm_trans_states.h contains class definitions for the replica
// manager transaction state interface.
//

#include <repl/service/transaction_state.h>

class service_admin_impl;

class trans_state_register_service : public transaction_state {
public:
	trans_state_register_service();		// null
	~trans_state_register_service();	// null
	void orphaned(Environment &);
	void committed();
};

class trans_state_get_control : public transaction_state {
public:
	trans_state_get_control(service_admin_impl *);
	~trans_state_get_control();	// null
	void orphaned(Environment &);
	void committed();
	service_admin_impl	*get_return_value();
private:
	// return value
	service_admin_impl		*sai;

	// Disallow
	trans_state_get_control();
};

class trans_state_add_rma : public transaction_state {
public:
	trans_state_add_rma(repl_mgr_impl *obj, replica_int::rma_reconf_ptr r);
	~trans_state_add_rma();
	void orphaned(Environment &);
	void committed();

private:
	repl_mgr_impl *rm_repl_obj;
	replica_int::rma_reconf_ptr rma_p;
	// Disallow
	trans_state_add_rma();
};

// This is used by mini-transaction repl_mgr_impl::register_lca()
class trans_state_register_lca : public transaction_state {
public:
	trans_state_register_lca(cb_coord_control_impl *impl_obj);
	~trans_state_register_lca();
	void orphaned(Environment &);
	void committed();
	cb_coord_control_impl	*get_control_impl();
private:
	cb_coord_control_impl	*cbcc_p;
};

#include <repl/repl_mgr/rm_trans_states_in.h>

#endif	/* _RM_TRANS_STATES_H */
