/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  hxdoor_service_in.h
 *
 */

#ifndef _HXDOOR_SERVICE_IN_H
#define	_HXDOOR_SERVICE_IN_H

#pragma ident	"@(#)hxdoor_service_in.h	1.9	08/05/20 SMI"

inline void
hxdoor_service::lock()
{
	hs_lck.lock();
}

inline void
hxdoor_service::unlock()
{
	hs_lck.unlock();
}

inline bool
hxdoor_service::lock_held()
{
	return (hs_lck.lock_held());
}

inline const char *
hxdoor_service::get_name()
{
	return (_sname);
}

// Return the current service state.
inline replica_int::rma_service_state
hxdoor_service::get_state()
{
	return (_sstate);
}

inline replica::service_num_t
hxdoor_service::get_service_id()
{
	return (servid);
}

// Called on client before an outgoing invocation.
// Returns true if the invocation is allowed, false if the service is down.
// If the service is frozen, then this blocks until it is unfrozen.

inline bool
hxdoor_service::start_client_invo()
{
	ASSERT(!lock_held());
	return (invo_fl.hold());
}

// Called on client after an outgoing invocation The freeze flag indicates
// whether we should freeze the service (because there was an exception which
// indicated that a freeze is necessary).
inline void
hxdoor_service::client_invo_done(bool freeze)
{
	invo_fl.done();
	if (freeze) {
		invo_fl.block_now();
	}
}

inline void
hxdoor_service::freeze_service_invos()
{
	// This disable command is called safely multiple times without
	// any intervening enable command.
	// Blocking is disabled prior to the freeze in order to prevent
	// the freeze from waiting for a msg blocked in flow control.
	//
	CORBA::SystemException se =
	    CORBA::COMM_FAILURE(0, CORBA::COMPLETED_NO);
	resource_blocker::the().disable_flow_blocking(this, se);

	RMA_DBG(("hxdoor_service::freeze_service_invos %d\n", servid));
	lock();
	// Wake up threads that are blocked waiting for the freeze to begin
	freezing = true;
	freeze_cv.broadcast();
	unlock();
	invo_fl.freeze_if_unfrozen();
	//
	// Notify any threads that are blocked waiting for the freeze to
	// complete.
	//
	lock();
	freezing = false;
	freeze_cv.broadcast();
	unlock();
}

inline void
hxdoor_service::unfreeze_service_invos()
{

	if (!CORBA::is_nil(primary_prov_p) &&
	    !(primary_prov_p->_handler()->server_dead())) {
		// Ensure that flow control enables blocking of invocations for
		// this service now that they are about to be unfrozen.
		// This enable command is called safely multiple times without
		// any intervening disable command.
		//
		resource_blocker::the().enable_flow_blocking(this);

		RMA_DBG(("hxd_svc::unfreeze_service_invos %d\n", servid));
		invo_fl.unfreeze_if_frozen();
	}
}

inline xdoor_id
hxdoor_service::get_new_objid()
{
	ASSERT(lock_held());
	return (++last_object_id);
}

inline void
hxdoor_service::update_objid(xdoor_id objid)
{
	data_lock.lock();
	if (objid > last_object_id) {
		last_object_id = objid;
	}
	data_lock.unlock();
}

inline bool
hxdoor_service::ignore_unrefs()
{
	return ((hs_flags & HS_IGNORE_UNREF) != 0);
}

inline bool
hxdoor_service::marshals_blocked()
{
	return ((hs_flags & HS_MARSHALS_BLOCKED) != 0);
}

inline bool
hxdoor_service::primary_known()
{
	return ((hs_flags & HS_PRIMARY_KNOWN) != 0);
}

inline bool
hxdoor_service::reconnecting()
{
	return ((hs_flags & HS_RECONNECTING) != 0);
}
inline bool
hxdoor_service::is_down()
{
	return (invo_fl.failed());
}

inline void
hxdoor_service::wait_for_freeze_request()
{
	lock();
	while (!freezing) {
		RMA_DBG(("wait_for_freeze_req\n"));
		freeze_cv.wait(&hs_lck);
	}
	unlock();
}

inline void
hxdoor_service::wait_for_freeze_complete()
{
	lock();
	while (freezing) {
		RMA_DBG(("wait_for_freeze_complete\n"));
		freeze_cv.wait(&hs_lck);
	}
	unlock();
}

//
// Returns true if the current reconfiguration has been canceled.
//
inline bool
hxdoor_service::reconfig_canceled(cmm::seqnum_t seqnum)
{
	//
	// A sequence number of zero is invalid and means that we are not
	// canceled. It is passed in when this is not called from a
	// reconfiguration step, in which case we return false.
	//
	return (seqnum != 0ULL && (seqnum <= canceled_seqnum));
}

#endif	/* _HXDOOR_SERVICE_IN_H */
