//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _HXDOOR_H
#define	_HXDOOR_H

#pragma ident	"@(#)hxdoor.h	1.105	08/07/17 SMI"

#include <sys/list_def.h>
#include <sys/os.h>
#include <h/replica_int.h>
#include <h/cmm.h>
#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/xdoor_kit.h>
#include <orb/xdoor/rxdoor.h>
#include <orb/xdoor/standard_xdoor.h>
#include <orb/handler/handler.h>
#include <orb/infrastructure/freeze_lock.h>
#include <orb/flow/resource_blocker.h>
#include <orb/refs/unref_threadpool.h>
#include <orb/debug/haci_debug.h>

#include <orb/fault/fault_injection.h>

#if defined(HACI_DBGBUFS) && !defined(NO_RMA_DBGBUF)
#define	RMA_DBGBUF
#endif

#ifdef	RMA_DBGBUF
extern dbg_print_buf rma_dbg;
#define	RMA_DBG(a) HACI_DEBUG(ENABLE_RMA_DBG, rma_dbg, a)
#else
#define	RMA_DBG(a)
#endif

//
// Hxdoors are highly available xdoors. They are used as part of the replica
// framework. The replica framework has the concept of a service which has a
// set of objects and a primary and zero or more secondaries.
// Objects can only be created on the primary and are identified by their
// service id and xdoor id which form the hxdoor_id. The hxdoor_id is unique
// throughout the cluster.
//
// Hxdoors allow us to reconnect to a new primary when a failure occurs. This
// reconnection is transparent to the handlers performing invocations.
//

// This is the global id of an hxdoor.  hxdoor references are passed
// to other kernel domains by marshaling the hxdoor_id.
//
class hxdoor_id : public rxdoor_descriptor {
public:
	hxdoor_id();
	hxdoor_id(rxdoor_ref &);

	hxdoor_id(xdoor_id, replica::service_num_t);

	void		_put(MarshalStream &wms);
	void		_get(MarshalStream &rms);

	static uint_t	_sizeof_marshal();

	virtual void	put_ref_job(rxdoor_ref *);
	//
	// Data members - HA service ID Let this be public since many
	// classes like hxdoor, hxdoor_kit and hxdoor_manager need to
	// use it frequently. Other option is to make it private and
	// have all the hxdoor* classes as friends.
	//
	replica::service_num_t	serviceid;

private:
	hxdoor_id &operator = (hxdoor_id &);

};

class hxdoor_kit : public rxdoor_kit
{
public:
	hxdoor_kit();

	virtual void 	unmarshal_cancel(ID_node &, MarshalStream &);
	virtual void 	marshal_roll_back(ID_node &, MarshalStream &);

	// Lookup rxdoor using kit specific information
	virtual rxdoor	*lookup_rxdoor(bool *, ID_node &, MarshalStream &,
			    rxdoor_bucket **);

	virtual rxdoor	*lookup_rxdoor_from_invo_header(rxdoor_invo_header *,
			    rxdoor_bucket **);

	virtual rxdoor 	*lookup_rxdoor_from_refjob(rxdoor_ref *refp,
			    rxdoor_bucket **xbpp);

	virtual	rxdoor	*create(rxdoor_descriptor &);

protected:
	virtual void	unmarshal_cancel_remainder(ID_node &,
			    rxdoor_descriptor&, MarshalStream &);
};

// forward declaration
class hxdoor;
class hxdoor_service;

class hxdoor_manager: public notify_membership_change {
public:
	friend class hxdoor_service;
	friend class hxdoor_kit;

	hxdoor_manager();

	static hxdoor_manager	&the();
	static int		initialize();

	// register a new hxdoor.
	void			register_hxdoor(hxdoor *xdp,
				    replica::service_num_t);

	// release an hxdoor registration matching the hxdoor_id. This is
	// called by mc_checkpoint<CKPT>::delete_obj only.
	void			release_by_ckpt(hxdoor_id &hxdid);

	// Return pointer to rxdoor
	hxdoor			*lookup_hxdoor(bool *, hxdoor_id &,
				    rxdoor_bucket **);

	// Used during reconfiguration to rebuild the foreign reference count
	// of hxdoors.
	void			cleanup_foreignrefs(callback_membership *,
				    callback_membership *);

	virtual void		update_current();

	bool			lock_held();

private:
	void			lock();
	void			unlock();

	hxdoor_manager(const hxdoor_manager &);
	hxdoor_manager &operator = (hxdoor_manager &);

	os::mutex_t		lck;
	os::condvar_t		cv;

	static	hxdoor_manager	*hxdmgr;
};


//
// HA Xdoor interface.
//
class hxdoor :
	public standard_xdoor_server
{
	friend class hxdoor_manager;
	friend class hxdoor_service;
	friend class hxdoor_kit;
	friend class rma;

public:
	static hxdoor_kit	hxd_kit;

	// create new hxdoor
	static hxdoor	*create(handler *h);
	static hxdoor	*create(hxdoor_id &id);

	xdoor_id		get_objectid();
	replica::service_num_t	get_serviceid();

	virtual	rxdoor_kit	&get_kit();
	virtual void		get_xdoor_desc(rxdoor_descriptor &xd);

	// marshal functions
	void		marshal(ID_node &, MarshalStream &, Environment *);
	void		marshal_local(Environment *);
	void		marshal_roll_back(ID_node &, MarshalStream &);
	virtual void	marshal_received();
	void		marshal_cancel();
	void		unmarshal(ID_node &, MarshalStream &, bool,
			    Environment *);

	virtual void	marshal_xdid(MarshalStream &);

	bool		reference_counting();
	void		accept_unreferenced();
	void		reject_unreferenced(uint_t);
	void		deliver_unreferenced();
	void		clear_unreferenced();
	void		revoked();
	void		unref_by_ckpt();

	// Xdoor methods implemented here.
	void		post_extract_done(handler_type);
	bool		client_unreferenced(handler_type, uint_t marshcnt);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	virtual void 	make_oneway_header(uint_t, sendstream *);
	virtual void 	make_twoway_header(uint_t, sendstream *, ushort_t);
#else
	virtual void 	make_oneway_header(sendstream *);
	virtual void 	make_twoway_header(sendstream *, ushort_t);
#endif

	ExceptionStatus	request_twoway(invocation &);
	ExceptionStatus	request_oneway(invocation &);

	//
	// Identify resources needed at hxdoor level for request
	// This method is used for invocation messages and not reply messages.
	//
	sendstream	*reserve_resources(resources *resourcep);

	// Called if reserve_resources is called but request will not be called
	void		release_resources(resources *);

	// Identify resources needed at hxdoor level for invocation reply
	sendstream	*reserve_reply_resources(resources *resourcep);

	void		send_reply(service &s);

	// Called by handler when it gives out a reference making
	// 0->1 transition.
	void			have_client_ref();
	// Called by secondary when processing a checkpoint.
	void			add_secondary_obj();
	// Called by secondary when processing a become_spare or a checkpoint
	bool			remove_secondary_obj();

	// Returns true if the hxdoor is the primary
	bool			i_am_primary();

	// Returns true if this hxdoor must reconnect to the new primary.
	bool			must_reconnect();
#ifdef DEBUGGER_PRINT
	handler 		*mdb_get_userhandler();
	handler 		*mdb_get_kernhandler();
#endif

	virtual	const char	*xdoor_type();

	//
	// This method returns state of the service.
	bool    server_dead();

private:
	// The following constructor supports server xdoors
	hxdoor(handler *h);

	// The following constructor supports client xdoors
	hxdoor(hxdoor_id &id);

#ifdef DEBUG
	~hxdoor();
#endif
	// Utilities for hxdoors
	hxdoor_service		*get_service();
	void			handle_possible_unreferenced();

	virtual handler		*get_unref_handler();
	virtual uint_t		get_defer_task_tag();
	void			unref_rejected();

	void			register_with_primary(ID_node &);

	// Used by RMA reconfiguration.
	//
	// Called on clients to discard information about a primary that is
	// failed or shutting down. Returns true if the xdoor went away, false
	// if an unmarshal is in progress. Note that if an unmarshal is in
	// progress and the caller really wants the xdoor to go away, then
	// it needs to set a flag indicating to the unmarshal code that it
	// should release the xdoor when the unmarshal completes.
	void			unregister_from_primary();
	void			disconnect_client();

#ifdef DEBUG
	void			mark_down();
#endif
	void			become_primary();
	void			become_secondary();
	void			become_spare();

	// Called by RMA to see if this hxdoor has a client reference.
	bool			has_client_ref();

	// Returns true if this is a server rxdoor.
	virtual bool		local();

	// For now the kernel handler is always the server (if we are primary).
	// We have lots of assumptions in the code about this.

	virtual handler		*get_server_handler();
	virtual void		set_server_handler(handler *);
	virtual handler 	*get_client_handler();

	// Disallow assignments and pass by value
	hxdoor(const hxdoor &);
	hxdoor &operator = (hxdoor &);
};

#include <repl/rma/hxdoor_in.h>

#endif	/* _HXDOOR_H */
