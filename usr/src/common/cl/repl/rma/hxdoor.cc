//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hxdoor.cc	1.168	08/07/17 SMI"

// Implementation file for hxdoors.

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/xdoor/rxdoor.h>
#include <repl/rma/hxdoor.h>
#include <repl/rma/hxdoor_service.h>
#include <repl/service/replica_handler.h>
#include <repl/rma/rma.h>
#include <orb/fault/fault_injection.h>
#include <orb/refs/unref_threadpool.h>
#include <orb/msg/orb_msg.h>
#include <orb/transport/nil_endpoint.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/member/members.h>
#include <sys/mc_probe.h>

// hxdoor_id methods

void
hxdoor_id::put_ref_job(rxdoor_ref *ref)
{
	ref->xdid = xdid;
	ref->serviceid = serviceid;
}

//
// hxdoor_kit stuff
//
hxdoor_kit hxdoor::hxd_kit;

// marshal_roll_back
//
// Roll back the marshal of a hxdoor.
// The marshal stream contains a global hxdoor id followed by some
// xdoor type specific data.
// We must look up the xdoor and call its marshal_roll_back method.
//
void
hxdoor_kit::marshal_roll_back(ID_node &node, MarshalStream &xdoors)
{
	hxdoor_id	hxdid;
	hxdoor		*xdp;
	hxdoor_service	*hsp;
	rxdoor_bucket	*xbp;

	hxdid._get(xdoors);

	RMA_DBG(("hxdoor_kit::marshal_roll_back\n"));
	hsp = rma::the().lookup_service(hxdid.serviceid);
	CL_PANIC(hsp != NULL);
	xbp = hsp->bucket_hash(hxdid.xdid);
	xbp->lock();
	hsp->unlock();
	xdp = (hxdoor *)xbp->find(hxdid.xdid);
	xdp->marshal_roll_back(node, xdoors);
	xbp->unlock();
}

//
// lookup_rxdoor
//
// Virtual method called by rxdoor_kit::unmarshal() to lookup existing
// hxdoor or create one if it does not exist. If *new_door=true and
// rxdoor is not found, a new one will be created.
//
rxdoor *
hxdoor_kit::lookup_rxdoor(bool *new_door, ID_node &, MarshalStream &xdoors,
    rxdoor_bucket **xbpp)
{
	hxdoor_id	hxdid;

	// get hxdoor id
	hxdid._get(xdoors);

	return ((rxdoor *)hxdoor_manager::the().lookup_hxdoor(new_door,
	    hxdid, xbpp));
}

//
// lookup_rxdoor_from_invo_header
//
// Virtual method looks up hxdoor from information in the invocation header.
//
rxdoor *
hxdoor_kit::lookup_rxdoor_from_invo_header(rxdoor_invo_header *headerp,
    rxdoor_bucket **xbpp)
{
	hxdoor_id	hxdid;

	hxdid.xdid = headerp->xdid;
	hxdid.serviceid = ((rxdoor_twoway_header *)headerp)->svcid;

	bool	new_door = false;

	return ((rxdoor *)hxdoor_manager::the().lookup_hxdoor(&new_door,
	    hxdid, xbpp));
}

//
// lookup_rxdoor_from_refjob
//
// Virtual method looks up hxdoor using hxdoor_id contained in a refjob.
//
rxdoor *
hxdoor_kit::lookup_rxdoor_from_refjob(rxdoor_ref *refp,
    rxdoor_bucket **xbpp)
{
	hxdoor_id	hxdid;

	hxdid.xdid = refp->xdid;
	hxdid.serviceid = refp->serviceid;

	bool	new_door = false;

	return ((rxdoor *)hxdoor_manager::the().lookup_hxdoor(&new_door,
	    hxdid, xbpp));
}

//
// unmarshal_cancel
//
// Cancel the unmarshal of a hxdoor.
// The marshal stream contains a hxdoor id followed by some
// xdoor type specific data.
//
void
hxdoor_kit::unmarshal_cancel(ID_node &node, MarshalStream &xdoors)
{
	hxdoor_id hxdid;

	hxdid._get(xdoors);
	unmarshal_cancel_remainder(node, hxdid, xdoors);
}

//
// unmarshal_cancel_remainder
//
void
hxdoor_kit::unmarshal_cancel_remainder(ID_node &from_node,
	rxdoor_descriptor &rxdid, MarshalStream &rms)
{
	//
	// hxdoors sent by the primary have an extra boolean indicating
	// whether the server performed an implicit register as part of its
	// marshal.
	//
	if (rms.get_boolean()) { // Check if it was from primary
		bool server_new = rms.get_boolean();
		if (server_new) {
			// The server performed an implicit register when it
			// marshaled this.  We send an unreg to make up for
			// that.
			// Note that it doesn't matter if another unmarshal
			// has already created the xdoor on this node. We need
			// to do an unregister either way.
			refcount::the().add_unreg(from_node, rxdid);
		}
	}
}


//
// create
//
// Create hxdoor with the given hxdoor id.
//
rxdoor *
hxdoor_kit::create(rxdoor_descriptor &xd)
{
	hxdoor *hxdp = hxdoor::create((hxdoor_id &)xd);
	//
	// The kit will create an hxdoor if it unmarshalled a new one.
	// The rxdoor_kit::unmarshal() method will store the door in
	// the in bucket, but we need to tell the service that a new
	// door was created.
	//
	hxdoor_service *hsp = rma::the().
	    lookup_service_unlocked(((hxdoor_id &)xd).serviceid);
	CL_PANIC(hsp);
	hsp->hxdoor_created();

	return (hxdp);
}

//
//  hxdoor_manager stuff
//
hxdoor_manager *hxdoor_manager::hxdmgr = NULL;

//
// initialize
//
// Create the hxdoor_manager
//
int
hxdoor_manager::initialize()
{
	ASSERT(hxdmgr == NULL);
	hxdmgr = new hxdoor_manager;
	if (hxdmgr == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// register_hxdoor
//
//   This method is called on the primary to add a hxdoor in the
//   service structure. The method keeps track of number of hxdoors
//   created in this service by calling hxdoor_created() method of the
//   service.
//
void
hxdoor_manager::register_hxdoor(hxdoor *xdp, replica::service_num_t servid)
{
	hxdoor_service *hsp;
	hsp = rma::the().lookup_service(servid);
	CL_PANIC(hsp != NULL);
	hsp->store(xdp);
	hsp->hxdoor_created();
	hsp->unlock();
}

//
// release_by_ckpt
//
// release an hxdoor registration matching the service_num_t and xdoor id.
// This is called by mc_checkpoint<CKPT>::delete_obj only.
//
void
hxdoor_manager::release_by_ckpt(hxdoor_id &hxdid)
{
	hxdoor  *hxdp;
	hxdoor_service *hsp;

	hsp = rma::the().lookup_service(hxdid.serviceid);
	CL_PANIC(hsp != NULL);
	rxdoor_bucket *xbp = hsp->bucket_hash(hxdid.xdid);

	// Lock the hxdoor (by locking its bucket).
	xbp->lock();
	hsp->unlock();
	hxdp = (hxdoor *)xbp->find(hxdid.xdid);
	xbp->unlock();

	if (hxdp == NULL) {
		//
		// We did not find the hxdoor on this node. See CR 6339151.
		//
		// If an HA object gets unreferenced, but the service gets
		// frozen (due to addition of new secondary) before the
		// implementation object's destructor is called, the object
		// does not get checkpointed to the new secondary by the
		// dump_state() method of the service. Once the dump_state()
		// completes and the service is unfrozen, the implementation
		// object's destructor gets called. This checkpoints the
		// delete operation to the secondaries.
		//
		// The new secondary did not create the object. So the hxdoor
		// lookup will fail when the delete checkpoint comes in. It
		// is safe to ignore this because the HA object has been
		// deleted in the primary (and in those secondaries that DO
		// have the object). Record the lookup failure in the debug
		// buffer and move on.
		//
		RMA_DBG(("(%p)HA: hxdoor %d.%d does not exist on secondary\n",
		    this, hxdid.serviceid, hxdid.xdid));
	} else {
		hxdp->unref_by_ckpt();
	}
}

//
// lookup_hxdoor -  Returns a pointer to the hxdoor.
// When a hxdoor is not found and new_door is true,
// the method creates a new hxdoor.
//
// Note: the caller must unlock the bucket lock.
//
hxdoor *
hxdoor_manager::lookup_hxdoor(bool *new_door, hxdoor_id &hxdid,
    rxdoor_bucket **xbpp)
{
	hxdoor_service	*hsp = rma::the().lookup_service(hxdid.serviceid);
	ASSERT(hsp != NULL && hsp->lock_held());
	CL_PANIC(hsp);
	rxdoor_bucket	*xbp = hsp->bucket_hash(hxdid.xdid);
	xbp->lock();

	hsp->unlock();

	if (xbpp) {
		*xbpp = xbp;
	}
	hxdoor	*hxdp = (hxdoor *)xbp->find(hxdid.xdid);

	if (hxdp == NULL) {
		//
		// we unmarshalled a new hxdoor, primary must have
		// allocated an id. As potential primary, keep track
		// of the last xdid assigned within the system.
		//
		hsp->update_objid(hxdid.xdid);
	}

	if (hxdp) {
		//
		// found the rxdoor. set flag to indicate that new
		// rxdoor was not created.
		//
		*new_door = false;

	} else if (*new_door) {
		hxdp = hxdoor::create(hxdid);
		//
		// The kit will create an hxdoor if it unmarshalled a new one.
		// The rxdoor_kit::unmarshal() method will store the door in
		// the in bucket, but we need to tell the service that a new
		// door was created.
		//
		hsp->hxdoor_created();
	}
	return (hxdp);
}

//
// cleanup_foreignrefs
//
// Cleanup references sent to dead nodes
//
void
hxdoor_manager::cleanup_foreignrefs(callback_membership *curr_mbrship,
    callback_membership *new_mbrship)
{
	rma::the().cleanup_foreignrefs(curr_mbrship, new_mbrship);
}

//
// update_current
//
// Any data structures that need to be updated due to membership change
// are done here.
//
void
hxdoor_manager::update_current()
{
	// nothing to do
}

//
// hxdoor implementations
//

//
// create
//
// Create an hxdoor for the specified handler
//
hxdoor *
hxdoor::create(handler *h)
{
	hxdoor *xdp;

	if ((xdp = new hxdoor(h)) == NULL) {
		return (NULL);
	}

	// register_hxdoor will allocate the object id.
	hxdoor_manager::the().register_hxdoor(xdp,
	    ((replica_handler *)h)->get_service()->get_sid());

	return (xdp);
}

//
// create
//
// Create an hxdoor for the given hxdoor_id
//
hxdoor *
hxdoor::create(hxdoor_id &id)
{
	return (new hxdoor(id));
}

//
// Constructor for primary (or server side) hxdoor
//
hxdoor::hxdoor(handler *h) :
	standard_xdoor_server(h)
{
	//
	// need to set hxdoor specific flags. HAS_CLIENT_REF flag is
	// set here to avoid the hxdoor from been considered
	// unreferenced soon after it has been created.
	//
	flags |= PRIMARY | HAS_CLIENT_REF;

	// All initialization is done by standard_xdoor_server and
	// rxdoor. Since there are no HA servers in userland, make
	// sure that the handler is kernel handler.
	//
	ASSERT(!h->is_user());
}

//
// Constructor for client and/or secondary hxdoor
//
hxdoor::hxdoor(hxdoor_id &hxdid) :
	standard_xdoor_server(hxdid.xdid)
{
	//
	// For hxdoors, we don't have servers in userland so the value
	// of server_handler_index should be set to
	// Xdoor::KERNEL_HANDLER.
	//
	server_handler_index = Xdoor::KERNEL_HANDLER;

}

//
// get_serviceid
//
// Returns service id of the service that this hxdoor belongs to.
//
replica::service_num_t
hxdoor::get_serviceid()
{
	//
	// every h(r)xdoor points to its bucket.
	// every bucket points to the rxdoor_node that contains it.
	// In this case the rxdoor_node is also hxdoor_service
	//
	return (((hxdoor_service *)(rxdoor::bucketp->rnp))->get_service_id());
}

//
// get_kit
//
// Return hxd_kit.
//
rxdoor_kit &
hxdoor::get_kit()
{
	return (hxd_kit);
}

//
// get_xdoor_desc
//
// Return hxdoor_id as generic rxdoor_descriptor.
//
void
hxdoor::get_xdoor_desc(rxdoor_descriptor &xd)
{
	hxdoor_id &hxdid = (hxdoor_id &)xd;

	hxdid.xdid = rxdoor::server_xdoor;
	hxdid.serviceid = get_serviceid();
}

//
// marshal
//
// Send a hxdoor as argument to invocation or as return value. This
// method puts the hxdoor representation in the marshalstream. This
// method is called while sending an hxdoor to another domain.
//
void
hxdoor::marshal(ID_node &node, MarshalStream &sms, Environment *)
{
	hxdoor_service *hsp = get_service();
	lock();
	//
	// Wait until marshals are unblocked. They are blocked if the
	// service is being shutdown. They are also blocked on the
	// primary briefly during switchover.
	//
	while (hsp->marshals_blocked() && members::the().still_alive(node)) {
		get_cv().wait(&get_lock());
	}

	//
	// We don't update marshalsmade until after we sleep. This
	// prevents us from being deleted while we are
	// asleep.
	//
	if (i_am_primary()) {
		ASSERT(has_client_ref());
		standard_xdoor_server::do_marshal(node, sms);
	} else {
		foreignrefs++;
		CL_PANIC(foreignrefs != 0);

		marshalsmade++;
		CL_PANIC(marshalsmade != 0);

		// ask the kit to marshal this xdoor.
		get_kit().marshal_xdoor(sms, this);
	}

	unlock();
}

//
// marshal_local
//
// Send a hxdoor as argument to invocation or as return value. This
// method is called when sending an hxdoor to the same domain. This
// does not actually put the hxdoor representation in the
// marshalstream but just adjusts counts to indicate that the hxdoor
// has one more reference.
//
void
hxdoor::marshal_local(Environment *e)
{
	if (i_am_primary()) {
		//
		// standard_xdoor_server's marshal_local() method will
		// check for REVOKED or REVOKED_PENDING. These flags
		// are n/a for hxdoors and hence no exception will be
		// returned.
		//
		standard_xdoor_server::marshal_local(e);
	} else {
		// Same as standard_xdoor_client::marshal_local(e);
		lock();
		marshalsmade++;
		// increment unmarshal count to match handler
		umarshout++;
		CL_PANIC(umarshout != 0);
		unlock();
	}
}

//
// marshal_roll_back
//
// This hxdoor had been marshalled and so has its counts incremented
// to account to the reference in transit. The system decided not to
// send the reference so decrement the counts to indicate that there
// is not a reference in transit anymore.
//
void
hxdoor::marshal_roll_back(ID_node &node, MarshalStream &rms)
{
	RMA_DBG(("hxdoor::marshal_roll_back %p\n", this));
	ASSERT(lock_held());
	//
	// Read in the extra boolean that indicates whether the door
	// is marshalled by primary or secondary.
	//
	if (rms.get_boolean()) {
		standard_xdoor_server::marshal_roll_back(node, rms);
	} else {
		if (--foreignrefs == 0) {
			get_cv().broadcast();
			handle_possible_unreferenced();
		}
	}
}

//
// marshal_received
//
// Hxdoor reference sent to some node has been acknowledged by that
// node. Decrement counts to indicate that there is no reference in
// transit.
//
void
hxdoor::marshal_received()
{
	standard_xdoor_server::marshal_received();
}

//
// marshal_cancel
//
// Called when the marshaling is canceled.  The marshal count is
// reconciled with that of the handler.  It is possible that this will
// allow the xdoor to be deleted.
//
void
hxdoor::marshal_cancel()
{
	standard_xdoor_server::marshal_cancel();
}

//
// unmarshal
//
//   Hxdoor unmarshal method is different than standard xdoor
//   unmarshal.  In case where this node is a secondary, the hxdoor is
//   not deleted if all clients go away. So it cannot depend on the
//   fact that it was newly created, to decide if needs to send
//   register-unregister messages to the server. It makes decision
//   based on whether or not it has client references. Hxdoor on
//   primary is created with HAS_CLIENT_REF flag set. This flag is set
//   on the secondary if it is also a client and for client hxdoor
//   that is not secondary if it marshals this hxdoor to some other
//   client. UNREG_NEEDED bit is set if the current unmarshal causes
//   the hxdoor to send a register message to the primary.
//
void
hxdoor::unmarshal(ID_node &from_node, MarshalStream &rms, bool, Environment *)
{
	hxdoor_service	*hsp = get_service();
	bool		new_ref = false;
	bool		from_primary = rms.get_boolean();
	bool		server_new = false;

	ASSERT(lock_held());
	//
	// This is a new reference if this hxdoor does not have local
	// reference and also does not have another unmarshal in
	// progress. An unmarshal is in progress if umarshout > 0,
	// which will happen if a reference has been unmarshalled at
	// xdoor level but not yet at the handler layer. When it gets
	// unmarshalled at the handler layer, it will set
	// HAS_CLIENT_REF flag. The hxdoor may not have any local
	// references, but it may have marshalled this reference to
	// another node, therefore foreignrefs != 0. In this case we
	// need to simulate that the client/secondary hxdoor has not
	// been 'deleted' yet.
	//
	if ((!has_client_ref()) && (foreignrefs == 0)) {
		new_ref = true;
	}

	if (from_primary) {
		server_new = rms.get_boolean();
	}

	hxdoor_id hxdid;
	get_xdoor_desc(hxdid);

#ifdef HXD_DEBUG
	RMA_DBG(("%p hxd::unm %d.%d new_ref=%d, srvr_new=%d fr_pri=%d\n", this,
	    hxdid.serviceid, hxdid.xdid, new_ref, server_new, from_primary));
#endif

	if (!hsp->primary_known()) {
		//
		// We are disconnecting or disconnected from the current
		// primary as part of a switchover or failover.  Here we don't
		// want to register with the primary. If we have started
		// reconnecting again, then the reference counting stuff
		// is in the process of being enabled, so we go through
		// the normal path below.
		//
		if (from_primary && server_new) {
			//
			// Undo the implicit registration done by
			// the primary when it sent this.
			//
			refcount::the().add_unreg(from_node,
			    (rxdoor_descriptor &)hxdid);
		}
	} else {
		//
		// We initialized from_primary from the marshal stream,
		// so that we knew whether or not to unmarshal the
		// value of server_new.  Now we are actually going to
		// proceed with the reference counting protocol and we
		// need to do that with respect to the current primary,
		// which may not correspond to the opinion at marshal
		// time.
		//
		// If we are receiving from a node that is now primary
		// and didn't know it, then we may need to register
		// with it (it didn't set the bit on marshal). If we
		// are receiving from a node that was primary and is
		// not now, then we are guaranteed that it didn't set
		// the bit (because such marshals would have completed
		// while we are disconnected due to the wait in
		// become_secondary()).
		//
		from_primary =
			ID_node::match(from_node, get_server_node());

		// server_new must be false if from_primary is false.
		CL_PANIC(from_primary || !server_new);

		//
		// This is the common case.
		//
		// This part is reached in the steady state and during
		// the time that a new primary has been selected but
		// this hxdoor may or may not have finished
		// reconnecting. This is the interval after
		// disconnect_client() has been called but before the
		// door has reconnected. The state is indicated by
		// HS_RECONNECTING bit set in the hxdoor_service. If
		// the door has client references, an unreg message
		// must have been sent at the end of
		// disconnect_client() causing UNREG_NEEDED bit to be
		// reset. Later on when the door reconnects, it will
		// send a reg message causing the corresponding bit on
		// the server to be set.
		//
		//
		// The following code is equivalent to the bulk of
		// standard_xdoor_client::unmarshal().
		//
		if (from_primary) {
			//
			// The reference counting algorithm is optimized for
			// the (server_new && new_ref) and (!server_new &&
			// !new_ref) cases. In those cases we need to do
			// nothing. The other combinations of server_new and
			// new_ref need to queue a reference counting job to
			// make up for the incorrect assumptions made by the
			// primary when it marshaled.
			//
			if (server_new && !new_ref) {
				//
				// The primary didn't know about a
				// reference on this node, so it
				// accounted for one. We have queued a
				// reg req when we received a ref from
				// a non-primary node. This will
				// result in primary thinking that we
				// have 2 references. So send an unreg
				// req to primary so it does not count
				// us twice.
				//
				refcount::the().add_unreg(from_node,
				    (rxdoor_descriptor &)hxdid);

			} else if (!server_new && new_ref) {
				//
				// The primary thought there
				// was a reference on this
				// node, but there isn't. We
				// need to tell it about the
				// reference.
				//
				refcount::the().add_reg(from_node,
				    (rxdoor_descriptor &)hxdid, from_node);

			} else if (server_new && new_ref) {
				//
				// do nothing - states match
				// if (new_ref) below will set
				// UNREG_NEEDED flag.
				//
			} else {
				// !server_new && !new_ref case
				// again states match so do nothing
			}

		} else if (new_ref && (!i_am_primary())) { // not from primary
			//
			// We received a new reference from a
			// non-primary node. We need to register it,
			// unless we are primary ourselves.
			//
			refcount::the().add_reg(get_server_node(),
			    (rxdoor_descriptor &)hxdid, from_node);
		}
		if ((!i_am_primary()) && (new_ref)) {
			flags |= UNREG_NEEDED;
		}
	}

	umarshout++;
	CL_PANIC(umarshout != 0);
}

void
hxdoor::marshal_xdid(MarshalStream &ms)
{
	hxdoor_id hxdid;
	get_xdoor_desc(hxdid);

	hxdid._put(ms);

	if (i_am_primary()) {
		ms.put_boolean(true);
	} else {
		ms.put_boolean(false);
	}
}

//
// accept_unreferenced
//
// The replica handler is going away because the implementation object
// is going away. So it is time for the hxdoor to also go away.
//
void
hxdoor::accept_unreferenced()
{
	CL_PANIC(marshalsmade == 0);

	//
	// When unreference was delivered to the implementation object,
	// this flag would have been set.
	//
	CL_PANIC((flags & UNREF_PENDING) != 0);

	bucketp->erase(this);

	delete this;
}

//
// reject_unreferenced
//
// The handler rejected an unreference at this time.
//
void
hxdoor::reject_unreferenced(uint_t handler_marshal_count)
{

	// Show that an unreference no longer is pending
	CL_PANIC((flags & UNREF_PENDING) != 0);
	flags &= ~(UNREF_PENDING | MARSHAL_DURING_UNREF);

	//
	// A secondary does not give out references to itself.
	// Therefore an unref notice should only come up from the hxdoor
	// when this is the primary.
	//
	CL_PANIC(i_am_primary());

	//
	// Conditions may again call for an unreference.
	// For example, a reference given out while processing the unref
	// notice may already have gone away.
	//
	if (handler_marshal_count == marshalsmade) {
		handle_possible_unreferenced();
	}
}

//
// deliver_unreferenced
//
// The hxdoor is being unreferenced. Check with the handler and let it
// know if the xdoor can be deleted.
//
void
hxdoor::deliver_unreferenced()
{
	uint_t		handler_marshal_count;
	bool		xdoor_unref = false;
	handler		*handp = get_server_handler();
	os::mutex_t	*lckp = &get_lock();

	// begin_xdoor_unref acquires the handler lock
	handler_marshal_count = handp->begin_xdoor_unref();
	lckp->lock();

#ifdef HXD_DEBUG
	RMA_DBG(("hxd::del_unref %p svc id = %d "
	    "hdlr_mc = %d, marshalsmade = %d."
	    "flags = %x\n", this, get_service()->get_service_id(),
	    handler_marshal_count, marshalsmade, flags));
#endif
	CL_PANIC(flags & (PRIMARY | UNREF_PENDING));
	//
	// If the service is becoming_secondary, this hxdoor should not be
	// unreferenced.
	//
	if (get_service()->ignore_unrefs()) {
		RMA_DBG(("service %d disabled unref of %p\n",
		    get_service()->get_service_id(), this));

		xdoor_unref = false;
		flags &= ~(UNREF_PENDING | MARSHAL_DURING_UNREF);
		lckp->unlock();
		handp->end_xdoor_unref(xdoor_unref);
		return;
	}

	if ((handler_marshal_count == marshalsmade) &&
	    ((flags & (MARSHAL_DURING_UNREF | HAS_CLIENT_REF)) == 0)) {
		//
		// The handler has not marshaled this xdoor or added a
		// local reference since we queued the unreferenced
		// notification. We can now tell the handler that it
		// can delete us.
		//
		xdoor_unref = true;
	} else {
		RMA_DBG(("%p: del_unref not accept_unref()\n", this));
		flags &= ~(MARSHAL_DURING_UNREF | UNREF_PENDING);

		if (handler_marshal_count == marshalsmade) {
			handle_possible_unreferenced();
		}
	}
	lckp->unlock();
	handp->end_xdoor_unref(xdoor_unref);
}

//
// clear_unreferenced
//
// The handler has performed last_unref.
// This method enables the hxdoor to generate more unreference events.
// An unreference spin cycle would result, if this method
// called handle_possible_unreferenced.
//
void
hxdoor::clear_unreferenced()
{
	os::mutex_t &lck = get_lock();
	lck.lock();

	// Show that an unreference no longer is pending
	CL_PANIC((flags & UNREF_PENDING) != 0);
	flags &= ~(UNREF_PENDING | MARSHAL_DURING_UNREF);

	//
	// The hxdoor differs from other xdoors in its response to the command
	// reject_unreference. Unlike other xdoors, the hxdoor must remain
	// in existence as long as the replica_handler exists, either
	// as a primary or secondary. The replica_handler just told us that it
	// refused to go away.
	//
	// The hxdoor conditions for sending an unref notice to the handler
	// are probably still true. If this method called the method
	// handle_possible_unreferenced, another unref notice would be sent
	// to the handler if the hxdoor unreference conditions were still true.
	// The object _unreferenced method would reject the unref notice again.
	// Then this cycle would repeat indefinitely.
	//
	// Under this approach, the hxdoor will not send another unref notice
	// to the handler until after some action has occurred at the
	// hxdoor level. This avoids the spin cycle described above.
	//
	lck.unlock();
}

//
// revoked
//
// Hxdoors cannot be revoked
//
void
hxdoor::revoked()
{
}

//
// unref_by_ckpt
//
// Called on secondary hxdoor to delete it when all references to the
// primary have gone away and the primary is being unreferenced.
//
void
hxdoor::unref_by_ckpt()
{
	RMA_DBG(("%p: unref_by_ckpt %d.%d\n", this, get_serviceid(),
	    rxdoor::server_xdoor));

	ASSERT(!lock_held());

#ifdef DEBUG
	// Need to lock before checking private data.
	lock();
	// Must be ready to go away.
	ASSERT((umarshout == 0) &&
	    (handlers[0] == NULL || handlers[1] == NULL));

	//
	// The flag will mostly be SECONDARY. During reconfiguration
	// when this checkpoint is flushed by the new primary, the flag
	// may be PRIMARY.
	//
	ASSERT(flags & (SECONDARY | PRIMARY));

	// Must have kernel handler - we only support kernel ha obj for now.
	ASSERT(get_server_handler());

	ASSERT(marshalsmade == 0);
	unlock();
#endif // DEBUG

	//
	// This will deliver unreferenced to the impl obj, which will be
	// deleted. That deletion will trigger the handler to call
	// remove_secondary_obj() on this hxdoor, which will cause it to be
	// deleted.
	//
	((replica_handler *)get_server_handler())->unref_by_ckpt();
}

//
// post_extract_done
//
// Hxdoor requires additional work after extract_done().
//
void
hxdoor::post_extract_done(handler_type type)
{
	if (type == Xdoor::KERNEL_HANDLER) {
		flags |= rxdoor::HAS_CLIENT_REF;
	}
}

//
// client_unreferenced
//
// Supports calls from client handlers when all local proxies have
// been released.  It may be called on a server xdoor if either the
// client or the server is at user level; i.e., one of the handlers is
// the gateway handler.
//
bool
hxdoor::client_unreferenced(handler_type type, uint_t marshcnt)
{
	os::mutex_t *lckp = &get_lock();
	lckp->lock();

	RMA_DBG((
	    "%p hxd::clnt_unref(%d) = %d type=%d fl=%x um_h[%d]=%d umo-%d\n",
	    this, marshcnt, marshalsmade - marshcnt, type, flags, type,
	    umarsh_handler[type], umarshout));

	ASSERT(marshcnt <= marshalsmade);
	marshalsmade -= marshcnt;

	ASSERT(handlers[type] != NULL);

	if (umarsh_handler[type] > 0) {
		lckp->unlock();
		return (false);
	}

	if (type == Xdoor::KERNEL_HANDLER) {
		// We no longer have a client reference from the kernel handler.
		ASSERT(flags & HAS_CLIENT_REF);
		flags &= ~HAS_CLIENT_REF;
		//
		// Don't clear out handler if this is a primary or secondary.
		//
		if ((flags & (PRIMARY|SECONDARY)) == 0) {
			handlers[type] = NULL;
		}
	} else {
		// Nothing special for user handlers, since they can only be
		// clients for now.
		handlers[type] = NULL;
	}

	// Check if xdoor should be released
	handle_possible_unreferenced();
	lckp->unlock();
	return (true);
}

//
// make_oneway_header
//
// Oneways are not supported by hxdoors
//
#if (SOL_VERSION >= __s10) && !defined(UNODE)
void
hxdoor::make_oneway_header(uint_t, sendstream *)
#else
void
hxdoor::make_oneway_header(sendstream *)
#endif
{
	ASSERT(0);
}

//
// make_twoway_header
//
// Put in hxdoor information to a twoway invocation header.
//
#if (SOL_VERSION >= __s10) && !defined(UNODE)
void
hxdoor::make_twoway_header(uint_t cluster_id, sendstream *sendstreamp,
    ushort_t slot)
#else
void
hxdoor::make_twoway_header(sendstream *sendstreamp, ushort_t slot)
#endif
{
	rxdoor_twoway_header *rx_hdrp = ((rxdoor_twoway_header *)sendstreamp->
	    make_header(sizeof (rxdoor_twoway_header)));

	rx_hdrp->xdid = rxdoor::server_xdoor;
	rx_hdrp->slot = slot;
	rx_hdrp->svcid = get_serviceid();
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	rx_hdrp->zone_cluster_id = cluster_id;
#endif
}

//
// request_twoway
//
// A request from a local client. It may be destined locally or remotely.
// We forward remote request through the underlying xdoor. Local requests
// go directly to the handler, since there may not even be an underlying
// door for them.
//
ExceptionStatus
hxdoor::request_twoway(invocation &inv)
{
	hxdoor_service	*hsp = get_service();
	ExceptionStatus	result = CORBA::NO_EXCEPTION;
	Environment	*e = inv.get_env();
	bool		wait_for_freeze = false;

	ASSERT(!inv.se->get_invo_mode().is_oneway());
	CL_PANIC(!e->exception());

	if (i_am_primary()) {
		// Local invocation.
		rxdoor::convert_local_stream(inv);
		if (e->exception()) {
			RMA_DBG(("convert_local_stream exception\n"));
			ASSERT(e->sys_exception());
			result = CORBA::LOCAL_EXCEPTION;
		} else {
			// Send the request to the handler.
			get_server_handler()->handle_incoming_call(inv);

			//
			// For local invocations with exceptions the
			// exception is marshaled in send_reply.
			// We return NO_EXCEPTION in this case.
			// XXX We should probably make this
			// more consistent with the remote case,
			// but that requires being able to figure
			// out that there was an exception.
			//

			if (e->get_freeze_retry()) {
				//
				// This is set in send_reply and indicates
				// that we need to retry after waiting for
				// a freeze.
				//
				RMA_DBG(("%p hxdoor::request_twoway: "
				    "wait_for_freeze local\n",
				    this));
				wait_for_freeze = true;
			}
		}
	} else {
		result = rxdoor::request_twoway(inv);

		if (result == CORBA::LOCAL_EXCEPTION) {
			ASSERT(e->sys_exception());
			if (CORBA::COMM_FAILURE::_exnarrow(e->exception())) {
				// The underlying xdoor failed, we indicate to
				// the handler that it needs to retry from the
				// top.
				e->system_exception(CORBA::RETRY_NEEDED(0,
				    CORBA::COMPLETED_MAYBE));
				RMA_DBG(("%p hxdoor::LOCAL_EXCEPTION retry\n",
				    this));
			}
		} else if (result == CORBA::RETRY_EXCEPTION) {
			//
			// The primary asked us to retry.
			//

			RMA_DBG(("%p hxdoor::request_twoway: wait_for_freeze "
			    "remote\n", this));
			wait_for_freeze = true;
		}
	}

	if (wait_for_freeze) {
		//
		// The primary raised an exception because it is freezing. That
		// means that we will be asked to freeze soon. Block here until
		// we are requested to freeze.
		//
		// Note that we need to block before we call client_invo_done,
		// because if we called it first, then the freeze could come
		// and go before we call wait_for_frozen() and we would block
		// until the next freeze, which could be a long time in the
		// future.
		//
		get_service()->wait_for_freeze_request();

		//
		// The marshal stream contains the exception indicating
		// that a retry needs to be done by the handler.
		//
		result = CORBA::REMOTE_EXCEPTION;
	}

	// Decrement the invo count we incremented in reserve_resources()
	hsp->client_invo_done(false);

	//
	// Because of the way freeze_lock works, the freezing thread can't
	// complete until we call client_invo_done(). To avoid hogging the CPU
	// in a retry loop, we must make sure that the freeze actually runs
	// before we return.
	//
	if (wait_for_freeze) {
		get_service()->wait_for_freeze_complete();
	}
	return (result);
}

//
// reserve_resources
//
// This supports both local and remote communications. It is a local
// invocation if this hxdoor is the primary. Returns sendstream
// pointer, which can be NULL in case of error.  This method is used
// for invocation messages and not reply messages.
//
sendstream *
hxdoor::reserve_resources(resources *resourcep)
{
	MC_PROBE_0(hxdoor_reserve_start, "clustering HA hxdoor", "");

	hxdoor_service	*hsp = get_service();
	sendstream	*sendstreamp;
	//
	// FAULT POINT
	// This generic fault point will trigger a fault before the underlying
	// xdoor is chosen for the target node.  This fault allows us to test
	// the freezing/recovery mechanism of the framework when the target
	// node dies/slows down/etc.
	//
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_1,
		FaultFunctions::generic);

	// When the service is frozen, wait until the service is not frozen
	while (hsp->start_client_invo()) {
		//
		// The service is not frozen and is up.
		//
		if (i_am_primary()) {
			//
			// We are primary.  Use a local sendstream.
			// This usually happens for invocations from the
			// gateway, but it can also occur from a kernel handler
			// if the handler had not yet become primary when the
			// request started.
			//
			// CURRENTLY, local invocations do not use an rxdoor
			// level header. So no resources are required at the
			// rxdoor level. Thus, we jump down to the transport
			// level.
			//
			sendstreamp =
			    standard_xdoor_server::reserve_resources(resourcep);

			ASSERT(resourcep->get_env()->exception() == NULL);
			return (sendstreamp);
		}

		// Primary is not on the same node. Remote invocation
		//
		// HA requests use the address of the service object
		// to identify request group membership.
		//
		resourcep->set_request_group(hsp);

		// hxdoors support 2-way invocations only.

		resourcep->add_send_header(sizeof (rxdoor_twoway_header));
		resourcep->set_node(get_server_node());

		// Have the lower protocol layers specify its requirements
		sendstreamp = orb_msg::reserve_resources(resourcep);

		if (sendstreamp != NULL) {

			// FAULT POINT
			// This generic fault point allows us to trigger a
			// fault after the underlying xdoor has been chosen,
			// therefore committing the framework to attempt to
			// communicate with a particular node.  We may now
			// kill/slow down/other that target node.
			// Retries will trigger this fault point again.
			// (CAREFUL: you might want to use a *_once fault)
			FAULTPT_HA_FRMWK(
				FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_2,
				FaultFunctions::generic);

			// Success
			return (sendstreamp);
		}
		Environment	*e = resourcep->get_env();
		ASSERT(e->sys_exception());
		// Pass non-system_exceptions back to the caller.
		if (CORBA::COMM_FAILURE::_exnarrow(e->exception()) == NULL) {
			// Report the failure to the caller.
			hsp->client_invo_done(false);
			return (NULL);
		}
		//
		// The current primary is dead.  Loop around. By the
		// time we leave hsp->start_client_invo() next time,
		// we will either have a new primary or the service
		// will be marked as dead, causing us to fall out of
		// the loop.
		//
		e->clear();
		hsp->client_invo_done(true);

		// Release any resources reserved for the old destination.
		resourcep->resource_release();
	}

	// The service has died
	resourcep->get_env()->system_exception(CORBA::COMM_FAILURE(0,
	    CORBA::COMPLETED_NO));

	return (NULL);
}

//
// release_resources
//
// Called if reserve_resources is called but the request will not be sent.
//
void
hxdoor::release_resources(resources *)
{
	// Undo the start_client_invo in reserve_resources.
	get_service()->client_invo_done(false);

}

//
// reserve_reply_resources
//
// Identify resources needed for invocation reply. The server hxdoor
// does not know where the reply should go. The upper layer must
// specify the destination node.
//
sendstream *
hxdoor::reserve_reply_resources(resources *resourcep)
{
	//
	// standard_xdoor_server::reserve_reply_resources() checks if
	// it is a local invocation and allocates the right amount of
	// resources.  Only a primary sends reply for an invocation.
	//
	ASSERT(i_am_primary());

	return (standard_xdoor_server::reserve_reply_resources(resourcep));
}

//
// send_reply
//
// Called on the primary hxdoor to send reply back to the client.
//
void
hxdoor::send_reply(service &serv)
{
	ASSERT((i_am_primary()));

	// FAULT POINT
	// Fault before the invocation reply can be sent.
	// The retry of this invocation will trigger this fault point again.
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY,
		FaultFunctions::generic);

	if (serv.get_src_node().ndid != orb_conf::node_number()) {
		//
		// Reply messages never block in flow control.
		// So no need to specify request group.
		//
		rxdoor::send_reply(serv);
	} else {
		//
		// This is a local reply.
		//
		// If we have a RETRY_NEEDED exception, then we don't want to
		// go back to userland and enter a retry spin loop. Besides
		// being inefficient, this can cause serious problems if the
		// userland process is running in real time mode, since it
		// prevents any further progress by other threads on this cpu.
		// We only get this exception if the service is in the process
		// of freezing. We are guaranteed to get a
		// freeze_service_invos() request on this node in the near
		// future. Se we set a flag to ask hxdoor::request_twoway()
		// to wait for the freeze to come in and be processed before
		// allowing the retry to occur.
		//
		// We need to block in hxdoor::request_twoway() rather than
		// here because CORBA::PRIMARY_FROZEN exceptions may be sent by
		// the skeleton, with the primary invo lock held at the handler
		// level. That hold on the lock must be released before the
		// freeze_primary() request will complete on the primary.  That
		// in turn holds up the freeze_service_invos() request that we
		// want.
		//

		Environment *e = serv.get_env();
		if (e->exception() &&
		    CORBA::PRIMARY_FROZEN::_exnarrow(e->exception())) {
			RMA_DBG(("%p hxdoor::send_reply: set_freeze_retry\n",
			    this));
			//
			// We tag the environment to inform the
			// hxdoor::request_twoway code that it should block.
			// Note that we must use this special freeze_retry
			// field in the environment for this because the
			// exception is deleted as part of sending the reply
			// and we don't have any other way of passing
			// information back.
			//
			// Ultimately this should be done using an hxdoor reply
			// header see 4114937.
			//
			e->set_freeze_retry();
		} else {
			e->clear_freeze_retry();
		}

		// Local requests are not transmitted and do not need an xdoor
		rxdoor::send_reply_local(serv);
	}
}

//
// have_client_ref
//
// Called by the handler when the primary performs a zero-to-one transition.
// This means that a local client was just created.
// Eventually, when we support more than one handler, we will need to keep
// track of this flag on a per handler basis.
//
void
hxdoor::have_client_ref()
{
	lock();
	ASSERT(i_am_primary());
	flags |= HAS_CLIENT_REF;
	unlock();
}

//
// add_secondary_obj
//
// Changes the hxdoor state from spare to secondary. It is called by
// replica_handler::add_impl() to tell the hxdoor that it has added a
// secondary object.
//
void
hxdoor::add_secondary_obj()
{
	lock();
	become_secondary();
	RMA_DBG(("%p hxdoor::add_secondary_obj() flags = %x\n",
	    this, flags));
	unlock();
}

//
// remove_secondary_obj
//
// Converts an hxdoor from secondary to a spare.
// replica_handler::rem_impl() calls this method to tell has
// removed secondary object. If the hxdoor does not have any client
// references, it will remove the handler and be ready to be
// unreferenced.
//
bool
hxdoor::remove_secondary_obj()
{
	os::mutex_t *lckp = &get_lock();
	lckp->lock();

	become_spare();

	if ((flags & HAS_CLIENT_REF) ||
	    umarsh_handler[Xdoor::KERNEL_HANDLER] > 0) {
		// Need to keep the handler around.
		lckp->unlock();
		return (false);
	} else {
		// We can remove the handler and may want to delete ourselves.
		handlers[KERNEL_HANDLER] = NULL;
		handle_possible_unreferenced();
		lckp->unlock();
		return (true);
	}
}

const char*
hxdoor::xdoor_type()
{
	if (i_am_primary()) {
		return ("hxd_s");
	} else {
		return ("hxd_c");
	}
}

//
// get_service
//
// Returns pointer to the hxdoor_service this hxdoor belongs to.
//
hxdoor_service *
hxdoor::get_service()
{
	return ((hxdoor_service *)(rxdoor::bucketp->rnp));
}

//
// handle_possible_unreferenced - determine whether all activity on this
// xdoor has ceased.
//
void
hxdoor::handle_possible_unreferenced()
{
	//
	// Do we have any local clients, in transit marshals,
	// incoming invocations or unmarshals in progress
	//
	if ((services != 0) || has_foreignrefs() || (has_client_ref())) {
		return;
	}
	//
	// Only allow one outstanding unreference.
	//
	if ((flags & UNREF_PENDING) != 0) {
		return;
	}

	if (i_am_primary()) {
		if (!get_service()->ignore_unrefs()) {
#ifdef HXD_DEBUG
			RMA_DBG(("%p: hdl_pos_unref adding: "
			    "hdlr = %p\n", this, get_server_handler()));
#endif
			//
			// This assert is perhaps incorrect, because
			// the hxdoor may decide on its own to go
			// away. So it should be fine as long as it
			// matches handler's marshal count. This check
			// will be done by deliver_unreference()
			// before it decides to delete the hxdoor.
			//
			// ASSERT(marshalsmade == 0);

			// Show that an unreference is pending
			flags |= UNREF_PENDING;

			// Schedule unreferenced notification for the hxdoor
			unref_threadpool::the().defer_unref_processing(this);
		}
	} else {
		unregister_from_primary();
		//
		// If this is a secondary, we need to keep the hxdoor
		// around even if clients  have gone away.
		//
		if (flags & SECONDARY) {
			RMA_DBG(("%p hxd sec unreg\n", this));
			return;
		}
		//
		// This is a Client not on a secondary node.
		// So the hxdoor needs to be deleted.
		//
		RMA_DBG(("%p clnt hxd\n", this));
		bucketp->erase(this);
		delete this;
	}
}

//
// get_unref_handler
//
// Return the server handler for use by the unref_task.
//
handler *
hxdoor::get_unref_handler()
{
	ASSERT(get_server_handler() != NULL);
	return (get_server_handler());
}

//
// get_defer_task_tag
//
// Implements the defer_task virtual method for hxdoor:
// defer_task <- unref_task <- hxdoor
// Returns the tag for the unref_task (actually a defer_task).
// Currently only the hxdoor uses this tag. The HA framework uses
// the tag to find pending unref_tasks from hxdoors belonging to a service.
//
uint_t
hxdoor::get_defer_task_tag()
{
	return ((uint_t)get_serviceid());
}

//
// unref_rejected
//
//   This hxdoor was removed from the unref_threadpool because it
//   should not be unreferenced. Call reject_unreferenced() for this
//   hxdoor. We need to know the handler's marshal count in order to
//   do so.
//
void
hxdoor::unref_rejected()
{
	handler		*handp = get_server_handler();
	os::mutex_t	*lckp = &get_lock();

	//
	// begin_xdoor_unref acquires the handler lock and returns handler's
	// marshal count.
	//
	uint_t handler_marshal_count = handp->begin_xdoor_unref();
	lckp->lock();

	reject_unreferenced(handler_marshal_count);

	lckp->unlock();
	// unlock handler and tell it that xdoor is not being unreferenced.
	handp->end_xdoor_unref(false);
}

//
// register_with_primary
//
//  Called from hxdoor_service::connect_to_primary() called in case of
//  switchover and failover. The hxdoor has a client and would
//  have called unregister_from_primary() in disconnect_client(). Now
//  it must register with the new primary.
//
void
hxdoor::register_with_primary(ID_node &primary_node)
{
	hxdoor_id	hxdid;

	get_xdoor_desc(hxdid);

	refcount::the().add_reg(primary_node,
	    (rxdoor_descriptor &)hxdid, orb_conf::current_id_node());

	flags |= UNREG_NEEDED;
}

//
// unregister_from_primary
//
// Unregister from primary if we are registered. This method is called
// when the last reference to this hxdoor goes away.
//
void
hxdoor::unregister_from_primary()
{
	CL_PANIC(!i_am_primary());

	if (flags & UNREG_NEEDED) {
		hxdoor_id	hxdid;

		get_xdoor_desc(hxdid);

		refcount::the().add_unreg(get_server_node(),
		    (rxdoor_descriptor &)hxdid);

		flags &= ~UNREG_NEEDED;
	}
}

//
// disconnect_client
//
// Reset the UNREG_NEEDED flag. We do not actually send UNREG messages
// but just pretend that we did.
//
void
hxdoor::disconnect_client()
{
	if (!i_am_primary() && (flags & UNREG_NEEDED)) {
		flags &= ~UNREG_NEEDED;
	}
}

#ifdef DEBUG
void
hxdoor::mark_down()
{
}
#endif

//
// become_primary
//
// Tells the hxdoor to become primary.
//
void
hxdoor::become_primary()
{
	flags |= PRIMARY;
	flags &= ~SECONDARY;
	RMA_DBG(("%p hxdoor::become_primary() flags=%x masks=%llx.%llx\n",
	    this, flags, refmask.bitmask(), extramask.bitmask()));

	ASSERT(get_server_handler() != NULL);
	get_server_handler()->become_primary_event();
}

//
// become_secondary
//
// Tells the hxdoor to become secondary.
//
void
hxdoor::become_secondary()
{
	flags |= SECONDARY;
	flags &= ~PRIMARY;
	//
	// Wait for in-transit references to reach their destinations
	// and be accounted for before reconnect to the new primary
	// begins. hxdoor_service::become_secondary() already blocks
	// marshals. So we prevent new foreign references being
	// generated.
	//
	while (foreignrefs != 0) {
		RMA_DBG(("%p hxdoor::become_secondary() flags=%x, "
		    "foreignrefs=%d\n", this, flags, foreignrefs));

		get_cv().wait(&get_lock());
	}
	refmask.empty();
	extramask.empty();


	ASSERT(get_server_handler() != NULL);
	get_server_handler()->become_secondary_event();
}

//
// become_spare
//
// Tells hxdoor to become spare.
//
void
hxdoor::become_spare()
{
	flags &= ~SECONDARY;
}

//
// has_client_ref
//
// Returns true if there is a client reference to this hxdoor. We have a client
// reference if there is a reference through the gateway or if there is a
// reference through a kernel handler (indicated by the HAS_CLIENT_REF flag) or
// we have unmarshaled the xdoor, but the handler has not been unmarshaled
// yet. We need to keep track of whether we have a client reference so that we
// know if we need to reconnect during a reconfiguration.
//
bool
hxdoor::has_client_ref()
{
	bool hcf = (handlers[Xdoor::USER_HANDLER] != NULL) ||
		((flags & HAS_CLIENT_REF) != 0) || (umarshout != 0);

	return (hcf);
}


//
// local
//
//   Called by rxdoor::request_twoway() to determine if this is a
//   local invocation.  Returns true if this hxdoor is the primary. An
//   HA invocation is local only if primary is on the same node.
//
//   NOTE: This definition of local() is not same as that for other
//   rxdoors. Other rxdoors use it to make sure that the server is on
//   the local node. This function is also called by
//   get_server_handler() method to ensure that only the server xdoor
//   is calling this method.
//
//   Hxdoor defines its own get_server_handler() method and does not
//   call local().
//
//   Hxdoor lock is not needed here.  In case of switchover, the
//   hxdoor could become secondary, but before that, the primary will
//   be frozen and the invocation will get an exception.
//
bool
hxdoor::local()
{
	if (i_am_primary()) {
		return (true);
	} else {
		return (false);
	}
}

handler *
hxdoor::get_server_handler()
{
	ASSERT(flags & (rxdoor::PRIMARY | rxdoor::SECONDARY));
	return (rxdoor::handlers[Xdoor::KERNEL_HANDLER]);
}

void
hxdoor::set_server_handler(handler *h)
{
	ASSERT(flags & (rxdoor::PRIMARY | rxdoor::SECONDARY));
	rxdoor::handlers[Xdoor::KERNEL_HANDLER] = h;
}

handler *
hxdoor::get_client_handler()
{
	return (rxdoor::handlers[Xdoor::USER_HANDLER]);
}

//
// server_dead
//
// service state is found by querying the corresponding hxdoor_service.
// Returns true if service is DOWN.
//
bool
hxdoor::server_dead()
{
	bool service_state = false;

	get_service()->lock();
	service_state = (get_service()->get_service_state() == replica::S_DOWN);
	get_service()->unlock();

	return (service_state);
}
