/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)reconnect_object_impl.cc	1.3	08/05/20 SMI"

#include <repl/rma/reconnect_object_impl.h>
#include <repl/rma/rma.h>

reconnect_object_impl::reconnect_object_impl() :
	unref_called(true)
{
#ifdef HXD_DEBUG
	RMA_DBG(("%p reconn obj created\n", this));
#endif
}

void
reconnect_object_impl::_unreferenced(unref_t cookie)
{
	lock.lock();
	if (_last_unref(cookie)) {
		unref_called = true;
		cv.signal();
	}
	lock.unlock();
}

//
// delete_after_unreferenced
//
// Wait for the object to be unreferenced and delete it.
//
void
reconnect_object_impl::delete_after_unreferenced()
{
	lock.lock();

	while (!unref_called) {
		cv.wait(&lock);
	}
#ifdef HXD_DEBUG
	RMA_DBG(("%p: deleting recon obj\n", this));
#endif
	delete this;
}

//
// wait_until_unreferenced
//
// Wait until the object is unreferenced.
//
void
reconnect_object_impl::wait_until_unreferenced()
{
	lock.lock();
	while (!unref_called) {
		cv.wait(&lock);
	}
	lock.unlock();
}
