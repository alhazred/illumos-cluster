/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HXDOOR_SERVICE_H
#define	_HXDOOR_SERVICE_H

#pragma ident	"@(#)hxdoor_service.h	1.12	08/05/20 SMI"

#include <orb/xdoor/rxdoor.h>

// forward declaration
class hxdoor;
class reconnect_object_impl;

class hxdoor_service : public rxdoor_node {
	friend class hxdoor_kit;
public:
	hxdoor_service(replica::service_num_t, const char *service_name);

	~hxdoor_service();

	void	init_service(const replica_int::init_service_info &);

	void	lock();
	void	unlock();
	bool	lock_held();

	void	record_state(replica_int::rma_service_state);

	// Returns the last successful call from reconf on this object
	replica_int::rma_service_state	get_state();

	// Returns the service state based on the last successful reconf call.
	replica::service_state		get_service_state();

	xdoor_id			get_new_objid();
	replica::service_num_t		get_service_id();
	void				update_objid(xdoor_id objid);
	void				store(hxdoor *xdp);

	// Functions used during service reconfiguration.
	// Called on server (primary or secondary).

	// Primary -> Secondary
	void				become_secondary();
	// Secondary -> Primary
	void				become_primary();
	void				become_spare();
	// Primary
	void				disable_unreferenced();
	// Primary
	void				enable_unreferenced();
	bool				shutdown_service();

	// Shutdown the service.
	void				shutdown();

	// block marshals
	void				block_marshals();

	// Prepare for shutdown
	void				shutdown_check_prepare();

	// unblock marshals
	void				unblock_marshals();

	// Called on all clients to drop underlying xdoors
	void				disconnect_clients();

	// Called on all clients to reconnect to the new primary.
	void				connect_to_primary(
					    replica_int::rma_repl_prov_ptr
					    primary);
	//
	// Called on primary on behalf of a client to get a new dummy
	// object for reconnection.
	//
	void	get_reconnect_objects(replica_int::reconnect_object_out rcon_p,
		    replica_int::reconnect_object_out dcon_p);

	// Called on all clients when the service is down.
	void	mark_down();	// All

	// Called from hxdoors which are part of this bucket.
	// Attempt to start a client invocation. If the service is frozen
	// this blocks until it is unfrozen. Returns true if the invocation
	// is permitted, false if the service is down.
	bool	start_client_invo();		// All

	//
	// Called when an invocation is completed.  This must be called
	// for every start_client_invo() call that returns true. The freeze
	// flag is set to true if the service should be marked as frozen. This
	// is set if the underlying xdoor returns an exception, to avoid
	// repeated retry attempts.
	//
	void	client_invo_done(bool freeze);	// All

	// Called on all clients to freeze/unfreeze service
	void	freeze_service_invos();		// All
	void	unfreeze_service_invos();	// All

	// Keep track of the number of standard xdoors. Only used on primary.
	bool	ignore_unrefs();

	void	wakeup_marshals();
	void	freeze_invos_to_dead_primary();
	bool	disconnect_from_dead_primary(cmm::seqnum_t, os::systime *);
	void	disconnect_from_dead_primary_cancel(cmm::seqnum_t);

	void	mark_dead_primary_disconnected_on_all_nodes();
	void	wait_on_cmm_to_disconnect_dead_primary();

	// Has the current cmm reconfiguration been canceled.
	bool	reconfig_canceled(cmm::seqnum_t);

	// Keep track of the number of hxdoors in this service.
	void	hxdoor_created();

	// Returns true if the service is ready to shutdown. It also
	// re-enables marshals.
	bool	shutdown_ready();

	bool	reconnecting();		// Are we reconnecting
	bool	marshals_blocked();	// Are marshals_blocked
	bool	primary_known();	// Do we know the primary node
	bool	is_down();		// Is the service down

	//
	// Used by invocation threads to block until a freeze request
	// comes in.
	//
	void	wait_for_freeze_request();

	//
	// Used by invocation threads to block until a freeze request
	// is completed. By using this they guarantee that the freeze has
	// occurred.
	//
	void	wait_for_freeze_complete();

#ifdef FAULT_RECONF
	// Return true if blocked in step 3 of reconfiguration.
	bool		fi_check_step3_blocked();

	// Unblock reconnects that were blocked by a fault point.
	// This is static and unblocks all blocked reconnects, however the
	// it has not been tested for more than one service at a time.
	static void	unblock_reconnect();

	// Wait for a service to be blocked in reconnect by a fault point.
	static void	wait_for_blocked_reconnect();
#endif

	// Get the serivce name.
	const char *get_name();

#ifdef DEBUGGER_PRINT
	int		mdb_dump_service();
#endif

private:
	// helper function that reconnects hxdoors to new primary.
	void	reconnect_hxdoors();

#ifdef	RMA_DBGBUF
	char	dbg_id[40];
#endif
	os::mutex_t	hs_lck;		// Protects the bucket and its hxdoors.

	freeze_lock	invo_fl;	// coordinates freeze of invos

	// Used on primary to wait for clients to finish reconnecting
	// Used on secondary to to signal completion of reconnect to
	// CMM reconfiguration.
	os::condvar_t	reconnect_cv;	// coordinates reconnection

	replica_int::rma_repl_prov_ptr		primary_prov_p;
	//
	// C++ pointers to reconnect and disconnect objs that the
	// primary creates.
	//
	reconnect_object_impl			*reconnect_objp;
	reconnect_object_impl			*disconnect_objp;
	//
	// Object ref to disconnect_obj on the service primary.  Used
	// by secondaries to disconnect from a primary during
	// switchover.
	//
	replica_int::reconnect_object_ptr	disconnect_p;
	//
	// The last call from reconf on this service
	//
	replica_int::rma_service_state	_sstate;
	// The service name.
	char				*_sname;

	uchar_t			hs_flags;

	enum {
		HS_IGNORE_UNREF 	= 0x01,
		HS_MARSHALS_BLOCKED 	= 0x02,
		HS_PRIMARY_KNOWN	= 0x04,
		HS_PRIMARY 		= 0x10,
		HS_SECONDARY		= 0x20,
		HS_SPARE		= 0x40,
		HS_RECONNECTING		= 0x80
	};

	replica::service_num_t	servid;

	//
	// Highest object id allocated so far.  Only meaningful on the primary.
	//
	xdoor_id	last_object_id;
	//
	// The data_lock can be acquired with the hxdoor lock held.
	// This lock protects n_hxdoors, shutting_down and last_object_id.
	//
	os::mutex_t	data_lock;
	//
	// The last cmm sequence number to be canceled. This is checked during
	// the CMM reconfiguration steps and if it corresponds to the current
	// step then we know that we have been canceled.
	//
	cmm::seqnum_t		canceled_seqnum;

	// Indicates that we are freezing client invocations.
	bool			freezing;
	// Clients block on this to wait for the freeze to occur.
	os::condvar_t		freeze_cv;

	//
	// Used by the service state machine on the replica manager primary to
	// get notified after the dead primary has been disconnected on all
	// nodes by the CMM reconfiguration.
	//
	os::condvar_t	disconnect_dead_primary_on_all_nodes_cv;

	//
	// Indicate if the dead primary has been disconnected on all nodes
	// by the CMM reconfiguration. Only used by failover, not switchover.
	//
	bool		is_dead_primary_disconnected_on_all_nodes;

	// Used by 2-phase check for shutdown
	bool			can_shutdown;

	// Used to decide whether to use new shutdown protocol
	bool			shutdown_prepare_called;

#ifdef FAULT_RECONF
	//
	// Called when we hit a fault point telling us to block a reconnect.
	// Blocks on fi_cv until woken up by a call to unblock_reconnect().
	//
	static void		reconnect_delay();

	// Used to synchronize blocking during fault injection
	static os::condvar_t	fi_cv;
	static os::mutex_t	fi_lock;
	//
	// Set when a thread has this the fault point causing it to block in
	// reconnect. Checked by wait_for_blocked_reconnect().
	//
	static bool		reconnect_blocked;
	//
	// Set when step 3 is blocked waiting for a reconnect to complete.
	// Checked by fi_check_step3_blocked().
	//
	bool			step3_blocked_on_reconnect;
#endif

	// Disallow assignments and pass by value
	hxdoor_service(const hxdoor_service &);
	hxdoor_service &operator = (hxdoor_service &);
};


#include <repl/rma/hxdoor_service_in.h>

#endif	/* _HXDOOR_SERVICE_H */
