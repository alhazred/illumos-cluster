/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  hxdoor_in.h
 *
 */

#ifndef _HXDOOR_IN_H
#define	_HXDOOR_IN_H

#pragma ident	"@(#)hxdoor_in.h	1.40	08/05/20 SMI"

inline
hxdoor_id::hxdoor_id()
	:rxdoor_descriptor()
{
}

inline
hxdoor_id::hxdoor_id(xdoor_id xd, replica::service_num_t sid)
	: rxdoor_descriptor(xd), serviceid(sid)
{
}

inline void
hxdoor_id::_put(MarshalStream &wms)
{
	rxdoor_descriptor::_put(wms);
	wms.put_unsigned_short(serviceid);
}

inline void
hxdoor_id::_get(MarshalStream &rms)
{
	rxdoor_descriptor::_get(rms);
	serviceid = rms.get_unsigned_short();
}


inline uint_t
hxdoor_id::_sizeof_marshal()
{
#ifdef MARSHAL_DEBUG
	//
	// 1 uint32_t for marshal type identifier +
	// 1 ushort_t for serviceid
	//
	return ((uint_t)sizeof (uint32_t) + (uint_t)sizeof (ushort_t) +
	    rxdoor_descriptor::_sizeof_marshal());
#else
	// 1 ushort_t for serviceid
	return ((uint_t)sizeof (ushort_t) +
	    rxdoor_descriptor::_sizeof_marshal());
#endif
}


// The marshal length of an hxdoor is that of the hxdoor_id plus any
// extra stuff added by the underlying standard xdoor.
// XXX should be a better way to do this
inline
hxdoor_kit::hxdoor_kit() :
    rxdoor_kit(HXDOOR_XKID,
    sizeof (hxdoor_id) + standard_xdoor_client::to_client_kit.get_marshal_len())
{
}

inline
hxdoor_manager::hxdoor_manager()
{
}

inline hxdoor_manager &
hxdoor_manager::the()
{
	ASSERT(hxdmgr != NULL);
	return (*hxdmgr);
}

inline bool
hxdoor_manager::lock_held()
{
	return (lck.lock_held());
}

inline void
hxdoor_manager::lock()
{
	lck.lock();
}

inline void
hxdoor_manager::unlock()
{
	lck.unlock();
}

//
// hxdoor methods
//

inline ExceptionStatus
hxdoor::request_oneway(invocation &)
{
	ASSERT(0);
	return (CORBA::NO_EXCEPTION);
}

inline bool
hxdoor::reference_counting()
{
	return (true);
}

#ifdef DEBUG
inline
hxdoor::~hxdoor()
{
	RMA_DBG(("%p ~hxd mrshmd=%d, fr=%d, flags=%x svcs=%d\n",
	    this, marshalsmade, foreignrefs, flags, services));
}
#endif

inline xdoor_id
hxdoor::get_objectid()
{
	return (rxdoor::server_xdoor);
}

inline bool
hxdoor::i_am_primary()
{
	return (flags & rxdoor::PRIMARY);
}

inline bool
hxdoor::must_reconnect()
{
	//
	// It is possible that this hxdoor marshaled a reference.  So
	// foreignrefs > 0. We do not wait for in-transit references
	// to be received on the destination node and the ack to be
	// sent back. The reference counting protocol requires that we
	// stay registered with the server until all references that
	// we have marshaled are unmarshaled by the receiver and the
	// receiver has registered them with the primary. This
	// prevents the object from getting a premature unreferenced
	// notification
	//
	// Consider an umarshal happening during interval where the
	// hxdoor has disconnected from the old primary but has not
	// finished reconnecting with the new one.  If the hxdoor does
	// not have client refs, then new_ref = true.  If the server
	// thought this was a new ref, it would have set corresponding
	// bit in refmask. Now when it is the turn of this hxdoor to
	// get reconnected, it would send another reg message. This
	// would incorrectly cause the extramask bit to be set on the
	// server. So we use the UNREG_NEEDED flag to decide if an
	// hxdoor should reconnect to the new primary. UNREG_NEEDED
	// would be set when the first ref is unmarshalled on this
	// node.  The reconnecting hxdoor must not send another reg
	// message if UNREG_NEEDED is set.
	//
	return ((foreignrefs != 0) ||
		(has_client_ref() && ((flags & UNREG_NEEDED) == 0)));
}

#ifdef DEBUGGER_PRINT

inline handler *
hxdoor::mdb_get_userhandler()
{
	return (rxdoor::handlers[rxdoor::client_handler_index]);
}

inline handler *
hxdoor::mdb_get_kernhandler()
{
	return (rxdoor::handlers[rxdoor::server_handler_index]);
}

#endif

#endif	/* _HXDOOR_IN_H */
