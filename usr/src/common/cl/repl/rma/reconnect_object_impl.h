/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RECONNECT_OBJECT_IMPL_H
#define	_RECONNECT_OBJECT_IMPL_H

#pragma ident	"@(#)reconnect_object_impl.h	1.4	08/05/20 SMI"

#include <h/replica_int.h>
#include <orb/object/adapter.h>
#include <sys/threadpool.h>
#include <sys/dbg_printf.h>

//
// Each hxdoor_service created 2 objects of this type.  One is used on
// primary to wait for clients to finish reconnecting.  The other is
// used on secondary to tell CMM reconfiguration about completion of
// disconnecting from old primary.
//
class reconnect_object_impl : public McServerof<replica_int::reconnect_object> {
public:
	reconnect_object_impl();
	bool	unref_done();
	void	unref_done(bool);
	void	_unreferenced(unref_t);
	void	delete_after_unreferenced();
	void	wait_until_unreferenced();

private:
	bool		unref_called;
	os::mutex_t	lock;
	os::condvar_t   cv;
};

#include <repl/rma/reconnect_object_impl_in.h>

#endif /* _RECONNECT_OBJECT_IMPL_H */
