/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  rma_in.h
 *
 */

#ifndef _RMA_IN_H
#define	_RMA_IN_H

#pragma ident	"@(#)rma_in.h	1.18	08/09/19 SMI"

inline static rma &
rma::the()
{
	ASSERT(the_rmap != NULL);
	return (*the_rmap);
}

inline void
rma::lock()
{
	lck.lock();
}

inline void
rma::unlock()
{
	lck.unlock();
}

inline void
rma_prov_impl::lock()
{
	rpi_lock.lock();
}

inline void
rma_prov_impl::unlock()
{
	rpi_lock.unlock();
}

inline bool
rma::lock_held()
{
	return (lck.lock_held());
}

#endif	/* _RMA_IN_H */
