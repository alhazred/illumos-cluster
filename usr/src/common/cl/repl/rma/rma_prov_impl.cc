/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rma_prov_impl.cc	1.37	08/05/20 SMI"

#include <repl/rma/rma.h>
#include <repl/rmm/rmm.h>


rma_prov_impl::rma_prov_impl(replica_int::handler_repl_prov_ptr provp,
    hxdoor_service *sp) :
	_state(replica_int::RMA_CONSTRUCTED)
{
	ASSERT(sp != NULL);
	lock();
	rpe = replica_int::PROV_NONE;
	RMA_DBG(("%p(%d) rma_prov_impl::rma_prov_impl()\n", this,
	    sp->get_service_id()));
	wrapped = replica_int::handler_repl_prov::_duplicate(provp);
	servicep = sp;
	unlock();
}

void
rma_prov_impl::get_state(replica_int::rma_repl_prov_state &prov_state,
	replica_int::repl_prov_exception &prov_exception, Environment &)
{
	lock();

	if (servicep != NULL) {
		RMA_DBG(("%p(%d) rma_prov_impl::get_state()\n", this,
		    servicep->get_service_id()));
	} else {
		RMA_DBG(("%p(0) rma_prov_impl::get_state()\n", this));
	}

	prov_state = _state;

	prov_exception = rpe;

	unlock();
}

void
rma_prov_impl::become_secondary(bool primary_has_failed, Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::become_secondary()\n", this,
	    servicep->get_service_id()));
	servicep->disable_unreferenced();
	wrapped->become_secondary(primary_has_failed, e);
	_state = replica_int::RMA_BECOME_SECONDARY;
	store_exception(e);
}

void
rma_prov_impl::become_secondary_wrapup(Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::become_secondary_wrapup()\n", this,
	    servicep->get_service_id()));
	servicep->become_secondary();
	_state = replica_int::RMA_BECOME_SECONDARY_WRAPUP;
	store_exception(e);
}

void
rma_prov_impl::add_secondary(
	replica::checkpoint_ptr sec, const char *sec_name, Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::add_secondary()\n", this,
	    servicep->get_service_id()));
	wrapped->add_secondary(sec, sec_name, e);
	_state = replica_int::RMA_ADD_SECONDARY;
	store_exception(e);
}

void
rma_prov_impl::commit_secondary(Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::commit_secondary()\n", this,
	    servicep->get_service_id()));
	wrapped->commit_secondary(e);
	_state = replica_int::RMA_COMMIT_SECONDARY;
	store_exception(e);

	// Inform the rma if we just committed the RM service.
	if (servicep->get_service_id() == RMM_SERVICE_ID) {
		rma::the().rm_in_sync();
	}
}

void
rma_prov_impl::remove_secondary(
	replica::checkpoint_ptr sec, const char *sec_name, Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::remove_secondary()\n", this,
	    servicep->get_service_id()));
	wrapped->remove_secondary(sec, sec_name, e);
	_state = replica_int::RMA_REMOVE_SECONDARY;
	store_exception(e);
}

void
rma_prov_impl::freeze_primary(Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::freeze_primary()\n", this,
	    servicep->get_service_id()));
	wrapped->freeze_primary(e);
	_state = replica_int::RMA_FREEZE_PRIMARY;
	store_exception(e);
}

void
rma_prov_impl::unfreeze_primary(Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::unfreeze_primary()\n", this,
	    servicep->get_service_id()));
	wrapped->unfreeze_primary(e);
	_state = replica_int::RMA_UNFREEZE_PRIMARY;
	store_exception(e);
}

void
rma_prov_impl::become_primary_prepare(
	const replica::secondary_seq &secs, Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::become_primary_prepare() "
	    "received %d secondaries\n", this, servicep->get_service_id(),
	    secs.length()));
	wrapped->become_primary_prepare(secs, e);
	ASSERT(!e.exception());
	servicep->become_primary();
	_state = replica_int::RMA_BECOME_PRIMARY_PREPARE;
	store_exception(e);
}

void
rma_prov_impl::become_primary(
	const replica::repl_name_seq &secondary_names, Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::become_primary()\n", this,
	    servicep->get_service_id()));
	wrapped->become_primary(secondary_names, e);
	servicep->enable_unreferenced();
	_state = replica_int::RMA_BECOME_PRIMARY;
	store_exception(e);

	// Inform the rma if we just committed the RM service.
	if (servicep->get_service_id() == RMM_SERVICE_ID) {
		rma::the().rm_in_sync();
	}
}

bool
rma_prov_impl::is_receiving_ckpt(Environment &e)
{
	RMA_DBG(("%p(%d) rma_prov_impl::is_receiving_ckpt()\n", this,
	    servicep->get_service_id()));
	bool ret = wrapped->is_receiving_ckpt(e);
	return (ret);
}

void
rma_prov_impl::become_spare(Environment &e)
{
	if (!check_orphan(e)) {
		return;
	}
	RMA_DBG(("%p(%d) rma_prov_impl::become_spare()\n", this,
	    servicep->get_service_id()));
	wrapped->become_spare(e);
	servicep->become_spare();
	_state = replica_int::RMA_BECOME_SPARE;
	store_exception(e);
}

uint32_t
rma_prov_impl::shutdown(replica::repl_prov_shutdown_type shutdown_type,
    Environment &e)
{
	if (!check_orphan(e)) {
		return (0);
	}
	RMA_DBG(("%p(%d) rma_prov_impl::shutdown()\n", this,
	    servicep->get_service_id()));
	uint32_t err_code = wrapped->shutdown(shutdown_type, e);
	if (err_code != 0) {
		return (err_code);
	}
	_state = replica_int::RMA_SHUTDOWN;

	// If the shutdown is successful we should set servicep to NULL
	// since it can be deleted any time now without our knowledge.
	if (!e.exception()) {
		servicep = NULL;
	}
	store_exception(e);
	return (0);
}

CORBA::Object_ptr
rma_prov_impl::get_root_obj(Environment &e)
{
	ASSERT(servicep != NULL);
	RMA_DBG(("%p(%d) rma_prov_impl::get_root_obj()\n", this,
	    servicep->get_service_id()));
	return (wrapped->get_root_obj(e));
}

//
// get_reconnect_objects
//
// lint override for info msg suggesting declaring arguments as
// consts. That is not entirely correct because the method on
// servicep, sets these args.
//
// It is possible that some node is joining and calls
// connect_to_primary() for this service and the service is in the
// process of being shutdown. So servicep could be NULL.
//
void
rma_prov_impl::get_reconnect_objects(
	replica_int::reconnect_object_out recon_p,
	replica_int::reconnect_object_out discon_p, Environment &)//lint -e 1746
{
	if (servicep != NULL) {
		servicep->get_reconnect_objects(recon_p, discon_p);
	} else {
		CL_PANIC(_state == replica_int::RMA_SHUTDOWN);
		recon_p = replica_int::reconnect_object::_nil();
		discon_p = replica_int::reconnect_object::_nil();
	}
}


void
#ifdef DEBUG
rma_prov_impl::_unreferenced(unref_t cookie)
#else
rma_prov_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	// Note that we can't look at the service_id here, since servicep
	// may be invalid. XXX Maybe we should NULL it out in shutdown().
	RMA_DBG(("%p rma_prov_impl::_unreferenced()\n", this));
	CORBA::release(wrapped);
	delete this;
}

// Note that check_orphan locks, and store_exception unlocks.
bool
rma_prov_impl::check_orphan(Environment &e)
{
	lock();

	//
	// Servicep is NULL after the rma_prov_impl::shutdown is successfully
	// called. In that case we shouldn't get any more valid calls.
	//
	if (servicep == NULL || e.is_orphan()) {
		unlock();
		return (false);
	}

	return (true);
}

void
rma_prov_impl::store_exception(Environment &e)
{
	ASSERT(rpi_lock.lock_held());

	CORBA::Exception	*ex = e.exception();
	if (!ex) {
		rpe = replica_int::PROV_NONE;
	} else if (replica::repl_prov_failed::_exnarrow(ex)) {
		rpe = replica_int::PROV_REPL_PROV_FAILED;
	} else if (replica::become_secondary_failed::_exnarrow(ex)) {
		rpe = replica_int::PROV_BECOME_SECONDARY_FAILED;
	} else if (replica::service_busy::_exnarrow(ex)) {
		rpe = replica_int::PROV_SERVICE_BUSY;
	}
	unlock();
}
