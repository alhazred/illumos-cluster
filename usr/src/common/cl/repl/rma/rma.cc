//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rma.cc	1.123	08/11/24 SMI"

#include <orb/infrastructure/orb.h>
#include <repl/rma/rma.h>
#include <repl/repl_mgr/repl_mgr_impl.h>
#include <sys/rm_util.h>
#include <nslib/ns.h>
#include <repl/service/repl_tid_impl.h>

#ifdef	RMA_DBGBUF
uint32_t rma_dbg_size = 100000;
dbg_print_buf rma_dbg(rma_dbg_size);
#endif

rma::reconf_impl::reconf_impl()
{
}

// Registers the reconf object with the RM
void
rma::reconf_impl::register_reconf()
{
	Environment e;

	replica_int::rma_reconf_var reconf_ref = get_objref();
	replica::rm_admin_var rm_adm = rm_util::get_rm();
	replica_int::rm_var the_rm = replica_int::rm::_narrow(rm_adm);
	ASSERT(!CORBA::is_nil(the_rm));
	the_rm->add_rma(reconf_ref, e);
	if (e.exception()) {
		e.exception()->print_exception("rma::reconf can't "
		    "talk to RM\n");
		//
		// SCMSGS
		// @explanation
		// An HA framework component failed to register with the
		// Replica Manager.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: rma::reconf can't talk to RM");
		/*NOTREACHED*/
	}
}

//
// init_services
//
// Creates hxdoor_service objects for HA services in the cluster.
// This function can potentially be called multiple times if the RM
// fails over while this node is joining the cluster.
//
void
rma::reconf_impl::init_services(
    const replica_int::init_service_seq &service_list,
    Environment &e)
{
	hxdoor_service *hs;
	rma::the().lock();

	//
	// Do the orphan check under the rma lock so that if the orphan
	// check passes we are sure that everything will be executed
	// before any other thread is able to look at our state.
	//
	if (e.is_orphan()) {
		rma::the().unlock();
		return;
	}

	RMA_DBG(("reconf_impl::init_services\n"));
	// We shouldn't have any callback registered this early.
	ASSERT(!rma::the().has_callback());
	const replica_int::init_service_seq	&seq_ref = service_list;

	for (uint32_t jindex = 0; jindex < seq_ref.length(); jindex++) {

		// Set our state to a value that will help a recovering RM
		// determine the state of the service.
		replica_int::rma_service_state init_state;
		switch (seq_ref[jindex].state) {
		case replica::S_UP:
			init_state = replica_int::RMA_SERVICE_UNFREEZE;
			break;
		case replica::S_UNINIT:
			init_state = replica_int::RMA_SERVICE_ADD;
			break;
		case replica::S_DOWN:
			//
			// The two states below will never be passed
			// to the new RMA. They are included here only
			// make lint happy.
			//
		case replica::S_DOWN_FROZEN_FOR_UPGRADE:
		case replica::S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ:
			init_state = replica_int::RMA_SERVICE_MARK_DOWN;
			break;
		case replica::S_FROZEN_FOR_UPGRADE:
		case replica::S_FROZEN_FOR_UPGRADE_BY_REQ:
			init_state =
			    replica_int::RMA_SERVICE_FREEZE;
			break;
		case replica::S_UNSTABLE:
		default:
			RMA_DBG(("Service %d state = %d\n",
			    seq_ref[jindex].sid, seq_ref[jindex].state));
			ASSERT(0);
			// keep lint happy
			init_state = replica_int::RMA_SERVICE_UNFREEZE;
			break;
		}
		//
		// The service can exist if the RM primary died right
		// after sending reconf service state checkpoint but
		// before sending dumpstate complete checkpoint and
		// this checkpoint was executed before become_spare
		// was called on the joining RM. In this case
		// init_services would have been called. The new RM
		// primary will re-run add_secondary for this joining
		// node and resend all checkpoints as dumpstate. A new
		// service can register right after the new RM primary
		// has unfrozen but before it is frozen again by
		// rmm::spare_promotion() to add this joining node as
		// a secondary. In that case, we need to create
		// hxdoor_service object for the new service. It is
		// also possible that an existing service was marked
		// down in the same interval. If the joining node had
		// created hxdoor_service object for this service as a
		// result of dumpstate from the now-dead RM primary,
		// its state will be updated as part of
		// reconfiguration and the service will be marked
		// accordingly.
		//
		hs = rma::the().lookup_service_unlocked(seq_ref[jindex].sid);
		if (hs) {
			RMA_DBG(("reconf_impl::init_services hs %d exists\n",
			    seq_ref[jindex].sid));
		} else {
			hs = rma::the().add_service(seq_ref[jindex].sid,
			    (const char *)seq_ref[jindex].name);

			hs->lock();
			hs->record_state(init_state);
			hs->init_service(seq_ref[jindex]);
			hs->unlock();
		}
	}
	rma::the().unlock();
}

void
rma::reconf_impl::freeze_service_prepare(replica::service_num_t sid,
    Environment &e)
{
	RMA_DBG(("reconf_impl::freeze_service_prepare %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);
	ASSERT(sp != NULL && sp->lock_held());
	// The rm state machine should guarantee this.
	ASSERT(sp->get_state() != replica_int::RMA_SERVICE_FREEZE_PREPARE);

	if (!e.is_orphan()) {
		sp->record_state(replica_int::RMA_SERVICE_FREEZE_PREPARE);
	}
	sp->unlock();
}

void
rma::reconf_impl::freeze_service_invos(replica::service_num_t sid,
    Environment &e)
{
	RMA_DBG(("reconf_impl::freeze_service_invos %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);
	ASSERT(sp != NULL && sp->lock_held());

	if (!e.is_orphan()) {
		sp->record_state(replica_int::RMA_SERVICE_FREEZE);
		sp->unlock();
		sp->freeze_service_invos();
	} else {
		sp->unlock();
	}
}

void
rma::reconf_impl::unfreeze_service_invos(replica::service_num_t sid,
    Environment &e)
{
	RMA_DBG(("reconf_impl::unfreeze_service_invos %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);
	ASSERT(sp != NULL && sp->lock_held());
	if (!e.is_orphan()) {
		sp->unfreeze_service_invos();
		sp->record_state(replica_int::RMA_SERVICE_UNFREEZE);
	}
	sp->unlock();
}

void
rma::reconf_impl::connect_to_primary(const replica_int::primary_info &info,
    Environment &e)
{
	RMA_DBG(("reconf_impl::connect_to_primary %d\n", info.sid));
	hxdoor_service *sp = rma::the().lookup_service(info.sid);
	ASSERT(sp != NULL && sp->lock_held());

	if (!e.is_orphan()) {
		sp->record_state(replica_int::RMA_SERVICE_CONNECT);
		sp->connect_to_primary(info.primary);
	}
	sp->unlock();
}

void
rma::reconf_impl::block_marshals(replica::service_num_t sid, Environment &e)
{
	RMA_DBG(("reconf_impl::block_marshals %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);

	// The RM can fail when it's cleaning up a dead service, in which
	// case it's possible that the new RM will call block_marshals
	// after shutdown_service has been called by the old RM.
	if (NULL == sp) {
		return;
	}

	ASSERT(sp != NULL && sp->lock_held());
	if (!e.is_orphan()) {
		sp->block_marshals();
	}
	sp->unlock();
}

void
rma::reconf_impl::disconnect_clients(replica::service_num_t sid,
    Environment &e)
{
	RMA_DBG(("reconf_impl::disconnect_clients %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);

	ASSERT(sp != NULL && sp->lock_held());
	if (!e.is_orphan()) {
		sp->record_state(replica_int::RMA_SERVICE_DISCONNECT);
		sp->disconnect_clients();
	}
	sp->unlock();
}

void
rma::reconf_impl::mark_down(replica::service_num_t sid,
    Environment &e)
{
	RMA_DBG(("reconf_impl::mark_down %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);
	ASSERT(sp != NULL && sp->lock_held());
	if (!e.is_orphan()) {
		sp->record_state(replica_int::RMA_SERVICE_MARK_DOWN);
		sp->mark_down();
	}
	sp->unlock();
}

void
rma::reconf_impl::add_service(replica::service_num_t sid,
    const char *sname, Environment &)
{
	ASSERT(rma::the().lookup_service(sid) == NULL);
	rma::the().lock();
	hxdoor_service *hs = rma::the().add_service(sid, sname);
	hs->freeze_service_invos();
	hs->lock();
	rma::the().state_changed(hs, replica::S_UNINIT);
	hs->unlock();
	rma::the().unlock();
}

void
rma::reconf_impl::shutdown_service(replica::service_num_t sid, Environment &e)
{
	RMA_DBG(("reconf_impl::shutdown %d\n", sid));
	rma::the().lock();
	//
	// Do the orphan check under the rma lock so that if the orphan
	// check passes we are sure that everything will be executed
	// before any other thread is able to look at our state.
	//
	if (!e.is_orphan()) {
		rma::the().shutdown_service(sid);
		// rma::the().shutdown_service() releases the rma lock.
	} else {
		rma::the().unlock();
	}
}

void
rma::reconf_impl::get_service_states(
    const replica_int::service_num_seq &sns,
    replica_int::rma_service_state_seq_out rsss, Environment &)
{
	RMA_DBG(("reconf_impl::get_service_states\n"));

	rma::the().lock();
	rma::the().get_service_states(sns, rsss);
	rma::the().unlock();
} //lint !e1746

//
// shutdown_check_prepare
//
// Start the process for shutdown_check
//
void
rma::reconf_impl::shutdown_check_prepare(replica::service_num_t sid,
	Environment &e)
{
	RMA_DBG(("reconf_impl::shut_chk_prepare %d\n", sid));
	hxdoor_service *sp = rma::the().lookup_service(sid);

	if (sp == NULL) {
		return;
	}

	ASSERT(sp != NULL && sp->lock_held());
	if (!e.is_orphan()) {
		sp->shutdown_check_prepare();
	}
	sp->unlock();
}

bool
rma::reconf_impl::shutdown_ready(replica::service_num_t sid, Environment &e)
{
	// It's possible that a RM shuts the service down,
	// dies, and the new RM calls this function again.
	// So we returns true if we don't find the service.
	bool is_ready = true;
	hxdoor_service *hs = rma::the().lookup_service(sid);
	if (hs != NULL && !e.is_orphan()) {
		is_ready = hs->shutdown_ready();
		hs->unlock();
	}
	RMA_DBG(("reconf_impl::shutdown_ready %d %d\n", sid, is_ready));
	return (is_ready);
}

void
rma::reconf_impl::_unreferenced(unref_t cookie)
{
	(void) _last_unref(cookie);
	RMA_DBG(("reconf_impl::_unreferenced\n"));
}

rma::admin_impl::admin_impl()
{
	Environment e;

	replica_int::rma_admin_ptr admin_ref = get_objref();
	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	ASSERT(!CORBA::is_nil(ctxp));
	//
	// The rma_admin object needs to be bound
	// in the ALL_CLUSTER context. This is becuase
	// from a cz a client program may try to
	// lookup the root nameserver. In order to get
	// the reference of the root nameserver, this
	// rma_admin object is accessed.
	//
	ctxp->bind("rma_admin", admin_ref, e);
	if (e.exception()) {
		e.exception()->print_exception("rma::admin_impl "
		    "failed to bind");
		//
		// SCMSGS
		// @explanation
		// An HA framework component failed to register with the name
		// server.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: rma::admin_impl failed to bind");
	}
}

replica_int::rma_repl_prov_ptr
rma::admin_impl::wrap_handler_repl_prov(replica_int::handler_repl_prov_ptr prov,
    replica::service_num_t sid, Environment &e)
{
	RMA_DBG(("Wrapping provider for service %d\n", sid));

	hxdoor_service *sp = rma::the().lookup_service(sid);
	if (sp == NULL) {
		replica::failed_service *faileds = new replica::failed_service;
		ASSERT(faileds != NULL);
		e.exception(faileds);
		return (replica_int::rma_repl_prov::_nil());
	}

	rma_prov_impl *provp = new rma_prov_impl(prov, sp);
	sp->unlock();

	return (provp->get_objref());
}

replica_int::tid_factory_ptr
rma::admin_impl::get_tid_factory(Environment &)
{
	return ((new kernel_tid_factory)->get_objref());
}

void
rma::admin_impl::register_callback(replica::service_state_callback_ptr sc,
    Environment &)
{
	RMA_DBG(("register_callback %p\n", sc));
	rma::the().register_callback(sc);
}

void
rma::admin_impl::_unreferenced(unref_t cookie)
{
	(void) _last_unref(cookie);
}

rma::init_service_task::init_service_task(replica::service_state_info_list *l,
    replica::service_state_callback_ptr sc) :
	callback_p(sc),
	ss_list(l)
{
}

rma::init_service_task::~init_service_task()
{
	delete ss_list;
	ss_list = NULL;
	callback_p = replica::service_state_callback::_nil();
}

void
rma::init_service_task::execute()
{
	RMA_DBG(("init_service_task::execute\n"));
	Environment e;
	callback_p->init_services(*ss_list, e);
	// We can get exception if a usrland program registers the callback
	// and then dies. Just remove the callback.
	if (e.exception() != NULL) {
		RMA_DBG(("init_service_task exception\n"));
		rma::the().remove_callback(callback_p);
	}
	delete this;
}

rma::state_change_task::state_change_task(const char *service_name,
    replica::service_state ss, replica::service_state_callback_ptr sc)
{
	callback_p = replica::service_state_callback::_duplicate(sc);
	ss_info.service_name = service_name;
	ss_info.s_state = ss;
}

rma::state_change_task::~state_change_task()
{
	CORBA::release(callback_p);
	callback_p = replica::service_state_callback::_nil();
}

void
rma::state_change_task::execute()
{
	RMA_DBG(("state_change_task::execute, service_name: %s state: %d\n",
	    (char *)ss_info.service_name, ss_info.s_state));
	Environment e;
	callback_p->state_changed(ss_info, e);
	// We can get exception if a usrland program registers the callback
	// and then dies. Just remove the callback.
	if (e.exception() != NULL) {
		RMA_DBG(("state_change_task exception\n"));
		rma::the().remove_callback(callback_p);
	}
	delete this;
}

rma *rma::the_rmap = NULL;

rma::rma() : reconfig_in_progress(false),
    canceled_seqnum(0ULL),
    rm_synced(false),
    callback_threadpool(false, 0, "rma callback threadpool")
{
}

rma::~rma()
{
	SList<replica::service_state_callback>::ListIterator
	    iter(callback_list);
	replica::service_state_callback_ptr iter_list_p =
	    replica::service_state_callback::_nil();

	while ((iter_list_p = iter.get_current()) != NULL) {
		iter.advance();
		callback_list.erase(iter_list_p);
		CORBA::release(iter_list_p);
	}
}

int
rma::initialize()
{
	ASSERT(the_rmap == NULL);
	the_rmap = new rma;
	ASSERT(the_rmap != NULL);

	int retval = 0;
	// Make callback_threadpool realtime and give it 1 thread.
	if ((retval = the_rmap->callback_threadpool.set_sched_props(
	    orb_conf::rt_maxpri(), orb_conf::rt_classname())) != 0) {
		RMA_DBG(("can't make rma callback thread realtime\n"));
		return (retval);
	}
	if (the_rmap->callback_threadpool.change_num_threads(1) < 1) {
		RMA_DBG(("can't create thread for rma callback thread\n"));
		return (ENOMEM);
	}
	return (0);
}

// Register a service_state_callback with the rma.
void
rma::register_callback(replica::service_state_callback_ptr sc)
{
	hxdoor_service	*hs;
	uint32_t	count = 0;
	int		i = 0;

	RMA_DBG(("rma::register_callback: %p\n", sc));
	lock();

	services_lock.rdlock();

	// Get the number of services and initialize a sequence of that length.
	for (i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		count += services[i].count();
	}

	replica::service_state_info_list *sl =
	    new replica::service_state_info_list(count);
	replica::service_state_info_list &slref = *sl;

	// Fill the sequence up with info.
	uint_t j = 0;
	for (i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			hs->lock();
			if (hs->get_service_id() >= STARTING_SERVICE_ID) {
				slref[j].service_name = hs->get_name();
				slref[j].s_state = hs->get_service_state();
				j++;
			}
			hs->unlock();
		}
	}
	services_lock.unlock();
	slref.length(j);

	callback_lck.lock();
	replica::service_state_callback_ptr callback_p =
	    replica::service_state_callback::_duplicate(sc);
	callback_list.append(callback_p);

	// Defer al task to call sc->init_services.
	init_service_task *init_tsk = new init_service_task(sl, callback_p);
	callback_threadpool.defer_processing(init_tsk);

	callback_lck.unlock();
	unlock();
}

void
rma::remove_callback(replica::service_state_callback_ptr
#ifdef RMA_DBGBUF
    sc
#endif
)
{
	callback_lck.lock();

	SList<replica::service_state_callback>::ListIterator
	    iter(callback_list);
	replica::service_state_callback_ptr iter_list_p =
	    replica::service_state_callback::_nil();

	while ((iter_list_p = iter.get_current()) != NULL) {
		if (iter_list_p->_equiv(sc)) {
			RMA_DBG(("rma::remove_callback: %p\n", sc));
			callback_list.erase(iter_list_p);
			CORBA::release(iter_list_p);
			break;
		}
		iter.advance();
	}
	callback_lck.unlock();
}

// Let rma know that a service of the specified name just go into
// the specified state.
void
rma::state_changed(hxdoor_service *hs, replica::service_state ss)
{
	ASSERT(hs->lock_held());
	callback_lck.lock();

	SList<replica::service_state_callback>::ListIterator
	    iter(callback_list);
	replica::service_state_callback_ptr iter_list_p =
	    replica::service_state_callback::_nil();

	while ((iter_list_p = iter.get_current()) != NULL) {
		if (hs->get_service_id() >= STARTING_SERVICE_ID) {
			RMA_DBG(("state_changed: %s %d\n",
			    hs->get_name(), ss));
			state_change_task *tsk = new
			    state_change_task(hs->get_name(),
			    ss, iter_list_p);
			callback_threadpool.defer_processing(tsk);
		}
		iter.advance();
	}
	callback_lck.unlock();
}

// This walks through the services supplied in sns, and puts their current
// state into rsss.
void
rma::get_service_states(const replica_int::service_num_seq &sns,
    replica_int::rma_service_state_seq_out rsss)
{
	ASSERT(lock_held());

	hxdoor_service	*hs;
	uint_t		i;
	uint_t		which_serv;

	rsss = new replica_int::rma_service_state_seq(sns.length(),
	    sns.length());

	services_lock.rdlock();
	for (i = 0; i < sns.length(); i++) {
		which_serv = sns[i] % RMA_SERVICE_HASH_SIZE;
		DList<hxdoor_service>::ListIterator li(services[which_serv]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			if (hs->get_service_id() == sns[i]) {
				break;
			}
		}
		if (hs == NULL) {
			(*rsss)[i] = replica_int::RMA_SERVICE_NONE;
		} else {
			(*rsss)[i] = hs->get_state();
		}
		RMA_DBG(("h_s::get_state(%d) = %d\n", sns[i], (*rsss)[i]));
	}
	services_lock.unlock();
}

hxdoor_service *
rma::add_service(replica::service_num_t sid, const char *service_name)
{
	hxdoor_service *hs;

	ASSERT(lock_held());

	// Don't mess with the list of services while we are reconfiguring.
	while (reconfig_in_progress) {
		cv.wait(&lck);
	}

	ASSERT(service_name != NULL && service_name[0] != 0);

	hs = new hxdoor_service(sid, service_name);

	services_lock.wrlock();
	services[sid % RMA_SERVICE_HASH_SIZE].append(hs);
	services_lock.unlock();

	RMA_DBG(("rma(%p)::add_service(%d)\n", this, sid));
	return (hs);
}

//
// Note that lookup_service will either return NULL, or a pointer to a service
// it has locked.  If lookup_service returns a non-null value, the caller
// must call unlock when finished with it.
//
hxdoor_service *
rma::lookup_service(replica::service_num_t sid)
{
	hxdoor_service *hs;
	uint_t		which_serv;

	ASSERT(!lock_held());

	services_lock.rdlock();
	which_serv = sid % RMA_SERVICE_HASH_SIZE;
	DList<hxdoor_service>::ListIterator li(services[which_serv]);
	for (; (hs = li.get_current()) != NULL; li.advance()) {
		if (hs->get_service_id() == sid) {
			break;
		}
	}

	if (hs != NULL) {
		hs->lock();
	}
	services_lock.unlock();
#ifdef HXD_DEBUG
	RMA_DBG(("rma(%p)::lookup_service(%d) returning %p\n", this, sid, hs));
#endif
	return (hs);
}

//
// Note that lookup_service will either return NULL, or a pointer to a service
// The caller locks-unlocks rma.
//
hxdoor_service *
rma::lookup_service_unlocked(replica::service_num_t sid)
{
	hxdoor_service *hs;
	uint_t		which_serv;

	ASSERT(lock_held());

	which_serv = sid % RMA_SERVICE_HASH_SIZE;
	services_lock.rdlock();
	DList<hxdoor_service>::ListIterator li(services[which_serv]);
	for (; (hs = li.get_current()) != NULL; li.advance()) {
		if (hs->get_service_id() == sid) {
			break;
		}
	}
	services_lock.unlock();

	return (hs);
}

// Try to shut the specified service down.
void
rma::shutdown_service(replica::service_num_t sid)
{
	ASSERT(lock_held());

	// Don't mess with the list of services while we are reconfiguring.
	while (reconfig_in_progress) {
		cv.wait(&lck);
	}

	services_lock.wrlock();
	hxdoor_service *hs;
	DList<hxdoor_service> &bp = services[sid % RMA_SERVICE_HASH_SIZE];
	DList<hxdoor_service>::ListIterator li(bp);

	for (; (hs = li.get_current()) != NULL; li.advance()) {
		if (hs->get_service_id() == sid) {
			break;
		}
	}
	//
	// It's possible that a RM shuts the service down, dies, and the new RM
	// calls this function again. So, it is ok to not find the service.
	//
	if (hs == NULL) {
		services_lock.unlock();
		unlock();
		return;
	}

	hs->lock();

	hs->record_state(replica_int::RMA_SERVICE_SHUTDOWN);
	hs->shutdown();
	(void) bp.erase(hs);
	services_lock.unlock();
	//
	// hxdoor_service destructor waits for unreference on
	// reconnect and disconnect_objects to complete. So release
	// the rma lock after removing hxdoor_service object from our
	// services table.
	//
	unlock();

	delete hs;
}

void
rma::register_rma()
{
	the_reconf.register_reconf();
}

void
rma::rm_in_sync()
{
	lock();
	rm_synced = true;
	cv.signal();
	unlock();
}

void
rma::wait_rm_sync()
{
	lock();
	while (!rm_synced) {
		cv.wait(&lck);
	}
	unlock();
}

// get_reconf should only be needed by the rmm.
replica_int::rma_reconf_ptr
rma::get_reconf()
{
	return (the_reconf.get_objref());
}

//
// freeze_invos_to_dead_primaries
//
// Called from the orb reconfiguration code in quiesce_step.  There is
// now a new cluster membership. We need to check if any primaries
// have died and freeze invocations to the service if so.
//
void
rma::freeze_invos_to_dead_primaries()
{
	hxdoor_service *hs;

	//
	// We use the reconfig_in_progress variable to make sure that we can
	// safely traverse the hxdoor list during this step.  We can't just
	// hold the lock while we walk the list because these operations may
	// depend on unmarshals occuring which will need to get the same lock.
	//
	lock();
	ASSERT(!reconfig_in_progress);
	reconfig_in_progress = true;
	unlock();

	RMA_DBG(("rma::freeze_invos_to_dead_primaries\n"));
	int i;

	//
	// First we wake up all threads which are blocked in marshal. They Can
	// then recheck if their destination node is alive and continue.
	// This is needed before we freeze services, since the freeze depends
	// on threads which have begun invocations to dead nodes to drain out
	// relatively quickly.
	//
	services_lock.rdlock();
	for (i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			hs->wakeup_marshals();
		}
	}

	for (i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			hs->freeze_invos_to_dead_primary();
		}
	}
	services_lock.unlock();

	lock();
	reconfig_in_progress = false;
	cv.broadcast();
	unlock();
}

//
// cleanup_foreignrefs
//
// Called from CMM's refcnt_cleanup_step to process unreferences for
// hxdoors whose only clients were on the nodes that have died.
//
void
rma::cleanup_foreignrefs(callback_membership *curr_mbrship,
    callback_membership *new_mbrship)
{
	hxdoor_service *hs;

	services_lock.rdlock();
	for (int i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			if (ID_node::match(hs->get_node(),
			    orb_conf::current_id_node())) {
#ifdef RMA_DBUG
				RMA_DBG(("rma::cln_for hsp=%p\n"));
#endif
				hs->cleanup_foreignrefs(curr_mbrship,
				    new_mbrship);
			}
		}
	}
	services_lock.unlock();
}

//
// disconnect_from_dead_primaries
//
// Called from the CMM reconfiguration refcnt_cleanup_step. There is
// now a new cluster membership. We need to check if any primaries
// have died and disconnect clients from the old primary.
//
bool
rma::disconnect_from_dead_primaries(cmm::seqnum_t seqnum, os::systime *tmoutp)
{
	hxdoor_service	*hs;
	bool		timedout = false;

	//
	// We use the reconfig_in_progress variable to make sure that we can
	// safely traverse the hxdoor list during this step.  We can't just
	// hold the lock while we walk the list because these operations may
	// depend on unmarshals occurring which will need to get the same lock.
	//
	lock();
	ASSERT(!reconfig_in_progress);
	if (canceled_seqnum == seqnum) {
		// This step has been canceled. Return ASAP.
		unlock();
		return (false);

	}
	reconfig_in_progress = true;
	unlock();

	RMA_DBG(("rma::disconnect_from_dead_primaries\n"));
	services_lock.rdlock();
	for (int i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			if (hs->disconnect_from_dead_primary(seqnum, tmoutp)) {
				timedout = true;
				break;
			} else if (canceled_seqnum == seqnum) {
				break;
			}

		}
		if (timedout || (canceled_seqnum == seqnum)) {
			break;
		}
	}
	services_lock.unlock();

	lock();
	reconfig_in_progress = false;
	cv.broadcast();
	unlock();
	return (timedout);
}

//
// disconnect_from_dead_primaries_cancel
//
// Called from the orb reconfiguration code when we need to cancel
// refcnt_cleanup_step. We update the canceled sequence number in each
// hxdoor_service and signal the thread which may be running
// disconnect_from_dead_primaries().
//
void
rma::disconnect_from_dead_primaries_cancel(cmm::seqnum_t seqnum)
{
	hxdoor_service *hs;

	lock();
	canceled_seqnum = seqnum;
	if (!reconfig_in_progress) {
		unlock();
		return;
	}

	RMA_DBG(("rma::disconnect_from_dead_primaries_cancel %d\n", seqnum));
	services_lock.rdlock();
	for (int i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			hs->disconnect_from_dead_primary_cancel(seqnum);
		}
	}
	services_lock.unlock();
	unlock();
}

void
rma::mark_dead_primaries_disconnected_on_all_nodes()
{
	hxdoor_service *hs;
	//
	// We use the reconfig_in_progress variable to make sure that we can
	// safely traverse the hxdoor list during this step.  We can't just
	// hold the lock while we walk the list because these operations may
	// depend on unmarshals occurring which will need to get the same lock.
	//
	lock();
	ASSERT(!reconfig_in_progress);
	reconfig_in_progress = true;
	unlock();

	services_lock.rdlock();
	RMA_DBG(("rma::mark_dead_primaries_disconnected_on_all_nodes.\n"));
	for (int i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL; li.advance()) {
			hs->mark_dead_primary_disconnected_on_all_nodes();
		}
	}
	services_lock.unlock();

	lock();
	reconfig_in_progress = false;
	cv.broadcast();
	unlock();
}

#ifdef FAULT_RECONF

// Wait for a thread to block in step3.
void
rma::wait_for_step3_blocked()
{
	fi_lock.lock();
	while (!fi_check_step3_blocked()) {
		fi_cv.wait(&fi_lock);
	}
	fi_lock.unlock();
}

// Wait for threads blocked in step3 to unblock.
void
rma::wait_for_step3_unblocked()
{
	fi_lock.lock();
	while (fi_check_step3_blocked()) {
		fi_cv.wait(&fi_lock);
	}
	fi_lock.unlock();
}

// Called by hxdoor code when it blocks in step3.
void
rma::fi_signal_blocked()
{
	fi_lock.lock();
	fi_cv.broadcast();
	fi_lock.unlock();
}

// Walk all services and see if they are blocked in step 3.
bool
rma::fi_check_step3_blocked()
{
	hxdoor_service *hs;
	bool blocked = false;

	//
	// We need to drop the fi_lock before grabbing the rma and hxdoor
	// locks.
	//
	fi_lock.unlock();
	lock();
	if (!reconfig_in_progress) {
		unlock();
		fi_lock.lock();
		return (false);
	}
	services_lock.rdlock();
	for (int i = 0; i < RMA_SERVICE_HASH_SIZE && !blocked; i++) {
		DList<hxdoor_service>::ListIterator li(services[i]);
		for (; (hs = li.get_current()) != NULL && !blocked;
		    li.advance()) {
			if (hs->fi_check_step3_blocked()) {
				blocked = true;
			}
		}
	}
	services_lock.unlock();
	unlock();

	fi_lock.lock();
	return (blocked);
}

#endif
#ifdef DEBUG
bool
rma::has_callback()
{
	bool ret;
	callback_lck.lock();
	ret = (!callback_list.empty());
	callback_lck.unlock();
	return (ret);
}
#endif // DEBUG
