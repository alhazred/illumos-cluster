//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hxdoor_service.cc	1.21	08/05/20 SMI"

// Implementation file for hxdoor_service

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/xdoor/rxdoor.h>
#include <repl/rma/hxdoor.h>
#include <repl/rma/hxdoor_service.h>
#include <repl/service/replica_handler.h>
#include <repl/rma/rma.h>
#include <repl/rma/reconnect_object_impl.h>
#include <orb/refs/unref_threadpool.h>
#include <orb/msg/orb_msg.h>
#include <orb/transport/nil_endpoint.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/member/members.h>

#ifdef PROBES
#include <sys/mc_probe.h>
#endif

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#endif

#ifdef _KERNEL
#include <sys/systm.h>		// for maxusers
#else
#include <unode/systm.h>	// for maxusers
#endif

//
// This number is used to scale the number of hxdoor buckets based
// on the amount of memory in the system. It can be set in /etc/system.
//
uint_t	hxdoor_buckets_scale = 16;

static uint_t
hash_lists()
{
	return (1 << (os::highbit((ulong_t)maxusers * 32) - 1)); //lint !e571
}

//
// A new service bucket is allocated the first time we register an
// hxdoor associated with that service.  The invocation freeze lock
// invo_fl starts out unfrozen, and will be frozen explicitly by the
// RM.
//
// The state of the hxdoor_service object is set HS_SPARE to begin
// with. If this node gets added as a secondary provider, as a
// consequence of dumpstate operation, all hxdoors will be converted
// to secondary hxdoors. Nothing special is done to tell the
// hxdoor_service that it has been made a secondary. This flag is
// reset in become_primary and become_secondary.
//
hxdoor_service::hxdoor_service(replica::service_num_t s,
	const char *service_name) :
	rxdoor_node(orb_conf::current_id_node(), 2048, 4096),
	primary_prov_p(nil),
	reconnect_objp(NULL),
	disconnect_objp(NULL),
	disconnect_p(nil),
	_sstate(replica_int::RMA_SERVICE_ADD),
	_sname(os::strdup(service_name)),
	hs_flags(HS_IGNORE_UNREF | HS_SPARE),
	servid(s),
	last_object_id(0),
	canceled_seqnum(0ULL),
	freezing(false),
	is_dead_primary_disconnected_on_all_nodes(false),
	can_shutdown(true),
	shutdown_prepare_called(false)
{
#ifdef	RMA_DBGBUF
	os::sprintf(dbg_id, "hs %d %p", s, this);
#endif
#ifdef FAULT_RECONF
	step3_blocked_on_reconnect = false;
#endif

}

// Destructor. Make sure that there is no hxdoor around.
hxdoor_service::~hxdoor_service()
{
	CL_PANIC(can_shutdown);
	delete[] _sname;

	//
	// Wait for unreference on reconnect_object. Suppose a
	// reconnecting secondary died and service shutdown was issued
	// simultaneously, then a reference to reconnect_obj could
	// have been marshaled to that node. So foreignrefs > 0 and
	// the masks will indicate which node the reference was sent
	// to. As part of reconfiguration for that node's death, this
	// node will call marshal_received() and unregister_client()
	// to account for the reference on the dead
	// node. reconnect_objp cannot be deleted until it is
	// unreferenced. There is no harm in waiting here, because as
	// far as the framework is concerned, the service has been
	// shutdown, so it will not receive any more invocations. We
	// can not wait for it to be unreferenced in the shutdown()
	// method because then we will be holding the rma lock for
	// that duration. That will prevent all unmarshals and
	// reference counting activity for other hxdoors too because
	// their lookup needs to acquire rma lock.
	//
	// The other option is to check if reconnect_objp has been
	// unreferenced in shutdown() and return false if not. Then
	// the rma can put the rm_repl_service object of this service
	// on the periodic task and come back later to see if it can
	// be shutdown.
	//
	// rma::shutdown_service() locks the hxdoor_service object and
	// then calls delete on it.
	//
	if (reconnect_objp) {
		reconnect_objp->delete_after_unreferenced();
		reconnect_objp = NULL;
	}

	if (disconnect_objp) {
		//
		// If disconnect_objp != NULL, then this is/was the
		// primary node for this service and would have
		// created and given out references to
		// disconnect_objp. Wait for it to be
		// unreferenced. Hxdoor_service objects on all other
		// nodes would have released the reference when
		// shutdown_ready() was called on them.
		//
		disconnect_objp->delete_after_unreferenced();
		disconnect_objp = NULL;
	}
	disconnect_p = nil; // released by shutdown_ready()
	//
	// primary_prov_p should have been released either by
	// disconnect_clients or hxdoor_service::shutdown
	//
	ASSERT(CORBA::is_nil(primary_prov_p));
	primary_prov_p = nil;	// make lint happy
}

// record the service state.
void
hxdoor_service::record_state(replica_int::rma_service_state sstate)
{
	ASSERT(lock_held());

	// get the current service state.
	replica::service_state s_old = get_service_state();

	// Update the state and get the new service state.
	_sstate = sstate;
	replica::service_state s_new = get_service_state();

	if (s_old != s_new) {
		RMA_DBG(("%s record_state old=%d, new=%d:\n",
		    dbg_id, s_old, s_new));
		rma::the().state_changed(this, s_new);
	}
}

void
hxdoor_service::init_service(const replica_int::init_service_info &info)
{
	ASSERT(lock_held());
	//
	// We don't need to call rma::state_changed since we know that
	// there aren't any callbacks registered this early.
	//
	ASSERT(!rma::the().has_callback());

	switch (info.state) {
	case replica::S_UP:
		connect_to_primary(info.primary);
		if (info.primary->_handler()->server_dead()) {
			unlock();
			freeze_service_invos();
			lock();
		}
		break;
	case replica::S_UNINIT:
		unlock();
		freeze_service_invos();
		lock();
		break;
	case replica::S_FROZEN_FOR_UPGRADE:
	case replica::S_FROZEN_FOR_UPGRADE_BY_REQ:
		connect_to_primary(info.primary);
		unlock();
		freeze_service_invos();
		lock();
		break;
	case replica::S_DOWN:
		//
		// The two states below will never be passed to
		// hxdoor_service object. They are included here only
		// make lint happy.
		//
	case replica::S_DOWN_FROZEN_FOR_UPGRADE:
	case replica::S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ:
		unlock();
		freeze_service_invos();
		lock();
		mark_down();
		break;
	case replica::S_UNSTABLE:
	default:
		ASSERT(0);
		break;
	}
}

//
// disable_unreferenced
//
// We are in the middle of a switchover. So hold unreference
// processing.
//
void
hxdoor_service::disable_unreferenced()
{
	uint_t	i;
	rxdoor_bucket *bp;

	lock();

	RMA_DBG(("%s: disable_unrefs\n", dbg_id));
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}

	// By setting this flag with all bucket locks held, we can
	// read it with any of the locks held.
	hs_flags |= HS_IGNORE_UNREF;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->unlock();
	}
	//
	// There might be some hxdoor unreference tasks already in the
	// unref list but haven't been delivered to the
	// replica_handlers yet.  We discard all those unref tasks
	// here. These will be regenerated when enable_unreferenced()
	// is called on the new primary.
	//
	task_list_t	 reject_unref;
	unref_threadpool::the().flush_tag(servid, &reject_unref);
	//
	// For all the hxdoors removed from unref_threadpool,
	// call unref_rejected()
	//
	hxdoor		*hxdoorp;

	while ((hxdoorp = (hxdoor *)reject_unref.reapfirst()) != NULL) {
		hxdoorp->unref_rejected();
	}

	unlock();
}

//
// become_secondary
//
// Convert this service to a secondary.
//
void
hxdoor_service::become_secondary()
{
	uint_t		i = 0;
	rxdoor_bucket 	*bp;
	hxdoor 		*hxdp;
	rx_list		*lp = lists;
	rx_list::ListIterator li;

	// disable_unreferenced() must have been called already.
	ASSERT((hs_flags & HS_IGNORE_UNREF) != 0);

	lock();
	//
	// We need to make sure that there are no references in
	// transit and that we do not marshal more references during
	// this interval.
	//
	block_marshals();

	hs_flags |= HS_SECONDARY;
	hs_flags &= ~(HS_PRIMARY | HS_SPARE);

	//
	// unlock the service so that when a unreg message arrives,
	// rma::lookup_service() can lookup and return locked
	// service. hxdoor::become_secondary gives up hxdoor (bucket)
	// lock while waiting for bitmasks to change.
	//
	unlock();

	//
	// Check if we were the primary and are now becoming
	// secondary. If so then we need to delete reconnect object.
	// It is safe to modify reconnect and disconnect object
	// pointers outside lock because all service state changes are
	// processed by the state machine serially and unreference of
	// these objects does not delete them.
	//
	if (reconnect_objp) {
		//
		// Wait for unreference on reconnect_object. There should not
		// be any references at this point.
		//
		RMA_DBG(("%s: bec_sec wait for recon_obj unref\n", dbg_id));
		reconnect_objp->delete_after_unreferenced();
		reconnect_objp = NULL;
	}
	//
	// Check if we were the primary and are now becoming
	// secondary. If so then we need to make sure that all
	// reference counting messages to us have been received.
	// That is true if we receive unreference on the
	// disconnect object.
	//
	if (disconnect_objp) {
		//
		// Wait for unreference on disconnect_object. This
		// ensures that all clients and secondaries have
		// disconnected.
		//
		RMA_DBG(("%s: bec_sec wait for dis_obj unref\n", dbg_id));
		disconnect_objp->delete_after_unreferenced();
		disconnect_objp = NULL;
	}
	// Convert all the hxdoors to secondary.
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
		for (uint_t j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			for (; (hxdp = (hxdoor *)li.get_current()) != NULL;
			    li.advance()) {

				hxdp->become_secondary();
			}
		}
		bp->unlock();
	}

	lock();
	unblock_marshals();
	unlock();
}

//
// become_spare
//
// Convert this object to a spare.
//
void
hxdoor_service::become_spare()
{
#ifdef DEBUG
	uint_t			i;
	rxdoor_bucket 		*bp;
	rx_list			*lp = lists;
	rx_list::ListIterator	li;

	lock();
	hs_flags |= HS_SPARE;
	hs_flags &= ~(HS_PRIMARY | HS_SECONDARY);

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
		for (uint_t j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			hxdoor *hxdp;
			while ((hxdp = (hxdoor *)li.get_current()) != NULL) {
				if (hxdp->flags & hxdoor::SECONDARY) {
					os::warning("become_spare() did not"
					    " remove object for %d %d,"
					    " handler = %p\n",
					    hxdp->get_serviceid(),
					    hxdp->server_xdoor,
					    hxdp->get_server_handler());
					// Clean up for now.
					hxdp->become_spare();
				}
				li.advance();
			}
		}

		bp->unlock();
	}

	unlock();
#endif
}

//
// shutdown
//
// Shutdown this hxdoor_service, called when a service is shutdown.
//
void
hxdoor_service::shutdown()
{
	ASSERT(lock_held());

	RMA_DBG(("%s shutdown called\n", dbg_id));
	if (!CORBA::is_nil(primary_prov_p)) {
		//
		// If the service was shutdown alive, disconnect_clients()
		// never gets called on this service so the primary_prov_p
		// never gets released. We need to release the primary_prov_p
		// here.
		//
		CORBA::release(primary_prov_p);
		primary_prov_p = nil;
	}
	// There should be no more clients at this point.
	CL_PANIC(can_shutdown);
	//
	// If we are ready to shutdown we must obtain all the locks here
	// to prevent the case where another thread is holding one of
	// these locks and is about to do a unlock.
	//
	rxdoor_bucket *bp;
	uint_t i;
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}
	data_lock.lock();
}

//
// enable_unreferenced
//
// We are now a primary. This means that we must allow unreference to
// occur. Scan the list of hxdoors and have each hxdoor check whether
// the hxdoor should declare unreferenced.
//
void
hxdoor_service::enable_unreferenced()
{
	uint_t			i, j;
	rxdoor_bucket 		*bp;
	hxdoor 			*hxdp;
	rx_list::ListIterator 	li;
	rx_list			*lp = lists;

	//
	// Wait for all clients to finish reconnecting. We release the
	// reference we have to let the reconnect_object be
	// unreferenced. Clients will release their reference to
	// reconnect object after sending all register messages. Wait
	// for unreference on reconnect_object to happen.
	//
	reconnect_objp->wait_until_unreferenced();

	lock();

	RMA_DBG(("%s: enable_unrefs\n", dbg_id));
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}
	//
	// By setting this flag with all bucket locks held, we can read it with
	// any of the locks held.
	//
	hs_flags &= ~HS_IGNORE_UNREF;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		for (j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			while ((hxdp = (hxdoor *)li.get_current()) != NULL) {

				// Advance before possibly deleting
				// the current element
				li.advance();
				hxdp->handle_possible_unreferenced();
			}
		}
		bp->unlock();
	}
	unlock();
}

//
// become_primary
//
// We are now primary.  We need to unblock marshals, but we are still
// ignoring unreferenced on our hxdoors.
//
void
hxdoor_service::become_primary()
{
	uint_t			i, j;
	rxdoor_bucket 		*bp;
	hxdoor 			*hxdp;
	rx_list::ListIterator 	li;
	rx_list			*lp = lists;

	lock();

	ASSERT(invo_fl.frozen());
	ASSERT(reconnect_objp == NULL);
	ASSERT(disconnect_objp == NULL);
	ASSERT(disconnect_p == nil);

	hs_flags |= (HS_PRIMARY | HS_PRIMARY_KNOWN);
	hs_flags &= ~(HS_SECONDARY | HS_SPARE);

	// Create reconnect object
	reconnect_objp = new reconnect_object_impl;

	// Create disconnect_object
	disconnect_objp = new reconnect_object_impl;

	RMA_DBG(("%s: become_primary rcon_obj=%p, dcon_obj=%p\n", dbg_id,
	    reconnect_objp, disconnect_objp));

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		for (j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			for (; (hxdp = (hxdoor *)li.get_current()) != NULL;
			    li.advance()) {
				hxdp->become_primary();
			}
		}

		bp->unlock();
	}

	unlock();
}

//
// hxdoor_created
//
// Mark that a new hxdoor has been created and hence the service can
// not shutdown.
//
void
hxdoor_service::hxdoor_created()
{
	data_lock.lock();
	can_shutdown = false;
	data_lock.unlock();
}

//
// shutdown_check_prepare
//
// Prepare to shutdown.
//
void
hxdoor_service::shutdown_check_prepare()
{
	ASSERT(lock_held());
	data_lock.lock();
	RMA_DBG(("shutdown_check_prepare, can_shutdown=%d\n", can_shutdown));

	shutdown_prepare_called = true;

	if (is_empty()) {
		can_shutdown = true;
	} else {
		CL_PANIC(!can_shutdown);
	}
	data_lock.unlock();
}

//
// shutdown_ready
//
// Return true if all HA objects in this service have been unreferenced.
//
bool
hxdoor_service::shutdown_ready()
{
	ASSERT(lock_held());

	RMA_DBG(("hxdoor_service::shutdown_ready %d can_shut=%d\n",
	    servid, can_shutdown));
	//
	// Release reference to disconnect_p since this instance of
	// service will never be used after this point, because it is
	// down and is on the RM periodic cleanup_task list. So
	// connect_to_primary and disconnect_clients will also never
	// be called. The reference is released here so that by the
	// time rma::shutdown_service() is called on primary rma the
	// object would have been unreferenced and the invocation will
	// return in a timely manner.
	//
	// Release reference to disconnect_obj on the primary.
	// ok even if disconnect_p == nil.
	//
	CORBA::release(disconnect_p);
	disconnect_p = nil;

	//
	// If the RM primary is still running version0, then it would
	// have called block_marshals and we need to unblock them
	// here.
	//
	if (!shutdown_prepare_called) {
		unblock_marshals();
	}

	return (can_shutdown);
}

//
// block_marshals
//
// This is called in become_secondary() and by the RM's cleanup
// thread. In both cases we need to make sure that there are'nt any
// references in transit at the time we are becoming secondary or
// shutting down. So we just block marshals.
//
// In become_secondary(), we dissociate hxdoors from old primary and
// reconnect them with the new primary. This is done by sending
// register messages to the new primary. We block marshals from
// old primary to prevent so receiving nodes can determine which node
// to send reference counting messages to.
//
// The RM's cleanup thread wakes up periodically and checks if a
// service in the cleanup service list has any outstanding remote
// client references or not by calling shutdown_ready on all RMA's for
// the service in question. In order to get an accurate account of
// remote client references, we want to block marshals for the service
// temporarily so that there will be no references in transit when
// shutdown_ready is called. Note shutdown_ready will unblock marshals
// before returning. We do not dissociate hxdoors from the current
// primary if the service is being shutdown because we do not need to
// reconnect to a new primary. But we do want make sure
//
void
hxdoor_service::block_marshals()
{
	ASSERT(lock_held());
	RMA_DBG(("hxdoor_service::block_marshals %d.\n", servid));

	uint_t		i;
	rxdoor_bucket	*bp;
	//
	// By grabbing the bucket lock we guarantee that all ongoing
	// marshals are done and no new marshals will be
	// processed.
	//
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}
	//
	// Disable marshals. By setting this flag with all bucket locks held,
	// we can read it with any of the locks held.
	//
	hs_flags |= HS_MARSHALS_BLOCKED;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->unlock();
	}
}

void
hxdoor_service::unblock_marshals()
{
	ASSERT(lock_held());
	RMA_DBG(("hxdoor_service::unblock_marshals %d.\n", servid));

	uint_t		i;
	rxdoor_bucket	*bp;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}
	//
	// Enable marshals. By setting this flag with all bucket locks held,
	// we can read it with any of the locks held.
	//
	CL_PANIC((hs_flags & HS_MARSHALS_BLOCKED) != 0);
	hs_flags &= ~HS_MARSHALS_BLOCKED;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		// Wake up any threads waiting for marshals to be unblocked.
		bp->cv.broadcast();
		bp->unlock();
	}
}

//
// disconnect_clients
//
// Disconnect hxdoors from the current primary. Called during switchover
// to disassociate the service from the old primary. Also, called during
// quiesce_step of CMM reconfiguration if the primary has died.
//
void
hxdoor_service::disconnect_clients()
{
	uint_t			i, j;
	hxdoor			*hxdp;
	rxdoor_bucket		*bp;
	rx_list			*lp = lists;
	rx_list::ListIterator	li;

	RMA_DBG(("hxdoor_service::disconnect_clients %d\n", servid));

	ASSERT(lock_held());
	//
	// Set flag to indicate the current primary is invalid.
	//
	hs_flags &= ~HS_PRIMARY_KNOWN;
	//
	// Now we go through the buckets one by one and disconnect all
	// hxdoors.
	//
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
		for (j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			while ((hxdp = (hxdoor *)li.get_current()) != NULL) {
				li.advance();
				hxdp->disconnect_client();
			}
		}
		bp->unlock();
	}
	//
	// Release the reference to disconnect_object. This will cause
	// an UNREG message to be queued to the old primary. When the
	// old primary receives this message it knows that all
	// reference counting messages from this node have been
	// received. disconnect_p is set in connect_to_primary and could
	// be nil if the primary died while we were connecting to it.
	//
	CORBA::release(disconnect_p);
	disconnect_p = nil;

	//
	// If we completed disconnecting the xdoors, clear the primary_provider
	//
	if (!CORBA::is_nil(primary_prov_p)) {
		CORBA::release(primary_prov_p);
		primary_prov_p = nil;
		//
		// Cannot set server_node to unknown yet, because if
		// (another) reconfiguration happens before whatever
		// called disconnect_clients(), it will fail to clear
		// bitmask for dead nodes in
		// rma::cleanup_foreignrefs(), because the nodeid will
		// not match the current node id. So let nodeid value
		// be that of the previous primary. When new primary
		// is selected, that value will be assigned in
		// connect_to_primary.
		//
	}
}

#ifdef FAULT_RECONF

os::mutex_t	hxdoor_service::fi_lock;
os::condvar_t	hxdoor_service::fi_cv;
bool		hxdoor_service::reconnect_blocked = false;

bool
hxdoor_service::fi_check_step3_blocked()
{
	lock();
	RMA_DBG(("%s: hxdoor_service::fi_check_step3_blocked\n", dbg_id));

	if ((hs_flags & HS_MARSHALS_BLOCKED) == 0) {
		unlock();
		return (false);
	}

	if (step3_blocked_on_reconnect) {
		unlock();
		return (true);
	}

	unlock();
	return (false);
}

void
hxdoor_service::reconnect_delay()
{
	fi_lock.lock();
	reconnect_blocked = true;
	while (reconnect_blocked) {
		fi_cv.wait(&fi_lock);
	}
	fi_lock.unlock();
}

void
hxdoor_service::unblock_reconnect()
{
	fi_lock.lock();
	reconnect_blocked = false;
	fi_cv.broadcast();
	fi_lock.unlock();
}

void
hxdoor_service::wait_for_blocked_reconnect()
{
	fi_lock.lock();
	reconnect_blocked = true;
	while (!reconnect_blocked) {
		fi_cv.wait(&fi_lock);
	}
	fi_lock.unlock();
}

#endif // FAULT_RECONF

//
// get_reconnect_objects
//
//  Invoked on the service primary by reconnecting clients.
//  Returns references to reconnect and disconnect objects.
//
void
hxdoor_service::get_reconnect_objects(replica_int::reconnect_object_out rcon_p,
		    replica_int::reconnect_object_out dcon_p)
{

	lock();
	ASSERT(reconnect_objp != NULL);

	RMA_DBG(("%p ret reconn obj ref\n", this));

	rcon_p = reconnect_objp->get_objref();
	//
	// Set flag to indicate that unref cannot happen yet.
	// reconnect_object_impl::unreferenced() will set unref_called
	// to true when all references go away.
	//
	reconnect_objp->unref_done(false);

	dcon_p = disconnect_objp->get_objref();
	disconnect_objp->unref_done(false);
	unlock();
}

//
// connect_to_primary
//
// Called on all nodes to reconnect all objects in this service to
// the new primary. On the new primary, we just set the node id to
// current node. We do not need to reconnect hxdoors on the primary
// because the xdoor bitmasks are maintained only for remote
// clients. Local clients are tracked by the number of references the
// replica_handler has.
//
void
hxdoor_service::connect_to_primary(replica_int::rma_repl_prov_ptr primary)
{
	rxdoor_bucket	*bp;
	uint_t		i;

	ASSERT(lock_held());

	// During RM recovery, the RM may ask us to connect to the same
	// provider again. If we're already connected, then there is nothing to
	// do.
	if (primary->_equiv(primary_prov_p)) {
		return;
	}
	ASSERT(CORBA::is_nil(primary_prov_p));
	RMA_DBG(("%s: hxdoor_service::connect_to_primary %p\n",
	    dbg_id, primary));

	//
	// If the primary we are connecting to has already died, then ignore
	// it. The RM will figure it out and pick a new one.
	// We need to do this here under the service lock for
	// synchronization with CMM reconfiguration. In particular, we need to
	// cover the case where this is done after the CMM recovery has done
	// clean up associated with the death of the new primary.
	//
	if (primary->_handler()->server_dead()) {
		return;
	}
	if (hs_flags & HS_PRIMARY) {
		//
		// HS_PRIMARY_KNOWN flag is set in the
		// become_primary() method along with HS_PRIMARY flag.
		//
		// We are primary.
		primary_prov_p =
		    replica_int::rma_repl_prov::_duplicate(primary);

		RMA_DBG(("%s: hxdoor_service::connect_to_primary, "
		    "we are primary %p\n", dbg_id, primary_prov_p));

		set_node(orb_conf::current_id_node());
		return;
	}
	//
	// get nodeid of node hosting the primary. Another option is
	// to send in the node id as a parameter register_service_with_rm().
	//
	remote_handler *hdlp = (remote_handler *)primary->_handler();
	rxdoor *xdp = (rxdoor *)hdlp->get_xdoorp();

	// hxdoor cannot be null if the the primary is remote.
	CL_PANIC(xdp != NULL);

	set_node(xdp->get_server_node());
	RMA_DBG(("%s pri on node %d\n", dbg_id, get_node().ndid));

	//
	// Setting the HS_PRIMARY_KNOWN flag causes any new reconfiguration
	// caused by the death of the new primary to block until we clear it.
	// We change the flag with service lock and the bucket locks held so
	// that we can read it with any of them held.
	//
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}

	hs_flags |= HS_PRIMARY_KNOWN | HS_RECONNECTING;
	is_dead_primary_disconnected_on_all_nodes = false;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->unlock();
	}
	//
	// We must drop the service lock here to allow unmarshals of
	// hxdoors in this service.
	//
	unlock();

#ifdef FAULT_RECONF
	if (fault_triggered(FAULTNUM_RECONF_DELAY_RECONNECT, NULL, NULL)) {
		reconnect_delay();
	}
#endif
	//
	// Get reference to reconnect object. If the service is
	// shutting down then nil references will be returned.
	//
	Environment e;

	replica_int::reconnect_object_ptr reconnect_p = nil;
	primary->get_reconnect_objects(reconnect_p, disconnect_p, e);

	if (e.exception()) {
		//
		// The primary may have died in this interval.
		//
		// Note: Setting reconnect_p to nil is ok since the
		// only operation done is to release it.
		//
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			reconnect_p = nil;
			disconnect_p = nil;
			e.clear();
		} else {
			CL_PANIC(0);
		}
		//
		// Cannot simply return from here because hs_flags
		// need to be reset.
		//
	} else {
		if (reconnect_p) {
			reconnect_hxdoors();
		}
	}

	// Clear these flags with all locks held
	lock();
	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}
	hs_flags &= ~HS_RECONNECTING;
	//
	// Wake up any CMM reconfiguration that is waiting for the
	// reconnect to complete.
	//
	reconnect_cv.broadcast();

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		//
		// Wake up any marshals which are waiting for marshals
		// to be unblocked.
		//
		bp->cv.broadcast();
		bp->unlock();
	}

	primary_prov_p = replica_int::rma_repl_prov::_duplicate(primary);
	//
	// Release reconnect object pointer to cause unreference to be
	// delivered on the primary.
	//
	// CORBA::release() checks if this is a nil reference
	//
	CORBA::release(reconnect_p);

	// Return with service lock held.
}

//
//  reconnect_hxdoors
//
//  helper function to connect each individual hxdoor.
//
void
hxdoor_service::reconnect_hxdoors()
{
	hxdoor			*hxdp;
	rx_list			*lp = lists;
	rx_list::ListIterator 	li;
	ID_node &pri_node =	get_node();
	rxdoor_bucket		*bp;
	uint_t			i;

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
		for (uint_t j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			while ((hxdp = (hxdoor *)li.get_current()) != NULL) {
				//
				// Check if this hxdoor needs
				// reconnecting
				//
				if (hxdp->must_reconnect()) {
					RMA_DBG(("%p must_reconn\n", hxdp));
					// Send register message. The
					// hxdoor can get the server
					// node id and we need not
					// pass it as param to the
					// method. But this will be
					// slightly faster, i think.
					hxdp->register_with_primary(pri_node);
				}

				li.advance();

			} // while

		} // for (j ...

		bp->unlock();
	}
}

//
// mark_down
//
// Called by the rma when the service is down.
//
void
hxdoor_service::mark_down()
{
	uint_t	i;
	rxdoor_bucket *bp;

#ifdef DEBUG
	rx_list 		*lp = lists;
	rx_list::ListIterator	li;
	hxdoor *hxdp;
#endif

	ASSERT(lock_held());

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
		bp->lock();
	}
	hs_flags &= ~HS_MARSHALS_BLOCKED;

	// Cause all new invocations to fail
	invo_fl.fail();

	for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
#ifdef DEBUG
		for (uint_t j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			while ((hxdp = (hxdoor *)li.get_current()) != NULL) {
				// Advance iterator earlier to allow
				// for list deletion
				li.advance();
				hxdp->mark_down();
			}
		}
#endif
		//
		// We have enabled marshals again. If marshals were blocked
		// by disconnect_clients because the primary was dead, we can
		// now inform them that the marshals have been enabled.
		//
		bp->cv.broadcast();
		bp->unlock();
	}
}

//
// store
//
// Allocate a unique object id. The algorithm is same as that used by
// rxdoors.  See orb/xdoor/rxdoor_mgr.cc register_rxdoor() method for
// explanation.  Called from register_hxdoor().
//
void
hxdoor_service::store(hxdoor *xdp)
{
	ASSERT(lock_held());
	rxdoor_bucket *xbp = NULL;

	CL_PANIC(hs_flags & HS_PRIMARY);

	// If id hasn't been allocated yet, get one.
	//
	// find a unique xdoor id
	//
	rxdoor *rxdp;
	do {
		if (last_object_id == XDOOR_MAXDID) {
			last_object_id = 1;
		} else {
			last_object_id++;
		}
		rxdp = lookup_rxdoor(last_object_id, &xbp);
		if (rxdp) {
			//
			// lookup_rxdoor returns pointer to a
			// locked bucket containing this
			// rxdoor. Since a rxdoor matching
			// last_object_id already exists, we
			// cannot use that id. The locked
			// bucket is not of any use to us so
			// unlock it.
			//
			xbp->unlock();
		}
	} while (rxdp != NULL);

	xdp->server_xdoor = last_object_id;

	RMA_DBG(("%p hxd_svc::store svcid=%d new xdid %d\n",
	    xdp, get_service_id(), xdp->server_xdoor));
	//
	// lookup_rxdoor returns a locked bucket for this xdoor id.
	// There is a fixed set of buckets hashed on the door ids. So
	// xbp can never by NULL.
	//
	xbp->store(xdp);
	xbp->unlock();
}

//
// wakeup_marshals
//
// Wakeup threads that were waiting for marshals to be unblocked so
// they can proceed if the destination node is still alive. If the
// destination node has died, they will return an exception to the
// caller.
//
void
hxdoor_service::wakeup_marshals()
{
	lock();
	RMA_DBG(("%s: wakeup_marshals()\n", dbg_id));
	if ((hs_flags & HS_MARSHALS_BLOCKED) != 0) {
		uint_t	i;
		rxdoor_bucket *bp;

		for (i = 0, bp = buckets; i < num_buckets; i++, bp++) {
			bp->lock();
			bp->cv.broadcast();
			bp->unlock();
		}
	}
	unlock();
}

//
// freeze_invos_to_dead_primary
//
// Called from CMM recongifuration to see if the primary for this
// service has died.
//
void
hxdoor_service::freeze_invos_to_dead_primary()
{
	lock();
	RMA_DBG(("%s freeze_invos_to_dead_primary()\n", dbg_id));

	if (!CORBA::is_nil(primary_prov_p)) {
		if (primary_prov_p->_handler()->server_dead()) {
			RMA_DBG(("%s primary %p is down\n", dbg_id,
			    primary_prov_p));
			//
			// If the last call we saw is unfreeze, we
			// need to do record_state, which may deliver
			// a event to the callbacks registered with
			// rma.
			//
			if (_sstate == replica_int::RMA_SERVICE_UNFREEZE) {
				record_state(replica_int::RMA_SERVICE_FREEZE);
			}
			unlock();
			freeze_service_invos();
			lock();
		}
	} else {
		RMA_DBG(("%s no primary\n", dbg_id));
	}
	RMA_DBG(("%s freeze_invos_to_dead_primary() done\n", dbg_id));
	unlock();
}

//
// disconnect_from_dead_primary
//
// Called during CMM reconfiguration refcnt_cleanup_step.
// We check if the current primary for this service is down and if so, we
// disconnect from this primary by detaching all underlying xdoors and
// clearing the primary pointer. This leaves the service in a state that the RM
// can hand a new primary to.
//
// We block here waiting for any reconnect that is in progress to
// complete; We stop waiting if the CMM step timeout expires or the
// CMM cancels the step due to a membership change. We use the value
// pointed to by tmoutp for the timeout, and the sequence number to
// detect the step being canceled (see comments for
// hxdoor_service::disconnect_from_dead_primary_cancel() for details
// of the implementation of refcnt_cleanup_step cancellation). Note
// that the data structure pointed to by tmoutp is allocated on the
// stack of the caller, so we should only access it from this stack
// context.
//
// If the timeout expires, then we return true, else false.
//
bool
hxdoor_service::disconnect_from_dead_primary(cmm::seqnum_t seqnum,
	os::systime *tmoutp)
{
	bool timedout = false;

	lock();
	RMA_DBG(("%s: disconnect_from_dead_primary\n", dbg_id));

	//
	// If the service is currently reconnecting, we must wait for the
	// reconnect to complete. We can't check if the primary is down
	// until this is complete, because the new primary is not set until
	// the reconnect succeeds.
	//
	os::condvar_t::wait_result wait_res = os::condvar_t::SIGNALED;

	while ((wait_res != os::condvar_t::TIMEDOUT) &&
	    reconnecting() &&
	    !reconfig_canceled(seqnum)) {

		RMA_DBG(("%s: waiting for reconnect\n", dbg_id));

#ifdef FAULT_RECONF
		step3_blocked_on_reconnect = true;
		rma::the().fi_signal_blocked();
#endif
		wait_res = reconnect_cv.timedwait(&hs_lck, tmoutp);

		RMA_DBG(("%s: wait done: primary_known %d canceled %d\n",
		    dbg_id, primary_known(), reconfig_canceled(seqnum)));
	}

#ifdef FAULT_RECONF
	step3_blocked_on_reconnect = false;
	rma::the().fi_signal_blocked();
#endif

	if (wait_res == os::condvar_t::TIMEDOUT) {

		RMA_DBG(("%s: disconnect_from_dead_primary timed out "
		    "waiting for reconnect\n", dbg_id));
		timedout = true;

	} else if (reconfig_canceled(seqnum)) {

		RMA_DBG(("%s: disconnect_from_dead_primary canceled waiting"
		    " for reconnect\n", dbg_id));

	} else if (!CORBA::is_nil(primary_prov_p) &&
	    primary_prov_p->_handler()->server_dead() && !is_down()) {
		//
		// The primary is down. Cleanup state associated with this
		// primary so that we leave things in a state where the RM
		// can plug in a new primary.
		//
		// Note that we don't need to perform disconnect_clients
		// on a service that has already been shut down.
		//
		RMA_DBG(("%s: primary %p is down\n", dbg_id, primary_prov_p));

		disconnect_clients();

		RMA_DBG(("%s: clients disconnected.\n", dbg_id));
	}
	unlock();
	return (timedout);
}

//
// disconnect_from_dead_primary_cancel
//
// Called when the CMM cancels refcnt_cleanup_step::cancel().  The
// seqnum parameter identifies the sequence number of the
// reconfiguration being canceled.  The canceled_seqnum member
// contains the sequence number of the last reconfiguration to be
// canceled. We update that while holding the service lock. After
// updating this value, we wake up any threads that may be blocked in
// hxdoor_service::disconnect_from_dead_primary().
//
// At other times, the service may be waiting on reconnect_cv until
// the reconnect object is unreferenced in enable_unreferenced(),
// become_secondary() and ~hxdoor_service.
//
void
hxdoor_service::disconnect_from_dead_primary_cancel(cmm::seqnum_t seqnum)
{
	lock();
	RMA_DBG(("%s hxdoor_service::disconnect_from_dead_primary_cancel()\n",
	    dbg_id));
	canceled_seqnum = seqnum;
	reconnect_cv.broadcast();
	unlock();
}

void
hxdoor_service::mark_dead_primary_disconnected_on_all_nodes()
{
	lock();
	if (!primary_known()) {
		RMA_DBG(("%s hxdoor_service:: Mark that dead primary "
		    "has been disconnected on all nodes.\n", dbg_id));
		is_dead_primary_disconnected_on_all_nodes = true;
		disconnect_dead_primary_on_all_nodes_cv.broadcast();
	}
	unlock();
}

void
hxdoor_service::wait_on_cmm_to_disconnect_dead_primary()
{
	ASSERT(lock_held());
	while (!is_dead_primary_disconnected_on_all_nodes) {
		disconnect_dead_primary_on_all_nodes_cv.wait(&hs_lck);
	}
	CL_PANIC(!primary_known());
	unlock();
}

//
// Get the service_state based on the last rma call received for this service.
// This helps us map rma calls into the service_states that we pass to the
// service_state callbacks.
//
replica::service_state
hxdoor_service::get_service_state()
{
	ASSERT(lock_held());
	replica::service_state ss = replica::S_DOWN;
	switch (_sstate) {
	case replica_int::RMA_SERVICE_ADD:
		ss = replica::S_UNINIT;
		break;
	case replica_int::RMA_SERVICE_FREEZE_PREPARE:
	case replica_int::RMA_SERVICE_FREEZE:
	case replica_int::RMA_SERVICE_CONNECT:
	case replica_int::RMA_SERVICE_DISCONNECT:
		ss = replica::S_UNSTABLE;
		break;
	case replica_int::RMA_SERVICE_UNFREEZE:
		ss = replica::S_UP;
		break;
	case replica_int::RMA_SERVICE_MARK_DOWN:
	case replica_int::RMA_SERVICE_SHUTDOWN:
		ss = replica::S_DOWN;
		break;
	case replica_int::RMA_SERVICE_NONE:
	default:
		ASSERT(0);
	}
	return (ss);
}
