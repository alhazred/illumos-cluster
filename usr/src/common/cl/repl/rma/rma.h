//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _RMA_H
#define	_RMA_H

#pragma ident	"@(#)rma.h	1.87	08/09/19 SMI"

#include <h/replica_int.h>
#include <orb/object/adapter.h>
#include <sys/threadpool.h>
#include <sys/dbg_printf.h>
#include <repl/rma/hxdoor.h>
#include <repl/rma/hxdoor_service.h>
#include <sys/os.h>

// The rma_prov_impl is the interface between the RM and the handler_repl_prov.
// It is created when a provider is passed to the wrap_handler_repl_prov
// method on the rma::admin object.  The calls are intercepted to perform
// hxdoor-level processing.

class rma_prov_impl : public McServerof<replica_int::rma_repl_prov> {
public:
	rma_prov_impl(replica_int::handler_repl_prov_ptr, hxdoor_service *);

	// IDL methods for replica_int::rma_repl_prov

	// get_state returns an object specifying the last call on the
	// object and the exception it returned.
	void	get_state(replica_int::rma_repl_prov_state &,
		    replica_int::repl_prov_exception &, Environment &);
	void    become_secondary(bool primary_failed, Environment &);
	void    become_secondary_wrapup(Environment &);
	void    add_secondary(replica::checkpoint_ptr,
		    const char *, Environment &);
	void	commit_secondary(Environment &);
	void    remove_secondary(replica::checkpoint_ptr, const char *,
		    Environment &);
	void    freeze_primary(Environment &);
	void    unfreeze_primary(Environment &);
	void    become_primary_prepare(const replica::secondary_seq &,
		    Environment &);
	void    become_primary(const replica::repl_name_seq &, Environment &);
	void    become_spare(Environment &);
	bool	is_receiving_ckpt(Environment &);
	uint32_t    shutdown(replica::repl_prov_shutdown_type shutdown_type,
		    Environment &);

	CORBA::Object_ptr get_root_obj(Environment &);

	void	get_reconnect_objects(replica_int::reconnect_object_out,
		    replica_int::reconnect_object_out, Environment &);

	void	_unreferenced(unref_t);
private:
	void	lock(void);
	void	unlock(void);

	bool	check_orphan(Environment &);
	void	store_exception(Environment &);

	os::mutex_t				rpi_lock;

	// Stores the information corresponding to the last invocation
	replica_int::rma_repl_prov_state	_state;
	// Stores the exception sent in the last invocation
	replica_int::repl_prov_exception	rpe;

	// Reference to the generic_repl_prov in the server domain.  We forward
	// invocations up to this wrapped reference when necessary.
	replica_int::handler_repl_prov_ptr	wrapped;

	// A pointer to the hxdoor_service object corresponding to the HA
	// service this provider serves.
	hxdoor_service				*servicep;

	rma_prov_impl(const rma_prov_impl &);
	rma_prov_impl &operator = (rma_prov_impl &);
};

#define	RMA_SERVICE_HASH_SIZE	16

class rma {
public:
	// There is one rma::reconf per node. This is registered with the
	// rm and is invoked during service reconfiguration.
	class reconf_impl : public McServerof<replica_int::rma_reconf> {
	public:
		reconf_impl();

		void	register_reconf();

		// IDL methods for replica_int::rma_reconf
		void	init_services(
			    const replica_int::init_service_seq &service_list,
			    Environment &);
		void	freeze_service_prepare(
			    replica::service_num_t, Environment &);
		void	freeze_service_invos(
			    replica::service_num_t, Environment &);
		void	unfreeze_service_invos(
			    replica::service_num_t, Environment &);
		void	connect_to_primary(
			    const replica_int::primary_info &, Environment &);
		void	block_marshals(
			    replica::service_num_t, Environment &);
		void	shutdown_check_prepare(
			    replica::service_num_t, Environment &);
		void	disconnect_clients(
			    replica::service_num_t, Environment &);
		void	mark_down(
			    replica::service_num_t, Environment &);
		void	add_service(replica::service_num_t, const char *,
			    Environment &);
		void	shutdown_service(
			    replica::service_num_t, Environment &);
		void	get_service_states(
			    const replica_int::service_num_seq &,
			    replica_int::rma_service_state_seq_out,
			    Environment &);
		bool	shutdown_ready(replica::service_num_t,
			    Environment &);

		void	_unreferenced(unref_t);

	private:
		// prevent assignment and copying
		reconf_impl(const reconf_impl &);
		reconf_impl &operator = (reconf_impl &);
	};

	// There is one rma::admin per node. This is registered in the
	// local nameserver as rma_admin, and used during provider
	// registration to wrap an rma_prov_impl object around a
	// handler_repl_prov
	class admin_impl : public McServerof<replica_int::rma_admin> {
	public:
		admin_impl();

		// IDL methods for replica::rma_admin
		replica_int::rma_repl_prov_ptr
			wrap_handler_repl_prov(
			    replica_int::handler_repl_prov_ptr,
			    replica::service_num_t, Environment &);

		void register_callback(replica::service_state_callback_ptr,
		    Environment &);

		replica_int::tid_factory_ptr get_tid_factory(Environment &);

		void	_unreferenced(unref_t);

	private:
		// prevent assignment and copying
		admin_impl(const admin_impl &);
		admin_impl &operator = (admin_impl &);
	};

	// init_service_task is used to defer a init_services
	// callback to the rgmd.
	class init_service_task : public defer_task {
	public:
		init_service_task(replica::service_state_info_list *,
		    replica::service_state_callback_ptr);
		~init_service_task();
		void execute();
	private:
		replica::service_state_callback_ptr callback_p;
		replica::service_state_info_list *ss_list;
	};

	// state_change_task is used to defer a state_changed
	// callback to the rgmd.
	class state_change_task : public defer_task {
	public:
		state_change_task(const char *service_name,
		    replica::service_state,
		    replica::service_state_callback_ptr);
		~state_change_task();
		void execute();
	private:
		replica::service_state_callback_ptr callback_p;
		replica::service_state_info ss_info;
	};

	rma();
	~rma();
	// initialize returns true if successful.
	static int	initialize();
	static rma &	the();

	void		get_service_states(
			    const replica_int::service_num_seq &,
			    replica_int::rma_service_state_seq_out);
	hxdoor_service	*add_service(replica::service_num_t, const char *);
	hxdoor_service	*lookup_service(replica::service_num_t);
	hxdoor_service	*lookup_service_unlocked(replica::service_num_t);

	void		shutdown_service(
			    replica::service_num_t);

	void		register_rma();

	replica_int::rma_reconf_ptr get_reconf();

	// Functions related to handling service_state callbacks.
	//
	void state_changed(hxdoor_service *, replica::service_state);
	void register_callback(replica::service_state_callback_ptr);
	void remove_callback(replica::service_state_callback_ptr);
	//
	// Called during quiesce_step of CMM reconfiguration. We freeze client
	// invocations for all services whose server is now dead. We also
	// clean up services that has been shutdown.
	//
	void		freeze_invos_to_dead_primaries();
	//
	// Called from refcnt_cleanup_step of CMM reconfiguration to cleanup
	// reference counting information associated with nodes which
	// have died.
	//
	void		cleanup_foreignrefs(callback_membership *,
			    callback_membership *);

	//
	// Called during refcnt_cleanup_step of CMM reconfiguration. We
	// freeze marshals and detach xdoors for all services whose
	// server is now dead.
	//
	bool		disconnect_from_dead_primaries(cmm::seqnum_t seqnum,
			    os::systime *tmout);
	void		disconnect_from_dead_primaries_cancel(
			    cmm::seqnum_t seqnum);

	//
	// Called during rmm_cleanup_step of CMM reconfiguration. At this
	// point, we know for sure that all nodes have completed disconnecting
	// from dead primaries if there are any and the corresponding state
	// machines can move forward from PRIMARY_DEAD state.
	//
	void		mark_dead_primaries_disconnected_on_all_nodes();

	// Tells the rma the the RM is in sync with the cluster.
	void		rm_in_sync();
	// Blocks until the RM is synced up.
	void		wait_rm_sync();

	void		lock();
	void		unlock();
	bool		lock_held();

#ifdef FAULT_RECONF
	// Called by fi code to see if a service is blocked in step 3.
	void		wait_for_step3_blocked();
	// Called by fi code to see if no service is blocked in step 3.
	void		wait_for_step3_unblocked();
	// Called by hxdoor code when a thread blocks in step 3
	void		fi_signal_blocked();
#endif

#ifdef DEBUG
	bool		has_callback(); // whether callback_list has members.
#endif

#ifdef DEBUGGER_PRINT
	int		mdb_dump_all_services();
	int		mdb_dump_service(ushort_t id);
#endif

private:
	admin_impl	the_admin;
	reconf_impl	the_reconf;

	bool		reconfig_in_progress;
	cmm::seqnum_t	canceled_seqnum;

	// Whether the local RM is synced up with other RM in the cluster.
	// A node can't join the cluster before the RM is synced up.
	bool		rm_synced;

	// Used to protect everything above.
	os::mutex_t	lck;
	os::condvar_t	cv;

	//
	// The list of services referenced on this node.
	// For now use an array of lists since we need to walk through this
	// list and the hash_table_t template does not yet support an
	// iterator.
	//
	DList<hxdoor_service> services[RMA_SERVICE_HASH_SIZE];

	//
	// Write lock is required when adding or removing a service,
	// otherwise a read lock is required to access "services".
	// A reader/writer lock is required in order to avoid significant
	// lock contention when performing lookups of hxdoors.
	//
	os::rwlock_t	services_lock;

	//
	// The service_state_callback. We have one rgmd for each zone
	// cluster and one rgmd for the physical cluster. Hence, we
	// are going to have a list for callbacks. Currently, only
	// rgmd registers for callbacks with RMA.
	//
	SList<replica::service_state_callback> callback_list;

	//
	// The threadpool for delivering callbacks. It'll be a
	// real time threadpool with just one thread.
	//
	threadpool callback_threadpool;

	//
	// Used to protect the callback_list. This is a bottom level lock
	// so don't try to grab any other ha lock while holding it. When
	// using this lock the following is always true: when two threads
	// are trying to register callback/events for the same service, the
	// thread that gets the rma::lck first always gets the
	// rma::callback_lck first. Because we need to get the service
	// information under the rma and hxdoor_service lock, the above
	// ensures correctness.
	//
	os::mutex_t callback_lck;

	static rma *the_rmap;

#ifdef FAULT_RECONF
	// Synchronization for fi blocking code.
	os::mutex_t	fi_lock;
	os::condvar_t	fi_cv;
	bool		fi_check_step3_blocked();
#endif

	// prevent assignment and copying
	rma(const rma &);
	rma &operator = (rma &);
};

#include <repl/rma/rma_in.h>

#endif	/* _RMA_H */
