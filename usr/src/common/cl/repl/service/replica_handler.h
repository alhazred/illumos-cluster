//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _REPLICA_HANDLER_H
#define	_REPLICA_HANDLER_H

#pragma ident	"@(#)replica_handler.h	1.84	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/handler/remote_handler.h>
#include <repl/service/repl_service.h>
#include <orb/debug/haci_debug.h>
#include <orb/refs/unref_threadpool.h>
#ifdef _KERNEL_ORB
#include <repl/rma/hxdoor.h>
#include <repl/service/ckpt_handler.h>
#endif

class replica_handler_kit : public remote_handler_kit {
public:
	replica_handler_kit();
	remote_handler	*create(Xdoor *);
	void	unmarshal_cancel_remainder(service &);

	static replica_handler_kit &the();
	static int initialize();
	static void shutdown();
private:
	static replica_handler_kit *the_replica_handler_kit;
};


//
// Replica handler.  Used for replicated objects.
//
// Handlers are associated with a repl_service which knows the state of
// the service.  The interesting service states from the handler's perspective
// are PRIMARY, SECONDARY and CLIENT.  The same handler is used for all states
// (there is no client handler or server handler), though some
// operations are only valid for certain states - the handler checks with
// its repl_service before performing any of these.
//
class replica_handler :
	public remote_handler,
	public unref_task {
public:
	hkit_id_t			handler_id();

	// Returns the handler to be used by the unref_task
	virtual handler		*get_unref_handler();

	// Returns the unref_task type
	virtual unref_task_type_t	unref_task_type();

	//
	// Create a new proxy object which is used on the primary for
	// local invocations.
	// This should only be called by a template which knows the
	// type of the proxy being created and cast to.
	//
	void			*create_proxy(InterfaceDescriptor *actual_type,
				    InterfaceDescriptor *deep_type);

	void			revoke();
	void			marshal(service &, CORBA::Object_ptr);
	CORBA::Object_ptr	unmarshal(service &, CORBA::TypeId,
				    generic_proxy::ProxyCreator, CORBA::TypeId);

	bool			is_local();
	ExceptionStatus		invoke(arg_info &, ArgValue *,
				    InterfaceDescriptor *, Environment &);
	void			handle_incoming_call(service &);

	virtual void		*narrow(CORBA::Object_ptr, CORBA::TypeId,
				    generic_proxy::ProxyCreator);

	virtual void		*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void			release(CORBA::Object_ptr);

	void			new_reference();

	bool			is_user();

	repl_service		*get_service();

	void			set_service(repl_service *);

#ifdef _KERNEL_ORB
	void			*local_invoke(CORBA::TypeId,
				    InterfaceDescriptor *, Environment &);
	ExceptionStatus		local_invoke_done(Environment &);

	// Acquires handler lock and return handler's marshal count
	virtual uint_t		begin_xdoor_unref();

	//  If the xdoor has been unreferenced, decides if the handler
	// can be unreferenced too.
	virtual void		end_xdoor_unref(bool);

	// Decides if a handler on the unref_threadpool can be ureferenced.
	void			deliver_unreferenced();

	// Returns true when unreference can be performed at this time.
	bool			last_unref();

	// Returns true if a freeze_primary() is in progress
	bool			freeze_in_progress(Environment *);

	// Unreferenced by a checkpoint.
	void			unref_by_ckpt();

	// Associate an implementation object with the handler.
	void			add_impl(CORBA::Object_ptr obj);
	void			rem_impl();

	//
	// Get the service_id of the underlying hxdoor.
	// This is only to be called when there is a hxdoor.
	//
	replica::service_num_t get_service_id();

	//
	// Get the xdoor_id of the underlying hxdoor.
	// This is only to be called when there is a hxdoor.
	//
	xdoor_id get_object_id();

	//
	// When a queued unreferenced on the defer list
	// is to be discarded, update the handler state
	// to show that an unreference is not pending.
	//
	void			clear_unref_flag();

	// Notify handler that it has become a primary.
	virtual void		become_primary_event();

	// Notify handler that it has become a secondary.
	virtual void		become_secondary_event();

	// Constructor used when creating a new object on the primary.
	replica_handler(repl_service *s, CORBA::Object_ptr objp);

	// server_dead returns service state.
	// Returns true if the service is DOWN.
	//
	bool	server_dead();

	//
	// Identify nodes holding references
	//
	void	identify_nodes_holding_refs(nodeset &refset);

#endif // _KERNEL_ORB

	// Constructor used when unmarshaling a new client reference.
	replica_handler(Xdoor *);

	//
	// The handler is deleted on clients when all references go away and
	// on servers (primary or secondary) when the object is deleted
	// (which is only legal after unreferenced is delivered).
	//
	~replica_handler();

private:
	// To access sensitive data.
	void		lock();
	void		unlock();
	bool		lock_held();

	void		incref();

	// Utilities for managing the reference count in the proxies.
	void	incpxref(CORBA::Object_ptr);
	uint_t	decpxref(CORBA::Object_ptr);

	CORBA::Object_ptr _obj();

	//
	// Number of proxies.  Note that all references, whether
	// primary, secondary or client go through proxies.
	//
	uint_t		_nref;

	// The replicated service associated with this handler.
	repl_service	*_servicep;

	// Record handler state
	handler_state_t	state_;

	//
	// We may or may not have an implementation object associated with
	// the handler.
	//
	// Primary - always has an implementation object.
	//
	// Secondary - has an implementation object,
	// EXCEPT for a period where we learn about the object through a
	// checkpoint message and have not yet made the appropriate call to
	// convert this to a secondary reference. However, no unreference
	// action will occur during this period (an assert checks this fact).
	//
	// Client only - does not have an implementation object,
	// but does have a proxy object pointer.
	//
	CORBA::Object_ptr _impl_object;
	CORBA::Object_ptr proxy_obj_;

	os::mutex_t	_lck;

	// Disallow assignments and pass by value.
	replica_handler(const replica_handler &);
	replica_handler & operator = (replica_handler &);
};

class transaction_state;
class ha_transaction_id;

//
// trans_ctx - Common parent for all the transacton context objects.
// This parent class identifies the type of the context object.
// Also contains a reference to the HA transaction id object.
//
class trans_ctx {
public:
	typedef enum {
		CLIENT,
		PRIMARY,
		SECONDARY
	} context_type;

	context_type get_type();

	virtual ~trans_ctx();

	// Get the HA transaction id object reference.
	replica::transaction_id_ptr	get_trans_id();

protected:
	trans_ctx(context_type, replica::transaction_id_ptr);

	//
	// The HA transaction id object reference
	//
	// The client_ctx obtains this object reference.
	// Only the client_ctx performs the release on this object reference.
	//
	// This means that no reference counting occurs when the object
	// reference is held by another type of context. This means that
	// we cannot destroy a client_ctx until after all other contexts
	// that have this reference are destroyed. This does not follow
	// recommended practice of reference counting all references,
	// but it can work.
	//
	replica::transaction_id_ptr	trans_id_p;

private:
	//
	// We never create an object of this type.
	// This identifies the kind of child class.
	//
	context_type	type;
};

//
// Client's context is the transaction id and its object.
//
class client_ctx
	: public trans_ctx
{
	friend class primary_ctx;
public:
	client_ctx(replica_handler *);

	~client_ctx();

	//
	// Called when beginning an invocation to set up the transaction
	// id data if necessary.
	//
	static client_ctx	*begin(replica_handler *, Environment &);

	//
	// Called when the mini-transaction is completed (we got a reply or
	// an exception).
	//
	void	completed(Environment &e);

	replica_handler		*handlerp;

private:
	// disallow
	client_ctx(const client_ctx &);
	client_ctx & operator = (client_ctx &);
};

class orphan_thread;

#ifdef _KERNEL_ORB
//
// The primary transaction context is passed in the environment to all
// invocations on replicated objects.
// We may be a client_ctx too.
//
class primary_ctx
	: public trans_ctx
{
	friend class replica_handler;
	friend class orphan_thread;
	friend class ha_transaction_id;
	friend class transaction_state;
	friend class multi_ckpt_handler;
	friend class ckpt_handler_client;
public:
	//
	// Enum identifies the purpose of the primary_ctx.
	//
	// The following 2 values are temporary because some services
	// do checkpoints on these operations even though that is
	// not officially supported.
	//	ADD_SECONDARY_CKPT, REMOVE_SECONDARY_CKPT
	//
	typedef enum {
		MINI_TRANSACTION,	// invoked from a mini-transaction
		PUSH_CKPT,		// invoked from push_ckpt_task
		BECOME_PRIMARY_CKPT,	// invoked from become_primary
		BECOME_SECONDARY_CKPT,	// invoked from become_secondary
		ADD_SECONDARY_CKPT,	// invoked from add_secondary
		REMOVE_SECONDARY_CKPT	// invoked from remove_secondary
	} invoke_env;

	// Constructors
	primary_ctx(replica_handler *);
	primary_ctx(replica_handler *, invoke_env, Environment &e);

	~primary_ctx();

	// Extract the primary_ctx from the environment.
	static primary_ctx	*extract_from(Environment &e);

	// Get the saved transaction state.
	transaction_state	*get_saved_state();

	// The following are only used by the HA framework.

	//
	// Whether the mini-transaction needs an add_commit. If a mini
	// transaction ever did a checkpoint, we will need to send a commit.
	// If the service writer did a commit, the framework will not
	// send anymore commit.
	//
	bool	need_commit();

	// Set needcommit to true.
	void	needcommit_yes();

	// Set needcommit to false;
	void	needcommit_no();

	//
	// The primary context is no longer needed because of a retry.
	// Clean up state and destroy the primary context.
	//
	void	discard_context();

private:
	// Helper function. Setup for the local invocation.
	void	setup_local(client_ctx *c_ctxp, Environment &e);
	void	add_client_ctx(Environment &e);

	// Called from replica_handler.
	static void	begin_local(replica_handler *, Environment &e);

	void	completed_local(Environment &e);

	void	begin_remote(replica::transaction_id_ptr tr_id,
	    replica_handler *h, Environment &e);

	void	primary_ctx::completed_remote(Environment &e);

	void	get_saved_state(replica_handler *h, Environment &e);

	// Only called from orphan_thread::process_orphaned().
	void	begin_orphaned(replica::transaction_id_ptr trid,
	    transaction_state *st);

	void	completed_orphaned(Environment &e);

	// True if commit is needed for this transaction
	bool		needcommit;

	// Indicates the environment where the primary_ctx is created.
	invoke_env	env_type;

	// The client_ctx. We need this info during local invocation.
	client_ctx	*clnt_ctx;

	//
	// Any saved state for this transaction. A non-NULL value indicates
	// that the current invocation is a retry operation and that
	// some state was found. Note that this can be a retry even if
	// the saved_state is NULL.
	//
	transaction_state	*saved_state;

	//
	// The replica_handler which is being invoked
	// This is non-NULL for invocations and NULL for orphan requests
	// and push checkpoints.
	//
	replica_handler		*handlerp;

	// disallow
	primary_ctx(const primary_ctx &);
	primary_ctx & operator = (primary_ctx &);
};

//
// The secondary transaction context is passed with all checkpoint invocations.
//
class secondary_ctx
	: public trans_ctx
{
	friend class ckpt_handler_server;
	friend class state_hash_table_t;
	friend class transaction_state;
public:
	// Extract the secondary_ctx from the environment.
	static secondary_ctx	*extract_from(Environment &e);

	// Get the transaction state.
	transaction_state	*get_saved_state();

private:
	secondary_ctx(Environment &e);
	~secondary_ctx();


	void	begin(replica::transaction_id_ptr tr_id);
	void	completed(Environment &e);

	// The transaction state that matches the trans_id.
	transaction_state	*saved_state;

	// The checkpoint handler this came through.
	ckpt_handler_server	*handlerp;

	// disallow
	secondary_ctx(const secondary_ctx &);
	secondary_ctx & operator = (secondary_ctx &);
};
#endif // _KERNEL_ORB

#if defined(HACI_DBGBUFS) && !defined(NO_REPL_DBGBUF)
#define	REPL_DBGBUF
extern dbg_print_buf repl_dbg;
#define	REPL_DBG(args) HACI_DEBUG(ENABLE_REPL_DBG, repl_dbg, args)
#else
#define	REPL_DBG(args)
#endif

#ifndef NOINLINES
#include <repl/service/replica_handler_in.h>
#endif  // _NOINLINES

#endif	/* _REPLICA_HANDLER_H */
