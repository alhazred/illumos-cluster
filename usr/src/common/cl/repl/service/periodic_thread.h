/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PERIODIC_THREAD_H
#define	_PERIODIC_THREAD_H

#pragma ident	"@(#)periodic_thread.h	1.11	08/05/20 SMI"

#include <orb/infrastructure/orbthreads.h>

// The thread that executes the jobs in it periodically
// (interval is sleeptime). For an example of how to use
// it see repl_service.cc
class periodic_thread : public orbthread {
public:
	static int initialize();
	void shutdown();

	// The job that's executed periodically.
	class periodic_job {
	public:
		// Execute() is called by periodic_thread periodically.
		virtual void execute() = 0;

		// Return how often the job is run. In unit of sleeptime.
		virtual uint_t get_period() = 0;

		virtual ~periodic_job();
	};

	periodic_thread();

	static periodic_thread &the();

	// Start the thread.
	int initialize_int();

	// Add a job to be processed periodically.
	void add_job(periodic_job *);
	// Remove a job.
	void remove_job(periodic_job *);

private:
	// Preallocated thread.
	static void the_thread(void *arg);

	// The main function.
	void main_loop();

	// The list of jobs.
	// CSTYLED
	SList<periodic_job> jobs;

	// current tick. In unit of sleeptime.
	uint_t tick;

	os::usec_t sleeptime;

	static periodic_thread *the_periodic_thread;
};

#endif	/* _PERIODIC_THREAD_H */
