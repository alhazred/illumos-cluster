/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  replica_tmpl_in.h
 *
 */

#ifndef _REPLICA_TMPL_IN_H
#define	_REPLICA_TMPL_IN_H

#pragma ident	"@(#)replica_tmpl_in.h	1.11	08/05/20 SMI"

//
// class mc_checkpoint inline methods
//

template<class T>
inline
mc_checkpoint<T>::mc_checkpoint() :
	handler_(this)
{
}

template<class T>
inline T *
mc_checkpoint<T>::get_objref()
{
	handler_.new_reference();
	return ((T *)this);
}

//
//  Revoke the object.
//  operation returns after all current invocations have terminated.
//
template<class T>
inline void
mc_checkpoint<T>::revoke()
{
	handler_.revoke();
}

template<class T>
inline void
mc_checkpoint<T>::prov_info(generic_repl_prov *grp)
{
	handler_.set_state_info(grp->get_state_hash());
}

//
// class repl_server inline methods
//

inline McServerof<replica::repl_prov> *
generic_repl_server::get_prov()
{
	return ((McServerof<replica::repl_prov> *)this);
}

template<class CKPT>
inline mc_checkpoint<CKPT> *
repl_server<CKPT>::get_ckpt()
{
	return ((mc_checkpoint<CKPT> *)this);
}

//
// Called by service when it is ready to get started.
// By the time this returns this server may already have been
// converted to a primary or secondary.
// When this is called, the framework takes responsibility for deleting
// this object, whether or not the registration succeeds.
//
inline void
generic_repl_server::register_with_rm(
    uint_t priority,
    bool restart_flag, // Restart the service if it has failed.
    Environment &e)
{
	_my_repl_prov->register_with_rm(NULL, NULL, priority,
	    restart_flag, MAX_SECONDARIES, e);
}

// Same as above, but with dependencies.
inline void
generic_repl_server::register_with_rm(uint_t priority,
    replica::service_dependencies *service_depends_on,
    replica::prov_dependencies *provider_depends_on,
    bool restart_flag,
    Environment &e)
{
	_my_repl_prov->register_with_rm(
	    service_depends_on, provider_depends_on, priority,
	    restart_flag, MAX_SECONDARIES, e);
}

// Same as the first register_with_rm(), but with desired number of secondaries
inline void
generic_repl_server::register_with_rm(
    uint_t priority,
    bool restart_flag, // Restart the service if it has failed.
    uint32_t new_desired_numsecondaries,
    Environment &e)
{
	_my_repl_prov->register_with_rm(NULL, NULL, priority,
	    restart_flag, new_desired_numsecondaries, e);
}

//
// Same as the first register_with_rm(), but with dependencies and desired
// number of secondaries.
//
inline void
generic_repl_server::register_with_rm(uint_t priority,
    replica::service_dependencies *service_depends_on,
    replica::prov_dependencies *provider_depends_on,
    bool restart_flag,
    uint32_t new_desired_numsecondaries,
    Environment &e)
{
	_my_repl_prov->register_with_rm(
	    service_depends_on, provider_depends_on, priority,
	    restart_flag, new_desired_numsecondaries, e);
}

//
// Called by service when it is ready to get started.
// By the time this returns this server may already have been
// converted to a primary or secondary.
//
inline void
generic_repl_server::request_primary_mode()
{
	_my_repl_prov->request_primary_mode();
}

#ifdef	_KERNEL_ORB
// Only used by the RMM, which has it's own registration path
inline replica_int::handler_repl_prov_ptr
generic_repl_server::register_for_rmm()
{
	_my_repl_prov->register_for_rmm();
	return (_my_repl_prov->get_objref());
}
#endif

//
// class mc_replica_of inline methods
//

//
// Used by secondary to associate an implementation object with a
// reference passed from the primary.
// With rolling upgrade, there are three possible cases:
//	Secondary implementation and primary object deep type are the same.
//	Secondary implementation is newer than the primary object deep type.
//	Secondary implementation is older than the primary object deep type.
//
template<class T>
inline
mc_replica_of<T>::mc_replica_of(CORBA::Object_ptr obj)
{
	//
	// Make sure that the secondary implementation supports the deep type
	// (i.e., secondary is not older than the primary.)
	//
	ASSERT(T::_interface_descriptor()->is_type_supported(
	    obj->_deep_type()->get_tid()));

	_handlerp = (replica_handler *)obj->_handler();
	_handlerp->add_impl(_this_obj());
	ASSERT(!_get_service()->is_primary());
}

//
// Equivalent to get_objref() in McServerof template.
//
// Create a new reference by allocating a new proxy.
//
template<class T>
inline T *
mc_replica_of<T>::get_objref()
{
	return ((T *)_handlerp->create_proxy(T::_interface_descriptor(),
	    T::_interface_descriptor()));
}

//
// Equivalent to get_objref above, except that it allocates a proxy of a
// designated type. Designated type must be supported by deep type.
//
template<class T>
inline T *
mc_replica_of<T>::get_objref(CORBA::type_info_t *tp)
{
	return ((T *)_handlerp->create_proxy(T::_interface_descriptor(),
	    (InterfaceDescriptor *)tp));
}

template<class T>
inline handler *
mc_replica_of<T>::_handler()
{
	return (_handlerp);
}

//
// Returns true when it is safe to delete the implementation object.
//
// Equivalent to _last_unref() in McServerof template.
//
template<class T>
inline bool
mc_replica_of<T>::_last_unref(unref_t)
{
	return (_handlerp->last_unref());
}

//
// Commit a transaction.
//
template<class T>
inline void
mc_replica_of<T>::add_commit(Environment &e)
{
	primary_ctx *ctxp = primary_ctx::extract_from(e);

	//
	// It's possible that user says add_commit but there is
	// no secondary.
	//
	if (ctxp->need_commit()) {
		// Only send one commit.
		ctxp->needcommit_no();
		_get_repl_prov()->add_commit(ctxp->get_trans_id());
	}
	//
	// FAULT POINT
	// This fault point allows injection after a transaction
	// has been committed
	//
	FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT,
	    FaultFunctions::generic);
}

template<class T>
inline repl_service *
mc_replica_of<T>::_get_service()
{
	return (_handlerp->get_service());
}

template<class T>
inline generic_repl_prov *
mc_replica_of<T>::_get_repl_prov()
{
	return (_get_service()->get_repl_provp());
}

template<class T>
replica::repl_prov_ptr
mc_replica_of<T>::get_provider()
{
	return (_get_repl_prov()->get_implp());
}

#endif	/* _REPLICA_TMPL_IN_H */
