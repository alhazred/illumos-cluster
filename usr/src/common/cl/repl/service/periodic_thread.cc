/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)periodic_thread.cc	1.16	08/05/20 SMI"

#include <repl/service/periodic_thread.h>
#include <orb/infrastructure/clusterproc.h>

periodic_thread *periodic_thread::the_periodic_thread = NULL;

int
periodic_thread::initialize()
{
	ASSERT(the_periodic_thread == NULL);
	the_periodic_thread = new periodic_thread;
	if (the_periodic_thread == NULL)
		return (ENOMEM);
	return (the_periodic_thread->initialize_int());
}

void
periodic_thread::shutdown()
{
	ASSERT(the_periodic_thread == this);
	the_periodic_thread = NULL;
	delete this;
}

periodic_thread::periodic_thread() : tick(0), sleeptime(10000000)
{
}

periodic_thread &
periodic_thread::the()
{
	ASSERT(the_periodic_thread != NULL);
	return (*the_periodic_thread);
}

int
periodic_thread::initialize_int()
{
	//
	//  register ourselves by linking into the list of orb created
	//  threads.
	//
	registerthread();

#ifdef _KERNEL
	if (clnewlwp((void(*)(void *))periodic_thread::the_thread,
	    NULL, 60, NULL, NULL) != 0) {
#else
	if (os::thread::create(NULL, (size_t)NULL,
	    (void *(*)(void *))periodic_thread::the_thread,
	    NULL, (long)THR_BOUND|THR_DETACHED, NULL)) {
#endif

#ifdef DEBUG
		os::warning("failed to create thread for periodic_thread");
#endif
		(void) unregisterthread();
		return (ENOMEM);
	}
	return (0);
}

void
periodic_thread::the_thread(void *)
{
	the().main_loop();
}

void
periodic_thread::main_loop()
{
	periodic_job *the_job;
	lock();
	while (state == orbthread::ACTIVE) {
		if (jobs.empty()) {
			threadcv.wait(&threadLock);
		} else {
			SList<periodic_job>::ListIterator iter(jobs);
			for (;
			    (the_job = iter.get_current()) != NULL;
			    iter.advance()) {
				if (tick % the_job->get_period() == 0) {
					the_job->execute();
				}
			}
			tick++;
			os::systime time_out;
			time_out.setreltime(sleeptime);
			(void) threadcv.timedwait(&threadLock, &time_out);
		}
	}

	state = orbthread::DEAD;
	jobs.dispose();
	threadGone.signal();
	unlock();
}

void
periodic_thread::add_job(periodic_job *newjob)
{
	lock();
	if (jobs.empty()) {
		threadcv.signal();
	}
	jobs.append(newjob);
	unlock();
}

void
periodic_thread::remove_job(periodic_job *oldjob)
{
	lock();
	(void) jobs.erase(oldjob);
	unlock();
}

periodic_thread::periodic_job::~periodic_job()
{
}
