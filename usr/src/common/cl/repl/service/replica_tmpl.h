/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPLICA_TMPL_H
#define	_REPLICA_TMPL_H

#pragma ident	"@(#)replica_tmpl.h	1.72	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/ix.h>
#include <repl/service/ckpt_handler.h>
#include <repl/service/replica_handler.h>
#include <repl/service/repl_service.h>
#include <repl/service/transaction_state.h>

#ifdef _KERNEL_ORB
#include <repl/rma/hxdoor.h>
#endif

#include <orb/fault/fault_injection.h>

// This is also defined in rm_repl_service.h
#define	MAX_SECONDARIES	(NODEID_MAX - 1)

//
// Checkpoint objects must be declared using the mc_checkpoint template
// This is similar to the mc_checkpoint template (e.g. reference counting),
// except that it has some special requirements for the invoke interface.
//
template <class T> class mc_checkpoint : protected T {
public:
	mc_checkpoint();

	virtual		~mc_checkpoint();

	virtual handler	*_handler();

	// IDL methods

	virtual void	initialize_seq(replica::ckpt_seq_t seq, Environment &);

	virtual void	get_high(replica::ckpt_seq_t &seq, Environment &);

	virtual void	process_to(replica::ckpt_seq_t seq, Environment &);

	virtual void	flush_to(replica::ckpt_seq_t seq, Environment &);

	virtual void	stop_receiving_ckpt(Environment &);

	virtual void	start_receiving_ckpt(Environment &);

	virtual void	ckpt_noop(Environment &);

	T		*get_objref();

	//
	//  Revoke the object.
	//  operation returns after all current invocations have terminated.
	//
	void		revoke();

protected:
	void		prov_info(generic_repl_prov *grp);

private:
	ckpt_handler_server handler_;
};

class generic_repl_server :
	public McServerof<replica::repl_prov> {

public:
	McServerof<replica::repl_prov>	*get_prov();

	virtual ~generic_repl_server();

	//
	// Called by service when it is ready to get started.
	// By the time this returns this server may already have been
	// converted to a primary or secondary.
	// When this is called, the framework takes responsibility for deleting
	// this object, whether or not the registration succeeds.
	//
	void		register_with_rm(uint_t priority,
	    bool restart_flag, // Restart the service if it has failed.
	    Environment &e);

	// Same as above, but with dependencies.
	void		register_with_rm(uint_t priority,
	    replica::service_dependencies *service_depends_on,
	    replica::prov_dependencies *provider_depends_on,
	    bool restart_flag,
	    Environment &e);

	//
	// Same as the first register_with_rm(), but with desired number of
	// secondaries.
	//
	void		register_with_rm(uint_t priority,
	    bool restart_flag, // Restart the service if it has failed.
	    uint32_t new_desired_numsecondaries, Environment &e);

	//
	// Same as the first register_with_rm(), but with dependcies and
	// desired number of secondaries.
	//
	void		register_with_rm(uint_t priority,
	    replica::service_dependencies *service_depends_on,
	    replica::prov_dependencies *provider_depends_on,
	    bool restart_flag,
	    uint32_t new_desired_numsecondaries, Environment &e);

	//
	// Called by service when it is ready to get started.
	// By the time this returns this server may already have been
	// converted to a primary or secondary.
	//
	void		request_primary_mode();

	//
	// Called on a spare provider to shut it down. This is to serve
	// as an additional event to help the service to do clean up.
	// Normally ha services shouldn't need to do anything in this
	// function, instead they should be able to clean everything
	// up in the become_spare function and spares shouldn't have
	// anything more to clean.
	//
	virtual void	shutdown_spare(replica::repl_prov_shutdown_type,
			    Environment &);

	// Returns a Solaris error code.
	virtual uint32_t	forced_shutdown(Environment &);

	// Only used by constructor of mc_replica_of objects.
	virtual repl_service	*get_service();

#ifdef	_KERNEL_ORB
	// Only used by the RMM, which has it's own registration path
	replica_int::handler_repl_prov_ptr register_for_rmm();
#endif

	// Set up the checkpoint handlers.
	replica::checkpoint_ptr		set_checkpoint(CORBA::type_info_t *);

protected:
	//
	// The underlying _my_repl_prov takes care of the references for
	// the repl_prov and checkpoint interfaces of this service.
	// It also ensures that the primary provides the appropriate
	// proxy for the checkpoint interface when this repl_prov is
	// primary.
	//
	generic_repl_server();

	//
	// Each service has its own repl_prov which intercepts the repl_prov
	// implementation doing all the work that is common to all repl_provs.
	//
	generic_repl_prov	*_my_repl_prov;
};

//
// Replica server objects must be declared using the repl_server template.
// This creature is unusual in that it hands out two different interfaces.
//
template<class CKPT> class repl_server :
	public generic_repl_server,			// First interface
	public mc_checkpoint<CKPT> {			// Second interface
public:
	virtual				~repl_server();

	mc_checkpoint<CKPT>		*get_ckpt();

	//
	// We give one reference of each interface to generic_repl_prov,
	// which will release them when it's ready to be deleted.
	// We must wait till we get the _unreferenced from both interfaces
	// before we delete this.
	// There are no other references going around.
	//
	void		_unreferenced(unref_t);

protected:
	repl_server(const char *svc_id, const char *prov_id);

private:
	//
	// The number of _unreferenced delivered to this object. When this
	// count reaches two (one for each handler) we deliver unreferenced
	// to the child class.
	//
	uint16_t		count_unref;
};

//
// Replicated objects must be declared using the mc_replica_of template.
//
template<class T> class mc_replica_of :
	protected T
{
public:
	//
	// Used by replication-aware code on the primary when it creates
	// new objects
	//
	mc_replica_of(generic_repl_server *grsp);

	//
	// Used by secondary to associate an implementation object with a
	// reference passed from the primary.
	//
	mc_replica_of(CORBA::Object_ptr obj);

	virtual		~mc_replica_of();

	//
	// Equivalent to get_objref() in McServerof template.
	//
	// Create a new reference by allocating a new proxy.
	//
	T		*get_objref();

	//
	// Equivalent to get_objref() above, but gets a proxy of the
	// designated type. Designated type must be supported by deep type.
	//
	T		*get_objref(CORBA::type_info_t *);

	//
	// Returns true when it is safe to delete the implementation object.
	//
	// Equivalent to _last_unref() in McServerof template.
	//
	bool		_last_unref(unref_t);

	virtual handler	*_handler();

	//
	// Returns true if the service is currently being frozen.
	// For use by methods that want to force a retry if a freeze is in
	// progress.
	//
	bool	_freeze_in_progress(Environment *e);

	//
	// Commit a transaction.
	//
	void		add_commit(Environment &e);

	// Returns a pointer to the provider object.
	replica::repl_prov_ptr	get_provider();

private:
	repl_service	*_get_service();

	generic_repl_prov *_get_repl_prov();

	replica_handler *_handlerp;
};

#include <repl/service/replica_tmpl_in.h>

#endif	/* _REPLICA_TMPL_H */
