/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  transaction_state_in.h
 *
 */

#ifndef _TRANSACTION_STATE_IN_H
#define	_TRANSACTION_STATE_IN_H

#pragma ident	"@(#)transaction_state_in.h	1.23	08/05/20 SMI"

// Undo the get_objref in get_state().
inline void
state_hash_table_t::get_state_done(transaction_state *st)
{
	if ((st != NULL) && (!st->is_orphaned()) && (!st->is_completed())) {
		//
		// Release the object reference and clear objref. We need to
		// clear the objref value before the CORBA::release() because
		// "this" can be deleted at any time after the release.
		//
		CORBA::Object_ptr tmp_objref = st->objref;
		ASSERT(!CORBA::is_nil(st->objref));
		st->objref = nil;
		CORBA::release(tmp_objref);
	}
}

inline
state_hash_table_t::state_hash_table_t(generic_repl_prov *g) :
	repl_provp(g),
	num_uncmt(0),
	role(SPARE)
{
}

inline
state_hash_table_t::~state_hash_table_t()
{
}

inline generic_repl_prov *
state_hash_table_t::get_repl_prov()
{
	return (repl_provp);
}

inline void
state_hash_table_t::lock()
{
	lck.lock();
}

inline void
state_hash_table_t::unlock()
{
	lck.unlock();
}

inline bool
state_hash_table_t::is_primary()
{
	ASSERT(lck.lock_held());
	return (role == PRIMARY);
}

inline bool
state_hash_table_t::is_secondary()
{
	ASSERT(lck.lock_held());
	return (role == SECONDARY);
}

inline bool
state_hash_table_t::is_spare()
{
	ASSERT(lck.lock_held());
	return (role == SPARE);
}

inline
transaction_state::transaction_state() :
	objref(NULL),
	flags(0)
{
}

inline bool
transaction_state::is_committed()
{
	return (flags & COMMITTED);
}

inline void
transaction_state::frmwrk_committed()
{
	committed();
	set_commit();
	state_hash->st_committed();
}

inline void
transaction_state::invoke_completed()
{
	// If the application didn't call locks_recovered, we do it.
	if (!is_lockrvd()) {
		locks_recovered();
	}

	//
	// Undo the get_objref() in state_hash_table_t::lookup_state.  Release
	// the object reference and clear objref. We need to clear the objref
	// value before the CORBA::release() because "this" can be deleted at
	// any time after the release.
	//
	CORBA::Object_ptr tmp_objref = objref;
	ASSERT(!CORBA::is_nil(objref));
	objref = nil;
	CORBA::release(tmp_objref);
}

inline replica::transaction_id_ptr
transaction_state::get_trans_id()
{
	return (trans_id);
}

inline state_hash_table_t *
transaction_state::get_state_hash()
{
	return (state_hash);
}

inline bool
transaction_state::is_orphaned()
{
	return (((flags & ORPHANED) != 0) ? true : false);
}

inline bool
transaction_state::is_completed()
{
	return (((flags & COMPLETED) != 0) ? true : false);
}

inline bool
transaction_state::is_retried()
{
	return (((flags & RETRIED) != 0) ? true : false);
}

inline bool
transaction_state::is_lockrvd()
{
	return (((flags & LOCK_RVD) != 0) ? true : false);
}

inline void
transaction_state::set_orphaned()
{
	flags |= ORPHANED;
}

inline void
transaction_state::set_retried()
{
	flags |= RETRIED;
}

inline void
transaction_state::set_commit()
{
	ASSERT(!is_committed());
	flags |= COMMITTED;
}

inline void
transaction_state::set_completed()
{
	flags |= COMPLETED;
}

inline void
transaction_state::set_lockrvd()
{
	flags |= LOCK_RVD;
}

#endif	/* _TRANSACTION_STATE_IN_H */
