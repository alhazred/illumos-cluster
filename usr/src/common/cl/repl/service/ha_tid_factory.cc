//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ha_tid_factory.cc	1.6	08/05/20 SMI"

#include <repl/service/ha_tid_factory.h>
#include <nslib/ns.h>
#include <repl/service/replica_handler.h>
#include <orb/infrastructure/orbthreads.h>
#ifdef _KERNEL_ORB
#include <repl/service/repl_tid_impl.h>
#endif // _KERNEL_ORB

ha_tid_factory *ha_tid_factory::the_factory;

ha_tid_factory &
ha_tid_factory::the()
{
	return (*ha_tid_factory::the_factory);
}

void
ha_tid_factory::initialize()
{
	ha_tid_factory::the_factory = new ha_tid_factory;
}

void
ha_tid_factory::shutdown()
{
	delete (ha_tid_factory::the_factory);
	ha_tid_factory::the_factory = NULL;
}

ha_tid_factory::ha_tid_factory()
#ifndef _KERNEL_ORB
	: kernel_factory(NULL)
#endif // _KERNEL_ORB
{
}

ha_tid_factory::~ha_tid_factory()
{
#ifndef _KERNEL_ORB
	lck.lock();
	if (!CORBA::is_nil(kernel_factory)) {
		CORBA::release(kernel_factory);
		kernel_factory = NULL;
	}
#endif // _KERNEL_ORB
}

replica::transaction_id_ptr
ha_tid_factory::get_tid()
{
	replica::transaction_id_ptr tid_ptr = NULL;
#ifdef _KERNEL_ORB
	ha_transaction_id *tid_impl = new ha_transaction_id(NULL);
	tid_ptr = tid_impl->get_objref();
#else
	Environment e;
	lck.lock();

	if (CORBA::is_nil(kernel_factory)) {
		// Get the kernel_factory for us.
		CORBA::Object_var rma_obj =
		    ns::wait_resolve_local("rma_admin", e);
		ASSERT(!e.exception() && !CORBA::is_nil(rma_obj));
		replica_int::rma_admin_var rma_var =
		    replica_int::rma_admin::_narrow(rma_obj);
		ASSERT(!CORBA::is_nil(rma_var));

		kernel_factory = rma_var->get_tid_factory(e);
		ASSERT(!e.exception() && !CORBA::is_nil(kernel_factory));
	}
	tid_ptr = kernel_factory->get_tid(e);
	ASSERT(!e.exception() && !CORBA::is_nil(tid_ptr));

	lck.unlock();
#endif // _KERNEL_ORB

	return (tid_ptr);
}
