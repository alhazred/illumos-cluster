/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CKPT_HANDLER_H
#define	_CKPT_HANDLER_H

#pragma ident	"@(#)ckpt_handler.h	1.96	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/handler/remote_handler.h>
#include <orb/xdoor/Xdoor.h>
#include <sys/os.h>
#include <sys/threadpool.h>
#include <orb/debug/haci_debug.h>
#include <h/replica.h>
#include <repl/service/transaction_state.h>
#include <repl/service/multi_ckpt_handler.h>
#include <orb/refs/unref_threadpool.h>

#if defined(HACI_DBGBUFS) && !defined(NO_CKPT_DBGBUF)
#define	CKPT_DBGBUF
#endif

#ifdef	CKPT_DBGBUF
extern dbg_print_buf ckpt_dbg;
#define	CKPT_DBG(a) HACI_DEBUG(ENABLE_CKPT_DBG, ckpt_dbg, a)
#else
#define	CKPT_DBG(a)
#endif

const replica::ckpt_seq_t ckpt_range = 0x80000000;

class ckpt_handler_kit : public remote_handler_kit {
public:
	ckpt_handler_kit();
	remote_handler	*create(Xdoor *);
	void	unmarshal_cancel_remainder(service &);

	static ckpt_handler_kit &the();
	static int initialize();
	static void shutdown();
private:
	static ckpt_handler_kit *the_ckpt_handler_kit;
	// Disallow assignments and pass by value.
	ckpt_handler_kit(const ckpt_handler_kit &);
	ckpt_handler_kit &operator = (ckpt_handler_kit &);
};

//
// Checkpoint handler. Used for checkpoint objects in
// the replica framework.
//
class ckpt_handler : public remote_handler {
	friend class print;
	friend class ckpt_list;
public:
	hkit_id_t handler_id();
	bool is_user();

	//
	// A helper function. It does a translate_in_roll_back on the
	// xdoors contained in service.
	//
	static void xdoor_roll_back(recstream *);

protected:
	// InterfaceDescriptor lookups are supported for this handler class.
	ckpt_handler(Xdoor *xdp);
	ckpt_handler();
	void	incref();

	// Number of proxies/pointers.
	uint_t	nref;

private:
	// Disallow assignments and pass by value.
	ckpt_handler(const ckpt_handler &);
	ckpt_handler &operator = (ckpt_handler &);
};

//
// ckpt_elem - the list element that contains a checkpoint
//
class ckpt_elem : public defer_task {
	friend class ckpt_list;
	friend class ckpt_handler_server;
public:
	ckpt_elem(replica::ckpt_seq_t, service &, ckpt_handler_server *);

	void execute();

	// Dispose of this checkpoint.
	void dispose();

private:
	// The sequence number.
	replica::ckpt_seq_t	seq;

	//
	// The environment that the service below uses. We can't use the
	// environment passed in by the service in the constructor because
	// that environment will be freed as soon as the rxdoor's method
	// returns.
	//
	Environment		env;

	// The recstream of the checkpoint request.
	recstream		*re;

	ckpt_handler_server	*ckhandler;
};

class ckpt_info;

// The sorted linked list that contains the checkpoints.
class ckpt_list : public IntrList<ckpt_elem, _SList> {
public:
	//
	// The checkpoint list could be non-empty when we decide to get
	// rid of the ckpt_handler_server (that is, the secondary).
	//
	~ckpt_list();

	// Insert the ckpt_elem info the list, with the min_seq as specified.
	void insert(replica::ckpt_seq_t min_seq, ckpt_elem *);

	// Dispose of all the queued up checkpoints.
	void dispose_ckpts();
};

//
// Checkpoint handler server portion. It contains a
// reference to the actual object implementation, as
// well as a marshaling count, to maintain coherence
// with the Xdoor layer call to unreference.
//
class ckpt_handler_server :
	public ckpt_handler,
	public unref_task
{
	friend class print;
	friend class ckpt_elem;
public:
	// Called by ORB::initialize to initialize the threadpool for
	// checkpoints.
	static int initialize();

	// Called by ORB::shutdown to shutdown the ckpt_threadpool.
	static void shutdown();

	// InterfaceDescriptor lookups are supported for this handler class
	//
	ckpt_handler_server(CORBA::Object_ptr impl_objp);
	~ckpt_handler_server();

	void	handle_incoming_call(service &);

	// Acquires handler lock and return handler's marshal count.
	uint_t	begin_xdoor_unref();

	// If the xdoor has been unreferenced, decides if the handler
	// can be unreferenced too.
	void	end_xdoor_unref(bool xdoor_unref);

	// Decides if a handler on the unref_threadpool can be ureferenced.
	void	deliver_unreferenced();

	// Returns true when unreference can be performed at this time.
	bool	last_unref();

	// Returns the handler to be used by the unref_task
	virtual handler		*get_unref_handler();

	// Returns the unref_task type
	virtual unref_task_type_t	unref_task_type();

	// Generate a new reference to the implementation object.
	void	new_reference();

	void			revoke();
	void			marshal(service &, CORBA::Object_ptr);

	CORBA::Object_ptr	unmarshal(service &, CORBA::TypeId,
				    generic_proxy::ProxyCreator, CORBA::TypeId);

	virtual void		*narrow(CORBA::Object_ptr, CORBA::TypeId,
				    generic_proxy::ProxyCreator);

	virtual void		*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void			release(CORBA::Object_ptr);

	bool			is_local();

	void			set_state_info(state_hash_table_t *ht);
	state_hash_table_t	*get_state_hash();

	// Get the min_seq.
	replica::ckpt_seq_t	get_min_seq();

	// Initialize the sequence numbers.
	void			initialize_seq(replica::ckpt_seq_t m);

	//
	// It returns the highest sequence number this ckpt_handler
	// received in order so far.
	//
	replica::ckpt_seq_t	get_high_seq();

	// used during a rm reconfiguration to flush old checkpoints
	void			process_to(replica::ckpt_seq_t);

	// Wait for all the checkpoints being delivered to the ckpt_handler.
	void			wait_deliver();

	//
	// Wait until all the checkpoints up to the specified point
	// are processed by the ckpt_threadpool.
	//
	void			flush_to(replica::ckpt_seq_t);

	// Wait for the checkpoints in ckpt_threadpool to be processed.
	void			quiesce_ckpt_threadpool();

	// This node is becoming spare, clean up.
	void			become_spare();

	//
	// This provider has been taken away from primary's secondary list
	// and will not receive any ckpt messages.
	//
	void			stop_receiving_ckpt();

	//
	// This provider has been added to primary's secondary list
	// and will start receiving ckpt messages from the primary.
	//
	void			start_receiving_ckpt();

	bool			is_receiving_ckpt();

protected:
	// Which object
	CORBA::Object_ptr	_obj();

private:
	CORBA::Object_ptr	obj_;

	// The method that actually processes a checkpoint and piggybacked info
	void			process(service &);

	// Called by ckpt_threadpool after just processed a checkpoint.
	void			processed_checkpoint(replica::ckpt_seq_t);

	// Record revoke state
	revoke_state_t		revoke_state_;

	// Record handler state
	handler_state_t		state_;

	// The sequence number of the dumpstate message we are expecting.
	replica::ckpt_seq_t	dump_seq;

	// The list of dumpstate messages waiting to be processed.
	ckpt_list		dump_msgs;

	// The sequence that is smaller than all unprocessed checkpoints
	replica::ckpt_seq_t	min_seq;

	// The seq up to which all ckpts should commit
	replica::ckpt_seq_t	commit_seq;

	// The sequence number that we just put into the ckpt_threadpool
	// to be processed.
	replica::ckpt_seq_t	last_seq;

	// The sequence number that we just processed.
	replica::ckpt_seq_t	last_process_seq;

	// Whether we are waiting for delivery from transport.
	bool			waiting_deliver;

	// whether we are flushing to a certain point.
	bool			flushing;

	//
	// When flushing is set to true, this tells us where are we
	// flushing to. When flushing is false it has no meaning.
	//
	replica::ckpt_seq_t	flushing_seq;

	//
	// The condvar that we use to wait for all checkpoints to be
	// processed by ckpt_handler_server::handle_incoming_call.
	//
	os::condvar_t		quiesce_cv;

	// The list that stores the unprocessed checkpoints.
	ckpt_list		checkpoints;

	// State hashtable.
	state_hash_table_t	*hashtab;

	// Disallow assignments and pass by value
	ckpt_handler_server(const ckpt_handler_server &);
	ckpt_handler_server &operator = (ckpt_handler_server &);

	//
	// Indicate whether we are still receiving ckpts from the service
	// primary.
	//
	// This is set to true in initialize_seqs() and to fasle by
	// remove_secondary() on service primary and in become_spare().
	//
	// This flag should be used together with provider's state flag
	// by RM primary to determine wether a provider is indeed a secondary
	// which requires not only the provider's state is secondary but
	// also the receiving_ckpt flag to be true.
	//
	// When a secondary is removed from the primary's multi_ckpt secondary
	// list but yet to be converted to a spare, we need to set this flag to
	// false so that in case of service primary or RM primary failure,
	// we will not treat this provider as a secondary if this flag is
	// false.
	//
	bool	receiving_ckpt;
};

//
// The client side of a checkpoint handler has to keep
// track of the deep type of an object.
//
class ckpt_handler_client :
	public ckpt_handler,
	public knewdel
{
public:
	friend class print;

	// Used to pass arguments to the invoke() method.
	class ckpt_handler_client_invoke {
	public:
		ckpt_handler_client_invoke();

		commit_list		cmt_list;
		delete_list		dlt_list;
		uint_t			number_commits;
		uint_t			number_deletes;

	private:
		// Disallow assignments and pass by value.
		ckpt_handler_client_invoke(ckpt_handler_client_invoke &);
		ckpt_handler_client_invoke &operator =
		    (ckpt_handler_client_invoke &);
	};

	// InterfaceDescriptor lookups are supported for this handler class.
	ckpt_handler_client(Xdoor *xdp);

	~ckpt_handler_client();

	ExceptionStatus	invoke(arg_info &, ArgValue *, InterfaceDescriptor *,
	    Environment &);

	// The real implementation.
	ExceptionStatus		invoke(arg_info &ai,
		ArgValue			*avp,
		InterfaceDescriptor		*infp,
		replica::ckpt_seq_t		seqnum,
		replica::ckpt_seq_t		commit_seq,
		ckpt_handler_client_invoke	*invoke_argsp,
		Environment			&e);

	//
	// Initialize the data structures in the handler. This includes
	// setting up the objref and the sequence numbers.
	//
	void 			init(replica::checkpoint_ptr sec);

	void			marshal(service &, CORBA::Object_ptr);

	CORBA::Object_ptr	unmarshal(service &, CORBA::TypeId,
				    generic_proxy::ProxyCreator, CORBA::TypeId);

	virtual void		*narrow(CORBA::Object_ptr, CORBA::TypeId,
				    generic_proxy::ProxyCreator);

	virtual void		*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void			release(CORBA::Object_ptr obj);

	void			new_reference();

	bool			is_local();

	replica::ckpt_seq_t	get_last_dump_seq();

protected:
	void			incpxref(CORBA::Object_ptr o);
	uint_t			decpxref(CORBA::Object_ptr o);
	CORBA::Object_ptr 	_obj();

private:
	//
	// called by invoke() to process oneway invocations.
	//
	void process_oneway_invo(replica::ckpt_seq_t *, replica::ckpt_seq_t *);

	// Disallow assignments and pass by value
	ckpt_handler_client(const ckpt_handler_client &);
	ckpt_handler_client &operator = (ckpt_handler_client &);

	// Pointer to proxy object for the deep type.
	CORBA::Object_ptr	proxy_obj_;

	// The sequence number for the next dumpstate message.
	replica::ckpt_seq_t 	dump_seq;

	// The seq for the last flush_to call. Used for dumpstate flow control.
	replica::ckpt_seq_t 	last_flush_seq;

	//
	// The reference to the remote secondary object. Needed when we
	// want to send a flow control (flush_to) message to the secondary.
	//
	replica::checkpoint_ptr	objref;
};

#ifndef NOINLINES
#include <repl/service/ckpt_handler_in.h>
#endif  // _NOINLINES

#endif	/* _CKPT_HANDLER_H */
