/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  repl_tid_impl.h
 *
 */

#ifndef _REPL_TID_IMPL_H
#define	_REPL_TID_IMPL_H

#pragma ident	"@(#)repl_tid_impl.h	1.25	08/05/20 SMI"

//
// repl_tid_impl.h 	Header file for handlers which pass around tid objects.
//

#include <repl/service/replica_handler.h>
#include <repl/service/ha_tid_factory.h>
#include <sys/threadpool.h>

// There is one kernel_tid_factory per usrland process. It creates
// tid objects for its usrland counter part.
//
// The locking order is tid_factory lock > tid lock.
class kernel_tid_factory : public McServerof<replica_int::tid_factory> {
public:
	replica::transaction_id_ptr get_tid(Environment &);

	// Removes a tid object from the tid_list. Called right before
	// the tid is deleted.
	void remove(ha_transaction_id *);

	void _unreferenced(unref_t);
	// Tries to delete this factory object. It's always called with
	// the lock held. If it doesn't delete this then it unlocks before
	// returning.
	void try_delete();

	kernel_tid_factory();

	void lock();
	void unlock();
private:
	// Whether unreferenced has been received.
	bool is_unref;

	// The kernel factory will keep track of all the tids it created
	// for the usrland process.
	SList<ha_transaction_id> tid_list;

	// for protecting the tid_list.
	os::mutex_t lck;
};

// Class to store a single reference to secondary TID object.

class secondary_state_elem : public _SList::ListElem {
	friend class ha_transaction_id;
public:
	secondary_state_elem(replica::trans_state_ptr);

	~secondary_state_elem();

private:
	replica::trans_state_ptr objp_;
};


// Class for client TID objects.  One of these is created for each
// mini-transaction.  As part of processing checkpoint messages, the
// secondaries invoke the callback method, passing a reference to a
// checkpointed state object on the secondary.  When the mini-transaction
// completes, the client calls the completed() method, which causes the
// references to the secondary tid objects to be released.  The secondary
// objects contain a reference to the tid which they release when they delete
// their transaction state.

class ha_transaction_id : public McServerof<replica::transaction_id> {
public:
	ha_transaction_id(kernel_tid_factory *);

	// IDL method
	void	register_state(replica::trans_state_ptr secondary_obj,
	    CORBA::Environment &);

	// Called locally by the client to indicate that the transaction
	// completed.  We release references to any secondary objects so they
	// can throw away their state.
	void	completed(CORBA::Environment &);

	// Called when this tid is created for a usrland program and
	// that usrland program exits/dies. It returns true if the
	// factory can go away, returns false if it still needs to access
	// the factory object.
	bool client_exited();

	// When the client gets unreferenced, then no replicas have any
	// more state about the transaction. The completed() call has
	// already been made (the client keeps a reference until after
	// it calls completed()).
	void	_unreferenced(unref_t);

	// Inform all the secondary transaction states about the death of the
	// client.
	void	broadcast_client_death();

private:
	// Helper function. Does the real work for completed(), called with
	// the lock held.
	void	completed_locked();

	// destructor can only be called privately.
	~ha_transaction_id();

	// The factory that created this object. It's only set for the
	// tid objects that are created on behalf of a usrland program.
	kernel_tid_factory *factoryp;

	// Used to hold the references to secondary objects.  Disposing this
	// list releases the references and corresponds to sending forget
	// messages to the secondaries.
	IntrList<secondary_state_elem, _SList> secondary_state_list;

	// Invocation has completed.
	bool		done;
	os::mutex_t	lck;
};

// For delivering client_died event to all the transaction_states.
class client_died_task : public defer_task {
public:
	client_died_task(ha_transaction_id *);
	void execute();

private:
	ha_transaction_id *tidp;
	replica::transaction_id_ptr tid_ref;
};


// External definition needed to unmarshal object references.
extern MarshalInfo_object _replica_transaction_id_ArgMarshalFuncs;

#include <repl/service/repl_tid_impl_in.h>

#endif	/* _REPL_TID_IMPL_H */
