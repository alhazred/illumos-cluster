/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  replica_handler_in.h
 *
 */

#ifndef _REPLICA_HANDLER_IN_H
#define	_REPLICA_HANDLER_IN_H

#pragma ident	"@(#)replica_handler_in.h	1.26	08/05/20 SMI"

//
// trans_ctx methods
//

inline
trans_ctx::trans_ctx(context_type typ, replica::transaction_id_ptr trid_p) :
	type(typ),
	trans_id_p(trid_p)
{
}

inline trans_ctx::context_type
trans_ctx::get_type()
{
	return (type);
}

inline replica::transaction_id_ptr
trans_ctx::get_trans_id()
{
	return (trans_id_p);
}

#ifdef _KERNEL_ORB
//
// primary_ctx methods
//

inline
primary_ctx::primary_ctx(replica_handler *handp) :
	trans_ctx(trans_ctx::PRIMARY, nil),
	needcommit(false),
	env_type(MINI_TRANSACTION),
	clnt_ctx(NULL),
	saved_state(NULL),
	handlerp(handp)
{
}

inline
primary_ctx::primary_ctx(replica_handler *handp, invoke_env typ,
    Environment &e) :
	trans_ctx(trans_ctx::PRIMARY, nil),
	needcommit(false),
	env_type(typ),
	clnt_ctx(NULL),
	saved_state(NULL),
	handlerp(handp)
{
	e.trans_ctxp = (void *)this;
}

// Whether the mini-transaction needs an add_commit.
inline bool
primary_ctx::need_commit()
{
	return (needcommit);
}

// Set needcommit to true.
inline void
primary_ctx::needcommit_yes()
{
	needcommit = true;
}

// Set needcommit to false;
inline void
primary_ctx::needcommit_no()
{
	needcommit = false;
}

inline primary_ctx *
primary_ctx::extract_from(Environment &e)
{
	return ((primary_ctx *)e.trans_ctxp);
}

inline transaction_state *
primary_ctx::get_saved_state()
{
	return (saved_state);
}

// Only called from orphan_thread::process_orphaned().
inline void
primary_ctx::begin_orphaned(replica::transaction_id_ptr trid_p,
    transaction_state *st)
{
	trans_id_p = trid_p;
	saved_state = st;
}

inline void
primary_ctx::completed_orphaned(Environment &e)
{
	trans_id_p = nil;
	saved_state = NULL;
	e.trans_ctxp = NULL;
}

//
// The primary context is no longer needed because of a retry.
// Clean up state and destroy the primary context.
//
inline void
primary_ctx::discard_context()
{
	trans_id_p = nil;
	delete this;
}

//
// secondary_ctx methods
//

inline
secondary_ctx::secondary_ctx(Environment &e) :
	trans_ctx(trans_ctx::SECONDARY, nil),
	saved_state(NULL),
	handlerp(NULL)
{
	e.trans_ctxp = (void *)this;
}

// Extract the secondary_ctx from the environment.
inline secondary_ctx *
secondary_ctx::extract_from(Environment &e)
{
	return ((secondary_ctx *)e.trans_ctxp);
}

// Get the transaction state.
inline transaction_state *
secondary_ctx::get_saved_state()
{
	return (saved_state);
}

inline void
secondary_ctx::begin(replica::transaction_id_ptr trid_p)
{
	trans_id_p = trid_p;
	saved_state = handlerp->get_state_hash()->get_state(trid_p);
}

inline void
secondary_ctx::completed(Environment &e)
{
	trans_id_p = nil;
	handlerp->get_state_hash()->get_state_done(saved_state);
	saved_state = NULL;
	e.trans_ctxp = NULL;
}
#endif // _KERNEL_ORB

//
// client_ctx methods
//

//
// replica_handler methods
//

inline repl_service *
replica_handler::get_service()
{
	return (_servicep);
}

inline void
replica_handler::set_service(repl_service *rsp)
{
	ASSERT(lock_held());
	_servicep = rsp;
}

inline void
replica_handler::lock()
{
	_lck.lock();
}

inline void
replica_handler::unlock()
{
	_lck.unlock();
}

inline bool
replica_handler::lock_held()
{
	return (_lck.lock_held());
}

inline void
replica_handler::incref()
{
	lock();
	_nref++;
	CL_PANIC(_nref != 0); 	// Check for wraparounds
	unlock();
}

inline void
replica_handler::incpxref(CORBA::Object_ptr o)
{
	lock();
	o->_this_component_ptr()->increase();
	unlock();
}

inline uint_t
replica_handler::decpxref(CORBA::Object_ptr o)
{
	return (o->_this_component_ptr()->decrease());
}

inline unref_task_type_t
replica_handler::unref_task_type()
{
	return (unref_task::HANDLER);
}

#ifdef _KERNEL_ORB

inline bool
replica_handler::freeze_in_progress(Environment *e)
{
	return (_servicep->freeze_in_progress(e));
}

//
// Get the service_id of the underlying hxdoor.
// This is only to be called when there is a hxdoor.
//
inline replica::service_num_t
replica_handler::get_service_id()
{
	ASSERT(!is_user());
	ASSERT(xdoorp);
	return (((hxdoor *)xdoorp)->get_serviceid());
}

//
// Get the object_id of the underlying hxdoor.
// This is only to be called when there is a hxdoor.
//
inline xdoor_id
replica_handler::get_object_id()
{
	ASSERT(!is_user());
	ASSERT(xdoorp);
	return (((hxdoor *)xdoorp)->get_objectid());
}

#endif // _KERNEL_ORB

#endif	/* _REPLICA_HANDLER_IN_H */
