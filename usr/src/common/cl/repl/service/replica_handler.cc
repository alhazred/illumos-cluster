//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)replica_handler.cc	1.167	08/05/20 SMI"

#include <repl/service/replica_handler.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>

#ifdef _KERNEL_ORB
#include <repl/rma/hxdoor.h>
#include <repl/service/repl_tid_impl.h>

#else
#include <repl/service/ha_tid_factory.h>
#include <orb/xdoor/solaris_xdoor.h>
#endif

#include <orb/object/schema.h>
#include <orb/flow/resource.h>
#include <sys/mc_probe.h>

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#endif

#ifdef _KERNEL
#include <sys/thread.h>
#endif

#ifdef REPL_DBGBUF
uint32_t repl_dbg_size = 100000;
dbg_print_buf repl_dbg(repl_dbg_size);
#endif

//
// This is a generic comment about ASSERTs that check handler states
// at various places: These should be CL_PANICs but the RM itself goes
// through some somewhat unecpected transitions as indicated in bug
// 4838114, changing these to CL_PANICs may lead to greater
// instability. This issue needs to be revisited after the above bug
// has been fixed.
//

//
// Handler Kit stuff
//

replica_handler_kit *replica_handler_kit::the_replica_handler_kit = NULL;

//
// replica_handler_kit constructor.
//
replica_handler_kit::replica_handler_kit() :
	remote_handler_kit(REPL_HKID)
{
}

remote_handler *
replica_handler_kit::create(Xdoor *xdp)
{
	return (new replica_handler(xdp));
}

void
replica_handler_kit::unmarshal_cancel_remainder(service &)
{
}

//
// Returns a reference to replica_handler_kit instance.
//
replica_handler_kit&
replica_handler_kit::the()
{
	ASSERT(the_replica_handler_kit != NULL);
	return (*the_replica_handler_kit);
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
replica_handler_kit::initialize()
{
	ASSERT(the_replica_handler_kit == NULL);
	the_replica_handler_kit = new replica_handler_kit();
	if (the_replica_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
replica_handler_kit::shutdown()
{
	if (the_replica_handler_kit != NULL) {
		delete the_replica_handler_kit;
		the_replica_handler_kit = NULL;
	}
}

//
// replica_handler methods
//

#ifdef _KERNEL_ORB
//
// Constructor used when creating the object on the primary.
//
replica_handler::replica_handler(repl_service *servicep,
    CORBA::Object_ptr objp) :
	_nref(0),
	_servicep(servicep),
	_impl_object(objp),
	proxy_obj_(CORBA::Object::_nil())
{
	set_cookie((void *)objp);
	_servicep->add_handler();
}
#endif // _KERNEL_ORB

//
// Constructor used when unmarshaling.
// There is no need to create a handler on the primary to support unmarshalling.
// So begin life as a client handler. The add_impl method can convert the
// handler to a secondary handler.
//
replica_handler::replica_handler(Xdoor *xdp) :
	remote_handler(xdp),
	_nref(0),
	_servicep(NULL),
	state_(CLIENT_HANDLER),
	_impl_object(nil),
	proxy_obj_(CORBA::Object::_nil())
{
}

replica_handler::~replica_handler()
{
	CL_PANIC(_nref == 0);
	CL_PANIC(_servicep != NULL);

	//
	// The replica handler can be a primary, secondary, or client.
	// This adds to the number of possible states.
	// Better be in one of these states if we are being deleted.
	//
	ASSERT(state_.get() == NEW_HANDLER ||
	    state_.get() == UNREF_DONE ||
	    state_.get() == UNREF_CKPT_DONE ||
	    state_.get() == SECONDARY_HANDLER ||
	    state_.get() == CLIENT_HANDLER);

	_servicep->rem_handler();
	_impl_object = nil;
	_servicep = NULL;
	proxy_obj_ = nil;
}

hkit_id_t
replica_handler::handler_id()
{
	return (replica_handler_kit::the().get_hid());
}

bool
replica_handler::is_local()
{
	return (true);
}

bool
replica_handler::is_user()
{
#ifdef _KERNEL_ORB
	return (false);
#else
	return (true);
#endif
}

CORBA::Object_ptr
replica_handler::_obj()
{
	return (_impl_object);
}

//
// get_unref_handler - Returns the handler to be used by the unref_task
//
handler *
replica_handler::get_unref_handler()
{
	return (this);
}

//
// release - one reference has been released.
// Discard anything no longer needed.
//
void
replica_handler::release(CORBA::Object_ptr obj)
{
	lock();
	if (decpxref(obj) == 0) {
		if (!CORBA::is_nil(proxy_obj_) && obj->_this_component_ptr() ==
		    proxy_obj_->_this_component_ptr()) {
			proxy_obj_ = CORBA::Object::_nil();
		}
		delete obj;

		REPL_DBG(("%p release nref %d state 0x%x\n",
		    this, _nref, state_.get()));

		ASSERT(_nref > 0);
		if (--_nref == 0) {
			// All local references have gone.
			if (xdoorp != NULL) {
				if (xdoorp->client_unreferenced(
				    Xdoor::KERNEL_HANDLER, marshcount)) {

					if (CORBA::is_nil(_impl_object)) {
						//
						// We are a client (or spare).
						//
						// A client cannot be a primary
						ASSERT(_servicep != NULL);
						ASSERT(!_servicep->
						    is_primary());

						//
						// Get rid of the handler.
						//
						// Client handler does
						// not participate in
						// unreference
						// protocol.
						//
						// No need to unlock since
						// we are deleting the lock.
						//
						delete this;
						return;
					}
				}
				// Either the xdoor has passed up a
				// reference to this handler as part of
				// an unmarshal or we are a primary
				// or secondary for this object. We'd
				// better stick around.  The xdoor has
				// accounted for the marshal count we sent
				// down.
				//
				marshcount = 0;
			} else {
				// There are no xdoors

#ifdef _KERNEL_ORB
				if (_servicep->is_primary()) {
					//
					// This is a primary that
					// released its last reference.
					//
					switch (state_.get()) {
					case ACTIVE_HANDLER:
						state_.set(UNREF_SCHEDULED);

						//
						// Schedule unreferenced
						// notification to object impl
						//
						_servicep->get_repl_provp()->
						    add_unref(this);
						break;

					case UNREF_UNSCHEDULE:
						state_.set(UNREF_SCHEDULED);
						break;

					case UNREF_NO_PROCESS:
						state_.set(UNREF_PROCESS);
						break;

					default:
						ASSERT(0);
					}
				} else {
					// This handler was created intending to
					// become a primary, and
					// subsequently failed to become
					// primary (i.e. it raised a
					// primary_failed exception).
					//
					ASSERT(state_.get() == ACTIVE_HANDLER);

					if (CORBA::is_nil(_impl_object)) {
						delete this;
						return;
					}
				}
#else
				ASSERT(0);
#endif

			}
		}
	}
	unlock();
}

#ifdef _KERNEL_ORB

//
// begin_xdoor_unref
//
//   obtains handler lock and returns handler's marshal count.
//   Called by the xdoor's deliver_unreferenced() method.
//
uint_t
replica_handler::begin_xdoor_unref()
{
	lock();
	REPL_DBG(("%p beg_xd_unref state 0x%x\n", this, state_.get()));
	ASSERT(_servicep->is_primary());
	return (marshcount);
}

//
// end_xdoor_unref
//
//   called by xdoor to inform the handler if the xdoor has decided to
//   delete itself. If the handler does not have any more references and
//   service is not frozen, the service will schedule the handler to be
//   unreferenced.
//
void
replica_handler::end_xdoor_unref(bool xdoor_unref)
{
	REPL_DBG(("%p end_xdoor_unref. xdoor_unref = %d\n", this, xdoor_unref));
	ASSERT(lock_held());
	if (xdoor_unref) {
		if (_nref == 0) {
			switch (state_.get()) {
				case ACTIVE_HANDLER:
					state_.set(UNREF_SCHEDULED);
					REPL_DBG(("%p scheduling repl_hdlr"
					    " for unref\n", this));
					get_service()->get_repl_provp()->
					    add_unref(this);
					break;

				case UNREF_UNSCHEDULE:
					state_.set(UNREF_SCHEDULED);
					break;

				case UNREF_NO_PROCESS:
					state_.set(UNREF_PROCESS);
					break;

				default:
					ASSERT(0);
			}
		}
	} else {
		// Unreference protocol is executed only on the primary.
		ASSERT((state_.get() == ACTIVE_HANDLER) ||
		    (state_.get() == UNREF_NO_PROCESS) ||
		    (state_.get() == UNREF_UNSCHEDULE));
	}
	unlock();
}

//
// unref_by_ckpt - called from hxdoor::unref_by_ckpt
// when processing a checkpoint that unreferences the replica_handler.
// This handler must now be acting as a secondary.
//
void
replica_handler::unref_by_ckpt()
{
	lock();
	REPL_DBG(("%p unref_by_ckpt mc=%d nref=%d state 0x%x\n",
	    this, marshcount, _nref, state_.get()));

	// This set of asserts prove that the handler is now a secondary
	ASSERT(_servicep != NULL);
	ASSERT(!_servicep->is_primary());
	ASSERT(is_not_nil(_impl_object));

	// The handler must be inactive
	ASSERT(marshcount == 0);
	ASSERT(_nref == 0);

	ASSERT(state_.get() == SECONDARY_HANDLER);
	state_.set(UNREF_CKPT_PROCESS);

	unlock();

	//
	// Bypass deliver_unreferenced here.
	//
	_impl_object->_unreferenced((unref_t)this);

	// Do not access any handler fields because the handler is now gone.
}

//
// clear_unref_flag - an unreference was on the defer list.
// The system destroyed the unreference notification
// as part of the process of becoming a secondary.
// However, the handler is not yet a secondary.
// Tell any existing hxdoor that an unref task is no longer outstanding.
//
void
replica_handler::clear_unref_flag()
{
	lock();
	REPL_DBG(("%p clr_unref\n", this));

	ASSERT(state_.get() == UNREF_SCHEDULED);
	state_.set(ACTIVE_HANDLER);

	//
	// An existing hxdoor will not generate any more unreference actions
	// until any outstanding unref finishes. Tell hxdoor to allow unrefs
	// again. The hxdoor flag to ignore unrefs is currently true,
	// so no need to worry about spin loops.
	//
	// The xdoor may not exist if there are no secondaries and the
	// object has never been marshaled outside this domain.
	//
	// The system can do a become_secondary operation on
	// a service that has no secondaries when this service depends on
	// a service that is being switched over. That switchover will
	// ultimately fail, because that switchover would result in
	// this service being shut down.
	//
	if (xdoorp != NULL) {
		xdoorp->clear_unreferenced();
	}
	unlock();
}

//
// deliver_unreferenced - process an unref_task for a handler.
//
//   The unreferenced method of the implementation object is called if the
//   handler can be deleted. The destructor of implementation object calls
//   rem_impl() method of the handler to delete the handler.
//
void
replica_handler::deliver_unreferenced()
{
	lock();
	REPL_DBG(("%p del_unref xdoorp %p state 0x%x implobj %p\n",
	    this, xdoorp, state_.get(), _impl_object));

	ASSERT(is_not_nil(_impl_object));

	//
	// Record this information on the stack, because
	// after unreference has been delivered to the implementation object
	// the handler may no longer exist.
	//
	generic_repl_prov	*gen_provp = get_service()->get_repl_provp();

	if (state_.get() == UNREF_UNSCHEDULE) {
		//
		// A 0->1 ref count transition occurred.
		// No longer need to unreference the implementation object.
		//
		state_.set(ACTIVE_HANDLER);

		// Tell the service that this unref task finished processing
		gen_provp->unref_done();

		unlock();
		return;
	}

	//
	// Now we are about to actually process the unreference.
	//
	ASSERT(state_.get() == UNREF_SCHEDULED);
	state_.set(UNREF_PROCESS);

	//
	// Cannot hold the handler lock while delivering unreference
	// to the implementation object.
	//
	unlock();

	//
	// The cookie is used by objects supporting multiple interfaces.
	// The cookie allows the implementation object to determine
	// which interface received the unreference notification.
	// The overwhelming majority of implementation objects have only
	// one interface, and thus ignore this argument.
	//
	_impl_object->_unreferenced((unref_t)this);

	// The handler may no longer exist after the above operation completes.

	// Tell the service that this unref task finished processing
	gen_provp->unref_done();
}


//
// last_unref - Returns true when no new object references have
// been created since the time when unreference notification was initiated.
// Can only be called from the implementation object
// when dealing with an unreference notification.
// This method is not idempotent.
//
bool
replica_handler::last_unref()
{
	bool	unref_ok;

	lock();
	REPL_DBG(("%p last_unref state 0x%x\n", this, state_.get()));

	//
	// Adjust state to show that unreference was delivered to the
	// implementation object.
	//
	switch (state_.get()) {
	case UNREF_PROCESS:
		unref_ok = true;
		state_.set(UNREF_DONE);
		break;

	case UNREF_NO_PROCESS:
		unref_ok = false;
		state_.set(ACTIVE_HANDLER);
		break;

	case UNREF_CKPT_PROCESS:
		unref_ok = true;
		state_.set(UNREF_CKPT_DONE);
		break;

	default:
		ASSERT(0);
		unref_ok = false;
	}
	unlock();
	return (unref_ok);
}

void
replica_handler::add_impl(CORBA::Object_ptr obj)
{
	ASSERT(_impl_object == NULL);
	_impl_object = obj;
	set_cookie((void *)obj);

	ASSERT(state_.get() == CLIENT_HANDLER);
	state_.set(SECONDARY_HANDLER);

	((hxdoor *)xdoorp)->add_secondary_obj();
	REPL_DBG(("%p add_impl %p\n", this, obj));
}

//
// rem_impl - the implementation object destructor calls this
// method to clean up its connection with the handler.
//
void
replica_handler::rem_impl()
{
	lock();

	REPL_DBG(("%p rem_impl xdoorp %p state 0x%x\n",
	    this, xdoorp, state_.get()));

	set_cookie(NULL);

	//
	// Any active unref_task must have been processed before reaching here.
	// This method is called after unreference has been delivered to
	// the implementation object.
	//
	// The handler could also be a client.
	//
	// We know that rem_impl is only called when the service is not frozen.
	// Therefore we can safely check is_primary() here.
	//
	if (_servicep->is_primary()) {
		//
		// The implementation object sometimes ignores a request to
		// unreference the object. Later the implementation object is
		// just deleted.
		//
		// In the normal case, the system delivers unreference to the
		// implementation object, which self-destructs and destroys the
		// replica_handler.
		//
		ASSERT(state_.get() == UNREF_DONE ||
		    state_.get() == NEW_HANDLER);

		//
		// The implementation object is going away and this is
		// the primary handler. A primary handler requires an
		// implementation object. So we had better not have
		// any references at this point.
		//
		ASSERT(_nref == 0);
		//
		// If xdoorp is NULL, this is a primary that was never
		// marshaled. We can just delete the handler.
		//
		// If xdoorp is non NULL, then we need to
		// disconnect the hxdoor from the handler.
		//
		if (xdoorp != NULL) {
			REPL_DBG(("%p add_delete %d.%d\n", this,
			    get_service_id(), get_object_id()));

			//
			// Need to checkpoint deletion to secondary.
			//
			get_service()->get_repl_provp()->
				add_delete(get_service_id(), get_object_id());

			// Get rid of the underlying hxdoor.
			os::mutex_t &xdoor_lock =
			    ((hxdoor *)xdoorp)->get_lock();
			xdoor_lock.lock();
			xdoorp->accept_unreferenced();
			xdoor_lock.unlock();
		}
		delete this;
		return;
	} else {
		// Since this method was called by the implementation object
		// and this section is only reached when the handler is not
		// the primary, we know that this is a secondary.
		//
		// The system does an unref_by_ckpt upon secondaries.
		// This does not involve a handler_unref_task.
		// There is no need to accept an unreference from the hxdoor.
		// The handler cannot remain a secondary.
		// The handler must self-destruct.
		//
		// The system destroys the implementation object when
		// a secondary is converted to a spare. The system does
		// neither an unref_by_ckpt nor a normal unreference.
		// The replica_handler must either become a client
		// or self-destruct.
		//
		if (xdoorp != NULL) {
			//
			// Tell the hxdoor that we no longer have an
			// implementation object and ask it if we can
			// delete ourselves.
			//
			if (((hxdoor *)xdoorp)->remove_secondary_obj()) {
				//
				// The handler is not becoming a client.
				//
				ASSERT(_nref == 0);
				ASSERT(state_.get() == SECONDARY_HANDLER ||
				    state_.get() == UNREF_CKPT_DONE);
				delete this;
				return;
			}
			//
			// Otherwise handler becomes a client.
			//
		} else if (_nref == 0) {
			//
			// This supports cleanup after an error situation.
			// The replica handler was created with the intent
			// to become a primary. References may or may not
			// have been created. In either case, those references
			// are gone now. The system has not given out a
			// reference to another node, so there is no hxdoor.
			// Before reaching this point, the repl_service knows
			// that this handler is not the primary.
			// Because of the error situation, the system
			// never delivered a "become secondary event" to
			// the handler. The system is now telling the handler
			// to become a spare. Since there are no references,
			// the handler self-destructs.
			// This error occurs on the repl_service, which does
			// not have a list of all of its handlers. So we
			// are unable to propagate the error to the affected
			// handlers.
			//
			ASSERT(state_.get() == NEW_HANDLER ||
			    state_.get() == ACTIVE_HANDLER);
			state_.set(SECONDARY_HANDLER);
			delete this;
			return;
		}
		//
		// Otherwise proxies exist. Become a client.
		//
	}
	_impl_object = NULL;
	REPL_DBG(("%p rem_impl became client state 0x%x\n",
	    this, state_.get()));
	ASSERT(state_.get() == SECONDARY_HANDLER);
	state_.set(CLIENT_HANDLER);
	unlock();
}


// server_dead
//
// server_dead returns service state. service state is found by querying
// the  hxdoor.
// Returns true if the service is DOWN.
//
bool
replica_handler::server_dead()
{
	bool status;
	lock();
	if (xdoorp != NULL) {
		status = ((hxdoor *)xdoorp)->server_dead();
	} else {
		status = true;
	}
	unlock();
	return (status);
}

//
// identify_nodes_holding_refs - Identify nodes holding reference.
// This is only a hint, as change can occur at any time.
//
void
replica_handler::identify_nodes_holding_refs(nodeset &refset)
{
	//
	// Determine which other nodes hold a reference
	// This method initializes the nodeset. So we must
	// do foreign references before the local node.
	//
	((hxdoor *)get_xdoorp())->identify_client_nodes(refset);

	// Add info about whether local node is holding a reference
	if (_nref != 0) {
		refset.add_node(orb_conf::local_nodeid());
	}
}

#endif // _KERNEL_ORB

//
// Create a new proxy object for the handler.
//
// If the handler had an unreference pending, that unreference request
// is discarded.
//
// Only called if we are the primary.
//
// This can be called from two places:
// 1. mc_replica_of::get_objref() and
// 2. mc_replica_of::get_objref(CORBA::type_info_t *tp).
//
void *
replica_handler::create_proxy(InterfaceDescriptor *actual_type,
    InterfaceDescriptor *deep_type)
{
	// Only allow widen to an older type.
	ASSERT(actual_type->is_type_supported(deep_type->get_tid()));

	lock();
	REPL_DBG(("%p new_reference xdoorp %p state_ 0x%x\n",
	    this, xdoorp, state_.get()));

	if (!CORBA::is_nil(proxy_obj_)) {
		//
		// With Rolling Upgrade, it's possible that the proxy we want
		// to create is for a different deep type. If so,
		// we are likely to be asked for more references with the
		// same deep type so change the cached proxy_obj_ pointer
		// to a proxy of the new deep type.
		//
		if (proxy_obj_->_interface_descriptor() == actual_type &&
		    proxy_obj_->_deep_type() == deep_type) {
			//
			// Return a duplicate of the proxy.
			//
			proxy_obj_->_this_component_ptr()->increase();
			unlock();
			return (proxy_obj_);
		}
		REPL_DBG(("replica_handler(%p)::create_proxy forget %s create "
		    "%s\n", this, proxy_obj_->_deep_type()->get_name(),
		    deep_type->get_name()));

		// Forget the old proxy and make a new one.
		proxy_obj_ = CORBA::Object::_nil();
	}
	unlock();

	//
	// Try to use the interface descriptor to create a proxy of the
	// deep type rather than creating a proxy of an ancestor class.
	// Keep a pointer to this proxy for unmarshal() and narrow()'s
	// later on. Note that the proxy_obj_ pointer doesn't have a
	// reference count associated with it; it is released when the
	// handler is released.
	//
	CORBA::Object_ptr obj = actual_type->create_proxy_obj(this, deep_type);
	ASSERT(!CORBA::is_nil(obj));

	lock();

	if (_nref == 0) {
		//
		// There are currently no local references.
		//
#ifdef _KERNEL_ORB
		if (xdoorp != NULL) {
			// Tell the hxdoor that we have references
			((hxdoor *)xdoorp)->have_client_ref();
		}
#endif
		// Update handler state.
		switch (state_.get()) {
		case ACTIVE_HANDLER:
		case UNREF_UNSCHEDULE:
		case UNREF_NO_PROCESS:
			// No state change.
			break;

		case NEW_HANDLER:
			state_.set(ACTIVE_HANDLER);
			break;
		case UNREF_DONE:
			state_.set(ACTIVE_HANDLER);
			if (xdoorp != NULL) {
				//
				// The hxdoor now knows about client refs,
				// so this will not enter unref spin loop.
				//
				// Since xdoorp cannot be NULL, we can use
				// it to get hold of its lock.
				//
#ifdef _KERNEL_ORB
				((hxdoor *)xdoorp)->lock();
#else
				((solaris_xdoor *)xdoorp)->lock();
#endif
				xdoorp->reject_unreferenced(marshcount);
#ifdef _KERNEL_ORB
				((hxdoor *)xdoorp)->unlock();
#else
				((solaris_xdoor *)xdoorp)->unlock();
#endif
			}
			break;

		case UNREF_SCHEDULED:
			state_.set(UNREF_UNSCHEDULE);

			if (xdoorp != NULL) {
				//
				// The hxdoor now knows about client refs,
				// so this will not enter unref spin loop.
				//
#ifdef _KERNEL_ORB
				((hxdoor *)xdoorp)->lock();
#else
				((solaris_xdoor *)xdoorp)->lock();
#endif
				xdoorp->reject_unreferenced(marshcount);
#ifdef _KERNEL_ORB
				((hxdoor *)xdoorp)->unlock();
#else
				((solaris_xdoor *)xdoorp)->unlock();
#endif
			}
			break;

		case UNREF_PROCESS:
			state_.set(UNREF_NO_PROCESS);

			if (xdoorp != NULL) {
				//
				// The hxdoor now knows about client refs,
				// so this will not enter unref spin loop.
				//
#ifdef _KERNEL_ORB
				((hxdoor *)xdoorp)->lock();
#else
				((solaris_xdoor *)xdoorp)->lock();
#endif
				xdoorp->reject_unreferenced(marshcount);
#ifdef _KERNEL_ORB
				((hxdoor *)xdoorp)->unlock();
#else
				((solaris_xdoor *)xdoorp)->unlock();
#endif
			}
			break;

		default:
			ASSERT(0);
		}
	} else {
		//
		// Local references already existed.
		//
		ASSERT(state_.get() == ACTIVE_HANDLER ||
		    state_.get() == UNREF_UNSCHEDULE ||
		    state_.get() == UNREF_NO_PROCESS);
	}

	//
	// If another thread raced ahead of us and set proxy_obj_,
	// be sure proxy_obj_ meets our needs or don't use it.
	//
	if (!CORBA::is_nil(proxy_obj_) &&
	    proxy_obj_->_interface_descriptor() == actual_type &&
	    proxy_obj_->_deep_type() == deep_type) {
		//
		// Return a duplicate of the proxy.
		//
		ASSERT(_nref != 0);
		proxy_obj_->_this_component_ptr()->increase();
		unlock();
		delete obj;
		return (proxy_obj_);
	}

	// Count the new reference to the handler in the proxy we created.
	_nref++;
	CL_PANIC(_nref != 0);	// Check for wraparounds
	proxy_obj_ = obj;
	unlock();
	return (obj);
}

//
// Generate a new reference to the implementation object.
//
void
replica_handler::new_reference()
{
	ASSERT(_nref > 0);
	incref();
}

void
replica_handler::revoke()
{
	//
	// SCMSGS
	// @explanation
	// An attempt was made to use a feature that has not been implemented.
	// @user_action
	// Contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "HA: revoke not implemented for replica_handler");
}

void
replica_handler::marshal(service &_in, CORBA::Object_ptr objp)
{
	lock();

	// Increment the marshal count always.
	// marshal_roll_back() will recover in case there is an exception later.
	//
	// Incrementing marshcount prevents an existing xdoor, or an xdoor
	// (that will get created due to a later marshal) from
	// getting disconnected until this marshal gets rolled back or goes
	// through translate_in.
	// If there is an exception in this marshal, and no later marshals
	// occur. marshal_roll_back will bring marshcount back to 0.
	//
	marshcount++;

	// XXX We need to check for revoke here when we implement it.

	if (xdoorp == NULL) {
		// We must be the primary.
		ASSERT(is_not_nil(_impl_object));

		// Allocate a new hxdoor.
#ifdef _KERNEL_ORB
		xdoorp = hxdoor::create(this);
#endif
		// We do blocking allocations and should not be getting errors.
		ASSERT(xdoorp != NULL);
	}
	unlock();

	remote_handler::marshal(_in, objp);
	_in.put_unsigned_short(_servicep->get_sid());
}

extern InterfaceDescriptor _Ix__type(CORBA_Object);

//
// Unmarshal the handler.  This is called with the xdoor lock held.
// We must synchronize with the replica_handler::release function,
// which may have set _nref to zero and told the xdoor that we
// no longer have client references in this domain.
//
// tid is the typeid that was extracted from the incoming marshalstream.
// to_tid is the typeid that we should narrow the proxy to.
//
CORBA::Object_ptr
replica_handler::unmarshal(service &serv, CORBA::TypeId tid,
    generic_proxy::ProxyCreator create_func, CORBA::TypeId to_tid)
{
	replica::service_num_t service_num;

	lock();

	// Somebody has a reference for us to be able to receive one.
	ASSERT(_nref != 0 ||
	    state_.get() == ACTIVE_HANDLER ||
	    state_.get() == UNREF_UNSCHEDULE ||
	    state_.get() == UNREF_NO_PROCESS ||
	    state_.get() == SECONDARY_HANDLER ||
	    state_.get() == CLIENT_HANDLER);

	//
	// Unmarshal service id and associate it with the handler if necessary.
	// XXX Note that we could get the service id from the hxdoor.
	//
	service_num = serv.get_unsigned_short();
	if (_servicep == NULL) {
		set_service(repl_service_manager::the().
		    lookup_hold(service_num, true));
		_servicep->add_handler_locked();
		_servicep->unlock();
	}
	ASSERT(_servicep != NULL);

	//
	// It should be safe to unlock here, since the only data that has been
	// updated is the service field, and that is never set except in
	// unmarshal, and if it is set twice, it will be to the same value.
	//
	unlock();

	//
	// Find the interface descriptor for this type.
	//
	InterfaceDescriptor *infp =
	    OxSchema::schema()->descriptor(tid, this);
	ASSERT(infp != NULL);
	lock();

	CORBA::Object_ptr obj;

	if (!CORBA::is_nil(proxy_obj_)) {
		//
		// Check to see that the proxy supports the correct deep type.
		// Note: the deep type can be CORBA::Object if another ORB
		// calls idlversion_impl::get_dynamic_descriptor() in this
		// ORB.
		// For HA references, the deep type can be changed so
		// we might be unmarshalling an older reference.
		//
		if (infp == proxy_obj_->_deep_type()) {
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			return (obj);
		}
		// Forget the old proxy if we can cache the new one.
		if (infp != &_Ix__type(CORBA_Object)) {
			REPL_DBG(("replica_handler(%p)::unmarshal forget %s "
			    "create %s\n", this,
			    proxy_obj_->_deep_type()->get_name(),
			    infp->get_name()));
			proxy_obj_ = CORBA::Object::_nil();
		}
	}

	//
	// Try to use the interface descriptor to create a proxy of the
	// deep type rather than creating a proxy of an ancestor class.
	// Keep a pointer to this proxy for unmarshal() and narrow()'s
	// later on. Note that the proxy_obj_ pointer doesn't have a
	// reference count associated with it; it is released when the
	// handler is released.
	//
	// If the server object is newer than the client code, we won't
	// be able to create a proxy for the deep type so we create
	// a proxy for the deepest (ancestor) type we know about locally.
	//
	// Also, don't cache the proxy if this is an unmarshal
	// for idlversion_impl::get_dynamic_descriptor().
	//
	unlock();
	obj = infp->create_proxy_obj(this, infp);
	if (CORBA::is_nil(obj)) {
		// Server is newer so leave proxy_obj_ nil.
		obj = (CORBA::Object_ptr)(*create_func)(this, infp);
		// Account for the new reference.
		incref();
	} else if (infp == &_Ix__type(CORBA_Object)) {
		// Account for the new reference.
		incref();
	} else {
		lock();
		if (CORBA::is_nil(proxy_obj_)) {
			proxy_obj_ = obj;
			// Account for the new reference.
			_nref++;
			CL_PANIC(_nref != 0);	// Check for wraparound
			unlock();
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(obj, to_tid);
		} else if (infp == proxy_obj_->_deep_type()) {
			//
			// We lost the race against another unmarshal()
			// or a create_proxy().  Use proxy_obj_ only if
			// it meets our needs.
			//
			ASSERT(_nref != 0);
			CORBA::Object_ptr tmp_obj = obj;
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			delete tmp_obj;
		} else {
			// Account for the new reference.
			_nref++;
			CL_PANIC(_nref != 0);	// Check for wraparound
			unlock();
		}
	}
	return (obj);
}

//
// invoke -	determines the xdoor.
//		Marshals the method call information.
//		Passes the remote invocation onto the xdoor, and
//		checks for error results.
//
ExceptionStatus
replica_handler::invoke(arg_info &ai, ArgValue *avp, InterfaceDescriptor *infp,
    Environment &e)
{
	MC_PROBE_0(replica_invoke_start, "clustering HA handler", "");

	e.check_used("unchecked environment used for new invocation in rh");
	ASSERT(!e.exception());

#ifdef _KERNEL_ORB
	//
	// This check will catch the reuse of the Environment
	// in a nested invocation when the prior invocation came
	// from a different node. Invoke occurs on the client side.
	// So the source node should be the local node.
	//
	ASSERT(e.get_src_node().ndid == orb_conf::local_nodeid());
#endif	// _KERNEL_ORB

	// if the service is already shut down, return INV_OBJREF exception
	if (_servicep->is_shutting_down()) {
		REPL_DBG(("replica handler (%p): return INV_OBJREF to local"
		    " reference as the service has already been"
		    " shutdown.\n", this));
		e.system_exception(CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		invocation::exception_return(ai, avp);
		return (CORBA::SYSTEM_EXCEPTION);
	}

	ASSERT(xdoorp != NULL);

	os::envchk::check_nonblocking(os::envchk::NB_INTR |
	    os::envchk::NB_INVO | os::envchk::NB_RECONF,
	    "replica invocation");

	ExceptionStatus	result;
	trans_ctx	*ctxp = (trans_ctx *)e.trans_ctxp;
	client_ctx	*c_ctxp = NULL;

	//
	// Begin the mini-transaction.
	// If it's a new invocation set the client_ctx up.
	//
	if (ctxp == NULL) {
		c_ctxp = client_ctx::begin(this, e);
	} else if (ctxp->get_type() == trans_ctx::PRIMARY) {
#ifdef _KERNEL_ORB
		//
		// This a retry from a local invocation.
		// Since we are going remote, we need a client context.
		//
		primary_ctx	*p_ctxp = (primary_ctx *)ctxp;

		//
		// We can only be here if we are retrying because
		// of a freeze, and the freeze algorithm guarantees that all
		// retries have been processed. We know that this context
		// cannot have any saved state.
		//
		ASSERT(p_ctxp->saved_state == NULL);

		if (p_ctxp->clnt_ctx != NULL) {
			//
			// Already have a client context.
			// This can happen when invoke remote,
			// retry local, and now retry remote.
			// Reuse client context.
			//
			c_ctxp = p_ctxp->clnt_ctx;
		} else {
			// Get a new client context
			c_ctxp = client_ctx::begin(this, e);
		}
		// Discard the primary context.
		p_ctxp->discard_context();
#endif // _KERNEL_ORB
	} else {
		ASSERT(ctxp->get_type() == trans_ctx::CLIENT);
		c_ctxp = (client_ctx *)e.trans_ctxp;
	}
	ASSERT(c_ctxp != NULL);
	ASSERT(c_ctxp->handlerp == this);
	ASSERT(c_ctxp->get_type() == trans_ctx::CLIENT);
	ASSERT(is_not_nil(c_ctxp->get_trans_id()));
	MC_PROBE_0(replica_invoke_ctx, "clustering HA handler", "");

	// Check that the server version of the object supports this method.
	int inf_index = infp->lookup_type_index(ai.id, false);
	if (inf_index < 0) {
		REPL_DBG(("replica_handler(%p)::invoke %s %x %x %x %x\n", this,
		    infp->get_name(), ai.id[0], ai.id[1], ai.id[2], ai.id[3]));
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		invocation::exception_return(ai, avp);
		e.mark_used();
		c_ctxp->completed(e);
		return (CORBA::LOCAL_EXCEPTION);
	}

	//
	// Determine message handling semantics
	// We don't support mini-transactions for oneways.
	//
	invocation_mode		invo_mode(ai);
	ASSERT(!invo_mode.is_oneway());

	// Specify resources required for message
	resources	resource(resources::BASIC_FLOW_CONTROL, &e, invo_mode,
			    INVO_MSG);

	//
	// Replica handler header is marshaled as data.
	// data size right now is: TID + method_number + transaction id
	// which is 4 + 1/2 + (1/4 + 4 + 1) = 10
	// XXX Change this to use marshal_sizeof.
	//
	resource.add_send_data(10 * sizeof (uint32_t));

	// Account for the space required to marshal arguments
	resource.add_argsizes(ai, avp);

#ifdef _KERNEL
	//
	// If the thread blocks or is preempted, we want the thread to
	// effectively have a high priority (60) so that the thread
	// can complete processing in a reasonable time period.
	//
	THREAD_KPRI_REQUEST();
#endif

	//
	// Walk the message protocol layers to reserve resources
	// and obtain a send stream for the invocation.
	//
	invocation	invo(xdoorp->reserve_resources(&resource), &e);

	if (e.exception()) {
		if (CORBA::PRIMARY_FROZEN::_exnarrow(e.exception())) {
			xdoorp->release_resources(&resource);
			e.clear();
			result = CORBA::RETRY_EXCEPTION;
		} else {
			invocation::exception_return(ai, avp);
			e.mark_used();
			c_ctxp->completed(e);
			result = CORBA::LOCAL_EXCEPTION;
		}
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		return (result);
	}

#ifdef FAULT_HA_FRMWK

	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(FAULTNUM_HA_FRMWK_REMOTE_LOCAL,
	    &fault_argp, &fault_argsize)) {

		InvoTriggers::clear(FAULTNUM_HA_FRMWK_REMOTE_LOCAL);

		xdoorp->release_resources(&resource);

		uint32_t	totalsize;
		FaultFunctions::switchover_arg_t	*switchover_argp =
		    FaultFunctions::switchover_arg_alloc(totalsize,
		    (nodeid_t)2,
		    "ha_sample_server",
		    "1");

		os::warning("REMOTE_LOCAL n %d svc %s\n",
		    switchover_argp->nodeid, switchover_argp->svc_info);

		// The system clears the fault number
		FaultFunctions::switchover(FAULTNUM_HA_FRMWK_REMOTE_LOCAL,
		    switchover_argp, totalsize);

		os::warning("REMOTE_LOCAL done\n");

#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		return (CORBA::RETRY_EXCEPTION);
	}

#endif // FAULT_HA_FRMWK

#ifdef _KERNEL_ORB
	nodeid_t dest_node = invo.se->get_dest_node().ndid;

	//
	// If the service just switched over and the local node is the
	// new primary, we should just return a retry exception.
	//
	if (dest_node == orb_conf::node_number()) {
		ASSERT(invo.se->get_dest_node().incn ==
		    orb_conf::local_incarnation());
		//
		// Let the xdoor know that we are giving up the resource
		// we just reserved.
		//
		xdoorp->release_resources(&resource);
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		return (CORBA::RETRY_EXCEPTION);
	}
#endif

	// Note that we must not return until after we do the request

	//
	// Load the interface type ID and method number.
	// This is similar to remote_handler::put_index_method() except
	// we use the type ID instead of the interface index number.
	//
	// The format is typeid, type index, and then variable size method.
	// Note that the version interface is handled as a special case
	// since it is called on objects we don't know the type of
	// (see remote_handler::get_inf_descriptor() and
	// remote_handler_kit::unmarshal()).
	//
	invo.put_bytes(ai.id, (uint_t)sizeof (CORBA::MD5_Tid));

	// Load the method number.
	if (infp->sizeof_methods((uint_t)inf_index) == 1) {
		ASSERT(ai.method <= UCHAR_MAX);
		invo.put_octet((uint8_t)ai.method);
	} else {
		ASSERT(ai.method <= USHRT_MAX);
		invo.put_unsigned_short((uint16_t)ai.method);
	}

	invo.put_object(c_ctxp->get_trans_id());

	// Marshall info related to method to be invoked on other side.
	invo.marshal_call(ai, avp);

	//
	// When there is an exception from marshal_call the objects in
	// ai are rolled back. We need to rollback the objects not in ai.
	//
	if (e.exception()) {
		ASSERT(e.sys_exception());

		// Unput the transaction id.
		service::unput_object(c_ctxp->get_trans_id());

		xdoorp->release_resources(&resource);
		c_ctxp->completed(e);
		invocation::exception_return(ai, avp);
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		return (CORBA::SYSTEM_EXCEPTION);
	}

	result = xdoorp->request_twoway(invo);

#ifdef _KERNEL
	THREAD_KPRI_RELEASE();
#endif
	//
	// If there is a local exception, there wont be a recstream
	// Otherwise the result status is in the marshalstream
	//
	if (result != CORBA::LOCAL_EXCEPTION)
		result = (ExceptionStatus) invo.get_unsigned_long();

	switch (result) {
	case CORBA::NO_EXCEPTION:
		invo.unmarshal_normal_return(ai, avp, &e);
		break;
	case CORBA::USER_EXCEPTION:
		invo.unmarshal_exception_return(ai, avp, &e);
		break;
	case CORBA::SYSTEM_EXCEPTION:
		invo.unmarshal_sys_exception_return(ai, avp, &e);
		// fall through to LOCAL_EXCEPTION case
	case CORBA::LOCAL_EXCEPTION:
		if (CORBA::RETRY_NEEDED::_exnarrow(e.exception()) ||
		    CORBA::PRIMARY_FROZEN::_exnarrow(e.exception())) {
			e.clear();
			return (CORBA::RETRY_EXCEPTION);
		}
		invocation::exception_return(ai, avp);
		break;
	case CORBA::UNINITIALIZED:
	case CORBA::RETRY_EXCEPTION:
	case CORBA::REMOTE_EXCEPTION:
	default:
		//
		// SCMSGS
		// @explanation
		// An invocation completed with an invalid result status.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: unknown invocation result status %d", result);
		break;
	}
	e.mark_used();
	c_ctxp->completed(e);

	MC_PROBE_0(replica_invoke_end, "clustering HA handler", "");
	return (result);
}

#ifdef _KERNEL_ORB
//
// local_invoke
// We support local invocations when we are primary. The service knows that
// and deals with freezing of the service etc.
// The combination of local_invoke() and local_invoke_done() need to do
// similar management of the transaction id object as invoke() and
// handle_incoming_call().
//
// Before Rolling Upgrade was implemented in SC 3.1, a local_invoke could
// only be called if there was already a reference unmarshalled. With RU,
// it is possible to call local_invoke during the unmarshal process, so it
// is possible that there are no current references to this handler.
//
void *
replica_handler::local_invoke(CORBA::TypeId to_tid, InterfaceDescriptor *infp,
    Environment &e)
{
	REPL_DBG(("local_invoke(%p): %u, %u\n", this,
	    infp->get_tid()));
	if (_servicep == NULL || _impl_object == NULL) {
		// This invoke is on an invalid local object.
		REPL_DBG(("local_invoke called on invalid object\n"));
		return (NULL);
	}

	if (_servicep->try_local_invo(&e)) {

		// Verify the same constraints that we do for remote invokes.
		e.check_used("unchecked environment used for new invocation in "
		    "rh li");
		ASSERT(!e.exception());

		os::envchk::check_nonblocking(os::envchk::NB_INTR |
		    os::envchk::NB_INVO |
		    os::envchk::NB_RECONF, "replica local invocation");

#ifdef _KERNEL
		//
		// If the thread blocks or is preempted, we want the thread to
		// effectively have a high priority (60) so that the thread
		// can complete processing in a reasonable time period.
		//
		THREAD_KPRI_REQUEST();
#endif

		//
		// Set up the primary_ctx in the environment.
		// Set up the saved state if it's a retry.
		//
		primary_ctx::begin_local(this, e);
		ASSERT(!e.exception());

		//
		// Note that thread priority reverts to normal
		// in method "local_invoke_done".
		//
#ifdef FAULT_HA_FRMWK

		void		*fault_argp;
		uint32_t	fault_argsize;

		if (fault_triggered(FAULTNUM_HA_FRMWK_LOCAL_REMOTE,
		    &fault_argp, &fault_argsize)) {

			InvoTriggers::clear(FAULTNUM_HA_FRMWK_LOCAL_REMOTE);

			//
			// Undo the the try_local_invo call.
			//
			_servicep->invo_done(&e);

			//
			// Leave the Environment alone.
			// Want the context to remain unchanged for the retry.
			//

			uint32_t	totalsize;
			FaultFunctions::switchover_arg_t *switchover_argp =
			    FaultFunctions::switchover_arg_alloc(totalsize,
			    (nodeid_t)1,
			    "ha_sample_server",
			    "2");

			os::warning("LOCAL_REMOTE n %d svc %s\n",
			    switchover_argp->nodeid, switchover_argp->svc_info);

			// The system clears the fault number
			FaultFunctions::switchover(
			    FAULTNUM_HA_FRMWK_LOCAL_REMOTE,
			    switchover_argp, totalsize);

			os::warning("LOCAL_REMOTE done\n");

#ifdef _KERNEL
			THREAD_KPRI_RELEASE();
#endif
			return (NULL);
		}

#endif // FAULT_HA_FRMWK

		// Check for an unknown deep type.
		if (infp->is_unknown()) {
			//
			// Return the COMM_FAILURE exception since the object
			// reference is dead.
			//
			e.system_exception(CORBA::COMM_FAILURE(0,
			    CORBA::COMPLETED_NO));

			// Clean up the environment and transaction context.
			e.mark_used();
			((primary_ctx*)e.trans_ctxp)->completed_local(e);

			// Undo the the try_local_invo call.
			_servicep->invo_done(&e);

#ifdef _KERNEL
			THREAD_KPRI_RELEASE();
#endif
			return (NULL);
		}
		//
		// Check to make sure the implementation object and proxy
		// deep type both support the method being called.
		//
		InterfaceDescriptor &idesc =
		    *_impl_object->_interface_descriptor();
		int inf_index = idesc.lookup_type_index(to_tid, true);
		if (inf_index < 0 ||
		    (infp != &idesc && !infp->is_type_supported(to_tid))) {
			//
			// The interface is not supported, throw the version
			// exception.
			//
			e.system_exception(CORBA::VERSION(0,
			    CORBA::COMPLETED_NO));

			// Clean up the environment and transaction context.
			e.mark_used();
			((primary_ctx*)e.trans_ctxp)->completed_local(e);

			// Undo the the try_local_invo call.
			_servicep->invo_done(&e);

#ifdef _KERNEL
			THREAD_KPRI_RELEASE();
#endif
			return (NULL);
		}
		//
		// Cast the implementation object to the C++ class that
		// will be used to make the call (see the stub code
		// generated by the IDL compiler).
		//
		return (idesc.cast(_impl_object->_this_ptr(),
		    (uint_t)inf_index));
	} else {
		return (NULL);
	}
}

//
// local_invoke_done
// A local invoke has completed.  We can now release the transaction id
// reference and tell it that the transaction is complete.
//
ExceptionStatus
replica_handler::local_invoke_done(Environment &e)
{
	ExceptionStatus retval = CORBA::NO_EXCEPTION;
	if (e.exception() &&
	    CORBA::PRIMARY_FROZEN::_exnarrow(e.exception())) {
		//
		// The service detected that we were freezing the primary and
		// decided to defer processing this method until the primary
		// is unfrozen. We drop the hold on the freeze lock and block
		// until the service is unfrozen.
		//
		e.clear();
		retval = CORBA::RETRY_EXCEPTION;
		_servicep->invo_done(&e);
		_servicep->wait_for_unfrozen();
	} else {
		primary_ctx	*ctxp = primary_ctx::extract_from(e);
		ASSERT(ctxp != NULL);
		if (ctxp->need_commit()) {
			get_service()->get_repl_provp()->add_commit(
			    ctxp->get_trans_id());
		}
		ctxp->completed_local(e); // deletes ctxp
		_servicep->invo_done(&e);

		//
		// Mark in the Environment that an invocation completed to
		// verify that the caller is checking for exceptions
		//
		e.mark_used();
	}

#ifdef _KERNEL
	THREAD_KPRI_RELEASE();
#endif
	return (retval);
}

//
// Called by the hxdoor.
//
void
replica_handler::handle_incoming_call(service &serv)
{
	MC_PROBE_0(replica_incoming_call_start, "clustering HA handler", "");

	//
	// Replica Handler Header -
	// After the header for the xdoor layer has been stripped off,
	// the header now consists of:
	//	TID	- type ID of the interface to use (instead of index).
	//	Method	- identifies the method to be invoked.
	//	Object  - reference to transaction id object.
	//
	// Note that the invocation method is totally general.
	// The information must identify the method and the class,
	// which is represented by a receiver function.
	// Note that this does not require the component to have a full IDL
	// interface, but something that follows these conventions.
	//
	Environment	&e = *(serv.get_env());
	primary_ctx	ctx(this, primary_ctx::MINI_TRANSACTION, e);

	// Get the object.
	ASSERT(_impl_object != NULL);
	CORBA::Object_ptr object_p = _impl_object;

	InterfaceDescriptor *infp = _impl_object->_interface_descriptor();
	InterfaceDescriptor::ReceiverFunc recfunc;
	uint_t inf_index;
	uint_t method;

	//
	// Note: this is copied from remote_handler::get_index_method()
	// except that we have the type ID rather than an interface index.
	// Extract the type ID of the interface for the method number.
	//
	CORBA::MD5_Tid tid;
	_ox_get_tid(serv, tid);

	inf_index = (uint_t)infp->lookup_type_index(tid, false);

	// Extract the method number.
	if (infp->sizeof_methods(inf_index) == 1)
		method = serv.get_octet();
	else
		method = serv.get_unsigned_short();

	recfunc = infp->lookup_rec_func(inf_index, method);

	if (recfunc == NULL) {
		ASSERT(0);
		// XXX need to free service object. Generic reply?
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
	}

	// Get the transaction ID object.
	replica::transaction_id_var tr_obj =
	    (replica::transaction_id_ptr)serv.get_object(
	    &_replica_transaction_id_ArgMarshalFuncs);

	ctx.begin_remote(tr_obj, this, e);

	if (e.exception() == NULL) {
		if (!_servicep->try_incoming_invo(&e)) {
			e.system_exception(
			    CORBA::PRIMARY_FROZEN(0, CORBA::COMPLETED_NO));
		}
	} else {
		ASSERT(ctx.get_saved_state() == NULL);
	}
	//
	// If the client is dead we neglect the request. If there is a
	// saved_state for the request then let _unreferenced() deal with it.
	//
	if (e.exception()) {
		//
		// Roll back the translate_in on xdoors. We need to minus one
		// here because the transaction_id is already unmarshaled in
		// by its handler and we don't need to rollback it.
		// Do not do a prepare(). Leave the XdoorTab state
		// as it was, so it will roll back only the xdoors that
		// have not yet been unmarshaled
		//
		Xdoor_manager::translate_in_roll_back(
			serv.re->get_xdoorcount() - 1,
			serv.re->get_XdoorTab());

		// Dispose the recstream now
		serv.set_recstream(NULL);

		xdoorp->send_reply(serv);

		ASSERT(!e.exception());
	} else {
		(*recfunc)(infp->cast(object_p->_this_ptr(), inf_index), serv);
		_servicep->invo_done(&e);

		// Mark the mini transaction as committed.
		if (ctx.need_commit()) {
			get_service()->get_repl_provp()->add_commit(tr_obj);
		}
	}
	ASSERT(serv.se == NULL);

	ctx.completed_remote(e);

	MC_PROBE_0(replica_incoming_call_end, "clustering HA handler", "");
}

#else	// _KERNEL_ORB

void
replica_handler::handle_incoming_call(service &)
{
	//
	// SCMSGS
	// @explanation
	// An invocation was made on an HA server object in user land. This is
	// not currently supported.
	// @user_action
	// Contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "HA: not implemented for userland");
}

#endif	// _KERNEL_ORB

//
// Narrow the object to an object of the given type.
// Note that we can only get a new reference from
// get_objref(), unmarshal(), or narrow().
//
void *
replica_handler::narrow(CORBA::Object_ptr object_p,
    CORBA::TypeId to_tid, generic_proxy::ProxyCreator create_func)
{
	// No need to lock this assert since this must be true for the
	// duration of the call.
	ASSERT(_nref > 0 || _impl_object != NULL);

	void	*new_proxyp = direct_narrow(object_p, to_tid);
	if (new_proxyp != NULL) {
		//
		// Successful narrow on the existing proxy.
		// Increase the refcount in the proxy.
		//
		incpxref(object_p);
	} else if (object_p->_deep_type()->is_type_supported(to_tid) ||
	    object_p->_deep_type()->is_unknown()) {
		//
		// Note that the check:
		// object_p->_deep_type()->is_type_supported(to_tid)
		// is to make sure we don't allow invocations on the
		// server object it doesn't support.
		// If the deep type is unknown, we allow narrow
		// but don't allow invocations (see invoke()).
		//
		new_proxyp = (*create_func)(this, object_p->_deep_type());
		if (new_proxyp != NULL) {
			lock();

			// Increase the handler reference count.
			_nref++;
			ASSERT(_nref != 0);

			//
			// If we were unable to create a single proxy for
			// the deep type when the object was unmarshalled,
			// we save the pointer to reduce the number of
			// proxies created.
			//
			if (CORBA::is_nil(proxy_obj_) &&
			    !object_p->_deep_type()->is_unknown()) {
				proxy_obj_ = (CORBA::Object_ptr)new_proxyp;
			}

			unlock();
		}
	}
	return (new_proxyp);
}

//
// duplicate - someone is using an additional object reference.
//
void *
#ifdef DEBUG
replica_handler::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId tid)
#else
replica_handler::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));

	// Cannot duplicate if you do not already have a reference.
	ASSERT(_nref > 0);

	// Increase the proxy reference count.
	incpxref(object_p);

	return (object_p);
}

#ifdef _KERNEL_ORB
//
// become_primary_event - Notify handler that it has become a primary.
//
void
replica_handler::become_primary_event()
{
	ASSERT(state_.get() == SECONDARY_HANDLER);
	state_.set(ACTIVE_HANDLER);
}


//
// become_secondary_event - Notify handler that it has become a secondary.
//
void
replica_handler::become_secondary_event()
{
	ASSERT(state_.get() == ACTIVE_HANDLER ||
	    state_.get() == SECONDARY_HANDLER ||
	    state_.get() == UNREF_DONE);
	state_.set(SECONDARY_HANDLER);
}
#endif // _KERNEL_ORB

//
// trans_ctx methods
//

trans_ctx::~trans_ctx()
{
	ASSERT(trans_id_p == nil);
}

//
// client_ctx methods
//

//
// client_ctx constructor - the client context obtains an HA transaction id
// object reference. The client context is solely responsible for releasing
// the HA transaction id object reference.
//
client_ctx::client_ctx(replica_handler *handp) :
    trans_ctx(trans_ctx::CLIENT, ha_tid_factory::the().get_tid()),
    handlerp(handp)
{
}

client_ctx::~client_ctx()
{
}

// Begin a mini-transaction request on a client.
client_ctx *
client_ctx::begin(replica_handler *handp, Environment &e)
{
	//
	// This is a new request. Allocate a transaction ID object
	// for this mini-transaction. The constructor will get a new tid.
	//
	e.trans_ctxp = new client_ctx(handp);

	return ((client_ctx *)e.trans_ctxp);
}

// We have completed a mini-transaction on the client.
void
client_ctx::completed(Environment &e)
{
	ASSERT(!CORBA::is_nil(trans_id_p));
	Environment local_e;
	trans_id_p->completed(local_e);
	ASSERT(!local_e.exception());

	CORBA::release(trans_id_p);
	trans_id_p = nil;
	delete this;
	e.trans_ctxp = NULL;
}

#ifdef _KERNEL_ORB

//
// primary_ctx methods
//

primary_ctx::~primary_ctx()
{
	CL_PANIC(saved_state == NULL);
	clnt_ctx = NULL;		// Make lint happy
	handlerp = NULL;		// Make lint happy
}

void
primary_ctx::setup_local(client_ctx *c_ctxp, Environment &e)
{
	ASSERT(c_ctxp != NULL);

	// Set the primary_ctx up properly.
	clnt_ctx = c_ctxp;
	trans_id_p = c_ctxp->trans_id_p;

	//
	// Any existing client context must be associated with the same
	// handler.  This assertion should catch illegal attempts to reuse the
	// environment for invocations on different ha services.
	//
	ASSERT(c_ctxp->handlerp == handlerp);

	// Set the environment up properly.
	e.trans_ctxp = (void *)this;
}

//
// begin_local - Begin a mini-transaction request on the primary node.
//
void
primary_ctx::begin_local(replica_handler *handp, Environment &e)
{
	if (e.trans_ctxp != NULL) {
		//
		// A retry. The transaction context is either a client
		// context when the earlier attempt was remote, or a
		// primary_ctx when the earlier attempt was local.
		//
		trans_ctx	*ctxp = (trans_ctx *)e.trans_ctxp;
		if (ctxp->get_type() == trans_ctx::CLIENT) {
			primary_ctx	*p_ctxp;

			p_ctxp = new primary_ctx(handp);
			p_ctxp->setup_local((client_ctx *)ctxp, e);

			// Get saved state if any
			p_ctxp->get_saved_state(handp, e);

			//
			// If we have transaction_state we have to send commit
			// later.  The new primary may not send any more
			// checkpoints so we can't depend on the checkpointing
			// to set the commit needed flag.
			//
			if (p_ctxp->get_saved_state() != NULL) {
				p_ctxp->needcommit_yes();
			}
		} else {
			//
			// The environment must contain a primary_ctx and
			// we are retrying because the previous attempt
			// raised the freezing_need_retry exception.
			// We can simply reuse the old primary_ctx.
			// Note that we can't have a secondary_ctx, since
			// they are only used for checkpoints.
			//
			ASSERT(ctxp->get_type() == trans_ctx::PRIMARY);
		}
	} else {
		e.trans_ctxp = new primary_ctx(handp);
	}
}

//
// Add a new client_ctx where we didn't have one before
//
void
primary_ctx::add_client_ctx(Environment &e)
{
	setup_local(client_ctx::begin(handlerp, e), e);
}

void
primary_ctx::completed_local(Environment &e)
{
	ASSERT(e.trans_ctxp == this);

	// release any hold on the saved state.
	if (saved_state != NULL) {
		saved_state->invoke_completed();
		saved_state = NULL;
	}

	if (clnt_ctx != NULL) {
		ASSERT(is_not_nil(trans_id_p));
		//
		// Clean up transaction id.
		// After this call the primary_ctx may be deleted any time.
		//
		clnt_ctx->completed(e);
	} else {
		ASSERT(CORBA::is_nil(trans_id_p));
		e.trans_ctxp = NULL;
	}
	trans_id_p = nil;
	delete this;
}

void
primary_ctx::begin_remote(replica::transaction_id_ptr trid_p,
    replica_handler *handp, Environment &e)
{
	trans_id_p = trid_p;
	get_saved_state(handp, e);

	//
	// If we have transaction_state we have to send commit later.
	// The new primary may not send any more checkpoints so we
	// can't depend on the checkpointing to set the commitneeded.
	//
	if (saved_state != NULL) {
		needcommit_yes();
	}
}

void
primary_ctx::completed_remote(Environment &e)
{
	ASSERT(e.trans_ctxp == this);

	// release any hold on the saved state.
	if (saved_state != NULL) {
		saved_state->invoke_completed();
		saved_state = NULL;
	}
	trans_id_p = nil;
	e.trans_ctxp = NULL;
}

void
primary_ctx::get_saved_state(replica_handler *handp, Environment &e)
{
	ASSERT(saved_state == NULL);
	saved_state = handp->get_service()->
	    get_repl_provp()->lookup_state(trans_id_p, &e);
}

//
// secondary_ctx methods
//

secondary_ctx::~secondary_ctx()
{
	ASSERT(saved_state == NULL);
	handlerp = NULL;		// Make lint happy
}

#endif // _KERNEL_ORB

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <repl/service/replica_handler_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
