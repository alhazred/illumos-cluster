//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// repl_tid_impl.cc
//
//

#pragma ident	"@(#)repl_tid_impl.cc	1.18	08/05/20 SMI"

#include <repl/service/repl_tid_impl.h>
#include <nslib/ns.h>
#include <repl/service/replica_handler.h>
#include <orb/infrastructure/orbthreads.h>

replica::transaction_id_ptr
kernel_tid_factory::get_tid(Environment &)
{
	lck.lock();
	ha_transaction_id *tid_impl = new ha_transaction_id(this);
	REPL_DBG(("kernel_tid_factory::get_tid: %p %p count: %d\n",
	    this, tid_impl, tid_list.count()));

	replica::transaction_id_ptr tid_ptr = tid_impl->get_objref();
	tid_list.append(tid_impl);
	lck.unlock();
	return (tid_ptr);
}

void
#ifdef DEBUG
kernel_tid_factory::_unreferenced(unref_t cookie)
#else
kernel_tid_factory::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	// The usrland program exited or died. We need to pass this information
	// on to all the transaction_ids we created.
	ha_transaction_id *tid_ptr = NULL;
	lck.lock();
	is_unref = true;
	REPL_DBG(("kernel_tid_factory::_unreferenced %p count: %d\n", this,
	    tid_list.count()));

	SList<ha_transaction_id>::ListIterator iter(tid_list);
	while ((tid_ptr = iter.get_current()) != NULL) {
		iter.advance();
		if (tid_ptr->client_exited()) {
			(void) tid_list.erase(tid_ptr);
		}
	}
	try_delete();
}

void
kernel_tid_factory::try_delete()
{
	ASSERT(lck.lock_held());
	if (tid_list.empty() && is_unref) {
		REPL_DBG(("deleting kernel_tid_factory: %p\n", this));
		delete this;
	} else {
		unlock();
	}
}

// IDL method
// Called by the secondary as part of processing a checkpoint message
// for this transaction.
// If we have already received a reply, then we ignore this callback.
// Otherwise we keep track of the reference to the secondary state object.
// This ensures that the secondary will hang on to the state in case we need
// to do a retry.
void
ha_transaction_id::register_state(replica::trans_state_ptr secondary_obj,
    CORBA::Environment &)
{
	lck.lock();
	if (!done) {
		secondary_state_list.prepend(new
		    secondary_state_elem(secondary_obj));
	}
	lck.unlock();
}

// Called locally by the client to indicate that the transaction has completed.
// Release references to any secondary objects so they can throw away their
// state.
void
ha_transaction_id::completed(Environment &)
{
	lck.lock();
	completed_locked();
	lck.unlock();
}

void
ha_transaction_id::completed_locked()
{
	ASSERT(lck.lock_held());
	// We need to check whether done is set because if the usrland
	// process dies it's possible for client_exited to be called first.
	if (!done) {
		done = true;

		// Remove it from the factory.
		if (factoryp != NULL) {
			lck.unlock();

			// We need to drop the lock before calling remove.
			// It's ok to drop the lock here since we just set
			// done to true and if there is another thread calling
			// client_exited it will see the done and leave us
			// alone.
			factoryp->remove(this);

			lck.lock();
			factoryp = NULL;
		}

		secondary_state_list.dispose();
	}
}

bool
ha_transaction_id::client_exited()
{
	bool retval = false;
	lck.lock();
	REPL_DBG(("ha_transaction_id::client_exited %p %d\n", this, done));
	ASSERT(factoryp != NULL);

	// If completed() thread is running we let it handle the cleanup.
	if (!done) {
		done = true;
		factoryp = NULL;
		common_threadpool::the().defer_processing(
		    new client_died_task(this));

		// If another thread is calling completed and is accessing the
		// factoryp, we return false and keep this object around. The
		// other thread will release the object when it's done with it.
		retval = true;
	}

	lck.unlock();
	return (retval);
}

// When the client gets unreferenced, then no replicas have any more state
// about the transaction. The completed() call has already been made (the
// client keeps a reference until after it calls completed()).
void
ha_transaction_id::_unreferenced(unref_t cookie)
{
	lck.lock();
	if (!_last_unref(cookie)) {
		lck.unlock();
		return;
	}
	REPL_DBG(("ha_transaction_id::_unreferenced %p %d\n", this, done));

	// It's possible for a usrland client to get a tid and then dies.
	// In that case we can get _unreferenced before the kernel_tid_factory
	// gets unreferenced. We need to call completed to remove ourselves
	// from the kernel_tid_factory.
	completed_locked();

	delete this;
}

void
ha_transaction_id::broadcast_client_death()
{
	lck.lock();
	REPL_DBG(("broadcast_client_death %p\n", this));
	ASSERT(done);
	// Tell all the transaction_states that they are now orphaned.
	secondary_state_elem *elemp;
	Environment e;
	IntrList<secondary_state_elem, _SList>::ListIterator
	    iter(secondary_state_list);
	while ((elemp = iter.get_current()) != NULL) {
		iter.advance();
		REPL_DBG(("client died: %p\n", elemp));
		elemp->objp_->client_died(e);
		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()) !=
			    NULL);
			e.clear();
		}
	}
	secondary_state_list.dispose();
	lck.unlock();
}
