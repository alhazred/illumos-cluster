/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MULTI_CKPT_HANDLER_H
#define	_MULTI_CKPT_HANDLER_H

#pragma ident	"@(#)multi_ckpt_handler.h	1.62	08/05/20 SMI"

#include <sys/os.h>
#include <h/replica.h>
#include <h/replica_int.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>
#include <orb/handler/handler.h>
#include <sys/list_def.h>
#include <repl/service/transaction_state.h>

//
// The place where a thread goes to sleep when it waits for its ack inside
// an invoke.
//
class sleep_elem : public _DList::ListElem {
public:
	sleep_elem(replica::ckpt_seq_t s);
	replica::ckpt_seq_t seq;
	os::condvar_t cv;
};

typedef SList<replica::transaction_id> commit_list;

// Stores the ids of an unreferenced ha objects.
class delete_elem : public _SList::ListElem {
public:
	delete_elem(replica::service_num_t, xdoor_id);
	replica::service_num_t serviceid;
	xdoor_id objectid;
};

typedef IntrList<delete_elem, _SList> delete_list;

//
// The class that helps to compare two sequence numbers when wrap around
// could occur. To be able to compare two sequence numbers we need one
// minimum sequence number. Note that below only > and == is originally
// implemented. Every other operators depend on these two.
//
class ckpt_info {
public:
	ckpt_info(replica::ckpt_seq_t a, replica::ckpt_seq_t c);
	bool operator > (replica::ckpt_seq_t a);
	bool operator == (replica::ckpt_seq_t a);
	bool operator < (replica::ckpt_seq_t a);
	bool operator <= (replica::ckpt_seq_t a);
	bool operator >= (replica::ckpt_seq_t a);
	bool operator != (replica::ckpt_seq_t a);
private:
	//
	// The minimum seq that we know both numbers we compare will be
	// bigger than.
	//
	replica::ckpt_seq_t minseq;
	replica::ckpt_seq_t value;
};

class ckpt_handler_server;

//
// The handler that manages the secondaries. It handles tasks of forwarding
// checkpoint calls to the underlying ckpt_handlers, synchronizing among
// secondaries when it first comes up as primary (become_primary_prepare),
// and interfacing with generic_repl_prov to add and remove secondaries.
//
class multi_ckpt_handler : public handler {
public:
	multi_ckpt_handler(ckpt_handler_server *hdlrp, state_hash_table_t *t);

	~multi_ckpt_handler();

	// Below are methods to be called by generic_repl_prov.

	// Release one underlying references.
	void	release_ref(replica::checkpoint_ptr ckpt);

	// Release all the underlying references.
	void	release_refs(bool is_shutdown);

	// Add a new checkpoint reference to the multicast group.
	void	add_ref(replica::checkpoint_ptr ckpt);

	// Synchronize among the multicast group.
	void	become_secondary();

	// Check the validity of a new checkpoint
	bool	check_secondary(replica::checkpoint_ptr sec);

	// Add a new secondary into the multicast group and initialize it.
	void	add_secondary(replica::checkpoint_ptr sec, Environment &e);

	// Synchronize among the multicast group.
	void	become_primary(const replica::secondary_seq &);

	// Quiesce the checkpoints on this node.
	void	become_spare();

	// Cleanup before shutdown.
	void	shutdown();

	//
	// Add the transaction_id contained in the environment into
	// the commit list.
	//
	void	add_commit(replica::transaction_id_ptr);

	//
	// Add the deletion of a ha object into the delete_list.
	// The deletion will be piggybacked onto the next checkpoint.
	//
	void	add_delete(replica::service_num_t, xdoor_id);

	// Lower layer interface

	//
	// Called from the stubs. Forwarded to the members of the multicast
	// group.
	//
	ExceptionStatus	invoke(arg_info &, ArgValue *, InterfaceDescriptor *,
			    Environment &);

	// Helper methods for reference counting.

	void	*direct_narrow(CORBA::Object_ptr, CORBA::TypeId);
	bool	local_narrow(CORBA::Object_ptr, CORBA::TypeId);
	void	new_reference();

	void	incpxref(CORBA::Object_ptr);
	uint_t	decpxref(CORBA::Object_ptr);

	// These methods are now available for reference counting.

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void	release(CORBA::Object_ptr);

	// These methods shall never be called.

	void	marshal(service &, CORBA::Object_ptr);
	void	marshal_roll_back(CORBA::Object_ptr);
	bool	is_local();
	bool	is_user();

	static	void	initialize();
	//
	// We send a flush_to request once every flush_range messages.
	// The sequence number we flush to is commit_seq - flush_range.
	//
	static replica::ckpt_seq_t	flush_range;

	// Function to upgrade this handler to supporting a newer
	// type of checkpoints.
	bool	set_type(CORBA::type_info_t *tp);

	ckpt_handler_server		*get_my_ckpt_handler_svr();

private:

	// Convenience methods called by invoke()
	void	wait_for_ckpt_ack(replica::ckpt_seq_t);
	void	flush_to_secondaries(replica::ckpt_seq_t);

	void				lock();
	void				unlock();

	// stores the multicast group.
	SList<replica::checkpoint>	secondaries;

	// where threads that are waiting for ack in invoke() go to sleep.
	IntrList<sleep_elem, _DList>	sleeping;

	// the minimum seq of all unacked checkpoints
	replica::ckpt_seq_t		min_seq;

	// the seq for the next checkpoint
	replica::ckpt_seq_t		max_seq;

	// the highest seq of all checkpoints ready to commit
	replica::ckpt_seq_t		commit_seq;

	// The seq for the last flush_to call.
	replica::ckpt_seq_t		last_flush_seq;

	// The ckpt_handler_server for the service when it is a secondary.
	ckpt_handler_server		*my_ckpt_handler_svr;

	// The state hash table of the service.
	state_hash_table_t		*state_hash;

	// Set to true if the last checkpoint we sent out has content in it,
	// therefore we need to send another checkpoint that commit it.
	bool				last_ckpt_need_commit;

	//
	// The mutex protects everything except the secondary list,
	// the cmt_list and the dlt_list.
	//
	os::mutex_t			lck;

	os::condvar_t			cv;

	// The reader/writer lock is used to protect the secondary list
	os::rwlock_t			rwlck;

	// This list stores the transaction_ids of the committed transactions
	commit_list			cmt_list;

	// The list that contains all the deleted ha objects's ids.
	delete_list			dlt_list;

	//
	// Protects dlt_list and cmt_list. We need a seperate lock because we
	// get the lock from the unreferenced thread, therefore we shouldn't
	// use the same lock that protects an invocation. All operations
	// under this lock are local and fast and don't block on anything.
	//
	os::mutex_t			fastlck;

	// Count of proxies/pointers.
	uint_t	nref;

	// The lowest type which is supported by this handler
	CORBA::type_info_t	*current_type;

	// Disallow assignments and pass by value
	multi_ckpt_handler(const multi_ckpt_handler &);
	multi_ckpt_handler & operator = (multi_ckpt_handler &);
};

#ifndef NOINLINES
#include <repl/service/multi_ckpt_handler_in.h>
#endif  // _NOINLINES

#endif	/* _MULTI_CKPT_HANDLER_H */
