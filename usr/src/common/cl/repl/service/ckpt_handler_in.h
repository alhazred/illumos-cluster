/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  ckpt_handler_in.h
 *
 */

#ifndef _CKPT_HANDLER_IN_H
#define	_CKPT_HANDLER_IN_H

#pragma ident	"@(#)ckpt_handler_in.h	1.38	08/05/20 SMI"

//
// ckpt_elem inline methods
//

inline
ckpt_elem::ckpt_elem(replica::ckpt_seq_t number, service &serv,
    ckpt_handler_server *handlerp) :
	seq(number),
#ifdef _KERNEL_ORB
	env(serv.get_env()->get_src_node()),
#endif
	re(serv.re),
	ckhandler(handlerp)
{
	serv.re = NULL;
}

inline
ckpt_list::~ckpt_list()
{
	CL_PANIC(empty());
}

//
// ckpt_handler_kit methods
//

inline
ckpt_handler_kit::ckpt_handler_kit() : remote_handler_kit(CKPT_HKID)
{
}

inline void
ckpt_handler_kit::unmarshal_cancel_remainder(service &)
{
}

//
// ckpt_handler methods
//

inline
ckpt_handler::ckpt_handler(Xdoor *xdp) :
	remote_handler(xdp),
	nref(0)
{
}

inline
ckpt_handler::ckpt_handler() :
	nref(0)
{
}

inline void
ckpt_handler::incref()
{
	lock();
	nref++;
	unlock();
}

inline unref_task_type_t
ckpt_handler_server::unref_task_type()
{
	return (unref_task::HANDLER);
}


inline void
ckpt_handler_server::set_state_info(state_hash_table_t *ht)
{
	hashtab = ht;
}

inline state_hash_table_t *
ckpt_handler_server::get_state_hash()
{
	return (hashtab);
}

// The following functions are to be called by ckpt_obj only.
inline replica::ckpt_seq_t
ckpt_handler_server::get_min_seq()
{
	lock();
	replica::ckpt_seq_t a = min_seq;
	unlock();
	return (a);
}

inline CORBA::Object_ptr
ckpt_handler_server::_obj()
{
	return (obj_);
}

inline void
ckpt_handler_server::stop_receiving_ckpt()
{
	lock();
	receiving_ckpt = false;
	unlock();
}

inline void
ckpt_handler_server::start_receiving_ckpt()
{
	lock();
	receiving_ckpt = true;
	unlock();
}

inline bool
ckpt_handler_server::is_receiving_ckpt()
{
	bool	ret;
	lock();
	ret = receiving_ckpt;
	unlock();
	return (ret);
}

//
// ckpt_handler_client methods
//

inline
ckpt_handler_client::ckpt_handler_client(Xdoor *xdp) :
	ckpt_handler(xdp),
	dump_seq(1),
	last_flush_seq(0),
	proxy_obj_(CORBA::Object::_nil())
{
}

inline
ckpt_handler_client::~ckpt_handler_client()
{
}

inline void
ckpt_handler_client::init(replica::checkpoint_ptr sec)
{
	objref = sec;
	dump_seq = 1;
	last_flush_seq = 0;
}

inline replica::ckpt_seq_t
ckpt_handler_client::get_last_dump_seq()
{
	return (dump_seq - 1);
}

inline void
ckpt_handler_client::incpxref(CORBA::Object_ptr o)
{
	lock();
	o->_this_component_ptr()->increase();
	unlock();
}

inline uint_t
ckpt_handler_client::decpxref(CORBA::Object_ptr o)
{
	return (o->_this_component_ptr()->decrease());
}

inline CORBA::Object_ptr
ckpt_handler_client::_obj()
{
	return (nil);
}

//
// Methods for ckpt_handler_client_invoke
//
inline
ckpt_handler_client::ckpt_handler_client_invoke::ckpt_handler_client_invoke() :
	number_commits(0),
	number_deletes(0)
{
}

#endif	/* _CKPT_HANDLER_IN_H */
