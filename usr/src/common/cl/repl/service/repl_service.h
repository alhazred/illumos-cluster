/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPL_SERVICE_H
#define	_REPL_SERVICE_H

#pragma ident	"@(#)repl_service.h	1.89	08/05/20 SMI"

#include <sys/os.h>
#include <sys/hashtable.h>
#include <orb/debug/haci_debug.h>
#include <orb/object/adapter.h>
#include <orb/infrastructure/freeze_lock.h>
#include <h/replica.h>
#include <h/replica_int.h>

#ifdef _KERNEL_ORB
#include <repl/service/multi_ckpt_handler.h>
#include <repl/service/transaction_state.h>
#include <repl/service/periodic_thread.h>
#include <orb/refs/unref_threadpool.h>
#endif

class replica_handler;

//
// XXX This is not a very good place.  Move this comment block
// somewhere else.
//
// Lock Ordering
// There are many layers in the orb which hold locks.  Each layer needs to
// perform operations with its lock and that of the next layer (above or below)
// held.  Most of the complexity is related to creation and deletion of
// services, since these can be created a layer above or below the current
// layer. The following locks are held in the kernel.
//
// standard xdoor lock
// hxdoor lock
// replica handler lock
// repl_service lock
// repl_service_manager lock
// generic_repl_prov lock
//
// They are acquired in the following order:
// replica handler > hxdoor > standard xdoor
// repl_service_manager > repl_service > generic_repl_prov
//
// The general model is that when we receive a reference to an object (handler
// or service) from a lower layer, we acquire the lock of the lower layer and
// establish the appropriate linkage between this and the lower layer,
// allocating the object if necessary. When we decide to delete something at a
// higher layer (usually because no local references exist), we must acquire
// the lock on the lower layer to ensure that we are not receiving another
// reference to the same object.  We usually make the decision to delete while
// holding this layer's lock, so to maintain lock ordering, we must drop our
// lock before acquiring the lower layer's lock.  This opens a window where a
// new reference to the object may come in from the lower level.  We use a
// version number on each object increase it (holding both locks) whenever we
// detect that we are performing such a "zero to one" transition.  This
// protocol is used between hxdoors and standard xdoors, replica handlers and
// hxdoors, replica handlers and repl_services?, repl_services and the
// replica_service_manager, generic_repl_provs and repl_services.  Standard
// xdoors, replica handlers and generic_repl_provs are all at the end of a
// linkage chain, and they each use their own reference counting technique to
// decide when to try to go away (foreign reference counts for standard xdoors,
// proxy counts for replica handlers, replica manager requests for
// generic_repl_provs.

#if defined(HACI_DBGBUFS) && !defined(NO_SERV_DBGBUF)
#define	SERV_DBGBUF
#endif

#ifdef	SERV_DBGBUF
extern dbg_print_buf service_dbg;
#define	SERV_DBG(a)	HACI_DEBUG(ENABLE_SERVICE_DBG, service_dbg, a)
#else
#define	SERV_DBG(a)
#endif


// Forward declaration
class generic_repl_prov;
class repl_service;
class transaction_state;
class state_hash_table_t;

#ifdef _KERNEL_ORB

class multi_ckpt_handler;
class ckpt_handler_server;

//
// During some stages of reconfiguration (such as when the service is frozen),
// we don't want to deliver unreferenced to replica_handlers, instead we store
// those unreferenced in service_unref to be processed later. service_unref
// also keeps track of the number of unreferenced currently being processed by
// the unreference thread.
//
class service_unref {
public:
	service_unref();
	~service_unref();

	//
	// Depending on our current state, add handler unreference task
	// to the defer_list or the unref_threadpool.
	// Called by replica_handler via generic_repl_prov.
	//
	void add_unref(replica_handler *);

	//
	// Called by replica_handler via generic_repl_prov at the end of
	// deliver_unreferenced. It decrements the unref_count and does a
	// cv.signal when unref_count reaches zero.
	//
	void unref_done();

	//
	// Enable unreferenced again. Deliver unreferenced to the handlers
	// in defer_list.
	//
	void enable_unreferenced();

	//
	// Called by generic_repl_prov::freeze_primary. It waits till the
	// unref_count to be zero.
	//
	void freeze_primary();

	//
	// Called by generic_repl_prov::become_secondary. It throws away
	// all the unreferenced queued up in the defer list.
	//
	void become_secondary();

#ifdef DEBUG
	void become_primary();
#endif

	//
	// Called by generic_repl_prov to make sure service has
	// completed unref.
	//
	void service_unref::wait_till_unref_done();

private:
	void lock();
	void unlock();

	//
	// The number of unref tasks that have been both
	// 1. given to the unref_threadpool for unreference processing, and
	// 2. have not reported processing completion.
	//
	// The system uses this count to ensure that certain actions
	// do not occur while unreference tasks are being processed.
	//
	uint_t unref_count;

	// Indicates whether we should defer unreferenced.
	bool defer_unref;

#ifdef DEBUG
	// Indicates whether we are primary
	bool is_primary;
#endif
	//
	// when the service is frozen, we put unreferenced handler in the
	// defer_list instead of processing them.
	//
	IntrList<unref_task, _SList> defer_list;

	// The lock that protects defer_list as well as unref_count.
	os::mutex_t lck;
	os::condvar_t cv;

	// Disallow assignments and pass by value
	service_unref(const service_unref &);
	service_unref & operator = (service_unref &);
};

// The task that gets executed periodically. It pushes uncommitted
// checkpoints on the secondaries to be executed.
class push_ckpt_task : public periodic_thread::periodic_job {
public:
	push_ckpt_task(generic_repl_prov *);
	~push_ckpt_task();
	void execute();
	uint_t get_period();
private:
	generic_repl_prov *repl_provp;
};

//
// The handler level repl_prov. It deals with reconfiguration requests at the
// handler level and forwards the requests to the service specific repl_provs.
//
class generic_repl_prov : public McServerof<replica_int::handler_repl_prov> {
public:
	// IDL methods.
	void	become_secondary(bool, Environment &);

	void	add_secondary(replica::checkpoint_ptr sec_chkpt,
	    const char *secondary_name,
	    Environment &);

	void	commit_secondary(Environment &);

	void	remove_secondary(replica::checkpoint_ptr sec_chkpt,
	    const char *secondary_name,
	    Environment &);

	void	freeze_primary(Environment &);
	void	unfreeze_primary(Environment &);
	void	become_primary(const replica::repl_name_seq &, Environment &);
	void	become_spare(Environment &);
	uint32_t	shutdown(replica::repl_prov_shutdown_type shutdown_type,
		    Environment &);

	void	become_primary_prepare(const replica::secondary_seq &,
	    Environment &);

	//
	// check if the provider is receiving ckpt from primary. This is
	// used on secondary only.
	//
	bool	is_receiving_ckpt(Environment &);

	CORBA::Object_ptr get_root_obj(Environment &);

	// _unreferenced() for replica_int::handler_repl_prov.
	void	_unreferenced(unref_t);

	// Methods called by the implementation
	void	register_with_rm(
	    replica::service_dependencies *service_depends_on,
	    replica::prov_dependencies *provider_depends_on,
	    uint_t priority, bool restart_flag,
	    uint32_t new_desired_numsecondaries, Environment &e);

	void	register_for_rmm();

	void	request_primary_mode();

	// Methods called by replica_handler

	// lookup saved state by transaction id.
	transaction_state *lookup_state(replica::transaction_id_ptr,
	    Environment *);

	//
	// Called by replica_handler.
	// Add handler unreference task to the appropriate list.
	//
	void add_unref(replica_handler *);

	// Called by replica_handler at the end of deliver_unreferenced.
	void unref_done();

	// End of methods called by replica_handler.

	repl_service *get_service();
	void	set_service(repl_service *rsp);

	replica::checkpoint_ptr get_checkpoint();

	// These are used by repl_server<CKPT> to initialize this class.
	bool set_checkpoint(replica::checkpoint_ptr p);
	handler *get_ckpt_handler();

	// Add a commit message for the transaction_id contained in the
	// environment
	void add_commit(replica::transaction_id_ptr t);

	// Add a delete ha object message. The deletion will be
	// piggybacked onto the next checkpoint message.
	void add_delete(replica::service_num_t, xdoor_id);

	void	set_service_id(replica::service_num_t sid);
	replica::service_num_t get_service_id();

	// Returns a copy of the service name string
	char *get_service_name() const;

	state_hash_table_t *get_state_hash();

	// This is called by state_hash_table_t::remove to notify us
	// that all the transaction states are gone.
	void all_states_gone();

	// Try to do the necessary cleanup when shutting down. It is
	// called with repl_service lock held and it will drop the lock.
	void try_cleanup();

	// This is always allocated as part of a full repl_prov implementation
	// using the repl_server<> template.
	generic_repl_prov(replica::repl_prov_ptr ag,
	    replica::checkpoint_ptr my_ckpt, ckpt_handler_server *ckpth,
	    const char *svc_nm, const char *prov_nm);

	// Destructor.
	~generic_repl_prov();

	// Accessor function for the implementation object.
	replica::repl_prov_ptr	get_implp();

private:
	// Utility function used by register_with_rm();
	void shutdown_dead_service();

	// It manages when to process unreferences and when to delay them.
	service_unref _service_unref;

	// The transaction_state hash table
	state_hash_table_t _state_hash;

	replica::repl_prov_ptr _actual_implp;

	// The global identifier for this service.
	replica::service_num_t _service_id;

	// The registered name for this service.
	char	*_service_name;

	// The registered name for this provider.
	char	*_provider_name;

	repl_service *_servicep;

	//
	// A proxy for the checkpoint interface. When we are primary we
	// associate this with a client multicast checkpoint handler
	// and return this as the checkpoint interface when requested.
	//
	replica::checkpoint_ptr _ckpt_proxy;

	// A multicast checkpoint handler.
	// This maintains a multicast group of all secondary checkpoints when
	// we are primary.
	multi_ckpt_handler _ckpt_handler;

	// The checkpoint interface for this service when it is a secondary.
	replica::checkpoint_ptr _my_ckpt;

	// We need to keep this pointer so that when we shutdown we can
	// notify the rm that we are going away.
	replica::repl_prov_reg_ptr scb;

	// The task that we run periodically to make sure that all checkpoints
	// are processed. This is to prevent the case where a checkpoint
	// sits on a secondary forever and holds all the references of objects
	// in it.
	push_ckpt_task push_task;

	// Disallow assignments and pass by value.
	generic_repl_prov(const generic_repl_prov &);
	generic_repl_prov &operator = (generic_repl_prov &);
};
#endif // _KERNEL_ORB

//
// There is one repl_service class per replicated service which is
// referenced in this domain (as primary, secondary or client). It is
// allocated the first time it is referenced.  This can be either when a
// secondary for the replicated service is created in this domain (note
// that all repl_provs begin life as a secondary) or when a reference to an
// object in the service is unmarshaled.
//
class repl_service {
public:
	// Constructor used when created without a repl_prov.
	repl_service(replica::service_num_t sid);

	void	lock();
	void	unlock();
	bool	lock_held();

	//
	// Add a new handler to the service. Either because it has been
	// unmarshaled or it has been created by the primary.
	//
	void	add_handler();
	void	add_handler_locked();

	//
	// Add a new handler to the service. Either because it has been
	// unmarshaled or it has been created by the primary.
	//
	void	rem_handler();

	// Answers whether there are anymore handlers in this domain.
	bool	has_no_handler();

	void	hold();
	void	release();

	// Returns true if it can be deleted, false otherwise.
	bool	verify_delete();

	//
	// Begin freezing invocations. Called before we call freeze_primary()
	// on the service implementation. This has the effect of causing
	// _freeze_in_progress() calls by the service to return true and
	// ensures that local invocations which return the retry exception
	// block instead of spinning.
	//
	void 	begin_freezing();

	// Wait until invocations have drained out and don't allow any more in.
	void 	freeze_invos();

	// Allow invocations to start again
	void 	unfreeze_invos();

	// Is a freeze_primary() in progress
	bool 	freeze_in_progress(Environment *);

	// Check whether invocations are frozen
	bool	invos_frozen();

	// Keep track of whether underlying repl_provp is the primary
	void 	became_primary();
	void 	became_secondary();
	bool	is_primary();

	//
	// Called by the handler to see if it should perform an invocation in
	// this domain.  Returns true if a local invocation has been permitted
	// - i.e. this domain contains an active primary.
	// If it returns true then the caller must later call invo_done() when
	// it is through with the invocation.
	//
	bool	try_local_invo(Environment *e);

	//
	// Called by the handler when it receives an invocation from another
	// domain. Returns true if the invocation can proceed and false if
	// the service is frozen.
	// If it returns true then the caller must later call invo_done() when
	// it is through with the invocation.
	// This function does not block because we do not want to tie up
	// server threads on the freezing primary.
	//
	bool	try_incoming_invo(Environment *e);

	//
	// Called when done with the permission granted by an earlier call to
	// try_local_invo() and try_incoming_invo().
	//
	void	invo_done(Environment *e);

	//
	// Called to block the current thread until the service is unfrozen.
	// This is done when we are freezing and want to block a retry until
	// the freeze and unfreeze have been performed.
	//
	void	wait_for_unfrozen();

	replica::service_num_t get_sid();

	enum {
		SHUTTING_DOWN = 0x1, // Whether this replica is shutting down.
		// Whether this replica is forced shutting down.
		FORCED_SHUTTING_DOWN = 0x2,
		REGISTRATION_DONE = 0x4 // whether the registration completed.
	};

	// set the flags
	void	mark_registration_done();
	void	mark_shutting_down(bool is_forced_shutdown);

	// clear the flags
	void	clear_flags();

	// get the flags
	bool	is_registration_done();
	bool	is_shutting_down();
	bool	is_forced_shutting_down();

#ifdef _KERNEL_ORB
	//
	// Add an repl_prov object to the service.  This happens when an
	// repl_prov is brought on line in this domain.  There can only be one
	// such repl_prov in the domain at any time.
	//
	void	add_repl_prov(generic_repl_prov *sp);

	//
	// Remove an repl_prov from the service. This happens when an repl_prov
	// is shutdown in this domain.  This may cause the service to go away.
	//
	void	rem_repl_prov();

	generic_repl_prov	*get_repl_provp();

	// An repl_prov has started up for service sid.
	static repl_service *add_new_repl_prov(
	    replica::service_num_t sid, generic_repl_prov *sp);
#endif // _KERNEL_ORB

private:
	//
	// The flags showing the state of the provider. Protected by the
	// mutex lock.
	//
	uint_t	flags;

	// See if the service should be deleted and delete it if so.
	// It is called with the lock held and returns with it released.
	void	try_delete();

#ifdef _KERNEL_ORB
	//
	// If this domain contains an repl_prov for the service, then this
	// points to it.
	//
	generic_repl_prov	*_repl_provp;
#endif // _KERNEL_ORB

	//
	// The number of handlers associated with this service in this domain.
	// Note that the service can go away when _repl_provp is NULL and
	// _num_handlers is zero.
	//
	uint_t			_num_handlers;

	// The global identifier for this service.
	replica::service_num_t	_service_id;

	// Is the _repl_provp (if any) primary?
	bool			_is_primary;

	// Freeze locks protecting invocations.
	freeze_lock		invo_fl;	// Used most of the time.
	freeze_lock		freezing_fl;	// Used when freezing.

	os::mutex_t		_lck;

	// Disallow assignments and pass by value
	repl_service(const repl_service &);
	repl_service &operator = (repl_service &);
};

// XXX should this be in a separate file?

// The repl_service_manager manages all the replicated services which are known
// to this domain.  It is consulted when we need to find a service based on its
// identifier and when we allocate a new service.
class repl_service_manager {
public:
	static int initialize();
	static void shutdown();

	// Lookup a service given its service id.
	// The lookup gets the repl_service lock to prevent it from
	// being released.
	// This call must be followed by a unlock() call on the repl_service,
	// when the hold should be released.
	repl_service *lookup_hold(replica::service_num_t sid, bool create);

	// Try to remove a service from the list.
	void	remove_service(replica::service_num_t sid);

	void	lock();
	void	unlock();
	bool	lock_held();

	static repl_service_manager &the();

private:
	os::mutex_t _lck;

	// Same with lookup_hold but called with the lock held.
	repl_service *lookup_hold_locked(replica::service_num_t sid,
	    bool create);

	static repl_service_manager *the_repl_service_manager;

	// We look up the services based on the service id
	hashtable_t<repl_service *, replica::service_num_t> _service_list;
};

#include <repl/service/repl_service_in.h>

#endif	/* _REPL_SERVICE_H */
