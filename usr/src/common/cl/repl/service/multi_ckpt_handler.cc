/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)multi_ckpt_handler.cc 1.88     08/05/20 SMI"

#include <repl/service/multi_ckpt_handler.h>
#include <repl/service/ckpt_handler.h>
#include <repl/service/replica_handler.h>
#include <orb/object/schema.h>
#include <orb/infrastructure/orb.h>

//
// We send a flush_to request once every flush_range messages.
// The sequence number we flush to is commit_seq - flush_range.
//
// This parameter can impact performance. On one hand you don't want
// to set the number too small because you don't want to do flush_to
// too often. On the other hand you don't want to set it too big either
// since in that case the flush_to can take too long and can make
// the primary look hung for too long.
//
replica::ckpt_seq_t multi_ckpt_handler::flush_range = 2000;

// /etc/system tunable for setting flush_range.
unsigned long ckpt_flush_range = multi_ckpt_handler::flush_range;

// initialize
//
// Sets static variables
// static
void
multi_ckpt_handler::initialize()
{
	if ((ckpt_flush_range >= 1000) && (ckpt_flush_range <= 10000)) {
		flush_range = (replica::ckpt_seq_t) ckpt_flush_range;
	}
}

multi_ckpt_handler::multi_ckpt_handler(ckpt_handler_server *hdlrp,
    state_hash_table_t *t) :
	min_seq(1),
	max_seq(1),
	commit_seq(0),
	last_flush_seq(0),
	my_ckpt_handler_svr(hdlrp),
	state_hash(t),
	last_ckpt_need_commit(false),
	nref(0),
	current_type(replica::checkpoint::_get_type_info(0))
{
}

//
// Narrow, whenever possible, using the same proxy/C++ object pointer.
//
void *
multi_ckpt_handler::direct_narrow(CORBA::Object_ptr objp, CORBA::TypeId tid)
{
	ASSERT(objp != NULL);

	InterfaceDescriptor &idesc = *objp->_interface_descriptor();
	int inx = idesc.lookup_type_index(tid, true);
	return (inx < 0 ? NULL : idesc.cast(objp->_this_ptr(), (uint_t)inx));
}

//
// Return true if handler supports type to_tid.
//
inline bool
multi_ckpt_handler::local_narrow(CORBA::Object_ptr objp, CORBA::TypeId to_tid)
{
	return (objp->_interface_descriptor()->is_type_supported(to_tid));
}

void *
multi_ckpt_handler::narrow(CORBA::Object_ptr object_p, CORBA::TypeId to_tid,
    generic_proxy::ProxyCreator)
{
	ASSERT(nref > 0);
	void    *new_proxyp = direct_narrow(object_p, to_tid);
	if (new_proxyp != NULL) {
		incpxref(object_p);
	}
	return (new_proxyp);
}

void *
#ifdef DEBUG
multi_ckpt_handler::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId tid)
#else
multi_ckpt_handler::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));
	ASSERT(nref > 0);

	incpxref(object_p);

	return (object_p);
}


void
multi_ckpt_handler::release(CORBA::Object_ptr obj)
{
	lock();
	if (decpxref(obj) == 0) {
		SERV_DBG(("multi_ckpt_handler(%p): in release, nref from %d"
		    " to %d\n", this, nref, (nref - 1)));

		delete obj;
		ASSERT(nref > 0);
		if (--nref == 0) {
			//
			// We are through with the current version of handler,
			// so reinitialize the checkpoint type.
			//
			current_type = replica::checkpoint::_get_type_info(0);
		}
	}
	unlock();
}

//
// Create a new reference for the handler.
//
void
multi_ckpt_handler::new_reference()
{
	lock();
	SERV_DBG(("multi_ckpt_handler(%p): in new_reference, nref from %d"
	    " to %d\n", this, nref, (nref + 1)));
	nref++;
	ASSERT(nref != 0);
	unlock();
}

void
multi_ckpt_handler::marshal(service &, CORBA::Object_ptr)
{
	ASSERT(0);
}

void
multi_ckpt_handler::marshal_roll_back(CORBA::Object_ptr)
{
	ASSERT(0);
}

bool
multi_ckpt_handler::is_local()
{
	ASSERT(0);
	return (false);
}

bool
multi_ckpt_handler::is_user()
{
	ASSERT(0);
	return (false);
}

//
// This function is called when an HA service is going through an upgrade
// and is ready to start functioning as a newer versioned service.
// Here, we need to check that all of the secondaries support this newer
// interface, and then upgrade the least type.
//
// Return true if all of the secondaries support this and we have changed
// the least type, and false otherwise.
//
bool
multi_ckpt_handler::set_type(CORBA::type_info_t *tp)
{
	// Grab the lock to protect current type.
	lock();

	// Grab the lock to protect the secondaries list.
	rwlck.rdlock();

	replica::checkpoint_ptr		sec_p;

	//
	// Iterate through the list of secondaries to make sure that any
	// live secondaries support the new type.
	//
	SList<replica::checkpoint>::ListIterator	iter(secondaries);
	for (; (sec_p = iter.get_current()) != NULL; iter.advance()) {
		if (!sec_p->_supports_type(tp)) {

			//
			// We only want to reject the set_type call if the
			// secondary that doesn't support the type is alive.
			// Call the ckpt_noop method on the secondary,
			// and check for COMM_FAILURE.
			//
			Environment e;
			sec_p->ckpt_noop(e);
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				//
				// The secondary is dead, so we don't care
				// that it couldn't support the new type.
				// Just continue. The secondary will be removed
				// the next time invoke is called.
				//
				SERV_DBG(("multi_ckpt_handler(%p): checkpoint "
				    "%p doesn't support the interface, but it"
				    " is dead.\n", this, sec_p));
			} else {
				SERV_DBG(("multi_ckpt_handler(%p): checkpoint "
				    "%p doesn't support the interface\n", this,
				    sec_p));
				rwlck.unlock();
				unlock();
				return (false);
			}
		}
	}

	rwlck.unlock();

	// Update the current_type.
	current_type = tp;

	unlock();

	return (true);
}

//
// invoke
//
// Multicast invoke to all the secondaries. When it returns, all the
// minitransactions with lower sequence number are acked and this
// minitransaction is acked by all the live secondaries.
//
ExceptionStatus
multi_ckpt_handler::invoke(arg_info &ai, ArgValue *avp,
    InterfaceDescriptor *infp, Environment &e)
{
	e.check_used("unchecked environment used for new invocation");
	ASSERT(e.exception() == NULL);

	ckpt_handler_client::ckpt_handler_client_invoke	*invoke_argsp = new
	    ckpt_handler_client::ckpt_handler_client_invoke();
	//
	// Check that this method is supported by the proxy.
	// No need to lock for this check since the InterfaceDescriptor method
	// table doesn't change.
	//
	int inx = infp->lookup_type_index(ai.id, false);
	if (inx < 0) {
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		return (CORBA::LOCAL_EXCEPTION);
	}

	lock();
	//
	// Normally, the stub code will check that a method is allowed
	// based on the deep type. We lock here and check again since
	// we allow the deep type to be changed dynamically and the
	// proxy doesn't have its own lock.  Also, we want to fail early
	// rather than getting version exceptions on the individual
	// checkpoint interface invocations.
	//
	if (!((InterfaceDescriptor *)current_type)->is_type_supported(ai.id)) {
		unlock();
		SERV_DBG(("multi_ckpt_handler(%p)::invoke %s %x %x %x %x\n",
		    this, ((InterfaceDescriptor *)current_type)->get_name(),
		    ai.id[0], ai.id[1], ai.id[2], ai.id[3]));
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		return (CORBA::LOCAL_EXCEPTION);
	}

	fastlck.lock();

	// Obtain the current commit list for our exclusive use.
	invoke_argsp->cmt_list.concat(cmt_list);
	ASSERT(cmt_list.empty());

	// Obtain the current delete list for our exclusive use.
	invoke_argsp->dlt_list.concat(dlt_list);
	ASSERT(dlt_list.empty());

	fastlck.unlock();

	//
	// The commit list length remains unchanged for all secondaries.
	// The system determines the length by walking the list.
	//
	invoke_argsp->number_commits = invoke_argsp->cmt_list.count();

	//
	// The delete list length remains unchanged for all secondaries.
	// The system determines the length by walking the list.
	//
	invoke_argsp->number_deletes = invoke_argsp->dlt_list.count();

	//
	// Get the transaction context.
	//
	primary_ctx	*ctxp = primary_ctx::extract_from(e);
	ASSERT(ctxp != NULL);

	//
	// If this is a push checkpoint, and there is nothing to push, and
	// the last checkpoint we sent doesn't have any data in it, then
	// we just return.
	//
	if (ctxp->env_type == primary_ctx::PUSH_CKPT &&
	    invoke_argsp->number_commits == 0 &&
	    invoke_argsp->number_deletes == 0) {
		// There is nothing to send.
		if (last_ckpt_need_commit && ((max_seq - 1) == commit_seq)) {
			//
			// We need to send a commit and if we sent a checkpoint
			// now we will commit it. Send this checkpoint out.
			// Its only use is to commit the previous checkpoint
			// so we don't need commit for this one.
			//
			last_ckpt_need_commit = false;
		} else {
			//
			// No need to send this checkpoint so just return.
			// Note we haven't done any marshalling yet so
			// there is no need for cleanup.
			//
			unlock();
			delete invoke_argsp;
			return (CORBA::NO_EXCEPTION);
		}
	} else {
		last_ckpt_need_commit = true;
	}
	//
	// We have taken a snapshot of the commit list. In order to
	// ensure that any commits are delivered and processed before the next
	// checkpoint, we must also claim the next sequence number.
	//

	// get the sequence number for the checkpoint
	replica::ckpt_seq_t my_seq = max_seq++;

	// Assert that wrap around never happen.
	CL_PANIC(max_seq != (min_seq - 1));

	// Used by our flow control.
	replica::ckpt_seq_t current_commit_seq = commit_seq;

	//
	// rwlock is used to protect secondary list. We need to get this lock
	// before dropping the mutex lock so that there will be nobody in
	// between locks.
	//
	rwlck.rdlock();
	unlock();

	// If we have secondaries, we should send commit later.
	if (!secondaries.empty()) {
		if (ctxp != NULL &&
		    ctxp->env_type == primary_ctx::MINI_TRANSACTION) {
			if (CORBA::is_nil(ctxp->get_trans_id())) {
				//
				// This is a new local invocation. Create the
				// client_ctx.
				//
				ctxp->add_client_ctx(e);
			}
			ctxp->needcommit_yes();
		}
	}

	// The list to store dead secondaries.
	SList<replica::checkpoint>	*dead_secondariesp =
	    new SList<replica::checkpoint>;

	replica::checkpoint_ptr		secondary_p;

	//
	// Transmit the checkpoint to each of the secondaries sequentially.
	//
	SList<replica::checkpoint>::ListIterator	iter(secondaries);
	CORBA::Exception *exp;
	for (; (secondary_p = iter.get_current()) != NULL; iter.advance()) {
		ASSERT(secondary_p->_handler() != NULL);

		(void) ((ckpt_handler_client *)secondary_p->_handler())
		    ->invoke(ai, avp, secondary_p->_deep_type(), my_seq,
			commit_seq, invoke_argsp, e);
		exp = e.exception();
		if (exp) {
			if (!CORBA::VERSION::_exnarrow(exp) &&
			    !CORBA::COMM_FAILURE::_exnarrow(exp)) {
				//
				// SCMSGS
				// @explanation
				// An unexpected return value was encountered
				// when performing an internal operation.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available.
				//
				(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC,
				    MESSAGE,
				    "HA: exception %s (major=%d) sending "
				    "checkpoint.", exp->_name(), exp->_major());
			} else {
				//
				// We either got a COMM_FAILURE or
				// VERSION exception. There is a
				// small window after a set_type call
				// when a "old" secondary might be on
				// the multicast list. If so, the
				// secondary must be dead.
				//
				dead_secondariesp->append(replica::checkpoint::
				    _duplicate(secondary_p));

				e.clear();
			}
		}
	}

	rwlck.unlock();

	//
	// Delete the dead secondaries from the multicast group.
	// Note that we duplicate the dead checkpoint interface when invoke
	// fails and release it here. This is to deal with the case that
	// several threads notice that the secondary died and try to delete
	// it from the multicast group at the same time.
	//
	while (is_not_nil(secondary_p = dead_secondariesp->reapfirst())) {
		release_ref(secondary_p);
		CORBA::release(secondary_p);
	}

	delete dead_secondariesp;
	dead_secondariesp = NULL;

	// Empty the commit list properly before it gets deleted.
	replica::transaction_id_ptr	trid_p;
	while (is_not_nil(trid_p = invoke_argsp->cmt_list.reapfirst())) {
		CORBA::release(trid_p);
	}

	delete_elem	*elemp;
	while ((elemp = invoke_argsp->dlt_list.reapfirst()) != NULL) {
		delete (elemp);
	}

	delete invoke_argsp;
	invoke_argsp = NULL;

	//
	// If there is a checkpoint with smaller sequence number not
	// acked yet, we put our thread into sleep on sleeping. Each
	// time a checkpoint is acked, we check whether there is any
	// other thread waiting in the sleep list.
	//
	lock();
	if (my_seq != min_seq) {
		wait_for_ckpt_ack(my_seq);
	}

	commit_seq = my_seq;
	min_seq++;

	// Assert that wrap around never happen.
	CL_PANIC(max_seq != (min_seq - 1));

	sleep_elem					*slpelemp;
	IntrList<sleep_elem, _DList>::ListIterator	iter2(sleeping);

	for (; (slpelemp = iter2.get_current()) != NULL; iter2.advance()) {
		// Wake up the thread with the next sequence number.
		if (slpelemp->seq == min_seq) {
			slpelemp->cv.signal();
			break;
		}
	}

	//
	// Run the flow control once every flush_range checkpoints.
	//

	flush_to_secondaries(current_commit_seq - flush_range);

	unlock();

	// We always neglect exceptions in this invoke.
	return (CORBA::NO_EXCEPTION);
}

void
multi_ckpt_handler::wait_for_ckpt_ack(replica::ckpt_seq_t seq)
{
	sleep_elem	slpelem(seq);

	sleeping.append(&slpelem);
	while (seq != min_seq) {
		slpelem.cv.wait(&lck);
	}
	(void) sleeping.erase(&slpelem);
}

//
// flush_to_secondaries
//
// Flow control. Wait until all the secondaries have processed
// the checkpoints up to (current_commit_seq - flush_range).
// Most of the time this should return immediately since all the
// secondaries should already processed the checkpoints.
//
void
multi_ckpt_handler::flush_to_secondaries(replica::ckpt_seq_t flush_to_seq)
{
	replica::checkpoint_ptr				secondary_p;
	SList<replica::checkpoint>::ListIterator	iter(secondaries);
	//
	// The sequence that we know will be smaller than all numbers involved
	// here: (min_seq - ckpt_range)
	//

	// We only flush once every flush_range checkpoints.
	ckpt_info	flush_info(min_seq - ckpt_range,
	    flush_to_seq - flush_range);

	if (flush_info >= last_flush_seq) {
		Environment *envp = new Environment;
		CORBA::Exception *exp;
		// Update last_flush_seq and do the flushing.
		last_flush_seq = flush_to_seq;
		rwlck.rdlock();
		iter.reinit(secondaries);
		for (; (secondary_p = iter.get_current()) != NULL;
		    iter.advance()) {
			secondary_p->flush_to(flush_to_seq, *envp);
			exp = envp->exception();
			if (exp) {
				if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
					envp->clear();
				} else {
					(void) orb_syslog_msgp->log(
					    SC_SYSLOG_PANIC, MESSAGE,
					    "HA: exception %s (major=%d) "
					    "from flush_to().", exp->_name(),
					    exp->_major());
				}
			}
		}
		rwlck.unlock();
		delete envp;
	}
}

//
// Release one underlying reference. If the secondary we are removing
// is alive we need to tell it what sequence number it should process
// checkpoints to. This allows the secondary to shutdown cleanly.
//
void
multi_ckpt_handler::release_ref(replica::checkpoint_ptr ckpt)
{
	replica::checkpoint_ptr a;
	Environment e;

	//
	// Obtaining both locks ensures us that all the checkpoints up to
	// max_seq-1 are sent out to all the secondaries.
	//
	lock();
	// rwlock protects the secondary list.
	rwlck.wrlock();
	//
	// This secondary is being removed. So mark it to stop
	// receiving checkpoints. But we need to flush any checkpoints
	// on the wire.
	//
	ckpt->process_to(max_seq - 1, e);
	CORBA::Exception *exp = e.exception();
	if (exp) {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			e.clear();
		} else {
			//
			// SCMSGS
			// @explanation
			// An unexpected return value was encountered when
			// performing an internal operation.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "HA: exception %s (major=%d) from process_to().",
			    exp->_name(), exp->_major());
		}
	}
	//
	// set the flag on this secondary to indicate that the primary
	// stops sending ckpt messages to it.
	//
	ckpt->stop_receiving_ckpt(e);
	exp = e.exception();
	if (exp) {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			e.clear();
		} else {
			//
			// SCMSGS
			// @explanation
			// An unexpected return value was encountered when
			// performing an internal operation.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "HA: exception %s (major=%d) from "
			    "stop_receiving_ckpt().", exp->_name(),
			    exp->_major());
		}
	}

	unlock();

	// It's ok if the secondary is not found, since it could have been
	// released by others.
	SList<replica::checkpoint>::ListIterator iter(secondaries);
	for (; (a = iter.get_current()) != NULL; iter.advance()) {
		if (a->_equiv(ckpt)) {
			(void) secondaries.erase(a);
			CORBA::release(a);
			break;
		}
	}
	rwlck.unlock();
}


//
// Release all the underlying references.
//
// This can be called either by become_secondary and shutdown.
//
void
multi_ckpt_handler::release_refs(bool is_shutdown)
{
	replica::checkpoint_ptr sec_ckpt_p;
	Environment e;
	CORBA::Exception *exp;

	lock();
	// When this is called the service is frozen.
	CL_PANIC(commit_seq == max_seq - 1);
	rwlck.wrlock();
	while ((sec_ckpt_p = secondaries.reapfirst()) != NULL) {
		if (is_shutdown) {
			sec_ckpt_p->stop_receiving_ckpt(e);
			exp = e.exception();
			if (exp) {
				if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
					e.clear();
					CORBA::release(sec_ckpt_p);
					continue;
				} else {
					(void) orb_syslog_msgp->log(
					    SC_SYSLOG_PANIC, MESSAGE,
					    "HA: exception %s (major=%d) "
					    "from stop_receiving_ckpt().",
					    exp->_name(), exp->_major());
				}
			}
		}
		//
		// This is called by become_secondary when we are
		// converting a primary to a secondary. So all the
		// secondaries in the secondaries list are still valid
		// and will be receiving ckpt messages from the new
		// primary. Also called when becoming spare and
		// shutting down to make sure all checkpoints sent to
		// this secondary are processed. This is necessary to
		// make sure there are'nt any checkpoints in the orb
		// or transport layers which may need to be delivered
		// to this secondary.
		//
		sec_ckpt_p->process_to(commit_seq, e);
		exp = e.exception();
		if (exp) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				e.clear();
				CORBA::release(sec_ckpt_p);
			} else {
				(void) orb_syslog_msgp->log(
				    SC_SYSLOG_PANIC, MESSAGE,
				    "HA: exception %s (major=%d) "
				    "from process_to().", exp->_name(),
				    exp->_major());
			}
		}
	}
	rwlck.unlock();
	unlock();
}

// Add a new checkpoint reference to the multicast group.
void
multi_ckpt_handler::add_ref(replica::checkpoint_ptr ckpt)
{
	ASSERT(ckpt != NULL);
	ASSERT(ckpt->_supports_type(current_type));
	//
	// XXX we may not need the rwlock at all if the framework
	// guarantees that when add_ref is called, there won't be
	// any checkpointing in process. In which case we shouldn't
	// need to lock the mutex when we do set_min above.
	//
	replica::checkpoint_ptr ckpt_dup =
		replica::checkpoint::_duplicate(ckpt);
	rwlck.wrlock();
	secondaries.append(ckpt_dup);

	// Inform the secondary that the primary will start sending ckpts to it.
	Environment	e;
	CORBA::Exception *exp;

	ckpt->start_receiving_ckpt(e);
	exp = e.exception();
	if (exp) {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			e.clear();
		} else {
			//
			// SCMSGS
			// @explanation
			// An unexpected return value was encountered when
			// performing an internal operation.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(
			    SC_SYSLOG_PANIC, MESSAGE,
			    "HA: exception %s (major=%d) from "
			    "start_receiving_ckpt().", exp->_name(),
			    exp->_major());
		}
	}
	rwlck.unlock();
}

//
// become_secondary
//
// When doing a switchover, the old primary needs to synchronize with
// the old secondaries before it declares itself as a secondary. This should
// be called when the service is frozen.
//
void
multi_ckpt_handler::become_secondary()
{
	Environment e;

	//
	// If there are no secondaries then there is nothing to do.
	// We don't need to hold the rwlck here because we know when this
	// is called, the primary is frozen and there is no one accessing
	// the secondary list.
	//
	if (secondaries.empty()) {
		return;
	}
	//
	// When this is called, the service should be frozen, which means we
	// can't be waiting for any replies.
	//
	ASSERT(sleeping.empty());

	// Set the parameters for the ckpt_handler.
	my_ckpt_handler_svr->initialize_seq(commit_seq);

	// become_secondary is always called after freeze_primary, in which
	// we pushed out a checkpoint that carries all the commits and
	// deletes to the secondary. So by this time there shouldn't be
	// any.
	ASSERT(cmt_list.empty());
	ASSERT(dlt_list.empty());

	// Throw away our secondary list. Note that this will send proper
	// information to the secondaries so that they know where to
	// process their checkpoints to.
	release_refs(false);
}

//
// check_secondary
// Checks to see whether the given secondary can support the interface
// version that is currently being used by the service.
//
// Returns true if the secondary is valid, false otherwise.
//
bool
multi_ckpt_handler::check_secondary(replica::checkpoint_ptr sec)
{
	ASSERT(current_type != NULL);

	bool retv = false;

	lock();

	if (!sec->_supports_type(current_type)) {
		SERV_DBG(("multi_ckpt_handler(%p): ckpt %p doesn't support"
		    " the interface\n", this, sec));
		retv = false;
	} else {
		retv = true;
	}

	unlock();

	return (retv);
}

void
multi_ckpt_handler::add_secondary(replica::checkpoint_ptr sec, Environment &e)
{
	//
	// We are transitioning from dumping state to sending regular
	// checkpoints. Tell the secondary to process all the dump messages.
	//
	sec->flush_to(((ckpt_handler_client *)(sec->_handler()))->
	    get_last_dump_seq(), e);

	CORBA::Exception *exp = e.exception();

	if (exp) {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			e.clear();
			return;
		} else {
			(void) orb_syslog_msgp->log(
			    SC_SYSLOG_PANIC, MESSAGE,
			    "HA: exception %s (major=%d) from flush_to().",
			    exp->_name(), exp->_major());
		}
	}
	//
	// Initialize the ckpt_handler on the new secondary.
	// When we set the minimum sequence num of the new secondary,
	// we only need to make sure that the min we give it is the
	// smallest one it will ever see in the future.
	//
	lock();
	sec->initialize_seq(commit_seq, e);
	unlock();
	exp = e.exception();

	if (!exp) {
		// Add to the multicast group.
		// check_secondary must have been called before this.
		add_ref(sec);
	} else {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			e.clear();
			return;
		} else {
			//
			// SCMSGS
			// @explanation
			// An unexpected return value was encountered when
			// performing an internal operation.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(
			    SC_SYSLOG_PANIC, MESSAGE,
			    "HA: exception %s (major=%d) from "
			    "initialize_seq().",
			    exp->_name(), exp->_major());
		}
	}
}

//
// become_primary
//
// Synchronize among the multicast group. The primary could fail during
// the function, but that doesn't cause any problem. If primary fails
// during step1, apparently we are ok. If primary fails during step2,
// some secondary may already received process_to message and others not.
// However, that doesn't cause any problem since when the next new primary
// comes up, it will again find the correct seq to process_to and ask
// the rest of secondaries to do it. Note here that do process_to twice
// doesn't cause any trouble.
//
void
multi_ckpt_handler::become_primary(const replica::secondary_seq &secs)
{
	replica::ckpt_seq_t high_seq;
	replica::ckpt_seq_t current_min_seq;
	replica::checkpoint_ptr a;
	Environment e;

	lock();

	// Before anything else, initialize the current_type to replica::ckpt.
	current_type = replica::checkpoint::_get_type_info(0);

	unlock();

	// step1, set up the new multicast group.
	ASSERT(secondaries.empty());
	for (uint_t i = 0; i < secs.length(); i++) {
		add_ref(secs[i]);
		// Add ref should never fail here, since this is a
		// failover/switchover.
	}

	// step2, find the smallest of all the high seq in the multicast group
	high_seq = my_ckpt_handler_svr->get_high_seq();
	current_min_seq = my_ckpt_handler_svr->get_min_seq();
	CORBA::Exception *exp;

	rwlck.rdlock();
	SList<replica::checkpoint>::ListIterator iter(secondaries);
	for (; (a = iter.get_current()) != NULL; iter.advance()) {
		replica::ckpt_seq_t seq;
		a->get_high(seq, e);
		SERV_DBG(("%p sec=%p, get_high_seq=%u\n", this, a, seq));

		//
		// If there is an exception, we didn't get any valid info.
		// This method can be called during a node reconfiguration,
		// and a secondary may have gone away.
		//
		exp = e.exception();
		if (exp) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				e.clear();
				continue;
			} else {
				//
				// SCMSGS
				// @explanation
				// An unexpected return value was encountered
				// when performing an internal operation.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available.
				//
				(void) orb_syslog_msgp->log(
				    SC_SYSLOG_PANIC, MESSAGE,
				    "HA: exception %s (major=%d) from "
				    "get_high().", exp->_name(), exp->_major());
			}
		}

		ckpt_info high_info(current_min_seq, high_seq);
		if (high_info > seq)
			high_seq = seq;
	}

	// step3, tell the multicast group to process their checkpoints.
	// Note that in process_to, the min and commit seq will
	// be set correctly so there is no need to sync them.
	my_ckpt_handler_svr->process_to(high_seq);

	// Reinitialize the iterator
	iter.reinit(secondaries);

	for (; (a = iter.get_current()) != NULL; iter.advance()) {
		a->process_to(high_seq, e);
		exp = e.exception();
		if (exp) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				e.clear();
			} else {
				(void) orb_syslog_msgp->log(
				    SC_SYSLOG_PANIC, MESSAGE,
				    "HA: exception %s (major=%d) from "
				    "process_to().", exp->_name(),
				    exp->_major());
			}
		}
	}

	// Wait till all the checkpoints are processed before proceeding.
	my_ckpt_handler_svr->quiesce_ckpt_threadpool();

	// Set the parameters for the multi_ckpt_handler.
	min_seq = max_seq = high_seq + 1;
	commit_seq = high_seq;
	last_flush_seq = commit_seq;

	rwlck.unlock();
}

void
multi_ckpt_handler::become_spare()
{
	my_ckpt_handler_svr->become_spare();
}

void
multi_ckpt_handler::shutdown()
{
	ASSERT(cmt_list.empty());

	// There shouldn't be any active checkpointing going on.
	ASSERT(sleeping.empty());
	ASSERT(commit_seq == max_seq - 1);

	// Get rid of the info about secondaries.
	release_refs(true);
}

// Add the transaction_id contained in the environment into the commit list
void
multi_ckpt_handler::add_commit(replica::transaction_id_ptr t)
{
	ASSERT(is_not_nil(t));
	fastlck.lock();
	cmt_list.append(replica::transaction_id::_duplicate(t));
	fastlck.unlock();
}

// Add the serviceid and objectid to the delete list.
void
multi_ckpt_handler::add_delete(replica::service_num_t sid,
    xdoor_id oid)
{
	delete_elem *elemp = new delete_elem(sid, oid);
	fastlck.lock();
	dlt_list.append(elemp);
	fastlck.unlock();
}

multi_ckpt_handler::~multi_ckpt_handler()
{
	ASSERT(secondaries.empty());
	ASSERT(sleeping.empty());
	ASSERT(cmt_list.empty());
	ASSERT(nref == 0);
	dlt_list.dispose();
	ASSERT(dlt_list.empty());
	my_ckpt_handler_svr = NULL;
	state_hash = NULL;
	current_type = NULL;
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <repl/service/multi_ckpt_handler_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
