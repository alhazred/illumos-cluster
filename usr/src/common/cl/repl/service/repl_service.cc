//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_service.cc	1.150	08/11/24 SMI"

#include <sys/threadpool.h>
#include <repl/service/repl_service.h>
#include <repl/service/replica_handler.h>
#ifdef	_KERNEL_ORB
#include <repl/service/periodic_thread.h>
#include <repl/rmm/rmm.h>
#include <orb/infrastructure/orb.h>
#endif
#include <repl/service/ha_tid_factory.h>
#include <sys/rm_util.h>
#include <orb/fault/fault_injection.h>
#include <orb/refs/unref_threadpool.h>

#ifdef	SERV_DBGBUF
uint32_t service_dbg_size = 10000;
dbg_print_buf service_dbg(service_dbg_size);
#endif

repl_service_manager	*repl_service_manager::the_repl_service_manager = NULL;

//
// Add a new handler to the service. Either because it has
// been unmarshaled or it has been created by the primary.
//
void
repl_service::add_handler()
{
	lock();
	add_handler_locked();
	unlock();
}

//
// Remove a handler from the service.  This may cause the service to go away.
//
void
repl_service::rem_handler()
{
	lock();
	ASSERT(_num_handlers > 0);
	_num_handlers--;
	if (_num_handlers == 0) {
#ifdef _KERNEL_ORB
		if (_repl_provp != NULL) {
			// try_cleanup will drop the lock.
			_repl_provp->try_cleanup();
		} else {
			// try_delete will drop the lock.
			try_delete();
		}
#else
		try_delete();
#endif
		return;
	}
	unlock();
}

//
// Returns true if the repl_service is successfully deleted.
// It always returns with the repl_service lock freed.
//
void
repl_service::try_delete()
{
	ASSERT(lock_held());
	if (verify_delete()) {
		//
		// We must drop the service lock before getting the
		// service manager's lock.
		//
		unlock();
		repl_service_manager::the().remove_service(_service_id);
	} else {
		unlock();
	}
}

//
// Verify that we can delete the repl_service.
//
bool
repl_service::verify_delete()
{
	ASSERT(lock_held());
#ifdef _KERNEL_ORB
	if (_repl_provp != NULL) {
		return (false);
	}
#endif
	return (_num_handlers == 0);
}

//
// Called by the handler to see if it should perform an invocation
// in this domain.  Returns true of a local invocation has been
// permitted - i.e. this domain contains an active primary.
// If it returns true then the caller must later call invo_done
// when it is through with the invocation.
//
bool
repl_service::try_local_invo(Environment *e)
{
	bool	freezing = false;

	if (!invo_fl.try_hold()) {
		// We are a primary that is either freezing or frozen.
		if (freezing_fl.try_hold()) {
			// We are a freezing primary.
			freezing = true;
		} else {
			//
			// We are a frozen primary. Wait for the unfreeze.
			// Note that we may no longer be primary at the time we
			// are unfrozen.
			// This use of the lock can't fail, so ignore return
			// value.
			//
			(void) invo_fl.hold();
		}
	}

	if (_is_primary) {
		//
		// FAULT POINT
		// This generic fault point will allow us to inject a fault
		// during a local invocation.
		//
		FAULTPT_HA_FRMWK(
			FAULTNUM_HA_FRMWK_REPL_SERVICE_TRY_LOCAL_INVO,
			FaultFunctions::generic);

		if (is_shutting_down()) {
			//
			// The service has already been shut down, force it to
			// try remote invo.
			//
			invo_fl.done();

			// The primary can't be in freezing state here.
			CL_PANIC(!freezing);

			return (false);
		} else {
			if (freezing) {
				//
				// Note that we hold the freeze_fl
				// lock in the environment so that we
				// know to release that rather than
				// invo_fl in invo_done().
				//
				e->set_freezing();
			}
			return (true);
		}
	} else {
		if (freezing) {
			freezing_fl.done();
		} else {
			invo_fl.done();
		}
		return (false);
	}
}

//
// Called by the handler when it receives an invocation from another domain.
// Returns true if the invocation can proceed and false if the service is
// frozen. This function does not block because we do not want to tie up
// a server thread on the freezing primary.
//
bool
repl_service::try_incoming_invo(Environment *e)
{
	ASSERT(_is_primary);

	if (invo_fl.try_hold()) {
		return (true);
	} else {
		// We are either freezing or frozen.
		if (freezing_fl.try_hold()) {
			//
			// We are freezing.
			// Note that fact that we hold the freeze_fl lock in
			// the environment so that we know to release that
			// rather than invo_fl in invo_done().
			//
			e->set_freezing();
			return (true);
		} else {
			// We are frozen. Fail the attempt.
			return (false);
		}
	}
}

#ifdef _KERNEL_ORB

// This happens when a repl_prov is brought on line in this domain.  There
// can only be one such repl_prov in the domain at any time.
void
repl_service::add_repl_prov(generic_repl_prov *rp)
{
	ASSERT(lock_held());
	ASSERT(_repl_provp == NULL);
	_repl_provp = rp;
}

//
// Remove a repl_prov from the service. This happens when a repl_prov is
// shutdown in this domain.  This may cause the service to go away.
//
void
repl_service::rem_repl_prov()
{
	ASSERT(lock_held());
	ASSERT(_repl_provp != NULL);
	_repl_provp = NULL;

	// try_delete will drop the lock.
	try_delete();
}

//
// We have a new repl_prov for service sid in this domain.
// Create the service if necessary (there may or may not be references
// to the service in the domain already).
// This is one of the two ways that a new service can be created.
//
// If the repl_service with the specified sid already has a repl_prov
// it returns NULL. Otherwise return the pointer to the repl_service.
//
repl_service *
repl_service::add_new_repl_prov(replica::service_num_t sid,
    generic_repl_prov *rp)
{
	repl_service	*rsp;
	repl_service	*result;

	rsp = repl_service_manager::the().lookup_hold(sid, true);
	if (rsp->_repl_provp != NULL) {
		result = NULL;
	} else {
		result = rsp;
		rsp->add_repl_prov(rp);
		rp->set_service(rsp);
	}
	rsp->unlock();
	return (result);
}

//
// Is a freeze_primary() in progress? Called by implementation methods.
//
bool
repl_service::freeze_in_progress(Environment *e)
{
	if (invo_fl.blocked()) {
		e->system_exception(
		    CORBA::PRIMARY_FROZEN(0, CORBA::COMPLETED_NO));
		return (true);
	}
	return (false);
}

// generic_repl_prov IDL methods

//
// Called by the RM when it wants to convert this primary repl_prov to a
// secondary.
// primary_has_failed, means that this provider already returned a
// repl_prov_failed exception from an attempt to make it primary. We don't
// forward the become_secondary call to the provider, since it should have
// stayed in secondary state. We still need to adjust the infrastructure.
//
void
generic_repl_prov::become_secondary(bool primary_has_failed, Environment &e)
{
	// FAULT_POINT
	FAULTPT_HA_FRMWK_THIS_SVC(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SECONDARY, _service_id,
	    FaultFunctions::generic);

	SERV_DBG(("%s.%s(%d): become_secondary\n", _service_name,
	    _provider_name, _service_id));
	REPL_DBG(("become secondary\n"));
	ASSERT(_servicep->invos_frozen());

	// Remove the push ckpt task.
	periodic_thread::the().remove_job(&push_task);

	// Push current commits out.
	push_task.execute();

	if (!primary_has_failed) {
		//
		// become_secondary does not result in checkpoints.
		// In the future if checkpoints ever occur for become_secondary,
		// then will need a primary_ctx.
		//
		_actual_implp->become_secondary(e);
		ASSERT(e.sys_exception() == NULL);
	}

	// This will synchronize among the old secondaries if this is a
	// switch over.
	_ckpt_handler.become_secondary();

	_state_hash.become_secondary();

	// Throws away all the unreferenced queued up in _service_unref.
	_service_unref.become_secondary();

	ASSERT(e.trans_ctxp == NULL);
	primary_ctx	ctx(NULL, primary_ctx::BECOME_SECONDARY_CKPT, e);

	// Tell the repl_service we are no longer primary.
	_servicep->became_secondary();

	e.trans_ctxp = NULL;

	// Allow invocations to wake up and go to the new primary
	_servicep->unfreeze_invos();

	//
	// Release our reference to the multi_ckpt_handler. The implementation
	// will also release its reference as part of the become_secondary call
	// so then the multi_ckpt_handler will get _unreferenced.
	//
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

//
// Called when this node is being made a secondary from a spare.
//
void
generic_repl_prov::commit_secondary(Environment &)
{
	// FAULT_POINT
	FAULTPT_HA_FRMWK_THIS_SVC(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_COMMIT_SECONDARY, _service_id,
	    FaultFunctions::generic);

	// This replica is becoming a secondary from a spare, we need to
	// wait for old transaction states to clean out.
	_state_hash.wait_till_empty();
	_state_hash.become_secondary();
}

//
// Add a new secondary to the service.
// The RM must freeze the service before making this call.
//
void
generic_repl_prov::add_secondary(replica::checkpoint_ptr secondary_p,
    const char *secondary_name, Environment &e)
{
	// FAULT_POINT
	FAULTPT_HA_FRMWK_THIS_SVC(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_ADD_SECONDARY, _service_id,
	    FaultFunctions::generic);

	SERV_DBG(("%s.%s(%d): add_secondary\n", _service_name, _provider_name,
	    _service_id));

	// Remove the push ckpt task.
	periodic_thread::the().remove_job(&push_task);

	// Push current commits out.
	push_task.execute();


	//
	// Check to make sure that this secondary can support the
	// version that the service is currently running.
	// XXX Possible race condition: Can the least type of the handler
	// change between this check and the add_secondary?
	//
	if (!_ckpt_handler.check_secondary(secondary_p)) {
		e.exception(new replica::invalid_version);

		// Restart the push ckpt task.
		periodic_thread::the().add_job(&push_task);

		return;
	}

	//
	// Tell the ckpt_handler_client about the object reference above it.
	// It's needed when the ckpt_handler_client does flow control for the
	// dumpstate messages. It also resets the sequence numbers inside
	// the handler.
	//
	((ckpt_handler_client *)(secondary_p->_handler()))->init(secondary_p);

	ASSERT(e.trans_ctxp == NULL);

	//
	// The addition of a primary context
	// is temporary because some services do checkpoints
	// on this operation even though this is not officially supported.
	//
	primary_ctx	ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, e);

	_actual_implp->add_secondary(secondary_p, secondary_name, e);
	ASSERT(e.sys_exception() == NULL);
	if (e.exception()) {
		e.exception()->print_exception("exception adding secondary\n");
		//
		// SCMSGS
		// @explanation
		// A failure occurred while attempting to add a secondary
		// provider for an HA service.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: exception adding secondary");
	} else {
		_ckpt_handler.add_secondary(secondary_p, e);
	}
	e.trans_ctxp = NULL;

	// Periodically run the push ckpt task.
	periodic_thread::the().add_job(&push_task);
}

//
// Remove a secondary from the service.
// The RM must have frozen the service before making this call.
//
void
generic_repl_prov::remove_secondary(replica::checkpoint_ptr secondary_p,
    const char *secondary_name, Environment &e)
{
	// FAULT_POINT
	FAULTPT_HA_FRMWK_THIS_SVC(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY_2, _service_id,
	    FaultFunctions::generic);

	// FAULT_POINT
	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY,
	    FaultFunctions::generic);

	SERV_DBG(("%s.%s(%d): remove_secondary %p\n", _service_name,
	    _provider_name, _service_id, secondary_p));

	_ckpt_handler.release_ref(secondary_p);

	ASSERT(e.trans_ctxp == NULL);

	//
	// The addition of a primary context
	// is temporary because some services do checkpoints
	// on this operation even though this is not officially supported.
	//
	primary_ctx	ctx(NULL, primary_ctx::REMOVE_SECONDARY_CKPT, e);

	_actual_implp->remove_secondary(secondary_name, e);
	CL_PANIC(e.exception() == NULL);

	e.trans_ctxp = NULL;
}

//
// Called on the primary when the RM wants to freeze the service.
// The generic repl_prov implements this.
//
void
generic_repl_prov::freeze_primary(Environment &e)
{
	SERV_DBG(("%s.%s(%d): freeze_primary\n", _service_name,
	    _provider_name, _service_id));

	REPL_DBG(("freeze_primary\n"));

	//
	// Inform the implementation object to prepare for the upcoming
	// freeze_primary request.
	//
	_actual_implp->freeze_primary_prepare(e);
	CL_PANIC(e.exception() == NULL);

	// Start to perform the actual freeze on the primary.

	// First wait till all transaction states are gone.
	_state_hash.wait_till_empty();

	//
	// Start freezing invocations. After this, invocations which
	// ask if the service is freezing will be told that it is.
	//
	_servicep->begin_freezing();

	//
	// Tell the implementation to unblock any long lived invocations
	// and to make sure that that no more invocations depend on
	// another invocation or an unreferenced coming in.
	//
	_actual_implp->freeze_primary(e);
	CL_PANIC(e.exception() == NULL);

	// Wait for invocations in progress to drain out.
	_servicep->freeze_invos();

	//
	// Set a flag so that unreferenced will be deferred.
	// Wait until there is no more unreferenced going on in the service.
	// Note that at this point the state hashtable is empty (guaranteed
	// by freeze_primary_prepare), so we don't need to worry about
	// all_states_gone() reenabling the unref.
	//
	// References must not be given out once unreferences have been
	// deferred. So this must occur after invocations have been frozen.
	//
	_service_unref.freeze_primary();
}

//
// Called on the primary when the RM wants to unfreeze the service.
// The generic repl_prov implements this.
//
void
generic_repl_prov::unfreeze_primary(Environment &e)
{
	REPL_DBG(("unfreeze_primary\n"));

	_actual_implp->unfreeze_primary(e);
	CL_PANIC(e.exception() == NULL);

	// This will take care of the orphaned and completed states that
	// are not going to receive commits from anybody.
	_state_hash.become_primary();

	//
	// Deliver unreferenced to the handlers in defer_list if there are
	// no more state objects.
	//
	if (_state_hash.empty()) {
		_service_unref.enable_unreferenced();
	}

	// Allows invocations and unreferenced to come in.
	_servicep->unfreeze_invos();
}

//
// Called by the RM when it wants to make this repl_prov the primary.
// We check if it is a valid operation and forward the operation to the
// implementation.
//
void
generic_repl_prov::become_primary(
    const replica::repl_name_seq &secondary_names, Environment &e)
{
	// FAULT_POINT
	FAULTPT_HA_FRMWK_THIS_SVC(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_PRIMARY, _service_id,
	    FaultFunctions::generic);

	SERV_DBG(("%s.%s(%d): become_primary %p.%p\n",
	    _service_name, _provider_name, _service_id, this, _servicep));
	REPL_DBG(("become_primary\n"));
	ASSERT(_servicep->invos_frozen());

#ifdef DEBUG
	_service_unref.become_primary();
#endif
	_servicep->became_primary();

	ASSERT(e.trans_ctxp == NULL);
	primary_ctx	ctx(NULL, primary_ctx::BECOME_PRIMARY_CKPT, e);
	_actual_implp->become_primary(secondary_names, e);
	ASSERT(_ckpt_proxy != NULL);
	e.trans_ctxp = NULL;
	ASSERT(e.sys_exception() == NULL);

	// Periodically run the push ckpt task.
	periodic_thread::the().add_job(&push_task);
}

//
// Called by the RM when it wants to make this repl_prov the primary.
// We clean up any work that we have left as a secondary. We figure out
// how many checkpoints we need to process and process them.
// When we leave we are a frozen primary, from the perspective of local
// invocations and unreferenced (almost - we still need to
// call become_primary.
//
void
generic_repl_prov::become_primary_prepare(
    const replica::secondary_seq &secs, Environment &)
{
	SERV_DBG(("%s.%s(%d): become_primary_prepare %p.%p\n",
	    _service_name, _provider_name, _service_id, this, _servicep));

	_servicep->freeze_invos(); // Force new local invos to block.
	_ckpt_handler.become_primary(secs);
}

//
// Called by the RM when it wants to convert this secondary repl_prov to a
// spare.
// This may cause the repl_service to go away
//
void
generic_repl_prov::become_spare(Environment &e)
{
	// FAULT_POINT
	FAULTPT_HA_FRMWK_THIS_SVC(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE_2, _service_id,
	    FaultFunctions::generic);

	// FAULT_POINT
	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE,
	    FaultFunctions::generic);

	SERV_DBG(("%s.%s(%d): become_spare\n", _service_name, _provider_name,
	    _service_id));

	// This will process all checkpoints.
	_ckpt_handler.become_spare();

	// Deletes unreferenced transaction states from the hash table.
	_state_hash.become_spare();

	// Deliver become_spare to the service. The service should delete all
	// their ha objects in this step. There shouldn't be any exceptions.
	_actual_implp->become_spare(e);
	ASSERT(e.exception() == NULL);
}

CORBA::Object_ptr
generic_repl_prov::get_root_obj(Environment &e)
{
	SERV_DBG(("%s.%s(%d): get_root_obj\n", _service_name, _provider_name,
	    _service_id));

	CORBA::Object_ptr objptr = _actual_implp->get_root_obj(e);
	ASSERT(e.sys_exception() == NULL);
	if (e.exception()) {
		ASSERT(CORBA::is_nil(objptr));
		return (CORBA::Object::_nil());
	}
	return (objptr);
}

//
// Called by the RM when it wants to shutdown this replica.
// Returns a Solaris error code.
//
uint32_t
generic_repl_prov::shutdown(replica::repl_prov_shutdown_type shutdown_type,
    Environment &e)
{
	SERV_DBG(("%s.%s(%d): shutdown(type %d)\n", _service_name,
	    _provider_name, _service_id, shutdown_type));

	bool is_forced_shutdown = (shutdown_type ==
	    replica::FORCED_SERVICE_SHUTDOWN);

	// FAULT_POINT
	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_SHUTDOWN,
	    FaultFunctions::generic);

	ASSERT(e.exception() == NULL);
	_state_hash.wait_till_empty();

	if (_servicep->is_primary()) {
		if (is_forced_shutdown) {
			uint32_t err_code = _actual_implp->forced_shutdown(e);
			if (err_code != 0) {
				return (err_code);
			}
			//
			// This is a forced shutdown. We should not call
			// _ckpt_handler.shutdown() until we know for sure
			// that there are no more clients references to the
			// service. This is because _ckpt_handler.shutdown()
			// assumes that there are no more active ckpts and
			// we can't garantee that until all clients references
			// are gone. That's why in case of forced shutdown. we
			// need to move the call to _ckpt_handler.shutdown()
			// into generic_repl_prov::try_cleanup() after the
			// number of handlers has dropped to zero.
			//
		} else {
			ASSERT(shutdown_type == replica::SERVICE_SHUTDOWN);
			_actual_implp->shutdown(e);
			if (!e.exception()) {
				//
				// This is a normal shutdown.
				// We need to flush out all ckpts and release
				// the reference right away because the
				// state machine will soon convert all
				// secondaries to spares without calling
				// remove_secondaries() on the primary.
				//

				// Remove the push ckpt task.
				periodic_thread::the().remove_job(&push_task);
				push_task.execute();

				//
				// The service is shutting down. Release
				// references to all the secondaries.
				//
				_ckpt_handler.shutdown();
			}
		}
		ASSERT(e.sys_exception() == NULL);

		if (!e.exception()) {
			_servicep->lock();
			_servicep->mark_shutting_down(is_forced_shutdown);
			try_cleanup();
		}
	} else {
		// shutting down a spare.
		_actual_implp->shutdown_spare(shutdown_type, e);
		ASSERT(e.exception() == NULL);

		_servicep->lock();
		_servicep->mark_shutting_down(is_forced_shutdown);
		try_cleanup();
	}

	return (0);
}

bool
generic_repl_prov::is_receiving_ckpt(Environment &)
{
	ckpt_handler_server *my_ckpt_handler_svr;
	my_ckpt_handler_svr = _ckpt_handler.get_my_ckpt_handler_svr();
	ASSERT(my_ckpt_handler_svr);

	return (my_ckpt_handler_svr->is_receiving_ckpt());
}

//
// This is always allocated as part of a full repl_prov implementation
// using the repl_server<> template.
//
generic_repl_prov::generic_repl_prov(replica::repl_prov_ptr ag,
    replica::checkpoint_ptr my_ckpt,
    ckpt_handler_server *ckpth, const char *svc_nm, const char *prov_nm) :
	_state_hash(this),
	_actual_implp(ag),
	_service_id(0),
	_servicep(NULL),
	_ckpt_proxy(NULL),
	_ckpt_handler(ckpth, &_state_hash),
	_my_ckpt(my_ckpt),
	scb(nil),
	push_task(this)
{

	_service_name = os::strdup(svc_nm);
	ASSERT(_service_name != NULL);

	_provider_name = os::strdup(prov_nm);
	ASSERT(_provider_name != NULL);
}

generic_repl_prov::~generic_repl_prov()
{
	//
	// Wait for service unref to complete. It is possible that the
	// service_unref hasn't yet been notified about the completion
	// of the unreferenced on the last object but it is guaranteed
	// to get this notification very soon.
	//
	_service_unref.wait_till_unref_done();


	_actual_implp	= NULL;

	//
	// The checkpoint proxy is now completely reference counted. We
	// should have gotten a new reference during a call to set_checkpoint,
	// and then released that reference during the call to become_secondary.
	//
	_ckpt_proxy = NULL;

	_servicep	= NULL;
	_my_ckpt	= nil;

	ASSERT(CORBA::is_nil(scb));
	scb = nil; // Placate lint.

	ASSERT(_service_name != NULL);
	ASSERT(_provider_name != NULL);
	SERV_DBG(("%s.%s(%d): ~generic_repl_prov\n", _service_name,
	    _provider_name, _service_id));

	delete [] _service_name;
	delete [] _provider_name;
}

char *
generic_repl_prov::get_service_name() const
{
	ASSERT(_service_name != NULL);
	return (os::strdup(_service_name));
}

//
// set_checkpoint
// This checks with the multi_ckpt_handler to see if setting the type
// is valid, and if so, adjusts the proxy the generic_repl_prov points at.
//
// Returns false if the multi_ckpt_handler can't support this version,
// true otherwise.
//
bool
generic_repl_prov::set_checkpoint(replica::checkpoint_ptr p)
{

	bool retv = true;
	replica::checkpoint_ptr tmp_p = _ckpt_proxy;

	SERV_DBG(("%s.%s(%d): set_checkpoint to type %u. Current proxy is %p. "
	    "Handler is %p\n", _service_name, _provider_name, _service_id,
	    *(p->_deep_type()->get_tid()), _ckpt_proxy, &_ckpt_handler));

	// Set the type on the multi_ckpt_handler.
	// If this call returns false, then the change was invalid and we
	// should return an error.
	if (_ckpt_handler.set_type((CORBA::type_info_t*)p->_deep_type())) {
		//
		// Set our internal copy to the new proxy. This is handed out in
		// calls to generic_repl_prov::get_checkpoint.
		//
		_ckpt_proxy = replica::checkpoint::_duplicate(p);

		// Now, release the old proxy.
		CORBA::release(tmp_p);

		//
		// There is no need to do anything else to the old proxy. If
		// there are other references outstanding, it will continue
		// to associate to the multi_ckpt_handler. If a method is
		// invoked on it which the multi_ckpt_handler supports, but the
		// old proxy does not, it will return the version exception.
		// It will eventually go away when all references are gone.
		//

		retv = true;
	} else {
		retv = false;
	}

	return (retv);
}

//
// _unreferenced() for replica::repl_prov.
//
void
#ifdef DEBUG
generic_repl_prov::_unreferenced(unref_t cookie)
#else
generic_repl_prov::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	ASSERT(_servicep == NULL);

	SERV_DBG(("%s.%s(%d): _unreferenced\n", _service_name, _provider_name,
	    _service_id));

	//
	// Releasing the objects will cause the repl_server to delete itself,
	// which will delete this generic_repl_prov.
	//
	CORBA::release(_my_ckpt);
	_my_ckpt = replica::checkpoint::_nil();
	CORBA::release(_actual_implp);
	_actual_implp = replica::repl_prov::_nil();
}

void
generic_repl_prov::add_unref(replica_handler *handlerp)
{
	_service_unref.add_unref(handlerp);
}

void
generic_repl_prov::try_cleanup()
{
	ASSERT(_servicep->lock_held());

	// We cleanup only when this provider has completed registration
	// and is shutting down. We must wait for the registration to
	// complete because otherwise the value of scb is not set yet.
	// If this is not the primary, we go ahead and cleanup. If it's
	// the primary, we wait for all handlers to go away.
	if (_servicep->is_shutting_down() &&
	    _servicep->is_registration_done() &&
	    (!_servicep->is_primary() || _servicep->has_no_handler())) {
		//
		// A non-primary provider can be shut down individually without
		// affecting the running service. And the repl_service object
		// waits for other objects such as the repl_server object to
		// go away by unreference mechanism before destroying itself.
		//
		// If a provider on this node joins this service right  after
		// the old one is cleaned up, the same repl_service object
		// will be reused. If we don't clear the flags, we will
		// run into bug 4663251.
		//

		if (_servicep->is_primary()) {
			if (_servicep->is_forced_shutting_down()) {
				//
				// If this service is forced shutdown, we
				// leave the multi_ckpt_handler working
				// until all clients have released their
				// references. Since now the service is
				// marked down and there are no more handlers,
				// we know for sure that all clients references
				// are gone and it's safe to delete the
				// reference to the multi_ckpt_handler.
				//

				// Remove the push ckpt task.
				periodic_thread::the().remove_job(&push_task);
				push_task.execute();

				//
				// The service is shutting down. Release
				// references to all the secondaries.
				//
				_ckpt_handler.shutdown();
			}

			// Release our reference to the multi_ckpt_handler.
			CORBA::release(_ckpt_proxy);
			_ckpt_proxy = nil;
		}

		_servicep->clear_flags();

		_servicep->rem_repl_prov();
		set_service(NULL);

		if (is_not_nil(scb)) {
			CORBA::release(scb);
			scb = nil;
		}
	} else {
		_servicep->unlock();
	}
}

//
// All the transaction states are gone. We can deliver unreferenced
// to the ha objects.
//
void
generic_repl_prov::all_states_gone()
{
	ASSERT(_state_hash.role == state_hash_table_t::PRIMARY);
	_service_unref.enable_unreferenced();
}

// Methods for rmm
void
generic_repl_prov::register_for_rmm()
{
	SERV_DBG(("%p: registering for RMM\n", this));
	repl_service *rsp = repl_service::add_new_repl_prov(RMM_SERVICE_ID,
	    this);
	ASSERT(rsp != NULL);
	set_service(rsp);
}

static replica::service_dependencies	no_service_dependencies;
static replica::prov_dependencies	no_prov_dependencies;

//
// Methods called by the implementation
//

//
// register_with_rm - register the service with the replica manager.
// We give up the registration attempt after two failures. It will be
// retried on other providers, if any.
//
void
generic_repl_prov::register_with_rm(
    replica::service_dependencies	*service_depends_on,
    replica::prov_dependencies		*prov_depends_on,
    uint_t				priority,
    bool				restart_flag,
    uint32_t				new_desired_numsecondaries,
    Environment				&e)
{
	replica::rm_admin_var			rm_admin_v = rm_util::get_rm();
	replica_int::rma_admin_var		rma_admin_v =
						    rm_util::get_rma_admin();

	replica::service_admin_var		serv_admin_v;
	replica_int::handler_repl_prov_var	myref_v = get_objref();
	replica_int::rma_repl_prov_var		rma_prov_v;
	replica_int::rm_service_admin_var	rm_sa_v;
	int 					attempts = 1;

	SERV_DBG(("%s.%s(%d): registering with RM, numsec = %d\n",
	    _service_name, _provider_name, _service_id,
	    new_desired_numsecondaries));

	ASSERT(e.exception() == NULL);

	// FAULT_POINT
	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_1,
	    FaultFunctions::generic);

	//
	// Make sure the prov dependencies are a subset of the service
	// dependencies.
	//
	if (prov_depends_on != NULL) {
		if (service_depends_on == NULL) {
#ifdef DEBUG
			os::printf("Missing service dependencies\n");
#endif
			e.exception(new replica::invalid_dependency());
			return;
		}

		// For each service in the prov_dependency list
		for (uint_t i = 0; i < prov_depends_on->length(); i++) {
			bool	found = false;

			ASSERT((char *)(*prov_depends_on)[i].service);
			ASSERT((char *)(*prov_depends_on)[i].repl_prov_desc);

			// Make sure there's one in service_dependencies.
			for (uint_t j = 0; j < service_depends_on->length();
			    j++) {
				ASSERT((char *)(*service_depends_on)[j]);

				if (strcmp((*prov_depends_on)[i].service,
				    (*service_depends_on)[j]) == 0) {
					found = true;
					break;
				}
			}
			if (!found) {
#ifdef DEBUG
				os::printf("Missing service dependency\n");
#endif
				e.exception(new replica::invalid_dependency());
				return;
			}
		}
	}

	replica::service_state req_state = replica::S_UP;
again:
	// Register the service
	if (service_depends_on == NULL) {
		service_depends_on = &no_service_dependencies;
	}
	rm_admin_v->register_service(_service_name, *service_depends_on,
	    _service_id, new_desired_numsecondaries, req_state, e);
	CORBA::Exception	*exceptp = e.exception();
	if (exceptp != NULL) {
		if (replica::too_many_services::_exnarrow(exceptp) != NULL) {
			SERV_DBG(("%s.%s(%d): register_service: "
			    "too_many_services\n", _service_name,
			    _provider_name, _service_id));
			return;
		}
		if (replica::invalid_numsecs::_exnarrow(exceptp) != NULL) {
			SERV_DBG(("%s.%s(%d): register_service: "
			    "invalid desired numsecs %d\n", _service_name,
			    _provider_name, _service_id,
			    new_desired_numsecondaries));
			return;
		}
		//
		// Check if a service we depend upon is frozen. If yes then
		// We should also come up in a frozen state.
		//
		if (replica::dependent_service_frozen::_exnarrow(exceptp) !=
		    NULL) {
			req_state = replica::S_FROZEN_FOR_UPGRADE;
			e.clear();
			goto again;
		}

		replica::service_already_exists		*saep =
		    replica::service_already_exists::_exnarrow(exceptp);

		if (saep == NULL) {
			SERV_DBG(("%s.%s(%d): register_service failed\n",
			    _service_name, _provider_name, _service_id));

			ASSERT(!exceptp->is_system_exception());
			return;
		}

		// XXX Should check that dependencies are the same?
		if ((saep->state == replica::S_DOWN) ||
		    (saep->state == replica::S_DOWN_FROZEN_FOR_UPGRADE) ||
		    saep->state == replica::S_DOWN_FROZEN_FOR_UPGRADE_BY_REQ) {
			if (restart_flag) {
				SERV_DBG(("%s.%s(%d): service dead, "
				    "restarting\n", _service_name,
				    _provider_name, _service_id));
				shutdown_dead_service();
				//
				// If the service was frozen for
				// upgrade when it went down, it is
				// noted here.
				//
				req_state = saep->state;

				e.clear();
				goto again;
			} else {
				SERV_DBG(("%s.%s(%d): failed_service "
				    "exception\n", _service_name,
				    _provider_name, _service_id));
				e.exception(new replica::failed_service);
				return;
			}
		} else {
			// Use the service that is registered.
			SERV_DBG(("%s.%s(%d): already registered - OK\n",
			    _service_name, _provider_name, _service_id));
			_service_id = saep->sid;
			e.clear();
		}
	}

	ASSERT(e.exception() == NULL);

	SERV_DBG(("%s.%s(%d): registered service\n",
	    _service_name, _provider_name, _service_id));

	// FAULT_POINT
	FAULTPT_HA_FRMWK(
	    FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_2,
	    FaultFunctions::generic);

	rma_prov_v = rma_admin_v->
	    wrap_handler_repl_prov(myref_v, _service_id, e);
	if (e.exception()) {
		if (restart_flag &&
		    (replica::failed_service::_exnarrow(e.exception()) !=
		    NULL)) {
			// Someone did a shutdown. Start the service up again.
			SERV_DBG(("%s.%s(%d): wrap_handler_repl_prov failed, "
			    "restarting\n", _service_name, _provider_name,
			    _service_id));
			e.clear();
			goto again;
		} else {
			SERV_DBG(("%s.%s(%d): wrap_handler_repl_prov failed\n",
			    _service_name, _provider_name, _service_id));
			e.clear();
			return;
		}
	}

	serv_admin_v = rm_admin_v->get_control(_service_name, e);
	if (e.exception()) {
		if (restart_flag &&
		    (replica::unknown_service::_exnarrow(e.exception()) !=
		    NULL)) {
			// Someone did a shutdown.
			e.clear();
			goto again;
		} else {
			SERV_DBG(("%s.%s(%d): get_control failed\n",
			    _service_name, _provider_name, _service_id));
			e.clear();
			return;
		}
	}

	rm_sa_v = replica_int::rm_service_admin::_narrow(serv_admin_v);
	ASSERT(is_not_nil(rm_sa_v));

	// Find the associated repl_service, creating it if necessary.
	if (repl_service::add_new_repl_prov(_service_id, this) == NULL) {
		//
		// Error. The user is registering a service on the same node
		// twice.
		//
		e.exception(new replica::repl_prov_already_exists);
		SERV_DBG(("%s.%s(%d): add_new_repl_prov failed\n",
		    _service_name, _provider_name, _service_id));
		return;
	}

	if (prov_depends_on == NULL) {
		prov_depends_on = &no_prov_dependencies;
	}
	replica::repl_prov_reg_ptr	reg_ref;
	rm_sa_v->register_rma_repl_prov(rma_prov_v, _provider_name,
	    *prov_depends_on, _my_ckpt, priority, reg_ref, e);
	if (e.exception()) {
		//
		// Since we successfully associated a repl_service with
		// this generic_repl_prov, we need to undo that before
		// we retry/abort.
		//
		ASSERT(_servicep != NULL);
		_servicep->lock();
		_servicep->rem_repl_prov();
		_servicep = NULL;

		CORBA::Exception	*exp = e.exception();
		if (restart_flag &&
		    (replica::failed_service::_exnarrow(exp) != NULL ||
		    replica::deleted_service::_exnarrow(exp) != NULL)) {
			SERV_DBG(("%s.%s(%d): register_rma_repl_prov failed, "
			    "attempt #%d\n", _service_name, _provider_name,
			    _service_id, attempts));
			e.clear();
			attempts++;
			if (attempts <= 2) {
				goto again;
			} else {
				return;
			}
		} else {
			SERV_DBG(("%s.%s(%d): register_rma_repl_prov failed\n",
			    _service_name, _provider_name, _service_id));
			e.clear();
			return;
		}
	}

	_servicep->lock();
	ASSERT(CORBA::is_nil(scb));
	scb = reg_ref;
	_servicep->mark_registration_done();
	try_cleanup();
}

//
// Used by register_with_rm() when the service is down and the caller wants to
// automatically restart it.
//
void
generic_repl_prov::shutdown_dead_service()
{
	replica::rm_admin_var rm_v = rm_util::get_rm();
	Environment e;

	replica::service_admin_var sa = rm_v->get_control(_service_name, e);
	if (replica::unknown_service::_exnarrow(e.exception())) {
		// Someone got there before us. That's fine.
		e.clear();
		return;
	}
	ASSERT(e.exception() == NULL);
	//
	// The service_admin reference is to the current version of the service
	// at the time of the get_control() call. We need to be sure that it
	// refers to a version of the service that is down, and not to one that
	// has already been restarted by someone else.
	//
	replica::service_info *infop = sa->get_service_info(e);
	if (replica::deleted_service::_exnarrow(e.exception())) {
		// Someone got there before us. That's fine.
		e.clear();
		return;
	}
	ASSERT(e.exception() == NULL);

	if (infop->s_state != replica::S_DOWN) {
		delete infop;
		return;
	}
	delete infop;
	//
	// Now shut down the service. It will either succeed or fail with a
	// deleted_service exception (if someone else shut it down first).
	//
	sa->shutdown_service(true, e);

	//
	// Currently replica::service_admin::service_changed is not returned.
	// If in future it can be returned, then this assert will catch it.
	// ASSERT(replica::service_admin::service_changed::_exnarrow(
	// e.exception()) == NULL);

	ASSERT(replica::service_busy::_exnarrow(e.exception()) == NULL);
	ASSERT(replica::depends_on::_exnarrow(e.exception()) == NULL);
	if (replica::deleted_service::_exnarrow(e.exception())) {
		// Someone got there before us. That's fine.
		e.clear();
	}
	// Panic if some unexpected exception was returned.
	CL_PANIC(e.exception() == NULL);
}

//
// Methods called by the implementation.
//
void
generic_repl_prov::request_primary_mode()
{
	Environment e;
#ifdef DEBUG
	os::printf("making this node primary RM\n");
#endif
	replica::rm_admin_var rm_v = rm_util::get_rm();
	replica::service_admin_var sa =
	    rm_v->get_control(_service_name, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "register_with_rm: get_control failed");
		return;
	}

	sa->change_repl_prov_status(_provider_name, replica::SC_SET_PRIMARY,
	    false, e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "generic::change_repl_prov_status");
		return;
	}
}

// Methods called by replica_handler

transaction_state *
generic_repl_prov::lookup_state(replica::transaction_id_ptr tridp,
    Environment *e)
{
	return (_state_hash.lookup_state(tridp, e));
}

// End of methods called by replica_handler.

//
// Periodic task that pushes checkpoints to be executed.
//
void
push_ckpt_task::execute()
{
	Environment e;
	primary_ctx	ctx(NULL, primary_ctx::PUSH_CKPT, e);
	repl_provp->get_checkpoint()->ckpt_noop(e);
	ASSERT(e.exception() == NULL);
	e.trans_ctxp = NULL;
}

uint_t
push_ckpt_task::get_period()
{
	return (1);
}

push_ckpt_task::~push_ckpt_task()
{
	repl_provp = NULL;
}


//
// add_unref - Add the replica_handler unreference task to the appropriate list.
// When not freezing the service, tell the handler to
// queue itself for actual unreference processing.
// When freezing the service, add unreference task to the defer list
// and do not unreference the handler at this time.
//
void
service_unref::add_unref(replica_handler *handlerp)
{
	lock();
	ASSERT(is_primary);

	if (defer_unref) {
		//
		// The unreference processing is deferred
		//
		defer_list.append(handlerp);
		unlock();
	} else {
		unref_count++;
		CL_PANIC(unref_count != 0);	// Check for wraparound

		//
		// Drop lock now to reduce period of lock contention.
		// The lock only needs to protect the unref_count.
		//
		unlock();

		// Schedule to deliver unreferenced notification to object impl
		unref_threadpool::the().defer_unref_processing(handlerp);
	}
}

void
service_unref::unref_done()
{
	lock();
	ASSERT(unref_count > 0);
	ASSERT(is_primary);
	if (--unref_count == 0) {
		cv.signal();
	}
	unlock();
}

//
// enable_unreferenced - deliver unreferenced to the handlers in defer_list
//
void
service_unref::enable_unreferenced()
{
	lock();
	defer_unref = false;
	unref_task	*taskp;
	while ((taskp = defer_list.reapfirst()) != NULL) {
		unref_count++;
		CL_PANIC(unref_count != 0);	// Check for wraparound

		// Add the task now to the threadpool for processing
		unref_threadpool::the().defer_unref_processing(taskp);
	}
	unlock();
}

void
service_unref::become_secondary()
{
	lock();
	ASSERT(defer_unref);
#ifdef DEBUG
	is_primary = false;
#endif
	//
	// Throw away all the unreferenced tasks queued up in the defer list.
	// Clear the handler state that says an unreference is pending.
	//
	unref_task *taskp;
	while ((taskp = defer_list.reapfirst()) != NULL) {
		((replica_handler *)(taskp->get_unref_handler()))->
		    clear_unref_flag();
	}
	unlock();
}

void
service_unref::freeze_primary()
{
	// Wait for unreferenced to drain out.
	lock();
	ASSERT(!defer_unref);
	defer_unref = true;
	while (unref_count > 0)
		cv.wait(&lck);
	unlock();
}

void
service_unref::wait_till_unref_done()
{
	lock();
	while (unref_count > 0) {
		cv.wait(&lck);
	}
	unlock();
}

#endif // _KERNEL_ORB

// Static
int
repl_service_manager::initialize()
{
	ASSERT(the_repl_service_manager == NULL);
	the_repl_service_manager = new repl_service_manager();
	if (the_repl_service_manager == NULL)
		return (ENOMEM);

	ha_tid_factory::initialize();
#ifdef	_KERNEL_ORB
	int retval;

	// Start the orb threads.
	if ((retval = orphan_thread::initialize()) != 0) {
		return (retval);
	}
	if ((retval = periodic_thread::initialize()) != 0) {
		return (retval);
	}

	// Initialize the checkpoint handling threadpool.
	if ((retval = ckpt_handler_server::initialize()) != 0)
		return (retval);
#endif
	return (0);
}

void
repl_service_manager::shutdown()
{
	if (the_repl_service_manager == NULL) {
		return;
	}
#ifdef _KERNEL_ORB
	periodic_thread::the().shutdown();
	ckpt_handler_server::shutdown();
	orphan_thread::the().shutdown();
#endif
	ha_tid_factory::shutdown();

	delete the_repl_service_manager;
	the_repl_service_manager = NULL;
}

//
// Lookup a service given its service id.
// The caller holds the repl_service_manager lock and the call returns
// with the service lock held if the service is found.
//
repl_service *
repl_service_manager::lookup_hold_locked(replica::service_num_t sid,
    bool create_it)
{
	ASSERT(lock_held());

	repl_service *rsp = _service_list.lookup(sid);
	if (rsp == NULL && create_it) {
		SERV_DBG(("Allocating new repl_service for %d\n", sid));
		rsp = new repl_service(sid);
		_service_list.add(rsp, sid);
	}
	if (rsp != NULL) {
		rsp->lock();
	}
	return (rsp);
}

repl_service *
repl_service_manager::lookup_hold(replica::service_num_t sid, bool create_it)
{
	lock();
	repl_service *rsp = lookup_hold_locked(sid, create_it);
	unlock();
	return (rsp);
}

//
// Try to remove a service from the list. It is called when the caller
// thinks that the service can go away.
//
void
repl_service_manager::remove_service(replica::service_num_t sid)
{
	lock();
	repl_service *rsp = lookup_hold_locked(sid, false);

	// The repl_service could have been deleted so we must check here.
	if (rsp != NULL) {
		// XXX This could be more efficient if the hash template
		// supported delete by reference.  This is not performance
		// critical, since it doesn't happen very often, so don't
		// worry about it for now.

		// We need to verify we can delete it again. This is
		// because the caller dropped its repl_service lock before
		// calling remove_service, and something could have
		// happened in between.
		if (rsp->verify_delete()) {
			(void) _service_list.remove(sid);
			delete rsp;
		} else {
			// Release the lock lookup_hold obtained.
			rsp->unlock();
		}
	}
	unlock();
}
