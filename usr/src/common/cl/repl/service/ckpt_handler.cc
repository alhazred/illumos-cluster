/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ckpt_handler.cc	1.155	08/05/20 SMI"

#include <repl/service/ckpt_handler.h>
#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/standard_xdoor.h>

#include <repl/rma/hxdoor.h>
#include <repl/service/repl_tid_impl.h>
#include <orb/object/schema.h>
#include <orb/flow/resource.h>
#include <repl/service/replica_handler.h>
#include <repl/service/multi_ckpt_handler.h>
#include <h/replica.h>
#include <orb/infrastructure/orb.h>

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#endif

#ifdef	CKPT_DBGBUF
uint32_t ckpt_dbg_size = 200000;
dbg_print_buf ckpt_dbg(ckpt_dbg_size);
#endif

// Handler Kit stuff
ckpt_handler_kit *ckpt_handler_kit::the_ckpt_handler_kit = NULL;

//
// The threadpool that processes all the checkpoints on this node.
// Construct this with 0 threads. The threads are created in initialize()
// called during ORB::initialize
//
threadpool ckpt_threadpool(false, 0, "checkpoint processing threadpool");

remote_handler *
ckpt_handler_kit::create(Xdoor *xdp)
{
	return (new ckpt_handler_client(xdp));
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
ckpt_handler_kit::initialize()
{
	ASSERT(the_ckpt_handler_kit == NULL);
	the_ckpt_handler_kit = new ckpt_handler_kit();
	if (the_ckpt_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
ckpt_handler_kit::shutdown()
{
	if (the_ckpt_handler_kit != NULL) {
		delete the_ckpt_handler_kit;
		the_ckpt_handler_kit = NULL;
	}
}

//
// Returns a reference to ckpt_handler_kit instance.
//
ckpt_handler_kit&
ckpt_handler_kit::the()
{
	ASSERT(the_ckpt_handler_kit != NULL);
	return (*the_ckpt_handler_kit);
}

//
// ckpt_handler methods
//

ckpt_handler_server::ckpt_handler_server(CORBA::Object_ptr impl_objp) :
    obj_(impl_objp),
    dump_seq(1),
    min_seq(0),
    commit_seq(0),
    last_seq(0),
    last_process_seq(0),
    waiting_deliver(false),
    flushing(false),
    flushing_seq(0),
    hashtab(NULL),
    receiving_ckpt(false)
{
}

hkit_id_t
ckpt_handler::handler_id()
{
	return (ckpt_handler_kit::the().get_hid());
}

bool
ckpt_handler::is_user()
{
#ifdef _KERNEL_ORB
	return (false);
#else
	return (true);
#endif
}

// Roll back the translate in on the xdoors in the service.
void
ckpt_handler::xdoor_roll_back(recstream *re)
{
	uint32_t xdoorcnt = re->get_xdoorcount();
	MarshalStream &xdoors = re->get_XdoorTab();
	xdoors.prepare();
	Xdoor_manager::translate_in_roll_back(xdoorcnt, xdoors);
}

//
// server methods
//

ckpt_handler_server::~ckpt_handler_server()
{
	// We must get the lock to ensure that whatever caused
	// the unreferenced to be triggered has finished with it.
	// There is no need to unlock, since we are deleting the lock.
	lock();

	//
	// It is only safe to delete an implementation object if
	// it has no references.
	//
	ASSERT(nref == 0);
	ASSERT(xdoorp == NULL);

	ASSERT(state_.get() == NEW_HANDLER ||
	    state_.get() == UNREF_DONE);

	// Make lint happy. Null the pointers
	obj_	= NULL;
	hashtab	= NULL;
}

bool
ckpt_handler_server::is_local()
{
	return (true);
}

// Initialize ckpt_threadpool.
int
ckpt_handler_server::initialize()
{
	// The ckpt_threadpool is constructed as a global variable already
	int tcnt = ckpt_threadpool.change_num_threads(1);
	if (tcnt < 1) {
#ifdef DEBUG
		os::warning("Could not create checkpoint threadpool thread");
#endif
		return (ENOMEM);
	}
	return (0);
}

// Shutdown the ckpt_threadpool.
void
ckpt_handler_server::shutdown()
{
	ckpt_threadpool.quiesce(threadpool::QBLOCK_EMPTY);
	ckpt_threadpool.shutdown();
}

//
// process - checkpoints are not immediately processed.
// Upon arrival checkpoints are queued for later processing.
// This method processes a checkpoint from the work queue.
//
void
ckpt_handler_server::process(service &serv)
{
	replica::transaction_id_var	trans_id_v;

	//
	// We must process commits here. We can't process commits as soon
	// as we receive them because at that time the transaction state
	// for the commit may not have been created yet. Remember the
	// transaction state is created when processing a checkpoint, and
	// processing of checkpoints are delayed.
	//

	//
	// Process the commits for transactions.
	//
	uint_t		number_commits = serv.get_unsigned_int();
	uint_t		i;

	for (i = 0; i < number_commits; i ++) {
		replica::transaction_id_var	trid_v =
		    (replica::transaction_id_ptr)
		    serv.get_object(&_replica_transaction_id_ArgMarshalFuncs);
		CL_PANIC(is_not_nil((replica::transaction_id_ptr) trid_v));
		hashtab->lock();
		transaction_state	*statep = hashtab->lookup(trid_v);

		//
		// Since right now it's possible for the framework and the
		// service to both send commit, we want to make sure that we
		// only do commit once.
		//
		if (statep && !statep->is_committed()) {
			statep->frmwrk_committed();
			//
			// If it's already unreferenced we should delete
			// the state object from the hash table.
			//
			if (statep->is_orphaned() || statep->is_completed()) {
				(void) hashtab->remove_from_hashtable(
				    statep->get_trans_id());
				delete statep;
			}
		}
		hashtab->unlock();
	}

	// Get the delete messages for ha objects.
	uint_t		number_deletes = serv.get_unsigned_int();

	for (i = 0; i < number_deletes; i++) {
		replica::service_num_t sid = serv.get_unsigned_short();
		xdoor_id xdid = serv.get_unsigned_int();
		hxdoor_id  hxdid(xdid, sid);
		CKPT_DBG(("rel_by_ckpt sid=%d, xdid=%d\n", sid, xdid));
		hxdoor_manager::the().release_by_ckpt(hxdid);
	}

	// This is the object being invoked.
	CORBA::Object_ptr object_p = _obj();

	InterfaceDescriptor *infp = object_p->_interface_descriptor();
	uint_t inf_index;
	InterfaceDescriptor::ReceiverFunc recfunc = get_index_method(serv,
	    infp, inf_index);

	if (recfunc == NULL) {
		//
		// SCMSGS
		// @explanation
		// One of the components is running at an unsupported older
		// version.
		// @user_action
		// Ensure that same version of Sun Cluster software is
		// installed on all cluster nodes.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: Secondary version %u does not support checkpoint"
		    " method %d on interface %s.", infp->get_tid(),
		    inf_index, infp->get_name());
	}

	// Get the transaction ID object.
	trans_id_v = (replica::transaction_id_ptr)
	    serv.get_object(&_replica_transaction_id_ArgMarshalFuncs);

	//
	// A NULL trans_id_v means this is an administrative message instead
	// of a real checkpoint message. We don't need to set up the
	// secondary_ctx in this case.
	//
	if (CORBA::is_nil(trans_id_v)) {
		(*recfunc)(infp->cast(object_p->_this_ptr(), inf_index), serv);
		return;
	}

	secondary_ctx	ctx(*(serv.get_env()));
	ctx.handlerp = this;
	ctx.begin(trans_id_v);

	// unmarshal -> call method -> marshal back
	(*recfunc)(infp->cast(object_p->_this_ptr(), inf_index), serv);

	ctx.completed(*(serv.get_env()));
	ctx.handlerp = NULL;
}

//
// Note that we call processed_checkpoint for both regular checkpoint
// and dumpstate messages. Because they never mix up, i.e. we always
// get all the dumpstate messages first and then checkpoints, we can
// tell when dumpstate completed when the first real checkpoint comes.
//
void
ckpt_handler_server::processed_checkpoint(replica::ckpt_seq_t seq)
{
	// Update the last_process_seq. Wake others up if need to.
	lock();
	CL_PANIC(last_process_seq + 1 == seq);
	last_process_seq = seq;
	ckpt_info last_info(last_process_seq - ckpt_range, last_process_seq);
	if (flushing && last_info >= flushing_seq) {
		quiesce_cv.broadcast();
	}
	unlock();
}

//
// handle_incoming_call - was adapted from remote_handler::handle_incoming_call
// to support checkpoint processing.
//
// ckpt handler header - After the header for the xdoor layer has
// been stripped off, the header now consists of:
//	checkpoint sequence number
//	commit sequence number
//	count of piggybacked commits
//	piggybacked commit marshalled objects
//	count of piggybacked deletes
//	piggybacked deletes (service id and object id)
//	number identifying the method to be invoked.
//	index used to find the receiver function for the invoked class
//	transaction id marshalled object
//
// Only the checkpoint and commit sequence numbers are extracted and
// processed directly in this method.
//
void
ckpt_handler_server::handle_incoming_call(service &serv)
{
	ASSERT(!serv.get_env()->exception());

	//
	// FAULT POINT
	// This generic fault point allows fault injection during an incoming
	// checkpoint.  Before we can process the checkpoint on the
	// ha object.
	//
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_CKPT_HANDLER_HANDLE_INCOMING_CALL,
		FaultFunctions::generic);

	// Get the current checkpoint sequence number.
	replica::ckpt_seq_t	current_seq =
	    (replica::ckpt_seq_t)serv.get_unsigned_long();

	// Get the commit checkpoint sequence number.
	replica::ckpt_seq_t	piggy_seq =
	    (replica::ckpt_seq_t)serv.get_unsigned_long();

	CKPT_DBG(("%p new %u, %u\n", this, current_seq, piggy_seq));

	if (current_seq == 0) {
		if (piggy_seq == 0) {
			//
			// This is an admin message, we should
			// process it immediately.
			//
			ASSERT(serv.re->get_msgtype() == INVO_MSG);
			ASSERT(!serv.re->get_invo_mode().is_oneway());

			// Process the checkpoint and piggybacked info
			process(serv);

			CKPT_DBG(("%p process admin done.\n", this));
			return;
		} else if (piggy_seq < ckpt_range) {
			//
			// This is a dumpstate message.
			//
			ASSERT(serv.re->get_msgtype() == CKPT_MSG);
			ASSERT(serv.re->get_invo_mode().is_oneway());

			ckpt_elem	*dpelem = new ckpt_elem(piggy_seq,
			    serv, this);

			lock();
			if (piggy_seq == dump_seq) {
				//
				// This is the dumpstate message
				// we are waiting for, process it.
				//
				dump_seq++;
				CKPT_DBG(("%p dump %u\n", this,
				    dpelem->seq));
				ckpt_threadpool.defer_processing(dpelem);
				dump_msgs.atfirst();

				// Process others in the list.
				while ((dpelem =
				    dump_msgs.get_current()) != NULL &&
				    dpelem->seq == dump_seq) {
					(void) dump_msgs.reapfirst();
					dump_seq++;

					CKPT_DBG(("%p dump %u\n", this,
					    dpelem->seq));

					ckpt_threadpool.
					    defer_processing(dpelem);
				}
			} else {
				//
				// This is not the dumpstate with
				// the right sequence, insert it
				// into the queue and process later.
				//
				dump_msgs.insert(0, dpelem);
			}
			unlock();
			CKPT_DBG(("%p dumpstate done........\n", this));
			return;
		}
	}

	// This is a checkpoint.
	lock();
	ASSERT(serv.re->get_msgtype() == CKPT_MSG);
	ASSERT(serv.re->get_invo_mode().is_oneway());

#ifdef DEBUG
	//
	// Sanity check. We shouldn't receive any checkpoint
	// that has been processed. It should catch duplicate
	// checkpoints too.
	//
	ckpt_info	db_check_info(min_seq, current_seq);
	ASSERT(db_check_info > last_seq);
#endif // DEBUG

	//
	// Insert this checkpoint into the checkpoint list.
	// It will be processed later when a commit for it comes.
	//
	ckpt_elem	*ckelem = new ckpt_elem(current_seq, serv, this);

	ckpt_info	piggy(min_seq, piggy_seq);

	if (piggy > commit_seq) {
		commit_seq = piggy_seq;
	}
	checkpoints.insert(min_seq, ckelem);

	//
	// Process all the committed checkpoints. Note that all
	// the committed checkpoints may not be in our list yet,
	// some of them may still in orbthread pool. So we use
	// last_seq to make sure that we process checkpoints in
	// order. This step should be fast as we are handing the
	// checkpoints to another thread to process.
	//
	checkpoints.atfirst();

	// use ckpt_info to deal with wrap around
	ckpt_info commit_info(min_seq, commit_seq);

	while (((ckelem = checkpoints.get_current()) != NULL) &&
	    (commit_info >= ckelem->seq) &&	// committed
	    (last_seq + 1 == ckelem->seq)) {	// in sequence
		CKPT_DBG(("%p process %u\n", this, ckelem->seq));
		(void) checkpoints.reapfirst();
		min_seq = ckelem->seq - ckpt_range;
		last_seq++;
		ckpt_threadpool.defer_processing(ckelem);
	}

	// Wake the quiesce thread up.
	if (waiting_deliver && last_seq == commit_seq) {
		quiesce_cv.broadcast();
	}
	CKPT_DBG(("%p done\n", this));
	unlock();
}

//
// It returns the highest sequence number this ckpt_handler
// received in sequence so far.
// This is only called from multi_ckpt_handler::become_primary_prepare,
// which happens when the 1) The old primary died and orb checkpoint threadpool
// has been quiesced. 2) During a switchover, after become_secondary_prepare
// is called, which means the commit_seq has been updated correctly by
// process_to. We can always answer the get_high_seq question
// correctly, because in case 1, all checkpoints are known to us; and in
// case 2, we have the correct commit_seq information and we know there is
// no checkpoint bigger than commit_seq.
//
replica::ckpt_seq_t
ckpt_handler_server::get_high_seq()
{
	replica::ckpt_seq_t last;
	ckpt_elem *ckelem;

	lock();
	CKPT_DBG(("%p get_high_seq, last_seq: %u, commit_seq: %u\n",
	    this, last_seq, commit_seq));

	// The smallest value we return is the commit_seq. We know that
	// everything before commit_seq is on our node.
	ckpt_info commit_info(min_seq, commit_seq);

	checkpoints.atfirst();
	// Find the checkpoint that's not committed. Everything committed
	// are considered in sequence.
	while (((ckelem = checkpoints.get_current()) != NULL) &&
	    (commit_info >= ckelem->seq)) {
		checkpoints.advance();
	}

	if (ckelem == NULL) {
		last = commit_seq;
	} else {
		last = ckelem->seq;

		ckpt_info last_info(min_seq, last);
		ASSERT(last_info >= commit_seq + 1);
		if (last_info != (commit_seq + 1)) {
			last = commit_seq;
		} else {
			checkpoints.advance();

			// Find the highest in sequence ckpt seq number.
			while ((ckelem = checkpoints.get_current()) != NULL &&
			    (ckelem->seq == last + 1)) {
				last++;
				checkpoints.advance();
			}
		}
	}

	CKPT_DBG(("%p get_high_seq, return %u\n", this, last));
	unlock();
	return (last);
}


// Process_to can be called in the following scenario:
// 1) From become_primary. We are guaranteed that there is no hidden
// checkpoints in the transport level.
void
ckpt_handler_server::process_to(replica::ckpt_seq_t seq)
{
	replica::transaction_id_var trans_id;
	ckpt_elem *ckelem;

	lock();
	CKPT_DBG(("%p process_to: %u\n", this, seq));

	checkpoints.atfirst();
	ckpt_info commit_info(min_seq, seq);
	while ((ckelem = checkpoints.get_current()) != NULL) {
		if (commit_info >= ckelem->seq) { // committed
			if (last_seq + 1 == ckelem->seq) { // in sequence
				CKPT_DBG(("%p process %u\n",
				    this, ckelem->seq));
				(void) checkpoints.erase(ckelem);
				min_seq = ckelem->seq - ckpt_range;
				last_seq++;
				ckpt_threadpool.defer_processing(ckelem);
			} else {
				// not in sequence, skip.
				checkpoints.advance();
			}
		} else {
			// not committed, delete.
			(void) checkpoints.erase(ckelem);
			ckelem->dispose();
		}
	}

	// Set the commit seq.
	commit_seq = seq;

	CKPT_DBG(("%p process_to: done commit_seq=%u\n", this, commit_seq));
	unlock();

	// Without this step, it's possible for the secondary on the RM
	// node to get unreferenced for the xdoor before the checkpoints
	// are processed. This will cause checkpoints to be lost. See bug
	// 4017584.
	wait_deliver();
}

// Wait for all the checkpoints being delivered to the ckpt_handler.
void
ckpt_handler_server::wait_deliver()
{
	// Make sure that all checkpoints committed are added to
	// the ckpt processing thread. This is to deal with the case
	// where process_to has delivered the correct commit_seq to us,
	// but some checkpoints are still in transport/orb level.
	lock();
	waiting_deliver = true;
	CKPT_DBG(("%p wait_deliver\n", this));
	while (last_seq != commit_seq) {
		quiesce_cv.wait(&lck);
	}
	waiting_deliver = false;
	unlock();
}

// Wait until all the checkpoints up to the specified point are processed.
// We use flush_to for both dumpstate and checkpoint messages. To understand
// how it works remember that the primary always dumps state first, then
// calls initialize_seq, then start checkpointing to us. So we initialize
// last_process_seq to that of the last dumpstate message (we know it's 0)
// in the constructor, and we set the last_process_seq to the proper value
// in initialize_seq. This way flush_to always does the right thing.
void
ckpt_handler_server::flush_to(replica::ckpt_seq_t seq)
{
	lock();
	flushing = true;
	flushing_seq = seq;
	CKPT_DBG(("%p flush_to %u\n", this, seq));
	ckpt_info flush_info(seq - ckpt_range, seq);
	while (flush_info > last_process_seq) {
		quiesce_cv.wait(&lck);
	}
	flushing = false;
	unlock();
}

// Wait till all checkpoints are processed. This is called when this node
// is becoming primary or spare, in which cases we want to make sure
// that no checkpoints will be delivered beyond a certain point.
// This should only be called after wait_deliver is called. When this
// is called all the checkpoints are known to us and no more
// will be delivered to us from the transport.
void
ckpt_handler_server::quiesce_ckpt_threadpool()
{
	CL_PANIC(last_seq == commit_seq);
	CL_PANIC(checkpoints.empty());

	CKPT_DBG(("%p quiesce\n", this));
	// Wait till all checkpoints are processed.
	ckpt_threadpool.quiesce(threadpool::QFENCE);
}

// We are a spare now, clean up.
void
ckpt_handler_server::become_spare()
{
	CKPT_DBG(("%p become_spare\n", this));
	lock();
	//
	// Most of the time become_spare follows a process_to, which sets
	// everything right. However, if the primary died before it calls
	// process_to, we need to throw away checkpoints. In either case,
	// we know all the checkpoints have been delivered to us by the
	// transport.
	//
	checkpoints.dispose_ckpts();
	dump_msgs.dispose_ckpts();

	unlock();

	quiesce_ckpt_threadpool();

	// Reset the sequence number for the start of the next dumpstate.
	lock();
	dump_seq = 1;
	last_process_seq = 0;
	receiving_ckpt = false;
	unlock();
}

void
ckpt_handler_server::initialize_seq(replica::ckpt_seq_t m)
{
	lock();
	min_seq = commit_seq = last_seq = last_process_seq = m;
	CKPT_DBG(("%p  initialize_seq to %u\n", this, m));
	unlock();
}

//
// revoke - future invocations are prevented by revoking any attached xdoor.
//	No new xdoors are allowed.
//	It is possible that no xdoors were ever created.
//
//	The revoke operation may take an arbitrary period of time, because
//	it will not complete until all current remote invocations complete.
//
//	It is permissible to perform a revoke operation
//	after an unreferenced operation.
//
void
ckpt_handler_server::revoke()
{
	lock();
	ASSERT(revoke_state_.get() != REVOKED_HANDLER);
	revoke_state_.set(REVOKED_HANDLER);
	if (xdoorp != NULL) {
		// Incrementing marshcount prevents xdoor from
		// getting disconnected
		// We cannot hold the lock while calling xdoorp->revoked()
		// as it may block waiting for something that needs to
		// get the handler lock - e.g., it may try to marshal the object
		marshcount++;
		unlock();

		xdoorp->revoked();
		// We do an implicit marshal_cancel in revoked() to account for
		// the extra marshcount increment above.
		// This causes the usual unreferenced protocol to happen
		// to disconnect the xdoor from the handler

		// The unref protocol may have deleted the handler, so do not
		// grab the lock or refer to the handler structure anymore
	} else {
		// Xdoor has already been disconnected.
		// Unref would have been delivered when nref went to zero,
		// or when the xdoor was disconnected.
		// In the case where the implementation did a revoke prior to
		// any get_objref, it will NOT get an unreferenced. Is this OK?
		unlock();
	}
}

//
// get_unref_handler - Returns the handler to be used by the unref_task
//
handler *
ckpt_handler_server::get_unref_handler()
{
	return (this);
}

//
// begin_xdoor_unref
//
//   obtains handler lock and returns handler's marshal count.
//   Called by the xdoor's deliver_unreferenced() method.
//
uint_t
ckpt_handler_server::begin_xdoor_unref()
{
	lock();
	return (marshcount);
}

//
// end_xdoor_unref
//
//   called by xdoor to inform the handler if the xdoor has decided to
//   delete itself. The handler may decide to delete implementation object
//   thereby deleting itself.
//
//   See orb/handler/standard_handler.cc end_xdoor_unref() method for
//   explanation of how/what value handler state has.
//
void
ckpt_handler_server::end_xdoor_unref(bool xdoor_unref)
{
	ASSERT(lock_held());
	if (xdoor_unref) {
		xdoorp = NULL;
		//
		// Set marshcount to 0, in preparation for the next xdoor
		// to get attached to this handler. We set it here instead
		// after creating the new xdoor, because the handler::marshal
		// routine increments/checks marshcount before creating the
		// new xdoor. This is needed to be able to recover from a
		// memory allocation failure or xdoorid shortage.
		//
		marshcount = 0;
		if (nref == 0) {
			switch (state_.get()) {
			case ACTIVE_HANDLER:

				ASSERT(is_not_nil(obj_));
				//
				// Now we are about to actually
				// process the unreference.
				//
				state_.set(UNREF_PROCESS);

				//
				// Cannot hold the handler lock while delivering
				// unreference to the implementation object.
				//
				unlock();
				//
				// The cookie is used by objects supporting
				// multiple interfaces.  The cookie allows the
				// implementation object to determine which
				// interface received the unreference
				// notification.  The overwhelming majority of
				// implementation objects have only one
				// interface, and thus ignore this argument.
				//
				obj_->_unreferenced((unref_t)this);
				return;

			case UNREF_UNSCHEDULE:
				state_.set(UNREF_SCHEDULED);
				break;

			case UNREF_NO_PROCESS:
				state_.set(UNREF_PROCESS);
				break;

			default:
				ASSERT(0);
			}
		}
	} else {
		ASSERT((state_.get() == ACTIVE_HANDLER) ||
		    (state_.get() == UNREF_NO_PROCESS) ||
		    (state_.get() == UNREF_UNSCHEDULE));
	}
	unlock();
}

//
// deliver_unreferenced - process an unref_task for a handler.
//
void
ckpt_handler_server::deliver_unreferenced()
{
	lock();
	ASSERT(is_not_nil(obj_));

	if (state_.get() == UNREF_UNSCHEDULE) {
		//
		// A 0->1 ref count transition occurred.
		// No longer need to unreference the implementation object.
		//
		state_.set(ACTIVE_HANDLER);
		unlock();
		return;
	}

	//
	// Now we are about to actually process the unreference.
	//
	ASSERT(state_.get() == UNREF_SCHEDULED);
	state_.set(UNREF_PROCESS);

	//
	// Cannot hold the handler lock while delivering unreference
	// to the implementation object.
	//
	unlock();

	//
	// The cookie is used by objects supporting multiple interfaces.
	// The cookie allows the implementation object to determine
	// which interface received the unreference notification.
	// The overwhelming majority of implementation objects have only
	// one interface, and thus ignore this argument.
	//
	obj_->_unreferenced((unref_t)this);

	// The handler may no longer exist after the above operation completes.
}

//
// last_unref - Returns true when no new object references have
// been created since the time when unreference notification was initiated.
// Can only be called from the implementation object once
// when dealing with an unreference notification.
// This method is not idempotent.
//
bool
ckpt_handler_server::last_unref()
{
	bool	unref_ok;

	lock();
	//
	// Adjust state to show that unreference was delivered to the
	// implementation object.
	//
	switch (state_.get()) {
	case UNREF_PROCESS:
		unref_ok = true;
		state_.set(UNREF_DONE);
		break;

	case UNREF_NO_PROCESS:
		unref_ok = false;
		state_.set(ACTIVE_HANDLER);
		break;

	default:
		ASSERT(0);
		unref_ok = false;
	}
	unlock();
	return (unref_ok);
}

//
// new_reference - create a new reference for the handler.
//
// If the handler had an unreference pending, that unreference request
// is discarded.
//
void
ckpt_handler_server::new_reference()
{
	lock();

	//
	// The handler must be in a viable state to permit new references.
	//
	if (revoke_state_.get() == REVOKED_HANDLER) {
		os::panic("can't get new reference");
	}

	if (nref == 0 && xdoorp == NULL) {
		//
		// Perform a zero to one transition.
		// The handler now has given out a reference and is not new.
		// Any pending unreference is cancelled.
		//
		switch (state_.get()) {
		case NEW_HANDLER:
		case UNREF_DONE:
			state_.set(ACTIVE_HANDLER);
			break;

		case UNREF_SCHEDULED:
			state_.set(UNREF_UNSCHEDULE);
			break;

		case UNREF_PROCESS:
			state_.set(UNREF_NO_PROCESS);
			break;

		default:
			ASSERT(0);
		}
		// no need to initialize marshal counts
	} else {
		// References already existed.
		//
		ASSERT(state_.get() == ACTIVE_HANDLER ||
		    state_.get() == UNREF_UNSCHEDULE ||
		    state_.get() == UNREF_NO_PROCESS);
	}
	nref++;
	ASSERT(nref != 0);	// Check for wraparound
	unlock();
}

//
// marshal - the operation will be rejected if the handler is revoked.
//
void
ckpt_handler_server::marshal(service &_in, CORBA::Object_ptr)
{
	ASSERT(!_in.get_env()->is_nonblocking());

	lock();

	// We must have a reference if we are marshaling one.
	ASSERT(nref > 0);

	// Increment the marshal count always.
	// marshal_roll_back will recover in case there is an exception later

	// Incrementing marshcount prevents an existing xdoor, or an xdoor
	// (that will get created due to a later marshal) from
	// getting disconnected until this marshal gets rolled back or goes
	// through translate_in.
	// If there is an exception in this marshal, and no later marshals
	// occur. marshal_roll_back will bring marshcount back to 0
	marshcount++;
	//
	// Don't marshal revoked server xdoors.
	//
	if (revoke_state_.get() == REVOKED_HANDLER) {
		unlock();

		// Throw exception
		_in.get_env()->system_exception(
			CORBA::BAD_PARAM(0, CORBA::COMPLETED_MAYBE));
		return;
	}

	if (xdoorp == NULL) {
		// Allocate a server xdoor.
		xdoorp = standard_xdoor_server::create(this);
		// We do blocking allocations and should not be getting NULL
		ASSERT(xdoorp != NULL);
	}
	unlock();
	remote_handler::marshal(_in, obj_);
}

//
// Unmarshal the handler.
//
CORBA::Object_ptr
ckpt_handler_server::unmarshal(service &, CORBA::TypeId,
    generic_proxy::ProxyCreator, CORBA::TypeId to_tid)
{
	lock();
	// We can't receive a reference if we know of none.
	ASSERT(nref != 0 || xdoorp != NULL);

	// Account for the new reference.
	nref++;
	unlock();

	//
	// Get the object pointer by casting from the narrowest type.
	// We have to adjust the pointer in case this object uses
	// multiple inheritance.
	//
	return ((CORBA::Object_ptr)direct_narrow(obj_, to_tid));
}

//
// Narrow the object to an object of the given type.
//
void *
ckpt_handler_server::narrow(CORBA::Object_ptr,
    CORBA::TypeId to_tid, generic_proxy::ProxyCreator)
{
	//
	// Narrow for the server is simple, because the deep type of
	// the object is the one the object already has, and thus is
	// known to the domain.  Only casting has to be performed.
	//
	void	*objectp = direct_narrow(obj_, to_tid);
	if (objectp != NULL) {
		//
		// Yes, can cast.
		// Must have a reference if can narrow that reference.
		//
		ASSERT(nref > 0);
		incref();
	}
	return (objectp);
}

//
// duplicate - someone is using an additional object reference.
//
void *
ckpt_handler_server::duplicate(CORBA::Object_ptr object_p,
#ifdef DEBUG
    CORBA::TypeId tid)
#else
    CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));

	// We must have a reference if we are duplicating it.
	ASSERT(nref > 0);

	// Increase the reference count.
	incref();

	//
	// Notice: return the object_p parameter instead of the obj_ value,
	// because the obp may have been narrowed to a different type.
	//
	return (object_p);
}

// release
void
ckpt_handler_server::release(CORBA::Object_ptr)
{
	lock();

	ASSERT(nref > 0);
	nref--;

	if (nref == 0 && xdoorp == NULL) {
		//
		// No references exist any more. Schedule an unreference.
		//
		switch (state_.get()) {
		case ACTIVE_HANDLER:
			//
			// Schedule unreferenced notification
			// to object impl.
			//
			state_.set(UNREF_SCHEDULED);
			unref_threadpool::the().
			    defer_unref_processing(this);
			break;

		case UNREF_UNSCHEDULE:
			state_.set(UNREF_SCHEDULED);
			break;

		case UNREF_NO_PROCESS:
			state_.set(UNREF_PROCESS);
			break;

		default:
			ASSERT(0);
		}
	}
	unlock();
}

//
// Client side methods.
//

bool
ckpt_handler_client::is_local()
{
	return (false);
}

// marshal
void
ckpt_handler_client::marshal(service &_in, CORBA::Object_ptr obp)
{
	ASSERT(!_in.get_env()->is_nonblocking());

	remote_handler::marshal(_in, obp);
	incmarsh();
}

extern InterfaceDescriptor _Ix__type(CORBA_Object);

//
// Unmarshal the handler.  This is called with the xdoor lock held.
// We must synchronize with the ckpt_handler_client::release function,
// which may have set nref to zero.
//
CORBA::Object_ptr
ckpt_handler_client::unmarshal(service &, CORBA::TypeId tid,
    generic_proxy::ProxyCreator create_func, CORBA::TypeId to_tid)
{
	CORBA::Object_ptr obj;

	//
	// Find the interface descriptor for this type.
	//
	InterfaceDescriptor *infp =
	    OxSchema::schema()->descriptor(tid, this);
	ASSERT(infp != NULL);

	lock();
	if (!CORBA::is_nil(proxy_obj_)) {
		//
		// Check to see that the proxy supports the correct deep type.
		// Note: the deep type can be CORBA::Object if another ORB
		// calls idlversion_impl::get_dynamic_descriptor() in this
		// ORB.
		// Otherwise, for non-HA references, the deep type should
		// always be the same.
		//
		if (infp == proxy_obj_->_deep_type()) {
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			return (obj);
		}
		// Forget the old proxy if we can cache the new one.
		if (infp != &_Ix__type(CORBA_Object)) {
			proxy_obj_ = CORBA::Object::_nil();
		}
	}

	//
	// Try to use the interface descriptor to create a proxy of the
	// deep type rather than creating a proxy of an ancestor class.
	// Keep a pointer to this proxy for unmarshal() and narrow()'s
	// later on. Note that the proxy_obj_ pointer doesn't have a
	// reference count associated with it; it is released when the
	// handler is released.
	//
	// If the server object is newer than the client code, we won't
	// be able to create a proxy for the deep type so we create
	// a proxy for the deepest (ancestor) type we know about locally.
	//
	// Also, don't cache the proxy if this is an unmarshal
	// for idlversion_impl::get_dynamic_descriptor().
	//
	unlock();
	obj = infp->create_proxy_obj(this, infp);
	if (CORBA::is_nil(obj)) {
		// Server is newer so leave proxy_obj_ nil.
		obj = (CORBA::Object_ptr)(*create_func)(this, infp);
		// Account for the new reference.
		incref();
	} else if (infp == &_Ix__type(CORBA_Object)) {
		// Account for the new reference.
		incref();
	} else {
		lock();
		if (CORBA::is_nil(proxy_obj_)) {
			proxy_obj_ = obj;
			// Account for the new reference.
			nref++;
			ASSERT(nref != 0);	// Check for wraparound
			unlock();
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(obj, to_tid);
		} else {
			//
			// We lost the race so delete ours and use the
			// other one.
			//
			ASSERT(infp == proxy_obj_->_deep_type());
			CORBA::Object_ptr tmp_obj = obj;
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			delete tmp_obj;
		}
	}
	return (obj);
}

//
// Narrow the object to an object of the given type.
// Note that we can only get a new reference from
// get_objref(), unmarshal(), or narrow().
//
void *
ckpt_handler_client::narrow(CORBA::Object_ptr object_p,
    CORBA::TypeId to_tid, generic_proxy::ProxyCreator create_func)
{
	void	*new_proxyp = direct_narrow(object_p, to_tid);
	if (new_proxyp != NULL) {
		//
		// Successful narrow on the existing proxy.
		// Increase the refcount in the proxy.
		//
		incpxref(object_p);
	} else if (object_p->_deep_type()->is_type_supported(to_tid) ||
	    object_p->_deep_type()->is_unknown()) {
		//
		// Note that the check:
		// object_p->_deep_type()->is_type_supported(to_tid)
		// is to make sure we don't allow invocations on the
		// server object it doesn't support.
		// If the deep type is unknown, we allow narrow
		// but don't allow invocations (see invoke()).
		//
		new_proxyp = (*create_func)(this, object_p->_deep_type());
		if (new_proxyp != NULL) {
			lock();

			// Increase the handler reference count.
			ASSERT(nref > 0);
			nref++;
			ASSERT(nref != 0);

			//
			// If we were unable to create a single proxy for
			// the deep type when the object was unmarshalled,
			// we save the pointer to reduce the number of
			// proxies created.
			//
			if (CORBA::is_nil(proxy_obj_) &&
			    !object_p->_deep_type()->is_unknown()) {
				proxy_obj_ = (CORBA::Object_ptr)new_proxyp;
			}

			unlock();
		}
	}
	return (new_proxyp);
}

//
// duplicate - someone is using an additional object reference.
//
void *
ckpt_handler_client::duplicate(CORBA::Object_ptr object_p,
#ifdef DEBUG
    CORBA::TypeId tid)
#else
    CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));

	// Cannot duplicate if you do not already have a reference.
	ASSERT(nref > 0);

	// Increase the proxy reference count.
	incpxref(object_p);

	return (object_p);
}

//
// release
//
void
ckpt_handler_client::release(CORBA::Object_ptr obj)
{
	lock();
	if (decpxref(obj) == 0) {
		if (!CORBA::is_nil(proxy_obj_) && obj->_this_component_ptr() ==
		    proxy_obj_->_this_component_ptr()) {
			proxy_obj_ = CORBA::Object::_nil();
		}
		delete obj;
		ASSERT(nref > 0);
		if (--nref == 0) {
			if (xdoorp->client_unreferenced(Xdoor::KERNEL_HANDLER,
			    marshcount)) {
				delete this;
				return;
			} else {
				// The xdoor has passed up a reference to this
				// handler as part of an unmarshal. We'd better
				// stick around.
				// The xdoor has accounted for the marshal
				// count we sent down.
				marshcount = 0;
			}
		}
	}
	unlock();
}

//
// Generate a new reference to the implementation object.
//
void
ckpt_handler_client::new_reference()
{
	ASSERT(nref > 0);
	incref();
}

void
ckpt_handler_client::process_oneway_invo(replica::ckpt_seq_t *seqnump,
    replica::ckpt_seq_t *commitp)
{
	if (*seqnump == 0 && *commitp == 0) {
		// It's a dumpstate message.
		*commitp = dump_seq++;

		//
		// Checkpoint Flow control.
		// Wait until all the secondaries
		// have processed the dump messages up to
		// (dump_seq - flush_range).
		//
		replica::ckpt_seq_t flush_to_seq =
			*commitp - multi_ckpt_handler::flush_range;

		// We only flush once every flush_range checkpoints.
		ckpt_info flush_info(dump_seq - ckpt_range,
		    flush_to_seq - multi_ckpt_handler::flush_range);

		if (flush_info >= last_flush_seq) {
			last_flush_seq = flush_to_seq;
			Environment	env;
			objref->flush_to(flush_to_seq, env);
			if (env.exception()) {
				if (CORBA::COMM_FAILURE::
				    _exnarrow(env.exception())) {
					env.clear();
					return;
				} else {
					//
					// SCMSGS
					// @explanation
					// An unexpected return value was
					// encountered when performing an
					// internal operation.
					// @user_action
					// Contact your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					//
					(void) orb_syslog_msgp->log(
					    SC_SYSLOG_PANIC, MESSAGE,
					    "HA: exception %s (major=%d) from "
					    "flush_to().",
					    env.exception()->_name(),
					    env.exception()->_major());
				}
			}
		}
	}
}

//
// invoke -	determines the xdoor.
//		Marshals the method call information.
//		Passes the remote invocation onto the xdoor, and
//		checks for error results.
//
// Put transaction_id and sequence number into the stream
// For now we still get the info about whether it is a checkpoint
// message from the special sequence number. When both seqnum
// and commit are zeros, we know it's not a checkpoint message.
//
// The ckpt handler msg header is defined in the comment prologue
// for method handle_incoming_call. These two methods must completely
// agree on that msg header format.
//
ExceptionStatus
ckpt_handler_client::invoke(arg_info 	&ai,
	ArgValue			*avp,
	InterfaceDescriptor		*infp,
	replica::ckpt_seq_t		seqnum,
	replica::ckpt_seq_t		commit,
	ckpt_handler_client_invoke	*invoke_argsp,
	Environment			&e)
{
	e.check_used("unchecked environment used for new invocation");
	ASSERT(!e.exception());

	os::envchk::check_nonblocking(
	    os::envchk::NB_INTR | os::envchk::NB_INVO |
	    os::envchk::NB_RECONF, "checkpoint invocation");

	invocation_mode invo_mode(ai);

	if (invo_mode.is_oneway()) {
		//
		// Oneway messages are either checkpoint or dumpstate
		// messages. They should be synchronous.
		//
		invo_mode.set_synchronous();
		process_oneway_invo(&seqnum, &commit);
	} else {
		// Twoway messages are admin messages.
		ASSERT(seqnum == 0 && commit == 0);
	}

	//
	// FAULT POINT
	// Generic fault point before the checkpoint is invoked
	// NOTE: This fault point may be called triggered several times
	// during an invocation depending on how many checkpoints are involved
	// i.e.: 2-phase mini transaction has up to 2 checkpoints
	//
	FAULTPT_HA_FRMWK(
		FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_1,
		FaultFunctions::generic);

	resources::flow_policy_t	policy;
	orb_msgtype			msgtype;
	if (invo_mode.is_oneway()) {
		policy = resources::NO_FLOW_CONTROL;
		msgtype = CKPT_MSG;
	} else {
		policy = resources::BASIC_FLOW_CONTROL;
		msgtype = INVO_MSG;
	}

	// Check that the server version of the object supports this method.
	int inf_index = infp->lookup_type_index(ai.id, false);
	if (inf_index < 0) {
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		invocation::exception_return(ai, avp);
		return (CORBA::LOCAL_EXCEPTION);
	}

	ExceptionStatus result;

	bool		retry_needed;

	//
	// I must retry ckpt messages here. Because ckpt messages are sent
	// to different nodes, if one path fails we can't just return to
	// the top and resend to all the nodes.
	//
	do {
		retry_needed = false;

		// Specify resources required for message.
		resources *resourcep = new resources(policy, &e, invo_mode,
		    msgtype);

		//
		// ckpt handler msg header is marshalled as data.
		// Each msg has one transaction id object reference
		// for itself plus one for each piggybacked commit.
		//
		resourcep->add_send_data(18 +
		    (invoke_argsp->number_deletes * 2 *
		    (uint_t)(sizeof (uint32_t))) +
		    ((invoke_argsp->number_commits + 1) * 21));

		// Account for the space required to marshal arguments
		resourcep->add_argsizes(ai, avp);

		//
		// Walk the message protocol layers to reserve resources
		// and obtain a send stream for the invocation.
		//
		invocation invo(xdoorp->reserve_resources(resourcep), &e);

		if (e.exception()) {
			ASSERT(e.sys_exception()->completed() ==
			    CORBA::COMPLETED_NO);
			invocation::exception_return(ai, avp);
			e.mark_used();
			delete resourcep;
			return (CORBA::LOCAL_EXCEPTION);
		}

		//
		// put the sequence number for this checkpoint as well as for
		// the committed checkpoint into invocation sendstream.
		//
		invo.put_unsigned_long(seqnum);
		invo.put_unsigned_long(commit);

		//
		// Put the committed transaction_ids.
		// There is no limit on the length of this list.
		//
		invo.put_unsigned_int(invoke_argsp->number_commits);

		replica::transaction_id_ptr	trid_p;

		for (invoke_argsp->cmt_list.atfirst();
		    is_not_nil(trid_p =
			invoke_argsp->cmt_list.get_current());
		    invoke_argsp->cmt_list.advance()) {

			invo.put_object(trid_p);
		}

		//
		// Put the deleted ha objects ids.
		// There is no limit on the length of this list.
		// Actually encountered case where the list was
		// too long to fit in an unsigned short.
		//
		invo.put_unsigned_int(invoke_argsp->number_deletes);

		delete_elem	*dle;
		for (invoke_argsp->dlt_list.atfirst();
		    (dle = invoke_argsp->dlt_list.get_current()) != NULL;
		    invoke_argsp->dlt_list.advance()) {
			invo.put_unsigned_short(dle->serviceid);
			invo.put_unsigned_int(dle->objectid);
			CKPT_DBG(("send ckpt for svc=%d, oid=%u\n",
			    dle->serviceid, dle->objectid));
		}

		// Load the interface type index and method number.
		put_index_method(invo, infp, inf_index, ai.method);

		primary_ctx	*ctxp = primary_ctx::extract_from(e);

		if (ctxp != NULL) {
			invo.put_object(ctxp->get_trans_id());
		} else {
			invo.put_object(nil);
		}
		// Marshall info related to method to be invoked on other side
		invo.marshal_call(ai, avp);

		if (e.exception()) {
			// exception during marshaling of arguments

			// Unput the committed transaction_ids.
			for (invoke_argsp->cmt_list.atfirst();
			    is_not_nil(trid_p =
			    invoke_argsp->cmt_list.get_current());
			    invoke_argsp->cmt_list.advance()) {

				service::unput_object(trid_p);
			}
			if (ctxp != NULL) {
				service::unput_object(ctxp->get_trans_id());
			} else {
				service::unput_object(nil);
			}
			e.sys_exception()->completed(CORBA::COMPLETED_NO);
			invocation::exception_return(ai, avp);
			e.mark_used();
			delete resourcep;
			return (CORBA::LOCAL_EXCEPTION);
		}

		if (invo_mode.is_oneway()) {
			result = xdoorp->request_oneway(invo);
		} else {
			result = xdoorp->request_twoway(invo);
		}
		//
		// FAULT POINT
		// Inject generic faults during a checkpoint invocation,
		// after we've invoked the checkpoint on the secondary.
		// (And it replied)
		//
		FAULTPT_HA_FRMWK(FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_2,
		    FaultFunctions::generic);

		if (result != CORBA::NO_EXCEPTION &&
		    CORBA::RETRY_NEEDED::_exnarrow(e.exception())) {
			retry_needed = true;
			e.clear();
			delete resourcep;
			continue;
		}
		//
		// For oneways, there is no recstream to process further
		// Any LOCAL_EXCEPTION would have been set in the Environment
		// already
		//
		if (invo_mode.is_oneway()) {
			// return after updating stats
			delete resourcep;
			break;
		}

		if (result != CORBA::LOCAL_EXCEPTION) {
			result = (ExceptionStatus)invo.get_unsigned_long();
		}

		switch (result) {
		case CORBA::NO_EXCEPTION:
			invo.unmarshal_normal_return(ai, avp, &e);
			break;
		case CORBA::USER_EXCEPTION:
			invo.unmarshal_exception_return(ai, avp, &e);
			break;
		case CORBA::SYSTEM_EXCEPTION:
			invo.unmarshal_sys_exception_return(ai, avp,
			    &e);
			// fall-through to LOCAL_EXCEPTION
		case CORBA::LOCAL_EXCEPTION:
			//
			// There must be a system exception in case
			// of LOCAL EXCEPTION
			//
			ASSERT(e.sys_exception());
			invocation::exception_return(ai, avp);
			break;
		case CORBA::UNINITIALIZED:
		case CORBA::RETRY_EXCEPTION:
		case CORBA::REMOTE_EXCEPTION:
		default:
			os::panic("Unknown invocation result status %d",
			    result);
			break;
		}

		delete resourcep;

	} while (retry_needed);

	e.mark_used();
	return (result);
}

//
// When this invoke is called, we are trying to access one secondary
// instead of checkpointing to all the secondaries. We need to set
// special ckpt sequence numbers that isn't possible for real checkpoints
// to let the secondary know that it needs to treat this message
// specially.
//
ExceptionStatus
ckpt_handler_client::invoke(arg_info &ai, ArgValue *avp,
    InterfaceDescriptor *infp, Environment &e)
{
	e.check_used("unchecked environment used for new invocation");

	//
	// Can't go through this path as part of a mini-transaction.
	//
	// Can go through this path as part of add_secondary,
	// in which case we remove the primary_ctx because it should
	// not be marshalled for add_secondary.
	//
	primary_ctx	*primary_ctxp = NULL;
	if (e.trans_ctxp != NULL &&
	    (((trans_ctx *)(e.trans_ctxp))->get_type() == trans_ctx::PRIMARY &&
	    ((primary_ctx *)(e.trans_ctxp))->env_type ==
	    primary_ctx::ADD_SECONDARY_CKPT)) {
		primary_ctxp = (primary_ctx *)(e.trans_ctxp);
		e.trans_ctxp = NULL;
	} else {
		ASSERT(e.trans_ctxp == NULL);
	}

	ckpt_handler_client::ckpt_handler_client_invoke *invoke_argsp = new
	    ckpt_handler_client::ckpt_handler_client_invoke;

	ExceptionStatus	status = invoke(ai, avp, infp, 0, 0, invoke_argsp, e);

	delete invoke_argsp;

	// Restore the context
	e.trans_ctxp = primary_ctxp;

	return (status);
}

//
// Insert an element into the checkpoint list and maintain the list sorted.
//
void
ckpt_list::insert(replica::ckpt_seq_t min_seq, ckpt_elem *le)
{
	ckpt_elem *ce; // temporary element

	ASSERT(le != NULL);
	ckpt_info value_info(min_seq, le->seq);
	atfirst();

	if ((ce = get_current()) == NULL) {
		// empty list.
		prepend(le);
		return;
	}

	if (value_info < ce->seq) {
		ASSERT(value_info != ce->seq);
		prepend(le);
		return;
	}

	while ((ce = (ckpt_elem *)get_current()->next()) != NULL &&
	    value_info > ce->seq) {
		advance();
	}
	insert_a(le);
}

//
// Delete all the checkpoints in the list. Note that here we need to undo
// the unmarshaling that was done by hxdoor.
//
void
ckpt_list::dispose_ckpts()
{
	ckpt_elem *elemp;
	while ((elemp = reapfirst()) != NULL) {
		elemp->dispose();
	}
}

//
// Execute the checkpoint task.
//
void
ckpt_elem::execute()
{
	service serv(re, NULL, &env);
	ckhandler->process(serv);

	CKPT_DBG(("%p exec %u\n", ckhandler, seq));

	//
	// Give ckpt_handler a chance to do some checkpoint related processing.
	// For example, flow control stuff.
	//
	ckhandler->processed_checkpoint(seq);

	delete this;
}

//
// Dispose of the checkpoint task. Note that here we need to undo
// the unmarshaling that was done by hxdoor.
//
void
ckpt_elem::dispose()
{
	// Roll back the translate_in on xdoors.
	ckpt_handler::xdoor_roll_back(re);
	re->done();
	delete this;
}

//
// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.
//

#ifdef NOINLINES
#define	inline
#include <repl/service/ckpt_handler_in.h>
#undef  inline		// in case code is added below that uses inline
#endif  // NOINLINES
