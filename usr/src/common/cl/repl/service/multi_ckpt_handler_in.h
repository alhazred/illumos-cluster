/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  multi_ckpt_handler_in.h
 *
 */

#ifndef _MULTI_CKPT_HANDLER_IN_H
#define	_MULTI_CKPT_HANDLER_IN_H

#pragma ident	"@(#)multi_ckpt_handler_in.h	1.20	08/05/20 SMI"

inline
delete_elem::delete_elem(replica::service_num_t id, xdoor_id o) :
    serviceid(id), objectid(o), _SList::ListElem(this)
{
}

inline
sleep_elem::sleep_elem(replica::ckpt_seq_t s) : _DList::ListElem(this), seq(s)
{
}

inline
ckpt_info::ckpt_info(replica::ckpt_seq_t a, replica::ckpt_seq_t c) :
    minseq(a), value(c)
{
}

inline bool
ckpt_info::operator > (replica::ckpt_seq_t a)
{
	return (((value >= minseq && a >= minseq) ||
	    (value < minseq && a < minseq)) ?
	    (value > a) : (value < a));
}

inline bool
ckpt_info::operator == (replica::ckpt_seq_t a)
{
	return (value == a);
}

inline bool
ckpt_info::operator < (replica::ckpt_seq_t a)
{
	return (!((*this) >= a));
}

inline bool
ckpt_info::operator <= (replica::ckpt_seq_t a)
{
	return ((*this) < a || (*this) == a);
}

inline bool
ckpt_info::operator >= (replica::ckpt_seq_t a)
{
	return ((*this) > a || (*this) == a);
}

inline bool
ckpt_info::operator != (replica::ckpt_seq_t a)
{
	return (!((*this) == a));
}

inline void
multi_ckpt_handler::lock()
{
	lck.lock();
}

inline void
multi_ckpt_handler::unlock()
{
	lck.unlock();
}

inline ckpt_handler_server *
multi_ckpt_handler::get_my_ckpt_handler_svr()
{
	return (my_ckpt_handler_svr);
}

inline void
multi_ckpt_handler::incpxref(CORBA::Object_ptr o)
{
	lock();
	o->_this_component_ptr()->increase();
	unlock();
}

inline uint_t
multi_ckpt_handler::decpxref(CORBA::Object_ptr o)
{
	return (o->_this_component_ptr()->decrease());
}

#endif	/* _MULTI_CKPT_HANDLER_IN_H */
