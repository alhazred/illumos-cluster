/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  repl_service_in.h
 *
 */

#ifndef _REPL_SERVICE_IN_H
#define	_REPL_SERVICE_IN_H

#pragma ident	"@(#)repl_service_in.h	1.36	08/05/20 SMI"

#ifdef _KERNEL_ORB
inline
service_unref::service_unref() : unref_count(0), defer_unref(true)
{
#ifdef DEBUG
	is_primary = false;
#endif
}

inline
service_unref::~service_unref()
{
	ASSERT(unref_count == 0);
	ASSERT(defer_list.empty());
}

#ifdef DEBUG
inline void
service_unref::become_primary()
{
	is_primary = true;
}
#endif

inline void
service_unref::lock()
{
	lck.lock();
}

inline void
service_unref::unlock()
{
	lck.unlock();
}

inline
push_ckpt_task::push_ckpt_task(generic_repl_prov *g) : repl_provp(g)
{
}

inline void
generic_repl_prov::unref_done()
{
	_service_unref.unref_done();
}

inline repl_service *
generic_repl_prov::get_service()
{
	return (_servicep);
}

inline void
generic_repl_prov::set_service(repl_service *rsp)
{
	_servicep = rsp;
}

inline replica::checkpoint_ptr
generic_repl_prov::get_checkpoint()
{
	return (_ckpt_proxy);
}

inline handler *
generic_repl_prov::get_ckpt_handler()
{
	return (&_ckpt_handler);
}

inline void
generic_repl_prov::add_commit(replica::transaction_id_ptr t)
{
	ASSERT(_state_hash.role != state_hash_table_t::SECONDARY);
	_ckpt_handler.add_commit(t);
}

inline void
generic_repl_prov::add_delete(replica::service_num_t sid,
    xdoor_id oid)
{
	ASSERT(_state_hash.role != state_hash_table_t::SECONDARY);
	_ckpt_handler.add_delete(sid, oid);
}

inline void
generic_repl_prov::set_service_id(replica::service_num_t sid)
{
	_service_id = sid;
}

inline replica::service_num_t
generic_repl_prov::get_service_id()
{
	return (_service_id);
}

inline state_hash_table_t *
generic_repl_prov::get_state_hash()
{
	return (&_state_hash);
}

inline replica::repl_prov_ptr
generic_repl_prov::get_implp()
{
	return (_actual_implp);
}

inline generic_repl_prov *
repl_service::get_repl_provp()
{
	return (_repl_provp);
}

#endif // _KERNEL_ORB

inline
repl_service::repl_service(replica::service_num_t sid) :
    _service_id(sid),
#ifdef _KERNEL_ORB
    _repl_provp(NULL),
#endif // _KERNEL_ORB
    _num_handlers(0),
    _is_primary(false),
    flags(0)
{
}

inline void
repl_service::lock()
{
	_lck.lock();
}

inline void
repl_service::unlock()
{
	_lck.unlock();
}

inline bool
repl_service::lock_held()
{
	return (_lck.lock_held());
}

// Answers whether there is anymore handlers in this domain.
inline bool
repl_service::has_no_handler()
{
	ASSERT(lock_held());
	return (_num_handlers == 0);
}

// Add a new handler to the service. Either because it has
// been unmarshaled or it has been created by the primary.
inline void
repl_service::add_handler_locked()
{
	ASSERT(lock_held());
	_num_handlers++;
	CL_PANIC(_num_handlers != 0);	// Check for wraparound
}

//
// Used as part of the freeze_primary() algorithm. Has the effect of
// causing new invocations to hold the freezing_fl freeze lock, instead of
// the invo_fl freeze lock. See comments below for freeze_invos() for an
// explanation of the freeze algorithm.
//
inline void
repl_service::begin_freezing()
{
	invo_fl.block_now();
}

//
// Wait until invocations have drained out and don't allow any more in.
//
// We need to prevent new invocations before (or atomically with) waiting for
// old ones to drain out.  This guarantees that we eventually terminate.  This
// could normally be achieved with a single freeze lock, however it is made
// more complex by services whose methods perform invocations from the primary
// to some other object which, in turn, can't complete until they succeed in
// performing more invocations back into the service. We must avoid a deadlock
// where we have blocked new invocations and wind up preventing such ongoing
// ones from completing.
//
// To acheive this we use two freeze locks in combination with the
// _freeze_in_progress() support. _freeze_in_progress() can be used by services
// to find out if we are currently freezing.
// Services which perform these nested invocatitons must guarantee that they
// don't start new outbound invocations while the service is freezing. Instead
// they should use the _freeze_in_progress() support to defer processing of
// these methods until the service is again unfrozen.
// When the service is not freezing, invocations hold the invo_fl lock. When it
// is freezing, they hold the freezing_fl lock (the begin_freezing() method
// above effects this transition). By performing a freeze on invo_fl first (and
// assuming that service implementations use the _freeze_in_progress()
// functionality correctly) we guarantee that there are no more nested
// invocations in progress when we attempt to freeze freezing_fl. Thus we avoid
// deadlocks.
//
inline void
repl_service::freeze_invos()
{
	invo_fl.freeze();
	freezing_fl.freeze();
}

inline bool
repl_service::invos_frozen()
{
	return (invo_fl.frozen());
}

inline void
repl_service::unfreeze_invos()
{
	invo_fl.unfreeze();
	freezing_fl.unfreeze();
}

// We don't need to lock these, because either can only happen when the
// service is frozen, and it is only checked when an hold has been claimed.

inline void
repl_service::became_primary()
{
	_is_primary = true;
}

inline void
repl_service::became_secondary()
{
	_is_primary = false;
}

//
// The caller needs to make sure that when this function is called
// _is_primary is not changing.
//
inline bool
repl_service::is_primary()
{
	return (_is_primary);
}

inline void
repl_service::invo_done(Environment *e)
{
	if (e->service_is_freezing()) {
		freezing_fl.done();
		e->clear_freezing();
	} else {
		invo_fl.done();
	}
}

inline void
repl_service::wait_for_unfrozen()
{
	// This is called if we are blocked, but potentially not frozen.
	// Calling hold() waits until we are unfrozen.
	invo_fl.hold();
	// Release the hold now that it has served its purpose.
	invo_fl.done();
}

inline replica::service_num_t
repl_service::get_sid()
{
	return (_service_id);
}

inline void
repl_service::mark_registration_done()
{
	flags |= REGISTRATION_DONE;
}

inline void
repl_service::mark_shutting_down(bool is_forced_shutdown)
{
	flags |= SHUTTING_DOWN;
	if (is_forced_shutdown) {
		flags |= FORCED_SHUTTING_DOWN;
	}
}

inline void
repl_service::clear_flags()
{
	flags &= 0x0;
}

inline bool
repl_service::is_registration_done()
{
	return ((flags & REGISTRATION_DONE) != 0);
}

inline bool
repl_service::is_shutting_down()
{
	return ((flags & SHUTTING_DOWN) != 0);
}

inline bool
repl_service::is_forced_shutting_down()
{
	return ((flags & FORCED_SHUTTING_DOWN) != 0);
}

inline void
repl_service_manager::lock()
{
	_lck.lock();
}

inline void
repl_service_manager::unlock()
{
	_lck.unlock();
}

inline bool
repl_service_manager::lock_held()
{
	return (_lck.lock_held());
}

inline repl_service_manager &
repl_service_manager::the()
{
	ASSERT(the_repl_service_manager != NULL);
	return (*the_repl_service_manager);
}

#endif	/* _REPL_SERVICE_IN_H */
