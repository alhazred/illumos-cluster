/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)transaction_state.cc 1.74     08/05/20 SMI"

#include <repl/service/transaction_state.h>
#include <repl/service/repl_service.h>
#include <repl/service/replica_handler.h>
#include <orb/infrastructure/clusterproc.h>

// We need to figure out whether the client is dead. Return
// an exception when we noticed that the client is dead.
//
// When this is a new request, the lookup_state will return after all
// retries are processed.
//
// It's only called from primary_ctx::get_saved_state().
transaction_state *
state_hash_table_t::lookup_state(replica::transaction_id_ptr trid,
    Environment *e)
{
	ASSERT(is_not_nil(trid));

	// Hold this lock to resolve the race condition between
	// transaction_state.get_objref() and delete the transaction_state
	lock();
	// Test whether the client is dead.
	if (trid->_handler()->server_dead()) {
		e->system_exception(CORBA::COMM_FAILURE
		    (0, CORBA::COMPLETED_NO));
		unlock();
		return (NULL);
	}

	transaction_state *st = lookup(trid);
	if (st == NULL) {
		// state is null, a new request. We need to wait till
		// the num_uncmt is zero before proceed.
		while (num_uncmt != 0)
			cv.wait(&lck);
	} else {
		REPL_DBG(("lookup found %p\n", st));

		// a retry

		// We must get a new reference here, otherwise an
		// unreferenced could come in while we are processing
		// the transaction state.
		CL_PANIC(!(st->is_orphaned() || st->is_completed()));
		st->objref = st->get_objref();
		st->set_retried();
	}

	unlock();
	return (st);
}

// Blocks until the hashtable has no more elements in it.
void
state_hash_table_t::wait_till_empty()
{
	lock();
	while (!empty())
		cv.wait(&lck);
	unlock();
}

//
// add_to_hashtable - Add a transaction_state to the hash table.
// Should be called without holding the hashtable lock.
//
void
state_hash_table_t::add_to_hashtable(transaction_state *entry,
    replica::transaction_id_ptr objref)
{
	lock();
	ASSERT(is_secondary());

	// Can't have different states for the same mini-transaction
	CL_PANIC(lookup(objref) == NULL);

	// Add to hash table
	obj_hashtable_t<transaction_state *>::add(entry, objref);
	num_uncmt++;
	CL_PANIC(num_uncmt != 0);	// Check wraparounds
	unlock();
}

// Remove the transaction state from the hash table. Should the hashtable
// becomes empty, we enable unreferenced to replica_handler and wake
// wait_till_empty thread up.
transaction_state *
state_hash_table_t::remove_from_hashtable(replica::transaction_id_ptr tridp)
{
	ASSERT(lck.lock_held());
	// remove from hash table
	transaction_state *st;

	st = obj_hashtable_t<transaction_state *>::remove(tridp);
	CL_PANIC(st != NULL);
	CL_PANIC(st->is_orphaned() || st->is_completed());

	// If the hash table becomes empty, we need to process the unreferences
	// stored in generic_repl_prov defer_list. We also need to
	// wake the wait_till_empty thread up.
	if (empty()) {
		if (role == PRIMARY) {
			repl_provp->all_states_gone();
		}
		cv.signal();
	}
	return (st);
}

// This must be called with the hashtable lock held.
void
state_hash_table_t::st_committed()
{
	ASSERT(lck.lock_held());
	ASSERT(num_uncmt > 0);
	num_uncmt--;

	// There might be some new requests waiting to be processed.
	// This is necessary only for primary.
	if (role == PRIMARY && num_uncmt == 0) {
		cv.broadcast(); // wake up new requests.
	}
}

// This is to be used by the secondary_ctx::begin only. It looks up the
// state object associated with the trans_id in the hash table.
// NULL is returned if the state object is not found.
// If a state is found, get_objref is called on the state object.
transaction_state *
state_hash_table_t::get_state(replica::transaction_id_ptr trans_id)
{
	lock();
	ASSERT(trans_id);
	transaction_state *st = lookup(trans_id);

	// Once a state object is set as orphaned/completed, we will no longer
	// do get_objref on it. The state will wait for a commit
	// or become primary before it goes away.
	if ((st != NULL) && (!st->is_orphaned()) && (!st->is_completed()))
		st->objref = st->get_objref();

	REPL_DBG(("get_state found %p\n", st));
	unlock();
	return (st);
}

// This is node is becoming a primary, so we need to deliver completed/orphaned
// as necessary.
void
state_hash_table_t::become_primary()
{
	REPL_DBG(("become_primary %p\n", this));
	lock();
	role = PRIMARY;
	// We process all the unreferenced states here.
	//
	// There might be some states that have received completed() but
	// haven't gone away because they are waiting for the committed. We
	// know that all those transactions must have processed all the
	// checkpoints (otherwise they couldn't have received completed()),
	// therefore we can directly deliver committed() to them.
	//
	// We don't care about the order of transactions we deliver committed,
	// they couldn't be related to each other.
	//
	iterator iter(this);
	transaction_state *st;
	while ((st = iter.get_current()) != NULL) {
		iter.advance();
		if (st->is_completed()) {
			st->frmwrk_committed();
			repl_provp->add_commit(st->get_trans_id());
			(void) remove_from_hashtable(st->get_trans_id());
			delete st;
		} else if (st->is_orphaned()) {
			orphan_thread::the().add_state(st);
		}
	}
	unlock();
}

// This node is becoming a spare, so we delete all the unreferenced states.
void
state_hash_table_t::become_spare()
{
	REPL_DBG(("become_spare %p\n", this));
	lock();
	role = SPARE;
	iterator iter(this);
	transaction_state *trans_st;
	while ((trans_st = iter.get_current()) != NULL) {
		iter.advance();
		if (trans_st->is_orphaned() || trans_st->is_completed()) {
			CL_PANIC(!trans_st->is_committed());
			// Set it such that we don't trigger assertion in
			// the destructor.
			trans_st->set_commit();

			// Decrement out uncommitted state count.
			st_committed();
			(void) remove_from_hashtable(trans_st->get_trans_id());
			delete trans_st;
		}
	}
	unlock();
}

void
state_hash_table_t::become_secondary()
{
	REPL_DBG(("become_secondary %p\n", this));
	lock();
	role = SECONDARY;
	unlock();
}

transaction_state::~transaction_state()
{
	REPL_DBG(("~transaction_state %p\n", this));
	//
	// Make sure that all the retries that are not committed
	// call locks_recovered.
	// ((is_retried() || is_orphaned()) && !is_committed()) => is_lockrvd()
	//
	CL_PANIC(!((is_retried() || is_orphaned()) && !is_committed()) ||
	    is_lockrvd());
	CORBA::release(trans_id);
	ASSERT(CORBA::is_nil(objref));
	objref = nil;	// for lint
	trans_id = NULL;
	state_hash = NULL;
}

//
// We can't do register_state inside the constructor because right after
// the trans_id->register_state() call the transaction_state can be
// unreferenced.
//
void
transaction_state::register_state(Environment &e)
{
	ASSERT(e.exception() == NULL);

	// Initialize the data structure within transaction_state.
	secondary_ctx	*ctxp = secondary_ctx::extract_from(e);
	ASSERT(ctxp != NULL);
	trans_id = replica::transaction_id::_duplicate(ctxp->get_trans_id());
	ASSERT(is_not_nil(trans_id));
	ASSERT(ctxp->handlerp != NULL);
	state_hash = ctxp->handlerp->get_state_hash();
	state_hash->add_to_hashtable(this, trans_id);

	// Defer a task to do the registration.
	common_threadpool::the().defer_processing(
	    new register_state_task(this));
}

// Tells the framework that as far as this transaction is concerned, it's
// safe to allow other transactions to proceed.
void
transaction_state::locks_recovered()
{
	state_hash->lock();
	ASSERT(!is_lockrvd());
	set_lockrvd();
	if (!is_committed())
		state_hash->st_committed();
	state_hash->unlock();
}

void
transaction_state::committed()
{
}

replica::checkpoint_ptr
transaction_state::get_checkpoint()
{
	return (state_hash->get_repl_prov()->get_checkpoint());
}

void
transaction_state::_unreferenced(unref_t cookie)
{
	bool delete_this = false;

	state_hash->lock();
	if (!_last_unref(cookie)) {
		state_hash->unlock();
		return;
	}

	REPL_DBG(("transaction_state::_unreferenced %p\n", this));
	if (trans_id->_handler()->server_dead() ||
	    (flags & CLIENT_DIED) != 0) {
		set_orphaned();
		REPL_DBG(("orphaned\n"));

		// Only process orphaned() when it's the primary.
		// When a secondary becomes primary it will check
		// it's state hashtable and delivered orphaned()
		// to those marked as ORPHANED.
		if (state_hash->is_primary()) {
			// This transaction may have been replayed
			// on this node (by the new primary). We only
			// want to delivered the orphaned() (which
			// will do a replay again) when we haven't
			// replayed before.
			if (!is_retried() && !is_committed()) {
				// deliver orphaned.
				// We don't remove the transaction
				// state from the hashtable here. It
				// will be removed after the orphaned
				// is processed.
				//
				// It's ok not to remove the state object
				// from the hash table here. Because the
				// client died, the next lookup_state
				// won't try to get_objref.
				orphan_thread::the().add_state(this);

			} else {
				// delete the state object.
				(void) state_hash->remove_from_hashtable(
					trans_id);
				delete_this = true;
			}
		} else if (state_hash->is_secondary()) {
			// On secondary if the transaction has been
			// committed we delete it.
			//
			// Otherwise we won't do anything. Only the
			// primary dealt with the cleanup. The
			// secondaries need to wait till they
			// get commits before they go away.
			//
			// It's ok not to remove the state object
			// from the hash table here. Because the
			// client died, the next lookup_state
			// won't try to get_objref.
			if (is_committed()) {
				(void) state_hash->remove_from_hashtable(
					trans_id);
				delete_this = true;
			}
		} else {
			ASSERT(state_hash->is_spare());
			// If it's not committed, we need to decrement
			// the uncommit count in the hashtable.
			if (!is_committed()) {
				set_commit();
				state_hash->st_committed();
			}
			// Deletes all unreferenced transaction state from
			// spare.
			(void) state_hash->remove_from_hashtable(trans_id);
			delete_this = true;
		}
	} else { // transaction completed on client
		set_completed();

		// If this is the primary, this must be a new primary. In this
		// case we are guaranteed that all the checkpoints are
		// processed and it's safe to deliver the commit.
		if (state_hash->is_primary()) {
			if (!is_committed() && !is_retried()) {
				frmwrk_committed();
				state_hash->get_repl_prov()->
					add_commit(trans_id);
			}
			(void) state_hash->remove_from_hashtable(trans_id);
			delete_this = true;
		} else if (state_hash->is_secondary()) {
			// We wait till we get both committed and unreferenced
			// before we remove the transaction state.
			if (is_committed()) {
				(void) state_hash->remove_from_hashtable(
					trans_id);
				delete_this = true;
			}
		} else {
			ASSERT(state_hash->is_spare());
			// We delete all unreferenced transaction state.
			if (!is_committed()) {
				set_commit();
				state_hash->st_committed();
			}
			(void) state_hash->remove_from_hashtable(trans_id);
			delete_this = true;
		}
	}
	REPL_DBG(("delete_this: %d\n", delete_this));
	state_hash->unlock();
	if (delete_this) delete this;
}


void
transaction_state::client_died(Environment &)
{
	flags |= CLIENT_DIED;
}

register_state_task::register_state_task(transaction_state *s) :
    trans_id(s->get_trans_id()),
    trans_st(s->get_objref())
{
}

void
register_state_task::execute()
{
	REPL_DBG(("register_state %p\n", trans_st));
	Environment e;
	trans_id->register_state(trans_st, e);
	if (e.exception() != NULL) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			e.clear();
		} else {
			CL_PANIC(0);
		}
	}
	CORBA::release(trans_st);
	delete this;
}

orphan_thread *orphan_thread::the_orphan_thread = NULL;

// static
int
orphan_thread::initialize()
{
	ASSERT(the_orphan_thread == NULL);
	the_orphan_thread = new orphan_thread();
	if (the_orphan_thread == NULL)
		return (ENOMEM);
	return (the_orphan_thread->initialize_int());
}

void
orphan_thread::shutdown()
{
	ASSERT(the_orphan_thread == this);
	the_orphan_thread = NULL;
	delete this;
}

orphan_thread::orphan_thread()
{
}

orphan_thread &
orphan_thread::the()
{
	ASSERT(the_orphan_thread != NULL);
	return (*the_orphan_thread);
}

// Create the thread and initialize.
int
orphan_thread::initialize_int()
{
	// register ourselves with the ORB. This involves adding
	// our orbthread structure to the linked list of orb threads.
	registerthread();

#ifdef _KERNEL
	if (clnewlwp((void(*)(void *))orphan_thread::the_thread,
	    (void *)this, 98, NULL, NULL) != 0) {
#else
	if (os::thread::create
		(NULL, (size_t)0, (void *(*)(void *))orphan_thread::the_thread,
		(void *)this, (long)THR_BOUND|THR_DETACHED, NULL)) {
#endif

#ifdef DEBUG
		os::warning("failed to create thread for orphan_thread");
#endif
		(void) unregisterthread();
		return (ENOMEM);
	}
	return (0);
}

// The preallocated thread.
void *
orphan_thread::the_thread(void *arg)
{
	orphan_thread *orphanp = (orphan_thread *)arg;
	orphanp->process_orphaned();
	return (NULL);
}

void
orphan_thread::process_orphaned()
{
	lock();
	transaction_state *st = NULL;
	do {
		while (((st = pending_list.reapfirst()) == NULL) &&
		    (state == orbthread::ACTIVE)) {
			threadcv.wait(&threadLock);
		}

		if (st != NULL) {
			unlock();
			REPL_DBG(("process_orphaned %p\n", st));

			state_hash_table_t *table = st->get_state_hash();
			replica::transaction_id_ptr trid = st->get_trans_id();

			Environment e;
			primary_ctx ctx(NULL, primary_ctx::MINI_TRANSACTION,
			    e);

			ctx.begin_orphaned(trid, st);

			CL_PANIC(!st->is_retried());
			st->orphaned(e);
			// If the service writer didn't call locks_recovered
			// from orphaned, we do it for them.
			if (!st->is_lockrvd()) {
				st->locks_recovered();
			}

			ctx.completed_orphaned(e);

			// Mark this minitransaction as committed. When this
			// commit arrives at the secondary, combined with the
			// unreferenced it will delete the transaction state.
			table->get_repl_prov()->add_commit(trid);

			table->lock();
			(void) table->remove_from_hashtable(trid);
			table->unlock();
			delete st;

			lock();
		}
		// When checking if we are shutting down, we check
		// st instead of flags. This ensures that we deal with
		// all pending orphaned before we go away.
	} while (st != NULL); //lint !e644

	ASSERT(pending_list.empty());

	state = orbthread::DEAD;  // declare ourselves dead

	// We are being shutdown. Let them know we are done.
	threadGone.signal();
	unlock();
}

// Add an orphaned message for the specified transaction state
// to the orphaned list.
void
orphan_thread::add_state(transaction_state *st)
{
	lock();
	if (pending_list.empty())
		threadcv.signal();
	pending_list.append(st);
	unlock();
}
