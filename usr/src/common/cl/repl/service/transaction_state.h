/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TRANSACTION_STATE_H
#define	_TRANSACTION_STATE_H

#pragma ident	"@(#)transaction_state.h	1.52	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/replica.h>
#include <sys/hashtable.h>
#include <orb/infrastructure/orbthreads.h>

class generic_repl_prov;
class multi_ckpt_handler;
class ckpt_handler_server;
class transaction_state;
class secondary_ctx;

// state_hash_table_t has a lock, so does the transaction_state. The locking
// order is state_hash_table_t > transaction_state

// the hash table to store the secondary state objects. State_hash_table should
// only be accessed from ha framework objects. It shall never be accessed by
// service implementation.
class state_hash_table_t : public obj_hashtable_t<transaction_state *> {
#ifdef DEBUG
	friend class generic_repl_prov;
#endif
public:
	state_hash_table_t(generic_repl_prov *g);

	~state_hash_table_t();

	generic_repl_prov *get_repl_prov();

	void lock();
	void unlock();

	// If the client is dead when lookup_state is called, return
	// a NULL transaction id and an exception in environment.
	transaction_state *lookup_state(replica::transaction_id_ptr,
					Environment *);

	// Inform hashtable that one more transaction state is committed.
	void st_committed();

	// add/remove to/from the hash table
	// Do not use add/remove on the obj_hash_table_t directly
	// lookup picks up obj_hash_table_t::lookup
	void add_to_hashtable(transaction_state *, replica::transaction_id_ptr);
	transaction_state *remove_from_hashtable(replica::transaction_id_ptr);

	// Blocks until the hashtable has no more elements in it.
	void wait_till_empty();

	// This is to be used by the secondary_ctx::begin only. It looks up the
	// state object associated with the trans_id in the hash table.
	// NULL is returned if the state object is not found.
	// If a state is found, get_objref is called on the state object.
	transaction_state *get_state(replica::transaction_id_ptr);

	// Undo the get_objref in get_state().
	void get_state_done(transaction_state *st);

	// Used by generic_repl_prov during become_primary call.
	void become_primary();

	// Used by generic_repl_prov during become_spare call. It deletes all
	// the unreferenced transaction_states from the hashtable.
	void become_spare();

	// Called by generic_repl_prov during become_secondary call. It
	// sets the role to secondary.
	void become_secondary();

	// Tells transaction_state about our current role.
	bool is_primary();
	bool is_secondary();
	bool is_spare();
private:
	// The number of transaction states that are uncommitted.
	uint_t num_uncmt;

	// The current role of this replica.
	char role;
	enum { PRIMARY, SECONDARY, SPARE };

	generic_repl_prov *repl_provp;
	os::mutex_t lck;
	os::condvar_t cv;

	// Disallow direct add/remove from obj_hashtable_t without
	// going through this class
	void add(transaction_state *, replica::transaction_id_ptr);
	transaction_state *remove(replica::transaction_id_ptr);

	// Disallow assignments and pass by value
	state_hash_table_t(const state_hash_table_t &);
	state_hash_table_t &operator = (state_hash_table_t &);
};

// Base class for secondary state objects.
class transaction_state : public McServerof<replica::trans_state>
{
	friend class state_hash_table_t;
	friend class ckpt_handler_server;
	friend class standard_handler_server;
	friend class generic_repl_prov;
	friend class primary_ctx;
	friend class orphan_thread;
	friend class multi_ckpt_handler;
	friend class register_state_task;
public:
	transaction_state();
	virtual ~transaction_state();

	// register this state object with the client. The environment passed
	// shouldn't contain any exception. The environment passed out will
	// never contain any exception (cleared before return).
	void register_state(Environment &);

	// This should be called during a retry, after all the locks
	// that this transaction needs are obtained.
	void locks_recovered();

	// Answers whether a committed has been received.
	bool is_committed();

	// orphaned is called on a primary when the client died.
	// We need to recover our local state into a consistent state.
	virtual void orphaned(Environment &) = 0;

	// committed() is called to commit the changes recorded in the
	// state object.
	virtual void committed();

	// Returns the checkpoint interface. This helps service to send
	// checkpoint from inside orphaned().
	replica::checkpoint_ptr get_checkpoint();

	// Idl method. Used by the framework. It's called when the client
	// is a usrland program and that program dies.
	virtual void	client_died(Environment &);
private:
	// called by the framework to commit the transaction and inform
	// the state hashtable of it. This is called with the hashtable
	// lock held.
	void frmwrk_committed();

	// Called by replica_handler after it has completed a invocation (retry)
	// that uses this transaction state.
	void invoke_completed();

	replica::transaction_id_ptr get_trans_id();

	state_hash_table_t *get_state_hash();

	// it figures out whether the client completed request or died
	void _unreferenced(unref_t);

	bool is_orphaned();
	bool is_completed();
	bool is_retried();
	bool is_lockrvd();

	void set_orphaned();
	void set_retried();
	void set_commit();
	void set_completed();
	void set_lockrvd();

	// The object reference for this transaction_state. It is initialized
	// to this->get_objref() when the first reference is passed out.
	CORBA::Object_ptr objref;

	replica::transaction_id_ptr trans_id;

	// Backpointer to the hashtable
	state_hash_table_t *state_hash;

	uint_t flags;
	enum { COMMITTED = 0x1,	// transaction is committed.
	    RETRIED = 0x2,	// transaction has been retried.
	    ORPHANED = 0x4,	// transaction is orphaned.
	    COMPLETED = 0x8,	// transaction is completed.
	    LOCK_RVD = 0x10,	// all the locks are recovered.
	    CLIENT_DIED	= 0x20	// The client is dead. It'll move to
				// orphaned state when _unreferenced comes.
	};

	// Disable assignment and pass by value
	transaction_state(const transaction_state &);
	transaction_state &operator = (transaction_state &);
};

// The task that registers a transaction state with the client tid.
class register_state_task : public defer_task {
public:
	register_state_task(transaction_state *);
	void execute();

private:
	replica::transaction_id_ptr trans_id;
	replica::trans_state_ptr trans_st;
};

// The thread that handles orphaned. We need this thread because orphaned
// could takes some time, and we don't want to block unreference thread
// to do the work. There is one such thread per node. It's initialized in
// ORB::initialize().
class orphan_thread : public orbthread {
public:
	static orphan_thread &the();

	orphan_thread();

	static int initialize();
	void shutdown();

	// Start the thread. Returns zero for success and error code if we
	// fails to create a new thread.
	int initialize_int();

	// Add_state() adds the specified state to be orphaned by orphan thread
	void add_state(transaction_state *);
private:
	// The list of state objects to be orphaned.
	SList<transaction_state> pending_list;

	// The preallocated thread
	static void *the_thread(void *arg);

	// Process orphaned().
	void process_orphaned();

	static orphan_thread *the_orphan_thread;

	// Disable assignment and pass by value
	orphan_thread(const orphan_thread &);
	orphan_thread &operator = (orphan_thread &);
};

#include <repl/service/transaction_state_in.h>

#endif	/* _TRANSACTION_STATE_H */
