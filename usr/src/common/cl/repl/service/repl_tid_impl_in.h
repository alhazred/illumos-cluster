/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  repl_tid_impl_in.h
 *
 */

#ifndef _REPL_TID_IMPL_IN_H
#define	_REPL_TID_IMPL_IN_H

#pragma ident	"@(#)repl_tid_impl_in.h	1.15	08/05/20 SMI"

//
// repl_tid_impl_in.h 	The inline functions for repl_tid_impl.
//

inline void
kernel_tid_factory::remove(ha_transaction_id *tid)
{
	lock();
	(void) tid_list.erase(tid);
	try_delete();
}

inline
kernel_tid_factory::kernel_tid_factory() :
    is_unref(false)
{
}

inline void
kernel_tid_factory::lock()
{
	lck.lock();
}

inline void
kernel_tid_factory::unlock()
{
	lck.unlock();
}

inline
secondary_state_elem::secondary_state_elem(replica::trans_state_ptr objp) :
    objp_(replica::trans_state::_duplicate(objp)), _SList::ListElem(this)
{
}

inline
secondary_state_elem::~secondary_state_elem()
{
	CORBA::release(objp_);
}

inline
ha_transaction_id::ha_transaction_id(kernel_tid_factory *k) :
    factoryp(k),
    done(false)
{
}

inline
ha_transaction_id::~ha_transaction_id()
{
	CL_PANIC(secondary_state_list.empty());
	ASSERT(factoryp == NULL);
}

inline
client_died_task::client_died_task(ha_transaction_id *t) :
    tidp(t),
    tid_ref(t->get_objref())
{
}

inline void
client_died_task::execute()
{
	tidp->broadcast_client_death();
	CORBA::release(tid_ref);
}

#endif	/* _REPL_TID_IMPL_IN_H */
