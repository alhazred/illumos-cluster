//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)replica_tmpl.cc	1.34	08/05/20 SMI"

#include <sys/os.h>
#include <repl/service/replica_tmpl.h>
#include <errno.h>
#include <repl/service/replica_handler.h>

#ifdef _KERNEL_ORB
#include <repl/rma/rma.h>
#endif

//
// class mc_checkpoint methods
//

template<class T>
mc_checkpoint<T>::~mc_checkpoint()
{
}

template<class T>
handler	*
mc_checkpoint<T>::_handler()
{
	return (&handler_);
}

template<class T>
void
mc_checkpoint<T>::initialize_seq(replica::ckpt_seq_t seq, Environment &)
{
	handler_.initialize_seq(seq);
}

template<class T>
void
mc_checkpoint<T>::get_high(replica::ckpt_seq_t &seq, Environment &)
{
	seq = handler_.get_high_seq();
}

template<class T>
void
mc_checkpoint<T>::process_to(replica::ckpt_seq_t seq, Environment &)
{
	handler_.process_to(seq);
}

template<class T>
void
mc_checkpoint<T>::flush_to(replica::ckpt_seq_t seq, Environment &)
{
	handler_.flush_to(seq);
}

template<class T>
void
mc_checkpoint<T>::stop_receiving_ckpt(Environment &)
{
	handler_.stop_receiving_ckpt();
}

template<class T>
void
mc_checkpoint<T>::ckpt_noop(Environment &)
{
}

template<class T>
void
mc_checkpoint<T>::start_receiving_ckpt(Environment &)
{
	handler_.start_receiving_ckpt();
}

//
// class mc_replica_of methods
//

//
// Used by replication-aware code on the primary when it creates
// new objects
//
template<class T>
mc_replica_of<T>::mc_replica_of(generic_repl_server *grsp)
{
	// Allocate a new handler.
	_handlerp = new replica_handler(grsp->get_service(), _this_obj());

	ASSERT(_get_service()->is_primary());
}

template<class T>
mc_replica_of<T>::~mc_replica_of()
{
	_handlerp->rem_impl();
}

//
// Returns true if the service is currently being frozen.
// For use by methods that want to force a retry if a freeze is in progress.
//
template<class T>
bool
mc_replica_of<T>::_freeze_in_progress(Environment *e)
{
	return (_handlerp->freeze_in_progress(e));
}

//
// class generic_repl_server methods
//

generic_repl_server::generic_repl_server() :
	_my_repl_prov(NULL)
{
}

generic_repl_server::~generic_repl_server()
{
	delete _my_repl_prov;
	_my_repl_prov = NULL;
}

//
// Called on a spare provider to shut it down. This is to serve
// as an additional event to help the service to do clean up.
// Normally ha services shouldn't need to do anything in this
// function, instead they should be able to clean everything
// up in the become_spare function and spares shouldn't have
// anything more to clean.
//
void
generic_repl_server::shutdown_spare(replica::repl_prov_shutdown_type,
    Environment &)
{
}

//
// Returns a Solaris error code. If the HA service does not support
// forced shutdown, Solaris error code ENOTSUP is returned.
//
uint32_t
generic_repl_server::forced_shutdown(Environment &)
{
	//
	// Get the service name.
	// Memory for the service name is allocated on the heap.
	// We need to free the memory before returning.
	//
	char *service_name = _my_repl_prov->get_service_name();

	REPL_DBG(("ERROR : HA service %s (service id %d) does not implement "
	    "forced_shutdown()\n", service_name,
	    _my_repl_prov->get_service_id()));

	delete [] service_name;
	return (ENOTSUP);
}

// Only used by constructor of mc_replica_of objects.
repl_service *
generic_repl_server::get_service()
{
	return (_my_repl_prov->get_service());
}

replica::checkpoint_ptr
generic_repl_server::set_checkpoint(CORBA::type_info_t *tp)
{
	// Actually, this will be done before this call.
	// First, get the Interface Descriptor for this version.
	// CORBA::type_info_t *tid = CKPT::_interface_descriptor()->
	//    get_type_info(version);

	// Now, create the proxy object.
	CORBA::Object_ptr p = CORBA::create_proxy_obj(tp,
	    _my_repl_prov->get_ckpt_handler());

	//
	// Finally, associate this proxy with the handler.
	// Note! This cast is relying on proxy<CKPT> inheriting from
	// CKPT and CKPT inheriting from replica::checkpoint.
	//
	if (!_my_repl_prov->set_checkpoint((replica::checkpoint_ptr)p)) {
		CORBA::release(p);
		p = CORBA::Object::_nil();
	}

	return ((replica::checkpoint_ptr)p);
}

//
// class repl_server methods
//

template <class CKPT>
repl_server<CKPT>::repl_server(const char *svc_id, const char *prov_id) :
	count_unref(0)
{
	_my_repl_prov = new generic_repl_prov(get_prov()->get_objref(),
	(replica::checkpoint_ptr)(get_ckpt()->get_objref()),
	(ckpt_handler_server *) get_ckpt()->_handler(),
	svc_id, prov_id);

	SERV_DBG(("Created generic repl_prov %p\n", _my_repl_prov));

	prov_info(_my_repl_prov);
}

template<class CKPT>
repl_server<CKPT>::~repl_server()
{
}

//
// We give one reference of each interface to generic_repl_prov,
// which will release them when it's ready to be deleted.
// We must wait till we get the _unreferenced from both interfaces
// before we delete this.
// There are no other references going around.
//
template<class CKPT>
void
#ifdef DEBUG
repl_server<CKPT>::_unreferenced(unref_t cookie)
#else
repl_server<CKPT>::_unreferenced(unref_t)
#endif
{
#ifdef DEBUG
	if ((handler *)cookie ==
	    mc_checkpoint<CKPT>::_handler()) {
		//
		// unreference notification for mc_checkpoint interface
		//
		ASSERT(((ckpt_handler_server *)
		    (mc_checkpoint<CKPT>::_handler()))->last_unref());
	} else {
		//
		// unreference notification for repl_prov interface
		//
		ASSERT((handler *)cookie ==
		    McServerof<replica::repl_prov>::_handler());

		ASSERT(((standard_handler_server *)
		    (McServerof<replica::repl_prov>::_handler()))->
		    last_unref());
	}
#endif	// DEBUG
	if (os::atomic_add_16_nv(&count_unref, 1) == 2) {
		//
		// Only delete the object one time after
		// receiving both unref notices.
		//
		delete this;
		return;
	}
}
