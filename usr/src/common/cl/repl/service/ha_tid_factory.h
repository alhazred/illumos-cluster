/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  ha_tid_factory.h
 *
 */

#ifndef _HA_TID_FACTORY_H
#define	_HA_TID_FACTORY_H

#pragma ident	"@(#)ha_tid_factory.h	1.5	08/05/20 SMI"

#include <h/replica.h>
#include <h/replica_int.h>
#include <orb/object/adapter.h>

// The factory object for ha_transaction_id. In the kernel it'll create
// ha_transaction_id objects. In the usrland it'll ask for an object
// from the local rma.
class ha_tid_factory {
public:
	ha_tid_factory();
	~ha_tid_factory();

	// Called during ORB::initialize and ORB::shutdown.
	static void initialize();
	static void shutdown();

	static ha_tid_factory &the();
	replica::transaction_id_ptr get_tid();

private:
	static ha_tid_factory *the_factory;
#ifndef _KERNEL_ORB
	// Every usrland factory has a kernel factory that creates tid
	// for it.
	replica_int::tid_factory_ptr kernel_factory;

	// protects the kernel_factory.
	os::mutex_t lck;
#endif // _KERNEL_ORB
};

#endif	/* _HA_TID_FACTORY_H */
