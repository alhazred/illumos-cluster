/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rm_util.cc	1.15	08/05/20 SMI"

#include <sys/rm_util.h>
#include <nslib/ns.h>
#include <sys/os.h>

//
// External interface for cl_orb
//
extern "C" replica::rm_admin_ptr
get_rm()
{
	return (rm_util::get_rm());
}
// Library function to return a reference to the RM from the local nameserver
replica::rm_admin_ptr
rm_util::get_rm()
{
	Environment e;
	replica::rm_admin_ptr	the_rm = nil;

	CORBA::Object_var rm_obj = ns::wait_resolve_local("repl_mgr", e);
	ASSERT(!e.exception() && !CORBA::is_nil(rm_obj));
	the_rm = replica::rm_admin::_narrow(rm_obj);
	return (replica::rm_admin::_duplicate(the_rm));
}

// Library function to return a reference to the rma admin object from
// the local nameserver
replica_int::rma_admin_ptr
rm_util::get_rma_admin()
{
	Environment e;
	static replica_int::rma_admin_ptr	the_rma_admin = nil;

	if (!CORBA::is_nil(the_rma_admin))
		return (replica_int::rma_admin::_duplicate(the_rma_admin));

	CORBA::Object_var rma_obj = ns::wait_resolve_local("rma_admin", e);
	ASSERT(!e.exception() && !CORBA::is_nil(rma_obj));
	the_rma_admin = replica_int::rma_admin::_narrow(rma_obj);
	return (replica_int::rma_admin::_duplicate(the_rma_admin));
}
