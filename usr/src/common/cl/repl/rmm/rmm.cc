//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rmm.cc	1.73	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <repl/rmm/rmm.h>
#include <sys/rm_util.h>
#include <nslib/ns.h>

#ifdef _KERNEL
#include <sys/uadmin.h>
#include <sys/vfs.h>
#include <sys/dumphdr.h>
#else
#include <unode/unode.h>
#endif // _KERNEL

#ifdef	RMM_DBGBUF
uint32_t rmm_dbg_size = 10000;
dbg_print_buf rmm_dbg(rmm_dbg_size);
#endif

rmm 		*rmm::the_rmm = NULL;
os::mutex_t	rmm_lock::lck;

// Rank compare function:  Returns true if rank a is larger, false
// otherwise.  The primary is greater than a secondary, and secondaries
// are greater than spares.  If the provider type is equal, node_order::cmp
// is used to break the tie.
bool
rmm::first_rank_is_greater(const replica_int::rmm_prov_rank &a,
	const replica_int::rmm_prov_rank &b)
{
	if (a.state == b.state) {
#ifdef	DEBUG
		if (a.state == replica_int::RMM_PROV_PRIMARY) {
			ASSERT(node_order::cmp(a.nodeid, b.nodeid) == 0);
		}
#endif
		return (node_order::cmp(a.nodeid, b.nodeid) == 1);
	} else {
		switch (b.state) {
		case replica_int::RMM_PROV_PRIMARY:
			return (false);
		case replica_int::RMM_PROV_SECONDARY:
			return (a.state == replica_int::RMM_PROV_PRIMARY);
		case replica_int::RMM_PROV_SPARE:
			return (true);
		case replica_int::RMM_PROV_UNKNOWN:
			CL_PANIC(0);
		}
	}
	// NOTREACHED
	CL_PANIC(0);
	return (false);
}

// Static function called to allocate the rmm.  Passes the provider and
// checkpoint object into the constructor
int
rmm::initialize(replica_int::handler_repl_prov_ptr provp,
	replica::checkpoint_ptr ckpt, const char *name)
{
	CL_PANIC(the_rmm == NULL);
	the_rmm = new rmm(provp, ckpt, name);
	ASSERT(the_rmm != NULL);
	return (0);
}

void
rmm::shutdown() const
{
	CL_PANIC(the_rmm != NULL);
	delete the_rmm;
	the_rmm = NULL;
}

// The rmm constructor adds the service to the rma, stores the provider,
// checkpoint, and rma::reconf objects, and wraps the handler.  The provider
// is assumed to have been initialized with the RMM_SERVICE_ID at the
// service level.
rmm::rmm(replica_int::handler_repl_prov_ptr provp,
    replica::checkpoint_ptr ckpt, const char *name) :
	McNoref<replica_int::rmm>(XDOOR_RMM),
	primary_prov(nil),
	last_mbrshp(lone_membership::the()),
	primary_local(false),
	service_up(false),
	promote_task_deferred(false),
	run_promote_task(false),
	disqualification_running(false),
	sec_name_seq(NULL),
	sn_mbrs(NULL)
{
	Environment e;
	service_name = new char [os::strlen(name) + 1];
	(void) os::strcpy(service_name, name);

	rma::the().lock();
	hxsr = rma::the().add_service(RMM_SERVICE_ID, name);
	ASSERT(hxsr != NULL);
	hxsr->freeze_service_invos();
	rma::the().unlock();

	lnid = orb_conf::node_number();
	last_mbrshp->hold();

	current[lnid].reconf = rma::the().get_reconf();
	current[lnid].ckpt = replica::checkpoint::_duplicate(ckpt);
	current[lnid].state = replica_int::RMM_PROV_UNKNOWN;

	replica_int::rma_admin_var rav = rm_util::get_rma_admin();
	current[lnid].prov = rav->wrap_handler_repl_prov(provp,
	    RMM_SERVICE_ID, e);

	CL_PANIC(!e.exception());

	RMM_DBG(("(%p) rmm::rmm finished\n", this));
} //lint !e1744 class members not initialized

rmm::~rmm()
{
	RMM_DBG(("(%p) rmm::~rmm called\n", this));

	reconfigure_lock.lock(rmm_lock::CMM_RECONFIG);
	delete [] service_name;
	service_name = NULL;

	// XXX free other objects (in current)
}

// As long as this function is called after step 4 of the orb
// reconfiguration (when the cluster has reached "joined" status)
// The primary_prov variable should always be set to a valid
// primary provider object when the reconfigure lock is held
// (the primary may have died, but invocations will return an
// exception rather than causing an internal error).
// The return value indicates success or failure of the registration.
bool
rmm::register_provider()
{
	Environment e;
	CORBA::Object_var rm_obj;
	naming::naming_context_var ctxp_l = ns::local_nameserver();
	replica_int::rma_repl_prov_var	tmp_primary;

	reconfigure_lock.lock(rmm_lock::OTHER);
	tmp_primary = replica_int::rma_repl_prov::_duplicate(primary_prov);
	ASSERT(!CORBA::is_nil(tmp_primary));
	reconfigure_lock.unlock(rmm_lock::OTHER);
	rm_obj = tmp_primary->get_root_obj(e);
	if (e.exception() != NULL) {
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			e.clear();
			return (false);
		} else {
			CL_PANIC(0);
		}
	} else {
		ctxp_l->rebind(service_name, rm_obj, e);
		CL_PANIC(!e.exception());
	}
	return (true);
}

#ifdef	_FAULT_INJECTION

// This not be called before the first reconfiguration.  It returns the
// node that is hosting the primary.  If called during the first steps of
// reconfiguration, it could return a nodeid for a node that has crashed.
// Only available for fault injection.

//
// External interface for cl_orb
//
extern "C" nodeid_t
rmm_primary_node()
{
	return (rmm::the().primary_node());
}

nodeid_t
rmm::primary_node()
{
	reconfigure_lock.lock(rmm_lock::OTHER);

	nodeid_t the_primary_nodeid = candidate_rank.nodeid;

	reconfigure_lock.unlock(rmm_lock::OTHER);

	return (the_primary_nodeid);
}

#endif	// _FAULT_INJECTION

// The rmm_lock is a non-thread-specific mutex.  It is used to lock the
// reconfiguration process and the candidate.  A normal mutex would not
// work because in some cases we are not guaranteed the same thread will
// be used to lock that was used to unlock.

rmm_lock::rmm_lock() : owner(NO_OWNER)
{
}

void
rmm_lock::lock(lock_owner lo)
{
	lck.lock();
	while (owner != NO_OWNER)
		cv.wait(&lck);
	owner = lo;
	lck.unlock();
}

void
#ifdef DEBUG
rmm_lock::unlock(lock_owner lo)
#else
rmm_lock::unlock(lock_owner)
#endif
{
	lck.lock();
	ASSERT(owner == lo);
	owner = NO_OWNER;
	cv.signal();
	lck.unlock();
}

void
rmm_lock::lock_if_not_owned(lock_owner lo)
{
	lck.lock();
	if (owner == lo) {
		lck.unlock();
		return;
	}
	while (owner != NO_OWNER)
		cv.wait(&lck);
	owner = lo;
	lck.unlock();
}

bool
rmm_lock::is_owned_by(lock_owner lo) const
{
	bool ret_val;
	lck.lock();
	ret_val = (lo == owner);
	lck.unlock();
	return (ret_val);
}

// Convert the state supplied by rma_repl_prov::get_state to the state
// used by the rmm.  We should always find either a spare or a secondary
// (if it's a primary, we shouldn't be asking with this function, and it
// will ASSERT()).
#ifdef DEBUG
replica_int::rmm_prov_state
rmm::rma_to_rmm_state(replica_int::rma_repl_prov_state rmm_state,
	replica_int::repl_prov_exception rpe) const
#else
replica_int::rmm_prov_state
rmm::rma_to_rmm_state(replica_int::rma_repl_prov_state rmm_state,
	replica_int::repl_prov_exception) const
#endif
{
	switch (rmm_state) {
	case replica_int::RMA_REMOVE_SECONDARY:
	case replica_int::RMA_BECOME_SECONDARY:
	case replica_int::RMA_BECOME_SECONDARY_WRAPUP:
	case replica_int::RMA_BECOME_PRIMARY_PREPARE:
	case replica_int::RMA_SHUTDOWN:
		CL_PANIC(0);
		break;
	case replica_int::RMA_CONSTRUCTED:
	case replica_int::RMA_BECOME_SPARE:
		ASSERT(rpe == replica_int::PROV_NONE);
		return (replica_int::RMM_PROV_SPARE);
	case replica_int::RMA_FREEZE_PRIMARY:
	case replica_int::RMA_UNFREEZE_PRIMARY:
	case replica_int::RMA_BECOME_PRIMARY:
		ASSERT(rpe == replica_int::PROV_NONE);
		break;
	case replica_int::RMA_ADD_SECONDARY:
		// If it's already primary, we should never be asking
		CL_PANIC(0);
		return (replica_int::RMM_PROV_PRIMARY);
	case replica_int::RMA_COMMIT_SECONDARY:
		ASSERT(rpe == replica_int::PROV_NONE);
		return (replica_int::RMM_PROV_SECONDARY);
	default:
		CL_PANIC(0);
		break;
	}
	// NOTREACHED
	CL_PANIC(0);
	return (replica_int::RMM_PROV_UNKNOWN);
}

// If a node will always be disqualified, it doesn't need to actively
// participate in disqualification.  A node is guaranteed to be
// disqualified if it has a spare provider and there is a higher ranked
// node in the current membership.
bool
rmm::always_disqualified(const cmm::membership_t &mbrshp,
    replica_int::rmm_prov_rank &rank) const
{
	node_order	n(rank.nodeid);
	CL_PANIC(n.valid());
	if (rank.state == replica_int::RMM_PROV_SPARE)
		for (n.higher(); n.valid(); n.higher())
			if (mbrshp.members[n.current()] != INCN_UNKNOWN)
				return (true);

	return (false);
}

// Disqualify the providers of joining cluster nodes and collect
// references to their reconf and provider objects by exchanging state
// with them.
bool
rmm::disqualify_node(nodeid_t n, incarnation_num incn,
	replica_int::rmm_prov_rank &out_rank)
{
	replica_int::rmm_ptr rrmm;
	Environment e;

	if (n == orb_conf::node_number()) {
		return (true);
	}

	// Release any old references and clear fields in the reference
	// object for the node.
	current[n].reset();

	// Rather than check the membership in the loop, it is checked here.
	// that allows us to automatically clear out the current[n] object
	// if a node has become unknown.
	if (incn == INCN_UNKNOWN) {
		return (true);
	}
	// Only take note if the node is in the cluster.
	RMM_DBG(("rmm::disqualify_node(%d) incn = %d\n", n, incn));

	// XXX We will need to use version manager to know what version
	// of the interface to construct the Anchor for.
	rrmm = new Anchor<replica_int::rmm_stub> (n, XDOOR_RMM,
	    replica_int::rmm::_interface_descriptor());
	ASSERT(rrmm);

	rrmm->exchange_state(current[lnid].prov, local_rank,
	    current[n].prov, out_rank, current[n].reconf,
	    current[n].ckpt, start_frozen, sec_nodeset.contains(n), e);
	CORBA::release(rrmm);

	//
	// Return false if there was an error (which would mean the node
	// has died).
	//
	if (e.exception()) {
		return (false);
	} else {
		RMM_DBG(("rmm::disqualify_node(%d) successful "
		    " reconf(%d) = %p\n", n, n, current[n].reconf));
		current[n].state = out_rank.state;
		return (true);
	}
}

// Called from step 4.  The candidate lock protects the disqualification
// candidate provider, and is dropped in disqualification (step 5).  The
// reconfigure lock is used to lock the data used by the spare promotion
// task, and is dropped in service_unfreeze (step 8).  If the reconfiguration
// is re-started, we hold onto the reconfigure lock during the rest of the
// reconfiguation, so that register_provider and the spare promotion task
// don't see intermediate rmm state.  The candidate lock is dropped by
// disqualification, or by reconfig_prepare_return if there is a return
// transition after step 4.
void
rmm::reconfig_prepare()
{
	CL_PANIC(!candidate_lock.is_owned_by(rmm_lock::CMM_RECONFIG));
	candidate_lock.lock(rmm_lock::CMM_RECONFIG);
	CL_PANIC(!disqualification_running);

	reconfigure_lock.lock_if_not_owned(rmm_lock::CMM_RECONFIG);
}

void
rmm::reconfig_prepare_return()
{
	candidate_lock.unlock(rmm_lock::CMM_RECONFIG);
}

// disqualification is a distributed algorithm used to supply joining nodes
// with the current primary provider (if there is one), or choose a new
// primary (if there isn't).  In addition, the node that hosts the primary
// provider retrieves references for the provider, checkpoint and reconf
// objects on every other node.  These are used to provide the primary with
// the current list of secondaries, and to add new spares as secondaries in
// the spare promotion thread.
bool
rmm::disqualification(callback_membership *mbrshp)
{
	Environment			e;
	node_order			no;
	nodeid_t			n;
	replica_int::rmm_prov_rank	out_rank;
	uint_t 				secondary_count = 0;

	RMM_DBG(("rmm::disqualification starting "
	    "incn = %d\n", mbrshp->membership().members[lnid]));

	if (primary_local || (!CORBA::is_nil(primary_prov) &&
	    !primary_prov->_handler()->server_dead())) {
		// We have a valid primary, and therefore a valid
		// candidate, so we release the candidate lock
		// to keep incoming exchange_state calls from blocking,
		// and then have the node hosting the primary call each
		// joining node to disqualify it and get its state.

		disqualification_running = true;
		candidate_lock.unlock(rmm_lock::CMM_RECONFIG);
		RMM_DBG(("rmm::d14n found valid primary\n"));

		// There's no work to do unless this node hosts the primary,
		// in which case it needs to disqualify all joining nodes.
		if (primary_local) {
			CL_PANIC(local_rank.state ==
			    replica_int::RMM_PROV_PRIMARY);
#ifdef	DEBUG
			if (CORBA::is_nil(primary_prov)) {
				CL_PANIC(!connected_to_primary);
			}
			if (!sec_nodeset.is_empty()) {
				CL_PANIC(sn_mbrs != NULL);
			}
#endif	// DEBUG
			RMM_DBG(("rmm::d14n primary hosted here\n"));
			for (no.highest(); no.valid(); no.lower()) {
				n = no.current();
				CL_PANIC(last_mbrshp != NULL);

				// We return if there is an error.  Errors
				// can only occur if a node has died, in
				// which case we are already reconfiguring.
				//
				// Note that we don't store the new
				// membership if we fail during the
				// disqualification process.  This means
				// That we might disqualify nodes twice in
				// subsequent reconfigurations.  This is fine.
				// If we never called become_primary, but
				// the primary is local, we know that we won
				// the last disqualification and called
				// become_primary_prepare, but there might
				// be nodes that didn't connect, so we
				// re-disqualify all nodes to ensure we are
				// once again chosen by all nodes as primary.
				if (!became_primary ||
				    (mbrshp->membership().members[n] !=
				    last_mbrshp->membership().members[n])) {
					// See if disqual is canceled.
					candidate_lock.lock(
					    rmm_lock::CMM_EXAMINE);
					if (!disqualification_running) {
						candidate_lock.unlock(
						    rmm_lock::CMM_EXAMINE);
						return (true);
					}
					candidate_lock.unlock(
					    rmm_lock::CMM_EXAMINE);

					// If a different incarnation of a
					// node is joining, and we were adding
					// the old node's spare as a secondary
					// in the first become_primary, we need
					// to remove it now so its spare will
					// be promoted later.
					if (sec_nodeset.contains(n) &&
					    (mbrshp->membership().members[n] !=
					    sn_mbrs->membership().members[n])) {
						sec_nodeset.remove_node(n);
					}
					if (!disqualify_node(n,
					    mbrshp->membership().members[n],
					    out_rank)) {
						return (true);
					}
					// If we are reconfiguring after a
					// canceled step 5 or 6, we need to
					// keep track of those providers that
					// may be spares but were added in
					// become_primary_prepare.  If these
					// are actually secondaries, no harm
					// done.
					if (!sec_nodeset.contains(n))
						continue;
					current[n].state =
					    replica_int::RMM_PROV_SECONDARY;
					secondary_count++;
				}
			} // for (no.highest()...)

			// If we're reconfiguring after a cancelled step 5 or
			// 6, we need to correct sn_mbrs and sec_name_seq in
			// accordance with the current set of secondaries in
			// sec_nodeset. A secondary
			// provider might have left the cluster, and we
			// don't want to pass an invalid secondary to
			// repl_mgr_impl::add_rma() during step 7 as part of
			// rmm::primary_promotion(). Note that we're doing
			// this only if we're the primary.
			//
			if (sec_name_seq != NULL) {
				delete sec_name_seq;
			}

			sec_name_seq = new replica::repl_name_seq(
						    secondary_count);
			ASSERT(sec_name_seq != NULL);

			uint_t ind = 0;

			//
			// Start from lowest nodeid as the RMM primary is
			// usually the node with the highest nodeid.
			//
			for (no.lowest(); no.valid(); no.higher()) {
				n = no.current();

				if (!sec_nodeset.contains(n)) {
					continue;
				}

				RMM_DBG(("rmm::d14n Adding secondary"
				    " for node %d\n", n));
				(*sec_name_seq)[ind] = new char [4];
				ASSERT((*sec_name_seq)[ind] != NULL);
				(void) os::snprintf((*sec_name_seq)[ind],
				    4UL, "%u", n);
				ASSERT(strlen((*sec_name_seq)[ind]) < 3);
				ind++;
				if (ind == secondary_count) {
					break;
				}
			}
			sec_name_seq->length(secondary_count);

			if (sn_mbrs != NULL) {
				sn_mbrs->rele();
				sn_mbrs = NULL;
			}
			sn_mbrs = mbrshp;
			sn_mbrs->hold();
		} // if (primary_local)

		return (primary_local);
	} // if (primary_local || (!CORBA::is_nil(primary_prov))...)

	// Here we know that there is no valid primary, so we prepare to go
	// through the full disqualification process.
	RMM_DBG(("rmm::d14n no valid primary\n"));

	// If there is a dead reference lying around, release it.
	if (!CORBA::is_nil(primary_prov)) {
		CL_PANIC(primary_prov->_handler()->server_dead());
		CORBA::release(primary_prov);
		primary_prov = nil;
	}

	// Set the flags that will be used in subsequent reconfigurations
	// to determine whether the necessary work has been performed.
	connected_to_primary = false;
	became_primary = false;
	service_started = false;

	// We always bring up the service completely, unfreezing it, when we
	// are selecting a new primary.  We may also need to promote our
	// provider to secondary status if it is a spare (the primary will
	// tell us).  These flags are passed in exchange_state, so we need
	// to clear them now.
	start_frozen = false;
	promote_spare_to_secondary = false;

	// Our provider information is stored in the current structure, just
	// like the information for the other providers.  We pull it out here
	// and set the candidate_prov_v and candidate_rank, as well as the
	// local_rank (which is used to compare against the candidates
	// examined during disqualification).
	candidate_prov_v = replica_int::rma_repl_prov::
	    _duplicate(current[lnid].prov);

	replica_int::rma_repl_prov_state rma_prov_state;
	replica_int::repl_prov_exception	rpe;

	current[lnid].prov->get_state(rma_prov_state, rpe, e);
	CL_PANIC(!e.exception());
	local_rank.state = candidate_rank.state =
	    rma_to_rmm_state(rma_prov_state, rpe);

#ifdef	RMM_DEBUG
	if (local_rank.state == replica_int::RMM_PROV_SPARE) {
		RMM_DBG(("rmm::d14n found spare provider\n"));
	} else {
		CL_PANIC(local_rank.state == replica_int::RMM_PROV_SECONDARY);
		RMM_DBG(("rmm::d14n found secondary provider\n"));
	}
#endif	// RMM_DEBUG

	local_rank.nodeid = candidate_rank.nodeid = lnid;

	// We call become_spare in case this provider was in progress of
	// becoming a secondary.  No need to do this if the service has
	// never been up, and it could interfere in the case where we're
	// starting the service the first time from all spares.
	if (service_up && (local_rank.state == replica_int::RMM_PROV_SPARE)) {
		current[lnid].prov->become_spare(e);
		CL_PANIC(!e.exception());
	}
	// At this point we unlock the candidate so that exchange_state
	// invocations from other nodes will not block (the candidate is
	// ready).
	disqualification_running = true;
	candidate_lock.unlock(rmm_lock::CMM_RECONFIG);

	//
	// If we are guaranteed that we will always be disqualified, there's
	// no point in exchanging state with other nodes.  So we just
	// return here.
	// But,  if service has been up we must wait to see if there is
	// a secondary around.
	//
	if (!service_up &&
	    always_disqualified(mbrshp->membership(), local_rank)) {
		return (false);
	}

	sec_nodeset.empty();

	// The disqualificaton loop.  We go from highest to lowest because
	// it's more likely a higher ranked node will disqualify us, so we
	// can stop communication sooner.
	for (no.highest(); no.valid(); no.lower()) {
		n = no.current();

		// No need to disqualify ourselves.
		if (n == lnid) {
			continue;
		}

		// We need to lock the candidate while we're examining it.
		candidate_lock.lock(rmm_lock::CMM_EXAMINE);

		//
		// If we've been disqualified by some other node, we can
		// stop work now. , if service has never been up
		// or if the  other is not a spare
		//
		if ((candidate_rank.nodeid != lnid) &&
		    (!service_up ||
		    candidate_rank.state != replica_int::RMM_PROV_SPARE)) {
			candidate_lock.unlock(rmm_lock::CMM_EXAMINE);
			return (false);
		}

		if (!disqualification_running) {
			candidate_lock.unlock(rmm_lock::CMM_EXAMINE);
			return (false);
		}

		candidate_lock.unlock(rmm_lock::CMM_EXAMINE);

		// Exchange state with the other node
		if (!disqualify_node(n, mbrshp->membership().members[n],
		    out_rank)) {
			return (false);
		}

		// If the node is unknown, we released any old references
		// to objects on the node in the disqualify_node call, and
		// we know that it can't have disqualified us, so we go to
		// the next node.
		if (mbrshp->membership().members[n] == INCN_UNKNOWN) {
			continue;
		}

		//
		// If the rank we received from the other node is greater
		// than our rank, we end disqualification, if the other is
		// not a spare, or the service has never been up.
		// The other node will contact us to update our candidate.
		//
		if (first_rank_is_greater(out_rank, local_rank) &&
		    (!service_up ||
		    out_rank.state != replica_int::RMM_PROV_SPARE)) {
			RMM_DBG(("rmm::d14n Node %d greater\n",
			    out_rank.nodeid));
			return (false);
		}

		// If we're going to become primary, we need to keep track
		// of the number of secondaries to add in become_primary,
		// which is the number of providers that have our state.

		if (out_rank.state == local_rank.state) {
			sec_nodeset.add_node(n);
			secondary_count++;
		}
	}

	//
	// If the best candidate is in spare mode and service has been up
	// this means that primary and secondaries are all dead.  We cannot
	// go directly from spare to primary if the service has already been
	// up.  For now, reboot the node without panic (a panic would occur
	// on the hxdoor code ,or in primary_connection()).
	//
	if (service_up &&
	    (candidate_rank.state == replica_int::RMM_PROV_SPARE)) {
		//
		// SCMSGS
		// @explanation
		// Primary and secondaries for Replica Manger service are all
		// dead, remaining nodes which are booting cannot rebuild RM
		// state and must reboot immediatly, even if they were able to
		// form a valid partition from the quorum point of view.
		// @user_action
		// No action required
		//
		(void) orb_syslog_msgp->log(
		    SC_SYSLOG_WARNING, MESSAGE,
		    "HA: primary RM died during boot, "
		    "no secondary available, must reboot.\n");
		// give some time to other to get to same state as us
		os::usecsleep((os::usec_t)2*MICROSEC);
#ifdef _KERNEL
		vfs_unmountall();
		(void) VFS_MOUNTROOT(rootvfs, ROOT_UNMOUNT);
		vfs_syncall();
		dump_messages();
#if SOL_VERSION >= __s11
		mdboot(A_REBOOT, AD_BOOT, NULL, B_FALSE);
#else
		mdboot(A_REBOOT, AD_BOOT, NULL);
#endif // SOL_VERISON
#else
		reboot_unode();
#endif // _KERNEL
	}

	// If we haven't returned yet, we're hosting the primary
	primary_local = true;
	RMM_DBG(("rmm::d14n This node hosting primary\n"));

	if (sn_mbrs != NULL) {
		sn_mbrs->rele();
		sn_mbrs = NULL;
	}
	sn_mbrs = mbrshp;
	sn_mbrs->hold();

	// We call become_primary_prepare at the end of this invocation, so
	// we prepare both the list of secondary names and the list of
	// secondaries at this point.  We use the references we collected
	// in exchange_state calls to do this.  The name we use for each
	// provider is the nodeid of its host in an ASCII string.

	replica::secondary_seq	sec_seq(secondary_count);

	if (sec_name_seq != NULL) {
		delete sec_name_seq;
	}

	sec_name_seq = new replica::repl_name_seq(secondary_count);

	uint_t i = 0;

	for (no.lowest(); no.valid(); no.higher()) {
		n = no.current();

		if (!sec_nodeset.contains(n)) {
			continue;
		}

		current[n].state = replica_int::RMM_PROV_SECONDARY;
		sec_seq[i] = replica::checkpoint::_duplicate(current[n].ckpt);
		RMM_DBG(("rmm::d14n Adding secondary for node %d\n", n));
		(*sec_name_seq)[i] = new char [4];
		(void) os::snprintf((*sec_name_seq)[i], 4UL, "%u", n);
		CL_PANIC(strlen((*sec_name_seq)[i]) < 3);
		i++;
		if (i == secondary_count) {
			break;
		}
	}
	sec_seq->length(secondary_count);
	sec_name_seq->length(secondary_count);
	current[lnid].prov->become_primary_prepare(sec_seq, e);
	local_rank.state = current[lnid].state = replica_int::RMM_PROV_PRIMARY;

	// Mark the candidate as the primary in case we return to
	// disqualification in the future.
	candidate_lock.lock(rmm_lock::CMM_EXAMINE);
	candidate_rank.state = replica_int::RMM_PROV_PRIMARY;
	candidate_lock.unlock(rmm_lock::CMM_EXAMINE);

	RMM_DBG(("rmm::d14n Called become_primary_prepare\n"));
	CL_PANIC(!e.exception());

	return (true);
}

void
rmm::disqualification_return()
{
	// If we return from disqualification, we set this boolean to false
	// to indicate to both exchange_state on other nodes and
	// disqualification on this node that the step has been canceled.
	// We know the boolean will be true because it is protected by
	// the candidate lock and set to true by disqualification before the
	// lock is dropped (the lock is grabbed in step 4).

	candidate_lock.lock(rmm_lock::CMM_CANCEL);
	CL_PANIC(disqualification_running);
	disqualification_running = false;
	candidate_lock.unlock(rmm_lock::CMM_CANCEL);
}

// exchange_state passes in the provider and rank of the caller, and
// returns the provider, rank, reconf object, checkpoint object.  It compares
// the candidate rank to the caller's rank, and if the caller's is higher,
// it replaces the candidate rank and candidate provider with the caller's.
// Also passed in is the start_frozen flag, which tells the node whether to
// unfreeze the service when it reaches the last step (this is used by the
// spare promotion thread on the node hosting the primary).
void
rmm::exchange_state(replica_int::rma_repl_prov_ptr in_prov,
	const replica_int::rmm_prov_rank &in_rank,
	replica_int::rma_repl_prov_out out_prov,
	replica_int::rmm_prov_rank &out_rank,
	replica_int::rma_reconf_out out_reconf,
	replica::checkpoint_out out_ckpt,
	bool st_frozen,
	bool p_spare_to_sec,
	Environment &e)
{
	// Only examine the candidate when we have the lock
	candidate_lock.lock(rmm_lock::EXCHANGE_STATE);
	if (!disqualification_running) {
		RMM_DBG(("rmm::exchange_state called from (%d) after "
		    "disqualification step canceled\n", in_rank.nodeid));
		replica_int::bad_exchange_time *bet = new
		    replica_int::bad_exchange_time;
		ASSERT(bet != NULL);
		e.exception(bet);
		candidate_lock.unlock(rmm_lock::EXCHANGE_STATE);
		return;
	}
	RMM_DBG(("rmm::exchange_state called from (%d) %d: local state %d\n",
	    in_rank.nodeid, in_rank.state, local_rank.state));
	out_prov = replica_int::rma_repl_prov::_duplicate(current[lnid].prov);
	copy_rank(out_rank, local_rank);
	out_reconf = replica_int::rma_reconf::_duplicate(current[lnid].reconf);
	out_ckpt = replica::checkpoint::_duplicate(current[lnid].ckpt);
	if (first_rank_is_greater(in_rank, candidate_rank)) {
		RMM_DBG(("rmm::exchange_state (%d) rank higher\n",
		    in_rank.nodeid));
		start_frozen = st_frozen;
		copy_rank(candidate_rank, in_rank);
		candidate_prov_v =
		    replica_int::rma_repl_prov::_duplicate(in_prov);
		promote_spare_to_secondary = p_spare_to_sec;
	}
	candidate_lock.unlock(rmm_lock::EXCHANGE_STATE);
}

replica_int::rma_reconf_ptr
rmm::get_rma_reconf_for_node(nodeid_t n) const
{
	return (replica_int::rma_reconf::_duplicate(current[n].reconf));
}

// primary_connection is where each node calls connect_to_primary.
void
rmm::primary_connection()
{
	Environment	e;

	// We only accept calls to exchange_state during disqualification
	disqualification_running = false;

	// If we've already connected to this primary, we don't need to do
	// so again.
	if (connected_to_primary) {
		return;
	}

	RMM_DBG(("rmm::primary_connection\n"));

#ifdef	DEBUG
	if (promote_spare_to_secondary)
		CL_PANIC(candidate_rank.state == replica_int::RMM_PROV_PRIMARY);
#endif

	//
	// If the candidate is a spare and the service has been up, that
	// means this node joined an existing cluster, and then all of the
	// other nodes in that cluster died before it's spare was promoted.
	// In that case, the current cluster is really a new cluster, and
	// this node needs to reboot.
	//
	if (candidate_rank.state == replica_int::RMM_PROV_SPARE && service_up) {
		// XXX it would be nice to do something more graceful here.

		//
		// SCMSGS
		// @explanation
		// This node joined an existing cluster. Then all of the other
		// nodes in the cluster died before the HA framework
		// components on this node could be properly initialized.
		// @user_action
		// This node must be rebooted.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "HA: no valid secondary provider in rmm - aborting");
	}

	hxsr->lock();
	// We only do one invocation here, and in the author's opinion,
	// checking for a canceled step would add unnecessary complexity.
	//
	// (If the node hosting the primary has failed, all other nodes
	// in the membership will hang here.  If it hasn't, this should
	// complete just fine.  Asking if we've been canceled could leave
	// some nodes connected and others not.  Recovery from that condition
	// could still work correctly (if we're starting from a secondary,
	// any new node would have a spare, so the primary selected in the
	// first disqualification would be selected again.  If we're starting
	// with a spare, we declared all spares to be secondaries in the
	// disqualification step, so the same condition applies.  The
	// node hosting the primary doesn't contact "known nodes" in the
	// current design, however, so minor changes would be needed), but it's
	// more to think about.)
	hxsr->connect_to_primary(candidate_prov_v);
	hxsr->unlock();

	RMM_DBG(("rmm::primary_connection called connect_to_primary\n"));

	// We set the primary here.
	primary_prov = replica_int::rma_repl_prov::_duplicate(candidate_prov_v);

	// If the candidate is a spare, or if we are a spare and have been
	// told by the primary that we should promote ourselves to secondary
	// status, we can commit our provider as a secondary because we know
	// we were added as a secondary in the first call to become_primary.
	// We don't update the state in the current structure because it isn't
	// used (we always query the provider unless we're hosting the primary.)

	if (((candidate_rank.state == replica_int::RMM_PROV_SPARE) ||
	    ((local_rank.state == replica_int::RMM_PROV_SPARE) &&
	    promote_spare_to_secondary)) && !primary_local) {
		current[lnid].prov->commit_secondary(e);
		CL_PANIC(!e.exception());
		RMM_DBG(("rmm::primary_connection committed secondary\n"));
	}

	// We've connected to the primary, and since we have a primary, the
	// service can be considered active.
	connected_to_primary = true;
	service_up = true;
}

//
// Returns the nodeid where the RM primary will be located.  Used by the
// Version Manager
//
nodeid_t
rmm::primary_location()
{
	nodeid_t primary_loc;

	candidate_lock.lock(rmm_lock::CMM_EXAMINE);
	primary_loc = candidate_rank.nodeid;
	candidate_lock.unlock(rmm_lock::CMM_EXAMINE);

	return (primary_loc);
}

void
rmm::primary_connection_return()
{
	// In case, for whatever, reason, step 6 wasn't run before we
	// reconfigure and step 4 runs.
	disqualification_running = false;
}

// This is where we call become_primary and unfreeze_primary.
void
rmm::primary_promotion(callback_membership *mbrshp)
{
	Environment	e;

	if (primary_local) {
		// Store the new membership now - we're guaranteed to run the
		// rest of this function.
		ASSERT(last_mbrshp != NULL);
		last_mbrshp->rele();
		last_mbrshp = mbrshp;
		last_mbrshp->hold();
	}

	// We only need to promote the primary if we didn't already.
	if (became_primary) {
		return;
	}

	RMM_DBG(("rmm::primary_promotion\n"));

	if (primary_local) {
		// We only do one "off-node" invocation in this step as
		// well.  The only way for this invocation to fail is for
		// this node to die.  It can hang for a while and then
		// succeed (because a secondary isn't available to receive
		// checkpoints).  If we canceled and waited, this call
		// wouldn't be any different anyway, because the secondary
		// name sequence should match the secondaries we sent in
		// in become_primary_prepare() (old secondaries might have
		// have failed, but new secondaries can't have appeared, and
		// passing in failed secondaries is ok).
		current[lnid].prov->become_primary(*sec_name_seq, e);
		// The primary is local, so this call cannot fail.
		CL_PANIC(!e.exception());
		RMM_DBG(("rmm::primary_promotion called become_primary\n"));
		delete sec_name_seq;
		// Lint thinks the assignment to NULL causes a leak, but we
		// just deleted it.
		sec_name_seq = NULL; //lint !e423
		// All the spare committed themselves as secondaries in the
		// previous step, and will never be disqualified by this node
		// again.
		sec_nodeset.empty();
		ASSERT(sn_mbrs != NULL);
		sn_mbrs->rele();
		sn_mbrs = NULL;
		// It's now safe to unfreeze the primary  (the service is
		// still frozen.
		current[lnid].prov->unfreeze_primary(e);
		CL_PANIC(!e.exception());
		RMM_DBG(("rmm::primary_promotion called unfreeze_primary\n"));
	}

	// we've called become_primary.
	became_primary = true;
}

// This is where the service is unfrozen.  We only unfreeze the service if
// the start_frozen flag is not set.
void
rmm::service_startup()
{
	if (!service_started && !start_frozen) {
		hxsr->lock();
		hxsr->unfreeze_service_invos();
		hxsr->unlock();
		RMM_DBG(("rmm::service_startup unfroze service\n"));
	}

	// This puts the spare promotion task in the threadpool.  The
	// task is allocated statically, so we have a variable that
	// keeps track of whether it's deferred.
	if (primary_local) {
		if (!promote_task_deferred) {
			common_threadpool::the().defer_processing(&spt);
			promote_task_deferred = true;
		}
		run_promote_task = true;
		RMM_DBG(("rmm::service_startup added spare_promote task\n"));
	}

	// Now the service is started.
	service_started = true;
}

// We can safely drop the reconfigure lock here now that we know that the
// service is unfrozen on all nodes.
void
rmm::reconfig_cleanup()
{
	reconfigure_lock.unlock(rmm_lock::CMM_RECONFIG);
}

bool
rmm::spare_promotion(void)
{
	node_order	no;
	Environment	e;
	nodeid_t	n;
	bool	spare_found = false;

	reconfigure_lock.lock(rmm_lock::PROMOTION_THREAD);

	// Set the flag to false.  It will be checked when we're done to see
	// if we should run again.
	run_promote_task = false;

	RMM_DBG(("rmm::spare_promotion starting\n"));

	// First, check to see if there are any spares.  We can do this with
	// the reconfigure lock held.
	for (no.highest(); no.valid(); no.lower()) {
		n = no.current();
		if ((last_mbrshp->membership().members[n] !=
		    INCN_UNKNOWN) && (current[n].state ==
		    replica_int::RMM_PROV_SPARE)) {
			RMM_DBG(("rmm::pt found spare node %d\n", n));
			spare_found = true;
			break;
		}
	}

	// If there weren't any spares, return.  The run flag could not
	// have been set again because the reconfigure lock was never
	// dropped.
	if (!spare_found) {
		CL_PANIC(!run_promote_task);
		RMM_DBG(("rmm::spare_promotion found no spares\n"));
		promote_task_deferred = false;
		reconfigure_lock.unlock(rmm_lock::PROMOTION_THREAD);
		return (false);
	}

	// Whenever we do an invocation that could block, we need to drop
	// the reconfigure lock.  Here we're going to freeze the primary.
	reconfigure_lock.unlock(rmm_lock::PROMOTION_THREAD);

	// The values for current for our node don't change after they
	// are set in the constructor, so this is safe, even though we
	// we don't hold the lock.
	current[lnid].prov->freeze_primary(e);
	CL_PANIC(!e.exception());
	RMM_DBG(("rmm::promotion_thread froze primary\n"));

	promote = new node_info[NODEID_MAX+1];

	// Now we get a copy of the entire current structure, in promote.
	reconfigure_lock.lock(rmm_lock::PROMOTION_THREAD);

	// Store the spare membership in a local variable
	callback_membership *promote_mbrshp = NULL;
	promote_mbrshp = last_mbrshp;
	promote_mbrshp->hold();

	for (no.highest(); no.valid(); no.lower()) {
		promote[no.current()].copy(current[no.current()]);
	}

	// We're going to freeze all the nodes, so we set start_frozen to
	// true.
	start_frozen = true;
	reconfigure_lock.unlock(rmm_lock::PROMOTION_THREAD);

	// Freeze all the nodes, ignoring errors.  Nodes that
	// weren't in the membership when we copied it will start frozen,
	// because of the start_frozen flag.
	for (no.highest(); no.valid(); no.lower()) {
		n = no.current();
		if (promote_mbrshp->membership().members[n] != INCN_UNKNOWN) {
			ASSERT(!CORBA::is_nil(promote[n].reconf));
			promote[n].reconf->freeze_service_invos(
			    RMM_SERVICE_ID, e);
			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					e.clear();
				} else {
					CL_PANIC(0);
				}
			}
		}
	}
	RMM_DBG(("rmm::spare_promotion froze service\n"));

	// Call add_secondary for all the spares.
	for (no.highest(); no.valid(); no.lower()) {
		n = no.current();
		if ((promote_mbrshp->membership().members[n] != INCN_UNKNOWN) &&
		    (promote[n].state == replica_int::RMM_PROV_SPARE)) {
			CL_PANIC(promote[n].prov != nil);
			char sname[4];
			(void) os::snprintf(sname, 4UL, "%u", n);
			CL_PANIC(strlen(sname) < 3);
			current[lnid].prov->add_secondary(
			    promote[n].ckpt, sname, e);
			// The primary is local, so it won't return
			// an exception.
			CL_PANIC(!e.exception());
			RMM_DBG(("rmm::spare_promotion added sec %d\n", n));
			// The only exception we can get here is COMM_FAILURE.
			promote[n].prov->commit_secondary(e);
			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					e.clear();
				} else {
					CL_PANIC(0);
				}
			}
			promote[n].state = replica_int::RMM_PROV_SECONDARY;
		}
	}

	// Now we can go ahead and unfreeze the primary.
	current[lnid].prov->unfreeze_primary(e);
	CL_PANIC(!e.exception());
	RMM_DBG(("rmm::spare_promotion unfroze primary\n"));

	// Finally, we need to lock and get a new copy of the membership,
	// and set start_frozen to false, so that we can unfreeze any
	// node that was frozen, and new nodes will start unfrozen.
	reconfigure_lock.lock(rmm_lock::PROMOTION_THREAD);
	for (no.highest(); no.valid(); no.lower()) {
		n = no.current();
		if (promote_mbrshp->membership().members[n] ==
		    last_mbrshp->membership().members[n]) {
			if (promote[n].state == replica_int::RMM_PROV_SECONDARY)
				current[n].state = promote[n].state;
		} else
			// Get new reconf objects to unfreeze
			promote[n].copy(current[n]);
	}
	ASSERT(promote_mbrshp != NULL);
	promote_mbrshp->rele();
	promote_mbrshp = last_mbrshp;
	promote_mbrshp->hold();
	start_frozen = false;
	reconfigure_lock.unlock(rmm_lock::PROMOTION_THREAD);

	// Here we unfreeze every node.
	for (no.highest(); no.valid(); no.lower()) {
		n = no.current();
		if (promote_mbrshp->membership().members[n] != INCN_UNKNOWN) {
			ASSERT(promote[n].reconf != nil);
			promote[n].reconf->unfreeze_service_invos(
			    RMM_SERVICE_ID, e);
			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					e.clear();
				} else {
					CL_PANIC(0);
				}
			}
		}
	}

	RMM_DBG(("rmm::spare_promotion unfroze service\n"));

	// Finally, we release the promotion structures,
	promote_mbrshp->rele();
	promote_mbrshp = NULL;
	delete [] promote;

	// And check to see if we need to be run again.
	reconfigure_lock.lock(rmm_lock::PROMOTION_THREAD);
	if (run_promote_task) {
		reconfigure_lock.unlock(rmm_lock::PROMOTION_THREAD);
		return (true);
	} else {
		promote_task_deferred = false;
		reconfigure_lock.unlock(rmm_lock::PROMOTION_THREAD);
		return (false);
	}
}

// The spare_promote_task just calls the spare_promotion function.
void
rmm::spare_promote_task::execute(void)
{
	while (rmm::the().spare_promotion()) { }
}
