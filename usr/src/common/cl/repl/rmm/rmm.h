/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RMM_H
#define	_RMM_H

#pragma ident	"@(#)rmm.h	1.34	08/05/20 SMI"

#include <h/replica_int.h>
#include <orb/object/adapter.h>
#include <repl/rma/rma.h>
#include <orb/member/cmm_callback_impl.h>
#include <orb/infrastructure/orbthreads.h>
#include <sys/clconf_int.h>
#include <sys/node_order.h>
#include <orb/debug/haci_debug.h>
#include <sys/nodeset.h>

#if defined(HACI_DBGBUFS) && !defined(NO_RMM_DBGBUF)
#define	RMM_DBGBUF
#endif

#ifdef RMM_DBGBUF
extern dbg_print_buf rmm_dbg;
#define	RMM_DBG(a) HACI_DEBUG(ENABLE_RMM_DBG, rmm_dbg, a)
#else
#define	RMM_DBG(a)
#endif

extern "C" nodeid_t rmm_primary_node();

// Service IDs 1-9 are reserved for special purposes, like the rmm managing
// a service.  The HA RM gets everything above 9.
#define	RMM_SERVICE_ID	1

class rmm_lock {
public:
	rmm_lock();
	enum	lock_owner { NO_OWNER, CMM_RECONFIG, CMM_CANCEL, CMM_EXAMINE,
		    EXCHANGE_STATE, PROMOTION_THREAD, OTHER };
	void	lock(lock_owner);
	void	unlock(lock_owner);
	void	lock_if_not_owned(lock_owner);
	bool	is_owned_by(lock_owner) const;
private:
	lock_owner		owner;
	os::condvar_t		cv;
	static os::mutex_t	lck;
};

class rmm : public McNoref <replica_int::rmm> {
public:
	// Calls the constructor
	static int 	initialize(replica_int::handler_repl_prov_ptr,
			    replica::checkpoint_ptr, const char *name);
	void		shutdown() const;
	// Used to bind the root object in the nameserver
	bool		register_provider();
	static rmm &	the();
	// (Step 4) Grabs locks
	void		reconfig_prepare();
	// (Step 4 cancel) Drops candidate lock
	void		reconfig_prepare_return();
	// (Step 5) Decides on the next primary
	bool		disqualification(callback_membership *);
	// (Step 5 cancel) Invalidates exchange_state calls
	void		disqualification_return();
	// (Step 6) Connects to the primary
	void		primary_connection();
	// (Step 6) Tells the version manager the primary node
	nodeid_t	primary_location();
	// (Step 6 cancel) invalidates exchange_state
	void		primary_connection_return();
	// (Step 7) Calls become_primary on the new primary
	void		primary_promotion(callback_membership *);
	// (Step 8) Unfreezes the service
	void		service_startup();
	// (Step 9) Unlocks reconfiguration
	void		reconfig_cleanup();
	// Used in a deferred task to promote spares to secondaries
	bool		spare_promotion();
	// Inter-rmm invocation for disqualification
	virtual void	exchange_state(replica_int::rma_repl_prov_ptr,
			    const replica_int::rmm_prov_rank &,
			    replica_int::rma_repl_prov_out,
			    replica_int::rmm_prov_rank &,
			    replica_int::rma_reconf_out,
			    replica::checkpoint_out,
			    bool,
			    bool,
			    Environment &);

	replica_int::rma_reconf_ptr get_rma_reconf_for_node(nodeid_t n) const;

#ifdef	_FAULT_INJECTION
	// Used by the HA RM fault injection tests.  It returns the nodeid
	// the node hosting the HA RM primary.
	nodeid_t	primary_node();
#endif	// _FAULT_INJECTION

private:
	// Task used for spare promotion
	class spare_promote_task : public defer_task {
	public:
		void execute();
	};

	// node_info is a convenience class, just meant for the RMM
	// Stores info from exchange state in one object
	class node_info {
	public:
		node_info();
		~node_info();

		void copy(const node_info &);
		void reset();

		// public members, but in a private class
		replica_int::rma_reconf_ptr	reconf;
		replica_int::rma_repl_prov_ptr	prov;
		replica_int::rmm_prov_state	state;
		replica::checkpoint_ptr		ckpt;

	private:
		node_info(const node_info &);
		node_info &operator = (node_info &);
	};

	rmm(replica_int::handler_repl_prov_ptr, replica::checkpoint_ptr,
	    const char *);

	~rmm();

	replica_int::rmm_prov_state
			rma_to_rmm_state(replica_int::rma_repl_prov_state,
			    replica_int::repl_prov_exception) const;

	bool		always_disqualified(const cmm::membership_t &,
			    replica_int::rmm_prov_rank &) const;
	bool		disqualify_node(nodeid_t, incarnation_num,
			    replica_int::rmm_prov_rank &);

	static bool	first_rank_is_greater(
			    const replica_int::rmm_prov_rank &,
			    const replica_int::rmm_prov_rank &);
	static void 	copy_rank(replica_int::rmm_prov_rank &,
			    const replica_int::rmm_prov_rank &);

	// The pointer to the rmm, created in ::initialize
	static rmm			*the_rmm;
	// The hxdoor_service pointer for the rmm managed service (to
	// avoid having to lock the rma during reconfiguration)
	hxdoor_service			*hxsr;
	// The pointer to the current primary provider
	replica_int::rma_repl_prov_ptr	primary_prov;

	// The name of the service
	char				*service_name;

	// Local provider and status info
	nodeid_t			lnid;
	replica_int::rmm_prov_rank	local_rank;

	// Information that travels with the candidate
	replica_int::rma_repl_prov_var	candidate_prov_v;
	replica_int::rmm_prov_rank	candidate_rank;

	// Pointers to the incarnation numbers for all nodes
	callback_membership		*last_mbrshp;

	// Status flags
	// Whether this node is hosting the primary
	bool				primary_local;
	// Whether the service should be started frozen (set in exchange_state,
	// used to allow spare_promotion)
	bool				start_frozen;
	bool				promote_spare_to_secondary;
	bool				connected_to_primary;
	bool				became_primary;
	bool				service_started;
	bool				service_up;
	bool				reconfigure_lock_held;
	bool				candidate_lock_held;
	bool				promote_task_deferred;
	bool				run_promote_task;
	bool				disqualification_running;

	// Used to pass the secondary names to become_primary.
	// Constructed in disqualification, used in primary_promotion
	replica::repl_name_seq		*sec_name_seq;

	// Used to keep track of which nodes were passed in as secondaries
	nodeset				sec_nodeset;
	callback_membership		*sn_mbrs;

	// On the node hosting the primary, these are used to keep
	// references to the objects on other nodes, and their state
	node_info			current[NODEID_MAX + 1];
	node_info			*promote;

	// The spare promotion task
	spare_promote_task		spt;

	rmm_lock			reconfigure_lock;
	rmm_lock			candidate_lock;

	// Disallow
	rmm();
	rmm(const rmm &);
	rmm &operator = (rmm &);
};

#include <repl/rmm/rmm_in.h>

#endif	/* _RMM_H */
