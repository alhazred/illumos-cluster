/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  rmm_in.h
 *
 */

#ifndef _RMM_IN_H
#define	_RMM_IN_H

#pragma ident	"@(#)rmm_in.h	1.6	08/05/20 SMI"

inline
rmm::node_info::node_info(void)
{
	reconf = nil;
	prov = nil;
	ckpt = nil;
	state = replica_int::RMM_PROV_UNKNOWN;
}

inline
rmm::node_info::~node_info()
{
	reset();
}

inline void
rmm::node_info::copy(const node_info &a)
{
	reset();
	reconf = replica_int::rma_reconf::_duplicate(a.reconf);
	prov = replica_int::rma_repl_prov::_duplicate(a.prov);
	ckpt = replica::checkpoint::_duplicate(a.ckpt);
	state = a.state;
}

inline void
rmm::node_info::reset(void)
{
	if (!CORBA::is_nil(reconf)) {
		CORBA::release(reconf);
		reconf = nil;
	}
	if (!CORBA::is_nil(prov)) {
		CORBA::release(prov);
		prov = nil;
	}
	if (!CORBA::is_nil(ckpt)) {
		CORBA::release(ckpt);
		ckpt = nil;
	}
	state = replica_int::RMM_PROV_UNKNOWN;
}

inline void
rmm::copy_rank(replica_int::rmm_prov_rank &out,
	const replica_int::rmm_prov_rank &in)
{
	out.state = in.state;
	out.nodeid = in.nodeid;
}

inline rmm &
rmm::the(void)
{
	ASSERT(the_rmm != NULL);
	return (*the_rmm);
}

#endif	/* _RMM_IN_H */
