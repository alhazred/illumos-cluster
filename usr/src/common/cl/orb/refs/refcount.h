//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _REFCOUNT_H
#define	_REFCOUNT_H

#pragma ident	"@(#)refcount.h	1.105	08/05/20 SMI"

//
// This module supports reference counting.
//

#include <limits.h>
#include <sys/list_def.h>
#include <h/cmm.h>
#include <orb/invo/common.h>
#include <orb/member/Node.h>
#include <orb/xdoor/rxdoor_defs.h>
#include <orb/infrastructure/orbthreads.h>
#include <orb/buffers/marshalstream.h>
#include <orb/debug/orbadmin_state.h>
#include <orb/fault/fault_injection.h>
#include <h/replica.h>	// needed for replica::service_num_t

class nodejobs;

// Define REFCNT_DEBUG_CONFIGURATION when performing tests and want
// to force multiple messages and encounter boundary conditions
// during the reference count stress test.
#undef	REFCNT_DEBUG_CONFIGURATION

extern ID_node invalid_node;

//
// refcnt_control - is the control record for a reference counting message.
// This record specifies flags for processing the data
// and identifies the number of jobs.
//
class refcnt_control {
public:
	refcnt_control();

	void		set_refs(uint_t);
	uint_t		get_refs() const;
	void		set_translate_acks(uint_t);
	uint_t		get_translate_acks() const;

	// Returns all of the flags for this message
	// Primarily intended for debug traces.
	uchar_t		get_flags() const;

	// Sets the piggy reply flag when a confirmation is needed
	void		set_piggy_reply();

	// Returns whether there is a refcount piggy reply in this message
	bool		has_piggy_reply();

	// Sets the urgent flag when want immediate confirmation
	void		set_urgent();

	// Returns whether the sender wants immediate confirmation for this msg
	bool		has_urgent();

private:
	//
	// The refcnt message control record is presented next.
	//
	uint8_t		content;	// message flags
	uint_t		refs;		// count of server reference jobs
	uint_t		translate_acks;	// count of translate ack jobs

	// Bit flags for field content
	enum {
		REPLY_PIGGY	= 0x01,	// Ack flag for last ref msg
		URGENT		= 0x02	// Need confirmation urgently
	};
};

// XXX - The size of the refcnt msg buffer is currently an arbitrary decision,
// as there is not yet support for reserving buffers. So currently any
// size will work. The intention is to force (and test) the node reconfiguration
// software to use a maximum size for its refcnt message.
//
// At this time the system only transmits the bytes needed for the actual data
// and not the maximum size.
//
#ifdef	REFCNT_DEBUG_CONFIGURATION
const uint_t	refcnt_buf_size = 80;
#else
const uint_t	refcnt_buf_size = 8192;
#endif

// The following constants manage reference count data structures

// These constants determine the size of the arrays used to hold ref jobs
#ifdef	REFCNT_DEBUG_CONFIGURATION
const uint_t	NUMBER_REFS_PER_BLOCK = 20;
#else
const uint_t	NUMBER_REFS_PER_BLOCK = 500;
#endif

// At system startup this many data structures are allocated for ref jobs
const uint_t	NODEJOBS_INITIAL_BLOCKS = 8;

// Maximum number of data structures allowed on free list
const uint_t	NODEJOBS_MAX_FREE_BLOCKS = 12;


//
// translate_ack - stores one translate ack job for a node.
//
class translate_ack {
	friend class nodejobs;
public:
	translate_ack();
	void		_put(MarshalStream &);

private:
	uint64_t	ack_cookie;
};

//
// The following constants are used to manage the use of the
// maximum size buffer for reference count messages.
//
#ifdef MARSHAL_DEBUG
const uint_t	sizeof_marshal_debug = (uint_t)(sizeof (uint_t));
#else
const uint_t	sizeof_marshal_debug = 0;
#endif	// MARSHAL_DEBUG

const uint_t	sizeof_marshalled_ack =
		    sizeof_marshal_debug + ((uint_t)sizeof (uint64_t));

//
// ref_jobs - manages reference jobs in a doubly ended queue.
// The oldest entries are pulled from the head of the queue.
// New entries are added to the tail of the queue.
//
// The implementation uses a list of arrays of active reference jobs
// and a list of pre-allocated arrays for reference jobs. This data structure
// was chosen to minimize the need for memory allocations.
//
template<class A> class ref_jobs {
public:
	//
	// ref_block - contains an array of reference count jobs.
	// This structure exists in order to minimize memory allocations.
	//
	class ref_block : public _SList::ListElem {
	public:
		//
		// Because of BugId 4012797, we need to inline the constructor
		// here. This only causes problems with the Intel 4.2 compiler.
		//
		ref_block() : head(0), tail(0), _SList::ListElem(this) { }

		int	head;	// offset into array of ref jobs
				// is first valid entry, unless equal to tail.

		int	tail;	// offset into array of ref jobs
				// is first free entry, unless past end of array

		// Contains reference count jobs
		A	job[NUMBER_REFS_PER_BLOCK];
	};

	//
	// Iterator to walk the list of jobs. Initializes at the first job.
	//
	class iterator {
	public:
		//
		// Because of BugId 4012797, we need to inline the constructor
		// here. This only causes problems with the Intel 4.2 compiler.
		//
		// Initializes at the first job.
		iterator(ref_jobs<A> *jobs) :
			li(jobs->ref_active_list),
			index(li.get_current()->head) { }
		A	*get_current() const;
		void	advance();

	private:
		typename IntrList<ref_block, _SList>::ListIterator li;
		uint_t	index;	// Index into current element.
	};

	friend class iterator;

	ref_jobs(os::mutex_t *);

	// Free all of the heap allocated data structures
	~ref_jobs();

	// The data structures are initialized and the specified number
	// of arrays are allocated.
	void	initialize(int number_arrays);

	// The heap allocated structures are all released.
	void	release();

	// Returns the current tail entry, and advances the tail by one entry
	A 	*add_to_tail();

	// Returns the number of entries.
	inline uint_t	get_count() const;

	// Flush some of the jobs to the marshal stream.
	void	flush_jobs(uint_t, MarshalStream &);

	// Purge jobs which have been sent.
	void	purge_jobs(uint_t);

private:
	// A new block is added to the list of active blocks
	void		add_active_block(ref_block *ref_blockp);

	// Number of ref_blocks on the free list
	uint_t		num_free_blocks;

	// Pointer to the lock that is used to control access
	// Currently, the lock pointer is needed only for the debug version
	os::mutex_t	*lockp;

	// The reference counting information is stored in singly linked lists
	// of arrays of reference count jobs. This arrangement was chosen
	// to avoid memory allocation operations on every reference count job.
	// The number of "reference job arrays" was chosen to ensure that under
	// normal conditions a small number are recycled without any
	// memory allocations. The orb node reconfiguration synchronizes with
	// the class nodejobs to avoid memory allocations.
	//
	// The singly linked list is always left with the current element
	// pointing to the last element. The justification is that element adds
	// occurs individually, whereas element removals involve many elements
	// at at time.
	//
	// The oldest element is at the head (first) of the list.

	// Where the reference blocks wait before being used.
	IntrList<ref_block, _SList>   ref_free_list;

	// Where the reference count jobs wait processed
	IntrList<ref_block, _SList>   ref_active_list;

	// Number of current entries
	uint_t	count;
};

//
// nodejobs - stores information about lazy reference counting jobs to
// be performed for a node. Each node has one object of this type per node
// in the cluster.
//
class nodejobs {
friend class refcount;
public:
	nodejobs();
	~nodejobs();

	// Prior to any reference count activity, perform initialization.
	void	initialize(nodeid_t mynode);

	// Queue a register job for later transmission to the server.
	bool	add_reg(ID_node &server_node, rxdoor_descriptor &xd,
		    ID_node &lent_node);

	// Queue an unregister job for later transmission to the server.
	void	add_unreg(ID_node &server_node, rxdoor_descriptor &xd);

	// Queue translate ack job
	void	add_translate_ack(ID_node &, uint64_t);

	// Add/remove dependencies on translate acks to this node.
	void	add_dependent_reg(ID_node &nd, nodeid_t ndid);
	void	remove_dependent_reg(ID_node &nd, nodeid_t ndid);

	// Checks whether need to flush
	bool	flush_check();

	// Sends whatever data it can. Decides when it should be called again
	// and, if necessary, adjusts the timer passed in to the appropriate
	// time.
	void	flush_node(os::systime *);

	// Process incoming reference count message
	void	process_refmsg(refcnt_control *ref_hdrp, MarshalStream &rms,
	    incarnation_num from_incn);

	//
	// Reconfiguration methods.
	//
	// Methods called during CMM reconfiguration. This involves
	// updating the incarnation of the node we communicate with,
	// cleaning up if the old incarnation has died, preparing to
	// communicate if the node has joined and waiting for register
	// messages added as part of unmarshaling from nodes which are
	// now dead are delivered to the server.
	//
	void	cleanup_dead_refcount(incarnation_num incn,
		    cmm::seqnum_t seqnum);

	// Cancel reconfiguration numbered seqnum.
	void		cleanup_dead_refcount_cancel(cmm::seqnum_t seqnum);

	// Provide state information
	void	read_state(state_refcnt &state);

#ifdef FAULT_RECONF
	void		wait_for_step1_blocked();
	void		wait_for_step1_unblocked();
	bool		fi_blocked;
#endif

private:
	// Process the ref count jobs waiting in the wait list.
	// If a register has been acknowledged the dependency on this register
	// is removed from the translate ack list on the appropriate node.
	void		confirm_arrived(uint_t);

	// Process incoming reference count jobs.
	void		process_refs(uint_t, MarshalStream &);

	void		process_translate_acks(uint_t, MarshalStream &);

	// Utility function called from allocate_message_slots. It moves the
	// relevant number of jobs from being potential jobs to allocated
	// jobs for this message.
	uint_t		take_slots(uint_t *potential_jobs, uint_t *num_jobs,
	    uint_t avail_slots);

	// Allocate jobs for transmission.
	bool		allocate_message_slots(refcnt_control *);

	// Transmit a msg containing reference count job data
	bool		flush_data();

	// Initialize state and allocate resources
	void		init();

	// The node has died. Clean up and reset the data structures associated
	// with it.
	void		cleanup_dead_incn();

	void		lock();
	void		unlock();
	bool		lock_held();

	// The work in the structure is for the node with this incarnation
	// number and no other.
	ID_node		 node;

	// True whenever a confirmation must be sent.  We need a confirmation
	// for any message containing refs.  Having only one msg outstanding at
	// a time guarantees serialization of delivery to the server on a per
	// xdoor basis and allows us to clean up easily when the message is
	// acknowledged.
	bool		confirm;

	//
	// True when an outbound message is being be sent (i.e. in the function
	// flush_node()). We only allow one message to be sent to a node at a
	// time.
	//
	bool		outbound_msg_underway;

	// Indicates whether we failed to fit all of our ref messages in the
	// last message we sent. This indicates that we need to expidite
	// flushing as soon as the current ref messaes are acknowledged, since
	// allowing ref messages to back up can cause a serious reference
	// counting backlog.
	bool		refs_deferred;

	// Reg/unreg jobs to servers are not deleted until after they
	// have been both sent and confirmed.
	// This records the number of ref jobs awaiting confirmation.
	// The number of reg/unreg jobs in a message does not always match
	// the number of reg/unreg jobs in the queue for several reasons.
	uint_t		number_refs_sent;

	// During reconfiguration we wait for the register ref message with
	// this index to be acknowledged.
	// This is the last register in our queue which was added while
	// unmarshaling from a node which is now dead. By waiting for it, we
	// guarantee that the server xdoor knows about all references which
	// have been forwarded on by dead nodes. This guarantee helps it
	// accurately recalculate its foreign references after a node failure.
	int		wait_index;

	// Step 1 has been canceled for this seqnum. We need this because
	// the cancel can come in before the cleanup_dead_refcount call.
	cmm::seqnum_t	canceled_seqnum;

	// The next time that we should flush messages to this node. This is
	// adjusted every time we send, based on the size of the messages
	// we generate. It is also set to "now" sometimes, when we decide that
	// we want to flush immediately.
	os::systime	next_flush_time;

	os::usec_t	thread_sleep;	// Time between nodejobs scans

	os::mutex_t	lck;

	os::rwlock_t	rwlck;

	//
	// Supports synchronization with reconfiguration activity.
	//
	os::condvar_t	cv;

	// Registrations that we depend on before we can send our acks.
	uint_t	regs_needed_total;
	uint_t	regs_needed[NODEID_MAX + 1];

	//
	// The following are a snapshot of the counts that can be safely sent.
	//
	uint_t	snapshot_needed_total;
	uint_t	snapshot_ack_count;
	uint_t	snapshot_regs_needed[NODEID_MAX + 1];

#ifdef FAULT_RECONF
	os::condvar_t	fault_cv;
#endif

	// Server ref jobs are managed here
	ref_jobs<rxdoor_ref> refs;

	// translate ack jobs are managed here
	ref_jobs<translate_ack> translate_acks;
};

//
// refcount - supports:
// 1) orb message initialization and shutdown.
// 2) reference counting.
// 3) orb reconfiguration.
//

//
// Note that the base class, orbthread, provides a lock and two cvs that
// are used from the refcount class. The base class provides lock and
// unlock calls to manipulate the lock.
//
class refcount : public orbthread {
public:
	refcount();

	virtual		~refcount();

	// Perform initialization at system startup
	static int	initialize();

	static	void	shutdown();

	static refcount &the();

	// this method handles incoming messages containing reference count info
	static void	HandleRefCnts(recstream *);

	// daemon to lazily send references around
	static void	autom_thread(void *);

	//
	// Reconfiguration methods. Note that there is no reconfig_step2.
	//
	// Initiate reconfiguration. Disable reference counting.
	void	cleanup_dead_refcount(const cmm::membership_t &membership,
	    cmm::seqnum_t seqnum);

	// Cancel this reconfiguration
	void	cleanup_dead_refcount_cancel(cmm::seqnum_t seqnum);

	// sets up a reg job for the server xdoor
	void	add_reg(ID_node &server_node, rxdoor_descriptor &xd,
		    ID_node &lent_node = invalid_node);

	void	remove_dependent_reg(ID_node &lent_node, nodeid_t ndid);

	// sets up an unreg job for the server xdoor
	void	add_unreg(ID_node &server_node, rxdoor_descriptor &xd);

	// sets up a translate_ack job
	void	add_translate_ack(ID_node &, uint64_t);


	// Obtain the state for the specified destination node
	int	read_state(void *argp);

#ifdef FAULT_RECONF
	void	wait_for_step1_blocked(nodeid_t nd);
	void	wait_for_step1_unblocked(nodeid_t nd);
#endif

private:
	int initialize_int();

	nodejobs	ndtab[NODEID_MAX+1];

	static refcount *the_refcount;

	// The real work is done here.
	void		flush_nodes();
};

#include <orb/refs/refcount_in.h>

#endif	/* _REFCOUNT_H */
