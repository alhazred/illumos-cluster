//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.

#pragma ident	"@(#)unref_stats.cc	1.9	08/05/20 SMI"

#include <orb/refs/unref_threadpool.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif

//
// Define constants to speed up histogram calculation.
//
const os::hrtime_t	TIME_BASE	= 10000LL;
const os::hrtime_t	TIME_BASE1	= 100000LL;
const os::hrtime_t	TIME_BASE2	= 1000000LL;
const os::hrtime_t	TIME_BASE3	= 10000000LL;
const os::hrtime_t	TIME_BASE4	= 100000000LL;
const os::hrtime_t	TIME_BASE5	= 1000000000LL;
const os::hrtime_t	TIME_BASE6	= 10000000000LL;

const int	QUEUE_FACTOR = 4;
const int	QUEUE_BASE = 1;
const int	QUEUE_BASE1 = QUEUE_BASE * QUEUE_FACTOR;
const int	QUEUE_BASE2 = QUEUE_BASE1 * QUEUE_FACTOR;
const int	QUEUE_BASE3 = QUEUE_BASE2 * QUEUE_FACTOR;
const int	QUEUE_BASE4 = QUEUE_BASE3 * QUEUE_FACTOR;
const int	QUEUE_BASE5 = QUEUE_BASE4 * QUEUE_FACTOR;
const int	QUEUE_BASE6 = QUEUE_BASE5 * QUEUE_FACTOR;

#define	unref_histogram(VALUE, BASE, HISTOGRAM) \
	if (VALUE <= BASE) { \
		HISTOGRAM[0]++; \
	} else if (VALUE <= BASE ## 1) { \
		HISTOGRAM[1]++; \
	} else if (VALUE <= BASE ## 2) { \
		HISTOGRAM[2]++; \
	} else if (VALUE <= BASE ## 3) { \
		HISTOGRAM[3]++; \
	} else if (VALUE <= BASE ## 4) { \
		HISTOGRAM[4]++; \
	} else if (VALUE <= BASE ## 5) { \
		HISTOGRAM[5]++; \
	} else if (VALUE <= BASE ## 6) { \
		HISTOGRAM[6]++; \
	} else { \
		HISTOGRAM[7]++; \
	}

//
// unref_stats methods
//

unref_stats::unref_stats()
{
	init();
}

//
// init - set all statistics to zero
//
void
unref_stats::init()
{
	for (int i = 0; i < HIST_SIZE; i++) {
		hist_wait_handler[i] = 0;
		hist_wait_xdoor[i] = 0;
		hist_work_handler[i] = 0;
		hist_work_xdoor[i] = 0;
		hist_queue_length[i] = 0;
	}
}

//
// histogram_queue_length - update histogram data queue length
// based upon current queue length.
//
void
unref_stats::histogram_queue_length(uint_t length)
{
	unref_histogram(length, QUEUE_BASE, hist_queue_length)
}

//
// histogram_handler_wait - update histogram data for handler unref tasks
// using the waiting time of this one.
//
void
unref_stats::histogram_handler_wait(os::hrtime_t total_time)
{
	unref_histogram(total_time, TIME_BASE, hist_wait_handler)
}

//
// histogram_xdoor_wait - update histogram data for xdoor unref tasks
// using the waiting time of this one.
//
void
unref_stats::histogram_xdoor_wait(os::hrtime_t total_time)
{
	unref_histogram(total_time, TIME_BASE, hist_wait_xdoor)
}

//
// histogram_handler_work - update histogram data for handler unref tasks
// using the unreference work time of this one.
//
void
unref_stats::histogram_handler_work(os::hrtime_t total_time)
{
	unref_histogram(total_time, TIME_BASE, hist_work_handler)
}

//
// histogram_xdoor_work - update histogram data for xdoor unref tasks
// using the unreference work time of this one.
//
void
unref_stats::histogram_xdoor_work(os::hrtime_t total_time)
{
	unref_histogram(total_time, TIME_BASE, hist_work_xdoor)
}

#ifdef _KERNEL_ORB
//
// read_stats - supports a cladmin command to
// read the unref statistics for one node.
//
// static
int
unref_stats::read_stats(void *argp)
{
	return (copyout(this, argp, sizeof (unref_stats)));
}

//
// cl_read_orb_stats_unref - supports a cladmin command to
// read the unref statistics for one node.
//
extern "C" int
cl_read_orb_stats_unref(void *argp)
{
	return (unref_threadpool::the().stats.read_stats(argp));
}
#else
extern "C" int
cl_read_orb_stats_unref(void *)
{
	return (ENOTSUP);
}
#endif	// _KERNEL_ORB
