//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)unref_threadpool.cc	1.51	08/05/20 SMI"

#include <orb/refs/unref_threadpool.h>
#include <orb/handler/handler.h>
#include <orb/debug/orbadmin_state.h>
#include <orb/infrastructure/orb.h>

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
#include <orb/fault/fault_injection.h>
#include <orb/msg/orb_msg.h>
#endif

#if defined(_KERNEL) && !defined(DEBUG)
#include <sys/cpuvar.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif

//
// Un-Comment the following definition when you want
// warning messages about unref tasks that take excessive time.
//
// #define	DEBUG_WARN_LONG_UNREF

#ifdef DEBUG_WARN_LONG_UNREF
#include <orb/invo/corba.h>
#include <orb/handler/remote_handler.h>

static const long long	MS_IN_NS = 1000000;	// number of nanoseconds
static const long long	TIME_LIMIT_MS = 5 * MS_IN_NS;
#endif	// DEBUG_WARN_LONG_UNREF

//
// unref_task data and methods
//

unref_task::unref_task() :
	enque_time(0LL)
{
}

//
// task_done - Method called when the threadpool decides to throw away a task.
// This happens only during shutdown on the unref_threadpool.
// The system should destroy all handlers and xdoors prior to shutting
// down the unref_threadpool. The unref_task is a part of either a handler
// or a xdoor. We cannot safely delete the object.
//
// XXX - Until the system
// does a clean shutdown, do nothing. After the system does a clean
// shutdown, the system should assert that this code is never called.
//
void
unref_task::task_done()
{
}

//
// unref_rejected - the unreference notification was removed from the
// unref_threadpool. This method causes the entity to do the appropriate
// cleanup.
//
// This default implementation should never be called,
// because it is for unref_tasks that never get flushed.
//
void
unref_task::unref_rejected()
{
	ASSERT(0);
}

//
// set_enque_time - record the time the unref_task was enqueued
//
void
unref_task::set_enque_time()
{
	enque_time = os::gethrtime();
}

//
// execute - perform an unreference task coming from a handler
//
void
unref_task::execute()
{
	unref_threadpool	&unref = unref_threadpool::the();

	// Adjust minimum count of idle thread in unref_threadpool
	unref.adjust_min_idle();

	unref_task_type_t task_type = unref_task_type();

#ifdef DEBUG_WARN_LONG_UNREF
	//
	// These actions should only occur on a handler of this type
	//
	handler		*handp = get_unref_handler();

	hkit_id_t	handlerid = handp->handler_id();

	InterfaceDescriptor  *interfacep = NULL;

	if (handler_kit::is_remote_handler(handlerid)) {
		remote_handler	*rhandlerp = (remote_handler *)handp;
		*interfacep = rhandlerp->interface_descriptor();
		ASSERT(interfacep != NULL);

		void *vtblp = NULL;

		if (task_type == unref_task_type::HANDLER) {
			CORBA::Object_ptr objp = rhandlerp->_obj();
			ASSERT(!CORBA::is_nil(objp));

			//
			// The object exists as long as the handler
			// Record the vtbl before the object goes away
			//
			vtblp = *((void **)objp);
		}
	}

#endif	// DEBUG_WARN_LONG_UNREF

	//
	// Record time at the beginning of unreference
	//
	os::hrtime_t	begin_unref_time = os::gethrtime();
	update_histogram_wait(task_type, begin_unref_time - enque_time);

	deliver_unreferenced();

	//
	// Record time at the end of unreference
	//
	// The handler_unref_task may have been deleted.
	// Do not access handler member data.

	os::hrtime_t	work_time = os::gethrtime() - begin_unref_time;
	update_histogram_work(task_type, work_time);

#ifdef DEBUG_WARN_LONG_UNREF
	// Long Unreference times should be reported
	if (work_time >= TIME_LIMIT_MS) {
		if (task_type == unref_task::HANDLER) {
			os::warning("handler (hkit id %d) unref for %s at %p"
			    " took time %lldms\n", handlerid,
			    interfacep->get_name(), vtblp, work_time/MS_IN_NS);

		} else if (task_type == unref_task::XDOOR) {
			if (interfacep != NULL) {
				os::warning("xdoor unref for handler %s "
				    "(hkit id %d) took time %lldms\n",
				    interfacep->get_name(), handlerid,
				    work_time/MS_IN_NS);
			} else {
				os::warning("xdoor unref for handler "
				    "(hkit id %d) took time %lldms\n",
				    handlerid, work_time/MS_IN_NS);
			}
		} else {
			ASSERT(0);
		}
	}
#endif	// DEBUG_WARN_LONG_UNREF

	// Set unref_done to indicate one unreference task was processed.
	unref.set_unref_done();
}

//
// update wait histogram corresponding to indicated type
//
void
unref_task::update_histogram_wait(unref_task_type_t type,
    os::hrtime_t total_time)
{
	switch (type) {
		case unref_task::HANDLER:
			unref_threadpool::the().stats.histogram_handler_wait(
			    total_time);
			break;

		case unref_task::XDOOR:
			unref_threadpool::the().stats.histogram_xdoor_wait(
			    total_time);
			break;

		default:
			ASSERT(0);
			break;
	}
}

//
// update work histogram corresponding to indicated type
//
void
unref_task::update_histogram_work(unref_task_type_t type,
    os::hrtime_t total_time)
{
	switch (type) {
		case unref_task::HANDLER:
			unref_threadpool::the().stats.histogram_handler_work(
			    total_time);
			break;

		case unref_task::XDOOR:
			unref_threadpool::the().stats.histogram_xdoor_work(
			    total_time);
			break;

		default:
			ASSERT(0);
			break;
	}
}


//
// unref_threadpool data and methods
//

// Threadpool used to handle delivering unreferenced notifications
unref_threadpool *unref_threadpool::the_unref_threadpool = NULL;



//
// Construct the threadpool with no threads.
// The actual number of threads is set in the initialize_int() called during
// ORB::initialize
//
unref_threadpool::unref_threadpool() :
	threadpool(false, 0, "unref threadpool"),
	unref_done(false),
	min_num_idle(0),
	threads_min(0),
	all_busy_last_period(false)
{
}

int
unref_threadpool::initialize()
{
	ASSERT(the_unref_threadpool == NULL);
	the_unref_threadpool = new unref_threadpool();
	if (the_unref_threadpool == NULL)
		return (ENOMEM);
	return (the_unref_threadpool->initialize_int());
}

//
// initialize - specifies the number of threads to process unref tasks.
//
int
unref_threadpool::initialize_int()
{
	int unref_threads_desired;

#if defined(_KERNEL) && !defined(DEBUG)
	//
	// The number of threads for an unref_threadpool of a non-debug kernel
	// should scale with the number of CPU's.
	//
	unref_threads_desired = 1 + ncpus;
#else
	//
	// Use a fixed number of unref threads for user processes and
	// debug kernels.
	//
	unref_threads_desired = 2;

#endif	// defined(_KERNEL) && !defined(DEBUG)

	// Create the worker threads for the unref_threadpool
	int unref_threads_created = change_num_threads(unref_threads_desired);

	// The unref_threadpool should always retain at least this many threads
	threads_min = unref_threads_created;

	// Initially all threads are idle
	min_num_idle = unref_threads_created;

	if (unref_threads_created < unref_threads_desired) {
		//
		// SCMSGS
		// @explanation
		// The system was unable to create threads that deal with no
		// longer needed objects. The system fails to create threads
		// when memory is not available. This message can be generated
		// by the inability of either the kernel or a user level
		// process. The kernel creates unref threads when the cluster
		// starts. A user level process creates threads when it
		// initializes.
		// @user_action
		// Take steps to increase memory availability. The
		// installation of more memory will avoid the problem with a
		// kernel inability to create threads. For a user level
		// process problem: install more memory, increase swap space,
		// or reduce the peak work load.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: unable to create desired unref threads");
		return (ENOMEM);
	}
	return (0);
}

void
unref_threadpool::unref_shutdown()
{
	//
	// unref_threadpool::initialize() may not have been called.
	// In userland this can happen if it was never started and
	// in the kernel ORB::initialize might have failed
	// before initializing unref_threadpool.
	//
	// In that case, there is nothing to shutdown.
	//
	if (the_unref_threadpool == NULL) {
		return;
	}

	the_unref_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
	the_unref_threadpool->shutdown();
	//
	// XXX We can't delete the unref threadpool until we're sure
	// unreferenced can't be delivered on any object, so we leave
	// it hanging.  But we test it in unode, where we think we know
	// what's going on (ORB::shutdown is never run in the kernel)
	//
	// Whether we leave the structure hanging or not, we clear the
	// pointer to allow a new unref_threadpool to be allocated if
	// ORB::initialize is called again
	//
#ifdef	_KERNEL_ORB
	delete the_unref_threadpool;
#endif
	the_unref_threadpool = NULL;
}

unref_threadpool::~unref_threadpool()
{
	// quiesce has been done in ORB::shutdown,
	// which should already have been called.
}

//
// flush_tag - Throw away all the unreferenced for the specified tag.
//
//   All tasks removed from the unref_threadpool are returned on a
//   list to be processed by caller of the routine. Processing is left
//   up to the caller to prevent deadlocks. For ex: obtaining hxdoor
//   lock while holding unref_threadpool lock prevents any new hxdoors
//   from being added to the unref_threadpool.
//
void
unref_threadpool::flush_tag(uint_t tag, task_list_t *flushed_listp)
{
	// Prevent any new tasks from getting serviced
	block_processing();

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
	//
	// Set fault point to transfer threads from unref_threadpool
	// to invocation thread pool, for bugId 4644079
	//
	if (fault_triggered(FAULTNUM_HA_TRANSFER_TASK_TO_UNREF_THR,
	    NULL, NULL)) {
		os::printf("Fault "
			"FAULTNUM_HA_TRANSFER_TASK_TO_UNREF_THR triggered.\n");
		//
		// Add threads to unref_threadpool to make sure it has
		// threads that we can transfer
		//
		(void) change_num_threads(5);
		ASSERT(total_number_threads() > 5);
		//
		// Request thread transfer from unref_threadpool to
		// invocation threadpool. This will result in addition
		// of 5 transfer tasks to the unref_threadpool.
		//
		(orb_msg::the().
			get_server_threadpool(orb_msg::THREADPOOL_INVOS)).
			transfer_worker_threads(5, this);
	}
#endif
	//
	// Scan pending tasks for a particular tag and delete them.
	// Call to block_processing() above will have transferred all
	// tasks in the 'pending' list to 'blocked_waiting' list. So
	// we scan the 'blocked_waiting' list for given tag.
	//
	lock();

	task_list_t::ListIterator	iter(blocked_waiting);
	defer_task			*taskp;

	while ((taskp = (defer_task *)iter.get_current()) != NULL) {
		// Advance the list iterator before the erase (being safe)
		iter.advance();
		if (taskp->get_defer_task_tag() == tag) {
			// Remove this unref_task from the list
			(void) blocked_waiting.erase(taskp);

			// Add it to the flushed list
			flushed_listp->prepend(taskp);
		}
	}
	unlock();
	// There is a race with dropping the lock. We miss any new
	// unrefs added However, this should be guaranteed not to
	// happen by the caller The caller must ensure that the more
	// unreference task are not added during this time frame.
	//
	// block_processing only blocks new tasks from getting serviced.
	// We now wait till all tasks in-progress are done.
	//
	quiesce(QEMPTY);

	// Now reenable processing
	unblock_processing();
}

//
// set_unref_done
//
// NOTE: We do not get the threadpool lock here in order to avoid lock
// contention. During periods of high unreference activity, there can
// be 1000s of requests per second for this lock. It is ok if the
// value of min_num_idle is not accurate. If the vaue is inaccurately
// set to true, system will think that unref tasks were processed and
// may try to free up some threads processing unreference. This may
// temporarily slow down unreference activity. If it was inaccurately
// set to false, then the system will try to increase number of
// threads processing unreferences.
//
void
unref_threadpool::set_unref_done()
{
	unref_done = true;
}

//
// adjust_min_idle - used to maintain the lowest count of idle threads
// during this period.
//
// NOTE: We do not get the threadpool lock here in order to avoid lock
// contention. During periods of high unreference activity, there can
// be 1000s of requests per second for this lock. It is ok if the
// value of min_num_idle is not accurate. The result will be that the
// system will not free up threads soon enough. This will not affect
// the unref processing.
//
void
unref_threadpool::adjust_min_idle()
{
	int	count_idle = number_idle_threads();
	if (count_idle < min_num_idle) {
		min_num_idle = count_idle;
	}
}

#ifdef _KERNEL_ORB
//
// manage_thread_usage
// This method is periodically called to manage thread usage.
//
// Note: We only adjust the number of threads in 'increment' units.
//
// XXX - when the resource_balancer supports changes in the
// XXX - 'increment' value, we should use a different value
// XXX - for the unref_threadpool.
//
// The resource_balancer decides whether any adjustment
// is made to the number of threads.
//
// Parameters
//	increment - the unit for thread changes used in server thread pool
//	lent_threads - number threads lent from invocation server thread pool
// Returns:
//	0			- no change needed
//	positive increment number - the number of threads wanted
//	negative increment number - the number of threads can release
//
int
unref_threadpool::manage_thread_usage(int increment, int lent_threads)
{
	int	result	= 0;

	lock();

	if (number_idle_threads() == 0) {
		// All worker threads are now busy.

		if (all_busy_last_period && (!unref_done) &&
		    (task_count() > 0)) {
			//
			// All threads were busy at the start of this period
			// and no work was completed this period.
			//
			result = increment;

		} else if (task_count() >
		    3 * (uint_t)(increment + lent_threads + threads_min)) {
			//
			// The work backlog is excessive.
			// Unref tasks normally require little processing,
			// so want a few tasks for each worker thread
			// to make the thread transfer worthwhile.
			// We want to free memory by processing unreferences.
			//
			result = increment;
		}

		// Show that all threads were busy this last period.
		all_busy_last_period = true;

	} else {
		// All worker threads are not now busy.
		all_busy_last_period = false;

		if (min_num_idle >= increment &&
		    total_number_threads() >= increment + threads_min) {
			//
			// The unref_threadpool has more than an increment
			// number of idle threads and would still retain
			// the minimum number of threads after releasing
			// "increment" number of threads.
			//
			result = -increment;
		}
	}
	if (threads_min + lent_threads != total_number_threads()) {
		//
		// Do not ask for any transfers when a worker thread
		// transfer is already in progress.
		//
		result = 0;
	}

	// Reset unref_done
	unref_done = false;

	// Reset the minimum number of idle threads this period
	min_num_idle = number_idle_threads();

	unlock();

	return (result);
}

//
// read_state - provide state about unref_threadpool
//
int
unref_threadpool::read_state(void *argp)
{
	state_unref	state;

	// Obtain the number of tasks queued in the unref_threadpool
	state.length = task_count();

	return (copyout(&state, argp, sizeof (state_unref)));
}
#endif	// _KERNEL_ORB

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <orb/refs/unref_threadpool_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
