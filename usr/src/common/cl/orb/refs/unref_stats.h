/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNREF_STATS_H
#define	_UNREF_STATS_H

#pragma ident	"@(#)unref_stats.h	1.10	08/05/20 SMI"

#if defined(__cplusplus)

#include <sys/os.h>

class unref_stats {
public:
	unref_stats();

	void	init();

#ifdef _KERNEL_ORB
	int	read_stats(void *argp);
#endif

	void	histogram_queue_length(uint_t length);
	void	histogram_handler_wait(os::hrtime_t total_time);
	void	histogram_xdoor_wait(os::hrtime_t total_time);
	void	histogram_handler_work(os::hrtime_t total_time);
	void	histogram_xdoor_work(os::hrtime_t total_time);

	enum {HIST_SIZE = 8};

	//
	// Currently, the system does not lock the histograms during updates.
	// The histograms are always incremented.
	// So this means that the system might lose an increment
	// when contending threads attempt to update the same histogram.
	// That is not considered important at this time. Simply add
	// locking or atomic writes if this does become important.
	//

	// histograms for time waiting to begin unref activity
	uint_t		hist_wait_handler[HIST_SIZE];
	uint_t		hist_wait_xdoor[HIST_SIZE];

	// histograms for queue length when unref job is queued
	uint_t		hist_queue_length[HIST_SIZE];

	// histograms for time taken to perform just the unref activity
	uint_t		hist_work_handler[HIST_SIZE];
	uint_t		hist_work_xdoor[HIST_SIZE];
};

extern "C" {
#endif /* __cplusplus */

/*
 * Functions support cladmin commands
 */
#if defined(_KERNEL_ORB)
int	cl_read_orb_stats_unref(void *argp);
#endif

#if defined(__cplusplus)
}
#endif

#endif	/* _UNREF_STATS_H */
