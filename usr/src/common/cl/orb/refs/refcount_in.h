/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  refcount_in.h
 *
 */

#ifndef _REFCOUNT_IN_H
#define	_REFCOUNT_IN_H

#pragma ident	"@(#)refcount_in.h	1.28	08/05/20 SMI"

//
// refcnt_control inline methods
//

inline
refcnt_control::refcnt_control() : content(0)
{
}

inline void
refcnt_control::set_refs(uint_t u)
{
	refs = u;
}

inline uint_t
refcnt_control::get_refs() const
{
	return (refs);
}

inline void
refcnt_control::set_translate_acks(uint_t u)
{
	translate_acks = u;
}

inline uint_t
refcnt_control::get_translate_acks() const
{
	return (translate_acks);
}

inline uchar_t
refcnt_control::get_flags() const
{
	return (content);
}

//
// set_piggy_reply - initializes the value of the "content" field.
//
inline void
refcnt_control::set_piggy_reply()
{
	content |= refcnt_control::REPLY_PIGGY;
}

// Returns whether there is a refcnt confirmation in this message
inline bool
refcnt_control::has_piggy_reply()
{
	return ((content & refcnt_control::REPLY_PIGGY) != 0);
}

//
// set_urgent - Sets the urgent flag when want immediate confirmation.
// This method must be called after method "set_piggy_reply".
//
inline void
refcnt_control::set_urgent()
{
	ASSERT((content == 0) || (content == refcnt_control::REPLY_PIGGY));

	content |= refcnt_control::URGENT;
}

// Returns whether the sender wants immediate confirmation
inline bool
refcnt_control::has_urgent()
{
	return ((content & refcnt_control::URGENT) != 0);
}

inline
translate_ack::translate_ack()
{
}

//
// Marshal a translate_ack job.
//
inline void
translate_ack::_put(MarshalStream &wms)
{
	wms.put_unsigned_longlong(ack_cookie);
}

inline void
nodejobs::lock()
{
	lck.lock();
}

inline void
nodejobs::unlock()
{
	lck.unlock();
}

inline bool
nodejobs::lock_held()
{
	return (lck.lock_held());
}

inline uint_t
nodejobs::take_slots(uint_t *potential_jobs, uint_t *num_jobs,
    uint_t avail_slots)
{
	uint_t	reqs;

	reqs = MIN(*potential_jobs, avail_slots);
	*num_jobs += reqs;
	*potential_jobs -= reqs;
	return (reqs);
}

inline bool
nodejobs::flush_check()
{
	return ((node.incn != INCN_UNKNOWN) && !next_flush_time.is_future());
}

inline refcount &
refcount::the()
{
	ASSERT(the_refcount != NULL);
	return (*the_refcount);
}

// remove a dependent register for the node.
inline void
refcount::remove_dependent_reg(ID_node &lent_node, nodeid_t ndid)
{
	ndtab[lent_node.ndid].remove_dependent_reg(lent_node, ndid);
}

// sets up a unreg job for the server xdoor
inline void
refcount::add_unreg(ID_node &server_node, rxdoor_descriptor &door_desc)
{
	ndtab[server_node.ndid].add_unreg(server_node, door_desc);
}

// sets up a translate ack job
inline void
refcount::add_translate_ack(ID_node &nd, uint64_t ackid)
{
	ndtab[nd.ndid].add_translate_ack(nd, ackid);
}

#endif	/* _REFCOUNT_IN_H */
