/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _UNREF_THREADPOOL_H
#define	_UNREF_THREADPOOL_H

#pragma ident	"@(#)unref_threadpool.h	1.35	08/05/20 SMI"

#include <sys/os.h>
#include <sys/threadpool.h>
#include <orb/refs/unref_stats.h>

class handler;

//
// unref_task - This task represents an unreference notification
// from either a handler or a xdoor.
//
class unref_task : public defer_task {
	friend class service_unref;
public:
	enum unref_task_type_t {HANDLER, XDOOR};

	virtual void		execute();
	virtual void		task_done();

	virtual handler		*get_unref_handler() = 0;
	//
	// Handlers and server xdoors inherit from unref_task. They use
	// deliver_unreferenced() method to process unref
	// notification. If a xdoor or handler decides that it can be
	// deleted, it schedules itself on the unref
	// threadpool. Threadpool's deferred_task_handler() method
	// calls execute() for each task on the list. execute() method
	// for unref_task calls deliver_unreferenced().
	//
	virtual void		deliver_unreferenced();

	virtual void		unref_rejected();

	void			set_enque_time();

	static void		update_histogram_wait(unref_task_type_t,
				    os::hrtime_t);
	static void		update_histogram_work(unref_task_type_t,
				    os::hrtime_t);

protected:
	// An unref_task should not be created by itself.
	unref_task();

	virtual unref_task_type_t	unref_task_type() = 0;

	os::hrtime_t			enque_time;
};

typedef unref_task::unref_task_type_t unref_task_type_t;

//
// unref_threadpool -
// A threadpool used to asynchronously deliver unreferenced to
// implementation objects or handlers. This avoids lock ordering problems.
// A server handler or xdoor adds an unref_task to the threadpool.
// The threadpool will call the execute method on each queued unref_task.
//
// The unreference protocol is multi-thread safe.
//
// Lock order requirement - An unref_task is added to the
// unref_threadpool when the unref_task lock is already held. In order
// to avoid deadlock, one cannot acquire the unref_task lock (xdoor or
// handler) after acquiring the unref_threadpool lock.
//
// See comments regarding flush regarding tags associated with each unref_task
//
class unref_threadpool : public threadpool {
public:
	static unref_threadpool &the();

	unref_threadpool();

	virtual ~unref_threadpool();

	static int initialize();
	int	initialize_int();
	static void unref_shutdown();

	void	defer_unref_processing(unref_task *);

	//
	// Each unref_tasks has a tag.
	// flush_tag will wait till all currently processed tasks are done
	// and iterate over the rest of the tasks, remove any that
	// match the tag and return them on a list.
	//
	// Only hxdoors use this feature. Everything else uses the DEFAULT_TAG.
	// If this changes in the future some management scheme on the tags
	// must be imposed.
	//
	void    flush_tag(uint_t tag, task_list_t *);

	// Sets unref_done to true to indicate that an unref task was
	// executed recently.
	void	set_unref_done();

	// used to maintain the lowest count of idle threads during this period.
	void	adjust_min_idle();

#ifdef _KERNEL_ORB
	//
	// This method is periodically called to manage thread usage.
	// Returns:
	//	0		- no change needed
	//	positive increment number - the number of threads wanted
	//	negative increment number - the number of threads can release
	//
	// Note: We only adjust the number of threads in 'increment' units.
	//
	int	manage_thread_usage(int increment, int lent_threads);

	// Command used by orbadmin to obtain the state of the unref_threadpool
	int	read_state(void *argp);
#endif

	unref_stats	stats;

private:
	static unref_threadpool *the_unref_threadpool;

	// Set to true if at least one unref task completed in the time period
	bool		unref_done;

	// Minimum number of idle thread during the latest time period
	int		min_num_idle;

	// The minimum number of threads that the unref_threadpool must retain
	int		threads_min;

	// True when at the end of the last time period all threads were busy
	bool		all_busy_last_period;

	// Disallow assignments and pass by value
	unref_threadpool(const unref_threadpool &);
	unref_threadpool &operator = (unref_threadpool &);
};

#ifndef NOINLINES
#include <orb/refs/unref_threadpool_in.h>
#endif  // _NOINLINES

#endif	/* _UNREF_THREADPOOL_H */
