/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  unref_threadpool_in.
 *
 */

#ifndef _UNREF_THREADPOOL_IN_H
#define	_UNREF_THREADPOOL_IN_H

#pragma ident	"@(#)unref_threadpool_in.h	1.21	08/05/20 SMI"

// unref_task methods

//
// deliver_unreferenced
//   process unreference for unref_task
//
inline void
unref_task::deliver_unreferenced()
{
	ASSERT(0);
}

// unref_threadpool methods
//

// static
inline unref_threadpool &
unref_threadpool::the()
{
	ASSERT(the_unref_threadpool != NULL);
	return (*the_unref_threadpool);
}

//
// defer_processing - record the time before queueing the defer_task
// when unref statistic collections are active.
//
inline void
unref_threadpool::defer_unref_processing(unref_task *taskp)
{
	taskp->set_enque_time();

	defer_processing(taskp);
}

#endif	/* _UNREF_THREADPOOL_IN_H */
