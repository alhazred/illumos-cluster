//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)refcount.cc	1.157	08/05/20 SMI"

#include <orb/refs/refcount.h>
#include <orb/buffers/marshalstream.h>
#include <orb/member/members.h>
#include <orb/flow/resource.h>
#include <orb/flow/resource_mgr.h>
#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/rxdoor.h>
#include <orb/xdoor/rxdoor_kit.h>
#include <orb/xdoor/translate_mgr.h>
#include <orb/debug/orb_trace.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/clusterproc.h>
#include <orb/debug/orb_stats_mgr.h>

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_rpc.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif


// These constants define the permissible range for sleeping between flushing
// reference count jobs.
const os::usec_t	autom_thread_sleep_max = 512000;
const os::usec_t	autom_thread_sleep_min = 16000;

// There is one reference counting object per node.
refcount *refcount::the_refcount = NULL;

// ref_jobs methods
template<class A>
ref_jobs<A>::ref_jobs(os::mutex_t *lckp) :
	lockp(lckp),
	num_free_blocks(0),
	count(0)
{
}

template<class A>
ref_jobs<A>::~ref_jobs()
{
	release();
}

//
// add_active_block - A new block is added to the list of active blocks.
//
template<class A>
void
ref_jobs<A>::add_active_block(ref_block *ref_blockp)
{
	ASSERT(ref_blockp != NULL);
	ref_blockp->head = 0;
	ref_blockp->tail = 0;
	ref_active_list.append(ref_blockp);
}

//
// initialize - The dynamic data structures are initialized and
// the specified number of arrays are allocated.
//
template<class A>
void
ref_jobs<A>::initialize(int number_arrays)
{
	ASSERT(number_arrays >= 2);

	ASSERT(count == 0);

	// Initialize the active list with one data structure with no entries
	ASSERT(ref_active_list.empty());
	add_active_block(new ref_block);

	// Allocate the free list
	for (int i = 1; i < number_arrays; i++) {
		num_free_blocks++;
		ref_free_list.append(new ref_block);
	}
}

//
// release - Heap allocated resources are freed and related control information
// is marked to show that no heap allocated resources are in use.
//
template<class A>
void
ref_jobs<A>::release()
{
	num_free_blocks = 0;

	ref_free_list.dispose();
	ref_active_list.dispose();
	count = 0;
}

//
// add_to_tail - Returns the current tail entry, and advances the tail
// by one entry. The caller is expected to fill in the entry.
//
// The nodejobs lock must be held until the caller has filled in the entry.
// After the nodejobs lock is released the entry can be processed and removed.
//
template<class A>
A *
ref_jobs<A>::add_to_tail()
{
	ASSERT(lockp->lock_held());

	// Get the last element in the list
	ref_block	*elemp = ref_active_list.get_current();
	uint_t		offset = elemp->tail;

	// If this array is full get another one
	if (++(elemp->tail) >= NUMBER_REFS_PER_BLOCK) {
		ref_block	*ref_blockp = ref_free_list.reapfirst();
		if (ref_blockp == NULL) {
			ref_blockp = new ref_block;
		} else {
			ASSERT(num_free_blocks > 0);
			num_free_blocks--;
		}
		add_active_block(ref_blockp);
		ref_active_list.atlast();
	}
	ASSERT(elemp->head < elemp->tail);

	count++;
	ASSERT(count != 0); // Check for wrap.

	// Zero out the entry
	bzero(&(elemp->job[offset]), sizeof (A));

	// Returns the entry, and the caller is expected to fill in the values.
	return (&(elemp->job[offset]));
}

//
// Get the number of entries. No need to hold the lock (caller must decide if
// he cares).
//
template<class A>
uint_t
ref_jobs<A>::get_count() const
{
	return (count);
}

//
// flush_jobs - flush reference jobs to the marshal stream.
// No need for lock to be held, so long as caller can guarantee that at least
// num_jobs jobs exist.
//
template<class A>
void
ref_jobs<A>::flush_jobs(uint_t num_jobs, MarshalStream &mainbuf)
{
	//
	// Marshal jobs
	//
	// Iterate through list reading oldest entries.
	//
	iterator iter(this);
	for (int i = 0; i < num_jobs; i++) {
		A	*jobp = iter.get_current();
		ASSERT(jobp != NULL);
		jobp->_put(mainbuf);
		iter.advance();
	}
}

//
// purge_jobs - purge the specified number of reference jobs from the beginning
// of the list.
//
template<class A>
void
ref_jobs<A>::purge_jobs(uint_t purge_num)
{
	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("purge jobs, count = %d-%d=%d\n", count, purge_num,
	    count - purge_num));

	ASSERT(lockp->lock_held());

	ASSERT(count >= purge_num);
	count -= purge_num;

	ref_active_list.atfirst();
	while (purge_num > 0) {
		ref_block	*elemp = ref_active_list.get_current();
		ASSERT(elemp != NULL);

		ASSERT(elemp->head < elemp->tail);
		uint_t block_elems = elemp->tail - elemp->head;
		uint_t purge_elems = MIN(block_elems, purge_num);
		elemp->head += purge_elems;
		purge_num -= purge_elems;

		// Check for end of this element
		ASSERT(elemp->head <= NUMBER_REFS_PER_BLOCK);
		if (elemp->head == NUMBER_REFS_PER_BLOCK) {

			// Do we need to purge an element?
			if (num_free_blocks < NODEJOBS_MAX_FREE_BLOCKS) {
				num_free_blocks++;
			} else {
				// An element is deleted from the free list
				// so that we can keep the current entry in the
				// current element alive for now.
				delete ref_free_list.reapfirst();
			}
			// Move this element to the free list
			ref_free_list.append(ref_active_list.reapfirst());

			if (ref_active_list.empty()) {
				// The active list must contain at least one
				// element
				add_active_block(ref_free_list.reapfirst());
				ASSERT(num_free_blocks > 0);
				num_free_blocks--;
			}
			ASSERT(ref_active_list.get_current() != NULL);
		}
	}
	ref_active_list.atlast();
}


//
// Advance the iterator to the next job, jumping to the next element if
// necessary. May advance past the last job.
//
template<class A>
void
ref_jobs<A>::iterator::advance()
{
	ASSERT(li.get_current() != NULL);
	// Check for end of entries in this block
	ASSERT(index < NUMBER_REFS_PER_BLOCK);
	if (index == NUMBER_REFS_PER_BLOCK - 1) {
		ASSERT(li.get_current()->tail == NUMBER_REFS_PER_BLOCK);
		ORB_DBPRINTF(ORB_TRACE_ITERATE, ("advance: next elem\n"));

		index = 0;
		// Get the next element in the list
		li.advance();
		ASSERT((li.get_current() == NULL) ||
		    (li.get_current()->head == 0));
	} else {
		ASSERT(index < li.get_current()->tail);

		ORB_DBPRINTF(ORB_TRACE_ITERATE, ("advance: index %d tail %d\n",
		    index, li.get_current()->tail));

		// Advance the entry index.
		// Note that this can push the index past the end of the list,
		// but the code will catch that on the next iteration.
		index++;
	}
}

//
// Get the job currently pointed to by the iterator. Returns NULL if the
// iterator is at the end.
//
template<class A>
A *
ref_jobs<A>::iterator::get_current() const
{
	ref_jobs<A>::ref_block	*currentp = li.get_current();
	if ((currentp == NULL) || (index >= currentp->tail)) {
		// Iterated past all valid entries
		return (NULL);
	}
	return (&(currentp->job[index]));
}


// nodejobs

nodejobs::nodejobs() :
    confirm(false),
    outbound_msg_underway(false),
    refs_deferred(false),
    number_refs_sent(0),
    wait_index(-1),
    canceled_seqnum(0),
    thread_sleep(autom_thread_sleep_max),
    regs_needed_total(0),
    snapshot_needed_total(0),
    snapshot_ack_count(0),
    refs(&lck),
    translate_acks(&lck)
{

#ifdef FAULT_RECONF
	fi_blocked = false;
#endif
	for (nodeid_t i = 0; i <= NODEID_MAX; i++) {
		regs_needed[i] = 0;
		snapshot_regs_needed[i] = 0;
	}
}

nodejobs::~nodejobs()
{
}

//
// init - initialize the reference counting state and allocate resources
//
void
nodejobs::init()
{
	ASSERT(lock_held());

	// Set the state variables
	number_refs_sent = 0;

	confirm = false;

	// Allocate data structures
	refs.initialize(NODEJOBS_INITIAL_BLOCKS);
	translate_acks.initialize(NODEJOBS_INITIAL_BLOCKS);
	for (nodeid_t i = 0; i <= NODEID_MAX; i++) {
		regs_needed[i] = 0;
		snapshot_regs_needed[i] = 0;
	}
	regs_needed_total = 0;
	snapshot_needed_total = 0;
	snapshot_ack_count = 0;
	thread_sleep = autom_thread_sleep_max;
	next_flush_time.setreltime(thread_sleep);

	// The remaining members don't need to be initialized.
	// No need to initialize this since it is guaranteed to be false.
	ASSERT(!outbound_msg_underway);
	// Set this to false for completeness, though it is not strictly
	// necessary since it is just a hint.
	refs_deferred = false;

	// Guaranteed to be -1.
	ASSERT(wait_index == -1);
}

//
// The node has died. Clean up and reset the data structures associated with
// it.
//
void
nodejobs::cleanup_dead_incn()
{
	ASSERT(lock_held());

	ORB_DBPRINTF(ORB_TRACE_REFCNT, ("nj %d.%d: cleanup_dead_incn()\n",
	    node.ndid, node.incn % 1000));

	// Wait for any flush_node()s in progress to drain out.
	while (outbound_msg_underway) {
		cv.wait(&lck);
	}

	// Setting the incarnation number to INCN_UNKNOWN will ensure that any
	// attempts to add jobs will fail. This is needed because we drop the
	// lock while purging the refs below.
	node.incn = INCN_UNKNOWN;

	unlock();

	//
	// If we have REG jobs queued to the dead node, tell the xdoors that
	// their registration has completed. This ensures that we don't have
	// any client xdoors in an intermediate state. We do this by pretending
	// that all refs have received an ACK from the dead incarnation.  Note
	// that it is safe to call get_count() where without the lock held
	// because we know that no jobs will be added (node.incn is
	// INCN_UNKNOWN) and that no jobs will be purged by an orphaned
	// incoming confirmation (we hold the write lock).
	//
	confirm_arrived(refs.get_count());

	lock();

	//
	// It is now safe to release the resources because the system will not
	// be communicating with this node. The node may later rejoin the
	// cluster, so we do not destroy basic data structures.
	//
	refs.release();
	translate_acks.release();
}

//
// initialize - Prior to any reference count activity, initialize node info.
//
void
nodejobs::initialize(nodeid_t mynode)
{
	node.ndid = mynode;
	node.incn = INCN_UNKNOWN;
}

//
// process_refmsg - Process incoming reference count message.
// The routine processes flag information, and delegates the processing of the
// reference count updates.
//
// A CMM reconfiguration can occur while this call is in progress. To avoid
// conflicting with that, we grab a read lock while processing the messages.
// The CMM reconfiguration will grab the write lock. Using a read lock allows
// us to process multiple messages from the same node concurrently.
// We perform an orphan check under the read lock and the reconfiguration code
// sets the membership under the write lock.
//
void
nodejobs::process_refmsg(refcnt_control *ref_hdrp, MarshalStream &rms,
    incarnation_num from_incn)
{
	uint_t	n_refs = ref_hdrp->get_refs();
	uint_t	n_translate_acks = ref_hdrp->get_translate_acks();
	bool	msg_confirmed = ref_hdrp->has_piggy_reply();
	bool	flush_now = false;

	rwlck.rdlock();

	if (from_incn != node.incn) {
		rwlck.unlock();
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: ignoring ref msg: from_incn = %d\n",
		    node.ndid, node.incn % 1000, from_incn));
		orb_stats_mgr::the().inc_orphans(node.ndid, REFJOB_MSG);
		return;
	}

	ORB_DBPRINTF(ORB_TRACE_REFMSG,
	    ("nj %d.%d: Rec refcnts flag %d ref %d translate_ack %d\n",
	    node.ndid, node.incn % 1000, ref_hdrp->get_flags(), n_refs,
	    n_translate_acks));

	// Messages must have some work.
	ASSERT(msg_confirmed || n_refs != 0 || n_translate_acks != 0);

	if (msg_confirmed) {
		// If we have refs which haven't been sent because of this,
		// we send them immediately.
		if (refs_deferred)
			flush_now = true;

		// Post-process all waiting registers.
		confirm_arrived(number_refs_sent);
	}
	process_refs(n_refs, rms);
	process_translate_acks(n_translate_acks, rms);

	if (n_refs != 0) {
		lock();
		// Show the need to confirm arrival of reference data.
		confirm = true;
		if (ref_hdrp->has_urgent()) {
			// They want it ASAP.
			flush_now = true;
		} else if (!flush_now) {
			// Indicate that we should flush on the next attempt.
			next_flush_time.setreltime(0L);
		}
		unlock();
	}
	rwlck.unlock();

	if (flush_now) {
		lock();
		flush_node(NULL);
		unlock();
	}
}

//
// confirm_arrived - Called when a confirmation is received from the
// remote node that it received our last message containing refs or
// during reconfiguration when the destination node has died.
//
// We purge the number of refs indicated.
//
// This is called with the read lock held, preventing reconfiguration
// from cleaning things up from under us.
//
void
nodejobs::confirm_arrived(uint_t confirm_num)
{
	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("nj %d.%d: confirm_arrived num %d\n",
	    node.ndid, node.incn % 1000, confirm_num));

	ref_jobs<rxdoor_ref>::iterator iter(&refs);
	for (uint_t i = 0; i < confirm_num; i++, iter.advance()) {
		//
		// Extract the ref job entry at the head of the list
		//
		rxdoor_ref	*refp = iter.get_current();
		ASSERT(refp != NULL);

		if (refp->is_reg) {
			// translate_acks are blocked somewhere waiting for
			// this confirmation. This removes the dependency.
			refcount::the().remove_dependent_reg(refp->lent_node,
			    node.ndid);
		}

		//
		// If we just acknowledged the register that we are
		// waiting for to complete quiesce_step of
		// reconfiguration, then update the state and notify
		// the waiting thread. It is safe to read wait_index
		// without holding the lock, as it is set to a non -1
		// value with the write lock held and we hold the read
		// lock.  The serialization protocol for ref messages
		// means that there can only be one confirm_arrived()
		// in progress at a time, so we don't need to hold the
		// lock when decrementing the count either. We do of
		// course, need to hold the lock across the
		// cv.signal().
		//
		if (wait_index != -1) {
			wait_index--;
			ORB_DBPRINTF(ORB_TRACE_REFCNT,
			    ("nj %d.%d: wait_index = %d\n", node.ndid,
			    node.incn, wait_index));
			if (wait_index == -1) {
				// We have processed the reference we were
				// waiting for.
				lock();
				cv.signal();
				unlock();
			}
		}
	}
	lock();
	number_refs_sent = 0;
	refs.purge_jobs(confirm_num);
	unlock();
}

//
// process_refs
// Process reference count jobs from another node.
// The jobs are combinations of registers and unregisters. This is indicated by
// a boolean in the marshal stream.
//
// The jobs are directed at server xdoors on this node and each job
// contains the kit id of the xdoor. This is followed by the xdoor_id
// of the xdoor. If this job is for an hxdoor, it also contains the
// serviceid. The kit id is used to obtain a kit for this type xdoor
// and the kit is used to lookup the target server xdoor.
//
// Called with the read lock held and the mutex not held.
// On return, the marshal stream has been advanced to the end of the job
// information.
//
void
nodejobs::process_refs(uint_t num_jobs, MarshalStream &rms)
{
	rxdoor		*xdp;
	rxdoor_bucket	*xbp;
	rxdoor_ref	ref;
	xkit_id_t	kitid;

	while (num_jobs-- != 0) {
		// read refjob from marshal stream
		ref._get(rms);

		kitid = ref.get_kit_id();
		rxdoor_kit *kitp = (rxdoor_kit *)
		    (xdoor_repository::the().get_kit(kitid));

		xdp = kitp->lookup_rxdoor_from_refjob(&ref, &xbp);

		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Process client job xdid=%d svc=%d "
		    "is_reg = %d\n",
		    node.ndid, node.incn % 1000, ref.xdid, ref.serviceid,
		    ref.is_reg));

		if (ref.is_reg) {
			xdp->register_client(node);
		} else {
			xdp->unregister_client(node);
		}
		xbp->unlock();
	}
}

//
// Process incoming translate_ack jobs by passing them to the translate
// manager.
// Called with the read lock held and the mutex not held.
// On return, the marshal stream has been advanced to the end of the job
// information.
//
void
nodejobs::process_translate_acks(uint_t num_jobs, MarshalStream &rms)
{
	translate_mgr	&ttm = translate_mgr::the();
	tr_out_info	*translate_datap;

	while (num_jobs-- != 0) {
		translate_datap = (tr_out_info *)
		    (uintptr_t)rms.get_unsigned_longlong();

		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Process translate ack %p count=%d\n",
		    node.ndid, node.incn % 1000, translate_datap, num_jobs));

		ttm.ack_translates(node, translate_datap);
	}
}

//
// add_reg - Queue an ref reference job for later transmission
//
bool
nodejobs::add_reg(ID_node &server_node, rxdoor_descriptor &xdoor_desc,
	ID_node &lent_node = invalid_node)
{
	lock();

	//
	// Inc reference jobs only go to the node hosting the object.
	//
	ASSERT(node.ndid == server_node.ndid);

	// We discard refs if the server node is no longer alive.
	if (server_node.incn == node.incn) {
		// Load the register job
		rxdoor_ref *refp = refs.add_to_tail();
		refp->is_reg	= true;
		refp->lent_node	= lent_node;
		xdoor_desc.put_ref_job(refp);

		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Added reg xdid=%d svc=%d count=%d\n",
		    node.ndid, node.incn % 1000, xdoor_desc.xdid,
		    refp->serviceid, refs.get_count()));
		unlock();
		return (true);
	}
	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("nj %d.%d: Didn't add reg xdid=%d incn %d\n",
	    node.ndid, node.incn % 1000, xdoor_desc.xdid, server_node.incn));
	unlock();
	return (false);
}

//
// add_unreg - Queue an unreg reference job for later transmission
//
void
nodejobs::add_unreg(ID_node &server_node, rxdoor_descriptor &xdoor_desc)
{
	lock();

	if (server_node.incn == node.incn) {
		// Load the unreg job
		rxdoor_ref *refp = refs.add_to_tail();
		refp->is_reg = false;
		xdoor_desc.put_ref_job(refp);

		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Added unreg xdid=%d svc=%d count=%d\n",
		    node.ndid, node.incn % 1000, xdoor_desc.xdid,
		    refp->serviceid, refs.get_count()));
	} else {
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Didn't add unreg xdid=%d incn %d\n",
		    node.ndid, node.incn % 1000, xdoor_desc.xdid,
		    server_node.incn));
	}
	unlock();
}

//
// add_translate_ack - Queue a translate ack job for later transmission
//
void
nodejobs::add_translate_ack(ID_node &nd, uint64_t ackid)
{
	lock();

	if (nd.incn == node.incn) {
		// Load the ack job
		translate_ack *ackp = translate_acks.add_to_tail();
		ackp->ack_cookie = ackid;

		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Added translate_ack job %llx count=%d\n",
		    node.ndid, node.incn % 1000, ackid,
		    translate_acks.get_count()));
	} else {
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: Didn't add translate_ack job incn %d id "
		    "%llx\n", node.ndid, node.incn % 1000, nd.incn, ackid));
	}
	unlock();
}

//
// Called when a REG job is queued to a node which needs to place a dependency
// on an upcoming translate_ack for this node.
//
void
nodejobs::add_dependent_reg(ID_node &nd, nodeid_t ndid)
{
	lock();
	if (nd.incn == node.incn) {
		regs_needed[ndid]++;
		regs_needed_total++;
		ASSERT(regs_needed_total != 0);
	}
	unlock();
}

//
// Called when a REG job has been confirmed from a remote node and or the node
// has died. We can now remove the dependency from our translate_acks.
//
void
nodejobs::remove_dependent_reg(ID_node &nd, nodeid_t ndid)
{
	lock();
	if (nd.incn == node.incn) {
		// Remove from the snapshot if possible.
		if (snapshot_regs_needed[ndid] != 0) {
			snapshot_regs_needed[ndid]--;
			ASSERT(snapshot_needed_total != 0);
			snapshot_needed_total--;
			if (snapshot_needed_total == 0 &&
				snapshot_ack_count != 0) {
				// We get to send our acks now. Get going ASAP.
				next_flush_time.setreltime(0L);
			}
		} else {
			ASSERT(regs_needed[ndid] != 0);
			regs_needed[ndid]--;
			ASSERT(regs_needed_total != 0);
			regs_needed_total--;
		}
	}
	unlock();
}

//
// flush_node - determines whether there is need to transmit
// a confirmation message or a message containing reference count job data.
//
// Note: This method drops the lock when transmitting a message.
//
void
nodejobs::flush_node(os::systime *time_outp)
{
	ASSERT(lock_held());

	// No need to send information to self
	if (node.ndid == orb_conf::node_number()) {
		return;
	}

	//
	// We use the outbound_msg_underway flag to make sure that there is
	// only one call to flush_data() at a time for each node.
	//
	if (outbound_msg_underway)
		return;

	outbound_msg_underway = true;
	if (flush_data()) {
		// We had too much for a single buffer. Don't advance the
		// timer. This will make sure that we are called again very
		// soon.
		next_flush_time.setreltime(0L);

		// Shorten sleep period so that next time through we have a
		// lower timer.
		thread_sleep = MAX(thread_sleep / 2, autom_thread_sleep_min);
	} else {
		// Lengthen sleep period
		thread_sleep = MIN(thread_sleep * 2, autom_thread_sleep_max);
		next_flush_time.setreltime(thread_sleep);
	}
	if (time_outp != NULL && next_flush_time.is_before(*time_outp)) {
		time_outp->set(next_flush_time);
	}
	outbound_msg_underway = false;

	// Notify the reconfiguration thread that outbound_msg_underway is
	// false.
	cv.signal();
}

//
// Calculates the number of each type of job which we will transmit to the
// node. Imposes a maximum message size.
//
bool
nodejobs::allocate_message_slots(refcnt_control *hdrp)
{
	uint_t	potential_refs, potential_acks;
	uint_t	num_refs, num_acks;
	bool	full_msg;

	potential_refs = potential_acks = 0;
	num_refs = num_acks = 0;

#ifdef FAULT_RECONF
	// This fault point prevents us from sending confirmation messages.
	if (!fault_triggered(FAULTNUM_RECONF_DEFER_CONFIRM, NULL, NULL)) {
		if (confirm) {
			hdrp->set_piggy_reply();
		}
	}
#else
	if (confirm) {
		hdrp->set_piggy_reply();
	}
#endif // FAULT_RECONF

	// We can only have one batch of refs in transit at a time.
	refs_deferred = false;
	if (number_refs_sent == 0) {
		potential_refs = refs.get_count();
	} else {
		// Keep track of the fact that we have failed to send out refs
		// because of waiting for an ack. We will use this later to be
		// more aggressive in pushing out these jobs when the ack comes
		// in.
		if (refs.get_count() != 0)
			refs_deferred = true;
	}

	//
	// If we have no acks in our snapshot then take a new snapshot.
	// If there acks which are not in the current snapshot have no
	// dependencies, then we move them into the current snapshot.
	//
	if (snapshot_ack_count == 0 || regs_needed_total == 0) {
		snapshot_ack_count = translate_acks.get_count();
		if (regs_needed_total != 0) {
			for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
				snapshot_regs_needed[i] += regs_needed[i];
				regs_needed[i] = 0;
			}
			snapshot_needed_total += regs_needed_total;
			regs_needed_total = 0;
		}
	}

	// We can only send translate_acks if all regs needed to other nodes
	// have been acked.
	if (snapshot_needed_total == 0) {
		potential_acks = snapshot_ack_count;
	}

#ifdef FAULT_RECONF
	// This fault point prevents us from sending translate_ack messages.
	if (fault_triggered(FAULTNUM_RECONF_DEFER_ACK, NULL, NULL)) {
		ORB_DBPRINTF(ORB_TRACE_REFMSG,
		    ("nj %d.%d: refcnt flush: defer acks\n", node.ndid,
		    node.incn % 1000));
		potential_acks = 0;
	}
#endif // FAULT_RECONF

#ifdef DRC_DEBUG
	if (confirm || refs.get_count() != 0 || number_refs_sent != 0 ||
	    translate_acks.get_count() != 0 || snapshot_ack_count != 0 ||
	    snapshot_needed_total != 0) {
		ORB_DBPRINTF(ORB_TRACE_REFMSG,
		    ("nj %d.%d: refcnt flush: con %d ref %d(unacked %d) "
		    "ack %d snap ack %d needed %d\n",
		    node.ndid, node.incn % 1000, confirm, refs.get_count(),
		    number_refs_sent, translate_acks.get_count(),
		    snapshot_ack_count, snapshot_needed_total));
	}
#endif
	//
	// Now we have the maximum number we could send. Try to fit them in
	// the maximum sized buffer.
	//
	uint_t	control_marshal_size = (uint_t)sizeof (refcnt_control);
#ifdef MARSHAL_DEBUG
	// space for M_BYTES and length
	control_marshal_size += 2 * (uint_t)sizeof (uint_t);
#endif
	uint_t	remaining_space = refcnt_buf_size - control_marshal_size;

	// Distribute the rest of the marshal stream fairly over the remaining
	// slots.
	uint_t total_space = (potential_refs * rxdoor_ref::_sizeof_marshal) +
	    (potential_acks * sizeof_marshalled_ack);

	if (total_space <= remaining_space) {
		// Everything fits, easy.
		num_refs = potential_refs;
		num_acks = potential_acks;
		remaining_space -= total_space;
		full_msg = false;
	} else {
		// We have too many jobs for one message. Allocate them as
		// fairly, by trying to have the same number of each type of
		// job. This helps avoid constipation.
		//
		// Note that this code is written so that it should be
		// relatively easy to add more types of jobs. It could be more
		// efficient if we make it less flexible. Also, it is not
		// really very inefficient. We will only go through the loop
		// twice (the number of job types). We only do any of this if
		// we have too many jobs for a single message, in which case
		// the extra work here is relatively minor compared to that of
		// marshaling the jobs.
		uint_t	req_size;
		uint_t	slots;

		full_msg = true;

		// First do some special case preferential allocation.  If we
		// are doing a reconfiguration, then it is important to get our
		// refs out, so we allocate them here before we try to be fair.
		if (wait_index != -1) {
			slots = remaining_space / rxdoor_ref::_sizeof_marshal;
			remaining_space -=
			    take_slots(&potential_refs, &num_refs, slots) *
			    rxdoor_ref::_sizeof_marshal;
		}

		do {
			req_size = 0;
			slots = 0;
			if (potential_refs != 0) {
				req_size += rxdoor_ref::_sizeof_marshal;
			}
			if (potential_acks != 0) {
				req_size += sizeof_marshalled_ack;
			}
			// If we have some jobs left allocate the remaining
			// space to them fairly.
			if ((req_size > 0) &&
			    (slots = (remaining_space / req_size)) > 0) {
				remaining_space -= take_slots(&potential_refs,
				    &num_refs, slots) *
				    rxdoor_ref::_sizeof_marshal;

				remaining_space -= take_slots(&potential_acks,
				    &num_acks, slots) * sizeof_marshalled_ack;
			}
		} while (req_size > 0 && slots > 0);

		// Maybe squeeze a few more in.
		// This would only happen if some jobs were bigger than others.
		if (potential_refs != 0 &&
		    (remaining_space >= rxdoor_ref::_sizeof_marshal)) {
			slots = remaining_space / rxdoor_ref::_sizeof_marshal;
			remaining_space -= take_slots(&potential_refs,
			    &num_refs, slots) * rxdoor_ref::_sizeof_marshal;
		}

		if (potential_acks != 0 &&
		    (remaining_space >= sizeof_marshalled_ack)) {
			slots = remaining_space / sizeof_marshalled_ack;
			remaining_space -= take_slots(&potential_acks,
			    &num_acks, slots) * sizeof_marshalled_ack;
		}
	}

#ifdef DRC_DEBUG
	if (confirm || (num_refs != 0) || (num_acks != 0)) {
		ORB_DBPRINTF(ORB_TRACE_REFMSG,
		    ("nj %d.%d: refcnt msg: ref %d ack %d\n",
		    node.ndid, node.incn % 1000, num_refs, num_acks));
	}
#endif

	hdrp->set_refs(num_refs);
	hdrp->set_translate_acks(num_acks);

	if (num_refs != 0) {
		ASSERT(number_refs_sent == 0);
		number_refs_sent = num_refs;

		if ((wait_index != -1) || (potential_refs != 0)) {
			//
			// We want an immediate confirmation, either because an
			// active reconfiguration is dependent on it or because
			// we have more refs to send which didn't fit in the
			// message and they won't get sent until we receive a
			// confirmation.
			//
			hdrp->set_urgent();
		}
	}
	return (full_msg);
}

//
// flush_data - sends pending reference count job information
// that fits in one limited size message to the destination node.
//
// The system needs to place a limit on the number of outstanding messages,
// so that it can place an upper bound on the required resources. This enables
// the system to reserve sufficient resources so that a reconfiguration can
// be carried out with existing resources.
//
// The messages with ref(register/unregister) jobs must be serialized by the
// reference count subsystem, because the transport does not guarantee in-order
// message delivery. This is because the xdoor servers expect an alternating
// sequence of reg/unreg notifications. If the messages were delivered out of
// order, that would not be guaranteed and the xdoor would get confused.
//
// Note: This method drops the lock when transmitting a message.
//
// Returns true if we need to do another flush (more data to send).
//
bool
nodejobs::flush_data()
{
	ASSERT(lock_held());

	//
	// This top level loop support message retry in case of failure on any
	// particular communications path.
	// The lock is dropped while a message transmission attempt occurs to
	// avoid blocking the queueing of new work.
	// If a reconfiguration occurs which declares this node down, then we
	// will get a COMM_FAILURE and will return. The refcount part of the
	// reconfiguration waits for us to return in that case.
	//
	bool		need_retry;
	Environment	e;
	refcnt_control	hdr;

	bool full_msg = allocate_message_slots(&hdr);
	//
	// A msg requiring a confirmation can arrive while a reference count
	// message is being sent (since we drop the lock). Thus we must clear
	// the confirm flag before sending the msg.
	//
	if (hdr.has_piggy_reply()) {
		confirm = false;
	} else if ((hdr.get_refs() == 0) && (hdr.get_translate_acks() == 0)) {
		// Nothing to send.
		return (false);
	}

	//
	// Cannot hold the lock when creating a msg sendstream for the
	// following reason.
	// The reserve_resources method will ask the Endpoint Registry for an
	// Endpoint. The Endpoint Registry always returns either an Endpoint or
	// COMM_FAILURE, which represents a dead node. There exists a time
	// period between when the Endpoint Registry loses the last Endpoint
	// that links to the destination node and when the CMM declares the
	// node down. The Endpoint Registry blocks requests for an Endpoint
	// during this window.
	//
	// The destination node can die during a node
	// reconfiguration. The CMM will declare that node dead during
	// the next node reconfiguration.  The system sends reference
	// count messages during of node reconfigurations.  In
	// refcount_cleanup_step of node reconfiguration, the
	// reference counting subsystem needs to acquire the lock in
	// order to complete the step. If the step cannot be completed
	// or cancelled, the next reconfiguration cannot begin; and
	// the system deadlocks.
	//
	unlock();

	uint_t msg_size = (uint_t)sizeof (hdr) +
	    ((hdr.get_refs()) * rxdoor_ref::_sizeof_marshal) +
	    ((hdr.get_translate_acks()) * sizeof_marshalled_ack);


	do {
		// Identify message resources
		resources_datagram	resource(&e,
		    0,					// header size
		    msg_size,				// data size
		    node, REFJOB_MSG);

		//
		// Reference Count messages are not flow controlled.
		// The reference count subsystem serializes messages itself.
		//
		sendstream *send_stream = orb_msg::reserve_resources(&resource);
		if (send_stream == NULL) {
			// This should only occur when the node is dead.
			ASSERT(e.exception());
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			lock();
			return (false);
		}
		ASSERT(!e.exception());

		// Put the header.
		send_stream->put_bytes((void *)&hdr, (uint_t)sizeof (hdr));

		// Obtain marshal stream for data
		MarshalStream	&mainbuf = send_stream->get_MainBuf();

		// Flush the reference count information.
		// Reference count jobs must be flushed in this order.
		refs.flush_jobs(hdr.get_refs(), mainbuf);
		translate_acks.flush_jobs(hdr.get_translate_acks(), mainbuf);

		ORB_DBPRINTF(ORB_TRACE_REFMSG,
		    ("nj %d.%d: Sending refcnt\n", node.ndid,
		    node.incn % 1000));

		orb_msg::send(send_stream);
		need_retry = (CORBA::RETRY_NEEDED::_exnarrow(e.exception()) !=
		    NULL);

		ORB_DBPRINTF(ORB_TRACE_REFMSG,
		    ("nj %d.%d: Sent refcnt. need_retry %d\n",
		    node.ndid, node.incn % 1000, need_retry));
		//
		// Only the following exception are expected
		//
		ASSERT(!e.exception() ||
		    CORBA::COMM_FAILURE::_exnarrow(e.sys_exception()) ||
		    CORBA::RETRY_NEEDED::_exnarrow(e.sys_exception()));
		//
		// A COMM_FAILURE only occurs when a node died, in which case
		// orb reconfiguration will perform a cleanup.
		// The system will retry in case of other errors.
		//
		e.clear();
		send_stream->done();
	} while (need_retry);

	lock();

	//
	// Purge the ack jobs that were sent. Server refs must wait for a
	// confirm_arrived.
	//
	translate_acks.purge_jobs(hdr.get_translate_acks());
	ASSERT(snapshot_ack_count >= hdr.get_translate_acks());
	snapshot_ack_count -= hdr.get_translate_acks();

	//
	// A full msg signifies that the system should not wait as long for a
	// flush
	//
	return (full_msg);
}

//
// Called when the membership changes.
// If this node has died, clean up the associated state.
// If not, then we need to flush our REG messages which have dependencies
// for nodes which are now dead.
//
void
nodejobs::cleanup_dead_refcount(incarnation_num incn, cmm::seqnum_t seqnum)
{
	if (node.ndid == orb_conf::node_number()) {
		return;
	}

	if (incn != node.incn) {
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: cleanup_dead_refcount incn %u->%u:\n",
		    node.ndid, node.incn % 1000, node.incn, incn));
		//
		// By grabbing the write lock here we ensure that all incoming
		// messages for this node have been processed. We set the
		// incarnaton number with this lock held and that allows
		// incoming messages to perform orphan checking by looking at
		// the incarnation number with the read lock held.
		//
		rwlck.wrlock();

		//
		// We also hold the mutex when setting the incarnation number.
		// This is used by callers which want to queue jobs to a
		// specific incarnation number.
		//
		lock();

		if (node.incn != INCN_UNKNOWN) {
			// The node was running and has died. Purge any
			// obsolete info.
			cleanup_dead_incn();
		}
		if (incn != INCN_UNKNOWN) {
			// We have a new incarnation of the node. Allocate
			// resources for it.
			init();
		}
		node.incn = incn;
		unlock();
		rwlck.unlock();
	} else if (incn != INCN_UNKNOWN) {
		//
		// By grabbing the write lock here we ensure that no incoming
		// messages are being processed.
		// This allows us to scan the ref_jobs without worrying about
		// confirm_arrived() being called.
		//
		rwlck.wrlock();
		lock();
		//
		// The node is still alive. Search for any queued REG jobs for
		// xdoors which were received from a node that is now dead.
		// [The lent_node value refers to the node which sent the
		// reference.] We find the last such xdoor currently queued and
		// then wait for it to be acknowledged.
		//
		rxdoor_ref *refp;
		wait_index = -1;
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("nj %d.%d: refs=%d wait_index %d\n", node.ndid,
		    node.incn % 1000, refs.get_count(), wait_index));
		uint_t i;
		ref_jobs<rxdoor_ref>::iterator iter(&refs);
		for (i = 0; ((refp = iter.get_current()) != NULL);
		    iter.advance(), i++) {
			if (!refp->is_reg)
				continue;
			ORB_DBPRINTF(ORB_TRACE_REFCNT,
			    ("nj %d.%d: refs=%d i=%d wait_index %d, xdid=%d, "
			    "lent=%d.%d\n",
			    node.ndid, node.incn % 1000,
			    refs.get_count(), i, wait_index,
			    refp->xdid,
			    refp->lent_node.ndid, refp->lent_node.incn));
			if (!members::the().still_alive(refp->lent_node)) {
				wait_index = (int)i;
			}
		}
		ASSERT(i == refs.get_count());
		ASSERT(wait_index < (int)refs.get_count());

		//
		// Now that we have scanned the ref jobs, we need to drop the
		// write lock before we start waiting to allow the
		// confirmations to come in.
		//
		rwlck.unlock();

		//
		// We have identified the last queued register (if any) that
		// needs to be acknowledged. Now we wait for that to happen or
		// for the reconfiguration to be canceled.
		//
		while ((wait_index != -1) && (seqnum != canceled_seqnum)) {
			ORB_DBPRINTF(ORB_TRACE_REFCNT,
			    ("nj %d.%d: waiting wait_index=%d\n",
			    node.ndid, node.incn % 1000, wait_index));

#ifdef FAULT_RECONF
			fi_blocked = true;
			fault_cv.broadcast();
#endif
			cv.wait(&lck);
		}
#ifdef FAULT_RECONF
		fi_blocked = false;
		fault_cv.broadcast();
#endif
		unlock();
	}
}

#ifdef FAULT_RECONF

void
nodejobs::wait_for_step1_blocked()
{
	lock();
	while (!fi_blocked) {
		os::warning("nj %d.%d: waiting for step 1 to be blocked",
		    node.ndid, node.incn);
		fault_cv.wait(&lck);
	}
	os::warning("nj %d.%d: step 1 is blocked", node.ndid, node.incn);
	unlock();
}

void
nodejobs::wait_for_step1_unblocked()
{
	lock();
	while (fi_blocked) {
		os::warning("nj %d.%d: waiting for step 1 to be unblocked",
		    node.ndid, node.incn);
		fault_cv.wait(&lck);
	}
	os::warning("nj %d.%d: step 1 is unblocked", node.ndid, node.incn);
	unlock();
}

#endif

//
// We have canceled this reconfiguration. Wake up the step1 thread that may be
// blocked.
//
void
nodejobs::cleanup_dead_refcount_cancel(cmm::seqnum_t seqnum)
{
	lock();
	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("nj %d.%d: cleanup_dead_refcount_cancel seqnum = %lld\n",
	    node.ndid, node.incn % 1000, seqnum));
	canceled_seqnum = seqnum;
	cv.signal();
	unlock();
}

//
// Provide state information
//
void
nodejobs::read_state(state_refcnt &state)
{
	state.ref_jobs = (int)refs.get_count();
	state.ack_jobs = (int)translate_acks.get_count();
}

//
// refcount methods
//

// constructor
refcount::refcount()
{
}

// destructor
refcount::~refcount()
{
}

int
refcount::initialize()
{
	ASSERT(the_refcount == NULL);
	the_refcount = new refcount();
	if (the_refcount == NULL)
		return (ENOMEM);
	return (the_refcount->initialize_int());
}

void
refcount::shutdown()
{
	if (the_refcount != NULL) {
		delete the_refcount;
		the_refcount = NULL;
	}
}

//
// initialize_int - this private method initializes the newly created
// refcount object.
//
int
refcount::initialize_int()
{
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		ndtab[i].initialize(i);
	}

	//
	// register ourselves by linking into the list of orb created
	// threads.
	//
	registerthread();

	// Create the background thread which sends queued requests.
#ifdef _KERNEL
	//
	// We use a priority higher than that of invocation server threads,
	// which use the default priority value. This thread drives actions
	// which free up resources, such as translate_ack's and corba objects.
	// Freeing memory is highly desirable.
	//
	if (clnewlwp((void(*)(void *))refcount::autom_thread, NULL, 65,
	    NULL, NULL) != 0) {
#else
	if (os::thread::create(NULL, 0L,
	    (void *(*)(void *))refcount::autom_thread,
	    NULL, (long)(THR_BOUND | THR_DETACHED), NULL)) {
#endif
		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);

		//
		// SCMSGS
		// @explanation
		// The system could not create the needed thread, because
		// there is inadequate memory.
		// @user_action
		// There are two possible solutions. Install more memory.
		// Alternatively, reduce memory usage. Since this happens
		// during system startup, application memory usage is normally
		// not a factor.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: failed to create thread for autom_thread");

		(void) unregisterthread();
		return (ENOMEM);
	}
	return (0);
}

// cleanup_dead_refcount
void
refcount::cleanup_dead_refcount(const cmm::membership_t &membership,
    cmm::seqnum_t seqnum)
{
	nodeid_t i;

	for (i = 1; i <= NODEID_MAX; i++) {
		ndtab[i].cleanup_dead_refcount(membership.members[i], seqnum);
	}
}

// cleanup_dead_refcount_cancel
void
refcount::cleanup_dead_refcount_cancel(cmm::seqnum_t seqnum)
{
	nodeid_t i;

	for (i = 1; i <= NODEID_MAX; i++) {
		ndtab[i].cleanup_dead_refcount_cancel(seqnum);
	}
}

//
// add_reg
//
// sets up register job for the server xdoor
//
// The hxdoor will queue register job when it is reconnecting to
// primary. This job will have lent_node set to the current node. This
// register job is not because a reference was received from some node
// and hence there is no translate_ack waiting for this reference to
// be acknowledged. However, to allow the usual processing the lent_node
// is set to current node id.
//
void
refcount::add_reg(ID_node &server_node, rxdoor_descriptor &xdoor_desc,
    ID_node &lent_node)
{
	//
	// The ack for lent_node can't be sent until this inc is acked.  We need
	// to add that dependency before we add the inc.
	//
	ndtab[lent_node.ndid].add_dependent_reg(lent_node, server_node.ndid);

	if (!ndtab[server_node.ndid].add_reg(server_node, xdoor_desc,
	    lent_node)) {
		// primary node died so remove dependant reg
		ndtab[lent_node.ndid].
		    remove_dependent_reg(lent_node, server_node.ndid);
	}
}

//
// HandleRefCnts - handles arriving reference count messages.
// The message is passed to the appropriate nodejob based on the source node.
//
// static
void
refcount::HandleRefCnts(recstream *re)
{
	refcount	&trfc  = refcount::the();
	MarshalStream	&rms = re->get_MainBuf();
	ID_node		src_node = re->get_src_node();

	refcnt_control	ref_hdr;
	// Get the reference count message header.
	re->get_bytes((void *)&ref_hdr, (uint_t)(sizeof (ref_hdr)));

	// Process a reference count message from a specified node.
	trfc.ndtab[src_node.ndid].process_refmsg(&ref_hdr, rms, src_node.incn);

	re->done();
}

//
// autom_thread - is a thread that periodically flushes reference counting
// jobs in the various node queues.
//
void
refcount::autom_thread(void *)
{
	// Initialized in refcount::initialize
	refcount::the().flush_nodes();
}

void
refcount::flush_nodes()
{
	nodeid_t i;

	lock();

	//
	// The refcount thread loops until told to shutdown.
	//
	do {
		os::systime	time_out;
		time_out.setreltime(autom_thread_sleep_max);

		// The reference counting jobs are processed by server node
		for (i = 1; i <= NODEID_MAX; i++) {
			//
			// The decision as to whether to flush is made on a per
			// node basis according to how many references are
			// pending and how long since the last flush to that
			// specific node.
			//
			ndtab[i].lock();
			if (ndtab[i].flush_check()) {
				// time_out will be set to the earliest time
				// that a node wants to wake up.
				ndtab[i].flush_node(&time_out);
			}
			ndtab[i].unlock();
		}

		//
		// Wake up after a time period or when ordered to shutdown
		//
		(void) threadcv.timedwait(&threadLock, &time_out);

	} while (state == orbthread::ACTIVE);

	// The thread will now go away.
	state = orbthread::DEAD;
	threadGone.signal();
	unlock();
}

//
// read_state - support cladm command.
// Obtain the state for the specified destination node
//
int
refcount::read_state(void *argp)
{
	state_refcnt	state_ref_cnt;

	if (copyin(argp, &state_ref_cnt, sizeof (state_refcnt)) != 0) {
#ifdef DEBUG
		os::warning("error in copyin for state_count");
#endif
		return (EFAULT);
	}
	nodeid_t	nodeid = (nodeid_t)state_ref_cnt.node;
	// Check for valid node id
	if (nodeid == NODEID_UNKNOWN || nodeid > NODEID_MAX) {
		return (EINVAL);
	}
	if (!members::the().alive(nodeid)) {
		// Do not return state for node not in the cluster
		state_ref_cnt.node = NODEID_UNKNOWN;
	} else {
		// Return state for node in the cluster
		ndtab[nodeid].read_state(state_ref_cnt);
	}
	return (copyout(&state_ref_cnt, argp, sizeof (state_refcnt)));
}

#ifdef FAULT_RECONF

void
refcount::wait_for_step1_blocked(nodeid_t nd)
{
	ndtab[nd].wait_for_step1_blocked();
}

void
refcount::wait_for_step1_unblocked(nodeid_t nd)
{
	ndtab[nd].wait_for_step1_unblocked();
}

#endif
