//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)standard_handler.cc	1.113	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <orb/handler/standard_handler.h>
#include <orb/xdoor/Xdoor.h>
#include <orb/refs/unref_threadpool.h>
#ifdef _KERNEL_ORB
#include <orb/xdoor/standard_xdoor.h>
#else
#include <orb/xdoor/solaris_xdoor.h>
#endif
#include <orb/object/schema.h>
#include <orb/fault/fault_injection.h>

//
// Handler Kit stuff
//
standard_handler_kit *standard_handler_kit::the_standard_handler_kit = NULL;

remote_handler *
standard_handler_kit::create(Xdoor *xdp)
{
	return (new standard_handler_client(xdp));
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
standard_handler_kit::initialize()
{
	ASSERT(the_standard_handler_kit == NULL);
	the_standard_handler_kit = new standard_handler_kit();
	if (the_standard_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
standard_handler_kit::shutdown()
{
	if (the_standard_handler_kit != NULL) {
		delete the_standard_handler_kit;
		the_standard_handler_kit = NULL;
	}
}

//
// This method returns reference to standard_handler_kit instance.
//
inline standard_handler_kit&
standard_handler_kit::the()
{
	ASSERT(the_standard_handler_kit != NULL);
	return (*the_standard_handler_kit);
}
//
// standard_handler methods
//

bool
standard_handler::is_user()
{
#ifdef _KERNEL_ORB
	return (false);
#else
	return (true);
#endif
}

hkit_id_t
standard_handler::handler_id()
{
	return (standard_handler_kit::the().get_hid());
}

//
// standard_handler_server methods
//

standard_handler_server::standard_handler_server(CORBA::Object_ptr impl_objp) :
	obj_(impl_objp)
{
	//
	// If we are in userland, the orb server code may not be initialized.
	//
#ifndef	_KERNEL_ORB
	if (!ORB::is_server_initialized()) {
		(void) ORB::server_initialize();
	}
#endif
}

standard_handler_server::~standard_handler_server()
{
	//
	// We must get the lock to ensure that whatever caused
	// the unreferenced to be triggered has finished with it.
	// There is no need to unlock, since we are deleting the lock.
	//
	lock();

	//
	// It is only safe to delete an implementation object if
	// it has no references.
	//
	ASSERT(nref == 0);
	ASSERT(xdoorp == NULL);

	ASSERT(state_.get() == NEW_HANDLER || state_.get() == UNREF_DONE);

	obj_ = nil;	// for lint
}

//
// revoke - future invocations are prevented by revoking any attached xdoor.
//	No new xdoors are allowed.
//	It is possible that no xdoors were ever created.
//
//	The revoke operation may take an arbitrary period of time, because
//	it will not complete until all current remote invocations complete.
//
//	It is permissible to perform a revoke operation after an unreferenced
//	operation.
//
void
standard_handler_server::revoke()
{
	lock();
	ASSERT(revoke_state_.get() != REVOKED_HANDLER);
	revoke_state_.set(REVOKED_HANDLER);
	if (xdoorp != NULL) {
		// Incrementing marshcount prevents xdoor from
		// getting disconnected
		// We cannot hold the lock while calling xdoorp->revoked()
		// as it may block waiting for something that needs to
		// get the handler lock - e.g., it may try to marshal the object
		marshcount++;
		unlock();

		xdoorp->revoked();
		// We do an implicit marshal_cancel in revoked() to account for
		// the extra marshcount increment above.
		// This causes the usual unreferenced protocol to happen
		// to disconnect the xdoor from the handler

		// The unref protocol may have deleted the handler, so do not
		// grab the lock or refer to the handler structure anymore
	} else {
		// Xdoor has already been disconnected.
		// Unref would have been delivered when nref went to zero,
		// or when the xdoor was disconnected.
		// In the case where the implementation did a revoke prior to
		// any get_objref, it will NOT get an unreferenced. Is this OK?
		unlock();
	}
}

//
// begin_xdoor_unref
//
//   obtains handler lock and returns handler's marshal count.
//   Called by the xdoor's deliver_unreferenced() method.
//
uint_t
standard_handler_server::begin_xdoor_unref()
{
	FAULTPT_UNREF(FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF, this,
	    fault_unref::generic_test_op);

	lock();
	return (marshcount);
}

//
// end_xdoor_unref
//
//   called by xdoor to inform the handler if the xdoor has decided to
//   delete itself. The handler may decide to delete implementation object
//   thereby deleting itself.
//
void
standard_handler_server::end_xdoor_unref(bool xdoor_unref)
{
	ASSERT(lock_held());
	if (xdoor_unref) {
		xdoorp = NULL;
		//
		// Set marshcount to 0, in preparation for the next xdoor
		// to get attached to this handler. We set it here instead
		// after creating the new xdoor, because the handler::marshal
		// routine increments/checks marshcount before creating the
		// new xdoor. This is needed to be able to recover from a
		// memory allocation failure or xdoorid shortage.
		//
		marshcount = 0;
		if (nref == 0) {
			switch (state_.get()) {
			case ACTIVE_HANDLER:
				//
				// No references
				// exist. Deliver unreference
				// to implementation object.
				//
				state_.set(UNREF_PROCESS);
				ASSERT(is_not_nil(obj_));

				unlock();

				obj_->_unreferenced();

				//
				// The implementation object
				// does not exist anymore.
				//
				return;

			case UNREF_UNSCHEDULE:
				//
				// Handler can be in UNREF_UNSCHEDULE state in
				// the following scenario:
				// 1. BEGIN: state=NEW_HANDLER, xdoor=0, nref= 0
				// 2. (create local ref) ->
				//    state = ACTIVE_HANDLER, nref = 1
				// 3. (release local ref) ->
				//    state = UNREF_SCHEDULED, nref = 0 and
				//    remains on unref queue before
				//    deliver_unreferenced() is called for it.
				// 4. (create local ref) ->
				//    state = UNREF_UNSCHEDULED, nref = 1
				// 5. (create foreign ref) -> xdoor != 0,
				// 6. (release local ref) -> nref = 0
				// 7. (release foreign ref) -> xdoor = 0,
				//    nref = 0, state = UNREF_UNSCHEDULED
				//
				state_.set(UNREF_SCHEDULED);
				break;

			case UNREF_NO_PROCESS:
				//
				// Handler can be in UNREF_NOPROCESS state in
				// the following scenario:
				// 1. BEGIN: state=NEW_HANDLER, xdoor=0, nref=0
				// 2. (create local ref) ->
				//    state = ACTIVE_HANDLER, nref = 1
				// 3. (release local ref) ->
				//    state = UNREF_SCHEDULED, nref = 0.
				//    deliever_unreferenced() is called which
				//    sets state to UNREF_PROCESS and releases
				//    handler lock.
				// 4. (create local ref) ->
				//    state = UNREF_NO_PROCESS, nref = 1.
				// 5. (create foreign ref) -> xdoor != 0.
				// 6. (release local ref) -> nref = 0.
				// 7. (release foreign ref) -> xdoor = 0,
				//    nref = 0, state = UNREF_NO_PROCESS.
				// This has to happen before _unreferenceed()
				// for the object in step 3.
				//
				state_.set(UNREF_PROCESS);
				break;

			default:
				ASSERT(0);
				break;
			}
			//
			// The unlock() below is for states other than
			// ACTIVE_HANDLER.
			//
			unlock();
			return;
		} else {		// xdoor_unref = 1, nref != 0
			//
			// Arm fault points for some unref_fi tests.
			// It is necessary to set fault points while
			// holding the handler lock because as soon as
			// lock is released, delive_unreferenced()
			// method can be called before the triggers
			// are added resulting in incorrect test
			// behavior.
			//
			FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_6, this,
			    fault_unref::generic_test_op);

			FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_7, this,
			    fault_unref::generic_test_op);

			//
			// NOW that deliver_unreferenced() triggers
			// are set, release the handler lock.
			//
			unlock();

			FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_5, this,
			    fault_unref::generic_test_op);

			FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_6, this,
			    fault_unref::generic_test_op);

			FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_7, this,
			    fault_unref::generic_test_op);

		}	// xdoor_unref = 1, nref != 0

	} else {	// xdoor_unref = false
		//
		// standard_handler_server can be in one of the
		// following states.
		// 1. ACTIVE_HANDLER: this is straight forward.
		//
		// 2. UNREF_NO_PROCESS: Assume the handler is in UNREF_PROCESS
		// state and following sequence of events happens:
		//
		//   i. A local reference is created, state = UNREF_NO_PROCESS,
		//	nref=1
		//  ii. A remote reference is created. xdoorp != NULL
		// iii. Remote ref is released causing the xdoor to be put on
		//	the unref_threadpool and UNREF_PENDING bit set.
		//  iv. A remote reference is created and marshalled, causing,
		//	xdoor's MARSHAL_DURING_UNREF flag to be set.

		//   v. The xdoor then processes
		//	deliver_unreferenced(), finds
		//	MARSHAL_DURING_UNREF set and decides to tell the
		//	handler that it is NOT going away. To do this, it
		//	calls the handler's end_xdoor_unref() method.

		//
		// The handler's state remains UNREF_NO_PROCESS. The local
		// reference is released after the creation of xdoor. Since
		// xdoorp != NULL, it does not change state of the handler
		// in the release() method.
		//
		// 3. UNREF_UNSCHEDULE: Assume that the handler was scheduled
		// to be unreferenced and is on the unref queue.
		//
		//   i. A local reference is created, state - UNREF_UNSCHEDULE,
		//	ref = 1.
		// Further steps are same as 2(ii) through 2(v).
		// The handler has stayed in UNREF_UNSCHEDULE state.
		//
		ASSERT((state_.get() == ACTIVE_HANDLER) ||
		    (state_.get() == UNREF_NO_PROCESS) ||
		    (state_.get() == UNREF_UNSCHEDULE));

		unlock();
	}
}

//
// get_unref_handler - Returns the handler to be used by the unref_task
//
handler *
standard_handler_server::get_unref_handler()
{
	return (this);
}

//
// deliver_unreferenced - process an unref_task for a handler.
//
void
standard_handler_server::deliver_unreferenced()
{
	//
	// Setting up the unref_fi tests for all the unref tests that will be
	// triggered inside deliver_unreferenced().
	// Note:	For more information about unref path, refer to
	// 		src/fault_numbers/faultnum_unref.h
	//
	// Since the fault function will create reference or release reference,
	// which all require grabbing the lock. So the fault functions should be
	// called outside the lock to prevent a deadlock.
	//
	FAULTPT_UNREF(FAULTNUM_UNREF_SETUP_DELIVER_UNREFERENCED, this,
	    fault_unref::generic_test_op);

	FAULTPT_UNREF(FAULTNUM_UNREF_SETUP_WAIT_DEL_UNREF, this,
	    fault_unref::generic_test_op);

	//
	// If FAULTNUM_UNREF_REGOBJ_PATH_1 is triggered, run test 1 with
	// unreference path 1
	//
	FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_1, this,
	    fault_unref::generic_test_op);

	lock();

	ASSERT(is_not_nil(obj_));

	if (state_.get() == UNREF_UNSCHEDULE) {
		//
		// A 0->1 ref count transition occurred.
		// No longer need to unreference the implementation object.
		//
		state_.set(ACTIVE_HANDLER);
		unlock();

		//
		// If FAULTNUM_UNREF_REGOBJ_PATH_2 is triggered, run test 2
		// unreference path 2.
		//
		FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_2, this,
		    fault_unref::generic_test_op);

		return;
	}

	//
	// Now we are about to actually process the unreference.
	//
	ASSERT(state_.get() == UNREF_SCHEDULED);
	state_.set(UNREF_PROCESS);

	//
	// Cannot hold the handler lock while delivering unreference
	// to the implementation object.
	//
	unlock();

	//
	// The cookie is used by objects supporting multiple interfaces.
	// The cookie allows the implementation object to determine
	// which interface received the unreference notification.
	// The overwhelming majority of implementation objects have only
	// one interface, and thus ignore this argument.
	//
	obj_->_unreferenced((unref_t)this);

	// The handler may no longer exist after the above operation completes.
}

//
// last_unref - Returns true when no new object references have
// been created since the time when unreference notification was initiated.
// Can only be called from the implementation object once
// when dealing with an unreference notification.
// This method is not idempotent.
//
bool
standard_handler_server::last_unref()
{
	bool	unref_ok;

	//
	// Setting up the unref_fi tests for all the unref_fi tests that could
	// be triggered inside last_unref(). If there's no trigger is set,
	// the setup will do nothing.
	// Note:	For more information about unref path, refer to
	// 		src/fault_numbers/faultnum_unref.h
	//
	FAULTPT_UNREF(FAULTNUM_UNREF_SETUP_LAST_UNREF, this,
	    fault_unref::generic_test_op);

	//
	// If FAULTNUM_UNREF_REGOBJ_PATH_3 is triggered, run test 3 with
	// unreference path 3
	//
	FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_3, this,
	    fault_unref::generic_test_op);

	lock();
	//
	// Adjust state to show that unreference was delivered to the
	// implementation object.
	//
	switch (state_.get()) {
	case UNREF_PROCESS:
		unref_ok = true;
		state_.set(UNREF_DONE);
		break;

	case UNREF_NO_PROCESS:
		unref_ok = false;
		state_.set(ACTIVE_HANDLER);
		break;

	default:
		ASSERT(0);
		unref_ok = false;
	}
	unlock();

	//
	// If FAULTNUM_UNREF_REGOBJ_PATH_4 is triggered, run test 4 with
	// unreference path 4
	//
	FAULTPT_UNREF(FAULTNUM_UNREF_REGOBJ_PATH_4, this,
	    fault_unref::generic_test_op);

	return (unref_ok);
}

//
// new_reference - create a new reference for the handler.
//
// If the handler had an unreference pending, that unreference request
// is discarded.
//
void
standard_handler_server::new_reference()
{
	lock();
	//
	// The handler must be in a viable state to permit new references.
	//
	if (revoke_state_.get() == REVOKED_HANDLER) {
		//
		// SCMSGS
		// @explanation
		// An attempt was made to obtain a new reference on a revoked
		// handler.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: can't get new reference");
	}

	if (nref == 0 && xdoorp == NULL) {
		//
		// Perform a zero to one transition.
		// The handler now has given out a reference and is not new.
		// Any pending unreference is cancelled.
		//
		switch (state_.get()) {
		case NEW_HANDLER:
		case UNREF_DONE:
			state_.set(ACTIVE_HANDLER);
			break;

		case UNREF_SCHEDULED:
			state_.set(UNREF_UNSCHEDULE);
			break;

		case UNREF_PROCESS:
			state_.set(UNREF_NO_PROCESS);
			break;

		default:
			ASSERT(0);
		}
		// no need to initialize marshal counts
	} else {
		// References already existed.
		//
		ASSERT(state_.get() == ACTIVE_HANDLER ||
		    state_.get() == UNREF_UNSCHEDULE ||
		    state_.get() == UNREF_NO_PROCESS);
	}
	nref++;
	ASSERT(nref != 0);	// Check for wraparound

	unlock();
}

//
// marshal - the operation will be rejected if the handler is revoked.
//
void
standard_handler_server::marshal(service &_in, CORBA::Object_ptr)
{
	lock();
	// We must have a reference if we are marshaling one.
	ASSERT(nref > 0);

	// Increment the marshal count always.
	// marshal_roll_back will recover in case there is an exception later

	// Incrementing marshcount prevents an existing xdoor, or an xdoor
	// (that will get created due to a later marshal) from
	// getting disconnected until this marshal gets rolled back or goes
	// through translate_in.
	// If there is an exception in this marshal, and no later marshals
	// occur. marshal_roll_back will bring marshcount back to 0
	marshcount++;

	//
	// Don't marshal revoked server xdoors.
	//
	if (revoke_state_.get() == REVOKED_HANDLER) {
		unlock();

		// Throw exception
		_in.get_env()->system_exception(
		    CORBA::BAD_PARAM(0, CORBA::COMPLETED_MAYBE));
		return;
	}

	if (xdoorp == NULL) {
		// Allocate an xdoor of type server port
#ifdef _KERNEL_ORB
		xdoorp = standard_xdoor_server::create(this);
#else
		xdoorp = solaris_xdoor_server::create(this, true);
#endif
		// We do blocking allocations and should not get NULL
		ASSERT(xdoorp != NULL);
	}
	unlock();
	remote_handler::marshal(_in, obj_);
}

//
// unmarshal - This method unmarshals the handler.
//
CORBA::Object_ptr
standard_handler_server::unmarshal(service &, CORBA::TypeId,
    generic_proxy::ProxyCreator, CORBA::TypeId to_tid)
{
	lock();
	// We can't receive a reference if we know of none.
	ASSERT(nref != 0 || xdoorp != NULL);

	// Account for the new reference.
	nref++;
	ASSERT(nref != 0);		// catch wraparounds
	unlock();

	//
	// Get the object pointer by casting from the narrowest type.
	// We have to adjust the pointer in case this object uses
	// multiple inheritance.
	//
	return ((CORBA::Object_ptr)direct_narrow(obj_, to_tid));
}

//
// Narrow the object to an object of the given type.
//
void *
standard_handler_server::narrow(CORBA::Object_ptr,
    CORBA::TypeId to_tid, generic_proxy::ProxyCreator)
{
	//
	// Narrow for the server is simple, because the deep type of
	// the object is the one the object already has, and thus is
	// known to the domain.  Only casting has to be performed.
	//
	void	*objectp = direct_narrow(obj_, to_tid);
	if (objectp != NULL) {
		//
		// Yes, can cast.
		// Must have a reference if can narrow that reference.
		//
		ASSERT(nref > 0);
		incref();
	}
	return (objectp);
}

//
// duplicate - someone is using an additional object reference.
//
void *
standard_handler_server::duplicate(CORBA::Object_ptr object_p,
#ifdef DEBUG
    CORBA::TypeId tid)
#else
    CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));

	// We must have a reference if we are duplicating it.
	ASSERT(nref > 0);

	// Increase the reference count.
	incref();

	//
	// Notice: return the object_p parameter instead of the obj_ value,
	// because the object_p may have been narrowed to a different type.
	//
	return (object_p);
}

void
standard_handler_server::release(CORBA::Object_ptr)
{
	lock();

	ASSERT(nref > 0);
	nref--;

	if (nref == 0 && xdoorp == NULL) {
		//
		// No references exist any more. Schedule an unreference.
		//
		switch (state_.get()) {
		case ACTIVE_HANDLER:
			//
			// Schedule unreferenced notification to
			// object impl.
			//
			state_.set(UNREF_SCHEDULED);
			unref_threadpool::the().
			    defer_unref_processing(this);
			break;

		case UNREF_UNSCHEDULE:
			state_.set(UNREF_SCHEDULED);
			break;

		case UNREF_NO_PROCESS:
			state_.set(UNREF_PROCESS);
			break;

		default:
			ASSERT(0);
		}
	}
	unlock();
}

bool
standard_handler_server::is_local()
{
	return (true);
}

bool
standard_handler_server::server_dead()
{
	return (false);
}

CORBA::Object_ptr
standard_handler_server::_obj()
{
	return (obj_);
}

//
// Client side methods.
//

void
standard_handler_client::marshal(service &_in, CORBA::Object_ptr obp)
{
	incmarsh();
	remote_handler::marshal(_in, obp);
}

extern InterfaceDescriptor _Ix__type(CORBA_Object);

//
// unmarshal - called with the xdoor lock held.
// We must synchronize with the standard_handler_client::release function,
// which may have set nref to zero.
//
CORBA::Object_ptr
standard_handler_client::unmarshal(service &, CORBA::TypeId tid,
    generic_proxy::ProxyCreator create_func, CORBA::TypeId to_tid)
{
	CORBA::Object_ptr obj;

	//
	// Find the interface descriptor for this type.
	//
	InterfaceDescriptor *infp =
	    OxSchema::schema()->descriptor(tid, this);
	ASSERT(infp != NULL);

	lock();
	if (!CORBA::is_nil(proxy_obj_)) {
		//
		// Check to see that the proxy supports the correct deep type.
		// Note: the deep type can be CORBA::Object if another ORB
		// calls idlversion_impl::get_dynamic_descriptor() in this
		// ORB.
		// Otherwise, for non-HA references, the deep type should
		// always be the same.
		//
		if (infp == proxy_obj_->_deep_type()) {
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			return (obj);
		}
		// Forget the old proxy if we can cache the new one.
		if (infp != &_Ix__type(CORBA_Object)) {
			proxy_obj_ = CORBA::Object::_nil();
		}
	}

	//
	// Try to use the interface descriptor to create a proxy of the
	// deep type rather than creating a proxy of an ancestor class.
	// Keep a pointer to this proxy for unmarshal() and narrow()'s
	// later on. Note that the proxy_obj_ pointer doesn't have a
	// reference count associated with it; it is released when the
	// handler is released.
	//
	// If the server object is newer than the client code, we won't
	// be able to create a proxy for the deep type so we create
	// a proxy for the deepest (ancestor) type we know about locally.
	//
	// Also, don't cache the proxy if this is an unmarshal
	// for idlversion_impl::get_dynamic_descriptor().
	//
	unlock();
	obj = infp->create_proxy_obj(this, infp);
	if (CORBA::is_nil(obj)) {
		// Server is newer so leave proxy_obj_ nil.
		obj = (CORBA::Object_ptr)(*create_func)(this, infp);
		// Account for the new reference.
		incref();
	} else if (infp == &_Ix__type(CORBA_Object)) {
		// Account for the new reference.
		incref();
	} else {
		lock();
		if (CORBA::is_nil(proxy_obj_)) {
			proxy_obj_ = obj;
			// Account for the new reference.
			nref++;
			ASSERT(nref != 0);	// Check for wraparound
			unlock();
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(obj, to_tid);
		} else {
			//
			// We lost the race so delete ours and use the
			// other one.
			//
			ASSERT(infp == proxy_obj_->_deep_type());
			CORBA::Object_ptr tmp_obj = obj;
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			delete tmp_obj;
		}
	}
	return (obj);
}

//
// Narrow the object to an object of the given type.
// Note that we can only get a new reference from
// get_objref(), unmarshal(), or narrow().
//
void *
standard_handler_client::narrow(CORBA::Object_ptr object_p,
    CORBA::TypeId to_tid, generic_proxy::ProxyCreator create_func)
{
	void	*new_proxyp = direct_narrow(object_p, to_tid);
	if (new_proxyp != NULL) {
		//
		// Successful narrow on the existing proxy.
		// Increase the refcount in the proxy.
		//
		incpxref(object_p);
	} else if (object_p->_deep_type()->is_type_supported(to_tid) ||
	    object_p->_deep_type()->is_unknown()) {
		//
		// Note that the check:
		// object_p->_deep_type()->is_type_supported(to_tid)
		// is to make sure we don't allow invocations on the
		// server object it doesn't support.
		// If the deep type is unknown, we allow narrow
		// but don't allow invocations (see invoke()).
		//
		new_proxyp = (*create_func)(this, object_p->_deep_type());
		if (new_proxyp != NULL) {
			lock();

			// Increase the handler reference count.
			ASSERT(nref > 0);
			nref++;
			ASSERT(nref != 0);

			//
			// If we were unable to create a single proxy for
			// the deep type when the object was unmarshalled,
			// we save the pointer to reduce the number of
			// proxies created.
			//
			if (CORBA::is_nil(proxy_obj_) &&
			    !object_p->_deep_type()->is_unknown()) {
				proxy_obj_ = (CORBA::Object_ptr)new_proxyp;
			}

			unlock();
		}
	}
	return (new_proxyp);
}

//
// duplicate - someone is using an additional object reference.
//
void *
standard_handler_client::duplicate(CORBA::Object_ptr object_p,
#ifdef DEBUG
    CORBA::TypeId tid)
#else
    CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));

	// Cannot duplicate if you do not already have a reference.
	ASSERT(nref > 0);

	// Increase the proxy reference count.
	incpxref(object_p);

	return (object_p);
}

//
// release
//
void
standard_handler_client::release(CORBA::Object_ptr obj)
{
	lock();
	if (decpxref(obj) == 0) {
		if (!CORBA::is_nil(proxy_obj_) && obj->_this_component_ptr() ==
		    proxy_obj_->_this_component_ptr()) {
			proxy_obj_ = CORBA::Object::_nil();
		}
		delete obj;
		ASSERT(nref > 0);
		if (--nref == 0) {
			if (xdoorp->client_unreferenced(Xdoor::KERNEL_HANDLER,
			    marshcount)) {
				delete this;
				return;
			} else {
				// The xdoor has passed up a reference to this
				// handler as part of an unmarshal. We'd better
				// stick around.
				// The xdoor has accounted for the marshal
				// count we sent down.
				marshcount = 0;
			}
		}
	}
	unlock();
}

//
// Generate a new reference to the implementation object.
//
void
standard_handler_client::new_reference()
{
	ASSERT(nref > 0);
	incref();
}

bool
standard_handler_client::is_local()
{
	return (false);
}

bool
standard_handler_client::server_dead()
{
	return (xdoorp->server_dead());
}

CORBA::Object_ptr
standard_handler_client::_obj()
{
	return (CORBA::Object::_nil());
}
