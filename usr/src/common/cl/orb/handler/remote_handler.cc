//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)remote_handler.cc	1.115	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <orb/handler/remote_handler.h>
#include <orb/xdoor/Xdoor.h>
#include <orb/flow/resource.h>
#include <sys/os.h>
#include <sys/mc_probe.h>
#include <sys/thread.h>

#ifdef _KERNEL_ORB
#include <orb/infrastructure/orb_conf.h>

#endif	// _KERNEL_ORB

//
// remote_handler_kit methods
//

remote_handler_kit::~remote_handler_kit()
{
}

//
// unmarshal - Extract a handler from a recstream.
// The recstream MainBuf marshalstream contains a type id (in MD5 format).
// The recstream XdoorTab marshalstream contains an xdoor pointer
// that already has been translated in.
//
// If the xdoor already has a handler for this reference,
// then we use that (it may be a client or server handler).
// Otherwise we allocate a new client handler, because the server would
// already have a handler.
//
// Any remaining unmarshalling is delegated to the derived handler type.
//
// The create_func and typeid arguments are used to narrow the object
// before returning.
//
CORBA::Object_ptr
remote_handler_kit::unmarshal(service &serv,
    generic_proxy::ProxyCreator create_func, CORBA::TypeId narrow_tid)
{
	Xdoor *xdp = serv.get_xdoor();

	// Extract the object's deep type id.
	CORBA::MD5_Tid	tid;
	_ox_get_tid(serv, tid);

	handler *handlerp = xdp->extract_handler(Xdoor::KERNEL_HANDLER);
	if (handlerp == NULL) {
		// The xdoor currently has no handler.
		handlerp = create(xdp);
	}

	ASSERT(handler_kit::is_remote_handler(handlerp->handler_id()));
	remote_handler *remhandlerp = (remote_handler *)handlerp;

	CORBA::Object_ptr object_p =
	    remhandlerp->unmarshal(serv, tid, create_func, narrow_tid);

	xdp->extract_done(Xdoor::KERNEL_HANDLER, remhandlerp, true);

	return (object_p);
}

//
// unmarshal_cancel - the unmarshal operation is being cancelled.
// Not only does the handler need to clean up, but the xdoor level
// must also be requested to clean up.
//
void
remote_handler_kit::unmarshal_cancel(service &serv)
{
	Xdoor		*xdp;
	CORBA::MD5_Tid	tid;

	xdp = serv.get_xdoor();

	// Extract the object's deep type id.
	_ox_get_tid(serv, tid);

	xdp->unmarshal_roll_back();

	//
	// Allow the derived handlers to cancel the unmarshal of any
	// handler specific data, so that we advance through the stream
	// by the right amount.
	//
	unmarshal_cancel_remainder(serv);
}

//
// send_reply - provides the handler level support for reply messages.
// The method marshals the reply information and passes the msg to the
// xdoor level.
//
// In the kernel, this method creates an appropriate sized sendstream for
// the reply message.
//
// In user land, this method does not create a sendstream.
// This difference is due to the requirement that the sendstream must be
// created on the stack for the door interface.
//
void
remote_handler::send_reply(service &serv, arg_info &info, ArgValue *value)
{
	Environment	*e = serv.get_env();
	if (e->exception()) {
		//
		// An exception occurred.
		// The handler does not need to marshal anything,
		// because the xdoor level will generate an error reply
		// in a new sendstream.
		//
		xdoorp->send_reply(serv);
		return;
	}
#ifdef _KERNEL_ORB
	service_twoway *serv2 = NULL;

	if (serv.get_service_type() == service::TWOWAY) {
		serv2 = (service_twoway *) &serv;
	}

	//
	// Identify message resources.
	// The reply will have at least result status (four bytes).
	//
	resources resource(resources::REPLY_FLOW, e, invocation_mode::NONE,
	    0,				// header size
	    (uint_t)sizeof (uint_t),	// data size
	    serv.get_src_node(), serv2, REPLY_MSG);

	// Account for the space required to marshal arguments.
	resource.add_argsizes(info, value);

	//
	// Create a sendstream for the reply message.
	//
	sendstream *sendstreamp = xdoorp->reserve_reply_resources(&resource);
	if (sendstreamp == NULL) {
		//
		// Client Node is dead.
		// There is no place to return reply info,
		// even though the request was performed.
		//
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(e->exception()));
		xdoorp->send_reply(serv);
		return;
	}
	serv.set_sendstream(sendstreamp);
#else
	sendstream *sendstreamp = serv.se;
	ASSERT(sendstreamp != NULL);
#endif	// _KERNEL_ORB

	ASSERT(!e->exception());

	// Assume the best and marshal NO_EXCEPTION.
	sendstreamp->put_unsigned_long(CORBA::NO_EXCEPTION);

	// Marshal the out/inout/return arguments.
	serv.marshal_reply(info, value, e);

	if (e->exception()) {
		ASSERT(e->sys_exception() != NULL);
		e->sys_exception()->completed(CORBA::COMPLETED_YES);

		// Still send a reply when a marshalling error occurs.
	}
	xdoorp->send_reply(serv);
}

//
// handle_incoming_call - extracts from the message header information
//			that specifies the target method and then
//			invokes that target method.
//
void
remote_handler::handle_incoming_call(service &serv)
{
	MC_PROBE_0(rh_incoming_call_start, "clustering orb handler", "");

	// Get the object.
	CORBA::Object_ptr object_p = _obj();

	//
	// Remote Handler Header -
	// After the header for the xdoor layer has been stripped off,
	// the header now consists of:
	//	Index	- to the receiver function table for the invoked class
	//	Method	- identifies the method to be invoked.
	//
	// Note that the invocation method is totally general.
	// The information must identify the method and the class,
	// which is represented by a receiver function.
	// Note that this does not require the component to have a full IDL
	// interface, but something that follows these conventions.
	//
	InterfaceDescriptor *infp = object_p->_interface_descriptor();
	uint_t inf_index;
	InterfaceDescriptor::ReceiverFunc recfunc = get_index_method(serv,
	    infp, inf_index);

	if (recfunc == NULL) {
		ASSERT(0);
		// XXX need to free service object. Generic reply?
		Environment &e = *serv.get_env();
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		return;
	}

	MC_PROBE_0(rh_incoming_call_toHandle, "clustering orb handler", "");

	// unmarshall -> call method -> marshall return value -> send reply
	(*recfunc)(infp->cast(object_p->_this_ptr(), inf_index), serv);

#ifdef _KERNEL_ORB
	ASSERT(serv.se == NULL);
#endif
	MC_PROBE_0(rh_incoming_call_end, "clustering orb handler", "");
}

//
// marshal - load a representation of this handler into the marshal stream.
// The marshal stream contains in order:
//	1. the handler id,
//	2. type of the object, and
//	3. the xdoor pointer.
// Note that the xdoor must not be released until the pointer is extracted
// from the sendstream in Xdoor_manager::translate_out.
// This is usually prevented by keeping marshal counts at both the handler
// and xdoor levels.
//
void
remote_handler::marshal(service &_in, CORBA::Object_ptr objp)
{
	//
	// Blocking can occur during the translate_out process.
	//
	ASSERT(!_in.get_env()->is_nonblocking());

	// Marshal the object type id.
	_ox_put_tid(_in, objp->_deep_type()->get_tid());

	ASSERT(xdoorp != NULL);
	_in.put_xdoor(xdoorp);
}

//
// Base implementation.
//
void
remote_handler::marshal_roll_back(CORBA::Object_ptr)
{
	//
	// We allow handler::marshal to complete even if the xdoorp is NULL
	// or if could not be allocated or was revoked etc.
	// After all the arguments were marshalled, marshal_roll_back will
	// be called before the buffers are discarded.
	//
	// Corresponding to every handler::marshal, we either do
	// translate_in at xdoor layer (normal case)
	// decrement marshcount in marshal_roll_back (if no xdoor exists)
	// call xdoorp->marshal_cancel in marshal_roll_back (if xdoor exists)
	// Once an xdoor is created the handler will not disconnect till all
	// in progress marshals have been accounted for as above.
	//
	// We need to hold the lock while checking for xdoorp == NULL
	// While handler::marshal increments marshcount preventing any
	// existing xdoor from getting disconnected, it can still cause
	// a new xdoor to be created due to a new outgoing marshal.
	//
	lock();
	if (xdoorp == NULL) {
		//
		// We can't use xdoorp->marshal_cancel here as the xdoor could
		// not be created.
		// Decrement marshcount corresponding to increment in
		// handler::marshal
		//
		ASSERT(marshcount > 0);
		marshcount--;
		unlock();
	} else {
		unlock();	// Dont hold lock across call to xdoor
		//
		// handler has been marshaled, but not xdoor, this means that
		// we can safely access the xdoor, since the marshal counts are
		// different, preventing the xdoor from going away.
		//
		// Dont decrement the marshcount, but instead tell the xdoor
		// that a handler marshal has been rolled back and the XdoorTab
		// will be destroyed before the xdoor layer does translate_in
		// This is the normal handler/xdoor reference counting protocol.
		//
		xdoorp->marshal_cancel();
	}
}

//
// invoke - supports requests to remote nodes. Determines whether request is
// one or two way, and calls appropriate method.
//
ExceptionStatus
remote_handler::invoke(arg_info &ai, ArgValue *avp, InterfaceDescriptor *infp,
    Environment &e)
{
	ExceptionStatus	result;

	MC_PROBE_0(rh_invoke_start, "clustering orb handler", "");
	MC_PROBE_0(rh_invoke_probecost, "clustering orb handler", "");

	e.check_used("unchecked environment used for new invocation");
	ASSERT(!e.exception());

#ifdef _KERNEL_ORB
	//
	// This check will catch the reuse of the Environment
	// in a nested invocation when the prior invocation came
	// from a different node. Invoke occurs on the client side.
	// So the source node should be the local node.
	//
	ASSERT(e.get_src_node().ndid == orb_conf::local_nodeid());
#endif	// _KERNEL_ORB

	// Determine whether Oneway/Twoway.
	invocation_mode		invo_mode(ai);

#ifdef INTR_DEBUG
	// Save old state for restoring later.
	bool was_nonblocking =
	    (bool) os::envchk::is_nonblocking(os::envchk::NB_INVO);
	if (invo_mode.is_nonblocking()) {
		os::envchk::set_nonblocking(os::envchk::NB_INVO);
	} else {
		os::envchk::check_nonblocking(os::envchk::NB_INTR |
		    os::envchk::NB_INVO |
		    os::envchk::NB_RECONF, "blocking invocation");
	}
#endif

	if (invo_mode.is_oneway()) {
		result = invoke_oneway(ai, avp, invo_mode, infp, e);
	} else {
		result = invoke_twoway(ai, avp, invo_mode, infp, e);
	}

	if (result != CORBA::RETRY_EXCEPTION) {
		e.mark_used();
	}
#ifdef INTR_DEBUG
	if (!was_nonblocking)
		os::envchk::clear_nonblocking(os::envchk::NB_INVO);
#endif
	MC_PROBE_1(rh_invoke_end, "clustering orb handler", "",
	    tnf_long, type, result);
	return (result);
}

//
// invoke_oneway - perform a oneway invocation.
//
ExceptionStatus
remote_handler::invoke_oneway(arg_info &ai, ArgValue *avp,
    const invocation_mode &invo_mode, InterfaceDescriptor *infp, Environment &e)
{
	// Check that the server version of the object supports this method.
	int inx = infp->lookup_type_index(ai.id, false);
	if (inx < 0) {
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		return (CORBA::LOCAL_EXCEPTION);
	}

	// Specify resources required for message.
	resources	resource(resources::BASIC_FLOW_CONTROL, &e, invo_mode,
			    INVO_ONEWAY_MSG);

	//
	// Remote handler header is marshalled as data.
	// The remote handler payload contains method (octet) and
	// receiver function (octet), which rounds up to one word.
	//
	resource.add_send_data((uint_t)sizeof (uint32_t));

	// Account for the space required to marshal arguments.
	resource.add_argsizes(ai, avp);

#ifdef _KERNEL
	//
	// If the thread blocks or is preempted, we want the thread to
	// effectively have a high priority (60) so that the thread
	// can complete processing in a reasonable time period.
	//
	THREAD_KPRI_REQUEST();
#endif

	//
	// Walk the message protocol layers to reserve resources
	// and obtain a send stream for the invocation.
	//
	invocation	invo(xdoorp->reserve_resources(&resource), &e);

	if (e.exception()) {
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		ASSERT(e.sys_exception()->completed() == CORBA::COMPLETED_NO);

		//
		// The request did not reach the server.
		// The resources object destructor frees allocated resources
		// for oneway msgs when an exception exists.
		//
		return (CORBA::LOCAL_EXCEPTION);
	}

	// Load the interface type index and method number.
	put_index_method(invo, infp, inx, ai.method);

	// Marshall info related to method to be invoked on other side.
	invo.marshal_call(ai, avp);

	if (e.exception()) {
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		// exception during marshalling of arguments
		e.sys_exception()->completed(CORBA::COMPLETED_NO);
		invocation::exception_return(ai, avp);

		//
		// The request did not reach the server.
		// The resources object destructor frees allocated resources
		// for oneway msgs when an exception exists.
		//
		return (CORBA::LOCAL_EXCEPTION);
	}

	ExceptionStatus result = xdoorp->request_oneway(invo);

#ifdef _KERNEL
	THREAD_KPRI_RELEASE();
#endif

	//
	// Any LOCAL_EXCEPTION would have been set in the Environment already
	//
	// The resources object destructor frees allocated resources
	// for oneway msgs when an exception exists.
	// Thus do not need to check for exception here.
	//
	// We could have a RETRY_NEEDED exception with LOCAL_EXCEPTION status
	// and we need to return result here.
	//
	if (CORBA::RETRY_NEEDED::_exnarrow(e.exception())) {
		result = CORBA::RETRY_EXCEPTION;
		e.clear();
	}
	return (result);
}

//
// invoke_twoway - perform an rpc invocation.
//
ExceptionStatus
remote_handler::invoke_twoway(arg_info &ai, ArgValue *avp,
    const invocation_mode &invo_mode, InterfaceDescriptor *infp, Environment &e)
{
	MC_PROBE_0(rh_invoke_twoway_start, "clustering orb handler", "");
	//
	// All rpc requests are blocking
	// Environment may have been reused
	//
	e.clear_nonblocking();

	// Check that the server version of the object supports this method.
	int inx = infp->lookup_type_index(ai.id, false);
	if (inx < 0) {
		e.system_exception(CORBA::VERSION(0, CORBA::COMPLETED_NO));
		invocation::exception_return(ai, avp);
		return (CORBA::LOCAL_EXCEPTION);
	}

	// Specify resources required for message
	resources	resource(resources::BASIC_FLOW_CONTROL, &e, invo_mode,
			    INVO_MSG);

	//
	// Remote handler header is marshalled as data.
	// The remote handler payload contains method (octet) and
	// receiver function (octet), which rounds up to uint16_t.
	// Marshal debug is of size uint32_t and each octet has a
	// marshal debug.
	//
#ifdef MARSHAL_DEBUG
	resource.add_send_data(2 * (uint_t)sizeof (uint32_t) +
		(uint_t)sizeof (uint16_t));
#else
	resource.add_send_data((uint_t)sizeof (uint16_t));
#endif
	// Account for the space required to marshal arguments
	resource.add_argsizes(ai, avp);

#ifdef _KERNEL
	//
	// If the thread blocks or is preempted, we want the thread to
	// effectively have a high priority (60) so that the thread
	// can complete processing in a reasonable time period.
	//
	THREAD_KPRI_REQUEST();
#endif
	//
	// Walk the message protocol layers to reserve resources
	// and obtain a send stream for the invocation.
	//
	invocation	invo(xdoorp->reserve_resources(&resource), &e);

	if (e.exception()) {
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		ASSERT(e.sys_exception()->completed() == CORBA::COMPLETED_NO);
		invocation::exception_return(ai, avp);
		return (CORBA::LOCAL_EXCEPTION);
	}

	// Load the interface type index and method number.
	put_index_method(invo, infp, inx, ai.method);

	// Marshall info related to method to be invoked on other side.
	invo.marshal_call(ai, avp);

	if (e.exception()) {
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif
		// Exception during marshalling of arguments.
		e.sys_exception()->completed(CORBA::COMPLETED_NO);
		invocation::exception_return(ai, avp);
		return (CORBA::LOCAL_EXCEPTION);
	}

	ExceptionStatus result = xdoorp->request_twoway(invo);

#ifdef _KERNEL
	THREAD_KPRI_RELEASE();
#endif
	//
	// If there is a local/retry exception, there won't be a recstream.
	// Otherwise, the result status is in the marshalstream.
	//
	if (result != CORBA::LOCAL_EXCEPTION)
		result = (ExceptionStatus) invo.get_unsigned_long();

	switch (result) {
	case CORBA::NO_EXCEPTION:
		invo.unmarshal_normal_return(ai, avp, &e);
		break;
	case CORBA::USER_EXCEPTION:
		invo.unmarshal_exception_return(ai, avp, &e);
		break;
	case CORBA::SYSTEM_EXCEPTION:
		invo.unmarshal_sys_exception_return(ai, avp, &e);
		// fall-through to LOCAL_EXCEPTION
	case CORBA::LOCAL_EXCEPTION:
		// There must be a system exception in case of LOCAL EXCEPTION
		ASSERT(e.sys_exception());
		if (CORBA::RETRY_NEEDED::_exnarrow(e.exception())) {
			e.clear();
			return (CORBA::RETRY_EXCEPTION);
		}
		invocation::exception_return(ai, avp);
		break;
	case CORBA::UNINITIALIZED:
	case CORBA::RETRY_EXCEPTION:
	case CORBA::REMOTE_EXCEPTION:
	default:
		//
		// SCMSGS
		// @explanation
		// An invocation completed with an invalid result status.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: invalid invocation result status %d", result);
		break;
	}
	MC_PROBE_0(rh_invoke_twoway_end, "clustering orb handler", "");
	return (result);
} //lint !e1746

//
// Narrow, whenever possible, using the same proxy/C++ object pointer.
//
void *
remote_handler::direct_narrow(CORBA::Object_ptr objp, CORBA::TypeId tid)
{
	ASSERT(objp != NULL);

	InterfaceDescriptor &idesc = *objp->_interface_descriptor();
	int inx = idesc.lookup_type_index(tid, true);
	return (inx < 0 ? NULL :
	    idesc.cast(objp->_this_ptr(), (uint_t)inx));
}

//
// Output the interface index number and method number to the invocation stream.
//
void
remote_handler::put_index_method(invocation &invo, InterfaceDescriptor *infp,
    int inx, uint_t method)
{
	ASSERT((uint16_t)inx <= USHRT_MAX);
	invo.put_unsigned_short((uint16_t)inx);

	// Load the method number.
	if (infp->sizeof_methods((uint_t)inx) == 1) {
		ASSERT(method <= UCHAR_MAX);
		invo.put_octet((uint8_t)method);
	} else {
		ASSERT(method <= USHRT_MAX);
		invo.put_unsigned_short((uint16_t)method);
	}
}

//
// Extract the interface index and method numbers.
//
InterfaceDescriptor::ReceiverFunc
remote_handler::get_index_method(service &serv, InterfaceDescriptor *infp,
    uint_t &inx)
{
	uint_t method;

	// Extract the interface type index.
	inx = serv.get_unsigned_short();

	// Extract the method number.
	if (infp->sizeof_methods(inx) == 1)
		method = serv.get_octet();
	else
		method = serv.get_unsigned_short();

	return (infp->lookup_rec_func(inx, method));
}

//
// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.
//
#ifdef NOINLINES
#define	inline
#include <orb/handler/remote_handler_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
