/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)gateway.cc	1.128	08/07/28 SMI"

#include <orb/xdoor/standard_xdoor.h>
#include <orb/xdoor/simple_xdoor.h>
#include <orb/handler/gateway.h>
#include <orb/object/adapter.h>
#include <orb/flow/resource.h>
#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <sys/sunddi.h>
#include <sys/kmem.h>
#include <orb/buffers/marshalstream.h>
#include <orb/debug/orb_trace.h>
#include <sys/thread.h>
#include <sys/cl_comm_support.h>

#ifndef _KERNEL
#include <door.h>
#include <signal.h>
#define	door_ki_hold(dh)
#define	door_ki_rele(dh)
#define	door_ki_upcall(dh, da) door_call(dh, da)
#define	door_ki_info(dh, di) door_info(dh, di)
#endif

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#endif

#include <orb/xdoor/door_header.h>

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <sys/zone.h>
#include <sys/vc_int.h>
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

#if defined(UNODE)
#include <unode/systm.h>
#else
#include <sys/systm.h>
#endif

static gateway_manager the_gateway_manager;

//
// Returns a reference to the local name server.
//
extern "C" door_handle_t
gateway_get_local_ns()
{
	naming::naming_context_var local_ns =
	    local_nameserver_root_funcp((nodeid_t)NULL);
	ASSERT(!CORBA::is_nil(local_ns));

	// The narrow we do here assumes name server uses remote_handler.
	ASSERT(handler_kit::is_remote_handler(
	    local_ns->_handler()->handler_id()));
	//lint -e1774
	remote_handler *h = (remote_handler *)(local_ns->_handler());
	//lint +e1774
	return (the_gateway_manager.translate_up(h->get_xdoorp(), false));
}

//
// Returns the type ID of the local name server object.
//
extern "C" void
get_local_ns_typeid(uint32_t *typeidp)
{
	naming::naming_context_var local_ns =
	    local_nameserver_root_funcp((nodeid_t)NULL);
	ASSERT(!CORBA::is_nil(local_ns));
	bcopy(local_ns->_deep_type()->get_tid(), typeidp,
	    sizeof (CORBA::MD5_Tid));
}

//
// count_Buf methods
//

count_Buf::count_Buf(uchar_t *ptr, uint_t len, refcnt *cnt, alloc_type b,
    alloc_type d) :
	Buf(ptr, len, b, d),
	count(cnt)
{
	count->hold();
}

Buf *
count_Buf::dup(uint_t start, uint_t len, os::mem_alloc_type flag)
{
	ASSERT(start >= first());
	ASSERT((start + len) <= length());
	// CSTYLED
	return ((Buf *) new (flag) count_Buf(base_address() + start, len,
	    count));
}

count_Buf::~count_Buf()
{
	ASSERT(data_alloc == OTHER);
	count->rele();
	count = NULL;
}

//
// gateway_manager methods
//

// add a mapping of handler to door in the gateway table
void
gateway_manager::register_mapping(door_id_t did, gateway_handler *h)
{
	ASSERT(lock_held());

	ASSERT(gwm_table.lookup(did) == NULL);	// No duplicates
	gwm_table.add(h, did);
}

// remove a mapping
void
gateway_manager::release_mapping(door_id_t did)
{
	ASSERT(lock_held());
	ASSERT(gwm_table.lookup(did) != NULL);
	(void) gwm_table.remove(did);
}

//
// Lookup a handler in the mapping table given a door id.  We don't
// need to do the reverse lookup since the door id is stored in the
// handler.
//
gateway_handler *
gateway_manager::lookup_mapping(door_id_t did)
{
	ASSERT(lock_held());
	return (gwm_table.lookup(did));
}

//
// Translates an object reference from user to kernel (door to xdoor).
//
Xdoor *
gateway_manager::translate_down(door_handle_t dh, door_id_t did, bool unref)
{
	Xdoor *xdp;
	gateway_handler	*h;

	lock();
	h = lookup_mapping(did);
	if (h == NULL) {
		// didn't find it, create new handler and xdoor
		h = new gateway_handler(dh, did, 0, true);
		register_mapping(did, h);
		// keep a hold on the door
		door_ki_hold(dh);
		if (unref) {
			// standard xdoor
			xdp = standard_xdoor_server::create(h);
		} else {
			// simple xdoor
			xdp = simple_xdoor_server::create(h);
		}
		// We do blocking allocations and should not get NULL
		ASSERT(xdp != NULL);
		h->set_xdp(xdp);
	} else {
		xdp = h->get_xdp();
	}
	h->inc_kernel_marsh();
	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: translate_down: %p %p %p %u\n", this, xdp,
	    dh, h->get_kernel_marsh()));
	unlock();
	ASSERT(xdp != NULL);
	return (xdp);
}

//
// Translates a reference from kernel to user (xdoor to door).
// unmarshal is true if this xdoor has been unmarshalled at the
// xdoor level.  translate_up is also called from gateway_get_local_ns
// to get a reference that hasn't been unmarshalled (with unmarshalled
// set to false).
//
door_handle_t
gateway_manager::translate_up(Xdoor *xdp, bool unmarshalled)
{
	gateway_handler *h;
	door_handle_t dh;

	h = (gateway_handler *)xdp->extract_handler(Xdoor::USER_HANDLER);
	if (h == NULL) {
		// didn't find it. Create a new handler.
		h = new gateway_handler(NULL, (door_id_t)0, xdp, false);
	}

	lock();
	dh = h->unmarshal_up(xdp);
	unlock();
	ASSERT(dh != NULL);
	// Set handler and adjust unmarshal counts if appropriate
	xdp->extract_done(Xdoor::USER_HANDLER, h, unmarshalled);

	return (dh);
}

//
// gateway_handler methods
//

//
// The gateway handler doesn't have a handler kit for passing this
// handler type as a reference to another node.
// Handler_id() is used so idlversion_impl::get_dynamic_descriptor()
// can tell that a handler is a gateway handler.
//
hkit_id_t
gateway_handler::handler_id()
{
	return (GATEWAY_HKID);
}

void
gateway_handler::marshal(service &, CORBA::Object_ptr)
{
}

void
gateway_handler::marshal_roll_back(CORBA::Object_ptr)
{
}

void *
gateway_handler::narrow(CORBA::Object_ptr, CORBA::TypeId,
    generic_proxy::ProxyCreator)
{
	return (NULL);
}

void *
gateway_handler::duplicate(CORBA::Object_ptr, CORBA::TypeId)
{
	return (NULL);
}

void
gateway_handler::release(CORBA::Object_ptr)
{
}

bool
gateway_handler::is_local()
{
	return (gwh_local);
}

bool
gateway_handler::is_user()
{
	return (true);
}

// Translate a stream of door references from user to kernel.
void
gateway_handler::translate_down_stream(sendstream *se, door_desc_t *dp,
    uint_t ndoors)
{
	uint_t i;

	for (i = 0; i < ndoors; i++) {
		door_handle_t dh;
		door_info_t di;
		Xdoor	*xdp;

		// get door handle
#ifdef _KERNEL
		ASSERT(dp[i].d_attributes & DOOR_HANDLE);
		dh = dp[i].d_data.d_handle;
#else
		ASSERT(dp[i].d_attributes & DOOR_DESCRIPTOR);
		dh = dp[i].d_data.d_desc.d_descriptor;
#endif

		if (door_ki_info(dh, &di) == 0) {
			ORB_DBPRINTF(ORB_TRACE_GATEWAY,
			    ("gate: translate_down_stream: %p %p %p\n",
			    this, get_xdp(), dh));
			xdp = the_gateway_manager.translate_down(dh,
			    di.di_uniquifier,
			    ((dp[i].d_attributes & DOOR_UNREF_MULTI) != 0));
			if (xdp == NULL) {
				// This can only happen if the handler was
				// revoked. We need to handle the error
				// by raising an exception and recovering
				// the counts
				ASSERT(0);
				continue;
			}
			se->put_xdoor(xdp);
		} else {
#ifdef DEBUG
			os::warning("gateway_handler::translate_down_stream: "
			    "passed non-door file 0x%p as argument", dh);
#endif
		}
	}
}

// Release door references passed from user level.
void
gateway_handler::translate_down_stream_cancel(door_desc_t *dp, uint_t ndoors)
{
	uint_t i;

	for (i = 0; i < ndoors; i++) {
		// release handles
#ifdef _KERNEL
		ASSERT(dp[i].d_attributes & DOOR_HANDLE);
		door_ki_rele(dp[i].d_data.d_handle);
#else
		ASSERT(dp[i].d_attributes & DOOR_DESCRIPTOR);
#endif
	}
}

//
// translate_up_stream - Translate xdoors in a recstream
// into an array of door references for door communications from kernel to user.
//
door_desc_t *
gateway_handler::translate_up_stream(recstream *re, uint_t ndoors)
{
	door_desc_t *dp;

	dp = new door_desc_t[ndoors];

	for (uint_t i = 0; i < ndoors; i++) {
		door_handle_t	dh;
		Xdoor		*xdp;

		xdp = re->get_xdoor();
		dh = the_gateway_manager.translate_up(xdp, true);

#ifdef _KERNEL
		dp[i].d_attributes = DOOR_HANDLE | DOOR_RELEASE;
		dp[i].d_data.d_handle = dh;
#else
		//
		// XXX we don't handle automatic release properly for UNODE
		//
		dp[i].d_attributes = DOOR_DESCRIPTOR;
		dp[i].d_data.d_desc.d_descriptor = dh;
#endif
	}
	return (dp);
}

void
gateway_handler::prepare_send(service &s, door_arg_t *da)
{
	size_t		dsize = da->data_size;
	size_t		ndid = da->desc_num;
	sendstream	*se = s.se;
	int		error = 0;

	if (dsize) {
		ASSERT(dsize <= UINT_MAX);	// for cast below
		error = se->put_u_bytes(da->data_ptr, (uint_t)dsize, false);
		if (error != 0) {
			s.get_env()->system_exception(
			    CORBA::BAD_PARAM((uint_t)error,
			    CORBA::COMPLETED_NO));
		}
	}

	if (ndid) {
		if (error == 0) {
			ASSERT(ndid <= UINT_MAX);	// for cast below
			translate_down_stream(se, da->desc_ptr, (uint_t)ndid);
		}
	}
}

Buf *
gateway_handler::exception_prepare_return(CORBA::SystemException *ex,
    service &serv, door_arg_t *da)
{
	Buf		*b;
#if defined(_FAULT_INJECTION)
	Environment	*e = serv.get_env();

	// Reply msg must use blocking memory allocation
	ASSERT(!e->is_nonblocking());

	// Allocate space for the Invo triggers and exception.
	MarshalStream	wms(
		InvoTriggers::header_size() +
		InvoTriggers::triggers_size(InvoTriggers::the()) +
		6 * (uint_t)sizeof (uint32_t), e);

	uint_t hdr_size = wms.alloc_chunk(InvoTriggers::header_size());

	// Buffer allocation should not fail when blocking is enabled.
	ASSERT(hdr_size == InvoTriggers::header_size());
#else
	MarshalStream	wms(serv.get_env());
#endif
	ex->_put(wms);

#if defined(_FAULT_INJECTION)
	// Marshal Invo triggers for K->U reply.
	InvoTriggers::put(wms);
#endif

	// gather all the data from the list of Bufs & copy them to one buffer
	b = wms.coalesce_region().reap();

	da->data_ptr = (char *)b->head();	// Args are here
	da->data_size = b->span();
	da->rbuf = (char *)b->head();
	da->rsize = b->span() + b->avail();

	da->desc_ptr = NULL;
	da->desc_num = 0;

	return (b);
}

Buf *
gateway_handler::prepare_return(service &s, door_arg_t *da)
{
	recstream	*re = s.re;
	uint_t		count;
	Buf		*b;
	MarshalStream	&rms = re->get_MainBuf();

	// must set environment since we will be doing allocations
	rms.set_env(s.get_env());

#if defined(_FAULT_INJECTION)
	//
	// Allocate and prepend space for header because either the recstream
	// is a nil_recstream (reply from local kernel server) or the rxdoor
	// layer has trimmed out all headers (reply from remote server).
	//
	InvoTriggers::prepend_header_buf(rms);

	// Marshal Invo triggers for K->U reply.
	InvoTriggers::put(rms);
#endif

	//
	// gather all the data from the list of Bufs & copy them to one buffer
	//
	b = rms.coalesce_region().reap();

	da->data_ptr = (char *)b->head();	// Args are here
	da->data_size = b->span();
	da->rbuf = (char *)b->head();
	da->rsize = b->span() + b->avail();

	// Create descriptor arguments
	if ((count = re->get_xdoorcount()) != 0) {
		da->desc_ptr = translate_up_stream(re, count);
		da->desc_num = count;
#ifndef _KERNEL
		//
		// Verify that file descriptors are valid doors
		//
		for (uint_t i = 0; i < count; i++) {
			door_info_t dinfo;
			int fp = da->desc_ptr[i].d_data.d_desc.d_descriptor;
			if (door_info(fp, &dinfo) < 0) {
				perror("door_info");
				ASSERT(0);
			}
		}
#endif
	} else {
		da->desc_ptr = NULL;
		da->desc_num = 0;
	}
	return (b);
}

//
// prepare_receive - transforms a recstream message formatted invocation
// into the format suitable for door based communications.
//
Buf *
#ifdef _LP64
gateway_handler::prepare_receive(service &serv, door_arg_t *da, ulong_t &retbuf)
#else
gateway_handler::prepare_receive(service &serv, door_arg_t *da)
#endif
{
	recstream	*re = serv.re;
	uint_t		count;
	Buf		*bufp;
	MarshalStream	&rms = re->get_MainBuf();

	// Must set environment since we will be doing allocations
	rms.set_env(serv.get_env());

	Environment *envp = serv.get_env();
	ASSERT(envp);

	//
	// Marshal door_msg_hdr (contains zone_id & zone_uniqid).
	// Invocations that originate from remote nodes or the local
	// kernel are marked as originating from the global zone.
	//
	door_msg_hdr_t::prepend_header_buf(rms);
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	door_msg_hdr_t door_header(envp->get_zone_id(), INVO_MSG,
	    envp->get_cluster_id());
#else
	door_msg_hdr_t door_header(INVO_MSG);
#endif
	rms.write_header((void *)&door_header, sizeof (door_msg_hdr_t));

#if defined(_FAULT_INJECTION)
	//
	// Allocate and prepend space for header because either the recstream
	// is a nil_recstream (invo from local kernel server) or the rxdoor
	// layer has trimmed out all headers (invo from remote server).
	//
	InvoTriggers::prepend_header_buf(rms);

	// Marshal Invo triggers for K->U invocation path.
	InvoTriggers::put(rms);
#endif
	//
	// Gather all the data from the list of Bufs & copy them to one buffer.
	// Removes the Buffer from recstream and returns it to caller.
	// The Buffer may be reused in prepare_reply.
	//
	bufp = rms.coalesce_region().reap();

	if (bufp != NULL) {
		//
		// Specify incoming data location and length
		//
		da->data_ptr = (char *)bufp->head();
		da->data_size = bufp->span();

		if (re->get_invo_mode().is_oneway()) {
			da->rbuf = NULL;
			da->rsize = 0;
		} else {
			// Reuse the argument data buffer for the reply
			//
			// Message headers are 32 bit aligned.
			// So we are guaranteed a minimum 32 bit alignment.
			//
			da->rbuf = (char *)bufp->head();
			da->rsize = bufp->span() + bufp->avail();
#ifdef _LP64
			//
			// The reply buffer on a 64 bit system must
			// begin on a 64 bit boundary.
			//
			if (da->rsize < 2 * sizeof (ulong_t)) {
				//
				// Existing buffer is too small
				// to always guarantee proper 64 bit
				// alignment for the reply buffer.
				// Must have a buffer for return data.
				// An error results when no buffer exists.
				// System will allocate another buffer
				// when specified buffer is too small.
				//
				da->rbuf = (char *)(&retbuf);
				da->rsize = sizeof (ulong_t);
			} else {
				//
				// The above test guarantees that enough
				// bytes exist for the alignment algorithm
				// to be successful.
				//
				// Align the reply buffer start address
				da->rbuf = (char *)(((uintptr_t)(da->rbuf) +
				    (uintptr_t)_LONG_ALIGNMENT - 1l) &
				    ~((uintptr_t)_LONG_ALIGNMENT - 1l));

				// Adjust size downwards because of alignment
				da->rsize &= ~((uintptr_t)_LONG_ALIGNMENT - 1l);
			}
#endif	// _LP64
		}
		// The reply buffer must be properly byte aligned
		ASSERT(((uintptr_t)(da->rbuf) &
		    ((uintptr_t)_LONG_ALIGNMENT - 1l)) == 0);
	} else {
		da->data_ptr = NULL;
		da->data_size = 0;
		da->rbuf = NULL;
		da->rsize = 0;
	}
	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: prepare_receive: door_arg %p %d %p %d %p %d\n",
	    da->data_ptr, da->data_size,
	    da->desc_ptr, da->desc_num,
	    da->rbuf, da->rsize));

	//
	// Create door descriptor arguments.
	// Door descriptors represent object references.
	//
	if ((count = re->get_xdoorcount()) != 0) {
		da->desc_ptr = translate_up_stream(re, count);
		da->desc_num = count;
	} else {
		da->desc_ptr = NULL;
		da->desc_num = 0;
	}
	return (bufp);
}

//
// prepare_reply - translates an invocation reply from door communication format
// to the sendstream format.
//
void
gateway_handler::prepare_reply(service &serv, door_arg_t *da, refcnt *cnt)
{
	sendstream	*se = serv.se;

	//
	// Create a count_Buf to be used by the sendstream. The transport
	// will free the Buf when it's done. When the count_Buf is deleted,
	// it will do a cnt->rele.
	// Note that this will result in tcp_transport doing a copy of the
	// data during Buf::get_mblk. We should revisit this to avoid this
	// copy.
	//
	ASSERT(da->rsize <= INT_MAX);	// for casts below
	count_Buf	*bufp =
	    new count_Buf((uchar_t *)da->data_ptr, (uint_t)da->rsize, cnt);
	bufp->incspan((int)da->rsize);

#if defined(_FAULT_INJECTION)
	//
	// Intercept the Invo triggers from the U->K reply before
	// putting the reply data into the sendstream.
	//
	MarshalStream	tmp_ms(bufp, serv.get_env());

	InvoTriggers::get(tmp_ms);
	se->get_MainBuf().useregion(tmp_ms.region());
#else
	se->get_MainBuf().usebuf(bufp);
#endif

	if (da->desc_num) {
		translate_down_stream(se, da->desc_ptr, da->desc_num);
	}
}

void
gateway_handler::release_doors(door_desc_t *doors, uint_t ndid)
{
	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: release_doors: %p %p %d\n", this, doors, ndid));
	translate_down_stream_cancel(doors, ndid);
}

//
// handle_incoming_call - is the kernel handler level interface supporting
// incoming requests to user-level servers. The interface supports
// both oneway and twoway requests.
//
void
gateway_handler::handle_incoming_call(service &serv)
{
	int		result;
	door_arg_t	da;
	Buf		*rec_bufp;		// Received info buffer
	char		*reply_bufp;
	door_handle_t	dh = get_door();
	door_desc_t	*descp;
	bool		oneway;

	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: handle_incoming: receive %p %p\n", this, get_xdp()));

	//
	// prepare_receive returns the buffer that contains the incoming data.
	// This same buffer may be used to contain the reply data.
	// However, the start address for the reply data may differ
	// as that address must satisfy alignment requirements.
	// There are conditions under which the door system will use
	// a different reply buffer.
	//
#ifdef _LP64
	ulong_t		aligned_retbuf;
	rec_bufp = prepare_receive(serv, &da, aligned_retbuf);
#else
	rec_bufp = prepare_receive(serv, &da);
#endif
	reply_bufp = da.rbuf;

#ifdef _LP64
	int	reply_advance;
	if (da.rsize != 0) {
		reply_advance = (int)(rec_bufp->span() + rec_bufp->avail() -
		    (uint_t)da.rsize);
		ASSERT(reply_advance >= 0);
		ASSERT(reply_advance < _LONG_ALIGNMENT);
	} else {
		reply_advance = 0;
	}
#endif
	descp = da.desc_ptr;

	oneway = serv.re->get_invo_mode().is_oneway();
	serv.set_recstream(NULL);

	// Mask signals before doing the door_ki_upcall.
#ifdef _KERNEL
	k_sigset_t new_mask, old_mask;
	sigfillset(&new_mask);
	sigreplace(&new_mask, &old_mask);
#else
	sigset_t new_mask;
	(void) sigfillset(&new_mask);
#endif

	//
	// Pass the request to the user-level server.
	// After the call da will contain the reply data.
	//
	result = door_ki_upcall(dh, &da);

#ifdef _KERNEL
	// Restore the thread signal property.
	sigreplace(&old_mask, NULL);
#endif

	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: handle_incoming: reply %p %p = %d\n",
	    this, get_xdp(), result));

	// free door descriptors used for upcall
	delete [] descp;

	if (oneway) {
		if (rec_bufp != NULL) {
			// free receive buffer
			rec_bufp->done();
		}
		ASSERT(serv.se == NULL);
		return;			// nothing else to do
	}

	Environment	*e = serv.get_env();

	if (result != 0) {
		//
		// Error occurred on invocation.
		// No reply data on error return,
		// but still need to free receive buffer.
		//
		if (rec_bufp != NULL) {
			rec_bufp->done();
		}
		// Any needed rollbacks are handled automatically by the
		// release of the file pointers passed to door_upcall

		if (result == EAGAIN) {
			// Need to retry.
			os::usecsleep((os::usec_t)1000000);
			e->system_exception(
			    CORBA::RETRY_NEEDED((uint_t)EAGAIN,
			    CORBA::COMPLETED_NO));
		} else {
			CORBA::CompletionStatus completed;
			if (result == EINVAL || result == EBADF) {
				//
				// upcall didn't complete; no server process
				// or no available server threads.
				//
				completed = CORBA::COMPLETED_NO;
			} else {
				completed = CORBA::COMPLETED_YES;
			}
			e->system_exception(
			    CORBA::INV_OBJREF((uint_t)result, completed));
		}
		//
		// The xdoor level will recognize the error and send
		// an exception reply. The rxdoor exception_reply code
		// will retry if necessary.
		//
		get_xdp()->send_reply(serv);
	} else {
		// Succesful invocation occurred.
		//
		// Make sure rec_buf contains the reply data. The data in
		// rec_buf may be reused in multiple retries.
		//
		// Note that after this step the rec_buf contains reply data.
		// The Buf that contains incoming data is either reused
		// or freed so we no longer need to worry about it.
		//
		ASSERT(da.rsize <= INT_MAX);	// for casts below
		if (reply_bufp == NULL ||
		    (reply_bufp != da.data_ptr)) {
			//
			// Overflow occurred on return.
			// The current receive buffer will not be used so
			// dispose of it.
			//
			if (rec_bufp != NULL) {
				rec_bufp->done();
			}
			// Create a new Buf to contain the reply data.
			rec_bufp = new CBuf((uchar_t *)da.data_ptr,
			    da.rsize);
			rec_bufp->incspan((int)da.rsize);
		} else {
			// The received buffer was used for the reply.
#ifdef _LP64
			//
			// Adjust the start address for the alignment shift.
			//
			rec_bufp->advance(reply_advance);
#endif
			//
			// Reset size of the buffer to match the return size.
			// Doors should allocate a new buffer when overflow
			// occurs. An ASSERT in incspan makes sure there is no
			// overflow.
			//
			rec_bufp->incspan((int)((uint_t)da.rsize -
			    rec_bufp->span()));
		}
		//
		// Everytime before passing the buffer to the transport,
		// we do a hold by creating a count_Buf (in prepare_reply).
		// The transport does a release on the buffer by deleting
		// count_Buf. Eventually when everyone is done with
		// the data, the refcount goes to zero,
		// and rec_bufp->done will be called from refcnt_unref.
		// Here we hold the initial refcnt.
		//
		rec_bufp->hold();

		uint_t		ndid = da.desc_num;
		door_desc_t	*doors = new door_desc_t[ndid];

		// Save copy of door descriptors for later release.
		ASSERT(doors != NULL);	// for lint override below
		bcopy(da.desc_ptr, doors,
		    ndid * sizeof (door_desc_t)); //lint !e668

		bool retry_needed;
		do {
			service_twoway *serv2 = NULL;

			if (serv.get_service_type() == service::TWOWAY) {
				serv2 = (service_twoway *)&serv;
			}

			retry_needed = false;

			// Specify resources required for reply message
			resources	resource(resources::REPLY_FLOW, e,
			    invocation_mode::NONE,
			    0,			// header size
			    0,			// data size
			    serv.get_src_node(), serv2, REPLY_MSG);

			//
			// Data from the user level server will come in its
			// own buffer, so space is needed principally for
			// any message headers.
			//
			// Walk the message protocol layers to reserve
			// resources and obtain a send stream for the reply.
			//
			sendstream	*sendstreamp = get_xdp()->
			    reserve_reply_resources(&resource);
			if (sendstreamp == NULL) {
				//
				// Reply messages are not blocked in flow
				// control and reply messages use blocking
				// memory allocations.
				//
				ASSERT(CORBA::COMM_FAILURE::_exnarrow(
				    e->exception()));
				//
				// The exception will be cleared by the error
				// processing code for send_reply.
				//
			} else {
				serv.set_sendstream(sendstreamp);
				//
				// Make sendstream use the buffer that contains
				// the reply. It will create a count_Buf,
				// which does a rdata->hold
				//
				prepare_reply(serv, &da, rec_bufp);
			}
			//
			// Always call send_reply for reply processing.
			// When an exception is present, it will not use the
			// sendstream and it will do needed error cleanup.
			// The transport will free the count_Buf inside
			// sendstream, which does a rdata->rele
			//
			get_xdp()->send_reply(serv);

			if (e->exception()) {
				if (CORBA::RETRY_NEEDED::_exnarrow(
				    e->exception())) {
					retry_needed = true;
				}
				e->clear();
			}

		} while (retry_needed);

		// Release our hold on rdata.
		rec_bufp->rele();

		// Release passed references after send completes.
		release_doors(doors, ndid);
		delete [] doors;
	}
	ASSERT(serv.se == NULL);
}

//
// begin_xdoor_unref
//
//   obtains handler lock and returns handler's marshal count.
//   Called by the xdoor's deliver_unreferenced() method.
//
uint_t
gateway_handler::begin_xdoor_unref()
{
	the_gateway_manager.lock();

	ORB_DBPRINTF(ORB_TRACE_GATEWAY, ("gate: beg_xdoor_unref: %p %p %u\n",
	    this, get_xdp(), get_kernel_marsh()));

	lock();
	return (get_kernel_marsh());
}

//
// end_xdoor_unref
//
//   called by xdoor to inform the handler if the xdoor has decided to
//   delete itself. The handler may decide to delete implementation object
//   thereby deleting itself.
//
void
gateway_handler::end_xdoor_unref(bool xdoor_unref)
{
	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: %p end_xdoor_unref xdoor_unref = %d\n", this,
	    xdoor_unref));
	ASSERT(lock_held());
	ASSERT(the_gateway_manager.lock_held());
	if (xdoor_unref) {
		door_handle_t dh = get_door();
		door_id_t did = get_did();

		the_gateway_manager.release_mapping(did);

		unlock();
		the_gateway_manager.unlock();

		//
		// Generate an unreferenced upcall if no other process
		// on this node is using the server.
		//
		door_ki_rele(dh);

		delete this;
		//
		// Suppress lint warning about not referencing dh below.
		//
	} else {		//lint !e529
		unlock();
		the_gateway_manager.unlock();
	}
}

//
// Called by the proxy when it receives an unref from a process.
// See if it should be deleted.  Deal with races where translate_up is
// being performed on the same object by calling door_is_unref() after
// grabbing the lock.
//
void
gateway_handler::try_release()
{
	door_handle_t dh;
	door_info_t di;
	int err;

	the_gateway_manager.lock();
	lock();
	dh = get_door();
	err = door_ki_info(dh, &di);

	ASSERT(err == 0);
	ORB_DBPRINTF(ORB_TRACE_GATEWAY,
	    ("gate: try_release: %p %p dh %p attr %d\n",
	    this, get_xdp(), dh, di.di_attributes));
	if (di.di_attributes & DOOR_IS_UNREF) {
		Xdoor *xdp = get_xdp();
		bool do_delete;
		unsigned int kmarsh = get_kernel_marsh();

		//
		// The process received all references we sent up.
		// Throw away the vnode and the mapping.
		// Clearing the door handle lets any new translate_up
		// know that it needs to create a new door.
		//
		set_door(NULL);

		do_delete = xdp->client_unreferenced(Xdoor::USER_HANDLER,
		    kmarsh);
		if (!do_delete) {
			// The xdoor has passed up a reference to this handler.
			clear_kernel_marsh();
		}
		ORB_DBPRINTF(ORB_TRACE_GATEWAY,
		    ("gate: try_release: %p %p nulled fp %p deleted %d "
		    "kmarsh %d\n", this, xdp, dh, do_delete, kmarsh));

		if (dh) {
			the_gateway_manager.release_mapping(get_did());
			unlock();
			door_ki_rele(dh);
		} else {
			unlock();
		}

		if (do_delete)
			delete this;
	} else {
		// There are outstanding marshals.  Wait for the next
		// unreferenced call.
		unlock();
	}
	the_gateway_manager.unlock();
}

//
// Called by translate_up when the handler is being passed upwards.
// We deal with the race where this handler is trying to delete itself,
// but a new reference comes up.
//
#if __cplusplus >= 199711L
extern "C" {
#endif
#ifdef _KERNEL
typedef void (*pc_cookiep)(void *, door_arg_t *, void (**)(void *,
    void *), void **, int *);
#else
typedef void (*pc_cookiep)(void *, char *, size_t, door_desc_t *, uint_t);
#endif
#if __cplusplus >= 199711L
}
#endif

door_handle_t
gateway_handler::unmarshal_up(Xdoor *xdp)
{
	door_handle_t dh;
	int err = 0;

	lock();
	if ((dh = get_door()) == NULL) {
		// didn't find it, create a proxy door server
		uint_t flags;
		door_info_t di;
		door_id_t did;

		if (xdp->reference_counting()) {
			flags = DOOR_UNREF_MULTI;
		} else {
			flags = 0;
		}
		//
		// Some comments here about door_ki_create() since
		// there is no man page for this interface. Function
		// call to create a "kernel" door server.  A kernel
		// door server provides a way for a user-level process
		// to invoke a function in the kernel through a
		// door_call.  From the caller's point of view, a
		// kernel door server looks the same as a user-level
		// one (except the server pid is 0).  Unlike normal
		// door calls, the kernel door function is invoked via
		// a normal function call in the same thread and
		// context as the caller.  The arguments to the
		// function are:
		// - pointer to server function
		// - door-specific data called 'cookie'
		// - attributes that define door behavior
		// - pointer to door_handle

		// We use kernel doors for server objects in kernel to
		// receive invocations from clients in user land on
		// the same node. Arguments we pass to door_ki_create
		// are: xdoor_proxy() as the server function (see
		// comments for fn.), cookie or door specific data is
		// the address of this gateway_handler object and flags
		// indicate whether we want to receive multiple unref
		// notifications.
		//

#ifdef _KERNEL
		err = door_ki_create((pc_cookiep)gateway_handler::xdoor_proxy,
		    (void *)this, flags, &dh);
#else
		dh = door_create((pc_cookiep)gateway_handler::xdoor_proxy,
		    (void *)this, flags);
		if (dh < 0)
			err = errno;
#endif
		ASSERT(err == 0);
		set_door(dh);
		(void) door_ki_info(dh, &di);
		did = di.di_uniquifier;
		set_did(did);
		the_gateway_manager.register_mapping(did, this);
	}
	//
	// Bump reference count on the door.  Must be done under the
	// handler lock to protect against try_release().
	//
	door_ki_hold(dh);

	unlock();
	return (dh);
}

void
gateway_handler::inc_kernel_marsh()
{
	lock();
	++gwh_kernel_mc;
	unlock();
}

//
// Destructor function called after a successful return from xdoor_proxy.
// This will be called after all reply data has been copied to user level.
//
void
gateway_handler::xdoor_proxy_free(void *, void *arg)
{
	/*CSTYLED*/
	delete ((gateway_proxy_data *)arg);
}

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
//
// The zone threadpool thread executes this method after picking up
// the deferred task.
//
// The fault injection data that has been passed in the 'datap'
// structure is stored as thread specific data of the current zone
// threadpool thread. After the invocation is done, this thread specific
// data (that might have been updated as a result of the invocation) is
// stuffed back into the 'datap' structure; so that the original thread
// can use the data.
//
void
gateway_task::execute()
{
	gateway_task_lock.lock();

#ifdef _FAULT_INJECTION
	//
	// Stuff fault injection information into thread-specific data.
	//
	FaultFunctions::tsd_svc_id(datap->svc_id);
	InvoTriggers::set_invo_trig_tsd(datap->trigp);
#endif

	datap->result = (datap->xdp)->request_twoway(*(datap->invop));

#ifdef _FAULT_INJECTION
	//
	// Retrieve fault injection information from thread-specific
	// data and save it. Clear the thread-specific data for the
	// current zone threadpool thread.
	//
	datap->trigp = InvoTriggers::get_invo_trig_tsd();

	FaultFunctions::tsd_svc_id(0);
	InvoTriggers::set_invo_trig_tsd(NULL);
#endif

	gateway_task_cv.signal();
	gateway_task_lock.unlock();
}
#endif	// SOL_VERSION >= __s10 && defined(_KERNEL)

//
// xdoor_proxy
//
void
#ifdef _KERNEL
gateway_handler::xdoor_proxy(void *data, door_arg_t *da,
    void (**destfnp)(void *, void *),
    void **destarg, int *error)
#else
gateway_handler::xdoor_proxy(void *data, char *argp, size_t arg_size,
    door_desc_t *dp, uint_t n_desc)
#endif
{
	gateway_handler	*h = (gateway_handler *)data;
	Xdoor		*xdp = h->get_xdp();

#ifndef _KERNEL
	if (arg_size == 0 && n_desc == 0)
		return;
#endif

	ASSERT(h != NULL);

#ifdef _KERNEL
	if (da->data_ptr == DOOR_UNREF_DATA) {
		// client side unreferenced from user level
		ORB_DBPRINTF(ORB_TRACE_GATEWAY,
		    ("gate: xdoor_proxy: unref %p %p\n", h, xdp));
		h->try_release();
		return;
	}

	*error = 0;		// Always - we return errors as exceptions

	//
	// Keep the data in door_arg_t. It's not safe to use da because it
	// will be overwritten if the same thread make another door_call.
	//
	door_arg_t inda = *da;
#else
	if (argp == DOOR_UNREF_DATA) {
		// client side unreferenced from user level
		ORB_DBPRINTF(ORB_TRACE_GATEWAY,
		    ("gate: xdoor_proxy: unref %p %p\n", h, xdp));
		h->try_release();
		return;
	}
	door_arg_t inda;

	inda.data_ptr = argp;
	inda.data_size = arg_size;
	inda.desc_ptr = dp;
	inda.desc_num = n_desc;
	inda.rbuf = argp;
	inda.rsize = arg_size;
#endif

#if defined(_FAULT_INJECTION)
	//
	// Unmarshal Invo triggers from U-K invocation.
	// Note this will adjust da->data_ptr and da->data_size.
	//
	InvoTriggers::get(&inda);
#endif

	Buf		*bufp = NULL;
	Environment	e;

	//
	// Unmarshal the door_msg_hdr
	// and put the zone info into the Environment
	//
	door_msg_hdr_t door_header;
	(void) xcopyin((caddr_t)(inda.data_ptr),
	    (caddr_t)&door_header, sizeof (door_msg_hdr_t));
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zoneid_t zid = door_header.get_zone_id();
	zoneid_t curr_zid;
	uint_t zc_id;
	uint_t curr_zcid = 0;
	bool bad_invo = false;

	if ((curr_zid = getzoneid()) != zid) {
		//
		// SCMSGS
		// @explanation
		// Zone information that has been passed does
		// not match the client credentials.
		// @user_action
		// Someone has tampered with the Sun cluster code. This
		// is a security violation. Shut down the identified
		// zone.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "Executing in zones context with id %d"
		    " that does not match id %d in the door header\n",
		    curr_zid, zid);
		bad_invo = true;
	} else {
		//
		// We are in the same zone context which was
		// passed in the door header, so the zoneid in the
		// door header has not been tampered with.
		//
		int ret;
		e.set_zone_id(zid);
		zone_t *zone_ref = zone_find_by_id(zid);
		ASSERT(zone_ref != NULL);
		e.set_zone_uniqid(zone_ref->zone_uniqid);
		zc_id = door_header.get_cluster_id();
		//
		// We shoudn't allow the invocation to proceed if
		// the zone cluster id of the zone from where the
		// invocation originated is not the same as zone
		// cluster id in the invocation header.
		// The cluster id of a native non-global zone is
		// BASE_NONGLOBAL_ID (2). Since we do not use the clconf
		// interfaces here and directly lookup the cluster-id hash
		// table, we can get a value of 0, which means that there
		// are no zone clusters that match the zone name.
		// Need to check for this condition here.
		//
		ret = zc_getidbyname_funcp(zone_ref->zone_name, &curr_zcid);
		if (((curr_zid == 0) && (zc_id != BASE_CLUSTER_ID)) ||
		    ((ret == -1) && (curr_zid != 0) &&
		    (zc_id != BASE_NONGLOBAL_ID)) ||
		    ((ret != -1) && (curr_zcid != zc_id))) {
			//
			// SCMSGS
			// @explanation
			// Zone cluster id information that has been passed does
			// not match the client credentials.
			// @user_action
			// Someone has tampered with the Sun cluster code.
			// This is a security violation. Shut down the
			// identified zone.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "Invalid zone cluster id %d passed for zone %s\n",
			    zc_id, zone_ref->zone_name);
			bad_invo = true;
		} else {
			e.set_cluster_id(zc_id);
		}
		zone_rele(zone_ref);
	}
#endif
	inda.data_ptr += sizeof (door_msg_hdr_t);
	inda.data_size -= sizeof (door_msg_hdr_t);

	bool retry_needed;

	do {
		retry_needed = false;

		// Specify resources required for message
		resources	resource(resources::BASIC_FLOW_CONTROL, &e,
		    invocation_mode::NONE, INVO_MSG);

		// The gateway handler transfers an existing message
		ASSERT(inda.data_size <= UINT_MAX);
		resource.add_send_data((uint_t)inda.data_size);

		//
		// If the thread blocks or is preempted, we want the thread to
		// effectively have a high priority (60) so that the thread
		// can complete processing in a reasonable time period.
		//
#ifdef _KERNEL
		THREAD_KPRI_REQUEST();
#endif
		//
		// Walk the message protocol layers to reserve resources
		// and obtain a send stream for the invocation.
		//
		invocation invo(xdp->reserve_resources(&resource), &e);
		if (!e.exception()) {
			h->prepare_send(invo, &inda);

			if (e.exception()) {
				xdp->release_resources(&resource);
			}
		}
#if (SOL_VERSION >= __s10) && !defined(UNODE)
		if (bad_invo) {
			//
			// We will raise an exception here as
			// reserve_resource() does not expect an exception
			// to be set.
			//
			if (e.exception()) {
				// clear the exception
				e.clear();
			} else {
				xdp->release_resources(&resource);
			}
			e.system_exception(CORBA::BAD_PARAM(EPERM,
			    CORBA::COMPLETED_NO));
		}
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

		//
		// At this point, all the data have been unmarshalled and the
		// thread is ready to do the invocation.
		// Here, we do the following :
		//
		// (1) If we are running in non-global zone, we stuff the data
		// required for the invocation into a gateway_data structure,
		// and handoff the task to a thread on the zone threadpool.
		// We wait here till the zone threadpool thread completes
		// the task.
		//
		// We retrieve the fault injection data stored in the thread
		// specific data of the current thread, and stuff it into the
		// gateway_data. After the worker thread finishes the
		// invocation, we retrieve the possibly changed fault injection
		// data from the gateway_data and stuff it back into the
		// thread specific data of the current thread.
		// This is done so that the fault injection data is passed
		// to the threadpool thread, and any changes in the fault
		// injection data as a result of the invocation is put back
		// into the current thread successfully.
		//
		// (2) If we are in global zone, or if the Operating System
		// is older than Solaris 10, we do the invocation as part of
		// the current thread.
		//
		if (!e.exception()) {
			ExceptionStatus result;

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
			if (getzoneid() != GLOBAL_ZONEID) {
				struct gateway_data *gate_datap =
				    new gateway_data;
				gate_datap->xdp = xdp;
				gate_datap->invop = &invo;
#ifdef _FAULT_INJECTION
				gate_datap->svc_id =
				    FaultFunctions::tsd_svc_id();
				gate_datap->trigp =
				    InvoTriggers::get_invo_trig_tsd();
#endif

				gateway_task *taskp =
				    new gateway_task(gate_datap);
				taskp->gateway_task_lock.lock();
				zone_threadpool::the().defer_processing(taskp);
				taskp->gateway_task_cv.wait(
				    &(taskp->gateway_task_lock));
				taskp->gateway_task_lock.unlock();
				delete taskp;
				result = gate_datap->result;
#ifdef _FAULT_INJECTION
				InvoTriggers::set_invo_trig_tsd(
				    gate_datap->trigp);
#endif
				delete gate_datap;
			} else {
				result = xdp->request_twoway(invo);
			}

#else	// (SOL_VERSION >= __s10) && defined(_KERNEL)

			result = xdp->request_twoway(invo);

#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

			//
			// Any remote exception is already marshalled.
			// Check to ensure that any local exception is visible.
			//
			ASSERT(result == CORBA::NO_EXCEPTION ||
			    result == CORBA::REMOTE_EXCEPTION ||
			    e.exception());

			if (CORBA::RETRY_NEEDED::_exnarrow(e.exception())) {
				retry_needed = true;
				e.clear();
			}
		}
#ifdef _KERNEL
		THREAD_KPRI_RELEASE();
#endif

		//
		// Cleanup before going out of the loop as invo variable will
		// go out of scope.
		//
		if (!retry_needed) {
			// Release door descriptor and free memory.
			if (inda.desc_num != 0) {
				h->release_doors(inda.desc_ptr, inda.desc_num);

				// free descriptor data
#ifdef _KERNEL
				kmem_free(inda.desc_ptr, inda.desc_num *
				    sizeof (door_desc_t));
#endif

				inda.desc_ptr = NULL;
			}

			if (e.exception()) {
				ASSERT(e.exception());
				//
				// should marshal exception information into
				// reply buffer
				//
				bufp = h->exception_prepare_return(
				    e.sys_exception(), invo, &inda);
			} else {
				// convert recstream into door args
				bufp = h->prepare_return(invo, &inda);
			}
		}
	} while (retry_needed);

#ifdef _KERNEL
	// Restore *da to the correct value.
	*da = inda;

	gateway_proxy_data	*pdd;

	pdd = new gateway_proxy_data(bufp, da->desc_ptr);
	*destfnp = gateway_handler::xdoor_proxy_free;
	*destarg = (void *)pdd;
#else
	(void) door_return(inda.data_ptr, inda.data_size,
	    inda.desc_ptr, inda.desc_num);
	perror("door_return");
	ASSERT(0);
#endif

	//
	// Suppress lint warning about not referencing bufp below
}	//lint !e550
