/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _COUNTER_HANDLER_H
#define	_COUNTER_HANDLER_H

#pragma ident	"@(#)counter_handler.h	1.44	08/05/20 SMI"

#include <orb/handler/handler.h>
#include <sys/os.h>

// delegate_counter_handler
//
// We define two classes: the delegate_counter_handler class
// and the counter class. Both are similar, in that they only
// keep a reference count for serverless (clientless?) objects.
// However, the delegate_counter_handler class assumes
// the counter is kept by the object itself, thus we have just
// one instance of the handler per domain.
// Notice too that we only have one mutex lock per domain for
// all objects relying on this handler. This can be alleviated by
// means of a hashing function, with a collection of mutexes, hashed
// by the address of the object. Notice that the equiv function would
// have to be rewritten for objects using this handler, as all of them
// use just one instance of it.
//
class delegate_counter_handler : public handler {
public:
	// InterfaceDescriptor lookups are unsupported for this handler class

	delegate_counter_handler();
	virtual	~delegate_counter_handler();

	void	lock();
	void	unlock();

	void	marshal(service &, CORBA::Object_ptr);
	void	marshal_roll_back(CORBA::Object_ptr);
	bool	is_local();
	bool	is_user();

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void	release(CORBA::Object_ptr);

private:
	static os::mutex_t  lck;

	// Disallow assignments and pass by value
	int delegate_counter_hander(const delegate_counter_handler &);
	delegate_counter_handler & operator = (delegate_counter_handler &);
};

//
//
// This class does not need client/server stuff, nor
// a kit for that matter. Its only mission is to provide
// reference counting. It may be necessary to have a hash table
// of locks, instead of the global lock we have right now.
//
class counter_handler : public delegate_counter_handler {
public:
	// InterfaceDescriptor lookups are unsupported for this handler class

	counter_handler();

	virtual		~counter_handler();

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void		release(CORBA::Object_ptr);

private:
	uint_t		refcount;

	// Disallow assignements and pass by value
	counter_handler(const counter_handler &);
	counter_handler & operator = (counter_handler &);
};

#include <orb/handler/counter_handler_in.h>

#endif	/* _COUNTER_HANDLER_H */
