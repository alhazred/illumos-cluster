/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _COMBO_HANDLER_H
#define	_COMBO_HANDLER_H

#pragma ident	"@(#)combo_handler.h	1.60	08/05/20 SMI"

#include <orb/handler/remote_handler.h>
#include <orb/xdoor/Xdoor.h>
#ifdef _KERNEL_ORB
#include <orb/xdoor/rxdoor.h>
#endif

class combo_handler_kit : public remote_handler_kit {
public:
	combo_handler_kit();
	remote_handler	*create(Xdoor *);
	void	unmarshal_cancel_remainder(service &);

	static int initialize();
	static void shutdown();
	static combo_handler_kit& the();


private:
	// Disallow assignments or pass by value.
	combo_handler_kit(const combo_handler_kit &);
	combo_handler_kit &operator = (combo_handler_kit &);

	static combo_handler_kit *the_combo_handler_kit;
};

//
// combo_handler is a mixture of simple remote handler and simple xdoor
// (with no reference counting).
// It has a facility to set its xdoor number.
//
class combo_handler :
	public remote_handler,
	public knewdel
{
public:
	hkit_id_t handler_id();

	// InterfaceDescriptor lookups are supported for this handler class.
	combo_handler(Xdoor *xdp);

	combo_handler();

	void		marshal(service &, CORBA::Object_ptr);
	bool		is_user();

private:
	// Disallow assignments or pass by value.
	combo_handler(const combo_handler &);
	combo_handler &operator = (combo_handler &);
};

class combo_handler_server : public combo_handler {
public:
	// InterfaceDescriptor lookups are supported for this handler class.
	combo_handler_server(CORBA::Object_ptr);
#ifdef	_KERNEL_ORB
	combo_handler_server(CORBA::Object_ptr, xdoor_id);
#endif

	// The server handler is deleted when the object is deleted.
	~combo_handler_server();

	CORBA::Object_ptr unmarshal(service &, CORBA::TypeId,
	    generic_proxy::ProxyCreator, CORBA::TypeId);

	void	revoke();

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	bool	is_local();

	// These methods do nothing for combo servers.
	void	release(CORBA::Object_ptr);

protected:
	CORBA::Object_ptr _obj();

private:
	// Record revoke state.
	revoke_state_t		revoke_state_;

	// Pointer to server object.
	CORBA::Object_ptr	obj_;

	// Disallow assignments or pass by value.
	combo_handler_server(const combo_handler_server &);
	combo_handler_server &operator = (const combo_handler_server &);
};

//
// combo_handler_client
//
class combo_handler_client : public combo_handler {
public:
	// InterfaceDescriptor lookups are supported for this handler class.
	combo_handler_client(Xdoor *xdp);

#ifdef _KERNEL_ORB
	combo_handler_client(nodeid_t, xdoor_id);
#else // _KERNEL_ORB
	combo_handler_client(int);
#endif // _KERNEL_ORB

	CORBA::Object_ptr unmarshal(service &, CORBA::TypeId,
	    generic_proxy::ProxyCreator, CORBA::TypeId);

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void	release(CORBA::Object_ptr);

	// Generate a new reference to the implementation object.
	void	new_reference();

	bool	is_local();

protected:
	CORBA::Object_ptr _obj();

	void	incref();

	void	incpxref(CORBA::Object_ptr o);

	uint_t	decpxref(CORBA::Object_ptr o);

private:
	uint_t	nref;

	// Pointer to proxy object for the deep type.
	CORBA::Object_ptr	proxy_obj_;

	// Disallow assignments and pass by value.
	combo_handler_client(const combo_handler_client &);
	combo_handler_client &operator = (combo_handler_client &);
};

#ifndef NOINLINES
#include <orb/handler/combo_handler_in.h>
#endif  // _NOINLINES

#endif	/* _COMBO_HANDLER_H */
