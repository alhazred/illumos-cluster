/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NIL_HANDLER_H
#define	_NIL_HANDLER_H

#pragma ident	"@(#)nil_handler.h	1.32	08/05/20 SMI"

#include <orb/handler/handler.h>

//
// Used to handle null reference passing across domains.
//

class nil_handler_kit : public handler_kit {
public:
	nil_handler_kit();
	~nil_handler_kit();

	CORBA::Object_ptr unmarshal(service &, generic_proxy::ProxyCreator,
	    CORBA::TypeId);

	void unmarshal_cancel(service &);
	static nil_handler_kit &the();
	static int initialize();
	static void shutdown();


private:
	static nil_handler_kit *the_nil_handler_kit;
	// Disallow assignments and pass by value.
	nil_handler_kit(const nil_handler_kit &);
	nil_handler_kit &operator = (nil_handler_kit &);
};

class nil_handler : public handler {
public:
	hkit_id_t	handler_id();

	nil_handler();
	~nil_handler();

	static nil_handler the_nil_handler;

	void	marshal(service &, CORBA::Object_ptr);
	void	marshal_roll_back(CORBA::Object_ptr);
	bool	is_local() { return (false); }
	bool	is_user() { return (false); }
	void	*duplicate(CORBA::Object_ptr, CORBA::TypeId) { return (NULL); }
	void	release(CORBA::Object_ptr) {}
	void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
	    generic_proxy::ProxyCreator) { return (NULL); }

private:
	// Disallow assignments and pass by value.
	nil_handler(const nil_handler &);
	nil_handler &operator = (nil_handler &);
};

#endif	/* _NIL_HANDLER_H */
