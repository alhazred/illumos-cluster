/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  handler_in.h
 *
 */

#ifndef _HANDLER_IN_H
#define	_HANDLER_IN_H

#pragma ident	"@(#)handler_in.h	1.15	08/05/20 SMI"

//
// handler_kit methods
//

inline hkit_id_t
handler_kit::get_hid() const
{
	return (kit_id);
}

inline bool
handler_kit::is_remote_handler(hkit_id_t hid)
{
	return ((hid == STANDARD_HKID) ||
		(hid == COMBO_HKID) ||
		(hid == CKPT_HKID) ||
		(hid == REPL_HKID));
}

//
// handler methods
//

inline
handler::handler() :
	handler_cookie(NULL)
{
}

//
// revoke_state_t methods
//

inline
handler::revoke_state_t::revoke_state_t() :
	_state(NOT_REVOKED)
{
}

//
// get - Returns current revoke state
//
inline handler::revoke_states_t
handler::revoke_state_t::get() const
{
	return ((handler::revoke_states_t)_state);
}

//
// set - Sets revoke state
//
inline void
handler::revoke_state_t::set(handler::revoke_states_t state)
{
	_state = state;
}

//
// handler_state_t methods
//

inline
handler::handler_state_t::handler_state_t() :
	_state(NEW_HANDLER)
{
}

inline
handler::handler_state_t::handler_state_t(handler::handler_states_t state) :
	_state(state)
{
}

//
// get - Returns current handler state
//
inline handler::handler_states_t
handler::handler_state_t::get() const
{
	return ((handler::handler_states_t)_state);
}

//
// set - Sets handler state
//
inline void
handler::handler_state_t::set(handler::handler_states_t state)
{
	_state = state;
}

//
// handler_repository methods
//

inline
handler_repository::handler_repository()
{
	for (int i = 0; i < NUM_HANDLER_KITS; i++) {
		handler_kits[i] = NULL;
	}
}

inline
handler_repository::~handler_repository()
{
}

inline handler_kit *
handler_repository::get_kit(hkit_id_t kid)
{
	ASSERT((int8_t)kid >= 0 && kid < NUM_HANDLER_KITS);
	return (handler_kits[kid]);
}

#endif	/* _HANDLER_IN_H */
