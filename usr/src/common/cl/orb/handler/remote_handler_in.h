/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  remote_handler_in.h
 *
 */

#ifndef _REMOTE_HANDLER_IN_H
#define	_REMOTE_HANDLER_IN_H

#pragma ident	"@(#)remote_handler_in.h	1.9	08/05/20 SMI"

inline
remote_handler_kit::remote_handler_kit(hkit_id_t kid) :
	handler_kit(kid)
{
}

inline
remote_handler::remote_handler(Xdoor *xdp) :
	marshcount(0),
	xdoorp(xdp)
{
}

//
// When we are created with an implementation object.
// (i.e., server side handler)
//
inline
remote_handler::remote_handler() :
	marshcount(0),
	xdoorp(NULL)
{
}

inline Xdoor *
remote_handler::get_xdoorp()
{
	return (xdoorp);
}

inline void
remote_handler::lock()
{
	lck.lock();
}

inline void
remote_handler::unlock()
{
	lck.unlock();
}

inline bool
remote_handler::lock_held()
{
	return (lck.lock_held());
}

inline bool
remote_handler::try_lock()
{
	return ((bool) lck.try_lock());
}

inline void
remote_handler::incmarsh()
{
	lock();
	marshcount++;
	unlock();
}

//
// Return true if handler supports type to_tid.
//
inline bool
remote_handler::local_narrow(CORBA::Object_ptr objp, CORBA::TypeId to_tid)
{
	return (objp->_interface_descriptor()->is_type_supported(to_tid));
}

#endif	/* _REMOTE_HANDLER_IN_H */
