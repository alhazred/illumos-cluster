//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)nil_handler.cc	1.7	08/05/20 SMI"

#include <sys/os.h>
#include <orb/handler/nil_handler.h>

// Nil handler kit
nil_handler_kit *nil_handler_kit::the_nil_handler_kit = NULL;

//
// Nil handler
//
nil_handler	nil_handler::the_nil_handler;

nil_handler_kit::nil_handler_kit() :
    handler_kit(NIL_HKID)
{
}

nil_handler_kit::~nil_handler_kit()
{
}

// Always return nil
CORBA::Object_ptr
nil_handler_kit::unmarshal(service &, generic_proxy::ProxyCreator,
    CORBA::TypeId)
{
	return (CORBA::Object::_nil());
}

void
nil_handler_kit::unmarshal_cancel(service &)
{
}

//
// This method returns reference to nil_handler_kit instance.
//
nil_handler_kit&
nil_handler_kit::the()
{
	ASSERT(the_nil_handler_kit != NULL);
	return (*the_nil_handler_kit);
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
nil_handler_kit::initialize()
{
	ASSERT(the_nil_handler_kit == NULL);
	the_nil_handler_kit = new nil_handler_kit();
	if (the_nil_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
nil_handler_kit::shutdown()
{
	if (the_nil_handler_kit != NULL) {
		delete the_nil_handler_kit;
		the_nil_handler_kit = NULL;
	}
}

hkit_id_t
nil_handler::handler_id()
{
	return (nil_handler_kit::the().get_hid());
}

nil_handler::nil_handler()
{
}

nil_handler::~nil_handler()
{
}

void
nil_handler::marshal(service &, CORBA::Object_ptr)
{
}

void
nil_handler::marshal_roll_back(CORBA::Object_ptr)
{
}
