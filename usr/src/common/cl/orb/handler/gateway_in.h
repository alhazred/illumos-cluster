/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  gateway_in.h
 *
 */

#ifndef _GATEWAY_IN_H
#define	_GATEWAY_IN_H

#pragma ident	"@(#)gateway_in.h	1.9	08/05/20 SMI"

//
// class gateway_handler methods
//

//
// constructor
// InterfaceDescriptor lookups are unsupported for this handler class
//
inline
gateway_handler::gateway_handler(door_handle_t dh, door_id_t did, Xdoor *xdp,
    bool islocal) :
	gwh_door(dh),
	gwh_did(did),
	gwh_xdp(xdp),
	gwh_kernel_mc(0),
	gwh_local(islocal)
{
}

inline void
gateway_handler::unmarshal_roll_back()
{
}

inline void
gateway_handler::lock()
{
	gwh_lock.lock();
}

inline void
gateway_handler::unlock()
{
	gwh_lock.unlock();
}

inline int
gateway_handler::lock_held()
{
	return (gwh_lock.lock_held());
}

inline unsigned int
gateway_handler::get_kernel_marsh()
{
	return (gwh_kernel_mc);
}

inline void
gateway_handler::clear_kernel_marsh()
{
	gwh_kernel_mc = 0;
}

inline Xdoor *
gateway_handler::get_xdp()
{
	return (gwh_xdp);
}

inline void
gateway_handler::set_xdp(Xdoor *xdp)
{
	gwh_xdp = xdp;
}

inline door_handle_t
gateway_handler::get_door()
{
	return (gwh_door);
}

inline void
gateway_handler::set_door(door_handle_t dh)
{
	gwh_door = dh;
}

inline door_id_t
gateway_handler::get_did()
{
	return (gwh_did);
}
inline void
gateway_handler::set_did(door_id_t did)
{
	gwh_did = did;
}

//
// class gateway_manager methods
//

inline
gateway_manager::gateway_manager()
{
}

inline void
gateway_manager::lock()
{
	gwm_lock.lock();
}

inline void
gateway_manager::unlock()
{
	gwm_lock.unlock();
}

inline int
gateway_manager::lock_held()
{
	return (gwm_lock.lock_held());
}

//
// class gateway_proxy_data methods
//

inline
gateway_proxy_data::gateway_proxy_data(Buf *bufp, door_desc_t *descr) :
	gwp_buf(bufp),
	gwp_desc(descr)
{
}

inline
gateway_proxy_data::~gateway_proxy_data()
{
	if (gwp_buf) {
		gwp_buf->done();
	}
	delete [] gwp_desc;
}

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
//
// Constructor and Destructor for gateway_task class. gateway_task is
// used by the zone threadpool thread
//
inline
gateway_task::gateway_task(struct gateway_data *data) :
	datap(data)
{
}

inline
gateway_task::~gateway_task()
{
}
#endif	// SOL_VERSION >= __s10 && defined(_KERNEL)

#endif	/* _GATEWAY_IN_H */
