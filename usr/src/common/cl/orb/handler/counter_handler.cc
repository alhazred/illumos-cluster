//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)counter_handler.cc	1.23	08/05/20 SMI"

#include <orb/object/schema.h>
#include <orb/handler/counter_handler.h>

// delegate_counter_handler global structs

// There is only one instance of the delegate_counter_handler

//  One mutex is good enough
os::mutex_t delegate_counter_handler::lck;

// constructor
delegate_counter_handler::delegate_counter_handler()
{
}

delegate_counter_handler::~delegate_counter_handler()
{
}

void *
delegate_counter_handler::narrow(CORBA::Object_ptr object_p, CORBA::TypeId tid,
    generic_proxy::ProxyCreator)
{
	void	*proxyp = _ox_cast(object_p, tid);
	if (proxyp != NULL) {
		// yes, can cast
		(void) duplicate(object_p, tid);
	}
	return (proxyp);
}

void *
delegate_counter_handler::duplicate(CORBA::Object_ptr obp, CORBA::TypeId)
{
	lock();

	// XXX We need to add ASSERTs to catch an unsupported 0->1 transition
	obp->_this_component_ptr()->increase();

	unlock();
	return (obp);
}

// release
void
delegate_counter_handler::release(CORBA::Object_ptr obp)
{
	lock();
	if (obp->_this_component_ptr()->decrease() == 0) {
		unlock();
		obp->_unreferenced((unref_t)0);
	} else {
		unlock();
	}
}

void
delegate_counter_handler::marshal(service &, CORBA::Object_ptr)
{
}

void
delegate_counter_handler::marshal_roll_back(CORBA::Object_ptr)
{
}

bool
delegate_counter_handler::is_local()
{
	return (true);
}

bool
delegate_counter_handler::is_user()
{
#ifdef _KERNEL_ORB
	return (false);
#else
	return (true);
#endif
}

//
// counter_handler methods
//

// constructor
counter_handler::counter_handler() :
	refcount(1)
{
}

counter_handler::~counter_handler()
{
}

void *
counter_handler::duplicate(CORBA::Object_ptr obp, CORBA::TypeId)
{
	lock();
	//
	// We do not support a 0->1 transition in counter handler (yet)
	// This ASSERT catches such transitions
	//
	ASSERT(refcount != 0);
	refcount++;
	ASSERT(refcount != 0);		// ASSERT for wraparound
	unlock();
	return (obp);
}


// release
void
counter_handler::release(CORBA::Object_ptr obp)
{
	lock();
	ASSERT(refcount > 0);
	if (--refcount == 0) {
		unlock();
		obp->_unreferenced((unref_t)0);
	} else {
		unlock();
	}
}
