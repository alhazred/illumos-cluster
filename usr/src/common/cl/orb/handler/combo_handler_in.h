/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  combo_handler_in.h
 *
 */

#ifndef _COMBO_HANDLER_IN_H
#define	_COMBO_HANDLER_IN_H

#pragma ident	"@(#)combo_handler_in.h	1.8	08/05/20 SMI"

//
// combo_handler_kit methods
//

inline
combo_handler_kit::combo_handler_kit() :
	remote_handler_kit(COMBO_HKID)
{
}

inline void
combo_handler_kit::unmarshal_cancel_remainder(service &)
{
}

//
// combo_handler methods
//

inline
combo_handler::combo_handler(Xdoor *xdp) :
	remote_handler(xdp)
{
}

inline
combo_handler::combo_handler()
{
}

inline
void
combo_handler_client::incref()
{
	lock();
	nref++;
	ASSERT(nref != 0);	// catch wraparounds
	unlock();
}

inline
void
combo_handler_client::incpxref(CORBA::Object_ptr o)
{
	lock();
	o->_this_component_ptr()->increase();
	unlock();
}

inline
uint_t
combo_handler_client::decpxref(CORBA::Object_ptr o)
{
	return (o->_this_component_ptr()->decrease());
}

#endif	/* _COMBO_HANDLER_IN_H */
