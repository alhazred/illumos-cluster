/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _STANDARD_HANDLER_H
#define	_STANDARD_HANDLER_H

#pragma ident	"@(#)standard_handler.h	1.74	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/handler/remote_handler.h>
#include <sys/os.h>
#include <orb/refs/unref_threadpool.h>

class standard_handler_kit : public remote_handler_kit {
public:
	standard_handler_kit();
	remote_handler	*create(Xdoor *);
	void	unmarshal_cancel_remainder(service &);

	static standard_handler_kit &the();
	static int initialize();
	static void shutdown();

private:
	// Disallow assignments and pass by value
	standard_handler_kit(const standard_handler_kit &);
	standard_handler_kit &operator = (standard_handler_kit &);

	static standard_handler_kit *the_standard_handler_kit;
};

//
// Standard handler. Usual handler for most objects.
//
class standard_handler : public remote_handler {
public:
	hkit_id_t handler_id();

	bool is_user();

protected:
	// InterfaceDescriptor lookups are supported for this handler class.
	standard_handler(Xdoor *xdp);

	standard_handler();

	void	incref();

	// Number of proxies/pointers.
	uint_t	nref;

private:
	// Disallow assignments and pass by value.
	standard_handler(const standard_handler &);
	standard_handler &operator = (standard_handler &);
};


//
// Standard handler server contains a
// reference to the actual object implementation, as
// well as a marshalling count, to maintain coherence
// with the Xdoor layer.
//
class standard_handler_server :
	public standard_handler,
	public unref_task {
#if defined(_FAULT_INJECTION)
	// used by unref_fi test
	friend class fault_unref;
#endif
public:
	// InterfaceDescriptor lookups are supported for this handler class.
	standard_handler_server(CORBA::Object_ptr impl_objp);

	~standard_handler_server();

	// Acquires handler lock and return handler's marshal count.
	uint_t	begin_xdoor_unref();

	// If the xdoor has been unreferenced, decides if the handler
	// can be unreferenced too.
	void	end_xdoor_unref(bool);

	// Decides if a handler on the unref_threadpool can be ureferenced.
	void	deliver_unreferenced();


	// Returns true when unreference can be performed at this time.
	bool	last_unref();

	// Returns the handler to be used by the unref_task
	virtual handler		*get_unref_handler();

	// Return the type of unref_task
	virtual unref_task_type_t	unref_task_type();

	// Generate a new reference to the implementation object.
	void	new_reference();

	void	revoke();
	void	marshal(service &, CORBA::Object_ptr);

	CORBA::Object_ptr unmarshal(service &, CORBA::TypeId,
	    generic_proxy::ProxyCreator, CORBA::TypeId);

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void	release(CORBA::Object_ptr);

	bool	is_local();

	// Answers whether the server is dead.
	bool	server_dead();

protected:
	// Which object
	CORBA::Object_ptr	_obj();

private:
	CORBA::Object_ptr	obj_;

	// Record revoke state
	revoke_state_t		revoke_state_;

	// Record handler state
	handler_state_t		state_;

	// Disallow assignments and pass by value
	standard_handler_server(const standard_handler_server &);
	standard_handler_server &operator = (standard_handler_server &);
};

//
// The client side of a standard handler has to keep
// track of the deep type of an object.
//
class standard_handler_client :
	public standard_handler,
	public knewdel
{
public:
	// InterfaceDescriptor lookups are supported for this handler class.
	standard_handler_client(Xdoor *xdp);

	~standard_handler_client();

	void	marshal(service &, CORBA::Object_ptr);
	CORBA::Object_ptr unmarshal(service &, CORBA::TypeId,
	    generic_proxy::ProxyCreator, CORBA::TypeId);

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void	release(CORBA::Object_ptr obj);

	// Generate a new reference to the implementation object.
	void	new_reference();

	bool	is_local();

	// Answers whether the server is dead.
	bool	server_dead();

protected:
	void	incpxref(CORBA::Object_ptr o);

	uint_t	decpxref(CORBA::Object_ptr o);

	CORBA::Object_ptr _obj();

private:
	// Pointer to proxy object for the deep type.
	CORBA::Object_ptr	proxy_obj_;

	// Disallow assignments and pass by value.
	standard_handler_client(const standard_handler_client &);
	standard_handler_client &operator = (standard_handler_client &);
};

#include <orb/handler/standard_handler_in.h>

#endif	/* _STANDARD_HANDLER_H */
