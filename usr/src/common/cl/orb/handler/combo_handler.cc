//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)combo_handler.cc	1.67	08/05/20 SMI"

#include <sys/os.h>
#include <orb/object/schema.h>
#include <orb/handler/combo_handler.h>

#ifdef _KERNEL_ORB
#include <orb/infrastructure/orb_conf.h>
#include <orb/xdoor/simple_xdoor.h>
#else
#include <orb/xdoor/solaris_xdoor.h>
#endif

combo_handler_kit *combo_handler_kit::the_combo_handler_kit = NULL;

remote_handler *
combo_handler_kit::create(Xdoor *xdp)
{
	return (new combo_handler_client(xdp));
}

//
// Called by ORB::initialize to dynamically instantiate handler_kit.
//
int
combo_handler_kit::initialize()
{
	ASSERT(the_combo_handler_kit == NULL);
	the_combo_handler_kit = new combo_handler_kit();
	if (the_combo_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
combo_handler_kit::shutdown()
{
	if (the_combo_handler_kit != NULL) {
		delete the_combo_handler_kit;
		the_combo_handler_kit = NULL;
	}
}

//
// This method returns reference to combo_handler_kit instance.
//
inline combo_handler_kit&
combo_handler_kit::the()
{
	ASSERT(the_combo_handler_kit != NULL);
	return (*the_combo_handler_kit);
}

//
// combo_handler methods
//

bool
combo_handler::is_user()
{
#ifdef _KERNEL_ORB
	return (false);
#else
	return (true);
#endif
}

hkit_id_t
combo_handler::handler_id()
{
	return (combo_handler_kit::the().get_hid());
}

//
// marshal - loads the handler into the stream
//
void
combo_handler::marshal(service &serv, CORBA::Object_ptr objp)
{
	incmarsh();
	remote_handler::marshal(serv, objp);
}

//
//  Server methods
//

//
// constructor
//
combo_handler_server::combo_handler_server(CORBA::Object_ptr obp) :
	obj_(obp)
{
#ifdef _KERNEL_ORB
	xdoorp = simple_xdoor_server::create(this);
#else
	xdoorp = solaris_xdoor_server::create(this, 0);
#endif
}

//
// constructor
//

#ifdef _KERNEL_ORB

combo_handler_server::combo_handler_server(CORBA::Object_ptr obp,
    xdoor_id xdid) :
	obj_(obp)
{
	xdoorp = simple_xdoor_server::create(this, xdid);
}

#endif	// _KERNEL_ORB

//
//  destructor - revoke the xdoor first.
//
combo_handler_server::~combo_handler_server()
{
	// Combo handlers should always be revoked before being destroyed.
	revoke();
	ASSERT(revoke_state_.get() == REVOKED_HANDLER);
	obj_ = NULL;
}

//
// Unmarshal the handler.
// Nothing to do for server combo handlers.
//
CORBA::Object_ptr
combo_handler_server::unmarshal(service &, CORBA::TypeId,
    generic_proxy::ProxyCreator, CORBA::TypeId to_tid)
{
	//
	// Get the object pointer by casting from the narrowest type.
	// We have to adjust the pointer in case this object uses
	// multiple inheritance.
	//
	return ((CORBA::Object_ptr)direct_narrow(obj_, to_tid));
}

//
// revoke - the command can only be issued once.
//	The method requests that the xdoor revoke itself.
//	When the xdoor completes the revoke, the xdoor notifies the handler.
//
void
combo_handler_server::revoke()
{
	// The revoke command can only be issued once.
	lock();
	ASSERT(revoke_state_.get() != REVOKING_HANDLER &&
	    revoke_state_.get() != REVOKED_HANDLER);
	revoke_state_.set(REVOKING_HANDLER);
	unlock();

	xdoorp->revoked();

	lock();
	ASSERT(revoke_state_.get() == REVOKING_HANDLER);
	revoke_state_.set(REVOKED_HANDLER);
	unlock();
}

//
// Narrow the object to an object of the given type.
//
void *
combo_handler_server::narrow(CORBA::Object_ptr, CORBA::TypeId to_tid,
    generic_proxy::ProxyCreator)
{
	//
	// Narrow for the server is simple, because the deep type of
	// the object is the one the object already has, and thus is
	// known to the domain.  Only casting has to be performed.
	//
	return (direct_narrow(obj_, to_tid));
}

//
// duplicate - someone is using an additional object reference.
// Objects supported by this handler are not reference counted.
//
void *
#ifdef DEBUG
combo_handler_server::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId tid)
#else
combo_handler_server::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));
	return (object_p);
}

bool
combo_handler_server::is_local()
{
	return (true);
}

CORBA::Object_ptr
combo_handler_server::_obj()
{
	return (obj_);
}

//
// This method does nothing for combo servers.
//
void
combo_handler_server::release(CORBA::Object_ptr)
{
}

//  Client methods

//
// constructor - used for objects with non-fixed locations>
//
combo_handler_client::combo_handler_client(Xdoor *xdp) :
	combo_handler(xdp),
	nref(0),
	proxy_obj_(CORBA::Object::_nil())
{
}

//
// constructor - used for objects with known locations.
//
#ifdef _KERNEL_ORB

combo_handler_client::combo_handler_client(nodeid_t nd, xdoor_id xdid) :
	combo_handler(NULL),
	nref(1), // XXX
	proxy_obj_(CORBA::Object::_nil())
{
	// Can't create a client for a local server in this way (4161344)
	ASSERT(nd != orb_conf::node_number());
	xdoorp = simple_xdoor_client::create(nd, xdid, this);
}

#else	// _KERNEL_ORB

//
// constructor - used for objects with known locations
//
combo_handler_client::combo_handler_client(int xdid) :
	combo_handler(NULL),
	nref(1), // XXX
	proxy_obj_(CORBA::Object::_nil())
{
	xdoorp = solaris_xdoor_client::create(this, xdid);
}

#endif	// _KERNEL_ORB

extern InterfaceDescriptor _Ix__type(CORBA_Object);

//
// Unmarshal the handler.  This is called with the xdoor lock held.
// We must synchronize with the combo_handler_client::release function,
// which may have set nref to zero.
//
CORBA::Object_ptr
combo_handler_client::unmarshal(service &, CORBA::TypeId tid,
    generic_proxy::ProxyCreator create_func, CORBA::TypeId to_tid)
{
	CORBA::Object_ptr obj;

	//
	// Find the interface descriptor for this type.
	//
	InterfaceDescriptor *infp =
	    OxSchema::schema()->descriptor(tid, this);
	ASSERT(infp != NULL);

	lock();
	if (!CORBA::is_nil(proxy_obj_)) {
		//
		// Check to see that the proxy supports the correct deep type.
		// Note: the deep type can be CORBA::Object if another ORB
		// calls idlversion_impl::get_dynamic_descriptor() in this
		// ORB.
		// Otherwise, for non-HA references, the deep type should
		// always be the same.
		//
		if (infp == proxy_obj_->_deep_type()) {
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			return (obj);
		}
		// Forget the old proxy if we can cache the new one.
		if (infp != &_Ix__type(CORBA_Object)) {
			proxy_obj_ = CORBA::Object::_nil();
		}
	}

	//
	// Try to use the interface descriptor to create a proxy of the
	// deep type rather than creating a proxy of an ancestor class.
	// Keep a pointer to this proxy for unmarshal() and narrow()'s
	// later on. Note that the proxy_obj_ pointer doesn't have a
	// reference count associated with it; it is released when the
	// handler is released.
	//
	// If the server object is newer than the client code, we won't
	// be able to create a proxy for the deep type so we create
	// a proxy for the deepest (ancestor) type we know about locally.
	//
	// Also, don't cache the proxy if this is an unmarshal
	// for idlversion_impl::get_dynamic_descriptor().
	//
	unlock();
	obj = infp->create_proxy_obj(this, infp);
	if (CORBA::is_nil(obj)) {
		// Server is newer so leave proxy_obj_ nil.
		obj = (CORBA::Object_ptr)(*create_func)(this, infp);
		// Account for the new reference.
		incref();
	} else if (infp == &_Ix__type(CORBA_Object)) {
		// Account for the new reference.
		incref();
	} else {
		lock();
		if (CORBA::is_nil(proxy_obj_)) {
			proxy_obj_ = obj;
			// Account for the new reference.
			nref++;
			ASSERT(nref != 0);	// Check for wraparound
			unlock();
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(obj, to_tid);
		} else {
			//
			// We lost the race so delete ours and use the
			// other one.
			//
			ASSERT(infp == proxy_obj_->_deep_type());
			CORBA::Object_ptr tmp_obj = obj;
			//
			// Narrow the proxy to the requested type.
			//
			obj = (CORBA::Object_ptr)direct_narrow(proxy_obj_,
			    to_tid);
			if (!CORBA::is_nil(obj))
				proxy_obj_->_this_component_ptr()->increase();
			unlock();
			delete tmp_obj;
		}
	}
	return (obj);
}

//
// Narrow the object to an object of the given type.
// Note that we can only get a new reference from
// get_objref(), unmarshal(), or narrow().
//
void *
combo_handler_client::narrow(CORBA::Object_ptr object_p,
    CORBA::TypeId to_tid, generic_proxy::ProxyCreator create_func)
{
	void	*new_proxyp = direct_narrow(object_p, to_tid);
	if (new_proxyp != NULL) {
		//
		// Successful narrow on the existing proxy.
		// Increase the refcount in the proxy.
		//
		incpxref(object_p);
	} else if (object_p->_deep_type()->is_type_supported(to_tid) ||
	    object_p->_deep_type()->is_unknown()) {
		//
		// Note that the check:
		// object_p->_deep_type()->is_type_supported(to_tid)
		// is to make sure we don't allow invocations on the
		// server object it doesn't support.
		// If the deep type is unknown, we allow narrow
		// but don't allow invocations (see invoke()).
		//
		new_proxyp = (*create_func)(this, object_p->_deep_type());
		if (new_proxyp != NULL) {
			lock();

			// Increase the handler reference count.
			ASSERT(nref > 0);
			nref++;
			ASSERT(nref != 0);

			//
			// If we were unable to create a single proxy for
			// the deep type when the object was unmarshalled,
			// we save the pointer to reduce the number of
			// proxies created.
			//
			if (CORBA::is_nil(proxy_obj_) &&
			    !object_p->_deep_type()->is_unknown()) {
				proxy_obj_ = (CORBA::Object_ptr)new_proxyp;
			}

			unlock();
		}
	}
	return (new_proxyp);
}

//
// duplicate - someone is using an additional object reference.
//
void *
#ifdef DEBUG
combo_handler_client::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId tid)
#else
combo_handler_client::duplicate(CORBA::Object_ptr object_p, CORBA::TypeId)
#endif
{
	ASSERT(local_narrow(object_p, tid));

	// Cannot duplicate if you do not already have a reference.
	ASSERT(nref > 0);

	// Increase the proxy reference count.
	incpxref(object_p);

	return (object_p);
}

void
combo_handler_client::release(CORBA::Object_ptr obj)
{
	lock();
	if (decpxref(obj) == 0) {
		if (!CORBA::is_nil(proxy_obj_) && obj->_this_component_ptr() ==
		    proxy_obj_->_this_component_ptr()) {
			proxy_obj_ = CORBA::Object::_nil();
		}
		delete obj;
		ASSERT(nref > 0);
		if (--nref == 0) {
			if (xdoorp->client_unreferenced(Xdoor::KERNEL_HANDLER,
			    marshcount)) {
				delete this;
				return;
			} else {
				// The xdoor has passed up a reference to this
				// handler as part of an unmarshal. We'd better
				// stick around.
				// The xdoor has accounted for the marshal
				// count we sent down.
				marshcount = 0;
			}
		}
	}
	unlock();
}

//
// Generate a new reference to the implementation object.
//
void
combo_handler_client::new_reference()
{
	ASSERT(nref > 0);
	incref();
}

bool
combo_handler_client::is_local()
{
	return (false);
}

CORBA::Object_ptr
combo_handler_client::_obj()
{
	return (CORBA::Object::_nil());
}

//
// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.
//
#ifdef NOINLINES
#define	inline
#include <orb/handler/combo_handler_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
