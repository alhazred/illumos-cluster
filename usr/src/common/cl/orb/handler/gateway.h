/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _GATEWAY_H
#define	_GATEWAY_H

#pragma ident	"@(#)gateway.h	1.57	08/05/20 SMI"

#include <orb/handler/handler.h>
#include <orb/xdoor/Xdoor.h>
#include <sys/hashtable.h>
#include <sys/file.h>
#include <sys/door.h>
#include <sys/refcnt.h>

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
#include <orb/infrastructure/common_threadpool.h>
#include <h/replica.h>
#endif

#ifdef _FAULT_INJECTION
#include <orb/fault/fault_injection.h>
#endif

#ifndef _KERNEL
typedef int door_handle_t;
#endif

class gateway_handler : public handler {
public:
	// InterfaceDescriptor lookups are unsupported for this handler class
	gateway_handler(door_handle_t dh, door_id_t did, Xdoor *xdp,
		bool islocal);

	hkit_id_t handler_id();

	void	marshal_roll_back(CORBA::Object_ptr);
	void	unmarshal_roll_back();

	// Lower layer interfaces
	void	handle_incoming_call(service &);

	// Allow Xdoor to do unref processing
	uint_t	begin_xdoor_unref();
	void	end_xdoor_unref(bool);

	void	try_release();			// client side.

	void	marshal(service &, CORBA::Object_ptr);

	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator);

	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId);

	void	release(CORBA::Object_ptr);

	bool	is_local();
	bool	is_user();

	void		prepare_send(service &, door_arg_t *);

	Buf *		prepare_return(service &, door_arg_t *);

	Buf *		exception_prepare_return(CORBA::SystemException *,
			    service &, door_arg_t *);

#ifdef _LP64
	Buf *		prepare_receive(service &, door_arg_t *, ulong_t &);
#else
	Buf *		prepare_receive(service &, door_arg_t *);
#endif

	void		prepare_reply(service &, door_arg_t *, refcnt *);

	void		translate_down_stream(sendstream *, door_desc_t *,
			    uint_t);

	void		translate_down_stream_cancel(door_desc_t *, uint_t);

	door_desc_t	*translate_up_stream(recstream *, uint_t);

	door_handle_t	unmarshal_up(Xdoor *xdp);

	void		release_doors(door_desc_t *, uint_t);

	void		lock();
	void		unlock();
	int		lock_held();

	void		inc_kernel_marsh();
	unsigned int	get_kernel_marsh();
	void		clear_kernel_marsh();

	Xdoor		*get_xdp();
	void		set_xdp(Xdoor *xdp);
	door_handle_t	get_door();
	void		set_door(door_handle_t dh);
	door_id_t	get_did();
	void		set_did(door_id_t did);

	//
	// xdoor_proxy
	//
	// Proxy function that serves as the kernel door server for user to
	// kernel invocations. Every server object in the kernel that has a
	// client reference in userland has one solaris door using this
	// function as the door server function. To the kernel orb it looks
	// like a client handler. It takes data from door_arg_t structure and
	// converts it into an invocation object containing sendstream and
	// recstream. Then it invokes the xdoor's request_twoway() method and
	// follows the usual invocation path. For the reply, it converts the
	// recstream back to door_arg_t before returning from the client's
	// door_call().
	//
	// The arguments are:
	// data - pointer to the cookie or door-specific data that is assigned
	//	at the time of door creation.
	// da	- door_arg_t contains data for invocation. This may contain
	// 	object references, which are converted to xdoor references by
	//	gateway	handler's prepare_send() method. This is still pointing
	//	to user level address.
	// destfnp - pointer to function for freeing memory used to send
	// 	invocation results back to userland. This must be called
	// 	whenever there is no exception returned from the invocation.
	// destarg - pointer to buffer containing invocation results.
	// 	error - not used (was intended to return error, but we use
	// 	exceptions).
	//
	// In the bigger picture, there is one door created in the kernel for
	// every server that has given out a reference to a client in user
	// land. For each of these doors, the door-specific data points to the
	// gateway handler that created it.
	//
#ifdef _KERNEL
	static void xdoor_proxy(void *data, door_arg_t *da,
	    void (**destfnp)(void *, void *), void **destarg, int *error);
#else
	static void xdoor_proxy(void *data, char *argp, size_t arg_size,
	    door_desc_t *dp, uint_t n_desc);
#endif

	//
	// Destructor function called after a successful return from
	// xdoor_proxy. This will be called after all reply data has been
	// copied to user level.
	//
	static void xdoor_proxy_free(void *, void *arg);

private:
	// Number of references passed to kernel xdoors.
	unsigned int	gwh_kernel_mc;

	Xdoor		*gwh_xdp;

	//
	// door handle is used to pass and operate on doors but may not
	// be unique (i.e., more than one door handle can refer to the
	// same door).  door id is guaranteed to be unique for a given
	// door so it is used in the mapping table.
	//
	door_handle_t	gwh_door;
	door_id_t	gwh_did;

	bool		gwh_local;
	os::mutex_t	gwh_lock;

	// Disallow assignments and pass by value
	gateway_handler(const gateway_handler &);
	gateway_handler &operator = (gateway_handler &);
};

//
// The gateway_manager keeps track of the mappings between kernel xdoors
// and user-level doors.  The doors here are stored and looked up in the
// table based on vnode since two references to the same object through
// different file structures need to map to the same xdoor.
//
class gateway_manager {
public:
	gateway_manager();

	void		register_mapping(door_id_t, gateway_handler *);
	void		release_mapping(door_id_t);
	Xdoor		*translate_down(door_handle_t, door_id_t, bool);
	door_handle_t	translate_up(Xdoor *, bool unmarshalled);

	void		lock();
	void		unlock();
	int		lock_held();

protected:
	gateway_handler *lookup_mapping(door_id_t);

private:
	os::mutex_t	gwm_lock;

	hashtable_t<gateway_handler *, door_id_t> gwm_table;

	// Disallow assignments and pass by value
	gateway_manager(const gateway_manager &);
	gateway_manager &operator = (gateway_manager &);
};

class gateway_proxy_data {
public:
	gateway_proxy_data(Buf *bufp = NULL, door_desc_t *descr = NULL);
	~gateway_proxy_data();

private:
	Buf *gwp_buf;
	door_desc_t *gwp_desc;

	// Disallow assignments and pass by value
	gateway_proxy_data(const gateway_proxy_data &);
	gateway_proxy_data &operator = (gateway_proxy_data &);
};

//
// The Buf that keeps a pointer to a refcnt. It does a hold in its
// constructor and a rele in its destructor.
//
class count_Buf : public Buf {
public:
	count_Buf(uchar_t *, uint_t, refcnt *, alloc_type = HEAP,
	    alloc_type = OTHER);

	~count_Buf();

	Buf	*dup(uint_t, uint_t, os::mem_alloc_type);

private:
	refcnt	*count;

	// Disallow assignment and copy constructor
	count_Buf(const count_Buf &);
	count_Buf &operator = (count_Buf &);
};


#if (SOL_VERSION >= __s10) && defined(_KERNEL)
//
// Data required by the zone threadpool thread
// to support processing an invocation from a non-global zone.
//
struct gateway_data {
	Xdoor		*xdp;
	ExceptionStatus result;
	invocation	*invop;
#ifdef _FAULT_INJECTION
	replica::service_num_t svc_id;
	InvoTriggers	*trigp;
#endif
};

//
// The gateway::xdoor_proxy() code uses this class for handing off
// an invocation from a non-global zone to a zone threadpool thread.
// An object(task) of this type is enqueued on the deferred task list
// so that a zone threadpool thread can pick up the task later.
// The 'datap' structure contains the data required for processing
// the invocation.
//
class gateway_task : public defer_task {
public:
	gateway_task(struct gateway_data *);
	~gateway_task();
	void execute();

	os::mutex_t	gateway_task_lock;
	os::condvar_t	gateway_task_cv;
	struct gateway_data *datap;
};

#endif // SOL_VERSION >= __s10 && defined(_KERNEL)

#include <orb/handler/gateway_in.h>

#endif	/* _GATEWAY_H */
