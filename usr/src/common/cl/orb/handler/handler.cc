//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//


#pragma ident	"@(#)handler.cc	1.64	08/05/20 SMI"

#include <sys/os.h>
#include <orb/object/schema.h>
#include <orb/handler/handler.h>
#include <orb/refs/unref_threadpool.h>

handler_repository	*handler_repository::the_handler_repository = NULL;
//
// handler methods
//

handler::~handler()
{
	handler_cookie = NULL; // Make lint happy. Null the pointers.
}

hkit_id_t
handler::handler_id()
{
	// Some handlers - multi_ckpt_handler and hxdoor do not have a
	// handler kit and an handler id. Rest do.
	// However, the debug routine is_remote_handler uses this method
	// and we return an invalid id if called.
	return ((hkit_id_t)-1);
}

//
// Takes care of an incoming invocation
// Only server side handlers implement this operation.
//
// The default implementation is for handlers not supporting this operation.
//
void
handler::handle_incoming_call(service &)
{
	ASSERT(0);
}

uint_t
handler::begin_xdoor_unref()
{
	ASSERT(0);
	return (0);
}

void
handler::end_xdoor_unref(bool)
{
	ASSERT(0);
}

//
// The following methods, is_local, is_user, narrow, duplicate and release,
// were all pure virtual methods at one point.  With the introduction
// of the SC5.2 compiler, derived classes are required to define all pure
// virtual methods of their base classes, even if the derived class doesn't
// use the method.
//
// To avoid this clutter we've made these "unpure" virtual methods so that the
// derived classes no longer have to worry about defining unused methods.
//
// We've added asserts here to assure us that we are never called.
//
bool
handler::is_local()
{
	ASSERT(0);
	return (false);
}

bool
handler::is_user()
{
	ASSERT(0);
	return (false);
}

void *
handler::narrow(CORBA::Object_ptr, CORBA::TypeId, generic_proxy::ProxyCreator)
{
	ASSERT(0);
	return (NULL);
}

void *
handler::duplicate(CORBA::Object_ptr, CORBA::TypeId)
{
	ASSERT(0);
	return (NULL);
}

void
handler::release(CORBA::Object_ptr)
{
	ASSERT(0);
}

void
handler::new_reference()
{
}

//
// invoke - This version performs marshaling/unmarshaling
// of parameters. It is the one called from the stubs.
// Remote invocations do not happen on the server side
//
// The default implementation is for handlers not supporting this action.
//
ExceptionStatus
handler::invoke(arg_info &, ArgValue *, InterfaceDescriptor *, Environment &)
{
	ASSERT(0);
	if (is_local()) {
		return (CORBA::SYSTEM_EXCEPTION);
	} else {
		return (CORBA::LOCAL_EXCEPTION);
	}
}

void *
handler::local_invoke(CORBA::TypeId, InterfaceDescriptor *, Environment &)
{
	return (NULL);
}

ExceptionStatus
handler::local_invoke_done(Environment &)
{
	ASSERT(0);
	if (is_local()) {
		return (CORBA::SYSTEM_EXCEPTION);
	} else {
		return (CORBA::LOCAL_EXCEPTION);
	}
}

void
handler::send_reply(service &, arg_info &, ArgValue *)
{
	ASSERT(0);
}

bool
handler::server_dead()
{
	ASSERT(0);
	return (false);
}

void
handler::set_cookie(void *val)
{
	ASSERT(is_local());
	handler_cookie = val;
}

void *
handler::get_cookie()
{
	ASSERT(is_local());
	return (handler_cookie);
}

//
// become_primary_event - Notify handler that it has become a primary.
// Most handlers do not support this notification.
//
void
handler::become_primary_event()
{
	ASSERT(0);
}


//
// become_secondary_event - Notify handler that it has become a secondary.
// Most handlers do not support this notification.
//
void
handler::become_secondary_event()
{
	ASSERT(0);
}

bool
handler::equiv(CORBA::Object_ptr o1, CORBA::Object_ptr o2)
{
	//
	// Equivalence is implemented based on the unicity of the
	// handler associated with an object reference.
	//
	if (!CORBA::is_nil(o1) && !CORBA::is_nil(o2)) {
		return (o1->_handler()) == (o2->_handler());
	} else {
		return (o1 == o2);
	}
}

uint_t
handler::hash(uint_t sz)
{
	uintptr_t hashval = (uintptr_t)this;

	return (((uint_t)((hashval >> 7) + (hashval >> 3))) % sz);
}

//
// handler::handler_state_t methods
// Return:	string name that is converted from the handler state
//
#if defined(_FAULT_INJECTION)
const char *
handler::handler_state_t::handler_state_2_string(handler_states_t state)
{
	switch (state) {
	case NEW_HANDLER:
		return ("NEW_HANDLER");

	case ACTIVE_HANDLER:
		return ("ACTIVE_HANDLER");

	case UNREF_SCHEDULED:
		return ("UNREF_SCHEDULED");

	case UNREF_PROCESS:
		return ("UNREF_PROCESS");

	case UNREF_DONE:
		return ("UNREF_DONE");

	case UNREF_UNSCHEDULE:
		return ("UNREF_UNSCHEDULE");

	case UNREF_NO_PROCESS:
		return ("UNREF_NO_PROCESS");

	case SECONDARY_HANDLER:
		return ("SECONDARY_HANDLER");

	case UNREF_CKPT_PROCESS:
		return ("UNREF_CKPT_PROCESS");

	case UNREF_CKPT_DONE:
		return ("UNREF_CKPT_DONE");

	case CLIENT_HANDLER:
		return ("CLIENT_HANDLER");

	default:
		return ("UNKNOWN_HANDLER_STATE");
	}
}
#endif // _FAULT_INJECTION

//
// handler_kit methods
//

handler_kit::handler_kit(const hkit_id_t kid) :
	kit_id(kid)
{
	handler_repository::the().add_kit(this);
}

handler_kit::~handler_kit()
{
	handler_repository::the().remove_kit(this);
}

//
// Handler Repository stuff
//

// Add a kit to the repository.  Note that we only allow one kit of a
// given id to be added.  Anything else does not make sense.
void
handler_repository::add_kit(handler_kit *kitp)
{
	hkit_id_t	kid = kitp->get_hid();

	ASSERT((kid < NUM_HANDLER_KITS) && (handler_kits[kid] == NULL));
	handler_kits[kid] = kitp;
}

// Remove a kit from the repository.
void
handler_repository::remove_kit(handler_kit *kitp)
{
	hkit_id_t	kid = kitp->get_hid();

	ASSERT((kid < NUM_HANDLER_KITS) && (handler_kits[kid] == kitp));
	handler_kits[kid] = NULL;
}

//
// Called by ORB::initialize to dynamically instantiate handler_repository.
//
int
handler_repository::initialize()
{
	ASSERT(the_handler_repository == NULL);
	the_handler_repository = new handler_repository();
	if (the_handler_repository == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// Called at the end of ORB::shutdown.
//
void
handler_repository::shutdown()
{
	if (the_handler_repository != NULL) {
		delete the_handler_repository;
		the_handler_repository = NULL;
	}
}

//
// This method returns reference to handler_repository instance.
//
handler_repository &
handler_repository::the()
{
	ASSERT(the_handler_repository != NULL);
	return (*the_handler_repository);
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <orb/handler/handler_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
