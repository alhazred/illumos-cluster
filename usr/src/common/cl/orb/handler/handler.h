//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _HANDLER_H
#define	_HANDLER_H

#pragma ident	"@(#)handler.h	1.82	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/invo/invocation.h>

typedef uint8_t hkit_id_t;

//
// Handler Kit Ids for various handlers - reserves slots in the
// handler respoitory.
// Note that there is a debug function is_remote_handler() that
// should be updated if we add new handler types that inherit from
// remote_handler.
// XXX Also, changing these will affect rolling upgrade.
//
#define	NIL_HKID	0
#define	STANDARD_HKID	1
#define	COMBO_HKID	3
#define	BULKIO_HKID	4
#define	SOLOBJ_HKID	5
#define	CKPT_HKID	8
#define	REPL_HKID	9
#define	DATA_CONTAINER_HKID	10
#define	NAMING_ITER_HKID	11
#define	BULKIO_HKID_V1	12

//
// The number of handler kits we support.  This needs to be updated
// whenever an additional handler kit is added.
//
#define	NUM_HANDLER_KITS (BULKIO_HKID_V1 + 1)

//
// The gateway handler doesn't have a handler kit for passing this
// handler type as a reference to another node.
// This number is used so idlversion_impl::get_dynamic_descriptor()
// can tell that a handler is a gateway handler.
//
#define	GATEWAY_HKID	(hkit_id_t)-2

//
// Each handler type implements a kit object which specifies its type
// using an hkit_id_t identifier and understands how to unmarshal
// the objects associated with handlers of this type.
// When an IDL object reference is marshaled, its kit id is marshaled
// first followed by the handler specific marshal data.
// Note that this requires that the the kit be defined with the same
// kit id in all domains which wish to be able to marshal or unmarshal
// such references.  For that reason, we statically allocate kit ids using
// the identifiers defined above.
//
class handler_kit {
public:
	//
	// Extract an object from the marshal stream.  Some handlers
	// generate a proxy reference for the object. The ProxyCreator
	// parameter specifies the appropriate proxy for the parameter
	// being unmarshaled.
	// The typeid argument specifies the type of the object to return
	// so the caller doesn't need to do more narrows/casts.
	//
	virtual CORBA::Object_ptr	unmarshal(service &,
			generic_proxy::ProxyCreator, CORBA::TypeId) = 0;

	// Cancel the unmarshal of an object reference.
	virtual void	unmarshal_cancel(service &) = 0;

	hkit_id_t	get_hid() const;

	static bool	is_remote_handler(hkit_id_t);

protected:
	handler_kit(const hkit_id_t kid);

	virtual		~handler_kit();

private:
	// The kit id for this kit.
	hkit_id_t	kit_id;

	// Disallow default constructor.
	handler_kit();

	// Disallow assignments and pass by value.
	handler_kit(const handler_kit &);
	handler_kit &operator = (handler_kit &);
};

//
// class handler is the base class for all types of handlers, and
// defines the interface that all handlers must support.
// The handler class supports infrastructure operations on an object,
// such as marshalling/unmarshalling and maintaining information
// for the infrastructure about an object.
// The proxy supports the methods for an object on the client side.
// The implementation object support methods for an object on the server side.
//
// Note: smart pointers should NOT be used by any of the handler classes
// because smart pointers grab a lock before doing any of the assignment
// operation. This operation involves a call to release method
// implemented by the handler classes which in turn grab handler locks.
// As a result usage of smart pointer's by handler functions in USERLAND would
// lead to a Deadlock. similarly some of the operator overloading methods
// call duplicate implemented by handler classes which will again lead to
// Deadlock in KERNEL code.
//
class handler {
public:
	// Lower layer interface

	//
	// Returns the id of the handler type.
	//
	virtual hkit_id_t	handler_id();

	//
	// Takes care of an incoming invocation.
	// Only server side handlers implement this operation.
	//
	virtual void 		handle_incoming_call(service &);

	//
	// Xdoor can be unreferenced only if its marshal count matches
	// that of the handler. Therefore it acquires handler lock by
	// calling begin_xdoor_unref() method. After doing its checks,
	// xdoor calls end_xdoor_unref() method indicating if the
	// xdoor is being deleted or not. The handler then can decide
	// if it should be unreferenced. The lock ordering is obtain
	// the handler lock first, and then the xdoor lock.
	//
	// begin_xdoor_unref - obtains handler lock and returns
	// handler's marshal count.
	//
	virtual uint_t  	begin_xdoor_unref();
	//
	// end_xdoor_unref - informs handler whether the underlying
	// xdoor has been unreferenced and releases the handler lock.
	//
	virtual void    	end_xdoor_unref(bool);

	// Upper layer interface

	//
	// Puts a representation of the object in the
	// sending stream of the service structure.
	//
	virtual void  		marshal(service &, CORBA::Object_ptr) = 0;

	//
	// Invoked when marshaling of the parameter list
	// for some other object fails. This gives a chance to the
	// handler to recover its marshal count (maybe do some other
	// maintenance).
	//
	virtual void  		marshal_roll_back(CORBA::Object_ptr) = 0;

	//
	// True for servers, false for clients.
	//
	virtual bool		is_local() = 0;

	//
	// True for user-level and gateway handlers, false for kernel
	// things.
	//
	virtual bool		is_user() = 0;

	//
	// This version performs marshaling/unmarshaling
	// of parameters. It is the one called from the stubs.
	// Remote invocations do not happen on the server side.
	//
	virtual ExceptionStatus	invoke(arg_info &, ArgValue *,
	    InterfaceDescriptor *infp, Environment &);

	//
	// Return a pointer to the implementation object if this can
	// be processed in this domain; otherwise, return NULL.
	// If non-NULL is returned, the caller must call local_invoke_done()
	// when it is finished with the pointer.
	// We provide a default implementation since most handlers don't
	// implement this.
	//
	virtual void *local_invoke(CORBA::TypeId, InterfaceDescriptor *,
	    Environment &);

	//
	// We are done with the invoke() which was permitted by a previous call
	// to local_invoke();
	//
	virtual ExceptionStatus	local_invoke_done(Environment &);

	//
	// Narrow operation for objects.
	// Returns an object reference of the specified type id.
	// It increases the reference count of the object or proxy.
	//
	virtual void	*narrow(CORBA::Object_ptr, CORBA::TypeId,
			    generic_proxy::ProxyCreator) = 0;

	//
	// Returns another reference of the same type as given.
	// It always increases the reference count.
	//
	virtual void	*duplicate(CORBA::Object_ptr, CORBA::TypeId) = 0;

	//
	// Decreases the reference count of the object.
	// In most cases, it will perform extra checks to determine
	// when unreferenced should be called for the xdoor or the
	// object being managed.
	//
	virtual void	release(CORBA::Object_ptr) = 0;

	//
	// Increase the reference count of the object.
	// This is a helper routine for CORBA::create_proxy_obj().
	//
	virtual void	new_reference();

	//
	// Determines if the two object references are the same
	// There is a default implementation which works well with most
	// of the handler types.
	//
	virtual bool	equiv(CORBA::Object_ptr, CORBA::Object_ptr);

	//
	// Returns a "hash" value, guaranteed to be the same for
	// all references of the object (and 0 <= hash value < size).
	//
	virtual uint_t	hash(uint_t size);

	//
	// High-level routines can set and retrieve a cookie for
	// implementation-dependent purposes.  Generally it will be used
	// to provide a mechanism to get from a CORBA::Object to an
	// underlying implementation.  These functions are used to set
	// and get the cookie. It is up to the caller to perform any
	// necessary locking.
	//
	void	set_cookie(void *val);
	void	*get_cookie();

	//
	// This routine is called from skeleton through the service object
	// to do the handler level reply message processing.
	//
	virtual void	send_reply(service &, arg_info &, ArgValue *);

	//
	// The handlers that really need this will have to override it.
	// It answers whether the server for a client is dead.
	//
	virtual bool	server_dead();

	//
	// Notify handler that it has become a primary.
	// Most handlers do not support this notification.
	//
	virtual void	become_primary_event();

	//
	// Notify handler that it has become a secondary.
	// Most handlers do not support this notification.
	//
	virtual void	become_secondary_event();

	enum revoke_states_t {
		//
		// Revoke - the revoke state bits pertain only to
		// the revoke process and are currently independent
		// of the handler states.
		//
		NOT_REVOKED,
		REVOKING_HANDLER,	// Revoke in progress
		REVOKED_HANDLER		// Revoke completed
	};

	//
	// revoke_state_t - this class supports accessing the revoke state.
	// The internal storage is packed into an 8 bit quantity to save space.
	//
	class revoke_state_t {
	public:
		revoke_state_t();

		// Return current revoke state.
		revoke_states_t	get() const;

		// Set revoke state.
		void		set(revoke_states_t);

	private:
		uint8_t		_state;
	};

	//
	// Define the valid states for a server handler
	// that supports an unreference protocol.
	//
	// Those handlers that can only be client handlers
	// do not participate in the handler states described here.
	//
	// The handler states are defined here in order to promote
	// consistency among handlers.
	//
	// Server handler objects inherit from handler_unref_task.
	// The state machine notes when the handler_unref_task is active.
	// There is a small window during which processing for the current
	// unreference notification can overlap with a new unreference
	// notification. This is safe because the current unreference
	// notification does NOT access either the handler_unref_task
	// or the handler during this window.
	//
	// The system does full state checking in DEBUG systems using
	// assert statements. The no-debug system does not always execute the
	// last_unref method, and can directly destroy the handler instead.
	// This is permissable because the assert statements in the destructor
	// also go away in a no-debug system.
	//
	// Notes:
	// In the handler states table, the presence of references refers to
	// the existence of references to handler's implementation object.
	// A client handler does not have an implementation object,
	// and therefore cannot have a reference to its implementation object.
	// The system does not allow the creation of references to
	// secondaries, so secondary handlers do not have references to their
	// implementation object.
	//
	// Server handlers inherit from handler_unref_task.
	// So an unref_task is always present. The table identifies
	// the states in which the unref_task is active.
	//
	// (*) next to the action or state means that this is only
	// allowed for a handler supporting multiple 0->1 ref count transitions.
	// Reference counted handlers begin with a zero ref count.
	//
	// (**) next to the action or state means that this is only
	// allowed for replica handler. The replica handler is unique in that
	// the replica handler can be either a server or client handler.
	//
	enum handler_states_t {
		//
		// State: The handler was just created as a server handler.
		//
		// No references exist.
		// unref_task inactive.
		//
		// Transition Action	Next State
		// create reference	ACTIVE_HANDLER
		// destroy handler	handler destroyed
		//
		NEW_HANDLER = 0,

		//
		// State: This is the normal state for an active handler.
		//
		// When a replica handler switches between primary and
		// secondary there is a small window during which no
		// references may exist. Before the switchover completes,
		// the replica handler will either become a secondary or
		// have references. This violates the rules of a pure
		// state machine. So this description identifies the
		// properties of this state, including this window.
		//
		// Yes references exist.
		// unref_task inactive.
		//
		// Transition Action		Next State
		// create reference		ACTIVE_HANDLER
		// release last ref		UNREF_SCHEDULED
		// (**)become secondary		SECONDARY_HANDLER
		//
		ACTIVE_HANDLER = 1,

		//
		// State: The handler has queued an unref_task
		// on the unref_threadpool.
		//
		// No references exist.
		// unref_task active.
		//
		// Transition Action		Next State
		// deliver unreference		UNREF_PROCESS
		// (*)create reference		UNREF_UNSCHEDULE
		// (**)clear_unref_flag		ACTIVE_HANDLER
		//	This transition occurs when a replica handler
		//	is switching from being a primary to a secondary.
		//	This action discards the unref notice.
		//
		UNREF_SCHEDULED = 2,

		//
		// State: The system has executed the handler
		// delivered_unreferenced method.
		//
		// No references exist.
		// unref_task active.
		//
		// Transition Action		Next State
		// last_unref			UNREF_DONE
		// (*)create reference		UNREF_NO_PROCESS
		//
		UNREF_PROCESS = 3,

		//
		// State: The handler is inactive and can be destroyed
		// at any time.
		//
		// No references exist.
		// unref_task inactive.
		//
		// Transition Action		Next State
		// destroy handler		handler destroyed
		// (*)create reference		ACTIVE_HANDLER
		// (**)become secondary		SECONDARY_HANDLER
		//
		UNREF_DONE = 4,

		//
		// State(*): A 0->1 transition occurred
		// before deliver unreferenced occurred.
		//
		// Yes references exist.
		// unref_task active.
		//
		// Transition Action	Next State
		// create reference	UNREF_UNSCHEDULE
		// release last ref	UNREF_SCHEDULED
		// deliver_unreferenced	ACTIVE_HANDLER
		//
		UNREF_UNSCHEDULE = 5,

		//
		// State(*): A 0->1 transition occurred after the
		// execution of the handler deliver_unreferenced method.
		//
		// Yes references exist.
		// unref_task active.
		//
		// Transition Action	Next State
		// create reference	UNREF_NO_PROCESS
		// last_unref		ACTIVE_HANDLER
		// release last ref	UNREF_PROCESS
		//
		UNREF_NO_PROCESS = 6,

		//
		// State(**): The replica handler is a secondary.
		//
		// No references exist.
		// unref_task inactive.
		//
		// Transition Action	Next State
		// become primary	ACTIVE_HANDLER
		// unref_by_ckpt	UNREF_CKPT_PROCESS
		// become secondary	SECONDARY_HANDLER
		//
		// become spare and
		//	no primary refs	exist	handler destroyed
		//	yes primary refs exist	CLIENT_HANDLER
		//
		SECONDARY_HANDLER = 7,

		//
		// State(**): Received an unreference via a checkpoint
		// The implementation object cannot reject
		// an unreference arriving by a checkpoint.
		//
		// No references exist.
		// unref_task inactive.
		//
		// Transition Action	Next State
		// last_unref		UNREF_CKPT_DONE
		//
		UNREF_CKPT_PROCESS = 8,

		//
		// State(**): The handler's last_unref method has been executed,
		// but the handler has not yet been destroyed.
		// The implementation object cannot reject
		// an unreference arriving by a checkpoint.
		//
		// No references exist.
		// unref_task inactive.
		//
		// Transition Action	Next State
		// rem_impl		handler destroyed
		//	There can be no references to the primary,
		//	so method rem_impl destroys the handler.
		//
		UNREF_CKPT_DONE = 9,

		//
		// State(**): The handler is neither a primary nor a secondary
		// handler. Non-primary handlers are created in this state.
		//
		// No references exist.
		// unref_task inactive.
		//
		// Transition Action	Next State
		// destroy handler	handler destroyed
		// add_impl		SECONDARY_HANDLER
		//	This adds an implementation object and converts
		//	the handler to a secondary.
		//
		CLIENT_HANDLER = 10
	};

	//
	// handler_state_t - this class supports accessing the handler state.
	// The internal storage is packed into an 8 bit quantity to save space.
	//
	class handler_state_t {
	public:
		handler_state_t();
		handler_state_t(handler_states_t state);

		// Return current handler state
		handler_states_t	get() const;

		// Set handler state
		void			set(handler_states_t);

#if defined(_FAULT_INJECTION)
		// convert handler state to string name
		static const char	*handler_state_2_string(
					    handler_states_t);
#endif

	private:
		uint8_t			_state;
	};

protected:
	handler();

	virtual ~handler();

private:
	//
	// High-level routines can set and retrieve this cookie for
	// implementation-dependent purposes.  Generally it will be used
	// to provide a mechanism to get from a CORBA::Object to an
	// underlying implementation.
	//
	void			*handler_cookie;

	// Disallow assignments and pass by value
	handler(const handler &);
	handler &operator = (handler &);
};

//
// A handler repository is a data base of handlers, kept by handler id.
// Handlers can be registered if they have reserved a slot in the defines
// above.
//
class handler_repository {
public:
	handler_repository();
	~handler_repository();

	// returns the only handler repository
	static handler_repository &the();

	static int initialize();
	static void shutdown();

	void	add_kit(handler_kit *);
	void	remove_kit(handler_kit *);

	handler_kit *get_kit(hkit_id_t kid);

private:
	// We maintain an array of handler kit pointers.
	// This is initialized to NULL by C++ (since the repository is
	// a static class). The constructors and destructors for each
	// handler kit add and remove the kits from this array using the
	// utility functions above.
	//
	handler_kit *handler_kits[NUM_HANDLER_KITS];

	// Disallow assignments and pass by value
	handler_repository(const handler_repository &);
	handler_repository &operator = (handler_repository &);

	static handler_repository *the_handler_repository;
};

#ifndef NOINLINES
#include <orb/handler/handler_in.h>
#endif  // NOINLINES

#endif	/* _HANDLER_H */
