/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _REMOTE_HANDLER_H
#define	_REMOTE_HANDLER_H

#pragma ident	"@(#)remote_handler.h	1.52	08/05/20 SMI"

#include <orb/handler/handler.h>
#include <orb/object/schema.h>
#include <orb/xdoor/Xdoor.h>

//
// remote_handler defines the common routines and features
// of all handlers  which need to be used by other remote handlers
// (inter-address space invocations). Notice that it only
// handles the calls common to all remote handlers.
//

// Forward declaration
class remote_handler;

//
// The remote_handler_kit defines a shared unmarshal implementation
// which calls the virtual function "create()" to allocate the
// appropriate derived handler and a handler virtual unmarshal()
// function do any handler specific unmarshaling.
// Remote handler objects are marshaled as an xdoor pointer followed by an
// MD5 typeid.
//
class remote_handler_kit : public handler_kit {
public:
	CORBA::Object_ptr unmarshal(service &, generic_proxy::ProxyCreator,
		CORBA::TypeId);

	void	unmarshal_cancel(service &);

protected:
	virtual remote_handler	*create(Xdoor *) = 0;
	virtual void		unmarshal_cancel_remainder(service &) = 0;

	remote_handler_kit(hkit_id_t kid);
	~remote_handler_kit();

private:
	// Disallow assignments and pass by value.
	remote_handler_kit(const remote_handler_kit &);
	remote_handler_kit & operator = (remote_handler_kit &);
};

class remote_handler : public handler {
#ifndef	_KERNEL
	//
	// following friend class defines pthread_atfork handler functions.
	// They lock handlers when fork()/fork1() is called by user-land
	// process, to avoid fork-one safety problem.
	//
	friend class pthread_atfork_handler;
#endif	// _KERNEL
public:
	// Lower layer interface
	void	handle_incoming_call(service &);

	// Upper layer interface
	void	marshal(service &, CORBA::Object_ptr);
	void	marshal_roll_back(CORBA::Object_ptr);

	ExceptionStatus	invoke(arg_info &, ArgValue *, InterfaceDescriptor *,
	    Environment &);

	//
	// Returns the xdoor for the remote handler.
	//
	Xdoor	*get_xdoorp();

	//
	// Unmarshal handler specific information and do any necessary
	// reference counting.
	// This is called with the xdoor lock held.
	// Returns a reference to the object managed by this handler.
	// It may create a proxy or use an implementation object.
	//
	virtual CORBA::Object_ptr unmarshal(service &, CORBA::TypeId,
	    generic_proxy::ProxyCreator, CORBA::TypeId) = 0;

	virtual void	send_reply(service &, arg_info &, ArgValue *);

	//
	// To be used by server handlers. It returns the object associated with
	// the handler. Clients return nil.
	//
	virtual CORBA::Object_ptr _obj() = 0;

	//
	// Narrows, whenever possible, using the same
	// proxy/C++ object pointer.
	//
	void *direct_narrow(CORBA::Object_ptr, CORBA::TypeId);

	//
	// Returns true if this handler supports type to_tid.
	//
	bool local_narrow(CORBA::Object_ptr, CORBA::TypeId to_tid);

protected:
	//
	// constructor -
	// InterfaceDescriptor lookups are supported for this handler class.
	//

	// When we are created via an unmarshal (client side).
	remote_handler(Xdoor *xdp);

	// When we are created with an implementation object (server side).
	remote_handler();

	// Locking functions.
	os::mutex_t	lck;
	void		lock();
	void		unlock();
	bool		lock_held();
	bool		try_lock();

	// Number of marshalling operations performed.
	uint_t		marshcount;
	void		incmarsh();

	//
	// Helper function for invoke() to output the interface index
	// and method numbers.
	//
	void	put_index_method(invocation &invo, InterfaceDescriptor *infp,
		    int index, uint_t method);

	//
	// Helper function for handle_incoming_call() to extract
	// the interface index and method numbers and return the
	// appropriate receiver function to call.
	//
	InterfaceDescriptor::ReceiverFunc get_index_method(service &serv,
	    InterfaceDescriptor *infp, uint_t &index);

	//
	// The system requires following variable to be accurate.
	// Pointer to the xdoor associated with this handler, otherwise NULL.
	//
	Xdoor		*xdoorp;

private:
	ExceptionStatus		invoke_oneway(arg_info &ai,
	    ArgValue *avp, const invocation_mode &invo_mode,
	    InterfaceDescriptor *infp, Environment &e);

	ExceptionStatus		invoke_twoway(arg_info &ai,
	    ArgValue *avp, const invocation_mode &invo_mode,
	    InterfaceDescriptor *infp, Environment &e);

	// Disallow assignments and pass by value.
	remote_handler(const remote_handler &);
	remote_handler &operator = (remote_handler &);
};

#ifndef NOINLINES
#include <orb/handler/remote_handler_in.h>
#endif  // _NOINLINES

#endif	/* _REMOTE_HANDLER_H */
