/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  standard_handler_in.h
 *
 */

#ifndef _STANDARD_HANDLER_IN_H
#define	_STANDARD_HANDLER_IN_H

#pragma ident	"@(#)standard_handler_in.h	1.15	08/05/20 SMI"

//
// standard_handler_kit methods
//

inline
standard_handler_kit::standard_handler_kit() :
	remote_handler_kit(STANDARD_HKID)
{
}

inline void
standard_handler_kit::unmarshal_cancel_remainder(service &)
{
}

//
// standard_handler methods
//

inline
standard_handler::standard_handler(Xdoor *xdp) :
	remote_handler(xdp),
	nref(0)
{
}

inline
standard_handler::standard_handler() :
	remote_handler(),
	nref(0)
{
}

//
// Note: the lock is used for other purposes besides just incrementing
// the reference count so we probably can't we use atomic ops here.
//
inline void
standard_handler::incref()
{
	lock();
	nref++;
	ASSERT(nref != 0);
	unlock();
}

//
// standard_handler_server methods
//

inline unref_task_type_t
standard_handler_server::unref_task_type()
{
	return (unref_task::HANDLER);
}

//
// standard_handler_client methods
//

inline
standard_handler_client::standard_handler_client(Xdoor *xdp) :
	standard_handler(xdp),
	proxy_obj_(CORBA::Object::_nil())
{
}

inline
standard_handler_client::~standard_handler_client()
{
}

inline void
standard_handler_client::incpxref(CORBA::Object_ptr o)
{
	lock();
	o->_this_component_ptr()->increase();
	unlock();
}

inline uint_t
standard_handler_client::decpxref(CORBA::Object_ptr o)
{
	return (o->_this_component_ptr()->decrease());
}

#endif	/* _STANDARD_HANDLER_IN_H */
