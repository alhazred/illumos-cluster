/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  fault_flowcontrol.h
 *
 */

#ifndef _FAULT_FLOWCONTROL_H
#define	_FAULT_FLOWCONTROL_H

#pragma ident	"@(#)fault_flowcontrol.h	1.16	08/05/20 SMI"

//
// Used by src/orbtest/flowcontrol_fi tests. Refer to
// src/fault_numbers/faultnum_flowcontrol.h
//

#if defined(_FAULT_INJECTION)

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/flow/resource_balancer.h>

//
// fault_flowcontrol class which provides some static methods for flowcontrol_fi
//

class fault_flowcontrol {
public:

	//
	// This is used to identify which operation to take when a certain
	// faultpoint is set. The generic_test uses these to process the
	// fault point.
	//
	enum op_t {
		RESOURCE_ALLOCATOR_1 = 0,
		CHECK_FOR_RESOURCE_CHANGES_1 = 1,
		PROCESS_REQUEST_RESOURCES_1 = 3,
		SIGNAL_STOP = 4,
		SIGNAL_RELEASE = 5,
		SIGNAL_BOTH = 6,
		PROCESS_MESSAGE = 7,
		LOOK_FOR_THREAD = 8
	};

	// Argument for flowcontrol_fi trigger
	struct flowcontrol_arg_t {
		// Which operation to take
		op_t			op;

		// Values for changing the resource pool allocations
		int32_t			cv_num;
		os::threadid_t	threadid;
	};

	//
	// Generic test function which calls another test function based on
	// the operation..
	//
	static void	generic_test_op(uint_t fault_num, void *fault_argp,
			    uint32_t argsize);

private:
	// Disallow making instances of this class.
	fault_flowcontrol();
	~fault_flowcontrol();

	//
	// The following functions are real test functions that will force the
	// test flowcontrol to go to the certain path. They are called by
	// above function.
	//

	// resource_pool::resource_allocator - request when resources free
	static void flowcontrol_resource_allocator_1(flowcontrol_arg_t *argp);

	// process_resource_request - new node asking for resources
	static void flowcontrol_process_request_resources_1();

	// check_for_resource_changes_1 - have threads to return to server
	static void flowcontrol_check_for_resource_changes_1();

	// trigger the FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE fault point
	static void flowcontrol_signal_release(flowcontrol_arg_t *argp);

	// trigger the FAULTNUM_FLOWCONTROL_SIGNAL_STOP fault point
	static void flowcontrol_signal_stop(flowcontrol_arg_t *argp);

	// grab the current threadid and set up look_for_thread trigger
	static void flowcontrol_process_message(flowcontrol_arg_t *);

	// compare current threadid to argp's and signal release if match
	static void flowcontrol_look_for_thread(flowcontrol_arg_t *argp);
};

#endif // _FAULT_INJECTION

#endif	/* _FAULT_FLOWCONTROL_H */
