/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _FAULT_INJECTION_IN_H
#define	_FAULT_INJECTION_IN_H

#pragma ident	"@(#)fault_injection_in.h	1.24	08/05/20 SMI"


//
// Class FaultTriggers
//

inline
FaultTriggers::FaultTriggers() : _hashtab(_hash_table_size)
{
	_modified = false;
	_total_argsize = 0;
}


//
// Class FaultTriggers::trigger_t
//

inline
FaultTriggers::trigger_t::trigger_t(uint32_t flt_num, void *flt_argp,
					uint32_t flt_argsize)
{
	_fault_num = flt_num;
	_fault_argp = (uchar_t *)flt_argp;
	_fault_argsize = flt_argsize;
}

inline
FaultTriggers::trigger_t::~trigger_t()
{
	delete [] _fault_argp;
}

inline uint32_t
FaultTriggers::trigger_t::fault_num()
{
	return (_fault_num);
}

inline void *
FaultTriggers::trigger_t::fault_arg()
{
	return ((void *) _fault_argp);
}

inline uint32_t
FaultTriggers::trigger_t::fault_argsize()
{
	return (_fault_argsize);
}



//
// Class InvoTriggers
//

inline
InvoTriggers::InvoTriggers(InvoTriggers *trigp) :
    next(trigp)
{
}

// static
inline void
InvoTriggers::put(sendstream *se)
{
	put(se->get_MainBuf());
}

// static
inline void
InvoTriggers::put(recstream *re)
{
	put(re->get_MainBuf());
}

// static
inline void
InvoTriggers::put(MarshalStream &mstream)
{
	put_trigger_info(mstream, the());
}

// static
inline void
InvoTriggers::get(sendstream *se)
{
	get(se->get_MainBuf());
}

// static
inline void
InvoTriggers::get(recstream *re)
{
	get(re->get_MainBuf());
}

inline void
InvoTriggers::get_trigger_info(recstream *re)
{
	get_trigger_info(re->get_MainBuf());
}

inline uint32_t
InvoTriggers::header_size()
{
	return ((uint32_t)sizeof (fi_header_t));
}

inline void
InvoTriggers::reserve_resources(resources *resourcep, InvoTriggers *trigp)
{
	resourcep->add_send_header(sizeof (fi_header_t));
	resourcep->add_send_data(triggers_size(trigp));
}

inline
InvoTriggers::fi_header_t::fi_header_t(int num, uint32_t len) :
    num_triggers(num),
    data_len(len)
{
}

inline
InvoTriggers::fi_header_t::fi_header_t()
{
}

//
// Retrieve thread specific data (_tsd)
//
// static
inline InvoTriggers *
InvoTriggers::get_invo_trig_tsd()
{
	return ((InvoTriggers *)_tsd.get_tsd());
}

//
// Set thread specific data (_tsd)
//
// static
inline void
InvoTriggers::set_invo_trig_tsd(InvoTriggers *trigp)
{
	_tsd.set_tsd((uintptr_t)trigp);
}

//
// Class NodeTriggers
//
inline NodeTriggers &
NodeTriggers::the()
{
	return (_the_node_triggers);
}

#endif	/* _FAULT_INJECTION_IN_H */
