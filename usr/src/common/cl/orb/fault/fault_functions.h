/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_FUNCTIONS_H
#define	_FAULT_FUNCTIONS_H

#pragma ident	"@(#)fault_functions.h	1.54	09/03/06 SMI"

#if defined(_FAULT_INJECTION)

#include <h/replica.h>
#include <orb/invo/common.h>
#include <h/mc_sema.h>

#define	MAX_EVENT_ORDER_SETS	10

class recstream;

extern "C" void fi_cmm_reboot(uint_t, void *, uint32_t);

//
// Repository of common fault functions (called from fault points).
// Each fault point must be of type:
//
//	static void func(uint32_t, void *, uint32_t);
//
// The 1st parameter will be passed the fault number that triggers
// the fault point.
//
// The 2nd parameter will be a pointer to the fault argument; it's
// up to the function on how to interpret the data pointed to by the
// pointer.  It's recommended that the expected fault argument and its
// data type be documented with each fault function.
//
// The 3rd parameter will be the size, in bytes, of the fault argument.
//
class FaultFunctions {
public:
	//
	// The following provides counting feature to fault functions to
	// give control of when an operation should be performed.  For example,
	// you could specify that the operation for a fault point should be
	// performed on the 3rd time the fault point is triggered, or after
	// it's been triggered five times, or the first four times
	// it's triggered, etc.
	//
	// This structure is normally a member of another fault argument
	// structure, e.g. generic_arg_t.  Fault functions can use the routine
	// FaultFunctions::do_counting() to perform the counting.  It returns
	// true when the value of the "counter" member satisfies the conditions
	// set by the "when" and "count" members.
	//
	// counter_when_t enum (specifies when FaultFunctions::do_counting()
	// should return true -- thus when the calling fault function should
	// perform the operation):
	//
	//	ALWAYS	-- always return true, regardless of counter.
	//	UNTIL	-- return true UNTIL counter reaches count.
	//	AT	-- return true only when counter is AT count.
	//	AFTER	-- return true AFTER counter has reached count.
	//	EVERY	-- return true EVERYtime counter is divisible by count.
	//	ODD	-- return true when counter is odd.
	//	EVEN	-- return true when counter is even.
	//	RANDOM	-- return true at random time.
	//
	// counter_t structure members:
	//
	//	when	-- when FaultFunctions::do_counting() should return
	//		   true.
	//		   Default: ALWAYS.
	//	count	-- value of "counter" is compared against this value.
	//	counter	-- counter modified by FaultFunctions::do_counting().
	//
	// Note: When 'when' is set to RANDOM, the value of 'count' is taken
	// to be the mean-time-between-failure value.  Furthermore, since
	// 'counter' will be used for storing the current random value, it
	// should be initialized to a random (seed) value, e.g. by calling
	// os::gethrtime().
	//
	// Note: FaultFunctions::do_counting() modifies the structure's member
	// "counter" member directly, i.e. the fault argument itself.  For an
	// Invo Trigger, since the fault argument is "carried" with the
	// invocation, the counter applies to all fault points with the
	// same fault number along the path of the invocation, even when
	// the fault points are executed on different nodes (or different
	// address spaces).  Furthermore, multiple invocations will also
	// share the same counter as long as the same trigger is used.
	//
	// For a Node Trigger, each node will have a separate copy of the
	// fault argument.  The counter is not shared across nodes.
	//
	enum counter_when_t {
			ALWAYS,
			UNTIL,
			AT,
			AFTER,
			EVERY,
			ODD,
			EVEN,
			RANDOM
	};

	struct counter_t {
		counter_t() : when(ALWAYS), count(0), counter(0) {}

		counter_t(counter_when_t w, uint32_t c)
		    : when(w), count(c),
		    counter((w == RANDOM) ? (uint32_t)os::gethrtime() : 0) {}

		enum counter_when_t	when;
		uint32_t		count;
		uint32_t		counter;
	};

	//
	// Fault argument type for the generic() fault function.
	//
	// Fault function generic() provides a counting feature which
	// allows control of when a specified operation should be performed.
	//
	// Members:
	//
	//	op		-- specifies the operation (a fault function)
	//			   to be performed.
	//	counter		-- used for counting.  See counter_t.
	//	data_length	-- size, in bytes, of the following arbitrary
	//			   data.  The data and the size will be passed
	//			   to the operation as its fault argument and
	//			   fault argument size, respectively.
	//	data		-- overlayed with arbitrary data which will
	//			   passed to the operation as its fault
	//			   argument.
	//
	enum generic_op_t {
		PANIC,
		SCQ_WAIT,
		SIGNAL,
		REBOOT,
		REBOOT_ONCE,
		REBOOT_SPECIFIED,
		REBOOT_DIFFERENT,
		CMM_REBOOT,
		HA_DEP_REBOOT,
		HA_DEP_SIGNAL,
		SERVICE_REBOOT,
		HA_DEP_SEMA_V,
		SLEEP,
		PXFS_SLEEP,
		SLEEP_ONCE,
		SEMA_P,
		SEMA_P_ONCE,
		SEMA_P_SPECIFIED,
		SEMA_V,
		SEMA_V_ONCE,
		SEMA_V_SPECIFIED,
		PXFS_RESOLVE,
		REBOOT_HA_RM,
		SWITCHOVER,
		SEND_HELD_TRINFO_ACK,
		SEMA_P_WITH_SHARED_DATA,
		SEMA_V_WITH_SHARED_DATA,
		CHECK_EVENT_ORDER
	};
	struct generic_arg_t {
		enum generic_op_t	op;
		uint32_t		data_length;
		counter_t		counter;
		char			data[1];
	};

	//
	// Events for which fault functions such as reboot() and hang_thread()
	// must wait before returning.
	//
	enum wait_for_op_t {
		NO_WAIT,
		WAIT_FOR_UNKNOWN,	// wait until node is marked unknown
		WAIT_FOR_DEAD,		// wait until node is marked dead
		WAIT_FOR_REJOIN,	// wait until node rejoins
		WAIT_FOR_LOCAL_SIGNAL	// wait for local signal
	};

	//
	// Fault argument type for fault functions such as reboot() and
	// hang_thread().  Members:
	//
	//	op	-- event to wait for before returning.
	//	nodeid	-- misc uses.  The fault function reboot(), for
	//		   instance, uses it as the target of reboot.
	//
	struct wait_for_arg_t {
		enum wait_for_op_t	op;
		nodeid_t		nodeid;
	};

	//
	// Fault argument type for fault function cmm_reboot().  Members:
	//
	//	step	-- the CMM step at which to reboot the node.
	//	op	-- event to wait for before returning.
	//	nodeid	-- the node to be rebooted (0 means current node).
	//
	struct cmm_reboot_arg_t {
		uint32_t		step;
		enum wait_for_op_t	op;
		nodeid_t		nodeid;
	};

	//
	// Fault argument type for fault function
	// used in 'hearsay' test cases
	// Members:
	//
	//	nodeB	--	node B who we should wait for
	//			to hear from
	//	nodeC	--	node C which is rebooted
	//			and whose status node B is reporting
	//			to local node A
	//
	struct delay_for_hearsay_arg_t {
		nodeid_t	nodeB;
		nodeid_t	nodeC;
	};

	//
	// Fault argument type for fault functions
	// that test event ordering.
	// Members :
	//	index_into_event_order_sets --
	//		There can be multiple independent sets of events that
	//		the fault subsystem tracks at this time.
	//		Note that each fault point is considered one event.
	//		This index identifies the set of events to which
	//		this event belongs : event_order_sets[index]
	//		will be used to test the ordering of that particular
	//		set of events.
	//
	//	order_of_event --
	//		This value specifies the numerical order of this event
	//		relevant to the set of events to which this fault point
	//		belongs.
	//
	struct event_order_arg_t {
		int index_into_event_order_sets;
		int order_of_event;
	};

	//
	// Fault argument type for fault function ha_dep_reboot().  Members:
	//
	//	sid	-- the service id to fault.
	//	op	-- event to wait for before returning.
	//	nodeid	-- the node to be rebooted.
	//
	// Note, these reboots depend on the service id saved in
	// thread-specific data.
	//
	struct ha_dep_reboot_arg_t {
		replica::service_num_t	sid;
		enum wait_for_op_t	op;
		nodeid_t		nodeid;
	};

	//
	// Fault argument type for fault function service_reboot().  Members:
	//
	//	sid	-- the service id to fault.
	//	op	-- event to wait for before returning.
	//	nodeid  -- the node to be rebooted.
	//
	// Note, this struct defined the same as struct ha_dep_reboot_arg_t
	// above. It is repeatedly defined here for clarification. Also,
	// these reboots will only be triggered depending on the service id.
	//
	struct svc_reboot_arg_t {
		replica::service_num_t  sid;
		enum wait_for_op_t	op;
		nodeid_t		nodeid;
	};

	//
	// Support for ha_rm_reboots. They depend on string based infromation
	// used in places like rm_repl_service::iterate_over
	//
	// sid		- service id
	// op		- type of wait
	// nodeid	- node on which primary RM exists
	// fn_name	- string info

#define	MAX_FN_NAME_LEN	256

	typedef struct {
		replica::service_num_t	sid;
		enum wait_for_op_t	op;
		nodeid_t		nodeid;
		char			fn_name[MAX_FN_NAME_LEN];
	} ha_rm_reboot_arg_t;

	//
	// Fault argument for mc_sema-related fault functions such as sema_p()
	// and sema_v().  Members:
	//
	//	id	-- the cookie for the mc_sema object.
	//	nodeid	-- misc. uses.  Usually used by the *_specified()
	//		   fault functions.
	//	wait	-- specifies event to wait for before returning.
	//
	struct mc_sema_arg_t {
		mc_sema::cookie	id;
		nodeid_t	nodeid;
		wait_for_arg_t	wait;
	};

	//
	// Support for ha_dep mc_sema objects
	//
	// sid			- service id
	// id			- the cookie for the mc_sema object
	// nodeid		- Misc uses
	// wait_for_arg_t	- wait type
	//
	typedef struct {
		replica::service_num_t	sid;
		mc_sema_arg_t		mc_sema_arg;
	} ha_dep_mc_sema_arg_t;

	//
	// Support for mc_sema_with_data_arg_t actions.
	// This is similar to a mc_sema action, except that
	// there is additional storage for the fault operation data.
	// This structure also has a field to specify
	// whether the trigger should obey once-only semantics.
	//
	// mc_sema_arg		-	mc_sema arg object
	// trigger_once		-	whether to obey once-only semantics
	// shared_data_len	-	size of the shared data stored
	//				in this struct
	// shared_data		-	fault operation shared data stored
	//
	// The usage of shared_data is like this :
	// - while populating a mc_sema_with_data_arg_t structure,
	// memory is allocated for the entire length
	// { sizeof (mc_sema_arg_t) + sizeof (trigger_once) +
	// sizeof (shared_data_len) + length of shared data }.
	// The data to store as shared_data is copied byte-by-byte
	// starting at shared_data offset in the struct.
	//
	// trigger_once really serves as a boolean value.
	// But to avoid padding mess, we use a 4-byte space for it instead.
	//
	typedef struct {
		mc_sema_arg_t		mc_sema_arg;
		uint32_t		trigger_once;
		uint32_t		shared_data_len;
		char			shared_data[1];
	} mc_sema_with_data_arg_t;

	//
	// Fault argument for fault functions such as set_sys_exception()
	// which set SystemExceptions.
	// Members:
	//
	//	_major		-- the exception major number.
	//	_minor		-- the exception minor number.
	//	completed	-- the exception completion status.
	//	counter		-- used for counting.  See counter_t.
	//	verbose		-- fault functions might use this to decide
	//			   whether to print warning message.
	//
	struct sys_exception_arg_t {
		sys_exception_arg_t() : verbose(true) {}

		uint_t			_major;
		uint_t			_minor;
		CORBA::CompletionStatus	completed;
		counter_t		counter;
		bool			verbose;
	};

	//
	// Fault argument type for fault function switchover().
	// Members:
	//	nodeid	 -- Node on which switchover needs to be performed
	//	svc_info -- This has 2 parts : First part (string) representing
	//		    the replicated 'service name' and a second part
	//		    (string) representing the 'provider name'
	//		    (which will become the new primary for the
	//		    given service after switchover completes).
	//
	struct switchover_arg_t {
		nodeid_t	nodeid;
		char		svc_info[1];
	};

	//
	// Argument for fault function failpath().
	//
	// Note, the fault function failpath(), the "failpath" utility
	// and this structure do not use the Fault Injection Framework.
	// This structure is placed here for convenience.
	//
	struct failpath_t {
		failpath_t() : force(false) {}

		int32_t 	local_adapterid;
		nodeid_t	remote_nodeid;

		//
		// Set to true to fail the path even if it's
		// the only one left to the remote node.
		//
		bool		force;
	};


	// --------------------------------------------------------------------
	// Support and convenience routines for the generic() fault function.
	// --------------------------------------------------------------------

	//
	// Convenience routine to allocate a generic_arg_t structure and append
	// an arbitrary fault argument to it.
	//
	// Fault function generic() provides a counting feature which
	// allows control of when a specified operation should be performed.
	// By default generic() always performs the specified operation.
	//
	// Arguments:
	//	total_size	-- total size allocated is returned in this arg.
	//	op		-- the operation to perform.
	// 	fault_argp	-- points to the fault argument to be appended
	//			   to the generic_arg_t structure.
	//	fault_argsize	-- size, in bytes, of the fault argument.
	//	when		-- see comments for counter_when_t.
	//			   Default: ALWAYS
	//	count		-- see comments for counter_t.
	//			   Default: 0
	//	alloc_type	-- memory allocation type.
	//			   Default: os::SLEEP
	//
	// Returns:
	//	A pointer to the allocated memory.
	//
	// NOTE: caller is resposible for deleting the allocated memory.
	//
	static generic_arg_t *generic_arg_alloc(
				uint32_t &		total_size,
				enum generic_op_t	op,
				void *			fault_argp,
				uint32_t		fault_argsize,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				os::mem_alloc_type	alloc_type = os::SLEEP);

	//
	// Convenience routine to allocate a switchover_arg_t structure and
	// append an arbitrary fault argument to it.
	//
	// Arguments:
	//	uint32_t	-- size of the structure, this is passed back
	//	nodeid_t	-- node id to run switchover from
	//	service_name	-- service to switchover
	//	prov_name	-- provider to switchover to
	//
	// Returns:
	//	A pointer to the allocated memory.
	//
	// NOTE: caller is resposible for deleting the allocated memory.
	//
	static switchover_arg_t *switchover_arg_alloc(
				uint32_t &		total_size,
				nodeid_t		nodeid,
				char 			*service_name,
				char 			*prov_name);

	//
	// Convenience routine to allocate a mc_sema_with_data_arg_t structure
	// and append an arbitrary fault argument to it.
	//
	// Arguments:
	//	total_size	--	size of the structure,
	//				this is passed back
	//	sema_arg	--	mc_sema_arg_t structure with
	//				its values filled in
	//	once		--	whether to obey once-only semantics
	//	data_len	--	total length (in bytes) of shared data
	//	data		--	pointer to starting address of
	//				shared data. This routine copies in
	//				data_len bytes of data from
	//				'data' address and appends it into
	//				the mc_sema_with_data_arg_t structure
	//
	// Returns:
	//	A pointer to the allocated memory.
	//
	// NOTE: caller is responsible for deleting the allocated memory.
	//
	static mc_sema_with_data_arg_t *mc_sema_with_data_arg_alloc(
				uint32_t &		total_size,
				mc_sema_arg_t		sema_arg,
				bool			once,
				uint32_t		data_len,
				void			*data);

	//
	// Convenience routine to get to any fault operation data
	// stored in a generic_arg_t structure
	//
	// Arguments:
	//	generic_argsp	--	pointer to generic_arg_t structure
	//	datap		--	this routine changes this pointer
	//				to point at the data stored
	//				in the generic_arg_t structure
	//	data_len	--	this routine sets this value
	//				to the length of the data stored
	//				in the generic_arg_t structure
	//
	static void get_fault_data_from_generic_arg(
				void			*generic_argsp,
				void			**datap,
				uint32_t		&data_len);

	//
	// A convenience routine for adding an Invo Trigger whose fault point
	// calls the generic() fault function.
	//
	// Fault function generic() provides a counting feature which
	// allows control of when a specified operation should be performed.
	// By default generic() always performs the specified operation.
	//
	// Arguments:
	//	op		-- specifies operation (fault function) to be
	//			   performed by generic().
	//	fault_num	-- fault number to add.
	//	fault_argp	-- pointer to the fault argument.  To be passed
	//			   to the operation as fault argument.
	//	fault_argsize	-- size, in bytes, of the fault argument.
	//	when		-- see comments for counter_when_t.
	//			   Default: ALWAYS
	//	count		-- see comments for counter_t.
	//			   Default: 0
	//	alloc_type	-- memory allocation type.
	//			   Default: os::SLEEP
	//
	static void	invo_trigger_add(
				enum generic_op_t	op,
				uint_t			fault_num,
				void *			fault_argp,
				uint32_t		fault_argsize,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				os::mem_alloc_type	alloc_type = os::SLEEP);

	//
	// A convenience routine for adding a Node Trigger whose fault point
	// calls the generic() fault function.
	//
	// Arguments:
	//	op		-- specifies operation (fault function) to be
	//			   performed by generic().
	//	fault_num	-- fault number to add.
	//	fault_argp	-- pointer to the fault argument.  To be passed
	//			   to the operation as fault argument.
	//	fault_argsize	-- size, in bytes, of the fault argument.
	//	nodeid		-- the node(s) to add the Node Trigger to.
	//			   Valid values are:
	//
	//				1) A node ID
	//				2) TRIGGER_THIS_NODE
	//				3) TRIGGER_ALL_NODES
	//
	//	when		-- see comments for counter_when_t.
	//			   Default: ALWAYS
	//	count		-- see comments for counter_t.
	//			   Default: 0
	//	alloc_type	-- memory allocation type.
	//			   Default: os::SLEEP
	//
	static void	node_trigger_add(
				enum generic_op_t	op,
				uint_t			fault_num,
				void *			fault_argp,
				uint32_t		fault_argsize,
				nodeid_t		nodeid,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				os::mem_alloc_type	alloc_type = os::SLEEP);

	//
	// A convenience routine for adding a Boot Trigger whose fault point
	// calls the generic() fault function.
	//
	// Arguments:
	//	op		-- specifies operation (fault function) to be
	//			   performed by generic().
	//	fault_num	-- fault number to add.
	//	fault_argp	-- pointer to the fault argument.  To be passed
	//			   to the operation as fault argument.
	//	fault_argsize	-- size, in bytes, of the fault argument.
	//	nodeid		-- the node(s) to add the Boot Trigger to.
	//			   Valid values are:
	//
	//				1) A node ID
	//				2) TRIGGER_THIS_NODE
	//				3) TRIGGER_ALL_NODES
	//
	//	when		-- see comments for counter_when_t.
	//			   Default: ALWAYS
	//	count		-- see comments for counter_t.
	//			   Default: 0
	//	alloc_type	-- memory allocation type.
	//			   Default: os::SLEEP
	//
	static void	boot_trigger_add(
				enum generic_op_t	op,
				uint_t			fault_num,
				void *			fault_argp,
				uint32_t		fault_argsize,
				nodeid_t		nodeid,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				os::mem_alloc_type	alloc_type = os::SLEEP);

	//
	// Convenience routines for adding an Invo/Node/Boot Trigger whose
	// fault point calls fault functions which set SystemExceptions.
	//
	// Arguments:
	//	fault_num	-- fault number to add.
	//	sys_except	-- SystemException to be set by the fault pt.
	//	nodeid		-- the node(s) to add the Node/Boot Trigger to.
	//			   Valid values are:
	//
	//				1) A node ID
	//				2) TRIGGER_THIS_NODE
	//				3) TRIGGER_ALL_NODES
	//
	//	when		-- see comments for counter_when_t.
	//			   Default: ALWAYS
	//	count		-- see comments for counter_t.
	//			   Default: 0
	//	verbose		-- fault functions might use this to decide
	//			   whether to print warning message.
	//			   Default: true
	//	alloc_type	-- memory allocation type.
	//			   Default: os::SLEEP
	//
	// Usage Example:
	//	FaultFunctions::invo_trigger_add(
	//		FAULTNUM_ORB_SET_COMM_FAILURE,
	//		CORBA::COMM_FAILURE(3, CORBA::COMPLETED_MAYBE),
	//		FaultFunctions::EVERY, 3);
	//
	static void	invo_trigger_add(
				uint_t			fault_num,
				CORBA::SystemException	&sys_except,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				bool			verbose = true,
				os::mem_alloc_type	alloc_type = os::SLEEP);

	static void	node_trigger_add(
				uint_t			fault_num,
				CORBA::SystemException	&sys_except,
				nodeid_t		nodeid,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				bool			verbose = true,
				os::mem_alloc_type	alloc_type = os::SLEEP);

	static void	boot_trigger_add(
				uint_t			fault_num,
				CORBA::SystemException	&sys_except,
				nodeid_t		nodeid,
				enum counter_when_t	when = ALWAYS,
				uint32_t		count = 0,
				bool			verbose = true,
				os::mem_alloc_type	alloc_type = os::SLEEP);


	// --------------------------------------------------------------------
	// Fault functions.
	// --------------------------------------------------------------------

	//
	// Generic fault function.  Calls another fault function based on
	// the operation (enum generic_op_t) specified in the fault argument.
	//
	// Fault function generic() provides a counting feature which
	// allows control of when a specified operation should be performed.
	// For example, you can specify that the operation for a fault point
	// should be performed on the 3rd time the fault point is triggered,
	// or after it's been triggered five times, or the first four times
	// it's triggered.  See comments for counter_t for more info.
	//
	// By default generic() always performs the specified operation.
	//
	// Fault argument:
	//	A generic_arg_t struct.
	//
	static void	generic(uint_t, void *, uint32_t);

	//
	// Panic a node/kernel (current or remote).
	// Fault argument:
	//	If none, then the current node is assumed.
	//	Otherwise, the node ID of the node to panic.
	// WARNING:
	//	If called from user-land process, this will cause the current
	//	or remote KERNEL to panic, not the process itself.
	//
	static void	panic(uint_t, void *, uint32_t);

	// Called when a node panic message is received.
	static void	panic_rpc_handler(recstream *);

	//
	// Block a thread
	//
	// The wait/signal pair of fault functions must be used together.
	// These fault functions can be used to sync two or more threads
	// together in a very primitive fashion.  (Both threads must in
	// kernel space)
	//
	static void	wait(uint_t, void *, uint32_t);
	static void	signal(uint_t, void *, uint32_t);

	//
	// HA Dependency version of the signal function
	// This function will check the value of the TSD and compare it to
	// the passed in value (they are the sid's).. if they match.. we
	// signal()
	//
	static void	ha_dep_signal(uint_t, void *, uint32_t);

	//
	// Reboot a node
	//
	// This faultfunction is used to reboot a node
	// or to halt it if its a unode process. The halting
	// is done via a raise(SIGINT) to kill the current
	// process.  (This is done to avoid dumping core)
	// Fault Argument:
	//	A wait_for_arg_t struct that specifies the nodeid to reboot
	//	and the wait type.  Nodeid 0 means reboot the current node.
	//
	static void	reboot(uint_t, void *, uint32_t);

	//
	// Reboot a node once
	//
	// This faultfunction implements only-once semantics, by turning off
	// the fault point before calling reboot.  (It will turn the fault
	// off in the Invo triggers and Node triggers)
	// Fault Argument:
	//	A wait_for_arg_t struct that specifies the nodeid to reboot
	//	and the wait type.  Nodeid 0 means reboot the current node.
	//
	static void	reboot_once(uint_t, void *, uint32_t);

	//
	// Reboot a specific node
	//
	// This fault function will verify that the current node has the same
	// node id as the one specified in the fault arguments before rebooting
	// it.  In other words, this function only allows a node to reboot
	// itself.
	// Fault Argument:
	//	A wait_for_arg_t struct that specifies the nodeid to reboot
	//	and the wait type.  Nodeid 0 means always reboot the current
	//	node.
	//
	// NOTE: This is different from just plain reboot, because some faults
	// specially in HA fault-injection testing will be triggered several
	// times during a retry.  In this case we dont want the retries to keep
	// on attempting to reboot a specific node.  (Which plain reboot would
	// do).
	//
	static void	reboot_specified(uint_t, void *, uint32_t);

	// Reboot the HA RM
	static void	reboot_ha_rm(uint_t, void *, uint32_t);

	//
	// Reboot a specific node only if different from current node
	//
	// This fault function is the same as reboot_specified() above, except
	// that a reboot is only performed if the specified node to be rebooted
	// is DIFFERENT from the current node.  In other words, this function
	// only allows rebooting another node (unless nodeid 0 is specified).
	// Fault Argument:
	//	A wait_for_arg_t struct that specifies the nodeid to reboot
	//	and the wait type.  Nodeid 0 means always reboot the current
	//	node.
	//
	// NOTE: This is useful for fault points that are located in both the
	// request and reply paths, and we want to reboot only the other node.
	// E.g. a client on node A makes a request to the server on node B, and
	// we want the client to reboot the server only when it gets the
	// reply from the server.  Since the fault point is located on both
	// request and reply paths, we don't want the server to reboot itself
	// when it's servicing the request.
	//
	static void	reboot_different(uint_t, void *, uint32_t);

#if defined(_KERNEL_ORB)
	//
	// CMM reboot
	//
	// This fault point will reboot a remote / local node when the
	// step of the local node matches the step specified in the fault
	// arguments
	// Fault Argument:
	//	A cmm_reboot_arg_t structure that specifies the node to reboot
	//	and the step to do it in.  (and a wait type)
	//
	static void	cmm_reboot(uint_t, void *, uint32_t);
#endif // _KERNEL_ORB

	//
	// HA dependecy reboot
	//
	// This fault point will reboot a remote / local node when the
	// sid specified matches the stored sid in tsd.
	// (This is a ONLY_ONCE semantics fault function)
	// Fault Argument
	//	A ha_dep_reboot_arg_t structure that specifies the sid, nodeid,
	// 	and wait op.
	//
	static void	ha_dep_reboot(uint_t, void *, uint32_t);

	//
	// Service reboot
	//
	// This fault point will reboot a remote / local node with the
	// specified sid.
	// (This is a ONLY_ONCE semantics fault function)
	// Fault Argument
	//	A svc_reboot_arg_t structure that specifies the sid, nodeid,
	//	and wait op.
	//
	static void	service_reboot(uint_t, void *, uint32_t);

	//
	// Reboot all nodes
	//
	// This function will reboot the complete cluster
	// It will examine the current membership, and begin to reboot each
	// node. (With the current node last)
	//
	static void	reboot_all_nodes();

	// Called when a node reboot message is received
	static void	reboot_rpc_handler(recstream *);

	// Called when a node reboot clean message is received
	static void	reboot_clean_rpc_handler(recstream *);

#if defined(_KERNEL_ORB)
	// Called by reboot_*_rpc_handler
	// It will do the gruntwork for reboot
	// Argument
	//	clean - do a clean reboot ?
	static void	_do_reboot(bool clean);
#endif

	//
	// do a sema_p on a mc_sema object
	//
	// This fault function will do a sema_p operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object.
	//
	static void	sema_p(uint_t, void *, uint32_t);

	//
	// do a sema_p on a mc_sema object
	//
	// This fault function will do a sema_p operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object.  (implements ONLY ONCE semantics)
	//
	static void	sema_p_once(uint_t, void *, uint32_t);

	//
	// This is an internal routine for use by FaultFunctions methods
	// that need to do sema_p_once type of semantics.
	// This routine takes an additional argument nodeid_to_clear
	// that specifies the nodeid from which the triggers have to be cleared
	// to obey the once-only semantics.
	//
	static void	sema_p_once_internal(uint_t, void *, uint32_t, int);

	//
	// do a sema_p on a mc_sema object
	//
	// This fault function will do a sema_p operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object.
	//
	// This functions will only do the sema_p operation if the nodeid passed
	// in the fault argument matches the current nodeid, or if its 0.
	//
	static void	sema_p_specified(uint_t, void *, uint32_t);

	//
	// do a sema_v on a mc_sema object
	//
	// This fault function will do a sema_v operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object.
	//
	static void	sema_v(uint_t, void *, uint32_t);

	//
	// do a sema_v on a mc_sema object
	//
	// This fault function will do a sema_v operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object. (Implements ONLY ONCE semantics)
	//
	static void	sema_v_once(uint_t, void *, uint32_t);

	//
	// This is an internal routine for use by FaultFunctions methods
	// that need to do sema_v_once type of semantics.
	// This routine takes an additional argument nodeid_to_clear
	// that specifies the nodeid from which the triggers have to be cleared
	// to obey the once-only semantics.
	//
	static void	sema_v_once_internal(uint_t, void *, uint32_t, int);

	//
	// do a sema_v on a mc_sema object
	//
	// This fault function will do a sema_v operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object.
	//
	// This functions will only do the sema_v operation if the nodeid passed
	// in the fault argument matches the current nodeid, or if its 0.
	//
	static void	sema_v_specified(uint_t, void *, uint32_t);

	//
	// do a sema_v on a mc_sema object (ha dep version)
	//
	// This fault function will do a sema_v operation on a mc_sema object
	// the fault_argument structure mc_sema_arg_t constains the cookie
	// for the mc_sema object.
	// Will verify that the thread specified data (sid) stored matches the
	// one specified on the fault arguments
	//
	static void	ha_dep_sema_v(uint_t, void *, uint32_t);

	//
	// do a sema_p or sema_p_once type of action on a mc_sema object,
	// with additional storage for fault operation data
	//
	// This fault function will do a sema_p or sema_p_once type of operation
	// on a mc_sema object. The fault argument structure contains the cookie
	// for the mc_sema object.
	//
	static void	sema_p_with_shared_data(uint_t, void *, uint32_t);

	//
	// do a sema_v or sema_v_once type of action on a mc_sema object,
	// with additional storage for fault operation data
	//
	// This fault function will do a sema_v or sema_v_once type of operation
	// on a mc_sema object. The fault argument structure contains the cookie
	// for the mc_sema object.
	//
	static void	sema_v_with_shared_data(uint_t, void *, uint32_t);

	//
	// sleep for time specified
	//
	// Fault args are of type os::usec_t
	//
	static void	sleep(uint_t, void *, uint32_t);

	//
	// bind a data container object to the nameserver and
	// sleep for time specified
	//
	// Fault args are of type os::usec_t
	//
	static void	pxfs_sleep(uint_t, void *, uint32_t);

	//
	// sleep for time specified
	//
	// Fault args are of type os::usec_t.  Implements ONLY ONCE semantics
	//
	static void	sleep_once(uint_t, void *, uint32_t);

	//
	// Set a system exception as specified in the fault argument.
	// Fault Argument:
	//	sys_exception_arg_t structure.
	// Note:
	//	This fault function can be called only from FAULT_POINT_ENV()
	//	or FAULTPT_ENV_*() macros.
	//
	static void	set_sys_exception(uint_t, Environment *,
			    void *, uint32_t);

	//
	// Given a local adapter ID and a remote node ID, fail the associated
	// pathend.  This is mainly for the "failpath" test utility.
	//
	// Note:
	//	This method, the "failpath" utility and the failpath_t
	//	structure do not use the Fault Injection Framework.
	//	This method is placed here for convenience.
	//
	// Returns:
	//	0		- successful.
	//	EAGAIN		- there is only one endpoint to the specified
	//			  remote node and the "force" field of the
	//			  failpath_t argument is set to false.
	//	EINVAL		- invalid remote node ID.
	//	ECANCELED	- the pathend is already down (returned by
	//			  path_manager::fault_revoke_pathend()).
	//	ENOENT		- the pathend doesn't exist (returned by
	//			  path_manager::fault_revoke_pathend()).
	//
	static int	failpath(failpath_t &);

	//
	// Check event order.
	// This fault-function is used to check if an event has occurred in
	// its expected numerical order in a set of events.
	// If the event has not occurred in its proper order,
	// then this function panics the node
	// signifying failure of the test case.
	// A unode-process will abort with a coredump.
	// Fault Argument:
	//	A event_order_arg_t struct that specifies the event order set
	//	to use in the event_order_sets array, and the order of
	//	a particular event to test,
	//
	static void	check_event_order(uint_t, void *, uint32_t);

	// --------------------------------------------------------------------
	// Support routines for fault function implementations.
	// --------------------------------------------------------------------

	//
	// Support for multiple wait_for_X types
	// Will do the setup for the particular wait according to the specified
	// wait_for opcode.
	//
	static void	do_setup_wait(wait_for_arg_t *);

	//
	// Support for multiple wait_for_X types
	// Will do the wait for the particular wait according to the specified
	// wait_for opcode.
	//
	static void	do_wait(wait_for_arg_t *);

#if defined(_KERNEL_ORB)
	//
	// setup_wait_for_unknown
	// Call before we wait for unknown to mark _unknown_signaled down,
	// and set who we're waiting for
	//
	// Arguments:
	//	The nodeid that we want to wait for to go unknown
	//
	static void	setup_wait_for_unknown(nodeid_t);

	//
	// setup_wait_for_multi_unknown
	// Call this to set up which node(s) to wait for unknown by
	// specifying the node id. It is necessary to call this each time
	// for each node to wait for.
	//
	// Arguments:
	//	The nodeid that we want to wait for to go unknown
	//
	static void	setup_wait_for_multi_unknown(nodeid_t);

	//
	// reset_wait_for_multi_unknown
	// Reset the bitmap on node(s) which is previously being waited
	// for unknown, if any. It is recommended to call this before
	// calling setup_wait_for_multi_unknown.
	//
	static void	reset_wait_for_multi_unknown();

	//
	// wait_for_unknown
	// Wait until cmm marks the node unknown
	//
	static void	wait_for_unknown();

	//
	// wait_for_multi_unknown
	// Wait until cmm marks all the node(s) unknown
	//
	static void	wait_for_multi_unknown();

	//
	// signal_unknown
	// entry for CMM to signal when a node has gone unknown
	//
	// Arguments:
	//	The id of the node that has gone unknown
	//
	static void	signal_unknown(nodeid_t);

	//
	// setup_wait_for_dead
	// Call before we wait for dead to mark _dead_signaled down,
	// and set who we're waiting for
	//
	// Arguments:
	//	The nodeid that we want to wait for to go dead
	//
	static void	setup_wait_for_dead(nodeid_t);

	//
	// setup_wait_for_multi_dead
	// Call this to set up which node(s) to wait for dead by
	// specifying the node id. It is necessary to call this each
	// time for each node to wait for.
	//
	// Arguments:
	//	The nodeid that we want to wait for to go dead
	//
	static void	setup_wait_for_multi_dead(nodeid_t);

	//
	// reset_wait_for_multi_dead
	// Reset the bitmap on node(s) which is previously being waited
	// for dead, if any. It is recommended to call this before
	// calling setup_wait_for_multi_dead.
	//
	static void	reset_wait_for_multi_dead();

	//
	// wait_for_dead
	// Wait until cmm marks the node dead
	//
	static void	wait_for_dead();

	//
	// wait_for_multi_dead
	// Wait until cmm marks all the node(s) dead
	//
	static void	wait_for_multi_dead();

	//
	// setup_wait_for_rejoin
	// Call before we wait for node rejoin to set who we're waiting for
	// (This implies that the node goes down and then come back up)
	//
	// Arguments:
	//	The nodeid we want to wait for to rejoin
	//
	static void	setup_wait_for_rejoin(nodeid_t);

	//
	// setup_wait_for_multi_rejoin
	// Call this to set up which node(s) to wait for rejoin by
	// specifying the node id. It is necessary to call this each
	// time for each node to wait for.
	//
	// Arguments:
	//	The nodeid that we want to wait for to go rejoin
	//
	static void	setup_wait_for_multi_rejoin(nodeid_t);

	//
	// reset_wait_for_multi_rejoin
	// Reset the bitmap on node(s) which is previously being waited
	// for rejoin, if any. It is recommended to call this before
	// calling setup_wait_for_multi_rejoin.
	//
	static void	reset_wait_for_multi_rejoin();

	//
	// wait_for_rejoin
	// Wait until cmm marks the node dead and then rejoin
	//
	static void	wait_for_rejoin();

	//
	// wait_for_multi_rejoin
	// Wait until cmm marks all the node(s) rejoin
	//
	static void	wait_for_multi_rejoin();

	//
	// signal_membership_change
	// Called from the CMM callback everytime that the membership changes
	// after we've set the new membership
	//
	// Arguments:
	//	None
	//
	static void	signal_membership_change();

	//
	// get_current_cmm_step
	// Returns the current cmm step from the state info from the
	// cmm control object
	// Returns: true if the cmm step can be obtained, false otherwise.
	//
	static bool	get_current_cmm_step(uint32_t &current_step);

#endif // _KERNEL_ORB

	//
	// reset_event_order_set
	// Reset a set (specified by index) in the event order sets to 0.
	// If index is -1, then reset all sets in the event order sets array
	// to 0.
	//
	static void	reset_event_order_set(int index);

	//
	// Helper routine to provide counting features to fault functions.
	// See comments for counter_t for more info.
	//
	// Returns:
	//	true when the value of the counter_t structure member
	//	"counter" satisfies the conditions of the "when" and "count"
	//	members; false otherwise.
	//
	// Note: this routine modifies the counter_t structure's member
	// "counter" member directly, i.e. the fault argument itself.  For an
	// Invo Trigger, since the fault argument is "carried" with the
	// invocation, the counter applies to all fault points with the
	// same fault number along the path of the invocation, even when
	// the fault points are executed on different nodes (or different
	// address spaces).  Furthermore, multiple invocations will also
	// share the same counter as long as the same trigger is used.
	//
	// For a Node Trigger, each node will have a separate copy of the
	// fault argument.  The counter is not shared across nodes.
	//
	static bool	do_counting(counter_t &);

	//
	// tsd_svc_id(replica::service_num_t)
	// Save the service id in thread specific data.  The key (tsd) is static
	// in the fault injection framework
	//
	// Arguments:
	//	the svc id to save in tsd
	//
	static void	tsd_svc_id(const replica::service_num_t);

	//
	// tsd_svc_id()
	// Get the svc id stored in TSD
	//
	// Arguments:
	//	None
	// Returns:
	//	the svc id stored (or 0 if none)
	//
	static replica::service_num_t tsd_svc_id();

	//
	// Front end to system call that will get the nodeid of the HA RM
	// host
	//
	static nodeid_t rm_primary_node();

	//
	// Switchover a HA service
	//
	// This faultfunction is used to switchover a HA service.
	// Fault Argument:
	//	A switchover_arg_t struct that specifies the nodeid on which
	//	to run the switchover command, replicated service name and
	//	the provider's name (this will be the new primary for this
	//	service after switchover is done)
	//
	static void	switchover(uint_t fault_num, void *fault_argp,
			    uint32_t fault_argsizep);

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
	//
	// This fault function is used to send a tr_info ack that was
	// held up by an earlier faultpoint FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK
	//

	static void	send_held_trinfo_ack(uint_t fault_num, void *fault_argp,
			    uint32_t fault_argsizep);

	// members to store nodeid and tr_info_id for send_held_trinfo_ack()
	static ID_node	dont_ack_node;
	static uint64_t	dont_ack_trinfo;

#endif
	//
	// This fault function is used to do generic operation on a
	// specific service
	//
	static void 	generic_this_svc(uint32_t fault_num, uint32_t svc_num,
			    void (*fault_fp)(uint32_t, void *, uint32_t));

private:
#if defined(_KERNEL_ORB)
	// Condition variable to wait on for fault functions (and lock)
	static os::condvar_t	_cv;
	static os::mutex_t	_lock;

	static void	lock();
	static void	unlock();

	// Support for wait_for_unknown
	static int		_unknown_signaled;
	static nodeid_t		_unknown_wf_nodeid;

	// Support for wait_for_multiple_unknown
	static uint64_t		wait_for_unknown_bitmap;
	static uint64_t		current_unknown_bitmap;

	// Support for wait_for_dead
	static int		_dead_signaled;
	static ID_node		_dead_wf_node;

	// Support for wait_for_multi_dead
	static uint64_t		wait_for_dead_bitmap;
	static uint64_t		current_dead_bitmap;
	static ID_node	  _dead_node[NODEID_MAX];

	// Support for wait_for_rejoin
	static int		_rejoin_signaled;
	static ID_node		_rejoin_wf_node;

	// Support for wait_for_multi_rejoin
	static uint64_t		wait_for_rejoin_bitmap;
	static uint64_t		current_rejoin_bitmap;
	static ID_node	  _rejoin_node[NODEID_MAX];

	// Support for wait / signal (operation pair)
	static int		_signaled;

	//
	// Event Order Tests
	// ------------------
	//
	// The engineer can implement test scenarios that check
	// whether fault points execute in a specified order.
	//
	// The engineer specifies a sequence of events, where each
	// event is a fault point. This is called an Event Order Set.
	//
	// When each of these fault points is armed,
	// the fault point identifies the Event Order Set
	// and the relative order within that set.
	// When any of these fault points triggers, the system
	// panics the node should the event occur out of order.
	//
	// The engineer can implement a test where multiple
	// independent sequences of events can occur.
	// There is no correlation between the events of
	// different Event Order Sets.
	//
	// Let's go through an example.
	//
	// Say there are three events A, B and C
	// (possibly occuring as part of multiple threads)
	// that should happen in order : A, then B and then C.
	// And say we want to have a test case to ensure
	// that these three events A, B and C happen in order.
	//
	// At the begininning of a test, the engineer resets
	// all of the counters for events.
	//
	//    FaultFunctions::reset_event_order_set(-1);
	//
	// The engineer adds a separate fault trigger for A,B,& C.
	//
	// Each fault point has an event_order_arg_t structure
	// as fault argument. The first parameter identifies
	// the event sequence to which this fault point belongs.
	// The first parameter is also the index into an array of
	// event counters "event_order_sets[]". The second argument
	// specifies the relative order of this fault point in this
	// sequence of events. Note that the first fault point is
	// number 1.
	//
	// In this case the event_order_arg_t values would be:
	//					A	B	C
	//					-	-	-
	// index_into_event_order_sets		0	0	0
	// order_of_event			1	2	3
	//
	// When the system executes the fault point, the system
	// increments the counter for the specified Event Order Set.
	// The system compares the counter value against the expected
	// value for this event, and panics the node when a mismatch
	// occurs, because the fault point was not executed in the
	// correct order.
	//
	// Any specific event can only occur once in a sequence of events,
	// and any specific event can only belong to one sequence of events.
	//
	// All of the events of a sequence must occur on one node.
	// There is no ordering between events on different nodes.
	// At this time the feature only supports events occurring in the
	// kernel (or unode equivalent).
	//
	// Fault points that do not belong to a specific Event Order Set
	// have no impact on the Event Order Set.
	//
	// Now let's look at two possible sequences of events:
	//
	// Good Sequence	counter value :		Expected
	//			event_order_sets[0]	order_of_event
	// -------------	-------------------	--------------
	// Initial value		0
	// Event A happens		1			1
	// Event B happens		2			2
	// Event C happens		3			3
	//
	// Bad Sequence		counter value :		Expected
	//			event_order_sets[0]	order_of_event
	// -------------	-------------------	--------------
	// Initial value		0
	// Event A happens		1			1
	// Event C happens		2			3
	// ** Node Panic due to the above mismatch (2 != 3) **
	//
	// In a single test case, we can have MAX_EVENT_ORDER_SETS
	// of event sets being tested for ordered occurrence
	// (we test order between events in a set, and not between sets).
	//
	// For example, another test could require
	// that events M,N,O,P must occur in order
	// and that events X,Y,Z must occur in order,
	// but that there is no relationship between the events
	// in different Event Order Sets.
	//
	static int	event_order_sets[MAX_EVENT_ORDER_SETS];

	//
	// Does the gruntwork for switchover
	// Arguments:
	//	service		- replicated service name
	//	provider	- New Primary
	//
	static void	_switchover_service(char *service, char *provider);

#endif	// _KERNEL_ORB

	// TSD for service id (for dependency testing)
	static os::tsd		_tsd_sid_key;

	// Disallow making instances of this class.
	FaultFunctions();
	~FaultFunctions();
};

#endif	// _FAULT_INJECTION

#endif	/* _FAULT_FUNCTIONS_H */
