/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fault_functions.cc	1.96	09/03/06 SMI"

#include <sys/os.h>
#include <sys/cladm_int.h>
#include <sys/clconf.h>
#include <sys/cl_comm_support.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/buffers/marshalstream.h>
#include <orb/fault/fault_injection.h>
#include <orb/fault/fault_functions.h>
#include <orb/fault/fault_rpc.h>
#include <errno.h>

#include <h/replica.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <nslib/naming_context_impl.h>
#include <nslib/data_container_impl.h>
#include <sys/mc_sema_impl.h>
#include <sys/rm_util.h>

#include <cmm/cmm_ns.h>

#if defined(_KERNEL_ORB)
#include <orb/member/members.h>
#include <repl/rmm/rmm.h>
#include <orb/transport/path_manager.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/xdoor/translate_mgr.h>
#endif

#if defined(_KERNEL)
#include <sys/uadmin.h>
#include <sys/vfs.h>
#include <sys/dumphdr.h>
#include <sys/callb.h>
#include <sys/sunddi.h>

extern ksema_t fsflush_sema;

#elif defined(_KERNEL_ORB)
#include <signal.h>
#include <stdio.h>
#include <unode/unode.h>
#endif

//
// class FaultFunctions
//
//	Repository of common fault functions (called from fault points).
//	Each fault point must be of type:
//
//		static void func(uint32_t, void *, uint32_t);
//
//	The 1st parameter will be passed the fault number that triggers
//	the fault point.
//	The 2nd parameter will be a pointer to the fault argument; it's
//	up to the function on how to interpret the data pointed to by the
//	pointer.  It's recommended that the expected fault argument and its
//	data type be documented with each fault function.
//	The 3rd parameter will be the size, in bytes, of the fault argument.
//

// Initialize static members

#if defined(_KERNEL_ORB)

// General Purpose cv and lock
os::mutex_t	FaultFunctions::_lock;
os::condvar_t	FaultFunctions::_cv;

// Wait for Unknown support
int		FaultFunctions::_unknown_signaled	= 0;
nodeid_t	FaultFunctions::_unknown_wf_nodeid	= 0;

// Wait for Multiple Unknown support
uint64_t	FaultFunctions::wait_for_unknown_bitmap	= 0;
uint64_t	FaultFunctions::current_unknown_bitmap	= 0;

// Wait for Dead support
int		FaultFunctions::_dead_signaled		= 0;
ID_node		FaultFunctions::_dead_wf_node;

// Wait for Multiple Dead support
uint64_t	FaultFunctions::wait_for_dead_bitmap	= 0;
uint64_t	FaultFunctions::current_dead_bitmap	= 0;
ID_node	 FaultFunctions::_dead_node[NODEID_MAX];

// Wait for Rejoin support
int		FaultFunctions::_rejoin_signaled	= 0;
ID_node		FaultFunctions::_rejoin_wf_node;

// Wait for Multiple Rejoin support
uint64_t	FaultFunctions::wait_for_rejoin_bitmap	= 0;
uint64_t	FaultFunctions::current_rejoin_bitmap	= 0;
ID_node	 FaultFunctions::_rejoin_node[NODEID_MAX];

// Wait Signal support
int		FaultFunctions::_signaled		= 0;

// Event Order support
int		FaultFunctions::event_order_sets[MAX_EVENT_ORDER_SETS] = {0};

#if defined(_KERNEL_ORB) && !defined(_KERNEL)

// Delay translate_info ack support
ID_node		FaultFunctions::dont_ack_node;
uint64_t	FaultFunctions::dont_ack_trinfo		= 0;

#endif

#endif // _KERNEL_ORB

// TSD for service id (for dependency testing)
os::tsd		FaultFunctions::_tsd_sid_key;

//
// Convenience routine to allocate a generic_arg_t structure and append
// an arbitrary fault argument to it.
//
// Fault function generic() provides a counting feature which
// allows control of when a specified operation should be performed.
// By default generic() always performs the specified operation.
//
// Arguments:
//	total_size	-- total size allocated is returned in this arg.
//	op		-- the operation to perform.
// 	fault_argp	-- points to the fault argument to be appended
//			   to the generic_arg_t structure.
//	fault_argsize	-- size, in bytes, of the fault argument.
//	when		-- see comments for counter_when_t.
//			   Default: ALWAYS
//	count		-- see comments for counter_t.
//			   Default: 0
//	alloc_type	-- memory allocation type.
//			   Default: os::SLEEP
//
// Returns:
//	A pointer to the allocated memory.
//
// NOTE: caller is resposible for deleting the allocated memory.
//
FaultFunctions::generic_arg_t *
FaultFunctions::generic_arg_alloc(
		uint32_t &		total_size,
		enum generic_op_t	op,
		void *			fault_argp,
		uint32_t		fault_argsize,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		os::mem_alloc_type)
{
	generic_arg_t	*argp;

	// Calculate how much memory to allocate
	total_size = (uint32_t)sizeof (generic_arg_t) + fault_argsize;

	// XXX - Need to allocate based on specified allocation type.
	argp = (generic_arg_t *)new char[total_size];

	// Fill in the generic_arg_t portion
	argp->op = op;
	argp->counter.when = when;
	argp->counter.count = count;
	argp->counter.counter =
		(when == RANDOM) ? (uint32_t)os::gethrtime() : 0;
	argp->data_length = fault_argsize;

	//
	// Copy the fault argument into the extra memory we allocated
	// at the end of the generic_arg_t structure
	//
	bcopy(fault_argp, argp->data, (size_t)fault_argsize);

	return (argp);
}

//
// Convenience routine to allocate a switchover_arg_t structure and
// append an arbitrary fault argument to it.
// Arguments:
// 	uint32_t	-- size of the structure, this is passed back
//	nodeid_t	-- node id to run switchover from
//	service_name	-- service to switchover
//	prov_name	-- provider to switchover to
//
// Returns:
//	A pointer to the allocated memory.
//
// NOTE: caller is resposible for deleting the allocated memory.
FaultFunctions::switchover_arg_t *
FaultFunctions::switchover_arg_alloc(
		uint32_t 		&total_size,
		nodeid_t		nodeid,
		char 			*service_name,
		char 			*prov_name)
{
	switchover_arg_t	*argp;
	uint32_t		servicelen = (uint32_t)os::strlen(service_name);
	uint32_t		provlen = (uint32_t)os::strlen(prov_name);

	// Calculate how much memory to allocate
	total_size = (uint32_t)sizeof (nodeid) + servicelen + 1 + provlen + 1;

	// Need to allocate based on specified allocation type
	argp = (switchover_arg_t *)new char[total_size];

	// Fill in the switchover_arg_t portion
	argp->nodeid = nodeid;

	(void) os::strcpy(argp->svc_info, service_name);
	(void) os::strcpy(argp->svc_info + servicelen + 1, prov_name);

	return (argp);
}

//
// Convenience routine to allocate a mc_sema_with_data_arg_t structure
// and append an arbitrary fault argument to it.
//
// Arguments:
//	total_size	--	size of the structure,
//				this is passed back
//	sema_arg	--	mc_sema_arg_t structure with
//				its values filled in
//	once		--	whether to obey trigger-once-only semantics
//	data_len	--	total length (in bytes) of shared data
//	data		--	pointer to starting address of shared data,
//				this routine copies in data_len bytes of data
//				from 'data' address and appends it into
//				the mc_sema_with_data_arg_t structure
//
// Returns:
//	A pointer to the allocated memory.
//
// NOTE: caller is responsible for deleting the allocated memory.
//
FaultFunctions::mc_sema_with_data_arg_t *
FaultFunctions::mc_sema_with_data_arg_alloc(
		uint32_t &		total_size,
		mc_sema_arg_t		sema_arg,
		bool			once,
		uint32_t		data_len,
		void			*data)
{
	mc_sema_with_data_arg_t *argp;

	uint32_t trig_once = (uint32_t)once;

	// Calculate how much memory to allocate
	total_size =
	    (uint32_t)sizeof (sema_arg) + (uint32_t)sizeof (trig_once) +
	    (uint32_t)sizeof (data_len) + data_len;

	// Need to allocate based on specified allocation type
	argp = (mc_sema_with_data_arg_t *)new char[total_size];

	// Structure-Copy the sema arg structure
	argp->mc_sema_arg = sema_arg;

	// Store the once-only trigger semantics flag
	argp->trigger_once = trig_once;

	// Store the shared data length
	argp->shared_data_len = data_len;

	// Byte-copy in the shared data
	bcopy(data, argp->shared_data, data_len);

	return (argp);
}

//
// Convenience routine to get to any fault operation data
// stored in a generic_arg_t structure
//
// Arguments:
//	generic_argsp	--	pointer to generic_arg_t structure
//	datap		--	this routine changes this pointer
//				to point at the data stored
//				in the generic_arg_t structure
//	data_len	--	this routine sets this value
//				to the length of the data stored
//				in the generic_arg_t structure
//
void
FaultFunctions::get_fault_data_from_generic_arg(
		void			*generic_argsp,
		void			**datap,
		uint32_t		&data_len)
{
	generic_arg_t *argsp = (generic_arg_t *)generic_argsp;
	if (argsp == NULL) {
		*datap = NULL;
		data_len = 0;
	} else {
		*datap = argsp->data;
		data_len = argsp->data_length;
	}
}

//
// A convenience routine for adding an Invo Trigger whose fault point
// calls the generic() fault function.
//
// Fault function generic() provides a counting feature which
// allows control of when a specified operation should be performed.
// By default generic() always performs the specified operation.
//
// Arguments:
//	op		-- specifies operation (fault function) to be
//			   performed by generic().
//	fault_num	-- fault number to add.
//	fault_argp	-- pointer to the fault argument.  To be passed
//			   to the operation as fault argument.
//	fault_argsize	-- size, in bytes, of the fault argument.
//	when		-- see comments for counter_when_t.
//			   Default: ALWAYS
//	count		-- see comments for counter_t.
//			   Default: 0
//	alloc_type	-- memory allocation type.
//			   Default: os::SLEEP
//
void
FaultFunctions::invo_trigger_add(
		enum generic_op_t	op,
		uint_t			fault_num,
		void *			fault_argp,
		uint32_t		fault_argsize,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		os::mem_alloc_type	alloc_type /* = os::SLEEP */)
{
	generic_arg_t	*argp;
	uint32_t	size;

	argp = generic_arg_alloc(size, op, fault_argp, fault_argsize,
						when, count, alloc_type);
	InvoTriggers::add(fault_num, argp, size);
	delete argp;
}

//
// A convenience routine for adding a Node Trigger whose fault point
// calls the generic() fault function.
//
// Arguments:
//	op		-- specifies operation (fault function) to be
//			   performed by generic().
//	fault_num	-- fault number to add.
//	fault_argp	-- pointer to the fault argument.  To be passed
//			   to the operation as fault argument.
//	fault_argsize	-- size, in bytes, of the fault argument.
//	nodeid		-- the node(s) to add the Node Trigger to.
//			   Valid values are:
//
//				1) A node ID
//				2) TRIGGER_THIS_NODE
//				3) TRIGGER_ALL_NODES
//
//	when		-- see comments for counter_when_t.
//			   Default: ALWAYS
//	count		-- see comments for counter_t.
//			   Default: 0
//	alloc_type	-- memory allocation type.
//			   Default: os::SLEEP
//
void
FaultFunctions::node_trigger_add(
		enum generic_op_t	op,
		uint_t			fault_num,
		void *			fault_argp,
		uint32_t		fault_argsize,
		nodeid_t		nodeid,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		os::mem_alloc_type	alloc_type /* = os::SLEEP */)
{
	generic_arg_t	*argp;
	uint32_t	size;

	argp = generic_arg_alloc(size, op, fault_argp, fault_argsize,
						when, count, alloc_type);
	NodeTriggers::add(fault_num, argp, size, (int)nodeid);
	delete argp;
}

//
// A convenience routine for adding a Boot Trigger whose fault point
// calls the generic() fault function.
//
// Arguments:
//	op		-- specifies operation (fault function) to be
//			   performed by generic().
//	fault_num	-- fault number to add.
//	fault_argp	-- pointer to the fault argument.  To be passed
//			   to the operation as fault argument.
//	fault_argsize	-- size, in bytes, of the fault argument.
//	nodeid		-- the node(s) to add the Boot Trigger to.
//			   Valid values are:
//
//				1) A node ID
//				2) TRIGGER_THIS_NODE
//				3) TRIGGER_ALL_NODES
//
//	when		-- see comments for counter_when_t.
//			   Default: ALWAYS
//	count		-- see comments for counter_t.
//			   Default: 0
//	alloc_type	-- memory allocation type.
//			   Default: os::SLEEP
//
void
FaultFunctions::boot_trigger_add(
		enum generic_op_t	op,
		uint_t			fault_num,
		void *			fault_argp,
		uint32_t		fault_argsize,
		nodeid_t		nodeid,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		os::mem_alloc_type	alloc_type /* = os::SLEEP */)
{
	generic_arg_t	*argp;
	uint32_t	size;

	argp = generic_arg_alloc(size, op, fault_argp, fault_argsize,
						when, count, alloc_type);
	BootTriggers::add(fault_num, argp, size, (int)nodeid);
	delete argp;
}

//
// Convenience routines for adding an Invo/Node/Boot Trigger whose
// fault point calls fault functions which set SystemExceptions.
//
// Arguments:
//	fault_num	-- fault number to add.
//	sys_except	-- SystemException to be set by the fault pt.
//	nodeid		-- the node(s) to add the Node/Boot Trigger to.
//			   Valid values are:
//
//				1) A node ID
//				2) TRIGGER_THIS_NODE
//				3) TRIGGER_ALL_NODES
//
//	when		-- see comments for counter_when_t.
//			   Default: ALWAYS
//	count		-- see comments for counter_t.
//			   Default: 0
//	verbose		-- fault functions might use this to decide
//			   whether to print warning message.
//	alloc_type	-- memory allocation type.
//			   Default: os::SLEEP
//
// Usage Example:
//	FaultFunctions::invo_trigger_add(
//		FAULTNUM_ORB_SET_COMM_FAILURE,
//		CORBA::COMM_FAILURE(3, CORBA::COMPLETED_MAYBE),
//		FaultFunctions::EVERY, 3);
//
void
FaultFunctions::invo_trigger_add(
		uint_t			fault_num,
		CORBA::SystemException	&sys_except,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		bool			verbose /* = true */,
		os::mem_alloc_type)
{
	sys_exception_arg_t	arg;

	arg._major = sys_except._major();
	arg._minor = sys_except._minor();
	arg.completed = sys_except.completed();
	arg.counter.when = when;
	arg.counter.count = count;
	arg.counter.counter = (when == RANDOM) ? (uint32_t)os::gethrtime() : 0;
	arg.verbose = verbose;

	InvoTriggers::add(fault_num, &arg, (uint_t)sizeof (arg));
}

void
FaultFunctions::node_trigger_add(
		uint_t			fault_num,
		CORBA::SystemException	&sys_except,
		nodeid_t		nodeid,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		bool			verbose /* = true */,
		os::mem_alloc_type)
{
	sys_exception_arg_t	arg;

	arg._major = sys_except._major();
	arg._minor = sys_except._minor();
	arg.completed = sys_except.completed();
	arg.counter.when = when;
	arg.counter.count = count;
	arg.counter.counter = (when == RANDOM) ? (uint32_t)os::gethrtime() : 0;
	arg.verbose = verbose;

	NodeTriggers::add(fault_num, &arg, (uint_t)sizeof (arg), (int)nodeid);
}

void
FaultFunctions::boot_trigger_add(
		uint_t			fault_num,
		CORBA::SystemException	&sys_except,
		nodeid_t		nodeid,
		enum counter_when_t	when /* = ALWAYS */,
		uint32_t		count /* = 0 */,
		bool			verbose /* = true */,
		os::mem_alloc_type)
{
	sys_exception_arg_t	arg;

	arg._major = sys_except._major();
	arg._minor = sys_except._minor();
	arg.completed = sys_except.completed();
	arg.counter.when = when;
	arg.counter.count = count;
	arg.counter.counter = (when == RANDOM) ? (uint32_t)os::gethrtime() : 0;
	arg.verbose = verbose;

	BootTriggers::add(fault_num, &arg, (uint_t)sizeof (arg), (int)nodeid);
}

//
// Generic fault function.  Calls another fault function based on
// the operation (enum generic_op_t) specified in the fault argument.
//
// Fault function generic() provides a counting feature which
// allows control of when a specified operation should be performed.
// For example, you can specify that the operation for a fault point
// should be performed on the 3rd time the fault point is triggered,
// or after it's been triggered five times, or the first four times
// it's triggered.  See comments for counter_t for more info.
//
// By default generic() always performs the specified operation.
//
// Fault argument:
//	A generic_arg_t struct.
//
void
FaultFunctions::generic(uint_t fault_num, void *fault_argp, uint32_t)
{
	ASSERT(fault_argp != NULL);

	generic_arg_t	*fa = (generic_arg_t *)fault_argp;

	// Check counter.
	if (! do_counting(fa->counter)) {
		return;
	}

	// Do the specified operation.
	switch (fa->op) {
	case SCQ_WAIT:
		wait(fault_num, fa->data, fa->data_length);
		break;

	case SIGNAL:
		signal(fault_num, fa->data, fa->data_length);
		break;

	case CMM_REBOOT:
#if defined(_KERNEL_ORB)
		fi_cmm_reboot_funcp(fault_num, fa->data, fa->data_length);
#else
		ASSERT(0);
#endif // _KERNEL_ORB
		break;

	case HA_DEP_REBOOT:
		ha_dep_reboot(fault_num, fa->data, fa->data_length);
		break;

	case HA_DEP_SIGNAL:
		ha_dep_signal(fault_num, fa->data, fa->data_length);
		break;

	case SERVICE_REBOOT:
		service_reboot(fault_num, fa->data, fa->data_length);
		break;

	case PANIC:
		panic(fault_num, fa->data, fa->data_length);
		break;

	case REBOOT:
		reboot(fault_num, fa->data, fa->data_length);
		break;

	case REBOOT_ONCE:
		reboot_once(fault_num, fa->data, fa->data_length);
		break;

	case REBOOT_SPECIFIED:
		reboot_specified(fault_num, fa->data, fa->data_length);
		break;

	case REBOOT_HA_RM:
		reboot_ha_rm(fault_num, fa->data, fa->data_length);
		break;

	case REBOOT_DIFFERENT:
		reboot_different(fault_num, fa->data, fa->data_length);
		break;

	case SLEEP:
		sleep(fault_num, fa->data, fa->data_length);
		break;

	case PXFS_SLEEP:
		pxfs_sleep(fault_num, fa->data, fa->data_length);
		break;

	case SLEEP_ONCE:
		sleep_once(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_P:
		sema_p(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_P_ONCE:
		sema_p_once(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_P_SPECIFIED:
		sema_p_specified(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_V:
		sema_v(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_V_ONCE:
		sema_v_once(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_V_SPECIFIED:
		sema_v_specified(fault_num, fa->data, fa->data_length);
		break;

	case HA_DEP_SEMA_V:
		ha_dep_sema_v(fault_num, fa->data, fa->data_length);
		break;

	case SWITCHOVER:
		switchover(fault_num, fa->data, fa->data_length);
		break;

	case PXFS_RESOLVE:
		// Invalid fault opcode specified
		ASSERT(false);
		break;

	case SEND_HELD_TRINFO_ACK:

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
		send_held_trinfo_ack(fault_num, fa->data, fa->data_length);
#else
		ASSERT(0);
#endif
		break;

	case SEMA_P_WITH_SHARED_DATA:
		sema_p_with_shared_data(fault_num, fa->data, fa->data_length);
		break;

	case SEMA_V_WITH_SHARED_DATA:
		sema_v_with_shared_data(fault_num, fa->data, fa->data_length);
		break;

	case CHECK_EVENT_ORDER:
		check_event_order(fault_num, fa->data, fa->data_length);
		break;

	default:
		// Invalid fault opcode specified
		ASSERT(false);
		break;
	}
}

//
// Panic a node/kernel (current or remote).
// Fault argument:
//	If 0, then the current node is assumed.
//	Otherwise, the node ID of the node to panic.
// WARNING:
//	If called from user-land process, this will cause the current
//	or remote KERNEL to panic, not the process itself.
//
void
FaultFunctions::panic(uint_t fault_num, void *fault_argp, uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (wait_for_arg_t));

	wait_for_arg_t	*wait_argp;
	nodeid_t	nodeid;

	// Extract the wait_for_arg_t structure from the fault argument
	wait_argp = (wait_for_arg_t *)fault_argp;

	if (wait_argp->nodeid == 0) {
		wait_argp->nodeid = orb_conf::local_nodeid();
	}

	nodeid = wait_argp->nodeid;

	// Setup the wait_for_X
	do_setup_wait(wait_argp);

	(void) fault_rpc::send_fault(fault_rpc::NODE_PANIC, nodeid, fault_num);
	//
	// The panic currently causes an error for the oneway message.
	// So the code always waits.
	//
	// Do the wait_for_X
	do_wait(wait_argp);
}

//
// Called when a node/kernel panic (fault injection rpc) message is received.
//
void
FaultFunctions::panic_rpc_handler(recstream *re)
{
	uint32_t	fault_num = re->get_unsigned_long();

#ifdef _KERNEL_ORB
	ID_node	&src_node = re->get_src_node();

	os::panic("Node panic from injected fault 0x%x on node %d to node %d\n",
		fault_num, src_node.ndid, orb_conf::local_nodeid());
#else
	os::panic("User process panic from injected fault 0x%x to node %d\n",
		fault_num, orb_conf::local_nodeid());
#endif
}

//
// Block a thread until signaled (wait and signal must be used as a pair
// with signal)
//
// Fault Arguments
//	None
void
FaultFunctions::wait(uint_t fault_num, void *, uint32_t)
{
#ifdef _KERNEL_ORB
	os::warning("Fault 0x%x will wait for a signal.", fault_num);

	lock();

	while (!_signaled) {
		_cv.wait(&_lock);
	}

	_signaled = 0;

	unlock();
#else
	// We must be in userspace. Call down into the kernel with cladm
	if (os::cladm(CL_CONFIG, CL_FAULT_WAIT, &fault_num) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_WAIT): %s\n",
			strerror(errno));
	}
#endif	// _KERNEL_ORB
}

//
// Unblock a thread that is waiting (wait and signal must be used as a pair)
//
// Fault Arguments
//	None
void
FaultFunctions::signal(uint_t fault_num, void *, uint32_t)
{
#ifdef _KERNEL_ORB
	os::warning("Fault 0x%x has done a signal.", fault_num);

	lock();

	_signaled = 1;
	_cv.broadcast();

	unlock();
#else
	// We must be in userspace call down into the kernel with cladm
	if (os::cladm(CL_CONFIG, CL_FAULT_SIGNAL, &fault_num) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_SIGNAL): %s\n",
			strerror(errno));
	}
#endif	// _KERNEL_ORB
}

//
// Unblock a thread that is waiting (wait and signal must be used as a pair)
//
// Fault Arguments
//	None
void
#ifdef _KERNEL_ORB
FaultFunctions::ha_dep_signal(uint_t fault_num, void *argp, uint32_t arg_len)
#else
FaultFunctions::ha_dep_signal(uint_t, void *, uint32_t)
#endif
{
#ifdef _KERNEL_ORB
	ASSERT(argp != NULL);
	ASSERT(arg_len == sizeof (replica::service_num_t));

	replica::service_num_t	sid = *(replica::service_num_t *)argp;

	if (sid == tsd_svc_id()) {
		os::warning("Fault 0x%x has done a ha-dep-signal.", fault_num);

		lock();

		_signaled = 1;
		_cv.broadcast();

		unlock();

		// turn off the fault before first (Global and Environment)
		// No need to copy the wait arguments (we already did)
		NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
		InvoTriggers::clear(fault_num);
	}
#else
	// We must be in userspace call down into the kernel with cladm
	os::panic("This system call does not exist!");
#endif	// _KERNEL_ORB
}

//
// Reboot a node
// This fault-function is used to reboot a node or to halt it if its
// a unode-process.  (The halt is done via raise(SIGINT) to avoid dumping
// core.
// Fault Argument:
//	A wait_for_arg_t struct that specifies the nodeid to reboot
//	and the wait type.  Nodeid 0 means reboot the current node.
//
void
FaultFunctions::reboot(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (wait_for_arg_t));

	wait_for_arg_t	*wait_argp;
	nodeid_t	nodeid;

	// Extract the wait_for_arg_t structure from the fault argument
	wait_argp = (wait_for_arg_t *)fault_argp;

	if (wait_argp->nodeid == 0)
		wait_argp->nodeid = orb_conf::local_nodeid();

	nodeid = wait_argp->nodeid;

	// Setup the wait_for_X
	do_setup_wait(wait_argp);

	if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid, fault_num)) {
		// Wait for unknown
		do_wait(wait_argp);
	}
}

//
// Reboot a node once
//
// This faultfunction implements only-once semantics, by turning off
// the fault point before calling reboot.  (It will turn the fault off on
// Node and Invo triggers) The function also waits until all nodes in the
// cluster have joined so that we can clear BootTriggers as well.
// Fault Argument:
//	A wait_for_arg_t struct that specifies the nodeid to reboot
//	and the wait type.  Nodeid 0 means reboot the current node.
//
void
FaultFunctions::reboot_once(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (wait_for_arg_t));

	wait_for_arg_t	*wait_argp;
	wait_for_arg_t	wait_cpy;
	nodeid_t	nodeid;
	Environment	e;

	// Extract the wait_for_arg_t structure from the fault argument
	wait_argp = (wait_for_arg_t *)fault_argp;

	if (wait_argp->nodeid == 0)
		wait_argp->nodeid = orb_conf::local_nodeid();

	nodeid = wait_argp->nodeid;

	// Copy the wait info we'll need it after its been deleted by
	// the NodeTriggers::clear() and the InvoTriggers::clear() below.
	wait_cpy.op = wait_argp->op;
	wait_cpy.nodeid = wait_argp->nodeid;

	// Set up the wait_for_X
	do_setup_wait(&wait_cpy);

	// Turn off the fault before sending the fault message
	// to the target node.
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);

#if	defined(_KERNEL)
	// Make sure that all nodes of the cluster are online.
	int		iteration_count = 0;
	int		max_iterations = 20;
	int		num_seconds_wait = 5;
	bool	state = false;

	while (!state && (iteration_count < max_iterations)) {
		state = true;
		quorum_status_t *clust_memb;
		if (cluster_get_quorum_status_funcp(&clust_memb) == 0) {
			for (uint32_t i = 0; i < clust_memb->num_nodes; i++) {
				if (clust_memb->nodelist[i].state
				    != QUORUM_STATE_ONLINE) {
					os::printf("Node %d not up\n", i);
					state = false;
				}
			}
		}
		if (!state) {
			os::usecsleep((long)num_seconds_wait * 1000000);
		}
		iteration_count++;
	}

	// ASSERT(state);
	if (!state) {
		os::printf("+++ FAIL: node is not up unexpectedly.\n");
		os::printf("Skip rebooting node %d\n", nodeid);
		return;
	}
#endif

	os::printf("Rebooting node %d\n", nodeid);
	if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid, fault_num)) {
		// Wait for unknown
		do_wait(&wait_cpy);
	}
}

//
// Reboot a specific node
//
// This fault function will verify that the current node has the same
// node id as the one specified in the fault arguments before rebooting
// it.  In other words, this function only allows a node to reboot itself.
// Fault Argument:
//	A wait_for_arg_t struct that specifies the nodeid to reboot
//	and the wait type.  Nodeid 0 means always reboot the current
//	node.
//
// NOTE: This is different from just plain reboot, because some faults
// specially in HA fault-injection testing will be triggered several
// times during a retry.  In this case we dont want the retries to keep
// on attempting to reboot a specific node.  (Which plain reboot would
// do).
//
void
FaultFunctions::reboot_specified(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (wait_for_arg_t));

	wait_for_arg_t	*wait_argp;
	nodeid_t	nodeid;

	// Extract the wait_for_arg_t structure from the fault argument
	wait_argp = (wait_for_arg_t *)fault_argp;

	if (wait_argp->nodeid == 0)
		wait_argp->nodeid = orb_conf::local_nodeid();

	nodeid = wait_argp->nodeid;

	if (nodeid == orb_conf::local_nodeid()) {
		// Set up the wait for unknown
		do_setup_wait(wait_argp);

		if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid,
		    fault_num)) {
			// Wait for unknown
			do_wait(wait_argp);
		}
	}
}

void
FaultFunctions::reboot_ha_rm(
	uint_t fault_num,
	void *fault_argp,
	uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (ha_rm_reboot_arg_t));

	ha_rm_reboot_arg_t	*reboot_argp;
	nodeid_t		nodeid;
	wait_for_arg_t		wait_arg;

	// Extract the ha_rm_reboot_arg_t structure from the fault argument
	reboot_argp = (ha_rm_reboot_arg_t *)fault_argp;

	if (reboot_argp->nodeid == 0)
		reboot_argp->nodeid = orb_conf::local_nodeid();

	nodeid = reboot_argp->nodeid;

	wait_arg.op = reboot_argp->op;
	wait_arg.nodeid = nodeid;

	if (nodeid == orb_conf::local_nodeid()) {
		// Set up the wait for unknown
		do_setup_wait(&wait_arg);

		if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid,
		    fault_num)) {
			// Wait for unknown
			do_wait(&wait_arg);
		}
	}
}

//
// Reboot a specific node only if different from current node
//
// This fault function is the same as reboot_specified() above, except
// that a reboot is only performed if the specified node to be rebooted
// is DIFFERENT from the current node.  In other words, this function
// only allows rebooting another node (unless nodeid 0 is specified).
// Fault Argument:
//	A wait_for_arg_t struct that specifies the nodeid to reboot
//	and the wait type.  Nodeid 0 means always reboot the current
//	node.
//
// NOTE: This is useful for fault points that are located in both the
// request and reply paths, and we want to reboot only the other node.
// E.g. a client on node A makes a request to the server on node B, and
// we want the client to reboot the server only when it gets the
// reply from the server.  Since the fault point is located on both
// request and reply paths, we don't want the server to reboot itself
// when it's servicing the request.
//
void
FaultFunctions::reboot_different(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (wait_for_arg_t));

	wait_for_arg_t	*wait_argp;
	nodeid_t	nodeid;

	// Extract the wait_for_arg_t structure from the fault argument
	wait_argp = (wait_for_arg_t *)fault_argp;

	nodeid = wait_argp->nodeid;

	if (nodeid != orb_conf::local_nodeid()) {
		if (nodeid == 0) {
			// Nodeid 0 means current node.
			nodeid = orb_conf::local_nodeid();
		}

		// Set up the wait for unknown
		do_setup_wait(wait_argp);

		if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid,
		    fault_num)) {
			// Wait for unknown
			do_wait(wait_argp);
		}
	}
}

//
// HA dependency reboot
//
// This fault point will reboot a remote / local node when the sid
// specified matches the sid stored in tsd.
// Fault Argument:
//	A ha_dep_reboot_arg_t structure that specifies the node to reboot
//	and the sid, and wait op
//
// NOTE: Fault function is ONLY_ONCE semantics
//
// NOTE: If the sid in the fault arguments is set to 0, and the sid in TSD is
// not set (returns 0) then we'll accept this.   Make sure you are careful
// when you do this.. and use invo triggers in this case
//
void
FaultFunctions::ha_dep_reboot(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (ha_dep_reboot_arg_t));

	ha_dep_reboot_arg_t	*ha_dep_argp	= NULL;
	wait_for_arg_t		wait_arg;
	replica::service_num_t	target_sid;
	replica::service_num_t	thread_sid;
	nodeid_t		nodeid;

	// Extract the ha_dep_reboot_arg_t structure from the fault argument
	ha_dep_argp = (ha_dep_reboot_arg_t *)fault_argp;

	if (ha_dep_argp->nodeid == 0)
		ha_dep_argp->nodeid = orb_conf::local_nodeid();

	target_sid = ha_dep_argp->sid;
	nodeid = ha_dep_argp->nodeid;

	// See if we're in the correct sid
	thread_sid = tsd_svc_id();

	if (target_sid != thread_sid) {
		return;
	}

	wait_arg.op = ha_dep_argp->op;
	wait_arg.nodeid = nodeid;

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// turn off the fault before first (Global and Environment)
	// No need to copy the wait arguments (we already did)
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);

	if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid,
	    fault_num)) {
		// Wait for unknown
		do_wait(&wait_arg);
	}
}

//
// Service reboot
//
// This fault point will reboot a remote / local node of the specified sid
// Fault Argument:
//	A svc_reboot_arg_t structure that specifies the node to reboot
//	and the sid, and wait op
//
// NOTE: Fault function is ONLY_ONCE semantics
//
//
void
FaultFunctions::service_reboot(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (svc_reboot_arg_t));

	svc_reboot_arg_t	*svc_argp = NULL;
	wait_for_arg_t	  wait_arg;
	nodeid_t		nodeid;

	// Extract the svc_reboot_arg_t structure from the fault argument
	svc_argp = (svc_reboot_arg_t *)fault_argp;

	if (svc_argp->nodeid == 0)
		svc_argp->nodeid = orb_conf::local_nodeid();

	nodeid = svc_argp->nodeid;

	wait_arg.op = svc_argp->op;
	wait_arg.nodeid = nodeid;

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// turn off the fault before first (Global and Environment)
	// No need to copy the wait arguments (we already did)
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);

	if (fault_rpc::send_fault(fault_rpc::NODE_REBOOT, nodeid,
	    fault_num)) {
		// Wait for unknown
		do_wait(&wait_arg);
	}
}

//
// Called when a node reboot (fault injection rpc) message is received.
//
void
FaultFunctions::reboot_rpc_handler(recstream *re)
{
	uint32_t	fault_num = re->get_unsigned_long();

#if defined(_KERNEL_ORB)
	ID_node	&src_node = re->get_src_node();

	os::warning("Node reboot from injected fault 0x%x "
		"on node %d to node %d",
		fault_num, src_node.ndid, orb_conf::local_nodeid());

	fi_do_reboot_funcp(false);
#else
	os::warning("User process ignored node_reboot fault message 0x%x",
		fault_num);
#endif // _KERNEL_ORB
}

//
// Reboot all nodes
//
// Will reboot all nodes in cluster.  Will examine the membership
// and reboot all nodes.  (Current node last)
// Will do a clean reboot.
//
void
FaultFunctions::reboot_all_nodes()
{
#if defined(_KERNEL_ORB)
	// If we're in the kernel or unode just execute the reboot otherwise
	// we'll need to call into the kernel to access the clconf routines

	nodeid_t root_node = orb_conf::get_root_nodeid();
	nodeid_t	this_node = orb_conf::local_nodeid();

	wait_for_arg_t	wait_arg;

	// Wait for node to go DEAD, since doing a clean reboot will cause the
	// node to do a cmm_callback_impl::shutdown which will cause it to go
	// straight to DOWN
	wait_arg.op = WAIT_FOR_DEAD;

	if (this_node != root_node)
		os::warning("reboot_all_nodes was not invoked on root node, "
		    "this could lead to a loss of quorum!");

	os::warning("Will reboot all nodes in membership");

	for (nodeid_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		if ((members::the().alive(nodeid)) &&
		    (nodeid != this_node) &&
		    (nodeid != root_node)) {
			wait_arg.nodeid = nodeid;
			do_setup_wait(&wait_arg);
			os::warning("Rebooting node %d.", nodeid);
			(void) fault_rpc::send_fault(
				fault_rpc::NODE_REBOOT_CLEAN, nodeid, 0);
			do_wait(&wait_arg);
		} // alive
	}

	// Reboot the root node
	os::warning("Rebooting the root node (%d).", root_node);
	(void) fault_rpc::send_fault(fault_rpc::NODE_REBOOT_CLEAN,
								root_node, 0);

	if (root_node != this_node) {
		// Now reboot this node
		os::warning("Rebooting local node (%d).", this_node);
		(void) fault_rpc::send_fault(fault_rpc::NODE_REBOOT_CLEAN,
								this_node, 0);
	}
#else
	// We must be in userspace. Call down into the kernel with cladm
	if (os::cladm(CL_CONFIG, CL_FAULT_REBOOT_ALL_NODES, NULL) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_REBOOT_ALL_NODES): "
			"%s\n", strerror(errno));
	}
#endif // _KERNEL_ORB
}

//
// Called when a node reboot clean(fault injection rpc) message is received.
//
void
FaultFunctions::reboot_clean_rpc_handler(recstream *re)
{
	uint32_t	fault_num = re->get_unsigned_long();

#if defined(_KERNEL_ORB)
	ID_node	&src_node = re->get_src_node();

	os::warning("Node reboot from injected fault 0x%x "
		"on node %d to node %d",
		fault_num, src_node.ndid, orb_conf::local_nodeid());

	fi_do_reboot_funcp(true);
#else
	os::warning("User process ignored node_reboot fault message 0x%x",
		fault_num);
#endif // _KERNEL_ORB
}

//
// do a sema_p on a mc_sema object
//
// This fault function will do a sema_p operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.
void
FaultFunctions::sema_p(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (mc_sema_arg_t));

	mc_sema_arg_t		*sema_arg_ptr	= NULL;
	wait_for_arg_t		wait_arg;
	mc_sema::sema_var	sema_ref;
	mc_sema::cookie		sema_id;
	Environment		e;

	// Get the mc sema fault arguments
	sema_arg_ptr = (mc_sema_arg_t *)fault_argp;
	sema_id = sema_arg_ptr->id;
	ASSERT(sema_id != 0);

	// Extract the wait_for arguments
	wait_arg.op = sema_arg_ptr->wait.op;
	wait_arg.nodeid = sema_arg_ptr->wait.nodeid;

	// Create the mc_sema argument with that cookie
	sema_ref = (new mc_sema_impl(sema_id))->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	os::warning("Fault 0x%x will do a sema_p on mc_sema: %lu",
		fault_num, sema_id);

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// Do a sema_p operation on it
	sema_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("FaultFunctions::sema_p");
		return;
	}

	// do the wait_for_X
	do_wait(&wait_arg);
}

//
// do a sema_p on a mc_sema object
//
// This fault function will do a sema_p operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.  (Implements ONLY ONCE semamtics)
//
void
FaultFunctions::sema_p_once(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	// By default, clear the triggers from all nodes
	sema_p_once_internal(fault_num, fault_argp,
	    fault_arglen, (int)TRIGGER_ALL_NODES);
}

//
// This is an internal routine for use by FaultFunctions methods
// that need to do sema_p_once type of semantics.
// This routine takes an additional argument nodeid_to_clear
// that specifies the nodeid from which the triggers have to be cleared
// to obey the once-only semantics.
//
void
FaultFunctions::sema_p_once_internal(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen, int nodeid_to_clear)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (mc_sema_arg_t));

	mc_sema_arg_t		*sema_arg_ptr	= NULL;
	wait_for_arg_t		wait_arg;
	mc_sema::sema_var	sema_ref;
	mc_sema::cookie		sema_id;
	Environment		e;

	// Get the mc sema fault arguments
	sema_arg_ptr = (mc_sema_arg_t *)fault_argp;
	sema_id = sema_arg_ptr->id;
	ASSERT(sema_id != 0);

	// Copy the wait info (we'll need it after turning the fault off
	wait_arg.op = sema_arg_ptr->wait.op;
	wait_arg.nodeid = sema_arg_ptr->wait.nodeid;

	// Create the mc_sema argument with that cookie
	sema_ref = (new mc_sema_impl(sema_id))->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	// turn off the fault before first (Global and Environment)
	// No need to copy the wait arguments (we already did)
	NodeTriggers::clear(fault_num, nodeid_to_clear);
	InvoTriggers::clear(fault_num);

	os::warning("Fault 0x%x will do a sema_p on mc_sema: %lu",
		fault_num, sema_id);

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// Do a sema_p operation on it
	sema_ref->sema_p(e);
	if (e.exception()) {
		e.exception()->print_exception("FaultFunctions::sema_p");
		return;
	}

	// do the wait_for_X
	do_wait(&wait_arg);
}

//
// do a sema_p on a mc_sema object
//
// This fault function will do a sema_p operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.
//
// This functions will only do the sema_v operation if the nodeid passed
// in the fault argument matches the current nodeid, or if its 0.
void
FaultFunctions::sema_p_specified(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (mc_sema_arg_t));

	mc_sema_arg_t		*sema_arg_ptr	= NULL;
	wait_for_arg_t		wait_arg;
	mc_sema::sema_var	sema_ref;
	mc_sema::cookie		sema_id;
	nodeid_t		nodeid;
	Environment		e;

	// Get the mc sema fault arguments
	sema_arg_ptr = (mc_sema_arg_t *)fault_argp;
	sema_id = sema_arg_ptr->id;
	ASSERT(sema_id != 0);

	// Extract the wait_for arguments
	wait_arg.op = sema_arg_ptr->wait.op;
	wait_arg.nodeid = sema_arg_ptr->wait.nodeid;

	nodeid = sema_arg_ptr->nodeid;

	if (nodeid == 0)
		nodeid = orb_conf::local_nodeid();

	if (nodeid == orb_conf::local_nodeid()) {
		// Create the mc_sema argument with that cookie
		sema_ref = (new mc_sema_impl(sema_id))->get_objref();
		ASSERT(!CORBA::is_nil(sema_ref));

		os::warning("Fault 0x%x will do a sema_p on mc_sema: %lu",
			fault_num, sema_id);

		// Setup the wait_for_X
		do_setup_wait(&wait_arg);


		// Do a sema_p operation on it
		sema_ref->sema_p(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "FaultFunctions::sema_p_specified");
			return;
		}

		// do the wait_for_X
		do_wait(&wait_arg);
	}
}

//
// do a sema_v on a mc_sema object
//
// This fault function will do a sema_v operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.
void
FaultFunctions::sema_v(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (mc_sema_arg_t));

	mc_sema_arg_t		*sema_arg_ptr = NULL;
	wait_for_arg_t		wait_arg;
	mc_sema::sema_var	sema_ref;
	mc_sema::cookie		sema_id;
	Environment		e;

	// Get the mc sema fault arguments
	sema_arg_ptr = (mc_sema_arg_t *)fault_argp;
	sema_id = sema_arg_ptr->id;
	ASSERT(sema_id != 0);

	// Extract the wait_for arguments
	wait_arg.op = sema_arg_ptr->wait.op;
	wait_arg.nodeid = sema_arg_ptr->wait.nodeid;

	// Create the mc_sema argument with that cookie
	sema_ref = (new mc_sema_impl(sema_id))->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	os::warning("Fault 0x%x will do a sema_v on mc_sema: %lu",
		fault_num, sema_id);

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// Do a sema_v operation on it
	sema_ref->sema_v(e);
	if (e.exception()) {
		e.exception()->print_exception("FaultFunctions::sema_v");
	}

	// do the wait_for_X
	do_wait(&wait_arg);
}

//
// do a sema_v on a mc_sema object
//
// This fault function will do a sema_v operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.  (Implements ONLY ONCE semantics)
//
void
FaultFunctions::sema_v_once(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	// By default, clear the triggers from all nodes
	sema_v_once_internal(fault_num, fault_argp,
	    fault_arglen, (int)TRIGGER_ALL_NODES);
}

//
// This is an internal routine for use by FaultFunctions methods
// that need to do sema_v_once type of semantics.
// This routine takes an additional argument nodeid_to_clear
// that specifies the nodeid from which the triggers have to be cleared
// to obey the once-only semantics.
//
void
FaultFunctions::sema_v_once_internal(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen, int nodeid_to_clear)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (mc_sema_arg_t));

	mc_sema_arg_t		*sema_arg_ptr	= NULL;
	wait_for_arg_t		wait_arg;
	mc_sema::sema_var	sema_ref;
	mc_sema::cookie		sema_id;
	Environment		e;

	// Get the mc sema fault arguments
	sema_arg_ptr = (mc_sema_arg_t *)fault_argp;
	sema_id = sema_arg_ptr->id;
	ASSERT(sema_id != 0);

	// Copy the wait_for_arg_t because we'll need it after the fault
	// is turned off and the fault args are lost
	wait_arg.op = sema_arg_ptr->wait.op;
	wait_arg.nodeid = sema_arg_ptr->wait.nodeid;

	// Create the mc_sema argument with that cookie
	sema_ref = (new mc_sema_impl(sema_id))->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	// turn off the fault before first (Global and Environment)
	// No need to copy the wait arguments (we already did)
	NodeTriggers::clear(fault_num, nodeid_to_clear);
	InvoTriggers::clear(fault_num);

	os::warning("Fault 0x%x will do a sema_v on mc_sema: %lu",
		fault_num, sema_id);

	// Setup the wait_for_X
	do_setup_wait(&wait_arg);

	// Do a sema_v operation on it
	sema_ref->sema_v(e);
	if (e.exception()) {
		e.exception()->print_exception("FaultFunctions::sema_v");
		return;
	}

	// do the wait_for_X
	do_wait(&wait_arg);
}

//
// do a sema_v on a mc_sema object
//
// This fault function will do a sema_v operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.
//
// This functions will only do the sema_v operation if the nodeid passed
// in the fault argument matches the current nodeid, or if its 0.
void
FaultFunctions::sema_v_specified(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (mc_sema_arg_t));

	mc_sema_arg_t		*sema_arg_ptr	= NULL;
	wait_for_arg_t		wait_arg;
	mc_sema::sema_var	sema_ref;
	mc_sema::cookie		sema_id;
	nodeid_t		nodeid;
	Environment		e;

	// Get the mc sema fault arguments
	sema_arg_ptr = (mc_sema_arg_t *)fault_argp;
	sema_id = sema_arg_ptr->id;
	ASSERT(sema_id != 0);

	// Extract the wait_for arguments
	wait_arg.op = sema_arg_ptr->wait.op;
	wait_arg.nodeid = sema_arg_ptr->wait.nodeid;

	nodeid = sema_arg_ptr->nodeid;

	if (nodeid == 0)
		nodeid = orb_conf::local_nodeid();

	if (nodeid == orb_conf::local_nodeid()) {
		// Create the mc_sema argument with that cookie
		sema_ref = (new mc_sema_impl(sema_id))->get_objref();
		ASSERT(!CORBA::is_nil(sema_ref));

		os::warning("Fault 0x%x will do a sema_v on mc_sema: %lu",
			fault_num, sema_id);

		// Setup the wait_for_X
		do_setup_wait(&wait_arg);

		// Do a sema_v operation on it
		sema_ref->sema_v(e);
		if (e.exception()) {
			e.exception()->print_exception(
			    "FaultFunctions::sema_v_specified");
		}

		// do the wait_for_X
		do_wait(&wait_arg);
	}
}

//
// do a ha_dep_sema_v on a mc_sema object
//
// This fault function will do a sema_v operation on a mc_sema object
// the fault_argument structure mc_sema_arg_t constains the cookie
// for the mc_sema object.
//
// Will examine the value of the thread specific data to make sure it matches
// the value specified on the arguments (the sid)
//
// Will implement ONLY ONCE SEMANTICS
void
FaultFunctions::ha_dep_sema_v(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (ha_dep_mc_sema_arg_t));

	ha_dep_mc_sema_arg_t	*ha_dep_sema_arg_ptr = NULL;
	mc_sema_arg_t		sema_arg;
	mc_sema::sema_var	sema_ref;
	replica::service_num_t	target_sid;
	replica::service_num_t	thread_sid;
	Environment		e;

	// Get the ha dep mc sema fault arguments
	ha_dep_sema_arg_ptr = (ha_dep_mc_sema_arg_t *)fault_argp;

	// See if we're in the correct sid
	target_sid = ha_dep_sema_arg_ptr->sid;
	thread_sid = tsd_svc_id();

	if (target_sid != thread_sid) {
		return;
	}

	// Extract the mc_sema arguments
	sema_arg.id = ha_dep_sema_arg_ptr->mc_sema_arg.id;
	sema_arg.nodeid = ha_dep_sema_arg_ptr->mc_sema_arg.nodeid;
	ASSERT(sema_arg.id != 0);

	// Extract the wait_for arguments
	sema_arg.wait.op = ha_dep_sema_arg_ptr->mc_sema_arg.wait.op;
	sema_arg.wait.nodeid = ha_dep_sema_arg_ptr->mc_sema_arg.wait.nodeid;

	// turn off the fault (Global and Environment)
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);

	// Create the mc_sema argument with that cookie
	sema_ref = (new mc_sema_impl(sema_arg.id))->get_objref();
	ASSERT(!CORBA::is_nil(sema_ref));

	os::warning("Fault (%lu) will do a ha_dep_sema_v on mc_sema: %lu",
		fault_num, sema_arg.id);

	// Setup the wait_for_X
	do_setup_wait(&(sema_arg.wait));

	// Do a sema_v operation on it
	sema_ref->sema_v(e);
	if (e.exception()) {
		e.exception()->print_exception("FaultFunctions::sema_v");
	}

	// do the wait_for_X
	do_wait(&(sema_arg.wait));
}

//
// do a sema_p or sema_p_once type of action on a mc_sema object,
// with additional storage for fault operation data
//
// This fault function will do a sema_p or sema_p_once type of operation
// on a mc_sema object. The fault argument structure contains the cookie
// for the mc_sema object.
//
void
FaultFunctions::sema_p_with_shared_data(
    uint_t fault_num, void *fault_argp, uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);

	os::warning(
	    "Fault (%lu) will do a sema_p_with_shared_data\n", fault_num);

	mc_sema_with_data_arg_t *argp = NULL;
	mc_sema_arg_t		*mc_sema_argp = NULL;

	// Cast the fault arguments into the proper type
	argp = (mc_sema_with_data_arg_t *)fault_argp;

	//
	// Shared data is for use by code that fires the fault point.
	// We need to just do a sema_p or sema_p_once type of action
	// with the mc_sema_arg_t embedded in the fault arg structure.
	// For the sema_p_once type of action, we clear the triggers
	// from the local node only, since the triggers would be armed
	// on the local node only; so we use sema_p_once_internal
	// instead of sema_p_once directly
	//
	mc_sema_argp = &(argp->mc_sema_arg);
	if (argp->trigger_once) {
		sema_p_once_internal(fault_num, (void *)mc_sema_argp,
		    (uint32_t)sizeof (mc_sema_arg_t), (int)TRIGGER_THIS_NODE);
	} else {
		sema_p(fault_num, (void *)mc_sema_argp,
		    (uint32_t)sizeof (mc_sema_arg_t));
	}

	os::warning("Fault (%lu) returning from a sema_p_with_shared_data\n",
	    fault_num);
}

//
// do a sema_v or sema_v_once type of action on a mc_sema object,
// with additional storage for fault operation data
//
// This fault function will do a sema_v or sema_v_once type of operation
// on a mc_sema object. The fault argument structure contains the cookie
// for the mc_sema object.
//
void
FaultFunctions::sema_v_with_shared_data(
    uint_t fault_num, void *fault_argp, uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);

	os::warning(
	    "Fault (%lu) will do a sema_v_with_shared_data\n", fault_num);

	mc_sema_with_data_arg_t *argp = NULL;
	mc_sema_arg_t		*mc_sema_argp = NULL;

	// Cast the fault arguments into the proper type
	argp = (mc_sema_with_data_arg_t *)fault_argp;

	//
	// Shared data is for use by code that fires the fault point.
	// We need to just do a sema_v or sema_v_once type of action
	// with the mc_sema_arg_t embedded in the fault arg structure.
	// For the sema_v_once type of action, we clear the triggers
	// from the local node only, since the triggers would be armed
	// on the local node only; so we use sema_v_once_internal
	// instead of sema_v_once directly
	//
	mc_sema_argp = &(argp->mc_sema_arg);
	if (argp->trigger_once) {
		sema_v_once_internal(fault_num, (void *)mc_sema_argp,
		    (uint32_t)sizeof (mc_sema_arg_t), (int)TRIGGER_THIS_NODE);
	} else {
		sema_v(fault_num, (void *)mc_sema_argp,
		    (uint32_t)sizeof (mc_sema_arg_t));
	}
	os::warning("Fault (%lu) done a sema_v_with_shared_data\n", fault_num);
}

//
// sleep for time specified
//
// Fault args are of type os::usec_t
//
void
FaultFunctions::sleep(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (os::usec_t));

	os::usec_t	sleep_time = 0;

	bcopy(fault_argp, &sleep_time, sizeof (os::usec_t));

	os::warning("Fault 0x%x will go to sleep for (%lu) usecs.",
		fault_num, sleep_time);

	// Sleep for the time specified
	os::usecsleep(sleep_time);

	os::warning("Fault 0x%x awoke", fault_num);
}

//
// sleep for time specified
//
// Fault args are of type os::usec_t.  Implements ONLY ONCE semantics
//
void
FaultFunctions::sleep_once(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (os::usec_t));

	os::usec_t	sleep_time = 0;

	bcopy(fault_argp, &sleep_time, sizeof (os::usec_t));

	// turn off the fault before first (Global and Environment)
	// No need to copy the wait arguments (we already did)
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);

	os::warning("Fault 0x%x will go to sleep for (%lu) usecs.",
		fault_num, sleep_time);

	// Sleep for the time specified
	os::usecsleep(sleep_time);

	os::warning("Fault 0x%x awoke", fault_num);
}

//
// sleep for time specified after binding an object to the nameserver
// This is used by pxfs fault injection tests. This is same as the
// fault function sleep except that this function binds an object
// to the nameserver with 'faultpoint' number as the name
//
// Fault args are of type os::usec_t
//
void
FaultFunctions::pxfs_sleep(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	Environment	e;

	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (os::usec_t));

	os::usec_t	sleep_time = 0;

	bcopy(fault_argp, &sleep_time, sizeof (os::usec_t));

	os::warning("Fault 0x%x will go to sleep for %lu usecs.",
		fault_num, sleep_time);

	data_container_impl *data = new data_container_impl();
	data_container::data_var dobj = data->get_objref();
	char	*tstr = "test string ";

	dobj->set_data(tstr, e);

	char 	name[64];
	naming::naming_context_var context = root_nameserver_funcp();

	os::sprintf(name, "pxfs_sleep-%lu", fault_num);

	context->bind(name, dobj, e);

	if (e.exception()) {
		e.exception()->print_exception("ERROR:");
		os::printf("ERROR: can't bind \"%s\"\n", name);
	} else {
		// Sleep for the time specified
		os::usecsleep(sleep_time);
		os::warning("Fault 0x%x awoke", fault_num);
	}
}

//
// Set a system exception as specified in the fault argument.
// Fault Argument:
//	sys_exception_arg_t structure.
// Note:
//	This fault function can be called only from FAULT_POINT_ENV()
//	or FAULTPT_ENV_*() macros.
//
void
FaultFunctions::set_sys_exception(uint_t fault_num, Environment *envp,
		void *fault_argp, uint32_t fault_argsize)
{
	ASSERT(envp != NULL);
	ASSERT(fault_argp != NULL);
	ASSERT(fault_argsize == sizeof (sys_exception_arg_t));

	sys_exception_arg_t	*argp = (sys_exception_arg_t *)fault_argp;

	// Check counter.
	if (! do_counting(argp->counter)) {
		return;
	}

	if (argp->verbose) {
		os::warning("Fault 0x%x setting SystemException %s(%d, %d)",
			fault_num, major_translation[argp->_major].name,
			argp->_minor, argp->completed);
	}

	envp->system_exception(CORBA::SystemException(argp->_major,
		argp->_minor, argp->completed));
}

//
// Given a local adapter ID and a remote node ID, fail the associated pathend.
// This is mainly for the "failpath" test utility.
//
// Note:
//	This method, the "failpath" utility and the failpath_t
//	structure do not use the Fault Injection Framework.
//	This method is placed here for convenience.
//
// Returns:
//	0		- successful.
//	EAGAIN		- there is only one endpoint to the specified
//			  remote node and the "force" field of the
//			  failpath_t argument is set to false.
//	EINVAL		- invalid remote node ID.
//	ECANCELED	- the pathend is already down (returned by
//			  path_manager::fault_revoke_pathend()).
//	ENOENT		- the pathend doesn't exist (returned by
//			  path_manager::fault_revoke_pathend()).
//
int
FaultFunctions::failpath(failpath_t &path)
{
	// Validate remote node ID.
	if ((path.remote_nodeid == orb_conf::local_nodeid()) ||
	    !orb_conf::node_configured(path.remote_nodeid)) {
		return (EINVAL);
	}

#if defined(_KERNEL_ORB)
	// Ensure there's at least two endpoints, unless path.force == false.
	if (! path.force &&
	    num_endpoints_funcp(path.remote_nodeid) < 2) {
		return (EAGAIN);
	}

	return (fault_revoke_pathend_funcp(path.local_adapterid,
	    path.remote_nodeid));
#else
	// We're in user-land, call the kernel version via _cladm().
	if (os::cladm(CL_CONFIG, CL_FAULT_FAILPATH, &path) != 0) {
		return (errno);
	}
	return (0);
#endif
}

//
// do_setup_wait
// Support for multiple wait_for_X types.  This function will examine the
// wait_for argument structure and do the correct setup for the
// specified wait for opcode.
void
FaultFunctions::do_setup_wait(wait_for_arg_t *wait_argp)
{
#if defined(_KERNEL_ORB)
	// If we're in the kernel or unode then we can just call setup

	nodeid_t	nodeid;

	if (wait_argp->nodeid == 0)
		nodeid = orb_conf::local_nodeid();
	else
		nodeid = wait_argp->nodeid;

	// Examine the opcode for the wait type
	switch (wait_argp->op) {
	case WAIT_FOR_UNKNOWN :
		// Set up wait for unknown
		setup_wait_for_unknown(nodeid);
		break;

	case WAIT_FOR_DEAD :
		// Set up wait for dead
		setup_wait_for_dead(nodeid);
		break;

	case WAIT_FOR_REJOIN :
		// Set up wait for rejoin
		setup_wait_for_rejoin(nodeid);
		break;

	case WAIT_FOR_LOCAL_SIGNAL :
		// Set up to wait for a local signal
		_signaled = 0;
		break;

	case NO_WAIT :
		// No wait
		break;

	default :
		// Invalid wait for opcode specified
		ASSERT(false);
		break;
	} // Switch

#else
	// We must be in userspace call down into the kernel with cladm
	if (os::cladm(CL_CONFIG, CL_FAULT_DO_SETUP_WAIT, wait_argp) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_DO_SETUP_WAIT): %s\n",
			strerror(errno));
	}
#endif	// _KERNEL_ORB
}

//
// do_wait
// Support for multiple wait_for_X types.  This function will examine the
// wait_for argument structure and do the correct wait for the
// specified wait for opcode.
void
FaultFunctions::do_wait(wait_for_arg_t *wait_argp)
{
#if defined(_KERNEL_ORB)
	// If we're in the kernel or unode then we can call the wait functions

	// Examine the opcode for the wait type
	switch (wait_argp->op) {
	case WAIT_FOR_UNKNOWN :
		wait_for_unknown();
		break;

	case WAIT_FOR_DEAD :
		wait_for_dead();
		break;

	case WAIT_FOR_REJOIN :
		wait_for_rejoin();
		break;

	case WAIT_FOR_LOCAL_SIGNAL :
		wait(0, NULL, 0);
		break;

	case NO_WAIT :
		break;

	default :
		// Invalid wait for opcode specified
		os::panic("Invalid wait opcode specified (%d)\n",
			wait_argp->op);
		break;
	} // Switch
#else
	// If we're in user-space, call down into the kernel to do the wait
	if (os::cladm(CL_CONFIG, CL_FAULT_DO_WAIT, wait_argp) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_DO_WAIT): %s\n",
			strerror(errno));
	}
#endif // _KERNEL_ORB
}

//
// Helper routine to provide counting features to fault functions.
// See comments for counter_t for more info.
//
// Returns:
//	true when the value of the counter_t structure member
//	"counter" satisfies the conditions of the "when" and "count"
//	members; false otherwise.
//
// Note: this routine modifies the counter_t structure's member
// "counter" member directly, i.e. the fault argument itself.  For an
// Invo Trigger, since the fault argument is "carried" with the
// invocation, the counter applies to all fault points with the
// same fault number along the path of the invocation, even when
// the fault points are executed on different nodes (or different
// address spaces).  Furthermore, multiple invocations will also
// share the same counter as long as the same trigger is used.
//
// For a Node Trigger, each node will have a separate copy of the
// fault argument.  The counter is not shared across nodes.
//
bool
FaultFunctions::do_counting(counter_t &cntr)
{
	cntr.counter++;

	// Examine counting information (specifies when to do the operation).
	switch (cntr.when) {
	case ALWAYS:
		// Always return true, regardless of counter.
		return (true);

	case UNTIL:
		// Return true UNTIL counter reaches count.
		return (cntr.counter < cntr.count);

	case AT:
		// Return true only when counter is AT count.
		return (cntr.counter == cntr.count);

	case AFTER:
		// Return true AFTER counter has reached count.
		return (cntr.counter > cntr.count);

	case EVERY:
		// Return true EVERYtime counter is divisible by count.
		return ((cntr.counter % cntr.count) == 0);

	case ODD:
		// Return true whenever counter is odd.
		return ((cntr.counter & 1) != 0);

	case EVEN:
		// Return true whenever counter is even.
		return ((cntr.counter & 1) == 0);

	case RANDOM:
		// Return true at random time.
		//
		// Note, we use the 'count' member as
		// the mean-time-between-failure value and the
		// 'counter' member to store the current random value.
		cntr.counter = (cntr.counter * 2416U + 374441U) % 1771875U;
		return (cntr.counter < 1771875U / cntr.count);

	default:
		ASSERT(0);
		return (false);	// in non-debug mode ASSERT above doesn't exist
	}
}

//
// Lock the fault functions cv lock
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::lock()
{
	_lock.lock();
}
#endif // _KERNEL_ORB

//
// Unlock the fault functions cv lock
#if defined(_KERNEL_ORB)
void
FaultFunctions::unlock()
{
	_lock.unlock();
}
#endif // _KERNEL_ORB

//
// set_up_wait_for_unknown
// Will set the _unknown_signaled variable down, and set the nodeid that
// we're gonna wait for
//
// Arguments:
//	Nodeid of node to wait for
#if defined(_KERNEL_ORB)
void
FaultFunctions::setup_wait_for_unknown(nodeid_t nodeid)
{
	lock();

	// Zero out the bitmap current_unknown_bitmap
	reset_wait_for_multi_unknown();

	// setup_wait_for_unknown will in turn call this internally
	setup_wait_for_multi_unknown(nodeid);

	unlock();
}
#endif // _KERNEL_ORB

//
// reset_wait_for_multi_unknown
// Will reset both bitmap wait_for_unknown_bitmap and current_unknown_bitmap
// to zero.
//
// Arguments:
//	None
#if defined(_KERNEL_ORB)
void
FaultFunctions::reset_wait_for_multi_unknown()
{
	// This bitmap used for storing the nodeid(s) which expect to
	// go unknown
	wait_for_unknown_bitmap = 0;

	// This bitmap used for storing the nodeid(s) which is/are
	// in unknown state
	current_unknown_bitmap = 0;
}
#endif // _KERNEL_ORB

//
// setup_wait_for_multi_unknown
// Add the nodeid of the node which wait for unknown to the bitmap.
//
// Arguments:
//	Nodeid of node to wait for
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::setup_wait_for_multi_unknown(nodeid_t nodeid)
{
	uint64_t nid = (uint64_t)nodeid;

	wait_for_unknown_bitmap |= (1<<(nid-1)); //lint !e647
}
#endif // _KERNEL_ORB

//
// wait_for_unknown
// Will wait until the we get signaled that this node has been marked unknown
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::wait_for_unknown()
{
	os::printf("==> th %8x: FI framework going to sleep waiting for node "
		"unknown...\n", os::threadid());

	wait_for_multi_unknown();

	os::printf("==> th %8x: FI framework detected a node unknown"
		" transition.\n", os::threadid());
}
#endif // _KERNEL_ORB

//
// Wait_for_multi_unknown
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::wait_for_multi_unknown()
{
	// Check if we've already been signaled
	lock();

	// Keep checking in a loop until two bitmaps match each other
	while (wait_for_unknown_bitmap != current_unknown_bitmap)
		_cv.wait(&_lock);

	unlock();
}
#endif // _KERNEL_ORB

//
// signal_unknown
// Entry point for the cmm to signal the we have marked this node unknown
// (This is called from the clock thread)
//
// Arguments:
//	The nodeid that we're signaling is unknown
#if defined(_KERNEL_ORB)
void
FaultFunctions::signal_unknown(nodeid_t nodeid)
{
	lock();

	uint64_t nid = (uint64_t)nodeid;

	// Update current_unknown_bitmap with the nodeid of a node which
	// become unknown and broadcast this change.
	current_unknown_bitmap |= (1<<(nid-1)); //lint !e647
	_cv.broadcast();

	unlock();
}
#endif // _KERNEL_ORB

//
// set_up_wait_for_dead
// Will set the nodeid that we're gonna wait for
//
// Arguments:
//	Nodeid of node to wait for
#if defined(_KERNEL_ORB)
void
FaultFunctions::setup_wait_for_dead(nodeid_t nodeid)
{
	lock();

	// Zero out the bitmap current_dead_bitmap
	reset_wait_for_multi_dead();

	// setup_wait_for_dead will in turn call this internally
	setup_wait_for_multi_dead(nodeid);

	unlock();
}
#endif // _KERNEL_ORB

//
// reset_wait_for_multi_dead
// Will reset both bitmap wait_for_dead_bitmap and current_dead_bitmap
// to zero.
//
// Arguments:
//	None
#if defined(_KERNEL_ORB)
void
FaultFunctions::reset_wait_for_multi_dead()
{
	int i;

	// This bitmap used for storing the nodeid(s) which expect to
	// go dead
	wait_for_dead_bitmap = 0;

	// This bitmap used for storing the nodeid(s) which is/are
	// in dead state
	current_dead_bitmap = 0;

	// Initialize the array of both nodeid and incarnation number
	for (i = 0; i < NODEID_MAX; i++) {
	    _dead_node[i].ndid = 0;
	    _dead_node[i].incn = 0;
	}
}
#endif // _KERNEL_ORB

//
// setup_wait_for_multi_dead
// Add the nodeid of the node which wait for dead to the bitmap.
//
// Arguments:
//	Nodeid of node to wait for
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::setup_wait_for_multi_dead(nodeid_t nodeid)
{
	incarnation_num incn_num = members::the().incn(nodeid);

	ASSERT(incn_num != INCN_UNKNOWN);
	ASSERT(nodeid != 0);

	// Store the nodeid and current incarnation number
	_dead_node[nodeid-1].ndid = nodeid;
	_dead_node[nodeid-1].incn = incn_num;

	uint64_t nid = (uint64_t)nodeid;
	wait_for_dead_bitmap |= (1<<(nid-1)); //lint !e647

	os::printf("wait_for_dead_bitmap is (0x%llx) so far.",
		wait_for_dead_bitmap);

}
#endif // _KERNEL_ORB

//
// wait_for_dead
// Will wait until the we get signaled that this node has been marked dead
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::wait_for_dead()
{
	os::printf("==> th %8x: FI framework going to sleep waiting for node "
		"dead... (%d)\n", os::threadid(), _dead_wf_node.ndid);

	wait_for_multi_dead();

	os::printf("==> th %8x: FI framework detected a node dead"
		" transition. (%d)\n", os::threadid(), _dead_wf_node.ndid);
}
#endif // _KERNEL_ORB

//
// wait_for_multi_dead
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::wait_for_multi_dead()
{
	os::printf("==> th %8x: FI framework going to sleep waiting for "
		"multiple nodes dead...with wait_for_dead_bitmap (0x%llx) "
		"and current_dead_bitmap (0x%llx)\n", os::threadid(),
		wait_for_dead_bitmap, current_dead_bitmap);

	lock();

	// Wait in the loop until node(s) go dead
	while (wait_for_dead_bitmap != current_dead_bitmap)
		_cv.wait(&_lock);

	unlock();

	os::printf("==> th %8x: FI framework detected multiple nodes dead"
		" transition. \n", os::threadid());
}
#endif // _KERNEL_ORB

//
// set_up_wait_for_rejoin
// Will set the the nodeid that we're gonna wait for
// This wait will wait for a node that has gone dead to come back
// (As if rebooted)
//
// Arguments:
//	Nodeid of node to wait for
#if defined(_KERNEL_ORB)
void
FaultFunctions::setup_wait_for_rejoin(nodeid_t nodeid)
{
	lock();

	// Zero out the bitmap current_rejoin_bitmap
	reset_wait_for_multi_rejoin();

	// setup_wait_for_rejoin will in turn call this internally
	setup_wait_for_multi_rejoin(nodeid);

	unlock();
}
#endif // _KERNEL_ORB

//
// reset_wait_for_multi_rejoin
// Will reset both bitmap wait_for_rejoin_bitmap and current_rejoin_bitmap
// to zero.
//
// Arguments:
//	None
#if defined(_KERNEL_ORB)
void
FaultFunctions::reset_wait_for_multi_rejoin()
{
	int i;

	// This bitmap used for storing the nodeid(s) which expect to
	// go rejoin
	wait_for_rejoin_bitmap = 0;

	// This bitmap used for storing the nodeid(s) which is/are
	// in rejoin state
	current_rejoin_bitmap = 0;

	// Initialize the array of both nodeid and incarnation number
	for (i = 0; i < NODEID_MAX; i++) {
	    _rejoin_node[i].ndid = 0;
	    _rejoin_node[i].incn = 0;
	}
}
#endif // _KERNEL_ORB

//
// setup_wait_for_multi_rejoin
// Add the nodeid of the node which wait for rejoin to the bitmap.
//
// Arguments:
//	Nodeid of node to wait for
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::setup_wait_for_multi_rejoin(nodeid_t nodeid)
{
	incarnation_num incn_num = members::the().incn(nodeid);

	ASSERT(incn_num != INCN_UNKNOWN);
	ASSERT(nodeid != 0);

	// Store the nodeid and current incarnation number
	_rejoin_node[nodeid-1].ndid = nodeid;
	_rejoin_node[nodeid-1].incn = incn_num;

	uint64_t nid = (uint64_t)nodeid;
	wait_for_rejoin_bitmap |= (1<<(nid-1)); //lint !e647

	os::printf("wait_for_rejoin_bitmap is (0x%llx) so far.",
		wait_for_rejoin_bitmap);
}
#endif // _KERNEL_ORB

//
// wait_for_rejoin
// Will wait until this node has rejoined
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::wait_for_rejoin()
{
	os::printf("==> th %8x: FI framework going to sleep waiting for node "
		"rejoin... (%d)\n", os::threadid(), _rejoin_wf_node.ndid);

	wait_for_multi_rejoin();

	os::printf("==> th %8x: FI framework detected a node rejoin"
		" transition. (%d)\n", os::threadid(), _rejoin_wf_node.ndid);
}
#endif // _KERNEL_ORB

//
// wait_for_multi_rejoin
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::wait_for_multi_rejoin()
{
	os::printf("==> th %8x: FI framework going to sleep waiting for "
		"multiple nodes rejoin...with wait_for_rejoin_bitmap (0x%llx) "
		"and current_rejoin_bitmap (0x%llx)\n", os::threadid(),
		wait_for_rejoin_bitmap, current_rejoin_bitmap);

	lock();

	// Wait in the loop until node(s) rejoin
	while (wait_for_rejoin_bitmap != current_rejoin_bitmap)
		_cv.wait(&_lock);

	unlock();

	os::printf("==> th %8x: FI framework detected multiple nodes rejoin"
		" transition. \n", os::threadid());
}
#endif // _KERNEL_ORB

//
// signal_membership_change.
// Entry point for the cmm to signal the we have marked this node alive.
// This is called everytime that there is a membership change (from the
// CMM callback).
//
// Arguments:
//	None
#if defined(_KERNEL_ORB)
void
FaultFunctions::signal_membership_change()
{
	lock();

	uint64_t tmp_bitmap, nid;

	// Check against the dead nodeid
	// Shortcircuit this if statement if the _dead_wf_node has not been
	// initialized.
	if (wait_for_dead_bitmap != 0) {
	    for (tmp_bitmap = wait_for_dead_bitmap, nid = 0; tmp_bitmap > 0;
		tmp_bitmap >>= 1, nid++) {
		if (tmp_bitmap & 1) {
		    if ((_dead_node[nid].incn != INCN_UNKNOWN) &&
			(_dead_node[nid].ndid) &&
			(!members::the().still_alive(_dead_node[nid]))) {
			current_dead_bitmap |= (1<<nid); //lint !e647
			os::printf("Node %lld just become dead.\n", nid+1);
			os::printf("current_dead_bitmap is 0x%llx now.\n",
			    current_dead_bitmap);
		    }
		}
	    }
	}

	// Check against the alive nodeid
	// Check that the node has rejoined, and has a different incn than the
	// one we registered in setup
	if (wait_for_rejoin_bitmap != 0) {
	    for (tmp_bitmap = wait_for_rejoin_bitmap, nid = 0; tmp_bitmap > 0;
		tmp_bitmap >>= 1, nid++) {
		if (tmp_bitmap & 1) {
		    if ((_rejoin_node[nid].incn != INCN_UNKNOWN) &&
			(_rejoin_node[nid].ndid) &&
			(members::the().alive(_rejoin_node[nid].ndid)) &&
			(!members::the().still_alive(_rejoin_node[nid]))) {
			current_rejoin_bitmap |= (1<<nid); //lint !e647
			os::printf("Node %lld just start to rejoin.\n", nid+1);
			os::printf("current_rejoin_bitmap is 0x%llx now.\n",
			    current_rejoin_bitmap);
		    }
		}
	    }
	}

	_cv.broadcast();
	unlock();
}
#endif // _KERNEL_ORB

//
// tsd_svc_id(replica::service_num_t sid)
//
void
FaultFunctions::tsd_svc_id(const replica::service_num_t sid)
{
	_tsd_sid_key.set_tsd((uintptr_t)sid);
}

//
// tsd_svc_id()
//
replica::service_num_t
FaultFunctions::tsd_svc_id()
{
	return ((replica::service_num_t)_tsd_sid_key.get_tsd());
}

//
// rm_primary_node()
// Will return the nodeid of the ha rm primary host
//
nodeid_t
FaultFunctions::rm_primary_node()
{
#if defined(_KERNEL_ORB)
	return (rmm_primary_node_funcp());
#else
	nodeid_t	nid;

	if (os::cladm(CL_CONFIG, CL_FAULT_RM_PRIMARY_NODE, &nid) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_RM_PRIMARY_NODE): %s\n",
			strerror(errno));
		return (NODEID_UNKNOWN);
	}
	return (nid);
#endif // _KERNEL_ORB
}

//
// switchover - This fault function is used to switchover a HA service.
//
// The switchover command can be issued from any node in the cluster.
// The fault injection framework distributes commands with datagrams.
// Datagrams are inherently asynchronous. We want the switchover command
// to be synchronous. So we always issue the switchover command locally so that
// the switchover will be synchronous.
//
// The fault_argp contains a struct with the following information:
//	nodeid		-- Node on which switchover needs to be performed
//				The nodeid is ignored.
//				XXX - should eliminate this field.
//
//	service		-- Replicated Service Name
//	provider 	-- New Primary (after switchover is done)
//
// XXX - this command does not always succeed.
// XXX - should be changed to return success/failure.
//
void
FaultFunctions::switchover(uint_t fault_num, void *fault_argp, uint32_t)
{
	ASSERT(fault_argp != NULL);

#if defined(_KERNEL_ORB)
	// The fault injection framework must not be affected by InvoTriggers
	InvoTriggers	*trigp = InvoTriggers::disconnect_triggers();

	switchover_arg_t	*switchover_argp =
				    (switchover_arg_t *)fault_argp;

	// Get the replicated service name
	char	*service_name = switchover_argp->svc_info;
	size_t	svc_len = os::strlen((const char *)service_name);

	//
	// Get the provider name (this will be the new primary for
	// the replicated service)
	//
	char	*prov_name = service_name + svc_len + 1;

	// Call the function that performs the actual switchover
	_switchover_service(service_name, prov_name);

	InvoTriggers::reconnect_triggers(trigp);
#else
	os::warning("User process ignored switchover fault message 0x%x",
	    fault_num);
#endif // _KERNEL_ORB

	// Clear the trigger
	NodeTriggers::clear(fault_num, TRIGGER_ALL_NODES);
	InvoTriggers::clear(fault_num);
}

//
// _switchover_service - performs a node switchover for the specified service.
//
// It will do the grunt work for switchover
// Arguments:
//	service		- replicated service name
//	provider	- New Primary
//
#if defined(_KERNEL_ORB)
void
FaultFunctions::_switchover_service(char *service_name, char *prov_name)
{
	ASSERT(service_name != NULL);
	ASSERT(prov_name != NULL);

	// Get the handle to the rma
	replica::rm_admin_var	rm_ref_v = get_rm_funcp();

	os::printf("Fault Fn. switchover_service: %s:%s\n",
	    service_name, prov_name);

	ASSERT(!CORBA::is_nil(rm_ref_v));

	Environment	e;

	// Get a handle to the service admin. for this service
	replica::service_admin_var	svc_admin_v =
	    rm_ref_v->get_control(service_name, e);

	if (e.exception()) {
		e.exception()->print_exception("ERROR: Fault Fn. "
		    "switchover : get_control failed\n");
		return;
	}

	replica::repl_prov_seq_var	rp_list_v;

	// Get a list of the replica providers for this service
	svc_admin_v->get_repl_provs(rp_list_v, e);
	if (e.exception()) {
		e.exception()->print_exception("ERROR: Fault Fn. "
		    "switchover: get_repl_provs failed\n");
		return;
	}

	// Find the right provider to switchover
	bool	found = false;
	for (uint32_t i = 0; i < rp_list_v->length(); i++) {

		char	*provp = rp_list_v[i].repl_prov_desc;

		if (os::strcmp(provp, prov_name) == 0) {
			found = true;
			break;
		}
	}

	if (!found) {
		os::warning("ERROR: Fault Fn. switchover: '%s' not in the "
		    "provider list", prov_name);
		return;
	}

	// Change the given provider (prov_name) to PRIMARY
	os::printf("Changing the primary for service '%s'..\n", service_name);

	svc_admin_v->change_repl_prov_status(prov_name,
	    replica::SC_SET_PRIMARY, false, e);

	if (e.exception()) {
		e.exception()->print_exception("ERROR: Fault Fn. switchover : "
		    "change_repl_prov_status \n");
	} else {
		os::printf("New primary for service '%s' is '%s'\n",
		    service_name, prov_name);
	}
}
#endif // _KERNEL_ORB


#if defined(_KERNEL_ORB) && !defined(_KERNEL)
//
//  This function is called when fault point
//  FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_1 is triggered in
//  automaton_impl::begin_state().
//  dont_ack_trinfo contains the id for xdoors translated in for an
//  earlier invocation that has not been acknowledged. This method will
//  acknowledge the id.
//
void
FaultFunctions::send_held_trinfo_ack(uint_t fault_num, void *, uint32_t)
{
	ASSERT(dont_ack_trinfo != 0);

	os::printf("Fault 0x%x: send_held_trinfo_ack: Acking held up tr_info\n",
	    fault_num, dont_ack_trinfo);
	translate_mgr::the().done_tr_in(dont_ack_node, dont_ack_trinfo);
	dont_ack_trinfo = 0;

	NodeTriggers::clear(fault_num, (int)TRIGGER_THIS_NODE);
}

#endif

#ifdef	_KERNEL_ORB
//
// Check event order.
// This fault-function is used to check if an event has occurred in
// its expected numerical order in a set of events.
// If the event has not occurred in its proper order,
// then this function panics the node signifying failure of the test case.
// A unode-process will abort with a coredump.
// Fault Argument:
//	A event_order_arg_t struct that specifies the event order set
//	to use in the event_order_sets array, and the order of
//	a particular event to test.
//
// It is obvious that this kind of fault function has to have
// a once-only semantics.
// If it is being checked that event E is happening in its proper order,
// then a repeat of event E cannot be checked for its proper order
// using the same event order set without resetting the set value.
// So it is implicit that we turn off the fault trigger
// before checking the event order in this function.
// Note that such a fault trigger is always a NodeTrigger.
//
void
FaultFunctions::check_event_order(uint_t fault_num, void *fault_argp,
		uint32_t fault_arglen)
{
	ASSERT(fault_argp != NULL);
	ASSERT(fault_arglen == sizeof (wait_for_arg_t));

	// Extract the wait_for_arg_t structure from the fault argument
	event_order_arg_t	*ev_order_argp;
	ev_order_argp = (event_order_arg_t *)fault_argp;

	// Extract out the required data from the fault argument
	int set = ev_order_argp->index_into_event_order_sets;
	int order = ev_order_argp->order_of_event;

	//
	// Now that we have extracted the data from the fault args,
	// clear the fault trigger.
	//
	NodeTriggers::clear(fault_num, (int)TRIGGER_THIS_NODE);
	InvoTriggers::clear(fault_num);

	// Do the event order checking
	ASSERT(event_order_sets[set] >= 0);
	event_order_sets[set] += 1;
	if (event_order_sets[set] != order) {
		// Out of order event
		os::panic("Node panic due to out of order event occurrence : "
		    "injected fault 0x%x on node %d, set = %d, "
		    "set value = %d, order = %d\n",
		    fault_num, orb_conf::local_nodeid(),
		    set, event_order_sets[set], order);
	}
}
#else
void
FaultFunctions::check_event_order(uint_t, void *, uint32_t)
{
	// This feature is not available in non-unode userland
	os::panic("check_event_order fault function does not exist!");
}
#endif

#ifdef	_KERNEL_ORB
//
// Reset a set (specified by index) in the event order sets to 0.
// If index is -1, then reset all sets in the event order sets array to 0.
//
void
FaultFunctions::reset_event_order_set(int index)
{
	if (index == -1) {
		for (uint_t i = 0; i < MAX_EVENT_ORDER_SETS; i++) {
			event_order_sets[i] = 0;
		}
	} else {
		ASSERT(index >= 0);
		ASSERT(index < MAX_EVENT_ORDER_SETS);
		event_order_sets[index] = 0;
	}
}
#else
void
FaultFunctions::reset_event_order_set(int)
{
	// This feature is not available in non-unode userland
	os::panic("reset_event_order_set fault function does not exist!");
}
#endif

#if defined(_KERNEL_ORB)
void
FaultFunctions::generic_this_svc(uint32_t fault_num, uint32_t svc_num,
    void (*fault_fp)(uint32_t, void *, uint32_t))
{
	// Allocate for generic_arg_t type
	void	    *fault_argp;
	uint32_t	fault_argsize;

	// os::printf("FaultFunctions::generic_this_svc\n");

	if (fault_triggered(fault_num, &fault_argp, &fault_argsize)) {

		// Expect to receive a generic_arg_t type from the trigger
		ASSERT(fault_argp != NULL);
		// ASSERT(fault_argsize == sizeof (generic_arg_t));

		generic_arg_t *f_argp = (generic_arg_t*) fault_argp;

		// Extract the data part of generic_arg_t type and store
		// it into a pointer of svc_reboot_arg_t type
		svc_reboot_arg_t *argp = (svc_reboot_arg_t *)(f_argp->data);

		// Expect the size of a svc_reboot_arg_t type from the
		// data length
		// ASSERT((f_argp->data_length) == sizeof (svc_reboot_arg_t));

		// os::printf("nodeid from svc_reboot_arg_t is %d\n",
		//	argp->nodeid);
		// os::printf("op from svc_reboot_arg_t is %d\n", argp->op);
		// os::printf("sid from svc_reboot_arg_t is %d\n", argp->sid);
		// os::printf("sid from pass-in arg is %d\n", svc_num);

		if (argp->sid == svc_num) {
			(*fault_fp)(fault_num, fault_argp, fault_argsize);
		}
	}
}
#endif // _KERNEL_ORB
