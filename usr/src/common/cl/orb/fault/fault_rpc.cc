//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fault_rpc.cc	1.47	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/flow/resource.h>
#include <orb/fault/fault_injection.h>
#include <orb/fault/fault_functions.h>
#include <orb/fault/fault_rpc.h>
#include <orb/msg/orb_msg.h>
#include <sys/cl_comm_support.h>

#if defined(_KERNEL) || !defined(_KERNEL_ORB)
#include <sys/cladm_int.h>
#endif

#if defined(_KERNEL_ORB)
#ifndef _KERNEL
#include <unode/systm.h>
#endif
#include <sys/cladm_int.h>
#include <orb/member/members.h>
#include <orb/transport/nil_endpoint.h>
#endif

//
// List of fault injection rpc handlers.
// NOTE: The order of handler routines below must match fault_rpc::opcode_t.
//
#if defined(_KERNEL_ORB)
orb_msg::message_handler fault_rpc_handler[fault_rpc::N_HANDLERS] = {
	BootTriggers::add_rpc_handler,
	NodeTriggers::add_rpc_handler,
	NodeTriggers::clear_rpc_handler,
	NodeTriggers::clear_all_rpc_handler,
	FaultFunctions::panic_rpc_handler,
	FaultFunctions::reboot_rpc_handler,
	FaultFunctions::reboot_clean_rpc_handler
};
#endif

//
// is_fault_msg
//
// Returns true of the messagetype is FAULT_MSG
//
bool
fault_rpc::is_fault_msg(orb_msgtype msgt)
{
	return (msgt == FAULT_MSG);
}

//
// send_fault
//
// Transmit fault number to destination node
//
// Returns true when messsage successfully transmitted.
//
// static
bool
fault_rpc::send_fault(opcode_t opcode, nodeid_t nodeid, uint32_t fault_num)
{
	return (fault_rpc::send_fault_arg(opcode, nodeid, fault_num, NULL, 0));
}

//
// send_fault_arg
//
// Transmit fault number plus fault information to destination node. It will
// retry when there is a RETRY_NEEDED exception.
//
// Returns true when messsage successfully transmitted.
//
// static
bool
fault_rpc::send_fault_arg(opcode_t opcode, nodeid_t nodeid, uint32_t fault_num,
    void *argp, uint32_t size)
{
	Environment	e;
	bool result;
#if defined(_KERNEL_ORB)
	ID_node		node(nodeid, members::the().incn(nodeid));
	uint32_t	header_size = (uint32_t)sizeof (fault_header_t);
#else
	uint32_t	header_size = (uint32_t)sizeof (cladm_header_t);
#endif
	uint32_t	data_size = (2 * (uint32_t)sizeof (uint32_t)) + size;

	// Only need to retry if we are in kernel. If we are in usrland
	// the gateway handler code will retry for us.
#if defined(_KERNEL_ORB)
	bool retry_needed;
	do {
		retry_needed = false;

		resources_datagram	resource(&e, header_size, data_size,
					    node, FAULT_MSG);
#else // _KERNEL_ORB
		resources_datagram	resource(&e, header_size, data_size,
					    FAULT_MSG);
#endif // _KERNEL_ORB

		sendstream *sendstreamp = orb_msg::reserve_resources(&resource);

		if (e.exception()) {
			// Fault Injection framework simply drops the message
			// when it encounters an error
			e.clear();
			return (false);
		}
		// Marshal the fault number and associated fault information
		sendstreamp->put_unsigned_long(fault_num);
		sendstreamp->put_unsigned_long(size);
		if (size > 0) {
			sendstreamp->put_bytes(argp, size);
		}

		result = send_message(opcode, nodeid, sendstreamp);

		// Retry code.
#if defined(_KERNEL_ORB)
		if (!result) {
			// Result is false if there is an exception.
			ASSERT(e.exception());
			if (CORBA::RETRY_NEEDED::_exnarrow(e.exception())) {
				retry_needed = true;
			}
			e.clear();
		}

	} while (retry_needed);
#endif // _KERNEL_ORB

	e.clear();
	return (result);
}

//
// send_message
//
//	Sends a message to a remote node (one-way message).
//
// Parameters:
//	se	-- pointer to sendstream.
//
// Returns: true when message transmitted without error
//
// static
bool
fault_rpc::send_message(opcode_t opcode, nodeid_t nodeid, sendstream *se)
{
	bool		result;
#if defined(_KERNEL_ORB)
	fault_header_t header((uint32_t)opcode);
	se->write_header((void *)&header, sizeof (fault_header_t));

	if (nodeid == orb_conf::node_number()) {
		// Message is for this node.
		// Convert to nil_recstream so we can call the msg handler.
		nil_recstream	*re = make_recstream_funcp((nil_sendstream*)se);

		// Call the message handler directly.
		handle_messages(re);
	} else {
		orb_msg::send(se);
	}
	Environment	*e = se->get_env();
	if (e->exception()) {
		result = false;
	} else {
		result = true;
	}
#else
	// Ask kernel to do the fault rpc for us using _cladm().
	// XXX	This could go away if sdoor_transport and the gateway
	//	support channels.
	cladm_header_t	*headerp;
	Buf		*bufp;
	// Need to calculate span before doing make_header
	uint_t		spn = se->get_MainBuf().span();

	headerp = (cladm_header_t *)se->make_header(sizeof (cladm_header_t));
	headerp->opcode = (uint32_t)opcode;
	headerp->nodeid = nodeid;
	headerp->datasize = spn;

	// Gather all data and copy into one buffer.
	bufp = se->get_MainBuf().coalesce_region().reap();

	// Send message to the kernel.
	if (os::cladm(CL_CONFIG, CL_FAULT_RPC, bufp->head()) != 0) {
		os::warning("_cladm(CL_CONFIG, CL_FAULT_RPC): %s\n",
		    strerror(errno));
		result = false;
	} else {
		result = true;
	}

	bufp->done();
#endif // _KERNEL_ORB

	se->done();
	return (result);
}

//
// fault_rpc::handle_messages()
//
//	Message handler routine called by the transport when it receives
//	a fault injection rpc message
//
// Parameters:
//	re	-- pointer to the receive stream.
//
// Returns:
//	None.
//
#if defined(_KERNEL_ORB)
void
fault_rpc::handle_messages(recstream *re)
{
	fault_header_t	header;
	re->read_header((void *)&header, sizeof (fault_header_t));

	ASSERT(header.opcode <= N_HANDLERS);
	ASSERT(fault_rpc_handler[header.opcode] != NULL);

	// Call the fault injection rpc handler.
	(*fault_rpc_handler[header.opcode])(re);

	re->done();
}
#endif

//
// _cladm() handler routine to allow fault injection rpc from user land.
//
#if defined(_KERNEL) || defined(_KERNEL_ORB)
int
cl_fault_injection(int cmd, void *argp)
{
	int	err;

	switch (cmd) {
	case CL_FAULT_RPC: {
		// Send fault rpc msgs on behalf of user processes.
		fault_rpc::cladm_header_t clhead;

		err = xcopyin(argp, &clhead, sizeof (clhead));
		if (err != 0) {
			os::warning("xcopyin error for CL_FAULT_RPC: %d", err);
			return (err);
		}

		Environment	e;
		ID_node		node(clhead.nodeid,
		    members::the().incn(clhead.nodeid));
		uint32_t	header_size =
		    (uint32_t)sizeof (fault_rpc::fault_header_t);

		// Determine message resources needed by caller.
		resources_datagram	resource(&e, header_size,
					    clhead.datasize, node, FAULT_MSG);

		sendstream	*sendstreamp;

		sendstreamp = orb_msg::reserve_resources(&resource);
		if (e.exception()) {
			// Fault Injection framework simply drops the message
			// when it encounters an error
			e.clear();
			break;
		}
		// Marshal any data provided along with the header
		if (clhead.datasize > 0) {
			err = sendstreamp->put_u_bytes(
			    (caddr_t)argp + sizeof (clhead),
			    clhead.datasize, false);
			if (err != 0) {
				sendstreamp->done();
				return (err);
			}
		}
		(void) fault_rpc::send_message((fault_rpc::opcode_t)clhead.
		    opcode, clhead.nodeid, sendstreamp);
		break;
	    }

	case CL_FAULT_GET_NODETRIG:
		// For user programs to get a NodeTrigger from kernel table.
		return (NodeTriggers::the().get_nodetrig(argp));

	case CL_FAULT_DO_SETUP_WAIT: {
		// System call to setup the wait environment on the kernel
		// The argument should be the wait_for_arg_t
		FaultFunctions::wait_for_arg_t	wait_arg;

		err = xcopyin(argp, &wait_arg, sizeof (wait_arg));
		if (err != 0) {
			os::warning("xcopyin error for "
			    "CL_FAULT_DO_SETUP_WAIT: %d", err);
			return (err);
		}

		FaultFunctions::do_setup_wait(&wait_arg);
		break;
	    }

	case CL_FAULT_DO_WAIT: {
		// System call to the do_wait on the kernel
		// The argument should be the wait_for_arg_t
		FaultFunctions::wait_for_arg_t	wait_arg;

		err = xcopyin(argp, &wait_arg, sizeof (wait_arg));
		if (err != 0) {
			os::warning("xcopyin error for CL_FAULT_DO_WAIT: %d",
			    err);
			return (err);
		}

		FaultFunctions::do_wait(&wait_arg);
		break;
	    }

	case CL_FAULT_WAIT: {
		// System call to the FaultFunctions::wait on the kernel
		// the only important argument is the fault_num
		uint32_t	fault_num;

		err = xcopyin(argp, &fault_num, sizeof (fault_num));
		if (err != 0) {
			os::warning("xcopyin error for CL_FAULT_WAIT: %d", err);
			return (err);
		}

		FaultFunctions::wait(fault_num, NULL, 0);
		break;
	    }

	case CL_FAULT_SIGNAL: {
		// System call to the FaultFunctions::signal on the kernel
		// the only important argument is the fault_num
		uint32_t	fault_num;

		err = xcopyin(argp, &fault_num, sizeof (fault_num));
		if (err != 0) {
			os::warning("xcopyin error for CL_FAULT_SIGNAL: %d",
			    err);
			return (err);
		}

		FaultFunctions::signal(fault_num, NULL, 0);
		break;
	    }

	case CL_FAULT_REBOOT_ALL_NODES:
		// System call to the FaultFunctions::reboot_all_nodes on
		// the kernel -- no arguments are needed
		FaultFunctions::reboot_all_nodes();
		break;

	case CL_FAULT_RM_PRIMARY_NODE: {
		// System call to the rmm interface to get the
		// nodeid of the primary ha-rm host
		nodeid_t	nid = FaultFunctions::rm_primary_node();

		err = xcopyout(&nid, argp, sizeof (nid));
		if (err != 0) {
			os::warning("xcopyout error for "
			    "CL_FAULT_RM_PRIMARY_NODE: %d", err);
			return (err);
		}

		break;
	    }

	case CL_FAULT_FAILPATH: {
		// Interface between user- and kernel-level
		// FaultFunctions::failpath().
		FaultFunctions::failpath_t	path;

		err = xcopyin(argp, &path, sizeof (path));
		if (err != 0) {
			os::warning("xcopyin error for CL_FAULT_FAILPATH: %d",
			    err);
			return (err);
		}
		return (FaultFunctions::failpath(path));
	    }

	default:
		return (EINVAL);
	}

	return (0);
}
#endif // _KERNEL
