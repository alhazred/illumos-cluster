/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _FAULT_UNREF_H
#define	_FAULT_UNREF_H

#pragma ident	"@(#)fault_unref.h	1.9	08/05/20 SMI"

//
// Used by src/orbtest/unref_fi tests. Refer to
// src/fault_numbers/faultnum_unref.h
//

#if defined(_FAULT_INJECTION)


#include <orb/invo/common.h>
#include <orb/handler/handler.h>
#include <orb/handler/standard_handler.h>

//
// fault_unref class which provides some static methods for unref_fi
//

class fault_unref {
public:

	//
	// This is used to identify which operation to take when a certain
	// faultpoint is set.
	//
	enum op_t {
		// for setup
		CREATE_NEW_REF,
		WAIT_CREATE_NEW_REF,

		// for test functions
		REGOBJ_UNREF_PATH_1,
		REGOBJ_UNREF_PATH_2,
		REGOBJ_UNREF_PATH_3,
		REGOBJ_UNREF_PATH_4,
		REGOBJ_UNREF_PATH_5,
		REGOBJ_UNREF_PATH_6,
		REGOBJ_UNREF_PATH_7
	};

	// Argument for unref_fi trigger
	struct unref_arg_t {
		// How many new references that need to be created for the test
		int			numref;

		// Which operation to take
		op_t			op;
	};

	//
	// UNREF Fault function, used to trigger unref tests
	//
	static void	faultpt_unref(uint_t fault_num,
			    standard_handler_server *std_handlerp,
			    void (*fp)(standard_handler_server *,
			    uint_t fault_num, void *fault_argp,
			    uint32_t argsize));

	//
	// Generic test function which calls another test function based on
	// the operation..
	//
	static void	generic_test_op(standard_handler_server *std_handlerp,
			    uint_t fault_num, void *fault_argp,
			    uint32_t argsize);

private:
	// Disallow making instances of this class.
	fault_unref();
	~fault_unref();

	// Disallow these methods to be called from outside of the class

	// verify current handler state is the same as the expected one
	static void	verify_handler_unref_state(
			    handler::handler_states_t cur_state,
			    handler::handler_states_t exp_state);

	//
	// New how many references and release how many references.
	//
	static void	regobj_new_ref(standard_handler_server *, int howmany);
	static void	regobj_new_ref_wait(standard_handler_server *,
			    int howmany);

	static void	regobj_rel_ref(standard_handler_server *, int howmany);

	// Regobj

	//
	// The following functions are real test functions that will force the
	// test unrefernce to go to the certain path. They are called by
	// above functions.
	// Note:
	//	it takes 2 argument, 1 is pointer to standard_handler_server,
	//	2 is how many unreferences need to be created or released.
	//
	static void	regobj_unref_path_1(standard_handler_server *, int);
	static void	regobj_unref_path_2(standard_handler_server *, int);
	static void	regobj_unref_path_3(standard_handler_server *, int);
	static void	regobj_unref_path_4(standard_handler_server *, int);

	static void	regobj_unref_path_5(standard_handler_server *, int);
	static void	regobj_unref_path_6(standard_handler_server *, int);
	static void	regobj_unref_path_7(standard_handler_server *, int);
	//
	// HA obj
	// Note: Not yet available.
	//

	//
	// Used to synchronize test methods.
	//
	static os::mutex_t	wait_mutex;
	static os::condvar_t	wait_cv;
	static bool		signalled;
};

#endif // _FAULT_INJECTION

#endif	/* _FAULT_UNREF_H */
