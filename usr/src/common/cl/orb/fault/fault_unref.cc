//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

//
// implementation of fault_unref class. Used by unref_fi tests.
//

#pragma ident   "@(#)fault_unref.cc 1.18     08/05/20 SMI"

#include <sys/os.h>
#include <orb/fault/fault_unref.h>
#include <orb/fault/fault_injection.h>

handler *test_obj_handlerp = NULL;

os::mutex_t	fault_unref::wait_mutex;
os::condvar_t	fault_unref::wait_cv;
bool		fault_unref::signalled = false;

//
// FAULT function
// Note: This function is a general wrapper function. It takes the fault_num,
// the standard_handler_server pointer and the function pointer as parameters.
// What it does is to check if the passed fault_num is triggered, if so,
// it will call the function(fp).
//
void
fault_unref::faultpt_unref(
    uint_t fault_num,
    standard_handler_server *std_handlerp,
    void (*fp)(standard_handler_server *, uint_t, void *, uint32_t))
{
	void		*argp;
	uint32_t	argsize;

	if (fault_triggered(fault_num, &argp, &argsize)) {
		(*fp)(std_handlerp, fault_num, argp, argsize);
	}
}

//
// Generic test function. Calls another test function based on the
// operation (enum generic_op_t). It will first check if the current obj
// is the same one as the one which setup this trigger. If not, it will
// return right away.
//
void
fault_unref::generic_test_op(standard_handler_server *std_handlerp,
	uint_t, void *argp, uint32_t argsize)
{
	//
	// Check if test_obj_handlerp has been created. The fault point
	// is relevant only if the test object exists.
	//
	if (test_obj_handlerp == NULL) {
		return;
	}

	ASSERT(argp != NULL);
	ASSERT(argsize == sizeof (unref_arg_t));

	unref_arg_t	*unref_argp = (unref_arg_t *)argp;

	//
	// check if the current object is the testobj. if not, return.
	//
	if (std_handlerp != test_obj_handlerp) {
		return;
	}
	//
	// How many references need to be created or released. carried by
	// unref_argp.
	//
	int	how_many = unref_argp->numref;

	ASSERT(how_many > 0);

	switch (unref_argp->op) {
	case CREATE_NEW_REF:
		regobj_new_ref(std_handlerp, how_many);
		break;
	case WAIT_CREATE_NEW_REF:
		regobj_new_ref_wait(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_1:
		regobj_unref_path_1(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_2:
		regobj_unref_path_2(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_3:
		regobj_unref_path_3(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_4:
		regobj_unref_path_4(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_5:
		regobj_unref_path_5(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_6:
		regobj_unref_path_6(std_handlerp, how_many);
		break;
	case REGOBJ_UNREF_PATH_7:
		regobj_unref_path_7(std_handlerp, how_many);
		break;
	default:
		ASSERT(0);
	}
}

//
// verify if the handler state, which is passed by argument 1, is the same
// as the expected state which is passed by argument 2. If not, set a global
// trigger to report test failure so that client will be able to check
// test result later on. Nothing need to be done if the states are identical.
//
void
fault_unref::verify_handler_unref_state(
    handler::handler_states_t cur_state,
    handler::handler_states_t exp_state)
{
	if (cur_state != exp_state) {
		os::printf("ERROR: fault_unref::verify_handler_unref_state(): "
		    "Current handler state is %s instead of expected "
		    "state %s.\n",
		    handler::handler_state_t::handler_state_2_string(
		    cur_state),
		    handler::handler_state_t::handler_state_2_string(
		    exp_state));

		// Add a trigger to report failure
		NodeTriggers::add(FAULTNUM_UNREF_FAIL, NULL, 0,
		    TRIGGER_ALL_NODES);
	}
}

//
// Used to release create how many references of the object passed by
// std_handlerp.
//
void
fault_unref::regobj_new_ref(standard_handler_server *std_handlerp,
    int how_many)
{
	for (int i = how_many; i > 0; i--) {
		std_handlerp->new_reference();
	}
	// os::printf("%p regobj_new_ref %d \n", std_handlerp, how_many);
}

//
// Same as regobj_new_ref, except waits for signal before it creates
// references.
//
void
fault_unref::regobj_new_ref_wait(standard_handler_server *std_handlerp,
    int how_many)
{
	wait_mutex.lock();
	while (!signalled) {
		wait_cv.wait(&wait_mutex);
	}

	for (int i = how_many; i > 0; i--) {
		std_handlerp->new_reference();
	}

	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_WAIT_DEL_UNREF);

	signalled = false;
	wait_mutex.unlock();
	// os::printf("%p regobj_new_ref_wait %d \n", std_handlerp, how_many);
}

//
// Used to release how many references of the object passed by std_handlerp.
//
void
fault_unref::regobj_rel_ref(standard_handler_server *std_handlerp,
    int how_many)
{
	for (int i = how_many; i > 0; i--) {
		CORBA::release(std_handlerp->_obj());
	}
	// os::printf("%p regobj_rel_ref %d \n", std_handlerp, how_many);
}

//
// regular orb object unreference tests
//

//
// The following are test methods for covering different paths.
//
// When one of the unref faults is triggered. if the current object
// is the test object, it will try to force it to go to the designed
// unreference path by creating some new references and releasing
// references at different places. For more information about different
// unreference paths, please refer to src/fault_numbers/faultnum_unref.h
//

// Test 1: regobj unref path 1
void
fault_unref::regobj_unref_path_1(standard_handler_server *std_handlerp,
    int how_many)
{
	// os::printf("%p regobj_unref_path_1 %d \n", std_handlerp, how_many);
	//
	// Verify that if the current handler state is the one we are expecting
	// to see at this point.
	//
	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_UNSCHEDULE);
	//
	// After releasing how many references, the handler state will go back
	// to the original place where we triggered the tests, if we don't
	// clear the test trigger, it is possible for the test going to a loop.
	// Clearing trigger also includes clearing the generic trigger.
	//
	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_1);
	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_DELIVER_UNREFERENCED);

	// Release how many references which are created during the test setup.
	regobj_rel_ref(std_handlerp, how_many);

	// Verify if the current handler state is the one we expected.
	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_SCHEDULED);
}

// Test 2: regobj unref path 2
void
fault_unref::regobj_unref_path_2(standard_handler_server *std_handlerp,
    int how_many)
{
	// os::printf("%p regobj_unref_path_2 %d \n", std_handlerp, how_many);
	// Verify if the current handler state is the one we expected.
	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::ACTIVE_HANDLER);

	//
	// Clear test trigger to prevent the test being re-triggered after
	// releasing how many references.
	//
	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_2);
	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_DELIVER_UNREFERENCED);

	regobj_rel_ref(std_handlerp, how_many);

}

// Test 3: regobj unref path 3
void
fault_unref::regobj_unref_path_3(standard_handler_server *std_handlerp,
    int how_many)
{
	// os::printf("%p regobj_unref_path_3 %d \n", std_handlerp, how_many);
	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_NO_PROCESS);

	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_3);
	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_LAST_UNREF);

	regobj_rel_ref(std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_PROCESS);
}

// Test 4: regobj unref path 4
void
fault_unref::regobj_unref_path_4(standard_handler_server *std_handlerp,
    int how_many)
{
	// os::printf("%p regobj_unref_path_4 %d \n", std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::ACTIVE_HANDLER);

	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_4);
	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_LAST_UNREF);
	regobj_rel_ref(std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_SCHEDULED);
}

//
// Test 5: regobj_unref_path_5
//
//  Tests handler states in end_xdoor_unref().
//
void
fault_unref::regobj_unref_path_5(standard_handler_server *std_handlerp,
    int how_many)
{
	// os::printf("%p regobj_unref_path_5 %d \n", std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::ACTIVE_HANDLER);

	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF);
	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_5);

	regobj_rel_ref(std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_SCHEDULED);
}

//
// Test 6: regobj_unref_path_6
//
//  Tests handler states in deliver_unreferenced(). Uses test path 5
//  above to get handler to desired state at beginning of the test.
//  FAULTNUM_UNREF_REGOBJ_PATH_6 is triggered twice resulting in this
//  method also being called twice. First time it sets triggers
//  deliver_unreferenced(). Second time it releases reference that
//  will result indeliver_unreferenced() being called.
//
void
fault_unref::regobj_unref_path_6(standard_handler_server *std_handlerp,
    int how_many)
{
	static bool first_time = true;

	if (first_time) {
		//  os::printf("%p regobj_unref_path_6 %d: setting triggers"
		//	" in deliver_unreferenced() \n", std_handlerp,
		//  	how_many);
		//
		// Triggers FAULTNUM_UNREF_REGOBJ_PATH_1.
		// deliver_unreference() method. Releasing reference
		// will put the handler on the unref_threadpool.
		//
		fault_unref::unref_arg_t	arg, g_arg;

		arg.numref = g_arg.numref = how_many;
		arg.op = fault_unref::REGOBJ_UNREF_PATH_1;
		g_arg.op = fault_unref::WAIT_CREATE_NEW_REF;

		NodeTriggers::add(FAULTNUM_UNREF_SETUP_WAIT_DEL_UNREF, &g_arg,
		    (uint32_t)sizeof (g_arg));

		NodeTriggers::add(FAULTNUM_UNREF_REGOBJ_PATH_1,
		    &arg, (uint32_t)sizeof (arg));

		first_time = false;
		return;
	}

	// os::printf("%p regobj_unref_path_6 %d \n", std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::ACTIVE_HANDLER);

	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF);
	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_6);

	regobj_rel_ref(std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_SCHEDULED);
	//
	// Reset first_time. This is required so that when this method is
	// called for a new test, first_time=false and it will set triggers.
	//
	first_time = true;

	//
	// This test is setup such that deliver-unreferenced() will
	// wait until we signal it. This is to impose an order on
	// creation and release of references in
	// deliver_unreferenced() method.
	//
	wait_mutex.lock();
	signalled = true;
	wait_cv.signal();
	wait_mutex.unlock();
}

//
// Test 7: regobj_unref_path_7
//
//  Tests handler states in deliver_unreferenced(). Uses test path 5
//  above to get handler to desired state at beginning of the test.
//
void
fault_unref::regobj_unref_path_7(standard_handler_server *std_handlerp,
    int how_many)
{
	static bool first_time = true;

	if (first_time) {
		//  os::printf("%p regobj_unref_path_7 %d: setting triggers"
		//  	" in deliver_unreferenced() \n", std_handlerp,
		//  	how_many);
		//
		// Trigger FAULTNUM_UNREF_REGOBJ_PATH_2
		// deliver_unreference() method. Releasing reference
		// will put the handler on the unref_threadpool.
		//
		fault_unref::unref_arg_t	arg, g_arg;

		arg.numref = g_arg.numref = how_many;
		arg.op = fault_unref::REGOBJ_UNREF_PATH_2;
		g_arg.op = fault_unref::WAIT_CREATE_NEW_REF;

		NodeTriggers::add(FAULTNUM_UNREF_SETUP_WAIT_DEL_UNREF, &g_arg,
		    (uint32_t)sizeof (g_arg));

		NodeTriggers::add(FAULTNUM_UNREF_REGOBJ_PATH_2,
		    &arg, (uint32_t)sizeof (arg));

		first_time = false;
		return;
	}

	// os::printf("%p regobj_unref_path_7 %d \n", std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::ACTIVE_HANDLER);

	NodeTriggers::clear(FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF);
	NodeTriggers::clear(FAULTNUM_UNREF_REGOBJ_PATH_7);

	regobj_rel_ref(std_handlerp, how_many);

	verify_handler_unref_state(std_handlerp->state_.get(),
	    handler::UNREF_SCHEDULED);
	//
	// Reset first_time. This is required so that when this method is
	// called for a new test, first_time=false and it will set triggers.
	//
	first_time = true;

	//
	// This test is setup such that deliver-unreferenced() will
	// wait until we signal it. This is to impose an order on
	// creation and release of references in
	// deliver_unreferenced() method.
	//
	wait_mutex.lock();
	signalled = true;
	wait_cv.signal();
	wait_mutex.unlock();
}
