//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

//
// implementation of fault_flowcontrol class. Used by flowcontrol_fi tests.
//

#pragma ident   "@(#)fault_flowcontrol.cc 1.21     08/05/20 SMI"

#include <sys/os.h>
#include <orb/fault/fault_flowcontrol.h>
#include <orb/fault/fault_injection.h>
#include <orb/flow/resource_balancer.h>
#include <h/flowcontrol_fi.h>

//
// Generic test function. Calls another test function based on the
// operation (enum generic_op_t).
//
void
fault_flowcontrol::generic_test_op(uint_t, void *argp, uint32_t argsize)
{
	ASSERT(argp != NULL);
	ASSERT(argsize == sizeof (flowcontrol_arg_t));

	flowcontrol_arg_t	*flowcontrol_argp = (flowcontrol_arg_t *)argp;

	switch (flowcontrol_argp->op) {
	case RESOURCE_ALLOCATOR_1:
		flowcontrol_resource_allocator_1(flowcontrol_argp);
		break;
	case CHECK_FOR_RESOURCE_CHANGES_1:
		flowcontrol_check_for_resource_changes_1();
		break;
	case PROCESS_REQUEST_RESOURCES_1:
		flowcontrol_process_request_resources_1();
		break;
	case SIGNAL_RELEASE:
		flowcontrol_signal_release(flowcontrol_argp);
		break;
	case SIGNAL_STOP:
		flowcontrol_signal_stop(flowcontrol_argp);
		break;
	case SIGNAL_BOTH:
		flowcontrol_signal_release(flowcontrol_argp);
		flowcontrol_signal_stop(flowcontrol_argp);
		break;
	case PROCESS_MESSAGE:
		flowcontrol_process_message(flowcontrol_argp);
		break;
	case LOOK_FOR_THREAD:
		flowcontrol_look_for_thread(flowcontrol_argp);
		break;
	default:
		ASSERT(0);
	}
}

//
// fault_flowcontrol::flowcontrol_resource_allocator_1
// Called in the "normal" invocation case, when there are resources
// available. It adds a trigger for SIGNAL_RESOURCE_ALLOCATOR_1
// which the test program sees and verifies that the system has
// correctly hit this path.
//
void
fault_flowcontrol::flowcontrol_resource_allocator_1(flowcontrol_arg_t *)
{
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_RESOURCE_ALLOCATOR_1,
	    NULL, 0);
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_RESOURCE_ALLOCATOR_1);
}

//
// fault_flowcontrol::flowcontrol_check_for_resource_changes_1
// This function is called when testing for resource release. If
// a node has threads_free > threads_min + threads_increment, it will
// hit this code path in resource_balancer::check_for_resource_changes.
// This triggers the fault point which the test program checks to see
// that the resources have indeed been released.
//
void
fault_flowcontrol::flowcontrol_check_for_resource_changes_1()
{
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_C_RESOURCE_CHANGES_1,
	    NULL, 0);
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_CHECK_FOR_RESOURCE_CHANGES_1);
}

//
// fault_flowcontrol::flowcontrol_process_request_resources_1
// This is called when a node joins the cluster and requests resources
// when it does not have the low value of resources. This signals
// that the code path has been hit.
//
void
fault_flowcontrol::flowcontrol_process_request_resources_1()
{
	//
	// This fault point is hit on the server, but we want to know from
	// the client, so add the trigger on all nodes.
	//
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_S_PRR_1, NULL, 0,
	    TRIGGER_ALL_NODES);
	NodeTriggers::clear(FAULTNUM_FLOWCONTROL_PROCESS_REQUEST_RESOURCES_1);
}

//
// fault_flowcontrol::flowcontrol_signal_release
// The function is called whenever something has happened that causes
// us to want to release blocked invocations. The invocations are
// doing a cv_timed_wait, and periodically check to see if the
// SIGNAL_RELEASE fault point is triggered. When it is, they return.
//
void
fault_flowcontrol::flowcontrol_signal_release(flowcontrol_arg_t *argp)
{
	//
	// This function is used for signaling release on both potential
	// condition variables used in the flowcontrol server. Currently,
	// there are only two, specified by the cv_num argument.
	//
	unsigned int fnum = 0;
	if (argp->cv_num == 2) {
		fnum = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE_2;
	} else {
		fnum = FAULTNUM_FLOWCONTROL_SIGNAL_RELEASE;
	}

	NodeTriggers::add(fnum, NULL, 0, TRIGGER_ALL_NODES);
}

//
// fault_flowcontrol::flowcontrol_signal_stop
// This function is called when we are forking threads to exhaust
// resources. This triggers the SIGNAL_STOP fault, telling the test to
// stop forking threads.
//
void
fault_flowcontrol::flowcontrol_signal_stop(flowcontrol_arg_t*)
{
	NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_STOP, NULL,
	    0, TRIGGER_ALL_NODES);
}

//
// flowcontrol_process_message
// This function is always called from the fault point in orb_msg::
// process_message. It is called when the thread which will process this
// invocation has taken the invocation out of the threadpool, and has
// just unpacked the InvoTriggers information.
// This will trigger the same fault point, but with a different fault
// argument. The argument will contain the threadid_t of the current
// thread. The next time this thread goes through process_message, it
// will hit the fault point.
void
fault_flowcontrol::flowcontrol_process_message(flowcontrol_arg_t*)
{
	// Clear the trigger that got us here
	InvoTriggers::clear(FAULTNUM_FLOWCONTROL_PROCESS_MESSAGE);

	flowcontrol_arg_t next_argp;  // The arg to pass to look_for_thread

	// Give it the id of the current thread
	next_argp.op = LOOK_FOR_THREAD;
	next_argp.threadid = os::threadid();

	NodeTriggers::add(FAULTNUM_FLOWCONTROL_PROCESS_MESSAGE,
	    &next_argp, (uint32_t)sizeof (next_argp));
}

//
// flowcontrol_look_for_thread
// This function is hit when the PROCESS_MESSAGE fault point
// is triggered by flowcontrol_process_message. Therefore,
// the fault argument should have a threadid set. This function
// compares the current threadid to the arg threadid. If they
// are the same, the thread has been successfully reused and
// we should signal to release the other threads. If not, we
// do nothing, and wait for the fault point to be hit again.
//
void
fault_flowcontrol::flowcontrol_look_for_thread(flowcontrol_arg_t *argp)
{
	ASSERT(argp->threadid != NULL);

	if (argp->threadid == os::threadid()) {
		NodeTriggers::add(FAULTNUM_FLOWCONTROL_SIGNAL_STOP, NULL,
		    0, TRIGGER_ALL_NODES);
	}
}
