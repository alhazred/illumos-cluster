//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fault_injection.cc	1.59	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/invo/invocation.h>
#include <orb/fault/fault_rpc.h>
#include <orb/fault/fault_injection.h>
#include <sys/vnode.h>
#include <sys/stat.h>

#if defined(_KERNEL_ORB) && defined(_KERNEL)
#include <sys/file.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <stdio.h>
#include <unistd.h>
#include <unode/unode_config.h>
#endif

#if !defined(_KERNEL_ORB)
#include <sys/cladm_int.h>
#endif


// ----------------------------------------------------------------------------
// Fault point interfaces
// ----------------------------------------------------------------------------

//
// fault_triggered()
//
//	Returns true if there is a trigger exists for the fault number given
//	in the parameter.  This routine can be used to provide local fault
//	points (e.g. those that must access local variables).
//
// Parameters:
//	fault_num	-- fault number for the fault point.
//	fault_argpp	-- pointer to a void * variable.  Upon return this
//			   variable is set to point to the fault argument
//			   corresponding to the trigger (NULL if there is
//			   no fault argument).
//	fault_argsizep	-- pointer to a uint32_t variable.  Upon return this
//			   variable is set to the size of the fault argument
//			   (zero if there is no fault argument).
//
// Usage example:
//
//	#if defined(FAULT_FOO)
//		void		*fault_argp;
//		uint32_t	fault_argsize;
//
//		if (fault_triggered(FAULTNUM_FOO_SEND, &fault_argp,
//		    &fault_argsize) {
//			...	// do stuff when fault number FAULTNUM_FOO_SEND
//			...	// is triggered
//		}
//	#endif
//
bool
fault_triggered(uint32_t fault_num, void **fault_argpp,
    uint32_t *fault_argsizep)
{
	// Check Invo triggers.
	InvoTriggers	*trigp = InvoTriggers::the();	// could be NULL

	if (trigp && trigp->is_triggered(fault_num, fault_argpp,
	    fault_argsizep)) {
		return (true);
	}

	// Check Node triggers.
	if (NodeTriggers::the().is_triggered(fault_num, fault_argpp,
	    fault_argsizep)) {
		return (true);
	}

	return (false);
}

//
// The FAULT_POINT() macro is a wrapper around this function.  Do not call
// this function directly; use the FAULT_POINT() or the FAULTPT_*() macros.
//
void
_fault_point(uint32_t fault_num, void (*fault_fp)(uint32_t, void *, uint32_t))
{
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(fault_num, &fault_argp, &fault_argsize)) {
		fault_fp(fault_num, fault_argp, fault_argsize);
	}
}

//
// The FAULT_POINT_ENV() macro is a wrapper around this function.  Do not call
// this function directly; use the FAULT_POINT_ENV() or the FAULTPT_ENV_*()
// macros.
//
void
_fault_point(uint32_t fault_num, Environment *envp,
    void (*fault_fp)(uint32_t, Environment *, void *, uint32_t))
{
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(fault_num, &fault_argp, &fault_argsize)) {
		fault_fp(fault_num, envp, fault_argp, fault_argsize);
	}
}


// ----------------------------------------------------------------------------
// Fault Injection Framework
// ----------------------------------------------------------------------------

//
// class FaultTriggers
//

const uint32_t	FaultTriggers::_hash_table_size = 8;

FaultTriggers::~FaultTriggers()
{
}

//
// FaultTriggers::is_triggered()
//
//	If a trigger with fault number 'fault_num' exists, then
//
//		1) Pointer to the fault argument associated with the trigger
//		   is returned in the area pointed to by the argument
//		   'fault_argpp'.  (NULL pointer is returned if there is
//		   no fault argument.)
//		2) The size of the fault argument, in bytes, is returned
//		   in the area pointed to by the argument 'fault_argsizep'.
//		   (0 is returned if there is no fault argument.)
//		3) true is returned.
//
//	Otherwise, false is returned.
//
// WARNING:
//	Although a reader lock is used to prevent simultaneous write access to
//	the hash table, it is possible that after returning from this function
//	another thread could delete (clears) the trigger.  This would also
//	delete the trigger's fault argument and fault argument size
//	information, and thus render the pointers returned in 'fault_argpp'
//	and 'fault_argsizep' invalid.  Be careful.
//
// Parameters:
//	fault_num	-- fault number of trigger to search for.
//	fault_argpp	-- pointer to area to return pointer to
//			   fault argument carried by the trigger.
//	fault_argsizep	-- pointer to area to return the size, in bytes,
//			   of the fault argument.
//
// Returns:
//	See description above.
//
// Side Effects:
//	See WARNING message above.
//
bool
FaultTriggers::is_triggered(uint32_t fault_num, void **fault_argpp,
    uint32_t *fault_argsizep)
{
	bool	exist = false;

	// Use reader lock since we're only looking up the hash table.
	rdlock();
	if (_hashtab.refcount() > 0) {
		trigger_t	*triggerp = _hashtab.lookup(&fault_num);

		if (triggerp != NULL) {
			if (fault_argpp != NULL) {
				*fault_argpp = triggerp->fault_arg();
			}
			if (fault_argsizep != NULL) {
				*fault_argsizep = triggerp->fault_argsize();
			}
			exist = true;
		}
	}
	unlock();
	return (exist);
}

//
// FaultTriggers::trig_add()
//
//	Adds a trigger with fault number 'fault_num' into the hash table.
//	If a trigger with the same fault number already exists in the
//	hash table then it will be replaced with the specified one.
//
//	Caller must set the writer lock prior to calling this method.
//
// Parameters:
//	fault_num	-- fault number.
//	fault_argp	-- pointer to fault argument (data passed to the
//			   fault point when triggered).
//	fault_argsize	-- size of the fault argument in bytes.
//	copy_fault_arg	-- if true, then data pointed to by 'fault_argp' is
//			   copied into a new buffer prior to adding the
//			   trigger.  Otherwise, 'fault_argp' is assumed to
//			   point to an allocated buffer (when the trigger is
//			   cleared, delete is called on the buffer).
//
// Returns:
//	None.
//
// Side Effects:
//	If the argument 'copy_fault_arg' is true, then data pointed to by
//	'fault_argp' is copied into a new buffer prior to adding the trigger.
//	Otherwise, 'fault_argp' is assumed to point to an allocated buffer;
//	when the trigger is cleared, delete is called on the buffer.
//
//	XXX We always do NO_SLEEP allocation below to avoid the
//	non-blocking invocation debugging check since info about non-blocking
//	invocation is not always available to the fault injection mechanism.
//	If memory allocation fails we'll print a warning message and return
//	without adding the fault trigger.
//
//	Sets the _modified flag to true if the trigger is added.
//
void
FaultTriggers::trig_add(uint32_t fault_num, void *fault_argp,
    uint32_t fault_argsize, bool copy_fault_arg /* = true */)
{
	trigger_t		*triggerp;
	os::mem_alloc_type	flag = os::NO_SLEEP;

	// Writer lock must be set since we're modifying the hash table.
	ASSERT(wrlock_held());

	// Remove existing trigger with the same fault number, if any.
	trig_clear(fault_num);

	// Allocate space, if necessary, for copying the fault argument.
	if (fault_argp == NULL || fault_argsize == 0) {
		fault_argp = NULL;
		fault_argsize = 0;
	} else if (copy_fault_arg) {
		/* CSTYLED */
		char *argbuf = new (flag) char[fault_argsize];
		if (argbuf == NULL) {
			os::warning("Trigger %u not added: can't allocate "
			    "buffer to copy fault argument", fault_num);
			return;
		}
		bcopy(fault_argp, argbuf, (size_t)fault_argsize);
		fault_argp = argbuf;
	}

	/* CSTYLED */
	triggerp = new (flag) trigger_t(fault_num, fault_argp, fault_argsize);
	if (triggerp == NULL) {
		os::warning("Trigger %u not added: can't allocate trigger",
		    fault_num);
		delete [] (uchar_t *)fault_argp;
	} else if (_hashtab.add(triggerp, &fault_num, flag) == NULL) {
		os::warning("Trigger %u not added: can't allocate hash table "
		    "entry", fault_num);
		delete triggerp;		// also deletes fault_argp
	} else {
		_modified = true;
		_total_argsize += fault_argsize;
	}
}

//
// FaultTriggers::trig_clear()
//
//	Remove the trigger with fault number 'fault_num', if it exists,
//	from the hash table.
//
//	Caller must set the writer lock prior to calling this method.
//
// Parameters:
//	fault_num	-- fault number of trigger to be removed.
//
// Returns:
//	None.
//
// Side Effects:
//	Deletes the trigger and its associated fault argument and fault
//	argument size information.
//	Sets the _modified flag to true if the trigger is removed.
//
void
FaultTriggers::trig_clear(uint32_t fault_num)
{
	trigger_t	*triggerp;

	// Writer lock must be set since we're modifying the hash table.
	ASSERT(wrlock_held());

	if ((triggerp = _hashtab.remove(&fault_num)) != NULL) {
		_total_argsize -= triggerp->fault_argsize();
		_modified = true;
		delete triggerp;
	}
}

//
// FaultTriggers::trig_clear_all()
//
//	Removes all triggers, if any, stored in the hash table.
//
//	Caller must set the writer lock prior to calling this method.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// Side Effects:
//	Deletes each trigger stored in the hash table and its associated
//	fault argument and fault argument size information.
//	Sets the _modified flag to true if at least one trigger is removed.
//
void
FaultTriggers::trig_clear_all()
{
	// Write lock must be set since we're modifying the hash table.
	ASSERT(wrlock_held());

	if (_hashtab.refcount() > 0) {
		_hashtab.dispose();
		_total_argsize = 0;
		_modified = true;
	}
}

//
// FaultTriggers::iterate()
//
//	Iterate through the hash table.  For each entry found the callback
//	function 'cb_f' will be called with the callback data 'cb_data'
//	passed as argument.  The iteration continues until all entries
//	have been processed or the callback function returns false.
//
//	Caller must set the lock (reader or writer depending on the intended
//	operation) prior to calling this method.
//
// Parameters:
//	cb_f	-- pointer to callback routine.
//	cb_data	-- pointer to callback data which will be passed to the
//		   callback routine each time it's called.
//
// Returns:
//	None.
//
void
FaultTriggers::iterate(bool (*cb_f)(trigger_t *, void *), void *cb_data)
{
	// Either reader or writer lock (depending upon the intended operation)
	// must be set.
	ASSERT(lock_held());
	_hashtab.iterate(cb_f, cb_data);
}

//
// FaultTriggers::num_triggers()
//
//	Returns the number of triggers in the hash table.
//
// Parameters:
//	None.
//
uint32_t
FaultTriggers::num_triggers()
{
	return (_hashtab.refcount());
}

//
// FaultTriggers::total_argsize()
//
//	Returns total bytes of all fault arguments in the hash table.
//
// Parameters:
//	None.
//
uint32_t
FaultTriggers::total_argsize()
{
	return (_total_argsize);
}

//
// FaultTriggers::is_modified()
//
//	Returns true if triggers have been added/cleared, false otherwise.
//
// Parameters:
//	None.
//
bool
FaultTriggers::is_modified()
{
	return (_modified);
}

//
// FaultTriggers::set_modified()
//
//	Sets the _modified flag.
//
// Parameters:
//	flag	-- value of flag to set.
//
// Returns:
//	None.
//
void
FaultTriggers::set_modified(bool flag)
{
	_modified = flag;
}

//
// Class InvoTriggers
//

//
// InvoTriggers::add()
//
//	Adds an Invo trigger to the calling thread.  If a trigger with
//	the same fault number already exists, it will be replaced with the
//	specified one.
//
// Parameters:
//	fault_num	-- fault number associated with the trigger.
//	fault_argp	-- points to the trigger's fault argument.
//	fault_argsize	-- size, in bytes, of the fault argument.
//
// Returns:
//	None.
//
// Side Effects:
//	The fault argument is copied into the calling thread's specific data.
//
// static
void
InvoTriggers::add(uint32_t fault_num, void *fault_argp, uint32_t fault_argsize)
{
	InvoTriggers	*trigp = InvoTriggers::the();	// could be NULL

	if (trigp != NULL) {
		// No need to lock since Invo Triggers are thread-specific.
		trigp->trig_add(fault_num, fault_argp, fault_argsize);
	}
}

//
// InvoTriggers::clear()
//
//	Clears an Invo trigger (if any) from the calling thread.
//
// Parameters:
//	fault_num	-- fault number associated with the trigger.
//
// Returns:
//	None.
//
void
InvoTriggers::clear(uint32_t fault_num)
{
	InvoTriggers	*trigp = InvoTriggers::the();	// could be NULL

	if (trigp != NULL) {
		// No need to lock since Invo Triggers are thread-specific.
		trigp->trig_clear(fault_num);
	}
}

//
// InvoTriggers::clear_all()
//
//	Clears all Invo triggers from the calling thread.
//
// Parameters:
//	None.
//
// Returns:
//	None.
//
// static
void
InvoTriggers::clear_all()
{
	InvoTriggers	*trigp = InvoTriggers::the();	// could be NULL

	if (trigp != NULL) {
		// No need to lock since Invo Triggers are thread-specific.
		trigp->trig_clear_all();
	}
}

//
// InvoTriggers::the()
//
//	Returns pointer to calling thread's InvoTriggers.
//	Allocates one if it doesn't exist.
//
// Parameters:
//	XXX The argument 'flag' specifies how to allocate the InvoTriggers.
//	In user land, this is ignored.  In kernel land, the default is NO_SLEEP
//	allocation.  This is to avoid the non-blocking invocation debug check
//	since info about non-blocking invocation is not always available to
//	the fault injection mechanism.
//
// Side Effects:
//	If the allocation fails, a warning message is printed and NULL is
//	returned.
//
// static
InvoTriggers *
InvoTriggers::the(os::mem_alloc_type flag)
{
	// Get the Invo triggers from the tsd
	InvoTriggers	*trigp = (InvoTriggers *)_tsd.get_tsd();
	if (trigp == NULL) {
		//
		// No InvoTriggers for this thread yet; create one.
		// Note, the allocation flag is ignored in user-land.
		//
		/* CSTYLED */
		if ((trigp = new (flag) InvoTriggers(trigp)) != NULL) {
			_tsd.set_tsd((uintptr_t)trigp);
		} else {
			os::warning("Can't allocate InvoTriggers");
		}
	}
	return (trigp);
}

//
// InvoTriggers::new_trigger
//
//	Allocates a new object.
//	Any previously existing InvoTriggers becomes inactive,
//	but not destroyed.
//
// Parameters:
//	XXX The argument 'flag' specifies how to allocate the InvoTriggers.
//	In user land, this is ignored.  In kernel land, the default is NO_SLEEP
//	allocation.  This is to avoid the non-blocking invocation debug check
//	since info about non-blocking invocation is not always available to
//	the fault injection mechanism.
//
// Side Effects:
//	If the allocation fails, a warning message is printed and NULL is
//	returned.
//
// static
InvoTriggers *
InvoTriggers::new_trigger(os::mem_alloc_type flag)
{
	InvoTriggers	*trigp;

	// Get current InvoTriggers from the tsd
	InvoTriggers	*old_trigp = (InvoTriggers *)_tsd.get_tsd();

	//
	// Always create a new InvoTriggers
	// Note, the allocation flag is ignored in user-land.
	//
	/* CSTYLED */
	if ((trigp = new (flag) InvoTriggers(old_trigp)) != NULL) {
		_tsd.set_tsd((uintptr_t)trigp);
	} else {
		os::warning("Can't allocate InvoTriggers");
	}

	return (trigp);
}

//
// Destructor - the next InvoTrigger, if any, becomes active.
// Then this InvoTrigger self destructs.
//
InvoTriggers::~InvoTriggers()
{
	// Locking is not required because InvoTriggers are thread specific
	trig_clear_all();

	//
	// The next InvoTrigger becomes active.
	// The "next" pointer can be NULL, and often is NULL.
	//
	_tsd.set_tsd((uintptr_t)next);
	next = NULL;					// keep lint happy
}

//
// disconnect_triggers
// Disconnect the thread's InvoTriggers, which deactivate them.
// Returns a pointer to the thread's InvoTriggers.
//
// This was created so that Fault Injection framework functions could
// be executed without active InvoTriggers. No memory allocation is needed.
//
// static
InvoTriggers *
InvoTriggers::disconnect_triggers()
{
	// Get current InvoTriggers from the tsd
	InvoTriggers	*old_trigp = (InvoTriggers *)_tsd.get_tsd();

	// Record that there are no InvoTriggers
	_tsd.set_tsd((uintptr_t)0L);

	ASSERT(_tsd.get_tsd() == (uintptr_t)0L);

	return (old_trigp);
}

//
// Reconnect a thread to its InvoTriggers.
// The argument was previously obtained through disconnect_triggers().
//
// This was created so that Fault Injection framework functions could
// be executed without active InvoTriggers. No memory allocation is needed.
//
// static
void
InvoTriggers::reconnect_triggers(InvoTriggers *old_trigp)
{
	//
	// Any remote invocation while the old set of InvoTriggers
	// were disconnected could result in an empty
	// InvoTriggers entry. So we are just going to blow away
	// any existing InvoTriggers.
	//
	InvoTriggers	*junk_trigp = (InvoTriggers *)_tsd.get_tsd();
	if (junk_trigp != NULL) {
		// No need to lock since Invo Triggers are thread-specific.
		junk_trigp->trig_clear_all();
		delete junk_trigp;
	}

	//
	// Reconnect the old InvoTriggers.
	// Null means there were no old triggers.
	//
	_tsd.set_tsd((uintptr_t)old_trigp);
}

//
// InvoTriggers::triggers_size(InvoTriggers *trigp)
//
//	The specified InvoTrigger could be NULL.
//	Returns amount of space needed to marshal the triggers.
//	Each trigger is marshaled with the following layout:
//
//		* The fault number associated with the trigger (uint32_t).
//		* The size of the associated fault argument (uint32_t).
//		* If any, the fault argument (series of bytes).
//
uint32_t
InvoTriggers::triggers_size(InvoTriggers *trigp)
{
	if (trigp == NULL) {
		return (0);
	}
	return (trigp->num_triggers() * 2 * (uint32_t)sizeof (uint32_t) +
		trigp->total_argsize());
}

//
// InvoTriggers::put_trigger_info()
//
//	Marshals each Invo trigger at the end of 'ms' with layout:
//
//		* The fault number associated with the trigger (uint32_t).
//		* The size of the associated fault argument (uint32_t).
//		* The fault argument, if any (series of bytes).
//
//	Then places the msg header at the beginning of the stream.
//
// Parameters:
//	ms	-- the MarshalStream.
//	trigp	-- pointer to the InvoTriggers (could be NULL)
// Returns:
//	None.
//
// Side Effects:
//	Leaves the stream's cursor at the beginning.
//
// static
void
InvoTriggers::put_trigger_info(MarshalStream &ms, InvoTriggers *trigp)
{
	uint32_t	numtrigs;
	uint32_t	datalen;

	// Save length of regular marshaled data.
	datalen = ms.span();

	if (trigp == NULL) {
		numtrigs = 0;
	} else {
		numtrigs = trigp->num_triggers();
		if (numtrigs > 0) {
			// We got triggers to ship across.
			ms.goto_end();

			// marshal all triggers one at a time
			// Note, no need to lock since Invo Triggers are
			// thread-specific.
			trigp->iterate(_put_callback, &ms);
		}
	}

	// Marshal the header.
	fi_header_t header((int)numtrigs, datalen);
	ms.write_header((void *)&header, sizeof (fi_header_t));
}

//
// InvoTriggers::_put_callback()
//
//	Callback function to be passed to iterate() to marshal each trigger's
//	fault number, fault argument size and fault argument (if any).
//	'cb_data' must point to the MarshalStream.
//
bool
InvoTriggers::_put_callback(trigger_t *trigger, void *cb_data)
{
	MarshalStream	&ms = *(MarshalStream *)cb_data;

	ms.put_unsigned_long(trigger->fault_num());
	ms.put_unsigned_long(trigger->fault_argsize());
	if (trigger->fault_argsize() > 0) {
		ms.put_bytes(trigger->fault_arg(), trigger->fault_argsize());
	}
	return (true);				// continue with next trigger
}

//
// InvoTriggers::copy_triggers
//
// Copy Invo triggers from current thread to waiting thread.
// This method exists because it is not safe to deliver the Invo triggers to a
// waiting thread until after the orphan message check has been performed and
// the system can guarantee that the waiting thread will always be there.
// Therefore on the reply path, the Invo triggers are delivered first to the
// interrupt thread, and then later copied to the waiting invocation thread.
//
// The source address for fault info could be NULL.
//
// static
void
InvoTriggers::copy_triggers(InvoTriggers *srcp, InvoTriggers *destp)
{
	if (srcp == NULL) {
		//
		// Do not have any Invo triggers to transfer.
		// In this error situation, the existing trigger information
		// in the waiting thread is left unchanged.
		//
		return;
	}

	//
	// The place for the arriving trigger information cannot
	// be the same place as the destination.
	//
	ASSERT(destp != srcp);

	//
	// The system does not guarantee that the invoking thread will
	// have a place for trigger info.
	// XXX - This should be fixed, because the system is losing trigger info
	//
	if (destp == NULL) {
		os::warning("Cannot deliver trigger info to waiting thread\n");
		return;
	}

	// No need to lock since Invo Triggers are thread-specific.
	destp->trig_clear_all();		// start with a clean slate
	if (srcp->num_triggers() > 0) {
		// There are triggers to copy
		srcp->iterate(copy_callback, destp);
	}
}

//
// InvoTriggers::copy_callback
//
// Callback function for copying Invo triggers.
//
// Return true because always want to iterate to the next trigger.
//
// static
bool
InvoTriggers::copy_callback(trigger_t *triggerp, void *cb_data)
{
	InvoTriggers	*dest_trigp = (InvoTriggers *)cb_data;

	//
	// Now add trigger to the destination hash table.
	// Last arg is true to copy the buffer.
	// Note, no need to lock since Invo Triggers are thread-specific.
	//
	dest_trigp->trig_add(triggerp->fault_num(), triggerp->fault_arg(),
	    triggerp->fault_argsize(), true);

	return (true);				// continue with next trigger
}

//
// InvoTriggers::set_up_reply
//
// Set up to handle fault information arriving in reply.
//
// static
void
InvoTriggers::set_up_reply(invocation *invop)
{
	// Get the current threads fault info address
	InvoTriggers	*trigp = the();		// could be NULL

	// Record the place to put fault information from reply message.
	invop->set_invotrigp(trigp);
}

//
// InvoTriggers::strip_trigger_info
//
//	This method discards the fault information, including both data
//	and header.
//
// static
void
InvoTriggers::strip_trigger_info(MarshalStream &mstream)
{
	fi_header_t	header;
	mstream.read_header((void *)&header, sizeof (fi_header_t));

	if (header.num_triggers > 0) {
		// Trim off the marshaled triggers.
		mstream.advance(header.data_len, false);
		mstream.trim_end();
		mstream.prepare();
	}
}

//
// InvoTriggers::get(MarshalStream &)
//
//	Unmarshals Invo triggers.  See put() and _put_callback() for
//	how the triggers are laid out.
//
//	Upon entry, the MarshalStream's cursor must be at the beginning
//	of the InvoTriggers header.
//
// Parameters:
//	ms	-- the MarshalStream.
//
// Returns:
//	None.
//
// Side Effects:
//	Upon return both header and triggers are trimmed out of the stream.
//	Upon return the stream's cursor is placed at the start of the stream.
//
// static
void
InvoTriggers::get(MarshalStream &ms)
{
	InvoTriggers	*trigp = the();		// could be NULL

	// If we couldn't get calling thread's InvoTriggers...
	if (trigp == NULL) {
		strip_trigger_info(ms);
	} else {
		// Unmarshal the trigger info into this InvoTrigger
		trigp->get_trigger_info(ms);
	}
}

//
// InvoTriggers::get_trigger_info(MarshalStream &)
//
//	Unmarshals Invo triggers.  See put() and _put_callback() for
//	how the triggers are laid out.
//
//	Upon entry, the MarshalStream's cursor must be at the beginning
//	of the InvoTriggers header.
//
// Parameters:
//	ms	-- the MarshalStream.
//
// Returns:
//	None.
//
// Side Effects:
//	Upon return both header and triggers are trimmed out of the stream.
//	Upon return the stream's cursor is placed at the start of the stream.
//
//	XXX We always do NO_SLEEP allocation below to avoid the
//	non-blocking invocation debugging check since info about non-blocking
//	invocation is not always available to the fault injection mechanism.
//	If the allocation fails we'll print a warning message and return.
//
void
InvoTriggers::get_trigger_info(MarshalStream &ms)
{
	int	i;

	// Start with a clean slate for fault information
	// No need to lock since Invo Triggers are thread-specific.
	trig_clear_all();

	// Get the header.
	fi_header_t	header;
	ms.read_header((void *)&header, sizeof (fi_header_t));

	// If there are no triggers marshaled...
	if (header.num_triggers == 0) {
		//
		// No need to call strip_trigger_info as the header has
		// already been trimmed and there is no data
		//
		return;
	}

	// Skip regular marshaled data to get to the triggers.
	ms.advance(header.data_len, false);

	// Unmarshal the triggers.
	for (i = 0; i < header.num_triggers; i++) {
		uint32_t	num;			// fault number
		uchar_t		*argp = NULL;		// fault argument
		uint32_t	argsize;		// fault argument size

		num = ms.get_unsigned_long();
		argsize = ms.get_unsigned_long();
		if (argsize > 0) {
			//
			// Since fault argument can span several buffers in the
			// MarshalStream, we'll copy it into a contiguous
			// buffer first before adding it to the hash table.
			//
			/* CSTYLED */
			argp = new (os::NO_SLEEP) uchar_t[argsize];

			// if argp is NULL,
			// get_bytes will discard the fault argument.
			ms.get_bytes(argp, argsize);
			if (argp == NULL) {
				os::warning("Can't allocate buffer to "
				    "get InvoTriggers %u", num);
				continue;		// get next trigger
			}
		}
		//
		// Now add trigger to the hash table.  Last arg is false to
		// avoid copying the argp buffer.
		//
		trig_add(num, argp, argsize, false);
	}
	//
	// Trim off triggers from the stream.
	// This is necessary because, for example, in K->K->U invocation we
	// don't want triggers marshaled for the K->K path to be also sent to
	// to the user land.
	//
	ms.prepare();			// move back to the start of data
	ms.advance(header.data_len, false);
	ms.trim_end();
	ms.prepare();
}

#if defined(_KERNEL) || defined(_KERNEL_ORB)

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif
//
// InvoTriggers::get(door_arg_t *)
//
//	Special case for the gateway's xdoor_proxy() for U->K invocation path.
//	Unmarshal Invo triggers from door data in user space.
//	See put() and _put_callback() for how the triggers are laid out.
//
// Parameters:
//	da	-- pointer to door_arg_t.
//
// Returns:
//	None.
//
// Side Effects:
//	Upon return, the header and the triggers will be "trimmed off" from
//	the door argument by adjusting da->data_ptr and data->data_size.
//
// static
void
InvoTriggers::get(door_arg_t *da)
{
	fi_header_t	header;
	uint32_t	trigslen, datalen;
	int		err;
	Environment	e;

	// Get InvoTriggers header from user land.
	if (copyin(da->data_ptr, &header, sizeof (fi_header_t)) != 0) {
		os::panic("copyin error during InvoTriggers::get");
	}

	// "Trim off" header from the door argument.
	da->data_ptr += sizeof (fi_header_t);

	// Save length of regular marshaled data.
	datalen = header.data_len;

	// Calculate length of the marshaled triggers.
	if (header.num_triggers > 0) {
		ASSERT(da->data_size >=
		    (sizeof (fi_header_t) + (size_t)datalen));
		trigslen = (uint32_t)da->data_size -
				(uint32_t)sizeof (fi_header_t) - datalen;
	} else {
		trigslen = 0;
	}

	// Use a temporary MarshalStream.
	MarshalStream	ms((uint32_t)sizeof (fi_header_t) + trigslen, &e);

	// Copy header into the MarshalStream.
	header.data_len = 0;		// we won't copy the regular data
	ms.put_bytes(&header, sizeof (fi_header_t), false);

	// Copy marshaled triggers into the MarshalStream.
	if (header.num_triggers > 0) {
		err = ms.put_u_bytes(da->data_ptr + datalen, trigslen, false);
		ASSERT(err == 0);
	}

	// Now unmarshal the triggers.
	ms.prepare();
	get(ms);

	// Adjust data_size to reflect only the regular marshaled data.
	da->data_size = datalen;

	// Dispose MarshalStream's buffers.
	ms.dispose();
}

#elif ! defined(_KERNEL_ORB)

//
// InvoTriggers::get(char *&, uint32_t &)
//
//	Special case for solaris xdoor's object_server() for K->U and U->U
//	invocation paths.  Unmarshal Invo triggers from door arguments.
//	See put() and _put_callback() for how the triggers are laid out.
//
// Parameters:
//	data		-- reference to pointer to door data.
//	data_size	-- reference to the size of the door data.
//
// Returns:
//	None.
//
// Side Effects:
//	Upon return, the header and the triggers will be "trimmed off" from
//	the door argument by adjusting 'data' and 'data_size'.
//
// static
void
InvoTriggers::get(char *&data, uint32_t &data_size)
{
	fi_header_t	*headerp = (fi_header_t *)data;
	Environment	e;

	// Put door arguments in a temporary MarshalStream.
	nil_Buf	tmpbuf((uchar_t *)data, data_size, data_size, Buf::STACK);
	MarshalStream	ms(&tmpbuf, &e);

	// Unmarshal triggers.
	get(ms);

	// "Trim off" header from the door argument.
	data += sizeof (fi_header_t);

	// Adjust data_size to reflect only the regular marshaled data.
	data_size = headerp->data_len;

	ms.dispose();
}

#endif // _KERNEL

//
// InvoTriggers::prepend_header_buf()
//
//	Allocates a buffer for the header and prepends it to a MarshalStream.
//	Used when the MarshalStream already contains marshaled data but
//	has no space preallocated for headers.
//
//
// Parameters:
//	ms	-- the MarshalStream.
//
// Returns:
//	None.
//
// Side Effects:
//	Leaves the cursor at start of the original first buffer.
//
void
InvoTriggers::prepend_header_buf(MarshalStream &ms)
{
	MarshalStream	tmp_ms((uint32_t)sizeof (fi_header_t), ms.get_env());

	//
	// This method supports both oneway and twoway messages.
	// Oneway messages can be non-blocking.
	// This code absolutely has to acquire a buffer for the
	// fault injection message header.
	// The system treats all oneways to user level as blocking twoways
	// with no return value.
	// Any oneway messages must allow blocking.
	//
	ASSERT(!ms.get_env()->is_nonblocking());

	// alloc space for header
	uint32_t hdr_size = tmp_ms.alloc_chunk(sizeof (fi_header_t));
	ASSERT(hdr_size == sizeof (fi_header_t));

	tmp_ms.useregion(ms.region());		// move orig to end of temp
	ms.useregion(tmp_ms.region());		// return all to orig
}

//
// Reader/writer lock operations.
//
// Since Invo Triggers are thread-specific, there's no need for locking.
// We'll overload the following lock-related routines from class FaultTriggers
// with no-op routines.
//
void
InvoTriggers::rdlock()
{
}

void
InvoTriggers::wrlock()
{
}

void
InvoTriggers::unlock()
{
}

bool
InvoTriggers::rdlock_held()
{
	return (true);
}

bool
InvoTriggers::wrlock_held()
{
	return (true);
}

bool
InvoTriggers::lock_held()
{
	return (true);
}

//
// Thread-specific Invo triggers.
//
os::tsd	 InvoTriggers::_tsd(InvoTriggers::_tsd_destroy);

// TSD destructor
void
InvoTriggers::_tsd_destroy(void *valuep)
{
	delete ((InvoTriggers *)valuep);
}


//
// Class NodeTriggers
//
NodeTriggers	NodeTriggers::_the_node_triggers;

NodeTriggers::~NodeTriggers()
{
	NodeTriggers::wrlock();
	trig_clear_all();

	// Do not unlock because there is only one such object and there
	// is nothing to access once it is gone.
}

//
// NodeTriggers::add()
//
//	Adds a Node trigger to a node (or all nodes).
//
// Parameters:
//	fault_num	-- fault number associated with the trigger.
//	fault_argp	-- points to the fault argument to be associated with
//			   the trigger.  (Specify NULL if there is no fault
//			   argument.)
//	fault_argsize	-- size, in bytes, of the fault argument.
//	nodeid		-- the node's ID.
//			   If equal to TRIGGER_THIS_NODE, then the trigger
//			   is added to the current node.
//			   If equal to TRIGGER_ALL_NODES, then the trigger
//			   is added to all nodes in the cluster.
//			   Default: TRIGGER_THIS_NODE.
//
// Returns:
//	None.
//
// Side Effects:
//	The fault argument is copied.
//
// static
void
NodeTriggers::add(uint32_t fault_num, void *fault_argp, uint32_t fault_argsize,
    int nodeid)
{
	int	maxid;			// till which nodeid to go through

	switch (nodeid) {
	case TRIGGER_ALL_NODES:
		nodeid = 1;			// starting node ID
		maxid = NODEID_MAX;
		break;
	default:
		if ((nodeid <= 0) ||
		    !orb_conf::node_configured((nodeid_t)nodeid)) {
			os::printf("NodeTriggers::add: unknown node ID %d\n",
			    nodeid);
			return;
		}
		maxid = nodeid;
		break;
	}

	// Just to make sure.
	if (fault_argp == NULL || fault_argsize == 0) {
		fault_argp = NULL;
		fault_argsize = 0;
	}

	for (; nodeid <= maxid; ++nodeid) {
#if defined(_KERNEL_ORB)
		if ((nodeid_t)nodeid == orb_conf::local_nodeid()) {
			// Add directly to this node's NodeTriggers.
			NodeTriggers::the().wrlock();
			NodeTriggers::the().trig_add(fault_num, fault_argp,
			    fault_argsize);
			NodeTriggers::the().unlock();
			continue;
		}
#endif
		if (orb_conf::node_configured((nodeid_t)nodeid)) {
			(void) fault_rpc::send_fault_arg(
			    fault_rpc::NODETRIGGERS_ADD, (nodeid_t)nodeid,
			    fault_num, fault_argp, fault_argsize);
		}
	}
}

//
// NodeTriggers::clear()
//
//	Clears a Node trigger from a node (or all nodes).
//
// Parameters:
//	fault_num	-- fault number of the trigger to be cleared.
//	nodeid		-- the node's ID.
//			   If equal to TRIGGER_THIS_NODE, then the trigger
//			   is cleared from the current node.
//			   If equal to TRIGGER_ALL_NODES, then the trigger
//			   is cleared from all nodes in the cluster.
//			   Default: TRIGGER_THIS_NODE.
//
// Returns:
//	None.
//
// static
void
NodeTriggers::clear(uint32_t fault_num, int nodeid)
{
	int	maxid;			// till which nodeid to go through

	switch (nodeid) {
	case TRIGGER_ALL_NODES:
		nodeid = 1;			// starting node ID
		maxid = NODEID_MAX;
		break;
	default:
		if ((nodeid <= 0) ||
		    !orb_conf::node_configured((nodeid_t)nodeid)) {
			os::printf("NodeTriggers::clear: unknown node ID %d\n",
			    nodeid);
			return;
		}
		maxid = nodeid;
		break;
	}

	for (; nodeid <= maxid; ++nodeid) {
#if defined(_KERNEL_ORB)
		// Clear directly from this node's NodeTriggers.
		if ((nodeid_t)nodeid == orb_conf::local_nodeid()) {
			NodeTriggers::the().wrlock();
			NodeTriggers::the().trig_clear(fault_num);
			NodeTriggers::the().unlock();
			continue;
		}
#endif

		if (orb_conf::node_configured((nodeid_t)nodeid)) {
			(void) fault_rpc::send_fault(
			    fault_rpc::NODETRIGGERS_CLEAR,
			    (nodeid_t)nodeid, fault_num);
		}
	}
}

//
// NodeTriggers::clear_all()
//
//	Clears all Node triggers of a node (or all nodes).
//
// Parameters:
//	nodeid	-- the node's ID.
//		   If equal to TRIGGER_THIS_NODE, then all triggers of the
//		   current node are cleared.
//		   If equal to TRIGGER_ALL_NODES, then all triggers of each
//		   node in the cluster are cleared.
//		   Default: TRIGGER_THIS_NODE.
//
// Returns:
//	None.
//
void
NodeTriggers::clear_all(int nodeid)
{
	int	maxid;			// till which nodeid to go through

	switch (nodeid) {
	case TRIGGER_ALL_NODES:
		nodeid = 1;			// starting node ID
		maxid = NODEID_MAX;
		break;
	default:
		if ((nodeid <= 0) ||
		    !orb_conf::node_configured((nodeid_t)nodeid)) {
			os::printf("NodeTriggers::clear_all: unknown "
			    "node ID %d\n", nodeid);
			return;
		}
		maxid = nodeid;
		break;
	}

	for (; nodeid <= maxid; ++nodeid) {
#if defined(_KERNEL_ORB)
		if ((nodeid_t)nodeid == orb_conf::local_nodeid()) {
			NodeTriggers::the().wrlock();
			NodeTriggers::the().trig_clear_all();
			NodeTriggers::the().unlock();
			continue;
		}
#endif

		if (orb_conf::node_configured((nodeid_t)nodeid)) {
			(void) fault_rpc::send_fault(
			    fault_rpc::NODETRIGGERS_CLEAR_ALL,
			    (nodeid_t)nodeid, 0);
		}
	}
}

#if !defined(_KERNEL_ORB)
//
// Overload (virtual) method in user-land to allow user programs to get
// NodeTriggers from the kernel table.
//
// See FaultTriggers::is_triggered() for more information.
//
// WARNING:
//	Multi-threaded programs must prevent multiple threads from querying
//	and using the same trigger (i.e. same fault number) at the same time.
//	As can be seen in the code below, although a writer lock is used this
//	routine calls trig_add() when a requested trigger is found.  trig_add()
//	deletes any existing trigger with the same number from the hash table
//	prior to adding the one just obtained from the kernel.  E.g. the
//	following could have a disastrous effect if executed by multiple
//	threads at the same time:
//
//		void		*fault_argp;
//		uint32_t	fault_argsize;
//
//		if (fault_triggered(16532, &fault_argp, &fault_argsize) {
//			// Do something with fault argument pointed to by
//			// fault_argp.
//		}
//
//	or
//
//		FAULTPT_FOO(FAULTNUM_FOO_BAR, FaultFunctions::generic);
//
//	By the time the 1st thread returns from fault_triggered() and
//	starts using the fault argument, the 2nd thread obtains the
//	same trigger from the kernel and deletes the trigger currently
//	referenced by the 1st trigger.
//
bool
NodeTriggers::is_triggered(uint32_t fault_num, void **fault_argpp,
    uint32_t *fault_argsizep)
{
	// Default fault argument buffer size.
	const uint32_t		default_bufsize = 64;

	// Fault argument buffer (data will be copied from kernel table).
	uchar_t			*argbuf;
	uint32_t		argbuf_size;
	_cl_get_nodetrig_t	udata;

	wrlock();				// serialize access

	argbuf = new uchar_t[default_bufsize];
	ASSERT(argbuf != NULL);
	argbuf_size = default_bufsize;

	//
	// Prepare structure for getting NodeTrigger from kernel.
	// Note: argbuf is cast to uint64_t so we don't have to deal with
	//	 data model mismatch between kernel- and user-land.
	//
	udata.fault_num = fault_num;
	udata.fault_argsize = argbuf_size;
	udata.fault_argp = (uint64_t)argbuf;

	// Get NodeTrigger from kernel via _cladm().
	while (os::cladm(CL_CONFIG, CL_FAULT_GET_NODETRIG, &udata) != 0) {
		//
		// The following errno values have special meanings:
		//
		//	ENOENT	-- the trigger doesn't exist.
		//	ENOSPC	-- the fault argument buffer isn't big enough;
		//		   udata.fault_argsize should contain the
		//		   needed buffer size upon _cladm()'s return.
		//
		// See NodeTriggers::get_nodetrig() code for more info.
		//
		switch (errno) {
		case ENOENT:
			//
			// The trigger doesn't exist.  Clear the existing
			// trigger, if any, from this program's table.
			//
			trig_clear(fault_num);

			delete [] argbuf;
			unlock();
			return (false);

		case ENOSPC:
			//
			// Previous buffer isn't big enough for the fault
			// argument.  Reallocate buffer.  udata.fault_argsize
			// should contain the needed buffer size.
			//
			delete [] argbuf;
			argbuf = new uchar_t[udata.fault_argsize];
			ASSERT(argbuf != NULL);
			argbuf_size = udata.fault_argsize;

			// Try again.
			udata.fault_num = fault_num;
			udata.fault_argsize = argbuf_size;
			udata.fault_argp = (uint64_t)argbuf;

			break;

		default:
			os::warning("_cladm(CL_CONFIG, CL_FAULT_GET_NODETRIG) "
			    "for fault 0x%x: %s\n",
			    fault_num, strerror(errno));

			delete [] argbuf;
			unlock();
			return (false);
		}
	}

	// We got the trigger from the kernel table.  Add it to this program's.
	if (udata.fault_argsize == 0) {
		// The fault has no argument.
		trig_add(fault_num, NULL, 0);

		if (fault_argpp != NULL) {
			*fault_argpp = NULL;
		}
		if (fault_argsizep != NULL) {
			*fault_argsizep = 0;
		}

		delete [] argbuf;
	} else {
		// Last arg is false to prevent copying of argbuf.
		trig_add(fault_num, argbuf, udata.fault_argsize, false);

		if (fault_argpp != NULL) {
			*fault_argpp = argbuf;
		}
		if (fault_argsizep != NULL) {
			*fault_argsizep = udata.fault_argsize;
		}
	}

	unlock();
	return (true);
}
#endif // !KERNEL_ORB

//
// For user-land programs to get a NodeTrigger from kernel table
// (via _cladm()).  Called by cl_fault_injection().
//
// Parameters:
//	uaddr	-- pointer to _cl_get_nodetrig_t struct in user space.
//
// Returns:
//	0	-- successful: the requested trigger exists.
//		   The fault argument, if any, is copied to the user buffer
//		   pointed to by the fault_argp member of the struct.
//		   The fault_argsize member of the struct will be set to
//		   to the actual size of the fault argument.
//	ENOENT	-- the requested trigger doesn't exist.
//	ENOSPC	-- the requested trigger exists, but the user-supplied buffer
//		   isn't big enough for the fault argument.
//		   Upon return, the fault_argsize member of the struct will
//		   be set to the needed size.
//	Other errno value.
//
int
NodeTriggers::get_nodetrig(void *uaddr)
{
	_cl_get_nodetrig_t	udata;
	void			*f_argp;
	uint32_t		f_argsize;
	int			err;

	// Copyin user-supplied data.
	if ((err = xcopyin(uaddr, &udata, sizeof (udata))) != 0) {
		return (err);
	}

	//
	// Get the requested trigger.
	// Note, this calls the kernel version of NodeTriggers::is_triggered().
	//
	if (! is_triggered(udata.fault_num, &f_argp, &f_argsize)) {
		return (ENOENT);		// trigger doesn't exist
	}

	// If user-supplied buffer is not big enough...
	if (udata.fault_argsize < f_argsize) {
		// Return the needed buffer size.
		udata.fault_argsize = f_argsize;
		if ((err = xcopyout(&udata, uaddr, sizeof (udata))) != 0) {
			return (err);
		}
		return (ENOSPC);
	}

	//
	// Copy fault argument to user-supplied buffer.
	//
	// Lint complains about size incompatibility for third arg
	// of xcopyout() below. Casting doesn't always help because
	// size_t is either ulong_t or uint_t, while f_argsize is
	// always uint32_t.
	//
	//lint -e511
	err = xcopyout(f_argp, (void *)udata.fault_argp,
	    (size_t)f_argsize);
	//lint +e511

	if (err != 0) {
		return (err);
	}

	// Return actual size of fault argument.
	udata.fault_argsize = f_argsize;
	if ((err = xcopyout(&udata, uaddr, sizeof (udata))) != 0) {
		return (err);
	}

	return (0);
}

//
// Handles remote NodeTriggers::add() request.
//
void
NodeTriggers::add_rpc_handler(recstream *re)
{
	uint32_t	fault_num = re->get_unsigned_long();
	uint32_t	fault_argsize = re->get_unsigned_long();
	uchar_t		*fault_argp = NULL;

	if (fault_argsize > 0) {
		/* CSTYLED */
		fault_argp = new (os::NO_SLEEP) uchar_t[fault_argsize];
		if (fault_argp == NULL) {
			os::warning("Can't allocate buffer to add trigger %u\n",
			    fault_num);
			return;
		}
		re->get_bytes(fault_argp, fault_argsize);
	}

	//
	// Last arg is false to prevent trig_add() from copying the fault
	// argument since we already allocated the fault_argp buffer above.
	//
	NodeTriggers::the().wrlock();
	NodeTriggers::the().trig_add(fault_num, fault_argp, fault_argsize,
	    false);
	NodeTriggers::the().unlock();
}

//
// Handles remote NodeTriggers::clear() request.
//
void
NodeTriggers::clear_rpc_handler(recstream *re)
{
	NodeTriggers::the().wrlock();
	NodeTriggers::the().trig_clear(re->get_unsigned_long());
	NodeTriggers::the().unlock();
}

//
// Handles remote NodeTriggers::clear_all() request.
//
void
NodeTriggers::clear_all_rpc_handler(recstream *)
{
	NodeTriggers::the().wrlock();
	NodeTriggers::the().trig_clear_all();
	NodeTriggers::the().unlock();
}

//
// Reader/writer lock operations.
//

void
NodeTriggers::rdlock()
{
	_rwlock.rdlock();
}

void
NodeTriggers::wrlock()
{
	_rwlock.wrlock();
}

void
NodeTriggers::unlock()
{
	_rwlock.unlock();
}

bool
NodeTriggers::rdlock_held()
{
	return ((bool)_rwlock.read_held());
}

bool
NodeTriggers::wrlock_held()
{
	return ((bool)_rwlock.write_held());
}

bool
NodeTriggers::lock_held()
{
	return ((bool)_rwlock.lock_held());
}


//
// Class BootTriggers
//

#if	defined(_KERNEL)
//
// Initial offset is 0
//
size_t	BootTriggers::_offset = 0;

#endif	// _KERNEL

//
// BootTriggers::add()
//
//	Adds a Node trigger to a node (or all nodes).
//
// Parameters:
//	fault_num	-- fault number associated with the trigger.
//	fault_argp	-- points to the fault argument to be associated with
//			   the trigger.  (Specify NULL if there is no fault
//			   argument.)
//	fault_argsize	-- size, in bytes, of the fault argument.
//	nodeid		-- the node's ID.
//			   If equal to TRIGGER_THIS_NODE, then the trigger
//			   is added to the current node.
//			   If equal to TRIGGER_ALL_NODES, then the trigger
//			   is added to all nodes in the cluster.
//			   Default: TRIGGER_THIS_NODE.
//
// Returns:
//	None.
//
// Side Effects:
//	The fault argument is copied.
//
// static
void
BootTriggers::add(uint32_t fault_num, void *fault_argp, uint32_t fault_argsize,
    int nodeid)
{
	int	maxid;		// till which nodeid to go through

	switch (nodeid) {
	case TRIGGER_ALL_NODES:
		nodeid = 1;			// starting node ID
		maxid = NODEID_MAX;
		break;
	default:
		if ((nodeid <= 0) ||
		    !orb_conf::node_configured((nodeid_t)nodeid)) {
			os::printf("BootTriggers::add: unknown node ID %d\n",
			    nodeid);
			return;
		}
		maxid = nodeid;
		break;
	}

	// Just to make sure.
	if (fault_argp == NULL || fault_argsize == 0) {
		fault_argp = NULL;
		fault_argsize = 0;
	}

	for (; nodeid <= maxid; ++nodeid) {
#if defined(_KERNEL_ORB)
		if ((nodeid_t)nodeid == orb_conf::local_nodeid()) {
			// Add directly to this node's BootTriggers.
			BootTriggers::_save_fault(fault_num, fault_argp,
			    fault_argsize);
			continue;
		}
#endif
		if (orb_conf::node_configured((nodeid_t)nodeid)) {
			(void) fault_rpc::send_fault_arg(
			    fault_rpc::BOOTTRIGGERS_ADD,
			    (nodeid_t)nodeid, fault_num,
			    fault_argp, fault_argsize);
		}
	}
}


#if	defined(_KERNEL_ORB)
//
// Handles remote BootTriggers::add() request.
//
void
BootTriggers::add_rpc_handler(recstream *re)
{
	uint32_t	fault_num = re->get_unsigned_long();
	uint32_t	fault_argsize = re->get_unsigned_long();
	uchar_t		*fault_argp = NULL;

	if (fault_argsize > 0) {
		/* CSTYLED */
		fault_argp = new (os::NO_SLEEP) uchar_t[fault_argsize];
		if (fault_argp == NULL) {
			os::warning("Can't allocate buffer to add trigger %u\n",
			    fault_num);
			return;
		}
		re->get_bytes(fault_argp, fault_argsize);
	}
	BootTriggers::_save_fault(fault_num, fault_argp, fault_argsize);
}
#endif

//
// Removes all BootTriggers by deleting the boottriggerinfo
// file
//
void
BootTriggers::clear_all()
{
#if	defined(_KERNEL)
	int error;
	char filename[PATH_MAX + 1] = "/etc/cluster/boottriggerinfo";
	// Delete the file
	error = vn_remove(filename, UIO_SYSSPACE, RMFILE);
	if (error != 0 && error != ENOENT) {
		os::warning("ERROR: Can't remove boot fault file: %s\n",
		    filename);
	}

	struct	vnode	*vnodep;
	int fmode = S_IFREG | S_IRUSR + S_IRGRP | S_IROTH;
	if ((error = vn_open(filename, UIO_SYSSPACE, FCREAT | FWRITE,
	    fmode, &vnodep, (enum create)0, 0)) != 0) {
		os::warning("Couldn't open triggerinfo file\n");
		return;
	}
	vn_rele(vnodep);
#elif !defined(_KERNEL) && defined(_KERNEL_ORB)
	char	filename[PATH_MAX + 1];
	os::sprintf(filename, "%s/boottriggerinfo",
	    unode_config::the().node_dir());
	// Delete the file
	if (unlink(filename) < 0) {
		os::warning("ERROR: Can't remove boot fault file: %s (%s)\n",
		    filename, strerror(errno));
	}
#endif
}

//
// Saves the fault onto a file
//
#if defined(_KERNEL_ORB)
void
BootTriggers::_save_fault(uint32_t fault_num, void *fault_argp,
    uint32_t fault_argsize)
{
#if defined(_KERNEL)

	BootTriggers::_fault_header_t	fault_header;
	size_t		header_size;
	int		rslt;
	ssize_t		resid = 0;

	struct	vnode	*vnodep;
	static char filename[] = "/etc/cluster/boottriggerinfo";
	int error;

	if ((error = vn_open(filename, UIO_SYSSPACE, FWRITE | FSYNC,
	    0, &vnodep, (enum create) 0, 0)) != 0) {
		os::panic("BootTriggers: Fault 0x%x: Can't open %s: %d\n",
		    fault_num, filename, error);
	}

	ASSERT(vnodep != NULL);

	fault_header.fault_num		= fault_num;
	fault_header.fault_argsize	= fault_argsize;
	header_size			= sizeof (fault_header);

	// Write out the header onto the file
	rslt = vn_rdwr(UIO_WRITE, vnodep, (caddr_t)&fault_header,
	    (ssize_t)header_size, (offset_t)_offset, UIO_SYSSPACE,
	    FSYNC | FDSYNC | FAPPEND, (rlim64_t)RLIM_INFINITY, kcred, &resid);
	if (rslt != 0) {
		os::panic("BootTriggers: Fault 0x%x: Can't write to %s: %d\n",
		    fault_num, filename, error);
	}
	_offset += header_size;

	// Now write the fault args if any
	if (fault_argsize > 0) {
		rslt = vn_rdwr(UIO_WRITE, vnodep, (caddr_t)fault_argp,
		    (ssize_t)fault_argsize, (offset_t)_offset,
		    UIO_SYSSPACE, FSYNC | FDSYNC | FAPPEND,
		    (rlim64_t)RLIM_INFINITY, kcred, &resid);
		_offset += fault_argsize;
		if (rslt != 0) {
			os::panic("BootTriggers: Fault 0x%x: Can't write to %s:"
			    " %d\n", fault_num, filename, error);
		}
	}

	os::printf("Writing fault number %d with size %d\n", fault_num,
	    fault_argsize);

	// Release the vnode
	vn_rele(vnodep);

#else
	// Unode uses user level file i/o interfaces

	char	filename[PATH_MAX + 1];
	FILE	*fp;

	//
	// Open the trigger info file
	//
	os::sprintf(filename, "%s/boottriggerinfo",
	    unode_config::the().node_dir());
	fp = fopen(filename, "a");
	if (fp == NULL) {
		os::panic("BootTriggers: Fault 0x%x: Can't open %s\n",
		    fault_num, filename);
	}

	BootTriggers::_fault_header_t	fault_header;
	size_t				header_size;
	size_t				rslt;

	fault_header.fault_num		= fault_num;
	fault_header.fault_argsize	= fault_argsize;
	header_size			= sizeof (fault_header);

	// Write out the header onto the file
	//
	// Lint doesn't like that we might be using a NULL fp below. Note
	// that we'd have panicked above if fp is NULL.
	//
	//lint -e668
	rslt = fwrite(&fault_header, header_size, (size_t)1, fp);
	if (rslt != 1) {
		os::panic("BootTriggers: Fault 0x%x: Can't write to %s\n",
		    fault_num, filename);
	}

	// Now write the fault args if any
	if (fault_argsize > 0) {
		rslt = fwrite(fault_argp, (size_t)fault_argsize, (size_t)1, fp);
		if (rslt != 1) {
			os::panic("BootTriggers: Fault 0x%x: Can't write to "
			    "%s\n", fault_num, filename);
		}
	}

	(void) fclose(fp);
	//lint +e668

	return;
#endif	// _KERNEL
}
#endif // _KERNEL_ORB


#if defined(_KERNEL_ORB)
//
// load_faults()
// Read in the persistent node faults that were stored prior to
// this node being booted.
// This is an interface to be invoked at boot time.
void
BootTriggers::load_faults()
{
#if defined(_KERNEL)

	struct	vnode	*vnodep;

	//
	// Open the trigger info file
	//
	static char filename[] = "/etc/cluster/boottriggerinfo";
	int error;

	if ((error = vn_open(filename, UIO_SYSSPACE, FREAD | FSYNC,
	    0, &vnodep, (enum create) 0, 0)) != 0) {
		if (error == ENOENT) {
			vnodep = NULL;
		} else {
			os::panic("BootTriggers: Can't open %s: %d\n",
			    filename, error);
		}
	}

	if (vnodep != NULL) {
		BootTriggers::_fault_header_t	fault_header;
		size_t		header_size;
		int		rslt;
		ssize_t		resid = 0;
		char 		*fault_argp = NULL;

		_offset = 0;
		header_size	= sizeof (fault_header);
		bool	triggers_left = true;

		while (triggers_left) {
			rslt = vn_rdwr(UIO_READ, vnodep, (char*)&fault_header,
			    (ssize_t)header_size, (offset_t)_offset,
			    UIO_SYSSPACE, FSYNC,
			    (rlim64_t)RLIM_INFINITY, kcred, &resid);
			_offset += header_size;
			if (resid > 0) {
				// There are no remaining faults to be read
				break;
			}

			if (rslt != 0) {
				break;
			}

			if (fault_header.fault_argsize > 0) {
				// Allocate space to read arguments.
				fault_argp =
				    new char[fault_header.fault_argsize];
				ASSERT(fault_argp != NULL);

				rslt = vn_rdwr(UIO_READ, vnodep, fault_argp,
				    (ssize_t)fault_header.fault_argsize,
				    (offset_t)_offset,
				    UIO_SYSSPACE, FSYNC,
				    (rlim64_t)RLIM_INFINITY, kcred, &resid);
				if (resid > 0) {
					// No remaining faults to be read
					break;
				}
				_offset += fault_header.fault_argsize;
			}

			os::printf("Loaded fault %d with argsize %d\n",
			    fault_header.fault_num, fault_header.fault_argsize);
			// Now add this fault as a node_trigger
			NodeTriggers::add(fault_header.fault_num, fault_argp,
			    fault_header.fault_argsize, (int)TRIGGER_THIS_NODE);

			// delete the allocated space
			delete []fault_argp;
		}
		vn_rele(vnodep);
	}

	_offset = 0;
	// remove file before opening in create mode
	error = vn_remove(filename, UIO_SYSSPACE, RMFILE);
	if (error != 0 && error != ENOENT) {
		os::warning("Couldn't remove triggerinfo file\n");
		return;
	}

	int fmode = S_IFREG | S_IRUSR + S_IRGRP | S_IROTH;
	if ((error = vn_open(filename, UIO_SYSSPACE, FCREAT | FWRITE,
	    fmode, &vnodep, (enum create)0, 0)) != 0) {
		os::warning("Couldn't open triggerinfo file\n");
		return;
	}
	vn_rele(vnodep);

#else
	//
	// This is the UNODE implementation
	// of this functions.  Which will read a file named:
	// /tmp/clusternamenodenumber.boottriggerinfo
	// which will contain a list of node triggers to activate
	//

	char	filename[PATH_MAX + 1];
	FILE	*fp;

	//
	// Open the trigger info file
	//
	os::sprintf(filename, "%s/boottriggerinfo",
	    unode_config::the().node_dir());
	fp = fopen(filename, "r");
	if (fp == NULL) {
		if (errno == ENOENT) {
			return;			// No boot triggers
		} else {
			os::printf("ERROR: Can't open boot time fault file: "
			    "%s (%s)\n", filename, strerror(errno));
			exit(1);
		}
	}

	BootTriggers::_fault_header_t	fault_header;
	char				*fault_argp	= NULL;
	size_t				rslt;

	do {
		// Read the header for the fault
		rslt = fread(&fault_header, sizeof (fault_header),
		    (size_t)1, fp);
		if (rslt != 1) {
			break;
		}

		// Read the fault arguments if any
		if (fault_header.fault_argsize > 0) {
			// Allocate space to read the arguments
			fault_argp = new char[fault_header.fault_argsize];
			ASSERT(fault_argp != NULL);
			//
			// Lint warns that fault_argp could be NULL.
			// Suppress the warning.
			//
			//lint -e668
			rslt = fread(fault_argp,
			    (size_t)fault_header.fault_argsize, (size_t)1, fp);
			//lint +e668
			if (rslt != 1) {
				os::warning("Unexpected return value from "
				    "fread.  (%d)\n", rslt);

				delete []fault_argp;
				break;
			}
		} else {
			fault_argp = NULL;
		}

		// Now add this fault as a node_trigger
		os::printf("Adding fault: %d with argsize: %d\n",
		    fault_header.fault_num, fault_header.fault_argsize);
		NodeTriggers::add(fault_header.fault_num, fault_argp,
		    fault_header.fault_argsize, (int)TRIGGER_THIS_NODE);

		// delete the allocated space
		delete []fault_argp;
	} while (true);

	(void) fclose(fp);

	// Now delete the file
	if (unlink(filename) < 0) {
		os::printf("ERROR: Can't remove boot fault file: %s (%s)\n",
		    filename, strerror(errno));
		exit(1);
	}

	return;

#endif // _KERNEL
}

#endif // _KERNEL_ORB
