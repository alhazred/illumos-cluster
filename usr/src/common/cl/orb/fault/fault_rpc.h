/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULT_RPC_H
#define	_FAULT_RPC_H

#pragma ident	"@(#)fault_rpc.h	1.25	08/05/20 SMI"

#if defined(_FAULT_INJECTION)
#if defined(__cplusplus)

#include <orb/invo/common.h>
#include <orb/flow/resource.h>
#include <orb/buffers/marshalstream.h>

//
// Support routines to allow fault points to do rpc, e.g. rebooting/panicing
// a remote node, orb_msg::FAULT_MSG message types
//

class fault_rpc {
public:
	// Types of rpc.
	enum opcode_t {
		BOOTTRIGGERS_ADD,
		NODETRIGGERS_ADD,
		NODETRIGGERS_CLEAR,
		NODETRIGGERS_CLEAR_ALL,
		NODE_PANIC,
		NODE_REBOOT,
		NODE_REBOOT_CLEAN,
		N_HANDLERS
	};

	//
	// Return wether the message type is FAULT_MSG
	//
	static bool	is_fault_msg(orb_msgtype);

#if defined(_KERNEL_ORB)
	//
	// Message handler routine which the transport calls when it receives
	// a orb_msg::FAULT_MSG message type
	//
	static void	handle_messages(recstream *);
#endif

	// Transmit fault information to destination node
	static bool	send_fault(opcode_t opcode, nodeid_t nodeid,
			    uint32_t fault_num);

	static bool	send_fault_arg(opcode_t opcode, nodeid_t nodeid,
			    uint32_t fault_num, void *argp, uint32_t size);

	// Sends a fault message to a remote node (one-way message).
	static bool	send_message(opcode_t opcode, nodeid_t nodeid,
			    sendstream *sendstreamp);

	//
	// User-land processes use _cladm() to ask kernel to do
	// fault injection rpc on their behalf.
	// XXX	This could go away if sdoor_transport and the gateway
	//	support message types
	//
	struct cladm_header_t {
#if defined(MARSHAL_DEBUG)
		uint32_t	marshal_debug;	// this must be the 1st word
#endif	// MARSHAL_DEBUG

		uint32_t	opcode;
		uint32_t	nodeid;
		uint32_t	datasize;
	};

	class fault_header_t {
	public:
		fault_header_t(uint32_t op) : opcode(op) { }

		fault_header_t() { }
#if defined(MARSHAL_DEBUG)
		uint32_t	marshal_debug;	// this must be the 1st word
#endif	// MARSHAL_DEBUG

		uint32_t	opcode;
	};

private:
	// Disallow assignments and pass by value
	fault_rpc(const fault_rpc &);
	fault_rpc & operator = (fault_rpc &);
};

extern "C" {
#endif /* __cplusplus */

/*
 * Routine to handle fault injection suport _cladm() commands.
 * Called from clconfig().
 */
#if defined(_KERNEL) || defined(_KERNEL_ORB)
int	cl_fault_injection(int cmd, void *arg);
#endif

#if defined(__cplusplus)
}
#endif

#endif /* _FAULT_INJECTION */
#endif	/* _FAULT_RPC_H */
