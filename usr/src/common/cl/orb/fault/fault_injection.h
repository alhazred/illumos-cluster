/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _FAULT_INJECTION_H
#define	_FAULT_INJECTION_H

#pragma ident	"@(#)fault_injection.h	1.39	08/05/20 SMI"

#if defined(_FAULT_INJECTION)

#include <sys/door.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/flow/resource.h>
#include <orb/invo/invocation.h>
#include <sys/hashtab_def.h>

// Two convenience values to specify the node ID of the current
// node and all nodes in the cluster.
#define	TRIGGER_THIS_NODE	orb_conf::local_nodeid()
#define	TRIGGER_ALL_NODES	-1

#ifndef _KERNEL
#define	xcopyin(src, dest, size) (bcopy(src, dest, size), 0)
#define	xcopyout(src, dest, size) (bcopy(src, dest, size), 0)
#endif

// ----------------------------------------------------------------------------
// Fault point interfaces
// ----------------------------------------------------------------------------

//
// FAULT_POINT() macro
//
//	This macro can be used to insert fault points for _debugging_ purposes
//	since it is not protected by the FAULT_* macro switches defined in
//	<fault_numbers/fault_numbers.h>.
//
//	However, final code should, instead, use the FAULTPT_*() macros
//	defined in <fault_numbers/fault_numbers.h> and other "fault-number"
//	files included by it.  These macros allow one to select only certain
//	fault points to compiled into (or out of) the code.
//
//	See the file <fault_numbers/fault_numbers.h> for more information
//	on the FAULT_* switches and the FAULTPT_*() macros.
//
//	This macro defines to nothing if _FAULT_INJECTION is _not_ defined.
//
// Parameters:
//	fault_num	-- fault number for the fault point.
//	fault_fp	-- pointer to a fault function.
//
// Fault functions must of type
//
//	void func(uint32_t, void *, uint32_t)
//
// and will be called when there is a trigger associated with the fault number.
// The following parameters will be passed to the fault function:
//
//	* The fault number (uint32_t).
//	* Pointer to the trigger's fault argument (void *).
//	* The size, in bytes, of the fault argument (uint32_t).
//
// The class FaultFunctions provide a set of common fault functions.
//
// Usage example:
//
//	FAULT_POINT(123, FaultFunctions::remote_panic)
//
#define	FAULT_POINT(fault_num, fault_fp) _fault_point((fault_num), (fault_fp));

//
// FAULT_POINT_ENV() macro
//
//	This macro is the same as FAULT_POINT() macro except it is used
//	for calling fault functions of the following type:
//
//		void func(uint32_t, Environment *, void *, uint32_t)
//
//	Useful for setting fault points which set exceptions.
//
//	Final code shouldn't use this macro directly.  Instead use macros
//	such as FAULTPT_ENV_*() defined in <fault_numbers/fault_numbers.h>
//	and other "fault-number" files included by it.
//
// Usage example:
//
//	FAULT_POINT_ENV(123, rm.get_env(), FaultFunctions::set_sys_exception)
//
#define	FAULT_POINT_ENV(fault_num, envp, fault_fp) \
    _fault_point((fault_num), (envp), (fault_fp));

//
// fault_triggered()
//
//	Returns true if there is a trigger exists for the fault number given
//	in the parameter.  This routine can be used to provide local fault
//	points (e.g. those that must access local variables).
//
// Parameters:
//	fault_num	-- fault number for the fault point.
//	fault_argpp	-- pointer to a void * variable.  Upon return this
//			   variable is set to point to the fault argument
//			   corresponding to the trigger (NULL if there is
//			   no fault argument).
//	fault_argsizep	-- pointer to a uint32_t variable.  Upon return this
//			   variable is set to the size of the fault argument
//			   (zero if there is no fault argument).
//
// Usage example:
//
//	#if defined(FAULT_FOO)
//		void		*fault_argp;
//		uint32_t	fault_argsize;
//
//		if (fault_triggered(FAULTNUM_FOO_SEND, &fault_argp,
//		    &fault_argsize) {
//			...	// do stuff when fault number FAULTNUM_FOO_SEND
//			...	// is triggered
//		}
//	#endif
//
bool	fault_triggered(uint32_t fault_num, void **fault_argpp,
	    uint32_t *fault_argsizep);

//
// The FAULT_POINT() macro is a wrapper around this function.  Do not call
// this function directly; use the FAULT_POINT() or the FAULTPT_*() macros.
//
void	_fault_point(uint32_t fault_num,
	    void (*fault_fp)(uint32_t, void *, uint32_t));

//
// The FAULT_POINT_ENV() macro is a wrapper around this function.  Do not call
// this function directly; use the FAULT_POINT_ENV() or the FAULTPT_ENV_*()
// macros.
//
void	_fault_point(uint32_t fault_num, Environment *,
	    void (*fault_fp)(uint32_t, Environment *, void *, uint32_t));


// ----------------------------------------------------------------------------
// Fault Injection Framework
// ----------------------------------------------------------------------------

//
// Class FaultTriggers
//
//	Base class to manage the collection, deletion and checking of
//	fault injection triggers.
//
class FaultTriggers {
private:
	static const uint32_t	_hash_table_size;

public:
	//
	// The members below are for fault injection framework use only.
	//

	// If a trigger with fault number 'fault_num' exists, then
	//
	//	1) Pointer to the fault argument associated with the trigger
	//	   is returned in the area pointed to by 'fault_argpp'.  (NULL
	//	   pointer is returned if there is no fault argument.)
	//	2) The size of the fault argument, in bytes, is returned
	//	   in the area pointed to by 'fault_argsizep'.  (0 is returned
	//	   if there is no fault argument.)
	//	3) true is returned.
	//
	// Otherwise, false is returned.
	virtual bool	is_triggered(uint32_t fault_num, void **fault_argpp,
			    uint32_t *fault_argsizep);

protected:
	FaultTriggers();

	//
	// All clean up should be done in the destructor for the
	// derived class.
	//
	virtual ~FaultTriggers();

	// Trigger container.
	class trigger_t {
	public:
		trigger_t(uint32_t flt_num, void *flt_argp,
		    uint32_t flt_argsize);
		~trigger_t();

		// Access functions to private members.
		uint32_t	fault_num();
		void		*fault_arg();
		uint32_t	fault_argsize();

	private:
		uint32_t	_fault_num;		// fault number
		uchar_t		*_fault_argp;		// fault argument
		uint32_t	_fault_argsize;		// fault argument size
	};

	//
	// Add a trigger with fault number 'fault_num' into the hash table.
	// If a trigger with the same fault number already exists in the
	// hash table, then it will be replaced with the specified one.
	// By default fault arguments (pointed to by 'fault_argp') are copied.
	// Caller must set the writer lock prior to calling this method.
	//
	virtual void	trig_add(uint32_t fault_num,
			    void *fault_argp, uint32_t fault_argsize,
			    bool copy_fault_arg = true);

	//
	// Remove a trigger with fault number 'fault_num', if it exists,
	// from the hash table.
	// Caller must set the writer lock prior to calling this method.
	//
	virtual void	trig_clear(uint32_t fault_num);

	//
	// Remove all triggers, if any, stored in the hash table.
	// Caller must set the writer lock prior to calling this method.
	//
	void		trig_clear_all();

	//
	// Iterate through the hash table.  For each entry found the callback
	// function 'cb_f' will be called with the callback data 'cb_data'
	// passed as argument.  The iteration continues until all entries
	// have been processed or the callback function returns false.
	// Caller must set the lock (reader or writer depending on the intended
	// operation) prior to calling this method.
	//
	virtual void	iterate(bool (*cb_f)(trigger_t *, void *),
			    void *cb_data);

	// Returns the number of triggers in the hash table.
	virtual uint32_t	num_triggers();

	// Returns total bytes of all fault arguments in the hash table.
	virtual uint32_t	total_argsize();

	// Returns true if triggers have been added/cleared.
	virtual bool	is_modified();

	// Sets the _modified flag.
	virtual void	set_modified(bool);

	virtual void	rdlock() = 0;
	virtual void	wrlock() = 0;
	virtual void	unlock() = 0;
	virtual bool	rdlock_held() = 0;
	virtual bool	wrlock_held() = 0;
	virtual bool	lock_held() = 0;

private:
	//
	// Hash table for storage and fast lookup of triggers.
	// Uses fault numbers as indices.
	//
	hash_table_t<trigger_t, 1>	_hashtab;

	bool		_modified;	// true if triggers were added/cleared
	uint32_t	_total_argsize;	// total bytes of fault arguments

	// Disallow assignments or pass by value.
	FaultTriggers(const FaultTriggers &);
	FaultTriggers &operator = (const FaultTriggers &);
};

//
// Class InvoTriggers
//
//	Manages collection, deletion, checking, marshaling and unmarshaling
//	of Invo triggers.
//
class InvoTriggers : public FaultTriggers {
public:
	//
	// Adds an Invo trigger to the calling thread.
	//
	static void	add(uint32_t fault_num, void *fault_argp,
			    uint32_t fault_argsize);

	//
	// Clears an Invo trigger from the calling thread.
	//
	static void	clear(uint32_t fault_num);

	//
	// Clears all Invo triggers from the calling thread.
	//
	static void	clear_all();

	//
	// The members below are for fault injection framework use only.
	//

	~InvoTriggers();

	//
	// Returns pointer to calling thread's Invo triggers.
	// Allocates one if it doesn't exist.
	//
	// XXX The argument specifies how to allocate the InvoTriggers.
	// In user land, this is ignored.  In kernel land, the default
	// is NO_SLEEP allocation.  This is to avoid the non-blocking
	// invocation debug check since info about non-blocking
	// invocation is not always available to the fault injection
	// mechanism.  If the allocation fails, a warning message is
	// printed and NULL is returned.
	//
	static InvoTriggers	*the(os::mem_alloc_type = os::NO_SLEEP);

	// Always create a new object and deactivate old object
	static InvoTriggers	*new_trigger(os::mem_alloc_type = os::NO_SLEEP);

	//
	// Disconnect the thread's InvoTriggers, which deactivate them.
	// Returns a pointer to the thread's InvoTriggers.
	//
	static InvoTriggers	*disconnect_triggers();

	//
	// Reconnect a thread to its InvoTriggers.
	// The argument was previously obtained through disconnect_triggers().
	//
	static void		reconnect_triggers(InvoTriggers *);

	// Reserve resources for marshaling Invo triggers.
	static void	reserve_resources(resources *, InvoTriggers *);

	//
	// Marshal each Invo trigger at the end of a MarshalStream and
	// put a header at the beginning of the stream.
	//
	static void	put(sendstream *);
	static void	put(recstream *);
	static void	put(MarshalStream &);

	static void	put_trigger_info(MarshalStream &mstream,
			    InvoTriggers *trigp);

	// Unmarshal Invo triggers.
	static void	get(sendstream *);
	static void	get(recstream *);
	static void	get(MarshalStream &);

	void		get_trigger_info(recstream *);
	void		get_trigger_info(MarshalStream &);

	// Discard trigger information from message
	static void	strip_trigger_info(MarshalStream &mstream);

	// Copy Invo triggers from current thread to waiting thread
	static	void	copy_triggers(InvoTriggers *srcp, InvoTriggers *destp);

	// Set up to handle fault information arriving in reply
	static void	set_up_reply(invocation *invop);

#if defined(_KERNEL) || defined(_KERNEL_ORB)
	//
	// Special case for the gateway's xdoor_proxy() for U->K path.
	// Unmarshal Invo triggers from user-land door data.
	// Note, this will modify da->data_ptr and da->data_size upon return.
	//
	static void	get(door_arg_t *da);

#elif ! defined(_KERNEL_ORB)
	//
	// Special case for solaris xdoor's object_server() for K->U and U->U
	// paths.  Unmarshal Invo triggers from door arguments.
	// Note, this will modify both 'data' and 'data_size' upon return.
	//
	static void	get(char *&data, uint32_t &data_size);
#endif
	//
	// Allocates a buffer for msg header and prepends it to a MarshalStream.
	// Used when the MarshalStream already contains marshaled data but
	// has no space preallocated for headers.
	//
	static void	prepend_header_buf(MarshalStream &);

	// Returns amount of space needed to marshal the header.
	static uint32_t	header_size();

	// Returns amount of space needed to marshal the triggers.
	static uint32_t	triggers_size(InvoTriggers *trigp);

	// Header for marshaling Invo triggers.
	class fi_header_t {
	public:
		fi_header_t(int num, uint32_t len);

		fi_header_t();

#if defined(MARSHAL_DEBUG)
		uint32_t	marshal_debug;	// this must be the 1st word
#endif	// MARSHAL_DEBUG

		// All valid Invo triggers are shipped between nodes
		int		num_triggers;

		//
		// Length of regular marshaled data (placed between
		// this header and Invo triggers).
		//
		uint32_t	data_len;
	};

	//
	// Since Invo Triggers are thread-specific, there's no need for
	// locking.  Define the following lock-related routines from
	// class FaultTriggers with no-op routines.
	//
	void	rdlock();
	void	wrlock();
	void	unlock();
	bool	rdlock_held();
	bool	wrlock_held();
	bool	lock_held();

	//
	// Set and retrieve fault injection data
	// from thread specific data.
	//
	static InvoTriggers	*get_invo_trig_tsd();
	static void		set_invo_trig_tsd(InvoTriggers *);

private:
	InvoTriggers(InvoTriggers *trigp);

	// Disallow constructor without argument
	InvoTriggers();

	// Callback function for marshaling Invo triggers.
	static bool	_put_callback(trigger_t *, void *);

	// Callback function for copying Invo triggers.
	static bool	copy_callback(trigger_t *, void *);

	static void	_tsd_destroy(void *);		// TSD destructor

	// Disallow assignments and pass by value
	InvoTriggers(const InvoTriggers &);
	InvoTriggers & operator = (InvoTriggers &);

	// Thread-specific data.
	static os::tsd	_tsd;

	//
	// InvoTriggers can be stacked. Only the top InvoTriggers is active.
	// This supports server thread reuse without loss of InvoTriggers info
	//
	InvoTriggers	*next;
};

//
// Class NodeTriggers
//
//	Manages collection, deletion, checking, marshaling and unmarshaling
//	of Node triggers (a.k.a. Global triggers).
//	There is one instance of this class per node.
//
class NodeTriggers : public FaultTriggers {
public:
	//
	// Adds a Node trigger to a node (or all nodes).
	// Note: the fault argument is copied.
	//
	static void	add(uint32_t fault_num, void *fault_argp,
			    uint32_t fault_argsize,
			    int nodeid = TRIGGER_THIS_NODE);

	//
	// Clears a Node trigger from a node (or all nodes).
	//
	static void	clear(uint32_t fault_num,
			    int nodeid = TRIGGER_THIS_NODE);

	//
	// Clears all Node triggers of a node (or all nodes).
	//
	static void	clear_all(int nodeid = TRIGGER_THIS_NODE);


	//
	// The members below are for fault injection framework use only.
	//

	~NodeTriggers();

#if !defined(_KERNEL_ORB)
	//
	// Overload (virtual) method in user-land to allow user programs to get
	// NodeTriggers from the kernel table.
	//
	bool	is_triggered(uint32_t fault_num, void **fault_argpp,
		    uint32_t *fault_argsizep);
#endif

	// Returns pointer to this node's Node Triggers.
	static NodeTriggers	&the();

	// Handles remote NodeTriggers::add() request.
	static void	add_rpc_handler(recstream *);

	// Handles remote NodeTriggers::clear() request.
	static void	clear_rpc_handler(recstream *);

	// Handles remote NodeTriggers::clear_all() request.
	static void	clear_all_rpc_handler(recstream *);

	//
	// For user-land programs to get a NodeTrigger (via _cladm()).
	// Called by cl_fault_injection().
	//
	int	get_nodetrig(void *uaddr);

	//
	// NodeTriggers require a real lock
	//
	void	rdlock();
	void	wrlock();
	void	unlock();
	bool	rdlock_held();
	bool	wrlock_held();
	bool	lock_held();

private:
	os::rwlock_t	_rwlock;

	// For user-land programs to get a NodeTrigger (via _cladm()).
	struct _cl_get_nodetrig_t {
		uint32_t	fault_num;	// IN: fault number to get
		uint32_t	fault_argsize;	// IN: size of fault arg buffer
						// OUT: size of fault argument

		//
		// IN: ptr to fault arg buffer.
		// Note: uint64_t so we don't have to deal with data model
		//	 mismatch between kernel- and user-land.
		//
		uint64_t	fault_argp;
	};

	// Stores Node Triggers.
	static NodeTriggers	_the_node_triggers;
};

//
// Class BootTriggers
//
//	Manages collection, marshaling and unmarshaling
//	of Boot triggers.  These triggers will be stored on a file until the
//	next time that the cluster is rebooted, at which time
//	they will be re-loaded and converted into node triggers
//	There is one instance of this class per node.
//
class BootTriggers {
public:
	//
	// Adds a Boot trigger to a node (or all nodes).
	// Note: the fault argument is copied.
	//
	static void	add(uint32_t fault_num, void *fault_argp,
			    uint32_t fault_argsize,
			    int nodeid = TRIGGER_THIS_NODE);

	//
	// Clears all BootTriggers by removing the triggerinfo
	// file.
	//
	static void clear_all();

	//
	// The members below are for fault injection framework use only.
	//
	// Loads stored faults from file into NodeTriggers
	//
	static void	load_faults();

#if defined(_KERNEL_ORB)
	// Handles remote NodeTriggers::add() request.
	static void	add_rpc_handler(recstream *);

private:
	// This structure is used to save and load the faults from a file
	struct _fault_header_t {
		uint32_t	fault_num;
		uint32_t	fault_argsize;
	};

#if	defined(_KERNEL)
	// Keep track of where we are in the bootTriggers file.
	static	size_t	_offset;
#endif
	// Save the fault onto a file
	static void	_save_fault(uint32_t fault_num, void *fault_argp,
			    uint32_t fault_argsize);

#endif	// _KERNEL_ORB
};

#include <orb/fault/fault_injection_in.h>

#else // _FAULT_INJECTION

#define	FAULT_POINT(a, b)
#define	FAULT_POINT_ENV(a, b, c)

#endif // _FAULT_INJECTION

// Include fault numbers and fault functions.
#include <fault_numbers/fault_numbers.h>
#include <orb/fault/fault_functions.h>

// Needed for unref_fi tests.
#include <orb/fault/fault_unref.h>

// Needed for flowcontrol_fi tests
#include <orb/fault/fault_flowcontrol.h>

#endif	/* _FAULT_INJECTION_H */
