//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pathend_state_proxy.cc	1.14	08/05/20 SMI"


#include <orb/query/pathend_state_proxy.h>
#include <orb/transport/transport.h>
#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/sc_syslog_msg.h>
#include <sys/cl_events.h>
#include <sys/cl_eventdefs.h>

//
// Pathend State Proxy
//

//
// constructor
// - get the name of the component
//
pathend_state_proxy::pathend_state_proxy(const char *cmp_name, char *ex_name) :
    component_state_proxy()
{
	ASSERT(cmp_name != NULL);

	// Save the internal and external names
	int_name = os::strdup(cmp_name);
	ext_name = os::strdup(ex_name);

	// initialize the state to unknown
	my_state = pathend::P_CONSTRUCTED;
}

//
// destructor
//
pathend_state_proxy::~pathend_state_proxy()
{
	delete []int_name;
	delete []ext_name;
}


//
// get state
// do the mapping from the internal state to the external state
//
sc_state_code_t
pathend_state_proxy::get_state()
{
	// Static map of internal states to external states
	static const sc_state_code_t	state_map[] = {
	    SC_STATE_WAIT,		// P_CONSTRUCTED
	    SC_STATE_FAULTED,		// P_INITERR
	    SC_STATE_WAIT,		// P_INITIATED
	    SC_STATE_ONLINE,		// P_UP
	    SC_STATE_FAULTED,		// P_CLEANING
	    SC_STATE_FAULTED,		// P_DRAINING
	    SC_STATE_OFFLINE,		// P_DELCLEAN
	    SC_STATE_OFFLINE,		// P_DELETING
	    SC_STATE_OFFLINE		// P_DELETED
	};

	ASSERT(proxy_lock_held());
	return (state_map[my_state]);
}

//
// get state status
// Useful for printing message in the syslog
//
proxy_state_stat_t
pathend_state_proxy::get_state_status()
{
	// Static map of internal states to external state status
	static const proxy_state_stat_t	stat_map[] = {
	    TRANSIENT,		// P_CONSTRUCTED
	    FINAL,		// P_INITERR
	    TRANSIENT,		// P_INITIATED
	    FINAL,		// P_UP
	    TRANSIENT,		// P_CLEANING
	    FINAL,		// P_DRAINING
	    TRANSIENT,		// P_DELCLEAN
	    TRANSIENT,		// P_DELETING
	    FINAL		// P_DELETED
	};

	ASSERT(proxy_lock_held());
	return (stat_map[my_state]);
}

//
// get state string
//
// returns a string that has a mapping from the internal state.
//
const char *
pathend_state_proxy::get_state_string()
{
	// static maps on internal states to external state strings
	static const char	*state_map[] = {
		"Path being constructed",		// P_CONSTRUCTED
		"Path errors during initiation",	// P_INITERR
		"Path being initiated",			// P_INITIATED
		"Path online",				// P_UP
		"Path being cleaned up",		// P_CLEANING
		"Path being drained",			// P_DRAINING
		"Path being deleted and cleaned",	// P_DELCLEAN
		"Path being deleted",			// P_DELETING
		"Path has been deleted" 		// P_DELETED
	};

	ASSERT(proxy_lock_held());
	return (state_map[my_state]);
}

//
// get state format string
//
// returns a format string, that has a mapping from the internal state and
// which is useful in generating a unique message ID for the path state
// when printed in syslog.
//
const char *
pathend_state_proxy::get_state_fmtstring()
{
	// static maps on internal states to external state strings
	static const char	*fmtstate_map[] = {
		// P_CONSTRUCTED

		//
		// SCMSGS
		// @explanation
		// A communication link is being established with another
		// node.
		// @user_action
		// No action required.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s being constructed"),

		// P_INITERR

		//
		// SCMSGS
		// @explanation
		// Communication could not be established over the path. The
		// interconnect might have failed or the remote node might be
		// down.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s errors during initiation"),

		// P_INITIATED

		//
		// SCMSGS
		// @explanation
		// A communication link is being established with another
		// node.
		// @user_action
		// No action required.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s being initiated"),

		// P_UP

		//
		// SCMSGS
		// @explanation
		// A communication link has been established with another
		// node.
		// @user_action
		// No action required.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s online"),

		// P_CLEANING

		//
		// SCMSGS
		// @explanation
		// A communication link is being removed with another node.
		// The interconnect might have failed or the remote node might
		// be down.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s being cleaned up"),

		// P_DRAINING

		//
		// SCMSGS
		// @explanation
		// A communication link is being removed with another node.
		// The interconnect might have failed or the remote node might
		// be down.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s being drained"),

		// P_DELCLEAN

		//
		// SCMSGS
		// @explanation
		// A communication link is being removed with another node.
		// The interconnect might have failed or the remote node might
		// be down.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s being deleted and cleaned"),

		// P_DELETING

		//
		// SCMSGS
		// @explanation
		// A communication link is being removed with another node.
		// The interconnect might have failed or the remote node might
		// be down.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s being deleted"),

		// P_DELETED

		//
		// SCMSGS
		// @explanation
		// A communication link is being removed with another node.
		// The interconnect might have failed or the remote node might
		// be down.
		// @user_action
		// Any interconnect failure should be resolved, and/or the
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Path %s has been deleted")
	};

	ASSERT(proxy_lock_held());
	return (fmtstate_map[my_state]);
}

//
//
//  1. Change the state of the proxy.
//  2. Publish a sysevent
//  3. Post the state change to the cluster syslog
//  Note: May be called in clock interrupt context
//
void
pathend_state_proxy::set_state(int new_state)
{
	const char *path_name = get_extname();
	const char *old_pathend_state = get_state_string();
	const char *new_pathend_state;
	proxy_state_stat_t new_state_stat;

	ASSERT(proxy_lock_held());
	my_state = (pathend::pathend_state)new_state;
	new_pathend_state = get_state_string();
	new_state_stat = get_state_status();

	(void) sc_publish_event(ESC_CLUSTER_TP_PATH_STATE_CHANGE,
	    CL_EVENT_PUB_TP, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM,
	    0, 0, 0,
	    CL_TP_PATH_NAME, SE_DATA_TYPE_STRING, path_name,
	    CL_OLD_STATE, SE_DATA_TYPE_STRING, old_pathend_state,
	    CL_NEW_STATE, SE_DATA_TYPE_STRING, new_pathend_state,
	    0);

	//
	// Print the syslog message only if the state is important enough
	// to be printed. This is a measure to keep syslog cleaner.
	//
	if (new_state_stat == FINAL) {
		post_event_to_syslog(STATE_CHANGED, new_pathend_state);
	}
}

// Get a reference to the internal lock
os::mutex_t &
pathend_state_proxy::get_lock()
{
	/* LINTED */
	return (csp_lock);
}
