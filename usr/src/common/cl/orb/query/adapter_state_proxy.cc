//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)adapter_state_proxy.cc	1.14	08/05/20 SMI"

#include <orb/query/adapter_state_proxy.h>
#include <orb/transport/transport.h>
#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/sc_syslog_msg.h>
#include <sys/cl_events.h>
#include <sys/cl_eventdefs.h>

//
// Adapter State Proxy
//

//
// constructor
// - get the name of the component
//
adapter_state_proxy::adapter_state_proxy(const char *cmp_name, char *ex_name) :
    component_state_proxy()
{
	ASSERT(cmp_name != NULL);

	// Save the internal and external names
	int_name = os::strdup(cmp_name);
	ext_name = os::strdup(ex_name);

	// initialize the state to unknown
	my_state = adapter::A_CONSTRUCTED;
}

//
// destructor
//
adapter_state_proxy::~adapter_state_proxy()
{
	delete []int_name;
	delete []ext_name;
}


//
// get state
// do the mapping from the internal state to the external state
//
sc_state_code_t
adapter_state_proxy::get_state()
{
	// Static map of internal states to external states
	static const sc_state_code_t	state_map[] = {
	    SC_STATE_ONLINE,		// A_CONSTRUCTED
	    SC_STATE_OFFLINE,		// A_FAULTED
	    SC_STATE_OFFLINE		// A_DELETED
	};

	ASSERT(proxy_lock_held());
	return (state_map[my_state]);
}

//
// get state status
// Useful for printing message in the syslog
//
proxy_state_stat_t
adapter_state_proxy::get_state_status()
{
	// Static map of internal states to external states
	static const proxy_state_stat_t	stat_map[] = {
	    FINAL,		// A_CONSTRUCTED
	    FINAL,		// A_FAULTED
	    FINAL		// A_DELETED
	};

	ASSERT(proxy_lock_held());
	return (stat_map[my_state]);
}

//
// get state string
//
// returns a string that has a mapping from the internal state.
// messages need to be modified for adapters
//
const char *
adapter_state_proxy::get_state_string()
{
	// static maps on internal states to external state strings
	static const char	*state_map[] = {
		"Adapter constructed",			// A_CONSTRUCTED
		"Adapter is faulted",			// A_FAULTED
		"Adapter has been disabled" 		// A_DELETED
	};

	ASSERT(proxy_lock_held());
	return (state_map[my_state]);
}

//
// get state format string
//
// returns a format string, that has a mapping from the internal state and
// which is useful in generating a unique message ID for the adapter state
// when printed in syslog.
//
const char *
adapter_state_proxy::get_state_fmtstring()
{
	// static maps on internal states to external state strings
	static const char	*fmtstate_map[] = {

		// A_CONSTRUCTED

		//
		// SCMSGS
		// @explanation
		// A network adapter has been initialized.
		// @user_action
		// No action required.
		//
		SC_SYSLOG_MESSAGE("clcomm: Adapter %s constructed"),

		// A_FAULTED

		//
		// SCMSGS
		// @explanation
		// A network adapter has encountered a fault.
		// @user_action
		// Any interconnect failure should be resolved, and/or a
		// failed node rebooted.
		//
		SC_SYSLOG_MESSAGE("clcomm: Adapter %s is faulted"),

		// A_DELETED (Actually disabled)

		//
		// SCMSGS
		// @explanation
		// A network adapter has been disabled.
		// @user_action
		// No action required.
		//
		SC_SYSLOG_MESSAGE("clcomm: Adapter %s has been disabled")
	};

	ASSERT(proxy_lock_held());
	return (fmtstate_map[my_state]);
}

//
//  1. Change the state of the proxy.
//  2. Publish a sysevent
//  3. Post the state change to the cluster syslog
//  Note: May be called in clock interrupt context
//
void
adapter_state_proxy::set_state(int new_state)
{
	const char *interface_name = get_extname();
	const char *old_interface_state = get_state_string();
	const char *new_interface_state;
	proxy_state_stat_t new_state_stat;

	ASSERT(proxy_lock_held());
	my_state = (adapter::adapter_state)new_state;
	new_interface_state = get_state_string();
	new_state_stat = get_state_status();

	(void) sc_publish_event(ESC_CLUSTER_TP_IF_STATE_CHANGE,
	    CL_EVENT_PUB_TP, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM,
	    0, 0, 0,
	    CL_TP_IF_NAME, SE_DATA_TYPE_STRING, interface_name,
	    CL_OLD_STATE, SE_DATA_TYPE_STRING, old_interface_state,
	    CL_NEW_STATE, SE_DATA_TYPE_STRING, new_interface_state,
	    0);

	//
	// Print the syslog message only if the state is important enough
	// to be printed. This is a measure to keep syslog cleaner.
	// Although adapter_state_proxy does not print any redundant
	// messages, this is to keep parity with pathend_state_proxy.
	//
	if (new_state_stat == FINAL) {
		post_event_to_syslog(STATE_CHANGED, new_interface_state);
	}
}

// Get a reference to the internal lock
os::mutex_t &
adapter_state_proxy::get_lock()
{
	/* LINTED */
	return (csp_lock);
}
