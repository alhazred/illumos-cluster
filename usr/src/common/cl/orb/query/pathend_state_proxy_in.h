/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  pathend_state_proxy_in.h
 *
 */

#ifndef _PATHEND_STATE_PROXY_IN_H
#define	_PATHEND_STATE_PROXY_IN_H

#pragma ident	"@(#)pathend_state_proxy_in.h	1.6	08/05/20 SMI"

//
// Pathend State proxy
//
inline const char *
pathend_state_proxy::get_name()
{
	ASSERT(int_name != NULL);

	return (int_name);
}

inline const char *
pathend_state_proxy::get_extname()
{
	ASSERT(ext_name != NULL);

	return (ext_name);
}

inline component_state_proxy::component_type_t
pathend_state_proxy::_type()
{
	return (COMP_PATHEND);
}

#endif	/* _PATHEND_STATE_PROXY_IN_H */
