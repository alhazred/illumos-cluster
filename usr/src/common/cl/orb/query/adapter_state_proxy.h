/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ADAPTER_STATE_PROXY_H
#define	_ADAPTER_STATE_PROXY_H

#pragma ident	"@(#)adapter_state_proxy.h	1.9	08/05/20 SMI"

#include <clconf/comp_state_proxy.h>

class adapter_state_proxy : public component_state_proxy {
public:
	// constructor
	// - set the name of the component
	adapter_state_proxy(const char *cmp_name, char *ex_name);

	// destructor
	~adapter_state_proxy();

	// Name is stored in the object
	// invoked with lock held
	const char *get_name();

	// Get the external name of the object.
	const char		*get_extname();

	// Get external state
	// invoked with lock held
	sc_state_code_t get_state();

	// Get the state status
	proxy_state_stat_t get_state_status();

	// Get state string
	// invoked with lock held
	const char *get_state_string();

	// Get state format string, used to post to syslog
	const char 		*get_state_fmtstring();

	// Set the internal state
	// invoked with lock held
	void set_state(int new_state);

	// Get a reference to the internal lock
	os::mutex_t &get_lock();

private:
	// Returns the type of component that this proxy is monitoring
	component_type_t 	_type();

	// adapter internal name
	char			*int_name;

	// adapter external name
	char			*ext_name;

	// Internal state
	int			my_state;

	// Disallow assignments and pass by value
	adapter_state_proxy(const adapter_state_proxy &);
	adapter_state_proxy &operator = (adapter_state_proxy &);

}; // adapter_state_proxy

#include <orb/query/adapter_state_proxy_in.h>

#endif	/* _ADAPTER_STATE_PROXY_H */
