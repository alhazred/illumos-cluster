/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  pathend_state_proxy.h
 *
 */

#ifndef _PATHEND_STATE_PROXY_H
#define	_PATHEND_STATE_PROXY_H

#pragma ident	"@(#)pathend_state_proxy.h	1.9	08/05/20 SMI"

#include <clconf/comp_state_proxy.h>

//
// XXX - The following code is a sample usage of the query interface proxy
// framework.  This class is only an excercise, and not the true implementation
// of the pathend state proxy
//
// A Sample implmentation of a proxy for a specific component
//
class pathend_state_proxy : public component_state_proxy {
public:
	// constructor
	// - set the name of the component
	pathend_state_proxy(const char *cmp_name, char *ex_name);

	// destructor
	~pathend_state_proxy();

	// Name is stored in the object
	// invoked with lock held
	const char *get_name();

	// Get the state status
	proxy_state_stat_t get_state_status();

	// Get the external name of the object.
	const char 		*get_extname();

	// Get external state
	// invoked with lock held
	sc_state_code_t get_state();

	// Get state string
	// invoked with lock held
	const char *get_state_string();

	// Get state format string, used to post to syslog
	const char 		*get_state_fmtstring();

	// Set the internal state
	// invoked with lock held
	void set_state(int new_state);

	// Get a reference to the internal lock
	os::mutex_t &get_lock();

protected:
	// Returns the type of component that this proxy is monitoring
	component_type_t _type();

private:
	// pathend internal name
	char			*int_name;

	// pathend external name
	char			*ext_name;

	// Internal state
	int			my_state;

	// Disallow assignments and pass by value
	pathend_state_proxy(const pathend_state_proxy &);
	pathend_state_proxy &operator = (pathend_state_proxy &);

}; // pathend_state_proxy

#include <orb/query/pathend_state_proxy_in.h>

#endif	/* _PATHEND_STATE_PROXY_H */
