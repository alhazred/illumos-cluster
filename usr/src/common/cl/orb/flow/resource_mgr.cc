//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)resource_mgr.cc	1.30	08/05/20 SMI"

#include <orb/member/members.h>
#include <orb/flow/resource_mgr.h>
#include <orb/flow/resource_pool.h>
#include <orb/flow/resource_balancer.h>
#include <orb/flow/resource_done.h>
#include <orb/debug/orb_trace.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/orb_conf.h>

//
// Class resource_mgr methods
//

//
// handle_messages - process incoming flow control messages,
// and hand message data to appropriate handling routine.
//
// static
void
resource_mgr::handle_messages(recstream *recstreamp)
{
	ID_node		&src_node = recstreamp->get_src_node();
	MarshalStream   &recmarsh = recstreamp->get_MainBuf();

	// It would be nice to check for an orphan message here.
	// Unfortunately, the msg could be orphanned between here
	// and where some data structure is locked for update.

	// A very weak attempt to avoid using global data if we are
	// shutting down
	if (!ORB::is_shutdown()) {
		// Determine message type
		switch ((flow_msg_t)recmarsh.get_octet()) {
		case ONEWAY_NOTICES:
			resource_mgr::process_oneways_done(src_node, recmarsh);
			break;
		case RESOURCE_GRANT:
			resource_mgr::process_resource_grant(src_node,
			    recmarsh);
			break;
		case RESOURCE_POLICY:
			resource_mgr::process_resource_policy(src_node,
			    recmarsh);
			break;
		case RESOURCE_RELEASE_REQ:
			resource_balancer::the().
			    process_resource_release(src_node, recmarsh, true);
			break;
		case RESOURCE_RELEASE:
			resource_balancer::the().
			    process_resource_release(src_node, recmarsh,
			    false);
			break;
		case RESOURCE_RECALL:
			resource_mgr::process_resource_recall(src_node,
			    recmarsh);
			break;
		case REQUEST_RESOURCES:
			resource_balancer::the().
			    process_request_resources(src_node, recmarsh);
			break;
		case RESOURCE_RELEASE_ACK:
			resource_mgr::
			    process_resource_release_ack(src_node, recmarsh);
			break;
		default:
			// Unknown flow control message type
			ASSERT(0);
		}
	}
	recstreamp->done();
}

//
// process_oneways_done - the server node has sent a message containing
// information about resources released by the completions of oneway
// messages for a specified resource pool.
//
// static
void
resource_mgr::process_oneways_done(ID_node &src_node, MarshalStream &recmarsh)
{
	resource_grant	done1s;

	// Unmarshal a record containing information about released resources
	recmarsh.get_bytes((void *)&done1s, sizeof (resource_grant));

	ORB_DBPRINTF(ORB_TRACE_ONE_FLOW,
	    ("process_oneways_done (%d.%3d,%d) threads %d\n",
	    src_node.ndid, (src_node.incn % 1000),
	    done1s.pool, done1s.threads));

	// Select resource pool for processing oneway done information
	resource_pool::resource_pools[src_node.ndid][done1s.pool].
	    process_oneways_done(src_node, done1s);
}


//
// process_resource_grant - the server node has sent a message containing
// information about new resources being granted to a resource pool.
//
// static
void
resource_mgr::process_resource_grant(ID_node &src_node, MarshalStream &recmarsh)
{
	resource_grant	grant;

	// Unmarshal a record containing information about new resources
	recmarsh.get_bytes((void *)&grant, sizeof (resource_grant));

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("process_resource_grant (%d.%3d,%d) threads %d\n",
	    src_node.ndid, (src_node.incn % 1000), grant.pool, grant.threads));

	// Select resource pool for processing oneway done information
	resource_pool::resource_pools[src_node.ndid][grant.pool].
	    process_resource_grant(src_node, grant);
}

//
// process_resource_policy - the server node has sent a message containing
// parameters that control flow control.
//
// static
void
resource_mgr::process_resource_policy(ID_node &src_node,
    MarshalStream &recmarsh)
{
	resource_policy		policy;

	// Unmarshal a record containing information about new resources
	recmarsh.get_bytes((void *)&policy, sizeof (resource_policy));

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("process_resource_policy (%d.%3d,%d)\n",
	    src_node.ndid, (src_node.incn % 1000), policy.pool));

	// Select resource pool for processing oneway done information
	resource_pool::resource_pools[src_node.ndid][policy.pool].
	    process_resource_policy(src_node, policy);
}

//
// process_resource_recall - the server node has sent a message ordering the
// client to release resources.
//
// static
void
resource_mgr::process_resource_recall(ID_node &src_node,
    MarshalStream &recmarsh)
{
	resource_grant	grant;

	// Unmarshal a record containing information about new resources
	recmarsh.get_bytes((void *)&grant, sizeof (resource_grant));

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("process_resource_recall (%d.%3d,%d) threads %d\n",
	    src_node.ndid, (src_node.incn % 1000), grant.pool, grant.threads));

	// Select resource pool for processing oneway done information
	resource_pool::resource_pools[src_node.ndid][grant.pool].
	    process_resource_recall(src_node, grant);
}

//
// process_resource_release_ack - the server node has sent a message to
// acknowledge to the client the release resources.
//
// static
void
resource_mgr::process_resource_release_ack(ID_node &src_node,
    MarshalStream &recmarsh)
{
	resource_grant	grant;

	// Unmarshal a record containing information about new resources
	recmarsh.get_bytes((void *)&grant, sizeof (resource_grant));

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("process_resource_release_ack (%d.%3d,%d) threads %d\n",
	    src_node.ndid, (src_node.incn % 1000), grant.pool, grant.threads));

	resource_pool::resource_pools[src_node.ndid][grant.pool].
	    process_resource_release_ack(src_node);
}

//
// send_message - utility method for transmitting a message to a remote
// resource manager.
//
// static
void
resource_mgr::send_message(ID_node &dest_node, flow_msg_t msgtype, void *argbuf,
    uint_t argsize)
{
	Environment	e;

	ASSERT(dest_node.incn != INCN_UNKNOWN);

	bool retry_needed = false;
	// Loop if retry is needed.
	do {

		// Identify message resources
		resources_datagram resource(&e,
		    0,			// header size
		    1 + argsize,	// data size (msg type + payload)
		    dest_node, FLOW_MSG);

		//
		// Check whether message can proceed now.
		// System does not block flow control messages.
		// The send stream is allocated when the msg can proceed.
		//
		sendstream *sendstreamp = orb_msg::reserve_resources(&resource);

		if (sendstreamp == NULL) {
			// This should only occur when the node is dead.
			// Node reconfiguration will perform the cleanup.
			ASSERT(e.exception());
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			return;
		}

		// Identify flow control message type
		sendstreamp->put_octet((uint8_t)msgtype);

		// Load the data payload
		sendstreamp->put_bytes(argbuf, argsize);

		retry_needed = orb_msg::send_datagram(sendstreamp);

	} while (retry_needed);
}

//
// initialize - create global data structures used by resource_mgr and
// resource_balancer.
//
// static
int
resource_mgr::initialize()
{
	// Initialize the resource_dones
	int ret = resource_done::initialize();
	if (ret != 0) {
		return (ret);
	}
	ret = resource_pool::initialize();
	if (ret != 0) {
		return (ret);
	}
	if ((ret = resource_balancer::initialize()) != 0) {
		return (ret);
	}
	return (0);
}

//
// shutdown - create global data structures used by resource_mgr and
// resource_balancer.
//
// static
void
resource_mgr::shutdown()
{
	resource_balancer::shutdown();
	resource_done::shutdown();
	resource_pool::shutdown();
}

//
// reconfig_reallocate_resources - perform resource management work
// that cannot occur during vm_bootstrap_calc_step(), which forbids
// inter-node communications.
//
// static
void
resource_mgr::reconfig_reallocate_resources()
{
	//
	// The resource balancer hands out threads freed by
	// nodes that died in step 1.
	// The resource_balancer initiates contact with new clients.
	//
	resource_balancer::the().reconfig_reallocate_resources();
}

//
// marshal - information from the resources object identifying resources
// usage is marshalled into the sendstream. The resource_mgr on the server
// extracts this information.
//
// static
void
oneway_flow_header::marshal(sendstream *sendstreamp)
{
	oneway_flow_header	*headerp = (oneway_flow_header *)sendstreamp->
	    make_header(sizeof (oneway_flow_header));
	ASSERT(headerp != NULL);
	headerp->pool = sendstreamp->get_resourcep()->get_pool();
#ifdef	DRC_DEBUG
	void *rp = sendstreamp->get_resourcep();
#endif
	ORB_DBPRINTF(ORB_TRACE_ONE_FLOW,
	    ("oneway_flow_header::marshal msg(%p)\n", rp));
}

//
// unmarshal - information from the oneway flow control header is unmarshalled,
// and stored in the recstream.
//
// static
resource_defs::resource_pool_t
oneway_flow_header::unmarshal(recstream *recstreamp)
{
	oneway_flow_header	header;

	recstreamp->read_header((void *)&header, sizeof (header));

	ORB_DBPRINTF(ORB_TRACE_ONE_FLOW, ("oneway_flow_header::unmarshal\n"));

	return (header.pool);
}
