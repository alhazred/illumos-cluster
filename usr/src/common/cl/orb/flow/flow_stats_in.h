/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  flow_stats_in.h
 *
 */

#ifndef _FLOW_STATS_IN_H
#define	_FLOW_STATS_IN_H

#pragma ident	"@(#)flow_stats_in.h	1.5	08/05/20 SMI"

inline void
flow_stats::inc_msg_by_policy(resources::flow_policy_t policy)
{
	msg_by_policy[policy]++;
}

inline void
flow_stats::inc_blocked()
{
	blocked++;
}

inline void
flow_stats::inc_unblocked()
{
	unblocked++;
}

inline void
flow_stats::inc_reject()
{
	reject++;
}

inline void
flow_stats::inc_pool_requests()
{
	pool_requests++;
}

inline void
flow_stats::inc_pool_grants()
{
	pool_grants++;
}

inline void
flow_stats::inc_pool_denies()
{
	pool_denies++;
}

inline void
flow_stats::inc_pool_recalls()
{
	pool_recalls++;
}

inline void
flow_stats::inc_pool_releases()
{
	pool_releases++;
}

inline void
flow_stats::inc_balancer_requests()
{
	balancer_requests++;
}

inline void
flow_stats::inc_balancer_grants()
{
	balancer_grants++;
}

inline void
flow_stats::inc_balancer_denies()
{
	balancer_denies++;
}

inline void
flow_stats::inc_balancer_recalls()
{
	balancer_recalls++;
}

inline void
flow_stats::inc_balancer_releases()
{
	balancer_releases++;
}

#endif	/* _FLOW_STATS_IN_H */
