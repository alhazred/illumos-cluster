//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)resource_done.cc	1.8	08/05/20 SMI"

#include <orb/member/members.h>
#include <orb/flow/resource_done.h>
#include <orb/flow/resource_pool.h>
#include <orb/flow/resource_balancer.h>
#include <orb/debug/orb_trace.h>
#include <orb/msg/orb_msg.h>

resource_done *resource_done::resource_dones[NODEID_MAX+1] = {NULL};

//
// Class resource_done methods
//

resource_done::resource_done() :
	my_incn(INCN_UNKNOWN),
	last_period_had_dones(false),
	flushed_this_period(false)
{
	// Show that initialization has not occurred
	done_record.pool = resource_defs::NUMBER_POOLS;

	// No oneways have completed for each resource pool
	done_record.threads = 0;
}

//
// init - When an incarnation number change occurs, the method
// initializes internal data.
//
void
resource_done::init(nodeid_t node, resource_defs::resource_pool_t pool)
{
	lck.lock();
	// Check to see whether system needs to initialize the data structure
	incarnation_num		new_incn = members::the().incn(node);
	if (my_incn == new_incn) {
		// There was no change, and hence current values remain valid
		lck.unlock();
		return;
	}
	// Record the currently supported incarnation number
	my_incn = new_incn;

	// The pool does not change once initialized
	ASSERT(done_record.pool == resource_defs::NUMBER_POOLS ||
	    done_record.pool == pool);
	done_record.pool = pool;

	//
	// The resource_done only accepts information having an
	// incarnation number that matches its own.
	//
	// At this point in time the object may have already batched
	// information about resources freed by completed
	// oneway invocations from the previous node incarnation.
	// Purge this information.
	//
	done_record.threads = 0;

	last_period_had_dones = false;
	flushed_this_period = false;

	lck.unlock();
}


//
// initialize
//
// Create resource_dones data structure.
//
int
resource_done::initialize()
{
	ASSERT(resource_dones[0] == NULL);

	for (int i = 1; i < NODEID_MAX + 1; i++) {
		resource_dones[i] =
		    new resource_done[resource_defs::NUMBER_POOLS];
		if (resource_dones[i] == NULL) {
			for (int j = i-1; j > 0; j--) {
				delete resource_dones[j];
			}
			return (ENOMEM);
		}
	}
	// Unused location set toindicate that array has been
	// initialized.
	resource_dones[0] = resource_dones[1];
	return (0);
}

//
// shutdown
//
// Delete resource_dones
//
void
resource_done::shutdown()
{
	if (resource_dones[0]) {
		for (int i = 1; i < NODEID_MAX + 1; i++) {
			delete resource_dones[i];
		}
	}
}

//
// oneway_done - a oneway request has completed work on the server and
// no longer needs its resources. Notices to the client are batched.
//
void
resource_done::oneway_done(ID_node &src_node)
{
	lck.lock();

	// Discard information about resources released by orphan oneways
	if (my_incn != src_node.incn) {
		lck.unlock();
		return;
	}
	//
	// Batch the information about resources released by oneway dones
	// XXX - currently only server threads are managed.
	//
	done_record.threads++;
	ASSERT(done_record.threads != 0);	// check for overflow

	ORB_DBPRINTF(ORB_TRACE_ONE_FLOW,
	    ("oneway_done (%d.%3d,%d) threads %d\n",
	    src_node.ndid, (src_node.incn % 1000),
	    done_record.pool, done_record.threads));

	if (done_record.threads <
	    resource_balancer::the().threads_increment(done_record.pool)) {
		// Do not transmit at this time
		lck.unlock();
		return;
	}

	// Copy the done record so that we can drop the lock
	// while sending the message.
	resource_grant	grant(done_record);

	// Reset information about oneway freed resources
	done_record.threads = 0;

	flushed_this_period = true;

	lck.unlock();

	ORB_DBPRINTF(ORB_TRACE_ONE_FLOW,
	    ("oneway_done sending msg\n"));

	resource_mgr::send_message(src_node, resource_mgr::ONEWAY_NOTICES,
	    (void *)&grant, (uint_t)sizeof (resource_grant));
}

//
// flush_notices - flush oneway done notices that have been stuck for
// an extended period.
//
void
resource_done::flush_notices(nodeid_t node)
{
	lck.lock();

	if (!last_period_had_dones || flushed_this_period) {
		// There are no done notices left over from the last period

		// Reset flags
		last_period_had_dones = false;
		flushed_this_period = false;

		lck.unlock();
		return;
	}
	// There are no done notices left over from the last period

	// Reset flags
	last_period_had_dones = false;
	flushed_this_period = false;

	// Copy the done record so that we can drop the lock
	// while sending the message.
	resource_grant	grant(done_record);

	// Reset information about oneway freed resources
	done_record.threads = 0;

	lck.unlock();

	ORB_DBPRINTF(ORB_TRACE_ONE_FLOW,
	    ("flush_notices sending msg\n"));

	ID_node		src_node(node, my_incn);

	resource_mgr::send_message(src_node, resource_mgr::ONEWAY_NOTICES,
	    (void *)&grant, (uint_t)sizeof (resource_grant));
}
