/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RESOURCE_DONE_H
#define	_RESOURCE_DONE_H

#pragma ident	"@(#)resource_done.h	1.8	08/05/20 SMI"

//
// This module manages the server side completion of oneway messages.
//

#include <orb/invo/common.h>
#include <sys/os.h>
#include <orb/flow/resource_defs.h>
#include <orb/flow/resource_mgr.h>

//
// resource_done - this object batches oneway completion notices,
// forwards the information to the client node when enough have occurred.
//
class resource_done {
public:
	resource_done();

	static resource_done	*select_resource_done(nodeid_t node,
				    resource_defs::resource_pool_t pool);

	void			init(nodeid_t node,
				    resource_defs::resource_pool_t pool);

	void			oneway_done(ID_node &src_node);

	void			flush_notices(nodeid_t node);

	static int		initialize();
	static void		shutdown();

private:
	//
	// The system does not guarantee that a node died will occur between
	// two different incarnations of a node. Therefore we must remember
	// the old incarnation number so that we can detect a
	// new node incarnation
	//
	incarnation_num		my_incn;

	resource_grant		done_record;

	//
	// Periodically the system must flush done notices in cases where
	// the batch number is not reached in a time period.
	// These two boolean values support flushing.
	//
	bool			last_period_had_dones;
	bool			flushed_this_period;

	os::mutex_t		lck;

	// Disallow assignments and pass by value
	resource_done(const resource_done &);
	resource_done & operator = (resource_done &);

	//
	// Each resource_done manages the oneway completion notices
	// for one resource pool belonging to one client node.
	//
	static resource_done	*resource_dones[NODEID_MAX + 1];
};


#include <orb/flow/resource_done_in.h>

#endif	/* _RESOURCE_DONE_H */
