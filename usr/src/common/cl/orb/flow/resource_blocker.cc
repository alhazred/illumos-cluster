//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)resource_blocker.cc 1.18    08/05/20 SMI"

#include <orb/member/Node.h>
#include <orb/flow/resource.h>
#include <orb/flow/resource_blocker.h>
#include <orb/flow/resource_pool.h>
#include <orb/flow/resource_defs.h>
#include <orb/debug/orb_trace.h>

//
// There is one of these objects per client node.
//
resource_blocker	*resource_blocker::the_resource_blocker = NULL;

//
// Class resource_blocker methods
//

int
resource_blocker::initialize()
{
	if (the_resource_blocker == NULL) {
		the_resource_blocker = new resource_blocker;
		if (the_resource_blocker == NULL) {
			return (ENOMEM);
		}
	}
	return (0);
}

void
resource_blocker::shutdown()
{
	if (the_resource_blocker) {
		delete the_resource_blocker;
		the_resource_blocker = NULL;
	}
}
//
// disable_blocking - Any blocked message belonging the specified request group
// is rejected with the specified exception.
// This command remains in effect till removed.
//
// Each and every disable must be removed, and preferrably as soon as possible.
// The subsystem optimizes performance for the case when no cancels are
// in effect.
//
// This command is idempotent.
//
// Only WOULDBLOCK or COMM_FAILURE are pemitted as the desired exception.
//
void
resource_blocker::disable_flow_blocking(void *group,
    CORBA::SystemException &exception)
{
	ASSERT(CORBA::WOULDBLOCK::_exnarrow(&exception) != NULL ||
	    CORBA::COMM_FAILURE::_exnarrow(&exception) != NULL);

	lck.lock();

	if (number_disables != 0) {
		//
		// There are outstanding disable requests
		//
		disable_request		*requestp = search_for_disable(group);
		if (requestp != NULL) {
			//
			// Blocking was already disabled
			//
			ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
			    ("resource_blocker: multiple disable on group %p\n",
			    group));

			//
			// Should use the same error code
			//
			ASSERT(requestp->
			    disable_exception.type_match(&exception));

			//
			// No need to reject requests twice
			//
			lck.unlock();
			return;
		}
	}

	disable_request	*disablep = new disable_request(group, exception);

	number_disables++;

	// Determine correct bucket/lock
	int	which = hashslot(group);

	//
	// Add the cancel to the correct bucket.
	// This will prevent subsequent message requests belonging
	// to this request group from blocking.
	//
	buckets[which].append(disablep);

	//
	// Once the lock is dropped, the disable can go away at any time.
	// This complicates the process of rejecting already blocked requests,
	// because the system should not reject requests after the disable
	// goes away.
	//
	// System design requires that the resource_pool lock be
	// acquired before the resource_blocker lock.
	//
	// Holding the resource_blocker lock while scanning all resource_pool's
	// would block all outbound flow controlled messages. Clearly that
	// is undesirable. So the design drops the lock now.
	//
	lck.unlock();

	//
	// Reject already blocked requests in each pool belonging
	// to the specified request group.
	//
	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		for (int pool = 0; pool < resource_defs::NUMBER_POOLS; pool++) {
			resource_pool::resource_pools[node][pool].
			    disable_blocking(group);
		}
	}
}

//
// confirm_and_reject - the resource_pool found a message request belonging
// to a request group that had blocking disabled. Determine whether
// blocking is currently disabled for that request group. Tell the
// resource_pool to go ahead and reject the message when blocking is
// still disabled. Note the possibility exists that blocking for the
// request group may have been disabled, enabled, and then disabled
// again while searching went on for blocked messages. This is both OK
// and the reason why we obtain the type of exception from the data
// structures of the resource_blocker. Locking protects this operation,
// so that we guarantee that a message is only rejected while blocking
// has been disabled.
//
void
resource_blocker::confirm_and_reject(void *group, resource_pool *poolp,
    resources *resourcep)
{
	// First check for any disables requests
	//
	// One can check this variable without holding the lock.
	// The key is that the system obtains the word value of
	// number_disables atomically. Any competing change appears to
	// occur either before or after this test.
	//
	if (number_disables == 0) {
		// Blocking was re-enabled
		return;
	}

	lck.lock();

	disable_request	*disablep = search_for_disable(group);
	if (disablep == NULL) {
		// Blocking was re-enabled
		lck.unlock();
		return;
	}

	//
	// Blocking is still disabled for this request group.
	// Reject the request.
	//
	poolp->reject_blocked_request(resourcep, disablep->disable_exception);
	lck.unlock();
}

//
// enable_blocking - Enable blocking for the specified request group.
//
// This command is idempotent.
//
void
resource_blocker::enable_flow_blocking(void *group)
{
	// First check for any disables requests
	//
	// This command can be issued multiple times.
	// After the first time, this command becomes a no-op.
	//
	// One can check this variable without holding the lock.
	// The key is that the system obtains the word value of
	// number_disables atomically. Any competing change appears to
	// occur either before or after this test.
	//
	if (number_disables == 0) {
		// Blocking was already enabled
		ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
		    ("resource_blocker: enable on enabled group %p\n",
		    group));
		return;
	}

	lck.lock();

	disable_request	*disablep = search_for_disable(group);
	if (disablep == NULL) {
		// Blocking was already enabled
		ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
		    ("resource_blocker: enable on enabled group %p\n",
		    group));
		lck.unlock();
		return;
	}

	ASSERT(number_disables != 0);
	number_disables--;

	// Determine correct bucket/lock
	int	which = hashslot(group);

	// Remove the disable request from the correct bucket
	(void) buckets[which].erase(disablep);
	lck.unlock();

	delete disablep;
}

//
// find - returns the pointer to the disable_request object for a request group
// if one exists. Otherwise, the method returns NULL.
//
disable_request *
resource_blocker::find_disable(void *group)
{
	// First check for any disables requests
	//
	// The vast majority of the time will see the system having
	// no outstanding disables. Therefore optimize for normal case.
	//
	// One can check this variable without holding the lock.
	// The key is that the system obtains the word value of
	// number_disables atomically. Any competing change appears to
	// occur either before or after this test.
	//
	if (number_disables == 0) {
		return (NULL);
	}

	lck.lock();
	disable_request	*requestp = search_for_disable(group);
	lck.unlock();
	return (requestp);
}

//
// search_for_disable - performs the search for a disable request.
//
disable_request *
resource_blocker::search_for_disable(void *group)
{
	ASSERT(lck.lock_held());

	// Determine correct bucket/lock
	int	which = hashslot(group);

	disable_request	*requestp;

	// Search the bucket list for a match
	buckets[which].atfirst();
	while (NULL != (requestp = buckets[which].get_current())) {
		if (group == requestp->request_group) {
			// Request group is identified as cancelled
			return (requestp);
		}
		buckets[which].advance();
	}
	// Request group not identified as cancelled
	return (NULL);
}
