//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)resource_pool.cc	1.56	08/05/20 SMI"

#include <orb/flow/resource_pool.h>
#include <orb/flow/resource.h>
#include <orb/member/members.h>
#include <orb/debug/orb_trace.h>
#include <orb/flow/resource_blocker.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/os.h>
#include <orb/debug/orb_stats_mgr.h>
#include <orb/debug/orbadmin_state.h>
#include <orb/fault/fault_injection.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif

resource_pool *resource_pool::resource_pools[NODEID_MAX+1] = {NULL};

//
// Class resource_pool methods
//

resource_pool::resource_pool() :
    thread_total(0),		// Show node down.
    thread_free(INT_MAX),	// Show infinite resources to allow all
				// requests to go through to transport, where
				// the transport will reject the msg.
    thread_needed(0),		// Show nothing blocked.
    thread_low(0),		//
    thread_moderate(0),		//
    thread_high(0),		//
    thread_increment(1),	//
    thread_torecall(0),		// No recall
    thread_torelease(0),	// No threads for pending recall
    thread_free_min(0),		// Initially no free threads
    pool(resource_defs::NUMBER_POOLS),	// Value is set after node becomes alive
    allow_request_additional(false),	// Disable requests
    resource_request_denied(false),	// No denial happened
    server_node(NODEID_UNKNOWN, INCN_UNKNOWN)
{
	vp_version.major_num = 1;
	vp_version.minor_num = 0;
}

//
// initialize
//
// Create resource_dones data structure.
//
int
resource_pool::initialize()
{
	ASSERT(resource_pools[0] == NULL);
	for (int i = 1; i < NODEID_MAX + 1; i++) {
		resource_pools[i] =
		    new resource_pool[resource_defs::NUMBER_POOLS];
		if (resource_pools[i] == NULL) {
			for (int j = i-1; j > 0; j--) {
				delete resource_pools[j];
			}
			return (ENOMEM);
		}
	}
	// Unused location in the array is used to indicate that array
	// has been initialized.
	resource_pools[0] = resource_pools[1];
	return (0);
}

//
// shutdown
//
// Delete resource_pools
//
void
resource_pool::shutdown()
{
	if (resource_pools[0]) {
		for (int i = 1; i < NODEID_MAX + 1; i++) {
			delete resource_pools[i];
		}
	}
}

//
// resource_allocator - Checks resource availability.
// If resources are not immediately available, and the request is non-blocking
// return with error. Otherwise wait until resources are available.
// Resources are reserved via accounting.
//
void
resource_pool::resource_allocator(resources *resourcep)
{
	lck.lock();
	//
	// The resource pool information is valid for precisely
	// one incarnation of the server node.
	ID_node		*nodep = resourcep->get_nodep();
	if (nodep->incn == INCN_UNKNOWN) {
		//
		// Some message requests, such as those using
		// simple xdoors, do not specify the incarnation number;
		// but flow control and the transport must.
		//
		// There is a small window when the system has published
		// a new membership list, but the reconfiguration process
		// has not yet informed the resource pool. This code
		// effectively handles that condition by pretending that the
		// request was processed just before the node reconfiguration.
		//
		// All blocked requests are purged when an incarnation number
		// changes. This action avoids the case where the system
		// attempts to transmit a message with INCN_UNKNOWN bound
		// to the wrong incarnation number.
		//
		nodep->incn = server_node.incn;
	} else if (nodep->incn != server_node.incn) {
		//
		// The destination is not up. Do not allocate resources.
		// Fail the request.
		//
		// XXX - There is a small window when the system has published
		// a new membership list, but the reconfiguration process
		// has not yet informed the resource pool. If someone sends
		// a message to the new incarnation number during this small
		// window, the message will be rejected incorrectly.
		// A number system design properties currently prevent
		// this from being a problem. Since the marshalling of objects
		// is blocked during this window, the client node cannot
		// learn about objects having the new incarnation number.
		// This means that invocation request/reply, reference counting,
		// signals, and checkpoint messages will not hit the problem.
		// Flow control messages are protected by the reconfiguration
		// process. CMM messages ignore incarnation numbers and are not
		// flow controlled anyway. The fault injection messages could
		// hit this problem under contrived circumstances. However,
		// fault injection messages are not flow controlled and that
		// message type has other potential problems.
		//
		ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
		    ("msg(%p) invalid incn to (%d.%3d,%d)\n",
		    resourcep, nodep->ndid, (nodep->incn % 1000),
		    resourcep->get_pool()));

		resourcep->envp->system_exception(CORBA::COMM_FAILURE
		    (0, CORBA::COMPLETED_NO));
		lck.unlock();
		return;
	}
	if (!resources_available(resourcep)) {
		//
		// All resources are not available.
		//
		if (resourcep->envp->is_nonblocking()) {
			ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
			    ("msg(%p) block not allowed\n",
			    resourcep));

			orb_stats_mgr::the().inc_reject(server_node.ndid);

			// The request does not allow blocking
			resourcep->envp->system_exception(CORBA::WOULDBLOCK
			    (0, CORBA::COMPLETED_NO));
			lck.unlock();
			return;
		};

		disable_request	*disablep = resource_blocker::the().
		    find_disable(resourcep->get_request_group());
		if (disablep != NULL) {
			ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
			    ("msg(%p) block disabled. group: %p\n",
			    resourcep, resourcep->get_request_group()));

			orb_stats_mgr::the().inc_reject(server_node.ndid);

			// Blocking is disabled for this request group
			resourcep->envp->system_exception(
			    disablep->disable_exception);
			lck.unlock();
			return;
		};

		resourcep->state = resources::BLOCKED;

		// Catch an attempt to block awaiting resources for a dead node
		ASSERT(server_node.incn != INCN_UNKNOWN);

		// Enqueue the request pending resource availability or cancel
		pending_list.append(resourcep);

		orb_stats_mgr::the().inc_blocked(server_node.ndid);

		thread_needed++;
		ASSERT(thread_needed > 0);

		ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
		    ("msg(%p) blocked. (%d.%3d,%d) t %d need %d\n",
		    resourcep, server_node.ndid, (server_node.incn % 1000),
		    pool, thread_total, thread_needed));

		// Inform resource management that we are out of resources
		lck.unlock();
		need_more_resources();
		lck.lock();

		// Wait for flow control to process request
		while (resourcep->state == resources::BLOCKED) {
			resourcep->cv.wait(&lck);
		}
		// The resource object is no longer on the pending_list
		// The count of threads needed is adjusted when the
		// system takes the request off the pending_list.

		orb_stats_mgr::the().inc_unblocked(server_node.ndid);
	} else {
		//
		// Setting up the flowcontrol_fi test for the resources
		// available path.
		//
		FAULTPT_FLOWCONTROL(FAULTNUM_FLOWCONTROL_RESOURCE_ALLOCATOR_1,
			fault_flowcontrol::generic_test_op);

		// All resources are available, so allocate them.
		resources_allocate(resourcep);
		resourcep->state = resources::ALLOCATED;

		// Adjust the minimum free thread count this period
		if (thread_free_min > thread_free) {
			thread_free_min = thread_free;
		}
	}
	lck.unlock();
}

//
// select_release_message - utility method for selecting the correct
// RESOURCE_RELEASE or REQUEST_RESOURCE_RELEASE message to a remote
// resource manager according to the running version number.
// Version 1.0 Select RESOURCE_RELEASE message.
// Version 2.0 Set allow_request_additional to false to deny any
// additional request and choose RESOURCE_RELEASE_REQ message.
//
void
resource_pool::select_release_message(resource_mgr::flow_msg_t &msg_type)
{
	if ((vp_version.major_num == 1) && (vp_version.minor_num == 0)) {
		msg_type = resource_mgr::RESOURCE_RELEASE;
	} else {
		ASSERT(allow_request_additional);
		allow_request_additional = false;
		msg_type = resource_mgr::RESOURCE_RELEASE_REQ;
	}
}

//
// resource_release - all reserved resources are freed.
// After releasing resources, if a blocked request can now be unblocked,
// then the system unblocks it.
//
// Not only can a message be orphanned, but the release of resources can
// be orphanned by the node reconfiguration process. The node reconfiguration
// process does not wait for all existing message requests to release their
// resources. The following are among the many race conditions that must
// be prevented by an orphan check:
//
// 1) Server node dies while numerous twoway invocations are awaiting replies.
// The reconfiguration code aborts the twoway invocations.
// The resource counts are reset.
// Then the aborted twoway invocations release resources
// no longer accounted for.
//
// 2) Outbound msg obtains resources.
// Server node dies.
// Other system activity monopolizes the system.
// Server node rejoins cluster.
// Outbound msg get rejected in transport, releases resources, and
// CORRUPTs the counts.
//
// 3) Outbound twoway msg obtains resources and goes to server node.
// Reply msg arrives and passes orphan msg check,
// but has not yet released resources.
// Server node dies.
// Reconfiguration completes.
// Other system activity monopolizes the system.
// Server node rejoins cluster.
// The above twoway message completes, releases resources, CORRUPTs the counts.
//
void
resource_pool::resource_release(resources *resourcep)
{
	lck.lock();

	if (server_node.incn != resourcep->get_nodep()->incn) {
		// The resource release has been orphanned. Ignore it.

		// Tell the resources object that it has no resources
		resourcep->state = resources::NOTHING_RESERVED;

		lck.unlock();
		return;
	}

	resourcep->state = resources::NOTHING_RESERVED;

	// Release resources belonging to finished request.
	resources_free(resourcep);

	// Wakeup as many blocked requests as possible
	wakeup();

	lck.unlock();
}

//
// need_more_resources - the resource pool does not have enough resources
// to allow a message request. Determine whether it is appropriate to
// ask the server node for more resources. When appropriate, the resource pool
// requests additional resources.
//
void
resource_pool::need_more_resources()
{
	ASSERT(!lck.lock_held());
	lck.lock();

	if (!allow_request_additional || thread_torecall > 0) {
		//
		// Have already requested additional resources or
		// the server has a recall pending.
		// Do not request more at this time.
		//
		lck.unlock();
		return;
	}
	//
	// Determine whether to request resource at this time
	//
	if (thread_total <= thread_moderate ||
	    (thread_total < thread_high && thread_needed >= thread_increment)) {
		allow_request_additional = false;

		//
		// Fault point triggered when node asks for more resources
		//
		FAULTPT_FLOWCONTROL(FAULTNUM_FLOWCONTROL_NEED_MORE_RESOURCES_1,
			fault_flowcontrol::generic_test_op);

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("request_resources more (%d.%3d,%d) t %d\n",
		    server_node.ndid, (server_node.incn % 1000), pool,
		    thread_total));

		uint8_t	payload = (uint8_t)pool;

		lck.unlock();

		orb_stats_mgr::the().inc_pool_requests(server_node.ndid);

		resource_mgr::send_message(server_node,
		    resource_mgr::REQUEST_RESOURCES,
		    (void *)&payload, (uint_t)sizeof (uint8_t));
	} else {
		lck.unlock();
	}
	ASSERT(!lck.lock_held());
}

//
// wakeup - attempt to find and wakeup as many blocked requests as possible
//
void
resource_pool::wakeup()
{
	ASSERT(lck.lock_held());

	// Now see if any requests are blocked.
	// The normal case hopefully should be no blocked requests.
	if (pending_list.empty()) {
		// No waiting requests
		return;
	}

	// Find a request to unblock
	resources	*blockedp;
	pending_list.atfirst();
	while (NULL != (blockedp = pending_list.get_current())) {
		// Advance the list pointer because the current item may
		// be removed from the list.
		pending_list.advance();

		if (resources_available(blockedp)) {
			// All resources are available, so allocate them.
			resources_allocate(blockedp);

			// Take the request off the pending list.
			(void) pending_list.erase(blockedp);
			thread_needed--;
			ASSERT(thread_needed >= 0);

			// Wake up the waiting thread
			blockedp->state = resources::ALLOCATED;
			blockedp->cv.signal();

			// The resources have been allocated
			ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
			    ("msg(%p) unblocked. (%d.%3d,%d) t %d need %d\n",
			    blockedp, server_node.ndid,
			    (server_node.incn % 1000),
			    pool, thread_total, thread_needed));

			if (min_resources_unavailable()) {
				// Not enough resources to support any other
				// requests at this time
				break;
			}
		}
	}
	if (thread_needed > 0) {
		// Inform resource management that we are out of resources
		lck.unlock();
		need_more_resources();
		lck.lock();
	}
}

//
// got_threads - the resource pool has received some threads.
// First, use threads to satisfy any outstanding recall.
// Use remaining threads to satisfy outstanding msg requests.
//
void
resource_pool::got_threads(int new_thread)
{
	//
	// Set fault point to execute when the client gets a resource
	// grant from the server
	//
	FAULTPT_FLOWCONTROL(FAULTNUM_FLOWCONTROL_GOT_THREADS,
		fault_flowcontrol::generic_test_op);

	ASSERT(lck.lock_held());

	if (thread_torecall > 0 && thread_torecall > thread_torelease) {
		// The server wants to recall resources.
		//
		// Intercept as many threads as we can up to the number needed.
		// This can be less than the number of threads done.
		//
		int	want_thread = thread_torecall - thread_torelease;
		int	grab_thread = MIN(want_thread, new_thread);

		new_thread -= grab_thread;
		thread_torelease += grab_thread;

		//
		// It is safe to drop the lock now, as the state is consistent.
		// The lock will be dropped if a msg is sent.
		//
		check_release();
	}

	// Add remaining new threads to those free
	thread_free += new_thread;

	if (thread_free == new_thread) {
		// There were no free threads.
		// Wakeup as many blocked requests as possible
		wakeup();
	}
}

//
// process_oneways_done - the server node has sent a message containing
// information about resources released by the completions of oneway
// messages for this resource pool.
//
void
resource_pool::process_oneways_done(ID_node &src_node, resource_grant &done1s)
{
	lck.lock();
	// Discard information about resources released by orphan oneways
	if (server_node.incn != src_node.incn) {
		lck.unlock();
		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}
	ASSERT(done1s.threads <= thread_total - thread_free);

	// Use the threads to satisfy recalls first and then msg requests
	got_threads(done1s.threads);

	lck.unlock();
}

//
// process_resource_grant - the server node has sent a message containing
// information about new resources being granted to this resource pool.
//
void
resource_pool::process_resource_grant(ID_node &src_node, resource_grant &grant)
{
	lck.lock();
	// Discard information about orphanned resource grants
	if (server_node.incn != src_node.incn) {
		lck.unlock();
		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_resource_grant discarded: (%d.%d,%d)\n",
		    src_node.ndid, (src_node.incn % 1000), pool));
		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}

	if (grant.threads == 0) {
		//
		// Setting up the flowcontrol_fi test for case when server
		// rejects additional resources
		//
		FAULTPT_FLOWCONTROL(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_GRANT,
			fault_flowcontrol::generic_test_op);

		//
		// The request for more resources was rejected.
		//
		orb_stats_mgr::the().inc_pool_denies(server_node.ndid);

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_resource_grant denied: (%d.%d,%d) t %d\n",
		    src_node.ndid, (src_node.incn % 1000), pool,
		    thread_total));
		//
		// The system prevents a flood of resource requests
		// by delaying further requests until after
		// the resource_thread processes this flag.
		//
		resource_request_denied = true;
		ASSERT(!allow_request_additional);

		//
		// Even if this resource pool needs resources,
		// it will not ask for them at this time.
		// Do not want a flood of request and denial messages.
		// The resource management subsystem thread will eventually
		// have the resource pool check for resource changes.
		// At that time, the resource pool will see if it still
		// needs to ask for more resources.
		//
	} else {
		// Resources were granted
		orb_stats_mgr::the().inc_pool_grants(server_node.ndid);

		// Acquire the resources
		thread_total += grant.threads;
		ASSERT(thread_total >= 0);

		// Permit requests for additional resources
		ASSERT(!allow_request_additional);
		allow_request_additional = true;

		// Use threads to satisfy recalls first and then msg requests
		got_threads(grant.threads);

		// Reset the minimum number of free threads this period
		thread_free_min = thread_free;
	}
	lck.unlock();
}

//
// process_resource_policy - the server node has sent a message containing
// a new set of parameters for managing resource usage.
//
void
resource_pool::process_resource_policy(ID_node &src_node,
    resource_policy &policy)
{
	lck.lock();
	// Discard information about orphanned resource policies
	if (server_node.incn != src_node.incn) {
		lck.unlock();
		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_resource_policy discarded: (%d.%d,%d)\n",
		    src_node.ndid, (src_node.incn % 1000), pool));
		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}

	//
	// Any valid policy will have more than zero threads.
	// On each node reconfiguration the system distributes policy info.
	// The system does not guarantee that threads will be granted
	// before the next node reconfiguration occurs. Thus the client
	// can have a total of zero threads and have an outstanding request
	// when a policy arrives.
	//
	bool	first_policy = (thread_high == 0);

	// Reset the parameters that manage resource usage.
	thread_low		= policy.threads_low;
	thread_moderate		= policy.threads_moderate;
	thread_high		= policy.threads_high;
	thread_increment	= policy.threads_increment;
	ASSERT(thread_low > 0);
	ASSERT(thread_moderate > 0);
	ASSERT(thread_high > 0);
	ASSERT(thread_increment > 0);

	//
	// The resource_balancer may choose to change the policy at
	// any time. Thus this change can happen regardless as to whether
	// the client has an outstanding request.
	//

	if (first_policy) {
		//
		// Received the first policy information.
		// No request can be made prior to receiving the first
		// policy info. Hence no outstanding requests are possible.
		// Enable the client to ask for resources.
		//
		allow_request_additional = true;
	}

	//
	// Determine whether to request resource at this time
	//
	if (thread_total >= thread_low || !allow_request_additional) {
		//
		// Do not need resources at this time or
		// already have an outstanding request.
		//
		lck.unlock();
		return;
	}
	allow_request_additional = false;

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("request_resources initial (%d.%3d,%d) t %d\n",
	    server_node.ndid, (server_node.incn % 1000), pool, thread_total));

	uint8_t	payload = (uint8_t)pool;

	lck.unlock();

	orb_stats_mgr::the().inc_pool_requests(server_node.ndid);

	resource_mgr::send_message(server_node, resource_mgr::REQUEST_RESOURCES,
	    (void *)&payload, (uint_t)sizeof (uint8_t));
}

//
// process_resource_recall - the server node has requested that the client
// node release some resources. The server can send a multiple recall requests
// to the client without an intervening resource release.
//
void
resource_pool::process_resource_recall(ID_node &src_node, resource_grant &grant)
{
	//
	// Fault point hit when server requests resources back from
	// client
	//
	FAULTPT_FLOWCONTROL(FAULTNUM_FLOWCONTROL_PROCESS_RESOURCE_RECALL,
		fault_flowcontrol::generic_test_op);

	lck.lock();
	// Discard information about orphanned resource grants
	if (server_node.incn != src_node.incn) {
		lck.unlock();
		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_resource_recall discarded: (%d.%d,%d)\n",
		    src_node.ndid, (src_node.incn % 1000), pool));
		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}
	orb_stats_mgr::the().inc_pool_recalls(server_node.ndid);

	ASSERT(grant.threads > 0);

	// There can be no free threads when a recall is already pending
	ASSERT(thread_torecall == 0 || thread_free == 0);

	thread_torecall += grant.threads;

	if (thread_free > 0) {
		//
		// This is the first recall and there are free threads.
		//
		ASSERT(thread_torelease == 0);
		thread_torelease = MIN(thread_free, grant.threads);

		thread_free -= thread_torelease;

		// Adjust the minimum free thread count this period
		if (thread_free_min > thread_free) {
			thread_free_min = thread_free;
		}
	}

	// If possible release threads.
	check_release();

	lck.unlock();
}

//
// process_resource_release_ack - the server node has sent a message
// acknowledging the resource released on this resource pool.
//
void
resource_pool::process_resource_release_ack(ID_node &src_node)
{
	lck.lock();
	// Discard information about orphanned resource release ack
	if (server_node.incn != src_node.incn) {
		lck.unlock();
		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_resource_release_ack discarded: (%d.%d,%d)\n",
		    src_node.ndid, (src_node.incn % 1000), pool));
		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}

	// Permit requests for additional resources
	ASSERT(!allow_request_additional);
	allow_request_additional = true;

	if (thread_torelease > 0) {
		check_release();
	}

	lck.unlock();
}

//
// node_death_cleanup - All blocked requests are rejected with
// communication failure. Show no resources available.
//
void
resource_pool::node_death_cleanup()
{
	ASSERT(lck.lock_held());

	resources	*resourcep;

	// Do not permit requests for additional resources
	allow_request_additional = false;

	// Dead nodes do not deny anything
	resource_request_denied = false;

	// Show server node is dead.
	thread_total = 0;
	thread_low = 0;
	thread_moderate = 0;
	thread_high = 0;

	// Prevent blocking in flow control subsystem when server node is dead.
	// Transport will catch attempts to transmit to dead nodes.
	//
	thread_free = INT_MAX;

	// Show no recall pending
	thread_torecall = 0;
	thread_torelease = 0;

	//
	// Reject all blocked requests
	//
	pending_list.atfirst();
	while (NULL != (resourcep = pending_list.get_current())) {
		// Advance the list pointer because the current item will
		// be removed from the list.
		pending_list.advance();

		// Take the request off the pending list.
		(void) pending_list.erase(resourcep);
		thread_needed--;
		ASSERT(thread_needed >= 0);

		// The request is aborted
		resourcep->envp->system_exception(CORBA::COMM_FAILURE
		    (0, CORBA::COMPLETED_NO));
		resourcep->state = resources::NOTHING_RESERVED;

		// Wake up the waiting thread
		resourcep->cv.signal();

		// The resources have not been allocated
		ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
		    ("msg(%p) unblocked node died (%d.%3d,%d) t %d need %d\n",
		    resourcep, server_node.ndid, (server_node.incn % 1000),
		    pool, thread_total, thread_needed));
	}
	ASSERT(pending_list.empty());
	ASSERT(thread_needed == 0);
}

//
// check_for_resource_changes - examine the resource pool state, and
// initiate actions to change resource levels as appropriate.
//
void
resource_pool::check_for_resource_changes()
{
	resource_mgr::flow_msg_t resource_release_msg_type;

	if (orb_conf::is_local_incn(server_node)) {
		// Nothing to adjust for local case
		return;
	}
	lck.lock();
	if (resource_request_denied) {
		// The last request for more resources was denied.
		resource_request_denied = false;

		// Time to allow requests for more resources
		ASSERT(!allow_request_additional);
		allow_request_additional = true;

		//
		// Determine whether we need resources for blocked msg requests.
		//
		// When a resource grant is denied, the resource pool does not
		// immediately ask for more resources. The resource pool waits
		// for the resource_thread to execute. This delay prevents a
		// flood of request and denial msgs.
		//
		if (thread_needed > 0) {
			// Inform resource management that we need resources
			lck.unlock();
			need_more_resources();
			ASSERT(!lck.lock_held());
			return;
		}
	}

	if (!allow_request_additional) {
		// Only allow one outstanding change request

		thread_free_min = thread_free;
		lck.unlock();
		return;
	}

	ASSERT(thread_free_min <= thread_free);
	//
	// The following conditions must be true
	// in order to release resources:
	// 1. Must have free threads to release
	// 2. After releasing threads, still must have a minimum number
	// 3. Release policy criteria must be met.
	//
	if (thread_free >= thread_increment &&
	    thread_total >= thread_low + thread_increment &&
	    ((thread_total <= thread_moderate &&
	    thread_free_min >= (2 * thread_increment)) ||
	    (thread_total > thread_moderate))) {
		//
		// Set up fault point to monitor whether resources
		// have been released
		//
		FAULTPT_FLOWCONTROL
		    (FAULTNUM_FLOWCONTROL_CHECK_FOR_RESOURCE_CHANGES_1,
			fault_flowcontrol::generic_test_op);
		//
		// It is time to release threads to the server.
		// This will not release the last few threads
		//
		thread_total -= thread_increment;
		thread_free -= thread_increment;
		ASSERT(thread_total >= thread_low);
		ASSERT(thread_free >= 0);

		thread_free_min = thread_free;

		resource_grant	grant(pool, thread_increment);

		//
		// Select the correct resource release message to send
		//
		select_release_message(resource_release_msg_type);

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("release_resources (%d.%3d,%d) t %d r %d\n",
		    server_node.ndid, (server_node.incn % 1000), grant.pool,
		    thread_total, grant.threads));

		lck.unlock();

		orb_stats_mgr::the().inc_pool_releases(server_node.ndid);

		resource_mgr::send_message(server_node,
		    resource_release_msg_type,
		    (void *)&grant, (uint_t)sizeof (resource_grant));
	} else {
		thread_free_min = thread_free;
		lck.unlock();
	}
}

//
// disable_blocking - any and all message requests that are blocked and
// belong to the specified group are rejected and unblocked.
// A complication is that the disable may go away at any time.
//
void
resource_pool::disable_blocking(void *disable_group)
{
	resources	*resourcep;

	// Scan all blocked message requests because multiple requests
	// may have been cancelled
	lck.lock();
	pending_list.atfirst();
	while (NULL != (resourcep = pending_list.get_current())) {
		//
		// Advance the list pointer because the current item may
		// be removed from the list.
		//
		pending_list.advance();

		if (disable_group == resourcep->get_request_group()) {
			//
			// If blocking is still disabled, reject request
			//
			resource_blocker::the().confirm_and_reject(
			    disable_group, this, resourcep);
		}
	}
	lck.unlock();
}

//
// reject_blocked_request - the resource_blocker has confirmed that
// blocking for this request is in effect. So reject the request.
//
void
resource_pool::reject_blocked_request(resources *resourcep,
    CORBA::SystemException &exception)
{
	ASSERT(lck.lock_held());

	ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
	    ("msg(%p) had block disabled. group: %p\n",
	    resourcep, resourcep->get_request_group()));

	// Take the request off the pending list.
	(void) pending_list.erase(resourcep);
	thread_needed--;
	ASSERT(thread_needed >= 0);

	// This request can no longer block
	resourcep->envp->system_exception(exception);
	resourcep->state = resources::NOTHING_RESERVED;

	// Wake up the waiting thread
	resourcep->cv.signal();

	// The resources have not been allocated
	ORB_DBPRINTF(ORB_TRACE_FLOW_BLOCK,
	    ("msg(%p) unblocked, disable (%d.%3d,%d) t %d need %d\n",
	    resourcep, server_node.ndid, (server_node.incn % 1000), pool,
	    thread_total, thread_needed));
}

//
// resources_free - the resources reserved to support this specific message
// are now released in this resource pool.
//
void
resource_pool::resources_free(resources *)
{
	ASSERT(lck.lock_held());

	// Each message needs:
	//	1 server thread
	// XXX - eventually multiple resources will be supported
	//
	// When the thread_total is zero that means that the server is down,
	// otherwise the thread_total will be non-zero.
	//
	ASSERT(server_node.incn == INCN_UNKNOWN || thread_free < thread_total);
	if ((thread_torecall > 0) && (thread_torecall > thread_torelease)) {
		thread_torelease++;
		ASSERT(thread_torelease > 0);
		//
		// Have finished updating state based on the release of
		// resources from the finished request.
		//
		// It is safe to drop the lock now, as the state is consistent.
		// The lock will be dropped if a msg is sent.
		//
		check_release();
	} else {
		thread_free++;
		ASSERT(thread_free > 0);
	}
}

//
// check_release - if enough free threads are available, release them to the
// server.
//
void
resource_pool::check_release()
{
	ASSERT(lck.lock_held());

	int	release_thread;
	resource_mgr::flow_msg_t resource_release_msg_type;

	if (!allow_request_additional) {
		return;
	}
	ASSERT(thread_torecall >= thread_torelease);
	if (thread_torelease == thread_torecall) {
		// Have exactly the right number of threads
		release_thread = thread_torelease;
	} else if (thread_torelease >= thread_increment) {
		// Have an increment worth of threads to release
		release_thread = thread_increment;
	} else {
		// Do not have enough threads to release at this time
		return;
	}
	// Adjust counts of threads needed for recall
	thread_torecall -= release_thread;
	thread_torelease -= release_thread;
	ASSERT(thread_torecall >= 0);
	ASSERT(thread_torelease >= 0);

	thread_total -= release_thread;
	// The server should leave at least 1 thread on the client
	ASSERT(thread_total > 0);

	thread_free_min = thread_free;

	//
	// Set up fault point to monitor when resources are released
	//
	FAULTPT_FLOWCONTROL(FAULTNUM_FLOWCONTROL_CHECK_RELEASE,
		fault_flowcontrol::generic_test_op);

	resource_grant		grant(pool, release_thread);

	//
	// Select the correct resource release message to send
	//
	select_release_message(resource_release_msg_type);

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("release_resources (%d.%3d,%d) t %d r %d\n",
	    server_node.ndid, (server_node.incn % 1000), grant.pool,
	    thread_total, grant.threads));

	lck.unlock();

	orb_stats_mgr::the().inc_pool_releases(server_node.ndid);

	resource_mgr::send_message(server_node,	resource_release_msg_type,
	    (void *)&grant, (uint_t)sizeof (resource_grant));

	lck.lock();
}

//
// read_state - support cladm command.
// Obtain resource_pool state for specified server node and pool.
//
// static
int
resource_pool::read_state(void *argp)
{
	state_of_resource_pool	state;

	if (copyin(argp, &state, sizeof (state_of_resource_pool)) != 0) {
#ifdef DEBUG
		//
		// SCMSGS
		// @explanation
		// The system failed a copy operation supporting statistics
		// reporting.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error in copyin for state_resource_pool");
#endif
		return (EFAULT);
	}
	// Check for valid pool
	if (state.pool < 0 ||
	    state.pool >= resource_defs::NUMBER_POOLS) {
		return (EINVAL);
	}

	nodeid_t	nodeid = (nodeid_t)state.node;

	// Check for valid node id
	if (nodeid == NODEID_UNKNOWN || nodeid > NODEID_MAX) {
		return (EINVAL);
	}
	if (!members::the().alive(nodeid)) {
		// Do not return state for node not in the cluster
		state.node = NODEID_UNKNOWN;
	} else {
		// Return state for node in the cluster
		resource_pools[state.node][state.pool].extract_state(state);
	}
	return (copyout(&state, argp, sizeof (state_of_resource_pool)));
}

//
// extract_state - support cladm command by extracting the state
// for this resource_pool.
//
void
resource_pool::extract_state(state_of_resource_pool &state)
{
	state.thread_total = thread_total;
	state.thread_free = thread_free;
	state.thread_needed = thread_needed;
	state.thread_torecall = thread_torecall;
	state.thread_torelease = thread_torelease;
}
