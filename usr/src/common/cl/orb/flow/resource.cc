//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)resource.cc	1.41	08/05/20 SMI"

#include <orb/flow/resource.h>
#include <orb/invo/invo_args.h>
#include <orb/invo/invocation.h>
#include <orb/invo/argfuncs.h>
#include <sys/mc_probe.h>

#ifdef _KERNEL_ORB
#include <orb/flow/resource_pool.h>
#include <orb/flow/resource_mgr.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/debug/orb_stats_mgr.h>
#include <cmm/cmm_version.h>
#endif

#include <sys/sysmacros.h>

resources::resources(flow_policy_t pol, Environment *e,
    invocation_mode inv_mode, orb_msgtype msgt) :
	_DList::ListElem(this),
	policy(pol),
	envp(e),
	invo_mode(inv_mode),
#ifdef RECONFIG_POOL_ACTIVE
	pool(inv_mode.is_reconfig() ? resource_defs::RECONFIG_POOL
	    : resource_defs::DEFAULT_POOL),
#else
	pool(resource_defs::DEFAULT_POOL),
#endif
#ifdef _KERNEL_ORB
	serv2(NULL),
#endif
	msgtype(msgt),
	request_group(NULL),
	state(NOTHING_RESERVED)
#ifdef _FAULT_INJECTION
,	fi_processed(false)
#endif	// _FAULT_INJECTION
{
	ASSERT(envp != NULL);
	if (inv_mode.is_nonblocking()) {
		e->set_nonblocking();
	} else {
		e->clear_nonblocking(); // Environment may have been reused
	}
}

resources::resources(flow_policy_t pol, Environment *e,
    invocation_mode inv_mode, uint_t header_size, uint_t data_size,
#ifdef _KERNEL_ORB
    ID_node &node,
    service_twoway *sp,
#endif // _KERNEL_ORB
    orb_msgtype msgt) :
	_DList::ListElem(this),
	policy(pol),
	envp(e),
	invo_mode(inv_mode),
#ifdef RECONFIG_POOL_ACTIVE
	pool(inv_mode.is_reconfig() ? resource_defs::RECONFIG_POOL
	    : resource_defs::DEFAULT_POOL),
#else
	pool(resource_defs::DEFAULT_POOL),
#endif
	send(header_size, data_size),
#ifdef _KERNEL_ORB
	target_node(node),
	serv2(sp),
#endif // _KERNEL_ORB
	msgtype(msgt),
	request_group(NULL),
	state(NOTHING_RESERVED)
#ifdef _FAULT_INJECTION
,	fi_processed(false)
#endif	// _FAULT_INJECTION
{
	ASSERT(envp != NULL);
	if (inv_mode.is_nonblocking()) {
		e->set_nonblocking();
	} else {
		e->clear_nonblocking(); // Environment may have been reused
	}
}

//
// destructor - resources reserved for the request are destroyed here.
//
resources::~resources()
{
	switch (state) {
	case NOTHING_RESERVED:
		// Nothing to do, because no resources reserved yet.
		break;
	case BLOCKED:
		// The object must be removed from the pending list
		// prior to deletion.
		ASSERT(0);
		break;

	case ALLOCATED:

#ifdef _KERNEL_ORB
// Only inter-node messages reaching the kernel are flow controlled.

		// Always release twoway msg resources here.
		// Oneway msg resources only released if error occurred.
		if (!invo_mode.is_oneway() || envp->exception()) {
			resource_pool::select_resource_pool(
			    target_node.ndid, pool)->resource_release(this);
		}
#else
		ASSERT(0);
#endif
		break;
	}
}

//
// resource_release - Release any resources held by this message request.
// A message retry can occur to a different destination node.
// Resource reservations are specific to a particular destination node.
// Any reserved resource must be freed.
// Currently, HA messages retry to a different destination node
// from the hxdoor::reserve_resourcess level depending on the error.
// This is only for retrying the same message. A different message must
// use a different "resources" object.
//
void
resources::resource_release()
{
	switch (state) {
	case NOTHING_RESERVED:
		// Nothing to do, because no resources reserved yet.
		break;
	case BLOCKED:
		// The object must be removed from the pending list
		// before a message retry can occur.
		ASSERT(0);
		break;

	case ALLOCATED:
#ifdef _KERNEL_ORB
		// Only inter-node messages reaching kernel are flow controlled.
		resource_pool::select_resource_pool(target_node.ndid, pool)->
		    resource_release(this);
		break;
#else
		// Not possible. Fall through to default error.
#endif
	default:
		ASSERT(0);
		break;
	}
	ASSERT(state == NOTHING_RESERVED);

#ifdef DEBUG
	// The resource object condvar is only used in conjunction with
	// the lock belonging to its assigned resource pool.
	// The system only allows one lock to be used with a condvar
	// while in debug mode.
	// Sometimes the system retries a message to another node,
	// which requires a different resource pool.
	// This operation effectively creates a "new" condvar
	// for each message transmission attempt.
	//
	cv.reset();
#endif
	// The caller must have already dealt with any exception
	ASSERT(!envp->exception());

#ifdef _KERNEL_ORB
	// Force the system to reassign the destination
	target_node.ndid = NODEID_UNKNOWN;
	target_node.incn = INCN_UNKNOWN;
#endif
}

//
// flow_control - Decide whether request can proceed at this time.
// Errors are returned as exceptions in the environment.
//
void
resources::flow_control()
{
	MC_PROBE_0(resources_flow_control_start, "clustering orb message", "");
#ifdef _KERNEL_ORB
	orb_stats_mgr::the().inc_msg_by_policy(target_node.ndid, policy);
#endif

	switch (policy) {
	case BASIC_FLOW_CONTROL:

#ifdef _KERNEL_ORB
	// Only inter-node messages reaching the kernel are flow controlled.
		if (target_node.ndid == orb_conf::node_number()) {
			ASSERT(target_node.incn ==
			    orb_conf::local_incarnation());
			policy = NO_FLOW_CONTROL;
			break;
		}

		// Oneway messages require a flow control header
		if (invo_mode.is_oneway()) {
			add_send_header((uint_t)sizeof (oneway_flow_header));
		}

		// Reserve resources for the msg request
		resource_pool::select_resource_pool(target_node.ndid, pool)->
		    resource_allocator(this);
#else
		ASSERT(0);
#endif
		break;
	case NO_FLOW_CONTROL:
		// Currently assume resources are always available
		break;
	case REPLY_FLOW:
		// Flow control occurs on the request message
		break;
	case NUMBER_POLICIES:
		ASSERT(0);	// Should not happen. Make lint happy.
		break;
	}
	MC_PROBE_0(resources_flow_control_end, "clustering orb message", "");
}

//
// send_buffer_size - Calculate send main buffer size
//
uint_t
resources::send_buffer_size()
{
	uint_t	size = send.header_size + send.data_size +
	    (SIZE_FOR_OBJECT * send.num_objs);

	if (send.num_unknowns > 0) {
		size += SIZE_FOR_UNKNOWN * send.num_unknowns;

		// Provide some extra space for longer unknowns.
		// This is an estimate so it does not have to be correct.
		size += PAD_SIZE;
	}
	return (size);
}

//
// send_xdoortab_size - Calculate size of xdoor buffer size
// appropriate for when the xdoors are first marshalled as pointers.
//
// The IDL compiler does not distinguish between real objects and
// data having special marshal routines. So the object count may be high.
// Therefore this value should be used to set the "chunksize" for the
// xdoortab marshal stream, instead of creating the buffer in the constructor.
//
// The return value is forced to have a minimum value.
//
uint_t
resources::send_xdoortab_size()
{
	uint_t	size = ((uint_t)sizeof (void *)) * send.num_objs;
	return (MAX(XDOORCHUNK, size));
}

//
// add_argsizes - Add the size information for a remote invocation
//
void
resources::add_argsizes(arg_info &arginfo, ArgValue *argvaluep)
{
	arg_sizes	*argsizesp = arginfo.get_argsizesp();
	switch (msgtype) {
	case REFJOB_MSG:
#ifdef	CMM_VERSION_0
	case CMM_MSG:
#endif	// CMM_VERSION_0
	case CMM1_MSG:
#ifdef DEBUG
	case STOP_MSG:
#endif // DEBUG
	case VM_MSG:
	case SIGNAL_MSG:
	case FLOW_MSG:
	case FAULT_MSG:
	case COMPLETED_MAYBE_RESOLVE_MSG:
	case N_MSGTYPES:
	default:
		// Only IDL generated messages specify message sizes this way
		ASSERT(0);
		break;

	case REPLY_MSG:
		// The reply data is now being transmitted
		add_send_data(argsizesp->in_size);
		add_send_unknown(argsizesp->in_unknown);
		add_send_objs(argsizesp->in_objtref);

		marshal_time_size_determination(arginfo, argvaluep,
		    invo_args::out_param, argsizesp);

		break;

	case INVO_MSG:
	case INVO_ONEWAY_MSG:
	case CKPT_MSG:
		add_send_data(argsizesp->out_size);
		add_send_unknown(argsizesp->out_unknown);
		add_send_objs(argsizesp->out_objtref);

		add_reply_data(argsizesp->in_size);
		add_reply_unknown(argsizesp->in_unknown);
		add_reply_objs(argsizesp->in_objtref);

		marshal_time_size_determination(arginfo, argvaluep,
		    invo_args::in_param, argsizesp);

		break;
	}
}

//
// marshal_time_size_determination - computes the size at marshal time
// for those parameters requiring this support.
//
void
resources::marshal_time_size_determination(arg_info &arginfo,
    ArgValue *argvaluep, invo_args::arg_mode test_mode, arg_sizes *argsizesp)
{
	//
	// Args requiring marshal time size determination are less common.
	// So optimize for the case where none exist.
	//
	uint_t	num_size_args = argsizesp->num_marshal_size;
	if (num_size_args == 0) {
		return;
	}
	uint_t		argc = arginfo.argc();

	arg_desc	*argdescp = arginfo.argdesc();

	// The first entry is the return value.
	// This method starts with the first parameter
	ArgValue	*argp = &argvaluep[1];

	ArgMarshalFuncs	*argmarshalp = arginfo.marshal_funcs();

	//
	// Process the arguments that require marshal time size determination
	//
	for (uint_t i = 1; i < argc; i++, argdescp++, argp++, argmarshalp++) {
		arg_desc		descriptor = *argdescp;
		invo_args::arg_type	type = descriptor.get_argtype();

		//
		// Currently only support marshal time size determination
		// for argument going from the client to server.
		//
		if (descriptor.is_marshal_time_size()) {
			invo_args::arg_mode argmode = descriptor.get_argmode();

			if ((argmode & test_mode) != 0) {
				switch (type) {
				case invo_args::t_number_types:
				case invo_args::t_void:
				case invo_args::t_boolean:
				case invo_args::t_char:
				case invo_args::t_octet:
				case invo_args::t_short:
				case invo_args::t_unsigned_short:
				case invo_args::t_long:
				case invo_args::t_unsigned_long:
				case invo_args::t_longlong:
				case invo_args::t_unsigned_longlong:
#ifdef ORBFLOAT
				case invo_args::t_float:
				case invo_args::t_double:
#endif
					// The marshal size for the above types
					// is always determined by the compiler
					ASSERT(0);
					break;

				case invo_args::t_string:
				case invo_args::t_object:
					// Marshal time size determination is
					// not yet supported for the above types
					ASSERT(0);
					break;

				case invo_args::t_addr: { // Begin scope
					// Some complex types support
					// marshal time size
					// determination

					MarshalInfo_complex *marshalp =
					    argmarshalp->datfuncs;
					ASSERT(marshalp != NULL);
					ASSERT(marshalp->marshal_sizef != NULL);

					void	*datap = argp->t_vaddr;

					if (argmode == invo_args::out_param &&
					    marshalp->freef != NULL) {
						datap = *(void **)datap;
					}
					//
					// Call the marshal_size
					// function of the complex
					// data type only if the data
					// pointer is not null.
					//
					if (datap != NULL) {
						add_send_data((*marshalp->
						    marshal_sizef)(datap));
					}
				} // end scope
					break;
				}
			}
			if (--num_size_args == 0) {
				// Found all of the parameters requiring
				// marshal time size determination
				return;
			}
		}
	}
	// There is one remaining parameter requiring marshal time
	// size determination. It must be the return argument
	ASSERT(num_size_args == 1);

	arg_desc	return_descriptor = arginfo.rdesc();
	ASSERT(return_descriptor.is_marshal_time_size());

	invo_args::arg_mode return_mode = return_descriptor.get_argmode();

	if ((return_mode & test_mode) == 0) {
		// Wrong in/out mode
		return;
	}

	invo_args::arg_type return_type = return_descriptor.get_argtype();
	switch (return_type) {
	case invo_args::t_number_types:
	case invo_args::t_void:
	case invo_args::t_boolean:
	case invo_args::t_char:
	case invo_args::t_octet:
	case invo_args::t_short:
	case invo_args::t_unsigned_short:
	case invo_args::t_long:
	case invo_args::t_unsigned_long:
	case invo_args::t_longlong:
	case invo_args::t_unsigned_longlong:
#ifdef ORBFLOAT
	case invo_args::t_float:
	case invo_args::t_double:
#endif
		// The marshal size for the above types
		// is always determined by the compiler
		ASSERT(0);
		break;

	case invo_args::t_string:
	case invo_args::t_object:
		// Marshal time size determination is
		// not yet supported for the above types
		ASSERT(0);
		break;

	case invo_args::t_addr: {
			// Some complex types support
			// marshal time size determination

			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);

			void	*datap = argp->t_vaddr;
			//
			// Return types are always invo_args::out_param
			//
			if (marshalp->freef != NULL) {
				datap = *(void **)datap;
			}

			add_send_data((*marshalp->marshal_sizef)(datap));
		}
		break;
	}
}

//
// resources_datagram methods
//

//
// destructor for kernel oneway messages has no resources to release.
//
resources_datagram::~resources_datagram()
{
}
