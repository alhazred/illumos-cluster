/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RESOURCE_BLOCKER_H
#define	_RESOURCE_BLOCKER_H

#pragma ident	"@(#)resource_blocker.h	1.12	08/05/20 SMI"

#include <sys/os.h>
#include <sys/list_def.h>

class resources;
class resource_pool;

//
// disable_request - encapsulates information needed to support one
// disable blocking request.
//
class disable_request : public _DList::ListElem {
public:
	disable_request(void *group, CORBA::SystemException &exception);

	void			*request_group;
	CORBA::SystemException	disable_exception;
};

//
// resource_blocker - this object supports that ability to disable blocking
// for messages belonging to a request group. A disable blocking command
// remains in effect until explicitly removed.
//
// This capability requires that each message request be tagged with a
// unique "request_goup", which is a relevant address. Each message request
// can have only one "request_group". This feature is intended to support
// upper level services (HA is the one known example) that need to force
// message requests out of a blocked state.
//
// The system enables blocking as the default state for all request groups.
//
// Message requests occur frequently, while disable commands are rare events
// of limited time duration. This object attempts to minimize checking costs.
//
// This software can be safely used with multiple threads that are all trying
// to enable or disable blocking for a request group. The latest command
// processed by the resource_blocker becomes the effective command.
// The resource_blocker commands do not nest. Thus one disable (or enable)
// command has the same effect as several disable (or enable commands).
// If someone issues a disable command while a disable command is in effect,
// the exceptions specified in both old and new disable commands must match.
//
// The HA subsystem can and does issue the disable command multiple times.
// This provided the rationale for the above semantics.
//
// Lock Order Hierarchy - whenever both the resource_pool and resource_blocker
// locks are needed, the resource_pool lock must be acquired first.
//
class resource_blocker {
public:
	resource_blocker();

	static int 	initialize();
	static void	shutdown();
	static resource_blocker	&the();

	//
	// Any blocked message belonging the specified request group
	// is rejected with the specified exception.
	// This command remains in effect till removed.
	//
	void		disable_flow_blocking(void *group,
			    CORBA::SystemException &exception);

	//
	// The resource_pool has found a blocked message request that might
	// belong to a group not allowing blocking. If blocking still
	// disabled, tell resource_pool to reject message.
	//
	void		confirm_and_reject(void *group, resource_pool *poolp,
			    resources *resourcep);

	//
	// Enable flow control blocking for the specified request group
	//
	void		enable_flow_blocking(void *group);

	// Returns true if the request group for this message has been cancelled
	disable_request	*find_disable(void *group);

private:
	static resource_blocker	*the_resource_blocker;

	// Hash to the correct bucket/lock.
	int		hashslot(void *request_group);

	// Return a pointer to the disable_request if blocking is disabled.
	disable_request *search_for_disable(void *group);

	enum { DISABLE_TABLE_SIZE = 256 };	// Number of buckets

	// The number of cancels in effect
	unsigned int	number_disables;

	// The outstanding oneway resource objects are kept in
	// an array of lists. The specific list is chosen by hashing.
	//
	IntrList<disable_request, _DList>	buckets[DISABLE_TABLE_SIZE];

	// Each bucket has a corresponding lock controlling access
	os::mutex_t	lck;

	// Disallow assignments and pass by value
	resource_blocker(const resource_blocker &);
	resource_blocker & operator = (resource_blocker &);
};

#include <orb/flow/resource_blocker_in.h>

#endif	/* _RESOURCE_BLOCKER_H */
