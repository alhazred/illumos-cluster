/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RESOURCE_MGR_H
#define	_RESOURCE_MGR_H

#pragma ident	"@(#)resource_mgr.h	1.24	08/05/20 SMI"

//
// This module supports the exchange of flow control information between
// nodes.
//

#include <sys/os.h>
#include <orb/member/Node.h>
#include <orb/flow/resource.h>
#include <orb/flow/resource_defs.h>
#include <orb/buffers/marshalstream.h>

//
// resource_grant - identifies resources being provided to a resource pool.
// The resources can represent allocated resources that have been freed
// by the completion of oneways.
//
// Alternatively, the resources can represent a change in resources from
// the server. The change can be an increase or decrease in resources.
//
// The flow control message type selects one of the above situations.
//
class resource_grant {
public:
	resource_grant();
	resource_grant(resource_defs::resource_pool_t new_pool,
	    int new_threads);

	resource_defs::resource_pool_t	pool;
	int				threads;
};

//
// resource_params - provides information about the flow control parameters.
//
class resource_policy {
public:
	resource_policy();
	resource_policy(resource_defs::resource_pool_t new_pool,
	    int low, int moderate, int high, int increment);

	resource_defs::resource_pool_t	pool;
	int				threads_low;
	int				threads_moderate;
	int				threads_high;
	int				threads_increment;
};

//
// resource_mgr - supports the exchange of resource information between nodes.
//
// On the server side this object notifies the client resource_mgr when
// oneway requests have finished using resources.
//
// On the client side this object receives notices about oneway requests that
// have been serviced by the server node and thus no longer need resources.
//
class resource_mgr {
public:
	// These values identify the type of resource_mgr messages
	enum flow_msg_t {
		ONEWAY_NOTICES,		// Resource from oneways done
		RESOURCE_GRANT,		// New resources from server
		RESOURCE_POLICY,	// New flow control parameters
		RESOURCE_RELEASE,	// Resources released from client
		RESOURCE_RECALL,	// Client must release resources
		REQUEST_RESOURCES,	// Want resources
		RESOURCE_RELEASE_REQ,   // Resources released from client
		RESOURCE_RELEASE_ACK    // Resources release acknowledged
	};

	static void		handle_messages(recstream *recstreamp);

	static void		node_reconfiguration();

	static void		reconfig_reallocate_resources();

	static void		send_message(ID_node &dest_node,
				    flow_msg_t msgtype, void *argbuf,
				    uint_t argsize);

	static int		initialize();
	static void		shutdown();

private:
	static void		process_oneways_done(ID_node &src_node,
				    MarshalStream &recmarsh);

	static void		process_resource_grant(ID_node &src_node,
				    MarshalStream &recmarsh);

	static void		process_resource_policy(ID_node &src_node,
				    MarshalStream &recmarsh);

	static void		process_resource_recall(ID_node &src_node,
				    MarshalStream &recmarsh);

	static void		process_resource_release_ack(ID_node &src_node,
				    MarshalStream &recmarsh);

	// Disallow assignments and pass by value
	resource_mgr(const resource_mgr &);
	resource_mgr & operator = (resource_mgr &);
};

//
// oneway_flow_header - this message header accompanies oneway messages.
// This header identifies the resource usage for this message.
// The message header implicitly signals the use of 1 server thread.
// XXX - at this time, do not plan to flow control messages serviced by
// XXX - the interrupt thread.
//
class oneway_flow_header {
public:
	static void	marshal(sendstream *sendstreamp);
	static resource_defs::resource_pool_t unmarshal(recstream *recstreamp);

#ifdef MARSHAL_DEBUG
	uint32_t	marshal_debug;		// constant for validity check
						// must be the first data member
#endif
	resource_defs::resource_pool_t	pool;
};

#include <orb/flow/resource_mgr_in.h>

#endif	/* _RESOURCE_MGR_H */
