/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  resource_mgr_in.h
 *
 */

#ifndef _RESOURCE_MGR_IN_H
#define	_RESOURCE_MGR_IN_H

#pragma ident	"@(#)resource_mgr_in.h	1.6	08/05/20 SMI"

//
// constructor - this version is only for use by methods which extract
// a resource_grant from a message.
//
inline
resource_grant::resource_grant()
{
}

//
// constructor - this version is for all other use.
//
inline
resource_grant::resource_grant(resource_defs::resource_pool_t new_pool,
    int new_threads) :
	pool(new_pool),
	threads(new_threads)
{
}

//
// constructor - this version is only for use by methods which extract
// a resource_policy from a message.
//
inline
resource_policy::resource_policy()
{
}

//
// constructor - this version is for all other use.
//
inline
resource_policy::resource_policy(resource_defs::resource_pool_t new_pool,
    int low, int moderate, int high, int increment) :
	pool(new_pool),
	threads_low(low),
	threads_moderate(moderate),
	threads_high(high),
	threads_increment(increment)
{
}

#endif	/* _RESOURCE_MGR_IN_H */
