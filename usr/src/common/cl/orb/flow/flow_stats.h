/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _FLOW_STATS_H
#define	_FLOW_STATS_H

#pragma ident	"@(#)flow_stats.h	1.8	08/05/20 SMI"

#include <sys/os.h>
#include <sys/types.h>
#include <orb/flow/resource_defs.h>
#include <orb/flow/resource.h>

//
// flow_stats - records statistics about flow control operations
// supporting the interaction with one specific cluster node.
//
// In order to reduce the impact on performance, the system does not
// use locking to protect statistics operations. This means that the
// statistics are imprecise. If two threads attempt to simultaneously
// update a counter, one update will overwrite the other update.
//
class flow_stats {
public:
	flow_stats();

	void		init();

	void		inc_msg_by_policy(resources::flow_policy_t policy);
	void		inc_blocked();
	void		inc_unblocked();
	void		inc_reject();
	void		inc_pool_requests();
	void		inc_pool_grants();
	void		inc_pool_denies();
	void		inc_pool_recalls();
	void		inc_pool_releases();
	void		inc_balancer_requests();
	void		inc_balancer_grants();
	void		inc_balancer_denies();
	void		inc_balancer_recalls();
	void		inc_balancer_releases();

	//
	// The following fields record flow control activity.
	//
	// Always specify the precise size of these fields,
	// because these statistics are recorded in the kernel
	// and passed to user level applications. We do not want to do
	// conversions between different size long's, etc.
	//
	uint32_t	msg_by_policy[resources::NUMBER_POLICIES];
	uint32_t	blocked;	// Number of blocked msg's
	uint32_t	unblocked;	// Number of unblocked msg's

	uint32_t	reject;		// Number of msg's rejected because:
					// no resources for non-blocking, or
					// request group has blocking disabled.

	uint32_t	pool_requests;
	uint32_t	pool_grants;
	uint32_t	pool_denies;
	uint32_t	pool_recalls;
	uint32_t	pool_releases;
	uint32_t	balancer_requests;
	uint32_t	balancer_grants;
	uint32_t	balancer_denies;
	uint32_t	balancer_recalls;
	uint32_t	balancer_releases;
};

//
// cl_flow_stats - used by cladm commands to access flow control statistics
//
struct cl_flow_stats {
	nodeid_t	nodeid;
	flow_stats	stats;
};

#include <orb/flow/flow_stats_in.h>

#endif	/* _FLOW_STATS_H */
