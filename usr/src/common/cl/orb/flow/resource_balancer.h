/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RESOURCE_BALANCER_H
#define	_RESOURCE_BALANCER_H

#pragma ident	"@(#)resource_balancer.h	1.31	08/05/20 SMI"

#if defined(__cplusplus)

//
// Each node contains exactly one resource_balancer object,
// which manages the allocation of resources to all client resource pools.
//

#include <orb/flow/resource_defs.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orbthreads.h>
#include <sys/os.h>

class ID_node;
class MarshalStream;
class resource_grant;

struct resource_pool_static_values {
	resource_defs::resource_pool_t	pool;

	// States whether pool size is fixed.
	bool				fixed_size;

	//
	// Number of thread initially granted to each client.
	// Number of threads below which the client will not voluntarily drop.
	//
	int				threads_low;

	// Minimum number of server threads
	int				threads_minimum;
};

//
// Do not want to reallocate resources if the involved clients
// have almost the same amount of resources.
// Require that the donor and recipient nodes differ in the
// number of threads by this multiple of the increment value.
//
const int		reallocation_inc_multiple = 2;

//
// resource_balancer - there is one object per server node.
// This object acquires the resources and places them under the control
// of the resource management subsystem. When the resources are no longer
// needed, this object releases them. The following events
// change the amount of resources:
//	1. client nodes joining/leaving the cluster
//	2. increase/decrease in message traffic
//
// Initially, the resource_balancer manages the number of server threads.
// Later other resources will be managed.
//
// This object decides whether to grant requests from clients for resources.
// This object also decides when to take resources away from clients.
//
class resource_balancer : public orbthread {
public:
	resource_balancer();

	static int	initialize();
	static void 	shutdown();

	static resource_balancer	&the();

	// This will become a thread that periodically manages resources
	static void	resource_thread(void *);

	void		process_request_resources(ID_node &src_node,
			    MarshalStream &recmarsh);

	void		process_resource_release(ID_node &src_node,
			    MarshalStream &recmarsh,
			    bool acknowledge_needed);

	void		node_reconfiguration();

	void		reconfig_reallocate_resources();

	int		threads_increment(resource_defs::resource_pool_t pool);

	int		get_threads_total(resource_defs::resource_pool_t pool);

	int		get_threads_min(resource_defs::resource_pool_t pool);

	void		set_threads_min(resource_defs::resource_pool_t pool,
			    int minimum);

	bool		set_policy(resource_defs::resource_pool_t pool, int low,
			    int moderate, int high, int increment,
			    int number_nodes);

	void		get_policy(resource_defs::resource_pool_t pool,
			    int &low, int &moderate, int &high, int &increment);

	int		number_nodes();

	int		read_state(void *argp);

private:
	int		initialize_thread();

	void		reallocate_resources(ID_node &src_node,
			    resource_defs::resource_pool_t pool);

	void		reallocate_resources_from_dead_nodes();

	void		distribute_policy();

	bool		validate_policy(resource_defs::resource_pool_t pool,
			    int number_nodes, int low, int moderate, int high,
			    int increment);

	void		send_grant(ID_node &dest_node,
			    resource_defs::resource_pool_t pool, int change);

	void		request_resources(ID_node &src_node,
			    resource_defs::resource_pool_t src_pool,
			    int threads_needed);

	void		resource_release(ID_node &src_node,
			    resource_grant &grant, bool acknowledge_needed);

	void		discard_surplus_threads(
			    resource_defs::resource_pool_t pool, int change);

	void		manage_unrefs();

#ifdef DEBUG
	void		validate_number_threads();
#endif

	class resource_pool_info {
		public:
		void	initialize(resource_pool_static_values *pool_valuesp);

		// Number of threads to allocate initially to each client.
		int	threads_low;

		//
		// Number of threads used to identify whether
		// we have few or many threads. The thresholds for
		// acquiring/retaining threads are lower
		// when we have few threads. This value is recomputed
		// at each node reconfiguration based on the number of
		// nodes currently in the cluster.
		//
		int	threads_moderate;

		// Number of threads that the server will not normally exceed
		int	threads_high;

		// Number of threads actually created
		int	threads_total;

		// Number of threads to batch together
		int	threads_increment;

		//
		// Minimum number of server threads.
		// This number of threads will be created on the server node
		// at system start up time. The system will never
		// allow the number of threads on the server node
		// to drop below this value.
		//
		int	threads_minimum;

		//
		// Number of threads not allocated but in existance.
		//
		// This acts as a pool of threads that can quickly be
		// granted to clients without need for thread creation.
		//
		// When all of the other nodes have dropped out of the
		// cluster, the threads in this group process any
		// orphan messages.
		//
		// This collection of threads also guarantees that some
		// threads exist despite the presence of memory hogging
		// applications, such as data base systems.
		//
		int	threads_unallocated;

		//
		// The static values for this resource pool are available
		// from this data structure.
		//
		resource_pool_static_values	*valuesp;

		// Number of threads granted to each pool
		//
		// Orphan messages can arrive after the resource balancer
		// has released all of the server threads.
		// The orphan message check occurs after the system
		// delivers the message to a server thread.
		// Thus the system cannot allow the number of server threads
		// to go to zero once the number of server threads has
		// climbed above zero.
		//
		// There is never a node 0. If there are no other nodes
		// in a cluster, one server thread will be allocated to
		// the dummy node 0. This thread will be recycled when
		// other nodes rejoin the cluster. When the node first comes
		// up, no thread will be allocated to dummy node 0.
		//
		int	threads_allocated[NODEID_MAX + 1];

		// Number of threads recalled from client.
		// The client node has not yet released these threads yet.
		int	threads_recalled[NODEID_MAX + 1];

		// Number of threads approved for allocation pending
		// recall of resources from another node.
		int	threads_pending[NODEID_MAX + 1];

		// True if the client node has joined the cluster and
		// has not yet received an initial set of resources.
		bool	needs_initial[NODEID_MAX + 1];
	};

	resource_pool_info	pool_info[resource_defs::NUMBER_POOLS];

	incarnation_num		node_incn[NODEID_MAX + 1];

	os::mutex_t		lck;

	os::sc_syslog_msg	resource_balancer_msg; // log msgs to syslog

	static resource_balancer *the_resource_balancer;

	// Disallow assignments and pass by value
	resource_balancer(const resource_balancer &);
	resource_balancer & operator = (resource_balancer &);
};

//
// cl_flow_specs - used by cladm commands to read/change flow control settings.
//
struct cl_flow_specs {
	resource_defs::resource_pool_t	pool;
	int				number_nodes;
	int				threads_low;
	int				threads_moderate;
	int				threads_high;
	int				threads_increment;
};

//
// cl_flow_threads_min - used by cladm commands to read/change
// server threads minimum setting for the specified pool.
//
struct cl_flow_threads_min {
	resource_defs::resource_pool_t	pool;
	int				threads_minimum;
};

#include <orb/flow/resource_balancer_in.h>

extern "C" {
#endif /* __cplusplus */

/*
 * Functions support cladmin commands
 */
#if defined(_KERNEL_ORB)
int	cl_read_flow_settings(void *argp);
int	cl_change_flow_settings(void *argp);
int	cl_read_threads_min(void *argp);
int	cl_change_threads_min(void *argp);
#endif

#if defined(__cplusplus)
}
#endif

#endif	/* _RESOURCE_BALANCER_H */
