/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RESOURCE_POOL_H
#define	_RESOURCE_POOL_H

#pragma ident	"@(#)resource_pool.h	1.30	08/05/20 SMI"

#include <sys/os.h>
#include <sys/list_def.h>
#include <orb/member/Node.h>
#include <orb/invo/corba.h>
#include <orb/flow/resource_defs.h>
#include <orb/flow/resource_mgr.h>
#include <limits.h>
#include <orb/debug/orbadmin_state.h>
#include <sys/vm_util.h>

class resources;
class resource_grant;
class resource_policy;

//
// resource_pool - this object identifies the resources available for
// a particular pool. Each pool manages information on the client node
// about resources on the server node for a specific purpose.
//
// The resource pool information about resource availability is valid
// for precisely one incarnation of the server node. The change of the
// incarnation number for a resource pool is not synchronized with other
// system activity. There is a close correlation between a node reconfiguration
// and the change of the incarnation information in a resource pool, but
// there is a time gap. These facts force the resource pool to deal
// with several different race conditions. The code has comments describing
// the protection against these race conditions.
//
// The user level is only allowed to know about the resource pool types.
//
// Lock Order Hierarchy - whenever both the resource_pool and resource_blocker
// locks are needed, the resource_pool lock must be acquired first.
//
class resource_pool {
public:
	resource_pool();

	virtual ~resource_pool() {}

	static resource_pool	*select_resource_pool(nodeid_t node,
				    resource_defs::resource_pool_t pool);

	static int	initialize();
	static void	shutdown();

	static int	read_state(void *argp);

	void		extract_state(state_of_resource_pool &state);

	void		resource_allocator(resources *resourcep);
	void		resource_release(resources *resourcep);

	void		disable_blocking(void *cancel_group);

	void		reject_blocked_request(resources *resourcep,
			    CORBA::SystemException &exception);

	void		init(nodeid_t node,
			    resource_defs::resource_pool_t new_pool);

	void		check_for_resource_changes();

	void		process_oneways_done(ID_node &src_node,
			    resource_grant &done1s);
	void		process_resource_grant(ID_node &src_node,
			    resource_grant &grant);
	void		process_resource_policy(ID_node &src_node,
			    resource_policy &policy);
	void		process_resource_recall(ID_node &src_node,
			    resource_grant &grant);
	void		process_resource_release_ack(
			    ID_node &src_node);

	//
	// Each node acts as a client node to other nodes.
	// Each client node has a separate resource pool
	// for each resource pool type for each server node.
	// For each client node, all of the other nodes in the cluster
	// act as server nodes.
	//
	static resource_pool	*resource_pools[];

private:
	bool		resources_available(resources *resourcep);
	bool		min_resources_unavailable();
	void		resources_allocate(resources *resourcep);
	void		resources_free(resources *resourcep);
	void		need_more_resources();
	void		wakeup();
	void		got_threads(int new_thread);
	void		node_death_cleanup();
	void		check_release();
	void		select_release_message(
			    resource_mgr::flow_msg_t &msg_type);

	// Server thread counts
	//
	// thread_total holds the total number of server threads for this pool.
	//
	// The value of thread_total is never smaller than the number threads
	// free unless the server node is down.
	//
	int		thread_total;
	//
	// thread_free holds the number of free server threads for this pool.
	//
	// This value is set to an impossibly high value to prevent blocking
	// when the server node is dead.
	// When a message requests resources to transmit to a dead node,
	// the pool always has server thread resources.
	// The transport will reject the message request.
	//
	int		thread_free;
	//
	// The number of threads needed is equal to the number of
	// blocked requests. This information supports the process
	// that reallocates resources between nodes.
	//
	int		thread_needed;
	//
	// The number of threads below which the client will
	// not voluntarily drop.
	//
	int		thread_low;
	//
	// The number of threads the resource pool will tend towards when
	// the server node is completely busy.
	//
	int		thread_moderate;
	//
	// The maximum number of threads the server node will grant unless
	// a resource exhaustion deadlock threatens.
	//
	// This value is zero when the client has not received a valid
	// set of policy information from the currently valid live server node.
	//
	int		thread_high;
	//
	// The number of threads to request additional resources are needed.
	//
	int		thread_increment;
	//
	// The number of threads the server wants recalled.
	//
	int		thread_torecall;
	//
	// The number of threads to be released
	//
	int		thread_torelease;
	//
	// The lowest number of idle threads in the current time period.
	// The value MAX_INT signifies that no requests have completed
	// during this time period.
	//
	int		thread_free_min;

	// The resource pool must identify itself when requesting resources
	resource_defs::resource_pool_t	pool;

	//
	// The protocol for requesting more resources from the server node
	// allows only one request to be outstanding at a time.
	//
	bool		allow_request_additional;
	//
	// After a request for more resources has been denied,
	// the resource pool must wait a little while before asking again.
	// This flag is set true when a resource request is denied,
	// and is cleared later by the resource_thread.
	//
	bool		resource_request_denied;

	//
	// The resources in a resource pool are only valid for
	// precisely one incarnation of the server node.
	// The resource_pool must record the incarnation number,
	// because there is no direct synchronization between
	// node reconfiguration and the granting of resources.
	// There is a small time delay between when the public membership
	// incarnation list is published and when the resource_pool updates
	// its incarnation number. A message can acquire resources during this
	// small window. The incarnation number in the resource pool
	// ensures that the allocation of resources matches the current
	// incarnation number of the resource pool, and not necessarily the
	// published incarnation number.
	//
	// The system uses the value INCN_UNKNOWN for a dead server node.
	//
	ID_node		server_node;

	version_manager::vp_version_t	vp_version;

	// Synchronizes activity in the resource pool.
	// See lock hierarchy comment at beginning of the file.
	os::mutex_t	lck;

	// Blocked requests are placed on this list.
	// A doubly linked list is used for two reasons.
	// Eventually multiple resources will be managed and
	// then the system could regularly pull a request other than the first.
	// Eventually, prioritization will be supported.
	//
	IntrList<resources, _DList>	pending_list;

	// Disallow assignments and pass by value
	resource_pool(const resource_pool &);
	resource_pool & operator = (resource_pool &);
};

#include <orb/flow/resource_pool_in.h>

#endif	/* _RESOURCE_POOL_H */
