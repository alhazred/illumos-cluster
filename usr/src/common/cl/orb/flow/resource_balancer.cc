//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)resource_balancer.cc	1.56	08/05/20 SMI"

#include <orb/flow/resource_balancer.h>
#include <orb/flow/resource_mgr.h>
#include <orb/flow/resource_done.h>
#include <orb/flow/resource_pool.h>
#include <orb/buffers/marshalstream.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/clusterproc.h>
#include <orb/member/members.h>
#include <orb/debug/orb_trace.h>
#include <sys/threadpool.h>
#include <orb/debug/orb_stats_mgr.h>
#include <orb/debug/orbadmin_state.h>
#include <orb/refs/unref_threadpool.h>
#include <sys/rsrc_tag.h>

#ifdef _KERNEL
#include <sys/systm.h>
#include <sys/tuneable.h>
#include <sys/var.h>
#else
#include <unode/systm.h>
#endif

// Specify the time interval for checking resource usage
static const os::usec_t		resource_time_period = 1000000;

//
// The resource_balancer constructor contains assertions about the
// relative values of the thread parameters. These constraints
// are mandatory.
//

//
// The following value can (optionally) be set in the file "/etc/system"
// for real machines. This value is currently not set on unode pseudo machines.
// When set (not negative), this value specifies the minimum number of
// server threads allocated for the default resource pool upon system
// startup. The system ignores this value when this value is lower
// than "default_pool_threads_minimum".
//
int			min_threads_default_pool = -1;

//
// The minimum number of server threads created at system start up time
// is currently a multiple of the system value "maxusers".
// XXX - this value should be determined through tests with real workloads.
//
static const int	maxusers_factor_min = 2;

//
// The maximum number of server threads that the system will ever create
// is currently a multiple of the system value "maxusers".
// XXX - this value should be determined through tests with real workloads.
//
static const int	maxusers_factor_max = 8;

//
// Specify the factor used to calculate the threads_increment value.
// When larger number of server threads are present, a higher increment value
// reduces overhead. The tradeoff is that higher increment values limit
// the ability to fine tune the distribution of resources.
// The reason for the different factors is that fixed and variable size
// resource pools are based off different quantities.
//
static const int	fix_size_inc_factor = 20;

#ifdef _KERNEL
static const int	vary_size_inc_factor = 50;
#else
static const int	vary_size_inc_factor = 5;
#endif

#ifdef _KERNEL
//
// This value represents the maximum number of pages that
// one server thread would require in order to complete processing
// for a typical request.
//
// This value includes:
//	<pages for stack> +
//	<pages for recstream and sendstream> +
//	<pages for heap space data structures used to process request> +
//	etc.
//
// This value is used in calculations that determine whether the
// system can safely create more server threads and still have
// enough memory so that all of the resulting server threads can process
// requests without exhausting memory. Memory needs vary widely.
// So it is safer to overestimate the typical memory requirements
// of a server thread.
//
static const int	pages_per_server_thread = 10;

// Minimum number of server threads for a real machine.
// A small number of threads would make the system highly vulnerable
// to resource exhaustion deadlocks.
//
static const int	default_pool_threads_minimum = 100;
#else
// Minimum number of server threads for a unode pseudo machine.
// This number is lower on a unode machine because we want to
// exercise flow control scenarios with smaller numbers of threads.
// Unode systems do not (yet) run heavy user work loads,
// and hence are not as likely to encounter resource exhaustion deadlocks.
//
static const int	default_pool_threads_minimum = 10;
#endif

#ifdef RECONFIG_POOL_ACTIVE
static const int	reconfig_pool_threads_minimum = 1;
#endif

//
// Specify the minimum initial number of threads for the default resource pool.
// This number will be larger as system conditions warrant (ie. more memory).
// Be aware that this number of threads will be granted to each node
// joining the cluster, and that can be a large number (NODEID_MAX - 1).
//
static const int		default_pool_threads_low = 10;

#ifdef RECONFIG_POOL_ACTIVE
//
// Empirical testing has shown that the reconfiguration resource pool
// only needs one thread in a fixed size resource pool.
//
static const int		reconfig_pool_threads_low = 1;
#endif

//
// Used to determine initial number of threads granted to a client.
// It is a multiple of the increment value.
//
static const int		initial_alloc_factor = 2;

resource_pool_static_values	pool_values[resource_defs::NUMBER_POOLS] = {
	{resource_defs::DEFAULT_POOL, false,
	default_pool_threads_low, default_pool_threads_minimum}
#ifdef RECONFIG_POOL_ACTIVE
,	{resource_defs::RECONFIG_POOL, true, reconfig_pool_threads_low,
		reconfig_pool_threads_minimum}
#endif
};

resource_balancer *resource_balancer::the_resource_balancer = NULL;

//
// resource_pool_info class methods
//

//
// initialize - the system determines the values for one resource pool.
//
void
resource_balancer::resource_pool_info::initialize(
    resource_pool_static_values *pool_valuesp)
{
	threads_low	= pool_valuesp->threads_low;
	threads_total	= 0;
	threads_minimum	= pool_valuesp->threads_minimum;
	threads_unallocated = 0;
	valuesp		= pool_valuesp;

	for (nodeid_t node = 0; node <= NODEID_MAX; node++) {
		//
		// No threads allocated to clients
		// till node reconfiguration.
		//
		threads_allocated[node] = 0;
		//
		// No threads recalled initially
		//
		threads_recalled[node] = 0;
		//
		// No threads approved for allocation pending recall
		//
		threads_pending[node] = 0;
		//
		// Does not yet need initial set of resources
		//
		needs_initial[node] = false;
	}

	//
	// The minimum number of server threads on real clusters may be
	// increased if conditions warrant.
	//
	if (valuesp->pool == resource_defs::DEFAULT_POOL) {
		if (min_threads_default_pool >= default_pool_threads_minimum) {
			//
			// System administrator has specified a valid value
			// for the minimum number of server threads.
			//
			threads_minimum = min_threads_default_pool;
		} else {
			// Calculate the minimum number of server threads
			//
			int	threads_min = maxusers * maxusers_factor_min;

			if (threads_min > threads_minimum) {
				threads_minimum = threads_min;
			}
			//
			// On large machines with gigabytes of memory, this
			// number becomes unreasonably high (> 4000
			// sometimes). Limit the minimun number of threads to
			// 1000.
			//
			if (threads_minimum > 1000) {
				threads_minimum = 1000;
			}
		}
	}

	//
	// Determine thread increment size
	//
	if (valuesp->fixed_size) {
		//
		// Fixed size resource pool will choose increment size
		// based on initial number of threads granted to each client
		//
		threads_increment = threads_low / fix_size_inc_factor;
	} else {
		// Variable size resource pool will
		// choose increment size based on minimum size,
		// because we do not know how large it will become.
		//
		threads_increment = threads_minimum / vary_size_inc_factor;
	}
	if (threads_increment == 0) {
		threads_increment = 1;
	}

	//
	// The initial number of server threads on real clusters may be
	// increased if conditions warrant. Currently only the default
	// resource pool automatically expands with system resources.
	//
	if (valuesp->pool == resource_defs::DEFAULT_POOL) {
		int	threads_lo = threads_increment * initial_alloc_factor;
		if (threads_lo > threads_low) {
			threads_low = threads_lo;
		}
	}

	//
	// Determine moderate thread count
	//
	if (valuesp->fixed_size) {
		//
		// For fixed size resource pools the number of threads
		// does not change. Thus the moderate level is the same
		// as the initial level.
		//
		threads_moderate = threads_low;
	} else {
		// This value is recomputed at node reconfiguration time.
		threads_moderate = threads_low + threads_increment;
	}

	// Determine upper bounds on number of server threads
	threads_high = NODEID_MAX * threads_low;

	//
	// The high number of server threads on real clusters may be
	// increased if conditions warrant. Currently only the default
	// resource pool automatically expands with system resources.
	//
	if (valuesp->pool == resource_defs::DEFAULT_POOL) {
		int	threads_hi;
		if (min_threads_default_pool >= default_pool_threads_minimum) {
			//
			// System administrator has specified a valid value
			// for the minimum number of server threads.
			// Use that value to calculate upper bounds on threads.
			//
			threads_hi = (threads_minimum / maxusers_factor_min) *
			    maxusers_factor_max;
		} else {
			// Calculate the upper bounds on server threads
			//
			threads_hi = maxusers * maxusers_factor_max;

#ifdef _KERNEL
			//
			// Allow as many invocation server threads as the
			// maximum number of processes on a real cluster.
			//
			if (threads_hi < v.v_proc) {
				threads_hi = v.v_proc;
			}
#endif
		}

		//
		// The upper bounds must be high enough to allow each potential
		// node in the cluster to have a minimum number of threads.
		//
		if (threads_hi > threads_high) {
			threads_high = threads_hi;
		}
	}

	if (!valuesp->fixed_size) {
		//
		// Variable size resource pools may require adjustment.
		//
		// Ensure that some values are multiples of the increment
		// value. Round down if needed.
		//
		threads_minimum = ((threads_minimum + threads_increment - 1) /
		    threads_increment) * threads_increment;
		threads_high = ((threads_high + threads_increment - 1) /
		    threads_increment) * threads_increment;
	}

	// This occurs on the initial startup
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("resource_balancer: initial values: pool %d\n"
	    "\tlo %d mod %d hi %d min %d inc %d\n",
		valuesp->pool, threads_low, threads_moderate,
		threads_high, threads_minimum, threads_increment));
}

//
// resource_balancer class methods
//

//
// resource_balancer constructor
//
resource_balancer::resource_balancer() :
	resource_balancer_msg(SC_SYSLOG_FRAMEWORK_TAG, SC_SYSLOG_FRAMEWORK_RSRC,
	    NULL)
{
	nodeid_t	node;

#ifdef _KERNEL
	// This occurs on the initial startup
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("resource_balancer: maxusers %d availrmem %d\n",
	    maxusers, availrmem));
#endif

	for (int i = 0; i < resource_defs::NUMBER_POOLS; i++) {
		pool_info[i].initialize(&pool_values[i]);

		if (!validate_policy((resource_defs::resource_pool_t)i,
		    NODEID_MAX,
		    pool_info[i].threads_low,
		    pool_info[i].threads_moderate,
		    pool_info[i].threads_high,
		    pool_info[i].threads_increment)) {
			//
			// SCMSGS
			// @explanation
			// The flow control policy is controlled by a set of
			// parameters. These parameters do not satisfy
			// guidelines. Another message from validay_policy
			// will have already identified the specific problem.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) resource_balancer_msg.log(SC_SYSLOG_PANIC,
			    MESSAGE, "clcomm: Invalid flow control parameters");
		}

		// Allocate minimum set of server threads
		pool_info[i].threads_unallocated = orb_msg::the().
		    change_num_threads(orb_msg::THREADPOOL_INVOS,
		    pool_info[i].threads_minimum);

		pool_info[i].threads_total = pool_info[i].threads_unallocated;

#ifdef DEBUG
		lck.lock();
		validate_number_threads();
		lck.unlock();
#endif

		if (pool_info[i].threads_unallocated == 0) {
			//
			// SCMSGS
			// @explanation
			// The system creates server threads to support
			// requests from other nodes in the cluster. The
			// system could not create any server threads during
			// system startup. This is caused by a lack of memory.
			// @user_action
			// There are two solutions. Install more memory.
			// Alternatively, take steps to reduce memory usage.
			// Since the creation of server threads takes place
			// during system startup, application memory usage is
			// normally not a factor.
			//
			(void) resource_balancer_msg.log(SC_SYSLOG_PANIC,
			    MESSAGE,
			    "clcomm: Could not create any threads for pool %d",
			    i);
		}
		if (pool_info[i].threads_unallocated !=
		    pool_info[i].threads_minimum) {
			//
			// The system is probably in serious trouble,
			// because we should be able to create the minimum
			// initial set of server threads.
			// However, the system did create at least one thread.
			// So the system may be able to persevere. The
			// system will try again later to create more threads.
			//

			//
			// SCMSGS
			// @explanation
			// The system creates server threads to support
			// requests from other nodes in the cluster. The
			// system could not create the desired minimum number
			// of server threads. However, the system did succeed
			// in creating at least 1 server thread. The system
			// will have further opportunities to create more
			// server threads. The system cannot create server
			// threads when there is inadequate memory. This
			// message indicates either inadequate memory or an
			// incorrect configuration.
			// @user_action
			// There are multiple possible root causes.
			//
			// If the system administrator specified the value of
			// "maxusers", try reducing the value of "maxusers".
			// This reduces memory usage and results in the
			// creation of fewer server threads.
			//
			// If the system administrator specified the value of
			// "cl_comm:min_threads_default_pool" in
			// "/etc/system", try reducing this value. This
			// directly reduces the number of server threads.
			// Alternatively, do not specify this value. The
			// system can automatically select an appropriate
			// number of server threads.
			//
			// Another alternative is to install more memory.
			//
			// If the system administrator did not modify either
			// "maxusers" or "min_threads_default_pool", then the
			// system should have selected an appropriate number
			// of server threads. Contact your authorized Sun
			// service provider to determine whether a workaround
			// or patch is available.
			//
			(void) resource_balancer_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "clcomm: Created %d threads, wanted %d for pool %d",
			    pool_info[i].threads_unallocated,
			    pool_info[i].threads_minimum,
			    i);
		}
	}
	for (node = 0; node <= NODEID_MAX; node++) {
		node_incn[node] = INCN_UNKNOWN;
	}
}

// static
int
resource_balancer::initialize()
{
	ASSERT(the_resource_balancer == NULL);
	the_resource_balancer = new resource_balancer();
	if (the_resource_balancer == NULL) {
		return (ENOMEM);
	}
	return (the_resource_balancer->initialize_thread());
}

void
resource_balancer::shutdown()
{
	if (the_resource_balancer != NULL) {
		delete the_resource_balancer;
		the_resource_balancer = NULL;
	}
}

//
// resource_thread - becomes a thread that periodically manages resources
//
// static
void
resource_balancer::resource_thread(void *)
{
	// Initialized in resource_balancer::initialize_thread

	os::usec_t		thread_sleep = resource_time_period;

	resource_balancer	&balancer = resource_balancer::the();

	balancer.lock();
	//
	// The resource thread should loop forever unless told to shutdown
	//
				/* CSTYLED */
	while (true) {		//lint !e716 !e774
		//
		// Wake up after a time period or when ordered to shutdown
		//
		os::systime	time_out;
		time_out.setreltime(thread_sleep);
		(void) balancer.threadcv.timedwait(&balancer.threadLock,
		    &time_out);

		if (balancer.state != orbthread::ACTIVE) {
			// Need to shutdown
			break;
		}

		// Manage resource usage in unref_threadpool
		balancer.manage_unrefs();

		//
		// Manage the resources used by real client nodes
		//
		for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
			for (int pool = 0; pool < resource_defs::NUMBER_POOLS;
			    pool++) {
				// Flush old done notices
				resource_done::select_resource_done(node,
				    (resource_defs::resource_pool_t)pool)->
				    flush_notices(node);

				//
				// Check resource pools to see if any need
				// resource changes.
				//
				resource_pool::select_resource_pool(node,
				    (resource_defs::resource_pool_t)pool)->
				    check_for_resource_changes();
			}
		}
	}
	// The thread will now go away.
	balancer.state = orbthread::DEAD;
	balancer.threadGone.signal();
	balancer.unlock();
}

//
// initialize_thread - starts up the thread that periodically manages resources.
//
int
resource_balancer::initialize_thread()
{
	// Register with the list of orb threads
	registerthread();

	// Create the thread that manages resources
#ifdef _KERNEL
	if (clnewlwp((void(*)(void *))resource_balancer::resource_thread,
	    NULL, 60, NULL, NULL) != 0) {
#else
	if (os::thread::create(NULL, (size_t)0,
	    (void *(*)(void *))resource_balancer::resource_thread,
	    NULL, (long)(THR_BOUND | THR_DETACHED), NULL)) {
#endif
		//
		// SCMSGS
		// @explanation
		// The system could not create the needed thread, because
		// there is inadequate memory.
		// @user_action
		// There are two possible solutions. Install more memory.
		// Alternatively, reduce memory usage. Since this happens
		// during system startup, application memory usage is normally
		// not a factor.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: failed to create thread for resource_thread");

		(void) unregisterthread();
		return (ENOMEM);
	}
	return (0);
}

//
// manage_unrefs - manage the resource usage by the unref_threadpool.
// We give the unreference processing priority over invocation processing,
// because unreference processing frees resources while invocations
// generally consume resources.
//
void
resource_balancer::manage_unrefs()
{
	unref_threadpool	&unref = unref_threadpool::the();

	int			unref_needs;

	ASSERT(pool_info[resource_defs::DEFAULT_POOL].threads_recalled[0] == 0);

	//
	// Find out what resource thread change the unref_threadpool wants.
	// The unref_threadpool will not ask for a change while a transfer
	// is in progress.
	//
	unref_needs = unref.manage_thread_usage(
	    pool_info[resource_defs::DEFAULT_POOL].threads_increment,
	    pool_info[resource_defs::DEFAULT_POOL].threads_allocated[0] +
	    pool_info[resource_defs::DEFAULT_POOL].threads_pending[0]);

	if (unref_needs == 0) {
		// No change required
		return;
	}
	if (unref_needs < 0) {
		//
		// The unref threadpool wants to return threads.
		// We can always accept threads.
		//
		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("transfer from unref_threadpool threads %d\n",
		    -unref_needs));

		orb_msg::the().server_threads[orb_msg::THREADPOOL_INVOS].
		    transfer_worker_threads(-unref_needs, &unref);

		// The unref_threadpool pretends to be node 0
		ID_node		unref_node(0, 0);

		// Make the release look like it came from a client
		resource_grant	grant(resource_defs::DEFAULT_POOL,
				    -unref_needs);

		lck.lock();

		// Redistribute the resources from the unref threadpool
		resource_release(unref_node, grant, false);
	} else {
		//
		// The unref threadpool wants more threads.
		//
		lck.lock();

		//
		// Do not let the unref threadpool grab all of the threads.
		// Want to leave enough threads so that a node joining the
		// cluster can get its initial allotment of threads.
		// If the unref threadpool has almost all of the threads,
		// then the unref threadpool should clear its backlog
		// relatively quickly. When multiple nodes join the cluster
		// and the unref threadpool has almost all of the threads,
		// there will be a little delay before all of these nodes
		// get threads. But that should not be a problem.
		//
		if ((pool_info[resource_defs::DEFAULT_POOL].threads_total -
		    pool_info[resource_defs::DEFAULT_POOL].threads_allocated[0])
		    <=
		    pool_info[resource_defs::DEFAULT_POOL].threads_low) {
			lck.unlock();
			return;
		}

		// The unref_threadpool pretends to be node 0
		ID_node		unref_node(0, 0);

		orb_stats_mgr::the().inc_balancer_requests(0);

		// Make the request look like it came from a client
		request_resources(unref_node, resource_defs::DEFAULT_POOL,
		    unref_needs);
	}
	ASSERT(!lck.lock_held());
}

#ifdef DEBUG
//
// validate_number_threads - check whether the accounting info about
// the number of threads matches reality.
//
void
resource_balancer::validate_number_threads()
{
	ASSERT(lck.lock_held());

	//
	// Add up the number of threads among all pools.
	//
	int	threads_count = 0;
	for (int i = 0; i < resource_defs::NUMBER_POOLS; i++) {
		threads_count += pool_info[i].threads_total;
	}

	// Subtract number of threads loaned to the unref threadpool.
	threads_count -=
	    pool_info[resource_defs::DEFAULT_POOL].threads_allocated[0];

	int	threads_actual = orb_msg::the().
		    server_threads[orb_msg::THREADPOOL_INVOS].
		    total_number_threads();

	int	threads_transfer =
		    pool_info[resource_defs::DEFAULT_POOL].threads_increment;

	//
	// The system can be transferring up to an increment worth of threads
	// in either direction between the general invocation threadpool
	// and the unref threadpool. The system only allows one active transfer.
	//
	ASSERT(threads_actual <= threads_count + threads_transfer ||
	    threads_actual >= threads_count - threads_transfer);
}
#endif

//
// reallocate_resources - could not allocate more threads.
// Attempt to recall threads from a resource pool with an abundance.
// If the requestor has more threads than any other resource pool,
// deny the request.
//
// XXX - Currently only support recalling an increment worth of threads.
// XXX - There may be some unallocated threads.
// XXX - These are ignored because they will be less than an "increment".
//
void
resource_balancer::reallocate_resources(ID_node &src_node,
    resource_defs::resource_pool_t pool)
{
	ASSERT(lck.lock_held());

	nodeid_t	high_node = src_node.ndid;
	int		thread_count =
			    pool_info[pool].threads_allocated[src_node.ndid];

	//
	// Find the node with the most threads
	// Never reallocate from the fake node representing the unref threadpool
	//
	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		if (pool_info[pool].threads_allocated[high_node] <
		    ((pool_info[pool].threads_allocated[node] +
		    pool_info[pool].threads_pending[node]) -
		    pool_info[pool].threads_recalled[node])) {
			high_node = node;
			thread_count = pool_info[pool].threads_allocated[node];
		}
	}

	//
	// The victim node must be different from the requesting node.
	// In order to prevent the frequent ping-pong of threads between
	// busy nodes, the system requires that the victim node have
	// considerably more resources than the requestor.
	//
	// Note that a node cannot have considerably more threads than itself.
	//
	if ((reallocation_inc_multiple * pool_info[pool].threads_increment) <=
	    thread_count - pool_info[pool].threads_allocated[src_node.ndid]) {
		ASSERT(high_node != src_node.ndid);
		//
		// Found a client node that has much more resources
		//
		// Only one request at a time should be outstanding
		ASSERT(pool_info[pool].threads_pending[src_node.ndid] == 0);
		//
		// Mark the requesting node.
		//
		pool_info[pool].threads_pending[src_node.ndid] =
		    pool_info[pool].threads_increment;

		if (pool_info[pool].threads_pending[high_node] > 0) {
			//
			// The victim was scheduled to get more threads.
			// We will steal the victim's allocation.
			//
			ASSERT(pool_info[pool].threads_pending[high_node] ==
			    pool_info[pool].threads_increment);

			pool_info[pool].threads_pending[high_node] = 0;

			ID_node	dest_node(high_node, node_incn[high_node]);

			lck.unlock();

			resource_grant	grant(pool, 0);

			ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
			    ("reallocate_resources victim (%d.%3d,%d) a %d\n",
			    dest_node.ndid, (dest_node.incn % 1000), pool,
			    pool_info[pool].threads_allocated[high_node]));

			orb_stats_mgr::the().
			    inc_balancer_denies(dest_node.ndid);

			// Tell the client that the request was denied
			resource_mgr::send_message(dest_node,
			    resource_mgr::RESOURCE_GRANT,
			    (void *)&grant, (uint_t)(sizeof (resource_grant)));

			return;
		}

		//
		// Mark the victim node.
		// A node with a lot of resources may be victimized
		// multiple times simultaneously.
		//
		pool_info[pool].threads_recalled[high_node] +=
		    pool_info[pool].threads_increment;

		resource_grant	grant(pool, pool_info[pool].threads_increment);
		ID_node		dest_node(high_node, node_incn[high_node]);

		lck.unlock();

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("reallocate_resources take (%d.%3d,%d) t %d a %d\n",
		    dest_node.ndid, (dest_node.incn % 1000), pool,
		    grant.threads,
		    pool_info[pool].threads_allocated[high_node]));

		orb_stats_mgr::the().inc_balancer_recalls(dest_node.ndid);

		// Tell the client to release resources
		resource_mgr::send_message(dest_node,
		    resource_mgr::RESOURCE_RECALL,
		    (void *)&grant, (uint_t)(sizeof (resource_grant)));
	} else {
		// No victim nodes are available.
		//
		// The client must have some threads if the thread values
		// have been properly configured.
		//
		ASSERT(pool_info[pool].threads_allocated[src_node.ndid] > 0);

		lck.unlock();

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("reallocate_resources failed (%d.%3d,%d) a %d\n",
		    src_node.ndid, (src_node.incn % 1000), pool,
		    pool_info[pool].threads_allocated[src_node.ndid]));

		orb_stats_mgr::the().inc_balancer_denies(src_node.ndid);

		if (src_node.ndid == 0) {
			//
			// The request came from the unref threadpool.
			// Silently ignore the request.
			// The unref threadpool will try again
			// during the next time period.
			//
			return;
		}
		// The request came from a client node.

		resource_grant	grant(pool, 0);

		// Tell the client that the request was denied
		resource_mgr::send_message(src_node,
		    resource_mgr::RESOURCE_GRANT,
		    (void *)&grant, (uint_t)(sizeof (resource_grant)));
	}
}

//
// process_request_resources - a request from a client node for a
// set of resources has arrived on the server node. The server grants resources
// to the client.
//
void
resource_balancer::process_request_resources(ID_node &src_node,
    MarshalStream &recmarsh)
{
	lck.lock();
	if (src_node.incn != node_incn[src_node.ndid]) {
		// Discard orphan request

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_request_additional orphan (%d.%3d)\n",
		    src_node.ndid, (src_node.incn % 1000)));

		lck.unlock();

		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}
	orb_stats_mgr::the().inc_balancer_requests(src_node.ndid);

	resource_defs::resource_pool_t	src_pool;
	uint8_t		payload;

	// Unmarshal request payload
	recmarsh.get_bytes((void *)&payload, (uint_t)(sizeof (uint8_t)));
	ASSERT(payload < resource_defs::NUMBER_POOLS);

	// Convert byte to enum type with appropriate sign extension
	src_pool = (resource_defs::resource_pool_t)payload;

	// Only one change request at a time is permitted
	ASSERT(pool_info[src_pool].threads_pending[src_node.ndid] == 0);

	ASSERT(pool_info[src_pool].threads_allocated[src_node.ndid] >= 0);

	if (pool_info[src_pool].threads_recalled[src_node.ndid] > 0) {
		// Server wants resources back from client.
		// Request denied.
		lck.unlock();

		resource_grant	grant(src_pool, 0);

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_request_additional denied (%d.%3d,%d) a %d\n",
		    src_node.ndid, (src_node.incn % 1000), src_pool,
		    pool_info[src_pool].threads_allocated[src_node.ndid]));

		orb_stats_mgr::the().inc_balancer_denies(src_node.ndid);

		// Tell the client request denied
		resource_mgr::send_message(src_node,
		    resource_mgr::RESOURCE_GRANT,
		    (void *)&grant, (uint_t)(sizeof (resource_grant)));

		return;
	}

	int	threads_needed;

	//
	// First check - does the client have the minimum number of threads ?
	// and
	// Second check - can the system give enough threads to bring the
	// client up to the minimum value ?
	//
	if (pool_info[src_pool].threads_allocated[src_node.ndid] <
	    pool_info[src_pool].threads_low &&
	    (pool_info[src_pool].threads_high >=
	    pool_info[src_pool].threads_total +
	    (pool_info[src_pool].threads_low -
	    pool_info[src_pool].threads_allocated[src_node.ndid]))) {
		//
		// The client does not have the desirable minimum
		// number of threads.
		//
		threads_needed = pool_info[src_pool].threads_low -
		    pool_info[src_pool].threads_allocated[src_node.ndid];

		//
		// Fault point used to verify new node entry.
		//
		FAULTPT_FLOWCONTROL
		    (FAULTNUM_FLOWCONTROL_PROCESS_REQUEST_RESOURCES_1,
			fault_flowcontrol::generic_test_op);
	} else {
		// Otherwise use the increment value
		threads_needed = pool_info[src_pool].threads_increment;
	}

	// The request has been unmarshalled and is  valid
	request_resources(src_node, src_pool, threads_needed);
	ASSERT(!lck.lock_held());
}

//
// request_resources - a non-orphan request arrived from a node
// that does not have a pending recall. This method will attempt
// to provide "threads_needed" number of threads to the requesting node.
//
void
resource_balancer::request_resources(ID_node	&src_node,
    resource_defs::resource_pool_t	src_pool,
    int		threads_needed)
{
	ASSERT(lck.lock_held());

	if (pool_info[src_pool].threads_high <
	    (pool_info[src_pool].threads_total + threads_needed) -
	    pool_info[src_pool].threads_unallocated) {
		//
		// Cannot allocate more threads.
		// Must reallocate threads from other clients or
		// deny the request.
		//
		reallocate_resources(src_node, src_pool);
		ASSERT(!lck.lock_held());
		return;
	}
	int	threads_already;
	//
	// Check for unallocated threads.
	//
	int	unallocated = pool_info[src_pool].threads_unallocated;
	if (unallocated == 0) {
		threads_already = 0;
	} else if (threads_needed > unallocated) {
		// Take as many threads as possible from the free ones
		threads_already = unallocated;
		pool_info[src_pool].threads_unallocated = 0;
	} else {
		// Take all needed threads from the free ones
		threads_already = threads_needed;
		pool_info[src_pool].threads_unallocated -= threads_needed;
	}

	int	change_needed = threads_needed - threads_already;

#ifdef _KERNEL
	//
	// Does system have adequate memory for more server threads ?
	//
	// Each server thread on average needs some pages of memory for
	// its stack and work space.
	//
	// We do not want to drop down to the absolute minimum.
	// So we pad the request by asking for twice the number of pages
	// that we think we might need.
	//
	if (change_needed > 0) {
		//
		pgcnt_t		memory_needs =
		    (pgcnt_t)(tune.t_minarmem + (2 * (change_needed *
		    pages_per_server_thread)));		//lint !e571 !e647
		if (availrmem <= memory_needs) {
			//
			// Not enough memory to safely allocate more
			// server threads.
			//
			// Return any threads we were about to grab from
			// the pool of unallocated threads.
			//
			pool_info[src_pool].threads_unallocated = unallocated;
			//
			// Must reallocate threads from other clients or
			// deny the request.
			//
			reallocate_resources(src_node, src_pool);
			ASSERT(!lck.lock_held());
			return;
		}
	}
#endif	// _KERNEL

	//
	// Allocate needed new threads.
	// It is safe to request a zero change.
	//
	int	change = orb_msg::the().change_num_threads(
		    orb_msg::THREADPOOL_INVOS, change_needed);
	//
	// The number of threads must be a positive number.
	// The client will ask for more if it does not get enough.
	//
	ASSERT(change >= 0);

	pool_info[src_pool].threads_total += change;

#ifdef DEBUG
	validate_number_threads();
#endif

	change += threads_already;

	if (pool_values[src_pool].fixed_size &&
	    change != pool_info[src_pool].threads_low) {
		//
		// SCMSGS
		// @explanation
		// The system can create a fixed number of server threads
		// dedicated for a specific purpose. The system expects to be
		// able to create this fixed number of threads. The system
		// could fail under certain scenarios without the specified
		// number of threads. The server node creates these server
		// threads when another node joins the cluster. The system
		// cannot create a thread when there is inadequate memory.
		// @user_action
		// There are two possible solutions. Install more memory.
		// Alternatively, reduce memory usage. Application memory
		// usage could be a factor, if the error occurs when a node
		// joins an operational cluster and not during cluster
		// startup.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Fixed size resource_pool short server threads:"
		    " pool %d for client %d total %d", src_pool, src_node.ndid,
		    pool_info[src_pool].threads_allocated[src_node.ndid]);
	}

	pool_info[src_pool].threads_allocated[src_node.ndid] += change;

	// Tell the client about the resource grant
	send_grant(src_node, src_pool, change);
}

//
// process_resource_release - the client node has released reserved resources.
// Unmarshal the client information and then process the release.
//
void
resource_balancer::process_resource_release(ID_node &src_node,
    MarshalStream &recmarsh, bool acknowledge_needed)
{
	lck.lock();
	if (src_node.incn != node_incn[src_node.ndid]) {
		// Discard orphan release

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("process_resource_release orphan (%d.%3d)\n",
		    src_node.ndid, (src_node.incn % 1000)));

		lck.unlock();

		orb_stats_mgr::the().inc_orphans(src_node.ndid, FLOW_MSG);
		return;
	}
	orb_stats_mgr::the().inc_balancer_releases(src_node.ndid);

	resource_grant	grant;

	// Unmarshal initial request payload
	recmarsh.get_bytes((void *)&grant, (uint_t)(sizeof (resource_grant)));
	ASSERT(grant.pool < resource_defs::NUMBER_POOLS);

	// Release the resources
	resource_release(src_node, grant, acknowledge_needed);

	ASSERT(!lck.lock_held());
}

//
// resource_release - adjust the client resource usage information
// and then redistribute the resources
//
void
resource_balancer::resource_release(ID_node &src_node,
    resource_grant &grant, bool acknowledge_needed)
{
	ASSERT(lck.lock_held());

	ASSERT(grant.threads > 0);

	// Only one change request at a time is permitted
	ASSERT(pool_info[grant.pool].threads_pending[src_node.ndid] == 0);

	// The client must have been allocated at least this many threads
	ASSERT(pool_info[grant.pool].threads_allocated[src_node.ndid] >=
	    grant.threads);

	// Show that the client no longer has these threads
	pool_info[grant.pool].threads_allocated[src_node.ndid] -= grant.threads;

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("resource_release (%d.%3d,%d) t %d a %d\n",
	    src_node.ndid, (src_node.incn % 1000), grant.pool, grant.threads,
	    pool_info[grant.pool].threads_allocated[src_node.ndid]));

	if (pool_info[grant.pool].threads_recalled[src_node.ndid] > 0) {
		// The threads have been recalled
		ASSERT(pool_info[grant.pool].threads_recalled[src_node.ndid] >=
		    grant.threads);

		pool_info[grant.pool].threads_recalled[src_node.ndid] -=
		    grant.threads;
	}

	//
	// Find the node with the fewest threads that has a pending request
	// for more threads.
	//
	nodeid_t	low_node = 0;
	int		count_threads = INT_MAX;
	if (grant.pool == resource_defs::DEFAULT_POOL &&
	    pool_info[grant.pool].threads_pending[0] > 0) {
		//
		// The fake node representing the unref threadpool can have
		// a pending request. The unref threadpool gets resources
		// before real nodes.
		//
		// low_node is already zero which represents unref_threadpool
		//
		count_threads = grant.threads;
	} else {
		for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
			if (pool_info[grant.pool].threads_pending[node] > 0 &&
			    count_threads >
			    pool_info[grant.pool].threads_allocated[node]) {
				//
				// Found a node needing threads with
				// fewer threads.
				//
				low_node = node;
				count_threads = pool_info[grant.pool].
				    threads_allocated[node];
			}
		}
	}

	if (count_threads == INT_MAX) {
		// No node needs these threads.
		//
		if (pool_info[grant.pool].threads_total - grant.threads <=
		    pool_info[grant.pool].threads_minimum) {
			//
			// Keep threads in the unallocated pool.
			// It is OK to have extra unallocated threads.
			//
			pool_info[grant.pool].threads_unallocated +=
			    grant.threads;
		} else {
			//
			// Release the threads
			//
			int	change = orb_msg::the().change_num_threads(
			    orb_msg::THREADPOOL_INVOS, -grant.threads);

			// Should always be able to release threads
			ASSERT(change == -grant.threads);

			pool_info[grant.pool].threads_total -= grant.threads;

#ifdef DEBUG
			validate_number_threads();
#endif
		}
		lck.unlock();
	} else {
		// Give the threads to the node with the fewest threads
		//
		ASSERT(pool_info[grant.pool].threads_pending[low_node] ==
		    grant.threads);

		pool_info[grant.pool].threads_pending[low_node] -=
			grant.threads;
		pool_info[grant.pool].threads_allocated[low_node] +=
			grant.threads;

		ID_node		dest_node(low_node, node_incn[low_node]);

		// Tell the client about the resource grant
		send_grant(dest_node, grant.pool, grant.threads);
	}

	if (acknowledge_needed) {
		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("send_resource_release_ack (%d.%3d,%d) t %d a %d\n",
		    src_node.ndid, (src_node.incn % 1000), grant.pool,
		    grant.threads,
		    pool_info[grant.pool].threads_allocated[src_node.ndid]));

		if (src_node.ndid == 0) {
			ASSERT(grant.pool == resource_defs::DEFAULT_POOL);
		} else {
			resource_mgr::send_message(src_node,
			    resource_mgr::RESOURCE_RELEASE_ACK,
			    (void *)&grant,
			    (uint_t)(sizeof (resource_grant)));
		}
	}
}

//
// node_died - release the resources held by the node that died.
//
void
resource_balancer::node_reconfiguration()
{
	int			i;
	incarnation_num		new_incn;

	lck.lock();
	//
	// The system does not guarantee that a "node died" event will be seen
	// in between two consecutive "node alive" events. So this object
	// must keep track of the incarnation numbers in order to ensure that
	// it always processes a "node died" event before allowing a node to
	// rejoin the cluster.
	//
	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		new_incn = members::the().incn(node);
		if (node == orb_conf::node_number()) {
			//
			// The local node does not need remote
			// server threads for invocations to itself.
			//
			node_incn[node] = new_incn;
			continue;
		}
		if (node_incn[node] != INCN_UNKNOWN &&
		    node_incn[node] != new_incn) {
			//
			// The node went from alive to a different incarnation.
			// The node had to die to change incarnation.
			// Therefore release its resources.
			// If the node has rejoined, it will ask for new
			// resources.
			//
			for (i = 0; i < resource_defs::NUMBER_POOLS; i++) {
				//
				// The live node may have died prior to
				// obtaining any server threads.
				//
				ASSERT(pool_info[i].threads_allocated[node] >=
				    0);
				//
				// Take threads away from the failed node.
				// Do not send messages during reconfig step 1.
				// These threads will be handed out or destroyed
				// during reconfig step 1.
				// Cannot destroy these threads because some
				// other node may be waiting on a thread recall.
				//
				pool_info[i].threads_unallocated +=
					pool_info[i].threads_allocated[node];

				pool_info[i].threads_allocated[node] = 0;
				pool_info[i].threads_recalled[node] = 0;
				pool_info[i].threads_pending[node] = 0;

				// Dead nodes do not need to initiate contact
				pool_info[i].needs_initial[node] = false;
			}
		}
		if (new_incn != INCN_UNKNOWN &&
		    new_incn != node_incn[node]) {
			//
			// The node is alive now and has a
			// new incarnation number.
			// This means that the node has
			// just joined the cluster.
			//
			for (i = 0; i < resource_defs::NUMBER_POOLS; i++) {
				pool_info[i].needs_initial[node] = true;
			}
		}
		node_incn[node] = new_incn;
	}
	lck.unlock();
}

//
// reconfig_reallocate_resources - hand out threads taken from dead nodes in
// reconfiguration step 1, and distribute updated policy parameters to
// all active nodes.
//
void
resource_balancer::reconfig_reallocate_resources()
{
	reallocate_resources_from_dead_nodes();

	//
	// Distribute flow control policy to all active nodes.
	//
	distribute_policy();
}

//
// reallocate_resources_from_dead_nodes - communications are not allowed
// during reconfiguration step 1. Therefore we delay until reconfiguration
// step 2 the reallocation of resources reclaimed from dead nodes.
// Another node may be waiting for theses resources, and thus we cannot
// simply release the resources in reconfiguration step 1.
//
void
resource_balancer::reallocate_resources_from_dead_nodes()
{
	int	i;

	lck.lock();
	//
	// Grant any pending requests for threads as long as threads exist.
	// The fake node representing the unref threadpool can have a
	// pending request.
	//
	for (i = 0; i < resource_defs::NUMBER_POOLS; i++) {
		for (nodeid_t node = 0; node <= NODEID_MAX; node++) {
			int	pending = pool_info[i].threads_pending[node];

			if (pending > 0 &&
			    pool_info[i].threads_unallocated >= pending) {
				//
				// Have enough threads for this node
				//
				ASSERT(pool_info[i].threads_recalled[node] ==
				    0);

				pool_info[i].threads_unallocated -= pending;
				pool_info[i].threads_allocated[node] += pending;
				pool_info[i].threads_pending[node] = 0;

				ID_node	dest_node(node, node_incn[node]);

				// Tell the client about the resource grant
				send_grant(dest_node,
				    (resource_defs::resource_pool_t)i, pending);

				lck.lock();
			}
		}
		ASSERT(pool_info[i].threads_unallocated >= 0);
	}
	//
	// Time to deal with surplus threads left over from dead nodes.
	//
	// The number of server threads supporting a resource pool
	// can not drop to zero, because of the
	// threat from orphan messages. We do not know when the last orphan
	// message has been delivered to a server thread.
	//
	// The threads_minimum feature ensures that we always have a
	// non-zero number of server threads for the default resource pool.
	//
	// Also must ensure that the number server threads does not drop
	// below the minimum level.
	//
	// The threads borrowed by the unref threadpool are not available
	// to process orphan messages.
	//
	for (i = 0; i < resource_defs::NUMBER_POOLS; i++) {
		if (pool_info[i].threads_unallocated > 0 &&
		    pool_info[i].threads_total > pool_info[i].threads_minimum) {
			//
			// Fault point for destroying threads during
			// reconfiguration
			//
			FAULTPT_FLOWCONTROL
			    (FAULTNUM_FLOWCONTROL_DESTROY_THREADS,
			    fault_flowcontrol::generic_test_op);

			//
			// Have more threads than needed.
			//
			int	change = pool_info[i].threads_total -
			    pool_info[i].threads_minimum;
			if (change > pool_info[i].threads_unallocated) {
				change = pool_info[i].threads_unallocated;
			}
			discard_surplus_threads(
			    (resource_defs::resource_pool_t)i, change);
		}
	}
	lck.unlock();
}

//
// discard_surplus_threads - we have more threads than needed.
// After checking to see that we have enough threads, the method
// destroys no longer need threads.
//
void
resource_balancer::discard_surplus_threads(
    resource_defs::resource_pool_t pool, int change)
{
	ASSERT(lck.lock_held());

	if (pool == resource_defs::DEFAULT_POOL) {
		//
		// Ensure that we still have some threads
		// for processing incoming invocations
		// after allowing for borrowed threads.
		//
		int	change2 = pool_info[pool].threads_total -
		    (pool_info[pool].threads_allocated[0] +
		    pool_info[pool].threads_low);
		ASSERT(change2 >= 0);
		if (change > change2) {
			change = change2;
			if (change == 0) {
				return;
			}
		}
	}

	if (change == 0) {
		return;
	}

	//
	// Destroy threads not needed
	//
	(void) orb_msg::the().change_num_threads(
	    orb_msg::THREADPOOL_INVOS, -change);

	// Reduce total number of threads in existance
	ASSERT(pool_info[pool].threads_total >= change);
	pool_info[pool].threads_total -= change;
	pool_info[pool].threads_unallocated -= change;

#ifdef DEBUG
	validate_number_threads();
#endif
}

//
// distribute_policy -
// Computes a new moderate thread level for a variable size resource pool.
// A new policy is sent to all nodes for a variable size resource pool.
//
// Any node joining a cluster requests resources AFTER receiving
// policy information. So send policy information to all newly joined
// clients for each resource pool. The server node initiates contact
// with the client node in this manner.
//
void
resource_balancer::distribute_policy()
{
	for (int pool = 0; pool < resource_defs::NUMBER_POOLS; pool++) {
		if (!pool_values[pool].fixed_size) {
			//
			// The moderate thread level only varies for
			// variable size resource pools.
			// Compute new setting for moderate number threads.
			//
			int	number_client_nodes = number_nodes() - 1;
			if (number_client_nodes == 0) {
				//
				// Avoid divide by zero problem.
				// This happens when the server node is the only
				// node in the cluster.
				//
				number_client_nodes = 1;
			}
			//
			// Want to encourage an equal distribution of threads
			// among client nodes. Round down to the nearest
			// multiple of the increment value.
			//
			pool_info[pool].threads_moderate =
			    ((pool_info[pool].threads_high /
			    number_client_nodes) /
			    pool_info[pool].threads_increment) *
			    pool_info[pool].threads_increment;
		}
		//
		// All nodes receive the same policy information
		// for any resource pool.
		//
		// The mechanism for ensuring that a fixed
		// size pool remains fixed, is to tell the
		// client that low, moderate, and high are
		// the same.
		//
		resource_policy		policy(
		    (resource_defs::resource_pool_t)pool,
		    pool_info[pool].threads_low,
		    pool_info[pool].threads_moderate,
		    pool_values[pool].fixed_size ?
			pool_info[pool].threads_low :
			pool_info[pool].threads_high,
		    pool_info[pool].threads_increment);

		//
		// Only distribute policy information to real nodes
		//
		for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
			if (node_incn[node] == INCN_UNKNOWN) {
				//
				// No need to distribute policy to a dead node.
				//
				continue;
			}
			if (node == orb_conf::node_number()) {
				//
				// The local node does not need remote
				// server threads for invocations to itself.
				//
				continue;
			}
			bool	need_msg = !pool_info[pool].valuesp->fixed_size;

			lck.lock();
			if (pool_info[pool].needs_initial[node]) {
				//
				// This is the first time dealing with this
				// client node and resource_pool.
				//
				ASSERT(pool_info[pool].
				    threads_allocated[node] == 0);
				ASSERT(pool_info[pool].
				    threads_recalled[node] == 0);
				ASSERT(pool_info[pool].
				    threads_pending[node] == 0);

				pool_info[pool].needs_initial[node] = false;

				//
				// Regardless of whether the pool size is fixed,
				// a new node must receive a policy message.
				//
				need_msg = true;
			}
			lck.unlock();

			if (need_msg) {
				ID_node	dest_node(node, node_incn[node]);

				ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
				    ("distribute_policy (%d.%3d,%d)"
				    " lo %d mod %d hi %d inc %d\n",
				    dest_node.ndid,
				    (dest_node.incn % 1000),
				    pool,
				    pool_info[pool].threads_low,
				    pool_info[pool].threads_moderate,
				    pool_info[pool].threads_high,
				    pool_info[pool].threads_increment));

				// Tell the client about the policy
				resource_mgr::send_message(dest_node,
				    resource_mgr::RESOURCE_POLICY,
				    (void *)&policy,
				    (uint_t)(sizeof (resource_policy)));
			}
		}
	}
}

//
// get_threads_total - returns the total number of server threads
// for the specified resource_pool.
//
int
resource_balancer::get_threads_total(resource_defs::resource_pool_t pool)
{
	return (pool_info[pool].threads_total);
}

//
// get_threads_min - returns the minimum number of threads for the
// specified resource pool.
//
int
resource_balancer::get_threads_min(resource_defs::resource_pool_t pool)
{
	return (pool_info[pool].threads_minimum);
}

//
// set_threads_min - changes the minimum number of threads for the
// specified resource pool. The system will NOT always be able to
// create enough threads to meet the new value when the thread level
// is increased.
//
// Note that there is no guarantee that the system ever created the
// previous minimum number of threads.
//
void
resource_balancer::set_threads_min(resource_defs::resource_pool_t pool,
    int minimum)
{
	int	new_minimum;

	lck.lock();

	if (minimum == pool_info[pool].threads_minimum) {
		//
		// The value does not change. Nothing to do.
		//
		lck.unlock();
		return;
	}

	//
	// Do not allow the new value of threads_minimum to drop
	// below the increment level.
	//
	if (minimum < pool_info[pool].threads_increment) {
		new_minimum = pool_info[pool].threads_increment;
	} else {
		new_minimum = minimum;
	}

	pool_info[pool].threads_minimum = new_minimum;

	if (new_minimum == pool_info[pool].threads_total) {
		//
		// No change needed
		//
	} else 	if (new_minimum > pool_info[pool].threads_total) {
		//
		// Must attempt to create more threads.
		// The attempt to create more threads does not always work,
		// but the system will continue to work in any case.
		//
		int	new_threads = orb_msg::the().
		    change_num_threads(orb_msg::THREADPOOL_INVOS,
		    new_minimum - pool_info[pool].threads_total);
		ASSERT(new_threads >= 0);

		pool_info[pool].threads_unallocated += new_threads;
		pool_info[pool].threads_total += new_threads;

#ifdef DEBUG
		validate_number_threads();
#endif
	} else {
		//
		// Have more threads than the new minimum
		//
		int	change;
		int	above_min = pool_info[pool].threads_total - new_minimum;

		// Can only destroy unallocated threads
		if (above_min > pool_info[pool].threads_unallocated) {
			change = pool_info[pool].threads_unallocated;
		} else {
			change = above_min;
		}
		discard_surplus_threads(pool, change);
	}
	lck.unlock();
}

//
// set_policy - sets the flow control policy parameters for the
// specified resource_pool. The policy parameters are validated.
// A panic will occur if the policy parameters are invalid for any reason.
//
// Returns true when the operation succeeds
//
bool
resource_balancer::set_policy(resource_defs::resource_pool_t pool, int low,
    int moderate, int high, int increment, int num_nodes)
{
	lck.lock();
	if (!validate_policy(pool, num_nodes, low, moderate, high,
	    increment)) {
		lck.unlock();
		return (false);
	}

	//
	// This simple method does not adjust the number of existing threads.
	// Therefore the following constraint must hold.
	//
	if (high < pool_info[pool].threads_total) {
		//
		// SCMSGS
		// @explanation
		// An attempt was made to change the flow control policy
		// parameter specifying the high number of server threads for
		// a resource pool. The system does not allow the high number
		// to be reduced below current total number of server threads.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Cannot make high %d less than current total %d",
		    high, pool_info[pool].threads_total);
		lck.unlock();
		return (false);
	}
	//
	// The code is not yet safe for changing values of threads_increment.
	// XXX - plan to make it safe in the future.
	//
	if (increment != pool_info[pool].threads_increment) {
		//
		// SCMSGS
		// @explanation
		// An attempt was made to change the flow control policy
		// parameter that specifies the thread increment level. The
		// flow control system uses this parameter to set the number
		// of threads that are acted upon at one time. This value
		// currently cannot be changed.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Cannot change increment");
		lck.unlock();
		return (false);
	}

	pool_info[pool].threads_low		= low;
	pool_info[pool].threads_moderate	= moderate;
	pool_info[pool].threads_high		= high;
	pool_info[pool].threads_increment	= increment;

	//
	// The mechanism for ensuring that a fixed
	// size pool remains fixed, is to tell the
	// client that low, moderate, and high are
	// the same.
	//
	resource_policy		policy(pool,
	    pool_info[pool].threads_low,
	    pool_info[pool].threads_moderate,
	    pool_values[pool].fixed_size ?
		pool_info[pool].threads_low :
		pool_info[pool].threads_high,
	    pool_info[pool].threads_increment);

	//
	// Distribute the policy to each active client on a real node.
	//
	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		//
		// An active client always has at least one thread
		//
		if (pool_info[pool].threads_allocated[node] > 0) {
			ID_node		dest_node(node, node_incn[node]);

			ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
			    ("new policy (%d.%3d,%d)"
			    " lo %d mod %d hi %d inc %d\n",
			    dest_node.ndid, (dest_node.incn % 1000), pool,
			    policy.threads_low,
			    policy.threads_moderate,
			    policy.threads_high,
			    policy.threads_increment));

			lck.unlock();

			// Tell the client about the policy
			resource_mgr::send_message(dest_node,
			    resource_mgr::RESOURCE_POLICY,
			    (void *)&policy,
			    (uint_t)(sizeof (resource_policy)));

			lck.lock();
		}
	}
	lck.unlock();
	return (true);
}

//
// get_policy - returns the flow control policy parameters for the
// specified resource_pool.
//
void
resource_balancer::get_policy(resource_defs::resource_pool_t pool, int &low,
    int &moderate, int &high, int &increment)
{
	lck.lock();

	low		= pool_info[pool].threads_low;
	moderate	= pool_info[pool].threads_moderate;
	high		= pool_info[pool].threads_high;
	increment	= pool_info[pool].threads_increment;

	lck.unlock();
}

//
// validate_policy - The thread values must satisfy the following constraints
// in order for resource reallocation to work properly.
//
// Returns true when parameter relationships are acceptable.
//
bool
resource_balancer::validate_policy(resource_defs::resource_pool_t pool,
    int num_nodes, int low, int moderate, int high, int increment)
{
	if (moderate < low) {
		//
		// SCMSGS
		// @explanation
		// The system checks the proposed flow control policy
		// parameters at system startup and when processing a change
		// request. The moderate server thread level cannot be less
		// than the low server thread level.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: validate_policy: invalid relationship moderate %d "
		    "low %d pool %d", moderate, low, pool);
		return (false);
	}

	if (moderate > high) {
		//
		// SCMSGS
		// @explanation
		// The system checks the proposed flow control policy
		// parameters at system startup and when processing a change
		// request. The moderate server thread level cannot be higher
		// than the high server thread level.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: validate_policy: invalid relationship moderate %d "
		    "high %d pool %d", moderate, high, pool);
		return (false);
	}

	if (pool_values[pool].fixed_size && low != moderate) {
		//
		// SCMSGS
		// @explanation
		// The system checks the proposed flow control policy
		// parameters at system startup and when processing a change
		// request. The low and moderate server thread levels must be
		// the same for fixed size resource pools.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: validate_policy: fixed size pool low %d must match"
		    " moderate %d", low, moderate);
		return (false);
	}

	//
	// This formula checks to ensure that this server node has enough
	// server threads for the entire cluster.
	//
	if (pool_values[pool].fixed_size && high < num_nodes * low) {
		//
		// SCMSGS
		// @explanation
		// The system checks the proposed flow control policy
		// parameters at system startup and when processing a change
		// request. The high server thread level must be large enough
		// to grant the low number of threads to all of the nodes
		// identified in the message for a fixed size resource pool.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: validate_policy: high too small. high %d low %d "
		    "nodes %d pool %d", high, low, num_nodes, pool);
		return (false);
	}

	//
	// This formula checks to ensure that the low value for threads is
	// at least some minimum value consistent with policy algorithm.
	//
	if (!pool_values[pool].fixed_size &&
	    low < reallocation_inc_multiple * increment) {
		//
		// SCMSGS
		// @explanation
		// The system checks the proposed flow control policy
		// parameters at system startup and when processing a change
		// request. The low server thread level must not be less than
		// twice the thread increment level for resource pools whose
		// number threads varies dynamically.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: validate_policy: threads_low not big enough "
		    "low %d pool %d", low, pool);
		return (false);
	}

	//
	// This formula checks to ensure that this server node has enough
	// server threads for the entire cluster.
	//
	if (!pool_values[pool].fixed_size &&
	    high < ((num_nodes - 1) * (low - increment) + increment)) {
		//
		// SCMSGS
		// @explanation
		// The system checks the proposed flow control policy
		// parameters at system startup and when processing a change
		// request. For a variable size resource pool, the high server
		// thread level must be large enough to allow all of the nodes
		// identified in the message join the cluster and receive a
		// minimal number of server threads.
		// @user_action
		// No user action required.
		//
		(void) resource_balancer_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: validate_policy: high not enough. high %d low %d "
		    "inc %d nodes %d pool %d", high, low, increment,
		    num_nodes, pool);
		return (false);
	}
	return (true);
}

//
// number_nodes - returns the number of nodes flow control is
// currently supporting.
//
int
resource_balancer::number_nodes()
{
	int	number_of_nodes = 0;

	// Note that real nodes start with number 1
	for (int i = 1; i <= NODEID_MAX; i++) {
		if (node_incn[i] != INCN_UNKNOWN) {
			number_of_nodes++;
		}
	}
	return (number_of_nodes);
}

//
// send_grant - send a grant to client.
//
void
resource_balancer::send_grant(ID_node &dest_node,
    resource_defs::resource_pool_t pool, int change)
{
	ASSERT(lck.lock_held());
	ASSERT(change >= 0);

	ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
	    ("send_grant (%d.%3d,%d) t %d a %d\n",
	    dest_node.ndid, (dest_node.incn % 1000), pool, change,
	    pool_info[pool].threads_allocated[dest_node.ndid]));

	lck.unlock();

	orb_stats_mgr::the().inc_balancer_grants(dest_node.ndid);

	if (dest_node.ndid == 0) {
		//
		// The grant goes to the unref threadpool
		// It is safe to request a transfer of zero threads.
		//
		ASSERT(pool == resource_defs::DEFAULT_POOL);

		ORB_DBPRINTF(ORB_TRACE_FLOW_ALLOC,
		    ("transfer to unref_threadpool threads %d\n",
		    change));

		unref_threadpool::the().transfer_worker_threads(change,
		    &orb_msg::the().server_threads[orb_msg::THREADPOOL_INVOS]);
	} else {
		// The grant goes to a client node

		resource_grant	grant(pool, change);

		// Tell the client about the resources
		resource_mgr::send_message(dest_node,
		    resource_mgr::RESOURCE_GRANT,
		    (void *)&grant,
		    (uint_t)(sizeof (resource_grant)));
	}
}

//
// cl_read_flow_settings - supports the cladmin command to
// read the orb flow control settings.
//
extern "C" int
cl_read_flow_settings(void *argp)
{
	cl_flow_specs		flow_specs;
	resource_balancer	&balancer = resource_balancer::the();
	sc_syslog_msg_handle_t	handle;

	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != 0) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (copyin(argp, &flow_specs, sizeof (cl_flow_specs)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The system failed a copy operation supporting flow control
		// state reporting.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error in copyin for cl_read_flow_settings");
		sc_syslog_msg_done(&handle);
		return (EFAULT);
	}
	// Check for valid pool
	if (flow_specs.pool >= resource_defs::NUMBER_POOLS) {
		sc_syslog_msg_done(&handle);
		return (EINVAL);
	}

	// Determine how many nodes flow control believes are in the cluster
	flow_specs.number_nodes = balancer.number_nodes();

	balancer.get_policy(flow_specs.pool, flow_specs.threads_low,
	    flow_specs.threads_moderate, flow_specs.threads_high,
	    flow_specs.threads_increment);

	sc_syslog_msg_done(&handle);

	return (copyout(&flow_specs, argp, sizeof (cl_flow_specs)));
}

//
// cl_change_flow_settings - supports the cladmin command to
// change the settings used by orb flow control.
//
extern "C" int
cl_change_flow_settings(void *argp)
{
	cl_flow_specs		flow_specs;
	resource_balancer	&balancer = resource_balancer::the();
	sc_syslog_msg_handle_t	handle;

	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != SC_SYSLOG_MSG_STATUS_GOOD) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (copyin(argp, &flow_specs, sizeof (cl_flow_specs)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The system failed a copy operation supporting a flow
		// control state change.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error in copyin for cl_change_flow_settings");
		sc_syslog_msg_done(&handle);
		return (EFAULT);
	}
	// Check for valid pool
	if (flow_specs.pool >= resource_defs::NUMBER_POOLS) {
		sc_syslog_msg_done(&handle);
		return (EINVAL);
	}

	sc_syslog_msg_done(&handle);

	//
	// Tell flow control to use a new set of policy parameters
	//
	if (balancer.set_policy(flow_specs.pool, flow_specs.threads_low,
	    flow_specs.threads_moderate, flow_specs.threads_high,
	    flow_specs.threads_increment, flow_specs.number_nodes)) {
		// Operation succeeded
		return (0);
	} else {
		// Operation not allowed
		return (ECANCELED);
	}
}

//
// cl_read_threads_min - supports the cladmin command to
// read the orb minimum server threads setting.
//
extern "C" int
cl_read_threads_min(void *argp)
{
	cl_flow_threads_min	flow_threads_min;
	resource_balancer	&balancer = resource_balancer::the();
	sc_syslog_msg_handle_t	handle;

	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != 0) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (copyin(argp, &flow_threads_min, sizeof (cl_flow_threads_min)) !=
	    0) {
		//
		// SCMSGS
		// @explanation
		// The system failed a copy operation supporting flow control
		// state reporting.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error in copyin for cl_read_threads_min");
		sc_syslog_msg_done(&handle);
		return (EFAULT);
	}
	// Check for valid pool
	if (flow_threads_min.pool >= resource_defs::NUMBER_POOLS) {
		sc_syslog_msg_done(&handle);
		return (EINVAL);
	}

	flow_threads_min.threads_minimum =
	    balancer.get_threads_min(flow_threads_min.pool);

	sc_syslog_msg_done(&handle);

	return (copyout(&flow_threads_min, argp, sizeof (cl_flow_threads_min)));
}

//
// cl_change_threads_min - supports the cladmin command to
// change the orb minimum number of server threads.
//
extern "C" int
cl_change_threads_min(void *argp)
{
	cl_flow_threads_min	flow_threads_min;
	resource_balancer	&balancer = resource_balancer::the();
	sc_syslog_msg_handle_t	handle;

	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != SC_SYSLOG_MSG_STATUS_GOOD) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (copyin(argp, &flow_threads_min, sizeof (cl_flow_threads_min)) !=
	    0) {
		//
		// SCMSGS
		// @explanation
		// The system failed a copy operation supporting a flow
		// control state change.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error in copyin for cl_change_threads_min");
		sc_syslog_msg_done(&handle);
		return (EFAULT);
	}
	// Check for valid pool
	if (flow_threads_min.pool >= resource_defs::NUMBER_POOLS) {
		sc_syslog_msg_done(&handle);
		return (EINVAL);
	}

	sc_syslog_msg_done(&handle);

	//
	// Tell flow control to use new value for minimum number server threads
	//
	balancer.set_threads_min(flow_threads_min.pool,
	    flow_threads_min.threads_minimum);

	// Operation succeeded
	return (0);
}

//
// read_state - supports the cladm command to obtain the state for
// the specified pool.
//
int
resource_balancer::read_state(void *argp)
{
	state_balancer	state_bal;
	sc_syslog_msg_handle_t	handle;

	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != SC_SYSLOG_MSG_STATUS_GOOD) {
		return (SC_SYSLOG_MSG_STATUS_ERROR);
	}
	if (copyin(argp, &state_bal, sizeof (state_balancer)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The system failed a copy operation supporting statistics
		// reporting.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: error in copyin for state_balancer");
		return (EFAULT);
	}
	// Check for valid pool
	if (state_bal.pool < 0 ||
	    state_bal.pool >= resource_defs::NUMBER_POOLS) {
		return (EINVAL);
	}
	state_bal.threads_total = pool_info[state_bal.pool].threads_total;

	state_bal.threads_unallocated = pool_info[state_bal.pool].
	    threads_unallocated;

	return (copyout(&state_bal, argp, sizeof (state_balancer)));
}
