//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)flow_stats.cc	1.5	08/05/20 SMI"

#include <orb/flow/flow_stats.h>

//
// struct msg_stats methods
//

flow_stats::flow_stats()
{
	init();
}

void
flow_stats::init()
{
	blocked = 0;
	unblocked = 0;
	reject = 0;

	pool_requests = 0;
	pool_grants = 0;
	pool_denies = 0;
	pool_recalls = 0;
	pool_releases = 0;

	balancer_requests = 0;
	balancer_grants = 0;
	balancer_denies = 0;
	balancer_recalls = 0;
	balancer_releases = 0;

	for (int i = 0; i < resources::NUMBER_POLICIES; i++) {
		msg_by_policy[i] = 0;
	}
}
