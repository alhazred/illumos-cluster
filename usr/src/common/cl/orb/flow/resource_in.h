/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  resource_in.h
 *
 */

#ifndef _RESOURCE_IN_H
#define	_RESOURCE_IN_H

#pragma ident	"@(#)resource_in.h	1.26	08/05/20 SMI"

//
// Class resources_msg methods
//

inline
resources::resources_msg::resources_msg(uint_t hdr_size, uint_t dat_size) :
	header_size(hdr_size),
	data_size(dat_size),
	num_unknowns(0),
	num_objs(0)
{
}

inline
resources::resources_msg::resources_msg() :
	header_size(0),
	data_size(0),
	num_unknowns(0),
	num_objs(0)
{
}

inline
resources::resources_msg::~resources_msg()
{
}

//
// Class resources methods
//

//
// Accessor methods for adding information to the resources object.
//

// The following methods accumulate msg resource requirements

inline void
resources::add_send_header(uint_t hdr_size)
{
	send.header_size += hdr_size;
}

inline void
resources::add_send_data(uint_t dat_size)
{
	send.data_size += dat_size;
}

inline void
resources::add_send_unknown(uint_t num_unk)
{
	send.num_unknowns += num_unk;
}

inline void
resources::add_send_objs(uint_t num_obj)
{
	send.num_objs += num_obj;
}

inline void
resources::add_reply_header(uint_t hdr_size)
{
	reply.header_size += hdr_size;
}

inline void
resources::add_reply_data(uint_t dat_size)
{
	reply.data_size += dat_size;
}

inline void
resources::add_reply_unknown(uint_t num_unk)
{
	reply.num_unknowns += num_unk;
}

inline void
resources::add_reply_objs(uint_t num_obj)
{
	reply.num_objs += num_obj;
}

// In LP64 add convenience methods to take size_t as argument.
// They just call the above methods with cast to uint_t argument
#ifdef _LP64
inline void
resources::add_send_header(size_t hdr_size)
{
	ASSERT(hdr_size <= UINT_MAX);
	add_send_header((uint_t)hdr_size);
}

inline void
resources::add_send_data(size_t dat_size)
{
	ASSERT(dat_size <= UINT_MAX);
	add_send_data((uint_t)dat_size);
}

inline void
resources::add_reply_header(size_t hdr_size)
{
	ASSERT(hdr_size <= UINT_MAX);
	add_reply_header((uint_t)hdr_size);
}

inline void
resources::add_reply_data(size_t dat_size)
{
	ASSERT(dat_size <= UINT_MAX);
	add_reply_data((uint_t)dat_size);
}
#endif

//
// The following methods set various message attributes
//

inline void
resources::set_mode(invocation_mode inv_mode)
{
	invo_mode = inv_mode;
}

#ifdef _KERNEL_ORB
inline void
resources::set_node(ID_node &node)
{
	target_node = node;
}
#endif

inline void
resources::set_send_msgtype(orb_msgtype msgt)
{
	msgtype = msgt;
}

inline void
resources::set_env(Environment *e)
{
	envp = e;
}

//
// set_request_group - identifies the group to which a request belongs.
// Subsequent attempts to set this value are ignored.
//
inline void
resources::set_request_group(void *group)
{
	if (request_group == NULL) {
		request_group = group;
	}
}

inline void
resources::set_oneway()
{
	invo_mode.set_oneway();
}

inline void
resources::set_twoway()
{
	invo_mode.set_twoway();
}

//
// Accessor methods for getting information from the resources object.
//

// Return cumulative total size of all headers
inline uint_t
resources::send_header_size()
{
	return (send.header_size);
}

inline invocation_mode
resources::get_invo_mode()
{
	return (invo_mode);
}

inline resource_defs::resource_pool_t
resources::get_pool()
{
	return (pool);
}

#ifdef _KERNEL_ORB
inline ID_node *
resources::get_nodep()
{
	// Some transports (nil,sdoor) do not use this field.
	// So insist that the field has been initialized before use.
	ASSERT(target_node.ndid != NODEID_UNKNOWN);

	return (&target_node);
}

inline void
resources::set_nodeid(nodeid_t n)
{
	ASSERT(target_node.ndid == NODEID_UNKNOWN);
	target_node.ndid = n;
}

inline service_twoway *
resources::get_service_twoway()
{
	return (serv2);
}
#endif

inline orb_msgtype
resources::get_send_msgtype()
{
	return (msgtype);
}

inline resources::flow_policy_t
resources::get_flow_policy()
{
	return (policy);
}

inline Environment *
resources::get_env()
{
	return (envp);
}

inline void *
resources::get_request_group()
{
	return (request_group);
}

#ifdef _FAULT_INJECTION
inline void
resources::record_fault_injection()
{
	fi_processed = true;
}

inline bool
resources::show_fault_injection()
{
	return (fi_processed);
}
#endif	// _FAULT_INJECTION

//
// resources_datagram methods
//

#ifdef _KERNEL_ORB
inline
resources_datagram::resources_datagram(Environment *e, uint_t header_size,
    uint_t data_size, ID_node &node, orb_msgtype msgt) :
	resources(NO_FLOW_CONTROL, e, invocation_mode::ONEWAY, header_size,
	    data_size, node, NULL, msgt)
{
}
#else
inline
resources_datagram::resources_datagram(Environment *e, uint_t header_size,
    uint_t data_size, orb_msgtype msgt) :
	resources(NO_FLOW_CONTROL, e, invocation_mode::ONEWAY, header_size,
	    data_size, msgt)
{
}
#endif

#endif	/* _RESOURCE_IN_H */
