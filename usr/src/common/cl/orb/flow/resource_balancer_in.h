/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  resource_balancer_in.h
 *
 */

#ifndef _RESOURCE_BALANCER_IN_H
#define	_RESOURCE_BALANCER_IN_H

#pragma ident	"@(#)resource_balancer_in.h	1.6	08/05/20 SMI"

// static
inline resource_balancer &
resource_balancer::the()
{
	ASSERT(the_resource_balancer != NULL);
	return (*the_resource_balancer);
}

//
// threads_increment - return the thread change increment value for
// the specified resource_pool.
//
inline int
resource_balancer::threads_increment(resource_defs::resource_pool_t pool)
{
	return (pool_info[pool].threads_increment);
}

#endif	/* _RESOURCE_BALANCER_IN_H */
