/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RESOURCE_DEFS_H
#define	_RESOURCE_DEFS_H

#pragma ident	"@(#)resource_defs.h	1.15	08/05/20 SMI"

#include <cmm/cmm_version.h>

/*
 * Description of ORB message types
 *
 * We define the enum here outside of any class so that C files
 * can pick up the same definitions.
 *
 * Note: When changing this list be aware that rsm and tcp transports
 * have a table that must match the declaration order shown here.
 * Search for array declarations containing N_MSGTYPES.
 *
 * A message type enum value cannot be retired in any release
 * that allows a rolling upgrade to that release, because
 * that would change values of the remaining message types.
 *
 */
typedef enum { INVO_MSG,	/* Request part of 2-way invocation */
	REPLY_MSG,		/* Reply part of a 2-way invocation */
	REFJOB_MSG,		/* ORB Reference Counting messages */
#ifdef	CMM_VERSION_0
	CMM_MSG,		/* CMM messages */
#endif // CMM_VERSION_0
#ifdef DEBUG
	STOP_MSG,		/* Cluster stop message */
#endif // DEBUG
	SIGNAL_MSG,		/* Signal related messages */
	CKPT_MSG,		/* Checkpoint messages */
	FLOW_MSG,		/* Flow control messages */
	INVO_ONEWAY_MSG,	/* Request part of 1-way invocation */
	COMPLETED_MAYBE_RESOLVE_MSG,	/* Resolve COMPLETED_MAYBE */
	FAULT_MSG,		/* Fault injection framework messages */
	VM_MSG,			/* Bootstrap VM messages */
	CMM1_MSG,		/* CMM version 1 message */
	N_MSGTYPES
} orb_msgtype;

#ifdef __cplusplus

//
// Definitions related to orb resources.
// This class acts as a name space. The principal purpose is
// to avoid conditional compilation for kernel vs user level code.
// Secondly, it avoids circular dependencies in the header files.
//
class resource_defs {
public:
	//
	// Identify the resource pool for this remote request.
	//
	enum resource_pool_t {	DEFAULT_POOL,	// Unless specified, use this
#ifdef RECONFIG_POOL_ACTIVE
				RECONFIG_POOL,	// Node reconfiguration
#endif
				NUMBER_POOLS };
};

#endif	// __cplusplus

#endif	/* _RESOURCE_DEFS_H */
