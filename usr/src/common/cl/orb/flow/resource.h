/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RESOURCE_H
#define	_RESOURCE_H

#pragma ident	"@(#)resource.h	1.35	08/05/20 SMI"

//
// This defines the descriptor that identifies the resources used
// by one remote request. Each module which processes a resource descriptor
// adds information about the resources required by that module to
// process the message. The resource descriptor also includes information
// about the nature of the message, including such information as oneway.
// After all of the other modules have filled in their resource
// requirements, the flow control module uses the resource descriptor
// to manage resource usage and determine whether the system should
// reject, block, or process the remote request. A remote request can
// be oneway or rpc.
//

#include <orb/invo/invo_args.h>
#include <orb/invo/invocation_mode.h>
#include <orb/flow/resource_defs.h>
#include <sys/os.h>
#include <limits.h>
#include <sys/list_def.h>
#ifdef _KERNEL_ORB
#include <orb/member/Node.h>
#endif

// forward declarations
#ifdef _KERNEL_ORB
class service_twoway;
#endif

union ArgValue;

//
// resources - specify the resources needed for this remote request.
//
class resources : public _DList::ListElem {
	friend class resource_pool;
public:
	// Identify the flow control policy types
	enum flow_policy_t {	NO_FLOW_CONTROL,	// Always permit msg.
				BASIC_FLOW_CONTROL,	// Flow control active.
				REPLY_FLOW,		// Done at request time
				NUMBER_POLICIES};	//

	// Define constant to use as minimum size for XdoorTab.
	// This is needed because the size information is not available
	// at the place where the reply sendstream is created.
	enum { XDOORCHUNK = 64 };

	resources(flow_policy_t pol, Environment *e, invocation_mode inv_mode,
	    orb_msgtype msgt);

#ifdef _KERNEL_ORB
	resources(flow_policy_t pol, Environment *e, invocation_mode inv_mode,
	    uint_t header_size, uint_t data_size, ID_node &node,
	    service_twoway *, orb_msgtype msgt);
#else // _KERNEL_ORB
	resources(flow_policy_t pol, Environment *e, invocation_mode inv_mode,
	    uint_t header_size, uint_t data_size, orb_msgtype msgt);
#endif // _KERNEL_ORB

	virtual ~resources();

	//
	// General purpose methods
	//

	//
	// Decide whether request can proceed at this time.
	// Errors are returned as exceptions in the environment.
	//
	void	flow_control();

	// Release any resources held by this message request
	void	resource_release();

	// Calculate send main buffer size
	uint_t	send_buffer_size();

	// Calculate size of xdoor buffer size
	uint_t	send_xdoortab_size();

	// Add the size information for a remote invocation
	void	add_argsizes(arg_info &arginfo, ArgValue *argvaluep);

	//
	// Accessor methods for adding information to the resources object.
	//

	// The following methods accumulate msg resource requirements
	void	add_send_header(uint_t hdr_size);
	void	add_send_data(uint_t dat_size);
	void	add_send_unknown(uint_t num_unk);
	void	add_send_objs(uint_t num_obj);

	void	add_reply_header(uint_t hdr_size);
	void	add_reply_data(uint_t dat_size);
	void	add_reply_unknown(uint_t num_unk);
	void	add_reply_objs(uint_t num_obj);

#ifdef _LP64
	void	add_send_header(size_t hdr_size);
	void	add_send_data(size_t dat_size);
	void	add_reply_header(size_t hdr_size);
	void	add_reply_data(size_t dat_size);
#endif

	//
	// The following methods set various message attributes
	//
	void	set_mode(invocation_mode inv_mode);
	void	set_flow_policy(flow_policy_t policy);
#ifdef _KERNEL_ORB
	void	set_node(ID_node &node);
#endif
	void	set_send_msgtype(orb_msgtype msgt);
	void	set_env(Environment *e);
	void	set_request_group(void *group);
	void	set_oneway();
	void	set_twoway();

	//
	// Accessor methods for getting information from the resources object.
	//

	// Return cumulative total size of all headers
	uint_t				send_header_size();

	flow_policy_t			get_flow_policy();
	Environment			*get_env();
	invocation_mode			get_invo_mode();
	resource_defs::resource_pool_t	get_pool();
#ifdef _KERNEL_ORB
	ID_node				*get_nodep();
	void				set_nodeid(nodeid_t);
	service_twoway			*get_service_twoway();
#endif
	orb_msgtype			get_send_msgtype();
	void				*get_request_group();

#ifdef _FAULT_INJECTION
	void				record_fault_injection();
	bool				show_fault_injection();
#endif	// _FAULT_INJECTION

private:
	void	marshal_time_size_determination(arg_info &arginfo,
		    ArgValue *argvaluep, invo_args::arg_mode test_mode,
		    arg_sizes *argsizesp);

	// Define a constant to use for guessing the size of an unknown
	// sized arg
	enum { SIZE_FOR_UNKNOWN = 40 };

	// Define a constant to use for guessing the size of an objects
	// data in the main buffer (type id (1W) + service(1w))
	enum { SIZE_FOR_OBJECT = 20 };

	// Define a constant to use for padding when unknowns exist.
	enum { PAD_SIZE = 1024 };

	enum resources_state_t { NOTHING_RESERVED, // Initial state
				BLOCKED,	// Needs known, but unavailable
				ALLOCATED };	// Resources allocated

	//
	// resources_msg - specify resources for this message.
	//
	class resources_msg {
	public:
		resources_msg(uint_t hdr_size, uint_t dat_size);
		resources_msg();

		~resources_msg();

		// The entries are public because the class is only visible
		// within the class resources
		//
		// These fields record the cumulative requirements for
		// all processing steps
		uint_t	header_size;	// message header size
		uint_t	data_size;	// ordinary marshalled data
		uint_t	num_unknowns;	// number unknown size items
		uint_t	num_objs;	// number object references
	};

	// The initial flow control subsystem manages server thread
	// usage from the client node. Currently messages subject to
	// flow control require exactly one server thread.
	// Reply messages are controlled by means of controlling the requests.
	// Thus the existance of the message resource descriptor
	// implicitly implies the need for one server thread.
	// XXX - should messages serviced by the interrupt thread need
	// XXX - active flow control then this point must be re-examined.

	flow_policy_t			policy;
	Environment			*envp;
	invocation_mode			invo_mode;
	resource_defs::resource_pool_t	pool;
	resources_msg			send;	// outbound msg requirements
	resources_msg			reply;	// reply msg requirements

#ifdef _KERNEL_ORB
	// Local transports do not communicate
	// with remote nodes. So could be empty.
	//
	ID_node				target_node;

	// In case of a two way service reply, the endpoint registry needs
	// to access during a call to reserve_resources the service object
	// for certain transport information that is stored with the service
	// object
	service_twoway			*serv2;

#endif

	// Send msg is of this type.
	// No need to know type for reply msg.
	//
	orb_msgtype			msgtype;

	// Unique value representing a class of requests.
	// This value is used in identifying requests for cancellation.
	// Recommend the address of an object used by the service provider.
	// Two examples are provided:
	// Node - unique address provided by members class object for a node.
	// HA Service - unique address of the object providing the service.
	//
	void				*request_group;

	// Contains the current state of the resources object.
	resources_state_t		state;

#ifdef _FAULT_INJECTION
	// True when Fault Injection requirements have been processed
	bool				fi_processed;
#endif	// _FAULT_INJECTION

	// Flow control uses this to signal the request that it can proceed.
	os::condvar_t			cv;
};

//
// resources_datagram - identifies resources for a kernel datagram message.
// This class enables some shortcuts in resource specification for
// messages that are always: oneway and not flow controlled
//
class resources_datagram : public resources {
public:
#ifdef _KERNEL_ORB
	resources_datagram(Environment *e, uint_t header_size,
	    uint_t data_size, ID_node &node, orb_msgtype msgt);
#else // _KERNEL_ORB
	resources_datagram(Environment *e, uint_t header_size,
	    uint_t data_size, orb_msgtype msgt);
#endif // _KERNEL_ORB

	virtual				~resources_datagram();
};

#include <orb/flow/resource_in.h>

#endif	/* _RESOURCE_H */
