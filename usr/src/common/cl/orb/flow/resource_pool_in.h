/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  resource_pool_in.h
 *
 */

#ifndef _RESOURCE_POOL_IN_H
#define	_RESOURCE_POOL_IN_H

#pragma ident	"@(#)resource_pool_in.h	1.18	08/05/20 SMI"

//
// Class resource_pool inline methods
//

inline resource_pool *
resource_pool::select_resource_pool(nodeid_t node,
    resource_defs::resource_pool_t pool)
{
	return (&resource_pools[node][pool]);
}
//
// resources_available - returns true when resources are available
//
inline bool
resource_pool::resources_available(resources *)
{
	// Each message needs:
	//	1 server thread
	// XXX - eventually multiple resources will be checked
	//
	return (thread_free != 0);
}

//
// min_resources_unavailable - returns true if there are not enough
// resources to support another request at this time.
//
inline bool
resource_pool::min_resources_unavailable()
{
	// Each message needs at least:
	//	1 server thread
	// XXX - eventually multiple resources will be checked
	//
	return (thread_free == 0);
}

//
// resources_allocate - resources in this resource pool are allocated to
// support one message.
//
inline void
resource_pool::resources_allocate(resources *)
{
	// Each message needs:
	//	1 server thread
	// XXX - eventually multiple resources will be supported
	//
	ASSERT(thread_free != 0);
	thread_free--;

	// Adjust the minimum free thread count this period
	if (thread_free_min > thread_free) {
		thread_free_min = thread_free;
	}
}

#endif	/* _RESOURCE_POOL_IN_H */
