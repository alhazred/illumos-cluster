/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  resource_blocker_in.h
 *
 */

#ifndef _RESOURCE_BLOCKER_IN_H
#define	_RESOURCE_BLOCKER_IN_H

#pragma ident	"@(#)resource_blocker_in.h	1.11	08/05/20 SMI"

//
// Class disable_request inline methods
//
inline
disable_request::disable_request(void *group,
    CORBA::SystemException &exception) :
	_DList::ListElem(this),
	request_group(group),
	disable_exception(exception._major(), exception._minor(),
	    exception.completed())
{
}

//
//  Class resource_blocker inline methods
//

inline
resource_blocker::resource_blocker() :
	number_disables(0)
{
}


inline resource_blocker &
resource_blocker::the()
{
	return (*the_resource_blocker);
}

//
// hashslot - hashes the request_group to produce a bucket index.
// Even though the request_group is a byte address, it is expected that
// normally it will be a word or larger address. Therefore this hash
// algorithm drops the low bits.
//
inline int
resource_blocker::hashslot(void *request_group)
{
	return ((int)((uintptr_t)request_group >> 3) &
	    (DISABLE_TABLE_SIZE - 1));
}

#endif	/* _RESOURCE_BLOCKER_IN_H */
