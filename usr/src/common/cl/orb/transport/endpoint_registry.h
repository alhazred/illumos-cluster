/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ENDPOINT_REGISTRY_H
#define	_ENDPOINT_REGISTRY_H

#pragma ident	"@(#)endpoint_registry.h	1.23	08/05/20 SMI"

#include <sys/os.h>
#include <sys/refcnt.h>
#include <sys/list_def.h>
#include <orb/member/cmm_callback_impl.h>
#include <orb/buffers/marshalstream.h>
#include <orb/flow/resource.h>
#include <orb/transport/pm_client.h>
#include <orb/debug/haci_debug.h>

#if defined(HACI_DBGBUFS) && !defined(NO_ER_DBGBUF)
#define	ER_DBGBUF
#endif

#ifdef	ER_DBGBUF
extern dbg_print_buf er_dbg;
#define	ER_DBG(a) HACI_DEBUG(ENABLE_ER_DBG, er_dbg, a)
#else
#define	ER_DBG(a)
#endif

#if defined(_FAULT_INJECTION)
extern "C" uint_t num_endpoints(nodeid_t);
#endif

extern "C" sendstream *endpoint_reserve_resources(resources *);

// the endpoint registry interface, which all endpoints (including
// the nil_endpoint used for local communication) inherit from.

class er_if : public refcnt, public _SList::ListElem {
public:
	// The constructor, which takes an argument for the initial reference
	// count
	er_if(uint_t);

	//
	// The registration callback method is used by the endpoint registry
	// to notify the endpoint object that it has been successfully
	// registered with the registry. This callback is made while still
	// holding the endpoint registry locks, so that the endpoint object
	// can do its post registration processing atomically with the
	// endpoint registration operation.
	//
	virtual void registration_callback() = 0;

	// add_resources is used before get_sendstream to insert any
	// resources the transport uses into the resources structure
	virtual void add_resources(resources *) = 0;

	// get_sendstream should return the sendstream used for marshalling
	// the data and xdoors.
	virtual sendstream *get_sendstream(resources *) = 0;
};

class endpoint_registry : public refcnt {
public:
	// initialize allocates the registry and stores its location
	static int			initialize();
	static endpoint_registry &	the();

	// Called from the cmm_callback_impl when the node is being shutdown
	static void			shutdown();

	// reserve_resources is the entry point for retrieving a sendstream.
	// the nodeid will be looked up in the resources structure
	sendstream			*reserve_resources(resources *);
	// Used from step 1 of the cmm reconfiguration to update the
	// membership information
	void				set_membership(callback_membership *);
	// called from the generic transport (and the nil_endpoint code) to
	// add and remove endpoints from the registry.
	bool				register_endpoint(er_if *, nodeid_t,
					    incarnation_num);
	void				unregister_endpoint(er_if *, nodeid_t);
	// Called from the path_manager to deliver node_alive on new
	// registrants
	void				add_client(pm_client *);

#if defined(_FAULT_INJECTION)
	// Returns number of endpoints to a particular node.  The number
	// could have changed before the function returns, however.
	uint_t				num_endpoints(nodeid_t);
#endif

private:
	// initialize() is a member method, so the constructor can be private
	endpoint_registry();
	// likewise shutdown() and the destructor
	~endpoint_registry();

	// The location where the pointer to the registry is stored, which
	// is used in the().
	static endpoint_registry 		*the_endpoint_registry;

	struct ninfo_t {
		IntrList<er_if, _SList>			the_eps;
		IntrList<er_if, _SList>::ListIterator	iter;
		os::mutex_t				nlock;
		os::condvar_t				fail_cv;
	} ninfo[NODEID_MAX+1];

	callback_membership 			*cm;
	cmm::membership_t			regmbrs;
};

#include <orb/transport/endpoint_registry_in.h>

#endif	/* _ENDPOINT_REGISTRY_H */
