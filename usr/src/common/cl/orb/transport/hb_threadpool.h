/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HB_THREADPOOL_H
#define	_HB_THREADPOOL_H

#pragma ident	"@(#)hb_threadpool.h	1.9	08/05/20 SMI"

#include <sys/list_def.h>
#include <sys/dbg_printf.h>
#include <orb/debug/haci_debug.h>

#if defined(HACI_DBGBUFS) && !defined(NO_HB_DBGBUF)
#define	HBT_DBGBUF
#endif

#ifdef	HBT_DBGBUF
extern dbg_print_buf hbt_dbg;
#define	HBT_DBG(a) HACI_DEBUG(ENABLE_HB_DBG, hbt_dbg, a)
#else
#define	HBT_DBG(a)
#endif

class pm_send_defer_task;

// This new threadpool is used only for heartbeat processing.
// This class definition is taken from threadpool.h.
// This hb_threadpool is very much similar to threadpool class but
// a much shorter and simpler than the general threadpool. It has a very
// small subset of the data and methods from the original threadpool class.
// Two of the methods which are common have slightly different implementation.
// Look at threadpool.h for details about the data or methods.

class hb_threadpool {
public:
	static void 	thread_handler(hb_threadpool *);
	bool 		defer_processing(pm_send_defer_task *);
	void 		send_heartbeat();

	static int 	initialize();
	void 	shutdown();
	static hb_threadpool &the();
	bool		lock_held();
#ifdef DEBUGGER_PRINT
	void debugger_print();
#endif

protected:
	void 		lock();
	void 		unlock();

	typedef IntrList<pm_send_defer_task, _SList> task_list_t;
	task_list_t	pending;	// Pending task list.

private:
	hb_threadpool(int, char *);
	~hb_threadpool();
	void		deferred_task_handler();

	bool		time_to_shutdown;
	int		nthreads;	// no. of threads in hb_threadpool
	os::condvar_t	threadcv;
	os::mutex_t	threadlock;
	static hb_threadpool *the_heartbeat_threadpool;

	// Statistics
	uint64_t	int_hb;		// no. of hbs sent by interrupts.
	uint64_t	rt_hb;		// no. of hbs sent by RT threads.
	uint_t		count;		// no. of pending tasks.
	// rt_pend tells us if there are any tasks taken out from the
	// list by a RT thread but did not get chance to execute(ex.pinned
	// down interrupt).
	uint_t		rt_pend;
};

#ifndef NOINLINES
#include <orb/transport/hb_threadpool_in.h>
#endif

#endif	/* _HB_THREADPOOL_H */
