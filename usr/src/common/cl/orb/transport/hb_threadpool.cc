//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hb_threadpool.cc	1.17	08/05/20 SMI"

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/clusterproc.h>
#include <orb/transport/transport.h>
#include <orb/transport/hb_threadpool.h>
#include <cmm/ff_impl.h>
#ifdef _KERNEL
#include <sys/disp.h>
#include <sys/cpuvar.h>
#endif

// in orb/transport/path_manager.cc
extern void
	(*path_manager_cyclic_interface_funcp)(path_manager::cyclic_caller_t);

hb_threadpool *hb_threadpool::the_heartbeat_threadpool = NULL;

#ifdef	HBT_DBGBUF
dbg_print_buf hbt_dbg(8192);
#endif

#ifdef _KERNEL
//
// hb_threadpool_send_heartbeat_wrapper is a fuction that the hb_threadpool
// provides the clhbsndr streams module for calling into the hb_threadpool
// code for sending out heartbeats from its rput routine.
//
// hb_threadpool calls clhbsndr_set_hbfunc to set a function pointer in the
// clhbsndr module point to the hb_threadpool_send_heartbeat_wrapper
// routine. The clhbsndr_set_hbfunc is implemented by the clhbsndr module.
//
// This contrived approach has been taken to prevent clhbsndr module from
// having any dependency on the cl_comm module, since the clhbsndr gets pushed
// on some public network adapters even before cl_comm is loaded. Besides
// the clhbsndr module should also be capable of getting loaded in the non
// cluster mode since it is added to the /etc/iu.ap file.
//

extern "C" {
void clhbsndr_set_hbfunc(void (*)(void));
void hb_threadpool_send_heartbeat_wrapper(void);
}

void
hb_threadpool_send_heartbeat_wrapper()
{
	hb_threadpool::the().send_heartbeat();
}
#endif

//
// Constructor for hb_threadpool.
// This creates "ncpus" number of real-time threads for heartbeat processing.
// Also initializes different statistics.
//
hb_threadpool::hb_threadpool(int pri, char *cname) : time_to_shutdown(false)
{
	int error;

#ifdef _KERNEL
	nthreads = ncpus;
#else
	nthreads = 1;
#endif
	for (int i = 0; i < nthreads; i++) {
		error = clnewlwp((void (*)(void *))thread_handler, this, pri,
			cname, NULL);
		if (error != 0)
			//
			// SCMSGS
			// @explanation
			// The system was unable to create thread used for
			// heartbeat processing.
			// @user_action
			// Take steps to increase memory availability. The
			// installation of more memory will avoid the problem
			// with a kernel inability to create threads. For a
			// user level process problem: install more memory,
			// increase swap space, or reduce the peak work load.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "thread create for hb_threadpool failed");
		else
			HBT_DBG(("created thread %d\n", i));
	}
	int_hb = 0;
	rt_hb = 0;
	count = 0;
	rt_pend = 0;
}

//
// This initialize routine is called from path_manager's initialize
// routine. This creates the golbal hb_threadpool object, which is
// used by the transports later.
//
int
hb_threadpool::initialize()
{
	int pri, error;
	id_t cid;
	char *cname;

	ASSERT(the_heartbeat_threadpool == NULL);
	pri = orb_conf::rt_maxpri();
	cname = (char *)orb_conf::rt_classname();
	error = getcid(cname, &cid);
	if (error != 0) {
		//
		// SCMSGS
		// @explanation
		// An attempt to change the thread scheduling class failed,
		// because the scheduling class was not configured.
		// @user_action
		// Configure the system to support the desired thread
		// scheduling class.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "Scheduling class %s not configured", cname);
		return (error);
	}

	the_heartbeat_threadpool = new hb_threadpool(pri, cname);
	if (the_heartbeat_threadpool == NULL)
		return (ENOMEM);
#ifdef _KERNEL
	clhbsndr_set_hbfunc(hb_threadpool_send_heartbeat_wrapper);
#endif
	return (0);
}

hb_threadpool::~hb_threadpool()
{
	shutdown();
}

//
// Set the shutdown flag to true so that no more tasks are added by
// defer_processing. Also when this flag is set to true, the real-time
// threads exit.
//
void
hb_threadpool::shutdown()
{
	defer_task *ta;

	lock();
	time_to_shutdown = true;
	// wake up any sleeping threads. If a thread is not sleeping it
	// would exit when it is done with executing the current task.
	threadcv.broadcast();
	// Wait till all threads are exited.
	while (nthreads != 0) {
		threadcv.wait(&threadlock);
	}
	// Take out any pending tasks in the list
	while ((ta = pending.reapfirst()) != NULL) {
		ta->task_done();
	}
	unlock();
}

//
// This is the handler passed to the real-time threads when created.
//
void
hb_threadpool::thread_handler(hb_threadpool *hb_thr)
{
	hb_thr->deferred_task_handler();
}

//
// This method is called by the clock thread to schedule a real-time thread
// to send a heartbeat.
// This routine appends a pm_send_defer_task to the pending list of
// tasks and sets a flag in pm_send_defer_task object. It then signals
// the waiting threads to execute the task.
// If a task is already deferred then it just returns without adding another
// task.
//
bool
hb_threadpool::defer_processing(pm_send_defer_task *pmsdta)
{
	bool deferred = false;

	lock();

	// Do not schedule any more heartbeats if we are shutting down
	if (time_to_shutdown) {
		unlock();
		return (deferred);
	}

	if (! pmsdta->is_deferred()) {
		HBT_DBG(("deferred a heartbeat at %llx\n", gethrtime()));
		deferred = true;
		pending.append(pmsdta);
		count++;
		pmsdta->set_deferred(true);
		threadcv.signal();
	}
	unlock();

	return (deferred);
}

//
// This method is the handler for the real-time threads.
// Each real-time threads executes this handler until the shutdown.
// The thread waits on a condition variable until it is signalled.
// The thread gets signal when a task is added (see defer_processing
// method). Once it is waken up, it sets the deferred flag to false
// and then does execute().
// It is important that set_deferred(false) is done before the lock
// is released otherwise an interrupt can occur pinning down the
// real-time thread and can pin it down for a long time causing the
// task execution delayed long enough to cause a path timeout.
//
void
hb_threadpool::deferred_task_handler()
{
	pm_send_defer_task *pmsdta;

	lock();

	// while it is not time to shutdown either wait for a task or
	// if there are tasks, execute them
	while (!time_to_shutdown) {
		if ((pmsdta = pending.reapfirst()) == NULL) {
			threadcv.wait(&threadlock);
			continue;
		}
		if (!pending.empty()) {
			threadcv.signal();
		}
		count--;
		rt_pend++;
		pmsdta->set_deferred(false);
		rt_hb++;
		unlock();
		pmsdta->execute();
		HBT_DBG(("RT thread done execute at %llx\n", gethrtime()));
		lock();
		rt_pend--;
	}
	nthreads--;
	threadcv.broadcast();
	unlock();
	// thread exits
}

//
// This method is called only in the network interrupt context from
// the clhbsndr stream module's rput routine. Processing includes:
//	* cluster per tick processing  catchup
//	* heartbeat scheduling catchup
//	* send heartbeats
//
void
hb_threadpool::send_heartbeat()
{
	pm_send_defer_task *pmsdta;
	void (*funcp)(path_manager::cyclic_caller_t);

	// If the clock cyclic has been delayed beyond the set threshold,
	// the needed per tick processing will be done.
	ff_admin_impl::the().sc_per_tick_processing(ff_admin_impl::INTRPT_THRD);

	// If the PM cyclic has been delayed beyond the set
	// threshold, the needed heartbeat scheduling will be done.
	if ((funcp = path_manager_cyclic_interface_funcp) != NULL)
		(*funcp)(path_manager::NET_INTRPT_THRD);

	// Post all pending heartbeats to the network driver - even if the
	// heartbeat cyclic had *not* been delayed, and no additional
	// heartbeat queueing occurred as a result of the above.
	// This is done because the real time threads which process the
	// heartbeat queues may have been prevented from running by
	// a very heavy interrupt load.  So we service the heartbeat
	// queues and post the heartbeats to the network driver now at interrupt
	// level. Check to see if there are any tasks pending without acquiring
	// lock (to minimize lock contention) and then check
	// again after acquiring lock if the task list is non-empty.
	//
	if (!pending.empty()) {
		lock();
		HBT_DBG(("In send_heartbeat at %llx count=%d\n", gethrtime(),
		    count));
		while ((pmsdta = pending.reapfirst()) != NULL) {
			pmsdta->set_deferred(false);
			int_hb++;
			count--;
			unlock();
			pmsdta->execute();
			lock();
		}
		unlock();
	}
}
