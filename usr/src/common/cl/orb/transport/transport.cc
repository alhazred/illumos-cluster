//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)transport.cc	1.101	08/05/20 SMI"

#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb.h>
#include <orb/transport/transport.h>
#include <sys/clconf_int.h>
#include <orb/query/pathend_state_proxy.h>
#if defined(FAULT_TRANSPORT)
#include <orb/transport/endpoint_registry.h>
#endif

#ifdef	TRANSPORT_DBGBUF
uint32_t trans_dbg_size = 10000;
dbg_print_buf trans_dbg(trans_dbg_size);
#endif

#ifdef	PERPE_PMHB_DBGBUF
uint32_t perpe_hb_dbg_size = 8192;
#endif

// The time, in microseconds, to sleep between trying to initiate the pathend
// when the previous connection was rejected by the path_manager.  This is
// to prevent thrashing on forming a connection with a node that has been
// declared unreachable or dead but is still hanging around.  There is no
// obvious heuristic here.
#define	PE_THRASH_SLEEP		1000000

transport::transport(const char *name, bool ds) :
	defer_send(ds)
{
	// Initialize the incarnation number for all the endpoint
	// lists
	nodeid_t nid;
	for (nid = 1; nid <= NODEID_MAX; nid++)
		the_endpoints_incn[nid] = 0;

	TRANS_DBG(("T(%p) created (%s)\n", this, name));
}

transport::~transport()
{
	// Verify that all adapters have been removed
	adapter_lock.lock();
	ASSERT(the_adapters.empty());
	adapter_lock.unlock();

	TRANS_DBG(("T(%p) deleted\n", this));
}


bool
transport::add_adapter(clconf_adapter_t *ap)
{
	if (os::strcmp(transport_id(), clconf_adapter_get_transport_type(ap))) {
		return (false);
	}
	adapter *up = new_adapter(ap);
	ASSERT(get_adapter(up->get_adapter_id()) == NULL);
	adapter_lock.lock();
	the_adapters.append(up);
	adapter_lock.unlock();
	// Always return true, deal with bad adapter in remove_adapter
	return (true);
}

bool
transport::remove_adapter(clconf_adapter_t *ap)
{
	if (os::strcmp(transport_id(), clconf_adapter_get_transport_type(ap))) {
		return (false);
	}

	adapter *up;
	int id = clconf_obj_get_id((clconf_obj_t *)ap);
	adapter_lock.lock();
	IntrList<adapter, _SList>::ListIterator iter(the_adapters);
	while ((up = iter.get_current()) != NULL) {
		if (up->get_adapter_id() == id) {
			ASSERT(the_adapters.exists(up));
			(void) the_adapters.erase(up);
			break;
		}
		iter.advance();
	}
	adapter_lock.unlock();
	ASSERT(up != NULL);
	up->set_state(adapter::A_DELETED);
	delete up;
	return (true);
}

adapter *
transport::get_adapter(int adp_id)
{
	adapter *up;
	adapter_lock.lock();
	IntrList<adapter, _SList>::ListIterator iter(the_adapters);
	while ((up = iter.get_current()) != NULL) {
		if (up->get_adapter_id() == adp_id) {
		    break;
		}
		iter.advance();
	}
	adapter_lock.unlock();
	TRANS_DBG(("T(%p) A(%p) %d retrieved\n", this, up, adp_id));
	return (up);
}

//
// transport::get_pathend()
//
// This call returns pointer to a pathend object that matches the passed
// node id and adapter id. The pathend object can be retrieved either
// using the local adapter id or the remote adapter id. The second
// parameter indicates whether the adapter is passed is local. A
// get_pathend_hold is also done on the pathend before return. Caller is
// responsible for get_pathend_rele.
//
pathend *
transport::get_pathend(int adp_id, bool local_adp, nodeid_t dest)
{
	pathend *pe;
	int adp_id_to_match;
	ASSERT(dest <= NODEID_MAX);
	lock_node(dest);
	IntrList<pathend, _SList>::ListIterator iter(the_pathends[dest]);
	while ((pe = iter.get_current()) != NULL) {
		adp_id_to_match = local_adp ? pe->local_adapter_id() :
		    pe->remote_adapter_id();
		if (adp_id_to_match == adp_id) {
			// Caller is responsible for doing a get_pathend_rele()
			pe->get_pathend_hold();
			break;
		}
		iter.advance();
	}
	unlock_node(dest);
	TRANS_DBG(("T(%p) P(%p) (A_id(%s) = %d, nid = %d) retrieved\n", this,
	    pe, local_adp ? "local" : "remote", adp_id, dest));
	return (pe);
}

bool
transport::add_path(clconf_path_t *clconf_pathp)
{
	pathend *	pathendp;
	nodeid_t	nid = clconf_path_get_remote_nodeid(clconf_pathp);

	if (!this_transport(clconf_pathp, false))
		return (false);

	TRANS_DBG(("T(%p) add_path(%p) to node %d\n", this, clconf_pathp, nid));
	// ASSERT that the pathend is not in the list.

	pathendp = new_pathend(clconf_pathp);
	lock_node(nid);
	the_pathends[nid].prepend(pathendp);
	unlock_node(nid);

	pathendp->startup();

	return (true);
}

void
transport::add_endpoint(endpoint *epp, nodeid_t nid)
{
	TRANS_DBG(("T(%p) add E(%p) to node %d\n", this, epp, nid));
	ASSERT(node_locks[nid].lock_held());
	ASSERT(epp != NULL);
	ASSERT(!the_endpoints[nid].exists(epp->get_translist_elem()));
	the_endpoints[nid].prepend(epp->get_translist_elem());
	// Increment the list incarnation number to indicate a change
	// in the list.
	the_endpoints_incn[nid]++;
	epp->startup();
}

void
transport::remove_endpoint(endpoint *epp, nodeid_t nid)
{
	TRANS_DBG(("T(%p) remove E(%p)\n", this, epp));
	ASSERT(node_locks[nid].lock_held());
	ASSERT(the_endpoints[nid].exists(epp->get_translist_elem()));
	(void) the_endpoints[nid].erase(epp->get_translist_elem());
	// Increment the list incarnation number to indicate a change
	// in the list.
	the_endpoints_incn[nid]++;
}

bool
transport::remove_path(clconf_path_t *clconf_pathp)
{
	nodeid_t	nid = clconf_path_get_remote_nodeid(clconf_pathp);

	if (!this_transport(clconf_pathp, true))
		return (false);

	TRANS_DBG(("T(%p) remove_path(%p) to node %d\n",
	    this, clconf_pathp, nid));

	lock_node(nid);

	IntrList<endpoint, _SList>::ListIterator eli(the_endpoints[nid]);
	endpoint *endpointp;

	for (; (endpointp = eli.get_current()) != NULL; eli.advance())
		endpointp->path_down(clconf_pathp);

	IntrList<pathend, _SList>::ListIterator pli(the_pathends[nid]);
	pathend *pathendp;

	for (; (pathendp = pli.get_current()) != NULL; pli.advance())
		if (pathendp->eq(clconf_pathp))
			break;

	unlock_node(nid);

	ASSERT(pathendp != NULL);

	pathendp->remove_path(clconf_pathp);

	lock_node(nid);
	ASSERT(pathendp->verify_count(0));
	ASSERT(the_pathends[nid].exists(pathendp));
	(void) the_pathends[nid].erase(pathendp);
	unlock_node(nid);

	pathendp->get_pathend_rele();

	return (true);
}

bool
transport::path_up(clconf_path_t *clconf_pathp)
{
	nodeid_t	nid = clconf_path_get_remote_nodeid(clconf_pathp);

	if (!this_transport(clconf_pathp, false))
		return (false);

	TRANS_DBG(("T(%p) path_up(%p)\n", this, clconf_pathp));

	lock_node(nid);

	IntrList<pathend, _SList>::ListIterator pli(the_pathends[nid]);
	pathend *pathendp;

	for (; (pathendp = pli.get_current()) != NULL; pli.advance())
		if (pathendp->path_up(clconf_pathp))
			break;

	ASSERT(pathendp != NULL);

	unlock_node(nid);
	return (true);
}

bool
transport::path_down(clconf_path_t *clconf_pathp)
{
	nodeid_t	nid = clconf_path_get_remote_nodeid(clconf_pathp);

	if (!this_transport(clconf_pathp, false))
		return (false);

	TRANS_DBG(("T(%p) path_down(%p)\n", this, clconf_pathp));

	lock_node(nid);

	IntrList<endpoint, _SList>::ListIterator eli(the_endpoints[nid]);
	endpoint *endpointp;

	for (; (endpointp = eli.get_current()) != NULL; eli.advance())
		endpointp->path_down(clconf_pathp);

	IntrList<pathend, _SList>::ListIterator pli(the_pathends[nid]);
	pathend *pathendp;

	for (; (pathendp = pli.get_current()) != NULL; pli.advance())
		if (pathendp->path_down(clconf_pathp))
			break;

	ASSERT(pathendp != NULL);

	unlock_node(nid);

	return (true);
}

bool
transport::node_alive(nodeid_t, incarnation_num)
{
	// Nothing to do here
	return (true);
}

bool
transport::disconnect_node(nodeid_t nid, incarnation_num incn)
{
	TRANS_DBG(("T(%p) disconnect_node(nid = %u, incn = %u)\n", this, nid,
	    incn));
	lock_node(nid);

	IntrList<endpoint, _SList>::ListIterator eli(the_endpoints[nid]);
	endpoint *endpointp;

	for (; (endpointp = eli.get_current()) != NULL; eli.advance())
		endpointp->disconnect_node(incn);

	IntrList<pathend, _SList>::ListIterator pli(the_pathends[nid]);
	pathend *pathendp;

	for (; (pathendp = pli.get_current()) != NULL; pli.advance())
		pathendp->disconnect_node(incn);

	unlock_node(nid);

	return (true);
}

void
transport::shutdown()
{
	delete this;
}

bool
transport::node_died(nodeid_t nid, incarnation_num incn)
{
	TRANS_DBG(("T(%p) node_died(nid = %u, incn = %u)\n", this, nid, incn));

	lock_node(nid);

	IntrList<endpoint, _SList>::ListIterator eli(the_endpoints[nid]);
	endpoint *endpointp;

	// Store the current incarnation number of the endpoint list.
	// This is used later to check if there were any changes to the
	// endpoint list during the time we release the lock on the list.
	uint64_t current_ep_incn = the_endpoints_incn[nid];

	while ((endpointp = eli.get_current()) != NULL) {
		eli.advance();
		if (endpointp->node_died_prepare(incn)) {
			unlock_node(nid);
			endpointp->node_died();
			lock_node(nid);
			endpointp->rele();
			// Check if the endpoint list has changed during
			// the time we released the lock. If changed,
			// reinit the list iterator.
			if (current_ep_incn != the_endpoints_incn[nid]) {
				eli.reinit(the_endpoints[nid]);
				current_ep_incn = the_endpoints_incn[nid];
			}
		}
	}

	unlock_node(nid);
	return (true);
}

//
// this_transport() returns true if the path represented by "p" is of
// this transport type, otherwise false.
// old_tree flag is used to determine if we should look in the old clconf
// tree or the current(or new) tree. remove_path should look into the old
// clconf tree because the adapter objects that make the path might not
// exist in the new tree. Old tree is always guaranteed to have the
// adapters. Currently, other callers of this method should be able to find
// the adapter objects in the current tree, so they all should call with
// false for the second argument.
//
bool
transport::this_transport(clconf_path_t *p, bool old_tree)
{
	clconf_cluster_t *cl;

	if (old_tree)
		cl = clconf_cluster_get_old();
	else
		cl = clconf_cluster_get_current();
	clconf_adapter_t *ap = clconf_path_get_adapter(p, CL_REMOTE, cl);
	bool ret = (os::strcmp(transport_id(),
	    clconf_adapter_get_transport_type(ap)) == 0);
	clconf_obj_release((clconf_obj_t *)cl);
	return (ret);
}

pathend::pathend(transport *transp, clconf_path_t *p) :
    pm_if(1), _SList::ListElem(this), clconf_pathp(p), transportp(transp),
    pe_node(clconf_path_get_remote_nodeid(p), INCN_UNKNOWN),
#ifdef	USE_TICKSTATS
    send_tick_stat("send", 1),
    recv_tick_stat("recv", 1),
#endif
#ifdef PERPE_PMHB_DBGBUF
    hb_dbgp(NULL),
    hb_dbgo(NULL),
#endif
    p_state(P_CONSTRUCTED), task_deferred(false),
    delay_retry(false),
    pdt(this), pmsdt(this), gpr(this),
    last_send_time(0),
    last_sendsched_time(0),
    ep_state(0),
    last_reg_hrtime(0),
    cur_reg_hrtime(0),
    drop_hrtime(0),
    drop_reason(PDR_NONE)
{
//	clconf_obj_hold(clconf_pathp);

	// Compute the maximum allowed send delay from the timeout.
	// Assume that if the send thread was held up by about 4/5 the
	// the timeout for that path, then something is wrong.
	// The original timeout is in milliseconds, but we store it in
	// nanoseconds so we don't need to do arithmetic later on.

	max_send_delay = (((hrtime_t)clconf_path_get_timeout(p))
				* (4 * HR_TICKS_PER_MS / 5));
	ASSERT(max_send_delay > 0);

#ifdef PERPE_PMHB_DBGBUF
	// Allocate the per pathend heartbeat debug buffers
	hb_dbgp = new dbg_print_buf(perpe_hb_dbg_size, UNODE_DBGOUT_MMAP,
	    true);
	hb_dbgo = new dbg_print_buf(perpe_hb_dbg_size, UNODE_DBGOUT_MMAP,
	    true);
#endif

	// Get id of local adapter from clconf_path
	clconf_cluster_t *cl = clconf_cluster_get_current();
	char *e_name = clconf_get_pathname(p, cl);
	local_adp_id = clconf_path_get_adapterid(p, CL_LOCAL);
	remote_adp_id = clconf_path_get_adapterid(p, CL_REMOTE);
	TRANS_DBG(("T(%p) P(%p) %u:%u created\n", transportp, this,
	    pe_node.ndid, pe_node.incn));

	// Adding a state proxy
	char	*pname, *pe_name;
	nodeid_t	n1, n2;
	uint_t		aid1, aid2;

	n1 = orb_conf::local_nodeid();
	n2 = pe_node.ndid;
	aid1 = (uint_t)local_adp_id;
	aid2 = (uint_t)clconf_path_get_adapterid(p, CL_REMOTE);
	pname = clconf_get_internal_pathname(n1, aid1, n2, aid2);
	pe_name = new char[strlen(pname) + strlen(".?") + 1];
	os::sprintf(pe_name, "%s.%s", pname, (n1 != pe_node.ndid) ? "l" : "h");

	// Release the clconf tree
	clconf_obj_release((clconf_obj_t *)cl);

	pe_sproxy = new pathend_state_proxy(pe_name, e_name);
	// No need to acquire proxy lock before register_with_csr because
	// we do not access any critical data in the method
	// unregister_from_csr is currently called from the destructor.
	// even if we have two threads with one doing register and other
	// doing unregister, they are protected by the table lock in the
	// comp_state_registry.
	if (pe_sproxy->register_with_csr())
	    TRANS_DBG(("E(%p) CSR Registration of pathend state proxy failed",
		this));
	pe_sproxy->proxy_lock();
	pe_sproxy->set_state(p_state);
	pe_sproxy->proxy_unlock();
	delete [] pname;
	delete [] pe_name;
	delete [] e_name;
}

pathend::~pathend()
{
	TRANS_DBG(("T(%p) P(%p) %u:%u deleted\n", transportp, this,
	    pe_node.ndid, pe_node.incn));
//	clconf_obj_release(clconf_pathp);
	clconf_pathp = NULL;
	transportp = NULL;
	send_buf = NULL;
	rec_buf = NULL;

#ifdef PERPE_PMHB_DBGBUF
	// Delete the per pathend heartbeat debug buffers
	delete hb_dbgp;
	delete hb_dbgo;
	// Since the above two lines free the two debug buffers the NULL
	// assignments below does not create memory leaks.
	hb_dbgp = hb_dbgo = NULL;	//lint !e423
#endif

	// Delete the state proxy
	// We are holding the lock here because we want to make sure that if
	// we do not delete the object if any other thread is working on it.
	pe_sproxy->proxy_lock();
	delete pe_sproxy;
	pe_sproxy = NULL;		//lint !e423

	ASSERT(p_state == P_DELETED);
	ASSERT(pe_node.incn == INCN_UNKNOWN);
	ASSERT(!task_deferred);
}

void
pathend::startup()
{
	lock();
	queue_task();
	unlock();
	TRANS_DBG(("T(%p) P(%p) %u:%u starting\n", transportp, this,
		pe_node.ndid, pe_node.incn));
}

const ID_node &
pathend::node() const
{
	return (pe_node);
}

clconf_path_t *
pathend::path() const
{
	return (clconf_pathp);
}

const char *
pathend::get_extname() const
{
	return (pe_sproxy->get_extname());
}

#ifdef PERPE_PMHB_DBGBUF
const dbg_print_buf *
pathend::get_hb_dbg() const
{
	return (hb_dbgp);
}

// Point the current debug buffer pointer to the other buffer.
void
pathend::swap_hb_dbg()
{
	dbg_print_buf	*tmp_bufp;

	ASSERT(lock_held());
	tmp_bufp = hb_dbgp;
	hb_dbgp = hb_dbgo;
	hb_dbgo = tmp_bufp;
}
#endif

// path_up is only necessary to notify the other components the path is
// being declared up.  The acceptance of the pathend from the defer_task
// implies the path will be considered up, so if register_pathend returns
// true in enter_machine, the p_state is set to P_UP.
bool
pathend::path_up(clconf_path_t *p)
{
	if (!eq(p))
		return (false);

	ASSERT(lock_held());
	ASSERT(p_state == P_INITIATED);

	return (true);
}

bool
pathend::path_down(clconf_path_t *p)
{
	if (!eq(p))
		return (false);

	lock();
	switch (p_state) {
	case P_UP:
		TRANS_DBG(("T(%p) P(%p) %u:%u P_UP -> P_CLEANING on"
		    " path_down\n", transportp, this, pe_node.ndid,
		    pe_node.incn));
		set_state(P_CLEANING);
		queue_task();
		break;

	case P_INITIATED:
		TRANS_DBG(("T(%p) P(%p) %u:%u P_INITIATED -> P_CLEANING on"
		    " path_down\n", transportp, this, pe_node.ndid,
		    pe_node.incn));
		set_state(P_CLEANING);
		pe_cv.broadcast();
		break;

	case P_CONSTRUCTED:
	case P_INITERR:
	case P_CLEANING:
	case P_DRAINING:
	case P_DELCLEAN:
	case P_DELETING:
	case P_DELETED:
		//
		// SCMSGS
		// @explanation
		// The system maintains state information about a path. A
		// path_down operation is not allowed in this state.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend %p: path_down not allowed in state %d",
		    this, p_state);
		break;
	default:
		//
		// SCMSGS
		// @explanation
		// The system maintains state information about a path. The
		// state information is invalid.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend %p: %d is not a pathend state", this,
		    p_state);
		break;
	}
	unlock();

	return (true);
}

//
// void pathend::init_error(void)
//
// This method puts the pathend in the P_INITERR state. This method is
// called by specific transports when they encounter errors during initiate
// that might require user attention. Initiate attempts continue after
// putting the pathend in the P_INITERR state. This state transition is
// only to bring the initiate problems to the attention of the user.
// Caller must call this method with the lock held.
//
void
pathend::init_error(void)
{
	ASSERT(lock_held());
	if (p_state == P_CONSTRUCTED)
		set_state(P_INITERR);
}

void
pathend::reinit()
{
	// The default implementation is null.
	// Derived classes are allowed to override.
}

void
pathend::disconnect_node(incarnation_num the_incn)
{
	lock();

	if (pe_node.incn != the_incn) {
		unlock();
		return;
	}

	switch (p_state) {
	case P_UP:
		TRANS_DBG(("T(%p) P(%p) %u:%u P_UP -> P_CLEANING on "
		    "disconnect_node\n", transportp, this,
		    pe_node.ndid, pe_node.incn));
		set_state(P_CLEANING);
		queue_task();
		break;
	case P_INITIATED:
		TRANS_DBG(("T(%p) P(%p) %u:%u P_INITIATED -> P_CLEANING on "
		    "disconnect_node\n", transportp, this,
		    pe_node.ndid, pe_node.incn));
		set_state(P_CLEANING);
		pe_cv.broadcast();
		break;
	// Ignored in these states because the path is already going down
	case P_CLEANING:
	case P_DRAINING:
	case P_DELCLEAN:
	case P_DELETING:
		break;
	case P_DELETED:
	case P_CONSTRUCTED:
	case P_INITERR:
		ASSERT(pe_node.incn == INCN_UNKNOWN);
		//
		// SCMSGS
		// @explanation
		// The system maintains state information about a path. The
		// disconnect_node operation is not allowed in this state.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend %p: disconnect_node not allowed",
		    this);
		break;
	default:
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend %p: %d is not a pathend state", this,
		    p_state);
		break;
	}

	unlock();
}

/*
 *
 *		Remove Path state transitions
 *
 *	CONSTRUCTED, INITERR :  If we are in these states then remove path
 *				should simply change the state to DELETING
 *				Since we do not drop the lock until the
 *				state is changed to DELETING there is no
 *				race condition with any other threads
 *
 *	INITIATED	     :  This is a temporary state. We will be
 *				in CLEANING or UP state very soon, so wait
 *				for the signal which is done when the state
 *				is changed to CLEANING or UP in enter_machine.
 *
 *	UP		     :	If the path is UP set the state to DELCLEAN
 *				and put a task in the threadpool and wait
 *				in the DELCLEAN state for a signal. When the
 *				task is executed in enter_machine cleanup is
 *				done and the waiting thread will be waken up.
 *
 *	CLEANING	     :	We are in this state means there is a task
 *				queued to do the cleanup (hence the assert).
 *				change the state to DELCLEAN and wait in
 *				DELCLEAN state for the signal from the
 *				thread doing cleanup.
 *
 *	DELCLEAN	     :  Wait here for the signal from the thread doing
 *				the cleanup in enter_machine. When we are
 *				awake we will be in DELETING state.
 *
 *	DELETING	     :	Here wait for the refcnt_unref to signal on
 *				the condition variable which it does after
 *				changing the state to DELETED.
 *
 *	DELETED		     : 	We are done!
 *
 */

void
pathend::remove_path(clconf_path_t *p)
{
	bool not_done;

	if (!eq(p))
		return;

	lock();

	do {
		not_done = true;
		switch (p_state) {
		case P_CONSTRUCTED:
		case P_INITERR:
			TRANS_DBG(("T(%p) P(%p) %u:%u remove_path in %s\n",
			    transportp, this, pe_node.ndid, pe_node.incn,
			    p_state == P_CONSTRUCTED ? "P_CONSTRUCTED" :
			    "P_INITERR"));
			set_state(P_DELETING);
			delete_notify();
			unlock();
			rele();
			lock();
			break;
		case P_INITIATED:
			TRANS_DBG(("T(%p) P(%p) %u:%u remove_path in "
			    "P_INITIATED\n", transportp, this, pe_node.ndid,
			    pe_node.incn));
			set_state(P_DELCLEAN);
			pe_cv.broadcast();
			break;
		case P_UP:
			TRANS_DBG(("T(%p) P(%p) %u:%u remove_path in P_UP\n",
			    transportp, this, pe_node.ndid, pe_node.incn));
			set_state(P_DELCLEAN);
			queue_task();
			break;
		case P_CLEANING:
			TRANS_DBG(("T(%p) P(%p) %u:%u remove_path in "
			    "P_CLEANING\n", transportp, this, pe_node.ndid,
			    pe_node.incn));
			ASSERT(task_deferred);
			set_state(P_DELCLEAN);
			break;
		case P_DRAINING:
			TRANS_DBG(("T(%p) P(%p) %u:%u remove_path in"
			    " P_DRAINING\n", transportp, this, pe_node.ndid,
			    pe_node.incn));
			set_state(P_DELETING);
			break;
		case P_DELCLEAN:
		case P_DELETING:
			pe_cv.wait(&(pe_sproxy->get_lock()));
			break;
		case P_DELETED:
			not_done = false;
			break;
		default:
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: Pathend %p: %d is not a pathend state",
			    this, p_state);
			break;
		}
	} while (not_done);

	unlock();

	if (pe_sproxy->unregister_from_csr())
		//
		// SCMSGS
		// @explanation
		// The system failed to unregister the pathend state proxy.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Unregister of pathend state proxy failed");


}

void
pathend::atomic_copy_64(uint64_t *dst, uint64_t *src)
{
	os::atomic_copy_64(dst, src);
}

void
pathend::fail()
{
	TRANS_DBG(("T(%p) P(%p) %u:%u fail called\n", transportp, this,
	    pe_node.ndid, pe_node.incn));
	path_manager::the().revoke_pathend(this);
}

void
pathend::refcnt_unref()
{
	TRANS_DBG(("T(%p) P(%p) %u:%u refcnt_unref called\n", transportp, this,
	    pe_node.ndid, pe_node.incn));

	lock();

	ASSERT(!task_deferred);
	ASSERT((p_state == P_DRAINING) || (p_state == P_DELETING));

	// Do not hold the lock if sleeping.
	if (p_state == P_DRAINING) {
		if (delay_retry) {
			unlock();
			os::usecsleep((long)PE_THRASH_SLEEP);
			lock();
		}
		delay_retry = false;

		// We drop lock and reacquire it in reinit()
		reinit();
	}

	ASSERT((p_state == P_DRAINING) || (p_state == P_DELETING));

	pe_node.incn = INCN_UNKNOWN;

	// Here we have lock and make sure that we do not drop it until
	// the state is changed to either DELETED or CONSTRUCTED.
	if (p_state == P_DELETING) {
		// Tell the remove thread that we're ready to reap the
		// pathend
		set_state(P_DELETED);
		pe_cv.broadcast();
	} else if (p_state == P_DRAINING) {
		set_state(P_CONSTRUCTED);
		hold();
		queue_task();
	}

	unlock();
}

void
pathend::task_execute()
{
	TRANS_DBG(("T(%p P(%p) %u:%u entering task_execute\n", transportp,
	    this, pe_node.ndid, pe_node.incn));
	lock();
	while (task_not_done()) {
		enter_machine();
		ASSERT(lock_held());
	}
	task_deferred = false;
	unlock();
	rele();
}

bool
pathend::task_not_done()
{
	ASSERT(lock_held());
	ASSERT(p_state != P_INITIATED);
	return ((p_state == P_CONSTRUCTED) || (p_state == P_INITERR) ||
	    (p_state == P_DELCLEAN) || (p_state == P_CLEANING));
}

/*
 *
 *			enter_machine	state transitions
 *
 *	CONSTRUCTED/INITERR :	We try to initiate and if we fail, the state
 *				is changed to INITERR by the specific transport
 *				and not here. Since we drop the lock
 *				in initiate(), we make sure that we are still
 *				in CONSTRUCTED/INITERR state before changing
 *				the state to INITIATED. Then we try to
 *				register the pathend. We drop the lock while
 *				doing the registration and it is possible
 *				that the state is not INITIATED when we
 *				return from register_pathend(). If it is
 *				not in INITIATED state then it should be in
 *				DELCLEAN state. If we have successfully
 *				registered and still in INITIATED state
 *				then we create the endpoint and wait for
 *				endpoint to be registered. If the endpoint
 *				is registered and we are still in INITIATED
 *				state then the state is changed to UP.
 *				At any point of time when the lock is not
 *				held by this thread and the state is changed,
 *				the new state would be either CLEANING or
 *				DELCLEAN and we continue to that case in the
 *				switch statement.
 *
 *	CLEANING/DELCLEAN   :	We will be in CLEANING state if path is
 *				going down and DELCLEAN state if path is
 *				getting removed. In both the cases state machine
 *				will do the cleanup. If the path is getting
 *				removed we change the state to DELETING and
 *				wake up the thread that is waiting in the
 *				remove_path and if the path is going down
 *				state is changed to DRAINING. Lock is dropped
 *				while doing the cleanup() and hence we have
 *				to check the state to put the pathend in
 *				correct state, i.e., DRAINING or DELETING.
 *
 *	All other states are not valid here.
 *
 */

void
pathend::enter_machine()
{
	incarnation_num incn = INCN_UNKNOWN;
	endpoint *epp;
	bool registered = false;
	pathend_state saved_state;

	ASSERT(lock_held());

	//
	// After this point, paths can be formed. We want to make sure
	// CMM gets the node_is_reachable callbacks. Wait for
	// it to register with us before proceeding.
	//
	pe_sproxy->get_lock().unlock();
	path_manager::the().wait_for_cmm();
	pe_sproxy->get_lock().lock();

	switch (p_state) {
	case P_CONSTRUCTED:
	case P_INITERR:
		ASSERT(pe_node.incn == INCN_UNKNOWN);
		TRANS_DBG(("T(%p) P(%p) %u:%u initiating\n", transportp, this,
		    pe_node.ndid, pe_node.incn));
		saved_state = p_state;
		initiate(send_buf, rec_buf, incn);
		ASSERT(lock_held() && task_deferred);
		if (p_state == P_CONSTRUCTED || p_state == P_INITERR) {
			set_state(P_INITIATED);
#ifdef PERPE_PMHB_DBGBUF
			// Swap debug buffers so that old contents
			// do not get overwritten. Done to aid path timeout
			// debugging after the path has come up subsequently.
			swap_hb_dbg();
#endif
			pe_node.incn = incn;
			TRANS_DBG(("T(%p) P(%p) %u:%u registering\n",
			    transportp, this, pe_node.ndid, pe_node.incn));
			registered =
			    path_manager::the().register_pathend(this,
			    pe_sproxy->get_lock());
			ASSERT(lock_held());
			if (p_state != P_INITIATED) {
				ASSERT(p_state == P_DELCLEAN);
				break;
			}
			if (registered) {
			    TRANS_DBG(("T(%p) P(%p) %u:%u register "
				"succeeded\n", transportp, this,
				pe_node.ndid, pe_node.incn));

			    // Record the registration time
			    last_reg_hrtime = cur_reg_hrtime;
			    cur_reg_hrtime = os::gethrtime();
			    ep_state = P_EP_NEW;
			    unlock();
			    // Create endpoint
			    epp = new_endpoint(transportp);
			    transportp->lock_node(pe_node.ndid);
			    lock();
			    if (p_state == P_INITIATED) {
				unlock();
				transportp->add_endpoint(epp, pe_node.ndid);
				transportp->unlock_node(pe_node.ndid);
				lock();
				// Wait until the endpoint comes up and gets
				// registered or until we are told that the
				// endpoint bringup failed.
				while (ep_state & P_EP_NEW) {
					pe_cv.wait(&(pe_sproxy->get_lock()));
				}
				// Put the path in the UP state if the ep
				// came up successfully. Initiate the process
				// to bringdown the path otherwise.
				if (p_state == P_INITIATED) {
					if (ep_state & P_EP_REGISTERED) {
						set_state(P_UP);
					} else {
						ep_state |= P_EP_FAULTED;
						while ((p_state !=
							P_CLEANING) &&
							(p_state !=
							P_DELCLEAN)) {
							pe_cv.wait(&(
							pe_sproxy->get_lock()));
						}
					}
				}
			    } else {
				transportp->unlock_node(pe_node.ndid);
				epp->rele();
			    }
			} else {
				TRANS_DBG(("T(%p) P(%p) %u:%u register "
				    "rejected\n", transportp, this,
				    pe_node.ndid, pe_node.incn));
				set_state(P_CLEANING);
				delay_retry = true;
			}
		} else {
			TRANS_DBG(("T(%p) P(%p) %u:%u removed in %s\n",
			    transportp, this, pe_node.ndid, pe_node.incn,
			    saved_state == P_CONSTRUCTED ? "P_CONSTRUCTED" :
			    "P_INITERR"));
			ASSERT(p_state == P_DELETING);
			// If we get a remove_path in the CONSTRUCTED state,
			// we move right to DELETING, not DELCLEAN, and let
			// the defer_task clean up when initiate finishes.
			cleanup();
		}
		break;
	case P_DELCLEAN:
	case P_CLEANING:
		TRANS_DBG(("T(%p) P(%p) %u:%u P_CLEANING/P_DELCLEAN\n",
		    transportp, this, pe_node.ndid, pe_node.incn));
		cleanup();
		ASSERT(lock_held() && task_deferred);
		if (p_state == P_CLEANING)
			set_state(P_DRAINING);
		else {
			ASSERT(p_state == P_DELCLEAN);
			set_state(P_DELETING);
		}
		unlock();
		rele();
		lock();
		// If the path is being removed then signal any threads
		// that are waiting on the condition variable.
		if (p_state == P_DELETING)
			pe_cv.broadcast();
		break;
	case P_UP:
	case P_DRAINING:
	case P_DELETING:
	case P_INITIATED:
	case P_DELETED:
		//
		// SCMSGS
		// @explanation
		// The system maintains state information about a path. A
		// deferred task is not allowed in this state.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend %p: deferred task not allowed in state %d",
		    this, p_state);
		break;
	default:
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend %p: %d is not a pathend state",
		    this, p_state);
		break;
	}

}

// Check if a scheduled thread has been delayed. Panic machine if this is
// indeed the case.
void
pathend::check_for_delay(hrtime_t last_sched_time, hrtime_t last_exec_time,
			    hrtime_t max_delay, const char *pmsg)
{
	ASSERT(lock_held());

	// last_xxx_time == 0 means that the event has never occurred before.
	// This means that we will not catch a delay before the first event
	// happens. Seems acceptable for now.

	if (last_exec_time != 0 && last_sched_time != 0 &&
	    (last_sched_time > last_exec_time + max_delay)) {
		uint32_t diff = (uint32_t)((last_sched_time - last_exec_time)
					/ HR_TICKS_PER_MS);
		TRANS_DBG(("T(%p) P(%p) %s %lld %lld %lld %d\n",
			    transportp, this, pmsg, last_sched_time,
			    last_exec_time, max_delay, diff));

#ifdef	CHECK_FOR_SENDTHREAD_RUNNING
		//
		// SCMSGS
		// @explanation
		// The pathend aborted the node for the specified reason.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Pathend: Aborting node because %s for %u ms",
		    pmsg, diff);
#else
		// SC_SYSLOG_ERROR is not defined for kernel mode

		//
		// SCMSGS
		// @explanation
		// The system would have aborted the node for the specified
		// reason if the check for send thread running was enabled.
		// @user_action
		// No user action is required.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Pathend: would abort node because %s for %u ms",
		    pmsg, diff);
#endif	// CHECK_FOR_SENDTHREAD_RUNNING
	}
}

void
pathend::mark_ep_faulted()
{
	ASSERT(!lock_held());
	lock();
	if (p_state == P_INITIATED || p_state == P_UP) {
		ep_state |= P_EP_FAULTED;
	}
	unlock();
}

void
pathend::pm_put(const void * const thebuf)
{
#if (defined(_KERNEL) && defined(PERPE_PMHB_DBGBUF))
	HB_DBG(this, ("P(%x) put(%016llx)\n",
	    (int)curthread->t_pil, *(uint64_t *)thebuf));
#else
	HB_DBG(this, ("P(%p) PE(%p) N(%d) put(%016llx)\n",
	    path(), this, node().ndid, *(uint64_t *)thebuf));
#endif

#ifdef	FAULT_TRANSPORT
	void *fault_argp;
	uint32_t fault_argsize;

	// We check if the drop_all_outgoing_msgs is fault pt is
	if (fault_triggered(FAULTNUM_PM_DROP_ALL_OUTGOING_MSGS, &fault_argp,
	    &fault_argsize)) {
		os::printf("Fault 0x%x: Dropping pathend message to %d\n",
			FAULTNUM_PM_DROP_ALL_OUTGOING_MSGS, pe_node.ndid);
		return;
	}
#endif	// FAULT_TRANSPORT

	lock();
	atomic_copy_64((uint64_t *)send_buf, (uint64_t *)thebuf);
	ASSERT((p_state == P_INITIATED) || (p_state == P_UP));
	if (transportp->defer_send) {
		hrtime_t curr_time = os::gethrtime();
#ifdef	FAULT_TRANSPORT
		// simulate a send thread delay
		if (fault_triggered(FAULTNUM_TRANSPORT_DELAY_SEND_THREAD,
				    &fault_argp, &fault_argsize)) {
			os::printf("Fault 0x%x: late send thread to node %d\n",
				    FAULTNUM_TRANSPORT_DELAY_SEND_THREAD,
				    pe_node.ndid);
			last_sendsched_time = last_send_time +
							max_send_delay + 1;
		}
#endif	// FAULT_TRANSPORT

		// Verify that the send thread is not delayed.
		if (path_manager::the().is_enabled())
			check_for_delay(last_sendsched_time, last_send_time,
				max_send_delay, "send thread not running");

		last_sendsched_time = curr_time;

		queue_send();	// send out later
	} else
		pm_send_internal();
	unlock();
}

void
pathend::pm_get(void * const thebuf)
{
	// updates the rec_buf if needed
	pm_get_internal();

	atomic_copy_64((uint64_t *)thebuf, (uint64_t *)rec_buf);
#if (defined(_KERNEL) && defined(PERPE_PMHB_DBGBUF))
	HB_DBG(this, ("G(%x) get(%016llx)\n",
	    (int)curthread->t_pil, *(uint64_t *)thebuf));
#else
	HB_DBG(this, ("P(%p) PE(%p) N(%d) get(%016llx)\n",
	    path(), this, node().ndid, *(uint64_t *)thebuf));
#endif
}

#ifdef WRAP_TEST
// Simulate heartbeat wrapping
// When fault point is triggered, copy the modified local heartbeat
// (advanced with large token value) back to rec_buf, so to satisfy the later
// pm_isnewer check.
void
pathend::pm_wrap(void * const thebuf)
{
	atomic_copy_64((uint64_t *)rec_buf, (uint64_t *)thebuf);
}
#endif  // WRAP_TEST


// Determine if the heartbeat in newbuf is more current that the last
// heartbeat accepted.
bool
pathend::pm_isnewer(const void *newbuf)
{
	return (path_manager::the().pm_isnewer(newbuf, rec_buf));
}

#ifdef	USE_TICKSTATS
// Check if  the tick stats are  ok. If not, and  the associated fault
// point is switched on, the node is panicked.
//
// Note: for  the check to make sense,  it must be called  from a very
// high priority thread  that is virtually guaranteed to  run, such as
// the clock interrupt thread.
//
// If a specific transport does  not use tick stats, and never updates
// them, pm_check_tickstats() will become a no-op.
void
pathend::pm_check_tickstats()
{
	send_tick_stat.check();
	recv_tick_stat.check();
}
#endif	// USE_TICKSTATS

void
pathend::send_execute()
{
//	TRANS_DBG(("T(%p) P(%p) %u:%u send_execute called\n", transportp, this,
//		pe_node.ndid, pe_node.incn));
	lock();
	ASSERT(transportp->defer_send);

	last_send_time = os::gethrtime();

	if ((p_state == P_INITIATED) || (p_state == P_UP)) {
		pm_send_internal();
	} else {
		TRANS_DBG(("T(%p) P(%p) send_execute failed: p_state == %u\n",
		    transportp, this, p_state));
	}

	unlock();
	rele();
}

pathend_defer_task::pathend_defer_task(pathend *p) : pe(p)
{
}

void
pathend_defer_task::execute()
{
	pe->task_execute();
}

void
pathend_defer_task::task_done()
{
	ASSERT(0);
}

pm_send_defer_task::pm_send_defer_task(pathend *p) :
	pe(p)
{
	send_task_deferred = false;
}

void
pm_send_defer_task::execute()
{
	pe->send_execute();
}

bool
pm_send_defer_task::is_deferred()
{
	ASSERT(hb_threadpool::the().lock_held());
	return (send_task_deferred);
}

void
pm_send_defer_task::set_deferred(bool value)
{
	ASSERT(hb_threadpool::the().lock_held());
	send_task_deferred = value;
}

void
pm_send_defer_task::task_done()
{
	ASSERT(0);
}

get_pathend_refcnt::get_pathend_refcnt(pathend *p) : refcnt(1), pe(p)
{
}

void
get_pathend_refcnt::refcnt_unref()
{
	delete pe;
}

endpoint::endpoint(transport *tr, pathend *pe) : er_if(1),
    pathendp(pe), transportp(tr), clconf_pathp(pe->path()), ep_node(pe->node()),
    e_state(E_CONSTRUCTED), task_deferred(false), translist_elem(this),
    edt(this), eddt(this)
{
	// the pathend is guaranteed to be there because the pathend
	// state machine is still running when the endpoint is created,
	// and it has a hold on the pathend.
	pathendp->hold();
	local_adp_id = (uint_t)pathendp->local_adapter_id();
	TRANS_DBG(("T(%p) E(%p) %u:%u created\n", transportp, this,
	    ep_node.ndid, ep_node.incn));
}

endpoint::~endpoint()
{
	ASSERT((e_state == E_CONSTRUCTED) || (e_state == E_DELETED));
	ASSERT(pathendp == NULL);
	transportp = NULL;
	clconf_pathp = NULL;
}

void
endpoint::startup()
{
	// The endpoint waits until it is on the list to start up the
	// deferred task.
	lock();
	queue_task();
	unlock();
	TRANS_DBG(("T(%p) E(%p) %u:%u starting\n", transportp, this,
	    ep_node.ndid, ep_node.incn));
}

//
// endpoint::registration_callback
//
// This method is called from endpoint_registry::register_endpoint
// to let the endpoint know that registration is complete and the
// endpoint should set its state to E_REGISTERED.
//
void
endpoint::registration_callback()
{
	ASSERT(lock_held());
	ASSERT(e_state == E_CONSTRUCTED);
	e_state = E_REGISTERED;
}

// virtual method default
void
endpoint::timeouts_disable()
{
}

// virtual method default
void
endpoint::timeouts_enable()
{
}

void
endpoint::path_down(clconf_path_t *p)
{
	if (!eq(p))
		return;

	lock();

	switch (e_state) {
	case E_CONSTRUCTED:
		TRANS_DBG(("T(%p) E(%p) %u:%u E_CONSTRUCTED -> "
		    "E_PUSHING on path_down\n", transportp, this,
		    ep_node.ndid, ep_node.incn));
		e_state = E_PUSHING;
		//
		// The pathend is waiting for the registration status of the
		// endpoint. The endpoint is registered from the state machine
		// when the endpoint enters the state machine in the
		// E_CONSTRUCTED state.  Since now the state machine entry
		// point has changed, notify the pathend right away that the
		// registration is not going to happen.
		//
		// It is possible that the endpoint has already entered the
		// state machine but has dropped the endpoint lock from within
		// the call to initiate. When the call to initiate returns,
		// the state machine will call record_ep_registration also.
		// It is ok to call record_ep_registration multiple times with
		// the registration status false.
		//
		pathendp->record_ep_registration(false);
		queue_task();
		break;
	case E_REGISTERED:
		endpoint_registry::the().unregister_endpoint(this,
		    ep_node.ndid);
		e_state = E_PUSHING;
		TRANS_DBG(("T(%p) E(%p) %u:%u E_REGISTERED -> "
		    "E_PUSHING on path_down\n", transportp, this,
		    ep_node.ndid, ep_node.incn));
		ep_cv.broadcast();
		unblock();
		queue_task();
		break;
	case E_CLEANING:
	case E_DRAINING:
	case E_DELETED:
	case E_PUSHING:
		// If the endpoint hangs around for a long time
		// waiting for sends to complete, the old endpoint
		// can still be on the endpoint list even after
		// the new endpoint has formed a connection.
		// This is fine.  We just ignore it.
		// We can also be in E_PUSHING state here. Look at
		// Bug 4317457 for more details.
		break;
	default:
		//
		// SCMSGS
		// @explanation
		// The system maintains information about the state of an
		// Endpoint. The Endpoint state is invalid.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Endpoint %p: %d is not an endpoint state",
		    this, e_state);
		break;
	}

	unlock();
}

void
endpoint::disconnect_node(incarnation_num the_incn)
{
	if (ep_node.incn != the_incn)
		return;

	lock();

	switch (e_state) {
	case E_CONSTRUCTED:
		TRANS_DBG(("T(%p) E(%p) %u:%u E_CONSTRUCTED -> "
		    "E_PUSHING on disconnect_node\n", transportp,
		    this, ep_node.ndid, ep_node.incn));
		e_state = E_PUSHING;
		//
		// The pathend is waiting for the registration status of the
		// endpoint. The endpoint is registered from the state machine
		// when the endpoint enters the state machine in the
		// E_CONSTRUCTED state.  Since now the state machine entry
		// point has changed, notify the pathend right away that the
		// registration is not going to happen.
		//
		// It is possible that the endpoint has already entered the
		// state machine but has dropped the endpoint lock from within
		// the call to initiate. When the call to initiate returns,
		// the state machine will call record_ep_registration also.
		// It is ok to call record_ep_registration multiple times with
		// the registration status false.
		//
		pathendp->record_ep_registration(false);
		queue_task();
		break;
	case E_REGISTERED:
		endpoint_registry::the().unregister_endpoint(this,
		    ep_node.ndid);
		e_state = E_PUSHING;
		TRANS_DBG(("T(%p) E(%p) %u:%u E_REGISTERED -> "
		    "E_PUSHING on disconnect_node\n", transportp,
		    this, ep_node.ndid, ep_node.incn));
		ep_cv.broadcast();
		unblock();
		queue_task();
		break;
	case E_PUSHING:
	case E_CLEANING:
	case E_DRAINING:
	case E_DELETED:
		break;
	default:
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Endpoint %p: %d is not an endpoint state",
		    this, e_state);
		break;
	}

	unlock();
}

bool
endpoint::node_died_prepare(incarnation_num the_incn)
{
	lock();
	if ((ep_node.incn != the_incn) || (e_state != E_PUSHING)) {
		unlock();
		return (false);
	}
	TRANS_DBG(("T(%p) E(%p) %u:%u node_died_prepare(true)\n", transportp,
	    this, ep_node.ndid, ep_node.incn));
	hold();
	unlock();
	return (true);
}

void
endpoint::node_died()
{
	TRANS_DBG(("T(%p) E(%p) %u:%u node_died called\n", transportp, this,
	    ep_node.ndid, ep_node.incn));
	lock();
	while (e_state == E_PUSHING)
		ep_cv.wait(&ep_lock);
	unlock();
}

void
endpoint::release_pathend()
{
	TRANS_DBG(("T(%p) E(%p) %u:%u release_pathend\n", transportp, this,
	    ep_node.ndid, ep_node.incn));
	ASSERT(lock_held());
	ASSERT(e_state == E_CLEANING);
	pathendp->rele();
	pathendp = NULL;
}

void
endpoint::task_execute()
{
	TRANS_DBG(("T(%p) E(%p) %u:%u entering task_execute\n", transportp,
	    this, ep_node.ndid, ep_node.incn));
	lock();
	while (task_not_done()) {
		enter_machine();
		ASSERT(lock_held());
	}
	task_deferred = false;
	unlock();
	rele();
}

bool
endpoint::task_not_done()
{
	ASSERT(lock_held());
	return ((e_state == E_CONSTRUCTED) || (e_state == E_PUSHING));
}

void
endpoint::enter_machine()
{
	bool e_a;
	ASSERT(lock_held());

	switch (e_state) {
	case E_CONSTRUCTED:
		TRANS_DBG(("T(%p) E(%p) %u:%u initiating\n", transportp, this,
		    ep_node.ndid, ep_node.incn));
		initiate();
		ASSERT(lock_held());
		if (e_state == E_CONSTRUCTED) {
			e_a = endpoint_registry::the().register_endpoint(this,
			    ep_node.ndid, ep_node.incn);
			if (e_a) {
				TRANS_DBG(("T(%p) E(%p) %u:%u register "
				    "success\n", transportp, this,
				    ep_node.ndid, ep_node.incn));
				//
				// The endpoint state should have changed to
				// E_REGISTERED through a synchronous callback
				// from the above register_endpoint method.
				//
				ASSERT(e_state == E_REGISTERED);
				pathendp->record_ep_registration(true);
			} else {
				TRANS_DBG(("T(%p) E(%p) %u:%u register "
				    "rejected\n", transportp, this,
				    ep_node.ndid, ep_node.incn));
				e_state = E_PUSHING;
				pathendp->record_ep_registration(false);
			}
			ep_cv.broadcast();
		} else {
			pathendp->record_ep_registration(false);
		}
		break;
	case E_PUSHING:
		TRANS_DBG(("T(%p) E(%p) %u:%u E_PUSHING in machine\n",
		    transportp, this, ep_node.ndid, ep_node.incn));
		push();
		ASSERT(lock_held());
		ASSERT(e_state == E_PUSHING);
		TRANS_DBG(("T(%p) E(%p) %u:%u E_PUSHING -> E_CLEANING in "
		    "machine\n", transportp, this, ep_node.ndid, ep_node.incn));
		e_state = E_CLEANING;
		ep_cv.broadcast();
		cleanup();
		ASSERT(lock_held());
		ASSERT(pathendp == NULL);
		ASSERT(e_state == E_CLEANING);
		TRANS_DBG(("T(%p) E(%p) %u:%u E_CLEANING -> E_DRAINING in "
		    "machine \n", transportp, this,
		    ep_node.ndid, ep_node.incn));
		e_state = E_DRAINING;
		unlock();
		transportp->lock_node(ep_node.ndid);
		transportp->remove_endpoint(this, ep_node.ndid);
		transportp->unlock_node(ep_node.ndid);
		rele();
		lock();
		break;
	case E_CLEANING:
	case E_REGISTERED:
	case E_DRAINING:
	case E_DELETED:
		//
		// SCMSGS
		// @explanation
		// The system maintains information about the state of an
		// Endpoint. A deferred task is not allowed in this state.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Endpoint %p: deferred task not allowed in "
		    "state %d", this, e_state);
		break;
	default:
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Endpoint %p: %d is not an endpoint state", this,
		    e_state);
		break;
	}
}

void
endpoint::refcnt_unref()
{
	TRANS_DBG(("T(%p) E(%p) %u:%u refcnt_unref called\n", transportp,
	    this, ep_node.ndid, ep_node.incn));
	ASSERT(!task_deferred);
	ASSERT((e_state == E_CONSTRUCTED) || (e_state == E_DRAINING));
	ASSERT(verify_count(0));

	if (e_state == E_CONSTRUCTED) {
		ASSERT(pathendp != NULL);
		pathendp->rele();
		pathendp = NULL;
		delete this;
		return;
	} else {
		e_state = E_DELETED;
		common_threadpool::the().defer_processing(&eddt);
	}
}

#if defined(FAULT_TRANSPORT)
//
// Fails the associated pathend.
// Fault Argument: FaultFunctions::counter_t
//
// In the fault_pathend_fail method below, serialize access to avoid more than
// one thread from failing the last pathend. This is done via the serialize
// mutex.
static os::mutex_t	serialize;

void
endpoint::fault_pathend_fail()
{
	const uint32_t		fault_num = FAULTNUM_TRANSPORT_PATHEND_FAIL;
	void			*fault_argp;
	uint32_t		fault_argsize;
	uint32_t		num_ep;
	pathend			*pep;
	FaultFunctions::counter_t *counterp;

	// To avoid two nodes from failing their common path, only the node
	// with higher nodeid can fail the pathend.
	if (orb_conf::local_nodeid() < ep_node.ndid) {
		return;
	}

	if (! fault_triggered(fault_num, &fault_argp, &fault_argsize)) {
		return;
	}
	ASSERT(fault_argsize == sizeof (*counterp));

	counterp = (FaultFunctions::counter_t *)fault_argp;
	if (! FaultFunctions::do_counting(*counterp)) {
		return; 	// it's not time to fail the pathend yet
	}

	serialize.lock();

	// Fail the pathend only if we have at least two endpoints.
	num_ep = endpoint_registry::the().num_endpoints(ep_node.ndid);
	if (num_ep < 2) {
		os::printf("Fault 0x%x: only %d endpoints to node %d. "
			"Ignored\n", fault_num, num_ep, ep_node.ndid);
		serialize.unlock();
		return;
	}

	lock();
	if ((pep = pathendp) != NULL) {
		os::printf("Fault 0x%x: failing pathend to node %d\n",
			fault_num, ep_node.ndid);
		pep->hold();
		unlock();
		pep->fail();
		pep->rele();
	} else {
		unlock();
	}

	serialize.unlock();
}
#endif // FAULT_TRANSPORT

endpoint_defer_task::endpoint_defer_task(endpoint *p) : ep(p)
{
}

void
endpoint_defer_task::execute()
{
	ep->task_execute();
}

void
endpoint_defer_task::task_done()
{
	ASSERT(0);
}

endpoint_delete_defer_task::endpoint_delete_defer_task(endpoint *p) : ep(p)
{
}

void
endpoint_delete_defer_task::execute()
{
	delete ep;
}

void
endpoint_delete_defer_task::task_done()
{
	ASSERT(0);
}

adapter::adapter(transport *tr, clconf_adapter_t *ap) :
    transp(tr),
    adp_id(clconf_obj_get_id((clconf_obj_t *)ap)), a_state(A_CONSTRUCTED),
    _SList::ListElem(this)
{
	TRANS_DBG(("T(%p) A(%p) created\n", transp, this));
	char *e_name = clconf_get_adaptername(ap);
	char *i_name = clconf_obj_get_int_name((clconf_obj_t *)ap);

	ad_sproxy = new adapter_state_proxy(i_name, e_name);
	if (ad_sproxy->register_with_csr())
	    TRANS_DBG(("A(%p) CSR Registration of adapter state proxy failed",
		this));
	ad_sproxy->proxy_lock();
	ad_sproxy->set_state(a_state);
	ad_sproxy->proxy_unlock();

	delete [] i_name;
	delete [] e_name;
}

adapter::~adapter()
{
	TRANS_DBG(("T(%p) A(%p) deleted\n", transp, this));

	// Delete the state proxy
	if (ad_sproxy->unregister_from_csr())
		//
		// SCMSGS
		// @explanation
		// The system failed to unregister an adapter state proxy.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Unregister of adapter state proxy failed");

	// We are holding the lock here because we want to make sure that if
	// we do not delete the object if any other thread is working on it.
	ad_sproxy->proxy_lock();
	delete ad_sproxy;
	ad_sproxy = NULL;		//lint !e423
	transp = NULL;

	ASSERT(a_state == A_DELETED);
}
