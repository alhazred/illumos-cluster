//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)path_manager.cc	1.190	08/12/17 SMI"

#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <sys/time.h>
#include <sys/mutex.h>
#include <sys/cpuvar.h>
#include <sys/cyclic.h>
#include <sys/clconf_int.h>
#include <orb/invo/common.h>
#include <orb/transport/path_manager.h>
#include <orb/transport/transport.h>
#include <orb/member/members.h>
#include <sys/ddi.h>
#include <h/cmm.h>

//
// This code is compiled only into kernel module.
// So, we do not need to use #ifdef _KERNEL around the code
// that is meant for the weaker form of membership in which
// multiple partitions of the cluster are allowed to survive.
//
#include <sys/clconf_int.h>
#ifdef	WEAK_MEMBERSHIP
#include <ccr/persistent_table.h>
#endif	// WEAK_MEMBERSHIP

#ifdef	PM_DBGBUF
const uint32_t pm_dbg_size = 32768;
dbg_print_buf pm_dbg(pm_dbg_size);
#endif

#if defined(PMHB_DBGBUF) && !defined(PERPE_PMHB_DBGBUF)
const uint32_t hb_dbg_size = 16384;
dbg_print_buf hb_dbg(hb_dbg_size,  UNODE_DBGOUT_MMAP, true);
#endif

//
// If the following macro PM_FIRST_PATH_DOWN_PANIC_SUPPORT is defined,
// the path manager defines a global variable pm_first_path_down_panic
// that controls whether the local node should panic in the event of the
// first path to any node going down. The value should be set to nonzero
// if panic on first path is desired (test code only). Default is 0. The
// next variable pm_ignore_path_drop_mask provides a way to ignore path
// drops to a select set of nodes - this is to facilitate tests that
// intentionally bring down machines. The bits in this mask get cleared
// as soon as the corresponding node establishes its first path with the
// local node.
// This mechanism has been provided to support path timeout debugging
// with non debug bits where the fault injection framework is not
// available. Note that this support is available only for 32 nodes,
// for the ease of updating the mask from adb..
//
#define	PM_FIRST_PATH_DROP_PANIC_SUPPORT
#ifdef	PM_FIRST_PATH_DROP_PANIC_SUPPORT
uint32_t	pm_first_path_drop_panic	= 0x0;
uint32_t	pm_ignore_path_drop_mask	= 0x0;
#endif

// When WRAP_TEST is defined, the fault point assciated with this test
// can be activated, for the purpose of testing proper hearbeat wrapping.
// A passed in token value will be added onto: (1) the
// current_token, i.e. current time; (2) the local heartbeat
// that's received, and (3) the local token in each path group record.
// This is to simulate system's being up long enough time, to have a
// heartbeat value that would eventually wrap.
// When the test is de-activated, current time and heartbeat will be set
// back to normal.  The test is for per-node basis.
// Two global variables defined: wrap_token is the passed in token value;
// wrap_record shows for each path group whether the test has been activated
// and the simulated heartbeats have been sent for this path group.

#ifdef WRAP_TEST
int	wrap_record[PM_PA_GROUP_SIZE] = {0, 0, 0, 0, 0};
uint32_t	wrap_token = 0;
#endif // WRAP_TEST

//
// Number of cyclic intervals after which the network interrupt should
// call for heartbeat scheduling and checking.
//
uint_t cyclic_delay_factor = 2;


//
// The hb_threadpool::send_heartbeat will call the function pointed to by
// this pointer in interrupt context.  Interrupts can begin before the
// path manager object exists - providing a function pointer allows the
// interrupt code to check for this.  The function is also called by the
// path_manager cyclic handler (cluster_heartbeat() ).
//
void (*path_manager_cyclic_interface_funcp)
	(path_manager::cyclic_caller_t) = NULL;

path_manager *path_manager::the_path_manager = NULL;

//
//
// Called from:
//	1. The path manager cyclic handler, cluster_heartbeat (at lock level),
//	per the cyclic interval. (who == PM_CYCLIC_THRD)
//
//	2. hb_threadpool::send_heartbeat() in network interrupt context.
//	   If the cyclic has been delayed (for a time specified by the
//	   cyclic_delay_factor variable) then heartbeat processing is done.
//	   (who == NET_INTRPT_THRD).
//
// When the path manager is initialized, path_manager_cyclic_interface_funcp
// should be made to point to this routine. Interrupts can begin before the
// path manager object exists - providing a function pointer allows the
// interrupt code to check for this.
//
static void
path_manager_cyclic_interface(path_manager::cyclic_caller_t who)
{
	path_manager::the().pm_tick(who);
}

//
// The path manager constructor creates a Solaris cyclic to drive the
// pm_tick processing.  This function is specified as the handler to be
// invoked when the cyclic fires.
//
void
cluster_heartbeat(void)
{
	path_manager_cyclic_interface(path_manager::PM_CYCLIC_THRD);
}

//
// Constructor for the pm_if parent object.  The argument is the number the
// reference count is initialized to.
//
pm_if::pm_if(uint_t r) :
	refcnt(r)
{
}

//
// The path_record stores the fields associated with a given path (other
// than the inter-node heartbeat quantum, which is stored in the path_group
// structure.
//
path_record::path_record() :
	cpp(NULL),
	pep(NULL),
	tout(0),
	local_token(0),
	remote_token(0)
{
}

path_record::~path_record()
{
	ASSERT((cpp == NULL) && (pep == NULL) && (tout == 0));
	ASSERT((local_token == 0) && (remote_token == 0));
}

//
// Called when a pathend is registered.	 A hold on the pathend is taken and
// the local token value is initialized to the parameter starttime.
//
void
path_record::add_pathend(pm_if *pathendp, uint32_t starttime)
{
	ASSERT(pep == NULL);
	ASSERT(tout != 0);
	ASSERT(remote_token == 0);
	ASSERT(pathendp->path() == cpp);
	pep = pathendp;
	pep->hold();
	local_token = starttime;
	PM_DBG(("P(%p) PE(%p) added\n", cpp, pep));
}

//
// Called when a pathend is revoked, times-out, etc.  The hold on the
// pathend is released, and the token values are reset.
//
void
path_record::drop_pathend(pm_if::path_drop_reason dr)
{
	ASSERT(pep != NULL);
	pep->record_drop_info(dr);

#ifdef PERPE_PMHB_DBGBUF
	PM_DBG(("Pathend %s P(%p) PE(%p) HBDB(%p) dropped (reason %d)\n",
	    pep->get_extname(), cpp, pep, pep->get_hb_dbg(), dr));
#else
	PM_DBG(("Pathend %s P(%p) PE(%p) dropped (reason %d)\n",
	    pep->get_extname(), cpp, pep, dr));
#endif

#ifdef PM_FIRST_PATH_DROP_PANIC_SUPPORT
	if (pm_first_path_drop_panic &&
	    !(pm_ignore_path_drop_mask & (1U << (pep->node().ndid - 1)))) {
#ifdef _KERNEL
		debug_enter("Path dropped\n");
#endif
		os::panic("Path dropped\n");
	}
#endif

#ifdef FAULT_TRANSPORT
	void *fault_argp;
	uint32_t fault_argsize;
	uint64_t pathmask, rebootmask;
	char failstring[] = "Wrong path dropped, pathend = %p\n";

	// When this fault is activated, it will cause the machine to panic
	// if the path that is being dropped is to or from a node not
	// specified in the pathmask.  The pathmask is a 64 bit mask in which
	// each successive bit corresponds to a node (1->1, 2->2, 4->3, 8->4,
	// ...).  When the bit is set to 1, paths to or from that node are
	// allowed to fail.  The fault can be used from the arm_faults
	// program, or directly from tests.  The panic string is also
	// written into the debug buffer "for safe keeping."
	if (fault_triggered(FAULTNUM_PM_PANIC_IN_PATH_DOWN, &fault_argp,
	    &fault_argsize)) {
		ASSERT(fault_argsize == sizeof (uint64_t));
		rebootmask = *((uint64_t *)fault_argp);
		pathmask = (uint64_t)1 << (orb_conf::node_number() - 1);
		pathmask |= (uint64_t)1 << (pep->node().ndid - 1);
		if ((pathmask & rebootmask) == 0) {
			PM_DBG((failstring, pep));
			os::panic(failstring, pep);
		}
	}
#endif	// FAULT_TRANSPORT

	pep->rele();
	pep = NULL;
	local_token = 0;
	remote_token = 0;
}

//
// A path_group is a collection of path_records with a common inter-heartbeat
// quantum.  All of the path_records in a path_group are scheduled and
// processed together to improve spatial locality.
//
path_group::path_group(uint32_t q) :
	quantum(q),
	schedule_le(this),
	pg_le(this),
	timeleft(0)
{
}

path_group::~path_group()
{
	schedule_le.ptr_assert();
	pg_le.ptr_assert();
	ASSERT(timeleft == 0);
}

//
// Allocates/initializes the path_manager object and initializes the
// pm cyclic interface function pointer.
//
int
path_manager::initialize()
{
	int err;

	if ((err = hb_threadpool::initialize()) != 0) {
		return (err);
	}

	ASSERT(the_path_manager == NULL);
	the_path_manager = new path_manager(PM_CYCLIC_HZ);
	ASSERT(the_path_manager != NULL);

	//
	// send_heartbeat can call on the path manager if this function
	// pointer has been initialized.
	//
	path_manager_cyclic_interface_funcp = path_manager_cyclic_interface;

	// start the pm heartbeat cyclic
	path_manager::the().start_pm_cyclic();

	PM_DBG(("Path Manager (%p) initialized\n", the_path_manager));
	return (0);
}

//
// The constructor for the path_manager initializes variables and calculates
// conversion values.  Check the .h file for the meaning of these variables.
//
path_manager::path_manager(uint_t pm_cyclic_hz) :
	pm_cyclic(0),
	net_intr_run_time(0),
	net_intr_run_delta(0),
	net_intr_count(0),
	pm_cyclic_count(0),
	hrlast(0),
	schedule_slot(0),
	// We initialize the last_adapter_list_slot as if the current list
	// has no empty slots, which is one way of interpreting a null list.
	last_adapter_list_slot(PM_AD_GROUP_SIZE-1),
	current_token(0),
	max_token_div_2(UINT32_MAX/2),
	// Set the initial value large enough. It cannot be 0x7fffffffffffffff
	// because we multiply this by 2 while calculating the actual
	// max_clockthread_delay and that would cause this value to
	// become negative.
	max_clockthread_delay(INT64_MAX/4),
#ifdef REPEAT_TO_CATCHUP
	ncatchup(0),
#endif
	pm_cmmp(NULL),
#ifdef	USE_TICKSTATS
	pm_cyclic_stat("heartbeat", 10),
#endif
#ifdef	DEBUG
	pm_lbolt(0),
	pm_hrt(0),
#endif	// DEBUG
	timeouts_enabled(true)
{

	for (nodeid_t i = 0; i <= NODEID_MAX; i++) {
		mbrshp.members[i] = INCN_UNKNOWN;
		expected_mbrs.members[i] = INCN_UNKNOWN;
		logged_stale_mbrs.members[i] = INCN_UNKNOWN;
		unreach_mbrs.members[i] = INCN_UNKNOWN;
		add_client_mbrs.members[i] = INCN_UNKNOWN;
		node_timeouts[i] = 0;
		max_timeouts[i] = 0;
		alive[i] = false;
	}
	ASSERT(pm_cyclic_hz != 0);
	pmtlen = 1000 / pm_cyclic_hz;
	tokentlen = pmtlen / 4;
	if (tokentlen < 1)
		tokentlen = 1;
	hrtlen = drv_hztousec((clock_t)1) * 1000LL;
	ASSERT((pmtlen * HR_TICKS_PER_MS) >= hrtlen);


}

path_manager::~path_manager(void)
{
	PM_DBG(("Path Manager deleted\n"));
	ASSERT(transport_list.empty());
	ASSERT(other_list.empty());
	ASSERT(pg_list.empty());
#ifdef	DEBUG
	for (uint_t i = 0; i <= PM_SCHED_SIZE; i++) {
		// workaround for 4238081
		send_schedule[i].dispose();
		ASSERT(send_schedule[i].empty());
	}
#endif
	// workaround for 4238081
	long_schedule.dispose();
	ASSERT(long_schedule.empty());
	the_path_manager = NULL;
	ASSERT(pm_cmmp == NULL);
	ASSERT(hrlast == 0);
}

//
// start the pm heartbeat cyclic
//
void
path_manager::start_pm_cyclic()
{
	hrtime_t cyclic_interval;

// UNODE emulates the cyclic
#ifdef _KERNEL
	cyc_handler_t hdlr;
	cyc_time_t when;
#endif

	// path manager cyclic interval in nsec
	cyclic_interval = pmtlen * HR_TICKS_PER_MS;

// UNODE emulates the cyclic
#ifdef _KERNEL
	//
	// Register a cyclic handler with the Solaris cyclic subsystem
	// for processing pm ticks .
	//
	hdlr.cyh_func = (cyc_func_t)cluster_heartbeat;
	hdlr.cyh_level = CY_LOCK_LEVEL;
	hdlr.cyh_arg = NULL;

	when.cyt_when = 0;
	when.cyt_interval = cyclic_interval;

	mutex_enter(&cpu_lock);
	pm_cyclic = cyclic_add(&hdlr, &when);
	mutex_exit(&cpu_lock);
#endif

	// If the path manager cyclic is delayed, the network interrupt
	// thread will call for heartbeat processing.  The delay
	// threshold value is periodically re-established as the
	// current time (hrtime) plus this delta.
	// The assertion is needed to make sure that interrupts do not
	// compete with the path manager cyclic.  If the cyclic_delay_factor
	// is 2, then the threshold is based on a delay of 1 heartbeat
	// cyclic interval.
	ASSERT(cyclic_delay_factor > 1);
	net_intr_run_delta = (hrtime_t)cyclic_delay_factor * cyclic_interval;
}

void
path_manager::wait_for_cmm()
{
	sched_lock.lock();
	while (pm_cmmp == NULL) {
		PM_DBG(("wait_for_cmm\n"));
		pm_cmm_wait_cv.wait(&sched_lock);
	}
	sched_lock.unlock();
}

//
// disable() pertains to pathend heartbeat timeouts plus other transport
// timeouts (eg. ACK abort timeouts for the TCP transport). Heartbeat timeout
// monitoring is disabled directly here using the variable timeouts_enabled.
// Disabling of other transport timeouts is delegated to each specific
// transport.
// This is called primarily by cmm_ctl -d to support developer debugging.
// Nodes may enter kadb without resulting in panics due to path timeouts.
//
bool
path_manager::disable()
{
	pm_client *pmcp;

	timeouts_lock.lock(); // sychronize disable and enable
	bool oldval = timeouts_enabled;
	timeouts_enabled = false;

	//
	// if transitioning from enabled to disabled, delegate the
	// disabling of transport timeouts.
	//
	if (oldval) {
		block_lock.lock();
		pmclist_t::ListIterator iter(transport_list);
		for (; (pmcp = iter.get_current()) != NULL; iter.advance()) {
			pmcp->timeouts_disable();
		}
		block_lock.unlock();
	}
	timeouts_lock.unlock();

	PM_DBG(("Path Manager monitoring disabled\n"));

	return (oldval);
}

//
// Re-enable heartbeat monitoring and transport timeouts.
// See path_manager::disable above.
//
bool
path_manager::enable()
{
	pm_client *pmcp;

	timeouts_lock.lock(); // synchronize disable and enable
	bool oldval = timeouts_enabled;
	timeouts_enabled = true;

	//
	// if transitioning from disabled to enabled, delegate the
	// enabling of path endpoint timeouts.
	//
	if (!oldval) {
		block_lock.lock();
		pmclist_t::ListIterator iter(transport_list);
		for (; (pmcp = iter.get_current()) != NULL; iter.advance()) {
			(pmcp->timeouts_enable)();
		}
		block_lock.unlock();
	}
	timeouts_lock.unlock();

	PM_DBG(("Path Manager monitoring enabled\n"));
	return (!oldval);
}

bool
path_manager::is_enabled()
{
	return (timeouts_enabled);
}

//
// Returns the largest timeout on any path to the specified node in msecs.
//
uint32_t
path_manager::get_max_delay(nodeid_t nodeid)
{
	unreach_sched_lock.lock();
	uint32_t t = max_timeouts[nodeid];
	unreach_sched_lock.unlock();
	return (t);
}


//
// shutdown() informs all pm_clients that the system is shutting down
// (which should only happen after all the paths and adapters have been
// removed) and reaps them from the lists.  It also releases its reference
// to the cmm (which it was using to deliver node_is_reachable and
// node_is_unreachable.  Finally, the object itself is deleted.
//
void
path_manager::shutdown()
{
	PM_DBG(("Path Manager shutdown\n"));
	pm_client *pmcp;

	// Stop the network interrupt from calling the heartbeat interface
	path_manager_cyclic_interface_funcp = NULL;

// UNODE emulates the cyclic
#ifdef _KERNEL
	//
	// Unregister with the Solaris cyclic subsystem
	//
	if (pm_cyclic != CYCLIC_NONE) {
		mutex_enter(&cpu_lock);
		cyclic_remove(pm_cyclic);
		mutex_exit(&cpu_lock);
		pm_cyclic = CYCLIC_NONE;
	}
#endif


	hrlast = 0;	// reset hrlast
	block_lock.lock();
	mem_lock.lock();
	sched_lock.lock();

	while ((pmcp = other_list.reapfirst()) != NULL)
		pmcp->shutdown();

	while ((pmcp = transport_list.reapfirst()) != NULL)
		pmcp->shutdown();

	sched_lock.unlock();
	mem_lock.unlock();
	block_lock.unlock();

	pm_cmmp->dec_ref(DONT_EXIT);
	pm_cmmp = NULL;

	// shutdown the hb_threadpool
	hb_threadpool::the().shutdown();

	delete this;
}

//
// Add a pm_client of to the appropriate list (specified by ct).
//
void
path_manager::add_client(client_type ct, pm_client *pmcp)
{
	adapter_group *agp;
	path_group *pgp;
	clconf_adapter_t *cl_adp;
	uint_t i;

	PM_DBG(("Path Manager add client (%p) type %d\n", pmcp, ct));

	ASSERT(pmcp != NULL);

	clconf_cluster_t *cl = clconf_cluster_get_current();
	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl, CL_NODE,
	    (int)orb_conf::node_number());

	block_lock.lock();
	mem_lock.lock();

	IntrList<adapter_group, _SList>::ListIterator ali(ag_list);
	while ((agp = ali.get_current()) != NULL) {
		for (i = 0; i < PM_AD_GROUP_SIZE; i++) {
			if (agp->adapter_id[i] == CLCONF_ID_UNKNOWN)
				continue;
			cl_adp = (clconf_adapter_t *)clconf_obj_get_child(nd,
			    CL_ADAPTER, agp->adapter_id[i]);
			// XXX this isn't safe, the clconf tree could have
			// been replaced. The alternative is to put a hold
			// on the adapter object, which wastes memory because
			// it keeps every clconf tree that adds an adapter
			// in memory.  Prefers the race over the wasted
			// memory for now.
			ASSERT(cl_adp != NULL);
			(void) pmcp->add_adapter(cl_adp);
		}
		ali.advance();
	}

	clconf_obj_release((clconf_obj_t *)cl);

	// Add each path
	IntrList<path_group, _SList>::ListIterator pli(pg_list);
	while ((pgp = pli.get_current()) != NULL) {
		for (i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp == NULL)
				continue;
			(void) pmcp->add_path(pgp->pra[i].cpp);
		}
		pli.advance();
	}

	sched_lock.lock();

	// Under the sched_lock, make a path_up call for each path with
	// a registered pathend
	pli.reinit(pg_list);
	while ((pgp = pli.get_current()) != NULL) {
		for (i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp == NULL)
				continue;
			if (pgp->pra[i].pep != NULL)
				(void) pmcp->path_up(pgp->pra[i].cpp);
		}
		pli.advance();
	}

	switch (ct) {
	case (PM_TRANSPORT):
		transport_list.append(pmcp->get_pmlist_elem());
		break;
	case (PM_OTHER):
		other_list.append(pmcp->get_pmlist_elem());
		break;
	default:
		//
		// SCMSGS
		// @explanation
		// The system attempted to add a client of unknown type to the
		// set of path manager clients.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Invalid path_manager client_type (%d)", ct);
	}
	sched_lock.unlock();

	// Use the add_client_mbrs membership to call node_alive for all
	// current members of the cluster
	for (nodeid_t n = 1; n <= NODEID_MAX; n++)
		if (add_client_mbrs.members[n] != INCN_UNKNOWN)
			(void) pmcp->node_alive(n, add_client_mbrs.members[n]);

	mem_lock.unlock();
	block_lock.unlock();
}

//
// Remove a client (again from the appropriate list).
//
void
path_manager::remove_client(client_type ct, pm_client *pmcp)
{
/*
	IntrList<path_group, _SList>::ListIterator pli;
	adapter_group *agp;
	path_group *pgp;
	clconf_adapter_t *cl_adp;
	clconf_path_t *clpt;
	int *i_p;
	uint_t i;
*/
	PM_DBG(("Path Manager remove client (%p) type %d\n", pmcp, ct));
	block_lock.lock();
	mem_lock.lock();
	sched_lock.lock();
	ASSERT(pmcp != NULL);
	switch (ct) {
	case (PM_TRANSPORT):
		ASSERT(transport_list.exists(pmcp->get_pmlist_elem()));
		(void) transport_list.erase(pmcp->get_pmlist_elem());
		break;
	case (PM_OTHER):
		ASSERT(other_list.exists(pmcp->get_pmlist_elem()));
		(void) other_list.erase(pmcp->get_pmlist_elem());
/*
		// Just as in the normal path_down case, we declare
		// up paths down when removing the client
		pli.reinit(pg_list);
		while ((pgp = pli.get_current()) != NULL) {
			for (i = 0; i < PM_PA_GROUP_SIZE; i++) {
				if (pgp->pra[i].cpp == NULL)
					continue;
				if (pgp->pra[i].pep != NULL)
					(void) pmcp->path_down(pgp->pra[i].cpp);
			}
			pli.advance();
		}
*/
		break;
	default:
		os::panic("Invalid path_manager client_type (%d)", ct);
	}
	sched_lock.unlock();
/*
	// Now, under the mem_lock, we make lists of the paths and adapters
	// to be removed.  Then we can drop the mem_lock and remove them.
	// We are guaranteed that the paths won't disappear, because we
	// still hold the block_lock.  We make copies of the adapter ids.
	// and get their clconf equivalents after dropping the mem_lock.

	SList<clconf_path_t> clplist;

	pli.reinit(pg_list);
	while ((pgp = pli.get_current()) != NULL) {
		for (i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp == NULL)
				continue;
			clplist.prepend(pgp->pra[i].cpp);
		}
		pli.advance();
	}

	SList<int> ad_idlist;

	IntrList<adapter_group, _SList>::ListIterator ali(ag_list);
	while ((agp = ali.get_current()) != NULL) {
		for (i = 0; i < PM_AD_GROUP_SIZE; i++) {
			if (agp->adapter_id[i] == CLCONF_ID_UNKNOWN)
				continue;
			i_p = new int(agp->adapter_id[i]);
			ad_idlist.prepend(i_p);
		}
		ali.advance();
	}
*/
	mem_lock.unlock();
/*
	// Note that this protocol will only work if remove_path guarantees
	// that all messages are pushed to the orb layer before it returns.
	// Otherwise, a node_died following a remove_client could miss
	// messages that need to be pushed.  The transport endpoint pushes
	// messages before releasing the pathend, so this protocol is safe.

	while ((clpt = clplist.reapfirst()) != NULL)
		(void) pmcp->remove_path(clpt);

	clconf_cluster_t *cl = clconf_cluster_get_current();
	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl, CL_NODE,
	    (int)orb_conf::node_number());

	while ((i_p = ad_idlist.reapfirst()) != NULL) {
		cl_adp = (clconf_adapter_t *)clconf_obj_get_child(nd,
		    CL_ADAPTER, *i_p);
		(void) pmcp->remove_adapter(cl_adp);
		delete i_p;
	}

	clconf_obj_release((clconf_obj_t *)cl);
*/
	block_lock.unlock();
}

// List seems to miss indirection through a method function pointer as a use
// of a variable, so we turn off that error message for the generic pm_client
// call functions.
//lint -e550

//
// node_call, path_call, and adapter_call all call pmcfn on each of the
// pm_clients in the pmclist_t pmcl with the supplied arguments.
//
bool
path_manager::node_call(pmclist_t &pmcl, pm_node_func pmcfn, nodeid_t nodeid,
    incarnation_num incn)
{
	pm_client *pmcp;

	pmclist_t::ListIterator iter(pmcl);
	for (; (pmcp = iter.get_current()) != NULL; iter.advance())
		(void) (pmcp->*pmcfn)(nodeid, incn);

	return (true);
}

bool
path_manager::path_call(pmclist_t &pmcl, pm_path_func pmcfn,
    clconf_path_t *clconf_pathp)
{
	pm_client *pmcp;

	pmclist_t::ListIterator iter(pmcl);
	for (; (pmcp = iter.get_current()) != NULL; iter.advance())
		(void) (pmcp->*pmcfn)(clconf_pathp);

	return (true);
}

bool
path_manager::adapter_call(pmclist_t &pmcl, pm_adapter_func pmcfn,
    clconf_adapter_t *clconf_adapterp)
{
	pm_client *pmcp;

	pmclist_t::ListIterator iter(pmcl);
	for (; (pmcp = iter.get_current()) != NULL; iter.advance())
		(void) (pmcp->*pmcfn)(clconf_adapterp);

	return (true);
}

//lint +e550

//
// Add the supplied adapter. Uses the mem_lock to ensure that the
// pm_client lists don't change.
//
bool
path_manager::add_adapter(clconf_adapter_t *clconf_adapterp)
{
	adapter_group *agp;
	int ad_id(clconf_obj_get_id((clconf_obj_t *)clconf_adapterp));

	PM_DBG(("Adding A(%p)\n", clconf_adapterp));
	mem_lock.lock();

	// Because of the way we remove ids, any free slots are
	// guaranteed to be in the first group.

	IntrList<adapter_group, _SList>::ListIterator li(ag_list);

	if (last_adapter_list_slot == (PM_AD_GROUP_SIZE - 1)) {
		// When the last_adapter_list_slot is the last slot in the
		// adapter_group, we need to allocate a new group.
		ASSERT(((agp = li.get_current()) == NULL) ||
		    (agp->adapter_id[last_adapter_list_slot] !=
		    CLCONF_ID_UNKNOWN));
		agp = new adapter_group();
		ASSERT(agp != NULL);
		agp->adapter_id[0] = ad_id;
		ag_list.prepend(agp);
		last_adapter_list_slot = 0;
	} else {
		agp = li.get_current();
		ASSERT(agp != NULL);
		ASSERT(last_adapter_list_slot < (PM_AD_GROUP_SIZE - 1) &&
		    (agp->adapter_id[last_adapter_list_slot+1] ==
		    CLCONF_ID_UNKNOWN));
		last_adapter_list_slot++;
		agp->adapter_id[last_adapter_list_slot] = ad_id;
	}

	(void) adapter_call(transport_list, &pm_client::add_adapter,
	    clconf_adapterp);
	(void) adapter_call(other_list, &pm_client::add_adapter,
	    clconf_adapterp);
	mem_lock.unlock();
	return (true);
}

bool
path_manager::change_adapter(clconf_adapter_t *clconf_adapterp)
{
	mem_lock.lock();
	PM_DBG(("Calling pmclients for adapter property change\n"));
	(void) adapter_call(transport_list, &pm_client::change_adapter,
	    clconf_adapterp);
	(void) adapter_call(other_list, &pm_client::change_adapter,
	    clconf_adapterp);
	mem_lock.unlock();
	return (true);
}

//
// Remove the supplied adapter.  Called under the block_lock to ensure that the
// pm_client lists don't change.
//
bool
path_manager::remove_adapter(clconf_adapter_t *clconf_adapterp)
{
	uint_t i = 0;
	bool slot_found = false;
	adapter_group *agp = NULL, *fagp;
	int ad_id(clconf_obj_get_id((clconf_obj_t *)clconf_adapterp));

	PM_DBG(("Removing A(%p)\n", clconf_adapterp));
	block_lock.lock();
	mem_lock.lock();

	// Find the slot the adapter id is stored in while keeping track
	// of the first group (where we will find an adapter_id to swap into
	// the hole left by the removed adapter_id).  When we find the flag,
	// move the last entry in the first group into the slot we're freeing.
	// If the resulting first group is empty, delete it.

	IntrList<adapter_group, _SList>::ListIterator li(ag_list);
	fagp = li.get_current();
	while (!slot_found && ((agp = li.get_current()) != NULL)) {
		for (i = 0; i < PM_AD_GROUP_SIZE; i++) {
			if (agp->adapter_id[i] != ad_id)
				continue;
			slot_found = true;
			break;
		}
		li.advance();
	}

	ASSERT(slot_found);

	if ((last_adapter_list_slot == 0) && (agp == fagp)) {
		// If this is the last adapter group and the last adapter
		// id in the group, or it just happens that we're deleting
		// the only filled slot in the first adapter group (which is
		// the only group with free slots), we just delete the
		// group and reset the last_adapter_list_slot to its
		// initialization value.
		ASSERT(i == 0);
		agp->adapter_id[i] = CLCONF_ID_UNKNOWN;
		last_adapter_list_slot = PM_AD_GROUP_SIZE - 1;
		ASSERT(ag_list.exists(agp));
		(void) ag_list.erase(agp);
		delete agp;
	} else if (last_adapter_list_slot == 0) {
		// The head group on the list (which is always the only
		// group with free slots) has only one id, so it must be
		// deleted)
		ASSERT((agp != fagp) &&
		    (fagp->adapter_id[0] != CLCONF_ID_UNKNOWN) &&
		    (PM_AD_GROUP_SIZE == 1) ||
		    ((fagp->adapter_id[1] == CLCONF_ID_UNKNOWN)));
		ASSERT(i < PM_AD_GROUP_SIZE);
		agp->adapter_id[i] = fagp->adapter_id[0];
		fagp->adapter_id[0] = CLCONF_ID_UNKNOWN;
		ASSERT(ag_list.exists(fagp));
		(void) ag_list.erase(fagp);
		delete fagp;
		last_adapter_list_slot = PM_AD_GROUP_SIZE - 1;
	} else {
		ASSERT(last_adapter_list_slot != 0);
		ASSERT(fagp->adapter_id[last_adapter_list_slot] !=
		    CLCONF_ID_UNKNOWN);
		ASSERT(i < PM_AD_GROUP_SIZE);
		agp->adapter_id[i] = fagp->adapter_id[last_adapter_list_slot];
		fagp->adapter_id[last_adapter_list_slot] = CLCONF_ID_UNKNOWN;
		last_adapter_list_slot--;
	}

	mem_lock.unlock();

	(void) adapter_call(transport_list, &pm_client::remove_adapter,
	    clconf_adapterp);
	(void) adapter_call(other_list, &pm_client::remove_adapter,
	    clconf_adapterp);
	block_lock.unlock();
	return (true);
}

// Return the minimum timeout (in nanoseconds) of all paths to all nodes
// excluding the path that is passed in.
// Will return UINT_MAX * HR_TICKS_PER_MS if there are no paths at all.
hrtime_t
path_manager::get_min_timeout(clconf_path_t *p)
{
	uint32_t	min_timeout = UINT_MAX;
	uint32_t	tout;
	IntrList<path_group, _SList>::ListIterator li(pg_list);
	path_group *pgp;

	ASSERT(mem_lock.lock_held() || sched_lock.lock_held());

	for (; (pgp = li.get_current()) != NULL; li.advance()) {
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if ((pgp->pra[i].cpp == NULL) ||
			    (pgp->pra[i].cpp == p)) {
				continue;
			}
			ASSERT(pgp->pra[i].tout != 0);
			tout = clconf_path_get_timeout(pgp->pra[i].cpp);
			if (tout < min_timeout)
				min_timeout = tout;
		}
	}

	PM_DBG(("new minimum timeout is %u ms\n", min_timeout));
	return ((hrtime_t)(min_timeout * HR_TICKS_PER_MS));
}

// Return the maximum timeout  (in milliseconds) to a particular node,
// over  all existing  paths to  that  node but not "p". Note that the
// individual timeouts are computed as tout = t + 2 * q in determining
// the maximum timeout.
// We need to add 2 * quantum to the account for quantum granularity
// of scheduling on both the local and remote node.
// Will return 0 if there are no paths to that node.

uint32_t
path_manager::get_max_timeout(clconf_path_t *p, nodeid_t nodeid)
{
	uint32_t	max_timeout = 0;
	uint32_t	tout, t, qt;
	IntrList<path_group, _SList>::ListIterator li(pg_list);
	path_group *pgp;

	// Holding mem_lock or sched_lock is sufficient.
	ASSERT(mem_lock.lock_held() || sched_lock.lock_held());

	for (; (pgp = li.get_current()) != NULL; li.advance()) {
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if ((pgp->pra[i].cpp == NULL) ||
			    (pgp->pra[i].cpp == p)) {
				continue;
			}
			nodeid_t ndid =
				clconf_path_get_remote_nodeid(pgp->pra[i].cpp);
			if (ndid != nodeid) {
				continue;
			}
			ASSERT(pgp->pra[i].tout != 0);
			t = clconf_path_get_timeout(pgp->pra[i].cpp);
			qt = clconf_path_get_quantum(pgp->pra[i].cpp);
			tout = t + 2 * qt;
			if (tout > max_timeout) {
				max_timeout = tout;
			}
		}
	}

	PM_DBG(("new maximum tout to node %d is %d\n", nodeid, max_timeout));
	return (max_timeout);
}

bool
path_manager::is_path_up(clconf_path_t *clconf_pathp)
{
	IntrList<path_group, _SList>::ListIterator li(pg_list);
	path_group *pgp;

	// Holding mem_lock or sched_lock is sufficient.
	ASSERT(mem_lock.lock_held() || sched_lock.lock_held());

	for (; (pgp = li.get_current()) != NULL; li.advance()) {
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp == NULL)
				continue;
			if (pgp->pra[i].cpp == clconf_pathp) {
				if ((pgp->pra[i].pep != NULL) &&
					(pgp->pra[i].pep->is_pe_up())) {
					return (true);
				} else {
					return (false);
				}
			}
		}
	}

	return (false);
}

path_record *
path_manager::create_pathrec(clconf_path_t *clconf_pathp, uint32_t q,
    uint32_t t)
{
	bool slot_found = false;
	path_group *pgp;
	path_record *prec = NULL;

	// Try to find a spare path_record with a matching quantum.  If there
	// is one, use it.
	IntrList<path_group, _SList>::ListIterator li(pg_list);
	while (!slot_found && ((pgp = li.get_current()) != NULL)) {
		if (q != pgp->quantum) {
			li.advance();
			continue;
		}
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp != NULL)
				continue;
			ASSERT(pgp->pra[i].tout == 0);
			ASSERT(pgp->pra[i].pep == NULL);
			slot_found = true;
			sched_lock.lock();
			pgp->pra[i].cpp = clconf_pathp;
			pgp->pra[i].tout = t;
			prec = &pgp->pra[i];
			sched_lock.unlock();
			break;
		}
		li.advance();
	}

	// If a slot wasn't found, allocate a new path_group and then use the
	// first record.
	if (!slot_found) {
		pgp = new path_group(q);
		ASSERT(pgp != NULL);
		pgp->pra[0].cpp = clconf_pathp;
		pgp->pra[0].tout = t;
		prec = &pgp->pra[0];
		// We need to prevent bunchings of path_groups in the
		// scheduling array, so we do a search on all the
		// potential slots within the next quantum to figure out
		// where to place it.  (In addition to placing it in the
		// global pg_list.)
		uint_t group_count = UINT_MAX;
		uint_t slot_num = schedule_slot;
		uint_t tgroupc;
		sched_lock.lock();
		pg_list.prepend(&(pgp->pg_le));
		for (uint_t i = 0; i < q; i++) {
			tgroupc = send_schedule[(i + schedule_slot) %
			    PM_SCHED_SIZE].count();
			if (tgroupc < group_count) {
				group_count = tgroupc;
				slot_num = (i + schedule_slot) % PM_SCHED_SIZE;
			}
			if (tgroupc == 0)
				break;
		}
		send_schedule[slot_num].prepend(&(pgp->schedule_le));
		sched_lock.unlock();
	}
	return (prec);
}

// Add a path using an existing but unused path_record or allocating a new
// path_group if necessary.
bool
path_manager::add_path(clconf_path_t *clconf_pathp)
{
	PM_DBG(("Adding P(%p)\n", clconf_pathp));

	uint32_t q	= clconf_path_get_quantum(clconf_pathp);
	uint32_t t	= clconf_path_get_timeout(clconf_pathp);

	mem_lock.lock();

	// The original timeout is stored in milliseconds, but the timeout
	// stored in the path_group is in the same units as the token passed
	// between machines.
	t = msectotoken(t);
	q = msectopmt(q);

	(void) create_pathrec(clconf_pathp, q, t);

	(void) path_call(transport_list, &pm_client::add_path, clconf_pathp);
	(void) path_call(other_list, &pm_client::add_path, clconf_pathp);
	mem_lock.unlock();
	return (true);
}

//
// A utility function to drop the pathend corresponding to a given path_record.
// The timeout time of the remote node is updated, and drop_pathend is called
// on the path_record.
//
void
path_manager::drop_pathend(path_record &pr, pm_if::path_drop_reason dr)
{
	hrtime_t node_to;
	uint32_t tout;

	ASSERT(sched_lock.lock_held());
	ASSERT(pr.cpp != NULL);
	ASSERT(pr.pep != NULL);

	// Add a quantum to the timeout to account for scheduling slop on
	// the remote node.
	tout = clconf_path_get_timeout(pr.cpp) +
		    clconf_path_get_quantum(pr.cpp);
	node_to = os::gethrtime() + HR_TICKS_PER_MS * (hrtime_t)tout;

	unreach_sched_lock.lock();
	if (node_to > node_timeouts[pr.pep->node().ndid])
		node_timeouts[pr.pep->node().ndid] = node_to;
	unreach_sched_lock.unlock();

	pr.drop_pathend(dr);
}

//
// remove_path frees the path_record associated with the supplied path, and
// deletes the path_group if it has no active records.
//
bool
path_manager::remove_path(clconf_path_t *clconf_pathp)
{
	PM_DBG(("Removing P(%p)\n", clconf_pathp));
	bool group_empty = true;
	bool path_found = false;
	uint32_t q;
	path_group *pgp;

	q = msectopmt(clconf_path_get_quantum(clconf_pathp));

	block_lock.lock();
	mem_lock.lock();

	IntrList<path_group, _SList>::ListIterator li(pg_list);
	while (!path_found && ((pgp = li.get_current()) != NULL)) {
		if (q != pgp->quantum) {
			li.advance();
			continue;
		}
		group_empty = true;
		sched_lock.lock();
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if ((pgp->pra[i].cpp != NULL) &&
			    (pgp->pra[i].cpp != clconf_pathp))
				group_empty = false;
			if (pgp->pra[i].cpp != clconf_pathp)
				continue;
			ASSERT(pgp->pra[i].tout != 0);
			path_found = true;
			if (pgp->pra[i].pep != NULL) {
				drop_pathend(pgp->pra[i], pm_if::PDR_REMOVED);
				ASSERT(pgp->pra[i].pep == NULL);
				(void) path_call(other_list,
				    &pm_client::path_down, pgp->pra[i].cpp);
			}
			ASSERT(pgp->pra[i].pep == NULL);
			pgp->pra[i].cpp = NULL;
			pgp->pra[i].tout = 0;
		}

		if (group_empty) {
			ASSERT(pg_list.exists(&(pgp->pg_le)));
			(void) pg_list.erase(&(pgp->pg_le));
		}
		sched_lock.unlock();
		li.advance();
	}

	mem_lock.unlock();

	(void) path_call(transport_list, &pm_client::remove_path, clconf_pathp);
	(void) path_call(other_list, &pm_client::remove_path, clconf_pathp);

	block_lock.unlock();
	return (true);
}

// max_timeouts[]  stores t  + 2  * q.  The quantum  is  added because
// pm_tick() checks the  path only when its quantum  expires, so there
// could   be   a  full   quantum   delay   between   the  time   when
// node_is_unreachable() bumps  the unreach membership,  and pm_tick()
// brings the  path down.  The factor of  2 comes in for  the slack on
// the remote node.

void
path_manager::update_maxtimeouts(clconf_path_t *clconf_pathp, uint32_t newt,
    uint32_t newq)
{
	uint32_t 	t = clconf_path_get_timeout(clconf_pathp);
	uint32_t	q = clconf_path_get_quantum(clconf_pathp);
	nodeid_t	nodeid = clconf_path_get_remote_nodeid(clconf_pathp);
	hrtime_t 	t_hr;
	uint32_t	oldtout, newtout;

	ASSERT(sched_lock.lock_held());
	unreach_sched_lock.lock();
	// This condition is true when path goes down. Here we need to
	// find out the new max_clockthread_delay (which may have increased
	// after a path has gone down). We also need to find out if the
	// max_timeouts have changed. Note that we need to find these
	// values from all the paths except the one that has gone down.
	// get_min_timeout and get_max_timeout essentially give the min
	// and max timeout values for all paths except the one that has
	// gone down.
	if ((newt == 0) && (newq == 0))  {
		t_hr = HR_TICKS_PER_MS * (hrtime_t)t;
		if (lessthanorequal_max_clockthread_delay(t_hr)) {
			hrtime_t t_min = get_min_timeout(clconf_pathp);
			update_max_clockthread_delay(t_min);
		}
		oldtout = t + 2 * q;
		ASSERT(oldtout <= max_timeouts[nodeid]);

		if (oldtout == max_timeouts[nodeid]) {
			uint32_t new_max_timeout =
			    get_max_timeout(clconf_pathp, nodeid);
			if (new_max_timeout != 0) {
				PM_DBG(("max_timeouts[%d] has changed"
					" from %d to %d.\n", nodeid,
					max_timeouts[nodeid],
					new_max_timeout));
				max_timeouts[nodeid] = new_max_timeout;
			}
		}
	} else if ((newq == q) && (newt == t)) {
		// This condition is true when a path comes up. In this
		// case, because of the new path, we might end up increasing
		// the max_timeout value since this path has a higher timeout
		// We might also end up reducing the max_clockthread_delay
		// value.
		newtout = newt + 2 * newq;
		if (newtout > max_timeouts[nodeid]) {
			PM_DBG(("Increasing max_timeouts[%d] from %d to %d\n",
			    nodeid, max_timeouts[nodeid], newtout));
			max_timeouts[nodeid] = newtout;
		}
		t_hr = HR_TICKS_PER_MS * (hrtime_t)newt;
		if (lessthan_max_clockthread_delay(t_hr))
			update_max_clockthread_delay(t_hr);

	} else {
		// This is the case when the timeout and quantum parameters
		// of the path are being changed (using change_path). In this
		// case, there are 2 situations. The path could be up or down.
		// If the path is down, we don't need to make any updates
		// since they would have already been done when the path
		// went down.
		// If the path is up, we first need to find out if the
		// old timeout value of the path is the same as the
		// max_timeout. If so, we first determine the max_timeout for
		// all paths other than the one that just came up.
		// Then, if the path's new timeout value is greater than
		// the max_timeout determined above, we need to update
		// the max_timeout.
		// We also need to update the max_clockthread_delay if
		// the path is up. If the path's new timeout value is
		// less than the max_clockthread_delay, we should update
		// the max_clockthread_delay.
		if (is_path_up(clconf_pathp)) {
			oldtout = t + 2 * q;
			newtout = newt + 2 * newq;
			// If the old tout of the path for which
			// the parameters are being changed is
			// the same as the max timeout, we need
			// to find out the new max timeout for
			// all paths other than this path.
			if (oldtout == max_timeouts[nodeid]) {
				uint32_t new_max_timeout =
					get_max_timeout(clconf_pathp, nodeid);
				PM_DBG(("Changing max_timeouts[%d] "
					"from %d to %d\n",
					nodeid, max_timeouts[nodeid],
					new_max_timeout));
				max_timeouts[nodeid] = new_max_timeout;
			}
			// The max_timeouts[nodeid] should be
			// further updated if necessary to new tout
			// of this path.
			if (newtout > max_timeouts[nodeid]) {
				PM_DBG(("Path is up. Increasing "
					"max_timeouts[%d] from %d to %d\n",
					nodeid, max_timeouts[nodeid],
					newtout));
				max_timeouts[nodeid] = newtout;
			}
			// Also, the max_clockthread_delay may need to be
			// updated.
			t_hr = HR_TICKS_PER_MS * (hrtime_t)newt;
			if (lessthan_max_clockthread_delay(t_hr))
				update_max_clockthread_delay(t_hr);
		}
	}
	unreach_sched_lock.unlock();
}

//
// Changes the clconf_pathp's heartbeat/quantum to newpathp's values.
//
bool
path_manager::change_path(clconf_path_t *clconf_pathp, clconf_path_t *newpathp)
{
	bool group_empty = true;
	bool path_found = false;
	uint32_t q, t, oldq;
	path_group *pgp;
	path_record *prec;
	uint32_t new_tmout, new_qtm, old_qtm;

	block_lock.lock();
	mem_lock.lock();

	PM_DBG(("Changing P(%p) newpathp=%p\n", clconf_pathp, newpathp));

	old_qtm = clconf_path_get_quantum(clconf_pathp);
	oldq = msectopmt(old_qtm);
	new_tmout = clconf_path_get_timeout(newpathp);
	new_qtm = clconf_path_get_quantum(newpathp);
	t = msectotoken(new_tmout);
	q = msectopmt(new_qtm);

	// If quantum did not change, then it is easy. Just find the path
	// record in the quantum group and update its timeout value.
	// update the maxtimeout values too.
	if (oldq == q) {
		IntrList<path_group, _SList>::ListIterator li(pg_list);
		while (!path_found && ((pgp = li.get_current()) != NULL)) {
		    if (oldq != pgp->quantum) {
			li.advance();
			continue;
		    }
		    for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp != clconf_pathp)
				continue;
			ASSERT(pgp->pra[i].tout != 0);
			sched_lock.lock();
			pgp->pra[i].tout = t;
			update_maxtimeouts(clconf_pathp, new_tmout, new_qtm);
			sched_lock.unlock();
			path_found = true;
		    }
		    li.advance();
		}
		ASSERT(path_found);
	} else {
		// If quantum changed, first find the path group which has
		// the path record for the path and remove it from the
		// old group.
		pm_if	*pep = NULL;
		uint32_t lt = 0, rt = 0;

		IntrList<path_group, _SList>::ListIterator li(pg_list);
		while (!path_found && ((pgp = li.get_current()) != NULL)) {
		    if (oldq != pgp->quantum) {
			li.advance();
			continue;
		    }
		    group_empty = true;
		    sched_lock.lock();
		    for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if ((pgp->pra[i].cpp != NULL) &&
			    (pgp->pra[i].cpp != clconf_pathp))
				group_empty = false;
			if (pgp->pra[i].cpp != clconf_pathp)
				continue;
			path_found = true;
			pep = pgp->pra[i].pep;
			lt = pgp->pra[i].local_token;
			rt = pgp->pra[i].remote_token;
			pgp->pra[i].cpp = NULL;
			pgp->pra[i].tout = 0;
			pgp->pra[i].pep = NULL;
			pgp->pra[i].local_token = 0;
			pgp->pra[i].remote_token = 0;
		    }
		    if (group_empty) {
			ASSERT(pg_list.exists(&(pgp->pg_le)));
			(void) pg_list.erase(&(pgp->pg_le));
		    }
		    sched_lock.unlock();
		    li.advance();
		}
		ASSERT(path_found);
		// path record pointer will be valid beacuse we are
		// holding mem_lock.
		// After it is removed from the old path group, create
		// a new path record with new quantum. This may result
		// in creating a new path group too.
		// Update maxtimeout values accordingly.
		prec = create_pathrec(clconf_pathp, q, t);
		sched_lock.lock();
		prec->pep = pep;
		update_maxtimeouts(clconf_pathp, new_tmout, new_qtm);
		prec->local_token = lt;
		prec->remote_token = rt;
		sched_lock.unlock();
	}
	clconf_path_set_timeout(clconf_pathp, new_tmout);
	clconf_path_set_quantum(clconf_pathp, new_qtm);

	mem_lock.unlock();

	/*
	 * Currently transports and other pm clients are not interested
	 * in the dynamically changeable path properties.
	 * Also, path_call(), as it exists today cannot be used to
	 * call transports and other pm clients for change path because
	 * change path takes two arguments.
	 * path_call will have to be changed if we need to call transports and
	 * other pm clients.
	 */
	block_lock.unlock();
	return (true);
}

//
// node_alive membership is recorded in add_client_mbrs, and passed
// through to the clients
//
bool
path_manager::node_alive(nodeid_t nodeid, incarnation_num incn)
{
	mem_lock.lock();
	add_client_mbrs.members[nodeid] = incn;
	(void) node_call(transport_list, &pm_client::node_alive, nodeid, incn);
	(void) node_call(other_list, &pm_client::node_alive, nodeid, incn);
	mem_lock.unlock();
	return (true);
}

//
// disconnect_node is called when all paths to a remote node have been lost
// or when the cmm declares a remote node dead (prior to the call to
// node_died).  It automatically unregisters all pathends corresponding to
// that remote incarnation.  The local membership and the alive array keep
// track of whether disconnect_node has already been called for this nodeid
// and incn.
//
bool
path_manager::disconnect_node(nodeid_t nodeid, incarnation_num incn)
{
	bool paths = false;
	ASSERT(sched_lock.lock_held());

	if ((mbrshp.members[nodeid] == incn) && alive[nodeid])
		alive[nodeid] = false;
	else
		return (false);


	PM_DBG(("disconnect_node (%u,%u)\n", nodeid, incn));

	path_group *pgp;
	IntrList<path_group, _SList>::ListIterator li(pg_list);
	while ((pgp = li.get_current()) != NULL) {
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp != NULL) {
				if (pgp->pra[i].pep == NULL)
					continue;
				if (pgp->pra[i].pep->node().ndid == nodeid) {
					ASSERT(pgp->pra[i].pep->node().incn ==
					    incn);
					drop_pathend(pgp->pra[i],
					    pm_if::PDR_DISCONNECTED);
					paths = true;
				}
			}
		}
		li.advance();
	}

	if (paths) {
		(void) node_call(transport_list, &pm_client::disconnect_node,
		    nodeid, incn);
		(void) node_call(other_list, &pm_client::disconnect_node,
		    nodeid, incn);
		PM_DBG(("disconnect_node (%u,%u) passed\n", nodeid, incn));
	}

	return (paths);
}

//
// node_died first calls disconnect_node, then node_died on all the clients
//
bool
path_manager::node_died(nodeid_t nodeid, incarnation_num incn)
{
	block_lock.lock();
	sched_lock.lock();
	(void) disconnect_node(nodeid, incn);
	sched_lock.unlock();

	mem_lock.lock();
	add_client_mbrs.members[nodeid] = INCN_UNKNOWN;
	mem_lock.unlock();

	PM_DBG(("node_died (%u,%u)\n", nodeid, incn));
	(void) node_call(transport_list, &pm_client::node_died, nodeid, incn);
	(void) node_call(other_list, &pm_client::node_died, nodeid, incn);

	block_lock.unlock();
	return (true);
}

//
// path_up and path_down are generated internally to the path_manager.  Each
// calls the corresponding function on the pm_clients.
bool
path_manager::path_up(clconf_path_t *clconf_pathp)
{
	PM_DBG(("path_up (%p)\n", clconf_pathp));
	ASSERT(sched_lock.lock_held());
	uint32_t tout	= clconf_path_get_timeout(clconf_pathp);
	uint32_t q	= clconf_path_get_quantum(clconf_pathp);

	update_maxtimeouts(clconf_pathp, tout, q);

	(void) path_call(transport_list, &pm_client::path_up, clconf_pathp);
	(void) path_call(other_list, &pm_client::path_up, clconf_pathp);
	return (true);
}

bool
path_manager::path_down(clconf_path_t *clconf_pathp)
{
	ASSERT(clconf_pathp != NULL);

	// Now  that  the  path  is  going  down, we  may  have  to  recompute
	// max_clockthread_delay (the  minimum timeout could  have increased),
	// and  the   max_timeout  to  that  node  (the   maximum  could  have
	// decreased).

	// Two reasons to wait till here before updating max_timeouts[]:
	//
	//	1) now that the pathend is dropped, we can just walk
	//	   the path group list to recompute the max_timeouts[]
	//	2) here we are guaranteed that the pathend has been dropped,
	//	   and so the node_timeout is updated *before* we change
	//	   the max_timeouts[] (else there would be a window where
	//	   max_timeouts[] has decreased, but node_timeout has not
	//	   increased yet.

	ASSERT(sched_lock.lock_held());

	update_maxtimeouts(clconf_pathp, 0, 0);

	PM_DBG(("path_down (%p)\n", clconf_pathp));
	(void) path_call(transport_list, &pm_client::path_down, clconf_pathp);
	(void) path_call(other_list, &pm_client::path_down, clconf_pathp);
	return (true);
}

//
// path_manager::stale_incn
//
// Verifies if the given incarnation number from the specified node has
// already turned stale. Return value is true if the incarnation is stale.
// The third argument is an out parameter and is set to true upon return
// from the method if the passed incarnation is deemed to be stale because
// this node has already heard from a newer incarnation of the remote node.
// Called to perform checks during pathend registration.
//
bool
path_manager::stale_incn(nodeid_t ndid, incarnation_num incn,
    bool *heard_from_newer_p)
{
	if (heard_from_newer_p != NULL) {
		*heard_from_newer_p = false;
	}

	if (incn > mbrshp.members[ndid]) {
		//
		// A brand New incarnation
		//
		return (false);
	} else if (incn < mbrshp.members[ndid]) {
		//
		// We have already heard from a newer incarnation
		//

		if (heard_from_newer_p != NULL) {
			*heard_from_newer_p = true;
		}
		return (true);
	} else {
		//
		// Same as the current incarnation, more validity checks needed
		//
		if (!alive[ndid]) {
			//
			// The remote node has already been kicked out of
			// the membership and a path from it won't be
			// accepted unless we hear from a newer incarnation.
			//
			return (true);
		} else if (incn == unreach_mbrs.members[ndid]) {
			//
			//    The remote node has already been declared
			//    unreachable, but the cmm is yet to complete its
			//    distributed processing before it can kick this
			//    node out of the membership. However, either this
			//    node (or we the local node) is guaranteed go out
			//    of membership. Thus, we cannot accept a new path
			//    from the node.
			//
			return (true);
		} else {
			//
			// Current valid incarnation
			//
			return (false);
		}
	}
}

//
// update_node_incarnation is called by the transport to notify the path
// manager that the transport intends to register a pathend from this node
// incarnation subsequently. The return value indicates if the path manager
// will allow this registration to proceed based on incarnation number checks.
//
bool
path_manager::update_node_incarnation(nodeid_t ndid, incarnation_num incn)
{
	bool	heard_from_newer = false;

#ifdef	WEAK_MEMBERSHIP
	//
	// Check from CCR whether the local node has unresolved CCR changes
	// that have been done when the cluster was in a split-brain scenario
	// while the property multiple_partitions=true was set in CCR.
	//
	if (ccrlib::split_brain_ccr_change_exists()) {
		char rnodename[CL_MAX_LEN+1];
		clconf_get_nodename(ndid, rnodename);
		PM_DBG(("This node has unresolved CCR changes done after "
		    "split brain when multiple_partitions=true in CCR\n"));
		//
		// SCMSGS
		// @explanation
		// The local node has unresolved CCR changes that were done
		// in this node's partition when the cluster was in
		// a split-brain scenario while the property
		// multiple_partitions=true was set in CCR.
		// The administrator must first resolve the CCR changes.
		// Hence, this node will reject the communication
		// attempt from a remote node.
		// @user_action
		// Decide whether the CCR data on this node should be
		// the final CCR data that is used by the cluster.
		//
		// If yes, then reboot the remote node into noncluster mode
		// and mark its CCR data as invalid.
		// Mark this local node's CCR data as valid,
		// and then reboot the remote node into cluster mode.
		//
		// If no, then reboot this local node into noncluster mode
		// and mark its CCR data as invalid.
		// Mark the remote node's CCR data as valid
		// and reboot this local node into cluster mode.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Rejecting communication attempt from "
		    "node %s; local node has unresolved CCR changes done "
		    "after split brain.", rnodename);
		return (false);
	}
#endif	// WEAK_MEMBERSHIP

	mem_lock.lock();
	sched_lock.lock();
	unreach_sched_lock.lock();
	if (stale_incn(ndid, incn, &heard_from_newer)) {
		unreach_sched_lock.unlock();
		//
		// Syslog a message in case the current node incarnation is
		// not acceptable because the local node has already heard
		// from a newer incarnation of the remote node. This is most
		// likely not a result of out of order delivery of messages
		// but rather a result of the system administrator playing
		// with the system time on the cluster nodes. The system
		// administrator must take corrective steps before a path
		// from the remote node can be accepted. The membership
		// logged_stale_mbrs is used to avoid logging duplicate
		// messages.
		//
		if (heard_from_newer &&
		    incn != logged_stale_mbrs.members[ndid]) {
			char	rnodename[CL_MAX_LEN+1];
			time_t	stale_time;
			char 	stale_str[os::tod::CTIME_STR_LEN];
			time_t	expected_time;
			char	expected_str[32];

			stale_time = (time_t)incn;
			if (os::tod::ctime_r(&stale_time, stale_str,
			    os::tod::CTIME_STR_LEN) == NULL) {
				// This should never really happen
				stale_str[0] = 0;
			}
			expected_time = (time_t)expected_mbrs.members[ndid];
			if (os::tod::ctime_r(&expected_time, expected_str,
			    os::tod::CTIME_STR_LEN) == NULL) {
				// This should never really happen
				expected_str[0] = 0;
			}
			clconf_get_nodename(ndid, rnodename);

			//
			// SCMSGS
			// @explanation
			// It is likely that system time was changed backwards
			// on the remote node followed by a reboot after it
			// had established contact with the local node. When
			// two nodes establish contact in the Sun Cluster
			// environment, they make a note of each other's boot
			// time. In the future, only connection attempts from
			// this same or a newer incarnation of the remote node
			// will be accepted. If time has been adjusted on the
			// remote note such that the current boot time there
			// appears less than the boot time when the first
			// contact was made between the two nodes, the local
			// node will refuse to talk to the remote node until
			// this discrepancy is corrected. Note that the time
			// printed in this message is GMT time and not the
			// local time.
			// @user_action
			// If system time change on the remote node was
			// erroneous, reset the system time there to the
			// original value and reboot that node. Otherwise,
			// reboot the local node. This will make the local
			// node forget about any earlier contacts with the
			// remote node and will allow communication between
			// the two nodes to proceed. This step should be
			// performed with caution keeping quorum
			// considerations in mind. In general it is
			// recommended that system time on a cluster node be
			// changed only if it is feasible to reboot the entire
			// cluster.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "clcomm: Rejecting communication attempt from a"
			    " stale incarnation of node %s; reported boot time"
			    " %s, expected boot time %s or later.",
			    rnodename, stale_str, expected_str);

			logged_stale_mbrs.members[ndid] = incn;
		}

		sched_lock.unlock();
		mem_lock.unlock();
		PM_DBG(("Stale node %d incarnation %d\n", ndid, incn));
		return (false);
	}
	unreach_sched_lock.unlock();

	ASSERT(incn >= mbrshp.members[ndid]);
	ASSERT(incn >= expected_mbrs.members[ndid]);
	if (incn > mbrshp.members[ndid]) {
		//
		// If cluster shutdown is in progress, we will
		// fail the registration here as we don't want
		// a new joinee in cluster shutdown state.
		//
		if (cmm_impl::the().get_cluster_shutdown()) {
			PM_DBG(("Cluster shutdown in progress. Ignore "
			    "node %d with incarnation %d\n", ndid, incn));
			sched_lock.unlock();
			mem_lock.unlock();
			return (false);
		}
		//
		// A new incanation of the remote node is attempting
		// to establish a path with us. Need to disconnect with
		// the old incarnation, unless already done.
		//
		if (mbrshp.members[ndid] == expected_mbrs.members[ndid]) {
			(void) disconnect_node(ndid, mbrshp.members[ndid]);
		}
		expected_mbrs.members[ndid] = incn;
		PM_DBG(("New node %d incarnation %d\n", ndid, incn));
	} else {
		ASSERT(incn == mbrshp.members[ndid]);
		ASSERT(incn == expected_mbrs.members[ndid]);
	}

	sched_lock.unlock();
	mem_lock.unlock();

	return (true);
}

//
// register_pathend is called from the generic transport to register a
// pathend when it has formed a connection to the remote node.  A reference
// to the pathend lock is also passed so we can use the correct lock ordering.
//
bool
path_manager::register_pathend(pm_if *pp, os::mutex_t &pl)
{
	const ID_node &ppn = pp->node();

#ifdef PERPE_PMHB_DBGBUF
	PM_DBG(("register_pathend %s P(%p) PE(%p) HBDB(%p)\n",
		pp->get_extname(), pp->path(), pp, pp->get_hb_dbg()));
#else
	PM_DBG(("register_pathend %s P(%p) PE(%p)\n",
		pp->get_extname(), pp->path(), pp));
#endif

#ifdef PM_FIRST_PATH_DROP_PANIC_SUPPORT
	// A path has been established with the node. This node should
	// not be exempt from path timeout panics anymore.
	pm_ignore_path_drop_mask &= ~(1U << (ppn.ndid - 1));
#endif

	pl.unlock();
	mem_lock.lock();
	sched_lock.lock();

	//
	// Fail the registration if it does not carry the same incarnation
	// that was used for preregistration.
	//
	if (ppn.incn != expected_mbrs.members[ppn.ndid]) {
		sched_lock.unlock();
		mem_lock.unlock();
		pl.lock();
		return (false);
	}

	if (ppn.incn > mbrshp.members[ppn.ndid]) {
		// This is the first pathend to this incarnation of the
		// remote node.
		PM_DBG(("first pathend P(%p) PE(%p)\n", pp->path(), pp));
		unreach_sched_lock.lock();
		node_timeouts[ppn.ndid] = os::gethrtime();
		unreach_sched_lock.unlock();
		mbrshp.members[ppn.ndid] = ppn.incn;
		alive[ppn.ndid] = true;
	} else {
		//
		// The incarnation number validity might have changed between
		// update_node_incarnation and now. Need to repeat the check.
		//
		unreach_sched_lock.lock();
		if (stale_incn(ppn.ndid, ppn.incn, NULL)) {
			PM_DBG(("old pathend P(%p) PE(%p)\n", pp->path(), pp));
			unreach_sched_lock.unlock();
			sched_lock.unlock();
			mem_lock.unlock();
			pl.lock();
			return (false);
		}
		unreach_sched_lock.unlock();
	}

	ASSERT((mbrshp.members[ppn.ndid] == ppn.incn) && alive[ppn.ndid]);

	pl.lock();

	path_group *pgp;
	bool path_found = false;
	uint32_t q = msectopmt(clconf_path_get_quantum(pp->path()));

	// The path_record corresponding to this pathend's path is found,
	// and the pathend is added to the record.

	IntrList<path_group, _SList>::ListIterator li(pg_list);
	while (!path_found && ((pgp = li.get_current()) != NULL)) {
		if (q != pgp->quantum) {
			li.advance();
			continue;
		}
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp != pp->path())
				continue;
			path_found = true;
			// The token-time converted current time is used as
			// the initial local token time, resulting in the
			// pathend being timed out after the path timeout
			// if path_manager-to-path_manager communication is
			// never established.
			pgp->pra[i].add_pathend(pp, hrtotoken(os::gethrtime()));
			break;
		}
		li.advance();
	}

	// path_up will be ignored by the transport, and is delivered for
	// the sake of the other clients.  The transport will transition
	// to the up state when this function returns true.
	if (path_found) {
		PM_DBG(("pathend accepted P(%p) PE(%p)\n", pp->path(), pp));
		(void) path_up(pp->path());
	}

	sched_lock.unlock();
	mem_lock.unlock();
	return (path_found);
}

//
// revoke_pathend is called by a transport when it wants to immediately
// fail the path (if it is currently up).
//
void
path_manager::revoke_pathend(pm_if *pp)
{
	mem_lock.lock();
	sched_lock.lock();

	PM_DBG(("revoke_pathend P(%p) PE(%p)\n", pp->path(), pp));
	path_group *pgp;
	bool path_found = false;
	uint32_t q = msectopmt(clconf_path_get_quantum(pp->path()));

	// Look through all the path_records for the record with the matching
	// path pointer.  When found, if the pathend is registered, revoke it.

	IntrList<path_group, _SList>::ListIterator li(pg_list);
	while (!path_found && ((pgp = li.get_current()) != NULL)) {
		if (q != pgp->quantum) {
			li.advance();
			continue;
		}
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			if (pgp->pra[i].cpp != pp->path())
				continue;
			path_found = true;
			if (pgp->pra[i].pep != NULL) {
				ASSERT(pgp->pra[i].pep == pp);
				drop_pathend(pgp->pra[i], pm_if::PDR_REVOKED);
				(void) path_down(pgp->pra[i].cpp);
			}
			break;
		}
		li.advance();
	}

	sched_lock.unlock();
	mem_lock.unlock();
}

#ifdef	USE_TICKSTATS
// monitor_pathend_ticks()

// Checks the tick stats for  all active pathends. If no tick has
// happened   for  a   sufficiently   long  time,   and  if   the
// corresponding fault is activated, panic.

void
path_manager::monitor_pathend_ticks()
{
	path_group	*pgp;	// pointer to the path group;
	IntrList<path_group, _SList>::ListIterator
		li(pg_list);	// iterator for global list of path groups

	ASSERT(sched_lock.lock_held());

	// walk through list of all path groups
	for (; ((pgp = li.get_current()) != NULL); li.advance()) {

		// walk through all path records in that group
		for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			path_record &pr = pgp->pra[i];

			// skip if there is no path or pathend pointer
			if ((pr.cpp == NULL) || (pr.pep == NULL)) {
				continue;
			}

			// have pathend check its tick stats
			pr.pep->pm_check_tickstats();
		}
	}
}
#endif	// USE_TICKSTATS


//
// pm_isnewer:
// Utility routine provided by the path manager that pathends can
// use to determine is a new heartbeat message handed over to it
// by the transport is more recent than the one that it has already
// accepted earlier. The first parameter is the pointer to a buffer
// with the new heartbeat, the second is the pointer to the current
// heartbeat. Assumes that the buffers are propely aligned and are
// not being overwritten concurrently while this routine is called.
//
// A heartbeat that carries a logically larger (wraparounds can happen)
// local token is considered newer. Relies on uint subtraction to take
// care of wraparound issues. A new heartbeat that appears to be newer
// by more that MAX_TOKEN/2 is declared old as most certainly
// the token space has wrapped around to create "smaller" new tokens.
// If heartbeats do not arrive for a duration larger than the path
// timeout (which hopefully is much smaller than MAX_TOKEN/2), the path
// is expected to go down - hence the above heuristic. Ideally we should
// be using some multiple of path_timeout instead of MAX_TOKEN/2, but
// we choose this in favor of avoiding grabbing locks.
//
bool
path_manager::pm_isnewer(const void *newbuf, const void *curbuf)
{
	bool	isnewer;
	const pm_heartbeat	*newhbp;
	const pm_heartbeat	*curhbp;

	newhbp = (const pm_heartbeat *) newbuf;
	curhbp = (const pm_heartbeat *) curbuf;
	isnewer = (newhbp->get.local - curhbp->get.local <= max_token_div_2) ||
	    ((newhbp->get.local == curhbp->get.local) &&
	    (newhbp->get.remote - curhbp->get.remote <= max_token_div_2));
	if (curhbp->get.local == 0) {
		PM_DBG(("Forcing isnewer due to cur==0\n"));
		isnewer = 1;
	}
	return (isnewer);
}

//
// Processing consists of the following:
//	* failfast for long disruptions of cyclic interval
//	* schedule heartbeat sends
//	* check for heartbeat timeouts
//	* check node unreachable status
//
// Heartbeats are scheduled per the interval (quantum) specified for each
// path.  The quantum is specified in multiples of the pm tick length, pmtlen,
// (which is the minimum quantum). The pm cyclic has been programmed using
// this as it's interval and pm_tick is called from the cyclic handler.
// See path_manager.h for additional detail on heartbeats, quantums, and
// timeouts.
//
// Also to handle spurious delays in cyclic processing, pm_tick is called
// from hb_threadpool::send_heartbeat in network interrupt context.
//
void
path_manager::pm_tick(cyclic_caller_t who)
{
	hrtime_t	hrcurrent = os::gethrtime();
	path_group	*pgp;
	IntrList<path_group, _SList> temp_list;
	pm_heartbeat	hb;
	bool		group_empty;
	bool		pmtick_delayed;

	//
	// Do speculative check on the cyclic delay - only lock
	// after determining something must be done.
	// If we are in the interrupt context, we do a trylock
	// as we do not want to block the interrupt thread if
	// we  know that another thread is handling the heartbeats.
	// For cyclics, we block as the cyclic subsystem can fire
	// multiple times if delayed and we need to allow all of them
	// to complete.
	//
	if (who == NET_INTRPT_THRD) {
		if (hrcurrent < net_intr_run_time) {
			return;
		}
		if (pm_cyclic_lock.try_lock() == 0) {
			return;
		}
	} else {
		pm_cyclic_lock.lock();
	}

	if (who == NET_INTRPT_THRD) {
		if (hrcurrent < net_intr_run_time) {
			pm_cyclic_lock.unlock();
			return;
		}
		PM_DBG(("pm_tick late by %llx nsec\n",
		    (hrcurrent - net_intr_run_time)));
		net_intr_count++;
	} else
		pm_cyclic_count++;

	// update the net_intr_run_time (delay threshold)
	net_intr_run_time = hrcurrent + net_intr_run_delta;


#ifdef	USE_TICKSTATS
	// Gather statistics on heartbeat cyclic slop. No other thread touches
	// these, so no need to take any locks.
	pm_cyclic_stat.update();
#endif

#ifdef  FAULT_TRANSPORT
	void *fault_argp;
	uint32_t fault_argsize;
#endif

#ifdef  WRAP_TEST
	uint32_t	saved_current;
#endif

#ifdef	CHECK_FOR_PM_CYCLIC_RUNNING
#ifdef	FAULT_TRANSPORT
	// simulate a delayed cyclic
	if (fault_triggered(FAULTNUM_PM_DELAY_CLOCK_THREAD,
			    &fault_argp, &fault_argsize)) {
		os::printf("Fault 0x%x: late cyclic\n",
			FAULTNUM_PM_DELAY_CLOCK_THREAD);
		unreach_sched_lock.lock();
		hrlast = hrcurrent - max_clockthread_delay - 1;
		unreach_sched_lock.unlock();
	}
#endif	// FAULT_TRANSPORT

	unreach_sched_lock.lock();
	// Here we are comparing with LONG_MAX because previously due to a
	// bug max_clockthread_delay was set to LONG_MAX and we were comparing
	// with that value. Just to get same statistical data, the comparision
	// is being done with INT32_MAX(and not LONG_MAX because it is a very
	// number when compiled in 64-bit). But we do not panic when this
	// is true. We just print a message in the syslog and panic only
	// when the difference is more than max_clockthread_delay, which is
	// now about 5.5 secs. (with timeout=10, quantum=1).
	if ((hrlast != 0) && (hrcurrent - hrlast > (hrtime_t)INT32_MAX)) {
		// Note: the error message is off by pmtlen
		if (timeouts_enabled &&
		    (hrcurrent - hrlast > max_clockthread_delay)) {
			//
			// SCMSGS
			// @explanation
			// The system is unable to send heartbeats for a long
			// time. (This is half of the minimum of timeout
			// values of all the paths. If the timeout values for
			// all the paths is 10 secs then this value is 5
			// secs.) There is probably heavy interrupt activity
			// causing the clock thread to get delayed, which in
			// turn causes irregular heartbeats. The node is
			// aborted because it is considered to be in 'sick'
			// condition and it is better to abort this node
			// instead of causing other nodes (or the cluster) to
			// go down.
			// @user_action
			// Check to see what is causing high interrupt
			// activity and configure the system accordingly.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "Aborting node because pm_tick "
			    "delay of %lld ms exceeds %lld ms",
			    (hrcurrent - hrlast) / HR_TICKS_PER_MS,
			    max_clockthread_delay / HR_TICKS_PER_MS);
		} else {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "pm_tick delay of %lld ms exceeds %lld ms",
			    (hrcurrent - hrlast) / HR_TICKS_PER_MS,
			    (hrtime_t)INT32_MAX / HR_TICKS_PER_MS);
#ifdef REPEAT_TO_CATCHUP
			if (!timeouts_enabled)
				hrlast = 0;
#else
			hrlast = 0;
#endif
		}
	}
	unreach_sched_lock.unlock();
#endif	// CHECK_FOR_CLOCKTHREAD_RUNNING

#ifdef	DEBUG
	// Maintain pm_lbolt and pm_hrt for debugging purposes
	(void) os::drv_getparm(LBOLT, &pm_lbolt);
	pm_hrt	 = os::gethrtime();
#endif	// DEBUG


#ifdef	USE_TICKSTATS
	// Check the snd/recv tick stats for all active pathends. If no tick has
	// happened for a sufficiently long time, and if the corresponding
	// fault is activated, panic.
	//
	// XXX	This is pretty heavy-weight, and should be taken out
	//	once the ordeal with the path timeouts is over.

	sched_lock.lock();
	monitor_pathend_ticks();
	sched_lock.unlock();
#endif	// USE_TICKSTATS

	sched_lock.lock();

	do {
		pmtick_delayed = false;
		// Store the ideal time of this tick.
		if (hrlast == 0)
			hrlast = hrcurrent;
		else
			hrlast += pmtlen * HR_TICKS_PER_MS;

		current_token = hrtotoken(hrcurrent);
#ifdef WRAP_TEST
		// simulate heart beat wrapped -
		// if triggered/activated, the current time will be added
		// with the read in wrap_token value, which can be large
		// enough to cause the heart beat value wrap.  This new
		// time will be used in the following heartbeat sent, until
		// when the fault is cleared.

		saved_current = current_token;
		if (fault_triggered(FAULTNUM_PM_HB_WRAP,
		    &fault_argp, &fault_argsize)) {
			ASSERT(fault_argsize == sizeof (uint32_t));
			unreach_sched_lock.lock();
			wrap_token = *((uint32_t *)fault_argp);
			current_token += wrap_token;
			unreach_sched_lock.unlock();
		}
#endif


		// If the current scheduler slot is the size of the
		// scheduling array, then we process the long-quantum
		// path_groups, which are stored in an extra list at the end
		// of the scheduling array.  After that we process slot 0.
		// XXX Update the long_schedule to be a sorted list using
		// time deltas.  The RFE for this is 4220555
		if (schedule_slot == PM_SCHED_SIZE) {
			while ((pgp = long_schedule.reapfirst())
			    != NULL) {
				if (pgp->timeleft >= PM_SCHED_SIZE) {
					// Still not ready to be put into
					// a real slot. We append it to a
					// temporary list.
					pgp->timeleft -= PM_SCHED_SIZE;
					temp_list.append(&(pgp->schedule_le));
				} else {
					// Put the group into the real slot.
					send_schedule[pgp->timeleft].prepend
					    (&(pgp->schedule_le));
					pgp->timeleft = 0;
				}
			}
			long_schedule.concat(temp_list);
			schedule_slot = 0;
		}

		// Process the groups in the current slot.
		while ((pgp = send_schedule[schedule_slot].reapfirst())
		    != NULL) {
			ASSERT(pgp->timeleft == 0);
			group_empty = true;
			for (uint_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
				path_record &pr = pgp->pra[i];
				nodeid_t	ndid;
				if (pr.cpp != NULL)
					group_empty = false;
				if (pr.pep == NULL)
					continue;
				pr.pep->pm_get(&(hb.hb));
				ndid = pr.pep->node().ndid;
				// if local_m is zero, either we haven't
				// received a message from the remote node,
				// or this is the first message in which the
				// remote_token is defined.
				if (hb.get.local == 0) {
					pr.remote_token = hb.get.remote;
				} else {
#ifdef WRAP_TEST
				if (fault_triggered(FAULTNUM_PM_HB_WRAP,
				    &fault_argp, &fault_argsize)) {
					if (wrap_record[i] == 0) {
						// first time
						unreach_sched_lock.lock();
						pr.local_token += wrap_token;
						hb.get.local += wrap_token;
						pr.pep->pm_wrap(&(hb.hb));
						pr.pep->pm_get(&(hb.hb));
						wrap_record[i] = 1;
						unreach_sched_lock.unlock();
					}
				} else if (wrap_record[i] == 1) {
					// fault test has been
					// cleared;
					// set record and time back
					// to normal.
					unreach_sched_lock.lock();
					current_token = saved_current;
					hb.get.local -= wrap_token;
					pr.local_token -= wrap_token;
					pr.pep->pm_wrap(&(hb.hb));
					pr.pep->pm_get(&(hb.hb));
					wrap_record[i] = 0;
					unreach_sched_lock.unlock();
				}
#endif // WRAP_TEST

					// These are unsigned integers, so the
					// distance between a newer and older
					// number is calculated correctly even
					// when the numbers are wrapping around.
					// If the local token in the message
					// is newer, we store both the local
					// and remote tokens.
					if ((current_token - hb.get.local) <=
					    (current_token - pr.local_token)) {
						pr.local_token = hb.get.local;
						pr.remote_token = hb.get.remote;
					}
				}
				// Now we send the message, with the current
				// time as the local token and the most
				// recently received remote_token.
				hb.put.local = current_token;
				hb.put.remote = pr.remote_token;
				pr.pep->pm_put(&(hb.hb));
#ifdef	USE_TICKSTATS
				// Check the send/recv tick stats on the pathend
				pr.pep->pm_check_tickstats();
#endif

				// Finally, if timeouts are enabled and the
				// time difference between the current time
				// and the last received time (in token units)
				// is larger than the timeout, we declare the
				// path down. The path is also downed if the
				// node has been marked dead by
				// node_is_unreachable().

				unreach_sched_lock.lock();
				bool is_dead = (pr.pep->node().incn
				    <= unreach_mbrs.members[ndid]);
				unreach_sched_lock.unlock();
				bool ep_is_faulted = pr.pep->is_ep_faulted();
				bool hb_timedout = timeouts_enabled &&
				    ((current_token - pr.local_token)
				    > pr.tout);

				// The following if-else clause records the
				// pathdrop reason using the right precedence.

				pm_if::path_drop_reason dr = pm_if::PDR_NONE;
				if (is_dead) {
					dr = pm_if::PDR_PEER_DEAD;
				} else if (hb_timedout) {
					dr = pm_if::PDR_HB_TIMEOUT;
				} else if (ep_is_faulted) {
					dr = pm_if::PDR_EP_FAULT;
				}

				if (is_dead || hb_timedout || ep_is_faulted) {
					drop_pathend(pr, dr);
					(void) path_down(pr.cpp);
				}
			}
			//
			// Now we either delete the path_group if it has no
			// active records, or reschedule it into the short-term
			// or long-term list.
			//
			if (group_empty) {
				delete pgp;
			} else if (pgp->quantum >= PM_SCHED_SIZE) {
				pgp->timeleft = pgp->quantum + schedule_slot -
				PM_SCHED_SIZE;
				long_schedule.prepend(&(pgp->schedule_le));
			} else {
				send_schedule[(pgp->quantum + schedule_slot) %
				PM_SCHED_SIZE].prepend(&(pgp->schedule_le));
			}
		}

		schedule_slot++;

#ifdef REPEAT_TO_CATCHUP
		//
		// If the cyclic is delayed, the cyclic subsystem will fire
		// the cyclic mutliple times to make up, but if heartbeat
		// processing is called from network interrupt context, we
		// catch up by looping here.
		//
		if ((who == NET_INTRPT_THRD) && (hrtime_t)(hrcurrent - hrlast) >
		    (hrtime_t)(pmtlen * HR_TICKS_PER_MS)) {
			ncatchup++;
			pmtick_delayed = true;
		}
#endif

	} while (pmtick_delayed);

	sched_lock.unlock();
	pm_cyclic_lock.unlock();
}

// The CMM should receive node_is_unreachable before it receives the next
// node_is_reachable.  The only event that can generate a node_is_reachable
// call is the registration of an endpoint with the endpoint registry.

void
path_manager::node_is_unreachable(nodeid_t nodeid, incarnation_num incn)
{
	uint32_t	tout;		// in milliseconds

	// Assert that node was reachable before.
	PM_MARK_CLEAR(nodeid, reachable_order_bitvec);
	PM_DBG(("(%d,%d) node_is_unreachable\n", nodeid, incn));

	unreach_sched_lock.lock();

	// pm_tick() will see the unreach membership, and bring down all paths
	// to that node.  This guarantees that the other side  will see a path
	// timeout within  max_timeouts[].

	unreach_mbrs.members[nodeid] = incn;

	// First see if a node_timeout has been set (the path has been brought
	// down  earlier,  and possibly  removed,  but node_timeouts[],  still
	// remembers it).

	tout = (uint32_t)((node_timeouts[nodeid]
		- os::gethrtime()) / HR_TICKS_PER_MS);

	// Take the maximum timeout of all  existing paths to that node (we do
	// not distinguish whether they are  up or down), or the node_timeout,
	// whatever is further in the future.

	if (tout < max_timeouts[nodeid]) {
		tout = max_timeouts[nodeid];
	}
	unreach_sched_lock.unlock();

	// When  the pm_cmmp  is nulled  out  in shutdown(),  there should  no
	// longer be any calls to node_is_unreachable.

	if (pm_cmmp != NULL) {
		PM_DBG(("(%d,%d) calling CMM node_is_unreachable with"
		    " tout %d\n", nodeid, incn, tout));
		pm_cmmp->update_node_status(nodeid, INCN_UNKNOWN,
		    (cmm::timeout_t)tout);
	}
}

// This is called by rawdlpi_adapter when a panic message is received

void
path_manager::node_is_panicking(nodeid_t nodeid, incarnation_num incn)
{
	PM_DBG(("(%d,%d) node_is_panicking\n", nodeid, incn));

	unreach_sched_lock.lock();

	//
	// pm_tick() will see the unreach membership, and bring down all paths
	// to that node.
	//

	unreach_mbrs.members[nodeid] = incn;

	unreach_sched_lock.unlock();
}

void
path_manager::node_is_reachable(nodeid_t nodeid, incarnation_num incn)
{
	// Assert that node was unreachable before.
	PM_MARK_SET(nodeid, reachable_order_bitvec);
	PM_DBG(("(%d,%d) node_is_reachable\n", nodeid, incn));

	// No need to take the sched_lock for pm_cmmp. On shutdown,
	// all endpoints will be gone by the time the pm_cmmp is zeroed.
	if (pm_cmmp != NULL) {
		PM_DBG(("(%d,%d) calling CMM node_is_reachable.\n",
			nodeid, incn));
		pm_cmmp->update_node_status(nodeid, incn, (cmm::timeout_t)0);
	}
}


#if defined(_FAULT_INJECTION)

//
// External interface for cl_orb
//
extern "C" int
fault_revoke_pathend(int local_adapterid, nodeid_t remote_nodeid)
{
	return (path_manager::the().fault_revoke_pathend(
	    local_adapterid, remote_nodeid));
}

//
// Method to support the "failpath" test utility: given a
// local adapter ID and a remote node ID, fail the associated pathend.
// This method is very similar to revoke_pathend().
// Returns:
//	0		- success.
//	ECANCELED	- the pathend is already down.
//	ENOENT		- the pathend doesn't exist.
//
int
path_manager::fault_revoke_pathend(int local_adapterid, nodeid_t remote_nodeid)
{
	mem_lock.lock();

	path_group	*pgp;
	int		retval = ENOENT;		// path not found

	IntrList<path_group, _SList>::ListIterator li(pg_list);

	// Look through all the path_records for the pathend with the matching
	// local adapter ID and remote node ID.
	while ((pgp = li.get_current()) != NULL) {
		for (uint32_t i = 0; i < PM_PA_GROUP_SIZE; i++) {
			// If the record is unused.
			if (pgp->pra[i].cpp == NULL) {
				continue;
			}

			// Compare local adapter ID.
			if (local_adapterid !=
			    clconf_path_get_adapterid(pgp->pra[i].cpp,
				CL_LOCAL)) {
				continue;
			}

			// Compare remote node ID.
			if (remote_nodeid !=
			    clconf_path_get_remote_nodeid(pgp->pra[i].cpp)) {
				continue;
			}

			// We got the path.  If it's not down, revoke it.
			sched_lock.lock();
			if (pgp->pra[i].pep != NULL) {
				PM_DBG(("fault_revoke_pathend A(%d) N(%d)\n",
				    local_adapterid, remote_nodeid));
				drop_pathend(pgp->pra[i],
				    pm_if::PDR_REVOKED_FI);
				(void) path_down(pgp->pra[i].cpp);
				retval = 0;		// success
			} else {
				retval = ECANCELED;	// path already down
			}
			sched_lock.unlock();
			goto done;
		}
		li.advance();
	}

done:
	mem_lock.unlock();
	return (retval);
}
#endif // _FAULT_INJECTION


// The  functions  below  maintain  the max_clockthread_delay,  which  is  the
// maximum time (in nanoseconds) that the clock thread can be late. We want to
// panic the  node before other  nodes are infected  with the malaise,  so the
// max_clockthread_delay  must  be  less  than  the smallest  timeout  to  any
// node. Pick  it to be  half of  the smallest timeout,  and add a  pmtlen (to
// avoid arithmetic during the check, which is more frequent):
//
// max_clockthread_delay = min_timeout / 2 + pmtlen
//
// Note:  need  to  hold  the sched_tout_lock when reading/writing
//	  max_clockthread_delay.
//
// The arguments are in hrtime units (nanoseconds)

// Returns true if max_clockthread_delay needs to be reduced due
// to new path timeout of tout_nsec.
bool
path_manager::lessthan_max_clockthread_delay(hrtime_t tout_nsec)
{
	ASSERT(unreach_sched_lock.lock_held());
	if (tout_nsec < 2LL * (max_clockthread_delay - pmtlen
				* HR_TICKS_PER_MS)) {
		return (true);
	}
	return (false);
}

// Returns true if max_clockthread_delay needs to be increased due
// to removal of path timeout of tout_nsec.
bool
path_manager::lessthanorequal_max_clockthread_delay(hrtime_t tout_nsec)
{
	ASSERT(unreach_sched_lock.lock_held());
	if (tout_nsec <= 2LL * (max_clockthread_delay - pmtlen
				* HR_TICKS_PER_MS)) {
		return (true);
	}
	return (false);
}

// Updates max_clockthread_delay with new value
void
path_manager::update_max_clockthread_delay(hrtime_t	tout_nsec)
{
	hrtime_t	new_delay;
	ASSERT(unreach_sched_lock.lock_held());

	new_delay = tout_nsec / 2LL + HR_TICKS_PER_MS * (hrtime_t)pmtlen;

	PM_DBG(("Updating max_clockthread_delay from %lld to %lld.\n",
		max_clockthread_delay, new_delay));

	max_clockthread_delay = new_delay;
}

#ifdef	PM_VERIFY_ORDER
// The  order_bitvec class  is  used to  verify  that per-node  events
// happen   in   the   right   order.   For  instance,   a   call   to
// node_is_reachable()    must   be    followed   by    a    call   to
// node_is_unreachable().    So  inside   those  two   functions,  the
// order_bitvec  class is used  to keep  track of  which one  has been
// called last, by setting and  clearing the bits corresponding to the
// nodeid. It causes  an assertion failure if the  proper order is not
// maintained.

path_manager::order_bitvec::order_bitvec() :
	bitvec(0)
{
}

path_manager::order_bitvec::~order_bitvec()
{
	ASSERT(bitvec == 0);
}

void
path_manager::order_bitvec::set(nodeid_t nid)
{
	bitlock.lock();
	ASSERT(nid > 0 && nid <= 64);
	ASSERT((bitvec & (1ULL << (nid - 1))) == 0);
	bitvec |= (1ULL << (nid - 1));
	bitlock.unlock();
}

void
path_manager::order_bitvec::clear(nodeid_t nid)
{
	bitlock.lock();
	ASSERT(nid > 0 && nid <= 64);
	ASSERT((bitvec & (1ULL << (nid - 1))) != 0);
	bitvec &= ~(1ULL << (nid - 1));
	bitlock.unlock();
}
#endif	// PM_VERIFY_ORDER

//
// External interface for cl_haci
//
extern "C" uint32_t
pm_get_max_delay(nodeid_t nodeid)
{
	return (path_manager::the().get_max_delay(nodeid));
}

extern "C" bool
pm_remove_path(clconf_path_t *clconf_pathp)
{
	return (path_manager::the().remove_path(clconf_pathp));
}

extern "C" bool
pm_add_path(clconf_path_t *clconf_pathp)
{
	return (path_manager::the().add_path(clconf_pathp));
}

extern "C" bool
pm_disable(void)
{
	return (path_manager::the().disable());
}

extern "C" bool
pm_enable(void)
{
	return (path_manager::the().enable());
}

extern "C" void
pm_register_cmm(cmm_impl *cmmp)
{
	path_manager::the().register_cmm(cmmp);
}

extern "C" void
pm_unregister_cmm(cmm_impl *cmmp)
{
	path_manager::the().unregister_cmm(cmmp);
}
