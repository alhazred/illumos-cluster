/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _TRANSPORT_H
#define	_TRANSPORT_H

#pragma ident	"@(#)transport.h	1.64	08/05/20 SMI"

#include <orb/fault/fault_injection.h>

#include <orb/transport/pm_client.h>
#include <orb/transport/path_manager.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/flow/resource.h>
#include <orb/query/pathend_state_proxy.h>
#include <orb/query/adapter_state_proxy.h>
#include <orb/infrastructure/orbthreads.h>
#include <orb/debug/haci_debug.h>


#if defined(HACI_DBGBUFS) && !defined(NO_TRANSPORT_DBGBUF)
#define	TRANSPORT_DBGBUF
#endif

#ifdef	TRANSPORT_DBGBUF
extern dbg_print_buf trans_dbg;
#define	TRANS_DBG(a) HACI_DEBUG(ENABLE_TRANS_DBG, trans_dbg, a)
#else
#define	TRANS_DBG(a)
#endif

#ifdef	_KERNEL		// don't check for sendthread running in unode mode

// 		The sendthread failfast mechanism:
//
// Although not observed in practice, a path timeout could occur under
// heavy interrupt  load, since the  deferred send threads run  at the
// lower realtime priorities, and may  not get a chance to execute. If
// this is  the case, other  services essential to the  functioning of
// the cluster may  also be affected. A failfast  mechanism panics the
// node if  this condition is detected. It  kicks in when a  send on a
// path has been delayed for more than 1/2 of the path timeout on that
// path.
// In unode  mode, the  sending of the  heartbeat messages  involves a
// door  call, which can  delay the  send thread  if the  other node's
// process happens to  be suspended (or in the  debugger). If one node
// is  put  into the  debugger,  all  the  other nodes  failfast  soon
// thereafter.  To  avoid this  nuisance,  the  failfast mechanism  is
// switched off for unode.
#define	CHECK_FOR_SENDTHREAD_RUNNING
#endif

// The defer task for the pathend, included in the
// pathend object as pdt so that tasks can be deferred
// without allocating memory

class pathend;
class endpoint;

class pathend_defer_task : public defer_task {
public:
	pathend_defer_task(pathend *);
	void execute();
	void task_done();
private:
	pathend *pe;

	pathend_defer_task(const pathend_defer_task &);
	pathend_defer_task &operator = (pathend_defer_task &);
};

class pm_send_defer_task : public defer_task {
public:
	pm_send_defer_task(pathend *);
	void execute();
	void task_done();
	bool is_deferred();
	void set_deferred(bool);
private:
	pathend *pe;
	bool	send_task_deferred;	// protected by hb_threadpool lock
	pm_send_defer_task(const pm_send_defer_task &);
	pm_send_defer_task &operator = (pm_send_defer_task &);
};

class get_pathend_refcnt : public refcnt {
public:
	get_pathend_refcnt(pathend *);
private:
	void refcnt_unref();
	pathend *pe;
};

class pathend : public pm_if, public _SList::ListElem {
public:
	friend class transport;
	friend class pathend_defer_task;
	friend class pm_send_defer_task;

	//
	// The P_INITERR is a special state which exists only to indicate
	// to the user that pathend initilization has been experiencing
	// problems. As far as state transitions are concerned, P_INITERR
	// is equivalent to P_CONSTRUCTED.
	//
	enum pathend_state { P_CONSTRUCTED = 0, P_INITERR, P_INITIATED, P_UP,
		P_CLEANING, P_DRAINING, P_DELCLEAN, P_DELETING, P_DELETED };

	// The transport-specific destructor must be implemented
	virtual	~pathend();

	// From the Path Monitor interface (pm_if)
	void 	pm_get(void * const);
	void 	pm_put(const void * const);
#ifdef WRAP_TEST
	void	pm_wrap(void * const);
#endif

	// To release after a get_pathend()
	void	get_pathend_rele();

	pathend_state	get_state();

	// Accessor functions for the generic endpoint
	const ID_node	&node() const;
	clconf_path_t	*path() const;

	// Returns the local adapter_id for this pathend
	int	local_adapter_id() const;

	// Returns the remote adapter_id for this pathend
	int	remote_adapter_id() const;

	const char *get_extname() const;

#ifdef PERPE_PMHB_DBGBUF
	const dbg_print_buf *get_hb_dbg() const;
	void swap_hb_dbg();
#endif

	virtual void atomic_copy_64(uint64_t *dst, uint64_t *src);

	// Called by the specific transport when it detects the connection
	// has failed.  Cannot be called with the lock held.
	void		fail();

	// Called by the specific transport when it detects an error
	// during initiate. Must be called with the lock held.
	void		init_error();

	void			record_drop_info(path_drop_reason);

	bool			is_pe_up();

	// Calls to get/set associated endpoint faulted information.
	bool			is_ep_faulted();
	void			mark_ep_faulted();
	void			record_ep_registration(bool);

#ifdef DEBUGGER_PRINT
	void debugger_print(int);
#endif

protected:
	pathend(transport *, clconf_path_t *);


	//
	// pm_get_internal is called from pm_get(), it is a hook that provides
	// a specific transport with an opportunity to process a heartbeat
	// token before passing it to the generic transport. The default impl
	// is a noop. See rsm_transport for a specific example.
	virtual void	pm_get_internal() {};

	// These pure virtual functions are implemented by the
	// specific transports

	virtual	void	pm_send_internal() = 0;

	// Create the connection to the remote node and perform the
	// handshake to retrieve the incarnation number
	virtual	void	initiate(char *&send_buf, char *&rec_buf,
			    incarnation_num &incn) = 0;

	// Tear down the connection state
	virtual	void	cleanup() = 0;

	// Tranports are allowed to retain some of the connection
	// information after a cleanup has been done to allow the
	// ongoing communication to complete. When a pathend is
	// recycled and moved back to the constructed state after
	// draining, reinit is called to cleanup any remnants from
	// its previous incarnation. The default implementation is
	// null.
	virtual void	reinit();

	// When the pathend receives remove_path, it will call delete_notify
	// if the machine is initiating.  It must not block or allocate any
	// memory, but can be used to, for instance, wake threads
	// that might be sleeping.  It is called with the lock
	// held.  The default implementation is null
	virtual void	delete_notify() {};

	// Create a specific endpoint
	virtual endpoint *	new_endpoint(transport *) = 0;

#ifdef	USE_TICKSTATS
	// Check if  the tick stats are  ok. If not, and  the associated fault
	// point is switched on, the node is panicked.
	//
	// Note: for  the check to make sense,  it must be called  from a very
	// high priority thread  that is virtually guaranteed to  run, such as
	// the clock interrupt thread.
	//
	// If a specific transport does  not use tick stats, and never updates
	// them, pm_check_tickstats() will become a no-op.

	void pm_check_tickstats();
#endif	// USE_TICKSTATS

	// Now we have the implemented functions

	// The standard locking functions
	void		lock();
	void		unlock();
	bool		lock_held();

	// The routine to determine if the new heartbeat (passed parameter)
	// is newer than the last currently accepted heartbeat
	bool		pm_isnewer(const void *);

	clconf_path_t		*clconf_pathp;
	transport		*transportp;
	ID_node			pe_node;

#ifdef	USE_TICKSTATS
	// tick stats record stats on sent and recv'd heartbeats.
	// They are inherited by the specific transports, who can, but need
	// not update the tick stats.
	tick_stat	send_tick_stat;
	tick_stat	recv_tick_stat;
#endif

#ifdef PERPE_PMHB_DBGBUF
	// Per pathend heartbeat debug buffers. Each pathend maintains a
	// pair of heartbeat debug buffers. Everytime a pathend comes up
	// is switches the heartbeat buffer to be used. This is so that the
	// the heartbeat information from a path down can be maintained
	// even after the path has come back on. A pointer to the current
	// buffer in use is maintained in hb_dbgp and is logged in the
	// pm_dbg debug buffer every time the pathend is registered and is
	// dropped. Pointer to the other buffer is maintained in hb_dbgo.
	dbg_print_buf	*hb_dbgp;
	dbg_print_buf	*hb_dbgo;
#endif

private:
	// Start the defer task the first time;
	void			startup();

	// Called by the generic transport when TCR events occur
	bool		path_up(clconf_path_t *);
	bool		path_down(clconf_path_t *);
	void		disconnect_node(incarnation_num);
	void		remove_path(clconf_path_t *);

	// To hold during a get_pathend()
	void	get_pathend_hold();

	// To set the current state
	void	set_state(pathend_state new_state);

	// Returns true if the supplied path object is equal to the
	// object the pathend was constructed with, false otherwise
	bool		eq(clconf_path_t *);

	// pathend utility functions
	// The entry point for the pathend_defer_task
	void		task_execute();
	// The entry point for the pm_send_defer_task
	void		send_execute();

	// task_execute() calls enter_machine() to move through the
	// the states.
	void		enter_machine();

	void	check_for_delay(hrtime_t last_sched_time,
				hrtime_t last_exec_time,
				hrtime_t max_delay, const char *pmsg);

	// Called by the code inherited through refcnt
	void		refcnt_unref();

	bool			task_not_done();
	void			queue_task();
	void			queue_send();

	pathend_state		p_state;
	bool			task_deferred;
	bool			delay_retry;
	pathend_defer_task	pdt;
	pm_send_defer_task	pmsdt;
	get_pathend_refcnt	gpr;
	os::condvar_t		pe_cv;
	char			*send_buf;
	char			*rec_buf;

	pathend_state_proxy	*pe_sproxy;

	// Local adapter id from clconf_path_t
	int			local_adp_id;
	// Remote adapter id from clconf_path_t
	int			remote_adp_id;
	// maximum allowed delay for the send thread, in nanoseconds
	hrtime_t		max_send_delay;
	// last time a successful send happened (absolute, in ns)
	hrtime_t		last_send_time;
	// last time a send was scheduled (absolute, in ns)
	hrtime_t		last_sendsched_time;

	//
	// Endpoint state. It is a collection of flags that captures assorted
	// information regarding the endpoint.
	//
	// P_EP_NEW: The pathend state machine has just created or is in the
	//	process of creating the endpoint. The endpoint state machine
	//	has not yet come back to tell the pathend whether the endpoint
	//	could be successfully created and registered.
	//
	// P_EP_REGISTERED: The endpoint associated with the pathend has been
	//	successfully registered with the endpoint state machine.
	//
	// P_EP_FAULTED: The endpoint has detected a communication error over
	//	the path. The path should be torn down.
	//
	int		ep_state;
	enum {P_EP_NEW = 0x1, P_EP_REGISTERED = 0x2, P_EP_FAULTED = 0x4};

	// Debug information about pathend drop and registration
	// cur_reg_hrtime : Most recent pathend registration time
	// last_reg_hrtime: Registration time prior to cur_reg_hrtime
	// drop_hrtime	  : Most recent path drop time
	// drop_reason	  : Why was the path dropped
	hrtime_t		last_reg_hrtime;
	hrtime_t		cur_reg_hrtime;
	hrtime_t		drop_hrtime;
	path_drop_reason	drop_reason;

	pathend(const pathend &);
	pathend &operator = (pathend &);
};

class endpoint_defer_task : public defer_task {
public:
	endpoint_defer_task(endpoint *);
	void execute();
	void task_done();
private:
	endpoint *ep;

	endpoint_defer_task(const endpoint_defer_task &);
	endpoint_defer_task &operator = (endpoint_defer_task &);
};

class endpoint_delete_defer_task : public defer_task {
public:
	endpoint_delete_defer_task(endpoint *);
	void execute();
	void task_done();
private:
	endpoint *ep;

	endpoint_delete_defer_task(const endpoint_delete_defer_task &);
	endpoint_delete_defer_task &operator = (endpoint_delete_defer_task &);
};

class endpoint : public er_if {
public:
	friend class transport;
	friend class endpoint_defer_task;
	friend class endpoint_delete_defer_task;

	enum endpoint_state { E_CONSTRUCTED, E_REGISTERED,
	    E_PUSHING, E_CLEANING, E_DRAINING, E_DELETED };

	// From the Endpoint Registry interface (er_if)
	void			registration_callback();
	virtual void 		add_resources(resources *) = 0;
	virtual	sendstream *	get_sendstream(resources *) = 0;

	virtual			~endpoint();
	endpoint_state		get_state();

	// Returns the local adapter_id for this endpoint
	uint_t	local_adapter_id() const;

#if defined(FAULT_TRANSPORT)
	// Fails the associated pathend.
	// Fault Argument: FaultFunctions::counter_t
	void	fault_pathend_fail();
#endif
	//
	// Invoked by specific transports; specific transport endpoint
	// class should provide an override method
	//
	virtual void	timeouts_disable();
	virtual void	timeouts_enable();


protected:
	endpoint(transport *, pathend *);

	// The standard locking functions
	void		lock();
	void		unlock();
	bool		lock_held();

	// The virtual functions the specific transport must implement.
	// unblock is called with the lock held and must not block.  It
	// is called when the endpoint is declared down so that threads
	// can unblock and fail.
	virtual	void	unblock() = 0;
	// initiate should set up the connection.
	virtual	void	initiate() = 0;
	// drain should push all messages up to the orb_msg layer.
	virtual void	push() = 0;
	// release should go through the process of tearing down the
	// endpoint, releasing the pathend as soon as possible.
	virtual void	cleanup() = 0;


	void	release_pathend();

	pathend			*pathendp;
	transport		*transportp;
	clconf_path_t		*clconf_pathp;
	ID_node			ep_node;


private:
	// Implemented by the generic endpoint
	void	path_down(clconf_path_t *);
	void	disconnect_node(incarnation_num);
	bool	node_died_prepare(incarnation_num);
	void	node_died();

	// Returns true if the supplied path object is equal to the
	// object the pathend was constructed with, false otherwise
	bool		eq(clconf_path_t *);

	// Support functions
	_SList::ListElem *	get_translist_elem();
	void			startup();
	void			refcnt_unref();
	void			task_execute();
	void			enter_machine();
	bool			task_not_done();
	void			queue_task();

	endpoint_state			e_state;
	bool				task_deferred;
	_SList::ListElem		translist_elem;
	endpoint_defer_task		edt;
	endpoint_delete_defer_task	eddt;
	os::condvar_t			ep_cv;
	os::mutex_t			ep_lock;
	uint_t				local_adp_id;

	endpoint(const endpoint &);
	endpoint &operator = (endpoint &);
};

class adapter : public _SList::ListElem {
public:
	enum adapter_state {A_CONSTRUCTED = 0, A_FAULTED, A_DELETED};
	adapter(transport *, clconf_adapter_t *);
	virtual ~adapter();
	int	get_adapter_id() const;
	void 	set_state(adapter_state new_state);

protected:
	transport *transp;

private:
	int	adp_id; // local adapter id from clconf_adapter_t

	adapter_state 		a_state;
	adapter_state_proxy	*ad_sproxy;

	adapter(const adapter &);
	adapter &operator = (adapter &);
};

class transport : public pm_client {
public:
	transport(const char *, bool);
	virtual	~transport();

	const bool defer_send;

	bool add_adapter(clconf_adapter_t *);
	bool remove_adapter(clconf_adapter_t *);
	bool change_adapter(clconf_adapter_t *);
	bool add_path(clconf_path_t *);
	bool remove_path(clconf_path_t *);
	bool path_up(clconf_path_t *);
	bool path_down(clconf_path_t *);
	bool node_alive(nodeid_t, incarnation_num);
	bool node_died(nodeid_t, incarnation_num);
	bool disconnect_node(nodeid_t, incarnation_num);

	virtual void	shutdown();

	void	add_endpoint(endpoint *, nodeid_t);
	void	remove_endpoint(endpoint *, nodeid_t);
	void	lock_node(nodeid_t);
	void	unlock_node(nodeid_t);

	// Return a pathend with a hold on it corresponding to the
	// adapter id and remote node
	// There is an assumption that through an adapter there is only
	// path to a remote node
	pathend	*get_pathend(int, bool, nodeid_t);

	// Find's the nth local adapter
	// No locking is done (should we?)
	adapter *get_adapter(int);

	void	defer_send_processing(defer_task *);
#ifdef DEBUGGER_PRINT
	void 	print_pathends_info(int);
#endif
protected:
	virtual const char 	*transport_id() = 0;
	virtual	pathend		*new_pathend(clconf_path_t *) = 0;
	virtual	adapter		*new_adapter(clconf_adapter_t *) = 0;

	os::mutex_t			adapter_lock;
	IntrList<adapter, _SList>	the_adapters;
	// the_endpoints_incn array is an array of incarnation numbers
	// corresponding to each endpoint lists. The incarnation numbers
	// are initialized to 0 and the incarnation number corresponding
	// to an endpoint list gets incremented every time an element
	// is added or deleted from that endpoint list. A change in the
	// incarnation number indicates that the list has changed and we
	// need to reinitialize any list iterators to iterate
	// through the list.
	uint64_t 			the_endpoints_incn[NODEID_MAX+1];
	IntrList<endpoint, _SList>	the_endpoints[NODEID_MAX+1];
	IntrList<pathend, _SList>	the_pathends[NODEID_MAX+1];
	os::mutex_t			node_locks[NODEID_MAX+1];
private:
	bool				this_transport(clconf_path_t *, bool);

	transport(const transport &);
	transport &operator = (transport &);
};

#include <orb/transport/transport_in.h>

#endif	/* _TRANSPORT_H */
