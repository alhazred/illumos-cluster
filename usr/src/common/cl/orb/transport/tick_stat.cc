//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)tick_stat.cc	1.10	08/05/20 SMI"

//	tick_stat class, keeps statistics of ticks = periodic events.
//
// The high-resolution  gethrtime() feature  is used for  timing.  The
// tick  statistics   can  be  read  from  user-land   via  the  kstat
// framework. All counter units are microseconds, except for "last". A
// fault point can be enabled such that the node panics when ticks are
// late by a certain percentage.   This is useful for the debugging of
// spurious path timeout problems.

#include <orb/transport/tick_stat.h>
#include <orb/transport/path_manager.h>// for access to PM_DBG(), HB_DBG()


//	constructor.

//	Takes an identifying string as argument, and a delay factor. The delay
//	factor determines  how much the  tick can be  late until a  panic will
//	happen. (Note that  the panic will only occur  if the respective fault
//	is triggered). Let's  say you have armed the fault  with a 20% allowed
//	delay, and  the tick stat  has a delay  factor of 2, then  the maximum
//	permitted delay (relative  to the average tick length)  will be 2*20 =
//	40%. Note that the identifying  string  MUST  NOT BE FREED, i.e. it is
//	assumed to be a string literal.

tick_stat::tick_stat(const char *name_str, const uint32_t allowed_delay_fac) :
	max_len(0),
	min_len(1000000000),
	record_count(0),
	last(0),
	total_len(0),
	total_sq_len(0),
#ifdef	USE_KSTATS
	kstat_p(NULL),
#endif
	allowed_delay_factor(allowed_delay_fac)
{
	ASSERT(name_str != NULL);	// insist on name
	name = name_str;

#ifdef	USE_KSTATS
	// set up the kstats
	initialize_kstat();
#endif
	// print pointers to the tick stats, to aid adb debugging
	print_pointers();
}

//	destructor

tick_stat::~tick_stat()
{
#ifdef	USE_KSTATS
	// remove kstat from kstat framework, free resources
	delete_kstat();
#endif
	name = NULL;	// make lint happy
}


//	update()

// 	This  function  updates the  tick  statistics,  and should  be
// 	called every time  a tick occurs. It stores  the current time,
// 	records  the  time  difference  from the  previous  call,  and
// 	maintains a  sum of  all tick lengths,  a sum of  the squares,
// 	average, and variance.  If a tick comes in late, and the fault
// 	point is switched on, the node will be panicked.

void
tick_stat::update()
{
	hrtime_t	current;
	uint64_t	diff;


	lock.lock();
	current	= os::gethrtime();
	ASSERT(current != 0);
	if (last == 0) {
		// first call, initialize
		last = current;
		lock.unlock();
		return;
	}

	ASSERT(current >= last);	// 64bit - should never see wrap around

	diff		= ((uint64_t)(current - last)) / 1000;
	last		= current;
	total_len	+= diff;
	total_sq_len	+= diff * diff;
	record_count	++;
	average		= total_len / record_count;


	if (max_len < diff) {
		max_len = diff;
#ifdef	FAULT_ALL
		verify("update", diff);	// verify that tick length is o.k.
#endif	// FAULT_ALL
	}

	if (min_len > diff) {
		min_len = diff;
	}

	ASSERT(min_len <= max_len);
	ASSERT(min_len <= average && average <= max_len);

	variance = total_sq_len / record_count - average * average;

	lock.unlock();
}

//	check()

//	Checks  tick statistics.  This  function is  typically invoked
//	from the clock  interrupt thread, and tests if  there has been
//	an undue delay  in the tick update.  If a  tick comes in late,
//	and the fault point is switched on, the node will be panicked.

void
tick_stat::check()
{
	hrtime_t	current	= os::gethrtime();
	uint64_t	diff;

	// it is o.k. to take the lock here, since other threads hold this
	// lock only very briefly, and will quickly finish due to priority
	// inheritance.

	lock.lock();
	if (record_count == 0) {
		// first tick has not been recorded yet, return
		lock.unlock();
		return;
	}
	diff	= ((uint64_t)(current - last)) / 1000;

#ifdef	FAULT_ALL
	verify("check", diff);	// verify that tick length is o.k.
#endif

	lock.unlock();
}

#ifdef	USE_KSTATS
//	initialize_kstat()

//	Creates, initializes, and installs the kstats.

void tick_stat::initialize_kstat()
{
	tick_kstat_t	*kp = NULL;	// pointer to private kstat data

	// create unique name for this kstat by appending the "this" pointer
	ASSERT(sizeof (kstat_name) < KSTAT_STRLEN);
	os::sprintf(kstat_name, "%s%p", TICK_STAT_KS_PREFIX, this);

	// allocate named kstat structure
	kstat_p = kstat_create((char *)name, 0, kstat_name, TICK_STAT_KS_CLASS,
				KSTAT_TYPE_NAMED,
				(uint_t) (sizeof (tick_kstat_t) /
				sizeof (kstat_named_t)), 0);
	ASSERT(kstat_p != NULL);

	// initialize the data section of the kstat
	kp	= (tick_kstat_t *)kstat_p->ks_data;
	ASSERT(kp != NULL);

	kstat_named_init(&kp->tk_average,	"average",  KSTAT_DATA_UINT64);
	kstat_named_init(&kp->tk_variance,	"variance", KSTAT_DATA_UINT64);
	kstat_named_init(&kp->tk_max,		"max",	    KSTAT_DATA_UINT64);
	kstat_named_init(&kp->tk_min,		"min",	    KSTAT_DATA_UINT64);
	kstat_named_init(&kp->tk_count,		"count",    KSTAT_DATA_UINT64);

	// point the private pointer to this object
	kstat_p->ks_private = this;

	// point the kstat framework to the update function. This
	// function is called whenever the kstats are read from userland.
	// At that point, update_kstat copies the local stats into the kstats.
	kstat_p->ks_update = update_kstat;

	// initialization complete, start using the kstat
	kstat_install(kstat_p);

}

//	update_kstat()

//	This  (static!)  function is  called  by  the kstat  framework
//	whenever  the kstats  are read  from userland.  It  copies the
//	local stats into the kstats.

int tick_stat::update_kstat(kstat_t *ksp, int rw)
{
	ASSERT(ksp != NULL);

	if (rw == KSTAT_WRITE) {
		// don't allow to write the kstats
		return (EACCES);
	} else {
		// request to read the kstats, go ahead and do it
		tick_stat	*tsp;	// pointer to tick stats
		tick_kstat_t	*kp;	// pointer to private kstat info

		tsp = (tick_stat *)ksp->ks_private;
		ASSERT(tsp != NULL);
		kp  = (tick_kstat_t *)ksp->ks_data;
		ASSERT(kp != NULL);

		tsp->lock.lock();
		kp->tk_average.value.ui64	= tsp->average;
		kp->tk_variance.value.ui64	= tsp->variance;
		kp->tk_max.value.ui64		= tsp->max_len;
		kp->tk_min.value.ui64		= tsp->min_len;
		kp->tk_count.value.ui64		= tsp->record_count;
		tsp->lock.unlock();

		return (0);
	}
}

//	delete_kstat()

//	Delete the tick kstat from the Solaris kstat framework

void tick_stat::delete_kstat()
{
	ASSERT(kstat_p != NULL);
	kstat_delete(kstat_p);		// also frees all associated resources
	kstat_p = NULL;
}
#endif	// USE_KSTATS


//	print_pointers()

//	prints out pointers to the tick statistics, such that they can
//	be dumped out more easily from within adb/kadb/mdb

void
tick_stat::print_pointers()
{
	PM_DBG(("%s: tick (%p) avg (%p) var(%p) max(%p) min(%p) count(%p)\n",
		name, this, &average, &variance, &max_len, &min_len,
		&record_count));
}

#ifdef	FAULT_ALL
//	verify()

//	Verify that the current tick length is not too long. This only
//	happens if  the corresponding  fault point is  triggered. Takes
//	informational  string and  current  tick length as arguments.

void
tick_stat::verify(const char *infostring, uint64_t current_tick_len)
{
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(FAULTNUM_TICK_DELAY_PANIC, &fault_argp,
				&fault_argsize)) {
		uint64_t	slop;
		uint32_t	max_percent_delay;
		ASSERT(fault_argp != NULL);
		ASSERT(fault_argsize == sizeof (uint32_t));

		max_percent_delay = *((uint32_t *)fault_argp)
			* allowed_delay_factor;
		// the allowed delay is relative to the average
		slop = (average / 100) * max_percent_delay;

		if (current_tick_len > average + slop) {
			PM_DBG(("%s: tick (%p) is late (%s): tick_len = %llu,"
			" avg= %llu, slop = %llu.\n", name, this,
			infostring, current_tick_len, average, slop));

			os::panic("%s: tick (%p) is late (%s): tick_len = "
				"%llu, avg= %llu, slop = %llu.\n", name, this,
				infostring, current_tick_len, average, slop);
		}

	}
}
#endif	// FAULT_ALL
