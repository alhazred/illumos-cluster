//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sdoor_transport.cc	1.33	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <orb/transport/sdoor_transport.h>
#include <sys/mman.h>

//
// Free the overflow Buf
//
void
DoorOverflowBuf::done()
{
	(void) munmap((char *)base_address(), length());
	Buf::done();
}

//
// destructor - Needs to be public so that sendstreams can be allocated on the
// stack in solaris_xdoor_server::object_server.  Should be changed
// to protected if/when the stack allocation is removed.
sdoor_sendstream::~sdoor_sendstream()
{
	MainBuf.dispose();
}

void
sdoor_sendstream::send(bool, orb_seq_t &)
{
	//
	// SCMSGS
	// @explanation
	// This operation should never occur.
	// @user_action
	// Contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "clcomm: sdoor_sendstream::send");
}

//
// exception_prepare - An exception occurred. Eliminate all data.
// XXX - Normally a new sendstream is created for the exception msg.
// XXX - However, user land currently must reuse the sendstream.
//
void
sdoor_sendstream::exception_prepare()
{
	MainBuf.prepare();
	// trim_end calls done() on all buffers except the first one
	// to preserve the headers. Adjusts last ptr on first buffer to
	// discard any data
	MainBuf.trim_end();

	XdoorTab.dispose();
	OffLineBufs.dispose();
	xdoorcount = 0;
}

void
sdoor_sendstream::done()
{
	if (alloc_type == Buf::HEAP) {
		delete this;
	} else {
		// We do this here and not in destructor as
		// sendstream's destructor as that destructor
		// is not called in some cases where we do a door_return
		MainBuf.dispose();
		XdoorTab.dispose();
		OffLineBufs.dispose();
		xdoorcount = 0;
	}
}

// sdoor_recstream methods
sdoor_recstream::~sdoor_recstream()
{
}

// Method declared pure virtual in base class, not applicable to this class
bool
sdoor_recstream::initialize(os::mem_alloc_type)
{
	ASSERT(0);
	return (false);
}

void
sdoor_recstream::done()
{
	if (alloc_type == Buf::HEAP) {
		delete this;
	} else {
		// We do this here and not in destructor as
		// sendstream's destructor as that destructor
		// is not called in some cases where we do a door_return
		MainBuf.dispose();
		XdoorTab.dispose();
		OffLineBufs.dispose();
		xdoorcount = 0;
	}
}

//
// reserve_resources - Identify resources needed at transport level for request.
// Returns a send stream pointer, which can be NULL in case of error.
//
sendstream *
sdoor_transport::reserve_resources(resources *resourcep)
{
	//
	// The sdoor transport uses no header.
	//
	// Local messages are not flow controlled.
	//
	/*CSTYLED*/
	return (new (resourcep->get_invo_mode().nonblocking_type())
	    sdoor_sendstream(resourcep));
}

sdoor_sendstream::sdoor_sendstream(resources *resourceptr) :
	sendstream(resourceptr, &MainBuf),
	MainBuf(resourceptr->get_env(), resourceptr->send_buffer_size()),
	alloc_type(Buf::HEAP)
{
	uint_t hdr_size = MainBuf.alloc_chunk(resourceptr->send_header_size());
	ASSERT(hdr_size == resourceptr->send_header_size() ||
	    CORBA::WOULDBLOCK::_exnarrow(get_env()->exception()));
}

//
// sdoor_sendstream constructor - currently this sendstream is
// only used for 2-way messages (see recstream constructor).
// Messages using this sendstream type do not have a message header that
// contains information differentiating oneway and twoway messages.
//
sdoor_sendstream::sdoor_sendstream(Buf::alloc_type flag, Buf *b,
    resources *resourceptr) :
	sendstream(resourceptr, &MainBuf),
	MainBuf(b, resourceptr->get_env()),
	alloc_type(flag)
{
	uint_t hdr_size = MainBuf.alloc_chunk(resourceptr->send_header_size());
	ASSERT(hdr_size == resourceptr->send_header_size() ||
	    CORBA::WOULDBLOCK::_exnarrow(get_env()->exception()));
}
