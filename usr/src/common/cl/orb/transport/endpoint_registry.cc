//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)endpoint_registry.cc	1.43	08/10/13 SMI"

#include <orb/invo/invocation.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/msg/orb_msg.h>
#include <orb/transport/transport.h>
#include <orb/transport/path_manager.h>
#include <sys/mc_probe.h>

#if defined(_FAULT_INJECTION)
#include <cmm/cmm_impl.h>
#endif

#ifdef	ER_DBGBUF
uint32_t er_dbg_size = 10000;
dbg_print_buf er_dbg(er_dbg_size);
#endif

endpoint_registry *endpoint_registry::the_endpoint_registry = NULL;

er_if::er_if(uint_t r) : refcnt(r), _SList::ListElem(this)
{
}

int
endpoint_registry::initialize()
{
	ASSERT(the_endpoint_registry == NULL);
	the_endpoint_registry = new endpoint_registry();
	ASSERT(the_endpoint_registry != NULL);
	the_endpoint_registry->hold();
	ER_DBG(("endpoint_registry(%x) initialized\n", the_endpoint_registry));
	return (0);
}

// XXX Is there a real way to make sure there aren't invocations hanging
// around in here?
void
endpoint_registry::shutdown()
{
	if (the_endpoint_registry == NULL) {
		return;
	}
#ifdef	DEBUG
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		the_endpoint_registry->ninfo[i].nlock.lock();
		ASSERT(the_endpoint_registry->ninfo[i].the_eps.empty());
		the_endpoint_registry->ninfo[i].nlock.unlock();
	}
#endif	// DEBUG

	the_endpoint_registry->rele();
	ER_DBG(("endpoint_registry(%x) shutdown\n", the_endpoint_registry));
	the_endpoint_registry = NULL;
}

endpoint_registry::endpoint_registry() :
    cm(lone_membership::the())
{
	cm->hold();
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		ninfo[i].iter.reinit(ninfo[i].the_eps);
		regmbrs.members[i] = INCN_UNKNOWN;
	}
}

endpoint_registry::~endpoint_registry()
{
	ASSERT(the_endpoint_registry != this);
	cm->rele();
	cm = NULL;
}

//
// External interface for cl_orb
//
extern "C" sendstream *
endpoint_reserve_resources(resources *resourcep)
{
	return (endpoint_registry::the().reserve_resources(resourcep));
}

sendstream *
endpoint_registry::reserve_resources(resources *resourcep)
{
	MC_PROBE_0(endpoint_reg_reserve_start, "clustering orb message", "");
	Environment	*e = resourcep->get_env();
	ID_node		*np = resourcep->get_nodep();
	nodeid_t	n = np->ndid;
	er_if		*epp = NULL;
	sendstream	*sp;
	endpoint	*prefer_epp = NULL;
#ifdef _KERNEL_ORB
	serv2_transport_info *tip = NULL;
#endif


	// CMM thread that drives reconfiguration should not send messages.
	// The Version Manager is closely enough linked with the CMM to
	// avoid deadlocks.

#if defined(_FAULT_INJECTION)

	// Enables fault message to send across nodes even though the
	// fault points are located in code path of non-blocking
	// context of a component.

	if ((resourcep->get_send_msgtype() != VM_MSG) &&
	    (resourcep->get_send_msgtype() != FAULT_MSG)) {
#else
	if (resourcep->get_send_msgtype() != VM_MSG) {
#endif // _FAULT_INJECTION
		os::envchk::check_nonblocking(os::envchk::NB_RECONF,
		    "endpoint_registry::reserve_resources called during "
		    "cmm reconfigure ");
	}

	hold();

	// There should be no exceptions when we reach reserve_resources
	ASSERT(e->exception() == NULL);
	ASSERT((n > 0) && (n <= NODEID_MAX));

	// For non-blocking invocations, we currently require them to be
	// marked as unreliable also.
	// RFE needed to support nonblocking reliable invocations
	ASSERT(!(e->is_nonblocking()) ||
		resourcep->get_invo_mode().is_unreliable());

	// ER_DBG(("reserve_resources called: nid=%u, incn=%u, msg_type=%u\n",
	// n, np->incn, resourcep->get_send_msgtype()));

#ifdef	DEBUG
	//
	// It is possible to have already acquired this lock when the
	// message type is STOP_MSG.
	//
	if (!ninfo[n].nlock.lock_held()) {
		ninfo[n].nlock.lock();
	}
#else
	ninfo[n].nlock.lock();
#endif

#ifdef _KERNEL_ORB
	// If this is a reply for a two way invocation, the corresponding
	// two way service object might carry an endpoint preference.
	// Try using this endpoint.

	service_twoway *serv2 = NULL;
	if (resourcep->get_send_msgtype() == REPLY_MSG &&
	    (serv2 = resourcep->get_service_twoway()) != NULL) {
		tip = serv2->get_transport_info();
		if (tip->isvalid()) {
			prefer_epp = tip->get_endpoint();
			ASSERT(prefer_epp != NULL);
			if (ninfo[n].the_eps.exists((er_if *)prefer_epp)) {
				// The preferred endpoint is available, use it
				epp = prefer_epp;
				goto endpoint_chosen;
			} else {
				// The preferred endpoint is gone. Invalidate
				// the transport info that has a pointer to
				// the endpoint.

				tip->invalidate();
				prefer_epp = NULL;

				// Now proceed with an endpoint selection as
				// if there were no preferred endpoint.
			}
		}
	}
#endif

	if (orb_msg::is_cmm_msg(resourcep->get_send_msgtype())) {
		if (ninfo[n].the_eps.empty())
			goto comm_failure;
	} else {
		// If the node is dead, return COMM_FAILURE
		if (cm->membership().members[n] == INCN_UNKNOWN)
			goto comm_failure;

		// Define the incn if there isn't one defined.
		if (np->incn == INCN_UNKNOWN)
			np->incn = cm->membership().members[n];

		//
		// If the chosen incarnation does not match the incn
		// of the currently registered endpoints, or the current
		// incarnation has been declared unreachable, wait until
		// the node is declared dead, then return COMM_FAILURE.
		//
		if ((np->incn != regmbrs.members[n]) ||
		    ninfo[n].the_eps.empty()) {
#if defined(_FAULT_INJECTION)
			//
			// Fault injection messages can be sent by the
			// transitions thread doing node reconfiguration work.
			// These messages must fail immediately or deadlock
			// will result.
			//
			if ((resourcep->get_send_msgtype() == FAULT_MSG) &&
			    (os::thread::self() ==
				cmm_impl::get_transition_thread_id())) {
					goto comm_failure;
			}
#endif

#ifdef DEBUG
			//
			// This is a message to bring down all the nodes
			// in the cluster. Return comm_failure here.
			//
			if (resourcep->get_send_msgtype() == STOP_MSG) {
				goto comm_failure;
			}
#endif	// DEBUG
			//
			// If the message is a version manager message, the
			// version manager will wait for the cmm to cancel
			// inside the cmm thread, so just return comm failure
			// directly.
			//
			if (resourcep->get_send_msgtype() == VM_MSG) {
				goto comm_failure;

			//
			// Goto comm_failure for COMPLETED_MAYBE_RESOLVE_MSG
			// message type. Such scenarios can occur when a
			// fault message was sent and the orb code tries
			// to determine the successful delivery of the
			// message. A change in the incarnation number
			// means that the target node went down and came
			// up (very quickly) and hence we can return
			// comm_failure directly, else deadlock will occur
			//
			} else if (resourcep->get_send_msgtype() ==
			    COMPLETED_MAYBE_RESOLVE_MSG) {
				goto comm_failure;
			//
			// Return immediately with WOULDBLOCK if there are
			// no endpoints, but the remote node has not been
			// declared down.
			//
			} else if (e->is_nonblocking()) {
				if (cm->membership().members[n] != np->incn) {
					goto comm_failure;
				}
				e->system_exception(CORBA::WOULDBLOCK(0,
				    CORBA::COMPLETED_NO));
				ninfo[n].nlock.unlock();
				rele();
				return (NULL);
			}
			while (cm->membership().members[n] == np->incn)
				ninfo[n].fail_cv.wait(&(ninfo[n].nlock));
			goto comm_failure;
		}
		ASSERT((np->incn == cm->membership().members[n]) &&
		    (np->incn == regmbrs.members[n]));
	}
	ASSERT(!ninfo[n].the_eps.empty());
	if ((epp = ninfo[n].iter.get_current()) == NULL) {
		ninfo[n].iter.reinit(ninfo[n].the_eps);
		epp = ninfo[n].iter.get_current();
	}
	ninfo[n].iter.advance();

endpoint_chosen:
	ASSERT(epp != NULL);
	epp->hold();
	ninfo[n].nlock.unlock();
	epp->add_resources(resourcep);
	resourcep->flow_control();
	if (e->exception() &&
	    (CORBA::WOULDBLOCK::_exnarrow(e->exception()) ||
	    CORBA::COMM_FAILURE::_exnarrow(e->exception()))) {
		epp->rele();
		return (NULL);
	}
	sp = epp->get_sendstream(resourcep);
	epp->rele();
	rele();
	MC_PROBE_0(endpoint_reg_reserve_end, "clustering orb message", "");
	return (sp);

comm_failure:
	e->system_exception(CORBA::COMM_FAILURE(0,
	    CORBA::COMPLETED_NO));
	ninfo[n].nlock.unlock();
	rele();
	return (NULL);
}

void
endpoint_registry::set_membership(callback_membership *m)
{
	ASSERT(m->membership().members[orb_conf::node_number()] ==
	    orb_conf::local_incarnation());

	callback_membership *oldmbrs = cm;
	nodeid_t i;
	incarnation_num incn;

	ER_DBG(("set_membership called\n"));

	m->hold();

	for (i = 1; i <= NODEID_MAX; i++)
		ninfo[i].nlock.lock();
	cm = m;
	for (i = 1; i <= NODEID_MAX; i++)
		ninfo[i].nlock.unlock();

	for (i = 1; i <= NODEID_MAX; i++) {
		if (m->membership().members[i] !=
		    oldmbrs->membership().members[i]) {
			if ((incn = oldmbrs->membership().members[i]) !=
			    INCN_UNKNOWN)
				(void) path_manager::the().node_died(i, incn);
			if ((incn = m->membership().members[i]) !=
			    INCN_UNKNOWN)
				(void) path_manager::the().node_alive(i, incn);
			ninfo[i].nlock.lock();
			ninfo[i].fail_cv.broadcast();
			ninfo[i].nlock.unlock();
		}
	}
	oldmbrs->rele();
}

bool
endpoint_registry::register_endpoint(er_if *epp, nodeid_t nid,
    incarnation_num incn)
{
	ER_DBG(("register_endpoint(%x) called\n", epp));
	ninfo[nid].nlock.lock();
	if (ninfo[nid].the_eps.empty()) {
		if ((regmbrs.members[nid] != INCN_UNKNOWN) &&
		    (regmbrs.members[nid] >= incn)) {
			ninfo[nid].nlock.unlock();
			return (false);
		}
		regmbrs.members[nid] = incn;
		if (nid != orb_conf::node_number())
			path_manager::the().node_is_reachable(nid, incn);
	}
	ASSERT(regmbrs.members[nid] == incn);
	ninfo[nid].the_eps.prepend(epp);
	epp->hold();

	//
	// Make a callback into the endpoint so that the endpoint can set
	// its state to E_REGISTERED. This call must be made before dropping
	// the node lock so that we do not start giving away references
	// to this endpoint to the ORB before the endpoint has moved to the
	// E_REGISTERED state.
	//
	epp->registration_callback();

	ninfo[nid].nlock.unlock();
	return (true);
}

void
endpoint_registry::unregister_endpoint(er_if *epp, nodeid_t nid)
{
	ER_DBG(("unregister_endpoint(%x) called\n", epp));
	ninfo[nid].nlock.lock();
	ASSERT(ninfo[nid].the_eps.exists(epp));
	(void) ninfo[nid].the_eps.erase(epp);
	ninfo[nid].iter.reinit(ninfo[nid].the_eps);
	epp->rele();
	if (ninfo[nid].the_eps.empty())
		if (nid != orb_conf::node_number())
			path_manager::the().node_is_unreachable(nid,
			    regmbrs.members[nid]);
	ninfo[nid].nlock.unlock();
}

#if defined(_FAULT_INJECTION)
//
// Entry point for cl_orb
//
extern "C" uint_t
num_endpoints(nodeid_t nid)
{
	return (endpoint_registry::the().num_endpoints(nid));
}

//
// Returns number of endpoints to a particular node.
//
uint_t
endpoint_registry::num_endpoints(nodeid_t nid)
{
	ASSERT(nid != NODEID_UNKNOWN);

	uint_t	count;

	ninfo[nid].nlock.lock();
	count = ninfo[nid].the_eps.count();
	ninfo[nid].nlock.unlock();
	return (count);
}
#endif // _FAULT_INJECTION
