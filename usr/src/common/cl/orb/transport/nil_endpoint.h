/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _NIL_ENDPOINT_H
#define	_NIL_ENDPOINT_H

#pragma ident	"@(#)nil_endpoint.h	1.51	08/05/20 SMI"

#include <orb/buffers/Buf.h>
#include <orb/buffers/marshalstream.h>
#include <orb/flow/resource.h>
#include <orb/transport/endpoint_registry.h>

//
// For nil recstream
//
class nil_recstream : public recstream {
public:
	ID_node &get_src_node();

	bool	initialize(os::mem_alloc_type);

	nil_recstream(orb_msgtype, const invocation_mode &, uint_t,
	    Region &, Region &, Region &);

private:
	// Disallow assignments and pass by value
	nil_recstream(const nil_recstream &);
	nil_recstream &operator = (nil_recstream &);
};

class nil_sendstream : public sendstream {
public:
	ID_node		&get_dest_node();

	void	send(bool, orb_seq_t &);

	// for local invocations
	nil_recstream	*make_recstream();

	nil_sendstream(resources *new_resourcep);

protected:
	~nil_sendstream();

private:
	MarshalStream	MainBuf;

	// Disallow assignments and pass by value
	nil_sendstream(const nil_sendstream &);
	nil_sendstream &operator = (nil_sendstream &);
};

extern "C" nil_recstream *make_recstream(nil_sendstream *);

//
// nil_endpoint - this is a dummy endpoint, which exists primarily to support
// an equivalent interface for the nil version of the sendstream.
// The nil_endpoint supports specifying the resources required for a message
// and providing a sendstream.
//

class nil_endpoint : public er_if  {
public:
	static int	initialize();
	static void	shutdown();

	void		registration_callback();
	void 		add_resources(resources *resourcep);
	sendstream	*get_sendstream(resources *resourcep);

private:
	nil_endpoint();
	~nil_endpoint();

	static nil_endpoint *the_nil_endpoint;

	// Disallow assignments and pass by value
	nil_endpoint(const nil_endpoint&);
	nil_endpoint& operator = (nil_endpoint&);
};

#endif	/* _NIL_ENDPOINT_H */
