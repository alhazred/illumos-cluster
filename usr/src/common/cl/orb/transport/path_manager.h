/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _PATH_MANAGER_H
#define	_PATH_MANAGER_H

#pragma ident	"@(#)path_manager.h	1.91	08/05/27 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <orb/buffers/marshalstream.h>
#include <orb/infrastructure/orbthreads.h>
#include <sys/mc_probe.h>
#include <sys/clconf_int.h>
#include <orb/transport/pm_client.h>
#include <cmm/cmm_impl.h>
#include <orb/debug/haci_debug.h>
#include <sys/cmn_err.h>
#include <sys/heartbeat.h>
#include <sys/cyclic.h>
#include <orb/transport/hb_threadpool.h>

extern "C" void cluster_heartbeat(void);
extern "C" void pm_register_cmm(cmm_impl *cmmp);
extern "C" void pm_unregister_cmm(cmm_impl *cmmp);
extern "C" bool pm_enable(void);
extern "C" bool pm_disable(void);
extern "C" uint32_t pm_get_max_delay(nodeid_t);
extern "C" bool pm_add_path(clconf_path_t *);
extern "C" bool pm_remove_path(clconf_path_t *);

#if defined(_FAULT_INJECTION)
extern "C" int fault_revoke_pathend(int, nodeid_t);
#endif

// Macro  defined  means: verify  that  proper  ordering  of calls  is
// maintained,   e.g.   after    a   node_is_reachable()   follows   a
// node_is_unreachable().
#ifdef	DEBUG
#define	PM_VERIFY_ORDER
#endif

// Macro  defined  means:  check   that  the  path manager cyclic  is  running
// properly,  and   if  not,  failfast  the  node.   Don't  check  for
// cyclic running in unode.
#ifdef	_KERNEL
#define	CHECK_FOR_PM_CYCLIC_RUNNING
#endif

// Macro defined  means: update and  check the tick  statistics, which
// measure  the  quality  of   the  heartbeats,  and  are  useful  for
// debugging.
#if	defined(DEBUG) || defined(FAULT_TRANSPORT)
#define	USE_TICKSTATS
#include <orb/transport/tick_stat.h>
#endif

// Test for proper heartbeat wrapping
#if	defined(DEBUG) && defined(FAULT_TRANSPORT)
#define	WRAP_TEST
#endif


#if defined(HACI_DBGBUFS) && !defined(NO_PM_DBGBUF)
#define	PM_DBGBUF
#endif

#ifdef	PM_DBGBUF
extern dbg_print_buf pm_dbg;
#define	PM_DBG(a) HACI_DEBUG(ENABLE_PM_DBG, pm_dbg, a)
#else
#define	PM_DBG(a)
#endif

#if defined(HACI_DBGBUFS) && !defined(NO_PMHB_DBGBUF)
#define	PMHB_DBGBUF
#endif

// If pm_tick() is not executed regularly, then it is possible that
// hrlast falls a lot behind hrcurrent. The cyclic subsystem will do
// calls to make up, but if heartbeat processing is called from network
// interrupt context, it is best to catch up as soon as possible.  With
// REPEAT_TO_CATCHUP defined, pm_tick loops to process more than one
// slot.
#define	REPEAT_TO_CATCHUP

// Heartbeat buffers are per pathend buffers (except in the unode environment)
// to facilitate debugging path timeout problems. In case of unode dynamically
// allocated debug buffers are not supported yet, hence only one unified
// statically allocated buffer is used.
#ifdef	PMHB_DBGBUF
#if defined(_KERNEL)
#define	PERPE_PMHB_DBGBUF
#define	HB_DBG(pe, a)	HACI_DEBUG(ENABLE_HB_DBG, (*(pe)->hb_dbgp), a)
#else
extern dbg_print_buf	hb_dbg;
#define	HB_DBG(pe, a)	HACI_DEBUG(ENABLE_HB_DBG, hb_dbg, a)
#endif	// _KERNEL
#else
#define	HB_DBG(pe, a)
#endif	// PMHB_DBGBUF


//			The Path Manager Basics:

// The path manager is responsible for notifying pm_clients (which
// include transports and other things that need to keep track of the
// status of paths) of adapter- and path- related events, and monitoring
// the status of paths after they have been initialized in the transports
// The fundamental objects used for monitoring path are pathends.
// Pathends are registered with the pm by the transports when the latter
// establish connections with the remote node.  Each pathend is associated
// with a clconf_path, nodeid and incarnation, and each is reference counted.
// The registration of pathends can be rejected if the node has already been
// declared down or the path removed.  In addition the registration of a
// pathend to a new incarnation will cause the old incarnation to be
// declared unreachable.  Paths are automatically declared up when their
// corresponding pathends are successfully registered.
//
// To detect failure of an interconnect or of a node, a heartbeat
// mechanism is used.  Heartbeat messages are sent periodically across each
// path, and if heartbeat messages are not received in a timely fashion, the
// path is declared down. Any action that causes a path to
// be declared down can cause the  number of endpoints to that node to
// go  to  zero,  which  will  cause the  endpoint  registry  to  call
// node_is_unreachable on the pm.  Node_is_unreachable() in turn calls
// the cmm to  inform it that the node has  become unreachable, and to
// tell the  cmm when  it can be  sure that  the remote node  has also
// called node_is_unreachable().   To ensure that the  other node will
// get  a  node_is_unreachable()  soon, the  unreach_mbrs  incarnation
// number is  set such  that pm_tick() will  bring the path
// down.
//
// Interactive disable/enable of path monitoring can be done using
// /usr/cluster/bin/cmm_ctl.
//
// Heartbeat Processing Detail
// ----------------
//
// Heartbeat message sending and checking is driven by a Solaris cyclic
// started by the pm constructor and running at clock level interrupt context.
// The cyclic executes on the cpu where the cyclic initialization was done.
// Currently, no attempt is made to determine which cpu will be used.
// Each cyclic interval the path_manager::pm_tick method is called. This method
// manages the scheduling of heartbeat sending, and the checking of received
// heartbeats.
//
// Each heartbeat message consists of a 32 bit local
// token and a 32 bit remote token.  The remote token is not interpreted
// by the local node except that zero is ignored.  The local token consists
// of an arbitrary time in units of token ticks.  By passing the local token
// to the remote node in the message, and then having it pass the token back,
// the pm can be sure that the remote node has successfully exchanged a pm
// message within the timeout period associated with the path. If messages
// from node 1 to 2, for instance, are not getting through, node 1 will
// receive old tokens from node 2 and will declare the path down.  Node 2
// will not receive any new tokens from node 1 and will declare the path
// down.
//
// The fundamental parameters used by pm_tick for managing heartbeats
// are the quantum, which controls how often heartbeats are sent, and the
// timeout, which controls how much time must pass before a path is
// declared down.  A low quantum increases CPU and network loads because
// more messages are sent and received.  A low timeout value increases the
// speed at which failures can be detected, but also increases the risk
// of a path timeout due to a transient load.  The quantum should be
// at least a factor of 3 smaller than the timeout to ensure a round trip
// can easily happen within the timeout interval.  Also, the path manager
// uses the notion of a pm tick (length equal to the cyclic interval) and
// a heartbeat token tick which is currently 1/4 of the pm tick length.
// The token tick is what is included in the inter-node heartbeat messages.
//
//
// Each path is represented by a path_record, which is allocated when
// add_path is called by the topology manager, and deallocated when
// remove_path is called.  Groups of path_records with the same inter-
// message quantum are combined into path_group objects.  The path_groups
// are maintained in an array of lists used for monitoring (sending/checking)
// heartbeats. The current scheduling array position is stored between calls.
// Each pm tick, each path_group in the current list is reaped and each of
// the paths that have an associated pathend is monitored; which consists
// of (1) scheduling the sending of a heartbeat to the remote end; (2)
// checking the last received heartbeat from the remote; and (3) if the
// node has been marked as unreacheable, take the path down.
// Then, if the quantum is less than the size of the
// array, the path_group is placed on the scheduling array list at position
// ((current_position + quantum) % array_size).  If the quantum is larger
// than the array size, the path_group is placed on a long-term list
// together with the time at which the path_group should next be checked.
// Then the array position is incremented.  When the array position exceeds
// the size of the array, the long-term list is reaped, and each path_group
// is either placed back on the long-term list with an updated execution
// time, or it is put into the appropriate schedule slot.  Then the array
// position is updated to point to the 0 slot.
//
// The heartbeat timeout and quantum for each node and adapter are stored
// in the cmm infrastructure file (/etc/cluster/ccr/global/infrastructure) in
// milliseconds.
//
//
// The default values for a transport come from the clpl file (/etc/cluster/
// clpl).  the clpl files also define minimum and maximum values.
//



// Lock Ordering
// Here is the transport-system-wide lock ordering, highest to lowest:
// path_manager::timeouts_lock
// path_manager::block_lock
// path_manager::mem_lock
// path_manager::pm_cyclic_lock
// path_manager::sched_lock
// transport::node_lock
// endpoint::lock
// pathend::lock
// endpoint_registry::ninfo[nodeid].nlock
// path_manager::unreach_sched_lock

//  		The pm cyclic failfast mechanism
//
// In  theory, the  clockthread  should run  every  10ms. In  pratice,
// clockthread delays as  long as 10 seconds have  been observed.  The
// problems are due to an interrupt thread hanging on to the CPU for a
// prolonged time.   As of this  writing 8/19/99, Solaris 7  (599) and
// Solaris  8 have  a design  flaw  in that  the clock  thread can  be
// blocked on  the callout  table lock, which  can be held  by another
// thread,  which in  turn could  be  pinned by  an interrupt  thread,
// which,  if  the  corresponding  driver  is  poorly  written,  could
// therefore hold up the clock thread for an arbitrary amount of time.
// If this happens, the path manager cyclic is also affected and
// the  path manager's  heartbeat messaging  will be
// disrupted, and it is best to kill the affected node before problems
// spread to the rest of  the cluster, and e.g.  another, healthy node
// is  kicked out of  the cluster  by the  CMM. Therefore,  a failfast
// mechanism is in place, which  panics the machine the first time the
// path manager cyclic runs again. The maximum allowed delay for the
// path manager cyclic was picked to be  about 1/2 of the minimum path timeout
// to any node.
//
// To accomodate this, the clhbsndr module is pushed on network streams
// so that when a cyclic disruption is detected, pm_tick is called
// immediately at network interrupt level. Also to deal with the case
// where the real time heartbeat threads are prevented from sending
// scheduled heartbeats because of heavy interrupt load, any scheduled
// heartbeats are sent immediately in network interrupt context.
//
//
// With regard to the clock thread bug, forcing the heartbeat cyclic to
// execute on a different cp than the clock cyclic would resolve the
// problem of falling behind in pm tick processing for SMP configurations
// but not UP. Even if the clock thread bug is fixed there remains the
// problem of sending the heartbeats.  We'd like to send
// heartbeats directly from the cyclic thread and eliminate the need for
// the realtime threads (and clhbsndr) but don't want to call into the streams
// modules at a high interrupt level because there is a chance for
// blocking (with the processor interrupt level remaining high).
//


// The size of pm messages, and the size of the buffer in the argument to
// pm_get and pm_put
#define	PM_MESSAGE_SIZE		8

// These are both used as initializers for array members of classes, so
// constants wouldn't work
// The number of path_records in a path_group
#define	PM_PA_GROUP_SIZE	5
// The number of adapter ids in an adapter_group
#define	PM_AD_GROUP_SIZE	5
// The size of the scheduling array
#define	PM_SCHED_SIZE		20
// The number of high resolution ticks per second.
#define	HR_TICKS_PER_MS		(1000000LL)

class pm_if : public refcnt {
public:
	// argument is initial reference count
	pm_if(uint_t);

	// Various reasons for which a path can be dropped
	enum path_drop_reason {PDR_NONE = 0, PDR_EP_FAULT, PDR_HB_TIMEOUT,
	    PDR_REMOVED, PDR_PEER_DEAD, PDR_DISCONNECTED, PDR_REVOKED,
	    PDR_REVOKED_FI, PDR_MISC};


	// retrieve a pm message (pointer points to size PM_MESSAGE_SIZE)
	virtual void pm_get(void * const) = 0;
	// send a pm message (pointer points to size PM_MESSAGE_SIZE)
	virtual void pm_put(const void * const) = 0;
	// retrieve the nodeid and incarnation associated with the pathend
	virtual const ID_node &node() const = 0;
	// retrieve the clconf_path pointer associated with the pathend
	virtual clconf_path_t *path() const = 0;
	// retrieve the pathend external name
	virtual const char *get_extname() const = 0;
	// determine if the associated endpoint is faulted
	virtual bool is_ep_faulted() = 0;
	// Determines whether the pathend associated with the clconf_path_t
	// is in P_UP state.
	virtual bool is_pe_up() = 0;
	// record pathend drop information
	virtual void record_drop_info(path_drop_reason) = 0;
#ifdef PERPE_PMHB_DBGBUF
	// retrieve the heartbeat debug buffer associated with the pathend
	virtual const dbg_print_buf *get_hb_dbg() const = 0;
#endif
#ifdef	USE_TICKSTATS
	// check the tick stats, panic if they are out of spec.
	virtual	void pm_check_tickstats() = 0;
#endif
#ifdef  WRAP_TEST
	// copy the potentially wrapped local heartbeat back to rec_buf
	virtual void pm_wrap(void * const) = 0;
#endif
};

typedef union {
	uint64_t hb;
	struct {
		uint32_t remote;
		uint32_t local;
	} put;
	struct {
		uint32_t local;
		uint32_t remote;
	} get;
} pm_heartbeat;


// path_record could just as easily be a struct.  It's only used in the
// path_manager
class path_record {
public:
	path_record();
	~path_record();

	// associate a pathend with the path
	void add_pathend(pm_if *, uint32_t);
	// disassociate the pathend associated with the path
	void drop_pathend(pm_if::path_drop_reason);

	// locking : cpp and tout can be read holding either the mem_lock
	// or the sched_lock, and are written holding both.  pep, local_token
	// and remote_token must be read and written holding the sched_lock

	// If cpp is NULL, this record is unused.  Otherwise it corresponds
	// to the path pointed to.
	clconf_path_t	*cpp;
	// Assuming cpp is not NULL, if pep is NULL there is no registered
	// pathend, and the path is down.  If it isn't NULL there is a pathend
	// and the path is up.
	pm_if		*pep;
	// The timeout associated with the path, in units of token ticks.
	uint32_t	tout;
	// The most recent local token we received from the remote node
	uint32_t	local_token;
	// The most recent remote token we received from the remote node.
	uint32_t	remote_token;
};

class path_group {
public:
	// The argument is the quantum, in pm ticks.
	path_group(uint32_t);
	~path_group();

	// locking : quantum is constant. pg_le can be read with either the
	// mem_lock or the sched_lock held, and is written holding both.  pra
	// has the rules associated with the path_record.  The other fields
	// are read and written holding the sched_lock.

	// the inter-message quantum, in units of pm ticks
	const uint32_t		quantum;
	// The scheduling array list element
	_SList::ListElem	schedule_le;
	// The path group list element
	_SList::ListElem	pg_le;
	// The time left until the next send, when on the long-term
	// scheduling list
	uint_t			timeleft;
	// The array of path_record structures
	path_record		pra[PM_PA_GROUP_SIZE];
};

//
// adapter_groups are arrays of adapter_ids (which are clconf_obj_ids, defined
// as ints in sys/clconf_int.h) that can be placed into a list.  We store the
// ids in arrays because the ids are small, so we gain both speed (because of
// spatial cache locality) and save memory (because storing the adapters
// together reduces memory allocation overhead).
//
// Read the comment near ag_list below to understand how and why adapter_ids
// must be stored.

class adapter_group : public _SList::ListElem {
public:
	adapter_group();
	~adapter_group();

	int			adapter_id[PM_AD_GROUP_SIZE];
};

typedef	IntrList<pm_client, _SList> pmclist_t;

class path_manager : public pm_client {
public:
	// Transports are stored on a different list, to allow better
	// error checking.
	enum client_type { PM_TRANSPORT, PM_OTHER };

	enum cyclic_caller_t { PM_CYCLIC_THRD, NET_INTRPT_THRD };

	// called from initial.cc
	static	int		initialize();
		void		shutdown();

	static	path_manager&	the();
	static	bool		is_initialized();

	// called from initialize to start the pm heartbeat cyclic
	void			start_pm_cyclic();

	// Called in cmm::initiate to give the pm a pointer to the cmm
	// for notification.  We're not doing this through CORBA just
	// to keep the possible complications down.  The cmm will register
	// after the pm initiates.  dec_ref is called in shutdown.
	void			register_cmm(cmm_impl *);

	// Cancel a CMM registration
	void			unregister_cmm(cmm_impl *);

	void wait_for_cmm();

	//
	// Called from the transport to indicate an intention to subsequently
	// register a pathend from this node incarnation. This call is
	// significant when the new pathend is from a node that has booted up
	// with a new incarnation. In this case the path manager needs to make
	// a disconnect_node call on the previous incarnation.
	//
	// This step was added to make the path manager aware of a new
	// incarnation of a node before the transport calls into the
	// version manager for version verification.
	//
	bool	update_node_incarnation(nodeid_t, incarnation_num);

	//
	// Called from the transport to register a pathend.  The pathend
	// lock is passed separately.
	//
	bool			register_pathend(pm_if *, os::mutex_t &);

	// Called to revoke a pathend.  If the pathend is registered, the
	// path will be declared down.  Otherwise, this is a no-op.
	void			revoke_pathend(pm_if *);

#ifdef	USE_TICKSTATS
	// Checks the tick stats for all active pathends. If no tick
	// has happened for a sufficiently long time, and if the
	// corresponding fault is activated, panic. Called from
	// pm_tick (cyclic at lock level).
	void			monitor_pathend_ticks();
#endif

	// Called from the handler registered with the Solaris cyclic
	// subsystem and also from network interrupt context to schedule
	// the sending of, and checking the receiving of heartbeats.
	void			pm_tick(cyclic_caller_t);

	// Utility routine provided by the path manager that pathends can
	// use to determine is a new heartbeat message handed over to it
	// by the transport is more recent than the one that it has already
	// accepted earlier. The first parameter is the pointer to a buffer
	// with the new heartbeat, the second is the pointer to the current
	// heartbeat.
	bool	pm_isnewer(const void *, const void *);

	// Called from the endpoint registry when the number of endpoints to a
	// given node drops to zero. It "notes" the occurance in unreach_mbrs,
	// which is checked later in  pm_tick() and then drops all
	// pathends to that node.  This is done to avoid lock ordering.
	// violations.
	void			node_is_unreachable(nodeid_t, incarnation_num);

	// Called from dlpi connection when a panic packet is received
	void			node_is_panicking(nodeid_t, incarnation_num);

	// Called from the endpoint registry when the number of endpoints
	// to a given node increases to one.  This is delivered directly
	// to the cmm.
	void			node_is_reachable(nodeid_t, incarnation_num);

	// Called from specific transports to reject connection attempts from
	// a node that has been declared unreachable but has not rebooted with
	// a new incarnation number. Returns the most recent incarnation number
	// that has been declared unreachable for the given node, INCN_UNKNOWN
	// if the node has never been declared unreachable.
	// Transports are not required but are encouraged to reject stale
	// connection attempts. Even if a transport allows one such connection
	// to proceed, the pathend will be rejected by the path manager during
	// registration.
	incarnation_num		get_unreachable_incn(nodeid_t);

	// Called from the endpoint registry when a given incarnation of a
	// node is declared dead by the CMM.  The pm drops any pathends
	// associated with the node and circulates the news among all the
	// pm_clients
	bool	node_died(nodeid_t, incarnation_num);

	// Called from the endpoint registry when a given incarnation of a
	// node is declared alive by the CMM.  The pm circulates the news
	// among all the pm_clients
	bool	node_alive(nodeid_t, incarnation_num);

	// Called from the topology manager to add a new path.  The pm
	// allocates a path_record for the path and circulates the news
	// among all the pm_clients
	bool	add_path(clconf_path_t *);

	path_record	*create_pathrec(clconf_path_t *, uint32_t, uint32_t);

	// Called from the topology manager to add a new adapter.  The pm
	// circulates the news among all the pm_clients
	bool	add_adapter(clconf_adapter_t *);

	// Called from the topology manager when an already existing
	// adapter property is changed
	bool	change_adapter(clconf_adapter_t *);

	// Called from the topology manager to remove an adapter.  The pm
	// circulates the news among all the pm_clients
	bool	remove_adapter(clconf_adapter_t *);

	// Called from the topology manager to remove a path.  The pm
	// drops the pathend associated with the connection (if there is
	// one) and deallocates the path_record for the path.  Then it
	// circulates the news among all the pm_clients
	bool	remove_path(clconf_path_t *);

	// Called from the topology manager to change a paths's properties,
	// like heartbeat timeout/quantum. If quantum is changed, path
	// record will be moved from old quantum group to new quantum
	// group. If only timeout is changed then the value is updated
	// in its place.
	// This method is NOT inherited from pm_client for now as currently
	// pm_clients have nothing to do when the path properties changed.
	bool	change_path(clconf_path_t *, clconf_path_t *);

	// Add a pm_client to the pm
	void 	add_client(client_type, pm_client *);

	// Remove a pm_client from the pm
	void 	remove_client(client_type, pm_client *);

	// Enable or disable the timeouts on pm messages
	bool	disable();
	bool	enable();
	bool	is_enabled();

	// return the maximum timeout on any configured path to the argument
	// node.
	uint32_t get_max_delay(nodeid_t);
	void update_maxtimeouts(clconf_path_t *, uint32_t, uint32_t);

#if defined(_FAULT_INJECTION)
	// Method to support the "failpath" test utility: given a
	// local adapter ID and a remote node ID, fail the associated pathend.
	// This method is very similar to revoke_pathend().
	// Returns:
	//	0		- success.
	//	ECANCELED	- the pathend is already down.
	//	ENOENT		- the pathend doesn't exist.
	//
	int	fault_revoke_pathend(int, nodeid_t);
#endif

#ifdef DEBUGGER_PRINT
	void get_paths(int);
	void debugger_print();
#endif

private:
	// The argument is the number of pm ticks per second
	path_manager(uint_t);
	virtual ~path_manager();

	// Check to verify if a remote node incarnation is stale. Return
	// value is true if the incarnation is stale. The third argument
	// is an out parameter and is set to true upon return from the
	// method if the passed incarnation is deemed to be stale because
	// this node has already heard from a newer incarnation of the
	// remote node.
	bool	stale_incn(nodeid_t, incarnation_num, bool *);

	// Internal functions that do the work, similar to iterate_over()
	// in the replica manager
	bool	node_call(pmclist_t &, pm_node_func, nodeid_t, incarnation_num);
	bool	adapter_call(pmclist_t &, pm_adapter_func, clconf_adapter_t *);
	bool	path_call(pmclist_t &, pm_path_func, clconf_path_t *);

	// This is here to satisfy the pm_client interface.  Calling it
	// will cause a panic (the pm doesn't need to register with itself).
	_SList::ListElem *get_pmlist_elem();

	// Notifies the pm_clients that the pm has established
	// communication on this path to the remote node.  This is called
	// during register_pathend
	bool	path_up(clconf_path_t *);

	// The pm uses path_down to notify the pm_clients that it has lost
	// communication on this path to the remote node.  The pathend will be
	// dropped before it is called.
	bool	path_down(clconf_path_t *);

	// The pm uses disconnect_node to tell the pm_clients to
	// declare any paths to the incarnation of the node down.
	// All relevant pathends will be dropped before it is called.
	bool	disconnect_node(nodeid_t, incarnation_num);

	// Determine whether the pathend associated with the clconf_path
	// is up
	bool	is_path_up(clconf_path_t *);

	// Find minimum timeout (in nanoseconds) of all paths to all nodes.
	// excluding the path that is passed in.
	// Returns UINT_MAX * HR_TICKS_PER_MS if there are no paths.
	hrtime_t	get_min_timeout(clconf_path_t *);

	// Find maximum timeout (in milliseconds) of all paths to nodeid
	// excluding the path that is passed in.
	// Returns 0 if there are no paths.
	uint32_t	get_max_timeout(clconf_path_t *, nodeid_t nodeid);


	// Release the pathend associated with the path_record (if any) and
	// record the timeout for the cmm.
	void	drop_pathend(path_record &, pm_if::path_drop_reason);

	// translates milliseconds into pm ticks
	uint32_t	msectopmt(uint32_t);
	// translates milliseconds to token ticks
	uint32_t	msectotoken(uint32_t);
	// translates high resolution time into token ticks
	uint32_t	hrtotoken(hrtime_t);

	// Returns true if max_clockthread_delay needs to be reduced due
	// to new path timeout of tout_nsec.
	bool	lessthan_max_clockthread_delay(hrtime_t tout_nsec);
	// Returns true if max_clockthread_delay needs to be increased due
	// to removal of path timeout of tout_nsec.
	bool	lessthanorequal_max_clockthread_delay(hrtime_t tout_nsec);
	// Updates max_clockthread_delay with new value
	void	update_max_clockthread_delay(hrtime_t tout_nsec);

	static path_manager *the_path_manager;

	// The lists of pm_clients that have been registered
	pmclist_t transport_list;
	pmclist_t other_list;

	// The list of all groups of path records
	IntrList<path_group, _SList> pg_list;
	// The scheduling array
	IntrList<path_group, _SList> send_schedule[PM_SCHED_SIZE];
	IntrList<path_group, _SList> long_schedule;

	// The list of all groups of adapter ids, protected by the mem_lock
	//
	// We store the list of all adapters (by adapter_id) that have been
	// added so that when a new pm_client registers, we can "replay" the
	// list of adapters back into the client in the form of add_adapter
	// calls.
	//
	// XXX Note that because we must convert the adapter_ids back into
	// clconf_adapter_t objects, we make clconf calls.  There is no
	// guaranteed synchronization between the addition of clients and
	// ccr callbacks that might change the clconf tree.  This means we
	// could wind up needing to look up an adapter that isn't there.
	// Until the design of clconf changes, this problem will be difficult
	// to fix unless we grab a reference to the clconf tree and store the
	// clconf_adapter_t pointer itself, which could be a big memory leak.
	//
	// The ag_list is arranged in such a way that any free slots are
	// always at the end of the first adapter_group in the list (that is,
	// in the higher array slots).  The variable last_adapter_list_slot,
	// defined below, is set to the array location of the highest used
	// slot in the first adapter_group (or, if the list is empty, it is
	// set to PM_AD_GROUP_SIZE-1, the index of the highest slot in a group,
	// for the sake of consistency).  When adding an adapter, if
	// last_adapter_list_slot is less than PM_AD_GROUP_SIZE-1, then we
	// simply put the adapter id in the next highest slot of the first
	// adapter_group and increment last_adapter_list_slot.  Otherwise,
	// we allocate a new adapter_group, put it on the head of the list,
	// put the adapter_id into slot 0, and set last_adapter_list_slot to
	// 0.
	//
	// When we remove an adapter, if last_adapter_list_slot is not 0,
	// we find the slot containing the adapter_id corresponding to the
	// adapter being removed, and then swap the adapter_id in the first
	// adapter_group at index last_adapter_list_slot into that slot, and
	// decrement last_adapter_list_slot.  If last_adapter_list_slot is 0,
	// we still swap the only element in the first adapter group into the
	// slot of the adapter being removed, but then we delete the first
	// adapter_group and set last_adapter_list_slot to PM_AD_GROUP_SIZE-1.
	// (If we are deleting the last adapter_id group in the whole list,
	// or if we happen to be deleting the last adapter_id in the first
	// adapter_group, we don't swap anything and instead simply delete
	// the first group and set last_adapter_list_slot to PM_AD_GROUP_SIZE-1.
	// Note that when there are no adapters left, we will be in the same
	// state as we started in).
	//
	// In summary, we keep any free slots at the end of the first
	// adapter_group, and whenever an adapter is removed, we just fill
	// the hole with the last element in the first group, deleting that
	// group if it becomes empty.

	IntrList<adapter_group, _SList> ag_list;

	// path manager cyclic identifier
	cyclic_id_t	pm_cyclic;

	// time when the network interrupt should call heartbeat processing
	hrtime_t 	net_intr_run_time;

	// the delta added to current time to get net_intr_run_time value
	hrtime_t	net_intr_run_delta;

	uint32_t net_intr_count;
	uint32_t pm_cyclic_count;

	// The incarnations of the most recently registered pathends
	cmm::membership_t mbrshp;
	// true if there is a registered pathend for that node, false otherwise
	bool	alive[NODEID_MAX+1];

	//
	// The incarnation numbers expected from registering pathends. This
	// array keeps track of the incarnation numbers carried by the
	// version negotiation messages. A pathend register will be allowed
	// only from the incarnations that were agreed upon at the time
	// of version negotiation.
	//
	cmm::membership_t expected_mbrs;

	//
	// The last incarnation numbers for each node for which the path
	// manager has already syslogged a stale incarnation message. This
	// is used to avoid logging the same message every time a path
	// establishment attempt is received from the same stale incarnation
	// of a node.
	//
	cmm::membership_t logged_stale_mbrs;

	// The unreachable nodes. Signals to pm_tick() to bring the path
	// down.
	cmm::membership_t unreach_mbrs;

	// The membership we have received from the endpoint registry via
	// calls to node_alive and node_died (for add_client).  Protected
	// by the mem_lock.
	cmm::membership_t add_client_mbrs;

	// The time in the future at which the remote node is guaranteed to
	// notice the connection between the nodes has failed.
	hrtime_t	node_timeouts[NODEID_MAX+1];

	// The maximum timeouts the pm has seen to a given node.
	uint32_t	max_timeouts[NODEID_MAX+1];

	// The length, in milliseconds, of pm tick
	uint32_t pmtlen;
	// The length, in milliseconds, of a token tick
	uint32_t tokentlen;
	// The length, in nanoseconds, of one clock tick
	hrtime_t hrtlen;
	// The high resolution time the pm last went through the scheduling
	// array
	hrtime_t hrlast;
	// The current position in the scheduling array
	uint_t schedule_slot;
	uint_t last_adapter_list_slot;
	// The current value of the local_token the pm is passing the
	// remote node
	uint32_t current_token;

	// The value of maximum possible token value divided by two, used
	// in determining whether a current token is newer that the last
	// one seen wrt wraparounds. This is defined as UINT32_MAX/2 in the
	// constructor.
	const uint32_t max_token_div_2;

	// The maximum time (in nanoseconds) for which the clock thread
	// can be delayed before the node failfasts
	hrtime_t	max_clockthread_delay;

#ifdef REPEAT_TO_CATCHUP
	// ncatchup shows No. of times hrlast caught up with hrcurrent
	// (for statistical purpose only)
	uint32_t ncatchup;
#endif

	// The pm's pointer to the cmm.  Protected by the sched_lock.
	cmm_impl *pm_cmmp;

	os::condvar_t	pm_cmm_wait_cv;

	// path manager cyclic lock
	os::mutex_t	pm_cyclic_lock;

	// The lock grabbed in pm_tick()
	os::mutex_t	sched_lock;

	// the lock used to synchronize disable and enable of timeouts
	os::mutex_t	timeouts_lock;

	// Lock that guards everything that is grabbed in node_is_unreachable.
	// This lock is also grabbed by pm_tick(), so you cannot wait on
	// anything while holding this lock.
	os::mutex_t	unreach_sched_lock;

	// The lock grabbed by calls on the pm_client interface that
	// should only block to allocate memory or for other short operations,
	// and by register_pathend and revoke_pathend
	os::mutex_t	mem_lock;
	// The lock grabbed by calls on the pm_client interface that might
	// block for longer periods of time (like remove_path) and by
	// register_ and remove_client
	os::mutex_t	block_lock;
#ifdef	USE_TICKSTATS
	// path manager cyclic statistics.
	tick_stat	pm_cyclic_stat;
#endif

#ifdef	PM_VERIFY_ORDER
	// The  order_bitvec class  is  used to  verify  that per-node  events
	// happen   in   the   right   order.   For  instance,   a   call   to
	// node_is_reachable()    must   be    followed   by    a    call   to
	// node_is_unreachable().    So  inside   those  two   functions,  the
	// order_bitvec  class is used  to keep  track of  which one  has been
	// called last, by setting and  clearing the bits corresponding to the
	// nodeid. It causes  an assertion failure if the  proper order is not
	// maintained.

	class	order_bitvec {
	public:
		order_bitvec();
		~order_bitvec();
		void set(nodeid_t	nid);
		void clear(nodeid_t	nid);
	private:
		uint64_t	bitvec;
		os::mutex_t	bitlock;
		// Disable compiler's default copy constructor and bitwise
		// assignment
		order_bitvec(const order_bitvec &);
		order_bitvec &operator = (order_bitvec &);
	};

	// Bitvector to assert that node_is_reachable() and
	// node_is_unreachable() are called in the proper order.
	order_bitvec	reachable_order_bitvec;
#define	PM_MARK_SET(nid, obitvec) obitvec.set(nid)
#define	PM_MARK_CLEAR(nid, obitvec) obitvec.clear(nid)
#else	// PM_VERIFY_ORDER
#define	PM_MARK_SET(nid, obitvec)
#define	PM_MARK_CLEAR(nid, obitvec)
#endif


#ifdef	DEBUG
	// The pair (pm_lbolt, pm_hrt) can be used to get the relation between
	// hrtime and lbolt, for debugging.
	clock_t  pm_lbolt;	// lbolt,  updated in pm_tick, for debugging
	hrtime_t pm_hrt;	// hrtime, updated in pm_tick, in sync with
				// lbolt.
#endif	// DEBUG

	bool	timeouts_enabled;

	// Disallow assignments and pass by value
	path_manager(const path_manager &);
	path_manager &operator = (path_manager &);
};

#include <orb/transport/path_manager_in.h>

#endif	/* _PATH_MANAGER_H */
