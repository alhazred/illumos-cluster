/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  hb_threadpool_in.h
 *
 */

#ifndef _HB_THREADPOOL_IN_H
#define	_HB_THREADPOOL_IN_H

#pragma ident	"@(#)hb_threadpool_in.h	1.4	08/05/20 SMI"

inline void
hb_threadpool::lock()
{
	threadlock.lock();
}

inline void
hb_threadpool::unlock()
{
	threadlock.unlock();
}

inline bool
hb_threadpool::lock_held()
{
	return (threadlock.lock_held());
}

inline hb_threadpool &
hb_threadpool::the()
{
	ASSERT(the_heartbeat_threadpool != NULL);
	return (*the_heartbeat_threadpool);
}

#endif	/* _HB_THREADPOOL_IN_H */
