/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  sdoor_transport_in.h
 *
 */

#ifndef _SDOOR_TRANSPORT_IN_H
#define	_SDOOR_TRANSPORT_IN_H

#pragma ident	"@(#)sdoor_transport_in.h	1.10	08/05/20 SMI"

// DoorOverflow Buf constructor - set the data_alloc type to USER_CONTEXT
// This points to mmapped data as part of this door call
// bflag - indicates where the Buf itself was allocated
inline
DoorOverflowBuf::DoorOverflowBuf(void *v, size_t l, alloc_type bflag) :
	Buf((uchar_t *)v, l, bflag, USER_CONTEXT)
{
}

inline
sdoor_recstream::sdoor_recstream(Buf::alloc_type t, orb_msgtype msgt) :
	recstream(msgt, invocation_mode::NONE),	// Only 2way msg supported now
	alloc_type(t)
{
}

#endif	/* _SDOOR_TRANSPORT_IN_H */
