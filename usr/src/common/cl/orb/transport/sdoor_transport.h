/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SDOOR_TRANSPORT_H
#define	_SDOOR_TRANSPORT_H

#pragma ident	"@(#)sdoor_transport.h	1.33	08/05/20 SMI"

#include <orb/buffers/Buf.h>
#include <orb/buffers/marshalstream.h>
#include <orb/flow/resource.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

//
// Door Buffer Management
//

//
// Used to describe overflow area from door_call (mmapped region)
//
class DoorOverflowBuf: public Buf {
public:
	// constructor
	// alloc_type indicates where the Buf itself was allocated
	DoorOverflowBuf(void *v, size_t l, alloc_type = HEAP);
protected:
	void	done();
private:
	// Disallow pass by value, assignments
	DoorOverflowBuf(const DoorOverflowBuf &);
	DoorOverflowBuf &operator = (const DoorOverflowBuf &);
};

//
// Service definitions for doors. Some control of the marshal engine
// is gained in this manner.
//
// XXX We could allocate the first default Bufs for MainBuf and
//	XdoorTab as part of this object.
//
class sdoor_sendstream: public sendstream {
public:
	sdoor_sendstream(resources *);
	sdoor_sendstream(Buf::alloc_type, Buf *, resources *);

	~sdoor_sendstream();

	void		send(bool, orb_seq_t &);
	//
	// An exception occurred. Eliminate all data.
	// XXX - Normally a new sendstream is created for the exception msg.
	// XXX - However, user land currently must reuse the sendstream.
	//
	void	exception_prepare();

	void	done();			// Destructor

private:
	MarshalStream		MainBuf;
	Buf::alloc_type		alloc_type;

	// Disallow pass by value and assignments
	sdoor_sendstream(const sdoor_sendstream &);
	sdoor_sendstream &operator = (const sdoor_sendstream &);
};

class sdoor_recstream: public recstream {
public:
	sdoor_recstream(Buf::alloc_type, orb_msgtype);

	// Needs to be public so that recstreams can be allocated on the
	// stack in solaris_xdoor_server::object_server.  Can be changed
	// to protected if/when the stack allocation is removed.
	~sdoor_recstream();

	void		done();		// Destructor

	bool	initialize(os::mem_alloc_type);
private:
	Buf::alloc_type		alloc_type;		// Allocated with new?

	// Disallow pass by value and assignments
	sdoor_recstream(const sdoor_recstream &);
	sdoor_recstream &operator = (const sdoor_recstream &);
};

//
// sdoor_transport - this transport supports user <-> kernel communications.
// There are no nodes in the sdoor transport, as it supports local
// communications only. The sdoor transport supports
// specifying the resources required for a message and providing a send stream.
//
class sdoor_transport {
public:
	//
	// Identify resources needed at transport level for request.
	//
	static sendstream *reserve_resources(resources *resourcep);

private:
	// Disallow assignments and pass by value
	sdoor_transport(const sdoor_transport&);
	sdoor_transport& operator = (sdoor_transport&);
};

#include <orb/transport/sdoor_transport_in.h>

#endif	/* _SDOOR_TRANSPORT_H */
