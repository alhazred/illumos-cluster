/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  path_manager_in.h
 *
 */

#ifndef _PATH_MANAGER_IN_H
#define	_PATH_MANAGER_IN_H

#pragma ident	"@(#)path_manager_in.h	1.26	08/05/20 SMI"

inline
adapter_group::adapter_group() :
	_SList::ListElem(this)
{
	for (int i = 0; i < PM_AD_GROUP_SIZE; i++)
		adapter_id[i] = CLCONF_ID_UNKNOWN;
}

inline
adapter_group::~adapter_group()
{
#ifdef	DEBUG
	for (int i = 0; i < PM_AD_GROUP_SIZE; i++)
		ASSERT(adapter_id[i] == CLCONF_ID_UNKNOWN);
#endif
}

inline path_manager &
path_manager::the(void)
{
	ASSERT(the_path_manager != NULL);
	return (*the_path_manager);
}

inline bool
path_manager::is_initialized(void)
{
	return (the_path_manager != NULL);
}

inline uint32_t
path_manager::msectopmt(uint32_t msecs)
{
	if (msecs >= pmtlen) {
		return (msecs / pmtlen);
	} else {
		ASSERT(msecs >= pmtlen);
		return (1);
	}
}

inline uint32_t
path_manager::msectotoken(uint32_t msecs)
{
	ASSERT(msecs >= tokentlen);
	return (msecs / tokentlen);
}

inline uint32_t
path_manager::hrtotoken(hrtime_t hrt)
{
	uint32_t retval = (uint32_t)(hrt / (tokentlen * HR_TICKS_PER_MS));
	if (retval == 0)
		return (1);
	else
		return (retval);
}

inline void
path_manager::register_cmm(cmm_impl *cmmp)
{
	sched_lock.lock();
	ASSERT(pm_cmmp == NULL);
	ASSERT(cmmp != NULL);
	pm_cmmp = cmmp;
	pm_cmm_wait_cv.broadcast();
	sched_lock.unlock();
	PM_DBG(("registered cmm (%p)\n", pm_cmmp));
}

inline void
path_manager::unregister_cmm(cmm_impl *cmmp)
{
	sched_lock.lock();
	if (pm_cmmp == cmmp) {
		PM_DBG(("unregistered cmm (%p)\n", pm_cmmp));
		pm_cmmp = NULL;
	}
	sched_lock.unlock();
}

inline incarnation_num
path_manager::get_unreachable_incn(nodeid_t ndid)
{
	return (unreach_mbrs.members[ndid]);
}

#endif	/* _PATH_MANAGER_IN_H */
