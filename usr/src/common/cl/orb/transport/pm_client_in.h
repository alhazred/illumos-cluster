/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  pm_client_in.h
 *
 */

#ifndef _PM_CLIENT_IN_H
#define	_PM_CLIENT_IN_H

#pragma ident	"@(#)pm_client_in.h	1.4	08/05/20 SMI"

inline
pm_client::pm_client() :
	pmlist_elem(this)
{
}

inline _SList::ListElem *
pm_client::get_pmlist_elem()
{
	return (&pmlist_elem);
}

#endif	/* _PM_CLIENT_IN_H */
