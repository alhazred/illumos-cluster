/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _TICK_STAT_H
#define	_TICK_STAT_H

#pragma ident	"@(#)tick_stat.h	1.9	08/05/20 SMI"

// This header file is included by both the user-land reader of kernel
// tick statistics, and the kernel components that create the stats.

#include <sys/types.h>
#include <sys/os.h>

// Unode does not support kstats, so don't use there
#if	!defined(_KERNEL_ORB) || defined(_KERNEL)	// not unode
#define	USE_KSTATS
#endif

//	Pack  version info into  the class  name.  The  version number
//	needs to  be bumped whenever  the tick_kstat_t is  changed, as
//	the  kstat reader's  data  layout  must be  in  sync with  the
//	kernel's. Do not modify  the TICK_STAT_CLASS, since that's how
//	the kstat reader finds the module.

#ifdef	USE_KSTATS
#include <sys/kstat.h>
#define	TICK_STAT_VERSION	"1.0"
#define	TICK_STAT_CLASS		"tick_v"
#define	TICK_STAT_KS_CLASS  TICK_STAT_CLASS TICK_STAT_VERSION
#define	TICK_STAT_KS_PREFIX	"tick_ks:"

//	data structure used to share data between kernel and user land

typedef struct {
	kstat_named_t	tk_average;
	kstat_named_t	tk_variance;
	kstat_named_t	tk_max;
	kstat_named_t	tk_min;
	kstat_named_t	tk_count;
} tick_kstat_t;
#endif	// USE_KSTATS

//	tick_stat class, keeps statistics of ticks = periodic events.
//
// The high-resolution  gethrtime() feature  is used for  timing.  The
// tick  statistics   can  be  read  from  user-land   via  the  kstat
// framework. All counter units are microseconds, except for "last". A
// fault point can be enabled such that the node panics when ticks are
// late by a certain percentage.   This is useful for the debugging of
// spurious path timeout problems.

#ifdef	_KERNEL_ORB	// unode or kernel
#include <orb/fault/fault_injection.h>

class tick_stat {
public:

//	Takes  an   identifying  string  as  argument,   and  a  delay
//	factor. The delay  factor determines how much the  tick can be
//	late until a panic will happen. (Note that the panic will only
//	occur  if the respective  fault is  triggered). Let's  say you
//	have armed  the fault with a  20% allowed delay,  and the tick
//	stat has a delay factor of 2, then the maximum permitted delay
//	(relative to the average tick length) will be 2*20 = 40%. Note
//	that the  identifying  string  MUST  NOT  BE FREED, i.e. it is
//	assumed to be a string literal.
	tick_stat(const char *name_str, const uint32_t allowed_delay_fac);

//	destructor frees kstat resources
	~tick_stat();

// 	This  function  updates the  tick  statistics,  and should  be
// 	called every time  a tick occurs. It stores  the current time,
// 	records  the  time  difference  from the  previous  call,  and
// 	maintains a  sum of  all tick lengths,  a sum of  the squares,
// 	average, and variance.  If a tick comes in late, and the fault
// 	point is switched on, the node will be panicked.
	void update();

//	Checks  tick statistics.  This  function is  typically invoked
//	from the clock  interrupt thread, and tests if  there has been
//	an undue delay  in the tick update.  If a  tick comes in late,
//	and the fault point is switched on, the node will be panicked.
	void check();

private:

#ifdef	USE_KSTATS
//	Creates, initializes, and installs the kstats.
	void tick_stat::initialize_kstat();

//	This  (static!)  function is  called  by  the kstat  framework
//	whenever  the kstats  are read  from userland.  It  copies the
//	local stats into the kstats.
	static int tick_stat::update_kstat(kstat_t *ksp, int rw);

//	Delete the tick kstat from the Solaris kstat framework
	void tick_stat::delete_kstat();
#endif	// USE_KSTATS

//	prints out pointers to the tick statistics, such that they can
//	be dumped out more easily from within adb/kadb/mdb
	void	print_pointers();

#ifdef	FAULT_ALL
//	Verify that the current tick length is not too long. This only
//	happens if  the corresponding  fault point is  triggered. Takes
//	informational  string and  current  tick length as arguments.
	void	verify(const char *infostring, uint64_t current_tick_len);
#endif	// FAULT_ALL

// data members
	uint64_t	average;	// average tick length
	uint64_t	variance;	// variance
	uint64_t	max_len;	// max tick len recorded
	uint64_t	min_len;	// min tick len recorded
	uint64_t	record_count;	// bumped every time update() is called
	const char	*name;		// pointer to name
	hrtime_t	last;		// hrtime during last update
	uint64_t	total_len;	// sum of all tick lengths
	uint64_t	total_sq_len;	// sum of squares of all tick lengths
	os::mutex_t	lock;		// protects the tick stats
#ifdef	USE_KSTATS
	kstat_t		*kstat_p;	// pointer to kernel stat (kstat)
#endif
	uint32_t	allowed_delay_factor; // delay factor for fault panic
#ifdef	USE_KSTATS
	// XXX the space provided below will be o.k. so long as the "%p"
	// format prints out two characters per byte
	char		kstat_name[sizeof (TICK_STAT_KS_PREFIX)
			+ 2 * sizeof (size_t) + 1]; // prefix + pointer
#endif

	tick_stat(const tick_stat &);		// no copy constructor
	tick_stat &operator = (tick_stat &);	// no pass-by-value
	tick_stat();				// no default constructor

};
#endif	// _KERNEL_ORB


#endif	/* _TICK_STAT_H */
