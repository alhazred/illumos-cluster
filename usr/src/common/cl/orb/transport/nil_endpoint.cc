/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)nil_endpoint.cc	1.58	08/05/20 SMI"

#include <orb/transport/nil_endpoint.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>

nil_endpoint *nil_endpoint::the_nil_endpoint = NULL;

//
// nil_sendstream methods
//

nil_sendstream::nil_sendstream(resources *resourceptr) :
	sendstream(resourceptr, &MainBuf),
	MainBuf(resourceptr->get_env(), resourceptr->send_buffer_size())
{
	uint_t hdr_size = MainBuf.alloc_chunk(resourceptr->send_header_size());
	ASSERT(hdr_size == resourceptr->send_header_size() ||
	    CORBA::WOULDBLOCK::_exnarrow(get_env()->exception()));
}

nil_sendstream::~nil_sendstream()
{
	MainBuf.dispose();
}

//
// Accessor function for external kernel module
//
extern "C" nil_recstream *
make_recstream(nil_sendstream *se)
{
	return (se->make_recstream());
}


// Convert current sendstream into a recstream by moving all the data
// (and xdoors) and adjusting the xdoorcount.
nil_recstream *
nil_sendstream::make_recstream()
{
	nil_recstream	*re = new nil_recstream(resourcep->get_send_msgtype(),
	    resourcep->get_invo_mode(), xdoorcount, MainBuf.region(),
	    XdoorTab.region(), OffLineBufs);
	ASSERT(MainBuf.empty());
	ASSERT(XdoorTab.empty());
	ASSERT(OffLineBufs.empty());
	xdoorcount = 0;
	return (re);
}

ID_node &
nil_sendstream::get_dest_node()
{
	return (orb_conf::current_id_node());
}

void
nil_sendstream::send(bool, orb_seq_t &)
{
	//
	// SCMSGS
	// @explanation
	// The system attempted to use a "send" operation for a local
	// invocation. Local invocations do not use a "send" operation.
	// @user_action
	// Contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "clcomm: nil_sendstream::send");
}

//
// nil_recstream methods
//

nil_recstream::nil_recstream(orb_msgtype msgt, const invocation_mode &mode,
    uint_t xdcnt, Region &data, Region &xdoors, Region &off) :
	recstream(msgt, mode)
{
	// Initializing variables in base class recstream
	MainBuf.useregion(data);
	XdoorTab.useregion(xdoors);
	xdoorcount = xdcnt;
	OffLineBufs.concat(off); // Transfer contents from off to OffLineBufs
	// All the incoming Regions have transferred data to the recstream
	// and must be empty now
	ASSERT(off.empty());
	ASSERT(data.empty());
	ASSERT(xdoors.empty());
}

// Method declared pure virtual in base class, not applicable to this class
bool
nil_recstream::initialize(os::mem_alloc_type)
{
	ASSERT(0);
	return (false);
}

ID_node &
nil_recstream::get_src_node()
{
	return (orb_conf::current_id_node());
}

//
// nil_endpoint methods
//

int
nil_endpoint::initialize()
{
	ASSERT(the_nil_endpoint == NULL);
	the_nil_endpoint = new nil_endpoint();
	ASSERT(the_nil_endpoint != NULL);
	// This register does not fail
	(void) endpoint_registry::the().register_endpoint(the_nil_endpoint,
	    orb_conf::local_nodeid(), orb_conf::local_incarnation());
	return (0);
}

nil_endpoint::nil_endpoint() : er_if(0)
{
}

void
nil_endpoint::shutdown()
{
	ASSERT(the_nil_endpoint != NULL);
	// the unregister will cause the endpoint to get unreferenced
	// and delete itself
	endpoint_registry::the().unregister_endpoint(the_nil_endpoint,
	    orb_conf::local_nodeid());
	the_nil_endpoint = NULL;
}

nil_endpoint::~nil_endpoint()
{
}

//
// Callback from register_endpoint. No action is needed.
//
void
nil_endpoint::registration_callback()
{
}

//
// Add the needed resources into the resource object.  Currently does nothing.
//
void
nil_endpoint::add_resources(resources *)
{
}

//
// Returns a send stream pointer, which can be NULL in case of error.
//
sendstream *
nil_endpoint::get_sendstream(resources *resourcep)
{
	//
	// The nil_endpoint itself uses no header.
	//
	// Local messages are not flow controlled.
	//
	//
	// the node can always communicate with itself.
	// Allocate a buffer with at least the requested number of bytes.
	// Header and Data must be word size multiples.
	// The start of data buffer cursor is advanced past the bytes reserved
	// for the headers.
	//

	Environment	*e = resourcep->get_env();
	nil_sendstream	*sendstreamp;

	/*CSTYLED*/
	sendstreamp = new (resourcep->get_invo_mode().nonblocking_type())
	    nil_sendstream(resourcep);
	if (sendstreamp == NULL) {
		ASSERT(e->is_nonblocking());
		e->system_exception(CORBA::WOULDBLOCK(
		    (uint_t)sizeof (nil_sendstream), CORBA::COMPLETED_NO));
		return (NULL);
	} else if (CORBA::WOULDBLOCK::_exnarrow(resourcep->
	    get_env()->exception())) {
		//
		// Only this specific exception prevents the creation
		// of the main buffer. Without a main buffer the sendstream
		// is useless. Other exceptions may be present in the case
		// of a reply message.
		//
		ASSERT(e->is_nonblocking());

		// Main Buffer allocation failed.  Get rid of sendstream.
		// This releases the endpoint.
		sendstreamp->done();
		return (NULL);
	} else {
		// success
		return (sendstreamp);
	}
}
