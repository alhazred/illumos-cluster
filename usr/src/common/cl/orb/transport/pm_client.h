/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _PM_CLIENT_H
#define	_PM_CLIENT_H

#pragma ident	"@(#)pm_client.h	1.24	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <sys/list_def.h>
#include <sys/clconf_int.h>

//
// This is the interface used by the path_manager as notification system for
// transport and cluster related events.  An object inheriting from this
// interface would register itself with the path_manager with the following
// command:
//
//  path_manager::the().add_client(this, path_manager::PM_OTHER);
//
// To remove itself as a client, it would call:
//
//  path_manager::the().remove_client(this, path_manager::PM_OTHER);
//
// Where the two arguments must be exactly the same as in the add_client
// case.
//
// There are three tiers of calls on this interface:
//
// Blocking calls:  remove_adapter, remove_path, node_died
// These calls can wait for things like reference counts to drain, etc.
// Efforts should be made to make this waiting as short as possible, of
// course.  No remote invocations are allowed in these calls.
//
// Memory allocation calls:  add_adapter, add_path, node_alive
// These calls can allocate memory, and can also block for short periods
// of time, where "short" is relative to the wait times in blocking calls.
// If anything could block that could be deferred, defer it.  Again, no
// remote invocations.
//
// Clock interrupt context calls:  path_up, path_down, disconnect_node
// These calls cannot allocate any memory or block (where blocking can be
// defined roughly as waiting on a condition variable), as they can be called
// from the clock interrupt context, which runs at interrupt level 10.  Any
// work that could block or allocate memory must be deferred.  Further, the
// critical mutex section of any function that grabs this lock must follow
// the same rules.  Generally, one can create a defer_task the corresponding
// memory allocation call, and then defer it to do your work in the clock
// calls.  Obviously, remote invocations are forbidden.
//
// There is also a shutdown() call, which you should do nothing with,
// as the semantics of shutdown are currently undefined.
//
// There is a partial ordering to the way in which these calls can be made.
// However, some calls can occur concurrently.  To understand what comes
// below, remember that every path P is associated with a local adapter A.
//
// Partial ordering:
//
// There can only be one call in each tier of calls occuring at one time.
// (This restriction is enforced by the path_manager.)
//
// There can only be one add_adapter, add_path, change_adapter, remove_adapter,
// or remove_path call occuring at one time.  (This restriction is inforced by
// the Topology Manager.)
//
// There can only be one call to node_alive or node_died occuring at one
// time.  (This restriction is inforced by the endpoint_registry.)
//
// Note that the restrictions listed above, especially the last two, are
// unnecessary, and might be removed if greater concurrency on these paths
// is needed.
//
// Any adapter A starts out as "unregistered".  When add_adapter(A) is
// called, A is considered "registered".  It will be considered registered
// until remove_adapter(A) (for the same A) is called, at which point it
// will be again considered unregistered.
//
// Any path P starts out as "unregistered".  When add_path(P) is called,
// its associated adapter A is guaranteed to be registered, and P will at
// that point also be considered registered.  When remove_path(P) is called,
// the path will be considered unregistered, but remove_path(P) can only
// be called if P's associated adapter A is still registered.
//
// Any registered path P starts out as "down".  When path_up(P) is called,
// the path is considered "up".  It will be up until path_down(P) or
// disconnect_node() is called.  path_down(P) will only be called on paths
// in the up and registered states.
//
// change_adapter(A) is called after the adapter is added to change an
// adapter property. Adapter is considered "registered" during this operation.
//
// disconnect_node(N, I) where N is a nodeid and I is an incarnation_num, is
// a batched version of path_down().  If you are registered as type PM_OTHER,
// you should declare all paths to node N that are in the up state to be in
// the down state, and can ignore the incarnation number.  Clients registered
// as PM_TRANSPORT might have paths to different node incarnations "on the
// way up", so the generic transport code will use the incarnation number.
// to avoid declaring newer paths down.
//
// remove_path(P) will never be called if the path is in an up state.  (This
// is actually true only for the PM_OTHER type of client.  Clients that
// register as PM_TRANSPORT can get remove_path(P) called when the path is
// up.  This is done on purpose so the transport doesn't try to automatically
// re-form the path after receiving path_down(P).  Only the generic transport
// code should register as type PM_TRANSPORT, so this should only be of
// interest to anyone modifying that code, or the path_manager code that
// enforces this restriction.)  P is also guaranteed to be registered.
//
// remove_adapter(A) will never be called if any path P associated with A
// is in the registered state.
//
// Any node N starts out as "dead", which is equivalent to having it's
// incarnation number defined as INCN_UNKNOWN in the membership.  When
// node_alive(N, I) is called, the node will be considered in the "alive"
// state, and it's incarnation number in the membership will be I.
// node_died(N, I) can only be called for nodes that are alive, and have
// the incarnation number I.  After node_died() completes, the node is
// once again considered dead.
//
// The timeouts_disable and timeouts_enable methods apply only to transport
// clients.  They allow the path manager to delegate disable/enable of timeouts
// to each specific transport. Those transports for which this pertains,
// (eg. TCP retransmission timeouts) will provide overrides for these virtual
// methods. The path manager makes these calls primarily in support of
// cmm_ctl -d to support developer debugging.


class pm_client {
public:
	friend class path_manager;

	pm_client();

	// Add a new adapter to this transport. Called when the configuration
	// changes.
	virtual bool	add_adapter(clconf_adapter_t *) = 0;

	// Remove an adapter from this transport.
	virtual bool	remove_adapter(clconf_adapter_t *) = 0;

	// Change adapter properties
	virtual bool	change_adapter(clconf_adapter_t *) = 0;

	// Adding a path. This call originates from TM when it notices that
	// a new path is available to the cluster. Returns true when this
	// client acted on the add_path, returns false when it doesn't
	// care about this add_path
	virtual bool	add_path(clconf_path_t *) = 0;

	// Called by the TM when a path is no longer available.
	// XXX should be made pure virtual later.
	virtual bool	remove_path(clconf_path_t *) = 0;

	// Called by ER to notify that a node with the specified incarnation
	// is alive.
	virtual bool	node_alive(nodeid_t, incarnation_num) = 0;

	// Called by ER to declare incarnation of the node down.
	virtual bool	node_died(nodeid_t, incarnation_num) = 0;

	// Called by path manager to declare this node down.
	// Called from the clock thread to declare the path up.
	virtual bool	path_up(clconf_path_t *) = 0;

	// Called from the clock thread to declard the path down.
	virtual bool	path_down(clconf_path_t *) = 0;

	// Called from the Path Monitor to declare the paths associated
	// with the incarnation of the node down.
	virtual bool	disconnect_node(nodeid_t, incarnation_num) = 0;

	virtual	void	shutdown() = 0;

	//
	// Invoked by path_manager::disable/path_manager::enable
	// Specific transports should provide an override method
	//
	virtual void timeouts_disable();
	virtual void timeouts_enable();

private:
	_SList::ListElem *get_pmlist_elem();
	_SList::ListElem pmlist_elem;

	// Don't allow copying or assignment.
	pm_client(const pm_client &);
	pm_client &operator = (pm_client &);
};

/*CSTYLED*/
typedef bool (pm_client::*pm_adapter_func)(clconf_adapter_t *);
/*CSTYLED*/
typedef bool (pm_client::*pm_path_func)(clconf_path_t *);
/*CSTYLED*/
typedef bool (pm_client::*pm_node_func)(nodeid_t, incarnation_num);


#include <orb/transport/pm_client_in.h>

#endif	/* _PM_CLIENT_H */
