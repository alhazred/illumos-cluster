/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  transport_in.h
 *
 */

#ifndef _TRANSPORT_IN_H
#define	_TRANSPORT_IN_H

#pragma ident	"@(#)transport_in.h	1.30	08/05/20 SMI"

inline void
transport::lock_node(nodeid_t nid)
{
	node_locks[nid].lock();
}

inline void
transport::unlock_node(nodeid_t nid)
{
	node_locks[nid].unlock();
}

inline bool
transport::change_adapter(clconf_adapter_t *)
{
	return (false);
}

inline int
pathend::local_adapter_id() const
{
	return (local_adp_id);
}

inline int
pathend::remote_adapter_id() const
{
	return (remote_adp_id);
}

inline bool
pathend::eq(clconf_path_t *clconf_path)
{
	return (clconf_pathp == clconf_path);
}

inline void
pathend::lock()
{
	pe_sproxy->proxy_lock();
}

inline void
pathend::unlock()
{
	pe_sproxy->proxy_unlock();
}

inline bool
pathend::lock_held()
{
	return (pe_sproxy->proxy_lock_held());
}

inline pathend::pathend_state
pathend::get_state()
{
	return (pathend::p_state);
}

inline void
pathend::queue_task()
{
	ASSERT(lock_held());
	if (!task_deferred) {
		hold();
		common_threadpool::the().defer_processing(&pdt);
		task_deferred = true;
	}
}

inline bool
pathend::is_pe_up()
{
	return (p_state == P_UP);
}

inline bool
pathend::is_ep_faulted()
{
	return (ep_state & P_EP_FAULTED);
}

inline void
pathend::record_drop_info(path_drop_reason dr)
{
	// Don't care about whether the lock is held or not as
	// the following is only for postmortem debugging
	drop_reason = dr;
	drop_hrtime = os::gethrtime();
}

//
// Records EP registration result and wakes up the state machine that
// might be waiting for the registration to complete in the P_INITIATED
// state. Called from endpoint state machine. The boolean parameter
// indicated whether the endpoint registration was successful.
//
inline void
pathend::record_ep_registration(bool success)
{
	lock();
	ASSERT(!(ep_state & P_EP_REGISTERED));
	ep_state &= ~P_EP_NEW;
	if (success) {
		ep_state |= P_EP_REGISTERED;
	}
	pe_cv.broadcast();
	unlock();
}

inline void
endpoint::queue_task()
{
	ASSERT(lock_held());
	if (!task_deferred) {
		hold();
		common_threadpool::the().defer_processing(&edt);
		task_deferred = true;
	}
}

inline uint_t
endpoint::local_adapter_id() const
{
	return (local_adp_id);
}

inline void
pathend::queue_send()
{
	ASSERT(lock_held());
	if (hb_threadpool::the().defer_processing(&pmsdt))
		hold();
}

inline void
pathend::get_pathend_hold()
{
	gpr.hold();
}

inline void
pathend::get_pathend_rele()
{
	gpr.rele();
}

inline void
pathend::set_state(pathend::pathend_state new_state)
{
	ASSERT(lock_held());

	p_state = new_state;
	pe_sproxy->set_state(p_state);
}

inline bool
endpoint::eq(clconf_path_t *clconf_path)
{
	// XXX Is this right?  We might need an equality function supplied
	// by clconf.
	return (clconf_pathp == clconf_path);
}

inline void
endpoint::lock()
{
	ep_lock.lock();
}

inline void
endpoint::unlock()
{
	ep_lock.unlock();
}

inline bool
endpoint::lock_held()
{
	return (ep_lock.lock_held());
}

inline endpoint::endpoint_state
endpoint::get_state()
{
	return (endpoint::e_state);
}

inline _SList::ListElem *
endpoint::get_translist_elem()
{
	return (&translist_elem);
}

inline int
adapter::get_adapter_id() const
{
	return (adp_id);
}

inline void
adapter::set_state(adapter::adapter_state new_state)
{
	a_state = new_state;
	ad_sproxy->proxy_lock();
	ad_sproxy->set_state(a_state);
	ad_sproxy->proxy_unlock();
}

#endif	/* _TRANSPORT_IN_H */
