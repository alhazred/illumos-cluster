//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fast_path.cc	1.31	09/02/18 SMI"

#include <sys/cl_net.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/ip/fpconf.h>
#include <orb/ip/fast_path.h>

extern fpconf *the_fpconf;
queue_t *cl_net_fpath_open(udi_handle_t *device_uh,
    const char *, int, int, uchar_t *, size_t *,
    uchar_t *, size_t *, t_uscalar_t *, char *);
int cl_net_fpath_close(udi_handle_t device_uh);

int cl_net_fpath_ioctl_send(queue_t *, uchar_t *, size_t, ushort_t, uchar_t);
static int dl_attach(udi_handle_t, int);
static int dl_bind(udi_handle_t, uint32_t);
static int dl_info(udi_handle_t, uchar_t *, size_t *, uchar_t *, size_t *,
    t_uscalar_t *);

#ifdef DL_NOTIFY_REQ
static int dl_notify_req(udi_handle_t, const char *device);
#endif
static int clean_stream_modules(udi_handle_t uh, const char *ifname);

static os::sc_syslog_msg fp_syslog_msg("Cluster.Networking", "cl_net", NULL);

uint_t fp_sap = CL_FP_SAP;

//
// Retry if DL_ATTACH_REQ fails with DL_INITFAILED.
// Workaround for bug 5020901.
//
#define	DL_ATTACH_RETRY_INTERVAL	1000000L	/* one second */

uint32_t dl_attach_retry_max = 2;

//
// This routine is called from fpconf::add_adapter (via a call to
// fp_adapter::open_queue)to open a private interconnect device for use
// by scalable services and clprivnet. First find the path open the
// driver, attach  & bind to the driver to a private sap.
//
queue_t *
cl_net_fpath_open(udi_handle_t *device_uhp,
    const char *device, int inst, int vlanid,
    uchar_t *local_addr, size_t *mac_len, uchar_t *bcast_addr,
    size_t *bcast_len, t_uscalar_t *device_mtu, char *adp)
{
	udi_handle_t drv_uh = NULL;
	int error;
	queue_t *cl_netfq;
	char		ifname[255];
	struct strioctl	iocb;
	clfpstr_info_t	clfpstr_info;

#if SOL_VERSION >= __s11
	if ((error = udi_open_vop(device, VLAN_PPA(inst, vlanid),
	    &drv_uh)) != 0) {
#else
	if ((error = udi_open_vop(device, -1, &drv_uh)) != 0) {
#endif
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Cannot open %s error %d\n", device, error);
		FP_DBG(("    udi_open %s failed error %d\n", device, error));
		return ((queue_t *)NULL);
	}

#if SOL_VERSION <= __s10
	if ((error = dl_attach(drv_uh, VLAN_PPA(inst, vlanid))) != 0) {
		(void) udi_close(drv_uh);
		FP_DBG(("    dl_attach %s failed error %d\n", device, error));
		return ((queue_t *)NULL);
	}
#endif

	//
	// The two bytes following the destination and source MAC
	// addresses are either a protocol in Ethernet 2.0 or the
	// packet length in 802.3.  To make sure the protocol is
	// not mistaken for the length of the packet the sap needs
	// to be greater than the maximum ethernet packet length.
	//
	ASSERT(fp_sap > 1500);

	if ((error = dl_bind(drv_uh, fp_sap)) != 0) {
		(void) udi_close(drv_uh);
		FP_DBG(("    dl_bind %s failed error %d\n", device, error));
		return ((queue_t *)NULL);
	}

	if ((error = dl_info(drv_uh, local_addr, mac_len, bcast_addr,
	    bcast_len, device_mtu)) != 0) {
		(void) udi_close(drv_uh);
		FP_DBG(("    dl_info %s failed error %d\n", device, error));
		return ((queue_t *)NULL);
	}

	// Pop all autopush modules
	(void) sprintf(ifname, "%s%d", device, VLAN_PPA(inst, vlanid));
	(void) clean_stream_modules(drv_uh, ifname);

	//
	// Push the clhbsndr module. This module enables sending heartbeats
	// in the interrupt context when the usual mechanism of sending through
	// real time threads fails when the machine is experiencing high
	// network interrupt load.
	//
	if ((error = udi_push(drv_uh, "clhbsndr")) != 0) {
		FP_DBG(("    udi_push %s failed error %d\n",
		    "clhbsndr", error));
		(void) udi_close(drv_uh);
		return ((queue_t *)NULL);
	}

	//
	// We will do our own IEEE usr priority and mblk b_band management
	// and hence there is no need to send the CLIOCSPRIVBBAND ioctl to
	// clhbsndr.
	//

	// Push the clfpstr module for processing incoming packets
	if ((error = udi_push(drv_uh, CLFPSTR_MODULE_NAME)) != 0) {
		FP_DBG(("    udi_push %s failed error %d\n",
		    CLFPSTR_MODULE_NAME, error));
		(void) udi_close(drv_uh);
		return ((queue_t *)NULL);
	}

	//
	// Let the clfpstr module know of the fp_adp object. The clfpstr
	// module will use this information to forward the link up/down
	// notifications to the right adapter object.
	//
	clfpstr_info.adp = adp;
	iocb.ic_cmd = CLIOCSFPADP;
	iocb.ic_timout = 0;
	iocb.ic_len = (int)sizeof (clfpstr_info_t);
	iocb.ic_dp = (char *)&clfpstr_info;

	if ((error = udi_ioctl(drv_uh, I_STR, (intptr_t)&iocb)) != 0) {
		FP_DBG(("    udi_ioctl CLIOCSFPADP failed error %d\n", error));
		(void) udi_close(drv_uh);
		return ((queue_t *)NULL);
	}

#ifdef DL_NOTIFY_REQ
	//
	// Send notify req so that the driver can keep us updated about
	// link up and down. The link up and down notifications are used
	// to quickly learn about about failed links (before the path
	// manager can detect the failure). However, correctness is not
	// impacted if these notifications are not available. In fact,
	// many drivers do not provide such notifications. Thus, we
	// ignore the  returned error code.
	//
	(void) dl_notify_req(drv_uh, device);
#endif

	if (drv_uh) {
		*device_uhp = drv_uh;
		cl_netfq = WR(strvp2wq(UDI2VP(drv_uh))); /*lint !e666 */
		FP_DBG(("    cl_netfq = %p\n", cl_netfq));
		return (cl_netfq);
	}
	return ((queue_t *)NULL);
}

//
// This routine is called by fpconf::remove_adapter via a call to
// fp_adapter::close_queue.
//
int
cl_net_fpath_close(udi_handle_t device_uh)
{
	return (udi_close(device_uh));
}

//
// This routine sends a DLPI fast path request to the driver underneath.
// Return 0 on success, 1 otherwise.
//
int
cl_net_fpath_ioctl_send(queue_t *q, uchar_t *rmacp, size_t maclen,
    ushort_t sap, uchar_t upri)
{
	mblk_t	*ioc_mp;
	mblk_t	*dlureq_mp;
	struct iocblk	*iocp;
	dl_unitdata_req_t	*dl_udata;

	if (q == NULL || rmacp == NULL || maclen == 0) {
		return (1);
	}

	// Allocate the unit data request
	dlureq_mp = os::allocb((uint_t)(sizeof (dl_unitdata_req_t) +
	    (size_t)maclen + sizeof (ushort_t)), BPRI_HI, os::SLEEP);

	// Stuff the unit data req header
	dlureq_mp->b_datap->db_type = M_PROTO;
	dl_udata = (dl_unitdata_req_t *)dlureq_mp->b_wptr;
	dl_udata->dl_primitive = DL_UNITDATA_REQ;
	dl_udata->dl_dest_addr_offset =
	    (t_uscalar_t)sizeof (dl_unitdata_req_t);
	//
	// IEEE user priority  : larger value is higher priority
	// DL unitdata request : lower value is higher priority
	// Callers guarantee that upri will be in [0 - MAX_8021D_UPRI].
	//
	ASSERT(upri <= MAX_8021D_UPRI);
	dl_udata->dl_priority.dl_min = (MAX_8021D_UPRI - upri);
	dl_udata->dl_priority.dl_max = (MAX_8021D_UPRI - upri);
	dl_udata->dl_dest_addr_length = (t_uscalar_t)(maclen +
	    sizeof (ushort_t));
	dlureq_mp->b_wptr += sizeof (dl_unitdata_req_t);

	// Append the remote MAC address and the sap
	bcopy(rmacp, dlureq_mp->b_wptr, maclen);
	dlureq_mp->b_wptr += maclen;
	bcopy(&sap, dlureq_mp->b_wptr, sizeof (ushort_t));
	dlureq_mp->b_wptr += sizeof (ushort_t);

	dlureq_mp->b_band = UPRI_TO_BBAND(upri);
	//
	// Allocate the ioctl mp and append the unit data request mp to the
	// ioctl mp.
	//
	if ((ioc_mp = mkiocb(DL_IOC_HDR_INFO)) == NULL) {
		freemsg(dlureq_mp);
		return (1);
	}
	ioc_mp->b_cont = dlureq_mp;

	// Set the ioctl data size
	iocp = (struct iocblk *)ioc_mp->b_rptr;
	iocp->ioc_count = msgdsize(ioc_mp->b_cont);

	// Send the ioctl
	putnext(q, ioc_mp);

	return (0);
}

//
// This routine is called by the rput routine of the clfpstr module
// to process the DLPI fast path request ack on the fpconf stream.
//
extern "C" {
void
cl_net_fpath_ioctl_ack(char *adp, mblk_t *mp)
{
	mblk_t *tmpmp;

	//
	// At this point the ioctl ack/nack chain is in mp.
	//
	if (mp->b_datap->db_type == M_IOCNAK) {
		/* Fast path is not supported */
		freemsg(mp);
		mp = NULL;
		goto done;
	}

	//
	// It's an ack. First mp is the ioctl mp, next one if the unit data
	// request. The third one is the DLPI header response from the driver.
	//
	ASSERT(mp->b_datap->db_type == M_IOCACK);
	ASSERT(mp->b_cont != NULL && mp->b_cont->b_cont != NULL);

	// Get rid of the ioctl mp
	tmpmp = mp;
	mp = mp->b_cont;
	freeb(tmpmp);
	if (mp == NULL) {
		goto done;
	}

	// Get rid of the unit data request mp
	tmpmp = mp;
	mp = mp->b_cont;
	freeb(tmpmp);
	if (mp == NULL) {
		goto done;
	}

	//
	// At this point mp should have the desired M_DATA mblk with the
	// DLPI header in it.
	//
	ASSERT(mp->b_datap->db_type == M_DATA);

done:
	//
	// Hand over the fast path response mblk to the fp adapter object
	// who is responsible for this stream..
	//
	((fp_adapter *)adp)->set_fp_ack_mchain(mp);
}

//
// This routine is called to notify fpconf of a link status change.
//
void
cl_net_fpath_link_status_update(char *fp_adp_char_ptr, int link_up)
{
	fp_adapter *my_fp_adp = (fp_adapter *)fp_adp_char_ptr;
	the_fpconf->link_status_update(my_fp_adp, link_up ? true : false);
}
}

static int
dl_attach(udi_handle_t uh, int unit)
{
	dl_attach_req_t *attach_req;
	dl_error_ack_t *error_ack;
	union DL_primitives *dl_prim;
	mblk_t *mp;
	int error;
	uint_t retry = 0;

dl_attach_retry:

	if ((mp = allocb(sizeof (dl_attach_req_t), BPRI_MED)) == NULL) {
		cmn_err(CE_WARN, "dl_attach: allocb failed");
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;
	mp->b_wptr += sizeof (dl_attach_req_t);

	attach_req = (dl_attach_req_t *)mp->b_rptr;
	attach_req->dl_primitive = DL_ATTACH_REQ;
	attach_req->dl_ppa = (t_uscalar_t)unit;

	if ((error = udi_msg(uh, mp, &mp, (timestruc_t *)NULL)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			MESSAGE, "dl_attach: udi_msg failed %d error\n",
			error);
		ASSERT(0);
		return (error);
	}

	dl_prim = (union DL_primitives *)mp->b_rptr;
	switch (dl_prim->dl_primitive) {
	case DL_OK_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_ok_ack_t)) {
			//
			// SCMSGS
			// @explanation
			// Could not attach to the private interconnect
			// interface.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_attach: DL_OK_ACK protocol error\n");
			break;
		}
		if (((dl_ok_ack_t *)dl_prim)->dl_correct_primitive !=
		    DL_ATTACH_REQ) {
			//
			// SCMSGS
			// @explanation
			// Wrong primitive returned to the DL_ATTACH_REQ.
			// @user_action
			// Reboot the node. If the problem persists, check the
			// documentation for the private interconnect.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_attach: DL_OK_ACK rtnd prim %u\n",
			    ((dl_ok_ack_t *)dl_prim)->dl_correct_primitive);
			break;
		}
		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			//
			// SCMSGS
			// @explanation
			// Could not attach to the physical device.
			// @user_action
			// Check the documentation for the driver associated
			// with the private interconnect. It might be that the
			// message returned is too small to be valid.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "dl_attach: DL_ERROR_ACK protocol error\n");
			break;
		}

		error_ack = (dl_error_ack_t *)dl_prim;
		switch (error_ack->dl_errno) {
		case DL_BADPPA:
			//
			// SCMSGS
			// @explanation
			// Could not attach to the physical device. We are
			// trying to open a fast path to the private transport
			// adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_attach: DL_ERROR_ACK bad PPA\n");
			break;

		case DL_ACCESS:
			//
			// SCMSGS
			// @explanation
			// Could not attach to the physical device. We are
			// trying to open a fast path to the private transport
			// adapters.
			// @user_action
			// Reboot of the node might fix the problem
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_attach: DL_ERROR_ACK access error\n");
			break;

		case DL_INITFAILED:
			//
			// Workaround for 5020901. This code can be in a
			// race with ifkconfig that results in this
			// error. Retrying is effective in resolving it.
			//
			FP_DBG(("DL_ATTACH_REQ failed with DL_INITFAILED "
			    "ppa %d\n", attach_req->dl_ppa));

			if (retry++ < dl_attach_retry_max) {
				freemsg(mp);
				os::usecsleep(DL_ATTACH_RETRY_INTERVAL);
				goto dl_attach_retry;
			}
			/* FALL THROUGH */
		default:
			//
			// SCMSGS
			// @explanation
			// Could not attach to the physical device. We are
			// trying to open a fast path to the private transport
			// adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_attach: DLPI error %u\n",
			    error_ack->dl_errno);
			break;
		}
		break;
	default:
		//
		// SCMSGS
		// @explanation
		// Could not attach to the physical device. We are trying to
		// open a fast path to the private transport adapters.
		// @user_action
		// Reboot of the node might fix the problem.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "dl_attach: bad ACK header %u\n", dl_prim->dl_primitive);
		break;
	}

	freemsg(mp);
	return (-1);
}

static int
dl_bind(udi_handle_t uh, uint_t sap)
{
	dl_bind_req_t *bind_req;
	dl_error_ack_t *error_ack;
	dl_bind_ack_t *bind_ack;
	union DL_primitives *dl_prim;
	mblk_t *mp;
	int error;

	if ((mp = allocb(sizeof (dl_bind_req_t), BPRI_MED)) == NULL) {
		cmn_err(CE_WARN, "dl_bind: allocb failed");
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;

	bind_req = (dl_bind_req_t *)mp->b_wptr;
	mp->b_wptr += sizeof (dl_bind_req_t);
	bind_req->dl_primitive = DL_BIND_REQ;
	bind_req->dl_sap = sap;
	bind_req->dl_max_conind = 0;
	bind_req->dl_service_mode = DL_CLDLS;
	bind_req->dl_conn_mgmt = 0;
	bind_req->dl_xidtest_flg = 0;

	if ((error = udi_msg(uh, mp, &mp, (timestruc_t *)NULL)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"dl_bind: udi_msg failed %d error \n",
			error);
		ASSERT(0);
		return (error);
	}

	dl_prim = (union DL_primitives *)mp->b_rptr;
	switch (dl_prim->dl_primitive) {
	case DL_BIND_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_bind_ack_t)) {
			//
			// SCMSGS
			// @explanation
			// Could not bind to the physical device. We are
			// trying to open a fast path to the private transport
			// adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_bind: DL_BIND_ACK protocol error\n");
			break;
		}
		bind_ack = (dl_bind_ack_t *)dl_prim;
		if (bind_ack->dl_sap != sap) {
			//
			// SCMSGS
			// @explanation
			// SAP in acknowledgment to bind request is different
			// from the SAP in the request. We are trying to open
			// a fast path to the private transport adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "dl_bind: DL_BIND_ACK bad sap %u\n",
			    ((dl_bind_ack_t *)dl_prim)->dl_sap);
			break;
		}

		if (bind_ack->dl_addr_length + bind_ack->dl_addr_offset >
		    (size_t)(mp->b_wptr-mp->b_rptr)) {
			//
			// SCMSGS
			// @explanation
			// Sanity check. The message length in the
			// acknowledgment to the bind request is different
			// from what was expected. We are trying to open a
			// fast path to the private transport adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_bind ack bad len %d\n",
			    bind_ack->dl_addr_length);
			break;
		}

		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			//
			// SCMSGS
			// @explanation
			// Could not bind to the physical device. We are
			// trying to open a fast path to the private transport
			// adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_bind: DL_ERROR_ACK protocol error\n");
			break;
		}
		error_ack = (dl_error_ack_t *)dl_prim;
		//
		// SCMSGS
		// @explanation
		// DLPI protocol error. We cannot bind to the physical device.
		// We are trying to open a fast path to the private transport
		// adapters.
		// @user_action
		// Reboot of the node might fix the problem.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "dl_bind: DLPI error %u\n", error_ack->dl_errno);
		break;

	default:
		//
		// SCMSGS
		// @explanation
		// An unexpected error occurred. The acknowledgment header for
		// the bind request (to bind to the physical device) is bad.
		// We are trying to open a fast path to the private transport
		// adapters.
		// @user_action
		// Reboot of the node might fix the problem.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "dl_bind: bad ACK header %u\n", dl_prim->dl_primitive);
		break;
	}
	freemsg(mp);
	return (-1);
}

static int
dl_info(udi_handle_t uh, uchar_t *local_addr, size_t *mac_len,
    uchar_t *bcast_addr, size_t *bcast_len, t_uscalar_t *device_mtu)
{
	dl_info_req_t *info_req;
	dl_error_ack_t *error_ack;
	dl_info_ack_t *info_ack;
	union DL_primitives *dl_prim;
	mblk_t *mp;
	int error;
	uchar_t *addrp;

	*mac_len = 0;

	if ((mp = allocb(sizeof (dl_info_req_t), BPRI_MED)) == NULL) {
		cmn_err(CE_WARN, "dl_info: allocb failed");
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;

	info_req = (dl_info_req_t *)mp->b_wptr;
	mp->b_wptr += sizeof (dl_info_req_t);
	info_req->dl_primitive = DL_INFO_REQ;

	if ((error = udi_msg(uh, mp, &mp, (timestruc_t *)NULL)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"dl_info: udi_msg failed %d error \n",
			error);
		ASSERT(0);
		return (error);
	}

	/*
	 * If the response message is not an ack for the DL_INFO_REQ,
	 * wait until, the ack arrives. In the meanwhile drop all
	 * incoming messages - they are most likely broadcast packets
	 * from other nodes.
	 */
	do {
		dl_prim = (union DL_primitives *)mp->b_rptr;
		if (mp->b_datap->db_type == M_PROTO ||
		    mp->b_datap->db_type == M_PCPROTO) {
			if (dl_prim->dl_primitive == DL_INFO_ACK ||
			    dl_prim->dl_primitive == DL_ERROR_ACK) {
				/* The response we have been waiting for */
				break;
			}
		}
		/*
		 * Drop this message, and get the next one from the queue
		 */
		FP_DBG(("dl_info: dropped msg db_type %d dl_prim %d\n",
			mp->b_datap->db_type, dl_prim->dl_primitive));
		freemsg(mp);
		mp = NULL;
		error = udi_msg(uh, NULL, &mp, (timestruc_t *)NULL);
		if (error != 0) {
			FP_DBG(("dl_info_req: udi_msg failed, error %d\n",
			    error));
			ASSERT(0);
			return (error);
		}
	} while (true);

	switch (dl_prim->dl_primitive) {
	case DL_INFO_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_info_ack_t)) {
			//
			// SCMSGS
			// @explanation
			// Could not get a DLPI info_ack message from the
			// physical device. We are trying to open a fast path
			// to the private transport adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_info: DL_INFO_ACK protocol error\n");
			break;
		}
		info_ack = (dl_info_ack_t *)dl_prim;

		if (info_ack->dl_addr_length > MAX_MAC_ADDR_SIZE) {
			//
			// SCMSGS
			// @explanation
			// Size of MAC address in acknowledgment of the bind
			// request exceeds the maximum size allotted. We are
			// trying to open a fast path to the private transport
			// adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "huge address size %d \n",
			    info_ack->dl_addr_length);
			break;
		}

		if (info_ack->dl_addr_length + info_ack->dl_addr_offset >
		    (size_t)(mp->b_wptr-mp->b_rptr)) {
			//
			// SCMSGS
			// @explanation
			// Sanity check. The message length in the
			// acknowledgment to the info request is different
			// from what was expected. We are trying to open a
			// fast path to the private transport adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_info ack bad len %d\n",
			    info_ack->dl_addr_length);
			break;
		}

		addrp = mp->b_rptr + info_ack->dl_addr_offset;

		//
		// Subtract the two bytes related to the sap
		// which is also unfortunately sent up.
		//

		*mac_len = info_ack->dl_addr_length - sizeof (ushort_t);

		bcopy((uchar_t *)addrp, (uchar_t *)local_addr,
		    (size_t)info_ack->dl_addr_length);

		addrp = mp->b_rptr + info_ack->dl_brdcst_addr_offset;
		*bcast_len = info_ack->dl_brdcst_addr_length;
		bcopy((uchar_t *)addrp, (uchar_t *)bcast_addr,
		    (size_t)info_ack->dl_brdcst_addr_length);
		*device_mtu = info_ack->dl_max_sdu;
		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			//
			// SCMSGS
			// @explanation
			// Could not get a info_ack from the physical device.
			// We are trying to open a fast path to the private
			// transport adapters.
			// @user_action
			// Reboot of the node might fix the problem.
			//
			(void) fp_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "dl_info: DL_ERROR_ACK protocol error\n");
			break;
		}
		error_ack = (dl_error_ack_t *)dl_prim;
		//
		// SCMSGS
		// @explanation
		// DLPI protocol error. We cannot get a info_ack from the
		// physical device. We are trying to open a fast path to the
		// private transport adapters.
		// @user_action
		// Reboot of the node might fix the problem.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "dl_info: DLPI error %u\n", error_ack->dl_errno);
		break;

	default:
		//
		// SCMSGS
		// @explanation
		// An unexpected error occurred. The acknowledgment header for
		// the info request (to bind to the physical device) is bad.
		// We are trying to open a fast path to the private transport
		// adapters.
		// @user_action
		// Reboot of the node might fix the problem.
		//
		(void) fp_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "dl_info: bad ACK header %u\n", dl_prim->dl_primitive);
		break;
	}
	freemsg(mp);
	return (-1);
}

#ifdef DL_NOTIFY_REQ
/*
 * Send the DL_NOTIFY_REQ to the driver.
 */
static int
dl_notify_req(udi_handle_t uh, const char *device)
{
	dl_notify_req_t *notify_req;
	dl_error_ack_t *error_ack;
	dl_notify_ack_t *notify_ack;
	union DL_primitives *dl_prim;
	mblk_t *mp;
	int error;

	if ((mp = allocb(sizeof (dl_notify_req_t), BPRI_MED)) == NULL) {
		FP_DBG(("dl_notify_req: allocb failed\n"));
		return (ENOSR);
	}
	mp->b_datap->db_type = M_PROTO;

	notify_req = (dl_notify_req_t *)mp->b_wptr;
	mp->b_wptr += sizeof (dl_notify_req_t);
	notify_req->dl_primitive	= DL_NOTIFY_REQ;
	notify_req->dl_notifications	= DL_NOTE_LINK_UP |
					DL_NOTE_LINK_DOWN;
	notify_req->dl_timelimit = 0;

	if ((error = udi_msg(uh, mp, &mp, (timestruc_t *)NULL)) != 0) {
		FP_DBG(("dl_notify_req: udi_msg failed, error %d\n", error));
		ASSERT(0);
		return (error);
	}

	/*
	 * If the response message is not an ack for the DL_NOTIFY_REQ,
	 * wait until, the ack arrives. In the meanwhile drop all
	 * incoming messages - they are most likely broadcast packets
	 * from other nodes.
	 */
	do {
		dl_prim = (union DL_primitives *)mp->b_rptr;
		if (mp->b_datap->db_type == M_PROTO ||
		    mp->b_datap->db_type == M_PCPROTO) {
			if (dl_prim->dl_primitive == DL_NOTIFY_ACK ||
			    dl_prim->dl_primitive == DL_ERROR_ACK) {
				/* The response we have been waiting for */
				break;
			}
		}
		/* Drop this message, and get the next one from the queue */
		FP_DBG(("dl_info: dropped msg db_type %d dl_prim %d\n",
			mp->b_datap->db_type, dl_prim->dl_primitive));
		freemsg(mp);
		mp = NULL;
		error = udi_msg(uh, NULL, &mp, (timestruc_t *)NULL);
		if (error != 0) {
			FP_DBG(("dl_notify_req: udi_msg failed, error %d\n",
			    error));
			ASSERT(0);
			return (error);
		}
	} while (true);

	switch (dl_prim->dl_primitive) {
	case DL_NOTIFY_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) <
		    sizeof (dl_notify_ack_t)) {
			ASSERT(0);
			FP_DBG(("dl_notify_req: bad ack\n"));
			break;
		}
		notify_ack = (dl_notify_ack_t *)dl_prim;
		if ((notify_ack->dl_notifications &
			(DL_NOTE_LINK_UP | DL_NOTE_LINK_DOWN)) !=
			(DL_NOTE_LINK_UP | DL_NOTE_LINK_DOWN)) {
			FP_DBG(("dl_notify_req: no driver support\n"));
		}
		FP_DBG(("dl_notify_req: driver (%s) support available\n",
		    device));
		freemsg(mp);
		return (0);

	case DL_ERROR_ACK:
		if ((size_t)(mp->b_wptr-mp->b_rptr) < sizeof (dl_error_ack_t)) {
			ASSERT(0);
			FP_DBG(("dl_notify_req: bad ack\n"));
			break;
		}
		error_ack = (dl_error_ack_t *)dl_prim;
		if (error_ack->dl_errno == DL_UNSUPPORTED ||
		    error_ack->dl_errno == DL_NOTSUPPORTED) {
			FP_DBG(("dl_notify_req: no driver (%s) support\n",
			    device));
			freemsg(mp);
			return (0);
		}
		ASSERT(0);
		FP_DBG(("dl_notify_req: dlpi error %d, device %s\n",
		    error_ack->dl_errno, device));
		break;

	default:
		ASSERT(0);
		break;
	}
	freemsg(mp);
	return (-1);
}
#endif

static int
clean_stream_modules(udi_handle_t uh, const char *ifname)
{
	int error;
	char module_name[FMNAMESZ + 1];

	/*
	 * Remove all modules which appear on this STREAM. This is specifically
	 * aimed at removing the mcnet module since it is autopushed on all
	 * instances of specified devices. However, other modules found here
	 * are not expected, nor needed except for the clhbsndr module.
	 * We will go ahead and remove the clhbsndr module also. It will get
	 * pushed again later. We choose to pop and then repush clhbsndr
	 * back again to keep the code simple.
	 */
	while ((error = udi_ioctl(uh, I_LOOK, (intptr_t)module_name)) == 0) {
		FP_DBG(("clean_stream_modules: removing %s from %s\n",
		    module_name, ifname));

		if ((error = udi_ioctl(uh, I_POP, (long)NULL)) != 0) {
			FP_DBG(("clean_stream_modules: "
			    "udi_ioctl I_POP failed of %s module from %s, "
			    "error %d\n", module_name, ifname, error));
		}
	}

	/*
	 * udi_ioctl returns EINVAL if there are no modules remaining
	 * on the STREAM.  Any other error is a problem.
	 */
	if (error != EINVAL) {
		FP_DBG(("clean_stream_modules: "
		    "udi_ioctl I_LOOK failed, error %d\n", error));
		return (error);
	}

	return (0);
}
