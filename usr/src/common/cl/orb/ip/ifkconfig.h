/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _IFKCONFIG_H
#define	_IFKCONFIG_H

#pragma ident	"@(#)ifkconfig.h	1.15	08/05/20 SMI"

/*
 * header file for kernel version of ifconfig
 */


#ifdef __cplusplus
extern "C" {
#endif

#include	<sys/param.h>
#include	<sys/types.h>
#include	<sys/user.h>
#include	<sys/vfs.h>
#include	<sys/vnode.h>
#include	<sys/file.h>
#include	<sys/stream.h>
#include	<sys/stropts.h>
#include	<sys/strsubr.h>
#include	<sys/dlpi.h>
#include	<sys/vnode.h>
#include	<sys/socket.h>
#include	<sys/sockio.h>
#include	<net/if.h>
#include	<sys/cred.h>
#include	<sys/sysmacros.h>
#include	<sys/errno.h>
#include	<sys/sunddi.h>
#include	<netinet/ip6.h>
#include	<netinet/in.h>
#include	<inet/common.h>
#include	<inet/ip.h>
#include	<inet/tcp.h>
#include	<sys/socketvar.h>

/*
 * set the following bits in cmd to perform the desired action
 */

#define	IFK_PLUMB	(1<<0) 	/* for physical devices */
#define	IFK_CREATE	(1<<0) 	/* for logical devices */
#define	IFK_UNPLUMB	(1<<1) 	/* for physical devices */
#define	IFK_DESTROY	(1<<1) 	/* for logical devices */
#define	IFK_NETMASK	(1<<2) 	/* set if netmask is to be set */
#define	IFK_IPADDR	(1<<3) 	/* set if ipaddress is to be set */
#define	IFK_UP		(1<<4) 	/* set if interface to be config'd up */
#define	IFK_DOWN	(1<<5)	/* set if interface is to be downed */
#define	IFK_PRIVATE	(1<<6)	/* make address private */
#define	IFK_NOTPRIVATE	(1<<7)	/* make address not private */
#define	IFK_ARP		(1<<8)  /* if set, enable ARP */
#define	IFK_NOARP	(1<<9)  /* if set, disable ARP */
#define	IFK_MTU		(1<<10)  /* if set, mtu is to be set */
#define	IFK_BRDCST	(1<<11) /* if set, broadcast address is to be setK */

/*
 * set the logical unit in ifk_interface_t to this to perform
 * actions on the physical device...
 */

#define	IFK_PHYSICAL	(-1)

typedef struct ifk_interface {
	const char	*ifk_device_name;
	int		ifk_device_instance;
	int		ifk_vlan_id;
	const char	*ifk_vlan_device_name;
	int		ifk_logical_unit;
	struct in_addr	ifk_netmask;
	struct in_addr	ifk_ipaddr;
	struct in_addr	ifk_broadcast;
	int		ifk_mtu;
} ifk_interface_t;

int ifkconfig(const ifk_interface_t *in, int cmd, int *error);

/*
 * Remove any extraneous mcnet module (or others) from the transport.
 */
#define	MCNET_MODULE_NAME "mcnet"

/*
 * The following undefs are to prevent further trouble with definition of nil
 * and noop in networking header files
 */

#undef nil
#undef noop


#ifdef __cplusplus
}
#endif

#endif	/* _IFKCONFIG_H */
