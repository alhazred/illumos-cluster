/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FPCONF_H
#define	_FPCONF_H

#pragma ident	"@(#)fpconf.h	1.32	08/09/08 SMI"

#undef nil // <inet/common.h> redefines nil

#include <sys/disp.h>
#include <netinet/in.h>
#include <inet/common.h>
#include <inet/led.h>
#ifdef _KERNEL
#include <sys/kstr.h>
#endif

#include <sys/vnode.h>
#include <sys/hashtable.h>
#include <sys/socket.h>
#include <net/if.h>
#include <net/route.h>


#include <sys/list_def.h>
#include <orb/transport/pm_client.h>
#include <sys/threadpool.h>
#include <sys/os.h>

#include <h/naming.h>
#include <h/network.h>
#include <orb/object/adapter.h>
#include <orb/debug/haci_debug.h>
#include <orb/ip/vlan.h>
#include <orb/ip/udi_util.h>
#include <orb/ip/fast_path.h>

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef _KERNEL

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>
#include <sys/strsun.h>


#endif	/* _KERNEL */

#include <h/naming.h>
#include <h/streams.h>
#include <h/sol.h>

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#define	MAX_MAC_ADDR_SIZE	22
#define	ETHERTYPE_COUNT		5

#if defined(HACI_DBGBUFS) && !defined(NO_FP_DBGBUF)
#define	FP_DBGBUF
#endif

#ifdef FP_DBGBUF
extern dbg_print_buf fp_dbg;
#define	FP_DBG(a)	HACI_DEBUG(ENABLE_FP_DBG, fp_dbg, a)
#else
#define	FP_DBG(a)
#endif

class fp_holder;
class fp_adapter;

//
// Information that muxq_impl or clprivnet gets when it queries fpconf
// for an appropriate queue on which an outgoing packet can be sent.
//
typedef struct {
	queue_t *q;	// A pointer to the device's streams write queue
	uchar_t *rmacaddr;	// remote mac address
	uchar_t *lmacaddr;	// local mac address
	unsigned long size;	// mac address size
	uchar_t *bcstaddr;	// broadcast mac address
	unsigned long bcstsize;	// broadcast mac address size
	fp_holder *fph;	// Corresponding fp holder object
	mblk_t *fp_mp;	// The precomputed dlpi fast path header

	// The following fields are applicable only to clprivnet redirection
	ushort_t ho_sap;		// SAP, host order
	nodeid_t remote_node;		// Remote nodeid
	fp_adapter *adapterp;		// Pointer to the fp_adapter object
	bool brdcast_capable_adapter;	// Whether the adapter is bcast capable
	uint_t upri;			// IEEE 802.1d user priority
} ret_data_t;

class fp_holder : public _SList::ListElem {
public:
	fp_holder(clconf_path_t *);
	~fp_holder();
	bool fp_path_data(ushort_t, uchar_t, bool, ret_data_t &);
	void incr_ref();
	void decr_ref();
	void path_up();
	void path_down();
	bool is_path_up();
	bool is_bw_available(size_t msg_size);
	clconf_path_t *get_path();
	bool get_queue();
	bool rel_queue();
	bool same_device(network::macinfo_t &macaddr);
	void update_remote_macaddr(network::macinfo_t &macaddr);
	fp_adapter* get_adapter();
private:
	queue_t *queue;

	//
	// DLPI fast path headers (unicast and broadcast) for every known sap
	// and IEEE 802.1d user priority value.
	//
	mblk_t *fp_ucast_headers[ETHERTYPE_COUNT][NUM_8021D_UPRI];
	mblk_t *fp_bcast_headers[ETHERTYPE_COUNT][NUM_8021D_UPRI];

	bool is_up;
	bool is_waiting;
	clconf_path_t *path;
	fp_adapter *fp_adap;
	bool have_remote_mac;
	uchar_t remote_mac_addr[MAX_MAC_ADDR_SIZE];
	uchar_t local_mac_addr[MAX_MAC_ADDR_SIZE];
	uchar_t bcast_mac_addr[MAX_MAC_ADDR_SIZE];
	size_t mac_addr_size;
	size_t bcast_addr_size;
	int ref_count;
	char *remote_device_name;
	int remote_device_instance;
	bool	brdcast_capable_adapter;
	os::condvar_t	fp_holder_cv;
	os::mutex_t	fp_holder_lock;
};

typedef IntrList<fp_holder, _SList> fp_holder_list_t;

class pernodepath {
public:
	pernodepath();
	~pernodepath();
	void mark_path_up(clconf_path_t *);
	void mark_path_down(clconf_path_t *);
	void append_path(clconf_path_t *);
	void remove_path(clconf_path_t *);
	void mark_all_down();
	bool get_data(ushort_t, uchar_t, ret_data_t &, size_t msgsize,
	    network::cid_t&);
	bool get_data(ushort_t, uchar_t, ret_data_t &, network::cid_t&,
	    uint32_t&);
	bool get_broadcastdata(ushort_t, uchar_t, nodeid_t, ret_data_t &,
	    long &);
	void decr_ref_count(ret_data_t &);
	void start_matching(network::macinfoseq_t_var macaddr);
	bool has_active_list();
	void link_status_update(fp_adapter *, bool);

private:
	//
	// Each pernodepath object maintains three lists
	// fp_active_list: List of fp holders that represents paths
	//		that are up and can be used to send data
	// fp_link_down_list: List of fp holders that represent paths
	//		for which the driver has sent link down
	//		notifications but that have not yet been
	//		declared down by the path manager. These paths
	//		are not used for sending data.
	// fp_path_down_list: List of fp holders that represent paths
	//		that have been declared down by the path
	//		manager are are not used for sending data.
	//
	//	A	: Active
	//	LD	: Link Down
	//	PD	: Path Down
	//
	//	Start			-----link_up------------
	//	|			|			|
	//	v			v			|
	//	PD -----path_up------> A ----link_down-----> LD
	//	^			|			|
	//	|			|			|
	//	-------path_down--------------------------------
	//
	fp_holder_list_t fp_active_list;
	fp_holder_list_t fp_link_down_list;
	fp_holder_list_t fp_path_down_list;
	os::mutex_t fp_holder_list_lock;
};

class fp_adapter : public _SList::ListElem {
public:
	fp_adapter(clconf_adapter_t *);
	~fp_adapter();
	queue_t *get_queue(uchar_t *, size_t *, uchar_t *, size_t *);
	void rel_queue();
	mblk_t *get_fp_header(uchar_t *, size_t, ushort_t, uchar_t);
	void set_fp_ack_mchain(mblk_t *mp);
	bool bandwidth_allocator(size_t msgsize);
	bool adap_compare(const char *, int);
	void adap_info(network::macinfoseq_t &, uint_t);
	void calculate_bandwidth(clconf_adapter_t *);
	bool open_queue();
	void close_queue();
	const char *get_device();
	int get_device_instance();
	t_uscalar_t get_mtu();

private:
	udi_handle_t device_uh;
	queue_t *queue;
	uint_t network_bw_percent;
	int pkt_drop_count;
	uint64_t allowed_bandwidth;
	uint_t total_bandwidth;
	clock_t next_timeout;
	clock_t timeout_incr;
	size_t total_bytes;
	os::mutex_t bandwidth_lock;
	os::mutex_t mac_addr_lock;
	uchar_t local_mac_addr[MAX_MAC_ADDR_SIZE];
	uchar_t bcast_mac_addr[MAX_MAC_ADDR_SIZE];
	size_t local_mac_size;
	size_t bcast_addr_size;
	char *local_device;
	int local_device_instance;
	t_uscalar_t device_mtu;
	int local_vlan_id;

	// Number of fp_holder objects with references to this queue
	int ref_cnt;

	// Used a temporary store for the DLPI fast path acknowledgement
	mblk_t *fp_ack_mchain;

	//
	// Whether a DLPI fast path request has been sent to the driver
	// and we are waiting for an acknowledgement from the driver.
	//
	bool waiting_for_fp_ack;

	//
	// Used to make sure only one DLPI fast path request is being
	// worked on at any point of time.
	//
	bool fp_query_in_progress;
	os::mutex_t adp_lock;
	os::condvar_t adp_cv;

	// Disallow assignments and pass by value
	fp_adapter(const fp_adapter &);
	fp_adapter &operator = (fp_adapter &);
};

typedef IntrList<fp_adapter, _SList> fp_adapter_list_t;

class fpconf : public pm_client, public _SList::ListElem {
public:
	// Functions inheritted from pm_client. They are used by
	// path manager to deliver events to ipconf.
	bool add_adapter(clconf_adapter_t *);
	bool remove_adapter(clconf_adapter_t *);
	bool change_adapter(clconf_adapter_t *);
	bool add_path(clconf_path_t *);
	bool remove_path(clconf_path_t *);
	bool node_alive(nodeid_t, incarnation_num);
	bool node_died(nodeid_t, incarnation_num);
	bool path_up(clconf_path_t *);
	bool path_down(clconf_path_t *);
	bool disconnect_node(nodeid_t, incarnation_num);
	void link_status_update(fp_adapter *, bool);
	void shutdown();
	_SList::ListElem *get_pmlist_elem();

	fpconf();
	~fpconf();

	void get_data_and_incr_count(ushort_t, uchar_t, uint_t,
	    ret_data_t &, size_t, network::cid_t&);
	void get_data_and_incr_count(ushort_t, uchar_t, uint_t,
	    ret_data_t &, network::cid_t&, uint32_t&);
	void get_broadcastdata_incr_count(ushort_t, uchar_t,
	    ret_data_t &, nodeid_t&);
	void decr_count(uint_t nodeid, ret_data_t &);
	bool bandwidth_allocator(size_t msg_size);

	void config_up();
	void config_down();

	void register_with_ns();
	void fp_ns_if();
	network::cl_net_ns_if_ptr get_node_obj(int);

	//
	// List of all known ethertypes and a way to map an ethertype to
	// an index in the array. Used to precompute dlpi fast path headers
	// and store them so that the can be retrieved quickly based on
	// the ethertype. Any particular ethertype has the same index in
	// the ethertypes array below and in the fast path header arrays
	// fp_ucast_headers and fp_bcast_headers in fp_holder objects.
	// The number of ethertypes is ETHERTYPE_COUNT.
	//
	static int ethertype_to_index(ushort_t);
	static ushort_t ethertypes[ETHERTYPE_COUNT];

private:
	pernodepath *paths[NODEID_MAX + 1];
	int work_count[NODEID_MAX + 1];
	os::mutex_t work_lock;
	os::condvar_t work_cv;
	os::mutex_t path_lock;

	t_uscalar_t	clprivnet_mtu;
	void		recompute_clprivnet_mtu();

	// private copy constructor will disallow pass by value
	fpconf(const fpconf &);

	// private assignment operator will disallow assignments
	fpconf &operator = (fpconf &);
};

class cl_net_ns_if_impl_t : public McServerof<network::cl_net_ns_if> {
public:
	cl_net_ns_if_impl_t(fpconf *f) {fpc = f; }
	~cl_net_ns_if_impl_t() {}

	void get_mac_info(network::macinfoseq_t_out macinfo, Environment& e);
	void _unreferenced(unref_t);
private:
	fpconf *fpc;
};

#ifdef _KERNEL



class pkt_redirector {

public:
	pkt_redirector();
	~pkt_redirector();
	void _unreferenced(unref_t);

#ifdef _KERNEL

	void redirect_packet(nodeid_t, ushort_t, queue_t *, mblk_t *);

	network::cid_action_t extract_pkt_info(queue_t *, mblk_t *,
	    network::cid_t& cid, nodeid_t ret_node,
	    ushort_t, uint32_t&);

	mblk_t *create_dlpi_hdr(size_t, uint_t);

	void pkt_redirector::get_ip_redirect_data(ushort_t, uchar_t,
	    nodeid_t, network::cid_t&, ret_data_t&, uint32_t&);

	void pkt_redirector::get_broadcast_redirect_data(ushort_t,
	    uchar_t, ret_data_t&, nodeid_t&);

	void pkt_redirector::xfer_packet(mblk_t *, ret_data_t&, uchar_t, bool);

	void fragment_timeout(queue_t *q, mblk_t *mp);

#endif	// _KERNEL

private:

	// private copy constructor will disallow pass by value
	pkt_redirector(const pkt_redirector &);

	// private assignment operator will disallow assignments
	pkt_redirector &operator = (pkt_redirector &);
};



/*
 * IP Fragment Manager Implementation
 */

/*
 * Unode notes: only a small portion of fragment_manager is used in unode
 * and none of fastpath and fragment handling, so _KERNEL is around
 * most of this file.
 */

// fragment class is the holder class for ip fragments.
//
// IPV6: Fields in this class are modified for IPV6. All addresses are
// 128-bit, and ident is 32-bit. For IPV4 fragments, make sure all
// addresses are V4-mapped before using this class. Assigning the IPV4
// 16-bit ident to the 32-bit ident of this class should work, since the
// most significant 16 bits are zero-filled by the compiler.
//
class fragment : public _DList::ListElem {
public:

	~fragment();
	fragment(uint32_t id, const in6_addr_t& src, const in6_addr_t& dst,
	    uint16_t offset, uint16_t extent);

	void set_node(nodeid_t n, mblk_t *mp);
	void update_info(uint8_t prot, uint16_t sport, uint16_t dport);

	bool valid_node();
	nodeid_t get_nodeid(network::cid_t& cid);
	void add_mp_to_list(mblk_t *mp);
	os::mutex_t fragment_mutex;
	bool compare(uint32_t id, const in6_addr_t& src,
	    const in6_addr_t& dst);
	bool extend(uint16_t offset, uint16_t extent);
	time_t start_time;
private:
	uint32_t ident;
	in6_addr_t srcaddr;
	in6_addr_t dstaddr;
	uint8_t proto;
	uint16_t srcport;
	uint16_t dstport;
	nodeid_t nodeid;
	mblk_t *mp_list;
	uint16_t frag_offset;
};

typedef IntrList<fragment, _DList> fragment_list_t;




class fragment_manager {

public:
	fragment_manager(uint32_t, time_t);
	~fragment_manager();

	fragment *fragment_compare(queue_t *q, uint16_t ident,
	    uint32_t srcaddr, uint32_t dstaddr, uint32_t proto,
	    uint16_t offset, uint16_t extent, bool mf, bool *tbd);

	fragment *fragment_compare_v6(queue_t *q, uint32_t ident,
	    const in6_addr_t& srcaddr, const in6_addr_t& dstaddr,
	    uint16_t offset, uint16_t extent,
	    bool mf, bool *tbd);

	void fragment_timer_expired(queue_t *q, mblk_t *mp);

private:

	//
	// An array of lists is used for fragment processing. The <src-ip,
	// ident> pair uniquely identifies the list where the frag holder
	// object can be found.
	//
	fragment_list_t *fragment_list_array;

	fragment_list_t& get_fragment_list(const in6_addr_t& src, uint32_t id);

	uint32_t	fragment_hash_size;

	os::mutex_t fragment_list_lock;
	bool fragment_timer_running;
	mblk_t *fragment_timer_mp;

	time_t	fragment_timeout_period;


	// Dont allow the operators to be overloaded.
	// Disallow assignments and pass by value
	fragment_manager(const fragment_manager &);
	fragment_manager &operator = (fragment_manager &);

};
#endif // _KERNEL

#include <orb/ip/fpconf_in.h>


#endif	/* _FPCONF_H */
