//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ipconf.cc	1.57	09/02/13 SMI"

#include <orb/ip/ifkconfig.h>
#include <orb/invo/common.h>
#include <orb/ip/ipconf.h>
#include <sys/stream.h>
#include <sys/strsubr.h>
#include <sys/strsun.h>
#include <sys/socketvar.h>
#include <sys/clconf_int.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/transport/path_manager.h>
#include <orb/infrastructure/orbthreads.h>



ipconf *ipconf::the_ipconf = NULL;

void
ipconf_task::execute()
{
	ipconf::the().execute();
}

void
ipconf_task::task_done()
{
}

ipconf_path_elem::ipconf_path_elem(clconf_path_t *pth) :
    is_up(false), path(pth), _SList::ListElem(this)
{
}

// Find the list element containing the path.
ipconf_path_elem *
ipconf::find(clconf_path_t *pth)
{
	ASSERT(lck.lock_held());
	ipconf_pathlist_t::ListIterator iter(paths);
	ipconf_path_elem *current;
	while ((current = iter.get_current()) != NULL &&
	    current->path != pth) {
		iter.advance();
	}
	ASSERT(current != NULL);
	ASSERT(current->path == pth);
	return (current);
}

ipconf::ipconf() :
    in_threadpool(false),
    tcp_so(NULL),
    rs_vnode(NULL),
    rs_seq(0),
    vm_adm(NULL)
{
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		current_path[i] = NULL;
		current_adapter[i] = NULL;
		processing_needed[i] = false;
		node_reject_route[i] = false;
	}

	bzero(&rs_msg, sizeof (rs_msg));
	bzero(addr_addrs, sizeof (addr_addrs));
}

int
ipconf::initialize_int()
{
	struct sockparams	*sp = NULL;
	int			the_err;

	//
	// get vm_admin object
	//
	vm_adm = vm_util::get_vm();
	if (CORBA::is_nil(vm_adm))
		CL_PANIC(!"ipconf: Could not get vm_admin object");

	//
	// Configure the local pernode address (used by cluster resident
	// applications) on /dev/clprivnet0
	// Should never fail; but on rare occasions we have seen EINTR
	// returned (from setifname function in ifkconfig).
	// Return error after logging. clconfig in the bootcluster script
	// will return an error, and the node goes into halt. Essentially,
	// we do not want this node to continue booting.
	//
	if ((the_err = plumb_clprivnet()) != 0) {
		return (the_err);
	}

	//
	// Open a TCP socket that will be used to close TCP connections to
	// disconnected nodes quickly. Failure to create this socket (unlikely)
	// will result in disabling the optimization that enables quick TCP
	// connection aborts.
	//
#if	SOL_VERSION >= __s11

	ksocket_t ksp;

	ksocket_socket(&ksp, AF_INET, SOCK_STREAM, 0, KSOCKET_SLEEP, CRED());
	tcp_so = ((struct sonode *)(ksp));
#else
	vnode_t	*vp;
	if ((vp = solookup(AF_INET, SOCK_STREAM, 0, NULL, &the_err)) != NULL) {
		tcp_so = socreate(vp, AF_INET, SOCK_STREAM, 0, SOV_SOCKSTREAM,
		    NULL, &the_err);
	}
#endif

	// Initialize the rs_msg struct for use with the routing socket.
	// (See "man -s 7p route" for information on this public interface)

	rs_msg.hdr.rtm_version = RTM_VERSION;
	rs_msg.hdr.rtm_type = RTM_ADD;
	rs_msg.hdr.rtm_seq = ++rs_seq;
	rs_msg.hdr.rtm_addrs = RTA_DST | RTA_GATEWAY | RTA_NETMASK;

	// Prepare a struct sockaddr_in for adding addresses to route message
	struct sockaddr_in rs_address, rs_netmask;
	bzero(&rs_address, sizeof (rs_address));
	bzero(&rs_netmask, sizeof (rs_netmask));
	rs_address.sin_family = AF_INET;

	// Destination network
	(void) clconf_get_user_ipaddr(1, &(rs_address.sin_addr));
	(void) clconf_get_user_netmask(&(rs_netmask.sin_addr));

	// Convert to host order for bitwise operations
	rs_address.sin_addr.s_addr = ntohl(rs_address.sin_addr.s_addr) &
					ntohl(rs_netmask.sin_addr.s_addr);

	// Convert back to network order
	rs_address.sin_addr.s_addr = htonl(rs_address.sin_addr.s_addr);

	bcopy(&rs_address, rs_msg.adrs, sizeof (rs_address)); //lint !e419
	addr_addrs[0] = rs_msg.adrs + ((char *)&(rs_address.sin_addr) -
	    (char *)&rs_address);

	// Gateway (fake, because we're using RTF_REJECT, so just use loopback)
	rs_address.sin_addr.s_addr = htonl((uint32_t)0x7F000001);
	bcopy(&rs_address, rs_msg.adrs + sizeof (rs_address),
	    sizeof (rs_address)); //lint !e419
	addr_addrs[1] = addr_addrs[0] + sizeof (rs_address);

	// Netmask for network of pernode addresses
	bcopy(&rs_netmask, rs_msg.adrs + sizeof (rs_netmask) * 2, //lint !e419
	    sizeof (rs_netmask)); //lint !e419
	addr_addrs[2] = addr_addrs[1] + sizeof (rs_netmask);

	// The size of the message
	rs_msg.hdr.rtm_msglen = (uint32_t)sizeof (rs_msg) +
	    3 * (uint32_t)sizeof (rs_address);

	//
	// Look up the routing socket.  We don't use AF_INET as it says to
	// in the man page because the socket isn't actually listed under
	// that category.  Specifying the address family is only important
	// if you are reading from the socket...it is irrelevant if you are
	// writing to it.  We shut down the reading side of the socket a few
	// lines down.
	//

#if 	SOL_VERSION >= __s11

	the_err = ksocket_socket(&ksp, PF_ROUTE, SOCK_RAW, AF_INET,
	    KSOCKET_SLEEP, CRED());
	if (the_err) {
		//
		// SCMSGS
		// @explanation
		// The system prepares IP communications across the private
		// interconnect. A lookup operation on the routing socket
		// failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: ksocket creation on routing socket failed"
		    " with error = %d", the_err);
		return (the_err);
	}

	// We don't need to read back our messages from the routing socket

	ksocket_shutdown(ksp, SHUT_RD, CRED());

	//
	// Retrieve the sonode from the ksocket_t.
	// Retreive the vnode from the sonode.
	//
	struct sonode *so = ((struct sonode *)ksp);
	rs_vnode = SOTOV(so);
#else	//  SOL_VERSION >= __s11
	vnode_t *avp;
	avp = solookup(PF_ROUTE, SOCK_RAW, AF_UNSPEC, NULL, &the_err);
	if (avp == NULL) {
		//
		// SCMSGS
		// @explanation
		// The system prepares IP communications across the private
		// interconnect. A lookup operation on the routing socket
		// failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: solookup on routing socket failed with error = %d",
		    the_err);
		return (the_err);
	}

	// Create our own routing socket with the avp.
	struct sonode *rsocket = socreate(avp, PF_ROUTE, SOCK_RAW, AF_INET,
	    SOV_SOCKSTREAM, NULL, &the_err);
	if (rsocket == NULL) {
		//
		// SCMSGS
		// @explanation
		// The system prepares IP communications across the private
		// interconnect. A socket create operation on the routing
		// socket failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: socreate on routing socket failed with error = %d",
		    the_err);
		return (the_err);
	}

	// We don't need to read back our messages from the routing socket
	(void) soshutdown(rsocket, SHUT_RD);

	// Retrieve the vnode of the socket
	rs_vnode = SOTOV(rsocket);
#endif	// SOL_VERSION >= __s11
	ASSERT(rs_vnode != NULL);

	//
	// Add Host reject routes for remote nodes which are configured. When
	// the first path to a remote node becomes active, the reject route
	// for that node will be deleted.
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if (orb_conf::node_configured(i) && orb_conf::local_nodeid()
		    != i)
			update_reject_route(i, RTM_ADD);
	}

	return (the_err);
}

//
// External interface for cl_haci
//
extern "C" bool
ipconf_is_initialized()
{
	return (ipconf::is_initialized());
}

int
ipconf::initialize()
{
	int the_err;
	ipconf *t_ipconf = NULL;

	ASSERT(the_ipconf == NULL);
	t_ipconf = new ipconf;
	ASSERT(t_ipconf != NULL);

	the_err = t_ipconf->initialize_int();
	the_ipconf = t_ipconf;

	if (the_err != 0)
		return (the_err);

	path_manager::the().add_client(path_manager::PM_OTHER, the_ipconf);

	return (0);
}

int
ipconf::plumb_clprivnet()
{
	int	rc = 0;
	ifk_interface_t local_if;

	int cmd = IFK_PLUMB | IFK_NETMASK | IFK_IPADDR | IFK_UP |
	    IFK_ARP | IFK_PRIVATE;

	local_if.ifk_device_name = "clprivnet";
	local_if.ifk_device_instance = 0;
	local_if.ifk_vlan_id = 0;
	local_if.ifk_vlan_device_name = NULL;
	local_if.ifk_logical_unit = IFK_PHYSICAL;
	(void) clconf_get_user_netmask(&(local_if.ifk_netmask));
	(void) clconf_get_user_ipaddr(orb_conf::local_nodeid(),
	    &(local_if.ifk_ipaddr));

	if (ifkconfig(&local_if, cmd, &rc) != 0) {
		//
		// SCMSGS
		// @explanation
		// The system failed to configure IP communications across the
		// private interconnect of this device and IP address,
		// resulting in the error identified in the message. This
		// happened during initialization. Someone has used the
		// "lo0:1" device before the system could configure it.
		// @user_action
		// If you used "lo0:1", use another device. Otherwise, Contact
		// your authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: per node IP config %s%d:%d (%d): %d.%d.%d.%d "
		    "failed with %d",
		    local_if.ifk_device_name, local_if.ifk_device_instance,
		    local_if.ifk_logical_unit, cmd,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b1,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b2,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b3,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b4, rc);
		cmn_err(CE_WARN, "clcomm: per node IP config %s%d:%d (%d): "
		    "%d.%d.%d.%d failed with %d\n",
		    local_if.ifk_device_name, local_if.ifk_device_instance,
		    local_if.ifk_logical_unit, cmd,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b1,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b2,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b3,
		    local_if.ifk_ipaddr._S_un._S_un_b.s_b4, rc);
	}
	return (rc);
}

int
ipconf::unplumb_clprivnet()
{
	int rc = 0;
	ifk_interface_t local_if;
	int cmd = IFK_DESTROY;

	local_if.ifk_device_name = "clprivnet";
	local_if.ifk_device_instance = 0;
	local_if.ifk_vlan_id = 0;
	local_if.ifk_vlan_device_name = NULL;
	local_if.ifk_logical_unit = IFK_PHYSICAL;

	if (ifkconfig(&local_if, cmd, &rc) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: failed to unplumb clprivnet0, rc %d\n", rc);
	}
	return (rc);
}

int
ipconf::update_clprivnet_mtu()
{
	int rc = 0;
	//
	// Rather that setting the mtu explicitly we just unplumb and replumb
	// back. This is to avoid race conditions with the driver regarding
	// the current value of the mtu.
	//
	if ((rc = unplumb_clprivnet()) == 0) {
		rc = plumb_clprivnet();
	}
	return (rc);
}

// We don't use NODEID_MAX in this algorithm because it's inherently based
// on a maximum of 64 nodes.  It will need to be changed if NODEID_MAX is
// changed.
void
ipconf::get_pairwise_ipaddr(nodeid_t src, nodeid_t dst, struct in_addr *addr)
{
	nodeid_t high_id, low_id;

	const struct in_addr *mynet = NULL;
	ASSERT(src >= 1 && src <= 64);
	ASSERT(dst >= 1 && dst <= 64);

	mynet = clconf_get_cluster_network_addr();
	ASSERT(mynet != NULL);

	ASSERT(src != dst);

	addr->_S_un._S_un_w.s_w1 = mynet->_S_un._S_un_w.s_w1;
	addr->_S_un._S_un_w.s_w2 = 0;

	// convert to host byte order for bit wise operations
	addr->s_addr = ntohl(addr->s_addr);
	addr->s_addr |= (uint32_t)LOGICAL_PAIRWISE_MASK;

	if (src > dst) {
		addr->s_addr |= (uint32_t)LOGICAL_PAIRWISE_DST_LOW_TAG;
		high_id = src;
		low_id = dst;
	} else {
		addr->s_addr |= (uint32_t)LOGICAL_PAIRWISE_DST_HIGH_TAG;
		high_id = dst;
		low_id = src;
	}

	ASSERT(low_id != 64);

	// We map node 64 to zero
	if (high_id == 64)
		high_id = 0;

	addr->s_addr |= (uint32_t)(high_id << LOGICAL_PAIRWISE_HIGH_SHIFT);
	addr->s_addr |= (uint32_t)(low_id <<
	    LOGICAL_PAIRWISE_LOW_SHIFT);

	// convert back to network byte order
	addr->s_addr = htonl(addr->s_addr);
}

// ifkconfig an IP.
// ndid: the remote node the IP is used to talk to.
// ip_ada: the local adapter to host the IP.
// cmd: the command for ifkconfig.
void
ipconf::pairwise_config(nodeid_t ndid, ipconf_adapter_t *ip_ada, int cmd)
{
	ASSERT(ip_ada != NULL);
	ifk_interface_t netint;
	int the_err;

	netint.ifk_device_name = ip_ada->dname;
	netint.ifk_device_instance = ip_ada->dinst;
	netint.ifk_vlan_id = ip_ada->dvlanid;
	netint.ifk_vlan_device_name = NULL; // ifkconfig ignores name
	netint.ifk_logical_unit = ndid; //lint !e713

	if ((cmd & IFK_IPADDR) != 0) {
		// Configure the IP. Shouldn't be destroying it at the same
		// time.
		ASSERT((cmd & IFK_DESTROY) == 0);
		get_pairwise_ipaddr(orb_conf::local_nodeid(), ndid,
		    &netint.ifk_ipaddr);
	}

	if ((cmd & IFK_NETMASK) != 0) {
		// configure the netmask. Should only go along with
		// configuring the ip.
		ASSERT((cmd & IFK_IPADDR) != 0);
		netint.ifk_netmask.s_addr = htonl(
		    (uint32_t)LOGICAL_PAIRWISE_NETMASK);
	}

	if (ifkconfig(&netint, cmd, &the_err) != 0) {
		//
		// SCMSGS
		// @explanation
		// The system failed to configure private network device for
		// IP communications across the private interconnect of this
		// device and IP address, resulting in the error identified in
		// the message.
		// @user_action
		// Ensure that the network interconnect device is supported.
		// Otherwise, Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: can't ifkconfig private interface: "
		    "%s:%d cmd %d error %d", netint.ifk_device_name,
		    netint.ifk_device_instance, cmd, the_err);
	}
}

// pernode_config simply finds the pernode address of the remote node,
// the remote pairwise address between the nodes, and then sets a route
// with the supplied cmd and flags using the already opened routing socket.
void
ipconf::pernode_config(nodeid_t rnode, uchar_t cmd, int flags)
{
	struct in_addr	rs_addr;
	int the_err;

	// synchronize use of rs_msg
	rs_msg_lock.lock();

	rs_msg.hdr.rtm_type = cmd;
	rs_msg.hdr.rtm_seq = ++rs_seq;
	rs_msg.hdr.rtm_flags = flags;

	(void) clconf_get_user_ipaddr(rnode, &rs_addr);
	bcopy(&rs_addr, addr_addrs[0], sizeof (rs_addr));

	get_pairwise_ipaddr(rnode, orb_conf::local_nodeid(), &rs_addr);

	bcopy(&rs_addr, addr_addrs[1], sizeof (rs_addr));

	rs_addr.s_addr = htonl((uint32_t)LOGICAL_PERNODE_NETMASK);
	bcopy(&rs_addr, addr_addrs[2], sizeof (rs_addr));

	the_err = vn_rdwr(UIO_WRITE, rs_vnode, (caddr_t)(&rs_msg),
	    (ssize_t)rs_msg.hdr.rtm_msglen, (int64_t)0, UIO_SYSSPACE, 0,
	    (rlim64_t)MAXOFF32_T, kcred, NULL);

	rs_msg_lock.unlock();

	if (the_err != 0) {
		//
		// SCMSGS
		// @explanation
		// The system prepares IP communications across the private
		// interconnect. A write operation to the routing socket
		// failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Couldn't write to routing socket: %d", the_err);
	}
}

//
// ipconf::pernode_disconnect.
//
// This method is called from ipconf::use_path when the last path to a
// remote node goes down to abort all TCP connections to the remote
// per node IP address. It calls perip_disconnect to do the real job.
//
void
ipconf::pernode_disconnect(nodeid_t rnode)
{
	struct in_addr		raddr;

	(void) clconf_get_user_ipaddr(rnode, &raddr);

	perip_disconnect(&raddr);

#if SOL_VERSION >= __s10
	privip_disconnect(rnode);
#endif
}

//
// ipconf::pairwise_disconnect.
//
// This method is called from ipconf::use_path when the last path to a
// remote node goes down to abort all TCP connections to the remote
// pair wise IP address. It calls perip_disconnect to do the real job.
//
void
ipconf::pairwise_disconnect(nodeid_t rnode)
{
	struct in_addr		raddr;

	get_pairwise_ipaddr(rnode, orb_conf::local_nodeid(), &raddr);

	perip_disconnect(&raddr);
}

//
// ipconf::perip_disconnect.
//
// This method is called from pernode_disconnect and pairwise_disconnect to
// abort all TCP connections to the respective IP addresses. This method uses
// the TCP_IOC_ABORT_CONN ioctl (PSARC 2001/292) to quickly abort all TCP
// connections to the per node HA-IP address hosted at the remote node.
//
// The TCP_IOC_ABORT_CONN ioctl is available s8u7 and beyond. Bits built
// with earlier Solaris bases treat ipconf::perip_disconnect as a no-op.
//
#ifdef TCP_IOC_ABORT_CONN
void
ipconf::perip_disconnect(struct in_addr *raddrp)
{
	struct sockaddr_in	*sinp;
	struct strioctl 	iocb;
	tcp_ioc_abort_conn_t	tcpac;

	if (tcp_so == NULL) {
		return;
	}

	// Setup the TCP_IOC_ABORT_CONN ioctl connection filters.
	// First the local connection filter.
	tcpac.ac_local.ss_family  = AF_INET;
	sinp = (struct sockaddr_in *)&tcpac.ac_local;
	sinp->sin_addr.s_addr = INADDR_ANY;
	sinp->sin_port = 0;

	// The remote connection filter.
	tcpac.ac_remote.ss_family = AF_INET;
	sinp = (struct sockaddr_in *)&tcpac.ac_remote;
	sinp->sin_addr.s_addr = raddrp->s_addr;
	sinp->sin_port = 0;

	// TCP connection state filter.
	tcpac.ac_start	= TCPS_SYN_SENT;
	tcpac.ac_end	= TCPS_TIME_WAIT;

	// Prepare the ioctl.
	iocb.ic_cmd	= TCP_IOC_ABORT_CONN;
	iocb.ic_timout	= 0;
	iocb.ic_len	= (int)sizeof (tcp_ioc_abort_conn_t);
	iocb.ic_dp	= (char *)&tcpac;

	// Send the ioctl.

#if	SOL_VERSION >= __s11
	int32_t error = 0;
	SOP_IOCTL(tcp_so, I_STR, (intptr_t)&iocb, FKIOCTL, CRED(), &error);
#else
	(void) kstr_ioctl(SOTOV(tcp_so), I_STR, (intptr_t)&iocb);
#endif
}
#else
void
ipconf::perip_disconnect(struct in_addr *)
{
}
#endif // TCP_IOC_ABORT_CONN

// Use the new path to host the logical IP for talking to the node ndid.
// It will bring the old IP down if necessary. It's only called from
// ipconf::execute.
void
ipconf::use_path(nodeid_t ndid, clconf_path_t *newpath)
{
	ASSERT(current_path[ndid] != newpath);
	ASSERT(lck.lock_held());

	clconf_cluster_t *cl = NULL;
	ipconf_adapter_t *ada_old = NULL;
	ipconf_adapter_t *ada_new = NULL;
	clconf_adapter_t *ada_tmp = NULL;

	if (newpath != NULL) {
		cl = clconf_cluster_get_current();
		ada_tmp = clconf_path_get_adapter(newpath, CL_LOCAL, cl);
		ASSERT(ada_tmp != NULL);
		ada_new = new ipconf_adapter_t;
		ada_new->dname =
		    os::strdup(clconf_adapter_get_dlpi_device_name(ada_tmp));
		ada_new->dinst = clconf_adapter_get_device_instance(ada_tmp);
		ada_new->dvlanid = clconf_adapter_get_vlan_id(ada_tmp);
		clconf_obj_release((clconf_obj_t *)cl);
	}

	if (current_path[ndid] != NULL) {
		ada_old = current_adapter[ndid];
		ASSERT(ada_old != NULL);
	}

	// Put the ip mapping into the hash table. Note that for a short time
	// between ifkconfig and putting the new mapping the hashtable the
	// mapping isn't correct. This is part of the semantics and
	// sender should be able to handle it.
	set_ip_mapping(ndid, newpath);

	// Drop the lock before try to configure.
	lck.unlock();

	// As soon as we drop the lock the clconf_path may be destroyed
	// by a remove_path. We need to be careful not to access clconf_path
	// any more.

	// Don't need to do anything if the adapters are the same for both
	// paths.
	if ((ada_old == NULL) || (ada_new == NULL) ||
	    (strcmp(ada_old->dname, ada_new->dname) != 0) ||
	    (ada_old->dinst != ada_new->dinst) ||
	    (ada_old->dvlanid != ada_new->dvlanid)) {
		// Bring the old interface down.
		if (current_path[ndid] != NULL) {
			pairwise_config(ndid, ada_old, IFK_DOWN);
		}

		// Bring the new interface up.
		if (newpath != NULL) {
			pairwise_config(ndid, ada_new, IFK_CREATE |
			    IFK_IPADDR | IFK_NETMASK | IFK_UP | IFK_PRIVATE);
		}

		// If this is the first connection, we set the per-node route
		if ((current_path[ndid] == NULL) && (newpath != NULL)) {
			pernode_config(ndid, RTM_ADD,
			    RTF_UP | RTF_STATIC | RTF_GATEWAY | RTF_HOST);
		}

		// If this is the last connection, we delete the route
		if ((current_path[ndid] != NULL) && (newpath == NULL)) {
			//
			// Delete the current route.
			//
			pernode_config(ndid, RTM_DELETE, 0);

			//
			// Abort all existing connections to the remote node
			// so that the applications do not have to wait for
			// the TCP connection timeouts to happen to realize
			// that the remote node has become unreachable.
			// Connections to both the remote pernode and the
			// remote pairwise addresses need to be aborted.
			// Connections that were made by connect calls at this
			// node will have the pernode address as the remote
			// address. However, connections that were accepted by
			// this node using the accept system call might have
			// pairwise address as the remote address if the
			// application did not do a bind.
			//
			pernode_disconnect(ndid);
			pairwise_disconnect(ndid);
		}

		// Destroy the old interface.
		if (current_path[ndid] != NULL) {
			pairwise_config(ndid, ada_old, IFK_DESTROY);
		}
	}

	current_path[ndid] = newpath;
	current_adapter[ndid] = ada_new;

	// Free the hold on the old adapter.
	if (ada_old != NULL) {
		delete [] ada_old->dname;
		delete ada_old;
	}

	// Regain the lock.
	lck.lock();
}

void
ipconf::defer_job(nodeid_t ndid)
{
	ASSERT(lck.lock_held());
	processing_needed[ndid] = true;
	if (!in_threadpool) {
		common_threadpool::the().defer_processing(&the_task);
		in_threadpool = true;
	}
}

void
ipconf::clprivnet_mtu_changed()
{
	lock();
	defer_job(orb_conf::local_nodeid());
	unlock();
}

bool
ipconf::add_path(clconf_path_t *pth)
{

	ASSERT(pth != NULL);
	ipconf_path_elem *elemp = new ipconf_path_elem(pth);
	lock();
	paths.append(elemp);

	unlock();
	return (true);
}

bool
ipconf::remove_path(clconf_path_t *pth)
{
	nodeid_t remote_node;
	clconf_path_t *newpath;

	ASSERT(pth != NULL);
	// acquire path_lock before acquiring lck and calling use_path()
	path_lock.lock();
	lock();
	// Remove it from our list.
	ipconf_path_elem *pelem = find(pth);

	(void) paths.erase(pelem);
	delete pelem;

	// Do not defer the job here. We want this to happen synchronously.
	remote_node = clconf_path_get_remote_nodeid(pth);

	//
	// If the remote node is running the old pairwise subnet code,
	// then determine the best new path to use for the subnet.
	// In this case, if there are no active paths remaining,
	// pernode_disconnect will be called from use_path.
	//
	if (is_pairwise_subnet_version(remote_node)) {
		newpath = get_path(remote_node);
		if (newpath != current_path[remote_node])
			use_path(remote_node, newpath);

		// Add reject route if there are no active paths
		if (newpath == NULL)
			update_reject_route(remote_node, RTM_ADD);
	} else {
		//
		// Just check for at least one active path
		// If no active paths then add reject route and abort all
		// existing TCP connections.
		//
		newpath = active_path_check(remote_node);
		if (newpath == NULL) {
			update_reject_route(remote_node, RTM_ADD);
			// Release the ipconf lock before calling
			// pernode_disconnect.
			unlock();
			pernode_disconnect(remote_node);
			lock();
		}
	}


	unlock();
	path_lock.unlock();
	return (true);
}

bool
ipconf::path_up(clconf_path_t *pth)
{

	ASSERT(pth != NULL);
	lock();

	// update info
	ipconf_path_elem *pelem = find(pth);
	pelem->is_up = true;

	// defer processing
	defer_job(clconf_path_get_remote_nodeid(pth));

	unlock();
	return (true);
}

bool
ipconf::path_down(clconf_path_t *pth)
{
	ASSERT(pth != NULL);
	lock();

	// update info
	ipconf_path_elem *pelem = find(pth);
	pelem->is_up = false;

	// defer processing
	defer_job(clconf_path_get_remote_nodeid(pth));
	unlock();
	return (true);
}

bool
ipconf::disconnect_node(nodeid_t ndid, incarnation_num)
{
	lock();

	// Mark all the paths to that node as down.
	ipconf_pathlist_t::ListIterator iter(paths);
	ipconf_path_elem *pelem;
	while ((pelem = iter.get_current()) != NULL) {
		if (clconf_path_get_remote_nodeid(pelem->path) == ndid) {
			pelem->is_up = false;
		}
		iter.advance();
	}

	// defer processing
	defer_job(ndid);

	unlock();
	return (true);
}

//
// Return true if the node-pair is running the pairwise subnet protocol.
// Called with the ipconf lock held.
//
bool
ipconf::is_pairwise_subnet_version(nodeid_t remote_node)
{
	version_manager::vp_version_t vpv;
	sol::incarnation_num incn = 0;
	Environment			e;

#ifdef DEBUG
	//
	// check that node is configured
	//
	if (!orb_conf::node_configured(remote_node))
		ASSERT(0);
#endif // DEBUG

	//
	// Retrieve protocol version
	//
	vm_adm->get_nodepair_running_version("ipconf", remote_node, incn,
	    vpv, e);
	if (e.exception()) {
		//
		// Something has gone wrong; but we will try to operate
		// in version 1.0 mode
		//
#ifdef DEBUG
		(e.exception())->print_exception("ipconf: vm_adm exception "
			"in determining version, will try to operate "
			"in version 1.0 mode");
#endif // DEBUG
		return (true);
	}

	if ((vpv.major_num == 1) && (vpv.minor_num == 0))
		return (true);
	else
		return (false);


}



//
//
// The host reject route must be deleted when there is at least one active
// path; and added when there are no active paths.
//
void
ipconf::update_reject_route(nodeid_t rnode, uchar_t cmd)
{
	struct in_addr		rs_addr;
	int			the_err;
	int			flags = 0;

	// synchronize use of rsm_msg
	rs_msg_lock.lock();

	//
	// If the update isn't necessary just return.
	//
	if (((cmd == RTM_ADD) && (node_reject_route[rnode])) ||
	    ((cmd == RTM_DELETE) && (!node_reject_route[rnode]))) {
		rs_msg_lock.unlock();
		return;
	}

	if (cmd == RTM_ADD)
		flags = (RTF_UP | RTF_REJECT | RTF_STATIC | RTF_PRIVATE |
		    RTF_HOST);
	else
		flags = RTF_HOST;


	rs_msg.hdr.rtm_type = cmd;
	rs_msg.hdr.rtm_seq = ++rs_seq;
	rs_msg.hdr.rtm_flags = flags;

	// get address for rejection
	(void) clconf_get_user_ipaddr(rnode, &rs_addr);
	bcopy(&rs_addr, addr_addrs[0], sizeof (rs_addr));

	// use loopback address for reject gateway
	rs_addr.s_addr = htonl((uint32_t)0x7f000001);
	bcopy(&rs_addr, addr_addrs[1], sizeof (rs_addr));


	// add or delete  reject route through the routing socket
	the_err = vn_rdwr(UIO_WRITE, rs_vnode, (caddr_t)(&rs_msg),
	    (ssize_t)rs_msg.hdr.rtm_msglen, (int64_t)0, UIO_SYSSPACE, 0,
	    (rlim64_t)MAXOFF32_T, kcred, NULL);

	// update reject array
	node_reject_route[rnode] = (cmd == RTM_ADD) ? true : false;

	rs_msg_lock.unlock();

	if (the_err != 0) {
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Couldn't write to routing socket: %d", the_err);
	}

}


void
ipconf::execute()
{
	clconf_path_t	*newpath;

	// Whether we still have jobs to process.
	bool have_jobs = true;

	path_lock.lock();
	lock();
	ASSERT(in_threadpool);

	while (have_jobs) {
		nodeid_t nid;
		for (nid = 1; nid <= NODEID_MAX; nid ++) {
			if (processing_needed[nid]) {
				processing_needed[nid] = false;
				//
				// Processing needed for the local node implies
				// that the clprivnet mtu has changed and the
				// IP plumbing needs to be resynced.
				//
				if (nid == orb_conf::local_nodeid()) {
					unlock();
					if (update_clprivnet_mtu() != 0) {
						cmn_err(CE_PANIC, "Failed to"
						    " replumb clprivnet0\n");
					}
					lock();
					continue;
				}
				//
				// The host reject route must be deleted when
				// there is at least one active path and added
				// when there are no active paths. When there
				// removal, abort TCP connections.
				// If the remote node is running the old
				// pairwise subnet code, then determine the
				// best new path to use for the subnet; also
				// if there are not active paths remaining,
				// pernode_disconnect is called from use_path.
				//
				if (is_pairwise_subnet_version(nid)) {
					newpath = get_path(nid);
					if (newpath != current_path[nid])
						use_path(nid, newpath);
					update_reject_route(nid, newpath ?
					    RTM_DELETE : RTM_ADD);
				} else {
					newpath = active_path_check(nid);
					if (newpath == NULL) {
						update_reject_route(nid,
						    RTM_ADD);
						// Release the ipconf lock
						// before calling
						// pernode_disconnect.
						unlock();
						pernode_disconnect(nid);
						lock();
					} else {
						update_reject_route(nid,
						    RTM_DELETE);
					}
				}

			}
		}

		// Don't reset in_threadpool yet, this will prevent having two
		// threadpool thread running execute at the same time.
		//
		// The use_path function drops the lock before ifkconfig
		// the ip and grabs the lock before returning.
		//
		// As soon as we drop the lock, the paths list can change
		// and the newpath may no longer be accurate. What we do
		// in that case is move the ip to an incorrect adapter first,
		// and ifconfig it onto the correct adapter later the next
		// time we go through the loop.
		//
		// Something could have changed when we drop the lock
		// in use_path. We need to verify that there are indeed
		// no more jobs before returning.

		have_jobs = false;

		// Verify whether there really is no more jobs.
		for (nid = 1; nid <= NODEID_MAX; nid ++) {
			if (processing_needed[nid]) {
				have_jobs = true;
				break;
			}
		}
	}
	in_threadpool = false;
	unlock();
	path_lock.unlock();
}

//
// Scan path list. Return when an active path is found for the remote node.
// Return NULL if none found.
//
clconf_path_t *
ipconf::active_path_check(nodeid_t nid)
{
	ASSERT(nid != orb_conf::local_nodeid());
	ASSERT(lck.lock_held());


	ipconf_path_elem *pelem;
	ipconf_pathlist_t::ListIterator iter(paths);
	while ((pelem = iter.get_current()) != NULL) {
		if (clconf_path_get_remote_nodeid(pelem->path) == nid &&
		    pelem->is_up) {
			return (pelem->path);
		}
		iter.advance();
	}
	return (NULL);
}

// Pick a path from all paths between the two nodes. We use a hashing
// algorithm to achieve fairness among the adapters within a node.
clconf_path_t *
ipconf::get_path(nodeid_t nid)
{
	ASSERT(nid != orb_conf::local_nodeid());
	ASSERT(lck.lock_held());

	clconf_path_t *result = NULL;
	// Find out whether the bigger nodeid for the two ends of the paths
	// is local or remote.
	clconf_endtype_t bigend = (nid > orb_conf::local_nodeid() ?
	    CL_REMOTE : CL_LOCAL);

	// Choose one path from all the active paths to host the logical
	// ip. Both nodes should choose the same path using the algorithm.
	// The algorithm should fully utilize all the adapters on one
	// node to host different logical IPs.
	//
	// We create a key based on the two adapterid and two nodeid of
	// the path. Then we generate a random number deterministicly
	// from the key. The path with the biggest number generated gets
	// picked. This algorithm ensures that if the random number
	// generator is good, the adapters will be used fairly.
	ipconf_path_elem *pelem;
	uint_t hash_number = 0;		// The hash number from key.
	ipconf_pathlist_t::ListIterator iter(paths);
	while ((pelem = iter.get_current()) != NULL) {
		if (clconf_path_get_remote_nodeid(pelem->path) == nid &&
		    pelem->is_up) {
			// An easy to calculate key which is the same
			// on both nodes and should work well enough.
			uint_t key = orb_conf::local_nodeid() + nid +
			    (uint_t)clconf_path_get_adapterid(pelem->path,
			    CL_LOCAL) + (uint_t)clconf_path_get_adapterid(
			    pelem->path, CL_REMOTE);

			// The divident we use should be bigger than the
			// number of paths between nodes.
			uint_t tmphash = (key << 10) % 97;
			if (tmphash > hash_number ||
			    (tmphash == hash_number &&
			    clconf_path_get_adapterid(pelem->path, bigend) >
			    clconf_path_get_adapterid(result, bigend))) {
				hash_number = tmphash;
				result = pelem->path;
			}
		}
		iter.advance();
	}
	return (result);
}

//
// External interface for cl_haci
//
extern "C" in_addr_t
ipconf_get_physical_ip(in_addr_t logical_ip)
{
	return (ipconf::the().get_physical_ip(logical_ip));
}

// Map the specified logical ip to the physical ip of the adapter
// that's hosting the logical ip. If we can't find logical_ip_address
// in our mapping then we return logical_ip_address.
in_addr_t
ipconf::get_physical_ip(in_addr_t logical_ip)
{
	in_addr_t result;
	lock();
	result = iphash.lookup(logical_ip);
	unlock();
	if (result == NULL) {
		result = logical_ip;
	}
	return (result);
}

// Helper function. Set the logical ip to physical ip mapping using
// the specified path to the specified remote node.
void
ipconf::set_ip_mapping(nodeid_t remote_nid, clconf_path_t *newpath)
{
	ASSERT(lck.lock_held());

	// Get the logical IP hosted by the remote node for communicating with
	// the local node.
	struct in_addr logical_ip;
	get_pairwise_ipaddr(remote_nid, orb_conf::local_nodeid(), &logical_ip);

	in_addr_t phyaddr = (in_addr_t)0;
	if (newpath != NULL) {
		clconf_cluster_t *cl = clconf_cluster_get_current();
		// get the remote physical addr.
		clconf_adapter_t *remote_ada = clconf_path_get_adapter(
		    newpath, CL_REMOTE, cl);
		const char *phystr = clconf_obj_get_property(
		    (clconf_obj_t *)remote_ada, "ip_address");

		phyaddr = os::inet_addr(phystr);
		clconf_obj_release((clconf_obj_t *)cl);
	}
	unlock();

	// Can't do memory allocation with the lock held.
	base_hash_table::hash_entry *entptr = NULL;
	if (newpath != NULL) {
		entptr = new base_hash_table::hash_entry;
	}

	lock();
	// Remove current mapping first.
	(void) iphash.remove(logical_ip.s_addr);

	// add new mapping if any.
	if (newpath != NULL) {
		iphash.add(phyaddr, logical_ip.s_addr, entptr);
	}
}

#if SOL_VERSION >= __s10
void
ipconf::privip_disconnect(nodeid_t rnode)
{
	// Private IP list
	privip_map privateip_map;
	struct in_addr		raddr;
	privip_map_elem *privip_elem = NULL;

	// Initialize the private IP mapping information from the CCR
	if (privateip_map.initialize() != 0)
		return;

	// Read the privip_ccr table to identify the update
	if (privateip_map.read_ccr_mappings() != 0) {
		privateip_map.shutdown();
		return;
	}

	SList<privip_map_elem>::ListIterator iter(privateip_map.get_map_list());

	while ((privip_elem = iter.get_current()) != NULL) {
		if (privip_elem->get_nodeid() == rnode) {
			raddr._S_un._S_addr = htonl(privip_elem->get_addr());
			perip_disconnect(&raddr);
		}
		iter.advance();
	}
	privateip_map.shutdown();
}
#endif
