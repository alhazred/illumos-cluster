/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident "@(#)vlan.c 1.3       08/05/20     SMI"

#include <orb/ip/vlan.h>

/*
 * User priority values to be used for different types of interconnect
 * traffic.
 */
uint_t sc_hb_usr_pri	= 7; /* Heartbeats */
uint_t sc_orb_usr_pri	= 6; /* ORB traffic */
uint_t sc_app_usr_pri	= 6; /* Application traffic */
uint_t sc_scal_usr_pri	= 0; /* Scalable services forwarded traffic */

/*
 * IEEE user priority --> mblk b_band.
 *
 * Values in the array are same as those used by the
 * vlan_set_u_pri_to_bband function in the Solaris VLAN module.
 */
uchar_t sc_upri_to_bband[NUM_8021D_UPRI] = {0, 0, 0, 96, 128, 160, 192, 224};

/*
 * mblk b_band --> IEEE user priority.
 *
 * Values in the array are as documented under PSARC 2000/147.
 */
uint_t sc_bband_to_upri[256] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
	4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
	5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7
};

/*
 * Function to convert DLPI priority values into mblk b_band values.
 * Note that DLPI priority values are *not* the same as IEEE 802.1d
 * priority values. 802.1d priority values can be 0-7 where larger
 * the value, higher the priority. DLPI priority values can be 0-100
 * where lower the value, higher the priority. Solaris has adopted
 * a convention where DLPI priorities >7 are treated as equal to 7
 * and then a 1-1 mapping is created between the 8 effective DLPI
 * priorities and the 802.1d user priorities.
 */
char
sc_dlpri_to_bband_f(uint_t dlp)
{
	uint_t up;

	if (dlp > MAX_8021D_UPRI)
		dlp = MAX_8021D_UPRI;
	up = MAX_8021D_UPRI - dlp;
	return ((char)sc_upri_to_bband[up]);
}
