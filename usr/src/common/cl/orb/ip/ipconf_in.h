/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_IPCONF_IN_H
#define	_IPCONF_IN_H

#pragma ident	"@(#)ipconf_in.h	1.15	08/05/20 SMI"

#include <orb/ip/ipconf.h>
#include <cl_net/netlib.h>

inline bool
ipconf::add_adapter(clconf_adapter_t *)
{
	return (false);
}

inline bool
ipconf::change_adapter(clconf_adapter_t *)
{
	return (false);
}

inline bool
ipconf::remove_adapter(clconf_adapter_t *)
{
	return (false);
}

inline bool
ipconf::node_alive(nodeid_t, incarnation_num)
{
	return (false);
}

inline bool
ipconf::node_died(nodeid_t, incarnation_num)
{
	return (false);
}


inline void
ipconf::shutdown()
{
	ASSERT(paths.empty());
}

inline void
ipconf::lock()
{
	lck.lock();
}

inline void
ipconf::unlock()
{
	lck.unlock();
}

// Static
inline bool
ipconf::is_initialized()
{
	return (the_ipconf != NULL);
}

// Static
inline ipconf &
ipconf::the()
{
	ASSERT(the_ipconf != NULL);
	return (*the_ipconf);
}

#endif	/* _IPCONF_IN_H */
