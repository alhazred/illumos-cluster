/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _IPCONF_H
#define	_IPCONF_H

#pragma ident	"@(#)ipconf.h	1.28	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/list_def.h>
#include <sys/hashtable.h>
#include <orb/transport/pm_client.h>
#include <sys/threadpool.h>
#include <sys/os.h>
#include <h/network.h>
#include <orb/ip/fpconf.h>
#include <sys/socket.h>
#include <net/if.h>
#include <net/route.h>
#include <sys/vm_util.h>
#if SOL_VERSION >= __s10
#include <privip/privip_map_int.h>
#endif

extern "C" in_addr_t ipconf_get_physical_ip(in_addr_t logical_ip);
extern "C" bool ipconf_is_initialized(void);

class ipconf;
class fpconf;

// A ipconf_task is put onto a common threadpool when we need to use
// another thread to process path_up/path_down event. ipconf_task doesn't
// have any data because all the information is kept in the ipconf::the().
class ipconf_task : public defer_task {
public:
	void execute();
	void task_done();
};

// The element for the path list we are keeping. What we really need is
// the list of paths that are up. But because we can't allocate memory
// in path_up (may be called from clock interrupt), we must put
// a path into a list in add_path, and update its status when getting
// path_up/path_down.
class ipconf_path_elem : public _SList::ListElem {
	friend class ipconf;
public:
	ipconf_path_elem(clconf_path_t *);
private:
	bool is_up;	// Is the path up?
	clconf_path_t *path;
};

typedef IntrList<ipconf_path_elem, _SList> ipconf_pathlist_t;

// This is the class that's responsible for configuring IP addresses for
// the private network.
//
// It will register with the path_monitor to get the events about node
// alive/died and path up/down, and ifconfig up/down the corresponding
// interfaces.
//
// The  code functionality prior to 3.1 update 1 is as follows:
//
// The basic algorithm is as follows. ipconf will keep track of all the paths
// that are alive, and choose a path using a well known algorithm among nodes.
// Whenever a path_up/path_down is delivered, we choose a new path based on the
// algorithm, and if it's different from the path we are currently using,
// we ifkconfig the old ip address down and ifkconfig it up on the new adapter.
//
// The class also manages the pernode addresses and routes, ifconfiging the
// address for the node on a clone of the loopback device, adding a reject
// route for the class-c-size network used for pernode addresses, and then
// adding the route to a host when the first path goes up and removing it
// when the last path goes down.  (The reject route ensures that a client
// connecting to a node with no available paths will get an immediate failure
// without having to leave the machine to go to the default router.)
//
// Begining with 3.1 update 1, the pernode addresses are configured on the
// clprivnet DLPI driver.  This is a pseudo device driver which provides for
// striping application traffic over the underlying physical interfaces.
// Through use of the Node-Pair protocol of the Rolling Upgrade Version
// Manager, backward compatibility is provided.  A node pairwise determination
// is made as to whether the clprivnet interface will be used or if the older
// static route and pairwise subnet interface will be used.
//
// The pernode subnet reject route is now removed regardless of the negotiated
// node-pair protocol.  When the old version is to be used, static routes for
// the remote node will continue to be created and host reject routes will be
// created for nodes which are configured but not yet running.  The management
// of reject routes will be as follows:
//
// * add node reject route upon first path_add callback for remote node
// * remove node reject route upon first path_up callback for remote node
// * add node reject route upone node_died callback for remote node

class ipconf : public pm_client {
public:
	// Functions inheritted from pm_client. They are used by
	// path manager to deliver events to ipconf.
	bool add_adapter(clconf_adapter_t *);
	bool remove_adapter(clconf_adapter_t *);
	bool change_adapter(clconf_adapter_t *);
	bool add_path(clconf_path_t *);
	bool remove_path(clconf_path_t *);
	bool node_alive(nodeid_t, incarnation_num);
	bool node_died(nodeid_t, incarnation_num);
	bool path_up(clconf_path_t *);
	bool path_down(clconf_path_t *);
	bool disconnect_node(nodeid_t, incarnation_num);
	void shutdown();

	static ipconf &the();
	static bool is_initialized();

	// Called from ORB::initialize. Create the ipconf object and
	// register it with the path_manager.
	static int initialize();

	ipconf();

	// Called from ipconf_task::execute. Do the real work (ifkconfig
	// interfaces etc) based on the events delivered to us from the
	// path manager.
	void execute();

	// Map the specified logical ip to the physical ip of the adapter
	// that's hosting the logical ip. If we can't find logical_ip_address
	// in our mapping then we return logical_ip_address.
	// We only store the mapping for the logical IPs on remote_nodes
	// that we use for communication.
	in_addr_t get_physical_ip(in_addr_t logical_ip);

	//
	// Called by fpconf to let ipconf know that clprivnet mtu has
	// changed and ipconf should replumb clprivnet.
	//
	void clprivnet_mtu_changed();

private:
	// Initialization that happens in the object after it has been
	// created.  Called from initialize.
	int initialize_int();

	int plumb_clprivnet();
	int unplumb_clprivnet();
	int update_clprivnet_mtu();

	struct ipconf_adapter {
		char *dname;
		int dinst;
		int dvlanid;
	};
	typedef ipconf_adapter ipconf_adapter_t;

	// Helper function. ifkconfig the specified adapter with the cmd.
	// The configured interface is for talking to the specified node.
	void pairwise_config(nodeid_t, ipconf_adapter_t *ada, int cmd);

	// Helper function.  Perform cmd for pernode route to node nodeid_t
	// with flags.
	void pernode_config(nodeid_t, uchar_t cmd, int flags);

	// Helper functions. Pernode_disconnect and pairwise_disconnect
	// abort all TCP connections to the logical pernode and pairwise
	// addresses respectively hosted at the specified node. Both methods
	// call perip_disconnect with the right IP address to get the job done.
	void pernode_disconnect(nodeid_t);
	void pairwise_disconnect(nodeid_t);
	void perip_disconnect(struct in_addr *);
#if SOL_VERSION >= __s10
	void privip_disconnect(nodeid_t);
#endif

	// Helper function. Use the specified path for communicating with
	// the specified remote node. Configure the IP on that path.
	void use_path(nodeid_t ndid, clconf_path_t *newpath);

	// Helper function. Find the ipconf_path_elem for the specified path.
	ipconf_path_elem *find(clconf_path_t *);

	// Helper function. Calculates the pairwise address for the two
	// specified nodes
	static void get_pairwise_ipaddr(nodeid_t, nodeid_t,
	    struct in_addr *addr);

	// Helper function. Set the logical ip to physical ip mapping using
	// the specified path to the specified remote node. If the path
	// pointer is NULL we will just remove the old mapping.
	void set_ip_mapping(nodeid_t, clconf_path_t *);

	// Put the task for processing node ndid into the threadpool.
	void defer_job(nodeid_t);

	// The lock is used to protect all the private data sturcture but
	// not current_path. The current_path[] is only accessed by
	// the execute() function, which is run from the common_threadpool.
	// There could only be one execute() running at one time. This is
	// ensured by using the in_threadpool flag. As a result we don't
	// need any lock to protect current_path.
	// This lock can be acquired from interrupt context.
	void lock();
	void unlock();
	os::mutex_t lck;

	// We need path_lock to protect current_path array.
	// Use this to protect access and modifications to current_path[]
	// Lock Order is path_lock followed by lck.
	os::mutex_t path_lock;

	// Pick a path from all paths between the two nodes. We use a hashing
	// algorithm to achieve fairness among the adapters within a node.
	clconf_path_t *get_path(nodeid_t);

	// Return pointer to the first active path found for the remote node
	clconf_path_t *active_path_check(nodeid_t);

	// Path list.
	ipconf_pathlist_t paths;

	// The path we are using for a remote node.
	clconf_path_t *current_path[NODEID_MAX+1];

	// The adapter we are using for a remote node.
	ipconf_adapter_t *current_adapter[NODEID_MAX+1];

	// Since path_up/path_down is delivered in clock interrupt, we need to
	// delay processing of it to another thread. We coordinate
	// the processing using the following flag. When we need to do
	// some processing (choose a new path and failover IP if necessary),
	// we set the flag to be true and put this into the common
	// threadpool if it's not already in there. When we process the job
	// we set the flag back to false.
	bool processing_needed[NODEID_MAX+1];

	// Whether we are currently in common threadpool or not.
	bool in_threadpool;

	// Pointer to the TCP socket used for closing TCP connections to
	// disconnected nodes quickly.
	struct sonode *tcp_so;

	// A pointer to the routing socket vnode, opened in the constructor
	vnode *rs_vnode;

	// lock for synchronizing use of the routing socket msg structure
	// Lock Order is lock followed by rs_msg_lock.
	os::mutex_t rs_msg_lock;

	// A routing message header to pass to the routing socket
	struct {
		rt_msghdr_t hdr;
		char adrs[128]; // This leaves plenty of space for addresses
	} rs_msg;

	// The location, in rs_msg, of the destination, gateway, and netmask
	// in_addr_ts in the rs_msg respectively.
	char *addr_addrs[3];

	// To keep track of route change sequence number
	int rs_seq;

	// The task we put into common threadpool to process path_down event.
	ipconf_task the_task;

	// The pointer to the global instance of this class.
	static ipconf *the_ipconf;

	// iphash is for supporting mapping a logical ip to
	// the physical ip hosting it. This is used by clconf_get_physical_ip.
	hashtable_t<in_addr_t, in_addr_t> iphash;

	//
	// configured nodes which are not running will have an array entry
	// of true.
	bool node_reject_route[NODEID_MAX +1];

	void update_reject_route(nodeid_t, uchar_t);

	// Return true if the remote node is running pairwise subnet protocol.
	bool ipconf::is_pairwise_subnet_version(nodeid_t);

	// version manager reference
	version_manager::vm_admin_ptr	vm_adm;
};


#include <orb/ip/ipconf_in.h>

#endif	/* _IPCONF_H */
