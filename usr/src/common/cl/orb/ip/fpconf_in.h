/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FPCONF_IN_H
#define	_FPCONF_IN_H

#pragma ident	"@(#)fpconf_in.h	1.14	08/05/20 SMI"

#include <orb/ip/fpconf.h>
#include <cl_net/netlib.h>

inline bool
fpconf::node_alive(nodeid_t, incarnation_num)
{
	return (false);
}

inline bool
fpconf::node_died(nodeid_t, incarnation_num)
{
	return (false);
}

inline _SList::ListElem *
fpconf::get_pmlist_elem()
{
	return (this);
}

inline void
fpconf::shutdown()
{
	os::prom_printf("Shutdown called. do appropriate error chekcing\n");
}

inline int
fpconf::ethertype_to_index(ushort_t etype)
{
	for (int i = 0; i < ETHERTYPE_COUNT; i++) {
		if (ethertypes[i] == etype)
			return (i);
	}
	return (-1);
}

inline void
fp_holder::path_up()
{
	fp_holder_lock.lock();
	ASSERT(is_up == false);
	is_up = true;
	fp_holder_lock.unlock();
}

inline bool
fp_holder::is_path_up()
{
	return (is_up);
}

inline clconf_path_t *
fp_holder::get_path()
{
	return (path);
}

inline void
fp_holder::incr_ref()
{
	fp_holder_lock.lock();
	ref_count++;
	fp_holder_lock.unlock();
}

inline void
fp_holder::decr_ref()
{
	fp_holder_lock.lock();
	ASSERT(ref_count > 0);
	ref_count--;
	if (ref_count == 0 && is_waiting) {
		//
		// Remove path thread has been waiting for the ref count to
		// drop to zero. Send a signal.
		//
		fp_holder_cv.broadcast();
	}
	fp_holder_lock.unlock();
}

inline bool
fp_holder::same_device(network::macinfo_t &macinfo)
{
	fp_holder_lock.lock();
	if ((macinfo.instance == remote_device_instance) &&
	    (!os::strcmp((const char *)macinfo.device_name,
	    remote_device_name))) {
		fp_holder_lock.unlock();
		return (true);
	} else {
		fp_holder_lock.unlock();
		return (false);
	}
}

inline bool
fp_adapter::adap_compare(const char *device, int inst)
{
	if ((local_device_instance == inst) &&
	    (!os::strcmp(local_device, device))) {
		return (true);
	} else {
		return (false);
	}
}

inline const char *
fp_adapter::get_device()
{
	return (local_device);
}

inline int
fp_adapter::get_device_instance()
{
	return (local_device_instance);
}

inline void
#ifdef DEBUG
cl_net_ns_if_impl_t::_unreferenced(unref_t cookie)
#else
cl_net_ns_if_impl_t::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}


//
// Fragment handling support for mcnet and clprivnet
//


#ifdef _KERNEL	// no fragment handling done for unode

extern uint32_t frag_hash_size;

//
// Given an offset and the extent of a fragment, this function decides if
// it "extends" the longest data portion of the enclosing datagram from
// offset 0. This function is used to determine if fragments of the same
// datagram are arriving in order, each one extending the longest portion
// of data from offset 0.
//
inline bool
fragment::extend(uint16_t off, uint16_t ext)
{
	if (off <= frag_offset && (off + ext) > frag_offset) {
		//
		// This fragment "adds" to the longest portion. Always
		// remember the next offset we expect.
		//
		frag_offset = (off + ext);
		return (true);
	}
	return (false);
}

inline bool
fragment::valid_node()
{
	// See if you have to get the fragment lock here.
	// Atleast make sure that there is an assertion
	// that lock is held.

	ASSERT(fragment_mutex.lock_held());
	if (nodeid != NODEID_UNKNOWN)
		return (true);
	else
		return (false);
}

inline uint_t
fragment::get_nodeid(network::cid_t& cid)
{
	ASSERT(fragment_mutex.lock_held());
	bcopy(&dstaddr, &cid.ci_laddr, sizeof (dstaddr));
	bcopy(&srcaddr, &cid.ci_faddr, sizeof (srcaddr));
	cid.ci_fport = srcport;
	cid.ci_lport = dstport;
	cid.ci_protocol = proto;
	return (nodeid);
}

inline bool
fragment::compare(uint32_t id,
    const in6_addr_t& src, const in6_addr_t& dst)
{
	if ((ident == id) &&
	    IN6_ARE_ADDR_EQUAL(&srcaddr, &src) &&
	    IN6_ARE_ADDR_EQUAL(&dstaddr, &dst)) {
		return (true);
	} else {
		return (false);
	}
}

inline void
fragment::update_info(uint8_t pr, uint16_t sport, uint16_t dport)
{
	proto = pr;
	srcport = sport;
	dstport = dport;
}

inline fragment_list_t&
fragment_manager::get_fragment_list(const in6_addr_t& src, uint32_t id)
{
	ulong_t index = ((ntohl(V4_PART_OF_V6(src)) ^ ((id) ^
	    ((id) >> 8))) % frag_hash_size);
	return (fragment_list_array[index]);
}

#endif	// _KERNEL

#endif	/* _FPCONF_IN_H */
