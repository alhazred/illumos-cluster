/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _VLAN_H
#define	_VLAN_H

#pragma ident	"@(#)vlan.h	1.3	08/05/20 SMI"

#include <sys/types.h>

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * IEEE 802.1d has defined 8 user priority values 0-7.
 */
#define	MAX_8021D_UPRI	7
#define	NUM_8021D_UPRI	(MAX_8021D_UPRI + 1)

/*
 * IEEE 802.1d user priority values to be used with various categories
 * of private interconnect traffic. The global variables are defined in
 * vlan.c. Legal values are 0-7 but there is no way to enforce that they
 * do not get set to values >7. As a result, these parameters should not
 * be used directly. Use the four macros defined below that treat values
 * >7 as 7.
 */
extern uint_t sc_hb_usr_pri;
extern uint_t sc_orb_usr_pri;
extern uint_t sc_app_usr_pri;
extern uint_t sc_scal_usr_pri;

#define	SC_HB_USR_PRI	\
	(sc_hb_usr_pri < MAX_8021D_UPRI) ? sc_hb_usr_pri : MAX_8021D_UPRI
#define	SC_ORB_USR_PRI	\
	(sc_orb_usr_pri < MAX_8021D_UPRI) ? sc_orb_usr_pri : MAX_8021D_UPRI
#define	SC_APP_USR_PRI	\
	(sc_app_usr_pri < MAX_8021D_UPRI) ? sc_app_usr_pri : MAX_8021D_UPRI
#define	SC_SCAL_USR_PRI	\
	(sc_scal_usr_pri < MAX_8021D_UPRI) ? sc_scal_usr_pri : MAX_8021D_UPRI

/*
 * Two arrays to map user priorities values into mblk b_band values
 * and vice versa.
 */
extern uchar_t sc_upri_to_bband[];
extern uint_t sc_bband_to_upri[];

/*
 * Function to map a DLPI priority value to mblk b_band value. This is
 * used by clprivnet.
 */
extern char sc_dlpri_to_bband_f(uint_t);

/*
 * Macros to convert user priority <--> b_band. It is the caller's
 * responsibility to ensure that the passed values are 0-7 for user
 * priority and 0-255 for b_band.
 */
#define	UPRI_TO_BBAND(p)	(sc_upri_to_bband[p])
#define	BBAND_TO_UPRI(b)	(sc_bband_to_upri[b])

/*
 * The mblk b_band value to be used for various types of interconnect
 * traffic based on the current set of (configurable) user priority
 * values.
 */
#define	SC_HB_B_BAND	UPRI_TO_BBAND(SC_HB_USR_PRI)
#define	SC_ORB_B_BAND	UPRI_TO_BBAND(SC_ORB_USR_PRI)
#define	SC_APP_B_BAND	UPRI_TO_BBAND(SC_APP_USR_PRI)
#define	SC_SCAL_B_BAND	UPRI_TO_BBAND(SC_SCAL_USR_PRI)

#ifdef  __cplusplus
}
#endif

#endif	/* _VLAN_H */
