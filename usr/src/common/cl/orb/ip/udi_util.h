/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UDI_UTIL_H
#define	_UDI_UTIL_H

#pragma ident	"@(#)udi_util.h	1.4	09/02/18 SMI"

/*
 * Header file for udi - the unified driver interface
 *
 * Solaris 10 introduces Layered Driver Framework (LDI) and the various
 * ways Sun Cluster use to access device driver should be replaced with
 * LDI over time. UDI is an abstraction layer that unifies the way raw
 * dlpi transport, fast path for mcnet, and ifkconfig access device
 * driver.
 *
 * For OS versions less than S10, the original ways of device access are
 * preserved. This include kstr_* calls, VOP_OPEN(), and socreate(). They
 * all however are "hidden" under UDI.
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _KERNEL

/*
 * Unode build. There is no unode support for the various low-level
 * interactions with device drivers in general. Thus, for udi, we
 * only seek to enable compilation. No code actually uses udi.
 * Rather than requiring all objects to comment out any udi data
 * member fields, we'll define an arbitrary udi type for them.
 */
typedef int udi_handle_t;

#else

#include	<sys/param.h>
#include	<sys/types.h>
#include	<sys/user.h>
#include	<sys/vfs.h>
#include	<sys/vnode.h>
#include	<sys/file.h>
#include	<sys/stream.h>
#include	<sys/stropts.h>
#include	<sys/strsubr.h>
#include	<sys/dlpi.h>
#include	<sys/vnode.h>
#include	<sys/socket.h>
#include	<sys/sockio.h>
#include	<net/if.h>
#include	<sys/cred.h>
#include	<sys/sysmacros.h>
#include	<sys/kstr.h>
#include	<sys/errno.h>
#include	<sys/sunddi.h>
#include	<netinet/ip6.h>
#include	<netinet/in.h>
#include	<inet/common.h>
#include	<inet/ip.h>
#include	<inet/tcp.h>
#include	<sys/socketvar.h>
#include	<sys/sol_version.h>


#if SOL_VERSION < __s10
/* No Layered Driver Framework support */
#else

#include	<sys/sunldi.h>
#include	<sys/sunldi_impl.h>

/* Enable all LDI related code */
#define	USE_LDI	1

#endif	/* SOL_VERSION */

struct udi_handle_s {
#ifdef USE_LDI
	ldi_handle_t	uh_lh;
	ldi_ident_t	uh_li;
#define	UDI2VP(uh) (((struct ldi_handle *)(uh)->uh_lh)->lh_vp)
#else
	/*
	 * We need all 3 fields to support the different ways device
	 * drivers are accessed before LDI. They are all grouped here so
	 * that a uniform handle can be used in all cases.
	 */
	struct sonode	*uh_so;		/* udp6 socket access, used by */
					/* IPv6 plumbing in ifkconfig */

	int		uh_fd;		/* Used to call kstr_close() */

	vnode_t		*uh_vp;		/* Holds vnode ptr from kstr_open() */
#define	UDI2VP(uh) ((uh)->uh_vp)
#endif
};

typedef struct udi_handle_s *udi_handle_t;


extern int udi_open(const char *device_name, int instance, udi_handle_t *);
extern int udi_close(udi_handle_t);
extern int udi_ioctl(udi_handle_t uh, int cmd, intptr_t arg);
extern int udi_push(udi_handle_t, const char *mod);
extern int udi_plink(udi_handle_t, udi_handle_t, int *);
extern int udi_punlink(udi_handle_t, int);
extern int udi_msg(udi_handle_t, mblk_t *smp, mblk_t **rmp,
	    timestruc_t *timeo);

extern int udi_open_inet6_sock(udi_handle_t *);

extern int udi_open_vop(const char *device_name, int inst, udi_handle_t *);


extern int getifmuxid(udi_handle_t, struct lifreq *lifq);

extern int getifflags(udi_handle_t, struct lifreq *lifq);

extern int getifnetmask(udi_handle_t, struct lifreq *lifq);

extern int setipaddr(udi_handle_t, const char *ifname,
	    const struct in_addr *addr);

extern int setifmuxid(udi_handle_t, struct lifreq *lifq);

extern int setifmtu(udi_handle_t, const char *ifname, int mtu);

extern int setifname(udi_handle_t, struct lifreq *lifq);

extern int setnetmask(udi_handle_t, const char *ifname,
	    const struct in_addr *addr);

extern int setbroadcast(udi_handle_t, const char *ifname,
	    const struct in_addr *addr);

extern int plumb_logical_if(udi_handle_t, const char *ifname);

extern int unplumb_logical_if(udi_handle_t, const char *ifname);


#endif	/* _KERNEL */



#ifdef __cplusplus
}
#endif

#endif	/* _UDI_UTIL_H */
