/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FAST_PATH_H
#define	_FAST_PATH_H

#pragma ident	"@(#)fast_path.h	1.7	08/05/20 SMI"

#include <clprivnet/clprivnet_export.h>

#define	CLFPSTR_MODULE_NAME "clfpstr"

/* Private SAP for fast_path */
#define	CL_FP_SAP  ETHERTYPE_CLPRIVNET

/*
 * Used by fpconf to send informantion about the fp_adap
 * object to the clfpstr module.
 */
typedef struct {
	char	*adp;
} clfpstr_info_t;

#endif	/* _FAST_PATH_H */
