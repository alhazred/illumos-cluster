/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)udi_util.c	1.6	09/02/18 SMI"

#include <sys/kmem.h>
#include <orb/ip/udi_util.h>

#define	DEV_PREFIX	"/dev/"
#define	DEV_PREFIX_VN	"/dev/net/"
#define	UDP6		"udp6"
#define	CLONE		"clone"

/* Read-only flag to indicate if LDI is used instead of kstr calls */
#ifdef USE_LDI
const ushort_t ifk_ldi_used = 1;
#else
const ushort_t ifk_ldi_used = 0;
#endif

#ifndef USE_LDI
/*
 * There does not seem to be an soclose method in solaris
 * instead the following VOP/VN operations are needed to properly close
 * a socket.
 * XX This should really be in base solaris
 */
static void
soclose(struct sonode *so)
{
#if	SOL_VERSION >= __s11
	(void) VOP_CLOSE(SOTOV(so), FREAD|FWRITE, 1, (offset_t)0, CRED(), NULL);
#else
	(void) VOP_CLOSE(SOTOV(so), FREAD|FWRITE, 1, (offset_t)0, CRED());
#endif
	VN_RELE(SOTOV(so));
}
#endif

/*
 * Replacement for directly opening udp6 sockets when plumbing
 * IPv6. Setting flags on V6 instances of interfaces can only be done
 * through sockets before LDI.
 */
int
udi_open_inet6_sock(udi_handle_t *uhp)
{
#ifdef USE_LDI
	return (udi_open(UDP6, -1, uhp));
#else
	udi_handle_t	uh;

	vnode_t *vp;
	vnode_t *avp;
	struct sonode *so;
	int error;

	/*
	 * XXX kstr_ioctl() has a problem with the vnode of /dev/udp6. We
	 * need to get a different vnode through the socket calls for it
	 * to work.
	 */
	avp = solookup(AF_INET6, SOCK_DGRAM, 0, NULL, &error);
	if (avp == NULL) {
		return (error);
	}
	so = socreate(avp, AF_INET6, SOCK_DGRAM, 0, SOV_SOCKSTREAM, NULL,
	    &error);
	if (so == NULL) {
		return (error);
	}
	vp = SOTOV(so);

	uh = kmem_alloc(sizeof (struct udi_handle_s), KM_SLEEP);
	uh->uh_so = so;
	uh->uh_fd = -1;
	uh->uh_vp = vp;

	*uhp = uh;
	return (0);
#endif
}

/*
 * Replacement for VOP_OPEN() calls used in raw_dlpi and fast_path
 * If instance number is not -1, style-1 open is used.
 * This is added to support device nodes that are only available
 * under /dev/net.
 */
int
udi_open_vop(const char *dev, int inst, udi_handle_t *uhp)
{
#ifdef USE_LDI
	return (udi_open(dev, inst, uhp));
#else
	udi_handle_t	uh;
	int		error;

	uh = kmem_alloc(sizeof (struct udi_handle_s), KM_SLEEP);
	uh->uh_so = NULL;
	uh->uh_fd = -1;

	/*
	 * Calling kstr_open() with a NULL argument as the fd ptr will
	 * call VOP_OPEN without associating the resulting vnode with an fd
	 */
	if ((error = kstr_open(ddi_name_to_major(CLONE),
	    ddi_name_to_major((char *)dev), &uh->uh_vp, NULL)) != 0) {
		kmem_free(uh, sizeof (struct udi_handle_s));
		*uhp = NULL;
		return (error);
	}

	*uhp = uh;
	return (0);
#endif
}

/*
 * Open the named device. If instance is not -1, it is appened to the
 * named device before the open.
 */
int
udi_open(const char *dev, int inst, udi_handle_t *uhp)
{
	udi_handle_t	uh;
	int		error;

#ifdef USE_LDI
	char devpath[64];

	uh = kmem_zalloc(sizeof (struct udi_handle_s), KM_SLEEP);

	(void) ldi_ident_from_dev(makedevice(
	    ddi_name_to_major(CLONE), ddi_name_to_major((char *)dev)),
	    &uh->uh_li);

#if SOL_VERSION >= __s11
	/*
	 * Devices are opened using nodes under /dev/net, which
	 * support only style-1, i.e. with instance numbers.
	 * The only time /dev/<device> is still used is for
	 * /dev/ip, /dev/udp, etc.
	 */
	if (inst == -1)
		(void) sprintf(devpath, "%s%s", DEV_PREFIX, dev);
	else
		(void) sprintf(devpath, "%s%s%d", DEV_PREFIX_VN, dev, inst);
#else
	if (inst == -1)
		(void) sprintf(devpath, "%s%s", DEV_PREFIX, dev);
	else
		(void) sprintf(devpath, "%s%s%d", DEV_PREFIX, dev, inst);
#endif
	ASSERT(strlen(devpath) < sizeof (devpath));

	if ((error = ldi_open_by_name(devpath, FREAD|FWRITE, CRED(),
	    &uh->uh_lh, uh->uh_li)) != 0) {
		ldi_ident_release(uh->uh_li);
		kmem_free(uh, sizeof (struct udi_handle_s));
		*uhp = NULL;
		return (error);
	}
#else
	uh = kmem_alloc(sizeof (struct udi_handle_s), KM_SLEEP);
	uh->uh_so = NULL;

	if ((error = kstr_open(ddi_name_to_major(CLONE),
	    ddi_name_to_major((char *)dev), &uh->uh_vp, &uh->uh_fd)) != 0) {
		kmem_free(uh, sizeof (struct udi_handle_s));
		*uhp = NULL;
		return (error);
	}
#endif
	*uhp = uh;
	return (0);
}

int
udi_close(udi_handle_t uh)
{
#ifdef USE_LDI
	ldi_handle_t lh;

	ASSERT(uh != NULL);

	lh = uh->uh_lh;
	ldi_ident_release(uh->uh_li);
	kmem_free(uh, sizeof (struct udi_handle_s));

	return (ldi_close(lh, FREAD|FWRITE, CRED()));
#else
	struct udi_handle_s tmp_uh;

	ASSERT(uh != NULL);

	tmp_uh = *uh;
	kmem_free(uh, sizeof (struct udi_handle_s));

	if (tmp_uh.uh_so != NULL) {
		soclose(tmp_uh.uh_so);
		return (0);
	}

	if (tmp_uh.uh_fd != -1)
		return (kstr_close(NULL, tmp_uh.uh_fd));
	else
		return (kstr_close(tmp_uh.uh_vp, -1));
#endif
}

int
udi_ioctl(udi_handle_t uh, int cmd, intptr_t arg)
{
#ifdef USE_LDI
	int rval;

	ASSERT(uh != NULL);
	return (ldi_ioctl(uh->uh_lh, cmd, arg, FKIOCTL, CRED(), &rval));
#else
	ASSERT(uh != NULL);
	return (kstr_ioctl(uh->uh_vp, cmd, arg));
#endif
}

int
udi_push(udi_handle_t uh, const char *mod)
{
#ifdef USE_LDI
	int rval;

	ASSERT(uh != NULL);
	return (ldi_ioctl(uh->uh_lh, I_PUSH, (intptr_t)mod,
	    FKIOCTL, CRED(), &rval));

#else
	ASSERT(uh != NULL);
	return (kstr_push(uh->uh_vp, (char *)mod));
#endif
}

int
udi_plink(udi_handle_t uh1, udi_handle_t uh2, int *muxid)
{
#ifdef USE_LDI
	ASSERT(uh1 != NULL && uh2 != NULL);
	return (ldi_ioctl(uh1->uh_lh, I_PLINK, (intptr_t)(uh2->uh_lh),
	    FREAD|FWRITE|FNOCTTY|FKIOCTL, CRED(), muxid));

#else
	ASSERT(uh1 != NULL && uh2 != NULL);
	return (kstr_plink(uh1->uh_vp, uh2->uh_fd, muxid));
#endif
}

int
udi_punlink(udi_handle_t uh, int muxid)
{
#ifdef USE_LDI
	int rval;

	ASSERT(uh != NULL);
	return (ldi_ioctl(uh->uh_lh, I_PUNLINK, (intptr_t)muxid,
	    FKIOCTL, CRED(), &rval));

#else
	ASSERT(uh != NULL);
	return (kstr_unplink(uh->uh_vp, muxid));
#endif
}

int
udi_msg(udi_handle_t uh, mblk_t *smp, mblk_t **rmp, timestruc_t *timeo)
{
#ifdef USE_LDI
	int error;

	ASSERT(uh != NULL);

	if (rmp == NULL && timeo != NULL &&
	    (timeo->tv_sec != 0 || timeo->tv_nsec != 0))
		return (EINVAL);

	if (smp == NULL && rmp == NULL)
		return (EINVAL);

	if (smp != NULL) {
		if ((error = ldi_putmsg(uh->uh_lh, smp)) != 0) {
			return (error);
		}
	}

	if (rmp == NULL)
		return (0);

	return (ldi_getmsg(uh->uh_lh, rmp, timeo));

#else
	ASSERT(uh != NULL);
	return (kstr_msg(uh->uh_vp, smp, rmp, timeo));
#endif
}

int
getifmuxid(udi_handle_t uh, struct lifreq *lifq)
{
	struct strioctl iocb;

	iocb.ic_cmd = SIOCGLIFMUXID;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (char *)lifq;

	return (udi_ioctl(uh, I_STR, (intptr_t)&iocb));
}

int
getifflags(udi_handle_t uh, struct lifreq *lifq)
{
	struct strioctl iocb;

	iocb.ic_cmd = SIOCGLIFFLAGS;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (caddr_t)lifq;

	return (udi_ioctl(uh, I_STR, (intptr_t)&iocb));
}

int
getifnetmask(udi_handle_t uh, struct lifreq *lifq)
{
	struct strioctl iocb;

	iocb.ic_cmd = SIOCGLIFNETMASK;  /*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (caddr_t)lifq;

	return (udi_ioctl(uh, I_STR, (intptr_t)&iocb));
}

int
setipaddr(udi_handle_t uh, const char *ifname, const struct in_addr *addr)
{
	struct lifreq *lifq;
	struct strioctl	iocb;
	int rc;

	lifq = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
	lifq->lifr_addr.ss_family = AF_INET;

	((struct sockaddr_in *)&lifq->lifr_addr)->sin_addr.s_addr =
	    addr->s_addr;
	(void) strncpy((caddr_t)lifq->lifr_name, ifname,
	    sizeof (lifq->lifr_name)-1);
	lifq->lifr_name[sizeof (lifq->lifr_name) - 1] = '\0';

	iocb.ic_cmd = SIOCSLIFADDR;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (char *)lifq;

	rc = udi_ioctl(uh, I_STR, (intptr_t)&iocb);
	kmem_free(lifq, sizeof (struct lifreq));
	return (rc);
}

int
setifmuxid(udi_handle_t uh, struct lifreq *lifq)
{
	struct strioctl	iocb;

	iocb.ic_cmd = SIOCSLIFMUXID;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (char *)lifq;

	return (udi_ioctl(uh, I_STR, (intptr_t)&iocb));
}

int
setifmtu(udi_handle_t uh, const char *ifname, int mtu)
{
	struct strioctl iocb;
	struct lifreq *lifr;
	int rc;

	lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
	iocb.ic_cmd = SIOCSLIFMTU;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifr);
	iocb.ic_dp = (caddr_t)lifr;

	(void) strncpy((caddr_t)lifr->lifr_name, ifname,
	    sizeof (lifr->lifr_name)-1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';
	lifr->lifr_metric = mtu;

	rc =  udi_ioctl(uh, I_STR, (intptr_t)&iocb);

	kmem_free(lifr, sizeof (struct lifreq));
	return (rc);
}

int
setifname(udi_handle_t uh, struct lifreq *lifq)
{
	struct strioctl	iocb;

	iocb.ic_cmd = SIOCSLIFNAME;
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (char *)lifq;

	return (udi_ioctl(uh, I_STR, (intptr_t)&iocb));
}

int
setnetmask(udi_handle_t uh, const char *ifname, const struct in_addr *addr)
{
	struct lifreq *lifq;
	struct strioctl	iocb;
	int rc;

	lifq = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
	lifq->lifr_addr.ss_family = AF_INET;

	((struct sockaddr_in *)&lifq->lifr_addr)->sin_addr.s_addr =
	    addr->s_addr;

	(void) strncpy((caddr_t)lifq->lifr_name, ifname,
	    sizeof (lifq->lifr_name)-1);
	lifq->lifr_name[sizeof (lifq->lifr_name) - 1] = '\0';

	iocb.ic_cmd = SIOCSLIFNETMASK;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (char *)lifq;

	rc =  udi_ioctl(uh, I_STR, (intptr_t)&iocb);

	kmem_free(lifq, sizeof (struct lifreq));
	return (rc);
}

int
setbroadcast(udi_handle_t uh, const char *ifname, const struct in_addr *addr)
{
	struct lifreq *lifq;
	struct strioctl	iocb;
	int rc;

	lifq = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
	lifq->lifr_addr.ss_family = AF_INET;

	((struct sockaddr_in *)&lifq->lifr_addr)->sin_addr.s_addr =
	    addr->s_addr;

	(void) strncpy((caddr_t)lifq->lifr_name, ifname,
	    sizeof (lifq->lifr_name)-1);
	lifq->lifr_name[sizeof (lifq->lifr_name) - 1] = '\0';

	iocb.ic_cmd = SIOCSLIFBRDADDR;	/*lint !e737 */
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifq);
	iocb.ic_dp = (char *)lifq;

	rc = udi_ioctl(uh, I_STR, (intptr_t)&iocb);

	kmem_free(lifq, sizeof (struct lifreq));
	return (rc);
}

int
plumb_logical_if(udi_handle_t uh, const char *ifname)
{
	struct lifreq *lifr;
	struct strioctl	iocb;
	int rc;

	lifr = kmem_zalloc(sizeof (struct lifreq), KM_SLEEP);
	(void) strncpy(lifr->lifr_name, ifname, sizeof (lifr->lifr_name) - 1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';

	iocb.ic_cmd = SIOCLIFADDIF;
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifr);
	iocb.ic_dp = (char *)lifr;

	rc =  udi_ioctl(uh, I_STR, (intptr_t)&iocb);

	kmem_free(lifr, sizeof (struct lifreq));
	return (rc);
}

int
unplumb_logical_if(udi_handle_t uh, const char *ifname)
{
	struct lifreq *lifr;
	struct strioctl	iocb;
	int rc;

	lifr = kmem_zalloc(sizeof (struct lifreq), KM_SLEEP);
	(void) strncpy(lifr->lifr_name, ifname, sizeof (lifr->lifr_name) - 1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';
	/*lint -e 737 */

	iocb.ic_cmd = SIOCLIFREMOVEIF;
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifr);
	iocb.ic_dp = (char *)lifr;

	rc =  udi_ioctl(uh, I_STR, (intptr_t)&iocb);

	kmem_free(lifr, sizeof (struct lifreq));
	return (rc);
}
