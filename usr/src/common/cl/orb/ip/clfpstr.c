/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clfpstr.c	1.7	08/05/20 SMI"

/*
 * This module provides support for cluster resident applications using
 * TCP/IP for internode communication.  A pseudo device driver (clprivnet)
 * provides for redirecting application traffic over all of the cluster
 * private physical network interfaces in an equally distributed way.
 * This module is pushed on the redirection Stream (private SAP of 0x832)
 * of each physical interface.  Outgoing packets routed from the clprivnet
 * driver pass directly to the physical interface.  Incoming ARP packets
 * are routed by this modules to the clprivnet driver.
 */


#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>

#include <sys/errno.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/dlpi.h>
#include <sys/strsun.h>

#include <sys/conf.h>
#include <sys/modctl.h>
#include <sys/cl_net.h>
#include <orb/ip/fast_path.h>

char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm drv/clprivnet";



extern void clprivnet_pkt_recv(mblk_t *);
extern void cl_net_fpath_ioctl_ack(char *, mblk_t *);
#if (defined(DL_NOTE_LINK_UP) && defined(DL_NOTE_LINK_DOWN))
extern void cl_net_fpath_link_status_update(char *, int);
#endif
static int clfpstr_ropen(queue_t *, dev_t *, int, int, cred_t *);
static int clfpstr_rclose(queue_t *, int, cred_t *);
static int clfpstr_rput(queue_t *, mblk_t *);
static int clfpstr_wput(queue_t *, mblk_t *);


static struct module_info clfpstr_info = {
	0x1235,			/* module id - random for now	*/
	CLFPSTR_MODULE_NAME,	/* module name			*/
	0,			/* min packet size		*/
	INFPSZ,			/* max packet size		*/
	512*1024,		/* hi-water mark		*/
	16*1024			/* lo-water mark		*/
};

static struct qinit clfpstr_rint = {
	(int (*)(void))clfpstr_rput,	/* put procedure		*/
	NULL,				/* service procedure		*/
	(int (*)(void))clfpstr_ropen,	/* open procedure		*/
	(int (*)(void))clfpstr_rclose,	/* close procedure		*/
	NULL,				/* qadmin procedure		*/
	&clfpstr_info,			/* module info structure	*/
	NULL,				/* module statistics structure	*/
	NULL,				/* r/w proc.			*/
	NULL,				/* info. procedure.		*/
	0				/* uio type for struio().	*/
};

static struct qinit clfpstr_wint = {
	(int (*)(void))clfpstr_wput,	/* put procedure		*/
	NULL,				/* service procedure		*/
	NULL,				/* open procedure		*/
	NULL,				/* close procedure		*/
	NULL,				/* qadmin procedure		*/
	&clfpstr_info,			/* module info structure	*/
	NULL,				/* module statistics structure	*/
	NULL,				/* r/w proc.			*/
	NULL,				/* info. procedure.		*/
	0				/* uio type for struio().	*/
};

static struct streamtab clfpstr_stream = {
	&clfpstr_rint,	/* read queue init structure	*/
	&clfpstr_wint,	/* write queue init structure	*/
	NULL,		/* mux read init structure	*/
	NULL		/* mux write init structure	*/
};

static struct fmodsw clfpstr_fsw = {
	CLFPSTR_MODULE_NAME,
	&clfpstr_stream,
	D_MP,
};

/*
 * Module linkage information for the kernel.
 */

static struct modlstrmod modl_clfpstr = {
	&mod_strmodops, /* Type of module.  This one is a Streams module */
	"fast-path recv module",
	&clfpstr_fsw	/* Module info */
};

static struct modlinkage modlinkage = {
	MODREV_1,
	{ (void *)&modl_clfpstr, NULL, NULL, NULL }
};

/*
 * Private data for each instance of the queue. Stores a pointer
 * to the fp_adap object that created this queue.
 */
typedef struct {
	char	*adp;
} clfpstr_data_t;

int
_init(void)
{
	int	error;

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

int
_fini(void)
{
	int error;


	error = mod_remove(&modlinkage);

	return (error);
}


/*
 * Read side open procedure
 */
static int
clfpstr_ropen(queue_t *rqp, dev_t *dev, int dummy, int sflag, cred_t *cred)
{
	dummy = dummy;
	cred = cred;
	dev = dev;

	if (rqp->q_ptr) {
		/*
		 * This instance has already been opened.  Nothing to do.
		 * Just return.
		 */
		return (0);
	}
	if (sflag != MODOPEN) {
		return (ENXIO);
	}

	rqp->q_ptr = kmem_zalloc(sizeof (clfpstr_data_t), KM_SLEEP);
	OTHERQ(rqp)->q_ptr = rqp->q_ptr;

	/* Turn our queue on ...  */
	qprocson(rqp);
	return (0);
}

/*
 * Read side close procedure
 */
static int
clfpstr_rclose(queue_t *rqp, int dummy, cred_t *cred)
{
	mblk_t *mp;

	dummy = dummy;
	cred = cred;

	/* Disable put and service procedures on this queue */
	qprocsoff(rqp);

	/* Free up any queued messages */
	while ((mp = getq((rqp))) != NULL) {
		freemsg(mp);
	}

	/* Free private data */
	if (rqp->q_ptr) {
		kmem_free(rqp->q_ptr, sizeof (clfpstr_data_t));
		rqp->q_ptr = NULL;
		OTHERQ(rqp)->q_ptr = NULL;
	}

	return (0);
}

/*
 * clfpstr_ioctl()
 *
 * Processes ioctl messages. Only the CLIOCSFPADP ioctl is processed.
 * The CLIOCSFPADP brings information about the FP Adapter object that
 * has created this stream.
 * Returns 1 if ioctl was processed, 0 otherwise.
 */
static int
clfpstr_ioctl(queue_t *wqp, mblk_t *mp)
{
	struct iocblk		*iocp;
	clfpstr_info_t		*infop;
	clfpstr_data_t		*datap;

	iocp = (struct iocblk *)mp->b_rptr;
	if (iocp->ioc_cmd == CLIOCSFPADP) {
		if (mp->b_cont == NULL ||
		    msgdsize(mp->b_cont) != sizeof (clfpstr_info_t)) {
			miocnak(wqp, mp, 0, EINVAL);
			return (1);
		}
		infop = (clfpstr_info_t *)mp->b_cont->b_rptr;
		if (infop == NULL) {
			miocnak(wqp, mp, 0, EINVAL);
			return (1);
		}
		datap = (clfpstr_data_t *)wqp->q_ptr;
		datap->adp = infop->adp;
		miocack(wqp, mp, 0, 0);
		return (1);
	} else {
		return (0);
	}
}

/*
 * The wput routine. If the message carries and ioctl, call clfpstr_ioctl
 * to process the message in case it is the CLIOCSFPADP ioctl. All other
 * messages are passed downstream.
 */
static int
clfpstr_wput(queue_t *wqp, mblk_t *mp)
{
	if (DB_TYPE(mp) == M_IOCTL) {
		if (clfpstr_ioctl(wqp, mp)) {
			return (0);
		}
	}
	putnext(wqp, mp);
	return (0);
}


/*
 * Processes ack/nack for the DL_IOC_HDR_INFO ioctl. Returns 1 if
 * the message was processed, 0 otherwise.
 */
static int
clfpstr_iocack(queue_t *rqp, mblk_t *mp)
{
	if (((struct iocblk *)mp->b_rptr)->ioc_cmd == DL_IOC_HDR_INFO) {
		cl_net_fpath_ioctl_ack(((clfpstr_data_t *)rqp->q_ptr)->adp,
		    mp);
		return (1);
	} else {
		return (0);
	}
}

/*
 * Let the fp_adapter object that created this stream know of change
 * in link status.
 */
#if (defined(DL_NOTE_LINK_UP) && defined(DL_NOTE_LINK_DOWN))
static void
clfpstr_notify(queue_t *rqp, mblk_t *mp)
{
	uint32_t note_type = ((dl_notify_ind_t *)mp->b_rptr)->dl_notification;
	if (note_type == DL_NOTE_LINK_UP) {
		cl_net_fpath_link_status_update(
		    ((clfpstr_data_t *)rqp->q_ptr)->adp, 1);
	} else if (note_type == DL_NOTE_LINK_DOWN) {
		cl_net_fpath_link_status_update(
		    ((clfpstr_data_t *)rqp->q_ptr)->adp, 0);
	} else {
		/* Upstream does not care about other notifications */
	}
	freemsg(mp);
}
#else /* Solaris 8 */
/* ARGSUSED */
void
clfpstr_notify(queue_t *rqp, mblk_t *mp)
{
	freemsg(mp);
}
#endif

/*
 * Receive side put procedure. M_DATA/M_PROTO(DL_UNITDATA_IND) messages are
 * transfered to the clprivnet driver. M_PROTO(DL_NOTIFY_IND) and
 * M_IOCACK/NACK(DL_IOC_HDR_INFO) go to fpconf. Rest are sent upstream
 * (shouldn't be any!).
 */
static int
clfpstr_rput(queue_t *rqp, mblk_t *mp)
{
	union DL_primitives *dl_prim;

	switch (DB_TYPE(mp)) {
		case M_DATA:
			clprivnet_pkt_recv(mp);
			break;
		case M_PROTO:
			dl_prim = (union DL_primitives *)mp->b_rptr;
			if (dl_prim->dl_primitive == DL_UNITDATA_IND) {
				clprivnet_pkt_recv(mp);
#ifdef DL_NOTIFY_IND
			} else if (dl_prim->dl_primitive == DL_NOTIFY_IND) {
				clfpstr_notify(rqp, mp);
#endif
			} else {
				/* Send it upstream */
				putnext(rqp, mp);
			}
			break;
		case M_IOCACK:
		case M_IOCNAK:
			if (clfpstr_iocack(rqp, mp)) {
				break;
			} else {
				/* send upstream */
				putnext(rqp, mp);
				break;
			}
		default:
			/* Other messages go upstream */
			putnext(rqp, mp);
			break;
	}

	return (0);

}
