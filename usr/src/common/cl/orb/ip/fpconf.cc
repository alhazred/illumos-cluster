//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fpconf.cc	1.53	08/09/08 SMI"

#undef nil // <inet/common.h> redefines nil

#include <orb/invo/common.h>
#include <sys/clconf_int.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/transport/path_manager.h>
#include <orb/infrastructure/orbthreads.h>
#include <orb/infrastructure/clusterproc.h>
#include <orb/transport/hb_threadpool.h>
#include <orb/ip/vlan.h>
#include <orb/ip/fpconf.h>
#include <nslib/ns.h>

#include <orb/ip/fast_path.h>
#include <cl_net/netlib.h>


extern "C" {
#include <sys/cmn_err.h>
#include <sys/ddi.h>
}

#include <orb/ip/ifkconfig.h>
#include <orb/ip/ipconf.h>
#include <sys/strsubr.h>
#include <sys/socketvar.h>
#include <orb/infrastructure/orb.h>

#include <inet/tcp.h>
#include <inet/mi.h>
#include <sys/ethernet.h>

#include <clprivnet/clprivnet_export.h>

extern fpconf *fpc;
fpconf *the_fpconf;

/*
 * List of known Ethertypes. Fpconf queries the (private interconnect)
 * drivers for DLPI fast path headers for these ether types and caches
 * the headers. Other Ethertypes (if any) take the DL_UNITDATA_REQ
 * route. Note that it is not really needed to list ARP and REVARP
 * Ethertypes here as those messages never get forwarded on the
 * private interconnect in the context of scalable services and in
 * the context of clprivnet, they get encapsulated in packets of
 * types CLPRIVNET. They are listed here just for the sake of
 * completeness. To keep searches over this array this efficient,
 * IP should be the first element and ARP and REVARP should be the
 * last two.
 */
ushort_t fpconf::ethertypes[] = {	ETHERTYPE_IP,
					ETHERTYPE_IPV6,
					ETHERTYPE_CLPRIVNET,
					ETHERTYPE_ARP,
					ETHERTYPE_REVARP};
#ifdef _KERNEL

class fragment_manager;
fragment_manager *fragment_managerp;

#define	UDP_MIN_HEADER_LENGTH 8
#define	IP_HDR_PROTOCOL_OFFSET 9
#define	IP_HDR_IDENT_OFFSET	4

#define	AH_SPI_OFFSET		4
#define	AH_BASELEN		12
#define	ESP_BASELEN		8


#define	CL_IPH_HDR_VERSION(ipha)                                \
	((int)(((ipha_t *)ipha)->ipha_version_and_hdr_length) >> 4)

time_t redirect_frag_timeout_period = 60;

// Number of fragment lists for hashed lookup of fragments
uint32_t frag_hash_size = 60;



//
// Randomizing variable for adapter selection when protocol is UDP
//
static uint16_t port_delta = 0;


//
// Fragment management code in pkt_redirector::extract_pkt_info is
// currently disabled. This currently appears to be the best performance
// option.
//
int	use_frag_holder = 0;


//
// packet redirection support for the clprivnet driver
// source code: ../common/cl/clprivnet/clprivnet.c
//
class pkt_redirector;
pkt_redirector *pkt_redirect_objp;

extern "C" {
static void pkt_redirect(nodeid_t, ushort_t, queue_t *, mblk_t *);
static void fragment_timeout(queue_t *, mblk_t *);
}

//
// NEED CONTRACTS
//
extern "C" void clprivnet_set_pkt_redirect(void (*)(uint_t, ushort_t,
    queue_t *, mblk_t *));
extern "C" void clprivnet_set_fragment_timeout(void (*)(queue_t *, mblk_t *));
extern "C" void clprivnet_set_local_nodeid(uint_t);
extern "C" void clprivnet_set_mtu(t_uscalar_t);
extern "C" void clprivnet_set_dlpri_to_bband(char (*)(uint_t));

#endif  /* _KERNEL */

#ifdef FP_DBGBUF
uint32_t fp_dbg_size = 10000;
dbg_print_buf	fp_dbg(fp_dbg_size, UNODE_DBGOUT_ALL, true);
#endif

extern queue_t *cl_net_fpath_open(udi_handle_t *,
    const char *, int, int, uchar_t *,
    size_t *, uchar_t *, size_t *, t_uscalar_t *, char *);
extern int cl_net_fpath_close(udi_handle_t);
int cl_net_fpath_ioctl_send(queue_t *, uchar_t *, size_t, ushort_t, uchar_t);

//
// This is the default amount of transport bandwidth which is consumed by
// scalable services in percent. So, if bandwidth of a transport adapter is
// 100Mbps, the default maximum bandwidth taken up scalable services is 80Mbps.
//

#define	DEFAULT_NW_BW	80


#define	FP_PATH_HASH_FUNC(cid, count)	((SUMOF(cid.ci_faddr) + cid.ci_fport) \
						% count)

#define	CLPRIVNET_HASH_FUNC(fp, lp, rndid, count)	\
	((fp + lp + rndid) % count)


//
// This list can be moved into the fpconf structure. It is here for
// two reasons.
// 1. Simplicity of implementation.
// 2. Not having an fp_holder access an element of fpconf.
//
fp_adapter_list_t fp_adapter_list;
os::mutex_t fp_adapter_list_lock;

void
fpconf_cl_net_if()
{

	the_fpconf->fp_ns_if();
}

//
// README FIRST:
// fp_holder_list_lock is used by path_up() and path_down() calls in CLK
// INTR to move elements from active to down list and vice versa. So, one
// has to be very very very very careful when using this lock for
// locking up the active and down lists.
// Things which should NEVER NEVER NEVER be done holding this lock are
// 1. allocating memory.
// 2. sleeping (doing a cv_wait())
// 3. putting msgs on streams context.
// 4. generally loops which can go on for a long time.
// 5. all such bad bad things.
//
// My suggestion is that this lock can be used only for lookups, adding
// already allocated elements to the list, or erasing an element from the
// list.
//
fp_holder::fp_holder(clconf_path_t *pth) :
	_SList::ListElem(this)
{
	// XXX Fix the constructor with more
	// initialization later.
	queue = NULL;
	for (uint_t i = 0; i < ETHERTYPE_COUNT; i++) {
		for (uint_t j = 0; j < NUM_8021D_UPRI; j++) {
			fp_ucast_headers[i][j] = NULL;
			fp_bcast_headers[i][j] = NULL;
		}
	}
	mac_addr_size = 0;
	is_up = false;
	path = pth;
	ref_count = 0;
	is_waiting = false;
	fp_adap = NULL;
	have_remote_mac = false;
	remote_device_name = (char *)NULL;
	brdcast_capable_adapter = true;
	bzero(local_mac_addr, (size_t)MAX_MAC_ADDR_SIZE);
	bzero(remote_mac_addr, (size_t)MAX_MAC_ADDR_SIZE);
	bzero(bcast_mac_addr, (size_t)MAX_MAC_ADDR_SIZE);
}

fp_holder::~fp_holder()
{
	ASSERT(!is_up);
	ASSERT(ref_count == 0);
	queue = NULL;
	path = NULL;
	fp_adap = NULL;
	delete remote_device_name;
	remote_device_name = NULL;
	for (uint_t i = 0; i < ETHERTYPE_COUNT; i++) {
		for (uint_t j = 0; j < NUM_8021D_UPRI; j++) {
			if (fp_ucast_headers[i][j] != NULL)
				freeb(fp_ucast_headers[i][j]);
			if (fp_bcast_headers[i][j] != NULL)
				freeb(fp_bcast_headers[i][j]);
		}
	}
	/*
	 * The following eliminates complains about not freeing queue
	 */
	/*lint -e1740 */
}


bool
fp_holder::get_queue()
{
	int device_instance;
	const char *device_name;
	fp_adapter *ada;

	fp_holder_lock.lock();

	ASSERT(path);

	clconf_cluster_t *cl = clconf_cluster_get_current();
	clconf_adapter_t *local_adap = NULL;
	clconf_adapter_t *remote_adap = NULL;

	local_adap = clconf_path_get_adapter(path, CL_LOCAL, cl);
	remote_adap = clconf_path_get_adapter(path, CL_REMOTE, cl);

	ASSERT(local_adap);
	ASSERT(remote_adap);

	device_instance = clconf_adapter_get_device_instance(local_adap);
	device_name = clconf_adapter_get_dlpi_device_name(local_adap);

	ASSERT(device_name != NULL);

	//
	// Set the fp_adapter object for this fp_holder.
	//

	fp_adapter_list_lock.lock();
	for (fp_adapter_list.atfirst();
	    ((ada = fp_adapter_list.get_current()) != NULL);
	    fp_adapter_list.advance()) {
		if (ada->adap_compare(device_name, device_instance)) {
			fp_adap = ada;
			break;
		}
	}

	fp_adapter_list_lock.unlock();

	//
	// fp_adap could be NULL if open_queue() failed before. We need to
	// make sure this routine returns false, so that no fp_holder is
	// created in non-debug mode.
	//
	ASSERT(fp_adap);

	//
	// 4510433 connection to scalable service times out and causes
	// Make a copy of the remote device name, since the memory can get
	// de-allocated when we release the cluster object later.
	//
	remote_device_name = os::strdup(
	    clconf_adapter_get_dlpi_device_name(remote_adap));
	remote_device_instance =
	    clconf_adapter_get_device_instance(remote_adap);

	ASSERT(remote_device_name != NULL);

	if ((strcmp(remote_device_name, "scid") == 0))
		brdcast_capable_adapter = false;

	queue = NULL;
	if (fp_adap) {
		queue = fp_adap->get_queue(local_mac_addr, &mac_addr_size,
		    bcast_mac_addr, &bcast_addr_size);
	}

	clconf_obj_release((clconf_obj_t *)cl);

	fp_holder_lock.unlock();

	if (queue != NULL) {
		return (true);
	} else {
		return (false);
	}
}

bool
fp_holder::rel_queue()
{
	fp_holder_lock.lock();
	ASSERT(!is_up);
	ASSERT(queue);

	if (queue == NULL) {
		fp_holder_lock.unlock();
		return (true);
	}

	FP_DBG(("rel queue called on fph %p queue = %p\n", this, queue));

	//
	// If ref count is non-zero, then this path is being used by
	// sender threads. So, wait until the ref count drops down to
	// zero. Set the is_waiting flag to true. The decr_ref_count()
	// should signal us when the ref count drops to zero.
	//
	while (ref_count != 0) {
		is_waiting = true;
		fp_holder_cv.wait(&fp_holder_lock);
	}
	fp_adap->rel_queue();
	queue = NULL;
	fp_holder_lock.unlock();

	return (true);
}

//
// Return redirection info to the caller given packet type (sap host order)
// and whether it is a broadcast request. In case it is a broadcast request
// the fast path header with the broadcast header is returned if the adapter
// is broadcast capable, else the unicast header is returned - this is what
// is expected by the caller.
//
bool
fp_holder::fp_path_data(ushort_t ho_type, uchar_t upri, bool bcast,
    ret_data_t &data)
{
	int	ethertype_index;

	fp_holder_lock.lock();
	//
	// If the path has gone down or if the underlying device could not
	// be opened, return.
	//
	if (!is_up || queue == NULL) {
		FP_DBG(("queue null in fp_path_data\n"));
		fp_holder_lock.unlock();
		return (false);
	}
	data.q = NULL;
	if (!have_remote_mac) {
		//
		// This implies that the path is not yet completely
		// setup. We still don't have the macaddress of the
		// remote endpoint yet. So, we return without doing
		// anything.
		// Return value indicates that the work_count
		// for this node has to be incremented and the
		// remote macaddress should be got.
		//
		FP_DBG(("mac addr sz zero in fp_path_data\n"));
		fp_holder_lock.unlock();
		return (true);
	}
	data.q = queue;
	data.ho_sap = ho_type;
	data.rmacaddr = remote_mac_addr;
	data.lmacaddr = local_mac_addr;
	data.size = (uint_t)mac_addr_size;
	data.bcstaddr = bcast_mac_addr;
	data.bcstsize = (uint_t)bcast_addr_size;
	data.fph = this;
	data.fp_mp = NULL;
	data.adapterp = fp_adap;
	data.brdcast_capable_adapter = brdcast_capable_adapter;
	data.upri = upri;

	if ((ethertype_index = fpconf::ethertype_to_index(ho_type)) >= 0) {
		ASSERT(upri <= MAX_8021D_UPRI);
		if (bcast && brdcast_capable_adapter)
			data.fp_mp = fp_bcast_headers[ethertype_index][upri];
		else
			data.fp_mp = fp_ucast_headers[ethertype_index][upri];
	}
	//
	// The ref_count will be decremented in the matching call to
	// decr_ref_count. As long as the ref count is non zero, the
	// adapter queue will stay open on behalf of this fp holder.
	//
	ref_count++;
	fp_holder_lock.unlock();

	return (false);
}

bool
fp_holder::is_bw_available(size_t msg_size)
{
	ASSERT(fp_adap);
	return (fp_adap->bandwidth_allocator(msg_size));
}

void
fp_holder::update_remote_macaddr(network::macinfo_t &macaddr)
{
	fp_holder_lock.lock();
	if (!is_up) {
		// Path went down while we were out getting the remote address.
		fp_holder_lock.unlock();
		return;
	}

	ASSERT(macaddr.size > 0);
	ASSERT(macaddr.size < MAX_MAC_ADDR_SIZE);
	if (have_remote_mac) {
		ASSERT(mac_addr_size == macaddr.size);
		ASSERT(bcmp(remote_mac_addr, macaddr.addr, mac_addr_size) == 0);
		fp_holder_lock.unlock();
		return;
	}
	mac_addr_size = macaddr.size;
	for (uint_t i = 0; i < mac_addr_size; i++) {
		remote_mac_addr[i] = (unsigned char)macaddr.addr[i];
	}
	have_remote_mac = true;
	//
	// Now that we know the remote mac addr, it's time to obtain the
	// DLPI fast path header in case the driver supports it. This needs
	// to be done for all known ethertypes for both broadcast and unicast.
	// We will do this in a loop over the ethertypes and within the loop
	// first for unicast, then for broadcast.
	//
	for (uint_t i = 0; i < ETHERTYPE_COUNT; i++) {
		for (uchar_t upri = 0; upri < NUM_8021D_UPRI; upri++) {
			fp_ucast_headers[i][upri] = fp_adap->get_fp_header(
			    remote_mac_addr, mac_addr_size,
			    fpconf::ethertypes[i], upri);

			// Now do the same thing for broadcast address

			fp_bcast_headers[i][upri] = fp_adap->get_fp_header(
			    bcast_mac_addr, bcast_addr_size,
			    fpconf::ethertypes[i], upri);
		}
	}
	fp_holder_lock.unlock();
}

fp_adapter*
fp_holder::get_adapter()
{
	return (fp_adap);
}

void
fp_holder::path_down()
{
	fp_holder_lock.lock();
	ASSERT(is_up);
	is_up = false;
	have_remote_mac = false;
	bzero(remote_mac_addr, (size_t)MAX_MAC_ADDR_SIZE);
	for (uint_t i = 0; i < ETHERTYPE_COUNT; i++) {
		for (uchar_t upri = 0; upri < NUM_8021D_UPRI; upri++) {
			if (fp_ucast_headers[i][upri] != NULL) {
				freeb(fp_ucast_headers[i][upri]);
				fp_ucast_headers[i][upri] = NULL;
			}
			if (fp_bcast_headers[i][upri] != NULL) {
				freeb(fp_bcast_headers[i][upri]);
				fp_bcast_headers[i][upri] = NULL;
			}
		}
	}
	fp_holder_lock.unlock();
}

pernodepath::pernodepath()
{
}

pernodepath::~pernodepath()
{
	fp_holder *fph;
	fp_holder_list_lock.lock();
	fp_path_down_list.atfirst();
	while ((fph = fp_path_down_list.get_current()) != NULL) {
		fp_path_down_list.advance();
		ASSERT(!(fph->is_path_up()));
		(void) fph->rel_queue();
		(void) fp_path_down_list.erase(fph);
		delete fph;
	}
	fp_holder_list_lock.unlock();
}

void
pernodepath::mark_path_up(clconf_path_t *pth)
{
	fp_holder *fph = NULL;
	fp_holder_list_lock.lock();

	FP_DBG(("path up called for pth = %x\n", pth));
	for (fp_path_down_list.atfirst();
	    (fph = fp_path_down_list.get_current()) != NULL;
	    fp_path_down_list.advance()) {
		if (fph->get_path() == pth) {
			(void) fp_path_down_list.erase(fph);
			fph->path_up();
			fp_active_list.append(fph);
			FP_DBG(("pnp %p: fh %p moved from PD list to A list\n",
			    this, fph));
			break;
		}
	}
	ASSERT(fph);
	fp_holder_list_lock.unlock();
}

void
pernodepath::mark_path_down(clconf_path_t *pth)
{
	fp_holder *fph = NULL;

	FP_DBG(("path down called for pth = %x\n", pth));
	fp_holder_list_lock.lock();
	for (fp_active_list.atfirst();
	    (fph = fp_active_list.get_current()) != NULL;
	    fp_active_list.advance()) {
		if (fph->get_path() == pth) {
			(void) fp_active_list.erase(fph);
			fph->path_down();
			fp_path_down_list.append(fph);
			FP_DBG(("pnp %p: fh %p moved from A list to PD list\n",
			    this, fph));
			break;
		}
	}
	if (fph == NULL) {
		for (fp_link_down_list.atfirst();
		    (fph = fp_link_down_list.get_current()) != NULL;
		    fp_link_down_list.advance()) {
			if (fph->get_path() == pth) {
				(void) fp_link_down_list.erase(fph);
				fph->path_down();
				fp_path_down_list.append(fph);
				FP_DBG(("pnp %p: fh %p moved from LD list to"
				    " PD list\n", this, fph));
				break;
			}
		}
	}
	ASSERT(fph);
	fp_holder_list_lock.unlock();
}

void
pernodepath::append_path(clconf_path_t *pth)
{
	fp_holder *fph = new fp_holder(pth);
	//
	// Try to open the adapter before putting the object on the
	// fp_holder_list. This way, there is practically zero contention
	// for locks between path_up()/path_down() calls.
	//
	if (fph->get_queue()) {
		fp_holder_list_lock.lock();
		fp_path_down_list.append(fph);
		FP_DBG(("pnp %p: fh %p added to PD list\n", this, fph));
		fp_holder_list_lock.unlock();
	} else {
		//
		// This implies that the local transport device
		// could not be opened correctly.
		//
		cmn_err(CE_WARN, "Scalable services will not run on this"
		    " cluster");
		FP_DBG(("append_path failed \n"));
		delete fph;
	}
}

void
pernodepath::remove_path(clconf_path_t *pth)
{
	fp_holder *fph = NULL;
	fp_holder_list_lock.lock();

	FP_DBG(("remove path called on pth = %x\n", pth));

	for (fp_path_down_list.atfirst();
	    (fph = fp_path_down_list.get_current()) != NULL;
	    fp_path_down_list.advance()) {
		if (fph->get_path() == pth) {
			(void) fp_path_down_list.erase(fph);
			FP_DBG(("pnp %p: fp %p removed from PD list\n",
			    this, fph));
			break;
		}
	}
	fp_holder_list_lock.unlock();
	ASSERT(fph);
	//
	// Release the fast path queue, before deleting the fp holder
	// object.
	//
	// Note: It's possible we do not find fph, since fph would not be
	// part of any list if get_queue() failed for it earlier.
	//
	if (fph) {
		if (!fph->rel_queue())
			ASSERT(0);
		delete fph;
	}
}

void
pernodepath::mark_all_down()
{
	fp_holder *fph = NULL;
	fp_holder_list_lock.lock();
	fp_active_list.atfirst();
	while ((fph = fp_active_list.get_current()) != NULL) {
		fp_active_list.advance();
		(void) fp_active_list.erase(fph);
		fph->path_down();
		fp_path_down_list.append(fph);
		FP_DBG(("pnp %p: fh %p moved from A list to PD list\n",
		    this, fph));
	}
	while ((fph = fp_link_down_list.get_current()) != NULL) {
		fp_link_down_list.advance();
		(void) fp_link_down_list.erase(fph);
		fph->path_down();
		fp_path_down_list.append(fph);
		FP_DBG(("pnp %p: fh %p moved from LD list to PD list\n",
		    this, fph));
	}
	fp_holder_list_lock.unlock();
}	

//
// Check which path to this node goes via the named adapter and update
// its status accordingly. If it is a link up notification then the
// fp_holder object representing the path needs to move from the
// fp_link_down_list to the fp_active_list. In case of link down
// notification the fp_folder object needs to move from the fp_active_list
// to the fp_link_down_list. In either case if the fp_holder is not
// found on the list it is expected to be on, then this notification
// is not important as the fp_holder object has moved to a different
// state via the path manager path_up/path_down mechanism. See fpconf.h
// for details on how the fp_holder moves from one list to another.
//
void
pernodepath::link_status_update(fp_adapter *adp, bool link_up)
{
	fp_holder *fhp;

	fp_holder_list_t &del_list =
		link_up ? fp_link_down_list : fp_active_list;
	fp_holder_list_t &add_list =
		link_up ? fp_active_list : fp_link_down_list;

	fp_holder_list_lock.lock();

	for (del_list.atfirst(); (fhp = del_list.get_current()) != NULL;
	    del_list.advance()) {
		if (fhp->get_adapter() == adp) {
			break;
		}
	}
	if (fhp != NULL) {
		if (del_list.erase(fhp)) {
			add_list.append(fhp);
		} else {
			ASSERT(0);
		}
		FP_DBG(("pnp %p: fh %p moved from %s list to %s list\n",
		    this, fhp, link_up ? "LD" : "A", link_up ? "A" : "LD"));
	}
	fp_holder_list_lock.unlock();
}

bool
pernodepath::get_broadcastdata(ushort_t ho_type, uchar_t upri,
    nodeid_t node, ret_data_t &data, long& adapter_hint)
{
	fp_holder *fph;
	long	adapter;
	bool	signal_needed = false;

	fp_holder_list_lock.lock();
	if (fp_active_list.empty()) {
		//
		// No active paths to the destination node.
		//
		FP_DBG(("get_brdcastdata: no active paths to destination\n"));
		fp_holder_list_lock.unlock();
		return (false);
	}

	if (adapter_hint == 0 || adapter_hint == -1) {
		//
		// Get the data for the first adapter on the list for the
		// remote node.  Set the adapter_hint for the next call
		// if the adapter is of a type which is broadcast capable.
		// Note: SCI adapters are not broadcast capable.
		//
		fp_active_list.atfirst();
		fph = fp_active_list.get_current();
		signal_needed = fph->fp_path_data(ho_type, upri, true, data);
		data.remote_node = node;
		if ((adapter_hint == 0) && data.brdcast_capable_adapter) {
			adapter_hint = (long)data.adapterp;
		}
	} else {
		// adapter_hint = 0;
		// fp_holder_list_lock.unlock();
		// return (signal_needed);

		//
		// Scan the list for the remote node and look for a match
		// with the hint broadcast adapter.  If a match is not
		// found, indicate this to the caller by setting adapter
		// hint to zero.
		//
		for (fp_active_list.atfirst();
		    (fph = fp_active_list.get_current()) != NULL;
		    fp_active_list.advance()) {
			adapter = (long)fph->get_adapter();
			if (adapter_hint == adapter) {
				break;
			}
		}
		if (fph == NULL) {
			adapter_hint = 0;
		}
	}

	fp_holder_list_lock.unlock();
	return (signal_needed);
}

bool
pernodepath::has_active_list()
{
	bool active;
	fp_holder_list_lock.lock();

	active = (fp_active_list.empty()) ? false : true;

	fp_holder_list_lock.unlock();
	return (active);

}


bool
pernodepath::get_data(ushort_t ho_type, uchar_t upri, ret_data_t &data,
    network::cid_t& cid, uint32_t& spi)
{
	fp_holder *fph;

	fp_holder_list_lock.lock();
	if (fp_active_list.empty()) {
		//
		// No active paths to the destination node.
		//
		FP_DBG(("no active paths to destination\n"));
		fp_holder_list_lock.unlock();
		return (false);
	}

	uint_t total_entries = fp_active_list.count();

	//
	// The hashing function is used to ensure that all packets
	// of a tcp connection, use the same path/adapter if possible.
	// This is done to prevent out of order delivery of packets
	// to the tcp stack on the proxy node. For udp, a randomizing
	// factor has been added to the destination port, so distribution
	// of datagrams will tend to be uniform.
	// If IPsec enabled, all connections with the same SPI, will use
	// the same path/adapter. We treat AH and ESP the same here.
	//

	int path_location;
	if (cid.ci_protocol == IPPROTO_AH || cid.ci_protocol == IPPROTO_ESP) {
		//
		// If IPsec enabled, port number will be in cipher text.
		// So use SPI for path location.
		//
		path_location = (int)CLPRIVNET_HASH_FUNC(spi, 0,
		    data.remote_node, total_entries);
	} else {
		path_location = (int)CLPRIVNET_HASH_FUNC(cid.ci_fport,
		    cid.ci_lport, data.remote_node, total_entries);
	}

	fp_active_list.atfirst();

	while (path_location--)
		fp_active_list.advance();

	fph = fp_active_list.get_current();
	ASSERT(fph);

	bool signal_needed = fph->fp_path_data(ho_type, upri, false, data);
	fp_holder_list_lock.unlock();
	return (signal_needed);

}

//
// This routine is called from muxq through the fpconf object, to get a
// path through which the packets can be sent. The three main things that
// this routine is responsible for is,
// 1. stripe data on a per connection basis most of the times.
// 2. check and allocate bandwidth on a selected adapter. If none available,
// scan the entire list of paths.
// 3. Increment the refcount of the fp holder object to prevent it from getting
// deleted and queue from closing when doing a putnext.
//
bool
pernodepath::get_data(ushort_t ho_type, uchar_t upri, ret_data_t &data,
    size_t msg_size, network::cid_t& cid)
{
	fp_holder *fph;
	network::cid_t tmp_cid;

	fp_holder_list_lock.lock();
	if (fp_active_list.empty()) {
		//
		// No active paths to the destination node.
		//
		FP_DBG(("no active paths to destination\n"));
		fp_holder_list_lock.unlock();
		return (false);
	}

	uint_t total_entries = fp_active_list.count();

	//
	// The hashing function is used to ensure that all packets
	// of a connection, use the same path/adapter if possible.
	// This is done to prevent out of order delivery of packets
	// to the tcp stack on the proxy node. However, the bad thing
	// with this method is that, striping can no longer be guaranteed
	// to be uniform across all the transport adapters.
	//

	bzero(&tmp_cid, sizeof (network::cid_t));
	int path_location = (int)FP_PATH_HASH_FUNC(cid, total_entries);

	fp_active_list.atfirst();

	while (path_location--)
		fp_active_list.advance();

	fph = fp_active_list.get_current();
	ASSERT(fph);

	if (!fph->is_bw_available(msg_size)) {
		//
		// No bw on the preferred adapter.
		// Search the entire list of active fp_holder objects
		// to see if bw is available on any on the fp_holders
		// Select the first one which has bandwidth available.
		//
		for (fp_active_list.atfirst();
		    (fph = fp_active_list.get_current()) != NULL;
			fp_active_list.advance()) {
			if (fph->is_bw_available(msg_size)) {
				break;
			}
		}
	}

	if (fph == NULL) {
		//
		// No bandwidth is available on any of the adapters.
		// This is a bad situation. Lets not aggravate it
		// by pumping more data through the transport adapters.
		//
		FP_DBG(("no bandwidth available on tr adapters\n"));
		fp_holder_list_lock.unlock();
		data.q = NULL;
		return (false);
	}

	ASSERT(fph);
	bool signal_needed = fph->fp_path_data(ho_type, upri, false, data);
	fp_holder_list_lock.unlock();
	return (signal_needed);
}

void
pernodepath::decr_ref_count(ret_data_t &data)
{
	data.fph->decr_ref();
}

void
pernodepath::start_matching(network::macinfoseq_t_var macaddr)
{
	fp_holder *fp;

	for (uint_t i = 0; i < macaddr.length(); i++) {
		fp_holder_list_lock.lock();
		FP_DBG(("start matching, %s, inst =%x\n",
		    (const char *)macaddr[i].device_name, macaddr[i].instance));
		for (fp_active_list.atfirst();
		    (fp = fp_active_list.get_current()) != NULL;
		    fp_active_list.advance()) {
			if (fp->same_device(macaddr[i])) {
				//
				// Update mac address for the fp_holder object
				// after dropping the list lock as update mac
				// address can block. Increment the ref count
				// before dropping the list lock so that the
				// fp_holder object cannot be deleted while
				// update_remote_address is in progress even
				// if the fp_holder gets deleted from the
				// lists.
				//
				fp->incr_ref();
				break;
			}
		}
		fp_holder_list_lock.unlock();
		if (fp != NULL) {
			fp->update_remote_macaddr(macaddr[i]);
			fp->decr_ref();	// Matches incr_ref above in for loop
		}
	}
}

fpconf::fpconf() :
	_SList::ListElem(this)
{
	int i;

	for (i = 0; i <= NODEID_MAX; i++) {
		paths[i] = NULL;
		work_count[i] = 0;
	}

	the_fpconf = this;

	if (clnewlwp_stack((void(*) (void *))fpconf_cl_net_if, NULL,
	    minclsyspri, NULL, NULL, _defaultstksz) != 0) {
		CL_PANIC(!"No PDT Fastpath thread.");
	}

	// create packet redirector object for support of the clprivnet driver
	pkt_redirect_objp = new pkt_redirector;

	//
	// Provide clprivnet driver with interfaces to the redirection object,
	// and provide it a way to translate any IEEE 802.1d priority values
	// it may receive from IP into mblk b_band that it may pass back to us.
	//
	clprivnet_set_pkt_redirect(&pkt_redirect);
	clprivnet_set_fragment_timeout(&fragment_timeout);
	clprivnet_set_dlpri_to_bband(sc_dlpri_to_bband_f);

	path_manager::the().add_client(path_manager::PM_OTHER, this);


}

fpconf::~fpconf()
{
	for (int i = 1; i <= NODEID_MAX; i++) {
		if (paths[i]) {
			delete paths[i];
			paths[i] = NULL;
		}
	}
}	

bool
fpconf::add_adapter(clconf_adapter_t *adap)
{
	fp_adapter *fp_adap = new fp_adapter(adap);

	if (fp_adap->open_queue() == NULL) {
		//
		// Device initialization failure shouldn't happen. We will
		// catch it in debug mode so that we could be alerted of
		// bugs in the driver. But we'll tolerate it in non-debug
		// mode and simply make sure subsequent code don't panic.
		//
		ASSERT(!"Failed to initialize fast path device");
		delete fp_adap;
		return (false);
	}

	fp_adapter_list_lock.lock();
	fp_adapter_list.append(fp_adap);
	recompute_clprivnet_mtu();
	fp_adapter_list_lock.unlock();

	return (true);
}

bool
fpconf::change_adapter(clconf_adapter_t *adap)
{
	fp_adapter *fp_adap;

	const char *device_name;
	device_name = clconf_adapter_get_dlpi_device_name(adap);
	ASSERT(device_name != NULL);

	int inst = clconf_adapter_get_device_instance(adap);

	fp_adapter_list_lock.lock();
	for (fp_adapter_list.atfirst();
	    (fp_adap = fp_adapter_list.get_current()) != NULL;
	    fp_adapter_list.advance()) {
		if (fp_adap->adap_compare(device_name, inst)) {
			fp_adap->calculate_bandwidth(adap);
			break;
		}
	}
	fp_adapter_list_lock.unlock();
	return (true);
}

bool
fpconf::remove_adapter(clconf_adapter_t *adap)
{
	fp_adapter *fp_adap;
	const char *device_name;
	device_name = clconf_adapter_get_dlpi_device_name(adap);
	ASSERT(device_name != NULL);
	int inst = clconf_adapter_get_device_instance(adap);

	FP_DBG(("remove adapter %s%d cladp %p\n", device_name, inst, adap));

	fp_adapter_list_lock.lock();
	for (fp_adapter_list.atfirst();
	    (fp_adap = fp_adapter_list.get_current()) != NULL;
	    fp_adapter_list.advance()) {
		if (fp_adap->adap_compare(device_name, inst)) {
			(void) fp_adapter_list.erase(fp_adap);
			break;
		}
	}
	recompute_clprivnet_mtu();
	fp_adapter_list_lock.unlock();
	ASSERT(fp_adap);

	FP_DBG(("removed adapter %s%d cladp %p fpadp %p\n",
	    device_name, inst, adap, fp_adap));

	if (fp_adap) {
		fp_adap->close_queue();
		delete fp_adap;
	}

	return (true);
}

bool
fpconf::add_path(clconf_path_t *pth)
{
	ASSERT(pth != NULL);
	nodeid_t node = clconf_path_get_remote_nodeid(pth);

	FP_DBG(("add path to node = %x\n", node));

	path_lock.lock();
	if (paths[node] == NULL) {
		//
		// List head of paths to a node is
		// null. So allocate one. The list head
		// will be destroyed in the fpconf destructor.
		//
		paths[node] = new pernodepath;
	}

	ASSERT(paths[node]);
	path_lock.unlock();

	paths[node]->append_path(pth);
	return (true);
}

bool
fpconf::remove_path(clconf_path_t *pth)
{
	ASSERT(pth != NULL);
	nodeid_t node = clconf_path_get_remote_nodeid(pth);

	FP_DBG(("remove path to node = %x\n", node));

	// List head cannot be null

	ASSERT(paths[node]);

	paths[node]->remove_path(pth);

	return (true);
}

bool
fpconf::path_up(clconf_path_t *pth)
{
	ASSERT(pth != NULL);
	nodeid_t node = clconf_path_get_remote_nodeid(pth);

	//
	// List head cannot be NULL here. An add_path() call should
	// have completed before path_up() happens for this path.
	//

	FP_DBG(("path up called for pth =%x, node =%x\n", pth, node));

	ASSERT(paths[node]);

	paths[node]->mark_path_up(pth);

	//
	// Will have to get the remotemac addr here. So increment
	// workcount and signal the thread which is going to get
	// the mac address for us.
	//
	work_lock.lock();
	if (work_count[node] == 0)
		work_count[node]++;

	FP_DBG(("work count for node %x is %x\n", node, work_count[node]));

	work_cv.signal();
	work_lock.unlock();

	return (true);
}

bool
fpconf::path_down(clconf_path_t *pth)
{
	ASSERT(pth != NULL);
	nodeid_t node = clconf_path_get_remote_nodeid(pth);

	FP_DBG(("path down called on pth = %x, node =%x\n", pth, node));
	//
	// List head cannot be null for same reasons as path_up() call.
	//

	ASSERT(paths[node]);

	paths[node]->mark_path_down(pth);

	return (true);
}

bool
fpconf::disconnect_node(nodeid_t node, incarnation_num)
{
	ASSERT(paths[node]);

	paths[node]->mark_all_down();

	return (true);
}

void
fpconf:: recompute_clprivnet_mtu()
{
	fp_adapter *fp_adap;
	t_uscalar_t adp_mtu;
	t_uscalar_t new_mtu = (t_uscalar_t)0xffffffff;

	ASSERT(fp_adapter_list_lock.lock_held());
	for (fp_adapter_list.atfirst();
	    (fp_adap = fp_adapter_list.get_current()) != NULL;
	    fp_adapter_list.advance()) {
		adp_mtu = fp_adap->get_mtu();
		if (adp_mtu < new_mtu)
			new_mtu = adp_mtu;
	}
	if (new_mtu != clprivnet_mtu) {
		FP_DBG(("Changing clprivnet mtu %d -> %d\n",
			clprivnet_mtu, new_mtu));
		clprivnet_mtu = new_mtu;
		clprivnet_set_mtu(clprivnet_mtu);
		if (ipconf::is_initialized()) {
			ipconf::the().clprivnet_mtu_changed();
		}
	}
}

//
// fpconf receives the link status change notifications on adapters and
// just forwards the calls to all pernodepath objects.
//
void
fpconf::link_status_update(fp_adapter *adp, bool link_up)
{
	FP_DBG(("%s%d adp %p: link is %s\n", adp->get_device(),
	    adp->get_device_instance(), adp, link_up ? "up" : "down"));
	path_lock.lock();
	for (int i = 1; i <= NODEID_MAX; i++) {
		if (paths[i] == NULL)
			continue;
		paths[i]->link_status_update(adp, link_up);
	}
	path_lock.unlock();
}

//
// Get the redirection data for a physical adapter on which to transmit a
// mac layer broadcast packet.
//
// An initial_node value greater than 1 means the caller is iterating one node
// at a time because there is no clusterwide broadcast adapter.  In this case,
// broadcast_adapter is set to -1 and there will be no search for such an
// adapter.  When initial_node is 1, this is a first-time call so an attempt
// is made to find a clusterwide broadcast adapter.  The search begins with
// broadcast_adapter initialized to 0.
//
void
fpconf::get_broadcastdata_incr_count(ushort_t ho_type, uchar_t upri,
    ret_data_t& data, nodeid_t& initial_node)
{
	bool signal_reqd = false;
	long	broadcast_adapter;
	uint_t  node;
	uint_t first_valid_node = 0;

	ASSERT(initial_node);

	// Initialize for clusterwide broadcast search
	broadcast_adapter = (initial_node == 1) ? 0 : -1;

	//
	// Search active paths for clusterwide broadcast adapter.
	//
	for (node = initial_node; node < NODEID_MAX; node++) {
		if (paths[node] && paths[node]->has_active_list()) {
			if (first_valid_node == 0) {
				first_valid_node = node;
			}

			//
			// get the redirection data and check for possibility
			// of a clusterwide broadcast adapter.
			//
			signal_reqd = paths[node]->get_broadcastdata(ho_type,
			    upri, node, data, broadcast_adapter);

			//
			// If there is not a single adapter for clusterwide
			// broadcast then update initial_node for the caller's
			// next iteration.
			//
			if (broadcast_adapter == 0 ||
			    broadcast_adapter == -1 || signal_reqd) {
				initial_node = (broadcast_adapter == -1) ?
				    node +1 : first_valid_node +1;
				data.brdcast_capable_adapter = false;
				break;
			}
		}
	}
	//
	// Caller iterations continue when initial node is > 1
	//
	if (first_valid_node == 0) {
		initial_node = 1;
	}
	//
	// This basically says that there is a fp_holder object in
	// which mac address has not been set. So, we do a lazy update
	// here and wake up the thread which is supposed to be getting
	// the remote mac address for us.
	//
	if (signal_reqd) {
		work_lock.lock();
		FP_DBG(("incremented work count for node =%x\n", node));
		work_count[node] = 1;
		work_cv.signal();
		work_lock.unlock();
	}

}

//
// Call into the path manager interface and increment the count
// on the fp holder being used. This prevents the queue from being
// closed when we are doing a putnext.
//

//
// Data for packet redirection to a physical interface is retrieved.  The
// information contained in cid is used for hashing to an fp holder
// object for a physical interface.  The ref count for the holder object
// is incremented to prevent the redirection queue from being closed.
// The caller is responsible for calling decr_count().  For this
// polymorphic version, there is no bandwidth utilization checking so
// msg_size is not needed.
//
void
fpconf::get_data_and_incr_count(ushort_t ho_type, uchar_t upri,
    uint_t node, ret_data_t& data, network::cid_t& cid, uint32_t& spi)
{
	bool signal_reqd = false;

	ASSERT(node);
	if (node > 0 && node <= NODEID_MAX && paths[node]) {
		signal_reqd = paths[node]->get_data(ho_type, upri,
		    data, cid, spi);
	}

	//
	// This basically says that there is a fp_holder object in
	// which mac address has not been set. So, we do a lazy update
	// here and wake up the thread which is supposed to be getting
	// the remote mac address for us.
	//
	if (signal_reqd) {
		work_lock.lock();
		FP_DBG(("incremented work count for node =%x\n", node));
		work_count[node] = 1;
		work_cv.signal();
		work_lock.unlock();
	}
}


void
fpconf::get_data_and_incr_count(ushort_t ho_type, uchar_t upri, uint_t node,
    ret_data_t &data, size_t msg_size, network::cid_t& cid)
{
	bool signal_reqd = false;

	ASSERT(node);
	if (node > 0 && node <= NODEID_MAX && paths[node]) {
		signal_reqd = paths[node]->get_data(ho_type, upri, data,
		    msg_size, cid);
	}

	//
	// This basically says that there is a fp_holder object in
	// which mac address has not been set. So, we do a lazy update
	// here and wake up the thread which is supposed to be getting
	// the remote mac address for us.
	//
	if (signal_reqd) {
		work_lock.lock();
		FP_DBG(("incremented work count for node =%x\n", node));
		work_count[node] = 1;
		work_cv.signal();
		work_lock.unlock();
	}

}

void
fpconf::decr_count(uint_t node, ret_data_t &data)
{
	ASSERT(node);
	ASSERT(paths[node]);

	if (node > 0 && node <= NODEID_MAX && paths[node])
		paths[node]->decr_ref_count(data);
}

void
fpconf::register_with_ns()
{
	Environment e;
	char mystring[25];

	cl_net_ns_if_impl_t *macobj = new cl_net_ns_if_impl_t(this);
	os::sprintf(mystring, "CL_NET_MAC_IF:%x", orb_conf::local_nodeid());
	naming::naming_context_var ctxp = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));

	ns::root_nameserver()->rebind(mystring, macobj->get_objref(), e);

	ASSERT(!e.exception());

	e.clear();
}

void
fpconf::fp_ns_if()
{
	network::cl_net_ns_if_ptr macobj;
	Environment e;
	network::macinfoseq_t_var mcinfo;
	work_lock.lock();
	while (true) {
		int dropped_lck = 0;
		for (int i = 0; i <= NODEID_MAX; i++) {
			if (work_count[i] > 0) {
				//
				// Reset the work_count for remote
				// node to zero, irrespective of error
				// or not. This way, we are guaranteed
				// to be woken up when someone increments
				// the work_count.
				//
				work_count[i] = 0;
				work_lock.unlock();
				dropped_lck++;
				macobj = get_node_obj(i);
				if (!CORBA::is_nil(macobj))
					macobj->get_mac_info(mcinfo, e);
				work_lock.lock();
				if ((e.exception()) ||
				    (CORBA::is_nil(macobj))) {
					//
					// Looks like the node we
					// were talking to died. Nothing
					// much can be done here.
					//
					if (!CORBA::is_nil(macobj))
						CORBA::release(macobj);
					e.clear();
					continue;
				}
				ASSERT(paths[i]);
				paths[i]->start_matching(mcinfo);
				CORBA::release(macobj);
			}
		}
		if (dropped_lck == 0)
			work_cv.wait(&work_lock);
		FP_DBG(("thread to get mac addr woken up\n"));
	}
}

network::cl_net_ns_if_ptr
fpconf::get_node_obj(int node)
{
	naming::naming_context_var ctxp;
	CORBA::Object_var objv;
	char nodestring[25];
	Environment e;
	network::cl_net_ns_if_ptr macobj;

	ctxp = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxp));
	os::sprintf(nodestring, "CL_NET_MAC_IF:%x", node);

	for (int retries = 0; retries < 5; retries++) {
		objv = ctxp->resolve(nodestring, e);
		if (e.exception()) {
			e.clear();
			os::usecsleep((ulong_t)10000);
		} else {
			break;
		}
	}

	if (!CORBA::is_nil(objv)) {
		macobj = network::cl_net_ns_if::_narrow(objv);
		if (CORBA::is_nil(macobj)) {
			FP_DBG(("no ns ref to macobj found \n"));
			return (NULL);
		} else {
			FP_DBG(("found a valid macobj\n"));
			return (macobj);
		}
	}

	return (NULL);
}



fp_adapter::fp_adapter(clconf_adapter_t *ada) :
	_SList::ListElem(this)
{
	total_bytes = 0;
	next_timeout = 0;
	allowed_bandwidth = 0;
	total_bandwidth = 0;
	timeout_incr = drv_usectohz((ulong_t)1000000);
	local_mac_size = 0;
	bcast_addr_size = 0;
	pkt_drop_count = 0;
	network_bw_percent = DEFAULT_NW_BW;

	// Initialize access to device driver
	device_uh = NULL;
	queue = NULL;

	// Take a copy of the device_name property as the clconf objects
	// get deleted. We could have taken a hold but that creates a
	// memory leak
	const char *device_name = clconf_adapter_get_dlpi_device_name(ada);

	ASSERT(device_name != NULL);

	calculate_bandwidth(ada);

	local_device = os::strdup(device_name);
	local_device_instance = clconf_adapter_get_device_instance(ada);
	local_vlan_id = clconf_adapter_get_vlan_id(ada);

	ref_cnt = 0;
	waiting_for_fp_ack = false;
	fp_query_in_progress = false;
	fp_ack_mchain = NULL;

	FP_DBG(("fp_adap constr ldev =%s, inst = %d\n",
	    local_device, local_device_instance));
	FP_DBG(("bandwidth set to %d\n", total_bandwidth));
	FP_DBG(("NW_bandwidth set to %d\n", network_bw_percent));
}

fp_adapter::~fp_adapter()
{
	delete [] local_device;
	ASSERT(queue == NULL && device_uh == NULL);
	ASSERT(fp_ack_mchain == NULL);
	/* The following avoids lints about not freeing queue, fp_ack_mchain */
	/*lint -e1540 */
}

void
fp_adapter::calculate_bandwidth(clconf_adapter_t *ada)
{
	const char *bw;

	bandwidth_lock.lock();
	bw = clconf_obj_get_property((clconf_obj_t *)ada, "bandwidth");
	if (bw != NULL) {
		total_bandwidth = (uint_t)os::atoi(bw);
	}

	if (total_bandwidth != 0) {
		ASSERT(total_bandwidth < (uint_t)(UINT_MAX/10000));
		//
		// Convert the bandwidth from Mbytes/sec to bytes/sec,
		// divided by 100. (to compensate for network_bw_percent
		// which is expressed as a percent)
		//
		total_bandwidth = total_bandwidth * 10000;
	} else {
		//
		// Default bandwidth of the device is 12.5Mbytes/sec.
		// total_bandwidth = 12500000/100
		//
		total_bandwidth = 125000;
	}

	bw = clconf_obj_get_property((clconf_obj_t *)ada, "nw_bandwidth");

	if (bw != NULL) {
		network_bw_percent = (uint_t)os::atoi(bw);
		if ((network_bw_percent < 1) || (network_bw_percent > 100))
			network_bw_percent = DEFAULT_NW_BW;
	}
	bandwidth_lock.unlock();
}

bool
fp_adapter::bandwidth_allocator(size_t msg_size)
{
	clock_t now;

	bandwidth_lock.lock();
	if (next_timeout == 0) {
		//
		// This should happen for only the first packet.
		// Get the current lbolt value and add 1 second
		// to it in terms of system ticks.
		//
		(void) drv_getparm(LBOLT, &next_timeout);
		next_timeout += timeout_incr;
		if (next_timeout == 0)
			next_timeout = 1;

		allowed_bandwidth = (ulong_t)network_bw_percent *
					(ulong_t)total_bandwidth;
		FP_DBG(("next timeout set to %lld\n", next_timeout));
		FP_DBG(("allowed bw = %lld\n", allowed_bandwidth));
	}

	ASSERT(allowed_bandwidth > 0);

	total_bytes += msg_size;
	if (total_bytes > (uint_t)allowed_bandwidth) {
		(void) drv_getparm(LBOLT, &now);
		if ((now - next_timeout) < 0) {
			//
			// Too many pkts. Will drop this one.
			//
			total_bytes -= msg_size;
			pkt_drop_count++;
			if (pkt_drop_count == 1) {
				FP_DBG(("Pkts are being dropped, START\n"));
				FP_DBG(("fp_adapter = 0x%x\n", this));
			}
			bandwidth_lock.unlock();
			return (false);
		} else {
			//
			// This is the start of a new timeout period.
			// Reset the total_bytes to this msg size.
			// and the timeout value to current lbolt
			// value plus one second.
			//
			next_timeout = now + timeout_incr;
			if (next_timeout == 0)
				next_timeout = 1;
			total_bytes = msg_size;
			if (pkt_drop_count > 0) {
				FP_DBG(
				    ("END, dropped = %d\n", pkt_drop_count));
				pkt_drop_count = 0;
			}
		}
	}
	bandwidth_lock.unlock();
	return (true);
}

void
fp_adapter::adap_info(network::macinfoseq_t &ainfo, uint_t index)
{
	ainfo[index].device_name = os::strdup(local_device);
	ainfo[index].instance = (uint_t)local_device_instance;
	ainfo[index].size = (uint_t)local_mac_size;

	/*lint -e574 -e676 */
	for (uint_t i = 0; i < (uint_t)local_mac_size; i++) {
		ainfo[index].addr[i] = (char)local_mac_addr[i];
	}
}

bool
fp_adapter::open_queue()
{
	queue = cl_net_fpath_open(&device_uh,
	    local_device, local_device_instance, local_vlan_id,
	    local_mac_addr, &local_mac_size, bcast_mac_addr,
	    &bcast_addr_size, &device_mtu, (char *)this);

	FP_DBG(("open_queue: dev =%s, inst = %d, queue = %p\n",
	    local_device, local_device_instance, queue));

	ASSERT(ref_cnt == 0);
	return (queue != NULL);
}

void
fp_adapter::close_queue()
{
	ASSERT(ref_cnt == 0);
	(void) cl_net_fpath_close(device_uh);
	device_uh = NULL;
	queue = NULL;
}

//
// Obtains a reference on the streams queue on the behalf of the caller
// (an fp_holder). Also returns local mac address and broadcast address
// associated with the adapter.
//
queue_t *
fp_adapter::get_queue(uchar_t *laddr, size_t *laddrlp, uchar_t *baddr,
    size_t *baddrlp)
{
	queue_t	*q;

	adp_lock.lock();
	*laddrlp = local_mac_size;
	bcopy(local_mac_addr, laddr, *laddrlp);
	*baddrlp = bcast_addr_size;
	bcopy(bcast_mac_addr, baddr, *baddrlp);
	q = queue;
	ref_cnt++;
	adp_lock.unlock();

	ASSERT(q != NULL);
	return (q);
}

//
// Release the reference obtained by a preceding get_queue.
//
void
fp_adapter::rel_queue()
{
	adp_lock.lock();
	ASSERT(ref_cnt > 0);
	ref_cnt--;
	adp_lock.unlock();
}

t_uscalar_t
fp_adapter::get_mtu()
{
	return (device_mtu);
}

//
// Called by the clfpstr rput routine to notify the fp_adapter object
// of the response to a dlpi fast path query.
//
void
fp_adapter::set_fp_ack_mchain(mblk_t *mp)
{
	adp_lock.lock();
	ASSERT(fp_query_in_progress);
	ASSERT(waiting_for_fp_ack);
	ASSERT(fp_ack_mchain == NULL);
	fp_ack_mchain = mp;
	waiting_for_fp_ack = false;
	adp_cv.broadcast();
	adp_lock.unlock();
}

//
// Called by fp_holder::update_mac_addr to obtain the dlpi fast path
// headers given the destination address and the dlpi sap.
//
mblk_t *
fp_adapter::get_fp_header(uchar_t *remote_addr, size_t addrl,
    ushort_t ho_type, uchar_t upri)
{
	mblk_t *mp;

	adp_lock.lock();

	// Allow only one query at a time
	while (fp_query_in_progress) {
		adp_cv.wait(&adp_lock);
	}

	//
	// Send the fast path probe to the driver. Set, fp_query in progress
	// to prevent queries on behalf of other fp_holders going down the
	// same stream. Drop the adap_lock before sending the ioctl as the
	// lock is also needed by the set_fp_ack_mchain method which is
	// needed by the ack which may arrive in the same thread context.
	//
	fp_query_in_progress = true;
	waiting_for_fp_ack = true;
	adp_lock.unlock();
	if (cl_net_fpath_ioctl_send(queue, remote_addr, addrl, ho_type, upri)
	    != 0) {
		ASSERT(0);
		return (NULL);
	}

	// Wait for the ack to come back
	adp_lock.lock();
	while (waiting_for_fp_ack) {
		adp_cv.wait(&adp_lock);
	}

	// Retrieve the ack, make space available for next query
	mp = fp_ack_mchain;
	fp_ack_mchain = NULL;

	// Let any waiting threads know that this query is done
	fp_query_in_progress = false;
	adp_cv.broadcast();

#if (defined(i386) || defined(__i386) || defined(__amd64))
	//
	// Solaris x86 TCP/IP stack has bugs where some DLPI users do not
	// send down a valid sap with a DL_UNITDATA_REQ mblk. The drivers
	// have worked around this by always using the sap bound to the
	// stream. Since, we need to xmit packets with sap values that could
	// be different from the bound sap, we need our own workaround.
	// Since we use the dlpi fast path here we have an opportunity to
	// fix the header returned by the driver. Among the drivers available
	// on x86 platforms, e1000g and ce need this fix. The GLD based
	// drivers (ibd, bge) return correct headers as they use the bound
	// sap only if the sap in the unitdata requests is not valid.
	//
	if (strcmp(local_device, "e1000g") == 0 ||
	    strcmp(local_device, "ce") == 0) {
		//
		// We know e1000g and ce use ethernet encapsulation and thus
		// the sap is the last two bytes in the returned fast path
		// header. This is true even if the device is operating in the
		// VLAN mode.
		//
		ushort_t	no_type = htons(ho_type);
		ASSERT(mp != NULL);
		if (mp != NULL) {
			bcopy(&no_type, mp->b_wptr - sizeof (ushort_t),
			    sizeof (ushort_t));
		}
	}
#else
	//
	// The DLPI fast path header returned by the SCI driver (which is
	// available only on SPARC) is not completely formed. The first two
	// bytes of the destination and src addresses don't get filled in.
	// This behavior is documented in bugid 5027118. The following is
	// to compensate for that bug.
	//
	if (strcmp(local_device, "scid") == 0) {
		//
		// We know scid uses (pseudo) Ethernet encapsulation with
		// address length 4.
		//
		if (mp != NULL) {
			bcopy(remote_addr, mp->b_rptr, addrl);
			bcopy(local_mac_addr, mp->b_rptr + addrl,
			    local_mac_size);
		}
	}
#endif

	//
	// For some reason the vlan driver ignores the dl_priority values
	// passed in the unitdata request and does not fill in the user
	// priority values in the tag nor does it set the b_band field of
	// the returned mblk. Instead the expectation is that the caller
	// will set the b_band field to the right value. Set the b_band
	// value to an appropriate value if it is not already set.
	//
	if (mp->b_band == 0) {
		mp->b_band = UPRI_TO_BBAND(upri);
	}

	adp_lock.unlock();

	return (mp);
}

void
cl_net_ns_if_impl_t::get_mac_info(network::macinfoseq_t_out macinfoout,
    Environment&)
{
	fp_adapter *fp_adap;
	uint_t i = 0;

	fp_adapter_list_lock.lock();
	macinfoout = new network::macinfoseq_t(fp_adapter_list.count());
	network::macinfoseq_t *minfo = macinfoout;

	minfo->length(fp_adapter_list.count());
	network::macinfoseq_t& macaddr = *minfo;

	for (fp_adapter_list.atfirst();
	    (fp_adap = fp_adapter_list.get_current()) != NULL;
	    fp_adapter_list.advance()) {
		fp_adap->adap_info(macaddr, i);
		i++;
	}
	fp_adapter_list_lock.unlock();
}

#ifdef _KERNEL

//
//		CLPRIVNET PACKET REDIRECTION FRAMEWORK
//
// For each physical network interface a private Stream is created using a
// special SAP to support packet redirection. This is initiated by the
// pernodepath::append method upon callback from the pathmanager object when
// a new path is added. The fundamental processing (done in fast_path.cc)
// consists of:
//	open physical interface driver
//	Attach to driver
//	Bind using private SAP (0x832)
//	Store Stream queue pointer in fpconf holder object
//
// Packet redirection for outgoing packets is done through a putnext operation
// using the special Stream write queue. Outgoing packets pass through the
// Stream module to the physical interface without further processing. Incoming
// packets from a remote clprivnet interface follow different paths depending
// on the type. For IP, packets arrive with a MAC header containing the SAP for
// IP and are sent upstream to IP directly. Non IP packets arrive with a MAC
// header containing the SAP for the special stream and are sent up to the
// packet redirect module,clfpstr. Clfpstr forwards the packet to the clprivnet
// driver. The clprivnet driver sends the packet up to the right module
// upstream based on the clprivnet MAC header.
//
// The distribution policy is such that IP packets are redirected equally over
// the physical interfaces for the operable redundant interconnect paths.
// Packets for a given TCP connection are redirected consistently over a single
// interface selected for that connection. For UDP, the redirection is not
// strictly by connection as for TCP - physical selection is on an individual
// packet basis. As the algorithm used for physical interface selection is not
// purely round-robin but is based mostly on hashing selected socket parameters
// the equality of usage is greater for a large number of connections. The
// policy does not consider load level.
//
// The physical interface selection is managed by the fpconf object.
// Registration with the cluster path manager by fpconf for interconnect
// configuration callbacks allows fpconf to maintain a list of information
// holder objects, with each object in the list representing an operable path
// to a remote node (and associated local network interface). There is one list
// per remote node. Retrieval of remote MAC addresses is support by a special
// thread, fpconf_cl_net_if, created in the fpconf constructor (formerly
// cl_net_if in inet_port_mgmt.cc). The packet MAC destination address is used
// for indexing into the path list array. Then a hash computation, based on
// connection identifier parameters, is used to select a list entry. A new hash
// function is implemented to support the needs of both clprivnet and the mcnet
// Scalable Services driver. All other aspects of fpconf remain unchanged and
// a single instantiation of fpconf serves both clprivnet and mcnet.
//
// If the list size should change as a result of interconnect path additions
// or removals, a TCP connection may hash to a different entry than it had
// previously, leading to possible out of order packet delivery. This would be
// a transient situation at worst so it is not a concern.
//
// For UDP the port numbers are randomized for better packet distribution.
//


extern "C" {
static void
pkt_redirect(nodeid_t rndid, ushort_t ho_type, queue_t *wq, mblk_t *mp)
{
	pkt_redirect_objp->redirect_packet(rndid, ho_type, wq, mp);

}
} // extern "C"

//
// This function is exported to the clprivnet driver through the function
// clprivnet_set_fragment_timeout. It provides clprivnet with access to the
// fragment_timeout method.
//
extern "C" {
static void
fragment_timeout(queue_t *wq, mblk_t *mp)
{
	pkt_redirect_objp->fragment_timeout(wq, mp);

}
} // extern "C"



//
// The pkt_redirector:: object accepts packets from the clprivnet driver and
// dispatches the packet to the redirection stream of a physical interface.
// Packet dispatching entails:
//	protocol information extraction and fragment handling
//	interface selection and redirection data retrieval
//	MAC header adjustment for redirection
//	transfer to special redirection Stream
//
// The first step to dispatching is to extract useful information from the
// packet headers. The clprivnet MAC level destination address is extracted -
// this provides the destination remote node id. For ARP packets, this is a
// broadcast address (all ones) and nothing further is needed. For IP packets,
// besides the remote nodeid, the destination and source port numbers must be
// extracted from the encapsulated TCP headers for subsequent use in physical
// interface adapter selection.
//
// Next, the extracted data is used to make a physical interface selection. The
// selected interface redirection holder object provides the physical interface
// MAC layer addresses for both source and destination, and a handle for the
// redirection Stream where the packet is to be transferred.
// The MAC header for the packet must be modified before transfer to the
// physical interface driver. For IP, the physical interface addresses are
// substituted for the clprivnet addresses. The packet type remains unchanged,
// so packets arriving at the destination physical interface are delivered
// directly to IP and the destination clprivnet driver is not involved.
//
//
// ARP packets require different MAC header modification. Because ARP
// operations are typically associated with a particular Stream or interface,
// ARP must receive operations pertaining to clprivnet on that Stream. So
// unlike incoming IP packets which can be sent directly to IP from the
// physical interface driver, incoming ARP packets must be sent up to ARP from
// the clprivnet driver. To accomplish this, the clprivnet MAC layer header
// addresses are not substituted as with IP, but instead is encapsulated with
// an additional header. The additional header contains the physical interface
// addressing and the packet type is set to that of the special redirection
// Stream SAP. When the remote physical interface receives the packet it sends
// it up the special redirection Stream and from there it is redirected to the
// clprivnet Stream for delivery to ARP.
// The final step in dispatching is a putnext operation using the physical
// interface Stream queue.
//

void
pkt_redirector::redirect_packet(nodeid_t remote_node, ushort_t ho_type,
    queue_t *wqp, mblk_t *mp)
{
	network::cid_action_t action;
	network::cid_t cid;
	mblk_t *temp_mp;
	mblk_t *nmp;
	nodeid_t search_node = 1;
	ret_data_t redirect_data;
	uint_t upri;
	uint32_t	spi;

	ASSERT(mp->b_datap->db_type == M_DATA);

	//
	// initialize for redirect interface selection
	//
	bzero(&cid, sizeof (cid));
	bzero(&redirect_data, sizeof (ret_data_t));

	//
	// Examine the packet and extract the header information
	//
	action = extract_pkt_info(wqp, mp, cid, remote_node, ho_type, spi);

	ASSERT(remote_node != 0);
	if (remote_node == 0)
		action = network::CID_DROP;

	//
	// Check mp->b_band to see if IP has requested a certain user
	// priority, else set to the default Sun Cluster Application
	// user priority.
	//
	upri = SC_APP_USR_PRI;
	if (mp->b_band != 0) {
		upri = BBAND_TO_UPRI(mp->b_band);
	}

	switch (action) {
		case network::CID_DISPATCH:
			if (remote_node == BROADCAST_ID) {
				//
				// If an adapter is found which can be used
				// to broadcast to all nodes then only one
				// xfer_packet call is needed. Otherwise loop
				// for xfer to each node.
				//
				do {
					get_broadcast_redirect_data(ho_type,
					    (uchar_t)upri, redirect_data,
					    search_node);

					if (redirect_data.q != NULL) {
						nmp = copymsg(mp);
						xfer_packet(nmp, redirect_data,
						    cid.ci_protocol,
						    redirect_data.
						    brdcast_capable_adapter);
					} else {
						break;
					}
				} while (search_node > 1);
				freemsg(mp);
				break;
			} else {
				// fragments are chained on b_next
				while (mp) {
					temp_mp = mp->b_next;
					mp->b_next = NULL;
					get_ip_redirect_data(ho_type,
					    (uchar_t)upri, remote_node, cid,
					    redirect_data, spi);
					redirect_data.remote_node = remote_node;
					if (redirect_data.q != NULL) {
						xfer_packet(mp, redirect_data,
						    cid.ci_protocol,
						    false);
					} else {
						freemsg(mp);
						mp = temp_mp;
						break;
					}
					mp = temp_mp;
				}
			}
			// fall through to catch early termination on
			// IP fragment chain processing.

		case network::CID_DROP:
			while (mp) {
				temp_mp = mp->b_next;
				mp->b_next = NULL;
				freemsg(mp);
				mp = temp_mp;
			}
			break;
		case network::CID_NOACTION:
			break;
		case network::CID_SENDUP:
		case network::CID_FORWARD:
		default:
			freemsg(mp);
			cmn_err(CE_NOTE,
			    "Unexpected action in clprivnet:redirect_pkt");
	}
}


//
// Allocate and initialize a standard DLPI UNIT_DATA_REQ mblk
// Used by clprivnet.
//
mblk_t *
pkt_redirector::create_dlpi_hdr(size_t maclen, uint_t upri)
{
	dl_unitdata_req_t *dl_udata;
	mblk_t *new_mp;

	if ((new_mp = allocb(sizeof (dl_unitdata_req_t) +
	    maclen, BPRI_HI)) == NULL) {
		return (NULL);
	}

	new_mp->b_datap->db_type = M_PROTO;
	new_mp->b_rptr = new_mp->b_wptr;

	dl_udata = (dl_unitdata_req_t *)new_mp->b_wptr;
	new_mp->b_wptr += sizeof (dl_unitdata_req_t);
	dl_udata->dl_primitive = DL_UNITDATA_REQ;
	dl_udata->dl_dest_addr_offset = (unsigned)sizeof (dl_unitdata_req_t);
	//
	// IEEE user priority  : larger value is higher priority
	// DL unitdata request : lower value is higher priority
	// upri to be in [0 - MAX_8021D_UPRI].
	//
	dl_udata->dl_priority.dl_min = (int)(MAX_8021D_UPRI - upri);
	dl_udata->dl_priority.dl_max = (int)(MAX_8021D_UPRI - upri);

	return (new_mp);
}

//
// Call into the path manager fast path interface for return of
// the redirection data (ie. redirect streams queue and mac addresses).
// Incrementing the refcount on the fp holder protects putnext from
// the queue being closed
//
//
void
pkt_redirector::get_broadcast_redirect_data(ushort_t ho_type, uchar_t upri,
    ret_data_t& redirection_data, nodeid_t& prev_node)
{

	redirection_data.q = NULL;
	redirection_data.rmacaddr = NULL;
	redirection_data.fp_mp = NULL;
	redirection_data.size = 0;

	ASSERT(fpc);

	fpc->get_broadcastdata_incr_count(ho_type, upri,
	    redirection_data, prev_node);

	if (redirection_data.q == NULL) {
		return;
	}

	ASSERT(redirection_data.rmacaddr);
	ASSERT(redirection_data.size);
	ASSERT(redirection_data.remote_node);
}


//
// Call into the path manager fast path interface for the redirection
// parameters (ie. redirect streams queue and  mac addresses).
// Incrementing the count on the fp holder protects putnext from
// the queue being closed
//
//
void
pkt_redirector::get_ip_redirect_data(ushort_t ho_type, uchar_t upri,
    nodeid_t remote_node, network::cid_t & cid, ret_data_t& redirection_data,
    uint32_t& spi)
{

	redirection_data.q = NULL;
	redirection_data.rmacaddr = NULL;
	redirection_data.size = 0;

	ASSERT(fpc);

	fpc->get_data_and_incr_count(ho_type, upri, remote_node,
	    redirection_data, cid, spi);

	//
	// If there are no active paths to this destination
	//
	if (redirection_data.q == NULL) {
		return;
	}
	ASSERT(redirection_data.q);
	ASSERT(redirection_data.rmacaddr);
	ASSERT(redirection_data.size);


}

//
// Make mblk adjustments for mac header and putnext to the physical
// interface.
//
void
pkt_redirector::xfer_packet(mblk_t *mp, ret_data_t& rdata,
    uchar_t protocol, bool broadcast)
{
	dl_unitdata_req_t	*dl_udata;
	uchar_t			*dlsap = NULL;
	mblk_t			*new_mp;
	size_t			hdr_size;

	if (rdata.q == NULL) {
		freemsg(mp);
		return;
	}

	//
	// If the underlying driver supports DLPI fast path we will attempt
	// to send this packet using the fast path. We already have fast
	// path headers computed.
	//
	if (rdata.fp_mp)  {
		//
		// Since the physical interface redirection Stream supports
		// DLPI Fast-Path, just need to adjust the mac header before
		// redirection.
		//
		hdr_size = (size_t)MBLKL(rdata.fp_mp);
		if (DB_REF(mp) == 1 && (size_t)MBLKHEAD(mp) >= hdr_size) {
			mp->b_rptr -= hdr_size;
			bcopy(rdata.fp_mp->b_rptr, mp->b_rptr, hdr_size);
			new_mp = mp;
			new_mp->b_band = rdata.fp_mp->b_band;
		} else {
			// Currently we need to use copymsg here to
			// work around the driver bugs where they change
			// the header without checking if there are any
			// references to the dblk.
			if ((new_mp = copymsg(rdata.fp_mp)) == NULL) {
				fpc->decr_count(rdata.remote_node, rdata);
				freemsg(mp);
				return;
			}
			new_mp->b_cont = mp;
		}
	} else {
		//
		// DLPI Fast-Path not supported by the redirection Stream
		//
		// Allocate and initialize a standard DLPI UNIT_DATA_REQ
		// M_PROTO mblk and link with the packet M_DATA mblk (mp).
		//
		if (broadcast) {
			new_mp = create_dlpi_hdr(rdata.bcstsize, rdata.upri);
		} else {
			new_mp = create_dlpi_hdr(rdata.size, rdata.upri);
		}
		//
		// The driver should use the dl_priority field of the unit
		// data request to compute the user priority to be stuffed in
		// the xmitted packet. To be doubly sure, we set the b_band
		// field of the M_DATA mblk to the right value.
		//
		mp->b_band = UPRI_TO_BBAND(rdata.upri);

		if (new_mp == NULL) {
			fpc->decr_count(rdata.remote_node, rdata);
			freemsg(mp);
			return;
		}
		new_mp->b_cont = mp;

		//
		// Fill M_PROTO msg with redirection destination mac address
		// and SAP
		//
		dl_udata = (dl_unitdata_req_t *)new_mp->b_rptr;
		dlsap = new_mp->b_rptr + sizeof (dl_unitdata_req_t);
		if (broadcast) {
			dl_udata->dl_dest_addr_length =
			    (unsigned)(rdata.bcstsize + sizeof (ushort_t));
			bcopy(rdata.bcstaddr, dlsap, rdata.bcstsize);
			bcopy(&rdata.ho_sap, dlsap + rdata.bcstsize,
			    sizeof (ushort_t));
			new_mp->b_wptr += (rdata.bcstsize + sizeof (ushort_t));
		} else {
			dl_udata->dl_dest_addr_length =
			    (unsigned)(rdata.size + sizeof (ushort_t));
			bcopy(rdata.rmacaddr, dlsap, rdata.size);
			bcopy(&rdata.ho_sap, dlsap + rdata.size,
			    sizeof (ushort_t));
			new_mp->b_wptr += (rdata.size + sizeof (ushort_t));
		}
	}

	//
	// Redirect the mblk to the physical interface.
	// Canput is not necessary for TCP because of its implicit flow
	// control. For efficiency, canput has been chosen over canputnext.
	//

	if (protocol == IPPROTO_TCP)
		putnext(rdata.q, new_mp);
	else {
		if (canput(rdata.q->q_next))
			putnext(rdata.q, new_mp);
		else {
			FP_DBG(("redirection stream full - dropping packet\n"));
			freemsg(new_mp);
		}
	}

	//
	// Drop the hold on the fp holder object. remove_path() can delete the
	// holder object now.
	//
	fpc->decr_count(rdata.remote_node, rdata);
}


//
// Extract packet header data from the mblk.  If there is packet framentation,
// then fragments are queued until the first fragment is received as only
// the first fragment contains the upper layer protocol data needed for
// selection of a physical interface. The type passed is in host order.
//
network::cid_action_t
pkt_redirector::extract_pkt_info(queue_t *q, mblk_t *bp, network::cid_t& cid,
    nodeid_t remote_node, ushort_t ho_type, uint32_t& spi)
{
	ipha_t			*ipha;
	uint16_t		*up;
	long			len, reqlen;
	ipha_t			tmp_hdr;
	int			iplen;
	network::cid_action_t action = network::CID_DROP;
	uint32_t		fragoffset_and_flags;
	bool			fragmented = false;
	bool			first_frag = false;
	fragment		*frag_holder = NULL;
	uchar_t 		*protocol_ptr;
	uint8_t			*pktid_ptr;


	//
	// Upper layer protocol data isn't needed for processing
	// non IP packets or IP broadcast packets so, for those,
	// return now. For normal IP, packet parsing continues on.
	//
	if (ho_type == ETHERTYPE_CLPRIVNET) {
		return (network::CID_DISPATCH);
	} else if (ho_type != ETHERTYPE_IP) {
		cmn_err(CE_NOTE,
		    "extract_pkt_info: drop packet - bad type = %x", ho_type);
		return (network::CID_DROP);
	}
	if (remote_node == BROADCAST_ID) {
		return (network::CID_DISPATCH);
	}

	// mblk message length
	len = bp->b_wptr - bp->b_rptr;

	//
	// Make a quick check of the upper layer protocol.  For UDP, the
	// info from the header isn't needed so a lot of processing
	// can be skipped. This shortcut is only used when fragment
	// processing is being bypassed.
	//
	if (len < IP_HDR_PROTOCOL_OFFSET)
		protocol_ptr = bp->b_cont->b_rptr + IP_HDR_PROTOCOL_OFFSET -
		    len;
	else
		protocol_ptr = bp->b_rptr + IP_HDR_PROTOCOL_OFFSET;


	if (*protocol_ptr == IPPROTO_UDP && use_frag_holder == 0) {
		if (len < IP_HDR_IDENT_OFFSET) {
			pktid_ptr = bp->b_cont->b_rptr +
			    IP_HDR_IDENT_OFFSET - len;
		} else {
			pktid_ptr =  bp->b_rptr + IP_HDR_IDENT_OFFSET;
		}

		cid.ci_fport = *(uint16_t *)pktid_ptr;
		goto done;
	}

	//
	// obtain IP packet header pointer
	//
	if ((len < IP_SIMPLE_HDR_LENGTH)) {
		if (!pullupmsg(bp, (size_t)IP_SIMPLE_HDR_LENGTH)) {
			cmn_err(CE_NOTE,
			    "extract_pkt_info: drop packet - pullup failed");
			return (network::CID_DROP);
		}
	}
	ipha = (ipha_t *)(bp->b_rptr);
	if (!OK_32PTR(ipha)) {
		bcopy(ipha, &tmp_hdr, (size_t)IP_SIMPLE_HDR_LENGTH);
		ipha = &tmp_hdr;
	}

	// We don't support IPV6.
	if (CL_IPH_HDR_VERSION(ipha) != IP_VERSION) {
		cmn_err(CE_NOTE,
		    "extract_pkt_info: drop packet - IPV6 unsupported");
		return (network::CID_DROP);
	}

	//
	// iplen contains the size of the IP header, len
	// contains the size of the mblk
	//
	iplen = (ipha->ipha_version_and_hdr_length & 0xF) << 2;
	len = bp->b_wptr - bp->b_rptr;

	if (use_frag_holder == 0)
		goto nofrag_holder;
	//
	// Check IP header to see if the packet is fragmented. Flags and
	// fragment offset fields are adjacent. The "more fragments" bit
	// (IPH_MF) is set for all fragments except the final fragment (but
	// the final frag will have a non-zero offset).  Fragment packets
	// cannot be dispatched until after the first fragment (which
	// contains the upper layer protocol info for interface selection).
	//
	fragoffset_and_flags = ntohs(ipha->ipha_fragment_offset_and_flags);
	if (fragoffset_and_flags & (IPH_MF | IPH_OFFSET))
		fragmented = true;
	if (!(fragoffset_and_flags & IPH_OFFSET))
		first_frag = true;

	if (fragmented) {
		ASSERT(*protocol_ptr == IPPROTO_UDP);

		//
		// fragment_compare() will either find a holder object for the
		// fragment in the list or create a new one. To prevent a
		// race with the timeout thread, fragment_compare() will return
		// with the fragment_mutex held. It is the responsiblity
		// of the calling thread to give up the fragment_mutex.
		//
		uint32_t srcaddr = ipha->ipha_src;
		uint32_t dstaddr = ipha->ipha_dst;
		uint16_t ident = ipha->ipha_ident;
		uint32_t proto = ipha->ipha_protocol;
		uint16_t offset = ((fragoffset_and_flags & IPH_OFFSET) << 3);
		uint16_t extent = ipha->ipha_length - (uint16_t)iplen;
		bool mf = (fragoffset_and_flags & IPH_MF? true: false);
		bool to_delete = false;

		frag_holder = fragment_managerp->fragment_compare(q, ident,
		    srcaddr, dstaddr, proto, offset, extent, mf, &to_delete);

		if (frag_holder == NULL) {
			// No unlocking here since frag holder was not
			// created successfully.
			cmn_err(CE_NOTE,
			    "extract_pkt_info: drop packet - no frag holder");
			return (network::CID_DROP);
		}
		ASSERT(frag_holder);

		//
		// Only the first fragment has the tcp/udp header info for
		// storage in fragment for subsequent adapter selection.
		// If valid_node exists the first fragment has been processed
		// already; otherwise nothing can be done until the first
		// fragment arrives.
		//
		if (!first_frag) {
			if (frag_holder->valid_node()) {
				remote_node = frag_holder->get_nodeid(cid);
				frag_holder->fragment_mutex.unlock();
				// It is ok to set the seq_num to -1 since
				// the tcp header always goes in the
				// first fragment.
				action = network::CID_DISPATCH;
			} else {
				frag_holder->add_mp_to_list(bp);
				frag_holder->fragment_mutex.unlock();
				action = network::CID_NOACTION;
			}

			// Delete frag holder if all fragments have
			// arrived and have arrived in order. Note that if
			// PDT is all 0's, valid_node() returns false. We
			// don't care, as long as all fragments are here,
			// the frag holder is done!
			//
			if (to_delete) {
				delete frag_holder;
			}

			return (action);
		}
	}

	//
	// Only unfragmented packet or first fragment processing proceeds
	// beyond here.
	//
	ASSERT(first_frag);

	// discard first fragment duplicates
	if ((fragmented) && (frag_holder->valid_node())) {
		frag_holder->fragment_mutex.unlock();
		cmn_err(CE_NOTE,
		    "extract_pkt_info: drop packet - duplicate fragment");
		return (network::CID_DROP);
	}

nofrag_holder:

	//
	// The required length of the message to be pulled is the
	// IP header size + protocol (TCP or UDP) header size.
	//
	if (ipha->ipha_protocol == IPPROTO_TCP) {
		reqlen = iplen + TCP_MIN_HEADER_LENGTH;
	} else if (ipha->ipha_protocol == IPPROTO_UDP) {
		reqlen = iplen + UDP_MIN_HEADER_LENGTH;
	} else if (ipha->ipha_protocol == IPPROTO_ICMP) {
		reqlen = iplen + ICMPH_SIZE;
	} else if (ipha->ipha_protocol == IPPROTO_AH) {
		reqlen = iplen + AH_BASELEN;
	} else if (ipha->ipha_protocol == IPPROTO_ESP) {
		reqlen = iplen + ESP_BASELEN;
	} else {
		// Other packets summarily dropped.
		return (network::CID_DROP);
	}
	if (len < reqlen) {
		if (!pullupmsg(bp, reqlen)) {
			if (fragmented)
				frag_holder->fragment_mutex.unlock();
			cmn_err(CE_NOTE,
			    "extract_pkt_info: drop packet - pullupmsg failed");
			return (network::CID_DROP);
		}
		//
		// new IP header pointer - cannot use old pointer values
		// after pullupmsg.
		//
		ipha = (ipha_t *)(bp->b_rptr);
		if (!OK_32PTR(ipha)) {
			bcopy(ipha, &tmp_hdr, (size_t)IP_SIMPLE_HDR_LENGTH);
			ipha = &tmp_hdr;
		}
	}

	//
	// Extract protocol header information (TCP, UDP, or ICMP)
	// The protocol type is returned in cid.ci_protocol so
	// xfer_packet() can optimize packet redirection for TCP.
	//
	up = (uint16_t *)(bp->b_rptr + iplen);
	if (ipha->ipha_protocol == IPPROTO_TCP) {
		cid.ci_lport = ntohs(*up++);
		cid.ci_fport = ntohs(*up);
		cid.ci_protocol = IPPROTO_TCP;
	} else if (ipha->ipha_protocol == IPPROTO_UDP) {
		cid.ci_lport = ntohs(*up++);
		cid.ci_fport = ntohs(*up) + (++port_delta % 10);
		cid.ci_protocol = IPPROTO_UDP;
	} else if (ipha->ipha_protocol == IPPROTO_ICMP) {
		cid.ci_lport = 0;
		cid.ci_fport = ++port_delta % 10;
		cid.ci_protocol = IPPROTO_ICMP;
	} else if (ipha->ipha_protocol == IPPROTO_AH) {
		uint32_t *spi_ptr =
		    (uint32_t *)(bp->b_rptr + iplen + AH_SPI_OFFSET);
		cid.ci_protocol = IPPROTO_AH;
		spi = ntohl(*spi_ptr);
	} else if (ipha->ipha_protocol == IPPROTO_ESP) {
		cid.ci_protocol = IPPROTO_ESP;
		spi = ntohl(*(uint32_t *)up);
	}

done:

	//
	// Update fragment info for first fragment.  Calling set_node
	// will allow fragment dispatching.
	//
	if (fragmented && use_frag_holder) {
		frag_holder->update_info(0, cid.ci_lport, cid.ci_fport);
		frag_holder->set_node(remote_node, bp);
	}

	return (network::CID_DISPATCH);
}

//
// This routine gets called from the service routine (clprivnet_rsrv)
// when the timer expires. All fragment holder objects must be processed.
//
void
pkt_redirector::fragment_timeout(queue_t *q, mblk_t *mp)
{

	fragment_managerp->fragment_timer_expired(q, mp);


}


pkt_redirector::pkt_redirector()
{

	nodeid_t nid = orb_conf::local_nodeid();

	FP_DBG(("pkt_redirector: constructor\n"));
	fragment_managerp = new fragment_manager(frag_hash_size,
	    redirect_frag_timeout_period);
	ASSERT(fragment_managerp);

	clprivnet_set_local_nodeid(nid);
}

pkt_redirector::~pkt_redirector()
{
	FP_DBG(("pkt_redirector: destructor\n"));
	delete fragment_managerp;
}


//
//
// IP Fragment Management for use by mcnet (muxq_impl.cc) and the IP packet
// redirector (above) for clprivnet support.
//



fragment_manager::fragment_manager(uint32_t hash_size,
    time_t timeout_period) :
	fragment_hash_size(hash_size),
	fragment_timer_running(false),
	fragment_timeout_period(timeout_period)
{
	FP_DBG(("fragment_manager: constructor\n"));
	ASSERT(fragment_hash_size > 0);
	ASSERT(fragment_timeout_period > 0);
	//
	// An array of fragment holder lists. Each <srcaddr, ident> pair
	// is uniquely mapped to one of the lists by get_frag_holder_list().
	//
	fragment_list_array = new fragment_list_t[fragment_hash_size];

	fragment_timer_mp = mi_timer_alloc((long)0);
	ASSERT(fragment_timer_mp);

}


//
// There is an implicit assumption that there
// aren't any live connections around.
//
fragment_manager::~fragment_manager()
{
	FP_DBG(("fragment_manager: destructor\n"));

	/*lint -e1740 */
	mi_timer_free(fragment_timer_mp);

	fragment *frag_holder;
	fragment_list_lock.lock();
	for (uint_t i = 0; i < fragment_hash_size; i++) {
		fragment_list_t& fragment_list =
		    fragment_list_array[i];

		for (fragment_list.atfirst();
		    (frag_holder = fragment_list.get_current()) != NULL;
		    fragment_list.advance()) {
			delete frag_holder;
		}
	}
	delete [] fragment_list_array;
	fragment_list_lock.unlock();

}

//
// We go through the hash of fragment holder objects, check if ip
// reassembly time has expired for each holder object. If it has not expired,
// allocate_more_time is set to true so that timer is restarted at
// the end. If reassembly time has exceeded for a holder object, we
// get the fragment_mutex of the holder object, erase it from the
// fragment list and then delete the object. fragment_mutex of the holder
// object ensures that an incoming fragment of the same packet doesn't
// get its hands on a holder object which is going to be deleted.
//
void
fragment_manager::fragment_timer_expired(queue_t *q, mblk_t *mp)
{
	bool		allocate_more_time = false;
	fragment	*frag_holder;

	FP_DBG(("fragment_manager::fragment_timer_expired()\n"));

	ASSERT(mp == fragment_timer_mp);

	if (!mi_timer_valid(mp)) {
		return;
	}

	fragment_list_lock.lock();
	ASSERT(fragment_timer_running);

	for (uint_t i = 0; i < fragment_hash_size; i++) {
		fragment_list_t& fragment_list =
		    fragment_list_array[i];

		fragment_list.atfirst();
		while ((frag_holder = fragment_list.get_current())
		    != NULL) {
			fragment_list.advance();
			time_t current_time = ddi_get_time();
			if ((current_time - frag_holder->start_time) <
				fragment_timeout_period) {
				// It is not yet deleting time so
				// set allocate_more_time to get the
				// timer restarted.
				allocate_more_time = true;
			} else {
				// Time to live has expired for this
				// fragment holder. Delete it.
				frag_holder->fragment_mutex.lock();
				(void) fragment_list.erase(frag_holder);
				frag_holder->fragment_mutex.unlock();
				FP_DBG(("timer expired:  Deleting a frag\n"));
				delete frag_holder;
			}
		}
	}
	fragment_timer_running = allocate_more_time;
	if (!allocate_more_time) {
		for (uint_t i = 0; i < fragment_hash_size; i++) {
			fragment_list_array[i].atfirst();
			ASSERT(!fragment_list_array[i].get_current());
		}
	}
	fragment_list_lock.unlock();
	if (allocate_more_time) {
		mi_timer(q, mp,
		    ((ulong_t)(fragment_timeout_period * 1000)) >> 1);
		FP_DBG(("fragment timer is restarted.\n"));
	} else {
		FP_DBG(("fragment timer is not restarted.\n"));
	}

}


fragment::fragment(uint32_t id,
    const in6_addr_t& src, const in6_addr_t& dst,
    uint16_t off, uint16_t ext) : _DList::ListElem(this)
{
	ident = id;
	srcaddr = src;
	dstaddr = dst;
	proto = 0;			// to be filled by update_info()
	nodeid = NODEID_UNKNOWN;
	mp_list = NULL;
	//
	// frag_offset is essentially the length of the continuous chunk
	// of data seen from offset 0. Initialize based on if this
	// fragment starts from offset 0 or not.
	//
	frag_offset = (off == 0? ext: 0);
	start_time = ddi_get_time();
}

fragment::~fragment()
{
	mblk_t *temp_mp;
	while (mp_list) {
		temp_mp = mp_list->b_next;
		mp_list->b_next = NULL;
		freemsg(mp_list);
		mp_list = temp_mp;
	}
}

// The first fragment at offset 0 has not yet arrived. So just add the
// mp to list of other fragments which have come in and wait for the
// fragment at offset 0 to come in.
void
fragment::add_mp_to_list(mblk_t *mp)
{
	ASSERT(fragment_mutex.lock_held());
	if (mp_list) {
		mblk_t *tmp_list = mp_list;
		while (tmp_list->b_next)
			tmp_list = tmp_list->b_next;
		tmp_list->b_next = mp;
	} else {
		mp_list = mp;
	}
}

//
// This function is called after the first ip fragment at offset 0
// comes in.  Determining the target node may require the first fragment.
// If the nodeid field is set this means the first fragment has in
// fact arrived. If other fragments have already arrived, they are linked
// up through the b_next field of mp.
//
void
fragment::set_node(nodeid_t n, mblk_t *mp)
{
	nodeid = n;
	ASSERT(mp->b_next == NULL);
	if (mp_list)
		mp->b_next = mp_list;
	mp_list = NULL;
	fragment_mutex.unlock();
}

// This routine goes through the fragment_list and sees if a holder
// object has already been created for this ip packet identified by the
// ip id, ip srcaddr, ip destaddr and protocol fields. If one is found,
// this routine returns the holder object with the fragment_mutex of the
// holder object held.
// If there isn't a holder object in fragment_list for this fragment, then
// a new one is initialized and returned with fragment_mutex held. It is
// necessary to return with the fragment_mutex held to prevent frag_timer()
// from deleting the holder object when the holder object is being accessed.
// frag_timer_running is used to re-start a timer. It is protected by the
// fragment_list_lock.
//
// Early fragment holder delete: The offset, extent, mf, and tbd parameters
// are used to decide if it's time to delete the frag holder. If all
// fragments have arrived in order (the common case), then fragment holder can
// be deleted as soon as the last fragment arrives. In that case,
// fragment_compare() removes the fragment holder from the global list, and sets
// tbd to true. This way, the caller has exclusive access to the fragment
// holder object, and after it's done, is expected to delete it.
//
// We don't optimize fragment holder deletion for out-of-order arrival of
// fragments, since that's not a common case. In those cases, fragment holder
// is kept around even though all fragments may have arrived. This timer
// thread is relied on to clean up these fragment holders.
//
// The distinction is made between the above two cases, so that we don't
// have to keep track of "holes" in datagram, a generally uncommon case
// that wouldn't justify the added state keeping.
//
fragment *
fragment_manager::fragment_compare(queue_t *q, uint16_t id, uint32_t src,
uint32_t dst, uint32_t, uint16_t offset, uint16_t extent, bool mf,
bool* tbd)
{
	in6_addr_t src6, dst6;

	IN6_IPADDR_TO_V4MAPPED(src, &src6);
	IN6_IPADDR_TO_V4MAPPED(dst, &dst6);

	return (fragment_compare_v6(q, (uint32_t)id, src6, dst6,
	    offset, extent, mf, tbd));
}

fragment *
fragment_manager::fragment_compare_v6(queue_t *q, uint32_t id,
    const in6_addr_t& src, const in6_addr_t& dst,
    uint16_t offset, uint16_t extent, bool mf, bool* tbd)
{
	fragment *frag_holder;
	fragment_list_t& fragment_list = get_fragment_list(src, id);

	FP_DBG(("fragment_manager::fragment_compare()\n"));

	*tbd = false;

	fragment_list_lock.lock();
	for (fragment_list.atfirst();
	    (frag_holder = fragment_list.get_current()) != NULL;
	    fragment_list.advance()) {
		if (frag_holder->compare(id, src, dst)) {
			// Found a holder object corresponding to the
			// ip fragment.
			frag_holder->fragment_mutex.lock();
			if (fragment_timer_running == 0) {
				// Start the timer.
				fragment_timer_running = true;
				FP_DBG(("  Started timer \n"));
				mi_timer(q, fragment_timer_mp,
				    (fragment_timeout_period * 1000));
			}

			if (frag_holder->extend(offset, extent) && !mf) {
				//
				// Last fragment that "completes" the
				// datagram. Indicate to the caller that
				// it's now ok to free the frag holder.
				//
				// By removing from the list, it's
				// possible that some duplicated fragments
				// can result in another frag holder
				// getting created. Those are marginal
				// cases, and the un-wanted frag holder
				// will be garbage collected by the
				// frag_timer() thread.
				//
				FP_DBG(("  Early frag holder removal\n"));
				(void) fragment_list.erase(frag_holder);
				*tbd = true;
			}

			fragment_list_lock.unlock();
			return (frag_holder);
		}
	}

	// Create a new holder object.
	frag_holder = new (os::NO_SLEEP) fragment(id, src, dst,
	    offset, extent);
	if (frag_holder) {
		FP_DBG(("  Created holder id = %d\n", id));
		fragment_list.append(frag_holder);
		frag_holder->fragment_mutex.lock();
		if (!fragment_timer_running) {
			// Start the timer.
			fragment_timer_running = true;
			mi_timer(q, fragment_timer_mp,
			    (fragment_timeout_period * 1000));
		}
	}
	fragment_list_lock.unlock();
	return (frag_holder);
}

#endif // _KERNEL
