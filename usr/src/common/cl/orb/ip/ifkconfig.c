/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ifkconfig.c	1.56	09/02/18 SMI"

/*
 *  this file contains kernel callable version of ifconfig.
 *
 *  there are a lot of lint overrides of the form:
 *	iocb.ic_cmd = IF_UNITSEL;	lint !e737
 *  This is because the macro for IF_UNITSEL and other commands do a cast
 *  that causes a loss of sign in promotion from int to unsigned int.
 *  This is safe as we are dealing with small integers
 */

#include <sys/kmem.h>
#include <sys/cl_net.h>
#include <orb/ip/vlan.h>
#include <orb/ip/ifkconfig.h>
#include <orb/ip/udi_util.h>
#include <sys/dbg_printf.h>

static int clean_stream_modules(udi_handle_t uh, const char *ifname);

static void preunplumb_cleanup(udi_handle_t, const char *ifname);

static int plumb_device(udi_handle_t uh_udp, const char *ifname,
	    const ifk_interface_t *in);

static int setandclearifflags(udi_handle_t uh, const char *ifname,
	    unsigned short set, unsigned short clear);

static int unplumb_device(udi_handle_t uh_udp, const char *ifname);



#define	ARP		"arp"
#define	IP		"ip"
#define	UDP		"udp"
#define	UDP6		"udp6"

#define	CLHBSNDR	"clhbsndr"
#define	CLPRIVNET	"clprivnet0"
#define	CLPRIVNET_DEV	"clprivnet"
#define	SCI_DEV		"scid"

#define	MAX_PLUMB_ATTEMPTS	3

/* Commands that the current v6 handling doesn't care about */
#define	IFK_V6_DONTCARE \
	(IFK_NETMASK|IFK_IPADDR|IFK_MTU|IFK_BRDCST|IFK_ARP|IFK_NOARP)

static int ifk_debug = 0;

/* Control if we must disable any v6 plumbing on the transport */
/* Disable by default - 6306113 */
static int ifk_disable_v6 = 1;

/*
 * Debug buffers for ifkconfig. dprintf() is used for development and
 * tracing. The ifk_debug should be set for dprintf() to work. eprintf()
 * is used to write error info. Callers of ifkconfig() should log to the
 * console if syslog messages are desired.
 */

#if defined(DBGBUFS) && !defined(NO_FOO_DBGBUF)
CREATE_DBGBUF(ifk_dbg, 8192, UNODE_DBGOUT_NONE);
#define	dprintf(x) if (ifk_debug) print_to_dbgbuf x
#define	eprintf(x) print_to_dbgbuf x
#else
#define	dprintf(x) if (ifk_debug) printf x
#define	eprintf(x) printf x
#endif

/*
 * int ifkconfig(const ifk_interface_t *in, int cmd, int *errnum)
 *
 * ifkconfig is a kernel routine that peforms the same operations
 * as the ifconfig command in the user land.
 *
 * If ifkconfig is called to only plumb the adapter without also
 * bringing it up, ifkconfig will not be able to detect is the
 * adapter is physically nonexistent. See detailed comments inside
 * the routine.
 */
int
ifkconfig_v4(const ifk_interface_t *in, int cmd, int *errnum)
{
	udi_handle_t	uh_ip, uh_udp;

	unsigned short	flags_to_be_set		= 0;
	unsigned short	flags_to_be_cleared	= 0;
	int		error;

	char		ifname[LIFNAMSIZ];


	(void) sprintf(ifname, "%s%d", in->ifk_device_name,
	    VLAN_PPA(in->ifk_device_instance, in->ifk_vlan_id));
	if (in->ifk_vlan_device_name != NULL) {
		ASSERT(strcmp(in->ifk_vlan_device_name, ifname) == 0);
	}

	if (in->ifk_logical_unit != IFK_PHYSICAL) {
		(void) sprintf(ifname + strlen(ifname), ":%d",
			in->ifk_logical_unit);
	}

	eprintf((&ifk_dbg, "ifkconfig %s cmd %d\n", ifname, cmd));


	/*
	 * Open /dev/ip
	 */

	if ((error = udi_open(IP, -1, &uh_ip)) != 0) {
		eprintf((&ifk_dbg, "udi_open of IP failed error %d\n",
		    error));
		*errnum = error;
		/* skip closing mux fd since it failed */
		return (-1);
	}

	/*
	 * Open /dev/udp
	 */

	if ((error = udi_open(UDP, -1, &uh_udp)) != 0) {
		eprintf((&ifk_dbg, "udi_open of UDP failed error %d\n",
		    error));
		*errnum = error;
		/* close ip mux_fd opened earlier */
		(void) udi_close(uh_ip);
		return (-1);
	}

	if (cmd & (IFK_PLUMB|IFK_CREATE)) {
		if (in->ifk_logical_unit == IFK_PHYSICAL) {
			if ((error = plumb_device(uh_udp, ifname, in)) != 0) {
				*errnum = error;
				(void) udi_close(uh_udp);
				(void) udi_close(uh_ip);
				return (-1);
			}
		} else {
			if ((error = plumb_logical_if(uh_ip, ifname)) != 0) {
				dprintf((&ifk_dbg, "plumb_logical_if of"
				    " %s failed with %d\n",
				    ifname, error));
				*errnum = error;
				(void) udi_close(uh_udp);
				(void) udi_close(uh_ip);
				return (-1);
			}
		}
	} else if (cmd & (IFK_UNPLUMB|IFK_DESTROY)) {
		dprintf((&ifk_dbg, "unplumbing interface %s\n", ifname));
		if (in->ifk_logical_unit != IFK_PHYSICAL) {
			if ((error = unplumb_logical_if(uh_ip, ifname)) != 0) {
				dprintf((&ifk_dbg, "unplumb_logical_if"
				    " of %s failed with %d\n",
				    ifname, error));
				*errnum = error;
				(void) udi_close(uh_udp);
				(void) udi_close(uh_ip);
				return (-1);
			}

			(void) udi_close(uh_udp);
			(void) udi_close(uh_ip);
			return (0);
		}
		/*
		 * First cleanup the device
		 */
		preunplumb_cleanup(uh_ip, ifname);

		/*
		 * Finally do the unplumb
		 */
		if ((error = unplumb_device(uh_udp, ifname)) != 0) {
			*errnum = error;
			(void) udi_close(uh_udp);
			(void) udi_close(uh_ip);
			return (-1);
		}

		(void) udi_close(uh_udp);
		(void) udi_close(uh_ip);
		return (0);
	}
	/*
	 * Set the IPADDR and NETMASK before bringing up the interface.
	 *
	 * If setting the address and not the mask, clear the netmask value
	 * and the kernel will assign the default.
	 * If setting both, set the mask first, so the broadcast address
	 * can be interpreted correctly.
	 * If setting the mask only,  set the new netmask value and leave
	 * the address the same.
	 *
	 */

	if (cmd & (IFK_NETMASK | IFK_IPADDR)) {
		struct in_addr value;
		struct in_addr o_value;
		struct lifreq *t_lifr;

		/* save current netmask value */
		t_lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
		(void) strncpy((caddr_t)t_lifr->lifr_name, ifname,
		    sizeof (t_lifr->lifr_name) - 1);
		if ((error = getifnetmask(uh_ip, t_lifr)) != 0) {
			eprintf((&ifk_dbg, "getifnetmask failed, error %d\n",
			    error));
			kmem_free(t_lifr, sizeof (struct lifreq));
			*errnum = error;
			goto error_exit;
		}
		o_value.s_addr =
		    ((struct sockaddr_in *)&t_lifr->lifr_addr)->sin_addr.s_addr;
		kmem_free(t_lifr, sizeof (struct lifreq));


		if (cmd & IFK_NETMASK) {
			bcopy(&(in->ifk_netmask), &value, sizeof (value));
		} else {
			/*
			 * we're setting the IP address but not the
			 * netmask - set the netmask to 0 and the kernel
			 * will pick a suitable default
			 */
			bzero(&value, sizeof (value));
		}


		if ((error = setnetmask(uh_ip, ifname, &value)) != 0) {
			eprintf((&ifk_dbg, "setnetmask failed, error %d\n",
			    error));
			*errnum = error;
			goto error_exit;
		}

		/* set ipaddress */
		if ((cmd & IFK_IPADDR) &&
		    (error = setipaddr(uh_ip, ifname, &in->ifk_ipaddr)) != 0) {
			eprintf((&ifk_dbg, "setipaddr failed, error %d\n",
			    error));
			*errnum = error;
			/* Restore netmask */
			if ((error = setnetmask(uh_ip, ifname, &o_value)) \
			    != 0) {
				eprintf((&ifk_dbg, "restore netmask failed, "
				    "error %d\n", error));
				*errnum = error;
			}
			goto error_exit;
		}
	}

	/*
	 * Handle various flags by setting bits in set or
	 * clear masks...
	 */

	if (cmd & IFK_UP) {
		flags_to_be_set |= IFF_UP;
	} else if (cmd & IFK_DOWN) {
		flags_to_be_cleared |= IFF_UP;
	}

	if (cmd & IFK_PRIVATE) {
		flags_to_be_set |= IFF_PRIVATE;
	} else if (cmd & IFK_NOTPRIVATE) {
		flags_to_be_cleared |= IFF_PRIVATE;
	}

	if (cmd & IFK_ARP) {
		flags_to_be_cleared |= IFF_NOARP;
	} else if (cmd & IFK_NOARP) {
		flags_to_be_set |= IFF_NOARP;
	}

	if (flags_to_be_set || flags_to_be_cleared) {
		if ((error = setandclearifflags(uh_ip, ifname,
		    flags_to_be_set, flags_to_be_cleared)) != 0) {
			*errnum = error;
			dprintf((&ifk_dbg, "setandclearflags failed"
				" error %d\n", error));

			goto error_exit;
		}
	}


	if (cmd & IFK_MTU) {
		if ((error = setifmtu(uh_ip, ifname, in->ifk_mtu)) != 0) {
			*errnum = error;
			goto error_exit;
		}
	}


	if (cmd & IFK_BRDCST) {
		if ((error = setbroadcast(uh_ip, ifname, &in->ifk_broadcast))
		    != 0) {
			*errnum = error;
			goto error_exit;
		}
	}

	(void) udi_close(uh_udp);
	(void) udi_close(uh_ip);
	return (0);

error_exit:
	/*
	 * Cleanup and exit. If we did plumbing during this call to ifkconfig,
	 * we should unplumb the device as well. Ignore the errors returned
	 * from these calls and return the real failure error.
	 */
	if (cmd & IFK_PLUMB) {
		dprintf((&ifk_dbg, "Calling preunplumb cleanup for"
		    " device %s.\n", ifname));
		preunplumb_cleanup(uh_ip, ifname);

		dprintf((&ifk_dbg, "Calling unplumb cleanup for device"
		    " %s.\n", ifname));
		(void) unplumb_device(uh_udp, ifname);
	}

	(void) udi_close(uh_udp);
	(void) udi_close(uh_ip);
	return (-1);
}

static int
plumb_device_v6(udi_handle_t uh_udp, const char *ifname,
    const ifk_interface_t *in)
{
	udi_handle_t	uh;
	int		ip_muxid;
	int		error;
	major_t		ndev;
	uint_t 		ppa;
	struct lifreq	*lifr;
	char		modname[FMNAMESZ + 1];

	ndev = ddi_name_to_major((char *)in->ifk_device_name);
	ppa = (uint_t)VLAN_PPA(in->ifk_device_instance, in->ifk_vlan_id);

	dprintf((&ifk_dbg, "plumb_device_v6: "
	    "ifname %s, ndev %d, ppa %d\n",
	    ifname, ndev, ppa));
	dprintf((&ifk_dbg, "plumb_device_v6: before open dev\n"));

	/*
	 * Open /dev/<ifname>. First try style-1 DLPI open, i.e.
	 * /dev/<device><instance>, such as /dev/qfe2. Note that
	 * for vlan devices, the instance is the ppa, which is,
	 * (vlan_id * 1000) + device_instance.
	 */

	if ((error = udi_open(in->ifk_device_name,
	    ppa, &uh)) != 0) {
		eprintf((&ifk_dbg, "plumb_device_v6: udi_open /dev/%s%d"
		    " failed error %d\n", in->ifk_device_name,
		    ppa, error));

		/*
		 * If the style-1 open fails, try /dev/<device> (style-2).
		 * Note that certain drivers, like ibd, supports
		 * style-1 more reliably. But older drivers like qfe
		 * supports style-2 only.
		 */
		error = udi_open(in->ifk_device_name, -1, &uh);

		eprintf((&ifk_dbg, "  retry udi_open /dev/%s"
		    " returned %d\n", in->ifk_device_name, error));

		if (error != 0)
			return (error);
	}

	dprintf((&ifk_dbg, "plumb_device_v6: after open dev\n"));

	/* Remove any extraneous STREAMS modules in our path. */
	(void) clean_stream_modules(uh, ifname);

	/*
	 * Push the clhbsndr module on the device. To be effective this
	 * module must be pushed directly above the device.
	 *
	 * Don't push on the clprivnet pseudo device
	 */
	if ((strcmp(ifname, CLPRIVNET) != 0) &&
	    ((error = udi_push(uh, CLHBSNDR)) != 0)) {
		eprintf((&ifk_dbg, "plumb_device_v6: udi_push %s failed,"
		    " error %d\n", CLHBSNDR, error));
		(void) udi_close(uh);
		return (error);
	}

	if ((error = udi_push(uh, IP)) != 0) {
		eprintf((&ifk_dbg, "plumb_device_v6: udi_push IP failed,"
		    " error %d\n", error));
		(void) udi_close(uh);
		return (error);
	}

	dprintf((&ifk_dbg, "plumb_device_v6: after push ip\n"));

	lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);

	/* Get existing flags */
	lifr->lifr_name[0] = '\0';
	if ((error = getifflags(uh, lifr)) != 0) {
		eprintf((&ifk_dbg, "plumb_device_v6: getifflags IP failed,"
		    " error %d\n", error));
		(void) udi_close(uh);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	/* set the IF_IPV* flag */
	lifr->lifr_flags |= IFF_IPV6;
	lifr->lifr_flags &= ~(IFF_BROADCAST | IFF_IPV4);

	(void) strncpy((caddr_t)lifr->lifr_name, ifname,
	    sizeof (lifr->lifr_name)-1);
	lifr->lifr_ppa = ppa;
	if ((error = setifname(uh, lifr)) != 0) {
		eprintf((&ifk_dbg, "plumb_device_v6: setifname IP failed,"
		    " error %d\n", error));
		(void) udi_close(uh);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	dprintf((&ifk_dbg, "plumb_device_v6: "
	    "after set ifname and unit\n"));

	/*
	 * Remove all modules on udp before pushing ARP
	 */
	while (udi_ioctl(uh_udp, I_LOOK, (intptr_t)modname) == 0) {
		dprintf((&ifk_dbg, "plumb_device_v6: "
		    "pop %s off /dev/udp6\n", modname));

		if ((error = udi_ioctl(uh_udp, I_POP, (long)NULL)) != 0) {
			eprintf((&ifk_dbg, "plumb_device_v6: "
			    "Can't pop %s off /dev/udp6\n", modname));
			(void) udi_close(uh);
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}
	}

	/*
	 * Push ARP to set muxid properly. Note that even though IPv6
	 * does not use ARP, this step is necessary to set up the stream
	 * properly.
	 */
	if ((error = udi_push(uh_udp, ARP)) != 0) {
		eprintf((&ifk_dbg, "plumb_device_v6: udi_push ARP failed,"
		    " error %d\n", error));
		(void) udi_close(uh);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	dprintf((&ifk_dbg, "plumb_device_v6: after pushing ARP\n"));

	if ((error = udi_plink(uh_udp, uh, &ip_muxid)) != 0) {
		eprintf((&ifk_dbg, "plumb_device_v6: udi_plink UDP6 failed,"
		    " error %d\n", error));
		(void) udi_close(uh);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	dprintf((&ifk_dbg, "plumb_device_v6: "
	    "after PLINK with UDP6\n"));

	(void) udi_close(uh);
	kmem_free(lifr, sizeof (struct lifreq));

	return (0);
}

static int
setandclearifflags_v6(const char *ifname,
    unsigned short set, unsigned short clear)
{
	udi_handle_t uh_sock;
	struct lifreq *lifr;
	struct strioctl iocb;
	int rc;

	if ((rc = udi_open_inet6_sock(&uh_sock)) != 0) {
		eprintf((&ifk_dbg, "setandclearifflags_v6:"
		    "udi_open_inet6_sock failed error %d\n", rc));
		return (rc);
	}

	lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);

	(void) strncpy((caddr_t)lifr->lifr_name, ifname,
	    sizeof (lifr->lifr_name)-1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';

	iocb.ic_cmd = SIOCGLIFFLAGS;
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifr);
	iocb.ic_dp = (caddr_t)lifr;

	if ((rc = udi_ioctl(uh_sock, I_STR, (intptr_t)&iocb)) != 0) {
		eprintf((&ifk_dbg, "setandclearifflags_v6:"
		    "udi_ioctl SIOCGLIFFLAGS "
		    "failed on %s: error %d\n", ifname, rc));
		(void) udi_close(uh_sock);
		kmem_free(lifr, sizeof (struct lifreq));
		return (rc);
	}

	lifr->lifr_flags &= ~((uint64_t)clear);
	lifr->lifr_flags |= ((uint64_t)set);

	iocb.ic_cmd = SIOCSLIFFLAGS;	/*lint !e737 */

	if ((rc = udi_ioctl(uh_sock, I_STR, (intptr_t)&iocb)) != 0) {
		eprintf((&ifk_dbg, "setandclearifflags_v6:"
		    "udi_ioctl SIOCSLIFFLAGS "
		    "failed on %s: error %d\n", ifname, rc));
		(void) udi_close(uh_sock);
		kmem_free(lifr, sizeof (struct lifreq));
		return (rc);
	}

	(void) udi_close(uh_sock);
	kmem_free(lifr, sizeof (struct lifreq));
	return (0);
}

static int
unplumb_device_v6(udi_handle_t uh_udp, const char *ifname)
{
	udi_handle_t	uh_sock;
	int		arp_muxid, ip_muxid;
	int		error;
	struct lifreq	*lifr;

	if ((error = udi_open_inet6_sock(&uh_sock)) != NULL) {
		eprintf((&ifk_dbg, "unplumb_device_v6: "
		    "udi_open_inet6_sock failed error %d\n", error));
		return (error);
	}

	lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
	(void) strncpy((caddr_t)lifr->lifr_name, ifname,
	    sizeof (lifr->lifr_name)-1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';

	if ((error = getifmuxid(uh_sock, lifr)) != 0) {
		eprintf((&ifk_dbg, "unplumb_device_v6: "
		    "getifmuxid failed, error %d\n", error));
		(void) udi_close(uh_sock);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	arp_muxid = lifr->lifr_arp_muxid;
	if (arp_muxid != -1 && arp_muxid != 0) {
		if ((error = udi_punlink(uh_udp, arp_muxid)) != 0) {
			eprintf((&ifk_dbg, "unplumb_device_v6: "
			    "unplink failed for arp\n"));
			(void) udi_close(uh_sock);
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}
	}

	ip_muxid = lifr->lifr_ip_muxid;
	if ((error = udi_punlink(uh_udp, ip_muxid)) != 0) {
		eprintf((&ifk_dbg, "unplumb_device_v6: "
		    "unplink failed for ip\n"));
		(void) udi_close(uh_sock);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	(void) udi_close(uh_sock);
	kmem_free(lifr, sizeof (struct lifreq));
	return (0);
}


int
ifkconfig_v6(const ifk_interface_t *in, int cmd, int *errnum)
{
	udi_handle_t	uh_udp;

	unsigned short	flags_to_be_set		= 0;
	unsigned short	flags_to_be_cleared	= 0;
	char		ifname[LIFNAMSIZ];
	int		error;

	/*
	 * No v6 support for clprivnet at this point.
	 * Also, since we only want a v6 instance to be on the transport
	 * adapter, we don't really care about logical adapters.
	 *
	 * SCI support is not available yet, since the driver assumes IPv4 only
	 * and it must be enhanced first before we can support it.
	 */
	if (strcmp(in->ifk_device_name, CLPRIVNET_DEV) == 0 ||
	    strcmp(in->ifk_device_name, SCI_DEV) == 0 ||
	    in->ifk_logical_unit != IFK_PHYSICAL)
		return (0);

	/*
	 * If cmd has only v4-relevant flags but not the v6 operations we
	 * support (plumb/unplumb/up/down), return here.
	 * If cmd has more than these flags, we continue, but these flags
	 * are ignored from here on.
	 */
	if ((cmd &= ~IFK_V6_DONTCARE) == 0)
		return (0);

	(void) sprintf(ifname, "%s%d", in->ifk_device_name,
	    VLAN_PPA(in->ifk_device_instance, in->ifk_vlan_id));
	if (in->ifk_vlan_device_name != NULL) {
		ASSERT(strcmp(in->ifk_vlan_device_name, ifname) == 0);
	}

	eprintf((&ifk_dbg, "ifkconfig_v6 %s cmd %d\n", ifname, cmd));

	/*
	 * Open /dev/udp6
	 */

	if ((error = udi_open(UDP6, -1, &uh_udp)) != 0) {
		eprintf((&ifk_dbg, "udi_open of UDP6 failed error %d\n",
		    error));
		*errnum = error;
		return (-1);
	}

	if (cmd & (IFK_PLUMB|IFK_CREATE)) {
		if ((error = plumb_device_v6(uh_udp, ifname, in)) != 0) {
			*errnum = error;
			(void) udi_close(uh_udp);
			return (-1);
		}

	} else if (cmd & (IFK_UNPLUMB|IFK_DESTROY)) {
		dprintf((&ifk_dbg, "unplumbing v6 instance of "
		    "interface %s\n", ifname));

		/*
		 * Finally do the unplumb
		 */
		if ((error = unplumb_device_v6(uh_udp, ifname)) != 0) {
			*errnum = error;
			(void) udi_close(uh_udp);
			/*
			 * ENXIO indicate no v6 interface configured
			 * Consider it a success.
			 */
			if (error == ENXIO) {
				return (0);
			} else {
				return (-1);
			}
		}

		(void) udi_close(uh_udp);
		return (0);
	}

	/*
	 * Handle various flags by setting bits in set or
	 * clear masks...
	 */

	if (cmd & IFK_UP) {
		flags_to_be_set |= IFF_UP;
	} else if (cmd & IFK_DOWN) {
		flags_to_be_cleared |= IFF_UP;
	}

	if (cmd & IFK_PRIVATE) {
		flags_to_be_set |= IFF_PRIVATE;
	} else if (cmd & IFK_NOTPRIVATE) {
		flags_to_be_cleared |= IFF_PRIVATE;
	}

	if (flags_to_be_set || flags_to_be_cleared) {
		if ((error = setandclearifflags_v6(ifname,
		    flags_to_be_set, flags_to_be_cleared)) != 0) {
			*errnum = error;
			dprintf((&ifk_dbg, "setandclearflags_v6 failed"
				" error %d\n", error));

			goto error_exit;
		}
	}

	(void) udi_close(uh_udp);
	return (0);

error_exit:
	/*
	 * Cleanup and exit. If we did plumbing during this call to ifkconfig,
	 * we should unplumb the device as well. Ignore the errors returned
	 * from these calls and return the real failure error.
	 */
	if (cmd & IFK_PLUMB) {
		dprintf((&ifk_dbg, "Calling unplumb cleanup for device"
		    " %s.\n", ifname));

		(void) unplumb_device_v6(uh_udp, ifname);
	}

	(void) udi_close(uh_udp);

	return (-1);
}

int
ifkconfig(const ifk_interface_t *in, int cmd, int *errnum)
{
	int rc;

	if ((rc = ifkconfig_v4(in, cmd, errnum)) != 0)
		return (rc);

	/*
	 * Configure a v6 instance alongside v4. This enables v6 support
	 * for Scalable Services, where a transport adapter must be
	 * configured to receive v6 packets forwarded to this node by the
	 * GIF node.
	 *
	 * If V6 plumb fails, do not return error since the transport can
	 * function with v4 only and not every cluster uses scalable
	 * services anyway. Just warn about the failure.
	 *
	 * Allow backdoor to disable any v6 actions.
	 */
	if (ifk_disable_v6) {
		eprintf((&ifk_dbg,
		    "ifkconfig_v6 not run: ifk_disable_v6 set\n"));
	} else {
		if ((rc = ifkconfig_v6(in, cmd, errnum)) != 0) {
			if (cmd & (IFK_PLUMB | IFK_CREATE)) {
			/*
			 * EEXIST indicates an interface configured.
			 * Return error so that the caller unplumbs and
			 * configures the interface.
			 */
				if (*errnum == EEXIST) {
					return (-1);
				}
				cmn_err(CE_WARN, "cl_comm: IPv6 "
				    "configuration failed on "
				    "interface %s%d. Cannot support IPv6 "
				    "for scalable service: error %d",
				    in->ifk_device_name,
				    VLAN_PPA(in->ifk_device_instance,
				    in->ifk_vlan_id), *errnum);
			}
		}
	}
	return (0);
}


/*
 * preunplumb_cleanup(vnode_t *vp, const char *ifname)
 *
 * This routine is called to bring an interface down and to dissociate the IP
 * address that has been associated with it erstwhile, before plumbing the
 * interface.
 *
 * No return values.
 *
 * Errors are ignored.
 */
static void
preunplumb_cleanup(udi_handle_t uh, const char *ifname)
{
	int	error;
	struct	in_addr value;

	bzero(&value, sizeof (value));

	/*
	 *  first disable interface, ignore errors.
	 */
	if ((error = setandclearifflags(uh, ifname,
					0, IFF_UP)) != 0) {
		dprintf((&ifk_dbg, "cleanup: setandclearflags %s failed,"
			" error %d ignored.\n", ifname, error));
	}

	/*
	 * set IP address to 0, ignore errors
	 */

	if ((error = setipaddr(uh, ifname, & value)) != 0) {
		dprintf((&ifk_dbg, "cleanup: setipaddr %s failed,"
			" error %d ignored.\n", ifname, error));
	}

}

static int
plumb_device(udi_handle_t uh_udp, const char *ifname,
    const ifk_interface_t *in)
{
	udi_handle_t	uh, new_uh;

	int 		arp_muxid;
	int		ip_muxid;

	int		error;

	major_t		ndev;
	uint_t 		ppa;
	int		ip_configure_needed = 1;
	int		plumb_attempts = 1;
	struct strioctl iocb;
	bband_info_t	bbinfo;

	/*
	 * Do not allocate lifr statically.
	 * lifreq structure is very big and if allocated statically
	 * consumes a lot of space on stack.
	 */
	struct lifreq	*lifr;


	ndev = ddi_name_to_major((char *)in->ifk_device_name);
	ppa = (uint_t)VLAN_PPA(in->ifk_device_instance, in->ifk_vlan_id);

	dprintf((&ifk_dbg, "plumb_device: ifname %s, ndev %d, ppa %d\n",
	    ifname, ndev, ppa));

	dprintf((&ifk_dbg, "plumb_device: before open dev\n"));

	/*
	 * On rare occasions, setifname has been seen to return EINTR. When
	 * that happens we close the stream and start over again here. If
	 * we start over MAX_PLUMB_ATTEMPTS times without success, we terminate
	 * and return the error to the caller.
	 */
	while (ip_configure_needed) {
		/*
		 * Open /dev/<ifname>. First try style-1 DLPI open, i.e.
		 * /dev/<device><instance>, such as /dev/qfe2. Note that
		 * for vlan devices, the instance is the ppa, which is,
		 * (vlan_id * 1000) + device_instance.
		 */
		if ((error = udi_open(in->ifk_device_name,
		    ppa, &uh)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: udi_open /dev/%s%d"
			    " failed error %d\n", in->ifk_device_name,
			    ppa, error));

			/*
			 * If the style-1 open fails, try /dev/<device>
			 * (style-2). Note that certain drivers, like ibd,
			 * supports style-1 more reliably. But older drivers
			 * like qfe supports style-2 only.
			 */
			error = udi_open(in->ifk_device_name, -1, &uh);

			eprintf((&ifk_dbg, "  retry udi_open /dev/%s"
			    " returned %d\n", in->ifk_device_name, error));

			if (error != 0)
				return (error);
		}

		dprintf((&ifk_dbg, "plumb_device: after open dev\n"));

		/*
		 * Ideally we should attempt to attach to physical unit, before
		 * attempting to plumb. However, since a call to this plumb
		 * routine is always followed by a call to bring the interface
		 * up and that routine does an attach anyway, we do not need
		 * to do the attach here.
		 */

		/* Remove any extraneous STREAMS modules in our path. */
		(void) clean_stream_modules(uh, ifname);

		/*
		 * Push the clhbsndr module on the device. To be effective this
		 * module must be pushed directly above the device.
		 *
		 * Don't push on the clprivnet pseudo device
		 */
		if ((strcmp(ifname, CLPRIVNET) != 0)) {
			if ((error = udi_push(uh, CLHBSNDR)) != 0) {
				eprintf((&ifk_dbg, "plumb_device: udi_push %s"
				    " failed, error %d\n", CLHBSNDR, error));
				(void) udi_close(uh);
				return (error);
			}
			/*
			 * Let the clhbsndr module know that it has been
			 * pushed on a private interconnect adapter and
			 * outgoing private interconnect traffic passing
			 * through it needs to tagged with a particular
			 * b_band value. This tagging is important when
			 * working with VLAN capable devices.
			 */
			bbinfo.bband = SC_ORB_B_BAND;
			iocb.ic_cmd = CLIOCSPRIVBBAND;
			iocb.ic_timout = 0;
			iocb.ic_len = sizeof (bband_info_t);
			iocb.ic_dp = (char *)&bbinfo;
			error = udi_ioctl(uh, I_STR, (intptr_t)&iocb);
			if (error != 0) {
				eprintf((&ifk_dbg, "plumb_device: udi_ioctl"
				    " %d failed, error %d\n", CLIOCSPRIVBBAND,
				    error));
				(void) udi_close(uh);
				return (error);
			}
		}

		/* Push IP on device */
		if ((error = udi_push(uh, IP)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: udi_push IP failed,"
			    " error %d\n", error));
			(void) udi_close(uh);
			return (error);
		}

		dprintf((&ifk_dbg, "plumb_device: after push ip\n"));

		lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
		bzero(lifr, sizeof (struct lifreq));

		/* Get existing flags */
		if ((error = getifflags(uh, lifr)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: "
			    "getifflags IP failed,"
			    " error %d\n", error));
			(void) udi_close(uh);
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}

		/* set the IFF_IPV* flag */
		lifr->lifr_flags |= IFF_IPV4;
		lifr->lifr_flags &= ~IFF_IPV6;

		(void) strncpy((caddr_t)lifr->lifr_name, ifname,
		    sizeof (lifr->lifr_name)-1);
		lifr->lifr_ppa = ppa;

		if ((error = setifname(uh, lifr)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: setifname IP "
			    "failed for %s error %d attempt %d\n",
			    ifname, error, plumb_attempts));

			(void) udi_close(uh);
			kmem_free(lifr, sizeof (struct lifreq));
			if (plumb_attempts < MAX_PLUMB_ATTEMPTS) {
				plumb_attempts++;
				continue;
			} else
				return (error);
		}
		ip_configure_needed = 0;
	}

	dprintf((&ifk_dbg, "plumb_device: after set ifname and unit\n"));

	if ((error = udi_plink(uh_udp, uh, &ip_muxid)) != 0) {
		eprintf((&ifk_dbg, "plumb_device: Error during P_LINK,"
		    " error %d\n", error));
		(void) udi_close(uh);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	dprintf((&ifk_dbg, "plumb_device: after plink\n"));

	if (lifr->lifr_flags & IFF_NOARP) {
		/* ARP is not required */
		lifr->lifr_arp_muxid = -1;
	} else {
		if ((error = udi_open(in->ifk_device_name, ppa,
		    &new_uh)) != 0) {
			if (error != ENODEV || (error =
			    udi_open(in->ifk_device_name, -1, &new_uh)) != 0) {
				eprintf((&ifk_dbg, "plumb_device: udi_open "
				    "failed error %d\n", error));
				(void) udi_close(uh);
				kmem_free(lifr, sizeof (struct lifreq));
				return (error);
			}
		}

		dprintf((&ifk_dbg, "plumb_device: after open dev\n"));

		/* Push ARP on the device */
		if ((error = udi_push(new_uh, ARP)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: "
				"udi_push ARP failed, "
				"error %d\n", error));
			(void) udi_close(uh);
			(void) udi_close(new_uh);
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}
		dprintf((&ifk_dbg, "plumb_device: after push arp\n"));

		/* Tell ARP the name and unit number. */
		lifr->lifr_ppa = ppa;
		if ((error = setifname(new_uh, lifr)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: "
				"setifname ARP failed, "
				"error %d\n", error));
			(void) udi_close(uh);
			(void) udi_close(new_uh);
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}
		dprintf((&ifk_dbg, "plumb_device: after select unit\n"));

		if ((error = udi_plink(uh_udp, new_uh, &arp_muxid)) != 0) {
			eprintf((&ifk_dbg, "plumb_device: "
				"Error during P_LINK, "
				"error %d\n", error));
			(void) udi_close(uh);
			(void) udi_close(new_uh);
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}

		(void) udi_close(new_uh);
		dprintf((&ifk_dbg, "plumb_device: after plink2\n"));

		lifr->lifr_arp_muxid = arp_muxid;
	}

	/*
	 * muxid is an array in the union
	 * Do not set any other field here.
	 */
	lifr->lifr_ip_muxid = ip_muxid;

	if ((error = setifmuxid(uh_udp, lifr)) != 0) {
		eprintf((&ifk_dbg, "plumb_device: "
		    "setifmuxid failed, error %d\n",
		    error));
		(void) udi_close(uh);
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	(void) udi_close(uh);
	kmem_free(lifr, sizeof (struct lifreq));

	dprintf((&ifk_dbg, "plumb_device: Leaving method\n"));
	return (0);

}

static int
setandclearifflags(udi_handle_t uh, const char *ifname,
    unsigned short set, unsigned short clear)
{
	struct lifreq *lifr;
	struct strioctl iocb;
	int rc;

	lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);

	(void) strncpy((caddr_t)lifr->lifr_name, ifname,
	    sizeof (lifr->lifr_name)-1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';

	iocb.ic_cmd = SIOCGLIFFLAGS;
	iocb.ic_timout = 15;
	iocb.ic_len = sizeof (*lifr);
	iocb.ic_dp = (caddr_t)lifr;

	if ((rc = udi_ioctl(uh, I_STR, (intptr_t)&iocb)) != 0) {
		kmem_free(lifr, sizeof (struct lifreq));
		return (rc);
	}

	lifr->lifr_flags &= ~((uint64_t)clear);
	lifr->lifr_flags |= ((uint64_t)set);

	iocb.ic_cmd = SIOCSLIFFLAGS;	/*lint !e737 */
	rc = udi_ioctl(uh, I_STR, (intptr_t)&iocb);

	kmem_free(lifr, sizeof (struct lifreq));
	return (rc);
}

static int
unplumb_device(udi_handle_t uh_udp, const char *ifname)
{
	int	arp_muxid, ip_muxid;
	int	error;
	struct lifreq *lifr;

	lifr = kmem_alloc(sizeof (struct lifreq), KM_SLEEP);
	(void) strncpy((caddr_t)lifr->lifr_name, ifname,
	    sizeof (lifr->lifr_name)-1);
	lifr->lifr_name[sizeof (lifr->lifr_name) - 1] = '\0';

	if ((error = getifmuxid(uh_udp, lifr)) != 0) {
		eprintf((&ifk_dbg, "unplumb: getifmuxid failed, error %d\n",
		    error));
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	arp_muxid = lifr->lifr_arp_muxid;

	if (arp_muxid != -1) {
		if ((error = udi_punlink(uh_udp, arp_muxid)) != 0) {
			eprintf((&ifk_dbg, "unplumb: "
			    "unplink failed for arp\n"));
			kmem_free(lifr, sizeof (struct lifreq));
			return (error);
		}
	}

	ip_muxid = lifr->lifr_ip_muxid;

	if ((error = udi_punlink(uh_udp, ip_muxid)) != 0) {
		eprintf((&ifk_dbg, "unplumb: unplink failed for ip\n"));
		kmem_free(lifr, sizeof (struct lifreq));
		return (error);
	}

	kmem_free(lifr, sizeof (struct lifreq));
	return (0);
}



static int
clean_stream_modules(udi_handle_t uh, const char *ifname)
{
	int error;
	char module_name[FMNAMESZ + 1];

	/*
	 * Remove all modules which appear on this STREAM. This is specifically
	 * aimed at removing the mcnet module since it is autopushed on all
	 * instances of specified devices. However, other modules found here
	 * are not expected, nor needed except for the clhbsndr module.
	 * We will go ahead and remove the clhbsndr module also. It will get
	 * pushed again later. We choose to pop and then repush clhbsndr
	 * back again to keep the code simple.
	 */
	while ((error = udi_ioctl(uh, I_LOOK, (intptr_t)module_name)) == 0) {
		if (strcmp(module_name, MCNET_MODULE_NAME) != 0 &&
		    strcmp(module_name, CLHBSNDR) != 0) {
			eprintf((&ifk_dbg, "clean_stream_modules: "
			    "removing unknown module (%s) found on %s\n",
			    module_name, ifname));
		} else {
			/*
			 * This is a debug-only message, implanted here for
			 * test, to ensure that we've removed mcnet from
			 * the stack.
			 */
			dprintf((&ifk_dbg, "clean_stream_modules: "
			    "removing %s module from %s\n",
			    module_name, ifname));
		}
		if ((error = udi_ioctl(uh, I_POP, (long)NULL)) != 0) {
			eprintf((&ifk_dbg, "clean_stream_modules: "
			    "udi_ioctl I_POP failed of %s module from %s, "
			    "error %d\n", module_name, ifname, error));
		}
	}

	/*
	 * udi_ioctl returns EINVAL if there are no modules remaining
	 * on the STREAM.  Any other error is a problem.
	 */
	if (error != EINVAL) {
		eprintf((&ifk_dbg, "clean_stream_modules: "
		    "udi_ioctl I_LOOK failed, error %d\n", error));
		return (error);
	}

	return (0);
}
