/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  Template definitions for various IDL/C++ mapping constructs
 *
 */

#ifndef _IX_TMPL_H
#define	_IX_TMPL_H

#pragma ident	"@(#)ix_tmpl.h	1.42	08/05/20 SMI"

#include <sys/os.h>
#ifdef linux
#include <orb/invo/corba.h>
#endif

//
// forward declarations
//
template<class A, class A_ptr> class _A_var_;
template<class A, class A_ptr> class _A_field_;
template<class A, class A_ptr> class _A_out_;

template<class T, class T_ptr> class _T_out_;

//
// Declare a field type T_field for an object reference pointer type T_ptr.
//
template<class A, class A_ptr> class _A_field_ {
	friend class _A_var_<A, A_ptr>;
public:
	_A_field_() { _ptr = NULL; }
	_A_field_(A_ptr p) { _ptr = p; }
	_A_field_(const _A_field_& f) {
		_ptr = A::_duplicate(f._ptr);
	}
	~_A_field_() {}
	_A_field_& operator = (A_ptr p);

	_A_field_& operator = (const _A_field_& f);

	_A_field_& operator = (const _A_var_<A, A_ptr>& v);

	operator A_ptr() const { return (_ptr); }
	A_ptr operator ->() const { return (_ptr); }
private :
	A_ptr _ptr;
};

//
// Helper classes for managing pointers to aggregate types.
//
template<class T, class T_ptr> class _T_var_ {
public:
	_T_var_() { _ptr = NULL; }
	_T_var_(T_ptr p) { _ptr = p; }
	_T_var_(const _T_var_& r) {
		_ptr = new T(*r._ptr);
	}
	virtual ~_T_var_() { delete _ptr; }

	_T_var_& operator = (const _T_var_& r);

	_T_var_& operator = (T_ptr p);

	T_ptr operator ->() const { return (_ptr); }

	// This is used to pass _var as an "in" parameter
	operator T_ptr() const { return (_ptr); }
	// This is used to pass _var as an "inout" parameter
	T_ptr& INOUT() { return (_ptr); } // an interim conversion function
	// This is used to pass _var as an "out" parameter
	T_ptr& out() {
		delete _ptr;
		_ptr = NULL;
		return (_ptr);
	}

protected:
	T_ptr _ptr;
};

//
// Declare a type name for an out parameter for types other than interfaces.
// The idea is to store the address of the pointer in an object and access
// the pointer indirectly.  Also, assignment to the object will release
// the old storage.
//
template<class T, class T_ptr> class _T_out_ {
public:
	_T_out_(T_ptr& p) { _ptr = &p; }
	_T_out_(_T_var_<T, T_ptr>& v) { _ptr = &(v.out()); }
	_T_out_(const _T_out_& p) { _ptr = p._ptr; }
	~_T_out_() {}
	_T_out_& operator = (T_ptr p) { *_ptr = p; return (*this); }
	_T_out_& operator = (const _T_out_& p) {
		*_ptr = *p._ptr; return (*this);
	}
	operator T_ptr() const { return (*_ptr); }
	T_ptr *operator& () { return (_ptr); }
private:
	T_ptr* _ptr;
};

//
// ditto for interface types.
//
template<class A, class A_ptr> class _A_out_ {
public:
	_A_out_(A_ptr& p) { _ptr = &p; }
	_A_out_(_A_var_<A, A_ptr>& v) { _ptr = &(v.out()); }
	_A_out_(const _A_out_& p) { _ptr = p._ptr; }
	~_A_out_() {}
	_A_out_& operator = (A_ptr p) { *_ptr = p; return (*this); }
	_A_out_& operator = (const _A_out_& p) {
		*_ptr = *p._ptr; return (*this);
	}
	operator A_ptr() const { return (*_ptr); }
	A_ptr *operator& () { return (_ptr); }
private:
	A_ptr* _ptr;
};

//
// Declare types for managed object references.
//
// This definition assumes that the given types T and T_ptr are
// already defined (as opposed to allowing forward references).
//
// Right now, these templates avoid operator T_ptr&() because
// some compilers get confused by this definition.
//
// Note that the CORBA spec. says that assignments between vars of the
// same type is OK and that assignments that would do a widen are NOT.
// Using a template handles this requirement.
//
template<class A, class A_ptr> class _A_var_ {
public:
	_A_var_() { _ptr = NULL; }
	_A_var_(A_ptr p) { _ptr = p; }
	_A_var_(const _A_var_& r) {
		_ptr = A::_duplicate(r._ptr);
	}
	~_A_var_() { CORBA::release(_ptr->_this_obj()); }

	A_ptr operator ->() const { return (_ptr); }

	_A_var_& operator = (const _A_var_& v);

	_A_var_& operator = (A_ptr p);

	// This is used to pass _var as an "in" parameter
	operator A_ptr() const { return (_ptr); }

	// This is used to pass _var as an "inout" parameter
	A_ptr& INOUT() { return (_ptr); } // an interim conversion function
	// This is used to pass _var as an "out" parameter
	A_ptr& out() {
		CORBA::release(_ptr->_this_obj());
		_ptr = NULL;
		return (_ptr);
	}

	A_ptr _ptr;
private:
	// Disallow comparisons - this is overkill as we need this only for
	//	Object_vars not others
	bool operator == (const _A_var_);
	bool operator == (const int _A_ptr_);
	bool operator != (const _A_var_);
	bool operator != (const int _A_ptr_);
	// Disallow assignment of A_field to A_var. Re: Bugid 4091549
	_A_var_& operator = (const _A_field_<A, A_ptr>& f);
};

#include <orb/invo/ix_tmpl_in.h>

#endif	/* _IX_TMPL_H */
