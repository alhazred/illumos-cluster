/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ORB_INVO_COMMON_H
#define	_ORB_INVO_COMMON_H

#pragma ident	"@(#)common.h	1.46	08/05/20 SMI"

#ifndef nil
#define	nil 0
#endif

#include <sys/os.h>
#include <orb/invo/ix.h>
#include <orb/invo/corba.h>
#include <orb/idl_datatypes/interfaceseq.h>

// XX This is here largely for NODEID_MAX.
#include <sys/clconf_int.h>

#endif	/* _ORB_INVO_COMMON_H */
