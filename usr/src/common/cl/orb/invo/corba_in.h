/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  corba_in.h
 *
 */

#ifndef _CORBA_IN_H
#define	_CORBA_IN_H

#pragma ident	"@(#)corba_in.h	1.31	08/07/05 SMI"

/*
 * 6533088: Error: nil is not defined
 */
#if SOL_VERSION >= __s10
#define	BUG_6533088
#endif

//
// Convenience functions so we avoid requiring CORBA:: prefix
//

inline bool
is_nil(CORBA::Object_ptr p)
{
	return (CORBA::is_nil(p));
}

inline bool
is_not_nil(CORBA::Object_ptr p)
{
	return (!CORBA::is_nil(p));
}

// static
inline bool
CORBA::is_nil(Object_ptr object_p)
{
	return (object_p == NULL);
}

//
// Object methods
//

// static
inline CORBA::Object_ptr
CORBA::Object::_nil()
{
#ifdef BUG_6533088
	return (0);
#else
	return (nil);
#endif
}

inline CORBA::Object_ptr
CORBA::Object::_this_obj()
{
	return (this);
}

//
// Exception methods
//

inline
CORBA::Exception::Exception(uint_t major, CORBA::TypeId new_tid,
    const char *name) :
	tid(new_tid),
	_major_(major),
	_name_(name)
{
}

//
// type_match - returns whether the exception types match
//
inline bool
CORBA::Exception::type_match(Exception *exceptp)
{
	return ((exceptp != NULL) && _ox_equal_tid(exceptp->get_typeid(), tid));
}

inline CORBA::TypeId
CORBA::Exception::get_typeid()
{
	return (tid);
}

inline uint_t
CORBA::Exception::_major() const
{
	return (_major_);
}

inline const char *
CORBA::Exception::_name() const
{
	return (_name_);
}

inline CORBA::exception_enum_t
CORBA::Exception::exception_enum()
{
	return ((CORBA::exception_enum_t)_major_);
}

//
// SystemException methods
//

inline uint_t
CORBA::SystemException::_minor()
{
	return (_minor_);
}

inline void
CORBA::SystemException::_minor(uint_t mi)
{
	_minor_ = mi;
}

inline void
CORBA::SystemException::completed(CORBA::CompletionStatus cs)
{
	_completed_ = cs;
}

inline CORBA::CompletionStatus
CORBA::SystemException::completed()
{
	return (_completed_);
}

//
// UserException methods
//

inline
CORBA::UserException::UserException(uint_t major, CORBA::TypeId new_tid,
    const char *name) :
	Exception(major, new_tid, name)
{
}

//
// _try_cast - provides the body for all _check_cast methods in the stubs
// that check whether one can cast an arbitrary exception to the given
// user exception.
//
// static
inline void *
CORBA::UserException::_try_cast(Exception *exceptp, CORBA::TypeId tid)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(), tid)) {
		return ((void *)exceptp);
	} else {
		return (NULL);
	}
}

//
// Specific system exception methods
//

inline
CORBA::BAD_PARAM::BAD_PARAM(uint_t minor, CORBA::CompletionStatus completed) :
	SystemException(BAD_PARAM_major, minor, completed)
{
}

// static
inline CORBA::BAD_PARAM *
CORBA::BAD_PARAM::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[BAD_PARAM_major].tid())) {
		return ((BAD_PARAM *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::COMM_FAILURE::COMM_FAILURE(uint_t minor,
    CORBA::CompletionStatus completed) :
	SystemException(COMM_FAILURE_major, minor, completed)
{
}

// static
inline CORBA::COMM_FAILURE *
CORBA::COMM_FAILURE::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[COMM_FAILURE_major].tid())) {
		return ((COMM_FAILURE *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::INV_OBJREF::INV_OBJREF(uint_t minor, CORBA::CompletionStatus completed) :
	SystemException(INV_OBJREF_major, minor, completed)
{
}

// static
inline CORBA::INV_OBJREF *
CORBA::INV_OBJREF::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[INV_OBJREF_major].tid())) {
		return ((INV_OBJREF *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::WOULDBLOCK::WOULDBLOCK(uint_t minor, CORBA::CompletionStatus completed) :
	SystemException(WOULDBLOCK_major, minor, completed)
{
}

// static
inline CORBA::WOULDBLOCK *
CORBA::WOULDBLOCK::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[WOULDBLOCK_major].tid())) {
		return ((WOULDBLOCK *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::RETRY_NEEDED::RETRY_NEEDED(uint_t minor,
    CORBA::CompletionStatus completed) :
	SystemException(RETRY_NEEDED_major, minor, completed)
{
}

// static
inline CORBA::RETRY_NEEDED *
CORBA::RETRY_NEEDED::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[RETRY_NEEDED_major].tid())) {
		return ((RETRY_NEEDED *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::PRIMARY_FROZEN::PRIMARY_FROZEN(
    uint_t minor, CORBA::CompletionStatus completed) :
	SystemException(PRIMARY_FROZEN_major, minor, completed)
{
}

// static
inline CORBA::PRIMARY_FROZEN *
CORBA::PRIMARY_FROZEN::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[PRIMARY_FROZEN_major].tid())) {
		return ((PRIMARY_FROZEN *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::VERSION::VERSION() :
	SystemException(VERSION_Exception_major, 1, CORBA::COMPLETED_YES)
{
}

inline
CORBA::VERSION::VERSION(uint_t minor, CORBA::CompletionStatus completed) :
	SystemException(VERSION_Exception_major, minor, completed)
{
}

// static
inline CORBA::VERSION *
CORBA::VERSION::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[VERSION_Exception_major].tid())) {
		return ((VERSION *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::CTX_EACCESS::CTX_EACCESS(uint_t minor,
    CORBA::CompletionStatus completed) :
	SystemException(CTX_EACCESS_major, minor, completed)
{
}

// static
inline CORBA::CTX_EACCESS *
CORBA::CTX_EACCESS::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[CTX_EACCESS_major].tid())) {
		return ((CTX_EACCESS *)exceptp);
	} else {
		return (NULL);
	}
}

inline
CORBA::NS_EPERM::NS_EPERM(uint_t minor,
    CORBA::CompletionStatus completed) :
	SystemException(NS_EPERM_major, minor, completed)
{
}

// static
inline CORBA::NS_EPERM *
CORBA::NS_EPERM::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && _ox_equal_tid(exceptp->get_typeid(),
	    major_translation[NS_EPERM_major].tid())) {
		return ((NS_EPERM *)exceptp);
	} else {
		return (NULL);
	}
}

//
// Environment methods
//

#ifdef _KERNEL_ORB
inline ID_node &
CORBA::Environment::get_src_node()
{
	return (source_node);
}
#endif

#ifdef DEBUG
//
// used to show that environment has been used for
// invocation or contains an exception and needs checking
//
inline void
CORBA::Environment::mark_used()
{
	needs_checking = true;
}

inline void
CORBA::Environment::has_checked()
{
	needs_checking = false;
}

inline void
CORBA::Environment::check_used(char *s)
{
	// This panic implies that an Environment
	// should have been checked for exceptions but has not been
	if (needs_checking) {
		os::panic("%s", s);
	}
}

#else

inline void
CORBA::Environment::mark_used()
{
}

inline void
CORBA::Environment::has_checked()
{
}

inline void
CORBA::Environment::check_used(char *)
{
}
#endif

//
// Returns pointer to exception (either user or system)
// NULL if none
//
inline CORBA::Exception *
CORBA::Environment::exception()
{
	has_checked();
	return (_ptr);
}

//
// Check if a system_exception exists
//
inline CORBA::SystemException *
CORBA::Environment::sys_exception()
{
	return (CORBA::SystemException::_exnarrow(_ptr));
}

// To set any Exception
inline void
CORBA::Environment::exception(Exception *p)
{
	mark_used();
	done();
	_ptr = p;
}

// To set system_exception without requiring to do a new
inline void
CORBA::Environment::system_exception(const SystemException &s)
{
	mark_used();
	done();
	sys_ex = s;
	_ptr = &sys_ex;
}

// To delete any old exception if any
inline void
CORBA::Environment::clear()
{
	done();
	_ptr = NULL;
	has_checked();
}

inline bool
CORBA::Environment::is_nonblocking()
{
	return (nonblocking);
}

inline void
CORBA::Environment::set_nonblocking()
{
	nonblocking = true;
}

inline void
CORBA::Environment::clear_nonblocking()
{
	nonblocking = false;
}

inline os::mem_alloc_type
CORBA::Environment::nonblocking_type()
{
	return (nonblocking ? os::NO_SLEEP : os::SLEEP);
}

inline bool
CORBA::Environment::service_is_freezing()
{
	return (service_freezing);
}

inline void
CORBA::Environment::set_freezing()
{
	service_freezing = true;
}

inline void
CORBA::Environment::clear_freezing()
{
	service_freezing = false;
}

inline bool
CORBA::Environment::get_freeze_retry()
{
	return (freeze_retry);
}

inline void
CORBA::Environment::set_freeze_retry()
{
	freeze_retry = true;
}

inline void
CORBA::Environment::clear_freeze_retry()
{
	freeze_retry = false;
}

inline void
CORBA::Environment::done()
{
	// If _ptr == &sys_ex, it is pointing to local memory
	if (_ptr != &sys_ex) {
		delete _ptr;
	}
	_ptr = NULL;
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)

inline zoneid_t
CORBA::Environment::get_zone_id() const
{
	return (zone_id);
}

inline uint32_t
CORBA::Environment::get_cluster_id() const
{
	return (cluster_id);
}

inline uint64_t
CORBA::Environment::get_zone_uniqid() const
{
	return (zone_uniqid);
}

inline void
CORBA::Environment::set_zone_id(zoneid_t zone)
{
	zone_id = zone;
}

inline void
CORBA::Environment::set_cluster_id(uint32_t clid)
{
	cluster_id = clid;
}

inline void
CORBA::Environment::set_zone_uniqid(uint64_t zone_uniq_id)
{
	zone_uniqid = zone_uniq_id;
}

#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

#endif	/* _CORBA_IN_H */
