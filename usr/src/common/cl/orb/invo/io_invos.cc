//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)io_invos.cc	1.48	08/05/20 SMI"

#include <orb/invo/io_invos.h>
#include <orb/invo/invocation.h>
#include <orb/buffers/marshalstream.h>
#include <orb/infrastructure/sigs.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <orb/infrastructure/clusterproc.h>
#include <orb/xdoor/rxdoor.h>
#include <orb/member/members.h>
#include <orb/fault/fault_injection.h>
#include <orb/debug/orb_trace.h>
#include <orb/xdoor/rxdoor_header.h>
#include <orb/debug/orb_stats_mgr.h>
#include <sys/mc_probe.h>

//
// The following code implements the core of remote signal support for
// Galileo.  We implement two finite state machines, one on the client
// and one on the server.  Due to out-of-order nature of message delivery,
// the server state machine is somewhat more complex.... Both the client
// and server keep track of the state via an state vector that's part of the
// slot structure.
//

//
// Client side state machine -------------------------------------------
//
//
//  State 1 (unallocated)
//  ---------------------
//  when allocated, send and next State = 2
//
//  State 2 (OB_SENT)
//  ---------------------
//  if error occurs, next State = 1.
//  if signal occurs, next State = 3
//  if results arrive, next State = 1
//
//
//  State 3 (OB_SENT | OB_SIGNALLED)
//  ------------------------------
//  Send signal message
//  if results arrive with signal bit set, next State = 4
//  else next State = 5
//
//
//  State 4 (OB_SENT | OB_SIGNALLED | OB_SIGACKED)
//  ---------------------------------------------
//  when we free, next State is 1
//
//
//  State 5 (OB_SENT | OB_SIGNALLED | OB_CANCELLED)
//  ---------------------------------------------
//  send cancel. once finished method arrives, next State is 1
//

//
// put declaration here so outbound_invo and outbound_invo_table can share w/o
// polluting global namespace
//
enum {	OB_SENT		= (1 << 0),	// start state
	OB_SIGNALLED	= (1 << 1),	// if signaled message was sent
	OB_SIGACKED	= (1 << 2),	// if server delivered signal to thread
	OB_CANCELLED	= (1 << 3)	// set if signal message missed
};

//
// instantiate our static data - see io_invos.h
//
outbound_invo	*outbound_invo_table::_buckets[outbound_invo_table::SIZE];
os::mutex_t 	outbound_invo_table::_locks[outbound_invo_table::SIZE];
uint_t		outbound_invo_table::_next_slot_number;

inbound_invo	*inbound_invo_table::_buckets[inbound_invo_table::SIZE];
os::mutex_t 	inbound_invo_table::_locks[inbound_invo_table::SIZE];

//
// The following gets lint to shut up about possible NULL
// ptrs being returned from new.  Fix if we change messages
// to arrive in interrupt context
//
//lint -function( operator new (r) )
//

//
// outbound_invo methods
//

//
// called when things don't work and we're tearing things down.
// cannot happen after we start signal protocol, so teardown is
// simple.
//
void
outbound_invo::oops()
{
	ASSERT(_state == OB_SENT);

	int which = outbound_invo_table::hashslot(_slot);

	outbound_invo_table::_locks[which].lock();

	outbound_invo **prev = &outbound_invo_table::_buckets[which];
	outbound_invo  *tmp = outbound_invo_table::_buckets[which];

	while (tmp != this) {		// find in hash chain
		prev = &tmp->_next;
		tmp = *prev;
	}
	*prev = _next;
	delete this;

	outbound_invo_table::_locks[which].unlock();
}

//
// wait_for_results - called to wait for results and release slot
//
ExceptionStatus
outbound_invo::wait_for_results()
{
	ExceptionStatus	result;

	bool		send_cancel = false;

	int		which = outbound_invo_table::hashslot(_slot);

	os::mutex_t	*lockp = &outbound_invo_table::_locks[which];

	lockp->lock();
	MC_PROBE_0(wait_for_results_wait, "clustering orb invocation", "");

	while (_result == CORBA::UNINITIALIZED) {

		ORB_DBPRINTF(ORB_TRACE_OUT_INVO,
		    ("outbound_invo::wait_for_results: thread 0x%x waiting\n",
		    _threadid));

#ifdef USE_SIGNAL_FORWARDING
		//
		// bug 4178496: cannot call cv_wait_sig() during remote
		// invocation as this causes recursive mutex lock.
		//
		if (_cv.wait_sig(lockp) == os::condvar_t::SIGNALED) {
			ORB_DBPRINTF(ORB_TRACE_OUT_INVO,
			    ("outbound_invo::wait_for_results: thread "
			    "0x%x signaled\n", _threadid));

			_state |= OB_SIGNALLED;
			//
			// Avoid holding bucket lock during send,
			// since lock is grabbed at interrupt priority.
			//
			lockp->unlock();
			signals::signal_send(_node, _slot); // send message
			lockp->lock();

			while (_result == CORBA::UNINITIALIZED) {
				_cv.wait(lockp);
			}
			break;	// break out of outer loop
		}
#else
		_cv.wait(lockp);
#endif
	}
	MC_PROBE_0(wait_for_results_over, "clustering orb invocation", "");
	ORB_DBPRINTF(ORB_TRACE_OUT_INVO,
	    ("outbound_invo::wait_for_results: thread 0x%x woke up "
	    "w/ result %d\n", _threadid, _result));

	result = _result;

	//
	// now finish up and delete if possible
	//
	if ((_state == OB_SENT) ||
	    (_state == (OB_SENT | OB_SIGNALLED | OB_SIGACKED))) {
		//
		// First case is normal one - we sent and received data
		// w/o reply.  Second case is where we got a signal and
		// received acknowledgement in the reply that the signal
		// was received.  In either case, we're done and can
		// free the slot.
		//
		outbound_invo	**prev = &outbound_invo_table::_buckets[which];
		outbound_invo	*outboundp =
		    outbound_invo_table::_buckets[which];

		// Find entry in hash chain
		while (outboundp != this) {
			prev = &outboundp->_next;
			outboundp = *prev;
		}
		*prev = _next;
		delete this;
	} else {
		//
		// We sent a signal message, but didn't receive an ack.
		// Send a cancel signal message.
		// Mark the message as cancelled, so that
		// it can be deleted once the finished signal message arrives.
		// Send it outside the lock, though...
		//
		ASSERT(_state == (OB_SENT | OB_SIGNALLED));
		_state |= OB_CANCELLED;
		_threadid = NULL;
		send_cancel = true;
	}

	lockp->unlock();

	if (send_cancel) {
		// Avoided send from wakeup
		signals::cancel_send(_node, _slot);
	}
	return (result);
}

//
// outbound_invo_table methods
//

//
// alloc - allocates a new outbound_invo_table entry for this invocation.
//
// static
outbound_invo *
outbound_invo_table::alloc(ID_node &node, invocation &invo)
{
	outbound_invo	*outboundp;

	outbound_invo	*new_outboundp =
	    new outbound_invo(node, OB_SENT, &invo);
	new_outboundp->_threadid = os::threadid();

again:
	slot_t	try_slot = 0x7FFF &
	    (os::atomic_add_32_nv(&_next_slot_number, 1));

	int	which = outbound_invo_table::hashslot(try_slot);

	_locks[which].lock();

	outboundp = _buckets[which];

	while (outboundp) {
		if (outboundp->_slot == try_slot)  {
			// This slot is already in use
			_locks[which].unlock();
			goto again;
		}
		outboundp = outboundp->_next;
	}

	//
	// if we're here, we have a valid (unused) slot
	// and are holding the lock for that slot.
	// Enter the entry into the list for the appropriate bucket.
	//
	new_outboundp->_slot = try_slot;
	new_outboundp->_next = _buckets[which];
	new_outboundp->_result = CORBA::UNINITIALIZED;
	_buckets[which] = new_outboundp;
	_locks[which].unlock();

	return (new_outboundp);
}

//
// wake up the sleeping thread that initiated this twoway request.
// The system executes this method in the INTERRUPT CONTEXT.
//
// static
void
outbound_invo_table::wakeup(slot_t slot, uint_t flags, recstream *re,
    ID_node &src_node)
{
	MC_PROBE_0(wakeup_start, "clustering orb invocation", "");
	outbound_invo *tmp;

	FAULTPT_ORPHAN(FAULTNUM_ORPHAN_REPLY_BEFORE_CHECK,
	    FaultFunctions::generic);

	int	which = hashslot(slot);

	_locks[which].lock();

	//
	// first, let's do an orphan message check now that
	// we're holding the lock...
	//
	// Validate that the reply came from the current node incarnation
	//
	if (!members::the().still_alive(src_node)) {
		orb_stats_mgr::the().inc_orphans(src_node.ndid, REPLY_MSG);

		//
		// We do not attempt to recover the XdoorTab since it is
		// from a node that we now consider to be down.
		//
		re->done();

		_locks[which].unlock();

		// Destructors take care of space
		return;
	}

	FAULTPT_ORPHAN(FAULTNUM_ORPHAN_REPLY_AFTER_CHECK,
	    FaultFunctions::generic);

	tmp = _buckets[which];

	while (tmp->_slot != slot) {
		tmp = tmp->_next;
	}
	ASSERT((tmp->_state & (OB_CANCELLED | OB_SIGACKED)) == 0);

	tmp->inv->set_recstream(re);

	if (rxdoor_reply_header::is_signaled(flags)) {
		ASSERT(tmp->_state & OB_SIGNALLED);
		tmp->_state |= OB_SIGACKED;
	}

	if (rxdoor_reply_header::is_exception(flags)) {
		tmp->_result = CORBA::REMOTE_EXCEPTION;
	} else if (rxdoor_reply_header::is_retry(flags)) {
		tmp->_result = CORBA::RETRY_EXCEPTION;
	} else {
		tmp->_result = CORBA::NO_EXCEPTION;
	}

#if defined(_FAULT_INJECTION)
	//
	// Copy the Invo triggers to the waiting thread.
	// Orphan message check has been performed, and can guarantee
	// that waiting thread exists.
	//
	InvoTriggers::copy_triggers(InvoTriggers::the(),
	    tmp->inv->get_invotrigp());
#endif

	// Now wakeup the thread that initiated this twoway request
	tmp->_cv.signal();

	_locks[which].unlock();
	MC_PROBE_0(wakeup_end, "clustering orb invocation", "");
}

//
// finished - processes an incoming signals finished message.
//
// static
void
outbound_invo_table::finished(ID_node &src_node, slot_t slot)
{
	int	which = hashslot(slot);

	_locks[which].lock();

	if (!members::the().still_alive(src_node)) {
		_locks[which].unlock();
		return;
	}

	outbound_invo	**prev = &_buckets[which];
	outbound_invo	*outboundp = _buckets[which];

	while (outboundp->_slot != slot) {
		prev = &outboundp->_next;
		outboundp = *prev;
	}

	ASSERT(outboundp->_state == (OB_SENT | OB_SIGNALLED | OB_CANCELLED));

	*prev = outboundp->_next;
	delete outboundp;

	_locks[which].unlock();
}

//
// cleanup_invocations - because a node reconfiguration occurred.
//
// static
void
outbound_invo_table::cleanup_invocations()
{
	//
	// For each bucket, process the invocations in the list of that bucket.
	//
	for (int i = 0; i < SIZE; i++) {

		_locks[i].lock();

		outbound_invo	**prev = &_buckets[i];
		outbound_invo	*outboundp = _buckets[i];

		while (outboundp) {
			if (!members::the().still_alive(outboundp->_node)) {
				//
				// Since we won't get any more messages, there's
				// no point in finishing the protocol.
				// Just set it to a known state.
				//
				outboundp->_state = OB_SENT;

				//
				// The thread may be alive or not.
				//
				if (outboundp->_threadid != NULL) {
					//
					// The thread is alive.
					//
					if (outboundp->_result ==
					    CORBA::UNINITIALIZED) {
						//
						// The thread has not received
						// any status. Wake up the
						// the thread with an error.
						//
						outboundp->_result =
						    CORBA::LOCAL_EXCEPTION;

						outboundp->_cv.signal();
					}
				} else {
					//
					// There is no thread for this entry.
					// There will be no message from
					// the dead node.
					// So delete the entry.
					//
					outboundp = outboundp->_next;
					delete *prev;
					*prev = outboundp;
					continue;
				}
			}
			prev = &outboundp->_next;
			outboundp = outboundp->_next;
		}
		_locks[i].unlock();
	}
}

//
// inbound_invo methods
//

//
// done - called when the system is preparing the reply message.
// The slot must be freed prior to sending the reply in order
// to avoid a race condition with the next request
// from the client that uses this slot.
//
// This method can be called multiple times because of reply msg retries.
//
void
inbound_invo::done()
{
	ASSERT((_state & IB_REQUEST) != 0);
	if ((_state & IB_REPLY) != 0) {
		//
		// This must be a reply msg retry.
		// Have already freed the slot and updated the state.
		// Do not need locking, because once this state has been
		// entered the state does not change till this object
		// is destroyed.
		//
		return;
	};

	int		which = inbound_invo_table::hashslot(_slot);

	inbound_invo_table::_locks[which].lock();

	ORB_DBPRINTF(ORB_TRACE_IN_INVO,
	    ("inbound_invo::done(%p) state %d\n", this, _state));
	//
	// Find and then free the slot.
	//
	inbound_invo	*inboundp = inbound_invo_table::_buckets[which];
	inbound_invo	**prev = &inbound_invo_table::_buckets[which];

	while (inboundp != this) {
		prev = &inboundp->_next;
		inboundp = *prev;
	}
	*prev = _next;

	// Should find this object
	ASSERT(this == inboundp);

	switch (_state) {

	case IB_REQUEST:
	case IB_REQUEST | IB_SIGNAL:
		_state |= IB_REPLY;
		break;

	default:
		// This panic implies we are in an invalid state

		//
		// SCMSGS
		// @explanation
		// The internal state describing the server side of a remote
		// invocation is invalid when the invocation completes server
		// side processing.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: inbound_invo::done: state is 0x%x",
		    _state);
		break;
	}

	inbound_invo_table::_locks[which].unlock();
}

//
// register_request - registers a newly arrived invocation request
// in the data structure that manages active server side requests.
//
void
inbound_invo::register_request(slot_t slot_)
{
	ASSERT(_state == IB_REQUEST);
	_slot = slot_;
	_threadid = os::threadid();

	int		which = inbound_invo_table::hashslot(_slot);

	inbound_invo_table::_locks[which].lock();

	//
	// Search for an existing entry representing a signal
	// for this invocation.
	//
	inbound_invo	*signalp = inbound_invo_table::_buckets[which];
	inbound_invo	**prev = &inbound_invo_table::_buckets[which];

	while (signalp != NULL) {
		if ((signalp->_slot == _slot) &&
		    (signalp->_node.ndid == _node.ndid) &&
		    (signalp->_node.incn == _node.incn)) {
			//
			// The signal got here first,
			// which should be a rare event.
			//
			// Remove the existing bucket entry for the signal
			//
			*prev = signalp->_next;
			ASSERT(signalp->_state == IB_SIGNAL);
			ASSERT(signalp->_threadid == NULL);
			delete signalp;

			_state |= IB_SIGNAL;

			clsendselfsig();

			break;
		} else {
			prev = &signalp->_next;
			signalp = *prev;
		}
	}
	// Enter the invocation request in the bucket list
	_next = inbound_invo_table::_buckets[which];
	inbound_invo_table::_buckets[which] = this;


	ORB_DBPRINTF(ORB_TRACE_IN_INVO,
	    ("inbound_invo::register_request(%p) state %d\n", this, _state));

	inbound_invo_table::_locks[which].unlock();
}

//
// signal - called when a signal message arrives from client node
//
// static
void
inbound_invo::signal(ID_node &node_, slot_t slot_)
{
	int		which = inbound_invo_table::hashslot(slot_);
	bool		send_finish = false;

	// Perform allocation outside critical region
	inbound_invo	*new_inboundp = new inbound_invo(node_, IB_SIGNAL);

	inbound_invo_table::_locks[which].lock();

	if (!members::the().still_alive(node_)) {
		inbound_invo_table::_locks[which].unlock();
		delete new_inboundp;
		return;
	}

	inbound_invo	*inboundp = inbound_invo_table::_buckets[which];
	inbound_invo	**prev = inbound_invo_table::_buckets + which;

	while (inboundp != NULL) {
		if ((inboundp->_slot == slot_) &&
		    (inboundp->_node.ndid == node_.ndid) &&
		    (inboundp->_node.incn == node_.incn)) {
			break;
		}
		prev = &inboundp->_next;
		inboundp = *prev;
	}

	if (inboundp != NULL) {
		//
		// The invocation or cancel got here first.
		//
		delete new_inboundp;

		ORB_DBPRINTF(ORB_TRACE_IN_INVO,
		    ("inbound_invo::signal(%p) state %d\n",
		    inboundp, inboundp->_state));

		switch (inboundp->_state) {
		case IB_REQUEST:
			ASSERT(inboundp->_threadid != NULL);
			inboundp->_state |= IB_SIGNAL;
			clsendlwpsig(inboundp->_threadid);
			break;

		case IB_CANCEL:
			// Must tell client that signal was processed
			send_finish = true;

			// Free the slot
			*prev = inboundp->_next;
			delete inboundp;

			break;

		default:
			//
			// SCMSGS
			// @explanation
			// The internal state describing the server side of a
			// remote invocation is invalid when a signal arrives
			// during processing of the remote invocation.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: inbound_invo::signal:_state is 0x%x",
			    inboundp->_state);
		}
	} else {
		// we're first or we missed the invocation altogether, in
		// which case we'll get a cancel message.
		//
		inboundp = new_inboundp;
		inboundp->_slot = slot_;
		inboundp->_next = inbound_invo_table::_buckets[which];
		inbound_invo_table::_buckets[which] = inboundp;

		ORB_DBPRINTF(ORB_TRACE_IN_INVO,
		    ("inbound_invo::signal(%p) state %d\n",
		    inboundp, inboundp->_state));
	}

	inbound_invo_table::_locks[which].unlock();

	if (send_finish) {
		signals::finish_send(node_, slot_);
	}
}

//
// cancel - process cancel message from client node
//
// static
void
inbound_invo::cancel(ID_node &node_, slot_t slot_)
{
	int		which = inbound_invo_table::hashslot(slot_);
	bool		send_finish = false;

	//
	// Do not want to block while holding lock.
	// Allocate memory outside critical region.
	//
	inbound_invo	*new_inboundp = new inbound_invo(node_, IB_CANCEL);

	inbound_invo_table::_locks[which].lock();

	if (!members::the().still_alive(node_)) {
		inbound_invo_table::_locks[which].unlock();
		delete new_inboundp;
		return;
	}

	inbound_invo	*inboundp = inbound_invo_table::_buckets[which];
	inbound_invo	**prev = inbound_invo_table::_buckets + which;

	while (inboundp != NULL) {
		if ((inboundp->_slot == slot_) &&
		    (inboundp->_node.ndid == node_.ndid) &&
		    (inboundp->_node.incn == node_.incn)) {
			break;
		}
		prev = &inboundp->_next;
		inboundp = *prev;
	}

	if (inboundp != NULL) {

		delete new_inboundp;

		ORB_DBPRINTF(ORB_TRACE_IN_INVO,
		    ("inbound_invo::cancel(%p) state %d\n",
		    inboundp, inboundp->_state));

		switch (inboundp->_state) {

		case IB_SIGNAL:
			// Must tell client that we processed signal
			send_finish = true;

			// Free the slot
			*prev = inboundp->_next;
			delete inboundp;

			break;

		default:
			//
			// SCMSGS
			// @explanation
			// The internal state describing the server side of a
			// remote invocation is invalid when a cancel message
			// arrives during processing of the remote invocation.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: inbound_invo::cancel:_state is 0x%x",
			    inboundp->_state);
		}
	} else {
		//  Cancel message arrived before the signal message.
		inboundp = new_inboundp;
		inboundp->_slot = slot_;
		inboundp->_next = inbound_invo_table::_buckets[which];
		inbound_invo_table::_buckets[which] = inboundp;

		ORB_DBPRINTF(ORB_TRACE_IN_INVO,
		    ("inbound_invo::cancel(%p) state %d\n",
		    inboundp, inboundp->_state));
	}

	inbound_invo_table::_locks[which].unlock();

	if (send_finish) {
		signals::finish_send(node_, slot_);
	}
}

//
// inbound_invo_table methods
//

//
// cleanup_invocations - because a node reconfiguration occurred.
//
// static
void
inbound_invo_table::cleanup_invocations()
{
	//
	// For each bucket process the invocations in that bucket.
	//
	for (int i = 0; i < SIZE; i++) {

		_locks[i].lock();

		inbound_invo	**prev = &_buckets[i];
		inbound_invo	*inboundp = _buckets[i];

		while (inboundp != NULL) {

			ORB_DBPRINTF(ORB_TRACE_IN_INVO,
			    ("inbound_invo_table::cleanup(%p) state %d\n",
			    inboundp, inboundp->_state));


			if (!members::the().still_alive(inboundp->_node)) {
				//
				// The source node died.
				//
				switch (inboundp->_state) {

				case inbound_invo::IB_SIGNAL:
				case inbound_invo::IB_CANCEL:
					//
					// The server completed request
					// processing or we never received
					// the actual request.
					// Delete entry.
					//
					inboundp = inboundp->_next;
					delete *prev;
					*prev = inboundp;
					continue;

				case inbound_invo::IB_REQUEST:
					//
					// A request was received,
					// but has not completed processing.
#ifdef USE_SIGNAL_FORWARDING
					//
					// Send thread a SIGINT signal.
					//
					// Signals should be sent only if
					// signal forwarding is enabled.
					// In absence of signal forwarding
					// threads processing invocations are
					// not prepared to deal with interrupted
					// calls which leads to undesirable and
					// even unknown side effects.
					clsendlwpsig(inboundp->_threadid);
#endif
					break;

				case inbound_invo::IB_REQUEST |
				    inbound_invo::IB_SIGNAL:
					//
					// Have already delivered a signal.
					//
					break;

				default:
					(void) orb_syslog_msgp->
					    log(SC_SYSLOG_PANIC, MESSAGE,
					    "clcomm: inbound_invo_table::"
					    "cleanup:_state is 0x%x",
					    inboundp->_state);
				}
			}
			prev = &inboundp->_next;
			inboundp = inboundp->_next;
		}
		_locks[i].unlock();
	}
}
