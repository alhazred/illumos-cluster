/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _INVOCATION_MODE_H
#define	_INVOCATION_MODE_H

#pragma ident	"@(#)invocation_mode.h	1.9	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>

class arg_info;

//
// invocation_mode - specifies message processing semantics.
//
class invocation_mode {
public:
	// These flags define the message handling semantics.
	enum mode_flags { NONE	= 0x00,	// The other bit flags are all false
		ONEWAY		= 0x01,	// All msg except 2way request + reply
		SYNCHRONOUS	= 0x02,
		NONBLOCKING	= 0x04,
		UNRELIABLE	= 0x08,
		RECONFIG	= 0x10	// Msg supports node reconfiguration
	};

	invocation_mode()			{ flags = 0; }
	invocation_mode(mode_flags flag)	{ flags = flag; }
	invocation_mode(uint_t flag)		{ flags = flag; }
	invocation_mode(arg_info &info);	// Initialize from stub params

	~invocation_mode()	{ }

	bool	is_oneway()
		{ return ((flags & ONEWAY) == ONEWAY); }
	bool	is_synchronous()
		{ return ((flags & SYNCHRONOUS) == SYNCHRONOUS); }
	bool	is_nonblocking()
		{ return ((flags & NONBLOCKING) == NONBLOCKING); }
	bool is_unreliable()
		{ return ((flags & UNRELIABLE) == UNRELIABLE); }
	bool is_reconfig()
		{ return ((flags & RECONFIG) == RECONFIG); }

	void    set_oneway() 		{ flags |= ONEWAY; }
	void	set_twoway()		{ flags &= ~ONEWAY; }
	void    set_synchronous() 	{ flags |= SYNCHRONOUS; }
	void    set_nonblocking() 	{ flags |= NONBLOCKING; }
	void    set_unreliable() 	{ flags |= UNRELIABLE; }
	void	set_reconfig()		{ flags |= RECONFIG; }
	uint_t 	get_flags() 		{ return (flags); }

	os::mem_alloc_type	nonblocking_type()
	    { return (is_nonblocking() ? os::NO_SLEEP : os::SLEEP); }

private:
	uint_t flags;
};

#endif	/* _INVOCATION_MODE_H */
