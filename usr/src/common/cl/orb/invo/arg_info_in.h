/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  arg_info_in.h
 *
 */

#ifndef _ARG_INFO_IN_H
#define	_ARG_INFO_IN_H

#pragma ident	"@(#)arg_info_in.h	1.7	08/05/20 SMI"

inline
arg_info::arg_info(CORBA::TypeId tid, uint_t method_num, arg_desc *descp,
    ArgMarshalFuncs *marshalfuncp, arg_sizes *sizesp, uint_t inv_mode) :
	id(tid),
	method(method_num),
	desc(descp),
	funcp(marshalfuncp),
	argsizesp(sizesp),
	invo_mode(inv_mode)
{
}

//
// Accessor methods to hide the complexity
//

//
// argc - returns the number of method arguments including the return value
//
inline uint_t
arg_info::argc()
{
	return (argsizesp->num_params);
}

//
// rdesc - returns the arg_desc for return parameter
//
inline arg_desc
arg_info::rdesc()
{
	return (desc[0]);
}

//
// argdesc - returns a pointer to the descriptors for actual parameters
//
inline arg_desc *
arg_info::argdesc()
{
	return (&desc[1]);
}

//
// marshal_funcs - returns a pointer to a list of structures supporting
// marshal time operations.
//
inline ArgMarshalFuncs *
arg_info::marshal_funcs()
{
	return (funcp);
}

//
// get_argsizesp - returns a pointer to a structure describing argument sizes
//
inline arg_sizes *
arg_info::get_argsizesp()
{
	return (argsizesp);
}

//
// get_invo_mode - returns the method invocation mode
//
inline invocation_mode
arg_info::get_invo_mode()
{
	return (invo_mode);
}

#endif	/* _ARG_INFO_IN_H */
