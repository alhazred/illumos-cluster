/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ARG_INFO_H
#define	_ARG_INFO_H

#pragma ident	"@(#)arg_info.h	1.9	08/05/20 SMI"

#include <orb/invo/invo_args.h>
#include <orb/invo/invocation_mode.h>
#include <orb/invo/argfuncs.h>

//
// arg_info - provides a means for obtaining information about
// a method and its arguments.
//
class arg_info {
public:
	arg_info(CORBA::TypeId tid, uint_t method_num, arg_desc *descp,
	    ArgMarshalFuncs *marshalfuncp, arg_sizes *sizesp, uint_t inv_mode);

	// Return the number of method arguments including the return value.
	uint_t		argc();

	// Return desc[0] which stores the arg_desc for the return parameter.
	arg_desc	rdesc();

	// Returns an array of descriptors for actual parameters. The array
	// starting from desc[1] onwards stores the descriptors for the
	// actual parameters.
	arg_desc	*argdesc();

	ArgMarshalFuncs	*marshal_funcs();

	// Return pointer to structure describing argument sizes.
	arg_sizes	*get_argsizesp();

	invocation_mode	get_invo_mode();

	CORBA::TypeId	id;
	uint_t		method;

private:
	arg_desc	*desc;
	ArgMarshalFuncs	*funcp;
	arg_sizes	*argsizesp;
	invocation_mode	invo_mode;
};

#include <orb/invo/arg_info_in.h>

#endif	/* _ARG_INFO_H */
