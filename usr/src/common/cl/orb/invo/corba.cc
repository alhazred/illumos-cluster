//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)corba.cc	1.94	08/07/05 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

//
// Base object, type, Environment, Exception support.
//

//
// Protect includes so that this file can be used with a different set
// of include files.
//

#include <orb/invo/common.h>
#include <orb/object/schema.h>
#include <orb/invo/invocation.h>
#include <orb/handler/handler.h>
#include <sys/os.h>
#include <sys/mc_probe.h>
#include <orb/fault/fault_injection.h>

#include <h/version.h>

#ifdef	_KERNEL_ORB
#include <orb/xdoor/rxdoor.h>
#include <orb/member/members.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb_conf.h>
#endif

//	Predefined object type ids
// Keep 0 free to catch/avoid uninitialized ids
//
// The CORBA object id must be defined prior to _CORBA_Object_type_index
//
uint32_t _Ix__tid(CORBA_Object_0)[4] = { 0, 0, 0, 1 };

//
// class CORBA::Object methods
//

CORBA::Object::Object()
{
}

CORBA::Object::~Object()
{
}

//
// CORBA::Object is a special case object.
// The IDL compiler internally defines a CORBA::Object interface
// so we can't redefine CORBA::Object using the IDL compiler directly.
// We do want to provide certain methods on CORBA::Object though for
// internal use so we have a corba.idl file to generate the code and
// then we need to copy it here and fix up the names slightly.
//
// Define stub code.
// Note that we depend on version.idl being compiled
// and linked with the ORB for the inf_descriptor marshal functions.
//
extern MarshalInfo_complex _inf_typeid_ArgMarshalFuncs;
extern MarshalInfo_complex _inf_descriptor_ArgMarshalFuncs;

static arg_desc _CORBA_Object__get_dynamic_descriptor_pdesc[2] = {
	0x2, 0x5
};
static arg_sizes _CORBA_Object__get_dynamic_descriptor_argsizes = {
	2, 4, 0, 0, 0, 2, 0, 0
};
static ArgMarshalFuncs _CORBA_Object__get_dynamic_descriptor_pfunc[2] = {
	ArgMarshalFuncs(&_inf_typeid_ArgMarshalFuncs),		//lint !e708
	ArgMarshalFuncs(&_inf_descriptor_ArgMarshalFuncs)	//lint !e708
};
static arg_info _CORBA_Object__get_dynamic_descriptor_pinfo(
	_Ix__tid(CORBA_Object_0),
	0,
	_CORBA_Object__get_dynamic_descriptor_pdesc,
	_CORBA_Object__get_dynamic_descriptor_pfunc,
	&_CORBA_Object__get_dynamic_descriptor_argsizes,
	invocation_mode::NONE);

//
// Stub routine.
//
inf_descriptor *
CORBA::Object_stub::_get_dynamic_descriptor(const inf_typeid tid,
    Environment &_environment)
{
	MC_PROBE_0(CORBA_Object__get_dynamic_descriptor_start,
		" CORBA_Object clustering", "");
	handler *_handlerp = _handler();
	ExceptionStatus _status;
	CORBA::Object_ptr this_obj;
	inf_descriptor *_result = NULL;

	do {
		this_obj = (CORBA::Object_ptr)
		    _handlerp->local_invoke(_Ix__tid(CORBA_Object_0),
			_deep_type(), _environment);
		if (CORBA::is_nil(this_obj)) {
			ArgValue _arg[2];
			_arg[0].t_addr = &_result;
			_arg[1].t_addr = tid;
			_status = _handlerp->invoke(
			    _CORBA_Object__get_dynamic_descriptor_pinfo,
			    _arg, _deep_type(), _environment);
		} else if (_environment.exception() != NULL) {
			_status = CORBA::LOCAL_EXCEPTION;
			break;
		} else {
			InterfaceDescriptor *infp =
			    OxSchema::schema()->descriptor(
				(const CORBA::TypeId)tid, NULL);
			if (infp != NULL)
				_result = infp->get_inf_descriptor();
			_status = _handlerp->local_invoke_done(_environment);
		}
	} while (_status == CORBA::RETRY_EXCEPTION);

	MC_PROBE_0(CORBA_Object__get_dynamic_descriptor_end,
		" CORBA_Object clustering", "");
	return (_result);
}

//
// Marshal time support functions for CORBA::octet_seq_t
//

void
_CORBA_octet_seq_t_put(service &_b, void *voidp)
{
	CORBA::octet_seq_t *sequencep = (CORBA::octet_seq_t *)voidp;
	_b.put_seq(sequencep, (unsigned)sizeof (uint8_t));
}

void
_CORBA_octet_seq_t_get(service &_b, void *voidp)
{
	CORBA::octet_seq_t *sequencep = (CORBA::octet_seq_t *)voidp;
	_b.get_seq(sequencep, (unsigned)sizeof (uint8_t));
}

void *
_CORBA_octet_seq_t_makef()
{
	return (new CORBA::octet_seq_t);
}

void
_CORBA_octet_seq_t_freef(void *voidp)
{
	CORBA::octet_seq_t *sequencep = (CORBA::octet_seq_t *)voidp;
	delete sequencep;
}

void
_CORBA_octet_seq_t_release(void *voidp)
{
	CORBA::octet_seq_t *sequencep = (CORBA::octet_seq_t *)voidp;
	sequencep->load(0, 0, NULL, false);
}

uint_t
_CORBA_octet_seq_t_marshal_size(void *voidp)
{
	CORBA::octet_seq_t *sequencep = (CORBA::octet_seq_t *)voidp;
	return (sequencep->_marshal_size() +
		(sequencep->length() * (unsigned)sizeof (uint8_t)));
}

MarshalInfo_complex _CORBA_octet_seq_t_ArgMarshalFuncs = {
	&_CORBA_octet_seq_t_put,
	&_CORBA_octet_seq_t_get,
	&_CORBA_octet_seq_t_makef,
	&_CORBA_octet_seq_t_freef,
	&_CORBA_octet_seq_t_release,
	NULL,
	&_CORBA_octet_seq_t_marshal_size
};

extern MarshalInfo_object _CORBA_Object_ArgMarshalFuncs;

//
// Marshal time support functions for CORBA::object_seq_t
//

void
_CORBA_object_seq_t_put(service &_b, void *voidp)
{
	CORBA::object_seq_t *sequencep = (CORBA::object_seq_t *)voidp;
	sequencep->_put(_b);
}

void
_CORBA_object_seq_t_get(service &_b, void *voidp)
{
	CORBA::object_seq_t *sequencep = (CORBA::object_seq_t *)voidp;
	sequencep->_get(_b, &_CORBA_Object_ArgMarshalFuncs);
}

void *
_CORBA_object_seq_t_makef()
{
	return (new CORBA::object_seq_t);
}

void
_CORBA_object_seq_t_freef(void *voidp)
{
	CORBA::object_seq_t *sequencep = (CORBA::object_seq_t *)voidp;
	delete sequencep;
}

void
_CORBA_object_seq_t_release(void *voidp)
{
	CORBA::object_seq_t *sequencep = (CORBA::object_seq_t *)voidp;
	sequencep->load(0, 0, NULL, false);
}

void
_CORBA_object_seq_t_unput(void *voidp)
{
	CORBA::object_seq_t *sequencep = (CORBA::object_seq_t *)voidp;
	sequencep->_unput();
}

uint_t
_CORBA_object_seq_t_marshal_size(void *voidp)
{
	CORBA::object_seq_t *sequencep = (CORBA::object_seq_t *)voidp;
	return (sequencep->_marshal_size() +
		0);
}

MarshalInfo_complex _CORBA_object_seq_t_ArgMarshalFuncs = {
	&_CORBA_object_seq_t_put,
	&_CORBA_object_seq_t_get,
	&_CORBA_object_seq_t_makef,
	&_CORBA_object_seq_t_freef,
	&_CORBA_object_seq_t_release,
	&_CORBA_object_seq_t_unput,
	&_CORBA_object_seq_t_marshal_size
};

static arg_desc _CORBA_Object__generic_method_pdesc[3] = {
	0xc2, 0xb, 0xb
};
static arg_sizes _CORBA_Object__generic_method_argsizes = {
	3, 0, 0, 0, 0, 2, 0, 2
};
ArgMarshalFuncs _CORBA_Object__generic_method_pfunc[3] = {
	ArgMarshalFuncs(&_CORBA_octet_seq_t_ArgMarshalFuncs),	//lint !e708
	ArgMarshalFuncs(&_CORBA_object_seq_t_ArgMarshalFuncs),	//lint !e708
	ArgMarshalFuncs()					//lint !e708
};
arg_info _CORBA_Object__generic_method_pinfo(
	_Ix__tid(CORBA_Object_0),
	1,
	_CORBA_Object__generic_method_pdesc,
	_CORBA_Object__generic_method_pfunc,
	&_CORBA_Object__generic_method_argsizes,
	invocation_mode::NONE);

//
// Stub routine.
//
void
CORBA::Object_stub::_generic_method(octet_seq_t &data, object_seq_t &objs,
    Environment &_environment)
{
	MC_PROBE_0(CORBA_Object__generic_method_start,
		" CORBA_Object clustering", "");
	handler *_handlerp = _handler();
	CORBA::Object_ptr this_obj;
	ExceptionStatus _status;

	do {
		this_obj = (CORBA::Object_ptr)
		    _handlerp->local_invoke(_Ix__tid(CORBA_Object_0),
			_deep_type(), _environment);
		if (CORBA::is_nil(this_obj)) {
			ArgValue _arg[3];
			_arg[1].t_addr = &data;
			_arg[2].t_addr = &objs;
			_status = _handlerp->invoke(
			    _CORBA_Object__generic_method_pinfo,
			    _arg, _deep_type(), _environment);
		} else if (_environment.exception() != NULL) {
			_status = CORBA::LOCAL_EXCEPTION;
			break;
		} else {
			this_obj->_generic_method(data, objs, _environment);
			_status = _handlerp->local_invoke_done(_environment);
		}
	} while (_status == CORBA::RETRY_EXCEPTION);

	MC_PROBE_0(CORBA_Object__generic_method_end,
		" CORBA_Object clustering", "");
}

static InterfaceDescriptor::VersionTable _Ix_type_index(CORBA_Object)[] = {
	{ NULL, 0 },
	{ NULL, 0 }
};

void *
_Ix_CreateProxy(CORBA_Object)(handler *handlerp, InterfaceDescriptor *infp)
{
	return (new proxy<CORBA::Object_stub>(handlerp, infp));
}

//
// Provide a structure containing all of the information needed to
// support marshal time operations on the CORBA::Object.
//
MarshalInfo_object	_CORBA_Object_ArgMarshalFuncs = {
	&_Ix_CreateProxy(CORBA_Object),
	_Ix__tid(CORBA_Object_0)
};

//
// Skel code for _get_dynamic_descriptor() method.
// Note that this code is basically the same as
// idlversion::_get_dynamic_descriptor() except we are operating on
// the object of interest rather than an idlversion object.
// See OxSchema::descriptor() for details.
//
static void
_CORBA_Object__get_dynamic_descriptor_receive(void *objectp, service &_stream)
{
	MC_PROBE_0(CORBA_Object__get_dynamic_descriptor_skel_start,
		" CORBA::Object clustering", "");
	CORBA::Object_ptr _this = (CORBA::Object_ptr)objectp;
	ArgValue _arg[2];
	inf_descriptor *_result = NULL;
	inf_typeid tid;
	_arg[1].t_addr = tid;
	_stream.unmarshal_receive(_CORBA_Object__get_dynamic_descriptor_pinfo,
	    _arg);
	FAULTPT_ORPHAN(FAULTNUM_ORB_GET_DESCRIPTOR, FaultFunctions::generic);
	InterfaceDescriptor *infp = OxSchema::schema()->descriptor(
	    (const CORBA::TypeId)tid, NULL);
	if (infp != NULL)
		_result = infp->get_inf_descriptor();
	_arg[0].t_addr = &_result;
	_stream.send_reply(_this, _CORBA_Object__get_dynamic_descriptor_pinfo,
	    _arg);
	MC_PROBE_0(CORBA_Object__get_dynamic_descriptor_skel_end,
		" CORBA::Object clustering", "");
}

static void
_CORBA_Object__generic_method_receive(void *objectp, service &_stream)
{
	MC_PROBE_0(CORBA_Object__generic_method_skel_start,
		" CORBA_Object clustering", "");
	CORBA::Object_ptr _this = (CORBA::Object_ptr)objectp;
	Environment &_environment = *(_stream.get_env());
	ArgValue _arg[3];
	CORBA::octet_seq_t data;
	_arg[1].t_addr = &data;
	CORBA::object_seq_t objs;
	_arg[2].t_addr = &objs;
	_stream.unmarshal_receive(_CORBA_Object__generic_method_pinfo, _arg);
	_this->_generic_method(data, objs, _environment);
	_stream.send_reply(_this, _CORBA_Object__generic_method_pinfo, _arg);
	MC_PROBE_0(CORBA_Object__generic_method_skel_end,
		" CORBA_Object clustering", "");
}

static InterfaceDescriptor::ReceiverFunc _CORBA_Object_receive_table[2] = {
	_CORBA_Object__get_dynamic_descriptor_receive,
	_CORBA_Object__generic_method_receive
};

static InterfaceDescriptor::ReceiverTable _Ix_index_func(CORBA_Object)[] = {
	{ 2, _CORBA_Object_receive_table },
};

InterfaceDescriptor _Ix__type(CORBA_Object)(
	"CORBA::Object_0",
	_Ix__tid(CORBA_Object_0),
	_Ix__tid(CORBA_Object_0),
	0,
	NULL,
	_Ix_type_index(CORBA_Object),
	_Ix_index_func(CORBA_Object),
	&_Ix_CreateProxy(CORBA_Object));

//
// Use the _ix_schema data structure to load info about the CORBA Object class
// into the overall schema.
//
static InterfaceDescriptor *_ix_schema[] = {
	&_CORBA_Object_type,
	NULL
};

//
// Lint complains about OxInitSchema not having any data members
// See bugid 4244933
//
static OxInitSchema _ix_init_schema(_ix_schema);	//lint !e1502

//
// _duplicate - every object inherits from CORBA::Object.
// Increment the object reference count.
//
CORBA::Object_ptr
CORBA::Object::_duplicate(CORBA::Object_ptr object_p)
{
	return ((CORBA::Object_ptr)(is_nil(object_p) ? nil :
	    object_p->_handler()->duplicate(object_p,
		_Ix__tid(CORBA_Object_0))));
}

//
// _ox_cast - Cast is assumed always to be called on a non-nil object reference
// which has methods and with known type ids.
// This is called by the stub method for local invocations.
// NULL is returned if the object cannot be cast to the requested type.
//
void *
_ox_cast(CORBA::Object_ptr object_p, CORBA::TypeId ancestor_tid)
{
	ASSERT(object_p != NULL);
	//
	// Note that we are using the interface descriptor that is
	// known at the time the stub code for the object was compiled
	// rather than the dynamic interface descriptor.
	// However, we don't allow invocations if the server object
	// doesn't support them (newer client with older server).
	//
	InterfaceDescriptor &idesc = *object_p->_interface_descriptor();
	int inf_index = idesc.lookup_type_index(ancestor_tid, true);
	if (inf_index < 0)
		return (NULL);
	InterfaceDescriptor *infp = object_p->_deep_type();
	if (infp != &idesc && !infp->is_type_supported(ancestor_tid))
		return (NULL);
	return (idesc.cast(object_p->_this_ptr(), (uint_t)inf_index));
}

//
// _ox_equal_tid - Compare two type ids. All tids use MD5 tids
//
bool
_ox_equal_tid(CORBA::TypeId tid1, CORBA::TypeId tid2)
{
	ASSERT(tid1 != NULL);
	ASSERT(tid2 != NULL);
	//
	// Optimization for the case the two tid ptrs are same
	// This is common because we allocate tids statically
	//
	if (tid1 == tid2) {
		return (true);
	}
	// A type id contains 4 words.
	// The full check compares each word.
	//
	for (int i = 0; i < 4; i++, tid1++, tid2++) {
		if (*tid1 != *tid2) {
			return (false);
		}
	}
	return (true);
}

//
// _ox_put_tid - Put a type id into a marshal buffer.
// All TypeId's are MD5 digests.
//
void
_ox_put_tid(service &serv, CORBA::TypeId tid)
{
	serv.put_bytes(tid, (uint_t)sizeof (CORBA::MD5_Tid));
}

//
// _ox_get_tid - Get a type id from the marshal buffer.
// Use the given typeid buffer (all TypeId's are MD5 digests)
//
void
_ox_get_tid(service &serv, CORBA::TypeId tid)
{
	serv.get_bytes(tid, (uint_t)sizeof (CORBA::MD5_Tid));
}

bool
CORBA::Object::_equiv(Object_ptr ob2)
{
	return (_handler()->equiv(this, ob2));
}

uint_t
CORBA::Object::_hash(uint_t sz)
{
	return (_handler()->hash(sz));
}

void *
CORBA::Object::_this_ptr()
{
	return (this);
}

//
// Return the compiled-in InterfaceDescriptor pointer for this class.
//
InterfaceDescriptor *
CORBA::Object::_interface_descriptor()
{
	return (&_CORBA_Object_type);
}

//
// Return the dynamic InterfaceDescriptor pointer for this class.
//
InterfaceDescriptor *
CORBA::Object::_deep_type()
{
	return (_interface_descriptor());
}

//
// Return the version number of the given object reference or 0 if nil.
//
int
CORBA::Object::_version(CORBA::Object_ptr object_p)
{
	return (is_nil(object_p) ? -1 :
	    object_p->_deep_type()->get_version(_Ix__tid(CORBA_Object_0)));
}

//
// Return the type info for CORBA::Object.
//
CORBA::type_info_t *
CORBA::Object::_get_type_info(int v)
{
	return (_Ix__type(CORBA_Object).get_type_info(v));
}

generic_proxy *
CORBA::Object::_this_component_ptr()
{
	ASSERT(0);
	return (NULL);
}

bool
CORBA::Object::_is_local()
{
	return (_handler()->is_local());
}

bool
CORBA::Object::_supports_type(type_info_t *tp)
{
	return (_deep_type()->is_type_supported(
	    ((InterfaceDescriptor *)tp)->get_tid()));
}

//
// Default implementation for generic method for those interfaces which
// don't need it.
//
void
CORBA::Object::_generic_method(octet_seq_t &, object_seq_t &, Environment &e)
{
	// Throw an unimplemented method exception.
	e.system_exception(VERSION());
}

//
// Release an object reference.  This operation is just forwarded on
// to the exchange.  The this pointer is passed as a parameter so that
// the exchange can decide to delete this object.
//
void
CORBA::release(CORBA::Object_ptr obj)
{
	if (is_not_nil(obj)) {
		obj->_handler()->release(obj);
	}
}

//
// Return a proxy object of the given type.
//
CORBA::Object_ptr
CORBA::create_proxy_obj(type_info_t *tp, handler *hp)
{
	if (tp == NULL || hp == NULL) {
		return (CORBA::Object::_nil());
	}
	CORBA::Object_ptr objp =
	    ((InterfaceDescriptor *)tp)->create_proxy_obj(hp,
		(InterfaceDescriptor *)tp);
	if (is_not_nil(objp)) {
		hp->new_reference();
	}
	return (objp);
}

//
// exception_data macro - declares fixed data for exceptions,
// and avoids error prone manual declarations.
//
// The id's rely on the large id name space to prevent collisions.
// The convention is to use the major number of the system exception
// in the type id. The exceptions are declared in corba.h
// The IDL compiler generates the type ids for user exceptions.
//
#define	exception_data(except) {{except ## _major, 0, 0, 0}, #except}

exception_major_data	major_translation[] = {
    exception_data(UserException),
    exception_data(SystemException),
    exception_data(BAD_PARAM),
    exception_data(COMM_FAILURE),
    exception_data(INV_OBJREF),
    exception_data(WOULDBLOCK),
    exception_data(RETRY_NEEDED),
    exception_data(PRIMARY_FROZEN),
    exception_data(VERSION_Exception),
    exception_data(CTX_EACCESS),
    exception_data(NS_EPERM)
};

//
// class CORBA::Exception methods
//

//
// Default Destructor - most exceptions do nothing in the destructor,
// and those exceptions use this default destructor. However a small number
// of UserException objects need to do something, and they provide their
// own destructor.
//
// Lint complains about not zeroing some pointers in destructor
//lint -e1540
CORBA::Exception::~Exception()
{
}
//lint +e1540

//
// class CORBA::SystemException
//

CORBA::SystemException::SystemException(uint_t major_, uint_t minor_,
    CompletionStatus compstat) :
	Exception(major_, major_translation[major_].tid(),
	    major_translation[major_].name),
	_minor_(minor_),
	_completed_(compstat)
{
}

bool
CORBA::SystemException::is_system_exception()
{
	return (true);
}

CORBA::SystemException *
CORBA::SystemException::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && exceptp->is_system_exception()) {
		return ((CORBA::SystemException *)exceptp);
	} else {
		return (NULL);
	}
}

static const char *
completion_status_str(CORBA::CompletionStatus c)
{
	switch (c) {
	case CORBA::COMPLETED_NO:
		return ("NO");
	case CORBA::COMPLETED_YES:
		return ("YES");
	case CORBA::COMPLETED_MAYBE:
		return ("MAYBE");
	default :
		ASSERT(!"Invalid CompletionStatus");
	}
	return (NULL);
}

void
CORBA::SystemException::print_exception(char *msg)
{
	void (*prf)(const char *, ...);
	prf = os::printf;

#ifdef _KERNEL
	if (*msg == '!') {
		prf = os::prom_printf;
		msg++;
	}
#endif

#ifndef _KERNEL
	prf("%s SystemException: %s major %d minor %d %s completed %s\n",
	    msg,  _name() ? _name() : "<Null Name>", _major(), _minor(),
	    strerror((int)_minor()) ? strerror((int)_minor()) : "",
	    completion_status_str(completed()));
#else
	    // XX WOuld be nice to have a kernel strerror
	prf("%s SystemException: %s major %d minor %d completed %s\n",
	    msg,  _name() ? _name() : "<Null Name>", _major(), _minor(),
	    completion_status_str(completed()));
#endif
}

void
CORBA::SystemException::_put(service &_b) const
{
	_put(_b.se->get_MainBuf());
}

void
CORBA::SystemException::_put(MarshalStream &_m) const
{
	_m.put_unsigned_long(CORBA::SYSTEM_EXCEPTION);
	_m.put_unsigned_long(_major_);
	_m.put_unsigned_long(_minor_);
	_m.put_long((int32_t)_completed_);
}

//
// class CORBA::UserException
//

bool
CORBA::UserException::is_system_exception()
{
	return (false);
}

CORBA::UserException *
CORBA::UserException::_exnarrow(Exception *exceptp)
{
	if (exceptp != NULL && !exceptp->is_system_exception()) {
		return ((CORBA::UserException *)exceptp);
	} else {
		return (NULL);
	}
}

void
CORBA::UserException::_put(service &serv) const
{
	serv.put_unsigned_long(CORBA::USER_EXCEPTION);
	_ox_put_tid(serv, tid);
}

void
#ifdef DEBUG
CORBA::UserException::print_exception(char *msg)
#else
CORBA::UserException::print_exception(char *)
#endif
{
#ifdef DEBUG
	void (*prf)(const char *, ...);
	prf = os::printf;

#ifdef _KERNEL
	if (*msg == '!') {
		prf = os::prom_printf;
		msg++;
	}
#endif

	prf("%s UserException: %s major %d\n",
	    msg, _name() ? _name() : "<Null Name>", _major());
#endif
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::BAD_PARAM::~BAD_PARAM()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::COMM_FAILURE::~COMM_FAILURE()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::INV_OBJREF::~INV_OBJREF()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::WOULDBLOCK::~WOULDBLOCK()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::RETRY_NEEDED::~RETRY_NEEDED()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::PRIMARY_FROZEN::~PRIMARY_FROZEN()
{
}

CORBA::VERSION::~VERSION()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::CTX_EACCESS::~CTX_EACCESS()
{
}

//
// An ancestor class has a virtual destructor.
// Avoid multiple copies by defining this function.
//
CORBA::NS_EPERM::~NS_EPERM()
{
}

//
// Environment constructor - used for a request on the client node.
//
Environment::Environment() :
	trans_ctxp(NULL),		// No transaction
#ifdef DEBUG
	needs_checking(false),
#endif
	nonblocking(false),		// Blocking allowed
	service_freezing(false),
	freeze_retry(false),
#ifdef _KERNEL_ORB
	source_node(orb_conf::current_id_node()),
#endif
	_ptr(NULL),			// No exception
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zone_id(GLOBAL_ZONEID),
#ifdef _KERNEL
	zone_uniqid(GLOBAL_ZONEUNIQID),
#else
	zone_uniqid(0),
#endif	// _KERNEL
	cluster_id(PHYSICAL_CLUSTER_ID),
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	sys_ex(SystemException_major, 0, COMPLETED_MAYBE)
{
}

#ifdef	_KERNEL_ORB
//
// Environment constructor - used when a request from another node
// arrives on the server node.
//
#if (SOL_VERSION >= __s10) && !defined(UNODE)
Environment::Environment(
    ID_node &src_node, zoneid_t zoneid, uint32_t clid,
    uint64_t zone_uniq_id) :
#else
Environment::Environment(ID_node &src_node) :
#endif
	trans_ctxp(NULL),		// No transaction
#ifdef DEBUG
	needs_checking(false),
#endif
	nonblocking(false),		// Blocking allowed
	service_freezing(false),
	freeze_retry(false),
	source_node(src_node),
	_ptr(NULL),			// No exception
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zone_id(zoneid),
	cluster_id(clid),
	zone_uniqid(zone_uniq_id),
#endif
	sys_ex(SystemException_major, 0, COMPLETED_MAYBE)
{
}
#endif	// _KERNEL_ORB

Environment::~Environment()
{
	check_used("unchecked environment going out of scope");
	ASSERT(trans_ctxp == NULL);	// Cleared by framework
	ASSERT(!service_freezing);	// Cleared by framework
	done();
	ASSERT(_ptr == NULL);
}

//
// release_exception - disconnect the Environment object from any exception.
// When an exception is connected to an Environment object, the exception is
// destroyed when the Environment destructor executes. This method permits
// the exception to outlive the Environment object.
//
// Returns a pointer to any current exception and guarantees that the
// exception is not stored in the Environment object.
//
CORBA::Exception *
Environment::release_exception()
{
	Exception	*ep = _ptr;
	if (ep == &sys_ex) {
		//
		// System exceptions are stored inside the Environment object.
		// Create a system exception object outside the
		// Environment object.
		//
		ep = new SystemException(sys_ex._major(), sys_ex._minor(),
		    sys_ex.completed());
	}
	_ptr = NULL;
	has_checked();
	return (ep);
}

#ifdef _KERNEL_ORB
//
// is_orphan - returns true when the current invocation came from a client
// node that is dead. Invocations from dead nodes are called
// orphan requests.
//
// This test does not know anything about nested invocations.
//
// This test is only useful for the server side.
// While this test can be executed on the client side,
// a client side test will never find an orphan.
//
bool
Environment::is_orphan()
{
	return (!(members::the().still_alive(source_node)));
}

//
// is_local_client - returns true when the invocation originated
// on the same node.
//
// The use of this method eliminates "location transparency".
// Therefore this method should not be used outside the orb
// without consulting with those responsible for the orb.
// Justification is required for using this method.
//
bool
Environment::is_local_client()
{
	//
	// It is not necessary to check the node incarnation number,
	// because a node cannot receive an orphan message from a
	// prior incarnation of itself.
	//
	return (source_node.ndid == orb_conf::node_number());
}
#endif	// _KERNEL_ORB
