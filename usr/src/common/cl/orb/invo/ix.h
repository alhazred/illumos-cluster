/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _IX_H
#define	_IX_H

#pragma ident	"@(#)ix.h	1.31	06/05/24 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//


//
// IDL compiler (Ix) specific definitions.
// These are macros that Ix emits so that we can control the generated
// code by modifying this header file rather than by modifying the translator.
//
// For some of these macros, we'd prefer to use templates but
// some cfront-based compilers still can't handle mixing templates and
// nested classes.
//
// The above comment is no longer for the Solaris MC environment. We have
// have converted most macros to templates. The main motivation was
// that debugging programs with macros was very painful.
//

//
// Name concatentation
//

//
// __Ix_concat is necessary to handle nested expressions such as
// #define	_Ix__tid(T) _Ix_concat(_, _Ix_concat(T, _tid)
//
#define	__Ix_concat(a, b) a ## b
#define	_Ix_concat(a, b) __Ix_concat(a, b)

#define	_Ix_T_ptr(A)	_Ix_concat(A, _ptr)
#define	_Ix_T_var(A)	_Ix_concat(A, _var)
#define	_Ix_T_field(A)	_Ix_concat(A, _field)
#define	_Ix_T_out(A)	_Ix_concat(A, _out)

//
// Some preprocessors can't handle empty parameters, e.g., f(a,).
//
#define	_Ix_NullParameter

//
// Declare the types associated with a forward reference of
// an interface type.
//
#define	_Ix_Forward(A) \
	class A; \
	typedef A *_Ix_T_ptr(A); \
	typedef _A_var_<A, _Ix_T_ptr(A)> _Ix_T_var(A); \
	typedef _A_field_<A, _Ix_T_ptr(A)> _Ix_T_field(A); \
	typedef _A_out_<A, _Ix_T_ptr(A)> _Ix_T_out(A); \
	/*lint -esym(1058, A *&) */ /*CSTYLED*/

//
// Declare the extra names when defining a typedef for an interface.
//
#define	_Ix_InterfaceTypedef(A, T) \
	typedef _Ix_T_ptr(A) _Ix_T_ptr(T); \
	typedef _Ix_T_var(A) _Ix_T_var(T); \
	typedef _Ix_T_field(A) _Ix_T_field(T); \
	typedef _Ix_T_out(A) _Ix_T_out(T);

//
// Declare the type names for a typedef to an aggregate.
//
#define	_Ix_AggrTypedef(S, T) \
	typedef _Ix_T_var(S) _Ix_T_var(T); \
	typedef _Ix_T_out(S) _Ix_T_out(T);

//
// Declare the cast (narrow) functions for an exception, if any.
//
#define	_Ix_ExceptCast(T) \
	static T *_exnarrow(CORBA::Exception *e) \
		{ return (T*)_check_cast(e); } \
	static void *_check_cast(CORBA::Exception *);

#include <orb/invo/ix_tmpl.h>
#include <orb/idl_datatypes/managed_seq.h>
#include <orb/object/proxy.h>

#endif	/* _IX_H */
