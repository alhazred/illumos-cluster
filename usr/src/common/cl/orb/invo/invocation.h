/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _INVOCATION_H
#define	_INVOCATION_H

#pragma ident	"@(#)invocation.h	1.121	08/05/20 SMI"

#include <orb/flow/resource.h>
#include <orb/invo/invocation_mode.h>
#include <orb/flow/resource_defs.h>
#include <orb/buffers/marshalstream.h>
#include <orb/invo/arg_info.h>

#ifdef _KERNEL_ORB
#include <orb/invo/io_invos.h>
#endif	// _KERNEL_ORB

// forward declaration
class Xdoor;

#if defined(_FAULT_INJECTION)
class InvoTriggers;
#endif

union ArgValue {
	bool		t_boolean;
	char		t_char;
	uint8_t		t_octet;
	int16_t		t_short;
	uint16_t	t_unsigned_short;
	int32_t		t_long;
	uint32_t	t_unsigned_long;
	int64_t		t_longlong;
	uint64_t	t_unsigned_longlong;
#ifdef ORBFLOAT
	float		t_float;
	double		t_double;
#endif
	char		*t_string;
	const char	*t_const_string;
	CORBA::Object_ptr t_objref;
	const void	*t_addr;
	void		*t_vaddr;
};

//
// The service class embodies a duplex channel used for
// invocations. On the one hand there is a sendstream,
// used by the client for requests, and by the server for the
// reply. On the other hand there is a recstream, used by the
// client to extract the reply, and by the server to extract the
// request.
//
// service borrows most of the calls of both MarshalStream
// adding a few convenience functions of
// its own, as well as some internal functions for marshaling
// and unmarshaling invocation parameters through the stubs
// and skeletons.
//
class service {
public:
	// These are public to be read-only, use set_* for setting them
	sendstream *se;
	recstream *re;

	void set_sendstream(sendstream *);
	void set_recstream(recstream *);

	// Return the env pointer associated with the invocation
	Environment	*get_env();

#ifdef _KERNEL_ORB
	ID_node		&get_src_node();
#endif

	// Stream marshaling/unmarshalling for a call
	void		unmarshal_receive(arg_info &, ArgValue *);
	void		send_reply(CORBA::Object_ptr, arg_info &, ArgValue *);
	void		request_complete(arg_info &, ArgValue *);

	// main stream marshaling/unmarshalling operations.
	void		put_boolean(const bool bo);
	void		put_char(const char ch);
	void		put_octet(const uint8_t uc);
	void		put_short(const int16_t sh);
	void		put_unsigned_short(const uint16_t us);
	void		put_int(const int);
	void		put_long(const int32_t lo);
	void		put_unsigned_int(const uint_t);
	void		put_unsigned_long(const uint32_t ul);
	void		put_unsigned_longlong(const uint64_t ull);
	void		put_longlong(const int64_t ll);
#ifdef ORBFLOAT
	void		put_float(const float fl);
	void		put_double(const double db);
#endif
	void		put_pointer(const void *ptr);
	void		put_string(const char *st);
	void		put_seq_hdr(const GenericSequence *sh);
	void		put_seq(const GenericSequence *se, uint_t si);
	void		put_bytes(const void *bs, uint_t len);
	int		put_u_bytes(const caddr_t bs, uint_t len);
	void		debug_put_word(const uint32_t);

	bool		get_boolean();
	char		get_char();
	uint8_t		get_octet();
	int16_t		get_short();
	uint16_t	get_unsigned_short();
	int		get_int();
	int32_t		get_long();
	uint_t		get_unsigned_int();
	uint32_t	get_unsigned_long();
	int64_t		get_longlong();
	uint64_t	get_unsigned_longlong();
#ifdef ORBFLOAT
	float		get_float();
	double		get_double();
#endif
	void		*get_pointer();
	char		*get_string();
	void		get_string_cancel();
	void		get_seq_hdr(GenericSequence *);
	void		get_seq(GenericSequence *, uint_t size);
	void		get_seq_cancel(uint_t size);
	void		get_bytes(void *, uint_t length);
	int		get_u_bytes(caddr_t, uint_t length);
	void		debug_get_word(const uint32_t);

	// extra ops
	void		put_object(const CORBA::Object_ptr);
	CORBA::Object_ptr get_object(MarshalInfo_object *marshal_funcs);

	// delegate to the sendstream
	void	put_off_line(Region &, align_t = DEFAULT_ALIGN);
	void	put_off_line(Buf *, align_t = DEFAULT_ALIGN);

	// delegate to the recstream
	uint_t	get_off_line(Region &r);
	void	get_off_line_cancel();
	uint_t	get_off_line_hdr();
	void	get_off_line_bytes(void *p, uint_t len);
	void	get_off_line_u_bytes(caddr_t, uint_t len);

	// delegate to sendstream
	void	put_xdoor(Xdoor *xdp);

	// Delegate to recstream
	Xdoor	*get_xdoor();

	// Interfaces for reply buffer optimization.

	// Reply buffer optimization consists of 4 primary interfaces
	// (there is also a check_reply_buf_support - see below)
	// client is the side that owns the memory where the reply goes and
	// is the requestor of a read operation.
	// server is the side that owns the data that forms the reply and is
	// the responder to a read operation.

	// add_reply_buffer on the client specifies where the reply data goes
	//	usually called during the marshalling of the handler during
	//	a read request (on the sendstream)
	//
	// We are guaranteed that add_reply_buffer will eventually succeed
	// if check_reply_buf_support() succeeds.
	// check_reply_buf_support() and add_reply_buffer() must always
	// be in sync.
	//
	// get_reply_buffer on the server gets a local cookie which is used
	// 	during put_offline_reply. usually called during the unmarshal
	//	of the handler on the server (on the recstream)
	// put_offline_reply on the server tells the transport to send the
	//	data to the client and put it in the memory represented by
	//	the cookie. The cookie is the same as that returned by
	//	get_reply_buffer. usually called during the marshalling of
	//	the handler on the server during reply (on a sendstream)
	// get_offline_reply on the client corresponds to the put_offline_reply
	//	done on the server. usually called during the unmarshalling of
	//	the handler on the client during reply (on a recstream)

	// add_reply_buffer/get_reply_buffer are delegated to transport-specific
	// sendstream/recstream classes to be implemented in transport-specific
	// ways. put_offline_reply/get_offline_reply currently have generic
	// implementations using the virtual methods send_reply_buf (on both
	// replyio_server_t and sendstream) (XX all of this needs more work to
	// work better in mixed transport environment)

	// The protocol is for the client code to first call
	// check_reply_buf_support. If the client gets "true" as the answer,
	// it can then do an add_reply_buffer. The add_reply_buffer will always
	// succeed. For the server to know whether or not it should do a
	// get_reply_buffer, the client MUST also marshal a boolean or octet
	// indicating that an add_reply_buffer is going to be done.
	// The server unmarshals the boolean or octet. If it finds that the
	// client did an add_reply_buffer, only then does it call
	// get_reply_buffer.

	// Transport should create a replyio_client_t and call marshal_cookie
	// on with the sendstream.MainBuf. This marshals a cookie to identify
	// the struct buf when the reply is sent. Transport can also marshal
	// additional information, if needed.
	// The cookie returned by this routine is passed again to the transport
	// by the handler when doing get_offline_reply, when the transport can
	// release/free the cookie. The transport is free to allocate the
	// replyio_client_t in any manner (usually we do "new") as release and
	// get_offline_reply will call back into the transport to free them
	// If the handler is destructed before the transaction completes and
	// has a chance to call get_offline_reply or get_offline_reply_cancel,
	// it will call release() on the replyio_client_t.
	// (get_offline_reply may be called at most one time with this cookie)

	// The first argument is a struct buf that describes the memory where
	// the reply should go.
	// The second argument is a hint on what alignment is preferred
	// (XX We currently do not use this alignment and also we can infer
	// some of this from the struct buf itself)
	replyio_client_t *add_reply_buffer(struct buf *, align_t);

	// Server calls get_reply_buffer when unmarshalling the request,
	// to get a replyio_server_t. This routine unmarshals what was
	// marshalled in add_reply_buffer
	// Transport is free to allocate the replyio_server_t in any manner
	// (usually we do "new") as handlers call release() when done with it.
	// If there is any error in setting up the replyio, then should not
	// raise an exception but should return a replyio_server_t in
	// an invalid state, the corresponding put_offline_reply/send_reply_buf
	// will have a chance to handle the error appropriately
	// The cookie returned by this routine is passed again to the transport
	// by the handler when doing put_offline_reply.
	// When the handler is destructed it will call release() on the cookie
	// (put_offline_reply may be called zero or more times with this cookie)
	// See comments in put_offline_reply on how the reply is actually sent
	replyio_server_t *get_reply_buffer();

	// cancel version of get_reply_buffer. This is called when the
	// incoming invocation goes through unmarshal_cancel
	// Should advance through the recstream exactly as much as
	// get_reply_buffer would. However, this need not actually create
	// the replyio_server_t object. There will be no put_offline_reply
	// called later
	void get_reply_buffer_cancel();

	// Handler calls put_offline_reply during marshalling of the reply
	// to actually add the reply data to the sendstream.
	// The dest cookie is the same as what get_reply_buffer returned.
	// The first argument is a Buf that describes the data to be copied
	// into the reply buffer on the client. src has transfer of ownership
	// semantics. transport will call src->done() when finished with the
	// copying either into other transport buffers or onto the client.
	// Transport could put src in some internal list in the sendstream, so
	// the done can happen later after this routine returns
	// Handler is responsible to make sure that the data backed by src is
	// not deleted/freed until done() is called
	// (This is all very similar to semantics of put_off_line)
	// The second argument is a replyio_server_t cookie that is the same
	// as what was returned by get_reply_buffer.
	// In case the reply could not be sent over a particular endpoint and/or
	// if the ORB retries the marshalling of the reply over a different
	// endpoint, the same cookie can be used multiple times.
	// So the replyio_server_t cookie should not be released in this routine
	// In case there is an error copying the data due to bad data in src,
	// transport should set BAD_PARAM(EFAULT, COMPLETED_NO) exception
	// In case the endpoint corresponding to the original request is down,
	// transport should attempt to send the data over the endpoint
	// corresponding to the reply sendstream. If that fails too,
	// then transport should set RETRY_NEEDED(ECOMM, COMPLETED_NO) exception
	// Since we retry sending the reply over multiple endpoints if some
	// fail in the middle of transmitting a reply, the transport should
	// increment a generation count in replyio_server_t each time this
	// is called
	// put_offline_reply is implemented in generic sendstream class.
	// It does most of the common work and calls send_reply_buf method on
	// the replyio_server_t, the does the sending of the reply
	// If this fails, then the data is sent using regular put_off_line
	void put_offline_reply(Buf *src, replyio_server_t *dest);

	// client calls get_offline_reply during unmarshal of reply to
	// take the data off the recstream. This call matches the
	// put_offline_reply done on the server.
	// This routine is only called if add_reply_buffer returned success
	// The argument is the same cookie that was returned by add_reply_buffer
	// Transport should unmarshal data it may have marshalled in
	// put_offline_reply
	// The handler is passing ownership of the cookie back to the transport
	// through this call and will not call release() again
	void get_offline_reply(replyio_client_t *);

	// cancel version of get_offline_reply.
	// The handler is passing ownership of the cookie back to the transport
	// through this call and will not call release() again
	void get_offline_reply_cancel(replyio_client_t *);

	// Client calls this routine to find out if the reply optimization
	// can be used for the specified memory. Usually, add_reply_buffer
	// is the interface that the transport uses to enable or disable reply
	// optimization for a particular transfer. In some particular cases,
	// the handler may have to do extra work if it need to enable reply
	// optimization. In those cases, the handler will call this routine to
	// find out if there is a chance that the add_reply_buffer will succeed.
	// So this is a hint and the handler has to be prepared for
	// add_reply_buffer failing even though this routine succeeds.
	// Transports cannot assume that add_reply_buffer will not be called
	// if this routine returns false as most handlers call add_reply_buffer
	// without calling this
	// XX This routine/interface needs more work.
	// For now :
	// the first argument is the size of the total transfer
	// second argument iotype_t (above) says what kind of source data
	// third argument is a pointer to the source data descriptor based
	// on the iotype. XX Need more info here
	// The fourth argument indicates the type of operation
	// (OP_READ/OP_WRITE). The details of which are available in
	// marshalstream.h.
	bool check_reply_buf_support(size_t, iotype_t, void *, optype_t);

	service(recstream *r, sendstream *s, Environment *e);

	virtual ~service();

	// Called from the stubs directly also
	static void unput_object(const CORBA::Object_ptr);
	void get_object_cancel();

	// This supports validation that we have the correct service object type
	enum service_type_t { SERVICE, INVO, ONEWAY, TWOWAY };

	virtual service_type_t	get_service_type();

	void marshal_reply(arg_info &, ArgValue *, Environment *);

protected:
	void	put_in_param(invo_args::arg_type, invo_args::arg_mode,
		    ArgMarshalFuncs *, ArgValue *);

	void	put_inout_param(invo_args::arg_type, invo_args::arg_mode,
		    ArgMarshalFuncs *, ArgValue *);

	void	get_param(invo_args::arg_type, invo_args::arg_mode,
		    ArgMarshalFuncs *, ArgValue *, bool);

	static void	release_param_direct(invo_args::arg_type,
			    ArgMarshalFuncs *, ArgValue *);

	static void	release_param_indirect(invo_args::arg_type,
			    invo_args::arg_mode, ArgMarshalFuncs *, ArgValue *);

private:
	void release_reply(arg_info &, ArgValue *, bool);

	static void marshal_reply_roll_back(arg_info &, ArgValue *);

	Environment	*env;

	// Prevent assignments and pass by value
	service(const service &);
	service &operator = (service &);
};

//
// class invocation is the specialization of service used
// by the client. It contains additional support to specify
// "fast" receiving buffers. The destructor of an invocation
// automatically "disposes" of all the buffers
//
class invocation : public service {
public:
	void marshal_call(arg_info &, ArgValue *);
	void unmarshal_normal_return(arg_info &, ArgValue *, Environment *);
	void unmarshal_exception_return(arg_info &, ArgValue *, Environment *);
	void unmarshal_sys_exception_return(arg_info &, ArgValue *,
		Environment *);

	// Call on an exception and need to clean up out/inout/return types
	// implemented by calling unmarshal_roll_back
	static void exception_return(arg_info &, ArgValue *);

	invocation(sendstream *s, Environment *e);

#if defined(_FAULT_INJECTION)
	void		set_invotrigp(InvoTriggers *trigp);
	InvoTriggers	*get_invotrigp();
#endif

	virtual service_type_t	get_service_type();

private:
	void		get_result(invo_args::arg_type, ArgMarshalFuncs *,
			    ArgValue *);

	static void	clear_param_direct(invo_args::arg_type,
			    ArgMarshalFuncs *, ArgValue *);

	static void	clear_param_indirect(invo_args::arg_type,
			    ArgMarshalFuncs *, ArgValue *);

	static void marshal_call_rollback(arg_info &, ArgValue *);

	static void unmarshal_roll_back(arg_info &, ArgValue *, bool);

#if defined(_FAULT_INJECTION)
	// Provides the address of where to deliver fault info
	InvoTriggers	*invotrigp;
#endif
	// Prevent assignments and pass by value
	invocation(const invocation &);
	invocation &operator = (invocation &);
};

#ifdef _KERNEL_ORB
//
// service_oneway - server side specialization of class service to
// support oneway invocations.
//
class service_oneway : public service {
public:
	service_oneway(recstream *, Environment *);

	void		set_pool(resource_defs::resource_pool_t);
	resource_defs::resource_pool_t get_pool();

	virtual service_type_t	get_service_type();

private:
	//
	// The pool information must be preserved until
	// after the oneway invocation has completed, so that oneway
	// flow control can release reserved resources.
	//
	resource_defs::resource_pool_t	pool;

	// Prevent assignments and pass by value
	service_oneway(const service_oneway &);
	service_oneway &operator = (service_oneway &);
};

//
// service_twoway - server side specialization of class service to
// support twoway invocations.
//
// XXX - eventually all of the server side reply methods from class service
// XXX - should be moved here.
//
class service_twoway : public service {
public:
	service_twoway(recstream *, Environment *, slot_t slot);

	ushort_t	get_index_invo();

	virtual service_type_t	get_service_type();

	inbound_invo	*get_inbound_invo();

	// Return pointer to transport info
	serv2_transport_info *get_transport_info();

private:
	//
	// Need to retain this information in this object,
	// because we release the slot prior to sending the reply
	// and we may have to re-marshal the reply.
	//
	inbound_invo	inbound;

	// Recstream transport information. This information needs
	// to be retrieved from the recstream and stored with the
	// service object as it will be needed when the sendstream
	// for the reply is being created. At that time the recstream
	// would be gone.
	serv2_transport_info	transport_info;

	// Prevent assignments and pass by value
	service_twoway(const service_twoway &);
	service_twoway &operator = (service_twoway &);
};

#endif	// _KERNEL_ORB

#include <orb/invo/invocation_in.h>
#endif	/* _INVOCATION_H */
