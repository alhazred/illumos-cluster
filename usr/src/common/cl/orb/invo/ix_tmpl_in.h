/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  ix_tmpl_in.h
 *
 */

#ifndef _IX_TMPL_IN_H
#define	_IX_TMPL_IN_H

#pragma ident	"@(#)ix_tmpl_in.h	1.9	08/05/20 SMI"

#include <fault_numbers/faultnum_orb.h>
#include <fault_numbers/fault_numbers.h>

#ifdef	_FAULT_INJECTION
extern bool fault_triggered(uint32_t fault_num, void **fault_argpp,
    uint32_t *fault_argsizep);
#endif

//
// These functions have two implementation's (kernel-land and user-land)
// because cas32 and cas64 operations are only defined in kernel-land.
// The implementaion takes into account the concurrent assignments in a
// Multi-Threaded application.
//
// Assignment operator overloading for classes _A_field_, _T_var_,
// _A_var_.
//
//
// The incoming dumb pointer rhs_ptr has been accounted for the reference,
// and this reference is being handed over to smart pointer of type _A_field.
// This reference is released when the pointer is out of scope.
//

template<class A, class A_ptr>
inline _A_field_<A, A_ptr>&
_A_field_<A, A_ptr>::operator = (A_ptr rhs_ptr)
{
#ifdef	_KERNEL
#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just release one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		CORBA::release((CORBA::Object_ptr)rhs_ptr->_this_obj());
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
					    original_value, new_value);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
					    original_value, new_value);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		//
		// Must adjust the reference count of the original value.
		// It is always safe to release a NIL reference.
		//
		CORBA::release(((CORBA::Object_ptr)
		    original_value)->_this_obj());
	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to release the reference.
		//
		CORBA::release((CORBA::Object_ptr)rhs_ptr->_this_obj());
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	//
	// When a dumb pointer is passed we dont explicitly check for the values
	// because in any case we need release one of references.
	//
	lp->lock();
	CORBA::release((CORBA::Object_ptr)_ptr->_this_obj());
	_ptr = rhs_ptr;
	lp->unlock();
#endif	// _KERNEL
	return (*this);
}

template<class A, class A_ptr>
inline _A_field_<A, A_ptr>&
_A_field_<A, A_ptr>:: operator = (const _A_field_& rhs_field)
{
#ifdef	_KERNEL
	//
	// The object reference must be duplicated before
	// assigning the object reference, because the
	// object reference becomes visible upon assignment.
	// Another thread could then immediately do
	// anything with this object reference.
	//
	A::_duplicate(rhs_field._ptr);
#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_field._ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_field._ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just release one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		CORBA::release((CORBA::Object_ptr)rhs_field._ptr->_this_obj());
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
				    original_value, new_value);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
				    original_value, new_value);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		//
		// Must adjust the reference count of the original value.
		// It is always safe to release a NIL reference.
		//
		CORBA::release(((CORBA::Object_ptr)
		    original_value)->_this_obj());
	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to release the reference.
		//
		CORBA::release((CORBA::Object_ptr)rhs_field._ptr->_this_obj());
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	lp->lock();
	if (rhs_field._ptr != _ptr) {
		CORBA::release((CORBA::Object_ptr)_ptr->_this_obj());
		_ptr = A::_duplicate(rhs_field._ptr);
	}
	lp->unlock();
#endif	// KERNEL
	return (*this);
}

template<class A, class A_ptr>
inline _A_field_<A, A_ptr>&
_A_field_<A, A_ptr>:: operator = (const _A_var_<A, A_ptr>& rhs_var)
{
#ifdef	_KERNEL
	//
	// The object reference must be duplicated before
	// assigning the object reference, because the
	// object reference becomes visible upon assignment.
	// Another thread could then immediately do
	// anything with this object reference.
	//
	A::_duplicate(rhs_var._ptr);
#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_var._ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_var._ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just release one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		CORBA::release((CORBA::Object_ptr)rhs_var._ptr->_this_obj());
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
				    original_value, new_value);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
				    original_value, new_value);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		//
		// Must adjust the reference count of the original value.
		// It is always safe to release a NIL reference.
		//
		CORBA::release(((CORBA::Object_ptr)
		    original_value)->_this_obj());
	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to release the reference.
		//
		CORBA::release((CORBA::Object_ptr)rhs_var._ptr->_this_obj());
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	lp->lock();
	if (rhs_var._ptr != _ptr) {
		CORBA::release((CORBA::Object_ptr)_ptr->_this_obj());
		_ptr = A::_duplicate(rhs_var._ptr);
	}
	lp->unlock();
#endif	// _KERNEL
	return (*this);
}

//
// operator overloading of classes managing pointers to aggregate types.
//
template<class T, class T_ptr>
inline _T_var_<T, T_ptr>&
_T_var_<T, T_ptr>::operator = (const _T_var_& rhs_var)
{
#ifdef	_KERNEL
	//
	// new instance must be created before
	// assigning the object reference, because the
	// object reference becomes visible upon assignment.
	// Another thread could then immediately do
	// anything with this object reference.
	//
	T_ptr new_ptr = new T(*rhs_var._ptr);

#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_var._ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_var._ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just delete one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		delete new_ptr;
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
	    original_value, (uint64_t)new_ptr);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
	    original_value, (uint32_t)new_ptr);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		// Must delete the old reference
		//
		delete (T_ptr)original_value;
		original_value = NULL;

	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to delete the reference.
		//
		delete new_ptr;
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	lp->lock();
	if (rhs_var._ptr != _ptr) {
		delete _ptr;
		_ptr = new T(*rhs_var._ptr);
	}
	lp->unlock();
#endif	// _KERNEL
	return (*this);
}

//
// The incoming dumb pointer rhs_ptr is a pointer to a object,
// and this pointer is assigned to smart pointer of type _T_var_.
// This pointer is deleted when the pointer is out of scope.
//
template<class T, class T_ptr>
inline _T_var_<T, T_ptr>&
_T_var_<T, T_ptr>::operator = (T_ptr rhs_ptr)
{
#ifdef	_KERNEL
#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just delete one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		delete (T_ptr)rhs_ptr;
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
	    original_value, new_value);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
	    original_value, new_value);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		// Must delete the old reference
		//
		delete (T_ptr)original_value;
		original_value = NULL;
	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to delete the reference.
		//
		delete (T_ptr)rhs_ptr;
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	//
	// When a dumb pointer is passed we dont explicitly check for the values
	// because in any case we need delete one of references.
	//
	lp->lock();
	delete _ptr;
	_ptr = rhs_ptr;
	lp->unlock();
#endif	// KERNEL
	return (*this);
}

//
// operator overloading of classes representing managed object references.
//
template<class A, class A_ptr>
inline _A_var_<A, A_ptr>&
_A_var_<A, A_ptr>::operator = (const _A_var_& rhs_var)
{
#ifdef	_KERNEL
	//
	// The object reference must be duplicated before
	// assigning the object reference, because the
	// object reference becomes visible upon assignment.
	// Another thread could then immediately do
	// anything with this object reference.
	//
	A::_duplicate(rhs_var._ptr);
#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_var._ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_var._ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just release one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		CORBA::release((CORBA::Object_ptr)rhs_var._ptr->_this_obj());
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
					    original_value, new_value);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
					    original_value, new_value);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		//
		// Must adjust the reference count of the original value.
		// It is always safe to release a NIL reference.
		//
		CORBA::release(((CORBA::Object_ptr)
		    original_value)->_this_obj());
	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to release the reference.
		//
		CORBA::release((CORBA::Object_ptr)rhs_var._ptr->_this_obj());
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	lp->lock();
	if (rhs_var._ptr != _ptr) {
		CORBA::release((CORBA::Object_ptr)_ptr->_this_obj());
		_ptr = A::_duplicate(rhs_var._ptr);
	}
	lp->unlock();
#endif	// _KERNEL
	return (*this);
}

//
// The incoming dumb pointer rhs_ptr has been accounted for the reference,
// and this reference is being handed over to smart pointer of type _A_var_.
// This reference is released when the pointer is out of scope.
//
template<class A, class A_ptr>
inline _A_var_<A, A_ptr>&
_A_var_<A, A_ptr>::operator = (A_ptr rhs_ptr)
{
#ifdef	_KERNEL
#ifdef _LP64
	uint64_t	original_value = (uint64_t)_ptr;
	uint64_t	new_value = (uint64_t)rhs_ptr;
#else
	uint32_t	original_value = (uint32_t)_ptr;
	uint32_t	new_value = (uint32_t)rhs_ptr;
#endif
	//
	// Check if new value is same as old. If it is no need
	// to do an assignment. Just release one reference because
	// after the assignment, the result should be that there is just
	// one reference instead of two.
	//
	if (original_value == new_value) {
		CORBA::release((CORBA::Object_ptr)rhs_ptr->_this_obj());
		return (*this);
	}
	//
	// In order to safely work with compare-and-swap
	// in a multi-threaded environment, we need to
	// be able to tell whether we made a change. So keep
	// track of the old value as known to us.
	//
	// If the current value (&_ptr) matches original_value,
	// then replace current value with new_value.
	//
#ifdef _LP64
	uint64_t	old_value = os::cas64((uint64_t *)&_ptr,
					    original_value, new_value);
#else
	uint32_t	old_value = os::cas32((uint32_t *)&_ptr,
					    original_value, new_value);
#endif
#ifdef	_FAULT_INJECTION
	if (fault_triggered(FAULTNUM_ORB_SMARTPTR_SLEEP, NULL, NULL)) {
		int sleep_secs = 10;
		os::printf("\nArmed Fault: FAULTNUM_ORB_SMARTPTR_SLEEP\n");
		os::usecsleep(1000*1000*sleep_secs); // sleep 10 seconds
	}
#endif
	if (original_value == old_value) {
		//
		// We successfully made the change.
		//
		// Must adjust the reference count of the original value.
		// It is always safe to release a NIL reference.
		//
		CORBA::release(((CORBA::Object_ptr)
		    original_value)->_this_obj());
	} else {
		//
		// Since original_value != old_value, then somebody else won the
		// race to assign a value. It does not matter
		// which of simultaneously contending threads wins
		// the assignment. This thread did not make the assignment
		// so needs to release the reference.
		//
		CORBA::release((CORBA::Object_ptr)rhs_ptr->_this_obj());
	}
#else	// USER-LAND
	os::mutex_t *lp = ATOMIC_LOCK(this);
	//
	// When a dumb pointer is passed we dont explicitly check for the values
	// because in any case we need release one of references.
	//
	lp->lock();
	CORBA::release((CORBA::Object_ptr)_ptr->_this_obj());
	_ptr = rhs_ptr;
	lp->unlock();
#endif	// KERNEL
	return (*this);
}

#endif	/* _IX_TMPL_IN_H */
