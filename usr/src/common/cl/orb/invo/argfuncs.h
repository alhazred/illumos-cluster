/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ARGFUNCS_H
#define	_ARGFUNCS_H

#pragma ident	"@(#)argfuncs.h	1.14	08/05/20 SMI"

//
// This file defines the functions used to process invocation arguments
//

#include <sys/os.h>
#include <orb/object/proxy.h>

class service;
class handler;

//
// Define types for the functions that support marshal time operations
// on complex data items.
//

//
// MarshalFunc - marshals the data
//
typedef void (*MarshalFunc)(service &, void *data_address);

//
// UnMarshalFunc - unmarshals the data
//
typedef void (*UnMarshalFunc)(service &, void *data_address);

//
// MakeFunc - make a data item of this type.
//		Only exists for variable size data items.
//		Only used for an "out" param.
//
typedef void *(*MakeFunc)();

//
// FreeFunc - frees a data item of this type.
//		Only exists for variable size data items.
//		Only used for an "out" param.
//
typedef void (*FreeFunc)(void *data_address);

//
// ReleaseFunc - release a data item of this type.
//		Only exists for variable size data items.
//		Only used for an "inout" param.
//
typedef void (*ReleaseFunc)(void *data_address);

//
// MarshalRollBackf - after a marshal operation fails, this
//		function performs cleanup for this data type.
//		The marshal stream is discarded elsewhere,
//		So this function does not modify the marshalstream.
//
//		This function exists only to clean up object references
//		that were actually marshalled.
//
//		There are data items that can contain object references,
//		but do not alway contain object references. A sequence of
//		object references is an example. The sequence could be empty.
//
typedef void (*MarshalRollBackf)(void *data_address);

//
// MarshalSizeFunc - provides a marshal size estimate at marshal time.
//		Since it is an estimate, the result may be wrong.
//
//		Not all complex data items support this operation yet.
//
typedef uint_t (*MarshalSizeFunc)(void *data_address);

//
// MarshalInfo_complex - marshal time support functions for complex data types,
// which includes struct's, union's, string's, and sequence's.
//
// The functions that take a "void *" argument all require that this argument
// be the address of the data type.
//
struct MarshalInfo_complex {
	MarshalFunc		marshalf;
	UnMarshalFunc		unmarshalf;
	MakeFunc		makef;
	FreeFunc		freef;
	ReleaseFunc		releasef;
	MarshalRollBackf	marshalrollbackf;
	MarshalSizeFunc		marshal_sizef;
};

//
// This definition appears here to avoid header file
// circular dependency issues.
//
typedef uint32_t *type_identifier_p;

//
// MarshalInfo_object - marshal time support information for objects
//
struct MarshalInfo_object {
	generic_proxy::ProxyCreator	createf;
	type_identifier_p		tid;
};

//
// ArgMarshalFuncs - this union contains a pointer to a structure
// supporting marshal time processing for some argument type.
//
union ArgMarshalFuncs {
	ArgMarshalFuncs();
	ArgMarshalFuncs(MarshalInfo_complex *);
	ArgMarshalFuncs(MarshalInfo_object *);

	MarshalInfo_complex	*datfuncs;
	MarshalInfo_object	*objfuncs;
};

#include <orb/invo/argfuncs_in.h>

#endif	/* _ARGFUNCS_H */
