/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  invo_args_in.h
 *
 */

#ifndef _INVO_ARGS_IN_H
#define	_INVO_ARGS_IN_H

#pragma ident	"@(#)invo_args_in.h	1.8	08/05/20 SMI"

inline
arg_desc::arg_desc() :
    descriptor(0)
{
}

inline
arg_desc::arg_desc(uint_t desc) :
    descriptor(desc)
{
}

inline
arg_desc::arg_desc(invo_args::arg_type argtype, invo_args::arg_mode argmode,
    invo_args::arg_size_info argsize) :
	descriptor(((uint_t)argtype << ArgType_shift) |
	    ((uint_t)argsize << ArgSize_shift) |
	    argmode)
{
}

inline
/*CSTYLED*/
arg_desc::operator uint_t (void) const
{
	return ((uint_t)descriptor);
}

inline invo_args::arg_type
arg_desc::get_argtype()
{
	return ((invo_args::arg_type)(descriptor >> ArgType_shift));
}

inline invo_args::arg_mode
arg_desc::get_argmode()
{
	return ((invo_args::arg_mode)(descriptor & ArgMode_mask));
}

inline bool
arg_desc::is_arg_out_inout()
{
	return ((descriptor & invo_args::out_param) != 0);
}

inline bool
arg_desc::is_arg_in_inout()
{
	return ((descriptor & invo_args::in_param) != 0);
}

inline bool
arg_desc::is_unknown_size()
{
	return ((descriptor & ArgSize_mask) ==
	    ((int)invo_args::size_unknown << ArgSize_shift));
}

inline bool
arg_desc::is_fixed_size()
{
	return ((descriptor & ArgSize_mask) ==
	    ((int)invo_args::size_compile_time << ArgSize_shift));
}

inline bool
arg_desc::is_marshal_time_size()
{
	return ((descriptor & ArgSize_mask) ==
	    ((int)invo_args::size_marshal_time << ArgSize_shift));
}

#endif	/* _INVO_ARGS_IN_H */
