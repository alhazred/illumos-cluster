/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  io_invos_in.h
 *
 */

#ifndef _IO_INVOS_IN_H
#define	_IO_INVOS_IN_H

#pragma ident	"@(#)io_invos_in.h	1.12	08/05/20 SMI"

//
// invo_base methods
//

inline
invo_base::invo_base(ID_node &node, ushort_t state) :
	_node(node),
	_state(state),
	_threadid(0)
{
}

inline ID_node  &
invo_base::node()
{
	return (_node);
}

inline slot_t
invo_base::slot()
{
	return (_slot);
}

//
// outbound_invo methods
//

inline
outbound_invo::outbound_invo(ID_node &node, ushort_t state, invocation *invop) :
	invo_base(node, state),
	inv(invop)
{
}

inline
outbound_invo::~outbound_invo()
{
}

//
// outbound_invo_table methods
//

inline int
outbound_invo_table::hashslot(slot_t slot)
{
	return (slot & (SIZE - 1));
}

//
// inbound_invo methods
//

inline
inbound_invo::inbound_invo(ID_node &node, ushort_t state) :
	invo_base(node, state)
{
}

inline
inbound_invo::~inbound_invo()
{
}

//
// was_signaled - determines whether or not thread has processed a signal.
//
inline bool
inbound_invo::was_signaled()
{
	// This method is only called when preparing to send a reply
	ASSERT((_state & IB_REQUEST) != 0 && (_state & IB_REPLY) != 0);

	return ((_state & IB_SIGNAL) != 0);
}

//
// inbound_invo_table methods
//

inline int
inbound_invo_table::hashslot(slot_t slot)
{
	return (slot & (SIZE - 1));
}

#endif	/* _IO_INVOS_IN_H */
