/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _INVO_ARGS_H
#define	_INVO_ARGS_H

#pragma ident	"@(#)invo_args.h	1.18	08/05/20 SMI"

// This header file specifies information common to both the orb and
// the IDL compiler.

#include <sys/types.h>
#include <sys/os.h>

//
// arg_sizes - structure holds information defining sizes for a methods args
//
struct arg_sizes {
	uint_t	num_params;		// number of args including return arg
	uint_t	in_size;		// Cumulative size of known in args
	uint_t	in_unknown;		// number of unknown-sized in args
	uint_t	in_objtref;		// number of in object references
	uint_t	out_size;		// Cumulative size of known out args
	uint_t	out_unknown;		// number of unknown-sized out args
	uint_t	out_objtref;		// number of out object references
	uint_t	num_marshal_size;	// number of arguments requiring
					// marshal time size determination
};

//
// invo_args - provides a name space for constants and type definitions
// shared between the orb and IDL compiler.
// XXX - once C++ supports name spaces, this should be converted to a name space
//
class invo_args {
public:
	// These define type attribute values for method arguments
	//
	enum arg_type {
		t_addr,			// 0: Complex types
		t_object,		// 1: Object pointer
		t_boolean,		// 2:
		t_char,			// 3:
		t_octet,		// 4:
		t_short,		// 5:
		t_unsigned_short,	// 6:
		t_long,			// 7:
		t_unsigned_long,	// 8:
		t_longlong,		// 9:
		t_unsigned_longlong,	// a:
#ifdef ORBFLOAT
		t_float,
		t_double,
#endif
		t_string,		// b:
		t_void,			// c: No return value
		t_number_types		// d: Not a type, but a count of types
	};
	//
	// These define the direction for a method's arguments.
	// The values of these constants are known to arg_desc.
	// The value of inout_param must be a bit OR of in_param and out_param.
	//
	enum arg_mode {
		err_param = 0,
		in_param = 1,
		out_param = 2,
		inout_param = 3
	};
	//
	// These define when size is computed for this argument.
	//
	enum arg_size_info {
		size_unknown = 0,
		size_compile_time = 1,
		size_marshal_time = 2
	};
};

//
// arg_desc - combines argument information in a single byte.
// The low two bits contain an arg_mode.
// The next two bits contain an arg_size_info.
// The higher bits contain an arg_descriptor.
//
class arg_desc {
public:
	arg_desc();
	arg_desc(uint_t desc);
	arg_desc(invo_args::arg_type argtype, invo_args::arg_mode argmode,
	    invo_args::arg_size_info argsize);

	// This conversion exists so that IDL compiler can output an arg_desc
	// as an integer containing the packed values.
	//
	/*CSTYLED*/
	operator uint_t (void) const;

	invo_args::arg_type	get_argtype();
	invo_args::arg_mode	get_argmode();
	bool			is_arg_out_inout();
	bool			is_arg_in_inout();
	bool			is_unknown_size();
	bool			is_fixed_size();
	bool			is_marshal_time_size();

private:
	// Define constants for packing the fields into a single value.
	enum { ArgSize_shift = 2,
		ArgType_shift = 4,
		ArgMode_mask = 3,
		ArgSize_mask = 0xc
	};

	ushort_t	descriptor;
};

#include <orb/invo/invo_args_in.h>

#endif	/* _INVO_ARGS_H */
