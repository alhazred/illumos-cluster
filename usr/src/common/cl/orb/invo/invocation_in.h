/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  invocation_in.h
 *
 */

#ifndef _INVOCATION_IN_H
#define	_INVOCATION_IN_H

#pragma ident	"@(#)invocation_in.h	1.57	08/05/20 SMI"

#ifdef _KERNEL_ORB
inline ID_node &
service::get_src_node()
{
	return (env->get_src_node());
}
#endif

// put_boolean
inline void
service::put_boolean(const bool bo)
{
	se->put_boolean(bo);
}

// put_char
inline void
service::put_char(const char ch)
{
	se->put_char(ch);
}

// put_octet
inline void
service::put_octet(const uint8_t uc)
{
	se->put_octet(uc);
}

// put_short
inline void
service::put_short(const int16_t sh)
{
	se->put_short(sh);
}

// put_unsigned_short
inline void
service::put_unsigned_short(const uint16_t us)
{
	se->put_unsigned_short(us);
}

// put_int
inline void
service::put_int(const int i)
{
	se->put_long(i);
}

// put_long
inline void
service::put_long(const int32_t lo)
{
	se->put_long(lo);
}

// put_unsigned_int
inline void
service::put_unsigned_int(const uint_t i)
{
	se->put_unsigned_int(i);
}

// put_unsigned_long
inline void
service::put_unsigned_long(const uint32_t ul)
{
	se->put_unsigned_long(ul);
}

// put_unsigned_longlong
inline void
service::put_unsigned_longlong(const uint64_t ull)
{
	se->put_unsigned_longlong(ull);
}

// put_longlong
inline void
service::put_longlong(const int64_t ll)
{
	se->put_longlong(ll);
}

#ifdef ORBFLOAT
// put_float
inline void
service::put_float(const float fl)
{
	se->put_float(fl);
}

// put_double
inline void
service::put_double(const double db)
{
	se->put_double(db);
}
#endif // ORBFLOAT

inline void
service::put_pointer(const void *p)
{
	se->put_pointer(p);
}

// put_string
inline void
service::put_string(const char *st)
{
	se->put_string(st);
}

// put_seq_hdr
inline void
service::put_seq_hdr(const GenericSequence *sh)
{
	se->put_seq_hdr(sh);
}

// put_seq
inline void
service::put_seq(const GenericSequence *seq, uint_t si)
{
	se->put_seq(seq, si);
}

// put_bytes
inline void
service::put_bytes(const void *bs, uint_t len)
{
	se->put_bytes(bs, len);
}


// put_u_bytes
inline int
service::put_u_bytes(const caddr_t bs, uint_t len)
{
	return (se->put_u_bytes(bs, len));
}

// debug_put_word
inline void
service::debug_put_word(const uint32_t t)
{
	se->debug_put_word(t);
}

// put_off_line
inline void
service::put_off_line(Region &r, align_t a)
{
	se->put_off_line(r, a);
}

// put_off_line
inline void
service::put_off_line(Buf *b, align_t a)
{
	se->put_off_line(b, a);
}

inline void
service::put_xdoor(Xdoor *xdp)
{
	se->put_xdoor(xdp);
}

// get_boolean
inline bool
service::get_boolean()
{
	return (re->get_boolean());
}

// get_char
inline char
service::get_char()
{
	return (re->get_char());
}

// get_octet
inline uint8_t
service::get_octet()
{
	return (re->get_octet());
}

// get_short
inline int16_t
service::get_short()
{
	return (re->get_short());
}

// get_unsigned_short
inline uint16_t
service::get_unsigned_short()
{
	return (re->get_unsigned_short());
}

// get_int
inline int
service::get_int()
{
	return (re->get_int());
}

// get_long
inline int32_t
service::get_long()
{
	return (re->get_long());
}

// get_unsigned_int
inline uint_t
service::get_unsigned_int()
{
	return (re->get_unsigned_int());
}

// get_unsigned_long
inline uint32_t
service::get_unsigned_long()
{
	return (re->get_unsigned_long());
}

// get_longlong
inline int64_t
service::get_longlong()
{
	return (re->get_longlong());
}


// get_unsigned_longlong
inline uint64_t
service::get_unsigned_longlong()
{
	return (re->get_unsigned_longlong());
}

#ifdef ORBFLOAT
// get_float
inline float
service::get_float()
{
	return (re->get_float());
}


// get_double
inline double
service::get_double()
{
	return (re->get_double());
}
#endif // ORBFLOAT

// get_pointer
inline void *
service::get_pointer()
{
	return (re->get_pointer());
}

// get_string
inline char *
service::get_string()
{
	return (re->get_string());
}

// get_string_cancel
inline void
service::get_string_cancel()
{
	re->get_string_cancel();
}

// get_seq_hdr
inline void
service::get_seq_hdr(GenericSequence *sep)
{
	re->get_seq_hdr(sep);
}

// get_seq
inline void
service::get_seq(GenericSequence *sp, uint_t size)
{
	re->get_seq(sp, size);
}

// get_seq_cancel
inline void
service::get_seq_cancel(uint_t size)
{
	re->get_seq_cancel(size);
}

// get_bytes
inline void
service::get_bytes(void *bs, uint_t length)
{
	re->get_bytes(bs, length);
}


// get_u_bytes
inline int
service::get_u_bytes(caddr_t bs, uint_t length)
{
	return (re->get_u_bytes(bs, length));
}

inline Xdoor *
service::get_xdoor()
{
	return (re->get_xdoor());
}

// get_off_line
inline uint_t
service::get_off_line(Region &r)
{
	return (re->get_off_line(r));
}

// get_off_line_cancel
inline void
service::get_off_line_cancel()
{
	re->get_off_line_cancel();
}

// get_off_line_hdr
inline uint_t
service::get_off_line_hdr()
{
	return (re->get_off_line_hdr());
}


// get_off_line_bytes
inline void
service::get_off_line_bytes(void *p, uint_t len)
{
	// We pass the environment in as recstream does have the environment
	re->get_off_line_bytes(p, len, get_env());
}

// get_off_line_bytes
inline void
service::get_off_line_u_bytes(caddr_t p, uint_t len)
{
	// We pass the environment in as recstream does have the environment
	re->get_off_line_u_bytes(p, len, get_env());
}

// debug_get_word
inline void
service::debug_get_word(const uint32_t t)
{
	re->debug_get_word(t);
}

inline replyio_client_t *
service::add_reply_buffer(struct buf *bufp, align_t a)
{
	return (se->add_reply_buffer(bufp, a));
}

inline bool
service::check_reply_buf_support(size_t len, iotype_t iotyp, void *obj_type,
    optype_t optype)
{
	return (se->check_reply_buf_support(len, iotyp, obj_type, optype));
}

inline void
service::get_offline_reply(replyio_client_t *bp)
{
	re->get_offline_reply(bp);
}

inline void
service::get_offline_reply_cancel(replyio_client_t *bp)
{
	re->get_offline_reply_cancel(bp);
}

inline replyio_server_t *
service::get_reply_buffer()
{
	return (re->get_reply_buffer());
}

inline void
service::get_reply_buffer_cancel()
{
	re->get_reply_buffer_cancel();
}

inline void
service::put_offline_reply(Buf *src, replyio_server_t *dest)
{
	se->put_offline_reply(src, dest);
}

inline void
service::set_sendstream(sendstream *s)
{
	if (se) se->done();
	se = s;
}

inline void
service::set_recstream(recstream *r)
{
	if (re) re->done();
	re = r;
}


inline
service::service(recstream *r, sendstream *s, Environment *e) :
	se(s), re(r), env(e)
{
}

// Return the env pointer associated with the invocation
inline Environment *
service::get_env()
{
	return (env);
}

//
// Class invocation methods
//

inline
invocation::invocation(sendstream *s, Environment *e) :
    service(NULL, s, e)
{
}

inline void
invocation::exception_return(arg_info &ai, ArgValue *ap)
{
	unmarshal_roll_back(ai, ap, false);
}

#ifdef _KERNEL_ORB
//
// Class service_oneway methods
//

inline
service_oneway::service_oneway(recstream *recstreamp, Environment *e) :
	service(recstreamp, NULL, e)
{
}

inline void
service_oneway::set_pool(resource_defs::resource_pool_t new_pool)
{
	pool = new_pool;
}

inline resource_defs::resource_pool_t
service_oneway::get_pool()
{
	return (pool);
}

//
// Class service_twoway methods
//

inline
service_twoway::service_twoway(recstream *recstreamp, Environment *e,
    slot_t slot) :
	service(recstreamp, NULL, e),
	inbound(recstreamp->get_src_node(), inbound_invo::IB_REQUEST)
{
	//
	// Register this invocation request.
	// The invocation is deregisterred prior to sending the reply.
	//
	inbound.register_request(slot);

	//
	// Extract transport information from recstream and store it.
	//
	recstreamp->transfer_serv2_transport_info(transport_info);

}

inline ushort_t
service_twoway::get_index_invo()
{
	return (inbound.slot());
}

inline inbound_invo *
service_twoway::get_inbound_invo()
{
	return (&inbound);
}

inline serv2_transport_info *
service_twoway::get_transport_info()
{
	return (&transport_info);
}
#endif	// _KERNEL_ORB

#endif	/* _INVOCATION_IN_H */
