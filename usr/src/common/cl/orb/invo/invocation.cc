//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)invocation.cc	1.130	08/07/05 SMI"

#include <orb/invo/invocation.h>
#include <orb/handler/handler.h>
#include <orb/object/schema.h>
#include <sys/os.h>
#include <orb/handler/nil_handler.h>
#include <sys/mc_probe.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/invo/argfuncs.h>

//
// Service methods.
//

service::~service()
{
	set_recstream(NULL);
	set_sendstream(NULL);
}

// put_object
void
service::put_object(const CORBA::Object_ptr objp)
{
	handler	*hp;

	if (CORBA::is_nil(objp)) {
		hp = &nil_handler::the_nil_handler;
	} else {
		hp = objp->_handler();
	}
	debug_put_word(M_OBJECT);
	put_octet(hp->handler_id());
	hp->marshal(*this, objp);
}

// unput_object
void
service::unput_object(const CORBA::Object_ptr objp)
{
	if (CORBA::is_nil(objp)) {
		nil_handler::the_nil_handler.marshal_roll_back(objp);
	} else {
		objp->_handler()->marshal_roll_back(objp);
	}
}

//
// Get object from marshal stream.
//
CORBA::Object_ptr
service::get_object(MarshalInfo_object *infop)
{
	debug_get_word(M_OBJECT);

	// Get the handler type id and then handler_kit.
	handler_kit *hk = handler_repository::the().get_kit(get_octet());

	ASSERT(hk);
	//
	// If hk is NULL it implies that an object was sent into a domain that
	// does not support this handler type. That is a programming error....
	// e.g., bulkio_object into a user-level domain.
	//

	//
	// Let the handler type extract the info it needs from
	// the stream, and create the proxy if need be.
	//
	return (hk->unmarshal(*this, infop->createf, infop->tid));
}

// unget_object
void
service::get_object_cancel()
{
	debug_get_word(M_OBJECT);

	// get the handler type id and then handler_kit
	handler_kit *hk = handler_repository::the().get_kit(get_octet());

	ASSERT(hk);
	//
	// If hk is NULL it implies that an object was sent into a domain that
	// does not support this handler type. That is a programming error....
	// e.g., bulkio_object into a user-level domain

	// Let the handler type extract the info it needs from
	// the  stream, and adjust any counts needed
	// Note: It is important to leave the service in a state such that all
	// data that would have been read in case of get_object has been
	// read, otherwise the next get_object_cancel will be reading bad data
	//
	hk->unmarshal_cancel(*this);
}

//
// marshal_call - marshals the information supporting the invocation
//		of a method on another node.
//
//	The method's outgoing arguments are marshalled.
//
void
invocation::marshal_call(arg_info &info, ArgValue *value)
{
	MC_PROBE_0(marshal_call_start, "clustering orb invocation", "");

	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_mode mode = descriptor.get_argmode();
		switch (mode) {
		case invo_args::in_param:
			put_in_param(descriptor.get_argtype(), mode,
			    argmarshalp, argvaluep);
			break;
		case invo_args::inout_param:
			put_inout_param(descriptor.get_argtype(), mode,
			    argmarshalp, argvaluep);
			break;
		case invo_args::out_param:
		case invo_args::err_param:
			break;
		// XXX Might be worthwhile to check out/inout params
		// to construct region for holding reply.
		}
	}
	if (get_env()->exception()) {
		// Need to rollback marshalled objects
		// We marshal all arguments even in the presence of exceptions
		// The handlers are expected to be able to roll_back even
		// objects they had exceptions while marshalling.
		marshal_call_rollback(info, value);
	}
	MC_PROBE_0(marshal_call_end, "clustering orb invocation", "");
}

//
// marshal_call_rollback
// Invoked to undo marshaling the outgoing parameters of an invocation
//
// static
void
invocation::marshal_call_rollback(arg_info &info, ArgValue *value)
{
	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_type type = descriptor.get_argtype();
		if (type == invo_args::t_addr) {
			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);
			if (marshalp->marshalrollbackf != NULL) {
				switch (descriptor.get_argmode()) {
				case invo_args::in_param:
					(*marshalp->marshalrollbackf)
					    (argvaluep->t_vaddr);
					break;
				case invo_args::inout_param:
					(*marshalp->marshalrollbackf)
					    (*(void **)argvaluep->t_vaddr);
					break;
				default:
					break;
				}
			}
		} else if (type == invo_args::t_object) {
			switch (descriptor.get_argmode()) {
			case invo_args::in_param:
				unput_object(argvaluep->t_objref);
				break;
			case invo_args::inout_param:
				unput_object(*(CORBA::Object_ptr *)
				    argvaluep->t_vaddr);
				break;
			default:
				break;
			}
		}
	}
	// The caller to marshal_call will do exception_return to
	// initialize the out/inout/return values and free memory for inout
}

//
// unmarshal_normal_return - On the client side, this function unmarshals the
// inout and out parameters as well as any return result.
//
void
invocation::unmarshal_normal_return(arg_info &info, ArgValue *value,
    Environment *e)
{
	MC_PROBE_0(unmarshal_normal_return_start, "clustering orb invocation",
	    "");

	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_mode mode = descriptor.get_argmode();
		if (mode != invo_args::in_param) {
			get_param(descriptor.get_argtype(), mode, argmarshalp,
			    argvaluep, true);
		}
	}
	//
	// Note that in the func array, the function corresponding to the
	// result is at the last (as opposed to the descriptor which is
	// provided by an accessor method and the value which is value[0])
	//
	get_result(info.rdesc().get_argtype(), argmarshalp, value);
	if (e->exception()) {
		// unmarshal even if there are exceptions during unmarshalling
		// At end we check for exception and roll_back all.
		unmarshal_roll_back(info, value, true);
	}
	MC_PROBE_0(unmarshal_normal_return_end, "clustering orb invocation",
	    "");
}

//
// unmarshal_roll_back - an exception occurred during an unmarshal
// on the client side.
// This method handles the out/inout arguments that were not unmarshalled.
// When true the "abrt" argument signifies that unmarshalling was aborted
// in the middle and needs more cleanup
// than in the case of exception_return.
//
// static
void
invocation::unmarshal_roll_back(arg_info &info, ArgValue *value, bool abrt)
{
	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_type type = descriptor.get_argtype();
		invo_args::arg_mode mode = descriptor.get_argmode();
		switch (mode) {
		case invo_args::out_param:
			if (abrt) {
				release_param_indirect(type, mode, argmarshalp,
				    argvaluep);
			} else {
				clear_param_indirect(type, argmarshalp,
				    argvaluep);
			}
			break;
		case invo_args::inout_param:
			release_param_indirect(type, mode, argmarshalp,
			    argvaluep);
			break;
		case invo_args::in_param:
		case invo_args::err_param:
			break;
		}
	}
	// Note that in the func array, the function corresponding to the
	// result is at the last (as opposed to the descriptor which is
	// provided by an accessor method and the value which is value[0])

	// roll_back the unmarshal of the return arg
	if (abrt) {
		release_param_direct(info.rdesc().get_argtype(), argmarshalp,
		    value);
	}
	// Clear even if calling release to set return values to NULL
	clear_param_direct(info.rdesc().get_argtype(), argmarshalp, value);
}

//
// unmarshal_exception_return - the reply messsage contains a user exception
//
// static
void
invocation::unmarshal_exception_return(arg_info &ai, ArgValue *avp,
	Environment *e)
{
	// Unmarshal the exception type id
	CORBA::MD5_Tid		md5_value;
	_ox_get_tid(*this, md5_value);

	//
	// From the type id find the ExceptDescriptor.
	// Use that to get the unmarshal function for this
	// specific user exception.
	// The unmarshal function creates a new user exception,
	// which we load into the environment.
	//
	e->exception((*(OxSchema::schema()->except(md5_value)->
	    get_unmarshal_func()))(*this));

	// ASSERT that it is a user exception
	ASSERT(e->exception());
	ASSERT(!e->sys_exception());

	// Initialize the out/inout/return value parameter to nil
	exception_return(ai, avp);
}

//
// Invoked on a system exception return.
//
// static
void
invocation::unmarshal_sys_exception_return(arg_info &, ArgValue *,
	Environment *ev)
{
	uint_t	major_code = get_unsigned_long();
	uint_t	minor_code = get_unsigned_long();
	CORBA::CompletionStatus status = (CORBA::CompletionStatus)get_long();

	// Set the exception
	ev->system_exception(CORBA::SystemException(major_code, minor_code,
	    status));
	ASSERT(ev->sys_exception());

	// caller calls exception_return to set out/inout/return args to NULL
}

//
// marshals the IN parameter described by the arguments.
//
void
service::put_in_param(invo_args::arg_type type, invo_args::arg_mode mode,
    ArgMarshalFuncs *argmarshalp, ArgValue *argvaluep)
{
	MC_PROBE_1(put_in_param_start, "clustering orb invocation", "",
	    tnf_long, type, type);

	switch (type) {
	case invo_args::t_void:
		break;	// For void return value
	case invo_args::t_addr: {
			void *datap = argvaluep->t_vaddr;

			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);

			//
			// Return parameters for complex types yield a pointer
			// to a pointer and hence this extra dereference
			//
			if (mode == invo_args::out_param &&
			    marshalp->freef != NULL) {
				datap = *(void **)datap;
			}

			(*marshalp->marshalf)(*this, datap);
		}
		break;
	case invo_args::t_boolean:
		put_boolean(argvaluep->t_boolean);
		break;
	case invo_args::t_char:
		put_char(argvaluep->t_char);
		break;
	case invo_args::t_octet:
		put_octet(argvaluep->t_octet);
		break;
	case invo_args::t_short:
		put_short(argvaluep->t_short);
		break;
	case invo_args::t_unsigned_short:
		put_unsigned_short(argvaluep->t_unsigned_short);
		break;
	case invo_args::t_long:
		put_long(argvaluep->t_long);
		break;
	case invo_args::t_unsigned_long:
		put_unsigned_long(argvaluep->t_unsigned_long);
		break;
	case invo_args::t_longlong:
		put_longlong(argvaluep->t_longlong);
		break;
	case invo_args::t_unsigned_longlong:
		put_unsigned_longlong(argvaluep->t_unsigned_longlong);
		break;
#ifdef ORBFLOAT
	case invo_args::t_float:
		put_float(argvaluep->t_float);
		break;
	case invo_args::t_double:
		put_double(argvaluep->t_double);
		break;
#endif
	case invo_args::t_string:
		put_string(argvaluep->t_const_string);
		break;
	case invo_args::t_object:
		put_object(argvaluep->t_objref);
		break;
	default:
		ASSERT(0);
		break;
	}
	MC_PROBE_0(put_in_param_end, "clustering orb invocation", "");
}

//
// marshals the inout or out parameter described by the arguments
//
void
service::put_inout_param(invo_args::arg_type type, invo_args::arg_mode mode,
    ArgMarshalFuncs *argmarshalp, ArgValue *argvaluep)
{
	MC_PROBE_1(put_inout_param_start, "clustering orb invocation", "",
	    tnf_long, type, type);

	void *datap = argvaluep->t_vaddr;

	switch (type) {
	case invo_args::t_addr: {
			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);

			//
			// Return parameters for complex types yield a pointer
			// to a pointer and hence this extra dereference
			//
			if (mode == invo_args::out_param &&
			    marshalp->freef != NULL) {
				datap = *(void **)datap;
			}

			(*argmarshalp->datfuncs->marshalf)(*this, datap);
		}
		break;
	case invo_args::t_boolean:
		put_boolean(*(bool *)datap);
		break;
	case invo_args::t_char:
		put_char(*(char *)datap);
		break;
	case invo_args::t_octet:
		put_octet(*(uint8_t *)datap);
		break;
	case invo_args::t_short:
		put_short(*(int16_t *)datap);
		break;
	case invo_args::t_unsigned_short:
		put_unsigned_short(*(uint16_t *)datap);
		break;
	case invo_args::t_long:
		put_long(*(int32_t *)datap);
		break;
	case invo_args::t_unsigned_long:
		put_unsigned_long(*(uint32_t *)datap);
		break;
	case invo_args::t_longlong:
		put_longlong(*(int64_t *)datap);
		break;
	case invo_args::t_unsigned_longlong:
		put_unsigned_longlong(*(uint64_t *)datap);
		break;
#ifdef ORBFLOAT
	case invo_args::t_float:
		put_float(*(float *)datap);
		break;
	case invo_args::t_double:
		put_double(*(double *data)p);
		break;
#endif
	case invo_args::t_string:
		put_string(*(char **)datap);
		break;

	case invo_args::t_object:
		put_object(*(CORBA::Object_ptr *)datap);
		break;
	default:
		ASSERT(0);
		break;
	}
	MC_PROBE_0(put_inout_param_end, "clustering orb invocation", "");
}

//
// get_param - Unmarshals the parameter described by the arguments
//
void
service::get_param(invo_args::arg_type type, invo_args::arg_mode mode,
    ArgMarshalFuncs *argmarshalp, ArgValue *argvaluep, bool client_side)
{
	void *datap = argvaluep->t_vaddr;

	switch (type) {
	case invo_args::t_addr: {
			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);

			//
			// Only variable length params have make/release funcs
			//
			if (invo_args::inout_param == mode &&
			    client_side && marshalp->releasef != NULL) {
				//
				// On the client side must release the old value
				// before assigning new value.
				//
				(*marshalp->releasef)(datap);

			} else if (invo_args::out_param == mode &&
			    marshalp->makef != NULL) {
				ASSERT(client_side);
				//
				// Create the memory for out parameter on the
				// client side.
				//
				datap = *(void **)datap = (*marshalp->makef)();
			}
			(*marshalp->unmarshalf)(*this, datap);
		}
		break;
	case invo_args::t_boolean:
		*(bool *)datap = get_boolean();
		break;
	case invo_args::t_char:
		*(char *)datap = get_char();
		break;
	case invo_args::t_octet:
		*(uint8_t *)datap = get_octet();
		break;
	case invo_args::t_short:
		*(int16_t *)datap = get_short();
		break;
	case invo_args::t_unsigned_short:
		*(uint16_t *)datap = get_unsigned_short();
		break;
	case invo_args::t_long:
		*(int32_t *)datap = get_long();
		break;
	case invo_args::t_unsigned_long:
		*(uint32_t *)datap = get_unsigned_long();
		break;
	case invo_args::t_longlong:
		*(int64_t *)datap = get_longlong();
		break;
	case invo_args::t_unsigned_longlong:
		*(uint64_t *)datap = get_unsigned_longlong();
		break;
#ifdef ORBFLOAT
	case invo_args::t_float:
		*(float *)datap = get_float();
		break;
	case invo_args::t_double:
		*(double *)datap = get_double();
		break;
#endif
	case invo_args::t_string:
		// If release happens often, it might be worthwhile to modify
		// get_string to take str as argument to reuse buffer.
		if (invo_args::inout_param == mode && client_side) {
			char *str = *(char **)datap;
			delete [] str;
		}
		*(char **)datap = get_string();
		break;
	case invo_args::t_object: {
			MarshalInfo_object *omarshalp = argmarshalp->objfuncs;
			ASSERT(omarshalp != NULL);

			if (invo_args::inout_param == mode && client_side) {
				//
				// first create received object, then release
				// old one. Optimization avoid recreating same
				// handler.
				//
				CORBA::Object_ptr old_obj =
				    *(CORBA::Object_ptr *)datap;

				*(CORBA::Object_ptr *)datap =
				    get_object(omarshalp);

				CORBA::release(old_obj);
			} else {
				*(CORBA::Object_ptr *)datap =
				    get_object(omarshalp);
			}
		}
		break;
	default:
		ASSERT(0);
		break;
	}
}

//
// get_result - Unmarshals the return value.
//
void
invocation::get_result(invo_args::arg_type type, ArgMarshalFuncs *argmarshalp,
    ArgValue *argvaluep)
{
	switch (type) {
	case invo_args::t_addr: {
			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);

			void *datap = argvaluep->t_vaddr;

			// All return parameters are invo_args::out_param
			if (marshalp->makef != NULL) {
				//
				// Create the memory for a variable
				// length return parameter on the client side
				//
				datap = *(void **)datap = (*marshalp->makef)();
			}
			(*marshalp->unmarshalf)(*this, datap);
		}
		break;
	case invo_args::t_void:
		/* function with no return value */
		break;
	case invo_args::t_boolean:
		argvaluep->t_boolean = get_boolean();
		break;
	case invo_args::t_char:
		argvaluep->t_char = get_char();
		break;
	case invo_args::t_octet:
		argvaluep->t_octet = get_octet();
		break;
	case invo_args::t_short:
		argvaluep->t_short = get_short();
		break;
	case invo_args::t_unsigned_short:
		argvaluep->t_unsigned_short = get_unsigned_short();
		break;
	case invo_args::t_long:
		argvaluep->t_long = get_long();
		break;
	case invo_args::t_unsigned_long:
		argvaluep->t_unsigned_long = get_unsigned_long();
		break;
	case invo_args::t_longlong:
		argvaluep->t_longlong = get_longlong();
		break;
	case invo_args::t_unsigned_longlong:
		argvaluep->t_unsigned_longlong = get_unsigned_longlong();
		break;
#ifdef ORBFLOAT
	case invo_args::t_float:
		argvaluep->t_float = get_float();
		break;
	case invo_args::t_double:
		argvaluep->t_double = get_double();
		break;
#endif
	case invo_args::t_string:
		argvaluep->t_string = get_string();
		break;
	case invo_args::t_object: {
			MarshalInfo_object *omarshalp = argmarshalp->objfuncs;
			ASSERT(omarshalp != NULL);
			argvaluep->t_objref = get_object(omarshalp);
		}
		break;
	default:
		ASSERT(0);
		break;
	}
}

//
// unmarshal_receive - Server side function used by the skeleton when handling
// an invocation. It  unmarshals the in and inout
// parameters.
//
void
service::unmarshal_receive(arg_info &info, ArgValue *value)
{
	MC_PROBE_0(unmarshal_receive_start, "clustering orb invocation", "");

	Environment *e = get_env();
	ASSERT(!e->exception());

	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_mode mode = descriptor.get_argmode();
		if (mode != invo_args::out_param) {
			get_param(descriptor.get_argtype(), mode, argmarshalp,
			    argvaluep, false);
		}
	}
	//
	// XXX If exception during unmarshalling, how to abort skel?
	// Need to put a check in the skel to check the Environment and
	// return before calling the receive func.
	// Currently we do not have any exceptions generated at this place
	// If introduce any in future we have to be aware of this change to
	// the skels.
	//
	if (e->exception()) {
		e->exception()->print_exception("unmarshal_receive: ");
		//
		// SCMSGS
		// @explanation
		// The server encountered an exception while unmarshalling the
		// arguments for a remote invocation. The system prints the
		// exception causing this error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Exception during unmarshal_receive");
	}
	//
	// Dispose off the recstream. It releases buffers to the transport
	// earlier. Otherwise they would not have been disposed off till
	// the server invocation actually completes
	//
	set_recstream(NULL);
	MC_PROBE_0(unmarshal_receive_end, "clustering orb invocation", "");
}

void
service::marshal_reply(arg_info &info, ArgValue *value, Environment *e)
{
	MC_PROBE_0(marshal_reply_start, "clustering orb invocation", "");

	// Request messages must have a sendstream for the reply.
	ASSERT(se != NULL);
	ASSERT(!e->exception());

	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_mode mode = descriptor.get_argmode();
		if (mode != invo_args::in_param) {
			put_inout_param(descriptor.get_argtype(), mode,
			    argmarshalp, argvaluep);
		}
	}
	// Return Value
	put_in_param(info.rdesc().get_argtype(), invo_args::out_param,
	    argmarshalp, value);

	// Handle exception
	if (e->exception()) {
		// Need to marshal_rollback the marshalcounts
		marshal_reply_roll_back(info, value);
	}
	MC_PROBE_0(marshal_reply_end, "clustering orb invocation", "");
}

//
// The bool argument specifies whether there was an exception returned
// by the receive function. If the exception is generated while processing
// send_reply for THIS invocation, the error processing is different.
//
void
service::release_reply(arg_info &info, ArgValue *value,
    bool exception_result)
{
	MC_PROBE_0(release_reply_start, "clustering orb invocation", "");

	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	if (exception_result) {
		//
		// This is an exception that was generated in the receive func
		// There should be no out/return parameters initialized
		// Server/skel code should not pass out params for user
		// exception.
		//
		// argc includes descriptor for return value.
		// Loop runs from 1..argc-1
		for (i = 1; i < argc;
		    i++, descriptorp++, argvaluep++, argmarshalp++) {
			arg_desc	descriptor = *descriptorp;
			invo_args::arg_type type = descriptor.get_argtype();
			if (descriptor.is_arg_in_inout()) {
				release_param_indirect(type,
				    descriptor.get_argmode(), argmarshalp,
				    argvaluep);
			}
		}
	} else {
		// This is a no-error case or ORB exception in reply handling
		//
		// argc includes descriptor for return value.
		// Loop runs from 1..argc-1
		for (i = 1; i < argc;
		    i++, descriptorp++, argvaluep++, argmarshalp++) {
			arg_desc	descriptor = *descriptorp;
			invo_args::arg_type type = descriptor.get_argtype();
			release_param_indirect(type, descriptor.get_argmode(),
			    argmarshalp, argvaluep);
		}
		release_param_direct(info.rdesc().get_argtype(), argmarshalp,
		    value);
	}
	MC_PROBE_0(release_reply_end, "clustering orb invocation", "");
}

//
// rollback any marshalled arguments that require rollback processing
//
// static
void
service::marshal_reply_roll_back(arg_info &info, ArgValue *value)
{
	uint_t		i;
	uint_t		argc = info.argc();
	arg_desc	*descriptorp = info.argdesc();
	ArgMarshalFuncs	*argmarshalp = info.marshal_funcs();
	ArgValue	*argvaluep = &value[1];

	// argc includes descriptor for return value. Loop runs from 1..argc-1
	for (i = 1; i < argc; i++, descriptorp++, argvaluep++, argmarshalp++) {
		arg_desc	descriptor = *descriptorp;
		invo_args::arg_type type = descriptor.get_argtype();
		if (descriptor.is_arg_out_inout()) {
			if (type == invo_args::t_addr) {
				MarshalInfo_complex *marshalp =
				    argmarshalp->datfuncs;
				ASSERT(marshalp != NULL);

				if (marshalp->marshalrollbackf != NULL) {
					(*marshalp->marshalrollbackf)(*(void **)
					    argvaluep->t_vaddr);
				}
			} else if (type == invo_args::t_object) {
				unput_object(*(CORBA::Object_ptr *)
				    argvaluep->t_vaddr);
			}
		}
	}
	// roll_back the reply
	invo_args::arg_type	type = info.rdesc().get_argtype();
	if (type == invo_args::t_addr) {
		MarshalInfo_complex *marshalp =	argmarshalp->datfuncs;
		ASSERT(marshalp != NULL);

		if (marshalp->marshalrollbackf != NULL) {
			(*marshalp->marshalrollbackf)(*(void **)
			    (value[0].t_vaddr));
		}
	} else if (type == invo_args::t_object) {
		unput_object(value[0].t_objref);
	}
}

//
// release_param_direct - processes the return result parameter only
// when the system no longer needs the result.
// This happens on the server side after the reply has been sent.
// It also happens on the client side after an exception occurs
// during unmarshal.
// Note: That still need to call clear_param_direct in the latter case.
//
// static
void
service::release_param_direct(invo_args::arg_type type,
    ArgMarshalFuncs *argmarshalp, ArgValue *argvaluep)
{
	switch (type) {
	case invo_args::t_addr: {
			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);

			// All return result parameters are invo_args::out_param
			if (marshalp->freef != NULL) {
				(*marshalp->freef)(*(void **)argvaluep->
				    t_vaddr);
			}
		break;
	}
	case invo_args::t_string:
		delete [] argvaluep->t_string;
		break;
	case invo_args::t_object:
		CORBA::release(argvaluep->t_objref);
		break;
	default:
		// Nothing to do for this type
		break;
	}
}

//
// When an exception is 'thrown', the return value must not return
// garbage for _var ptrs since the destructors will try to delete
// these garbage values later.
//
// e.g.:
// 	ptr_var = obj->resolve(name, e);
//	if (e.exception()) {
//		// _var destructor called with ptr_var set to nil
//		return;
//	}
//
// static
void
invocation::clear_param_direct(invo_args::arg_type type,
    ArgMarshalFuncs *argmarshalp, ArgValue *argvaluep)
{
	switch (type) {
	case invo_args::t_addr:
		ASSERT(argmarshalp->datfuncs != NULL);

		// All return result parameters are invo_args::out_param
		if (argmarshalp->datfuncs->makef != NULL) {
			*(void **)argvaluep->t_vaddr = NULL;
		} else {
			argvaluep->t_vaddr = NULL;
		}
		break;
	case invo_args::t_string:
		argvaluep->t_string = NULL;
		break;
	case invo_args::t_object:
		argvaluep->t_objref = nil;
		break;
	default:
		argvaluep->t_long = 0;		// Just to be safe
		break;
	}
}

//
// release_param_indirect
// Called to release the parameters which are pased by pointers
// Currently called for all params on server during marshal_reply
//	OUT/INOUT params on client if error during unmarshal_normal_return
//	OUT/INOUT params on client for unmarshal_roll_back
//
// static
void
service::release_param_indirect(invo_args::arg_type type,
    invo_args::arg_mode mode, ArgMarshalFuncs *argmarshalp,
    ArgValue *argvaluep)
{
	switch (type) {
	case invo_args::t_addr: {
			MarshalInfo_complex *marshalp = argmarshalp->datfuncs;
			ASSERT(marshalp != NULL);
			if (mode == invo_args::inout_param &&
			    marshalp->releasef != NULL) {
				(*marshalp->releasef)(argvaluep->t_vaddr);

			} else if (mode == invo_args::out_param &&
			    marshalp->freef != NULL) {
				(*marshalp->freef)(*(void **)argvaluep->
				    t_vaddr);

				*(void **)argvaluep->t_vaddr = NULL;
			}
		}
		break;
	case invo_args::t_string:
		/*CSTYLED*/
		delete [] (*(char **)argvaluep->t_vaddr);
		*(char **)argvaluep->t_vaddr = NULL;
		break;
	case invo_args::t_object:
		CORBA::release(*(CORBA::Object_ptr *)argvaluep->t_vaddr);
		*(CORBA::Object_ptr *)argvaluep->t_vaddr = nil;
		break;
	default:
		// Nothing to do for this type
		break;
	}
}

//
// clear_param_indirect
// Called to just set the pointers to nil/NULL without deleting/releasing
// anything - only called from unmarshal_roll_back.
// Note: this method is only called for parameters that are invo_args::out_param
//
// static
void
invocation::clear_param_indirect(invo_args::arg_type type,
    ArgMarshalFuncs *argmarshalp, ArgValue *argvaluep)
{
	switch (type) {
	case invo_args::t_addr:
		ASSERT(argmarshalp->datfuncs != NULL);

		// All parameters to this method are invo_args::out_param
		if (argmarshalp->datfuncs->makef != NULL) {
			*(void **)argvaluep->t_vaddr = NULL;
		}
		break;
	case invo_args::t_string:
		*(char **)argvaluep->t_vaddr = NULL;
		break;
	case invo_args::t_object:
		*(CORBA::Object_ptr *)argvaluep->t_vaddr = nil;
		break;
	default:
		// Nothing to do for this type
		break;
	}
}

//
// send_reply
// Server side function called after performing the request by the skel.
// The method passes the send reply down to the handler, which marshals
// the return data and sends the reply message. After the message has been
// sent this method releases data, because the invocation is finished with it.
//
void
service::send_reply(CORBA::Object_ptr obj, arg_info &info,
    ArgValue *value)
{
	MC_PROBE_0(send_reply_start, "clustering orb invocation", "");
	//
	// Remember whether there was an exception passed in from
	// the receive function. The cleanup in release_reply is
	// different depending on whether the exception occured
	// while marshalling/sending the reply or earlier.
	//
	Environment	*e = get_env();
	bool		had_exception = (e->exception() != NULL);

	//
	// Should not reach here with a system exception, with the exception
	// of CORBA::PRIMARY_FROZEN or CORBA::VERSION or CORBA::CTX_EACCESS
	// or CORBA::NS_EPERM
	//
	ASSERT(!CORBA::SystemException::_exnarrow(e->exception()) ||
	    CORBA::PRIMARY_FROZEN::_exnarrow(e->exception()) ||
	    CORBA::VERSION::_exnarrow(e->exception()) ||
	    CORBA::CTX_EACCESS::_exnarrow(e->exception()) ||
	    CORBA::NS_EPERM::_exnarrow(e->exception()));

	//
	// Tell handler to create and send a reply message.
	//
	ASSERT(is_not_nil(obj));

	//
	// rxdoor::exception_reply_remote has its own retry loop. Therefore
	// we don't need to worry about the exception case or whether
	// the exception will be replaced by RETRY_NEEDED.
	//
	bool retry_needed = false;
	do {
		obj->_handler()->send_reply(*this, info, value);

		//
		// If we are sending an exception back we can't get
		// retry_needed exception. All RETRY_NEEDED should be
		// caught by the retry loop in rxdoor::exception_reply_remote
		//
		if (CORBA::RETRY_NEEDED::_exnarrow(e->exception())) {
			ASSERT(!had_exception);
			retry_needed = true;
			e->clear();
		} else {
			retry_needed = false;
		}
	} while (retry_needed);

#ifdef _KERNEL_ORB
	ASSERT(se == NULL);
#endif

	// We could get a comm_failure from send_reply.
	if (e->exception()) {
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(e->exception()));
		e->clear();
	}

	release_reply(info, value, had_exception);

	MC_PROBE_0(send_reply_end, "clustering orb invocation", "");
}

//
// request_complete - Server side function called after performing the
// oneway request. Performs cleanup.
//
void
service::request_complete(arg_info &info, ArgValue *value)
{
	MC_PROBE_0(request_complete_start, "clustering orb invocation", "");

#ifdef _KERNEL_ORB
	// Oneway requests do not need a sendstream for a reply
	ASSERT(se == NULL);
#else
	// If we have a sendstream (e.g., user level), make sure
	// it's marked oneway.
	// XXX - the kernel/user interface needs a message header specifying
	// whether the message is oneway or twoway. Currently the user
	// code assumes that the message is twoway and always allocates
	// a reply sendstream. This should eventually be fixed.
	//
	if (se != NULL) {
		se->set_oneway();
	}
#endif
	// Should not reach here with a system exception
	ASSERT(!CORBA::SystemException::_exnarrow(get_env()->exception()));

	// We cannot return exceptions for oneways.
	// Should user exceptions be allowed on oneways?
	// ASSERT(!e->exception());

	get_env()->clear();

	release_reply(info, value, false);

	MC_PROBE_0(request_complete_end, "clustering orb invocation", "");
}

service::service_type_t
service::get_service_type()
{
	return (service::SERVICE);
}

//
// invocation class methods
//

#if defined(_FAULT_INJECTION)
void
invocation::set_invotrigp(InvoTriggers *trigp)
{
	invotrigp = trigp;
}

InvoTriggers *
invocation::get_invotrigp()
{
	return (invotrigp);
}
#endif

service::service_type_t
invocation::get_service_type()
{
	return (service::INVO);
}

#ifdef _KERNEL_ORB
//
// service_oneway class methods
//

service::service_type_t
service_oneway::get_service_type()
{
	return (service::ONEWAY);
}

//
// service_twoway class methods
//

service::service_type_t
service_twoway::get_service_type()
{
	return (service::TWOWAY);
}
#endif	// _KERNEL_ORB
