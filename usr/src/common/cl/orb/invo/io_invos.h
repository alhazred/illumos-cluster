//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _IO_INVOS_H
#define	_IO_INVOS_H

#pragma ident	"@(#)io_invos.h	1.22	08/05/20 SMI"

//
// This file describes the interface to the outbound and inbound
// invocation tables used to manage invocations to another node.
//

#include <sys/os.h>
#include <orb/member/Node.h>
#include <orb/invo/corba.h>

#if defined(_FAULT_INJECTION)
class InvoTriggers;
#endif

// Forward declarations
class recstream;
class invocation;

typedef unsigned short	slot_t;

class invo_base {
public:
	ID_node			&node();	// returns src/dest node
	slot_t			slot();		// returns slot number

protected:
	invo_base(ID_node &node, ushort_t state);

	ID_node			_node;		// other node
	slot_t			_slot;		// actual slot number
	ushort_t 		_state;		// vector of state bits
	os::threadid_t 		_threadid; 	// which thread
};

//
// outbound_invo - this class is used to manage one twoway invocation
// on the client side.
//
class outbound_invo: public invo_base {
	friend class outbound_invo_table;
public:
	//
	// called when things go wrong
	//
	void			oops();

	//
	// called by client to wait for results
	//
	ExceptionStatus		wait_for_results();

protected:

	outbound_invo(ID_node &node, ushort_t state, invocation *invop);
	~outbound_invo();

private:
	invocation		*inv;		// pending invocation
	ExceptionStatus		_result;	// invocation status
	os::condvar_t 		_cv;		// sleep here awaiting results
	outbound_invo		*_next;		// next in hash chain

	outbound_invo(const outbound_invo &);
	outbound_invo &operator = (outbound_invo &);
};

class outbound_invo_table {
	friend class outbound_invo;
public:
	//
	// allocates a new slot
	//
	static outbound_invo	*alloc(ID_node &, invocation &);

	//
	// called from handle_reply_message to wake up sleeping thread
	//
	static void		wakeup(slot_t, uint_t, recstream *, ID_node &);

	//
	// called by signals::handle_message when a finished message arrives
	//
	static void 		finished(ID_node &, slot_t);

	//
	// called when a reconfig has happened; wakes up threads waiting
	// on dead nodes
	//
	static void		cleanup_invocations();

	//
	// The size of the inbound and outbound tables must match,
	// and the size must be a power of 2.
	// Each remote invocation needs a slot, and so the table
	// size should be at least as big as the number of server threads.
	//
	enum { SIZE = 8192}; // must be power of 2

private:
	static int 		hashslot(slot_t);

	static outbound_invo	*_buckets[SIZE];	// hash buckets
	static os::mutex_t 	_locks[SIZE];		// locks for hash chains
	static uint_t		_next_slot_number;	// next free #
};

//
// inbound_invo - this class is used to manage one twoway invocation
// on the server side.
//
class inbound_invo: public invo_base {
	friend class inbound_invo_table;
public:
	//
	// Server side state machine --------------------------
	//
	// Background Rules
	//	1. A signal is not sent before a request.
	//	2. A signal can arrive after a request completes.
	//	3. A cancel can only arrive after a reply has been sent.
	//	4. Once the server begins preparing to send the reply,
	//		the slot is freed. The slot cannot remain in use
	//		after the reply message has been sent.
	//		Otherwise a race condition could occur, because
	//		the client could send a new request using that slot.
	//
	enum {	IB_REQUEST	= 0x01,		// Request arrived
		IB_REPLY	= 0x02,		// Reply in progress
		IB_SIGNAL	= 0x04,		// Signal arrived
		IB_CANCEL	= 0x08		// Signal cancel arrived
	};

	//
	// Only a subset of the possible combinations of the above are possible.
	// The legal states follow:
	//
	// State: IB_REQUEST
	//	A request has arrived.
	//	A slot has been allocated.
	//
	// Transition Action	Next State
	// signal arrives	IB_REQUEST | IB_SIGNAL
	// prepare reply	IB_REQUEST | IB_REPLY
	//
	//
	// State: IB_REQUEST | IB_REPLY
	//	A request has arrived.
	//	The slot has been freed.
	//	Currently, in the process of sending a reply.
	//
	// Transition Action	Next State
	// reply sent		inbound_invo destroyed
	//
	//
	// State: IB_REQUEST | IB_SIGNAL
	//	A request has arrived, and a signal has been delivered.
	//	A slot has been allocated.
	//
	// Transition Action	Next State
	// prepare reply	IB_REQUEST | IB_SIGNAL | IB_REPLY
	//
	//
	// State: IB_REQUEST | IB_SIGNAL | IB_REPLY
	//	A request has arrived, and a signal has been delivered.
	//	The slot has been freed.
	//	Currently, in the process of sending a reply.
	//
	// Transition Action	Next State
	// reply sent		inbound_invo destroyed
	//
	//
	// State: IB_SIGNAL
	//	A signal has arrived.
	//	There is no active request.
	//	A slot has been allocated.
	//
	// Transition Action	Next State
	// request arrives	IB_REQUEST | IB_SIGNAL
	// cancel arrives	IB_SIGNAL | IB_CANCEL
	//
	//
	// State: IB_CANCEL
	//	A cancel has arrived.
	//	There is no active request.
	//	A slot has been allocated.
	//
	// Transition Action	Next State
	// signal arrives	IB_SIGNAL | IB_CANCEL
	//
	//
	// State: IB_SIGNAL | IB_CANCEL
	//	Both a signal and a cancel have arrived.
	//	There is no active request.
	//	A slot has been allocated.
	//
	// Transition Action	Next State
	// sent finish		inbound_invo destroyed
	//
	// -------------------------------------------

	inbound_invo(ID_node &node, ushort_t state);

	~inbound_invo();

	//
	// called when the invocation completes
	//
	void			done();

	//
	// called to determine whether or not thread has processed signal
	//
	bool			was_signaled();

	//
	// called when a new inbound invocation arrives
	//
	void			register_request(slot_t);

	//
	// called when a signal message arrives from client node
	//
	static void		signal(ID_node &, slot_t);

	//
	// called when a cancel messages arrives from client ncode
	//
	static void		cancel(ID_node &, slot_t);

private:
	inbound_invo(const inbound_invo &);
	inbound_invo & operator = (inbound_invo &);

	inbound_invo		*_next;			// next in hash chain
};

class inbound_invo_table {
	friend class inbound_invo;
public:
	//
	// Called by cmm to cleanup server side invocations
	//
	static void		cleanup_invocations();

	//
	// The size of the inbound and outbound tables must match,
	// and the size must be a power of 2.
	// Each remote invocation needs a slot, and so the table
	// size should be at least as big as the number of server threads.
	//
	enum { SIZE = 8192};

private:
	static int 		hashslot(slot_t);

	static inbound_invo	*_buckets[SIZE];	// hash buckets
	static os::mutex_t 	_locks[SIZE];		// locks for hash chains
};

#include <orb/invo/io_invos_in.h>

#endif	/* _IO_INVOS_H */
