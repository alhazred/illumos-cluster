/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MESSAGE_MGR_H
#define	_MESSAGE_MGR_H

#pragma ident	"@(#)message_mgr.h	1.17	08/10/13 SMI"

#include <orb/member/cmm_callback_impl.h>
#include <sys/os.h>
#include <h/orbmsg.h>
#include <orb/object/adapter.h>
#include <sys/hashtable.h>

// The invalid orb seqnum. All messages that don't get a orb_seq_t from
// message_mgr::send_prepare should set their orb_seq to this value.
#define	INVALID_ORBSEQ	0xffffffffU

class message_mgr;

// Classes described here manage sequence numbers for orb level
// messages (except for cmm messages). Each orb message is assigned a unique
// sequence number before sending out. The receiving side records which
// message is received by using a bitmask. When there is a RETRY_NEEDED
// exception, the sending side will ask the receiving side whether the
// message in question is received, and resend it using a new sequence
// number if necessary.

// This class implements the bitmask as well as the common utility functions
// for manipulating the bitmask.
class msg_mgr {
public:
	// Returns (seq - num).
	static orb_seq_t minus(orb_seq_t &seq, uint32_t num);

	// Returns true if s1 is smaller than s2.
	static bool smallerthan(orb_seq_t s1, orb_seq_t s2);
protected:
	// The subclasses should choose the meaning for current_pos
	// and current_seq, therefore do the initialization themselves.
	msg_mgr();

	// Increment the sequence number by one.
	static void inc(orb_seq_t &);

	// Allocate memory for the bitmask and call reset().
	void create_buf();

	// Frees the memory for the bitmask.
	void delete_buf();

	// Get the position = base_pos + advance
	static int32_t get_pos(int32_t base_pos, int32_t advance);

	// Get the position corresponding to the sequence number. The
	// position will be calculated based on current_seq and current_pos.
	int32_t get_pos(orb_seq_t);

	// Whether a specified bit is set in the bitmask.
	bool is_set(int32_t);

	// Set the bit
	void set(int32_t);

	// clear the bit
	void clear(int32_t);

	// Reset the data. Called by create_buf. The child classes
	// should implement their own to reset their data.
	virtual void reset();

	// The bitmask.
	uint8_t *buf;

	// The current sequence number and it's position in the bitmask.
	int32_t current_pos;
	orb_seq_t current_seq;
};

// The hash table we use to identify the sequence numbers that can't be
// used. Right now the entry we put into the hashtable isn't important.
// We store the pointer to a class to differentiate from NULL
typedef hashtable_t<message_mgr *, orb_seq_t> seq_hash_t;

// Manages the info for the messages we are sending.
class send_msg_mgr : public msg_mgr {
	friend class node_msg_mgr;
	friend class message_mgr;
private:
	// constructor
	send_msg_mgr();

	// Reset the data.
	void reset();

	// Get the next available sequence number.
	// Returns true if we succeed, returns false if we can't
	// get a new seq because the tail is catching up with the head in
	// the bitmask.
	bool get_new_seq(orb_seq_t &);

	// The specified message is confirmed by the receiver. Update
	// our local info. Returns true if the confirmed message has
	// a seqnum equal to the min_seq.
	bool confirmed(orb_seq_t);

	// Stores the sequences that should never be used again. These
	// are the sequence numbers that has been rejected by COMPLETED_MAYBE.
	seq_hash_t hash;

	// For the message with the minimum seq that we don't know whether the
	// receiving side received or not.
	int32_t min_pos;
	orb_seq_t min_seq;

	// current_seq and current_pos points to the message we are
	// going to send out next.
};

// Manages the info for the messages we are receiving.
class recv_msg_mgr : public msg_mgr {
	friend class node_msg_mgr;
	friend class message_mgr;
private:
	// constructor
	recv_msg_mgr();

	// Reset the data.
	void reset();

	// received a message, update our local info.
	void received(orb_seq_t);

	// Is the message received?
	bool is_received(orb_seq_t);

	// Stores the sequences that should never be delivered again. These
	// are the sequence numbers that has been rejected by COMPLETED_MAYBE.
	seq_hash_t hash;

	// current_seq and current_pos points to the message with the
	// biggest seq that we are expecting.
};

// An anchor to the message_mgr on a remote node.
class msgmgrref : public Anchor<orbmsg::message_mgr_stub> {
public:
	msgmgrref(node_msg_mgr *, nodeid_t);
	~msgmgrref();
private:
	node_msg_mgr *parent;
};

// When we get a COMPLETED_MAYBE we use datagram to ask the receiver
// whether it has received the message. The sender will create a
// message_mgr_slot and sleep there waiting for the answer.
class message_mgr_slot {
public:
	message_mgr_slot(orb_seq_t, uint64_t incn);
	message_mgr_slot();

	orb_seq_t seq;
	uint64_t incn; // incarnation number.
	enum { UNINIT, IS_RECVD, NOT_RECVD }; // states.
	uint8_t state; // store the states.
};

// Manages the messages to and from a remote node.
class node_msg_mgr {
	friend class message_mgr;
	friend class msgmgrref;
private:
	// This node becomes alive. Create bitmask if necessary and reset all
	// data in sendmgr and recvmgr.
	void node_alive();

	// This node died. Delete the bitmask in sendmgr and recvmgr.
	void node_died();

	void lock();
	void unlock();

	node_msg_mgr();

	send_msg_mgr sendmgr;
	recv_msg_mgr recvmgr;

	// The reference to the remote node's message_mgr.
	orbmsg::message_mgr_ptr remotemgr;

	os::mutex_t lck;
	os::condvar_t cv;

	// Each time we gets a COMPLETED_MAYBE and asks the receiver whether
	// it received the message, we put a message_mgr_slot into the
	// following list and wait for the answer to come back.
	SList<message_mgr_slot> wait_list;

	// The incarnation number to be assigned to the next message_mgr_slot.
	uint64_t next_incn;
};

// This class implements the public interfaces we are exposing to other
// components inside the ORB. The orb_msg code will call functions here
// to obtain sequence numbers before sending, as well as updating bitmask
// info when receiving messages.
class message_mgr : public McNoref<orbmsg::message_mgr> {
public:
	// send_prepare and send_done are called on the sending side.
	//
	// Gets a new sequence number before send through
	// transport. Returns true if the incarnation in the ID_node
	// matches my current membership. Otherwise return false and
	// don't update our local information.
	//
	// When it returns false it means the node we are talking to
	// died. We shouldn't send the message out or call send_done
	// in that case.
	bool send_prepare(ID_node &dest, orb_seq_t &);

	// The send for the sepcified sequence number completed and the
	// RETRY_NEEDED exception, if any, was resolved. It is called
	// if and only if send_prepare returned true.
	void send_done(ID_node &dest, orb_seq_t);

	// DO_NOT_PROCESS: Orphaned message
	// OK_TO_PROCESS: Standard case
	// DEFERRED: Membership is not known - defer processing
	typedef enum { DO_NOT_PROCESS, OK_TO_PROCESS, DEFERRED }
		received_state_t;

	// received and is_received are called on the receiving side.
	//
	// Check whether the membership is known. If it isn't, defer
	// the processing of the message and return 'DEFERRED'.
	// Check whether the message is orphaned. If it isn't, update
	// our local bitmask/seq info and return true. Otherwise, don't
	// do anything and return false. The message should be discarded
	// in that case.
	received_state_t received(recstream *re, os::mem_alloc_type flag,
	    orb_seq_t seq);

	// Asks the receiving side whether the specified message is received.
	// This is used when the sending side gets a COMPLETED_MAYBE and
	// need to resolve it into a COMPLETED_YES or COMPLETED_NO.
	// Returns the correct completion status
	// If the remote node (receiving side) died, it returns COMPLETED_MAYBE
	CORBA::CompletionStatus is_received(ID_node &dest_node, orb_seq_t seq,
	    orb_msgtype msg_type);

	// Set the membership. It will compare the new membership
	// with its old copy and do necessary initialization/cleanups.
	void set_membership(callback_membership *);

	// Create the message_mgr. Only called from ORB::initialize.
	static void initialize();

	// Get the proxy to the message_mgr of the specified node. The caller
	// should call release_remote_mgr after it's done.
	orbmsg::message_mgr_ptr get_remote_mgr(nodeid_t);

	// Release the remote_mgr obtained in the previous function.
	void release_remote_mgr(nodeid_t);

	// The message handler for the COMPLETED_MAYBE related datagrams.
	static void handle_message(recstream *recstreamp);

	static message_mgr &the();

	message_mgr();

private:
	// Hold/release all the node_mgr's locks. All the locks must be
	// obtained before changing our memberp. This way when we are
	// processing a message we can just obtain one lock to read the
	// memberp.
	void lock_all();
	void unlock_all();

	// The receiving side implementation of is_received. It gets
	// the question and sends the answer back.
	void handle_question(ID_node &src_node, message_mgr_slot &slot);

	// The sending side implementation of is_received. It gets the
	// answer and returns to the caller.
	void handle_answer(ID_node &src_node, message_mgr_slot &slot);

	// Helper function. Send a message to the specified node. Keep
	// sending until we either succeed or gets a COMM_FAILURE.
	// Returns true when message is sent successfully.
	bool send_message(ID_node &dest_node, void *argbuf, uint_t argsize);

	// The node_msg_mgr for different nodeid.
	node_msg_mgr node_mgr[NODEID_MAX+1];

	// Our view of the membership. When the membership changes
	// (cmm step1), we will reset the nodes whose incn changed.
	callback_membership *memberp;
};

#include <orb/msg/message_mgr_in.h>

#endif	/* _MESSAGE_MGR_H */
