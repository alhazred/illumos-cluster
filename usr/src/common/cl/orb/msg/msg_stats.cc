//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)msg_stats.cc	1.6	08/05/20 SMI"

#include <orb/msg/msg_stats.h>

//
// msg_stats methods
//

msg_stats::msg_stats()
{
	init();
}

void
msg_stats::init()
{
	memory_out = 0;
	memory_in = 0;

	int	i;
	for (i = 0; i < N_MSGTYPES; i++) {
		msg_types_out[i] = 0;
		msg_types_in[i] = 0;
		orphans[i] = 0;
	}
	for (i = 0; i < NUMBER_HISTOGRAM_SIZES; i++) {
		size_histogram_out[i] = 0;
		size_histogram_in[i] = 0;
	}
}

//
// add_memory_out - The size parameter specifies the message size in bytes.
// This method records two statistics. One statistic records the total
// amount of bytes of memory used to hold the outbound message.
// The second statistic records a count of the number of messages within
// various size ranges.
//
void
msg_stats::add_memory_out(uint_t size)
{
	memory_out += size;

	//
	// Update outbound message size histogram
	//
	uint_t	i = 0;
	uint_t	base = msg_stats::HISTOGRAM_BASE;
	for (; i < msg_stats::NUMBER_HISTOGRAM_SIZES - 1; i++) {
		if (base >= size) {
			break;
		}
		base *= msg_stats::HISTOGRAM_FACTOR;
	}
	size_histogram_out[i]++;
}

//
// add_memory_in - The size parameter specifies the message size in bytes.
// This method records two statistics. One statistic records the total
// amount of bytes of memory used to hold the inbound message.
// The second statistic records a count of the number of messages within
// various size ranges.
//
void
msg_stats::add_memory_in(uint_t size)
{
	memory_in += size;

	//
	// Update inbound message size histogram
	//
	uint_t	i = 0;
	uint_t	base = msg_stats::HISTOGRAM_BASE;
	for (; i < msg_stats::NUMBER_HISTOGRAM_SIZES - 1; i++) {
		if (base >= size) {
			break;
		}
		base *= msg_stats::HISTOGRAM_FACTOR;
	}
	size_histogram_in[i]++;
}
