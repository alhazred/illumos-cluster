/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MSG_STATS_H
#define	_MSG_STATS_H

#pragma ident	"@(#)msg_stats.h	1.7	08/05/20 SMI"

#include <sys/os.h>
#include <sys/types.h>
#include <orb/flow/resource_defs.h>
#include <orb/flow/resource.h>

//
// msg_stats - records statistics about message related operations in the orb.
//
// Note that currently the system never checks for orphan
// fault or signal messages.
//
// In order to reduce the impact on performance, the system does not
// use locking to protect statistics operations. This means that the
// statistics are imprecise. If two threads attempt to simultaneously
// update a counter, one update will overwrite the other update.
//
class msg_stats {
public:
	msg_stats();

	void	init();

	void		inc_msg_types_out(orb_msgtype type);
	void		inc_msg_types_in(orb_msgtype type);
	void		inc_orphans(orb_msgtype type);
	void		add_memory_out(uint_t size);
	void		add_memory_in(uint_t size);

	enum {NUMBER_HISTOGRAM_SIZES = 8,
		HISTOGRAM_BASE = 64,
		HISTOGRAM_FACTOR = 4};
	//
	// The following fields record orb level message activity.
	//
	// Always specify the precise size of these fields,
	// because these statistics are recorded in the kernel
	// and passed to user level applications. We do not want to do
	// conversions between different size long's, etc.
	//
	uint32_t	msg_types_out[N_MSGTYPES];
	uint32_t	msg_types_in[N_MSGTYPES];
	uint32_t	orphans[N_MSGTYPES];
	uint64_t	memory_out;
	uint64_t	memory_in;
	uint32_t	size_histogram_out[NUMBER_HISTOGRAM_SIZES];
	uint32_t	size_histogram_in[NUMBER_HISTOGRAM_SIZES];
};

//
// cl_msg_stats - used by cladm commands to access message statistics
//
struct cl_msg_stats {
	nodeid_t	nodeid;
	msg_stats	stats;
};

#include <orb/msg/msg_stats_in.h>

#endif	/* _MSG_STATS_H */
