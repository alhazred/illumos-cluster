/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  message_mgr_in.h
 *
 */

#ifndef _MESSAGE_MGR_IN_H
#define	_MESSAGE_MGR_IN_H

#pragma ident	"@(#)message_mgr_in.h	1.9	08/05/20 SMI"

inline void
node_msg_mgr::lock()
{
	lck.lock();
}

inline void
node_msg_mgr::unlock()
{
	lck.unlock();
}

inline
node_msg_mgr::node_msg_mgr() :
	remotemgr(NULL),
	next_incn(0)
{
}

inline
msgmgrref::msgmgrref(node_msg_mgr *mgr, nodeid_t nid) :
	Anchor<orbmsg::message_mgr_stub>(nid, XDOOR_MSG_MGR,
	    orbmsg::message_mgr::_interface_descriptor()),
	parent(mgr)
{
}

inline
msgmgrref::~msgmgrref()
{
	ASSERT(parent->lck.lock_held());
	parent->remotemgr = NULL;
}

inline
msg_mgr::msg_mgr() :
	buf(NULL),
	current_pos(0),
	current_seq(0)
{
}

inline
send_msg_mgr::send_msg_mgr() :
	msg_mgr(),
	min_pos(0),
	min_seq(0)
{
}

inline
recv_msg_mgr::recv_msg_mgr() :
	msg_mgr()
{
}

inline
message_mgr_slot::message_mgr_slot(orb_seq_t s, uint64_t n) :
	seq(s),
	incn(n),
	state(message_mgr_slot::UNINIT)
{
}

inline
message_mgr_slot::message_mgr_slot() :
	seq(INVALID_ORBSEQ),
	incn(0),
	state(message_mgr_slot::UNINIT)
{
}

#endif	/* _MESSAGE_MGR_IN_H */
