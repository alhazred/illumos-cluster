/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ORB_MSG_H
#define	_ORB_MSG_H

#pragma ident	"@(#)orb_msg.h	1.36	08/05/20 SMI"


#ifdef __cplusplus

#include <sys/os.h>
#include <sys/threadpool.h>
#include <orb/flow/resource_defs.h>
#include <orb/fault/fault_injection.h>
#include <orb/buffers/marshalstream.h>
#include <orb/infrastructure/orbthreads.h>
#include <sys/list_def.h>
#include <cmm/cmm_version.h>

class resources;

//
// orb_msg - provides general message support to orb level messages
// for both incoming and outgoing messages. This object delivers incoming
// messages to the function registered to process a particular type
// of messages. There is only one instance of this object on a node,
// and it is located in the kernel.
//
class orb_msg : public orbthread {
	friend class resource_balancer;
public:
	static sendstream *	reserve_resources(resources *resourcep);

	// Bottom Orb level msg processing prior to transport
	static void		send(sendstream *sendstreamp);

	// Wake up deffered thread
	void	wakeup_deferred_msgs_thread();

	// Add an element to the deferred message list
	void	add_msg_to_deferred_list(recstream *re,
	    os::mem_alloc_type flag, orb_seq_t seq);
	//
	// Send a datagram orb level msg. The only possible exceptions
	// when sending a datagram are COMM_FAILURE and RETRY_NEEDED.
	// This method will check the exceptions, and return true
	// if RETRY_NEEDED. It will also clear the exception before return.
	// It does a done on the sendstream, so you shouldn't try to access
	// it anymore.
	//
	static bool		send_datagram(sendstream *sendstreamp);

#ifdef _KERNEL_ORB
	//
	// Use explicit values for enums as they are used as array indices.
	// The code assumes that the numbers start with zero and increase
	// sequentially.
	// The use of each threadpool is described in orb_msg.cc
	//
	enum threadpool_id_t {
		THREADPOOL_ORBUTIL = 0,
		THREADPOOL_CKPT = 1,
		THREADPOOL_INVOS = 2,
		THREADPOOL_REFS = 3,
		THREADPOOL_CMM = 4,
		N_THREADPOOLS = 5 };

	typedef void (*message_handler)(recstream *);

	static orb_msg &the();

	static bool is_cmm_msg(orb_msgtype);

	// Initialize the properties of a message type in the msg_props array.
	void	set_message_props(orb_msgtype, bool, threadpool_id_t,
		    message_handler);

	// Change the number of threads in a thread pool
	int	change_num_threads(threadpool_id_t, int);

	//
	// Return a reference to the specified server threadpool
	// This function currently only called by unref_threadpool::flush_tag()
	// When FAULTNUM_HA_TRANSFER_TASK_TO_UNREF_THR is triggered, which
	// is test for bugId 4626464
	//
#ifdef _FAULT_INJECTION
	threadpool &
	get_server_threadpool(threadpool_id_t);
#endif // _FAULT_INJECTION

	// Called by cmm to quiesce the checkpoint threadpool when a node died.
	void quiesce_ckpt_msg();

	// Called by the transport
	void	deliver_message(recstream *, os::mem_alloc_type, orb_seq_t);

	// Process the message.
	void	process_message(recstream *, os::mem_alloc_type);

	// Start the service threads
	static int	initialize();
	int	initialize_int();

	// Shutdown all the service threads
	static void	shutdown();

	orb_msg();

	~orb_msg();

private:
	//
	// msg_props: Per Message type attributes.
	//
	// direct_callback - controls whether the interrupt thread services
	//	an incoming message.
	//
	//	false = use a separate thread
	//	true  = use the interrupt thread
	//
	// threadpool_id - which threadpool to use for queuing messages
	//
	// orb_handler - message handler for this message type.
	//
	typedef struct {
		bool		direct_callback;
		threadpool_id_t	threadpool_id;
		message_handler	orb_handler;
	} orb_msg_prop_t;

	orb_msg_prop_t		msg_props[N_MSGTYPES];

	threadpool		server_threads[N_THREADPOOLS];

#if defined(FAULT_ORB)
	// For ORB message retry testing.
	static void	retry_unsent_msg(orb_msgtype, Environment *);
	static void	retry_sent_msg(orb_msgtype, Environment *);
#endif // FAULT_ORB

	static orb_msg *the_orb_msg;

#else	// _KERNEL_ORB
private:
#endif	// _KERNEL_ORB
	// Disallow assignments and pass by value
	orb_msg(const orb_msg &);
	orb_msg &operator = (orb_msg &);

	// Thread dedicated to process deferred messages if any
	static void	deferred_msgs_thread(void *);
	int		initialize_thread();

	// Process any deferred messages.
	void	deferred_msgs_processing();

	//
	// Protects deferred_msgs_list.
	//
	os::rwlock_t deferred_msgs_list_lock;

	//
	// Deferred messages list
	// These messages will be processed by the deferred_msgs_thread
	// once membership has been set.
	//
	IntrList<recstream, _SList> deferred_msgs_list;
};


#include <orb/msg/orb_msg_in.h>

#endif // __cplusplus

#endif	/* _ORB_MSG_H */
