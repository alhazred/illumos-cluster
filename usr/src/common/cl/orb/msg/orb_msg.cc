//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb_msg.cc	1.75	08/10/13 SMI"

#include <orb/infrastructure/orb_conf.h>

#ifdef _KERNEL
#include <orb/infrastructure/clusterproc.h>
#endif

#include <orb/msg/orb_msg.h>
#include <orb/fault/fault_injection.h>
#include <orb/buffers/marshalstream.h>

#ifndef _KERNEL_ORB
#include <orb/transport/sdoor_transport.h>
#else
#include <sys/mc_probe.h>
#include <orb/xdoor/rxdoor.h>
#include <orb/infrastructure/sigs.h>
#include <orb/refs/refcount.h>
#include <orb/fault/fault_rpc.h>
#include <orb/flow/resource_mgr.h>
#include <orb/fault/fault_injection.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/msg/message_mgr.h>
#include <orb/fault/fault_injection.h>
#include <orb/member/members.h>
#include <orb/debug/orb_stats_mgr.h>
#include <vm/vm_comm.h>
#include <sys/cl_comm_support.h>

// initial number of server invocation receiver threads for different pools.
const int	initial_orbutil_threads = 3;
const int	initial_ckpt_threads = 1;
const int	initial_ref_threads = 2;

// REFJOB messages need to be processed at a higher priority than the default
const int	threadpool_refs_pri = 65;

//
// Properties for orb message threadpools
// THREADPOOL_ORBUTIL - used for orb utility datagrams, which include
//	SIGNAL, FAULT, and flow control messages.
//	REPLY messages normally use the interrupt thread.
//	However, they use this threadpool in case of memory overflow.
//	Uses dynamic threadpool.
//	The initial number of threads is initial_orbutil_threads.
//
// THREADPOOL_CKPT - used for Checkpoint messages
//	Use single thread static threadpool
//	This is needed because we need explicit control of being able
//	to flush the threadpool or wait for completion of processing
//	of messages before we declare a node dead
//
// THREADPOOL_INVOS - used for both INVO and INVO_ONEWAY messages.
//	Uses static threadpool with initially 1 thread.
//	Flow control controls the number of threads.
//
// THREADPOOL_REFS - used for REFJOB datagrams.
//	The threadpool has a dynamic size.
//	The initial number of threads is initial_ref_threads.
//
// THREADPOOL_CMM - used for CMM messages.
//	The threadpool is static with one thread.
//	The thread runs as a high-priority realtime thread.
//

orb_msg *orb_msg::the_orb_msg = NULL;

// constructor
orb_msg::orb_msg()
{
	//
	// Set the threadpool properties known at compile time
	// Only set the name and dynamic property here. in orb_msg::initialize()
	// the server threads will actually be started.
	//
	(void) server_threads[THREADPOOL_ORBUTIL].modify(true, 0,
	    "THREADPOOL_ORBUTIL");

	(void) server_threads[THREADPOOL_CKPT].modify(false, 0,
	    "THREADPOOL_CKPT");

	(void) server_threads[THREADPOOL_INVOS].modify(false, 0,
	    "THREADPOOL_INVOS");

	(void) server_threads[THREADPOOL_REFS].modify(true, 0,
	    "THREADPOOL_REFS");
	(void) server_threads[THREADPOOL_REFS].set_sched_props(
	    threadpool_refs_pri);

	(void) server_threads[THREADPOOL_CMM].modify(false, 1,
	    "THREADPOOL_CMM");
	(void) server_threads[THREADPOOL_CMM].set_sched_props(
	    (int)orb_conf::rt_maxpri(), orb_conf::rt_classname());

	//
	// Initialize the message properties known at compile time.
	// Rest can be initialized dynamically
	//
	set_message_props(INVO_MSG, false, THREADPOOL_INVOS,
	    rxdoor::handle_twoway);

	set_message_props(REPLY_MSG, true, THREADPOOL_ORBUTIL,
	    rxdoor::handle_reply);

	set_message_props(REFJOB_MSG, false, THREADPOOL_REFS,
	    refcount::HandleRefCnts);

#ifdef	CMM_VERSION_0
	// This is initialized by cmm_comm_impl later
	set_message_props(CMM_MSG, false, THREADPOOL_CMM, NULL);
#endif // CMM_VERSION_0

	//
	// This is initialized by cmm_comm_impl later
	// version 1 CMM
	//
	set_message_props(CMM1_MSG, false, THREADPOOL_CMM, NULL);

#ifdef DEBUG
	//
	// This is initialized by cmm_comm_impl later.
	//
	set_message_props(STOP_MSG, true, THREADPOOL_ORBUTIL, NULL);
#endif // DEBUG

	set_message_props(VM_MSG, false, THREADPOOL_ORBUTIL,
	    vm_comm_handle_messages_funcp);

	set_message_props(SIGNAL_MSG, false, THREADPOOL_ORBUTIL,
	    signals::handle_messages);

	set_message_props(CKPT_MSG, false, THREADPOOL_CKPT,
	    rxdoor::handle_ckpt);

	set_message_props(FLOW_MSG, false, THREADPOOL_ORBUTIL,
	    resource_mgr::handle_messages);

	set_message_props(INVO_ONEWAY_MSG, false, THREADPOOL_INVOS,
	    rxdoor::handle_oneway);

	set_message_props(COMPLETED_MAYBE_RESOLVE_MSG, false,
	    THREADPOOL_ORBUTIL, message_mgr::handle_message);

	// If fault injection is defined, set the message handler
#ifdef _FAULT_INJECTION
	set_message_props(FAULT_MSG, false, THREADPOOL_ORBUTIL,
	    fault_rpc::handle_messages);
#else
	set_message_props(FAULT_MSG, false, THREADPOOL_ORBUTIL, NULL);
#endif
}

int
orb_msg::initialize()
{
	ASSERT(the_orb_msg == NULL);
	the_orb_msg = new orb_msg;
	if (the_orb_msg == NULL) {
		return (ENOMEM);
	}
	if (the_orb_msg->initialize_thread() != 0) {
		return (ENOMEM);
	}
	return (the_orb_msg->initialize_int());
}

//
// initialize_int - called from ORB::initialize.
// Actually start the server threads here
// instead of in the global constructor.
//
int
orb_msg::initialize_int()
{
	int t1 = server_threads[THREADPOOL_ORBUTIL].change_num_threads(
		initial_orbutil_threads);
	int t2 = server_threads[THREADPOOL_CKPT].change_num_threads(
		initial_ckpt_threads);
	int t3 = server_threads[THREADPOOL_REFS].change_num_threads(
		initial_ref_threads);

	// Return success if at least one thread was started in each pool
	return (((t1 > 0) && (t2 > 0) && (t3 > 0)) ? 0 : ENOMEM);
}

void
orb_msg::process_message(recstream *re, os::mem_alloc_type flag)
{
	MC_PROBE_0(orbmsg_process_msg_start, "clustering orb message", "");
	orb_msgtype msgtyp = re->get_msgtype();

#if defined(FAULT_ORPHAN)
	//
	// Defer reply messages. Since this fault point is before fault
	// triggers are unmarshaled, only NodeTriggers can be used.
	//
	if ((msgtyp == REPLY_MSG) && (flag == os::NO_SLEEP) &&
	    fault_triggered(FAULTNUM_ORPHAN_DEFER_REPLIES, NULL, NULL)) {
		server_threads[msg_props[msgtyp].threadpool_id].
		    defer_processing(re);
		return;
	}
#endif
	//
	// Called with os::SLEEP or if direct_callback, attempt to initialize
	// And if initialize was successful, call the orb handler
	//
	if ((flag == os::SLEEP) || msg_props[msgtyp].direct_callback) {
		MC_PROBE_0(orbmsg_process_msg_toinit, "clustering orb message",
		    "");
		if (re->initialize(flag)) {
			//
			// A message can arrive before the handler has
			// been registered. discard such messages.
			//
			if (msg_props[msgtyp].orb_handler == NULL) {
				re->done();
			} else {
				//
				// Record Statistics on received message
				//
				// The cost of determining the size of a
				// received message is expected to be
				// relatively inexpensive. The received
				// buffer structure usually matches that
				// generated. See orb_msg::send for an
				// explanation of why this operation
				// should be inexpensive.
				//
				orb_stats_mgr	&stats_mgr =
				    orb_stats_mgr::the();
				sol::nodeid_t	src_ndid =
				    re->get_src_node().ndid;
				stats_mgr.inc_msg_types_in(src_ndid, msgtyp);
				stats_mgr.add_memory_in(src_ndid, re->span());

#if defined(_FAULT_INJECTION)
				// Create a new place to put fault information,
				// while saving any old fault information.
				InvoTriggers	*trigp =
				    InvoTriggers::new_trigger(flag);

				if (trigp == NULL) {
					// No place to put data. So discard it.
					InvoTriggers::strip_trigger_info(
					    re->get_MainBuf());
				} else {
					// Unmarshal fault information
					// This thread receives the data
					trigp->get_trigger_info(re);
				}
				// The fault injection header and data have
				// been removed from the recstream.
#endif
				//
				// Set fault point to check threads being used
				// for invocation processing.
				//
				FAULTPT_FLOWCONTROL
				    (FAULTNUM_FLOWCONTROL_PROCESS_MESSAGE,
				    fault_flowcontrol::generic_test_op);

				(*(msg_props[msgtyp].orb_handler))(re);
#if defined(_FAULT_INJECTION)
				// Clear all triggers,
				// restoring any prior trigger info.
				if (trigp != NULL) {
					InvoTriggers::clear_all();
					delete trigp;
				}
#endif
			}
			MC_PROBE_0(orbmsg_process_msg_end,
			    "clustering orb message", "");
			return;
		}
	}

	// This msgttype is either not a direct callback or we had a memory
	// failure.
	// After the recstream task is enqueued,
	// eventually a server thread will grab this task, and
	// call this method with sleep enabled.
	server_threads[msg_props[msgtyp].threadpool_id].defer_processing(re);
}

//
// deliver_message
// (1) If source node is dead, deletes recstream object in recstream::done().
// (2) If membership info not known, appends recstream to
//	the deferred_msgs_list. No new memory is allocated.
// (3) Else the system processes the message now.
//
void
orb_msg::deliver_message(recstream *re, os::mem_alloc_type flag, orb_seq_t seq)
{
	MC_PROBE_0(orbmsg_deliver_message, "clustering orb message", "");
	if (seq != INVALID_ORBSEQ) {
		// Update local seq info if the message is from a live node.
		switch (message_mgr::the().received(re, flag, seq)) {
		case message_mgr::OK_TO_PROCESS:
			//
			// Process the message.
			//
			break;
		case message_mgr::DO_NOT_PROCESS:
			// If the src_node is dead we return.
			re->done();
			return;
		case message_mgr::DEFERRED:
			//
			// Membership is not known - Can't process the
			// message now. The message has already been placed
			// on a list for later processing.
			//
			return;
		default:
			/* NOTREACHED */
			ASSERT(0);
		}
	}

	process_message(re, flag);
}

//
// deferred_msgs_thread - becomes a thread that will
// process msgs once membership is known
// static
void
orb_msg::deferred_msgs_thread(void *)
{
	// Initialized in message_mgr::initialize_thread

	orb_msg::the().deferred_msgs_processing();
}

//
// deferred_msgs_processing - processes any deferred messages
// once membership is known.
//
void
orb_msg::deferred_msgs_processing()
{
	recstream *deferred_msg;

	//
	// Lock the threadLock mutex using lock orbthread method
	//
	lock();
	threadcv.wait(&threadLock);

	if (state == orbthread::ACTIVE) {
		deferred_msgs_list_lock.wrlock();

		for (deferred_msgs_list.atfirst();
		    (deferred_msg = deferred_msgs_list.get_current()) != NULL;
		    deferred_msgs_list.advance()) {
			deliver_message(deferred_msg,
			    deferred_msg->get_rec_flag(),
			    deferred_msg->get_rec_seq());
			(void) deferred_msgs_list.erase(deferred_msg);
		}
		deferred_msgs_list_lock.unlock();
		// The thread will now go away.
	}
	state = orbthread::DEAD;
	threadGone.signal();
	unlock();
}

//
// initialize_thread - starts up the thread that will manages deferred
// messages once membership is known.
//
int
orb_msg::initialize_thread()
{
	// Register with the list of orb threads
	registerthread();

	// Create the thread that manages resources
#ifdef _KERNEL
	if (clnewlwp((void(*)(void *))orb_msg::deferred_msgs_thread,
	    NULL, 60, NULL, NULL) != 0) {
#else
	if (os::thread::create(NULL, (size_t)0,
	    (void *(*)(void *))orb_msg::deferred_msgs_thread,
	    NULL, (long)(THR_BOUND | THR_DETACHED), NULL)) {
#endif
		(void) unregisterthread();
		return (ENOMEM);
	}
	return (0);
}

//
// add_msg_to_deferred_list()
//
void
orb_msg::add_msg_to_deferred_list(recstream *re, os::mem_alloc_type flag,
    orb_seq_t seq)
{
	deferred_msgs_list_lock.wrlock();
	re->set_rec_flag(flag);
	re->set_rec_seq(seq);
	deferred_msgs_list.append(re);
	deferred_msgs_list_lock.unlock();
}

//
// wakeup_deferred_msgs_thread - Signal the deferred_msgs_thread to proceed
// with deferred messages if any.
//
void
orb_msg::wakeup_deferred_msgs_thread()
{
	lock();
	threadcv.signal();
	unlock();
}

void
orb_msg::shutdown(void)
{
	uint_t p;

	if (the_orb_msg == NULL) {
		return;
	}

	// All nodes are declared dead and transports shutdown by now
	// So the traffic should be dying down!
	// we just wait till pending list is empty and threads are idle
	for (p = 0; p < N_THREADPOOLS; p++) {
		the_orb_msg->server_threads[p].quiesce(threadpool::QEMPTY);
	}
	for (p = 0; p < N_THREADPOOLS; p++) {
		the_orb_msg->server_threads[p].shutdown();
	}
	delete the_orb_msg;
	the_orb_msg = NULL;
}

// set_message_props
void
orb_msg::set_message_props(orb_msgtype mt, bool flag,
    orb_msg::threadpool_id_t tid, message_handler mh)
{
	ASSERT((uint_t)mt < N_MSGTYPES);
	msg_props[mt].direct_callback = flag;
	msg_props[mt].threadpool_id = tid;
	msg_props[mt].orb_handler = mh;
}

//
// change_num_threads - in the specified thread pool.
// This method does not guarantee that it will be able to make the change.
// The change can increase or decrease the number of threads.
// Returns the number of threads actually changed.
//
int
orb_msg::change_num_threads(threadpool_id_t thread_pool, int number_threads)
{
	return (server_threads[thread_pool].change_num_threads(number_threads));
}

//
// destructor
//
orb_msg::~orb_msg(void)
{
}

#endif	// _KERNEL_ORB

//
// send - Bottom Orb level msg processing prior to transport
//
// static
#ifdef _KERNEL_ORB
void
orb_msg::send(sendstream *sendstreamp)
{
	Environment *e = sendstreamp->get_env();
	ID_node &dest = sendstreamp->get_dest_node();
	orb_seq_t seq = INVALID_ORBSEQ;

	ASSERT(dest.ndid != orb_conf::node_number());
	ASSERT(!e->exception());
	ASSERT(!e->is_nonblocking() ||
	    sendstreamp->get_invo_mode().is_unreliable());

	// We shouldn't have a sequence number for non-blocking and
	// COMPLETED_MAYBE_RESOLVE_MSG messages.
	bool need_seq = (!e->is_nonblocking()) &&
	    (sendstreamp->get_msgtype() != COMPLETED_MAYBE_RESOLVE_MSG) &&
	    (sendstreamp->get_msgtype() != VM_MSG);

#ifdef _FAULT_INJECTION
	// Marshal the trigger info for remote messages
	InvoTriggers::put(sendstreamp);
#endif	// _FAULT_INJECTION

	//
	// Record outbound message statistics
	//
	// In order to determine the memory size of the message,
	// this software must add the size of all of the buffers.
	// This operation could become expensive. However, the
	// flow control "reserve_resources" usually succeeds in allocating
	// one buffer for MainBuf. Normally all of the object references
	// fit in one buffer in XdoorTab. Only rarely are there multiple
	// Offline buffers, and in those cases each offline buffer contains
	// a lot of data, so the overhead is relatively small.
	// Thus the total overhead of determining the message size is expected
	// to be relatively small. Memory size will be used at customer
	// sites to determine the utilization of the interconnect bandwidth.
	//
	orb_stats_mgr	&stats_mgr = orb_stats_mgr::the();
	stats_mgr.inc_msg_types_out(dest.ndid, sendstreamp->get_msgtype());
	stats_mgr.add_memory_out(dest.ndid, sendstreamp->span());

#ifdef DEBUG
	uint_t xdsize = sendstreamp->get_XdoorTab().span();
#endif // DEBUG

	MC_PROBE_0(orbmsg_send_sending, "clustering orb message", "");

#if defined(FAULT_ORB)
	retry_unsent_msg(sendstreamp->get_msgtype(), e);

	// Tempory testing stuff. Test not sending a message
	// and have a COMPLETED_MAYBE. This type of faults should be
	// infrequent.
	static uint_t execount = 0;
	static os::mutex_t exelck;
	if (!e->exception()) {
		exelck.lock();
		if (++execount > 50) {
			// This gives a COMPLETED_MAYBE 5% of time.
			retry_sent_msg(sendstreamp->get_msgtype(), e);
			// Because we are artificially raising a RETRY
			// exception without calling sendstream::send,
			// we need to allocate the sequence number ourselves
			if (need_seq &&
			    CORBA::RETRY_NEEDED::_exnarrow(e->exception()) &&
			    !message_mgr::the().send_prepare(dest, seq)) {
				e->system_exception(CORBA::COMM_FAILURE(0,
				    CORBA::COMPLETED_NO));
			}
			execount = 0;
		}
		exelck.unlock();
	}

	if (! e->exception()) {
		sendstreamp->send(need_seq, seq);
		if (! e->exception()) {
			retry_sent_msg(sendstreamp->get_msgtype(), e);
		}
	}
#else
	sendstreamp->send(need_seq, seq);
#endif

	MC_PROBE_0(orbmsg_send_sent, "clustering orb message", "");

	// verify that the XdoorTab was left unmodified by the transport
	// This is required by the reference counting protocol
	ASSERT(xdsize == sendstreamp->get_XdoorTab().span());

	//
	// ASSERTs to enforce transport behavior
	//
	// The transport can only raise system exceptions, if any.
	// If COMM_FAILURE or RETRY_NEEDED exception, then completion
	// status can be any.
	// For any other exception, the completion status must be
	// COMPLETED_NO and the transport must guarantee that the
	// message has not been received by the receiver. The ORB can
	// only handle such non-COMM_FAILURE exceptions.
	//
	ASSERT(!e->exception() ||
	    CORBA::COMM_FAILURE::_exnarrow(e->sys_exception()) ||
	    (CORBA::RETRY_NEEDED::_exnarrow(e->sys_exception()) &&
	    e->sys_exception()->completed() != CORBA::COMPLETED_YES) ||
	    (e->sys_exception()->completed() == CORBA::COMPLETED_NO));

	// Deal with RETRY_NEEDED exception. Convert RETRY_NEEDED
	// COMPLETED_MAYBE into either COMM_FAILURE, COMPLETED_NO or
	// no exception.
	if (CORBA::RETRY_NEEDED::_exnarrow(e->exception()) &&
	    e->sys_exception()->completed() == CORBA::COMPLETED_MAYBE) {
		if (!need_seq) {
			// If we are on a non-blocking context we can't ask the
			// other side. We return WOULDBLOCK exception instead.
			if (e->is_nonblocking()) {
				e->system_exception(CORBA::WOULDBLOCK(0,
				    CORBA::COMPLETED_MAYBE));
			}
		} else {
			orb_msgtype msg_type = sendstreamp->get_msgtype();

			// Ask the receiving side whether it received the
			// message. We set a fault to say that for this
			// is_received invocation the RETRY_NEEDED faults
			// should be generated at a lower rate.
			switch (message_mgr::the().is_received(dest,
			    seq, msg_type)) {
			case CORBA::COMPLETED_MAYBE :
				// implies remote node is dead
				e->system_exception(CORBA::COMM_FAILURE(0,
				    CORBA::COMPLETED_NO));
				break;
			case CORBA::COMPLETED_YES :
				// message was received, clear exception
				e->clear();
				break;
			case CORBA::COMPLETED_NO :
				// message was not received. change completion
				// status
				e->sys_exception()->completed(
				    CORBA::COMPLETED_NO);
				break;
			default :
				break;
			}
		}
	}

	// Send_done clear the bitmask for the sequence number we allocated.
	// We don't need to call send_done if there is a comm failure, in which
	// case the cmm reconfiguration will clear up the bitmask.
	if ((seq != INVALID_ORBSEQ) &&
	    !CORBA::COMM_FAILURE::_exnarrow(e->exception())) {
		message_mgr::the().send_done(dest, seq);
	}
}
#endif	// _KERNEL_ORB


// Send a datagram orb level msg. The only possible exceptions
// when sending a datagram are COMM_FAILURE and RETRY_NEEDED.
// This method will check the exceptions, and return true
// if RETRY_NEEDED. It will also clear the exception before return.
// static
#ifdef _KERNEL_ORB
bool
orb_msg::send_datagram(sendstream *sendstreamp)
{
	// datagrams are oneway. sanity check.
	ASSERT(sendstreamp->get_invo_mode().is_oneway());

	bool retry_needed = false;
	orb_msg::send(sendstreamp);
	Environment *e = sendstreamp->get_env();
	if (e->exception()) {
		if (CORBA::RETRY_NEEDED::_exnarrow(e->exception())) {
			retry_needed = true;
		} else {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e->exception()));
		}
		e->clear();
	}
	sendstreamp->done();
	return (retry_needed);
}
#endif // _KERNEL_ORB

//
// reserve_resources - this reserves resources for the orb_msg layer,
// which is the space needed for fault injection.
//
// static
sendstream *
orb_msg::reserve_resources(resources *resourcep)
{
#ifdef _FAULT_INJECTION
	if (!resourcep->show_fault_injection()) {
		// This is first time sending message.
		// Fault injection can reserve a lot of space,
		// so should only do it once.
		resourcep->record_fault_injection();

		// Reserve resources for marshalling trigger info
		InvoTriggers::reserve_resources(resourcep, InvoTriggers::the());
	}
#endif	// _FAULT_INJECTION

#ifdef _KERNEL_ORB
	return (endpoint_reserve_resources_funcp(resourcep));
#else
	return (sdoor_transport::reserve_resources(resourcep));
#endif
}


#if defined(FAULT_ORB) && defined(_KERNEL_ORB)
//
// Fault points for ORB message retries.
//
// The lint override is to stop it from complaining about 'e' not
// being used in the default case of the switch statement.
//
// static
void
orb_msg::retry_unsent_msg(orb_msgtype msgtype, Environment *e)

{
	switch (msgtype) {
	case INVO_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_INVO,
				e, FaultFunctions::set_sys_exception);
		break;
	case REPLY_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_REPLY,
				e, FaultFunctions::set_sys_exception);
		break;
	case REFJOB_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_REFJOB,
				e, FaultFunctions::set_sys_exception);
		break;
#ifdef	CMM_VERSION_0
	case CMM_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_CMM,
				e, FaultFunctions::set_sys_exception);
		break;
#endif 	// CMM_VERSION_0
	case CMM1_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_CMM,
				e, FaultFunctions::set_sys_exception);
		break;
	case VM_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_VM,
				e, FaultFunctions::set_sys_exception);
		break;
	case SIGNAL_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_SIGNAL,
				e, FaultFunctions::set_sys_exception);
		break;
	case CKPT_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_CKPT,
				e, FaultFunctions::set_sys_exception);
		break;
	case FLOW_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_FLOW,
				e, FaultFunctions::set_sys_exception);
		break;
	case INVO_ONEWAY_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_ONEWAY,
				e, FaultFunctions::set_sys_exception);
		break;
	case FAULT_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_FAULT,
				e, FaultFunctions::set_sys_exception);
		break;
	case COMPLETED_MAYBE_RESOLVE_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_UNSENT_MAYBE,
				e, FaultFunctions::set_sys_exception);
		break;
	default:
		break;
	}
} 	//lint !e715

// static
void
orb_msg::retry_sent_msg(orb_msgtype msgtype, Environment *e)
{
	switch (msgtype) {
	case INVO_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_INVO,
				e, FaultFunctions::set_sys_exception);
		break;
	case REPLY_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_REPLY,
				e, FaultFunctions::set_sys_exception);
		break;
	case REFJOB_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_REFJOB,
				e, FaultFunctions::set_sys_exception);
		break;
#ifdef	CMM_VERSION_0
	case CMM_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_CMM,
				e, FaultFunctions::set_sys_exception);
		break;
#endif // CMM_VERSION_0
	case CMM1_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_CMM,
				e, FaultFunctions::set_sys_exception);
		break;
	case VM_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_VM,
				e, FaultFunctions::set_sys_exception);
		break;
	case SIGNAL_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_SIGNAL,
				e, FaultFunctions::set_sys_exception);
		break;
	case CKPT_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_CKPT,
				e, FaultFunctions::set_sys_exception);
		break;
	case FLOW_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_FLOW,
				e, FaultFunctions::set_sys_exception);
		break;
	case INVO_ONEWAY_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_ONEWAY,
				e, FaultFunctions::set_sys_exception);
		break;
	case FAULT_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_FAULT,
				e, FaultFunctions::set_sys_exception);
		break;
	case COMPLETED_MAYBE_RESOLVE_MSG:
		FAULTPT_ENV_ORB(FAULTNUM_ORB_RETRY_SENT_MAYBE,
				e, FaultFunctions::set_sys_exception);
		break;
	default:
		break;
	}
} //lint !e715
#endif // FAULT_ORB && _KERNEL_ORB
