//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)message_mgr.cc	1.28	08/10/13 SMI"

#include <orb/msg/message_mgr.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/debug/orb_trace.h>
#include <orb/msg/orb_msg.h>
#include <orb/debug/orb_stats_mgr.h>
#include <sys/cl_comm_support.h>

// The bitmask size in byte and in bit.
#define	RBUFSIZE 2048
#define	RBUFSIZEBIT (RBUFSIZE << 3)

// The range of sequence number. (1 << 32) - 1.
#define	SEQRANGE	0xffffffffU

orb_seq_t
msg_mgr::minus(orb_seq_t &s, uint32_t num)
{
	orb_seq_t rst = (orb_seq_t)(s - num);

	// If we crossed the line we need to take into account the
	// INVALID_ORBSEQ.
	if (rst > s) {
		rst--;
	}
	return (rst);
}

// Increment the sequence by one.
void
msg_mgr::inc(orb_seq_t &s)
{
	if (++s == INVALID_ORBSEQ) {
		++s;
	}
	ASSERT(s != INVALID_ORBSEQ);
}

void
msg_mgr::reset()
{
	current_pos = 0;
	current_seq = 0;
	if (buf != NULL) {
		bzero(buf, (size_t)RBUFSIZE);
	}
}

void
msg_mgr::create_buf()
{
	if (buf == NULL) {
		buf = new uint8_t[RBUFSIZE];
	}
	reset();
}	

void
msg_mgr::delete_buf()
{
	delete[] buf;
	buf = NULL;
}

// Returns true if s1 is smaller than s2.
bool
msg_mgr::smallerthan(orb_seq_t s1, orb_seq_t s2)
{
	// Flip the highest bit to be our common minimum.
	orb_seq_t minseq = s1 ^ 0x80000000U;
	return (((s1 >= minseq && s2 >= minseq) ||
	    (s1 < minseq && s2 < minseq)) ?
	    (s1 < s2) : (s1 > s2));
}

int32_t
msg_mgr::get_pos(int32_t base_pos, int32_t advance)
{
	// It should never happen in our application for the advance
	// to be bigger than the buffer size.
	ASSERT(base_pos >= 0 && base_pos < RBUFSIZEBIT);
	ASSERT(advance < RBUFSIZEBIT && advance > -RBUFSIZEBIT);
	int32_t newpos = base_pos + advance;
	if (newpos >= RBUFSIZEBIT) {
		newpos -= RBUFSIZEBIT;
	} else if (newpos < 0) {
		newpos += RBUFSIZEBIT;
	}
	ASSERT(newpos >= 0 && newpos < RBUFSIZEBIT);
	return (newpos);
}

int32_t
msg_mgr::get_pos(orb_seq_t seq)
{
	ASSERT(seq != INVALID_ORBSEQ);

	// We know that the max distance between seq and current_seq is
	// RBUFSIZEBIT.

	// This won't work if the size of orb_seq_t is 64 bit.
	ASSERT(sizeof (orb_seq_t) < 8);
	int64_t diff = (int64_t)seq - (int64_t)current_seq;
	if (diff > RBUFSIZEBIT) {
		diff -= SEQRANGE;
	} else if (diff < -RBUFSIZEBIT) {
		diff += SEQRANGE;
	}
	ASSERT(diff < RBUFSIZEBIT && diff > -RBUFSIZEBIT);

	// Alternatively the above could be replaced by:
	// orb_seq_t diff = seq - current_seq;
	// which should save some time but look more obscure.

	return (msg_mgr::get_pos(current_pos, (int32_t)diff));
}

bool
msg_mgr::is_set(int32_t pos)
{
	ASSERT(pos >= 0 && pos < RBUFSIZEBIT);
	int32_t indx = pos / 8;
	int32_t offset = pos % 8;
	return ((buf[indx] & (1 << offset)) != 0); //lint !e661
}

void
msg_mgr::set(int32_t pos)
{
	ASSERT(pos >= 0 && pos < RBUFSIZEBIT);
	int32_t indx = pos / 8;
	int32_t offset = pos % 8;
	buf[indx] |= (1 << offset); //lint !e661
}

void
msg_mgr::clear(int32_t pos)
{
	ASSERT(pos >= 0 && pos < RBUFSIZEBIT);
	int32_t indx = pos / 8;
	int32_t offset = pos % 8;
	buf[indx] &= (~(1 << offset)); //lint !e661 !e502
}

void
send_msg_mgr::reset()
{
	msg_mgr::reset();
	min_pos = 0;
	min_seq = 0;
}

// Get the next available sequence number into the environment. It returns
// true if it succeeds and returns false it not.
bool
send_msg_mgr::get_new_seq(orb_seq_t &seq)
{
	ASSERT(buf != NULL);
	orb_seq_t newseq = current_seq;
	int32_t newpos = current_pos;
	// We need to move current pointers to the next right place.
	// If we are not able to do that we return false.
	while (true) {
		// Advance both.
		newpos = msg_mgr::get_pos(newpos, 1);
		msg_mgr::inc(newseq);
		if (newpos == min_pos) {
			return (false);
		}
		if (hash.lookup(newseq) != NULL) {
			// The sequence is marked as unusuable. We set the
			// corresponding bit and go on.
			set(newpos);
		} else {
			// We found the position to move current_seq and
			// current_pos to. We can return success.

			// the message we are about to send isn't confirmed.
			// So clear the bit.
			clear(newpos);

			// Set up the return value.
			seq = current_seq;

			// Set the current pointers correctly.
			current_pos = newpos;
			current_seq = newseq;
			return (true);
		}
	}
}

// The specified message is confirmed by the receiver. Update
// our local info. Returns true if a cv.broadcast is needed.
bool
send_msg_mgr::confirmed(orb_seq_t seq)
{
	ASSERT(buf != NULL);
	set(get_pos(seq));

	// Update the info about min if the message just confirmed is
	// the old min.
	if (seq == min_seq) {
		//
		// Advance the min_pos position until it finds a message
		// that has not been acked. Do not advance past the current
		// front of the circular buffer current_pos.
		//
		while (is_set(min_pos) && (min_pos != current_pos)) {
			msg_mgr::inc(min_seq);
			min_pos = msg_mgr::get_pos(min_pos, 1);
		}
		return (true);
	}
	return (false);
}

void
recv_msg_mgr::reset()
{
	msg_mgr::reset();
}

void
recv_msg_mgr::received(orb_seq_t seq)
{
	ASSERT(buf != NULL);
	// If seq >= current_seq, we need to clear all the bits between
	// seq and current_seq.
	if (!msg_mgr::smallerthan(seq, current_seq)) {

		// Clear the bits >= current_seq but < seq.
		while (current_seq != seq) {
			msg_mgr::inc(current_seq);
			clear(current_pos);
			current_pos = msg_mgr::get_pos(current_pos, 1);
		}

		// Set the bit for seq and advance current_pos/current_seq.
		set(current_pos);
		current_pos = msg_mgr::get_pos(current_pos, 1);
		msg_mgr::inc(current_seq);
	} else {
		set(get_pos(seq));
	}
}

bool
recv_msg_mgr::is_received(orb_seq_t seq)
{
	ASSERT(buf != NULL);
	bool ret;
	// We know that we didn't receive any message bigger than current_seq
	// so far.
	// if (seq >= current_seq)
	if (!msg_mgr::smallerthan(seq, current_seq)) {
		ret = false;
	} else {
		ret = is_set(get_pos(seq));
	}
	return (ret);
}

void
node_msg_mgr::node_alive()
{
	ASSERT(lck.lock_held());
	sendmgr.hash.dispose();
	recvmgr.hash.dispose();
	sendmgr.create_buf();
	recvmgr.create_buf();
}

void
node_msg_mgr::node_died()
{
	ASSERT(lck.lock_held());
	sendmgr.hash.dispose();
	recvmgr.hash.dispose();
	sendmgr.delete_buf();
	recvmgr.delete_buf();
}

message_mgr *the_message_mgr = NULL;

message_mgr &
message_mgr::the()
{
	ASSERT(the_message_mgr != NULL);
	return (*the_message_mgr);
}

message_mgr::message_mgr() :
	memberp(NULL),
	McNoref<orbmsg::message_mgr>(XDOOR_MSG_MGR)
{
}

void
message_mgr::lock_all()
{
	// Hold all the locks before write. This way we can check the
	// memberp with only one lock.
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		node_mgr[i].lock();
	}
}

void
message_mgr::unlock_all()
{
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		node_mgr[i].unlock();
	}
}

void
message_mgr::initialize()
{
	ASSERT(the_message_mgr == NULL);
	the_message_mgr = new message_mgr();
}

// We use the collective locks of all the nodes as the writer's lock.
// This way when a message comes, we only need to get one lock to check
// the membership.
void
message_mgr::set_membership(callback_membership *newmp)
{
	newmp->hold();
	lock_all();
	if (memberp != NULL) {
		callback_membership	*oldmemberp = memberp;

		// Any threads that we wake up should see the new membership
		memberp = newmp;

		// compare the old membership with the new one. If a node
		// just joined the cluster we call node_alive(), if a node
		// just left we call node_died().
		for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
			if (oldmemberp->get_incn(i) != newmp->get_incn(i)) {
				if (newmp->get_incn(i) != INCN_UNKNOWN) {
					node_mgr[i].node_alive();
				} else {
					node_mgr[i].node_died();
				}
				// There may be threads waiting to get new
				// sequence numbers, wake them up.
				node_mgr[i].cv.broadcast();
			}
		}
		oldmemberp->rele();
	} else {
		// We are joining the cluster. Do the initial setup.
		for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
			if (newmp->get_incn(i) != INCN_UNKNOWN) {
				node_mgr[i].node_alive();
			}
		}
		memberp = newmp;
		// process the msgs received before the memberhip was known
		// if any.
		orb_msg::the().wakeup_deferred_msgs_thread();
	}

	// unlock
	unlock_all();
}

bool
message_mgr::send_prepare(ID_node &nid, orb_seq_t &sq)
{
	ASSERT(nid.ndid != orb_conf::local_nodeid());

	bool ret = false;
	node_mgr[nid.ndid].lock();

	// If the node is still configured in the CCR and
	// the incn number is correct, we call get_new_seq. If get_new_seq
	// returns true, it means all is well and we should get out of the
	// loop. If it returns false, it means we are running out of seqnums
	// and we need to wait for some threads to complete their invocations.
	// If when we wake up the incn changed we abort.
	while (nid.incn == memberp->get_incn(nid.ndid) &&
	    !(ret = node_mgr[nid.ndid].sendmgr.get_new_seq(sq))) { //lint !e720
		node_mgr[nid.ndid].cv.wait(&node_mgr[nid.ndid].lck);
	}

	node_mgr[nid.ndid].unlock();
	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
	    ("%u %u send_prepare: %d\n", nid.ndid, sq, ret));
	return (ret);
}

void
message_mgr::send_done(ID_node &nid, orb_seq_t seq)
{
	ASSERT(seq != INVALID_ORBSEQ);
	node_mgr[nid.ndid].lock();
	if (nid.incn == memberp->get_incn(nid.ndid) &&
	    node_mgr[nid.ndid].sendmgr.confirmed(seq)) {
		node_mgr[nid.ndid].cv.broadcast();
	}
	node_mgr[nid.ndid].unlock();
	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM, ("%u %u send_done\n", nid.ndid, seq));
}

message_mgr::received_state_t
message_mgr::received(recstream *re, os::mem_alloc_type flag, orb_seq_t seq)
{
	ID_node &nid = re->get_src_node();
	ASSERT(seq != INVALID_ORBSEQ);
	ASSERT(nid.ndid != orb_conf::local_nodeid());
	received_state_t ret = DO_NOT_PROCESS;
	node_msg_mgr *nodep = &node_mgr[nid.ndid];

	nodep->lock();

	// We process the message if it's not orphaned. If a message is found
	// in the hashtable it means we told the sender we didn't received
	// the message, in which case we shouldn't process it either.
	if (!memberp) {
		//
		// This subsystem does not yet have membership information.
		// Defer processing of messages until we have
		// membership information.
		//
		orb_msg::the().add_msg_to_deferred_list(re, flag, seq);
		ret = DEFERRED;
	} else if ((nid.incn == memberp->get_incn(nid.ndid)) &&
	    (nodep->recvmgr.hash.lookup(seq) == NULL)) {
		nodep->recvmgr.received(seq);
		ret = OK_TO_PROCESS;
	} else {
		// This message was orphaned, leave state as DO_NOT_PROCESS
		ret = DO_NOT_PROCESS;
	}
	nodep->unlock();
	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
	    ("%u %u received: %d\n", nid.ndid, seq, ret));
	return (ret);
}

CORBA::CompletionStatus
message_mgr::is_received(ID_node &dest_node, orb_seq_t seq,
    orb_msgtype msg_type)
{
	bool		msg_sent;

	node_msg_mgr *nodep = &node_mgr[dest_node.ndid];
	nodep->lock();
	// Prepare a slot where the answer can be stored.
	message_mgr_slot slot(seq, (nodep->next_incn)++);
	nodep->wait_list.prepend(&slot);
	nodep->unlock();

	// send the question out.
	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
	    ("%u %u send_question\n", dest_node.ndid, seq));
	msg_sent =
	    send_message(dest_node, (void *)&slot, (uint_t)sizeof (slot));
	if (!msg_sent) {
		// Message was not sent successfully
		ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
		    ("%u %u Destination node went down: msg_type %d\n",
		    dest_node.ndid, seq, msg_type));
		//
		// If the message is not a Version Manager message and
		// the message is not a stop message and
		// it is not a fault message coming from the transition thread
		// then wait
		//
#ifdef DEBUG
		if ((msg_type != VM_MSG) && (msg_type != STOP_MSG) &&
		    !((msg_type == FAULT_MSG) &&
		    (os::thread::self() ==
		    get_transition_thread_id_funcp()))) {
#else
		if ((msg_type != VM_MSG) &&
		    !((msg_type == FAULT_MSG) &&
		    (os::thread::self() ==
		    get_transition_thread_id_funcp()))) {
#endif
			nodep->lock();
			ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
			    ("%u %u Waiting for node to be marked down. "
			    "msg_type %d\n",
			    dest_node.ndid, seq, msg_type));
			while (dest_node.incn ==
			    memberp->get_incn(dest_node.ndid)) {
				nodep->cv.wait(&nodep->lck);
			}
			(void) nodep->wait_list.erase(&slot);
			nodep->unlock();
			return (CORBA::COMPLETED_MAYBE);
		} else {
			//
			// We were unable to contact the other node.
			// So there will be no reply. COMM_FAILUE
			//
			nodep->lock();
			(void) nodep->wait_list.erase(&slot);
			nodep->unlock();
			return (CORBA::COMPLETED_MAYBE);
		}
	}

	// Now wait for the answer to come back.
	nodep->lock();
	while (dest_node.incn == memberp->get_incn(dest_node.ndid) &&
	    (slot.state == message_mgr_slot::UNINIT)) {
		nodep->cv.wait(&nodep->lck);
	}
	CORBA::CompletionStatus retval;
	switch (slot.state) {
	case message_mgr_slot::UNINIT :
		// This means the remote node died.
		retval = CORBA::COMPLETED_MAYBE;
		break;
	case message_mgr_slot::IS_RECVD :
		retval = CORBA::COMPLETED_YES;
		break;
	default :
		retval = CORBA::COMPLETED_NO;

		// If answer is NO we need to mark the sequence as unusable.
		// We store "this" to differentiate from NULL
		nodep->sendmgr.hash.add(this, seq);
		break;
	}
	(void) nodep->wait_list.erase(&slot);

	nodep->unlock();

	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
	    ("%u %u the answer is: %u\n", dest_node.ndid, seq, retval));
	return (retval);
}

bool
message_mgr::send_message(ID_node &dest_node, void *argbuf, uint_t argsize)
{
	Environment e;
	ASSERT(dest_node.incn != INCN_UNKNOWN);

	bool need_retry = false;
	// Loop if retry is needed.
	do {
	// Identify message resources
		resources_datagram resource(&e,
		    0,			// header size
		    argsize,		// data size
		    dest_node, COMPLETED_MAYBE_RESOLVE_MSG);

		//
		// Check whether message can proceed now.
		// System does not block flow control messages.
		// The send stream is allocated when the msg can proceed.
		//
		sendstream *sendstreamp = orb_msg::reserve_resources(&resource);

		if (sendstreamp == NULL) {
			// This should only occur when the node is dead.
			// Node reconfiguration will perform the cleanup.
			ASSERT(e.exception());
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			return (false);
		}

		// Load the data.
		sendstreamp->put_bytes(argbuf, argsize);

		need_retry = orb_msg::send_datagram(sendstreamp);
	} while (need_retry);

	// Message sent successfully
	return (true);
}

// static
void
message_mgr::handle_message(recstream *recstreamp)
{
	ID_node		&src_node = recstreamp->get_src_node();

	message_mgr_slot slot;
	recstreamp->get_bytes((void *)&slot, sizeof (slot));

	if (slot.state == message_mgr_slot::UNINIT) {
		// This is a question.
		message_mgr::the().handle_question(src_node, slot);
	} else {
		// This is an answer.
		message_mgr::the().handle_answer(src_node, slot);
	}

	recstreamp->done();
}

void
message_mgr::handle_question(ID_node &src_node, message_mgr_slot &slot)
{
	ASSERT(slot.seq != INVALID_ORBSEQ);
	node_msg_mgr *nodep = &node_mgr[src_node.ndid];

	nodep->lock();

	//
	// If it's an orphanned message we just return.
	//
	if (src_node.incn != memberp->get_incn(src_node.ndid)) {
		orb_stats_mgr::the().inc_orphans(src_node.ndid,
		    COMPLETED_MAYBE_RESOLVE_MSG);
		nodep->unlock();
		ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
		    ("%u %u orphan ignore_question\n",
		    src_node.ndid, slot.seq));
		return;
	}

	//
	// If the sequence is behind our window it means we have answered
	// the question before (otherwise the window couldn't move). We just
	// return in that case.
	//
	if (msg_mgr::smallerthan(slot.seq,
	    msg_mgr::minus(nodep->recvmgr.current_seq, RBUFSIZEBIT - 1))) {
		nodep->unlock();
		ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
		    ("%u %u ignore_question\n", src_node.ndid, slot.seq));
		return;
	}

	ASSERT(nodep->recvmgr.buf != NULL);
	if (nodep->recvmgr.is_received(slot.seq)) {
		slot.state = message_mgr_slot::IS_RECVD;
	} else {
		slot.state = message_mgr_slot::NOT_RECVD;
		// If the answer is no we need to mark the sequence as
		// unusable.
		if (nodep->recvmgr.hash.lookup(slot.seq) == NULL) {
			// We store "this" to differentiate from NULL
			nodep->recvmgr.hash.add(this, slot.seq);
		}
	}
	nodep->unlock();

	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM, ("%u %u handle_question: %d\n",
	    src_node.ndid, slot.seq, slot.state));

	//
	// Send the answer back. If the send_message fails, we can
	// ignore it as the reply is going to a node that is dead.
	//
	(void) send_message(src_node, (void *)&slot, (uint_t)sizeof (slot));
}

void
message_mgr::handle_answer(ID_node &src_node, message_mgr_slot &slot)
{
	ASSERT(slot.seq != INVALID_ORBSEQ);
	node_msg_mgr *nodep = &node_mgr[src_node.ndid];

	ORB_DBPRINTF(ORB_TRACE_SEQ_NUM,
	    ("%u %u handle_answer: %d\n", src_node.ndid, slot.seq, slot.state));

	nodep->lock();

	// If it's an orphaned message we just return.
	if (src_node.incn != memberp->get_incn(src_node.ndid)) {
		nodep->unlock();
		return;
	}

	// Find the slot where the sender is waiting for the answer.
	message_mgr_slot *wslt;
	for (nodep->wait_list.atfirst();
	    (wslt = nodep->wait_list.get_current()) != NULL;
	    nodep->wait_list.advance()) {
		if ((wslt->seq == slot.seq) && (wslt->incn == slot.incn)) {
			break;
		}
	}
	if (wslt != NULL) {
		// Found the matching slot.
		if (wslt->state == message_mgr_slot::UNINIT) {
			wslt->state = slot.state;
			nodep->cv.broadcast();
		} else {
			// Someone else already answered the question.
			ASSERT(wslt->state == slot.state);
		}
	}
	nodep->unlock();
}

// Get the proxy to the message_mgr of the specified node.
orbmsg::message_mgr_ptr
message_mgr::get_remote_mgr(nodeid_t nid)
{
	node_msg_mgr *nodep = &node_mgr[nid];
	orbmsg::message_mgr_ptr ret;
	nodep->lock();
	if (nodep->remotemgr == NULL) {
		// XXX We should do the memory allocation outside the lock
		nodep->remotemgr = new msgmgrref(nodep, nid);
	}
	ret = orbmsg::message_mgr::_duplicate(nodep->remotemgr);
	nodep->unlock();
	return (ret);
}

void
message_mgr::release_remote_mgr(nodeid_t nid)
{
	node_msg_mgr *nodep = &node_mgr[nid];
	nodep->lock();
	ASSERT(is_not_nil(nodep->remotemgr));
	// The destructor of msgmgrref will set the nodep->remotemgr
	// back to NULL.
	CORBA::release(nodep->remotemgr);
	nodep->unlock();
}
