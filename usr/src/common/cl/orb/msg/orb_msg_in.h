/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  orb_msg_in.h
 *
 */

#ifndef _ORB_MSG_IN_H
#define	_ORB_MSG_IN_H

#pragma ident	"@(#)orb_msg_in.h	1.18	08/05/20 SMI"

#ifdef _KERNEL_ORB

inline orb_msg &
orb_msg::the()
{
	ASSERT(the_orb_msg != NULL);
	return (*the_orb_msg);
}

inline bool
orb_msg::is_cmm_msg(orb_msgtype msgt)
{
#ifdef	CMM_VERSION_0
	return ((msgt == CMM_MSG) || (msgt == CMM1_MSG));
#else
	return ((msgt == CMM1_MSG));
#endif // CMM_VERSION_0
}

inline void
orb_msg::quiesce_ckpt_msg()
{
	server_threads[THREADPOOL_CKPT].quiesce(threadpool::QFENCE);
}

inline bool
is_cmm_msg(orb_msgtype msgt)
{
	return (orb_msg::is_cmm_msg(msgt));
}

#ifdef _FAULT_INJECTION
inline threadpool &
orb_msg::get_server_threadpool(threadpool_id_t pool_id)
{
	return (server_threads[pool_id]);
}
#endif // _FAULT_INJECTION

#endif	// _KERNEL_ORB

#endif	/* _ORB_MSG_IN_H */
