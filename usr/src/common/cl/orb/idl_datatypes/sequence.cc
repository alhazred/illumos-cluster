//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sequence.cc	1.13	06/05/24 SMI"

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

#include <orb/invo/common.h>

//
// class GenericSequence
//

//
// GenericSequence Destructor - this is a virtual destructor for an abstract
// class. The derived class is responsible for cleaning up the buffer.
// The derived class releases the buffer as appropriate,
// but does not change the _buffer pointer. This results in a lint warning.
//
GenericSequence::~GenericSequence()
{
}			//lint !e1540

//
// length - sets the current length to the specified number of entries.
// If the specified number of entries exceeds the prior
// maximum sequence length; the method allocates a new buffer, transfers
// the existing contents, and sets the maximum length to the new
// current length.
//
void
GenericSequence::length(uint_t num_entries)
{
	if (num_entries <= _maximum) {
		// The maximum sequence length does not change
		modify_current_length(num_entries);
	} else {
		//
		// The new sequence length is too long for the existing buffer.
		// Thus need to create a new buffer, and transfer the
		// existing contents to the new buffer.
		//
		void	*new_buffer = alloc_buf(num_entries);

		//
		// Transfer any valid entries in old sequence.
		//
		transfer_buf(new_buffer);

		if (_release) {
			free_buf();		// call to derived class
		}
		_init(num_entries, num_entries, new_buffer, true);
	}
}

//
// copy - makes this sequence a copy of an existing sequence.
// This method is called either in the constructor or
// after a "free()" of the buffer.
// Hence, need not check value of _release and/or free existing buffer.
//
void
GenericSequence::copy(const GenericSequence &s)
{
	if (s.maximum() == 0) {
		_init(0, 0, NULL, false);
	} else {
		_init(s.maximum(), s.length(), s.copy_buf(), true);
	}
}
