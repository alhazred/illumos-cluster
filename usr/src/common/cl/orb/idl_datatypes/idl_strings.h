/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  idl_strings.h
 *
 */

#ifndef _IDL_STRINGS_H
#define	_IDL_STRINGS_H

#pragma ident	"@(#)idl_strings.h	1.6	06/05/24 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

#include <sys/os.h>

//
// This file defines the infrastructure support for CORBA-like strings
//

typedef char *String_ptr;

class _string_field {
public:
	_string_field();
	_string_field(char *);
	_string_field(const char *);
	_string_field(const _string_field &);

	_string_field	&operator = (const _string_field &);
	_string_field	&operator = (char *);
	_string_field	&operator = (const char *);

	operator char *();
	operator const char *() const;

	bool		operator == (const char *) const;
	bool		operator != (const char *) const;

	// CSTYLED
	const char	&operator [](uint_t) const;
	char		&operator [](uint_t);

private:
	String_ptr	_str;
};

class _string_var {
public:
	_string_var();
	_string_var(char *);
	_string_var(const _string_var &);
	_string_var(const _string_field &);
	~_string_var();

	_string_var	&operator = (char *);
	_string_var	&operator = (const char *);
	_string_var	&operator = (const _string_var &);

	operator char *();

	// This is used to pass _string_var as an "in" parameter
	operator const char *() const;

	// This is used to pass _string_var as an "out" parameter
	char		*&out();

	// This is used to pass _string_var as an "inout" parameter
	char		*&INOUT();
private:
	String_ptr	_str;

	// Disallow assignment from _field to _var. Refer to Bug 4091549
	_string_var	&operator = (const _string_field &);
};

class _string_out {
public:
	_string_out(char *&);
	_string_out(_string_var &);
	_string_out(const _string_out &);
	~_string_out();

	_string_out	&operator = (char *);
	_string_out	&operator = (const _string_out &);

	operator const char *() const;

	String_ptr	*operator & ();
private:
	String_ptr	*_str;
};

#include <orb/idl_datatypes/idl_strings_in.h>

#endif	/* _IDL_STRINGS_H */
