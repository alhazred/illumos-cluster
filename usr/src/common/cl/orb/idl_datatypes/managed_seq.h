/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MANAGED_SEQ_H
#define	_MANAGED_SEQ_H

#pragma ident	"@(#)managed_seq.h	1.5	08/05/20 SMI"

#include <orb/invo/ix_tmpl.h>

//
// _ManagedSeq_ - template takes two parameters:
//	SEQUENCE_TYPE - the sequence type
//	ELEMENT_TYPE - the element type for the specified sequence type
//
template<class SEQUENCE_TYPE, class ELEMENT_TYPE> class _ManagedSeq_ :
	public _T_var_<SEQUENCE_TYPE, SEQUENCE_TYPE*>
{
public:
	_ManagedSeq_();
	_ManagedSeq_(SEQUENCE_TYPE *);
	_ManagedSeq_(const _ManagedSeq_ &);

	~_ManagedSeq_();

	uint_t		maximum() const;
	uint_t		length() const;
	ELEMENT_TYPE	&operator[](uint_t);
};

#include <orb/idl_datatypes/managed_seq_in.h>

#endif	/* _MANAGED_SEQ_H */
