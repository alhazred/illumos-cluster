/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  interfaces_in.h
 *
 */

#ifndef _INTERFACESEQ_IN_H
#define	_INTERFACESEQ_IN_H

#pragma ident	"@(#)interfaceseq_in.h	1.8	08/05/20 SMI"

//
// Sequence of Interfaces of type T inline methods
//

template<class T, class T_field, class T_ptr>
inline
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_Ix_InterfaceSeq_()
{
	_init(0, 0, (void *)NULL, false);
}

template<class T, class T_field, class T_ptr>
inline
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_Ix_InterfaceSeq_(uint_t max_entries)
{
	_init(max_entries, 0, allocbuf(max_entries), true);
}

template<class T, class T_field, class T_ptr>
inline
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_Ix_InterfaceSeq_(uint_t max_entries,
    uint_t num_entries)
{
	_init(max_entries, num_entries, allocbuf(max_entries), true);
}

template<class T, class T_field, class T_ptr>
inline
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_Ix_InterfaceSeq_(uint_t max_entries,
    uint_t num_entries, T_ptr *e, bool release_flag)
{
	_init(max_entries, num_entries, (void *)e, release_flag);
}

template<class T, class T_field, class T_ptr>
inline
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_Ix_InterfaceSeq_(
    const _Ix_InterfaceSeq_ &seq)
{
	copy(seq);
}

template<class T, class T_field, class T_ptr>
inline _Ix_InterfaceSeq_<T, T_field, T_ptr> &
_Ix_InterfaceSeq_<T, T_field, T_ptr>::operator = (const _Ix_InterfaceSeq_ &seq)
{
	free();
	copy(seq);
	return (*this);
}

template<class T, class T_field, class T_ptr>
inline _Ix_InterfaceSeq_<T, T_field, T_ptr> *
_Ix_InterfaceSeq_<T, T_field, T_ptr>::operator ->()
{
	return (this);
}

template<class T, class T_field, class T_ptr>
inline const _Ix_InterfaceSeq_<T, T_field, T_ptr> *
_Ix_InterfaceSeq_<T, T_field, T_ptr>::operator ->() const
{
	return (this);
}

template<class T, class T_field, class T_ptr>
inline const T_field &
_Ix_InterfaceSeq_<T, T_field, T_ptr>::operator[](uint_t index) const
{
	return (((T_field *)_buffer)[index]);
}

template<class T, class T_field, class T_ptr>
inline T_field &
_Ix_InterfaceSeq_<T, T_field, T_ptr>::operator[](uint_t index)
{
	return (((T_field *)_buffer)[index]);
}

template<class T, class T_field, class T_ptr>
inline T_ptr
_Ix_InterfaceSeq_<T, T_field, T_ptr>::reaplast()
{
	_length--;
	T_ptr *tp = (T_ptr *)_buffer;
	T_ptr p = tp[_length];
	tp[_length] = NULL;
	return (p);
}

template<class T, class T_field, class T_ptr>
inline void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::load(uint_t max_entries,
    uint_t num_entries, T_ptr *e, bool release_flag)
{
	_load(max_entries, num_entries, (void *)e, release_flag);
}

#endif	/* _INTERFACESEQ_IN_H */
