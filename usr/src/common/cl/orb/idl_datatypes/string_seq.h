/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _STRING_SEQ_H
#define	_STRING_SEQ_H

#pragma ident	"@(#)string_seq.h	1.9	08/01/21 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//


#include <orb/idl_datatypes/sequence.h>
#ifndef CLCOMM_COMPAT
#include <orb/invo/argfuncs.h>
#include <orb/invo/ix.h>
#endif
#include <orb/idl_datatypes/idl_strings.h>

//	Forward declaration
class service;

//
// _string_seq - the unusual "_" in the front of the class name avoids
// conflict with names used in the IDL files.
//
// This class support the built-in "sequence<string>".
//
// We cast rather than allocate _string_field elements because
// some compilers try to call the new[] with a constructor
// even though the constructor is a nop.
//
class _string_seq : public GenericSequence {
public:
	_string_seq();
	_string_seq(uint_t);
	_string_seq(uint_t, uint_t);
	_string_seq(uint_t, uint_t, char **, bool);
	_string_seq(const _string_seq &);

	virtual			~_string_seq();

	_string_seq		&operator = (const _string_seq &);

	const _string_field	&operator[](uint_t i) const;

	_string_field		&operator[](uint_t i);

	void		load(uint_t, uint_t, char **, bool);

	static char	**allocbuf(uint_t);

	//
	// The following methods must take arguments that match
	// the arguments defined in argfuncs.h
	//
	static void	_put(service &, void *);
	static void	_get(service &, void *);
	static void	*_makef();
	static void	_freef(void *);
	static void	_releasef(void *);
#ifndef CLCOMM_COMPAT
	static MarshalInfo_complex	ArgMarshalFuncs;
#endif

private:
	// Called from base class
	virtual void	*alloc_buf(uint_t);
	virtual void	free_buf();
	virtual void	transfer_buf(void *dest);
	virtual void	*copy_buf() const;

	//
	// free - the buffer will no longer be used.
	// Releases the buffer when "_release" has been set true.
	// This method leaves the sequence state in an unsafe state.
	// Nothing has been changed to show that there is no
	// buffer or sequence elements.
	// This method is called by either the destructor
	// (the internal state is no longer needed) or
	// a sequence method that immediately loads new data and state.
	//
	virtual void	free();

	//
	// The current sequence length can change.
	// The maximum sequence length does not change.
	// Performs clean up of any discarded elements.
	//
	virtual void	modify_current_length(uint_t num_entries);
};

#include <orb/idl_datatypes/string_seq_in.h>

#endif	/* _STRING_SEQ_H */
