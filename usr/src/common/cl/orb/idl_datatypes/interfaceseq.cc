//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)interfaceseq.cc	1.14	08/05/20 SMI"

//
// Sequence of Interfaces of type T methods
//

#include <orb/idl_datatypes/interfaceseq.h>
#include <orb/invo/invocation.h>

template<class T, class T_field, class T_ptr>
_Ix_InterfaceSeq_<T, T_field, T_ptr>::~_Ix_InterfaceSeq_()
{
	free();
}

template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_put(service &serv) const
{
	uint_t i;
	T_ptr *tpbuf = (T_ptr *)_buffer;
	serv.put_seq_hdr(this);
	for (i = 0; i < _length; i++) {
		serv.put_object(tpbuf[i]);
	}
}

//
// A Note on error handling.
// If there an exception during get_object, get_object will
// unmarshal_cancel from the recstream everything corresponding to
// this object and returns nil. At end of the unmarshal process,
// unmarshal_roll_back will release the whole InterfaceSeq to release
// the successful get_objects and do nothing for the failed ones
// If there is a memory alloc failure for tmpbuf, we have to do
// a get_object_cancel as there is no place to store the objects
// to let unmarshal_roll_back handle this case.
//
template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_get(service &serv,
    MarshalInfo_object *infop)
{
	serv.get_seq_hdr(this);
	if (_maximum != 0) {
		T_ptr	*tmpbuf = allocbuf(_maximum);

		// We assume blocking allocations and do not handle failures
		Environment	*e = serv.get_env();

		for (uint_t i = 0; i < _length; i++) {
			tmpbuf[i] = (T_ptr)serv.get_object(infop);
		}
		_buffer = (void *)tmpbuf;
		_release = true;
	}
}

template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::_unput() const
{
	T_ptr *tpbuf = (T_ptr *)_buffer;
	for (uint_t i = 0; i < _length; i++) {
		service::unput_object(tpbuf[i]);
	}
}

//
// allocbuf - Create a buffer for the specified number of entries
// with each entry initialized to nil.
//
// static
template<class T, class T_field, class T_ptr>
T_ptr *
_Ix_InterfaceSeq_<T, T_field, T_ptr>::allocbuf(uint_t num_entries)
{
	T_ptr	*interface_buf = new T_ptr[num_entries];
	ASSERT(interface_buf != NULL);
	for (uint_t i = 0; i < num_entries; i++) {
		interface_buf[i] = T::_nil();
	}
	return (interface_buf);
}

// Called from base class
template<class T, class T_field, class T_ptr>
void *
_Ix_InterfaceSeq_<T, T_field, T_ptr>::alloc_buf(uint_t num_entries)
{
	return ((void *)allocbuf(num_entries));
}

template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::free_buf()
{
	delete [] ((T_ptr *)_buffer);
}

//
// transfer_buf - the existing buffer contents are being transferred
// to a new buffer. The old buffer will be discarded.
// There is no net change in object references.
//
template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::transfer_buf(void *dest)
{
	T_ptr	*new_dest = (T_ptr *)dest;
	T_ptr	*tpbuf = (T_ptr *)_buffer;
	uint_t	num_entries = _length;
	while (num_entries--) {
		new_dest[num_entries] = tpbuf[num_entries];
	}
}

template<class T, class T_field, class T_ptr>
void *
_Ix_InterfaceSeq_<T, T_field, T_ptr>::copy_buf() const
{
	T_ptr *_buf = allocbuf(_maximum);
	T_ptr *tpbuf = (T_ptr *)_buffer;
	uint_t i;
	for (i = 0; i < _length; i++) {
		_buf[i] = T::_duplicate(tpbuf[i]);
	}
	for (i = _length; i < _maximum; i++) {
		_buf[i] = T::_nil();
	}
	return ((void *)_buf);
}

//
// free - the buffer will no longer be used.
// This method releases the buffer when "_release" has been set true.
// This method leaves the sequence state in an unsafe state.
// None of the variables have been changed to show that there is no
// buffer or sequence elements.
// This method is called by the destructor
// (the internal state is no longer needed) or
// by a sequence method that immediately loads new data and state.
//
template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::free()
{
	T_ptr	*tpbuf = (T_ptr *)_buffer;
	for (uint_t i = 0; i < _length; i++) {
		CORBA::release(tpbuf[i]);
		tpbuf[i] = T::_nil();
	}
	if (_release) {
		free_buf();
	}
}

//
// modify_current_length -
// The current sequence length can change.
// The maximum sequence length does not change.
// Performs clean up of any discarded elements.
//
template<class T, class T_field, class T_ptr>
void
_Ix_InterfaceSeq_<T, T_field, T_ptr>::modify_current_length(uint_t num_entries)
{
	if (num_entries == _length) {
		// No change
		return;
	}
	if (num_entries < _length) {
		//
		// Clean up discarded elements
		//
		T_ptr	*tpbuf = (T_ptr *)_buffer;
		for (uint_t i = num_entries; i < _length; i++) {
			CORBA::release(tpbuf[i]);
			tpbuf[i] = T::_nil();
		}
	}
	// Any elements in the buffer not currently valid have a nil entry.
	// Thus it is safe to make valid existing buffer entries.
	_length = num_entries;
}
