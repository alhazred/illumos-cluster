/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _INTERFACESEQ_H
#define	_INTERFACESEQ_H

#pragma ident	"@(#)interfaceseq.h	1.12	08/05/20 SMI"

#include <orb/idl_datatypes/sequence.h>

class service;
struct MarshalInfo_object;

//
// A Sequence of Interfaces of type T.
//
template<class T, class T_field, class T_ptr> class _Ix_InterfaceSeq_ :
    public GenericSequence {
public:
	_Ix_InterfaceSeq_();
	_Ix_InterfaceSeq_(uint_t);
	_Ix_InterfaceSeq_(uint_t, uint_t);
	_Ix_InterfaceSeq_(uint_t, uint_t, T_ptr *, bool);
	_Ix_InterfaceSeq_(const _Ix_InterfaceSeq_ &);

	virtual			~_Ix_InterfaceSeq_();

	_Ix_InterfaceSeq_	&operator = (const _Ix_InterfaceSeq_ &);
	_Ix_InterfaceSeq_	*operator ->();
	const _Ix_InterfaceSeq_	*operator ->() const;
	const T_field		&operator[](uint_t subscript) const;
	T_field			&operator[](uint_t);

	T_ptr			reaplast();

	void			load(uint_t, uint_t, T_ptr *, bool);

	void			_put(service &) const;

	//
	// A Note on error handling.
	// If there an exception during get_object, get_object will
	// unmarshal_cancel from the recstream everything corresponding to
	// this object and returns nil. At end of the unmarshal process,
	// unmarshal_roll_back will release the whole InterfaceSeq to release
	// the successful get_objects and do nothing for the failed ones
	// If there is a memory alloc failure for tmpbuf, we have to do
	// a get_object_cancel as there is no place to store the objects
	// to let unmarshal_roll_back handle this case.
	//
	void			_get(service &, MarshalInfo_object *infop);

	void			_unput() const;

	//
	// XXX It may be better to do this as T_var to get the automatic
	// constructor/destructor
	//
	static T_ptr		*allocbuf(uint_t);

private:
	// Called from base class
	virtual void		*alloc_buf(uint_t);
	virtual void		free_buf();
	virtual void		transfer_buf(void *);
	virtual void		*copy_buf() const;

	//
	// free - the buffer will no longer be used.
	// This method releases the buffer when "_release" has been set true.
	// This method leaves the sequence state in an unsafe state.
	// None of the variables have been changed to show that there is no
	// buffer or sequence elements.
	// This method is called by the destructor
	// (the internal state is no longer needed) or
	// by a sequence method that immediately loads new data and state.
	//
	virtual void		free();

	//
	// The current sequence length can change.
	// The maximum sequence length does not change.
	// Performs clean up of any discarded elements.
	//
	virtual void		modify_current_length(uint_t num_entries);
};

#include <orb/idl_datatypes/interfaceseq_in.h>

#endif	/* _INTERFACESEQ_H */
