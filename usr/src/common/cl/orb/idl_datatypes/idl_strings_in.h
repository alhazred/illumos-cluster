/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  idl_strings_in.h
 *
 */

#ifndef _IDL_STRINGS_IN_H
#define	_IDL_STRINGS_IN_H

#pragma ident	"@(#)idl_strings_in.h	1.6	06/05/24 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

//
// class _string_field inline methods
//

inline
_string_field::_string_field() : _str(NULL)
{
}

inline
_string_field::_string_field(char *charp) : _str(charp)
{
}

inline
_string_field::operator char *()
{
	return (_str);
}

inline
_string_field::operator const char *() const
{
	return (_str);
}

inline bool
_string_field::operator == (const char *charp) const
{
	return (_str == charp);
}

inline bool
_string_field::operator != (const char *charp) const
{
	return (_str != charp);
}

inline const char &
_string_field::operator [](uint_t index) const
{
	return (_str[index]);
}

inline char &
_string_field::operator [](uint_t index)
{
	return (_str[index]);
}

//
// class _string_var inline methods
//

inline
_string_var::_string_var() : _str(NULL)
{
}

inline
_string_var::_string_var(char *charp) : _str(charp)
{
}

inline
_string_var::~_string_var()
{
	delete [] _str;
}

inline
_string_var::operator char *()
{
	return (_str);
}

//
// This is used to pass _string_var as an "in" parameter
//

inline
_string_var::operator const char *() const
{
	return (_str);
}

//
// out - This is used to pass _string_var as an "out" parameter
//
inline char *&
_string_var::out()
{
	delete [] _str;
	_str = NULL;
	return (_str);
}

//
// INOUT - This is used to pass _string_var as an "inout" parameter
//
inline char *&
_string_var::INOUT()
{
	return (_str);
}

//
// class _string_out inline method
//

inline
_string_out::_string_out(char *&p)
{
	_str = &p;
}

inline
_string_out::_string_out(_string_var &sv)
{
	_str = &(sv.out());
}

inline
_string_out::_string_out(const _string_out &p)
{
	_str = p._str;
}

inline
_string_out::~_string_out()
{
}

inline _string_out &
_string_out::operator = (char *charp)
{
	*_str = charp;
	return (*this);
}

inline
_string_out::operator const char *() const
{
	return (*_str);
}

inline String_ptr *
_string_out::operator & ()
{
	return (_str);
}

#endif	/* _IDL_STRINGS_IN_H */
