/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
/*
 *  managed_seq_in.h
 *
 */

#ifndef _MANAGED_SEQ_IN_H
#define	_MANAGED_SEQ_IN_H

#pragma ident	"@(#)managed_seq_in.h	1.5	08/05/20 SMI"

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::_ManagedSeq_()
{
}

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::_ManagedSeq_(SEQUENCE_TYPE *mseqp) :
	_T_var_<SEQUENCE_TYPE, SEQUENCE_TYPE*>(mseqp)
{
}

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::_ManagedSeq_(const
    _ManagedSeq_ &manseq) :
	_T_var_<SEQUENCE_TYPE, SEQUENCE_TYPE *>(manseq)
{
}

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::~_ManagedSeq_()
{
}

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline uint_t
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::maximum() const
{
	//
	// The use of the "this" pointer eliminates
	// possible ambiguity in template usage.
	//
	return (this->_ptr->maximum());
}

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline uint_t
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::length() const
{
	//
	// The use of the "this" pointer eliminates
	// possible ambiguity in template usage.
	//
	return (this->_ptr->length());
}

template<class SEQUENCE_TYPE, class ELEMENT_TYPE>
inline ELEMENT_TYPE &
_ManagedSeq_<SEQUENCE_TYPE, ELEMENT_TYPE>::operator[](uint_t subscript)
{
	//
	// The use of the "this" pointer eliminates
	// possible ambiguity in template usage.
	//
	return (this->_ptr->operator[](subscript));
}

#endif	/* _MANAGED_SEQ_IN_H */
