/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SEQUENCE_H
#define	_SEQUENCE_H

#pragma ident	"@(#)sequence.h	1.16	06/05/24 SMI"

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//


#include <sys/types.h>
#include <sys/os.h>

//
// GenericSequence is the base class for sequence related classes
//
class GenericSequence {
public:
	// Accessors for protected fields
	uint_t	maximum() const;
	uint_t	length() const;
	bool	release() const;
	void	*buf() const;

	//
	// length - sets the current length to the specified number of entries.
	// If the specified number of entries exceeds the prior
	// maximum sequence length; the method allocates a new buffer, transfers
	// the existing contents, and sets the maximum length to the new
	// current length.
	//
	void	length(uint_t entries);

	// Initializers

	// _load - frees previous contents and then loads new data
	void	_load(uint_t max_entries, uint_t num_entries, void *bufp,
		    bool release_flag);

	// _init - overwrites previous contents with new data
	void	_init(uint_t max_entries, uint_t num_entries, void *bufp,
		    bool release_flag);

	//
	// All sequence child classes are marshalled with the
	// GenericSequence fields. These fields must be included
	// in the marshal data size.
	//
	uint_t	_marshal_size();

protected:
	virtual		~GenericSequence();

	//
	// The abstract class GenericSequence does not implement buffer support,
	// but does identify the necessary methods.
	//

	//
	// Allocate a buffer sized correctly for
	// the specified number of entries.
	//
	virtual void	*alloc_buf(uint_t entries) = 0;

	//
	// Deallocate the buffer.
	//
	virtual void	free_buf() = 0;

	//
	// Transfer the contents of this sequence
	// to the buffer specified in "dest".
	// This method must safely handle an existing sequence
	// with zero entries.
	//
	virtual void	transfer_buf(void *dest) = 0;

	//
	// Returns a pointer to a newly created buffer that contains a
	// duplicated copy of the valid data in this sequence.
	//
	virtual void	*copy_buf() const = 0;

	//
	// free - the buffer will no longer be used.
	// This method releases the buffer when "_release" has been set true.
	// This method leaves the sequence state in an unsafe state.
	// None of the variables have been changed to show that there is no
	// buffer or sequence elements.
	// This method is called by the destructor
	// (the internal state is no longer needed) or
	// by a sequence method that immediately loads new data and state.
	//
	virtual void	free() = 0;

	//
	// The current sequence length can change.
	// The maximum sequence length does not change.
	// Those sequence types requiring clean up of sequence elements,
	// such as sequences of strings or object references, must
	// perform clean up of any discarded elements.
	//
	virtual void	modify_current_length(uint_t num_entries) = 0;

	//
	// copy - makes this sequence a copy of an existing sequence.
	// This method is called either in the constructor or
	// after a "free()" of the buffer.
	// Hence, need not check value of _release and/or free existing buffer.
	//
	void		copy(const GenericSequence &);

	uint_t		_maximum;	// Number of allocated entries
	uint_t		_length;	// Number of actual valid entries

	//
	// All buffer entries (not just the current length)
	// must be initialized appropriately for the sequence type,
	// such as NULL's for a sequence of strings
	// and nil for a sequence of object references.
	//
	void		*_buffer;

	bool		_release;	// Whether _buffer needs to be freed
};

//
// Declare a C++ class named _NormalSeq_ which is a sequence of type T,
// where T is variable-length and not an interface or string type.
//
template<class T> class _NormalSeq_ : public GenericSequence {
public:
	_NormalSeq_();
	_NormalSeq_(uint_t max_entries);
	_NormalSeq_(uint_t max_entries, uint_t num_entries);

	_NormalSeq_(uint_t max_entries, uint_t num_entries, T *seqbufp,
	    bool release_flag);

	_NormalSeq_(const _NormalSeq_ &seq);

	virtual			~_NormalSeq_();

	_NormalSeq_		&operator = (const _NormalSeq_ &seq);
	_NormalSeq_		*operator ->();
	const _NormalSeq_	*operator ->() const;
	const T			&operator[](uint_t index) const;
	T			&operator[](uint_t index);

	void		load(uint_t max_entries, uint_t num_entries,
			    T *seqbufp, bool release_flag);

	static T	*allocbuf(uint_t num_entries);

private:
	// Called from base class
	virtual void	*alloc_buf(uint_t);

	//
	// Deallocate the buffer.
	//
	virtual void	free_buf();

	virtual void	transfer_buf(void *dest);

	virtual void	*copy_buf() const;

	//
	// free - the buffer will no longer be used.
	// This method releases the buffer when "_release" has been set true.
	// This method leaves the sequence state in an unsafe state.
	// None of the variables have been changed to show that there is no
	// buffer or sequence elements.
	// This method is called by the destructor
	// (the internal state is no longer needed) or
	// by a sequence method that immediately loads new data and state.
	//
	virtual void	free();

	//
	// The current sequence length can change.
	// The maximum sequence length does not change.
	// Performs clean up of any discarded elements.
	//
	virtual void		modify_current_length(uint_t num_entries);
};

//
// Declare a C++ class for a sequence of T where T is fixed length.
//
template<class T> class _FixedSeq_ : public GenericSequence {
public:
	_FixedSeq_();
	_FixedSeq_(uint_t max_entries);
	_FixedSeq_(uint_t max_entries, uint_t num_entries);

	_FixedSeq_(uint_t max_entries, uint_t num_entries, T *seqbufp,
	    bool release_flag);

	_FixedSeq_(const _FixedSeq_ &seq);

	virtual			~_FixedSeq_();

	_FixedSeq_		&operator = (const _FixedSeq_ &seq);
	_FixedSeq_		*operator ->();
	const _FixedSeq_	*operator ->() const;
	const T			&operator[](uint_t index) const;
	T			&operator[](uint_t index);

	T		*buffer() const;

	void		load(uint_t max_entries, uint_t num_entries,
			    T *seqbufp, bool release_flag);

	static T	*allocbuf(uint_t num_entries);

private:
	// Called from base class
	virtual void	*alloc_buf(uint_t num_entries);

	//
	// Deallocate the buffer.
	//
	virtual void	free_buf();

	virtual void	transfer_buf(void *dest);

	virtual void	*copy_buf() const;

	//
	// free - the buffer will no longer be used.
	// This method releases the buffer when "_release" has been set true.
	// This method leaves the sequence state in an unsafe state.
	// None of the variables have been changed to show that there is no
	// buffer or sequence elements.
	// This method is called by the destructor
	// (the internal state is no longer needed) or
	// by a sequence method that immediately loads new data and state.
	//
	virtual void	free();

	//
	// The current sequence length can change.
	// The maximum sequence length does not change.
	// Performs clean up of any discarded elements.
	//
	virtual void		modify_current_length(uint_t num_entries);
};



#include <orb/idl_datatypes/sequence_in.h>

#endif	/* _SEQUENCE_H */
