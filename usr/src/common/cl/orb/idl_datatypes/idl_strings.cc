//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)idl_strings.cc	1.6	06/05/24 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

#include <orb/idl_datatypes/idl_strings.h>
#include <sys/os.h>

//
// String_asg - convenience function for string assignment
//
static inline void
String_asg(char *&string1, const char *string2)
{
	if (string1 != string2) {
		delete [] string1;
		string1 = os::strdup(string2);
	}
}

//
// class _string_field methods
//

_string_field::_string_field(const char *charp)
{
	_str = os::strdup(charp);
}

_string_field::_string_field(const _string_field &str_field)
{
	_str = os::strdup((const char *)str_field);
}

//
// lint complains that assignment does not check for assignment to this
// We already check for equality of _str and contents of s in String_asg
// so this is not necessary
//
//lint -e1529
_string_field &
_string_field::operator = (const _string_field &str_field)
{
	String_asg(_str, (const char *)str_field);
	return (*this);
}
//lint +e1529

//
// There is a difference between assigning char * vs a const char * to
// a _string_field. Assigning const char * does an strdup (so the caller
// is still responsible for freeing the memory (if needed).
// Assiging a char * does not do an strdup and the callee is responsible
// for freeing the memory (eventually).
//
_string_field &
_string_field::operator = (char *charp)
{
	if (_str != charp) {
		delete [] _str;
		_str = charp;
	}
	return (*this);
}

_string_field &
_string_field::operator = (const char *charp)
{
	String_asg(_str, charp);
	return (*this);
}

//
// class _string_var methods
//

_string_var::_string_var(const _string_var &string_v)
{
	_str = os::strdup((const char *)string_v);
}

_string_var::_string_var(const _string_field &string_v)
{
	_str = os::strdup((const char *)string_v);
}

//
// There is a difference between assigning char * vs a const char * to
// a _string_var. Assigning const char * does an strdup (so the caller
// is still responsible for freeing the memory (if needed).
// Assiging a char * does not do an strdup and the callee is responsible
// for freeing the memory (eventually).
//
_string_var &
_string_var::operator = (char *charp)
{
	if (_str != charp) {
		delete [] _str;
		_str = charp;
	}
	return (*this);
}

_string_var &
_string_var::operator = (const char *charp)
{
	String_asg(_str, charp);
	return (*this);
}

//
// lint complains that assignment does not check for assignment to this
// We already check for equality of _str and contents of s in String_asg
// so this is not necessary
//
//lint -e1529
_string_var &
_string_var::operator = (const _string_var &string_v)
{
	String_asg(_str, (const char *)string_v);
	return (*this);
}
//lint -e1529

//
// class _string_out methods
//

//
// lint complains that assignment does not check for assignment to this
// We already check for equality of _str and contents of s in String_asg
// so this is not necessary
// lint also complains that _str is not modified in this assignment.
// It expects that assignment operators modify some member. _string_out however
// has weird semantics and it seems correct that just the pointer pointed to
// by _str is modified by this assignment.
// Long term we would like to remove _out support (4180360)
//
//lint -e1529 -e1539
_string_out &
_string_out::operator = (const _string_out &str_out)
{
	*_str = os::strdup((const char *)str_out);
	return (*this);
}
//lint +e1529 +e1539
