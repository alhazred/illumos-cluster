//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)string_seq.cc	1.6	06/05/31 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

#include <orb/idl_datatypes/string_seq.h>
#ifndef CLCOMM_COMPAT
#include <orb/invo/invocation.h>
#endif

#ifndef CLCOMM_COMPAT
//
// provide a structure containing all of the information needed to
// support marshal time operations on strings.
//
MarshalInfo_complex	_string_seq::ArgMarshalFuncs = {
	&_string_seq::_put,
	&_string_seq::_get,
	&_string_seq::_makef,
	&_string_seq::_freef,
	&_string_seq::_releasef,
	NULL,				// no unput method
	NULL				// no marshal size method
};
#endif

//
// class _string_seq
//

_string_seq::~_string_seq()
{
	_string_seq::free();
}

// static
char **
_string_seq::allocbuf(uint_t num_entries)
{
	char	**tmpbuf = new char *[num_entries];
	ASSERT(tmpbuf != NULL);
	for (uint_t i = 0; i < num_entries; i++) {
		tmpbuf[i] = NULL;
	}
	return (tmpbuf);
}

void *
_string_seq::alloc_buf(uint_t num_entries)
{
	return ((void *)allocbuf(num_entries));
}

void
_string_seq::free_buf()
{
	delete [] ((char **)_buffer);
}

//
// transfer_buf - transfer the contents of this sequence to a new buffer
//
void
_string_seq::transfer_buf(void *dest)
{
	char		**newdest = (char **)dest;
	char		**oldbuf = (char **)_buffer;
	uint_t		num_entries = _length;
	while (num_entries--) {
		newdest[num_entries] = oldbuf[num_entries];
	}
}

void *
_string_seq::copy_buf() const
{
	uint_t	i;
	char	**tmpbuf = new char *[_maximum];
	char	**oldbuf = (char **)_buffer;
	for (i = 0; i < _length; i++) {
		tmpbuf[i] = os::strdup((const char *)(oldbuf[i]));
	}
	for (i = _length; i < _maximum; i++) {
		tmpbuf[i] = NULL;
	}
	return ((void *)tmpbuf);
}

//
// free - the buffer will no longer be used.
// This method releases the buffer when "_release" has been set true.
// This method leaves the sequence state in an unsafe state.
// None of the variables have been changed to show that there is no
// buffer or sequence elements.
// This method is called by the destructor
// (the internal state is no longer needed) or
// by a sequence method that immediately loads new data and state.
//
void
_string_seq::free()
{
	char	**tmpbuf = (char **)_buffer;
	for (uint_t i = 0; i < _length; i++) {
		delete [] tmpbuf[i];
	}
	if (_release) {
		free_buf();
	}
}

//
// modify_current_length -
// The current sequence length can change.
// The maximum sequence length does not change.
// Performs clean up of any discarded elements.
//
void
_string_seq::modify_current_length(uint_t num_entries)
{
	if (num_entries == _length) {
		// No change
		return;
	}
	if (num_entries < _length) {
		//
		// Clean up discarded elements
		//
		char	**tmpbuf = (char **)_buffer;
		for (uint_t i = num_entries; i < _length; i++) {
			delete [] tmpbuf[i];
			tmpbuf[i] = NULL;
		}
	}
	// Any elements in the buffer not currently valid have a NULL entry.
	// Thus it is safe to make valid existing buffer entries.
	_length = num_entries;
}

//
// _string_seq marshal time support functions
//

#ifndef CLCOMM_COMPAT
//
// _put - marshal a string sequence
//
// static
void
_string_seq::_put(service &serv, void *voidp)
{
	_string_seq	*stringp = (_string_seq *)voidp;
	serv.put_seq_hdr(stringp);
	char	**stringlist = (char **)stringp->_buffer;
	for (uint_t i = 0; i < stringp->length(); i++) {
		serv.put_string(stringlist[i]);
	}
}

//
// _get - unmarshal a string sequence
//
// static
void
_string_seq::_get(service &serv, void *voidp)
{
	_string_seq	*stringp = (_string_seq *)voidp;

	serv.get_seq_hdr(stringp);
	ASSERT(!stringp->_release);
	ASSERT(stringp->_buffer == NULL);

	if (stringp->maximum() != 0) {
		uint_t	len = stringp->length();
		char	**stringlist = new char *[stringp->maximum()];
		ASSERT(stringlist != NULL);
		for (uint_t i = 0; i < len; i++) {
			stringlist[i] = serv.get_string();
		}
		stringp->_buffer = (void *)stringlist;
		stringp->_release = true;
	}
}
#endif // CLCOMM_COMPAT

// static
void *
_string_seq::_makef()
{
	return (new _string_seq);
}

// static
void
_string_seq::_freef(void *voidp)
{
	_string_seq	*stringp = (_string_seq *)voidp;
	delete stringp;
}

// static
void
_string_seq::_releasef(void *voidp)
{
	_string_seq	*stringp = (_string_seq *)voidp;
	stringp->load(0, 0, NULL, false);
}
