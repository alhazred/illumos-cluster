/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  string_seq_in.h
 *
 */

#ifndef _STRING_SEQ_IN_H
#define	_STRING_SEQ_IN_H

#pragma ident	"@(#)string_seq_in.h	1.5	08/05/20 SMI"

//
// _string_seq inline methods
//
inline
_string_seq::_string_seq()
{
	_init(0, 0, (void *)NULL, false);
}

inline
_string_seq::_string_seq(uint_t max_entries)
{
	_init(max_entries, 0, alloc_buf(max_entries), true);
}

inline
_string_seq::_string_seq(uint_t max_entries, uint_t num_entries)
{
	_init(max_entries, num_entries, alloc_buf(max_entries), true);
}

inline
_string_seq::_string_seq(uint_t max_entries, uint_t num_entries, char **bufp,
    bool release_flag)
{
	_init(max_entries, num_entries, (void *)bufp, release_flag);
}

inline
_string_seq::_string_seq(const _string_seq &seq)
{
	copy(seq);
}

inline _string_seq &
_string_seq::operator = (const _string_seq &seq)
{
	free();
	copy(seq);
	return (*this);
}

inline const _string_field &
_string_seq::operator[](uint_t i) const
{
	return (((_string_field *)_buffer)[i]);
}

inline _string_field &
_string_seq::operator[](uint_t i)
{
	return (((_string_field *)_buffer)[i]);
}

inline void
_string_seq::load(uint_t max_entries, uint_t num_entries, char **charbuf,
    bool release_flag)
{
	_load(max_entries, num_entries, (void *)charbuf, release_flag);
}

#endif	/* _STRING_SEQ_IN_H */
