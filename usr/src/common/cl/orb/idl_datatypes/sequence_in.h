/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  sequence_in.h
 *
 */

#ifndef _SEQUENCE_IN_H
#define	_SEQUENCE_IN_H

#pragma ident	"@(#)sequence_in.h	1.18	06/05/24 SMI"

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//

//
// GenericSequence inline methods
//

//
// Accessors for protected fields
//

inline uint_t
GenericSequence::maximum() const
{
	return (_maximum);
}

inline uint_t
GenericSequence::length() const
{
	return (_length);
}

inline bool
GenericSequence::release() const
{
	return (_release);
};

inline void *
GenericSequence::buf() const
{
	return (_buffer);
}

//
// Initializers
//

//
// _load - frees previous contents
//
inline void
GenericSequence::_load(uint_t max_entries, uint_t num_entries, void *bufp,
    bool release_flag)
{
	free();
	_init(max_entries, num_entries, bufp, release_flag);
}

//
// _init - overwrites previous contents
//
inline void
GenericSequence::_init(uint_t max_entries, uint_t num_entries, void *bufp,
    bool release_flag)
{
	_maximum = max_entries;
	_length = num_entries;
	_buffer = bufp;
	_release = release_flag;
}

//
// All sequence child classes are marshalled with GenericSequence fields.
// These fields must be included in the marshal size.
//
inline uint_t
GenericSequence::_marshal_size()
{
#ifdef MARSHAL_DEBUG
	return ((uint_t)(sizeof (uint_t) +	// M_SEQ or M_SEQ_HDR
	    2 * sizeof (uint_t) +		// _maximum
	    2 * sizeof (uint_t)));		// _length
#else
	return ((uint_t)(sizeof (uint_t) +	// _maximum
	    sizeof (uint_t)));			// _length
#endif
}

//
// _NormalSeq_ inline methods
//

template<class T>
inline
_NormalSeq_<T>::_NormalSeq_()
{
	_init(0, 0, (void *)NULL, false);
}

template<class T>
inline
_NormalSeq_<T>::_NormalSeq_(uint_t max_entries)
{
	_init(max_entries, 0, alloc_buf(max_entries), true);
}

template<class T>
inline
_NormalSeq_<T>::_NormalSeq_(uint_t max_entries, uint_t num_entries)
{
	_init(max_entries, num_entries, alloc_buf(max_entries), true);
}

template<class T>
inline
_NormalSeq_<T>::_NormalSeq_(uint_t max_entries, uint_t num_entries, T *seqbufp,
    bool release_flag)
{
	_init(max_entries, num_entries, (void *)seqbufp, release_flag);
}

template<class T>
inline
_NormalSeq_<T>::_NormalSeq_(const _NormalSeq_ &seq)
{
	copy(seq);
}

template<class T>
inline _NormalSeq_<T> &
_NormalSeq_<T>::operator = (const _NormalSeq_ &seq)
{
	free();
	copy(seq);
	return (*this);
}

template<class T>
inline _NormalSeq_<T> *
_NormalSeq_<T>::operator ->()
{
	return (this);
}

template<class T>
inline const _NormalSeq_<T> *
_NormalSeq_<T>::operator ->() const
{
	return (this);
}

template<class T>
inline const T &
_NormalSeq_<T>::operator[](uint_t index) const
{
	return (((T *)_buffer)[index]);
}

template<class T>
inline T &
_NormalSeq_<T>::operator[](uint_t index)
{
	return (((T *)_buffer)[index]);
}

template<class T>
inline void
_NormalSeq_<T>::load(uint_t max_entries, uint_t num_entries, T *seqbufp,
    bool release_flag)
{
	_load(max_entries, num_entries, (void *)seqbufp, release_flag);
}

// static
template<class T>
inline T *
_NormalSeq_<T>::allocbuf(uint_t num_entries)
{
	return (new T[num_entries]);
}

//
// _FixedSeq_ inline methods
//

template<class T>
inline
_FixedSeq_<T>::_FixedSeq_()
{
	_init(0, 0, (void *)0, false);
}

template<class T>
inline
_FixedSeq_<T>::_FixedSeq_(uint_t max_entries)
{
	_init(max_entries, 0, alloc_buf(max_entries), true);
}

template<class T>
inline
_FixedSeq_<T>::_FixedSeq_(uint_t max_entries, uint_t num_entries)
{
	_init(max_entries, num_entries, alloc_buf(max_entries), true);
}

template<class T>
inline
_FixedSeq_<T>::_FixedSeq_(uint_t max_entries, uint_t num_entries, T *seqbufp,
    bool release_flag)
{
	_init(max_entries, num_entries, (void *)seqbufp, release_flag);
}

template<class T>
inline
_FixedSeq_<T>::_FixedSeq_(const _FixedSeq_ &seq)
{
	copy(seq);
}

template<class T>
inline _FixedSeq_<T> &
_FixedSeq_<T>::operator = (const _FixedSeq_ &seq)
{
	free();
	copy(seq);
	return (*this);
}

template<class T>
inline _FixedSeq_<T> *
_FixedSeq_<T>::operator ->()
{
	return (this);
}

template<class T>
inline const _FixedSeq_<T> *
_FixedSeq_<T>::operator ->() const
{
	return (this);
}

template<class T>
inline const T &
_FixedSeq_<T>::operator[](uint_t index) const
{
	return (((T *)_buffer)[index]);
}

template<class T>
inline T &
_FixedSeq_<T>::operator[](uint_t index)
{
	return (((T *)_buffer)[index]);
}

template<class T>
inline T *
_FixedSeq_<T>::buffer() const
{
	return ((T *)_buffer);
}

template<class T>
inline void
_FixedSeq_<T>::load(uint_t max_entries, uint_t num_entries, T *seqbufp,
    bool release_flag)
{
	_load(max_entries, num_entries, (void *)seqbufp, release_flag);
}

// static
template<class T>
inline T *
_FixedSeq_<T>::allocbuf(uint_t num_entries)
{
	return (new T[num_entries]);
}

//
// class _NormalSeq_ methods
//

template<class T>
_NormalSeq_<T>::~_NormalSeq_()
{
	free();
}

template<class T>
void *
_NormalSeq_<T>::alloc_buf(uint_t num_entries)
{
	return ((void *)allocbuf(num_entries));
}

template<class T>
void
_NormalSeq_<T>::free_buf()
{
	delete [] ((T *)_buffer);
}

template<class T>
void
_NormalSeq_<T>::transfer_buf(void *dest)
{
	T	*newdest = (T *)dest;
	uint_t	num_entries = _length;
	while (num_entries--) {
		newdest[num_entries] = ((T *)_buffer)[num_entries];
	}
}

template<class T>
void *
_NormalSeq_<T>::copy_buf() const
{
	T *new_buffer = allocbuf(_maximum);
	for (uint_t i = 0; i < _length; i++) {
		new_buffer[i] = ((T *)_buffer)[i];
	}
	return ((void *)new_buffer);
}

//
// free - the buffer will no longer be used.
// This method releases the buffer when "_release" has been set true.
// This method leaves the sequence state in an unsafe state.
// None of the variables have been changed to show that there is no
// buffer or sequence elements.
// This method is called by the destructor
// (the internal state is no longer needed) or
// by a sequence method that immediately loads new data and state.
//
template<class T>
void
_NormalSeq_<T>::free()
{
	if (_release) {
		free_buf();
	}
}

//
// modify_current_length -
// The current sequence length can change.
// The maximum sequence length does not change.
//
template<class T>
void
_NormalSeq_<T>::modify_current_length(uint_t num_entries)
{
	if (num_entries == _length) {
		// No change
		return;
	}
	if (num_entries < _length) {
		//
		// Clean up discarded elements
		//
		// Complex data types can contain
		// object references, strings, and sequences.
		// Clean up is required for these complex data types.
		//
		// The empty constructor will always:
		//	initialize the object references to nil
		//	initialize strings to NULL
		//	initialize sequences to zero max length
		//
		// The empty constructor is safe for other data types.
		//
		// Assignment of dummy to the sequence entry will
		//	release any object references
		//	free memory held by strings and sequences
		//
		// Note that "dummy" is not entered into the sequence.
		// Instead the values of the fields of dummy are assigned
		// each discarded sequence entry.
		//
		T	dummy;
		T	*elementp = (T *)_buffer;
		for (uint_t i = num_entries; i < _length; i++) {
			elementp[i] = dummy;
		}
	}
	// Any elements in the buffer not currently valid have
	//	any object references are nil
	//	any strings are NULL
	//	any sequences are zero length
	// Thus it is safe to make valid existing buffer entries.
	_length = num_entries;
}

//
// class _FixedSeq_ methods
//

template<class T>
_FixedSeq_<T>::~_FixedSeq_()
{
	free();
}

template<class T>
void *
_FixedSeq_<T>::alloc_buf(uint_t num_entries)
{
	return ((void *)allocbuf(num_entries));
}

template<class T>
void
_FixedSeq_<T>::free_buf()
{
	delete [] ((T *)_buffer);
}

template<class T>
void
_FixedSeq_<T>::transfer_buf(void *dest)
{
	bcopy(_buffer, dest, sizeof (T) * _length);
}

template<class T>
void *
_FixedSeq_<T>::copy_buf() const
{
	T *new_buffer = allocbuf(_maximum);
	bcopy(_buffer, new_buffer, sizeof (T) * _length);
	return ((void *)new_buffer);
}

//
// free - the buffer will no longer be used.
// This method releases the buffer when "_release" has been set true.
// This method leaves the sequence state in an unsafe state.
// None of the variables have been changed to show that there is no
// buffer or sequence elements.
// This method is called by the destructor
// (the internal state is no longer needed) or
// by a sequence method that immediately loads new data and state.
//
template<class T>
void
_FixedSeq_<T>::free()
{
	if (_release) {
		free_buf();
	}
}

//
// modify_current_length -
// The current sequence length can change.
// The maximum sequence length does not change.
//
template<class T>
void
_FixedSeq_<T>::modify_current_length(uint_t num_entries)
{
	// These kinds of sequences do not contain:
	//	object references,
	//	strings, or
	//	sequences
	// Thus it is safe to make valid existing buffer entries.
	_length = num_entries;
}
#endif	/* _SEQUENCE_IN_H */
