//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb_trace.cc 1.17    08/05/20 SMI"

#include <orb/debug/orb_trace.h>

#ifdef	ORB_DBGBUF
//
// the_orb_trace - provides a central place to collect orb trace info
//
uint32_t orb_dbg_size = 200000;
dbg_print_buf	the_orb_trace(orb_dbg_size);
#endif


#ifdef DRC_DEBUG

//
// The default options
// Some options, such as the sequence number stuff,
// overwrite the debug buffer very quickly. So those options
// should only be turned on when needed.
//
uint_t		orb_trace_options = ORB_TRACE_RECONFIG | ORB_TRACE_FLOW_BLOCK |
		    ORB_TRACE_FLOW_ALLOC;

#endif	// DRC_DEBUG
