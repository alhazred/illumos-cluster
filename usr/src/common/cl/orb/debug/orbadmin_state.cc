//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.

#pragma ident	"@(#)orbadmin_state.cc	1.5	08/05/20 SMI"

#include <orb/debug/orbadmin_state.h>
#include <orb/flow/resource_pool.h>
#include <orb/flow/resource_balancer.h>
#include <orb/refs/unref_threadpool.h>
#include <orb/refs/refcount.h>

//
// cl_read_orb_state_pool - supports a cladmin command to
// read the client resource_pool state for one specified server node and pool.
//
extern "C" int
cl_read_orb_state_pool(void *argp)
{
	return (resource_pool::read_state(argp));
}

//
// cl_read_orb_state_balancer - supports a cladmin command to
// read the resource_balancer state for one specified pool.
//
extern "C" int
cl_read_orb_state_balancer(void *argp)
{
	return (resource_balancer::the().read_state(argp));
}

//
// cl_read_orb_state_unref - supports a cladmin command to
// read the unref_threadpool state.
//
extern "C" int
cl_read_orb_state_unref(void *argp)
{
	return (unref_threadpool::the().read_state(argp));
}

//
// cl_read_orb_state_refcount - supports a cladmin command to
// read the reference count state for one specified server node.
//
extern "C" int
cl_read_orb_state_refcount(void *argp)
{
	return (refcount::the().read_state(argp));
}
