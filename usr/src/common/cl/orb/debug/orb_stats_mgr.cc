//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb_stats_mgr.cc	1.10	08/05/20 SMI"

#include <orb/debug/orb_stats_mgr.h>
#include <orb/member/members.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/systm.h>
#endif

orb_stats_mgr	*orb_stats_mgr::the_orb_stats_mgr = NULL;

int
orb_stats_mgr::initialize()
{
	if (NULL == the_orb_stats_mgr) {
		the_orb_stats_mgr = new orb_stats_mgr;
		if (the_orb_stats_mgr == NULL) {
			return (ENOMEM);
		}
	}
	return (0);
}

void
orb_stats_mgr::shutdown()
{
	if (the_orb_stats_mgr) {
		delete the_orb_stats_mgr;
		the_orb_stats_mgr = NULL;
	}
}

orb_stats_mgr::orb_stats_mgr()
{
	for (sol::nodeid_t node = 0; node < NODEID_MAX + 1; node++) {
		reset(node);
	}
}

//
// reset - reinitialize the statistics for a specific node,
// which means that all the counters are reset to zero.
//
void
orb_stats_mgr::reset(sol::nodeid_t node)
{
	msg_stat[node].init();
	flow_stat[node].init();
}

//
// get_msg_stats - obtain the message statistics for one specific node
//
msg_stats
orb_stats_mgr::get_msg_stats(sol::nodeid_t node)
{
	return (msg_stat[node]);
}

//
// get_flow_stats - obtain the flow control statistics for one specific node
//
flow_stats
orb_stats_mgr::get_flow_stats(sol::nodeid_t node)
{
	return (flow_stat[node]);
}

#if defined(_KERNEL_ORB)

//
// cl_read_orb_stats_msg - supports a cladmin command to
// read the message statistics for one node.
//
extern "C" int
cl_read_orb_stats_msg(void *argp)
{
	return (orb_stats_mgr::read_orb_stats_msg(argp));
}

//
// read_orb_stats_msg - supports a cladmin command to
// read the message statistics for one node.
//
// static
int
orb_stats_mgr::read_orb_stats_msg(void *argp)
{
	cl_msg_stats	msg_statistics;

	if (copyin(argp, &msg_statistics, sizeof (cl_msg_stats)) != 0) {
		return (EFAULT);
	}
	// Check for valid node id
	if (msg_statistics.nodeid == NODEID_UNKNOWN ||
	    msg_statistics.nodeid > NODEID_MAX) {
		return (EINVAL);
	}
	if (!members::the().alive(msg_statistics.nodeid)) {
		// Do not return statistics for node not in the cluster
		msg_statistics.nodeid = NODEID_UNKNOWN;
		msg_statistics.stats.init();
	} else {
		// Return statistics for node in the cluster
		msg_statistics.stats = orb_stats_mgr::the().
		    msg_stat[msg_statistics.nodeid];
	}
	return (copyout(&msg_statistics, argp, sizeof (cl_msg_stats)));
}

//
// cl_read_orb_stats_flow - supports a cladmin command to
// read the orb flow control statistics for one node.
//
extern "C" int
cl_read_orb_stats_flow(void *argp)
{
	return (orb_stats_mgr::read_orb_stats_flow(argp));
}

//
// read_orb_stats_flow - supports a cladmin command to
// read the orb flow control statistics for one node.
//
// static
int
orb_stats_mgr::read_orb_stats_flow(void *argp)
{
	cl_flow_stats	flow_statistics;

	if (copyin(argp, &flow_statistics, sizeof (cl_flow_stats)) != 0) {
		return (EFAULT);
	}
	// Check for valid node id
	if (flow_statistics.nodeid == NODEID_UNKNOWN ||
	    flow_statistics.nodeid > NODEID_MAX) {
		return (EINVAL);
	}
	if (!members::the().alive(flow_statistics.nodeid)) {
		// Do not return statistics for node not in the cluster
		flow_statistics.nodeid = NODEID_UNKNOWN;
		flow_statistics.stats.init();
	} else {
		// Return statistics for node in the cluster
		flow_statistics.stats = orb_stats_mgr::the().
		    flow_stat[flow_statistics.nodeid];
	}
	return (copyout(&flow_statistics, argp, sizeof (cl_flow_stats)));
}

#endif	// _KERNEL_ORB
