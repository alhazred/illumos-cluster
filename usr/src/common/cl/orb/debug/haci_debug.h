/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HACI_DEBUG_H
#define	_HACI_DEBUG_H

#pragma ident	"@(#)haci_debug.h	1.12	08/05/20 SMI"

/* We define all haci dbg_printf related stuff here. */

#include <sys/dbg_printf.h>

#ifdef  __cplusplus
extern "C" {
#endif

#ifndef NO_DBGBUFS

#define	HACI_DBGBUFS

/* Options for enabling various debug traces. */

/*
 * This option records various HA framework checkpoint actions.
 * from src/repl/service/ckpt_handler*
 * The trace information is in the buffer "ckpt_dbg".
 */
#define	ENABLE_CKPT_DBG		0x00000001
/*
 * This option records various HA framework actions about
 * replica handler, transaction state, and transaction id activity
 * from src/repl/service/"*"
 * The trace information is in the buffer "repl_dbg".
 */
#define	ENABLE_REPL_DBG		0x00000002
/*
 * This option records various HA framework service actions
 * src/repl/service/repl_service*
 * The trace information is in the buffer "service_dbg".
 */
#define	ENABLE_SERVICE_DBG	0x00000004
/*
 * This option records various HA framework actions from src/repl/rma/"*"
 * The trace information is in the buffer "rma_dbg".
 */
#define	ENABLE_RMA_DBG		0x00000008
/*
 * This option records various HA framework actions from src/repl/repl_mgr/"*"
 * The trace information is in the buffer "rm_dbg_buf".
 */
#define	ENABLE_RM_DBG		0x00000010
/*
 * This option records various HA framework actions from src/repl/rmm/"*"
 * The trace information is in the buffer "rmm_dbg".
 */
#define	ENABLE_RMM_DBG		0x00000020
/*
 * This option records various ORB actions in the kernel.
 * The number of ORB actions would quickly overflow any reasonable
 * sized buffer. Flags control which ORB actions are recorded.
 * Refer to src/orb/orb_trace.* for details.
 * The trace information is in the buffer "the_orb_trace".
 */
#define	ENABLE_ORB_DBG		0x00000040
/*
 * The topology manager is involved in operations such as adding an adapter
 * or removing a path. Setting ENABLE_TM_DBG causes messages to be logged
 * by TM_DBG into the buffer "tm_dbg" for topology manager operations.
 */
#define	ENABLE_TM_DBG		0x00000080
/*
 * The transport is responsible for transmitting ORB messages between nodes
 * and sending heartbeats. This option enables tracing of
 * transport operations by TRANS_DBG into the buffer "trans_dbg". The
 * traced operations include the movement of paths and endpoints through
 * different states, including path failure.
 */
#define	ENABLE_TRANS_DBG	0x00000100
/*
 * The path manager subsystem is responsible for detecting path failures
 * and maintaing information on active paths.
 * This option enables tracing of path manager operations by PM_DBG
 * into the buffer "pm_dbg".  The traced operations include registration
 * of paths, paths going down, and heartbeats.
 */
#define	ENABLE_PM_DBG		0x00000200
/*
 * The endpoint registry maintains a list of active connections between nodes.
 * This option enables tracing of endpoint registry operations by ER_DBG
 * into the buffer "er_dbg". The traced operations include registration
 * and unregistration of endpoints.
 */
#define	ENABLE_ER_DBG		0x00000400
/*
 * Heartbeats are sent on each path between nodes to detect failures of
 * nodes or paths.
 * This option enables tracing of heartbeat operations by HB_DBG
 * into the path-specific buffer "*pe->hb_dbgp". The traced operations
 * the sending and receiving of each heartbeat.
 */
#define	ENABLE_HB_DBG		0x00000800
/*
 * This option records various Cluster Membership Monitor (CMM) actions,
 * such as quorum and node membership activity.
 * The trace information is in the buffer "cmm_dbg_buf".
 */
#define	ENABLE_CMM_DBG		0x00001000
/*
 * This option records various Cluster Configuration Repository (CCR) actions,
 * which revolve around changes to a cluster configuration.
 * The trace information is in the buffer "ccr_dbg".
 */
#define	ENABLE_CCR_DBG		0x00002000
/*
 * The clconf subsystem provides an interface between the CCR infrastructure
 * file and components that use the infrastructure data.
 * This option enables tracing of cluster configuration by CLCONF_DBG
 * into the buffer "clconf_dbg". The traced operations include clconf
 * updates, callback registration, and recovery.
 */
#define	ENABLE_CLCONF_DBG	0x00004000
/*
 * This option is currently not supported !!!
 * This option records various Remote Shared Memory (RSM) actions.
 * RSM is the transport implementation for remote shared memory interconnects.
 */
#define	ENABLE_RSM_DBG		0x00008000
/*
 * The tcp transport is the implementation of the transport on TCP.
 * This option enables tracing of tcp transport operations by TCP_DBPRINTF
 * into the buffer "tcp_debug".  The traced operations include starting
 * and stopping connections.
 */
#define	ENABLE_TCP_DBG		0x00010000
/*
 * This option enables enhanced tracing of tcp transport operations by
 * ETCP_DBPRINTF into the buffer "etcp_debug".  This trace results in
 * a large amound of collected data, as it traces information on every
 * packet sent and received
 */
#define	ENABLE_ETCP_DBG		0x00020000
/*
 * This option records various packet forwarding actions of packets received
 * from the public network on the GIF node to proxy nodes that process these
 * IP packets.
 * The trace information is in the buffer "fp_dbg".
 */
#define	ENABLE_FP_DBG		0x00040000
/*
 * This option records various solaris xdoor management actions.
 * A solaris xdoor is an xdoor that exists in a user level process.
 * The trace information is in the buffer pointed to by "solxdrbufp".
 */
#define	ENABLE_SDOOR_DBG	0x00080000
/*
 * This option records various actions of the raw dlpi transport used for
 * exchanging heartbeats.
 * The trace information is in the buffer "raw_dlpi_dbg".
 */
#define	ENABLE_RAW_DLPI_DBG	0x00100000
/*
 * This option records various actions inside the version manager.
 * The trace information is in the buffer "vm_dbg".
 */
#define	ENABLE_VM_DBG		0x00200000

extern uint32_t haci_dbg_option;

#define	HACI_DEBUG(flag, g_dbgbuf, a)	if (haci_dbg_option & flag) {\
						g_dbgbuf.dbprintf a; \
					} else

#define	HACI_DEBUG_CONT(flag, g_dbgbuf, a) if (haci_dbg_option & flag) {\
						g_dbgbuf.dbprintf_cont a; \
					} else

#endif /* NO_DBGBUFS */

#ifdef  __cplusplus
}
#endif


#endif	/* _HACI_DEBUG_H */
