/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  orb_stats_mgr_in.h
 *
 */

#ifndef _ORB_STATS_MGR_IN_H
#define	_ORB_STATS_MGR_IN_H

#pragma ident	"@(#)orb_stats_mgr_in.h	1.7	08/05/20 SMI"

extern orb_stats_mgr	the_orb_stats_mgr;

// static
inline orb_stats_mgr &
orb_stats_mgr::the()
{
	return (*the_orb_stats_mgr);
}

//
// Message statistics
//

inline void
orb_stats_mgr::inc_msg_types_out(sol::nodeid_t node, orb_msgtype type)
{
	msg_stat[node].inc_msg_types_out(type);
}

inline void
orb_stats_mgr::inc_msg_types_in(sol::nodeid_t node, orb_msgtype type)
{
	msg_stat[node].inc_msg_types_in(type);
}

inline void
orb_stats_mgr::inc_orphans(sol::nodeid_t node, orb_msgtype type)
{
	msg_stat[node].inc_orphans(type);
}

inline void
orb_stats_mgr::add_memory_out(sol::nodeid_t node, uint_t size)
{
	msg_stat[node].add_memory_out(size);
}

inline void
orb_stats_mgr::add_memory_in(sol::nodeid_t node, uint_t size)
{
	msg_stat[node].add_memory_in(size);
}

//
// Flow control related statistics
//
inline void
orb_stats_mgr::inc_msg_by_policy(sol::nodeid_t node,
    resources::flow_policy_t policy)
{
	flow_stat[node].inc_msg_by_policy(policy);
}

inline void
orb_stats_mgr::inc_blocked(sol::nodeid_t node)
{
	flow_stat[node].inc_blocked();
}

inline void
orb_stats_mgr::inc_unblocked(sol::nodeid_t node)
{
	flow_stat[node].inc_unblocked();
}

inline void
orb_stats_mgr::inc_reject(sol::nodeid_t node)
{
	flow_stat[node].inc_reject();
}

inline void
orb_stats_mgr::inc_pool_requests(sol::nodeid_t node)
{
	flow_stat[node].inc_pool_requests();
}

inline void
orb_stats_mgr::inc_pool_grants(sol::nodeid_t node)
{
	flow_stat[node].inc_pool_grants();
}

inline void
orb_stats_mgr::inc_pool_denies(sol::nodeid_t node)
{
	flow_stat[node].inc_pool_denies();
}

inline void
orb_stats_mgr::inc_pool_recalls(sol::nodeid_t node)
{
	flow_stat[node].inc_pool_recalls();
}

inline void
orb_stats_mgr::inc_pool_releases(sol::nodeid_t node)
{
	flow_stat[node].inc_pool_releases();
}

inline void
orb_stats_mgr::inc_balancer_requests(sol::nodeid_t node)
{
	flow_stat[node].inc_balancer_requests();
}

inline void
orb_stats_mgr::inc_balancer_grants(sol::nodeid_t node)
{
	flow_stat[node].inc_balancer_grants();
}

inline void
orb_stats_mgr::inc_balancer_denies(sol::nodeid_t node)
{
	flow_stat[node].inc_balancer_denies();
}

inline void
orb_stats_mgr::inc_balancer_recalls(sol::nodeid_t node)
{
	flow_stat[node].inc_balancer_recalls();
}

inline void
orb_stats_mgr::inc_balancer_releases(sol::nodeid_t node)
{
	flow_stat[node].inc_balancer_releases();
}

#endif	/* _ORB_STATS_MGR_IN_H */
