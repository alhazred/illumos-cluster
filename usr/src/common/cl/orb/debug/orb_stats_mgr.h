/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORB_STATS_MGR_H
#define	_ORB_STATS_MGR_H

#pragma ident	"@(#)orb_stats_mgr.h	1.11	08/05/20 SMI"

#if defined(__cplusplus)

#include <sys/os.h>
#include <sys/types.h>
#include <orb/flow/resource_defs.h>
#include <orb/flow/resource.h>
#include <h/sol.h>
#include <orb/msg/msg_stats.h>
#include <orb/flow/flow_stats.h>

//
// orb_stats_mgr - this class manages the collection of statistics about
// orb activity. There is only one object of this type on a node,
// and it resides in the kernel.
//
// In order to reduce the impact on performance, the system does not
// use locking to protect statistics operations. This means that the
// statistics are imprecise. If two threads attempt to simultaneously
// update a counter, one update will overwrite the other update.
//
// This class requires that a reset be performed on all statistics for
// that node in order to maintain some reasonable relationship between
// message and flow control statistics.
//
class orb_stats_mgr {
public:
	orb_stats_mgr();

	static int 	initialize();
	static void	shutdown();
	static orb_stats_mgr	&the();

	void		reset(sol::nodeid_t node);

	//
	// Message related statistics
	//
	void		inc_msg_types_out(sol::nodeid_t node, orb_msgtype type);
	void		inc_msg_types_in(sol::nodeid_t node, orb_msgtype type);
	void		inc_orphans(sol::nodeid_t node, orb_msgtype type);
	void		add_memory_out(sol::nodeid_t node, uint_t size);
	void		add_memory_in(sol::nodeid_t node, uint_t size);

	msg_stats	get_msg_stats(sol::nodeid_t node);

#if defined(_KERNEL_ORB)
	static int	read_orb_stats_msg(void *argp);
#endif

	//
	// Flow control related statistics
	//
	void		inc_msg_by_policy(sol::nodeid_t node,
			    resources::flow_policy_t policy);

	void		inc_blocked(sol::nodeid_t node);
	void		inc_unblocked(sol::nodeid_t node);
	void		inc_reject(sol::nodeid_t node);
	void		inc_pool_requests(sol::nodeid_t node);
	void		inc_pool_grants(sol::nodeid_t node);
	void		inc_pool_denies(sol::nodeid_t node);
	void		inc_pool_recalls(sol::nodeid_t node);
	void		inc_pool_releases(sol::nodeid_t node);
	void		inc_balancer_requests(sol::nodeid_t node);
	void		inc_balancer_grants(sol::nodeid_t node);
	void		inc_balancer_denies(sol::nodeid_t node);
	void		inc_balancer_recalls(sol::nodeid_t node);
	void		inc_balancer_releases(sol::nodeid_t node);

	flow_stats	get_flow_stats(sol::nodeid_t node);

#if defined(_KERNEL_ORB)
	static int	read_orb_stats_flow(void *argp);
#endif

private:
	static orb_stats_mgr	*the_orb_stats_mgr;

	msg_stats	msg_stat[NODEID_MAX+1];
	flow_stats	flow_stat[NODEID_MAX+1];
};

#include <orb/debug/orb_stats_mgr_in.h>

extern "C" {
#endif /* __cplusplus */

/*
 * Functions support cladmin commands
 */
#if defined(_KERNEL_ORB)
int	cl_read_orb_stats_msg(void *argp);
int	cl_read_orb_stats_flow(void *argp);
#endif

#if defined(__cplusplus)
}
#endif

#endif	/* _ORB_STATS_MGR_H */
