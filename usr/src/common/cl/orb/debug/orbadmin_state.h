/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBADMIN_STATE_H
#define	_ORBADMIN_STATE_H

#pragma ident	"@(#)orbadmin_state.h	1.8	08/05/20 SMI"

#include <sys/os.h>
#include <sys/types.h>

/*
 * Provides the means for orbadmin commands to access selected orb state
 */

struct state_of_resource_pool {
	int	node;
	int	pool;
	int	thread_total;
	int	thread_free;
	int	thread_needed;
	int	thread_torecall;
	int	thread_torelease;
};

struct state_balancer {
	int	pool;
	int	threads_total;
	int	threads_unallocated;
};

struct state_unref {
	uint_t	length;		/* Number of queued unreference jobs */
};

struct state_refcnt {
	int	node;
	int	ref_jobs;	/* Number of queued reference count jobs */
	int	ack_jobs;	/* Number of queued ack jobs */
};

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

/*
 * Functions support cladmin commands
 */
#if defined(_KERNEL_ORB)
int	cl_read_orb_state_pool(void *argp);
int	cl_read_orb_state_balancer(void *argp);
int	cl_read_orb_state_unref(void *argp);
int	cl_read_orb_state_refcount(void *argp);
#endif

#if defined(__cplusplus)
}
#endif

#endif	/* _ORBADMIN_STATE_H */
