/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ORB_TRACE_H
#define	_ORB_TRACE_H

#pragma ident	"@(#)orb_trace.h	1.24	08/05/20 SMI"

#include <orb/debug/haci_debug.h>

//
// XXX ORB_DBGBUF and DRC_DEBUG should be combined into one entity (probably
// the former, as that is how we handle all our other debug buffers).
//

#if defined(HACI_DBGBUFS) && !defined(NO_ORB_DBGBUF)
#define	ORB_DBGBUF
#endif

#ifdef	ORB_DBGBUF
extern dbg_print_buf the_orb_trace;
#define	ORB_DBG(a)	HACI_DEBUG(ENABLE_ORB_DBG, the_orb_trace, a)
#else
#define	ORB_DBG(a)
#endif


#if defined(ORB_DBGBUF) && !defined(NO_DRC_DEBUG)
#define	DRC_DEBUG
#endif

//
// This provides a utility for tracing orb activity.
//

//
// These options control which debug statements are enabled
// This allows them to be controlled via the debugger.
// These constants are always defined in order to catch name conflicts
//
const int	ORB_TRACE_TWOWAY	= 0x0000001;	// Remote 2way request
const int	ORB_TRACE_REPLY		= 0x0000002;	// Remote reply
const int	ORB_TRACE_ONEWAY	= 0x0000004;	// Remote 1way request
const int	ORB_TRACE_CKPT		= 0x0000008;	// Checkpoint request
const int	ORB_TRACE_REQ_LOC	= 0x0000010;	// local requests
const int	ORB_TRACE_MARSHAL	= 0x0000020;
const int	ORB_TRACE_TRANSLATE	= 0x0000040;
const int	ORB_TRACE_REFCNT	= 0x0000080;
const int	ORB_TRACE_OBJEND	= 0x0000100;
const int	ORB_TRACE_REFMSG	= 0x0000200;
const int	ORB_TRACE_SLEEP		= 0x0000400;
const int	ORB_TRACE_SLOT		= 0x0000800;
const int	ORB_TRACE_ITERATE	= 0x0001000;	// ref_jobs iteration
							// Flow control
const int	ORB_TRACE_ONE_FLOW	= 0x0002000;	// oneway invocations
const int	ORB_TRACE_FLOW_BLOCK	= 0x0004000;	// msg request blocks
const int	ORB_TRACE_FLOW_ALLOC	= 0x0008000;	// resource allocation

const int	ORB_TRACE_RECONFIG	= 0x0010000;	// Node reconfiguration
const int	ORB_TRACE_OUT_INVO	= 0x0020000;	// Wait for results
const int	ORB_TRACE_IN_INVO	= 0x0040000;	// Server invo tracking
const int	ORB_TRACE_SEQ_NUM	= 0x0080000;	// seqnum info

const int	ORB_TRACE_GATEWAY	= 0x0100000;	// Gateway handler acts

const int	ORB_TRACE_MONITOR	= 0x0200000;	// Memory Monitor

#ifdef DRC_DEBUG
extern uint_t		orb_trace_options;

//
// ORB_DBPRINTF - record orb trace information.
// Inputs
//	option - enables trace if set in orb_trace_options
//	args - The arguments to the print statement must be enclosed
//		with parenthesis, as in the following example:
//			("orb action %d\n", action_number)
//
#define	ORB_DBPRINTF(option, args)	if (option & orb_trace_options) {\
	ORB_DBG(args); \
	} else
#else
#define	ORB_DBPRINTF(option, args)
#endif

#endif	/* _ORB_TRACE_H */
