/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)haci_debug.cc	1.13	08/05/20 SMI"

#include <orb/debug/haci_debug.h>

#ifdef HACI_DBGBUFS

#ifdef DEBUG
uint32_t haci_dbg_option = (uint32_t)0xffffffff;
#else // DEBUG
uint32_t haci_dbg_option = (ENABLE_CCR_DBG |
	ENABLE_CMM_DBG |
	ENABLE_HB_DBG |
	ENABLE_ORB_DBG |
	ENABLE_TCP_DBG |
	ENABLE_RAW_DLPI_DBG |
	ENABLE_RM_DBG |
	ENABLE_SERVICE_DBG |
	ENABLE_PM_DBG |
	ENABLE_TM_DBG |
	ENABLE_VM_DBG);
#endif // DEBUG

#endif // HACI_DBGBUFS
