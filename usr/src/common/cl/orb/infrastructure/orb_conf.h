/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ORB_CONF_H
#define	_ORB_CONF_H

#pragma ident	"@(#)orb_conf.h	1.35	08/05/20 SMI"

// File containing the interface to routines which can be used
// To find out the configuration of the ORB.

#include <orb/invo/common.h>
#ifdef _KERNEL_ORB
#include <orb/member/Node.h>
#endif

class orb_conf {
public:
	// Reads in the configuration information.
	// Called during ORB::initialize
	static  int	configure();

	//
	// Local nodeid. This is constant.
	// node_number() and local_nodeid() return the same value
	// We should transition to using local_nodeid()
	//
	static nodeid_t local_nodeid();
	static nodeid_t node_number();

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
	// XXX This code is only available for the fault injection and
	// quorum tests and will be removed soon
	// Gets the root nodeid
	static nodeid_t get_root_nodeid();
#endif

	// Returns a boolean value if the node specified is a configured
	// member of the cluster.
	static	bool node_configured(nodeid_t);

	// Max RT priority and class name for this system
	static pri_t rt_maxpri();
	static const char *rt_classname();

#ifdef _KERNEL_ORB
	//
	// Local incarnation number. This is constant.
	//
	static incarnation_num local_incarnation();

	// Returns an ID_node corresponding to the current incarnation
	// This is a return by reference and the value can change if
	// there is an incarnation number in between
	// XXX If may be safer to have the signature as
	// void current_id_node(ID_node *) with caller supplied buffer
	static  ID_node	&current_id_node();

	// Checks whether the ID_node passed in corresponds to the current
	// incarnation of the local node.
	// If incn passed in is INCN_UNKNOWN, the incn check is skipped
	static	bool is_local_incn(ID_node &);
#endif
private:
	// These variables are set in orb_conf::initialize
#ifdef _KERNEL_ORB
	static ID_node current_node;
#else
	static nodeid_t current_node;
#endif
	// Max RT priority and class name for this system
	// In unode and for user land daemons if RT class is not configured
	// or if run as non super-user, we use the TS class for convenience
	static pri_t maxpri;
	static const char *rt_clname;

#ifndef _KERNEL_ORB
	// Used by userland node_configured().
	static os::mutex_t	*buf_lock;
#endif
};

#include <orb/infrastructure/orb_conf_in.h>
#endif	/* _ORB_CONF_H */
