//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cladmin_debug.cc	1.3	08/05/20 SMI"

#include <sys/cladm_debug.h>

#ifdef	CLADM_DBGBUF
dbg_print_buf cladm_dbg(0x10000);
#endif

#define	ALL_OPTIONS	(uint_t)0xffffffff

uint_t cladm_trace_options = ALL_OPTIONS;
int cladm_trace_level = CLADM_GREEN;
