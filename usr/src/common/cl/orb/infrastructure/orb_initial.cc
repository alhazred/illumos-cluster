//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb_initial.cc	1.190	08/05/20 SMI"


#include <orb/invo/common.h>
#include <sys/modctl.h>

#include <cplplrt/cplplrt.h>
#include <sys/cladm.h>

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_bootstrap misc/cl_load "
	"fs/procfs";


static struct modlmisc modlmisc = {
	&mod_miscops, "CORBA runtime support",
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL }
#endif
};

extern "C" int
_init(void)
{
	int	error;
	sc_syslog_msg_handle_t	handle;

	// fails if ENOMEM or unable to acquire mutex lock, both of which
	// are rare possibilities.
	if (sc_syslog_msg_initialize(&handle, SC_SYSLOG_FRAMEWORK_TAG,
	    SC_SYSLOG_FRAMEWORK_RSRC) != SC_SYSLOG_MSG_STATUS_GOOD) {
		return (ENOMEM);
	}

	if ((cluster_bootflags & CLUSTER_BOOTED) == 0) {
		//
		// SCMSGS
		// @explanation
		// Attempted to load the cl_comm module when the node was not
		// booted as part of a cluster.
		// @user_action
		// Users should not explicitly load this module.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "cl_orb: not booted in cluster mode.");
		sc_syslog_msg_done(&handle);
		return (EINVAL);
	}

	if ((error = mod_install(&modlinkage)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, SC_SYSLOG_WARNING, MESSAGE,
		    "cl_orb: error loading kernel module: %d", error);
		sc_syslog_msg_done(&handle);
		return (error);
	}

	sc_syslog_msg_done(&handle);

	_cplpl_init();		/* C++ initialization */

	return (0);
}

extern "C" int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
