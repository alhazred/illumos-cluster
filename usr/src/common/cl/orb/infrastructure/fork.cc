//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fork.cc	1.18	08/05/20 SMI"

#include <errno.h>
#include <sys/types.h>
#include <dlfcn.h>

#include <orb/invo/corba.h>
#include <orb/infrastructure/orb.h>
#include <orb/xdoor/solaris_xdoor.h>
#include <orb/xdoor/sxdoor.h>
#include <orb/handler/remote_handler.h>
#include <sys/os.h>
#include <sys/rsrc_tag.h>

// init function invokes pthread_handlers on fork()/fork1()/forkall() call.
void fork_init(void);

// pragma defined later as fork_init declaration needed.
/* CSTYLED */
#pragma init (fork_init)

typedef pid_t (*fork_fn_ptr)(void);

// prototypes for functions defined in this file
extern "C" pid_t _fork(void);
extern "C" pid_t _fork1(void);
extern "C" pid_t _vfork(void);

#ifdef	CONSISTENT_FORK
extern "C" pid_t _forkall(void);
#endif	// CONSISTENT_FORK

//
// These pragma definition facilitates interposing fork/fork1/vfork
// definitions in libc.
//
#pragma weak fork = _fork
#pragma weak fork1 = _fork1
#pragma weak vfork = _vfork

#ifdef	CONSISTENT_FORK
#pragma weak "forkall" = "_forkall"
#endif	// CONSISTENT_FORK

static fork_fn_ptr
get_libc_func(char *func)
{
	fork_fn_ptr ptr = NULL;

	ptr = (fork_fn_ptr)dlsym(RTLD_NEXT, func); //lint !e611

	if (ptr == NULL) {
		os::sc_syslog_msg	log(SC_SYSLOG_FRAMEWORK_TAG,
		    SC_SYSLOG_FRAMEWORK_RSRC, NULL);

		//
		// SCMSGS
		// @explanation
		// The function get_libc_func could not find the specified
		// function for the reason specified. Refer to the man pages
		// for "dlsym" and "dlerror" for more information.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) log.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Could not find %s(): %s", func, dlerror());
	}
	return (ptr);
}

//
// _fork()
//
// This call overrides the system call fork(2).
// This function prevents ORB servers from calling fork().
// The behaviour of real fork() is to return -1
// upon failure and set errno.
//
extern "C" pid_t
_fork(void)
{
	if (ORB::is_server_initialized()) {
		os::sc_syslog_msg	log(SC_SYSLOG_FRAMEWORK_TAG,
		    SC_SYSLOG_FRAMEWORK_RSRC, NULL);

		//
		// SCMSGS
		// @explanation
		// A user level process attempted to fork after ORB server
		// initialization. This is not allowed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) log.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm:Cannot fork() after ORB server initialization.");

		// Need some error code that indicates this operation is not
		// allowed.

		errno = ENOTSUP;
		return (-1);
	}

	// Get pointer to the real _fork function
	fork_fn_ptr real_fork = get_libc_func("_fork");

	if (real_fork == NULL) {
		errno = ENOENT;
		return (-1);
	}
	return ((*real_fork)());
}

//
// _fork1
//
// This call overrides the system call fork1(2).
// This function prevents ORB servers from calling fork().
// The behaviour of real fork1() is to return -1
// upon failure and set errno.
//
extern "C" pid_t
_fork1(void)
{
	if (ORB::is_server_initialized()) {
		os::sc_syslog_msg	log(SC_SYSLOG_FRAMEWORK_TAG,
		    SC_SYSLOG_FRAMEWORK_RSRC, NULL);

		//
		// SCMSGS
		// @explanation
		// A user level process attempted to fork1 after ORB server
		// initialization. This is not allowed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) log.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm:Cannot fork1() after ORB server initialization.");

		// Need some error code that indicates this operation is not
		// allowed.
		errno = ENOTSUP;
		return (-1);
	}

	// Get pointer to the real _fork1 function
	fork_fn_ptr real_fork1 = get_libc_func("_fork1");

	if (real_fork1 == NULL) {
		errno = ENOENT;
		return (-1);
	}
	return ((*real_fork1)());
}

//
// _vfork
//
// This call overrides the system call vfork(2).
// This function prevents ORB servers from calling fork().
// The behaviour of real vfork() is to return -1
// upon failure and set errno.
//

extern "C" pid_t
_vfork(void)
{
	if (ORB::is_server_initialized()) {
		os::sc_syslog_msg	log(SC_SYSLOG_FRAMEWORK_TAG,
		    SC_SYSLOG_FRAMEWORK_RSRC, NULL);

		//
		// SCMSGS
		// @explanation
		// A user level process attempted to vfork after ORB server
		// initialization. This is not allowed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) log.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm:Cannot vfork() after ORB server initialization.");

		// Need some error code that indicates this operation is not
		// allowed.
		errno = ENOTSUP;
		return (-1);
	}

	//
	// Get pointer to the real fork function...
	//
	// Actually, we have to substitute vfork with fork1
	// as otherwise this interposition would not work
	// (a procedure that calls vfork cannot return while
	// running in the child's context (see vfork(2))).
	//
	fork_fn_ptr real_vfork = get_libc_func("_fork1");

	if (real_vfork == NULL) {
		errno = ENOENT;
		return (-1);
	}
	return ((*real_vfork)());
}

#ifdef	CONSISTENT_FORK

// _forkall()
//
// This call overrides the system call forkall(2).
// This function prevents ORB servers from calling forkall().
// The behaviour of real forkall() is to return -1
// upon failure and set errno.
//
extern "C" pid_t
_forkall(void)
{
	if (ORB::is_server_initialized()) {
		os::sc_syslog_msg	log(SC_SYSLOG_FRAMEWORK_TAG,
		    SC_SYSLOG_FRAMEWORK_RSRC, NULL);

		//
		// SCMSGS
		// @explanation
		// A user level process attempted to forkall after ORB server
		// initialization. This is not allowed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) log.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm:Cannot forkall() after ORB server initialization.");

		// Need some error code that indicates this operation is not
		// allowed.

		errno = ENOTSUP;
		return (-1);
	}

	// Get pointer to the real _forkall function
	fork_fn_ptr real_forkall = get_libc_func("_forkall");

	if (real_forkall == NULL) {
		errno = ENOENT;
		return (-1);
	}
	return ((*real_forkall)());
}
#endif	// CONSISTENT_FORK

//
// following handlers make sure lock ordering is maintained and fork-one
// safety problem is avoided.
//
// prepare_handler
//
// prepare_handler is called before fork/fork1 system call is made.
// prepare_handler grabs all the locks defined in this library.
// It grabs invo lock defined in solaris_xdoor_client to make sure
// all invocations are blocked when a fork/fork1 call is made.
// It grabs all the handler and xdoor locks inorder to maintain
// lock ordering.
//
void
pthread_atfork_handler::prepare_handler()
{
	if (ORB::is_initialized()) {
		// sleep until no invocation is in progress.
		solaris_xdoor_client::invo_lck.lock();
		while (solaris_xdoor_client::invocation_in_progress()) {
			// wait on this condvar for the invocations to complete.
			solaris_xdoor_client::invo_cv.wait(
			    &(solaris_xdoor_client::invo_lck));
		}
again:
		sxdoor_manager::the().lock();
		// lock all handler's and xdoor's.
		solaris_xdoor *xdp = NULL;
		remote_handler *handlerp = NULL;
		sxdoor_table::the().lock();
		hashtable_t<solaris_xdoor *, door_id_t>::iterator iter(
		    sxdoor_table::the().doortab);
		while ((xdp = iter.get_current()) != NULL) {
			iter.advance();
			handlerp = (remote_handler *)xdp->get_user_handler();

			//
			// We're about to acquire locks on all the handlers
			// asssociated with sxdoors in the sxdoor_table. A
			// deadlock can arise between this thread and another
			// thread processing an unreference. If we encounter
			// a conflict here, we drop the locks on the
			// sxdoor_table and sxdoor_manager, and start over.
			// Note that we hold on to the handlers we've locked.
			// This is a small optimization.
			//
			if (handlerp != NULL && !handlerp->lock_held()) {
				if (!handlerp->try_lock()) {
					sxdoor_table::the().unlock();
					sxdoor_manager::the().unlock();
					goto again;
				}
			}
		}

		//
		// lock all the debug buffers by iterating dbg_print_buf_listp
		// which is a list of dbg_print_buf instances.
		//
		dbg_print_buf::dbgbuf_list_lock.lock();

		dbg_print_buf::dbg_print_buf_listp->atfirst();
		dbg_print_buf *dbg_bufp = NULL;
		while ((dbg_bufp =
		    dbg_print_buf::dbg_print_buf_listp->get_current())
		    != NULL) {
			dbg_bufp->lock();
			dbg_print_buf::dbg_print_buf_listp->advance();
		}
		c_dbgbuf_lock.lock();
	}
}

//
// parent_handler
//
// parent_handler unlocks all locks grabbed in prepare_handler
// in the parent process after the fork/fork1 system call.
//
void
pthread_atfork_handler::parent_handler()
{
	if (ORB::is_initialized()) {

		c_dbgbuf_lock.unlock();

		//
		// unlock all the debug buffers by iterating dbg_print_buf_listp
		// which is a list of dbg_print_buf instances.
		//
		dbg_print_buf::dbg_print_buf_listp->atfirst();
		dbg_print_buf *dbg_bufp = NULL;
		while ((dbg_bufp =
		    dbg_print_buf::dbg_print_buf_listp->get_current())
		    != NULL) {
			dbg_bufp->unlock();
			dbg_print_buf::dbg_print_buf_listp->advance();
		}
		dbg_print_buf::dbgbuf_list_lock.unlock();

		// unlock all handler's and xdoor's.
		solaris_xdoor *xdp = NULL;
		remote_handler *handlerp = NULL;
		hashtable_t<solaris_xdoor *, door_id_t>::iterator iter(
		    sxdoor_table::the().doortab);
		while ((xdp = iter.get_current()) != NULL) {
			iter.advance();
			handlerp = (remote_handler *)xdp->get_user_handler();

			//
			// It's possible that an xdoor that's being created
			// while an invocation is being unmarshalled didn't
			// have a handler in prepare_handler(). Later, by
			// the time the parent calls parent_handler(), the
			// handler for the xdoor might have been created.
			// Hence we check whether or not we hold the lock on
			// the handler here.
			//
			if ((handlerp != NULL) && (handlerp->lock_held())) {
				handlerp->unlock();
			}
		}
		sxdoor_table::the().unlock();
		sxdoor_manager::the().unlock();
		solaris_xdoor_client::invo_lck.unlock();
	}
}

//
// child_handler
//
// child_handler unlocks all locks grabbed in prepare_handler routine
// in the child process. Owner of all locks are updated to new threadid
// in the child process.
//
void
pthread_atfork_handler::child_handler()
{
	if (ORB::is_initialized()) {

		// unlock all the debug buffers.

		c_dbgbuf_lock.set_owner();
		c_dbgbuf_lock.unlock();

		//
		// unlock all the debug buffers by iterating dbg_print_buf_listp
		// which is a list of dbg_print_buf instances.
		//
		dbg_print_buf::dbgbuf_list_lock.set_owner();

		dbg_print_buf::dbg_print_buf_listp->atfirst();
		dbg_print_buf *dbg_bufp = NULL;
		while ((dbg_bufp =
		    dbg_print_buf::dbg_print_buf_listp->get_current())
		    != NULL) {
			dbg_bufp->lck.set_owner();
			dbg_bufp->unlock();
			dbg_print_buf::dbg_print_buf_listp->advance();
		}
		dbg_print_buf::dbgbuf_list_lock.unlock();

		// unlock all handler's and xdoor's.
		solaris_xdoor *xdp = NULL;
		remote_handler *handlerp = NULL;
		hashtable_t<solaris_xdoor *, door_id_t>::iterator iter(
		    sxdoor_table::the().doortab);
		while ((xdp = iter.get_current()) != NULL) {
			iter.advance();
			handlerp = (remote_handler *)xdp->get_user_handler();

			//
			// We really should only do the following for locks
			// that have a non-zero _owner field. But os::mutex_t
			// does not yet have a reliable interface to check for
			// this situation. We should revisit this in the
			// future.
			//
			if (handlerp != NULL) {
				handlerp->lck.set_owner();
				handlerp->unlock();
			}
		}
		sxdoor_table::the().lck.set_owner();
		sxdoor_table::the().unlock();

		sxdoor_manager::the().lck.set_owner();
		sxdoor_manager::the().unlock();

		solaris_xdoor_client::invo_lck.set_owner();
		solaris_xdoor_client::invo_lck.unlock();
	}
}

//
// fork_init
//
// Suppress lint complaint, since this funtion is not called explicitly
void
fork_init(void)
{
	int result = pthread_atfork(pthread_atfork_handler::prepare_handler,
	    pthread_atfork_handler::parent_handler,
	    pthread_atfork_handler::child_handler);
	ASSERT(result == 0);
}
