//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)copy.cc	1.54	08/05/20 SMI"

//
// This file handles the interface between the kernel's copyin/out and
// the MC copyin/out.
//

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/copy.h>
#include <h/addrspc.h>
#include <nslib/ns.h>
#include <stdio.h>
#include <sys/cl_comm_support.h>

#include <sys/thread.h>
#include <sys/copyops.h>

#include <sys/sol_version.h>

//
// This file and client/addrspace_impl.cc implement a mechanism to handle
// cross-node ioctls.  The problem is that the device driver can do random
// copyin/outs.  To handle this, we modify the low-level copyin/out/str
// routines to call mc_copyin.  For cross-node operations, we set thread-
// specific data with information on the originating node and pid.
// We assume that the process won't move or exit during the ioctl system
// call, so we can communicate with the home node's addrspace object.
// This object actually does the copyin/out to the originating process.
//
// In the new approach, there is a single addrspace object on each node,
// created when the module is loaded.  This object takes (pid, address,
// length) and does a copyin/copyout/copyinstr.  To support this, instead of
// passing around an addrspace object, ioctls now pass around the nodeid and
// pid.  In addition, if the ioctl is local, the new copyin/out routines are
// bypassed entirely.  The new code speeds up local ioctls by about a factor
// of 4.
//
// There is a special-case scenario to support the HA-ness of ioctls.  The
// problem is that if there is an ioctl that needs to be replayed after a
// failover/switchover (for e.g. the UFS '_FIOLFS' ioctl), then during the
// replay, there is no user thread that makes the call - it is done by the
// appropriate component as part of the failover/switchover, and the arguments
// are already in kernel space.  Hence, when the ioctl handler calls
// copyin/copyout, we need to do a simple bcopy.  To indicate to
// mc_copyin/mc_copyout that the caller of the ioctl is the kernel, we set
// the copy_args of the thread to (orb_conf::node_number, 0) before issuing
// the ioctl.
//

os::tsd copy::context; // Thread-specific context storage

//
// We originally had a transport problem with sending too-big packets.
// So, we fairly arbitrarily set a length so the data will fit in a
// 100K packet, and use xfersize to break the transfer into units no
// larger than this.  This may no longer be necessary.
//

#define	XXX_ARBITRARY_LENGTH (100 * 1024)

static inline size_t
xfersize(size_t n)
{
	return (MIN(n, XXX_ARBITRARY_LENGTH));
}

//
// On each node, we have a cache that points to the addrspace objects on
// each of the other nodes.  These entries are loaded from the name server
// on demand.
//
static addrspc::addrspace_var	addrspaceCache_v[NODEID_MAX+1];
static os::mutex_t		addrspace_mutex;

copy::copy()
{
	for (int i = 0; i <= NODEID_MAX; i++) {
		addrspaceCache_v[i] = addrspc::addrspace::_nil();
	}
}

//
// getAddrspc does the lookup of an addrspace object in the cache.
// getAddrspc returns nil if it can't find the addrspace object in the
// name server.  This shouldn't happen normally.
//
static addrspc::addrspace_ptr
getAddrspc(sol::nodeid_t nodeid)
{
	addrspc::addrspace_ptr	addrspc_p;
	ASSERT(nodeid <= NODEID_MAX);

	addrspace_mutex.lock();
	if (CORBA::is_nil(addrspaceCache_v[nodeid])) {
		// get an object reference of the mount name server
		naming::naming_context_var	ctx_v = root_nameserver_funcp();

		Environment	e;
		char		name[20];
		(void) sprintf(name, "addrspace.%d", nodeid);
		CORBA::Object_var	obj_v = ctx_v->resolve(name, e);
		if (e.exception()) {
			addrspace_mutex.unlock();
#ifdef DEBUG
			e.exception()->print_exception("getAddrspc: Exception "
							"getting\n");
#endif
			return (addrspc::addrspace::_nil());
		}
		addrspaceCache_v[nodeid] = addrspc::addrspace::_narrow(obj_v);
		ASSERT(is_not_nil(addrspaceCache_v[nodeid]));
	}
	addrspc_p = addrspc::addrspace::_duplicate(addrspaceCache_v[nodeid]);
	addrspace_mutex.unlock();
	return (addrspc_p);
}

//
// checkAddrspc -
// Releases the cached addrspace reference, and gets a new one from the
// name server, caching it again.  Returns the new reference if it differs
// from the reference passed in, otherwise returns nil.  Called by
// mc_xcopyin, mc_xcopyout, and mc_copyinstr when an invocation on the
// addrspace object raises a system exception.  Calling checkAddrspc with
// the old reference used for the invocation will refresh the cache and
// determine if the old reference was stale.  If it was stale (i.e., there
// is a valid new reference that differs from the old one), we want to
// retry the invocation with the new reference; if not, we give up and
// return an error.
//
static addrspc::addrspace_ptr
checkAddrspc(sol::nodeid_t nodeid, addrspc::addrspace_ptr old_obj_p)
{
	// Clear the addrspace object cache entry for the specified node.
	addrspace_mutex.lock();
	addrspaceCache_v[nodeid] = addrspc::addrspace::_nil();
	addrspace_mutex.unlock();

	// Get new reference from name server and cache it.
	addrspc::addrspace_ptr	new_obj_p = getAddrspc(nodeid);

	// Compare old and new references.
	if (!CORBA::is_nil(new_obj_p) && new_obj_p->_equiv(old_obj_p)) {
		//
		// Old and new references are the same.
		// Drop new reference and return nil.
		//
		CORBA::release(new_obj_p);
		return (addrspc::addrspace::_nil());
	} else {
		//
		// Either we got a different reference or a nil one.  Return it.
		// getAddrspc has already duplicated it if it's non-nil.
		//
		return (new_obj_p);
	}
}

static int	mc_copyin(const void *, void *, size_t);
static int	mc_xcopyin(const void *, void *, size_t);
static int	mc_copyout(const void *, void *, size_t);
static int	mc_xcopyout(const void *, void *, size_t);
static int	mc_copyinstr(const char *, char *, size_t, size_t *);
static int	mc_copyoutstr(const char *, char *, size_t, size_t *);
static int	mc_fuword8(const void *, uint8_t *);
static int	mc_fuiword8(const void *, uint8_t *);
static int	mc_fuiword32(const void *, uint32_t *);
static int	mc_suiword8(void *, uint8_t);
static int	mc_suiword32(void *, uint32_t);
static int	mc_fuword16(const void *, uint16_t *);
static int	mc_fuword32(const void *, uint32_t *);
static int	mc_fuword64(const void *, uint64_t *);
static int	mc_suword8(void *, uint8_t);
static int	mc_suword16(void *, uint16_t);
static int	mc_suword32(void *, uint32_t);
static int	mc_suword64(void *, uint64_t);

static struct copyops mc_copyops = {
	mc_copyin,
	mc_xcopyin,
	mc_copyout,
	mc_xcopyout,
	mc_copyinstr,
	mc_copyoutstr,
	mc_fuword8,
#if SOL_VERSION < __s10
	mc_fuiword8,
#endif
	mc_fuword16,
	mc_fuword32,
#if SOL_VERSION < __s10
	mc_fuiword32,
#endif
	mc_fuword64,
	mc_suword8,
#if SOL_VERSION < __s10
	mc_suiword8,
#endif
	mc_suword16,
	mc_suword32,
#if SOL_VERSION < __s10
	mc_suiword32,
#endif
	mc_suword64,
	default_physio
};

//
// Copy data in from user address to kernel address.
// 'cn' is the count of the number of bytes to copy.
//
static int
mc_xcopyin(const void *uaddr, void *kaddr, size_t cn)
{
	caddr_t userbuf = (caddr_t)uaddr;
	caddr_t driverbuf = (caddr_t)kaddr;

	// We have been called by the xcopyin code to perform the
	// network op.

	copy_args *args = copy::getcontext();
	ASSERT(args != (copy_args *)-1);
	ASSERT(args != (copy_args *)-2);
	ASSERT(args != NULL);

	if (args->pid == 0) {
		if (args->nodeid == orb_conf::node_number()) {
			//
			// A pid of zero is used to replay ioctls from the
			// local kernel during a switchover/failover.  At this
			// point, the data is already in the kernel and a
			// bcopy is in order.
			//
			bcopy(uaddr, kaddr, cn);
			return (0);
		} else {
			// This path (kernel making an ioctl call on a remote
			// node) has no callers yet, and it is unclear whether
			// we need to implement it.
			// For now, we have a panic in here to catch any
			// callers.
			//

			//
			// SCMSGS
			// @explanation
			// The system does not support copy operations between
			// the kernel and a user process when the specified
			// node is not the local node.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: Invalid copyargs: node %d pid %d",
			    args->nodeid, args->pid);
		}
	}

	addrspc::addrspace_var addrspace_obj = getAddrspc(args->nodeid);
	if (CORBA::is_nil(addrspace_obj)) {
		return (ENODEV);
	}
	Environment e;

	if (cn == 0) {
		return (0);
	}
	addrspc::ioctldata_t *data_ptr;
	//
	// We invalidate the context to make sure we don't end up
	// in here multiple times somehow.
	//
#ifdef DEBUG
	copy::setcontext((copy_args *)-2);
#endif
	while (cn != 0) {
		sol::error_t err;

		// Get the data from the remote process into data_ptr.
		err = addrspace_obj->copyin(args->pid,
		    (sol::uintptr_t)(long)userbuf,
		    (sol::size_t)xfersize(cn), data_ptr, e);
		if (e.exception()) {
			//
			// There are no user exceptions defined for
			// this interface, so this is a system
			// exception (e.g., the node is down or has
			// rebooted).  Since we're caching the
			// addrspace reference, we may have a stale
			// reference and need a new one.
			//
			ASSERT(e.sys_exception() != NULL);
			e.clear();

			// releases old reference in addrspace_obj
			addrspace_obj = checkAddrspc(args->nodeid,
			    addrspace_obj);
			if (CORBA::is_nil(addrspace_obj)) {
				// No new reference.
#ifdef DEBUG
				copy::setcontext(args);
#endif
				return (ENODEV);
			} else {
				//
				// Got a new reference different from
				// the old one.
				//
				continue;
			}
		}
		if (err != 0) {
			delete data_ptr;
#ifdef DEBUG
			copy::setcontext(args);
#endif
			return (EFAULT); // difference xcopy/copy
		}
		ASSERT(data_ptr->length() > 0);
		ASSERT(data_ptr->length() <= cn);
		// Copy the data from data_ptr to the kernel buffer.
		bcopy(data_ptr->buffer(), driverbuf,
				(size_t)data_ptr->length());
		cn -= data_ptr->length();
		driverbuf += data_ptr->length();
		userbuf += data_ptr->length();
		//
		// Copyin created data_ptr; we no longer want it
		// since the data has been copied to driverbuf.
		//
		delete data_ptr;
	}
#ifdef DEBUG
	copy::setcontext(args);
#endif
	return (0);
}

//
// Copy data out from kernel address to user address.
// 'cn' is the count of the number of bytes to copy.
//
static int
mc_xcopyout(const void *kaddr, void *uaddr, size_t cn)
{
	caddr_t driverbuf = (caddr_t)kaddr;
	caddr_t userbuf = (caddr_t)uaddr;

	// We have been called by the xcopyout code to perform the
	// network op.

	copy_args *args = copy::getcontext();
	ASSERT(args != (copy_args *)-1);
	ASSERT(args != (copy_args *)-2);
	ASSERT(args != NULL);

	if (args->pid == 0) {
		if (args->nodeid == orb_conf::node_number()) {
			//
			// A pid of zero is used to replay ioctls from the
			// local kernel during a switchover/failover.  At this
			// point, the data is already in the kernel and a
			// bcopy is in order.
			//
			bcopy(kaddr, uaddr, cn);
			return (0);
		} else {
			// This path (kernel making an ioctl call on a remote
			// node) has no callers yet, and it is unclear whether
			// we need to implement it.
			// For now, we have a panic in here to catch any
			// callers.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: Invalid copyargs: node %d pid %d",
			    args->nodeid, args->pid);
		}
	}

	if (cn == 0) {
		return (0); // check before we create data object
		//
		// Note, in sun4m Solaris, copyout(..,..,0) fails
		// while copyout(..,..,-1) succeeds, while
		// copyin(bad addr,...,0) gets EFAULT.  This is all
		// basically random behavior based on the details
		// of the assembly code, so let's just succeed.
		// If we want to be bug-for-bug compatible, it
		// will be very messy.
		//
	}
	addrspc::addrspace_var addrspace_obj = getAddrspc(args->nodeid);
	if (CORBA::is_nil(addrspace_obj)) {
		return (ENODEV);
	}
	Environment e;

	ASSERT(xfersize(cn) <= UINT_MAX);	// for casts below
	addrspc::ioctldata_t *data_ptr = new addrspc::ioctldata_t(
				(uint_t)xfersize(cn), (uint_t)xfersize(cn));

	while (cn != 0) {
		sol::error_t err;
		size_t xsize = xfersize(cn);
		data_ptr->length((uint_t)xsize);
		// Copy the data into the data_ptr buffer.
		bcopy(driverbuf, data_ptr->buffer(), xsize);
		ASSERT(data_ptr->length() <= data_ptr->maximum());
		// Send the data out to the remote process.
		err = addrspace_obj->copyout(args->pid,
		    (sol::uintptr_t)(long)userbuf,
		    (sol::size_t)xsize, *data_ptr, e);
		if (e.exception()) {
			//
			// There are no user exceptions defined for
			// this interface, so this is a system
			// exception (e.g., the node is down or has
			// rebooted).  Since we're caching the
			// addrspace reference, we may have a stale
			// reference and need a new one.
			//
			ASSERT(e.sys_exception() != NULL);
			e.clear();

			// Releases old reference in addrspace_obj.
			addrspace_obj = checkAddrspc(args->nodeid,
			    addrspace_obj);
			if (CORBA::is_nil(addrspace_obj)) {
				// No new reference.
				delete data_ptr;
				return (ENODEV);
			} else {
				//
				// Got a new reference different from
				// the old one.
				//
				continue;
			}
		}
		if (err != 0) {
			delete data_ptr;
			return (EFAULT);
		}
		ASSERT(data_ptr->length() > 0);
		ASSERT(data_ptr->length() <= cn);
		cn -= data_ptr->length();
		driverbuf += data_ptr->length();
		userbuf += data_ptr->length();
	}
	// Done with data_ptr.
	delete data_ptr;
	return (0);
}

//
// mc_copyin is like mc_xcopyin but it return -1 instead of an errno.
//
static int
mc_copyin(const void *uaddr, void *kaddr, size_t cn)
{
	return (mc_xcopyin(uaddr, kaddr, cn) ? -1 : 0);
}

static int
mc_copyout(const void *kaddr, void *uaddr, size_t cn)
{
	return (mc_xcopyout(kaddr, uaddr, cn) ? -1 : 0);
}

//
// Copy string in from user address to kernel address driverbuf.  'maxlength'
// is the maximum number of bytes to copy.  'lencopied' returns the number
// of bytes actually copied, including the terminating 0.  If the string
// doesn't fit in maxlength bytes, return ENAMETOOLONG.  If we can't find an
// addrspace object, things are bad, but we'll return ENODEV.
//
static int
mc_copyinstr(const char *uaddr, char *kaddr, size_t maxlength,
	size_t *lencopied)
{
	// We have been called by the copyinstr code to perform the
	// network op.

	int status;
	copy_args *args = copy::getcontext();
	ASSERT(args != (copy_args *)-1);
	ASSERT(args != (copy_args *)-2);
	ASSERT(args != NULL);

	Environment e;
	CORBA::Exception *ex;
	if (maxlength == 0) {
		return (EFAULT);
	}

	if (args->pid == 0) {
		if (args->nodeid == orb_conf::node_number()) {
			//
			// A pid of zero is used to replay ioctls from the
			// local kernel during a switchover/failover.  At this
			// point, the data is already in the kernel and a
			// bcopy is in order.
			//
			size_t len = strlen(uaddr) + 1;
			if (maxlength < len) {
				return (ENAMETOOLONG);
			}
			bcopy(uaddr, kaddr, len);
			return (0);
		} else {
			//
			// This path (kernel making an ioctl call on a remote
			// node) has no callers yet, and it is unclear whether
			// we need to implement it.
			// For now, we have a panic in here to catch any
			// callers.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: Invalid copyargs: node %d pid %d",
			    args->nodeid, args->pid);
		}
	}

	addrspc::addrspace_var addrspace_obj = getAddrspc(args->nodeid);
	if (CORBA::is_nil(addrspace_obj)) {
		return (ENODEV);
	}
	addrspc::ioctldata_t *data_ptr;
	if (maxlength > xfersize(maxlength)) {
		// XXX it should handle long strings.

		//
		// SCMSGS
		// @explanation
		// The system attempted to copy a string from user space to
		// the kernel. The maximum string length exceeds length limit.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: copyinstr: max string length %d too long",
		    maxlength);
		// maxlength = xfersize(maxlength);
	}
	do {
		status = addrspace_obj->copyinstr(args->pid,
		    (sol::uintptr_t)(long)uaddr, (sol::size_t)maxlength,
		    data_ptr, e);
		if ((ex = e.exception()) != NULL) {
			//
			// There are no user exceptions defined for
			// this interface, so this is a system
			// exception (e.g., the node is down or has
			// rebooted).  Since we're caching the
			// addrspace reference, we may have a stale
			// reference and need a new one.
			//
			ASSERT(e.sys_exception() != NULL);
			e.clear();

			// Releases old reference in addrspace_obj.
			addrspace_obj = checkAddrspc(args->nodeid,
			    addrspace_obj);
			if (CORBA::is_nil(addrspace_obj)) {
				// No new reference.
				return (ENODEV);
			}
			//
			// Got a new reference different from
			// the old one.  Fall through and repeat
			// the loop.
			//
		}
	} while (ex != NULL);

	if (status == EFAULT || status == ENAMETOOLONG) {
		delete data_ptr;
		return (status);
	}
	if (lencopied != NULL) {
		*lencopied = data_ptr->length();
	}

	bcopy(data_ptr->buffer(), kaddr, (size_t)data_ptr->length());
	delete data_ptr;
	return (status);
}

static int
mc_copyoutstr(const char *kaddr, char *uaddr,
	size_t maxlength, size_t *lencopied)
{
	// We have been called by the copyoutstr code to perform the
	// network op.

	int status = 0, status2;

	if (maxlength == 0) {
		return (EFAULT);
	}
	size_t len;
	for (len = 1; kaddr[len-1] != '\0'; len++) {
		if (len >= maxlength) {
			status = ENAMETOOLONG;
			break;
		}
	}
	//
	// Invariant: len <= maxlength and kaddr[len - 1] == '\0'
	// or len == maxlength, status = ENAMETOOLONG
	// To preserve Solaris semantics, we copy out maxlength
	// bytes even if we have ENAMETOOLONG
	//
	if (lencopied != 0) {
		*lencopied = len;
	}
	status2 = mc_copyout(kaddr, uaddr, len);
	if (status2 != 0) {
		return (EFAULT);
	} else {
		return (status);
	}
}

static int
mc_fuword8(const void *uaddr, uint8_t *kaddr)
{
	return (mc_xcopyin(uaddr, kaddr, (size_t)1) ? -1 : 0);
}

static int
mc_fuiword8(const void *uaddr, uint8_t *kaddr)
{
	return (mc_xcopyin(uaddr, kaddr, (size_t)1) ? -1 : 0);
}

static int
mc_fuword16(const void *uaddr, uint16_t *kaddr)
{
	return (mc_xcopyin(uaddr, kaddr, (size_t)2) ? -1 : 0);
}

static int
mc_fuword32(const void *uaddr, uint32_t *kaddr)
{
	return (mc_xcopyin(uaddr, kaddr, (size_t)4) ? -1 : 0);
}

static int
mc_fuiword32(const void *uaddr, uint32_t *kaddr)
{
	return (mc_xcopyin(uaddr, kaddr, (size_t)4) ? -1 : 0);
}

static int
mc_fuword64(const void *uaddr, uint64_t *kaddr)
{
	return (mc_xcopyin(uaddr, kaddr, (size_t)8) ? -1 : 0);
}

static int
mc_suword8(void *uaddr, uint8_t val)
{
	return (mc_xcopyout(&val, uaddr, sizeof (val)) ? -1 : 0);
}

static int
mc_suiword8(void *uaddr, uint8_t val)
{
	return (mc_xcopyout(&val, uaddr, sizeof (val)) ? -1 : 0);
}

static int
mc_suword16(void *uaddr, uint16_t val)
{
	return (mc_xcopyout(&val, uaddr, sizeof (val)) ? -1 : 0);
}

static int
mc_suword32(void *uaddr, uint32_t val)
{
	return (mc_xcopyout(&val, uaddr, sizeof (val)) ? -1 : 0);
}

static int
mc_suiword32(void *uaddr, uint32_t val)
{
	return (mc_xcopyout(&val, uaddr, sizeof (val)) ? -1 : 0);
}

static int
mc_suword64(void *uaddr, uint64_t val)
{
	return (mc_xcopyout(&val, uaddr, sizeof (val)) ? -1 : 0);
}

//
// Initialize a transport server thread to use the mc functions for
// copyin/out.
//
extern "C" void
set_server_copyops()
{
	install_copyops(curthread, &mc_copyops);
}
