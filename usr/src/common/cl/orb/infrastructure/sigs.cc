//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sigs.cc	1.31	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/sigs.h>
#include <orb/buffers/marshalstream.h>
#include <orb/invo/io_invos.h>
#include <orb/flow/resource.h>
#include <orb/msg/orb_msg.h>

//
// routines to implement signal messaging and receiving
//

//
// signal_send - send a signal from the client to the server for this slot.
//
void
signals::signal_send(ID_node &node, slot_t slot)
{
	send_message(SIGNAL, node, slot);
}

//
// cancel_send - the client tells the server to cancel any pending signal
// processing for this slot.
//
void
signals::cancel_send(ID_node &node, slot_t slot)
{
	send_message(CANCEL, node, slot);

}

//
// finish_send - the server tells the client that the server has processed
// the signal. Normally this information is piggybacked on the reply message.
// But this message is needed when the reply was sent before
// the signal arrived.
//
void
signals::finish_send(ID_node &node, slot_t slot)
{
	send_message(FINISH, node, slot);
}

void
signals::handle_messages(recstream *re)
{
	// recstream *re -
	//	Servers normally delete within handle_message.
	//	Clients delete with the invocation structure.

	ID_node		&src_node = re->get_src_node();

	int32_t		signal_msg_type = re->get_long();

	switch (signal_msg_type) {
	case SIGNAL:
		inbound_invo::signal(src_node, re->get_unsigned_short());
		break;

	case CANCEL:
		inbound_invo::cancel(src_node, re->get_unsigned_short());
		break;

	case FINISH:
		outbound_invo_table::finished(src_node,
		    re->get_unsigned_short());
		break;

	default:
		//
		// SCMSGS
		// @explanation
		// The system has received a signals message of unknown type.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: unknown type of signals message %d",
		    signal_msg_type);
	}
	re->done();
}

//
// send_message - transmit a signal message
//
void
signals::send_message(int message, ID_node &node, slot_t slot)
{
	Environment	e;
	bool		retry_needed;

	do {

		// Identify message resources
		resources_datagram	resource(&e,
		    0,						// header size
		    (uint_t)(sizeof (int) + sizeof (slot_t)),	// data size
		    node, SIGNAL_MSG);

		//
		// Check whether message can proceed now.
		// The send stream is allocated when the msg can proceed.
		//
		sendstream *send_stream = orb_msg::reserve_resources(&resource);
		if (send_stream == NULL) {
			// This should only occur when the node is dead.
			ASSERT(e.exception());
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			return;
		}

		// Marshal the message
		send_stream->put_long(message);
		send_stream->put_unsigned_short(slot);

		retry_needed = orb_msg::send_datagram(send_stream);

	} while (retry_needed);
}
