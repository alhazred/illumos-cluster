/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLUSTERPROC_H
#define	_CLUSTERPROC_H

#pragma ident	"@(#)clusterproc.h	1.14	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef _KERNEL
#define	STACK_SIZE	16384
#include <sys/thread.h>
void clsendselfsig(void);
void clclearlwpsig(void);
int clnewlwp(void (*)(void *), void *, int, const char *, klwp_t **);
int clnewlwp_stack(void (*)(void *), void *, int, const char *, klwp_t **,
	size_t);

/* global variables */
extern id_t	cl_syscid;	/* Our copy of syscid */

#else

#include <thread.h>
void clsendselfsig(void);
void clclearlwpsig(void);
int clnewlwp(void (*)(void *), void *, int, const char *, os::threadid_t *);
int clnewlwp_stack(void (*)(void *), void *, int, const char *,
    os::threadid_t *, size_t);
int getcid(char *, id_t *);
#endif

void clsendlwpsig(os::threadid_t t);

#ifdef	__cplusplus
}
#endif

#endif	/* _CLUSTERPROC_H */
