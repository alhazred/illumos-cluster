/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)freeze_lock.cc 1.13     08/05/20 SMI"

#include <orb/infrastructure/freeze_lock.h>

// Non-blocking invo() attempt
// Again, timing of block_now and fail aren't guarenteed.

bool
freeze_lock::try_hold(void)
{
	if (!frl_rw.try_rdlock()) {
		return (false);
	}
	if (flags & (FL_FREEZE_HELD | FL_BLOCK_HOLDS | FL_FAIL_HOLDS)) {
		frl_rw.unlock();
		return (false);
	}
	return (true);
}


// When we freeze, the service is already frozen by someone else, or it
// isn't, so when we leave wrlock, all other hold()s have either finished
// or are waiting behind us, so we just have to wait for the (possible)
// existing write lock to finish.

void
freeze_lock::freeze()
{
	frl_rw.wrlock();
	frl_lock.lock();
	freeze_count++;
#ifdef	DEBUG
	last_freezer = os::threadid();
#endif
	flags |= FL_FREEZE_HELD;
	// Remove blocking so we can assert in unfreeze
	flags &= ~FL_BLOCK_HOLDS;
	frl_lock.unlock();
	frl_rw.unlock();
}

// Increment the freeze count, but only if we start out unfrozen

void
freeze_lock::freeze_if_unfrozen()
{
	frl_rw.wrlock();
	frl_lock.lock();
	if (!freeze_count) {
#ifdef	DEBUG
		last_freezer = os::threadid();
#endif
		freeze_count++;
		flags |= FL_FREEZE_HELD;
		// Remove blocking so we can assert in unfreeze
		flags &= ~FL_BLOCK_HOLDS;
	}
	frl_lock.unlock();
	frl_rw.unlock();
}

// Similar to above, but any possible blocking (other than grabbing
// the mutex) results in failure.

bool
freeze_lock::try_freeze()
{
	// Every new reader grabs the read lock on frl_rw.  If the lock
	// isn't frozen, they'll continue to hold it.  If it isn't, they'll
	// drop it and wait on frl_cv.  However, because the read lock can
	// be held at any time, frl_rw.try_wrlock can fail at any time.
	// So below we first see if the lock is frozen, and if it is we
	// just add this freezer to the list.  If it isn't, then we try to
	// grab the write lock and freeze the old fashioned way.

	frl_lock.lock();

	if (flags & FL_FREEZE_HELD) {
#ifdef	DEBUG
		last_freezer = os::threadid();
#endif
		freeze_count++;
		// Remove blocking so we can assert in unfreeze
		flags &= ~FL_BLOCK_HOLDS;
		frl_lock.unlock();
		return (true);
	}

	ASSERT(freeze_count == 0);

	if (frl_rw.try_wrlock()) {
		flags |= FL_FREEZE_HELD;
		// Remove blocking so we can assert in unfreeze
		flags &= ~FL_BLOCK_HOLDS;
#ifdef	DEBUG
		last_freezer = os::threadid();
#endif
		freeze_count = 1;
		frl_rw.unlock();
		frl_lock.unlock();
		return (true);
	}

	frl_lock.unlock();
	return (false);
}

// Here we decrement the freeze_count, and remove the freeze and broadcast
// to waiting holders if it has gone to zero

void
freeze_lock::unfreeze()
{
	ASSERT(flags & FL_FREEZE_HELD);
	ASSERT(freeze_count > 0);

	frl_lock.lock();
	ASSERT(!(flags & FL_BLOCK_HOLDS));

	freeze_count--;

	if (freeze_count == 0) {
		flags &= ~FL_FREEZE_HELD;
		frl_cv.broadcast();
	}

	frl_lock.unlock();
}

// A more tolerant unfreeze that doesn't assert we are already frozen
// We still assert that we're not blocked.

void
freeze_lock::unfreeze_if_frozen()
{
	frl_lock.lock();
	if (freeze_count > 0) {
		ASSERT(!(flags & FL_BLOCK_HOLDS));
		freeze_count--;

		if (freeze_count == 0) {
			flags &= ~FL_FREEZE_HELD;
			frl_cv.broadcast();
		}
	}
	frl_lock.unlock();
}


// This is a private function that implements the rest of hold() (so
// the common path of hold() can be inlined).  If the lock is set to fail
// we just drop the read lock we got in hold() and return false.
//
// Whenever the read lock on frl_rw is aquired, FL_FREEZE_HELD is either
// true or false, and that status cannot change until the read lock is
// dropped.  This is because a potential freezer must aquire the write
// lock on frl_rw.  So, when frl_rw.rdlock() succeeds, we can check the
// FL_FREEZE_HELD flag at our leisure.  There is a race condition with
// FL_BLOCK_HOLDS, in that this function can exit with true when
// FL_BLOCK_HOLDS is set to true, but calls to block_now() (which sets
// this flag) have no guarenteed timing.
//
// The inner while loop simply avoids doing extra work when we know it
// would be pointless

bool
freeze_lock::wait_for_unfreeze()
{
#ifdef _KERNEL
	// Only available in the kernel version of rwlock
	ASSERT(frl_rw.lock_held());
#endif

	if (flags & FL_FAIL_HOLDS) {
		frl_rw.unlock();
		return (false);
	}

	do {
#ifdef _KERNEL
		// Only available in the kernel version of rwlock
		ASSERT(frl_rw.lock_held());
#endif
		frl_rw.unlock();
		frl_lock.lock();

		while ((flags & (FL_FREEZE_HELD | FL_BLOCK_HOLDS)) &&
		    ! (flags & FL_FAIL_HOLDS))
			frl_cv.wait(&frl_lock);

		frl_lock.unlock();

		if (flags & FL_FAIL_HOLDS)
			return (false);

		frl_rw.rdlock();

	} while (flags & (FL_FREEZE_HELD | FL_BLOCK_HOLDS));

	return (true);
}
