/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _COMMON_THREADPOOL_H
#define	_COMMON_THREADPOOL_H

#pragma ident	"@(#)common_threadpool.h	1.7	08/05/20 SMI"

#include <sys/threadpool.h>
#ifndef	_KERNEL_ORB
#include <orb/infrastructure/orb.h>
#endif // _KERNEL_ORB

//
// The common_threadpool is available for anyone to defer tasks when
// a job cannot be handled in the current context. The threadpool creates
// threads on demand with no max limit.
// See sys/threadpool.h on how to use a threadpool
// Use common_threadpool.defer_processing() to add a task
//

class common_threadpool {
public:
	static int initialize();
	static void shutdown();
	static threadpool &the();
private:
	static threadpool *the_common_threadpool;
};

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
//
// Any thread that starts an IDL invocation from inside a non-global zone
// uses this threadpool to hand-off the task at the gateway_handler level.
// This is done to ensure that the invocation, when running ORB kernel code,
// actually executes in the kernel (global zone). If we do not do the
// hand-off, the entire invocation is done in the same thread context; which
// when running in non-global zone, might not have necessary privileges.
// As a result, the invocation might not succeed.
//
// This threadpool is very similar to the common_threadpool declared above.
//
class zone_threadpool {
public:
	static int initialize();
	static void shutdown();
	static threadpool &the();
private:
	static threadpool *the_zone_threadpool;
};
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

#include <orb/infrastructure/common_threadpool_in.h>

#endif	/* _COMMON_THREADPOOL_H */
