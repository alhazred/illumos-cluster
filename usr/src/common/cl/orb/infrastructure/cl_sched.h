/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 *  Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 *  Use is subject to license terms.
 */

#ifndef _CL_SCHED_H
#define	_CL_SCHED_H

#pragma ident	"@(#)cl_sched.h	1.5	08/07/17 SMI"

#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/tspriocntl.h>
#include <sys/fsspriocntl.h>
#include <sys/fxpriocntl.h>
#include <sys/sol_version.h>

#ifdef _KERNEL

#if SOL_VERSION >= __s9
#define	S9_RT_PARAMS
#endif

#if SOL_VERSION < __s9
#define	PRE_S9
#endif

#ifdef	S9_RT_PARAMS

#include <sys/rt.h>
#include <sys/fx.h>

/*
 * control flags: Copied from /ws/on81-gate/usr/src/uts/common/disp/rt.c
 */
#define	RT_DOPRI	0x01    /* change priority */
#define	RT_DOTQ		0x02    /* change RT time quantum */
#define	RT_DOSIG	0x04    /* change RT time quantum signal */

/*
 * Control flags: Copied from /ws/on10-gate/usr/src/uts/common/disp/fx.c
 */
#define	FX_DOUPRILIM	0x01    /* change user priority limit */
#define	FX_DOUPRI	0x02    /* change user priority */
#define	FX_DOTQ		0x04    /* change time quantum */

#endif /* S9_RT_PARMS */
#endif /* _KERNEL */

#endif	/* _CL_SCHED_H */
