//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb.cc	1.91	08/10/13 SMI"

// Common for kernel, unode and userland

#include <orb/invo/common.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/common_threadpool.h>
#include <orb/object/adapter.h>
#include <orb/handler/handler.h>
#include <repl/service/repl_service.h>
#include <sys/clconf_int.h>
#include <sys/cl_comm_support.h>

#include <clconf/clconf_ccr.h>
#include <clconf/clnode.h>
#include <cmm/cmm_impl.h>
#include <nslib/data_container_handler.h>
#include <nslib/binding_iter_handler.h>
#include <orb/handler/nil_handler.h>
#include <repl/service/replica_handler.h>

#ifdef _KERNEL_ORB
// Begin Kernel and Unode
#include <orb/msg/message_mgr.h>
#include <orb/object/idlversion_impl.h>
#include <cmm/ff_impl.h>
#include <orb/debug/orb_stats_mgr.h>
#include <orb/refs/refcount.h>
#include <orb/flow/resource_mgr.h>
#include <orb/flow/resource_blocker.h>
#include <orb/infrastructure/common_threadpool.h>
#include <h/cmm.h>
#include <cmm/cmm_ns.h>
#include <quorum/common/quorum_impl.h>
#include <quorum/common/type_registry.h>
#include <orb/member/cmm_callback_impl.h>
#include <repl/repl_mgr/repl_mgr_prov_impl.h>
#include <orb/transport/path_manager.h>
#include <vm/vm_lca_impl.h>
#include <vm/vm_comm.h>
#include <repl/rma/rma.h>
#include <repl/rmm/rmm.h>
#include <repl/service/transaction_state.h>
#include <repl/service/ckpt_handler.h>
#include <repl/service/replica_tmpl.h>
#include <orb/msg/orb_msg.h>
#include <cmm/cmm_comm_impl.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/transport/nil_endpoint.h>
#include <nslib/ns.h>
#include <ccr/ccr_ds.h>
#include <ccr/ccr_tm.h>
#include <tm/kernel_tm.h>
#include <clconf/comp_state_reg.h>
#include <repl/service/repl_tid_impl.h>
#include <orb/xdoor/translate_mgr.h>

#ifdef	linux
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#endif

#ifdef _FAULT_INJECTION
#include <sys/mc_sema_impl.h>
#endif

#ifdef _KERNEL

// Begin Kernel only

#include <sys/modctl.h>
#include <orb/infrastructure/addrspace_impl.h>
#include <orb/ip/ipconf.h>
#include <orb/ip/fpconf.h>
#include <orb/monitor/monitor.h>

fpconf *fpc;

// End Kernel only

#endif // _KERNEL

#if !defined(_KERNEL) && defined(_KERNEL_ORB)

#include <unode/unode.h>

#endif // !defined(_KERNEL) && defined(_KERNEL_ORB)

#else

// Begin User land only

#include <unistd.h>
#include <sys/resource.h>
#include <orb/xdoor/sxdoor.h>

pid_t 		ORB::creator_pid = 0;

// End User land only

#endif // _KERNEL_ORB

//
// The following includes and externs are needed to satisfy the
// external function pointer used by modules external to cl_comm.
//
#include <sys/rm_util.h>

extern "C" idlversion_ptr get_idlversion_impl(nodeid_t);
#ifdef _KERNEL_ORB
extern "C" int clconfig_clmode(int, void *);
#if defined(_FAULT_INJECTION)
extern "C" uint_t num_endpoints(nodeid_t);
extern "C" void fi_do_reboot(bool);
#endif // _FAULT_INJECTION
extern "C" void cl_wait_until_orb_is_up(void);
#endif // _KERNEL_ORB

// Suppress lint complaint, since this funtion is not called explicitly

static void
orb_shutdown(void) //lint -e528
{
	ORB::shutdown();
}

// set ORB::shutdown to be the fini routine for this module
#pragma fini(orb_shutdown)

int
#ifdef	_KERNEL
ORB::initialize()
#else
ORB::initialize(bool increase_file_limit)
#endif	/* _KERNEL */
{

#ifndef _KERNEL_ORB
	//
	// Currently we require user programs that link with the ORB to
	// be run as superuser or setuid
	//
	if (geteuid() != 0) {
		return (EPERM);
	}
#endif // not defined _KERNEL_ORB

	int err;

	//
	// ORB::initialize may be called as often as you like,
	// but it will only be run once. If another thread is either
	// INITIALIZING or HALTING orb, we should wait here.
	//
	state_lock.lock();
	while ((orb_state == ORB::INITIALIZING) ||
	    (orb_state == ORB::HALTING)) {
		_orb_state_change_cv.wait(&state_lock);
	}

	if (orb_state == ORB::ACTIVE || orb_state == ORB::SERVER_ACTIVE) {
		// ORB has been initialized, return success
		state_lock.unlock();
		return (0);
	}

	orb_state = ORB::INITIALIZING;
	state_lock.unlock();

	// Initialize orb_syslog so that later modules can log messages
	if (!orb_syslog_msgp) {
		orb_syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_FRAMEWORK_TAG,
		    SC_SYSLOG_FRAMEWORK_RSRC, NULL);
		if (!orb_syslog_msgp) {
			state_lock.lock();
			orb_state = ORB::UNINIT;
			_orb_state_change_cv.broadcast();
			state_lock.unlock();
			return (ENOMEM);
		}
	}

	//
	// This operation will fail when the node is not being booted into
	// cluster mode. Some programs will try to initialize the orb
	// as a means of determining whether the node is part of a cluster.
	//
	if ((err = orb_conf::configure()) != 0) {
		ORB::shutdown();
		return (err);
	}


#ifndef	_KERNEL_ORB
	// Store pid of the process that initializes orb.
	creator_pid = getpid();
#endif

#if defined(_KERNEL) && !defined(_KERNEL_ORB)
	// Initialize cl_syscid for functions in clusterproc.c to use.
	if ((err = getcid("SYS", &cl_syscid)) != 0) {
		ORB::shutdown();
		return (err);
	}
#endif

	//
	// Originally, cl_comm comprised most of cluster kernel
	// components in one very large kernel module. As a result
	// of it's size, DTrace was unable to trace functions contained
	// within cl_comm because the text size was greater than 2MB.
	// In order to split up cl_comm into smaller more modular
	// kernel modules it became necessary to utilize function
	// pointers to resolve any undefined symbols that arose and
	// to remove the circular dependencies among the new modules.
	// Those function pointer are initialized here just prior
	// to initializing the ORB and joining the cluster.
	//


#ifdef _KERNEL_ORB
	//
	// The following function pointers are called only from
	// kernel orb components and are required to remove the
	// dependencies from lower level kernel modules to those
	// that are loaded later in the boot process.
	//
	// For example:
	// clconf_obj_release_funcp() is called from the cl_orb
	// since clconf_obj_release is not defined when that module loads.
	//
	clconfig_clmode_funcp = clconfig_clmode;
	endpoint_reserve_resources_funcp = endpoint_reserve_resources;
	vm_comm_handle_messages_funcp = vm_comm_handle_messages;
	get_transition_thread_id_funcp = get_transition_thread_id;
	make_recstream_funcp = make_recstream;
	clconf_cluster_get_current_funcp = clconf_cluster_get_current;
	clconf_obj_get_child_funcp = clconf_obj_get_child;
	clconf_obj_release_funcp = clconf_obj_release;
	cl_wait_until_orb_is_up_funcp = cl_wait_until_orb_is_up;
	kernel_tm_callback_funcp = kernel_tm_callback;
	pm_add_path_funcp = pm_add_path;
	pm_remove_path_funcp = pm_remove_path;
	pm_enable_funcp = pm_enable;
	pm_disable_funcp = pm_disable;
	pm_get_max_delay_funcp = pm_get_max_delay;
	pm_register_cmm_funcp = pm_register_cmm;
	pm_unregister_cmm_funcp = pm_unregister_cmm;

#if defined(_FAULT_INJECTION)
	rmm_primary_node_funcp = rmm_primary_node;
	num_endpoints_funcp = num_endpoints;
	fault_revoke_pathend_funcp = fault_revoke_pathend;
	fi_do_reboot_funcp = fi_do_reboot;
	fi_cmm_reboot_funcp = fi_cmm_reboot;
#endif // _FAULT_INJECTION
#endif // _KERNEL_ORB

#ifdef _KERNEL
	//
	// cluster_get_quorum_status_funcp is called from
	// FaultFunctions::reboot_once(). This invocation is only
	// called from the kernel.
	//
	cluster_get_quorum_status_funcp = cluster_get_quorum_status;

	//
	// Both ipconf_get_physical_ip_funcp and ipconf_is_initialized_funcp
	// are called from clconf_get_physical_ip.  Since the calling function
	// is only invoked when _KERNEL is defined, these functions maintain
	// the same restriction.
	//
	ipconf_get_physical_ip_funcp = ipconf_get_physical_ip;
	ipconf_is_initialized_funcp = ipconf_is_initialized;

#if SOL_VERSION >= __s10
	zc_getidbyname_funcp = data_server_impl::get_zcid;
#endif // SOL_VERSION >= __S10
#endif // _KERNEL

	//
	// The following function pointers are called by both kernel and
	// userland components so they must be initialized here.
	//
	get_idlversion_impl_funcp = get_idlversion_impl;
	root_nameserver_funcp = root_nameserver;
	root_nameserver_root_funcp = root_nameserver_root;
	local_nameserver_funcp = local_nameserver;
	local_nameserver_root_funcp = local_nameserver_root;
	get_rm_funcp = get_rm;

	if ((err = handler_repository::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = initialize_handler_kits()) != 0) {
		ORB::shutdown();
		return (err);
	}

#ifdef _KERNEL_ORB

	if ((err = orbthreadlist::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
	// Create the component registry
	if ((err = component_state_reg::create_registry()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = lone_membership::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
#endif	// _KERNEL_ORB

#ifndef _KERNEL
	// XX Should move to a separate routine
	//
	// ORB processes tend to use lots of file descriptors.  Set the
	// limits as high as possible.  For a privileged process, if requested,
	// set soft and hard limits to "no limit"; otherwise set the soft limit
	// to the current hard limit (generally 1024).
	//
	struct rlimit fdlim;

	err = 0;
#ifndef	linux
	if (increase_file_limit && (geteuid() == 0)) {
		fdlim.rlim_cur = (rlim_t)RLIM_INFINITY;
		fdlim.rlim_max = (rlim_t)RLIM_INFINITY;
	} else
#endif	// linux
	{
		if (getrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
			err = errno;
#ifdef DEBUG
			//
			// SCMSGS
			// @explanation
			// During cluster initialization within this user
			// process, the getrlimit call failed with the
			// specified error.
			// @user_action
			// Read the man page for getrlimit for a more detailed
			// description of the error.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "clcomm: getrlimit(RLIMIT_NOFILE): %s",
			    strerror(errno));
#endif
			ORB::shutdown();
			return (err);
		}
		fdlim.rlim_cur = fdlim.rlim_max;
	}

	if (err == 0 && setrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		err = errno;
#ifdef DEBUG
		//
		// SCMSGS
		// @explanation
		// During cluster initialization within this user process, the
		// setrlimit call failed with the specified error.
		// @user_action
		// Read the man page for setrlimit for a more detailed
		// description of the error.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: setrlimit(RLIMIT_NOFILE): %s", strerror(errno));
#endif
		ORB::shutdown();
		return (err);
	}

#endif	// not defined _KERNEL

#ifdef	_KERNEL_ORB

	// Start the unref_threadpool threads. Cannot do object unref till now
	if ((err = unref_threadpool::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	//
	// Create the common_threadpool and start up its threads.
	// Can't defer tasks until now.
	//
	if ((err = common_threadpool::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	if ((err = zone_threadpool::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
#endif

	// Initialize rxdoor membership manager
	if ((err = rxdoor_membership_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Start the rxdoor manager
	if ((err = rxdoor_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
	// Initialize Xdoor translate manager.
	if ((err = translate_mgr::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Start the endpoint registry
	if ((err = endpoint_registry::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Start the nil_endpoint (for the local transport path)
	if ((err = nil_endpoint::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Start the service threads.
	if ((err = orb_msg::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = resource_blocker::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
	if ((err = resource_mgr::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = refcount::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = orb_stats_mgr::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = rma::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Initialize local callback agent in the version manager framework
	if ((err = vm_lca_impl::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = hxdoor_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	if ((err = repl_service_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Allocate an HA RM provider (or test provider) and start up the rmm.
	repl_mgr_prov_impl *_p = new repl_mgr_prov_impl("ha repl_mgr");
	ASSERT(_p);
	replica_int::handler_repl_prov_ptr test_provp = _p->register_for_rmm();
	replica::checkpoint_ptr test_ckpt = _p->get_ckpt()->get_objref();

	err = rmm::initialize(test_provp, test_ckpt, "repl_mgr");
	CORBA::release(test_provp);
	CORBA::release(test_ckpt);
	if (err != 0) {
		ORB::shutdown();
		return (err);
	}

	// Start CCR data server read-only
	if ((err = initialize_ccr_ds()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// initialize clconf
	if ((err = cl_current_tree::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// construct clconf
	if ((err = clconf_ccr::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

#ifdef _FAULT_INJECTION
	// Initialize boot time fault points (K and UN only)
	BootTriggers::load_faults();
#endif // _FAULT_INJECTION

	// The system has started the ORB
	state_lock.lock();
	orb_state = ORB::ORB_UP;
	_orb_state_change_cv.broadcast();
	state_lock.unlock();

	// Retrieve the initial cmm parameters from the ccr
	cmm_impl::initialize_config();

	// Initialize the failfast facility.
	if ((err = ff_admin_impl::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Initialize the type registry implementation.
	if ((err = device_type_registry_impl::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Initialize the quorum algorithm implementation.
	if ((err = quorum_algorithm_impl::quorum_algorithm_init()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Initialize the version manager
	if ((err = vm_comm::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Initialize the Transport Component Registry
	if ((err = path_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Initialize the message_mgr.
	message_mgr::initialize();

	if ((err = kernel_tm::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

#ifdef _KERNEL
	//
	// The memory monitor should be started after most of
	// the system has initialized (consumed memory) and
	// before joining the cluster and allowing application work loads.
	//
	if ((err = monitor::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
#endif

#ifdef _KERNEL_ORB
	//
	// Create the idlversion object before joining the
	// cluster. This way if existing cluster members need type
	// information of new types on this node, they can query the
	// idlversion object. If any userland components have started,
	// they too can access the local idlversion object.
	//
	if ((err = idlversion_impl::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
#endif //  _KERNEL_ORB

	if ((err = cmm_comm_impl::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	// Joins the cluster.
	err = ORB::join_cluster();

	if (err) {
		//
		// SCMSGS
		// @explanation
		// The local node was unsuccessful in joining the cluster.
		// @user_action
		// There may be other related messages on this node that may
		// indicate the cause of this failure. Resolve the problem and
		// reboot the node.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Failed to join the cluster: error = %d.", err);

		// tell the sender_thread to clean up and exit
		cmm_comm_impl::shutdown();
		ORB::shutdown();
		return (err);
	}

#ifdef _KERNEL
	//
	// Load the RSM KA interface module
	// Any error in loading will result in a return of -1; this will
	// generally be the result of the driver not existing, and since
	// this is an unbundled driver, an error return is not a concern.
	//
	(void) modload("drv", "clif_rsm");

	//
	// Load the RSMRDT interface module
	// Any error in loading will result in a return of -1; this will
	// generally be the result of the driver not existing, and since
	// this is an unbundled driver, an error return is not a concern.
	//
	(void) modload("misc", "clif_rsmrdt");


	// Initialize the network fastpath hook.
	fpc = new fpconf;


	// Register with nameserver to get remote macaddr
	ASSERT(fpc);
	fpc->register_with_ns();

	// Initialize the IP failover component.
	if ((err = ipconf::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	//
	// In kernel and unode code, upgrade the orb state to SERVER_ACTIVE
	// at this point
	//

	state_lock.lock();
	orb_state = ORB::SERVER_ACTIVE;
	_orb_state_change_cv.broadcast();
	state_lock.unlock();

#endif // _KERNEL

#else // _KERNEL_ORB

	// Initialize sxdoor table
	if ((err = sxdoor_table::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}
	// Initialize sxdoor manager
	if ((err = sxdoor_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	//
	// Verify for user-land programs that link with the ORB that
	// the name server is accessible and running
	// with an operational kernel orb.
	//
	// This usually verifies that the program in question has permission to
	// access the object framework.
	//
	// This test also detects an attempt to perform orb operations in a
	// user process before the kernel orb has initialized.
	// There are user level programs, such as those supporting
	// global devices, that run before the kernel orb has initialized.
	// These user level programs then use non-orb based means to
	// accomplish their functions when the orb is not ready.
	//
	naming::naming_context_var ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
		ORB::shutdown();
		return (EPERM);
	}

	if ((err = repl_service_manager::initialize()) != 0) {
		ORB::shutdown();
		return (err);
	}

	state_lock.lock();
	orb_state = ORB::ACTIVE;
	_orb_state_change_cv.broadcast();
	state_lock.unlock();

#endif // _KERNEL_ORB

	return (0);
}

//
// Function to initialize all the handler kits defined in cl_comm.
// Called from ORB::initialize.
//
int
ORB::initialize_handler_kits()
{
	int err;
	// initialize all handler kit's.
	if ((err = standard_handler_kit::initialize()) != 0) {
		return (err);
	}

	if ((err = combo_handler_kit::initialize()) != 0) {
		return (err);
	}

	if ((err = binding_iter_handler_kit::initialize()) != 0) {
		return (err);
	}

	if ((err = data_container_handler_kit::initialize()) != 0) {
		return (err);
	}

	if ((err = nil_handler_kit::initialize()) != 0) {
		return (err);
	}

	if ((err = replica_handler_kit::initialize()) != 0) {
		return (err);
	}
#ifdef _KERNEL_ORB
	if ((err = ckpt_handler_kit::initialize()) != 0) {
		return (err);
	}
#endif
	return (0);

}

//
// This method is used only by userland ORB users to initialize services
// that are needed in order for them to create ORB server objects. User
// processes that only act as clients can be single threaded, while user
// processes that contain server objects must be multi-threaded.
//
#ifndef	_KERNEL_ORB
int
ORB::server_initialize()
{
	int retval;

	if (thr_main() == -1) { // is libthread linked?
		os::panic("clcomm::cannot start server clcomm without "
			"thread support\n");
		return (ENOTSUP);
	}

	state_lock.lock();
	if (orb_state == ORB::SERVER_ACTIVE) {
		state_lock.unlock();
		return (0);
	}
	if (orb_state != ORB::ACTIVE) {
		state_lock.unlock();
		return (ENOTSUP);
	}
	// Start the unref threadpool
	if ((retval = unref_threadpool::initialize()) != 0) {
		state_lock.unlock();
		return (retval);
	}
	// Start the common threadpool
	if ((retval = common_threadpool::initialize()) != 0) {
		state_lock.unlock();
		return (retval);
	}
	orb_state = ORB::SERVER_ACTIVE;
	_orb_state_change_cv.broadcast();
	state_lock.unlock();

	return (0);
}
#endif	// _KERNEL_ORB


//
// Note that ORB::shutdown can be called even if ORB::initialize did not
// fully complete. So shutdown code should be written to be safe to complete
// even if the initialization did not complete.
// Alternatives are to change ORB::initialize to backout in the middle in
// a safe way doing partial shutdown if some function fails.
//
void
ORB::shutdown()
{
	state_lock.lock();
	while (orb_state == ORB::HALTING) {
		_orb_state_change_cv.wait(&state_lock);
	}
	//
	// Another thread could have called shutdown at the same time
	// and did the work.
	//
	if (orb_state == ORB::UNINIT) {
		// orb has been halted
		state_lock.unlock();
		return;
	}

#ifdef _KERNEL_ORB
	//
	// If the monitoring of the paths is disabled, we don't want to run
	// shutdown, as some of what is done below might make remote
	// invocations to servers that have died which could hang indefinitely
	//
	if (!(path_manager::is_initialized()) ||
	    !(path_manager::the().is_enabled())) {
		state_lock.unlock();
		return;
	}
#else	// _KERNEL_ORB

	//
	// Only the process that performed initialization can perform shutdown
	// if door servers have been started
	//
	if (creator_pid != getpid()) {
		if (orb_state == ORB::SERVER_ACTIVE) {
#ifdef DEBUG
			os::printf("clcomm: ignoring shutdown call by process "
			    "(pid = %d), not the process who initialized "
			    "(pid = %d).\n", getpid(), creator_pid);
#endif
			state_lock.unlock();
			return;
		}
	}
#endif	// _KERNEL_ORB

	ASSERT(!((orb_state == ORB::UNINIT) || (orb_state == ORB::HALTING)));
	orb_state = ORB::HALTING;
	state_lock.unlock();

#ifdef _KERNEL_ORB

#ifdef _KERNEL
	addrspace_impl::shutdown();
#endif // _KERNEL

	(void) cmm_callback_impl::shutdown(NULL, 0);

	//
	// Tell the endpoint registry to kill the connections to all nodes
	// We will not be able to communicate with any other node after this
	// We wait till after the CMM gets a chance to send out its messages
	// in cmm_callback_impl::shutdown which causes the other nodes to
	// reconfigure faster
	//
	endpoint_registry::the().set_membership(lone_membership::the());

	cmm_comm_impl::shutdown();
	ff_admin_impl::shutdown();
	kernel_tm::shutdown();
	orb_msg::shutdown();
	if (path_manager::is_initialized()) {

		path_manager::the().shutdown();
	}
	vm_comm::shutdown();
	clconf_ccr::shutdown();
	cl_current_tree::shutdown();
	(void) component_state_reg::shutdown_registry();
	//
	// Exit all the rest of the orb/CMM threads
	// XXXX When doing shutdown, need to check whether all the threads
	// are throwing away pending tasks. They need to wait till all pending
	// work is done.
	//
	orbthreadlist::shutdown();

#endif // _KERNEL_ORB
	//
	// The orbthreadlist must be shut down before these because they use
	// orbthreads
	//
	repl_service_manager::shutdown();

	// Wait for threads to finish and disable the unref_threadpool
	unref_threadpool::unref_shutdown();

#ifdef	_KERNEL_ORB
	refcount::shutdown();
	rxdoor_manager::shutdown();
	resource_mgr::shutdown();

	orb_stats_mgr::shutdown();

	resource_blocker::shutdown();

	translate_mgr::shutdown();

#ifdef _KERNEL
	monitor::shutdown();
#endif

	//
	// The endpoint_registry is shut down here because refcount send
	// messages (and it has the lone membership, so it isn't doing
	// anything anyway).
	//
	lone_membership::shutdown();
	nil_endpoint::shutdown();
	endpoint_registry::shutdown();

#endif // _KERNEL_ORB

	common_threadpool::shutdown();

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	zone_threadpool::shutdown();
#endif

	shutdown_handler_kits();

	handler_repository::shutdown();

	// Delete syslog msg object

	delete orb_syslog_msgp;
	orb_syslog_msgp = NULL;

#ifndef _KERNEL
	// dbg_print_buf_list is erased in ORB shutdown as lists need to be
	// empty before process shutdown.
	//
	dbg_print_buf::dbg_buf_erase_list();
#endif  // _KERNEL

	//
	// At this point the infrastructure is no longer initialized
	//
	state_lock.lock();
	orb_state = ORB::UNINIT;
	_orb_state_change_cv.broadcast();
	state_lock.unlock();
}


//
// Function to deallocate all the handler kits initialized in cl_comm.
// Called from ORB::shutdown.
//
void
ORB::shutdown_handler_kits()
{
	// Reset all handler kit's.
	standard_handler_kit::shutdown();

	combo_handler_kit::shutdown();

	binding_iter_handler_kit::shutdown();

	data_container_handler_kit::shutdown();

	nil_handler_kit::shutdown();

	replica_handler_kit::shutdown();

#ifdef _KERNEL_ORB
	ckpt_handler_kit::shutdown();
#endif

}
#ifdef _KERNEL_ORB

// This ORB is now ready for operation, but first it must join the cluster
// by forcing the CMM to reconfigure.
//
// This include any initialization not done in ORB::initialize because
// CMM is not up or transport is available
int
ORB::join_cluster()
{
	Environment e;
	int err;

	//
	// Create the ORB's CMM callback object, and register it with CMM.
	// Note: this must be done before the CMM runs its transitions.
	//
	if ((err = cmm_callback_impl::initialize()) != 0)
		return (err);

	//
	// Start the CMM - this triggers the cluster join for this node.
	//
	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	cmm_cntl_v->start(e);
	if (e.exception()) {
		e.exception()->print_exception("join_cluster: "
		    "failed to start CMM control\n");
		return (EINVAL);
	}

	// The CMM now drives the reconfiguration asynchronously.  We wait
	// here for the orb to fully join the cluster.
	if ((err = cmm_callback_impl::thep()->wait_for_joined()) != 0) {
		return (err);
	}

	//
	// Create name server
	//
	if ((err = ns::init_nameserver()) != 0) {
		return (err);
	}

	// Register idlversion object with nameserver
	if ((err = idlversion_impl::register_with_ns()) != 0) {
		return (err);
	}

#ifdef _FAULT_INJECTION

	//
	// Register mc_sema_server with local nameserver
	// if you find it in the root nameserver
	//
	if ((err = mc_sema_impl::get_root_to_local()) != 0) {
		return (err);
	}

#endif // _FAULT_INJECTION

	//
	// Initialize ccr trans mgr
	//
	if ((err = initialize_ccr_tm()) != 0) {
		return (err);
	}

	// register local ccr data server with ccr tm
	if ((err = register_ccr_ds()) != 0) {
		return (err);
	}

#ifdef _KERNEL
	// start addrspace server
	if ((err = addrspace_impl::initialize()) != 0)
		return (err);
#endif // _KERNEL

	// Initialize the component_registry
	if ((err = component_state_reg::initialize_registry()) != 0)
		return (err);
	return (0);
}

//
// Wait until the orb is up.
//
void
ORB::wait_until_orb_is_up()
{
	state_lock.lock();
	while (!ORB::is_orb_up()) {
		_orb_state_change_cv.wait(&state_lock);
	}
	state_lock.unlock();
}

#include <sys/cladm.h>

// Helper routine for cladmin().
extern "C" int
cl_orb_initialize()
{
#ifdef _KERNEL
	int errno = ORB::initialize();

	// We require errors returned from ORB::initialize to be valid
	// errnos as they are returned through a system call.
	// Leaving this ASSERT in even though for non-debug case we put in
	// a workaround (see below). This is to catch error cases but not
	// panic a production system
	ASSERT(errno >= 0);

	// Workaround for bugid 4209758. Not everything in ORB::initialize
	// returns errno values. Workaround here to return EIO.
	// return (errno);
	return ((errno < 0) ? EIO : errno);
#else
	return (ORB::initialize());
#endif
}

// Helper routine for cladmin().
extern "C" int
cl_orb_is_initialized()
{
	return (ORB::is_initialized());
}

// C type helper routine for wait_until_orb_is_up
extern "C" void
cl_wait_until_orb_is_up()
{
	ORB::wait_until_orb_is_up();
}

#endif // _KERNEL_ORB
