/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  freeze_lock_in.h
 *
 */

#ifndef _FREEZE_LOCK_IN_H
#define	_FREEZE_LOCK_IN_H

#pragma ident	"@(#)freeze_lock_in.h	1.12	08/05/20 SMI"

inline
freeze_lock::freeze_lock(void) : flags(0), freeze_count(0)
{
}

inline
freeze_lock::~freeze_lock(void)
{

}

// Here we just aquire the underlying read lock.  If any of the flags
// that would cause us to block or fail are set, we enter wait_for_unfreeze
// where those are handled.
//
// hold will return false if the failure flag has been set, true otherwise.

inline bool
freeze_lock::hold(void)
{
	frl_rw.rdlock();
	if (flags & (FL_FREEZE_HELD | FL_BLOCK_HOLDS | FL_FAIL_HOLDS))
		return (wait_for_unfreeze());
	else
		return (true);
}

// If an invo is finishing, we just need to drop the read lock it is
// guarenteed to hold.  The write lock can't be held at this point

inline void
freeze_lock::done()
{
	ASSERT(!(flags & FL_FREEZE_HELD));
#ifdef _KERNEL
	// Only available in the ddi version of rwlock
	ASSERT(frl_rw.lock_held());
#endif
	frl_rw.unlock();
}


// As an optimization, we can block reads without actually freezing.  A
// call to freeze blocks until existing invocations finish.  With block_now()
// we can make the invocations wait, and then call freeze later to actually
// get the lock.  Every call to block_now must be followed by a freeze.
//
// The time at which block_now takes effect is not guarenteed.  It should
// be considered an optimization which causes calls to hold() to block
// shortly after it is called.

inline void
freeze_lock::block_now(void)
{
	frl_lock.lock();
	if (!(flags & FL_FREEZE_HELD))
		flags |= FL_BLOCK_HOLDS; // Only set if not frozen
	frl_lock.unlock();
}

// Returns true if the service is frozen, false otherwise
inline bool
freeze_lock::frozen()
{
	return (flags & FL_FREEZE_HELD);
}

// Returns true if the service is blocked, false otherwise
inline bool
freeze_lock::blocked()
{
	return (flags & (FL_BLOCK_HOLDS|FL_FREEZE_HELD));
}

// This causes calls to invo to return false.  It can be used if the
// service being protected is no longer active.  If you never call
// fail() on a freeze_lock, you don't need to worry about the return
// value from hold().
//
// Failures will start shortly after the call is made, but no timing is
// guarenteed.  It should be used to fail invocations early if they were
// just going to pass through and fail further down in the implementation

inline void
freeze_lock::fail()
{
	frl_lock.lock();
	flags |= FL_FAIL_HOLDS;

	// Now we want all potential invocations waiting in hold()
	// to wake up and fail
	frl_cv.broadcast();

	frl_lock.unlock();
}

// This reverses a call to fail(), making calls to hold() return true
// again
inline void
freeze_lock::unfail()
{
	ASSERT(flags & FL_FAIL_HOLDS);
	frl_lock.lock();
	flags &= ~FL_FAIL_HOLDS;
	frl_lock.unlock();
}

// Test for failure
inline bool
freeze_lock::failed()
{
	return (flags & FL_FAIL_HOLDS);
}

#endif	/* _FREEZE_LOCK_IN_H */
