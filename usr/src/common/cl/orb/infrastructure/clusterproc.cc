/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/*	  All Rights Reserved   */


#pragma ident	"@(#)clusterproc.cc	1.50	08/07/14 SMI"

#include <sys/types.h>
#include <sys/param.h>
#include <sys/systm.h>
#include <sys/signal.h>
#include <sys/user.h>
#include <sys/proc.h>
#include <sys/time.h>
#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/procset.h>
#include <sys/copyops.h>
#include <sys/os.h>
#include <orb/infrastructure/clusterproc.h>
#include <orb/infrastructure/cl_sched.h>

#ifdef _KERNEL
#include <sys/debug.h>
#include <sys/cmn_err.h>
#include <sys/disp.h>
#include <sys/kmem.h>
#include <sys/cpr_state.h>

#include <sys/sol_version.h>

#if SOL_VERSION >= __s9
#define	S9_RT_PARAMS
#endif

#if SOL_VERSION < __s9
#define	PRE_S9
#endif

#ifdef S9_RT_PARAMS
#include <sys/rt.h>
/*
 * control flags: Copied from Solaris file usr/src/uts/common/disp/rt.c
 */
#define	RT_DOPRI	0x01    /* change priority */
#define	RT_DOTQ		0x02    /* change RT time quantum */
#define	RT_DOSIG	0x04    /* change RT time quantum signal */

#endif

#else	/* _KERNEL */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef	linux
#include <pthread.h>
#else
#include <thread.h>
#endif

size_t default_stack_size = 1024 * 1024;

#endif /* _KERNEL */

/* This is the argument passed by clnewlwp_stack to the routine cllwpwrapper */
struct clwrapperarg {
	void (*func)(void *arg);	/* actual function to call */
	void *arg;			/* argument to above function */
	int  pri;			/* thread priority */
#ifdef _KERNEL
	/* Set only in the kernel for now */
	id_t cid;			/* scheduling class id */
#else
	int  is_rt;			/* if RT class or not */
#ifndef _KERNEL_ORB
	void *stackbase;		/* The pointer to the stack base */
#endif
#endif
};

#ifdef _KERNEL

/* XX Should be got from a in a header file */
extern "C" void set_server_copyops(void);

extern proc_t *proc_cluster;	/* defined in cl_bootstrap module */

id_t cl_syscid;			/* our copy of class id of SYS class */

/*
 * Static wrapper function used to provide automatic
 * call to lwp_exit for service threads.  Also sets up
 * appropriate copyops, CPR state, and thread priority for non-SYS threads.
 */

extern "C" void
cllwpwrapper(struct clwrapperarg *vec)
{
	/*
	 * lwp_create does not set the correct priority if a non-SYS
	 * class thread creates another of the same class with a different
	 * priority. We set it here
	 */
	if (vec->cid != cl_syscid) {
		pcparms_t pcparms_opt;
		int error;
#ifndef S9_RT_PARAMS
		/* Solaris 8 version */
		rtparms_t *rtpp;
#else
		/* Solaris 9 version */
		rtkparms_t *rtpp;
#endif

		/* Get the scheduling classid for RT. */
		if (getcid("RT", &pcparms_opt.pc_cid) != 0) {
			cmn_err(CE_PANIC,
			    "Real Time class not configured in this system");
		}

		/*
		 * Verify that vec->cid is RT class. We already check above
		 * for cl_syscid.
		 */
		if (pcparms_opt.pc_cid != vec->cid) {
			cmn_err(CE_PANIC,
			    "cllwpwrapper only supports RT/SYS classes");
		}

		/*
		 * Setup the parameters for parmsset. Note that we are
		 * using rtparms_t and rtkparms_t which are internal
		 * Solaris structure and these are subject to change,
		 * until we have a contract for them.
		 */

#ifndef S9_RT_PARAMS
		/* Set priority and time quantum */
		rtpp = (rtparms_t *)pcparms_opt.pc_clparms;
		rtpp->rt_pri = (pri_t)vec->pri;
		rtpp->rt_tqsecs = 0;
		rtpp->rt_tqnsecs = RT_TQDEF;
#else
		/* Set priority, time quantum and time quantum signal */
		rtpp = (rtkparms_t *)pcparms_opt.pc_clparms;
		rtpp->rt_pri = (pri_t)vec->pri;
		rtpp->rt_tqntm = RT_TQDEF;
		rtpp->rt_tqsig = 0;
		rtpp->rt_cflags = RT_DOPRI | RT_DOTQ | RT_DOSIG;
#endif
		/* XX Would be nice to not depend on these locks */
		mutex_enter(&pidlock);
		mutex_enter(&curproc->p_lock);
		error = parmsset(&pcparms_opt, curthread);
		mutex_exit(&curproc->p_lock);
		mutex_exit(&pidlock);
		if (error != 0) {
			cmn_err(CE_WARN, "Unable to set scheduling params. "
				"for RT thread. curthread %p errno %d\n",
				curthread, error);
		}
		/*
		 * On error we proceed with the warning that we could not
		 * setup the right priorities
		 */
	}

	set_server_copyops();
	set_cpr_state();

	/* Call the actual function that this thread is to execute */
	(*(vec->func))(vec->arg);

	clear_cpr_state();
	mutex_enter(&curproc->p_lock);
	lwp_exit();
	/* NOTREACHED */
}

/*
 * clnewlwp_stack creates a new lwp to service requests.
 * pri is the priority to create the threads at
 *	pri is relative to the specified class and not a global priority value
 * cid the scheduling class id, passing NULL gets default classid (SYS)
 * stksize specifies the thread stacksize for this lwp. If stksize is 0,
 * the stacksize is set to lwp_default_stksize.
 * Returns 0 on success.
 */
extern "C" int
clnewlwp_stack(void (*func)(void *), void *arg, int pri, const char *clname,
	klwp_t **lwpp, size_t stksize)
{
	klwp_t		*lwpid = NULL;
	id_t		cid;
	struct clwrapperarg vec;
	proc_t		*procp;
	k_sigset_t	mask;
#ifdef DEBUG
	pcpri_t		pcpri_opt;
#endif

#ifndef	linux
	/* Get the scheduling classid */
	if (clname == NULL) {
		/* Defaults to SYS class */
		cid = cl_syscid;
	} else if (getcid((char *)clname, &cid) != 0) {
		cmn_err(CE_PANIC,
		    "Scheduling class %s not configured in this system",
		    clname);
	}

	/* Verify that we are within priority limits of class */
	ASSERT(CL_GETCLPRI(&sclass[cid], &pcpri_opt) == 0);
#if SOL_VERSION < __s11
	ASSERT(pri <= (pcpri_opt.pc_clpmax - pcpri_opt.pc_clpmin));
#else
	if (cid != cl_syscid) {
		ASSERT(pri <= (pcpri_opt.pc_clpmax - pcpri_opt.pc_clpmin));
	}
#endif
#endif

	/* Setup arguments to cllwpwrapper */
	vec.func = func;
	vec.arg = arg;
	vec.pri = pri;
	vec.cid = cid;

	/* This is to support mcboot. Remove for FCS */
	procp = (proc_cluster != NULL) ? proc_cluster : &p0;

#ifdef NO_MCBOOT
	/* We always have the cluster process available at this point in boot */
	ASSERT(proc_cluster != NULL);
	procp = proc_cluster;
#endif

	sigemptyset(&mask);
	/*
	 * If we want to block certain signals on these lwps,
	 * then add to the mask here
	 * sigaddset(mask, ...);
	 */

	/*
	 * Pass is pri only for SYS class threads. For non-sys class threads
	 * it gets set in cllwpwrapper.
	 */
#ifdef	PRE_S9 /* interface in Solaris 8 Update 5 */

	lwpid = lwp_create((void (*)())cllwpwrapper, (char *)&vec, sizeof (vec),
	    procp, TS_RUN, (cid == cl_syscid) ? pri : maxclsyspri,
	    mask, cid);

#else	/* interface in Solaris 9 */
	size_t		cur_lwp_stksize = (size_t)0;
	klwp_t		*curlwp = ttolwp(curthread);

	/*
	 * Set the kernel stksize for this lwp's descendants.
	 * If curlwp is NULL lwp_childstksz will be
	 * set to lwp_default_stksize. Save lwp_childstksz.
	 */
	if ((stksize > _defaultstksz) && (curlwp != NULL)) {
		cur_lwp_stksize = curlwp->lwp_childstksz;
		curlwp->lwp_childstksz = stksize;
	}

	lwpid = lwp_create((void (*)(void))cllwpwrapper,
	    (char *)&vec, sizeof (vec),
	    procp, TS_RUN, (cid == cl_syscid) ? pri : maxclsyspri,
	    &mask, cid, 0);

	/* Restore lwp_childstksz */
	if ((stksize > _defaultstksz) && (curlwp != NULL)) {
		curlwp->lwp_childstksz = cur_lwp_stksize;
	}

#endif
	if (lwpp != NULL) {
		*lwpp = lwpid;
	}

	if (lwpid == NULL) {
		return (ENOMEM);
	}

	return (0);
}

/*
 * clnewlwp is a wrapper around clnewlwp_stack for creating new lwp's
 * with the default thread stacksize. Returns 0 on success.
 */

extern "C" int
clnewlwp(void (*func)(void *), void *arg, int pri, const char *clname,
	klwp_t **lwpp)
{
	return (clnewlwp_stack(func, arg, pri, clname, lwpp,
	    (size_t)STACK_SIZE));
}

void
clsendselfsig()
{
	clsendlwpsig(curthread);
}

/*
 * clsendlwpsig
 *
 * Send SIGINT to the specified thread.
 * We got the kthread_t from the inbound_invo table so we know
 * that it's good.  Must be called holding the right invo table
 * bucket lock to insure that t exists.
 */

void
clsendlwpsig(kthread_t *t)
{
	proc_t *p = ttoproc(t);

	/*
	 *  check if we have an ancient thread or we haven't
	 *  done a respawn yet - ignore if so.
	 */
	if (p == &p0)
		return;

	ASSERT(p == proc_cluster);

	mutex_enter(&p->p_lock);
	sigtoproc(p, t, SIGINT);
	mutex_exit(&p->p_lock);
}

/*
 * clclearlwpsig
 *
 *    Clear SIGINT sent to the calling thread. SIGINT is sent by clsendlwpsig().
 *    inbound_invocation::signal() and inbound_invocation::cleanup_invocation()
 *    call clsendlwpsig() to send SIGINT to a thread processing inbound
 *    invocation.
 *    clclearlwpsig() is called by the threadpool::deferred_task_handler() to
 *    clear pending SIGINT.
 *    Note: Internally, we send SIGINT only, so we clear that signal only.
 *    If SIGINT is pending for the thread, it *may* be pending for the lwp, in
 *    which case lwp_cursig = SIGINT.
 *
 *    If a signal was pending for the lwp, then we clear that in the lwp
 *    as well as for the thread. XXX - need to verify if this is needed.
 *
 *    Signals sent from other processes are handled by cluster() function in
 *    cl_bootstrap/dc_boot.c
 */
void
clclearlwpsig()
{
	kthread_t *thrp = curthread;
	klwp_t *lwpp = ttolwp(thrp);
	proc_t *procp = ttoproc(thrp);

	/*
	 * lint complains about excessive shift in sigword() called
	 * by sigismemeber() and sigdelset()
	 */
	if (sigismember(&thrp->t_sig, SIGINT) || /*lint !e572 !e778 */
	    (lwpp->lwp_cursig != 0)) {
		mutex_enter(&procp->p_lock);
		if (sigismember(&thrp->t_sig, SIGINT)) { /*lint !e572 !e778 */
			sigdelset(&thrp->t_sig, SIGINT); /*lint !e572 !e778 */
		}
		if (lwpp->lwp_cursig != 0) {
			if (sigismember(&thrp->t_sig, lwpp->lwp_cursig)) {
				sigdelset(&thrp->t_sig, lwpp->lwp_cursig);
			}
			lwpp->lwp_cursig = 0;
			if (lwpp->lwp_curinfo) {
				siginfofree(lwpp->lwp_curinfo);
				lwpp->lwp_curinfo = NULL;
			}
		}

		mutex_exit(&procp->p_lock);
	}
}

#else /* _KERNEL */

/*
 *  user land versions of these routines
 */

void
clclearlwpsig()
{
}

void
/*ARGSUSED*/
clsendlwpsig(thread_t t)
{
}

void
clsendselfsig()
{

}

/* Emulation routine in user land for the kernel equivalent */
int
getcid(char *clname, id_t *cid)
{
#ifdef	linux
	*cid = 0;
	return (0);
#else
	pcinfo_t pcinfo_opt;
	(void) strcpy(pcinfo_opt.pc_clname, clname);
	if (priocntl((idtype_t)0, (id_t)0, PC_GETCID,
	    (char *)&pcinfo_opt) == -1) {
		/*
		 * lint complains of no prototype for ___errno() but
		 * we already include errno.h where this is defined
		 */
		return (errno);	/*lint !e746 */
	}
	if (cid != NULL) {
		*cid = pcinfo_opt.pc_cid;
	}
	return (0);
#endif	/* linux */
}

extern "C" void *
cllwpwrapper(void *clarg)
{
#ifndef _KERNEL_ORB
	/*
	 * We record the last thread exited and do a thr_join on that thread.
	 * This way we can safely free the stack of that thread. The mutex
	 * is used to protect the info about the last thread exited.
	 */
	static os::mutex_t mp;
	static os::threadid_t last_thread = 0;
	static void *last_thread_stackbase = NULL;
#endif

	static int print_error = 1;
	void *retval;

	struct clwrapperarg *vec = (struct clwrapperarg *)clarg;

	/* Cast for func for difference between user and kernel signatures */
	void *(*func)(void *) = (void *(*)(void *))(vec->func);
	void *arg = vec->arg;

	if (vec->is_rt) {

#ifndef	linux
#ifndef S9_RT_PARAMS
		rtparms_t *rtpp;
		pcparms_t pcparms_opt;
		/* Get the scheduling classid for RT. */
		if (getcid("RT", &pcparms_opt.pc_cid) != 0) {
			(void) fprintf(stderr,
			    "Real Time class not configured in this system");
		}

		rtpp = (rtparms_t *)pcparms_opt.pc_clparms;
		rtpp->rt_pri = (pri_t)vec->pri;
		rtpp->rt_tqsecs = 0;
		rtpp->rt_tqnsecs = RT_TQDEF;
		if (priocntl((idtype_t)P_LWPID, (id_t)_lwp_self(), PC_SETPARMS,
		    (char *)&pcparms_opt) && print_error) {
#else
		/* Use the recommended varargs version of priocntl() */
		if (priocntl((idtype_t)P_LWPID, P_MYID, PC_SETXPARMS, "RT",
		    RT_KY_PRI, vec->pri, RT_KY_TQSECS, 0,
		    RT_KY_TQNSECS, RT_TQDEF, 0) && print_error) {
#endif
			perror("Unable to make real time");
			/* Print the error only once per process */
			print_error = 0;
		}
#else	/* linux */
		struct sched_param sched;
		sched.sched_priority = vec->pri;
		if (pthread_setschedparam(pthread_self(), SCHED_RR,
		    &sched) != 0 && print_error) {
			perror("Unable to make real time");
			/* Print the error only once per process */
			print_error = 0;
		}
#endif	/* linux */
	} else {
/*
 * XX Do the values being passed in make sense for user and kernel too?
 *		(void) thr_setprio(thr_self(), vec->pri);
 */
	}
	retval = (*func)(arg);

#ifndef _KERNEL_ORB
	/* Frees up the stack for the last thread that exited */
	mp.lock();
	if (last_thread != 0) {
		if (os::thread::join(last_thread) == 0) {
			free(last_thread_stackbase);
		}
	}

	last_thread = os::thread::self();
	last_thread_stackbase = vec->stackbase;
	mp.unlock();
#endif
	free((void *)vec);

	return (retval);
}

/* Make clnewlwp work in user land. now neglects priority */
/* Only supports RT class and TS class (clname == NULL, defaults to TS) */
/* stacksize 0 means use the default stack size */
extern "C" int
clnewlwp(void (*func)(void *), void *arg, int pri, const char *clname,
    thread_t *tidp)
{
	return (clnewlwp_stack(func, arg, pri, clname, tidp, (size_t)0));
}

/* stacksize 0 means use the default stack size */
extern "C" int
clnewlwp_stack(void (*func)(void *), void *arg, int pri, const char *clname,
    thread_t *tidp, size_t stacksize)
{
	void *stackbase;
	struct clwrapperarg *vec;
	vec = (struct clwrapperarg *)malloc(sizeof (struct clwrapperarg));

	vec->func = func;
	vec->arg = arg;
	vec->pri = pri;
	vec->is_rt = (((clname == NULL) || strcmp("RT", clname)) ? 0 : 1);

#ifdef _KERNEL_ORB
	/* unode, no need to malloc stack space */
	stackbase = NULL;
#else
	if (stacksize == 0) {
		stacksize = default_stack_size;
	}
	stackbase = malloc(stacksize);
	vec->stackbase = stackbase;
#endif

	return (os::thread::create(stackbase, stacksize, cllwpwrapper,
		(void *)vec, (long)THR_BOUND, tidp));
}
#endif /* _KERNEL */
