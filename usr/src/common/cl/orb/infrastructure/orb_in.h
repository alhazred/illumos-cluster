/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  orb_in.h
 *
 */

#ifndef _ORB_IN_H
#define	_ORB_IN_H

#pragma ident	"@(#)orb_in.h	1.8	08/05/20 SMI"

//
// Check if ORB has been initialized - useful in some ASSERTs
//
// static
inline bool
ORB::is_initialized()
{
	return ((orb_state == ORB::ACTIVE) ||
	    (orb_state == ORB::SERVER_ACTIVE));
}

//
// Check if ORB is initializing
//
// static
inline bool
ORB::is_initializing()
{
	return ((orb_state == ORB::INITIALIZING) ||
	    (orb_state == ORB::ORB_UP));
}

//
// Return true if cluster can support ORB actions
// i.e. state of initialization higher than ORB_UP
// including ACTIVE and SERVER_ACTIVE.
//
// static
inline bool
ORB::is_orb_up()
{
	return (orb_state == ORB::ORB_UP ||
	    orb_state == ORB::ACTIVE ||
	    orb_state == ORB::SERVER_ACTIVE);
} 

#ifndef _KERNEL_ORB
//
// Check if ORB can support server objects - userland only
//
// static
inline bool
ORB::is_server_initialized()
{
	return (orb_state == ORB::SERVER_ACTIVE);
}
#endif /* _KERNEL_ORB */

//
// Check if ORB has been shutdown - useful in some ASSERTs
//
// static
inline bool
ORB::is_shutdown()
{
	return (orb_state == ORB::HALTING);
}

#endif	/* _ORB_IN_H */
