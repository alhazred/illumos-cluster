/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  orb_conf_in.h
 *
 */

#ifndef _ORB_CONF_IN_H
#define	_ORB_CONF_IN_H

#pragma ident	"@(#)orb_conf_in.h	1.42	08/05/20 SMI"

#ifdef _KERNEL_ORB

inline nodeid_t
orb_conf::node_number()
{
	return (current_node.ndid);
}

inline nodeid_t
orb_conf::local_nodeid()
{
	return (current_node.ndid);
}

inline incarnation_num
orb_conf::local_incarnation()
{
	return (current_node.incn);
}

inline ID_node &
orb_conf::current_id_node()
{
	return (current_node);
}

inline bool
orb_conf::is_local_incn(ID_node &nd)
{
	return ((nd.ndid == current_node.ndid) &&
	    ((nd.incn == INCN_UNKNOWN) || (nd.incn == current_node.incn)));
}

#else

inline nodeid_t
orb_conf::node_number()
{
	return (current_node);
}

inline nodeid_t
orb_conf::local_nodeid()
{
	return (current_node);
}

#endif

inline pri_t
orb_conf::rt_maxpri()
{
	ASSERT(maxpri != 0);	// make sure this has been initialized
	return (maxpri);
}

inline const char *
orb_conf::rt_classname()
{
	ASSERT(rt_clname != NULL);	// make sure this has been initialized
	return (rt_clname);
}

#endif	/* _ORB_CONF_IN_H */
