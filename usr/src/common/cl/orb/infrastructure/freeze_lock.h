/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FREEZE_LOCK_H
#define	_FREEZE_LOCK_H

#pragma ident	"@(#)freeze_lock.h	1.16	08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>

//
// The freeze_lock object is a special variation on a rwlock intended
// to control invocations.  It is used in a few special settings including
// the repl_service object and some of the hxdoor code.
//
// One key difference between an rwlock and a freeze_lock is that
// freeze() and unfreeze() can be called from different threads and
// aren't exclusive (if the lock is frozen, a call to freeze returns
// immediately).

// The true number of states of a freeze_lock is too high to represent
// here, but here is a simple state diagram with the effect on holds,
// where an "active hold" is a call to hold() that has returned and
// done() has not yet been called.

// State		active holds	new holds	entered with
//
// CLEAR		possible	become active	new/unfreeze
// FROZEN		none		block		freeze
// BLOCKED		possible	block		block_now
// FAILED/CLEAR		possible	fail		(*)
// FAILED/FROZEN	none		fail		(*)
// FAILED/BLOCKED	possible	fail		(*)
//
// * The FAILED condition is applied with fail() and can be removed
// with unfail().  It causes blocked and subsequent calls to hold()
// to return zero.  Otherwise, the states are the same as their non
// failing counterparts

// Here is an allowable call matrix:
//
// State	freeze	unfreeze	block_now	fail	unfail
// CLEAR	y	n		y		y	n
// FROZEN	y	y		y (n/e)		y	n
// BLOCKED	y	n		y (n/e)		y	n
// FAILED/C	y	n		y 		y (n/e)	y
// FAILED/F	y	y		y (n/e)		y (n/e)	y
// FAILED/B	y	n		y (n/e)		y (n/e)	y
//
// (n/e) = no effect
//
// Notice that unfreeze cannot be called after block_now without
// first calling freeze.  This is intentional.  block_now is an
// optimization to allow a thread which knows it is going to freeze
// to start new invocations blocking early (which, with luck, will
// shorten the total time spent waiting in freeze for holders to
// drain out.

class freeze_lock {
public:
	freeze_lock();
	~freeze_lock();
	bool hold();			// Reserve an invocation
	bool try_hold();		// Non-blocking invo reservation
	void done();			// Finish an invocation

	void freeze();			// Block new invos, wait for reserved
					// invos to drain out
	void freeze_if_unfrozen();
	void unfreeze_if_frozen();

	bool try_freeze();		// Non-blocking freeze
	void unfreeze();		// Finish a freeze
	void block_now();		// Block new invos without freezing
	void fail();			// Cause new invos to fail
	void unfail();			// undo fail() call
	bool frozen();			// Test if lock is frozen
	bool blocked();			// Test if lock is blocked or frozen
	bool failed();			// Test if new reads are failing

// There is no blocked().  block_now is an optimization and timing
// isn't guarenteed.  freeze and use frozen() if you want to be sure.

private:
	bool wait_for_unfreeze();

	enum {
		FL_FREEZE_HELD 		= 0x01,
		FL_BLOCK_HOLDS		= 0x02,
		FL_FAIL_HOLDS		= 0x04
	};

	os::mutex_t	frl_lock;
	os::rwlock_t	frl_rw;
	os::condvar_t	frl_cv;
	uint_t		freeze_count;
	uint_t		flags;
#ifdef	DEBUG
	os::threadid_t	last_freezer;
#endif
};

#include <orb/infrastructure/freeze_lock_in.h>

#endif	/* _FREEZE_LOCK_H */
