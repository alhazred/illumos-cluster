//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb_conf.cc	1.47	08/05/20 SMI"

#include <sys/os.h>
#include <sys/cladm_int.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <sys/clconf_int.h>
#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/tspriocntl.h>
#include <sys/cl_comm_support.h>
#include <unistd.h>

#ifdef _KERNEL
#include <sys/disp.h>
#endif

#if (defined(_KERNEL_ORB) && !defined(_KERNEL))
#include <unode/unode.h>
#endif

// global variable declaration
#ifdef _KERNEL_ORB
ID_node orb_conf::current_node;
#else	// _KERNEL_ORB
nodeid_t orb_conf::current_node = NODEID_UNKNOWN;
#endif	// _KERNEL_ORB

pri_t orb_conf::maxpri = 0;
const char *orb_conf::rt_clname = NULL;

os::sc_syslog_msg	*orb_syslog_msgp = NULL;

// Static members of the ORB class
ORB::orb_state_t ORB::orb_state = ORB::UNINIT;
os::mutex_t ORB::state_lock;
os::condvar_t ORB::_orb_state_change_cv;

// Mutex used in node_configured()
#ifndef _KERNEL_ORB
os::mutex_t	*orb_conf::buf_lock = NULL;
#endif

int
orb_conf::configure()
{
#if defined(_KERNEL_ORB)
	clconf_init();
	current_node.ndid = clconf_get_nodeid();
	if (current_node.ndid == NODEID_UNKNOWN) {
		return (EINVAL);
	}

#ifdef _KERNEL
	timespec_t	tv;
	gethrestime(&tv);
#else
	struct timeval	tv;
	(void) gettimeofday(&tv, NULL);
#endif // _KERNEL

	// Calculate local incarnation number - it is based on gethrestime
	if (tv.tv_sec == INCN_UNKNOWN)
		tv.tv_sec = 1;
	current_node.incn = (incarnation_num)tv.tv_sec;
	ASSERT(current_node.incn != INCN_UNKNOWN);

#else // _KERNEL_ORB
	int flags = 0;
	if (os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &flags) != 0)
		return (errno);
	if ((flags & CLUSTER_BOOTED) == 0)
		return (EINVAL);
	current_node = NODEID_UNKNOWN;
	if (os::cladm(CL_CONFIG, CL_NODEID, &current_node) != 0)
		return (errno);
	if (current_node == NODEID_UNKNOWN) {
		return (EINVAL);
	}
	//
	// Initialize buf_lock used by node_configured(); This
	// initialization is safe because this is method is called at
	// the very beginning in ORB::initialize() and other threads
	// in orb have not been started yet.
	//
	buf_lock = new os::mutex_t;

#endif // _KERNEL_ORB

	//
	// Find the maximum real time priority value (relative to the
	// base of real time priorities) currently configured in the
	// system. In userland, if RT class is not configured or not superuser
	// we use the TS class maxpri
	//

#ifdef _KERNEL
	id_t		cid;
	pcpri_t		pc_pri;

	if (getcid("RT", &cid) || CL_GETCLPRI(&sclass[cid], &pc_pri)) {
		//
		// Sun Cluster requires that the RT thread scheduling class
		// be configured in the kernel.
		//

		//
		// SCMSGS
		// @explanation
		// Sun Cluster requires that the real time thread scheduling
		// class be configured in the kernel.
		// @user_action
		// Configure Solaris with the RT thread scheduling class in
		// the kernel.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: RT class not configured in this system");
		/* NOTREACHED */
	} else {
		maxpri = pc_pri.pc_clpmax - pc_pri.pc_clpmin;
		rt_clname = "RT";
	}
#else
	pcinfo_t	pc_info;

	(void) strcpy(pc_info.pc_clname, "RT");
	if ((geteuid() == 0) &&
	    priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info) != -1) {
		//
		// Can use RT class in user process only if superuser
		//
		rtinfo_t *rt_info = (rtinfo_t*)pc_info.pc_clinfo;
		maxpri = rt_info->rt_maxpri;
		rt_clname = "RT";
	} else {
		(void) strcpy(pc_info.pc_clname, "TS");
		if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
		    != -1) {
			//
			// Using TS thread scheduling class in user process
			//
			tsinfo_t *ts_info = (tsinfo_t*)pc_info.pc_clinfo;
			maxpri = ts_info->ts_maxupri;
			rt_clname = "TS";
		} else {
			//
			// SCMSGS
			// @explanation
			// The system requires either real time or time
			// sharing thread scheduling classes for use in user
			// processes. Neither class is available.
			// @user_action
			// Configure Solaris to support either real time or
			// time sharing or both thread scheduling classes for
			// user processes.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: RT or TS classes not configured");
			/* NOTREACHED */
		}
	}
#endif
	return (0);
}

// XX This code will go away once the tests are cleaned up
#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
nodeid_t
orb_conf::get_root_nodeid()
{
	// Since we dont support this in the CCR emulate this as
	// returning the lowest nodeid that is configured
	nodeid_t n;
	for (n = 1; n <= NODEID_MAX; n++) {
		if (node_configured(n))
			return (n);
	}
	return (NODEID_UNKNOWN);
}
#endif

//
// Is the specified node configured cluster node
//
bool
orb_conf::node_configured(nodeid_t n)
{
	bool ret = true;
#ifdef _KERNEL_ORB
	clconf_cluster_t *cl = clconf_cluster_get_current_funcp();
	clconf_obj_t *nd = clconf_obj_get_child_funcp((clconf_obj_t *)cl,
	    CL_NODE, (int)n);
	ret = (nd != NULL);
	clconf_obj_release_funcp((clconf_obj_t *)cl);

#else // _KERNEL_ORB

	static char 		buffer[CL_MAX_LEN+1];
	clnode_name_t 		clname;

	ASSERT(buf_lock != NULL);

	buf_lock->lock();
	buffer[0] = '\0';

	clname.nodeid = n;
	clname.name = buffer;
	clname.len = CL_MAX_LEN;
	if (os::cladm(CL_CONFIG, CL_GET_NODE_NAME, &clname) != 0) {
		ret = false;
	} else if (clname.name[0] == '\0') {
		// _cladm returns empty string for unconfigured nodeids
		ret = false;
	}

	buf_lock->unlock();

#endif // _KERNEL_ORB

	return (ret);
}
