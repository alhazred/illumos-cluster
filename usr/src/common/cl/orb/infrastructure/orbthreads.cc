//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orbthreads.cc	1.13	08/05/20 SMI"

#include <orb/infrastructure/orbthreads.h>

//
// The following global variable is used to keep track of all the threads
// created by the ORB.
//
orbthreadlist *orbthreadlist::the_orb_thread_list = NULL;

//
// Code to support orb threads.
//

orbthread::~orbthread()
{
}

//
//  Shutdown an orbthread by changing its state and signalling it.
//  The signal should cause it to exit its main processing loop
//  and signal the threadGone cv. Note that shutdown could be called on
//  the same thread twice, once from the ORB::shutdown and once from the
//  destructor of the object. Hence we first check the state to make sure
//  it is ACTIVE before attempting to shut it down.
//
void
orbthread::shutdown()
{
	lock();
	if (state == orbthread::ACTIVE) {
		state = orbthread::DYING;
		threadcv.signal();
	}
	while (state != DEAD) {
		threadGone.wait(&threadLock);
	}
	unlock();
}

//
// Registerthread adds a thread to the pool of orb threads by calling
// addthread. Now we belong to the orb pool and the orb can shut us down
// in an orderly way.
//
void
orbthread::registerthread()
{
	orbthreadlist::the().addthread(this);
}

//
// unregisterthread removes a thread from the pool of orb threads. This
// is typically done when the destructor function is stopping a thread.
// The return value is a boolean which returns true if the thread was found
// on the linked list of orb threads and was deleted. We return false if
// the thread was not found on the linked list of orb threads.
//
// Note that callers can depend on the bool return from this function to
// figure out whether the thread is still running and if they should shut it
// down.
//
bool
orbthread::unregisterthread()
{
	return (orbthreadlist::the().delthread(this));
}

// Allocate the orbthreadlist object.
int
orbthreadlist::initialize()
{
	ASSERT(the_orb_thread_list == NULL);
	the_orb_thread_list = new orbthreadlist;
	if (the_orb_thread_list == NULL)
		return (ENOMEM);
	return (0);
}

//
// addthread : add a thread to the list of orb threads. Obtain the listLock
// and append the thread to the singly linked list in the orbthreadpool.
//
void
orbthreadlist::addthread(orbthread *newthread)
{
	list_lock.lock();
	thread_list.append(newthread);
	list_lock.unlock();
}

//
// delthread : remove a thread from the list of orb threads. Call erase
// to find the thread and remove it. Note that it is ok if the thread is
// not found in the list of orb threads.
//
bool
orbthreadlist::delthread(orbthread *oldthread)
{
	bool ret;

	list_lock.lock();
	ret = thread_list.erase(oldthread);
	list_lock.unlock();
	return (ret);
}

//
// shutdown :	shutdown all the threads that belong to the ORB in an
//		orderly manner. Do so by removing each element from the
//		orb thread pool and calling its shutdown fuction.
//
void
orbthreadlist::shutdown()
{
	orbthread *next_orb_thread;

	if (the_orb_thread_list == NULL) {
		return;
	}

	the_orb_thread_list->list_lock.lock();
	while ((next_orb_thread = the_orb_thread_list->thread_list.reapfirst())
	    != NULL) {
		// Drop the lock before calling shutdown
		the_orb_thread_list->list_lock.unlock();
		next_orb_thread->shutdown();
		the_orb_thread_list->list_lock.lock();
	}
	the_orb_thread_list->list_lock.unlock();
	delete the_orb_thread_list;
	the_orb_thread_list = NULL;
}
