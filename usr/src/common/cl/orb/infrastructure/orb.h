/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORB_H
#define	_ORB_H

#pragma ident	"@(#)orb.h	1.56	08/05/20 SMI"

#include <sys/os.h>
#include <sys/rsrc_tag.h>
#include <orb/invo/argfuncs.h>

//
// Common syslog object
//
extern os::sc_syslog_msg	*orb_syslog_msgp;

//
// The ORB class manages the ORB subsystem.
// So this class primarily performs startup and shutdown for the ORB subsystem.
//
class ORB {
public:
	enum orb_state_t {
		UNINIT,		// Begin/Uninitialized state
		INITIALIZING,	// ORB is initializing
		ORB_UP,		// The system has started the ORB, but has
				// not joined the cluster
		ACTIVE,		// ORB initialization has completed
				// for CORBA client activity
		SERVER_ACTIVE,	// ORB initialization has completed
				// for CORBA server activity
		HALTING		// ORB is shutting down
	};

#ifdef	_KERNEL
	static int initialize();
#else
	static int initialize(bool increase_file_limit = true);
#endif	/* _KERNEL */

#ifndef	_KERNEL_ORB
	static int server_initialize();
#endif /* _KERNEL_ORB */

	static int join_cluster();
	static void shutdown();

	// Check if ORB has been initialized - useful in some ASSERTs
	static bool is_initialized();

	// Check if cluster can support ORB actions
	static bool is_orb_up();

	// Wait until the orb is up
	static void wait_until_orb_is_up();

	// Check if ORB has been initialized - useful in some ASSERTs
	static bool is_initializing();

	// Check if ORB can support server objects
#ifndef	_KERNEL_ORB
	static bool is_server_initialized();
#endif /* _KERNEL_ORB */

	// Check if ORB has been shutdown - useful in some ASSERTs
	static bool is_shutdown();

private:
	// Function to initialize all the handler kit's defined in cl_comm.
	static int initialize_handler_kits();

	// Function to shutdown all the handler kit's defined in cl_comm.
	static void shutdown_handler_kits();

	static orb_state_t orb_state;

	// Protects orb_state
	static os::mutex_t state_lock;

	// Protection for multiple threads calling ORB::initialize
	static os::condvar_t	_orb_state_change_cv;

#if !_KERNEL && !_KERNEL_ORB
	// pid of process that initialized ORB
	static pid_t	   creator_pid;
#endif // !_KERNEL && !_KERNEL_ORB

	// Disallow assignments and pass by value
	ORB(const ORB &);
	ORB &operator = (ORB &);
};

#include <orb/infrastructure/orb_in.h>

#endif	/* _ORB_H */
