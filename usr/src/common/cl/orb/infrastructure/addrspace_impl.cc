//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)addrspace_impl.cc	1.47	08/09/27 SMI"

#include <sys/types.h>
#include <sys/pathname.h>
#include <sys/debug.h>
#include <sys/errno.h>
#include <stdio.h>
#include <sys/proc.h>

#include <vm/as.h>
#include <vm/page.h>
#include <sys/os.h>
#include <orb/infrastructure/addrspace_impl.h>
#include <sys/uio.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>
#include <sys/procfs.h>
#include <sys/proc/prdata.h>
#include <sys/cl_comm_support.h>

os::mutex_t addrspace_impl::addrspace_lock;
bool addrspace_impl::addrspace_initialized = false;

//
// addrspace_startup is called when the orb module is loaded.
// This routine creates an addrspace object and registers it with the name
// server under addrspace.nodenum
//
int
addrspace_impl::initialize()
{
	Environment e;
	naming::naming_context_var ctxp;

	addrspace_lock.lock();

	ASSERT(!addrspace_initialized);
	addrspc::addrspace_var ref = (new addrspace_impl())->get_objref();
	char name[20];
	(void) sprintf(name, "addrspace.%u", orb_conf::node_number());
	ctxp = root_nameserver_funcp();
	ctxp->rebind(name, ref, e);
	if (e.exception() != NULL) {
		e.exception()->print_exception(
			"addrspace_impl: could not bind to name server");
		addrspace_initialized = false;
		addrspace_lock.unlock();
		return (ECOMM);
	}
	addrspace_initialized = true;
	addrspace_lock.unlock();
	return (0);
}

//
// This routine gets rid of the addrspace object when the orb is unloaded.
//
void
addrspace_impl::shutdown()
{
	Environment e;
	naming::naming_context_var ctxp;

	addrspace_lock.lock();
	if (!addrspace_initialized) {
		// Nothing to do
		addrspace_lock.unlock();
		return;
	}

	char name[20];
	(void) sprintf(name, "addrspace.%u", orb_conf::node_number());
	ctxp = root_nameserver_funcp();
	ctxp->unbind(name, e);
	if (e.exception()) {
		e.exception()->print_exception(
			"addrspace_impl: unable to unbind from name server");
		addrspace_lock.unlock();
		return;
	}
	addrspace_initialized = false;
	addrspace_lock.unlock();
}

void
#ifdef DEBUG
addrspace_impl::_unreferenced(unref_t cookie)
#else
addrspace_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

sol::error_t
addrspace_impl::copyin_common(int32_t proc_id, sol::uintptr_t addr,
    sol::size_t len, addrspc::ioctldata_t *datap, int &resid,
    Environment &)
{
	sol::error_t status;

	mutex_enter(&pidlock);
	proc_t *p = prfind(proc_id);
	// The process shouldn't have disappeared, since it is in
	// the middle of a system call.
	mutex_exit(&pidlock);
	struct uio _uio;
	struct iovec _iovec;

	_iovec.iov_base = (char *)datap->buffer();
	_iovec.iov_len = len;			//lint !e713
	_uio.uio_iov = &_iovec;
	_uio.uio_iovcnt = 1;
	_uio.uio_loffset = addr;
	_uio.uio_segflg = UIO_SYSSPACE;
	_uio.uio_resid = (ssize_t)len;
	status = prusrio(p, UIO_READ, &_uio, 1);
	ASSERT(_uio.uio_resid <= INT_MAX);	// for cast below
	resid = (int)_uio.uio_resid;

	return (status);
}

//
// copyin: we get data from user level, return the data
// All the work is done by prusrio.
//

sol::error_t
addrspace_impl::copyin(int32_t proc_id, sol::uintptr_t addr, sol::size_t len,
    addrspc::ioctldata_t_out data, Environment &_environment)
{
	sol::error_t status;
	int resid;

	ASSERT(len <= UINT_MAX);	// for casts below
	addrspc::ioctldata_t *datap = new addrspc::ioctldata_t(
						(uint_t)len, (uint_t)len);
	status = copyin_common(proc_id, addr, len, datap, resid, _environment);
	data = datap;
	if (resid != 0) {
		// prusrio doesn't return an error if a page doesn't exist
		return (EFAULT);
	}

	return (status);
}

sol::error_t
addrspace_impl::copyout(int32_t proc_id, sol::uintptr_t addr, sol::size_t len,
    const addrspc::ioctldata_t &data, Environment &)
{
	sol::error_t status;

	mutex_enter(&pidlock);
	proc_t *p = prfind(proc_id);
	mutex_exit(&pidlock);
	struct uio _uio;
	struct iovec _iovec;
	_iovec.iov_base = (char *)data.buffer();
	_iovec.iov_len = len;			//lint !e713
	_uio.uio_iov = &_iovec;
	_uio.uio_iovcnt = 1;
	_uio.uio_loffset = addr;
	_uio.uio_segflg = UIO_SYSSPACE;
	_uio.uio_resid = (ssize_t)len;
	status = prusrio(p, UIO_WRITE, &_uio, 1);
	if (_uio.uio_resid != 0) {
		// prusrio doesn't return an error if a page doesn't exist
		return (EFAULT);
	}
	return (status);
}

//
// copyinstr: we get data from user level, return the data
// We use addrspace::copyin to copy in maxlen bytes and then we truncate
// the result to the appropriate string length.  This is a bit inefficient
// since maxlen bytes get copied from user space, but at least we only
// send the bytes we need across the network.
// If the string doesn't fit, we return ENAMETOOLONG
//
sol::error_t
addrspace_impl::copyinstr(int32_t proc_id, sol::uintptr_t addr,
    sol::size_t maxlen, addrspc::ioctldata_t_out data,
    Environment &_environment)
{
	sol::error_t status;
	int resid;

	ASSERT(maxlen < UINT_MAX);	// for casts below
	addrspc::ioctldata_t *datap =
	    new addrspc::ioctldata_t((uint_t)maxlen + 1, (uint_t)maxlen + 1);
	status = copyin_common(proc_id, addr, maxlen, datap, resid,
		_environment);

	data = datap;
	if (status) {
		if (status == EIO)
			status = EFAULT;
		datap->length(0);
		return (status);
	}

	ASSERT(maxlen <= INT_MAX);	// for cast below
	ASSERT((int)maxlen >= resid);
	((char *)datap->buffer())[(int)maxlen - resid] = '\0';
	size_t len = strlen((char *)datap->buffer());
	//
	// Return ENAMETOOLONG if the string isn't NULL terminated.
	//
	if (len == maxlen) {
		datap->length(0);
		return (ENAMETOOLONG);
	}
	//
	// Return EFAULT if the string isn't NULL terminated
	// and located at the end of a page.
	//
	if ((resid > 0) && (len == ((int)maxlen - resid))) { //lint !e737
		datap->length(0);
		return (EFAULT);
	}
	ASSERT(len < UINT_MAX);		// for cast below
	datap->length((uint_t)len + 1);
	return (status);
}
