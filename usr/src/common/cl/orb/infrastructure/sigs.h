/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SIGS_H
#define	_SIGS_H

#pragma ident	"@(#)sigs.h	1.11	08/05/20 SMI"

//
// routines to implement signal messaging
//

#include <orb/invo/io_invos.h>
#include <orb/member/Node.h>

class recstream;

class signals {
public:
	static void signal_send(ID_node &, slot_t);
	static void cancel_send(ID_node &, slot_t);
	static void finish_send(ID_node &, slot_t);
	static void handle_messages(recstream *);
private:
	enum { SIGNAL = 1, CANCEL = 2, FINISH = 3};

	static void send_message(int message, ID_node &, slot_t);
};

#endif	/* _SIGS_H */
