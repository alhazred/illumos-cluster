/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ORBTHREADS_H
#define	_ORBTHREADS_H

#pragma ident	"@(#)orbthreads.h	1.21	08/05/20 SMI"

#include <sys/list_def.h>
#include <sys/threadpool.h>
#include <orb/infrastructure/common_threadpool.h>

//
// For each thread that the orb creates, there is one instance of the
// following. The orb threads should be derived from this class.
//
class orbthread {
public:
	enum state_t { ACTIVE, DYING, DEAD };

	void registerthread();
	void shutdown();
	bool unregisterthread();

	orbthread() : state(orbthread::ACTIVE) { }

	virtual		~orbthread();

protected:
	void		lock() { threadLock.lock(); }
	void		unlock() { threadLock.unlock(); }
	os::condvar_t	threadGone;
	os::condvar_t	threadcv;
	os::mutex_t	threadLock;
	state_t		state;
};

//
// There is just one instance of this class, which keeps a list of
// all the threads that belong to the orb. When new threads are
// created by the orb, they should add themselves to this list.
// When the orb wants to shutdown, it can traverse this list and
// invoke shutdown on each thread.
//
class orbthreadlist {
public:
	static int		initialize();
	static void		shutdown();
	void			addthread(orbthread *newthread);
	bool			delthread(orbthread *oldthread);
	static orbthreadlist	&the();
private:
	os::mutex_t	list_lock;    // to protect multiple list manipulators
	static orbthreadlist *the_orb_thread_list;
	SList<orbthread> thread_list;
};

#include <orb/infrastructure/orbthreads_in.h>

#endif	/* _ORBTHREADS_H */
