/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  common_threadpool_in.h
 *
 */

#ifndef _COMMON_THREADPOOL_IN_H
#define	_COMMON_THREADPOOL_IN_H

#pragma ident	"@(#)common_threadpool_in.h	1.8	08/05/20 SMI"

inline threadpool &
common_threadpool::the()
{
#ifdef	_KERNEL_ORB
	ASSERT(the_common_threadpool != NULL);
#else
	//
	// In the userland it is possible that the common_threadpool
	// was not initialized and someone is trying to use it. Create
	// it now.
	//
	if (!ORB::is_server_initialized()) {
		ORB::server_initialize();
	}
#endif // _KERNEL_ORB
	return (*the_common_threadpool);
}

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
inline threadpool &
zone_threadpool::the()
{
	ASSERT(the_zone_threadpool != NULL);
	return (*the_zone_threadpool);
}

#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)
#endif	/* _COMMON_THREADPOOL_IN_H */
