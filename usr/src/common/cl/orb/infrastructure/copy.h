/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _COPY_H
#define	_COPY_H

#pragma ident	"@(#)copy.h	1.15	08/05/20 SMI"

#include <sys/copyops.h>
#include <sys/os.h>
#include <h/sol.h>

//
// Copy_args is a simple structure to hold the node id and pid that we
// need to identify the process performing an ioctl.
//
class copy_args {
public:
	copy_args(sol::nodeid_t n, sol::pid_t p);

	sol::nodeid_t	nodeid;
	sol::pid_t	pid;
};

//
// The copy class provides an interface to the thread-specific storage.
// It is used to hold the copy_args associated with an ioctl.
// It is set/accessed with setcontext/getcontext.
//
// This class is used in response to a system call, and not in
// response to an invocation from a non-global zone. So, while handing
// off the IDL invocation (coming from a non-global zone) at the
// gateway level to a zone threadpool worker thread, we needn't hand
// off this thread-specific data.
//
class copy {
public:
	copy();
	static void setcontext(copy_args *argptr);
	static copy_args *getcontext();

private:
	static os::tsd context;
};

#include <orb/infrastructure/copy_in.h>

#endif	/* _COPY_H */
