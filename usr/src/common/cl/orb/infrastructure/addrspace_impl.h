/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _ADDRSPACE_IMPL_H
#define	_ADDRSPACE_IMPL_H

#pragma ident	"@(#)addrspace_impl.h	1.34	08/09/27 SMI"

#include <h/addrspc.h>
#include <orb/object/adapter.h>

// CSTYLED
class addrspace_impl : public McServerof<addrspc::addrspace> {
public:
	addrspace_impl() {}
	~addrspace_impl() {}
	void _unreferenced(unref_t arg);

	sol::error_t copyin(int32_t pid, sol::uintptr_t addr, sol::size_t len,
	    addrspc::ioctldata_t_out data, Environment &_environment);
	sol::error_t copyout(int32_t pid, sol::uintptr_t addr, sol::size_t len,
	    const addrspc::ioctldata_t &data, Environment &_environment);
	sol::error_t copyinstr(int32_t pid, sol::uintptr_t addr,
	    sol::size_t maxlen, addrspc::ioctldata_t_out data,
	    Environment &_environment);

	static int initialize();
	static void shutdown();

private:

	//
	// There can be only one instance of addrspace_impl.
	// The boolean addrspace_initialized will be used to
	// detect if the address space has been initialized
	// or not. When the address space has been initialized,
	// this variable will be set.
	// The mutex addrspace_lock will protect
	// the boolean variable.
	//
	static os::mutex_t	addrspace_lock;
	static bool		addrspace_initialized;

	static sol::error_t copyin_common(int32_t pid, sol::uintptr_t addr,
	    sol::size_t len, addrspc::ioctldata_t *data, int &resid,
	    Environment &_environment);
};

#endif	/* _ADDRSPACE_IMPL_H */
