/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)common_threadpool.cc	1.10	08/05/20 SMI"

#include <sys/os.h>
#include <orb/infrastructure/common_threadpool.h>

// This is a threadpool that can be used whenever a task needs to be
// done in a different thread.  It is just a wrapper around the threadpool
// code that provides a "the()" function, among other things.  Note that
// in userland (as opposed to the kernel) it must be explicitly started
// before it is used by calling the_common_threadpool::initialize();

threadpool *common_threadpool::the_common_threadpool = NULL;

//
// Specify the initial number of threads for the common threadpool.
// The number of threads changes dynamically for this threadpool.
// Most users of the common threadpool reside in the kernel,
// so a user level application has less need for these threads.
// XX - at some point we should collect statistics so that
// XX - we could properly tune this value.
//
#ifdef _KERNEL_ORB
const int	common_threadpool_size = 6;
#else
const int	common_threadpool_size = 4;
#endif

//
// Create the common threadpool and initialize the number of threads.
//
int
common_threadpool::initialize()
{
	ASSERT(the_common_threadpool == NULL);
#ifndef	_KERNEL_ORB
	if (os::thread::main() == -1) {
		os::panic("clcomm::common_threadpool requires "
			"thread support\n");
		return (ENOTSUP);
	}
#endif
	the_common_threadpool = new threadpool(true, 0, "Common threadpool");
	if (the_common_threadpool == NULL) {
		return (ENOMEM);
	}
	if (the_common_threadpool->change_num_threads(common_threadpool_size) ==
	    0) {
		delete the_common_threadpool;
		return (ENOMEM);
	}
	return (0);
}

void
common_threadpool::shutdown()
{
	//
	// No guarantee this won't race against calls to the().  We assume
	// when shutdown is called all clients of the common threadpool have
	// been shut down.
	//

	//
	// In userland its possible that the common threadpool was never
	// started and in the kernel ORB::initialize might have failed
	// before initializing the_common_threadpool.
	//
	if (the_common_threadpool == NULL) {
		return;
	}

	the_common_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
	the_common_threadpool->shutdown();

#ifdef	_KERNEL_ORB
	// Test deleting the common threadpool in unode only
	delete the_common_threadpool;
	the_common_threadpool = NULL;
#else
	//
	// In userland we just leak the memory in the corner case where
	// someone initializes it, shuts it down, and then reinitializes it,
	// because we don't assume we can safely delete it.
	//
	the_common_threadpool = NULL;
#endif
}


#if (SOL_VERSION >= __s10) && defined(_KERNEL)

//
// Threadpool for servicing IDL invocations starting from inside
// non-global zones
//
threadpool *zone_threadpool::the_zone_threadpool = NULL;

//
// Specify the initial number of threads for the zone threadpool.
// The number of threads changes dynamically for this threadpool.
//
const int	zone_threadpool_size = 6;

//
// Create the zone threadpool and initialize the number of threads.
//
int
zone_threadpool::initialize()
{
	ASSERT(the_zone_threadpool == NULL);

	the_zone_threadpool
	    = new threadpool(true, 0, "Zone threadpool");
	if (the_zone_threadpool == NULL) {
		return (ENOMEM);
	}
	if (the_zone_threadpool->change_num_threads(zone_threadpool_size) ==
	    0) {
		delete the_zone_threadpool;
		return (ENOMEM);
	}
	return (0);
}

void
zone_threadpool::shutdown()
{
	//
	// No guarantee this won't race against calls to the().  We assume
	// when shutdown is called all clients of the zone threadpool have
	// been shut down.
	//

	//
	// In the kernel, ORB::initialize might have failed
	// before initializing the_zone_threadpool.
	//
	if (the_zone_threadpool == NULL) {
		return;
	}

	the_zone_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
	the_zone_threadpool->shutdown();

	delete the_zone_threadpool;
	the_zone_threadpool = NULL;
}

#endif	// SOL_VERSION >= __s10 && defined(_KERNEL)
