/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _FORK_H
#define	_FORK_H

#pragma ident	"@(#)fork.h	1.10	08/05/20 SMI"

#include <sys/sol_version.h>
//
// Consistent Fork: In Solaris 10 and all future Solaris releases
// fork() will consistently be fork1() irrespective of libthread
// and libpthread linking order. This project has also implemented
// new forkall() interface which replicates all of the parent process's
// threads in the child (same as a previous libthread fork() behaviour).
//


#if SOL_VERSION >= __s10
#ifndef CONSISTENT_FORK
#define	CONSISTENT_FORK // PSARC/2003/053 Consistent Fork
#endif
#endif

//
// class pthread_atfork_handler is a wrapper class for combining
// all the  pthread_atfork handler's in one class. This class facilitates
// scoping of handler functions.
//
class pthread_atfork_handler {
public:
	static void prepare_handler();
	static void parent_handler();
	static void child_handler();
};


#endif	/* _FORK_H */
