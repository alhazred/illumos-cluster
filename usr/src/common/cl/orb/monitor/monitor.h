//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _MONITOR_H
#define	_MONITOR_H

#pragma ident	"@(#)monitor.h	1.11	08/05/20 SMI"

//
// Each node contains exactly one monitor.
// The monitor observes the system state (currently memory availability).
// The monitor publishes notifications when the system state changes.
//
// Components can subscribe to notifications by registerring callbacks.
// Any component that has registerred a callback must deregister
// prior to terminating.
//

#include <sys/os.h>
#include <sys/vmsystm.h>
#include <sys/list_def.h>
#include <sys/threadpool.h>
#include <orb/infrastructure/orbthreads.h>

//
// monitor - this class implements the system monitor.
//
class monitor : public orbthread {
public:
	enum system_state_t {
		UNKNOWN		= 0x0,	// Only used internal to monitor
					//
		MEMORY_STARVED	= 0x1,	// PURGE! Dangerously low on memory
					// 75% below MEMORY_PURGE
					//
		MEMORY_LOWER	= 0x3,	// Purge because even lower on memory
					// 50% below MEMORY_PURGE
					//
		MEMORY_LOW	= 0x2,	// Purge because low on memory
					// 25% below MEMORY_PURGE
					//
		MEMORY_PURGE	= 0x4,	// Start purging
					//
		MEMORY_PLENTY	= 0x5	// System has plenty of available memory
					// Anything above MEMORY_PURGE
	};

	// Function prototype used when registerring a callback
	typedef void (*callback_func)(system_state_t);

	static int	initialize();
	static void 	shutdown();

	static monitor	&the();

	//
	// The subscriber will immediately get a callback with the
	// current system state. After the initial callback,
	// the system will deliver a callback
	// only upon a state change.
	//
	void		subscribe(callback_func);

	//
	// The operation blocks until all "in progress"
	// callbacks have completed. After this method returns,
	// no more callbacks will be made on this function.
	//
	void		unsubscribe(callback_func);


	// Returns the stored system state.
	system_state_t	get_state();

	//
	// Determine the memory status of the node, store the state as
	// current state and return the new state.
	//
	system_state_t get_current_state();
private:
	//
	// monitor_task - this task performs the callback
	//
	class monitor_task : public defer_task {
	public:
		monitor_task(callback_func);

		//
		// Method that performs the actual callback
		//
		virtual void	execute();

		void		must_deregister();

		bool		active();

		void		set_active();

		callback_func	callback();

	private:
		// Disallow default constructor
		monitor_task();

		// disallowed - pass by value and assignment
		monitor_task(const monitor_task &);
		monitor_task &operator = (monitor_task &);

		enum {
			ACTIVE_CALLBACK = 0x01,	// The defer task is queued
			DEREGISTERING = 0x02	// Deregister pending
		};

		uint_t		flags;

		system_state_t	last_known_state;

		callback_func	callbackf;

		os::mutex_t	lck;
		os::condvar_t	cv;
	};

	// This will become a thread that periodically monitors the system
	static void	monitor_thread(void *);

	monitor();

	int			initialize_thread();

	void			examine_system_state();

	system_state_t		prior_state;
	system_state_t		current_state;

	//
	// The monitor_task can simultaneously be queued to a threadpool.
	// Thus we need a separate set of links to keep track of
	// all of the registerred subscribers.
	//
	SList<monitor_task>	subscribers;

	os::mutex_t		lck;

	static int		second_counter;

	static monitor		*the_monitor;

	// Disallow assignments and pass by value
	monitor(const monitor &);
	monitor &operator = (monitor &);
};

#include <orb/monitor/monitor_in.h>

#endif	// _MONITOR_H
