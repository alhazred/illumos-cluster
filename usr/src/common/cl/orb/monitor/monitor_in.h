/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MONITOR_IN_H
#define	_MONITOR_IN_H

#pragma ident	"@(#)monitor_in.h	1.6	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <sys/vmsystm.h>

extern pgcnt_t memory_monitor_purge_threshold;

//
// set_active - show that the task is active,
// which means that we are in the process of doing a callback.
//
inline void
monitor::monitor_task::set_active()
{
	flags |= ACTIVE_CALLBACK;
}

// static
inline monitor &
monitor::the()
{
	ASSERT(the_monitor != NULL);
	return (*the_monitor);
}


//
// get_state - Returns the current system state
//
inline monitor::system_state_t
monitor::get_state()
{
	return (current_state);
}

//
// Re-calculate the current memory status based on free pages in the
// node and return the current state.
//
inline monitor::system_state_t
monitor::get_current_state()
{
	pgcnt_t		purge_threshold;

	//
	// Determine the threshold value for purge callbacks.
	// Do this calculation every time so that we can enable
	// system experts to dynamically change values.
	// Sanity checks are there to ensure that the value is
	// within a supported range.
	//
	if (memory_monitor_purge_threshold < throttlefree + 100) {
		purge_threshold = throttlefree + 100;
	} else {
		purge_threshold = memory_monitor_purge_threshold;
	}

	//
	// Determine the memory availability state
	//
	if (freemem <= purge_threshold) {
		//
		// The system is dangerously low on memory.
		//
		current_state = MEMORY_STARVED;

	} else if (freemem <= purge_threshold * 2) {
		//
		// Purge because very low on memory
		//
		current_state = MEMORY_LOWER;

	} else if (freemem <= purge_threshold * 4) {
		//
		// Purge because low on memory
		// 25% below MEMORY_PURGE
		//
		current_state = MEMORY_LOW;

	} else if (freemem <= purge_threshold * 8) {
		//
		// Start purging
		//
		current_state = MEMORY_PURGE;

	} else {
		//
		// The system has enough memory.
		//
		current_state = MEMORY_PLENTY;
	}

	return (current_state);
}

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _MONITOR_IN_H */
