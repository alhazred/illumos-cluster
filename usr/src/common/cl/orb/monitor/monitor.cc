//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)monitor.cc	1.21	08/05/20 SMI"

#include <sys/systm.h>
#include <sys/vmsystm.h>

#include <orb/invo/corba.h>
#include <orb/infrastructure/orb.h>
#include <orb/debug/orb_trace.h>
#include <orb/monitor/monitor.h>
#include <orb/infrastructure/common_threadpool.h>
#include <orb/infrastructure/clusterproc.h>

//
// The following enables debugging of this subsystem,
// and specifically enables the test callback and periodic
// printing of memory availability.
//
// #define	DEBUG_MONITOR 1

//
// When the amount of free memory is at or below this
// number of free pages, the Memory Monitor declares MEMORY_STARVED.
//
// This value must be higher than "throttlefree".
//
// The following value can (optionally) be set higher in the file "/etc/system".
//
pgcnt_t		memory_monitor_purge_threshold = 0;

//
// Specify the number of free pages at which the Memory Monitor
// will generate a syslog message declaring that the system
// has dangerously low level of free memory.
//
// This value should be higher than "throttlefree".
//
// The following value can (optionally) be set in the file "/etc/system".
//
pgcnt_t		memory_monitor_reporting_level = 0;

// Specify whether already reported dangerously low level of memory
bool		reported_memory_level = false;

// Specify the time interval for system monitoring
os::usec_t	monitor_time_period = 200000;	// 200 milli-seconds

// Specify number of seconds between printing memory statistics
int		monitor_print_interval = 60;

uint_t		monitor_low_threshold = 100;

monitor		*monitor::the_monitor = NULL;

// Number of seconds since memory statistics were last output
int		monitor::second_counter = 0;

#ifdef DEBUG_MONITOR
//
// test_callback_function - tests the monitor capability by printing out
// a message whenever the state changes.
//
void
test_callback_function(monitor::system_state_t state)
{
	os::printf("test_callback_function: state %d\n", state);
}
#endif

//
// monitor_task methods
//

monitor::monitor_task::monitor_task(callback_func callbackfunc) :
	flags(ACTIVE_CALLBACK),	// Task always immediately queued after creation
	last_known_state(UNKNOWN),
	callbackf(callbackfunc)
{
}

//
// execute - performs the actual callback
//
void
monitor::monitor_task::execute()
{
	system_state_t	new_state;

	ORB_DBPRINTF(ORB_TRACE_MONITOR,
	    ("monitor_task::execute last: %d\n", last_known_state));

	lck.lock();
	while ((last_known_state != (new_state = monitor::the().get_state())) &&
	    ((flags & DEREGISTERING) == 0)) {

		//
		// Drop the monitor lock around the callback so that
		// it becomes possible to request deregistration.
		//
		lck.unlock();
		(*callbackf)(new_state);
		lck.lock();

		last_known_state = new_state;

		ORB_DBPRINTF(ORB_TRACE_MONITOR,
		    ("monitor_task::execute new: %d\n", last_known_state));
	}
	// Show that the callback task is no longer active
	flags &= ~ACTIVE_CALLBACK;

	// Wake up anyone waiting for this callback to complete.
	cv.signal();

	lck.unlock();
}

//
// must_deregister - This call will block until the monitor_task is no longer
// active and it is safe to destroy it.
//
void
monitor::monitor_task::must_deregister()
{
	lck.lock();
	flags |= DEREGISTERING;

	while ((flags & ACTIVE_CALLBACK) != 0) {
		cv.wait(&lck);
	}
	lck.unlock();
}

//
// active - return true when the monitor task is already queued to
// deliver a callback.
//
bool
monitor::monitor_task::active()
{
	bool	result;
	lck.lock();
	result = (flags & ACTIVE_CALLBACK) != 0;
	lck.unlock();
	return (result);
}

inline monitor::callback_func
monitor::monitor_task::callback()
{
	return (callbackf);
}

//
// monitor methods
//

monitor::monitor() :
	prior_state(MEMORY_PLENTY),
	current_state(MEMORY_PLENTY)
{
}

// static
int
monitor::initialize()
{
	ASSERT(the_monitor == NULL);
	the_monitor = new monitor();
	if (the_monitor == NULL) {
		return (ENOMEM);
	}
	return (the_monitor->initialize_thread());
}

//
// initialize_thread - starts up the thread that
// periodically monitors the system state
//
int
monitor::initialize_thread()
{
	// Register with the list of orb threads
	registerthread();

	//
	// Create the thread that manages resources.
	// The priority should be a little higher than
	// most other cluster worker threads.
	//
	if (clnewlwp((void(*)(void *))monitor::monitor_thread, NULL,
	    66, NULL, NULL) != 0) {

		//
		// SCMSGS
		// @explanation
		// The system could not create the needed thread, because
		// there is inadequate memory.
		// @user_action
		// There are two possible solutions. Install more memory.
		// Alternatively, reduce memory usage. Since this happens
		// during system startup, application memory usage is normally
		// not a factor.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: failed to create thread for monitor");

		(void) unregisterthread();
		return (ENOMEM);
	}

#ifdef DEBUG_MONITOR
	//
	// Test the callback capability
	//
	subscribe(&test_callback_function);
#endif

	return (0);
}

//
// monitor_thread - becomes a thread that periodically manages resources
//
// static
void
monitor::monitor_thread(void *)
{
	// Initialized in monitor::initialize_thread

	os::usec_t	thread_sleep = monitor_time_period;

	monitor		&monitor_ = monitor::the();

	monitor_.lock();
	//
	// The monitor thread should loop forever unless told to shutdown
	//
				/* CSTYLED */
	while (true) {		//lint !e716 !e774
		//
		// Wake up after a time period or when ordered to shutdown
		//
		os::systime	time_out;
		time_out.setreltime(thread_sleep);
		(void) monitor_.threadcv.timedwait(&monitor_.threadLock,
		    &time_out);

		if (monitor_.state != orbthread::ACTIVE) {
			// Need to shutdown
			break;
		}

		//
		// Check the system state and publish state changes
		// as needed
		//
		monitor_.examine_system_state();
	}
	// The thread will now go away.
	monitor_.state = orbthread::DEAD;
	monitor_.threadGone.signal();
	monitor_.unlock();
}

//
// shutdown - stop the monitor
//
void
monitor::shutdown()
{
	if (the_monitor == NULL) {
		return;
	}
#ifdef DEBUG_MONITOR
	//
	// Test the callback capability
	//
	the_monitor->unsubscribe(&test_callback_function);
#endif

	// All of the callbacks should have been deregisterred
	ASSERT(the_monitor->subscribers.empty());

	delete the_monitor;
	the_monitor = NULL;
}

//
// subscribe - add one new subscriber.
// The subscriber will immediately get a callback with the current system state.
// After the initial callback, the system will deliver a callback
// only upon a state change.
// A subscriber must only register once.
//
// XXX - should a check for double registration.
//
void
monitor::subscribe(callback_func callbackfunc)
{
	monitor_task	*taskp = new monitor_task(callbackfunc);

	lck.lock();

	// Record the new subscriber
	subscribers.append(taskp);

	lck.unlock();

	//
	// Ensure that the subscriber gets a callback with the current
	// system state.
	//
	common_threadpool::the().defer_processing(taskp);
}

//
// unsubscribe -
// The operation blocks until all "in progress"
// callbacks have completed. After this method returns,
// no more callbacks will be made on this function.
//
void
monitor::unsubscribe(callback_func callbackfunc)
{
	monitor_task	*taskp;

	//
	// Find the matching callback
	//
	lck.lock();
	subscribers.atfirst();
	while ((taskp = subscribers.get_current()) != NULL &&
	    taskp->callback() != callbackfunc) {
		subscribers.advance();
	}

	ASSERT(taskp != NULL);
	if (taskp == NULL) {
		//
		// The specified callback was not registerred.
		//
		lck.unlock();
		return;
	}
	// Remove the monitor_task from the list, but do not destroy it.
	(void) subscribers.erase(taskp);

	lck.unlock();

	//
	// This call will block until the monitor_task is no longer
	// active and it is safe to destroy it.
	//
	taskp->must_deregister();
	delete taskp;
}

//
// examine_system_state - looks at the system state (memory availability).
// When the system state changes, the system publishes the state change.
//
void
monitor::examine_system_state()
{
	monitor_task	*taskp;
	pgcnt_t		purge_threshold;

	// Determine current memory status of the node.
	if (get_current_state() == MEMORY_PLENTY) {
		//
		// System is healthy.
		// Ensure that Memory Monitor will again report
		// dangerously low memory levels.
		//
		reported_memory_level = false;
	}

	//
	// We allow the system expert to set the level of dangerously low
	// memory to be anything. We do provide a default level.
	// Thus the reporting level is not tied to the purge levels.
	// Stops reporting if already reported for a particular state.
	//
	if ((freemem <= memory_monitor_reporting_level ||
	    freemem <= throttlefree + 10) &&
	    (!reported_memory_level)) {
		//
		// SCMSGS
		// @explanation
		// The system is reporting that the system has a very low
		// level of free memory.
		// @user_action
		// If the system fails soon after this message, then there is
		// a significantly greater chance that the system ran out of
		// memory. In which case either install more memory or reduce
		// system load. When the system continues to function, this
		// means that the system recovered and no user action is
		// required.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: memory low: freemem 0x%x", freemem);

		reported_memory_level = true;
	}

#ifdef DEBUG_MONITOR
	//
	// Print memory statistics when memory conditions change
	// or a time period expires.
	//
	if ((monitor_print_interval <= ++monitor::second_counter) ||
	    (prior_state != current_state) ||
	    (current_state == MEMORY_STARVED)) {
		os::printf("availrmem %d freemem %d\n", availrmem, freemem);
		monitor::second_counter = 0;
	}
#endif

	//
	// Make callbacks whenever the state changes
	// and if we are still at the bottom level.
	//
	if ((prior_state != current_state) ||
	    (current_state == MEMORY_STARVED)) {
		//
		// Publish the state change
		//
		lck.lock();

		ORB_DBPRINTF(ORB_TRACE_MONITOR,
		    ("examine_system_state queuing callbacks"
		    " prior %d current %d\n",
		    prior_state, current_state));

		subscribers.atfirst();
		while ((taskp = subscribers.get_current()) != NULL) {
			if (!taskp->active()) {

				// Show that the callback task is active
				taskp->set_active();

				common_threadpool::the().
				    defer_processing(taskp);
			}
			subscribers.advance();
		}
		lck.unlock();
	}
	prior_state = current_state;
}
