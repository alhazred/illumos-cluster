/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)xdoor_kit.cc	1.8	08/05/20 SMI"

#include <orb/xdoor/xdoor_kit.h>
#include <orb/xdoor/Xdoor.h>

// Xdoor kit stuff
xdoor_kit::xdoor_kit(const xkit_id_t kid, uint_t ml) :
    kit_id(kid), marshal_length(ml)
{
	xdoor_repository::the().add_kit(this);
}

xdoor_kit::~xdoor_kit()
{
	xdoor_repository::the().remove_kit(this);
}

// xdoor repository stuff

xdoor_repository the_xdoor_repository;

//
// add_kit - adds a kit to the repository.  Note that we only allow one kit of a
// given id to be added.  Anything else does not make sense.
// The maximum xdoor size is also updated.
//
void
xdoor_repository::add_kit(xdoor_kit *kitp)
{
	xkit_id_t	kid = kitp->get_kid();

	// Update the maximum xdoor size
	uint_t	new_size = kitp->get_marshal_len();
	if (new_size > max_len_per_xdoor) {
		max_len_per_xdoor = new_size;
	}

	ASSERT(kid < NUM_XDOOR_KITS);
	ASSERT(xdoor_kits[kid] == NULL);
	xdoor_kits[kid] = kitp;
}

//
// remove_kit - removes a kit from the repository, and
// recalculates the maximum xdoor size.
//
void
xdoor_repository::remove_kit(xdoor_kit *kitp)
{
	xdoor_kit	*xkitp;
	xkit_id_t	kid = kitp->get_kid();

	ASSERT(kid < NUM_XDOOR_KITS);
	ASSERT(xdoor_kits[kid] == kitp);
	xdoor_kits[kid] = NULL;

	// Recalculate the maximum xdoor size.
	uint_t	max_size = 0;
	for (int i = 0; i < NUM_XDOOR_KITS; i++) {
		xkitp = xdoor_kits[i];
		if (xkitp != NULL && max_size < xkitp->get_marshal_len()) {
			max_size = xkitp->get_marshal_len();
		}
	}
	max_len_per_xdoor = max_size;
}

xdoor_repository&
xdoor_repository::the()
{
	return (the_xdoor_repository);
};
