/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  solaris_xdoor_in.h
 *
 */

#ifndef _SOLARIS_XDOOR_IN_H
#define	_SOLARIS_XDOOR_IN_H

#pragma ident	"@(#)solaris_xdoor_in.h	1.9	08/05/20 SMI"

inline
solaris_xdoor_kit::solaris_xdoor_kit()
	: xdoor_kit(SOLARIS_XKID, sizeof (int))
{

}

inline void
solaris_xdoor_kit::marshal_roll_back(MarshalStream &)
{

}

// Inline methods for solaris_xdoor_server

inline
solaris_xdoor::solaris_xdoor(os::mutex_t *mp, handler *h)
	: handler_(h),
    mc_handler(0),
    umarshout(0),
    umarsh_ip(0),
    fd(-1),
    flags(0),
    lckp(mp),
    hold_count(0)
{

}

inline void
solaris_xdoor::marshal_cancel()
{

}

inline void
solaris_xdoor::marshal_local(Environment *)
{

}

inline handler *
solaris_xdoor::get_user_handler() const
{
	return (handler_);
}

inline bool
solaris_xdoor::reference_counting()
{
	return (true);
}

inline ExceptionStatus
solaris_xdoor::request_oneway(invocation &)
{
	ASSERT(0);
	return (CORBA::NO_EXCEPTION);
}


inline int
solaris_xdoor::get_fd()
{
	return (fd);
}


inline void
solaris_xdoor::set_fd(int fdesc)
{
	fd = fdesc;
}

inline door_id_t
solaris_xdoor::get_id(void)
{
	return (id);
}

inline void
solaris_xdoor::set_id(door_id_t did)
{
	id = did;
}

inline void
solaris_xdoor::lock()
{
	lckp->lock();
}

inline void
solaris_xdoor::unlock()
{
	lckp->unlock();
}

inline bool
solaris_xdoor::lock_held()
{
	return (lckp->lock_held());
}

inline os::mutex_t&
solaris_xdoor::get_lock()
{
	return (*lckp);
}


// Inline methods for solaris_xdoor_server

inline bool
solaris_xdoor_server::local()
{
	return (true);
}

inline bool
solaris_xdoor_server::reference_counting()
{
	return (unref_wanted);
}

inline ExceptionStatus
solaris_xdoor_server::request_twoway(invocation &)
{
	ASSERT(0);
	return (CORBA::NO_EXCEPTION);
}

// Inline methods for solaris_xdoor_client
inline bool
solaris_xdoor_client::local()
{
	return (false);
}

// caller of this function is responsible to grab invo_lck lock.
inline bool
solaris_xdoor_client::invocation_in_progress()
{
	ASSERT(solaris_xdoor_client::invo_lck.lock_held());
	return (invo_count > 0);
}

#endif	/* _SOLARIS_XDOOR_IN_H */
