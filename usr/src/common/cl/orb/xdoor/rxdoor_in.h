/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  rxdoor_in.h
 *
 */

#ifndef _RXDOOR_IN_H
#define	_RXDOOR_IN_H

#pragma ident	"@(#)rxdoor_in.h	1.47	08/05/20 SMI"

inline
rxdoor_id::rxdoor_id()
	:rxdoor_descriptor()
{
}

inline
rxdoor_id::rxdoor_id(ID_node &nd, xdoor_id xd) : rxdoor_descriptor(xd), node(nd)
{
}

inline
rxdoor_id::rxdoor_id(nodeid_t ndid, incarnation_num incn, xdoor_id xd) :
	rxdoor_descriptor(xd), node(ndid, incn)
{
}

// Directly put/get the fields of ID_node as it is hard to get the
// compiler to accept _put/_get methods at the early place it is defined
inline void
rxdoor_id::_put(MarshalStream &wms)
{
	rxdoor_descriptor::_put(wms);

	wms.put_unsigned_short(node.ndid);
	wms.put_unsigned_long(node.incn);
}

inline void
rxdoor_id::_get(MarshalStream &rms)
{
	rxdoor_descriptor::_get(rms);
	node.ndid = rms.get_unsigned_short();
	node.incn = rms.get_unsigned_long();
}

inline uint_t
rxdoor_id::_sizeof_marshal()
{
#ifdef MARSHAL_DEBUG
	//
	// 1 uint32_t for marshal type identifier for node id +
	// 1 uint32_t for marshal type for incarnation number +
	// 1 uint32_t for incarnation number itself +
	// 1 short for the actual node id +
	// rxdoor_descriptor::_sizeof_marshal()
	//
	return (3 * (uint_t)sizeof (uint32_t) + (uint_t)sizeof (uint16_t) +
	    rxdoor_descriptor::_sizeof_marshal());
#else
	//
	// 1 uint32_t for incarnation number +
	// 1 short for the actual node id +
	// rxdoor_descriptor::_sizeof_marshal()
	//
	return (2 * (uint_t)sizeof (uint32_t) + (uint_t)sizeof (uint16_t) +
	    rxdoor_descriptor::_sizeof_marshal());
#endif
}

inline void
rxdoor_bucket::lock()
{
	lck.lock();
}

inline void
rxdoor_bucket::unlock()
{
	lck.unlock();
}

inline bool
rxdoor_bucket::lock_held()
{
	return (lck.lock_held());
}

inline rx_list *
rxdoor_bucket::list_hash(xdoor_id xdid)
{
	uint_t hashval = xdid & (rnp->num_lists - 1);
	return (rnp->lists + hashval);
}

inline void
rxdoor_bucket::store(rxdoor *xdp)
{
	ASSERT(lock_held());
	xdp->set_bucket(this);
	list_hash(xdp->server_xdoor)->prepend(xdp->get_list_elem());
}

inline void
rxdoor_bucket::erase(rxdoor *xdp)
{
	ASSERT(lock_held());
	(void) list_hash(xdp->server_xdoor)->erase(xdp->get_list_elem());
}

inline rxdoor_bucket *
rxdoor_node::bucket_hash(xdoor_id xdid)
{
	return (&buckets[(xdid >> bucket_shift) & (num_buckets - 1)]);
}

//
// rxdoor_membership_manager
//

inline rxdoor_membership_manager &
rxdoor_membership_manager::the()
{
	ASSERT(rxdmm != NULL);
	return (*rxdmm);
}


inline void
rxdoor_membership_manager::block_translate_in()
{
	reconfiguration_lock.wrlock();
}

inline void
rxdoor_membership_manager::unblock_translate_in()
{
	reconfiguration_lock.unlock();
}

inline void
rxdoor_membership_manager::begin_membership_lock()
{
	reconfiguration_lock.rdlock();
}

inline void
rxdoor_membership_manager::end_membership_lock()
{
	reconfiguration_lock.unlock();
}

//
// rxdoor methods
//

inline ID_node &
rxdoor_node::get_node()
{
	return (node);
}

inline ID_node &
rxdoor::get_server_node()
{
	return (bucketp->rnp->get_node());
}

//
// This is called from OxSchema::descriptor() to get the nodeid of the
// server for this Xdoor.  If the server is in a user-domain ORB on
// this node, we return NODEID_UNKNOWN so the object will be queried
// for its type.
//
inline nodeid_t
rxdoor::get_server_nodeid()
{
	if (server_handler_index == KERNEL_HANDLER)
		return (bucketp->rnp->get_node().ndid);
	else
		return (NODEID_UNKNOWN);
}

inline void
rxdoor::set_bucket(rxdoor_bucket *xbp)
{
	bucketp = xbp;
}

inline rxdoor_bucket *
rxdoor::get_bucket()
{
	return (bucketp);
}

inline rxdoor_elem *
rxdoor::get_list_elem()
{
	return (&elem);
}

inline void
rxdoor::lock()
{
	bucketp->lock();
}

inline void
rxdoor::unlock()
{
	bucketp->unlock();
}

inline os::mutex_t &
rxdoor::get_lock()
{
	return (bucketp->lck);
}

inline os::condvar_t &
rxdoor::get_cv()
{
	return (bucketp->cv);
}

inline bool
rxdoor::lock_held()
{
	return (bucketp->lock_held());
}

inline size_t
rxdoor::sizeof_reply_header()
{
	return (sizeof (rxdoor_reply_header));
}

#endif	/* _RXDOOR_IN_H */
