/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RXDOOR_DEFS_H
#define	_RXDOOR_DEFS_H

#pragma ident	"@(#)rxdoor_defs.h	1.4	08/05/20 SMI"

#include <orb/xdoor/xdoor_kit.h>

typedef uint32_t  xdoor_id;

// Xdoor constant definitions
const xdoor_id INVALID_XDOOR_ID = UINT_MAX;
const xdoor_id XDOOR_MAXFIXED = 16; // must be power of 2
const xdoor_id XDOOR_MAXDID = UINT_MAX - 1;

// forward delcaration
class rxdoor_ref;


class rxdoor_descriptor {
public:
	rxdoor_descriptor();
	rxdoor_descriptor(xdoor_id);

	virtual void	_put(MarshalStream &wms);
	virtual void	_get(MarshalStream &rms);

	static	uint_t	_sizeof_marshal();
	virtual void	put_ref_job(rxdoor_ref *) = 0;
	xdoor_id	xdid;
};

//
// rxdoor_ref - stores one reference count job for an xdoor. This can be
// a register (reg) or unregister (unreg) job.
//
class rxdoor_ref {
public:
	rxdoor_ref();
	void			_put(MarshalStream &);
	void			_get(MarshalStream &);
	xkit_id_t		get_kit_id();

	xdoor_id	xdid;
	ushort_t	serviceid; 	// Used by hxdoor
	ID_node		lent_node;
	bool		is_reg;

	static const uint_t _sizeof_marshal;
};

#include <orb/xdoor/rxdoor_defs_in.h>

#endif	/* _RXDOOR_DEFS_H */
