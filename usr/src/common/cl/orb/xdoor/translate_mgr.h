/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TRANSLATE_MGR_H
#define	_TRANSLATE_MGR_H

#pragma ident	"@(#)translate_mgr.h	1.29	08/05/20 SMI"

#include <sys/clconf_int.h>
#include <orb/xdoor/Xdoor.h>
#include <h/cmm.h>

class translate_mgr {

public:
	translate_mgr();
	~translate_mgr();

	static	translate_mgr &the();
	static int	initialize();
	static void	shutdown();

	void new_membership(const cmm::membership_t &membership);

	//
	// begin_tr_out() and done_tr_out() must bracket all calls to
	// translate_out.
	// infop points to information about the translate out.
	//
	void	begin_tr_out(ID_node &nd, tr_out_info *infop, Environment *);

	//
	// done_tr_out() or done_tr_out_not_sent() must be called if a call to
	// begin_tr_out succeeded.
	void	done_tr_out(ID_node &nd);
	void	done_tr_out_not_sent(ID_node &nd, tr_out_info *infop);

	// Called on the receiving side when the xdoors have been translated in
	// or the translate_in has been canceled. Note that this must be done
	// after the unmarshal() (or unmarshal_cancel()) of the individual
	// xdoors.
	void	done_tr_in(ID_node &nd, uint64_t translate_info);

	// Called by the reference counting code when an ack is received
	// indicating that the translate_in corresponding to the translate_out
	// has been performed on the remote node.
	void	ack_translates(ID_node &, tr_out_info *);

#ifdef DEBUGGER_PRINT
	void	mdb_dump_tr_nodes();
#endif

private:
	void	shutdown_int();

	static translate_mgr *the_translate_mgr;

	// Per node data structure.
	struct tr_node {
		tr_node();
#ifdef DEBUGGER_PRINT
		void	mdb_dump_tr_out_info();
#endif
		incarnation_num	incn;	// Current incarnation number.
		os::rwlock_t	rwlck;	// Get read lock when translating.
		// List of unacknowledged translates to the node.
		IntrList<tr_out_info, _DList> in_transit;
	};

	os::mutex_t	lck;
	tr_node tr_nodes[NODEID_MAX + 1];
};


#include <orb/xdoor/translate_mgr_in.h>

#endif	/* _TRANSLATE_MGR_H */
