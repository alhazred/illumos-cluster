/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rxdoor_defs.cc	1.3	08/05/20 SMI"

#include <orb/xdoor/rxdoor_defs.h>

// rxdoor_descriptor methods

// Directly put/get the fields of ID_node as it is hard to get the
// compiler to accept _put/_get methods at the early place it is defined
void
rxdoor_descriptor::_put(MarshalStream &wms)
{
	wms.put_unsigned_int(xdid);
}

void
rxdoor_descriptor::_get(MarshalStream &rms)
{
	xdid = rms.get_unsigned_int();
}

// rxdoor_ref

// the marshal size of a rxdoor_ref job

const uint_t rxdoor_ref::_sizeof_marshal =
#ifdef MARSHAL_DEBUG
		//
		// 4 debug bytes one each for is_reg, kit_id, xdid and
		// serviceid. service id is assigned a value for hxdoors
		// and not for std. and simple xdoors.
		//

		4 * (uint_t)(sizeof (uint_t)) +
#endif
		// is_reg
		((uint_t)sizeof (bool)) +
		// kit_id
		((uint_t)sizeof (xkit_id_t)) +
		// xdid
		((uint_t)sizeof (xdoor_id)) +
		// service id in case of hxdoor. XXX Remember to
		// change this if the size of replica::service_num_t
		// changes.  ((uint_t)sizeof (replica::service_num_t)));
		((uint_t)sizeof (uint16_t));
