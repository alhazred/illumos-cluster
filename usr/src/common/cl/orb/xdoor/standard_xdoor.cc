//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)standard_xdoor.cc	1.179	08/09/19 SMI"

#include <orb/xdoor/standard_xdoor.h>
#include <orb/handler/handler.h>
#include <orb/refs/refcount.h>
#include <orb/fault/fault_injection.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/xdoor/rxdoor_header.h>
#include <sys/mc_probe.h>
#include <orb/debug/orb_trace.h>


const char	*standard_xdoor_client::xdoor_type_string = "std_c";

standard_xdoor_to_client_kit standard_xdoor_client::to_client_kit;
standard_xdoor_to_server_kit standard_xdoor_client::to_server_kit;
standard_xdoor_from_server_kit standard_xdoor_server::from_server_kit;

//
// unmarshal_cancel_remainder -
//
// A decrement is sent to the sender, because we will not be using the
// reference we were sent.
//
void
standard_xdoor_kit::unmarshal_cancel_remainder(ID_node &from_node,
    rxdoor_descriptor &rxdid, MarshalStream &rms)
{
	rxdoor_id &did = (rxdoor_id &)rxdid;

	// standard_xdoors sent by the server have an extra boolean indicating
	// whether the server performed an implicit register as part of its
	// marshal.
	if (ID_node::match(from_node, did.node)) {
		bool server_new = rms.get_boolean();
		if (server_new) {
			// The server performed an implicit register when it
			// marshaled this.  We send an unreg to make up for
			// that.
			// Note that it doesn't matter if another unmarshal
			// has already created the xdoor on this node. We need
			// to do an unregister either way.
			refcount::the().add_unreg(did.node, rxdid);
		}
	}
}

rxdoor *
standard_xdoor_to_client_kit::create(rxdoor_descriptor &rxdid)
{
	return (standard_xdoor_client::create(rxdid));
}

rxdoor *
standard_xdoor_to_server_kit::create(rxdoor_descriptor &)
{
	ASSERT(0);
	return (NULL);
}

rxdoor *
standard_xdoor_from_server_kit::create(rxdoor_descriptor &rxdid)
{
	return (standard_xdoor_client::create(rxdid));
}

// Methods common to standard_xdoor_server and standard_xdoor_client

//
// reference_counting - all standard xdoors support reference counting
//
bool
standard_xdoor::reference_counting()
{
	return (true);
}

//
// unmarshal_roll_back
//
// Xdoor was unmarshalled but the orb decided to not accept
// this invocation. This may happen if the node from which this
// request came has died.
void
standard_xdoor::unmarshal_roll_back()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ASSERT(umarshout > 0);		// umarshout is unsigned
	umarshout--;
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): unmarshal_roll_back uo=%d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    umarshout));
	if (umarshout == 0) {
		handle_possible_unreferenced();
	}
	lckp->unlock();
}

//
// SERVER
//

//
// register_client - track the new client reference in the bitmask.
//
void
standard_xdoor_server::register_client(ID_node &node)
{
	ASSERT(lock_held());

	ASSERT(extramask.bitmask() ==
	    (extramask.bitmask() & refmask.bitmask()));
	if (refmask.contains(node.ndid)) {
		// We have marshaled a reference to this node. Note the
		// registration in the extra bitmask and expect an  unregister
		// later when the client receives that reference that we
		// sent..
		ASSERT(!extramask.contains(node.ndid));
		extramask.add_node(node.ndid);
	} else {
		refmask.add_node(node.ndid);
	}

	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("%s(%d.%3d:%d): register_client from %d masks %llx.%llx cnt %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    node.ndid, refmask.bitmask(), extramask.bitmask(), foreignrefs));
}

//
// unregister_client - Remove the client reference from the bitmask.
//	The reference count may have dropped to zero, and then it will
//	become unreferenced.
//
void
standard_xdoor_server::unregister_client(ID_node &node)
{
	ASSERT(lock_held());

	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("%s(%d.%3d:%d):unregister_client from %d masks "
	    "%llx.%llx cnt %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    node.ndid, refmask.bitmask(), extramask.bitmask(), foreignrefs));

	ASSERT(extramask.bitmask() ==
	    (extramask.bitmask() & refmask.bitmask()));
	ASSERT(refmask.contains(node.ndid));
	if (extramask.contains(node.ndid)) {
		extramask.remove_node(node.ndid);
	} else {
		refmask.remove_node(node.ndid);
		if (refmask.is_empty()) {
			handle_possible_unreferenced();
		}
	}
}

//
// Called during reconfiguration when some nodes died. We remove the node
// from our bitmasks and check if we are unreferenced.
//
void
standard_xdoor_server::cleanup_foreignrefs(const nodeset &dead_nodeset)
{
	ASSERT(lock_held());
	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("%s(%d.%3d:%d): cln_for dead=%llx %llx.%llx\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    dead_nodeset.bitmask(), refmask.bitmask(), extramask.bitmask()));

	ASSERT(extramask.bitmask() ==
	    (extramask.bitmask() & refmask.bitmask()));
	refmask.remove_nodes(dead_nodeset);
	extramask.remove_nodes(dead_nodeset);
	handle_possible_unreferenced();
}

//
// identify_client_nodes - Identify other nodes holding reference
// (this ignores local node). This provides only a hint about
// which nodes hold references, because this information can change
// at any time.
//
// Warning: in order to safely use this feature, you should know
// something about the activity of this specific object's references at the time
// this function is called that ensures that this information does not change.
//
void
standard_xdoor_server::identify_client_nodes(nodeset &refset)
{
	refset.set(refmask);
}

//
// marshal_roll_back -
//
// The foreign reference count is decremented to make up for the increment
// in marshal.
//
void
standard_xdoor_server::marshal_roll_back(ID_node &node, MarshalStream &rms)
{
	ASSERT(lock_held());
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d):marshal_roll_back from %d masks %llx.%llx cnt %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    node.ndid, refmask.bitmask(), extramask.bitmask(), foreignrefs));

	bool new_one = rms.get_boolean();

	//
	// Undoes the reference increment in marshal, and checks for possible
	// unreference. Note that the xdoor may be deleted in the call.
	//
	foreignrefs--;
	if (foreignrefs == 0) {
		//
		// Signal to indicate that this door does not have any
		// foreign references. This is used for hxdoor by
		// hxdoor_service, when it is waiting for in-transit
		// references for hxdoor to be delivered.
		//
		get_cv().broadcast();
	}
	if (new_one) {
		unregister_client(node);
	} else {
		handle_possible_unreferenced();
	}
}

//
// unreferenced_done - the xdoor now self-destructs.
// Called with the lock held.
// There is no activity involving this xdoor, as shown by the asserts.
//
void
standard_xdoor_server::unreferenced_done()
{
	ASSERT(lock_held());
	ASSERT(!has_foreignrefs());
	ASSERT(services == 0);
	ASSERT(umarshout == 0);
	ASSERT(handlers[0] == NULL);
	ASSERT(handlers[1] == NULL);
	ASSERT(!(flags & REVOKE_PENDING));

	flags |= UNREFERENCED;

	ORB_DBPRINTF(ORB_TRACE_OBJEND, ("%p unref done\n", this));

	rxdoor_manager::the().release_rxdoor(this);
}

//
// accept_unreferenced -
// The name of this method is somewhat misleading,
// as the xdoor may have references.
// This method always disconnects the server handler from the xdoor,
// and there is never any further communication with the server handler.
// The xdoor will self-destruct unless a revoke occurred while the
// xdoor was still active.
// Xdoor lock must be held by the caller.
//
void
standard_xdoor_server::accept_unreferenced()
{
	ASSERT(lock_held());
	ASSERT((flags & REVOKE_PENDING) == 0);

	// Clear UNREF_PENDING flag, the handler calls this in unreferenced()
	ASSERT((flags & UNREF_PENDING) != 0);
	flags &= ~UNREF_PENDING;


	// Set the server_handler to NULL.
	ASSERT(get_server_handler() != NULL);
	set_server_handler(NULL);

	//
	// If the handler is disconnecting due to a revoke, there may still
	// be foreignrefs or a client handler. Do not release the xdoor
	// in this case.
	//
	if (!has_foreignrefs() && (get_client_handler() == NULL)) {
		unreferenced_done();
	} else {
		ASSERT(flags & REVOKED);
	}
}

//
// deliver_unreferenced
//
//    process an unref_task for a standard_xdoor_server
//
void
standard_xdoor_server::deliver_unreferenced()
{
	os::mutex_t	*lckp = &get_lock();
	handler		*handp = get_server_handler();
	uint_t		handler_marshal_count;
	bool		xdoor_unref = false;

	// begin_xdoor_unref acquires the handler lock
	handler_marshal_count = handp->begin_xdoor_unref();

	lckp->lock();

	ASSERT(flags & UNREF_PENDING);
	flags &= ~UNREF_PENDING;

	ORB_DBPRINTF(ORB_TRACE_OBJEND,
	    ("%s(%d.%3d:%d): deliver unref: %p ,"
	    "flags = %d, h_m_c = %d, marshalsmade = %d,\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    this, flags, handler_marshal_count,
	    marshalsmade));

	if ((handler_marshal_count == marshalsmade) &&
	    ((flags & MARSHAL_DURING_UNREF) == 0)) {
		//
		// The xdoor can go away
		//
		xdoor_unref = true;

		//
		// The handler has not marshaled this xdoor since we
		// queued the unreferenced notification.
		// We can now destroy this xdoor and notify the handler
		// that we are gone.
		//

		set_server_handler(NULL);

		//
		// If the handler is disconnecting due to a revoke,
		// there may still be foreignrefs or a client handler.
		// Do not release the xdoor in this case.
		//
		if (!has_foreignrefs() &&
		    (get_client_handler() == NULL)) {
			unreferenced_done();
			// This xdoor is now deleted.
		} else {
			//
			// If the xdoor has been revoked, then the
			// xdoor needs to stay around until foreign
			// references go away.  But the handler can
			// now be unreferenced.
			//
			ASSERT(flags & REVOKED);
		}
	} else if (handler_marshal_count == marshalsmade) {
		//
		// Conditions may again call for an unreference.
		// For example, a reference given out while processing
		// the unref notice may already have gone away.
		//
		flags &= ~MARSHAL_DURING_UNREF;
		handle_possible_unreferenced();
	}
	lckp->unlock();
	handp->end_xdoor_unref(xdoor_unref);
}

//
// revoked - The user is revoking the object.
// All future requests on this xdoor are rejected.
// The xdoor remains valid.
// This operation can only be done once.
//
void
standard_xdoor_server::revoked()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ORB_DBPRINTF(ORB_TRACE_OBJEND,
	    ("%s(%d.%3d:%d): revoked\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));
	ASSERT(!(flags & (REVOKED | REVOKE_PENDING)));
	flags |= REVOKE_PENDING;

	// We finish revoke when no invocations are in in progress.
	while (services != 0) {
		revoke_cv.wait(&get_lock());
	}

	flags = (flags | REVOKED) & ~REVOKE_PENDING;

	//
	// Handler increments marshcount to prevent handler from getting
	// unreferenced and disconnecting xdoor.
	// We do an implicit marshal_cancel() to account for this.
	//
	marshalsmade++;
	ASSERT(marshalsmade != 0);
	handle_possible_unreferenced();
	lckp->unlock();
}

// A client handler has just been released, account for its marshals, making
// the marshal count match the remaining (server) handler.
void
standard_xdoor_server::dec_marshcount(uint_t marshcnt)
{
	ASSERT(lock_held());
	ASSERT(marshalsmade >= marshcnt);
	marshalsmade -= marshcnt;
}

bool
standard_xdoor_server::has_foreignrefs()
{
	ORB_DBPRINTF(ORB_TRACE_OBJEND, ("%p has_for for=%d masks=%llx.%llx\n",
	    this, foreignrefs, refmask.bitmask(), extramask.bitmask()));

	if (refmasks_empty() && (foreignrefs == 0)) {
		return (false);
	}
	return (true);
}

//
// handle_possible_unreferenced - if all activity on this xdoor has ceased,
// initiate xdoor unreference operation.
//
void
standard_xdoor_server::handle_possible_unreferenced()
{
	ASSERT(lock_held());
	ORB_DBPRINTF(ORB_TRACE_OBJEND, ("%p hdl_pos_unref\n", this));
	//
	// If REVOKED xdoor, foreignrefs may not be 0, but we still need to
	// deliver unreferenced to the handler.
	//
	if (services == 0 &&
	    umarshout == 0 &&
	    ((!has_foreignrefs() && get_client_handler() == NULL) ||
		(flags & REVOKED)) &&
	    !(flags & (REVOKE_PENDING | UNREF_PENDING))) {
		//
		// There is no activity on this xdoor.
		//
		ASSERT(!(flags & UNREFERENCED));
		handler *shp = get_server_handler();
		if (shp == NULL) {
			//
			// We may still have had a client handler
			// or some foreignrefs for a revoked xdoor
			//
			ASSERT(flags & REVOKED);
			if (get_client_handler() == NULL &&
			    !has_foreignrefs()) {
				ASSERT(extramask.is_empty());
				unreferenced_done();
			}
		} else {
			//
			// We have no more references so schedule
			// ourselves for unreference.
			// deliver_unreferenced() will then delete the
			// xdoor if no new references were given out
			// during the time it was on the unreferenced
			// queue.
			//
			flags |= UNREF_PENDING;

			ORB_DBPRINTF(ORB_TRACE_OBJEND,
			    ("%p unref schd\n", this));

			// Schedule unreferenced notification for this xdoor
			unref_threadpool::the().defer_unref_processing(this);
		}
		// At this point the Xdoor may have been deleted.
		// At a minimum the xdoor has been unreferenced.
	}
}

//
// get_unref_handler - return the server handler for use by the unref_task.
//
handler *
standard_xdoor_server::get_unref_handler()
{
	ASSERT(get_server_handler() != NULL);
	return (get_server_handler());
}

//
// unref_task_type
//
unref_task_type_t
standard_xdoor_server::unref_task_type()
{
	return (unref_task::XDOOR);
}

//
// reserve_resources - Identify resources needed at xdoor level for request.
// This supports "local" U->K and K->U invocations.
// No xdoor header is needed for local communications.
// Returns a send stream pointer, which can be NULL in case of error.
// This method is used for invocation messages and not reply messages.
//
sendstream *
standard_xdoor_server::reserve_resources(resources *resourcep)
{
	resourcep->set_nodeid(orb_conf::node_number());
	return (orb_msg::reserve_resources(resourcep));
}

//
// reserve_reply_resources - Identify resources needed for invocation reply.
// The server rxdoor does not know where the reply should go.
// The upper layer must specify the destination node.
//
sendstream *
standard_xdoor_server::reserve_reply_resources(resources *resourcep)
{
	// Obtain destination node from resources object
	ID_node		*nodep = resourcep->get_nodep();

	if (nodep->ndid != orb_conf::node_number()) {
		// Only remote invocation requires msg header
		resourcep->add_send_header(sizeof_reply_header());
	}

	// Have the lower protocol layers specify its requirements
	return (orb_msg::reserve_resources(resourcep));
}

//
// marshal -  when an xdoor marshals its reference, it increases
// the reference count.
//
// A standard xdoor marshals its xdoor type, its own rxdoor_id (whole).
//
void
standard_xdoor_server::marshal(ID_node &node, MarshalStream &wms,
    Environment *e)
{
	os::mutex_t	*lckp = &get_lock();
	lckp->lock();

	ASSERT(!(flags & UNREFERENCED));
	//
	// Only marshal valid xdoors that have not been revoked
	//
	if (flags & (REVOKED | REVOKE_PENDING)) {
		ORB_DBPRINTF(ORB_TRACE_MARSHAL,
		    ("%s(%d.%3d:%d): marshal failed\n",
		    xdoor_type(), get_server_node().ndid,
		    (get_server_node().incn % 1000), server_xdoor));

		//
		// Synchronize marshal count with handler
		// even though not marshalled.
		//
		marshalsmade++;
		ASSERT(marshalsmade != 0);

		//
		// We call unreferenced as the xdoor may no longer be needed.
		//
		handle_possible_unreferenced();
		e->system_exception(CORBA::BAD_PARAM(0,
		    CORBA::COMPLETED_MAYBE));
		lckp->unlock();
		return;
	}
	do_marshal(node, wms);

	lckp->unlock();
}

void
standard_xdoor_server::do_marshal(ID_node &node, MarshalStream &wms)
{
	ASSERT(lock_held());
	if ((flags & UNREF_PENDING) != 0) {
		//
		// A marshal is occuring while an unref task is pending.
		//
		// The MARSHAL_DURING_UNREF flag must be set before the
		// marshal count is incremented. This requirement exists
		// in order to ensure that any object reference marshal
		// beginning after the queueing of a xdoor unreference task
		// will result in the rejection of that xdoor unreference.
		//
		flags |= MARSHAL_DURING_UNREF;
	}
	// Synchronize marshal count with handler.
	marshalsmade++;
	ASSERT(marshalsmade != 0);

	ASSERT(extramask.bitmask() ==
	    (extramask.bitmask() & refmask.bitmask()));

	bool is_new;
	foreignrefs++;
	ASSERT(foreignrefs != 0); // Check for wraparound
	is_new = !refmask.contains(node.ndid);
	if (is_new) {
		ASSERT(!extramask.contains(node.ndid));
		refmask.add_node(node.ndid);
	}
	ASSERT(get_server_node().ndid != 0);

	//	Marshall the xdoor
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%p %s(%d.%3d:%d): marshal to %d masks %llx.%llx count %d\n",
	    this, xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    node.ndid, refmask.bitmask(), extramask.bitmask(), foreignrefs));

	get_kit().marshal_xdoor(wms, this);

	// put flag indicating if this is new xdoor being sent to the node
	wms.put_boolean(is_new);
}

//
// marshal_cancel - is called when the marshaling is canceled.
// The marshal count is reconciled with that of the handler.
// It is possible that this will allow the xdoor to be deleted.
//
void
standard_xdoor_server::marshal_cancel()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%s(%d.%3d:%d): marshal_cancel:\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));

	marshalsmade++;
	ASSERT(marshalsmade != 0);
	handle_possible_unreferenced();
	lckp->unlock();
}

//
// marshal_local - an invocation is occurring on the local machine.
//
void
standard_xdoor_server::marshal_local(Environment *e)
{
	lock();

	// Increment marshal and unmarshal counts to match handler
	marshalsmade++;
	umarshout++;
	ASSERT(marshalsmade != 0);
	ASSERT(umarshout != 0);

	if ((flags & UNREF_PENDING) != 0) {
		//
		// A marshal is occuring while an unref task is pending.
		//
		flags |= MARSHAL_DURING_UNREF;
	}

	// Always increment umarshout even on exception, roll_back will recover
	if (flags & (REVOKED | REVOKE_PENDING)) {
		e->system_exception(CORBA::BAD_PARAM(0,
				CORBA::COMPLETED_MAYBE));
	}
	unlock();
}

void
standard_xdoor_server::marshal_received()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();

	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): marshal_received masks %llx.%llx count %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    refmask.bitmask(), extramask.bitmask(), foreignrefs));
	ASSERT(foreignrefs > 0);
	if (--foreignrefs == 0) {
		get_cv().broadcast();
		handle_possible_unreferenced();
	}
	lckp->unlock();
}

//
// unmarshal - a local xdoor id description for the client is in the
// marshal stream. The method sends a decrease reference message to the
// client on the other node.
//
void
standard_xdoor_server::unmarshal(ID_node &node, MarshalStream &, bool,
		Environment *e)
{
	ASSERT(lock_held());

	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): unmarshal server from %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor, node.ndid));

	ASSERT(!(flags & UNREFERENCED));
	//
	// Unmarshal will fail if the server xdoor has been revoked.
	// The server xdoor no longer has a handler,
	// so unmarshaling the server does not make sense.
	//
	if (flags & (REVOKED | REVOKE_PENDING)) {
		e->system_exception(CORBA::BAD_PARAM(0,
				CORBA::COMPLETED_MAYBE));
		return;
	}
	umarshout++;
	ASSERT(umarshout != 0);
}

//
// incoming_prepare - determine whether this xdoor will accept this invocation.
//
void
standard_xdoor_server::incoming_prepare(Environment *e)
{
	ASSERT(lock_held());

	//
	// An xdoor only enters the UNREFERENCED state when the system
	// has confirmed that no references exist. Without references,
	// nobody can make an invocation.
	//
	ASSERT((flags & UNREFERENCED) == 0);
	ASSERT(((flags & UNREF_PENDING) == 0) ||
	    (flags & MARSHAL_DURING_UNREF));

	//
	// Validate that the xdoor is accepting invocations.
	//
	if ((flags & (REVOKED | REVOKE_PENDING)) != 0) {
		e->system_exception(CORBA::INV_OBJREF(ECANCELED,
			CORBA::COMPLETED_NO));
	} else {
		services++;
		ASSERT(services != 0);
	}
}

//
// incoming_done -	A remote invocation has completed.
//			Check for pending revoke or unreferenced.
//
void
standard_xdoor_server::incoming_done()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();

	ASSERT(services > 0);
	if (--services == 0) {
		// A pending revoke must be serviced prior to
		//	a pending unreferenced
		if (flags & REVOKE_PENDING) {
			revoke_cv.broadcast();
		} else {
			// The xdoor may now be unreferenced.
			handle_possible_unreferenced();
		}
	}
	lckp->unlock();
}

//
// create - creates an xdoor for the specified handler.
//
Xdoor *
standard_xdoor_server::create(handler *h)
{
	standard_xdoor_server *xdp;

	xdp = new standard_xdoor_server(h);

	rxdoor_manager::the().register_rxdoor(xdp);

	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%p %s(%d.%3d:%d): created\n",
	    xdp, xdp->xdoor_type(), xdp->get_server_node().ndid,
	    (xdp->get_server_node().incn % 1000), xdp->server_xdoor));

	return (xdp);
}

bool
standard_xdoor_server::local()
{
	return (true);
}

//
// We should forward this to the handler so that we can detect
// dead userland servers.
//
bool
standard_xdoor_server::server_dead()
{
	return (false);
}

const char *
standard_xdoor_server::xdoor_type()
{
	return ("std_s");
}

rxdoor_kit&
standard_xdoor_server::get_kit()
{
	return (standard_xdoor_server::from_server_kit);
}

//
// CLIENT
//

//
// inc_foreignref - increment the foreign reference count.
//
inline void
standard_xdoor_client::inc_foreignref()
{
	ASSERT(lock_held());

	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("%s(%d.%3d:%d): foreignrefs + 1 = %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    foreignrefs + 1));

	foreignrefs++;
	ASSERT(foreignrefs != 0);
}

//
// dec_foreignref - decrement the reference count.
//	The reference count may have dropped to zero, and then we may
//	become unreferenced.
//
inline void
standard_xdoor_client::dec_foreignref()
{
	ASSERT(lock_held());

	ORB_DBPRINTF(ORB_TRACE_REFCNT,
	    ("%s(%d.%3d:%d): foreignrefs - 1 = %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    foreignrefs - 1));

	ASSERT(foreignrefs != 0);

	if (--foreignrefs == 0)
		handle_possible_unreferenced();
}

//
// marshal_roll_back -
//
// The foreign reference count is decremented to make up for the increment
// in marshal.
//
void
standard_xdoor_client::marshal_roll_back(ID_node &, MarshalStream &)
{
	ASSERT(lock_held());
	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%s(%d.%3d:%d): marshal_roll_back: ",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));

	// Undoes the reference increment in marshal, and
	// checks for possible unreference.
	// Note that the xdoor may be deleted in the call.
	dec_foreignref();
}

// We don't keep track of marshal counts on the client side.
void
standard_xdoor_client::dec_marshcount(uint_t)
{
}

//
// handle_possible_unreferenced
//
// Called whenever there may be no more references to this xdoor.
// Usually the xdoor can be deleted at this stage, but it
// may need to be held around for the following reasons:
//
// 1) There still exists a handler for this xdoor.
//
// 2) A new reference to the object is in the process of being
// unmarshaled. The xdoor has been unmarshaled, but the handler portion
// has not.  This is caught by checking the umarshout count.
//
// 3) The xdoor has a non-zero foreign reference count. It must be
// kept around until the reference counting protocol completes.
//
// Any xdoors which are not released because an unmarshal is about to occur
// will soon be useful again, or the unmarshal will be rolled_back causing this
// function to be called again.
//
void
standard_xdoor_client::handle_possible_unreferenced()
{
	ASSERT(lock_held());

	if ((handlers[0] == NULL) && (handlers[1] == NULL) &&
	    (umarshout == 0) && (foreignrefs == 0)) {
		//
		// All handlers are gone and there are no foreign
		// or local references associated with the xdoor.
		// We get rid of it.
		//

		// First send an UNREGISTER to the server.
		rxdoor_id rxd;
		get_xdoor_desc(rxd);
		refcount::the().add_unreg(get_server_node(), rxd);

		ORB_DBPRINTF(ORB_TRACE_OBJEND,
		    ("%s(%d.%3d:%d): releasing %p\n",
		    xdoor_type(), get_server_node().ndid,
		    (get_server_node().incn % 1000), server_xdoor, this));
		rxdoor_manager::the().release_rxdoor(this);
	}
}

//
// reserve_resources - Identify resources needed at xdoor level for request.
// Returns a send stream pointer, which can be NULL in case of error.
// This method is used for invocation messages and not reply messages.
//
sendstream *
standard_xdoor_client::reserve_resources(resources *resourcep)
{
	MC_PROBE_0(std_xdr_reserve_start, "clustering orb xdoor", "");
	if (resourcep->get_invo_mode().is_oneway()) {
		resourcep->add_send_header(sizeof (rxdoor_oneway_header));
	} else {
		// remote invocation
		resourcep->add_send_header(sizeof (rxdoor_twoway_header));
	}
	resourcep->set_node(get_server_node());

	// Have the lower protocol layers specify its requirements
	return (orb_msg::reserve_resources(resourcep));
}

//
// marshal - loads the global xdoor id.
// We temporarily increase our foreign reference count until we are notified
// that the receiver has registered with the server. This prevents our xdoor
// from being deleted which in turn ensures that the server will not delete
// itself before it hears about the new object which is being sent.
//
// The Orb currently allows a client xdoor to be marshalled even if
// the server xdoor is revoked or the server node is dead. Thus, if the
// client xdoor exists, the Orb will marshal it.
//
void
standard_xdoor_client::marshal(ID_node &node, MarshalStream &wms, Environment *)
{
	lock();
	inc_foreignref();
	unlock();

	// Marshal the xdoor
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): marshal to %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor, node.ndid));

	get_kit().marshal_xdoor(wms, this);
}

// Nothing to do.
void
standard_xdoor_client::marshal_cancel()
{
	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%s(%d.%3d:%d): marshal_cancel\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));
}

//
// unmarshal
// If this is from the server, then the server has indicated whether it knew
// of any references. If the server was right, then we don't need to do any
// extra reference counting. If not then we need to send a register or
// unregister to make up for the server doing the wrong thing.
//
// If this is not from the server we need to register if this is the first time
// we received the xdoor.
//
// It is ok to unmarshal a client xdoor even when the server side
// has been revoked. Failure will occur when a remote invocation occurs.
//
void
standard_xdoor_client::unmarshal(ID_node &from_node, MarshalStream &rms,
    bool is_new, Environment *)
{
	ASSERT(lock_held());
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): unmarshal from %d.%d new=%d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    from_node.ndid, from_node.incn, is_new));

	ID_node		&server_node = get_server_node();
	refcount	&trfc = refcount::the();
	rxdoor_id	rxdid(server_node, server_xdoor);

	if (ID_node::match(from_node, server_node)) {
		bool server_new = rms.get_boolean();
		if (server_new && !is_new) {
			// The server didn't know we had a reference. This
			// means that it added a foreign reference bit for this
			// marshal. We have subsequently send a register when
			// we received it, so the server has two bits set. We
			// send an unregister to clear one of them.
			trfc.add_unreg(server_node, rxdid);
		} else if (!server_new && is_new) {
			// The server expected us to have a reference but that
			// reference has gone away.  This means that it didn't
			// add a reference bit for us. We send it a register to
			// make up for that.
			trfc.add_reg(server_node, rxdid, from_node);
		}
	} else if (is_new) {
		// First time unmarshaled and from another client node so
		// registration is needed.
		trfc.add_reg(server_node, rxdid, from_node);
	}

	umarshout++;
	ASSERT(umarshout != 0);
}

void
standard_xdoor_client::marshal_local(Environment *)
{
	lock();
	// increment unmarshal count to match handler
	umarshout++;
	ASSERT(umarshout != 0);
	unlock();
}

void
standard_xdoor_client::marshal_received()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): marshal_received\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));
	dec_foreignref();
	lckp->unlock();
}

//
// create - a new xdoor is created for the specified rxdoor identifier.
//
rxdoor *
standard_xdoor_client::create(rxdoor_descriptor &idx)
{
	standard_xdoor_client *xdp =
	    new standard_xdoor_client((rxdoor_id &)idx);

	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%p %s(...:%d): created\n",
	    xdp, xdp->xdoor_type(), xdp->server_xdoor));

	return (xdp);
}

bool
standard_xdoor_client::local()
{
	return (false);
}

//
// server_dead - uses current membership to decide if the server xdoor is dead.
//
bool
standard_xdoor_client::server_dead()
{
	return (!members::the().still_alive(get_server_node()));
}

const char *
standard_xdoor_client::xdoor_type()
{
	return (standard_xdoor_client::xdoor_type_string);
}

rxdoor_kit&
standard_xdoor_client::get_kit()
{
	return (standard_xdoor_client::to_client_kit);
}
