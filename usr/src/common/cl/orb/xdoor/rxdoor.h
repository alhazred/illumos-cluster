/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RXDOOR_H
#define	_RXDOOR_H

#pragma ident	"@(#)rxdoor.h	1.112	08/07/17 SMI"

// rxdoors are xdoors which support communications between nodes,
// as well as within a node. For each rxdoor there will be exactly
// one server rxdoor in the entire cluster. The server object is located
// on the same node as the server rxdoor, and the server object can be
// in either the kernel or a user domain. Other nodes can each have
// one client rxdoor.  An rxdoor only exists in the kernel domain.
// rxdoor is a base class.
//
// Each rxdoor has an rxdoor_id which is a combination of its server's
// node id, incarnation, and a local xdoor id.
//
// rxdoors can have 0, 1 or 2 handlers above them.
// 0: A couple of possibilities exist:
//	A) The rxdoor is in an intermediate state, waiting for a reference
//	counting protocol to complete so it can go away, or for a handler to be
//	unmarshaled.
//	B) The rxdoor has been revoked, but not yet unreferenced.
//
// 1: Server rxdoors (unless revoked) always have a server handler
// associated with them and they forward invocations up to this handler.
// Client rxdoors have client handlers above them.
// Client and server handlers can be either in the kernel domain or
// can be a gateway handler which is associated with a solaris door.
//
// 2: rxdoors can have a gateway handler and a kernel domain handler.
// For server rxdoors either one can be the server handler. A client
// rxdoor can have one or two client handlers (one each for the kernel
// and user domains).
//
// Invocations through a client handler go through the rxdoor which
// identifies the server. The server may be local (send to the server
// handler) or remote (send to the server rxdoor).
//
// Some rxdoors perform distributed reference counting, using the
// utilities in refcount.{h,cc}
//
// rxdoors are created in 3 ways:
// 1 Server rxdoors are created when an object reference is to be marshaled
// outside the kernel domain.  These use the register_rxdoor interface to
// add themselves to the rxdoor name space and to get a local xdoor id.
// 2 Client or server rxdoors can be allocated at a specified rxdoor id
// when they are created.  This is used for well known rxdoors which
// are used for bootstrapping the system. This is done via a variation
// on the register_rxdoor interface to the rxdoor manager.
// 3 rxdoors are unmarshaled as part of translate_in from another node.
// They are inserted into the rxdoor name space (this is for client rxdoors
// only).
// To ensure that the CORBA::equiv() function works, there can never be
// more than one rxdoor in a domain with the same rxdoor id.  This means
// that the unmarshal code must be careful to ensure that an rxdoor is not
// in the domain before allocating a new one.  The unmarshal is done by
// the rxdoor_kit which knows how to interpret the marshalstream
// describing the rxdoor.
//
// The state of all rxdoors in the cluster is cleaned up during CMM
// reconfiguration (after a node failure or join).  This includes breaking
// invocations to nodes which are now down and rebuilding reference counts
// on reference counting server rxdoors.
//
// The rxdoor manager maintains a table of all rxdoors in the domain.
// rxdoors are added to this table by the three operations described
// above.  The table is searched when we do an unmarshal - to see if we
// already have the rxdoor.  rxdoors are removed from the table when the
// rxdoor decides that it is no longer needed, using the release_rxdoor
// interface to the rxdoor manager.  The local table is traversed during CMM
// reconfiguration.
//
// The table is organized as a hierarchy:
// For each potential node in the system we have a linked list of
// rxdoor_nodes, where an rxdoor_node contains all rxdoors whose server is
// on a particular incarnation of a node.  There is a special rxdoor_node
// for each node which stores rxdoors which are not associated with a
// particular incarnation (i.e. those at well known locations). The
// rxdoor_nodes for well known rxdoors and for the local node are allocated on
// startup. Other rxdoor_nodes are allocated the first time we create an rxdoor
// associated with a specific incarnation of a node.
//
// Each rxdoor_node contains an array of buckets and each bucket contains
// a linked list of rxdoors.  The rxdoors are hashed to buckets based on
// the server xdoor id part of the rxdoor identifier.  The rxdoors in a
// bucket have a pointer back to the bucket to access some shared
// information (more later).
//
// Note that client rxdoors can outlive server rxdoors as programs can
// keep references to objects in older incarnations after the server dies. They
// can still be passed around and still must be unmarshaled correctly so that
// equiv() works. Exceptions are delivered only when a method invocation
// happens on the dead object.
//
// LOCKING
//
// Translations are protected from reconfigurations by a rwlock.
// The CMM acquires it for writing during reconfiguration; threads
// processing incoming messages acquire it for reading in
// handle_request_common for invocations, and in handle_request_twoway
// for replies.  This assures that a single orphan check
// performed just after acquiring the lock will be valid throughout
// the translate_in process.
//
// A lock is stored in each bucket of an rxdoor_node.  The rxdoors use
// this lock to protect their private data.
// Note that locks are shared among all rxdoors in a bucket
// and that one rxdoor should never try to acquire the lock of another
// rxdoor while holding its own.
//
// One lock, the rxdoor_manager lock, protects all the lists of
// rxdoor_node. The per bucket lock protects the list of rxdoors
// stored in the bucket's linked list.  To insert an rxdoor into the
// rxdoor name space, the rxdoor manager lock must be acquired to look up
// the rxdoor_node (creating it if necessary) and to protect it from
// deletion.  Then the bucket lock must be acquired so that the bucket
// list can be modified.  Once the bucket lock is acquired, the rxdoor
// manager lock may be dropped, since no further modification will be made
// to the rxdoor_node lists, and the rxdoor_node will not be deleted while
// something is being inserted into a bucket.
//
// At the end of each cluster reconfiguration, we delete all empty
// rxdoor_node as follows: Acquire the rxdoor manager lock (blocking
// any new attempts to insert to the rxdoor_node); Grab each bucket lock
// in turn (ensuring that any inserts in progress have completed); If a
// bucket is found to be non-empty, then the rxdoor_node cannot be
// deleted;  If all are empty, the rxdoor_node is removed from the rxdoor_node
// list and deleted.
//
// When an rxdoor decides that it wants to go away, it calls
// release_rxdoor with its lock held. Since this lock is the same as the
// one protecting the bucket list, it can simply remove itself from the
// list.
//
//
// Other locking:
// Handlers have locks which protect their state. When deleting client handlers
// and when deleting or revoking server rxdoors we need to get both the handler
// and rxdoor lock.  The handler lock should be acquired first.
// Deleting handlers call client_unreferenced() function on the rxdoor which
// gets the rxdoor lock and removes the linkage to the handler.
// Unreferenced to the server handler (standard_xdoors only) is initiated by
// the rxdoor, so it needs to acquire the lock in the reverse order.  This is
// done by passing off the work to another thread  This thread calls up the the
// handler and acquires the locks in the appropriate order. It uses flags and
// marshalcounts to check the the unreferenced is still valid.
//
// Reference counting operations involve getting the nodejobs lock in
// the refcount code.  This is safe to do with the rxdoor lock
// held.  The nodejobs code must not hold the nodejobs lock before calling
// into the rxdoor code. Note that some some operations which are
// performed with the handler lock and rxdoor lock held perform reference
// counting operations (acquiring the nodejobs lock).
//
// In summary:
//	bucket lock is the same as the rxdoor lock. (We use them
//	interchangeably below since they are used in different
//	contexts).
//	rxdoor manager lock > bucket lock
//	handler lock > rxdoor lock > nodejobs lock
//
#include <h/cmm.h>
#include <orb/xdoor/Xdoor.h>
#include <sys/os.h>
#include <sys/list_def.h>
#include <orb/handler/handler.h>
#include <sys/nodeset.h>
#include <orb/invo/io_invos.h>
#include <orb/debug/orb_trace.h>
#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/rxdoor_defs.h>
#include <orb/xdoor/rxdoor_kit.h>
#include <orb/xdoor/rxdoor_header.h>
#include <orb/refs/refcount.h>

// extern declaration
class callback_membership;
class rxdoor_ref;

//
// rxdoor_id is the global id of an rxdoor, both server and client.
// The global id corresponds to the server local xdoor id plus its node id.
// rxdoor references are passed to other kernel domains by marshaling rxdoor_id.
//
class rxdoor_id : public rxdoor_descriptor {
public:
	rxdoor_id();
	rxdoor_id(ID_node &nd, xdoor_id xd);
	rxdoor_id(nodeid_t ndid, incarnation_num incn, xdoor_id xd);

	void	_put(MarshalStream &wms);
	void	_get(MarshalStream &rms);

	static uint_t 	_sizeof_marshal();

	virtual void	put_ref_job(rxdoor_ref *);

	ID_node	node;
};

class rxdoor;

// rxlist uses a Dlist to make deletions faster
/*CSTYLED*/
typedef IntrList<rxdoor, _DList> rx_list;

// List element for a rxdoor.
// Used to put rxdoors in rxdoor_bucket list (rx_list) above.
typedef _DList::ListElem rxdoor_elem;

class rxdoor_node;	// Forward declaration

// Bucket for a list of rxdoors.
class rxdoor_bucket {
public:
	void		lock();
	void		unlock();
	bool		lock_held();

	void		store(rxdoor *xdp);
	void		erase(rxdoor *xdp);
	rx_list		*list_hash(xdoor_id xdid);
	rxdoor		*find(xdoor_id xdid);

	rxdoor_node	*rnp;		// The incarnation we belong to

	os::mutex_t	lck;		// protects the bucket and its rxdoors.
	os::condvar_t	cv;
};

// rxdoor_node
// A per incarnation number hash list of rxdoor lists and buckets.
// The xdoors are sparse and there may be a large number of them.
// We need to be able to perform fast lookups, so we have lots of lists
// (10s of thousands).  We have a smaller number of rxdoor_buckets each of
// which manages "lists_per_bucket", lists.
// We currently use double linked lists so that we can remove rxdoors quickly.
// If we are confident that the lists are short, then we can move to single
// linked lists.
// Currently the number of lists is assigned statically in the constructor.
// We could make this dynamic, based on the number of rxdoors - so we can keep
// the lists at a desired average length. The rxdoors can be rehashed safely by
// grabbing the rxdoor_node lock and all rxdoor_bucket lock.
// There should be no reason to dynamically change the number of
// rxdoor_buckets, since their locks are not held for very long except during
// reconfiguration.
//
class rxdoor_node {
	friend class rxdoor_bucket;
public:
	rxdoor_node(ID_node &, uint_t nbuckets, uint_t nlists);
	virtual ~rxdoor_node();

	// called from CMM step1 to assign new incarnation number
	void		set_node(ID_node &);

	rxdoor		*lookup_rxdoor(xdoor_id xdid, rxdoor_bucket **xbpp);

	rxdoor_bucket	*bucket_hash(xdoor_id xdid);

	// go through the server xdoors and delete those whose foreign
	// reference count is zero
	void		cleanup_foreignrefs(callback_membership *old_mbrs,
			    callback_membership *new_mbrs);

	bool		is_empty();

#ifdef DEBUGGER_PRINT
	bool		mdb_node_is_empty();
	ssize_t 	mdb_dump_node();
	ssize_t 	mdb_dump_bucket(rx_list *lp, int *num_doors);
#endif

	ID_node		&get_node();

protected:
	uint_t		num_lists;
	const uint_t	num_buckets;
	uint_t		lists_per_bucket;
	rxdoor_bucket	*buckets;
	rx_list		*lists;

private:
	ID_node		node;
	uint_t		bucket_shift;

	// Disallow assignments and pass by value
	rxdoor_node(const rxdoor_node &);
	rxdoor_node &operator = (rxdoor_node &);
};

//
// notify_membership_change
//
// This class is used to register with
// rxdoor_membership_manager. Classes inheriting from it are
// registered with rxdoor_membership_manager.
//
class notify_membership_change : public _SList::ListElem {
public:
	virtual void	cleanup_foreignrefs(callback_membership *,
			    callback_membership *) = 0;

	virtual void	update_current() = 0;

protected:
	notify_membership_change();

};

typedef IntrList<notify_membership_change, _SList> rxdoor_mgr_list;

// rxdoor_membership_manager
//
// It provides a single place to keep track of membership changes and
// call cleanup_foreignrefs() method for all xdoor managers that
// register with it.
//
class rxdoor_membership_manager {
public:
	~rxdoor_membership_manager();

	static int initialize();

	static rxdoor_membership_manager	&the();

	void	register_door_manager(notify_membership_change *me);

	void 	cleanup_foreignrefs(callback_membership *new_mbrship);
	//
	// these functions manipulate the reconfiguration lock
	// block/unblock translations is called from members::set_membership
	// to grab the lock for writing
	//
	void	block_translate_in();
	void	unblock_translate_in();
	//
	// begin/end membership is called from rxdoor::handle_request
	// to grab the lock for reading, to prevent the orphan-ness of
	// a message from changing while a translation is in progress
	//
	void	begin_membership_lock();
	void	end_membership_lock();

	void	update_current();

	void	register_with_membership_manager(notify_membership_change *);

private:
	rxdoor_membership_manager();

	os::rwlock_t		reconfiguration_lock;

	callback_membership 	*curr_membership;

	os::mutex_t		list_lock;
	rxdoor_mgr_list		mgr_list;

	static rxdoor_membership_manager *rxdmm;
};

// rxdoor
//
// Remote Xdoor interface.
//
class rxdoor : public Xdoor {
public:
	virtual ~rxdoor();


#if (SOL_VERSION >= __s10) && !defined(UNODE)
	virtual void 	make_twoway_header(uint32_t, sendstream *, ushort_t);
	virtual void 	make_oneway_header(uint32_t, sendstream *);
#else
	virtual void 	make_twoway_header(sendstream *, ushort_t);
	virtual void 	make_oneway_header(sendstream *);
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	// these are the message receivers for rxdoor messages
	static void	handle_oneway(recstream *);
	static void	handle_twoway(recstream *);
	static void	handle_reply(recstream *);
	static void	handle_ckpt(recstream *);

	// Xdoor methods implemented here.
	handler		*extract_handler(handler_type t);
	void		extract_done(handler_type, handler *,
			    bool unmarshalled);
	virtual void	post_extract_done(handler_type);
	bool		client_unreferenced(handler_type, uint_t marshcnt);

	// Oneway Request for a local or remote server.
	virtual ExceptionStatus  request_oneway(invocation &);

	// RPC Request for a local or remote server.
	virtual ExceptionStatus  request_twoway(invocation &);

	// Notification that client nodes have received the xdoor.
	virtual void	register_client(ID_node &);

	// Notification that client nodes have released the xdoor.
	virtual void	unregister_client(ID_node &);

	//
	// Used during cluster reconfiguration to recalculate the foreign
	// reference state.
	//
	virtual void	cleanup_foreignrefs(const nodeset &);

	virtual void	handle_possible_unreferenced() = 0;

	// The server node of the rxdoor.
	ID_node		&get_server_node();
	nodeid_t	get_server_nodeid();

	void		set_bucket(rxdoor_bucket *xbp);
	rxdoor_bucket	*get_bucket();

	rxdoor_elem	*get_list_elem();

	// send_reply - transmits reply message after translating any xdoors.
	// Supports both local and remote replies.
	//
	virtual void	send_reply(service &);

	// Transmits local reply message after translating any xdoors.
	// This version exists so that code can be shared with hxdoor code
	// when there is no local rxdoor.
	//
	static void		send_reply_local(service &);

	static void		convert_local_stream(invocation &inv);

	size_t			sizeof_reply_header();

	virtual const char	*xdoor_type() = 0;

	void			lock();
	void			unlock();

	os::mutex_t		&get_lock();
	bool			lock_held();

	virtual rxdoor_kit	&get_kit() = 0;

	virtual	void		get_xdoor_desc(rxdoor_descriptor &);

	virtual void		marshal_xdid(MarshalStream &);

#ifdef DEBUGGER_PRINT
	void			mdb_get_handlers(handler **, handler **);
#endif

	// Reference to the server xdoor
	xdoor_id	server_xdoor;

protected:
	// The following constructor supports server xdoors
	rxdoor(handler *h);

	// The following constructor supports client xdoors
	rxdoor(xdoor_id id);

	// Returns true if this is a server rxdoor.
	virtual bool	local() = 0;

	//
	// Called for server xdoors only to enable the xdoor to do any
	// special processing before an incoming call is passed up to
	// the handler. Generates an exception if the operation should not
	// proceed.
	//
	virtual void	incoming_prepare(Environment *);

	// Utilities for derived rxdoors
	os::condvar_t	&get_cv();

	//
	// Called for server xdoors only to allow the xdoor to do any
	// special processing after an incoming call has been processed.
	//
	virtual void	incoming_done();

	// Process an exception.
	static void	exception_reply_remote(service_twoway &serv);
	static void	exception_reply_local(service &serv);

	// for cleaning up xdoors
	virtual void	dec_marshcount(uint_t marshcnt) = 0;

	// Initialize the server handler for a server rxdoor
	// Only usable on server xdoor, will ASSERT fail on client rxdoors
	virtual void	set_server_handler(handler *h);

	// Returns the server handler for a server rxdoor
	// Only usable on server xdoor, will ASSERT fail on client rxdoors
	virtual handler	*get_server_handler();

	// Returns the client handler for a server rxdoor
	// Only usable on server xdoor, will ASSERT fail on client rxdoors
	handler	*get_client_handler();

	// An rxdoor has 0, 1, or 2 handlers:
	//	A: one for the kernel, and
	//	B: one for the gateway on behalf of a solaris door.
	// The handlers are also partitioned along another independent axis:
	//	A: server (at most one per node)
	//	B: client (one per domain: user and/or kernel)
	//
	// We can have up to 2 handlers, one for kernel and one for user.
	handler		*handlers[2];

	// These are indices into above array to identify the
	// client/server handlers.  only set in the constructor, and
	// remains valid until the xdoor is destroyed.
	// They are only initialized and used on server xdoors.
	// See the methods get_server_handler and get_client_handler above
	// The valid values are Xdoor::USER_HANDLER, Xdoor::KERNEL_HANDLER
	//
	char		server_handler_index;
	char		client_handler_index;

	// unmarshals outstanding on this xdoor.
	uint_t		umarshout;

	// unmarshals outstanding to a particular handler.
	ushort_t	umarsh_handler[2];

	//
	// This defines the bit meaning for the "flags" member.
	// At this time only server rxdoor objects need a "flags" member,
	// and thus the "flags" member is only declared in the server versions.
	// The flags definitions are here in order to standardize their naming.
	//
	enum {	REVOKE_PENDING		= 0x01,
		REVOKED			= 0x02,
		UNREFERENCED		= 0x04,

		// Set by handle_possible_unreferenced, and
		// always cleared by accept_unreferenced or reject_unreferenced.
		UNREF_PENDING		= 0x08,

		// A marshal occurred while an unref task was pending.
		MARSHAL_DURING_UNREF	= 0x10,

		// These are used by hxdoors
		// The kernel handler is a client.
		HAS_CLIENT_REF		= 0x20,

		// This is the primary
		PRIMARY			= 0x40,

		// This is the secondary
		SECONDARY		= 0x80,

		// Used do decide if unreg msg should be sent to server
		UNREG_NEEDED		= 0x100
	};

	//
	// Points to a bucket of rxdoors which stores some shared resources
	// for this rxdoor.
	//
	rxdoor_bucket	*bucketp;

	// used for linkage in hash table.
	rxdoor_elem	elem;

private:
	// Request for a server xdoor from a local client.
	ExceptionStatus  request_local(invocation &);

	// Support xdoor translation and transmit request
	ExceptionStatus	translate_and_send(service &serv);

	//
	// A request (oneway or twoway) with a valid rxdoor arrived.
	// Perform common processing needed to pass it to the server handler.
	//
	static void handle_request_common(ID_node &src_node, service &serv,
	    rxdoor_invo_header *, xkit_id_t kitid);

	// Disallow assignments and pass by value
	rxdoor(const rxdoor &);
	rxdoor & operator = (rxdoor &);
};

#include <orb/xdoor/rxdoor_in.h>

#endif	/* _RXDOOR_H */
