/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)translate_mgr.cc	1.31	08/05/20 SMI"

#include <orb/xdoor/translate_mgr.h>
#include <orb/member/cmm_callback_impl.h>
#include <orb/infrastructure/orb_conf.h>

// the_translate_mgr
translate_mgr	*translate_mgr::the_translate_mgr = NULL;

translate_mgr::translate_mgr()
{
}

translate_mgr::~translate_mgr()
{
}

int
translate_mgr::initialize()
{
	ASSERT(the_translate_mgr == NULL);
	the_translate_mgr = new translate_mgr;
	if (!the_translate_mgr) {
		return (ENOMEM);
	}
	return (0);
}

void
translate_mgr::shutdown()
{
	if (the_translate_mgr) {
		the_translate_mgr->shutdown_int();
		delete the_translate_mgr;
		the_translate_mgr = NULL;
	}
}

void
translate_mgr::shutdown_int()
{
	lck.lock();
	// empty out all tr_node lists.
	for (int i = 1; i <= NODEID_MAX; i++) {
		tr_nodes[i].in_transit.erase_list();
	}
	// don't unlock since we do not want more jobs added to the list.
}

//
// Called during CMM reconfiguration.
// Update the incarnation numbers of each node.  If a node has died, then
// pretend that acks have arrived. Other parts of the reconfiguration ensure
// that this is safe.
//
void
translate_mgr::new_membership(const cmm::membership_t &membership)
{
	ORB_DBPRINTF(ORB_TRACE_REFCNT, ("new_membership\n"));
	tr_node *trndp;
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		trndp = &tr_nodes[n];
		if (trndp->incn != membership.members[n]) {
			ID_node	nd(n, trndp->incn);

			//
			// By getting the write lock we guarantee that any
			// translate_out and sends in progress have comepleted.
			// We only do this for dead nodes, so we don't have to
			// worry about the send hanging.
			//
			// Step 1 of reconfiguration has already guaranteed us
			// that we will not receive any calls to
			// ack_translates() since it flushes orphaned refcount
			// messages.
			//
			trndp->rwlck.wrlock();

			// Update the incarnation number.
			trndp->incn = membership.members[n];

			//
			// Pretend that all marshals have been received.
			// No need to use the mutex here since we have the
			// write lock.
			//
			tr_out_info *infop;
			while ((infop = trndp->in_transit.reapfirst()) !=
			    NULL) {
				infop->ack_xdoors();
				ORB_DBPRINTF(ORB_TRACE_REFCNT,
				    ("new_membership: nd %d.%d deleting %p\n",
				    n, trndp->incn, infop));
				delete infop;
			}
			trndp->rwlck.unlock();
		}
	}
}

//
// begin_tr_out
// Calling begin_tr_out allows the caller to proceed to translate.
// A successful call (returning true) guarantees the caller that a
// cluster reconfiguration will not to occur until after done_tr_out is
// called (holds a read lock).  If a reconfiguration is in progress, then the
// call will block until it finishes.
// The caller indicates the node and incarnation number to which it
// wants to send the references. If the node has been declared down
// then the call will fail (returning with an exception), indicating to the
// caller that it should not try to marshal the reference to this node.
// The caller passes in a tr_out_info structure to be queued until an ack comes
// in or the node is declared down.
// The caller must match this call with a call to done_tr_out() and an
// asynchronous ack_translates() (which may come in before or after the
// done_tr_out()) OR a done_tr_out_not_sent() without an ack_translates().
//
// Legal combinations of what can happen next are:
// 1    done_tr_out()
//	ack_translates()
//	The xdoors were marshaled and sent. The ack was then received from the
//	destination node.
//
// 2    ack_translates()
//	done_tr_out()
//	The xdoors were marshaled and sent. The ack was received from the
//	destination node before the sending thread called done_tr_out().
//
// 3    done_tr_out()
//	new_membership()
//	The xdoors were marshaled and sent. The destination node died before
//	it sent an ack. The CMM reconfiguration guarantees that no more acks
//	will be received from nodes which are marked down by new_membership()
//	and new_membership() completes the protocol for the transmitted xdoors
//	by doing the equivalent of ack_translates().
//
// 4    done_tr_out_not_sent()
//	We hit a problem and either the xdoor marshals were not performed at
//	all or they were rolled back. The previously added tr_out_info needs to
//	be removed from the list and the read lock dropped.
//
void
translate_mgr::begin_tr_out(ID_node &nd, tr_out_info *infop, Environment *e)
{
	ASSERT(!e->is_nonblocking());

	// Get a read lock on the current incarnation number.
	tr_nodes[nd.ndid].rwlck.rdlock();

	// Check if we are sending to a node which is still alive.
	if (nd.incn == tr_nodes[nd.ndid].incn) {
		lck.lock();
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("begin_tr_out adding %p node: %d.%d\n",
		    infop, nd.ndid, nd.incn));
		tr_nodes[nd.ndid].in_transit.append(infop);
		lck.unlock();
	} else {
		e->system_exception(CORBA::COMM_FAILURE(0,
			CORBA::COMPLETED_MAYBE));
		tr_nodes[nd.ndid].rwlck.unlock();
	}
}


//
// done_tr_out_not_sent
// This indicates that the we will not receive an ack for these xdoors because
// we hit a problem and either the xdoor marshals were not performed at all or
// they were rolled back. The previously added tr_out_info needs to be removed
// from the list and the read lock dropped.
// When all read locks are dropped, then the write lock may be granted,
// allowing a reconfiguration to be performed.
//
void
translate_mgr::done_tr_out_not_sent(ID_node &nd, tr_out_info *infop)
{
	tr_node *trndp = &tr_nodes[nd.ndid];
	lck.lock();
	(void) trndp->in_transit.erase(infop);
	lck.unlock();
	// Finished with lock held in begin_tr_out
	tr_nodes[nd.ndid].rwlck.unlock();
	delete infop;	// does a dispose
}

//
// Called from the reference counting code when a translate_ack comes in.
// We notify all the xdoors in the marshaled set that they have been
// unmarshaled on the other end.
//
void
translate_mgr::ack_translates(ID_node &nd, tr_out_info *infop)
{
	tr_node *trndp = &tr_nodes[nd.ndid];

	ASSERT(infop != NULL);

	//
	// The reference counting subsystem flushes orphaned messages in step 1
	// of reconfiguration and we set the incarnation number in step 3, so
	// we should never see an incoming ack which doesn't match ours.
	//
	ASSERT(trndp->incn == nd.incn);

	// Notify xdoors.
	infop->ack_xdoors();

	lck.lock();
	(void) trndp->in_transit.erase(infop);
	lck.unlock();
	delete infop;	// does a dispose
}

translate_mgr::tr_node::tr_node() : incn(INCN_UNKNOWN)
{
}
