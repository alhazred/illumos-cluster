/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rxdoor_kit.cc	1.3	08/05/20 SMI"

#include <orb/xdoor/rxdoor_kit.h>
#include <orb/xdoor/rxdoor_mgr.h>
#include <orb/infrastructure/orb_conf.h>

//
// rxdoor_kit stuff
//

// Unmarshal one remote xdoor.
// The marshal stream contains a global xdoor id followed by some
// xdoor type specific data. We get ourselves an xdoor pointer and
// call its unmarshal method.
//
Xdoor *
rxdoor_kit::unmarshal(ID_node &node, MarshalStream &xdoors, Environment *e)
{
	rxdoor_bucket *xbp;
	rxdoor	*rxdp;

	bool	 create_door = true; // create rxdoor if not found

	// make sure we did not get a stale exception.
	ASSERT(e->exception() == NULL);

	// rxdoor_manager::configuration_lock held for reading,
	// so we know node is still a member

	rxdp = lookup_rxdoor(&create_door, node, xdoors, &xbp);

	ASSERT(rxdp);

	if (!create_door) {
		ORB_DBPRINTF(ORB_TRACE_MARSHAL,
		    ("%s(%d.%3d:%d): rx_kit unmarshal %p\n",
		    rxdp->xdoor_type(), rxdp->get_server_node().ndid,
		    (rxdp->get_server_node().incn % 1000), rxdp->server_xdoor,
		    rxdp));
	} else {
		xbp->store(rxdp);

		ORB_DBPRINTF(ORB_TRACE_TRANSLATE,
		    ("%s(%d.%3d:%d): translate_in: new one %p\n",
		    rxdp->xdoor_type(), rxdp->get_server_node().ndid,
		    (rxdp->get_server_node().incn % 1000), rxdp->server_xdoor,
		    rxdp));
	}

	// Do any xdoor specific unmarshaling.
	rxdp->unmarshal(node, xdoors, create_door, e);

	if (e->exception()) {
		// It may be necessary to release the xdoor now.
		rxdp->handle_possible_unreferenced();
		rxdp = NULL;	// return NULL
	}

	xbp->unlock();

	return (rxdp);
}

// Cancel the unmarshal of a remote (simple or standard) xdoor.
// The marshal stream contains a global xdoor id followed by some
// xdoor type specific data.
void
rxdoor_kit::unmarshal_cancel(ID_node &node, MarshalStream &xdoors)
{
	rxdoor_id	global_id;

	global_id._get(xdoors);
	unmarshal_cancel_remainder(node, global_id, xdoors);
}

void
rxdoor_kit::unmarshal_cancel_remainder(ID_node &, rxdoor_descriptor &,
    MarshalStream &)
{
	// default implementation used by simple_xdoor.
}

//
// marshal_xdoor
//
// Default marshal method for all rxdoors.
//
void
rxdoor_kit::marshal_xdoor(MarshalStream &ms, rxdoor_descriptor &xdesc)
{
	// marshal our kit id
	marshal(ms);
	// marshal relevant xdoor information
	xdesc._put(ms);
}

void
rxdoor_kit::marshal_xdoor(MarshalStream &ms, rxdoor *rxdp)
{
	// marshal our kit id
	marshal(ms);

	rxdp->marshal_xdid(ms);
}

rxdoor *
rxdoor_kit::lookup_rxdoor_from_invo_header(rxdoor_invo_header *headerp,
    rxdoor_bucket **xbp)
{
	return (rxdoor_manager::the().lookup_local_rxdoor(
	    ((rxdoor_oneway_header *)headerp)->xdid, xbp));
}

rxdoor *
rxdoor_kit::lookup_rxdoor_from_refjob(rxdoor_ref *refp,
    rxdoor_bucket **xbpp)
{
	return (rxdoor_manager::the().lookup_local_rxdoor(refp->xdid, xbpp));
}

// Roll back the marshal of a remote (simple or standard) xdoor.
// The marshal stream contains a global xdoor id followed by some
// xdoor type specific data.
// We must look up the xdoor and call its marshal_roll_back method.
void
rxdoor_kit::marshal_roll_back(ID_node &node, MarshalStream &xdoors)
{
	rxdoor		*xdp;
	bool		create_door = false;

	xdp = lookup_rxdoor(&create_door, node, xdoors, NULL);
	xdp->marshal_roll_back(node, xdoors);
	xdp->unlock();
}

//
// rxdoor_to_client_kit methods
//

//
// lookup_rxdoor
//
//    Lookup rxdoor for this kit. This method will be called for
//    xdoors that are marshalled from one client to another. This
//    method needs to lookup rxdoors in the node bucket given by
//    nodeid marshalled in.  It sets new_door = true, if the rxdoor
//    did not exist on this node and was created.  Note: new_door is a
//    in-out variable. If *new_door is true, then this method will
//    create a new xdoor if it did not exist. If it is false and the
//    xdoor was not found, it will return NULL.
//
rxdoor *
rxdoor_to_client_kit::lookup_rxdoor(bool *new_door, ID_node &,
    MarshalStream &xdoors, rxdoor_bucket **xbpp)
{
	rxdoor_id 	global_id;
	rxdoor		*rxdp;

	rxdoor_manager &trxdm = rxdoor_manager::the();

	// get the whole of rxdoor_id: xdoor_id + node id
	global_id._get(xdoors);
	rxdp = trxdm.lookup_rxdoor(global_id.node, global_id.xdid,
	    xbpp);

	if (rxdp) {
		//
		// found the rxdoor. set flag to indicate that new
		// rxdoor was not created.
		//
		*new_door = false;
		return (rxdp);
	} else if (*new_door) {	// rxdoor not found, create one
		rxdp = create(global_id);
	}

	return (rxdp);
}

//
// rxdoor_to_server_kit methods
//

rxdoor*
rxdoor_to_server_kit::lookup_rxdoor(bool *, ID_node &,
    MarshalStream &xdoors, rxdoor_bucket **xbp)
{
	rxdoor_manager &trxdm = rxdoor_manager::the();

	xdoor_id xdid = xdoors.get_unsigned_int();

	rxdoor *rxdp = trxdm.lookup_local_rxdoor(xdid, xbp);

	// This rxdoor better be found on the server
	ASSERT(rxdp);

	return (rxdp);
}

void
rxdoor_to_server_kit::marshal_xdoor(MarshalStream &ms, rxdoor_descriptor &xd)
{
	// marshal our kit id
	marshal(ms);

	// need to put in only the xdoor_id member since the server
	// node is known to the receiving side.
	ms.put_unsigned_int(xd.xdid);
}


void
rxdoor_to_server_kit::marshal_xdoor(MarshalStream &ms, rxdoor *rxdp)
{
	// marshal our kit id
	marshal(ms);

	// need to put in only the xdoor_id member since the server
	// node is known to the receiving side.
	ms.put_unsigned_int(rxdp->server_xdoor);
}

void
rxdoor_to_server_kit::unmarshal_cancel(ID_node &node, MarshalStream &xdoors)
{
	//
	// This one would have marshalled only the xdoorid
	//
	rxdoor_id rxdid;

	rxdid.xdid = xdoors.get_unsigned_int();
	rxdid.node = orb_conf::current_id_node();
	unmarshal_cancel_remainder(node, rxdid, xdoors);
}

//
// rxdoor_from_server_kit methods
//

rxdoor*
rxdoor_from_server_kit::lookup_rxdoor(bool *first_time, ID_node &node,
    MarshalStream &xdoors, rxdoor_bucket **xbp)
{
	rxdoor_id 	global_id;

	rxdoor_manager &trxdm = rxdoor_manager::the();

	global_id.node = node;
	global_id.xdid = xdoors.get_unsigned_int();

	rxdoor *rxdp = trxdm.lookup_rxdoor(global_id.node, global_id.xdid, xbp);

	if (rxdp) {
		//
		// found the rxdoor. set flag to indicate that new
		// rxdoor was not created.
		//
		*first_time = false;
		return (rxdp);
	} else {	// rxdoor not found.
		rxdp = create(global_id);
		*first_time = true;
	}

	return (rxdp);
}

void
rxdoor_from_server_kit::marshal_xdoor(MarshalStream &ms, rxdoor_descriptor &xd)
{
	// marshal our kit id
	marshal(ms);
	//
	// need to put in only the xdoor_id member since the server
	// node is known to the receiving side.
	//
	ms.put_unsigned_int(xd.xdid);
}

void
rxdoor_from_server_kit::marshal_xdoor(MarshalStream &ms, rxdoor *rxdp)
{
	// marshal our kit id
	marshal(ms);
	//
	// need to put in only the xdoor_id member since the server
	// node is known to the receiving side.
	//
	ms.put_unsigned_int(rxdp->server_xdoor);
}

void
rxdoor_from_server_kit::marshal_roll_back(ID_node &node, MarshalStream &xdoors)
{
	rxdoor		*xdp;

	xdoor_id xdid = xdoors.get_unsigned_int();
	xdp = rxdoor_manager::the().lookup_local_rxdoor(xdid, NULL);
	xdp->marshal_roll_back(node, xdoors);
	xdp->unlock();
}

void
rxdoor_from_server_kit::unmarshal_cancel(ID_node &node, MarshalStream &xdoors)
{
	//
	// This one would have marshalled only the xdoorid
	//
	rxdoor_id rxdid;

	rxdid.xdid = xdoors.get_unsigned_int();
	rxdid.node = node;
	unmarshal_cancel_remainder(node, rxdid, xdoors);
}
