/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  simple_xdoor_in.h
 *
 */

#ifndef _SIMPLE_XDOOR_IN_H
#define	_SIMPLE_XDOOR_IN_H

#pragma ident	"@(#)simple_xdoor_in.h	1.9	08/05/20 SMI"

//
// simple_xdoor -
//	A simple_xdoor is an xdoor which does not keep a reference count.
//
// There are two types of simple_xdoors,
// which are distinguished by the node incarnation number:
//	A) incarnation number == INCN_UNKNOWN
//		represents an xdoor that is valid with any incarnation number
//		on remote invocations.
//	B) normal incarnation number
//		represents an xdoor with the normal semantics.
//		The xdoor is valid as long as the incarnation number
//		is valid.
//

inline
simple_xdoor_to_client_kit::simple_xdoor_to_client_kit() :
	simple_xdoor_kit(),
	rxdoor_to_client_kit(SIMXD_TO_CLIENT_XKID, rxdoor_id::_sizeof_marshal())
{
}

inline
simple_xdoor_to_server_kit::simple_xdoor_to_server_kit() :
	simple_xdoor_kit(),
	rxdoor_to_server_kit(SIMXD_TO_SERVER_XKID,
	    rxdoor_descriptor::_sizeof_marshal())
{
}

inline
simple_xdoor_from_server_kit::simple_xdoor_from_server_kit() :
    rxdoor_from_server_kit(SIMXD_FROM_SERVER_XKID,
	rxdoor_descriptor::_sizeof_marshal())
{
}

//
// This constructor is used for servers
//
inline
simple_xdoor::simple_xdoor(handler *h) : rxdoor(h)
{
}

//
// This constructor is used for clients
//
inline
simple_xdoor::simple_xdoor(xdoor_id xdn) : rxdoor(xdn)
{
}

inline
simple_xdoor::~simple_xdoor()
{
}

//
// simple_xdoor_server - A simple_xdoor_server cannot get deleted.
//			The server xdoor is immortal.
//

inline
simple_xdoor_server::simple_xdoor_server(handler * h) :
	simple_xdoor(h), services(0), flags(0)
{
}

//
// Simple xdoor servers do not get deleted in our current implementation
//
inline
simple_xdoor_server::~simple_xdoor_server()
{
	ASSERT(0);
}

//
// simple_xdoor_client - these client xdoors exist only as long as at least
//			one handler needs it.
//

//
// constructor - used when unmarshaling a new xdoor.
//
inline
simple_xdoor_client::simple_xdoor_client(rxdoor_id &rx) :
	simple_xdoor(rx.xdid),
	foreignrefs(0)
{
}

inline
simple_xdoor_client::~simple_xdoor_client()
{
}

#endif	/* _SIMPLE_XDOOR_IN_H */
