/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rxdoor.cc	1.160	08/07/17 SMI"

// Implementation file for rxdoors.

#include <h/replica_int.h>
#include <sys/mc_probe.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/xdoor/rxdoor.h>
#include <orb/xdoor/rxdoor_mgr.h>
#include <orb/xdoor/rxdoor_header.h>
#include <orb/xdoor/translate_mgr.h>
#include <orb/flow/resource_done.h>
#include <orb/flow/resource.h>
#include <orb/transport/nil_endpoint.h>
#include <orb/debug/orb_stats_mgr.h>
#include <orb/msg/orb_msg.h>
#include <orb/member/members.h>
#include <orb/invo/io_invos.h>
#include <orb/fault/fault_injection.h>
#include <sys/cl_comm_support.h>

#ifdef _KERNEL
#include <sys/systm.h>
#endif


// Interface between the CMM and rxdoors that keeps tracks of current
// memebrship.
rxdoor_membership_manager *rxdoor_membership_manager::rxdmm = NULL;

//
// rxdoor_id
//
void
rxdoor_id::put_ref_job(rxdoor_ref *ref)
{
	ref->xdid = xdid;
}

// rxdoor_bucket methods

//
// Search for the rxdoor matching xdid.  Returns NULL on failure.
rxdoor *
rxdoor_bucket::find(xdoor_id xdid)
{
	ASSERT(lock_held());
	rxdoor	*xdp = NULL;

	rx_list::ListIterator li(list_hash(xdid));
	for (; (xdp = li.get_current()) != NULL; li.advance()) {
		if (xdp->server_xdoor == xdid) {
			break;
		}
	}
	return (xdp);
}

//
// rxdoor_node methods
//

rxdoor_node::rxdoor_node(ID_node &nd, uint_t nbuckets, uint_t nlists) :
	num_lists(nlists),
	num_buckets(nbuckets),
	node(nd)
{
	ASSERT((nbuckets & (nbuckets - 1)) == 0); // must be power of 2
	ASSERT((nlists & (nlists - 1)) == 0); // must be power of 2
	ASSERT(nlists >= nbuckets);
	buckets = new rxdoor_bucket[num_buckets];
	lists = new rx_list[nlists];
	lists_per_bucket = nlists / nbuckets;
	// Save doing a divide in bucket_hash()
	bucket_shift = (uint_t)os::highbit((ulong_t)lists_per_bucket) - 1;

	for (uint_t i = 0; i < num_buckets; i++) {
		buckets[i].rnp = this;
	}
}

rxdoor_node::~rxdoor_node()
{
	delete [] buckets;
	delete [] lists;
}

// Used for rxdoor_nodes which are allocated before we know the incarnation
// number (this occurs for the local rxdoor_node).
void
rxdoor_node::set_node(ID_node& nd)
{
	node.ndid = nd.ndid;
	node.incn = nd.incn;
}
//
// lookup_rxdoor
//
//    Return pointer to rxdoor matching the given xdoorid.  xbpp is
//    the address of a pointer to rxdoor_bucket.  This methods locks
//    the bucket while it finds and returns pointer to the
//    rxdoor. xbpp is set to this rxdoor_bucket to allow the caller to
//    unlock the bucket.
//
rxdoor *
rxdoor_node::lookup_rxdoor(xdoor_id xdid, rxdoor_bucket **xbpp)
{
	rxdoor_bucket *xbp = bucket_hash(xdid);
	xbp->lock();

	if (xbpp) {
		*xbpp = xbp;
	}
	return (xbp->find(xdid));
}

//
// cleanup_foreignrefs
//
// Called from rxdoor_manager::cleanup_foreignrefs() during step 3 of
// cmm reconfiguration. Only called on rxdoor_node for the server xdoors.
// This method calculates the bitmask of nodes which have died since the last
// time it was called and notifies all server xdoors of the node death.
//
void
rxdoor_node::cleanup_foreignrefs(callback_membership *old_mbrs,
    callback_membership *new_mbrs)
{
	rxdoor_bucket	*bp = buckets;
	rx_list		*lp = lists;
	rxdoor		*xdp;
	uint_t		i, j;
	rx_list::ListIterator li;

	nodeset dead_nodes;

	// Create a bitmask of dead nodes.
	for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
		if ((old_mbrs->membership().members[n] != INCN_UNKNOWN) &&
		    (old_mbrs->membership().members[n] !=
		    new_mbrs->membership().members[n])) {
			dead_nodes.add_node(n);
		}
	}

	if (dead_nodes.is_empty())
		return;

	ORB_DBPRINTF(ORB_TRACE_REFCNT, ("%p clnp_forrefs dead = 0x%x\n",
	    this, dead_nodes.bitmask()));
	// for each bucket
	for (i = 0; i < num_buckets; i++, bp++) {
		bp->lock();
		for (j = 0; j < lists_per_bucket; j++, lp++) {
			li.reinit(lp);
			while ((xdp = li.get_current()) != NULL) {
				// Advance before potentially erasing.
				li.advance();
				xdp->cleanup_foreignrefs(dead_nodes);
			}
		}
		bp->unlock();
	}
}

// Scan all buckets and see if this rxdoor_node is empty. If so, the
// caller can delete it.
// This is called with the rxdoor manager lock held. We then get the lock
// for every bucket until we find one that is non empty.  If all are empty,
// then we can delete this rxdoor_node.
// Inserts to the rxdoor_node require grabbing the rxdoor manager lock
// to look up the rxdoor_node followed by the bucket lock (the manager
// lock can be dropped after getting the bucket lock).
// Holding the rxdoor manager lock blocks any new attempts to find this
// rxdoor_node. Grabbing each rxdoor_bucket lock ensures that any attempts
// to insert into that bucket have completed.
bool
rxdoor_node::is_empty()
{
	rxdoor_bucket	*bp = buckets;
	rx_list		*lp = lists;
	uint_t		i, j;

	for (i = 0; i < num_buckets; i++, bp++) {
		bp->lock();
		for (j = 0; j < lists_per_bucket; j++, lp++) {
			if (!lp->empty()) {
				bp->unlock();
				return (false);
			}
		}
		bp->unlock();
	}

	return (true);
}

// notify_membership_change methods
//
notify_membership_change::notify_membership_change()
	:_SList::ListElem(this)
{
	rxdoor_membership_manager::the().register_door_manager(this);

}

rxdoor_membership_manager::rxdoor_membership_manager()
	:    curr_membership(NULL)
{
}

rxdoor_membership_manager::~rxdoor_membership_manager()
{
	// Need to empty list before its destructor can be called.
	list_lock.lock();
	mgr_list.erase_list();
	list_lock.unlock();
	if (curr_membership != NULL) {
		curr_membership->rele();
	}
	curr_membership = NULL; // make lint happy
}

int
rxdoor_membership_manager::initialize()
{
	ASSERT(rxdmm == NULL);
	rxdmm = new rxdoor_membership_manager;
	if (rxdmm == NULL) {
		return (ENOMEM);
	}
	return (0);
}

void
rxdoor_membership_manager::register_door_manager(notify_membership_change *me)
{
	// The lock is necessary during bringup when both
	// rxdoor_manager and hxdoor_manager are created. No one will
	// add to this later.
	list_lock.lock();
	mgr_list.append(me);
	list_lock.unlock();
}

void
rxdoor_membership_manager::cleanup_foreignrefs(callback_membership *new_mbrship)
{
	if (curr_membership != NULL) {
		ORB_DBPRINTF(ORB_TRACE_REFCNT,
		    ("cleanup_foreignrefs called \n"));
		// Cleanup server xdoors
		rxdoor_mgr_list::ListIterator li(mgr_list);
		notify_membership_change *mgr;
		while ((mgr = li.get_current()) != NULL) {
			mgr->cleanup_foreignrefs(curr_membership, new_mbrship);
			li.advance();
		}
		curr_membership->rele();
	}
	curr_membership = new_mbrship;
	curr_membership->hold();
}

//
// update_current
//
// This method is called on all registered xdoor managers to to allow them to
// perform any specific task after a membership change.
// Now only rxdoor_mgr does any useful work this method.
//
void
rxdoor_membership_manager::update_current()
{
	// Cleanup server xdoors
	rxdoor_mgr_list::ListIterator li(mgr_list);
	notify_membership_change *mgr;
	while ((mgr = li.get_current()) != NULL) {
		mgr->update_current();
		li.advance();
	}
}

//
// rxdoor methods
//

//
// The following constructor supports server xdoors
// The other fields are initialized when the rxdoor registers.
//
rxdoor::rxdoor(handler *handlerp) :
	server_xdoor(INVALID_XDOOR_ID),
	umarshout(0),
	elem(this)
{
	ASSERT(handlerp != NULL);
	if (handlerp->is_user()) {
		handlers[KERNEL_HANDLER] = NULL;
		handlers[USER_HANDLER] = handlerp;
		server_handler_index = USER_HANDLER;
		client_handler_index = KERNEL_HANDLER;
	} else {
		handlers[KERNEL_HANDLER] = handlerp;
		handlers[USER_HANDLER] = NULL;
		server_handler_index = KERNEL_HANDLER;
		client_handler_index = USER_HANDLER;
	}
	umarsh_handler[KERNEL_HANDLER] = 0;
	umarsh_handler[USER_HANDLER] = 0;
}

//
// The following constructor supports client xdoors
// Client rxdoor always know the server xdoor number when they are created.
//
rxdoor::rxdoor(xdoor_id xdn) :
	server_xdoor(xdn),
	umarshout(0),
	elem(this)
{
	handlers[USER_HANDLER] = NULL;
	handlers[KERNEL_HANDLER] = NULL;
	umarsh_handler[KERNEL_HANDLER] = 0;
	umarsh_handler[USER_HANDLER] = 0;
	server_handler_index = -1;		// An invalid value
	client_handler_index = -1;		// An invalid value
}

//
// incoming_prepare - Called for server xdoors only to enable the xdoor to do
// any special processing before an incoming call is passed up to
// the handler. Generates an exception if the operation should not
// proceed.
//
// This implementation is for those rxdoors that do not need this method.
//
void
rxdoor::incoming_prepare(Environment *)
{
	ASSERT(0);
}

//
// Called for server xdoors only to allow the xdoor to do any
// special processing after an incoming call has been processed.
//
// This implementation is for those rxdoors that do not need this method.
//
void
rxdoor::incoming_done()
{
	ASSERT(0);
}

//
// register_client - Notification that client nodes have received the xdoor.
// This method is only for a server rxdoor that supports reference counting.
//
// This implementation is for those rxdoors that do not need this method.
//
void
rxdoor::register_client(ID_node &)
{
	ASSERT(0);
}

//
// unregister_client - Notification that client nodes have released the xdoor.
// This method is only for a server rxdoor that supports reference counting.
//
// This implementation is for those rxdoors that do not need this method.
//
void
rxdoor::unregister_client(ID_node &)
{
	ASSERT(0);
}

//
// cleanup_foreignrefs - Used during cluster reconfiguration to recalculate
// the foreign reference state.
// This method is only for a server rxdoor that supports reference counting.
//
// This implementation is for those rxdoors that do not need this method.
//
void
rxdoor::cleanup_foreignrefs(const nodeset &)
{
	ASSERT(0);
}

//
// extract_handler - returns the xdoor's handler at either kernel or user
// level corresponding to the requested type.
//
handler *
rxdoor::extract_handler(handler_type type)
{
	ASSERT(type == KERNEL_HANDLER || type == USER_HANDLER);

	lock();
	umarsh_handler[type]++;
	while ((handlers[type] == NULL) && (umarsh_handler[type] > 1)) {
		// The first unmarshal of this handler is in progress.  We
		// allow that to complete since we only want one handler
		// created. Note that we haven't decremented umarshout yet, so
		// there is no fear of this xdoor being deleted while we wait.
		get_cv().wait(&get_lock());
	}
	unlock();
	return (handlers[type]);
}

//
// Notify xdoor that handler extraction is finished (decrements count
// of outstanding unmarshals).  unmarshalled indicates if this reference was
// unmarshalled at the xdoor level.
//
void
rxdoor::extract_done(handler_type type, handler *hp, bool unmarshalled)
{
	os::mutex_t	*lckp = &get_lock();

	ASSERT(hp != NULL);
	lckp->lock();
	ASSERT(handlers[type] == NULL || handlers[type] == hp);
	ASSERT(umarsh_handler[type] > 0);
	if ((--umarsh_handler[type] != 0) && (handlers[type] == NULL)) {
		get_cv().broadcast();
	}
	handlers[type] = hp;

	// This is for hxdoor specific operation
	post_extract_done(type);

	if (unmarshalled) {
		ASSERT(umarshout > 0);
		umarshout--;
		if (umarshout == 0) {
			handle_possible_unreferenced();
		}
	}
	lckp->unlock();  // xdoor may be deleted now, so use saved lckp.
}

//
// post_extract_done
//
// Noop for most xdoors
//
void
rxdoor::post_extract_done(handler_type)
{
}

//
// client_unreferenced - supports calls from client handlers.
// when all local proxies have been released.
// It may be called on a server xdoor if either the client or the server is
// at user level; i.e., one of the handlers is the gateway handler.
//
bool
rxdoor::client_unreferenced(handler_type type, uint_t marshcnt)
{

	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): c_unref uo=%d hmc=%d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    umarshout, marshcnt));

	// Account for the marshals of this handler.
	dec_marshcount(marshcnt);

	ASSERT(handlers[type] != NULL);

	if (umarsh_handler[type] > 0) {
		lckp->unlock();
		return (false);
	}

	// Release the appropriate handler
	handlers[type] = NULL;

	// check if xdoor should be released
	handle_possible_unreferenced();
	lckp->unlock();
	return (true);
}

//
// request_oneway -  processes oneway invocation requests.
// Local requests are passed to another method. For remote messages
// the routine creates the header, and passes the message to the transport.
//
ExceptionStatus
rxdoor::request_oneway(invocation &inv)
{
	MC_PROBE_0(rx_request1_start, "clustering orb xdoor", "");

	ASSERT(inv.se != NULL);

	if (local()) {
		return (request_local(inv));
	}

	// Create oneway message header in stream
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	Environment	*envp = inv.get_env();
	make_oneway_header(envp->get_cluster_id(), inv.se);
#else
	make_oneway_header(inv.se);
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	ORB_DBPRINTF(ORB_TRACE_ONEWAY,
	    ("%s(%d.%3d:%d): oneway remote\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));

	if (inv.se->get_resourcep()->get_flow_policy() ==
	    resources::BASIC_FLOW_CONTROL) {
		// Oneway invocations require a flow control header
		// that specifies resources to be freed upon request completion
		oneway_flow_header::marshal(inv.se);
	}

	ExceptionStatus retval = translate_and_send(inv);
	if (retval == CORBA::LOCAL_EXCEPTION) {
		inv.get_env()->sys_exception()->completed(CORBA::COMPLETED_NO);
	}
	return (retval);
}

//
// request_twoway -  processes twoway invocation requests.
// Local requests are passed to another method. For remote messages
// the routine creates the header, and passes the message to the transport.
// It waits for the reply, and unmarshals reply xdoors.
//
ExceptionStatus
rxdoor::request_twoway(invocation &inv)
{
	MC_PROBE_0(rx_request2_start, "clustering orb xdoor", "");

	ASSERT(inv.se != NULL);

	if (local()) {
		return (request_local(inv));
	}

	Environment	*e = inv.get_env();
	ASSERT(!e->is_nonblocking());

#ifdef DEBUG
	// Take a copy on the stack of the dest_node
	ID_node		dest_node = inv.se->get_dest_node();
#endif
	//
	// Obtain a free slot.
	// If a free slot is not available, we wait until one is available.
	//
	outbound_invo	*iip =
	    outbound_invo_table::alloc(inv.se->get_dest_node(), inv);

#if defined(_FAULT_INJECTION)
	// Prepare to handle fault information from reply message
	InvoTriggers::set_up_reply(&inv);
#endif

	// Create twoway message header in stream
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	make_twoway_header(e->get_cluster_id(), inv.se, iip->slot());
#else
	make_twoway_header(inv.se, iip->slot());
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	ORB_DBPRINTF(ORB_TRACE_TWOWAY,
	    ("%s(%d.%3d:%d): request_remote: index %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    iip->slot()));

	if (translate_and_send(inv) == CORBA::LOCAL_EXCEPTION) {
		e->sys_exception()->completed(CORBA::COMPLETED_NO);
		iip->oops();
		return (CORBA::LOCAL_EXCEPTION);
	}

	// After waiting for request result, deletes structure
	ExceptionStatus		result = iip->wait_for_results();

	if (result == CORBA::LOCAL_EXCEPTION) {
		ORB_DBPRINTF(ORB_TRACE_TWOWAY,
		    ("%s(%d.%3d:%d): local failure\n",
		    xdoor_type(), get_server_node().ndid,
		    (get_server_node().incn % 1000), server_xdoor));

		e->system_exception(
		    CORBA::COMM_FAILURE(0, CORBA::COMPLETED_MAYBE));

		return (CORBA::LOCAL_EXCEPTION);
	}
	//
	// We got a reply.
	//

	// We should be getting a reply from the node incarnation we sent to
	ASSERT(ID_node::match(inv.re->get_src_node(), dest_node));

	FAULTPT_ORPHAN(FAULTNUM_ORPHAN_UNMARSHAL_BEFORE_CHECK,
	    FaultFunctions::generic);

	uint_t xdoorcount = inv.re->get_xdoorcount();
	if (xdoorcount) {
		//
		// Protect xdoor translation process from membership change.
		// The thread blocks here while a node reconfiguration
		// changes membership.
		//
		rxdoor_membership_manager::the().begin_membership_lock();
		if (!members::the().still_alive(inv.re->get_src_node())) {
			// Orphan reply
			rxdoor_membership_manager::the().end_membership_lock();
			if (result == CORBA::NO_EXCEPTION) {
				e->system_exception(CORBA::COMM_FAILURE(0,
				    CORBA::COMPLETED_YES));
			} else {
				e->system_exception(CORBA::COMM_FAILURE(0,
				    CORBA::COMPLETED_MAYBE));
			}
			return (CORBA::LOCAL_EXCEPTION);
		}
		//
		// Translate xdoor references from reply message
		//
		Xdoor_manager::translate_in(inv.re->get_src_node(), xdoorcount,
		    inv.re->get_XdoorTab(), e);
		rxdoor_membership_manager::the().end_membership_lock();
		if (e->exception()) {
			e->sys_exception()->completed(CORBA::COMPLETED_YES);
			ORB_DBPRINTF(ORB_TRACE_TRANSLATE,
			    ("%s(%d.%3d:%d): invo translate in failed\n",
			    xdoor_type(), get_server_node().ndid,
			    (get_server_node().incn % 1000), server_xdoor));
			return (CORBA::LOCAL_EXCEPTION);
		}
	}

	FAULTPT_ORPHAN(FAULTNUM_ORPHAN_UNMARSHAL_AFTER_CHECK,
					FaultFunctions::generic);
	MC_PROBE_0(rx_request2_end, "clustering orb xdoor", "");
	return (result);
}

//
// translate_and_send - this method supports all invocation related messages,
// including both request and reply messages. The rxdoor header has already
// been completed. This method supports the translation of xdoors and sends the
// request.
//
ExceptionStatus
rxdoor::translate_and_send(service &serv)
{
	Environment	*e = serv.get_env();
	sendstream	*sendstreamp = serv.se;
	uint_t		xdoorcount = sendstreamp->get_xdoorcount();
	tr_out_info	*tr_infop = NULL;
	ID_node		&nd = sendstreamp->get_dest_node();

	//
	// The system does not send a request message from the client side when
	// an exception occurs. Separate code handles a reply message
	// containing an exception.
	//
	ASSERT(!e->exception());

	// The sendstream should have been bound to a valid incarnation number
	// in reserve_resources
	ASSERT(nd.incn != INCN_UNKNOWN);

	if (xdoorcount) {
		//
		// Translate the xdoor descriptions replacing the
		// region in XdoorTab.
		//
		tr_infop = Xdoor_manager::translate_out(nd, xdoorcount,
		    sendstreamp->get_XdoorTab(), e);
		if (e->exception()) {
			// It has to be a system_exception
			ASSERT(e->sys_exception());
			ASSERT(tr_infop == NULL);
			ORB_DBPRINTF(ORB_TRACE_TRANSLATE,
			    ("%s(%d.%3d:%d): invo translate out failed\n",
			    xdoor_type(), get_server_node().ndid,
			    (get_server_node().incn % 1000), server_xdoor));
			return (CORBA::LOCAL_EXCEPTION);
		}
	}

	orb_msg::send(sendstreamp);

	MC_PROBE_0(rx_send_sent, "clustering orb xdoor", "");

	// The sendstream should have been bound to a valid incarnation number
	// in reserve_resources which does not go invalid
	ASSERT(nd.incn != INCN_UNKNOWN);

	if (xdoorcount) {
		//
		// If we got a non-COMM_FAILURE exception then we didn't send
		// the xdoors and we do a roll_back to undo the effect of
		// translate_out().
		//
		if (e->exception() &&
			!CORBA::COMM_FAILURE::_exnarrow(e->exception())) {
				Xdoor_manager::translate_out_roll_back(nd,
				    sendstreamp->get_XdoorTab(), tr_infop);
				translate_mgr::the().done_tr_out_not_sent(nd,
				    tr_infop);
		} else {
			//
			// We either successfully sent the xdoors or the node
			// is dead. In both cases we claim that we are done.
			// If the destination node dies before sending us an
			// ack (which could happen whether or not we get a
			// COMM_FAILURE), then the CMM reconfiguration will
			// roll forward the reference counting protocol and
			// then clean up references from dead nodes.
			//
			translate_mgr::the().done_tr_out(nd);
		}
	}

	serv.set_sendstream(NULL);		// Deletes the sendstream
	if (e->exception()) {
		// Any error detected here occurred on the transmitting side
		// It has to be a system_exception
		ASSERT(e->sys_exception());
		return (CORBA::LOCAL_EXCEPTION);
	}
	MC_PROBE_0(rx_send_end, "clustering orb xdoor", "");
	return (CORBA::NO_EXCEPTION);
}

//
// only used for user to kernel (and kernel to user) invocations on
// the same node.
// This could share more implementation code with handle_request_message()
// if desired.
ExceptionStatus
rxdoor::request_local(invocation &inv)
{
	Environment	*e = inv.get_env();

	ORB_DBPRINTF(ORB_TRACE_REQ_LOC, ("%s(%d.%3d:%d): request_local!\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));
	//
	// The sendstream must be converted into a recstream for delivery
	// and any object references must be translated
	//
	convert_local_stream(inv);
	if (e->exception()) {
		return (CORBA::LOCAL_EXCEPTION);
	}
	//
	// The code is now acting as the server.
	// Test whether this xdoor will accept this invocation.
	//
	lock();
	incoming_prepare(e);
	unlock();
	if (e->exception()) {
		uint_t	xdoorcount = inv.re->get_xdoorcount();
		if (xdoorcount != 0) {
			Xdoor_manager::translate_in_roll_back(xdoorcount,
			    inv.re->get_XdoorTab());
		}
		return (CORBA::LOCAL_EXCEPTION);
	}
	//
	// Handler cannot go away while invocations are in progress.
	// The method has already incremented the count of active invocations.
	// Thus locking is not required to extract the handler.
	//
	// Determine server handler and call handle_incoming
	// Returns with the reply in a recstream and the reply xdoors
	// translated.
	//
	get_server_handler()->handle_incoming_call(inv);
	ASSERT(!e->exception());
	ASSERT(inv.se == NULL);

	// After this call the xdoor may have been deleted.
	// Be careful to avoid accessing its members from here on.
	incoming_done();

	return (CORBA::NO_EXCEPTION);
}

//
// send_reply - is called only for twoway invocations.
// The handler layer has already marshalled the out/inout/return arguments.
// Local replies are forwarded to another method.
// This method performs xdoor translation.
// For remote requests a message with an rxdoor level header is created
// and sent.
//
void
rxdoor::send_reply(service &serv)
{
	ID_node		&dest_node = serv.get_src_node();
	if (dest_node.ndid == orb_conf::local_nodeid()) {
		rxdoor::send_reply_local(serv);
		return;
	}
	//
	// The request came from a remote node.
	// Therefore the service object is a service_twoway object
	//
	ASSERT(service::TWOWAY == serv.get_service_type());
	service_twoway	&serv2 = (service_twoway &)serv;

	inbound_invo	*inboundp = serv2.get_inbound_invo();

	//
	// Release the inbound invo slot prior to sending remote reply.
	// It is safe to call this multiple times for reply msg retries.
	//
	inboundp->done();

	Environment	*e = serv2.get_env();
	if (e->exception()) {
		// When an exception is present there may not be a sendstream.
		exception_reply_remote(serv2);
		ASSERT(serv2.se == NULL);
		return;
	}
	// There must be a sendstream when no exception is present
	sendstream	*sendstreamp = serv2.se;
	ASSERT(sendstreamp != NULL);
	ASSERT(ID_node::match(dest_node, sendstreamp->get_dest_node()));

	// Obtain invocation table index
	ushort_t	the_index = serv2.get_index_invo();

	//
	// Determine whether signal occurred
	//
	uint_t		flags;
	if (inboundp->was_signaled()) {
		flags = rxdoor_reply_header::SIGNALED;
	} else {
		flags = rxdoor_reply_header::NONE;
	}

	// Create reply message header in stream
	rxdoor_reply_header::make_header(sendstreamp, the_index, flags);

	ORB_DBPRINTF(ORB_TRACE_REPLY, ("normal reply for %d.%3d index %d\n",
	    dest_node.ndid, (dest_node.incn % 1000), the_index));

	// Translate any object references and send reply message
	if ((translate_and_send(serv2) == CORBA::LOCAL_EXCEPTION) &&
	    CORBA::RETRY_NEEDED::_exnarrow(e->exception()) == NULL &&
	    CORBA::COMM_FAILURE::_exnarrow(e->exception()) == NULL) {
		// The normal reply failed
		e->sys_exception()->completed(CORBA::COMPLETED_YES);
		exception_reply_remote(serv2);
		ASSERT(serv2.se == NULL);
	} else {
		// succeeded, need retry or comm_failure.
		// Either way we release the sendstream and return to top.
		serv2.set_sendstream(NULL);	// Delete the sendstream
	}
}

//
// send_reply_local - is called only for local twoway invocations.
// The handler layer has aleady marshalled the out/inout/return arguments.
// This method performs xdoor translation.
// The sendstream is converted into a recstream.
//
// This method is static so that the hxdoor code can use it even when
// there is no rxdoor.
//
// static
void
rxdoor::send_reply_local(service &serv)
{
	Environment	*e = serv.get_env();
	if (e->exception()) {
		// When an exception is present, there may not be a sendstream
		// Transmit the reply error message.
		exception_reply_local(serv);
		ASSERT(serv.se == NULL);
		return;
	}
	// The request came from the local node.
	// Therefore the service object is an invocation object
	ASSERT(service::INVO == serv.get_service_type());
	invocation	&inv = (invocation &)serv;

	//
	// The sendstream must be converted into a recstream for delivery
	// and any object references must be translated
	//
	convert_local_stream(inv);
	if (e->exception()) {
		// Transmit the reply error message.
		exception_reply_local(serv);
		ASSERT(serv.se == NULL);
	}
}

//
// convert_local_stream - The sendstream must be converted into a recstream
// for delivery and any object references must be translated.
// The caller must check for exception and take appropriate recovery actions.
//
// static
void
rxdoor::convert_local_stream(invocation &inv)
{
	// This is from a local client. Convert the sendstream
	// to a recstream so we can read from it.
	nil_sendstream	*sendstreamp = (nil_sendstream *)inv.se;
	inv.set_recstream(make_recstream_funcp(sendstreamp));
	ASSERT(!inv.get_env()->exception());

	// Delete the sendstream
	inv.set_sendstream(NULL);

	recstream	*recstreamp = inv.re;
	uint_t		xdoorcount = recstreamp->get_xdoorcount();
	if (xdoorcount != 0) {
		//
		// Translate the xdoor descriptions replacing
		// the region in XdoorTab. Do it here before the releases
		// are performed by the skel.
		//
		Xdoor_manager::translate_local(xdoorcount,
		    recstreamp->get_XdoorTab(), inv.get_env());
	}
}

//
// get_xdoor_desc
//
// Default method for rxdoors that returns the xdoor descriptor.
//
void
rxdoor::get_xdoor_desc(rxdoor_descriptor &xd)
{
	rxdoor_id &rxdid = (rxdoor_id &)xd;

	rxdid.xdid = server_xdoor;
	rxdid.node = get_server_node();
}

//
// marshal_xdid
//
// Default method for rxdoors that marshals the xdoor_desc.
//
void
rxdoor::marshal_xdid(MarshalStream &ms)
{
	rxdoor_id	rxdid;

	get_xdoor_desc(rxdid);
	rxdid._put(ms);
}

// virtual destructor
rxdoor::~rxdoor()
{
	bucketp = NULL;		// for lint
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
void
rxdoor::make_oneway_header(uint_t cluster_id, sendstream *sendstreamp)
{
	rxdoor_oneway_header::make_header(cluster_id, sendstreamp,
	    server_xdoor);
}
#else
void
rxdoor::make_oneway_header(sendstream *sendstreamp)
{
	rxdoor_oneway_header::make_header(sendstreamp, server_xdoor);
}
#endif

#if (SOL_VERSION >= __s10) && !defined(UNODE)
void
rxdoor::make_twoway_header(uint_t cluster_id, sendstream *sendstreamp,
    ushort_t slot)
{
	rxdoor_twoway_header::make_header(cluster_id, sendstreamp,
	    server_xdoor, slot);
}
#else
void
rxdoor::make_twoway_header(sendstream *sendstreamp, ushort_t slot)
{
	rxdoor_twoway_header::make_header(sendstreamp, server_xdoor, slot);
}
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

//
// exception_reply_remote - sends exception reply to invocation originator.
// This method uses a new reply sendstream for the reply message, because
// there may not be a reply sendstream and performance is not critical here.
//
// static
void
rxdoor::exception_reply_remote(service_twoway &serv2)
{
	FAULTPT_ORB(FAULTNUM_ORB_RXDOOR_EXCEPTION_REPLY_REMOTE,
	    FaultFunctions::generic);

	// Discard any existing reply sendstream
	serv2.set_sendstream(NULL);

	Environment		*e = serv2.get_env();
	CORBA::Exception	*exceptionp;
	ID_node			&src_node = serv2.get_src_node();
	bool			retry_needed;

	// Obtain invocation table index
	ushort_t		the_index = serv2.get_index_invo();

	//
	// Reply messages always allow blocking.
	// So this exception should never occur.
	//
	ASSERT(CORBA::WOULDBLOCK::_exnarrow(e->exception()) == NULL);

	//
	// This error can happen when a normal reply is sent
	// or during a CMM "to_stop" transition.
	//
	if (CORBA::COMM_FAILURE::_exnarrow(e->exception()) != NULL) {
		//
		// This error prevents sending an exception reply
		//
		e->clear();
		return;
	}

	//
	// Transfer the exception from the Environment to a local variable
	// This is so that we can retry marshalling multiple times
	//
	exceptionp = e->release_exception();

	do {
		retry_needed = false;

		//
		// Identify message resources
		// The reply will have only a marshalled exception
		//
		resources	resource(resources::REPLY_FLOW, e,
		    invocation_mode::NONE,
		    (uint_t)sizeof (rxdoor_reply_header),	// header size
		    6 * (uint_t)sizeof (ulong_t),		// data size
		    src_node, &serv2, REPLY_MSG);
		//
		// Create a sendstream for the reply message
		// The error may have occurred before the system obtained
		// a valid rxdoor. This explains why the code skips the
		// rxdoor level "reserve_resources" method.
		//
		sendstream *sendstreamp = orb_msg::reserve_resources(&resource);

		//
		// An exception existed prior to the attempt to obtain a
		// sendstream.  So must directly check for presence of valid
		// sendstream.
		//
		if (sendstreamp == NULL) {
			//
			// Client Node is dead.
			// There is no place to return reply info,
			// even though the request was performed.
			//
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e->exception()));
			e->clear();
			delete exceptionp;
			return;
		}
		serv2.set_sendstream(sendstreamp);

		//
		// Load information from the exception that existed
		// upon entering this method.
		//
		exceptionp->_put(serv2);

		//
		// Determine whether signal occurred
		//
		uint_t		flags;
		if (CORBA::PRIMARY_FROZEN::_exnarrow(exceptionp) !=
		    NULL) {
			flags = rxdoor_reply_header::RETRY;
		} else {
			flags = rxdoor_reply_header::EXCEPTION;
		}

		if (serv2.get_inbound_invo()->was_signaled()) {
			flags |= rxdoor_reply_header::SIGNALED;
		}

		// Create reply message header in stream
		rxdoor_reply_header::make_header(sendstreamp, the_index, flags);

		ORB_DBPRINTF(ORB_TRACE_REPLY,
		    ("exception reply for %d.%3d index %d\n", src_node.ndid,
		    (src_node.incn % 1000), serv2.get_index_invo()));

		// Transmit the exception reply message
		orb_msg::send(sendstreamp);

		if (e->exception()) {
			if (CORBA::RETRY_NEEDED::_exnarrow(e->exception())) {
				retry_needed = true;
			} else {
				ASSERT(CORBA::COMM_FAILURE::_exnarrow(
				    e->exception()));
				retry_needed = false;
			}
			e->clear();
		}

		// Delete the sendstream
		serv2.set_sendstream(NULL);

	} while (retry_needed);

	delete exceptionp;
}

//
// exception_reply_local - sends exception reply to invocation originator.
// This method must be static so that it can be shared with the hxdoor code.
//
// static
void
rxdoor::exception_reply_local(service &serv)
{
	// Discard any existing reply sendstream
	serv.set_sendstream(NULL);

	Environment	*e = serv.get_env();

	// Release the exception from the Environment to a local variable
	// This is so reserve_resources does not see an incoming exception
	CORBA::Exception *ex = e->release_exception();
	ASSERT(!e->exception());
	ASSERT(ex != NULL);

	// Identify message resources
	// The reply will have only a marshalled exception
	//
	resources	resource(resources::REPLY_FLOW, e,
	    invocation_mode::NONE,
	    0,					// header size
	    4 * (uint_t)sizeof (ulong_t),	// data size
	    orb_conf::current_id_node(), NULL, REPLY_MSG);
	//
	// Create a sendstream for the reply message
	//
	sendstream	*sendstreamp = orb_msg::reserve_resources(&resource);
	//
	// Local requests are not flow controlled.
	// Reply messages are always blocking,
	// so the sendstream allocation simply waits for memory allocation.
	// Local transports do not check for exceptions prior to
	// allocating a sendstream. So this should always eventually succeed.
	//
	ASSERT(sendstreamp != NULL);
	serv.set_sendstream(sendstreamp);

	// Marshal exception information and delete it
	ex->_put(serv);
	delete ex;

	// Convert the sendstream to a recstream so we can read from it.
	nil_sendstream *se = (nil_sendstream *)sendstreamp;
	serv.set_recstream(make_recstream_funcp(se));
	serv.set_sendstream(NULL);
}

//
// get_server_handler
//
// Returns the server handler for a server rxdoor
// Only usable on server xdoor, will ASSERT fail on client rxdoors
handler *
rxdoor::get_server_handler()
{
	ASSERT(local());
	ASSERT(server_handler_index == Xdoor::USER_HANDLER ||
	    server_handler_index == Xdoor::KERNEL_HANDLER);
	return (handlers[server_handler_index]);
}


// Initialize the server handler for a server rxdoor
void
rxdoor::set_server_handler(handler *h)
{
	ASSERT(local());
	ASSERT(server_handler_index == Xdoor::USER_HANDLER ||
		server_handler_index == Xdoor::KERNEL_HANDLER);
	handlers[server_handler_index] = h;
}

//
//
// Returns the client handler for a server rxdoor
handler *
rxdoor::get_client_handler()
{
	ASSERT(local());
	ASSERT(client_handler_index == Xdoor::USER_HANDLER ||
	    client_handler_index == Xdoor::KERNEL_HANDLER);
	return (handlers[client_handler_index]);
}

//
// handle_reply - Process a remote invocation reply message.
// After validating that the reply message is valid,
// the routine then wakens the thread waiting on this remote invocation.
//
// The recstream is normally deleted with the invocation structure.
// The error path must delete the recstream.
//
// static
void
rxdoor::handle_reply(recstream *recstreamp)
{
	MC_PROBE_0(rx_reply_start, "clustering orb xdoor", "");

#if defined(FAULT_ORPHAN)
	//
	// This fault point turns off the FAULTNUM_ORPHAN_DEFER_REPLIES
	// fault point in orb_msg::deliver_message().
	//
	if (fault_triggered(FAULTNUM_ORPHAN_DEFER_REPLIES_OFF, NULL, NULL)) {
		NodeTriggers::clear(FAULTNUM_ORPHAN_DEFER_REPLIES);
	}
#endif

	// Reply messages are treated as twoway messages by invocation mode.
	recstreamp->set_twoway();

	// Unmarshal data from header and destroy header
	rxdoor_reply_header header(recstreamp);

	ID_node		&src_node = recstreamp->get_src_node();

	ORB_DBPRINTF(ORB_TRACE_REPLY,
	    ("handle reply from node %d.%3d index %d\n", src_node.ndid,
	    (src_node.incn % 1000), header.slot));

	outbound_invo_table::wakeup(header.slot, header.flags, recstreamp,
	    src_node);

	MC_PROBE_0(rx_reply_end, "clustering orb xdoor", "");
}

//
// handle_twoway - Accepts a twoway request message from a remote node
// and delivers the request message to a server handler.
//
// The recstream is normally deleted by the server.
// The error path must delete the recstream.
//
// static
void
rxdoor::handle_twoway(recstream *recstreamp)
{
	recstreamp->set_twoway();

	// The src_node is only valid as long as the recstream exists.
	ID_node		&src_node = recstreamp->get_src_node();
	Environment	e(src_node);

	rxdoor_twoway_header header(recstreamp);
	//
	// recstream was allocated on the heap.
	// The destructor for service will delete recstream
	//
	service_twoway	serv(recstreamp, &e, header.slot);

	handle_request_common(src_node, serv, &header, header.get_kit_id());

	MC_PROBE_0(rx_handle_message_end, "clustering orb xdoor", "");
}

//
// handle_oneway - Accepts a oneway request message from a remote node
// and delivers the request message to a server handler.
//
// The recstream is normally deleted by the server.
// The error path must delete the recstream.
//
// static
void
rxdoor::handle_oneway(recstream *recstreamp)
{
	// The invocation mode for remote messages defaults to oneway.

	//
	// The src_node is only valid as long as the recstream exists.
	// The recstream is gone when the handler completes the invocation.
	//
	ID_node		&src_node = recstreamp->get_src_node();
	Environment	e(src_node);

	//
	// recstream was allocated on the heap.
	// The destructor for service will delete recstream
	//
	service_oneway		serv(recstreamp, &e);

	// Unmarshal the flow control information for oneways
	serv.set_pool(oneway_flow_header::unmarshal(recstreamp));

	rxdoor_oneway_header header(recstreamp);
	//
	// Oneways are not supported by hxdoors, so use the default
	// kit id.
	//
	handle_request_common(src_node, serv, &header, header.get_kit_id());

	resource_done::select_resource_done(
	    serv.get_src_node().ndid, serv.get_pool())->
	    oneway_done(serv.get_src_node());

	MC_PROBE_0(rx_handle_message_end, "clustering orb xdoor", "");
}

//
// handle_ckpt - Accepts a ckpt oneway request message from a remote node
// and delivers the request message to a server handler.
// All checkpoint messages are processed by one server thread.
// The system does not flow control checkpoint messages.
//
// The recstream is normally deleted by the server.
// The error path must delete the recstream.
//
// static
void
rxdoor::handle_ckpt(recstream *recstreamp)
{
	// The invocation mode for remote messages defaults to oneway.

	// The src_node is only valid as long as the recstream exists.
	// The recstream is gone when the handler completes the invocation.
	ID_node		&src_node = recstreamp->get_src_node();
	Environment	e(src_node);

	// recstream was allocated on the heap.
	// The destructor for service will delete recstream
	service_oneway		serv(recstreamp, &e);

	rxdoor_oneway_header	header(recstreamp);

	handle_request_common(src_node, serv, &header, header.get_kit_id());

	MC_PROBE_0(rx_handle_message_end, "clustering orb xdoor", "");
}

//
// handle_request_common - an inbound request (either oneway or
// twoway) has arrived. We need to make sure that the server xdoor
// still exists and the client is alive before we choose the handle
// the request.
//
// NOTE: It appears that the kitid argument could be obtained from the
// invocation header and therefore it is not necessary to pass it as
// an explicit argument. However, rxdoor_invo_header cannot have
// virtual methods. Since the header can be oneway or twoway header,
// get_kit_id() has to be virtual so it can return the corresponding
// kit id. See rxdoor_header.h for more info.
//
// static
void
rxdoor::handle_request_common(ID_node &src_node, service &serv,
	rxdoor_invo_header *headerp, xkit_id_t kitid)
{
	rxdoor		*xdp;
	rxdoor_bucket	*xbp;

	Environment	*e = serv.get_env();
	ASSERT(!e->exception());

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	uint32_t	cluster_id = headerp->zone_cluster_id;
	e->set_cluster_id(cluster_id);
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	FAULTPT_ORPHAN(FAULTNUM_ORPHAN_REQUEST_BEFORE_RXDOOR_LOOKUP,
						FaultFunctions::generic);
	rxdoor_kit *kitp = (rxdoor_kit *)xdoor_repository::the().
	    get_kit(kitid);

	// Get and lock the target xdoor.
	rxdoor_membership_manager::the().begin_membership_lock();

	// The system will not find a valid xdoor for orphan messages
	xdp = kitp->lookup_rxdoor_from_invo_header(headerp, &xbp);

	//	Must first test that the request came from a valid node.
	//	An orphan message may attempt to use a server xdoor that
	//	has been recycled as a client xdoor, which would cause
	//	a panic in "incoming_prepare".
	//	*** The server xdoor should not be used prior to this test.
	//
	if (!members::the().still_alive(src_node)) {
		//
		// The source node has died.
		// The system will discard an exception reply message with
		// a COMM_FAILURE exception.
		//
		// Normally there is no place to return an error.
		// During a CMM "to_stop" transition, the system marks all of
		// the other nodes with invalid incarnation numbers, even
		// though those nodes may still be up. The system does not
		// process requests during this "to_stop" transition and must
		// not send a reply during this transition.
		// A node reconfiguration will clean up the discarded requests.
		//
		e->system_exception(CORBA::COMM_FAILURE(0,
		    CORBA::COMPLETED_NO));
		orb_stats_mgr::the().inc_orphans(src_node.ndid,
		    serv.re->get_msgtype());
	} else if (xdp == NULL) {
		//
		// This situation can only happen because of bug 4017584.
		// The client does a oneway and then releases the last
		// reference to the server. The system currently could
		// process the release before the oneway.
		// When that bug is fixed, should be able to assert
		// that the xdoor is not null.
		// The minor code has an error code that is used solely
		// to distinguish this bug situation.
		//
		e->system_exception(CORBA::INV_OBJREF(EIDRM,
					CORBA::COMPLETED_NO));
	} else {
		//
		// Test whether this xdoor will accept this invocation.
		// When successful, the count of active services is
		// incremented, which will guarantee that the door will stay
		// around for the duration of the invocation.
		//
		xdp->incoming_prepare(e);
	}
	xbp->unlock();

	if (e->exception()) {
		e->sys_exception()->completed(CORBA::COMPLETED_NO);
		uint_t	xdoorcount = serv.re->get_xdoorcount();
		Xdoor_manager::translate_in_cancel(src_node, xdoorcount,
		    serv.re->get_XdoorTab());
		rxdoor_membership_manager::the().end_membership_lock();

		// Transmit the reply error message
		if (!serv.re->get_invo_mode().is_oneway()) {
			//
			// We may not have an rxdoor at this point.
			//
			ASSERT(service::TWOWAY == serv.get_service_type());
			service_twoway	&serv2 = (service_twoway &)serv;

			//
			// Release inbound invo slot prior to sending reply.
			//
			inbound_invo	*inboundp = serv2.get_inbound_invo();
			inboundp->done();

			exception_reply_remote(serv2);
		}
		return;
	}

	//
	// Translate from xdoor identification numbers to xdoors.
	//
	uint_t		xdoorcount = serv.re->get_xdoorcount();
	if (xdoorcount != 0) {
		Xdoor_manager::translate_in(src_node, xdoorcount,
		    serv.re->get_XdoorTab(), e);
		if (e->exception()) {
			rxdoor_membership_manager::the().end_membership_lock();

			e->sys_exception()->completed(CORBA::COMPLETED_NO);
			ORB_DBPRINTF(ORB_TRACE_TRANSLATE,
			    ("%s(%d.%3d:%d): translate in failed\n",
			    xdp->xdoor_type(),
			    xdp->get_server_node().ndid,
			    (xdp->get_server_node().incn % 1000),
			    xdp->server_xdoor));

			// Transmit the reply error message
			if (!serv.re->get_invo_mode().is_oneway()) {
				//
				// Even though this is an exception reply,
				// go through send_reply to clean up
				// the inbound_invo structure.
				//
				xdp->send_reply(serv);
			}
			xdp->incoming_done();
			return;
		}

	}
	rxdoor_membership_manager::the().end_membership_lock();

	MC_PROBE_0(rx_handle_message_tohandle, "clustering orb xdoor", "");
	//
	// Handler cannot go away while invocations are in progress.
	// The method has already incremented the count of active invocations.
	// Thus locking is not required to extract the handler.
	//
	// Determine server handler and Pass the call to the handler.
	// Notice that the service structure is allocated on the stack,
	// while the buffers are not.
	// Note that the client can die while this call is in
	// progress.  The service must be able to deal with this.
	//
	xdp->get_server_handler()->handle_incoming_call(serv);

	//
	// Any required reply will have already been sent by this point.
	//
	MC_PROBE_0(rx_handle_message_handled, "clustering orb xdoor", "");

	//
	// After this call the xdoor may have been deleted.
	// Be careful to avoid accessing its members from here on.
	xdp->incoming_done();
	//
	// There can be no exception here. The reply message would have
	// consumed any exception.
	//
	ASSERT(!e->exception());
	ASSERT(serv.se == NULL);
}
