/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  sxdoor_in.h
 *
 */

#ifndef _SXDOOR_IN_H
#define	_SXDOOR_IN_H

#pragma ident	"@(#)sxdoor_in.h	1.8	08/05/20 SMI"

//
// This defines the inline methods for the classes that manage the xdoors
// in a user level process.
//

// Inline methods for sxdoor_table

// static
inline sxdoor_table &
sxdoor_table::the()
{
	ASSERT(sxdoor_table::the_sxdoor_tablep != NULL);
	return (*sxdoor_table::the_sxdoor_tablep);
}

inline void
sxdoor_table::lock()
{
	lck.lock();
}

inline void
sxdoor_table::unlock()
{
	lck.unlock();
}

inline bool
sxdoor_table::lock_held()
{
	return ((bool)lck.lock_held());
}

// Inline methods for sxdoor_manager

// static
inline sxdoor_manager &
sxdoor_manager::the()
{
	ASSERT(sxdoor_manager::the_sxdoor_managerp != NULL);
	return (*sxdoor_manager::the_sxdoor_managerp);
}

inline void
sxdoor_manager::lock()
{
	lck.lock();
}

inline void
sxdoor_manager::unlock()
{
	lck.unlock();
}

inline bool
sxdoor_manager::lock_held()
{
	return ((bool)lck.lock_held());
}

#endif	/* _SXDOOR_IN_H */
