/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)Xdoor.cc	1.110	08/05/20 SMI"

//
// This file contains a bunch of generic Xdoor management code.
// This includes the management of the xdoor identifiers used internally
// by a domain and the translation code used for passing lists of xdoor
// identifiers between domains.

#include <orb/xdoor/Xdoor.h>
#include <sys/mc_probe.h>

#ifdef _KERNEL_ORB
#include <orb/debug/orb_trace.h>
#include <orb/xdoor/translate_mgr.h>
#include <sys/os.h>
#include <sys/time.h>

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
os::mutex_t	server_done_mutex;
os::condvar_t	server_done_cv;
#endif


tr_out_info::tr_out_info(uint_t num_xdoors, Region &r, Environment *e) :
	_DList::ListElem(this),
	MarshalStream(r, e),
	count(num_xdoors)
{
}

tr_out_info::~tr_out_info()
{
	dispose();
}

// Notify all the xdoors that their marshal has been received.
void
tr_out_info::ack_xdoors()
{
	prepare();
	for (uint_t i = 0; i < count; i++) {
		Xdoor *xdp;
		xdp = (Xdoor *)get_pointer();
		xdp->marshal_received();
	}
}

// Xdoor manager stuff

// translate_out
//
// The input to translate_out is a list of xdoor pointers and a destination
// node id + incarnation number (i.e., the node to which we are sending the
// references). If we are marshaling reference counting xdoors, then we must
// not send any references to a node which has failed.
//
// The data from input MarshalStream is transfered to a tr_out_info data
// structure and stored until the marshals are acked by the reference counting
// protocol. The input MarshalStream is reinitialized with a new MarshalStream
// containing the translated xdoors. If there is an error, the original
// MarshalStream is disposed.
//
// The output MarshalStream's cursor is at head of stream when we return
// successfully.
//
// On success returns a pointer to a tr_out_info data strucutre which should
// be passed to one of translate_mgr::done_tr_out() or
// Xdoor_manager::translate_out_roll_back().
// On failure NULL is returned.
//
tr_out_info *
Xdoor_manager::translate_out(ID_node &node, uint_t num_xdoors,
    MarshalStream &xdoors, Environment *e)
{
	MC_PROBE_0(translate_out_start, "clustering orb xdoor", "");

	Xdoor	*xdp;
	uint_t	i;


	ASSERT(!e->exception());

	// Save off a copy of all the xdoor pointers for use in tr_infop.
	tr_out_info *tr_infop = new tr_out_info(num_xdoors, xdoors.region(),
	    e);
	ASSERT(!e->exception());
	ASSERT(xdoors.empty());
	MC_PROBE_0(translate_out_infop, "clustering orb xdoor", "");

	//
	// Get a read lock on the fact that the destination is alive.
	// If we are successful we return with this lock held and the caller
	// needs to call done_tr_out() or done_tr_out_not_sent().
	//
	translate_mgr::the().begin_tr_out(node, tr_infop, e);
	if (e->exception()) {
		ASSERT(e->sys_exception() != NULL);
		ORB_DBPRINTF(ORB_TRACE_TRANSLATE,
		    ("begin_tr_out to %d.%d failed\n", node.ndid, node.incn));
		translate_out_cancel(num_xdoors, *tr_infop);
		delete tr_infop; // Disposes the stream.
		return (NULL);
	}
	MC_PROBE_0(translate_out_lock, "clustering orb xdoor", "");

	//
	// Marshal the xdoors from "tr_infop" to "xdoors".
	// The sizeof (uint64_t) corresponds to the marshalled "tr_infop".
	// The "get_max_door_len()" is not adjusted when "MARSHAL_DEBUG"
	// is used. Hence the size is just a hint. Hence not considering
	// MARSHAL_DEBUG here.
	//
	xdoors.set_allocsize((uint_t)sizeof (uint64_t) +
		num_xdoors * xdoor_repository::the().get_max_xdoor_len());
	MC_PROBE_0(translate_out_alloc, "clustering orb xdoor", "");

	// Write down the identifier for this translate_out.
	xdoors.put_unsigned_longlong((uint64_t)tr_infop);

	tr_infop->prepare();		// rewind incoming Xdoortab
	for (i = 0; i < num_xdoors; i++) {
		ASSERT(!e->exception());
		xdp = (Xdoor *)tr_infop->get_pointer();

		xdp->marshal(node, xdoors, e);
		if (e->exception())
			break;
	}

	if (e->exception() != NULL) {
		//
		// We got an exception.
		// We need to undo the marshaling that we have done and cancel
		// the marshals that we have not done. The xdoor that fails
		// needs no recovery.
		//
		// Roll back the marshaling on the xdoors already marshaled.
		translate_out_roll_back_stream(node, i, xdoors);
		xdoors.dispose();

		// Cancel those that we haven't touched
		translate_out_cancel_stream(num_xdoors - (i + 1), *tr_infop);

		translate_mgr::the().done_tr_out_not_sent(node, tr_infop);
		tr_infop = NULL;
	}
	MC_PROBE_0(translate_out_end, "clustering orb xdoor", "");
	return (tr_infop);
}

// Cancels the marshal of the whole xdoor list. Places the cursor at the
// the first xdoor and uses the common translate_out_cancel_stream().
void
Xdoor_manager::translate_out_cancel(uint_t num_xdoors, MarshalStream &xdoors)
{
	xdoors.prepare();
	translate_out_cancel_stream(num_xdoors, xdoors);
	xdoors.dispose();
}


// The input to translate_out_cancel is a list of xdoor pointers with
// the cursor advanced to the first xdoor to be canceled. On completion,
// the cursor has advanced past the all the xdoor references which have
// been processed.
void
Xdoor_manager::translate_out_cancel_stream(uint_t num_xdoors,
    MarshalStream &xdoors)
{
	Xdoor	*xdp;

	for (uint_t i = 0; i < num_xdoors; i++) {
		xdp = (Xdoor *)xdoors.get_pointer();
		xdp->marshal_cancel();
	}
}

//
// Rolls back the work done in translate_out. Called when we decide
// not to send the references after all.
// This is called after translate out has completed.  We need to back out the
// the effect on the xdoors.
//
// The marshal stream is disposed before we return.
//
void
Xdoor_manager::translate_out_roll_back(ID_node &node, MarshalStream &xdoors,
    tr_out_info *tr_infop)
{
	if (tr_infop->count != 0) {
		translate_out_roll_back_stream(node, tr_infop->count, xdoors);
		tr_infop->count = 0;
	}
	xdoors.dispose();
}

//
// Rolls back the work done in translate_out. Called when we decide
// not to send the references after all.
// We start from the beginning of a stream of marshaled xdoors and
// roll back each one.
// No effort is made to leave the marshalstream cursor any specific place
void
Xdoor_manager::translate_out_roll_back_stream(ID_node &node, uint_t num_xdoors,
    MarshalStream &xdoors)
{
	xkit_id_t	xd_type;
	xdoor_kit	*xd_kitp;

	xdoors.prepare();	// rewind to start reading MarshalStream
	// Advance past the identifier for the translate_out().
	(void) xdoors.get_unsigned_longlong();

	for (uint_t i = 0; i < num_xdoors; i++) {
		xd_type = xdoors.get_octet();
		xd_kitp = xdoor_repository::the().get_kit(xd_type);
		xd_kitp->marshal_roll_back(node, xdoors);
	}
}

//
// The input MarshalStream should be freed and reinitialized with a
// new MarshalStream containing the translated xdoors.  If there is an
// error, the original xdoors is disposed.
//
// The output MarshalStream's cursor is at head of stream
// when we return successfully.
//
void
Xdoor_manager::translate_in(ID_node &node, uint_t num_xdoors,
    MarshalStream &xdoors, Environment *e)
{
	MC_PROBE_0(translate_in_start, "clustering orb xdoor", "");

#ifdef MARSHAL_DEBUG
	MarshalStream	newdoors((uint_t)(sizeof (uint32_t) +
	    sizeof (Xdoor *)) * num_xdoors, e);
#else
	MarshalStream	newdoors((uint_t)sizeof (Xdoor *) * num_xdoors, e);
#endif
	xkit_id_t	xdtyp;
	xdoor_kit	*xdkp;
	Xdoor		*xdp;
	uint_t		i;

	ASSERT(num_xdoors > 0);
	uint64_t tr_info = xdoors.get_unsigned_longlong();

	for (i = 0; i < num_xdoors; i++) {
		// xdoor type comes first.
		xdtyp = xdoors.get_octet();
		xdkp = xdoor_repository::the().get_kit(xdtyp);
		xdp = xdkp->unmarshal(node, xdoors, e);
		if (e->exception()) {
			break;
		}
		// OK: write down the xdoor id.
		newdoors.put_pointer(xdp);
	}

	if (e->exception()) {
		//
		// Recover from failure.
		// Assume no recovery is needed for the xdoor that failed and
		// that it has advanced the stream to the next xdoor's info.
		// We then cancel any remaining xdoors and rollback any that
		// were already translated in.
		// Rollback everything we have translated in.
		//
		newdoors.prepare();
		translate_in_roll_back(i, newdoors);

		// Cancel the translation of the remaining xdoors.
		translate_in_cancel_stream(node, num_xdoors - (i + 1), xdoors);

		newdoors.dispose();	// Dispose new region
	}

	translate_mgr::the().done_tr_in(node, tr_info);

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)

	if (fault_triggered(FAULTNUM_HA_FRMWK_KILL_NODE_2, NULL, NULL)) {
		server_done_mutex.lock();
		server_done_cv.wait(&server_done_mutex);
		server_done_mutex.unlock();
		//
		// Exit i.e. kill this unode once the test server has
		// shutdown.
		//
		os::printf("Received order to die. Dying\n");
		_exit(0);
	}

#endif

	xdoors.dispose();	// Dispose old region
	xdoors.useregion(newdoors.region()); // positions cursor at beginning

	MC_PROBE_0(translate_in_end, "clustering orb xdoor", "");
}

// We will not be calling translate in for these xdoors.  However,
// we must complete the reference counting protocol for them.
//
// The input to translate_in_cancel is a marshalstream which would
// have been passed to translate_in - a stream of marshaled xdoors.
// No effort is made to leave the marshal stream cursor any particular place
void
Xdoor_manager::translate_in_cancel(ID_node &node, uint_t num_xdoors,
    MarshalStream &xdoors)
{
	uint64_t tr_info;
	if (num_xdoors != 0) {
		tr_info = xdoors.get_unsigned_longlong();
		translate_in_cancel_stream(node, num_xdoors, xdoors);
		translate_mgr::the().done_tr_in(node, tr_info);
	}
}

// Used by translate_in to cancel remaining xdoors and by translate_in_cancel
// to cancel them all.
// The input is a marshalstream which contains a bunch of marshaled xdoors.
// The cursor points to the first translation to be canceled.
void
Xdoor_manager::translate_in_cancel_stream(ID_node &node, uint_t num_xdoors,
    MarshalStream &xdoors)
{
	xkit_id_t	xd_type;
	xdoor_kit	*xd_kitp;

	for (uint_t i = 0; i < num_xdoors; i++) {
		xd_type = xdoors.get_octet();
		xd_kitp = xdoor_repository::the().get_kit(xd_type);
		xd_kitp->unmarshal_cancel(node, xdoors);
	}
}

// A translate in has been performed, but we don't want to unmarshal the
// handlers.  Rolling back the unmarshals allows the xdoors to synchronise
// their unmarshal counts with the handlers.
// Input is a list of local xdoor pointers.
void
Xdoor_manager::translate_in_roll_back(uint_t num_xdoors, MarshalStream &xdoors)
{
	Xdoor	*xdp;

	for (uint_t i = 0; i < num_xdoors; i++) {
		xdp = (Xdoor *)xdoors.get_pointer();
		xdp->unmarshal_roll_back();
	}
}

//
// Adjusts both marshal and unmarshal counts for local xdoors,
// to make them look like the xdoor was both marshalled and
// unmarshalled in this domain.  This is used for "local"
// invocations, i.e. user to kernel or kernel to user invocations
// via the gateway.  Input is a list of xdoor pointers.
//
void
Xdoor_manager::translate_local(uint_t num_xdoors, MarshalStream &xdoors,
    Environment *e)
{
	Xdoor *xdp;
	uint_t i;

	xdoors.prepare();
	for (i = 0; i < num_xdoors; i++) {
		xdp = (Xdoor *)xdoors.get_pointer();
		xdp->marshal_local(e);		// bump marshal and unmarshal
	}
	// rewind
	xdoors.prepare();
	if (e->exception()) {
		// marshal_local returns an exception (BAD_PARAM)
		// and we need to recover unmarshalcounts
		// marshalcounts were already adjusted in marshal_local
		for (i = 0; i < num_xdoors; i++) {
			xdp = (Xdoor *)xdoors.get_pointer();
			xdp->unmarshal_roll_back();
		}
		// dispose the xdoorTab as the pointers in the xdoorTab may
		// now be invalid (xdoors may have been deleted
		xdoors.dispose();
	}
}
#endif	// _KERNEL_ORB

//
// class Xdoor methods
//

//
//  revoked - The xdoor is told to invalidate itself and accept no more
// calls for service. After a revoked has been processed by an
// xdoor further external calls cannot proceed.
// Only allowed for server xdoors. This default implementation is for clients.
//
void
Xdoor::revoked()
{
	ASSERT(0);
}

//
// accept_unreferenced - Used by handler after receiving an unreferenced call
// from the xdoor. It tells the xdoor to go away, and
// the handler will not attempt to make use of the xdoor any more.
// Only allowed for reference counted server xdoors.
// This default implementation is for all other xdoors.
//
void
Xdoor::accept_unreferenced()
{
	ASSERT(0);
}

//
// reject_unreferenced - Used by handler after receiving an unreferenced call
// from the xdoor. It tells the xdoor it should not go away
// at this time.
// Only allowed for reference counted server xdoors.
// This default implementation is for all other xdoors.
//
void
Xdoor::reject_unreferenced(uint_t)
{
	ASSERT(0);
}

//
// clear_unreferenced - Used by handler after receiving an unreferenced call
// from the xdoor. It tells the xdoor to enable unreferences
// but do not call handle_possible_unreferenced at this time.
// Only allowed for reference counted server xdoors.
// This default implementation is for all other xdoors.
//
void
Xdoor::clear_unreferenced()
{
	ASSERT(0);
}

//
// reserve_reply_resources - Identify resources needed at xdoor level
// for invocation reply. Only server Xdoor needs to implement this method.
//
sendstream *
Xdoor::reserve_reply_resources(resources *)
{
	ASSERT(0);
	return (NULL);
}

void
Xdoor::release_resources(resources *)
{
}

void
Xdoor::send_reply(service &)
{
	ASSERT(0);
}

bool
Xdoor::server_dead()
{
	ASSERT(0);
	return (false);
}
