/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SOLARIS_XDOOR_H
#define	_SOLARIS_XDOOR_H

#pragma ident	"@(#)solaris_xdoor.h	1.90	08/05/20 SMI"

//
// This defines the xdoor that is used in user level processes.
//


#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/xdoor_kit.h>
#include <orb/buffers/Buf.h>
#include <orb/invo/invocation.h>
#include <orb/handler/handler.h>
#include <orb/refs/unref_threadpool.h>
#include <sys/os.h>
#include <sys/door.h>

class solaris_xdoor_kit : public xdoor_kit {
public:
	solaris_xdoor_kit();
	void	marshal_roll_back(MarshalStream &);
};

//
// Each exported instance of a IDL interface creates a new Door
// (file descriptor). All external reference counting is performed
// by the kernel.
//

//
// An Xdoor abstract base class based on a Solaris door transport
//
// Pure virtual functions defined in super class
//
// Operations not required for Solaris Xdoors.
//	- Reference counting is done by the kernel
//	- No special headers are currently used
//
class solaris_xdoor : public Xdoor {
	friend class sxdoor_table;
public:
	static solaris_xdoor_kit XK;

	solaris_xdoor(os::mutex_t *mp, handler *h);

	virtual ~solaris_xdoor();

	void	marshal_cancel();
	void	marshal_local(Environment *);

	// Inline implementations
	handler *get_user_handler() const;

	handler *extract_handler(handler_type t);
	void	extract_done(handler_type, handler *, bool unmarshalled);

	bool	reference_counting();

	// Implementations
	void		marshal(MarshalStream &, Environment *);
	void		unmarshal(Environment *);
	void		accept_release();
	void		marshal_roll_back(MarshalStream &);
	void		unmarshal_roll_back();

	//
	// Identify resources needed at xdoor level for request
	//
	sendstream	*reserve_resources(resources *resourcep);

	bool		client_unreferenced(handler_type t, uint_t marshcnt);

	// Is this a client or server xdoor?
	virtual bool local() = 0;

	// If want to return LOCAL_EXCEPTION there must a sys exception raised
	ExceptionStatus	request_oneway(invocation &);

	virtual void 	handle_possible_unreferenced() = 0;

	void    	lock();
	void    	unlock();
	bool		lock_held();
	os::mutex_t 	&get_lock();

protected:

	int		get_fd();
	void		set_fd(int fdesc);
	door_id_t 	get_id();
	void		set_id(door_id_t did);


	handler		*handler_;

	// solaris_xdoors contain a pointer to a lock rather than embedding
	// a lock into the class so that the lock is persistent across the
	// deletion of an xdoor.  This means the lock can be held across
	// calls to handle_possible_unreferenced, which sometimes results
	// in deleting the xdoor it is called on.  rxdoors do something
	// similar in the kernel.
	os::mutex_t	*lckp;

	// Condition variables can be per-xdoor since there's no
	// persistence issue.  The xdoor can't go away while someone's
	// waiting on its cv.
	os::condvar_t	cv;

	uint_t		umarshout;
	ushort_t	umarsh_ip;		// unmarshals in progress.
	door_id_t	id;
	int		fd;			// The solaris door

	enum { REVOKE_PENDING		= 0x1,
		REVOKED			= 0x2,
		UNREFERENCED		= 0x4,
		UNREF_RECEIVED		= 0x08,

		// Set by handle_possible_unreferenced, and
		// always cleared by accept_unreferenced or reject_unreferenced.
		UNREF_PENDING		= 0x10,

		// A marshal occurred while an unref task was pending.
		MARSHAL_DURING_UNREF	= 0x20
	};

	uint_t	flags;

	// Counts of number of times object has been marshalled by the
	// handler.
	uint_t	mc_handler;

	// The hold count is used to indicate how many pointers to the
	// xdoor are in use. It is incremented when the xdoor is looked up
	// and decremented when the xdoor is released, so a value of 0
	// indicates that it is ok to delete the xdoor.
	//
	// This member is protected by the sxdoor_table lock.
	//
	uint_t	hold_count;
private:

	// Disallow assignments or pass by value
	solaris_xdoor(const solaris_xdoor &);
	solaris_xdoor &operator = (const solaris_xdoor &);
};


//
// Server side implementation
//
class solaris_xdoor_server:
	public solaris_xdoor,
	public unref_task
{
public:
	static Xdoor *create(handler *, bool);
	solaris_xdoor_server(handler *h, bool unref_wanted);
	void	marshal_cancel();

	// send_reply - handles any pending exception.
	void	send_reply(service &);

	bool local();
	bool reference_counting();

	ExceptionStatus	request_twoway(invocation &);

	void	revoked();
	void	handle_possible_unreferenced();
	void	deliver_unreferenced();
	void	unreferenced_done();
	void	unreferenced_upcall();

	nodeid_t   	get_server_nodeid();

protected:
	virtual handler			*get_unref_handler();
	virtual unref_task_type_t	unref_task_type();

	// Doors invocation receive function
	static void object_server(void *, char *, size_t, door_desc_t *,
	    uint_t);

	// Convert from Solaris door arguments to service object
	void	prepare_receive(service &, Buf *, door_desc_t *, uint_t);
	void	prepare_reply(service &, door_arg_t &);

	void	incoming_prepare(Environment &e);
	void	incoming_done();

private:
	bool	door_is_unref();	// Check if door is really unref

	bool	unref_wanted;
	uint_t	invos_in_progress;

	// Disallow assignments or pass by value
	solaris_xdoor_server(const solaris_xdoor_server &);
	solaris_xdoor_server &operator = (const solaris_xdoor_server &);
};

//
// Client side implementation
//
class solaris_xdoor_client:
	public solaris_xdoor,
	public knewdel
{
	//
	// following friend class defines pthread_atfork handler functions.
	// They lock handlers when fork()/fork1() is called by user-land
	// process, to avoid fork-one safety problem.
	//
	friend class pthread_atfork_handler;
public:
	static solaris_xdoor *create(handler *h, int d);

	solaris_xdoor_client(handler *h, int d);

	void	marshal_cancel();
	bool	local();

	// Perform twoway invocation
	ExceptionStatus	request_twoway(invocation &);

	void	handle_possible_unreferenced();

	// returns true if invocation is in progress and false
	// when no invocation is taking place. This is useful in
	// blocking fork by user-land clients when invocation is
	// in progress.
	// Note: caller of this function is responsible to grab
	// invo_lck lock.
	//
	static	bool	invocation_in_progress();
	nodeid_t   	get_server_nodeid();

protected:
	// Convert invocation object to Solaris door args
	void	prepare_send(invocation &, door_arg_t &);

	// Convert door results back into invocation object
	void	prepare_return(invocation &, door_arg_t &, char *);
private:
	// Disallow assignments or pass by value
	solaris_xdoor_client(const solaris_xdoor_client &);
	solaris_xdoor_client &operator = (const solaris_xdoor_client &);

	// counts the number of invocation in progress. This count
	// is useful in blocking fork by user-land clients when
	// invocation is in progress.
	//
	static	uint_t invo_count;

	static	os::mutex_t	invo_lck;
	static	os::condvar_t	invo_cv;

};

#include <orb/xdoor/solaris_xdoor_in.h>

#endif	/* _SOLARIS_XDOOR_H */
