/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _XDOOR_KIT_H
#define	_XDOOR_KIT_H

#pragma ident	"@(#)xdoor_kit.h	1.20	08/05/20 SMI"


#include <orb/invo/common.h>
#include <orb/buffers/marshalstream.h>

// Forward declarations
class Xdoor;

typedef uint8_t xkit_id_t;

#ifdef _KERNEL_ORB

#include <orb/member/Node.h>

//
// Xdoor Kit Ids for various xdoors - reserves slots in the xdoor repository.
//
#define	STDXD_TO_CLIENT_XKID	0
#define	STDXD_TO_SERVER_XKID	1
#define	STDXD_FROM_SERVER_XKID	2
#define	SIMXD_TO_CLIENT_XKID	3
#define	SIMXD_TO_SERVER_XKID	4
#define	SIMXD_FROM_SERVER_XKID	5
#define	HXDOOR_XKID		6

// The number of xdoor kits we support.  This needs to be updated if a
// kit with higher id than HXDOOR_XKID is added.
#define	NUM_XDOOR_KITS (HXDOOR_XKID + 1)

#else	// _KERNEL_ORB

//
// Xdoor Kit Ids for various xdoors - reserves slots in the xdoor repository.
//
#define	SOLARIS_XKID	0

// The number of xdoor kits we support.  This needs to be updated if a
// kit with higher id than SOLARIS_XKID is added.
#define	NUM_XDOOR_KITS (SOLARIS_XKID + 1)

#endif	// _KERNEL_ORB

//
// Each xdoor type implements a kit object which specifies its type
// using an xkit_id_t identifier. This understands how to create a
// server xdoor and how to unmarshal xdoors of this type.
// When an xdoor is marshaled, its kit id is marshaled first followed
// by the handler specific marshal data.
// Note that this requires that the the kit be defined with the same
// kit id in all domains which wish to be able to marshal or unmarshal
// such references.  For that reason, we statically allocate kit ids using
// the identifiers defined above.
//
class xdoor_kit {
public:
#ifdef	_KERNEL_ORB

	virtual void marshal_roll_back(ID_node &, MarshalStream &) = 0;
	virtual Xdoor *unmarshal(ID_node &, MarshalStream &, Environment *) = 0;
	virtual void unmarshal_cancel(ID_node &, MarshalStream &) = 0;

#else	// _KERNEL_ORB

	virtual void marshal_roll_back(MarshalStream &) = 0;

#endif	// _KERNEL_ORB

	xkit_id_t	get_kid();
	uint_t		get_marshal_len();
	void		marshal(MarshalStream &sms);

protected:
	xdoor_kit(const xkit_id_t kid, uint32_t ml);

	virtual ~xdoor_kit();

private:
	// The kit id for this kit.
	xkit_id_t	kit_id;

	uint_t		marshal_length;

	// Disallow default constructor.
	xdoor_kit();

	// Disallow assignments and pass by value.
	xdoor_kit(const xdoor_kit &);
	xdoor_kit & operator = (xdoor_kit &);
};

//
// An xdoor repository is a data base of xdoors, kept by xdoor id.
// Xdoor types can be registered if they have reserved a slot in the
// defines above.
//
class xdoor_repository {
public:
	xdoor_repository() { }
	~xdoor_repository() { }

	// returns the only handler repository
	static xdoor_repository &the();

	void	add_kit(xdoor_kit *);
	void	remove_kit(xdoor_kit *);

	xdoor_kit *get_kit(xkit_id_t kid)
	{
		ASSERT(((int8_t)kid) >= 0 && kid < NUM_XDOOR_KITS);
		return (xdoor_kits[kid]);
	}

	uint_t	get_max_xdoor_len() { return (max_len_per_xdoor); }

private:
	// We maintain an array of xdoor kit pointers.
	// This is initialized to NULL by C++ (since the repository is
	// a static class). The constructors and destructors for each
	// handler kit add and remove the kits from this array using the
	// utility functions above.
	xdoor_kit *xdoor_kits[NUM_XDOOR_KITS];

	// Initialized to zero because the xdoor repository is static.
	uint_t	max_len_per_xdoor;

	// Disallow assignments and pass by value
	xdoor_repository(const xdoor_repository &);
	xdoor_repository & operator = (xdoor_repository &);
};

#include <orb/xdoor/xdoor_kit_in.h>

#endif	/* _XDOOR_KIT_H */
