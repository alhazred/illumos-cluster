/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RXDOOR_HEADER_H
#define	_RXDOOR_HEADER_H

#pragma ident	"@(#)rxdoor_header.h	1.30	08/07/17 SMI"

#include <orb/invo/common.h>
#include <orb/xdoor/rxdoor_defs.h>
#include <orb/buffers/marshalstream.h>
#include <sys/sol_version.h>
//
// This file defines all of the rxdoor level message headers
//

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#else
#undef	UNODE
#endif

//
// NOTE: Classes rxdoor_oneway_header and rxdoor_twoway header cannot
// contain virtual methods. The reason is that the code using them
// does not allocate memory for objects of these types, neither on
// stack nor on from the heap. Instead they are constructed out of a
// byte stream. So while we can map data members to consecutive bytes
// in the byte stream, pointers to vtbl are meaningless across
// nodes. Calling a virtual method on such a object the receiving node
// will only result in a segmentation violation.
//
class rxdoor_invo_header {
public:
	//
	// Data layout for message header
	//
#if defined(MARSHAL_DEBUG)
	uint32_t	marshal_debug;		// constant for validity check
						// must be first data member
#endif
	xdoor_id	xdid;			// target xdoor
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// This data member will store the id of the
	// physical or virtual cluster
	// The id of the physical cluster is always PHYSICAL_CLUSTER_ID
	//
	uint32_t	zone_cluster_id;
#endif
};

//
// rxdoor_oneway_header - message header for oneway invocation and
// checkpoint messages
//
class rxdoor_oneway_header : public rxdoor_invo_header {
public:
	// constructor to read in the header from the recstream
	rxdoor_oneway_header(recstream *);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	static void		make_header(uint_t, sendstream *, xdoor_id);
#else
	static void		make_header(sendstream *, xdoor_id);
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	xkit_id_t		get_kit_id();

private:
	// Prevent default constructor
	rxdoor_oneway_header();

	// Prevent assignments and pass by value
	rxdoor_oneway_header(const rxdoor_oneway_header &);
	rxdoor_oneway_header &operator = (rxdoor_oneway_header &);
};

//
// rxdoor_twoway_header - message header for twoway invocation request messages
//
class rxdoor_twoway_header : public rxdoor_invo_header {
public:
	// constructor to read in the header from the recstream
	rxdoor_twoway_header(recstream *);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	static void		make_header(uint_t, sendstream *, xdoor_id,
	    ushort_t);
#else
	static void		make_header(sendstream *, xdoor_id, ushort_t);
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	xkit_id_t		get_kit_id();

	uint16_t	slot;			// invo table slot
	uint16_t	svcid;			// Used by hxdoor for serviceid

private:
	// Prevent default constructor
	rxdoor_twoway_header();

	// Prevent assignments and pass by value
	rxdoor_twoway_header(const rxdoor_twoway_header &);
	rxdoor_twoway_header &operator = (rxdoor_twoway_header &);
};

//
// rxdoor_reply_header - message header for invocation reply messages
//
class rxdoor_reply_header {
public:
	//
	// The enumeration represents orb message bit flags
	// Multiple bit flags can be true for the one message.
	// The flags word must be cleared initially.
	//
	// The server exception flag tells the client that an exception
	// occurred on the server side. This informs any upper level
	// message processing layers, such as the HA hxdoor level, that no
	// higher message headers were marshalled.
	//
	enum message_flags {
		NONE		= 0,
		SIGNALED	= 0x08,		// signal occurred
		EXCEPTION	= 0x10,		// Server had exception.
		RETRY		= 0x20		// Server wants us to retry.
	};

	// constructor to read in the header from the recstream
	rxdoor_reply_header(recstream *);

	static void	make_header(sendstream *, ushort_t, uint_t);

	// Test for signal bit set
	static bool	is_signaled(uint_t flags);

	// Test for server side exception
	static bool	is_exception(uint_t flags);

	// Test for server side retry request
	static bool	is_retry(uint_t flags);

	//
	// Data layout for message header
	//
#if defined(MARSHAL_DEBUG)
	uint32_t	marshal_debug;		// constant for validity check
						// must be first data member
#endif
	uint8_t		flags;			// msg flags
	uint8_t		pad;			// alignment padding
	uint16_t	slot;			// invo table slot

private:
	// Prevent default constructor
	rxdoor_reply_header();

	// Prevent assignments and pass by value
	rxdoor_reply_header(const rxdoor_reply_header &);
	rxdoor_reply_header &operator = (rxdoor_reply_header &);
};

#include <orb/xdoor/rxdoor_header_in.h>

#endif	/* _RXDOOR_HEADER_H */
