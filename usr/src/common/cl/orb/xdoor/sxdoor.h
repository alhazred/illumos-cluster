/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SXDOOR_H
#define	_SXDOOR_H

#pragma ident	"@(#)sxdoor.h	1.10	08/05/20 SMI"

//
// This defines the classes that manage the xdoors
// in a user level process.
//

#include <door.h>
#include <sys/hashtable.h>
#include <orb/debug/haci_debug.h>

#if defined(HACI_DBGBUFS) && !defined(NO_SOLXDR_DBGBUF)
#define	SOLXDR_DBGBUF
#endif

#ifdef	SOLXDR_DBGBUF
extern dbg_print_buf	*solxdrbufp;
#define	SOLXDR_DBG(a) HACI_DEBUG(ENABLE_SDOOR_DBG, (*solxdrbufp), a)
#else
#define	SOLXDR_DBG(a)
#endif

class Xdoor;
class solaris_xdoor;

class sxdoor_table {
	//
	// following friend class defines pthread_atfork handler functions.
	// They lock handler's when fork()/fork1() is called by user-land
	// process, to avoid fork-one safety problem.
	//
	friend class pthread_atfork_handler;
public:
	static sxdoor_table	&the();
	static int		initialize();

	void		register_door(solaris_xdoor *, door_id_t, bool);
	solaris_xdoor	*get_and_hold_door(door_id_t);
	void		release_door(solaris_xdoor *);
	void		remove_door(solaris_xdoor *, door_id_t);

protected:
	void		lock();
	void		unlock();
	bool		lock_held();

private:
	static sxdoor_table			*the_sxdoor_tablep;
	hashtable_t<solaris_xdoor *, door_id_t>	doortab;
	os::mutex_t				lck;
};

class sxdoor_manager {
	//
	// following friend class defines pthread_atfork handler functions.
	// They lock handler's when fork()/fork1() is called by user-land
	// process, to avoid fork-one safety problem.
	//
	friend class pthread_atfork_handler;
public:
	static sxdoor_manager	&the();
	static int		initialize();

	void	translate_in(MarshalStream &, uint_t, door_desc_t *,
		    Environment *);
	void	translate_in_cancel(uint_t, door_desc_t *);
	void	translate_out(uint_t, MarshalStream &, Environment *);

protected:
	Xdoor	*unmarshal_door(door_id_t, int, Environment *);

	void	lock();
	void	unlock();
	bool	lock_held();

private:
	static sxdoor_manager	*the_sxdoor_managerp;
	os::mutex_t		lck;
};

#include <orb/xdoor/sxdoor_in.h>

#endif	/* _SXDOOR_H */
