/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _XDOOR_H
#define	_XDOOR_H

#pragma ident	"@(#)Xdoor.h	1.101	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/xdoor/xdoor_kit.h>
#include <orb/handler/handler.h>

// Forward declarations.
class sendstream;
class MarshalStream;
class resources;
class invocation;

#ifdef _KERNEL_ORB

//
// This class represents a translate_out which has been performed, but which
// has not yet been acknowledged. The reference counting subsystem acknowledges
// them.  The xdoor pointers are stored in the marshalstream portion.
//
class tr_out_info : public MarshalStream, public _DList::ListElem {
	friend class translate_mgr;
	friend class Xdoor_manager;

public:
	tr_out_info(uint_t num_xdoors, Region &r, Environment *e);
	~tr_out_info();

	void	ack_xdoors();	// Notify the xdoors of an ack.

#ifdef DEBUGGER_PRINT
	uint_t	mdb_get_xdoor_count();
#endif

private:
	uint_t	count;		// number of xdoors.

	// Disallow assignments and pass by value
	tr_out_info(const tr_out_info &);
	tr_out_info &operator = (tr_out_info &);
};

//
// Xdoor_manager - static methods only.
//
// The xdoor manager is knowledgeable about performing translations between
// universal xdoor names and internal xdoor names, creating xdoors as needed.
//
class Xdoor_manager {
public:

	//
	// given a list of indices, returns a  list of external
	// xdoor references.
	// Sets exceptions in Environment
	static tr_out_info *translate_out(ID_node &, uint_t, MarshalStream &,
	    Environment *);

	//
	// Rolls back the work done in translate_out. Called when we decide
	// not to send the references after all.
	static void translate_out_roll_back(ID_node &, MarshalStream &,
	    tr_out_info *);

	//
	// given a list of indices tells that xdoors that they
	// will not be sent after all.
	//
	static void translate_out_cancel(uint32_t hm, MarshalStream &);
	static void translate_out_cancel_stream(uint32_t hm, MarshalStream &);

	//
	// Given a list of external xdoor references it returns
	// a list of internal refs. It creates those references
	// that need creation. This means that it performs a search
	// to avoid duplicates.
	// Sets exceptions if any in Environment
	static void translate_in(ID_node &, uint32_t, MarshalStream &,
	    Environment *);

	//
	// Given a list of external xdoor references it completes
	// the reference counting protocol with the sender without
	// creating local xdoor references.
	static void translate_in_cancel(ID_node &, uint32_t hm,
	    MarshalStream &);

	//
	// Given a list of local xdoor numbers it rolls back the effect
	// of translate in on the marshal counts.  This should be called
	// whenever a translate_in has been performed, but it is decided
	// not to perform the handler's unmarshal for some reason, such as
	// an error.
	static void translate_in_roll_back(uint32_t hm, MarshalStream &);

	//
	// Given a list of local xdoors numbers it adjusts both marshal
	// and unmarshal counts for a local invocation.
	//
	static void translate_local(uint32_t, MarshalStream &, Environment *);

private:
	//
	// Given a count and a list of external xdoor references xdoor
	// ids it rolls back the marshaling on those xdoors.
	static void translate_out_roll_back_stream(ID_node &, uint32_t
	    hm, MarshalStream &);

	//
	// Given a list of external xdoor references it completes
	// the reference counting protocol with the sender without
	// creating local xdoor references.
	static void translate_in_cancel_stream(ID_node &, uint32_t hm,
	    MarshalStream &);
};
#endif // _KERNEL_ORB

//
// Common Xdoor interface.
// The interface is fat in the sense that it accomodates both
// client and server side calls. The calls are labeled with the
// client/server/both indication
//
class Xdoor {
public:
	// Note that these must have values 0 and 1 since they are used to
	// index into an array.
	enum handler_type { USER_HANDLER = 0, KERNEL_HANDLER = 1 };

	// client
	//
	// Starts a request on the xdoor. gets passed an invocation
	// structure in which parameters have already been
	// marshaled
	//
	virtual ExceptionStatus  request_oneway(invocation &) = 0;
	virtual ExceptionStatus  request_twoway(invocation &) = 0;

	//
	// client_unreferenced - works with client handlers.  This tells the
	// xdoor that the handler has no more references. Because of the lock
	// hierarchy, there is a window during which actions can occur, which
	// can change the need to perform an unreference.  The arg marshcnt
	// provides the number of marshals the handler has performed.
	// Returns false if the xdoor has passed up a new reference to the
	// handler as part of an unmarshal (meaning that the handler should
	// stay around), true if the handler has been detached from the xdoor.
	// In either case, the marshcnt arg is accounted for by the xdoor.
	//
	virtual bool    client_unreferenced(handler_type,
	    uint_t marshcnt) = 0;

	// server
	//
	// The xdoor is told to invalidate itself and accept no more
	// calls for service. After a revoked has been processed by an
	// xdoor further external calls cannot proceed.
	//
	virtual void    revoked();

	// server
	//
	// Used by the handler after it received an unreferenced call
	// from the xdoor. It tells the xdoor it should go away, and
	// the handler will not attempt to make use of it any more.
	//
	virtual void    accept_unreferenced();

	// server
	//
	// Used by the handler after it received an unreferenced call
	// from the xdoor. It tells the xdoor it should not go away
	// at this time.
	// The argument is the handler's marshal count.
	//
	virtual void    reject_unreferenced(uint_t);

	// server
	//
	// Used by the handler after it received an unreferenced call
	// from the xdoor. It tells the xdoor to enable unreferences
	// but do not call handle_possible_unreferenced at this time.
	//
	virtual void    clear_unreferenced();

	// both
	//
	// returns the handler associated with the xdoor and
	// ensures that no other handler can be extracted until
	// extract_done is called.
	//
	virtual handler *extract_handler(handler_type) = 0;

	// both
	//
	// notifies xdoor that handler extraction is finished.
	// unmarshalled indicates if this reference was unmarshalled
	// at the xdoor level (always true except when returning
	// name server references to user level).
	//
	virtual void	extract_done(handler_type, handler *,
	    bool unmarshalled) = 0;

	// both
	//
	// True if this xdoor has reference counting.
	//
	virtual bool    reference_counting() = 0;

#ifdef _KERNEL_ORB
	// both
	//
	// Marshal writes down a cluster-wide representation of
	// the xdoor. The first thing it does, though is write down
	// the type of xdoor.
	//
	virtual void	marshal(ID_node &, MarshalStream &, Environment *) = 0;

	//
	// The xdoor has been unmarshaled on the other side.
	//
	virtual void	marshal_received() = 0;

#else	// _KERNEL_ORB

	//
	// Marshal of user xdoors usually involves writing down a file
	// descriptor for a door.
	//
	virtual void	marshal(MarshalStream &, Environment *) = 0;
#endif	// _KERNEL_ORB


	// both
	//
	// Informs the xdoor that one of its last marshaling operations
	// was aborted.
	//
#ifdef _KERNEL_ORB
	virtual void    marshal_roll_back(ID_node &, MarshalStream &) = 0;
#else	// _KERNEL_ORB
	virtual void    marshal_roll_back(MarshalStream &) = 0;
#endif	// _KERNEL_ORB

	// both
	//
	// Informs the xdoor that it will not be marshaled after all.
	// Typically the xdoor reacts by reconciling its marshal count
	// with the handler.
	//
	virtual void    marshal_cancel() = 0;

#ifdef _KERNEL_ORB
	// both
	//
	// given the node from which a reference has arrived, the
	// xdoor is expected to extract from the marshal stream
	// whatever additional material may be in it. "isnew" indicates
	// whether the xdoor was created as part of that reference being
	// translated.
	//
	virtual void	unmarshal(ID_node &, MarshalStream &,
			bool, Environment *) = 0;
#endif

	// both
	//
	// Informs the xdoor that one of the unmarshaling operations
	// has to be aborted
	//
	virtual void    unmarshal_roll_back() = 0;

	// both
	//
	// Informs the xdoor that it has been passed as part of a local
	// invocation (user to kernel or kernel to user).  Both marshal
	// and unmarshal counts should be adjusted as appropriate.
	//
	virtual void	marshal_local(Environment *) = 0;

	//
	// Identify resources needed at xdoor level for request
	// This method is used for invocation messages and not reply messages.
	//
	virtual sendstream	*reserve_resources(resources *resourcep) = 0;

	//
	// We didn't use the resources we reserved. This is called
	// if we called reserve_resources but didn't call request.
	//
	virtual void release_resources(resources *);

	// Identify resources needed at xdoor level for invocation reply
	virtual sendstream	*reserve_reply_resources(resources *resourcep);

	//
	// send_reply - handles any pending exception, and transmits the
	// reply message after translating any xdoors.
	// Note that user level neither translates xdoors nor transmits
	// the reply.
	//
	virtual void		send_reply(service &);

	// For the client xdoors, this answers whether the server is dead.
	// The specific type of xdoor that wish to use this need to
	// implement it
	virtual bool		server_dead();

	// Return the nodeid of the server reference.
	virtual nodeid_t	get_server_nodeid() = 0;

protected:
	Xdoor()		{ };
	virtual ~Xdoor() { };

private:
	// Disallow assignments and pass by value
	Xdoor(const Xdoor &);
	Xdoor & operator = (Xdoor &);
};

#endif	/* _XDOOR_H */
