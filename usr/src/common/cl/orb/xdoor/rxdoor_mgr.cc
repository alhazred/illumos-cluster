//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rxdoor_mgr.cc	1.10	08/05/20 SMI"

#include <sys/os.h>
#ifdef _KERNEL
#include <sys/systm.h>
#endif
#include <orb/infrastructure/orb_conf.h>
#include <orb/xdoor/rxdoor_mgr.h>
#include <orb/member/members.h>
#include <orb/member/callback_membership.h>
#include <sys/list_def.h>

//
// Number of buckets to create for the rxdoor_node table used
// by the rxdoor_manager
//
uint_t rxdmgr_nbuckets = 65536;

//
// rxdoor_hash_table
//

rxdoor_hash_table::rxdoor_hash_table() :
	local_rx_node(orb_conf::current_id_node(), 2048, 4096)
{
	for (nodeid_t ndid = 1; ndid <= NODEID_MAX; ndid++) {
		ID_node nd(ndid, INCN_UNKNOWN);
		fixed_rx_nodes[ndid - 1] = new rxdoor_node(nd, 1, 16);
	}
}

rxdoor_hash_table::~rxdoor_hash_table()
{
	for (nodeid_t ndid = 1; ndid <= NODEID_MAX; ndid++) {
		delete fixed_rx_nodes[ndid - 1];
	}
}

//
// hash table size scales with physical memory (maxusers normally
// varies from 8 to 1024 and is set to number of megabytes of physical
// memory).
//
// static
uint_t
rxdoor_hash_table::default_num_hashlists()
{
	return (1 << (os::highbit((ulong_t)maxusers * 32) - 1)); //lint !e571
}

//
// check_idnode - "list callback" function to be used as the first
// parameter of DList::iterate(). Compares "node" member of the first
// parameter with the second argument. Returns false on a positive match.
// This is due to the way DList::iterate() is implemented. Note that the
// return value is opposite to what ID_node::match() returns.
//
bool
rxdoor_hash_table::check_idnode(rxdoor_node *rxnp, void *idnp)
{
	return (!ID_node::match(rxnp->get_node(), *((ID_node *)idnp)));
}

//
// get_rxdoor_node - Find and return an xdoor bucket for this rxdoor_id.
// See rxdoor.h for an explanation of the data structures here.
//
rxdoor_node *
rxdoor_hash_table::get_rxdoor_node(ID_node &idn)
{
	ASSERT(rxdoor_manager::the().table_lock.lock_held());

	DList<rxdoor_node>	&rnl = node_buckets[idn.ndid - 1];

	//
	// iterate returns the correct type via template cast.
	//
	return (rnl.iterate(rxdoor_hash_table::check_idnode,
	    (void *)&idn));
}

//
// create_rxdoor_node - create an xdoor bucket for this rxdoor_id.
// See rxdoor.h for an explanation of the data structures here.
// It is mandatory, that you already have searched and found that
// the rxdoor_node does not exist.
//
rxdoor_node *
rxdoor_hash_table::create_rxdoor_node(ID_node &idn)
{
	ASSERT(rxdoor_manager::the().table_lock.write_held());

	DList<rxdoor_node>	&rnl = node_buckets[idn.ndid - 1];
	rxdoor_node		*rnp;

	//
	// A PXFS client can have  100,000 active files,
	// each with an ordinary CORBA object and an xdoor.
	// So we need a reasonable number of buckets for that
	// many xdoors.
	//
	// XXX - lock contention is a performance problem
	// and locks are small. So this should be changed to
	// one lock per list and one list per bucket.
	//
	rnp = new rxdoor_node(idn, rxdmgr_nbuckets, (rxdmgr_nbuckets * 2));
	rnl.prepend(rnp);
	return (rnp);
}

//
// rxdoor_manager
//

rxdoor_manager 		*rxdoor_manager::the_rxdoor_manager = NULL;

// constructor
rxdoor_manager::rxdoor_manager() : max_xdid(XDOOR_MAXFIXED)
{
}

int
rxdoor_manager::initialize()
{
	ASSERT(the_rxdoor_manager == NULL);
	the_rxdoor_manager = new rxdoor_manager();
	if (the_rxdoor_manager == NULL) {
		return (ENOMEM);
	}
	return (0);
}

void
rxdoor_manager::shutdown()
{
	if (the_rxdoor_manager != NULL) {
		delete the_rxdoor_manager;
		the_rxdoor_manager = NULL;
	}
}

// update the local incarnation number
void
rxdoor_manager::update_current()
{
	table.local_rx_node.set_node(orb_conf::current_id_node());
}

//
// Called from step 3 of CMM reconfiguration to cleanup reference counting
// information associated with nodes which have died.
// This method keeps track of the last membership which was passed in and
// passes both the new and old membership to the rxdoor_node for the server
// xdoors on this node, so that it can notify the servers of any node deaths.
//
void
rxdoor_manager::cleanup_foreignrefs(callback_membership *curr_mbrship,
    callback_membership *new_mbrship)
{
	table_lock.wrlock();
	table.local_rx_node.cleanup_foreignrefs(curr_mbrship, new_mbrship);
	table_lock.unlock();
}

// Check all rxdoor_nodes and see if we can delete them.
void
rxdoor_manager::cleanup_resources()
{
	rxdoor_node	*rnp;

	table_lock.wrlock();
	for (nodeid_t ndid = 1; ndid <= NODEID_MAX; ndid++) {
		DList<rxdoor_node> &rnl = table.node_buckets[ndid - 1];
		rnl.atfirst();
		while ((rnp = rnl.get_current()) != NULL) {
			// Advance iterator before doing erase
			rnl.advance();
			if (rnp->is_empty()) {
				(void) rnl.erase(rnp);
				delete rnp;
			}
		}
	}
	table_lock.unlock();
}

//
// Lookup an rxdoor whose server is on this node. See comment in lookup_rxdoor
// for locking info.
//
rxdoor *
rxdoor_manager::lookup_local_rxdoor(xdoor_id xdid, rxdoor_bucket **xbpp)
{
	ID_node nd = orb_conf::current_id_node();

	if (xdid < XDOOR_MAXFIXED) {
		nd.incn = INCN_UNKNOWN;
	}
	return (lookup_rxdoor(nd, xdid, xbpp));
}

//
// Look up the rxdoor with the specified node id and xdoor id.
// Returns with the xdoor locked even if it is not found. Also returns the
// rxdoor_bucket pointer if requested (if xbpp is non NULL). The caller
// should perform an unlock when it is done with the rxdoor. The unlock can
// be done on the bucket or the xdoor (since they share a lock), but if caller
// may have caused the xdoor to be deleted, then it must use the bucket to do
// the unlock. Note that if the caller cannot guarantee that the lookup will
// find the xdoor, then it must pass in a non-null xbpp, so that it can unlock.
//
// If the rxdoor is local or fixed, then we don't need to get the table_lock,
// since they are allocated once and never deleted.
// If the rxdoor is associated with a specific incarnation of a remote node,
// then we need to lock the node_hash table to look up its rxdoor_node.  We can
// safely unlock the node_hash table once we have acquired a bucket lock in the
// rxdoor_node.
//
rxdoor *
rxdoor_manager::lookup_rxdoor(ID_node &node, xdoor_id xdid,
    rxdoor_bucket **xbpp)
{
	bool locked = false;
	rxdoor_node *rnp;

	if (node.incn == INCN_UNKNOWN) {
		// Optimize for fixed xdoor case.
		rnp = table.fixed_rx_nodes[(uint_t)node.ndid - 1];
	} else if (orb_conf::is_local_incn(node)) {
		// Optimize for local case.
		rnp = &table.local_rx_node;
	} else {
		locked = true;
		//
		// An rxdoor_node lives for a very
		// long time after being created. So optimize for the
		// common case where the rxdoor_node already exists.
		// A performance study showed that serious lock contention
		// would otherwise exist.
		//
		table_lock.rdlock();
		rnp = table.get_rxdoor_node(node);
		if (rnp == NULL) {
			table_lock.unlock();
			table_lock.wrlock();
			rnp = table.get_rxdoor_node(node);
			if (rnp == NULL) {
				rnp = table.create_rxdoor_node(node);
				ASSERT(rnp != NULL);
			}
		}
	}

	rxdoor_bucket *xbp = rnp->bucket_hash(xdid);

	if (xbpp != NULL) {
		*xbpp = xbp;
	}
	// Lock the bucket, caller needs to unlock
	xbp->lock();

	if (locked) {
		table_lock.unlock();
	}

	rxdoor *xdp = xbp->find(xdid);
	//
	// If the xdp is NULL, caller needs a xbpp to unlock. So the
	// caller should not have passed in a NULL value, and we would
	// have assigned xbp to xbpp.
	//
	ASSERT((xbpp != NULL) || (xdp != NULL));

	return (xdp);
}

//
// register_rxdoor
//
// Assign a unique xdoor_id to the new xdoor and store it in the
// table. Algorithm for finding uniqe xdoor_id: When an object
// reference is marshalled the first time, the handler creates a
// server xdoor. At this time, it needs to allocate a cluster-wide
// unique id to the new xdoor. The rxdoor_manager keeps track of the
// last id it allocated on this node in max_xdid variable. Whenever
// this node receives references to objects on other nodes, it stores
// these client xdoors in the same rxdoor_hash_table data
// structure. Whenever a node has to allocate a new xdoor id, it
// searches the table to see if max_xdid is being used. If it is then
// it increments max_xdid, repeats the search util it finds that no
// other xdoor is using this value and then assigns it to the new
// xdoor.
//
void
rxdoor_manager::register_rxdoor(rxdoor *xdp)
{
	rxdoor_id rxdid;

	rxdid.node = orb_conf::current_id_node();

	do {
		max_lock.lock();
		//
		// 0-XDOOR_MAXFIXED are reserved for fixed xdoor ids.
		//
		if (max_xdid == XDOOR_MAXDID) {
			max_xdid = XDOOR_MAXFIXED;
		}
		rxdid.xdid = max_xdid++;
		max_lock.unlock();
	} while (register_rxdoor(xdp, rxdid) == INVALID_XDOOR_ID);
}

xdoor_id
rxdoor_manager::register_rxdoor(rxdoor *xdp, rxdoor_id req_id)
{
	rxdoor_bucket *xbp;

	if (lookup_rxdoor(req_id.node, req_id.xdid, &xbp) != NULL) {
		// It's already taken
		xbp->unlock();
		return (INVALID_XDOOR_ID);
	}
	xdp->server_xdoor = req_id.xdid;
	xbp->store(xdp);
	xbp->unlock();
	return (req_id.xdid);
}

//
// release_rxdoor - release a registered rxdoor.
//
void
rxdoor_manager::release_rxdoor(rxdoor *xdp)
{
	rxdoor_bucket *xbp = xdp->get_bucket();
	ASSERT(xbp->lock_held());
	xbp->erase(xdp);
	delete xdp;
}
