/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  translate_mgr_in.h
 *
 */

#ifndef _TRANSLATE_MGR_IN_H
#define	_TRANSLATE_MGR_IN_H

#pragma ident	"@(#)translate_mgr_in.h	1.16	08/05/20 SMI"

#include <orb/refs/refcount.h>

inline translate_mgr &
translate_mgr::the()
{
	return (*the_translate_mgr);
}

inline void
translate_mgr::done_tr_in(ID_node &nd, uint64_t translate_info)
{
#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB) && !defined(_KERNEL)
	//
	// If the fault point is triggered, this function prevents
	// sending immediate acknowledgement for xdoors
	// received. Fault function SEND_HELD_TRINFO_ACK will send
	// this ack later.
	//
	if (fault_triggered(FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK, NULL, NULL)) {
		os::printf("Fault 0x%x: done_tr_in: Delaying ack %llx\n",
		    FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK, translate_info);
		FaultFunctions::dont_ack_trinfo = translate_info;
		FaultFunctions::dont_ack_node = nd;
		InvoTriggers::clear(FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK);
	} else {
		refcount::the().add_translate_ack(nd, translate_info);
	}

#else
	refcount::the().add_translate_ack(nd, translate_info);
#endif

}

//
// done_tr_out
// This indicates that the caller has finished marshaling its xdoors. We drop
// the read lock on the translation system.
// When all read locks are dropped, then the write lock may be granted,
// allowing a reconfiguration to be performed.
//
inline void
translate_mgr::done_tr_out(ID_node &nd)
{
	// Finished with lock held in begin_tr_out
	tr_nodes[nd.ndid].rwlck.unlock();
}

#endif	/* _TRANSLATE_MGR_IN_H */
