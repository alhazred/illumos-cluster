//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.

//
// This defines the methods for the classes that manage the xdoors
// in a user level process.
//

#pragma ident   "@(#)sxdoor.cc 1.10     08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <orb/xdoor/solaris_xdoor.h>
#include <orb/xdoor/sxdoor.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>


#ifdef SOLXDR_DBGBUF
dbg_print_buf	*solxdrbufp = NULL;
#endif

sxdoor_table	*sxdoor_table::the_sxdoor_tablep = NULL;

sxdoor_manager	*sxdoor_manager::the_sxdoor_managerp = NULL;

//
// sxdoor_table methods
//

//
// initialize - create the sxdoor_table object for the process.
//
// Currently, the objects created here are not destroyed
// while the process exists. One justification is that we do not
// know when a Solaris Door might deliver unref and trigger work.
//
// If the orb is restarted, the system
// will reuse the existing data structures.
//
// static
int
sxdoor_table::initialize()
{
	if (sxdoor_table::the_sxdoor_tablep == NULL) {
		sxdoor_table::the_sxdoor_tablep = new sxdoor_table;
		if (sxdoor_table::the_sxdoor_tablep == NULL) {
			return (ENOMEM);
		}
	}

#ifdef SOLXDR_DBGBUF
	if (solxdrbufp == NULL) {
		// Create a debug trace buffer
		solxdrbufp = new dbg_print_buf(4096);
		if (solxdrbufp == NULL) {
			return (ENOMEM);
		}
	}
#endif
	return (0);
}

//
// Add a new door/xdoor to the table.
//
void
sxdoor_table::register_door(solaris_xdoor *xdp, door_id_t id, bool hold)
{
	lock();
	ASSERT(doortab.lookup(id) == NULL);
	doortab.add(xdp, id);
	ASSERT(xdp->hold_count == 0);
	if (hold)
		xdp->hold_count = 1;
	unlock();
}

//
// Lookup door and keep a hold on it (preventing it from going away
// until release_door is called).
//
solaris_xdoor *
sxdoor_table::get_and_hold_door(door_id_t id)
{
	solaris_xdoor *xdp;

	lock();
	if ((xdp = doortab.lookup(id)) != NULL) {
		xdp->hold_count++;
		ASSERT(xdp->hold_count != 0);
	}
	unlock();
	return (xdp);
}

//
// Release hold on door.
//
void
sxdoor_table::release_door(solaris_xdoor *xdp)
{
	lock();
	ASSERT(xdp->hold_count > 0);
	xdp->hold_count--;
	unlock();
}

//
// Remove door from table if hold_count is 0.  If not, someone else is
// unmarshalling a reference to the door, so the removal should wait.
//
void
sxdoor_table::remove_door(solaris_xdoor *xdp, door_id_t id)
{
	bool freed = false;

	lock();
	ASSERT(doortab.lookup(id) == xdp);
	if (xdp->hold_count == 0) {
		freed = true;
		SOLXDR_DBG(("removing xdoor %p\n", xdp));
		(void) doortab.remove(id);
	}
	unlock();
	if (freed) {
		xdp->accept_release();
		SOLXDR_DBG(("removed xdoor %p\n", xdp));
	}
}

//
// sxdoor_manager methods
//

//
// initialize - create the sxdoor_manager object for the process.
//
// Currently, the object created here is not destroyed
// while the process exists. One justification is that we do not
// know when a Solaris Door might deliver unref and trigger work.
//
// If the orb is restarted, the system
// will reuse the existing data structure.
//
// static
int
sxdoor_manager::initialize()
{
	if (sxdoor_manager::the_sxdoor_managerp == NULL) {
		sxdoor_manager::the_sxdoor_managerp = new sxdoor_manager;
		if (sxdoor_manager::the_sxdoor_managerp == NULL) {
			return (ENOMEM);
		}
	}
	return (0);
}

//
// Translate incoming door descriptors to local representations.
// We could be passed the same door many times but a new file
// descriptor would be used each time. We use a match based on
// the door_id_t instead.
//
Xdoor *
sxdoor_manager::unmarshal_door(door_id_t id, int fd, Environment *e)
{
	solaris_xdoor *xdp;

	lock();
	xdp = sxdoor_table::the().get_and_hold_door(id);
	if (xdp != NULL) {
		// Found it. Use the original door descriptor
		// associated with this object.
		// We don't need the incoming descriptor anymore
		//
		SOLXDR_DBG(("found xdoor %p", xdp));
	} else {
		// Didn't find it (either not there or about to be
		// deleted).  Create a new one.  Returns with door
		// registered and held in table.
		//
		int new_desc = fd;
#ifndef _LP64
		// bug 4266886 - Choose a fd above 255 so that libc has
		// enough fds available.
		//
		new_desc = fcntl(fd, F_DUPFD, 256);
		if (new_desc < 0) {
			//
			// SCMSGS
			// @explanation
			// A user level process is unmarshalling a door
			// descriptor and creating a new door. The specified
			// operation on the fcntl operation fails. The "fcntl"
			// man page describes possible error codes.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: sxdoor: op %d fcntl failed: %s",
			    F_DUPFD, strerror(errno));
		}
#endif // !_LP64

		xdp = solaris_xdoor_client::create(NULL, new_desc);
		SOLXDR_DBG(("creating xdoor %p", xdp));

		// Set close-on-exec flag
		int ret = fcntl(new_desc, F_SETFD, FD_CLOEXEC);
		if (ret == -1) {
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: sxdoor: op %d fcntl failed: %s",
			    F_SETFD, strerror(errno));
		}
	}
	unlock();
	xdp->unmarshal(e);
	sxdoor_table::the().release_door(xdp);

	//
	// Close the incoming descriptor; It was either found in the table or
	// dup'd to be > 255
	//
	(void) close(fd);

	return (xdp);
}

//
// Converts incoming descriptors to xdoors, creating xdoors or closing
// descriptors as needed.
// Expects an empty Marshalstream (usually XdoorTab) and leaves it rewound
//
void
sxdoor_manager::translate_in(MarshalStream &wms, uint_t ndoors,
    door_desc_t *dptr, Environment *e)
{
	uint_t i;

	ASSERT(wms.empty());

	// Set allocsize for Marshalstream to size of buffer contents
	wms.set_allocsize(ndoors * (uint_t)sizeof (void *));

	//
	// We do not set the Environment in the recstream constructor
	// We set it here (which is a hack). Needed as we allocate memory
	// when we do a put_pointer
	//
	wms.set_env(e);

	for (i = 0; i < ndoors; i++) {
		int		d;
		door_id_t	did;
		Xdoor		*xdp;

		d = dptr->d_data.d_desc.d_descriptor;
		did = dptr->d_data.d_desc.d_id;
		xdp = unmarshal_door(did, d, e);
		wms.put_pointer(xdp);
		dptr++;
	}
	wms.prepare();	// rewind the Marshalstream
}

// For cases where we do not do a translate_in on the server during a door_call
void
sxdoor_manager::translate_in_cancel(uint_t ndoors, door_desc_t *dptr)
{
	uint_t i;

	for (i = 0; i < ndoors; i++) {
		(void) close(dptr[i].d_data.d_desc.d_descriptor);
	}
}

void
sxdoor_manager::translate_out(uint_t ndoors, MarshalStream &xdoors,
    Environment *e)
{
	uint_t i;

	// Create a new MarshalStream to output the result of translate_out
	MarshalStream newdoors(ndoors * (uint_t)sizeof (door_desc_t), e);

	xdoors.prepare();	// rewind input marshalstream
	for (i = 0; i < ndoors; i++) {
		Xdoor *xdp;

		ASSERT(!e->exception());
		xdp = (Xdoor *)xdoors.get_pointer();
		xdp->marshal(newdoors, e);
		if (e->exception())
			break;
	}

#ifdef DEBUG
	if (e->exception()) {
		os::warning("Need to handle exceptions in translate_out");
		e->exception()->print_exception("Exception in translate_out");

		// XXX uncomment this if marshal can raise an exception
		// XXX Need to implement them too
		// translate_out_roll_back_stream(i, newdoors, false);
		// newdoors.dispose();
		// translate_out_cancel_stream(ndoors - (i + 1), xdoors);
	}
#endif
	// Transfer newdoors to Marshalstream xdoors
	xdoors.dispose();
	xdoors.useregion(newdoors.region()); // Leaves cursor at head of stream
}
