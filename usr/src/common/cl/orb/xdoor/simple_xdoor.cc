//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)simple_xdoor.cc	1.123	08/07/31 SMI"


#include <orb/xdoor/simple_xdoor.h>
#include <orb/handler/handler.h>
#include <orb/member/members.h>
#include <orb/fault/fault_injection.h>
#include <orb/msg/orb_msg.h>
#include <orb/infrastructure/orb_conf.h>

const char	*simple_xdoor_server::xdoor_type_string = "sim_s";
const char	*simple_xdoor_client::xdoor_type_string = "sim_c";


simple_xdoor_to_client_kit simple_xdoor_client::to_client_kit;
simple_xdoor_to_server_kit simple_xdoor_client::to_server_kit;
simple_xdoor_from_server_kit simple_xdoor_server::from_server_kit;

rxdoor *
simple_xdoor_to_client_kit::create(rxdoor_descriptor &rxdid)
{
	return (simple_xdoor_client::create(rxdid));
}

rxdoor *
simple_xdoor_to_server_kit::create(rxdoor_descriptor &)
{
	ASSERT(0);
	return (NULL);
}

rxdoor *
simple_xdoor_from_server_kit::create(rxdoor_descriptor &rxdid)
{
	return (simple_xdoor_client::create(rxdid));
}

//
// Methods common to simple_xdoor_server and simple_xdoor_client
//

//
// unmarshal_roll_back - synchronize the marshal count,
//			and check whether still need the xdoor.
//	This routine is common for simple_xdoor_client and _server
//
void
simple_xdoor::unmarshal_roll_back()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ASSERT(umarshout > 0);
	umarshout--;
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%s(%d.%3d:%d): unmarshal_roll_back uo=%d: ",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor,
	    umarshout));
	if (umarshout == 0) {
		handle_possible_unreferenced();
	}
	lckp->unlock();
}

bool
simple_xdoor::reference_counting()
{
	return (false);
}

void
simple_xdoor::marshal_received()
{
}

void
simple_xdoor::marshal_roll_back(ID_node &, MarshalStream &)
{
}

void
simple_xdoor::marshal_cancel()
{
}

void
simple_xdoor::cleanup_foreignrefs(const nodeset &)
{
	// do nothing. Simple xdoor does not keep track of any foreign refs

	// We could be called because rxdoor hash table stores simple
	// xdoors which are in non-fixed locations and calls this
	// unconditionally
}

//
// SERVER
//

bool
simple_xdoor_server::local()
{
	return (true);
}

//
// revoked -
// The user is revoking the object.
// All future requests on this xdoor are rejected.
// The xdoor remains valid.
// This operation can only be done once.
//
// A simple xdoor server is immortal, and so is its lock. Thus
// no need to worry about its lock going away.
//
void
simple_xdoor_server::revoked()
{
	lock();
	ORB_DBPRINTF(ORB_TRACE_OBJEND, ("%s(%d.%3d:%d): revoke: ",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));
	ASSERT(!(flags & (REVOKED | REVOKE_PENDING)));
	flags |= REVOKE_PENDING;

	// We can finish revoke when no invocations are in in progress.
	while (services != 0) {
		revoke_cv.wait(&get_lock());
	}

	flags = (flags | REVOKED) & ~REVOKE_PENDING;

	// After this point the server handler can no longer be referenced
	// because it may not exist.
	ASSERT(get_server_handler() != NULL);
	set_server_handler(NULL);
	handle_possible_unreferenced();
	unlock();
}

// We don't keep track of marshal counts.
void
simple_xdoor_server::dec_marshcount(uint_t)
{
}


//
// server simple xdoors NEVER become unreferenced.
//
void
simple_xdoor_server::handle_possible_unreferenced()
{
	ASSERT(lock_held());
}

//
// reserve_resources - Identify resources needed at xdoor level for request.
// This supports "local" U->K and K->U invocations.
// No xdoor header is needed for local communications.
// Returns a send stream pointer, which can be NULL in case of error.
// This method is used for invocation messages and not reply messages.
//
sendstream *
simple_xdoor_server::reserve_resources(resources *resourcep)
{
	resourcep->set_nodeid(orb_conf::node_number());
	return (orb_msg::reserve_resources(resourcep));
}

//
// reserve_reply_resources - Identify resources needed for invocation reply.
// The server rxdoor does not know where the reply should go.
// The upper layer must specify the destination node.
//
sendstream *
simple_xdoor_server::reserve_reply_resources(resources *resourcep)
{
	// Obtain destination node from resources object
	ID_node		*nodep = resourcep->get_nodep();

	if (nodep->ndid != orb_conf::node_number()) {
		// Only remote invocation requires msg header
		resourcep->add_send_header(sizeof (rxdoor_reply_header));
	}

	// Have the lower protocol layers specify its requirements
	return (orb_msg::reserve_resources(resourcep));
}

//
// marshal - loads the server xdoor into the stream
//		provided that the xdoor has not been revoked.
//
void
simple_xdoor_server::marshal(ID_node &node, MarshalStream &wms, Environment *e)
{
	lock();
	//
	// Only marshal valid xdoors that have not been revoked
	//
	if (flags & (REVOKED | REVOKE_PENDING)) {
		ORB_DBPRINTF(ORB_TRACE_MARSHAL,
		    ("%s(%d.%3d:%d): marshal failed\n",
		    xdoor_type(), get_server_node().ndid,
		    (get_server_node().incn % 1000), server_xdoor));
		e->system_exception(CORBA::BAD_PARAM(0,
			CORBA::COMPLETED_MAYBE));
		unlock();
		return;
	}

	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%s(%d.%3d:%d): marshal to %d\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor, node.ndid));

	//	Marshal the xdoor
	get_kit().marshal_xdoor(wms, this);

	unlock();
}

//
// unmarshal - rejects attempts to unmarshal revoked server xdoor.
//
void
simple_xdoor_server::unmarshal(ID_node &, MarshalStream &, bool,
	Environment *e)
{
	ASSERT(lock_held());
	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%s(%d.%3d:%d): unmarshal\n",
	    xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor));

	//	Fail the unmarshal for a revoked xdoor.
	if (flags & (REVOKED | REVOKE_PENDING)) {
		e->system_exception(CORBA::BAD_PARAM(0,
				CORBA::COMPLETED_MAYBE));
	} else {
		umarshout++;
	}
}

//
// marshal_local - an invocation is occurring on the local machine.
//
void
simple_xdoor_server::marshal_local(Environment *e)
{
	lock();
	// always increment unmarshal count
	// in marshal_local_rollback this can be recovered easier
	umarshout++;
	// Raise exception if marshal for a revoked xdoor.
	if (flags & (REVOKED | REVOKE_PENDING)) {
		e->system_exception(CORBA::BAD_PARAM(0,
				CORBA::COMPLETED_MAYBE));
	}
	unlock();
}

//
// incoming_prepare - determine whether this xdoor will accept this invocation.
//
void
simple_xdoor_server::incoming_prepare(Environment *e)
{
	ASSERT(lock_held());
	// Validate that the xdoor is accepting invocations
	if (flags & (REVOKED | REVOKE_PENDING)) {
		e->system_exception(CORBA::INV_OBJREF(ECANCELED,
			CORBA::COMPLETED_NO));
	} else {
		services++;
	}
}

void
simple_xdoor_server::incoming_done()
{
	os::mutex_t	*lckp = &get_lock();

	lckp->lock();
	ASSERT(services > 0);
	if (--services == 0) {
		if (flags & REVOKE_PENDING) {
			revoke_cv.broadcast();
		} else {
			handle_possible_unreferenced();
		}
	}
	lckp->unlock();
}

//
// create - new xdoor, let the underlying software choose the xdid.
//
Xdoor *
simple_xdoor_server::create(handler *h)
{
	simple_xdoor_server *xdp = new simple_xdoor_server(h);

	rxdoor_manager::the().register_rxdoor(xdp);

	return (xdp);
}

//
// create - new xdoor using the specified the xdoor identification
//
Xdoor *
simple_xdoor_server::create(handler *h, xdoor_id new_xdid)
{
	rxdoor_id rxdid(orb_conf::node_number(), INCN_UNKNOWN, new_xdid);
	simple_xdoor_server *xdp;

	xdp = new simple_xdoor_server(h);
	if (rxdoor_manager::the().register_rxdoor(xdp, rxdid) ==
	    INVALID_XDOOR_ID) {
		delete xdp;
#ifdef DEBUG
		//
		// SCMSGS
		// @explanation
		// The system could not allocate a simple xdoor server. This
		// can happen when the xdoor number is already in use. This
		// message is only possible on debug systems.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Failed to allocate simple xdoor server %d",
		    new_xdid);
#endif
		return (NULL);
	}
	return (xdp);
}

const char *
simple_xdoor_server::xdoor_type()
{
	return (simple_xdoor_server::xdoor_type_string);
}

rxdoor_kit &
simple_xdoor_server::get_kit()
{
	return ((rxdoor_kit &)from_server_kit);
}

//
// CLIENT
//

// We don't keep track of marshal counts.
void
simple_xdoor_client::dec_marshcount(uint_t)
{
}

//
// handle_possible_unreferenced - called when a client handler releases
// all its references, or when an unmarshal fails and unmarshal_roll_back is
// called.  It checks to see if conditions are right for the xdoor to be
// released.
//
void
simple_xdoor_client::handle_possible_unreferenced()
{
	ASSERT(lock_held());

	if (umarshout == 0 && handlers[0] == NULL && handlers[1] == NULL &&
	    foreignrefs == 0) {
		//
		// We have no handlers and no outstanding unmarshals
		// Release xdoor now.
		//
		ORB_DBPRINTF(ORB_TRACE_OBJEND,
		    ("%s(%d.%3d:%d): releasing %p\n",
		    xdoor_type(), get_server_node().ndid,
		    (get_server_node().incn % 1000), server_xdoor, this));
		rxdoor_manager::the().release_rxdoor(this);
	}
}

//
// reserve_resources - Identify resources needed at xdoor level for request.
// Returns a send stream pointer, which can be NULL in case of error.
// This method is used for invocation messages and not reply messages.
//
sendstream *
simple_xdoor_client::reserve_resources(resources *resourcep)
{
	if (resourcep->get_invo_mode().is_oneway()) {
		resourcep->add_send_header(sizeof (rxdoor_oneway_header));
	} else {
		// remote invocation
		resourcep->add_send_header(sizeof (rxdoor_twoway_header));
	}
	resourcep->set_node(get_server_node());

	// Have the lower protocol layers specify its requirements
	return (orb_msg::reserve_resources(resourcep));
}

//
// marshal - client xdoor is loaded into the stream.
//
void
simple_xdoor_client::marshal(ID_node &node, MarshalStream & wms, Environment *)
{
	ORB_DBPRINTF(ORB_TRACE_MARSHAL,
	    ("%p %s(%d.%3d:%d): marshal to %d\n",
	    this, xdoor_type(), get_server_node().ndid,
	    (get_server_node().incn % 1000), server_xdoor, node.ndid));

	lock();
	foreignrefs++;
	unlock();

	//	Marshal the xdoor
	get_kit().marshal_xdoor(wms, this);
}

//
// unmarshal - synchronize the unmarshal count.
//
void
simple_xdoor_client::unmarshal(ID_node &, MarshalStream &, bool,
				Environment *)
{
	ASSERT(lock_held());
	umarshout++;
}

//
// marshal_local - an invocation is occurring on the local machine.
//
void
simple_xdoor_client::marshal_local(Environment *)
{
	// increment unmarshal count to match handler
	lock();
	umarshout++;
	unlock();
}

void
simple_xdoor_client::marshal_received()
{
	//
	// handle_possible_unreferenced() may delete the xdoor so must
	// use get a pointer to the lock.
	//

	os::mutex_t	*lckp = &get_lock();

	lckp->lock();

	ASSERT(foreignrefs > 0);
	foreignrefs--;
	if (foreignrefs == 0) {
		handle_possible_unreferenced();
	}
	lckp->unlock();
}

//
// create -  This one for the kit structure. It does very little
//
rxdoor *
simple_xdoor_client::create(rxdoor_descriptor &rxdid)
{
	return (new simple_xdoor_client((rxdoor_id &) rxdid));
}

//
// create - This one is used to create new ad_hoc xdoor clients.
//
Xdoor *
simple_xdoor_client::create(nodeid_t node, xdoor_id new_xdid, handler *handlerp)
{
	rxdoor_id rxdid(node, INCN_UNKNOWN, new_xdid);
	simple_xdoor_client *xdp;

	xdp = new simple_xdoor_client(rxdid);

	if (rxdoor_manager::the().register_rxdoor(xdp, rxdid) ==
	    INVALID_XDOOR_ID) {
		delete xdp;
#ifdef DEBUG
		//
		// SCMSGS
		// @explanation
		// The system could not allocate a simple xdoor client. This
		// can happen when the xdoor number is already in use. This
		// message is only possible on debug systems.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "clcomm: Failed to allocate simple xdoor client %d",
		    new_xdid);
#endif
		return (NULL);
	}
	// Set the client handler
	ASSERT(handlerp != NULL);
	xdp->handlers[Xdoor::KERNEL_HANDLER] = handlerp;
	xdp->client_handler_index = Xdoor::KERNEL_HANDLER;

	ORB_DBPRINTF(ORB_TRACE_MARSHAL, ("%p %s(%d.%3d:%d): created hdlr=%p\n",
	    xdp, xdp->xdoor_type(), xdp->get_server_node().ndid,
	    (xdp->get_server_node().incn % 1000), xdp->server_xdoor,
	    handlerp));

	return (xdp);
}

bool
simple_xdoor_client::local()
{
	return (false);
}

const char *
simple_xdoor_client::xdoor_type()
{
	return (simple_xdoor_client::xdoor_type_string);
}

rxdoor_kit &
simple_xdoor_client::get_kit()
{
	return ((rxdoor_kit &)to_client_kit);
}
