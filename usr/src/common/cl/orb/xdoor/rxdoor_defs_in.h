/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  rxdoor_defs_in.h
 *
 */

#ifndef _RXDOOR_DEFS_IN_H
#define	_RXDOOR_DEFS_IN_H

#pragma ident	"@(#)rxdoor_defs_in.h	1.4	08/05/20 SMI"

inline
rxdoor_descriptor::rxdoor_descriptor()
	: xdid(INVALID_XDOOR_ID)
{
}

inline
rxdoor_descriptor::rxdoor_descriptor(xdoor_id xdoor)
	: xdid(xdoor)
{
}

inline uint_t
rxdoor_descriptor::_sizeof_marshal()
{
#ifdef MARSHAL_DEBUG
	//
	// 1 uint32_t for marshal type +
	// 1 uint32_t for xdoor_id
	//
	return (2 * (uint_t)sizeof (uint32_t));
#else
	// 1 uint32_t for xdoor_id
	return ((uint_t)sizeof (uint32_t));
#endif
}

inline
rxdoor_ref::rxdoor_ref() : xdid(INVALID_XDOOR_ID), serviceid(0), is_reg(false)
{
}

//
// Marshal a rxdoor_ref job.
//
inline void
rxdoor_ref::_put(MarshalStream &wms)
{
	wms.put_boolean(is_reg);
	wms.put_unsigned_int(xdid);
	wms.put_unsigned_short(serviceid);
}

//
// Unmarshal a rxdoor_ref job.
//
inline void
rxdoor_ref::_get(MarshalStream &wms)
{
	is_reg = wms.get_boolean();
	xdid = wms.get_unsigned_int();
	serviceid = wms.get_unsigned_short();
}

//
// get_kit_id
//
//	This method must be used only on the server node that has
//	received a refjob. It returns the kit id to be used to find
//	the rxdoor in the refjob.
//
inline xkit_id_t
rxdoor_ref::get_kit_id()
{
	//
	// In order to avoid storing and marshalling the kit id, use
	// the serviceid field to decide what type of rxdoor is
	// contained in this ref job.
	//
	if (serviceid == 0) { // INVALID_SERVICE_ID
		//
		// This is used for all rxdoors since they are stored and
		// retrieved in the same manner.
		//
		return (STDXD_TO_CLIENT_XKID);
	} else {
		return (HXDOOR_XKID);
	}
}

#endif	/* _RXDOOR_DEFS_IN_H */
