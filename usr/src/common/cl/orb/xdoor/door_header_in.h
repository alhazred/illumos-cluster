/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * door_header_in.h
 *
 */

#ifndef	_DOOR_HEADER_IN_H
#define	_DOOR_HEADER_IN_H

#pragma ident	"@(#)door_header_in.h	1.5	08/07/17 SMI"

// Inline functions for door_msg_hdr

//
// Constructors
//
#if (SOL_VERSION >= __s10) && !defined(UNODE)
inline
door_msg_hdr_t::door_msg_hdr_t(
    zoneid_t zone, orb_msgtype type, uint32_t clid,
    uint64_t zone_uniq_id) :
    cluster_id(clid),
    zone_uniqid(zone_uniq_id),
    zone_id(zone),
    invo_way(type)
{
}
#else
inline
door_msg_hdr_t::door_msg_hdr_t(orb_msgtype type) :
    invo_way(type)
{
}
#endif

inline
door_msg_hdr_t::door_msg_hdr_t() :
#if (SOL_VERSION >= __s10) && !defined(UNODE)
    cluster_id(0),
#ifdef _KERNEL
    zone_uniqid(GLOBAL_ZONEUNIQID),
#else
    zone_uniqid(0),
#endif

    zone_id(GLOBAL_ZONEID),

#endif
    invo_way(INVO_ONEWAY_MSG)
{
}

inline
door_msg_hdr_t::door_msg_hdr_t(char *&data_ptr, size_t &data_size)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	cluster_id = ((door_msg_hdr_t *)data_ptr)->get_cluster_id();
	zone_id = ((door_msg_hdr_t *)data_ptr)->get_zone_id();
	zone_uniqid = ((door_msg_hdr_t *)data_ptr)->get_zone_uniqid();
#endif
	invo_way = (orb_msgtype) (((door_msg_hdr_t *)data_ptr)->get_invo_way());
	data_ptr += sizeof (door_msg_hdr_t);
	data_size -= sizeof (door_msg_hdr_t);
}


#if (SOL_VERSION >= __s10) && !defined(UNODE)
inline zoneid_t
door_msg_hdr_t::get_zone_id() const
{
	return (zone_id);
}

inline uint64_t
door_msg_hdr_t::get_zone_uniqid() const
{
	return (zone_uniqid);
}

inline uint32_t
door_msg_hdr_t::get_cluster_id() const
{
	return ((uint32_t)cluster_id);
}

inline void
door_msg_hdr_t::set_zone_id(zoneid_t zone)
{
	zone_id = zone;
}

inline void
door_msg_hdr_t::set_zone_uniqid(uint64_t zone_uniq_id)
{
	zone_uniqid = zone_uniq_id;
}

inline void
door_msg_hdr_t::set_cluster_id(uint32_t clid)
{
	cluster_id = clid;
}

#endif

inline orb_msgtype
door_msg_hdr_t::get_invo_way() const
{
	return (invo_way);
}

inline void
door_msg_hdr_t::set_invo_way(orb_msgtype type)
{
	invo_way = type;
}

inline
door_msg_hdr_t::~door_msg_hdr_t()
{
}

#endif	/* _DOOR_HEADER_IN_H */
