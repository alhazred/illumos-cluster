//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _STANDARD_XDOOR_H
#define	_STANDARD_XDOOR_H

#pragma ident	"@(#)standard_xdoor.h	1.112	08/05/20 SMI"


#include <orb/infrastructure/orbthreads.h>
#include <orb/xdoor/rxdoor_mgr.h>
#include <orb/xdoor/rxdoor_kit.h>
#include <orb/member/members.h>
#include <sys/nodeset.h>
#include <orb/refs/unref_threadpool.h>

//
// standard_xdoor - the orb manages this xdoor class using reference counts.
//		When the reference count reaches zero, the xdoor is deleted.
//
class standard_xdoor_kit {
public:
	void	unmarshal_cancel_remainder(ID_node &, rxdoor_descriptor &,
		    MarshalStream &);
};

class standard_xdoor_to_server_kit :
	public standard_xdoor_kit,
	public rxdoor_to_server_kit {
public:
	standard_xdoor_to_server_kit();
	virtual rxdoor *create(rxdoor_descriptor &);
};

class standard_xdoor_to_client_kit :
	public standard_xdoor_kit,
	public rxdoor_to_client_kit {
public:
	standard_xdoor_to_client_kit();
	virtual rxdoor *create(rxdoor_descriptor &);
};

class standard_xdoor_from_server_kit :
	public standard_xdoor_kit,
	public rxdoor_from_server_kit {
public:
	standard_xdoor_from_server_kit();
	virtual rxdoor *create(rxdoor_descriptor &);
};

class standard_xdoor : public rxdoor {
public:
	// constructor for server standard xdoors
	standard_xdoor(handler *h);

	// constructor for client standard xdoors
	standard_xdoor(xdoor_id xdn);

	virtual ~standard_xdoor();

	bool    reference_counting();

	void    unmarshal_roll_back();

protected:
	// Number of foreign references
	uint_t	foreignrefs;

private:
	// Disallow assignments and pass by value
	standard_xdoor(const standard_xdoor &);
	standard_xdoor &operator = (standard_xdoor &);
};

class standard_xdoor_server :
	public standard_xdoor,
	public unref_task
{
public:
	static standard_xdoor_from_server_kit from_server_kit;

	static Xdoor *create(handler *h = NULL);

	standard_xdoor_server(handler *h);

	virtual ~standard_xdoor_server();

	void	register_client(ID_node &);
	void	unregister_client(ID_node &);
	void	cleanup_foreignrefs(const nodeset &dead_nodeset);
	void	accept_unreferenced();
	void	deliver_unreferenced();
	bool	xdoor_inactive(uint_t);
	void	unreferenced_done();
	void	revoked();
	void	marshal(ID_node &, MarshalStream &, Environment *);
	void	do_marshal(ID_node &, MarshalStream &);
	void	marshal_roll_back(ID_node &, MarshalStream &);
	void	marshal_cancel();
	void	marshal_local(Environment *);
	virtual void	marshal_received();
	void	unmarshal(ID_node &, MarshalStream &, bool, Environment *);
	void	incoming_prepare(Environment *);
	void	incoming_done();

	bool	local();

	//
	// We should forward this to the handler so that we can detect
	// dead userland servers.
	//
	bool	server_dead();

	//
	// Identify resources needed at xdoor level for request
	// This method is used for invocation messages and not reply messages.
	//
	sendstream		*reserve_resources(resources *resourcep);

	// Identify resources needed at rxdoor level for invocation reply
	sendstream		*reserve_reply_resources(resources *resourcep);

	virtual const char	*xdoor_type();

	virtual rxdoor_kit	&get_kit();

	//
	// Identify other nodes holding reference
	// (this ignores local node).
	//
	void		identify_client_nodes(nodeset &refset);

protected:
	//
	// This is used by the hxdoor.
	//
	standard_xdoor_server(xdoor_id id);
	virtual handler			*get_unref_handler();
	virtual unref_task_type_t	unref_task_type();

	bool		has_foreignrefs();
	bool		refmasks_empty();

	// Number of times we have been marshaled
	uint_t		marshalsmade;

	// Number of services currently in progress
	uint_t		services;

	// Identifies current rxdoor state
	uint32_t	flags;

	// Used for revoke.
	os::condvar_t	revoke_cv;

	//
	// Two bitmasks for remote node references to this door In a
	// stable state we have a bit in refmask for each reference.
	// If we get register msg from a node right after we have sent
	// it a reference ourselves, then we set a bit in
	// extramask. This will be cleared shortly thereafter by an
	// unregister from the node.
	//
	nodeset		refmask;
	nodeset		extramask;

private:
	void		dec_marshcount(uint_t);
	void		handle_possible_unreferenced();

	// Disallow assignments and copy constructor
	standard_xdoor_server(const standard_xdoor_server &);
	standard_xdoor_server &operator = (standard_xdoor_server &);
};


//
// standard_xdoor_client - reference counting client xdoor
//
class standard_xdoor_client :
	public standard_xdoor,
	public knewdel
{
public:
	static standard_xdoor_to_client_kit  to_client_kit;
	static standard_xdoor_to_server_kit  to_server_kit;

	//
	// After the create, the xdoor is half useful, as it does not yet
	// know who passed it the reference. It will learn that
	// after the unmarshal function is called.
	//
	static rxdoor	*create(rxdoor_descriptor &);

	standard_xdoor_client(rxdoor_id &rx);

	virtual ~standard_xdoor_client();

	void	inc_foreignref();
	void	dec_foreignref();

	void	marshal(ID_node &, MarshalStream &, Environment *);
	void	marshal_roll_back(ID_node &, MarshalStream &);
	void	marshal_cancel();
	void	marshal_local(Environment *);
	virtual void	marshal_received();
	void	update_marshal_state();
	void	unmarshal(ID_node &, MarshalStream &, bool, Environment *);

	//
	// Identify resources needed at xdoor level for request
	// This method is used for invocation messages and not reply messages.
	//
	sendstream	*reserve_resources(resources *resourcep);

	bool		local();

	// Uses the current membership to decide if the server xdoor is dead.
	bool		server_dead();

	virtual const char	*xdoor_type();

	virtual rxdoor_kit	&get_kit();

private:
	void		dec_marshcount(uint_t);
	void		handle_possible_unreferenced();

	static const char	*xdoor_type_string;

	// Disallow assignments and pass by value
	standard_xdoor_client(const standard_xdoor_client &);
	standard_xdoor_client &operator = (standard_xdoor_client &);
};

#include <orb/xdoor/standard_xdoor_in.h>

#endif	/* _STANDARD_XDOOR_H */
