/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DOOR_HEADER_H
#define	_DOOR_HEADER_H

#pragma ident	"@(#)door_header.h	1.5	08/07/17 SMI"

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#else
#undef	UNODE
#endif

#include <orb/flow/resource_defs.h>
#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <sys/zone.h>
#endif
#include <orb/buffers/marshalstream.h>

//
// door_header.h - door message header containing zone information
// and identifier for ONEWAY/TWOWAY invocation
//

class door_msg_hdr_t {
public:
#if (SOL_VERSION >= __s10) && !defined(UNODE)

#ifdef _KERNEL
	door_msg_hdr_t(zoneid_t, orb_msgtype,
	    uint32_t cluster_id = 0,
	    uint64_t zone_uniq_id = GLOBAL_ZONEUNIQID);
#else
	door_msg_hdr_t(zoneid_t, orb_msgtype, uint32_t cluster_id = 0,
	    uint64_t zone_uniq_id = 0);
#endif

#else
	door_msg_hdr_t(orb_msgtype);
#endif
	door_msg_hdr_t();
	door_msg_hdr_t(char *&, size_t &);
	~door_msg_hdr_t();

	static void	prepend_header_buf(MarshalStream &);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	zoneid_t	get_zone_id() const;
	uint64_t	get_zone_uniqid() const;
	void		set_zone_id(zoneid_t);
	void		set_zone_uniqid(uint64_t);
	void		set_cluster_id(uint32_t);
	uint32_t	get_cluster_id() const;
#endif

	orb_msgtype	get_invo_way() const;
	void		set_invo_way(orb_msgtype);

#if defined(MARSHAL_DEBUG)
	uint32_t	marshal_debug; // this must be the 1st word
	//
	// The requirement is that the size of the door header in userland
	// and kernel should be the same.
	// On Sparc, the header sizes in userland and kernel address spaces
	// match, but not on AMD64. So, we need to add padding here.
	//
	// Padding is added here rather than at the end of the class, so that
	// the 4-byte padding word teams up with the 4-byte debug word
	// (declared above) resulting in proper alignment on 8-byte boundary.
	// If we do not add the padding here, the compiler will pad the debug
	// word for 8-byte alignment when compiling for 64-bit, but, will not
	// pad the debug word when compiling for 32-bit. This results in size
	// mismatch of this header between 32-bit and 64-bit compilation.
	//
	// IMPORTANT : If new fields are added to the door header, or if the
	// current arrangement of fields is changed, it has to be confirmed that
	// the size of the door header in userland and kernel are the same.
	//
	uint32_t	padding;
#endif	// MARSHAL_DEBUG

private:
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// virtual cluster id.
	//
	uint32_t	cluster_id;
	uint32_t	pad;
	//
	// Zone information
	// We identify a running zone based on its zone_id,
	// but zone ids can be recycled. So, a zone named "foo" could
	// get a zone_id that is same as the zone_id it got in one
	// of its previous generations. We use zone_uniqid to
	// distinguish between such cases, as zone_uniqid is a unique
	// generation number for zones.
	//

	uint64_t	zone_uniqid;
	zoneid_t	zone_id;
#endif
	orb_msgtype	invo_way;
};

#ifndef	NOINLINES
#include <orb/xdoor/door_header_in.h>
#endif	// _NOINLINES

#endif	/* _DOOR_HEADER_H */
