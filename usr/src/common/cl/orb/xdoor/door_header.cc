/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * door_header.cc
 *
 */

#pragma ident	"@(#)door_header.cc	1.2	08/05/20 SMI"

#include <orb/xdoor/door_header.h>

//
// In the case that space for marshalling the door header hasn't
// been reserved before, this method is used to allocate space
// for the door header.
//
void
door_msg_hdr_t::prepend_header_buf(MarshalStream &marshal)
{
	MarshalStream	temp_marshal(
	    (uint32_t)sizeof (door_msg_hdr_t), marshal.get_env());

	ASSERT(!marshal.get_env()->is_nonblocking());

	uint32_t header_size =
	    temp_marshal.alloc_chunk(sizeof (door_msg_hdr_t));

	ASSERT(header_size == sizeof (door_msg_hdr_t));

	// move original marshalstream region to end of temp
	temp_marshal.useregion(marshal.region());

	//
	// return all (door_msg_header concatenated with original)
	// to original marshalstream region
	//
	marshal.useregion(temp_marshal.region());
}
