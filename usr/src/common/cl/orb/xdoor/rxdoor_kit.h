/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RXDOOR_KIT_H
#define	_RXDOOR_KIT_H

#pragma ident	"@(#)rxdoor_kit.h	1.4	08/05/20 SMI"


#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/xdoor_kit.h>
#include <orb/xdoor/rxdoor_defs.h>

class rxdoor;
class rxdoor_id;
class rxdoor_invo_header;
class rxdoor_bucket;


class rxdoor_kit : public xdoor_kit {
public:
	virtual Xdoor *unmarshal(ID_node &, MarshalStream &, Environment *);
	//	virtual xkit_id_t unmarshal(MarshalStream &rms);

	virtual void unmarshal_cancel(ID_node &, MarshalStream &);
	virtual void marshal_roll_back(ID_node &, MarshalStream &);

	// Lookup rxdoor using kit specific information
	virtual rxdoor	*lookup_rxdoor(bool *, ID_node &, MarshalStream &,
			    rxdoor_bucket **) = 0;

	virtual rxdoor	*lookup_rxdoor_from_invo_header(rxdoor_invo_header *,
			    rxdoor_bucket **);

	virtual rxdoor 	*lookup_rxdoor_from_refjob(rxdoor_ref *refp,
			    rxdoor_bucket **xbpp);

	virtual void	marshal_xdoor(MarshalStream &, rxdoor_descriptor &);
	virtual void	marshal_xdoor(MarshalStream &, rxdoor *);

protected:
	// The marshal length of an rxdoor is that of the rxdoor_id plus any
	// extra stuff added by the derived class.
	rxdoor_kit(const xkit_id_t kid, uint32_t ml);

	// Create a client rxdoor of this type.
	virtual rxdoor *create(rxdoor_descriptor &) = 0;

	// The rxdoor id has been extracted from the marshal stream, now
	// advance the stream past any type specific extra data and do
	// any other work that is required to cancel an unmarshal.
	virtual void unmarshal_cancel_remainder(ID_node &, rxdoor_descriptor &,
		MarshalStream &);

};

//
// This is a kit used by rxdoors that are sent from client to server.
// The client side knows it is sending the reference to the server, so
// there is no need to marshal the node id of the xdoor server. The
// server knows it can find this xdoor in it local node.
//
class rxdoor_to_server_kit : public rxdoor_kit {
public:
	virtual void	marshal_xdoor(MarshalStream &, rxdoor_descriptor &);
	virtual void	marshal_xdoor(MarshalStream &, rxdoor *);
	virtual void	unmarshal_cancel(ID_node &, MarshalStream &);

protected:
	// The marshal length of an rxdoor is that of the rxdoor_id plus any
	// extra stuff added by the derived class.
	rxdoor_to_server_kit(const xkit_id_t kid, uint_t ml);

	// Lookup rxdoor using kit specific information
	virtual rxdoor	*lookup_rxdoor(bool *create, ID_node &, MarshalStream &,
			    rxdoor_bucket **);

};

//
// This is a kit used by rxdoors that are sent from client to another
// client, In this case, client needs to marshal the server node id
// and xdoor id.
//
class rxdoor_to_client_kit : public rxdoor_kit {
public:
	// This class should use the default implementation of:
	// marshal_xdoor(MarshalStream &, rxdoor_descriptor &);
	// marshal_xdoor(MarshalStream &, rxdoor *);

protected:
	// The marshal length of an rxdoor is that of the rxdoor_id plus any
	// extra stuff added by the derived class.
	rxdoor_to_client_kit(const xkit_id_t kid, uint_t ml);

	// Lookup rxdoor using kit specific information
	virtual rxdoor	*lookup_rxdoor(bool *create, ID_node &, MarshalStream &,
			    rxdoor_bucket **);

};

//
// This is a kit used by rxdoors that are sent from server to client.
// The receiving client can identify the server as the node from which
// this message was translated in. This again saves marshalling the
// server nodeid along with the xdoor.
//
class rxdoor_from_server_kit : public rxdoor_kit {
public:
	virtual void	marshal_xdoor(MarshalStream &, rxdoor_descriptor &);
	virtual void	marshal_xdoor(MarshalStream &, rxdoor *);
	virtual void	marshal_roll_back(ID_node &, MarshalStream &);
	virtual void 	unmarshal_cancel(ID_node &, MarshalStream &);
protected:
	// The marshal length of an rxdoor is that of the rxdoor_id plus any
	// extra stuff added by the derived class.
	rxdoor_from_server_kit(const xkit_id_t kid, uint_t ml);

	// Lookup rxdoor using kit specific information
	virtual rxdoor *lookup_rxdoor(bool *create, ID_node &, MarshalStream &,
			rxdoor_bucket **);
};

#include <orb/xdoor/rxdoor_kit_in.h>

#endif	/* _RXDOOR_KIT_H */
