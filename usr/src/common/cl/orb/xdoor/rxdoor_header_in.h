/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  rxdoor_header_in.h
 *
 */

#ifndef _RXDOOR_HEADER_IN_H
#define	_RXDOOR_HEADER_IN_H

#pragma ident	"@(#)rxdoor_header_in.h	1.15	08/07/17 SMI"

//
// rxdoor_invo_header methods
//
//
// rxdoor_oneway_header methods
//

//
// make_header - create a oneway message header
//
// static
#if (SOL_VERSION >= __s10) && !defined(UNODE)
inline void
rxdoor_oneway_header::make_header(uint_t cluster_id, sendstream *sendstreamp,
    xdoor_id target)
#else
inline void
rxdoor_oneway_header::make_header(sendstream *sendstreamp, xdoor_id target)
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)
{
	rxdoor_oneway_header *rx_hdrp = ((rxdoor_oneway_header *)sendstreamp->
	    make_header(sizeof (rxdoor_oneway_header)));

	rx_hdrp->xdid = target;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// Assign the cluster id here.
	//
	rx_hdrp->zone_cluster_id = cluster_id;
#endif
}

//
// get_kit_id
//
//   return kit id of contained in the invocation.  For all rxdoors,
//   the this method is called on the server that receives the
//   invocation. So the server will always lookup in the local_rx_node
//   for the server sxdoor. But to make it absolutely clear, the kit
//   id must be STDXD_TO_SERVER_XKID. This works for simple_xdoors too
//   because the rxdoor_manager::lookup_local_rxdoor() checks if the
//   xdoor_id falls in the fixed ids range and does a lookup in the
//   fixed_rx_nodes array. The actual lookup for server xdoor is done
//   by rxdoor_kit::lookup_lookup_rxdoor_from_invo_header() which
//   calls rxdoor_manager::lookup_local_rxdoor().
//
// static
inline xkit_id_t
rxdoor_oneway_header::get_kit_id()
{
	return (STDXD_TO_SERVER_XKID);
}

//
// constructor - extract the data fields from a oneway message header
// and then remove the header from the sendstream.
//
inline
rxdoor_oneway_header::rxdoor_oneway_header(recstream *recstreamp)
{
	recstreamp->read_header((void *)this, sizeof (rxdoor_oneway_header));
}
//
// rxdoor_twoway_header methods
//

//
// make_header - create a twoway request message header
//
// static
#if (SOL_VERSION >= __s10) && !defined(UNODE)
inline void
rxdoor_twoway_header::make_header(uint_t cluster_id, sendstream *sendstreamp,
    xdoor_id target, ushort_t index)
#else
inline void
rxdoor_twoway_header::make_header(sendstream *sendstreamp, xdoor_id target,
    ushort_t index)
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)
{
	rxdoor_twoway_header *rx_hdrp = ((rxdoor_twoway_header *)sendstreamp->
	    make_header(sizeof (rxdoor_twoway_header)));

	rx_hdrp->xdid = target;
	rx_hdrp->slot = index;
	// svcid is unused by xdoors other than hxdoors.
	rx_hdrp->svcid = 0;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	rx_hdrp->zone_cluster_id = cluster_id;
#endif
}

//
// constructor - extract the data fields from a twoway message header
// and then remove the header from the sendstream.
//
inline
rxdoor_twoway_header::rxdoor_twoway_header(recstream *recstreamp)
{
	recstreamp->read_header((void *)this, sizeof (rxdoor_twoway_header));
}

//
// get_kit_id
//
//   return kit id of contained in the invocation
//   See comment for rxdoor_oneway_header::get_kit_id()
//
inline xkit_id_t
rxdoor_twoway_header::get_kit_id()
{
	return ((svcid == 0) ? STDXD_TO_SERVER_XKID : HXDOOR_XKID);
}

//
// rxdoor_reply_header methods
//

//
// make_header - create a reply message header
//
// static
inline void
rxdoor_reply_header::make_header(sendstream *sendstreamp, ushort_t index,
    uint_t flags)
{
	rxdoor_reply_header	*rx_hdrp = ((rxdoor_reply_header *)sendstreamp->
	    make_header(sizeof (rxdoor_reply_header)));

	rx_hdrp->slot = index;

	// Ensure that only valid flag bits are set
	ASSERT((flags & ~(SIGNALED | EXCEPTION | RETRY)) == 0);

	rx_hdrp->flags = flags;
}

//
// constructor - extract the data fields from a reply message header
// and then remove the header from the sendstream.
//
inline
rxdoor_reply_header::rxdoor_reply_header(recstream *recstreamp)
{
	recstreamp->read_header((void *)this, sizeof (rxdoor_reply_header));
}

//
// Test of signal bit
//
// static
inline bool
rxdoor_reply_header::is_signaled(uint_t flags) {
	return ((bool)((flags & SIGNALED) != 0));
}

//
// Test for server side exception
//
// static
inline bool
rxdoor_reply_header::is_exception(uint_t flags) {
	return ((bool)((flags & EXCEPTION) != 0));
}

//
// Test for server retry request
//
// static
inline bool
rxdoor_reply_header::is_retry(uint_t flags) {
	return ((bool)((flags & RETRY) != 0));
}

#endif	/* _RXDOOR_HEADER_IN_H */
