//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)solaris_xdoor.cc	1.146	08/08/13 SMI"

#include <orb/xdoor/solaris_xdoor.h>
#include <orb/xdoor/sxdoor.h>
#include <orb/transport/sdoor_transport.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <door.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <string.h>
#include <orb/msg/orb_msg.h>
#include <nslib/ns.h>

#ifdef DEBUG
#include <sys/uadmin.h>
#endif

#if (SOL_VERSION >= __s10)
#include <zone.h>
#include <sys/cladm_int.h>
#include <ucred.h>
#endif	// SOL_VERSION >= __s10

#include <orb/xdoor/door_header.h>

// Kit stuff
solaris_xdoor_kit solaris_xdoor::XK;
//
// Door Buffer Management
//	- All Buffer sizes are allocated in units of bytes
//

// Minimum size of buffer used with replies
static const size_t min_reply_size = 1024;

// Maximum size of a local (stack based) Buffer
static const size_t max_local_size = 256 * 1024;

// persistent xdoor lock, could be broken up
static os::mutex_t xdoor_lock;

uint_t solaris_xdoor_client::invo_count = 0;

// conditional variable waiting on invo_count
os::condvar_t solaris_xdoor_client::invo_cv;

// lock protecting  invo_count
os::mutex_t solaris_xdoor_client::invo_lck;

//
// solaris_xdoor methods
//

solaris_xdoor::~solaris_xdoor()
{
	// to make lint happy
	handler_ = 0;
	lckp = 0;
}

//
// Extract handler from xdoor.
//
handler *
solaris_xdoor::extract_handler(handler_type)
{
	lock();
	umarsh_ip++;
	ASSERT(umarsh_ip != 0);
	while (handler_ == NULL && umarsh_ip > 1) {
		//
		// The first unmarshal of this handler is in progress.
		// Allows this to complete since we only want one handler
		// created.
		//
		cv.wait(&get_lock());
	}
	unlock();
	return (handler_);
}

//
// Notify xdoor that handler extraction is finished (decrements count
// of outstanding unmarshals).
//
void
solaris_xdoor::extract_done(handler_type, handler *hp, bool)
{
	os::mutex_t *mp = &get_lock();

	mp->lock();

	ASSERT(hp != NULL);
	ASSERT(handler_ == NULL || handler_ == hp);
	ASSERT(umarsh_ip > 0);
	if (--umarsh_ip != 0 && handler_ == NULL) {
		cv.broadcast();
	}
	handler_ = hp;
	ASSERT(umarshout > 0);
	--umarshout;
	SOLXDR_DBG(("extract_done xdoor %p uo %d\n", this, umarshout));
	handle_possible_unreferenced();
	mp->unlock();
}

//
// marshal - place a xdoor representation in the specified marshalstream
//
void
solaris_xdoor::marshal(MarshalStream &wms, Environment *e)
{
	door_desc_t d;
	int did;

	os::mutex_t *mp = &get_lock();
	mp->lock();

	//
	// Only marshal valid xdoors that have not been revoked
	//
	CL_PANIC(!(flags & UNREFERENCED));
	if (flags & (REVOKED | REVOKE_PENDING)) {
		//
		// Synchronize marshal count with handler
		// even though not marshalled.
		//
		mc_handler++;
		ASSERT(mc_handler != 0);

		//
		// We call unreferenced as the xdoor may no longer be needed.
		//
		handle_possible_unreferenced();
		e->system_exception(CORBA::BAD_PARAM(0,
		    CORBA::COMPLETED_MAYBE));
		mp->unlock();
		return;
	}

	if ((flags & UNREF_PENDING) != 0) {
		//
		// A marshal is occuring while an unref task is pending.
		//
		// The MARSHAL_DURING_UNREF flag must be set before the
		// marshal count is incremented. This requirement exists
		// in order to ensure that any object reference marshal
		// beginning after the queueing of a xdoor unreference task
		// will result in the rejection of that xdoor unreference.
		//
		flags |= MARSHAL_DURING_UNREF;
	}
	// Synchronize marshal count with handler.
	mc_handler++;
	ASSERT(mc_handler != 0);

	ASSERT(get_fd() != -1);

#ifndef _LP64
	// bug 4266886
	// Check if the door desc. value is < 255. It will be so if this
	// is the first time it is being marshalled. In order to free fds
	// < 255, marshal the original fd first time and dup'd fds later.

	did = fcntl(get_fd(), F_DUPFD, 256);

	if (get_fd() < 255) {
		// The original fd obtained from door_create() has close-on-exec
		// flag set. Since the orig fd is replaced by the dup'd fd,
		// close-on-exec flag must be set explicitly for the dup'd fd.

		int ret = fcntl(did, F_SETFD, FD_CLOEXEC);
		if (ret == -1) {
			//
			// SCMSGS
			// @explanation
			// A fcntl operation failed. The "fcntl" man page
			// describes possible error codes.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: solaris xdoor fcntl failed: %s",
			    strerror(errno));
		}
		int orig_fd = get_fd();
		set_fd(did);
		did = orig_fd;
	}
#else
	did = dup(get_fd());
#endif  // !_LP64

	if (did == -1) {
		//
		// SCMSGS
		// @explanation
		// A dup operation failed. The "dup" man page describes
		// possible error codes.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor dup failed: %s", strerror(errno));
	}

	// Set close-on-exec flag
	int ret = fcntl(did, F_SETFD, FD_CLOEXEC);
	if (ret == -1) {
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor fcntl failed: %s", strerror(errno));
	}
	mp->unlock();

	SOLXDR_DBG(("marshal xdoor %p\n", this));

	d.d_attributes = DOOR_DESCRIPTOR | DOOR_RELEASE;
	d.d_data.d_desc.d_descriptor = did;
	wms.put_bytes(&d, sizeof (d), false);
}

void
solaris_xdoor::marshal_roll_back(MarshalStream &rms)
{
	os::mutex_t *mp = &get_lock();

	door_desc_t d;
	rms.get_bytes(&d, sizeof (d), false);
	(void) close(d.d_data.d_desc.d_descriptor);

	mp->lock();
	// The close could have caused a door_is_unref() to succeed
	handle_possible_unreferenced();
	mp->unlock();
	SOLXDR_DBG(("marshal_roll_back xdoor %p\n", this));
}

//
// No-argument version of unmarshal for user level.
//
void
solaris_xdoor::unmarshal(Environment *e)
{
	lock();
	CL_PANIC(!(flags & UNREFERENCED));
	//
	// Unmarshal will fail if the server xdoor has been revoked.
	// The server xdoor no longer has a handler,
	// so unmarshaling the server does not make sense.
	//
	if (flags & (REVOKED | REVOKE_PENDING)) {
		e->system_exception(CORBA::BAD_PARAM(0,
				CORBA::COMPLETED_MAYBE));
		unlock();
		return;
	}

	umarshout++;
	ASSERT(umarshout != 0);
	SOLXDR_DBG(("unmarshalling xdoor %p uo %d\n", this, umarshout));
	unlock();
}

void
solaris_xdoor::unmarshal_roll_back()
{
	os::mutex_t *mp = &get_lock();

	mp->lock();
	ASSERT(umarshout > 0);
	umarshout--;
	SOLXDR_DBG(("unmarshal_roll_back xdoor %p uo %d\n", this, umarshout));
	handle_possible_unreferenced();
	mp->unlock();
}

//
// specify_resources - Identify resources needed at xdoor level for request.
//
// Reserve space for marshalling door_msg_hdr (containing zone info)
//
// This supports "local" U->K and K->U invocations.
// Returns a send stream pointer, which can be NULL in case of error.
//
sendstream *
solaris_xdoor::reserve_resources(resources *resourcep)
{
	resourcep->add_send_header(sizeof (door_msg_hdr_t));
	return (orb_msg::reserve_resources(resourcep));
}

//
// Called by the handler on the client side when there may be no more
// local references on a door.
//
bool
solaris_xdoor::client_unreferenced(handler_type, uint_t)
{
	os::mutex_t *mp = &get_lock();

	ASSERT(!local());	// must be client xdoor

	mp->lock();

	ASSERT(handler_ != NULL);

	if (umarsh_ip > 0) {
		mp->unlock();
		return (false);
	}
	handler_ = NULL;

	handle_possible_unreferenced();
	mp->unlock();
	return (true);
}

void
solaris_xdoor::accept_release()
{
	ASSERT(lock_held());
	SOLXDR_DBG(("deleting %p\n", this));
	ASSERT(!(flags & REVOKE_PENDING));
	ASSERT(get_fd() != -1);
	(void) close(get_fd());
	delete this;
}

//
// solaris_xdoor_server methods
//

//
// Return the current node id.
//
nodeid_t
solaris_xdoor_server::get_server_nodeid()
{
	return (orb_conf::local_nodeid());
}

void
solaris_xdoor_server::marshal_cancel()
{
	os::mutex_t *mp = &get_lock();
	mp->lock();

	mc_handler++;
	ASSERT(mc_handler != 0);
	handle_possible_unreferenced();
	mp->unlock();
	SOLXDR_DBG(("marshal_cancel xdoor %p\n", this));
}

bool
solaris_xdoor_server::door_is_unref()
{
	door_info_t di;

	ASSERT(lock_held());
	// Cannot call this in REVOKE_PENDING state
	ASSERT((flags & REVOKE_PENDING) == 0);
	ASSERT(get_fd() != -1);
	if (door_info(get_fd(), &di) < 0) {
		//
		// SCMSGS
		// @explanation
		// A door_info operation failed. Refer to the "door_info" man
		// page for more information.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor door_info failed");
	}

	return ((di.di_attributes & DOOR_IS_UNREF) != 0);
}

//
// handle_possible_unreferenced - determine whether activity involving
// this xdoor has ceased.
//
void
solaris_xdoor_server::handle_possible_unreferenced()
{
	ASSERT(lock_held());
	//
	// If revoked xdoor, door unref may not have arrived but we need to
	// deliver unreferenced to the handler
	//
	if ((invos_in_progress == 0) && (umarshout == 0) &&
	    ((flags & (REVOKE_PENDING | UNREF_PENDING)) == 0) &&
	    (door_is_unref() || ((flags & REVOKED) != 0))) {
		CL_PANIC(!(flags & UNREFERENCED));

		if (handler_ == NULL) {
			ASSERT(flags & REVOKED);
			if (door_is_unref()) {
				unreferenced_done();
			}
		} else {
			//
			// XXX see bug 4212287 and 4178842
			// this is fix for 4212287
			// if orb is shutting down, then unref_threadpool
			// is not available, and we can't complete the
			// unref.  We would like to make sure that the
			// xdoor is revoked....which is bug 4178842
			//
			if (ORB::is_shutdown()) {
				SOLXDR_DBG(
				    ("server unref xdoor %p %p"
				    "...ORB is shutdown\n",
				    this, handler_));
			} else {
				flags |= UNREF_PENDING;

				SOLXDR_DBG(("sched unref xd %p hdl %p fl %x\n",
				    this, handler_, flags));

				// Schedule notification to server handler
				unref_threadpool::the().
				    defer_unref_processing(this);
			}
		}
		// The xdoor may have been deleted by now
		// At a minimum the xdoor should have been unreferenced
	}
}

//
// get_unref_handler - return the server handler for use by the unref_task.
//
handler *
solaris_xdoor_server::get_unref_handler()
{
	return (handler_);
}

unref_task_type_t
solaris_xdoor_server::unref_task_type()
{
	return (unref_task::XDOOR);
}

//
// unreferenced_done - the xdoor now self destructs.
//
void
solaris_xdoor_server::unreferenced_done()
{
	ASSERT(lock_held());

#ifdef DEBUG
	if (!door_is_unref()) {
		// First print all door data
		os::printf("solaris_xdoor_server: %p\n", this);
		os::printf("   umarshout	: %u\n", umarshout);
		os::printf("   umarsh_ip	: %hu\n", umarsh_ip);
		os::printf("   door_id_t	: %ul\n", id);
		os::printf("   fd		: %d\n", fd);
		os::printf("   flags		: 0x%x\n", flags);
		os::printf("   hold_count	: %u\n", hold_count);
		os::printf("   unref_wanted	: %d\n", unref_wanted);
		os::printf("   invos_in_progress: %u\n", invos_in_progress);
		// panic the node
		(void) uadmin(A_DUMP, AD_BOOT, (uintptr_t)0);
	}
#endif

	ASSERT(invos_in_progress == 0);
	ASSERT(umarshout == 0);
	ASSERT(handler_ == NULL);
	ASSERT(!(flags & (REVOKE_PENDING | UNREF_PENDING)));

	flags |= UNREFERENCED;
	sxdoor_table::the().remove_door(this, get_id());
}

//
// deliver_unreferenced
//
//   process an unref_task for solaris_xdoor_server
//
void
solaris_xdoor_server::deliver_unreferenced()
{
	// get a reference to the lock. This lock is the scdoor_table lock
	// but cannot be accessed once the xdoor is deleted.

	os::mutex_t	&sxd_lock = get_lock();
	handler		*handp = get_unref_handler();
	uint_t		handler_marshal_count;
	bool		xdoor_unref = false;

	handler_marshal_count = handp->begin_xdoor_unref();

	sxd_lock.lock();

	// Show no longer have a pending unreference
	ASSERT((flags & UNREF_PENDING) != 0);
	flags &= ~UNREF_PENDING;
	if ((handler_marshal_count == mc_handler) &&
	    ((flags & MARSHAL_DURING_UNREF) == 0)) {
		// Break the connection with the handler
		ASSERT(handler_ != NULL);

		handler_ = NULL;
		//
		// Case 1. if the door has been unrefed delete it,
		// Case 2. if the door has not been unrefed or revoked,
		// then the program may have forked and we expect to get another
		// unreferenced, we set the REVOKED flag and wait for the unref
		// (see bugid 4242238 for more details)
		// Case 3. xdoor has been revoked and we wait for the door unref
		//
		xdoor_unref = true;
		if (door_is_unref()) {
			unreferenced_done();
		} else if ((flags & REVOKED) == 0) {
			ASSERT((invos_in_progress == 0) && (umarshout == 0));
			flags |= REVOKED;
		}
	} else {
		flags &= ~MARSHAL_DURING_UNREF;
		//
		// New reference may have been created and
		// released. So it may be necessary to handle
		// the unreference.
		//
		if (handler_marshal_count == mc_handler) {
			handle_possible_unreferenced();
		}
	}
	sxd_lock.unlock();
	handp->end_xdoor_unref(xdoor_unref);
}

//
// Create the server side object (Solaris Door)
//
solaris_xdoor_server::solaris_xdoor_server(handler *h, bool unref_needed) :
	solaris_xdoor(&xdoor_lock, h),
	unref_wanted(unref_needed),
	invos_in_progress(0)
{
	int		door_descr;
	door_info_t	info;
	uint_t		unref_flag;

	if (unref_wanted) {
		unref_flag = DOOR_UNREF_MULTI;
	} else {
		unref_flag = 0;
	}

	//
	// Mask all the signals before doing the door_create so that
	// the door_call handling threads will not get signals.
	//
	sigset_t new_mask, orig_mask;
	(void) sigfillset(&new_mask);
	(void) thr_sigsetmask(SIG_SETMASK, &new_mask, &orig_mask);

	//
	// Note that the 'cookie' for the door invocation is
	// the 'this' ptr, which is passed by Solaris as
	// the first argument to a door invocation.
	//
	door_descr =
	    door_create(solaris_xdoor_server::object_server, this, unref_flag);

	// Restore the thread signal property.
	(void) thr_sigsetmask(SIG_SETMASK, &orig_mask, NULL);

	if (door_descr == -1) {
		//
		// SCMSGS
		// @explanation
		// A door_create operation failed. Refer to the "door_create"
		// man page for more information.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor door_create failed");
	}

	// make sure object_server() does not peek before constructor is done.
	// See Note above.
	//
	lock();

	if (door_info(door_descr, &info) < 0) {
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor door_info failed");
	}
	set_fd(door_descr);	// initialize door id
	set_id(info.di_uniquifier);

	// done constructing object
	unlock();

	SOLXDR_DBG(("created sxd_s %p door %d\n", this, door_descr));
}

//
// All incoming door invocations come here.
//
void
solaris_xdoor_server::object_server(
		void	*cookie,
		char	*data,
		size_t 	data_size,
		door_desc_t *doors,
		uint_t	ndoors)
{
	Environment e;

	// Find the instance of the object being invoked
	solaris_xdoor_server *obj = (solaris_xdoor_server *)cookie;

	// Check for unreferenced information
	if (data == DOOR_UNREF_DATA) {
		obj->unreferenced_upcall();
		(void) door_return(NULL, 0, NULL, 0);

		// We really shouldn't get here.
		// See bug 4240146.

		return;
	}

#if defined(_FAULT_INJECTION)
	//
	// Unmarshal Invo triggers from K->U or U->U invocation path.
	// Note, this will adjust data and data_size.
	//
	// XXX Pass by reference of cast value doesn't work with SC5.2
	// XXX Need to file a bug, in the mean time we can work around
	// XXX it by passing in the proper type and cast it back.
	//
	uint32_t dsize = (uint32_t)data_size;
	InvoTriggers::get(data, dsize);
	data_size = (size_t)dsize;
#endif

	//
	// Unmarshal the door_msg_hdr
	// and put the zone info and the cluster id into the Environment
	//
#if (SOL_VERSION >= __s10)
	ucred_t *ucp = NULL;
	bool bad_invo = false;

	if (door_ucred(&ucp) != 0) {
		//
		// SCMSGS
		// @explanation
		// Failed to get the credential information associated with
		// the client of the current door invocation.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor: Failed to get credentials"
		    " returned, errno = %d", errno);
		CL_PANIC(0);
		bad_invo = true;
	}
	zoneid_t curr_zoneid;
	zoneid_t zid = ((door_msg_hdr_t *)data)->get_zone_id();
	ASSERT(ucp != NULL);
	if ((curr_zoneid = ucred_getzoneid(ucp)) < 0) {
		//
		// SCMSGS
		// @explanation
		// Failed to get the zone information associated with
		// the client of the current door invocation.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is
		// available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor: Failed to get zoneid from"
		    " credentials, returned errno = %d", errno);
		ASSERT(0);
		bad_invo = true;
	}
	if ((curr_zoneid != 0) && (curr_zoneid != zid)) {
		//
		// SCMSGS
		// @explanation
		// Zone information that has been passed does
		// not match the client credentials.
		// @user_action
		// Someone has tampered with Sun cluster code. This
		// is a security violation. Shut down the identified
		// zone.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "clcomm: solaris xdoor: Got zoneid %d instead of %d",
		    zid, curr_zoneid);
		bad_invo = true;
	} else {
		e.set_zone_id(zid);
		e.set_zone_uniqid(((door_msg_hdr_t *)data)->get_zone_uniqid());
		uint32_t zc_id = ((door_msg_hdr_t *)data)->get_cluster_id();
		if (zid != 0) {
			cz_id_t	czid;
			int	ret;
			czid.clid = 0;
			if (getzonenamebyid(zid, czid.name, ZONENAME_MAX)
			    == -1) {
				(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC,
				    MESSAGE, "clcomm: getzonenamebyid "
				    "failed: %s", strerror(errno));
				ASSERT(0);
				bad_invo = true;
			}
			if ((ret = os::cladm(CL_CONFIG, CL_GET_ZC_ID, &czid))
			    != 0) {
				(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC,
				    MESSAGE, "Failed to get cluster id, "
				    "ret val = %d\n", ret);
				ASSERT(0);
				bad_invo = true;
			}
			if (czid.clid != zc_id) {
				//
				// SCMSGS
				// @explanation
				// Zone cluster id information that has been
				// passed does not match the client
				// credentials.
				// @user_action
				// Someone has tampered with Sun cluster code.
				// This is a security violation. Shut down the
				// identified zone.
				//
				(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE,
				    MESSAGE, "clcomm: solaris xdoor: Got zone"
				    " cluster id %d instead of %d for zone %s",
				    zc_id, czid.clid, czid.name);
				bad_invo = true;
			}
		}
		e.set_cluster_id(zc_id);
	}
	ucred_free(ucp);
#endif // SOL_VERSION >= __S10
	data += sizeof (door_msg_hdr_t);
	data_size -= sizeof (door_msg_hdr_t);

	//
	// Create a Buf to hold the reply from this invocation.
	// If the reply get's bigger than this, we can't free
	// the data safely with the current architecture.
	// The reply data from the handler will be marshaled
	// into this buffer
	//
	uchar_t	local_buf[max_local_size];
	nil_Buf	reply_buf(local_buf, (unsigned)max_local_size, 0, Buf::STACK);

	// XX We need the equivalent of the orphan message checks in rxdoor.cc

	obj->incoming_prepare(e);

#if (SOL_VERSION >= __s10)
	if (e.exception() != NULL || bad_invo) {

		if (bad_invo) {
			if (e.exception()) {
				e.clear();
			} else {
				obj->incoming_done();
			}
			e.system_exception(CORBA::BAD_PARAM(EPERM,
			    CORBA::COMPLETED_NO));
		}
#else
	if (e.exception() != NULL) {
#endif // SOL_VERSION >= __s10

		//
		// If there is an exception we marshal the exception
		// into the reply_buf and return
		//

		// Close incoming descriptors that we did not process
		sxdoor_manager::the().translate_in_cancel(ndoors, doors);

		// Use a separate Environment for marshalling reply
		Environment env;

		// Create a marshalstream primed with the reply_buf, so that
		// the data is marshalled on the stack
		MarshalStream ms(&reply_buf, &env);

#if defined(_FAULT_INJECTION)
		(void) ms.alloc_chunk(InvoTriggers::header_size());
#endif

		// Marshal the system exception, cannot be user exception here
		ASSERT(e.sys_exception() != NULL);
		e.sys_exception()->_put(ms);

#if defined(_FAULT_INJECTION)
		// Marshal Invo triggers for K->U or U->U reply.
		InvoTriggers::put(ms);
#endif
		// Get the span of the marshalled data and check it fit
		uint_t ex_span = ms.span();
		ASSERT(ex_span <= max_local_size);

		// Verify that the Marshalstream has only 1 Buf and
		// that Buf is the same Buf we supplied
		ASSERT((ms.region().reap() == &reply_buf) && ms.empty());
		ASSERT(reply_buf.head() == local_buf);

		(void) door_return((char *)local_buf, ex_span, NULL, 0);

		// We shouldn't be here but it might happen.
		// See bug 4245337.

		//
		// SCMSGS
		// @explanation
		// An unusual but harmless event occurred. System operations
		// continue unaffected.
		// @user_action
		// No user action is required.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "clcomm: solaris xdoor: rejected invo: door_return"
		    " returned, errno = %d", errno);
		return;
	}

	// Specify resources required for message.
	// The invocation mode defaults to twoway.
	resources	resource(resources::NO_FLOW_CONTROL, &e,
			    invocation_mode::NONE, REPLY_MSG);
	uint_t		data_buf_size = (unsigned)max_local_size;
#if defined(_FAULT_INJECTION)
	resource.add_send_header(InvoTriggers::header_size());
	data_buf_size -= InvoTriggers::header_size();
#endif
	resource.add_send_data(data_buf_size);
	sdoor_sendstream send_stream(Buf::STACK, &reply_buf, &resource);

	nil_Buf	request_buf((uchar_t *)data, (uint_t)data_size,
	    (uint_t)data_size, Buf::STACK);

	sdoor_recstream	rs(Buf::STACK, INVO_MSG);

	// Create service with local sendstream references
	service	s(&rs, &send_stream, &e);

	// Initialize a service object with arguments
	obj->prepare_receive(s, &request_buf, doors, ndoors);

	// Call the handler function for processing
	obj->get_user_handler()->handle_incoming_call(s);

	// There should be no exceptions returned by handle_incoming_call
	// See comment in rxdoor.cc if this ASSERT fails
	ASSERT(!s.get_env()->exception());

	door_arg_t	da;

	if (send_stream.get_invo_mode().is_oneway()) {
		da.data_size = da.desc_num = 0;
	} else {
#if defined(_FAULT_INJECTION)
		// Marshal Invo triggers for K->U or U->U reply.
		InvoTriggers::put(&send_stream);
#endif
		// Prepare the response
		obj->prepare_reply(s, da);
	}

	// Reclaim buffers.
	// All our reply data better be on our stack (local_buf) at this point.
	// XXX This is not needed if service is considered going out of scope.
	s.set_sendstream(NULL);

	obj->incoming_done();

	// Return.
	(void) door_return(da.data_ptr, da.data_size, da.desc_ptr,
	    da.desc_num);

	//
	// We really shouldn't get here but we might under certain conditions.
	// See bug 4245337. It's safe to do a return here.
	//

	//
	// SCMSGS
	// @explanation
	// An unusual but harmless event occurred. System operations continue
	// unaffected.
	// @user_action
	// No user action is required.
	//
	(void) orb_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "clcomm: solaris xdoor: completed invo: door_return"
	    " returned, errno = %d", errno);
}

void
solaris_xdoor_server::unreferenced_upcall()
{
	os::mutex_t *mp = &get_lock();
	mp->lock();
	// A pending revoke must be processed before pending unref
	if (flags & REVOKE_PENDING) {
		cv.broadcast();
	} else {
		handle_possible_unreferenced();
	}
	mp->unlock();
}

void
solaris_xdoor_server::incoming_prepare(Environment &e)
{
	lock();
	CL_PANIC(!door_is_unref());
	if (flags & (REVOKED | REVOKE_PENDING)) {
		SOLXDR_DBG(("solaris_xdoor received incoming invocation in bad "
			"state %d\n", flags));
		e.system_exception(CORBA::INV_OBJREF(ECANCELED,
			CORBA::COMPLETED_NO));
	} else {
		CL_PANIC((flags & UNREFERENCED) == 0);
		++invos_in_progress;
		ASSERT(invos_in_progress != 0);
	}
	unlock();
}

void
solaris_xdoor_server::incoming_done()
{
	os::mutex_t *mp = &get_lock();

	mp->lock();
	ASSERT(invos_in_progress > 0);
	if (--invos_in_progress == 0) {
		// A pending revoke must be procecessed before pending unref
		if (flags & REVOKE_PENDING) {
			cv.broadcast();
		} else {
			handle_possible_unreferenced();
		}
	}
	mp->unlock();
}

// Revoke the door
void
solaris_xdoor_server::revoked()
{
	os::mutex_t *mp = &get_lock();

	mp->lock();
	ASSERT(!(flags & (REVOKED | REVOKE_PENDING)));
	flags |= REVOKE_PENDING;

	// Wait till all pending invos_in_progress are complete
	while (invos_in_progress != 0) {
		cv.wait(mp);
	}

	ASSERT((flags & REVOKE_PENDING));
	ASSERT(!(flags & REVOKED));

	flags = (flags | REVOKED) & ~REVOKE_PENDING;

	//
	// Handler increments marshcount to prevent handler from getting
	// unreferenced and disconnecting xdoor
	// We do an implicit marshal_cancel() to account for this
	//
	mc_handler++;
	ASSERT(mc_handler != 0);

	handle_possible_unreferenced();

	mp->unlock();
}

// Standard handler creates with unref_flag == true;
// Simple handler creates with unref_flag == false
//
Xdoor *
solaris_xdoor_server::create(handler *h, bool unref_flag)
{
	solaris_xdoor_server *xdp = new solaris_xdoor_server(h, unref_flag);
	door_id_t did = xdp->get_id();

	sxdoor_table::the().register_door(xdp, did, false);
	return (xdp);
}

//
// send_reply - handles any pending exception.
//	Transmission to another node does not occur at this level.
//
void
solaris_xdoor_server::send_reply(service &serv)
{
	// Solaris xdoors always use sdoor_sendstream
	sdoor_sendstream	*sendstreamp = (sdoor_sendstream *)(serv.se);

	Environment	*e = serv.get_env();
	if (e->exception()) {
		// Discard all sendstream data because of exception
		sendstreamp->exception_prepare();

		// Load exception information
		e->exception()->_put(serv);
		e->clear();
		return;
	}
	//
	// Need to do marshals here in case the last reference to an object
	// is being passed away.  Later the data can be translated to door
	// argument form.
	//
	uint_t	ndoors;
	if ((ndoors = sendstreamp->get_xdoorcount()) != 0) {
		sxdoor_manager::the().translate_out(ndoors,
		    sendstreamp->get_XdoorTab(), e);
	}
}

//
// Initialize the recstream based on the data passed with the door invocation
// XX This should all be done in sdoor_recstream constructor
//
void
solaris_xdoor_server::prepare_receive(
	service		&s,
	Buf		*b,
	door_desc_t	*dp,
	uint_t 		ndoors)
{
	s.re->get_MainBuf().usebuf(b);

	//
	// Translate incoming door arguments
	// XX we are not setting xdoorcount in sdoor_recstream
	//
	sxdoor_manager::the().translate_in(s.re->get_XdoorTab(), ndoors, dp,
	    s.get_env());
}

//
// Convert a sendstream into door return args.
//
void
solaris_xdoor_server::prepare_reply(service &inv, door_arg_t &da)
{
	sendstream *se = inv.se;
	Buf		*bp;
	char		*bptr;
	size_t		r_bsize;	// Rounded up data size (bytes)

	Region		&region = se->get_MainBuf().region();

	// Make sure our response will fit in the (single) local buffer
	r_bsize = roundup(region.span(), sizeof (door_desc_t));

	if (r_bsize + (se->get_xdoorcount() * sizeof (door_desc_t)) >
	    max_local_size) {
		//
		// SCMSGS
		// @explanation
		// The reply from a user level server will not fit in the
		// available space.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor: too much reply data");
	}

	// There is only 1 buf in this reply Region!
	bp = region.buffer();
	bptr = (char *)bp->head();

	// There is always some data to return (ExceptionStatus at least)
	da.data_ptr = (char *)bp->head();
	da.data_size = bp->span();
	ASSERT(da.data_size == region.span());

	//
	// Copy reply objects from XdoorTab to our stack based
	// buffer. Could probably do this better if we marshaled
	// directly into the MainBuf...
	//
	if ((da.desc_num = se->get_xdoorcount()) != 0) {
		// Start of any file descriptors (doors) we will be passing
		da.desc_ptr = (door_desc_t *)&bptr[r_bsize];

		ASSERT(inv.get_env()->exception() == NULL);
		// transfer data from marshal stream to door args on stack
		se->get_XdoorTab().region().xfer_contents((char *)da.desc_ptr,
		    da.desc_num * (unsigned)sizeof (door_desc_t),
		    inv.get_env());
		// XX If there can be an exception here, we should handle it
		ASSERT(inv.get_env()->exception() == NULL);
	} else {
		da.desc_ptr = NULL;
	}
}

//
// solaris_xdoor_client methods
//

//
// Since the server could be anywhere except this node, return unknown.
//
nodeid_t
solaris_xdoor_client::get_server_nodeid()
{
	return (NODEID_UNKNOWN);
}

void
solaris_xdoor_client::marshal_cancel()
{
}

void
solaris_xdoor_client::handle_possible_unreferenced()
{
	ASSERT(lock_held());
	if ((handler_ == NULL) && (umarshout == 0)) {
		sxdoor_table::the().remove_door(this, get_id());
	}
}

solaris_xdoor_client::solaris_xdoor_client(handler *h, int d) :
	solaris_xdoor(&xdoor_lock, h)
{
	door_info_t	info;

	if (door_info(d, &info) < 0) {
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: solaris xdoor door_info failed");
	}
	set_fd(d);		// initialize door id
	set_id(info.di_uniquifier);
}

//
// Create a Xdoor associated with this Solaris door
solaris_xdoor *
solaris_xdoor_client::create(handler *h, int d)
{
	solaris_xdoor_client *xdp = new solaris_xdoor_client(h, d);
	door_id_t did = xdp->get_id();

	sxdoor_table::the().register_door(xdp, did, true);
	return (xdp);
}

//
// Perform object invocation.
// Invocation object contains marshalled data at this point
//
ExceptionStatus
solaris_xdoor_client::request_twoway(invocation &inv)
{
	char		*rec_ptr;
	door_arg_t	da;
	Environment	*e = inv.get_env();

	// before the invocation specific processing begins increment
	// the invocation count. even though inter-domain invocation is
	// done by door call, invocation logically starts here because
	// prepare_send marshals solaris_xdoors, and would have incremented
	// marshal counts for xdoor in the invocation. These will get reset
	// once the invocation completes, therefore invo_count is
	// is incremented before prepare_send so that fork()/fork1() call
	// can be blocked if any invocation is in progress.
	//
	solaris_xdoor_client::invo_lck.lock();
	++invo_count;
	solaris_xdoor_client::invo_lck.unlock();

	// Transform outgoing invocation data into doors arguments
	prepare_send(inv, da);

	if (e->exception() != NULL) {
		ASSERT(e->sys_exception() != NULL);
		e->sys_exception()->completed(CORBA::COMPLETED_NO);
		inv.set_recstream(NULL);

		// decrement the invocation count.
		solaris_xdoor_client::invo_lck.lock();
		if (--invo_count == 0) {
			invo_cv.broadcast(); // wake-up fork call.
		}
		solaris_xdoor_client::invo_lck.unlock();

		return (CORBA::LOCAL_EXCEPTION);
	}

	rec_ptr = da.rbuf;	// Used to detect overflows

	// Mask the signals before making the door call.
	sigset_t new_mask, orig_mask;
	(void) sigfillset(&new_mask);
	(void) thr_sigsetmask(SIG_SETMASK, &new_mask, &orig_mask);

	// Now perform invocation. This results in call to the
	// xdoor_proxy funtion defined in gateway.cc.

	int result = door_call(get_fd(), &da);
	int errornumber = errno;

	// Restore the thread signal property.
	(void) thr_sigsetmask(SIG_SETMASK, &orig_mask, NULL);

	if (result < 0) {

		e->system_exception(CORBA::INV_OBJREF((uint_t)errornumber,
		    CORBA::COMPLETED_MAYBE));
		inv.set_recstream(NULL);

		// decrement the invocation count.
		solaris_xdoor_client::invo_lck.lock();
		if (--invo_count == 0) {
			invo_cv.broadcast(); // wake-up fork call.
		}
		solaris_xdoor_client::invo_lck.unlock();

		// System exception (no unmarshal)
		return (CORBA::LOCAL_EXCEPTION);
	}

	// decrement the invocation count.
	solaris_xdoor_client::invo_lck.lock();
	if (--invo_count == 0) {
		invo_cv.broadcast(); // wake-up fork call.

	}
	solaris_xdoor_client::invo_lck.unlock();

	// Now stick the results back in the marshal stream
	prepare_return(inv,  da, (char *)rec_ptr);
	if (e->exception() != NULL) {
		ASSERT(e->sys_exception() != NULL);
		e->sys_exception()->completed(CORBA::COMPLETED_YES);
		inv.set_recstream(NULL);
		return (CORBA::LOCAL_EXCEPTION);
	}

	// Exceptions from gateway/remote node if any are already marshalled
	// in recstream
	return (CORBA::NO_EXCEPTION);
}

//
// Convert a sendstream into door args.
//
// Prepare the recstream for return data. The main job is to make sure
// that the MainBuf data fits in one contigous data buffer and ditto for
// the Xdoor data.
//
void
solaris_xdoor_client::prepare_send(invocation &inv, door_arg_t &da)
{
	sendstream *se = inv.se;
	char 	*result_buf = NULL;
	size_t	result_size = 0;

	// Create a recstream for the results
	inv.set_recstream(new sdoor_recstream(Buf::HEAP, REPLY_MSG));
	recstream *re = inv.re;

	//
	// Marshal door_msg_hdr (contains zone_id & zone_uniqid)
	//
	door_msg_hdr_t *door_hdrp;
#if (SOL_VERSION >= __s10)
	zoneid_t zoneid = getzoneid();
	if (zoneid != 0) {
		//
		// If the invocation is coming from a non-global zone
		// then we need to add the cluster id information to the
		// environment here. The default cluster id in the environment
		// is 0, the cluster id of the global zone.
		//
		cz_id_t	czid;
		int	ret;
		czid.clid = 0;
		if (getzonenamebyid(zoneid, czid.name, ZONENAME_MAX) == -1) {
			//
			// SCMSGS
			// @explanation
			// The getzonenamebyid(3C) operation failed.
			// The man page for getzonenamebyid
			// describes possible error codes.
			// @user_action
			// Contact your authorized Sun service
			// provider to determine whether a workaround
			// or patch is available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC,
			    MESSAGE, "clcomm: getzonenamebyid "
			    "failed: %s", strerror(errno));
			CL_PANIC(0);
		}
		if ((ret = os::cladm(CL_CONFIG, CL_GET_ZC_ID, &czid))
		    != 0) {
			//
			// SCMSGS
			// @explanation
			// Failed to get the cluster id for a zone.
			// @user_action
			// Contact your authorized Sun service
			// provider to determine whether a workaround
			// or patch is available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC,
			    MESSAGE, "Failed to get cluster id, ret val = "
			    "%d\n", ret);
			CL_PANIC(0);
		}
		door_hdrp = new door_msg_hdr_t(zoneid, INVO_MSG, czid.clid);
	} else {
		door_hdrp = new door_msg_hdr_t(zoneid, INVO_MSG);
	}
#else  // (SOL_VERSION >= __s10)
	door_hdrp = new door_msg_hdr_t(INVO_MSG);
#endif // (SOL_VERSION >= __s10)
	ASSERT(door_hdrp);
	se->write_header((void *)door_hdrp, sizeof (door_msg_hdr_t));
	delete door_hdrp;

#if defined(_FAULT_INJECTION)
	// Marshal Invo triggers for U->K or U->U invocation path.
	InvoTriggers::put(se);
#endif

	// gather all the data from the list of Buf's and copy to one buffer
	Buf *bp = se->get_MainBuf().coalesce_region().reap();

	if (bp) {
		da.data_ptr = (char *)bp->head();	// Args are here
		da.data_size = bp->span();
		result_buf = (char *)bp->head();
		result_size = bp->span() + bp->avail();

		//
		// Reattach. We may re-use this Buf for return data
		//
		re->get_MainBuf().usebuf(bp);
	} else {
		da.data_ptr = NULL;
		da.data_size = 0;
	}


	// Create descriptor arguments
	if ((da.desc_num = se->get_xdoorcount()) != 0) {
		// translate xdoors to door descriptors
		sxdoor_manager::the().translate_out(da.desc_num,
		    se->get_XdoorTab(), inv.get_env());

		// coalesce data into one buffer
		da.desc_ptr = (door_desc_t *)
		    se->get_XdoorTab().coalesce_region().buffer()->head();
	} else {
		da.desc_ptr = NULL;
	}

	//
	// Find a place for the results.
	//	XXX It would be nice to know how much (if any) data we
	//	are expecting.
	//
	// An allocation here is better than having a kernel doors
	// overflow.
	//
	// The new buf can't just replace the old one in the recstream,
	// since the old one needs to be kept around until the data is
	// copied by the door_call.  Instead, prepend the new buf since
	// that's where the reply data will go unless there's an overflow.
	//
	if (result_size < min_reply_size) {
		Buf *bufp = new GWBuf((unsigned)min_reply_size);

		result_buf = (char *)bufp->head();
		result_size = min_reply_size;
		re->get_MainBuf().region().prepend(bufp);
		re->prepare();
	}
	da.rbuf = result_buf;
	da.rsize = result_size;
}

// Convert door results into a recstream.
// Return the results from the header in the reply buffer
void
solaris_xdoor_client::prepare_return(
	invocation	&inv,
	door_arg_t	&da,
	char		*rec_buf)
{
	recstream *re = inv.re;
	Buf *bp = NULL;

	if (da.data_ptr != rec_buf) {
		// Overflow occurred on return.
		// The current receive buffer will not be used so
		// dispose of it. Create a new Buf to track the
		// overflow area.
		bp = new DoorOverflowBuf((uchar_t *)da.data_ptr, da.data_size);
		bp->incspan((int)da.data_size);
	} else {
		//
		// Reset the size of the buffer to select the return args
		//
		bp = re->get_MainBuf().region().reap();
		ASSERT(da.data_ptr == (char *)bp->head());
		ssize_t olen = (ssize_t)bp->span();
		bp->incspan((int)((ssize_t)da.data_size - olen));
	}
	re->get_MainBuf().dispose();	// Dispose any additional buffers
	re->get_MainBuf().usebuf(bp);	// Reattach
	re->prepare();

#if defined(_FAULT_INJECTION)
	// Unmarshal Invo triggers from U->K or U->U invocation path.
	InvoTriggers::get(re);
#endif

	//
	// Translate incoming door objects
	// XX We are not setting the recstream's xdoorcount
	//
	sxdoor_manager::the().translate_in(re->get_XdoorTab(), da.desc_num,
	    da.desc_ptr, inv.get_env());
}
