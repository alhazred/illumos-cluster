//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _RXDOOR_MGR_H
#define	_RXDOOR_MGR_H

#pragma ident	"@(#)rxdoor_mgr.h	1.8	08/05/20 SMI"

#include <orb/xdoor/Xdoor.h>
#include <orb/xdoor/xdoor_kit.h>
#include <orb/xdoor/rxdoor.h>

//
// For each node we have a list of rxdoor_nodes.  Most nodes will have
// a single incarnation number, but we may have references to nodes
// that have died and restarted (these references must be managed
// correctly, since the client may need to perform equiv() and other
// operations on them).
//
// The incarnation number of zero is special.
// The rxdoor_node for local servers and for fixed rxdoors (incarnation number
// of zero) are allocated at startup.
//
// rxdoor_nodes are deleted when all of their xdoors are removed, so
// these lists should be short. However, have seen lists with over 3 dozen
// entries.
//
class rxdoor_hash_table {
	friend class rxdoor_manager;
public:
	rxdoor_hash_table();
	~rxdoor_hash_table();

	void		store(ID_node &node, rxdoor *xdp);

	rxdoor_node 	*get_rxdoor_node(ID_node &idx);
	rxdoor_node 	*create_rxdoor_node(ID_node &idx);

	//
	// wrapper on ID_node::match(). Return value is
	// the opposite of ID_node::match().
	//
	static bool	check_idnode(rxdoor_node *rxnp, void *idnp);

	//
	// helper function to calculate number of hashlists
	// which is different in user and kernel
	//
	static uint_t 	default_num_hashlists();

#ifdef DEBUGGER_PRINT
	void 		mdb_dump_table();
#endif

private:
	rxdoor_node 	local_rx_node;
	rxdoor_node 	*fixed_rx_nodes[NODEID_MAX];

	DList<rxdoor_node> node_buckets[NODEID_MAX];

	// Disallow assignments and pass by value
	rxdoor_hash_table(const rxdoor_hash_table &);
	rxdoor_hash_table &operator = (rxdoor_hash_table &);
};

class callback_membership;

class rxdoor_manager : public notify_membership_change {
	friend class rxdoor_node;
	friend class rxdoor_kit;
	friend rxdoor_hash_table;
public:
	rxdoor_manager();

	static 		rxdoor_manager & the();
	static int 	initialize();
	static void 	shutdown();

	//
	// When the rxdoor manager is created, it doesn't know the local
	// incarnation number. This is called when it is set.
	//
	virtual void	update_current();

	// register a new xdoor.
	void		register_rxdoor(rxdoor *xdp);

	// register a new xdoor at a fixed local id.
	xdoor_id 	register_rxdoor(rxdoor *xdp, rxdoor_id rxdid);

	// release an xdoor registration.
	void		release_rxdoor(rxdoor *xdp);

	rxdoor		*lookup_local_rxdoor(xdoor_id xdid,
			    rxdoor_bucket **xbpp);

	rxdoor		*lookup_rxdoor(ID_node &node, xdoor_id xdid,
			    rxdoor_bucket **xbpp);

	//
	// Used during reconfiguration to rebuild the foreign reference count
	// of standard xdoors.
	//
	void		cleanup_foreignrefs(callback_membership *,
			    callback_membership *);

	// Used during reconfiguration to cleanup any empty rxdoor_node
	void		cleanup_resources();

#ifdef DEBUGGER_PRINT
	void 		mdb_dump_table();
#endif

private:
	// counter of next free xdoor number
	xdoor_id		max_xdid;

	// The main thing!
	rxdoor_hash_table	table;

	// Protects max_xdid
	os::mutex_t		max_lock;

	//
	// A write lock must be held when adding or removing
	// rxdoor_node's from the table.
	// Otherwise a read lock can be used when accessing the table.
	//
	os::rwlock_t		table_lock;

	static rxdoor_manager 	*the_rxdoor_manager;

	// Disallow assignments and pass by value
	rxdoor_manager(const rxdoor_manager &);
	rxdoor_manager &operator = (rxdoor_manager &);
};

#include <orb/xdoor/rxdoor_mgr_in.h>

#endif	/* _RXDOOR_MGR_H */
