/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SIMPLE_XDOOR_H
#define	_SIMPLE_XDOOR_H

#pragma ident	"@(#)simple_xdoor.h	1.85	08/05/20 SMI"

#include <orb/xdoor/rxdoor.h>
#include <orb/xdoor/rxdoor_mgr.h>
#include <orb/xdoor/rxdoor_kit.h>

class simple_xdoor_kit {
public:

	void	unmarshal_cancel_remainder(ID_node &, rxdoor_id &,
		    MarshalStream &);
};


class simple_xdoor_to_client_kit :
	public simple_xdoor_kit,
	public rxdoor_to_client_kit
{
public:
	simple_xdoor_to_client_kit();
	virtual rxdoor *create(rxdoor_descriptor &);
};

class simple_xdoor_from_server_kit :
	public simple_xdoor_kit,
	public rxdoor_from_server_kit
{
public:
	simple_xdoor_from_server_kit();
	virtual rxdoor *create(rxdoor_descriptor &);
};

class simple_xdoor_to_server_kit :
	public simple_xdoor_kit,
	public rxdoor_to_server_kit
{
public:
	simple_xdoor_to_server_kit();
	virtual rxdoor *create(rxdoor_descriptor &);
};

//
// simple_xdoor -
//	A simple_xdoor is an xdoor which does not keep a reference count.
//
// There are two types of simple_xdoors,
// which are distinguished by the node incarnation number:
//	A) incarnation number == INCN_UNKNOWN
//		represents an xdoor that is valid with any incarnation number
//		on remote invocations.
//	B) normal incarnation number
//		represents an xdoor with the normal semantics.
//		The xdoor is valid as long as the incarnation number
//		is valid.
//
class simple_xdoor :
	public rxdoor,
	public knewdel
{
public:
	// This constructor is used for servers
	simple_xdoor(handler *);

	// This constructor is used for clients
	simple_xdoor(xdoor_id);

	virtual ~simple_xdoor();

	bool	reference_counting();

	// Common implementation for _server and _client
	void	unmarshal_roll_back();
	void	marshal_roll_back(ID_node &, MarshalStream &);
	void	marshal_cancel();
	virtual void	marshal_received();
	void	cleanup_foreignrefs(const nodeset &);

protected:

private:
	// Disallow assignments and pass by value
	simple_xdoor(const simple_xdoor &);
	simple_xdoor &operator = (simple_xdoor &);
};

//
// simple_xdoor_server - A simple_xdoor_server cannot get deleted.
//			The server xdoor is immortal.
//
class simple_xdoor_server: public simple_xdoor {
public:
	static simple_xdoor_from_server_kit from_server_kit;

	static Xdoor *create(handler *);
	static Xdoor *create(handler *, xdoor_id);

	simple_xdoor_server(handler *);

	// Simple xdoor servers do not get deleted in our current implementation
	~simple_xdoor_server();

	void	marshal(ID_node &, MarshalStream &, Environment *);
	void	unmarshal(ID_node &, MarshalStream &, bool, Environment *);
	void	incoming_prepare(Environment *);
	void	incoming_done();
	void	revoked();

	//
	// Identify resources needed at xdoor level for request
	// This method is used for invocation messages and not reply messages.
	//
	sendstream	*reserve_resources(resources *resourcep);

	// Identify resources needed at rxdoor level for invocation reply
	sendstream	*reserve_reply_resources(resources *resourcep);

	bool	local();

	void	marshal_local(Environment *);

	virtual const char	*xdoor_type();

	virtual rxdoor_kit	&get_kit();

private:
	void	dec_marshcount(uint_t);
	void	handle_possible_unreferenced();

	// Number of services currently in progress
	uint_t		services;

	// Identifies current rxdoor state
	ushort_t	flags;

	// Used for revoke.
	os::condvar_t	revoke_cv;

	static const char	*xdoor_type_string;

	// Disallow assignments and copy constructor
	simple_xdoor_server(const simple_xdoor_server &);
	simple_xdoor_server &operator = (simple_xdoor_server &);
};

//
// simple_xdoor_client - these client xdoors exist only as long as at least
//			one handler needs it.
//
class simple_xdoor_client: public simple_xdoor {
public:
	static simple_xdoor_to_server_kit to_server_kit;
	static simple_xdoor_to_client_kit to_client_kit;

	static rxdoor	*create(rxdoor_descriptor &);
	static Xdoor	*create(nodeid_t, xdoor_id, handler *);

	// used when unmarshaling a new xdoor.
	simple_xdoor_client(rxdoor_id &rx);

	~simple_xdoor_client();

	void	marshal(ID_node &, MarshalStream &, Environment *);
	virtual void	marshal_received();
	void	unmarshal(ID_node &, MarshalStream &, bool, Environment *);
	void	marshal_local(Environment *);

	//
	// Identify resources needed at xdoor level for request
	// This method is used for invocation messages and not reply messages.
	//
	sendstream	*reserve_resources(resources *resourcep);

	bool		local();

	virtual const char	*xdoor_type();

	virtual rxdoor_kit	&get_kit();

	// Number of foreign references
	uint_t		foreignrefs;

private:
	void	dec_marshcount(uint_t);
	void	handle_possible_unreferenced();

	static const char	*xdoor_type_string;

	// Disallow assignments and pass by value
	simple_xdoor_client(const simple_xdoor_client &);
	simple_xdoor_client &operator = (simple_xdoor_client &);
};

#include <orb/xdoor/simple_xdoor_in.h>

#endif	/* _SIMPLE_XDOOR_H */
