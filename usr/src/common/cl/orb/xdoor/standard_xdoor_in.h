/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  standard_xdoor_in.h
 *
 */

#ifndef _STANDARD_XDOOR_IN_H
#define	_STANDARD_XDOOR_IN_H

#pragma ident	"@(#)standard_xdoor_in.h	1.36	08/05/20 SMI"

inline
standard_xdoor_to_client_kit::standard_xdoor_to_client_kit() :
	standard_xdoor_kit(),
	rxdoor_to_client_kit(STDXD_TO_CLIENT_XKID, rxdoor_id::_sizeof_marshal())
{
}

inline
standard_xdoor_to_server_kit::standard_xdoor_to_server_kit() :
	standard_xdoor_kit(),
	rxdoor_to_server_kit(STDXD_TO_SERVER_XKID,
	    rxdoor_descriptor::_sizeof_marshal())
{
}

inline
standard_xdoor_from_server_kit::standard_xdoor_from_server_kit() :
    rxdoor_from_server_kit(STDXD_FROM_SERVER_XKID,
	rxdoor_descriptor::_sizeof_marshal())
{
}

//
// standard_xdoor methods
//

//
// constructor for server standard xdoors
//
inline
standard_xdoor::standard_xdoor(handler *h) :
	rxdoor(h),
	foreignrefs(0)
{
}

//
// constructor for client standard xdoors
//
inline
standard_xdoor::standard_xdoor(xdoor_id xdn) :
	rxdoor(xdn),
	foreignrefs(0)
{
}

inline
standard_xdoor::~standard_xdoor()
{
}

//
//
// standard_xdoor_server methods
//

inline
standard_xdoor_server::standard_xdoor_server(handler *h) :
	standard_xdoor(h),
	marshalsmade(0),
	services(0),
	flags(0)
{
}

inline
standard_xdoor_server::standard_xdoor_server(xdoor_id id) :
	standard_xdoor(id),
	marshalsmade(0),
	services(0),
	flags(0)
{
}

inline
standard_xdoor_server::~standard_xdoor_server()
{
}

//
// refmasks_empty
//
// Return true if refmask and extra_mask are empty.
//
inline bool
standard_xdoor_server::refmasks_empty()
{
	ASSERT(extramask.bitmask() ==
	    (extramask.bitmask() & refmask.bitmask()));
	if (refmask.is_empty()) {
		ASSERT(extramask.is_empty());
		return (true);
	}
	return (false);
}


//
// standard_xdoor_client methods
//

inline
standard_xdoor_client::standard_xdoor_client(rxdoor_id &rx) :
	standard_xdoor(rx.xdid)
{
}

inline
standard_xdoor_client::~standard_xdoor_client()
{
}

#endif	/* _STANDARD_XDOOR_IN_H */
