/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  rxdoor_kit_in.h
 *
 */

#ifndef _RXDOOR_KIT_IN_H
#define	_RXDOOR_KIT_IN_H

#pragma ident	"@(#)rxdoor_kit_in.h	1.4	08/05/20 SMI"

inline
rxdoor_kit::rxdoor_kit(const xkit_id_t kid, uint_t ml) :
	xdoor_kit(kid, ml)
{
}

inline
rxdoor_to_client_kit::rxdoor_to_client_kit(const xkit_id_t kid, uint_t ml) :
    rxdoor_kit(kid, ml)
{
}

inline
rxdoor_to_server_kit::rxdoor_to_server_kit(const xkit_id_t kid, uint_t ml) :
    rxdoor_kit(kid, ml)
{
}

inline
rxdoor_from_server_kit::rxdoor_from_server_kit(const xkit_id_t kid, uint_t ml) :
    rxdoor_kit(kid, ml)
{
}

#endif	/* _RXDOOR_KIT_IN_H */
