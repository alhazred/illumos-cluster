//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident    "@(#)members.cc 1.28     08/05/20 SMI"

#include <sys/os.h>
#include <orb/member/members.h>
#include <orb/xdoor/rxdoor.h>

ID_node invalid_node;

members the_members;

//
// constructor - initialize to no nodes in the cluster.
//
members::members()
{
	for (nodeid_t i = 0; i <= NODEID_MAX; i++) {
		the_membership.members[i] = INCN_UNKNOWN;
	}
}

//
// set_membership - update the published cluster membership.
//
// The members are assigned one word at a time to ensure that the
// update is atomic per member. We avoid any possibility of byte copies.
//
void
members::set_membership(const cmm::membership_t & membership)
{
	rxdoor_membership_manager::the().block_translate_in();
	for (nodeid_t i = 0; i <= NODEID_MAX; i++) {
		the_membership.members[i] = membership.members[i];
	}
	rxdoor_membership_manager::the().update_current();

	rxdoor_membership_manager::the().unblock_translate_in();
}
