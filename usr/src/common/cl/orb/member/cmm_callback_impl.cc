/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cmm_callback_impl.cc	1.181	08/05/20 SMI"

//
// This file contains the implementation of the cmm callback step
// infrastructure
//

#include <sys/clconf_int.h>
#include <sys/os.h>
#include <orb/member/cmm_callback_impl.h>
#include <cmm/cmm_ns.h>
#include <orb/member/members.h>
#include <orb/member/Node.h>
#include <orb/object/adapter.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/transport/path_manager.h>
#ifdef _KERNEL
#include <sys/callb.h>
#endif
#include <cmm/cmm_impl.h>
#include <orb/fault/fault_injection.h>

// Time for which shutdown goes to sleep after calling cmm's stop transition.
// This sleep is necessary to allow the CMM sender thread a chance to run
// and broadcast its state to the other nodes.
#define	SHUTDOWN_CMM_LAG 200		// in ms

//
// The callback implementation attempts to provide a predictable interface
// between the callbacks from the CMM itself and the steps that need to be
// taken during reconfiguration.  There are two kinds of steps...those that
// can block with remote invocations or messages and those that cannot.  The
// first step is of the latter kind, and is responsible for unblocking remote
// invocations, which demonstrates the first tricky part of this
// implementation...the first step must be run to unblock the potentially
// hung subsequent steps.  We do this by running any step that can block on
// a remote invocation in a different thread, to which we hand off tasks and
// wait for their completion.  If the step is canceled for some reason, or
// if the step exceeds it's allowed execution time, the cmm is allowed to
// reconfigure and run step 1, vm_bootstrap_calc, again.
// Steps running in a different thread
// must be written with care to ensure they do not interfere with possibly
// overlapping, previous steps (running in a subsequent reconfiguration if
// the thread step is canceled) and should try to detect cancelation as
// early as possible.
//
// The non-defered step is called a cmm_step_state.  The following guarantees
// are made on these kinds of steps:
//
// 1)	If step_trans(n) is called from the cmm, n->init(), n->transition(),
//	and n->fini() are guaranteed to be called.
// 2)	A step may be canceled during n->transition().  The step will be
//	notified that it has been canceled if it calls step_blocking() on the
//	callback_impl (it must subsequently call step_not_blocking()).  The
//	step can take any action it wishes to when it is canceled, including
//	ignoring the fact, but it must not block indefinitely or the node
//	will crash because the cmm will not be able to reconfigure.
// 3)	If a step is canceled, after the call to n->fini() there will be a
//	call to n->to_begin().  If it is not canceled, the next call will be
//	to n+1->init().
//
// Steps that run in different threads are called cmm_step_thread_states, and
// have slightly different rules:
//
// 1)	If step_trans(n) is called from the cmm, n->thread_init(),
//	n->thread_transition(), and n->thread_fini() are guaranteed to be
//	called.
// 2)	A step may be canceled during n->thread_transition().  It can check
//	for canceled status by calling canceled().  A step should complete
//	in the time allotted for it barring changes in cluster membership.
//	It is nice if the steps make a good-faith effort to notice canceled
//	status as early as possible, especially before (directly or indirectly)
//	making remote invocations.
// 3)	If a step is canceled, there will be a call to n->to_begin() after the
//	step is notified that it has been canceled.  to_begin() can run
//	concurrently with thread_init(), _transition(), or _fini(), so care must
//	be taken in what locks are grabbed by what functions.
//
// The init() and fini() calls are provided to ensure that the membership,
// timeout, and sequence number values are correct for the running step.  If
// you need to overload them, make sure you make the parent's call at some
// point.  (You should be able to put your specialized code at the start or
// the end of the transition call, however, so this should not be necessary).
//
// To guarantee that the thread of a cmm_step_thread_state is available to
// run its first step, the previous step must wait for any lingering step
// from a previous reconfiguration to complete.  (If we waited in the step
// that uses the thread, we might time out before being able to run the
// transition, violating guarantee 1) above.)

cmm_callback_impl *cmm_callback_impl::the_cmm_callback_implp = NULL;

cmm_step_thread *cmm_step_thread_state::the_step_threads[] = { 0 };

cmm_state *cmm_state::the_cmm_states[] = { 0 };

#ifdef	CMM_VERSION_0
cmm_state *cmm_state::the_cmm_states_v0[] = { 0 };

cmm_state **cmm_state::curr_cmm_states = NULL;

uint_t	cmm_state::num_orb_steps = NUM_ORB_STEPS;

#endif // CMM_VERSION_0

void (*dsm_cmm_callback_ptr)(void);
void (*dsm_fence_nodes_ptr)(const char*);
void (*dsm_release_scsi2_ptr)(const char*);
void (*dsm_wait_for_release_ptr)(void);
void (*dsm_acquire_fence_lock_ptr)(const char *);
incarnation_num last_fencing[NODEID_MAX + 1];

#ifdef CMM_VERSION_0

extern void (*select_states_v0_funcp)();
extern void (*select_states_v1_funcp)();

#endif // CMM_VERSION_0

cmm_callback_impl::cmm_callback_impl() :
	statep(NULL),
	_membership(lone_membership::the()),
	seqnum(0),
	joined(false),
	aborted(false),
	stopped(false),
	_step_blocking(false),
	last_canceled_sequence(0)
{
	_membership->hold();
	dsm_cmm_callback_ptr = NULL;
	dsm_fence_nodes_ptr = NULL;
	dsm_release_scsi2_ptr = NULL;
	dsm_wait_for_release_ptr = NULL;
	dsm_acquire_fence_lock_ptr = NULL;
	for (nodeid_t i = 1; i <= NODEID_MAX; i++)
		last_fencing[i] = INCN_UNKNOWN;
}

cmm_callback_impl::~cmm_callback_impl()
{
	lock();
	ASSERT(_membership != NULL);
	_membership->rele();
	_membership = NULL;
	statep = NULL;
	the_cmm_callback_implp = NULL;
}

int
cmm_callback_impl::initialize()
{
	ASSERT(the_cmm_callback_implp == NULL);

	the_cmm_callback_implp = new cmm_callback_impl;

	the_cmm_callback_implp->lock();

	int err = 0;

	if ((err = cmm_step_thread_state::
		initialize_threads(the_cmm_callback_implp)) != 0) {
		goto got_error;
	}

	if ((err = cmm_state::initialize_states()) != 0) {
		goto got_error;
	}

#ifdef CMM_VERSION_0

	// Select the default (new) set of states.
	cmm_state::select_states_v1();

#endif // CMM_VERSION_0

got_error:
	if (err != 0) {
		the_cmm_callback_implp->unlock();
		return (err);
	}

	the_cmm_callback_implp->statep = cmm_state::the(0); // cmm_begin_state

	the_cmm_callback_implp->unlock();

	Environment e;
	cmm::callback_info cbinfo;

	cbinfo.num_steps = NUM_ORB_STEPS;

	clconf_cluster_t *cl = clconf_cluster_get_current();
	cbinfo.stop_timeout = clconf_cluster_get_orb_stop_timeout(cl);
	cmm::timeout_t step_timeout = clconf_cluster_get_orb_step_timeout(cl);
	cbinfo.return_timeout = clconf_cluster_get_orb_return_timeout(cl);
	cbinfo.abort_timeout = clconf_cluster_get_orb_abort_timeout(cl);
	clconf_obj_release((clconf_obj_t *)cl);

	cbinfo.step_timeouts.length(cbinfo.num_steps);
	for (uint_t i = 0; i < cbinfo.num_steps; i++)
		cbinfo.step_timeouts[i] = step_timeout;

	//
	// Infinite timeout for rm_service_start_step
	// because it waits for the following tasks to complete:
	// - become_primary which can take arbitrarily long
	// - rma_lca_register_step thread to become available
	//   (see cmm_callback_steps.cc)
	// Note that step_timeouts array is indexed from 0 to (stepnum-1).
	// For example: step_timeouts[0] corresponds to the_cmm_states[1].
	//
	// Index to be adjusted whenever step list changes !
	// Index to be adjusted in callback_registry_impl::step_trans()
	// where there is a check for the old set of states being active.
	//
	cbinfo.step_timeouts[10] = 0;

	cmm::callback_var callbp_v = the_cmm_callback_implp->get_objref();
	cmm::callback_registry_var cbr_v = cmm_ns::get_callback_registry();
	cbr_v->add("orb", callbp_v, cbinfo, e);

#ifdef CMM_VERSION_0

	select_states_v0_funcp = cmm_state::select_states_v0;
	select_states_v1_funcp = cmm_state::select_states_v1;

#endif // CMM_VERSION_0

	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The system can not continue when callback registration
		// fails.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: join_cluster: failed to register ORB callbacks "
		    "with CMM.");
	}

#ifdef _KERNEL
	//
	// lint seems to think callb_add is not a function
	// It is probably confused by the declaration in callb.h
	//
	(void) callb_add(shutdown, NULL, CB_CL_UADMIN,	/*lint !e10 */
		"CMM shutdown");
#endif

	return (0);
}

// Called during clean shutdown of the system.
// See usr/src/uts/common/syscall/uadmin.c
// Note that this removes us from the cluster quickly and makes shre that
// invocations which are made to other nodes will fail (since after we leave
// the cluster they will not be accepted by other nodes). This does not
// call ORB::shutdown() because that does a cleaner shutdown which may end
// up blocking the uadmin call. If we want to do a clean shutdown, so
// we can modunload the orb then we should call cmm_callback_impl::shutdown()
// followed by ORB::shutdown(). This is done for unode.
boolean_t
cmm_callback_impl::shutdown(void *, int)
{
	if (the_cmm_callback_implp != NULL) {
		cmm::control_var cmm_cntl_v = cmm_ns::get_control();
		// We only run the stop transition if the cmm is prepared to,
		// and if monitoring of the paths is enabled.  We check for
		// path monitoring because if it is not enabled, the cmm could
		// hang indefinitely trying to send messages to unresponsive
		// nodes.
		if (is_not_nil(cmm_cntl_v) &&
		    !the_cmm_callback_implp->stopped &&
		    !the_cmm_callback_implp->aborted &&
		    path_manager::the().is_enabled()) {
			Environment e;
			cmm_cntl_v->stop(e);
			e.clear();
			the_cmm_callback_implp->wait_for_stopped();
			// Give the CMM a chance to send out its state
			// messages - so that other nodes can reconfigure
			// quickly. Sleep for SHUTDOWN_CMM_LAG msecs.
			os::usecsleep((os::usec_t)(SHUTDOWN_CMM_LAG * 1000));
		}
		if (path_manager::the().is_enabled()) {
			// shut down the threads allocated in the start state
			the_cmm_callback_implp->lock();
			cmm_step_thread_state::stop_all();
			the_cmm_callback_implp->unlock();
		}
	}
	return (B_TRUE);
}

void
#ifdef DEBUG
cmm_callback_impl::_unreferenced(unref_t cookie)
#else
cmm_callback_impl::_unreferenced(unref_t)
#endif
{
	cmm_step_thread_state::shutdown_threads();
	cmm_state::shutdown_states();
	ASSERT(_last_unref(cookie));
	delete this;
}

void
cmm_callback_impl::start_trans(Environment &e)
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl start transition\n",
	    orb_conf::local_nodeid()));

	lock();
	ASSERT(statep == cmm_state::the(0)); // The cmm_begin_state
	statep->transition(e);
	unlock();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl start transition done\n",
	    orb_conf::local_nodeid()));
}

void
cmm_callback_impl::set_reconf_data(cmm::seqnum_t sn,
	const cmm::membership_t &membership, Environment &)
{
	lock();

	ASSERT(statep == cmm_state::the(0)); // cmm_begin_state

	seqnum = sn;

	ASSERT(_membership != NULL);
	_membership->rele();

	_membership = new callback_membership(membership);

	unlock();
}

void
cmm_callback_impl::step_trans(
    uint32_t step_number,
    cmm::timeout_t tmout,
    Environment &e)
{
	// Set state to indicate stepN of reconfig in progress
	os::envchk::set_nonblocking(os::envchk::NB_RECONF);

	lock();
	statep = cmm_state::the(step_number);
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl step transition '%s' (%d) tm:%d\n",
	    orb_conf::local_nodeid(), statep->desc(), step_number, tmout));
	statep->init(_membership, seqnum, tmout);
	statep->transition(e);
	ASSERT(lock_held());
	statep->fini();
	unlock();

	// Clear state to indicate stepN of reconfig done
	os::envchk::clear_nonblocking(os::envchk::NB_RECONF);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl step transition '%s' (%d) done\n",
	    orb_conf::local_nodeid(), statep->desc(), step_number));
}

void
cmm_callback_impl::return_trans(cmm::timeout_t, Environment &e)
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl return transition\n",
	    orb_conf::local_nodeid()));

	lock();
	statep->to_begin();
	statep = cmm_state::the(0); // cmm_begin_state
	statep->transition(e);
	unlock();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl return transition done\n",
	    orb_conf::local_nodeid()));
}

void
cmm_callback_impl::stop_trans(cmm::timeout_t, Environment &)
{
	statep->to_stop();
	lock();
	statep = NULL;
	unlock();
}

void
cmm_callback_impl::abort_trans(cmm::timeout_t tmout, Environment &e)
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("cmm_callback_impl abort transition timeout %d\n", tmout));

	statep->to_abort(tmout);
	statep->transition(e);
}

//
// Function that can be called by the CMM automaton to signal that the current
// step needs to be terminated. This is invoked when a new reconfiguration
// needs to be started due to the failure of a node while the current
// reconfiguration is in progress.
//
// If the _step_blocking flag is set, this indicates that the current state
// is of cmm_step_thread_state class and that it is waiting for the
// cmm_step_thread or something else to complete its work. To cancel that work,
// this function cancels the running step thread object.
//
void
cmm_callback_impl::terminate_step(Environment &e)
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl terminate_step(deprecated) called\n",
	    orb_conf::local_nodeid()));

	e.system_exception(CORBA::VERSION(1, CORBA::COMPLETED_YES));
}

void
cmm_callback_impl::terminate_step_this_seqnum(cmm::seqnum_t terminate_seq,
	Environment &)
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("node %d: cmm_callback_impl terminate_step_this_seqnum called\n",
	    orb_conf::local_nodeid()));

	lock();
	//
	// Note the sequence number of the reconfiguration that is
	// being canceled. It is possible that this is called in step1
	// before set_reconf_data() has been called and the ORB does not
	// know the newest reconfiguration sequence number.
	//
	last_canceled_sequence = terminate_seq;

	if (_step_blocking)
		((cmm_step_state *) statep)->cancel();

	unlock();
}

// We wait here for the orb to fully join the cluster.	The joined flag is set
// by mark_joined.
int
cmm_callback_impl::wait_for_joined()
{
	char	*nodename = new char [CL_MAX_LEN+1];

	clconf_get_nodename(orb_conf::local_nodeid(), nodename);

	lock();
	if (!joined && !aborted) {
		//
		// SCMSGS
		// @explanation
		// The specified node is attempting to become a member of the
		// cluster.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "CMM: Node %s: attempting to join cluster.",
		    nodename);
	}
	while (!joined && !aborted) {
		cv.wait(&lck);
	}
	if (aborted) {
		unlock();
		delete [] nodename;
		return (ECANCELED); // Operation canceled
	}
	unlock();
	//
	// SCMSGS
	// @explanation
	// The specified node has joined the cluster.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: Node %s: joined cluster.", nodename);

	delete [] nodename;

	return (0);
}

void
cmm_callback_impl::wait_for_stopped()
{
	lock();
	while (!stopped && !aborted) {
		cv.wait(&lck);
	}
	unlock();
}

void
cmm_callback_impl::mark_joined()
{
	ASSERT(lock_held());
	if (!joined) {
		joined = true;
		cv.broadcast();
	}
}

void
cmm_callback_impl::mark_aborted()
{
	lock();
	aborted = true;
	cv.broadcast();
	unlock();
}

void
cmm_callback_impl::mark_stopped()
{
	lock();
	stopped = true;
	cv.broadcast();
	unlock();
}

void
cmm_callback_impl::step_blocking(cmm::seqnum_t sn)
{
	ASSERT(lock_held());
	ASSERT(!_step_blocking);
	_step_blocking = true;
	if (sn <= last_canceled_sequence)
		((cmm_step_state *) statep)->cancel();
}

void
cmm_callback_impl::step_not_blocking()
{
	ASSERT(lock_held());
	ASSERT(_step_blocking);
	_step_blocking = false;
}

cmm_state::cmm_state(const char *d) :
	description(d)
{
}

//
// the implementation of cmm_state::initialize_states is in
// cmm_callback_steps.cc for convenience.
//

void
cmm_state::shutdown_states()
{
	uint_t	i;
	for (i = 0; i <= NUM_ORB_STEPS; i++) {
		if (the_cmm_states[i] != NULL) {
			delete the_cmm_states[i];
			the_cmm_states[i] = NULL;
		}
	}
#ifdef CMM_VERSION_0
	for (i = 0; i <= NUM_ORB_STEPS; i++) {
		//
		// states 12 and 13 are undefined in version 0.
		//
		if (i == 12 || i == 13) {
			continue;
		}
		if (the_cmm_states_v0[i] != NULL) {
			delete the_cmm_states_v0[i];
			the_cmm_states_v0[i] = NULL;
		}
	}
#endif // CMM_VERSION_0
}

cmm_step_state::cmm_step_state(const char *d) :
	cmm_state(d),
	seqnum(0),
	step_timeout(0),
	membership(NULL)
{
}

cmm_step_state::~cmm_step_state()
{
	ASSERT(membership == NULL);
	membership = NULL;
}

void
cmm_step_state::init(callback_membership *mbrship, cmm::seqnum_t sn,
	cmm::timeout_t tmout)
{
	ASSERT(lock_held());
	ASSERT(membership == NULL);
	membership = mbrship;
	membership->hold();
	seqnum = sn;
	step_timeout = tmout;
}

void
cmm_step_state::fini()
{
	ASSERT(lock_held());
	ASSERT(membership != NULL);
	membership->rele();
	membership = NULL;
}

void
cmm_step_state::cancel()
{
}

void
cmm_step_state::to_begin()
{
}

void
cmm_step_state::to_abort(cmm::timeout_t)
{
	FAULTPT_KCMM(FAULTNUM_CMM_STOP_ABORT, FaultFunctions::generic);
	//
	// SCMSGS
	// @explanation
	// The node is going down due to a decision by the cluster membership
	// monitor.
	// @user_action
	// This message is preceded by other messages indicating the specific
	// cause of the abort, and the documentation for these preceding
	// messages will explain what action should be taken. The node should
	// be rebooted if node failure is unexpected.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE, "CMM aborting.");
}

// Illegal transition.
void
cmm_step_state::to_stop()
{
	ASSERT(0);
}

int
cmm_step_thread_state::initialize_threads(cmm_callback_impl *ccip)
{
	int err;
	uint_t i;

	for (i = 0; i < CSTS_NUM_THREADS; i++) {
		ASSERT(the_step_threads[i] == NULL);
		the_step_threads[i] = new cmm_step_thread();
		if (the_step_threads[i] == NULL)
			return (ENOMEM);
	}

	for (i = 0; i < CSTS_NUM_THREADS; i++) {
		if ((err = the_step_threads[i]->init(ccip->get_objref())) !=
		    0) {
			for (uint_t j = 0; j < i; j++)
				the_step_threads[i]->fini();
			return (err);
		}
	}
	return (0);
}

void
cmm_step_thread_state::shutdown_threads()
{
	for (uint_t i = 0; i < CSTS_NUM_THREADS; i++) {
		if (the_step_threads[i] != NULL) {
			delete the_step_threads[i];
			the_step_threads[i] = NULL;
		}
	}
}

cmm_step_thread_state::cmm_step_thread_state(const char *d, thr_id tid) :
	cmm_step_state(d),
	thread_mbrshp(NULL),
	thread_seqnum(0),
	my_thr_id(tid)
{
}

cmm_step_thread_state::~cmm_step_thread_state()
{
#ifdef	DEBUG
	for (uint_t i = 0; i < CSTS_NUM_THREADS; i++)
		ASSERT(the_step_threads[i] == NULL);
#endif
	ASSERT(thread_mbrshp == NULL);
}

void
cmm_step_thread_state::transition(Environment &e)
{
	ASSERT(lock_held());

	// If the thread isn't available, just return here.
	if (is_stopped())
		return;

	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	// Notify the callback_impl we will be waiting for our
	// thread to finish running thread_transition.
	cmm_callback_impl::thep()->step_blocking(seqnum);

	// Install ourselves as the current step.  When this returns
	// the membership and seqnum will have been copied to their
	// thread_ versions, and the thread_transition function is
	// guaranteed to be called
	the_cst().run_thread_transition(this, membership, seqnum);

	// Wait for the current step to complete.  If it is canceled
	// while running wait_for_done returns false.
	if (!the_cst().wait_for_done(seqnum, &timeout_time, e)) {
		ORB_DBPRINTF(ORB_TRACE_RECONFIG,
		    ("Step %s canceled while in sequence %d.\n",
		    desc(), seqnum));
	}

	cmm_callback_impl::thep()->step_not_blocking();
}

void
cmm_step_thread_state::thread_init(callback_membership *m, cmm::seqnum_t sn)
{
	ASSERT(lock_held());
	ASSERT(thread_mbrshp == NULL);
	thread_mbrshp = m;
	thread_mbrshp->hold();
	thread_seqnum = sn;
}

void
cmm_step_thread_state::thread_fini()
{
	ASSERT(thread_mbrshp != NULL);
	thread_mbrshp->rele();
	thread_mbrshp = NULL;
}

bool
cmm_step_thread_state::is_stopped()
{
	return (!the_cst().has_thread());
}

void
cmm_step_thread_state::stop()
{
	the_cst().fini();
}

void
cmm_step_thread_state::stop_all()
{
	for (uint_t i = 0; i < CSTS_NUM_THREADS; i++) {
		ASSERT(the_step_threads[i] != NULL);
		the_step_threads[i]->fini();
	}
}

void
cmm_step_thread_state::cancel()
{
	the_cst().cancel(seqnum);
}

bool
cmm_step_thread_state::canceled()
{
	return (the_cst().canceled(thread_seqnum));
}

cmm_begin_state::cmm_begin_state() :
	cmm_state("begin state")
{
}

cmm_begin_state::~cmm_begin_state()
{
}

void
cmm_begin_state::init(callback_membership *, cmm::seqnum_t,
	cmm::timeout_t)
{
	// Should never be called
	ASSERT(0);
}

void
cmm_begin_state::transition(Environment &)
{
}

void
cmm_begin_state::fini()
{
}

// Nothing to do. It is possible to get multiple return transitions in a row.
void
cmm_begin_state::to_begin()
{
}

// The stop transition is only performed on this state, since a return
// transaction is always performed first.
// Do not perform any cleanups/shutdowns here as they lead to
// inconsistencies within separate components.

void
cmm_begin_state::to_stop()
{
	cmm_callback_impl::thep()->mark_stopped();
}

void
cmm_begin_state::to_abort(cmm::timeout_t)
{
	FAULTPT_KCMM(FAULTNUM_CMM_STOP_ABORT, FaultFunctions::generic);
	(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE, "CMM aborting.");
}

cmm_step_thread::cmm_step_thread() :
	the_cmm_callback_ptr(nil),
	the_cmm_step(NULL),
	current_sequence(0),
	s_mbrshp(NULL),
	s_sn(0),
	_has_thread(false),
	sequence_canceled(true),
	step_running(false),
	active(true)
{
}

cmm_step_thread::~cmm_step_thread()
{
	the_cmm_callback_ptr = NULL;
	the_cmm_step = NULL;
	s_mbrshp = NULL;
}

// start the thread, return ENOMEM on failure
int
cmm_step_thread::init(cmm::callback_ptr cp)
{
	ASSERT(lock_held());
	the_cmm_callback_ptr = cp;
	if (!cmm_impl::cmm_create_thread(cmm_step_thread::step_thread, this,
	    CMM_MAXPRI - 2)) {
		CORBA::release(cp);
		return (ENOMEM);
	}
	_has_thread = true;
	return (0);
}

// Note that this can be called safely multiple times
void
cmm_step_thread::fini()
{
	ASSERT(lock_held());
	active = false;
	sequence_canceled = true;
	// We're actually being a little premature here, but the thread
	// will die soon and should not be used again
	_has_thread = false;
	cv.broadcast();
}

bool
cmm_step_thread::has_thread()
{
	return (_has_thread);
}

void
cmm_step_thread::cancel(cmm::seqnum_t sn)
{
	ASSERT(lock_held());
	ASSERT(current_sequence <= sn);
	current_sequence = sn;
	sequence_canceled = true;
	cv.broadcast();
}

bool
cmm_step_thread::canceled(cmm::seqnum_t sn)
{
	bool retval;
	ASSERT(!lock_held());
	lock();
	retval = ((sn < current_sequence) || ((sn == current_sequence) &&
	    sequence_canceled));
	unlock();
	return (retval);
}

// The thread. Accepts command as of which step to run from stp->step
void
cmm_step_thread::step_thread(void *arg)
{
	cmm_step_thread *stp = (cmm_step_thread *) arg;

	stp->process_steps();
}

void
cmm_step_thread::process_steps()
{
	lock();
	// active becomes false when the node is shutting down
	while (active) {
		// Wait for a step to appear, or to be shut down
		while ((the_cmm_step == NULL) && active)
			cv.wait(get_lock());
		// Run the step.  At the end, clear the cancel condition
		// and signal any waiting threads.
		if ((the_cmm_step != NULL) && active) {
			the_cmm_step->thread_init(s_mbrshp, s_sn);
			step_running = true;
			cv.broadcast();
			unlock();
			the_cmm_step->thread_transition();
			lock();
			the_cmm_step->thread_fini();
			the_cmm_step = NULL;
			cv.broadcast();
		}
	}
	ASSERT(sequence_canceled);
	cv.broadcast();
	ASSERT(!CORBA::is_nil(the_cmm_callback_ptr));
	unlock();
	CORBA::release(the_cmm_callback_ptr);
	the_cmm_callback_ptr = nil;
}

// The interface for the orb/rmm_stepN_state's to use this thread
void
cmm_step_thread::run_thread_transition(cmm_step_thread_state *step_state,
    callback_membership *m, cmm::seqnum_t sn)
{
	ASSERT(lock_held());
	CL_PANIC((the_cmm_step == NULL) && !step_running && active);
	CL_PANIC(sn == current_sequence);

	s_mbrshp = m;
	s_sn = sn;
	the_cmm_step = step_state;
	cv.broadcast();
	while (!step_running)
		cv.wait(get_lock());
	step_running = false;
}

bool
cmm_step_thread::wait_for_done(cmm::seqnum_t sn, os::systime *timeout_time,
    Environment &e)
{
	cmm::force_return *fr(NULL);

	ASSERT(lock_held());
	CL_PANIC(sn >= current_sequence);

	if (!_has_thread)
		return (true);

	if (sn > current_sequence) {
		current_sequence = sn;
		sequence_canceled = false;
	}

	// If there is no running step, we're done
	if (the_cmm_step == NULL)
		return (true);

	// If sequence_canceled is true, the step has already been canceled
	if (sequence_canceled)
		return (false);

	// wait while we're not canceled and there is a job running
	while (!sequence_canceled && (the_cmm_step != NULL)) {
		bool timedout;
		if (timeout_time != NULL) {
			timedout = (cv.timedwait(get_lock(), timeout_time)
			    == os::condvar_t::TIMEDOUT);
		} else {
			timedout = false;
			cv.wait(get_lock());
		}
		if (timedout) {
			// If we timed out and the job is finished,
			// return true, otherwise cancel the job
			// and the current state and return false
			// with a force_return exception
			if (the_cmm_step == NULL)
				// If there is no running step, we're done
				return (true);
			else {
				// The sequence number shouldn't have changed
				// while we were waiting
				ASSERT(sn == current_sequence);
				ORB_DBPRINTF(ORB_TRACE_RECONFIG,
				    ("Node %d: Step %s timed out\n",
				    orb_conf::local_nodeid(),
				    the_cmm_step->desc()));
				// we've timed out, cancel the sequence by
				// forcing a return transition
				sequence_canceled = true;
				if (!e.exception()) {
					fr = new cmm::force_return;
					e.exception(fr);
				}
				return (false);
			}
		} else if (the_cmm_step == NULL) {
			// If there is no running step, we're done
			return (true);
		} else if (sequence_canceled) {
			ASSERT(sn == current_sequence);
			// Otherwise we were canceled by the cmm
			return (false);
		}
	}

	/* NOTREACHED */
	ASSERT(0);

	return (false);
}
