/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NODE_H
#define	_NODE_H

#pragma ident	"@(#)Node.h	1.33	08/05/20 SMI"

#include <sys/clconf.h>		// for nodeid_t typedef
#include <sys/os.h>

//
// This constant represents an unknown node incarnation number.
//
enum { INCN_UNKNOWN = 0 };

typedef uint32_t	incarnation_num;

#ifdef _KERNEL_ORB
// ID_node
class ID_node {
public:
	ID_node();
	ID_node(const ID_node &id);
	ID_node(nodeid_t node_num, incarnation_num incarn);

	// Check for equality of two ID_nodes
	static bool match(const ID_node &node1, const ID_node &node2);

	nodeid_t		ndid;
	incarnation_num		incn;

};

//
// Create a global node object that can be used to represent invalid node.
// Defined in members.cc
// (should be a const, but that will require change in too many places)
//
extern ID_node invalid_node;

#include <orb/member/Node_in.h>

#endif	// _KERNEL_ORB

#endif	/* _NODE_H */
