/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  cmm_callback_impl_in.h
 *
 */

#ifndef _CMM_CALLBACK_IMPL_IN_H
#define	_CMM_CALLBACK_IMPL_IN_H

#pragma ident	"@(#)cmm_callback_impl_in.h	1.32	08/05/20 SMI"

inline static cmm_callback_impl *
cmm_callback_impl::thep()
{
	return (the_cmm_callback_implp);
}

inline os::mutex_t *
cmm_callback_impl::get_lock()
{
	return (&lck);
}

inline void
cmm_callback_impl::lock()
{
	lck.lock();
}

inline void
cmm_callback_impl::unlock()
{
	lck.unlock();
}

inline bool
cmm_callback_impl::lock_held()
{
	return (lck.lock_held());
}

inline const char *
cmm_state::desc()
{
	return (description);
}

inline cmm_state *
cmm_state::the(uint_t state)
{
#ifdef CMM_VERSION_0
	if (num_orb_steps == NUM_ORB_STEPS) {
		ASSERT(the_cmm_states[state] != NULL);
		return (the_cmm_states[state]);
	}
	ASSERT(num_orb_steps == NUM_ORB_STEPS_V0);
	ASSERT(the_cmm_states_v0[state] != NULL);
	return (the_cmm_states_v0[state]);
#else
	ASSERT(the_cmm_states[state] != NULL);
	return (the_cmm_states[state]);
#endif // CMM_VERSION_0
}

inline os::mutex_t *
cmm_state::get_lock()
{
	ASSERT(cmm_callback_impl::thep() != NULL);
	return (cmm_callback_impl::thep()->get_lock());
}

inline void
cmm_state::lock()
{
	get_lock()->lock();
}

inline void
cmm_state::unlock()
{
	get_lock()->unlock();
}

inline bool
cmm_state::lock_held()
{
	return (get_lock()->lock_held());
}

inline os::mutex_t *
cmm_step_thread::get_lock()
{
	ASSERT(cmm_callback_impl::thep() != NULL);
	return (cmm_callback_impl::thep()->get_lock());
}

inline void
cmm_step_thread::lock()
{
	get_lock()->lock();
}

inline void
cmm_step_thread::unlock()
{
	get_lock()->unlock();
}

inline bool
cmm_step_thread::lock_held()
{
	return (get_lock()->lock_held());
}

inline cmm_step_thread &
cmm_step_thread_state::the_cst()
{
	ASSERT(the_step_threads[my_thr_id] != NULL);
	return (*(the_step_threads[my_thr_id]));
}

inline cmm_step_thread &
cmm_step_thread_state::the_cst(thr_id td)
{
	ASSERT(the_step_threads[td] != NULL);
	return (*(the_step_threads[td]));
}

#endif	/* _CMM_CALLBACK_IMPL_IN_H */
