/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cmm_callback_steps.cc	1.40	08/05/20 SMI"

//
// This file contains the bulk of the cluster reconfiguration code.
//
// During cluster reconfiguration we do many things including:
// Transport:
//	Notify the transport of the new membership.
// ORB:
//	Wake up any threads which are waiting for a response from a node
//	which is now dead (these are hung in invoke).
//	Remove xdoors references associated with nodes which are now dead.
// Version Manager
//	Cause the version manager to communicate with its counterparts on
//	other nodes
// Replica Manager
//	The Replica manager depends on the reconfiguration to do some cleanup
//	of the RMAs during reconfiguration.
//	The Replica Manager is managed by the RMM which is driven by the higher
//	numbered steps here.
// Register the HA replica manager and the RMA
//
// The specific work done by the version manager and RMM are documented in
// those facilities rather than here.
//
// When modifying these interfaces, it is important to remember that the
// synchronization between steps is rather loose.  The only guarantee is that
// all nodes will be finished with a given step before the following one
// executes (or, if there is a change in membership or a reconfiguration is
// requested, that all nodes will return to the first step, but in that case
// some nodes may have just finished step N whereas other nodes return from
// step N+1).
//
// This looseness means that a step may start and finish executing on one
// node before the same step on a different node has started.  Any finer-
// grained synchronization is the responsibility of the implementor.
//

#include <sys/clconf_int.h>
#include <sys/os.h>
#include <sys/node_order.h>
#include <orb/member/cmm_callback_impl.h>
#include <orb/member/cmm_callback_steps.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_impl.h>
#include <orb/member/members.h>
#include <orb/refs/refcount.h>
#include <orb/xdoor/translate_mgr.h>
#include <orb/member/Node.h>
#include <orb/object/adapter.h>
#include <orb/flow/resource_mgr.h>
#include <repl/rma/rma.h>
#include <repl/rmm/rmm.h>
#include <repl/repl_mgr/rm_version.h>
#include <orb/msg/orb_msg.h>
#include <orb/msg/message_mgr.h>
#include <orb/debug/orb_trace.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/transport/endpoint_registry.h>
#include <orb/transport/path_manager.h>
#include <vm/vm_comm.h>
#include <vm/vm_lca_impl.h>
#include <sys/vm_util.h>

#ifdef _KERNEL
#include <sys/callb.h>
#endif
#include <orb/fault/fault_injection.h>

//
// Static function on cmm_state that creates the state objects
// The code is here for convenience.
//
// The assignment below is the only association between steps and their
// numbers.  The implementation relies on step 0 always being the begin
// step, and changing step 1 would be difficult.
// If the number of steps is changing, the CMM would
// also need a new interface to allow that change.  This interface would not
// require any distributed agreement on the part of the CMM, because every
// node could call it during step 1 based on the distributed agreement already
// provided by the version manager.
//
// Note that some steps wait for the thread that will be used to run the
// following step to complete.  The code has been altered so that they wait
// for the actual thread as opposed to the step number, but if the steps are
// re-shuffled or new steps using the same thread added, the code to do the
// wait in the adjacent steps may also have to be changed.
//
// The thread_transitions function of these following steps is executed in
// threads  represented by cmm_step_thread objects.
// resource_reallocation_step -- step 4
// rmm_disqualification_vm_step -- step 7
// rm_connect_vm_transmit_step -- step 8
// rm_promote_vm_calc_step -- step 9
// rma_lca_register_step -- step 12
// The transitions function of the remaining states is run on the thread
// represented by cb::cmm_callback_thread object. The steps preceding the
// above steps need to wait in wait_for_done() to make sure that the
// cmm_step_thread has completed its task. This is not needed for
// rmm_disqualification_vm_step and rm_connect_vm_transmit_step as these run
// on the same cmm_step_thread.
//

int
cmm_state::initialize_states()
{
	//
	// IMPORTANT: If you change the number or name of any step,
	// remember to change it in all the comments in this and
	// other files. Also remember to change the name in the
	// constructor of the step.
	//
	// Also, whenever step list changes, the following
	// values should be ajusted: i) Total number of steps
	// (NUM_ORB_STEPS) defined in callback impl, and ii) The
	// index of rmm_cleanup_step (this step timeout = 0)
	// in cmm_callback_impl::initialize.
	//

	//
	// Post 3.2 we support both Rolling Upgrade and Quantum
	// Leap.
	//
	the_cmm_states[0] = new cmm_begin_state();
	the_cmm_states[1] = new vm_bootstrap_calc_step();
	the_cmm_states[2] = new vm_bootstrap_commit_step();
	the_cmm_states[3] = new quiesce_step();
	the_cmm_states[4] = new resource_reallocation_step();
	the_cmm_states[5] = new refcnt_cleanup_step();
	the_cmm_states[6] = new rxdoor_cleanup_step();
	the_cmm_states[7] = new rmm_disqualification_vm_step();
	the_cmm_states[8] = new rm_connect_vm_transmit_step();
	the_cmm_states[9] = new rm_promote_vm_calc_step();
	the_cmm_states[10] = new rm_service_vm_commit_step();
	the_cmm_states[11] = new signal_cbc_rmm_cleanup_step();
	the_cmm_states[12] = new rma_lca_register_step();
	the_cmm_states[13] = new mark_joined_vm_step();

	//
	// Cluster shutdown steps. CMM reconfigurations during
	// cluster shutdown execute these steps.
	//
	the_cmm_states[14] = new quiesce_shutdown_step();
	the_cmm_states[15] = new resource_reallocation_step();
	the_cmm_states[16] = new refcnt_cleanup_shutdown_step();
	the_cmm_states[17] = new rxdoor_cleanup_shutdown_step();
	the_cmm_states[18] = new mark_joined_shutdown_step();

	for (uint_t i = 0; i <= NUM_ORB_STEPS; i++) {
		if (the_cmm_states[i] == NULL) {
			shutdown_states();
			return (ENOMEM);
		}
	}
#ifdef CMM_VERSION_0
	//
	// The first five steps is shared with version 1 states.
	//
	the_cmm_states_v0[0] = the_cmm_states[0];
	the_cmm_states_v0[1] = the_cmm_states[3];
	the_cmm_states_v0[2] = the_cmm_states[4];
	the_cmm_states_v0[3] = the_cmm_states[5];
	the_cmm_states_v0[4] = the_cmm_states[6];
	the_cmm_states_v0[5] = new rmm_disqualification_step();
	the_cmm_states_v0[6] = new rm_primary_connect_step();
	the_cmm_states_v0[7] = new rm_primary_promote_step();
	the_cmm_states_v0[8] = new rm_service_start_step();
	the_cmm_states_v0[9] = new rmm_cleanup_step();
	the_cmm_states_v0[10] = new rma_register_step();
	the_cmm_states_v0[11] = new mark_joined_step();
	//
	// The following two steps are not defined in version 0
	// CMM.
	//
	the_cmm_states_v0[12] = NULL;
	the_cmm_states_v0[13] = NULL;
	//
	// Cluster shutdown steps. CMM reconfigurations during
	// cluster shutdown execute these steps. The shutdown
	// states are shared with version 1.
	//
	the_cmm_states_v0[14] = the_cmm_states[14];
	the_cmm_states_v0[15] = the_cmm_states[15];
	the_cmm_states_v0[16] = the_cmm_states[16];
	the_cmm_states_v0[17] = the_cmm_states[17];
	the_cmm_states_v0[18] = the_cmm_states[18];

	for (uint_t i = 0; i <= NUM_ORB_STEPS; i++) {
		//
		// steps 12 and 13 are undefined in version 0 CMM.
		//
		if (i == 12 || i == 13) {
			continue;
		}
		if (the_cmm_states_v0[i] == NULL) {
			shutdown_states();
			return (ENOMEM);
		}
	}
#endif // CMM_VERSION_0
	return (0);
}

#ifdef CMM_VERSION_0
//
// Select and Initialize the old set of states as the callback steps.
//
void
cmm_state::select_states_v0()
{
	curr_cmm_states = the_cmm_states_v0;
	num_orb_steps = NUM_ORB_STEPS_V0;
}

//
// Select and Initialize the new set of states as the callback steps.
//
void
cmm_state::select_states_v1()
{
	curr_cmm_states = the_cmm_states;
	num_orb_steps = NUM_ORB_STEPS;
}

uint_t
cmm_state::get_num_orb_steps()
{
	return (num_orb_steps);
}
#endif // CMM_VERSION_0

vm_bootstrap_calc_step::vm_bootstrap_calc_step() :
	cmm_step_state("bootstrap version manager calculation")
{
}

//
// This step and the following two all operate before membership changes
// cause normal invocations to dead nodes to return with COMM_FAILURE.
// This means that any communication with other nodes must be done with
// great care, and it is the responsibility of the communicator to address
// changes in incarnation number as well as retries upon the failure of
// a single path, as opposed to an entire node.
//
// Messages of type VM_MSG, as implemented by the Version Manager, have these
// properties.  Most others will not.
//
void
vm_bootstrap_calc_step::transition(Environment &)
{
	ASSERT(lock_held());

	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	unlock();

	// Send the current membership out to syslog.
	print_membership();

	// Give the ER the new membership
	endpoint_registry::the().set_membership(membership);

	//
	// Note that we're blocking and need to be notified if the
	// step will be canceled
	//
	lock();
	cmm_callback_impl::thep()->step_blocking(seqnum);
	unlock();

	// Calculate bootstrap cluster running versions
	vm_comm::the().calculate_btstrp_cl(
	    membership, seqnum, &timeout_time);

	lock();
	cmm_callback_impl::thep()->step_not_blocking();
}

//
// Print out the current membership using one printf.
//
void
vm_bootstrap_calc_step::print_membership()
{
	// The character array must be large enough to contain the
	// text names of all cluster nodes separated by a space and
	// null terminated. This can be a lot of characters.
	// Use static to avoid stack allocation. This function does not
	// need to be mt safe since it is only called from the step code above
	//
	static char	member_list[NODEID_MAX * (CL_MAX_LEN + 1)];
	char		*currp = member_list;

	//
	// Generate only one print statement.
	// This is important because the underlying system will perform
	// memory allocation per print statement, and we want to minimize
	// memory allocation during reconfiguration.
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if (membership->membership().members[i] != INCN_UNKNOWN) {

			clconf_get_nodename(i, currp);
			currp += strlen(currp);
			// Add a space after each nodename
			*currp++ = ' ';
		}
	}
	*(currp - 1) = '\0'; // overwrite last space character

	(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: Cluster members: %s.", member_list);
}

void
vm_bootstrap_calc_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("vm_bootstrap_calc_step::cancel() seqnum %lld\n", seqnum));

	// Cancel version manager message sends if necessary
	vm_comm::the().cmm_step_cancel(seqnum);
}

vm_bootstrap_commit_step::vm_bootstrap_commit_step() :
	cmm_step_state("bootstrap version manager commit")
{
}

void
vm_bootstrap_commit_step::transition(Environment &)
{
	ASSERT(lock_held());

	unlock();

	// Commit bootstrap cluster running versions
	vm_comm::the().commit_btstrp_cl();

	lock();
}

void
vm_bootstrap_commit_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("vm_bootstrap_commit_step::cancel() seqnum %lld\n", seqnum));

	// Cancel version manager message sends if necessary
	vm_comm::the().cmm_step_cancel(seqnum);
}

quiesce_step::quiesce_step() :
	cmm_step_state("quiesce")
{
}

//
// Print out the current membership using one printf.
//
void
quiesce_step::print_membership(callback_membership *cluster_membership)
{
	// The character array must be large enough to contain the
	// text names of all cluster nodes separated by a space and
	// null terminated. This can be a lot of characters.
	// Use static to avoid stack allocation. This function does not
	// need to be mt safe since it is only called from the step code above
	//
	static char	member_list[NODEID_MAX * (CL_MAX_LEN + 1)];
	char		*currp = member_list;

	//
	// Generate only one print statement.
	// This is important because the underlying system will perform
	// memory allocation per print statement, and we want to minimize
	// memory allocation during reconfiguration.
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if (cluster_membership->membership().members[i] !=
		    INCN_UNKNOWN) {

			clconf_get_nodename(i, currp);
			currp += strlen(currp);
			// Add a space after each nodename
			*currp++ = ' ';
		}
	}
	*(currp - 1) = '\0'; // overwrite last space character

	//
	// SCMSGS
	// @explanation
	// This message identifies the nodes currently in the cluster.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: Cluster members: %s.", member_list);
}

//
// Common code shared by quiesce_step and quiesce_step_shutdown
// steps.
//
void
quiesce_step::common_quiesce_step1(callback_membership *cluster_membership)
{
	//
	// Quiesce the checkpoint threadpool. Note that this must be done
	// before we update the membership, otherwise the checkpoints
	// will be rejected as orphans.
	//
	orb_msg::the().quiesce_ckpt_msg();

	//
	// Inform message_mgr of the new membership. We need to do this
	// after quiesing the checkpoints. Because once message_mgr knows
	// about the new membership it'll discard orphaned messages and
	// we don't want to discard checkpoints.
	//
	message_mgr::the().set_membership(cluster_membership);

	//
	// Update our node's view of membership.
	// NOTE: this method may block briefly while translate_in's
	// complete (to assure that a given message has a consistent
	// view of orphan-ness)
	//
	members::the().set_membership(cluster_membership->membership());

#if defined(_FAULT_INJECTION)
	//
	// Signal the Fault Injection Framework that the membership has
	// changed
	//
	FaultFunctions::signal_membership_change();
#endif
}

//
// Common code shared by quiesce_step and quiesce_step_shutdown
// steps. This is invoked after call to set_blocking() in the transition
// function.
//
void
quiesce_step::common_quiesce_step2(callback_membership *cluster_membership,
    cmm::seqnum_t cmm_seqnum)
{
	//
	// Cleanup reference count messages for dead nodes and make sure that
	// enough messages have been delivered for us to be confident that
	// server xdoors have all necessary foreign references.
	//
	refcount::the().cleanup_dead_refcount(cluster_membership->membership(),
	    cmm_seqnum);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("quiesce: refcount done\n"));

	// Wake up threads waiting for a response from dead nodes.
	outbound_invo_table::cleanup_invocations();

	//
	// Initialize flow control information and reject any blocked requests
	//
	resource_mgr::node_reconfiguration();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("quiesce: resource_mgr done\n"));

	//
	// Signal any threads processing orphaned requests from
	// dead nodes.
	//
	inbound_invo_table::cleanup_invocations();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("quiesce: invo_table cleanup done\n"));

	//
	// Tell the RMAs to freeze invocations for services whose primary is
	// now dead.
	//
	// The RMA requires that the system does not block its messages
	// at this point.
	// This step must occur after the following actions:
	// 1. "node_died" has ordered the transport to
	// fail any message that the transport has queued to the dead node.
	// 2. "cleanup_invocations" has failed invocations awaiting responses
	// from the dead node.
	// 3. "resource_mgr::node_reconfiguration" has failed any message
	// requests to the dead node that were blocked pending resources.
	//
	rma::the().freeze_invos_to_dead_primaries();
}

//
// This step operates before membership changes
// cause normal invocations to dead nodes to return with COMM_FAILURE.
// This means that any communication with other nodes must be done with
// great care, and it is the responsibility of the communicator to address
// changes in incarnation number as well as retries upon the failure of
// a single path, as opposed to an entire node.
//
void
quiesce_step::transition(Environment &e)
{
	static char	node_name[CL_MAX_LEN];

	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	unlock();

#ifdef CMM_VERSION_0
	//
	// In CMM version 0, this is the first callback step. Hence
	// need to print out the membership information and
	// inform the endpoint_registry of the new membership.
	//
	if (cmm_impl::the().is_old_version()) {
		//
		// Send the current membership out to syslog.
		//
		quiesce_step::print_membership(membership);

		//
		// Give the Endpoint registry the new membership
		//
		endpoint_registry::the().set_membership(membership);
	}
#endif // CMM_VERSION_0

	quiesce_step::common_quiesce_step1(membership);

	lock();
	cmm_callback_impl::thep()->step_blocking(seqnum);
	unlock();
	quiesce_step::common_quiesce_step2(membership, seqnum);

	//
	// If the DSM callback pointer has been initialized
	// by modload'ing PXFS, then make the callback.
	//
	extern void (*dsm_cmm_callback_ptr)(void);
	if (dsm_cmm_callback_ptr != NULL) {
		(*dsm_cmm_callback_ptr)();
	}

	extern incarnation_num last_fencing[];
	extern void (*dsm_fence_nodes_ptr)(const char *);
	extern void (*dsm_release_scsi2_ptr)(const char *);
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if ((membership->membership().members[i] ==
		    INCN_UNKNOWN) &&
		    (last_fencing[i] != INCN_UNKNOWN)) {
			//
			// Node is not here now, but was here last time
			// we did fencing, so we must fence him this
			// time.
			//
			if (dsm_fence_nodes_ptr != NULL) {
				clconf_get_nodename(i, node_name);
				if (node_name[0] != '\0')
					(*dsm_fence_nodes_ptr)
					    (node_name);
			}
		}
		if ((membership->membership().members[i] !=
		    INCN_UNKNOWN) &&
		    (last_fencing[i] == INCN_UNKNOWN)) {
			//
			// Node is here now, but was not here last time
			// we did fencing, so he must be joining the
			// cluster, unfence him.
			//
			if (dsm_release_scsi2_ptr != NULL) {
				clconf_get_nodename(i, node_name);
				if (node_name[0] != '\0')
					(*dsm_release_scsi2_ptr)
					    (node_name);
			}
		}
		//
		// remember this membership so we know who to fence next
		// time.
		//
		last_fencing[i] = membership->membership().members[i];
	}
	lock();

	// Make sure the thread for next step is available.  wait_for_done will
	// add a force_return exception to the environment if it isn't.
	(void) cmm_step_thread_state::the_cst(cmm_step_thread_state::CSTS_ORB).
	    wait_for_done(seqnum, &timeout_time, e);

	cmm_callback_impl::thep()->step_not_blocking();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("quiesce_step: rma done\n"));
}

void
quiesce_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("quiesce_step::cancel() seqnum %lld\n", seqnum));
	refcount::the().cleanup_dead_refcount_cancel(seqnum);
}

quiesce_shutdown_step::quiesce_shutdown_step() :
	cmm_step_state("quiesce_shutdown")
{
}

void
quiesce_shutdown_step::transition(Environment &e)
{
	static char	node_name[CL_MAX_LEN];

	ASSERT(lock_held());
	ASSERT(cmm_impl::the().get_cluster_shutdown());

	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	unlock();

	//
	// Send the current membership out to syslog.
	//
	quiesce_step::print_membership(membership);

	//
	// Give the Endpoint registry the new membership
	//
	endpoint_registry::the().set_membership(membership);

	quiesce_step::common_quiesce_step1(membership);

	lock();
	cmm_callback_impl::thep()->step_blocking(seqnum);
	unlock();

	quiesce_step::common_quiesce_step2(membership, seqnum);

	//
	// Need to acquire FENCE_LOCK of other nodes here and never
	// release them to prevent UCMM from reconfiguring. This
	// operation is idempotent and therefore safe during
	// multiple reconfigurations.
	//
	extern void (*dsm_acquire_fence_lock_ptr)(const char *);
	nodeid_t curr_node = clconf_get_local_nodeid();
	if (dsm_acquire_fence_lock_ptr != NULL) {
		for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
			if (membership->membership().members[i] !=
			    INCN_UNKNOWN) {
				if (curr_node == i) {
					continue;
				}
				clconf_get_nodename(i, node_name);
				if (node_name[0] != '\0') {
					(*dsm_acquire_fence_lock_ptr)
					    (node_name);
				}
			}
		}
		ORB_DBPRINTF(ORB_TRACE_RECONFIG,
		    ("quiesce_step: Avoid fencing of nodes during shutdown\n"));
	}

	lock();
	//
	// Make sure the thread for next step is available.  wait_for_done will
	// add a force_return exception to the environment if it isn't.
	//
	(void) cmm_step_thread_state::the_cst(cmm_step_thread_state::CSTS_ORB).
	    wait_for_done(seqnum, &timeout_time, e);

	cmm_callback_impl::thep()->step_not_blocking();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("quiesce_shutdown_step: rma done\n"));
}

void
quiesce_shutdown_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("quiesce_shutdown_step::cancel() seqnum %lld\n", seqnum));
	refcount::the().cleanup_dead_refcount_cancel(seqnum);
}

resource_reallocation_step::resource_reallocation_step() :
	cmm_step_thread_state("resource reallocation",
	cmm_step_thread_state::CSTS_ORB)
{
}

// This is the first node reconfiguration step which allows communications
// with other nodes.
//
// At the beginning of this step all nodes have a correct view of the cluster
// node membership and have completed initialization needed to begin
// interaction with other nodes.
//
// This step first obtains the information from server nodes to permit
// client nodes to transmit flow controlled messages.
//
// We make blocking calls to other nodes in this transition, so it is
// important that we detect timeouts, since we don't want the cmm to
// shut this node down because of a failure on a remote node.
//
void
resource_reallocation_step::thread_transition()
{
	//
	// The resource management subsystem must request initial resources
	// from the server nodes for the client node.
	//
	// This action must occur before anything else can send
	// flow controlled messages, such as remote invocations.
	//
	// The step cannot be canceled until after the resource management
	// subsystem has completed its actions. The resource management
	// subsystem transmits oneway messages and does not wait for any
	// response.
	//
	resource_mgr::reconfig_reallocate_resources();
}

refcnt_cleanup_step::refcnt_cleanup_step() :
	cmm_step_state("reference count cleanup")
{
}

//
// Common code shared by refcnt_cleanup and refcnt_cleanup_shutdown
// steps.
//
void
refcnt_cleanup_step::common_transition(callback_membership *cluster_membership)
{
	//
	// Tell the translate_mgr about the new membership.
	// This will complete the protocol for any in-transit marshals to
	// dead nodes.
	// This must be done after the reference counting work because
	// that makes sure that any in-transit marshals which have been
	// forwarded to other nodes have been accounted for.
	//
	// This must be done before we call into the rxdoor_manager with the
	// new membership, because we need to have stopped marshaling to the
	// dead nodes before we clear their bits.
	//
	// This must be done before we call into the RMA because the RMA waits
	// for the in-transit protocol to complete for some hxdoors and this
	// will clean up those associated with dead nodes.
	//
	translate_mgr::the().new_membership(cluster_membership->membership());

	//
	// Recalculate the foreign reference count on all standard xdoor
	// servers. This will result in unreferenced being delivered to xdoors
	// whose only foreign references were held by nodes which are now
	// dead.
	//
	rxdoor_membership_manager::the().
	    cleanup_foreignrefs(cluster_membership);
}


void
refcnt_cleanup_step::transition(Environment &e)
{
	os::systime timeout_time((os::usec_t)step_timeout * 1000);
	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	unlock();
	refcnt_cleanup_step::common_transition(membership);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("refcnt_cleanup_step: rxdoor_manager done\n"));

	lock();
	cmm_callback_impl::thep()->step_blocking(seqnum);
	unlock();

	//
	// Tell the RMA to freeze marshals and detach the underlying
	// client xdoors of services from the old primary which is now
	// dead. Since this call can block indefinitely, we pass in the
	// timeout for this step. If the timeout expires, we simply
	// reconfigure and retry this call next time through.
	//
	if (rma::the().disconnect_from_dead_primaries(
		seqnum, &timeout_time)) {
		e.exception(new cmm::force_return);
	}

	lock();
	cmm_callback_impl::thep()->step_not_blocking();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("refcnt_cleanup_step: rma done\n"));
}

void
refcnt_cleanup_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("refcnt_cleanup_step::cancel() seqnum %lld\n", seqnum));
	// The RMA may block in this step. This will unblock it.
	rma::the().disconnect_from_dead_primaries_cancel(seqnum);
}

refcnt_cleanup_shutdown_step::refcnt_cleanup_shutdown_step() :
	cmm_step_state("reference count cleanup during shutdown")
{
}

void
refcnt_cleanup_shutdown_step::transition(Environment &)
{
	ASSERT(lock_held());
	ASSERT(cmm_impl::the().get_cluster_shutdown());
	unlock();
	refcnt_cleanup_step::common_transition(membership);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("refcnt_cleanup_shutdown_step: rxdoor_manager done\n"));

	lock();
}

void
refcnt_cleanup_shutdown_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("refcnt_cleanup_shutdown_step::cancel() seqnum %lld\n", seqnum));
	//
	// The RMA may block in this step. This will unblock it.
	//
	rma::the().disconnect_from_dead_primaries_cancel(seqnum);
}

rxdoor_cleanup_step::rxdoor_cleanup_step() :
	cmm_step_state("rxdoor count cleanup")
{
}

//
// Common code shared by rxdoor_cleanup and rxdoor_clean_shutdown
// steps.
//
void
rxdoor_cleanup_step::common_transition()
{
	//
	// Clean up any old data in the rxdoor manager
	// list can be scanned safely.
	//
	rxdoor_manager::the().cleanup_resources();
}

//
// At the beginning of this step, reference counting is enabled. All server
// xdoors have a reference for all remote clients (could be zero).
// Translates are still disabled.
//
// At the end, all server xdoors that have zero foreign reference
// counts are released (_unreferenced()).
// Translation of xdoors is reenabled.
//
// If this is the first time that the ORB has made it this far, then the
// ORB initialization thread is signaled that it is done.
//

void
rxdoor_cleanup_step::transition(Environment &e)
{
	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	unlock();

	rxdoor_cleanup_step::common_transition();

	// Grab locks in rmm in preparation for disqualification
	rmm::the().reconfig_prepare();

	lock();

	cmm_callback_impl::thep()->step_blocking(seqnum);

	//
	// Make sure the thread for the next is available.
	// wait_for_done will add a force_return exception to the
	// environment if it isn't.
	//
	(void) cmm_step_thread_state::the_cst(
	    cmm_step_thread_state::CSTS_RMM).
	    wait_for_done(seqnum, &timeout_time, e);

	cmm_callback_impl::thep()->step_not_blocking();
}

void
rxdoor_cleanup_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rxdoor_cleanup_step::to_begin() seqnum %lld\n", seqnum));
	//
	// Drop locks we shouldn't hold through the previous steps.
	//
	rmm::the().reconfig_prepare_return();
}

rxdoor_cleanup_shutdown_step::rxdoor_cleanup_shutdown_step() :
	cmm_step_state("rxdoor count cleanup during shutdown")
{
}

void
rxdoor_cleanup_shutdown_step::transition(Environment &e)
{
	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	ASSERT(lock_held());
	ASSERT(cmm_impl::the().get_cluster_shutdown());
	unlock();

	rxdoor_cleanup_step::common_transition();

	lock();

	cmm_callback_impl::thep()->step_blocking(seqnum);

	//
	// Make sure the thread for the next is available.
	// wait_for_done will add a force_return exception to the
	// environment if it isn't. It is called regardless of cluster
	// shutdown as it also updates the sequence number of the thread.
	//
	(void) cmm_step_thread_state::the_cst(
	    cmm_step_thread_state::CSTS_RMM).
	    wait_for_done(seqnum, &timeout_time, e);

	cmm_callback_impl::thep()->step_not_blocking();
}

void
rxdoor_cleanup_shutdown_step::to_begin() {

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rxdoor_cleanup_shutdown_step::to_begin() seqnum %lld\n", seqnum));
}

rmm_disqualification_vm_step::rmm_disqualification_vm_step() :
	cmm_step_thread_state("rmm disqualification_vm",
	cmm_step_thread_state::CSTS_RMM)
{
}

void
rmm_disqualification_vm_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	bool coordinator_local;

	coordinator_local = rmm::the().disqualification(thread_mbrshp);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_disqualification_vm_step: rmm disqualification done\n"));

	//
	// Prepare the version manager coordinator message
	// This also allocates the coordinator if it is on this node
	// When disqualification ends, the node that will host the rm
	// primary knows it will, even if other nodes don't yet know
	// what node it will be
	//
	vm_comm::the().generate_cl_message(coordinator_local);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_disqualification_step: vm cl message generated\n"));

}

void
rmm_disqualification_vm_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_disqualification_vm_step::to_begin() seqnum %lld\n",
	    seqnum));
	rmm::the().disqualification_return();
}

rm_connect_vm_transmit_step::rm_connect_vm_transmit_step() :
	cmm_step_thread_state("rm primary connect and vm info transmission",
	cmm_step_thread_state::CSTS_RMM)
{
}

void
rm_connect_vm_transmit_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	rmm::the().primary_connection();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_connect_vm_transmit_step: rm primary connected\n"));

	if (!canceled()) {
		vm_comm::the().send_cl_message(membership, seqnum);

		ORB_DBPRINTF(ORB_TRACE_RECONFIG,
		    ("rm_connect_vm_transmit_step: vm cl message sent\n"));
	}
}

void
rm_connect_vm_transmit_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_connect_vm_transmit_step::to_begin() seqnum %lld\n", seqnum));
	rmm::the().primary_connection_return();

	vm_comm::the().cmm_step_cancel(seqnum);
}


rm_promote_vm_calc_step::rm_promote_vm_calc_step() :
	cmm_step_thread_state("rm primary promote and vm rv calculation",
	cmm_step_thread_state::CSTS_RMM)
{
}

void
rm_promote_vm_calc_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	rmm::the().primary_promotion(thread_mbrshp);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_promote_vm_calc_step: rm primary promoted\n"));

	if (!canceled()) {
		vm_comm::the().calculate_cl(membership, seqnum, &timeout_time);

		ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("rm_promote_vm_calc_step: "
		    "vm cl versions calculated\n"));
	}
}

void
rm_promote_vm_calc_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_promote_vm_calc_step::to_begin() seqnum %lld\n", seqnum));
	vm_comm::the().cmm_step_cancel(seqnum);
}

rm_service_vm_commit_step::rm_service_vm_commit_step() :
	cmm_step_state("rm service startup and vm rv commitment")
{
}

void
rm_service_vm_commit_step::transition(Environment &)
{
	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	unlock();

	rmm::the().service_startup();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_service_vm_commit_step: rm service started\n"));

	vm_comm::the().commit_cl_rv();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_service_vm_commit_step: vm cl versions committed\n"));

	lock();
}

signal_cbc_rmm_cleanup_step::signal_cbc_rmm_cleanup_step() :
	cmm_step_state("RMM cleanup and if needed, signal the cb coordinator")
{
}

void
signal_cbc_rmm_cleanup_step::transition(Environment &e)
{
	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	unlock();
	vm_comm::the().lock();
	if (vm_comm::the().are_vps_locked()) {
		//
		// If the vps are locked in the version manager, it
		// means that an upgrade commit is in progress. It's
		// possible this cmm reconfiguration is triggered by
		// the cb_coordinator's calling to
		// vm_comm::start_upgrade_commit() to start
		// calculations of new rvs and uvs. If yes, it must be
		// waiting for the calculations of new rvs and uvs to
		// complete. An earlier CMM stepvm_boot --
		// strap_commit_step commits new rvs for bootstrap
		// cluster vps and the last cmm step --
		// rm_service_vm_commit_step commits new uvs for
		// non-bootstrap cluster vps. So if we are here, we
		// know that all the nodes must have committed new rvs
		// and uvs if there are any And we can safely notify
		// the cb_coordinator about this. The cb_coordinator
		// will do nothing if this cmm reconfiguration is not
		// triggerred by vm_comm::start_upgrade_commit().
		//
		repl_mgr_impl *repl_rmp = repl_mgr_impl::get_rm();
		ASSERT(repl_rmp);

		cmm::seqnum_t	up_seq = vm_comm::the().get_upgrade_seq();
		//
		// Make sure we unlock before calling
		// signal_cb_coordinator.
		// See bug 4812213 for more information.
		//
		vm_comm::the().unlock();

		repl_rmp->get_cb_coordinator()->signal_cb_coordinator(up_seq);
		ORB_DBPRINTF(ORB_TRACE_RECONFIG,
		    ("signal_cbc_rmm_cleanup_step: signaled cb_coordinator\n"));
	} else {
		vm_comm::the().unlock();
	}

	rmm::the().reconfig_cleanup();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("signal_cbc_rmm_cleanup_step: "
	    "rmm reconfig cleaned up\n"));

	lock();

	//
	// Mark that dead primaries have been disconnected on all nodes
	// by the CMM if there are any.
	//
	rma::the().mark_dead_primaries_disconnected_on_all_nodes();
	ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("rmm_cleanup: Finished "
	    "mark_dead_primaries_disconnected_done_on_all_nodes done.\n"));

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("signal_cbc_rmm_cleanup_step: rm marked stable\n"));

	cmm_callback_impl::thep()->step_blocking(seqnum);
	//
	// Make sure the thread for next step is available.
	// wait_for_done will add a force_return exception to the
	// environment if it isn't. This is called regardless of
	// cluster shutdown as it also updates the sequence number
	// of the thread.
	//
	(void) cmm_step_thread_state::the_cst(
	    cmm_step_thread_state::CSTS_RMA).
	    wait_for_done(seqnum, NULL, e);

	cmm_callback_impl::thep()->step_not_blocking();

	extern void (*dsm_wait_for_release_ptr)(void);
	if (dsm_wait_for_release_ptr != NULL) {
		(*dsm_wait_for_release_ptr)();
	}
}

void
signal_cbc_rmm_cleanup_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("signal_cbc_rmm_cleanup_step::cancel() seqnum %lld\n", seqnum));
	cmm_step_thread_state::the_cst(cmm_step_thread_state::CSTS_RMA).
	    cancel(seqnum);
}


rma_lca_register_step::rma_lca_register_step() :
	cmm_step_thread_state("rma and lca registration",
	cmm_step_thread_state::CSTS_RMA),
	provider_registered(false)
{
}

//
// This is a special step that contains a stop at the end.  It runs in
// its own thread, which is deleted (with the stop function) when the two
// tasks are completed.  First the root object of the HA replica manager
// is put into the local nameserver, and then the local rma is registered
// with the replica manager.  Next, the version manager lca is registered
// with the cb_coordinator in the replica manager.  These operations are
// done in their own thread because they depend on the successful completion
// of the rmm steps.
//
void
rma_lca_register_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	if (!provider_registered) {
		provider_registered = rmm::the().register_provider();
	}
	if (!provider_registered) {
		ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("rma_lca_register_step: "
		    "rmm failed to register provider "
		    "in sequence %llu\n", thread_seqnum));
		CL_PANIC(canceled());
		return;
	}

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rma_lca_register_step: rmm registered "
	    "HA RM provider in sequence %llu\n", thread_seqnum));

	version_manager::vp_version_t rm_rv;
	Environment	e;
	version_manager::vm_admin_var vm_adm_v =
	    vm_util::get_vm(orb_conf::local_nodeid());

	// This is a local invocation
	vm_adm_v->get_running_version("replica_manager", rm_rv, e);

	if (RM_CKPT_VERS(rm_rv) < 2) {
		//
		// This makes an HA invocation, and therefore will
		// block until it succeeds.
		//
		rma::the().register_rma();
	}
	//
	// Blocks until the local RM gets all the states from the
	// current primary RM.
	//
	rma::the().wait_rm_sync();

	// Register version manager lca
	vm_lca_impl::the().register_lca();	//lint !e1705
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rma_lca_register_step: VM_LCA registered with HA RM in "
	    "sequence %llx, thread exiting\n", thread_seqnum));

	//
	// The following call to stop() will delete this step thread,
	// but that's fine because we only want these functions to run
	// once, so skipping this on subsequent CMM reconfiguration is
	// correct.
	//
	lock();
	stop();
	unlock();
}

mark_joined_vm_step::mark_joined_vm_step() :
	cmm_step_state("mark_joined_vm: mark joined")
{
}

void
mark_joined_vm_step::transition(Environment &)
{
	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	cmm_callback_impl::thep()->mark_joined();

	//
	// Signal the vm_comm that it's safe to calculate if a commit
	// is needed since the reconfiguration has completely finished.
	// See 4868638 for more details.
	//
	vm_comm::the().lock();
	if (vm_comm::the().is_signal_needed()) {
		vm_comm::the().signal_vm_comm(seqnum);
		ORB_DBPRINTF(ORB_TRACE_RECONFIG,
		    ("marked_joined_step: signaled vm_comm...\n"));
	}
	vm_comm::the().unlock();
	(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: node reconfiguration #%lld completed.", seqnum);
}

mark_joined_shutdown_step::mark_joined_shutdown_step() :
	cmm_step_state("mark joined during shutdown")
{
}

void
mark_joined_shutdown_step::transition(Environment &)
{
	ASSERT(lock_held());
	ASSERT(cmm_impl::the().get_cluster_shutdown());

	cmm_callback_impl::thep()->mark_joined();

	(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: node reconfiguration #%lld completed.", seqnum);
}

#ifdef CMM_VERSION_0

rmm_disqualification_step::rmm_disqualification_step() :
	cmm_step_thread_state("rmm disqualification",
	cmm_step_thread_state::CSTS_RMM)
{
}

void
rmm_disqualification_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	(void) rmm::the().disqualification(thread_mbrshp);
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_disqualification_step: rmm disqualification done\n"));
}

void
rmm_disqualification_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_disqualification_step::to_begin() seqnum %lld\n", seqnum));
	rmm::the().disqualification_return();
}

rm_primary_connect_step::rm_primary_connect_step() :
	cmm_step_thread_state("rm primary connect",
	cmm_step_thread_state::CSTS_RMM)
{
}

void
rm_primary_connect_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	rmm::the().primary_connection();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_primary_connect_step: rm primary connected\n"));
}

void
rm_primary_connect_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_primary_connect_step::to_begin() seqnum %lld\n", seqnum));
	rmm::the().primary_connection_return();
}

rm_primary_promote_step::rm_primary_promote_step() :
	cmm_step_thread_state("rm primary promote",
	cmm_step_thread_state::CSTS_RMM)
{
}

void
rm_primary_promote_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	os::systime timeout_time((os::usec_t)step_timeout * 1000);

	rmm::the().primary_promotion(thread_mbrshp);

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_primary_promote_step: rm primary promoted\n"));
}

void
rm_primary_promote_step::to_begin()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_primary_promote_step::to_begin() seqnum %lld\n", seqnum));
}

rm_service_start_step::rm_service_start_step() :
	cmm_step_state("rm service startup")
{
}

void
rm_service_start_step::transition(Environment &)
{
	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	unlock();

	rmm::the().service_startup();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rm_service_start_step: rm service started\n"));

	lock();
}

rmm_cleanup_step::rmm_cleanup_step() :
	cmm_step_state("RMM cleanup")
{
}

void
rmm_cleanup_step::transition(Environment &e)
{
	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	unlock();

	rmm::the().reconfig_cleanup();

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_cleanup_step: "
		"rmm reconfig cleaned up\n"));

	lock();

	//
	// Mark that dead primaries have been disconnected on all nodes
	// by the CMM if there are any.
	//
	rma::the().mark_dead_primaries_disconnected_on_all_nodes();
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_cleanup: Finished "
		"mark_dead_primaries_disconnected_done_"
		"on_all_nodes done.\n"));

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_cleanup_step: rm marked stable\n"));

	cmm_callback_impl::thep()->step_blocking(seqnum);

	//
	// Make sure the thread for next step is available.
	// wait_for_done will add a force_return exception to the
	// environment if it isn't.
	//
	(void) cmm_step_thread_state::the_cst(
	    cmm_step_thread_state::CSTS_RMA).
	    wait_for_done(seqnum, NULL, e);

	cmm_callback_impl::thep()->step_not_blocking();

	extern void (*dsm_wait_for_release_ptr)(void);
	if (dsm_wait_for_release_ptr != NULL)
		(*dsm_wait_for_release_ptr)();
}

void
rmm_cleanup_step::cancel()
{
	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rmm_cleanup_step::cancel() seqnum %lld\n", seqnum));
	cmm_step_thread_state::the_cst(cmm_step_thread_state::CSTS_RMA).
	    cancel(seqnum);
}

rma_register_step::rma_register_step() :
	cmm_step_thread_state("rma registration",
	cmm_step_thread_state::CSTS_RMA),
	provider_registered(false)
{
}

//
// This is a special step that contains a stop at the end.  It runs in
// its own thread, which is deleted (with the stop function) when the two
// tasks are completed.  First the root object of the HA replica manager
// is put into the local nameserver, and then the local rma is registered
// with the replica manager.  Next, the version manager lca is registered
// with the cb_coordinator in the replica manager.  These operations are
// done in their own thread because they depend on the successful completion
// of the rmm steps.
//
void
rma_register_step::thread_transition()
{
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	if (!provider_registered) {
		provider_registered = rmm::the().register_provider();
	}
	if (!provider_registered) {
		ORB_DBPRINTF(ORB_TRACE_RECONFIG, ("rma_register_step: "
		    "rmm failed to register provider "
		    "in sequence %llu\n", thread_seqnum));
		CL_PANIC(canceled());
		return;
	}

	ORB_DBPRINTF(ORB_TRACE_RECONFIG,
	    ("rma_register_step: rmm registered "
	    "HA RM provider in sequence %llu\n", thread_seqnum));

	version_manager::vp_version_t rm_rv;
	Environment	e;
	version_manager::vm_admin_var vm_adm_v =
	    vm_util::get_vm(orb_conf::local_nodeid());

	// This is a local invocation
	vm_adm_v->get_running_version("replica_manager", rm_rv, e);

	if (RM_CKPT_VERS(rm_rv) < 2) {
		//
		// This makes an HA invocation, and therefore will
		// block until it succeeds.
		//
		rma::the().register_rma();
	}
	//
	// Blocks until the local RM gets all the states from the
	// current primary RM.
	//
	rma::the().wait_rm_sync();

	//
	// The following call to stop() will delete this step thread,
	// but that's fine because we only want these functions to run
	// once, so skipping this on subsequent CMM reconfiguration is
	// correct.
	//
	lock();
	stop();
	unlock();
}

mark_joined_step::mark_joined_step() :
	cmm_step_state("mark joined")
{
}

void
mark_joined_step::common_transition()
{
	cmm_callback_impl::thep()->mark_joined();
}

void
mark_joined_step::transition(Environment &)
{
	cmm::membership_t	mem(membership->membership());

	ASSERT(lock_held());
	ASSERT(!cmm_impl::the().get_cluster_shutdown());

	mark_joined_step::common_transition();

	//
	// SCMSGS
	// @explanation
	// The cluster membership monitor has processed a change in node or
	// quorum status.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: node reconfiguration #%lld completed.", seqnum);

	//
	// At the end of the first reconfiguration CMM registers its
	// version by recording it in the CCR. We do that here
	// since this is the last step of the reconfiguration in the
	// version 0 CMM.
	//
	cmm_impl::the().register_cmm_version(mem);
}

#endif // CMM_VERSION_0
