/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  callback_membership_in.h
 *
 */

#ifndef _CALLBACK_MEMBERSHIP_IN_H
#define	_CALLBACK_MEMBERSHIP_IN_H

#pragma ident	"@(#)callback_membership_in.h	1.4	08/05/20 SMI"

inline
callback_membership::callback_membership(const cmm::membership_t &mbrship) :
    refcnt(), _membership(mbrship)
{
}

inline const cmm::membership_t &
callback_membership::membership()
{
	return (_membership);
}

inline incarnation_num
callback_membership::get_incn(nodeid_t nid)
{
	return (_membership.members[nid]);
}

inline lone_membership *
lone_membership::the()
{
	ASSERT(the_lone_membership != NULL);
	return (the_lone_membership);
}

inline
lone_membership::lone_membership(const cmm::membership_t &mbrship) :
    callback_membership(mbrship)
{
}

#endif	/* _CALLBACK_MEMBERSHIP_IN_H */
