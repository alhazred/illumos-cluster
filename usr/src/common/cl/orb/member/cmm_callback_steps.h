/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CMM_CALLBACK_STEPS_H
#define	_CMM_CALLBACK_STEPS_H

#pragma ident	"@(#)cmm_callback_steps.h	1.13	08/05/20 SMI"

#include <orb/member/cmm_callback_impl.h>
#include <cmm/cmm_version.h>


class vm_bootstrap_calc_step : public cmm_step_state {
public:
	vm_bootstrap_calc_step();
	void	transition(Environment &);
	void	print_membership();
	void	cancel();
private:
	// Disallow assignments and pass by value
	vm_bootstrap_calc_step(const vm_bootstrap_calc_step &);
	vm_bootstrap_calc_step &operator = (vm_bootstrap_calc_step &);
};

class vm_bootstrap_commit_step : public cmm_step_state {
public:
	vm_bootstrap_commit_step();
	void	transition(Environment &);
	void	cancel();
private:
	// Disallow assignments and pass by value
	vm_bootstrap_commit_step(const vm_bootstrap_commit_step &);
	vm_bootstrap_commit_step &operator = (vm_bootstrap_commit_step &);
};

class quiesce_step : public cmm_step_state {
public:
	quiesce_step();
	void	transition(Environment &);
	static void common_quiesce_step1(callback_membership *);
	static void common_quiesce_step2(callback_membership *,
	    cmm::seqnum_t);
	static void	print_membership(callback_membership *);
	void	cancel();
private:
	// Disallow assignments and pass by value
	quiesce_step(const quiesce_step &);
	quiesce_step &operator = (quiesce_step &);
};

class quiesce_shutdown_step : public cmm_step_state {
public:
	quiesce_shutdown_step();
	void	transition(Environment &);
	void	cancel();
private:
	// Disallow assignments and pass by value
	quiesce_shutdown_step(const quiesce_shutdown_step &);
	quiesce_shutdown_step &operator = (quiesce_shutdown_step &);
};

//
class resource_reallocation_step : public cmm_step_thread_state {
public:
	resource_reallocation_step();
private:
	void    	thread_transition();	// does the real work.

	// Disallow assignments and pass by value
	resource_reallocation_step(const resource_reallocation_step &);
	resource_reallocation_step &operator = (resource_reallocation_step &);
};

//
class refcnt_cleanup_step : public cmm_step_state {
public:
	refcnt_cleanup_step();
	void	transition(Environment &);
	static void common_transition(callback_membership *);
	void	cancel();
private:
	// Disallow assignments and pass by value
	refcnt_cleanup_step(const refcnt_cleanup_step &);
	refcnt_cleanup_step &operator = (refcnt_cleanup_step &);
};

class refcnt_cleanup_shutdown_step : public cmm_step_state {
public:
	refcnt_cleanup_shutdown_step();
	void	transition(Environment &);
	void	cancel();
private:
	// Disallow assignments and pass by value
	refcnt_cleanup_shutdown_step(const refcnt_cleanup_shutdown_step &);
	refcnt_cleanup_shutdown_step &operator =
	    (refcnt_cleanup_shutdown_step &);
};

//
class rxdoor_cleanup_step : public cmm_step_state {
public:
	rxdoor_cleanup_step();
	void	transition(Environment &);
	static void common_transition();
	void	to_begin();
private:
	// Disallow assignments and pass by value
	rxdoor_cleanup_step(const rxdoor_cleanup_step &);
	rxdoor_cleanup_step &operator = (rxdoor_cleanup_step &);
};

class rxdoor_cleanup_shutdown_step : public cmm_step_state {
public:
	rxdoor_cleanup_shutdown_step();
	void	transition(Environment &);
	void	to_begin();
private:
	// Disallow assignments and pass by value
	rxdoor_cleanup_shutdown_step(const rxdoor_cleanup_shutdown_step &);
	rxdoor_cleanup_shutdown_step &operator =
	    (rxdoor_cleanup_shutdown_step &);
};

//
class rmm_disqualification_vm_step : public cmm_step_thread_state {
public:
	rmm_disqualification_vm_step();
	void	to_begin();
private:

	void    thread_transition();	// does the real work.

	// Disallow assignments and pass by value
	rmm_disqualification_vm_step(const rmm_disqualification_vm_step &);
	rmm_disqualification_vm_step &operator =
	    (rmm_disqualification_vm_step &);
};

class rm_connect_vm_transmit_step : public cmm_step_thread_state {
public:
	rm_connect_vm_transmit_step();
	void    to_begin();
private:
	void    thread_transition();    // does the real work.

	// Disallow assignments and pass by value
	rm_connect_vm_transmit_step(const rm_connect_vm_transmit_step &);
	rm_connect_vm_transmit_step &operator =
	    (rm_connect_vm_transmit_step &);
};

class rm_promote_vm_calc_step : public cmm_step_thread_state {
public:
	rm_promote_vm_calc_step();
	void	to_begin();
private:
	void	thread_transition();    // does the real work.

	// Disallow assignments and pass by value
	rm_promote_vm_calc_step(const rm_promote_vm_calc_step &);
	rm_promote_vm_calc_step &operator = (rm_promote_vm_calc_step &);
};

class rm_service_vm_commit_step : public cmm_step_state {
public:
	rm_service_vm_commit_step();
	void    transition(Environment &);
private:
	// Disallow assignments and pass by value
	rm_service_vm_commit_step(const rm_service_vm_commit_step &);
	rm_service_vm_commit_step &operator = (rm_service_vm_commit_step &);
};

class signal_cbc_rmm_cleanup_step: public cmm_step_state {
public:
	signal_cbc_rmm_cleanup_step();
	void	transition(Environment &);
	void	cancel();
private:
	signal_cbc_rmm_cleanup_step(const signal_cbc_rmm_cleanup_step &);
	signal_cbc_rmm_cleanup_step &operator = (signal_cbc_rmm_cleanup_step &);
};

class rma_lca_register_step : public cmm_step_thread_state {
public:
	rma_lca_register_step();
private:
	void	thread_transition();
	bool	provider_registered;

	// Disallow assignments and pass by value
	rma_lca_register_step(const rma_lca_register_step &);
	rma_lca_register_step &operator = (rma_lca_register_step &);
};

class mark_joined_vm_step : public cmm_step_state {
public:
	mark_joined_vm_step();
	void	transition(Environment &);
private:
	// Disallow assignments and pass by value
	mark_joined_vm_step(const mark_joined_vm_step &);
	mark_joined_vm_step &operator = (mark_joined_vm_step &);
};

class mark_joined_shutdown_step : public cmm_step_state {
public:
	mark_joined_shutdown_step();
	void	transition(Environment &);
private:
	// Disallow assignments and pass by value
	mark_joined_shutdown_step(const mark_joined_shutdown_step &);
	mark_joined_shutdown_step &operator = (mark_joined_shutdown_step &);
};

#ifdef	CMM_VERSION_0
//
class rmm_disqualification_step : public cmm_step_thread_state {
public:
	rmm_disqualification_step();
	void	to_begin();
private:

	void    thread_transition();	// does the real work.

	// Disallow assignments and pass by value
	rmm_disqualification_step(const rmm_disqualification_step &);
	rmm_disqualification_step &operator = (rmm_disqualification_step &);
};

//
class rm_primary_connect_step : public cmm_step_thread_state {
public:
	rm_primary_connect_step();
	void	to_begin();
private:
	void    thread_transition();	// does the real work.

	// Disallow assignments and pass by value
	rm_primary_connect_step(const rm_primary_connect_step &);
	rm_primary_connect_step &operator = (rm_primary_connect_step &);
};

//
class rm_primary_promote_step : public cmm_step_thread_state {
public:
	rm_primary_promote_step();
	void	to_begin();
private:
	void    thread_transition();	// does the real work.

	// Disallow assignments and pass by value
	rm_primary_promote_step(const rm_primary_promote_step &);
	rm_primary_promote_step &operator = (rm_primary_promote_step &);
};

//
class rm_service_start_step : public cmm_step_state {
public:
	rm_service_start_step();
	void	transition(Environment &);
private:
	// Disallow assignments and pass by value
	rm_service_start_step(const rm_service_start_step &);
	rm_service_start_step &operator = (rm_service_start_step &);
};

//
class rmm_cleanup_step: public cmm_step_state {
public:
	rmm_cleanup_step();
	void	transition(Environment &);
	void	cancel();
private:
	rmm_cleanup_step(const rmm_cleanup_step &);
	rmm_cleanup_step &operator = (rmm_cleanup_step &);
};

class rma_register_step : public cmm_step_thread_state {
public:
	rma_register_step();
private:
	void	thread_transition();
	bool	provider_registered;

	// Disallow assignments and pass by value
	rma_register_step(const rma_register_step &);
	rma_register_step &operator = (rma_register_step &);
};

class mark_joined_step : public cmm_step_state {
public:
	mark_joined_step();
	void	transition(Environment &);
	static void common_transition();
private:
	// Disallow assignments and pass by value
	mark_joined_step(const mark_joined_step &);
	mark_joined_step &operator = (mark_joined_step &);
};
#endif // CMM_VERSION_0

#endif	/* _CMM_CALLBACK_STEPS_H */
