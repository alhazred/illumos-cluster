/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MEMBERS_H
#define	_MEMBERS_H

#pragma ident	"@(#)members.h	1.22	08/05/20 SMI"

#include <orb/invo/common.h>
#include <h/cmm.h>

//
// Class members - provides current node membership information
//	from the cmm for use by the orb.
//
//	The class requires that the specified node is within
//	the range of possible nodes.
//
//	Locking is not required with this implementation structure.
//	The value for each member resides in one word.
//	"word store to memory" is atomic.
//	Software requesting the value for a member will never receive
//	a partial value.
//	There is no synchronization between msg processing and orb
//	reconfiguration to guarantee the exact time when an incoming message
//	will see a new member value.
//	The number of possible nodes is fixed even though the current
//	membership changes dynamically.
//

class members {
public:
	// Get at the static instance of this class
	static members &the();

	// constructor
	members();

	// Initialize the membership with current incarnation numbers
	// This should do an atomic update so readers nevers see partiall
	// status for a particular node
	void		set_membership(const cmm::membership_t & membership);

	// Returns the current incarnation number of nodeid nd
	incarnation_num incn(nodeid_t nd);

	// Returns whether node nd is alive or not (i.e., incn != INCN_UNKNOWN)
	bool		alive(nodeid_t nd);

	// Returns whether ID_node nd is still alive or not
	// ID_node has both a node id and incarnation, still_alive checks to
	// see if the incarnation number in nd still matches the current
	// incarnation.
	// nd.incn should NOT be INCN_UNKNOWN, nd should always have a valid
	// incarnation number
	bool		still_alive(ID_node &nd);
private:

	// fixed size array that contains one entry per possible node.
	cmm::membership_t	the_membership;
};

#include <orb/member/members_in.h>

#endif	/* _MEMBERS_H */
