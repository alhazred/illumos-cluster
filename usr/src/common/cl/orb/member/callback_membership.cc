/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)callback_membership.cc	1.8	08/05/20 SMI"

#include <sys/node_order.h>
#include <orb/member/callback_membership.h>
#include <orb/infrastructure/orb_conf.h>

callback_membership::~callback_membership()
{
}

lone_membership *lone_membership::the_lone_membership = NULL;

// Initialize the lone_membership object with a membership that has the
// local incarnation number under this node's nodeid, and INCN_UNKNOWN
// for all other nodes.
int
lone_membership::initialize()
{
	cmm::membership_t	m;
	node_order		no;
	ASSERT(the_lone_membership == NULL);

	for (no.lowest(); no.valid(); no.higher()) {
		if (no.current() == orb_conf::local_nodeid())
			m.members[no.current()] = orb_conf::local_incarnation();
		else
			m.members[no.current()] = INCN_UNKNOWN;
	}
	the_lone_membership = new lone_membership(m); //lint !e772
	ASSERT(the_lone_membership != NULL);
	return (0);
}

void
lone_membership::shutdown()
{
	if (the_lone_membership == NULL) {
		return;
	}

	the_lone_membership->rele();  // Original hold from constructor
	the_lone_membership = NULL;
}
