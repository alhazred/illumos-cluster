/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  Node_in.h
 *
 */

#ifndef _NODE_IN_H
#define	_NODE_IN_H

#pragma ident	"@(#)Node_in.h	1.7	08/05/20 SMI"

inline
ID_node::ID_node() :
    ndid(NODEID_UNKNOWN),
    incn(INCN_UNKNOWN)
{
}

inline
ID_node::ID_node(const ID_node &id) :
    ndid(id.ndid),
    incn(id.incn)
{
}
inline
ID_node::ID_node(nodeid_t n, incarnation_num i) :
    ndid(n),
    incn(i)
{
}

// Check for equality of two ID_nodes
// static
inline bool
ID_node::match(const ID_node& n1, const ID_node& n2)
{
	return (((n1.ndid == n2.ndid) && (n1.incn == n2.incn)) ? true : false);
}

#endif	/* _NODE_IN_H */
