/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CALLBACK_MEMBERSHIP_H
#define	_CALLBACK_MEMBERSHIP_H

#pragma ident	"@(#)callback_membership.h	1.8	08/05/20 SMI"

#include <h/cmm.h>
#include <sys/refcnt.h>

// The callback_membership is initialized in set_membership and reference
// counted.  It is passed through the different reconfiguration states and
// deletes itself when no longer needed.

class callback_membership : public refcnt {
public:
	callback_membership(const cmm::membership_t &);

	virtual				~callback_membership();

	const cmm::membership_t &	membership();
	incarnation_num			get_incn(nodeid_t);
private:
	cmm::membership_t		_membership;

	// Disallow assignments and pass by value
	callback_membership(const callback_membership &);
	callback_membership &operator = (callback_membership &);
};

class lone_membership : public callback_membership {
public:
	lone_membership(const cmm::membership_t &);

	static lone_membership *the();
	static int initialize();
	static void shutdown();
private:
	static lone_membership *the_lone_membership;

	// Disallow assignments and pass by value
	lone_membership(const lone_membership &);
	lone_membership &operator = (lone_membership &);
};

#include <orb/member/callback_membership_in.h>

#endif	/* _CALLBACK_MEMBERSHIP_H */
