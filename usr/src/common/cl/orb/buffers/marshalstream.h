/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MARSHALSTREAM_H
#define	_MARSHALSTREAM_H

#pragma ident	"@(#)marshalstream.h	1.34	08/05/20 SMI"

//
// MARSHAL_DEBUG is a debugging feature useful when implementing new transports
// or ORB marshalling/unmarshalling changes in handlers/xdoors.
// When MARSHAL_DEBUG is defined, we marshal an extra word with each put_*
// (and in other places) which is verified during unmarshal.
// Normally all ORB messages are a sequence of bytes with the writer and reader
// knowing the structure. Using MARSHAL_DEBUG we include the schema/structure
// in the message so we can verify that all three are in sync - writer/reader/
// message. Since the message overhead is too high we do not enable this for
// FCS.
//

#ifdef DEBUG
#define	MARSHAL_DEBUG
#else
#undef	MARSHAL_DEBUG
#endif

#include <orb/invo/common.h>
#include <orb/buffers/Buf.h>
#include <orb/invo/invocation_mode.h>
#include <orb/flow/resource_defs.h>
#include <orb/flow/resource.h>
#include <sys/threadpool.h>
#include <orb/buffers/replyio.h>

// forward declarations
class Xdoor;

#ifdef _KERNEL_ORB
class endpoint;
#endif

// typedef for sequence number used by message_mgr.
// XX This should ideally be in message_mgr.h, if header files are cleaned up
typedef uint_t orb_seq_t;

// For specifying alignment/type of buffers
enum align_t { DEFAULT_ALIGN = 0, PAGE_ALIGN = 1 };

// For specifying the type of io to be done
enum iotype_t { UIO_TYPE = 0, PAGE_TYPE = 1, REGION_TYPE = 2,
		BUF_TYPE = 3, NO_TYPE = -1 };

//
// For specifying the type operation (READ/WRITE)
// OP_READ : Should be used in cases whenever we are receiving some data
//	from the other node.
//		Eg. 1. Read request to the server
//		    2. The server-pull request from Server to client
//			to pull the pages which need to be written to
//			the underlying filesystem.
// OP_WRITE : Should be used in cases whenever we need to send data to
//	the other node.
//		Eg. Write request to the server.
//
enum optype_t {CL_OP_READ = 0, CL_OP_WRITE = 1};

//
// These constants identfy the data type of the following field
// of a message. Used with MARSHAL_DEBUG version of marshalling.
// However they need to be defined always to avoid too many ifdefs in code.
//
enum { M_BOOLEAN,		// 0
	M_SHORT,		// 1
	M_OCTET,		// 2
	M_INT,			// 3
	M_LONG,			// 4
	M_UNSIGNED_INT,		// 5
	M_UNSIGNED_LONG,	// 6
	M_UNSIGNED_SHORT,	// 7
	M_OFFLINE,		// 8
	M_OBJECT,		// 9
	M_CHAR,			// 10=0xa
	M_LONGLONG,		// 11=0xb
	M_UNSIGNED_LONGLONG,	// 12=0xc
	M_FLOAT,		// 13=0xd
	M_DOUBLE,		// 14=0xe
	M_STRING,		// 15=0xf
	M_SEQ_HDR,		// 16=0x10
	M_SEQ,			// 17=0x11
	M_BYTES,		// 18=0x12
	M_REPLY,		// 19=0x13
	M_POINTER,		// 20=0x14
	M_HEADER };		// 21=0x15

//
// A MarshalStream is made up of a Region (list of Bufs). It maintains a
// cursor into Region and is used to writing during marshal and for
// reading during unmarshal operations.
//
class MarshalStream {
public:
	enum { DEFAULTCHUNK = 512 };
	enum cursor_t { CURSOR_ATSTART = 0, CURSOR_ATEND = 1 };

	// constructors
	// We initialize two variables in Marshalstream
	// alloc_size - size of first buffer to be allocated
	// chunksize - size of subsequent buffers
	// Both default to DEFAULTCHUNK (above) unless set in constructor
	MarshalStream(uint_t first, Environment *);
	MarshalStream(Environment *, uint_t first = DEFAULTCHUNK,
	    uint_t chunk = DEFAULTCHUNK);
	MarshalStream(Buf *b, Environment *);
	MarshalStream(Region &bl, Environment *);

	virtual ~MarshalStream();

	// Accessor changes the byte size allocated when more space is needed.
	void	set_chunksize(uint_t);

	// Accessor changes the byte size allocated the first time
	void	set_allocsize(uint_t);

	//
	// Returns the Region. Normally used to transfer ownership
	// by concatening this with another region Or
	// by using useregion with another MarshalStream
	// by initializing another MarshalStream with it
	//
	Region &region();

	//
	// Returns the region like region() does.  In addition it
	// makes sure that the region has at most 1 buffer. It makes a
	// destructive copy of contents of vbuf and if the copy fails,
	// those contents are lost. Caller should make a copy of
	// buffers it needs to preserve before calling this method.
	// Used in the doors/gateway and rsm_tramsport code.
	// If an exception is seen, an empty region is returned.
	//
	Region &coalesce_region(CORBA::Environment *env = NULL);

#ifdef _KERNEL
	// Converts the region to a mblk chain (using Region::Region2mblk)
	// hsize specifies the hint for how much space to leave for headers
	// max_size specifies the maximum size for each mblk
	// returns NULL on error and sets exception in Environment
	// On error, the region is disposed
	// do_esballoc is the flag set according to clconf property "lazy_free"
	// for the local adapter; if do_esblloc is true, esballoc routine can
	// be used instead of allocb in get_mblk.
	mblk_t *Region2mblk(uint_t hsize, uint_t max_size, bool do_esballoc,
		    Environment *);
#endif

	uint_t	span();
	bool	empty();

	// Appends buffer to MarshalStream
	// Leaves cursor at start of buf if where == CURSOR_ATSTART
	// Leaves cursor at end of buf if where == CURSOR_ATEND
	void   usebuf(Buf *, cursor_t where = CURSOR_ATSTART);

	// Appends region to MarshalStream
	// Leaves cursor at start of region r if where == CURSOR_ATSTART
	// Leaves cursor at end of region r if where == CURSOR_ATEND
	void   useregion(Region &r, cursor_t where = CURSOR_ATSTART,
			uint_t cs = DEFAULTCHUNK);

	//
	// alloc_chunk - allocates a contiguous chunk of bytes prior
	// to start of normal data for use by headers.
	// This must be performed prior to the insertion of any normal data.
	// The number of bytes for headers must be a multiple of sizeof (int).
	// Return number of bytes actually reserved.
	//
	uint_t	alloc_chunk(uint_t header_size);
#ifdef _LP64
	// Convenience method
	uint_t	alloc_chunk(size_t header_size);
#endif

	//
	// make_header - makes a header out of the space reserved by alloc_chunk
	// Cursor placed after header.
	// make_header only assures int alignment and header_size should
	// be sizeof (int) aligned
	//
	void *make_header(size_t header_size);

	//
	// write_header -
	// makes a header out of the space reserved by alloc_chunk
	// Writes the contents of p into the reserved space.
	// Cursor advances to after header.
	// write_header only assures int alignment and header_size should
	// be sizeof (int) aligned. Use write_header for headers that are not
	// performance critical or have 64-bit variables in the header.
	// The struct passed into write_header should have the first
	// uint32_t be reserved for marshal_debug #if MARSHAL_DEBUG
	//
	void write_header(void *p, size_t header_size);

	// Sends cursor to last buffer of MarshalStream, ready to write at end
	void goto_end();

	// Returns whether on not the cursor for MarshalStream is at end
	// Can be used put_bytes to determine whether is it safe add a Buf
	// XXX Need to investigate whether there is a different way
	//	to implement put_u_bytes in shm_transport
	bool at_end();

	void prepare();

	// Advance the marshalstream forward by nbytes
	// alloc set to true => that we are creating a hole and should
	// 	move the cursor to end of the Buffers allocating new ones as
	//	needed
	// alloc set to false => we are skipping some data and should only
	//	move the cursor as per the span of the Bufs. It is an error if
	//	we go past the end of the marshalstrem.
	// Another way to think about this is that alloc == true => writing
	// alloc == false => reading the Marshalstream
	void advance(uint_t nbytes, bool alloc);

	void trim_beginning();
	void trim_end();

	// Disposes of all the buffers in the stream
	void dispose();

	// XX The arguments are not const as the compiler had a problem with
	// the inlining
	void put_boolean(bool bo);
	void put_char(char ch);
	void put_octet(uint8_t uc);
	void put_short(int16_t sh);
	void put_unsigned_short(uint16_t us);
	void put_int(int);
	void put_long(int32_t lo);
	void put_unsigned_int(uint_t);
	void put_unsigned_long(uint32_t ul);
	void put_unsigned_longlong(uint64_t ull);
	void put_longlong(int64_t ll);
#ifdef ORBFLOAT
	void put_float(float fl);
	void put_double(double db);
#endif // ORBFLOAT

	void put_pointer(void *ptr);

	void put_string(const char *);
	void put_seq_hdr(const GenericSequence *);
	void put_seq(const GenericSequence *, uint_t size);
	void put_bytes(const void *, uint_t length, bool debug = true);

	// This method is virtual to allow shm_sendstream to use UBufs
	virtual int put_u_bytes(const caddr_t, uint_t, bool debug = true);

#ifdef _LP64
	// Convenience methods
	void put_bytes(const void *, size_t length, bool debug = true);
#endif

	void debug_put_word(const uint_t);

	// MarshalStream methods

	void nextbuf();

	// Read contents of a header into the specified structure
	// Advances and trims the header from the marshalstream too
	void read_header(void *, size_t length);

	// Splits MarshalSteram at offset. Returns tail portion in argument.
	// return bool status of whether it worked or not
	bool split_at_boundary(uint_t, MarshalStream &, os::mem_alloc_type);
	bool split_at_boundary(uint_t, Region &, os::mem_alloc_type);

	bool get_boolean();
	char get_char();
	uint8_t get_octet();
	int16_t get_short();
	uint16_t get_unsigned_short();
	int get_int();
	int32_t get_long();
	uint_t get_unsigned_int();
	uint32_t get_unsigned_long();
	int64_t get_longlong();
	uint64_t get_unsigned_longlong();
#ifdef ORBFLOAT
	float get_float();
	double get_double();
#endif // ORBFLOAT

	void *get_pointer();
	char *get_string();
	void get_string_cancel();
	void get_seq_hdr(GenericSequence *);
	void get_seq(GenericSequence *, uint_t size);
	void get_seq_cancel(uint_t size);
	void get_bytes(void *, uint_t length, bool debug = true);
	int  get_u_bytes(caddr_t, uint_t length, bool debug = true);

#ifdef _LP64
	// Convenience method
	void get_bytes(void *, size_t length, bool debug = true);
	int  get_u_bytes(caddr_t, size_t length, bool debug = true);
#endif

	void debug_get_word(const uint_t);

	void		set_env(Environment *e);
	Environment	*get_env();

	// internal function performing the allocation of a new buffer
	bool allocbuf(uint_t nbytes = 0);

protected:
	//
	// Function to allocate a new buffer
	// does the new of the appropriate buffer type
	//
	virtual Buf * newbuf(uint_t nbytes);

	uint_t get_word();

	// The number of bytes to allocate.
	// The flow control subsystem collects information on size
	// and attempts to create a big enough buffer.
	// After the initial buffer allocation, the chunksize is used.
	uint_t	alloc_size;

	// Specifies number of bytes to allocate when more space is required.
	// The IDL compiler specifies data size in many cases, which
	// means that this should only be used for unknown or very large
	// sized messages.
	uint_t   chunksize;

	Region	vbuf;
	cursor	cur;

private:
	Environment *env;

	// Disallow assignments and pass by value
	MarshalStream(const MarshalStream &);
	MarshalStream &operator = (MarshalStream &);
};

//
// sendstream - is a collection of MarshalStreams, regions,
// and counters. It keeps separate track of xdoors, offline buffers
// and marshaled data, before it is sent in a message.
//
class sendstream {
public:
	uint_t get_xdoorcount() const;

	MarshalStream	&get_MainBuf();
	MarshalStream	&get_XdoorTab();
	orb_msgtype	get_msgtype() const;
	invocation_mode	get_invo_mode() const;
#ifdef _KERNEL_ORB
	virtual	ID_node	&get_dest_node() = 0;
#endif
	Environment	*get_env();
	resources	*get_resourcep();

	void		set_oneway();
	void		set_twoway();

	// Deletes or frees the sendstream
	// In kernel ORB done() is same as delete this, so we provide a
	// default implementation. If needed this can be made virtual
	// XX for user land we sendstreams/recstreams can be allocated on the
	// stack too and need a different done method. We are doing this as
	// being a pure virtual for now. We may want to consider making
	// the kernel implementation be as general as the user one
	// instead of making done() be virtual
#ifdef _KERNEL_ORB
	void		done();
#else
	virtual void	done() = 0;
#endif

	// Provide length of sendstream
	uint_t		span();

	//
	// Make a header prior to the start of data.
	// Cursor placed after header.
	//
	void *make_header(size_t header_size);

	//
	// write_header -
	// makes a header out of the space reserved by alloc_chunk
	// Writes the contents of p into the reserved space.
	// Cursor advances to after header.
	// write_header only assures int alignment and header_size should
	// be sizeof (int) aligned. Use write_header for headers that are not
	// performance critical or have 64-bit variables in the header.
	// The struct passed into write_header should have the first
	// uint32_t be reserved for marshal_debug #if MARSHAL_DEBUG
	//
	void write_header(void *p, size_t header_size);

	//
	// alloc_chunk - allocates a contiguous chunk of bytes prior
	// to start of normal data for use by headers.
	// This must be performed prior to the insertion of any normal data.
	// header_size must be multiple of int size in bytes.
	// Return number of bytes actually reserved.
	//
	uint_t	alloc_chunk(uint_t header_size);
#ifdef _LP64
	// Convenience method
	uint_t	alloc_chunk(size_t header_size);
#endif

	//
	// send - transmits the data in the marshal streams to the destination.
	// The argument passed in is the orb message sequence number.
	// Failure occurs for following reasons:
	// a) destination node has failed and CMM has called node_died
	// b) This is a non-blocking invocation and there was a memory failure
	// c) User data with bad address (bulkio_uio and gateway both do this)
	//
	// When implementing a transport, the ownership rules for buffers are :
	// send() should return only after it is done using the buffers
	// (or copied the buffers). This way handlers can cleanup on return
	// instead of waiting for a separate done() indication from the
	// transport.
	//	The data_alloc flag in Buf structure (used to do put_offline)
	// indicates where the data was allocated. If both the data and the
	// Buf structure was allocated on the heap, then the transport takes
	// ownership for the buffers and destroys them (called buf->done()) when
	// done. This allows for zero-copy implementations for callers that
	// do not care for when the buffers are destroyed.
	//	In cases where the buffer and/or data is not on the heap, then
	// we allow the Buf constructor to specify a os::notify_t pointer.
	// If the notify pointer is not specified, then the transport should
	// either return only after sending the buffers or copy them.
	// If the notify pointer is specified, the transport is allowed to
	// keep a pointer to the data and the caller should not destroy the data
	// till the state in the notify structure has changed. (See Buf.cc)
	//
	// The boolean argument refers to whether the message needs a sequence
	// number.  The sequence number selected is returned
	virtual void send(bool, orb_seq_t &) = 0;

	// delegate to MainBuf
	// If any need to be transport-specific, make them virtual
	void put_boolean(const bool bo);
	void put_char(const char ch);
	void put_octet(const uint8_t uc);
	void put_short(const int16_t sh);
	void put_unsigned_short(const uint16_t us);
	void put_int(const int lo);
	void put_long(const int32_t lo);
	void put_unsigned_int(const uint_t ul);
	void put_unsigned_long(const uint32_t ul);
	void put_unsigned_longlong(const uint64_t ull);
	void put_longlong(const int64_t ll);
#ifdef ORBFLOAT
	void put_float(const float fl);
	void put_double(const double db);
#endif
	void put_pointer(const void *ptr);
	void put_string(const char *st);
	void put_seq_hdr(const GenericSequence *sh);
	void put_seq(const GenericSequence *se, uint_t si);
	void put_bytes(const void *bs, uint_t len);
	int  put_u_bytes(const caddr_t bs, uint_t len, bool debug = true);
#ifdef _LP64
	// Convenience methods
	void put_bytes(const void *bs, size_t len);
#endif
	void debug_put_word(const uint_t);

	void put_off_line(Region &, align_t);
	void put_off_line(Buf *, align_t);

	void put_xdoor(Xdoor *xdp);

	// See comments in service class for how to call these methods
	virtual replyio_client_t *add_reply_buffer(struct buf *, align_t);

	virtual bool check_reply_buf_support(size_t, iotype_t, void *,
	    optype_t);

	void put_offline_reply(Buf *, replyio_server_t *);

	// Called by put_offline_reply if send_reply_buf on replyio_server_t
	// fails, used in tcp_transport implementation (could be better)
	virtual bool send_reply_buf(Buf *, replyio_server_t *);

	sendstream(resources *resourceptr, MarshalStream *);

protected:
	virtual ~sendstream();

	uint_t		xdoorcount;
	resources	*resourcep;

	// Transports allocate a transport-specific Marshalstream for MainBuf
	// To avoid a lot of virtual function calls to get_MainBuf we store a
	// pointer the MainBuf in the base class.
	MarshalStream	*mainbufp;

	MarshalStream	XdoorTab;
	Region		OffLineBufs;

private:
	// Prevent assignments and pass by value
	sendstream(const sendstream &);
	sendstream &operator = (sendstream &);
};

#ifdef _KERNEL_ORB
//
// serv2_transport_info is information that transports want to transfer from
// the request recstream to the reply sendstream through the service_twoway
// object in the case of a two way invocation. Currently the only information
// transferred is a pointer to the endpoint that brought the request message.
//
class serv2_transport_info {
public:
	serv2_transport_info();
	~serv2_transport_info();

	endpoint	*get_endpoint();
	void		set_endpoint(endpoint *);
	bool		isvalid();
	void		invalidate();

private:
	// Endpoint the request arrived on. A hold on the endpoint object must
	// be maintained to prevent the endpoint object from getting destructed,
	// while this object maintains a pointer to the endpoint.
	endpoint	*prefer_ep;

	// Once it is determined that the endpoint preference is for an endpoint
	// that is not registered any more the valid flag is set to false. Once
	// valid is not true any more, any future retries for reserve_resources
	// will ignore this endpoint preference.

	bool		valid;
};
#endif // _KERNEL_ORB

//
// recstream - is a collection of MarshalStreams, regions,
// and counters used to receive a message. The transport separates
// marshalled data, xdoors, and offline buffers.
//
class recstream : public defer_task {
public:
	MarshalStream		&get_MainBuf();
	MarshalStream		&get_XdoorTab();
#ifdef _KERNEL_ORB
	virtual	ID_node		&get_src_node() = 0;
#endif

	// Rewind all the Marshal Streams/Regions
	void			prepare();

	// Deletes the recstream itself
	// See comments in sendstream class
#ifdef _KERNEL_ORB
	void			done();
#else
	virtual void		done() = 0;
#endif

	// Provide length of recstream
	uint_t			span();

	uint_t			get_xdoorcount() const;
	orb_msgtype		get_msgtype() const;
	void			set_oneway();
	void			set_twoway();
	invocation_mode		get_invo_mode() const;

	// Read the contents of a header into the specified structure
	// Advances and trims the header from the marshalstream too
	void read_header(void *, size_t length);

	// Get delegated to MainBuf
	bool		get_boolean();
	char		get_char();
	uint8_t		get_octet();
	int16_t		get_short();
	uint16_t	get_unsigned_short();
	int		get_int();
	int32_t		get_long();
	uint_t		get_unsigned_int();
	uint32_t	get_unsigned_long();
	int64_t		get_longlong();
	uint64_t	get_unsigned_longlong();
#ifdef ORBFLOAT
	float		get_float();
	double  	get_double();
#endif
	void		*get_pointer();
	char		*get_string();
	void		get_string_cancel();
	void		get_seq_hdr(GenericSequence *);
	void		get_seq(GenericSequence *, uint_t size);
	void		get_seq_cancel(uint_t size);
	void		get_bytes(void *, uint_t length);
	int		get_u_bytes(caddr_t, uint_t length);
#ifdef _LP64
	// Convenience method
	void		get_bytes(void *, size_t length);
	int		get_u_bytes(caddr_t, size_t length);
#endif
	void		debug_get_word(const uint_t);
	Xdoor		*get_xdoor();

	uint_t		get_off_line(Region &r);
	void		get_off_line_cancel();
	uint_t		get_off_line_hdr();
	void		get_off_line_bytes(void *, uint_t, Environment *);
	void		get_off_line_u_bytes(caddr_t, uint_t, Environment *);

	virtual void	get_offline_reply(replyio_client_t *);
	void	get_offline_reply_cancel(replyio_client_t *);

	virtual replyio_server_t *get_reply_buffer();
	virtual void get_reply_buffer_cancel();

	// Called by threadpool code when processing is deferred
	void		execute();

	virtual bool initialize(os::mem_alloc_type) = 0;

	recstream(orb_msgtype, invocation_mode);

#ifdef _KERNEL_ORB
	recstream(orb_msgtype, invocation_mode, endpoint *);
	virtual void transfer_serv2_transport_info(serv2_transport_info &);
#endif

	orb_seq_t		get_rec_seq() const;
	os::mem_alloc_type	get_rec_flag() const;

	void set_rec_seq(orb_seq_t);
	void set_rec_flag(os::mem_alloc_type);

protected:
	virtual	~recstream();

	void	get_offline_reply_inline_data(replyio_client_t *);

#ifdef _KERNEL_ORB
	// endpoint on which the message was received
	endpoint		*ep;
#endif

	uint_t			xdoorcount;
	invocation_mode		invo_mode;
	orb_msgtype		msgtype;

	MarshalStream		MainBuf;
	MarshalStream		XdoorTab;
	Region			OffLineBufs;

private:
	// Prevent assignments and pass by value
	recstream(const recstream &);
	recstream &operator = (recstream &);

	//
	// rec_seq and rec_flag fields are valid only when
	// recstream is on the deferred_msgs_list
	//
	orb_seq_t		rec_seq;
	os::mem_alloc_type	rec_flag;

};

#ifndef NOINLINES
#include <orb/buffers/marshalstream_in.h>
#endif  // _NOINLINES

#endif	/* _MARSHALSTREAM_H */
