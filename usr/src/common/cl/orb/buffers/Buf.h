/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _BUF_H
#define	_BUF_H

#pragma ident	"@(#)Buf.h	1.88	08/05/20 SMI"

#include <orb/invo/common.h>
#include <sys/types.h>
#include <sys/list_def.h>
#include <sys/os.h>
#include <sys/sysmacros.h>
#include <sys/errno.h>
#include <sys/refcnt.h>

#ifdef _KERNEL
#include <sys/uio.h>
#endif	// _KERNEL

// Buf
//
// A buffer is a contiguous piece of memory. It can be
// referenced with a virtual or physical or both. In case
// it has a physical address, it should fit completely within
// a page frame.
//
// Note:: All lengths are in bytes
class Buf : public _DList::ListElem, public refcnt {
public:
	// Type to indicate where the allocation happened from
	// USER_CONTEXT implies the data points to user data valid only in the
	//	context of the calling thread
	// OTHER implies the data structure is embedded in some other structure
	enum alloc_type { HEAP, STACK, USER_CONTEXT, OTHER };

public:
	// done
	// Function which knows what to do with the buffer, when done with it
	//
	virtual void	done();

	// dup
	// Requests the buffer to make another buffer
	// pointing to the same memory area, but with a new span.
	//
	// f is the offset from the base_address of the current buffer
	// len is the length of the new buffer
	// span is 0, and needs to be incremented if the buffer has valid data
	//
	// current buffer is left unmodified (other than potential ref counts)
	//
	// dup is used on the receive side where a region is split into
	// multiple regions. If a Buf type does not support dup it either
	// cannot be used on the receive side (e.g. UBuf) or can only be
	// used in cases where the transports takes care to allocate Bufs
	// such that no dup is needed
	//
	virtual Buf *dup(uint_t f, uint_t len, os::mem_alloc_type flag);

	uint_t	first() const;
	uint_t	last() const;
	uint_t	length() const;
	void    length(uint_t len);
	uint_t	span();
	uint_t	avail();
	bool	empty();

	uchar_t	*head() const;
	uchar_t	*base_address() const;
	void	base_address(uchar_t *va);

	void	advance(int i);

	// Advance the tail pointer
	void	incspan(int i);

	// Change the tail pointer
	void	last(uint_t i);

	// xfer_contents - function to copy the contents of the first size bytes
	//	of buffer to caller-allocated buffer (can be remote buffer)
	// This is a destructive copy - the Buffer done method is called
	//	This is equivalent to (if size == span)
	//		creating a cursor, getdata, done
	//	OR { copy contents of b; b->done(); }
	//	This is equivalent to (if size < span)
	//		creating a cursor, getdata, advance(size)
	//	OR { copy contents of b; b->advance(size); }
	//
	// This is a convenience function as the alternatives are cumbersome
	//	It also allows buf-specific copy methods, e.g., UioBuf
	//
	// Errors are indicated as exceptions in e.
	virtual void xfer_contents(char *dest, uint_t size, Environment *e);

	// copy_contents - function to copy the size bytes of the buffer
	// starting at offset off to caller-allocated buffer
	// This is a non-destructive copy - the Buf is left unmodified
	//
	// This is a convenience function as the alternatives are cumbersome
	//	It also allows buf-specific copy methods, e.g., UioBuf
	//
	// Errors are indicated as exceptions in e.
	virtual void copy_contents(char *dest, uint_t size, uint_t off,
		Environment *e);

#ifdef _KERNEL
	// Transfers ownership of contents of Buf to caller.
	// i.e. does xfer_contents, returning the contents in an mblk list.
	// Caller is responsible for doing the freemsg
	// Deletes the Buf structure by calling done()
	// hsize is the size of header space to be left at the beginning
	// of each mblk created. This is a hint only and may not be honored
	// in all cases.
	// max is the maximum allowable length (MBLKL()) of each mblk
	// max does not include hsize
	// XX Till the implementation is fully done, some Buf types
	// return an exception NO_RESOURCES if the size is larger than max
	// do_esballoc is the flag set according to clconf property "lazy_free"
	// for the local adapter; if lazy_free is 1, do_esballoc will be false.
	virtual mblk_t *get_mblk(uint_t hsize, uint_t max, bool do_esballoc,
		    Environment *);

	// Copies contents of Buf to caller i.e. does copy_contents,
	// returning the contents in an mblk list.
	// Caller is responsible for doing the freemsg
	// Leave the original Buf unchanged.
	// hsize is the size of header space to be left at the beginning
	// of each mblk created. This is a hint only and may not be honored
	// in all cases.
	// max is the maximum allowable length (MBLKL()) of each mblk
	// max does not include hsize
	mblk_t *copy2mblk(uint_t hsize, uint_t max, Environment *);

	// pointer to streams free function used by Buf_esballoc
	frtn_t *frtnp();

#endif	// _KERNEL

	// Calls done when refcount goes to zero
	void	refcnt_unref();

	// This method returns true if the Buf contains all the data in
	// itself or in data structures guaranteed to survive this Buf.
	// Returns false, if the data pointed to by this Buf is contained
	// in some other data structure.
	// is_on_heap == buf_alloc == HEAP && data_alloc == HEAP
	bool is_on_heap();

protected:
	// NOTE: Do not make the constructor and/or destructor be public
	// Should not be using Buf class directly, always use a derived class
	// nil_Buf is actually a good candidate for most cases
	// It would have been nice if Buf was an abstract class, but we
	// have cleaned it enough to make most Buf types to use the default
	// implemenations and many define few if any methods.

	//
	// constructors
	// the two alloc_type flags indicate where the Buf structure itself
	// is allocated and where the data buffer (va) is allocated
	//
	Buf(alloc_type bflag = HEAP, alloc_type dflag = HEAP);

	Buf(uchar_t *, uint_t len, alloc_type = HEAP, alloc_type = HEAP);

	// head indicates to leave room for a header at front
	// len >= head and first_ is set to head, span is 0
	// Pointer notify_t is to request notification on buffer destruction
	Buf(uchar_t *, uint_t len, uint_t head, alloc_type = HEAP,
	    alloc_type = HEAP, os::notify_t *np = NULL);

	// destructor
	virtual ~Buf();

	alloc_type buf_alloc;	// type of allocation of Buf instance
	alloc_type data_alloc;	// type of allocation of data pointed to by this
private:
	uint_t	length_;	// total length of allocated memory
	uint_t	first_; 	// offset to first byte that makes sense
	uint_t	last_;		// offset to byte after last that makes sense
	uchar_t *vAddr;		// Base virtual address

#ifdef _KERNEL
	frtn_t	freefunc;	// free function structure
#endif // _KERNEL

	os::notify_t *notify_ptr;	// Set to get notification in destructor

	// Disallow assignments and pass by value
	Buf(const Buf &);
	Buf &operator = (Buf &);
};

// GWBuf
// These are the buffers that the marshal buffer is going to use.
// This is a type of buffer that assumes that it has to free memory
// when whomever uses it gets done with it.
class GWBuf : public Buf {
public:
	// Create a word aligned buffer at least as big as specified.
	// Always does blocking memory allocation
	// To do non-blocking memory allocation use
	// GWBuf::alloc_buffer and nil_Buf (see marshalstream::newbuf for eg.)
	GWBuf(uint_t byte_size);

	// Gets rid of memory allocated/assigned when buffer was created
	~GWBuf();

	// static alloc function to allocate a buffer usable to pass to GWBuf
	// or nil_Buf
	static uchar_t *alloc_buffer(uint_t, os::mem_alloc_type = os::SLEEP);

private:
	// static free function to free the buffer allocated by alloc_buffer
	static void free_buffer(uchar_t *);

	// Disallow assignments and pass by value
	GWBuf(const GWBuf &);
	GWBuf &operator = (GWBuf &);
};

// nil_Buf is used in cases where the data buffer is in some other class
// When creating a buffer of this type, should be careful that the holder
// structure (nil_Buf) does not outlive the data itself.
// E.g, we could be pointing to inline data in the recstream. Bufs/regions
// are transferred around and may have a life even after the recsteam is
// deleted. In that case, the Buf below has pointers to garbage.
//
// We could have used Buf class directly, but it seems worthwhile to separate
// the class in case we need to add more methods/ASSERTs etc.
//
class nil_Buf : public Buf {
public:
	nil_Buf(alloc_type);

	nil_Buf(uchar_t *, uint_t len, uint_t span, alloc_type = HEAP,
		os::notify_t *np = NULL);

	// If data buffer is created with data_alloc = HEAP (The
	// second alloc_type parameter to the constructor) then it
	// will delete the data buffer in the destructor.
	//
	nil_Buf(uchar_t *, uint_t len, uint_t span, alloc_type,
		alloc_type, os::notify_t *np = NULL);

	Buf *dup(uint_t, uint_t, os::mem_alloc_type);

	~nil_Buf();
private:
	// Disallow assignments and pass by value
	nil_Buf(const nil_Buf &);
	nil_Buf &operator = (nil_Buf &);
};

// CBuf
// These are buffers used by the gateway when passed data allocated
// from C code (i.e., without calling new).  Like GWBuf, it frees
// memory when whomever uses it gets done with it.  Unlike GWBuf,
// it directly calls kmem_free.
//
class CBuf : public Buf {
public:
	CBuf(uchar_t *u, size_t sz);

	// Gets rid of memory allocated when buffer was created
	~CBuf();

protected:
	size_t	size;		// real size of allocated data in bytes

private:
	// Disallow assignments and pass by value
	CBuf(const CBuf &);
	CBuf &operator = (CBuf &);
};

#ifdef DEBUG
struct mstats {
	uint_t mblks_allocated;
	uint_t mblks_allocated_exist;
	uint_t mblk_dup;
	uint_t mblk_freed;
	uint_t mblk_transferred;
	uint_t mblk_destruct;
};
extern struct mstats mblk_stats;
#define	UPDATE_MBLK_STATS(f, v)	mblk_stats.f += (v)

#else	// DEBUG
#define	UPDATE_MBLK_STATS(f, v)

#endif	// DEBUG

#ifdef _KERNEL

//
// MBlkBuf
// Uses mblk as buffer. Used in kernel mode tcp_transport.
//
class MBlkBuf : public Buf {
public:
	// dup
	// Requests the buffer to make another buffer
	// pointing to the same memory area, but with
	// the given length.
	//
	Buf *dup(uint_t, uint_t, os::mem_alloc_type flag);

	//
	// Create a MblkBuf out of an existing mblk
	// size is the value to initialize the span to (should not be more than
	// the value bounded by m->b_rptr, m->b_wptr)
	//
	// The done routine will free up the mblk.
	//
	MBlkBuf(mblk_t *mblkp, uint_t size);

	//
	// Constructor for MBlkBuf that is used when the mblk will
	// be provided later.
	//
	MBlkBuf(alloc_type = OTHER);

	//
	// The MBlkBuf was created without an mblk.
	// This method loads the mblk.
	//
	void	use_mblk(mblk_t *mblkp, uint_t size);

	//
	// Transfers ownership of mblk to caller.
	// Caller is responsible for doing the freeb
	// See comments in base class Buf
	//
	mblk_t *get_mblk(uint_t, uint_t, bool, Environment *);

	//
	// The destructor can be called even if the allocb in the
	// constructor failed. This destructor however does not
	// depend on whether the allocb failed.
	//
	~MBlkBuf();

private:
	mblk_t *mblk;

	// Disallow assignments and pass by value
	MBlkBuf(const MBlkBuf &);
	MBlkBuf &operator = (MBlkBuf &);
};

// PpBuf
//	Used to send a list of physical pages
//	Note: that the assumption here is that the pages are not destroyed
//	while this buffer is still being used by the transport/ORB. See
//	bulkio_impl for usage.
class PpBuf : public Buf {
public:
	PpBuf(page_t *, uint_t, os::notify_t *);

	~PpBuf();
private:
	struct buf buf;

	// Disallow assignments and pass by value
	PpBuf(const PpBuf &);
	PpBuf &operator = (PpBuf &);
};

// UBuf
// Uses uio struct pointing to user/kernel buffer.
// Used for eliminating one-copy for user-land buffers
//
class UBuf : public Buf {
public:
	UBuf(void *ua, uint_t len, os::notify_t *np = NULL);

	void copy_contents(char *dest, uint_t size, uint_t off, Environment *e);

	// xfer_contents, get_mblk is same as base Buf's

private:
	void *uaddr;

	// Disallow assignments and pass by value
	UBuf(const UBuf &);
	UBuf &operator = (UBuf &);
};

// UioBuf
// Uses uio struct pointing to user/kernel buffer.
// Used for eliminating one-copy for user-land buffers
//
class UioBuf : public Buf {
public:
	UioBuf(struct uio *uiop);

	void copy_contents(char *dest, uint_t size, uint_t off, Environment *e);

	void xfer_contents(char *dest, uint_t size, Environment *e);

	// get_mblk is same as base Buf's

private:
	uio_t _uio;	// copy of uio struct
	iovec_t _iov[DEF_IOV_MAX];

	// Disallow assignments and pass by value
	UioBuf(const UioBuf &);
	UioBuf &operator = (UioBuf &);
};

#endif	// _KERNEL

// cursor
// Used to read/write a Buf
//
class cursor {
public:
	cursor();
	cursor(Buf *b, uint_t w);

	// Like constructors but for reinitializing
	void setbuf(Buf *b);
	void setbuf(Buf *b, uint_t c);

	uint_t getdata(uint_t n, void *);
	uint_t putdata(uint_t n, const void *);
	// See comments in Marshalstream::advance
	uint_t skip(uint_t nbytes, bool alloc);
	uint_t get_u_data(uint_t, void *, int *);
	uint_t put_u_data(uint_t, const void *, int *);

	//
	// alloc_chunk - allocates a contiguous chunk of bytes prior
	// to start of normal data for use by headers.
	// This must be performed prior to the insertion of any normal data.
	// The number of bytes for headers must be a multiple of sizeof (int).
	// Return number of bytes actually reserved.
	uint_t	alloc_chunk(uint_t header_size);

	//
	// make_header - creates a header out of the space
	//	reserved by alloc_chunk. Cursor placed after header.
	//
	void *	make_header(uint_t header_size);

	void rewind();
	void trim_prefix();
	void trim_suffix();

	Buf *buffer() const;

protected:
	Buf *bu;
	uint_t    cursor_;	// offset from bu->vAddr
private:
	// Disallow assignements and pass by value
	cursor(const cursor &);
	cursor &operator = (cursor &);
};

// Region
class Region {
public:
	uint_t	span();
	bool	empty();

	//  positioning operations

	// Leaves the cursor pointing to first buffer of region
	void rewind();
	// Leaves the cursor pointing to last buffer of region
	void ffrwrd();

	//  List manipulation operations

	// reaps off first buffer in region.
	Buf *reap();

	// get rid of the buffers themselves (done() is called)
	void    dispose();

	// Leaves prefix in current Region, the tail into the
	// new region. Splits at current buffer, i.e., the current
	// is part of the tail
	void split(Region &r);

	// Leaves prefix in new region, tail in current Region
	// r should be empty
	void back_split(Region &r);

	//  Leaves the param empty.
	void concat(Region &r);

	//  Buffer insertion operations

	// Notice that a region gains ownership of the buffers
	// it gets passed. The region "delete"s them when the
	// region is deleted.
	void append(Buf *b);

	// puts the buffer at the beginning;
	void prepend(Buf *b);

	// inserts the buffer before the current buffer.
	void insert(Buf *b);

	//  Buffer traversal operations

	// given region offset, returns Buf offset
	// Leaves buffer list pointer at offset
	// Returns 0, if offset fails at correct Buf boundary or is past end
	uint_t goto_offset(uint_t offset);

	// Like goto_offset, but if the buffer offset is not 0
	// it inserts an extra buffer to ensure that "offset"
	// is exactly at the beginning of a buffer.
	// If either the buffer being split does not support dup or we are
	// out of memory, it fails and returns false. Else returns true
	bool goto_boundary(uint_t offset, os::mem_alloc_type flag);

	void goto_next();

	// Return the current buffer from the doubly linked list of buffers
	Buf *buffer()	const;

	// xfer_contents - function to copy the contents of the first size bytes
	//	of region to caller-allocated buffer (can be remote buffer too)
	// This is a destructive copy - the first size bytes of the region
	//	are removed or region is empty after this call
	//	This is equivalent to
	//		creating a MarshalStream, get_bytes, trim_beginning
	//
	// This is a convenience function as the alternatives are cumbersome
	//
	// Errors are indicated as exceptions in e.
	// If there is an exception, we advance past the rest of the size bytes
	// and delete them from the region
	void xfer_contents(char *dest, uint_t size, Environment *e);

	// copy_contents - function to copy the contents of the full region
	//	to a caller-allocated buffer (can be remote buffer too)
	// This is a non-destructive copy - the region is unmodified
	//		except buffers is left atfirst
	//	This is equivalent to
	//		creating a RMarshalStream, get_bytes, rewind
	//	OR for (r.rewind; b = (Buf *) r; r.goto_next()) {
	//		b->copy_contents(..); }
	//		r.rewind();
	//
	// This is a convenience function as the alternatives are cumbersome
	//
	// Errors are indicated as exceptions in e.
	// On error - Region is left unmodified, buffers is left atfirst
	void copy_contents(char *dest, uint_t size, Environment *e);

#ifdef _KERNEL
	// get_mblk is used in mblk_handler
	// uint_t argument is the size, which should be <= span() or region
	// The first size bytes of the region are transferred into a new mblk
	// allocated. Those bytes are removed from the Region
	// XX the semantics on error are clumsy. Need to cleanup
	mblk_t *get_mblk(uint_t, Environment *);

	// Converts the Region to a mblk chain (using Buf::get_mblk)
	// hsize specifies the hint for how much space to leave for headers
	// max_size specifies the maximum size for each mblk
	// returns NULL on error and sets exception in Environment
	// On error, the Region is disposed
	mblk_t *Region2mblk(uint_t hsize, uint_t max_size, bool do_esballoc,
		    Environment *);

	// Copies the contents of the Region to a mblk chain
	// hsize specifies the hint for how much space to leave for headers
	// max_size specifies the maximum size for each mblk
	// returns NULL on error and sets exception in Environment
	// On success and on error, the Region is left unmodified
	mblk_t *copy2mblk(uint_t hsize, uint_t max_size, Environment *);

#endif	// _KERNEL

	Region() { }

	~Region() { ASSERT(buffers.empty()); }
private:
	/*CSTYLED*/
	typedef IntrList<Buf, _DList> Buflist;
	Buflist buffers;

	// Disallow assignments and pass by value
	Region(const Region &);
	Region &operator = (Region &);
};

#ifdef _KERNEL
// utility class for managing a chain of mblks
// with emphasis on speed of appends
class mblk_chain {
public:
	mblk_chain();
	~mblk_chain();

	// Append an mblk to the chain
	void append(mblk_t *);

	// Append an mblk chain to the chain
	void append_chain(mblk_t *);

	// Dispose - freemsg
	void dispose();

	// Returns msgdsize() of mblk chain
	uint_t	span();

	// Destructive - transfers ownership of mblk chain to caller
	mblk_t *get_mblk_chain();

	// Return pointer to last mblk in chain.
	// Only valid till another mblk_chain operation
	// Caller is responsible for using the pointer returned appropriately
	mblk_t *last_mblk();
private:
	mblk_t *head;
	mblk_t **tailp;
};
#endif	// _KERNEL

// If inlines are enabled the _in.h headers are included here,
// otherwise non-inline methods are built into Buf.cc

#ifndef NOINLINES
#include <orb/buffers/Buf_in.h>
#endif	// _NOINLINES

#endif	/* _BUF_H */
