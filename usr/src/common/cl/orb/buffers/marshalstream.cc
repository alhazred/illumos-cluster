/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)marshalstream.cc	1.45	08/05/20 SMI"

#include <orb/infrastructure/orb.h>
#include <orb/buffers/marshalstream.h>
#include <sys/mc_probe.h>
#include <orb/msg/orb_msg.h>
#include <sys/ddi.h>

//  Marshal stream methods.

// virtual destructor
MarshalStream::~MarshalStream()
{
	ASSERT(vbuf.empty());
	env = NULL;
}

// newbuf
// To create a new empty buffer to the marshalstream
// This is a virtual function and can be made transport specific
Buf *
MarshalStream::newbuf(uint_t nbytes)
{
#ifdef _KERNEL
	if (get_env()->is_nonblocking()) {
		uchar_t *datap = GWBuf::alloc_buffer(nbytes, os::NO_SLEEP);
		if (datap == NULL) {
			get_env()->system_exception(CORBA::WOULDBLOCK(nbytes,
			    CORBA::COMPLETED_NO));
			return (NULL);
		}
		/*CSTYLED*/
		Buf *b = new (os::NO_SLEEP) nil_Buf(datap, nbytes, 0,
		    Buf::HEAP, Buf::HEAP);
		if (b == NULL) {
			delete [] datap;
			get_env()->system_exception(CORBA::WOULDBLOCK(
			    (uint_t)sizeof (nil_Buf), CORBA::COMPLETED_NO));
		}
		return (b);
	}
#endif
	return (new GWBuf(nbytes));
}

// allocbuf
bool
MarshalStream::allocbuf(uint_t nbytes)
{
	// If no current buffer being edited, tries to get the current buffer
	// in region.  Else, it requests the next buffer in region.
	// Notice that when a buffer is taken from a region, it starts
	// being written from  its first valid position.

	MC_PROBE_1(allocbuf_start, "clustering orb message", "",
		tnf_ulong, nbytes, nbytes);

	if (cur.buffer()) {
		vbuf.goto_next();
	}
	Buf *b = vbuf.buffer();
	if (!b) {
		// Allocate the max of nbytes or alloc_size
		b = newbuf(MAX(nbytes, alloc_size));
		if (b) {
			vbuf.append(b);
			ASSERT(vbuf.buffer() == b);

			// After initial buffer allocation use chunk size.
			alloc_size = chunksize;
		} else {
			MC_PROBE_0(allocbuf_end, "clustering orb message", "");
			return (false);
		}
	}
	cur.setbuf(b);
	MC_PROBE_0(allocbuf_end, "clustering orb message", "");
	return (b != NULL);
}

//
// alloc_chunk - allocates a contiguous chunk of bytes prior
// to start of normal data for use by headers.
// This must be performed prior to the insertion of any normal data.
// The number of bytes for headers must be a multiple of the word size in bytes.
//
// Return number of bytes actually reserved.
//
uint_t
MarshalStream::alloc_chunk(uint_t header_size)
{
	// Some transports will have allocated the first buffer already.
	// While other transports will not have allocated a buffer.
	//
	Buf	*bufferp = cur.buffer();
	if (bufferp == NULL) {
		// Allocate initial buffer
		bufferp = newbuf(MAX(alloc_size, chunksize));
		if (bufferp == NULL) {
			// Buffer allocation failed,
			// because memory was not immediately available.
			ASSERT(get_env()->exception());
			ASSERT(CORBA::WOULDBLOCK::_exnarrow(get_env()->
			    exception()));
			return (0);
		}

		// Place buffer in MarshalStream
		vbuf.append(bufferp);
		cur.setbuf(vbuf.buffer());

		// After initial buffer allocation use chunk size.
		alloc_size = chunksize;
	} else {
		ASSERT(bufferp == vbuf.buffer());
	}
	return (cur.alloc_chunk(header_size));
}

// usebuf
void
MarshalStream::usebuf(Buf * b, cursor_t where)
{
	// Appends the buffer to end of MarshalStream
	// XXX It would be nice to ASSERT that either the cursor is at end
	// of current region or the region is empty
	// if where is CURSOR_ATEND
	//	Leave the stream cursor at the end of buf b
	// else Leaves the stream to start writing at the start of buf b

	vbuf.append(b);
	if (where == CURSOR_ATEND)
		goto_end();
	else
		cur.setbuf(vbuf.buffer());
}

// useregion
void
MarshalStream::useregion(Region &r, cursor_t where, uint_t cs)
{
	// Attaches region r to end of MarshalStream
	// XXX It would be nice to ASSERT that either the cursor is at end
	// of current region or the region is empty
	// if where is CURSOR_ATEND
	//	Leave the stream cursor at the end of region r
	// else Leaves the stream to start writing at the start of region r

	chunksize = cs;
	vbuf.concat(r);
	if (where == CURSOR_ATEND)
		goto_end();
	else
		cur.setbuf(vbuf.buffer());
}

//
// coalesce_region - if the data does not already fit entirely in one buffer,
//	the data in the entire region is relocated to one buffer.
//	Preserves any reserved headers in first Buf.
//	The uint_t argument allows us to expand this to more buffers later
//
//	XXX Failures result in data loss
//
Region &
MarshalStream::coalesce_region(CORBA::Environment *envp /* = NULL */)
{
	uint_t		len;		// byte length of data to add to new buf
	uint_t		headers;	// byte length of headers
	Buf		*b;
	// Need a separate Environment as we may be here due to an exception
	Environment	e;
#ifdef DEBUG
	uint_t		old_len = vbuf.span();	// original data byte length
#endif
	//
	// If len is 0, it discards empty buffers other than first buffer
	// to preserve reserved headers.
	//
	// There can be 3 cases here:
	// 1. There are no buffers - nothing to coalesce.
	// 2. There is only 1 buffer - just return the first
	// 3. There are > 1 buffers - put all other buffers in the
	// first buffer. It is possible to find some other buffer in
	// list that has sufficient space to hold data from all
	// buffers, but coalesceing into that buffer is more work.
	//
	b = vbuf.reap();
	//
	// If the buffer list was empty, then there is nothing to
	// coalesce.
	//
	if (b == NULL) {
		return (region());
	}
	len = vbuf.span();	// span of all but first buffer
	if (b->is_on_heap() && (b->avail() >= len)) {
		//
		// If can fit rest of data in first buffer, use first
		// buffer. If the first buffer is not on the heap
		// (both Buf and data), we cannot use it as the buffer
		// for coalescing.  Copy rest of region to buffer.
		// This also handles the case of single buffer, len is
		// 0.
		//
		vbuf.xfer_contents((char *)b->head() + b->span(), len, &e);
		b->incspan((int)len);
	} else {
		// Cannot fit all data in first buffer

		//
		// First, calculate length to include length of first buffer.
		//
		len += b->span();		// span now includes full vbuf
		headers = b->first();
		//
		// Put back first buffer into region, becase we
		// removed it from there in the beginning.
		//
		vbuf.prepend(b);
		//
		// Allocate a new buffer that is sufficiently large to
		// hold all buffers
		//
		b = newbuf(roundup(len + headers, (uint_t)sizeof (ulong_t)));

		if ((b == NULL) || (b->avail() < (len + headers))) {
			//
			// newbuf failed to allocate buffer or could
			// not allocate a large enough buffer
			//
#ifdef DEBUG
			//
			// SCMSGS
			// @explanation
			// While supporting an invocation, the system wanted
			// to create one buffer that could hold the data from
			// two buffers. The system cannot create a big enough
			// buffer. After generating another system error
			// message, the system will panic. This message only
			// appears on debug systems.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "clcomm: coalesce_region request(%d) > MTUsize(%d)",
			    len + headers, b->avail());
#endif
			e.system_exception(CORBA::BAD_PARAM(
				EOVERFLOW, CORBA::COMPLETED_NO));
		} else {
			b->incspan((int)(len + headers));

			// Preserve any reserved space at head of buffer
			b->advance((int)headers);

			// Transfer the data from vbuf to b, emptying vbuf
			vbuf.xfer_contents((char *)b->head(), len, &e);
		}
	}
	if (e.exception()) {
		//
		// Delete the newly allocated buffer or the buffer to
		// which transferring contents failed. The caller must
		// make copy of any buffer it needs before calling
		// coalesce_region() because the buffers are destroyed
		// in case of exception.
		//
		if (b) {
			b->done();
		}
		dispose();
		//
		// Before allowing this method to return:
		// 1. The code must be redesigned to prevent data
		// loss, and

		// 2. Must decide whether to return the first or
		// second exception generated by functions called by
		// coalesce_region. We return the second exception
		// only if the caller wants to know about it. The
		// caller indicates this by specifying a non-null
		// envp. See bug 4869237.

		//
		// If the caller is interested in exceptions generated
		// here, transfer this exception to the caller.
		//
		if (envp != NULL) {
			envp->exception(e.release_exception());
		} else {
			// Tell someone what the error was
			e.exception()->
			    print_exception("!MarshalStream::coalesce_region");

			//
			// SCMSGS
			// @explanation
			// While supporting an invocation, the system wanted
			// to combine buffers and failed. The system
			// identifies the exception prior to this message.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: Exception in coalescing region - "
			    "Lost data");
		}
	} else {
		// At end of above, original vbuf should be empty and all the
		// data should be in Buf b.
		ASSERT(vbuf.empty());
		vbuf.prepend(b);
		ASSERT(old_len == b->span());
	}
	//
	// current pointer in vbuf is at head (prepend to empty list did that)
	//
	return (region());
}

void
MarshalStream::goto_end()
{
	vbuf.ffrwrd();		// go to last buffer in region
	Buf *b = vbuf.buffer();
	if (b) {
		cur.setbuf(b, b->last());
	} else {
		cur.setbuf(NULL);
	}
}


bool
MarshalStream::at_end()
{
	return (vbuf.empty());
	// XXX This is wrong but sufficient
	// The caller uses this as a check for doing some optimizations
	// The right thing to do is actually not at_end, but a check for
	// whether the cursor is at the end of a buffer and modify usebuf to
	// do a insert_a. Till then, this is sufficient for the gateway case
}

// advance
void
MarshalStream::advance(uint_t l, bool alloc)
{
	uint_t done;

	// if alloc is false (i.e. reading), span must be at least l
	CL_PANIC(alloc || (span() >= l));

	while ((done = cur.skip(l, alloc)) < l) {
		l -= done;
		if (!allocbuf(l)) {
			ASSERT(get_env()->sys_exception());
			ASSERT(alloc);	// should be in this code for alloc only
			break;
		}
	}
}

// trim_beginning
void
MarshalStream::trim_beginning()
{
	// Deletes the beginning of the region up to the current cursor
	Region re;

	if (cur.buffer()) {
		ASSERT(vbuf.buffer() == cur.buffer());
		cur.trim_prefix();
	}
	vbuf.back_split(re);
	re.dispose();
	cur.setbuf(vbuf.buffer());
}

//
// trim_end - deletes the end of the region starting at the current cursor.
// The current buffer is not deleted. At the end of the operation, the
// cursor is positioned at the end of data.
//
void
MarshalStream::trim_end()
{
	Region re;

	if (cur.buffer() == NULL) {
		// No buffer means nothing to trim
		return;
	}
	ASSERT(vbuf.buffer() == cur.buffer());

	// Trim data past cursor in current buffer
	cur.trim_suffix();

	// Discard the trailing buffers
	nextbuf();
	vbuf.split(re);
	re.dispose();

	// Position cursor at end of data.
	goto_end();
}

// put_string
void
MarshalStream::put_string(const char *st)
{
	debug_put_word(M_STRING);
	if (st == NULL) {
		put_unsigned_long(0);
	} else {
		ASSERT(os::strlen(st) < UINT_MAX);
		uint_t len = (uint_t)os::strlen(st);
		put_unsigned_long(len+1); // extra 1 is terminating null char
		put_bytes(st, len);	// skip the terminator
	}
}


// put_bytes
void
MarshalStream::put_bytes(const void *s, uint_t len, bool debug)
{
	// Loops through the given buffer allocating as many bufs as needed.

	uint_t wrn;

	// must have a valid environment in case we need to get a new Buf
	ASSERT(get_env() != NULL);

	if (s == NULL) {
		CL_PANIC(len == 0);
	}
	if (debug) {
		debug_put_word(M_BYTES);
		debug_put_word(len);
	}
	if (s != NULL) {
		while ((wrn = cur.putdata(len, s)) < len) {
			s = (char *)s + wrn;
			len -= wrn;
			if (!allocbuf(len)) {
				ASSERT(get_env()->sys_exception());
				break;
			}
		}
	}
}


// Like put_bytes, except it copies from the current user address space
int
MarshalStream::put_u_bytes(const caddr_t s, uint_t len, bool debug)
{
	char *ss = (char *)s;
	uint_t wrn;
	int error = 0;

	if (debug) {
		debug_put_word(M_BYTES);
		debug_put_word(len);
	}
	while ((wrn = cur.put_u_data(len, (void *)ss, &error)) < len) {
		if (error)
			break;
		ss = ss + wrn;
		len -= wrn;
		if (!allocbuf(len)) {
			ASSERT(get_env()->sys_exception());
			break;
		}
	}
	return (error);
}

// put_seq_hdr
void
MarshalStream::put_seq_hdr(const GenericSequence *seqp)
{
	debug_put_word(M_SEQ_HDR);

	//
	// The length of the sequence should not exceed
	// specified maximum sequence length. When running no-debug
	// the system will just create bigger data structures.
	//
	ASSERT(seqp->length() <= seqp->maximum());

	// Check that seqp is not null
	if (seqp) {
		put_unsigned_long(seqp->maximum());
		put_unsigned_long(seqp->length());
	} else {
		// The sequence is empty
		put_unsigned_long(0);
		put_unsigned_long(0);
	}
}


// put_seq
void
MarshalStream::put_seq(const GenericSequence *seqp, uint_t size)
{
	debug_put_word(M_SEQ);

	if (seqp) {
		uint_t seq_max = seqp->maximum();
		uint_t seq_len = seqp->length();
		//
		// The length of the sequence must not exceed the
		// maximum sequence length.
		//
		ASSERT(seq_len <= seq_max);
		if (seq_len > seq_max) {
			//
			// The sequence has overflown.
			// Resetting the length to the maximum
			// so that the buffer does not overrun
			// on the recieving side.
			//
			seq_len = seq_max;
			//
			// Add RED Level Debug once implemented
			//
		}
		put_unsigned_long(seq_max);
		put_unsigned_long(seq_len);

		// Find the actual number of bytes to be marshaled
		uint_t len = seq_len * size;

		// put bytes if length is non-zero.
		if (len != 0) {
			put_bytes(seqp->buf(), len);
		}
	} else {
		//
		// The sequence is empty
		//
		put_unsigned_long(0);
		put_unsigned_long(0);
	}
}

//   Read Marshal Stream Methods.

//
// debug_get_word - supports MARSHAL_DEBUG.
// Otherwise this method becomes an inlined empty function.
//
#ifdef MARSHAL_DEBUG
void
MarshalStream::debug_get_word(const uint32_t expected_type)	{
	uint32_t	actual_type = get_word();
	if (expected_type != actual_type) {
		os::tracedump();
		//
		// SCMSGS
		// @explanation
		// When MARSHAL_DEBUG is enabled, the system tags every data
		// item marshalled to support an invocation. This reports that
		// the current data item in the received message does not have
		// the expected type. The received message format is wrong.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: Marshal Type mismatch. Expecting type %d "
		    "got type %d",
		    expected_type, actual_type);
	}
}
#endif

//  get_word
uint32_t
MarshalStream::get_word()
{
	uint32_t ul;
	get_bytes((void *)&ul, (uint_t)sizeof (ul), false);
	return (ul);
}

// get_string
char *
MarshalStream::get_string()
{
	uint_t ul;
	char *vp;

	debug_get_word(M_STRING);
	ul = get_unsigned_long();
	if (ul == 0) {
		vp = NULL;
	} else {
		vp = new char[ul];
		get_bytes((void *)vp, ul - 1);	// terminator is not sent
		vp[ul - 1] = '\0';
	}
	return (vp);
}

// get_string_cancel
void
MarshalStream::get_string_cancel()
{
	uint_t ul;

	debug_get_word(M_STRING);
	ul = get_unsigned_long();
	if (ul != 0) {
		get_bytes(NULL, ul - 1);	// terminator is not sent
	}
}

// get_bytes
// The buffer pointer to by vp must be atleast l bytes in size
void
MarshalStream::get_bytes(void *vp, uint_t l, bool debug)
{
	uint_t rw;

	if (debug) {
		debug_get_word(M_BYTES);
		debug_get_word(l);
	}
	if (vp == NULL) {
		// NULL pointer passed in if need to skip a get_bytes
		// XX We can use a different call but this seems cleaner
		advance(l, false);
		return;
	}
	while ((rw = cur.getdata(l, vp)) < l) {
		vp = (char *)vp + rw;
		l -=  rw;
		nextbuf();
		ASSERT(cur.buffer());
	}
}

// get_u_bytes
// similar to get_bytes, but gets the data into a userland buffer
int
MarshalStream::get_u_bytes(caddr_t vp, uint_t l, bool debug)
{
	char *cvp = (char *)vp;
	uint_t rw;
	int error = 0;

	if (debug) {
		debug_get_word(M_BYTES);
		debug_get_word(l);
	}
	while ((rw = cur.get_u_data(l, cvp, &error)) < l) {
		cvp =  cvp + rw;
		l -=  rw;
		if (error) {
			// Advance the RMarshalStream to simulate the read
			advance(l, false);
			return (error);
		}
		nextbuf();
		ASSERT(cur.buffer());
	}
	return (error);
}

// get_seq_hdr
void
MarshalStream::get_seq_hdr(GenericSequence *seqp)
{
	debug_get_word(M_SEQ_HDR);
	uint_t _maximum = get_unsigned_long();
	uint_t _length = get_unsigned_long();

	//
	// The sequence length must not exceed the maximum sequence length
	//
	ASSERT(_length  <= _maximum);
	if (_length > _maximum) {
		//
		// In case of an error where the length is to big,
		// the maximum is increased so that the system
		// can continue to operate, which is better than
		// a  guaranteed panic.
		//
		_maximum = _length;
		// Add a Red Level Debug printf once implemented
	}
	ASSERT(!seqp->release());
	// _release will be set to false here. The caller of this method
	// usually allocates buffer soon after calling this method.
	// _release will be set to true at the time of buffer allocation.
	seqp->_load(_maximum, _length, NULL, false);
}

// get_seq
void
MarshalStream::get_seq(GenericSequence *seqp, uint_t size)
{
	debug_get_word(M_SEQ);

	uint_t _maximum = get_unsigned_long();
	uint_t _length = get_unsigned_long();
	//
	// The sequence length must not exceed the maximum sequence length
	//
	ASSERT(_length  <= _maximum);
	if (_length > _maximum) {
		//
		// In case of an error where the length is to big,
		// the maximum is increased so that the system
		// can continue to operate, which is better than
		// a  guaranteed panic.
		//
		_maximum = _length;
		// Add a Red Level Debug printf once implemented
	}
	ASSERT(!seqp->release());
	// seq->_release = true;

	// TODO: we should not be doing new char[],
	// we should be doing new T[] whatever the type is - allocbuf?
	uint_t seq_size = _maximum * size;
	void *_buffer = 0;

	// allocate buffer of maximum size
	_buffer = new char[seq_size];

	// Find the actual number of bytes we need read in.
	seq_size = _length * size;

	// Read in the bytes if _length > 0.
	if (seq_size > 0) {
		get_bytes(_buffer, seq_size);
	}

	seqp->_load(_maximum, _length, _buffer, true);
}

// get_seq_cancel
void
MarshalStream::get_seq_cancel(uint_t size)
{
	debug_get_word(M_SEQ);
	(void) get_unsigned_long();
	uint_t _length = get_unsigned_long();
	get_bytes(NULL, _length * size);
}

//
// sendstream methods
//

//
// sendstream constructor
//
// The transports allocate a transport-specific Marshalstream for MainBuf
// To avoid a lot of virtual function calls to get_MainBuf we store a
// pointer the MainBuf in the base class.
//
// Allocate a small XdoorTab as we usually have a small number of xdoors
// If we have more than that, we use the estimate in resourceptr
// We do not use the estimate in resourceptr always as it is not accurate
// enough - arrays of objects and non-rxdoor based objects cause this
// number to be overestimated.
//
sendstream::sendstream(resources *rptr, MarshalStream *mbp) :
	xdoorcount(0),
	resourcep(rptr),
	mainbufp(mbp),
	XdoorTab(rptr->get_env(), resources::XDOORCHUNK,
	    rptr->send_xdoortab_size())
{
	ASSERT(mbp != NULL);

	// We should be allocating Bufs for XdoorTab only on first xdoor marshal
	ASSERT(XdoorTab.empty());
}

sendstream::~sendstream()
{
	ASSERT(mainbufp->empty());
	mainbufp = NULL;
	XdoorTab.dispose();
	OffLineBufs.dispose();
	resourcep = NULL;
}

void
sendstream::put_off_line(Region &r, align_t)
{
	debug_put_word(M_OFFLINE);
	put_unsigned_long(r.span());
	OffLineBufs.concat(r);
}

void
sendstream::put_off_line(Buf *b, align_t)
{
	debug_put_word(M_OFFLINE);
	put_unsigned_long(b->span());
	OffLineBufs.append(b);
}

replyio_client_t *
sendstream::add_reply_buffer(struct buf *, align_t)
{
	debug_put_word(M_REPLY);
	return (NULL);
}

bool
sendstream::check_reply_buf_support(size_t, iotype_t, void *, optype_t)
{
	return (false);
}

void
sendstream::put_offline_reply(Buf *src, replyio_server_t *dest)
{
	replyio_server_t::send_reply_buf_result_t srb_result;

	// This sendstream does not support reply optimization
	// send it through regular data path
	debug_put_word(M_REPLY);

	// Increment the generation in the server_Buf, this is sent to the
	// remote side as a way of weeding out older reply ios
	dest->incr_generation();

	ASSERT(src != NULL);

	// Does not make sense to be sending 0 length reply buffers
	CL_PANIC(src->span() > 0);

	// We require that send_reply_buf does not write anything in MainBuf
	// as otherwise the put_booleans here will not be synchronized with
	// the gets in get_offline_reply
#ifdef DEBUG
	uint_t orig_span = get_MainBuf().span();
#endif
	// send_reply_buf copies the data to the remote node
	// If successful returns true and callee is responsible for
	// calling src->done().
	// If unsuccesful, returns false and "src" should be left unmodified
	// We first call send_reply_buf methods on the replyio_server_t,
	// if that is unsuccessful we call send_reply_buf on the sendnstream

	srb_result = dest->send_reply_buf(src, this);

	if (srb_result == replyio_server_t::SEND_REPLY_BUF_SUCCESS ||
	    (srb_result == replyio_server_t::SEND_REPLY_BUF_RETRY &&
	    send_reply_buf(src, dest))) {
		ASSERT(get_MainBuf().span() == orig_span);
		put_boolean(true);
	} else {
		ASSERT(get_MainBuf().span() == orig_span);
		put_boolean(false);
		put_off_line(src, DEFAULT_ALIGN);
		// do not do src->done, transferred ownership to put_off_line
	}
}

// If this routine is called on a sendstream derived class that has not
// overriden this method, then it implies we cannot support reply optimization
// for this transaction and we discovered this while sending the reply
// Returning false, makes sendstream::put_offline_reply use regular offline
// data.
bool
sendstream::send_reply_buf(Buf *, replyio_server_t *)
{
	return (false);
}

//
// recstream methods
//

recstream::~recstream()
{
#ifdef _KERNEL_ORB
	if (ep) {
		((refcnt *) ep)->rele();
		ep = NULL;
	}
#endif
	MainBuf.dispose();
	XdoorTab.dispose();
	OffLineBufs.dispose();
}

uint_t
recstream::get_off_line_hdr()
{
	debug_get_word(M_OFFLINE);
	return (get_unsigned_long());
}

uint_t
recstream::get_off_line(Region &r)
{
	uint_t len = get_off_line_hdr();
	// os::SLEEP version does not return error
	(void) OffLineBufs.goto_boundary(len, os::SLEEP);
	OffLineBufs.back_split(r);
	return (len);
}

void
recstream::get_off_line_cancel()
{
	Region r;
	uint_t len = get_off_line_hdr();
	// os::SLEEP version does not return error
	(void) OffLineBufs.goto_boundary(len, os::SLEEP);
	OffLineBufs.back_split(r);
	r.dispose();
}

void
recstream::get_off_line_bytes(void *p, uint_t len, Environment *e)
{
	// XX If would be better if recstream had an Environment too
	OffLineBufs.xfer_contents((char *)p, len, e);
}

void
recstream::get_off_line_u_bytes(caddr_t p, uint_t len, Environment *e)
{
	// XX If would be better if recstream had an Environment too

	// Transfer contents of OffLineBufs to a marshalstream
	MarshalStream ms(OffLineBufs, e);
	ASSERT(OffLineBufs.empty());
#ifdef DEBUG
	uint_t orig_span = ms.span();
#endif

	// Pass in false as debug flag to get_u_bytes as this does not
	// correspond to a put_bytes on the sender
	int error = ms.get_u_bytes(p, len, false);
	if (error != 0) {
		// In case of error too, get_u_bytes advances cursor len bytes
		e->system_exception(CORBA::BAD_PARAM((uint_t)error,
			CORBA::COMPLETED_MAYBE));
	}
	// Remove the transferred bytes from the marshalstream
	ms.trim_beginning();
	ASSERT(orig_span == (ms.span() + len));

	// Transfer contents back to OffLineBufs
	OffLineBufs.concat(ms.region());
	ASSERT(ms.empty());
}

// This unmarshals data corresponding to marshals done in put_offline_reply
// First data marshalled in a boolean
// true => used reply optimization and there is nothing else to do
// false => did not do any reply optimization and the data is sent using
//	put_off_line. Hence we should emulate this by doing a get_off_line
//	here and copying the data to the buf
void
recstream::get_offline_reply(replyio_client_t *rio_cp)
{
	debug_get_word(M_REPLY);

	// If put_offline_reply copied directly to reply area, nothing to do
	if (!get_boolean()) {
		get_offline_reply_inline_data(rio_cp);
	} else {
		rio_cp->mark_wait_for_copies();
	}

	rio_cp->release();	// counteracts allocation in add_reply_buffer
}

void
recstream::get_offline_reply_inline_data(replyio_client_t *rio_cp)
{
	Environment env;
	struct buf *bp = rio_cp->get_bufptr();

	uint_t len = get_off_line_hdr();
	ASSERT(len <= bp->b_bcount);
#ifdef _KERNEL
	// Emulating a reply optimization by copying the data into the
	// specified buffer.
	// If the buf was already a REMAPPED buffer we can directly do
	// a get_offline_bytes

	if ((bp->b_flags & B_REMAPPED) != 0) {
		get_off_line_bytes(bp->b_un.b_addr, len, &env);
		ASSERT(!env.exception());
	} else {
		// If not, we bioclone and copy the data, so that we
		// dont destroy the original buf's REMAPPED flag.
		// This is probably overkill, but seems safer.
		struct buf *cbp;

		cbp = bioclone(bp, (off_t)0, (size_t)len, (dev_t)0,
			(daddr_t)0, NULL, NULL, KM_SLEEP);
		bp_mapin(cbp);
		get_off_line_bytes(cbp->b_un.b_addr, len, &env);
		ASSERT(!env.exception());
		bp_mapout(cbp);
		biodone(cbp);
		freerbuf(cbp);
	}
#else
	// struct buf is not yet emulated in unode
	ASSERT(!"struct buf emulation in unode");
#endif
}

void
recstream::get_offline_reply_cancel(replyio_client_t *rio_cp)
{
	debug_get_word(M_REPLY);
	// If put_offline_reply copied directly to reply area, nothing to do
	if (!get_boolean()) {
		get_off_line_cancel();
	}
	rio_cp->release();	// counteracts allocation in add_reply_buffer
}

replyio_server_t *
recstream::get_reply_buffer()
{
	debug_get_word(M_REPLY);
	return (NULL);
}

void
recstream::get_reply_buffer_cancel()
{
	debug_get_word(M_REPLY);
}

// execute - defer_task method
void
recstream::execute()
{
#ifdef _KERNEL_ORB
	orb_msg::the().process_message(this, os::SLEEP);
#else
	ASSERT(!"recstream::execute called in userland");
	done();
#endif
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <orb/buffers/marshalstream_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
