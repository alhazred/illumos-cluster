/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _REPLYIO_H
#define	_REPLYIO_H

#pragma ident	"@(#)replyio.h	1.8	08/05/20 SMI"

#include <sys/os.h>

// forward declarations
class sendstream;

// classes for reply optimization
// XXX These two classes are under construction and are there to extract
// commonality between different transports with the eventual goal of making
// reply optmization work in a mixed transport cluster
// This code currently reflects a lot of tcp_transport code that is
// not endpoint-specific

//
// The protocol for using replyio is that the client first calls
// check_reply_buf_support(). If that returns true, the client then either
// does an add_reply_buffer() or tells the server to do an add_reply_buffer()
// depending on the direction of I/O movement. What's important is that
// add_reply_buffer() is always assumed to succeed.
//
class replyio_client_t {
public:
	replyio_client_t(struct buf *);

	virtual ~replyio_client_t();

	// Called by the handler to release when done with the ios
	virtual void release() = 0;

	struct buf *get_bufptr() const;

	// Called to marshal the cookie during add_reply_buffer
	void marshal_cookie(MarshalStream &);

	// Called by get_offline_reply
	void mark_wait_for_copies();
protected:
	struct buf *bufptr;

	//
	// Flag indicates that data copy operations have been initiated and
	// a call to release must wait for the copies to get over.
	//
	bool	wait_for_copies;
private:
	// Disallow assignments and pass by value
	replyio_client_t(const replyio_client_t &);
	replyio_client_t &operator = (replyio_client_t &);
};

class replyio_server_t {
public:
	replyio_server_t();

	replyio_server_t(MarshalStream&);

	virtual ~replyio_server_t();

	// Called by the handler to release when done with the ios
	virtual void release();

	//
	// replyio_server_t::send_reply_buf is called from
	// sendstream::put_offline_reply to transfer offline data to the
	// replyio client node during the marshalling process.
	// NOTE - cannot marshal anything to MainBuf in this routine.
	//
	// This method is used only by transports whose replyio infrastructure
	// is tied to an endpoint - type 1, e.g., the RSM transport and that
	// are capable to moving the data directly into the clients memory.
	// Other transports - type 2, e.g., TCP transport - use the
	// sendstream::send_reply_buf method for the transfer. For type 2
	// transports the client node is involved in a protocol that
	// coordinates receiving the data from the server and copying it at
	// the appropriate places.
	//
	// Note that the sendstream might be tied to an endpoint different
	// from the endpoint the replyio_server_t object is tied to. In fact
	// the two endpoints might even belong to two different transports.
	//
	// sendstream::put_offline_reply first makes a call to
	// replyio_server_t::send_reply_buf. If this call does not succeed
	// in transferring the data, the sendstream::put_offline_reply tries
	// sendstream::send_reply_buf if replyio_server_t::send_reply_buf
	// had returned with SEND_REPLY_BUF_RETRY.
	//
	// Type 1 transports (whose replyio infrastructure is endpoint
	// specific) will return SEND_REPLY_BUF_FAILED from
	// replyio_server_t::send_reply_buf upon error as they know that -
	//
	// (a)	If the sendstream transport is a type 1 transport,
	//	sendstream::send_reply_buf will be a no-op and thus there is
	//	no point making that call.
	// (b)	If the sendstream transport is a type 2 transport, its
	//	sendstream::send_reply_buf will try to do its own version of
	//	replyio transfer that the replyio_client_t object at the
	//	client that is a type 1 object will not be able to handle.
	//
	// Type 2 transports always return SEND_REPLY_BUF_RETRY. At present
	// we make the assumption that the existing TCP transport will be the
	// only type 2 transport we will have. Returning SEND_REPLY_BUF_RETRY
	// makes sure that if the sendstream transport is also the type 2 TCP
	// transport the sendstream::send_reply_buf will get a chance to send
	// the data. If the sendstream is a type 1 transport, the
	// sendstream::send_reply_buf will be a no-op and no damage done.
	//
	// This whole mechanism is to support the case where the replyio
	// infrastructure was setup on one type 1 endpoint and the actual
	// reply goes out on a different endpoint (maybe even a type 2
	// endpoint). We want to still be able to utilize the efficient
	// replyio data transfer mechanism available with the original
	// replyio setup on the type 1 transport.
	//
	typedef enum {	SEND_REPLY_BUF_FAILED = 0,
		SEND_REPLY_BUF_SUCCESS,
		SEND_REPLY_BUF_RETRY}	send_reply_buf_result_t;

	virtual send_reply_buf_result_t send_reply_buf(Buf *, sendstream *);

	// Generation number is incremented each time put_offline_reply is
	// called for this object
	void incr_generation();

	// Used by the transports to get current generation
	uint_t get_generation() const;

	// unmarshal cookie as part of get_reply_buffer/constructor
	void unmarshal_cookie(MarshalStream &);

	// unmarshal cookie as part of get_reply_buffer_cancel
	static void unmarshal_cookie_cancel(MarshalStream &);

	// Used by the transports to get the cookie
	void *get_cookie() const;

private:
	void *cookie;
	uint_t	generation;	// how many times was put_offline_reply called

	// Disallow assignments and pass by value
	replyio_server_t(const replyio_server_t &);
	replyio_server_t &operator = (replyio_server_t &);
};

#ifndef NOINLINES
#include <orb/buffers/replyio_in.h>
#endif  // _NOINLINES

#endif	/* _REPLYIO_H */
