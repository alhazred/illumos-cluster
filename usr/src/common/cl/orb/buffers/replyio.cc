/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)replyio.cc	1.11	08/05/20 SMI"

#include <orb/buffers/marshalstream.h>

//
// The destructor does not free the space pointed to by bufptr.
// That responsibility lies elsewhere.
//lint -e1540
replyio_client_t::~replyio_client_t()
{
}
//lint +e1540

void
replyio_client_t::marshal_cookie(MarshalStream &ms)
{
	ms.put_pointer((void *)this);
}

replyio_server_t::~replyio_server_t()
{
	// Make lint happy
	cookie = NULL;
}

void
replyio_server_t::unmarshal_cookie(MarshalStream &ms)
{
	cookie = ms.get_pointer();
	ASSERT(cookie != NULL);
}

void
replyio_server_t::unmarshal_cookie_cancel(MarshalStream &ms)
{
	(void) ms.get_pointer();
}

// default implementation of virtual methods
void
replyio_server_t::release()
{
	delete this;
}

replyio_server_t::send_reply_buf_result_t
replyio_server_t::send_reply_buf(Buf *, sendstream *)
{
	// Returning false makes sendstream::put_offline_reply call
	// send_reply_buf method on the sendstream
	return (replyio_server_t::SEND_REPLY_BUF_RETRY);
}

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <orb/buffers/replyio_in.h>
#undef inline		// in case code is added below that uses inline
#endif  // NOINLINES
