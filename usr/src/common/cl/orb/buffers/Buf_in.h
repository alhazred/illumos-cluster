/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  Buf_in.h
 *
 */

#ifndef _BUF_IN_H
#define	_BUF_IN_H

#pragma ident	"@(#)Buf_in.h	1.24	08/05/20 SMI"

//
// Buf methods
//

inline uint_t
Buf::first() const
{
	return (first_);
}

inline uint_t
Buf::last() const
{
	return (last_);
}

inline uint_t
Buf::length() const
{
	return (length_);
}

inline void
Buf::length(uint_t len)
{
	length_ = len;
}

inline uint_t
Buf::span()
{
	return (last_ - first_);
}

inline uint_t
Buf::avail()
{
	return (length_ - last_);
}

inline bool
Buf::empty()
{
	return (first_ == last_);
}

inline uchar_t	*
Buf::base_address() const
{
	return (vAddr);
}

inline void
Buf::base_address(uchar_t *va)
{
	vAddr = va;
}

// Asserts that base_address is not NULL, head is meaningless otherwise
inline uchar_t  *
Buf::head() const
{
	ASSERT(base_address());
	return (vAddr + first_);
}

inline void
Buf::advance(int i)
{
	first_ += i;
	ASSERT((first_ <= length_) && (first_ <= last_));
}

// Advance the tail pointer
inline void
Buf::incspan(int i)
{
	last_ += i;
	ASSERT((last_ >= first_) && (last_ <= length_));
}

// Change the tail pointer
inline void
Buf::last(uint_t i)
{
	last_ = i;
	ASSERT((last_ >= first_) && (last_ <= length_));
}

#ifdef _KERNEL
// pointer to streams free function used by esb_buffree_alloc
inline frtn_t *
Buf::frtnp()
{
	return (&freefunc);
}
#endif

inline bool
Buf::is_on_heap()
{
	return ((buf_alloc == HEAP) && (data_alloc == HEAP));
}

inline uchar_t *
GWBuf::alloc_buffer(uint_t sz, os::mem_alloc_type flag)
{
	// convert from bytes to number of ulongs.
	sz = (sz + (uint_t)sizeof (ulong_t) - 1) / (uint_t)sizeof (ulong_t);
	/*CSTYLED*/
	return ((uchar_t *)(new (flag) ulong_t[sz]));
}

inline void
GWBuf::free_buffer(uchar_t *p)
{
	delete [] p;
}

// Create a word aligned buffer at least as big as specified.
// Does blocking memory allocation
inline
GWBuf::GWBuf(uint_t byte_size) :
	Buf(alloc_buffer(byte_size), byte_size)
{
}

#ifdef _KERNEL
//
// Constructor for MBlkBuf that is used when the mblk will
// be provided later.
//
inline
MBlkBuf::MBlkBuf(alloc_type allocation_type) :
	Buf(NULL, 0, 0, allocation_type),
	mblk(NULL)
{
}
#endif	// _KERNEL

// Like constructors but for reinitializing
inline void
cursor::setbuf(Buf * b)
{
	bu = b;
	if (b)
	    cursor_ = bu->first();
}

inline void
cursor::setbuf(Buf * b, uint_t c)
{
	bu = b;
	cursor_ = c;
	ASSERT(!bu || (cursor_ >= bu->first()));
	ASSERT(!bu || (cursor_ <= bu->last()));
}

inline
cursor::cursor() :
	bu(0)
{
}

inline
cursor::cursor(Buf *b, uint_t w)
{
	setbuf(b, w);
}

inline void
cursor::rewind()
{
	cursor_ = bu->first();
}

inline void
cursor::trim_prefix()
{
	bu->advance(cursor_-bu->first());
}

inline void
cursor::trim_suffix()
{
	bu->incspan(cursor_-bu->last());
}

inline Buf *
cursor::buffer() const
{
	return (bu);
}

inline bool
Region::empty()
{
	return (buffers.empty());
}

// Leaves the cursor pointing to first buffer of region
inline void
Region::rewind()
{
	buffers.atfirst();
}

// Leaves the cursor pointing to last buffer of region
inline void
Region::ffrwrd()
{
	buffers.atlast();
}

// reaps off first buffer in region.
inline Buf *
Region::reap()
{
	return (buffers.reapfirst());
}

// Leaves prefix in current Region, the tail into the new region.
// Splits at current buffer, i.e., the current is part of the tail
inline void
Region::split(Region &r)
{
	buffers.split(r.buffers);
}

// Leaves prefix in new region, tail in current Region
// r should be empty
inline 	void
Region::back_split(Region &r)
{
	buffers.backsplit(r.buffers);
}

//  Leaves the param empty.
inline 	void
Region::concat(Region &r)
{
	buffers.concat(r.buffers);
}

//  Buffer insertion operations

// Notice that a region gains ownership of the buffers
// it gets passed. The region "delete"s them when the
// region is deleted.
inline 	void
Region::append(Buf *b)
{
	buffers.append(b);
}

// puts the buffer at the beginning;
inline 	void
Region::prepend(Buf *b)
{
	buffers.prepend(b);
}

// inserts the buffer before the current buffer.
inline 	void
Region::insert(Buf *b)
{
	buffers.insert_b(b);
}

inline void
Region::goto_next()
{
	buffers.advance();
}

// Return the current buffer from the doubly linked list of buffers
inline Buf *
Region::buffer() const
{
	return (buffers.get_current());
}

#ifdef _KERNEL
inline
mblk_chain::mblk_chain() :
	head(NULL),
	tailp(&head)
{
}

inline
mblk_chain::~mblk_chain()
{
	ASSERT(head == NULL);
}

// Append an mblk to the chain
inline void
mblk_chain::append(mblk_t *mp)
{
	// Only use this routine to append a single mblk
	// Use append_chain() to append a list of mblks
	ASSERT(mp->b_cont == NULL);
	*tailp = mp;
	tailp = &(mp->b_cont);
}

// Dispose - freemsg
inline void
mblk_chain::dispose()
{
	if (head != NULL) {
		freemsg(head);
		head = NULL;
		tailp = &head;
	}
}

// span - msgdsize
inline uint_t
mblk_chain::span()
{
	return ((uint_t)msgdsize(head));
}

// Destructive - transfers ownership of mblk chain to caller
inline mblk_t *
mblk_chain::get_mblk_chain()
{
	mblk_t *mp = head;
	head = NULL;
	tailp = &head;
	return (mp);
}
#endif	// _KERNEL

#endif	/* _BUF_IN_H */
