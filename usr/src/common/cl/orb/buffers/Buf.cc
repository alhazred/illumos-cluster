/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)Buf.cc	1.117	08/05/20 SMI"

#include <orb/buffers/Buf.h>
#include <orb/buffers/marshalstream.h>
#include <orb/infrastructure/orb.h>

//
// Buf constructor -
// initialize Buf constructor with refcount of 0 instead of the default of 1
//
Buf::Buf(alloc_type bflag, alloc_type dflag) :
    buf_alloc(bflag),
    data_alloc(dflag),
    length_(0),
    first_(0),
    last_(0),
    vAddr(0),
    notify_ptr(NULL),
    _DList::ListElem(this),
    refcnt(0)
{
}

//
// Buf constructor -
// initialize Buf constructor with refcount of 0 instead of the default of 1
//
Buf::Buf(uchar_t *vaddrp, uint_t len, alloc_type bflag, alloc_type dflag) :
	buf_alloc(bflag),
	data_alloc(dflag),
	length_(len),
	first_(0),
	last_(0),
	vAddr(vaddrp),
	notify_ptr(NULL),
	_DList::ListElem(this),
	refcnt(0)
{
}

//
// Buf constructor -
// initialize Buf constructor with refcount of 0 instead of the default of 1
//
Buf::Buf(uchar_t *vaddrp, uint_t len, uint_t head_pos, alloc_type bflag,
    alloc_type dflag, os::notify_t *np) :
	buf_alloc(bflag),
	data_alloc(dflag),
	length_(len),
	first_(head_pos),
	last_(head_pos),
	vAddr(vaddrp),
	notify_ptr(np),
	_DList::ListElem(this),
	refcnt(0)
{
	// Verify that the os::notify_t passed in, is in the right state
	ASSERT((np == NULL) || (np->get_state() == os::notify_t::WAITING));
	ASSERT(len >= head_pos);
}

// default implementation used by most of the Buf types
// delete if the buffer is allocated on the heap
// The destructor handles deleting/releasing the data buffer itself, if needed
void
Buf::done()
{
	if (buf_alloc == HEAP) {
		delete this;
	}
}

void
Buf::refcnt_unref()
{
	done();
}

// If constructor set notify_ptr, signal
Buf::~Buf()
{
	if (notify_ptr != NULL) {
		notify_ptr->signal();
		notify_ptr = NULL;
	}
	// Verify that the refcount is 0 before deletion
	ASSERT(verify_count(0));
	vAddr = 0;
}

// default implementation returns NULL
// XXX caller cannot distinguish between memory failure vs. calling
// dup on a Buf type that does not support it
Buf *
Buf::dup(uint_t, uint_t, os::mem_alloc_type)
{
	ASSERT(0);	// Should not be calling this with a wrong buffer type
	return (NULL);
}

// xfer_contents - function to copy the contents of the first size bytes
//	of buffer to caller-allocated buffer (can be remote buffer)
// This is a destructive copy - the Buffer done method is called
//	This is equivalent to (if size == span)
//		creating a cursor, getdata, done
//	OR { copy contents of b; b->done(); }
//	This is equivalent to (if size < span)
//		creating a cursor, getdata, advance(size)
//	OR { copy contents of b; b->advance(size); }
//
// This is a convenience function as the alternatives are cumbersome
//	It also allows buf-specific copy methods, e.g., UioBuf
//
// Errors are indicated as exceptions in e.
void
Buf::xfer_contents(char *dest, uint_t size, Environment *e)
{
	copy_contents(dest, size, 0, e);
	if (size == span()) {
		done();
	} else {
		advance((int)size);	// Advance the first pointer by size
	}
}

// copy_contents - function to copy the contents of the first size bytes
//	of the buffer to a caller-allocated buffer (can be remote buffer too)
// This is a non-destructive copy - the Buffer is left unmodified
//	This is equivalent to
//		creating a cursor, getdata
//	OR { copy contents of first size bytes of b; }
//
// This is a convenience function as the alternatives are cumbersome
//	It also allows buf-specific copy methods, e.g., UioBuf
//
// Errors are indicated as exceptions in e.
void
Buf::copy_contents(char *dest, uint_t size, uint_t off, Environment *e)
{
	if ((off + size) > span()) {
		e->system_exception(
			CORBA::BAD_PARAM(EOVERFLOW, CORBA::COMPLETED_MAYBE));
	} else {
		bcopy(((char *)head()) + off, dest, (size_t)size);
	}
}

// GWBuf - a type of buffer used by the ORB

// Gets rid of memory allocated when buffer was created
GWBuf::~GWBuf()
{
	free_buffer(base_address());
}

// nil_Buf

nil_Buf::nil_Buf(alloc_type bflag) :
	Buf(bflag, OTHER)
{
}

// constructor that also sets the length and span. Use when creating Buf that
// already points at valid data
nil_Buf::nil_Buf(uchar_t *u, uint_t sz, uint_t spn, alloc_type bflag,
    os::notify_t *np) :
	Buf(u, sz, 0, bflag, OTHER, np)
{
	ASSERT(spn <= sz);
	incspan((int)spn);
}

// constructor that also sets the length and span. Use when creating Buf
// that already points at valid data it also sets data_alloc which indicates
// the alloc_type of the valid data, if alloc_type is HEAP then it will be
// deallocated in the  destructor.
nil_Buf::nil_Buf(uchar_t *u, uint_t sz, uint_t spn, alloc_type bflag,
    alloc_type dflag, os::notify_t *np) :
	Buf(u, sz, 0, bflag, dflag, np)
{
	ASSERT(spn <= sz);
	incspan((int)spn);
}

nil_Buf::~nil_Buf()
{
	if (data_alloc == HEAP) {
		delete [] base_address();
	}
}

// Create a new nil_Buf start starts are base_address of
// base_address() + start and length of len
// Leave the Buf created with a span of 0 as the caller sets that
Buf *
nil_Buf::dup(uint_t start, uint_t len, os::mem_alloc_type flag)
{
	ASSERT(start >= first());
	ASSERT((start + len) <= length());
	// XX the support for notify_ptr does not work when we do dup
	// as we need to do the notify only after both Bufs are deleted
	// Wanted to ASSERT here, but member is not accessible from here
	/*CSTYLED*/
	return ((Buf *) new (flag) nil_Buf(base_address() + start, len, 0));
}

#ifdef _KERNEL
#include <sys/strsun.h>

static mblk_t *Buf_esballoc(Buf *, uint_t, Environment *);
static mblk_t *Buf_allocb(Buf *, uint_t, uint_t, Environment *);

// get_mblk
// See comments in Buf.h for semantics
mblk_t *
Buf::get_mblk(uint_t hsize, uint_t maxsize, bool do_esballoc, Environment *e)
{

	mblk_chain	mc;
	uint_t		len, size, orig_span = span();
	bool		do_copy;

	//
	// If size if small, do bcopy instead of esb_alloc - tune this size
	// If the Buf or data was not allocated on the heap we need to copy
	// However, if someone is waiting for the destructor of the Buf before
	// destroying the data, we can avoid the copy
	// See comment in marshalstream.h
	//
	// The boolean do_esballoc is set according to clconf "lazy_free"
	// property for each adapter type.  If "lazy_free" is set, that means
	// the driver will defer freeing of esblloc blocks.  This will affect
	// PXFS since getting the release message from get_mblk is on its
	// critical path.  Hence, esballoc cannot be used, and alloc should
	// be used instead.  (see bug 4281279)  In this case, do_esballoc
	// is set to false.
	// If the driver does not do lazy free however, esballoc should be
	// used, hence do_esballoc is set to true.
	// By default, do_esballoc is false.
	//

	if (do_esballoc) {
		do_copy = (orig_span <= 512) || (data_alloc == USER_CONTEXT) ||
		    (!is_on_heap() && (notify_ptr == NULL));
	} else {
		do_copy = true;
	}

	// If maxsize == 0, => there is no max size
	if (maxsize == 0) {
		maxsize = UINT_MAX;
	}

	// Calculate initial attempted length of first mblk to span
	if (orig_span > maxsize) {
		// The transports are more efficient at sending the mblks if the
		// first fragment is shorter than the rest as headers usually
		// can fit into the slack in the first mblk.
		// So, instead of making the first mblks be of maxsize, we make
		// the first one have the remainder.
		len = orig_span % maxsize;
	} else {
		len = orig_span;
	}

	// We use size instead of span() because Buf_esballoc/Buf_allocb
	// can delete the Buf and we cannot deference "this"
	size = orig_span;
	do {
		ASSERT(size == span());
		// limit len to "maxsize"
		len = MIN(len, maxsize);
		mblk_t *temp;

		if (do_copy) {
			temp = Buf_allocb(this, hsize, len, e);
		} else {
			temp = Buf_esballoc(this, len, e);
		}
		if (temp == NULL) {
			ASSERT(e->sys_exception());
			// Do not call Buf::done/cancel here.
			// do_copy == true, Buf_allocb would have called it
			// do_copy == false, disposing the mblk chain will
			// trigger a Buf::done() on the last freefunc call
			mc.dispose();
			return (NULL);
		}
		ASSERT((uint_t)MBLKL(temp) == len);
		mc.append(temp);
		// Do not call Buf::done/cancel here.
		// do_copy == true, Buf_allocb would have called it if last frag
		// do_copy == false, when the transport is done with
		// the mblks, it will trigger Buf::done() on last freefunc call

		// Adjust size to reflect bytes handled and iterate
		ASSERT(size >= len);
		size -= len;
	} while ((len = size) != 0);
	ASSERT(mc.span() == orig_span);
	return (mc.get_mblk_chain());
}

// copy2mblk
// See comments in Buf.h for semantics
mblk_t *
Buf::copy2mblk(uint_t hsize, uint_t maxsize, Environment *e)
{

	mblk_chain	mc;
	uint_t		len, off, orig_span = span();
	mblk_t		*mp;

	// If maxsize == 0, => there is no max size
	if (maxsize == 0) {
		maxsize = UINT_MAX;
	}

	// off is offset into Buf that has already been copied
	off = 0;
	while (off != orig_span) {
		// limit len to "maxsize"
		len = MIN(orig_span - off, maxsize);

		mp = os::allocb(len + hsize, BPRI_MED, e->nonblocking_type());
		if (mp == NULL) {
			// error during os::allocb - set exception
			ASSERT(e->is_nonblocking());
			e->system_exception(CORBA::WOULDBLOCK(len + hsize,
				CORBA::COMPLETED_MAYBE));
			mc.dispose();
			return (NULL);
		}
		// Adjust rptr to leave space for header
		mp->b_rptr += hsize;

		// Copy the contents of the buffer into the mblk
		copy_contents((char *)(mp->b_rptr), len, off, e);

		// Adjust the wptr
		mp->b_wptr = mp->b_rptr + len;
		ASSERT((uint_t)MBLKL(mp) == len);

		// append mblk to list
		mc.append(mp);

		// Increment offset into Buf for next iteration to copy
		off += len;
	}
	ASSERT(mc.span() == span());	// Copied full Buf
	return (mc.get_mblk_chain());
}

#endif

CBuf::CBuf(uchar_t *u, size_t size_bytes) :
	Buf(u, (uint_t)size_bytes),
	size(size_bytes)
{
	ASSERT(size_bytes <= UINT_MAX);
}

// Gets rid of memory allocated when buffer was created
CBuf::~CBuf()
{
	if (base_address()) {
#ifdef _KERNEL
		kmem_free(base_address(), size);
#else
		free(base_address());
#endif
	}
}

#ifdef _KERNEL

//
// PpBuf points to data that is not part of this buffer or owned by this buffer
// Some other entity locks the pages and encapsulates the pp list/buf structure
// within the PpBuf. So we mark the buf as allocated on the HEAP and the
// data alloc_type as OTHER
// The caller is expected to wait for the signal on the os::notify_t before
// destroying the pages and the notify_t structure.
// XX We could do the bp_mapin/mapout only during copyin/copyout
//
PpBuf::PpBuf(page_t *pp, uint_t len, os::notify_t *np) :
	Buf(NULL, len, 0, HEAP, OTHER, np)
{
	bioinit(&buf);

	//
	// B_KERNBUF is obsoleted by S9
	//
#ifdef B_KERNBUF
	buf.b_flags = B_KERNBUF | B_PAGEIO;
#else
	buf.b_flags = B_PAGEIO;
#endif
	buf.b_bcount = len;
	buf.b_bufsize = len;
	buf.b_pages = pp;
	bp_mapin(&buf);
	base_address((uchar_t *)buf.b_un.b_addr);
	ASSERT(len <= INT_MAX);
	incspan((int)len);
}

PpBuf::~PpBuf()
{
	// Before destroying the buffer, clean_up.
	bp_mapout(&buf);
	biodone(&buf);
}

//
// UioBuf points to data that is not part of this buffer or owned by this buffer
// This is usually a user address that is encapsulated within a UBuf.
// So we mark the buf as allocated on the HEAP and the
// data alloc_type as USER_CONTEXT
//
UioBuf::UioBuf(struct uio *uiop) :
	Buf(NULL, (uint_t)uiop->uio_resid, HEAP, USER_CONTEXT)
{
	ASSERT(uiop->uio_resid >= 0);
	if (uiodup(uiop, &_uio, _iov, DEF_IOV_MAX)) {
		//
		// SCMSGS
		// @explanation
		// The system attempted to use a uio that had more than
		// DEF_IOV_MAX fragments.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "clcomm: UioBuf: uio was too fragmented - %d",
		    uiop->uio_iovcnt);
	}
	ASSERT(uiop->uio_resid <= INT_MAX);
	incspan((int)uiop->uio_resid);
}

void
UioBuf::copy_contents(char *dest, uint_t size, uint_t off, Environment *e)
{
	int err;
	uio_t temp_uio;
	iovec_t temp_iov[DEF_IOV_MAX];

	if ((int)(off + size) > _uio.uio_resid) {
		e->system_exception(
			CORBA::BAD_PARAM(EOVERFLOW, CORBA::COMPLETED_MAYBE));
	} else {
		// Neglect return value, we guarantee there are enough iovs
		(void) uiodup(&_uio, &temp_uio, temp_iov, DEF_IOV_MAX);
		// Skip off bytes in uio
		uioskip(&temp_uio, (size_t)off);
		err = uiomove(dest, (size_t)size, UIO_WRITE, &temp_uio);
		if (err != 0) {
			e->system_exception(CORBA::BAD_PARAM((uint_t)err,
				CORBA::COMPLETED_MAYBE));
		}
	}
}

void
UioBuf::xfer_contents(char *dest, uint_t size, Environment *e)
{
	int err;
	if ((int)size > _uio.uio_resid) {
		e->system_exception(
			CORBA::BAD_PARAM(EOVERFLOW, CORBA::COMPLETED_MAYBE));
	} else if ((err = uiomove(dest, (size_t)size, UIO_WRITE, &_uio)) != 0) {
		e->system_exception(CORBA::BAD_PARAM((uint_t)err,
			CORBA::COMPLETED_MAYBE));
	}
	if (size == span()) {
		done();
	} else {
		advance((int)size);	// Advance the first pointer by size
	}
}

// UBuf points to data that is not part of this buffer or owned by this buffer
// This is usually a user address that is encapsulated within a UBuf.
// So we mark the buf as allocated on the HEAP and the
// data alloc_type as USER_CONTEXT
UBuf::UBuf(void *ua, uint_t len, os::notify_t *np) :
	Buf(NULL, len, 0, HEAP, USER_CONTEXT, np),
	uaddr(ua)
{
	ASSERT(len <= INT_MAX);
	incspan((int)len);
}

void
UBuf::copy_contents(char *dest, uint_t size, uint_t off, Environment *e)
{
	if ((off + size) > span()) {
		e->system_exception(
			CORBA::BAD_PARAM(EOVERFLOW, CORBA::COMPLETED_MAYBE));
		return;
	}
	int err = xcopyin((caddr_t)uaddr + first() + off, (caddr_t)dest,
	    (size_t)size);
	if (err != 0) {
		e->system_exception(CORBA::BAD_PARAM((uint_t)err,
			CORBA::COMPLETED_MAYBE));
	}
}

#ifdef DEBUG
struct mstats mblk_stats;
#endif

// constructor
// Create a MBlkBuf out of an existing mblk.
// We create a MBlkBuf of length same as input mblk and
// set the head to match rptr
// the span is set to match the input argument
//	XX it may be better to make it be based on wptr itself
// The done routine will free up the mblk
MBlkBuf::MBlkBuf(mblk_t *m, uint_t spn) :
	Buf(DB_BASE(m), (uint_t)MBLKSIZE(m), (uint_t)MBLKHEAD(m)),
	mblk(m)
{
	incspan((int)spn);
	ASSERT(spn <= (uint_t)MBLKL(m));
	ASSERT(DB_BASE(m) <= m->b_rptr);
	ASSERT(DB_LIM(m) >= m->b_wptr);
	ASSERT(MBLKSIZE(m) >= MBLKL(m));
	UPDATE_MBLK_STATS(mblks_allocated, 1);
	UPDATE_MBLK_STATS(mblks_allocated_exist, 1);
}

MBlkBuf::~MBlkBuf()
{
	// The destructor can be called even if the allocb in the
	// constructor failed. This destructor however does not
	//	depend on whether the allocb failed.
	UPDATE_MBLK_STATS(mblk_destruct, 1);
	if (mblk != NULL) {
		ASSERT(mblk->b_next == NULL);
		freeb(mblk);
		UPDATE_MBLK_STATS(mblk_freed, 1);
	}
}

//
// use_mblk - The MBlkBuf was created without an mblk.
// This method loads the mblk. This method cannot use blocking operations.
//
void
MBlkBuf::use_mblk(mblk_t *mblkp, uint_t size)
{
	ASSERT(mblk == NULL);
	mblk = mblkp;

	base_address(DB_BASE(mblkp));
	length((uint_t)MBLKSIZE(mblkp));

	// set the last_ in Buf
	int	mblk_head = (int)MBLKHEAD(mblkp);
	incspan((int)size + mblk_head);

	// set the first_ in Buf
	advance((int)MBLKHEAD(mblkp));

	ASSERT(size <= (uint_t)MBLKL(mblkp));
	ASSERT(DB_BASE(mblkp) <= mblkp->b_rptr);
	ASSERT(DB_LIM(mblkp) >= mblkp->b_wptr);
	ASSERT(MBLKSIZE(mblkp) >= MBLKL(mblkp));
	UPDATE_MBLK_STATS(mblks_allocated, 1);
	UPDATE_MBLK_STATS(mblks_allocated_exist, 1);
}

// get_mblk
mblk_t *
MBlkBuf::get_mblk(uint_t, uint_t, bool, Environment *)
{
	mblk_t *tmp = mblk;

	// We neglect the header size and maxsize for now
	// headers are already preallocated by streams transports
#if 0
	// mblk_handler usually keeps the size pretty small too
	// (maybe upto twice MTU). If we start sending down really large
	// mblks then this becomes an issue

	// If the span is too big
	if ((maxsize != 0) && (span() > maxsize)) {
		// XX We should convert to a stream of mblks in this case
		e->system_exception(CORBA::??(0,
			CORBA::COMPLETED_MAYBE));
		done();
		return (NULL);
	}
#endif
	ASSERT(mblk);
	// this consumes the mblk. set mblk to NULL
	mblk = NULL;
	tmp->b_rptr = head();
	tmp->b_wptr = base_address() + last();
	tmp->b_cont = NULL;
	ASSERT((uint_t)MBLKL(tmp) == span());
	ASSERT(DB_BASE(tmp) <= tmp->b_rptr);
	ASSERT(DB_LIM(tmp) >= tmp->b_wptr);
	ASSERT(DB_TYPE(tmp) == M_DATA);
	UPDATE_MBLK_STATS(mblk_transferred, 1);
	done();		// Call done() as this is a transfer of ownership
			// Buffer cannot be used after this
	return (tmp);
}

// dup
Buf *
MBlkBuf::dup(uint_t start, uint_t len, os::mem_alloc_type flag)
{
	mblk_t *nmp = os::dupb(mblk, BPRI_MED, flag);
	if (nmp == NULL)
		return (NULL);

	ASSERT(start >= first());
	ASSERT(len <= span());
	ASSERT((start + len) <= length());
	// Base rptr on base_address() in MBlkBuf instead of in mblk
	nmp->b_rptr = base_address() + start;	// advance rptr in new buffer
	nmp->b_wptr = nmp->b_rptr + len;	// set wptr in new buffer
	/*CSTYLED*/
	Buf *b = (Buf *) new (flag) MBlkBuf(nmp, 0);
	if (b == NULL) {
		// memory allocation failed, release dup'd mblk
		ASSERT(flag == os::NO_SLEEP);
		freeb(nmp);
		return (NULL);
	}
	// The above constructor does not fail for any other reason
	ASSERT(b->base_address());
	UPDATE_MBLK_STATS(mblk_dup, 1);
	return (b);
}

#endif

// getdata
uint_t
cursor::getdata(uint_t n, void *b)
{
	if (!bu)
		return (0);

	ASSERT(cursor_ <= bu->last());
	uint_t rest = bu->last() - cursor_;
	n = MIN(rest, n);
	char *src = (char *)(bu->base_address() + cursor_);
	char *dest = (char *)b;

	// One can unroll further if needed; with sparc this compiles into
	// load store instructions with a goto for the switch
	// statement.
// CSTYLED
//lint -e616
	switch (n) {
	case 8 :
		dest[7] = src[7];
	case 7 :
		dest[6] = src[6];
	case 6 :
		dest[5] = src[5];
	case 5 :
		dest[4] = src[4];
	case 4 :
		dest[3] = src[3];
	case 3 :
		dest[2] = src[2];
	case 2 :
		dest[1] = src[1];
	case 1 :
		dest[0] = src[0];
		break;
	default:
		bcopy(src, dest, (size_t)n);
	}
// CSTYLED
//lint +e616
	cursor_ += n;
	ASSERT(cursor_ <= bu->last());
	ASSERT(cursor_ >= bu->first());
	return (n);
}

#ifdef _KERNEL
// get_u_data
// getdata version that does copyout to user space
uint_t
cursor::get_u_data(uint_t n, void *b, int *errp)
{
	if (!bu)
		return (0);

	ASSERT(cursor_ <= bu->last());
	uint_t rest = bu->last() - cursor_;
	n = MIN(rest, n);
	*errp = xcopyout((caddr_t)(bu->base_address() + cursor_),
		(caddr_t)b, (size_t)n);
	// continue on error to increment cursor_ to keep reader in sync
	cursor_ += n;
	ASSERT(cursor_ <= bu->last());
	ASSERT(cursor_ >= bu->first());
	return (n);
}
#else
uint_t
cursor::get_u_data(uint_t, void *, int *)
{
	ASSERT(0);
	return (0);
}
#endif

// putdata
uint_t
cursor::putdata(uint_t n, const void *b)
{
	if (!bu)
		return (0);

	if (cursor_ >= bu->length())
		return (0);

	uint_t rest = bu->length() - cursor_;
	n = MIN(rest, n);
	char *src = (char *)b;
	char *dest = (char *)(bu->base_address() + cursor_);

	// One can unroll further if needed; with sparc this compiles into
	// load store instructions with a goto for the switch
	// statement.
// CSTYLED
//lint -e616
	switch (n) {
	case 8 :
		dest[7] = src[7];
	case 7 :
		dest[6] = src[6];
	case 6 :
		dest[5] = src[5];
	case 5 :
		dest[4] = src[4];
	case 4 :
		dest[3] = src[3];
	case 3 :
		dest[2] = src[2];
	case 2 :
		dest[1] = src[1];
	case 1 :
		dest[0] = src[0];
		break;
	default:
		bcopy(src, dest, (size_t)n);
	}
// CSTYLED
//lint +e616
	cursor_ += n;
	if (cursor_ > bu->last()) {
		bu->last(cursor_);
	}
	ASSERT(cursor_ <= bu->last());
	ASSERT(cursor_ > bu->first());
	return (n);
}

// put_u_data
// Version of putdata that does copyin from user space
uint_t
cursor::put_u_data(uint_t n, const void *b, int *errp)
{
	if (!bu)
		return (0);

	if (cursor_ >= bu->length())
		return (0);

	uint_t rest = bu->length() - cursor_;
	n = MIN(rest, n);
#ifdef _KERNEL
	*errp = xcopyin((caddr_t)b, (caddr_t)(bu->base_address() + cursor_),
	    (size_t)n);
#else
	bcopy((caddr_t)b, (caddr_t)(bu->base_address() + cursor_),
	    (size_t)n);
	*errp = 0;
#endif
	// continue even on error to
	// increment cursor_ too keep marshal process in sync
	cursor_ += n;
	if (cursor_ > bu->last()) {
		bu->last(cursor_);
	}
	ASSERT(cursor_ <= bu->last());
	ASSERT(cursor_ > bu->first());
	return (n);
}

//
// alloc_chunk - allocates a contiguous chunk of bytes prior
// to start of normal data for use by headers.
// This must be performed prior to the insertion of any normal data.
// The number of bytes for headers
// must be a multiple of the word size in bytes.
// Return number of bytes actually reserved.
//
// This routine can safely be called multiple times as long as no data
// has yet been entered into the buffer.
//
uint_t
cursor::alloc_chunk(uint_t header_size)
{
	// Ensure that no data has yet been placed in the buffer.
	ASSERT(bu->first() == cursor_);

	// There must be sufficient contiguous space for the chunk
	ASSERT(header_size <= bu->length());

	// The header size in bytes must be a multiple of the word size
	ASSERT(header_size % sizeof (int) == 0);

	// Advance the cursor past the reserved chunk
	cursor_ += header_size;

	// Adjust the last indicator
	bu->incspan((int)header_size);

	// Adjust the start of data cursor past the header area
	bu->advance((int)header_size);

	return (header_size);
}

//
// make_header - creates a header out of the space reserved by alloc_chunk.
// Cursor advanced to after header.
// The first uint32_t of the header stores a marshal_debug when compiled with
// #def MARSHAL_DEBUG
//
void *
cursor::make_header(uint_t header_size)
{
	// There must be a buffer
	ASSERT(bu);
	// There must be room in the buffer for the header
	ASSERT(bu->first() >= header_size);
	// The header size must be a word size multiple.
	ASSERT(header_size % sizeof (int) == 0);

	// Advance the start of data in the backwards direction
	bu->advance(0 - (int)header_size);

#ifdef MARSHAL_DEBUG
	// The first word identifies this field as a message header
	*((uint32_t *)(bu->head())) = M_HEADER;
#endif

	// Verify that cursor_ is indeed at end of header
	ASSERT(cursor_ >= bu->first());
	ASSERT((cursor_ - bu->first()) == header_size);

	// Provide a pointer for the header
	return ((void *)(bu->head()));
}

// skip
uint_t
cursor::skip(uint_t n, bool alloc)
{
	uint_t tail;

	if (bu == NULL)
		return (0);

	// If can allocate, then advance cursor upto end of the Buf length
	// If !allocate (reading), then advance cursor only upto Buf::last()
	if (alloc) {
		tail = bu->length();
	} else {
		tail = bu->last();
	}
	if (cursor_ >= tail) {	// return 0 if cursor is already at tail
		return (0);
	}

	tail -= cursor_;	// tail is amount of available space in Buf
	tail = MIN(tail, n);	// take MIN of this and requested n

	cursor_ += tail;
	if (cursor_ > bu->last()) {	// advance last() if needed
		ASSERT(!alloc);		// this does not happen for reads
		bu->last(cursor_);
	}
	ASSERT(cursor_ <= bu->last());
	ASSERT(cursor_ >= bu->first());
	return (tail);
}

//  span - count the number of bytes used in the region.
uint_t
Region::span()
{
	// Optimization for NULL region - happens quite often
	if (empty())
		return (0);

	Buflist::ListIterator iter(buffers);
	Buf *b;
	uint_t s;

	for (s = 0; (b = iter.get_current()) != NULL; iter.advance()) {
		s += b->span();
	}
	return (s);
}

// dispose
void
Region::dispose()
{
	Buf *b;

	while ((b = buffers.reapfirst()) != NULL) {
		b->done();
	}
}


// goto_offset
uint_t
Region::goto_offset(uint_t off)
{
	//  Note that offsets in regions are taken only within
	//  the valid span of the region. Thus they start at "first_"

	Buf * b;

	// Do not use Iterator here as caller depends on the list current
	// pointer being updated
	buffers.atfirst();
	while ((b = buffers.get_current()) != NULL) {
		if (off < b->span()) {
			// offset falls entirely in the current buffer
			// this is the offset within the valid span of buffer
			return (off);
		}
		off -= b->span();
		buffers.advance();
	}

	// beyond the end;
	ASSERT(off == 0);	// Catches cases where go to offset is called
				// exceed end of Region.
				// Important caller is transport recstream
				// initialize routine => message header does
				// not correspond to message or corrupted
	return (0);
}

//
// goto_boundary - places the cursor at the specified offset in the region.
//	The method ensures that the offset occurs at the beginning of a buffer.
// Returns whether method succeeds.
//
bool
Region::goto_boundary(uint_t offset, os::mem_alloc_type flag)
{
	uint_t	start;		// offset to first byte containing data
				// in the buffer containing the boundary point.
	uint_t	bound;		// offset to the boundary within the
				// buffer containing the boundary point.

	Buf *bound_buf;	// buffer containing the boundary point,
			// which can be any buffer in the region.
	Buf *new_buf;
#ifdef DEBUG
	uint_t old_len = span();
#endif

	bound = goto_offset(offset);
	if (bound == 0) {
		return (true);
	}

	bound_buf = buffers.get_current();
	ASSERT(bound < bound_buf->span());
	start = bound_buf->first();

	// Duplicate the buffer without copying data
	new_buf = bound_buf->dup(start, bound, flag);
	if (new_buf != NULL) {
		// After dup, new_buf should have a span of 0
		ASSERT(new_buf->span() == 0);
		// Verify that new_buf has been properly constructed
		ASSERT(new_buf->head() == bound_buf->head());

		// Increment span on new_buffer
		new_buf->incspan((int)bound);
		// The existing buf drops the pre-boundary data
		bound_buf->advance((int)bound);

		// After bound_buf is changed, verify that the new_buf and
		// bound_buf have contig buffers
		ASSERT(new_buf->head() + new_buf->span() == bound_buf->head());

		// The new buf adds back the pre-boundary data to the Region
		insert(new_buf);
		ASSERT(old_len == span());	// Region span should not change
		return (true);
	} else {
		return (false);
	}
}

// Copy the first size bytes of the Region into dest
// Remove the contents while copying them.
// If there is an exception, we advance past the rest of the size bytes and
// delete them from the region (equivalent to get_bytes + trim_beginning)
void
Region::xfer_contents(char *dest, uint_t size, Environment *e)
{
	Buf *b;
	while (size && ((b = reap()) != NULL)) {
		ASSERT(!e->exception());
		uint_t bsize = b->span();

		// Transfer at most size bytes
		if (bsize > size)
			bsize = size;
		b->xfer_contents(dest, bsize, e);
		dest += bsize;
		size -= bsize;
		if (e->exception()) {
			Environment e2;
			xfer_contents(dest, size, &e2);
			e2.clear();		// neglect any exceptions
			break;
		}
	}
}

// Copy the first size bytes of the Region into dest
void
Region::copy_contents(char *dest, uint_t size, Environment *e)
{
	Buflist::ListIterator iter(buffers);
	Buf *b;
	uint_t bsize;

	for (; !e->exception() && (size != 0); iter.advance()) {
		// If run out of list => size > r.span()
		if ((b = iter.get_current()) == NULL) {
			e->system_exception(CORBA::BAD_PARAM(EOVERFLOW,
			    CORBA::COMPLETED_MAYBE));
			break;
		}

		// Transfer at most size bytes bsize = MIN(b->span(), size);
		bsize = b->span();
		if (bsize > size)
			bsize = size;
		b->copy_contents(dest, bsize, 0, e);
		dest += bsize;
		size -= bsize;
	}
}

#ifdef _KERNEL

// Transfers first size bytes of region to an mblk
// XXX Error handling is clumsy - need to cleanup
mblk_t *
Region::get_mblk(uint_t size, Environment *e)
{
	ASSERT(size <= span());
	Buf *b_ptr = reap();
	mblk_t *mp;

	// Optimize for single buffer case
	if (empty() && (size == b_ptr->span())) {
		// get_mblk deletes the Buf also
		return (b_ptr->get_mblk(0, 0, false, e));
	}
	// Put back first buffer at head
	prepend(b_ptr);
	mp = os::allocb(size, BPRI_MED, e->nonblocking_type());
	if (mp == NULL) {
		ASSERT(e->is_nonblocking());
		e->system_exception(CORBA::WOULDBLOCK(size,
			CORBA::COMPLETED_MAYBE));
		// on error leave the region unmodified
		return (NULL);
	}
	xfer_contents((char *)mp->b_wptr, size, e);
	if (e->exception()) {
		// On transfer error, the first size bytes are truncated
		freeb(mp);
		return (NULL);
	}
	mp->b_wptr += size;
	return (mp);
}

// convert from a Region to mblks of max_size leaving room for headers of size
// hsize. This routine is destructive and leaves the Region empty
mblk_t *
Region::Region2mblk(uint_t hsize, uint_t max_size, bool do_esballoc,
	    Environment *e)
{
	Buf		*b;
	mblk_chain	mc;

	ASSERT(!e->exception());
	while ((b = reap()) != NULL) {	// Removes first buffer of region
		// get_mblk will call done on b or will set the freefunc in
		// mblks to call b->done()
		// append result to end of chain
		mc.append_chain(b->get_mblk(hsize, max_size, do_esballoc, e));
		if (e->exception()) {
			dispose();
			mc.dispose();
			return (NULL);
		}
	}
	ASSERT(empty());
	return (mc.get_mblk_chain());
}

// copy from a Region to mblks of max_size leaving room for headers of size
// hsize. The Region is left unmodified
mblk_t *
Region::copy2mblk(uint_t hsize, uint_t max_size, Environment *e)
{
	Buf		*b;
	mblk_chain	mc;

	// XX This routine creates 1 mblk per Buf in the Region
	// It may be better to allocate max_size'd mblks and copy multiple
	// Bufs into fewer mblks (RFE)
	ASSERT(!e->exception());
	Buflist::ListIterator iter(buffers);
	while ((b = iter.get_current()) != NULL) {
		// Allocate mblk and copy_contents
		mc.append_chain(b->copy2mblk(hsize, max_size, e));
		if (e->exception()) {
			mc.dispose();
			return (NULL);
		}
		iter.advance();
	}
	ASSERT(mc.span() == span());
	return (mc.get_mblk_chain());
}

void
buf_done(char *arg)
{
	Buf *b = (Buf *) arg;
	// Decrement refcount on Buf and if reached 0, calls done()
	b->rele();
}

// This routine is called by Buf::get_mblk to
// do an desballoca call to wrap the buffer into the right mblk speech
// Acts as a wrapper to desballoca and calls buf->done() in the free routine
// We increment a refcount in the Buf each time we do this, and buf_done will
// wait till the refcount goes to zero to call buf_done.
// "len" is the length of the mblk that we should esballoc
// If called with os::SLEEP, will wait till memory is available
// else can return NULL if no memory is immediately available
// So caller needs to be careful to not deference b if len == span() or returns
// NULL
mblk_t *
Buf_esballoc(Buf *b, uint_t len, Environment *e)
{
	ASSERT(len <= b->span());
	mblk_t *mp;
	frtn_t *freeptr = b->frtnp();	// get pointer to frtn struct in Buf

	// increment refcount on Buf
	b->hold();

	// XXX The prototype is wrong in the streams header file
	// It should be (void(*)(char *))
	freeptr->free_func = (void(*)(void))buf_done;
	freeptr->free_arg  = (char *)b;

	//
	// The use of desballoca as opposed to desballoc is significant.
	// desballoca starts with a higher reference count which tells
	// TCP that it is not free to overrun the dblk in case it needs
	// space to stuff in more headers. We used to use desballoc and
	// that was resulting in data corruption when lossy private
	// interconnects were in use. Dropped packets used to result in
	// TCP to send selective ack options for which TCP needed more
	// header space and it was writing that over good data in the
	// buffers provided by Sun Cluster.
	//
	while ((mp = (desballoca(b->head(), (size_t)len, BPRI_MED, freeptr))) ==
	    NULL) {
		if (e->is_nonblocking()) {
			b->rele();		// release hold on Buf
			// Above rele may cause the Buf to be deleted
			e->system_exception(CORBA::WOULDBLOCK(
			    (uint_t)(sizeof (dblk_t) + sizeof (mblk_t)),
			    CORBA::COMPLETED_MAYBE));
			return (NULL);
		}
		os::envchk::check_nonblocking(
		    os::envchk::NB_INVO | os::envchk::NB_INTR, "Buf_esballoc");
		if (strwaitbuf((size_t)len, BPRI_HI) != 0) {
			// Unable to queue to wait! sleep and retry
			// EINTR - how to abort? callers dont handle failure
			os::usecsleep((os::usec_t)100000);	// 100ms
		}
	}
	mp->b_wptr += len;
	// advance head ptr in Buf, as we have copied len bytes out
	b->advance((int)len);
	return (mp);
}

// Allocate an mblk from a Buf by copying len bytes from Buf to the newly
// allocated mblk.
// This will leave hsize bytes at the front of mblk allocated
// This advances the head pointer in the Buf and if this causes the Buf
// to become empty, it calls Buf::done().
// This also calls Buf::done(), if there is an allocation error
// So caller needs to be careful to not deference b if len == span() or returns
// NULL
mblk_t *
Buf_allocb(Buf *b, uint_t hsize, uint_t len, Environment *e)
{
	mblk_t *mp;

	ASSERT(len <= b->span());

	// allocate an mblk to account for header also
	mp = os::allocb(len + hsize, BPRI_HI, e->nonblocking_type());
	if (mp == NULL) {
		// error during os::allocb - set exception
		ASSERT(e->is_nonblocking());
		e->system_exception(CORBA::WOULDBLOCK(len + hsize,
				CORBA::COMPLETED_MAYBE));
		// free buffer and return
		b->done();
		return (NULL);
	}
	// Adjust rptr to leave space for header
	mp->b_rptr += hsize;

	// Transfer contents from buffer to mblk
	// xfer_contents advances the Buf's first ptr
	// If len == b->span(), b is deleted - should not be referenced any more
	b->xfer_contents((char *)mp->b_rptr, len, e);
	if (e->exception()) {
		freeb(mp);
		// Buf b would have been deleted by xfer_contents
		return (NULL);
	}
	mp->b_wptr = mp->b_rptr + len;
	return (mp);
}

// Append an mblk chain to the chain
void
mblk_chain::append_chain(mblk_t *mp)
{
	// Allow for NULL mblk chain to be passed in
	if (mp == NULL)
		return;

	*tailp = mp;
	// advance to end of mblk chain
	while (mp->b_cont != NULL) {
		mp = mp->b_cont;
	}
	tailp = &(mp->b_cont);
}

// Return pointer to last mblk in chain
mblk_t *
mblk_chain::last_mblk()
{
	mblk_t *mp = head;

	if (mp == NULL)
		return (NULL);

	// advance to end of mblk chain
	while (mp->b_cont != NULL) {
		mp = mp->b_cont;
	}
	return (mp);
}

#endif

// If inlines are disabled the methods need to be compiled
// into the library so we include the _in.h files while
// #defining inline to be nothing giving non-inline versions
// of the methods.

#ifdef NOINLINES
#define	inline
#include <orb/buffers/Buf_in.h>
#undef	inline		// in case code is added below that uses inline
#endif	// NOINLINES
