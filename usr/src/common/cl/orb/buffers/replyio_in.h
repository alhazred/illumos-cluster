/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  replyio_in.h
 *
 */

#ifndef _REPLYIO_IN_H
#define	_REPLYIO_IN_H

#pragma ident	"@(#)replyio_in.h	1.7	08/05/20 SMI"

inline
replyio_client_t::replyio_client_t(struct buf *bp) :
	bufptr(bp),
	wait_for_copies(false)
{
}

inline
struct buf *
replyio_client_t::get_bufptr() const
{
	return (bufptr);
}

inline void
replyio_client_t::mark_wait_for_copies()
{
	wait_for_copies = true;
}

inline
replyio_server_t::replyio_server_t() :
	cookie(NULL),
	generation(0)
{
}

inline
replyio_server_t::replyio_server_t(MarshalStream &ms) :
	generation(0)
{
	unmarshal_cookie(ms);
}

inline void *
replyio_server_t::get_cookie() const
{
	return (cookie);
}

inline uint_t
replyio_server_t::get_generation() const
{
	return (generation);
}

inline void
replyio_server_t::incr_generation()
{
	// Need not hold locks as there can only be one thread manipulating
	// the cookie at any given time. This is ensured by the ORB because
	// the sendstream is freed before it starts remarshalling in a
	// different sendstream
	++generation;
	ASSERT(generation != 0);	// check wraparound
}

#endif	/* _REPLYIO_IN_H */
