/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  marshalstream_in.h
 *
 */

#ifndef _MARSHALSTREAM_IN_H
#define	_MARSHALSTREAM_IN_H

#pragma ident	"@(#)marshalstream_in.h	1.17	08/05/20 SMI"

// MarshalStream ops

//
// constructor - creates stream specifying the needed buffer size and chunk size
//
inline
MarshalStream::MarshalStream(uint_t byte_size, Environment *e) :
	alloc_size(byte_size),		// Initial buffer size
	chunksize(DEFAULTCHUNK),	// Buffer expansion size
	env(e)
{
	// Buffer creation is a virtual function,
	// and virtual functions should not be called in the constructor.
	// So do not allocate any Bufs in the constructor
}

inline
MarshalStream::MarshalStream(Environment *e, uint_t byte_size,
    uint_t chunk_size) :
	alloc_size(byte_size),		// Initial buffer size
	chunksize(chunk_size),		// Buffer expansion size
	env(e)
{
	// Buffer creation is a virtual function,
	// and virtual functions should not be called in the constructor.
	// So do not allocate any Bufs in the constructor
}

//
// constructor - creates stream using the specified buffer
//
inline
MarshalStream::MarshalStream(Buf *b, Environment *e) :
	alloc_size(DEFAULTCHUNK),	// Initial buffer size
	chunksize(DEFAULTCHUNK),	// Buffer expansion size
	env(e)
{
	// The stream is currently empty.
	// The constructor is provided with the initial buffer
	vbuf.append(b);
	cur.setbuf(vbuf.buffer());
}

//
// constructor - creates a stream using the specified region.
//	The second arg specifies the size of the new region in bytes
//		(currently unused).
//
inline
MarshalStream::MarshalStream(Region &new_region, Environment *e) :
	alloc_size(DEFAULTCHUNK),	// Initial buffer size
	chunksize(DEFAULTCHUNK),	// Buffer expansion size
	env(e)
{
	// Concatenate specified region to the existing empty region
	vbuf.concat(new_region);
	cur.setbuf(vbuf.buffer());
}

#ifdef _KERNEL
inline mblk_t *
MarshalStream::Region2mblk(uint_t hsize, uint_t max_size, bool do_esballoc,
	Environment *e)
{
	return (region().Region2mblk(hsize, max_size, do_esballoc, e));
}
#endif

// prepare
inline void
MarshalStream::prepare()
{
	vbuf.rewind();
	cur.setbuf(vbuf.buffer());
}

//
// make_header - makes a header out of the space reserved by alloc_chunk.
// Cursor placed after header.
//
inline void *
MarshalStream::make_header(size_t header_size)
{
	// Go to first buffer
	prepare();

	ASSERT(header_size <= UINT_MAX);
	// Carve a header out of first buffer
	return (cur.make_header((uint_t)header_size));
}

//
// make_header - makes a header out of the space reserved by alloc_chunk.
// 		Writes the contents of p into the reserved space.
//		Cursor advances to after header.
//
inline void
MarshalStream::write_header(void *p, size_t header_size)
{
	void *dest = make_header(header_size);
#ifdef MARSHAL_DEBUG
	*((uint32_t *)p) = M_HEADER;
#endif
	bcopy(p, dest, header_size);
}

// Return the env pointer associated with the MarshalStream
inline Environment *
MarshalStream::get_env()
{
	return (env);
}

inline void
MarshalStream::set_chunksize(uint_t chunk_size)
{
	chunksize = chunk_size;
}

inline void
MarshalStream::set_allocsize(uint_t asize)
{
	alloc_size = asize;
}

inline Region &
MarshalStream::region()
{
	cur.setbuf(NULL);
	return (vbuf);
}

inline void
MarshalStream::dispose()
{
	cur.setbuf(NULL);
	vbuf.dispose();
}

inline uint_t
MarshalStream::span()
{
	return (vbuf.span());
}

inline bool
MarshalStream::empty()
{
	return (vbuf.empty());
}

inline void
MarshalStream::set_env(Environment *e)
{
	env = e;
}

#ifdef _LP64
// Wrappers to allow passing in size_t instead of uint_t
inline uint_t
MarshalStream::alloc_chunk(size_t len)
{
	ASSERT(len <= UINT_MAX);
	return (alloc_chunk((uint_t)len));
}

inline void
MarshalStream::put_bytes(const void *p, size_t len, bool debug)
{
	ASSERT(len <= UINT_MAX);
	put_bytes(p, (uint_t)len, debug);
}

inline void
MarshalStream::get_bytes(void *p, size_t len, bool debug)
{
	ASSERT(len <= UINT_MAX);
	get_bytes(p, (uint_t)len, debug);
}

inline int
MarshalStream::get_u_bytes(caddr_t bs, size_t len, bool debug)
{
	ASSERT(len <= UINT_MAX);
	return (get_u_bytes(bs, (uint_t)len, debug));
}
#endif

#ifdef MARSHAL_DEBUG
inline void
MarshalStream::debug_put_word(const uint_t x)
{
	// XXX For some reason, compiler is requiring the temporary variable...
	uint_t tul = x;
	put_bytes(&tul, sizeof (uint_t), false);
}
#else
inline void
MarshalStream::debug_put_word(const uint_t)
{
}
#endif

// put_char, put_octet, put_*short have inlined
//	calling specific routines on the writer class
inline void
MarshalStream::put_boolean(bool bo)
{
	debug_put_word(M_BOOLEAN);
	put_bytes(&bo, sizeof (bool), false);
}

inline void
MarshalStream::put_char(char ch)
{
	debug_put_word(M_CHAR);
	put_bytes(&ch, sizeof (char), false);
}

inline void
MarshalStream::put_octet(uint8_t uc)
{
	debug_put_word(M_OCTET);
	put_bytes(&uc, sizeof (uint8_t), false);
}

inline void
MarshalStream::put_short(short sh)
{
	debug_put_word(M_SHORT);
	put_bytes(&sh, sizeof (short), false);
}

inline void
MarshalStream::put_unsigned_short(uint16_t us)
{
	debug_put_word(M_UNSIGNED_SHORT);
	put_bytes(&us, sizeof (uint16_t), false);
}

inline void
MarshalStream::put_int(int i)
{
	debug_put_word(M_INT);
	put_bytes(&i, sizeof (int), false);
}

inline void
MarshalStream::put_long(int32_t lo)
{
	debug_put_word(M_LONG);
	put_bytes(&lo, sizeof (int32_t), false);
}

inline void
MarshalStream::put_unsigned_int(uint_t ui)
{
	debug_put_word(M_UNSIGNED_INT);
	put_bytes(&ui, sizeof (ui), false);
}

inline void
MarshalStream::put_unsigned_long(uint32_t ul)
{
	debug_put_word(M_UNSIGNED_LONG);
	put_bytes(&ul, sizeof (uint32_t), false);
}

inline void
MarshalStream::put_unsigned_longlong(uint64_t ull)
{
	debug_put_word(M_UNSIGNED_LONGLONG);
	put_bytes(&ull, sizeof (uint64_t), false);
}

inline void
MarshalStream::put_longlong(int64_t ll)
{
	debug_put_word(M_LONGLONG);
	put_bytes(&ll, sizeof (int64_t), false);
}

#ifdef ORBFLOAT
inline void
MarshalStream::put_float(float fl)
{
	debug_put_word(M_FLOAT);
	put_bytes(&fl, sizeof (float), false);
}

inline void
MarshalStream::put_double(double db)
{
	debug_put_word(M_DOUBLE);
	put_bytes(&db, sizeof (double), false);
}
#endif // ORBFLOAT

inline void
MarshalStream::put_pointer(void *ptr)
{
	debug_put_word(M_POINTER);
	put_bytes(&ptr, sizeof (void *), false);
};

inline bool
MarshalStream::get_boolean()
{
	debug_get_word(M_BOOLEAN);
	bool db;
	get_bytes(&db, sizeof (bool), false);
	return (db);
}
inline char
MarshalStream::get_char()
{
	debug_get_word(M_CHAR);
	char db;
	get_bytes(&db, sizeof (char), false);
	return (db);
}

inline uint8_t
MarshalStream::get_octet()
{
	debug_get_word(M_OCTET);
	uint8_t db;
	get_bytes(&db, sizeof (uint8_t), false);
	return (db);
}

inline int16_t
MarshalStream::get_short()
{
	debug_get_word(M_SHORT);
	short db;
	get_bytes(&db, sizeof (short), false);
	return (db);
}

inline uint16_t
MarshalStream::get_unsigned_short()
{
	debug_get_word(M_UNSIGNED_SHORT);
	uint16_t db;
	get_bytes(&db, sizeof (uint16_t), false);
	return (db);
}

inline int
MarshalStream::get_int()
{
	debug_get_word(M_INT);
	int db;
	get_bytes(&db, sizeof (int), false);
	return (db);
}

inline int32_t
MarshalStream::get_long()
{
	debug_get_word(M_LONG);
	int32_t db;
	get_bytes(&db, sizeof (int32_t), false);
	return (db);
}

inline uint_t
MarshalStream::get_unsigned_int()
{
	debug_get_word(M_UNSIGNED_INT);
	uint_t db;
	get_bytes(&db, sizeof (uint_t), false);
	return (db);
}

inline uint32_t
MarshalStream::get_unsigned_long()
{
	debug_get_word(M_UNSIGNED_LONG);
	uint32_t db;
	get_bytes(&db, sizeof (uint32_t), false);
	return (db);
}

inline int64_t
MarshalStream::get_longlong()
{
	debug_get_word(M_LONGLONG);
	int64_t db;
	get_bytes(&db, sizeof (int64_t), false);
	return (db);
}

inline uint64_t
MarshalStream::get_unsigned_longlong()
{
	debug_get_word(M_UNSIGNED_LONGLONG);
	uint64_t db;
	get_bytes(&db, sizeof (uint64_t), false);
	return (db);
}

#ifdef ORBFLOAT
inline float
MarshalStream::get_float()
{
	debug_get_word(M_FLOAT);
	float db;
	get_bytes(&db, sizeof (float), false);
	return (db);
}

inline double
MarshalStream::get_double()
{
	debug_get_word(M_DOUBLE);
	double db;
	get_bytes(&db, sizeof (double), false);
	return (db);
}
#endif // ORBFLOAT

inline void *
MarshalStream::get_pointer()
{
	debug_get_word(M_POINTER);
	void *ptr;
	get_bytes(&ptr, sizeof (void *), false);
	return (ptr);
}

#ifndef MARSHAL_DEBUG
// non-debug version only. debug version is in .cc file
inline void
MarshalStream::debug_get_word(const uint_t)
{
}
#endif

// nextbuf
inline void
MarshalStream::nextbuf()
{
	if (cur.buffer()) {
		vbuf.goto_next();
	}
	cur.setbuf(vbuf.buffer());
}

// read_header - corresponding to make_header on send side
// read and trim the header
inline void
MarshalStream::read_header(void *p, size_t hsize)
{
	// XX This assumes that the cursor is at the head of the
	// marshalstream. Need an ASSERT for this
	get_bytes(p, hsize, false);
#ifdef MARSHAL_DEBUG
	// The first word identifies this field as a message header
	ASSERT(*((uint32_t *)p) == M_HEADER);
#endif
	trim_beginning();
}

// split_at_boundary
// Splits the MarshalStream at offset off. Rewinds the Marshal Stream.
// Returns the tail portion in Region r.
inline bool
MarshalStream::split_at_boundary(uint_t off, Region &r, os::mem_alloc_type flag)
{
	if (vbuf.goto_boundary(off, flag)) {
		vbuf.split(r);
		ASSERT(vbuf.span() == off);
		prepare();	// Intialize current MarshalStream correctly
		return (true);
	} else {
		return (false);
	}
}

// split_at_boundary
// Splits the MarshalStream at offset off. Rewinds the Marshal Stream.
// Returns the tail portion in MarshalStream r.
inline bool
MarshalStream::split_at_boundary(uint_t off, MarshalStream &r,
    os::mem_alloc_type flag)
{
	if (vbuf.goto_boundary(off, flag)) {
		vbuf.split(r.region());
		ASSERT(vbuf.span() == off);
		prepare();	// Intialize current MarshalStream correctly
		r.prepare();	// Initialize the tail MarshalStream correctly
		return (true);
	} else {
		return (false);
	}
}

//
// sendstream methods
//

inline MarshalStream &
sendstream::get_MainBuf()
{
	ASSERT(mainbufp != NULL);
	return (*mainbufp);
}

inline MarshalStream &
sendstream::get_XdoorTab()
{
	return (XdoorTab);
}

inline orb_msgtype
sendstream::get_msgtype() const
{
	return (resourcep->get_send_msgtype());
}

inline invocation_mode
sendstream::get_invo_mode() const
{
	return (resourcep->get_invo_mode());
}

inline Environment *
sendstream::get_env()
{
	return (resourcep->get_env());
}

inline resources *
sendstream::get_resourcep()
{
	return (resourcep);
}

inline void
sendstream::set_oneway()
{
	resourcep->set_oneway();
}

inline void
sendstream::set_twoway()
{
	resourcep->set_oneway();
}

inline uint_t
sendstream::get_xdoorcount() const
{
	return (xdoorcount);
}

inline uint_t
sendstream::span()
{
	return (get_MainBuf().span() + XdoorTab.span() + OffLineBufs.span());
}

#ifdef _KERNEL_ORB
// user-land orb overrides this in sdoor_sendstream
inline void
sendstream::done()
{
	delete this;
}
#endif

// put_boolean
inline void
sendstream::put_boolean(const bool bo)
{
	get_MainBuf().put_boolean(bo);
}

// put_char
inline void
sendstream::put_char(const char ch)
{
	get_MainBuf().put_char(ch);
}

// put_octet
inline void
sendstream::put_octet(const uint8_t uc)
{
	get_MainBuf().put_octet(uc);
}

// put_short
inline void
sendstream::put_short(const int16_t sh)
{
	get_MainBuf().put_short(sh);
}

// put_unsigned_short
inline void
sendstream::put_unsigned_short(const uint16_t us)
{
	get_MainBuf().put_unsigned_short(us);
}

// put_int
inline void
sendstream::put_int(const int i)
{
	get_MainBuf().put_int(i);
}

// put_long
inline void
sendstream::put_long(const int32_t lo)
{
	get_MainBuf().put_long(lo);
}

// put_unsigned_int
inline void
sendstream::put_unsigned_int(const uint_t i)
{
	get_MainBuf().put_unsigned_int(i);
}

// put_unsigned_long
inline void
sendstream::put_unsigned_long(const uint32_t ul)
{
	get_MainBuf().put_unsigned_long(ul);
}

// put_unsigned_longlong
inline void
sendstream::put_unsigned_longlong(const uint64_t ull)
{
	get_MainBuf().put_unsigned_longlong(ull);
}

// put_longlong
inline void
sendstream::put_longlong(const int64_t ll)
{
	get_MainBuf().put_longlong(ll);
}

#ifdef ORBFLOAT
// put_float
inline void
sendstream::put_float(const float fl)
{
	get_MainBuf().put_float(fl);
}

// put_double
inline void
sendstream::put_double(const double db)
{
	get_MainBuf().put_double(db);
}
#endif // ORBFLOAT

// put_pointer
inline void
sendstream::put_pointer(const void *p)
{
	get_MainBuf().put_pointer((void *)p);
}

// put_string
inline void
sendstream::put_string(const char *st)
{
	get_MainBuf().put_string(st);
}

// put_seq_hdr
inline void
sendstream::put_seq_hdr(const GenericSequence *sh)
{
	get_MainBuf().put_seq_hdr(sh);
}

// put_seq
inline void
sendstream::put_seq(const GenericSequence *seq, uint_t si)
{
	get_MainBuf().put_seq(seq, si);
}

// put_bytes
inline void
sendstream::put_bytes(const void *bs, uint_t len)
{
	get_MainBuf().put_bytes(bs, len);
}

// put_u_bytes
inline int
sendstream::put_u_bytes(const caddr_t bs, uint_t len, bool debug)
{
	return (get_MainBuf().put_u_bytes(bs, len, debug));
}

#ifdef _LP64
// Convenience methods to use size_t also instead of just uint_t
inline void
sendstream::put_bytes(const void *bs, size_t len)
{
	get_MainBuf().put_bytes(bs, len);
}

// alloc_chunk
inline uint_t
sendstream::alloc_chunk(size_t header_size)
{
	return (get_MainBuf().alloc_chunk(header_size));
}
#endif

// debug_put_word
inline void
sendstream::debug_put_word(const uint_t t)
{
	get_MainBuf().debug_put_word(t);
}

// put_xdoor
inline void
sendstream::put_xdoor(Xdoor *xdp)
{
	get_XdoorTab().put_pointer(xdp);
	xdoorcount++;
}

// make_header
inline void *
sendstream::make_header(size_t header_size)
{
	return (get_MainBuf().make_header(header_size));
}

// write_header
inline void
sendstream::write_header(void *p, size_t header_size)
{
	get_MainBuf().write_header(p, header_size);
}

// alloc_chunk
inline uint_t
sendstream::alloc_chunk(uint_t header_size)
{
	return (get_MainBuf().alloc_chunk(header_size));
}

// serv2_transport_info methods
#ifdef _KERNEL_ORB
inline
serv2_transport_info::serv2_transport_info() :
    prefer_ep(NULL),
    valid(false)
{
}

inline
serv2_transport_info::~serv2_transport_info()
{
	if (prefer_ep) {
		((refcnt *) prefer_ep)->rele(); // Matches hold in set_info
	}
}

inline endpoint *
serv2_transport_info::get_endpoint()
{
	return (prefer_ep);
}

inline void
serv2_transport_info::set_endpoint(endpoint *ep)
{
	prefer_ep = ep;
	if (ep) {
		valid = true;
		((refcnt *) ep)->hold();
	}
}

inline void
serv2_transport_info::invalidate()
{
	valid = false;
}

inline bool
serv2_transport_info::isvalid()
{
	return (valid);
}
#endif

//
// recstream methods
//

inline
recstream::recstream(orb_msgtype msgt, invocation_mode mode) :
	xdoorcount(0),
	invo_mode(mode),
	msgtype(msgt),
	MainBuf(NULL),
	XdoorTab(NULL),
	rec_seq(0),
	rec_flag(os::NO_SLEEP)
{
#ifdef _KERNEL_ORB
	ep = NULL;
#endif
}

#ifdef _KERNEL_ORB
inline
recstream::recstream(orb_msgtype msgt, invocation_mode mode,
    endpoint *endp) :
    xdoorcount(0),
    invo_mode(mode),
    msgtype(msgt),
    MainBuf(NULL),
    XdoorTab(NULL),
    rec_seq(0),
    rec_flag(os::NO_SLEEP),
    ep(endp)
{
	if (ep) {
		((refcnt *) ep)->hold();
	}
}

inline void
recstream::transfer_serv2_transport_info(serv2_transport_info &ti)
{
	ti.set_endpoint(ep);
}

#endif

inline MarshalStream &
recstream::get_MainBuf()
{
	return (MainBuf);
}

inline MarshalStream &
recstream::get_XdoorTab()
{
	return (XdoorTab);
}

inline uint_t
recstream::get_xdoorcount() const
{
	return (xdoorcount);
}

inline uint_t
recstream::span()
{
	return (MainBuf.span() + XdoorTab.span() + OffLineBufs.span());
}

// Get all pointers in recstream to head of recstream
inline void
recstream::prepare()
{
	MainBuf.prepare();
	XdoorTab.prepare();
	OffLineBufs.rewind();
}

inline orb_msgtype
recstream::get_msgtype() const
{
	return (msgtype);
}

#ifdef _KERNEL_ORB
// user-land orb overrides this in sdoor_recstream
inline void
recstream::done()
{
	delete this;
}
#endif

inline void
recstream::set_oneway()
{
	invo_mode.set_oneway();
}

inline void
recstream::set_twoway()
{
	invo_mode.set_twoway();
}

inline invocation_mode
recstream::get_invo_mode() const
{
	return (invo_mode);
}

// get_boolean
inline bool
recstream::get_boolean()
{
	return (get_MainBuf().get_boolean());
}

// get_char
inline char
recstream::get_char()
{
	return (get_MainBuf().get_char());
}

// get_octet
inline uint8_t
recstream::get_octet()
{
	return (get_MainBuf().get_octet());
}

// get_short
inline int16_t
recstream::get_short()
{
	return (get_MainBuf().get_short());
}

// get_unsigned_short
inline uint16_t
recstream::get_unsigned_short()
{
	return (get_MainBuf().get_unsigned_short());
}

// get_int
inline int
recstream::get_int()
{
	return (get_MainBuf().get_int());
}

// get_long
inline int32_t
recstream::get_long()
{
	return (get_MainBuf().get_long());
}

// get_unsigned_int
inline uint_t
recstream::get_unsigned_int()
{
	return (get_MainBuf().get_unsigned_int());
}

// get_unsigned_long
inline uint32_t
recstream::get_unsigned_long()
{
	return (get_MainBuf().get_unsigned_long());
}

// get_longlong
inline int64_t
recstream::get_longlong()
{
	return (get_MainBuf().get_longlong());
}


// get_unsigned_longlong
inline uint64_t
recstream::get_unsigned_longlong()
{
	return (get_MainBuf().get_unsigned_longlong());
}

#ifdef ORBFLOAT
// get_float
inline float
recstream::get_float()
{
	return (get_MainBuf().get_float());
}


// get_double
inline double
recstream::get_double()
{
	return (get_MainBuf().get_double());
}
#endif // ORBFLOAT

// get_pointer
inline void *
recstream::get_pointer()
{
	return (get_MainBuf().get_pointer());
}

// get_string
inline char *
recstream::get_string()
{
	return (get_MainBuf().get_string());
}

// get_string_cancel
inline void
recstream::get_string_cancel()
{
	get_MainBuf().get_string_cancel();
}

// get_seq_hdr
inline void
recstream::get_seq_hdr(GenericSequence *sep)
{
	get_MainBuf().get_seq_hdr(sep);
}

// get_seq
inline void
recstream::get_seq(GenericSequence *sp, uint_t size)
{
	get_MainBuf().get_seq(sp, size);
}

// get_seq_cancel
inline void
recstream::get_seq_cancel(uint_t size)
{
	get_MainBuf().get_seq_cancel(size);
}

// get_bytes
inline void
recstream::get_bytes(void *bs, uint_t length)
{
	get_MainBuf().get_bytes(bs, length);
}

// get_u_bytes
inline int
recstream::get_u_bytes(caddr_t bs, uint_t length)
{
	return (get_MainBuf().get_u_bytes(bs, length));
}

#ifdef _LP64
// Convenience methods to use size_t also instead of just uint_t
inline void
recstream::get_bytes(void *bs, size_t length)
{
	get_MainBuf().get_bytes(bs, length);
}

inline int
recstream::get_u_bytes(caddr_t bs, size_t length)
{
	return (get_MainBuf().get_u_bytes(bs, length));
}
#endif

// debug_get_word
inline void
recstream::debug_get_word(const uint_t t)
{
	get_MainBuf().debug_get_word(t);
}

// read_header - corresponding to make_header on send side
inline void
recstream::read_header(void *p, size_t hsize)
{
	get_MainBuf().read_header(p, hsize);
}

// get_xdoor
inline Xdoor *
recstream::get_xdoor()
{
	return ((Xdoor *)get_XdoorTab().get_pointer());
}

inline orb_seq_t
recstream::get_rec_seq() const
{
	return (rec_seq);
}

inline os::mem_alloc_type
recstream::get_rec_flag() const
{
	return (rec_flag);
}

inline void
recstream::set_rec_seq(orb_seq_t seq)
{
	rec_seq = seq;
}

inline void
recstream::set_rec_flag(os::mem_alloc_type flag)
{
	rec_flag = flag;
}

#endif	/* _MARSHALSTREAM_IN_H */
