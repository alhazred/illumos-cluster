/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_IDLVERSION_IMPL_H
#define	_IDLVERSION_IMPL_H

#pragma ident	"@(#)idlversion_impl.h	1.5	08/05/20 SMI"

extern const char idlversion_template[];

#ifdef _KERNEL_ORB

#include <h/version.h>
#include <orb/object/adapter.h>

//
// One of these objects is created in each kernel ORB.
// The interface is used by the ORB framework to query other ORB domains
// for the interface descriptor (wire schema) of locally unknown type IDs.
//
class idlversion_impl : public McNoref<idlversion> {
public:
	idlversion_impl();
	~idlversion_impl();

	static int initialize();
	static int register_with_ns();

	inf_descriptor *get_dynamic_descriptor(const inf_typeid tid,
	    CORBA::Object_ptr obj, Environment &_environment);

	static idlversion_ptr self_p;
};

#endif // _KERNEL_ORB

#endif /* _IDLVERSION_IMPL_H */
