//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)schema.cc	1.35	08/05/20 SMI"

#include <limits.h>
#include <orb/object/schema.h>
#include <orb/object/adapter.h>
#include <nslib/ns.h>
#include <h/version.h>
#include <h/version_manager.h>
#include <sys/vm_util.h>
#include <sys/cl_comm_support.h>

#include <orb/infrastructure/orb.h>
#include <orb/object/idlversion_impl.h>

_Ix_extern__tid(CORBA_Object_0);
extern InterfaceDescriptor _Ix__type(CORBA_Object);

//
// ExceptDescriptor methods
//

ExceptDescriptor::ExceptDescriptor(const char *new_namep, CORBA::TypeId tid,
    OxTypeObj_UnmarshalException func) :
	_SList::ListElem(this),
	namep(new_namep),
	deep_type_id(tid),
	unmarshal(func)
{
}

//
// This is called by schema_list<T>::add() if a duplicate is found.
// Return true if update is allowed.
//
bool
ExceptDescriptor::update(ExceptDescriptor *exp)
{
	return (exp != NULL && os::strcmp(namep, exp->get_name()) == 0);
}

//
// InterfaceDescriptor methods.
//

extern InterfaceDescriptor::VersionTable unknown_inf[];

InterfaceDescriptor::InterfaceDescriptor(const char *new_namep,
    CORBA::TypeId tid, CORBA::TypeId vers_0_tid, uint_t vers,
    uint_t *offsets_p, VersionTable *type_list, ReceiverTable *func_list,
    generic_proxy::ProxyCreator create_func) :
	_SList::ListElem(this),
	namep(new_namep),
	deep_type_id(tid),
	offsets(offsets_p),
	type_to_index(type_list),
	index_to_receiver(func_list),
	version(vers),
	version_0_tid(vers_0_tid),
	create_proxyp(create_func)
{
	if (type_to_index == NULL)
		type_to_index = unknown_inf;

	// Find the highest interface index number which has methods.
	num_interfaces = 0;
	for (int i = 0; type_to_index[i].tid != NULL; i++) {
		if (num_interfaces <= type_to_index[i].index) {
			num_interfaces = type_to_index[i].index + 1;
		}
	}
}

//
// Return turn true if this interface descriptor is for an unknown type.
// (see remote_handler::get_inf_descrptor() for details)
//
bool
InterfaceDescriptor::is_unknown()
{
	return (type_to_index == unknown_inf);
}

//
// is_type_supported - Returns true if the target type id
// can be supported by this class.
//
// Linear search is appropriate because
//	A) high percentage of operations are on the deep type
//		which is the first object type in the list.
//	B) we recommend short ancestry trees, which usually
//		means no more than 2 ancestor classes.
//		XXX This may not be true as we add more versioned interfaces.
//
bool
InterfaceDescriptor::is_type_supported(CORBA::TypeId target_id)
{
	VersionTable *p = type_to_index;

	//
	// The list is one linear list that is organized in two parts.
	// Both parts are null terminated.
	// The first part has the type id's of classes with methods.
	// The second part has the type id's of classes without methods.
	// All interfaces inherit from CORBA::Object so we check that last.
	//
	for (int number_nulls = 0; ; p++) {
		if (p->tid == NULL) {
			if (++number_nulls < 2)
				continue;
			break;
		}
		if (_ox_equal_tid(target_id, p->tid)) {
			return (true);
		}
	}
	if (_ox_equal_tid(target_id, _Ix__tid(CORBA_Object_0)))
		return (true);
	return (false);
}

//
// lookup_type_index -
// 	Look up the object type and return the interface index number.
//	Return -1 if the type is not found.
//	This only searches types which have methods unless
//	'include_nm' is true.
//
// Linear search is appropriate because
//	A) high percentage of operations are on the deep class
//		which is the first object type in the list.
//	B) we recommend short ancestry trees, which usually
//		means no more than 2 ancestor classes.
//		XXX This may not be true as we add more versioned interfaces.
//
int
InterfaceDescriptor::lookup_type_index(CORBA::TypeId target_id, bool include_nm)
{
	VersionTable *p;

	// Find the invocation interface type id in the list.
	for (p = type_to_index; p->tid != NULL; p++) {
		if (_ox_equal_tid(target_id, p->tid)) {
			return ((int)p->index);
		}
	}
	//
	// CORBA::Object is inherited from all interfaces but doesn't
	// have an explicit entry in the type to index table.
	// Instead, we reserve a "magic" number which is guaranteed to
	// not conflict with the index values normally used.
	//
	if (_ox_equal_tid(target_id, _Ix__tid(CORBA_Object_0))) {
		return (INF_CORBA_OBJECT);
	}
	//lint -e796
	if (include_nm) {
		// Look for a match in the table of interfaces with no methods.
		while ((++p)->tid != NULL) {	//lint !e794
			if (_ox_equal_tid(target_id, p->tid)) {
				return ((int)p->index);
			}
		}
	}
	//lint +e796
	return (-1);
}

//
// lookup_rec_func -
//	Return the receiver function given an index from an
//	earlier lookup_type_index() call and the method number.
//
InterfaceDescriptor::ReceiverFunc
InterfaceDescriptor::lookup_rec_func(uint_t inf_index, uint_t method)
{
	if (inf_index == INF_CORBA_OBJECT) {
		InterfaceDescriptor::ReceiverTable &rt =
		    _Ix__type(CORBA_Object).index_to_receiver[0];
		if (method >= rt.num_methods) {
			return (NULL);
		}
		return (rt.table[method]);
	}
	if (inf_index >= num_interfaces ||
	    method >= index_to_receiver[inf_index].num_methods) {
		return (NULL);
	}
	return (index_to_receiver[inf_index].table[method]);
}

//
// Widen the given object to the desired interface type.
// This should not fail since lookup_rec_func() should have been
// called first to get the index.
//
void *
InterfaceDescriptor::cast(void *obj, uint_t inf_index)
{
	// We should only be doing a cast if we know the type at compile time.
	ASSERT(create_proxyp != NULL);

	if (offsets == NULL || inf_index == INF_CORBA_OBJECT)
		return (obj);
	return ((void *)((char *)obj + offsets[inf_index]));
}

//
// Return the version number for the requested interface.
//
int
InterfaceDescriptor::get_version(CORBA::TypeId target_id)
{
	//
	// Find the highest version InterfaceDescriptor which supports
	// the base interface asked for.
	// XXX This code currently relies on the order of entries in
	// type_to_index that the IDL compiler generates. Otherwise,
	// we should search the whole list and pick the highest version.
	//
	VersionTable *p = type_to_index;
	for (int number_nulls = 0; ; p++) {
		InterfaceDescriptor *ifp;
		if (p->tid == NULL) {
			if (++number_nulls >= 2)
				break;
			ifp = OxSchema::schema()->descriptor(
			    _Ix__tid(CORBA_Object_0), NULL);
			ASSERT(ifp != NULL);
			if (ifp != NULL && ifp->version_0_tid != NULL &&
			    _ox_equal_tid(target_id, ifp->version_0_tid)) {
				return ((int)ifp->version);
			}
			continue;
		}
		ifp = OxSchema::schema()->descriptor(p->tid, NULL);
		if (ifp != NULL && ifp->version_0_tid != NULL &&
		    _ox_equal_tid(target_id, ifp->version_0_tid)) {
			return ((int)ifp->version);
		}
	}
	return (-1);
}

//
// Return an opaque type for the InterfaceDescriptor of the given version.
//
CORBA::type_info_t *
InterfaceDescriptor::get_type_info(int v)
{
	VersionTable *p = type_to_index;
	for (int number_nulls = 0; ; p++) {
		InterfaceDescriptor *ifp;
		if (p->tid == NULL) {
			if (++number_nulls >= 2)
				break;
			ifp = OxSchema::schema()->descriptor(
			    _Ix__tid(CORBA_Object_0), NULL);
			ASSERT(ifp != NULL);
			if (ifp != NULL && ifp->version == (uint_t)v &&
			    _ox_equal_tid(version_0_tid, ifp->version_0_tid)) {
				return ((CORBA::type_info_t *)ifp);
			}
			continue;
		}
		ifp = OxSchema::schema()->descriptor(p->tid, NULL);
		if (ifp != NULL && ifp->version == (uint_t)v &&
		    _ox_equal_tid(version_0_tid, ifp->version_0_tid)) {
			return ((CORBA::type_info_t *)ifp);
		}
	}
	return (NULL);
}

//
// Return a new proxy for this interface but with the given deep type.
//
CORBA::Object_ptr
InterfaceDescriptor::create_proxy_obj(handler *handlerp,
    InterfaceDescriptor *dp)
{
	if (create_proxyp != NULL)
		return ((CORBA::Object_ptr)(*create_proxyp)(handlerp, dp));
	return (CORBA::Object::_nil());
}

//
// Return the size of the method table in bytes.
//
uint_t
InterfaceDescriptor::sizeof_methods(uint_t inf_index)
{
	if (inf_index == INF_CORBA_OBJECT)
		return (1);

	// Check array bounds.
	ASSERT(inf_index < num_interfaces);
	// Check that we know how many methods there are.
	ASSERT(index_to_receiver != NULL);

	//
	// If the interface descriptor was created for a type we don't
	// have local compiled knowledge for and then a module was
	// loaded which defined some of the inherited types, we
	// could have stale information for the number of methods.
	// For example: say we have two versions (0 and 1) of an interface A.
	// Node 1 creates a version 1 object and registers it with the
	// name server primary on node 2. Node 2 unmarshals the object,
	// calls node 1 to get the interface descriptor information and
	// creates an interface descriptor with entries for A_0 and A_1.
	// At this time, node 2 has no information about A_0 so
	// index_to_receiver[0 and 1].num_methods are both zero.
	// Later, a module is loaded into the ORB on node 2 which
	// registers an A_0 interface descriptor. Node 2 can now narrow
	// the reference from the name server to A_ptr and call the
	// version 0 methods but it will look at the A_1 interface
	// descriptor since this is the deep type for the object.
	//
	if (index_to_receiver[inf_index].num_methods == 0) {
		//
		// Find the TID that matches the inf_index.
		// This should be in the first half of the table since
		// lookup_type_index(tid, false) should have been called.
		//
		VersionTable *vp;
		for (vp = type_to_index; ; vp++) {
			if (vp->index == inf_index)
				break;
		}
		InterfaceDescriptor *infp =
		    OxSchema::schema()->descriptor(vp->tid, NULL);
		ASSERT(infp != NULL);
		for (vp = infp->type_to_index; ; vp++) {
			if (_ox_equal_tid(vp->tid, infp->deep_type_id))
				break;
		}
		index_to_receiver[inf_index] =
		    infp->index_to_receiver[vp->index];
	}
	ASSERT(index_to_receiver[inf_index].num_methods != 0);

	// Method number is zero to num_methods - 1.
	if (index_to_receiver[inf_index].num_methods <= UCHAR_MAX + 1)
		return (1);
	ASSERT(index_to_receiver[inf_index].num_methods <= USHRT_MAX + 1);
	return (2);
}

//
// Return the interface descriptor as a newly allocated inf_descriptor.
// The caller should do a delete when finished with the return value.
//
inf_descriptor *
InterfaceDescriptor::get_inf_descriptor()
{
	// Construct the return value.
	inf_descriptor *resultp = new inf_descriptor;
	resultp->name = namep;
	// Compute size of version table.
	uint_t i, n = 0;
	VersionTable *p = type_to_index;
	for (int number_nulls = 0; ; p++) {
		if (p->tid == NULL) {
			if (++number_nulls >= 2)
				break;
		}
		n++;
	}
	// Allocate sequence to hold n entries.
	resultp->versions.load(n, n, inf_versions_t::allocbuf(n), true);
	// Initialize the version entries.
	for (i = 0, p = type_to_index; i < n; i++, p++) {
		if (p->tid == NULL) {
			bzero(resultp->versions[i].tid, sizeof (inf_typeid));
		} else {
			// Note: assumes uint32_t is same size as uint_t.
			bcopy(p->tid, resultp->versions[i].tid,
			    sizeof (inf_typeid));
		}
		resultp->versions[i].index = p->index;
	}
	return (resultp);
}


//
// Return a reference to the interface version object on the given node.
//
idlversion_ptr
InterfaceDescriptor::get_idlversion(nodeid_t n)
{
	return (get_idlversion_impl_funcp(n));
}

//
// Return the ReceiverTable pointer for the given type ID.
// This is a helper routine for OxSchema::add_inf_descriptor().
//
InterfaceDescriptor::ReceiverTable *
InterfaceDescriptor::get_rtable(CORBA::TypeId tid)
{
	VersionTable *p;

	// Find the interface index number for the requested TID.
	for (p = type_to_index; p->tid != NULL; p++) {
		if (_ox_equal_tid(tid, p->tid)) {
			return (index_to_receiver + p->index);
		}
	}
	return (NULL);
}

//
// This is called by schema_list<T>::add() if a duplicate is found.
// Return true if update is allowed.
//
bool
InterfaceDescriptor::update(InterfaceDescriptor *infp)
{
	//
	// Note that the old entry might either be a "full" entry or
	// a "partial" entry that was obtained from an unknown object.
	// A module load at a later time might then add the same TID
	// so we have to update the old entry to be a "full" entry.
	// The new entry might can also be a "partial" entry if
	// separate threads had to ask the object for its type
	// and the answers received are the same (or should be!).
	//
	// If we are unloading a module, we have to "un-update" any
	// dynamically constructed entries so they no longer point to
	// things in the module.
	// Note that this "un-updated" entry is not the same as the
	// original dynamic entry since index_to_receiver is not
	// reconstructed.
	//
	if (infp == NULL) {
		offsets = NULL;
		index_to_receiver = NULL;
		version_0_tid  = NULL;
		create_proxyp = NULL;
		return (false);
	}
	if (os::strcmp(namep, infp->get_name()) == 0 &&
	    num_interfaces == infp->num_interfaces) {
		if (version_0_tid == NULL && infp->version_0_tid != NULL) {
			// We are updating a "partial" with a "full" entry.
			ASSERT(offsets == NULL);
			ASSERT(create_proxyp == NULL);
			ReceiverTable *rtp = index_to_receiver;

			offsets = infp->offsets;
			index_to_receiver = infp->index_to_receiver;
			version = infp->version;
			version_0_tid  = infp->version_0_tid;
			create_proxyp = infp->create_proxyp;

			delete [] rtp;
		}
		// Check if existing and new entry are both complete entries
		else if ((version_0_tid != NULL) &&
		    (infp->version_0_tid != NULL)) {
			//
			// Adding duplicate entry for the same
			// interface.  Not likely in practice but can
			// happen while testing if modules belonging
			// to different versions are loaded.
			// See bug 4996571.
			//

			//
			// SCMSGS
			// @explanation
			// The system records type identifiers for multiple
			// kinds of type data. The system checks for type
			// identifiers when loading type information. This
			// message identifies that a type is being reloaded
			// most likely as a consequence of reloading an
			// existing module.
			// @user_action
			// Reboot the node and do not load any Sun Cluster
			// modules manually.
			//
			(void) orb_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "clcomm: User Error: Loading duplicate TypeId: %s.",
			    namep);
		}
		return (true);
	}
	//
	// We only end up here in the unlikely event that different
	// interface type strings hashed to the same MD5 TID.
	//
	return (false);
}

//
// schema methods
//

//
// schema -  Return a pointer to the schema object.
//
// The schema object is initialized from static object constructors.
// The order in which static constructors are executed is not controlled.
// In order to ensure that the schema object
// exists, we create the schema object the first time something attempts
// to access the schema object.
//
OxSchema *
OxSchema::schema()
{
	if (schema_p == NULL) {
		schema_p = new OxSchema;
	}

	return (schema_p);
}

//
// constructor - the sizes of the two lists are based on the expected
// number of interfaces (classes) and exceptions.
//
OxSchema::OxSchema() :
	interface_list(512),
	except_list(128)
{
}

OxSchema::~OxSchema()
{
}

//
// load_interfaces - Add list of object type descriptors to the current schema.
//
void
OxSchema::load_interfaces(InterfaceDescriptor **listpp)
{
	interface_list.add_list(listpp);
}

//
// unload_interfaces -
// Remove list of object type descriptors from the current schema.
//
void
OxSchema::unload_interfaces(InterfaceDescriptor **listpp)
{
	interface_list.remove_list(listpp);
}

//
// load_except - Add a list of servers to the schema,
// which effectively registers the receive methods.
//
void
OxSchema::load_excepts(ExceptDescriptor **listpp)
{
	except_list.add_list(listpp);
}

//
// unload_except - Remove a list of servers from the schema.
//
void
OxSchema::unload_excepts(ExceptDescriptor **listpp)
{
	except_list.remove_list(listpp);
}

//
// Look up the given type id and find the corresponding descriptor.
// If the type is not known locally, ask the version object on the
// node that sent us this reference. If all else fails, return NULL.
//
InterfaceDescriptor *
OxSchema::descriptor(const CORBA::TypeId tid, handler *hp)
{
	// Check our local cache for the TID.
	InterfaceDescriptor *resultp = interface_list.find(tid);
	if (resultp != NULL || hp == NULL)
		return (resultp);

	//
	// We don't know about the TID locally.
	// We need to make an invocation to the ORB domain which contains
	// the object reference to get the interface descriptor
	// and cache the answer locally.
	//
	idlversion_var ver_v;
	inf_descriptor *infp = NULL;

	if (handler_kit::is_remote_handler(hp->handler_id())) {
		//
		// If this is a user land process, it may not have type
		// information in its local cache, but the kernel may
		// know about it so query the kernel. If the server
		// object is on another node, query the idl version
		// object on that node.
		//
		nodeid_t n =
		    ((remote_handler *)hp)->get_xdoorp()->get_server_nodeid();

		if (n != NODEID_UNKNOWN) {
			ver_v = InterfaceDescriptor::get_idlversion(n);
		}
	}
	//
	// We create a CORBA::Object proxy for the object reference
	// with the unknown type so we can pass it to the interface
	// version object. The interface version object needs the
	// object reference so user level objects are "translated"
	// appropriately.
	// Note: we create the proxy on the stack and don't adjust the
	// reference counts since we just need it long enough to
	// make an invocation (and a proxy object is small).
	//
	proxy<CORBA::Object_stub> obj(hp, &_Ix__type(CORBA_Object));
	Environment e;

	if (!CORBA::is_nil(ver_v)) {
		// Try getting the InterfaceDescriptor from the version object.
		infp = ver_v->get_dynamic_descriptor(tid, &obj, e);
		if (e.exception()) {
			//
			// We got an exception, set the version object
			// reference to nil so we take the "uncached"
			// code path.
			//
			e.clear();
			ver_v = idlversion::_nil();
			infp = NULL;
		}
	}
	if (CORBA::is_nil(ver_v)) {
		//
		// We were not able to get the information from the version
		// object. Try getting it from the object itself.
		//
		infp = obj._get_dynamic_descriptor(tid, e);
		if (e.exception()) {
			e.clear();
			infp = NULL;
		}
	}
	if (infp == NULL) {
		//
		// The server object is dead so we are not able to
		// determine the deep type. Note that we return
		// NULL which is handled specially in
		// InterfaceDescriptor::create_proxy_obj().
		// We create a partial descriptor here with the
		// deep type ID so that if this object is marshalled,
		// the type is not lost.
		// We don't register the new type so that if this type
		// is seen again, a different server might have the
		// correct information.
		// XXX This is a memory leak.
		// Maybe we should register and update later
		// if possible.
		//
		CORBA::TypeId tidp = new CORBA::MD5_Tid;
		bcopy(tid, tidp, sizeof (CORBA::MD5_Tid)); //lint !e668
		resultp = new InterfaceDescriptor("Unknown type",
		    tidp, NULL, 0, NULL, unknown_inf, NULL, NULL);

		return (resultp);
	}
	resultp = add_inf_descriptor(tid, infp);
	delete infp;
	return (resultp);
}

InterfaceDescriptor *
OxSchema::add_inf_descriptor(const CORBA::TypeId tid, inf_descriptor *infp)
{
	ASSERT(infp->versions.length() != 0);

	static uint32_t zero_tid[4] = { 0, 0, 0, 0 };
	uint_t i;
	uint_t inx = UINT_MAX;
	InterfaceDescriptor::VersionTable *types =
	    new InterfaceDescriptor::VersionTable [infp->versions.length() + 1];
	for (i = 0; i < infp->versions.length(); i++) {
		if (_ox_equal_tid(zero_tid, infp->versions[i].tid)) {
			types[i].tid = NULL;
		} else {
			//
			// Find the index into "types" of the TID that
			// describes the interface we asked for.
			//
			if (_ox_equal_tid(tid, infp->versions[i].tid)) {
				inx = i;
			}
			types[i].tid = new CORBA::MD5_Tid;
			bcopy(infp->versions[i].tid, types[i].tid,
			    sizeof (CORBA::MD5_Tid));
		}
		types[i].index = infp->versions[i].index;
	}
	ASSERT(inx != UINT_MAX);
	// Last entry is always NULL and is not part of infp->versions.
	types[i].tid = NULL;
	types[i].index = 0;

	// Find the highest interface index number which has methods.
	uint_t ninf = 0;
	for (i = 0; types[i].tid != NULL; i++) {
		if (ninf <= types[i].index) {
			ninf = types[i].index + 1;
		}
	}

	//
	// Construct the receiver table for the interfaces we know about
	// so we can check the number of methods.
	//
	InterfaceDescriptor *resultp;
	InterfaceDescriptor::ReceiverTable *rp, *rtablep =
	    new InterfaceDescriptor::ReceiverTable [ninf];
	for (i = 0; i < ninf; i++) {
		// Don't bother looking for the TID we are trying to create.
		if (i != inx &&
		    (resultp = descriptor(types[i].tid, NULL)) != NULL &&
		    (rp = resultp->get_rtable(types[i].tid)) != NULL) {
			rtablep[i] = *rp;
		} else {
			rtablep[i].num_methods = 0;
			rtablep[i].table = NULL;
		}
	}

	char *newname = os::strdup(infp->name);
	resultp = new InterfaceDescriptor(newname,
	    types[inx].tid, NULL, 0, NULL, types, rtablep, NULL);

	if (!add_descriptor(resultp)) {
		//
		// We lost the race and some other thread added
		// the type to the table ahead of us.
		//
		delete resultp;
		delete [] newname;
		delete [] rtablep;
		delete [] types;
		resultp = descriptor(tid, NULL);
		ASSERT(resultp != NULL);
	}
	return (resultp);
}

//
// add_descriptor - Add the interface descriptor to the table.
// Return true if added, false if entry already present.
//
bool
OxSchema::add_descriptor(InterfaceDescriptor *infp)
{
	return (interface_list.add(infp));
}

//
// remove_descriptor - Remove the interface descriptor to the table.
// This is only for testing.
//
void
OxSchema::remove_descriptor(InterfaceDescriptor *infp)
{
	interface_list.remove(infp);
}

//
// except - Look up the given type id and find the corresponding descriptor.
//
ExceptDescriptor *
OxSchema::except(CORBA::TypeId tid)
{
	return (except_list.find(tid));
}

//
// Initialization through static objects.
//

//
// Initialize Object typeids.
//
OxInitSchema::OxInitSchema(InterfaceDescriptor **listpp)
{
	_listpp = listpp;
	OxSchema::schema()->load_interfaces(listpp);
}

OxInitSchema::~OxInitSchema()
{
	OxSchema::schema()->unload_interfaces(_listpp);
} //lint !e1740

//
// Initialize exception data structures.
//
OxInitExcept::OxInitExcept(ExceptDescriptor **listpp)
{
	_listpp = listpp;
	OxSchema::schema()->load_excepts(listpp);
}

OxInitExcept::~OxInitExcept()
{
	OxSchema::schema()->unload_excepts(_listpp);
} //lint !e1740
