/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ADAPTER_IN_H
#define	_ADAPTER_IN_H

#pragma ident	"@(#)adapter_in.h	1.7	08/05/20 SMI"

//
// class impl_spec inline methods
//

template<class T, class H>
inline
impl_spec<T, H>::impl_spec() :
	handler_((CORBA::Object_ptr)this)
{
}

template<class T, class H>
inline T *
impl_spec<T, H>::get_objref()
{
	return ((T *)this);
}

//
// class McServerof inline methods
//

template<class T>
inline T *
McServerof<T>::get_objref()
{
	handler_.new_reference();
	return ((T *)this);
}

//
// Returns true when it is safe to delete the implementation object.
//
template<class T>
inline bool
McServerof<T>::_last_unref(unref_t)
{
	return (handler_.last_unref());
}

//
//  Revoke the object.
//  operation returns after all current invocations have terminated.
//
template<class T>
inline void
McServerof<T>::revoke()
{
	handler_.revoke();
}

//
// class McNoref inline methods
//

template<class T>
inline
McNoref<T>::McNoref() :
	handler_((CORBA::Object_ptr)this)
{
}

#ifdef _KERNEL_ORB
template<class T>
inline
McNoref<T>::McNoref(xdoor_id xdid) :
	handler_(this, xdid)
{
}
#endif

template<class T>
inline T *
McNoref<T>::get_objref()
{
	return ((T *)this);
}

//
//  Revoke the object.
//  operation returns after all current invocations have terminated.
//
template<class T>
inline void
McNoref<T>::revoke()
{
	handler_.revoke();
}

//
// class Anchor inline methods
//

#ifdef _KERNEL_ORB

template<class T>
inline
Anchor<T>::Anchor(nodeid_t nd, xdoor_id xd, InterfaceDescriptor *infp) :
	proxy<T>(new combo_handler_client(nd, xd), infp)
{
}

#else	// _KERNEL_ORB

template<class T>
inline
Anchor<T>::Anchor(int xd, InterfaceDescriptor *infp) :
	proxy<T>(new combo_handler_client(xd), infp)
{
}

#endif	// _KERNEL_ORB

template<class T>
inline
Anchor<T>::~Anchor()
{
}

#endif	/* _ADAPTER_IN_H */
