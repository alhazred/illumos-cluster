//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)proxy.cc	1.7	08/05/20 SMI"

//
// Declares methods for classes supporting proxies in the orb.
//

#include <orb/object/proxy.h>

generic_proxy::generic_proxy(handler *handlerp, InterfaceDescriptor *infp) :
	count_(1),
	hand_(handlerp),
	infdescriptor_(infp)
{
}

generic_proxy::~generic_proxy()
{
	hand_ = NULL;
	infdescriptor_ = NULL;
}

template<class T>
proxy<T>::proxy(handler *handlerp, InterfaceDescriptor *infp) :
	generic_proxy(handlerp, infp)
{
}

template<class T>
proxy<T>::~proxy()
{
}

template<class T>
handler *
proxy<T>::_handler()
{
	return (hand_);
}

//
// Return the dynamic InterfaceDescriptor pointer for this class.
//
template<class T>
InterfaceDescriptor *
proxy<T>::_deep_type()
{
	return (infdescriptor_);
}

//
// Should never call _unreferenced for a proxy.
//
template<class T>
void
proxy<T>::_unreferenced(unref_t)
{
	ASSERT(0);
}

//
// Return a pointer to the C++ proxy object.
//
template<class T>
generic_proxy *
proxy<T>::_this_component_ptr()
{
	return (this);
}
