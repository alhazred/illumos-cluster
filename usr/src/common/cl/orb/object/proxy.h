/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  Defines classes supporting proxies in the orb.
 *
 */

#ifndef _PROXY_H
#define	_PROXY_H

#pragma ident	"@(#)proxy.h	1.9	08/05/20 SMI"

#include <sys/types.h>
#include <sys/os.h>

class handler;
class InterfaceDescriptor;

// typedef used in _unreferenced() and last_unref()
typedef	void	*unref_t;

class generic_proxy {
public:
	//
	// Note that the client handler create function returns
	// an object reference.
	//
	typedef	void *(*ProxyCreator)(handler *, InterfaceDescriptor *);

	void		increase();
	uint_t		decrease();

protected:
	generic_proxy(handler *handlerp, InterfaceDescriptor *infp);

	virtual	 ~generic_proxy();

	uint_t			count_;
	handler			*hand_;
	InterfaceDescriptor	*infdescriptor_;
};

template<class T> class proxy : public T, public generic_proxy {
public:
	proxy(handler *handlerp, InterfaceDescriptor *infp);

	virtual ~proxy();

	// Handler is used for CORBA::Object methods.
	virtual handler		*_handler();

	// Should never call _unreferenced for a proxy.
	virtual void		_unreferenced(unref_t);

	//
	// Return the dynamic InterfaceDescriptor pointer for this proxy.
	// Note that different proxies can have different interface
	// descriptors for the same handler (or server object) and
	// that this descriptor can be either newer or older than the
	// one we were compiled with which _interface_descriptor() returns.
	//
	virtual InterfaceDescriptor	*_deep_type();

	// Set the deep type. This should be used with extreme caution.
	void _deep_type(InterfaceDescriptor *infp);

	// Return a pointer to the C++ proxy object.
	virtual generic_proxy	*_this_component_ptr();
};

#include <orb/object/proxy_in.h>

#endif	/* _PROXY_H */
