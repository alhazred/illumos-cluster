/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ADAPTER_H
#define	_ADAPTER_H

#pragma ident	"@(#)adapter.h	1.44	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/handler/handler.h>
#include <orb/handler/standard_handler.h>
#include <orb/handler/combo_handler.h>

// Server object templates

//
// impl_spec is used whenever a special kind
// of handler (H) is to be associated with a server class
// for interface T.
//
// This should be protected T but a lot of the counter handlers are blowing up
//
template<class T, class H> class impl_spec :
	public knewdel,
	protected T
{
public:
	impl_spec();
	virtual	~impl_spec();

	virtual handler	*_handler();

	T		*get_objref();

	void		_unreferenced(unref_t);

private:
	H		handler_;
};

//
// The McServerof template is used as the first public ancestor class
// for the implementation class of interface T, whenever full global
// reference counting is needed. This is the standard case in which
// most implementations will fit.
//
// Objects are created with an initial reference count of zero.
// The method get_objref provides new references, including the first one.
//
// The system delivers "unreferenced" to any server object, whose count
// has dropped to zero.
//
template<class T> class McServerof :
	public knewdel,
	protected T
{
public:
	McServerof();
	virtual		~McServerof();

	virtual handler	*_handler();

	T		*get_objref();

	//
	// Returns true when it is safe to delete the implementation object.
	//
	bool		_last_unref(unref_t);

	//
	//  Revoke the object.
	//  operation returns after all current invocations have terminated.
	//
	void		revoke();

private:
	standard_handler_server	handler_;
};

//
// Well Known Xdoors
//
#define	XDOOR_LOCAL_NS		1	// Local Name Server
#define	XDOOR_RMM		2	// RMM state exchange Server
#define	XDOOR_MSG_MGR		3	// ORB message sequence number server
#define	XDOOR_VM		4	// Version Manager server
#define	XDOOR_SCHEMA_CACHE	5	// IDL interface to schema cache

//
// The McNoref  template is used as the first public ancestor class for the
// implementation class of interface T, whenever no global reference counting
// is needed.
//
// Clients maintain a reference count to eliminate proxies.
//
// Server objects never get unreferenced calls. The handler is kept as
// part of the server object memory. The server xdoor is immortal.
//
template<class T> class McNoref :
	public knewdel,
	protected T
{
public:
	McNoref();

#ifdef _KERNEL_ORB
	McNoref(xdoor_id xdid);
#endif
	virtual		~McNoref();

	virtual handler	*_handler();

	T *		get_objref();

	void		_unreferenced(unref_t);

	//
	//  Revoke the object.
	//  operation returns after all current invocations have terminated.
	//
	void		revoke();

private:
	combo_handler_server	handler_;
};

// client templates
//
// The anchor template is used to create proxies which "hook" to a specific
// server object residing at a specific node. This template is for locating
// well known objects (e.g. the global name_server) declared with the McNoref
// template.
//
template<class T> class Anchor :
#ifdef _KERNEL_ORB
	public knewdel,
#endif
	public proxy<T>
{
public:

#ifdef _KERNEL_ORB
	Anchor(nodeid_t nd, xdoor_id xd, InterfaceDescriptor *infp);
#else
	Anchor(int xd, InterfaceDescriptor *infp);
#endif

	~Anchor();
};

#include <orb/object/adapter_in.h>

#endif	/* _ADAPTER_H */
