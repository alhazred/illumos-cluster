/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHEMA_H
#define	_SCHEMA_H

#pragma ident	"@(#)schema.h	1.46	06/05/24 SMI"

//
// $XConsortium$
//

//
// Copyright (c) 1993-94 Silicon Graphics, Inc.
// Copyright (c) 1993-94 Fujitsu, Ltd.
//
// Permission to use, copy, modify, distribute, and sell this software and
// its documentation for any purpose is hereby granted without fee, provided
// that (i) the above copyright notices and this permission notice appear in
// all copies of the software and related documentation, and (ii) the names of
// Silicon Graphics and Fujitsu may not be used in any advertising or
// publicity relating to the software without the specific, prior written
// permission of Silicon Graphics and Fujitsu.
//
// THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
// EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
// WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
//
// IN NO EVENT SHALL SILICON GRAPHICS OR FUJITSU BE LIABLE FOR
// ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
// OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
// LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.
//


#include <orb/invo/common.h>
#include <sys/list_def.h>

#define	IDLVERSION_TEMPLATE "idlversion_%d"

#define	_Ix_CreateProxy(Q) _Ix_concat(_, _Ix_concat(Q, _stub_create))

//
// Define the type descriptor for a given type.
//
#define	_Ix__tid(T) _Ix_concat(_, _Ix_concat(T, _tid))
#define	_Ix__type(T) _Ix_concat(_, _Ix_concat(T, _type))
#define	_Ix_extern__tid(Q) extern uint32_t _Ix__tid(Q)[]
#define	_Ix__offsets(T) _Ix_concat(_, _Ix_concat(T, _offsets))
#define	_Ix__excepts(T) _Ix_concat(_, _Ix_concat(T, _excepts))
#define	_Ix_type_index(T) _Ix_concat(_, _Ix_concat(T, _type_index))
#define	_Ix_index_func(T) _Ix_concat(_, _Ix_concat(T, _index_func))

//
// Offsets are used in adjusting object pointers during type
// changes for classes supporting multiple inheritance.
//

#define	_Ix_BeginOffsetList(Q) \
static uint_t _Ix__offsets(Q)[] = {

//
// Crude but effective way to compute the delta between a this pointer and
// the this pointer for a particular parent of the class.
//
#define	_Ix_OffsetEntry(T, Parent) \
(uint_t)((ulong_t)((Parent*)(T*)8) - (ulong_t)((T*)8))

#define	_Ix_EndOffsetList() \
};

#define	_Ix_ExceptDescriptor(D, Name, Q) \
ExceptDescriptor _Ix__type(D)( \
	Name, \
	/* id */ _Ix__tid(D), \
	/* unmarshal */ &Q::_get \
)

//
// Type for pointer to function that unmarshals an exception.
//
//
typedef CORBA::UserException *(*OxTypeObj_UnmarshalException)(service &);

//
// ExceptDescriptor - describes one exception
//
class ExceptDescriptor : public _SList::ListElem {
public:
	ExceptDescriptor(const char *new_namep, CORBA::TypeId tid,
	    OxTypeObj_UnmarshalException func);

	//
	// Return the type id of the exception.
	//
	CORBA::TypeId		get_tid();

	// Returns the function that will unmarshal this user exception.
	OxTypeObj_UnmarshalException	get_unmarshal_func();

	// Return the exception name (for debugging).
	const char		*get_name();

	//
	// This is called by schema_list<T>::add() if a duplicate is found.
	// Return true if update is allowed.
	//
	bool update(ExceptDescriptor *exp);

private:
	// Disallow default constructor
	ExceptDescriptor();

	const char			*namep;		// exception name

	CORBA::TypeId			deep_type_id;

	OxTypeObj_UnmarshalException	unmarshal;
};

// Forward declarations.
struct inf_descriptor;
_Ix_Forward(idlversion)

//
// InterfaceDescriptor - there is one instance of this class for
// CORBA:Object and every class that is a child of CORBA::Object.
// This class defines things that are true for all instances of a class.
//
class InterfaceDescriptor : public _SList::ListElem {
public:
	//
	// Function that processes remote invocations on the server side
	// for this type of object.
	//
	typedef void (*ReceiverFunc)(void *, service &);

	//
	// Table of method receive functions for a given interface.
	//
	struct ReceiverTable {
		uint_t		num_methods;
		ReceiverFunc	*table;
	};

	//
	// This table is used to map versioned CORBA::TypeId values to
	// an index into the index_to_receiver table and offset table
	// (i.e., the interface type index).
	// It is structured into two lists, the first part is the list
	// of TIDs for interfaces with methods, a null entry, then
	// the second part for interfaces without methods and a null entry.
	// If the index is zero, the TID is for this interface (instead
	// of an ancestor).
	// If the index is not zero, the TID is for an ancestor interface
	// and the version is the version of *this* interface when the
	// ancestor was added to this interface.
	//
	struct VersionTable {
		CORBA::TypeId		tid;
		uint_t			index;
	};

	InterfaceDescriptor(
	    const char		*new_namep,
	    CORBA::TypeId	tid,
	    CORBA::TypeId	vers_0_tid,
	    uint_t		vers,
	    uint_t		*offsets_p,
	    VersionTable	*type_list,
	    ReceiverTable	*func_list,
	    generic_proxy::ProxyCreator create_func);

	//
	// Returns true if the target type id is supported by this class.
	//
	bool is_type_supported(CORBA::TypeId target_id);

	// Return true if this type is actually an unknown type.
	bool is_unknown();

	// Return the size of the method table in bytes.
	uint_t sizeof_methods(uint_t index);

	//
	// Look up the object type and return its interface index number.
	// Return -1 if the type is not found.
	// This only searches types which have methods unless
	// 'include_nm' is true.
	//
	int lookup_type_index(CORBA::TypeId target_id, bool include_nm);

	//
	// Look up the receiver function given an interface index and
	// method number.
	//
	ReceiverFunc lookup_rec_func(uint_t index, uint_t method);

	//
	// Widen the given object to the desired interface type.
	//
	void *cast(void *obj, uint_t index);

	//
	// Return the version number for this interface.
	//
	int get_version(CORBA::TypeId target_id);

	//
	// Return an opaque type for the InterfaceDescriptor of the given
	// version.
	//
	CORBA::type_info_t *get_type_info(int v);

	//
	// Return the type id of the interface.
	//
	CORBA::TypeId get_tid();

	//
	// Return the interface name (useful for debugging).
	//
	const char *get_name();

	//
	// Return a new proxy for this interface but with the given deep type.
	//
	CORBA::Object_ptr create_proxy_obj(handler *, InterfaceDescriptor *);

	// Return the interface descriptor as a newly allocated inf_descriptor.
	inf_descriptor *get_inf_descriptor();

	// Return a reference to the idlversion object on the given node.
	static idlversion_ptr get_idlversion(nodeid_t n = NODEID_UNKNOWN);

	//
	// Return the ReceiverTable pointer for the given type ID.
	// This is a helper routine for OxSchema::add_inf_descriptor().
	//
	ReceiverTable *get_rtable(CORBA::TypeId tid);

	//
	// This is called by schema_list<T>::add() if a duplicate is found.
	// Return true if update is allowed.
	//
	bool update(InterfaceDescriptor *infp);

private:
	//
	// Special constants for representing CORBA::Object in the
	// type to index table (see lookup_type_index()).
	//
	enum {
		INF_INDEX_MAX = 65533,
		// 65534 is reserved for future protocol expansion if needed.
		INF_CORBA_OBJECT = 65535
	};

	const char	*namep;		// interface name
	CORBA::TypeId	deep_type_id;	// TID of deep type
	uint_t		*offsets;	// offsets for multiple inheritance
					// indexed by interface index type

	VersionTable	*type_to_index;		// TID to interface index type
	ReceiverTable	*index_to_receiver;	// method receive functions
	uint_t		num_interfaces;		// # of interfaces w/ methods
	uint_t		version;		// version number
	CORBA::TypeId	version_0_tid;		// index into type_to_index

	// Function to create proxy for deep type.
	generic_proxy::ProxyCreator create_proxyp;

	// Disallow default constructor.
	InterfaceDescriptor();

	// Prevent assignments and pass by value.
	InterfaceDescriptor(const InterfaceDescriptor &);
	InterfaceDescriptor &operator = (InterfaceDescriptor &);
};

//
// schema_list - supports different lists used by the schema class.
// Each schema_list is organized as an array of singly linked lists.
// The array size is chosen such that most lists will be of length 1 or 2.
// The individual lists are not sorted.
// The key to each entry is a type id based on MD5.
// Each entry must be a child of class _SList::ListElem.
//
// The system initializes schema lists at startup and expands the lists during
// rolling upgrades. The system never deletes entries.
//
template<class A> class schema_list {
public:
	// Constructor must specify the array size.
	// The size must be a positive number that is a power of two
	schema_list(uint32_t new_size);

	~schema_list();

	// Return an entry with the matching type id.
	A	*find(const CORBA::TypeId tid_target);

	// Add an entry. Return true if added, false if entry already present.
	bool	add(A *new_entry_p);

	// Add multiple entries.
	void	add_list(A **entries_p);

	// Remove an entry.
	void	remove(A *p);

	// Remove multiple entries.
	void	remove_list(A **entries_p);

private:
	// Disallow the default constructor.
	schema_list();

	// Return an entry with the matching type id.
	A	*find_locked(const CORBA::TypeId tid_target);

	// Prevent assignments and pass by value.
	schema_list(const schema_list &);
	schema_list &operator = (schema_list &);

	// Protect the schema_array for insert.
	os::mutex_t	lock;

	// Number of lists in array.
	uint_t		size;

	// Mask for hashing.
	uint32_t	hash_mask;

	// Pointer to the array of lists of schema elements.
	IntrList<A, _SList>	*schema_array;
};

class OxSchema {
public:
	OxSchema();
	~OxSchema();

	static OxSchema		*schema();

	void			load_interfaces(InterfaceDescriptor **);
	void			load_excepts(ExceptDescriptor **);
	void			unload_interfaces(InterfaceDescriptor **);
	void			unload_excepts(ExceptDescriptor **);

	InterfaceDescriptor	*descriptor(const CORBA::TypeId, handler *);
	InterfaceDescriptor	*add_inf_descriptor(const CORBA::TypeId,
				    inf_descriptor *infp);
	bool			add_descriptor(InterfaceDescriptor *);
	void			remove_descriptor(InterfaceDescriptor *);

	ExceptDescriptor	*except(CORBA::TypeId);

private:
	// Prevent assignments and pass by value.
	OxSchema(const OxSchema &);
	OxSchema &operator = (OxSchema &);

	static OxSchema				*schema_p;

	// Note: the size for the schema lists is specified in the constructor
	schema_list<InterfaceDescriptor>	interface_list;
	schema_list<ExceptDescriptor>		except_list;
};

//
// These classes are just for static initializers.
//

class OxInitSchema {
public:
	OxInitSchema(InterfaceDescriptor **listpp);
	~OxInitSchema();

private:
	InterfaceDescriptor **_listpp;
};

class OxInitExcept {
public:
	OxInitExcept(ExceptDescriptor **listpp);
	~OxInitExcept();

private:
	ExceptDescriptor **_listpp;
};

#include <orb/object/schema_in.h>

#endif	/* _SCHEMA_H */
