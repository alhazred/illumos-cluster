//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)adapter.cc	1.8	08/05/20 SMI"

#include <orb/object/adapter.h>

//
// class impl_spec methods
//

template<class T, class H>
impl_spec<T, H>::~impl_spec()
{
}

template<class T, class H>
void
impl_spec<T, H>::_unreferenced(unref_t)
{
}

template<class T, class H>
handler *
impl_spec<T, H>::_handler()
{
	return (&handler_);
}

//
// class McServerof methods
//

template<class T>
McServerof<T>::McServerof() :
	handler_(_this_obj())
{
}

template<class T>
McServerof<T>::~McServerof()
{
}

template<class T>
handler *
McServerof<T>::_handler()
{
	return (&handler_);
}

//
// class McNoref methods
//

template<class T>
McNoref<T>::~McNoref()
{
}

template<class T>
void
McNoref<T>::_unreferenced(unref_t)
{
}

template<class T>
handler *
McNoref<T>::_handler()
{
	return (&handler_);
}
