//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)idlversion_impl.cc	1.5	08/05/20 SMI"

extern "C" const char idlversion_template[] = "idlversion_%d";

#ifdef _KERNEL_ORB

#include <h/version.h>
#include <orb/object/schema.h>
#include <orb/object/idlversion_impl.h>
#include <nslib/ns.h>
#include <sys/cl_comm_support.h>

extern InterfaceDescriptor _Ix__type(CORBA_Object);

idlversion_ptr idlversion_impl::self_p = idlversion::_nil();

idlversion_impl::idlversion_impl()
	:McNoref<idlversion>(XDOOR_SCHEMA_CACHE)
{
}

idlversion_impl::~idlversion_impl()
{
}

int
idlversion_impl::initialize()
{
	ASSERT(self_p == nil);
	idlversion_impl *idlp = new idlversion_impl;
	if (idlp == NULL) {
		return (ENOMEM);
	}
	self_p = idlp->get_objref();
	return (0);
}

//
// Return the interface descriptor (wire schema) for the given type ID.
// The object reference is used to find the node where the object lives.
//
inf_descriptor *
idlversion_impl::get_dynamic_descriptor(const inf_typeid tid,
    CORBA::Object_ptr obj, Environment &)
{
	// Check our local cache for the TID.
	InterfaceDescriptor *resultp =
	    OxSchema::schema()->descriptor((const CORBA::TypeId)tid, NULL);
	if (resultp != NULL) {
		return (resultp->get_inf_descriptor());
	}

	// If we didn't find the TID locally, find out who to ask.
	inf_descriptor *infp;
	Environment e;
	handler *hp = obj->_handler();
	hkit_id_t hid = hp->handler_id();
	if (hid == GATEWAY_HKID) {
		// We have to ask the object itself since its in user land.
		proxy<CORBA::Object_stub> pobj(hp, &_Ix__type(CORBA_Object));
		infp = pobj._get_dynamic_descriptor(tid, e);
		if (e.exception()) {
			e.clear();
			return (NULL);
		}
	} else {
		// We have to ask the kernel where this object resides.
		if (!handler_kit::is_remote_handler(hid)) {
			return (NULL);
		}
		Xdoor *xdp = ((remote_handler *)hp)->get_xdoorp();
		if (xdp == NULL) {
			return (NULL);
		}
		nodeid_t n = xdp->get_server_nodeid();
		if (n == orb_conf::local_nodeid()) {
			// We should know the type locally but didn't find it.
			return (NULL);
		}

		idlversion_var ver_v = InterfaceDescriptor::get_idlversion(n);
		infp = ver_v->get_dynamic_descriptor(tid, obj, e);
		if (e.exception() != NULL) {
			e.clear();
			return (NULL);
		}
	}
	if (infp != NULL) {
		// Add the answer to our local cache.
		(void) OxSchema::schema()->add_inf_descriptor(
		    (const CORBA::TypeId)tid, infp);
	}
	return (infp);
}

//
// register_with_ns
//
// Register idlversion object in global and local nameserver.  It is
// necessary to register in local nameserver also because clients in
// userland lookup the local nameserver for a reference to the the
// idlversion object.
//
//
int
idlversion_impl::register_with_ns()
{
	naming::naming_context_var ctxp = root_nameserver_funcp();
	char name[24]; // "idlversion_NNNNN\0"
	Environment e;

	(void) os::snprintf(name, sizeof (name), idlversion_template,
	    orb_conf::local_nodeid());
	ctxp->rebind(name, self_p, e);
	if (e.exception() != NULL) {
		e.exception()->print_exception(
		    "idlversion_impl: Can't bind "
		    "version object into global nameserver");
		e.clear();
		return (EIO);
	}

	// register in local name server
	ctxp = local_nameserver_funcp((nodeid_t)NULL);
	ctxp->rebind(name, self_p, e);
	if (e.exception() != NULL) {
		e.exception()->print_exception(
		    "idlversion_impl: Can't bind "
		    "version object into local nameserver");
		e.clear();
		return (EIO);
	}
	return (0);
}

#endif // _KERNEL_ORB
