/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clhbsndr.c	1.5	08/05/20 SMI"

/*
 * The heartbeat sender streams module.
 *
 * This module gets pushed over all public and private network adapters
 * directly above the device driver. The sole purpose of this module is
 * to provide a hook for scheduling and sending heartbeats from the
 * network interrupt context. It acts as a pass through for all messages
 * travelling up/down the stream.
 *
 * Under normal circumstances heartbeats get scheduled from the clock
 * interrupt and get sent by real time threads. Under high network interrupt
 * load real time threads fail to run as they are lower in priority to
 * interrupts. Under certain situations even the clock fails to run regularly
 * during high network interrupt load conditions. Hence, the need for this
 * module and the heartbeat hook that it provides through its rput routine.
 *
 * This module gets pushed on public network streams through the autopush
 * mechanism. Relevant entries are added to the /etc/iu.ap file when the
 * package this module belongs to is installed.
 *
 * This module must not depend on any clusteing module as attempt will be
 * to push this module even when the node boots in the non cluster mode
 * since it is added to the autopush configuration in /etc/iu.ap. However,
 * this module must check if the node is booted in the cluster mode and exit
 * in case of non cluster mode boot.
 */

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>

#include <sys/errno.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/strsun.h>

#include <sys/conf.h>
#include <sys/modctl.h>

#include <sys/cladm.h>
#include <sys/cl_net.h>
#include <cplplrt/cplplrt.h>

char _depends_on[] = "misc/cl_runtime";

static int hbsndr_ropen(queue_t *, dev_t *, int, int, cred_t *);
static int hbsndr_rclose(queue_t *, int, cred_t *);
static int hbsndr_rput(queue_t *, mblk_t *);
static int hbsndr_wput(queue_t *, mblk_t *);

static struct module_info hbsndr_info = {
	0x1237,		/* module id - random for now */
	"clhbsndr",	/* module name - hbsndr */
	0,		/* min packet size */
	INFPSZ,		/* max packet size */
	64*1024,	/* hi-water mark */
	16*1024		/* lo-water mark */
};

static struct qinit hbsndr_rint = {
	(int (*)(void))hbsndr_rput,	/* put procedure */
	NULL,				/* service procedure */
	(int (*)(void))hbsndr_ropen,	/* open procedure */
	(int (*)(void))hbsndr_rclose,	/* close procedure */
	NULL,			/* qadmin procedure */
	&hbsndr_info,		/* module info structure */
	NULL,			/* module statistics structure */
	NULL,			/* r/w proc. */
	NULL,			/* info. procedure. */
	0			/* uio type for struio(). */
};

static struct qinit hbsndr_wint = {
	(int (*)(void))hbsndr_wput,	/* put procedure */
	NULL,			/* service procedure */
	NULL,			/* open procedure */
	NULL,			/* close procedure */
	NULL,			/* qadmin procedure */
	&hbsndr_info,		/* module info structure */
	NULL,			/* module statistics structure */
	NULL,			/* r/w proc. */
	NULL,			/* info. procedure. */
	0			/* uio type for struio(). */
};

static struct streamtab hbsndr_stream = {
	&hbsndr_rint,	/* read queue init structure */
	&hbsndr_wint,	/* write queue init structure */
	NULL,		/* mux read init structure */
	NULL		/* mux write init structure */
};

static struct fmodsw hbsndr_fsw = {
	"clhbsndr",
	&hbsndr_stream,
	D_MP,
};

/* Module linkage information for the kernel. */

static struct modlstrmod modl_hbsndr = {
	&mod_strmodops, /* Type of module.  This one is a Streams module */
	"Suncluster network interrupt heartbeat sender",
	&hbsndr_fsw	/* Module info */
};

static struct modlinkage modlinkage = {
	MODREV_1,
	{ (void *)&modl_hbsndr, NULL, NULL, NULL }
};

/*
 * _init()
 */
int
_init(void)
{
	int	error;

	if (!(cluster_bootflags & CLUSTER_BOOTED)) {
		/*
		 * Not booted in cluster mode. This module should not be
		 * in the stream.
		 */
		return (EINVAL);
	}

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();
	return (0);
}

/*
 * _info()
 */
int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

/*
 * _fini()
 */
int
_fini(void)
{
	_cplpl_fini();
	return (mod_remove(&modlinkage));
}

/*
 * Pointer to the send heartbeat function that is set by the cl_comm module
 * when that module is loaded and the function that is used to set the
 * pointer. This approach makes the cl_comm module depend on this module
 * rather than the other way round.
 */
static void (*the_hb_funcp)(void) = NULL;
void
clhbsndr_set_hbfunc(void (*hbf)())
{
	the_hb_funcp = hbf;
}


/*
 * HB sender module's private data - priv_bband is the value that this
 * module will assign to the b_band field of the mblks that pass through
 * it if they do not already have a non zero value. The value is set by
 * sending the CLIOCPRIVBBAND ioctl to this module. This facility is used
 * to assign an appropriate priority to Sun Cluster ORB traffic.
 */
typedef struct hbsndr_data_s {
	uchar_t	priv_bband;
} hbsndr_data_t;

/*
 * hbsndr_open()
 *
 * Read side open procedure
 */
/* ARGSUSED */
static int
hbsndr_ropen(queue_t *rqp, dev_t *dp, int a, int sflag, cred_t *cp)
{
	if (rqp->q_ptr) {
		/* This instance has already been opened.  Nothing to do. */
		return (0);
	}
	if (sflag != MODOPEN) {
		return (ENXIO);
	}
	/* Allocate space for private data */
	rqp->q_ptr = kmem_zalloc(sizeof (hbsndr_data_t), KM_SLEEP);
	((hbsndr_data_t *)rqp->q_ptr)->priv_bband = 0;
	OTHERQ(rqp)->q_ptr = rqp->q_ptr;

	/* Turn our queue on ... */
	qprocson(rqp);
	return (0);
}

/*
 * hbsndr_close()
 *
 * Read side close procedure
 */
/* ARGSUSED */
static int
hbsndr_rclose(queue_t *rqp, int a, cred_t *cp)
{
	/* Disable put and service procedures on this queue */
	qprocsoff(rqp);

	/* Free private data */
	if (rqp->q_ptr) {
		kmem_free(rqp->q_ptr, sizeof (hbsndr_data_t));
		rqp->q_ptr = NULL;
		OTHERQ(rqp)->q_ptr = NULL;
	}

	return (0);
}

/*
 * hbsndr_rput()
 *
 * Read side put procedure. Heartbeat send calls are made before
 * forwarding the message to the next module.
 */
static int
hbsndr_rput(queue_t *rqp, mblk_t *mp)
{
	/*
	 * First make a call into the heartbeat subsystem to check if any
	 * heartbeats need to be sent.
	 * Data might start arriving on the public network even before the
	 * heartbeat threadpool has been initialized. Hence, need to check
	 * if the heartbeat threadpool exists before making the call.
	 */
	if (the_hb_funcp != NULL) {
		(*the_hb_funcp)();
	}

	/* Next, forward the message upstream. */
	putnext(rqp, mp);

	return (0);
}

/*
 * Called by hbsndr_wput to process ioctls. Only the CLIOCPRIVBBAND
 * ioctl is processed by this module (return value 0) - rest are passed
 * on to next module (return value 1).
 */
static int
hbsndr_ioctl(queue_t *wqp, mblk_t *mp)
{
	struct iocblk   *iocp;
	bband_info_t	*bbip;

	iocp = (struct iocblk *)mp->b_rptr;
	if (iocp->ioc_cmd == CLIOCSPRIVBBAND) {
		if (mp->b_cont == NULL ||
		    MBLKL(mp->b_cont) != sizeof (bband_info_t)) {
			ASSERT(0);
			miocnak(wqp, mp, 0, EINVAL);
			return (0);
		}
		bbip = (bband_info_t *)mp->b_cont->b_rptr;
		((hbsndr_data_t *)wqp->q_ptr)->priv_bband = bbip->bband;
		miocack(wqp, mp, 0, 0);
		return (0);
	} else {
		return (1);
	}
}

/*
 * Write side put procedure. Updates b_band field of M_DATA mblks passing
 * through the module to the value sttored in the private data of this module.
 * Traps ioctls meant for this module. Acts as a pass through otherwise.
 */
static int
hbsndr_wput(queue_t *wqp, mblk_t *mp)
{
	unsigned char dbtype;

	dbtype = DB_TYPE(mp);
	if (dbtype == M_DATA) {
		/*
		 * Update the b_band field unless upper layer (IP or Sun
		 * Cluster?) has already set it to have a priority. The b_band
		 * value we set has been communicated to us by Sun Cluster and
		 * is the default value for the ORB traffic.
		 */
		if (mp->b_band == 0) {
			mp->b_band = ((hbsndr_data_t *)wqp->q_ptr)->priv_bband;
		}
	} else if (dbtype == M_IOCTL) {
		if (!hbsndr_ioctl(wqp, mp)) {
			return (0);
		}
	}
	putnext(wqp, mp);
	return (0);
}
