//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)newdel.cc	1.29	08/05/20 SMI"

#include <sys/types.h>
#include <sys/kmem.h>
#include <sys/os.h>

void *
shared_new(size_t len, os::mem_alloc_type flag, os::mem_zero_type zflag)
{
	uint64_t *p;

	// Debug check to verify that we are not doing blocking kmem allocations
	// in nonblocking contexts
	// XX We would like to say NB_ALL here to also catch cases where
	// we do blocking memory allocations in _unreferenced. However,
	// due to bugid 4247115 and others, we cannot yet.
#ifdef	_KERNEL
	os::envchk::check_nonblocking(
	    (flag == os::SLEEP) ? os::envchk::NB_INVO | os::envchk::NB_INTR : 0,
	    "blocking kmem_alloc");
#endif

	len += sizeof (uint64_t);

	//
	// Add extra padding for 64bits alignment on ia32 system. For vectors,
	// the compiler asks for some additional space for storing the counts.
	// The additional space requested by compiler is 32bits on ia32 which
	// will break the 64bits alignment. We will allocate extra space here
	// and use it in vector_new() to realign it to 64bits. See bug 4939179
	// for more information.
	//
	len += sizeof (uint64_t);

	if (zflag == os::ZERO) {
#ifdef	_KERNEL
		p = (uint64_t *)kmem_zalloc(len, (int)flag);
#else
		p = (uint64_t *)memalign(64, len);
		bzero((void *)p, len);
#endif
	} else {
#ifdef	_KERNEL
		p = (uint64_t *)kmem_alloc(len, (int)flag);
#else
		p = (uint64_t *)memalign(64, len);
#endif
	}

	if (p == NULL) {
		return (NULL);
	}
	*p = len;

	// Check for 64bits alignment.
	ASSERT(((uintptr_t)&p[1] & 0x7) == 0);

	return ((void *)(&p[1]));
}

// XXX Could not inline in sys/os.h as declared extern in new.h by compiler
void *operator
new(galsize_t len)
{
	return (shared_new((size_t)len, os::SLEEP, os::DONTZERO));
}

void operator
delete(void *ptr)
{
	if (ptr != NULL) {
		uint64_t *p = (uint64_t *)ptr;
		p--;
		ASSERT(*p <= (uint64_t)-1);
		ASSERT(*p != 0);
#ifdef	_KERNEL
		kmem_free(p, (size_t)*p);
#else
		free(p);
#endif
	}
}
