/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
/*      Copyright (c) 1989 AT&T                                 */
/*        All Rights Reserved                                   */
 
        .ident  "@(#)crtn.s 1.6	08/05/20 SMI"
        .file   "crtn.s"

/ This file is created for ProCompilers(x86) Compilation System

/ This code provides the end to the _init and _fini functions which are 
/ used as C++ static constructors and destructors.  This file is
/ included by cc as the last component of the ld command line

        .section       .init
#ifdef	__amd64
	popq	%rbx
#else
       	popl	%ebx
#endif
	ret	$0

        .section       .fini
#ifdef	__amd64
	popq	%rbx
#else
        popl	%ebx
#endif
	ret	$0
