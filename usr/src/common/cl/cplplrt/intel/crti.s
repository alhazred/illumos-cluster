/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
/*      Copyright (c) 1989 AT&T                                 */
/*        All Rights Reserved                                   */
 
        .ident  "@(#)crti.s 1.6	08/05/20 SMI"
        .file   "crti.s"

/ This file is created for ProCompilers(x86) Compilation System

/ stubs for C++ static constructor and destructor initialization
/ routines
/ This file is included by cc/ld before all application and library
/ objects.  If ld is building an executable, this file comes after
/ crt1.o; if ld is building a shared library, it is the first object
/ passed to ld by cc

/ e.g, a.out: ld /lib/crt1.o /lib/crti.o a.o b.o -lc -lsys /lib/crtn.o
/ shared lib: ld -G /lib/crti.o a.o b.o /lib/crtn.o

        .globl  _cplpl_init
        .globl  _cplpl_fini

        .section .init
        .align  4
	.globl	_cplpl_init
	.type   _cplpl_init,@function
_cplpl_init:
#ifdef	__amd64
	pushq	%rbx
#else
	pushl	%ebx
#endif

        .section .fini
        .align  4
	.globl	_cplpl_fini
	.type	_cplpl_fini,@function
_cplpl_fini:
#ifdef	__amd64
	pushq	%rbx
#else
	pushl	%ebx
#endif

