//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)vectutils.cc	1.14	08/05/20 SMI"

//
// These are called internally by the compiler to construct/destroy
// arrays (aka: vectors) of classes, for objects NOT allocated on the heap.
//
// This implements our C++ runtime support in the kernel as we cannot
// link in the kernel with libCrun which supplies these vector methods
// for userland applications.
//

#include <sys/os.h>
#include <sys/cmn_err.h>

typedef void (*destf)(void *); // destructor function type
typedef void (*consf)(void *); // constructor function type
#if defined(_LP64)
typedef ulong_t cr_size_t;
#else
typedef uint_t cr_size_t;
#endif

void *operator
new[](galsize_t len)
{
	return (operator new(len));
}

void operator
delete[](void *ptr)
{
	operator delete(ptr);
}

namespace __Crun {

	void *vector_new(void *addr, cr_size_t size, cr_size_t count,
	    consf cons, destf) {
		if (addr == NULL)
			return (NULL);

		*((uint64_t *)addr) = count;		// save the count
		uint64_t *res = &((uint64_t *)addr)[1]; // start of data

		// Check for 64bits alignment.
		ASSERT(((uintptr_t)res & 0x7) == 0);

		char *p = (char *)res;	// char* so we can do arithmetic
		char *ep = p + size * count;
		if (cons != NULL) {
			while (p < ep) {
				cons((void *)p);
				p += size;
			}
		}

		return ((void *)res);
	}

	void *vector_del(void *addr, cr_size_t size, destf dest) {
		if (addr == NULL)
			return (addr);

		uint64_t *res = &((uint64_t *)addr)[-1]; // original location
		cr_size_t t_size = (cr_size_t)*res * size;	// total size

		char *p = (char *)addr + t_size;	// just past the end

		if (dest != NULL) {
			while (p > (char *)addr) {	// destroy each entry
				p -= size;
				dest((void *)p);
			}
		}

		return ((void *)res);			// adjusted value
	}

	void vector_con(void *ptr, cr_size_t size, cr_size_t count,
	    void(* cons)(void *), void(*)(void *)) {
		ASSERT(ptr);
		ASSERT(cons);
		if (ptr != 0) {	// in case called for a null pointer
		    for (char *p = (char *)ptr;  count-- > 0;  p += size) {
			cons(p);
		    }
		}
	}

	void vector_des(void *ptr, cr_size_t size, cr_size_t count,
	    void(* dest)(void *)) {
		ASSERT(ptr);
		ASSERT(dest);
		if (ptr != 0) {	// in case called for a null pointer
			for (char *p = (char *)ptr + ((cr_size_t)count - 1) *
			    size; count-- > 0;  p -= size) {
				dest(p);
			}
		}
	}

	extern "C" typedef void(*cfptr)(void);

	void register_exit_code(cfptr)
	{
		return;
	}

	void pure_error()
	{
		ASSERT((0, "pure virtual function called"));	//lint !e505
#ifdef	_KERNEL
		cmn_err(CE_PANIC, "pure virtual function called");
#endif
	}
}
