/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SOLOBJ_IMPL_H
#define	_SOLOBJ_IMPL_H

#pragma ident	"@(#)solobj_impl.h	1.17	09/01/20 SMI"

#include <sys/sol_version.h>
#include <sys/cred.h>
//
// PSARC 2002/188 (Least Privilege). This inclusion is needed until we
// get a "contract private" contract with ON for a true marshal/unmarshal
// interface to Solaris from Sun Cluster.
//
#if (SOL_VERSION >= __s10)
#include <sys/cred_impl.h>
#endif

#include <solobj/solobj_handler.h>
#include <h/solobj.h>
#include <orb/object/adapter.h>

class solobj_impl : private solobj {		// use solobj namespace
public:
	// object used to pass user credentials (cred_t structure)
	class cred : public impl_spec<solobj::cred, Solobj_handler_cred> {
	public:
		cred(cred_t *credp);
		cred(service &b);
		~cred();
		void _unreferenced(unref_t);
		void marshal(service &b);
		cred_t *get_cred();

		static CORBA::Object_ptr unmarshal(service &b);
		static void unmarshal_cancel(service &b);

		//
		// These methods are specific to Solaris 10 because the
		// cred_t structure changed from S9.
		//
#if (SOL_VERSION >= __s10)
		void unmarshal_groups(service &linear_byte_seq,
		    uint_t ngroups);
		void unmarshal_privileges(service &linear_byte_seq);
		void marshal_groups(service &linear_byte_seq, uint_t ngroups);
		void marshal_privileges(service &linear_byte_sequence);
#endif

	private:
		cred_t	*_credp;
		//
		// If true, tells the destructor to free the Solaris
		// cred_t structure. If false, do nothing.
		//
		bool	_need_crfree;
		//
		// For Solaris 10, we can no longer point into the groups
		// structure in cred_t and populate it when unmarshalling.
		//
#if (SOL_VERSION >= __s10)
		gid_t	groups[NGROUPS_UMAX];
#endif
	};

	// Create an interface object for a Solaris cred_t structure
	static solobj::cred_ptr conv(cred_t *credp);

	// Return a Solaris cred_t structure for an interface object
	static cred_t *conv(solobj::cred_ptr credobj);

	// Return an object encapsulating kernel credentials
	static solobj::cred_ptr kcred();

private:
	// Object encapsulating kernel credentials
	static cred _kcred;
};

#include <solobj/solobj_impl_in.h>
#include <solobj/solobj_impl_cred_in.h>

#endif	/* _SOLOBJ_IMPL_H */
