//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)solobj_impl_cred.cc	1.28	08/05/20 SMI"

#include <solobj/solobj_impl.h>
#include <orb/infrastructure/orb.h>

#ifdef DEBUG
struct {
	uint_t total;
	uint_t fast;
	uint_t groups;
	uint_t ruid;
	uint_t rgid;
	uint_t suid;
	uint_t sgid;
} n_cred_marshals = { 0, 0, 0, 0, 0, 0, 0 };
#define	CRED_COUNTER_UPDATE(p, n)	os::atomic_add_32((p), (n))
#else
#define	CRED_COUNTER_UPDATE(p, n)
#endif

#if (SOL_VERSION >= __s10)
//
// This is really a four-element array of structs, each of which is a
// two-element array. Thus, when we linearize the structure during
// marshalling, there are 8 values, which are longs. This macro will
// most likely go away when we replace it with the marshal/unmarshal
// interface.
//
#define	ARRAY_OF_PRIV_SIZE 8
#endif

//
// **********************************************************************
// 				UNMARSHALLING CODE
// **********************************************************************
//
//
// We decided to use two different implementations of this constructor:
// one for S9 and one for S10. The S10 was sufficiently different that
// just using ifdefs all over the place would make the code look ugly
// and unreadable. The same rationale applies to the other methods
// in this file.
//
#if (SOL_VERSION >= __s10)
//
// Effects: The purpose of this constructor is to create a C++ object
//   that represents the credentials structure contained in the over-the-
//   wire linear sequence of bytes "b." It is called by the
//   "solobj_impl::cred::unmarshal()" method to recreate the object
//   on the callee's side of an IDL invocation.
// Parameters
//   * b (IN):  linear byte sequence used by the ORB.
//
solobj_impl::cred::cred(service &b)
{
	//
	// Create an initialized credentials structure, assign it to the
	// private data member "_credp, and the local variable "crp."
	// In this constructor, we modify the fields as we unmarshal
	// the solobj_impl::cred IDL object.
	//
	cred_t *crp = _credp = crget();
	_need_crfree = true;	// needed by the destructor to free or not

	// Unmarshal the fields of the IDL linear byte sequence one by one.
	uid_t euid = b.get_long(); 	// extract effective uid
	gid_t egid = b.get_long();	// extract effective gid
	uid_t ruid = b.get_long(); 	// extract real uid
	gid_t rgid = b.get_long();	// extract real gid
	uid_t suid = b.get_long(); 	// extract saved uid
	gid_t sgid = b.get_long();	// extract saved gid

	//
	// We need not unmarshal the reference count. It is just an
	// implementation detail. It should be set to 1 already.
	// The reference count is irrelevant for the PxFS server side
	// since it just passes it to the underlying file system. The
	// only entity that could care about the cred_t is the client.
	//

	//
	// Now set the fields of the cred_t structure "crp."
	//
	// Set the user ids.
	int result = crsetresuid(crp, ruid, euid, suid);
	if (result == -1) {
		//
		// UIDs are badly formed. Raise a CORBA system exception.
		// COMPLETED_NO means that the server was unable to
		// complete processing of the argument due to problems.
		//
		b.get_env()->system_exception(CORBA::BAD_PARAM(0,
			CORBA::COMPLETED_NO));
	}
	// Set the group ids.
	int result1 = crsetresgid(crp, rgid, egid, sgid);
	if (result1 == -1) {
		// GIDs are badly formed. Raise a CORBA system exception.
		b.get_env()->system_exception(CORBA::BAD_PARAM(0,
			CORBA::COMPLETED_NO));
	}

	//
	// cr_ngroups is the number of elements currently in use, not
	// the size of the cr_groups array.
	//
	uint_t ngroups = b.get_short();	// extract # of groups

	//
	// INVARIANT 1: the actual number of groups contained in a solobj_impl
	// object is always less than or equal to "ngroups_max."
	// INVARIANT 2: The actual number of groups is never less than 0.
	//
	ASSERT(ngroups <= ngroups_max && ngroups >= 0);

	// Unmarshal the privileges
	unmarshal_privileges(b);

	// Unmarshal the array elements of cr_groups.
	unmarshal_groups(b, ngroups);
}
#else
solobj_impl::cred::cred(service &b)
{
	// XXX See RFE to avoid this on each call
	cred_t *crp = _credp = crget();
	_need_crfree = true;

	int ngroups = b.get_short();
	crp->cr_uid = b.get_long();
	crp->cr_gid = b.get_long();
	if (ngroups == -1) {
		// Common case
		crp->cr_ruid = crp->cr_uid;
		crp->cr_rgid = crp->cr_gid;
		crp->cr_suid = crp->cr_uid;
		crp->cr_sgid = crp->cr_gid;
		crp->cr_ngroups = 0;
		return;
	}

	// Uncommon case
	crp->cr_ruid = b.get_long();
	crp->cr_rgid = b.get_long();
	crp->cr_suid = b.get_long();
	crp->cr_sgid = b.get_long();

	//
	// XXX Can this be an ASSERT? This seems to be catching the case
	// where different nodes can have different ngroups_max
	//
	if (ngroups > ngroups_max) {
		b.get_env()->system_exception(CORBA::BAD_PARAM(0,
			CORBA::COMPLETED_NO));
		while (ngroups-- > ngroups_max) {
			(void) b.get_long(); // discard some received groups
		}
	}
	crp->cr_ngroups = (unsigned int)ngroups;
	for (int i = 0; i < ngroups; i++) {
		crp->cr_groups[i] = b.get_long();
	}
}
#endif

CORBA::Object_ptr
solobj_impl::cred::unmarshal(service &b)
{
	return ((new solobj_impl::cred(b))->get_objref());
}

#if (SOL_VERSION >= __s10)
void
solobj_impl::cred::unmarshal_cancel(service &b)
{
	// Do the same as unmarshal but just ignore the returned data
	(void) b.get_long(); 	// extract effective uid
	(void) b.get_long();	// extract effective gid
	(void) b.get_long(); 	// extract real uid
	(void) b.get_long();	// extract real gid
	(void) b.get_long(); 	// extract saved uid
	(void) b.get_long();	// extract saved gid
	int ngroups = b.get_short();	// extract ngroups

	// Extract the crpriv.cpriv_flags field
	(void) b.get_unsigned_int();
	// Extract the privileges but ignore the values.
	for (int i = 0; i < ARRAY_OF_PRIV_SIZE; i++) {
		(void) b.get_long();
	}

	// Iterate over the groups, getting the values but ignoring them.
	if (ngroups == 0) {
		return;
	}
	while (ngroups--) {
		(void) b.get_long();
	}
}
#else
void
solobj_impl::cred::unmarshal_cancel(service &b)
{
	// Do the same as unmarshal but just neglect the returned data
	int ngroups = b.get_short();
	(void) b.get_long();		// uid
	(void) b.get_long();		// gid
	if (ngroups == -1) {
		return;
	}

	(void) b.get_long();		// ruid
	(void) b.get_long();		// rgid
	(void) b.get_long();		// suid
	(void) b.get_long();		// sgid

	while (ngroups--) {
		(void) b.get_long();
	}
}
#endif

void
solobj_impl::cred::_unreferenced(unref_t)
{
	delete this;
}

#if (SOL_VERSION >= __s10)
//
// Requires:	"lbs" linear byte sequence be writable. "ngroups" value
//		is between 0 and ngroups_max-1 inclusive.
// Effects:	This routine extracts group data from "lbs" and reconstructs
//		the group array in the credentials object.
// Parameters:
//   lbs (IN):	   linear byte sequence used by the ORB.
//   ngroups (IN): number of other groups the user belongs to; legal
//		   range of values is 0 to ngroups_max-1.
//
void
solobj_impl::cred::unmarshal_groups(service &linear_byte_seq, uint_t ngroups)
{
	cred_t *crp = _credp;

	if (ngroups == 0) {
		return; // Nothing to do, so return.
	}

	//
	// Unmarshal the array elements of cr_groups.
	// The cr_groups array is used to hold the gids of the groups
	// to which a user holding this credential belongs. Recall that
	// a user can belong to multiple groups. It's not common, but
	// it can happen.
	//
	// Iterate over (ngroups) groups and assign each to an
	// array element. Note that the array may actually have more
	// elements than ngroups; we just iterate for the actual number
	// used. Also note that "groups" array is allocated for the C++
	// object at constructor time for the cred_t. We could do this
	// dynamically, but that's costly. The reason we have to do this
	// at all is that we can only set the groups via an accessor
	// function where the argument is the groups array. We can't
	// iterate over the linear byte sequence and set each group
	// individually. So we have to create the entire group array,
	// extracting each gid value and assigning it to an array element.
	//
	for (int i = 0; i < ngroups; i++) {
		groups[i] = linear_byte_seq.get_long();
	}

	//
	// Set the groups in the credentials structure. This has the
	// effect of copying "ngroups" elements of "groups" to "crp."
	//
	ASSERT(ngroups <= ngroups_max && ngroups >=0);
	int result = crsetgroups(crp, ngroups, groups);
	if (result == -1) {	// ngroups > ngroups_max OR ngroups < 0
		//
		// This should never happen because the marshalling code
		// ensures that "ngroups <= ngroups_max and ngroups >= 0"
		//
		ASSERT(0);
	}
}

//
// Requires: "lbs" linear byte sequence contains elements that can
//    be extracted using "get_*" operations.
// Effects: This method unmarshals just the privileges in the credentials
//   object from "lbs." Returns nothing.
// Paramters:
//   * lbs (IN): linear sequence of bytes used by the ORB.
//
void
solobj_impl::cred::unmarshal_privileges(service &lbs)
{
	cred_t *crp = _credp;

	// Extract the crpriv.cpriv_flags
	crp->cr_priv.crpriv_flags = lbs.get_unsigned_int();

	//
	// Extract the crprivs array elements. The array is currently
	// of size PRIV_NSET (4).
	//
	// Reconstruct the crprivs array.
	// Extract cprivs[0].pbits[0]
	crp->cr_priv.crprivs[PRIV_EFFECTIVE].pbits[0] = lbs.get_unsigned_long();
	// Extract cprivs[0].pbits[1]
	crp->cr_priv.crprivs[PRIV_EFFECTIVE].pbits[1] = lbs.get_unsigned_long();
	// Extract cprivs[1].pbits[0]
	crp->cr_priv.crprivs[PRIV_INHERITABLE].pbits[0] =
	    lbs.get_unsigned_long();
	// Extract cprivs[1].pbits[1]
	crp->cr_priv.crprivs[PRIV_INHERITABLE].pbits[1] =
	    lbs.get_unsigned_long();
	// Extract cprivs[2].pbits[0]
	crp->cr_priv.crprivs[PRIV_PERMITTED].pbits[0] = lbs.get_unsigned_long();
	// Extract cprivs[2].pbits[1]
	crp->cr_priv.crprivs[PRIV_PERMITTED].pbits[1] = lbs.get_unsigned_long();
	// Extract cprivs[3].pbits[0]
	crp->cr_priv.crprivs[PRIV_LIMIT].pbits[0] = lbs.get_unsigned_long();
	// Extract cprivs[3].pbits[1]
	crp->cr_priv.crprivs[PRIV_LIMIT].pbits[1] = lbs.get_unsigned_long();

	//
	// NOTE: The problem with this implementation is that it uses
	// private constants defined in priv_impl.h, which in general
	// should not be accessible to clients. Clearly, if the
	// implementation changed, this code would break. What we really
	// need here are Solaris methods that allow us to reconstruct
	// the privileges from the linear byte sequence.
	//
	// However, Sun Cluster will pursue a contract "contract private"
	// with ON for a marshal unmarshal interface.
	//
}

//
// **********************************************************************
// 				MARSHALLING CODE
// **********************************************************************
//
//
// Requires: "lbs" linear byte sequence be writable. "ngroups" value
//		is between 0 and ngroups_max-1 inclusive.
// Effects:	This routine linearizes "ngroups" number of groups in the
//		group array and writes it to "lbs." If "ngroups" is zero,
//		there's nothing to linearize.
// Parameters:
//   lbs (IN):	   linear byte sequence used by the ORB.
//   ngroups (IN): number of other groups the user belongs to; legal
//		   range of values is 0-ngroups_max-1.
//
void
solobj_impl::cred::marshal_groups(service &lbs, uint_t ngroups)
{
	cred_t *crp = _credp;

	const gid_t *crgroups = crgetgroups(crp);

	//
	// For each group, starting at index 0, write it in order to the
	// linear byte sequence "lbs." If "ngroups" is 0, there's
	// nothing to marshal; this case is handled in the for loop.
	//

	//
	// That's how cred works - it allocates an array of ngroups length
	// although declares an array of length 1. So have to suppress this
	// warning from lint.
	//
	for (unsigned int i = 0; i < ngroups; i++) {
		lbs.put_long(crgroups[i]); //lint !e661 !e662
	} // end for

}

//
// Internally, the cred_priv_t structure looks pictorially like the
// following:
// cred_priv_t
//	crpriv_flags		uint_t
//	crprivs[PRIV_NSET]	priv_set_t
//		0	1		2	3
//	+-----------+-----------+-----------+-----------+
//	|pbits	    |pbits	|pbits	    |pbits	|
//	|   0   1   |   0   1   |   0   1   |   0   1   |
//	| +---+---+ | +---+---+ | +---+---+ | +---+---+ |
//	| |u32|u32| | |u32|u32| | |u32|u32| | |u32|u32| |
//	| +---+---+ | +---+---+ | +---+---+ | +---+---+ |
//	+-----------+-----------+-----------+-----------+
//
// Linear byte sequence:
//
// Effects: This method marshals just the privileges in the credentials
//   object into "lbs," the linear byte sequence. Returns nothing.
// Paramters:
//   * lbs (IN): linear sequence of bytes used by the ORB.
//
void
solobj_impl::cred::marshal_privileges(service &lbs)
{
	cred_t *crp = _credp;

	// Marshal the crpriv.cpriv_flags
	lbs.put_unsigned_int(crp->cr_priv.crpriv_flags);

	//
	// Marshal the crprivs array elements. The array is currently
	// of size PRIV_NSET (4). There are four sets; each set is
	// a struct of a two-element array of uint32_t values.
	// We don't interpret these values; we just need to know the
	// values of the base types so they can be marshalled as primitive
	// values.
	//
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_EFFECTIVE].pbits[0]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_EFFECTIVE].pbits[1]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_INHERITABLE].pbits[0]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_INHERITABLE].pbits[1]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_PERMITTED].pbits[0]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_PERMITTED].pbits[1]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_LIMIT].pbits[0]);
	lbs.put_unsigned_long(crp->cr_priv.crprivs[PRIV_LIMIT].pbits[1]);

	//
	// NOTE: The problem with this implementation is that it uses
	// private constants defined in priv_impl.h, which in general
	// should not be accessible to clients. Clearly, if the
	// implementation changed, this code would break. What we really
	// need here are Solaris methods that allow us to reconstruct
	// the privileges from the linear byte sequence.
	//
	// However, Sun Cluster will pursue a contract "contract private"
	// with ON for a marshal unmarshal interface. This interface
	// is the right way to insulate Sun Cluster from future changes
	// in Solaris.
	//
}

//
// Requires: _credp must already an allocated cred_t struct.
// Effects: The purpose of this routine is to linearize the cred_t
//   credentials structure that represents the solobj_impl IDL object.
//   That is, we take apart the cred_t structure, field by field, and
//   construct a linear sequence of bytes. The linearization is
//   represented by the service argument "b."  Returns nothing.
// Assumptions:
//   We assume that the marshalling of the solobj object in and IDL
//   invocation only happens at a PxFS client; a solobj never originates
//   at the PxFS server.
//
// Parameters
//   * b (IN): linear sequence of bytes used by the ORB
//
void
solobj_impl::cred::marshal(service &b)
{
	cred_t *crp = _credp;
	ASSERT(crp != NULL);

	//
	// Note that we need not marshal the reference count. The value
	// is meaningless to the PxFS server, but meaningful to the client
	// and is purely an implementation detail.
	//
	b.put_long(crgetuid(crp));  	// marshal effective uid
	b.put_long(crgetgid(crp));	// marshal effective gid
	b.put_long(crgetruid(crp));	// marshal real uid
	b.put_long(crgetrgid(crp));	// marshal real gid
	b.put_long(crgetsuid(crp));	// marshal saved uid
	b.put_long(crgetsgid(crp));	// marshal saved gid

	// Extract the number of groups actually used.
	uint_t ngroups = crgetngroups(crp);
	//
	// It is not possible for ngroups > ngroups_max because the
	// cred_t structure being marshalled for an IDL invocation
	// is on the same node where the cred_t was created at the PxFS
	// client. Perhaps they'll be different if either is somehow
	// corrupted. Let us check anyway and raise an exception on
	// the PxFS client side. If they are different, it is better
	// to catch it here on the client side, rather than sending it
	// across the wire to the server.
	// Note: This assumption is false when rolling upgrades from
	// Solaris 9 to Solaris 10 are taken into account.
	//
	if (ngroups > ngroups_max) {
		b.get_env()->system_exception(CORBA::BAD_PARAM(0,
			CORBA::COMPLETED_NO));
	}

	// Marshal the number of groups in the groups array
	b.put_short((short)ngroups);

	// Extract the privileges and marshal them
	marshal_privileges(b);

	// Extract the groups and marshal them
	marshal_groups(b, ngroups);
}
#else
void
solobj_impl::cred::marshal(service &b)
{
	cred_t *crp = _credp;

	CRED_COUNTER_UPDATE(&n_cred_marshals.total, 1);
	unsigned int ngroups = crp->cr_ngroups;
	if ((ngroups == 0) &&
	    (crp->cr_uid == crp->cr_ruid) && (crp->cr_uid == crp->cr_suid) &&
	    (crp->cr_gid == crp->cr_rgid) && (crp->cr_gid == crp->cr_sgid)) {
		b.put_short(-1);
		b.put_long(crp->cr_uid);
		b.put_long(crp->cr_gid);
		CRED_COUNTER_UPDATE(&n_cred_marshals.fast, 1);
		return;
	}
	ASSERT(ngroups <= INT16_MAX);
	b.put_short((short)ngroups);
	b.put_long(crp->cr_uid);
	b.put_long(crp->cr_gid);
	b.put_long(crp->cr_ruid);
	b.put_long(crp->cr_rgid);
	b.put_long(crp->cr_suid);
	b.put_long(crp->cr_sgid);
	//
	// thats how cred works - it allocates an array of ngroups length
	// although declares an array of length 1. So have to suppress this
	// warning from lint.
	//
	for (unsigned int i = 0; i < ngroups; i++) {
		b.put_long(crp->cr_groups[i]); //lint !e661 !e662
	}

	CRED_COUNTER_UPDATE(&n_cred_marshals.groups, ((ngroups > 0) ? 1 : 0));
	CRED_COUNTER_UPDATE(&n_cred_marshals.ruid,
		((crp->cr_ruid != crp->cr_uid) ? 1 : 0));
	CRED_COUNTER_UPDATE(&n_cred_marshals.rgid,
		((crp->cr_rgid > crp->cr_gid) ? 1 : 0));
	CRED_COUNTER_UPDATE(&n_cred_marshals.suid,
		((crp->cr_suid > crp->cr_uid) ? 1 : 0));
	CRED_COUNTER_UPDATE(&n_cred_marshals.sgid,
		((crp->cr_sgid > crp->cr_gid) ? 1 : 0));
}
#endif
