//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)solobj_handler.cc	1.24	08/05/20 SMI"

#include <solobj/solobj_impl.h>

// Do not confuse our CRED with that in /usr/include/sys/cred.h

#ifdef CRED
#undef CRED
#endif

Solobj_handler_kit	*Solobj_handler_kit::the_Solobj_handler_kit = NULL;

// static object encapsulating kernel credentials
solobj_impl::cred solobj_impl::_kcred(::kcred);

Solobj_handler::~Solobj_handler()
{
}

hkit_id_t
Solobj_handler::handler_id()
{
	return (Solobj_handler_kit::the().get_hid());
}

// marshal
void
Solobj_handler::marshal(service& b, CORBA::Object_ptr obj)
{
	// Marshal an object that is sent as an operation argument.
	b.put_octet(_type);
	switch (_type) {
	case Solobj_handler::CRED:
		((solobj_impl::cred *)obj)->marshal(b);
		break;
	case Solobj_handler::NONE:
	default:
		b.get_env()->system_exception(CORBA::INV_OBJREF(0,
			CORBA::COMPLETED_MAYBE));
	}
}

CORBA::Object_ptr
Solobj_handler_kit::unmarshal(service &se, generic_proxy::ProxyCreator,
    CORBA::TypeId)
{
	// Unmarshal a received object and create a local stub for it.

	switch ((Solobj_handler::solobj_type) se.get_octet()) {
	case Solobj_handler::CRED:
		return (solobj_impl::cred::unmarshal(se));
	case Solobj_handler::NONE:
	default:
		se.get_env()->system_exception(CORBA::INV_OBJREF(0,
			CORBA::COMPLETED_MAYBE));
		return (CORBA::Object::_nil());
	}
}

void
Solobj_handler_kit::unmarshal_cancel(service& se)
{
	// Get the implementation to advance the stream.

	switch ((Solobj_handler::solobj_type) se.get_octet()) {
	case Solobj_handler::CRED:
		solobj_impl::cred::unmarshal_cancel(se);
		break;
	case Solobj_handler::NONE:
	default:
		break;
	}
}

//
// Returns a reference to Solobj_handler_kit instance.
//
Solobj_handler_kit&
Solobj_handler_kit::the()
{
	ASSERT(the_Solobj_handler_kit != NULL);
	return (*the_Solobj_handler_kit);
}

//
// Called by _init routine in pxfs/client/px_module to dynamically
// instantiate Solobj_handler_kit.
//
int
Solobj_handler_kit::initialize()
{
	ASSERT(the_Solobj_handler_kit == NULL);
	the_Solobj_handler_kit = new Solobj_handler_kit();
	if (the_Solobj_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

//
// static
void
Solobj_handler_kit::shutdown()
{
	if (the_Solobj_handler_kit != NULL) {
		delete the_Solobj_handler_kit;
		the_Solobj_handler_kit = NULL;
	}
}

Solobj_handler_cred::~Solobj_handler_cred()
{
}
