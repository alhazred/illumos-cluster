/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  solobj_impl_in.
 *
 */

#ifndef _SOLOBJ_IMPL_IN_H
#define	_SOLOBJ_IMPL_IN_H

#pragma ident	"@(#)solobj_impl_in.h	1.7	08/05/20 SMI"

#include <solobj/solobj_impl.h>

// create an interface object for a Solaris cred_t structure
inline solobj::cred_ptr
solobj_impl::conv(cred_t *credp)
{
	return ((new solobj_impl::cred(credp))->get_objref());
}

// return a Solaris cred_t structure for an interface object
inline cred_t *
solobj_impl::conv(solobj::cred_ptr credobj)
{
	return (((solobj_impl::cred *)credobj)->get_cred());
}

inline solobj::cred_ptr
solobj_impl::kcred()
{
	return ((&_kcred)->get_objref());
}

#endif	/* _SOLOBJ_IMPL_IN_H */
