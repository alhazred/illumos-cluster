/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SOLOBJ_HANDLER_H
#define	_SOLOBJ_HANDLER_H

#pragma ident	"@(#)solobj_handler.h	1.19	08/05/20 SMI"

#include <orb/handler/counter_handler.h>

class Solobj_handler_kit : public handler_kit {
public:
	Solobj_handler_kit() : handler_kit(SOLOBJ_HKID) { }

	CORBA::Object_ptr unmarshal(service &, generic_proxy::ProxyCreator,
	    CORBA::TypeId);

	void unmarshal_cancel(service&);

	static Solobj_handler_kit &the();
	static int initialize();
	static void shutdown();
private:
	static Solobj_handler_kit *the_Solobj_handler_kit;
};

//
// A handler for Solaris types that are passed as
// interfaces in object invocations.
//
class Solobj_handler : public counter_handler {
public:
	// types of solobj transfer
	enum solobj_type { NONE, CRED };

	virtual hkit_id_t	handler_id();

	Solobj_handler(CORBA::Object_ptr, solobj_type t) : _type(t) { }
	virtual			~Solobj_handler();

	void marshal(service&, CORBA::Object_ptr);
protected:
	solobj_type	_type;
};

// Solobj_handler_cred
class Solobj_handler_cred : public Solobj_handler {
public:
	Solobj_handler_cred(CORBA::Object_ptr o) : Solobj_handler(o, CRED) {}

	virtual		~Solobj_handler_cred();
};
#endif	/* _SOLOBJ_HANDLER_H */
