/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CLIF_RDS_H
#define	_CLIF_RDS_H

#pragma ident	"@(#)clif_rds.h	1.3	08/09/09 SMI"

#include <sys/list_def.h>
#include <orb/transport/pm_client.h>

#include <../src/uts/common/sys/ib/clients/rds/rdsib_sc.h>


#define	RDS_IFNAMELEN		16
#define	RDS_CLPRIVNET_DEV	"clprivnet"

/*
 * Used to build list of endpoints with associated IP addresses.
 * This info is not kept in the CCR if a path is disabled.
 * By the time we get a callback, the IP address info is
 * deleted and so we must rely on our own records.
 */
typedef struct rds_adpinfo {
	nodeid_t	nodeid;
	char		ifname[RDS_IFNAMELEN];
	ipaddr_t	ipaddr;
	struct rds_adpinfo *next;
} rds_adpinfo_t;

/*
 * Internal version of rds_path_t with buffers for interface names
 * attached to avoid the need for kmem_alloc during interrupt context.
 */
typedef struct rds_path_int {
	rds_path_t	rds_path;
	char		buf_local[RDS_IFNAMELEN];
	char		buf_remote[RDS_IFNAMELEN];
} rds_path_int_t;

/*
 * Client object to receive Path Manager callbacks.
 * For RDS, only need path_up/down, and add/remove_adapter.
 */
class clifrds_path_monitor : public pm_client, public _SList::ListElem {
public:
	static void initialize();
	static void terminate();

	clifrds_path_monitor();
	~clifrds_path_monitor();

	bool add_adapter(clconf_adapter_t *);
	bool remove_adapter(clconf_adapter_t *);
	bool change_adapter(clconf_adapter_t *);
	bool add_path(clconf_path_t *);
	bool remove_path(clconf_path_t *);
	bool node_alive(nodeid_t, incarnation_num);
	bool node_died(nodeid_t, incarnation_num);
	bool path_up(clconf_path_t *);
	bool path_down(clconf_path_t *);
	bool disconnect_node(nodeid_t, incarnation_num);
	void shutdown();

	/*
	 * Helper functions to fill up rds_path_t
	 */
	int make_path(clconf_path_t *, rds_path_int_t *);
	int make_path_loopback(clconf_adapter_t *, rds_path_int_t *);
	int make_path_endpoint(nodeid_t, clconf_adapter_t *,
		rds_path_endpoint_t *);

	void print_path(const char *event, rds_path_t *);

private:

	static clifrds_path_monitor *the_clifrds_path_monitor;

	//
	// Disallow assignments and pass by value
	//
	clifrds_path_monitor(const clifrds_path_monitor &);
	clifrds_path_monitor &operator = (clifrds_path_monitor &);

};


#endif	/* _CLIF_RDS_H */
