/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

//
// The clif_rds module translates callbacks from the SC path manager
// to RDS callbacks to help RDS manager a path map of cluster paths.
// RDS uses this map when the cluster private addresses
// (clusternodeN-priv) are used for RDS communications.
// This module does not maintain any state about paths or adapters.
// See materials for CLARC 2006/1611 for more details.
//


#pragma ident	"@(#)clif_rds.cc	1.3	08/09/09 SMI"


#include <sys/types.h>
#include <sys/modctl.h>
#include <sys/cmn_err.h>
#include <sys/systm.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/dlpi.h>

#include <sys/sol_version.h>
#include <orb/invo/common.h>
#include <orb/transport/path_manager.h>
#include <cplplrt/cplplrt.h>

//
// Solaris RDS is available from S10u4 onward.
// No clif_rds is delivered for any builds before s10.
//
#if SOL_VERSION < __s10
#else

#include <rds/clif_rds.h>

extern "C" int inet_pton(int af, const char *cp, void *addr);
extern "C" int ibt_hw_is_present(void);

uint32_t clifrds_debug = 0;

os::sc_syslog_msg *clifrds_msgp = NULL;

clifrds_path_monitor *clifrds_path_monitor::the_clifrds_path_monitor = NULL;

// To get stdout debug messages of path events and adpinfo cache actions
// #define	CLIFRDS_DEBUG	1

// For modunload. Currently not supported.
os::mutex_t clifrds_lock;
int clifrds_modunload_ok = 0;
int clifrds_shutdown_done = 0;

//
// adpinfo_list contains a list of all endpoints, local and remote.
// We used it to store and retrieve IP addresses associated with the
// endpoints when it is not available in the CCR (say when we get a path
// callback after a path is administratively disabled.)
//
// XXX Elements are only inserted and modified in the list.
// No delete currently, since a reference count must be implemented
// to allow delete correctly. As the number of elements
// is real small (typically under 10), this is tolerable.
//
static os::mutex_t adpinfo_lock;
static rds_adpinfo_t *adpinfo_list = NULL;

/* inter-module dependencies */
char    _depends_on[] =  "misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm drv/rdsib";

static struct modlmisc mymodlmisc = {
	&mod_miscops, "SC-RDS Interface module v1.0",
};

static struct modlinkage mymodlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &mymodlmisc, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &mymodlmisc, NULL, NULL, NULL }
#endif
};

int
_init(void)
{
	int error;

	if ((cluster_bootflags & CLUSTER_BOOTED) == 0)
		return (EINVAL);

	if (ibt_hw_is_present() == 0)
		return (ENODEV);

	if ((error = mod_install(&mymodlinkage)) != 0)
		return (error);

	_cplpl_init();		/* C++ initialization */

	// Initialize the syslog facility
	clifrds_msgp = new os::sc_syslog_msg(
	    "Cluster.Interconnect", "clif_rds", NULL);

	clifrds_path_monitor::initialize();

	//
	// Register with rdsib with clprivnet device name so that clprivnet0
	// could be considered by IP as IB-capable.
	//
	rds_clif_name(RDS_CLPRIVNET_DEV);

	return (0);
}

int
_fini(void)
{
	clifrds_lock.lock();
	if (clifrds_modunload_ok == 0) {
		clifrds_lock.unlock();
		return (EBUSY);
	}
	if (clifrds_shutdown_done == 0) {
		clifrds_shutdown_done++;
		clifrds_path_monitor::terminate();
	}
	clifrds_lock.unlock();

	delete clifrds_msgp;
	clifrds_msgp = NULL;

	_cplpl_fini();
	return (mod_remove(&mymodlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&mymodlinkage, modinfop));
}

static ipaddr_t
adpinfo_lookup(nodeid_t nodeid, const char *ifname)
{
	rds_adpinfo_t	*ap;
	ipaddr_t	ret = (ipaddr_t)0;

	adpinfo_lock.lock();
	for (ap = adpinfo_list; ap != NULL; ap = ap->next) {
		if (ap->nodeid == nodeid && strcmp(ap->ifname, ifname) == 0) {
#ifdef	CLIFRDS_DEBUG
			os::printf("adpinfo_lookup: %d %s 0x%x\n",
			    nodeid, ifname, ap->ipaddr);
#endif
			ret = ap->ipaddr;
			break;
		}
	}
	adpinfo_lock.unlock();
	return (ret);
}


static void
adpinfo_insert(nodeid_t nodeid, const char *ifname, ipaddr_t ipaddr)
{
	rds_adpinfo_t	*ap;

	adpinfo_lock.lock();
	for (ap = adpinfo_list; ap != NULL; ap = ap->next) {
		if (ap->nodeid == nodeid && strcmp(ap->ifname, ifname) == 0) {
#ifdef	CLIFRDS_DEBUG
			if (ap->ipaddr != ipaddr) {
				os::printf("adpinfo_insert: %d %s 0x%x\n",
				    nodeid, ifname, ap->ipaddr);
			}
#endif
			ap->ipaddr = ipaddr;
			break;
		}
	}
	if (ap == NULL) {
		ap = (rds_adpinfo_t *)kmem_alloc(
		    sizeof (rds_adpinfo_t), KM_NOSLEEP);
		if (ap != NULL) {
			ap->nodeid = nodeid;
			bcopy(ifname, ap->ifname, sizeof (ap->ifname));
			ap->ipaddr = ipaddr;
			ap->next = adpinfo_list;
			adpinfo_list = ap;
#ifdef	CLIFRDS_DEBUG
			os::printf("adpinfo_insert new: %d %s 0x%x\n",
			    nodeid, ifname, ap->ipaddr);
#endif
		}
	}
	adpinfo_lock.unlock();
}


static uint32_t
get_device_type(const char *dev_name)
{
	// XXX: Need to do actual DL type discovery
	// to support vanity names in the future

	if (strcmp(dev_name, "ibd") == 0)
		return (DL_IB);

	return (DL_ETHER);
}


void
clifrds_path_monitor::print_path(const char *event, rds_path_t *rpth)
{
	char msgbuf[128];
	uchar_t	*cp, *ucp;

	if (clifrds_debug == 0)
		return;

	cp = (uchar_t *)&rpth->local.ipaddr;
	ucp = (uchar_t *)&rpth->local.node_ipaddr;

	os::sprintf(msgbuf,
		"%s, %d.%d.%d.%d, %d.%d.%d.%d -> ",
		rpth->local.ifname,
		cp[0], cp[1], cp[2], cp[3],
		ucp[0], ucp[1], ucp[2], ucp[3]);

	cp = (uchar_t *)&rpth->remote.ipaddr;
	ucp = (uchar_t *)&rpth->remote.node_ipaddr;

	os::sprintf(&msgbuf[os::strlen(msgbuf)],
		"%s, %d.%d.%d.%d, %d.%d.%d.%d",
		rpth->local.ifname,
		cp[0], cp[1], cp[2], cp[3],
		ucp[0], ucp[1], ucp[2], ucp[3]);

	cmn_err(CE_NOTE, "SC-RDS Debug: %s %s", event, msgbuf);
}


//
// CONSTRUCTOR
//
clifrds_path_monitor::clifrds_path_monitor() :
	_SList::ListElem(this)
{
#ifdef CLIFRDS_DEBUG
	os::printf("clifrds_path_monitor constructor: this = %x\n", this);
#endif
}



//
// DESTRUCTOR
//
clifrds_path_monitor::~clifrds_path_monitor()
{
}



//
// Register with the cluster Path Manager for callbacks.
// The PM would trigger callbacks right away to give us
// the current state of paths and adapters.
//
void
clifrds_path_monitor::initialize()
{
	ASSERT(the_clifrds_path_monitor == NULL);
	the_clifrds_path_monitor = new clifrds_path_monitor();
	ASSERT(the_clifrds_path_monitor != NULL);

	path_manager::the().add_client(path_manager::PM_OTHER,
	    the_clifrds_path_monitor);
}




void
clifrds_path_monitor::terminate()
{

	path_manager::the().remove_client(path_manager::PM_OTHER,
		the_clifrds_path_monitor);

	delete the_clifrds_path_monitor;
	the_clifrds_path_monitor = NULL;
}

//
// Add Adapter callback. RDS operates on paths, not adapters.
// But we use this to simulate a loopback path where the local
// and remote endpoints are identical. It allows RDS to
// work in the absence of remote peers (say early in a
// cluster boot before the peer comes up).
//
bool
clifrds_path_monitor::add_adapter(clconf_adapter_t *adp)
{
	rds_path_int_t	rds_path;

	ASSERT(adp != NULL);

#ifdef CLIFRDS_DEBUG
	os::printf("ADD ADAPTER adp = %x\n", adp);
#endif

	if (make_path_loopback(adp, &rds_path) != 0) {
		//
		// SCMSGS
		// @explanation
		// Cluster cannot generate a callback for the RDS module.
		// Operation of RDS sockets might be affected.
		// @user_action
		// If Reliable Datagram Sockets (RDS) is used by
		// applications running on this cluster, contact your
		// Sun Microsystems service representative to determine
		// if a workaround or patch is available.
		//
		(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to generate callback for add adapter event");
		return (false);
	}

	print_path("Add Adapter", (rds_path_t *)&rds_path);

	rds_path_up((rds_path_t *)&rds_path);

	return (true);
}


bool
clifrds_path_monitor::remove_adapter(clconf_adapter_t *adp)
{
	rds_path_int_t	rds_path;

	ASSERT(adp != NULL);

#ifdef CLIFRDS_DEBUG
	os::printf("REMOVE ADAPTER adp = %x\n", adp);
#endif

	if (make_path_loopback(adp, &rds_path) != 0) {
		//
		// SCMSGS
		// @explanation
		// Cluster cannot generate a callback for the RDS module.
		// Operation of RDS sockets might be affected.
		// @user_action
		// If Reliable Datagram Sockets (RDS) is used by
		// applications running on this cluster, contact your
		// Sun Microsystems service representative to determine
		// if a workaround or patch is available.
		//
		(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to generate callback for remove adapter event");
		return (false);
	}

	print_path("Remove Adapter", (rds_path_t *)&rds_path);

	rds_path_down((rds_path_t *)&rds_path);

	return (true);
}

bool
clifrds_path_monitor::change_adapter(clconf_adapter_t *)
{
	return (false);
}



bool
clifrds_path_monitor::add_path(clconf_path_t *)
{
	return (true);
}



bool
clifrds_path_monitor::remove_path(clconf_path_t *)
{
	return (true);
}

//
// Make an RDS endpoint from clconf_adapter_t
//
int
clifrds_path_monitor::make_path_endpoint(nodeid_t nodeid,
    clconf_adapter_t *adp, rds_path_endpoint_t *pep)
{
	const char	*adpname, *local_ipaddr, *device_name;

	adpname = clconf_obj_get_name((clconf_obj_t *)adp);
	pep->ifname[RDS_IFNAMELEN - 1] = '\0';
	(void) os::strncpy(pep->ifname, adpname, (size_t)RDS_IFNAMELEN);

	if (pep->ifname[RDS_IFNAMELEN - 1] != 0) {
		//
		// SCMSGS
		// @explanation
		// Cluster cannot generate a callback for the RDS module.
		// Operation of RDS sockets might be affected.
		// @user_action
		// If Reliable Datagram Sockets (RDS) is used by
		// applications running on this cluster, contact your
		// Sun Microsystems service representative to determine
		// if a workaround or patch is available.
		//
		(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Interface name %s too long", adpname);
		return (1);
	}

	device_name = clconf_obj_get_property(
	    (clconf_obj_t *)adp, "device_name");
	pep->iftype = get_device_type(device_name);

	local_ipaddr = clconf_obj_get_property(
	    (clconf_obj_t *)adp, "ip_address");

	if (local_ipaddr == NULL) {
		//
		// CCR no longer contains the IP address.
		// This happens when a path/adapter is disabled.
		// Try to lookup using our cached list.
		//
		pep->ipaddr = adpinfo_lookup(nodeid, pep->ifname);
		if (pep->ipaddr == 0) {
			//
			// SCMSGS
			// @explanation
			// Cluster cannot generate a callback for the RDS
			// module. Operation of RDS sockets might be affected.
			// @user_action
			// If Reliable Datagram Sockets (RDS) is used by
			// applications running on this cluster, contact your
			// Sun Microsystems service representative to determine
			// if a workaround or patch is available.
			//
			(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "Failed to retrieve IP address for %s",
			    adpname);
			return (1);
		}
	} else {
		(void) inet_pton(AF_INET, local_ipaddr, &pep->ipaddr);
		pep->ipaddr = ntohl(pep->ipaddr);

		//
		// Add adapter info to our own cached list for
		// lookup during path down callback.
		//
		adpinfo_insert(nodeid, pep->ifname, pep->ipaddr);
	}

	if (clconf_get_user_ipaddr(nodeid,
	    (struct in_addr *)&pep->node_ipaddr) != 0) {
		//
		// SCMSGS
		// @explanation
		// Cluster cannot generate a callback for the RDS module.
		// Operation of RDS sockets might be affected.
		// @user_action
		// If Reliable Datagram Sockets (RDS) is used by
		// applications running on this cluster, contact your
		// Sun Microsystems service representative to determine
		// if a workaround or patch is available.
		//
		(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to retrieve private IP address for node %d",
		    nodeid);
		return (1);
	}

	return (0);
}


//
// Make an RDS path from clconf_path_t
//
int
clifrds_path_monitor::make_path(clconf_path_t *pth, rds_path_int_t *rpi)
{
	rds_path_t		*rds_path;
	nodeid_t		nodeid;
	clconf_cluster_t	*cl_cluster_p;
	clconf_adapter_t	*cl_adapter_p;
	int			error;


	ASSERT(pth != NULL && rpi != NULL);


	rds_path = &rpi->rds_path;
	rds_path->local.ifname = rpi->buf_local;
	rds_path->remote.ifname = rpi->buf_remote;


	cl_cluster_p = clconf_cluster_get_current();

	/* Set data for local endpoint */
	nodeid = clconf_get_local_nodeid();
	cl_adapter_p = clconf_path_get_adapter(pth, CL_LOCAL, cl_cluster_p);
	ASSERT(cl_adapter_p != NULL);
	error = make_path_endpoint(nodeid, cl_adapter_p, &rds_path->local);

	/* Set data for remote endpoint */
	nodeid = clconf_path_get_remote_nodeid(pth);
	cl_adapter_p = clconf_path_get_adapter(pth, CL_REMOTE, cl_cluster_p);
	ASSERT(cl_adapter_p != NULL);
	error += make_path_endpoint(nodeid, cl_adapter_p, &rds_path->remote);


	clconf_obj_release((clconf_obj_t *)cl_cluster_p);
	return (error);
}

//
// Make an RDS path from clconf_adapter_t, where local and
// remote endpoints are set identical.
//
int
clifrds_path_monitor::make_path_loopback(clconf_adapter_t *adp,
    rds_path_int_t *rpi)
{
	rds_path_t		*rds_path;
	nodeid_t		nodeid;
	int			error;


	ASSERT(adp != NULL && rpi != NULL);


	rds_path = &rpi->rds_path;
	rds_path->local.ifname = rpi->buf_local;
	rds_path->remote.ifname = rpi->buf_remote;


	/* Set data for local endpoint */
	nodeid = clconf_get_local_nodeid();
	error = make_path_endpoint(nodeid, adp, &rds_path->local);

	/* Set remote endpoint equal to local */
	(void) os::strncpy(rds_path->remote.ifname,
	    rds_path->local.ifname, (size_t)RDS_IFNAMELEN);
	rds_path->remote.ipaddr = rds_path->local.ipaddr;
	rds_path->remote.node_ipaddr = rds_path->local.node_ipaddr;

	return (error);
}


//
// Path Up callback.
// This routine is called at clock ipl, so blocking is not allowed
//
bool
clifrds_path_monitor::path_up(clconf_path_t *pth)
{
	rds_path_int_t	rds_path;

	ASSERT(pth != NULL);

#ifdef CLIFRDS_DEBUG
	os::printf("PATH UP pathp = %x\n", pth);
#endif

	if (make_path(pth, &rds_path) != 0) {
		//
		// SCMSGS
		// @explanation
		// Cluster cannot generate a callback for the RDS module.
		// Operation of RDS sockets might be affected.
		// @user_action
		// If Reliable Datagram Sockets (RDS) is used by
		// applications running on this cluster, contact your
		// Sun Microsystems service representative to determine
		// if a workaround or patch is available.
		//
		(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to generate callback for path up event");
		return (false);
	}

	print_path("Path Up", (rds_path_t *)&rds_path);

	rds_path_up((rds_path_t *)&rds_path);

	return (true);
}


//
// This routine is called at clock ipl, so blocking is not allowed
//
bool
clifrds_path_monitor::path_down(clconf_path_t *pth)
{
	rds_path_int_t	rds_path;

	ASSERT(pth != NULL);

#ifdef CLIFRDS_DEBUG
	os::printf("PATH DOWN pathp = %x\n", pth);
#endif

	if (make_path(pth, &rds_path) != 0) {
		//
		// SCMSGS
		// @explanation
		// Cluster cannot generate a callback for the RDS module.
		// Operation of RDS sockets might be affected.
		// @user_action
		// If Reliable Datagram Sockets (RDS) is used by
		// applications running on this cluster, contact your
		// Sun Microsystems service representative to determine
		// if a workaround or patch is available.
		//
		(void) clifrds_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to generate callback for path down event");
		return (false);
	}

	print_path("Path Down", (rds_path_t *)&rds_path);

	rds_path_down((rds_path_t *)&rds_path);

	return (true);
}

//
// Called in interrupt context. No sleep allowed.
//
bool
clifrds_path_monitor::disconnect_node(nodeid_t, incarnation_num)
{
	return (true);
}


bool
clifrds_path_monitor::node_alive(nodeid_t, incarnation_num)
{
	return (true);
}


//
// All processing is done by disconnect_node
//
bool
clifrds_path_monitor::node_died(nodeid_t, incarnation_num)
{
	return (true);
}

void
clifrds_path_monitor::shutdown()
{
	clifrds_lock.lock();
	if (clifrds_shutdown_done == 0) {
		clifrds_shutdown_done++;
		clifrds_modunload_ok++;
		clifrds_path_monitor::terminate();
	}
	clifrds_lock.unlock();
}

#endif	/* SOL_VERSION */
