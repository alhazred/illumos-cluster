//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// lib/misc.cc - miscellaneous helper functions.
//

#pragma ident	"@(#)pxfs_misc.cc	1.8	08/05/20 SMI"

#include <sys/sol_conv.h>
#include <sys/sol_version.h>

#include "../version.h"
#include <pxfs/lib/pxfs_misc.h>

#if SOL_VERSION >= __s10
#ifndef	BUG_4954775
#define	BUG_4954775
#endif
#endif

//
// Return true when fobjtype and vnode type are compatible
//
// static
bool
pxfs_misc::fobj_vnode_type_match(PXFS_VER::fobj_type_t ftype,
    enum vtype vnode_type)
{
	bool	result;

	switch (vnode_type) {
	case VREG:
		result = ftype == PXFS_VER::fobj_file;
		break;

	case VDIR:
		result = ftype == PXFS_VER::fobj_unixdir;
		break;

	case VLNK:
		result = ftype == PXFS_VER::fobj_symbolic_link;
		break;

	case VSOCK:		// XXX - should have its own file object type
		result = ftype == PXFS_VER::fobj_file;
		break;

	case VBLK:
	case VCHR:
		result = (ftype == PXFS_VER::fobj_io) ||
		    (ftype == PXFS_VER::fobj_special);
		break;

	case VFIFO:
		result = ftype == PXFS_VER::fobj_special;
		break;

	case VNON:	// No file object
	case VDOOR:	// Not supported
	case VPROC:	// Not supported
	case VBAD:	// No file object
#ifdef BUG_4954775
	case VPORT:	// Not supported
#endif
		result = false;
		break;
	}
	return (result);
}

//
// Common code for initializing an fobj_info from a vnode.
//
void
pxfs_misc::init_fobjinfo(PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::fobj_type_t ftype,
    vnode_t *vnodep, const fid_t *fidp)
{
	ASSERT(fobj_vnode_type_match(ftype, vnodep->v_type));

	fobjinfo.ftype = ftype;
	fobjinfo.vtype = conv(vnodep->v_type);
	fobjinfo.vrdev = vnodep->v_rdev;
	fobjinfo.vflag = vnodep->v_flag & (VROOT | VDUP | VNOMAP | VNOSWAP |
	    VNOMOUNT | VSWAPLIKE | VDIROPEN);
	fobjinfo.fid.fobjid_len = fidp->fid_len;
	bcopy(fidp->fid_data, fobjinfo.fid.fobjid_data, (size_t)fidp->fid_len);
}

// This function hashes the dev_t and fid.
//
uint_t
pxfs_misc::hash_devt_fid(dev_t dev, const fid_t *fidp, uint_t hsz)
{
	ASSERT(fidp != NULL);
	uint_t	h_val = (getmajor(dev) + getminor(dev));
	uint_t	h = 0;
	for (ushort_t idx = 0; idx < fidp->fid_len; idx++) {
		h = (h << 4) + (uint_t)((int)fidp->fid_data[idx]);
	}
	h_val = (h_val + h) % hsz;
	return (h_val);
}
