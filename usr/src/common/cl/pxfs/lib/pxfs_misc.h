//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//  Miscellaneous PXFS library functions that are shared by different areas
//

#ifndef PXFS_MISC_H
#define	PXFS_MISC_H

#pragma ident	"@(#)pxfs_misc.h	1.8	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/vfs.h>
#include <sys/uio.h>
#include <sys/systm.h>

#include "../version.h"
#include PXFS_IDL(pxfs)

//
// Use a class for namespace.
//
class pxfs_misc {
public:
	// Based on uioseg, call kcopy or xcopyout/xcopyin.
	static int	uio_copyout(const void *from, void *to, size_t len,
	    uio_seg uioseg);

	static int	uio_copyin(const void *from, void *to, size_t len,
	    uio_seg uioseg);

	// Check that fobjtype and vnode type match
	static bool	fobj_vnode_type_match(PXFS_VER::fobj_type_t ftype,
	    enum vtype vnode_type);

	// Common code for initializing an fobj_info from a vnode.
	static void	init_fobjinfo(PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::fobj_type_t ftype, vnode_t *vp, const fid_t *fidp);

	//
	// Hash the dev_t and fid to generate an index into the hash table.
	// This is used in the pxfs client code to hash values for the
	// pxfobj hash table and on the server side to hash the values for the
	// fobj_ii hash table.
	//
	static uint_t	hash_devt_fid(dev_t dev, const fid_t *fidp, uint_t hsz);
};

#include <pxfs/lib/pxfs_misc_in.h>

#endif	// PXFS_MISC_H
