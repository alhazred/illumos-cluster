//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _PXFS_DEBUG_H
#define	_PXFS_DEBUG_H

#pragma ident	"@(#)pxfs_debug.h	1.6	08/05/20 SMI"

#include <sys/types.h>

#include <sys/dbg_printf.h>

#include "../version.h"

#define	PXFS_DBGBUF

#ifdef	PXFS_DBGBUF
extern dbg_print_buf pxfs_dbg;
#define	PXFS_DBG(a)	pxfs_dbg.dbprintf a
#else
#define	PXFS_DBG(a)
#endif

const int	PXFS_GREEN = 3;
const int	PXFS_AMBER = 2;
const int	PXFS_RED = 1;

//
// These options control which debug statements are enabled.
// This allows them to be controlled via the debugger.
// These constants are always defined in order to catch name conflicts.
//
const int	PXFS_TRACE_UFS			= 0x00000001;
const int	PXFS_TRACE_PROVCOMMON		= 0x00000002;
const int	PXFS_TRACE_FOBJ			= 0x00000004;
const int	PXFS_TRACE_FOBJPLUS		= 0x00000008;
const int	PXFS_TRACE_FILE			= 0x00000010;
const int	PXFS_TRACE_UNIXDIR		= 0x00000020;
const int	PXFS_TRACE_FLK			= 0x00000040;
const int	PXFS_TRACE_BULKIO		= 0x00000100;
const int	PXFS_TRACE_BULKIO_AIO		= 0x00000200;
const int	PXFS_TRACE_BULKIO_PAGES		= 0x00000400;
const int	PXFS_TRACE_BULKIO_UIO		= 0x00000800;
const int	PXFS_TRACE_ASYNC_IO		= 0x00001000;
const int	PXFS_TRACE_FS			= 0x00002000;
const int	PXFS_TRACE_FSMGR_C		= 0x00004000;
const int	PXFS_TRACE_PXVFS		= 0x00010000;
const int	PXFS_TRACE_PXFOBJ		= 0x00020000;
const int	PXFS_TRACE_PXFOBJPLUS		= 0x00040000;
const int	PXFS_TRACE_REG			= 0x00080000;
const int	PXFS_TRACE_DIR			= 0x00100000;
const int	PXFS_TRACE_ACCESS_CACHE		= 0x00200000;
const int	PXFS_TRACE_AIO_CALLBACK		= 0x00400000;
const int	PXFS_TRACE_MEM_ASYNC		= 0x00800000;
const int	PXFS_TRACE_IO			= 0x02000000;
const int	PXFS_TRACE_CHR			= 0x04000000;
const int	PXFS_TRACE_NLM			= 0x20000000;
const int	PXFS_TRACE_VXFS			= 0x40000000;

extern uint_t	pxfs_trace_options;
extern int	pxfs_trace_level;

//
// PXFS_DBPRINTF - record pxfs trace information.
// Inputs
//	option - enables trace if set in pxfs_trace_options
//	level - Debug level, currently red, amber and green
//	args - The arguments to the print statements must be enclosed
//		with parenthesis, as in the following example:
//			("pxfs action %d\n", action_number)
//
// Note that the trailing "else" statement in the macro removes
// ambiguity when the macro is nested in another "if" statement.
//
#ifdef PXFS_DBGBUF
#define	PXFS_DBPRINTF(option, level, args) \
if (((option & pxfs_trace_options) &&\
	(level <= pxfs_trace_level)) ||\
	(level == PXFS_RED)) {\
		PXFS_DBG(args); \
	} else
#else
#define	PXFS_DBPRINTF(option, level, args)
#endif	/* PXFS_DBGBUF */

#define	PXFS_KSTATS_ENABLED

#ifdef  PXFS_KSTATS_ENABLED
#define	PXFS_KSTATS(p, a)	{ if ((p) != NULL) { (a); } }
#else
#define	PXFS_KSTATS(p, a)
#endif  /* PXFS_KSTATS_ENABLED */

#endif // _PXFS_DEBUG_H
