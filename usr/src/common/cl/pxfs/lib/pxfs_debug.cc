//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_debug.cc	1.8	08/05/20 SMI"

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>

#ifdef	PXFS_DBGBUF

#ifdef DEBUG

//
// We want the debug buffers to be significantly larger
// if running debug bits - let's make it 10mb.
//
dbg_print_buf pxfs_dbg(0xA00000);

#else

// For the non-debug case we'l do with 512k
dbg_print_buf pxfs_dbg(0x80000);

#endif // DEBUG

#endif

#define	ALL_OPTIONS	(uint_t)0xffffffff

uint_t pxfs_trace_options = ALL_OPTIONS & ~(
	PXFS_TRACE_FLK);

int pxfs_trace_level = PXFS_AMBER;
