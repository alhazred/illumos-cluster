//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)device_replica_impl.cc	1.73	08/05/20 SMI"

#include <sys/conf.h>
#include <sys/bootconf.h>

#include <nslib/ns.h>
#include <sys/os.h>
#include <sys/sol_conv.h>
#include <sys/mc_probe.h>
#include <sys/cl_events.h>
#include <sys/rm_util.h>
#include <orb/infrastructure/orb_conf.h>
#include <cmm/cmm_ns.h>
#include <pxfs/common/pxfslib.h>
#include <pxfs/device/device_debug.h>
#include <pxfs/device/device_service_mgr.h>
#include <pxfs/device/exec_program.h>
#include <pxfs/device/device_replica_impl.h>
#include <pxfs/device/device_server_impl.h>
#include <pxfs/version.h>
#include <pxfs/server/io_impl.h>

#include <dc/sys/dc_debug.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

// Initialize static variables.
uint_t		device_replica_impl::primaries_count = 0;
os::mutex_t	device_replica_impl::primaries_lock;
bool		device_replica_impl::primaries_enabled = true;
DList<device_replica_impl>	device_replica_impl::device_replica_list;
os::mutex_t	device_replica_impl::device_replica_list_lock;

//
// This struct is used for building a table for supporting the mapping
// from version protocol spec file version number to the various IDL
// interface versions it represents.  The table will be a two dimensional
// array indexed by major/minor vp version.
//
typedef struct {		// idl interfaces
	int	device_server;	// mdc::device_server
	int	device_ckpt;	// repl_dc::repl_dev_server_ckpt
} device_ver_map_t;

//
// These are the current maximum indexes used for accessing the vp to idl
// version table.
//
const int	DEVICE_VP_MAX_MAJOR = 3;
const int	DEVICE_VP_MAX_MINOR = 0;

//
// The struct has an entry for each IDL interface which is being versioned. For
// a given VP major and minor version, we get the IDL version of those
// interfaces.
//
device_ver_map_t
    device_vp_to_idl[DEVICE_VP_MAX_MAJOR + 1][DEVICE_VP_MAX_MINOR +1] = {
	{ { 0, 0 } },		// VP Version 0.0 defined for indexing
	{ { 0, 0 } },		// VP Version 1.0 currently unused
	{ { 2, 2 } },		// VP Version 2.0 Object Consolidation
	{ { 3, 2 } }		// VP Version 3.0
};

//
// Constructor.
//
//lint -e1732 -e668
device_replica_impl::device_replica_impl(const char *s_id, const char *p_id,
    unsigned int dev_id, const char *takeover_pgm, const char *service_nm,
    mdc::dc_mapper_ptr _mapper, bool _register_if_first) :
	repl_server<repl_dc::repl_dev_server_ckpt>(s_id, p_id),
	_inactive(false), _is_primary(false), _ckpt_proxy(NULL)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::device_replica_impl(%s, %s, %d, %s, %s, "
	    "%p, %d).\n", s_id, p_id, dev_id, takeover_pgm, service_nm,
	    _mapper, _register_if_first));

	(void) strcpy(provider_id, p_id);
	devid = dev_id;
	//lint -e1733
	takeover_program = strcpy(new char[strlen(takeover_pgm) + 1],
	    takeover_pgm);
	//lint +e1733
	service_name = strcpy(new char[strlen(service_nm) + 1], service_nm);
	device_serverp = NULL;
	mapperobj = mdc::dc_mapper::_duplicate(_mapper);
	register_if_first = _register_if_first;
	add_to_ds_list(this);
}
//lint +e1732 +e668

//
// Destructor.
//
device_replica_impl::~device_replica_impl()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::~device_replica_impl().\n"));

	ASSERT(device_serverp == NULL);

	remove_from_ds_list(this);

	delete [] takeover_program;
	delete [] service_name;

	_ckpt_proxy = NULL;
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// Helper functions to manage the list of device service replicas
// stored on each node.
//
void
device_replica_impl::add_to_ds_list(device_replica_impl *drp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::add_to_ds_list(%p).\n", drp));

	device_replica_list_lock.lock();
	device_replica_list.prepend(drp);
	device_replica_list_lock.unlock();
}

void
device_replica_impl::remove_from_ds_list(device_replica_impl *drp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::remove_from_ds_list(%p).\n", drp));

	device_replica_list_lock.lock();
	(void) device_replica_list.erase(drp);
	device_replica_list_lock.unlock();
}

//
// Mark the device service with name 'service_name' as shutting down.
// If we are able to do this successfully, or if we don't find the service
// identified by 'service_name', we return success.
//
dc_error_t
device_replica_impl::set_inactive(const char *_service_name, bool inactive)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::set_inactive(%s, %d).\n",
	    _service_name, inactive));

	device_replica_impl *drp = NULL;
	dc_error_t error = DCS_SUCCESS;

	device_replica_list_lock.lock();
	for (device_replica_list.atfirst();
	    (drp = device_replica_list.get_current()) != NULL;
	    device_replica_list.advance()) {
		if (strcmp(drp->get_service_name(), _service_name) == 0) {
			break;
		}
	}

	if (drp != NULL) {
		error = drp->set_inactive(inactive);
	}

	device_replica_list_lock.unlock();

	return (error);
}

//
// Set the state of this replica to active or inactive.  If the replica is
// inactive, it will not let future 'become_primary' calls succeed.
//
dc_error_t
device_replica_impl::set_inactive(bool inactive)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::set_inactive(%d).\n", inactive));

	if (!inactive) {
		_inactive = false;
	} else {
		//
		// Check if this replica is curently the primary.  If not,
		// set state to inactive.
		//
		_inactive_lock.lock();
		if (_is_primary) {
			_inactive_lock.unlock();
			DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("set_inactive(%p): replica is primary.\n"));
			return (DCS_ERR_NODE_BUSY);
		}
		_inactive = true;
		_inactive_lock.unlock();
	}

	return (DCS_SUCCESS);
}

//
// Accessor function.
//
const char *
device_replica_impl::get_service_name()
{
	return (service_name);
}

//
// This is a static function called from
// device_service_mgr::start_device_service() to create a new
// device service replica and register it with the HA framework.
// There are two ways a device service replica can be created: (i) by an
// attempt to access the service in which case the DCS starts up replicas
// where necessary, and (ii) by a node booting, in which case the DSM starts up
// replicas for all the currently active services.  If a replica is created
// by mode (ii), then it is possible that the creation of the replica starts
// up a new incarnation of the device service.  In this case, the DCS will now
// have stale handle to the object representing this device service.  To update
// this information, the new primary should invoke 'register_device_server'.
// Note that if the replica is created using method (i), it is not safe to
// invoke 'register_device_server' because it will lead to a call back into
// the DCS from the DCS, and could deadlock the HA framework.
// Information on whether the replica was initially created via method (i) or
// (ii) is stored in the 'register_if_first' variable.
//
void
device_replica_impl::initialize(unsigned int dev_id, const char *service_nm,
    const char *takeover_pgm, uint_t priority, uint_t num_secondaries,
    mdc::dc_mapper_ptr mapperp, bool _register_if_first)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::initialize(%d, %s, %s, %d, %d, %p, %d).\n",
	    dev_id, service_nm, takeover_pgm, priority, num_secondaries,
	    mapperp, _register_if_first));
	//
	// First, get the service id and the provider id that this replica will
	// use to register with the framework.
	//
	char *s_id = dclib::get_serviceid(service_nm);

	char p_id[dclib::NODEID_STRLEN];
	(void) sprintf(p_id, "%d", orb_conf::node_number());

	//
	// Now, create the new replica and register it with
	// the framework.
	//
	device_replica_impl *repl_server_instance =
	    new device_replica_impl(s_id, p_id, dev_id, takeover_pgm,
		service_nm, mapperp, _register_if_first);

	Environment env;
	CORBA::Exception *ex;
	//
	// If the user did not explicitly set the desired number
	// of secondaries, then it's set to the default value.
	//
	repl_server_instance->register_with_rm(priority, true,
	    ((num_secondaries == 0) ? DCS_DEFAULT_DESIRED_NUM_SECONDARIES :
	    num_secondaries), env);
	if ((ex = env.exception()) != NULL) {
		//
		// If a replica is already registered, it means that
		// the dc_config HA server failed over and called
		// device_service_mgr::start_device_service() a second time.
		// We can safely ignore this case.
		//
		if (replica::repl_prov_already_exists::_exnarrow(ex) == NULL) {
			//
			// This is NOT the 'repl_prov_already_exists' exception.
			//
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("initialize(): exception '%s' while calling "
			    "register_with_rm().\n", ex->_name()));
			os::sc_syslog_msg msg(SC_SYSLOG_DEVICE_SERVICE_TAG,
			    service_nm, NULL);
			//
			// SCMSGS
			// @explanation
			// The device configuration system on this node has
			// suffered an internal error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "Error registering provider '%s' with the "
			    "framework.", p_id);
		}
		env.clear();
	}

	delete [] s_id;
}

//
// This static function takes in a StringSeq of provider ids and returns a
// comma-delimited list of nodes that the providers run on.
// For example, if the provider ids passed in are for the providers on
// nodes 1 and 3, this function returns "1,3".  It is the responsibility
// of the caller to free the memory returned.
//
char *
device_replica_impl::get_nodelist(const CORBA::StringSeq &nodes)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::get_nodelist().\n"));

	uint_t len = nodes.length();

	char *tmp;

	if (len == 0) {
		tmp = new char[1];
		tmp[0] = 0;
		return (tmp);
	}

	//lint -e647
	tmp = new char[len * dclib::NODEID_STRLEN + 1];
	//lint +e647

	//lint -e668
	(void) strcpy(tmp, (const char *)nodes[0]);
	for (uint_t i = 1; i < len; i++) {
		(void) strcat(tmp, ",");
		(void) strcat(tmp, (const char *)nodes[i]);
	}
	//lint +e668

	return (tmp);
}

//
// This function is called during replica state transitions to log state changes
// and to invoke the user program associated with this service appropriately.
// The usage of the user program is of the form:
// "takeover_pgm	-s service_name
//		-c command
//		[-o current_secondaries]
//		[-a additions]
//		[-d deletions]"
//
int
device_replica_impl::transition_notify(program_type type,
    const char *secondaries, const char *delta)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::transition_notify(%d, %s, %s).\n",
	    type, secondaries, delta));

	os::sc_syslog_msg msg(SC_SYSLOG_DEVICE_SERVICE_TAG, service_name, NULL);

	//
	// If no user program is specified, we just return success.
	//
	if (*takeover_program == 0) {
		DCS_DBPRINTF_G(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("transition_notify(%p): no takeover program.\n", this));
		return (0);
	}

	//
	// Every argument needs 4 characters for the preceding command-type
	// specification plus a null byte for the whole string.
	// For example, in the command:
	// "user_pgm -s <service> -c primary_to_secondary",
	// the "primary_to_secondary" argument needs four spaces for " -c ".
	// ("primary_to_secondary" is also the longest command).
	//
	size_t user_pgm_len = strlen(takeover_program) + 1 +
	    4 + strlen(service_name) +
	    4 + strlen("primary_to_secondary");
	if (secondaries != NULL)
		user_pgm_len += 4 + strlen(secondaries);
	if (delta != NULL)
		user_pgm_len += 4 + strlen(delta);

	char *user_pgm = new char[user_pgm_len];

	//lint -e668
	switch (type) {
	case MAKE_EXCLUSIVE:
		//
		// This replica is being made the first primary of this
		// incarnation of this service.  We need to log two things -
		// that a replica was added, and that this replica is the
		// primary.
		//
		(void) msg.log(SC_SYSLOG_NOTICE, ADDED, NULL);
		(void) msg.log(SC_SYSLOG_NOTICE, OWNERSHIP_CHANGED, NULL);

		(void) sprintf(user_pgm, "%s -s %s -c make_exclusive",
		    takeover_program, service_name);
		ASSERT(secondaries == NULL);
		ASSERT(delta == NULL);
		break;

	case MAKE_PRIMARY:
		// This replica is being made a primary.
		(void) msg.log(SC_SYSLOG_NOTICE, OWNERSHIP_CHANGED, NULL);

		(void) sprintf(user_pgm, "%s -s %s -c make_primary",
		    takeover_program, service_name);
		if (secondaries != NULL && *secondaries != '\0') {
			(void) strcat(user_pgm, " -o ");
			(void) strcat(user_pgm, secondaries);
		}
		if (delta != NULL && *delta != '\0') {
			(void) strcat(user_pgm, " -d ");
			(void) strcat(user_pgm, delta);
		}
		break;

	case PRIMARY_TO_SECONDARY:
		//
		// This replica is being downgraded from a primary to a
		// secondary.
		//
		(void) msg.log(SC_SYSLOG_NOTICE, OWNERSHIP_CHANGED, NULL);

		(void) sprintf(user_pgm, "%s -s %s -c primary_to_secondary",
		    takeover_program, service_name);
		ASSERT(secondaries == NULL);
		ASSERT(delta == NULL);
		break;

	case REMOVE_SECONDARY:
		// A secondary replica is being made a spare.
		(void) msg.log(SC_SYSLOG_NOTICE, REMOVED, NULL);

		(void) sprintf(user_pgm, "%s -s %s -c remove_secondary",
		    takeover_program, service_name);
		if (secondaries != NULL && *secondaries != '\0') {
			(void) strcat(user_pgm, " -o ");
			(void) strcat(user_pgm, secondaries);
		}
		ASSERT(delta != NULL && *delta != '\0');
		(void) strcat(user_pgm, " -d ");
		(void) strcat(user_pgm, delta);
		break;

	case ADD_SECONDARY:
		//
		// This call is made on the primary when a new replica is added
		// as a secondary - there is no need to log a message here as
		// that is done when this function is called with
		// 'MAKE_SECONDARY' on that replica.
		//
		(void) sprintf(user_pgm, "%s -s %s -c add_secondary",
		    takeover_program, service_name);
		if (secondaries != NULL && *secondaries != '\0') {
			(void) strcat(user_pgm, " -o ");
			(void) strcat(user_pgm, secondaries);
		}
		ASSERT(delta != NULL && *delta != '\0');
		(void) strcat(user_pgm, " -a ");
		(void) strcat(user_pgm, delta);
		break;

	case MAKE_SECONDARY:
		// This replica is being added as a secondary.
		(void) msg.log(SC_SYSLOG_NOTICE, ADDED, NULL);

		(void) sprintf(user_pgm, "%s -s %s -c make_secondary",
		    takeover_program, service_name);
		//lint +e668
		ASSERT(secondaries == NULL);
		ASSERT(delta == NULL);
		break;

	default:
		ASSERT(0);
	}

	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("transition_notify(%p): user_program = '%s'.\n", this, user_pgm));

	// Run the user program in a separate thread.  See BugId 4207582.
	Environment e;
	int err = exec_program::execute_program(user_pgm,
	    orb_conf::node_number(), true, &msg, e);

	// We are running the program locally, the node cannot be dead.
	ASSERT(err == 0);

	if (e.exception()) {
		sol::op_e *ex = sol::op_e::_exnarrow(e.exception());
		if (ex == NULL) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("transition_notify(%p): error while forking '%s'"
			    ".\n", this, user_pgm));

			DEVICE_DBPRINTF(
			    DEVICE_TRACE_DEVREPL,
			    DEVICE_RED,
			    ("device server:  Error while forking "
			    "program %s", user_pgm));
			e.clear();
			delete [] user_pgm;
			return (-1);
		}

		DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("transition_notify(%p): exception '%s' returned by "
		    "execute_program(%s).\n", this, e.exception()->_name(),
		    user_pgm));

		int error = ex->error;
		e.clear();
		delete [] user_pgm;
		return (error);
	}

	delete [] user_pgm;

	return (0);
}

//
// Call to disable new primaries on this node.  This will succeed only if
// there are currently no primaries hosted on this node.
//
bool
device_replica_impl::disable_primaries()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::disable_primaries().\n"));

	primaries_lock.lock();
	if (primaries_count > 0) {
		primaries_lock.unlock();
		return (false);
	}
	primaries_enabled = false;
	primaries_lock.unlock();
	return (true);
}

//
// Un-does the effect of disable_primaries().
//
void
device_replica_impl::enable_primaries()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::enable_primaries().\n"));

	primaries_lock.lock();
	primaries_enabled = true;
	primaries_lock.unlock();
}

//
// Return the state of device_replica_impl - whether new primaries are
// permitted on this node or not.
//
bool
device_replica_impl::is_disabled()
{
	return (!primaries_enabled);
}

//
// This method is called by the framework to make this replica a primary.
// It may currently be a spare or a secondary - these two cases need different
// upcalls to user space.
//
// device_replica_impl(replica::repl_prov::become_primary)
void
device_replica_impl::become_primary(
    const replica::repl_name_seq &secondary_names, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::become_primary(%p).\n",
	    secondary_names));

	MC_PROBE_0(dc_become_primary_start, "clustering pxfs", "");

	int err;
	uint_t i;
	uint_t len = secondary_names.length();
	uint_t current_len = current_replicas.length();
	bool success = true;
	os::sc_syslog_msg msg(SC_SYSLOG_DEVICE_SERVICE_TAG, service_name, NULL);
	char *new_state = CL_DS_BECOME_PRIMARY_STARTED;
	char *old_state;
	char nodename[CL_MAX_LEN + 1];
	cl_event_severity_t event_severity;
	cl_event_reason_code_t become_primary_reason;
	bool spare_to_primary = false;

	clconf_get_nodename(orb_conf::node_number(), nodename);

	//
	// Select the version for the checkpoint handler.
	// A new version of the device server will not be created
	// until the entire cluster has software supporting the new
	// version of PXFS, Mount, and Device subsystems. The new version
	// does not change any old functionality, it just adds functionality
	// for the new version. So the new version supports old stuff.
	//
	// Under this approach callbacks are not needed.
	// That could change in a future release.
	//
	version_manager::vp_version_t	device_version;
	(void) pxfslib::get_running_version("px_device", &device_version);

	CORBA::type_info_t	*typ =
	    repl_dc::repl_dev_server_ckpt::_get_type_info(
	    device_vp_to_idl[device_version.major_num]
	    [device_version.minor_num].device_ckpt);

	// Initialize the checkpoint proxy.
	replica::checkpoint_var		tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_dc::repl_dev_server_ckpt::_narrow(tmp_ckpt_v);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	_inactive_lock.lock();
	if (_inactive) {
		_inactive_lock.unlock();

		DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("become_primary(%p): cannot host service '%s' because "
		    "the replica is inactive.\n", this, service_name));

		//
		// SCMSGS
		// @explanation
		// A switchover/failover was attempted to a node that was
		// being removed from the list of nodes that could host this
		// device service.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) msg.log(SC_SYSLOG_NOTICE, MESSAGE, "Could not host "
		    "device service %s because this node is being removed "
		    "from the list of eligible nodes for this service.",
		    service_name);
		_environment.exception(new replica::repl_prov_failed());

		// Since we are returning exception, release the checkpoint ref.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;

		return;
	}
	_is_primary = true;
	_inactive_lock.unlock();

	primaries_lock.lock();
	if (!primaries_enabled) {
		ASSERT(primaries_count == 0);
		primaries_lock.unlock();

		DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("become_primary(%p): cannot host service '%s' because "
		    "the node is shutting down.\n", this, service_name));

		//
		// SCMSGS
		// @explanation
		// An attempt was made to start a device group on this node
		// while the node was being shutdown.
		// @user_action
		// If the node was not being shutdown during this time, or if
		// the problem persists, contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) msg.log(SC_SYSLOG_NOTICE, MESSAGE, "Could not host "
		    "device service %s because this node is being shut down",
		    service_name);
		_environment.exception(new replica::repl_prov_failed());
		_is_primary = false;

		// Since we are returning exception, release the checkpoint ref.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;

		return;
	}
	primaries_count++;
	primaries_lock.unlock();

	FAULTPT_DCS(FAULTNUM_DCS_DEVICE_SERVICE_BECOME_PRIMARY_S_B,
		FaultFunctions::generic);

	if (device_serverp == NULL) {
		//
		// We are being promoted from 'spare' to 'primary'.
		// Primary object has to be created.
		//
		spare_to_primary = true;

		device_serverp = new device_server_repl_impl(this, devid);
	}

	//
	// This call could be the result of a rolling upgrade commit.
	// We upgrade our reference to the device server to the
	// highest comitted version.
	//
	device_server_obj = device_serverp->get_objref(
	    mdc::device_server::_get_type_info(
	    device_vp_to_idl[device_version.major_num]
	    [device_version.minor_num].device_server));
	device_serverp->get_checkpoint()->ckpt_new_device_server(
	    device_server_obj, _environment);
	_environment.clear();

	if (spare_to_primary) {
		ASSERT(current_len == 0);

		err = transition_notify(MAKE_EXCLUSIVE, NULL, NULL);
		old_state = DS_STATE_SPARE;
		become_primary_reason = CL_REASON_DS_STARTUP;
	} else {
		//
		// We are being promoted from 'secondary' to 'primary'.
		//
		ASSERT(current_len > len);

		char *secondaries = get_nodelist(secondary_names);

		if (current_len == len + 1) {
			//
			// Number of replicas known by the previous primary
			// is the same as now - this is a switchover.
			// Make the appropriate upcall to user space.
			//
			DCS_DBPRINTF_G(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("become_primary(%p): this is a switchover of "
			    "service '%s'.\n", this, service_name));

			err = transition_notify(MAKE_PRIMARY, secondaries,
			    NULL);
			old_state = DS_STATE_SECONDARY;
			become_primary_reason = CL_REASON_DS_SWITCHOVER;
		} else {
			//
			// This is a failover.  It is possible that other
			// secondaries have also failed so calculate the list
			// of nodes that are no longer replicas, and make
			// the appropriate upcalls to user space.
			//
			DCS_DBPRINTF_G(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("become_primary(%p): this is a failover of "
			    "service '%s'.\n", this, service_name));

			uint_t del_replicas_len = current_len - len - 1;
			CORBA::StringSeq del_replicas(del_replicas_len,
			    del_replicas_len);

			uint_t del_count = 0;

			//
			// Iterate through the current list, and check for the
			// existence of the provider in the new list of
			// secondaries.
			//
			for (i = 0; i < current_len; i++) {

				bool found = false;
				ASSERT(del_count < del_replicas_len);

				const char *tmp_curr = current_replicas[i];

				//
				// Compare with the provider id of the current
				// replica.
				//
				if (strcmp(tmp_curr, provider_id) == 0) {
					found = true;
					continue;
				}

				// Compare with the other replicas.
				for (uint_t j = 0; j < len; j++) {
					if (strcmp(tmp_curr,
					    secondary_names[j]) == 0) {
						found = true;
						break;
					}
				}

				if (!found) {
					del_replicas[del_count] = tmp_curr;
					del_count++;
					if (del_count == del_replicas_len) {
						break;
					}
				}
			}

			char *del_nodes = get_nodelist(del_replicas);
			err = transition_notify(MAKE_PRIMARY, secondaries,
			    del_nodes);
			old_state = DS_STATE_SECONDARY;
			become_primary_reason = CL_REASON_DS_FAILOVER;
			delete [] del_nodes;
		}

		delete [] secondaries;
	}

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_PRIMARIES_CHANGING,
		CL_EVENT_PUB_DCS, CL_EVENT_SEV_INFO,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		get_service_name(),
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		old_state,
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		new_state,
		CL_REASON_CODE, DATA_TYPE_UINT32,
		become_primary_reason,
		CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		os::gethrtime(),
		CL_NODE_NAME, SE_DATA_TYPE_STRING,
		nodename,
		NULL);

	if (err != 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("become_primary(%p): failed with error '%d' for service "
		    "'%s'.\n", this, err, service_name));

		DEVICE_DBPRINTF(
		    DEVICE_TRACE_DEVREPL,
		    DEVICE_RED,
		    ("become_primary: failed with error %d\n", err));
		success = false;
		goto out;
	}

	FAULTPT_DCS(FAULTNUM_DCS_DEVICE_SERVICE_BECOME_PRIMARY_S_A,
		FaultFunctions::generic);

	ASSERT(err == 0);

	if (current_len != len + 1) {
		//
		// Update the list of nodes.
		//
		current_replicas.length(len + 1);
		current_replicas[0] = (const char *)provider_id;
		for (i = 1; i <= len; i++) {
			current_replicas[i] =
			    (const char *)secondary_names[i-1];
		}

		//
		// Checkpoint the list to the secondaries.
		//
		device_serverp->get_checkpoint()->ckpt_replica_list(
		    current_replicas, _environment);
		_environment.clear();
	}

	//
	// If the Device Server for this device service cannot be
	// converted to the primary, we raise an exception. The
	// HA framework will then try to make another secondary
	// provider (if any) for this device service, the primary.
	//
	err = device_serverp->convert_to_primary();
	if (err != 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("become_primary(%p): convert_to_primary() failed with "
		    "error '%d' for service '%s'.\n",
		    this, err, service_name));

		DEVICE_DBPRINTF(
		    DEVICE_TRACE_DEVREPL,
		    DEVICE_RED,
		    ("device_replica_impl::become_primary:"
		    "convert_to_primary() failed with error %d, %p\n", err,
		    this));
		success = false;
		goto out;
	}

	if ((current_replicas.length() == 1) && register_if_first) {
		//
		// We are the only primary and the 'register_if_first' flag is
		// set.  Register this device server with the DCS.  See
		// comments in ::initialize().
		//
		Environment e;
		dclib::get_dcobj()->register_device_server(device_server_obj,
		    service_name, e);
		if (e.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("become_primary(%p): exception '%s' while "
			    "calling register_device_server(%s).\n",
			    this, e.exception()->_name(), service_name));

			DEVICE_DBPRINTF(
			    DEVICE_TRACE_DEVREPL,
			    DEVICE_RED,
			    ("Failed to register device server with DCS\n"));
			e.clear();
			success = false;
			goto out;
		}
	}

	ASSERT(!CORBA::is_nil(mapperobj));

out:
	if (!success) {
		_environment.exception(new replica::repl_prov_failed());
		_is_primary = false;
		primaries_lock.lock();
		ASSERT(primaries_count > 0);
		ASSERT(primaries_enabled);
		primaries_count--;
		primaries_lock.unlock();

		// Since we are returning exception, release the checkpoint ref.
		CORBA::release(_ckpt_proxy);
		_ckpt_proxy = nil;
		event_severity = CL_EVENT_SEV_CRITICAL;
		new_state = CL_DS_BECOME_PRIMARY_FAILED;
	} else {
		event_severity = CL_EVENT_SEV_INFO;
		new_state = CL_DS_BECOME_PRIMARY_ENDED;
	}

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_PRIMARIES_CHANGING,
		CL_EVENT_PUB_DCS, event_severity,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		get_service_name(),
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		old_state,
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		new_state,
		CL_REASON_CODE, DATA_TYPE_UINT32,
		become_primary_reason,
		CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		os::gethrtime(),
		CL_NODE_NAME, SE_DATA_TYPE_STRING,
		nodename,
		NULL);

	FAULTPT_DCS(FAULTNUM_DCS_DEVICE_SERVICE_BECOME_PRIMARY_S_E,
		FaultFunctions::generic);

	MC_PROBE_0(dc_become_primary_end, "clustering pxfs", "");
}

//
// Called on a secondary (or spare) to create a new device_server.
// At the end of this method, we will become a secondary so we need to
// make an upcall to user space.
//
void
device_replica_impl::ckpt_new_device_server(
    mdc::device_server_ptr primary_obj, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_new_device_server(%p).\n",
	    primary_obj));
	//
	// Secondary object has to be created.  Note that if 'device_serverp'
	// has already been initialized before, it will be cleaned up by
	// the unref() mechanism.
	//
	if (device_serverp == NULL) {
		device_serverp = new device_server_repl_impl(this, primary_obj,
		    devid);	//lint !e423 unreference will do cleanup
		device_server_obj = mdc::device_server::_duplicate(primary_obj);
	}
}

void
device_replica_impl::ckpt_dump_state(mdc::device_server_ptr primary_obj,
    const sol::string_seq_t &replicas, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_dump_state(%p, %p).\n",
	    primary_obj, replicas));

	(void) transition_notify(MAKE_SECONDARY, NULL, NULL);
	// XXX For now, ignore errors.

	// Secondary object has to be created.
	if (device_serverp == NULL) {
		device_serverp = new device_server_repl_impl(this, primary_obj,
		    devid);	//lint !e423 unreference will do cleanup
		device_server_obj = mdc::device_server::_duplicate(primary_obj);
	}

	current_replicas = replicas;
}

//
// Called during the primary->secondary transition.
//
void
device_replica_impl::become_secondary(Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::become_secondary().\n"));

	MC_PROBE_0(dc_become_secondary_start, "clustering pxfs", "");

	char *new_state, *old_state;
	cl_event_severity_t event_severity = CL_EVENT_SEV_INFO;
	char nodename[CL_MAX_LEN + 1];

	if (_is_primary) {
	    old_state = DS_STATE_PRIMARY;
	} else {
	    old_state = DS_STATE_SPARE;
	}

	clconf_get_nodename(orb_conf::node_number(), nodename);

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_PRIMARIES_CHANGING,
		CL_EVENT_PUB_DCS, event_severity,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		get_service_name(),
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		old_state,
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		CL_DS_BECOME_SECONDARY_STARTED,
		CL_REASON_CODE, DATA_TYPE_UINT32,
		CL_REASON_UNKNOWN,
		CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		os::gethrtime(),
		CL_NODE_NAME, SE_DATA_TYPE_STRING,
		nodename,
		NULL);

	//
	// Reduce the primary count.  Note that even if there is a failure in
	// becoming secondary, we do not bump the count back up because
	// the HA f/w semantics are that if the become_secondary() fails on a
	// replica, then become_primary() will be called again on that replica.
	//
	primaries_lock.lock();
	ASSERT(primaries_count > 0);
	ASSERT(primaries_enabled);
	primaries_count--;
	primaries_lock.unlock();

	int err = internal_become_secondary();
	if (err != 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("become_secondary(%p): internal_become_secondary() failed"
		    " with error '%d' for service '%s'.\n", this, err,
		    get_service_name()));
		_environment.exception(new replica::become_secondary_failed());
		new_state = CL_DS_BECOME_SECONDARY_FAILED;
		event_severity = CL_EVENT_SEV_CRITICAL;
	} else {
		new_state = CL_DS_BECOME_SECONDARY_ENDED;
		event_severity = CL_EVENT_SEV_INFO;
	}

	(void) sc_publish_event(
		ESC_CLUSTER_DCS_PRIMARIES_CHANGING,
		CL_EVENT_PUB_DCS, event_severity,
		CL_EVENT_INIT_OPERATOR, 0, 0, 0,
		CL_DS_NAME, SE_DATA_TYPE_STRING,
		get_service_name(),
		CL_OLD_STATE, SE_DATA_TYPE_STRING,
		old_state,
		CL_NEW_STATE, SE_DATA_TYPE_STRING,
		new_state,
		CL_REASON_CODE, DATA_TYPE_UINT32,
		CL_REASON_UNKNOWN,
		CL_TIMESTAMP, SE_DATA_TYPE_TIME,
		os::gethrtime(),
		CL_NODE_NAME, SE_DATA_TYPE_STRING,
		nodename,
		NULL);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

	MC_PROBE_0(dc_become_secondary_end, "clustering pxfs", "");
}

int
device_replica_impl::internal_become_secondary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::internal_become_secondary().\n"));

	ASSERT(device_serverp != NULL);
	ASSERT(!_inactive);

	device_serverp->convert_to_secondary();
	int err = transition_notify(PRIMARY_TO_SECONDARY, NULL, NULL);
	if (err != 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("internal_become_secondary(%p): transition_notify() "
		    "failed with error '%d' for service '%s'.\n",
		    this, err, get_service_name()));
		DEVICE_DBPRINTF(
		    DEVICE_TRACE_DEVREPL,
		    DEVICE_RED,
		    ("become_secondary: Error running user program!\n"));
	} else {
		_is_primary = false;
	}

	return (err);
}

//
// Called on the primary when a new secondary is being added.
//
void
device_replica_impl::add_secondary(replica::checkpoint_ptr sec_chkpt,
    const char *secondary_name, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::add_secondary(%p, %s).\n",
	    sec_chkpt, secondary_name));

	repl_dc::repl_dev_server_ckpt_var ckpt =
	    repl_dc::repl_dev_server_ckpt::_narrow(sec_chkpt);

	ASSERT(!CORBA::is_nil(ckpt));
	ASSERT(device_serverp != NULL);

	// Make an upcall to the user program.
	char *secondaries = get_nodelist(current_replicas);
	(void) transition_notify(ADD_SECONDARY, secondaries, secondary_name);
	// XXX For now, ignore errors.
	delete [] secondaries;

	// Add the replica to the list.
	add_replica(secondary_name);

	// Checkpoint the addition to all the secondaries.
	device_serverp->get_checkpoint()->ckpt_add_replica(secondary_name,
	    _environment);
	if (_environment.exception()) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("add_secondary(%p): exception '%s' while calling "
		    "ckpt_add_replica(%s).\n", this,
		    _environment.exception()->_name(), secondary_name));
		_environment.clear();
		return;
	}

	// Dump the state of this object to the new secondary.
	ckpt->ckpt_dump_state(device_server_obj, current_replicas,
	    _environment);
	if (_environment.exception()) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("add_secondary(%p): exception '%s' while calling "
		    "ckpt_dump_state().\n", this,
		    _environment.exception()->_name()));
		_environment.clear();
		return;
	}

	// Dump the state of the device server object to the new secondary.
	device_serverp->dump_state(ckpt, _environment);
	if (_environment.exception()) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("add_secondary(%p): exception '%s' while calling "
		    "dump_state().\n", this,
		    _environment.exception()->_name()));
	}
	_environment.clear();
}

//
// The primary invokes this checkpoint on the spare as a part of dumping state.
// This checkpoint updates the list of replicas on the spare.
//
void
device_replica_impl::ckpt_replica_list(const sol::string_seq_t &replicas,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_replica_list(%p).\n",
	    replicas));

	current_replicas = replicas;
}

//
// Called on the secondaries to checkpoint the addition of a replica.
//
void
device_replica_impl::ckpt_add_replica(const char *new_replica,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_add_replica(%s).\n",
	    new_replica));

	add_replica(new_replica);
}

//
// Called on the primary when a secondary is being removed.
//
// device_replica_impl(replica::repl_prov::remove_secondary)
void
device_replica_impl::remove_secondary(const char *secondary_name,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::remove_secondary(%s).\n",
	    secondary_name));

	ASSERT(device_serverp != NULL);

	char *secondaries = get_nodelist(current_replicas);
	(void) transition_notify(REMOVE_SECONDARY, secondaries,
	    secondary_name);
	// XXX For now, ignore errors
	delete [] secondaries;

	remove_replica(secondary_name);

	device_serverp->get_checkpoint()->ckpt_remove_replica(secondary_name,
	    _environment);
	if (_environment.exception()) {
		DCS_DBPRINTF_A(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("remove_secondary(%p): exception '%s' while calling "
		    "ckpt_remove_replica(%s).\n", this,
		    _environment.exception()->_name(), secondary_name));
	}
	_environment.clear();
}

//
// Called on the secondaries to checkpoint the removal of a replica.
//
void
device_replica_impl::ckpt_remove_replica(const char *old_replica,
    Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_remove_replica(%s).\n",
	    old_replica));

	remove_replica(old_replica);
}

//
// If the replica passed in is not already a part of the list of replicas, add
// it to the list.
//
void
device_replica_impl::add_replica(const char *new_replica)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::add_replica(%s).\n",
	    new_replica));

	uint_t len = current_replicas.length();
	for (uint_t i = 0; i < len; i++) {
		if (strcmp(new_replica, (const char *)current_replicas[i])
		    == 0) {
			return;
		}
	}
	current_replicas.length(len + 1);
	current_replicas[len] = new_replica;
}

//
// If the replica passed in is currently a part of the list of replicas, remove
// it from the list.
//
void
device_replica_impl::remove_replica(const char *old_replica)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::remove_replica(%s).\n",
	    old_replica));

	//
	// Copy out all the replica names except for the one we are removing
	// into 'tmp'.
	//
	uint_t len = current_replicas.length();
	CORBA::StringSeq tmp(len - 1, len - 1);
	uint_t i, j;
	j = 0;
	for (i = 0; i < len; i++) {
		const char *str = current_replicas[i];
		if (strcmp(str, old_replica) != 0) {
			ASSERT(j < (len - 1));
			tmp[j] = str;
			j++;
		}
	}

	if (j == len) {
		return;
	}

	//
	// Copy out the new list of replicas into 'current_replicas'.
	//
	current_replicas = (const CORBA::StringSeq)tmp;
}

//
// Convert to spare (i.e., the same state as after the constructor).
//
void
device_replica_impl::become_spare(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::become_spare().\n"));

	os::sc_syslog_msg msg(SC_SYSLOG_DEVICE_SERVICE_TAG, service_name, NULL);
	(void) msg.log(SC_SYSLOG_NOTICE, REMOVED, NULL);

	_is_primary = false;

	if (device_serverp != NULL) {
		// Drop our reference to the object before doing a delete.
		device_server_obj = mdc::device_server::_nil();
		device_serverp->convert_to_spare();
		device_serverp = NULL;	//lint !e423 unreference does cleanup
	}
	current_replicas.length(0);
}

void
device_replica_impl::freeze_primary_prepare(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::freeze_primary_prepare().\n"));
}

//
// Note: either add_secondary() and unfreeze_primary() or
// become_secondary() will be called after this returns.
// Invocations and calls to _unreferenced() will be blocked by the HA
// framework after we return from here.
//
void
device_replica_impl::freeze_primary(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::freeze_primary()\n."));
}

void
device_replica_impl::unfreeze_primary(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::unfreeze_primary().\n"));
}

//
// Call made on the primary to shut down this device service.  We check to
// see if the device service is busy and if not, we shut it down.
//
void
device_replica_impl::shutdown(Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::shutdown().\n"));

	ASSERT(device_serverp != NULL);

	if (!device_serverp->mark_shutting_down()) {
		DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
		    ("shutdown(%p): mark_shutting_down() failed.\n", this));
		_environment.exception(new replica::service_busy());
		return;
	}

	//
	// Note that once we have called 'mark_shutting_down' and it has
	// returned true, the device service does not take any new requests,
	// so if we decide to back out of this, we need to reset the state of
	// the device service so that it does take new requests.  Calling
	// 'convert_to_primary' resets that state.
	//

	//
	// Make sure the user program is invoked so that the
	// appropriate clean up gets done.
	//
	int err = internal_become_secondary();
	if (err != 0) {
		// Convert the device service back to primary form.
		err = device_serverp->convert_to_primary();
		if (err != 0) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("shutdown(%p):  convert_to_primary() failed with "
			    "error '%d' for service '%s'.\n", this, err,
			    service_name));
		}
		_environment.exception(new replica::service_busy());
		return;
	}

	// Reduce the number of primaries on this node.
	primaries_lock.lock();
	ASSERT(primaries_count > 0);
	ASSERT(primaries_enabled);
	primaries_count--;
	primaries_lock.unlock();

	FAULTPT_DCS(FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_O,
	    FaultFunctions::generic);

	os::sc_syslog_msg msg(SC_SYSLOG_DEVICE_SERVICE_TAG, service_name, NULL);
	(void) msg.log(SC_SYSLOG_NOTICE, STATE_CHANGED, NULL);
	device_serverp = NULL;		//lint !e423 unreference does cleanup
	device_server_obj = mdc::device_server::_nil();

	// Release the reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

//
// This method is called on the current primary by the framework.
//
CORBA::Object_ptr
device_replica_impl::get_root_obj(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::get_root_obj().\n"));

	ASSERT(device_serverp != NULL);
	return (device_serverp->get_objref());
}

void
device_replica_impl::ckpt_new_io_object(fs::fobj_ptr io_obj,
    const fs::pvnode &realvp, solobj::cred_ptr credobj, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_new_io_object(%p, %p, %p).\n",
	    io_obj, realvp, credobj));

	ASSERT(device_serverp != NULL);

	device_serverp->ckpt_new_io_object(io_obj, realvp, credobj);
}

void
device_replica_impl::ckpt_new_io_object_v1(pxfs_v1::fobj_ptr io_obj,
    const pxfs_v1::pvnode &realvp, solobj::cred_ptr credobj, Environment &)
{
	ASSERT(device_serverp != NULL);

	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_new_io_object_v1(%p, %p, %p).\n",
	    io_obj, realvp, credobj));

	device_serverp->ckpt_new_io_object_v1(io_obj, realvp, credobj);
}

void
device_replica_impl::ckpt_open(fs::io_ptr io_obj, int32_t open_flags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	CL_PANIC(0);
}

void
device_replica_impl::ckpt_open_v1(pxfs_v1::io_ptr io_obj, int32_t open_flags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_open_v1(%p, %d, %p).\n",
	    io_obj, open_flags, credobj));


	io_repl_impl		*io_replp =
	    (io_repl_impl *) fobj_ii::get_fobj_ii(io_obj);

	io_replp->ckpt_open(open_flags, credobj, _environment);
}

void
device_replica_impl::ckpt_close(fs::io_ptr io_obj, int32_t close_flags,
    solobj::cred_ptr credobj, sol::error_t err, Environment &_environment)
{
	CL_PANIC(0);
}

void
device_replica_impl::ckpt_close_v1(pxfs_v1::io_ptr io_obj, int32_t close_flags,
    solobj::cred_ptr credobj, sol::error_t err, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_close_v1(%p, %d, %p, %d).\n",
	    io_obj, close_flags, credobj, err));

	io_repl_impl		*io_replp =
	    (io_repl_impl *) fobj_ii::get_fobj_ii(io_obj);

	io_replp->ckpt_close(close_flags, credobj, err, _environment);
}

void
device_replica_impl::ckpt_ioctl_begin(Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_ioctl_begin().\n"));

	io_repl_impl::ckpt_ioctl_begin(_environment);
}

void
device_replica_impl::ckpt_ioctl_end(sol::error_t err, int32_t result,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_ioctl_end(%d, %d).\n",
	    err, result));

	io_repl_impl::ckpt_ioctl_end(err, result,
	    _environment);
}

void
device_replica_impl::ckpt_locks(fs::fobj_ptr obj,
    const repl_pxfs::lock_info_seq_t &locks, Environment &)
{
	CL_PANIC(0);
}

void
device_replica_impl::ckpt_locks_v1(pxfs_v1::fobj_ptr obj,
    const repl_pxfs_v1::lock_info_seq_t &locks, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::ckpt_locks_v1(%p, %p).\n",
	    obj, locks));

	io_repl_impl		*io_replp =
	    (io_repl_impl *) fobj_ii::get_fobj_ii(obj);

	io_replp->ckpt_locks(locks);
}

repl_dc::repl_dev_server_ckpt_ptr
device_replica_impl::get_checkpoint_repl_dc()
{
	return (_ckpt_proxy);
}

//
// If this spare provider is being shutdown as part of service cleanup,
// then the "active" flag that the DCS maintains in the dc_service object,
// is set to false. This makes sure when the service is restarted, all
// providers including the spares, become a part of the service.
//
void
device_replica_impl::shutdown_spare(replica::repl_prov_shutdown_type
		shutdown_type, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DC_DEVICE_REPLICA,
	    ("device_replica_impl::shutdown_spare(%d).\n", shutdown_type));

	CORBA::Object_var obj;
	Environment e, env;

	//
	// Check if this is part of a service cleanup. We do not check
	// for any other types of spare shutdown (as a result of service
	// shutdown or provider removal) because we don't need to do
	// anything in those cases.
	//
	if (shutdown_type == replica::SERVICE_CLEANUP) {
		obj = ns::wait_resolve("DCS", e);
		if (e.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("shutdown_spare(%p): exception '%s' "
			    "while calling "
			    "ns::wait_resolve(DCS).\n",
			    this, e.exception()->_name()));
			e.clear();
			return;
		} else if (CORBA::is_nil(obj)) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("shutdown_spare(%p): "
			    "CORBA::is_nil(obj).\n", this));
			e.clear();
			return;
		}

		mdc::dc_config_var dcobj = mdc::dc_config::_narrow(obj);

		dcobj->set_service_inactive(service_name, env);

		//
		// The only exception we can get is for an invalid service
		// name when the DCS cannot find the dc_service object for
		// this service. The service no longer exists, so we
		// ignore the exception.
		//
		if (env.exception()) {
			DCS_DBPRINTF_R(DCS_TRACE_DC_DEVICE_REPLICA,
			    ("shutdown_spare(%p): exception '%s' while calling "
			    "set_service_inactive(%s).\n", this,
			    env.exception()->_name(), service_name));
		    env.clear();
		}
	}
}
