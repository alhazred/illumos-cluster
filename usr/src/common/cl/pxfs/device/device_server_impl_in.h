//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_DEVICE_SERVER_IMPL_IN_H
#define	_DEVICE_SERVER_IMPL_IN_H

#pragma ident	"@(#)device_server_impl_in.h	1.11	08/05/20 SMI"

//
// device_server_ii inline methods
//

inline
device_server_ii::~device_server_ii()
{
}

inline
device_server_norm_impl::~device_server_norm_impl()
{
}

inline
device_server_repl_impl::~device_server_repl_impl()
{
}

inline bool
device_server_ii::is_replicated() const
{
	return (serverp != NULL ? true : false);
}

inline device_replica_impl *
device_server_ii::get_serverp() const
{
	return (serverp);
}

inline pxfs_v1::fobj_ptr
device_server_ii::find_fobj(vnode_t *vp, const pxfs_v1::pvnode *real_pvnode,
    solobj::cred_ptr credobj, pxfs_v1::fobj_info &fobjinfo, Environment &env)
{
	return (device_serv.find_fobj(vp, real_pvnode, credobj,
	    fobjinfo, env));
}

// static
inline vnode_t *
device_server_ii::make_specvp(const pxfs_v1::pvnode &real_pvnode,
    cred_t *credp)
{
	return (device_server::make_specvp(real_pvnode,
	    credp));
}

inline void
device_server_ii::ckpt_new_io_object(fs::fobj_ptr io_obj,
    const fs::pvnode &real_pvnode, solobj::cred_ptr credobj)
{
	CL_PANIC(0);
}

inline void
device_server_ii::ckpt_new_io_object_v1(pxfs_v1::fobj_ptr io_obj,
    const pxfs_v1::pvnode &real_pvnode, solobj::cred_ptr credobj)
{
	device_serv.ckpt_new_io_object(io_obj, real_pvnode, credobj);
}

//
// device_server_norm_impl inline methods
//

inline repl_dc::repl_dev_server_ckpt_ptr
device_server_norm_impl::get_ckpt()
{
	ASSERT(0);
	return (repl_dc::repl_dev_server_ckpt::_nil());
}

inline void
device_server_norm_impl::get_open_device_fobj(const fs::pvnode &real_pvnode,
    int32_t flags,
    solobj::cred_ptr credobj,
    fs::fobj_out specfobj,
    fs::fobj_info &fobjinfo,
    Environment &_environment)
{
	CL_PANIC(0);
}

inline void
device_server_norm_impl::get_open_device_fobj_v1(
    const pxfs_v1::pvnode &real_pvnode,
    int32_t flags,
    solobj::cred_ptr credobj,
    pxfs_v1::fobj_out specfobj,
    pxfs_v1::fobj_info &fobjinfo,
    Environment &_environment)
{
	device_server_ii::device_serv.get_open_device_fobj_v1(
	    real_pvnode, flags, credobj, specfobj, fobjinfo, _environment);
}

inline void
device_server_norm_impl::get_open_device_fobj_v2(
    const pxfs_v1::pvnode &real_pvnode,
    int32_t flags,
    solobj::cred_ptr credobj,
    pxfs_v1::fobj_out specfobj,
    pxfs_v1::fobj_info &fobjinfo,
    Environment &_environment)
{
	device_server_ii::device_serv.get_open_device_fobj_v2(
	    real_pvnode, flags, credobj, specfobj, fobjinfo, _environment);
}

//
// device_server_repl_impl inline methods
//

inline repl_dc::repl_dev_server_ckpt_ptr
device_server_repl_impl::get_ckpt()
{
	return ((device_replica_impl*)(get_provider()))->
	    get_checkpoint_repl_dc();
}

inline void
device_server_repl_impl::get_open_device_fobj(const fs::pvnode &real_pvnode,
    int32_t flags,
    solobj::cred_ptr credobj,
    fs::fobj_out specfobj,
    fs::fobj_info &fobjinfo,
    Environment &_environment)
{
	CL_PANIC(0);
}

inline void
device_server_repl_impl::get_open_device_fobj_v1(
    const pxfs_v1::pvnode &real_pvnode,
    int32_t flags,
    solobj::cred_ptr credobj,
    pxfs_v1::fobj_out specfobj,
    pxfs_v1::fobj_info &fobjinfo,
    Environment &_environment)
{
	device_server_ii::device_serv.get_open_device_fobj_v1(
	    real_pvnode, flags, credobj, specfobj, fobjinfo, _environment);
}

inline void
device_server_repl_impl::get_open_device_fobj_v2(
    const pxfs_v1::pvnode &real_pvnode,
    int32_t flags,
    solobj::cred_ptr credobj,
    pxfs_v1::fobj_out specfobj,
    pxfs_v1::fobj_info &fobjinfo,
    Environment &_environment)
{
	device_server_ii::device_serv.get_open_device_fobj_v2(
	    real_pvnode, flags, credobj, specfobj, fobjinfo, _environment);
}

#endif	/* _DEVICE_SERVER_IMPL_IN_H */
