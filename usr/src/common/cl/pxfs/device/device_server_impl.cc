//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)device_server_impl.cc	1.76	08/05/20 SMI"

#include <sys/sysmacros.h>

#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>
#include <orb/infrastructure/orb_conf.h>

#include <pxfs/common/pxfslib.h>
#include <pxfs/device/device_server_impl.h>
#include <pxfs/device/device_debug.h>

#include <dc/sys/dc_debug.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//
// Constructor used for non-replicated device servers.
//
device_server_ii::device_server_ii()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_ii::device_server_ii().\n"));

	devid = 0;

	serverp = NULL;
	shutting_down = false;

	device_serv.initialize(this);
}

//
// Constructor used for replicated device servers.
//
device_server_ii::device_server_ii(unsigned int dev_id,
    device_replica_impl *srvrp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_ii::device_server_ii(%d, %p).\n",
	    dev_id, srvrp));

	devid = dev_id;
	serverp = srvrp;
	shutting_down = false;

	device_serv.initialize(this);
}

//
// Constructor for the unreplicated object.
// DCS will create one of these when instantiating a non-replicated device
// service.
//
device_server_norm_impl::device_server_norm_impl() :
	McServerof<mdc::device_server>()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_norm_impl::device_server_norm_impl().\n"));
}

//
// Constructor for the primary replicated object.
//
device_server_repl_impl::device_server_repl_impl(device_replica_impl *svrp,
    unsigned int dev_id) :
	device_server_ii(dev_id, svrp),
	mc_replica_of<mdc::device_server>(svrp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::device_server_repl_impl(%p, %d).\n",
	    svrp, dev_id));
}

//
// Constructor for the secondary replicated object.
//
device_server_repl_impl::device_server_repl_impl(device_replica_impl *svrp,
    mdc::device_server_ptr obj, unsigned int dev_id) :
	device_server_ii(dev_id, svrp),
	mc_replica_of<mdc::device_server>(obj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::device_server_repl_impl(%p, %p, %d).\n",
	    svrp, obj, dev_id));

	device_serv.set_not_primary();
}

//
// We get _unreferenced() when the device service object is released.
//
void
device_server_norm_impl::_unreferenced(unref_t arg)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_norm_impl::_unreferenced().\n"));

	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	delete this;
}

//
// We get _unreferenced() when we shut down the service.
//
void
device_server_repl_impl::_unreferenced(unref_t arg)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::_unreferenced().\n"));

	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	delete this;
}

//
// Return a reference of the highest commited version.
// This is called only during an upgrade callback.
//
void
device_server_norm_impl::_generic_method(CORBA::octet_seq_t &,
    CORBA::object_seq_t &objs, Environment &)
{
	objs[0] = get_objref();
}

//
// Return a reference of the highest commited version.
// This is called only during an upgrade callback.
//
void
device_server_repl_impl::_generic_method(CORBA::octet_seq_t &,
    CORBA::object_seq_t &objs, Environment &)
{
	objs[0] = get_objref();
}

//
// This function is called from HA service shutdown,
// and returns false if this device
// service has any open references to io objects.  Note that once this
// function returns true, the device service does not take any new requests,
// so if we decide to back out of the shutdown, we need to reset the state of
// the device service so that it does take new requests.  Calling
// 'convert_to_primary' resets that state.
//
bool
device_server_ii::mark_shutting_down()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_ii::mark_shutting_down().\n"));

	//
	// Don't allow the creation of any device file objects
	// during this check.
	//
	busy_lock.wrlock();

	if (!device_serv.is_empty()) {
		busy_lock.unlock();
		return (false);
	}

	//
	// Mark the service as busy, so new requests will retry after a while.
	// See pxspecial::open() for the retry code.
	//
	shutting_down = true;
	busy_lock.unlock();

	return (true);
}

//
// Convert this device_server object to primary.
// Convert all io objects to primary.
//
int
device_server_repl_impl::convert_to_primary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::convert_to_primary().\n"));

	int	error = 0;

	shutting_down = false;

	//
	// Convert all device fobjs from secondary form to primary form.
	//
	if ((error = device_serv.convert_to_primary()) == 0) {
			//
			// Want consistency between the two versions
			// about whether this is a primary.
			//
			device_serv.set_not_primary();
	}
	return (error);
}

//
// Convert this device_server replica to a secondary.
// Convert all io objects to secondary.
//
void
device_server_repl_impl::convert_to_secondary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::convert_to_secondary().\n"));

	device_serv.convert_to_secondary();
}

//
// Convert this device_server replica to a spare.
// Convert all io objects to spare.
// This is called after the service is frozen and all IDL invocations
// have completed (new IDL invocations and _unreferenced() are blocked).
//
void
device_server_repl_impl::convert_to_spare()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::convert_to_spare().\n"));

	device_serv.convert_to_spare();
	delete this;
}

//
// This function dumps the state of this device_server_repl_impl object to
// the given checkpoint.  This function is called from
// device_replica_impl::add_secondary().
//
void
device_server_repl_impl::dump_state(repl_dc::repl_dev_server_ckpt_ptr ckpt,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server_repl_impl::dump_state(%p).\n", ckpt));

	device_serv.dump_state(ckpt, env);
}
