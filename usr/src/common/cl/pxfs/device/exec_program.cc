//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)exec_program.cc	1.6	08/05/20 SMI"

#include <pxfs/device/exec_program.h>
#include <orb/infrastructure/orbthreads.h>
#include <sys/clconf_int.h>
#include <sys/cladm_debug.h>
#include <nslib/ns.h>
#include <h/repl_pxfs.h>
#include <pxfs/common/pxfslib.h>

// Five-second sleeps.
os::usec_t	exec_program::sleep_in_seconds = 5;

// Try a maximum of 12 times (i.e. for 1 minute)
uint_t		exec_program::max_retry_iterations = 12;

// Warn the user every 2 sleeps (i.e. 10 seconds)
uint_t		exec_program::warning_interval = 2;

//
// Returns true if 'id' is a current member of the cluster, false if not.
//
bool
exec_program::is_cluster_member(nodeid_t id)
{
	quorum_status_t *clust_memb;
	bool state = false;

	if (cluster_get_quorum_status(&clust_memb) != 0) {
		return (false);
	}
	for (uint32_t i = 0; i < clust_memb->num_nodes; i++) {
		if (clust_memb->nodelist[i].nid == id &&
		    clust_memb->nodelist[i].state == QUORUM_STATE_ONLINE) {
			state = true;
			break;
		}
	}
	cluster_release_quorum_status(clust_memb);
	return (state);
}

//
// Execute the user program 'program' on the node 'id'.  Keep in mind that
// this routine provides 'at-least-once' semantics, so the user program must
// be idempotent.
//
int
exec_program::execute_program(const char *program, sol::nodeid_t id,
    bool different_thread, os::sc_syslog_msg *msg_handle, Environment &env)
{
	if (!different_thread) {
		return (do_execution(program, id, msg_handle, env));
	}

	os::condvar_t cv;
	os::mutex_t lock;
	struct execution_data data;

	data.program = program;
	data.id = id;
	data.msg_handle = msg_handle;
	data.env = &env;
	data.cv_to_wakeup = &cv;
	data.lock = &lock;

	lock.lock();
	common_threadpool::the().defer_processing(
	    new work_task(execution_worker_thread, &data));
	cv.wait(&lock);
	lock.unlock();

	return (data.return_code);
}

//
// Function used by 'execute_program' to perform the execution of the program
// if 'different_thread' is set to true.
//
void
exec_program::execution_worker_thread(void *_data)
{
	struct execution_data	*data = (struct execution_data *)_data;

	(data->lock)->lock();
	data->return_code = do_execution(data->program, data->id,
	    data->msg_handle, *(data->env));
	(data->cv_to_wakeup)->signal();
	(data->lock)->unlock();
}

//
// Helper function called by 'execute_program' or 'execution_worker_thread' to
// do the actual execution.
//
int
exec_program::do_execution(const char *program, sol::nodeid_t id,
    os::sc_syslog_msg *msg_handle, Environment &env)
{
	CORBA::Object_var	obj;
	CORBA::Exception	*ex;
	char			name[20];
	naming::naming_context_var	ctxp = ns::root_nameserver();
	os::sprintf(name, "ha_mounter.%d", id);
	uint_t			i;

	for (i = 0; i < max_retry_iterations; i++) {
		//
		// If the node is not a current member of the cluster, return
		// an error.
		//
		if (!is_cluster_member(id)) {
			return (1);
		}

		if (i != 0) {
			env.clear();
			// Every 'warning_interval' times, print out a warning.
			if ((i % warning_interval) == 0) {
				//
				// SCMSGS
				// @explanation
				// Could not find clexecd to execute the
				// program on a node. Indicated retry times.
				// @user_action
				// This is an informational message, no user
				// action is needed.
				//
				(void) msg_handle->log(SC_SYSLOG_WARNING,
				    MESSAGE,
				    "Not found clexecd on node %d for %d "
				    "seconds.  Retrying...", id,
				    i * (uint_t)sleep_in_seconds);
				CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
				    ("do_execution:%s not found %s\n",
				    name, program));
			}
			os::usecsleep(sleep_in_seconds * 1000000);
		}

		// Get the object from the nameserver.
		obj = ctxp->resolve(name, env);
		if ((ex = env.exception()) != NULL) {
			CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
			    ("do_execution:%s resolve pbm\n", name));
			if (naming::not_found::_exnarrow(ex) != NULL) {
				CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
				    ("do_execution:%s resolve narrw\n",
				    name));
			}
			//
			// Lookup in the nameserver failed.
			// Try again.
			//
			continue;
		}

		// Narrow the object down to type 'ha_mounter'.
		repl_pxfs::ha_mounter_var	mounter =
		    repl_pxfs::ha_mounter::_narrow(obj);
		if (CORBA::is_nil(mounter)) {
			//
			// Some other object has bound as ha_mounter!!
			// Try again.
			//
			CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
			    ("do_execution:%s nil mounter\n", name));
			continue;
		}
		//
		// Execute the user program, specifying scheduling
		// class as RT.
		//
		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_GREEN,
		    ("do_execution:%s exec %s\n",
		    name, program));

		mounter->exec_program_with_opts(program, true, true,
		    true, env);

		CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_GREEN,
		    ("do_execution:%s exec %s excep %p\n",
		    name, program, env.exception()));

		if (((ex = env.exception()) != NULL) &&
		    (CORBA::SystemException::_exnarrow(ex) != NULL)) {
			CLEXEC_EXCEPTION(env, "do_execution", name);
			env.clear();
			//
			// The ha_mounter object died between the time
			// we got a reference to it, and the time we
			// called 'exec_program' on it.
			// Try again.
			//
			continue;
		}

		return (0);
	}

	ASSERT(i >= max_retry_iterations);

	// We've tried too many times.
	env.clear();
	pxfslib::throw_exception(env, EAGAIN);

	//
	// SCMSGS
	// @explanation
	// Could not find clexecd to execute the program on a node. Indicated
	// giving up after retries.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) msg_handle->log(SC_SYSLOG_WARNING, MESSAGE,
	    "Not found clexecd on node %d for %d seconds.  Giving up!",
	    id, i * 5);

	return (0);
}
