//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)device_service_mgr.cc	1.72	08/05/20 SMI"

#include <sys/os.h>
#include <sys/rm_util.h>
#include <sys/cladm_debug.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>
#include <dc/sys/dc_lib.h>
#include <pxfs/common/pxfslib.h>
#include <pxfs/device/device_service_mgr.h>
#include <pxfs/device/device_server_impl.h>
#include <pxfs/device/device_replica_impl.h>
#include <pxfs/device/device_debug.h>
#include <dc/sys/dc_debug.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

#define	BUS_RESET_PROGRAM "/usr/cluster/lib/sc/run_reserve -c reset_shared_bus"
#define	NODE_FENCE_PROGRAM	\
	"/usr/cluster/lib/sc/run_reserve -c fence_node_ng -f "
#define	RELEASE_SCSI2_PROGRAM	\
	"/usr/cluster/lib/sc/run_reserve -c release_shared_scsi2 -n "
#define	FENCE_LOCK_RETRIES 2	// number of times to try for fencing lock

//
// Table for defining any new service classes that need to
// be added during boot time
//
struct service_class {
	char *service_name;
	char *user_program;
	mdc::ha_dev_type ha_type;
};
static struct service_class class_table[] = {
	{ "SUNWlocal", "/bin/true", mdc::EIO_FAILOVER },	// Multi-owner
	{ NULL, NULL, mdc::EIO_FAILOVER }
};

// Initialize static variables.
device_service_mgr *device_service_mgr::the_dsm = NULL;
threadpool device_service_mgr::dsm_reconfig_threadpool(true, 1,
	"DSM reconfig threadpool");
threadpool device_service_mgr::dsm_release_scsi2_threadpool(true, 1,
	"DSM releasescsi2 threadpool");

extern "C" void
dsm_cmm_callback()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dsm_cmm_callback().\n"));

	device_service_mgr::cmm_callback();
}

extern "C" void
dsm_fence_nodes(const char *node)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dsm_fence_nodes(%s).\n", node));

	device_service_mgr::fence_nodes(node);
}

extern "C" void
dsm_release_scsi2(const char *node)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dsm_release_scsi2(%s).\n", node));
	device_service_mgr::release_scsi2(node);
}

extern "C" void
dsm_wait_for_release()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dsm_wait_for_release().\n"));
	device_service_mgr::wait_for_release();
}


extern "C" void
dsm_acquire_fence_lock(const char *node)
{
	device_service_mgr::acquire_fence_lock(node);
}

//
// Create any new service classes if they don't already exist.
// This is done only during DSM activation which occurs during
// the booting of the node
//
int
device_service_mgr::create_service_classes()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::create_service_classes().\n"));

	Environment e;
	CORBA::Exception *ex;
	mdc::dcs_error *ex_dcs = NULL;
	os::sc_syslog_msg msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG, "DSM", NULL);
	struct service_class *p;
	dc_error_t error = DCS_SUCCESS;

	for (p = class_table; p->service_name; p++) {

		//
		// Only add the service if the service does not exists
		//
		dclib::get_dcobj()->create_service_class(p->service_name,
		    p->user_program, p->ha_type, e);

		if ((ex = e.exception()) != NULL) {
			if ((ex_dcs = mdc::dcs_error::_exnarrow(ex)) != NULL) {
				error = (dc_error_t)ex_dcs->error;
			} else {
				DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("create_service_classes(): exception '%s'"
				    " while calling "
				    "create_service_class(%s, %s, %d).\n",
				    ex->_name(), p->service_name,
				    p->user_program, p->ha_type));
				error = DCS_ERR_COMM;
			}
			e.clear();

			//
			// If the service exists just continue.
			// Otherwise, log an error message.
			//
			if (error == DCS_ERR_SERVICE_CLASS_NAME) {
				DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("create_service_classes(): service class "
				    "'%s' already exists.\n",
				    p->service_name));
			} else {
				DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("create_service_classes(): "
				    "create_service_class(%s, %s, %d) "
				    "failed with error '%d'.\n",
				    p->service_name, p->user_program,
				    p->ha_type, error));

				//
				// SCMSGS
				// @explanation
				// The specified entry could not be added to
				// the dcs_service_classes table.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available.
				//
				(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Unable to create %s service class",
				    p->service_name);
				return (EINVAL);
			}
		}
	}

	return (0);
}

//
// This routine is called from the CL_GBLMNT_ENABLE system call.
// The first time it is called, we need to initialize the
// DSM, and start up the required device services replicas.
//
int
device_service_mgr::activate()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::activate().\n"));

	extern void (*dsm_cmm_callback_ptr)(void);
	extern void (*dsm_fence_nodes_ptr)(const char *);
	extern void (*dsm_release_scsi2_ptr)(const char *);
	extern void (*dsm_wait_for_release_ptr)(void);
	extern void (*dsm_acquire_fence_lock_ptr)(const char *);
	dsm_cmm_callback_ptr = dsm_cmm_callback;
	dsm_fence_nodes_ptr = dsm_fence_nodes;
	dsm_release_scsi2_ptr = dsm_release_scsi2;
	dsm_wait_for_release_ptr = dsm_wait_for_release;
	dsm_acquire_fence_lock_ptr = dsm_acquire_fence_lock;

	//
	// Make the callback once in case we missed a reconfig. racing with
	// a 'clconfig -g'.
	//
	dsm_cmm_callback();

	int error = 0;

	// Create the DSM and register it with the name server and the DCS.
	if (the_dsm == NULL) {
		//
		// Use a lock on the outside chance that there are two threads
		// racing to initialize the DSM.
		//
		static os::mutex_t _lock;

		_lock.lock();
		if (the_dsm != NULL) {
			_lock.unlock();
			return (0);
		}

		//
		// Create any new services that aren't already
		// registered in the CCR. Return any errors.
		//
		error = create_service_classes();
		if (error != 0) {
			_lock.unlock();
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("activate(): create_service_classes() failed "
			    "with error '%d'.\n", error));
			return (error);
		}

		the_dsm = new device_service_mgr();
		error = the_dsm->register_with_dcs();
		if (error != 0) {
			//
			// The object we just allocated will be freed up by
			// _unreferenced.
			//
			the_dsm = NULL;
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("activate(): register_with_dcs() failed "
			    "with error '%d'.\n", error));
		}

		_lock.unlock();
	}

	return (error);
}

//
// This routine is called from the CL_SWITCHBACK_ENABLE system call, to do
// required switchbacks on all the device services we started up via
// 'dcs_activate'.
//
int
device_service_mgr::do_switchbacks()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::do_switchbacks().\n"));

	//
	// 'the_dsm' will be NULL if 'clconfig -s' is called before
	// 'clconfig -g'.
	//
	if (the_dsm == NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("do_switchbacks(): pointer to the dsm is NULL.\n"));
		return (EINVAL);
	}
	the_dsm->do_switchbacks_internal();

	return (0);
}

//
// Constructor for 'replica_startup_params'.
//
device_service_mgr::replica_startup_params::replica_startup_params(
    uint32_t _dev_id, const char *_service_name, const char *_user_pgm,
    uint32_t _priority, uint_t num_secondaries, mdc::dc_mapper_ptr _mapper,
    bool _register_if_first, uint_t *_service_cnt, os::mutex_t
	*_service_cnt_lock, os::condvar_t *_service_cnt_cv)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::replica_startup_params:"
	    ":replica_startup_params(%d, %s, %s, %d, %d, %p, %d, %p).\n",
	    _dev_id, _service_name, _user_pgm, _priority,
	    num_secondaries, _mapper, _register_if_first, _service_cnt));

	dev_id = _dev_id;
	service_name = _service_name;
	user_pgm = _user_pgm;
	priority = _priority;
	num_of_secondaries = num_secondaries;
	mapper = _mapper;
	register_if_first = _register_if_first;
	service_cnt = _service_cnt;
	service_cnt_lock = _service_cnt_lock;
	service_cnt_cv = _service_cnt_cv;
}

//
// Destructor for 'replica_startup_params'.
//
device_service_mgr::replica_startup_params::~replica_startup_params()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::replica_startup_params:"
	    ":~replica_startup_params().\n"));

	// Do nothing.
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// Constructor for 'switchback_params'.
//
device_service_mgr::switchback_params::switchback_params(
	const char *_service_name, uint_t *_service_cnt,
	os::mutex_t *_service_cnt_lock, CORBA::StringSeq *_all_services)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::switchback_params:"
	    ":switchback_params(%s, %d).\n",
	    _service_name, _service_cnt));

	service_name = _service_name;
	service_cnt = _service_cnt;
	service_cnt_lock = _service_cnt_lock;
	all_services = _all_services;
}

//
// Destructor for 'switchback_params'.
//
device_service_mgr::switchback_params::~switchback_params()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dsm_cmm_callback().\n"));
	// Do nothing.
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// Constructor.
//
device_service_mgr::device_service_mgr()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::switchback_params::~switchback_params().\n"));

	services_to_switchback = NULL;
	_starting_up = true;
}

//
// Destructor.  Nothing to do.
//
device_service_mgr::~device_service_mgr()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::~device_service_mgr().\n"));
}

//
// _unreferenced() called on this object because a previous 'register_with_dcs'
// failed.
//
void
device_service_mgr::_unreferenced(unref_t arg)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::_unreferenced().\n"));

	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	delete this;
}

//
// This call registers this DSM with the DCS.  When a DSM registers with the
// DCS, the DCS will send the DCS messages to start up device services that can
// be hosted on this node.  When the DSM attempts to start up these device
// services, the "ha_mounter" object needs to be available to execute user
// programs.  So, before registering with the DCS, we need to make
// sure that the "ha_mounter" object is registered with the name server.
//
int
device_service_mgr::register_with_dcs()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::register_with_dcs().\n"));

	mdc::device_service_manager_var objv = get_objref();
	mdc::device_service_info_seq_var service_info;
	Environment env;
	os::sc_syslog_msg msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG, "Client", NULL);

	//
	// Bind the DSM object with the name server.
	//
	char dsm_name[20];
	(void) sprintf(dsm_name, "DSM.%d", orb_conf::node_number());
	naming::naming_context_var ctxv = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));
	ctxv->rebind(dsm_name, objv, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("register_with_dcs(%p): exception '%s' while calling "
		    "rebind(%s).\n", this, env.exception()->_name(),
		    dsm_name));
		//
		// SCMSGS
		// @explanation
		// There was a fatal error while this node was booting.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE, "Could not start up "
		    "DCS client because we could not contact the name server.");
		env.clear();
		return (EINVAL);
	}

	//
	// We create a local device server object here and pass it in to
	// the DCS server.  All access to suspended device services are
	// directed to the local node via this device server object.
	//
	device_server_norm_impl *dev_server =
	    new device_server_norm_impl();
	mdc::device_server_var dev_server_obj = dev_server->get_objref();

	// Register with the device configurator object.  Pass in the node id.
	dclib::get_dcobj()->register_dsm(objv, dev_server_obj,
	    orb_conf::node_number(), service_info, env);
	if (env.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("register_with_dcs(%p): exception '%s' while calling "
		    "register_dsm().\n", this, env.exception()->_name()));
		//
		// SCMSGS
		// @explanation
		// Some drivers identified in previous messages do not have
		// the same major number across cluster nodes, and devices
		// owned by the driver are being used in global device
		// services.
		// @user_action
		// Look in the /etc/name_to_major file on each cluster node to
		// see if the major number for the driver matches across the
		// cluster. If a driver is missing from the /etc/name_to_major
		// file on some of the nodes, then most likely, the package
		// the driver ships in was not installed successfully on all
		// nodes. If this is the case, install that package on the
		// nodes that don't have it. If the driver exists on all nodes
		// but has different major numbers, see the documentation that
		// shipped with this product for ways to correct this problem.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE, "Could not start up "
		    "DCS client because major numbers on this node do not "
		    "match the ones on other nodes.  See /var/adm/messages "
		    "for previous errors.");
		env.clear();
		return (EINVAL);
	}

	uint_t services_count = service_info.length();
	if (services_count == 0) {
		//
		// No replicas to startup.
		//
		services_to_switchback = NULL;
		_starting_up = false;
		DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("register_with_dcs(%p): no service to start up.\n",
		    this));
		return (0);
	}

	os::mutex_t wait_for_replicas_lock;
	os::condvar_t wait_for_replicas_cv;

	services_to_switchback = new CORBA::StringSeq(service_info.length());
	uint_t services_to_switchback_cnt = 0;
	device_service_mgr::replica_startup_params *params;

	//
	// Start device_service replicas for the devices that are active
	// and attached to this node.
	//
	for (uint_t i = 0; i < service_info.length(); i++) {

		Environment _environment;
		char *tmp_secondaries;
		uint_t num_secondaries = 0;

		dclib::get_dcobj()->get_property(
			service_info[i].service_name,
			"DCS_NumberOfSecondaries", tmp_secondaries,
			_environment);
		if (_environment.exception()) {
			//
			// If we could not get the num secondaries from the
			// the CCR, we set it to its default which is
			// represented by number 0.
			//
			DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("register_with_dcs(%p): exception '%s' while "
			    "retrieving the number of secondaries for "
			    "service '%s' from the CCR. Forcing to "
			    "default number.\n", this,
			    _environment.exception()->_name(),
			    service_info[i].service_name));
			num_secondaries = 0;
			_environment.clear();
		} else {
			num_secondaries = (uint_t)os::atoi(tmp_secondaries);
			DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("register_with_dcs(%p): number of secondaries "
			    "for service '%s' is %d.\n", this,
			    service_info[i].service_name,
			    num_secondaries));
		}

		//
		// The memory allocated for 'params' is freed by the
		// 'start_service_worker_thread' function.
		//
		params = new device_service_mgr::replica_startup_params(
		    service_info[i].dev_id, service_info[i].service_name,
		    service_info[i].user_pgm, service_info[i].priority,
		    num_secondaries, service_info[i].mapper, true,
		    &services_count, &wait_for_replicas_lock,
		    &wait_for_replicas_cv);
		common_threadpool::the().defer_processing(new work_task(
		    start_service_worker_thread, (void *)params));

		//
		// We can't do the switchback here because we have not yet
		// started up PXFS server replicas that depend on this replica.
		// Here, just store the name of the service, and then do the
		// actual switchback in 'do_switchbacks_internal'.
		//
		if (service_info[i].switchback_enabled) {
			DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("register_with_dcs(%p): adding service '%s' to "
			    "switchback list.\n", this,
			    service_info[i].service_name));
			(*services_to_switchback)[services_to_switchback_cnt] =
			    (const char *)(service_info[i].service_name);
			services_to_switchback_cnt++;
			services_to_switchback->length(
			    services_to_switchback_cnt);
		}
	}

	//
	// Note that we must wait for all the replicas to startup because
	// the PXFS routines that will be called after this routine returns
	// assume that all dependent device service replicas have been started
	// up.
	//

	wait_for_replicas_lock.lock();
	if (services_count != 0) {
		wait_for_replicas_cv.wait(&wait_for_replicas_lock);
	}
	ASSERT(services_count == 0);
	wait_for_replicas_lock.unlock();

	_starting_up = false;

	return (0);
}

//
// Attempt to switchback all the services stored in 'services_to_switchback'.
//
void
device_service_mgr::do_switchbacks_internal()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::do_switchbacks_internal().\n"));

	if ((services_to_switchback == NULL) ||
	    (services_to_switchback->length() == 0)) {
		//
		// Nothing to switchback.
		//
		DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("do_switchbacks_internal(%p): nothing to switch back.\n",
		    this));
		delete services_to_switchback;
		return;
	}

	// The memory allocated here is freed in 'switchback_worker_thread'.
	uint_t *services_count = new uint_t;
	os::mutex_t *switchback_lock = new os::mutex_t;

	*services_count = services_to_switchback->length();
	ASSERT(*services_count != 0);

	device_service_mgr::switchback_params *params;

	//
	// Loop through 'services_to_switchback', adding tasks to switch them
	// over.
	//
	for (uint_t i = 0; i < services_to_switchback->length(); i++) {
		//
		// The memory allocated in 'params' will be freed in
		// 'switchback_worker_thread'.
		//
		DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("do_switchbacks_internal(%p): going to switch back "
		    "service '%s'.\n", this,
		    (*services_to_switchback)[i]));
		params = new device_service_mgr::switchback_params(
		    (*services_to_switchback)[i], services_count,
		    switchback_lock, services_to_switchback);
		common_threadpool::the().defer_processing(new work_task(
		    switchback_worker_thread, (void *)params));
	}

	//
	// We do not need to wait for the switchovers to complete - this
	// translates to faster booting.
	//
}

//
// Worker thread to do switchback of a particular service.
//
void
device_service_mgr::switchback_worker_thread(void *data)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::switchback_worker_thread().\n"));

	device_service_mgr::switchback_params *params =
	    (device_service_mgr::switchback_params *)data;

	//
	// Call the DCS server to do the switchback.
	//
	Environment env;
	dclib::get_dcobj()->do_switchback(params->service_name,
	    orb_conf::node_number(), env);
	if (env.exception()) {
		DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("switchback_worker_thread(): exception '%s' while "
		    "calling do_switchback(%s, %d).\n",
		    env.exception()->_name(), params->service_name,
		    orb_conf::node_number()));
	}
	// It's OK if the switchback fails.
	env.clear();

	bool last = false;

	// Check if this is the last switchback.
	params->service_cnt_lock->lock();
	ASSERT(*(params->service_cnt) != 0);
	(*(params->service_cnt))--;
	if (*(params->service_cnt) == 0) {
		last = true;
	}
	params->service_cnt_lock->unlock();

	if (last) {
		//
		// This is the last switchback to occur - delete all the
		// memory allocated for these threads.
		//
		delete params->service_cnt;
		delete params->service_cnt_lock;
		delete params->all_services;
	}

	// Delete memory allocated in 'do_switchbacks_internal()'.
	delete params;
}

//
// This call is made on the DSM by the DCS to startup a device service.
//
// mdc::device_service_manager::start_device_service
void
device_service_mgr::start_device_service(uint32_t dev_id,
    const char *service_name, const char *user_pgm, uint_t priority,
    uint_t num_secondaries, mdc::dc_mapper_ptr mapper, Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::start_device_service(%d, %s, %s, %d, %d, "
	    "%p).\n", dev_id, service_name, user_pgm, priority,
	    num_secondaries, mapper));

	start_device_service_internal(dev_id, service_name, user_pgm,
		priority, num_secondaries, mapper, false);
}

//
// Worker thread to startup a device service replica.
//
void
device_service_mgr::start_service_worker_thread(void *data)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::start_service_worker_thread().\n"));

	Environment e;
	replica_startup_params *params = (replica_startup_params *)data;

	// Mark the service as active
	dclib::get_dcobj()->set_service_active(params->service_name, e);

	//
	// The only exception we can get is for an invalid service
	// name when the DCS cannot find the dc_service object for
	// this service. The service no longer exists, so we
	// ignore the exception.
	//
	if (e.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("start_service_worker_thread:exception '%s' while calling "
		    "set_service_active(%s).\n",
		    e.exception()->_name(), params->service_name));
		e.clear();
	}

	start_device_service_internal(params->dev_id, params->service_name,
	    params->user_pgm, params->priority, params->num_of_secondaries,
	    params->mapper, params->register_if_first);

	uint_t *cnt = params->service_cnt;
	params->service_cnt_lock->lock();
	ASSERT(*cnt != 0);
	(*cnt)--;
	if (*cnt == 0) {
		//
		// If we are the last thread to reach here, wakeup the thread
		// sleeping in 'register_with_dcs'.
		//
		params->service_cnt_cv->signal();
	}
	params->service_cnt_lock->unlock();

	delete params;
}

//
// Helper function to startup a device service replica.
//
void
device_service_mgr::start_device_service_internal(uint32_t dev_id,
    const char *service_name, const char *user_pgm, uint_t priority,
    uint_t num_secondaries, mdc::dc_mapper_ptr mapper,
    bool register_if_first)
{
	ASSERT(service_name != NULL);

	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::start_device_service_internal(%d, %s, %s, "
	    "%d, %d, %p).\n", dev_id, service_name, user_pgm, priority,
	    num_secondaries, mapper));

	device_replica_impl::initialize(dev_id, service_name, user_pgm,
	    priority, num_secondaries, mapper, register_if_first);
}

//
// Disable this DSM so that new device service replicas are not started on
// this node.
// If there are primary device service replicas currently on this node, then
// this call will return an EBUSY exception.
// If there are secondary or spare device service replicas currently on this
// node, then this call will shut them down.
//
void
device_service_mgr::disable_dsm(Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::disable_dsm().\n"));

	bool disabled = device_replica_impl::disable_primaries();
	if (!disabled) {
		DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("disable_dsm(%p): device is busy.\n", this));
		pxfslib::throw_exception(_environment, EBUSY);
	}
}

//
// Enable this DSM so that new device service replicas are started on this node.
// By default, the DSM is in the enabled state.
//
void
device_service_mgr::enable_dsm(Environment &)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::enable_dsm().\n"));

	device_replica_impl::enable_primaries();
}

//
// Return the device_server object for the given global dev_t.
//
mdc::device_server_ptr
dcs_resolve(dev_t gdev, bool &is_ha)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dcs_resolve(%p).\n", gdev));

	if (getmajor(gdev) >= (major_t)devcnt)
		return (mdc::device_server::_nil());
	mdc::device_server_ptr devserver = mdc::device_server::_nil();
	Environment e;
	int error;

	dclib::get_dcobj()->cascaded_get_device_server(getmajor(gdev),
	    getminor(gdev), orb_conf::node_number(), devserver,
	    is_ha, e);
	if (e.exception()) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("dcs_resolve(): exception '%s' while calling "
		    "cascaded_get_device_server(%d, %d, %d).\n",
		    e.exception()->_name(), getmajor(gdev),
		    getminor(gdev), orb_conf::node_number()));

		error = pxfslib::get_err(e);
		DEVICE_DBPRINTF(
		    DEVICE_TRACE_DCS,
		    DEVICE_RED,
		    ("dcs_resolve: err %d\n", error));
		e.clear();
		return (mdc::device_server::_nil());
	}

	return (devserver);
}

//
// Return the characteristics for the given global dev_t.
//
//lint -e1746
int
dcs_get_configured_nodes(sol::dev_t gdev, fs::dc_callback_ptr cb_obj,
    bool &is_ha, CORBA::String_out ha_service_name,
    sol::nodeid_seq_t_out nodes)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("dcs_get_configured_nodes(%p).\n", gdev));

	Environment e;
	dclib::get_dcobj()->cascaded_get_configured_nodes(getmajor(gdev),
	    getminor(gdev), orb_conf::node_number(), cb_obj, is_ha,
	    ha_service_name, nodes, e);

	int error = 0;
	CORBA::Exception *ex;
	if ((ex = e.exception()) != NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("dcs_get_configured_nodes(): exception '%s' while "
		    "calling cascaded_get_configured_nodes(%d, %d, %d).\n",
		    e.exception()->_name(), getmajor(gdev),
		    getminor(gdev), orb_conf::node_number()));

		sol::op_e *dce = sol::op_e::_exnarrow(ex);
		if (dce != NULL) {
#if 0
			//
			// We assume that devices that are not registered
			// with DCS are local.
			//
			is_ha = false;
			char *service_name = new char[1];
			service_name[0] = '\0';
			ha_service_name = service_name;
			sol::nodeid_seq_t *nodes_seq =
			    new sol::nodeid_seq_t(1, 1);
			(*nodes_seq)[0] = orb_conf::node_number();
			nodes = nodes_seq;
#else
			error = dce->error;
#endif
		} else {
			error = ENXIO;
		}
		DEVICE_DBPRINTF(
		    DEVICE_TRACE_DCS,
		    DEVICE_RED,
		    ("dc_get_configured_nodes: err %d\n", error));
		e.clear();
	}

	return (error);
}

//
// Confirm that all the major name <--> major number mappings the DCS server has
// sent us are valid on this node.
//
void
device_service_mgr::confirm_major_names(const sol::string_seq_t &major_names,
    const mdc::long_seq_t &major_numbers, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::confirm_major_names(%p).\n", major_names));

	uint_t len = major_numbers.length();
	os::sc_syslog_msg msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG, "Client", NULL);
	bool success = true;
	uint_t i;

	for (i = 0; i < len; i++) {
		char *major_name = ddi_major_to_name(major_numbers[i]);
		ASSERT((const char *)(major_names[i]) != NULL);
		if ((major_name == NULL) || (strcmp(
		    (const char *)(major_names[i]), major_name) != 0)) {
			//
			// The major number <--> major name mappings on this
			// node do not match the ones on the DCS server.  Log
			// an error message - we will eventually return the
			// error as an exception, but we go on to check for
			// other errors, and log them as well.
			//
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("confirm_major_names(%p): major number '%s' does "
			    "not match the one ('%s') on the DCS "
			    "server node.\n", major_names[i], major_name));
			//
			// SCMSGS
			// @explanation
			// The driver identified in this message does not have
			// the same major number across cluster nodes, and
			// devices owned by the driver are being used in
			// global device services.
			// @user_action
			// Look in the /etc/name_to_major file on each cluster
			// node to see if the major number for the driver
			// matches across the cluster. If a driver is missing
			// from the /etc/name_to_major file on some of the
			// nodes, then most likely, the package the driver
			// ships in was not installed successfully on all
			// nodes. If this is the case, install that package on
			// the nodes that don't have it. If the driver exists
			// on all nodes but has different major numbers, see
			// the documentation that shipped with this product
			// for ways to correct this problem.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE, "Major "
			    "number for driver (%s) does not match the one on "
			    "other nodes.  Confirm that the /etc/name_to_major "
			    "files are in sync on all cluster nodes.",
			    (const char *)(major_names[i]));
			success = false;
		}
	}

	if (!success) {
		pxfslib::throw_exception(_environment, EINVAL);
	}
}

void
device_service_mgr::shutdown_replica(const char *service_name,
    Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::shutdown_replica(%s).\n", service_name));

	dc_error_t err_val = DCS_SUCCESS;
	replica::service_info *svc_info = NULL;

	if (_starting_up) {
		//
		// This DSM is still starting up services - too early to
		// permit a shutdown.
		//
		DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("shutdown_replica(%p): service '%s' is still starting "
		    "up.\n", service_name));

		err_val = DCS_ERR_NODE_BUSY;
		goto out;
	}

	//
	// If the 'set_inactive' call returns true, that means that the device
	// service identified by 'service_name' is not the primary, and
	// it has been flagged to refuse to become the primary from now on.
	//
	err_val = device_replica_impl::set_inactive(service_name,
	    true);
	if (err_val == DCS_SUCCESS) {
		// Get the provider id for the replicas to be shutdown.
		char p_id[dclib::NODEID_STRLEN];
		(void) sprintf(p_id, "%d", (int)orb_conf::node_number());

		// Get a handle to the rma
		replica::rm_admin_var rm_ref = rm_util::get_rm();
		ASSERT(!CORBA::is_nil(rm_ref));

		// Get a handle to the service admin. for this service
		Environment e;
		char *s_id = dclib::get_serviceid(service_name);
		replica::service_admin_var s_admin = rm_ref->get_control(s_id,
		    e);
		delete [] s_id;
		if (e.exception()) {
			// This service is dead - return success.
			DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("shutdown_replica(%p): exception '%s' while "
			    "calling get_control() for service '%s'. "
			    "Service must be dead.\n",
			    this, e.exception()->_name(), service_name));
			e.clear();
			goto out;
		}

		//
		// Try the shutdown process a few times to handle the cases
		// where mounts are taking place for devices in this device
		// service while the replica is being shutdown.
		//
		static const uint_t max_retries = 3;
		uint_t j = 0;
		for (j = 0; j < max_retries; j++) {

			// We have to shutdown the dependent services first.
			svc_info = s_admin->get_service_info(e);
			if (e.exception()) {
				// This service is dead - return success.
				DCS_DBPRINTF_G(
				    DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("shutdown_replica(%p): exception '%s' "
				    "while calling get_service_info() for "
				    "service '%s'. Service must be dead.\n",
				    this, e.exception()->_name(),
				    service_name));
				e.clear();
				goto out;
			}

			uint_t i = 0;
			replica::service_admin_var s_dep_admin;

			//
			// Walk through the dependent services, shutting down
			// each one.
			//
			for (i = 0; i < svc_info->depend_on_this.length();
			    i++) {
				s_dep_admin = rm_ref->get_control(
				    (char *)svc_info->depend_on_this[i],
				    e);
				if (e.exception()) {
					// This service is dead - go on.
					DCS_DBPRINTF_G(
					    DCS_TRACE_DEVICE_SERVICE_MGR,
					    ("shutdown_replica(%p): exception "
					    "'%s' while calling "
					    "get_control() for service "
					    "'%s' depending on service "
					    "'%s'. Service must be dead.\n",
					    this, e.exception()->_name(),
					    svc_info->depend_on_this[i],
					    service_name));
					e.clear();
					continue;
				}

				s_dep_admin->change_repl_prov_status(p_id,
				    replica::SC_REMOVE_REPL_PROV, true,
				    e);
				if (e.exception()) {
					// This service is dead - go on.
					DCS_DBPRINTF_G(
					    DCS_TRACE_DEVICE_SERVICE_MGR,
					    ("shutdown_replica(%p): exception "
					    "'%s' while calling "
					    "change_repl_prov_status() for"
					    "service '%s' depending on "
					    "service '%s'. Service must be"
					    " dead.\n",
					    this, e.exception()->_name(),
					    svc_info->depend_on_this[i],
					    service_name));

					e.clear();
					continue;
				}
			}

			//
			// All dependent services have been shutdown - now
			// shutdown the device service.
			//
			s_admin->change_repl_prov_status(p_id,
			    replica::SC_REMOVE_REPL_PROV, true, e);
			if (e.exception()) {
				if (replica::invalid_repl_prov::_exnarrow(
				    e.exception())) {
					//
					// The replica is already inactive.
					// Return success.
					//
					e.clear();
					break;
				}

				DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("shutdown_replica(%p): exception '%s' "
				    "while calling "
				    "change_repl_prov_status() on service "
				    "'%s'.\n", this,
				    e.exception()->_name(), service_name));
				//
				// Uh-oh we failed.  Since we are guaranteed
				// to be the secondary (by the set_inactive()
				// call above), we should have succeeded
				// unless there was a transient condition (a
				// new mount while the replica was being
				// shutdown, maybe).
				// We try the same process a few times, but if
				// we don't succeed in our attempts, we have
				// no option but to print strong error
				// messages and exit (panic'ing seems
				// excessive).
				//
				e.clear();
			}
		}
		if (j == max_retries) {
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("shutdown_replica(%p): could not shutdown "
			    "service '%s'.\n", this, service_name));
			os::sc_syslog_msg msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG,
			    "Client", NULL);
			//
			// SCMSGS
			// @explanation
			// See message.
			// @user_action
			// If mounts or node reboots are on at the time this
			// message was displayed, wait for that activity to
			// complete, and then retry the command to shutdown
			// the device service replica. If not, then contact
			// your authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE, "Could not "
			    "shutdown replica for device service (%s).  Some "
			    "file system replicas that depend on this device "
			    "service may already be shutdown.  Future "
			    "switchovers to this device service will not "
			    "succeed unless this node is rebooted.",
			    service_name);
			err_val = DCS_ERR_NODE_BUSY;
		}
	}

out:
	// Cleanup and return;
	delete svc_info;

	if (err_val != DCS_SUCCESS) {
		_environment.exception(new mdc::dcs_error(err_val));
	}
}

//
// Called from the CMM in step one of the reconfiguration process to reset
// SCSI buses shared with nodes that just died.  See Bugid 4298040 for why
// this is necessary.
//
void
device_service_mgr::cmm_callback()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::cmm_callback().\n"));

	char *reset_call;

	reset_call = new char[strlen(BUS_RESET_PROGRAM) + 1];
	ASSERT(reset_call != NULL);

	(void) sprintf(reset_call, "%s", BUS_RESET_PROGRAM);

	//
	// Hand off the task to a threadpool dedicated to this task.
	//
	dsm_reconfig_threadpool.defer_processing(new work_task(
	    cmm_callback_worker, (void *) reset_call));
}


//
// Called from CMM reconfig when a node rejoins the cluster to drop scsi-2
// reservations held against that node.
//
void
device_service_mgr::release_scsi2(const char *node)
{
	char *fence_call;

	// fence_call is freed by cmm_callback_worker
	fence_call = new char[strlen(RELEASE_SCSI2_PROGRAM) + strlen(node) + 1];
	ASSERT(fence_call != NULL);

	(void) sprintf(fence_call, "%s%s", RELEASE_SCSI2_PROGRAM, node);

	// Release scsi-2 reservations on devices shared with this node
	dsm_release_scsi2_threadpool.defer_processing(new work_task(
	cmm_callback_worker, (void *) fence_call));
}

//
// Called from the end of the CMM reconfig to delay completion of a node's join
// until the release of scsi-2 reservations has comepleted.
//
void
device_service_mgr::wait_for_release()
{
	dsm_release_scsi2_threadpool.quiesce(threadpool::QEMPTY);
}

//
// Called from CMM reconfiguration quiesce_step during scshutdown
// to hold the FENCE_LOCK and prevent UCMM from fencing.
//
void
device_service_mgr::acquire_fence_lock(const char *node)
{
	Environment e;
	CORBA::Exception *ex;
	os::sc_syslog_msg msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG, "DSM", NULL);
	char *fence_lock;
	unsigned int i;
	//
	// Contact the nameserver
	//
	naming::naming_context_var ctxv = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));
	//
	// Store the fencing lock name in the new context.
	//
	fence_lock = new char[strlen("FENCE_LOCK.") + strlen(node) + 1];
	ASSERT(fence_lock != NULL);

	(void) sprintf(fence_lock, "FENCE_LOCK.%s", node);

	for (i = 0; i < FENCE_LOCK_RETRIES; i++) {
		(void) ctxv->bind_new_context(fence_lock, e);
		if ((ex = e.exception()) != NULL) {
			if (naming::already_bound::_exnarrow(ex) != NULL) {
				//
				// Fencing lock already held, proceed.
				//
				e.clear();
				break;
			} else {
				//
				// SCMSGS
				// @explanation
				// The local nameserver on this was not
				// locatable.
				// @user_action
				// Communication with the nameserver is
				// required during failover situations in
				// order to guarantee data intgrity. The
				// nameserver was not locatable on this node,
				// so this node will be halted in order to
				// gurantee data integrity. Contact your
				// authorized Sun service provider to
				// determine whether a workaround or patch is
				// available.
				//
				(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Could not obtain fencing lock because "
				    "we could not contact the nameserver.");
				e.clear();
				if (i == 1) {
					CL_PANIC(ex == NULL);
				}
			}
		} else {
			// succeeded in binding
			break;
		}
	}
	delete fence_lock;
}

//
// Grab a fencing lock and make the upcall to perform fencing of the specified
// node from shared devices.  Fencing procedes asynchronously and the lock will
// released by the program which actually does the fencing.
//
void
device_service_mgr::fence_nodes(const char *node)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::fence_nodes(%s).\n", node));

	Environment e;
	CORBA::Exception *ex;
	os::sc_syslog_msg msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG, "DSM", NULL);
	char *fence_call, *fence_lock;
	unsigned int i;

	// Contact the nameserver
	naming::naming_context_var ctxv = ns::local_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));

	// Store the fencing lock name in the new context
	fence_lock = new char[strlen("FENCE_LOCK.") + strlen(node) + 1];
	ASSERT(fence_lock != NULL);

	(void) sprintf(fence_lock, "FENCE_LOCK.%s", node);

	for (i = 0; i < FENCE_LOCK_RETRIES; i++) {
		(void) ctxv->bind_new_context(fence_lock, e);
		if ((ex = e.exception()) != NULL) {
			if (naming::already_bound::_exnarrow(ex) != NULL) {
				DCS_DBPRINTF_G(DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("fence_nodes(%s): lock already held.\n",
				    node));
				//
				// SCMSGS
				// @explanation
				// The lock used to specify that device
				// fencing is in progress is already held.
				// @user_action
				// This is an informational message, no user
				// action is needed.
				//
				(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Fencing lock already held, proceeding.");
					e.clear();
					break;
			} else {
				DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVICE_MGR,
				    ("fence_nodes(): exception '%s' while "
				    "calling bind_new_context() for node "
					"'%s'.\n", ex->_name(), node));
				(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
				    "Could not obtain fencing lock because "
				    "we could not contact the nameserver.");
				e.clear();
				if (i == 1)
					CL_PANIC(ex == NULL);
			}
		} else
			// succeeded in binding
			break;
	}

	delete fence_lock;

	// fence_call is freed by cmm_callback_worker
	fence_call = new char[strlen(NODE_FENCE_PROGRAM) + strlen(node) + 1];
	ASSERT(fence_call != NULL);

	(void) sprintf(fence_call, "%s%s", NODE_FENCE_PROGRAM, node);

	// Fence node from shared devices
	dsm_reconfig_threadpool.defer_processing(new work_task(
	    cmm_callback_worker, (void *) fence_call));
}



//
// Do the actual work of calling the user program to generate bus resets on
// non-fibre buses shared with dead nodes.  The memory for command is freed
// here since the call to this function is asynchronous.
//
void
device_service_mgr::cmm_callback_worker(void *command)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVICE_MGR,
	    ("device_service_mgr::cmm_callback_worker().\n"));

	Environment		env;
	os::sc_syslog_msg	*msg = NULL;

	CORBA::Object_var		obj;
	repl_pxfs::ha_mounter_var	mounter;
	char				name[20];
	naming::naming_context_var	ctxp = ns::root_nameserver();

	//
	// Get the 'clexecd' object from the nameserver.
	//
	os::sprintf(name, "ha_mounter.%d", orb_conf::node_number());
	obj = ctxp->resolve(name, env);

	// Narrow the object down to type 'ha_mounter'.
	if (env.exception() == NULL) {
		mounter = repl_pxfs::ha_mounter::_narrow(obj);
	}

	if ((env.exception() != NULL) || CORBA::is_nil(mounter)) {
		//
		// Either there is no object in the nameserver bound with the
		// name 'ha_mounter.<nodeid>', or the object bound in the
		// nameserver with that name is not of type 'ha_mounter'.
		//
		msg = new os::sc_syslog_msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG,
		    "DSM", NULL);
		(void) msg->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Could not find clexecd in nameserver.");
		delete msg;
		if (env.exception() != NULL) {
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("cmm_callback_worker(): exception '%s' while "
			    "calling resolve(%s).\n",
			    env.exception()->_name(), name));
			CLEXEC_EXCEPTION(env,
			    "cmm_callback_worker", "ha_mounter");
			env.clear();
		} else {
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
			    ("cmm_callback_worker(): CORBA::is_nil(mounter) "
			    "for name '%s'.\n", name));
			CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_RED,
			    ("cmm_callback_worker:%s exec %s"
			    " CORBA::is_nil (mounter)\n", name, command));
		}
		return;
	}

	//
	// Execute the user program in RT, in the foreground, syslog'ing any
	// output.
	//
	CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_GREEN,
	    ("cmm_callback_worker:%s exec %s\n", name, command));

	mounter->exec_program_with_opts((char *)command, true, true, true,
	    env);

	CLADM_DBPRINTF(CLADM_TRACE_CLEXEC, CLADM_GREEN,
	    ("cmm_callback_worker:%s exec %s excep %p\n",
	    name, command, env.exception()));

	if (env.exception() != NULL) {
		// Failed to execute the program - print out a warning.
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVICE_MGR,
		    ("cmm_callback_worker(): exception '%s' while calling "
		    "exec_program_with_opts(%s).\n",
		    env.exception()->_name(), command));
		msg = new os::sc_syslog_msg(SC_SYSLOG_DCS_DEVICE_SERVICE_TAG,
		    "DSM", NULL);
		//
		// SCMSGS
		// @explanation
		// There were problems making an upcall to run a user-level
		// program.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) msg->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Could not run %s. User program did not execute cleanly.",
		    (char *)command);
		delete msg;
		CLEXEC_EXCEPTION(env, "cmm_callback_worker", command);
		env.clear();
	}

	delete command;
}
