//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _DEVICE_SERVER_H
#define	_DEVICE_SERVER_H

#pragma ident	"@(#)device_server.h	1.7	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/dc_ki.h>

#include <solobj/solobj_impl.h>

#include <h/pxfs_v1.h>
#include <pxfs/version.h>
#include <pxfs/server/fs_base_impl.h>
#include <pxfs/server/fobj_impl.h>

// Forward declaration
class device_server_ii;

//
// device_server - supports device file objects.
//
class device_server : public fs_base_ii {
public:
	device_server();

	virtual base_type_t	get_base_type();

	// Type for fobj factory functions.
	typedef fobj_ii * (*fobj_ii_factory_t)(device_server_ii *dsp,
	    vnode_t *vp, const PXFS_VER::pvnode *real_pvnode, cred_t *credp,
	    fid_t *fidp);

	//
	// Find a fobj object corresponding to a given vnode and return
	// a new CORBA reference to it.
	// If the fobj object is not found, a new implementation object
	// is created based on the factory function.
	//
	PXFS_VER::fobj_ptr	find_fobj(vnode_t *vp,
	    const PXFS_VER::pvnode *real_pvnode,
	    solobj::cred_ptr credobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    Environment &env);

	// Make a new specvp from the given parameters.
	static vnode_t	*make_specvp(const PXFS_VER::pvnode &real_pvnode,
	    cred_t *credp);

	// Checkpoint routines for device_replica_impl.
	void		ckpt_new_io_object(PXFS_VER::fobj_ptr io_obj,
	    const PXFS_VER::pvnode &real_pvnode, solobj::cred_ptr credobj);

	void		get_open_device_fobj_v1(
	    const PXFS_VER::pvnode &real_pvnode, int32_t flags,
	    solobj::cred_ptr credobj, PXFS_VER::fobj_out specfobj,
	    PXFS_VER::fobj_info &fobjinfo, Environment &_environment);

	void		get_open_device_fobj_v2(
	    const PXFS_VER::pvnode &real_pvnode, int32_t flags,
	    solobj::cred_ptr credobj, PXFS_VER::fobj_out specfobj,
	    PXFS_VER::fobj_info &fobjinfo, Environment &_environment);

	void	initialize(device_server_ii *dev_server_iip);

	// Return true when there are no active devices
	bool	is_empty();

	void	dump_state(repl_dc::repl_dev_server_ckpt_ptr ckpt,
	    Environment &env);

	int	convert_to_primary();
	void	convert_to_secondary();
	void	convert_to_spare();

	void	set_not_primary();

	virtual bool	is_replicated() const;

protected:
	//
	// Creates a new device implementation object.
	//
	static fobj_ii	*device_server_norm_factory(device_server_ii *dsp,
	    vnode *vp, const PXFS_VER::pvnode *real_pvnode, cred_t *credp,
	    fid_t *fidp);

	static fobj_ii	*device_server_repl_factory(device_server_ii *dsp,
	    vnode *vp, const PXFS_VER::pvnode *real_pvnode, cred_t *credp,
	    fid_t *fidp);

	//
	// refcnt_unref can be overloaded if the child wants to do something
	// other than delete itself when the refcnt goes to zero.
	//
	virtual	void	refcnt_unref();

private:
	device_server_ii	*device_server_iip;

	// Factory function to create new fobjs from vnodes.
	fobj_ii_factory_t	rootfactory;

	//
	// Following operations are not allowed.
	//
	device_server & operator=(const device_server &);
	device_server(const device_server &);
};

#include <pxfs/device/device_server_in.h>

#endif	// _DEVICE_SERVER_H
