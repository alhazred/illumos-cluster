//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _DEVICE_SERVER_IMPL_H
#define	_DEVICE_SERVER_IMPL_H

#pragma ident	"@(#)device_server_impl.h	1.40	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/dc_ki.h>

#include <h/repl_dc.h>
#include <h/pxfs.h>
#include <orb/object/adapter.h>
#include <pxfs/device/device_replica_impl.h>
#include <pxfs/device/device_server.h>

//
// The device_server object has the same relationship to devices
// that fs_impl does to file system objects like files and directories.
// Both share common code for I/O fobjs in io_impl.cc.
// The device_server is a separate service from the file system because
// the node(s) which has/have a connection to the device doesn't have to be
// the same node(s) which has/have a connection to the file system that
// contains the "/dev/xxx" special device file.
// For HA devices, the device service can also failover/switchover
// independently from the file system containing the "/dev/xxx" file.
// This service is started by DCS when a VOP_OPEN() is attempted on
// the "/dev/xxx" device file.
//

//
// Declaration of internal implementation object.
//
class device_server_ii {
	friend class device_server;
public:
	// Constructors are protected.

	virtual ~device_server_ii();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual repl_dc::repl_dev_server_ckpt_ptr	get_ckpt() = 0;

	// Return true if this is a replicated object.
	bool			is_replicated() const;

	// Accessor function.
	device_replica_impl	*get_serverp() const;

	//
	// This function returns false if this device service holds any open
	// references to io objects.
	//
	bool			mark_shutting_down();

	//
	// Find a fobj object corresponding to a given vnode and return
	// a new CORBA reference to it.
	// If the fobj object is not found, a new implementation object
	// is created based on the factory function.
	//
	pxfs_v1::fobj_ptr	find_fobj(vnode_t *vp,
	    const pxfs_v1::pvnode *real_pvnode,
	    solobj::cred_ptr credobj,
	    pxfs_v1::fobj_info &fobjinfo,
	    Environment &env);


	// Make a new specvp from the given parameters.
	static vnode_t	*make_specvp(const pxfs_v1::pvnode &real_pvnode,
	    cred_t *crp);

	// Checkpoint routines for device_replica_impl.
	void		ckpt_new_io_object(fs::fobj_ptr io_obj,
	    const fs::pvnode &real_pvnode, solobj::cred_ptr credobj);

	void		ckpt_new_io_object_v1(pxfs_v1::fobj_ptr io_obj,
	    const pxfs_v1::pvnode &real_pvnode, solobj::cred_ptr credobj);

protected:
	// Constructor for non-HA device servers.
	device_server_ii();

	// Constructor for HA device servers.
	device_server_ii(unsigned int devid,  device_replica_impl *srvrp);

public:
	//
	// These objects manage the device fobj's,
	// which are closely tied to PXFS.
	//
	device_server	device_serv;

protected:
	//
	// Variable to indicate that this server is shutting down, and a lock
	// to protect it.
	//
	os::rwlock_t	busy_lock;
	bool		shutting_down;

private:
	// Pointer to the replica that this object is a part of.
	device_replica_impl	*serverp;

	// Device Server is assigned in the constructor.
	unsigned int		devid;

	//
	// Following operations are not allowed.
	//
	device_server_ii & operator=(const device_server_ii &rsh);
	device_server_ii(const device_server_ii &rsh);
};

class device_server_norm_impl : public device_server_ii,
    public McServerof<mdc::device_server> {

public:
	device_server_norm_impl();
	~device_server_norm_impl();
	void _unreferenced(unref_t arg);

	void _generic_method(CORBA::octet_seq_t &,
	    CORBA::object_seq_t &objs, Environment &);

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	repl_dc::repl_dev_server_ckpt_ptr get_ckpt();

	repl_dc::repl_dev_server_ckpt_ptr get_checkpoint()
	{
		return (get_ckpt());
	}

	void	get_open_device_fobj(const fs::pvnode &real_pvnode,
	    int32_t flags,
	    solobj::cred_ptr credobj,
	    fs::fobj_out specfobj,
	    fs::fobj_info &fobjinfo,
	    Environment &_environment);

	void	get_open_device_fobj_v1(
	    const pxfs_v1::pvnode &real_pvnode,
	    int32_t flags,
	    solobj::cred_ptr credobj,
	    pxfs_v1::fobj_out specfobj,
	    pxfs_v1::fobj_info &fobjinfo,
	    Environment &_environment);

	void	get_open_device_fobj_v2(
	    const pxfs_v1::pvnode &real_pvnode,
	    int32_t flags,
	    solobj::cred_ptr credobj,
	    pxfs_v1::fobj_out specfobj,
	    pxfs_v1::fobj_info &fobjinfo,
	    Environment &_environment);
};

class device_server_repl_impl : public device_server_ii,
    public mc_replica_of<mdc::device_server> {

public:
	// Constructor for the primary object.
	device_server_repl_impl(device_replica_impl *srvp,
	    unsigned int dev_id);

	// Constructor for the secondary object.
	device_server_repl_impl(device_replica_impl *srvp,
	    mdc::device_server_ptr obj,
	    unsigned int dev_id);

	~device_server_repl_impl();
	void _unreferenced(unref_t arg);

	void _generic_method(CORBA::octet_seq_t &,
	    CORBA::object_seq_t &objs, Environment &);

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	repl_dc::repl_dev_server_ckpt_ptr get_ckpt();

	repl_dc::repl_dev_server_ckpt_ptr get_checkpoint()
	{
		return (get_ckpt());
	}

	void	dump_state(repl_dc::repl_dev_server_ckpt_ptr ckpt,
	    Environment &env);

	int	convert_to_primary();
	void	convert_to_secondary();
	void	convert_to_spare();

	void	get_open_device_fobj(const fs::pvnode &real_pvnode,
	    int32_t flags,
	    solobj::cred_ptr credobj,
	    fs::fobj_out specfobj,
	    fs::fobj_info &fobjinfo,
	    Environment &_environment);

	void	get_open_device_fobj_v1(
	    const pxfs_v1::pvnode &real_pvnode,
	    int32_t flags,
	    solobj::cred_ptr credobj,
	    pxfs_v1::fobj_out specfobj,
	    pxfs_v1::fobj_info &fobjinfo,
	    Environment &_environment);

	void	get_open_device_fobj_v2(
	    const pxfs_v1::pvnode &real_pvnode,
	    int32_t flags,
	    solobj::cred_ptr credobj,
	    pxfs_v1::fobj_out specfobj,
	    pxfs_v1::fobj_info &fobjinfo,
	    Environment &_environment);
};

#include <pxfs/device/device_server_impl_in.h>

#endif	/* _DEVICE_SERVER_IMPL_H */
