//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _DEVICE_DEBUG_H
#define	_DEVICE_DEBUG_H

#pragma ident	"@(#)device_debug.h	1.5	08/05/20 SMI"

#include <sys/dbg_printf.h>

#define	DEVICE_DBGBUF

#ifdef	DEVICE_DBGBUF
extern dbg_print_buf device_dbg;
#define	DEVICE_DBG(a)	device_dbg.dbprintf a
#else
#define	DEVICE_DBG(a)
#endif

const int	DEVICE_GREEN = 3;
const int	DEVICE_AMBER = 2;
const int	DEVICE_RED = 1;

//
// These options control which debug statements are enabled.
// This allows them to be controlled via the debugger.
// These constants are always defined in order to catch name conflicts.
//
const int	DEVICE_TRACE_DEVREPL		= 0x00000001;
const int	DEVICE_TRACE_DEVSRVR		= 0x00000002;
const int	DEVICE_TRACE_DCS		= 0x00000004;

extern uint_t	device_trace_options;
extern int	device_trace_level;

//
// DEVICE_DBPRINTF - record pxfs trace information.
// Inputs
//	option - enables trace if set in pxfs_trace_options
//	level - Debug level, currently red, amber and green
//	args - The arguments to the print statements must be enclosed
//		with parenthesis, as in the following example:
//			("pxfs action %d\n", action_number)
//
// Note that the trailing "else" statement in the macro removes
// ambiguity when the macro is nested in another "if" statement.
//
#ifdef DEVICE_DBGBUF
#define	DEVICE_DBPRINTF(option, level, args) \
if (((option & device_trace_options) &&\
	(level <= device_trace_level)) ||\
	(level == DEVICE_RED)) {\
		DEVICE_DBG(args); \
	} else
#else
#define	DEVICE_DBPRINTF(option, level, args)
#endif	/* DEVICE_DBGBUF */

#endif	// _DEVICE_DEBUG_H
