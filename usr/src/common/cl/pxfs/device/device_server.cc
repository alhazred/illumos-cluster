//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)device_server.cc	1.11	08/05/20 SMI"

#include <pxfs/common/pxfslib.h>
#include <pxfs/device/device_debug.h>
#include <pxfs/device/device_server.h>
#include <pxfs/version.h>
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/client/pxfobj.h>
#include <dc/sys/dc_debug.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

// virtual
fs_base_ii::base_type_t
device_server::get_base_type()
{
	return (fs_base_ii::BASE_DEVICE_SERVER);
}

//
// Instantiate the device object (specvp) for the given dev_t in 'real_pvnode'
// and return it through 'specfobj' and 'fobjinfo'.
// This can be called from any node in the system.
//
//lint -e1746
void
device_server::get_open_device_fobj_v1(const PXFS_VER::pvnode &real_pvnode,
    int32_t flags, solobj::cred_ptr credobj, PXFS_VER::fobj_out specfobj,
    PXFS_VER::fobj_info &fobjinfo, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::get_open_device_fobj(%p, %d, %p).\n",
	    real_pvnode, flags, credobj));

	device_server_iip->busy_lock.rdlock();
	if (device_server_iip->shutting_down) {
		//
		// This server is in the middle of a shutdown.  Ask the client
		// to try again.
		//
		DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVER,
		    ("get_open_device_fobj(%p): in the middle of a "
		    "shutdown.\n", this));
		device_server_iip->busy_lock.unlock();
		pxfslib::throw_exception(_environment, EAGAIN);
		return;
	}

	cred_t *credp = solobj_impl::conv(credobj);

	// This returns a held vp or NULL.
	vnode_t		*svp = make_specvp(real_pvnode, credp);
	if (svp == NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVER,
		    ("get_open_device_fobj(%p): svp is null.\n", this));
		device_server_iip->busy_lock.unlock();
		pxfslib::throw_exception(_environment, ENXIO);
		return;
	}

	// Find or create an object for the device vnode.
	PXFS_VER::fobj_var	spec_obj = find_fobj(svp, &real_pvnode, credobj,
	    fobjinfo, _environment);
	VN_RELE(svp);
	ASSERT(!CORBA::is_nil(spec_obj));

	//
	// Do the open.
	//
	if (device_server_iip->is_replicated()) {
		io_repl_impl	*iop =
		    (io_repl_impl *)fobj_ii::get_fobj_ii(spec_obj);
		iop->open(flags, specfobj, fobjinfo, credobj, _environment);
	} else {
		io_norm_impl	*iop =
		    (io_norm_impl *)fobj_ii::get_fobj_ii(spec_obj);
		iop->open(flags, specfobj, fobjinfo, credobj, _environment);
	}

	device_server_iip->busy_lock.unlock();
}

//
// Instantiate the device object (specvp) for the given dev_t in 'real_pvnode'
// and return it through 'specfobj' and 'fobjinfo'.
// This can be called from any node in the system.
//
void
device_server::get_open_device_fobj_v2(const PXFS_VER::pvnode &real_pvnode,
    int32_t flags, solobj::cred_ptr credobj, PXFS_VER::fobj_out specfobj,
    PXFS_VER::fobj_info &fobjinfo, Environment &_environment)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::get_open_device_fobj_v2(%p, %d, %p).\n",
	    real_pvnode, flags, credobj));

	device_server_iip->busy_lock.rdlock();
	if (device_server_iip->shutting_down) {
		//
		// This server is in the middle of a shutdown.  Ask the client
		// to try again.
		//
		DCS_DBPRINTF_A(DCS_TRACE_DEVICE_SERVER,
		    ("get_open_device_fobj_v2(%p): in the middle of a "
		    "shutdown.\n", this));
		device_server_iip->busy_lock.unlock();
		_environment.exception(new mdc::ds_shutdown);
		return;
	}

	cred_t *credp = solobj_impl::conv(credobj);

	// This returns a held vp or NULL.
	vnode_t		*svp = make_specvp(real_pvnode, credp);
	if (svp == NULL) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVER,
		    ("get_open_device_fobj_v2(%p): svp is null.\n", this));
		device_server_iip->busy_lock.unlock();
		pxfslib::throw_exception(_environment, ENXIO);
		return;
	}

	// Find or create an object for the device vnode.
	PXFS_VER::fobj_var	spec_obj = find_fobj(svp, &real_pvnode, credobj,
	    fobjinfo, _environment);
	VN_RELE(svp);
	ASSERT(!CORBA::is_nil(spec_obj));

	//
	// Do the open.
	//
	if (device_server_iip->is_replicated()) {
		io_repl_impl	*iop =
		    (io_repl_impl *)fobj_ii::get_fobj_ii(spec_obj);
		iop->open(flags, specfobj, fobjinfo, credobj, _environment);
	} else {
		io_norm_impl	*iop =
		    (io_norm_impl *)fobj_ii::get_fobj_ii(spec_obj);
		iop->open(flags, specfobj, fobjinfo, credobj, _environment);
	}

	device_server_iip->busy_lock.unlock();
}
//lint +e1746

//
// Make a new specvp from the given parameters.
//
vnode_t *
device_server::make_specvp(const PXFS_VER::pvnode &real_pvnode, cred_t *crp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::make_specvp(%p, %p).\n", real_pvnode, crp));

	//
	// Convert the fobj for the realvp into a proxy vnode.
	// Note that this is one of the few places where pxfs server depends
	// on pxfs client. The design requires that the pxfs server for
	// 'real_pvnode' be either on the same node as the client calling
	// get_open_device_fobj() or be in an HA file system.
	// We want to prevent the case where the server for 'real_pvnode'
	// dies but the specfobj object we return is still in use since the
	// realvp is needed by the specfobj object.
	// Also note that realvp is held and we need to VN_RELE() when done.
	//
	vnode_t		*realvp = pxfobj::unpack_vnode(real_pvnode);
	if (realvp == NULL) {
		//
		// Only happens if the pxfs file system isn't locally mounted
		// or the real_pvnode was released (see ckpt_new_io_object()).
		//
		return (NULL);
	}

	//
	// For now, we don't handle remote streams devices.
	// See comment in io_repl_impl::open().
	//
	dev_t		gdev = realvp->v_rdev;
	major_t		maj;
	if ((maj = getmajor(gdev)) >= (major_t)devcnt ||
	    ddi_hold_installed_driver(maj) == NULL ||
	    STREAMSTAB(maj)) {
		VN_RELE(realvp);
		return (NULL);
	}

	//
	// Create the specvp.
	//
	vnode_t		*svp = specvp(realvp, gdev, realvp->v_type, crp);
	VN_RELE(realvp);
	return (svp);
}

//
// Find a fobj object corresponding to a given vnode and return a new
// CORBA reference to it.
// If the fobj object is not found, a new implementation object is created
// based on the factory function.
//
PXFS_VER::fobj_ptr
device_server::find_fobj(vnode_t *vp, const PXFS_VER::pvnode *real_pvnode,
    solobj::cred_ptr credobj, PXFS_VER::fobj_info &fobjinfo, Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::find_fobj(%p, %p, %p).\n",
	    vp, real_pvnode, credobj));

	ASSERT(vp != NULL);
	ASSERT(rootfactory != NULL);

	//
	// Get the file ID for constructing a new object.
	// We do this here so we don't have to delete the fobj if there is
	// an error.
	//
	fid_t	*fidp = new fid_t;
	fidp->fid_len = MAXFIDSZ;
	bzero(fidp->fid_data, (size_t)MAXFIDSZ);
#if	SOL_VERSION >= __s11
	int	error = VOP_FID(vp, fidp, NULL);
#else
	int	error = VOP_FID(vp, fidp);
#endif
	if (error != 0) {
		DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVER,
		    ("find_fobj(%p): VOP_FID(%p) failed with error %d.\n",
		    this, vp, error));
		DEVICE_DBPRINTF(
		    DEVICE_TRACE_DEVSRVR,
		    DEVICE_RED,
		    ("device_server_ii::find_fobj vp = %p VOP_FID() error %d\n",
		    vp, error));
		fobjinfo.ftype = PXFS_VER::fobj_fobj;
		pxfslib::throw_exception(env, error);
		delete fidp;
		return (PXFS_VER::fobj::_nil());
	}

	// TODO: hash by dev_t instead of vp.
	fobj_ii			*fobj_iip;
	fobj_ii			*newp;
	PXFS_VER::fobj_ptr	fobjp;
	int			bktno = vp_to_bucket(vp);

	//
	// First check to see if its already in the table.
	//
	fobj_ii_list_t	&hbkt = allfobj_list[bktno];
	fobjlist_locks[bktno].rdlock();
	fobj_ii_list_t::ListIterator iter(hbkt);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (VN_CMP(vp, fobj_iip->get_vp())) {		//lint !e666
			// XXX Found it. Should have kstat for this case.
			fobjp = fobj_iip->get_fobjref();
			fobjlist_locks[bktno].unlock();
			delete fidp;

			// Return the fobj information.
			pxfs_misc::init_fobjinfo(fobjinfo,
			    fobj_iip->get_ftype(), vp,
			    fobj_iip->get_fidp());
			return (fobjp);
		}
	}
	fobjlist_locks[bktno].unlock();

	//
	// Construct a new object.
	//
	newp = rootfactory(device_server_iip, vp, real_pvnode,
	    solobj_impl::conv(credobj), fidp);
	ASSERT(newp != NULL);

	//
	// We have to lock the hash list until we get a new CORBA reference;
	// otherwise, we have a race problem in _unreferenced() when it
	// calls _last_unref().
	//
	fobjlist_locks[bktno].wrlock();

	// Check to be sure object wasn't added while lock wasn't held.
	iter.reinit(hbkt);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (VN_CMP(vp, fobj_iip->get_vp())) {		//lint !e666
			fobjp = fobj_iip->get_fobjref();
			fobjlist_locks[bktno].unlock();

			// Delete the unused new object.
			delete newp;
			delete fidp;

			// Return the fobj information.
			pxfs_misc::init_fobjinfo(fobjinfo,
			    fobj_iip->get_ftype(), vp,
			    fobj_iip->get_fidp());
			return (fobjp);
		}
	}

	//
	// Return a new CORBA reference to this implementation object.
	// The hash list itself doesn't keep a CORBA reference to the object
	// so that we will get notified via _unreferenced() when the last
	// CORBA::release() happens.
	//
	fobj_iip = newp;
	fobjp = fobj_iip->get_fobjref();
	hbkt.prepend(fobj_iip);

	if (device_server_iip->is_replicated()) {
		//
		// Checkpoint the fobj creation to the secondary.
		//
		device_server_iip->get_ckpt()->
		    ckpt_new_io_object_v1(fobjp, *real_pvnode, credobj, env);
		env.clear();
	}
	fobjlist_locks[bktno].unlock();

	delete fidp;

	// Return the fobj information.
	pxfs_misc::init_fobjinfo(fobjinfo, fobj_iip->get_ftype(), vp,
	    fobj_iip->get_fidp());
	return (fobjp);
}

//
// For replicated instances of the device_server, this method is called on the
// secondaries by find_fobj() on the primary when a new fobj is created.
//
void
device_server::ckpt_new_io_object(PXFS_VER::fobj_ptr io_obj,
    const PXFS_VER::pvnode &real_pvnode, solobj::cred_ptr credobj)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::ckpt_new_io_object(%p, %p, %p).\n",
	    io_obj, real_pvnode, credobj));

	ASSERT(device_server_iip->is_replicated());

	//
	// We check if the cookie for this CORBA reference is NULL. If its not
	// NULL, it indicates that the implementation object has already been
	// created on this secondary, and this checkpoint is a replay after a
	// failover.
	//
	if (fobj_ii::get_fobj_ii(io_obj) != NULL) {
		// This checkpoint is a replay after a failover.
		return;
	}

	//
	// We maintain fid for replicated device object twice in the same
	// object : 1. In pvnode 2. In back_obj.fid
	// We need to do this so
	// that fobj_ii code does not need special handling for device file
	// objects, and also since we use fid for hashing on the client side.
	//
	PXFS_VER::fobjid_t	fobjid(real_pvnode.fobjinfo.fid.fobjid_len,
	    real_pvnode.fobjinfo.fid.fobjid_len,
	    (uint8_t *)real_pvnode.fobjinfo.fid.fobjid_data, false);

	PXFS_VER::io_var	iov = PXFS_VER::io::_narrow(io_obj);

	cred_t		*credp = solobj_impl::conv(credobj);

	fidlist.prepend(new
	    io_repl_impl(device_server_iip, fobjid, iov, real_pvnode, credp));
}

void
device_server::initialize(device_server_ii *dev_server_iip)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::initialize(%p).\n", dev_server_iip));

	device_server_iip = dev_server_iip;

	if (device_server_iip->is_replicated()) {
		rootfactory = device_server_repl_factory;
	} else {
		rootfactory = device_server_norm_factory;
	}
}

//
// Creates a new unreplicated I/O object.
//
fobj_ii *
device_server::device_server_norm_factory(device_server_ii *dsp, vnode *vp,
    const PXFS_VER::pvnode *, cred_t *, fid_t *fidp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::device_server_norm_factory(%p, %p, %p).\n",
	    dsp, vp, fidp));

	io_norm_impl	*io_implp = new io_norm_impl(dsp, vp, fidp);
	return ((fobj_ii *)io_implp);
}

//
// Creates a new replicated I/O object.
//
fobj_ii *
device_server::device_server_repl_factory(device_server_ii *dsp, vnode *vp,
    const PXFS_VER::pvnode *real_pvnode, cred_t *credp, fid_t *fidp)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::device_server_repl_factory(%p, %p, %p, %p, %p)"
	    ".\n", dsp, vp, real_pvnode, credp, fidp));

	io_repl_impl	*io_replp = new io_repl_impl(dsp, vp, *real_pvnode,
	    credp, fidp);
	return ((fobj_ii *)io_replp);
}

//
// is_empty - return true when there are no active devices
//
bool
device_server::is_empty()
{
	uint_t	i;
	uint_t	j;

	for (i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].wrlock();
	}
	for (i = 0; i < allfobj_buckets; i++) {
		if (!allfobj_list[i].empty()) {
			for (j = 0; j < allfobj_buckets; j++) {
				fobjlist_locks[j].unlock();
			}
			return (false);
		}
	}
	for (i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].unlock();
	}
	return (true);
}

//
// convert_to_primary
// We need to go through the list of io objects that we are currently serving
// and convert each of them to primary.
//
int
device_server::convert_to_primary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::convert_to_primary().\n"));

	_suspend_locking_ckpts = true;
	//
	// Loop through all the device fobjs to convert from secondary form
	// to primary form.
	//
	fobj_ii				*fobj_iip;
	fidlock.wrlock();
	fobj_ii_list_t::ListIterator	iter(fidlist);
	while ((fobj_iip = iter.get_current()) != NULL) {
		iter.advance();
		int	error = fobj_iip->convert_to_primary();
		if (error == 0) {
			(void) fidlist.erase(fobj_iip);
			bool	added = add_to_list(fobj_iip);
			ASSERT(added);
			continue;
		}
		//
		// There was an error converting this fobj to primary.
		// Return without trying to convert the remaining device
		// fobjs for this device server. Note that there is one
		// device server per device service and it handles all
		// the devices that are part of that service. For example,
		// in an SVM diskset there could be one or more metadevices.
		// Each of these metadevices have an io_repl_impl object,
		// backing them, and there is one device server object for
		// the the device service, serving all the io_repl_impl
		// objects.
		//
		if (error != EDOM) {
			DCS_DBPRINTF_R(DCS_TRACE_DEVICE_SERVER,
			    ("convert_to_primary(%p): convert_to_primary() "
			    "failed with error %d.\n", this, error));
			DEVICE_DBPRINTF(
			    DEVICE_TRACE_DEVSRVR,
			    DEVICE_RED,
			    ("device_server::convert_to_primary: error %d\n",
			    error));
			_suspend_locking_ckpts = false;
			fidlock.unlock();
			return (error);
		}
	}
	fidlock.unlock();
	_suspend_locking_ckpts = false;

	is_primary = true;
	return (0);
}

void
device_server::set_not_primary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::set_not_primary().\n"));

	is_primary = false;
}

//
// Convert this device_server replica to a secondary.
// Convert all io objects to secondary.
//
void
device_server::convert_to_secondary()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::convert_to_secondary().\n"));

	_suspend_locking_ckpts = true;

	//
	// Convert active objects to secondary form.
	//
	fobj_ii		*fobj_iip;
	fidlock.wrlock();
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobj_ii_list_t	&hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator	iter(hbkt);
		while ((fobj_iip = iter.get_current()) != NULL) {
			iter.advance();
			int	error = fobj_iip->convert_to_secondary();
			if (error == 0) {
				//
				// Remove the fobj from allfobj_list and put it
				// on fidlist.
				//
				(void) hbkt.erase(fobj_iip);
				fidlist.prepend(fobj_iip);
			}
		}
	}
	fidlock.unlock();
	_suspend_locking_ckpts = false;

	is_primary = false;
}

//
// Convert this device_server replica to a spare.
// Convert all io objects to spare.
// This is called after the service is frozen and all IDL invocations
// have completed (new IDL invocations and _unreferenced() are blocked).
//
void
device_server::convert_to_spare()
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::convert_to_spare().\n"));

	fobj_ii		*fobj_iip;

	//
	// We need to delete all fobj's and data since they won't get
	// _unreferenced().
	//
	fidlock.wrlock();
	fobj_ii_list_t::ListIterator iter(fidlist);
	while ((fobj_iip = iter.get_current()) != NULL) {
		iter.advance();
		(void) fidlist.erase(fobj_iip);
		fobj_iip->convert_to_spare();
	}
	fidlock.unlock();
}

//
// This function dumps the state to the specified secondary.
//
void
device_server::dump_state(repl_dc::repl_dev_server_ckpt_ptr ckpt,
    Environment &env)
{
	DCS_DBPRINTF_W(DCS_TRACE_DEVICE_SERVER,
	    ("device_server::dump_state(%p).\n", ckpt));

	fobj_ii		*fobj_iip;

	//
	// Dump all active fobj objects.
	// Note that this could cause another call to _unreferenced().
	//
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].wrlock();
		fobj_ii_list_t		&hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator	iter(hbkt);
		for (; (fobj_iip = iter.get_current()) != NULL;
		    iter.advance()) {
			//
			// Assert that the cast to io_repl_impl is OK.
			//
			ASSERT(fobj_iip->get_ftype() == PXFS_VER::fobj_io);
			ASSERT(fobj_iip->is_replicated());
			((io_repl_impl *)fobj_iip)->dump_io_state(ckpt, env);
			if (env.exception()) {
				fobjlist_locks[i].unlock();
				return;
			}
		}
		fobjlist_locks[i].unlock();
	}

	//
	// Dump objects which had errors converting from secondary to primary.
	// Note that this could cause another call to _unreferenced().
	//
	fidlock.wrlock();
	fobj_ii_list_t::ListIterator	iter(fidlist);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		//
		// Assert that the cast to io_repl_impl is OK.
		//
		ASSERT(fobj_iip->get_ftype() == PXFS_VER::fobj_io);
		ASSERT(fobj_iip->is_replicated());
		((io_repl_impl *)fobj_iip)->dump_io_state(ckpt, env);
		if (env.exception()) {
			fidlock.unlock();
			return;
		}
	}
	fidlock.unlock();
}

bool
device_server::is_replicated() const
{
	// Use the same function in device_server_ii
	return (device_server_iip->is_replicated());
}

//
// refcnt_unref can be overloaded if the child wants to do something
// other than delete itself when the refcnt goes to zero.
//
// device_server inherits from fs_base_ii, which inherits from refcnt.
// This class does not use that inherited reference counting,
// and as a contained class within device_server_ii that can not work.
//
void
device_server::refcnt_unref()
{
	ASSERT(0);
}
