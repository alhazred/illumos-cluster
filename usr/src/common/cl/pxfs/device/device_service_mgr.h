/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DEVICE_SERVICE_MGR_H
#define	_DEVICE_SERVICE_MGR_H

#pragma ident	"@(#)device_service_mgr.h	1.30	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <h/dc.h>

//
// Declaration of mdc::device_service_manager implementation object.
//
class device_service_mgr : public McServerof<mdc::device_service_manager> {
public:
	device_service_mgr();
	~device_service_mgr();

	void _unreferenced(unref_t arg);

	// mdc::device_service_manager::=
	void start_device_service(uint32_t dev_id, const char *service_name,
	    const char *user_pgm, uint_t priority, uint_t num_secondaries,
	    mdc::dc_mapper_ptr mapper,
	    Environment &_environment);
	void confirm_major_names(const sol::string_seq_t &major_names,
	    const mdc::long_seq_t &major_numbers, Environment &_environment);
	void shutdown_replica(const char *service_name,
	    Environment &_environment);
	void disable_dsm(Environment &_environment);
	void enable_dsm(Environment &_environment);
	//

	// Create new service classes entries
	static int create_service_classes();

	// Initialize the DSM.
	static int activate();

	// Perform required device service switchbacks.
	static int do_switchbacks();

	// Static function used to handle CMM callbacks during a reconfig.
	static void cmm_callback();

	// Used to perform node fencing during a reconfig
	static void fence_nodes(const char *node);

	// Used to release scsi2 reservations when a node rejoins the cluster
	static void release_scsi2(const char *node);
	static void wait_for_release();

	// Used to acquire fence lock
	static void acquire_fence_lock(const char *node);
private:

	// Pointer to the implementation object for the local DSM.
	static device_service_mgr *the_dsm;

	// Register this DSM with DCS (dc_config object).
	int register_with_dcs();

	// Do switchbacks on required device services.
	void do_switchbacks_internal();

	//
	// Used to hold the data passed in to 'start_service_worker_thread'.
	//
	class replica_startup_params {
	public:
		uint32_t dev_id;
		const char *service_name;
		const char *user_pgm;
		uint32_t priority;
		uint_t num_of_secondaries;
		mdc::dc_mapper_ptr mapper;
		bool register_if_first;
		uint_t *service_cnt;
		os::mutex_t *service_cnt_lock;
		os::condvar_t *service_cnt_cv;

		replica_startup_params(uint32_t _dev_id,
		    const char *_service_name, const char *_user_pgm,
		    uint_t _priority, uint_t num_sec,
		    mdc::dc_mapper_ptr _mapper,
		    bool _register_if_first, uint_t *_service_cnt,
		    os::mutex_t *_service_cnt_lock,
		    os::condvar_t *_service_cnt_cv);
		~replica_startup_params();
	};

	//
	// Used to hold the data passed in to 'switchback_worker_thread'.
	//
	class switchback_params {
	public:
		const char *service_name;
		uint_t *service_cnt;
		os::mutex_t *service_cnt_lock;
		CORBA::StringSeq *all_services;

		switchback_params(const char *_service_name,
		    uint_t *_service_cnt, os::mutex_t *_service_cnt_lock,
		    CORBA::StringSeq *_all_services);
		~switchback_params();
	};

	static void switchback_worker_thread(void *data);
	static void start_service_worker_thread(void *data);

	static void start_device_service_internal(uint32_t dev_id,
	    const char *service_name, const char *user_pgm, uint_t priority,
	    uint_t num_secondaries, mdc::dc_mapper_ptr mapper,
	    bool register_if_first);

	CORBA::StringSeq *services_to_switchback;

	bool _starting_up;

	//
	// Threadpool to handle the reconfig. callbacks made to the DSM
	// during CMM reconfigs.  We dedicate a thread to this high-priority
	// event to do away with the uncertainties of sharing, say, the
	// 'common_threadpool'.
	//
	static threadpool dsm_reconfig_threadpool;

	// seperate pool for releasing scsi2 so we can wait for it to complete
	static threadpool dsm_release_scsi2_threadpool;

	// Helper function for 'cmm_callback'.
	static void cmm_callback_worker(void *command);
};

//
// Return the device_server object for the given global dev_t.
//
mdc::device_server_ptr dcs_resolve(dev_t gdev, bool &is_ha);

//
// Return configuration information about a dev_t value from the DCS.
//
int dcs_get_configured_nodes(sol::dev_t, fs::dc_callback_ptr, bool &,
    CORBA::String_out, sol::nodeid_seq_t_out);

#endif	/* _DEVICE_SERVICE_MGR_H */
