//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// Supports ability to execute a program.
//

#ifndef EXEC_PROGRAM_H
#define	EXEC_PROGRAM_H

#pragma ident	"@(#)exec_program.h	1.5	08/05/20 SMI"

#include <sys/os.h>
#include <h/sol.h>
#include <orb/invo/corba.h>

//
// Use a class for namespace.
//
class exec_program {
public:
	//
	// Execute the user program 'program' on the node 'id'.  Returns a
	// non-zero value if the node 'id' is dead.  'env' may contain an
	// exception if the user program returns with an error, or if we
	// could not execute the user program after trying really hard.
	// Use 'msg_handle' to log warning messages.
	//
	static int	execute_program(const char *program, sol::nodeid_t id,
	    bool different_thread, os::sc_syslog_msg *msg_handle,
	    Environment &env);

private:

	//
	// Returns true if 'id' is a current member of the cluster, false if
	// not.
	//
	static bool	is_cluster_member(nodeid_t id);

	//
	// Function used by 'execute_program' to perform the execution of the
	// program if 'different_thread' is set to true.
	//
	static void	execution_worker_thread(void *data);

	//
	// Helper function called by 'execute_program' or 'execution_thread'
	// to do the actual execution.
	//
	static int	do_execution(const char *program, sol::nodeid_t id,
	    os::sc_syslog_msg *msg_handle, Environment &env);

	// Structure that contains the data passed in to 'execution_thread'.
	struct execution_data {
		const char		*program;
		sol::nodeid_t		id;
		os::sc_syslog_msg	*msg_handle;
		Environment		*env;
		os::condvar_t		*cv_to_wakeup;
		os::mutex_t		*lock;
		int			return_code;
	};

	static os::usec_t	sleep_in_seconds;
	static uint_t		max_retry_iterations;
	static uint_t		warning_interval;
};

#endif	// EXEC_PROGRAM_H
