//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _DEVICE_REPLICA_IMPL_H
#define	_DEVICE_REPLICA_IMPL_H

#pragma ident	"@(#)device_replica_impl.h	1.27	08/05/20 SMI"

#include <repl/service/replica_tmpl.h>
#include <h/repl_dc.h>
#include <h/pxfs.h>
#include <h/repl_pxfs.h>
#include <h/pxfs_v1.h>
#include <h/repl_pxfs_v1.h>
#include <dc/sys/dc_lib.h>
#include <dc/libdcs/libdcs.h>

//
// Used for publishing events.
//
#define	DS_STATE_PRIMARY "Primary"
#define	DS_STATE_SECONDARY "Secondary"
#define	DS_STATE_SPARE "Spare"

class device_server_repl_impl;

//
// This class is the interface between device_server_repl_impl and the HA
// framework.  Apart from the regular 'repl_server' functionality, this class
// also makes upcalls to user programs as required by state transitions.
//
class device_replica_impl :
	public repl_server<repl_dc::repl_dev_server_ckpt> {

public :
	device_replica_impl(const char *s_id, const char *p_id,
	    unsigned int dev_id, const char *takeover_pgm,
	    const char *service_nm, mdc::dc_mapper_ptr _mapper,
	    bool register_if_first);

	~device_replica_impl();

	// Create a new replica and register it with the HA framework.
	static void initialize(unsigned int dev_id,
	    const char *service_nm, const char *takeover_pgm,
	    uint_t priority, uint_t num_secondaries,
	    mdc::dc_mapper_ptr mapper, bool register_if_first);

	//
	// Required functions for replica framework.
	//
	// replica::repl_prov::=
	void become_secondary(Environment &_environment);

	void add_secondary(replica::checkpoint_ptr sec_chkpt,
	    const char *secondary_name, Environment &_environment);

	void remove_secondary(const char *secondary_name,
	    Environment &_environment);

	void freeze_primary_prepare(Environment &_environment);

	void freeze_primary(Environment &_environment);

	void unfreeze_primary(Environment &_environment);

	void become_primary(const replica::repl_name_seq &secondary_names,
	    Environment &_environment);

	void become_spare(Environment &_environment);

	void shutdown(Environment &_environment);

	void shutdown_spare(replica::repl_prov_shutdown_type
			shutdown_type, Environment &_environment);

	CORBA::Object_ptr get_root_obj(Environment &_environment);
	//

	//
	// Checkpoint operations.
	//
	// repl_dc::repl_dev_server_ckpt::=
	void ckpt_replica_list(const sol::string_seq_t &replicas,
	    Environment &_environment);

	void ckpt_add_replica(const char *new_replica,
	    Environment &_environment);

	void ckpt_remove_replica(const char *old_replica,
	    Environment &_environment);

	void ckpt_new_device_server(mdc::device_server_ptr primary_obj,
	    Environment &_environment);

	void ckpt_dump_state(mdc::device_server_ptr primary_obj,
	    const sol::string_seq_t &replicas, Environment &_environment);

	void ckpt_new_io_object(fs::fobj_ptr io_obj,
	    const fs::pvnode &real_pvnode, solobj::cred_ptr credobj,
	    Environment &_environment);

	void ckpt_new_io_object_v1(pxfs_v1::fobj_ptr io_obj,
	    const pxfs_v1::pvnode &real_pvnode, solobj::cred_ptr credobj,
	    Environment &_environment);

	void ckpt_open(fs::io_ptr io_obj, int32_t flags,
	    solobj::cred_ptr credobj, Environment &_environment);

	void ckpt_open_v1(pxfs_v1::io_ptr io_obj, int32_t flags,
	    solobj::cred_ptr credobj, Environment &_environment);

	void ckpt_close(fs::io_ptr io_obj, int32_t flags,
	    solobj::cred_ptr credobj, sol::error_t err,
	    Environment &_environment);

	void ckpt_close_v1(pxfs_v1::io_ptr io_obj, int32_t flags,
	    solobj::cred_ptr credobj, sol::error_t err,
	    Environment &_environment);

	void ckpt_ioctl_begin(Environment &_environment);

	void ckpt_ioctl_end(sol::error_t err, int32_t result,
	    Environment &_environment);

	void ckpt_locks(fs::fobj_ptr obj,
	    const repl_pxfs::lock_info_seq_t &locks,
	    Environment &_environment);

	void ckpt_locks_v1(pxfs_v1::fobj_ptr obj,
	    const repl_pxfs_v1::lock_info_seq_t &locks,
	    Environment &_environment);
	//

	// Accessor function.
	const char *get_service_name();

	//
	// Mark this replica as inactive or active.  If the state of the
	// replica is set to inactive, it is not currently the primary, and it
	// will not return successfully from 'become_primary'.
	//
	dc_error_t set_inactive(bool inactive);

	//
	// Static functions to allow/prevent primaries on this node.
	// Used during node shutdown.
	//
	static void enable_primaries();
	static bool disable_primaries();
	static bool is_disabled();

	//
	// Helper functions to manage the list of device service replicas
	// stored on each node.  These lists are used by the 'set_inactive'
	// call.
	//
	static void add_to_ds_list(device_replica_impl *dsp);
	static void remove_from_ds_list(device_replica_impl *dsp);

	// Mark the service 'service_name' as inactive or active.
	static dc_error_t set_inactive(const char *service_name, bool inactive);

	// Checkpoint accessor function
	repl_dc::repl_dev_server_ckpt_ptr	get_checkpoint_repl_dc();

private:
	enum program_type {
		MAKE_EXCLUSIVE = 0,
		MAKE_PRIMARY = 1,
		PRIMARY_TO_SECONDARY = 2,
		ADD_SECONDARY = 3,
		REMOVE_SECONDARY = 4,
		MAKE_SECONDARY = 5
	};

	//
	// Helper function called from become_secondary() and from shutdown().
	//
	int internal_become_secondary();

	void add_replica(const char *new_replica);
	void remove_replica(const char *old_replica);

	// Convert a sequence of replicas into a comma separated string.
	static char *get_nodelist(const CORBA::StringSeq &nodes);

	//
	// This function is called during replica state transitions to
	// log state changes and to invoke the user program associated
	// with this service appropriately.
	//
	int transition_notify(program_type type,
	    const char *secondaries, const char *delta);

	//
	// Maintain list of objects of this type.  Modified by 'add_to_ds_list'
	// and 'remove_from_ds_list'.
	//
	static DList<device_replica_impl>	device_replica_list;
	static os::mutex_t	device_replica_list_lock;

	static uint_t primaries_count;
	static os::mutex_t	primaries_lock;
	static bool		primaries_enabled;

	// Indicates whether this replica is inactive.
	os::mutex_t		_inactive_lock;
	bool			_inactive;

	// Indicates whether this replica is a primary.
	bool			_is_primary;

	//
	// This array keeps track of all the replicas of this
	// repl_server (including itself).
	// Since updating is always done within a framework event
	// or a checkpoint, no locking is needed.
	//
	CORBA::StringSeq	current_replicas;

	char			provider_id[dclib::NODEID_STRLEN];

	char			*takeover_program;
	char			*service_name;
	unsigned int		devid;

	//
	// This variable determines whether 'register_device_server' will be
	// called if this replica is the first primary of this service.
	// See ::initialize() for comments.
	//
	bool			register_if_first;

	// Implementation for the root object of the service.
	device_server_repl_impl *device_serverp;

	//
	// Keep a reference so become_primary() can send checkpoint
	// without causing _unreferenced() to be scheduled.
	//
	mdc::device_server_var	device_server_obj;

	// Reference to the dc_mapper object for this device service.
	mdc::dc_mapper_var	mapperobj;

	// Checkpoint proxy
	repl_dc::repl_dev_server_ckpt_ptr	_ckpt_proxy;
};

#endif	// _DEVICE_REPLICA_IMPL_H
