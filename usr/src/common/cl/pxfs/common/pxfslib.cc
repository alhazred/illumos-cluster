//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfslib.cc	1.7	08/05/20 SMI"

#include <sys/sol_conv.h>
#include <sys/rm_util.h>
#include <pxfs/common/pxfslib.h>

#define	MNTOPT_SEPARATOR	','

//
// Helper function: set the errno exception in the env.
//
// static
void
pxfslib::throw_exception(Environment &env, sol::error_t err)
{
	sol::op_e	*ex = new sol::op_e;
	ex->error = err;
	env.exception(ex);
}

//
// If there's an exception, return a corresponding errno.
// If the exception doesn't contain an errorno, return EIO.
// If no exception, but have err, return err.
//
// static
sol::error_t
pxfslib::get_err(const sol::error_t err, Environment &e)
{
	sol::error_t		error = 0;
	CORBA::Exception	*ex;
	sol::op_e		*ex_e;
	CORBA::SystemException  *sys_ex;

	if ((ex = e.exception()) != NULL) {
		if ((ex_e = sol::op_e::_exnarrow(ex)) != NULL) {
			error = ex_e->error;
		} else {
			//
			// A version exception must not occur at this point.
			// If one expects a possible version exception,
			// one must check for version exceptions and handle
			// such an event outside of this method.
			//
			ASSERT(!CORBA::VERSION::_exnarrow(e.exception()));

			//
			// If exception is a system exception, we can extract
			// the error number from the exception and must
			// return that value instead of the default EIO.
			// For all system exceptions, the error number is
			// present in the minor field.
			//
			if ((sys_ex = CORBA::SystemException::_exnarrow(ex))
				!= NULL) {
			    error = sys_ex->_minor();
			}
		}
		//
		// The code handles two cases, one, when the exception does
		// not contain an errorno and two, when the exception
		// cannot be narrowed to op_e or SystemException.
		//
		if (error == 0) {
			error = EIO;
		}
	}

	// Exception errno takes precedence.
	if (error == 0) {
		error = err;
	}
	return (error);
}

//
// Effects: Returns a non-nil object reference to the control object
// for the HA service named by "service_desc." Returns nil if the
// "service_desc" is an unknown service to the HA replica manager
// and raises the exception "replica::unknown_service." Asserts 0
// if some other exception is raised that's not listed in the IDL
// file for this function; no other exception should be raised.
//
// static
replica::service_admin_ptr
pxfslib::get_service_admin_ref(const char *, const char *service_desc,
    CORBA::Environment &e)
{
	replica::service_admin_ptr	control_ref;
	CORBA::Environment		env;
	CORBA::Exception		*ex;

	ASSERT(!e.exception());
	replica::rm_admin_var rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));

	control_ref = rm_ref->get_control(service_desc, env);
	//
	// PXFS packages its own user-defined exceptions from the callee
	// in the environment variable.  We need to examine each exception
	// and create a new one to pass back to the caller.
	//
	if ((ex = env.exception()) != NULL) {
		// If the service is unknown to the replica manager ....
		if (replica::unknown_service::_exnarrow(ex) != NULL) {
			// re-raise the exception
			e.exception(new replica::unknown_service);
		} else {
			ASSERT(0); // shouldn't get here
		}
		control_ref = replica::service_admin::_nil();
		env.clear(); // Clear the raised exception
	}

	return (control_ref);
}

//
// Looks for 'option' in 'mntoptions' - returns true if it exists.
// If strip is true, the 'option' is stripped from 'mntoptions'.
// Sadly, MNTOPT_NOCTO isn't passed to this function. UFS doesn't
// understand the option today so we don't ever see it.
//
// static
bool
pxfslib::exists_mntopt(const char *mntoptions, const char *option, bool strip)
{
	char	*tmp = const_cast<char *>(mntoptions);

	while ((tmp = os::strstr(tmp, option)) != NULL) {
		if ((tmp != mntoptions) &&
		    (*(tmp - 1) != MNTOPT_SEPARATOR)) {
			tmp += strlen(option);
			continue;
		}

		char	*optstart = tmp;
		tmp += strlen(option);
		if ((*tmp != MNTOPT_SEPARATOR) && (*tmp != 0)) {
			continue;
		}

		if (strip) {
			char	*str = optstart;
			for (str = optstart;
			    *str != ',' && *str != '\0' && *str != ' ';
			    str++) {
				// Nothing more to do
			}
			if (*str == ',') {
				str++;
			} else if (optstart != mntoptions) {
				optstart--;
			}
			//lint -e720 Boolean test of assignment
			while (*optstart++ = *str++) {
				// Nothing more to do
			}
			//lint +e720
		}
		// Found 'option' in 'mntoptions'.
		return (true);
	}

	return (false);
}

//
// Get the running version protocol for specified protocol;
//
// Error Status: 0 if call succeeded, >0 if an error occurred.
//
//    0 - call succeeded.
//    1 - NULL running_version pointer
//    2 - version manager not available
//    3 - version protocol name "pxfs" not found
//    4 - wrong mode for vp_name, such as nodepair vp.
//    5 - too early (no version has been set).
//
// static
int
pxfslib::get_running_version(char * const vp_namep,
    version_manager::vp_version_t *running_versionp)
{
	if (running_versionp == NULL) {
		return (1);
	}

	// Initialize to lowest possible version in case of error
	running_versionp->major_num = 0;
	running_versionp->minor_num = 0;

	version_manager::vm_admin_var	vm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_v)) {
		return (2);
	}

	Environment			e;
	version_manager::vp_version_t	rvers;
	vm_v->get_running_version(vp_namep, rvers, e);

	if (e.exception()) {
		if (version_manager::not_found::_exnarrow(e.exception())) {
			e.clear();
			return (3);
		}
		if (version_manager::wrong_mode::_exnarrow(e.exception())) {
			e.clear();
			return (4);
		}
		if (version_manager::too_early::_exnarrow(e.exception())) {
			e.clear();
			return (5);
		}
		e.clear();
		return (2);
	}

	running_versionp->major_num = rvers.major_num;
	running_versionp->minor_num = rvers.minor_num;
	return (0);
}
