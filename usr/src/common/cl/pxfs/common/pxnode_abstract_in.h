//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxnode_abstract_in.h	1.5	08/05/20 SMI"

//
// Convert vnode to pxnode_abstract
//
// static
inline pxnode_abstract	*
pxnode_abstract::vnode_to_pxnode_abstract(vnode_t *vp)
{
	ASSERT(vp->v_data != NULL);
	return ((pxnode_abstract *)vp->v_data);
}

#ifdef FSI

inline vnode_t *
pxnode_abstract::get_vp()
{
	return (pxnode_vnodep);
}

#else // FSI not defined

inline vnode_t *
pxnode_abstract::get_vp()
{
	return ((vnode_t *)this);
}

#endif	// FSI

#ifndef FSI

//
// Vnode accessor functions:
//
// The following functions are defined in Solaris when FSI is defined,
// but not otherwise.  Thus we provide the following functions for
// backward compatibility.
//

inline int
vn_is_readonly(vnode_t *vp)
{
	return (vp->v_vfsp->vfs_flag & VFS_RDONLY);
}

inline int
vn_has_flocks(vnode_t *vp)
{
	return (vp->v_filocks != NULL);
}

inline int
vn_has_mandatory_locks(vnode_t *vp, int mode)
{
	return ((vp->v_filocks != NULL) && (MANDLOCK(vp, mode)));
}

inline int
vn_has_cached_data(vnode_t *vp)
{
	return (vp->v_pages != NULL);
}

//
// Return nonzero if the vnode is a mount point, zero if not.
//
inline int
vn_ismntpt(vnode_t *vp)
{
	return (vp->v_vfsmountedhere != NULL);
}

// Retrieve the vfs (if any) mounted on this vnode
inline vfs_t *
vn_mountedvfs(vnode_t *vp)
{
	return (vp->v_vfsmountedhere);
}

#endif /* FSI not defined */
