//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef PXNODE_ABSTRACT_H
#define	PXNODE_ABSTRACT_H

#pragma ident	"@(#)pxnode_abstract.h	1.5	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/vfs.h>

#include <sys/sol_version.h>

/*
 * Vnode separation: File System Interfaces (FSI) requirement in Solaris 10
 *
 *     The file system's private node can no longer contain
 *     the vnode itself.  It can only contain a pointer to
 *     the vnode.  This was needed in order to enable
 *     future evolution of the vnode without affecting file systems.
 *     As seen below, the vnode used to be embedded in the pxnode object.
 *     Starting with Solaris 10, the pxnode object contains a pointer to the
 *     vnode instead.
 */
#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

//
// pxnode_abstract - this class provides a means to identify the version
// of a pxnode object associated with a proxy vnode.
//
#ifdef FSI
class pxnode_abstract
#else
class pxnode_abstract : public vnode
#endif
{
public:
	pxnode_abstract(vfs_t *vfsp, enum vtype vnode_type,
	    dev_t vnode_rdev, uint_t vnode_flag, struct vnodeops *vnodeopsp);

	virtual	~pxnode_abstract();

	enum	pxnode_version {
			VERSION_0 = 0,
			VERSION_1 = 1
	};

	virtual pxnode_version	get_pxnode_version() = 0;

	// Convert vnode to pxnode_abstract
	static pxnode_abstract	*vnode_to_pxnode_abstract(vnode_t *vp);

	vnode_t		*get_vp(void);

private:
#ifdef FSI
	vnode_t		*pxnode_vnodep;
#endif

	// Disallow assignments and pass by value.
	pxnode_abstract();
	pxnode_abstract(const pxnode_abstract &);
	pxnode_abstract &operator = (pxnode_abstract &);
};

#ifdef FSI

// Vnode Accessors

int		vn_is_readonly(vnode_t *);
int		vn_has_flocks(vnode_t *);
int		vn_has_mandatory_locks(vnode_t *, int);
int		vn_has_cached_data(vnode_t *);
int		vn_ismntpt(vnode_t *);
struct vfs	*vn_mountedvfs(vnode_t *);

#endif	// FSI

#include <pxfs/common/pxnode_abstract_in.h>

#endif	// PXNODE_ABSTRACT_H
