//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfslib_in.h	1.4	08/05/20 SMI"

// static
inline nodeid_t
pxfslib::get_nodeid(sol::lsysid_t sysid)
{
	return ((unsigned)sysid >> sol::bits_in_sysid);
}

// static
inline sol::lsysid_t
pxfslib::set_nodeid(sol::lsysid_t sysid, nodeid_t nodeid)
{
	return (sysid | (nodeid << sol::bits_in_sysid));
}

// static
inline sol::error_t
pxfslib::get_err(Environment &e)
{
	return (get_err(0, e));
}
