//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_main.cc	1.66	08/05/20 SMI"

//
// The purpose of this file is to support C++ startup and shutdown methods
// for module load and unload.
//
#include <sys/vfs.h>
#include <sys/mount.h>

#include <sys/sol_version.h>
#include <solobj/solobj_handler.h>
#include <pxfs/common/pxfslib.h>

#include <h/pxfs.h>
#include <pxfs/common/pxfslib.h>
#include <pxfs/mount/mount_debug.h>
#include <pxfs/mount/mount_replica_impl.h>
#include <pxfs/mount/mount_client_impl.h>
#include <pxfs/lock/fs_collection_impl.h>
#include <pxfs/server/fs_base_impl.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxdir.h>
#include <pxfs/bulkio/bulkio_impl.h>

#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

extern void pxfs_check_assertions(void);
extern int cl_flk_init(void);
extern int px_vfs_init(void);
extern void px_vfs_uninit(void);
extern int px_vn_init(char *namep);
extern void px_vn_uninit(void);
extern int pgio_vn_init(void);
extern void pgio_vn_uninit(void);

extern void nlm_init(void);

//
// The following variable determines the highest version of pxfs software
// that can be used to support a mounted file system. The version manager
// provides information about the highest version of pxfs software that is
// available on the cluster.
//	1 = version 1
//	2 = version 2
//
// This parameter can be set in "/etc/system". It is meant for debugging
// and is not supported.
//
int	pxfs_software_mount_level = 2;

//
// This variable can be used to enable/disable fastwrites through /etc/system.
// To enable fastwrite,  pxfs_fastwrite_enabled should be set to 1.
// To disable fastwrite, pxfs_fastwrite_enabled should be set to 0.
//
// NOTE : Please make sure that it has the same value on all the nodes
//	of the cluster.
//


int pxfs_fastwrite_enabled = 1;


//
// This section defines a set of operation functions for the proxy vfs.
// The purpose of this set of operation function is to capture the
// first mount and determine which version of pxfs software should
// support this particular file system. The system will then replace
// this set of operation functions with a set for that file system.
//
// This set of operation functions also supports a sync of all pxfs type
// file systems.
//
// None of the other vfs functions should be called.
//
static int px_mount(vfs_t *vfsp, vnode_t *mvp, struct mounta *uap, cred_t *cr);
static int px_mountroot(vfs_t *vfsp, enum whymountroot why);
static int px_unmount(vfs_t *vfsp, int flags, cred_t *cr);
static int px_root(vfs_t *vfsp, vnode_t **vpp);
static int px_statvfs(vfs_t *vfsp, struct statvfs64 *sp);
static int px_sync(vfs_t *vfsp, short flag, cred_t *cr);
static int px_vget(vfs_t *vfsp, vnode_t **vpp, struct fid *fidp);
static int px_swapvp(vfs_t *vfsp, vnode_t **vpp, char *nm);

#ifdef FSI

struct vfsops	*pxfs_vfs_opsp;

//
// pxfs_vfs_init - initializes the data structures, including the
// operations vectors, for the pxfs vfs data structures.
//
extern "C" int
pxfs_vfs_init(int fstype, char *)
{
	static const fs_operation_def_t px_vfsops_template[] = {
		VFSNAME_MOUNT, (fs_generic_func_p)px_mount,
		VFSNAME_UNMOUNT, (fs_generic_func_p)px_unmount,
		VFSNAME_ROOT, (fs_generic_func_p)px_root,
		VFSNAME_STATVFS, (fs_generic_func_p)px_statvfs,
		VFSNAME_SYNC, (fs_generic_func_p)px_sync,
		VFSNAME_VGET, (fs_generic_func_p)px_vget,
		VFSNAME_MOUNTROOT, (fs_generic_func_p)px_mountroot,
		"swapvp", (fs_generic_func_p)px_swapvp,
		VFSNAME_FREEVFS, (fs_generic_func_p)fs_freevfs,
		NULL, NULL
	};

	int error;

	//
	// Create the generic (version independent) vfs operations for pxfs
	//
	error = vfs_setfsops(fstype, px_vfsops_template, &pxfs_vfs_opsp);
	if (error != 0) {
		return (error);
	}

	//
	// Create vfs operations for pxfs
	//
	error = px_vfs_init();
	if (error != 0) {
		(void) vfs_freevfsops_by_type(fstype);
		return (error);
	}

	pxvfs::setfstype(fstype);
	return (0);
}

//
// pxfs_vfs_uninit - undoes the vfs initialization for pxfs
//
extern "C" void
pxfs_vfs_uninit(int fstype)
{
	(void) vfs_freevfsops_by_type(fstype);
	px_vfs_uninit();
}

//
// pxfs_vn_init - initializes the vnode data structures for pxfs
//
extern "C" int
pxfs_vn_init(char *namep)
{
	int error;

	// Initialize vnode data structures for pxfs version 1
	error = px_vn_init(namep);
	if (error != 0) {
		return (error);
	}

	// Initialize vnode data structures for bulkio subsystem
	// for pxfs version 1
	error = pgio_vn_init();
	if (error != 0) {
		px_vn_uninit();
		return (error);
	}

	return (error);
}

#else	// FSI

struct vfsops pxfs_vfsops = {

	px_mount,
	px_unmount,
	px_root,
	px_statvfs,
	px_sync,
	px_vget,
	px_mountroot,
	px_swapvp,
	fs_freevfs
};

struct vfsops *pxfs_vfs_opsp = &pxfs_vfsops;

//
// pxfs_vfs_init is called when the pxfs module is loaded
//
extern "C" void
pxfs_vfs_init(int fstype)
{
	pxvfs::setfstype(fstype);
}

#endif	// FSI

static int
px_mount(vfs_t *vfsp, vnode_t *mvp, struct mounta *uap, cred_t *credp)
{
	//
	// A remount operation uses the already existing vfs of the mounted
	// file system. The mounted file system will have a vfs that already
	// has the correct operations vector for the chosen pxfs software
	// version. Thus a remount operation should not arrive here.
	//
	ASSERT((vfsp->vfs_flag & VFS_REMOUNT) == 0);

	ASSERT(uap->flags & MS_GLOBAL);
	//
	// "mount -g" mounts use the static form of the
	// mount method.
	//

#ifdef FSI
	vfs_setops(vfsp, pxfs_vfsopsp);
#else
	vfsp->vfs_op = pxfs_vfsopsp;
#endif
	//
	// This method allocates
	// the pxvfs object after determining the fs reference.
	//
	return (pxvfs::mount(vfsp, mvp, uap, credp));
}

static int
px_unmount(vfs_t *, int, cred_t *)
{
	ASSERT(0);
	return (ENOTSUP);
}

static int
px_mountroot(vfs_t *, enum whymountroot)
{
	ASSERT(0);
	return (ENOTSUP);
}

static int
px_root(vfs_t *, vnode_t **)
{
	ASSERT(0);
	return (ENOTSUP);
}

static int
px_statvfs(vfs_t *, struct statvfs64 *)
{
	ASSERT(0);
	return (ENOTSUP);
}

//
// vfsp will be non-NULL if this is a sync in order to unmount one file system.
// vfsp will be NULL if this is a sync of all PXFS type file systems.
//
static int
px_sync(vfs_t *vfsp, short flag, cred_t *credp)
{
	if (vfsp != NULL) {
		ASSERT(0);
		return (ENOTSUP);
	} else {
		return (pxvfs::sync_all(flag, credp));
	}
}

static int
px_vget(vfs_t *, vnode_t **, struct fid *)
{
	ASSERT(0);
	return (ENOTSUP);
}

static int
px_swapvp(vfs_t *, vnode_t **, char *)
{
	ASSERT(0);
	return (ENOTSUP);
}

extern "C" int
stop_pxfs_server()
{
	// XXX need to check that all memory freed, no _unreferenced()
	// pending, etc.
	return (EBUSY);
}

//
// When a cluster node is going down cleanly (using init 0), we don't
// want that node to unmount all PXFS mounts on the way down (init 0
// calls umountall).  So we disable global unmounts via a rc0.d/K
// script that uses the cladm utility.  Any
// unmounts after that will see 'unmount_disabled' set to true, and
// return EBUSY.
//
extern "C" void
pxfs_disable_unmounts()
{
	pxvfs::disable_unmounts();
}

void
support_disable_unmount()
{
	extern void (*pxfs_disable_unmounts_ptr)(void);
	pxfs_disable_unmounts_ptr = pxfs_disable_unmounts;
}

extern "C" int
pxfs_main_startup()
{
	int	error;

#ifdef	DEBUG
	pxfs_check_assertions();
#endif	/* DEBUG */

	if ((error = Solobj_handler_kit::initialize()) != 0 ||

	    (error = fs_base_ii::startup()) != 0 ||

	    (error = mount_replica_impl::startup()) != 0 ||

	    (error = fs_collection_impl::startup()) != 0 ||

	    (error = cl_flk_init()) != 0 ||

	    (error = pxdir::startup()) != 0 ||

	    (error = access_cache::startup()) != 0 ||

	    (error = bulkio_impl::startup()) != 0 ||

	    (error = pxfs_mount_client_startup()) != 0 ||

	    (error = pxvfs::startup()) != 0) {
		return (error);
	}
	support_disable_unmount();
	nlm_init();
	return (0);
}
