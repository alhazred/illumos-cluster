//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FOBJ_ABSTRACT_H
#define	FOBJ_ABSTRACT_H

#pragma ident	"@(#)fobj_abstract.h	1.5	08/05/20 SMI"

//
// fobj_abstract - this class provides a means to identify the version
// of a file object.
//
class fobj_abstract {
public:
	fobj_abstract() {};
	virtual	~fobj_abstract() {};

	enum	fobj_version {
			VERSION_0 = 0,
			VERSION_1 = 1
	};

	virtual fobj_version	get_fobj_version() = 0;

private:
	// Disallow assignments and pass by value.
	fobj_abstract(const fobj_abstract &);
	fobj_abstract &operator = (fobj_abstract &);
};

#endif	// FOBJ_ABSTRACT_H
