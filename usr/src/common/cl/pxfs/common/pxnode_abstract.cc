//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxnode_abstract.cc	1.4	08/05/20 SMI"

#include <pxfs/common/pxnode_abstract.h>

#ifdef FSI

#include <sys/kmem.h>

pxnode_abstract::pxnode_abstract(vfs_t *vfsp, enum vtype vnode_type,
    dev_t vnode_rdev, uint_t vnode_flag, struct vnodeops *vnodeopsp)
{
	pxnode_vnodep = vn_alloc(KM_SLEEP);

	VN_SET_VFS_TYPE_DEV(pxnode_vnodep, vfsp, vnode_type, vnode_rdev);
	(void) vn_setops(pxnode_vnodep, vnodeopsp);
	pxnode_vnodep->v_data = (caddr_t)this;
	pxnode_vnodep->v_flag = vnode_flag | VPXFS;
}

pxnode_abstract::~pxnode_abstract()
{
	vn_free(pxnode_vnodep);
	pxnode_vnodep = NULL;
}

#else

pxnode_abstract::pxnode_abstract(vfs_t *vfsp, enum vtype vnode_type,
    dev_t vnode_rdev, uint_t vnode_flag, struct vnodeops *vnodeopsp)
{
	//
	// This step initializes the vnode count to one,
	// which in effect places a hold (VN_HOLD) on the vnode.
	//
	//lint -e666
	VN_INIT(get_vp(), vfsp, vnode_type, vnode_rdev);
	//lint +e666
	v_op = vnodeopsp;
	v_data = (caddr_t)this;
	v_filocks = NULL;
	v_shrlocks = NULL;
	v_vfsmountedhere = NULL;
	cv_init(&v_cv, NULL, CV_DEFAULT, NULL);
	v_flag = vnode_flag | VPXFS;
}

pxnode_abstract::~pxnode_abstract()
{
}

#endif
