//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// Check various assertons that could not be checked at compile time.
//

#pragma ident	"@(#)pxfs_check.cc	1.20	08/05/20 SMI"

#ifdef	DEBUG

#include <sys/vnode.h>
#include <sys/acl.h>
#include <sys/statvfs.h>

#include <h/sol.h>
#include <sys/os.h>

#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#ifndef	BUG_4954775
#define	BUG_4954775
#endif
#endif

static void check_sol_vtype(void);
static void check_fobj_attr(void);
static void check_statvfs(void);

void
pxfs_check_assertions()
{
	check_sol_vtype();
	check_fobj_attr();
	check_statvfs();
}

static void
check_sol_vtype()
{
	//
	// PXFS requires that the sol.idl vtype_t and Solaris vnode type
	// values match.
	//
#ifdef BUG_4954775
	if ((vtype)sol::VNON != VNON || (vtype)sol::VREG != VREG ||
	    (vtype)sol::VDIR != VDIR || (vtype)sol::VBLK != VBLK ||
	    (vtype)sol::VCHR != VCHR || (vtype)sol::VLNK != VLNK ||
	    (vtype)sol::VFIFO != VFIFO || (vtype)sol::VDOOR != VDOOR ||
	    (vtype)sol::VPRC != VPROC || (vtype)sol::VSOCK != VSOCK ||
	    (vtype)sol::VBAD != VBAD || (vtype)sol::VPORT != VPORT) {
		os::panic("check_sol_vtype");
	}
#else
	if ((vtype)sol::VNON != VNON || (vtype)sol::VREG != VREG ||
	    (vtype)sol::VDIR != VDIR || (vtype)sol::VBLK != VBLK ||
	    (vtype)sol::VCHR != VCHR || (vtype)sol::VLNK != VLNK ||
	    (vtype)sol::VFIFO != VFIFO || (vtype)sol::VDOOR != VDOOR ||
	    (vtype)sol::VPRC != VPROC || (vtype)sol::VSOCK != VSOCK ||
	    (vtype)sol::VBAD != VBAD) {
		os::panic("check_sol_vtype");
	}
#endif
}

static void
check_fobj_attr()
{
	//
	// fobj_impl::get_attributes and fobj_impl::set_attributes depend
	// on the layout of the type sol::vattr_t being equal to the
	// layout of vattr_t.
	//
	if (sizeof (sol::vattr_t) != sizeof (vattr_t)) {
		os::panic("check_sol_vattr_t");
	}
	//
	// XXX - perhaps we should check the offset of each field
	//

	//
	// fobj_ii::get_secattributes() and fobj_ii::set_secattributes()
	// depend on the layout of sol::aclent_t being the same as
	// struct aclent.
	//
	if (sizeof (sol::aclent_t) != sizeof (struct acl)) {
		os::panic("check_sol_aclent_t");
	}
}

static void
check_statvfs()
{
	if (sizeof (sol::statvfs64_t) != sizeof (statvfs64_t)) {
		os::panic("check_statvfs");
	}
	//
	// XXX - perhaps we should check the offset of each field
	//
}

#endif	/* DEBUG */
