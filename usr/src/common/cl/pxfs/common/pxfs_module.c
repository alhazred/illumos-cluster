/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pxfs_module.c	1.49	08/07/17 SMI"

#include <sys/errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/time.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <rpc/types.h>
#include <sys/systm.h>
#include <sys/modctl.h>

#include <sys/sol_version.h>
#include <cplplrt/cplplrt.h>

/*
 * As part of the File System Interfaces (FSI) project,
 * the following changes were made in Solaris 10.
 *
 * (i) Vnode separation
 *
 *     The file system's private node can no longer contain
 *     the vnode itself.  It can only contain a pointer to
 *     the vnode.  This has been necessitated in order to enable
 *     future evolution of the vnode without affecting file systems.
 *     For PxFS, these changes are incorporated in the proxy vnode,
 *     and the page io vnode used in the bulkio layer.
 *
 * (ii) Operation registration
 *
 *     The file system's vnode and vfs operations are now registered
 *     with the kernel, and the kernel generates the operation vectors.
 *     New interfaces have been introduced for registration purposes.
 *
 * (iii) Module loading
 *
 *     This includes the changes caused to the module loading process
 *     due to the new operation registration mechanism.  The virtual
 *     file system switch (vfssw) data structure has been replaced
 *     by the virtual file system definitions (vfsdef_t) data structure.
 *     The vfsdef_t data structure adds a version number and removes
 *     the pointer to the operations vector.
 */
#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

extern int cl_orb_is_initialized(void);

extern int pxfs_main_startup(void);

#ifdef FSI
extern int pxfs_vfs_init(int fstyp);
extern void pxfs_vfs_uninit(int fstype);
extern int pxfs_vn_init(char *namep);
#else
extern struct vfsops pxfs_vfsops;
extern void pxfs_vfs_init(int fstyp);
#endif

/* inter-module dependencies */
char _depends_on[] = "misc/cl_runtime misc/cl_load misc/cl_orb misc/cl_haci "
	"misc/cl_comm misc/cl_dcs fs/ufs misc/klmmod fs/procfs";

#ifdef FSI

static int
pxfs_init(int fstyp, char *name)
{
	int error;

	/*
	 * The vfs operations are registered here.
	 * Save the file system type number assigned by the kernel
	 * for later use.  It is saved in the proxy vfs data structure.
	 */
	error = pxfs_vfs_init(fstyp);
	if (error != 0) {
		return (error);
	}

	/*
	 * The vnode operations are registered here.
	 */
	error = pxfs_vn_init(name);
	if (error != 0) {
		pxfs_vfs_uninit(fstyp);
		return (error);
	}

	return (0);
}

#else

static int
pxfs_init(struct vfssw *vswp, int fstyp)
{
	/*
	 * This is the pre-FSI version of the function for vfs
	 * initializations.  This was a way of providing the kernel
	 * with the location of the file system's operation vector.
	 * This was also the way by which the kernel notified the
	 * the file system of the type number assigned to it.
	 * This number is the index into the virtual file system switch(vfssw)
	 * table that contains information on all file system types.
	 * The index identifies the table entry for a specific
	 * filesystem in the vfssw table.
	 */
	vswp->vsw_vfsops = &pxfs_vfsops;
	pxfs_vfs_init(fstyp);
	return (0);
}

#endif	/* FSI */

/*
 * With Solaris 10, the virtual file system switch (vfssw) data structure
 * has been replaced by the virtual file system definitions (vfsdef_t) data
 * structure.  The vfsdef_t data structure adds a version number,
 * and removes the pointer to the operations vector.  The operations
 * will be registered by the initialization routine (pxfs_init).
 */

#ifdef FSI
static vfsdef_t		px_fsops = {
	/* LINTED: Too few initializers for aggregate */
	VFSDEF_VERSION, "pxfs", pxfs_init, VSW_CANREMOUNT, NULL
};
#else
static struct vfssw	px_fsops = {
	/* LINTED: Too few initializers for aggregate */
	"pxfs", pxfs_init, &pxfs_vfsops, 0, NULL
};
#endif

/*
 * Module linkage information for the pxfs kernel module.
 */
static struct modlfs		modlfs = {
	&mod_fsops, "PXFS client code", &px_fsops
};

static struct modlinkage	modlinkage = {
	/* LINTED: Too few initializers for aggregate */
	MODREV_1, { (void *)&modlfs, NULL }
};

/*
 * This is called when the module is initialized.
 * If we return an error here, the module will not be installed
 * but not necessarily unloaded as well. In this case,
 * it is possible for _init() to be called more than once and
 * common variables changed the first time will not be reinitialized (cleared).
 */
int
_init()
{
	int error;

	if (!cl_orb_is_initialized()) {
		return (EAGAIN);
	}

	if ((error = mod_install(&modlinkage)) != 0) {
		return (error);
	}
	_cplpl_init();		/* C++ initialization */

	if ((error = pxfs_main_startup()) != 0) {
		_cplpl_fini();
		return (error);
	}

	return (0);
}

/*
 * Note: don't define a _fini() routine (and thus allow module unloading)
 * unless you also change usr/src/uts/{sparc,i86}/ml/modstubs.s and
 * change NO_UNLOAD_WSTUB() to WSTUB() for this module.
 */

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
