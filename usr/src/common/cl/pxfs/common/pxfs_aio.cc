//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_aio.cc	1.7	08/05/20 SMI"

#include <sys/aio_req.h>
#include <sys/cred.h>

#include <pxfs/common/pxnode_abstract.h>
#include <pxfs/client/pxnode.h>
#include <pxfs/client/pxchr.h>

extern "C" int
clpxfs_aio_read(vnode_t *vnodep, struct aio_req *aiop, cred_t *credp)
{
	ASSERT(IS_PXFSVP(vnodep));
	ASSERT(vnodep->v_type == VCHR);

	return (((pxchr *)
	    pxnode::VTOPX(vnodep))->pxfs_aiorw(aiop, credp, 0, false));
}

extern "C" int
clpxfs_aio_write(vnode_t *vnodep, struct aio_req *aiop, cred_t *credp)
{
	ASSERT(IS_PXFSVP(vnodep));
	ASSERT(vnodep->v_type == VCHR);

	return (((pxchr *)
	    pxnode::VTOPX(vnodep))->pxfs_aiorw(aiop, credp, 1, false));
}
