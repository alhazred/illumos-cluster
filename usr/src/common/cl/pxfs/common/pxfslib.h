//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//  Miscellaneous library functions that are shared by multiple subsystems.
//  These functions are not dependent upon any particular version of pxfs.
//

#ifndef _PXFSLIB_H
#define	_PXFSLIB_H

#pragma ident	"@(#)pxfslib.h	1.8	08/05/20 SMI"

#include <sys/errno.h>

#include <h/sol.h>
#include <h/replica.h> // XXX remove when get_service_admin_ref() is removed
#include <sys/vm_util.h>

//
// Use a class for namespace.
//
class pxfslib {
public:
	//
	// Looks for 'option' in 'mntoptions' - returns true if it exists.
	// If strip is true, strip out 'option' from 'mntoptions'.
	//
	bool static		exists_mntopt(const char *mntoptions,
	    const char *option, bool strip);

	// Extract the node id from the sysid value.
	static nodeid_t		get_nodeid(sol::lsysid_t sysid);

	// Set the node id field in the given sysid.
	static sol::lsysid_t	set_nodeid(sol::lsysid_t sysid,
	    nodeid_t nodeid);

	// Throw an exception via the Environment mechanism.
	static void		throw_exception(Environment &env,
	    sol::error_t err);

	//
	// Map the exception (if any) to a sol::error_t.
	// If there's an exception, but we can't dig an error_t
	// out of it, return EIO.
	//
	static sol::error_t	get_err(const sol::error_t err,
	    Environment &env);

	// Single argument version.
	static sol::error_t	get_err(Environment &env);

	static replica::service_admin_ptr	get_service_admin_ref(
	    const char *errmsg, const char *service_desc,
	    CORBA::Environment &e);

	static int		get_running_version(char * const vp_namep,
	    version_manager::vp_version_t *running_versionp);
};

#include <pxfs/common/pxfslib_in.h>

#if	SOL_VERSION >= __s11
/*
 * At a bare minimum, the file system should add the following file that uses
 * the vnode operation registration mechanism. (See PSARC/2007/124).
 * This means the file system does not take advantage of the strong
 * type-checking.
 */
#include <sys/vfs_opreg.h>
#endif

#endif	// _PXFSLIB_H
