/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


/*	Copyright (c) 1983, 1984, 1985, 1986, 1987, 1988, 1989 AT&T	*/
/*	  All Rights Reserved	  */

/*
 * University Copyright- Copyright (c) 1982, 1986, 1988
 * The Regents of the University of California
 * All Rights Reserved
 *
 * University Acknowledgment- Portions of this document are derived from
 * software developed by the University of California, Berkeley, and its
 * contributors.
 */

/*
 * Portions of this source code were derived from Berkeley 4.3 BSD
 * under license from the Regents of the University of California.
 */


#pragma ident	"@(#)pxreg.cc	1.51	09/01/13 SMI"

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/file.h>
#include <sys/flock.h>
#include <sys/vmsystm.h>
#include <sys/debug.h>
#include <sys/flock.h>
#include <sys/kstat.h>
#include <sys/uio.h>
#include <sys/cmn_err.h>

#include <vm/as.h>
#include <vm/seg.h>
#include <vm/seg_vn.h>
#include <vm/seg_map.h>
#include <vm/pvn.h>
#include <vm/page.h>

#include <sys/os.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include <pxfs/common/pxfslib.h>

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxreg.h>
#include <pxfs/client/mem_async.h>
#include <pxfs/client/fobj_client_impl.h>
#include <pxfs/bulkio/bulkio_impl.h>

#if SOL_VERSION == __s9
#define	_BUG_4924098
#endif

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//
// XXX
// DEF_IOV_MAX is not defined in any Solaris header file. It is instead
// defined in uts/common/syscall/rw.c . A PSARC contract needs to be pursued
// to get DEF_IOV_MAX defined in a header file that we can include.
//
#define	DEF_IOV_MAX	16	//lint -e760

#define	LIVELOCK_DELAYTIME(seqnum, random)	\
	(clock_t)((((unsigned int)random >> 10) & 0x3f) & ((1 << seqnum) - 1))

#define	GET_MOD_TIME(pxregp)						\
	((pxregp)->last_write_timestamp != 0 ?				\
	    ((pxregp)->allocate_timestamp +				\
		(os::gethrtime() - (pxregp)->last_write_timestamp)) :	\
	    (pxregp)->allocate_timestamp)

//
// global delay time for write_back_and_delete(),
// write_back_and_downgrade_to_read_only(), and
// delete_range()
//
clock_t pxfs_inval_delay = 10;

//
// Fudge factor to account for indirect and double-indirect blocks when
// reserving file space for writes.
//

const int space_fudge_factor = 2;

//
// Truncate a file could need blocks. Because, the underlying FS allocates
// a block (if one does not already exist) to set the new size. To account
// for this, we reserve 3 blocks assuming the worst case of triple indirection.
//
const size_t	blocks_for_truncate = 3;

//
// PxFS readahead variables
//

//
// A non-zero value enable read-ahead
//
unsigned int pxfs_readahead = 1;

//
// This variable decides how early the read-ahead needs to be kicked off.
// Eg. if it is set to 16 then the read-ahead would be kicked off before
// the number of available pages (paged-in) falls below 16 pages.
//
u_offset_t pxfs_rdahead_lead = 16 * PAGESIZE;

// This is the read-ahead size in terms of pages.
unsigned int pxfs_rdahead_size = 8;

//
// "bounced" indicates whether we are bouncing putpage requests or
// queuing them for processing via async tasks. This variable is not
// protected as the overhead of acquiring and releasing a lock on
// every page_out would be crippling. The worse that could happen is
// an extra page left dirty in memory for later processing.
//
bool		bounced = 0;

//
// High and low water marks for async task queue lengths at different
// memory levels for deciding page disposition while doing putpage.
// The comment for each value indicates the maximum memory that can be
// locked in pages on the async task queue for that count.
//
int PLENTY_LOW = 320;	// low when memory is plenty (160 mb in queue)
int PLENTY_HIGH = 2400;	// high when memory is plenty (1.2 gb in queue)
int PURGE_LOW = 32;	// low when memory is at purge (16 mb in queue)
int PURGE_HIGH = 64;	// high when memory is at purge (32 mb in queue)
int MEMLOW_LOW = 16;	// when memory is low (8 mb in queue)

//
// Tell whether the caller is a system thread or a from a user or
// non-local process like an nfs-client.
//
#define	IS_SYS_THREAD() (curproc == proc_pageout || curproc == proc_fsflush)

//
// Initialize the TSD key used to store and retrieve values specific to
// threads doing synchronous reads.
//
os::tsd		pxreg::sync_read_tsd;

//
// Return 1 if sequential read should free pages because the system is
// low on free memory.
//
inline int
pxreg::free_behind_read()
{
	return (monitor::the().get_current_state() != monitor::MEMORY_PLENTY);
}

pxreg::pxreg(fobj_client_impl *fclientp,
    vfs *vfsp, PXFS_VER::file_ptr filep, const PXFS_VER::fobj_info &fobjinfo) :
	pxfobjplus(fclientp, vfsp, filep, fobjinfo),
	delay_off(0),
	current_off(0),
	cur_pagedin_off(0),
	ext_offset(0),
	delay_len(0),
	ext_length(0),
	seqnum(0),
	rwlocks_granted(0),
	threads_waiting_for_token(0),
	threads_in_reserve_blocks(0),
	cur_data_token(0),
	req_data_token(0),
	cachedata_flag(true),
	truncate_in_progress(false),
	size_in_sync(true),
	allocate_timestamp(0),
	last_write_timestamp(0),
	mmap_pages(0),
	segmap_release_offset(0),
	aios_pending(0),
	sync_write_in_progress(false),
	cached_bandwidth(0)
{
	if (clientp != NULL) {
		//
		// Must have an fobj_client in order to support caching
		//
		pxflags |= PX_CACHE_DATA;
	}

#ifndef VXFS_DISABLED
	if ((pxvfs::VFSTOPXFS(get_vp()->v_vfsp))->get_underlying_fs_type() ==
	    pxvfs::VXFS) {
		vxfs_set_sync_on_close(true);
	} else {
		sync_on_close = false;
	}
#else
	sync_on_close = false;
#endif
}

pxreg::~pxreg()
{
	// All data pages should have been flushed
	ASSERT(!vn_has_cached_data(get_vp()));
}

//
// become_stale
//
// We used to flush dirty pages in vnode operation inactive()
// via VOP_PUTPAGE(). However, if
// VOP_INACTIVE() is called from a ufs transaction, for example when
// a file create flushes an old DNLC entry, then a race can result
// with pageout in a competing thread. The competing thread may lock
// its pages in preparation for I/O and then attempt to enter a
// moby transaction. But if this thread is bound to the next moby
// transaction it will block until the current transaction is
// complete. If the INACTIVE thread then blocks on the locked pages,
// the current moby transaction will never complete and deadlock results.
//
// Here the flush is processed by a threadpool thread and
// removes this transactional dependency.
//
void
pxreg::become_stale()
{
	vnode	*vnodep = pxnode::PXTOV(this);
	int	error = 0;

	// Avoid any possible deadlock with threads populating pages.
	check_wait_lock_cache();

	// Push all dirty pages to the server and destroy all pages.
	error = putpage_remote((u_offset_t)0, (size_t)0,
	    B_INVAL | B_FORCE, kcred);

	if (error == 0) {
		// vnode must not have pages now.
		CL_PANIC(!vn_has_cached_data(vnodep));
	}

	cache_lock.unlock();

	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("become_stale(%p) putpage error %d\n",
		    this, error));
	}

#ifdef DEBUG
	state_lock.lock();
	ASSERT(!vn_has_cached_data(get_vp()) || (pxflags & PX_RECYCLED));
	state_lock.unlock();
#endif

	pxfobjplus::become_stale();
}

//
// is_cached - Return true if this file is caching info is enabled
// (i.e., direct I/O is disabled).
//
bool
pxreg::is_cached()
{
	return (cachedata_flag &&
	    ((pxflags & (PX_CACHE_ATTRIBUTES | PX_CACHE_DATA)) ==
	    (PX_CACHE_ATTRIBUTES | PX_CACHE_DATA)));
}

//
// install_cachedata_flag - set the flag for directio.
//
void
pxreg::install_cachedata_flag(bool cached)
{
	cachedata_flag = cached;

	//
	// we don't support mmap with caching. Hence,
	// unset/set the vnode VNOMAP flag depending on whether
	// cached/uncached
	//
	if (cachedata_flag) {
		get_vp()->v_flag &= ~VNOMAP;
	} else {
		get_vp()->v_flag |= VNOMAP;
	}
}

int
pxreg::read(uio *uiop, int ioflag, cred *credp)
{
	ASSERT(reg_rwlock.lock_held());

	if (uiop->uio_resid == 0) {
		return (0);
	}

	if (uiop->uio_loffset < (offset_t)0) {
		return (EINVAL);
	}

	//
	// If data caching is disabled, go direct.
	//
	if (!is_cached()) {
		//
		// Caching is off. There should not be any cached
		// pages on the client at this point.
		//
		ASSERT(!vn_has_cached_data(get_vp()));
		return (read_uncached(uiop, ioflag, credp));
	}

	return (read_cache(uiop, ioflag, credp));
}

int
pxreg::write(uio *uiop, int ioflag, cred *credp)
{
	int error = 0;

	//
	// We used to assert here if the "write" lock was not
	// granted under a concurrent directio writes condition:
	//
	// if (pxfobj::is_cached() || (curthread->t_flag & T_DONTPEND))
	// 	ASSERT(reg_rwlock.write_held());
	//
	// Of course, we would trip this assert if a directio went
	// from on to off right after we rwlocked.
	//
	// We've now implemented a barrier to prevent any directio OFF
	// ioctls from comming in until all directio writes have
	// completed.  This will protect against corruption where
	// the cache is loaded before the directio write finishes.
	//
	// Unfortunately, since the barrier only protects the actual write
	// sequence (VOP_RWLOCK(), VOP_WRITE(), VOP_RWUNLOCK()) down
	// to the UFS filesystem, we still can transition *after* this
	// sequence has finished (and we're still holding the read
	// lock).  Therefore, we remove this assert.
	//
	// See the retry sequence below where we grab a write lock and
	// try again.
	//

	if (uiop->uio_resid == 0)
		return (0);

	if (uiop->uio_loffset < (offset_t)0) {
		return (EINVAL);
	}

	int bytes_to_write = (int)uiop->uio_resid;

	//
	// Ask for bandwidth to do this write. If pxreg's cache has enough
	// bandwidth cached to satisfy the current request, allocate from
	// there. Otherwise request from the global pool. If bandwidth is
	// not available the call will block until bandwidth is available.
	//
	// cached_bandwidth needn't be locked as only one writer is
	// allowed on a file and only writers allocate bandwidth.
	//
	if (cached_bandwidth >= bytes_to_write) {
		cached_bandwidth -= bytes_to_write;
	} else {
		error = pxvfs::wait_for_bandwidth(bytes_to_write,
		    cached_bandwidth);
		if (error) {
			return (error);
		}

		// Update pxreg's bandwidth cache
		cached_bandwidth -= bytes_to_write;
	}

retry:
	//
	// If data caching is disabled, go direct.
	//
	if (!is_cached()) {
		//
		// Caching is off. There should not be any cached
		// pages on the client at this point.
		//
		ASSERT(!vn_has_cached_data(get_vp()));
		error = write_uncached(uiop,
		    (curthread->t_flag & T_DONTPEND) ? ioflag | FSYNC : ioflag,
		    credp);

		//
		// EAGAIN is almost certainly from our directio transition
		// barrier change for 4404484 -- we make sure we have a
		// write lock and try again
		//
		if (EAGAIN == error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_RED,
			    ("write(%p) directio OFF transition trying again\n",
			    this));
			delay((clock_t)5);
			rwunlock(0);
			rwlock(1);
			goto retry;
		} else {
			return (error);
		}
	}

	//
	// If we are doing fastwrite, pre-allocating blocks can avoid
	// cache_lock contention while doing the page population loop
	// inside write_cache(). Call get_block_reservation() with nobmap
	// set to true as we don't want to wait for a REDZONE transition.
	// And since we don't wait for a REDZONE transition, we don't need
	// to increment threads_in_reserve_blocks before calling
	// get_block_reservation().
	//
	size_t reserved_bytes = 0;
	PXFS_VER::blkcnt_t reserve_blocks_cnt;
	pxvfs *pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);

	if (pxvfsp->fastwrite_enabled() && uiop->uio_resid > PAGESIZE) {
		reserve_blocks_cnt = get_block_reservation(bytes_to_write,
		    true, error);
		if (error != 0) {
			return (error);
		}
		if (reserve_blocks_cnt != 0) {
			reserved_bytes = bytes_to_write;
		}
	}

	//
	// It is possible for write_cache() to go the the uncached path if
	// get_write_token() returns TOKEN_UNCACHED -- i.e. its view of the
	// cachedata_flag has changed. In that case, we try again.
	//
	error = write_cache(uiop, ioflag, credp, reserved_bytes);

	if (EAGAIN == error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("write(%p) write_cache EAGAIN\n", this));
		delay((clock_t)5);
		goto retry;
	} else {
		return (error);
	}
}

//
// fsync out data if we ever did a remote allocating write.
//
int
pxreg::fsync_on_close(int syncflag, struct cred *credp)
{
	if (sync_on_close) {
		return (fsync(syncflag, credp));
	}

	return (0);
}

int
pxreg::fsync(int syncflag, struct cred *credp)
{
	int error = 0;

	check_wait_lock_cache();
	data_token_lock.rdlock();

	//
	// On close(), if there are uncommitted pages that couldn't be
	// flushed out due to lack of disk space, we destroy them here.
	// 'syncflag' will be set to PXFS_DESTROY_PAGES in that case. We
	// are treating a flag as an int for this particular case.
	//
	int flag = (syncflag == PXFS_DESTROY_PAGES) ? B_INVAL | B_FORCE : 0;

	// Write out all dirty pages.
	if (vn_has_cached_data(get_vp())) {
		error = pvn_vplist_dirty(get_vp(), (offset_t)0,
		    putapage_remote, flag | B_ASYNC, credp);
	}

	data_token_lock.unlock();

	//
	// The putapage above may have queued async requests, wait for
	// them to drain. If we are destroying pages wait for aios.
	//
	drain_asyncs((syncflag == PXFS_DESTROY_PAGES) ? true : false);
	cache_lock.unlock();

	// If the putapage_remote failed, don't go to the server.
	if (error) {
		return (error);
	}

	solobj::cred_var	cred_obj = solobj_impl::conv(credp);
	Environment		e;

	getfile()->cascaded_fsync(syncflag, cred_obj, e);
	error = pxfslib::get_err(e);
	e.clear();

	return (error);
}

int
pxreg::map(offset_t off, struct as *asp, caddr_t *addrp, size_t len,
	uchar_t prot, uchar_t maxprot, uint_t flags, struct cred *credp)
{
	/*
	 * Solaris 9 update 2 changed the size of struct segvn_crargs
	 * by adding a field to the end.  That field was:
	 *	uint_t grp_mem_policy_flags
	 *
	 * This broke binary compatability for all file system modules.
	 * So that we (Sun Cluster) with PXFS won't have to have two
	 * versions, one to support Solaris 9 FCS and update 1 and another
	 * version to support Solaris 9 update 2 and later, we have chosen
	 * to pad by a single uint_t the segvn_crargs structure passed
	 * to as_map() and indirectly to segvn_create().
	 */
#if defined(_BUG_4924098)
	struct padded_segvn_crargs {
		struct segvn_crargs vn_a_sans_pad;
		uint_t lgrp_mem_policy_flags_pad;
	} padded_vn_a = {{0}, 0};
#define	vn_a padded_vn_a.vn_a_sans_pad
#else	/* ! defined(_BUG_4924098) */
	struct segvn_crargs vn_a;
#endif	/* ! defined(_BUG_4924098) */

	int		error;
	vnode_t		*vnodep = get_vp();

	//
	// mmap is not allowed if caching is not supported.
	//
	if ((vnodep->v_flag & VNOMAP) ||
	    ((pxflags & (PX_CACHE_ATTRIBUTES | PX_CACHE_DATA)) !=
		(PX_CACHE_ATTRIBUTES | PX_CACHE_DATA))) {
		return (ENOSYS);
	}

	if (pxvfs::VFSTOPXFS(vnodep->v_vfsp)->is_forcedirectio_on()) {
		return (ENOTSUP);
	}

	if (off < 0 || off + (offset_t)len < 0) {
		return (EINVAL);
	}

	as_rangelock(asp);
	if ((flags & MAP_FIXED) == 0) {
		map_addr(addrp, len, off, 1, flags);
		if (*addrp == NULL) {
			as_rangeunlock(asp);
			return (ENOMEM);
		}
	} else {
		// User specified address - blow away any previous mappings.
		(void) as_unmap(asp, *addrp, len);
	}

	vn_a.vp = vnodep;
	vn_a.offset = (u_offset_t)off;
	vn_a.type = flags & MAP_TYPE;
	vn_a.prot = prot;
	vn_a.maxprot = maxprot;
	vn_a.cred = credp;
	vn_a.amp = NULL;
	vn_a.flags = flags & ~MAP_TYPE;

	error = as_map(asp, *addrp, len, (int (*)(void))segvn_create,
	    (caddr_t)&vn_a);
	as_rangeunlock(asp);
	return (error);
}

//
// addmap - pxfs only uses this information to detect a page map
// operation when directio is currently on. We turn off directio
// when addmap occurs.
//
int
pxreg::addmap(offset_t, as *, caddr_t, size_t len, uchar_t,
    uchar_t, uint_t, cred *)
{
	//
	// Keep track of number pages we have mapped, increment the
	// mmap_pages counter by the number of pages a 'len' byte
	// mapping can span.
	//
	mmap_pages_lock.wrlock();
	mmap_pages += btopr(len);
	mmap_pages_lock.unlock();

	if (cachedata_flag) {
		//
		// Caching is still enabled
		//
		return (0);
	}
	int		error;
	Environment	env;
	getfile()->enable_cachedata(env);

	if (env.exception()) {
		error = EIO;
	} else {
		error = 0;
	}
	return (error);
}

int
pxreg::delmap(offset_t, as *, caddr_t, size_t len, uint_t,
    uint_t, uint_t, cred *)
{
	//
	// Decrement mmap_pages counter by the number of pages this
	// mapping spans.
	//
	mmap_pages_lock.wrlock();
	mmap_pages -= btopr(len);
	ASSERT(mmap_pages >= 0);
	mmap_pages_lock.unlock();

	// Nothing else to do
	return (0);
}

//
// setattr
// It is possible to change the file size and other attributes
// in one setattr operation. At least NFS does this,
// and POSIX does not forbid it.
//
// Divert the truncate cases. Note that
// open with O_TRUNC flag, ends up in this routine. See vn_open().
//
int
pxreg::setattr(vattr *vap, int flags, cred *credp)
{
	if (vap->va_mask & AT_SIZE) {
		int	error;
		//
		// Follow the read and write protocol by  acquiring
		// the write lock.
		//
		rwlock(1);
		error = truncate(vap->va_size, credp);
		rwunlock(1);
		if (error) {
			return (error);
		} else {
			//
			// Clear the size change request.
			//
			vap->va_mask &= ~AT_SIZE;
			if (vap->va_mask == 0) {
				return (0);
			}
			//
			// There are other attribute changes.
			//
		}
	}

	//
	// If this is a request to change the mtime and if fastwrite is
	// enabled, we record that into allocate_timestamp. This will later
	// be applied to the file after the pages are flushed to disk
	// through page_out()/async_page_out(). Because, with fastwrites
	// enabled the disk-block allocation happens during [async_]page_out()
	// and that modifies the underlying inode and hence MTIME is updated.
	// This wipes out any MTIME modifications that were done after the
	// write(2) but, before the pages were flushed.
	//
	bool    fastwrite_flag =
	pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->fastwrite_enabled();

	if ((vap->va_mask & AT_MTIME) && (fastwrite_flag)) {
		allocate_timestamp = vap->va_mtime.tv_sec * NANOSEC
		    + vap->va_mtime.tv_nsec;
	}

	return (pxfobjplus::setattr(vap, flags, credp));
}

//
// space - support the truncate case.
//
int
pxreg::space(int cmd, struct flock64 *bfp, int, offset_t offset, cred *credp)
{
	int error;

	if (cmd != F_FREESP) {
		return (EINVAL);
	}

	error = convoff(pxnode::PXTOV(this), bfp, 0 /* SEEK_SET */, offset);
	if (error) {
		return (error);
	}
	if (bfp->l_len != 0) {
		return (EINVAL);
	}

	rwlock(1);
	error = truncate((u_offset_t)bfp->l_start, credp);
	rwunlock(1);
	return (error);
}

void
pxreg::rwlock(int write_lock)
{
	if (write_lock) {
		//
		// if directio and T_DONTPEND is not set we only grab a read
		// lock to allow concurrent writes
		//
		if (!is_cached() && 0 == (curthread->t_flag & T_DONTPEND)) {
			reg_rwlock.rdlock();
		} else {
			reg_rwlock.wrlock();
		}
	} else {
		reg_rwlock.rdlock();
	}
	incr_ref();
}

void
pxreg::rwunlock(int write_lock)
{
	decr_ref();

	if (write_lock) {
		//
		// We used to assert here if the "write" lock was not
		// granted under a concurrent directio writes condition:
		//
		// if (pxfobj::is_cached() || (curthread->t_flag & T_DONTPEND))
		// 	ASSERT(reg_rwlock.write_held());
		//
		// Of course, we would trip this assert if a directio went
		// from on to off right after we finished our write and
		// before we rwunlocked.
		//
		// We've now implemented a barrier to prevent any directio OFF
		// ioctls from comming in until all directio writes have
		// completed.  This will protect against corruption where
		// the cache is loaded before the directio write finishes.
		//
		// Unfortunately, since the barrier only protects the actual
		// write sequence (VOP_RWLOCK(), VOP_WRITE(), VOP_RWUNLOCK())
		// down to the UFS filesystem, we still can transition
		// *after* this sequence has finished (and we're still
		// holding the read lock).
		//
		// Therefore, we remove this assert.
		//
		reg_rwlock.unlock();
	} else {
		reg_rwlock.unlock();
	}
}

//
// This routine does the same function as rwunlock() except
// that it calls decr_ref_lockfree() instead of decr_ref()
//
void
pxreg::rwunlock_lockfree(int write_lock)
{
	decr_ref_lockfree();

	if (write_lock) {
		reg_rwlock.unlock();
	} else {
		reg_rwlock.unlock();
	}
}

//
// getpage - was cloned from pvn_getpages. We cannot use
// pvn_getpages because we want to pass this object
// to the getapage function.
//
// XXX Now that memcache is not a separate vnode any more this
// should be eliminated and should use pvn_get_pages!
//
int
pxreg::getpage(offset_t off, size_t len, uint_t *protp,
    page_t *plarr[], size_t plsz, seg *segp, caddr_t addr,
    seg_rw rw, cred *credp)
{
	vnode		*vnodep = pxnode::PXTOV(this);
	page_t		**ppp;
	size_t		sz;
	uint_t		xlen;			// Number of bytes left to read
	caddr_t		a;
	int		error = 0;
	u_offset_t	uoff;
	u_offset_t	eoff;			// Offset at end of this read
	uint_t		dummy_prot;
	token_status_t	token_status;

	//
	// True means this read sequentially follows the latest read
	//
	bool		seqmode;

	size_t		opt_size = 0;
	int		dolock = 0;

	//
	// In cases where the VM page size is smaller than the filesystem block
	// size (for example, X64 systems with a 4K page size), multiple pages
	// are needed to hold a single block of data. Of the pages representing
	// the block, if not all pages are faulted in, the filesystem block
	// could end up with uninitialized data. To prevent this, the starting
	// offset is changed to start from the block boundary.
	//
	if ((VBSIZE(vnodep) > PAGESIZE) && (segp != segkmap)) {
		offset_t	new_off;
		size_t		new_len;

		//
		// Round down "off" to the beginning of the file system block
		// and adjust len to keep the end point the same as it was.
		//
		new_off = off & ~(VBSIZE(vnodep) - 1LL);
		new_len = len + (size_t)(off - new_off);
		//
		// Make sure that there is enough room for paging in
		// additional VM page(s).  plsz is the size of the plarr
		// in bytes.  plarr is the array of VM pages to be
		// paged in during this getpage operation.
		//
		if (plsz >= new_len) {
			off = new_off;
			len = new_len;
		}
	}

	uoff = (u_offset_t)off;
	eoff = uoff + len;

	if (plarr == NULL) {
		// we don't do this yet
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("getpage(%p) async-readahead uoff %llx len %lx\n",
		    vnodep, uoff, len));
		return (0);
	} else {
		ASSERT(plsz >= len);	// insure that we have enough space
	}

	if (protp == NULL) {
		protp = &dummy_prot;
	}

	ppp = plarr;
	sz = (size_t)PAGESIZE;
	xlen = (uint_t)len;
	a = addr;

	*protp = PROT_ALL;

	//
	// XXX - simple policy for the time being. If the page is faulted
	//	on S_READ and then immediately S_WRITE, we will take two
	//	page faults. This should be optimized for files opened
	//	read-write.
	//
	if (rw != S_WRITE) {
		*protp &= ~PROT_WRITE;
	}

	PXFS_VER::acc_rights	req_acc = prot2acc(*protp);

	//
	// If mmap is not allowed for this vnode, we can't do getpage
	//
	if (vnodep->v_flag & VNOMAP) {
		return (ENOSYS);
	}

	//
	// The mmap case is identified by the
	// segment not being segkmap.
	//
	if (segp != segkmap) {
		//
		// Extending writes are a multi-step process, where we
		// update the file length, allocate disk blocks, create
		// new pages, and fill the new pages. To avoid deadlocks,
		// locks are dropped during parts of the write, but to
		// to avoid a reader seeing an inconsistent view, we must
		// block here until outstanding writes from this client
		// complete.  The writer lock is obtained during
		// VOP_WRLOCK().
		//
		// check to ensure against a recursive lock, for example
		// if we are faulting during a uiomove() that is part of
		// a write operation.
		//
		dolock = (reg_rwlock.rw_owner() != os::threadid());
		if (dolock) {
			rwlock(0);		// READ lock
		} else {
			incr_ref();
		}
	}

	if (req_acc == PXFS_VER::acc_rw) {
		token_status = get_write_token(error);
	} else {
		token_status = get_read_token(error);
	}
	if (token_status == TOKEN_EXCEPTION) {
		if (segp != segkmap) {
			if (dolock) {
				rwunlock(0);
			} else {
				decr_ref();
			}
		}
		return (error);
	}
	if ((rw != S_CREATE) && (uoff == current_off) &&
	    (!page_exists(vnodep, uoff))) {
		seqmode = true;
	} else {
		seqmode = false;
	}

	opt_size = pxfs_clustsz_nvalue;

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("getpage(%p) seqmode %d uoff %llx curr_off %llx"
	    " len %lx opt_size %lx rw %s\n",
	    this, seqmode, uoff, current_off, len, opt_size,
	    rw == S_WRITE ? "WRITE" : rw == S_READ ? "READ" : "OTHER"));

	//
	// Loop one page at a time and let the getapage function fill
	// in the next page in array.  We only allow one page to be
	// returned at a time (except for the last page) so that we
	// don't have any problems with duplicates and other such
	// painful problems.  This is a very simple minded algorithm,
	// but it does the job correctly.  Hopefully, we don't lose much
	// performance using the simple minded algorithm.
	//
	for (u_offset_t page_off = uoff;
	    xlen > 0 && page_off < eoff;
	    page_off += PAGESIZE, a += PAGESIZE, xlen -= (uint_t)PAGESIZE) {

		if (page_off + PAGESIZE > eoff) {
			//
			// Last time through - allow all of
			// what's left of the plarr[] array to be used.
			//
			sz = plsz - (size_t)(page_off - uoff);
		}
		//
		// Call getapage. If we are doing page population for a write,
		// loop till success or till we get a non-retriable error.
		//
		do {
			//
			// Increment 'threads_in_reserve_blocks' to prevent
			// cache_lock from being acquired by another thread.
			// We do this always even though getapage() will call
			// reserve_blocks() only when reading in a hole for
			// write. Threads needing cache_lock must wait for
			// threads_in_reserve_blocks to be 0 and will block
			// until this thread returns from getapage().
			//
			if (rw == S_WRITE) {
				incr_threads_in_reserve_blocks();
			}

			error = getapage(page_off, sz, protp, ppp, sz, segp, a,
			    rw, credp, seqmode, opt_size);

			if (rw == S_WRITE) {
				decr_threads_in_reserve_blocks();
			}

			if (error == 0) {
				break;
			}
			//
			// We incurred an error during the page_in, it may be
			// retriable.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_RED,
			    ("getpage(%p) error %d page %p\n",
			    this, error, ppp));

			if (rw == S_WRITE && plarr != NULL &&
			    error != EDEADLK && error != EAGAIN) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_REG,
				    PXFS_RED,
				    ("getpage(%p) read for write failed\n",
				    this, error, ppp));
			}

			//
			// Release page before retrying. We do not want to
			// unlock if the page is already unlocked in getapage().
			// If the page was unlocked in getapage *ppp will be
			// set to NULL. We also do not want to unlock the page
			// for the read case or for errors other than EDEADLK
			// and EAGAIN as the page will be unlocked later.
			//
			if (*ppp != NULL && rw == S_WRITE &&
			    (error == EDEADLK || error == EAGAIN)) {
				page_unlock(*ppp);
			}
		} while (rw == S_WRITE &&
		    (error == EDEADLK || error == EAGAIN));

		if (error != 0) {
			//
			// Release pages already got.
			//
			if (plarr != NULL) {
				for (ppp = plarr; *ppp != NULL; *ppp++ = NULL) {
					page_unlock(*ppp);
				}
				plarr[0] = NULL;
			}
			break;
		} else if (plarr != NULL) {
			ppp++;
		}
	}

	if (!error) {
		current_off = eoff;
		if (cur_pagedin_off < eoff) {
			cur_pagedin_off = eoff;
		}
	} else {
		if (segp != segkmap) {
			if (dolock) {
				rwunlock(0);
			} else {
				decr_ref();
			}
		}
		return (error);
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("getpage(%p) curr_off at end is %llx\n",
	    this, current_off));

	if (segp != segkmap) {
		if (rw == S_WRITE &&
		    (error = get_attr_write_token(credp)) == 0) {
			set_mtime();
			drop_attr_token();
		}
		//
		// Here we have to use the cache_lock free routines.
		// This is to avoid the lock ordering problems for
		// the page_lock and cache_lock. The correct lock
		// ordering is to grab the cache_lock and then to
		// grab the page_lock. But in this case, we have
		// already grabbed the page_lock as part of the
		// call to pvn_read_kluster() via getapage() call.
		// Hence we need to avoid taking cache_lock here.
		// Otherwise we will end up in a deadlock.
		//
		if (dolock) {
			rwunlock_lockfree(0);
		} else {
			decr_ref_lockfree();
		}
	}

	return (error);
}

//
// getapage - makes pages available.
// When the create option is specified, this method creates
// non-existent pages. Otherwise this method obtains the page
// information from the PXFS server.
//
int
pxreg::getapage(u_offset_t off, size_t len, uint_t *protp,
    page_t *plarr[], size_t plsz, seg *segp, caddr_t addr, seg_rw rw,
    cred_t *credp, bool seqmode, size_t opt_size)
{
	vnode		*vnodep = pxnode::PXTOV(this);
	page_t		*pp = NULL;
	u_offset_t	file_len;
	sol::error_t	err;
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	bool 	fastwrite_flag =
	    pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->fastwrite_enabled();

	ASSERT((off & PAGEOFFSET) == 0);
	ASSERT((len & PAGEOFFSET) == 0);

	// Initialize plarr[0] to NULL to handle error return correctly
	if (plarr != NULL) {
		plarr[0] = NULL;
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_AMBER,
		    ("getapage(%p) plarr is NULL\n", this));
		return (0);
	}

	if ((err = get_attr_read_token(credp)) != 0) {
		return (err);
	}
	file_len = get_length();
	drop_attr_token();

	//
	// It is OK to get a page past the end of the file if we are
	// creating a new page (e.g., we are being called from
	// segmap_pagecreate_mc()).
	//
	if (rw != S_CREATE && off >= file_len) {
		return (EFAULT);
	}

	PXFS_VER::acc_rights	req_acc = prot2acc(*protp);

	Environment	env;
again:
	if (!page_exists(vnodep, off)) {
		//
		// VM sub-system uses the S_CREATE flag to indicate
		// to the file system that a page for a particular offset
		// should be created if one does not already exist. This flag
		// is used to indicate access to the swap file. Swap is not
		// supported on PxFS and open() itself fails if the vnode type
		// is VISSWAP. The os::panic() below catches such a case.
		//
		if (rw == S_CREATE) {
			os::panic("Trying to create page for swap!\n");
		}

		u_offset_t	io_off;
		size_t		io_len;

		//
		// We acquired the read/write lock in getpage()
		//
		if (req_acc == PXFS_VER::acc_rw && !has_write_lock()) {
			os::panic("getapage: write token not held!\n");
		} else  if (!has_read_lock()) {
			os::panic("getapage: read token not held!\n");
		}

		u_offset_t	endoff = off + len;

		//
		// NOTE: vp_len should not be zero because, here,
		// we should have access for at least one page.
		//
		size_t		vp_len = len;

		if (endoff > file_len) {
			//
			// The length extends past the end-of-file.
			// Limit the read lenght for this page
			// to the number of bytes in this last page of the file.
			//
			vp_len = (size_t)(file_len & PAGEOFFSET);
		}

		pp = pvn_read_kluster(vnodep, off, segp, addr, &io_off, &io_len,
		    off, vp_len, 0);
		if (pp == NULL) {
			// Another thread got in and created the page.
			goto again;
		}
		//
		// We kick off our readahead before we page in to achieve a
		// greater degree of parallelism.
		//
		u_offset_t	next_off = off +
		    ((io_len + PAGESIZE - 1) & PAGEMASK);

		// Number of bytes to read ahead
		size_t		new_size = opt_size;

		if (pxfs_readahead &&
		    seqmode &&
		    (next_off < file_len) &&
		    (freemem >= lotsfree + pages_before_pager)) {

			u_offset_t temp = MIN(next_off + opt_size, file_len);

			if (new_size > (size_t)(temp - next_off)) {
				//
				// Must truncate the number of bytes to
				// read ahead in order to not read
				// past the end of the file.
				//
				new_size = (size_t)(temp - next_off);
				PXFS_DBPRINTF(
				    PXFS_TRACE_REG,
				    PXFS_GREEN,
				    ("getapage(%p) TRUNCATED to %lx\n",
				    this, new_size));
			}
			caddr_t		addr2 = addr + (next_off - off);
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("getapage(%p) readahead next %llx new_size %lx\n",
			    this, next_off, new_size));
			//
			// XXXX This is a gross hack, read-ahead in case of
			// a write fault requires rethinking the read-ahead
			// policy.
			//
			get_asyncpage(next_off, new_size, addr2, segp, credp);
		}

		bulkio::inout_pages_var	pglobj = bulkio_impl::conv_inout(
		    pp, io_off, (uint32_t)io_len, B_READ, *protp);

		//
		// Take a duplicate reference for the bulkio object, so that
		// we can do a release_pages even in case of an exception
		//
		bulkio::inout_pages_ptr		pglobj_dup =
		    bulkio::inout_pages::_duplicate(pglobj);

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("getapage(%p) pagein off %llx len %lx origlen %lx"
		    " pglobj %p\n",
		    this, io_off, io_len, len, (void *)pglobj));

		bulkio::file_hole_list_var	fdh;

		getfile()->page_in((sol::u_offset_t)io_off,
		    (sol::size_t)io_len, req_acc, pglobj.INOUT(),
		    fdh.out(), credobj, env);
		//
		// On return, we expect to free and/or unlock the pages.
		// We should tell the bulkio/transport
		// code that they should drop all references to these
		// pages before we free them.
		// Note that this is required in the error return case as the
		// transport may still be writing to these pages while the
		// error was propogating up.
		// In the case of non-error return this should not be needed,
		// but it seems safer to make sure the transport is not
		// updating pages which we are doing the zero/plist_init.
		//
		bulkio_impl::release_pages(pglobj_dup);
		CORBA::release(pglobj_dup);
		pglobj_dup = bulkio::inout_pages::_nil();

		err = pxfslib::get_err(env);

		if (err) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_RED,
			    ("getapage(%p) pagein err %d pp %p\n",
			    this, err, pp));

			pvn_read_done(pp, B_ERROR);
			return (err);
		}

		bulkio::file_hole	*fhl = fdh->buffer();
		uint_t			cnt = fdh->length();
		page_t			*fhp = pp;

		// Process one hole at a time
		for (uint_t i = 0; i < cnt; i++) {
			size_t		hole_len = fhl[i].len;
			u_offset_t	hole_off = fhl[i].off;

			ASSERT(fhp != NULL);

			// Convert this hole into zero filled pages
			do {
				ASSERT(hole_off >= fhp->p_offset);

				if (hole_off >= fhp->p_offset + PAGESIZE) {
					continue;
				}
				PXFS_DBPRINTF(
				    PXFS_TRACE_REG,
				    PXFS_GREEN,
				    ("getapage(%p) hole off %llx len %lx "
				    "mode %s\n",
				    this, hole_off, hole_len,
				    rw == S_WRITE ? "WRITE" : "OTHER"));

				//
				// When getting pages while holding write
				// permission, the server allocates backing
				// store.
				//
				if (rw != S_WRITE) {
					fhp->p_fsdata |= PXFS_HOLE;
				}

				//
				// If the file system block can accomodate more
				// than one page and we write only to the
				// higher address page[s] mapped to this block,
				// a flush will write out only the dirtied page.
				// This can result in the start of the block
				// having uninitialized data. To avoid that, we
				// dirty all pages mapped to the block to which
				// we are writing. The rest of the block
				// will be written from zero-filled pages.
				//
				if (fastwrite_flag &&
				    (VBSIZE(vnodep) > PAGESIZE) &&
				    (rw == S_WRITE)) {
					hat_setmod(fhp);
					PXFS_DBPRINTF(
					    PXFS_TRACE_REG,
					    PXFS_GREEN,
					    ("getapage(%p) mark this"
					    " page dirty; off 0x%llx\n",
					    this, fhp->p_offset));
				}
				size_t	zerolen = MIN(PAGESIZE, hole_len);

				ASSERT(zerolen != 0);
				ASSERT(zerolen <= PAGESIZE);

				pagezero(fhp, (uint_t)((unsigned long)hole_off
				    & PAGEOFFSET), (uint_t)zerolen);

				if ((rw == S_WRITE) &&
				    fastwrite_flag &&
				    ((err = alloc_blocks(fhp->p_offset,
				    hole_len, credp, false)) != 0)) {
					pvn_read_done(pp, B_ERROR);
					return (err);
				}

				hole_len -= zerolen;
				hole_off += zerolen;

			} while ((hole_len != 0) &&
			    ((fhp = page_list_next(fhp)) != NULL));
			if (fhp == NULL) {
				break;
			}
		}
		//
		// Zero the part of the last page beyond end of the read.
		//
		uint_t	xlen = (uint_t)(io_len & PAGEOFFSET);
		if (xlen != 0) {
			//
			// Assert that the previous page is the last page read
			// by this operation.
			//
			ASSERT(pp->p_prev->p_offset ==
				((io_off + io_len) & PAGEMASK));
			pagezero(pp->p_prev, xlen, (uint_t)PAGESIZE - xlen);
		}

		pvn_plist_init(pp, plarr, plsz, off, io_len, rw);
		return (0);
	}

	//
	// The requested page is available.
	//

	//
	// new_off is the offset that is pxfs_rdahead_lead bytes ahead
	// of current off and addr_new is the corresponding address.
	//
	u_offset_t	new_off = off + pxfs_rdahead_lead;
	caddr_t		addr_new = addr + pxfs_rdahead_lead;

	//
	// Check if we are less than pxfs_rdahead_lead bytes away from
	// the farthest offset paged-in (cur_pagedin_off). If this
	// is true then check whether that page is available. If not
	// then we kick-off the read-ahead. This effectively means
	// that a read-ahead is kicked-off when we still have
	// at least pxfs_rdahead_lead bytes of data available. Thus
	// by the time that data is consumed the next
	// pxfs_rdahead_size pages will be available thus avoiding
	// page_in() which is synchronous and hence expensive.
	//
	if (pxfs_readahead &&
	    (rw == S_READ) &&
	    ((new_off + (PAGESIZE * pxfs_rdahead_size)) < file_len) &&
	    ((cur_pagedin_off - new_off) < pxfs_rdahead_lead) &&
	    (freemem >= (lotsfree + pages_before_pager)) &&
	    (!page_exists(vnodep, new_off))) {

		get_asyncpage(new_off & PAGEMASK,
		    (PAGESIZE * pxfs_rdahead_size), addr_new, segp, credp);
	}

	//
	// The page exists in the cache, acquire the appropriate lock.
	// Holding the SE_EXCL lock in the S_WRITE case is necessary because
	// we need to serialize with other threads handling S_WRITE page
	// fault, or doing VOP_PUTPAGE(..., B_FREE), or processing the
	// write_back_and_downgrade_to_read_only call from the pager.
	//
	se_t	se = (req_acc == PXFS_VER::acc_rw ? SE_EXCL : SE_SHARED);

	if ((pp = page_lookup(vnodep, off, se)) == NULL) {
		//
		// This usually happens when page_exists() says the page
		// exists (which doesn't hold any locks), page_lookup()
		// waited to get the lock while another thread had the
		// page locked and was in the process of invalidating the
		// page, then page_lookup() proceeded and saw the page
		// no longer exists.
		//
		goto again;
	}

	//
	// Return other pages from cache up to plsz.
	//
	ASSERT(plarr != NULL);

	page_t	**pl = plarr;
	*pl++ = pp;
	size_t	alloc_len;

	if ((pp->p_fsdata & PXFS_HOLE) && rw == S_WRITE) {
		//
		// The page is a hole that does not have backing store.
		// Since we now allow write permission, we need
		// to allocate backing store.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("getapage(%p) bmap existing page off %llx\n",
		    this, pp->p_offset));

		if (pp->p_offset + PAGESIZE <= file_len) {
			alloc_len = (size_t)PAGESIZE;
		} else {
			//
			// Allocate space for the portion of the file
			// extending into this page. We don't want to
			// call alloc_blocks() with a allocation
			// request beyond EOF on this page, as this would
			// cause the filesize to be increased incorrectly.
			//
			alloc_len = (size_t)(file_len & PAGEOFFSET);
		}

		if ((err = alloc_blocks(pp->p_offset, alloc_len,
		    credp, false)) != 0) {
			*pl = NULL;
			return (err);
		}

		//
		// If the file system block can accomodate more than one page
		// and we write only to the higher address page[s] mapped to
		// this block, a flush will write out only the dirtied page.
		// This can result in the start of the block having
		// uninitialized data. To avoid that, we dirty all pages mapped
		// to the block to which we are writing. The rest of the block
		// will be written from zero-filled pages.
		//
		if (fastwrite_flag && (VBSIZE(vnodep) > PAGESIZE) &&
		    (pp->p_fsdata & PXFS_HOLE)) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("getapage(%p) mark this page dirty; off 0x%llx\n",
			    this, pp->p_offset));
			hat_setmod(pp);
		}

		pp->p_fsdata &= ~PXFS_HOLE;
	}

	u_offset_t	pgoff = off + PAGESIZE;
	u_offset_t	eoff = off + plsz;

	while (pgoff < eoff) {
		if ((pp = page_lookup_nowait(vnodep, pgoff, se)) == NULL) {
			break;
		}
		*pl++ = pp;
		if ((pp->p_fsdata & PXFS_HOLE) && rw == S_WRITE) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("getapage(%p) bmap existing page off %llx\n",
			    this, pp->p_offset));

			if (pp->p_offset + PAGESIZE <= file_len) {
				alloc_len = (size_t)PAGESIZE;
			} else {
				//
				// Allocate space for the portion of the file
				// extending into this page. We don't want to
				// call alloc_blocks() with a allocation
				// request beyond EOF on this page, as this
				// would cause the filesize to be increased
				// incorrectly.
				//
				alloc_len = (size_t)(file_len & PAGEOFFSET);
			}

			err = alloc_blocks(pp->p_offset, alloc_len, credp,
			    false);
			if (err != 0) {
				*pl = NULL;
				return (err);
			}

			//
			// If the file system block can accomodate more than
			// one page and we write only to the higher address
			// page[s] mapped to this block, a flush will write out
			// only the dirtied page. This can result in the start
			// of the block having uninitialized data. To avoid
			// that, we dirty all pages mapped to the block to
			// which we are writing. The rest of the block
			// will be written from zero-filled pages.
			//
			if (fastwrite_flag &&
			    (VBSIZE(vnodep) > PAGESIZE) &&
			    (pp->p_fsdata & PXFS_HOLE)) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_REG,
				    PXFS_GREEN,
				    ("getapage(%p) mark this page dirty; "
				    "off 0x%llx\n", this, pp->p_offset));
				hat_setmod(pp);
			}

			pp->p_fsdata &= ~PXFS_HOLE;
		}
		pgoff += PAGESIZE;
	}

	*pl = NULL;		// terminate the array of pointers

	return (0);
}

//
// get_asyncpage - queue one read ahead operation for asynchronous processing.
//
void
pxreg::get_asyncpage(u_offset_t off, size_t len, caddr_t addr,
    seg *segp, cred_t *credp)
{
	vnode		*vnodep = pxnode::PXTOV(this);
	uint_t		xlen;
	page_t		*pp;
	u_offset_t	io_off;
	size_t		io_len;

	ASSERT(cur_data_token & PXFS_READ_TOKEN);

	if ((pp = pvn_read_kluster(vnodep, off, segp, addr, &io_off, &io_len,
	    off, len, 1)) == NULL) {
		return;
	}

	//
	// Adjust the variable controlling read-ahead
	//
	current_off += io_len;
	if (cur_pagedin_off < (io_off + io_len)) {
		cur_pagedin_off = io_off + io_len;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("get_asyncpage(%p) curr_off %llx\n",
	    this, (u_offset_t)current_off));

	ASSERT(pp);

	//
	// Zero part of page which we are not going to read from disk.
	//
	if ((xlen = (uint_t)(io_len & PAGEOFFSET)) > 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("getasyncpage(%p) zeroing part of page %x\n",
		    this, xlen));
		pagezero(pp->p_prev, xlen, (uint_t)PAGESIZE - xlen);
	}

	//
	// This is the async case. Hand the request to mem_async layer.
	// Data token should not be downgraded till the async completes.
	// The lock will be released by the async thread after read
	// completion.
	//
	data_token_lock.rdlock();
	pxvfs *pxvfsp = pxvfs::VFSTOPXFS(vnodep->v_vfsp);
	pxvfsp->mem_async_threadpool->defer_processing(new mem_async_read(this,
	    pp, io_off, io_len, seqnum, credp));
	os::atomic_add_64(&(pxvfsp->async_task_count), 1);
}

//
// putpage - forces pages to storage.
//
// Called externally to PXFS
// only by NFS server during rfs_write operation to
// force data written by VOP_WRITE to the disk.
//
// Internally, PXFS uses this method to support fsync and clean up
// when a file becomes inactive.
//
// Ideally, pxreg would
// provide a new operation (or fsync would be extended) as a cleaner way
// of forcing file data to disk and we could eliminate putpage.
//
// The normal cases of putpage() are:
//	(1) len == 0 && off == 0 (all pages)
//	(2) len == MAXBSIZE (from segmap_release)
//	(3) len == PAGESIZE (from pageout)
//
int
pxreg::putpage(offset_t off, size_t len, int flags, cred *credp)
{
	//
	// Ignore putpage if page caching is disabled or a panic is in
	// progress.
	//
	// Solaris flushes all dirty pages on panic. For PxFS's dirty pages
	// pxreg's putpage method is called which waits for threads operating
	// on the file to drain. After panic there is no pre-emption. PxFS's
	// state will not change. Putpage will not return if another thread is
	// active on the file. Solaris has a bug which prevents deadman tick
	// from breaking a hanging sync-on-panic. As a workaround for this
	// solaris bug we ignore putpage calls if panic is in progress.
	//
	if (!is_cached() || panicstr) {
		return (0);
	}

	vnode		*vnodep = pxnode::PXTOV(this);
	int		error;

	//
	// If the filesystem has been unmounted already (eg. a forced
	// unmount) we use B_INVAL | B_FORCE to destroy the pages.
	//
	if ((pxvfs::VFSTOPXFS(get_vp()->v_vfsp))->is_unmounted()) {
		flags = flags | B_INVAL | B_FORCE;
	}

	//
	// If mmap is not allowed for this vnode, we can't do putpage
	//
	if (vnodep->v_flag & VNOMAP) {
		return (ENOSYS);
	}

	// Case when putpage applies to all pages of the vnode.
	if (len == 0) {
		//
		// hold data_token lock so that the token does not get changed.
		//
		check_wait_lock_cache();
		data_token_lock.rdlock();
		if (vn_has_cached_data(get_vp())) {
			error = pvn_vplist_dirty(vnodep, (u_offset_t)off,
			    putapage, flags, credp);
		}
		data_token_lock.unlock();
		cache_lock.unlock();
		return (error);
	}

	//
	// Loop over all offsets in the range [off...off + len]
	// looking for pages to deal with.  We set limits so
	// that we kluster to klustsize boundaries.
	//
	u_offset_t	uoff = (u_offset_t)off;
	u_offset_t	eoff = uoff + len;
	u_offset_t	io_off;
	size_t		io_len;
	page_t		*pp;

	//
	// Hold cache_lock to make sure page list is not changed.
	//
	check_wait_lock_cache();

	//
	// Try to cluster writes for pxfs_clustsz_nvalue bytes before
	// pushing it out, so that we can reduce the number of write
	// operations in the server.
	//
	if (flags & B_ASYNC) {
		if (delay_len == 0) {
			//
			// There is no pending write operation.
			// So make this the start of the pending write.
			//
			delay_off = uoff;
			delay_len = len;
			cache_lock.unlock();
			return (0);
		}

		if (delay_off + delay_len != uoff ||
		    delay_len >= pxfs_clustsz_nvalue) {
			//
			// The latest write operation cannot be combined
			// with the pending write operation.
			// So execute the pending write operation,
			// and make this latest write operation the
			// pending write operation.
			//
			u_offset_t	doff = delay_off;
			size_t		dlen = delay_len;
			delay_off = uoff;
			delay_len = len;
			eoff = doff + dlen;
			uoff = doff;
			len = dlen;
		} else {
			//
			// This write operation is contiguous with the
			// current pending write operation and within
			// the allowed number of bytes.
			// So combine these write operations.
			//
			delay_len += len;
			cache_lock.unlock();
			return (0);
		}
	}

	//
	// We don't want to give up our data privileges till this putpage
	// is done.
	//
	data_token_lock.rdlock();
	for (error = 0, io_off = uoff;
	    !error && io_off < eoff;
	    io_off += io_len) {
		if ((flags & B_INVAL) || ((flags & B_ASYNC) == 0)) {
			pp = page_lookup(vnodep, io_off,
				(flags & (B_INVAL | B_FREE)) ?
				SE_EXCL : SE_SHARED);
		} else {
			//
			// We are not invalidating, synchronously freeing or
			// writing pages. Use routine page_lookup_nowait() to
			// prevent reclaiming them from the free list.
			//
			pp = page_lookup_nowait(vnodep, io_off,
				(flags & B_FREE) ? SE_EXCL : SE_SHARED);
		}

		if (pp == NULL || pvn_getdirty(pp, flags) == 0) {
			//
			// No need to write this page back to the server.
			// Either the page:
			//	1) does not exist
			//	or
			//	2) no information needs to written to disk
			//
			io_len = PAGESIZE;
		} else {
			// pvn_getdirty says that the page is dirty,
			// and has marked the page as having an I/O in progress
			// (PAGE_IO_INUSE bit set in p_iolock).
			//
			// "io_off" and "io_len" are returned as the range of
			// pages we actually wrote. This allows us to skip
			// ahead more quickly since several pages may have
			// been dealt with by this iteration of the loop.
			//
			error = putapage(vnodep, pp, &io_off, &io_len, flags,
			    credp);
			if (error) {
				break;
			}
		}
	}
	data_token_lock.unlock();
	cache_lock.unlock();
	return (error);
}


//
// This is similar to putpage() above except that it is used to flush
// dirty pages when requested by the server object.
// The main difference is that server calls us with cr == kcred.
//
int
pxreg::putpage_remote(u_offset_t off, size_t len, int flags, cred *credp)
{
	vnode	*vnodep = pxnode::PXTOV(this);
	int	error = 0;
	bool	downgrade_readonly = false;

	ASSERT((off & PAGEOFFSET) == 0 || (flags & B_TRUNC) && len == 0);

	ASSERT(flags == (B_INVAL | B_FORCE) ||	// write_back_and_delete or
						// become_stale()
		flags == (B_INVAL | B_TRUNC) ||	// delete_range
		// write_back_and_downgrade_to_read_only with pages mmap'd
		flags == (B_FREE | B_FORCE) ||
		// write_back_and_downgrade_to_read_only no pages mmap'd
		flags == B_FREE ||
		flags == 0);			// sync

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("putpage_remote(%p) off %llx len %lx flags %s\n",
	    this, off, len,
	    flags == (B_INVAL | B_FORCE) ? "B_INVAL | B_FORCE" :
	    flags == (B_INVAL | B_TRUNC) ? "B_INVAL | B_TRUNC" :
	    flags == (B_FREE | B_FORCE) ? "B_FREE | B_FORCE" :
	    flags == (B_FREE) ? "B_FREE" : "0"));

	//
	// Check if we are here due to a call to downgrade read/write
	// permissions to read-only. For a token downgrade, if there
	// are no mmap'd pages on the vnode we pass B_FREE flag to
	// distinguish it from the sync() case. We clear the flag in
	// that case and set downgrade_readonly to true.
	//
	if (flags == B_FREE) {
		downgrade_readonly = true;
		flags = 0;
	}

	//
	// Apply putpage to all vnode pages. We ask pvn_vplist_dirty to do
	// the traversal in async mode. This is to avoid contention with
	// i/o locked pages held by async tasks. In putapage_remote() we
	// have to clear the async flag.
	//
	if (len == 0) {
		if (vn_has_cached_data(vnodep)) {
			error = pvn_vplist_dirty(vnodep, off, putapage_remote,
			    flags | B_ASYNC, credp);
		}

		//
		// Wait for all page IO to complete if we are not retaining
		// pages.
		//
		drain_asyncs((flags == 0) ? false : true);

		//
		// pvn_vplist_dirty() ignores IO locked and non-dirty pages
		// if B_ASYNC flag is passed. So we could still have pages
		// hanging off the vnode. Unless we were called to do a
		// sync(), we must free all pages for this vnode. To do
		// that we call pvn_vplist_dirty() with the B_FREE flag.
		//
		// If 'flags' is 0, we were called for doing a sync unless
		// we are downgrading a read-write token to read only.
		//
		if (!downgrade_readonly) {
			flags |= B_FREE;
		}

		if (downgrade_readonly ||
		    (flags != 0 && vn_has_cached_data(vnodep))) {
			error = pvn_vplist_dirty(vnodep, (u_offset_t)0,
			    putapage_remote, flags, credp);
		}

		return (error);
	}

	//
	// Loop over all offsets in the range [off...off + len]
	// looking for pages to deal with.
	//
	u_offset_t	eoff = off + len;
	u_offset_t	io_off;
	size_t		io_len;

	//
	// For invalidating or freeing a page, we require an exclusive lock
	// on the page.
	//
	se_t	se = (flags & (B_INVAL | B_FREE)) ? SE_EXCL : SE_SHARED;

	for (io_off = off; io_off < eoff; io_off += io_len) {
		page_t *pp = page_lookup(vnodep, io_off, se);

		if (pp == NULL || pvn_getdirty(pp, flags) == 0) {
			io_len = PAGESIZE;
		} else {
			// pvn_getdirty says that the page is dirty,
			// and has marked the page as having an I/O in progress
			// (PAGE_IO_INUSE bit set in p_iolock).
			//
			// limit kluster size to io_len
			io_len = (size_t)(eoff - io_off);
			error = putapage_remote(vnodep, pp, &io_off, &io_len,
			    flags, credp);
			if (error) {
				break;
			}
		}
	}

	// Wait for all page IO to complete.
	drain_asyncs(true);

	// Free all pages in the vnode.
	if (vn_has_cached_data(vnodep)) {
		error = pvn_vplist_dirty(vnodep, (u_offset_t)0,
		    putapage_remote, flags | B_FREE, credp);
	}

	// Wake up threads for waiting to retry page_in or page_upgrade.
	return (error);
}

//
// For the current state define by memory, async tasks and caller, decide
// what to do with a page that is dirty. The possible return values are:
//
// QUEUE => Create new mem_async_write task and queue it.
//
// BOUNCE => Don't do anything on the page, leave it dirty.
//
// SYNC => Do a synchronous page_out for the page.
//
inline pxreg::page_action_t
pxreg::get_page_disposition(bool is_write_thread, pxvfs *pxvfsp)
{
	uint64_t async_queue_length;

	async_queue_length = pxvfsp->async_task_count;

	//
	// This switch determines how to handle a putpage request based on
	// who the caller is, what the current memory conditions are and
	// what the current length of the async task queue is.
	//
	switch (monitor::the().get_current_state()) {
	case monitor::MEMORY_PLENTY:
		if (async_queue_length <= PLENTY_LOW && bounced) {
			bounced = false;
		}

		if (async_queue_length >= PLENTY_HIGH && !bounced) {
			bounced = true;
		}

		//
		// fsflush will never bounce, it will either queue it or wait
		//
		if (bounced) {
			return (is_write_thread ? BOUNCE : SYNC);
		}

		return (QUEUE);

	// At purge or below, we shorten the async queue length
	case monitor::MEMORY_PURGE:
	case monitor::MEMORY_LOW:
	case monitor::MEMORY_LOWER:
		if (is_write_thread) {
			//
			// Memory is critically low, but if we issue sync
			// pageouts for every page in an atomic write, it will
			// cripple the node. So we leave the dirty page,
			// write_cache will do a synchronous putpage after
			// queuing enough pages.
			//
			return (BOUNCE);
		}

		//
		// Since we are trying to free objects now, it does not make
		// sense to queue up more mem_async_request objects than can
		// be serviced.
		//
		if (async_queue_length <
		    pxvfsp->mem_async_threadpool->total_number_threads() * 2) {
			return (QUEUE);
		}

		//
		// caller is "fsflush" and queued_async > total async threads,
		// we can afford the hit on fsflush/pageout sleeping.
		//
		return (SYNC);

	// At memory starved we are desperate.
	case monitor::MEMORY_STARVED:
		//
		// Since we are trying to free objects now, it does
		// not make sense to queue up more async tasks than
		// there are async threads.
		//
		if (async_queue_length <
		    pxvfsp->mem_async_threadpool->total_number_threads()) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("page disp(%p) starved, queuing.\n",
			    this))
		    return (QUEUE);
		} else {
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("page disp(%p) starved, sync pageout.\n",
			    this))
			return (SYNC);
		}
	default:
		// This should never happen.
		ASSERT(false);
		return (SYNC);
	}
}

//
// Write a page to the server and the disk.
//
// static
int
pxreg::putapage(vnode *vnodep, page_t *pp, u_offset_t *offp, size_t *lenp,
    int flags, cred *credp)
{
	pxreg		*pxregp = (pxreg *)VTOPX(vnodep);
	pxvfs		*pxvfsp = pxvfs::VFSTOPXFS(pxregp->get_vp()->v_vfsp);
	kstat_t		*stats = pxvfsp->stats;
	u_offset_t	io_off;
	size_t		io_len;
	Environment	e;
	u_offset_t	vpoff;
	size_t		vplen;
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	bool		write_thread = false;

	vpoff = (pp->p_offset / pxfs_clustsz_nvalue) * pxfs_clustsz_nvalue;
	vplen = pxfs_clustsz_nvalue;

	ASSERT((pxregp->cur_data_token &
	    (PXFS_READ_TOKEN | PXFS_WRITE_TOKEN)) ==
	    (PXFS_READ_TOKEN | PXFS_WRITE_TOKEN));

	//
	// If a synchronous write is in progress and we are a system
	// thread (fsflush/pageout), ignore this putapage request.
	//
	if (IS_SYS_THREAD() && pxregp->sync_write_in_progress) {
		//
		// Tell VM to unlock the pages but to treat the pages
		// as still dirty. If we pass B_ERROR without B_INVAL
		// or B_FORCE to pvn_write_done(), VM considers the
		// error as a transient one and re-marks the pages as
		// modified.
		//
		ASSERT(pp->p_iolock_state & PAGE_IO_INUSE);
		pvn_write_done(pp, ((flags | B_ERROR) & ~(B_INVAL | B_FORCE)));

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("putapage(%p) skipping page %p\n",
		    pxregp, pp));

		return (0);
	}

	//
	// Check if the putapage() was due to a write cache() doing
	// segmap_release. If 'lenp' is NULL we were called to commit all
	// pages in the file, ie. due to a sync(). This call is from a
	// system process if 'curproc' is fsflush or pageout daemon.
	// 'segmap_release_offset' member of pxreg is set only by
	// write_cache. Thus is lenp is not null, curproc is not a system
	// thread and if segmap_release_offset falls within current
	// putpage's commit range, the caller is write_cache()/write().
	//
	u_offset_t seg_offset = pxregp->segmap_release_offset;

	if (lenp != NULL && !IS_SYS_THREAD() &&
	    seg_offset >= pp->p_offset && seg_offset < (pp->p_offset + *lenp)) {
		write_thread = true;
	}

	//
	// Kluster the pages before we check whether this is synchronous
	// or asynchronous request.
	// We should kluster the pages before we schedule the mem_async_write
	// request so that the klustered pages are locked. Otherwise, before
	// the mem_async_write task is picked up for execution another
	// putapage() can come in (via. fsflush)  and pick up one of the
	// pages that would have ideally been klustered by the already
	// scheduled mem_async_write task. This would break the
	// klustering and hence cause performance degradation.
	//
	pp = pvn_write_kluster(vnodep, pp, &io_off, &io_len, vpoff, vplen,
	    flags);

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("putapage(%p) len after kluster = %lx\n",
	    pxregp, io_len));

	if ((flags & B_ASYNC) != 0) {
		//
		// If there is any kind of delay in writing the IO out to the
		// physical device, then the number of mem_async objects being
		// put on the mem_async_threadpool can be greater than the
		// amount being taken off and serviced. This leads to many
		// thousands of mem_async objects waiting on the threadpool,
		// with their associated pages being locked down in memory.
		// This gets to a point where there is no freemem left and the
		// machine grinds to a halt.
		//
		// To avoid memory starvation on the node due to async pool
		// size and greedy writers we do throttling of writes and
		// async task queuing here. Depending on async task queue
		// length, available free pages and the originator of
		// putpage() request, we either create a new async task,
		// release the page back into the dirty pool or convert the
		// request into a synchronous one.
		//
		page_action_t action =
		    pxregp->get_page_disposition(write_thread, pxvfsp);

		switch (action) {
		case SYNC:
			//
			// We have to do a synchronous page out. Clear the
			// B_ASYNC flag.
			//
			flags &= ~B_ASYNC;

			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("putapage(%p): page 0x%p off 0x%llx "
			    "Throttling occurred\n", pxregp, pp, offp));
			PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
			    [PXVFS_STATS_THROTTLING_HITS].value.ui32++));

			break;

		case BOUNCE:
			if (flags & (B_INVAL | B_FORCE)) {
				//
				// These pages are being disposed off. It
				// doesn't make sense to have them around
				// for another try.
				//
				break;
			} else {
				//
				// Tell VM to unlock the pages but to treat
				// the pages as still dirty. If we pass
				// B_ERROR without B_INVAL or B_FORCE to
				// pvn_write_done(), VM considers the error
				// as a transient one and re-marks the
				// pages as modified.
				//
				pvn_write_done(pp, flags | B_ERROR);
			}
			return (0);
		case QUEUE:
			//
			// Data token should not be downgraded till the async.
			// completes. The lock will be released by the async
			// thread after write completion.
			//
			pxregp->data_token_lock.rdlock();
			pxvfsp->mem_async_threadpool->defer_processing(new
			    mem_async_write(pxregp, pp, io_off, io_len, flags,
			    pxregp->seqnum, credp, GET_MOD_TIME(pxregp)));
			os::atomic_add_64(&(pxvfsp->async_task_count), 1);
			return (0);
		}
	}

	//
	// This is the synchronous case
	//

	// Only used for a PxFS filesystem mounted without syncdir.
	sol::size_t	client_file_size =
	    (sol::size_t)pxregp->get_file_length();

	//
	// pvn_write_done() will be called directly here
	// instead of in the bulkio layer to free the pages.
	//
	bulkio::in_pages_var	pglobj = bulkio_impl::conv_in(
	    pp, io_off, (uint_t)io_len, flags | B_WRITE);

	//
	// We increment i/o count without waiting since this is a
	// synchronous request.
	//
	pxvfsp->increment_io_pending(false);

	pxregp->getfile()->page_out((sol::u_offset_t)io_off,
	    (sol::size_t)io_len, (sol::size_t) client_file_size,
	    flags | B_WRITE, pglobj, credobj, GET_MOD_TIME(pxregp),
	    pxregp->size_in_sync, e);

	// Decrement the pending i/o count.
	pxvfsp->decrement_io_pending();

	//
	// Wait for the PpBuf to get destructed
	// before doing a pvn_write_done(). This
	// has to be called before doing a pvn_write_done()
	// everywhere.
	//
	bulkio_impl::wait_on_sema(pglobj);

	// Make throttling values up-to-date
	pxvfsp->update_throughput((int)io_len);

	if (e.exception() == NULL) {
		// Good, no errors.
		pvn_write_done(pp, flags | B_WRITE);
	} else {
		int error = pxfslib::get_err(e);
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("putapage(%p) page_out error %d\n",
		    pxregp, error));
		//
		// Synchronous pageout with errors.
		// Bug here. We should destroy these pages if
		// the exception was a COMM_FAILURE.
		//
		pvn_write_done(pp, flags | B_WRITE | B_ERROR);
		return (error);
	}
	if (offp != NULL) {
		*offp = io_off;
	}
	if (lenp != NULL) {
		*lenp = io_len;
	}
	return (0);
}

//
// putapage_remote is called from pvn_vplist_dirty when all pages
// of the cache are to be processed. Otherwise, we are called
// from putpage_remote() to handle specific page ranges.
//
// static
int
pxreg::putapage_remote(vnode *vnodep, page_t *pp, u_offset_t *offp,
    size_t *lenp, int flags, cred *credp)
{
	pxreg			*pxregp = (pxreg *)VTOPX(vnodep);
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	page_t			*dirty_list = pp;
	u_offset_t		io_off;
	size_t			io_len;
	se_t			se;
	Environment		e;
	sol::size_t		client_file_size = 0;

	u_offset_t	vpoff;
	size_t		vplen;

	//
	// If this call was made by pvn_vplist_dirty() B_ASYNC would be
	// set. Must clear that before proceeding.
	//
	flags &= ~B_ASYNC;

	// Set page kluster start offset to kluster size boundary.
	vpoff = (pp->p_offset / pxfs_clustsz_nvalue) * pxfs_clustsz_nvalue;

	//
	// Limit vplen to pxfs_clustsz_nvalue. pvn_vplist_dirty() calls us
	// with lenp == NULL.
	//
	if (lenp == NULL) {
		vplen = pxfs_clustsz_nvalue;
	} else if ((vplen = *lenp) > pxfs_clustsz_nvalue) {
		vplen = pxfs_clustsz_nvalue;
	}

	//
	// write_back() and write_back_and_downgrade_to_read_only()
	// requires an exclusive lock.
	//
	se = (flags & (B_INVAL | B_FREE)) ? SE_EXCL : SE_SHARED;
	flags |= B_WRITE;

	//
	// Kluster the pages before scheduling the sync or async page_out.
	//
	pp = pvn_write_kluster(vnodep, pp, &io_off, &io_len,
				vpoff, vplen, flags);

	bulkio::in_pages_var pglobj = bulkio_impl::conv_in(
	    pp, io_off, (uint_t)io_len, flags);

	// Only used for a PxFS filesystem mounted without syncdir.
	client_file_size = (sol::size_t)pxregp->get_file_length();

	//
	// putapage_remote() is called only when there is a token downgrade
	// or the pages are being destroyed or when the file is being
	// sync'd. The resulting page_out requests have to be synchronous
	// since the caller would not have passed in the B_ASYNC flag.
	// However, doing serialized synchronous page_out's is bad for
	// througput as we won't utilize available bandwidth fully. To
	// solve this issue we have to be able to safely say all pages in
	// the given range have been committed to disk or destroyed.
	//
	// We queue up async requests and add code after every putpage()
	// call or every putpage_remote() call via pvn_vplist_dirty() to
	// wait for all queued async requests to drain and all io_async
	// objects created on the server to be destroyed. Waiting for async
	// requests makes sure that there are no more requests pending in
	// the async defer task list. Pages will be destroyed when the
	// io_async objects release their reference on the aio object.
	// Waiting for io_async signalling makes sure that there are no
	// pages left on the client after page_out.
	//

	//
	// If there are threads free to process an async request we queue
	// up a new request, otherwise we do a synchronous request. If
	// memory is really low do the queuing very strictly.
	//
	pxvfs *pxvfsp = pxvfs::VFSTOPXFS(vnodep->v_vfsp);
	int allowed_threads =
	    pxvfsp->mem_async_threadpool->total_number_threads();

	// Allow twice as many threads if we have enough memory.
	if (monitor::the().get_current_state() != monitor::MEMORY_STARVED) {
		allowed_threads *= 2;
	}

	if (pxvfsp->mem_async_threadpool->task_count() < allowed_threads) {
		//
		// Data token should not be downgraded till the async.
		// completes. The lock will be released by the async
		// thread after write completion.
		//
		pxregp->data_token_lock.rdlock();

		pxvfsp->mem_async_threadpool->defer_processing(new
		    mem_async_write(pxregp, pp, io_off, io_len, flags | B_ASYNC,
		    pxregp->seqnum, credp, GET_MOD_TIME(pxregp)));
		os::atomic_add_64(&(pxvfsp->async_task_count), 1);
		return (0);
	}

	// Increment io count without waiting.
	pxvfsp->increment_io_pending(false);

	//
	// Cache will discard pages after write if there is memory
	// pressure.
	//
	pxregp->getfile()->page_out((sol::u_offset_t)io_off,
	    (sol::size_t)io_len, client_file_size,
	    flags | B_WRITE, pglobj, credobj, GET_MOD_TIME(pxregp),
	    pxregp->size_in_sync, e);

	// Decrement the pending i/o count.
	pxvfsp->decrement_io_pending();

	//
	// Wait for the PpBuf to get destroyed before doing
	// a pvn_write_done().
	//
	bulkio_impl::wait_on_sema(pglobj);
	if (e.exception() == NULL) {
		// Good, no errors.
		pvn_write_done(dirty_list, flags | B_WRITE);
	} else {
		// Pageout completed with errors.
		pvn_write_done(dirty_list, flags | B_WRITE | B_ERROR);
		return (pxfslib::get_err(e));
	}

	if (offp != NULL) {
		*offp = io_off;
	}
	if (lenp != NULL) {
		*lenp = io_len;
	}
	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("putapage_remote(%p) page %p off %llx len %lx flags %x\n",
	    pxregp, pp, io_off, io_len, flags));

	return (0);
}

void
pxreg::dispose(page_t *pp, int flags, int dontneed, cred *)
{
	ASSERT(pxnode::PXTOV(this) == pp->p_vnode);
	ASSERT(flags == B_FREE || flags == B_INVAL);

	//
	// Handle the case when VN_DISPOSE is called as a result of
	// write_back_and_delete or write_back_and_downgrade_to_read_only
	// on clean writable pages that are locked in memory
	// (see pvn_getdirty).
	//
	// XXX - this code wouldn't be here if putpage_remote used a
	// customized version of the pvn_getdirty function.
	//
	if (flags == B_FREE && (pp->p_lckcnt != 0 || pp->p_cowcnt != 0)) {
		page_unlock(pp);
		return;
	}

	if (pp->p_fsdata) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("dispose(%p) freeing hole off %llx %s\n",
		    this, pp->p_offset,
		    flags == B_FREE ? "FREE" : "DESTROY"));
	}
	if (flags == B_FREE) {
		page_free(pp, dontneed);
	} else {
		pp->p_fsdata = 0;
		page_destroy(pp, dontneed);
	}
}

//
// Enable or disable data page caching.
//
void
pxreg::change_cachedata_flag(bool onoff)
{
	//
	// make sure no one is accessing the file while the
	// the caching is being changed.
	//
	check_wait_lock_cache();
	cachedata_flag = onoff;

	//
	// we don't support mmap with caching. Hence,
	// unset/set the vnode VNOMAP flag depending on whether
	// cached/uncached
	//
	if (cachedata_flag) {
		get_vp()->v_flag &= ~VNOMAP;
	} else {
		get_vp()->v_flag |= VNOMAP;
	}
	cache_lock.unlock();
}

int
pxreg::close(int flag, int cnt, offset_t offset, cred *credp)
{
	int error;

	// Handle file and share lock cleanup.
	error = pxfobj::close(flag, cnt, offset, credp);
	if (error) {
		return (error);
	}

	//
	// If syncdir mount option is not specified, we push pages to the server
	// on close. The reason we do this is to return ENOSPC to the caller of
	// close, in case there is no space on disk.
	//
	if (!pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->is_syncdir_on()) {
		if (!(pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->is_nocto_on())) {
			error = fsync_on_close(FSYNC, credp);
			if (error == ENOSPC) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_REG,
				    PXFS_RED,
				    ("close(%p) ENOSPC\n",
				    this));
				(void) fsync(PXFS_DESTROY_PAGES, credp);
			}
		}
	}
	return (error);
}

//
// check_wait_lock_cache
// if no threads are active, acquire the cache lock in order to have
// exclusive access to the cache. If there are any threads active let
// them finish. Except if a token request is being made to the server
// in that case proceed without waiting.
//
// If fastwrites is enabled, write_cache() and getpage() during modification
// of the pages need to call alloc_blocks() and could get stuck in
// reserve_blocks(). During this they hold the page lock but, don't hold
// the cache_lock. If a putpage() comes in at the sametime it would take
// the cache_lock and then block waiting for the page lock. This results
// in a deadlock.
// Hence, if fastwrites is enabled we also wait for threads_in_reserve_blocks
// to drop to 0 before taking the cache_lock.
//
// NOTE : threads_in_reserve_blocks indicates the potential number of threads
// that could have been blocked in reserve_blocks(). This counter is incremented
// before we call into alloc_blocks() and decremented after we return from
// alloc_blocks(). This is in anticipation that alloc_blocks() would call
// reserve_blocks().
//
void
pxreg::check_wait_lock_cache()
{
	bool    fastwrite_enabled = pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->
	    fastwrite_enabled();

	//
	// check_wait_lock_cache can be called when the calling thread holds
	// pxreg::rwlock as reader or writer. When rwlock is held as a writer
	// we can identify the case directly from the lock. However if the
	// lock is held as reader we use the sync_read_tsd key to identify
	// rwlock read ownership.
	//
	bool rwlock_owner = (sync_read_tsd.get_tsd() != NULL ||
	    reg_rwlock.rw_owner() == os::threadid());

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("check_wait_l_c(%p) rwlock_owner=%s tsd=%llx\n",
	    this, rwlock_owner ? "1" : "0", sync_read_tsd.get_tsd()));

	cache_lock.lock();
	//
	// We need to apply this condition only if the file is cached.
	// if the file is not cached than we have no way of knowing if
	// a call is being made to the server, In that situation the only
	// function calling here would be change_cachedata_flag() and
	// it should be allowed to change the flag. So if the file is
	// uncached do not worry about any active readers or writers.
	//
	// If the current thread owns the rwlock the cache lock is grabbed
	// unless there are no threads blocked in reserve_blocks(). And if
	// the current thread doesn't own the rwlock we grab the cache lock
	// when there are no threads modifying the file and no threads blocks
	// in reserve blocks().
	//
	while (is_cached() &&
	    ((fastwrite_enabled && (threads_in_reserve_blocks != 0)) ||
	    (!rwlock_owner &&
	    (rwlocks_granted != threads_waiting_for_token)))) {
		cache_lock.unlock();
		delay(pxfs_inval_delay);
		cache_lock.lock();
	}
}

//
// check_wait_lock_cache_revoke :
// cache_lock prevents modification to the page cache for the file.
// Threads allowed inside a code block protected by cache_lock and
// reg_rwlock are truncate thread[s] or thread[s] which called reserve_blocks()
// and is waiting for blocks to become available. Blocks may not become
// available until the server switches to redzone. The switch to redzone won't
// happen till sync_file_revoke() returns, that means we should not wait for
// threads in reserve_blocks() to finish. So we wait until all threads other
// than truncate or revoke have finished their work.
//
void
pxreg::check_wait_lock_cache_revoke()
{
	cache_lock.lock();
	//
	// We need to apply this condition only if the file is cached.
	//
	while (is_cached() && (rwlocks_granted !=
	    (threads_waiting_for_token + threads_in_reserve_blocks))) {
		cache_lock.unlock();
		delay(pxfs_inval_delay);
		cache_lock.lock();
	}
}



//
// Perform VOP_READ using the memory cache.
// This is modelled after common/fs/ufs/ufs_vnops.c:rdip().
//
int
pxreg::read_cache(uio *uiop, int ioflag, cred *credp)
{
	int		error = 0;
	uint_t		flags;
	ssize_t		oresid = uiop->uio_resid;

	uint_t		on;			// Number of bytes from
						// MAXBSIZE aligned offset
						// to actual start of read

	uint_t		nbytes;			// Number of bytes to read

	u_offset_t	file_size;

	u_offset_t	off;			// MAXBSIZE aligned offset
						// for start of read

	u_offset_t	uoffset;		// Offset for start of read

	u_offset_t	diff;			// Number bytes in file past
						// past the start of read
	caddr_t		base;


	token_status_t	status = get_read_token(error);

	if (status == TOKEN_EXCEPTION) {
		return (error);
	}
	if (status == TOKEN_UNCACHED) {
		// Caching status changed while not holding locks.
		ASSERT(!vn_has_cached_data(get_vp()));
		//
		// Don't append FSYNC to ioflag as it degrades performance.
		//
		error = read_uncached(uiop, ioflag, credp);
		return (error);
	}

	if ((error = get_attr_read_token(credp)) != 0) {
		return (error);
	}

	file_size = get_length();
	drop_attr_token();

	//
	// Copy data from the cache into the buffer supplied in uiop.
	// The loop iterates MAXBSIZE at a time (or less if the start or
	// end of the transfer is not MAXBSIZE aligned). If the file
	// pages are not mapped in segmap, uiomove generates a page
	// fault that ends up in getpage.
	//
	do {
		uoffset = (u_offset_t)uiop->uio_loffset;
		if (uoffset >= file_size) {
			break;
		}
		off = uoffset & (u_offset_t)MAXBMASK;
		on = (uint_t)(uoffset & (u_offset_t)MAXBOFFSET);
		nbytes = MIN((uint_t)MAXBSIZE - on, (uint_t)uiop->uio_resid);
		diff = file_size - uoffset;
		if (diff < (u_offset_t)nbytes) {
			nbytes = (uint_t)diff;
		}

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("read_cache(%p) off=%llx on=%lx n=%lx file_size=%llx\n",
		    this, off, (uint_t)on, nbytes, file_size));

		base = segmap_getmapflt(segkmap, pxnode::PXTOV(this), off + on,
		    (size_t)nbytes, 1, S_READ);

		error = uiomove(base + on, (size_t)nbytes, UIO_READ, uiop);
		if (error == 0) {
			flags = 0;

			//
			// If we read a whole block, we won't need this
			// buffer again soon. Do free behind only
			// if the system is low on free memory.
			//
			if (nbytes + on == MAXBSIZE && free_behind_read()) {
				flags = SM_DONTNEED | SM_FREE; // XXX | SM_ASYNC
			}
			//
			// In POSIX SYNC (FSYNC and FDSYNC) read mode,
			// we want to make sure that the page which has
			// been read, is written to the server if it is dirty.
			// The server should make sure that metadata is
			// flushed out as well.
			//
			if ((ioflag & FRSYNC) && (ioflag & (FSYNC | FDSYNC))) {
				flags &= ~SM_ASYNC;
				flags |= SM_WRITE;
			}

			//
			// putpage will be called for synchronous reads via
			// segmap_release. We mark threads doing synchronous
			// reads in order to notify the pxfs read/write
			// synchronization object, which then enables
			// multiple synchronous reads concurrently.
			//
			// To avoid false negatives for zero offset we set the
			// highest bit and "or" it with the read offset.
			//
			sync_read_tsd.set_tsd(
			    (uintptr_t)(uoffset | ~MAXOFFSET_T));
			PXFS_DBPRINTF(
			    PXFS_TRACE_REG,
			    PXFS_GREEN,
			    ("read_cache(%p) off=%llx set_tsd=%llx\n",
			    this, off, (uintptr_t)(uoffset | ~MAXOFFSET_T)));
			error = segmap_release(segkmap, base, flags);

			// Done with putpage, clear the sync read "mark".
			sync_read_tsd.set_tsd((uintptr_t)NULL);
		} else {
			(void) segmap_release(segkmap, base, 0);
		}
	} while (error == 0 && uiop->uio_resid > 0 && nbytes != 0);

	// XXX what about updating the last access time attribute?

	//
	// If we've already done a partial read, terminate
	// the read but return no error.
	//
	if (oresid != uiop->uio_resid) {
		error = 0;
	}

	return (error);
}

//
// required_blocks(size_t nbytes)
//
// The routine calculates the number of blocks needed for a given number
// of bytes.
//
PXFS_VER::blkcnt_t
pxreg::required_blocks(size_t nbytes)
{
	PXFS_VER::blkcnt_t blocks_required;

	//
	// Compute number of blocks required
	// We try to estimate as closely as possible the number of
	// blocks that the underlying filesystem
	// would use for this write request. It's ok to over-estimate
	// but never alright to under-estimate.
	//
	blocks_required = nbytes /
	    ((pxvfs::VFSTOPXFS(get_vp()->v_vfsp))->get_fs_bsize());

	blocks_required += blocks_required / space_fudge_factor;

	//
	// To handle those cases where number of bytes is
	// less than blocksize (which means it needs at least one block
	// for the data and the second one is the fudgefactor for
	// indirect blocks.
	//
	if (blocks_required < 2) {
		blocks_required = 2;
	}

	return (blocks_required);
}

//
// Reserve enough file system blocks to write 'numbytes' of data. If nobmap
// is true, this method will not wait for the server to switch to REDZONE.
// We return a failure in that case.
//
PXFS_VER::blkcnt_t
pxreg::get_block_reservation(size_t numbytes, bool nobmap, int &err)
{
	err = 0;

	//
	// Compute number of blocks required
	//
	PXFS_VER::blkcnt_t blocks_required = required_blocks(numbytes);
	pxvfs *pxvfsp = pxvfs::VFSTOPXFS((get_vp())->v_vfsp);

	PXFS_DBPRINTF(PXFS_TRACE_REG, PXFS_GREEN,
	    ("get_block_reservation(%p): blocks required=%lld\n",
	    this, blocks_required));

	PXFS_VER::blkcnt_t blocks_reserved =
	    pxvfsp->reserve_blocks(blocks_required, nobmap, err);
	if (err != 0) {
		return (0);
	}

	if (blocks_reserved > 0) {
		//
		// Required number of blocks are reserved.
		//
		PXFS_DBPRINTF(PXFS_TRACE_REG, PXFS_GREEN,
		    ("get_block_reservation(%p) succeeded for %lld bytes\n",
		    this, blocks_reserved));

		//
		// Setting sync_on_close to "true" irrespective of
		// whether the client is colocated with the primary or
		// not. As an optimization for UFS the sync_on_close
		// is not set to "true". We will override this when
		// we are in fastpath. Also, refer to the comments in
		// pxreg::close().
		//
		sync_on_close = true;
		return (blocks_reserved);
	}

	return (0);
}

//
// alloc_blocks(file offset, numbytes, cred, nodeadlock)
//
// numbytes is the number of bytes for which space must be reserved.
//
// There is a fast path and a slow path. The fast path checks if
// blocks are available in the client's cache. If yes we reserve
// the appropriate number of blocks and return.
//
// The slow path is used only when there are too few free blocks in
// the filesystem making fast path allocation risky. We may break POSIX
// by being too greedy and cause dirty pages not to have backing store.
// The server tells the clients when they should go through the slow path.
//
// If nodeadlock is true, it is ok to wait for a potential deadlock condition
// to be resolved on the server. See code below.
// nodeadlock is false when alloc_blocks() is called from getapage() because
// we don't want to wait for a long time if faulting in a page for a write.
//
// NOTE : The caller of alloc_blocks() should increment
// threads_in_reserve_blocks before calling it.
//
int
pxreg::alloc_blocks(u_offset_t uoff, size_t numbytes, struct cred *credp,
    bool nodeadlock)
{
	ASSERT(threads_in_reserve_blocks > 0);
	int	error = 0;
	bool	file_size_updated = false;

	//
	// First get this file's attribute write token to modify its
	// attributes.
	//
	if ((error = get_attr_write_token(credp)) != 0) {
		return (error);
	}

	// Store current file size.
	u_offset_t	file_size = get_length();

	//
	// Do not change the size of the file if it is being faulted
	// for write, only extend when called from write_cache
	//
	if (nodeadlock && (file_size < (uoff + numbytes))) {
		set_length(uoff + numbytes);
		file_size_updated = true;
	}

	// Check to see if the requested range is in the extended range.
	if (in_ext_range(uoff, numbytes)) {
		if (file_size_updated) {
			size_in_sync = false;
		}

		//
		// Writes falling into extended range will not have an
		// upto-date bmap timestamp from the server. We cannot use
		// the client time as the nodes will not be in sync at the
		// nanosecond level. As a workaround, we track the time of
		// write and add the difference between this tracked time
		// and time at pageout to the last known timestamp from the
		// server. There is a guarantee that the time thus derived
		// will not overshoot server's current time.
		//
		last_write_timestamp = os::gethrtime();

		drop_attr_token();
		return (0);
	} else {
		last_write_timestamp = 0L;
	}

	//
	// If we don't drop the attribute token here, we could deadlock
	// during a switchover since the bmap could block waiting for
	// unfreeze and the server could block waiting for the client
	// to unlock the attribute cache in order to downgrade it.
	//
	drop_attr_token();

reserve_blocks:
	//
	// Attempt to allocate blocks from free block pool for this node.
	//
	if (pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->fastwrite_enabled()) {
		//
		// If there were enough blocks available via reservation we
		// are done. Otherwise this call must wait for the server
		// to switch to REDZONE, hence second parameter is false.
		//
		PXFS_VER::blkcnt_t reserve_blocks_cnt =
		    get_block_reservation(numbytes, false, error);
		if (error != 0) {
			return (error);
		}
		if (reserve_blocks_cnt > 0) {
			return (0);
		}
	}

	//
	// Blocks are not available, that means the server is in REDZONE,
	// so blocks allocation will be allocated in slow path, ie. by bmap().
	//

	bulkio::inout_pages_ptr pglobj = NULL;
	solobj::cred_var credobj = solobj_impl::conv(credp);
	int err;
	sol::size_t len;
	Environment env;
	vattr new_attr;
	sol::nodeid_t server_node;
	int deadlock_count = 0;
	int64_t	 bmap_timestamp;
	PXFS_VER::server_status_t status = PXFS_VER::UNMOUNTING;

retry:
	len = numbytes;
	new_attr.va_mask = AT_ALL;

	kstat_t	 *stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;
	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_ALLOC].value.ui32++));

	getfile()->bmap((sol::u_offset_t)uoff, len, conv(new_attr),
	    server_node, bmap_timestamp, status, credobj, env);

	error = pxfslib::get_err(env);
	env.clear();

	//
	// If the server switched to GREENZONE we need to update the local
	// status and try the allocation through reserve_blocks().
	//
	if ((pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->fastwrite_enabled()) &&
	    (status == PXFS_VER::GREENZONE)) {
		pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->
		    set_server_status(status);
		goto reserve_blocks;
	}

	//
	// If UFS returns a false EDEADLK, wait for a bit and retry.  See
	// BugId 4307546 for details.  In principle, we could retry even if
	// 'nodeadlock' is false as long as the call is remote, but this event
	// should be rare enough that we needn't bother the client with the
	// location of the server.
	//
	// Actually, the EDEADLK has been seen to occur 3-4 times a second.
	//
	if ((nodeadlock) && (error == EDEADLK)) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_AMBER,
		    ("alloc_blocks: vp %p off 0x%llx len 0x%lx error %d"
		    "EDEADLK\n", get_vp(), uoff, numbytes, error));
		delay((clock_t)5);
		deadlock_count++;
		goto retry;
	}

	if (deadlock_count) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_AMBER,
		    ("alloc_blocks: vp %p off 0x%llx len 0x%lx "
		    "EDEADLK occurred (%d) time(s)\n",
		    get_vp(), uoff, numbytes, deadlock_count));
		deadlock_count = 0;
	}

	if ((err = get_attr_write_token(credp)) != 0) {
		return (err);
	}

	//
	// The 'bmap' IDL call may change some fields of the file attributes
	// (file length, number of blocks, mtime, and ctime).  We need to now
	// update these fields in the attribute cache.
	//
	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("alloc_blocks:vp %p off 0x%llx len 0x%lx error %d\n",
		    get_vp(), uoff, numbytes, error));
		//
		// If failed to allocate space, set the length field back
		// if it was changed before going remote.
		//
		if (file_size_updated) {
			set_length(file_size);
		}
	} else {
		//
		// No errors - update the attributes to reflect the most current
		// information.
		//
		update_length_fields(new_attr);

		//
		// Update the current extended file range only if file is
		// being extended.
		// Storing the extended file range during writes inside the
		// file is unnecessary because a complete block is allocated
		// during first write and the subsequent writes for that block
		// will be writes to existing page(s).
		//
		if (file_size_updated) {
			pxreg::set_ext_range(uoff, len);
			size_in_sync = true;
		}

		// Record the server time for the space allocation
		allocate_timestamp = (os::hrtime_t)bmap_timestamp;

		//
		// If the server node is remote and syncdir is not turned on,
		// then we need to sync out data pages on close to confirm
		// that allocations have made it to disk.  We maintain the
		// flag here that indicates whether any allocations were
		// remote.
		//
		if (server_node != orb_conf::node_number()) {
			sync_on_close = true;
		}
	}
	drop_attr_token();

	return (error);
}


//
// The chart below show the various scenarios that we have to
// worry about. The first column shows the relative page_size
// and maxbsize offsets. The 2nd column is the size of the file
// (note that it is on an offset below page_size. The 3rd column
// represents the write request size. The idea is to be
// able ascertain how to allocate blocks on disks and what
// the meaning of the various variables are.
//
//
//		File				Write
// ___________________________________________	off	___
// |		|||					|
// |		|||					| on
// |		|||			______ uoffset	|___
// |		|||			|||		|
// |		||| file_size		|||		|
// |					|||		|
// |					|||		| n
// |					|||		|
// |_PAGESIZE	___ rounded_file_size	|||		|
// |					|||		|
// |					|||		|
// |					|||___ end_off	|___
// |
// |
// |
// |
// |
// + MAXBSIZE
// |
// |

//
// write_cache - Perform VOP_WRITE when client side page caching is enabled.
// This is based common/fs/ufs/ufs_vnops.c:wrip() and differs in that we allow
// the file size to increase and have to deal with creating new pages
// differently.
//
int
pxreg::write_cache(uio *uiop, int ioflag, cred *credp, size_t pre_reserved)
{
	vnode		*vnodep = pxnode::PXTOV(this);
	u_offset_t	rounded_file_size;
	u_offset_t	file_size;
	u_offset_t	old_file_size;
	u_offset_t	off;
	int		error = 0;
	uint_t		numbytes;
	bool		pagecreate;
	caddr_t		base;
	rlim64_t	limit = (rlim64_t)uiop->uio_llimit;

	// Save the requested number of bytes to write
	ssize_t		start_resid = uiop->uio_resid;

	// resid before uiomove()
	ssize_t		premove_resid;

	int		newpage = 0;
	bool		size_changed = false;

	// Sanity Checks.

	if (limit == RLIM64_INFINITY || limit > (rlim64_t)MAXOFFSET_T) {
		limit = (rlim64_t)MAXOFFSET_T;
	}
	if ((rlim64_t)uiop->uio_loffset >= limit) {
		(void) proc_signal(ttoproc(curthread)->p_pgidp, SIGXFSZ);
		return (EFBIG);
	}

	token_status_t	status = get_write_token(error);

	if (status == TOKEN_EXCEPTION) {
		return (error);
	}
	if (status == TOKEN_UNCACHED) {
		// Caching status changed while not holding locks.
		ASSERT(!vn_has_cached_data(get_vp()));
		error = write_uncached(uiop, ioflag, credp);
		return (error);
	}

	//
	// We only need the read token here but we will need the write token
	// later. So might as well get the write token now.
	//
	if ((error = get_attr_write_token(credp)) != 0) {
		return (error);
	}
	old_file_size = file_size = get_length();
	drop_attr_token();

	if (ioflag & FAPPEND) {
		uiop->uio_loffset = (offset_t)file_size;
		//
		// The range lock is not incorrect for append case.
		//
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("write_cache(%p) off %llx len %lx fsize %llx\n",
	    this, uiop->uio_loffset, uiop->uio_resid, file_size));
	//
	// Copy data from the buffer supplied in uiop to the client page cache.
	// The loop iterates MAXBSIZE at a time (or less if the start or
	// end of the transfer is not MAXBSIZE aligned).
	//
	do {
		u_offset_t pg_create_start;
		u_offset_t pg_create_end;
		u_offset_t	uoff = (u_offset_t)uiop->uio_loffset;

		// Calculate offset at beginning of this page
		off = uoff & (u_offset_t)MAXBMASK;

		// Determine the offset within this page
		uint_t	mapon = (uint_t)(uoff & (u_offset_t)MAXBOFFSET);

		// Number of byte to write within this specific page
		numbytes = (uint_t)MIN(MAXBSIZE - mapon,
		    (uint_t)uiop->uio_resid);

		rounded_file_size = roundup(file_size, PAGESIZE);

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("write_cache(%p) uoff %llx off %llx mapon %x "
		    "n %x rounded_file_size %llx roundup %llx\n",
		    this, uoff, off, mapon, numbytes, rounded_file_size,
		    roundup(file_size, MAXBSIZE)));

		if (uoff >= limit) {
			error = EFBIG;
			break;
		}

		if (uoff + numbytes >= limit) {
			//
			// Since uoff + numbytes >= limit,
			// therefore numbytes >= limit - uoff,
			// and numbytes is an uint_t
			// so it is safe to cast it to an uint_t.
			//
			numbytes = (uint_t)(limit - (rlim64_t)uoff);
		}

		if (uoff + numbytes > file_size) {
			//
			// If the write starts at the begining of the
			// block, we can create the page. No need
			// to read data.
			//

			//
			// pg_create_start : page aligned offset from where
			//	we would need to create page(s).
			//
			// pg_create_end : page aligned offset till where we
			//	would need to create page(s).
			//
			// We would need to create page(s) only if
			// (pg_create_end > pg_create_start).
			//
			if (mapon == 0) {
				pg_create_start = off;
			} else {
				pg_create_start = MAX(off, rounded_file_size);
			}

			pg_create_end = roundup((uoff+numbytes), PAGESIZE);

			if (pg_create_end > pg_create_start) {
				pagecreate = 1;
			} else {
				pagecreate = 0;
			}

			old_file_size = file_size;
			file_size = uoff + numbytes;

			//
			// Show that we changed the file size.
			//
			size_changed = true;

			//
			// Since we are changing the file size, ensure that
			// blocks are allocated or reserved in the
			// filesystem before dirtying the page.
			//
			if (pre_reserved > numbytes) {
				//
				// pre-reservation can cover this page, we
				// only need to update attributes. First
				// get this file's attribute write token to
				// modify its attributes.
				//
				error = get_attr_write_token(credp);
				if (error) {
					return (error);
				}

				// Set current length.
				set_length(file_size);

				//
				// Check to see if the requested range is
				// in the extended range.
				//
				if (in_ext_range(uoff, numbytes)) {
					size_in_sync = false;
				}
				drop_attr_token();

				// Account for this page or part of page.
				pre_reserved -= numbytes;
			} else {
				incr_threads_in_reserve_blocks();
				error = alloc_blocks(uoff, (size_t)numbytes,
						credp, true);
				decr_threads_in_reserve_blocks();
			}
			if (error != 0) {
				break;
			}

			FAULTPT_PXFS(FAULTNUM_PXFS_BMAP_C_A,
			    FaultFunctions::generic);

		} else if (numbytes == MAXBSIZE) {
			//
			// When writing MAXBSIZE there is no need to
			// read any block off disk, but we must still
			// ensure that disk blocks are allocated.
			//
			pagecreate = 1;

			pg_create_start = off;
			pg_create_end = off + numbytes;

			//
			// We are changing the file size, we have to ensure that
			// blocks are allocated in the filesystem before hand.
			// If blocks were pre-reserved and that is enough to
			// satisfy the size to write, don't call alloc_blocks,
			// instead just set the file_size and decrement the
			// pre-reserved byte count.
			//
			if (pre_reserved > numbytes) {
				if ((error =
				    get_attr_write_token(credp)) != 0) {
					break;
				}
				if (get_length() < (uoff + numbytes)) {
					set_length(uoff + numbytes);
				}
				drop_attr_token();
				pre_reserved -= numbytes;
			} else {
				incr_threads_in_reserve_blocks();
				error = alloc_blocks(uoff, (size_t)numbytes,
				    credp, true);
				decr_threads_in_reserve_blocks();
				if (error != 0) {
					break;
				}
			}

			FAULTPT_PXFS(FAULTNUM_PXFS_BMAP_C_A,
			    FaultFunctions::generic);

		} else {
			//
			// Let the fault handler bring in the pages
			// ensuring that data pages are shuttled here
			// before we perform our partial writes.
			// Note that the fault code should know
			// that the pages are brought in for write
			// and as such backing store must be allocated.
			//
			pagecreate = 0;

			//
			// If file size has changed locally let the server know
			// before we incur a fault.
			//
			if ((pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->
			    fastwrite_enabled()) && has_size_changed()) {
				flush_attributes(false, false);
			}
		}

		ASSERT(numbytes != 0);

		//
		// Allocate kernel virtual address space.
		// Fault pages in that we are not creating from scratch.
		// "pagecreate == 0" specifies that the page will be faulted in
		// when we do the uiomove().
		// If the underlying filesystem's block size is larger
		// than our VM page size, we better try mapping and faulting in
		// the whole filesystem block so as to avoid garbage in
		// the remainder of the block.
		//
		// If the size has changed and pagecreate is not set
		// then we know we need to page_in() some data from the disk.
		// In such a case fault-in only that data which is available
		// on the disk. Note that page_in() now will call FS_RDWR_DATA()
		// when fastwrite is enabled and does not do any allocation
		// (ie. by calling FS_ALLOC_DATA).
		//
		if (VBSIZE(vnodep) > PAGESIZE) {
			u_offset_t bloff = uoff & ~(VBSIZE(vnodep) - 1LL);

			PXFS_DBPRINTF(PXFS_TRACE_REG, PXFS_GREEN,
			    ("write_cache(%p) segmap off 0x%llx len 0x%lx "
			    "create %d\n",
			    this, bloff, VBSIZE(vnodep), pagecreate));

			if ((size_changed) && (!pagecreate)) {
				base = segmap_getmapflt(segkmap, vnodep, bloff,
				    (size_t)mapon, !pagecreate, S_WRITE);
			} else {
				base = segmap_getmapflt(segkmap, vnodep,
				    bloff, (size_t)VBSIZE(vnodep),
				    !pagecreate, S_WRITE);
			}
		} else {
			PXFS_DBPRINTF(PXFS_TRACE_REG, PXFS_GREEN,
			    ("write_cache(%p) segmap off 0x%llx len 0x%lx "
			    "mapon %llx create %d\n",
			    this, off + mapon, numbytes, mapon, pagecreate));

			if ((size_changed) && (!pagecreate)) {
				base = segmap_getmapflt(segkmap, vnodep,
				    off, (size_t)mapon, !pagecreate, S_WRITE);
			} else {
				base = segmap_getmapflt(segkmap, vnodep,
				    off + mapon, (size_t)numbytes, !pagecreate,
				    S_WRITE);
			}
		}

		//
		// segmap_pagecreate() returns 1 if it calls
		// page_create_va() to allocate any pages.
		//
		newpage = 0;

		premove_resid = uiop->uio_resid;

		if (pagecreate) {
			uint_t start_mapon =
			    (uint_t)(pg_create_start & (u_offset_t)MAXBOFFSET);

			size_t pg_create_len =
			    (size_t)(pg_create_end - pg_create_start);

			newpage = segmap_pagecreate(segkmap,
			    base+start_mapon, pg_create_len, 0);
		}

		if (ioflag & (FSYNC | FDSYNC)) {
			// Dirty pages created due to synchronous write.
			sync_write_in_progress = true;
		}

		error = uiomove(base + mapon, (size_t)numbytes,
		    UIO_WRITE, uiop);

		if (newpage) {
			//
			// We created pages w/o initializing them completely,
			// thus we need to zero the part that wasn't set up.
			// This happens on most EOF write cases and if
			// we had some sort of error during the uiomove.
			//
			if (mapon != 0) {
				page_t *pp;
				uint_t page_start_to_zero;
				uint_t zerolen;

				if (rounded_file_size <= off) {
					page_start_to_zero = 0;
					zerolen = mapon;
				} else {
					page_start_to_zero = (uint_t)
					    (rounded_file_size - off);
					zerolen = mapon - page_start_to_zero;
				}
				for (; page_start_to_zero < mapon;
				    page_start_to_zero += (uint_t)PAGESIZE,
				    zerolen -= (uint_t)PAGESIZE) {

					pp = page_find(vnodep,
						off + page_start_to_zero);
					ASSERT(pp != NULL);
					PXFS_DBPRINTF(
					    PXFS_TRACE_REG,
					    PXFS_GREEN,
					    ("write_cache(%p)pagezero(%p,%d)\n",
					    this, pp, zerolen));

					pagezero(pp, 0,
						MIN((uint_t)PAGESIZE, zerolen));
				}
			}

			uint_t nmoved = (uint_t)((u_offset_t)uiop->uio_loffset -
			    (off + mapon));

			uint_t nzero = roundup(mapon + numbytes,
			    (uint_t)PAGESIZE) - (mapon + nmoved);

			if (nzero != 0) {
				(void) kzero(base + mapon + nmoved,
				    (size_t)nzero);
			}
			if (rounded_file_size <= off) {
				segmap_pageunlock(segkmap, base,
					(size_t)numbytes + mapon, S_WRITE);
			} else {
				segmap_pageunlock(segkmap, base + mapon,
					(size_t)numbytes, S_WRITE);
			}
		} else if (mapon > old_file_size) {
			//
			// We did not create a new page but we wrote beyond
			// the old end of file. Zero fill the page from old
			// end of file till start of write.
			//
			(void) kzero(base + (old_file_size - off),
			    (size_t)(mapon - (old_file_size - off)));
		}

		//
		// Update the file size based on the amount of data
		// actually written and before we release the pages
		// locked by segmap_getmapflt().
		// So that even if the updated pages were pushed out
		// we would have seen the new size on the server.
		//

		if (error == 0) {
			uint_t	flags = 0;

			if (ioflag & (FSYNC | FDSYNC)) {
				// force synchronous writes
				flags = SM_WRITE;
			} else if (numbytes + mapon == MAXBSIZE ||
			    IS_SWAPVP(vnodep)) {
				//
				// Have written a whole block. Start an
				// asynchronous write and mark the pages to
				// indicate that it won't be needed again
				// soon. The pages will be put on the
				// free list after they have been written
				// to the pager.
				//
				flags = SM_WRITE | SM_ASYNC | SM_DONTNEED;
			}

			//
			// Store the segment offset to mark the putpage()
			// due to this segmap_release() as a call initiated
			// from a write.
			//
			segmap_release_offset = off;
			error = segmap_release(segkmap, base, flags);
			segmap_release_offset = 0;

			//
			// If the operation failed and is synchronous,
			// then we need to unwind what uiomove() last
			// did so we can potentially return an error to
			// the caller.  If this write operation was
			// done in two pieces and the first succeeded,
			// then we won't return an error for the second
			// piece that failed.  However, we only want to
			// return a resid value that reflects what was
			// really done.
			//
			// Failures for non-synchronous operations can
			// be ignored since the page subsystem will
			// retry the operation until it succeeds or the
			// file system is unmounted.
			//
			if (error) {
				if (ioflag & (FSYNC | FDSYNC)) {
					uiop->uio_resid = premove_resid;
				} else {
					error = 0;
				}
			}
		} else {
			//
			// If we failed on a write, we may have already
			// allocated file blocks as well as pages.  It's
			// hard to undo the block allocation, but we must
			// be sure to invalidate any pages that may have
			// been allocated.
			//
			segmap_release_offset = off;
			(void) segmap_release(segkmap, base, SM_INVAL);
			segmap_release_offset = 0;
		}

		if (ioflag & (FSYNC | FDSYNC)) {
			// No dirty pages pending from synchronous write.
			sync_write_in_progress = false;
		}

		//
		// Check for an error on the uiomove() or synchronous
		// page push
		//
		if (error) {
			if (size_changed &&
			    get_attr_write_token(credp) == 0) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_REG,
				    PXFS_RED,
				    ("write_cache(%p) restore "
				    "old file size %llx error %d\n",
				    this, old_file_size, error));

				//
				// Restore file size back to what it was
				// before this write started.
				//
				set_length(old_file_size);
				drop_attr_token();
			}
		} else {
			//
			// Turn off the setuid bit if the write is not root
			// and the the file has setuid root and is executable.
			//
			error = clear_suid(credp);
		}

	} while (error == 0 && uiop->uio_resid > 0 && numbytes != 0);

	if (start_resid != uiop->uio_resid) {
		//
		// Some data was written.
		// Discard any error from the loop above.
		//
		error = 0;

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("write_cache(%p) resid %lx - %lx n %x\n",
		    this, start_resid, uiop->uio_resid, numbytes));

		if (get_attr_write_token(credp) == 0) {
			set_mtime();
			drop_attr_token();
		}
	}

	return (error);
}

//
// Set the file length (implements the space() system call).
//
int
pxreg::truncate(u_offset_t size, cred_t *credp)
{
	Environment		e;
	bool			fastwrites_enabled;
	int			err = 0;

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("truncate(%p) size %llx\n",
	    this, size));

	fastwrites_enabled = pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->
	    fastwrite_enabled();

	//
	// We could consume a block while truncating the file.
	// Hence, we should account for it.
	//
	if (fastwrites_enabled) {
		incr_threads_in_reserve_blocks();

		//
		// It's OK to ignore the return value of reserve_blocks()
		// because, it has either reserved the required number of
		// blocks or the server has switched to REDZONE. In either
		// case there is nothing additional that needs to be done.
		//
		(void) pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->
		    reserve_blocks(blocks_for_truncate, false, err);
		if (err != 0) {
			decr_threads_in_reserve_blocks();
			return (err);
		}
	}

	cache_lock.lock();

	if (fastwrites_enabled) {
		threads_in_reserve_blocks--;
	}

	//
	// We need to note that a call from the server could be
	// coming and hence we should not block.
	//
	threads_waiting_for_token++;

	//
	// set the truncate flag so that mmap threads block.
	//
	truncate_in_progress = true;

	cache_lock.unlock();

	//
	// make the call to the server.
	//
	getfile()->cascaded_truncate(size, e);

	int error = pxfslib::get_err(e);

	if (error == 0) {
		//
		// Obtain the attribute write token and set the file size now.
		// This is to avoid ovewriting the correct file size on
		// new primary by the incorrect file size send by this client
		// if recovery happens just after this truncate operation.
		//
		if ((error = get_attr_write_token(credp)) == 0) {
			set_length(size);
			drop_attr_token();
		}
	}
	cache_lock.lock();
	truncate_in_progress = false;
	threads_waiting_for_token--;
	cache_wait_cv.broadcast();
	cache_lock.unlock();

	return (error);
}

//
// get_write_token - obtain the write token for the entire file.
// Return false if caching was disabled while locks were not held.
//
//lint -e1038
pxreg::token_status_t
pxreg::get_write_token(int &err)
{
	Environment	e;
	uint_t		orig_seqnum;
	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;

	err = 0;

	cache_lock.lock();

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("get_write_token(%p) cur %x req %x seq %d\n",
	    this, cur_data_token, req_data_token, seqnum));

retry:
	//
	// Check for uncached write.
	//
	if (!is_cached()) {
		cache_lock.unlock();
		return (TOKEN_UNCACHED);
	}

	//
	// Truncate is calling the server, so our token can change.
	// Wait for truncate to finish.
	// no need to increase threads_waiting_for_token count
	//
	if (truncate_in_progress) {
		threads_waiting_for_token++;
		cache_wait_cv.wait(&cache_lock);
		threads_waiting_for_token--;
		goto retry;
	}

	if (cur_data_token & PXFS_WRITE_TOKEN) {
		cache_lock.unlock();
		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_DATA_TOKEN_HITS].value.ui32++));
		return (TOKEN_OK);
	}

	threads_waiting_for_token++;
	//
	// If some one is already in request with server, wait and retry.
	//
	if (req_data_token != 0) {
		cache_wait_cv.wait(&cache_lock);
		threads_waiting_for_token--;
		goto retry;
	}

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_TOKEN_MISSES].value.ui32++));

	//
	// Mark the request map to serialise requests to server
	// in this particular range.
	//
	req_data_token |= PXFS_WRITE_TOKEN;

	//
	// Note down the original seqnum to determine if an invalidation
	// arrives before the grant request returns.
	//
	orig_seqnum = seqnum;

	//
	// Drop all the locks before going to server to allow
	// efficient invalidations and avoid deadlocks.
	//
	cache_lock.unlock();

	//
	// Send remote invocation.
	//
	uint32_t	server_incarn;
	getfile()->cascaded_write_lock(server_incarn, e);

	//
	// Live lock avoidance we need a strategy to guarantee progress.
	// If the token was invalidated by the time we are here, then
	// let's back off by sleeping for a short time.
	//

	if (orig_seqnum != seqnum) {
		timespec_t	lhrestime;
		gethrestime(&lhrestime);
		delay(LIVELOCK_DELAYTIME(seqnum, lhrestime.tv_nsec));
	}

	cache_lock.lock();

	// Wake up other threads waiting for the token.
	req_data_token = 0;
	cache_wait_cv.broadcast();

	//
	// We can decrease the count since we are holding the cache lock.
	//
	threads_waiting_for_token--;

	if (e.exception()) {
		cache_lock.unlock();
		err = pxfslib::get_err(e);
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("get_write_token(%p) except cur %x req %x seq %d err %d\n",
		    this, cur_data_token, req_data_token, seqnum, err));

		// We don't expect errors other than ENOSPC and EIO.
		ASSERT(err == ENOSPC || err == EIO);
		e.clear();
		return (TOKEN_EXCEPTION);
	}

	uint32_t	server_incn =
	    pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn();
	if (server_incn != server_incarn) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_AMBER,
		    ("get_write_token(%p) retry server old %x cur %x\n",
		    this, server_incarn, server_incn));

		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_DATA_TOKEN_RETRIES].value.ui32++));

		goto retry;
	}

	//
	// Make sure we didn't receive any invalidation requests
	// before we received our grant access. This translates
	// to the time span between dropping the cache_lock and
	// getting the cache_lock back.
	//
	if (orig_seqnum != seqnum) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("get_write_token(%p) retry cur %x req %x seq %d\n",
		    this, cur_data_token, req_data_token, seqnum));

		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_DATA_TOKEN_RETRIES].value.ui32++));

		goto retry;
	}

	//
	// Since we dropped cache_lock, check again for uncached write.
	//
	if (!is_cached()) {
		cache_lock.unlock();
		return (TOKEN_UNCACHED);
	}

	//
	// We got the token for data access
	// Set appropriate access.
	//
	data_token_lock.wrlock();
	cur_data_token |= PXFS_WRITE_TOKEN | PXFS_READ_TOKEN;
	data_token_lock.unlock();

	cache_lock.unlock();
	return (TOKEN_OK);
}

//
// get_read_token - obtain the read token for the entire file.
//
// We currently have one token for the whole file. If we change
// this in the future, we should align the range to MAXBSIZE boundaries,
// because segmap always deals with MAXBSIZE chunks and we don't want
// to be in a situation where write_cache() calls getpage() and we come
// here without the necessary access.
//
pxreg::token_status_t
pxreg::get_read_token(int &err)
{
	//lint +e1038
	Environment	e;
	uint_t		orig_seqnum;
	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;

	err = 0;

	cache_lock.lock();

retry:
	//
	// Check for uncached read.
	//
	if (!is_cached()) {
		cache_lock.unlock();
		return (TOKEN_UNCACHED);
	}

	//
	// Truncate is calling the server, so our token can change.
	// Wait for truncate to finish.
	// no need to increase threads_waiting_for_token count.
	//
	if (truncate_in_progress) {
		threads_waiting_for_token++;
		cache_wait_cv.wait(&cache_lock);
		threads_waiting_for_token--;
		goto retry;
	}

	//
	// It is possible that we could have updated the size of the file
	// because, we have the WRITE token. Hence, we need to flush the
	// attributes (if the size has changed).
	//
	if (cur_data_token & PXFS_READ_TOKEN) {
		cache_lock.unlock();
		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_DATA_TOKEN_HITS].value.ui32++));
		if (pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->fastwrite_enabled() &&
		    has_size_changed()) {
			flush_attributes(false, false);
		}
		return (TOKEN_OK);
	}

	threads_waiting_for_token++;
	//
	// If some one is already in request with server, wait and retry.
	//
	if (req_data_token != 0) {
		cache_wait_cv.wait(&cache_lock);
		threads_waiting_for_token--;
		goto retry;
	}

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_TOKEN_MISSES].value.ui32++));

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("get_read_token(%p) cur %x req %x seq %d\n",
	    this, cur_data_token, req_data_token, seqnum));

	//
	// Mark the request map to serialise requests to server
	// in this particular range.
	//
	req_data_token |= PXFS_READ_TOKEN;

	//
	// Note down the original seqnum to determine if an invalidation
	// arrives before the grant request returns.
	//
	orig_seqnum = seqnum;

	//
	// Drop all the locks before going to server to allow
	// efficient invalidations and avoid deadlocks.
	//
	cache_lock.unlock();

	//
	// Send remote invocation.
	//
	uint32_t	server_incarn;
	getfile()->cascaded_read_lock(server_incarn, e);

	//
	// Live lock avoidance we need a strategy to guarantee progress.
	// If the token was invalidated by the time we are here, then
	// let's back off by sleeping for a short time.
	//

	if (orig_seqnum != seqnum) {
		timespec_t lhrestime;
		gethrestime(&lhrestime);
		delay(LIVELOCK_DELAYTIME(seqnum, lhrestime.tv_nsec));
	}

	cache_lock.lock();

	// Wake up other threads waiting for the token.
	req_data_token = 0;
	cache_wait_cv.broadcast();

	//
	// We can decrease the count since we are holding the cache lock.
	//
	threads_waiting_for_token--;

	//
	// Exception! not much can be done, the filesystem is dead!
	//
	if (e.exception()) {
		cache_lock.unlock();
		err = pxfslib::get_err(e);

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("get_read_token(%p) except cur %x req %x seq %d err %d\n",
		    this, cur_data_token, req_data_token, seqnum, err));

		// We don't expect errors other than ENOSPC and EIO.
		ASSERT(err == ENOSPC || err == EIO);
		err = EIO; // Can't return ENOSPC error for reads
		e.clear();

		return (TOKEN_EXCEPTION);
	}

	uint32_t	server_incn =
	    pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn();
	if (server_incn != server_incarn) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_AMBER,
		    ("get_read_token(%p) retry server old %x cur %x\n",
		    this, server_incarn, server_incn));

		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_DATA_TOKEN_RETRIES].value.ui32++));

		goto retry;
	}

	//
	// Make sure we didn't receive any invalidation requests
	// before we received our grant access. This translates
	// to the time span between dropping the cache_lock and
	// getting the cache_lock back.
	//
	if (orig_seqnum != seqnum) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("get_read_token(%p) retry cur %x req %x seq %d\n",
		    this, cur_data_token, req_data_token, seqnum));

		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_DATA_TOKEN_RETRIES].value.ui32++));

		goto retry;
	}

	//
	// Since we dropped lock_cache, check again for uncached read.
	//
	if (!is_cached()) {
		cache_lock.unlock();
		return (TOKEN_UNCACHED);
	}

	//
	// Set appropriate access.
	//
	data_token_lock.wrlock();
	cur_data_token |= PXFS_READ_TOKEN;
	data_token_lock.unlock();

	cache_lock.unlock();
	return (TOKEN_OK);
}

//
// data_write_back_and_delete - push all dirty pages to the server,
// remove all cached data pages,
// and drop privileges to cache pages.
//
void
pxreg::data_write_back_and_delete(uint32_t server_incarnation,
    Environment &_environment)
{
	int	error = 0;

	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;
	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_TOKEN_INVALS].value.ui32++));

	// livelock avoidance delay
	check_wait_lock_cache();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("write_back_delete(%p) orphan ndid %d\n",
		    this, _environment.get_src_node().ndid));
		pxfslib::throw_exception(_environment, EIO);
		cache_lock.unlock();
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("write_back_delete(%p) cur %x\n",
	    this, cur_data_token));

	FAULTPT_PXFS(FAULTNUM_PXFS_PUTPAGE_REMOTE_C_B, FaultFunctions::generic);

	//
	// This method will push out all dirty pages to the server,
	// including any pending write operation.
	// Show that there is no pending write operation.
	//
	delay_len = 0;
	delay_off = 0;

	//
	// This operation pushes all pages to the server and
	// destroys the page.
	//
	error = putpage_remote((u_offset_t)0, (size_t)0,
	    B_INVAL | B_FORCE, kcred);

	if (error == 0) {
		// putpage_remote succeeded, vnode must not have pages.
		CL_PANIC(!vn_has_cached_data(get_vp()));

		if (pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn() ==
		    server_incarnation) {
			//
			// Modify the data privileges only when the
			// operation was requested by the current primary.
			// The primary could have changed midway through
			// this function.
			//
			data_token_lock.wrlock();
			seqnum++;
			cur_data_token = 0;
			data_token_lock.unlock();
		}
	} else {
		_environment.exception(new sol::op_e(error));

	}

	cache_lock.unlock();
}

//
// data_write_back_downgrade_read_only -
// flush any modified pages to the server,
// and reduce page caching privileges to read only.
//
void
pxreg::data_write_back_downgrade_read_only(uint32_t server_incarnation,
    Environment &_environment)
{
	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;
	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_TOKEN_INVALS].value.ui32++));

	// livelock avoidance delay
	check_wait_lock_cache();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("downgrade(%p) orphan ndid %d\n",
		    this, _environment.get_src_node().ndid));
		pxfslib::throw_exception(_environment, EIO);
		cache_lock.unlock();
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("downgrade(%p) cur %x\n",
	    this, cur_data_token));

	FAULTPT_PXFS(FAULTNUM_PXFS_PUTPAGE_REMOTE_C_B, FaultFunctions::generic);

	//
	// This method will push out all dirty pages to the server,
	// including any pending write operation.
	// Show that there is no pending write operation.
	//
	delay_len = 0;
	delay_off = 0;

	//
	// There is no way for a new mapping to be created while we
	// are here as we hold the cache_lock. We do a read lock on
	// 'mmap_pages_lock' out of principle rather than necessity.
	//
	int putpage_flags;

	mmap_pages_lock.rdlock();
	putpage_flags = (mmap_pages > 0) ? B_FREE | B_FORCE : B_FREE;
	mmap_pages_lock.unlock();

	//
	// If there are mmaped pages force free all pages, see
	// the header file for an explanation.
	//
	int	error = putpage_remote((u_offset_t)0, (size_t)0,
	    putpage_flags, kcred);

	if (error) {
		_environment.exception(new sol::op_e(error));

	} else if (pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn() ==
	    server_incarnation) {
		//
		// Modify the data privileges only when the operation was
		// requested by the current primary.
		// The primary could have changed midway through this function.
		//
		data_token_lock.wrlock();
		seqnum++;
		cur_data_token = PXFS_READ_TOKEN;
		data_token_lock.unlock();
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("downgrade(%p) seq %d cur %x\n",
	    this, seqnum, cur_data_token));

	cache_lock.unlock();
}

//
// data_write_back - write all modified pages to the server.
// The cache retains the same read-write access rights to the pages.
//
void
pxreg::data_write_back(Environment &_environment)
{
	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;
	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_TOKEN_INVALS].value.ui32++));

	//
	// To maintain POSIX compliance, no partial writes can be allowed
	// following fsync(). Block here until pending I/O has settled.
	//
	check_wait_lock_cache();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("write_back(%p) orphan ndid %d\n",
		    this, _environment.get_src_node().ndid));
		pxfslib::throw_exception(_environment, EIO);
		cache_lock.unlock();
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("write_back(%p) cur %x\n",
	    this, cur_data_token));

	FAULTPT_PXFS(FAULTNUM_PXFS_PUTPAGE_REMOTE_C_B, FaultFunctions::generic);

	int	error = putpage_remote((u_offset_t)0, (size_t)0, 0, kcred);

	cache_lock.unlock();

	if (error) {
		_environment.exception(new sol::op_e(error));
	}
}

//
// data_delete_range - supports only truncate functionality.
//
// Remove pages in the specified range. Dirty pages are not sent to the server
// before they are discarded. After the operation completes,
// the client has no caching rights.
//
// Delete range works differently from other call backs from the
// server. The problem is that we should not delete the page
// that contains the "offset", if it is not aligned with page size.
// The data in the page is valid until "offset", and therefore
// we need to zero the rest of the page, to avoid insecure data
// being passed to user during the next read.
// If we have to give up the access rights, then we
// need to flush the page that contains offset back, and this will
// complicate the protocol. For now, the offset is simply rounded
// to the next page size, and zeroing the previous page to
// the right range.
//
void
pxreg::data_delete_range(sol::u_offset_t offset, uint32_t server_incarnation,
    Environment &_environment)
{
	int	error = 0;

	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;
	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
	    [PXVFS_STATS_DATA_TOKEN_INVALS].value.ui32++));

	// livelock avoidance delay
	check_wait_lock_cache();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("delete_range(%p) orphan ndid %d\n",
		    this, _environment.get_src_node().ndid));
		pxfslib::throw_exception(_environment, EIO);
		cache_lock.unlock();
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_REG,
	    PXFS_GREEN,
	    ("delete_range(%p) cur %x off %llx\n",
	    this, cur_data_token, offset));

	FAULTPT_PXFS(FAULTNUM_PXFS_PUTPAGE_REMOTE_C_B, FaultFunctions::generic);

	//
	// The offsets used to control read ahead should not be left pointing
	// to an offset past the end of the truncated file.
	// Reset the read ahead offsets to the beginning of the file
	//
	current_off = 0;
	cur_pagedin_off = 0;

	//
	// This method will destroy or push out all dirty pages to the server,
	// including any pending write operation.
	// Show that there is no pending write operation.
	//
	delay_len = 0;
	delay_off = 0;

	//
	// Zero the page if offset is not aligned with pagesize
	// properly. Also, we retain the access rights for this page.
	//
	if ((offset & PAGEOFFSET) != 0) {
		uint_t	xlen = (uint_t)(offset & PAGEOFFSET);

		page_t	*pp = page_lookup(pxnode::PXTOV(this),
		    (offset & PAGEMASK), SE_EXCL);

		if (pp) {
			//
			// pagezero will not change reference or modified bits
			// and therefore this page will not be made dirty.
			//
			pagezero(pp, xlen, (uint_t)PAGESIZE - xlen);
			page_unlock(pp);
		}
	}


	//
	// File truncation will result in a call to this function.
	// We are deleting the pages associated with the truncated
	// length. We also lose our write token, due to a race between
	// a local writer and the time it takes for the server thread to
	// clear out all the caches associated with the pages,
	// and setting the actual size on the underlying filesystem.
	//

	//
	// 'offset' represents the length of the new file. If 'offset'
	// is zero, the file has been truncated to zero length we need
	// invalidate all the pages associated with the file.  Otherwise
	// invalidate from 'offset' to the end.
	//
	if (offset != 0) {
		error = putpage_remote((u_offset_t)0, (size_t)offset,
		    B_INVAL | B_FORCE, kcred);
	}

	//
	// This operation pushes all dirty pages to the server and
	// destroys the page.
	//
	error = putpage_remote((offset & PAGEMASK), (size_t)0,
	    B_INVAL | B_TRUNC, kcred);

	if (error == 0) {
		// putpage_remote succeeded, vnode must not have pages.
		CL_PANIC(!vn_has_cached_data(get_vp()));

		if (pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn() ==
		    server_incarnation) {
			//
			// Modify the data privileges only when the
			// operation was requested by the current primary.
			// The primary could have changed midway through
			// this function.
			//
			data_token_lock.wrlock();
			seqnum++;
			cur_data_token = 0;
			data_token_lock.unlock();
		}
	} else {
		_environment.exception(new sol::op_e(error));
	}

	cache_lock.unlock();
}

//
// data_change_cache_flag - the server is informing us that
// there is a change in enabling/disabling caching.
//
void
pxreg::data_change_cache_flag(bool onoff, Environment &_environment)
{
	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_RED,
		    ("change_cache(%p) orphan ndid %d\n",
		    this, _environment.get_src_node().ndid));
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	change_cachedata_flag(onoff);
}

//
// Check to see if [off, len) is contained within the current extended file
// length range (the amount that can be written without allocating more
// disk blocks) and return true if so.
//
bool
pxreg::in_ext_range(u_offset_t off, size_t len)
{
	ASSERT(low_lock.lock_held());
	ASSERT(cur_data_token & PXFS_WRITE_TOKEN);

	//
	// If any part of the requested range is outside the cached extended
	// range, return false.
	//
	if (off < ext_offset || off + len > ext_offset + ext_length) {
		return (false);
	} else {
		return (true);
	}
}

void
pxreg::set_ext_range(u_offset_t off, size_t len)
{
	ASSERT(low_lock.lock_held());

	// Extended writes possible only if underlying filesysten is UFS.
	if (pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_underlying_fs_type() !=
	    pxvfs::UFS) {
		return;
	}

	ext_offset = off;
	ext_length = len;
}

int
pxreg::ioctl(int cmd, intptr_t arg, int flag, struct cred *credp, int *retp)
{
	int	result;

#ifndef VXFS_DISABLED
	if (is_setext_ioctl(cmd)) {
		result = vxfs_ioctl_setext(cmd, arg, flag, credp, retp);
	} else {
		result = pxfobj::ioctl(cmd, arg, flag, credp, retp);
	}
#else
	result = pxfobj::ioctl(cmd, arg, flag, credp, retp);
#endif
	return (result);
}

//
// recover_state - the new server is reconstructing state by
// asking each client for its cached state.
//
void
pxreg::recover_state(PXFS_VER::recovery_info_out recovery, Environment &env)
{
	// Must recover the base class info first
	pxfobjplus::recover_state(recovery, env);

	PXFS_VER::recovery_info	*recoveryp =
	    (PXFS_VER::recovery_info *)recovery;

	recoveryp->data_rights = cur_data_token;
} //lint !e1746

int
pxreg::write_uncached(uio *uiop, int ioflag, cred *credp)
{
	int i;
	uio new_uio;
	size_t bufsz = 0;
	caddr_t bufptr = NULL;
	iovec new_iov[DEF_IOV_MAX];
	iovec *last_iov = &new_iov[DEF_IOV_MAX-1];

	sol::size_t		iolen = (sol::size_t)uiop->uio_resid;
	Environment		e;
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	int			err = 0;

	//
	// uio_iovcnt cannot be less than zero or greater than DEF_IOV_MAX if
	// entering this code from readv() or writev() as the limit is
	// enforced in userland. However, there is no such limit covering
	// kernel to kernel calls e.g. NFS (v3 or v4) can legitimately supply
	// a uio pointer with >DEF_IOV_MAX iovecs when performing a write().
	//
	// If we receive <0 iovecs, or >DEF_IOV_MAX iovecs and the call is a
	// read() or made from userland, then we return EIO as this is an
	// unexpected situation i.e. it shouldn't happen.
	//

	if (uiop->uio_iovcnt < 0 ||
	    (uiop->uio_iovcnt > DEF_IOV_MAX &&
	    uiop->uio_segflg != UIO_SYSSPACE)) {

		cmn_err(CE_WARN,
		    "write_uncached: Returning EIO as uio_iovcnt is %d\n",
		    uiop->uio_iovcnt);
		return (EIO);
	}

	//
	// If fastwrite is enabled account for the blocks that this write
	// would consume.
	//
	if (pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->fastwrite_enabled()) {
		PXFS_VER::blkcnt_t blocks_required = required_blocks(iolen);

		//
		// It's OK to ignore the return value of reserve_blocks()
		// because, it has either reserved the required number of
		// blocks or the server has switched to REDZONE. In either
		// case there is nothing additional that needs to be done.
		//
		(void) pxvfs::VFSTOPXFS((get_vp())->v_vfsp)->
		    reserve_blocks(blocks_required, false, err);
		if (err != 0) {
			return (err);
		}
	}

	if (uiop->uio_iovcnt > DEF_IOV_MAX) {
		ASSERT(uiop->uio_segflg == UIO_SYSSPACE);
		//
		// We've been passed a uio structure with >DEF_IOV_MAX iovec's
		// (in SYSSPACE). However, the bulkio and Buf classes are both
		// built around the DEF_IOV_MAX limit - which should really
		// only apply to the readv() & writev() system calls.
		//
		// To work around this mismatch, we create a new uio & iovec
		// list and merge the data blocks from the iovecs beyond
		// DEF_IOV_MAX into one big iovec so that the new uio
		// structure can fit into a single bulkio object.
		//

		new_uio = *uiop;
		new_uio.uio_iov = new_iov;
		new_uio.uio_iovcnt = DEF_IOV_MAX;

		for (i = 0; i < uiop->uio_iovcnt; i++) {
			if (i < DEF_IOV_MAX-1) {
				//
				// Copy only the first DEF_IOV_MAX-1 iovec's
				//
				new_uio.uio_iov[i] = uiop->uio_iov[i];
			} else {
				//
				// We don't copy iovecs above DEF_IOV_MAX yet,
				// but work out how much data there is
				//
				bufsz += uiop->uio_iov[i].iov_len;
			}
		}
		ASSERT(bufsz > 0);

		//
		// Allocate the new buffer and merge the data blocks pointed
		// to by the last iovecs into it
		//
		last_iov->iov_base = bufptr =
		    (caddr_t)kmem_alloc((size_t)bufsz, KM_SLEEP);

		last_iov->iov_len = bufsz;

		for (i = DEF_IOV_MAX-1; i < uiop->uio_iovcnt; i++) {
			bcopy(uiop->uio_iov[i].iov_base, bufptr,
			    uiop->uio_iov[i].iov_len);

			bufptr += uiop->uio_iov[i].iov_len;
		}

		PXFS_DBPRINTF(
		    PXFS_TRACE_REG,
		    PXFS_GREEN,
		    ("pxreg::write_uncached: Merged %d iovecs into %d byte "
		    "buffer %p\n", uiop->uio_iovcnt-DEF_IOV_MAX+1, bufsz,
		    last_iov->iov_base));

		bulkio::in_uio_var uioobj =
		    bulkio_impl::conv_in(&new_uio, iolen);

		getfile()->uiowrite(iolen, uioobj, ioflag, credobj, e);

		//
		// The data pointed to by our new iovecs has been
		// written now, so we can free the new buffer. Note
		// that the original uio and iovecs are still intact,
		// and uioskip() (below) updates the uio_resid &
		// _uio_offset accordingly.
		//
		kmem_free(last_iov->iov_base, bufsz);
	} else {
		bulkio::in_uio_var uioobj =
		    bulkio_impl::conv_in(uiop, iolen);

		getfile()->uiowrite(iolen, uioobj, ioflag, credobj, e);
	}
	// XXX In case of error, is iolen set to 0 or whatever is appropriate?
	int	error = pxfslib::get_err(e);
	if (error == 0) {
		ASSERT(iolen <= (sol::size_t)uiop->uio_resid);
		uioskip(uiop, iolen);
	}
	return (error);
}

int
pxreg::read_uncached(uio *uiop, int ioflag, cred *credp)
{
	sol::size_t		iolen = (sol::size_t)uiop->uio_resid;
	Environment		e;
	solobj::cred_var	credobj = solobj_impl::conv(credp);

	//
	// uio_iovcnt cannot be less than zero or greater than DEF_IOV_MAX if
	// entering this code from readv() or writev(). The only case when this
	// can occur is when NFS does a read or write in direct I/O mode. Return
	// EIO with the hope that the server combines the iovectors.
	//
	if (uiop->uio_iovcnt < 0 || uiop->uio_iovcnt > DEF_IOV_MAX) {
		cmn_err(CE_WARN,
		    "read_uncached: Returning EIO as uio_iovcnt is %d\n",
		    uiop->uio_iovcnt);
		return (EIO);
	}

	bulkio::inout_uio_var	uioobj = bulkio_impl::conv_inout(uiop, iolen);
	getfile()->uioread(iolen, uioobj.INOUT(), ioflag, credobj, e);

	// XXX In case of error, is iolen set to 0 or whatever is appropriate?
	int	error = pxfslib::get_err(e);
	if (error == 0) {
		ASSERT(iolen <= (sol::size_t)uiop->uio_resid);
		uioskip(uiop, iolen);
	}
	return (error);
}

//
// Write back dirty pages and wait for async io if any to complete.
//
int
pxreg::sync_file()
{
	int error = 0;

	//
	// Pending writes may have made block allocations that may have been
	// lost. We have to wait for all on going write_cache()s to complete
	// before returning and turning the recovery flag off.
	// check_wait_lock_cache() waits for all VOP_WRITE()s that may have
	// called bmap() to drain.
	//
	check_wait_lock_cache();

	if (vn_has_cached_data(get_vp())) {
		error = putpage_remote((u_offset_t)0, (size_t)0, 0, kcred);
	}

	cache_lock.unlock();

	return (error);
}

//
// Write back dirty pages and wait for async io if any to complete.
// This is similar to ::sync_file(). Except that we could have threads
// blocked in reserve_blocks() through write_cache() waiting for blocks to be
// reserved.
//
int
pxreg::sync_file_revoke()
{
	int error = 0;

	//
	// Wait for writes in progress but not blocked in reserve_blocks()
	// to complete.
	//
	check_wait_lock_cache_revoke();

	if (vn_has_cached_data(get_vp())) {
		//
		// We are here because a request for block reservation resulted
		// in the filesystem switching to REDZONE and the server asked
		// clients to flush dirty pages. The reservation call could have
		// happened in the context of a pagefault, while writing to a
		// page which was a hole. The page is thus already locked and
		// existing putpage*() routines will deadlock while trying to
		// lock the page. Hence we call our own putpage routine,
		// putpage_revoke(), which skips locked pages.
		//
		error = putpage_revoke();
	}

	//
	// The putapage above may have queued async requests, wait for
	// them to drain.
	//
	drain_asyncs(false);
	cache_lock.unlock();

	return (error);
}

//
// putpage_revoke - forces pages to storage.
//
// This is clone of putpage() and does the follwing :
// 1. It doesn't acquire cache_lock
// 2. Skips IO locked pages
// 3. Issues synchronous pageout's for dirty pages
//
// NOTE:
// 1> This is used only by sync_file_revoke() to flush all the dirty pages
//    while switching to slowpath.
// 2> We don't use pvn_vplist_dirty() because that would wait on
//    IO locked paged.
//
int
pxreg::putpage_revoke()
{
	int		flags = 0;

	if (!is_cached()) {
		return (0);
	}

	vnode		*vnodep = pxnode::PXTOV(this);
	int		error;

	//
	// If the filesystem has been unmounted already (eg. a forced
	// unmount) we use B_INVAL | B_FORCE to destroy the pages.
	//
	if ((pxvfs::VFSTOPXFS(get_vp()->v_vfsp))->is_unmounted()) {
		flags = flags | B_INVAL | B_FORCE;
	}

	//
	// Loop over all offsets in the range [0...file-size]
	// looking for pages to deal with.  We set limits so
	// that we kluster to klustsize boundaries.
	//
	u_offset_t	uoff = (u_offset_t)0;
	u_offset_t	eoff;
	u_offset_t	io_off;
	size_t		io_len;
	page_t		*pp;

	//
	// We only need the read token here
	//
	if ((error = get_attr_read_token(kcred)) != 0) {
		return (error);
	}
	eoff = get_length();
	drop_attr_token();

	data_token_lock.rdlock();
	for (error = 0, io_off = uoff;
	    !error && io_off < eoff;
	    io_off += io_len) {

		pp = page_lookup_nowait(vnodep, io_off,
			(flags & B_FREE) ? SE_EXCL : SE_SHARED);
		if (pp == NULL || pvn_getdirty(pp, flags) == 0) {
			//
			// No need to write this page back to the server.
			// Either the page:
			//	1) does not exist
			//	or
			//	2) no information needs to written to disk
			//
			io_len = PAGESIZE;
		} else {
			// pvn_getdirty says that the page is dirty,
			// and has marked the page as having an I/O in progress
			// (PAGE_IO_INUSE bit set in p_iolock).
			//
			// "io_off" and "io_len" are returned as the range of
			// pages we actually wrote. This allows us to skip
			// ahead more quickly since several pages may have
			// been dealt with by this iteration of the loop.
			//
			error = putapage(vnodep, pp, &io_off, &io_len,
			    flags, kcred);
			if (error) {
				break;
			}
		}
	}
	data_token_lock.unlock();
	return (error);
}
