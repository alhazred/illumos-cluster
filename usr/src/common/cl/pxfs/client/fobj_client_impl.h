//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FOBJ_CLIENT_IMPL_H
#define	FOBJ_CLIENT_IMPL_H

#pragma ident	"@(#)fobj_client_impl.h	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)

// forward declaration
class pxfobjplus;

//
// fobj_client_impl - acts a general fobj client.
// All operations from the server to the client are invoked
// upon this object.
//
// The PXFS client does not always know the fobj type when first
// contacting the server. We want to establish the connection
// between client and server with one IDL operation. This reduces
// round trips across the interconnect for remote clients.
// This object only provides plumbing for connecting client and server.
// No client file information is retained in this object.
// Not applicable IDL operations are simply not used.
//
// There can only be one registered fobj client on a node for a particular fobj.
//
// The client node can discover a proxy vnode either through the
// local hash table or from the server. This fact coupled with
// multi-threading makes safely cleaning up challenging.
// The following describes the states that a fobj client goes through:
//
// State 0 - Initial State - not yet useable
// fobj_client
//	flags - RELEASED
//	CORBA refs = 1
//	Not yet connected to a proxy vnode
// Action
//	_unreferenced - destroy fobj_client
//	set_pxfobjplus -> state 1
//
// State 1 - Registered State
// fobj_client
//	flags - REGISTERED
//	CORBA refs >= 1
// pxfobjplus
//	flags -
//	v_count >= 1
//	can have dirty info
// Action
//	fobjhash_insert -> state 2
//
// State 2 - Ready State - initialization complete.
// fobj_client
//	flags - REGISTERED
//	CORBA refs >= 1
// pxfobjplus
//	flags - PX_FOBJHASH
//	v_count >= 1
//	can have dirty info
// Action
//	VN_RELE && v_count == 1 -> state 3
//
// State 3 - Become Inactive State - queued for cleanup
// fobj_client
//	flags - REGISTERED
//	CORBA refs >= 1
// pxfobjplus
//	flags - PX_INACTIVELIST
//	v_count == 1
//	can have dirty info
// Action
//	VN_HOLD -> state 2
//	become_stale -> state 4
//
// State 4 -  Become Stale State - flush dirty info to server
// fobj_client
//	flags - REGISTERED RELEASE_PENDING
//	CORBA refs >= 1
// pxfobjplus
//	flags -
//	v_count == 1
//	can not have dirty info
// Action
//	VN_HOLD -> state 2
//	otherwise -> state 5
//
// State 5 -  Stale State - the proxy vnode will go away
// fobj_client
//	flags - REGISTERED RELEASE_PENDING
//	CORBA refs >= 1
// pxfobjplus
//	flags - PX_STALE
//	v_count == 1
//	can not have dirty info
// Action
//	deregister from server -> state 6
//
// State 6 -  Zombie State
// fobj_client
//	flags - RELEASED
//	CORBA refs - told server to drop ref and we dropped our self-reference,
//			so reference count will drop to zero.
// pxfobjplus - no longer exists
// Action
//	_unreferenced - destroy fobj client
//
// The fobj_client effectively holds a self-reference
// from the time of the proxy vnode creation till the proxy vnode destruction.
// This avoids the reference count going to zero between the time
// the primary dies and a new primary recovers state.
//
class fobj_client_impl : public McServerof<PXFS_VER::fobj_client> {
public:
	fobj_client_impl();
	~fobj_client_impl();
	void	_unreferenced(unref_t arg);

	//
	// The fobj_client can be used by the PXFS client side kernel
	// or by the PXFS server via invocations.
	// These functions are used to synchronize between the two sides
	// when releasing the fobj_client.
	//
	void	set_pending_release();
	void	cancel_pending_release();
	void	set_flushed();
	void	rele();			// called when proxy vnode is deleted

	//
	// Set the associated proxy vnode object.
	// This is used only during the client registration phase.
	//
	void		set_pxfobjplus(pxfobjplus *px_fobjplusp);

	pxfobjplus	*get_pxfobjplusp();

	bool		wait_till_ok();

	PXFS_VER::fobj_client_ptr	recover_client_ref();

	// Methods supporting IDL operations

	// The following support operations for an arbitrary fobj client

	virtual void	get_open_count(uint32_t &count,
	    Environment &_environment);

	virtual void	get_vnode_count(uint32_t &count,
	    Environment &_environment);

	// The following support attribute operations

	virtual void	attr_release(uint64_t attr_seqnum,
	    uint64_t access_seqnum, Environment &_environment);

	virtual void	attr_write_back_and_release(uint64_t attr_seqnum,
	    uint64_t access_seqnum, sol::vattr_t &attributes,
	    Environment &_environment);

	virtual void	attr_write_back_and_downgrade(uint64_t attr_seqnum,
	    uint64_t access_seqnum, sol::vattr_t &attributes,
	    Environment &_environment);

	virtual void clear_attr_dirty(uint32_t server_incarn,
	    Environment &_environment);

	// The following support access cache operations

	virtual void	access_inval_cache(uint64_t access_seqnum,
	    Environment &_environment);

	// The following support directory cache operations

	virtual void	dir_inval_entry(const char *nm,
	    Environment &_environment);

	virtual void	dir_inval_all(Environment &_environment);

	// The following support data cache operations

	virtual void	data_write_back_and_delete(uint32_t server_incarnation,
	    Environment &_environment);

	virtual void	data_write_back_downgrade_read_only(
	    uint32_t server_incarnation, Environment &_environment);

	virtual void	data_write_back(Environment &_environment);

	virtual void	data_delete_range(sol::u_offset_t offset,
	    uint32_t server_incarnation, Environment &_environment);

	virtual void	data_change_cache_flag(bool onoff,
	    Environment &_environment);

	// End methods supporting IDL operations

private:
	bool	client_ready();

	// Disallow assignments and pass by value.
	fobj_client_impl(const fobj_client_impl &);
	fobj_client_impl &operator = (fobj_client_impl &);

	// Definition of values in cflags.
	enum {
		//
		// Set once the client registers with the server.
		// We never clear this bit in order to avoid
		// a very rare race condition.
		//
		REGISTERED	= 0x01,

		//
		// Have begun the process of cleaning up the proxy file object.
		// The client may or may not go away.
		//
		RELEASE_PENDING = 0x02,

		//
		// Set once client has successfully flushed info to server
		//
		FLUSHED		= 0x04,

		//
		// The client is definitely going away. Set prior to
		// informing the server.
		//
		DISCONNECTING	= 0x08,

		//
		// The process of releasing the client has completed,
		// and the server has been informed.
		//
		RELEASED	= 0x10,

		// _unreferenced() called
		UNREFERENCED	= 0x20
	};

	//
	// The proxy vnode object contains all of the state and
	// supports all of the real operations.
	//
	pxfobjplus	*pxfobjplusp;

	// Protects the variables in this object
	os::mutex_t	state_lock;

	//
	// Used to synchronize with threads waiting for
	// completion of client registration.
	//
	os::condvar_t	registered_cv;

	// Client state flags (see above)
	uchar_t		cflags;
};

#include <pxfs/client/fobj_client_impl_in.h>

#endif	// FOBJ_CLIENT_IMPL_H
