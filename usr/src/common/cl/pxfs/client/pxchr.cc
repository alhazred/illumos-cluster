//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pxchr.cc - proxy vnode for character special devices (i.e. VCHR vnodes).
//

#pragma ident	"@(#)pxchr.cc	1.7	08/05/20 SMI"

#include <sys/types.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/uio.h>
#include <sys/debug.h>
#include <sys/flock.h>
#include <sys/fs/pxfs_ki.h>
#include <sys/aio_req.h>
#include <sys/aio_impl.h>
#include <sys/cmn_err.h>

#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include <pxfs/common/pxfslib.h>
#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxchr.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/aio_callback_impl.h>
#include <pxfs/bulkio/bulkio_impl.h>

//
// Solaris defines cmpdev in multiple places.
// This file needs the version in the following header file.
//
#include <sys/sysmacros.h>

#if SOL_VERSION >= __s9
#define	S9_BP_CLEANUP
#endif

//
// XXX
// DEF_IOV_MAX is not defined in any Solaris header file. It is instead
// defined in uts/common/syscall/rw.c . A PSARC contract needs to be pursued
// to get DEF_IOV_MAX defined in a header file that we can include.
//
#define	DEF_IOV_MAX	16	//lint -e760

//
// mmap routine defined in specfs.
// For now, mmap of pxfs devices will only work if device is local
// to the node.
//
extern "C" int spec_char_map(dev_t, offset_t, as *, caddr_t *, size_t, uchar_t,
    uchar_t, uint_t, cred_t *);

pxchr::pxchr(vfs *vfsp, PXFS_VER::io_ptr udp,
    const PXFS_VER::fobj_info &fobjinfo) :
	pxfobj(vfsp, udp, fobjinfo)
{
	real_vp = NULL;
	is_hadev = false;
}

pxchr::~pxchr()
{
	if (real_vp != NULL) {
		//
		// real_vp has been initialized, hence VN_HELD,
		// so release it here.
		//
		VN_RELE(real_vp);
	}
} //lint !e1540 pointers are neither freed nor zero'ed by destructor

void
pxchr::set_realvp(vnode *rvp, bool is_ha)
{
	ASSERT(rvp != NULL);

	vnode_t *vp = get_vp();

	mutex_enter(&(vp->v_lock));

	is_hadev = is_ha;

	if (real_vp == NULL) {
		//
		// NOTE: "rvp" has already been VN_HELD - it is
		// up to this object to release it when appropriate.
		//
		real_vp = rvp;
		mutex_exit(&(vp->v_lock));
		return;
	}

	mutex_exit(&(vp->v_lock));

	// NOTE: real vp does not change!
	ASSERT(rvp == real_vp);

	VN_RELE(rvp);
}

//
// VOP_READ operation on a VCHR proxy vnode.
//
int
pxchr::read(uio *uiop, int ioflag, cred *credp)
{
	sol::size_t		iolen = (sol::size_t)uiop->uio_resid;
	Environment		e;
	solobj::cred_var	credobj = solobj_impl::conv(credp);

	//
	// uio_iovcnt cannot be less than zero or greater than DEF_IOV_MAX if
	// entering this code from readv() or writev(). The only case when this
	// can occur is when NFS does a read or write in direct I/O mode. Return
	// EIO with the hope that the server combines the iovectors.
	//
	if (uiop->uio_iovcnt < 0 || uiop->uio_iovcnt > DEF_IOV_MAX) {
		cmn_err(CE_WARN,
		    "pxchr::read: Returning EIO as uio_iovcnt is %d\n",
		    uiop->uio_iovcnt);
		return (EIO);
	}

	bulkio::inout_uio_var	uioobj = bulkio_impl::conv_inout(uiop, iolen);
	getio()->uioread(iolen, uioobj.INOUT(), ioflag, credobj, e);

	// XXX In case of error, is iolen set to 0 or whatever is appropriate?
	int	error = pxfslib::get_err(e);
	if (error == 0) {
		ASSERT(iolen <= (sol::size_t)uiop->uio_resid);
		uioskip(uiop, iolen);
	}
	return (error);
}

//
// VOP_WRITE operation on a VCHR proxy vnode.
//
int
pxchr::write(uio *uiop, int ioflag, cred *credp)
{
	sol::size_t		iolen = (sol::size_t)uiop->uio_resid;
	Environment		e;
	solobj::cred_var	credobj = solobj_impl::conv(credp);

	//
	// uio_iovcnt cannot be less than zero or greater than DEF_IOV_MAX if
	// entering this code from readv() or writev(). The only case when this
	// can occur is when NFS does a read or write in direct I/O mode. Return
	// EIO with the hope that the server combines the iovectors.
	//
	if (uiop->uio_iovcnt < 0 || uiop->uio_iovcnt > DEF_IOV_MAX) {
		cmn_err(CE_WARN,
		    "pxchr::write: Returning EIO as uio_iovcnt is %d\n",
		    uiop->uio_iovcnt);
		return (EIO);
	}

	bulkio::in_uio_var	uioobj = bulkio_impl::conv_in(uiop, iolen);
	getio()->uiowrite(iolen, uioobj, ioflag, credobj, e);

	// XXX In case of error, is iolen set to 0 or whatever is appropriate?
	int	error = pxfslib::get_err(e);
	if (error == 0) {
		ASSERT(iolen <= (sol::size_t)uiop->uio_resid);
		uioskip(uiop, iolen);
	}
	return (error);
}

int
pxchr::open(vnode **vnodepp, int flag, cred *credp)
{
	Environment		e;
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	PXFS_VER::fobj_var	fobjp2;
	PXFS_VER::fobj_info	fobjinfo;
	vnode_t			*oldvp;

	getio()->open(flag, fobjp2, fobjinfo, credobj, e);
	sol::error_t	error = pxfslib::get_err(e);
	if (error) {
		return (error);
	}

	//
	// Release the old vnode obtained during lookup
	// and create a new one for the object reference
	// returned by open().
	//
	oldvp = *vnodepp;
	*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
	    get_pxfobj(fobjp2, fobjinfo, NULL, NULL);
	ASSERT(*vnodepp != NULL);
	VN_RELE(oldvp);
	return (0);
}

int
pxchr::close(int flag, int cnt, offset_t offset, cred *cr)
{
	int error;

	// Handle file and share lock cleanup.
	error = pxfobj::close(flag, cnt, offset, cr);
	if (error) {
		return (error);
	}
	error = fsync(0, cr);
	if (error) {
		return (error);
	}

	// Do nothing if the caller holds other references to the vnode.
	if (cnt > 1) {
		return (0);
	}

	Environment e;
	solobj::cred_var credobj = solobj_impl::conv(cr);
	getio()->close(flag, (sol::u_offset_t)offset, credobj, e);
	return (pxfslib::get_err(e));
}

int
pxchr::fsync(int syncflag, struct cred *cr)
{
	solobj::cred_var cred_obj = solobj_impl::conv(cr);
	Environment e;

	getio()->cascaded_fsync(syncflag, cred_obj, e);
	return (pxfslib::get_err(e));
}

//
// Mmap for local devices.
//
int
pxchr::map(offset_t off, as *asp, caddr_t *addrp, size_t len, uchar_t prot,
    uchar_t maxprot, uint_t flags, cred *cr)
{
	PXFS_VER::fobj_ptr fobj;

	fobj = getfobj();

	//
	// For HA devices the following won't work.
	// XXX need to think for HA devices.
	//
	if (!fobj->_is_local()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_CHR,
		    PXFS_RED,
		    ("Map for a remote server object so fail!\n")); // XXX
		return (ENODEV);
	}

	if (get_vp()->v_type == VCHR) {
		return (spec_char_map(get_vp()->v_rdev, off, asp, addrp, len,
		    prot, maxprot, flags, cr));
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_CHR,
		    PXFS_RED,
		    ("Block device mmap not yet supported\n")); // XXX
		return (ENODEV);
	}
}

int
pxchr::realvp(vnode** vp)
{
	*vp = real_vp;
	return (0);
}

//
// The aio read/write routine. It allocates a callback object
// and sends to server.
// The bulkio aio object stores the aioreq structure, and the
// callback object implementation has a pointer to the bulkio aio
// object.
//
int
pxchr::pxfs_aiorw(struct aio_req *aiop, cred_t *cred_p, int iswrite,
    bool isretry)
{
	Environment e;
	solobj::cred_var credobj = solobj_impl::conv(cred_p);
	aio_req_t *reqp = (aio_req_t *)aiop->aio_private;
	struct buf *bp = &reqp->aio_req_buf;
	struct uio *uiop = aiop->aio_uio;
	struct iovec *iov;
	dev_t dev = get_vp()->v_rdev;

	// We have a test case that sets iov_len to -1 for a 64-bit kernel.
	// iov_len is a size_t (an unsigned variable) so it really becomes
	// 2^64 which is much larger than what we support. We don't support
	// transfers greater than 2^31-1 so we should return EINVAL here.

	if ((uiop->uio_iov->iov_len > INT_MAX) ||
	    (uiop->uio_resid > INT_MAX)) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_CHR,
		    PXFS_RED,
		    ("unsupported aio length\n"));
		return (EINVAL);
	}

	if ((uiop->uio_iov->iov_len < 0) || (uiop->uio_loffset < 0)) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_CHR,
		    PXFS_RED,
		    ("uio len and offset are -ve\n"));
		return (EINVAL);
	}

	if (uiop->uio_iov->iov_base == NULL) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_CHR,
		    PXFS_RED,
		    ("iov_base is NULL\n"));
		return (EFAULT);
	}

	if (!isretry) {
		iov = uiop->uio_iov;
		uiop->uio_resid = iov->iov_len;
		bp->b_proc = curproc;
		//
		// B_KERNBUF flag obsoleted by s9
		//
#ifdef S9_BP_CLEANUP
		bp->b_flags = B_BUSY | B_PHYS | B_ASYNC |
		    (iswrite ? B_WRITE : B_READ);
#else
		bp->b_flags = B_KERNBUF | B_BUSY | B_PHYS | B_ASYNC |
		    (iswrite ? B_WRITE : B_READ);
#endif
		bp->b_forw = (struct buf *)reqp;
		//
		// b_oerror field obsoleted in s9
		//
#ifndef S9_BP_CLEANUP
		bp->b_oerror = 0;
#endif
		bp->b_error = 0;
		bp->b_iodone = NULL;
		bp->b_edev = dev;
		bp->b_dev = cmpdev(dev);
		bp->b_lblkno = btodt(uiop->uio_loffset);
		bp->b_un.b_addr = iov->iov_base;
		bp->b_bcount = (size_t)iov->iov_len;

	} else {
		//
		// Retry of async io is allowed only
		// if the device is a HA device.
		//
		ASSERT(is_hadev);
		ASSERT(bp->b_proc);
		PXFS_DBPRINTF(
		    PXFS_TRACE_CHR,
		    PXFS_AMBER,
		    ("doing a retry (%p)\n", this));
	}

	bulkio::in_aio_var aioobj = bulkio_impl::aconv_in(aiop,
	    (iswrite ? B_WRITE : B_READ));

	ha_async *haio = NULL;
	if (is_hadev) {
		haio = new ha_async(aiop, cred_p, aioobj, (void *)this,
		    iswrite, orb_conf::node_number(), bp->b_proc->p_pid);
	}

	//
	// The aio_callback_impl object also stores a reference to the bulkio
	// `aioobj' created earlier. This is a necessary addition for
	// server-pull. In server-pull, the transport points to buffers in
	// bulkio that in turn map-in and lock pages containing the aio buffer.
	//
	// Normally, aio_callback is responsible for unlocking and mapping-out
	// these pages. This is because aio_callback calls aio_done() which
	// in turn maps out buffers if they were mapped in. It also signals
	// aio_cleanup(), which in turn unlock pages.
	//
	// If aio_callback() occurs before TCP on the client side receives
	// acks for the data it points to, we have several problems. To
	// ensure that the unlocking of pages occurs only after bulkio and
	// the transport are done using these resources, we store a reference
	// to the bulko client-side object called `aioobj' in aio_callback_impl.
	// When the callback occurs, the reference is used to call aioobj's
	// destructor which in turn waits for other destructors that ultimately
	// wait for the transport to signal that the I/O has completed and
	// all acks have been received.
	//

	// There's a bug or feature (?) in the impl_spec class from which
	// bulkio objects inherit. The problem is that new() sets the refcount
	// for the object to 1 and get_objref() doesn't change the refcount.
	// So we do a _duplicate for now in the constructor of aio_callback_impl
	//

	aio_callback_impl *aiocbk = new aio_callback_impl(aiop, is_hadev, haio,
	    aioobj);

	PXFS_DBPRINTF(
	    PXFS_TRACE_CHR,
	    PXFS_GREEN,
	    ("aio callback object is %p\n", aiocbk));
	pxfs_aio::aio_callback_var aiocbkobj = aiocbk->get_objref();

	//
	// XXX A change we should make later:
	// Rather than saving an in_aio_ptr in aio_callback_impl's constructor,
	// we can save an in_aio_ptr and then do a bulkio_impl::in_aio::_nil()
	// instead of release(). This is cleaner.
	//

	if (iswrite) {
		getio()->aiowrite(aiocbkobj, aioobj, credobj, e);
		if (e.exception() != NULL) {
			bp->b_flags |= B_ERROR;
			bp->b_resid = 0;
			bp->b_error = pxfslib::get_err(e);
			PXFS_DBPRINTF(
			    PXFS_TRACE_CHR,
			    PXFS_RED,
			    ("wr had ERR %d isretry %d\n",
			    bp->b_error, isretry));

			// Error: cancel the aio to unlock the pages.
			if (!isretry) {
				bulkio_impl::in_aio_clnt::get_impl_obj(
				    aioobj)->real_destructor();

				bulkio_impl::aio_cancel(aioobj);
				aioobj = bulkio::in_aio::_nil();
				aiocbk->clear_cbkobj();
				aiocbk->signal_invo_complete(WITH_EXCEPTION);
				return (bp->b_error);
			} else {
				//
				// This is the weird case where the
				// io was successful with the first
				// primary but is returning an error
				// after the failover or switchover.
				// Also happens when all the
				// primaries fail.
				// Since this is done on a retry
				// thread without user context,
				// we can't call aio_cancel.
				// Instead call aio_done() with bp set
				// to indicate an error during aio completion.
				//

				//
				// There's a bug here. See bug 4257446.
				// Pages might not be locked
				// if the first aio went through, but
				// but the retry failed since all primaries
				// died.
				//

				//
				// lock_pages() is a workaround for the bug.
				//

				bulkio_impl::in_aio_clnt::get_impl_obj(
				    aioobj)->lock_pages();
				bulkio_impl::in_aio_clnt::get_impl_obj(
				    aioobj)->real_destructor();
				aioobj = bulkio::in_aio::_nil();
				aio_done(bp);
				aiocbk->clear_cbkobj();
				aiocbk->signal_invo_complete(WITH_EXCEPTION);
				return (bp->b_error);
			}
		} else {
			aiocbk->signal_invo_complete(NO_EXCEPTION);
		}
	} else {
		getio()->aioread(aiocbkobj, aioobj, credobj, e);
		if (e.exception() != NULL) {
			bp->b_flags |= B_ERROR;
			bp->b_resid = 0;
			bp->b_error = pxfslib::get_err(e);
			PXFS_DBPRINTF(
			    PXFS_TRACE_CHR,
			    PXFS_RED,
			    ("rd had ERR %d isretry %d\n",
			    bp->b_error, isretry));
			if (!isretry) {
				bulkio_impl::in_aio_clnt::get_impl_obj(
				    aioobj)->real_destructor();
				bulkio_impl::aio_cancel(aioobj);
				aioobj = bulkio::in_aio::_nil();
				aiocbk->clear_cbkobj();
				aiocbk->signal_invo_complete(WITH_EXCEPTION);
				return (bp->b_error);
			} else {
				//
				// This is the weird case.
				// Refer to the comments above.
				// XXX Check on the error returned.
				//

				//
				// Bug 4257446.
				// See comments in the aio_write case.
				//

				// lock_pages() is a workaround for the bug.

				bulkio_impl::in_aio_clnt::get_impl_obj(
				    aioobj)->lock_pages();
				bulkio_impl::in_aio_clnt::get_impl_obj(
				    aioobj)->real_destructor();
				aioobj = bulkio::in_aio::_nil();
				aio_done(bp);
				aiocbk->clear_cbkobj();
				aiocbk->signal_invo_complete(WITH_EXCEPTION);
				return (bp->b_error);
			}
		} else {
			aiocbk->signal_invo_complete(NO_EXCEPTION);
		}
	}
	return (0);
}
