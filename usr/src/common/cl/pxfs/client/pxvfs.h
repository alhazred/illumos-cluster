/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PXVFS_H
#define	_PXVFS_H

#pragma ident	"@(#)pxvfs.h	1.21	09/01/13 SMI"

#include <vm/page.h>
#include <sys/vnode.h>
#include <sys/flock.h>
#include <sys/fs/pxfs_ki.h>

#include <sys/refcnt.h>
#include <sys/list_def.h>
#include <sys/threadpool.h>
#include <orb/monitor/monitor.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include PXFS_IDL(pxfs)
#include <pxfs/client/pxfobj.h>
#include <pxfs/client/pxfs_llm_callback_impl.h>

#define	PXVFS_STATS_NUM_OPEN_FILES	0
#define	PXVFS_STATS_ACCESS_TOKEN_HITS	1
#define	PXVFS_STATS_ACCESS_TOKEN_MISSES	2
#define	PXVFS_STATS_ACCESS_TOKEN_INVALS	3
#define	PXVFS_STATS_ATTR_TOKEN_HITS	4
#define	PXVFS_STATS_ATTR_TOKEN_MISSES	5
#define	PXVFS_STATS_ATTR_TOKEN_INVALS	6
#define	PXVFS_STATS_DATA_TOKEN_HITS	7
#define	PXVFS_STATS_DATA_TOKEN_MISSES	8
#define	PXVFS_STATS_DATA_TOKEN_INVALS	9
#define	PXVFS_STATS_DATA_ALLOC		10
#define	PXVFS_STATS_DATA_TOKEN_RETRIES	11
#define	PXVFS_STATS_THROTTLING_HITS	12
#define	PXVFS_STATS_MAX_NUM		13

#define	PXVFS_NODE_STATS_NUM_OPEN_FILES	0
#define	PXVFS_NODE_STATS_MAX_NUM	1

//
// Used only when mounting without syncdir.
// We are using a portion of the flag space from /usr/include/sys/file.h.
// This isn't nice but memcache_impl::fsync() will treat the following
// definition as an int instead of a flag to avoid collisions with
// traditional fsync flags.
//
#define	PXFS_DESTROY_PAGES		0xffff

extern struct vfsops *pxfs_vfsopsp;

// Forward declarations.
class pxvfs;
class fsmgr_client_impl;
class fobj_client_impl;
class pxfobjplus;

// Type for list of all pxvfs objects.
typedef IntrList<pxvfs, _SList> pxvfs_list_t;

//
// pxvfs_inactive_task - this task represents a need to process inactive
// proxy vnodes for a specific proxy file system (pxvfs).
//
// The proxy file system object (pxvfs) inherits from this class,
// and is the only user of this task. This approach avoids
// memory allocations when reaping inactive proxy vnodes.
//
class pxvfs_inactive_task : public defer_task {
public:
	virtual void		execute();
	virtual void		task_done();
protected:
	// a pxvfs_inactive_task should not be created by itself.
	pxvfs_inactive_task();

	virtual pxvfs		*get_pxvfs() = 0;
};

//
// pxvfs_inactive_threadpool - This threadpool processes all of the requests
// to reap inactive proxy vnodes for individual proxy file systems.
// There is one object of this type per client node.
//
class pxvfs_inactive_threadpool : public threadpool {
public:
	static pxvfs_inactive_threadpool	&the();

	static void		startup();
	static void		shutdown();

	virtual ~pxvfs_inactive_threadpool();

private:
	pxvfs_inactive_threadpool();

	static pxvfs_inactive_threadpool	*the_pxvfs_inactive_threadpool;

	// Disallow assignments and pass by value
	pxvfs_inactive_threadpool(const pxvfs_inactive_threadpool &);
	pxvfs_inactive_threadpool &operator = (pxvfs_inactive_threadpool &);
};


//
// pxvfs_list_elem - this class provides a wrapper for _SList::ListElem
// in order to eliminate ambiguity between the multiple _SList::ListElem
// in the class pxvfs. This particular class supports the ability to
// place the proxy vfs object on a list of all proxy file systems.
//
class pxvfs_list_elem : public _SList::ListElem {
protected:
	pxvfs_list_elem(void *);
};

//
// pxvfs - this is the client side proxy for the file system "vfs" structure.
//	There is one of these objects per PXFS file system that is accessible
//	on this client node.
//
// N.B.	Because pxvfs instances map 1-1 to vfs structs, and because we rely on
//	the fact that there is at most one proxy on a given node per
//	filesystem, this design does not allow us to mount more than one
//	instance of a filesystem at a time.
//
// Note that proxy vnodes don't change the reference count on the pxvfs
// since they all share the pointer stored in the v_vfsp->vfs_data pointer.
// The reference count is to fix the race where find_pxvfs()
// returns a pointer, unmount deletes the pxvfs and then the
// (stale) pointer is attempted to be used. Also, fsmgr_client_impl
// holds a reference to the pxvfs since _unreferenced() is asynchronous.
// XXX We should change this to use VFS_HOLD()/VFS_RELE() when
// we implement forced unmount.
//
class pxvfs :
	public pxvfs_list_elem,
	public refcnt,
	public pxvfs_inactive_task
{
	friend class pxvfs_inactive_task;
public:
	//
	// Functions to support Fastwrites.
	//
	bool		fastwrite_enabled();

	uint32_t	get_fs_bsize(); // Returns filesystem block size

	PXFS_VER::blkcnt_t	reserve_blocks(PXFS_VER::blkcnt_t want,
					    bool no_redzone_wait, int &err);

	void		revoke_blocks();

	void		set_server_status(PXFS_VER::server_status_t);

	// Note: constructor is protected.

	virtual ~pxvfs();

	// This method supports the pxvfs_inactive_task
	virtual pxvfs		*get_pxvfs();

	//
	// Accessor functions.
	// Note that get_fsobj() does not do a _duplicate() and the pointer
	// returned should not be released (pxvfs retains ownership).
	// It can be used to get a pointer for doing remote invocations and
	// to compare for equivalence.
	//
	PXFS_VER::filesystem_ptr	get_fsobj() const;
	vfs_t				*get_vfsp() const;
	uint32_t			get_server_incn();

	//
	// Called by mount_client_impl when the initial mount or a remount
	// occur.
	//
	void	set_mntoptions(const char *mntoptions);

	//
	// This is called to unlink a proxy vnode from the list of all
	// proxy vnodes for this file system.
	//
	void	pxfobj_inactive(pxfobj *pxfobjp);

	// Remove a pxfobjplus from the hash table
	bool	pxfobjplus_inactive(pxfobjplus *pxfobjplusp);

	// Insert a new pxfobj into the hash table or return existing pxfobj.
	pxfobj	*fobjhash_insert(pxfobj *new_pxfobjp);

	// Add a pxfobj to the inactive list for cleanup processing
	void	add_inactivelist(pxfobjplus *pxfobjplusp);

	// function to sync all pxfs file systems.
	static int	sync_all(short flag, cred *credp);

	//
	// Calls made to the cluster framework by the NLM when handling
	// deaths/restarts of lockd/statd.  These calls arrive via calls in
	// pxfs/server/nlm_pxfs.cc, which are exported directly to the Solaris
	// NLM.
	//
	static void	set_nlm_status(int32_t nlmid,
	    PXFS_VER::nlm_status status);

	static void	remove_file_locks(int32_t sysid);

	// Returns true if the underlying filesystem is a UFS filesystem.
	bool	is_ufs();

	void	new_file_system_primary(uint32_t server_incn,
	    Environment &_environment);

	// Replay all the sleeping locks that originated from this node.
	void	replay_sleeping_locks();

	//
	// Purge all caches before unmounting/removing a PXFS file system.
	// In case of a normal unmount, return true if file system
	// is still in use (active vnodes present), and false otherwise.
	// In case of a forced unmount, return false always.
	//
	bool	purge_caches(bool forced_unmount, cred *credp);

	// This should be called when the global unmount has succeeded.
	void	unmount_succeeded();

	// This should be called when the global unmount has failed.
	void	unmount_failed();

	// This is called to clean up is the file system server crashes.
	void	cleanup();

	//
	// Routines to insert and remove sleeping lock callback objects from
	// the stored list.
	//
	void	insert_llm_cbobj(pxfs_llm_callback_impl *llmp);
	void	remove_llm_cbobj(pxfs_llm_callback_impl *llmp);

	//
	// Function to find or create proxy vnodes for this pxfs file system.
	// The vnode is return held and the caller is should call VN_RELE()
	// when finished using the vnode.
	//
	vnode	*get_pxfobj(PXFS_VER::fobj_ptr fobjp,
	    const PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info *binfop,
	    fobj_client_impl *clientp);

	//
	// Find the proxy file object for the specified FID
	//
	pxfobj	*fid_to_proxy_file(fid_t *fidp);

	//
	// VFS operations dispatched from px_vfsops.cc
	//
	// XXX: Do these methods really need to be virtual?
	// It doesn't seem like it, but we need to carefully
	// consider the procfs case before changing them.
	//
	virtual int	mountroot(enum whymountroot why);
	virtual int	unmount(int, cred *credp);
	virtual int	root(vnode **vpp);
	virtual int	statvfs(struct statvfs64 *sp);
	virtual int	sync(short flag, cred *credp);
	virtual int	vget(vnode **vpp, struct fid *fidp);
	virtual int	swapvp(vnode **vpp, char *nm);

	// Static mount method: used by "mount -g" mounts.
	static int	mount(vfs *vfsp, vnode *mvp, mounta *uap, cred *credp);

	//
	// Return the pxvfs structure for a given PXFS file system
	// object. If 'fsinfop' is not NULL and 'fsobj' does not already
	// have a proxy, then use the file system info to create a new proxy.
	// Return NULL if the proxy could not be found or created.
	// Otherwise, the pointer is returned held() and the caller should
	// call rele() when finished using the pointer.
	//
	static pxvfs	*find_pxvfs(PXFS_VER::filesystem_ptr fsobj,
	    const PXFS_VER::fs_info *fsinfop);

	// Convert a vfs structure pointer to a pxvfs object pointer.
	static pxvfs	*VFSTOPXFS(vfs_t *vfsp);

	//
	// Set the fstype field; called at boot time by Solaris.
	//
	static void	setfstype(int fstype);

	static int	startup();
	static int	shutdown();

	static void	disable_unmounts();

	//
	// Query functions to retrieve the state of the 'syncdir' and 'nocto'
	// mount options.
	//
	bool	is_syncdir_on() const;
	bool	is_nocto_on() const;
	bool	is_forcedirectio_on() const;

	// Query function for force unmount operation.
	bool is_unmounted();

	enum underlying_fs_t {
		UNKNOWN = 0,
		UFS,
		VXFS,
		HSFS
	};

	underlying_fs_t	get_underlying_fs_type();

	//
	// Function to flush out all dirty CFS attributes when fs_flush calls
	// vfs_sync(SYNC_ATTR)
	//
	static void	sync_all_attr(void *);

	// Called to flush the filesystem's dirty data.
	int sync_filesystem(cred *credp, bool revoke);

	class pxfobj_hash_bkt {
	public:
		pxfobj_list_t	hlist;		// linked list for this bucket
		os::mutex_t	hlock;		// lock for this bucket.
		uint_t 		hlist_cnt;	// Number files in this bucket

		pxfobj_hash_bkt() : hlist_cnt(0) {}
	};

	static void memory_callback(monitor::system_state_t);

	//
	// These methods are common to the node. Throttling and bandwidth
	// calculations are common for all PxFS filesytems in the node.
	// Hence the methods for doing the same are static.
	//

	// Method to implement throttling logic.
	static void update_throughput(int bytes_xfrd);

	// Allocate bandwidth from per-second qouta.
	static int wait_for_bandwidth(int bytes_needed, int &bytes_allocated);

	// helper functions to track pending ios on the server due this client
	static void increment_io_pending(bool wait_for_slot);
	static void decrement_io_pending();

protected:

	//
	// Constructor: instantiated in mount() or find_pxvfs()
	// after we have an fs.
	//
	pxvfs(PXFS_VER::filesystem_ptr fs, fsmgr_client_impl *clientmgrp,
	    const PXFS_VER::fs_info *fsinfop, int fstype, vfs_t *vfsp,
	    uint32_t server_incarn);

	//
	// Create a proxy fobj. This is to be called by get_pxfobj()
	// when a new pxfobj actually needs to be created. It is
	// declared protected and virtual so other file system
	// implementations can override the default implementation.
	//
	// Note: this is virtual so that pxprocvfs can
	// construct pxprocdir and pxprocreg proxy vnodes.
	//
	virtual pxfobj	*make_pxfobj(PXFS_VER::fobj_ptr fobjp,
	    const PXFS_VER::fobj_info &fobjinfo, fobj_client_impl *clientp);

private:
	// Clean up inactive proxy vnodes
	void		empty_inactive_list();

	//
	// Wait for the cleanup up of inactive proxy vnodes
	// Return true if file system is busy.
	//
	bool		wait_empty_inactive_list(bool forced_unmount);

	int		connect_again(vnode **vnodepp,
	    PXFS_VER::fobj_ptr fobj_p,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    fobj_client_impl *client1p,
	    PXFS_VER::fobj_client_ptr client1_p,
	    Environment &e);

	// Search the all_pxvfs list for the given file system object.
	static pxvfs	*search(PXFS_VER::filesystem_ptr fsobj);

	// Helper function to get configured node parameters.
	static int	get_configured_nodes(bool &dev_is_ha,
	    CORBA::String_out dev_name, dev_t devid,
	    sol::nodeid_seq_t_out nodes, Environment &e);

	// Return true if underlying file system type is supported by pxfs.
	static bool	supported_bdev_fs(char *name);

#ifndef VXFS_DISABLED
	// Copy in all mount specific data and fix up mounta structure.
	static int	vxfs_copyinargs(sol::mounta &ma);

	//
	// The following functions are to handle the different
	// versions of the mount argument structures used by VxFS.
	//
	static int	vxfs_copyinargs_vxfs34(sol::mounta &ma);
	static int	vxfs_copyinargs_vxfs35(sol::mounta &ma);
	static int	vxfs_copyinargs_vxfs41(sol::mounta &ma);
#endif

	// Method to create the thread that monitors throttle values.
	static int	launch_throttle_monitor_thread();

	// Throttle monitor thread entry point.
	static void	throttle_monitor_thread(void *);

	//
	// Calculate difference between timespec_t 'start' and 'end'
	// in approximate milli-seconds.
	//
	static int64_t	diff_timespec(timespec_t start, timespec_t end);

public:
	//
	// These variables make sure that only one instance of 'sync_all_attr'
	// above is running at a time.
	//
	static bool		sync_all_attr_thread_running;
	static os::mutex_t	sync_all_attr_lock;

	//
	// These variables control the flushing of attributes
	//
	static int		sync_all_attr_throttle;
	static os::usec_t	sync_all_attr_interval[];

	// Throttle for controlling filesystem sync.
	static int		sync_filesystem_throttle;

	// Per-proxy file system pxvfs statistics.
	kstat_t			*stats;

	// Per-node pxvfs statistics.
	static kstat_t		*node_stats;

	//
	// Variables to track pending ios on server due this client
	//
	// i/o requests to the server are complete only when the
	// aio_callback routine signals completion. If we don't track
	// the number of i/o requests pending in the server we can
	// overload the server's i/o queues and cause the server to
	// thrash.
	//
	// The members below keep track of pending ios due to page_out.
	// Before issuing any new asynchronous i/o request we make sure
	// that there aren't more than 'max_permitted_ios' pending.
	// Synchronous requests will always increment and decrement pending
	// count without waiting. This way synchronous requests get
	// priority over asynchronous ones.
	//
	static os::mutex_t	io_pending_lock;
	static os::condvar_t	io_pending_cv;
	static int64_t		io_pending;

	//
	// This section declares write and throughput throttling
	// variables. All of these are tunable.
	//
	// XXX: Default values for these variables should be tuned
	// automatically based on total pages available in the node.
	//
	// Async task queue length throttling is separate from throughput
	// throttling and is described and implemented in pxreg_v1.cc
	//

	//
	// 'data_rate' is the perceived transfer rate to the server in
	// bytes per second. This is re-calculated every second. On seconds
	// without activity, data rate is set to the pre-configured default
	// of 20MB/s. The lowest rate we allow by default is 2mb per
	// second.
	//
	static int64_t data_rate;
	static int64_t data_rate_minimum;
	static int64_t data_rate_default;
	static os::mutex_t data_rate_lock;

	//
	// For every page out, ie. when the page are about to be freed, we
	// add the bytes transferred in that page_out to a global variable
	// 'bytes_sent_in_second', This is reset every second. The possible
	// data rate in the next second is derived from this variable. We
	// start off with 20MB.
	//
	static int64_t bytes_sent_in_second;

	//
	// 'bytes_written_in_second' is used to identify cases when writers
	// have been throttled even when there is plenty of free memory.
	// Low throughput should not result in throttling unless we are
	// beginning to clog this node with dirty pages.
	static int64_t bytes_written_in_second;

	//
	// From 'data_rate' we know what bytes per-second is possible.  To
	// implement throttling of writers we use a static window of one
	// second and a qouta of bytes available for consumption in that
	// window. This value is set to 'data_rate' every time throttle
	// monitor thread runs.
	//
	// If a write call finds that there is not enough bytes in the
	// qouta to cover that write, then we throttle the writer by making
	// it sleep till the per second qouta is updated. After qouta
	// updation all waiting threads will be signaled. If the current
	// write size is bigger than the maximum qouta (ie. qouta of 20mb
	// and the write is 30mb), the write waits for one qouta update and
	// proceeds.
	//
	static timespec_t	window_start;
	static int64_t		bytes_in_window;

	// Lock and cv to protect bandwidth allocation variables
	static os::condvar_t	bandwidth_cv;
	static os::mutex_t	bandwidth_lock;
	static int		bandwidth_chunk;

	//
	// Frequency in microseconds at which to wakeup writers
	// waiting for bandwidth.
	//
	static int throttle_monitor_interval;

	//
	// Maximum IO queue length allowed in server. This is an artificial
	// calculation and need not reflect the real i/o queue lengths in
	// the server.
	//
	static int max_permitted_ios;

protected:
	// PXFS Server file system object reference
	PXFS_VER::filesystem_ptr	fs_fsobj;

	// Proxy file system structure
	vfs_t			*fs_vfs;

	//
	// Proxy file system root directory vnode
	//
	// XXX:	If we choose to support file systems that change their root
	//	objects on the fly, this check will have to change.
	//
	vnode_t			*fs_rootvp;

private:
	bool		fastwrite;	// Indicates whether fastwrites are
					// enabled/disabled.
	uint32_t	pxfs_bsize;	// Fundamental filesystem block size
	PXFS_VER::blkcnt_t	blocks_available;	// Number of blocks
							// reserved for this
							// client.
	os::mutex_t	blocks_reservation;
	os::condvar_t	blocks_reservation_cv;
	PXFS_VER::server_status	pxvfs_status;

	//
	// This flag indicates whether an invocation to request more storage
	// blocks is in progress. If the flag is true, threads needing blocks
	// will sleep on blocks_reservation_cv and the blocks reservation
	// mutex will be released. Those threads will be woken up when the
	// block reservation invocation returns.
	//
	bool		blk_reserve_invo_in_progress;

	//
	// Identifies file system primary incarnation.
	// Used to identify orphanned client registrations.
	// Protected by server_incn_lock
	//
	uint32_t		server_incn;
public:
	os::rwlock_t		server_incn_lock;

	// Count of async tasks queued up via all pxvfs's
	static uint64_t		async_task_count;

	// Threadpool to service async page operations on this file-sytem.
	threadpool	*mem_async_threadpool;
private:

	// Definitions for bits in 'flags' below.
	enum {
		//
		// Set when beginning the process of umounting
		// this proxy file system. The unmount may succeed or fail.
		// During this time attempts to activate files
		// will be blocked until the unmount succeeds or fails.
		//
		PXFS_UNMOUNTING	= 0x01,

		//
		// Set if unmount() succeeded globally.
		//
		PXFS_UNMOUNTED	= 0x02,

		//
		// There are file activations waiting for unmount to finish.
		//
		PXFS_FILE_ACTIVATE	= 0x04,

		//
		// set if sync(SYNC_CLOSE) is called
		//
		PXFS_SHUTDOWN	= 0x08,

		//
		// Someone wants to know when all inactive proxy vnodes
		// have been cleaned up.
		//
		PXFS_INACTIVE_WAIT	= 0x10,

		//
		// The task to clean up inactive proxy vnodes has been queued.
		//
		PXFS_TASK_QUEUED	= 0x20,

		// The unmounting is forced.
		PXFS_FORCE_UNMOUNTING = 0x40
	};

	//
	// Start of the list of fields protected by the flags_lock
	// The flags_lock cannot be held when performing a VN_RELE.
	//
	os::mutex_t	flags_lock;
	os::condvar_t	flags_cv;		// signal changes to 'flags'
	uint_t		flags;			// see enum above for values

	//
	// Number of active proxy vnodes.
	// This includes all proxy vnodes that have not been
	// deactivated, in other words destroyed.
	//
	uint_t		active_cnt;

	//
	// List of proxy vnodes awaiting possible deactivation.
	// A proxy vnode can become active again while on this list.
	// The system uses this list in order to allow a separate thread
	// to process proxy vnode deactivation and avoid any possible deadlock.
	//
	pxfobj_list_t	inactive_list;

	//
	// Count of proxy vnodes queued on the inactive list
	// plus the count of proxy vnodes that are being processed
	// for deactivation.
	//
	// It takes a while to deactivate a proxy vnode,
	// because dirty information must be flushed back to the server.
	//
	uint_t		inactive_list_cnt;

	// End of the list of fields protected by the flags_lock

	//
	// Note: we keep a pointer to our local fsmgr_client rather
	// than a CORBA pointer so we can detect when the server crashes.
	// When fsmgr_client gets _unreferenced(), it will release its
	// hold to the pxvfs.
	//
	fsmgr_client_impl	*fsmgr_client_implp;	// our fs manager

	//
	// Hash of proxy vnodes for all the pxfs file sytems on this
	// node. This is similar to the in-core inode hash table used
	// by ufs.
	//
	static pxfobj_hash_bkt	*pxfobj_hash;
	static uint_t	pxfobjhsz;	// Size of the hash table
	static uint_t	pxfobjhsz_max;	// Maximum value for pxfobjhsz
	static uint_t	pxfobjh_len;	// Desired size of each hash chain

	pxfs_llm_callback_list_t llm_cb_list;	// list of all callback objects
	os::mutex_t	llm_cb_list_lock;	// protects 'llm_cb_list'

	static pxvfs_list_t all_pxvfs;		// list of all pxfs file systems
	static os::rwlock_t all_pxvfs_lock;	// protects 'all_pxvfs'

	static int	pxfstype;		// number assigned by Solaris
	static bool	unmounts_disabled;

	// Flags that indicate mount options.
	bool	_syncdir_on;
	bool	_nocto_on;
	bool	_forcedirectio_on;

	underlying_fs_t	underlying_fs;
};

#include <pxfs/client/pxvfs_in.h>

#endif	/* _PXVFS_H */
