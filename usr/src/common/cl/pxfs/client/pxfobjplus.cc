//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfobjplus.cc	1.22	08/05/20 SMI"

#include <sys/kstat.h>
#include <sys/dnlc.h>
#include <sys/time_impl.h>
#include <sys/time.h>

#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>
#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxnode.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxfobjplus.h>
#include <pxfs/client/fobj_client_impl.h>
#include <pxfs/client/access_cache.h>

pxfobjplus::pxfobjplus(fobj_client_impl *fclientp,
    vfs *vfsp, PXFS_VER::fobj_ptr fobjp, const PXFS_VER::fobj_info &fobjinfo) :
	pxfobj(vfsp, fobjp, fobjinfo),
	inactive_list_elem(this),
	attrib_seqnum(0),
	dirty_seqnum(0),
	clientp(fclientp),
	attr_dirty(0),
	access_rights(PXFS_VER::attr_none)
{
	// Initialize attributes
	attr.va_mask = 0;

	if (clientp != NULL) {
		//
		// Must have an fobj_client in order to support caching
		//
		pxflags |= PX_CACHE_ATTRIBUTES;
	}
}

pxfobjplus::~pxfobjplus()
{
	// Check to be sure any dirty attributes have been written back.
	ASSERT(attr.va_mask == 0);
} //lint !e1540 pointers are neither freed nor zero'ed by destructor

//
// inactive - someone executed a VN_RELE when the proxy vnode
// reference count was one. Start the process of freeing the proxy vnode.
// The proxy vnode may be re-activated before we can complete the process.
//
void
pxfobjplus::inactive()
{
	//
	// This flag does not change once set,
	// and hence locking is not required.
	//
	if ((pxflags & PX_CACHE_ATTRIBUTES) == 0) {
		//
		// There is no fobj_client, and hence
		// the proxy vnode has no dirty information
		//
		ASSERT(clientp == NULL);
		pxfobj::inactive();
		return;
	}

	state_lock.lock();

	ASSERT((pxflags & PX_STALE) == 0);

	ASSERT((pxflags & PX_INACTIVELIST) == 0);
	//
	// This kind of proxy vnode may have to flush
	// information back to the server. We do that
	// with a different thread. This different
	// thread will check for possible reclaim.
	//
	pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->add_inactivelist(this);

	pxflags |= PX_INACTIVELIST;

	state_lock.unlock();
}

//
// cleanup_proxy_vnode - The proxy vnode became unused.
// All dirty information has to be flushed back to the server
// before the proxy vnode can be destroyed.
//
// Locks are not held during operations that take a long time, which are:
// 1) waiting on the inactive list for processing,
// 2) flushing information back to the server.
//
// The proxy vnode can be reclaimed until the client disconnects
// from the server.
//
void
pxfobjplus::cleanup_proxy_vnode()
{
	state_lock.lock();

	ASSERT(pxflags & PX_INACTIVELIST);
	pxflags &= ~PX_INACTIVELIST;

	vnode_t		*vnodep = get_vp();

	//
	// Fault point for testing deactivation collision
	//
	// @@@ FAULT_POINT_DEACTIVATION_COLLISION_1

	// Acquire the vnode's lock
	mutex_enter(&vnodep->v_lock);

	// Test for any references to the proxy vnode
	if (vnodep->v_count > 1) {
		//
		// The proxy vnode has been reactivated.
		// Note that inactive() will get called again
		// after the reference count drops again.
		//
		vnodep->v_count--;
		mutex_exit(&vnodep->v_lock);

		//
		// The proxy vnode could not be recycled if it
		// never was used in the first place.
		//
		ASSERT(pxflags & PX_FOBJHASH);

		state_lock.unlock();
		return;
	}
	mutex_exit(&vnodep->v_lock);

	//
	// The system sets the PX_RECYCLED flag bit when something
	// has used the vnode. We clear this flag now so that we
	// can tell whether something may have created dirty information
	// after the flush operation. The vnode reference count provides
	// only information about whether a vnode is in use at that instant
	// in time, and does not provide historical information.
	//
	pxflags &= ~PX_RECYCLED;

	// The flush can take a long time. So drop the lock.
	state_lock.unlock();

	do {
		//
		// Flush any cached dirty information to server,
		// and destroy all pages.
		// We do this in a separate thread from the one calling
		// VN_RELE() to avoid deadlocks.
		//
		become_stale();

		//
		// We must flush information back to the server prior
		// setting this flag in order to avoid deadlocks.
		//
		if (clientp != NULL) {
			clientp->set_pending_release();
		}

		//
		// Fault point for testing deactivation collision
		//
		// @@@ FAULT_POINT_DEACTIVATION_COLLISION_2

		state_lock.lock();

		// Acquire the vnode's lock
		mutex_enter(&vnodep->v_lock);

		// Test for any references to the proxy vnode
		if (vnodep->v_count > 1) {
			//
			// The proxy vnode has been reactivated.
			// Note that inactive() will get called again
			// after the reference count drops again.
			//
			vnodep->v_count--;
			mutex_exit(&vnodep->v_lock);

			//
			// The proxy vnode could not be recycled if it
			// never was used in the first place.
			//
			ASSERT(pxflags & PX_FOBJHASH);

			state_lock.unlock();

			ASSERT(clientp != NULL);
			clientp->cancel_pending_release();

			return;
		}
		mutex_exit(&vnodep->v_lock);

		if ((pxflags & PX_RECYCLED) == 0) {
			//
			// Successfully flushed all dirty information
			// to server without someone re-using the proxy vnode.
			//
			break;
		}
		pxflags &= ~PX_RECYCLED;
		state_lock.unlock();
		//
		// The proxy vnode was placed in use again temporarily.
		// There may be dirty information to flush to server.
		//
		ASSERT(clientp != NULL);
		clientp->cancel_pending_release();
	} while (true);

	clientp->set_flushed();

	//
	// Fault point for testing deactivation collision
	//
	// @@@ FAULT_POINT_DEACTIVATION_COLLISION_3

	if (!(pxvfs::VFSTOPXFS(vnodep->v_vfsp)->pxfobjplus_inactive(this))) {
		//
		// The proxy vnode was placed in use again.
		// The proxy vnode reference count has already been adjusted.
		//
		state_lock.unlock();
		ASSERT(clientp != NULL);
		clientp->cancel_pending_release();
		return;
	}
	state_lock.unlock();

	ASSERT(!vn_has_cached_data(vnodep));

	//
	// The PXFS server can send invalidations and other requests
	// until the client has disconnected. Therefore disconnect
	// from the server before executing the object destructor.
	// The proxy vnode cannot safely respond to server requests
	// once object destructor execution begins.
	//
	if (clientp != NULL) {
		clientp->rele();
	}
	delete this;
}

//
// become_stale - flush any dirty information back to the server
//
void
pxfobjplus::become_stale()
{
	(void) flush_attributes(true, true);
	access_cache::purge(&head);
}

//
// get_open_count - return the count of holds on the proxy vnode
//
// Vnode reference counting has a strange quirk.
// When "v_count == 1", a VN_RELE does not result in a decrement
// of v_count. Instead the system starts the process of deactivating
// the vnode.
//
// Should the proxy vnode only exist because the proxy vnode was in the DNLC,
// purging would start the process of deactivating the proxy vnode and would
// not adjust the reference count. So we artificially raise the reference
// count, and thus obtain normal reference counting arithmetic.
// When the request arrived at the fobj_client, we placed a hold upon
// the vnode.
//
void
pxfobjplus::get_open_count(uint32_t &open_count, Environment &)
{
	vnode_t		*vnodep = get_vp();

	dnlc_purge_vp(vnodep);
	open_count = vnodep->v_count - 1;
}

//
// get_vnode_count - return the count of holds on the proxy vnode
//
// When the request arrived at the fobj_client, we placed a hold upon
// the vnode. We adjust the count to eliminate that hold.
//
void
pxfobjplus::get_vnode_count(uint32_t &open_count, Environment &)
{
	vnode_t		*vnodep = get_vp();

	open_count = vnodep->v_count - 1;
}

//
// attr_release -
// The attribute cache must give up its attribute read token.
//
void
pxfobjplus::attr_release(uint64_t attr_seqnum,
    uint64_t access_seqnum, Environment &e)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_PXFOBJPLUS,
	    PXFS_GREEN,
	    ("pxfobj+:attr_r(%p) seq attr %llu acc %llu\n",
	    this, attr_seqnum, access_seqnum));

	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
	    PXVFS_STATS_ATTR_TOKEN_INVALS].value.ui32++));

	low_lock.wrlock();

	if (e.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_RED,
		    ("pxfobj+:attr_r(%p) orphan ndid %d\n",
		    this, e.get_src_node().ndid));
		pxfslib::throw_exception(e, EIO);
		low_lock.unlock();
		return;
	}

	//
	// An invalidation request is only valid when it comes
	// with a new attribute sequence number.
	//
	if (attrib_seqnum < attr_seqnum) {
		//
		// During the process of deactivating a file proxy,
		// the file proxy will drop read permissions, and then
		// later tell the server when disconnecting. An
		// invalidation request can arrive during the time
		// period between these two steps.
		//
		// If this file
		// is in directio mode, we could end up with the server
		// thinking we have the token, but the client thinking
		// we don't.  See BugId 4298046.
		//
		if (access_rights != PXFS_VER::attr_none) {
			ASSERT(access_rights == PXFS_VER::attr_read);
			access_rights = PXFS_VER::attr_none;
		}
		attrib_seqnum = attr_seqnum;
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_GREEN,
		    ("pxfobj+:attr_r(%p) seq %llu invalid %llu\n",
		    this, attrib_seqnum, attr_seqnum));
	}
	low_lock.unlock();

	access_cache::purge(&head, access_seqnum);
}

//
// attr_write_back_and_release -
// The attribute cache must return any modified attributes
// and give up its attribute write token.
//
void
pxfobjplus::attr_write_back_and_release(uint64_t attr_seqnum,
    uint64_t access_seqnum, sol::vattr_t &attributes, Environment &e)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_PXFOBJPLUS,
	    PXFS_GREEN,
	    ("pxfobj+:attr_w_b_r(%p) seq attr %llu acc %llu\n",
	    this, attr_seqnum, access_seqnum));

	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
	    PXVFS_STATS_ATTR_TOKEN_INVALS].value.ui32++));

	// Default to return no dirty attributes
	attributes.va_mask = 0;

	low_lock.wrlock();

	if (e.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_RED,
		    ("pxfobj+:attr_w_b_r(%p) orphan ndid %d\n",
		    this, e.get_src_node().ndid));
		pxfslib::throw_exception(e, EIO);
		low_lock.unlock();
		return;
	}

	//
	// An invalidation request is only valid when it comes
	// with a new attribute sequence number.
	//
	if (attrib_seqnum < attr_seqnum) {
		//
		// There are possible race conditions.
		// The server can grant attr write privileges,
		// and before the message reaches the client,
		// the server sends this invalidation. So the client
		// would have whatever privilege existed before this
		// set of events. We can only guarantee that the client
		// will not have more privileges than the server thinks
		// the client has.
		//
		// If in directio mode, we could end up with the server
		// thinking we have the token, but the client thinking
		// we don't.  See BugId 4298046.
		//
		if (access_rights != PXFS_VER::attr_none) {
			if (access_rights == PXFS_VER::attr_write) {
				//
				// Ship any dirty cached attributes to server
				//
				attributes = conv(attr);

				//
				// Record what attributes are dirty.
				//
				attr_dirty = attr.va_mask;
				if (attr_dirty != 0) {
					dirty_seqnum = attrib_seqnum;
				}
			} else {
				//
				// Cannot have dirty attributes when holding
				// only read privileges.
				//
				attr_dirty = 0;
				dirty_seqnum = 0;
			}
			access_rights = PXFS_VER::attr_none;
			attr.va_mask = 0;

			// Invalidate any cached extended length info
			set_ext_range((u_offset_t)0, (size_t)0);
		}
		attrib_seqnum = attr_seqnum;
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_GREEN,
		    ("pxfobj+:attr_w_b_r(%p) seq %llu invalid %llu\n",
		    this, attrib_seqnum, attr_seqnum));

		// Invalidation request is invalid
		attr_dirty = 0;
		dirty_seqnum = 0;
	}

	low_lock.unlock();

	access_cache::purge(&head, access_seqnum);
}

//
// attr_write_back_and_downgrade
// The cache must return any modified attributes
// and downgrade its attr_write token to attr_read.
//
// XXX - the client code does not adhere to the above protocol
// and instead drops the attribute token entirely.
//
void
pxfobjplus::attr_write_back_and_downgrade(uint64_t attr_seqnum,
    uint64_t access_seqnum, sol::vattr_t &attributes, Environment &e)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_PXFOBJPLUS,
	    PXFS_GREEN,
	    ("pxfobj+:attr_w_b_d(%p) seq attr %llu acc %llu\n",
	    this, attr_seqnum, access_seqnum));

	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
	    PXVFS_STATS_ATTR_TOKEN_INVALS].value.ui32++));

	// Default to return no dirty attributes
	attributes.va_mask = 0;

	low_lock.wrlock();

	if (e.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_RED,
		    ("pxfobj+:attr_w_b_d(%p) orphan ndid %d\n",
		    this, e.get_src_node().ndid));
		pxfslib::throw_exception(e, EIO);
		low_lock.unlock();
		return;
	}

	//
	// An invalidation request is only valid when it comes
	// with a new attribute sequence number.
	//
	if (attrib_seqnum < attr_seqnum) {
		//
		// There are possible race conditions.
		// For example, the client could start with attribute read
		// privileges and do a set_attributes operation, which
		// results in the server granting attr write privileges. Before
		// the reply arrives with the attr write privileges, the
		// server could send this downgrade.
		//
		// If in directio mode, we could end up with the server
		// thinking we have the token, but the client thinking
		// we don't.  See BugId 4298046.
		//
		if (access_rights != PXFS_VER::attr_none) {
			if (access_rights == PXFS_VER::attr_write) {
				//
				// Ship any dirty cached attributes to server
				//
				attributes = conv(attr);

				//
				// Record what attributes are dirty.
				//
				attr_dirty = attr.va_mask;
				if (attr_dirty != 0) {
					dirty_seqnum = attrib_seqnum;
				}
			} else {
				//
				// Cannot have dirty attributes when holding
				// only read privileges.
				//
				attr_dirty = 0;
				dirty_seqnum = 0;
			}
			//
			// XXX The server thinks that the client has
			// read priviliges when the client drops the
			// privileges to NONE. This is wrong !
			//
			// This is being done so that the client
			// will get an updated ctime the next time
			// it wants attributes. The ctime may change
			// when the attributes are written back.
			//
			access_rights = PXFS_VER::attr_none;

			attr.va_mask = 0;

			// Invalidate any cached extended length info
			set_ext_range((u_offset_t)0, (size_t)0);
		}
		attrib_seqnum = attr_seqnum;
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_GREEN,
		    ("pxfobj+:attr_w_b_d(%p) seq %llu invalid %llu\n",
		    this, attrib_seqnum, attr_seqnum));

		// Invalidation request is invalid
		attr_dirty = 0;
		dirty_seqnum = 0;
	}

	low_lock.unlock();

	access_cache::purge(&head, access_seqnum);
}

//
// Replies to invocations are not highly available and can be lost. Attributes
// that are shipped to the server in response to a downgrade request can be
// lost. Hence a copy is saved in the attr_dirty field. In case the server
// dies and the new server initiates recovery, the value in attr_dirty is
// used to recover the attributes that could have been lost in transit.
//
// This invocation from the server acknowledges the successful receipt of dirty
// attributes for an attribute downgrade request. Since the attributes have
// now been synced to disk, the attr_dirty field and the dirty_seqnum fields
// are cleared.
//
void
pxfobjplus::clear_attr_dirty(uint32_t server_incarn, Environment &_environment)
{
	//
	// A request from an obsolete server is ignored.
	//
	if (pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn()
	    == server_incarn) {
		low_lock.wrlock();
		attr_dirty = 0;
		dirty_seqnum = 0;
		low_lock.unlock();
	}
}

//
// access_inval_cache - invalidate the access cache
//
void
pxfobjplus::access_inval_cache(uint64_t access_seqnum, Environment &)
{
	access_cache::purge(&head, access_seqnum);
}

//
// install_attr - Install attributes into the attribute cache.
//
// This operation occurs before anyone can do anything with the proxy vnode.
//
void
pxfobjplus::install_attr(const sol::vattr_t &attr_,
    PXFS_VER::attr_rights rights)
{
	//
	// We don't install any attributes if "rights" is "none":
	// in this case, the "attr_" parameter is just a place-holder.
	//
	if (rights == PXFS_VER::attr_none) {
		return;
	}
	attr = conv(attr_);
	attr.va_mask = 0;
	attr_dirty = 0;
	dirty_seqnum = 0;
	access_rights = rights;
}

//
// install_cachedata_flag - the default is to not support
// the flag for directio.
//
void
pxfobjplus::install_cachedata_flag(bool)
{
}

//
// access - check access permission and cache results.
//
int
pxfobjplus::access(int mode, int flags, cred *credp)
{
	if (!(pxflags & PX_CACHE_ATTRIBUTES)) {
		return (pxfobj::access(mode, flags, credp));
	}

	int		result;
	uint64_t	seqnum;
	bool		retry_needed;
	Environment	e;
	bool		allowed;

	do {
		result = access_cache::lookup(mode, credp, this);

		if (result != ENOENT) {
			ASSERT(result == 0 || result == EACCES);
			return (result);
		}
		solobj::cred_var	credobj = solobj_impl::conv(credp);

		do {
			getfobjplus()->cache_access(mode, flags, credobj,
			    allowed, seqnum, e);
			result = pxfslib::get_err(e);

			//
			// Check for error.
			// Note: 'allowed' and 'seqnum' are not valid on error
			//
			if (result != 0) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_PXFOBJPLUS,
				    PXFS_GREEN,
				    ("pxfobj+:access(%p) exception (%llu)\n",
				    this, seqnum));

				return (result);
			}

			retry_needed = seqnum < head.access_seqnum;

			access_cache::purge(&head, seqnum);

		} while (retry_needed);

		result = allowed ? 0 : EACCES;

		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_GREEN,
		    ("pxfobj+:access(%p) result %d seqnum %llu\n",
		    this, result, seqnum));

		if (result != 0 && (mode & (mode - 1)) != 0) {
			//
			// Access failed, but the result can not be cached
			// since more that one access type was requested.
			//
			return (result);
		}
	} while (!access_cache::enter(mode, credp, this, result, seqnum,
	    &head));

	return (result);
}

int
pxfobjplus::fsync(int, struct cred *)
{
	return (sync_attr());
}

//
// sync_attr - sync the cached attributes
//
int
pxfobjplus::sync_attr()
{
	return (flush_attributes(false, false));
}

//
// flush_attributes
// Write back dirty attributes.
// Discard attributes after the write if 'discard' is set to true.
//
int
pxfobjplus::flush_attributes(bool discard, bool sync)
{
	int	error = 0;

	//
	// Periodically the system flushes attributes.
	// Most of the time there is nothing to flush.
	// Avoid grabbing locks when there is nothing to do.
	//
	// The attributes could become dirty immediately
	// following this routine. So this check is safe.
	//
	if ((access_rights != PXFS_VER::attr_write) ||
	    (!discard && (attr.va_mask == 0))) {
		//
		// No work to do
		//
		return (error);
	}

	//
	// PXFS drops and reacquires the low_lock during various protocols.
	// That is unsafe. We must acquire and hold both locks in order to
	// prevent attribute flushes in the middle of these other operations,
	// as that can lead to data loss because of improper updating
	// of the file size.
	//
	high_lock.wrlock();
	low_lock.wrlock();

	//
	// If we've lost the write token, we don't need to get it back from
	// the server because the attributes should have been written back
	// when the token was downgraded.
	//
	if (access_rights == PXFS_VER::attr_write) {
		//
		// Write back attributes.
		//
		error = write_back_attr(0, discard, sync, kcred);
	} else if (discard) {
		//
		// There are no dirty attributes to flush. Just discard the
		// attribute token.
		//
		access_rights = PXFS_VER::attr_none;
	}

	ASSERT(error != 0 || attr.va_mask == 0);

	low_lock.unlock();
	high_lock.unlock();

	return (error);
}

//
// Write back any dirty cached attributes.
//
int
pxfobjplus::write_back_attr(int attrflags, bool discard, bool sync,
    cred_t *credp)
{
	int			error = 0;
	solobj::cred_var	credobj;
	Environment		e;
	sol::vattr_t		attributes;

	ASSERT(low_lock.write_held());
	ASSERT(access_rights == PXFS_VER::attr_write);

	if (attr.va_mask != 0) {
		attributes = conv(attr);

		if (discard) {
			access_rights = PXFS_VER::attr_none;
		}

		//
		// Record what attributes are dirty.
		// The server could fail while this operation is underway.
		// This must be done so that the recovery process can find
		// these modified attributes.
		//
		attr_dirty = attr.va_mask;
		dirty_seqnum = attrib_seqnum;

		//
		// The client is forbidden from holding 'low_lock' while
		// calling the server (to avoid deadlocks).  Hence, we drop the
		// lock before we go remote.  Note that because we have dropped
		// the lock, our token may get revoked before we get to the
		// server.  We handle this case after we re-grab 'low_lock'.
		//
		low_lock.unlock();

		credobj = solobj_impl::conv(credp);
		getfobjplus()->cache_write_all_attr(attributes, attrflags,
		    discard, sync, credobj,
		    pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn(),
		    e);
		error = pxfslib::get_err(e);

		low_lock.wrlock();

		//
		// The attribute token could have been taken away while the
		// low_lock was not held. If the node still holds the attribute
		// write token the attributes coming from the server identify
		// the attributes that were read back in.
		//
		if (error == 0 && access_rights == PXFS_VER::attr_write) {
			attr = conv(attributes);
		}

		if (discard ||
		    error == 0 ||
		    error == ECANCELED ||
		    error == EIO) {
			//
			// ECANCELED means that there is a new server.
			// The new server has already read the attributes
			// and written any dirty attributes to persistent
			// storage.
			//
			// EIO can be generated by a PxFS server that is
			// disabled or forced unmounted. We intentionally
			// clear the attributes when EIO is returned.
			//
			attr_dirty = 0;
			dirty_seqnum = 0;
			attr.va_mask = 0;
		}

	} else if (discard) {
		//
		// The client node is dropping attribute privileges
		// and has no dirty information.
		//
		access_rights = PXFS_VER::attr_none;

		// The client is forbidden from holding 'low_lock' while
		// calling the server (to avoid deadlocks).  Hence, we drop the
		// lock before we go remote.
		//
		low_lock.unlock();

		getfobjplus()->cache_attr_drop_token(
		    pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn(),
		    e);
		error = pxfslib::get_err(e);

		low_lock.wrlock();

	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_PXFOBJPLUS,
	    error ? PXFS_RED : PXFS_GREEN,
	    ("pxfobj+:write_back_attr(%p) err %d discard %d\n",
	    this, error, discard));

	return (error);
}

//
// getattr - get attributes and cache them (unless caching is disabled)
//
int
pxfobjplus::getattr(vattr *vap, int flags, cred *credp)
{
	if (!is_cached()) {
		return (pxfobj::getattr(vap, flags, credp));
	}

	int	error;

	//
	// Getting read privilege also gets the attributes
	//
	error = get_attr_read_token(credp);
	if (error) {
		return (error);
	}

	*vap = attr;

	if (is_cached()) {
		drop_attr_token();
	} else {
		error = drop_token_with_no_rights(credp);
	}
	return (error);
}

//
// setattr - set the attributes
//
// This is an optimized version to logically downgrade
// all client caches to have no rights, set the attributes
// and return the write token. Note that we can't get the
// write token, update the cache locally, and call
// write_all_attr() since we aren't doing the access checks
// on the client (a cache downgrade could write back attributes
// with kernel permission instead of user's permission).
// Therefore, the calling node participates in the protocol
// by passing dirty attributes (combine downgrade with request)
// and installing new attributes and write token on successful
// return. The calling node loses the token if there is an error.
//
int
pxfobjplus::setattr(vattr *vap, int flags, cred *credp)
{
	if (!is_cached()) {
		return (pxfobj::setattr(vap, flags, credp));
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_PXFOBJPLUS,
	    PXFS_GREEN,
	    ("pxfobj+:setattr(%p) enter mask: %x, %x\n",
	    this, vap->va_mask, is_cached()));

	if (vap->va_mask & AT_NOSET) {
		return (EINVAL);
	}

	Environment		e;
	uint64_t		seqnum;
	solobj::cred_var	credobj = solobj_impl::conv(credp);

	//
	// Block write token requests from this node
	// (similar to obtain_token()).
	//
	high_lock.wrlock();

	//
	// We save a copy of the (possibly) dirty attributes and sequence
	// number and pass this to the server. The sequence number is used
	// by the server to tell if there was a downgrade while the
	// locks are not held on the client and thus prevent installing
	// stale data.
	//
	low_lock.wrlock();

	vattr	wb_vattr = attr;

	if (vap->va_mask & (AT_GID | AT_UID | AT_MODE)) {
		//
		// Invalidate the access check cache.
		//
		access_cache::purge(&head);
	}

	low_lock.unlock();

	uint32_t	server_incarn;
	getfobjplus()->cache_set_attributes(conv(wb_vattr), 0, conv(*vap),
	    flags, credobj, seqnum, server_incarn, e);
	sol::error_t	error = pxfslib::get_err(e);

	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_RED,
		    ("pxfobj+:setattr(%p) exit err: %d seqnum %llu\n",
		    this, error, seqnum));

		high_lock.unlock();
		return (error);
	}

	low_lock.wrlock();

	if (is_cached() &&
	    seqnum >= attrib_seqnum &&
	    pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn() ==
	    server_incarn) {
		access_rights = PXFS_VER::attr_write;
		attrib_seqnum = seqnum;
	} else {
		//
		// One of the following applies:
		// 1) This file is in directio mode,
		// 2) there was an early invalidation,
		// 3) this came from the previous server
		// In all cases don't cache attributes.
		//
		// XXX Note that the server thinks we have write attributes,
		// while we think we don't. This is not optimal - we need to
		// revisit this when the performance of multiple writes with
		// directio turned on becomes important enough.
		//
		access_rights = PXFS_VER::attr_none;
	}
	attr = wb_vattr;
	attr.va_mask = 0;
	attr_dirty = 0;
	dirty_seqnum = 0;

	PXFS_DBPRINTF(
	    PXFS_TRACE_PXFOBJPLUS,
	    PXFS_GREEN,
	    ("pxfobj+:setattr(%p) exit seqnum %llu\n",
	    this, seqnum));

	drop_attr_token();
	return (0);
}

bool
pxfobjplus::is_cached()
{
	return ((pxflags & PX_CACHE_ATTRIBUTES) != 0);
}

//
// Return true if this file can support caching.
//
bool
pxfobjplus::can_cache()
{
	return (true);
}

//
// This is called to obtain the attribute read token.
//
sol::error_t
pxfobjplus::get_attr_read_token(cred_t *credp)
{
	sol::error_t	error = obtain_token(PXFS_VER::attr_read, credp);

	ASSERT(error != 0 || high_lock.lock_held());

	return (error);
}
//
// This is called to obtain the attribute write token.
//
sol::error_t
pxfobjplus::get_attr_write_token(cred_t *credp)
{
	sol::error_t	error = obtain_token(PXFS_VER::attr_write, credp);

	ASSERT(error != 0 || high_lock.write_held());

	return (error);
}


//
// obtain_token
// N.B.: if we are already holding a token that has more power than we need
// (e.g. we want read and we've got the write token) we use the token as it
// stands without communicating with the provider to downgrade it.
// Acquiring the requested access to the high and low locks in this case is
// sufficient.
//
sol::error_t
pxfobjplus::obtain_token(PXFS_VER::attr_rights req_rights, cred_t *credp)
{
	bool		has_write_lock = false;

	kstat_t		*stats =  pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->stats;

	if (req_rights == PXFS_VER::attr_write) {
		high_lock.wrlock();
		low_lock.wrlock();
		has_write_lock = true;
	} else {
		high_lock.rdlock();
		low_lock.rdlock();
	}

	if (req_rights <= access_rights) {
		//
		// Already have the needed attribute permissions
		//
		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
		    PXVFS_STATS_ATTR_TOKEN_HITS].value.ui32++));
	}
	//
	// Note: it is possible that a concurrent caller to this routine
	// will change the value of access_rights while we are examining
	// them below.  However, the only possible change is to increase
	// the value of access_rights, which is benign: it's not harmful
	// to have more rights than we actually need.
	//
	// The more interesting case is a concurrent request from the
	// provider to downgrade the token.  This is potentially a problem
	// when we are trying to get the read token; in this case we hold
	// the read version of the high and low locks.  The downgrade
	// request will also only acquire the read version of the low lock,
	// so it is possible that after we have read access_rights below,
	// the downgrade routine will reduce its value.  However, this is
	// OK because the downgrade routine is going to change the value
	// from write to read, and all we require in this situation is
	// read.  A routine which attempts to revoke permission must get
	// the write version of the low lock, so it will be excluded until
	// we are done.
	//
	while (req_rights > access_rights) {
		//
		// The token we have isn't powerful enough.
		// We must get the needed token from the server.
		//
		// Upgrade lock to the write level to serialize calls to
		// the provider.  Because we are trying to acquire a read
		// token (which has blocking semantics) we don't need to
		// worry about try semantics here.
		//
		if (!has_write_lock) {
			// Release read lock.
			low_lock.unlock();
			high_lock.unlock();

			// Acquire write lock.
			high_lock.wrlock();
			low_lock.wrlock();
			has_write_lock = true;

			//
			// We dropped the read lock; before we acquired
			// the write lock someone else could have raced
			// in and changed our rights.
			//
			continue;
		}

		ASSERT(high_lock.write_held());
		ASSERT(low_lock.write_held());

		//
		// We shouldn't have modified cached attributes if we are trying
		// to get the attribute token.
		//
		ASSERT(attr.va_mask == 0);

		//
		// Call the server. The low level lock is not held across
		// the call to allow invalidation requests to complete.
		// The high level lock is held to block other threads on
		// this node.
		//
		low_lock.unlock();

		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
		    PXVFS_STATS_ATTR_TOKEN_MISSES].value.ui32++));

		//
		// We use a local variable to get the new attributes so we don't
		// clobber the cached attributes while the low_lock is not held.
		//
		Environment		e;
		vattr			tmp_attr;
		uint32_t		server_incarn;
		uint64_t		seqnum;
		solobj::cred_var	credobj = solobj_impl::conv(credp);

		getfobjplus()->cache_get_all_attr(credobj, req_rights,
		    conv(tmp_attr), seqnum, server_incarn, e);
		sol::error_t error = pxfslib::get_err(e);

		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_PXFOBJPLUS,
			    PXFS_RED,
			    ("pxfobj+:obtain_token(%p) err %d seqnum %llu\n",
			    this, error, seqnum));

			high_lock.unlock();
			return (error);
		}

		low_lock.wrlock();

		if (pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->get_server_incn() !=
		    server_incarn) {
			//
			// The attribute token did not come from the
			// current server.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_PXFOBJPLUS,
			    PXFS_AMBER,
			    ("pxfobj+:obtain_token(%p) old server seqnum "
			    "%llu %llu\n",
			    this, attrib_seqnum, seqnum));

			continue;
		}

		//
		// If there was an early invalidation, the returned results are
		// invalid and we have to try again.
		//
		if (attrib_seqnum > seqnum) {

			PXFS_DBPRINTF(
			    PXFS_TRACE_PXFOBJPLUS,
			    PXFS_GREEN,
			    ("pxfobj+:obtain_token(%p) retry seqnum "
			    "%llu %llu\n",
			    this, attrib_seqnum, seqnum));

			continue;
		}

		access_rights = req_rights;
		attr = tmp_attr;
		attr.va_mask = 0;
		attr_dirty = 0;
		dirty_seqnum = 0;
		attrib_seqnum = seqnum;

		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJPLUS,
		    PXFS_GREEN,
		    ("pxfobj+:obtain_token(%p) rights %d seq %llu size %llx\n",
		    this, access_rights, seqnum, attr.va_size));
	}

	// Downgrade locks to the requested mode.
	if (has_write_lock && req_rights == PXFS_VER::attr_read) {
		low_lock.downgrade();
		high_lock.downgrade();
	}

	return (0);
}

//
// This is very similar to drop_attr_token(), except that the client
// doesn't have any access rights at the end. This is used to
// force the client to go to the server to get new access rights,
// mainly when direct I/O is turned on.
//
int
pxfobjplus::drop_token_with_no_rights(cred_t *credp)
{
	int	error = 0;

	if (attr.va_mask != 0) {
		error = write_back_attr(0, true, false, credp);
	} else {
		//
		// There are no dirty attributes to flush. Just discard the
		// attribute token.
		//
		access_rights = PXFS_VER::attr_none;
	}

	drop_attr_token();

	return (error);
}

void
pxfobjplus::set_mtime()
{
	timespec_t lhrestime;

	ASSERT(high_lock.write_held());
	ASSERT(access_rights == PXFS_VER::attr_write);

	gethrestime(&lhrestime);
	attr.va_mtime = attr.va_ctime = lhrestime;
	attr.va_mask |= (AT_MTIME | AT_CTIME);
}

//
// Clear Set-UID & Set-GID bits on successful write if not super-user
// and at least one of the execute bits is set.  If we always clear Set-GID,
// mandatory file and record locking is unuseable.
//
int
pxfobjplus::clear_suid(cred_t *credp)
{
	// Allow root to write and not change bits.
	if (credp->cr_uid == 0) {
		return (0);
	}
	int	error = get_attr_write_token(credp);
	if (error) {
		return (error);
	}
	if (attr.va_mode & (VEXEC | (VEXEC >> 3) | (VEXEC >> 6))) {
		if (attr.va_mode & (VSUID | VSGID)) {
			attr.va_mode &= ~(VSUID | VSGID);
			attr.va_mask |= AT_MODE;
		}
	}
	drop_attr_token();
	return (0);
}

//
// This call updates cached attributes for all those fields that are modified
// when a 'bmap' invocation is made on the server.  Note that this call only
// sets the local value of these variables, the actual value is already set on
// the server side by the 'bmap' call.
//
void
pxfobjplus::update_length_fields(vattr &_attr)
{
	ASSERT(high_lock.write_held());

	//
	// va_size field in the client attributes copy is not updated
	// with the len information from the server. see bugid 4362812
	// for more info.
	//

	attr.va_nblocks = _attr.va_nblocks;

	if ((attr.va_mtime.tv_sec < _attr.va_mtime.tv_sec) ||
	    ((attr.va_mtime.tv_sec == _attr.va_mtime.tv_sec) &&
	    (attr.va_mtime.tv_nsec < _attr.va_mtime.tv_nsec))) {
		attr.va_mtime = _attr.va_mtime;
	}
	if ((attr.va_ctime.tv_sec < _attr.va_ctime.tv_sec) ||
	    ((attr.va_ctime.tv_sec == _attr.va_ctime.tv_sec) &&
	    (attr.va_ctime.tv_nsec < _attr.va_ctime.tv_nsec))) {
		attr.va_ctime = _attr.va_ctime;
	}
}

//
// set_ext_range - the default is no support for file extents.
//
void
pxfobjplus::set_ext_range(u_offset_t, size_t)
{
	ASSERT(low_lock.lock_held());
}

//
// recover_state - the new server is reconstructing state by
// asking each client for its cached state.
//
void
pxfobjplus::recover_state(PXFS_VER::recovery_info_out recovery, Environment &)
{
	PXFS_VER::recovery_info	*recoveryp = new PXFS_VER::recovery_info;
	recovery = recoveryp;

	recoveryp->client_p = clientp->recover_client_ref();

	ID_node		&node = orb_conf::current_id_node();
	recoveryp->ndid = node.ndid;
	recoveryp->incn = node.incn;

	low_lock.wrlock();

	recoveryp->at_rights = access_rights;
	recoveryp->attr_seqnum = attrib_seqnum;
	//
	// When the attributes are clean the attribute change mask will be zero
	//
	vattr	*vattrp = (vattr *)&(recoveryp->attr);
	*vattrp = attr;
	vattrp->va_mask = attr_dirty;
	recoveryp->dirty_seqnum = dirty_seqnum;

	low_lock.unlock();

	access_cache::cache_wrlock();
	recoveryp->ac_cached = (head.first_acachep != NULL);
	recoveryp->ac_seqnum = head.access_seqnum;
	access_cache::cache_unlock();
}
