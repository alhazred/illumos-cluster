//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef ACCESS_CACHE_H
#define	ACCESS_CACHE_H

#pragma ident	"@(#)access_cache.h	1.8	08/05/20 SMI"

#include <sys/cred.h>
#include <sys/types.h>

#include <sys/os.h>

#include "../version.h"

// Forward declaration
class pxfobjplus;

//
// acache_struct - contains information about the results
// for one access operation.
//
struct acache_entry {
	pxfobjplus	*pxfobjplusp;
	acache_entry	*next;
	acache_entry	*prev;
	cred_t		*credp;
	int32_t		pos_mask;
	int32_t		neg_mask;
};

//
// acache_head - supports the access information for one file.
//
class acache_head {
public:
	acache_head();
	~acache_head();

	// The sequence number for these access cache entries
	uint64_t	access_seqnum;

	// Head of list of access cache entries for this file
	acache_entry	*first_acachep;
};

//
// access_cache - manages the cached access results for all
// proxy file systems on one machine.
//
class access_cache {
public:
	static int		lookup(int mode, cred_t *credp,
				    pxfobjplus *pxfobjplusp);

	static bool		enter(int mode, cred_t *credp,
	    pxfobjplus *pxfobjplusp, int result, uint64_t new_seqnum,
	    acache_head *headp);

	static void		purge(acache_head *headp);
	static void		purge(acache_head *headp, uint64_t new_seqnum);

	// Startup is called when the pxfs module is loaded
	static int		startup();

	// Shutdown is called when the pxfs module is unloaded
	static int		shutdown();

	static void		cache_wrlock();
	static void		cache_unlock();

private:
	static void		remove(acache_head *headp,
				    acache_entry *entryp);

	static void		purge_locked(acache_head *headp,
				    uint64_t new_seqnum);

	// This points to the access cache for this node
	static acache_entry	*acache_array;

	// These two items are based on dnlc size
	static uint32_t		acachesize;
	static uint32_t		acachemask;

	// Synchronizes activity in the access cache
	static os::rwlock_t	acache_lock;
};

#include <pxfs/client/access_cache_in.h>

#endif	// ACCESS_CACHE_H
