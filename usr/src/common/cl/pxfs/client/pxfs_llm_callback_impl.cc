//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_llm_callback_impl.cc	1.6	08/05/20 SMI"

#include "../version.h"
#include <pxfs/client/pxfs_llm_callback_impl.h>

//
// Constructor.  Call the init() routine to initialize this object.
//
pxfs_llm_callback_impl::pxfs_llm_callback_impl() :
	_DList::ListElem(this)
{
	init();
}

pxfs_llm_callback_impl::~pxfs_llm_callback_impl()
{
	// Nothing to do
}

//
// (Re)initializes the value of 'error' to INITIAL
//
void
pxfs_llm_callback_impl::init()
{
	error = pxfs_llm_callback_impl::INITIAL;
}

void
pxfs_llm_callback_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}

	delete this;
}

//
// This is called when pxfobj is sleeping for a file lock to be granted.
//
int
pxfs_llm_callback_impl::wait_for_lock()
{
	//
	// Note that wakeup() can be called before we get here
	// so we might not sleep at all.
	//
	sleep_lock.lock();
	while (error == pxfs_llm_callback_impl::INITIAL) {
		//
		// Wait_sig will return zero if we are woken up due to a
		// process signal.
		//
		if (!sleep_cv.wait_sig(&sleep_lock))
			error = EINTR;
	}
	sleep_lock.unlock();

	return (error);
}

void
pxfs_llm_callback_impl::signal(sol::error_t err)
{
	//
	// Its possible that this is called before pxfobj has
	// called wait_for_lock() above and thus we will be doing a
	// cv_signal() with no thread waiting. This should be rare
	// and is harmless.
	// It is also possible to be called more than once if the
	// pxfs server fails over to a new primary.
	//
	sleep_lock.lock();
	if (error == pxfs_llm_callback_impl::INITIAL) {
		error = err;
		sleep_cv.signal();
	}
	sleep_lock.unlock();
}

void
pxfs_llm_callback_impl::wakeup(Environment &)
{
	signal(0);
}
