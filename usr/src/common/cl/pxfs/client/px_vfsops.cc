//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)px_vfsops.cc	1.11	08/05/20 SMI"

#include <sys/types.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/uio.h>
#include <sys/statvfs.h>
#include <sys/mount.h>
#include <sys/debug.h>

#include <sys/sol_version.h>
#include <sys/os.h>

#include <pxfs/common/pxfslib.h>
#include "../version.h"
#include <pxfs/client/pxfobj.h>
#include <pxfs/client/pxvfs.h>

static int px_mount(vfs_t *vfsp, vnode_t *mvp, struct mounta *uap, cred_t *cr);
static int px_mountroot(vfs_t *vfsp, enum whymountroot why);
static int px_unmount(vfs_t *vfsp, int flags, cred_t *cr);
static int px_root(vfs_t *vfsp, vnode_t **vpp);
static int px_statvfs(vfs_t *vfsp, struct statvfs64 *sp);
static int px_sync(vfs_t *vfsp, short flag, cred_t *cr);
static int px_vget(vfs_t *vfsp, vnode_t **vpp, struct fid *fidp);
static int px_swapvp(vfs_t *vfsp, vnode_t **vpp, char *nm);
static void px_freevfs(vfs_t *vfsp);

#ifdef FSI

struct vfsops *pxfs_vfsopsp = NULL;

//
// px_vfs_init - initializes the pxfs vfs data structure operations vector
// for pxfs version 0. At mount time, the system will substitute this
// vfs operations vector to replace the generic vfs operations vector
// when pxfs version 0 will support the file system.
//
int
px_vfs_init()
{
	static const fs_operation_def_t px_vfsops_template[] = {
		VFSNAME_MOUNT, (fs_generic_func_p)px_mount,
		VFSNAME_UNMOUNT, (fs_generic_func_p)px_unmount,
		VFSNAME_ROOT, (fs_generic_func_p)px_root,
		VFSNAME_STATVFS, (fs_generic_func_p)px_statvfs,
		VFSNAME_SYNC, (fs_generic_func_p)px_sync,
		VFSNAME_VGET, (fs_generic_func_p)px_vget,
		VFSNAME_MOUNTROOT, (fs_generic_func_p)px_mountroot,
		"swapvp", (fs_generic_func_p)px_swapvp,
		VFSNAME_FREEVFS, (fs_generic_func_p)px_freevfs,
		NULL, NULL
	};

	int error;

	//
	// The following function allocates memory for a vfs operations vector,
	// and initializes the data structure.
	//
	error = vfs_makefsops(px_vfsops_template, &pxfs_vfsopsp);
	if (error != 0) {
		return (error);
	}
	return (0);
}

//
// px_vfs_uninit - undoes the vfs initialization,
// which basically means destroying the vfs operations vector.
//
void
px_vfs_uninit()
{
	if (pxfs_vfsopsp != NULL) {
		vfs_freevfsops(pxfs_vfsopsp);
	}
}

#else

struct vfsops pxfs_vfsops_v1 = {
	px_mount,
	px_unmount,
	px_root,
	px_statvfs,
	px_sync,
	px_vget,
	px_mountroot,
	px_swapvp,
	px_freevfs
};

struct vfsops *pxfs_vfsopsp = &pxfs_vfsops_v1;

#endif	// FSI

static int
px_mount(vfs_t *vfsp, vnode_t *mvp, struct mounta *uap, cred_t *cr)
{
	int	error;

	ASSERT(uap->flags & MS_GLOBAL);
	//
	// "mount -g" mounts use the static form of the
	// mount method; this method allocates
	// the pxvfs after determining the fs reference.
	//
	error = pxvfs::mount(vfsp, mvp, uap, cr);
	return (error);
}

static int
px_unmount(vfs_t *vfsp, int flags, cred_t *cr)
{
	return (pxvfs::VFSTOPXFS(vfsp)->unmount(flags, cr));
}

static int
px_mountroot(vfs_t *vfsp, enum whymountroot why)
{
	return (pxvfs::VFSTOPXFS(vfsp)->mountroot(why));
}

static int
px_root(vfs_t *vfsp, vnode_t **vpp)
{
	return (pxvfs::VFSTOPXFS(vfsp)->root(vpp));
}

static int
px_statvfs(vfs_t *vfsp, struct statvfs64 *sp)
{
	return (pxvfs::VFSTOPXFS(vfsp)->statvfs(sp));
}

//
// vfsp will be non-NULL if this is a sync in order to unmount one file system.
// vfsp will be NULL if this is a sync of all PXFS type file systems.
//
static int
px_sync(vfs_t *vfsp, short flag, cred_t *cr)
{
	if (vfsp != NULL) {
		return (pxvfs::VFSTOPXFS(vfsp)->sync(flag, cr));
	} else {
		return (pxvfs::sync_all(flag, cr));
	}
}

static int
px_vget(vfs_t *vfsp, vnode_t **vpp, struct fid *fidp)
{
	return (pxvfs::VFSTOPXFS(vfsp)->vget(vpp, fidp));
}

static int
px_swapvp(vfs_t *vfsp, vnode_t **vpp, char *nm)
{
	return (pxvfs::VFSTOPXFS(vfsp)->swapvp(vpp, nm));
}

static void
px_freevfs(vfs_t *vfsp)
{
	delete pxvfs::VFSTOPXFS(vfsp);
}
