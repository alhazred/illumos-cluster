//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// mem_async.cc - implementation of client side of the file paging protocol
//

#pragma ident	"@(#)mem_async.cc	1.13	08/05/20 SMI"

#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sys/cred.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/file.h>
#include <sys/uio.h>
#include <sys/debug.h>

#include <vm/hat.h>
#include <vm/page.h>
#include <vm/pvn.h>
#include <vm/as.h>
#include <vm/seg.h>

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <solobj/solobj_impl.h>

#include <pxfs/common/pxfslib.h>
#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxreg.h>
#include <pxfs/client/mem_async.h>
#include <pxfs/client/aio_callback_impl.h>

mem_async_write::~mem_async_write()
{
	// Allow the vnode to go away
	VN_RELE(pxnode::PXTOV(m_pxregp));

	// Allow the credentials to go away
	crfree(m_credp);
} //lint !e1540 pointers are neither freed nor zero'ed by destructor

//
// execute - perform one asynchronous write
//
void
mem_async_write::execute()
{

	//
	// There is an io lock on the page to be written asynchronously.
	// Any invalidation would have to wait until this page has
	// completed the I/O operation. So there should not be
	// an invalidation until this write completes.
	//
	ASSERT(m_pxregp->seqnum == m_seqnum);

	//
	// Since there has not been an invalidation,
	// we should still have write permission.
	//
	ASSERT(m_pxregp->has_write_lock());

	solobj::cred_var	credobj = solobj_impl::conv(m_credp);
	Environment		e;
	CORBA::Exception	*ex;
	sol::error_t		error;

	size_t		pxfs_clustsz = (size_t)(pxreg::pxfs_clustsz_nvalue *
			    PAGESIZE);

	u_offset_t	vpoff = m_pagep->p_offset / pxfs_clustsz * pxfs_clustsz;
	size_t		vplen = pxfs_clustsz;

	// Only used for a PxFS filesystem mounted without syncdir.
	sol::size_t	client_file_size =
	    (sol::size_t)m_pxregp->get_file_length();

	m_flags &= ~B_ASYNC;

	bulkio::in_pages_var	pglobj = bulkio_impl::conv_in(m_pagep, m_off,
				    (uint_t)m_len, m_flags | B_WRITE);

	aio_callback_impl	*aiocbk = new aio_callback_impl(
	    pxnode::PXTOV(m_pxregp), m_pagep, m_off, m_len, m_flags, pglobj);

	pxfs_aio::aio_callback_var	aiocbkobj = aiocbk->get_objref();

	PXFS_DBPRINTF(
	    PXFS_TRACE_MEM_ASYNC,
	    PXFS_GREEN,
	    ("async_write(%p) off %llx len %x fsize %x\n",
	    m_pxregp, m_off, m_len, client_file_size));

	//
	// m_client_file_size is useful only for a PxFS filesystem without
	// syncdir.
	//
	m_pxregp->getfile()->async_page_out(aiocbkobj,
	    (sol::u_offset_t)m_off,
	    (sol::size_t)m_len, (sol::size_t)client_file_size,
	    m_flags | B_ASYNC | B_WRITE, pglobj, credobj, m_timestamp,
	    m_pxregp->size_in_sync, e);

	//
	// If the server dies before page_out() invocation completes
	// (COMM_FAILURE) then there will not be a callback and the unref
	// processing of aiocbk will clean up the pages.
	//
	if ((ex = e.exception()) != NULL) {
		if (ex->is_system_exception()) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_MEM_ASYNC,
			    PXFS_RED,
			    ("async_write(%p) system exception\n",
			    m_pxregp));
			e.clear();
			m_pxregp->data_token_lock.unlock();
			delete this;
			return;
		}
	}
	//
	// Page cleanup is done now. If we get a callback anyway, the
	// callback processing will have no cleanup to do.
	//
	error = pxfslib::get_err(e);
	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_MEM_ASYNC,
		    PXFS_RED,
		    ("async_write(%p) async_page_out error = %d\n",
		    m_pxregp, error));
		e.clear();

		//
		// Wait for PpBuf to get destructed before
		// doing a pvn_write_done().
		//
		bulkio_impl::wait_on_sema(pglobj);

		page_t	*plist = aiocbk->get_pages();

		if ((pxvfs::VFSTOPXFS(m_pxregp->get_vp()->v_vfsp))->
		    is_unmounted()) {
			aio_callback_impl::try_invalidate_pages(plist, m_flags);
		} else {
			pvn_write_done(plist, B_ERROR | m_flags);
		}
		aiocbk->signal_invo_complete(WITH_EXCEPTION);
	} else {
		aiocbk->signal_invo_complete(NO_EXCEPTION);
	}

	//
	// The data token lock was taken in putapage before this mem_async_write
	// object was created to process this async write.
	//
	m_pxregp->data_token_lock.unlock();

	// decrement outstanding async task count.
	pxvfs *pxvfsp = pxvfs::VFSTOPXFS(m_pxregp->get_vp()->v_vfsp);
	os::atomic_add_64(&(pxvfsp->async_task_count), -1);

	delete this;
}

mem_async_read::~mem_async_read()
{
	// Allow the vnode to go away
	VN_RELE(pxnode::PXTOV(m_pxregp));

	// Allow the credentials to go away
	crfree(m_credp);
} //lint !e1540 pointers are neither freed nor zero'ed by destructor

//
// execute - process one asynchronous read from the client node.
//
void
mem_async_read::execute()
{
	//
	// There is an io lock on the page to be read asynchronously.
	// Any invalidation would have to wait until this page has
	// completed the I/O operation. So there should not be
	// an invalidation until this read completes.
	//
	ASSERT(m_pxregp->seqnum == m_seqnum);

	//
	// Since there has not been an invalidation,
	// we should still have write permission.
	//
	ASSERT(m_pxregp->has_read_lock());

	// We should not attempt to read past end-of-file
	ASSERT(m_off + (u_offset_t)m_len <= m_pxregp->attr.va_size);

	solobj::cred_var	credobj = solobj_impl::conv(m_credp);
	Environment		e;

	bulkio::in_aio_pages_var	pglobj = bulkio_impl::aconv_in_pgs(
	    m_pp, m_off, (uint_t)m_len, B_READ);

	aio_callback_impl	*aiocbk = new aio_callback_impl(
	    pxnode::PXTOV(m_pxregp), m_pp, m_off, m_len, pglobj);

	pxfs_aio::aio_callback_var	aiocbkobj = aiocbk->get_objref();

	PXFS_DBPRINTF(
	    PXFS_TRACE_MEM_ASYNC,
	    PXFS_GREEN,
	    ("async_read(%p) offset %llx len %x\n",
	    m_pxregp, m_off, m_len));

	m_pxregp->getfile()->async_page_in(aiocbkobj, (sol::u_offset_t)m_off,
	    (sol::size_t)m_len, PXFS_VER::acc_ro, pglobj, credobj, e);

	if (e.exception()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_MEM_ASYNC,
		    PXFS_RED,
		    ("async_read(%p) err %d pagep %p\n",
		    m_pxregp, pxfslib::get_err(e), m_pp));

		aiocbk->signal_invo_complete(WITH_EXCEPTION);

	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_MEM_ASYNC,
		    PXFS_GREEN,
		    ("async_read(%p) after readahead cur_off %llx\n",
		    m_pxregp, (u_offset_t)m_pxregp->current_off));

		aiocbk->signal_invo_complete(NO_EXCEPTION);
	}

	//
	// The data token lock was taken in pxreg::get_asyncpage before this
	// mem_async_read object was created to process this async read.
	//
	m_pxregp->data_token_lock.unlock();

	//
	// If the server crashes before the request completes,
	// we get an unreferenced on the callback object.
	// This can be used to do a pvn_read_done() with B_ERROR.
	//
	e.clear();

	// decrement outstanding async task count.
	pxvfs *pxvfsp = pxvfs::VFSTOPXFS(m_pxregp->get_vp()->v_vfsp);
	os::atomic_add_64(&(pxvfsp->async_task_count), -1);

	delete this;
}
