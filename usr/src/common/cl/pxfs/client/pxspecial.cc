//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pxspecial.cc - proxy vnode for a device special file (e.g., "/dev/console").
//

#pragma ident	"@(#)pxspecial.cc	1.11	08/05/20 SMI"

#include <sys/types.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/uio.h>
#include <sys/debug.h>
#include <sys/sysmacros.h>
#include <sys/fs/snode.h>

#include <h/dc.h>
#include <sys/sol_conv.h>
#include <sys/sol_version.h>
#include <solobj/solobj_impl.h>

#include <pxfs/common/pxfslib.h>
#include <pxfs/device/device_service_mgr.h>
#include "../version.h"
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/client/pxspecial.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxchr.h>

#if SOL_VERSION >= __s10
#ifndef	BUG_4954775
#define	BUG_4954775
#endif
#endif

pxspecial::pxspecial(vfs *vfsp, PXFS_VER::special_ptr spp,
    const PXFS_VER::fobj_info &fobjinfo) :
	pxfobj(vfsp, spp, fobjinfo)
{
}

pxspecial::~pxspecial()
{
}

//
// Special file open:
// We consult DCS to open the device on the right node and create a new
// fobj for it.
// Note that (*vpp) is a pointer to this object on entry and is replaced
// with a pointer to the vnode for the device when we return.
//
int
pxspecial::open(vnode **vpp, int flag, cred *cr)
{
	dev_t	dev = (*vpp)->v_rdev;
	major_t maj = getmajor(dev);
	int	error;
	bool	driver_installed = true;
	vnode_t	*vnodep = get_vp();

	ASSERT(vnodep->v_type == VFIFO || vnodep->v_type == VCHR ||
	    vnodep->v_type == VBLK);

	if (vnodep->v_type != VFIFO) {
		//
		// Some one is trying to open the device. Although this happens
		// ultimately in underlying specfs, we do it here to
		// intercept streams devices and return specfs vnode.
		// This helps in mounting /devices as pxfs file system
		// and test.
		// We need to do a ddi_hold_installed_driver() because
		// we cannot know whether the driver is a streams driver
		// or not before it is installed.
		// XXX May be removed before product - not sure
		// The installation of the driver may fail on this
		// node, in which case we should not fail the open,
		// but it should be forwarded to the server to decide
		// what needs to be done. For h/w devices such as tape
		// attach on a node where there are no tapes may fail,
		// but people may try to access the device on a different node.
		//
		if (maj >= (major_t)devcnt ||
		    ddi_hold_installed_driver(maj) == NULL) {
			driver_installed = false;
		}
	}

	if (vnodep->v_type == VFIFO || (driver_installed && STREAMSTAB(maj))) {
		vnode_t		*svp = specvp(*vpp, dev, vnodep->v_type, cr);
		if (svp == NULL) {
			return (ENXIO);
		}
		VN_RELE(*vpp);
		*vpp = svp;
#if	SOL_VERSION >= __s11
		error = VOP_OPEN(vpp, flag, cr, NULL);
#else
		error = VOP_OPEN(vpp, flag, cr);
#endif
		return (error);
	}

	//
	// Contact the DCS to resolve the device to the proper
	// device server.
	//
	bool is_ha;
retry:
	mdc::device_server_var devserver = dcs_resolve(dev, is_ha);
	if (CORBA::is_nil(devserver)) {
		// This could be a service with a local device class.
		if (!is_ha) {
			vnode_t *svp = specvp(*vpp, (*vpp)->v_rdev,
			    (*vpp)->v_type, cr);
			if (svp == NULL) {
				return (ENXIO);
			}
			VN_RELE(*vpp);
			*vpp = svp;
#if	SOL_VERSION >= __s11
			return (VOP_OPEN(vpp, flag, cr, NULL));
#else
			return (VOP_OPEN(vpp, flag, cr));
#endif
		}
		return (ENXIO);
	}

	//
	// Pass the special file object (realvp) to the device server
	// and it will return a device (specfs) fobj object.
	//
	pxvfs			*pxvfsp = pxvfs::VFSTOPXFS(vnodep->v_vfsp);
	PXFS_VER::pvnode	pvnode;
	pvnode.fileobj = PXFS_VER::fobj::_duplicate(getfobj());
	pvnode.vfs = PXFS_VER::filesystem::_duplicate(pxvfsp->get_fsobj());
	pxfs_misc::init_fobjinfo(pvnode.fobjinfo, PXFS_VER::fobj_special, *vpp,
	    ((pxfobj *)pxnode::VTOPX(*vpp))->get_fidp());

	PXFS_VER::fobj_var	specfobj;
	PXFS_VER::fobj_info	fobjinfo;
	solobj::cred_var	credobj = solobj_impl::conv(cr);
	Environment		e;
	CORBA::Exception	*ex;

	// Select the version for the device service
	version_manager::vp_version_t   device_version;
	(void) pxfslib::get_running_version("px_device", &device_version);

	if (device_version.major_num >= 3) {
		//
		// Use version 3
		// The device server constructs the object, opens it,
		// and returns it to us.
		//
		devserver->get_open_device_fobj_v2(pvnode, flag, credobj,
		    specfobj, fobjinfo, e);

		if ((ex = e.exception()) != NULL) {
			if ((mdc::ds_shutdown::_exnarrow(ex)) != NULL) {
				//
				// We got ds_shutdown exception. This means
				// that the device server is in the middle
				// of shutting down, and needs to be restarted
				// by the DCS for this open to succeed.
				// Sleep for 2 seconds to wait for the shutdown
				// to complete and try again.
				//
				e.clear();
				os::usecsleep((os::usec_t)2000000);
				goto retry;
			} else {
				//
				// Error in the underlying device open, return
				// the error value to the application trying to
				// access the device.
				//
				error = pxfslib::get_err(e);
				e.clear();
				return (error);
			}
		}

	} else {
		//
		// Use version 2
		// The device server constructs the object, opens it,
		// and returns it to us.
		//
		devserver->get_open_device_fobj_v1(pvnode, flag, credobj,
		    specfobj, fobjinfo, e);
		if (e.exception()) {
			error = pxfslib::get_err(e);
			if (error != EAGAIN) {
				return (error);
			}
			//
			// We got EAGAIN.  This means that the device server
			// is in the middle of shutting down, and needs to be
			// restarted by the DCS for this open to succeed.
			// Sleep for 2 seconds to wait for the shutdown to
			// complete and try again.
			//
			error = 0;
			os::usecsleep((os::usec_t)2000000);
			goto retry;
		}
	}

	//
	// This pxspecial object is the realvp for "specfobj".
	// Store it as such, and use it to set the real vp (using set_realvp())
	// of the proxy object created below.
	// NOTE: The VN_HOLD() on (*vpp) will be released when the new
	// vnode is released (i.e., we are transfering the hold via
	// the set_realvp() call below).
	//
	// Construct the proxy for device object.
	//
	*vpp = pxvfsp->get_pxfobj(specfobj, fobjinfo, NULL, NULL);
	if (*vpp == NULL) {
		// File system was force unmounted.
		ASSERT(pxvfsp->is_unmounted());
		return (EIO);
	}

	// XXX Should be a virtual function call instead of cast.
	switch (vnodep->v_type) {
	case VCHR:
		((pxchr *)(pxnode::VTOPX(*vpp)))->set_realvp(get_vp(), is_ha);
		break;

	case VBLK:
		// XXX In the future we might have a pxblk instead.
		((pxchr *)(pxnode::VTOPX(*vpp)))->set_realvp(get_vp(), is_ha);
		break;

	case VNON:
	case VREG:
	case VDIR:
	case VLNK:
	case VFIFO:
	case VDOOR:
	case VPROC:
	case VSOCK:
	case VBAD:
#ifdef BUG_4954775
	case VPORT:
#endif
	default:
		ASSERT(0);
	}
	return (0);
}

int
pxspecial::fsync(int syncflag, struct cred *cr)
{
	solobj::cred_var	cred_obj = solobj_impl::conv(cr);
	Environment		e;

	get_special()->fsync(syncflag, cred_obj, e);
	return (pxfslib::get_err(e));
}
