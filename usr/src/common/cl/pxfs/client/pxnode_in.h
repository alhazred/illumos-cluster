//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxnode_in.h	1.5	08/05/20 SMI"

#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

inline
pxnode::pxnode(vfs_t *vfsp, enum vtype vnode_type, dev_t vnode_rdev,
    uint_t vnode_flag) :
	pxnode_abstract(vfsp, vnode_type, vnode_rdev, vnode_flag, px_vnodeopsp)
{
}

// Convert vnode to pxnode
// static
inline pxnode *
pxnode::VTOPX(vnode_t *vp)
{
	return ((pxnode *)vp->v_data);
}


#ifdef FSI

// Convert pxnode to vnode
// static
inline vnode_t *
pxnode::PXTOV(pxnode *pxp)
{
	return (pxp->get_vp());
}

#else	// FSI not defined

// Convert pxnode to vnode
// static
inline vnode_t *
pxnode::PXTOV(pxnode *pxp)
{
	return ((vnode_t *)pxp);
}

#endif	// FSI
