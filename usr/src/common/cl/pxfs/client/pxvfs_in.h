//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxvfs_in.h	1.8	08/05/20 SMI"

//
// class pxvfs_inactive_task inline methods
//

//
// Constructor - declared this function just so
// that we could make it unavailable to the general world.
//
inline
pxvfs_inactive_task::pxvfs_inactive_task()
{
}

//
// class pxvfs_inactive_threadpool inline methods
//

// static
inline pxvfs_inactive_threadpool &
pxvfs_inactive_threadpool::the()
{
	ASSERT(the_pxvfs_inactive_threadpool != NULL);
	return (*the_pxvfs_inactive_threadpool);
}

//
// class pxvfs_list_elem inline methods
//

inline
pxvfs_list_elem::pxvfs_list_elem(void *elemp) :
	_SList::ListElem(elemp)
{
}

//
// class pxvfs inline methods
//

inline PXFS_VER::filesystem_ptr
pxvfs::get_fsobj() const
{
	return (fs_fsobj);
}

inline vfs_t *
pxvfs::get_vfsp() const
{
	return (fs_vfs);
}

inline uint32_t
pxvfs::get_server_incn()
{
	return (server_incn);
}

// static
inline void
pxvfs::setfstype(int fstype)
{
	pxfstype = fstype;
}

inline bool
pxvfs::is_syncdir_on() const
{
	return (_syncdir_on);
}

inline bool
pxvfs::is_nocto_on() const
{
	return (_nocto_on);
}

inline bool
pxvfs::is_forcedirectio_on() const
{
	return (_forcedirectio_on);
}

//
// Returns true if the underlying filesystem is a UFS filesystem;
// false if not.
//
inline bool
pxvfs::is_ufs()
{
	return (underlying_fs == UFS ? true : false);
}

inline pxvfs::underlying_fs_t
pxvfs::get_underlying_fs_type()
{
	return (underlying_fs);
}

//
// Convert a vfs structure pointer to a pxvfs object pointer.
//
// static
inline pxvfs *
pxvfs::VFSTOPXFS(vfs_t *vfsp)
{
	ASSERT(vfsp->vfs_op == pxfs_vfsopsp);
	return ((pxvfs *)vfsp->vfs_data);
}

//
// Tells whether fastwrites is enabled.
//
inline bool
pxvfs::fastwrite_enabled()
{
	return (fastwrite);
}

inline uint32_t
pxvfs::get_fs_bsize()
{
	return (pxfs_bsize);
}

inline void
pxvfs::revoke_blocks()
{
	blocks_reservation.lock();
	blocks_available = 0;
	pxvfs_status = PXFS_VER::SWITCH_TO_REDZONE;
	blocks_reservation.unlock();
}

//
// Calculate difference between timespec_t s start and end in
// approximate milli-seconds. We use multiples of two to make this
// faster. The loss of accuracy is tolerable as this routine is only
// used in throttling.
//
inline int64_t
pxvfs::diff_timespec(timespec_t start, timespec_t end)
{
	return ((int64_t)(end.tv_sec * 1024 + end.tv_nsec /(1024 * 1024)) -
			(start.tv_sec * 1024 + start.tv_nsec/(1024 * 1024)));
}

//
// Increment pending IO count for the node. Wait for a slot to be
// available if needed.
//
inline void
pxvfs::increment_io_pending(bool wait_for_slot)
{
	io_pending_lock.lock();

	//
	// If this i/o request needs a free slot, wait until it is
	// available and increment io count, otherwise increment
	// without waiting.
	//
	if (wait_for_slot && io_pending >= max_permitted_ios) {
		io_pending_cv.wait(&io_pending_lock);
	}

	ASSERT(io_pending >= 0);
	ASSERT(io_pending != INT_MAX);

	io_pending++;
	io_pending_lock.unlock();
}

//
// Decrement IO count for this node and wakeup any threads waiting for
// i/o slots in the server.
//
inline void
pxvfs::decrement_io_pending()
{
	io_pending_lock.lock();

	ASSERT(io_pending > 0);

	io_pending--;
	io_pending_cv.signal();
	io_pending_lock.unlock();
}
