//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mem_async_in.h	1.7	08/05/20 SMI"

// Used by getapage.
inline
mem_async_read::mem_async_read(pxreg *pxregp, page_t *pp, u_offset_t off,
    size_t len, uint_t seqnum, cred_t *credp) :
	m_pxregp(pxregp),
	m_credp(credp),
	m_pp(pp),
	m_off(off),
	m_len(len),
	m_seqnum(seqnum)
{
	// Keep vnode around till I/O completes
	ASSERT(!m_pxregp->is_stale());
	ASSERT(m_pxregp->get_vp()->v_count != 0);
	VN_HOLD(pxnode::PXTOV(m_pxregp));

	// Keep credentials around till I/O completes
	crhold(m_credp);
}

// Used by putapage.
inline
mem_async_write::mem_async_write(pxreg *pxregp, page_t *pagep,
    u_offset_t io_off, size_t io_len, int flags, uint_t seqnum,
    cred_t *credp, os::hrtime_t timestamp) :
	m_pxregp(pxregp),
	m_credp(credp),
	m_pagep(pagep),
	m_off(io_off),
	m_len(io_len),
	m_flags(flags | B_WRITE),
	m_seqnum(seqnum),
	m_timestamp(timestamp)
{
	// Keep vnode around till I/O completes
	ASSERT(!m_pxregp->is_stale());
	ASSERT(m_pxregp->get_vp()->v_count != 0);
	VN_HOLD(pxnode::PXTOV(m_pxregp));

	// Keep credentials around till I/O completes
	crhold(m_credp);
}
