//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef PXDIR_H
#define	PXDIR_H

#pragma ident	"@(#)pxdir.h	1.8	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/vfs.h>
#include <sys/cred.h>

#include <sys/os.h>
#include <orb/invo/common.h>
#include <sys/sol_version.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/client/pxfobjplus.h>

#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

//
// pxdir - proxy directory vnode (type VDIR)
//
// Cache lookup information in the Directory Name Lookup Cache (DNLC).
//
class pxdir : public pxfobjplus {
public:
	pxdir(fobj_client_impl *fclientp,
	    vfs *vfsp, PXFS_VER::unixdir_ptr udp,
	    const PXFS_VER::fobj_info &fobjinfo);

	virtual	~pxdir();

	// get unixdir object
	PXFS_VER::unixdir_ptr		getunixdir();

	//
	// vnode operations
	//
	virtual int	lookup(char *nm, vnode **vpp, pathname *pnp, int flags,
			    vnode *rdir, cred *credp);

	virtual int	create(char *nm, vattr *vap, vcexcl excl, int mode,
			    vnode **vpp, cred *credp, int flag);

	virtual int	remove(char *nm, cred *credp);

	virtual int	link(vnode *svp, char *tnm, cred *credp);

	virtual int	mkdir(char *nm, vattr *vap, vnode **vpp, cred *credp);

	virtual int	rmdir(char *nm, vnode *cdir, cred *credp);

	virtual int	readdir(uio *uiop, cred *credp, int &eof);

	virtual int	symlink(char *nm, vattr *vap, char *target,
			    cred *credp);

	virtual int	rename(char *oldnm, vnode *newdvp, char *newnm,
			    cred *credp);

	virtual int	read(uio *uiop, int ioflag, cred *credp);

	virtual int	write(uio *uiop, int ioflag, cred *credp);

	// End vnode operations

	// Methods supporting IDL operations for directory cache management

	void	dir_inval_entry(const char *nm,
	    Environment &_environment);

	void	dir_inval_all(Environment &_environment);

	// End methods supporting IDL operations

	// Startup is called after the module is loaded.
	static int	startup();

	// Shutdown is called before the module is unloaded.
	static int	shutdown();

	virtual void	recover_state(PXFS_VER::recovery_info_out recovery,
	    Environment &env);

private:
	//
	// Definitions for "op" parameter of lc_enter(). This parameter
	// specifies the type of operation to perform following a dnlc
	// update. By performing the operation after the server invocation
	// returns, an extra invocation by the server to the calling client
	// is eliminated.
	//
	// XXX - Ordering is not guaranteed outside of server context,
	// and further scrutiny of this type of operation may be required.
	//
	static const uint_t NONE  = 0x0;	// no operation
	static const uint_t DIR   = 0x1;	// clear cached directory offset
	static const uint_t INVAL = 0x2;	// force an invalidation
	static const uint_t ALL	= (DIR|INVAL);	// perform both operations

	//
	// Functions for manipulating the Directory Name Lookup Cache (DNLC).
	// PXFS enters files in the local DNLC regardless of whether
	// the file system containing the file is remote or local.
	//
	bool	lc_lookup(char *nm, vnode_t **vpp, uint_t &orig_seqnum);
	void	lc_enter(char *nm, vnode_t *vp, uint_t orig_seqnum, uint_t op);
	uint_t	lc_remove(char *nm);
	void	lc_check_remove(char *nm, uint_t orig_seqnum);

	uint32_t	current_seqnum();

	int		connect_again(char *namep, vnode **vnodepp,
	    uint32_t orig_seqnum, PXFS_VER::fobj_ptr fobj_p,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    fobj_client_impl *client1p,
	    PXFS_VER::fobj_client_ptr client1_p,
	    uint_t flush,
	    Environment &e);

	// Disallowed operations on pxdir
	pxdir &operator = (const pxdir &);
	pxdir(const pxdir &);

	// Protects access to data content
	os::rwlock_t	data_lock;

	// Invalidation sequence number
	uint32_t	inval_seqnum;

	// New entry sequence number
	uint32_t	entry_seqnum;

	//
	// Directory file end of information offset.
	// Valid only when value > 0
	//
	offset_t	end_off;

	// Dummy entry for negative name lookups
	static vnode_t	*noentp;

};

#include <pxfs/client/pxdir_in.h>

#endif	// PXDIR_H
