//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfobj_in.h	1.8	08/05/20 SMI"

inline PXFS_VER::fobj_ptr
pxfobj::getfobj() const
{
	return (fobj_obj);
}

inline const fid_t *
pxfobj::get_fidp() const
{
	return (fidp);
}

inline ushort_t
pxfobj::get_pxflags()
{
	return (pxflags);
}

inline void
pxfobj::set_inhashtable()
{
	//
	// Use locking because this operation can collide with others.
	//
	state_lock.lock();
	pxflags |= PX_FOBJHASH;
	state_lock.unlock();
}

inline void
pxfobj::not_inhashtable()
{
	//
	// Locking is not required because this is only done after
	// all references to this pxfobj are gone and nobody can get
	// a reference.
	//
	pxflags &= ~PX_FOBJHASH;
}

inline bool
pxfobj::is_inhashtable() const
{
	return ((pxflags & PX_FOBJHASH) != 0);
}

inline bool
pxfobj::is_inactive() const
{
	return ((pxflags & PX_INACTIVELIST) != 0);
}

inline void
pxfobj::set_stale()
{
	//
	// Locking is not required because this is only done after
	// all references to this pxfobj are gone and nobody can get
	// a reference.
	//
	pxflags |= PX_STALE;
}

inline bool
pxfobj::is_stale() const
{
	return ((pxflags & PX_STALE) != 0);
}
