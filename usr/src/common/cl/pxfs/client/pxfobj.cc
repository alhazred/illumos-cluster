//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfobj.cc	1.16	08/05/20 SMI"

#include <sys/file.h>
#include <sys/flock.h>
#include <sys/fcntl.h>
#include <sys/filio.h>
#include <sys/fs_subr.h>
#include <sys/fs/ufs_filio.h>

#include <sys/sol_version.h>
#include <sys/os.h>
#include <sys/sol_conv.h>
#include <orb/infrastructure/addrspace_impl.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxfobj.h>
#include <pxfs/client/pxdir.h>
#include <pxfs/client/pxchr.h>
#include <pxfs/client/pxreg.h>
#include <pxfs/client/pxfs_llm_callback_impl.h>
#include <pxfs/server/fs_impl.h>

#if SOL_VERSION >= __s9
#define	PSARC_2000_007
#endif

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//lint -e666
// PXFS makes extensive use of the inline function get_vp() within
// the vnode macros.
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

#ifdef DEBUG
uint32_t	pxfs_getsecattr_count = 0;
#endif


pxfobj::pxfobj(vfs *vfsp, PXFS_VER::fobj_ptr fobjp,
    const PXFS_VER::fobj_info &fobjinfo) :
	pxnode(vfsp, conv(fobjinfo.vtype), fobjinfo.vrdev, fobjinfo.vflag),
	_DList::ListElem(this),
	pxflags(0)
{
	fobj_obj = PXFS_VER::fobj::_duplicate(fobjp);

	set_fid(fobjinfo.fid.fobjid_len,
	    (const char *)fobjinfo.fid.fobjid_data);

	VFS_HOLD(vfsp);
}

pxfobj::~pxfobj()
{
	if (fidp != NULL) {
		kmem_free(fidp, fidp->fid_len + sizeof (ushort_t));
	}
	ASSERT(get_vp()->v_count == 1);
	ASSERT(!vn_has_cached_data(get_vp()));
	ASSERT(pxflags & PX_STALE);
	ASSERT((pxflags & PX_FOBJHASH) == 0);
	ASSERT((pxflags & PX_INACTIVELIST) == 0);
	CORBA::release(fobj_obj);
	VFS_RELE((pxnode::PXTOV(this))->v_vfsp);
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// inactive - someone executed a VN_RELE when the proxy vnode
// reference count was one. It may be appropriate to free the proxy vnode.
//
void
pxfobj::inactive()
{
	pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->pxfobj_inactive(this);
}

void
pxfobj::cleanup_proxy_vnode()
{
	ASSERT(0);
}

void
pxfobj::become_stale()
{
	ASSERT(0);
}

bool
pxfobj::is_cached()
{
	return (false);
}

//
// can_cache - is a virtual function that identifies
// whether the proxy file object can support caching.
// This is the default function, which
// returns false indicating that the proxy file cannot support caching.
//
bool
pxfobj::can_cache()
{
	return (false);
}

//
// Return the file ID.
// Note: the caller is responsible for setting the ufidp->fid_len
// to the size of ufidp->fid_data array before calling us.
//
int
pxfobj::vop_fid(struct fid *ufidp)
{
	//
	// We initialized the fidp in the pxfobj constructor, so it
	// must be non-NULL.
	//
	ASSERT(fidp != NULL);
	uint_t dstlen = ufidp->fid_len;
	uint_t srclen = fidp->fid_len;

	if (dstlen < srclen) {
		//
		// Return the size of the fid, so that they can rety
		// with a correctly sized buffer.
		//
		ufidp->fid_len = fidp->fid_len;
		return (ENOSPC);
	}

	bcopy(fidp->fid_data, ufidp->fid_data, (size_t)srclen);
	ufidp->fid_len = fidp->fid_len;

	return (0);
}


//
// This is called from pxfobj constructor
//
void
pxfobj::set_fid(uint_t fidlen, const char *fiddata)
{
	fidp = (fid_t *)kmem_alloc(fidlen + sizeof (ushort_t), KM_SLEEP);
	bcopy(fiddata, fidp->fid_data, (size_t)fidlen);
	fidp->fid_len = (ushort_t)fidlen;
}

int
pxfobj::open(vnode **, int, cred *)
{
	return (0);
}

int
pxfobj::close(int, int, offset_t, cred *cr)
{
	int error = 0;
	Environment e;
	solobj::cred_var	credobj = solobj_impl::conv(cr);

	//
	// When a process exits or a file is explicitly closed, any locks
	// held by the process must be released, to be consistent with
	// Unix semantics.
	// The problem is that the pid, which is used to identify the owners
	// of file and share locks, is not globally unique across the cluster.
	// We must use the node id of the node where the process requesting
	// the lock resides.
	// We need to pass the pid and node id because that
	// information is not available on the server.
	//
	state_lock.lock();
	if (pxflags & PX_HAS_LOCKS) {
		state_lock.unlock();
		fobj_obj->remove_locks((sol::pid_t)(ttoproc(curthread)->p_pid),
		    pxfslib::set_nodeid(0, orb_conf::node_number()),
		    credobj, e);
		error = pxfslib::get_err(e);
	} else {
		state_lock.unlock();
	}
	return (error);
}

int
pxfobj::fsync(int, struct cred *)
{
	return (0);
}

int
pxfobj::getattr(vattr *vap, int, cred *cr)
{
	sol::error_t		error;
	solobj::cred_var	credobj = solobj_impl::conv(cr);
	Environment		e;

	fobj_obj->get_attributes(vap->va_mask, conv(*vap), credobj, e);
	error = pxfslib::get_err(e);
	return (error);
}

int
pxfobj::setattr(vattr *vap, int flags, cred *cr)
{
	sol::error_t		error;
	solobj::cred_var	credobj = solobj_impl::conv(cr);
	Environment		e;

	fobj_obj->set_attributes(conv(*vap), flags, credobj, e);
	error = pxfslib::get_err(e);
	return (error);
}

int
pxfobj::access(int mode, int flags, cred *cr)
{
	sol::error_t		error;
	solobj::cred_var	credobj = solobj_impl::conv(cr);
	Environment		e;

	fobj_obj->access(mode, flags, credobj, e);
	error = pxfslib::get_err(e);

	return (error);
}

//
// XXX Duplicate definition of 'struct ufid' from sys/fs/ufs_inode.h.  We
// cannot include the file because it include 'ufs_fs.h', which contains a
// definition for a 'struct fs'.  This definition of 'struct fs' interferes
// with the idl module name 'fs' used for pxfs interfaces.
// We need to change the name of the idl module to something else, and then
// remove the duplicate definitions for 'ufid' and 'UFSROOTINO' from this file.
//
struct ufid {
	ushort_t ufid_len;
	int32_t ufid_ino;
	int32_t ufid_gen;
};

//
// XXX Duplicate definition of UFSROOTINO from sys/fs/ufs_fs.h.  See comments
// for 'struct ufid' above
//
#define	UFSROOTINO	((ino_t)2)	// i number of all roots

//
// This function is a hack, meticulously engineered to meet metamucil's
// needs in calling the _FIOIO UFS ioctl.  We try to produce as similar
// a result as possible from the ioctl when UFS is running under PXFS.
// The only difference in functionality is that at times, when the UFS version
// returns 'ESTALE', PXFS will return 'ENOENT'.  This is harmless from
// metamucil's point of view, since the only time it cares about the right
// error code is when it is opening the root inode, and we handcraft the error
// code to its correct value for that case.
// A lot of the code in this function has been picked out of 'ufs_fioio' - the
// reader is encouraged to read through that function first.
//
int
pxfobj::ioctl_fioio(intptr_t arg, int flag, struct cred *cr)
{
	int		error = 0;
	STRUCT_DECL(fioio, fio);	// copy of user's fioio struct
	struct file	*fpio = NULL;
	vnode_t		*vp = NULL;

	// Permission check.
	if (!drv_priv(cr)) {
		return (EPERM);
	}
	//
	// Make the compiler think 'flag' is used in 32-bit kernels where
	// STRUCT_INIT() doesn't check the flag.
	//
	flag = flag;
	STRUCT_INIT(fio, flag & DATAMODEL_MASK);

	//
	// Get user's copy of fioio struct.
	//
	if (copyin((const void *)arg, STRUCT_BUF(fio), STRUCT_SIZE(fio)))
		return (EFAULT);

	struct fid _fid;

	struct ufid *ufidp = (struct ufid *)&_fid;
	bzero((char *)ufidp, sizeof (struct ufid));
	ufidp->ufid_len = (ushort_t)(sizeof (struct ufid) - sizeof (ushort_t));
	ufidp->ufid_ino = (int32_t)STRUCT_FGET(fio, fio_ino);
	ufidp->ufid_gen = (int32_t)STRUCT_FGET(fio, fio_gen);

	// Call VFS_VGET on the proxy filesystem to get the appropriate vnode.
	error = VFS_VGET(get_vp()->v_vfsp, &vp, &_fid);

	//
	// An EINVAL error means that the appropriate vnode was not found.
	// This function needs to return different error codes in this case,
	// and we need to find out which one.
	// If we get an error other than EINVAL, we can just return with it.
	//

	if (error != 0 && error != EINVAL)
		goto errout;

	if (vp == NULL) {
		ASSERT(error == EINVAL);

		//
		// The '_FIOIO' ioctl returns two error codes: 'ENOENT' if the
		// file does not exist, and 'ESTALE' if the file has a different
		// generation number than the one passed in.
		// Using the implementation technique that we have chosen (using
		// VFS_VGET), we need to handcraft the error returns.
		// Now the only known caller of this ioctl is metamucil, and
		// the only error code checked for is 'ESTALE' when opening the
		// root inode.  Note that if we could not open the root inode,
		// the error has to be that the generation number has changed
		// (i.e. 'ESTALE').
		//

		if ((ino_t)ufidp->ufid_ino == UFSROOTINO) {	//lint !e571
			return (ESTALE);
		} else {
			return (ENOENT);
		}
	}

	//
	// We have got the vnode and now we need to create and return a fd to
	// the user.
	// The rest of this function is adapted from ufs_fioio()
	// XXX If ufs_fioio changes, this MUST change
	//

	//
	// Adapted from copen: get a file struct
	// Large Files: We open this file descriptor with FOFFMAX flag
	// set so that it will be like a large file open.
	//

	//
	// falloc returns with the fpio->f_tlock mutex held - this lock is
	// dropped either explicaitly below or when we call unfalloc in the
	// error case.
	//
	if (falloc(NULL, (FREAD|FOFFMAX), &fpio,
	    (int *)(STRUCT_FADDR(fio, fio_fd))))
		goto errout;

#if	SOL_VERSION >= __s11
	if ((error = VOP_ACCESS(vp, VREAD, 0, cr, NULL)) != 0) {
#else
	if ((error = VOP_ACCESS(vp, VREAD, 0, cr)) != 0) {
#endif
		goto errout;
	}

#if	SOL_VERSION >= __s11
	if ((error = VOP_OPEN(&vp, FREAD, cr, NULL)) != 0) {
#else
	if ((error = VOP_OPEN(&vp, FREAD, cr)) != 0) {
#endif
		goto errout;
	}

	//
	// Adapted from copen: initialize the file struct.
	//
	fpio->f_vnode = vp;

	//
	// Return the fd.
	//
	if (copyout(STRUCT_BUF(fio), (void *)arg, STRUCT_SIZE(fio))) {
		error = EFAULT;
		goto errout;
	}

	setf(STRUCT_FGET(fio, fio_fd), fpio);
	mutex_exit(&fpio->f_tlock);
	return (0);

errout:
	//
	// Free the file struct and fd.
	//
	if (fpio != NULL) {
		setf(STRUCT_FGET(fio, fio_fd), NULL);
		unfalloc(fpio);
	}

	//
	// Release the hold on the vnode.
	//
	if (vp != NULL)
		VN_RELE(vp);

	return (error);
}

int
pxfobj::ioctl(int cmd, intptr_t arg, int flag, struct cred *cr, int *retp)
{
	solobj::cred_var credobj = solobj_impl::conv(cr);
	int tmp_ret = 0;
	Environment e;

#ifdef DEBUG
	//
	// Debug code to return internal state to verify test results.
	//
#define	IOCTLNUM 72506	// Random number; must match testprogs/getstatus.c
	int v, err;
	if (cmd == IOCTLNUM) {
		v = is_cached();
		err = ::copyout((void *)&v, (void *)arg, sizeof (v));
		return (err ? EFAULT : 0);
	}
#endif

#if	SOL_VERSION >= __s10
	//
	// PxFS does not support SEEK_DATA/SEEK_HOLE yet. We let the
	// caller know this by returning ENOTTY. Supporting it needs
	// proper handling of FKIOCTLs on the server side.
	//
	if (cmd == _FIO_SEEK_DATA || cmd == _FIO_SEEK_HOLE) {
		return (ENOTTY);
	}
#endif

	//
	// Special handling is required for the _FIOIO UFS ioctls.
	//
	if (cmd == _FIOIO && pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->is_ufs()) {
		return (ioctl_fioio(arg, flag, cr));
	}

	fobj_obj->cascaded_ioctl(orb_conf::node_number(),
	    ttoproc(curthread)->p_pid, cmd, arg, flag, tmp_ret, credobj, e);

	//
	// Some ioctls ignore the return pointer and this is
	// signalled by a null value for retp. Rather than pass
	// a null pointer around, we make a temp variable and
	// if the retp was non-null we copy the temp back to the
	// user via the retp argument.
	//
	if (retp) {
		*retp = tmp_ret;
	}

	sol::error_t error = pxfslib::get_err(e);
	return (error);
}

int
pxfobj::pathconf(int cmd, ulong_t *valp, cred *cr)
{
	sol::uintptr_t value;
	solobj::cred_var credobj = solobj_impl::conv(cr);
	Environment e;

#if	SOL_VERSION >= __s10
	//
	// PxFS does not support SEEK_DATA/SEEK_HOLE, it does not make
	// sense to ask the underlying file system about it.
	//
	if (cmd == _PC_MIN_HOLE_SIZE) {
		return (EINVAL);
	}
#endif

	fobj_obj->pathconf(cmd, value, credobj, e);
	sol::error_t error = pxfslib::get_err(e);
	*valp = value;
	return (error);
}

void
pxfobj::dispose(page_t *pp, int fl, int dn, cred *cr)
{
#if	SOL_VERSION >= __s11
	fs_dispose(this->get_vp(), pp, fl, dn, cr, NULL);
#else
	fs_dispose(this->get_vp(), pp, fl, dn, cr);
#endif
}

int
pxfobj::setsecattr(vsecattr_t *vsap, int flag, cred *cr)
{
	solobj::cred_var credobj = solobj_impl::conv(cr);
	Environment e;
	PXFS_VER::secattr sattr;
	sattr.mask = vsap->vsa_mask;
	if (sattr.mask & VSA_ACL) {
		sattr.acl_ent.load((uint_t)vsap->vsa_aclcnt,
		    (uint_t)vsap->vsa_aclcnt,
		    (sol::aclent_t *)vsap->vsa_aclentp, false);
	}
	if (sattr.mask & VSA_DFACL) {
		sattr.acl_def_ent.load((uint_t)vsap->vsa_dfaclcnt,
		    (uint_t)vsap->vsa_dfaclcnt,
		    (sol::aclent_t *)vsap->vsa_dfaclentp, false);
	}
	fobj_obj->set_secattributes(sattr, flag, credobj, e);
	return (pxfslib::get_err(e));
}

int
pxfobj::getsecattr(vsecattr_t *vsap, int flag, cred *cr)
{
#ifdef DEBUG
	os::atomic_add_32(&pxfs_getsecattr_count, 1);
#endif

	sol::error_t		error;
	solobj::cred_var	credobj = solobj_impl::conv(cr);
	int32_t			aclcnt, dfaclcnt;
	PXFS_VER::secattr_var	sattr;
	Environment		e;

	//
	// If the actual ACLs are not needed, just return the
	// number of ACLs that would be returned.
	//
	if ((vsap->vsa_mask & (VSA_ACL | VSA_DFACL)) == 0) {
		fobj_obj->get_secattr_cnt(aclcnt, dfaclcnt, flag, credobj, e);
		error = pxfslib::get_err(e);
		if (error != 0) {
			return (error);
		}
		vsap->vsa_aclcnt = aclcnt;
		vsap->vsa_dfaclcnt = dfaclcnt;
	} else {
		fobj_obj->get_secattributes(sattr.out(), flag, credobj, e);
		error = pxfslib::get_err(e);
		if (error != 0) {
			return (error);
		}
		vsap->vsa_aclcnt = (int)sattr->acl_ent.length();
		//
		//lint -e571 -e573 During the cast we are loosing the
		// sign that's make the lint complain but this
		// is ok here.
		//
		if (vsap->vsa_aclcnt != 0) {
			vsap->vsa_aclentp = kmem_alloc(
			    (size_t)vsap->vsa_aclcnt *
			    sizeof (aclent_t), KM_SLEEP);
			bcopy(sattr->acl_ent.buffer(), vsap->vsa_aclentp,
			    ((size_t)vsap->vsa_aclcnt) * sizeof (aclent_t));
		}
		vsap->vsa_dfaclcnt = (int)sattr->acl_def_ent.length();
		if (vsap->vsa_dfaclcnt != 0) {
			vsap->vsa_dfaclentp = kmem_alloc(
			    (size_t)vsap->vsa_dfaclcnt *
			    sizeof (aclent_t), KM_SLEEP);
			bcopy(sattr->acl_def_ent.buffer(),
			    vsap->vsa_dfaclentp,
			    (size_t)vsap->vsa_dfaclcnt * sizeof (aclent_t));
		}
		//lint +e571 +e573
	}
	return (error);
}

//
//
// The following methods are invoked if the derived subclass does not
// support the corresponding vnode operation. The appropriate Solaris
// error code is returned.
//

int
pxfobj::read(uio *, int, cred *)
{
	return (ENOSYS);
}

int
pxfobj::write(uio *, int, cred *)
{
	return (ENOSYS);
}

// seek
int
pxfobj::seek(offset_t, offset_t *noffp)
{
	return ((*noffp < 0 || *noffp > MAXOFFSET_T) ? EINVAL : 0);
}

#if	SOL_VERSION >= __s9 /* new s9 frlock interface */
int
pxfobj::frlock(int cmd, flock64 *bfp, int flag, offset_t offset,
    flk_callback_t *flk_cbp,
    cred* cr)
#else
int
pxfobj::frlock(int cmd, flock64 *bfp, int flag, offset_t offset, cred* cr)
#endif
{
	u_offset_t start, end;
	Environment e;
	sol::error_t error;
	int rc;
	bool nlm_callback_made = false;

	//
	// Note: since PXFS doesn't support mandatory locking, we don't need
	// to check if the file is currently mapped into user space or not.
	//

	//
	// Check if this is an NLM request.
	//
	if (flag & F_REMOTELOCK) {
		bfp->l_sysid = pxfslib::set_nodeid(bfp->l_sysid,
			orb_conf::node_number());
		// Skip the consistency checks since NLM has already done them.
		goto dolock;
	}

	// Check for valid cmd parameter.
	switch (cmd) {
	case F_GETLK:
	case F_SETLK:
	case F_SETLKW:
#ifdef PSARC_2000_007
	case F_SETLK_NBMAND:
#endif
		ASSERT(!(flag & F_REMOTELOCK));
		flag |= F_PXFSLOCK;
		break;

	case F_HASREMOTELOCKS:
		bfp->l_sysid = pxfslib::set_nodeid(bfp->l_sysid,
			orb_conf::node_number());
		// Skip the consistency checks since NLM has already done them.
		goto dolock;

	case F_UNLKSYS:
		//
		// For PXFS remove all the locks on behalf of the client
		// identified by sysid.  Go directly to the LLM on the
		// PXFS client.
		//
		ASSERT(flag & F_REMOTELOCK);
		cl_flk_remove_locks_by_sysid(bfp->l_sysid);
		break;

	default:
		return (EINVAL);
	}

	bfp->l_pid = ttoproc(curthread)->p_pid;
	bfp->l_sysid = pxfslib::set_nodeid(0, orb_conf::node_number());

	// Verify l_type.
	switch (bfp->l_type) {
	case F_RDLCK:
		if (cmd != F_GETLK && !(flag & FREAD)) {
			return (EBADF);
		}
		break;

	case F_WRLCK:
		if (cmd != F_GETLK && !(flag & FWRITE)) {
			return (EBADF);
		}
		break;

	case F_UNLCK:
		break;

	default:
		return (EINVAL);
	}

	// Check the validity of the lock range.
	rc = flk_convert_lock_data(pxnode::PXTOV(this), bfp, &start,
	    &end, offset);
	if (rc != 0) {
		return (rc);
	}
	rc = flk_check_lock_data(start, end, MAXEND);
	if (rc != 0) {
		return (rc);
	}

	rc = convoff(pxnode::PXTOV(this), bfp, 0 /* SEEK_SET */, offset);
	if (rc != 0) {
		return (rc);
	}

dolock:
	solobj::cred_var		credobj = solobj_impl::conv(cr);
	pxfs_llm_callback_impl		*llmptr = NULL;
	PXFS_VER::pxfs_llm_callback_var cbobj;

#if	SOL_VERSION < __s9 /* l_callback and l_cbp obsoleted in s9 */
	callb_cpr_t			*(*cb_proc)(void *) = NULL;
	void				*cb_args = NULL;
#endif


	//
	// We check here to see if this is a blocking lock request.  If so,
	// we create a callback object for the server to use to wake us up
	// if required.
	//
	if ((cmd == F_SETLKW) &&
	    ((bfp->l_type == F_RDLCK) || (bfp->l_type == F_WRLCK))) {
		// Create the callback object.
		llmptr = new pxfs_llm_callback_impl();
		cbobj = llmptr->get_objref();

		// Insert llmptr on the list of all llmptr.
		pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->insert_llm_cbobj(llmptr);
	}

#if	SOL_VERSION < __s9 /* l_callback and l_cbp obsoleted in s9 */
	//
	// For blocking NLM requests, we need to save the callback function
	// and argument since the PXFS server may overwrite them.
	//
	if ((cmd == F_SETLKW) && (flag & F_REMOTELOCK)) {
		cb_proc = l_callback(bfp);
		cb_args = l_cbp(bfp);
	}
#endif

try_again:
	error = 0;

	for (;;) {
		FAULTPT_PXFS(FAULTNUM_PXFS_FRLOCK_C_O, FaultFunctions::generic);

		getfobj()->frlock(cmd, conv(*bfp), flag,
		    (sol::u_offset_t)offset, credobj, cbobj, e);

		CORBA::Exception *ex;
		if (((ex = e.exception()) == NULL) ||
		    (PXFS_VER::blocked::_exnarrow(ex) == NULL)) {
			//
			// No special processing required - just fall out of
			// the loop.
			//
			break;
		}

		//
		// We have received a 'blocked' exception from the server to
		// indicate to us that we should sleep.
		//
		ASSERT(PXFS_VER::blocked::_exnarrow(ex) != NULL);
		e.clear();

		ASSERT(llmptr != NULL);
		ASSERT(cmd == F_SETLKW);

		//
		// Mimic the LLM code and make the appropriate CPR calls before
		// sleeping, if necessary.
		// Make sure we make the NLM callback only once - see BugId
		// 4333303.
		//
#if	SOL_VERSION >= __s9
		if ((flag & F_REMOTELOCK) && (flk_cbp != NULL) &&
#else
		if ((flag & F_REMOTELOCK) && (cb_proc != NULL) &&
#endif
		    (!nlm_callback_made)) {
			nlm_callback_made = true;
#if	SOL_VERSION < __s9
			callb_cpr_t *cprp = (*cb_proc)(cb_args);
#else
			callb_cpr_t *cprp = flk_invoke_callbacks(flk_cbp,
							FLK_BEFORE_SLEEP);
#endif
			if (cprp == NULL) {
				error = llmptr->wait_for_lock();
			} else {
				mutex_enter(cprp->cc_lockp);
				CALLB_CPR_SAFE_BEGIN(cprp);
				mutex_exit(cprp->cc_lockp);
				error = llmptr->wait_for_lock();
				mutex_enter(cprp->cc_lockp);
				CALLB_CPR_SAFE_END(cprp, cprp->cc_lockp);
				mutex_exit(cprp->cc_lockp);
			}
#if	SOL_VERSION >= __s9
			(void) flk_invoke_callbacks(flk_cbp, FLK_AFTER_SLEEP);
#endif
		} else {
			error = llmptr->wait_for_lock();
		}

		switch (error) {
		case pxfs_llm_callback_impl::RETRY_LOCK:
			//
			// The server failed or switched over, and we need to
			// retry.
			//
			llmptr->init();
			goto try_again;

		case EIO:
			//
			// The PXFS server for this filesystem is dead.  Return
			// 'EIO' to the client.
			//
			break;

		case EINTR:
			//
			// A Ctrl-C woke us up - cancel the request on the
			// server.
			//
			getfobj()->frlock_cancel_request(cbobj, e);
			break;

		case 0:
			//
			// The server woke us up and we need to drive the
			// rest of the execution of the file locking request.
			//
			FAULTPT_PXFS(FAULTNUM_PXFS_PXFOBJWAKEUPLOCK_C_O,
				FaultFunctions::generic);
			getfobj()->frlock_execute_request(cbobj, e);
			break;

		default:
			ASSERT(0);
			//
			// Note that in the non-debug case, this will fall
			// through and return the error to the user.
			//
		}

		if (((ex = e.exception()) != NULL) &&
		    (PXFS_VER::invalid_cb::_exnarrow(ex) != NULL)) {
			//
			// The server must have failed or switched over
			// between the time we woke up and the time we
			// made the frlock_execute_request() call.
			// We need to retry this lock.
			//
			error = 0;
			e.clear();
			llmptr->init();
			continue;
		}

		break;
	}

	error = pxfslib::get_err(error, e);
	e.clear();

	if (llmptr != NULL) {
		// Remove from list of all llmptr.
		pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->remove_llm_cbobj(llmptr);

		// llmptr will be freed by _unreferenced() after we return.
	}

	if (error == 0) {
		// Record that this file might have a lock (see close()).
		state_lock.lock();
		pxflags |= PX_HAS_LOCKS;
		state_lock.unlock();
	}

	ASSERT(error >= 0);
	return (error);
}

//
// Effects: File share reservations are an advisory form of access control
//   among cooperating processes, on both local and remote machines.  They
//   are most often used by DOS or Windows emulators and DOS-based NFS
//   clients.  Native Unix versions of DOS or Windows applications may also
//   choose to use this form of access control.
//
int
pxfobj::vop_shrlock(int cmd, struct shrlock *bfp, int flag, cred *cr)
{
	Environment e;


	//
	// Assumption:
	//    We assume that the NLM server will set the nodeid
	// appropriately before this routine is called.  If it's not
	// set, we assume that a local client app is calling VOP_SHRLOCK,
	// and we must give it a node id.
	//
	// If the node id of lock_info.sysid is 0, then
	//    generate node id
	//    set the node id portion of lock_info.sysid to nodeid
	//
	if (pxfslib::get_nodeid(bfp->s_sysid) == 0) {
		nodeid_t node = orb_conf::node_number();	// get node id
		bfp->s_sysid = pxfslib::set_nodeid(bfp->s_sysid, node);
	} // end if

	//
	// Copy fields of original struct "bfp" to instance of IDL type
	// "lock_info."
	//
	sol::shrlock_t lock_info;
	shr_flatten(bfp, lock_info);

#if	SOL_VERSION >= __s10
	solobj::cred_var	credobj = solobj_impl::conv(cr);
	fobj_obj->shrlock(cmd, lock_info, flag, credobj, e);
#else
	fobj_obj->shrlock(cmd, lock_info, flag, NULL, e);
#endif
	sol::error_t error = pxfslib::get_err(e);

	if (error == 0) {
		// Record that this file might have a lock (see close()).
		state_lock.lock();
		pxflags |= PX_HAS_LOCKS;
		state_lock.unlock();
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_PXFOBJ,
		    PXFS_RED,
		    ("vop_shrlock(): trying to obtain"
		    " share lock returned error '%d.'"
		    " cmd=%d, sysid=0x%x, pid=%d.\n", error, cmd,
		    bfp->s_sysid, bfp->s_pid));
	} // end if

	//
	// Copy fields of flattened struct "lock_info" back to original
	// argument "bfp" to pick up any changes from the invocation.
	//
	shr_unflatten(lock_info, bfp);


	return (error);
}

int
pxfobj::space(int, struct flock64 *, int, offset_t, cred *)
{
	return (EINVAL);
}

int
pxfobj::realvp(vnode **)
{
	return (ENOSYS);
}

int
pxfobj::create(char *, vattr *, vcexcl, int, vnode **, cred *, int)
{
	return (ENOTDIR);
}

int
pxfobj::remove(char *, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::lookup(char *, vnode **, pathname *, int, vnode *, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::mkdir(char *, vattr *, vnode **, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::rmdir(char *, vnode *, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::readdir(uio *, cred *, int &)
{
	return (ENOTDIR);
}

int
pxfobj::map(offset_t, as *, caddr_t *, size_t, uchar_t, uchar_t, uint_t, cred *)
{
	return (ENOSYS);
}

int
pxfobj::addmap(offset_t, as *, caddr_t, size_t, uchar_t, uchar_t, uint_t,
    cred *)
{
	return (ENOSYS);
}

int
pxfobj::delmap(offset_t, as *, caddr_t, size_t, uint_t, uint_t, uint_t, cred *)
{
	return (ENOSYS);
}

int
pxfobj::getpage(offset_t, size_t, uint_t *, page_t *[],
	size_t, struct seg *, caddr_t, seg_rw, cred *)
{
	return (ENOSYS);
}

int
pxfobj::putpage(offset_t, size_t, int, cred *)
{
	return (ENOSYS);
}

int
pxfobj::readlink(uio *, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::symlink(char *, vattr *, char *, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::link(vnode *, char *, cred *)
{
	return (ENOTDIR);
}

int
pxfobj::rename(char *, vnode *, char *, cred *)
{
	return (ENOTDIR);
}

void
pxfobj::rwlock(int)
{
	// empty
}

void
pxfobj::rwunlock(int)
{
	// empty
}

//
// Pvnode is a packed vnode, which is a structure containing
// object references corresponding to the vnode.
//
// The caller passes in a vnode and storage for a pvnode structure;
// this routine fills in the pvnode fields.
// Return zero if we were successful or -1 if not.
//
int
pxfobj::pack_vnode(vnode_t *vp, PXFS_VER::pvnode &pvnode)
{
	if (vp == NULL) {
		// It is OK to pass NULL vnode pointers.
		pvnode.vfs = PXFS_VER::filesystem::_nil();
		pvnode.fileobj = PXFS_VER::fobj::_nil();

		// No point in clearing fobjinfo.
		return (0);
	}

	// This cast is safe since all PXFS proxies inherit from pxfobj.
	PXFS_VER::fobj_ptr	obj_ptr =
	    ((pxfobj *)pxnode::VTOPX(vp))->getfobj();
	ASSERT(!CORBA::is_nil(obj_ptr));

	Environment		e;
	PXFS_VER::fobj_type_t	ftype = obj_ptr->get_fobj_type(e);
	if (e.exception()) {
		pvnode.vfs = PXFS_VER::filesystem::_nil();
		pvnode.fileobj = PXFS_VER::fobj::_nil();
		return (-1);
	}

	pvnode.fileobj = PXFS_VER::fobj::_duplicate(obj_ptr);

	PXFS_VER::filesystem_ptr	fs_ptr =
	    pxvfs::VFSTOPXFS(vp->v_vfsp)->get_fsobj();
	ASSERT(!CORBA::is_nil(fs_ptr));
	pvnode.vfs = PXFS_VER::filesystem::_duplicate(fs_ptr);

	pxfs_misc::init_fobjinfo(pvnode.fobjinfo, ftype, vp,
	    ((pxfobj *)pxnode::VTOPX(vp))->get_fidp());

	return (0);
}

//
// unpack_vnode
// The caller passes in a pvnode structure and
// this routine makes a vnode corresponding to it and returns it.
// Return NULL if we could not convert the pvnode into a valid vnode.
//
vnode_t *
pxfobj::unpack_vnode(const PXFS_VER::pvnode &pvnode)
{
	// Make sure we have a valid pxfs file system and fobj.
	if (CORBA::is_nil(pvnode.vfs) || CORBA::is_nil(pvnode.fileobj)) {
		return (NULL);
	}

	//
	// XXX - find_pxvfs() only creates pxvfs's (not pxprocvfs's).
	//
	pxvfs	*pxvfsp = pxvfs::find_pxvfs(pvnode.vfs, NULL);
	if (pxvfsp == NULL) {
		return (NULL);
	}

	vnode_t	*vp = pxvfsp->get_pxfobj(pvnode.fileobj, pvnode.fobjinfo,
	    NULL, NULL);
	VFS_RELE(pxvfsp->get_vfsp());
	return (vp);
}

//
// The base class implementation of this method does not do anything.
// pxreg (regular files and sockets) override this implementation.
//
int
pxfobj::sync_file()
{
	return (0);
}

//
// The base class implementation of this method does not do anything.
// pxreg (regular files and sockets) overrides this implementation.
//
int
pxfobj::sync_file_revoke()
{
	return (0);
}
