//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)access_cache.cc	1.7	08/05/20 SMI"

#include <sys/dnlc.h>
#include <sys/errno.h>
#include <sys/kmem.h>
#include <sys/cred.h>

#include <sys/sol_version.h>

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/access_cache.h>
#include <pxfs/client/pxfobjplus.h>
#include <pxfs/client/pxvfs.h>

acache_entry	*access_cache::acache_array = NULL;

uint32_t	access_cache::acachesize = (uint32_t)ncsize;
uint32_t	access_cache::acachemask;

os::rwlock_t	access_cache::acache_lock;

//
// lookup - return whether access is permitted to this file.
//	0	= access allowed
//	EACCES	= access denied
//	ENOENT	= no entry, information not in access cache
//
// static
int
access_cache::lookup(int mode, cred_t *credp, pxfobjplus *pxfobjplusp)
{
	int		result;
	uint32_t	hash;
	acache_entry	*entryp;

	//
	// Compute hash index based on pxfobjplus pointer and cred pointer.
	//
	//lint -e571 warning: cast a 64bit to a 32bit
#if SOL_VERSION >= __s10
	hash = ((uint32_t)(((uint64_t)pxfobjplusp >> 4) ^
	    (uint64_t)crgetuid(credp)));
#else
	hash = ((uint32_t)(((uint64_t)pxfobjplusp >> 4) ^
	    (uint64_t)credp->cr_uid));
#endif
	hash = (hash & acachemask) % acachesize;
	//lint +e571

	entryp = &(acache_array[hash]);

	kstat_t		*stats =
	    pxvfs::VFSTOPXFS(pxfobjplusp->get_vp()->v_vfsp)->stats;

	acache_lock.rdlock();

	if (entryp->pxfobjplusp == pxfobjplusp) {
		if (crcmp(entryp->credp, credp) == 0) {
			if (((mode & entryp->neg_mask) ||
			    (mode & entryp->pos_mask) == mode)) {

				PXFS_DBPRINTF(
				    PXFS_TRACE_ACCESS_CACHE,
				    PXFS_GREEN,
				    ("access:lookup+ %p entry %p hash %d pos %x"
				    " neg %x mode %x\n",
				    pxfobjplusp, entryp, hash,
				    entryp->pos_mask, entryp->neg_mask, mode));

				if ((entryp->pos_mask & mode) == mode) {
					result = 0;
				} else {
					result = EACCES;
				}
				acache_lock.unlock();

				PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
				    PXVFS_STATS_ACCESS_TOKEN_HITS].
				    value.ui32++));

				return (result);
			} else {
				PXFS_DBPRINTF(
				    PXFS_TRACE_ACCESS_CACHE,
				    PXFS_GREEN,
				    ("access:lookup- mode %p entry %p "
				    "hash %d pos %x neg %x mode %x\n",
				    pxfobjplusp, entryp, hash,
				    entryp->pos_mask, entryp->neg_mask, mode));
			}
		} else {
			PXFS_DBPRINTF(
			    PXFS_TRACE_ACCESS_CACHE,
			    PXFS_GREEN,
			    ("access:lookup- cred %p entry %p hash %d\n",
			    pxfobjplusp, entryp, hash));
		}
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_ACCESS_CACHE,
		    PXFS_GREEN,
		    ("access:lookup- obj %p entry %p hash %d\n",
		    pxfobjplusp, entryp, hash));
	}
	acache_lock.unlock();

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
	    PXVFS_STATS_ACCESS_TOKEN_MISSES].value.ui32++));

	return (ENOENT);
}

//
// enter - cache the results for this access operation
//
// Return value
//	true	- results cached
//	false	- results not cached (sequence number obsolete)
//
// static
bool
access_cache::enter(int mode, cred_t *credp, pxfobjplus *pxfobjplusp,
    int result, uint64_t new_seqnum, acache_head *headp)
{
	uint32_t	hash;
	acache_entry	*entryp;

	//
	// Compute hash index based on pxfobjplus pointer and cred pointer.
	//
	//lint -e571 warning: cast a 64bit to a 32bit
#if SOL_VERSION >= __s10
	hash = ((uint32_t)(((uint64_t)pxfobjplusp >> 4) ^
	    (uint64_t)crgetuid(credp)));
#else
	hash = ((uint32_t)(((uint64_t)pxfobjplusp >> 4) ^
	    (uint64_t)credp->cr_uid));
#endif
	hash = (hash & acachemask) % acachesize;
	//lint +e571

	entryp = &(acache_array[hash]);

	acache_lock.wrlock();

	if (new_seqnum < headp->access_seqnum) {
		//
		// Obsolete sequence number
		//
		acache_lock.unlock();
		return (false);
	}

	purge_locked(headp, new_seqnum);

	//
	// If this slot has the same cred val and belongs to
	// to the same fobj, then we can just update
	// the mask and success vs. failure information.
	//
	if (entryp->pxfobjplusp != pxfobjplusp ||
	    crcmp(entryp->credp, credp) != 0) {
		//
		// The entry is unused or already in use by somebody else
		//
		if (entryp->pxfobjplusp != NULL)  {
			remove(&(entryp->pxfobjplusp->head), entryp);
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_ACCESS_CACHE,
		    PXFS_GREEN,
		    ("access:enter %p entry %p hash %d result %d\n",
		    pxfobjplusp, entryp, hash, result));

		//
		// Enter entry on the doubly-linked circular list for this file
		//
		if (headp->first_acachep != NULL) {
			entryp->next = headp->first_acachep;
			entryp->prev = headp->first_acachep->prev;
			headp->first_acachep->prev->next = entryp;
			headp->first_acachep->prev = entryp;
		} else {
			entryp->next = entryp;
			entryp->prev = entryp;
			headp->first_acachep = entryp;
		}

		//
		// Not in use or the old entry has been evicted.
		//
		entryp->pxfobjplusp = pxfobjplusp;
		entryp->credp = crdup(credp);
		entryp->pos_mask = 0;
		entryp->neg_mask = 0;
	}

	if (result == 0) {
		entryp->pos_mask |= mode;
	} else {
		entryp->neg_mask |= mode;
	}
	acache_lock.unlock();
	return (true);
}

//
// remove - an entry from the access cache
//
// static
void
access_cache::remove(acache_head *headp, acache_entry *entryp)
{
	ASSERT(acache_lock.write_held());

	PXFS_DBPRINTF(
	    PXFS_TRACE_ACCESS_CACHE,
	    PXFS_GREEN,
	    ("access:remove %p entryp %p\n",
	    entryp->pxfobjplusp, entryp));

	kstat_t		*stats =
	    pxvfs::VFSTOPXFS(entryp->pxfobjplusp->get_vp()->v_vfsp)->stats;

	entryp->pxfobjplusp = NULL;
	crfree(entryp->credp);
	entryp->credp = NULL;

	PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))[
	    PXVFS_STATS_ACCESS_TOKEN_INVALS].value.ui32++));

	if (entryp->next == entryp) {
		//
		// This was the only entry for this file.
		//
		headp->first_acachep = NULL;
		return;
	}
	//
	// The file has multiple entries
	//
	entryp->next->prev = entryp->prev;
	entryp->prev->next = entryp->next;

	if (headp->first_acachep == entryp) {
		headp->first_acachep = entryp->next;
	}
}

// static
void
access_cache::purge(acache_head *headp)
{
	acache_lock.wrlock();
	purge_locked(headp, (uint64_t)ULONG_MAX);
	acache_lock.unlock();
}

// static
void
access_cache::purge(acache_head *headp, uint64_t new_seqnum)
{
	acache_lock.wrlock();
	purge_locked(headp, new_seqnum);
	acache_lock.unlock();
}

//
// purge_locked - remove all obsolete access cache entries for this file.
//
// The access sequence number is used to determine obsolete entries.
//
// static
void
access_cache::purge_locked(acache_head *headp, uint64_t new_seqnum)
{
	acache_entry	*entryp;

	ASSERT(acache_lock.write_held());

	if (headp->access_seqnum >= new_seqnum) {
		//
		// This request is obsolete.
		// So ignore request.
		//
		return;
	}
	if (new_seqnum == ULONG_MAX) {
		// Deal with word overflow
		headp->access_seqnum = 0;
	} else {
		headp->access_seqnum = new_seqnum;
	}

	if ((entryp = headp->first_acachep) == NULL) {
		//
		// There are no access cache entries for this file
		//
		return;
	}

	kstat_t		*stats =
	    pxvfs::VFSTOPXFS(entryp->pxfobjplusp->get_vp()->v_vfsp)->stats;

	do {
		PXFS_DBPRINTF(
		    PXFS_TRACE_ACCESS_CACHE,
		    PXFS_GREEN,
		    ("access:purge %p entry %p\n",
		    entryp->pxfobjplusp, entryp));

		entryp->pxfobjplusp = NULL;
		crfree(entryp->credp);
		entryp->credp = NULL;

		PXFS_KSTATS(stats, ((KSTAT_NAMED_PTR(stats))
		    [PXVFS_STATS_ACCESS_TOKEN_INVALS].value.ui32++));

	} while (entryp = entryp->next,
	    entryp != headp->first_acachep);

	headp->first_acachep = NULL;
}

//
// startup - creates the access cache for this node.
//
// static
int
access_cache::startup()
{
	access_cache::acache_array = (acache_entry *)kmem_zalloc(acachesize *
	    sizeof (acache_entry), KM_SLEEP);
	ASSERT(access_cache::acache_array != NULL);

	//
	// If the cache size is a power of two, then
	// the mask is cachesize - 1.
	//
	if ((acachesize & (acachesize - 1)) == 0) {
		acachemask = acachesize - 1;
	} else {
		int i;
		for (i = 0; acachesize >> i; i++)
			;
		acachemask = (1 << i) - 1;
	}
	PXFS_DBPRINTF(
	    PXFS_TRACE_ACCESS_CACHE,
	    PXFS_GREEN,
	    ("access:startup acachesize %u %x mask %x\n",
	    acachesize, acachesize, acachemask));
	return (0);
}

//
// shutdown - PXFS does not support shutdown
//
// static
int
access_cache::shutdown()
{
	return (EBUSY);
}
