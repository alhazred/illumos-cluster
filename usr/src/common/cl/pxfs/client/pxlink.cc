//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pxlink.cc - proxy vnode for a symbolic link
//

#pragma ident	"@(#)pxlink.cc	1.8	08/05/20 SMI"

#include <sys/types.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/uio.h>
#include <sys/debug.h>

#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/client/pxlink.h>

pxlink::pxlink(fobj_client_impl *fclientp, vfs *vfsp,
    PXFS_VER::symbolic_link_ptr linkp,
    const PXFS_VER::fobj_info &fobjinfo) :
	pxfobjplus(fclientp, vfsp, linkp, fobjinfo)
{
	cached_target = NULL;
}

pxlink::~pxlink()
{
	if (cached_target != NULL) {
		delete cached_target;
	}
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

int
pxlink::readlink(uio *uiop, cred *cr)
{
	int 	error;

	if (cached_target == NULL) {
		Environment		e;
		solobj::cred_var	credobj = solobj_impl::conv(cr);
		PXFS_VER::symbolic_link::target_t	*target;

		getlink()->readlink(target, credobj, e);
		error = pxfslib::get_err(e);
		if (error) {
			return (error);
		}

		//
		// Do an atomic compare and swap.
		// The first thread to try and set cached_target will win
		// and any others will free the memory they allocated instead.
		//
		if (os::casptr((void **)&cached_target, NULL, target) != NULL) {
			delete target;
		}
	}

	// Only copyout the amount of data requested by the user
	size_t	iolen = cached_target->length();
	iolen = MIN((size_t)uiop->uio_resid, iolen);
	iovec	*iovp = uiop->uio_iov;

	error = pxfs_misc::uio_copyout(cached_target->buffer(),
	    iovp->iov_base, iolen, uiop->uio_segflg);
	if (error != 0) {
		return (error);
	}

	iovp->iov_base += iolen;
#if defined(_LP64)
	iovp->iov_len -= iolen;
#else
	iovp->iov_len -= (long)iolen;
#endif
	uiop->uio_resid -= (ssize_t)iolen;
	uiop->uio_loffset = 0;

	return (0);
}
