//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pxchr.h - proxy vnode for character special devices (i.e. VCHR vnodes)
//

#ifndef PXCHR_H
#define	PXCHR_H

#pragma ident	"@(#)pxchr.h	1.7	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/client/pxfobj.h>

class pxchr : public pxfobj {
public:
	pxchr(vfs *vfsp, PXFS_VER::io_ptr fobjp,
	    const PXFS_VER::fobj_info &fobjinfo);

	~pxchr();

	// vnode operations
	int	open(vnode **vpp, int flag, cred *cr);
	int	close(int flag, int count, offset_t offset, cred *cr);
	int	read(uio *uiop, int ioflag, cred *cr);
	int	write(uio *uiop, int ioflag, cred *cr);
	int	fsync(int syncflag, struct cred *cr);

	int	map(offset_t off, as *as, caddr_t *addrp, size_t len,
		    uchar_t prot, uchar_t maxprot, uint_t flags, cred *cr);

	int	realvp(vnode **vpp);

	//
	// The set_realvp function is called by pxspecial::open() when
	// a pxchr object is created.  The vnode representing the
	// pxspecial object is sent in as the realvp argument.
	//
	// NOTE:  Assumes that "realvp" will not change.
	//
	void	set_realvp(vnode *_realvp, bool);
	int	pxfs_aiorw(struct aio_req *aio, cred_t *cred_p, int iswrite,
	    bool isretry);

private:
	PXFS_VER::io_ptr	getio();

	vnode	*real_vp;
	bool	is_hadev;

	// disallowed operations
	pxchr& operator=(const pxchr& rsh);
	pxchr(const pxchr& rsh);
};

inline PXFS_VER::io_ptr
pxchr::getio()
{
	//
	// This conversion is safe as long as io inherits from
	// fobj (see pxfs.idl).
	//
	return ((PXFS_VER::io_ptr)getfobj());
}

#endif	// PXCHR_H
