//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fobj_client_impl.cc	1.12	08/05/20 SMI"

#include <sys/vnode.h>

#include "../version.h"
#include <pxfs/client/fobj_client_impl.h>
#include <pxfs/client/pxfobjplus.h>
#include <pxfs/client/pxdir.h>
#include <pxfs/client/pxreg.h>

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

#ifdef DEBUG
uint32_t	fobj_client_number_created = 0;
uint32_t	fobj_client_number_destroyed = 0;
#endif

fobj_client_impl::fobj_client_impl() :
	cflags(RELEASED),
	pxfobjplusp(NULL)
{
	//
	// Provide a means to get the implementation pointer
	// from the CORBA object reference
	//
	// LINTED: Call to virtual function within a contructor or destructor.
	_handler()->set_cookie((void *)this);

#ifdef DEBUG
	os::atomic_add_32(&fobj_client_number_created, 1);
#endif
}	

fobj_client_impl::~fobj_client_impl()
{
	ASSERT((cflags & RELEASE_PENDING) == 0);
	ASSERT(cflags & RELEASED);
	ASSERT(cflags & UNREFERENCED);

#ifdef DEBUG
	os::atomic_add_32(&fobj_client_number_destroyed, 1);
#endif

} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// _unreferenced - This is called when all CORBA references are gone.
// If the proxy vnode is created, we hold a self-reference until
// the proxy vnode tells us that the proxy vnode is going away (see rele()).
//
void
fobj_client_impl::_unreferenced(unref_t arg)
{
	state_lock.lock();
	ASSERT(_last_unref(arg));

	// The UNREFERENCED flag makes debugging easier
	ASSERT((cflags & RELEASE_PENDING) == 0);
	ASSERT(cflags & RELEASED);
	ASSERT((cflags & UNREFERENCED) == 0);
	cflags |= UNREFERENCED;

	state_lock.unlock();
	delete this;
}

//
// set_pxfobj - Set the associated proxy vnode object
// This is used only during the client registration phase.
//
void
fobj_client_impl::set_pxfobjplus(pxfobjplus *px_fobjplusp)
{
	ASSERT(pxfobjplusp == NULL);
	pxfobjplusp = px_fobjplusp;

	state_lock.lock();
	cflags &= ~RELEASED;
	cflags |= REGISTERED;

	//
	// This effectively holds a self-reference,
	// which prevents the fobj_client from being unreferenced
	// while the proxy vnode exists.
	//
	(void) get_objref();

	// Wake up all threads waiting for this client to become usable
	registered_cv.broadcast();

	state_lock.unlock();
}

//
// rele - This is called when the proxy vnode is deleted.
// We release our reference to the provider which will cause the
// provider to release its reference to us.
//
void
fobj_client_impl::rele()
{
	ASSERT(pxfobjplusp != NULL);

	state_lock.lock();
	cflags |= DISCONNECTING;

	// Notify any waiting threads about a state change
	registered_cv.broadcast();

	state_lock.unlock();

	//
	// Tell the server that this client is going away.
	//
	// The server will not process this request until all
	// outstanding server requests upon the client complete.
	// After this operation completes, the server will not
	// make any more requests upon the client.
	//
	// If the server is dead, we do not care, because
	// object references from a dead node are always released.
	//
	Environment	env;
	pxfobjplusp->getfobjplus()->
	    cache_remove_client((PXFS_VER::fobj_client_ptr)this, env);
	env.clear();

	state_lock.lock();
	ASSERT(cflags & REGISTERED);
	ASSERT(cflags & RELEASE_PENDING);
	ASSERT(cflags & FLUSHED);
	ASSERT((cflags & RELEASED) == 0);
	ASSERT((cflags & UNREFERENCED) == 0);
	cflags &= ~RELEASE_PENDING;
	cflags |= RELEASED;

	// Wake up all threads waiting for this client to deregister
	registered_cv.broadcast();

	//
	// The proxy vnode is going away.
	// We have to drop the self-reference so that the fobj_client
	// will also go away via unreference.
	//
	CORBA::release((PXFS_VER::fobj_client_ptr)this);

	state_lock.unlock();
}

//
// set_pending_release
// This is called prior to disconnecting from the server.
// Any thread obtaining a new reference from the server
// with this flag set will wait until release occurs or
// the release is cancelled.
//
void
fobj_client_impl::set_pending_release()
{
	state_lock.lock();
	ASSERT(cflags & REGISTERED);
	ASSERT((cflags & RELEASE_PENDING) == 0);
	ASSERT((cflags & FLUSHED) == 0);
	ASSERT((cflags & DISCONNECTING) == 0);
	ASSERT((cflags & RELEASED) == 0);
	ASSERT((cflags & UNREFERENCED) == 0);
	cflags |= RELEASE_PENDING;
	state_lock.unlock();
}

//
// cancel_pending_release
// A pending disconnect has been cancelled.
//
void
fobj_client_impl::cancel_pending_release()
{
	state_lock.lock();
	ASSERT(cflags & REGISTERED);
	ASSERT((cflags & DISCONNECTING) == 0);
	ASSERT((cflags & RELEASED) == 0);
	ASSERT((cflags & UNREFERENCED) == 0);
	ASSERT(cflags & RELEASE_PENDING);
	cflags &= ~(RELEASE_PENDING | FLUSHED);

	// Wake up all threads waiting on the pending disconnect
	registered_cv.broadcast();

	state_lock.unlock();
}

//
// set_flushed
// All information has been flushed to server.
//
void
fobj_client_impl::set_flushed()
{
	state_lock.lock();
	ASSERT(cflags & REGISTERED);
	ASSERT(cflags & RELEASE_PENDING);
	ASSERT((cflags & FLUSHED) == 0);
	ASSERT((cflags & DISCONNECTING) == 0);
	ASSERT((cflags & RELEASED) == 0);
	ASSERT((cflags & UNREFERENCED) == 0);
	cflags |= FLUSHED;
	state_lock.unlock();
}

//
// client_ready
// The fobj_client is guaranteed to remain in existence as long
// as the server holds a reference.
//
// This method waits until registration has completed.
//
bool
fobj_client_impl::client_ready()
{
	state_lock.lock();

	//
	// The server might try to request that the client do something
	// before the client finishes initialization. For example,
	// the server could invalidate the attributes before they
	// even arrive.
	//
	while ((cflags & REGISTERED) == 0) {
		registered_cv.wait(&state_lock);
	}

	if (cflags & FLUSHED) {
		//
		// The proxy file object is trying to go away
		// and has already flushed information to the server.
		//
		state_lock.unlock();
		return (false);
	}

	//
	// It is OK to be in the RELEASE_PENDING state,
	// as the client can still process server requests.
	//
	// Whenever the server makes a request upon the client,
	// the server must hold a read lock on the list of clients.
	// This prevents any client from disconnecting while the
	// server makes a request upon that client.
	//
	// Ensure that the proxy file object continues
	// in existance till this request completes
	//
	VN_HOLD(pxnode::PXTOV(pxfobjplusp));

	state_lock.unlock();
	return (true);
}

//
// wait_till_ok - wait until the client completes any
// transition, which can be either initialization or termination.
//
// Return value
//	true	- the client is safe to use
//			The proxy vnode reference count is incremented.
//
//	false	- the client went away
//
bool
fobj_client_impl::wait_till_ok()
{
	bool	result;

	state_lock.lock();

	while ((cflags & REGISTERED) == 0) {
		registered_cv.wait(&state_lock);
	}

	while (cflags & RELEASE_PENDING) {
		registered_cv.wait(&state_lock);
	}

	if (cflags & RELEASED) {
		//
		// The client went away.
		//
		result = false;
	} else {
		//
		// The client is OK.
		// Place a hold to keep proxy vnode around.
		//
		VN_HOLD(pxnode::PXTOV(pxfobjplusp));
		result = true;
	}
	state_lock.unlock();
	if (result) {
		pxfobjplusp->set_recycled();
	}
	return (result);
}

//
// recover_client_ref - a new server is recovering after a node failure,
// and needs to acquire a new object reference to the client.
//
PXFS_VER::fobj_client_ptr
fobj_client_impl::recover_client_ref()
{
	PXFS_VER::fobj_client_ptr	client_p;
	state_lock.lock();
	cflags &= ~(UNREFERENCED);
	client_p = get_objref();
	state_lock.unlock();
	return (client_p);
}

//
// get_open_count
// This operation is valid for proxy vnodes that supports caching.
//
void
fobj_client_impl::get_open_count(uint32_t &count, Environment &_environment)
{
	// Default to file not in use
	count = 0;

	if (client_ready()) {
		pxfobjplusp->get_open_count(count, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
}

//
// get_vnode_count
// This operation is valid for proxy vnodes that supports caching.
//
void
fobj_client_impl::get_vnode_count(uint32_t &count, Environment &_environment)
{
	// Default to file not in use
	count = 0;

	if (client_ready()) {
		pxfobjplusp->get_vnode_count(count, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
}

//
// attr_release
// This operation is valid for proxy vnodes that support caching
//
void
fobj_client_impl::attr_release(uint64_t attr_seqnum,
    uint64_t access_seqnum, Environment &_environment)
{
	if (client_ready()) {
		pxfobjplusp->attr_release(attr_seqnum, access_seqnum,
		    _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// Already flushed attributes and dropped token.
		//
		// The PXFS client sometimes drops the token without
		// telling the server, and that can hit this situation.
		//
		// An orphan request can hit this situation
		//
}

//
// attr_write_back_and_release
// This operation is valid for proxy vnodes that supports caching.
//
void
fobj_client_impl::attr_write_back_and_release(uint64_t attr_seqnum,
    uint64_t access_seqnum, sol::vattr_t &attributes,
    Environment &_environment)
{
	if (client_ready()) {
		pxfobjplusp->attr_write_back_and_release(attr_seqnum,
		    access_seqnum, attributes, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	} else {
		// client not ready
		//
		// Already flushed attributes and dropped token.
		//
		// The PXFS client sometimes drops the token without
		// telling the server, and that can hit this situation.
		//
		// An orphan request can hit this situation
		//

		// Return no dirty attributes
		attributes.va_mask = 0;
	}
}

//
// attr_write_back_and_downgrade
// This operation is valid for proxy vnodes that supports caching.
//
void
fobj_client_impl::attr_write_back_and_downgrade(uint64_t attr_seqnum,
    uint64_t access_seqnum, sol::vattr_t &attributes,
    Environment &_environment)
{
	if (client_ready()) {
		pxfobjplusp->attr_write_back_and_downgrade(attr_seqnum,
		    access_seqnum, attributes, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	} else {
		// client not ready
		//
		// Already flushed attributes and dropped token.
		//
		// The PXFS client sometimes drops the token without
		// telling the server, and that can hit this situation.
		//
		// An orphan request can hit this situation
		//

		// Return no dirty attributes
		attributes.va_mask = 0;
	}
}

//
// clear_attr_dirty
// This operation is valid for proxy vnodes that supports caching.
//
void
fobj_client_impl::clear_attr_dirty(uint32_t server_incarn,
    Environment &_environment)
{
	if (client_ready()) {
		pxfobjplusp->clear_attr_dirty(server_incarn, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
}

//
// access_inval_cache
// This operation is valid for proxy vnodes that supports caching.
//
void
fobj_client_impl::access_inval_cache(uint64_t access_seqnum,
    Environment &_environment)
{
	if (client_ready()) {
		pxfobjplusp->access_inval_cache(access_seqnum, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// Already flushed access cache.
		// Server may not know about that.
		// An orphan request can hit this situation.
		//
}

void
fobj_client_impl::dir_inval_entry(const char *nm,
    Environment &_environment)
{
	if (client_ready()) {
		ASSERT(pxfobjplusp->get_vp()->v_type == VDIR);
		pxdir	*pxdirp = (pxdir *)pxfobjplusp;

		pxdirp->dir_inval_entry(nm, _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// In order to get to this point the directory cannot
		// be in the DNLC cache. So the information has
		// already been purged.
		// This command can legitimately arrive at this point.
		//
}

void
fobj_client_impl::dir_inval_all(Environment &_environment)
{
	if (client_ready()) {
		ASSERT(pxfobjplusp->get_vp()->v_type == VDIR);
		pxdir	*pxdirp = (pxdir *)pxfobjplusp;

		pxdirp->dir_inval_all(_environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// In order to get to this point the directory cannot
		// be in the DNLC cache. So the information has
		// already been purged.
		// This command can legitimately arrive at this point.
		//
}

void
fobj_client_impl::data_write_back_and_delete(uint32_t server_incarnation,
    Environment &_environment)
{
	if (client_ready()) {
		ASSERT(pxfobjplusp->get_vp()->v_type == VREG ||
		    pxfobjplusp->get_vp()->v_type == VSOCK);
		pxreg	*pxregp = (pxreg *)pxfobjplusp;

		pxregp->data_write_back_and_delete(server_incarnation,
		    _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// Already flushed data
		// An orphan request can hit this
		//
}

void
fobj_client_impl::data_write_back_downgrade_read_only(
    uint32_t server_incarnation, Environment &_environment)
{
	if (client_ready()) {
		ASSERT(pxfobjplusp->get_vp()->v_type == VREG ||
		    pxfobjplusp->get_vp()->v_type == VSOCK);
		pxreg	*pxregp = (pxreg *)pxfobjplusp;

		pxregp->data_write_back_downgrade_read_only(server_incarnation,
		    _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// Already flushed data
		// An orphan request can hit this
		//
}

void
fobj_client_impl::data_write_back(Environment &_environment)
{
	if (client_ready()) {
		ASSERT(pxfobjplusp->get_vp()->v_type == VREG ||
		    pxfobjplusp->get_vp()->v_type == VSOCK);
		pxreg	*pxregp = (pxreg *)pxfobjplusp;

		pxregp->data_write_back(_environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// Already flushed data
		// An orphan request can hit this
		//
}

void
fobj_client_impl::data_delete_range(sol::u_offset_t offset,
    uint32_t server_incarnation, Environment &_environment)
{
	if (client_ready()) {
		ASSERT(pxfobjplusp->get_vp()->v_type == VREG ||
		    pxfobjplusp->get_vp()->v_type == VSOCK);
		pxreg	*pxregp = (pxreg *)pxfobjplusp;

		pxregp->data_delete_range(offset, server_incarnation,
		    _environment);
		VN_RELE(pxnode::PXTOV(pxfobjplusp));
	}
		// client not ready
		//
		// Already flushed data
		// An orphan request can hit this
		//
}

//
// data_change_cache_flag - This command can come in at any time
// while the proxy file object exists, regardless as to whether
// the client is holding pages.
//
void
fobj_client_impl::data_change_cache_flag(bool onoff, Environment &_environment)
{
	do {
		if (client_ready()) {
			ASSERT(pxfobjplusp->get_vp()->v_type == VREG);
			pxreg	*pxregp = (pxreg *)pxfobjplusp;

			pxregp->data_change_cache_flag(onoff, _environment);
			VN_RELE(pxnode::PXTOV(pxfobjplusp));
			return;
		}

		//
		// Must wait to see whether the proxy file object goes away.
		//
		state_lock.lock();
		while ((cflags & RELEASE_PENDING) &&
		    !(cflags & DISCONNECTING)) {
			registered_cv.wait(&state_lock);
		}

		if (cflags & DISCONNECTING) {
			//
			// The client will definitely go away
			// Nothing to do.
			//
			return;
		}
		state_lock.unlock();
	} while (true);
}
