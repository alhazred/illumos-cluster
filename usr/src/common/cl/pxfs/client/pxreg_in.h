//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxreg_in.h	1.10	09/02/04 SMI"

inline PXFS_VER::file_ptr
pxreg::getfile()
{
	// This conversion is safe as long as file inherits from fobj.
	return ((PXFS_VER::file_ptr)getfobj());
}

//
// rwlocks_granted is incremented atomically to maintain
// the consistency. This is because decr_ref_lockfree()
// changes the value of rwlocks_granted without holding the
// cache_lock.
//
inline void
pxreg::incr_ref()
{
	ASSERT(rwlocks_granted >= 0);
	cache_lock.lock();
	os::atomic_add_32(&rwlocks_granted, 1);
	cache_lock.unlock();
}

//
// rwlocks_granted is decremented atomically to maintain
// the consistency. This is because decr_ref_lockfree()
// changes the value of rwlocks_granted without holding the
// cache_lock.
//
inline void
pxreg::decr_ref()
{
	ASSERT(rwlocks_granted > 0);
	cache_lock.lock();
	os::atomic_add_32(&rwlocks_granted, -1);
	cache_lock.unlock();
}

//
// This routine does the same as decr_ref() except that
// it decrements rwlocks_granted atomically without holding
// the cache_lock.
//
inline void
pxreg::decr_ref_lockfree()
{
	ASSERT(rwlocks_granted > 0);
	os::atomic_add_32(&rwlocks_granted, -1);
}

//
// We ASSERT on rwlocks_granted for the next two routines because, the
// caller of this should have already incremented the rwlocks_granted.
//
inline void
pxreg::incr_threads_in_reserve_blocks()
{
	ASSERT(rwlocks_granted > 0);
	cache_lock.lock();
	threads_in_reserve_blocks++;
	cache_lock.unlock();
}

inline void
pxreg::decr_threads_in_reserve_blocks()
{
	ASSERT(rwlocks_granted > 0);
	cache_lock.lock();
	threads_in_reserve_blocks--;
	cache_lock.unlock();
}

inline bool
pxreg::has_read_lock()
{
	return (cur_data_token & PXFS_READ_TOKEN);
}

//
// Routine returns the range for which we have locks.
// Callers should have cache lock or rwlock held otherwise
// condition could have changed on return.
//
inline bool
pxreg::has_write_lock()
{
	return (cur_data_token & PXFS_WRITE_TOKEN);
}

//
// Convert Solaris page protection bits to Solaris MC type
//
inline PXFS_VER::acc_rights
pxreg::prot2acc(uint_t prot)
{
	if (prot == 0) {
		return (PXFS_VER::acc_none);
	} else if (!(prot & PROT_WRITE)) {
		return (PXFS_VER::acc_ro);
	} else {
		return (PXFS_VER::acc_rw);
	}
}

//
// Async writes hold the data token lock as readers. We wait for those
// to drain by taking the data_token lock as a writer. Then we need to
// wait for pending aios to signal completion to make sure all
// scheduled i/os have hit the disk.
//
inline void
pxreg::drain_asyncs(bool wait_for_aio)
{
	// Wait for mem_async_writes to drain.
	data_token_lock.wrlock();
	data_token_lock.unlock();

	if (wait_for_aio) {
		aios_pending_lock.lock();
		while (aios_pending > 0) {
			//
			// Wait for pending aio_callbacks to signal
			//
			aios_pending_cv.wait(&aios_pending_lock);
		}
		aios_pending_lock.unlock();
	}
}

//
// Increment pending AIO count for this regular file.
//
inline void
pxreg::increment_aio_pending()
{
	aios_pending_lock.lock();

	ASSERT(aios_pending >= 0);
	ASSERT(aios_pending != INT_MAX);

	aios_pending++;
	aios_pending_lock.unlock();
}

//
// Decrement AIO count for this regular file and signal waiter if the
// count becomes zero.
//
inline void
pxreg::decrement_aio_pending()
{
	aios_pending_lock.lock();

	ASSERT(aios_pending > 0);

	if (--aios_pending == 0) {
		//
		// There can be no waiters or one waiter, since any
		// waiter will have to hold the cache lock.
		//
		aios_pending_cv.signal();
	}
	aios_pending_lock.unlock();
}
