//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FSMGR_CLIENT_IMPL_H
#define	FSMGR_CLIENT_IMPL_H

#pragma ident	"@(#)fsmgr_client_impl.h	1.6	08/05/20 SMI"

#include <sys/vfs.h>

#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)

// Forward declarations.
class pxvfs;

//
// fsmgr_client_impl - used by a pxfs file system server to perform
// operations on a proxy file system client. There is one of these
// objects per proxy file system (pxvfs).
//
class fsmgr_client_impl : public McServerof<PXFS_VER::fsmgr_client> {
public:
	fsmgr_client_impl();
	~fsmgr_client_impl();
	void _unreferenced(unref_t arg);

	//
	// Helper function to set the pxvfsp. This should only be used
	// when establishing the pxvfs/fsmgr_client_impl linkage as part
	// of the mount process.
	//
	void	set_pxvfsp(pxvfs *pxfsp, PXFS_VER::fsmgr_server_ptr servermgrp);

	//
	// Helper function for pxvfs to release the reference to the
	// fsmgr server.
	//
	void	unmount_succeeded();

	// Methods supporting IDL operations for fsmgr_client

	virtual void	sync_filesystem(solobj::cred_ptr credobj,
	    Environment &_environment);

	//
	// This function gets called from the server when the server
	// decides to switch to REDZONE. It resets the local pool of blocks
	// to zero and syncs the dirty pages of this filesystem.
	//
	virtual void	revoke_allocation(solobj::cred_ptr credobj,
	    Environment &_environment);

	//
	// This is called by the server to inform the server status.
	//
	virtual void	inform_server_status(PXFS_VER::server_status_t status,
	    Environment &_environment);

	virtual void	new_file_system_primary(uint32_t server_incn,
	    Environment &_environment);

	virtual void	recover_state(const PXFS_VER::fobj_fid_t &fid,
	    PXFS_VER::recovery_info_out recovery, Environment &env);

	// End of methods supporting IDL operations

private:
	//
	// Pointer to the proxy file system we manage and a lock to
	// prevent races between _unreferenced() and unmount_succeeded().
	// We use a rwlock here instead of mutex because we could have
	// multiple readers ie. multiple syncs (sync_filesystem()), one
	// from a fs_ii::sync_recovery() and another from revoke_allocation()
	// in parallel.
	// Only those routines which modify the value of pxvfsp take the
	// pxvfsp_lock in WRITE mode.
	//
	pxvfs		*pxvfsp;
	os::rwlock_t	pxvfsp_lock;

	// Reference to the fs manager for this pxvfs client.
	PXFS_VER::fsmgr_server_ptr	servermgr;
};

#endif  // FSMGR_CLIENT_IMPL_H
