//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef PXFOBJPLUS_H
#define	PXFOBJPLUS_H

#pragma ident	"@(#)pxfobjplus.h	1.12	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/vfs.h>

#include <h/sol.h>
#include <sys/os.h>
#include <orb/invo/common.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/client/pxfobj.h>
#include <pxfs/client/access_cache.h>

// Forward declaration
class fobj_client_impl;

//
// inactive_list_elem - this class provides a wrapper for _DList::ListElem
// in order to eliminate ambiguity between the multiple _DList::ListElem
// in class pxfobjplus. This particular class supports the ability to
// place the proxy vnode on a list for inactive processing.
//
class inactive_list_elem : public _DList::ListElem {
protected:
	inactive_list_elem(void *);
};

//
// pxfobjplus - this is a parent class for  proxy vnode classes that
// support attribute caching and an access cache.
//
class pxfobjplus :
	public pxfobj,
	public inactive_list_elem
{
	friend class access_cache;
public:
	pxfobjplus(fobj_client_impl *fclientp,
	    vfs *vfsp, PXFS_VER::fobj_ptr fobjp,
	    const PXFS_VER::fobj_info &fobjinfo);

	virtual ~pxfobjplus();

	//
	// Note that getfobjplus() does not do a _duplicate() and the pointer
	// returned should not be released (pxfobj retains ownership).
	// It can be used to get a pointer for doing remote invocations and
	// to compare for equivalence.
	//
	PXFS_VER::fobjplus_ptr	getfobjplus() const;

	void		set_client(fobj_client_impl *fobjclientp);

	void		set_recycled();

	virtual void	become_stale();

	// Methods supporting IDL operations

	// The following support operations for an arbitrary fobj client

	void	get_open_count(uint32_t &count, Environment &_environment);

	void	get_vnode_count(uint32_t &count, Environment &_environment);

	// The following support attribute operations

	void	attr_release(uint64_t attr_seqnum,
	    uint64_t access_seqnum, Environment &_environment);

	void	attr_write_back_and_release(uint64_t attr_seqnum,
	    uint64_t access_seqnum, sol::vattr_t &attributes,
	    Environment &_environment);

	void	attr_write_back_and_downgrade(uint64_t attr_seqnum,
	    uint64_t access_seqnum, sol::vattr_t &attributes,
	    Environment &_environment);

	void	clear_attr_dirty(uint32_t server_incarn,
	    Environment &_environment);

	// The following support access cache operations

	void	access_inval_cache(uint64_t access_seqnum,
	    Environment &_environment);

	// End of methods supporting IDL operations

	//
	// Vnode operations. Only overload operations related to cached info
	//
	virtual int	access(int mode, int flags, cred *credp);

	virtual int	getattr(vattr *vap, int flags, cred *credp);
	virtual int	setattr(vattr *vap, int flags, cred *credp);

	virtual int	fsync(int syncflag, struct cred *credp);

	virtual void	inactive();

	// End of vnode operations

	//
	// Return true if for this file caching info is enabled
	// (i.e., direct I/O is disabled).
	//
	virtual bool	is_cached();

	//
	// Return true if this file can support caching.
	//
	virtual bool	can_cache();

	virtual void	cleanup_proxy_vnode();

	void		install_attr(const sol::vattr_t &attr_,
	    PXFS_VER::attr_rights rights);

	virtual void	install_cachedata_flag(bool cached);

	int		sync_attr();

	virtual void	recover_state(PXFS_VER::recovery_info_out recovery,
	    Environment &env);

protected:
	// Set the mtime for the cached attributes
	void		set_mtime();

	int		clear_suid(cred_t *credp);

	sol::error_t	get_attr_read_token(cred_t *credp);

	sol::error_t	get_attr_write_token(cred_t *credp);

	void		drop_attr_token();

	int		drop_token_with_no_rights(cred_t *credp);

	int		flush_attributes(bool discard, bool sync);

	//
	// Need this to get the file length without the attr lock
	// for writing pages. Ordinarily we don't care about file
	// length. However, when mounting a PxFS filesystem
	// without syncdir we need to pass the file length to page_out so
	// that we'll know the precise number of backing store blocks needed
	// to store pages being written out. Holding the attr lock in putapage
	// is dangerous. It introduces deadlocks.
	//
	u_offset_t	get_file_length();

	//
	// This tells us whether the file size has been modified.
	//
	bool		has_size_changed();

	// Functions to get/set the file size
	u_offset_t	get_length();

	void		set_length(u_offset_t size);

	void		update_length_fields(vattr &_attr);

	//
	// Set the extended file range for cached appends.
	//
	virtual void	set_ext_range(u_offset_t off, size_t len);

private:
	sol::error_t	obtain_token(PXFS_VER::attr_rights req_rights,
	    cred_t *credp);

	int		write_back_attr(int attrflags, bool discard,
			    bool sync, cred_t *credp);

	// Disallowed operations.
	pxfobjplus & operator = (const pxfobjplus &);
	pxfobjplus(const pxfobjplus &);

	// Attribute cache sequence number
	uint64_t	attrib_seqnum;

	//
	// The server can die after the client ships back attributes
	// and before the server processes those attributes. After that
	// event the client does not believe that the client has any
	// dirty attributes while the server never recorded those
	// dirty attributes. The client does the following in order
	// to support recovery operations:
	//
	//	1. Do not destroy the copy of the attributes shipped
	//	back to the server.
	//
	//	2. Record the sequence number of the dirty attributes.
	//
	//	3. Record the bit flag identifying the dirty bits.
	//
	//	4. Clear these fields whenever receiving new attributes
	//	or securely writing attributes to the server in an
	//	invocation request instead of an invocation reply.
	//
	uint64_t	dirty_seqnum;

	//
	// This contains the access cache info for this file
	//
	acache_head	head;

protected:
	//
	// Some types of proxy vnodes cache information.
	// The server uses the fobj_client interface to manage
	// the cached information.
	//
	fobj_client_impl	*clientp;

	// Cached file attributes
	vattr		attr;

	//
	// "low-level" lock; force exclusion between client and server
	// when accessing attributes.
	//
	os::rwlock_t	low_lock;

private:
	//
	// "high-level" lock; force exclusion among threads on this node
	// when accessing attributes.
	//
	os::rwlock_t 	high_lock;

	// Identifies which dirty attributes were last shipped
	uint_t		attr_dirty;

	//
	// Access rights for attribute cache
	// Cached attributes are only valid
	// when client holds either read or write privileges.
	//
	PXFS_VER::attr_rights		access_rights;
};

#include <pxfs/client/pxfobjplus_in.h>

#endif	// PXFOBJPLUS_H
