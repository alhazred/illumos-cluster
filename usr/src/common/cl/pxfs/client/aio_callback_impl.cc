//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)aio_callback_impl.cc	1.11	08/05/20 SMI"

#include <sys/aio_impl.h>
#include <vm/pvn.h>

#include <orb/infrastructure/copy.h>

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/aio_callback_impl.h>
#include <pxfs/client/pxchr.h>
#include <pxfs/client/pxreg.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/bulkio/bulkio_impl.h>

threadpool ha_async_threadpool(true, 2, "haasync thr", 10);

//
// Constructor for raw devices.
//
aio_callback_impl::aio_callback_impl(struct aio_req *aioreq,
	bool ha, ha_async *h, bulkio::in_aio_ptr b)
{
	aio = aioreq;
	reqp = (aio_req_t *)aioreq->aio_private;
	hadev = ha;
	haio = h;
	type_flag = PXFS_AIO;
	_flags = 0;

	//lint -e1061
	_inaioclntobj = bulkio_impl::in_aio_clnt::_duplicate(b);
	//lint +e1061 -e1401
	_pgobj = bulkio::in_pages::_nil();
	_pginobj = bulkio::in_aio_pages::_nil();

	_invo_complete_with_exception = false;
}

//
// Constructor for asynchronous pageout.
//
aio_callback_impl::aio_callback_impl(vnode_t *v, page_t *p,
	u_offset_t off, size_t len, int fl, void *pgo)
{
	_vp = v;
	_pp = p;
	_flags = fl;
	_offset = off;
	_io_len = len;
	type_flag = PXFS_APAGEOUT;
	//
	// _pgobj is necessary since we need to call wait_for_sema
	// on the bulkio object before doing a pvn_write_done().
	//
	_pgobj = bulkio::in_pages::_duplicate((bulkio::in_pages *)pgo);
	_pginobj = bulkio::in_aio_pages::_nil();
	_inaioclntobj = bulkio::in_aio::_nil();

	_invo_complete_with_exception = false;

	// Increment queued AIO count.
	((pxreg *)pxnode::VTOPX(_vp))->increment_aio_pending();

	//
	// Increment i/o count. We must wait for the current count to
	// come below the configured limit if needed.
	//
	pxvfs::VFSTOPXFS(_vp->v_vfsp)->increment_io_pending(true);
}

//
// Constructor for asynchronous read-ahead.
//
aio_callback_impl::aio_callback_impl(vnode_t *v, page_t *p, u_offset_t off,
	size_t len, bulkio::in_aio_pages *pobj)
{
	_vp = v;
	_pp = p;
	_offset = off;
	_io_len = len;
	type_flag = PXFS_APAGEIN;
	//
	// _pginobj is necessary since we need to call release_pages()
	// on the bulkio object before doing a pvn_read_done().
	//
	_pginobj = bulkio::in_aio_pages::_duplicate(pobj);
	_pgobj = bulkio::in_pages::_nil();
	_inaioclntobj = bulkio::in_aio::_nil();

	_invo_complete_with_exception = false;
}
//lint +e1401

aio_callback_impl::~aio_callback_impl()
{
	ASSERT(CORBA::is_nil(_pgobj));
	ASSERT(CORBA::is_nil(_pginobj));
	if (!CORBA::is_nil(_inaioclntobj)) {
		CORBA::release(_inaioclntobj);
		_inaioclntobj = bulkio::in_aio::_nil();
	}
	PXFS_DBPRINTF(
	    PXFS_TRACE_AIO_CALLBACK,
	    PXFS_GREEN,
	    ("deleting the callbackobj \n"));

	//
	// Decrement pending AIO count if this AIO callback was
	// created for a pageout operation.
	//
	if (type_flag == PXFS_APAGEOUT) {
		((pxreg *)pxnode::VTOPX(_vp))->decrement_aio_pending();

		// Update throttling variables.
		pxvfs::VFSTOPXFS(_vp->v_vfsp)->update_throughput((int)_io_len);

		// Decrement pending i/o count.
		pxvfs::VFSTOPXFS(_vp->v_vfsp)->decrement_io_pending();
	}
}
// lint !e1740 some pointers from aio_callback_impl neither freed
// nor zero'ed by destructor

//
// Unreferenced means iodone should have been called
// release the aioobject.
//
void
aio_callback_impl::_unreferenced(unref_t cookie)
{
	if (!_last_unref(cookie)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}

	if (type_flag == PXFS_AIO) {
		aio_unref();
	} else if (type_flag == PXFS_APAGEOUT) {
		apageout_unref();
	} else if (type_flag == PXFS_APAGEIN) {
		apagein_unref();
	} else {
		ASSERT(0);
	}
}

void
aio_callback_impl::apageout_unref()
{
	//
	// Server has died after starting the pageout()
	// request, but before the pageout() could complete.
	// if pp is not null.
	// So mark the pages as paged out with errors.
	//
	if (_pp != NULL) {
		//
		// Before doing pvn_write_done, inform the bulkio object
		// that the pages are being freed so it can ensure there
		// are not further references to these pages.
		//
		bulkio_impl::wait_on_sema(_pgobj);
		PXFS_DBPRINTF(
		    PXFS_TRACE_AIO_CALLBACK,
		    PXFS_GREEN,
		    ("apageout_unref(%p) after wait %p\n",
		    this, _pgobj));

		if ((pxvfs::VFSTOPXFS(_vp->v_vfsp))->is_unmounted()) {
			try_invalidate_pages(_pp, _flags);
		} else {
			pvn_write_done(_pp, B_ERROR | _flags);
		}
	}

	// Release the references,  duplicated in the constructor
	CORBA::release(_pgobj);
	_pgobj = bulkio::in_pages::_nil();
	delete this;
}

void
aio_callback_impl::apagein_unref()
{
	if (_pp != NULL) {
		//
		// Before doing pvn_read_done, inform the bulkio object
		// that the pages are being freed so it can ensure there
		// are not further references to these pages
		//
		bulkio_impl::release_pages(_pginobj);
		pvn_read_done(_pp, B_ERROR | B_READ);

		PXFS_DBPRINTF(
		    PXFS_TRACE_AIO_CALLBACK,
		    PXFS_RED,
		    ("apagein_unref(%p) page not cleared\n",
		    this));
	}
	CORBA::release(_pginobj);
	_pginobj = bulkio::in_aio_pages::_nil();
	delete this;
}

void
aio_callback_impl::aio_unref()
{
	if (aio != NULL) {
		//
		// Looks like the server went away
		// and nobody is going to call us back.
		// So set the error in the buf structure
		// to indicate that the io did not complete
		// and return. In the HA case, retry the
		// async io.
		//
		if (!hadev) {
			//
			// XXX I need to find out the correct error
			// to return in this case.
			//
			struct buf *bp = &reqp->aio_req_buf;
			bp->b_forw = (struct buf *)reqp;
			bp->b_resid = 0;
			bp->b_flags |= B_ERROR;
			bp->b_error = EIO;

			ASSERT(!CORBA::is_nil(_inaioclntobj));
			bulkio_impl::in_aio_clnt::get_impl_obj(
			    _inaioclntobj)->real_destructor();
			aio_done(bp);
		} else {
			//
			// XXX This is a ha device.
			// Will have to retry the io.
			//
			ASSERT(haio != NULL);
			ha_async_threadpool.defer_processing(haio);

			PXFS_DBPRINTF(
			    PXFS_TRACE_AIO_CALLBACK,
			    PXFS_GREEN,
			    ("aio_unref(%p) added %p to haasync list\n",
			    this, haio));
		}
	} else {
		//
		// This is the normal case. Since for a
		// HA device, a ha_async object has already
		// been allocated, it should be deleted.
		//

		// real_destructor() must have been called at by this time.

		if (hadev) {
			delete haio;
		}
	}
	delete this;
}

//
// Aio read is completed on the server side. Store the
// appropriate error flag if necessary. Call aiodone
// so that the aio layer can signal the process on this node.
// Note: The in argument aioobj would have been marshalled
// and thus the uio object of the process would have had
// the proper data. Care has to be taken to duplicate the
// uio object appropriately.
//
void
aio_callback_impl::aio_read_complete(bulkio::in_aio_ptr,
    int32_t berror, Environment &e)
{
	//
	// We wait to be notified that the aioread() invocation has
	// completed. This is to fix bug 4320081. It effectively prevents
	// a race between this callback and the aioread()'s exception handler.
	// The is_orphan() check prevents multiple callbacks from being
	// processed.
	//
	_invo_complete.wait();
	if (e.is_orphan()) {
		// server that sent this callback is dead.
		return;
	}
	if (_invo_complete_with_exception) {
		//
		// aioread() got an exception and that was processed.
		// Therefore abort callback processing.
		//
		return;
	}
	struct buf *bp = &reqp->aio_req_buf;

	bp->b_forw = (struct buf *)reqp;
	bp->b_resid = 0;

	if (berror) {
		bp->b_flags |= B_ERROR;
		bp->b_error = berror;
	} else {
		bp->b_error = 0;
	}

	ASSERT(!CORBA::is_nil(_inaioclntobj));
	bulkio_impl::in_aio_clnt::get_impl_obj(
		    _inaioclntobj)->real_destructor();
	aio_done(bp);
	clear_cbkobj();
}

//
// Aio write is completed on the server. DO the same as for read.
//
void
aio_callback_impl::aio_write_complete(int32_t berror, Environment &e)
{
	_invo_complete.wait();
	if (e.is_orphan()) {
		// Server that sent this callback is dead.
		return;
	}
	if (_invo_complete_with_exception) {
		//
		// aiowrite() got an exception that was processed.
		// Abort callback processing.
		//
		return;
	}
	struct buf *bp = &reqp->aio_req_buf;

	bp->b_forw = (struct buf *)reqp;
	bp->b_resid = 0;

	if (berror) {
		bp->b_flags |= B_ERROR;
		bp->b_error = berror;
	} else {
		bp->b_error = 0;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_AIO_CALLBACK,
	    PXFS_GREEN,
	    ("aio_write_complete(%p) buf %p addr %x REMAPPED %d err %d\n",
	    this, bp, bp->b_un.b_addr, bp->b_flags & B_REMAPPED, bp->b_error));

	ASSERT(!CORBA::is_nil(_inaioclntobj));
	bulkio_impl::in_aio_clnt::get_impl_obj(
		    _inaioclntobj)->real_destructor();
	aio_done(bp);
	clear_cbkobj();
}

void
aio_callback_impl::aio_pageout_complete(uint32_t start_len, uint32_t done_len,
	int32_t error, Environment &e)
{
	_invo_complete.wait();
	if (e.is_orphan()) {
		// the server than sent this callback is dead
		return;
	}
	if (_invo_complete_with_exception) {
		// async_page_out() got an exception. Abort processing.
		return;
	}
	//
	// In a non-HA environment, if the server dies,
	// the client can destroy the cached pages of the
	// vnodes located on the server. So the original
	// page_out() invocation on the client will get a
	// COMM_FAILURE when the client tries to push the pages
	// to the server who has died. At this point, the client
	// will destroy the pages. To prevent a race between the
	// client destroying the pages and the callbacks happening
	// on the io's already started by the server before it died,
	// it is necessary to use apageout_mutex.
	//
	PXFS_DBPRINTF(
	    PXFS_TRACE_AIO_CALLBACK,
	    PXFS_GREEN,
	    ("aio_pageout_complete(%p) off %x len %x err %d\n",
	    this, start_len, done_len, error));

	//
	// Before doing pvn_write_done, inform the bulkio object
	// that the pages are being freed so it can ensure there
	// are not further references to these pages. In particular,
	// when doing writes, the bulkio object hands a pointer to the
	// pages to the transport (to avoid a copy). We have to wait
	// till the transport/drivers are done referencing the pages
	// before we free them.
	//
	bulkio_impl::wait_on_sema(_pgobj);

	//
	// Case #1: server did not fragment the io. Happens 95%
	// of the time. Not expecting any more pageout_completes.
	//
	if ((size_t)done_len == _io_len) {
		apageout_mutex.lock();
		if (error && (pxvfs::VFSTOPXFS(_vp->v_vfsp))->is_unmounted()) {
			try_invalidate_pages(_pp, _flags);
		} else {
			pvn_write_done(_pp, ((error) ? B_ERROR : 0) | _flags);
		}
		_pp = NULL;
		apageout_mutex.unlock();
		return;
	}

	//
	// Case #2: This is the first fragment. Remove the corresponding
	// pages from the list.
	//
	if (start_len == 0) {
		apageout_mutex.lock();
		page_t *head = _pp, *tail = NULL;
		page_list_break(&head, &tail, btopr((size_t)done_len));
		if (error && (pxvfs::VFSTOPXFS(_vp->v_vfsp))->is_unmounted()) {
			try_invalidate_pages(head, _flags);
		} else {
			pvn_write_done(head, ((error) ? B_ERROR : 0) | _flags);
		}
		_pp = tail;
		apageout_mutex.unlock();
		return;
	}

	// For all the remaining fragments.

	page_t *p, *written = NULL;

	apageout_mutex.lock();
	for (uint32_t i = 0; i < done_len; i = i + (uint32_t)PAGESIZE) {
		p = page_find(_vp, _offset + start_len + i);
		ASSERT(p != NULL);
		if (_pp != NULL) {
			//
			// In case of local pageouts, the
			// evn_ufs::do_ufs_pageio will do
			// a pagelist break. So _pp will
			// point to only the first fragment.
			// This is the only scenario where
			// _pp can become null before all
			// the fragments are paged out.
			//
			page_sub(&_pp, p);
		}
		if (_pp == NULL)
			PXFS_DBPRINTF(
			    PXFS_TRACE_AIO_CALLBACK,
			    PXFS_RED,
			    ("aio_pageout_complete(%p) PP NULL i %x done %x\n",
			    this, i, done_len));

		page_add(&written, p);
	}
	apageout_mutex.unlock();

	if (error && (pxvfs::VFSTOPXFS(_vp->v_vfsp))->is_unmounted()) {
		try_invalidate_pages(written, _flags);
	} else {
		pvn_write_done(written, ((error) ? B_ERROR : 0) | _flags);
	}
}

page_t *
aio_callback_impl::get_pages()
{
	//
	// This is called from mem_async::page_out() if it gets a
	// COMM_FAILURE exception. The server might have started and
	// completed io's for some pageout fragments.
	//
	page_t *rp;
	apageout_mutex.lock();
	rp = _pp;
	_pp = NULL;
	apageout_mutex.unlock();
	return (rp);
}

// static
void
aio_callback_impl::try_invalidate_pages(page_t *page_list, int flags)
{
	page_t *plist = page_list;

	while (plist != NULL) {
		page_t *pp = plist;
		page_sub(&plist, pp);

		// try to get an exclusive lock on the page.
		if (page_tryupgrade(pp)) {
			pvn_write_done(pp,
			    B_INVAL | B_FORCE | B_ERROR | flags);
		} else {
			//
			// Could not get an exclusive lock.
			// no problem. pageout daemon will try
			// to push out the pages again next time.
			// might have better luck with this page
			// next time.
			//
			pvn_write_done(pp, B_ERROR | flags);
			PXFS_DBPRINTF(
			    PXFS_TRACE_AIO_CALLBACK,
			    PXFS_RED,
			    ("try_invalidate page "
			    "couldn't get exclusive lock\n"));
		}
	}
}

void
aio_callback_impl::aio_pagein_complete(bulkio::in_aio_pages_ptr,
    const bulkio::file_hole_list &fdh, int32_t error, Environment &e)
{
	//
	// The synchronization ensures that we process only one
	// aio_pagein_complete(). Two callbacks may arrive if
	// (1) The first pagein request completes on the server
	// and a callback is sent.
	// and (2) The pagein request's reply doesn't arrive because
	// it isn't processed fast enough on the server and the server
	// dies.
	// (1) + (2) can be simulated by a faultpoint.
	//
	_invo_complete.wait();
	if (e.is_orphan()) {
		// The server that sent this callback is dead.
		PXFS_DBPRINTF(
		    PXFS_TRACE_AIO_CALLBACK,
		    PXFS_RED,
		    ("aio_pagein_complete(%p) orphan err %d pp %p\n",
		    this, error, _pp));
		return;
	}

	if (_invo_complete_with_exception) {
		// The async_page_in() got an exception.
		PXFS_DBPRINTF(
		    PXFS_TRACE_AIO_CALLBACK,
		    PXFS_RED,
		    ("aio_pagein_complete(%p) exception err %d pp %p\n",
		    this, error, _pp));
		return;
	}

	// Must have specified a page for reading
	ASSERT(_pp);

	bulkio::file_hole	*fhl = fdh->buffer();
	uint_t			cnt = fdh->length();
	page_t			*fhp = _pp;

	PXFS_DBPRINTF(
	    PXFS_TRACE_AIO_CALLBACK,
	    PXFS_GREEN,
	    ("aio_pagein_complete(%p) hole count %d\n",
	    this, cnt));

	//
	// This code tries to zero pages that have been marked
	// as holes. Note that the hole length may be longer or
	// shorter than a page. The first loop goes over the hole
	// list, the second over all the pages lists.
	//
	for (uint_t i = 0; error == 0 && i < cnt; i++) {
		size_t		len = fhl[i].len;
		u_offset_t	off = fhl[i].off;
		ASSERT(fhp != NULL);
		PXFS_DBPRINTF(
		    PXFS_TRACE_AIO_CALLBACK,
		    PXFS_GREEN,
		    ("aio_pagein_complete(%p) hole off %llx len %lx\n",
		    this, off, len));

		do {
			ASSERT(off >= fhp->p_offset);

			if (off >= fhp->p_offset + PAGESIZE) {
				continue;
			}
			fhp->p_fsdata |= pxreg::PXFS_HOLE;

			uint_t	zerolen = (uint_t)MIN(PAGESIZE, len);

			ASSERT(zerolen != 0);
			ASSERT(zerolen <= PAGESIZE);

			PXFS_DBPRINTF(
			    PXFS_TRACE_AIO_CALLBACK,
			    PXFS_GREEN,
			    ("aio_pagein_complete(%p) zero len po %x "
			    "ao %llx len %lx\n",
			    this, (unsigned long)off & PAGEOFFSET,
			    off, zerolen));

			pagezero(fhp,
			    (uint_t)((unsigned long)off & PAGEOFFSET),
			    zerolen);

			len -= zerolen;
			off += zerolen;

			if (len == 0) {
				break;
			}
		} while ((fhp = page_list_next(fhp)) != NULL);

		if (fhp == NULL) {
			break;
		}
	}

	//
	// Before doing pvn_read_done, inform the bulkio object
	// that the pages are being freed so it can ensure there
	// are no further references to these pages.
	//
	bulkio_impl::release_pages(_pginobj);

	pvn_read_done(_pp, B_READ | (error ? B_ERROR : 0));
	_pp = NULL;
}

//
// ha_async methods
//

//
// This portion of the code is executed only in case of
// a failover of a HA device.
//
ha_async::ha_async(struct aio_req *r, cred_t *cre, bulkio::in_aio_ptr b,
	void *px, int flag, sol::nodeid_t nid, sol::pid_t pd)
{
	aiop = r;
	cred_p = cre;
	crhold(cred_p);
	bulkioobj = bulkio::in_aio::_duplicate(b);
	pxch = px;
	iswrite = flag;
	nodeid = nid;
	pid = pd;
}

ha_async::~ha_async()
{
	crfree(cred_p);
	ASSERT(!CORBA::is_nil(bulkioobj));
	CORBA::release(bulkioobj);
	bulkioobj = bulkio::in_aio::_nil();
}

//
// This routine is called from threadpool::defer_processing().
// This routine will restart the async io on the new primary.
//
void
ha_async::execute()
{
	pxchr *p;
	p = (pxchr *)pxch;

	// Set the context in which the aio request takes place.
	copy_args args(nodeid, pid);
	copy::setcontext(&args);

	//
	// There is a complicated reason to do aio_cancel() here.
	// The primary has died at this point. So the io is being
	// retried. We had locked the pages in the bulkio layer.
	// Since we will not be re-using the bulkio object, lets
	// unlock the pages by doing a aio_cancel() and restart()
	// the call afresh.
	//

	ASSERT(!CORBA::is_nil(bulkioobj));
	PXFS_DBPRINTF(
	    PXFS_TRACE_AIO_CALLBACK,
	    PXFS_GREEN,
	    ("ha_async:execute aio_cancel path\n"));

	bulkio_impl::in_aio_clnt::get_impl_obj(
		    bulkioobj)->real_destructor();
	bulkio_impl::aio_cancel((bulkio::in_aio *)bulkioobj);

	// Retrying the asynchronous io call again.
	int result = p->pxfs_aiorw(aiop, cred_p, iswrite, true);

	//
	// The return from the call is ignored. This is because
	// we have lost the orignal user context of the aio.
	//
	PXFS_DBPRINTF(
	    PXFS_TRACE_AIO_CALLBACK,
	    PXFS_GREEN,
	    ("ha_async:execute result %d\n", result));

	copy::setcontext(NULL);
	delete this;
}
