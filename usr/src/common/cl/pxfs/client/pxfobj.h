//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef PXFOBJ_H
#define	PXFOBJ_H

#pragma ident	"@(#)pxfobj.h	1.12	08/05/20 SMI"

#include <sys/sol_version.h>
#include <sys/list_def.h>

#include "../version.h"
#include <pxfs/client/pxnode.h>

#if SOL_VERSION >= __s9
#define	PSARC_2001_100
#endif

// Forward declarations.
class pxfobj;

// Type for list of pxfobj objects.
typedef IntrList<pxfobj, _DList> pxfobj_list_t;

//
// XXX For performance, we may want to define separate linked lists for
// finding a pxfobj by fobj and by fobjid. Currently, there is only one list.
//

//
// pxfobj - this is the parent class for all proxy vnodes.
//
// The default behavior for proxy vnodes is to contact the server for
// attributes and access permission.
//
// Proxy Vnode Reference Counting - pxfs uses the standard vnode reference count
// field v_count. The system does not destroy the proxy vnode until this count
// goes to zero (Note that there are other conditions). This count should
// only be incremented under one of the following conditions:
//
//	1) While holding the pxfobj_hash bucket lock.
//
//	2) While this thread has incremented the count,
//	such as for a thread queueing asynchronous I/O.
//
//	3) While holding the state_lock and the state
//	is not PX_STALE.
//
//	4) The root directory is treated specially,
//	because of when it is created.
//
class pxfobj :
	public pxnode,
	public _DList::ListElem
{
public:
	pxfobj(vfs *vfsp, PXFS_VER::fobj_ptr fobjp,
	    const PXFS_VER::fobj_info &fobjinfo);

	virtual ~pxfobj();

	//
	// Accessor functions.
	// Note that getfobj() does not do a _duplicate() and the pointer
	// returned should not be released (pxfobj retains ownership).
	// It can be used to get a pointer for doing remote invocations and
	// to compare for equivalence.
	//
	PXFS_VER::fobj_ptr	getfobj() const;

	// Functions used by pxvfs.
	const fid_t	*get_fidp() const;
	void		set_fid(uint_t fidlen, const char *fiddata);

	ushort_t	get_pxflags();

	void		set_inhashtable();
	void		not_inhashtable();
	bool		is_inhashtable() const;

	bool		is_inactive() const;

	void		set_stale();
	bool		is_stale() const;

	virtual void	become_stale();

	// Convert portable packed vnodes into proxies and vice versa.
	static int	pack_vnode(vnode_t *vp, PXFS_VER::pvnode &pvnode);
	static vnode_t	*unpack_vnode(const PXFS_VER::pvnode &pvnode);

	//
	// Vnode operations.
	//
	virtual int	open(vnode **vpp, int flag, cred *cr);
	virtual int	close(int flag, int count, offset_t offset, cred *cr);
	virtual int	fsync(int syncflag, struct cred *cr);
	virtual int	access(int mode, int flags, cred *cr);
	virtual void	inactive();

	virtual int	read(uio *uiop, int ioflag, cred *cr);
	virtual int	write(uio *uiop, int ioflag, cred *cr);

	virtual int	vop_fid(struct fid *ufidp);

	virtual void	rwlock(int write_lock);
	virtual void	rwunlock(int write_lock);

	virtual int	ioctl(int cmd, intptr_t arg, int flag, struct cred *cr,
	    int *rvalp);

	virtual int	getattr(vattr *vap, int flags, cred *cr);
	virtual int	setattr(vattr *vap, int flags, cred *cr);

	virtual int	map(offset_t off, as *as, caddr_t *addrp, size_t len,
		uchar_t prot, uchar_t maxprot, uint_t flags, cred *cr);

	virtual int	addmap(offset_t off, as *as, caddr_t addr, size_t len,
		uchar_t prot, uchar_t maxprot, uint_t flags, cred *cr);

	virtual int	delmap(offset_t off, as *as, caddr_t addr, size_t len,
		uint_t prot, uint_t maxprot, uint_t flags, cred *cr);

	virtual int	getpage(offset_t off, size_t len, uint_t *protp,
	    page_t *plarr[], size_t plsz, seg *seg, caddr_t addr, seg_rw rw,
	    cred *cr);

	virtual int	putpage(offset_t off, size_t len, int flags, cred *cr);

	virtual int	create(char *name, vattr *vap, vcexcl excl, int mode,
	    vnode **vpp, cred *cr, int flag);

	virtual int	remove(char *nm, cred *cr);

	virtual int	lookup(char *nm, vnode **vpp, pathname *pnp, int flags,
	    vnode *rdir, cred *cr);

	virtual int	mkdir(char *nm, vattr *vap, vnode **vpp, cred *cr);
	virtual int	rmdir(char *nm, vnode *cdir, cred *cr);
	virtual int	readdir(uio *uiop, cred *cr, int &eof);

	virtual int	symlink(char *nm, vattr *vap, char *target, cred *cr);
	virtual int	readlink(uio *uiop, cred *cr);
	virtual int	link(vnode *svp, char *tnm, cred *cr);

	virtual int	rename(char *oldnm, vnode *newdvp, char *newnm,
	    cred *cr);

	virtual int	seek(offset_t ooff, offset_t *noffp);

#ifdef PSARC_2001_100 /* new s9 frlock interface */
	virtual int	frlock(int cmd, struct flock64 *bfp, int flag,
	    offset_t offset, flk_callback_t *, cred *cr);
#else
	virtual int	frlock(int cmd, struct flock64 *bfp, int flag,
	    offset_t offset, cred *cr);
#endif
	//
	// Effects: File share reservations are an advisory form of access
	//   control among cooperating processes, on both local and remote
	//   machines.  They are most often used by DOS or Windows emulators
	//   and DOS-based NFS clients.  Native Unix versions of DOS or
	//   Windows applications may also choose to use this form of access
	//   control.
	// The cred_t field is added to implement the client-side shared
	// locking in NFSv4 (PSARC/2003/049). Though, the underlying file
	// system may not implement shared locking, the cred_t is
	// passed to the server for use in future.
	// Note: struct shrlock is defined in sys/share.h.
	//
	virtual int	vop_shrlock(int cmd, struct shrlock *bfp,
	    int flag, cred *cr);

	virtual int	space(int cmd, struct flock64 *bfp, int flag,
	    offset_t offset, cred *cr);

	virtual int	realvp(vnode** vpp);

	virtual int	pathconf(int cmd, ulong_t *valp, cred *cr);

	virtual void	dispose(page_t *pp, int fl, int dn, cred *cr);

	virtual int	setsecattr(vsecattr_t *vsap, int flag, cred *cr);
	virtual int	getsecattr(vsecattr_t *vsap, int flag, cred *cr);

	// End of vnode operations

	//
	// Return true if for this file caching info is enabled
	// (including direct I/O is disabled).
	//
	virtual bool	is_cached();

	//
	// Return true if this file can support caching.
	//
	virtual bool	can_cache();

	virtual void	cleanup_proxy_vnode();

	//
	// Called to sync dirty data to server.
	// pxreg (regular files and sockets) overrides this method.
	//
	virtual int	sync_file();
	virtual int	sync_file_revoke();


private:
	// Helper function that performs the UFS _FIOIO ioctl.
	int	ioctl_fioio(intptr_t arg, int flag, struct cred *cr);

	// Disallowed operations.
	pxfobj & operator = (const pxfobj &);
	pxfobj(const pxfobj &);

	fid_t		*fidp;

protected:
	// Server object reference
	PXFS_VER::fobj_ptr	fobj_obj;

	// Protects pxflags
	os::mutex_t	state_lock;

	//
	// Definition of flags for pxflags. Some flags are used by derived
	// classes. They are defined here to reduce memory overhead.
	//
	enum {
		// Flags used by pxfobj.

		//
		// Object is inactive (stale).
		// Contains no dirty information,
		// which means that all information is on the server
		//
		PX_STALE		= 0x01,

		//
		// Set if there have ever been any file or share locks.
		// It is used to reduce calls to the server on close().
		// Protected by state_lock;
		//
		PX_HAS_LOCKS		= 0x02,

		// Flags used by pxfobjplus
		PX_CACHE_ATTRIBUTES	= 0x04,

		// Flags used by pxvfs.
		PX_FOBJHASH	= 0x08,	// object is in pxfobj hashtable
		PX_INACTIVELIST = 0x10,	// object is on inactive_list

		// Flags used by pxreg.
		PX_CACHE_DATA	= 0x20,

		// Flag used by pxfobjplus
		// pxobj became reused during deactivation
		PX_RECYCLED	= 0x40
	};

	// Derived classes can use flags (to save memory).
	ushort_t	pxflags;
};

#include <pxfs/client/pxfobj_in.h>

#endif	// PXFOBJ_H
