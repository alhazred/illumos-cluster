//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pxdir.cc - proxy vnode for directories (i.e., VDIR vnodes).
//

#pragma ident	"@(#)pxdir.cc	1.18	09/01/13 SMI"

#include <sys/types.h>
#include <sys/t_lock.h>
#include <sys/param.h>
#include <sys/uio.h>
#include <sys/dnlc.h>
#include <sys/fs/snode.h>
#include <sys/debug.h>

#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>
#include <orb/monitor/monitor.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/client/pxdir.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/fobj_client_impl.h>

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

//
// This is the number of blocks that could potentially be consumed by the
// creation of a file. We are assuming that we would need 2 blocks for every
// new file created. The assumption is that creation of this file could spill
// into a new block or an indirect block and we try to accomodate this fact by
// reserving these blocks. This is an over estimated value but, it is safe.
//
const int blks_per_file_entry = 3;

// Dummy entry for negative name lookups.
vnode_t		*pxdir::noentp = NULL;


pxdir::pxdir(fobj_client_impl *fclientp,
    vfs *vfsp, PXFS_VER::unixdir_ptr udp, const PXFS_VER::fobj_info &fobjinfo) :
	pxfobjplus(fclientp, vfsp, udp, fobjinfo),
	inval_seqnum(0),
	entry_seqnum(0),
	end_off(0)
{
}

//
// destructor - the destructor is virtual.
// This makes the name visible to debuggers.
//
pxdir::~pxdir()
{
}

//
// lookup - the name in the local node's DNLC first,
// and then try the lookup on the PXFS server.
// Return the entry when found or ENOENT.
// Cache a successful name lookup.
//
int
pxdir::lookup(char *namep, vnode **vnodepp, pathname *, int, vnode *,
    cred *credp)
{
	//
	// First, check to see if the lookup is for the current directory.
	// An empty name (character string "") or "."
	// represent the current directory.
	// These shortcuts avoid DNLC lookups
	// and trips to the PXFS server.
	//
	if (((namep[0] == '.') && (namep[1] == '\0')) || (namep[0] == '\0')) {
		//
		// The current directory is already in use.
		// So the reference count must be non-zero.
		//
		ASSERT(get_vp()->v_count != 0);
		VN_HOLD(pxnode::PXTOV(this));

		*vnodepp = pxnode::PXTOV(this);
		return (0);
	}

	uint_t	orig_seqnum;
	int	err;

	if (lc_lookup(namep, vnodepp, orig_seqnum)) {
		// The local DNLC has the information in its cache.

		// Check access permissions before returning any other info.
#if	SOL_VERSION >= __s11
		err = VOP_ACCESS(get_vp(), VEXEC, 0, credp, NULL);
#else
		err = VOP_ACCESS(get_vp(), VEXEC, 0, credp);
#endif
		if (err == 0 && *vnodepp == noentp) {
			err = ENOENT;
		}
		if (err) {
			VN_RELE(*vnodepp);
			*vnodepp = NULL;
		} else {
			ASSERT(*vnodepp != NULL);
		}
		return (err);
	}

	solobj::cred_var	credobj = solobj_impl::conv(credp);
	PXFS_VER::fobj_var	fobj_v;
	PXFS_VER::fobj_info	fobjinfo;
	PXFS_VER::bind_info	binfo;
	Environment		e;
	sol::error_t		error;
	pxvfs			*pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);
	uint32_t		server_incn_orig = pxvfsp->get_server_incn();

	//
	// If the new client is not used, unreference will clean it up.
	//
	fobj_client_impl	*client1p = new fobj_client_impl;
	fobj_client_impl	*client2p;

	PXFS_VER::fobj_client_ptr	client1_p = client1p->get_objref();

	PXFS_VER::fobj_client_ptr	client2_p =
	    PXFS_VER::fobj_client::_nil();

	// Ask the server to lookup the file
	getunixdir()->lookup(namep, fobj_v, fobjinfo, binfo,
	    client1_p, client2_p, credobj, e);

	error = pxfslib::get_err(e);

	if (error) {
		if (error == ENOENT) {
			//
			// Cache negative name lookups.
			// Only cache the entry if there were no invalidations
			// and no entry exists in the cache (in other words,
			// don't replace a valid entry with an invalid one).
			//
			data_lock.wrlock();
			if (orig_seqnum == inval_seqnum) {
				entry_seqnum++;


				//
				// OS_VERSION_DEPENDENCY:
				// NOCRED is defined for Solaris 8 and
				// Solaris 9 but not defined for Solaris 10.
				//
#if (!defined(NOCRED))
				dnlc_enter(get_vp(), namep, noentp);
#else
				dnlc_enter(get_vp(), namep, noentp, NOCRED);
#endif
			}
			data_lock.unlock();
		}
	} else if (CORBA::is_nil(client2_p) || client1_p->_equiv(client2_p)) {
		//
		// This means we are creating a new proxy vnode
		//
		if (CORBA::is_nil(client2_p)) {
			//
			// Client side caching is not used
			//
			client1p = NULL;

			*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
			    get_pxfobj(fobj_v, fobjinfo, &binfo, client1p);
			if (*vnodepp == NULL) {
				ASSERT(pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
				    is_unmounted());
				error = EIO;
			} else {
				// Enter new proxy vnode into DNLC
				lc_enter(namep, *vnodepp, orig_seqnum, NONE);
			}
		} else {
			//
			// Client side caching is used
			//

			//
			// Fault point for testing deactivation collision
			//
			// @@@ FAULT_POINT_LOOKUP_CLIENT_COLLISION_2

			// Do not allow the server to change while
			// in the middle of creating a proxy vnode
			//
			pxvfsp->server_incn_lock.rdlock();
			if (server_incn_orig == pxvfsp->get_server_incn()) {
				*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
				    get_pxfobj(fobj_v, fobjinfo, &binfo,
				    client1p);
				pxvfsp->server_incn_lock.unlock();
				if (*vnodepp == NULL) {
					ASSERT(pxvfs::VFSTOPXFS(get_vp()->
					    v_vfsp)->is_unmounted());
					error = EIO;
				} else {
					// Enter new proxy vnode into DNLC
					lc_enter(namep, *vnodepp, orig_seqnum,
					    NONE);
				}
			} else {
				//
				// The registration was orphanned by
				// either a failover or switchover
				//
				pxvfsp->server_incn_lock.unlock();

				error = connect_again(namep, vnodepp,
				    orig_seqnum, fobj_v, fobjinfo, binfo,
				    client1p, client1_p, NONE, e);
			}
		}
	} else {
		//
		// There already is a proxy vnode
		//
		client2p = (fobj_client_impl *)
		    (client2_p->_handler()->get_cookie());

		//
		// Fault point for testing deactivation collision
		//
		// @@@ FAULT_POINT_LOOKUP_CLIENT_COLLISION_1

		//
		// The current client may not be ready
		//
		if (!client2p->wait_till_ok()) {
			//
			// The current client is going away
			//
			CORBA::release(client2_p);
			client2_p = PXFS_VER::fobj_client::_nil();

			error = connect_again(namep, vnodepp, orig_seqnum,
			    fobj_v, fobjinfo, binfo, client1p, client1_p,
			    NONE, e);
		} else {
			//
			// The existing client has completed initialization
			// and has a hold placed upon it.
			//
			*vnodepp = pxnode::PXTOV(client2p->get_pxfobjplusp());

			//
			// The DNLC lookup failed.
			// This could have happened for either:
			//	1) This is the first lookup for ".."
			//	or
			//	2) The DNLC entry was purged
			//
			// Enter proxy vnode into DNLC
			//
			lc_enter(namep, *vnodepp, orig_seqnum, NONE);
		}
	}
	CORBA::release(client1_p);
	CORBA::release(client2_p);

	return (error);
}

//
// create - this method creates a file object
//
int
pxdir::create(char *namep, vattr *vap, vcexcl excl, int mode, vnode **vnodepp,
    cred *credp, int flag)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	PXFS_VER::fobj_var	fobj_v;
	PXFS_VER::fobj_info	fobjinfo;
	PXFS_VER::bind_info	binfo;
	Environment		e;
	sol::error_t		error;
//	uint_t			orig_seqnum = inval_seqnum;
	uint32_t		orig_seqnum = current_seqnum();
	pxvfs			*pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);
	uint32_t		server_incn_orig = pxvfsp->get_server_incn();
	int			err = 0;

	//
	// Account for the space needed for this file/directory.
	//
	if (pxvfsp->fastwrite_enabled()) {
		(void) pxvfsp->reserve_blocks(blks_per_file_entry, false, err);
		if (err != 0) {
			return (err);
		}
	}

	//
	// If the new client is not used, unreference will clean it up.
	//
	fobj_client_impl	*client1p = new fobj_client_impl;
	fobj_client_impl	*client2p;

	PXFS_VER::fobj_client_ptr	client1_p = client1p->get_objref();

	PXFS_VER::fobj_client_ptr	client2_p =
	    PXFS_VER::fobj_client::_nil();

	getunixdir()->cascaded_create_fobj(namep, conv(*vap), conv(excl), mode,
	    fobj_v, fobjinfo, binfo, client1_p, client2_p, credobj,
	    (int32_t)flag, e);
	error = pxfslib::get_err(e);
	if (error) {
		CORBA::release(client1_p);
		CORBA::release(client2_p);
		return (error);
	}
	//
	// VOP_CREATE() can succeed if the file already exists.
	//
	if (CORBA::is_nil(client2_p) || client1_p->_equiv(client2_p)) {
		//
		// This means we are creating a new proxy vnode
		//
		if (CORBA::is_nil(client2_p)) {
			//
			// Client side caching is not used
			//
			client1p = NULL;

			*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
			    get_pxfobj(fobj_v, fobjinfo, &binfo, client1p);
			if (*vnodepp == NULL) {
				ASSERT(pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
				    is_unmounted());
				error = EIO;
			} else {
				// Enter new proxy vnode into DNLC
				lc_enter(namep, *vnodepp, orig_seqnum, DIR);
			}
		} else {
			//
			// Client side caching is used
			//
			// Do not allow the server to change while
			// in the middle of creating a proxy vnode
			//
			pxvfsp->server_incn_lock.rdlock();
			if (server_incn_orig == pxvfsp->get_server_incn()) {
				*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
				    get_pxfobj(fobj_v, fobjinfo, &binfo,
				    client1p);
				pxvfsp->server_incn_lock.unlock();
				if (*vnodepp == NULL) {
					ASSERT(pxvfs::VFSTOPXFS(get_vp()->
					    v_vfsp)->is_unmounted());
					error = EIO;
				} else {
					// Enter new proxy vnode into DNLC
					lc_enter(namep, *vnodepp, orig_seqnum,
					    DIR);
				}
			} else {
				//
				// The registration was orphanned by
				// either a failover or switchover
				//
				pxvfsp->server_incn_lock.unlock();

				error = connect_again(namep, vnodepp,
				    orig_seqnum, fobj_v, fobjinfo, binfo,
				    client1p, client1_p, DIR, e);
			}
		}
	} else {
		//
		// There already is a proxy vnode
		//
		client2p = (fobj_client_impl *)
		    (client2_p->_handler()->get_cookie());

		//
		// The current client may not be ready
		//
		if (!client2p->wait_till_ok()) {
			//
			// The current client is going away
			//
			CORBA::release(client2_p);
			client2_p = PXFS_VER::fobj_client::_nil();

			error = connect_again(namep, vnodepp, orig_seqnum,
			    fobj_v, fobjinfo, binfo, client1p, client1_p,
			    DIR, e);
		} else {
			//
			// The existing client has completed initialization
			// and has a hold placed upon it.
			//
			*vnodepp = pxnode::PXTOV(client2p->get_pxfobjplusp());

			// Enter proxy vnode into DNLC
			lc_enter(namep, *vnodepp, orig_seqnum, DIR);
		}
	}
	CORBA::release(client1_p);
	CORBA::release(client2_p);
	return (error);
}

//
// remove - remove the file from the directory.
// If there are no other references to the file, destroy the file.
//
int
pxdir::remove(char *namep, cred *credp)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	Environment		e;
	uint_t			orig_seqnum;

	//
	// The server expects us to remove our own cache entry.
	//
	orig_seqnum = lc_remove(namep);

	getunixdir()->remove_fobj(namep, credobj, e);

	//
	// Remove the file name to vnode entry,
	// if any such entry was added by another thread.
	//
	lc_check_remove(namep, orig_seqnum);
	return (pxfslib::get_err(e));
}

//
// mkdir - create a directory
//
int
pxdir::mkdir(char *namep, vattr *vap, vnode **vnodepp, cred *credp)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	PXFS_VER::fobj_var	fobj_v;
	PXFS_VER::fobj_info	fobjinfo;
	PXFS_VER::bind_info	binfo;
	Environment		e;
	sol::error_t		error;
//	uint_t			orig_seqnum = inval_seqnum;
	uint32_t		orig_seqnum = current_seqnum();
	pxvfs			*pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);
	uint32_t		server_incn_orig = pxvfsp->get_server_incn();
	int 			err = 0;

	//
	// Account for the space needed for this file/directory.
	//
	if (pxvfsp->fastwrite_enabled()) {
		(void) pxvfsp->reserve_blocks(blks_per_file_entry, false, err);
		if (err != 0) {
			return (err);
		}
	}

	//
	// If the new client is not used, unreference will clean it up.
	//
	fobj_client_impl	*client1p = new fobj_client_impl;
	fobj_client_impl	*client2p;

	PXFS_VER::fobj_client_ptr	client1_p = client1p->get_objref();

	PXFS_VER::fobj_client_ptr	client2_p =
	    PXFS_VER::fobj_client::_nil();

	getunixdir()->create_dir(namep, conv(*vap), fobj_v, fobjinfo,
	    binfo, client1_p, client2_p, credobj, e);
	error = pxfslib::get_err(e);

	//
	// Directory creation returns an error
	// if the directory already exists.
	//
	if (error == 0) {
		//
		// A directory supports client side caching,
		// and thus needs a fobj_client.
		//
		ASSERT(!CORBA::is_nil(client2_p));

		if (client1_p->_equiv(client2_p)) {
			//
			// This means that we create a new proxy vnode
			// for the newly created directory.
			//
			// Do not allow the server to change while
			// in the middle of creating a proxy vnode
			//
			pxvfsp->server_incn_lock.rdlock();
			if (server_incn_orig == pxvfsp->get_server_incn()) {
				*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
				    get_pxfobj(fobj_v, fobjinfo, &binfo,
				    client1p);
				pxvfsp->server_incn_lock.unlock();
				if (*vnodepp == NULL) {
					ASSERT(pxvfs::VFSTOPXFS(get_vp()->
					    v_vfsp)->is_unmounted());
					error = EIO;
				} else {
					// Enter new proxy vnode into DNLC
					lc_enter(namep, *vnodepp, orig_seqnum,
					    DIR);
				}
			} else {
				//
				// The registration was orphanned by
				// either a failover or switchover
				//
				pxvfsp->server_incn_lock.unlock();

				error = connect_again(namep, vnodepp,
				    orig_seqnum, fobj_v, fobjinfo, binfo,
				    client1p, client1_p, DIR, e);
			}
		} else {
			//
			// After a node failure someone else could lookup
			// the newly created directory before the retry
			// returns the successful creation reply.
			// In this case, we use the already existing vnode
			//
			client2p = (fobj_client_impl *)
			    (client2_p->_handler()->get_cookie());

			//
			// The current client may not be ready
			//
			if (!client2p->wait_till_ok()) {
				//
				// The current client is going away
				//
				CORBA::release(client2_p);
				client2_p = PXFS_VER::fobj_client::_nil();

				error = connect_again(namep, vnodepp,
				    orig_seqnum, fobj_v, fobjinfo, binfo,
				    client1p, client1_p, DIR, e);
			} else {
				//
				// Existing client has completed initialization
				// and has a hold placed upon it.
				//
				*vnodepp =
				    pxnode::PXTOV(client2p->get_pxfobjplusp());

				// Enter proxy vnode into DNLC
				lc_enter(namep, *vnodepp, orig_seqnum, DIR);
			}
		}
	}
	CORBA::release(client1_p);
	CORBA::release(client2_p);
	return (error);
}

int
pxdir::rmdir(char *nm, vnode *cdir, cred *credp)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	PXFS_VER::unixdir_ptr	udp;
	Environment		e;

	//
	// If the current directory is not in the same file system as this
	// directory, we pass nil since the current directory CORBA object
	// can't be converted into a local vnode pointer on the PXFS server
	// for the VOP_RMDIR() call to the underlying file system.
	// XXX current working directory doesn't have to be a PXFS directory
	// until after we have global root file system.
	//
	udp = (cdir->v_vfsp != get_vp()->v_vfsp ||
	    (cdir->v_flag & VPXFS) == 0) ?
	    PXFS_VER::unixdir::_nil() : ((pxdir *)VTOPX(cdir))->getunixdir();

	//
	// Note that the server will call back inval_all() which will flush
	// our name cache lookup entry for 'nm'.
	//
	uint_t	orig_seqnum = lc_remove(nm);
	getunixdir()->remove_dir(nm, udp, credobj, e);
	lc_check_remove(nm, orig_seqnum);
	return (pxfslib::get_err(e));
}

//
// Note: the starting offset in uio_loffset is not an actual offset.
// You can only compare for equality. It is a "cookie" which the
// file system is free to use however it likes.
//
int
pxdir::readdir(uio *uiop, cred *credp, int &eof)
{
	sol::error_t	error;

	data_lock.rdlock();

	if (end_off != 0 && uiop->uio_loffset == end_off) {
		data_lock.unlock();
		eof = 1;
		return (0);
	}
	data_lock.unlock();

	//
	// It is not really practical to handle multiple iovec's
	// since the data that is being read is organized into records
	// and a record cannot easily be split across a boundary.
	// We therefore only return as many whole records as fit into the
	// first iov segment.
	//
	iovec	*iovp = uiop->uio_iov;
	size_t	iolen = (size_t)iovp->iov_len;

	PXFS_VER::unixdir::rawdir_t	*rawdirp = NULL;

	PXFS_VER::unixdir::diroff_t	diroff =
	    (PXFS_VER::unixdir::diroff_t)uiop->uio_loffset;

	bool			eof_flag;
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	Environment		e;

	//
	// Read directory content from the server.
	//
	getunixdir()->readdir_raw(diroff, iolen, rawdirp, eof_flag, credobj, e);
	error = pxfslib::get_err(e);
	if (error) {
		return (error);
	}

	data_lock.wrlock();

	//
	// Cache the end offset to avoid an extra call to provider.
	//
	if (eof_flag) {
		end_off = (offset_t)diroff;
	}
	data_lock.unlock();

	ASSERT(rawdirp != NULL);
	iolen = rawdirp->length();

	error = pxfs_misc::uio_copyout(rawdirp->buffer(),
	    iovp->iov_base, iolen, uiop->uio_segflg);
	if (error == 0) {
		iovp->iov_base += iolen;

#if defined(_LP64)
		iovp->iov_len -= iolen;
#else
		iovp->iov_len -= (long)iolen;
#endif

		uiop->uio_resid -= (ssize_t)iolen;
		uiop->uio_loffset = (offset_t)diroff;
		eof = eof_flag;
	}

	delete rawdirp;
	return (error);
}

int
pxdir::link(vnode *svp, char *nm, cred *credp)
{
	Environment		e;
	sol::error_t		error;
	vnode			*real_vp;
	solobj::cred_var	credobj = solobj_impl::conv(credp);
//	uint_t			orig_seqnum = inval_seqnum;
	uint32_t		orig_seqnum = current_seqnum();
	pxvfs		   *pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);
	int			err = 0;

	//
	// Account for the space needed for this file/directory.
	//
	if (pxvfsp->fastwrite_enabled()) {
		(void) pxvfsp->reserve_blocks(blks_per_file_entry, false, err);
		if (err != 0) {
			return (err);
		}
	}

#if	SOL_VERSION >= __s11
	if (VOP_REALVP(svp, &real_vp, NULL) == 0) {
#else
	if (VOP_REALVP(svp, &real_vp) == 0) {
#endif
		svp = real_vp;
	}
	PXFS_VER::fobj_ptr	sfobj = ((pxfobj *)VTOPX(svp))->getfobj();

	getunixdir()->link_fobj(sfobj, nm, credobj, e);
	error = pxfslib::get_err(e);
	if (error) {
		return (error);
	}

	lc_enter(nm, svp, orig_seqnum, ALL);

	return (0);
}

int
pxdir::symlink(char *nm, vattr *vap, char *target, cred *credp)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	sol::error_t		error;
	Environment		e;
	pxvfs		   *pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);
	int 			err = 0;

	//
	// Account for the space needed for this file/directory.
	//
	if (pxvfsp->fastwrite_enabled()) {
		(void) pxvfsp->reserve_blocks(blks_per_file_entry, false, err);
		if (err != 0) {
			return (err);
		}
	}

	//
	// Note: we cannot create an entry in the lookup cache because
	// create_symlink doesn't return an fobj. The cache entry will be
	// created when the symlink is looked up the first time.
	//
	getunixdir()->create_symlink(nm, conv(*vap), target, credobj, e);

	error = pxfslib::get_err(e);
	if (error) {
		return (error);
	}

	//
	// We need to invalidate negative cache entries and clear the
	// readdir cache.
	//
	(void) lc_remove(nm);

	return (0);
}

int
pxdir::rename(char *oldnm, vnode *newdvp, char *newnm, cred *credp)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	Environment		e;
	vnode			*real_vp;

	ASSERT(newdvp->v_type == VDIR);
#if	SOL_VERSION >= __s11
	if (VOP_REALVP(newdvp, &real_vp, NULL) == 0) {
#else
	if (VOP_REALVP(newdvp, &real_vp) == 0) {
#endif
		newdvp = real_vp;
	}
	pxdir	*newpxdir = (pxdir *)VTOPX(newdvp);

	//
	// We check to see if "oldnm" is a directory and pass an fobj
	// reference if it is so that the directory provider object
	// can flush the directory name cache entry for "..".
	// Since vn_rename() did a VOP_LOOKUP() before calling us, this
	// lookup should be in the cache.
	// Note: we don't call CORBA::release(oldobj) since we are sharing
	// the reference saved in the pxfobj.
	//
	vnode_t			*vnodep = NULL;
	PXFS_VER::unixdir_ptr	oldobj = PXFS_VER::unixdir::_nil();

	if (lookup(oldnm, &vnodep, NULL, 0, NULL, credp) == 0) {
		if (vnodep->v_type == VDIR) {
			oldobj = ((pxdir *)VTOPX(vnodep))->getunixdir();
		}
	}

	//
	// Another thread can call lc_enter() between the time lc_remove()
	// is called and the file being removed on the server. Since
	// the server expects us to remove our own cache entry, we could
	// have a cache entry when the server thinks we don't and therefore
	// the server would not send us an invalidation if some other node
	// created a new object with the same name. If we create a new
	// entry in the name cache *after* deleting the file and we remove
	// it here, this only affects performance and not correctness.
	//
	// XXX could optimize for same directory rename and update
	// lookup table with new name rather than delete & create
	// new entries.
	//
	uint_t	orig_seqnum = lc_remove(oldnm);
	getunixdir()->rename_fobj(oldnm, newpxdir->getunixdir(), newnm,
	    credobj, oldobj, e);

	if (vnodep) {
		VN_RELE(vnodep);
	}
	lc_check_remove(oldnm, orig_seqnum);
	return (pxfslib::get_err(e));
}

int
pxdir::read(uio *, int, cred *)
{
	return (EISDIR);
}

int
pxdir::write(uio *, int, cred *)
{
	return (EISDIR);
}

//
// Return true if a name lookup entry is found and its vnode pointer.
//
bool
pxdir::lc_lookup(char *namep, vnode_t **vnodepp, uint_t& orig_seqnump)
{
	data_lock.rdlock();

	//
	// OS_VERSION_DEPENDENCY:
	// NOCRED is defined for Solaris 8 and Solaris 9 but not defined for
	// Solaris 10.
	//
#if (!defined(NOCRED))
	*vnodepp = dnlc_lookup(get_vp(), namep);
#else
	*vnodepp = dnlc_lookup(get_vp(), namep, NOCRED);
#endif

	if (*vnodepp == NULL) {
		//
		// Obtain the current value of the invalidation sequence
		// number. We hold rdlock_data when reading this value.
		//
		orig_seqnump = inval_seqnum;
		data_lock.unlock();
		return (false);
	}
	data_lock.unlock();
	return (true);
}

//
// Enter the name and vnode into the name lookup cache.
// We check to see if any names were removed on the server after they
// were created by comparing the original sequence number with the current
// one. If the sequence numbers don't match, we don't cache the lookup
// entry since the name may not exist anymore. Multiple name entries
// in different orders should be OK since we only enter them in the cache
// if the operation succeeded on the server.
//
void
pxdir::lc_enter(char *namep, vnode_t *vnodep, uint_t orig_seqnum,
    uint_t flush)
{
	data_lock.wrlock();
	if (orig_seqnum != inval_seqnum) {
		//
		// An invalidation request has been received from the
		// provider while a lookup was pending. To prevent
		// caching a stale entry, we don't enter the result
		// of lookup into the cache.
		// We also remove any cached entry for 'noent'.
		//
		inval_seqnum++;
		dnlc_remove(get_vp(), namep);
	} else {
		//
		// Make a dnlc entry.
		//
		entry_seqnum++;

		//
		// OS_VERSION_DEPENDENCY:
		// NOCRED is defined for Solaris 8 and Solaris 9 but
		// not defined for Solaris 10.
		//
#if (!defined(NOCRED))
		dnlc_update(get_vp(), namep, vnodep);
#else
		dnlc_update(get_vp(), namep, vnodep, NOCRED);
#endif
	}
	if (flush & DIR) {
		end_off = 0;
	}
	if (flush & INVAL) {
		inval_seqnum++;
	}
	data_lock.unlock();
}

//
// Remove a name from the local name lookup cache.
//
// We increment the sequence counter since different threads on the
// same node could do a create and a remove at the same time. The server
// may see the order "create and then remove" (thus both operations returning
// without errors) but the return RPC messages may be received in the other
// order and thus we would do lc_remove() and then lc_enter() on the client.
//
uint_t
pxdir::lc_remove(char *namep)
{
	uint_t	orig_seqnum;

	data_lock.wrlock();
	orig_seqnum = entry_seqnum;
	inval_seqnum++;
	dnlc_remove(get_vp(), namep);

	end_off = 0;

	data_lock.unlock();
	return (orig_seqnum);
}

//
// Check to see if a name might have been entered since it was removed.
// If so, make sure the name is removed from the local name lookup cache.
// Also, make sure that lc_enter() doesn't enter a stale entry by incrementing
// inval_seqnum.
//
void
pxdir::lc_check_remove(char *namep, uint_t orig_seqnum)
{
	data_lock.wrlock();
	inval_seqnum++;
	if (entry_seqnum == orig_seqnum) {
		// No new names have been entered so nothing to do here.
		data_lock.unlock();
		return;
	}
	dnlc_remove(get_vp(), namep);

	end_off = 0;

	data_lock.unlock();
}

//
// current_seqnum - Return the current sequence number.
//
uint32_t
pxdir::current_seqnum()
{
	data_lock.rdlock();
	uint32_t	currentseqnum = inval_seqnum;
	data_lock.unlock();
	return (currentseqnum);
}

//
// dir_inval_entry - Remove only a single entry from DNLC.
//
void
pxdir::dir_inval_entry(const char *namep, Environment &)
{
	(void) lc_remove((char *)namep);
}

//
// This is called when the directory is being deleted.
//
void
pxdir::dir_inval_all(Environment &)
{
	// Flush all DNLC cache entries which refer to this directory.
	data_lock.wrlock();
	inval_seqnum++;
	dnlc_purge_vp(get_vp());
	end_off = 0;
	data_lock.unlock();
}

//
// connect_again - The fobj_client originally given by the server went away.
// This method contacts the server and connects again with a new fobj_client.
//
int
pxdir::connect_again(char *namep, vnode **vnodepp, uint32_t orig_seqnum,
    PXFS_VER::fobj_ptr fobj_p,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    fobj_client_impl *client1p,
    PXFS_VER::fobj_client_ptr client1_p,
    uint_t flush,
    Environment &e)
{
	fobj_client_impl		*client2p;

	PXFS_VER::fobj_client_ptr	client2_p =
	    PXFS_VER::fobj_client::_nil();

	pxvfs			*pxvfsp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp);
	uint32_t		server_incn_orig;

	//
	// Keep trying until we succeed or encounter a fatal error
	//
	while (true) {
		server_incn_orig = pxvfsp->get_server_incn();

		((PXFS_VER::fobjplus_ptr)fobj_p)->
		    cache_new_client(binfo, client1_p, client2_p, e);

		if (e.exception()) {
			//
			// The file server object could not be restored after a
			// node failure.
			//
			e.clear();
			*vnodepp = NULL;
			return (EIO);
		}

		//
		// We only reconnect for those file objects supporting caching.
		//
		ASSERT(!CORBA::is_nil(client2_p));
		if (client1_p->_equiv(client2_p)) {
			//
			// This means we are creating a new proxy vnode
			//
			// Do not allow the server to change while in the
			// middle of creating a proxy vnode.
			//
			pxvfsp->server_incn_lock.rdlock();
			if (server_incn_orig != pxvfsp->get_server_incn()) {
				//
				// The registration was orphanned by a
				// failover or a switchover
				//
				pxvfsp->server_incn_lock.unlock();
				continue;
			}

			*vnodepp = pxvfs::VFSTOPXFS(get_vp()->v_vfsp)->
			    get_pxfobj(fobj_p, fobjinfo, &binfo, client1p);
			pxvfsp->server_incn_lock.unlock();
			if (*vnodepp == NULL) {
				ASSERT(pxvfs::VFSTOPXFS(get_vp()->
				    v_vfsp)->is_unmounted());
				return (EIO);
			}

			// Enter new proxy vnode into DNLC
			lc_enter(namep, *vnodepp, orig_seqnum, flush);

			return (0);
		} else {
			//
			// There already is a proxy vnode
			//
			client2p = (fobj_client_impl *)
			    (client2_p->_handler()->get_cookie());

			//
			// The current client may not be ready
			//
			if (client2p->wait_till_ok()) {
				//
				// Existing client has completed initialization
				// and has a hold placed upon it.
				//
				*vnodepp =
				    pxnode::PXTOV(client2p->get_pxfobjplusp());

				// Enter new proxy vnode into DNLC
				lc_enter(namep, *vnodepp, orig_seqnum, flush);

				return (0);
			}

			//
			// The specified client is going away. Try again.
			//
			CORBA::release(client2_p);
			client2_p = PXFS_VER::fobj_client::_nil();
		}
	}
}

//
// recover_state - the new server is reconstructing state by
// asking each client for its cached state.
//
void
pxdir::recover_state(PXFS_VER::recovery_info_out recovery, Environment &env)
{
	// Must recover the base class info first
	pxfobjplus::recover_state(recovery, env);

	PXFS_VER::recovery_info	*recoveryp =
	    (PXFS_VER::recovery_info *)recovery;

	if (entry_seqnum != 0) {
		//
		// The client has cached directory information
		//
		recoveryp->data_rights = 1;
	} else {
		recoveryp->data_rights = 0;
	}
} //lint !e1746

int
pxdir::startup()
{
	//
	// Initialize the dummy vnode for negative name lookups.
	// This vnode should only be passed to dnlc_enter() or dnlc_update()
	// and the initial "VN_HOLD" should never be released.
	//
#ifdef FSI
	noentp = vn_alloc(KM_SLEEP);
	VN_SET_VFS_TYPE_DEV(noentp, NULL, VNON, NODEV);
	(void) vn_setops(noentp, px_vnodeopsp);
#else
	noentp = new vnode_t;
	VN_INIT(noentp, NULL, VNON, NODEV);
	noentp->v_op = px_vnodeopsp;
	noentp->v_filocks = NULL;
	noentp->v_shrlocks = NULL;
	noentp->v_vfsmountedhere = NULL;
#endif
	noentp->v_flag |= VPXFS;
	return (0);
}

int
pxdir::shutdown()
{
	return (EBUSY);
}
