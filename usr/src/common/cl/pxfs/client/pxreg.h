//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef PXREG_H
#define	PXREG_H

#pragma ident	"@(#)pxreg.h	1.25	09/01/13 SMI"

#include <sys/cred.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/uio.h>

#include <h/sol.h>
#include <sys/os.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/client/pxfobjplus.h>
#include <pxfs/client/pxvfs.h>

//
// pxreg - proxy regular file vnode (type VREG)
//
// This object can cache pages on the client side.
// When activated this object participates in a coherency
// protocol with the server side file object. Caching
// is not activated when directio is enabled.
//
// Locks:
// cache_lock - used for protecting integrity of pages between get pages
// and invalidations. Any invalidation request from server must acquire
// write lock of cache_lock, to make sure that no other threads
// are doing getpage etc. cache_lock acts as the barrier between mmap
// fault updates and read/write system calls, it serves the same
// purpose as the i_content lock in UFS.
//
// Any VOP_RWLOCK() operation on the pxreg vnode acquires reg_rwlock.
//
// Lock Ordering:
// the hierarchy is:
// 	reg_rwlock
//	cache_lock
//	data_token_lock
//
// We drop the cache_lock, but not reg_rwlock,
// when we go over the wire to request read/write access in a range.
// To prevent two threads from going asking requests in the same range
// we use the cache_wait_cv to coordinate the outgoing requests.
//
// The seqnum is used for finding out whether an invalidation request
// has arrived before the reply from the granted request.
//
class pxreg : public pxfobjplus {
	friend class mem_async_read;
	friend class mem_async_write;
public:
	pxreg(fobj_client_impl *fclientp,
	    vfs *vfsp,
	    PXFS_VER::file_ptr filep,
	    const PXFS_VER::fobj_info &fobjinfo);

	virtual ~pxreg();

	void		become_stale();

	// Return a pointer to the server side file object (no CORBA::release).
	PXFS_VER::file_ptr	getfile();

	//
	// Vnode operations.
	//
	virtual int	close(int flag, int count, offset_t offset,
		    cred *credp);

	virtual int	read(uio *uiop, int ioflag, cred *credp);

	virtual int	write(uio *uiop, int ioflag, cred *credp);

	virtual int	fsync(int syncflag, struct cred *credp);

	virtual void	rwlock(int write_lock);

	virtual void	rwunlock(int write_lock);

	virtual int	ioctl(int cmd, intptr_t arg, int flag,
	    struct cred *credp, int *rvalp);

	virtual int	setattr(vattr *vap, int flags, cred *credp);

	virtual int	space(int cmd, struct flock64 *bfp, int flag,
		    offset_t offset, cred *credp);

	virtual int	map(offset_t off, as *as, caddr_t *addrp, size_t len,
		    uchar_t prot, uchar_t maxprot, uint_t flags, cred *credp);

	virtual int	addmap(offset_t off, as *as, caddr_t addr, size_t len,
		    uchar_t prot, uchar_t maxprot, uint_t flags, cred *credp);

	virtual int	delmap(offset_t off, as *as, caddr_t addr, size_t len,
		    uint_t prot, uint_t maxprot, uint_t flags, cred *credp);

	virtual void	dispose(page_t *pp, int flags, int dontneed,
		    cred *credp);

	virtual int	getpage(offset_t off, size_t len, uint_t *protp,
		    page_t *plarr[], size_t plsz, seg *segp, caddr_t addr,
		    seg_rw rw, cred *credp);

	// Putpage is needed for compatibility with NFS server code.
	virtual int	putpage(offset_t off, size_t len, int flags,
		    cred *credp);

	// End vnode operations

	// Methods supporting IDL operations for page cache management

	void	data_write_back_and_delete(uint32_t server_incarnation,
	    Environment &_environment);

	void	data_write_back_downgrade_read_only(uint32_t server_incarnation,
	    Environment &_environment);

	void	data_write_back(Environment &_environment);

	void	data_delete_range(sol::u_offset_t offset,
	    uint32_t server_incarnation,
	    Environment &_environment);

	void	data_change_cache_flag(bool onoff,
	    Environment &_environment);

	// End methods supporting IDL operations

	//
	// Return true if for this file caching info is enabled
	// (i.e., direct I/O is disabled).
	//
	virtual bool	is_cached();

	virtual void	install_cachedata_flag(bool cached);

	virtual void	recover_state(PXFS_VER::recovery_info_out recovery,
	    Environment &env);

	// Replace default routines defined in pxfobj.
	void	change_cachedata_flag(bool onoff);

	// Methods to help track async i/o s on this file
	void increment_aio_pending();
	void decrement_aio_pending();
	void drain_asyncs(bool wait_for_aio);

protected:
	//
	// Set the extended file range for cached appends.
	//
	virtual void	set_ext_range(u_offset_t off, size_t len);

public:
	enum {
		//
		// Flag stored in private fsdata of the page, to make sure
		// that we really pushed the page out when the server asks
		// us to push it out.
		//
		PXFS_HOLE = 0x01,

		//
		// Number of bytes in a page kluster.
		// Don't tie kluster size to page size. For page sizes we
		// could end-up pushing out too less or too much data.
		//
		// XXX: This should be a self tuning variable or a
		// user visible tunable. If kluster size is large for
		// a small memory machine we'l clog up memory with
		// async requests. For a big memory machine we won't
		// use the available pages optimally. 1/2 MB is a
		// compromise.
		//
		pxfs_clustsz_nvalue = 524288 // ie. 512k
	};

	//
	// To identify threads doing synchronous reads we store a
	// value in the thread for a unique Thread Specific Data
	// key. The member sync_read_tsd creates the key and provides
	// methods to store and retrieve values for that key.
	//
	static os::tsd sync_read_tsd;
private:
	enum token_status_t {
		TOKEN_OK = 0,
		TOKEN_EXCEPTION,
		TOKEN_UNCACHED
	};

	//
	// Indicates possible actions on a page due to a putpage() request.
	//
	enum page_action_t {
		QUEUE = 0, // Create a new asynchronous task for the page.
		BOUNCE,    // Ignore request and leave the page dirty.
		SYNC	   // Do a synchronous page_out of the page.
	};

	// Read/write bytes from the cache.
	int	read_cache(uio *uiop, int ioflag, cred *credp);
	int	write_cache(uio *uiop, int ioflag, cred *credp,
			    size_t pre_reserved);

	int	read_uncached(uio *uiop, int ioflag, cred *credp);
	int	write_uncached(uio *uiop, int ioflag, cred *credp);

	int	truncate(u_offset_t size, cred_t *credp);

	int	free_behind_read();

	int	fsync_on_close(int syncflag, struct cred *credp);

#ifndef VXFS_DISABLED
	int	vxfs_ioctl_setext(int cmd, intptr_t arg, int flag,
	    struct cred *credp, int *retp);
	void	vxfs_set_sync_on_close(bool value);
	bool	is_setext_ioctl(int cmd);
#endif

	// Functions that are used to manage when a thread is using the cache
	void	check_wait_lock_cache();
	void	check_wait_lock_cache_revoke();
	void	incr_ref();
	void	decr_ref();
	void	incr_threads_in_reserve_blocks();
	void	decr_threads_in_reserve_blocks();

	//
	// Versions which doesn't grab cache_lock and atomically decrements
	// rwlocks_granted
	//
	void	decr_ref_lockfree();
	void	rwunlock_lockfree(int write_lock);

	token_status_t	get_write_token(int &err);
	token_status_t	get_read_token(int &err);
	bool		has_read_lock();
	bool		has_write_lock();

	int		getapage(u_offset_t off, size_t len, uint_t *protp,
	    page_t *plarr[], size_t plsz, seg *segp, caddr_t addr, seg_rw rw,
	    cred_t *credp, bool seqmode, size_t opt_size);

	void		get_asyncpage(u_offset_t off, size_t len, caddr_t addr,
			    seg *segp, cred_t *credp);

	// pvn_vplist_dirty requires a non-member function
	static int	putapage(vnode *vnodep, page_t *pp, u_offset_t *offp,
			    size_t *lenp, int flags, cred *credp);

	PXFS_VER::blkcnt_t	required_blocks(size_t nbytes);

	// Reserve enough blocks from local pool to write 'numbytes'.
	PXFS_VER::blkcnt_t get_block_reservation(size_t numbytes, bool nobmap,
	    int &err);

	int	alloc_blocks(u_offset_t uoff, size_t n,
		struct cred *credp, bool nodeadlock);

	// The next two methods support invalidate/downgrade operations

	int		putpage_remote(u_offset_t off, size_t len,
			    int flags, cred *credp);

	static int	putapage_remote(vnode *vnodep, page_t *pp,
			    u_offset_t *offp, size_t *lenp, int flags,
			    cred *credp);

	//
	// This method flushes the dirty pages ignoring the IO locked ones.
	//
	int		putpage_revoke();

	//
	// Check whether within the extended file range for cached appends.
	//
	bool		in_ext_range(u_offset_t off, size_t len);

	//
	// Based on async queue size and available memory decide what
	// to do with a dirty page.
	//
	page_action_t get_page_disposition(bool is_write_thread, pxvfs *pxvfsp);

	// Convert Solaris page protection bits to rights
	static PXFS_VER::acc_rights	prot2acc(uint_t prot);

	// Disallowed operations.
	pxreg &operator = (const pxreg &);
	pxreg(const pxreg &);

	//
	// Called to sync dirty data to server.
	//
	virtual int	sync_file();

	//
	// This method is called by sync_filesystem() when the server decides
	// to switch to REDZONE to flush the dirty pages.
	//
	virtual int	sync_file_revoke();

	// Helper method for throttling calculations.
	int64_t diff_timespec(timespec_t start, timespec_t end);

public:
	enum {
		PXFS_READ_TOKEN = 0x1,
		PXFS_WRITE_TOKEN = 0x2
	};
private:
	// Data members are ordered by size.
	// This saves a few words of padding.
	// When multiplied by few a hundred thousand open files,
	// the savings become worthwhile for this simple ordering.

	//
	// This identifies the starting offset for a pending
	// asynchronous write. It is used to collect together
	// multiple writes into one write operation.
	// This is only valid if the number of bytes in delay_len
	// is non-zero.
	// Protected by cache_lock.
	//
	u_offset_t	delay_off;

	//
	// Offset after the last byte in the latest read, including read-ahead.
	// This is used to support read-ahead.
	// This value is not protected by a lock.
	//
	u_offset_t	current_off;

	// The farthest offset that has been paged-in.
	u_offset_t	cur_pagedin_off;

	//
	// The cached extended file range used for appends.
	// These fields are modified only when the low_lock is held.
	// Modified only when the low_lock is held.
	//
	u_offset_t	ext_offset;

	// There is an optimization for append writes which avoids invoking
	// bmap() for allocation when a write falls within extended file range.
	// In this case the client file size will not be in sync with the
	// underlying file system.
	// The following flag  determines whether the file size is in sync or
	// not, and will be used to sync the size using block allocation on
	// server.
	bool	size_in_sync;

	//
	// This records the server time for the latest space allocation.
	// Use this as a heuristic to reduce the number of log syncs.
	//
	os::hrtime_t	allocate_timestamp;

	//
	// Records the client time when a write falls into the extended
	// write optimization.
	//
	os::hrtime_t	last_write_timestamp;

	//
	// Ensures integrity of entire page cache
	//
	os::mutex_t	cache_lock;

	//
	// This identifies the number of bytes for a pending
	// asynchronous write. It is used to collect together
	// multiple writes into one write operation.
	// Protected by cache_lock.
	//
	size_t		delay_len;

	//
	// The cached extended file range used for appends.
	// Modified only when the low_lock is held.
	//
	size_t		ext_length;

	// VOP_RWLOCK() read/write lock
	os::rwlock_t	reg_rwlock;

	//
	// lock coordinates between local putpage and server callbacks
	// to downgrade token.
	//
	// This lock protects changes to cur_data_token
	//
	os::rwlock_t	data_token_lock;

	//
	// Sequence number for data token.
	// The sequence number is incremented whenever
	// the write permission may be given up.
	// The data_token_lock protects this field.
	//
	uint_t		seqnum;

	//
	// The count of rwlocks granted.
	// This is used to delay invalidations until
	// the current lock holders complete their work.
	//
	uint32_t	rwlocks_granted;

	int		threads_waiting_for_token;

	//
	// The count of threads blocked in reserve_blocks().
	//
	int		threads_in_reserve_blocks;

	//
	// Synchronizes access to the server in conjunction with cache_lock
	//
	os::condvar_t	cache_wait_cv;

	// Identifies the data cache permission held currently
	uchar_t		cur_data_token;

	// Identifies the data cache permissions being requested
	uchar_t		req_data_token;

	bool		sync_on_close;

	//
	// True means that caching is enabled.
	// When directio is enabled, we turn off caching.
	//
	bool		cachedata_flag;

	bool		truncate_in_progress;

	//
	// If the file is mmapped and has in-memory pages with write
	// permission we will not get a fault while modifying them.
	// Thus we can create dirty pages without holding the write
	// token if an mmapped page is modified after this nodes token
	// was downgraded to read.
	//
	// 'mmap_pages' tells us the number of pages with mapings for
	// this vnode, and thus whether it is safe to *NOT* throw away
	// pages while giving up our write token. If mmap_pages is '0'
	// we needn't throw away pages.
	//
	// A direct impact of this behaviour is, the client can have
	// pages with write permission without holding the write
	// token. Normally this would be bad. We rely on the code flow
	// which ensures that any write operation, except for faulted
	// in mmap'd pages, obtains the write token before modifying
	// the page data.
	//
	os::rwlock_t	mmap_pages_lock;
	int64_t		mmap_pages;

	//
	// putapage() can be called from fsflush, pageout_daemon, revoke()
	// from fastwrite or via segmap_release() after a write(). The page
	// is handled differently depending on the caller. We don't
	// 'bounce' pages unless the putapage() is from a write request.
	// write_cache() stores the offset of the page being released, in
	// 'segmap_release_offset'. This value is cleared after segmap
	// release returns. putapage() will treat the page as from a write
	// request only if 'segmap_release_offset' is within the vnode
	// offset of the page passed and vnode offset plus size of the
	// putapage.
	//
	u_offset_t segmap_release_offset;

	//
	// Dirty pages created for synchronous writes can be picked up
	// by fsflush or pageout daemon and marked non-dirty. If that
	// happens, segmap_release will not find dirty pages. Write
	// will return success before dirty pages have been written to
	// disk.
	//
	// To prevent the above problem we prevent fsflush or pageout
	// daemon from picking up dirty pages created by synchronous
	// writes. 'sync_write_in_progress' is set to true before
	// pages are marked dirty for a synchronous write and cleared
	// after segmap_release() returns. If 'sync_write_in_progress'
	// is true, only write_cache thread can flush dirty pages.
	//
	bool sync_write_in_progress;

	//
	// Pages written via the mem_async threadpool are destroyed only
	// when the corresponding aio_callback object signals write
	// completion. Even if all queued mem_async tasks are drained there
	// could be pages to be freed waiting for callback. To be sure of
	// freeing all pages, we have to keep track of pending aio_callback
	// objects. Only when there are no call backs left can we declare
	// that all pages associated with this pxreg have been freed.
	//
	// The variables below keep track of pending aio_callbacks via the
	// 'aios_pending' counter. Every queued aio increments it and it is
	// decremented when the server signals write completion. Whenever
	// 'aios_pending' becomes zero we signal any waiting thread.
	//
	os::mutex_t	aios_pending_lock;
	os::condvar_t	aios_pending_cv;
	int64_t		aios_pending;

	//
	// Write bandwidth is allocated in bandwidth_chunk size to avoid
	// lock contention for small writes. For writes of size less than
	// bandwidth_chunk, the remainder is cached in 'cached_bandwidth'
	// member and allocated to the next writer(s). If a write needs
	// more bandwidth than cached by 'cached_bandwidth', the existing
	// value is thrown away and a new bandwidth allocation is done.
	//
	int		cached_bandwidth;
};

#include <pxfs/client/pxreg_in.h>

#ifndef VXFS_DISABLED
#include <pxfs/common/vxfs_int.h>
#include <pxfs/client/pxreg_vxfs_in.h>
#endif

#endif	// PXREG_H
