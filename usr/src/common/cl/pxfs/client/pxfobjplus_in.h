//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfobjplus_in.h	1.6	08/05/20 SMI"

//
// class inactive_list_elem inline methods
//

inline
inactive_list_elem::inactive_list_elem(void *elemp) :
	_DList::ListElem(elemp)
{
}

//
// classe pxfobjplus inline methods
//

inline PXFS_VER::fobjplus_ptr
pxfobjplus::getfobjplus() const
{
	//
	// This conversion is safe as long as fobjplus inherits from fobj.
	//
	return ((PXFS_VER::fobjplus_ptr)fobj_obj);
}

inline void
pxfobjplus::set_recycled()
{
	state_lock.lock();
	pxflags |= PX_RECYCLED;
	state_lock.unlock();
}

inline void
pxfobjplus::set_client(fobj_client_impl *fobjclientp)
{
	clientp = fobjclientp;
}

//
// Get file length without holding attr lock. See comments in header file
//
inline u_offset_t
pxfobjplus::get_file_length()
{
	return (attr.va_size);
}

//
// Return the current file length.
//
inline u_offset_t
pxfobjplus::get_length()
{
	ASSERT(high_lock.lock_held());
	return (attr.va_size);
}

inline bool
pxfobjplus::has_size_changed()
{
	bool res;

	high_lock.rdlock();
	res = ((attr.va_mask & AT_SIZE) != 0);
	high_lock.unlock();
	return (res);
}

//
// Set the file length.
//
inline void
pxfobjplus::set_length(u_offset_t size)
{
	ASSERT(high_lock.write_held());
	ASSERT(access_rights == PXFS_VER::attr_write);

	attr.va_mask |= AT_SIZE;
	attr.va_size = size;
}

//
// This should be called after get_attr_read_token() or
// get_attr_write_token() to
// indicate that the token is available for other threads on this node to
// acquire.
//
inline void
pxfobjplus::drop_attr_token()
{
	low_lock.unlock();
	high_lock.unlock();
}
