//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// pxspecial.h - proxy vnode for a real link
//

#ifndef PXSPECIAL_H
#define	PXSPECIAL_H

#pragma ident	"@(#)pxspecial.h	1.7	08/05/20 SMI"

#include "../version.h"
#include <pxfs/client/pxfobj.h>

class pxspecial : public pxfobj {
public:
	pxspecial(vfs *vfsp, PXFS_VER::special_ptr spp,
	    const PXFS_VER::fobj_info &fobjinfo);

	~pxspecial();

	// Vnode operations.
	int	open(vnode **vpp, int flag, cred *cr);
	int	fsync(int syncflag, struct cred *cr);

private:
	PXFS_VER::special_ptr	get_special();

	// Disallowed operations.
	pxspecial & operator=(const pxspecial &rsh);
	pxspecial(const pxspecial &rsh);
};

inline PXFS_VER::special_ptr
pxspecial::get_special()
{
	// This conversion is safe as long as special inherits from fobj.
	return ((PXFS_VER::special_ptr)getfobj());
}

#endif	// PXSPECIAL_H
