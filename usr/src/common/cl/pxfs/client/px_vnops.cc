//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)px_vnops.cc	1.13	08/05/20 SMI"

#include <sys/types.h>
#include <sys/cred.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/flock.h>
#include <sys/uio.h>
#include <sys/debug.h>
#include <sys/fs_subr.h>
#include <sys/swap.h>

#include <vm/page.h>
#include <vm/as.h>
#include <vm/seg.h>
#include <pxfs/common/pxfslib.h>

#include "../version.h"
#include <pxfs/client/pxfobj.h>

//
// This file implements the vnodeops table for the vnode interface for PXFS
// file systems. Its basic job is to convert "C" calls to "C++" calls.
//

#if	SOL_VERSION >= __s11
static int px_open(vnode **, int, cred *, caller_context_t *);
static int px_close(vnode *, int, int, offset_t, cred *, caller_context_t *);
#else
static int px_open(vnode **, int, cred *);
static int px_close(vnode *, int, int, offset_t, cred *);
#endif
#if	SOL_VERSION >= __s10
static int px_read(vnode *, uio *, int, cred *, caller_context *);
static int px_write(vnode *, uio *, int, cred *, caller_context *);
#else
static int px_read(vnode *, uio *, int, cred *);
static int px_write(vnode *, uio *, int, cred *);
#endif
#if	SOL_VERSION >= __s11
static int px_ioctl(vnode *, int, intptr_t, int, cred *, int *,
    caller_context *);
static int px_getattr(vnode *, vattr *, int, cred *, caller_context *);
#else
static int px_ioctl(vnode *, int, intptr_t, int, cred *, int *);
static int px_getattr(vnode *, vattr *, int, cred *);
#endif

//
// An extra argument caller_context is added to VOP_SPACE, VOP_SETATTR,
// VOP_RWLOCK and VOP_RWUNLOCK to support NFSv4 delegations in
// accordance with PSARC 2004/172. Caller_context is used to identify
// caller of the operation whether it is NFS server or local process.
// When passed to PxFS, caller_context structure is not used. Hence
// it is explicitely set to NULL in PxFS client and server.
//

#if	SOL_VERSION >= __s10
static int px_setattr(vnode *, vattr *, int, cred *, caller_context *);
static int px_space(vnode *, int, struct flock64 *, int, offset_t, cred *,
    caller_context *);
static void px_rwlock(vnode *, int, caller_context *);
static void px_rwunlock(vnode *, int, caller_context *);
#else
static int px_setattr(vnode *, vattr *, int, cred *);
static int px_space(vnode *, int, struct flock64 *, int, offset_t, cred *);
static void px_rwlock(vnode *, int);
static void px_rwunlock(vnode *, int);
#endif

#if	SOL_VERSION >= __s11
static int px_access(vnode *, int, int, cred *, caller_context *);
#else
static int px_access(vnode *, int, int, cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_lookup(vnode *, char *, vnode **, pathname *, int, vnode *,
		cred *, caller_context_t *, int *, struct pathname *);
#else
static int px_lookup(vnode *, char *, vnode **, pathname *, int, vnode *,
		cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_create(vnode *, char *, vattr *, enum vcexcl, int, vnode **,
		cred *, int, caller_context_t *, vsecattr_t *);
#else
static int px_create(vnode *, char *, vattr *, enum vcexcl, int, vnode **,
		cred *, int);
#endif

#if	SOL_VERSION >= __s11
static int px_remove(vnode *, char *, cred *, caller_context_t *, int);
static int px_link(vnode *, vnode *, char *, cred *, caller_context_t *, int);
static int px_rename(vnode *, char *, vnode *, char *, cred *,
    caller_context_t *, int);
#else
static int px_remove(vnode *, char *, cred *);
static int px_link(vnode *, vnode *, char *, cred *);
static int px_rename(vnode *, char *, vnode *, char *, cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_mkdir(vnode *, char *, vattr *, vnode **, cred *,
    caller_context_t *, int, vsecattr_t *);
#else
static int px_mkdir(vnode *, char *, vattr *, vnode **, cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_rmdir(vnode *, char *, vnode *, cred *, caller_context_t *, int);
#else
static int px_rmdir(vnode *, char *, vnode *, cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_readdir(vnode *, uio *, cred *, int *, caller_context_t *, int);
static int px_symlink(vnode *, char *, vattr *, char *, cred *,
    caller_context_t *, int);
#else
static int px_readdir(vnode *, uio *, cred *, int *);
static int px_symlink(vnode *, char *, vattr *, char *, cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_readlink(vnode *, uio *, cred *, caller_context_t *);
static int px_fsync(vnode *, int, cred *, caller_context_t *);
static void px_inactive(vnode *, cred *, caller_context_t *);
static int px_fid(vnode *, fid *, caller_context_t *);
static int px_seek(vnode *, offset_t, offset_t *, caller_context_t *);
static int px_cmp(struct vnode *vp1, struct vnode *vp2, caller_context_t *);
#else
static int px_readlink(vnode *, uio *, cred *);
static int px_fsync(vnode *, int, cred *);
static void px_inactive(vnode *, cred *);
static int px_fid(vnode *, fid *);
static int px_seek(vnode *, offset_t, offset_t *);
static int px_cmp(struct vnode *vp1, struct vnode *vp2);
#endif

#if	SOL_VERSION >= __s11
static int px_frlock(vnode *, int, struct flock64 *, int, offset_t,
    flk_callback_t *, cred *, caller_context_t *);
#else
#if	SOL_VERSION >= __s9 /* new s9 frlock interface */
static int px_frlock(vnode *, int, struct flock64 *, int, offset_t,
    flk_callback_t *, cred *);
#else
static int px_frlock(vnode *, int, struct flock64 *, int, offset_t, cred *);
#endif
#endif

#if	SOL_VERSION >= __s11
static int px_realvp(vnode *, vnode **, caller_context_t *);
static int px_getpage(vnode *, offset_t, size_t, uint_t *, page **, size_t,
    seg *, caddr_t, enum seg_rw, cred *, caller_context_t *);
static int px_putpage(vnode *, offset_t, size_t, int, cred *,
    caller_context_t *);
static int px_map(vnode *, offset_t, as *, caddr_t *, size_t, uchar_t, uchar_t,
    uint_t, cred *, caller_context_t *);
static int px_addmap(vnode *, offset_t, as *, caddr_t, size_t, uchar_t, uchar_t,
    uint_t, cred *, caller_context_t *);
static int px_delmap(vnode *, offset_t, as *, caddr_t, size_t, uint_t, uint_t,
    uint_t, cred *, caller_context_t *);
static int px_dump(vnode_t *, caddr_t, int, int, caller_context_t *);
static int px_pathconf(vnode *, int, ulong_t *, cred *, caller_context_t *);
static int px_pageio(vnode *, page *, u_offset_t, size_t, int, cred *,
    caller_context_t *);
static int px_dumpctl(vnode_t *, int, int *, caller_context_t *);
static void px_dispose(vnode_t *, page_t *, int, int, struct cred *,
    caller_context_t *);
static int px_setsecattr(vnode *, vsecattr_t *, int, struct cred *,
    caller_context_t *);
static int px_getsecattr(vnode *, vsecattr_t *, int, struct cred *,
    caller_context_t *);
#else
static int px_realvp(vnode *, vnode **);
static int px_getpage(vnode *, offset_t, size_t, uint_t *, page **, size_t,
		seg *, caddr_t, enum seg_rw, cred *);
static int px_putpage(vnode *, offset_t, size_t, int, cred *);
static int px_map(vnode *, offset_t, as *, caddr_t *, size_t, uchar_t, uchar_t,
		uint_t, cred *);
static int px_addmap(vnode *, offset_t, as *, caddr_t, size_t, uchar_t, uchar_t,
		uint_t, cred *);
static int px_delmap(vnode *, offset_t, as *, caddr_t, size_t, uint_t, uint_t,
		uint_t, cred *);
static int px_dump(vnode_t *, caddr_t, int, int);
static int px_pathconf(vnode *, int, ulong_t *, cred *);
static int px_pageio(vnode *, page *, u_offset_t, size_t, int, cred *);
static int px_dumpctl(vnode_t *, int, int *);
static void px_dispose(vnode_t *, page_t *, int, int, struct cred *);
static int px_setsecattr(vnode *, vsecattr_t *, int, struct cred *);
static int px_getsecattr(vnode *, vsecattr_t *, int, struct cred *);
#endif

#if	SOL_VERSION >= __s11
static int px_shrlock(vnode *, int, shrlock *, int, cred *, caller_context_t *);
#else
#if	SOL_VERSION >= __s10
static int px_shrlock(vnode *, int, shrlock *, int, cred *);
#else
static int px_shrlock(vnode *, int, shrlock *, int);
#endif
#endif

#ifdef FSI

struct vnodeops *px_vnodeopsp = NULL;

fs_operation_def_t px_vnodeops_template[] = {
	VOPNAME_OPEN, (fs_generic_func_p)px_open,
	VOPNAME_CLOSE, (fs_generic_func_p)px_close,
	VOPNAME_READ, (fs_generic_func_p)px_read,
	VOPNAME_WRITE, (fs_generic_func_p)px_write,
	VOPNAME_IOCTL, (fs_generic_func_p)px_ioctl,
	VOPNAME_SETFL, (fs_generic_func_p)fs_setfl,
	VOPNAME_GETATTR, (fs_generic_func_p)px_getattr,
	VOPNAME_SETATTR, (fs_generic_func_p)px_setattr,
	VOPNAME_ACCESS, (fs_generic_func_p)px_access,
	VOPNAME_LOOKUP, (fs_generic_func_p)px_lookup,
	VOPNAME_CREATE, (fs_generic_func_p)px_create,
	VOPNAME_REMOVE, (fs_generic_func_p)px_remove,
	VOPNAME_LINK, (fs_generic_func_p)px_link,
	VOPNAME_RENAME, (fs_generic_func_p)px_rename,
	VOPNAME_MKDIR, (fs_generic_func_p)px_mkdir,
	VOPNAME_RMDIR, (fs_generic_func_p)px_rmdir,
	VOPNAME_READDIR, (fs_generic_func_p)px_readdir,
	VOPNAME_SYMLINK, (fs_generic_func_p)px_symlink,
	VOPNAME_READLINK, (fs_generic_func_p)px_readlink,
	VOPNAME_FSYNC, (fs_generic_func_p)px_fsync,
	VOPNAME_INACTIVE, (fs_generic_func_p)px_inactive,
	VOPNAME_FID, (fs_generic_func_p)px_fid,
	VOPNAME_RWLOCK, (fs_generic_func_p)px_rwlock,
	VOPNAME_RWUNLOCK, (fs_generic_func_p)px_rwunlock,
	VOPNAME_SEEK, (fs_generic_func_p)px_seek,
	VOPNAME_CMP, (fs_generic_func_p)px_cmp,
	VOPNAME_FRLOCK, (fs_generic_func_p)px_frlock,
	VOPNAME_SPACE, (fs_generic_func_p)px_space,
	VOPNAME_REALVP, (fs_generic_func_p)px_realvp,
	VOPNAME_GETPAGE, (fs_generic_func_p)px_getpage,
	VOPNAME_PUTPAGE, (fs_generic_func_p)px_putpage,
	VOPNAME_MAP, (fs_generic_func_p)px_map,
	VOPNAME_ADDMAP, (fs_generic_func_p)px_addmap,
	VOPNAME_DELMAP, (fs_generic_func_p)px_delmap,
	VOPNAME_POLL, (fs_generic_func_p)fs_poll,
	VOPNAME_DUMP, (fs_generic_func_p)px_dump,
	VOPNAME_PATHCONF, (fs_generic_func_p)px_pathconf,
	VOPNAME_PAGEIO, (fs_generic_func_p)px_pageio,
	VOPNAME_DUMPCTL, (fs_generic_func_p)px_dumpctl,
	VOPNAME_DISPOSE, (fs_generic_func_p)px_dispose,
	VOPNAME_GETSECATTR, (fs_generic_func_p)px_getsecattr,
	VOPNAME_SETSECATTR, (fs_generic_func_p)px_setsecattr,
	VOPNAME_SHRLOCK, (fs_generic_func_p)px_shrlock,
	NULL, NULL
};

//
// px_vn_init - initializes the vnode data structure (operations vector)
// for pxfs.
//
int
px_vn_init(char *namep)
{
	int	error;

	error = vn_make_ops(namep, px_vnodeops_template, &px_vnodeopsp);
	return (error);
}

//
// px_vn_uninit - uninitializes the vnode data structure (operations vector)
// for pxfs.
//
void
px_vn_uninit()
{
	if (px_vnodeopsp != NULL) {
		vn_freevnodeops(px_vnodeopsp);
	}
}

#else

struct vnodeops px_vnodeops = {
	px_open,
	px_close,
	px_read,
	px_write,
	px_ioctl,
	fs_setfl,
	px_getattr,
	px_setattr,
	px_access,
	px_lookup,
	px_create,
	px_remove,
	px_link,
	px_rename,
	px_mkdir,
	px_rmdir,
	px_readdir,
	px_symlink,
	px_readlink,
	px_fsync,
	px_inactive,
	px_fid,
	px_rwlock,
	px_rwunlock,
	px_seek,
	px_cmp,
	px_frlock,
	px_space,
	px_realvp,
	px_getpage,
	px_putpage,
	px_map,
	px_addmap,
	px_delmap,
	fs_poll,
	px_dump,
	px_pathconf,
	px_pageio,
	px_dumpctl,
	px_dispose,
	px_setsecattr,
	px_getsecattr,
	px_shrlock
};

struct vnodeops *px_vnodeopsp = &px_vnodeops;

#endif

static int
#if	SOL_VERSION >= __s11
px_open(struct vnode **vpp, int flag, struct cred *cr, caller_context_t *)
#else
px_open(struct vnode **vpp, int flag, struct cred *cr)
#endif
{
	//
	// Swapon to a pxfs file isn't supported so
	// fail the open if VISSWAP is set.
	//
	if ((*vpp)->v_flag & VISSWAP)
		return (EINVAL);
	else
		return (pxnode::VTOPX(*vpp)->open(vpp, flag, cr));
}

static int
#if	SOL_VERSION >= __s11
px_close(struct vnode *vp, int flag, int count, offset_t offset,
    struct cred *cr, caller_context_t *)
#else
px_close(struct vnode *vp, int flag, int count, offset_t offset,
    struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->close(flag, count, offset, cr));
}

static int
#if	SOL_VERSION >= __s10
px_read(struct vnode *vp, struct uio *uiop, int ioflag, struct cred *cr,
    caller_context *)
#else
px_read(struct vnode *vp, struct uio *uiop, int ioflag, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->read(uiop, ioflag, cr));
}

static int
#if	SOL_VERSION >= __s10
px_write(struct vnode *vp, struct uio *uiop, int ioflag, struct cred *cr,
    caller_context *)
#else
px_write(struct vnode *vp, struct uio *uiop, int ioflag, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->write(uiop, ioflag, cr));
}

static int
#if	SOL_VERSION >= __s11
px_ioctl(struct vnode *vp, int cmd, intptr_t arg, int flag, struct cred *cr,
    int *rvalp, caller_context_t *)
#else
px_ioctl(struct vnode *vp, int cmd, intptr_t arg, int flag, struct cred *cr,
    int *rvalp)
#endif
{
	return (pxnode::VTOPX(vp)->ioctl(cmd, arg, flag, cr, rvalp));
}

static int
#if	SOL_VERSION >= __s11
px_getattr(struct vnode *vp, struct vattr *vap, int flags, struct cred *cr,
    caller_context_t *)
#else
px_getattr(struct vnode *vp, struct vattr *vap, int flags, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->getattr(vap, flags, cr));
}

static int
#if	SOL_VERSION >= __s10
px_setattr(struct vnode *vp, struct vattr *vap, int flags, struct cred *cr,
    caller_context *)
#else
px_setattr(struct vnode *vp, struct vattr *vap, int flags, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->setattr(vap, flags, cr));
}

static int
#if	SOL_VERSION >= __s11
px_access(struct vnode *vp, int mode, int flags, struct cred *cr,
    caller_context_t *)
#else
px_access(struct vnode *vp, int mode, int flags, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->access(mode, flags, cr));
}

static int
#if	SOL_VERSION >= __s11
px_readlink(struct vnode *vp, struct uio *uiop, struct cred *cr,
    caller_context_t *)
#else
px_readlink(struct vnode *vp, struct uio *uiop, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->readlink(uiop, cr));
}

static int
#if	SOL_VERSION >= __s11
px_fsync(struct vnode *vp, int syncflag, struct cred *cr, caller_context_t *)
#else
px_fsync(struct vnode *vp, int syncflag, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->fsync(syncflag, cr));
}

static void
#if	SOL_VERSION >= __s11
px_inactive(struct vnode *vp, struct cred *, caller_context_t *)
#else
px_inactive(struct vnode *vp, struct cred *)
#endif
{
	pxnode::VTOPX(vp)->inactive();
}

static int
#if	SOL_VERSION >= __s11
px_lookup(struct vnode *dvp, char *nm, struct vnode **vpp,
    struct pathname *pnp, int flags, struct vnode *rdir, struct cred *cr,
    caller_context_t *, int *, struct pathname *)
#else
px_lookup(struct vnode *dvp, char *nm, struct vnode **vpp,
	struct pathname *pnp, int flags, struct vnode *rdir, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(dvp)->lookup(nm, vpp, pnp, flags, rdir, cr));
}

static int
#if	SOL_VERSION >= __s11
px_create(struct vnode *dvp, char *name, struct vattr *vap, enum vcexcl excl,
    int mode, struct vnode **vpp, struct cred *cr, int flag,
    caller_context_t *, vsecattr_t *)
#else
px_create(struct vnode *dvp, char *name, struct vattr *vap, enum vcexcl excl,
	int mode, struct vnode **vpp, struct cred *cr, int flag)
#endif
{
	return (pxnode::VTOPX(dvp)->create(name, vap, excl, mode, vpp,
	    cr, flag));
}

static int
#if	SOL_VERSION >= __s11
px_remove(struct vnode *dvp, char *nm, struct cred *cr, caller_context_t *, int)
#else
px_remove(struct vnode *dvp, char *nm, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(dvp)->remove(nm, cr));
}

static int
#if	SOL_VERSION >= __s11
px_link(struct vnode *tdvp, struct vnode *svp, char *tnm, struct cred *cr,
    caller_context_t *, int)
#else
px_link(struct vnode *tdvp, struct vnode *svp, char *tnm, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(tdvp)->link(svp, tnm, cr));
}

static int
#if	SOL_VERSION >= __s11
px_rename(struct vnode *sdvp, char *snm, struct vnode *tdvp, char *tnm,
    struct cred *cr, caller_context_t *, int)
#else
px_rename(struct vnode *sdvp, char *snm, struct vnode *tdvp, char *tnm,
    struct cred *cr)
#endif
{
	return (pxnode::VTOPX(sdvp)->rename(snm, tdvp, tnm, cr));
}

static int
#if	SOL_VERSION >= __s11
px_mkdir(struct vnode *dvp, char *nm, struct vattr *vap, struct vnode **vpp,
    struct cred *cr, caller_context_t *, int, vsecattr_t *)
#else
px_mkdir(struct vnode *dvp, char *nm, struct vattr *vap, struct vnode **vpp,
    struct cred *cr)
#endif
{
	return (pxnode::VTOPX(dvp)->mkdir(nm, vap, vpp, cr));
}

static int
#if	SOL_VERSION >= __s11
px_rmdir(struct vnode *dvp, char *nm, struct vnode *cdir, struct cred *cr,
    caller_context_t *, int)
#else
px_rmdir(struct vnode *dvp, char *nm, struct vnode *cdir, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(dvp)->rmdir(nm, cdir, cr));
}

static int
#if	SOL_VERSION >= __s11
px_readdir(struct vnode *vp, struct uio *uiop, struct cred *cr, int *eofp,
    caller_context_t *, int)
#else
px_readdir(struct vnode *vp, struct uio *uiop, struct cred *cr, int *eofp)
#endif
{
	int dummy_eof;

	return (pxnode::VTOPX(vp)->readdir(uiop, cr, eofp == NULL ?
	    dummy_eof : *eofp));
}

static int
#if	SOL_VERSION >= __s11
px_symlink(struct vnode *dvp, char *linkname, struct vattr *vap,
    char *target, struct cred *cr, caller_context_t *, int)
#else
px_symlink(struct vnode *dvp, char *linkname, struct vattr *vap,
    char *target, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(dvp)->symlink(linkname, vap, target, cr));
}

static int
#if	SOL_VERSION >= __s11
px_fid(struct vnode *vp, struct fid *fidp, caller_context_t *)
#else
px_fid(struct vnode *vp, struct fid *fidp)
#endif
{
	return (pxnode::VTOPX(vp)->vop_fid(fidp));
}

static void
#if	SOL_VERSION >= __s10
px_rwlock(struct vnode *vp, int write_lock, caller_context *)
#else
px_rwlock(struct vnode *vp, int write_lock)
#endif
{
	pxnode::VTOPX(vp)->rwlock(write_lock);
}

static void
#if	SOL_VERSION >= __s10
px_rwunlock(struct vnode *vp, int write_lock, caller_context *)
#else
px_rwunlock(struct vnode *vp, int write_lock)
#endif
{
	pxnode::VTOPX(vp)->rwunlock(write_lock);
}

static int
#if	SOL_VERSION >= __s11
px_seek(struct vnode *vp, offset_t ooff, offset_t *noffp, caller_context_t *)
#else
px_seek(struct vnode *vp, offset_t ooff, offset_t *noffp)
#endif
{
	return (pxnode::VTOPX(vp)->seek(ooff, noffp));
}

static int
#if	SOL_VERSION >= __s11
px_cmp(struct vnode *vp1, struct vnode *vp2, caller_context_t *)
#else
px_cmp(struct vnode *vp1, struct vnode *vp2)
#endif
{
	vnode_t *realvp;

	//
	// if this request for the compare comes in from lofs
	// there is no guarantee that this is really pxnode.
	// So we check first before calling px_realvp(),
	// otherwise we risk panicing the node  because realvp()
	// is a pxnode class method (unique to a pxnode)
	//
	if (vp1->v_op != vp2->v_op)
		return (0);

	if (vp2->v_type == VCHR) {
#if	SOL_VERSION >= __s11
		if (px_realvp(vp2, &realvp, NULL) == 0) {
#else
		if (px_realvp(vp2, &realvp) == 0) {
#endif
			return (vp1 == realvp);
		}
	}
	return (vp1 == vp2);
}

#if	SOL_VERSION >= __s11
static int
px_frlock(struct vnode *vp, int cmd, struct flock64 *bfp, int flag,
    offset_t offset, flk_callback_t *flk_cb, cred_t *cr, caller_context_t *)
{
	return (pxnode::VTOPX(vp)->frlock(cmd, bfp, flag, offset, flk_cb, cr));
}
#else
#if	SOL_VERSION >= __s9 /* new s9 frlock interface */
static int
px_frlock(struct vnode *vp, int cmd, struct flock64 *bfp, int flag,
	offset_t offset, flk_callback_t *flk_cb, cred_t *cr)
{
	return (pxnode::VTOPX(vp)->frlock(cmd, bfp, flag, offset, flk_cb, cr));
}
#else
static int
px_frlock(struct vnode *vp, int cmd, struct flock64 *bfp, int flag,
	offset_t offset, cred_t *cr)
{
	return (pxnode::VTOPX(vp)->frlock(cmd, bfp, flag, offset, cr));
}
#endif
#endif

static int
#if	SOL_VERSION >= __s10
px_space(struct vnode *vp, int cmd, struct flock64 *bfp, int flag,
    offset_t offset, struct cred *cr, caller_context *)
#else
px_space(struct vnode *vp, int cmd, struct flock64 *bfp, int flag,
    offset_t offset, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->space(cmd, bfp, flag, offset, cr));
}

static int
#if	SOL_VERSION >= __s11
px_realvp(struct vnode *vp, struct vnode **vpp, caller_context_t *)
#else
px_realvp(struct vnode *vp, struct vnode **vpp)
#endif
{
	return (pxnode::VTOPX(vp)->realvp(vpp));
}

static int
#if	SOL_VERSION >= __s11
px_getpage(struct vnode *vp, offset_t off, size_t len, uint_t *protp,
	page_t *plarr[], size_t plsz, struct seg *segp, caddr_t addr,
	enum seg_rw rw, struct cred *cr, caller_context_t *)
#else
px_getpage(struct vnode *vp, offset_t off, size_t len, uint_t *protp,
	page_t *plarr[], size_t plsz, struct seg *segp, caddr_t addr,
	enum seg_rw rw, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->getpage(off, len, protp, plarr, plsz, segp,
	    addr, rw, cr));
}

static int
#if	SOL_VERSION >= __s11
px_putpage(struct vnode *vp, offset_t off, size_t len, int flags,
	struct cred *cr, caller_context_t *)
#else
px_putpage(struct vnode *vp, offset_t off, size_t len, int flags,
	struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->putpage(off, len, flags, cr));
}

static int
#if	SOL_VERSION >= __s11
px_map(struct vnode *vp, offset_t off, struct as *asp, caddr_t *addrp,
	size_t len, uchar_t prot, uchar_t maxprot, uint_t flags,
	struct cred *cr, caller_context_t *)
#else
px_map(struct vnode *vp, offset_t off, struct as *asp, caddr_t *addrp,
	size_t len, uchar_t prot, uchar_t maxprot, uint_t flags,
	struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->map(off, asp, addrp, len, prot,
	    maxprot, flags, cr));
}

static int
#if	SOL_VERSION >= __s11
px_addmap(struct vnode *vp, offset_t off, struct as *asp, caddr_t addr,
	size_t len, uchar_t  prot, uchar_t maxprot, uint_t flags,
	struct cred *cr, caller_context_t *)
#else
px_addmap(struct vnode *vp, offset_t off, struct as *asp, caddr_t addr,
	size_t len, uchar_t  prot, uchar_t maxprot, uint_t flags,
	struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->addmap(off, asp, addr, len, prot, maxprot,
	    flags, cr));
}

static int
#if	SOL_VERSION >= __s11
px_delmap(struct vnode *vp, offset_t off, struct as *asp, caddr_t addr,
	size_t len, uint_t prot, uint_t maxprot, uint_t flags, struct cred *cr,
	caller_context_t *)
#else
px_delmap(struct vnode *vp, offset_t off, struct as *asp, caddr_t addr,
	size_t len, uint_t prot, uint_t maxprot, uint_t flags, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->delmap(off, asp, addr, len, prot, maxprot,
	    flags, cr));
}

static int
#if	SOL_VERSION >= __s11
px_pathconf(struct vnode *vp, int cmd, ulong_t *valp, struct cred *cr,
    caller_context_t *)
#else
px_pathconf(struct vnode *vp, int cmd, ulong_t *valp, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->pathconf(cmd, valp, cr));
}

static int
#if	SOL_VERSION >= __s11
px_pageio(struct vnode *, page_t *, u_offset_t, size_t, int, cred_t *,
    caller_context_t *)
#else
px_pageio(struct vnode *, page_t *, u_offset_t, size_t, int, cred_t *)
#endif
{
	return (ENOTSUP);
}

static int
#if	SOL_VERSION >= __s11
px_dump(vnode_t *, caddr_t, int, int, caller_context_t *)
#else
px_dump(vnode_t *, caddr_t, int, int)
#endif
{
	return (ENOTSUP);
}

static int
#if	SOL_VERSION >= __s11
px_dumpctl(vnode_t *, int, int *, caller_context_t *)
#else
px_dumpctl(vnode_t *, int, int *)
#endif
{
	return (ENOTSUP);
}

static void
#if	SOL_VERSION >= __s11
px_dispose(struct vnode *vp, page_t *pp, int fl, int dn, struct cred *cr,
    caller_context_t *)
#else
px_dispose(struct vnode *vp, page_t *pp, int fl, int dn, struct cred *cr)
#endif
{
	pxnode::VTOPX(vp)->dispose(pp, fl, dn, cr);
}

static int
#if	SOL_VERSION >= __s11
px_setsecattr(vnode *vp, vsecattr_t *vsap, int flag, struct cred *cr,
    caller_context_t *)
#else
px_setsecattr(vnode *vp, vsecattr_t *vsap, int flag, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->setsecattr(vsap, flag, cr));
}

static int
#if	SOL_VERSION >= __s11
px_getsecattr(vnode *vp, vsecattr_t *vsap, int flag, struct cred *cr,
    caller_context_t *)
#else
px_getsecattr(vnode *vp, vsecattr_t *vsap, int flag, struct cred *cr)
#endif
{
	return (pxnode::VTOPX(vp)->getsecattr(vsap, flag, cr));
}

static int
#if	SOL_VERSION >= __s11
px_shrlock(vnode *vp, int cmd, shrlock *bfp, int flag, cred *cr,
    caller_context_t *)
{
	return (pxnode::VTOPX(vp)->vop_shrlock(cmd, bfp, flag, cr));
}
#else
#if	SOL_VERSION >= __s10
px_shrlock(vnode *vp, int cmd, shrlock *bfp, int flag, cred *cr)
{
	return (pxnode::VTOPX(vp)->vop_shrlock(cmd, bfp, flag, cr));
}
#else
px_shrlock(vnode *vp, int cmd, shrlock *bfp, int flag)
{
	return (pxnode::VTOPX(vp)->vop_shrlock(cmd, bfp, flag, NULL));
}
#endif
#endif
