//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef AIO_CALLBACK_IMPL_H
#define	AIO_CALLBACK_IMPL_H

#pragma ident	"@(#)aio_callback_impl.h	1.6	08/05/20 SMI"

#include <sys/aio_req.h>
#include <sys/fdbuffer.h>
#include <nfs/nfs.h>
#include <nfs/nfssys.h>
#include <nfs/nfs_clnt.h>
#include <nfs/lm.h>

#include <h/bulkio.h>
#include <h/px_aio.h>
#include <sys/list_def.h>
#include <sys/threadpool.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/bulkio/bulkio_impl.h>

// Call back object for the asyncio.
// Store the bulkio object and have a reference to it.
// Store the original aio request so that io completion
// can be notified to the kaio layer.

#define	PXFS_APAGEOUT	0x01
#define	PXFS_AIO	0x02
#define	PXFS_APAGEIN	0x03

#define	NO_EXCEPTION	false
#define	WITH_EXCEPTION	true

class ha_async : public defer_task {
public:
	~ha_async();
	ha_async(struct aio_req *, cred_t *, bulkio::in_aio_ptr, void *, int,
	    sol::nodeid_t, sol::pid_t);
	// This is a virtual function in the defer_task class.
	void execute();
private:
	bulkio::in_aio_ptr bulkioobj;
	struct aio_req *aiop;
	cred_t *cred_p;
	void *pxch;
	int iswrite;
	sol::nodeid_t nodeid;
	sol::pid_t pid;
};


class aio_callback_impl : public McServerof<pxfs_aio::aio_callback> {
public:
	aio_callback_impl(struct aio_req *, bool, ha_async *,
			    bulkio::in_aio_ptr);
	aio_callback_impl(vnode_t *, page_t *, u_offset_t, size_t,
	    int, void *);
	aio_callback_impl(vnode_t *, page_t *, u_offset_t, size_t,
	    bulkio::in_aio_pages *);
	~aio_callback_impl();
	void _unreferenced(unref_t arg);
	void aio_unref();
	void apageout_unref();
	void apagein_unref();
	void clear_cbkobj();
	void signal_invo_complete(bool exception_status);
	page_t *get_pages();
	void aio_read_complete(bulkio::in_aio_ptr aioobj, int32_t berror,
	    Environment &e);
	void aio_write_complete(int32_t berror, Environment &e);
	void aio_pageout_complete(uint32_t start_len, uint32_t done_len,
	    int32_t berror, Environment &e);
	void aio_pagein_complete(bulkio::in_aio_pages_ptr aiopgobj,
	    const bulkio::file_hole_list &holes, int32_t error, Environment &e);
	static void try_invalidate_pages(page_t *, int);
private:
	int type_flag;
	int _flags;
	page_t *_pp;
	vnode_t *_vp;
	u_offset_t _offset;
	size_t _io_len;
	struct aio_req *aio;
	struct aio_req_t *reqp;
	bool hadev;
	ha_async *haio;
	bulkio::in_aio_ptr _inaioclntobj;
	os::mutex_t apageout_mutex;
	bulkio::in_pages_ptr _pgobj;
	bulkio::in_aio_pages_ptr _pginobj;
	os::notify_t _invo_complete;
	bool _invo_complete_with_exception;
};

#include <pxfs/client/aio_callback_impl_in.h>

#endif	// AIO_CALLBACK_IMPL_H
