//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fsmgr_client_impl.cc	1.10	08/06/16 SMI"

#include <sys/errno.h>

#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include <pxfs/common/pxfslib.h>
#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/fsmgr_client_impl.h>
#include <pxfs/client/pxfobjplus.h>

fsmgr_client_impl::fsmgr_client_impl() :
	pxvfsp(NULL),
	servermgr(PXFS_VER::fsmgr_server::_nil())
{
}

fsmgr_client_impl::~fsmgr_client_impl()
{
	ASSERT(pxvfsp == NULL);
	CORBA::release(servermgr);
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// If we want to detect when the server for a file system has crashed,
// we will need to change fsmgr_client_impl to be derived from the refcnt
// class and make pxvPXFS_VER::fs_clientmgr be a reference counted pointer so
// there aren't problems with pxvfs having a pointer to us after
// _unreferenced() has been called.
//
void
fsmgr_client_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}

	pxvfsp_lock.wrlock();
	if (pxvfsp != NULL) {
		pxvfs *p = pxvfsp;
		pxvfsp = NULL;
		pxvfsp_lock.unlock();
		p->cleanup();
		VFS_RELE(p->get_vfsp());
		// Note: we will be deleted when unmount_succeeded() is called.
	} else {
		pxvfsp_lock.unlock();
		delete this;
	}
}

//
// Helper function to set the pxvfsp. This should only be used
// when establishing the pxvfs/fsmgr_client_impl linkage as part
// of the mount process. Also, after calling this, unmount_succeeded()
// should be called to release the linkage.
//
void
fsmgr_client_impl::set_pxvfsp(pxvfs *pxfsp,
    PXFS_VER::fsmgr_server_ptr servermgrp)
{
	pxvfsp_lock.wrlock();
	ASSERT(pxfsp != NULL);
	ASSERT(pxvfsp == NULL);
	ASSERT(CORBA::is_nil(servermgr));

	pxvfsp = pxfsp;
	VFS_HOLD(pxvfsp->get_vfsp());
	servermgr = PXFS_VER::fsmgr_server::_duplicate(servermgrp);
	pxvfsp_lock.unlock();
}

//
// This function is called on the fsmgr_client by the fsmgr_server during a
// switchover/failover.
//
void
fsmgr_client_impl::new_file_system_primary(uint32_t server_incn,
    Environment &_environment)
{
	ASSERT(pxvfsp != NULL);
	pxvfsp->new_file_system_primary(server_incn, _environment);
}

//
// Helper function for pxvfs to release the reference to the fsmgr server.
//
void
fsmgr_client_impl::unmount_succeeded()
{
	CORBA::release(servermgr);
	servermgr = PXFS_VER::fsmgr_server::_nil();

	//
	// If _unreferenced() has already been called, we delete ourself.
	// Otherwise, we will delete ourself in _unreferenced().
	//
	pxvfsp_lock.wrlock();
	if (pxvfsp != NULL) {
		pxvfs *p = pxvfsp;
		pxvfsp = NULL;
		pxvfsp_lock.unlock();
		VFS_RELE(p->get_vfsp());
	} else {
		pxvfsp_lock.unlock();
		delete this;
	}
}

//
// sync_filesystem - flush the client side caches.
//
void
fsmgr_client_impl::sync_filesystem(solobj::cred_ptr credobj, Environment &)
{
	//
	// XXX There are is a big hole here. This
	// invocation is used to flush the filesytem.
	// A client process doing a write can come in
	// after the purge_caches() is done and do a
	// write, thereby caching pages.
	// The only fix for this problem is to implement
	// lockfs on pxfs.
	// XXX Should also return error values.

	//
	// Take a pxvfsp_lock here to prevent race condtion
	// between sync and unmount.
	//
	pxvfsp_lock.rdlock();

	if (pxvfsp != NULL) {
		cred_t *crp = solobj_impl::conv(credobj);
		(void) pxvfsp->sync_filesystem(crp, false);
	}

	pxvfsp_lock.unlock();
}

//
// revoke_allocation - This function is called by the server to revoke
// any local pool of blocks. Also, it flushes all the dirty pages to disk
// by skipping the IO locked pages.
//
void
fsmgr_client_impl::revoke_allocation(solobj::cred_ptr credobj, Environment &)
{
	pxvfsp->revoke_blocks();

	//
	// Take a pxvfsp_lock here to prevent race condtion
	// between sync and unmount.
	//
	pxvfsp_lock.rdlock();

	if (pxvfsp != NULL) {
		cred_t *crp = solobj_impl::conv(credobj);
		(void) pxvfsp->sync_filesystem(crp, true);
	}

	pxvfsp_lock.unlock();
}

void
fsmgr_client_impl::inform_server_status(PXFS_VER::server_status_t status,
    Environment &)
{
	pxvfsp->set_server_status(status);
}

//
// recover_state - the server file object wants to recover state from
// from any client proxy file object. The server does not know
// whether this proxy file system has a corresponding active proxy file object.
//
void
fsmgr_client_impl::recover_state(const PXFS_VER::fobj_fid_t &client_fid,
    PXFS_VER::recovery_info_out recovery, Environment &env)
{
	//
	// Unpack the shipped FID information into a valid FID structure
	//
	fid_t	*fidp = (fid_t *)kmem_alloc(client_fid.fobjid_len +
			    sizeof (ushort_t), KM_SLEEP);

	bcopy((const char *)client_fid.fobjid_data, fidp->fid_data,
	    (size_t)client_fid.fobjid_len);

	fidp->fid_len = (ushort_t)client_fid.fobjid_len;

	// Find the proxy file for the specified FID
	pxfobj		*pxfobjp = pxvfsp->fid_to_proxy_file(fidp);

	PXFS_DBPRINTF(
	    PXFS_TRACE_FSMGR_C,
	    PXFS_AMBER,
	    ("recover_state(%p) pxfobj(%p)\n",
	    this, pxfobjp));

	// No longer need the FID
	kmem_free(fidp, client_fid.fobjid_len + sizeof (ushort_t));

	if (pxfobjp == NULL) {
		//
		// No proxy file is active for this fid
		//
		pxfslib::throw_exception(env, ENOENT);
		return;
	}
	//
	// The file server only tries to recover state from a file client
	// that can cache state.
	//
	ASSERT(pxfobjp->can_cache());
	pxfobjplus	*pxfobjplusp = (pxfobjplus *)pxfobjp;

	// Certain pxfobjplus objects that can cache state may not be
	// caching, if the object is constructed using unpack_vnode.
	// Specifically, the pxfobjplus objects that are created during
	// a rename of a directory at unixdir_impl, calls
	// pxfobj::unpack_vnode(), these do not have caching enabled. In these
	// cases we cannot recover state and must return error to that
	// effect.
	if (pxfobjplusp->is_cached()) {
		pxfobjplusp->recover_state(recovery, env);
	} else {
		pxfslib::throw_exception(env, ENOENT);
	}
	VN_RELE(pxnode::PXTOV(pxfobjplusp));
} //lint !e1746
