//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef LLM_CALLBACK_IMPL_H
#define	LLM_CALLBACK_IMPL_H

#pragma ident	"@(#)pxfs_llm_callback_impl.h	1.6	08/05/20 SMI"

#include <sys/os.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)

// forward declarations
class pxfs_llm_callback_impl;

// type for list of all pxfs_llm_callback objects
typedef IntrList<pxfs_llm_callback_impl, _DList> pxfs_llm_callback_list_t;

//
// Implementation for the pxfs_llm_callback interface.
// This is used by the pxfs server to wake up client threads sleeping for
// file locks.
//
class pxfs_llm_callback_impl : public McServerof<PXFS_VER::pxfs_llm_callback>,
	public _DList::ListElem {
public:
	pxfs_llm_callback_impl();
	~pxfs_llm_callback_impl();
	void _unreferenced(unref_t arg);

	// This is called when pxfobj is sleeping for a file lock to be granted.
	int wait_for_lock();

	// (Re)initializes this object
	void init();

	void signal(sol::error_t err);

	// pxfs_llm_callback::*
	void wakeup(Environment &_environment);
	//

	// Special values for 'error'
	enum {
		INITIAL = -1,
		RETRY_LOCK = -2
	};

private:
	os::mutex_t	sleep_lock;	// lock to protect the following
	os::condvar_t	sleep_cv;	// condition variable for sleeping
	int		error;		// reason for wakeup
};

#endif	// LLM_CALLBACK_IMPL_H
