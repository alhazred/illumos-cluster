//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef PXNODE_H
#define	PXNODE_H

#pragma ident	"@(#)pxnode.h	1.7	08/05/20 SMI"

#include <vm/page.h>
#include <sys/share.h>
#include <sys/vfs.h>
#include <sys/acl.h>

#include <sys/sol_version.h>

#if SOL_VERSION >= __s9
#define	PSARC_2001_100
#endif

#ifdef PSARC_2001_100 /* new s9 frlock interface */
#include <sys/flock.h>
#endif

#include <sys/knewdel.h>

#include <pxfs/common/pxnode_abstract.h>

#include "../version.h"
#include PXFS_IDL(pxfs)

//
// The class pxnode is a common parent to all proxy vnode types. It is
// an abstract class and its main function is to dispatch a Solaris vnode
// operation into the target C++ object method and to allocate space for
// the vnode that the kernel sees.
//
class pxnode : public pxnode_abstract, public knewdel {
public:
	pxnode(vfs_t *vfsp, enum vtype vnode_type, dev_t vnode_rdev,
	    uint_t vnode_flag);
	virtual ~pxnode();

	virtual pxnode_version	get_pxnode_version();

	// Convert vnode to pxnode
	static pxnode	*VTOPX(vnode_t *vp);

	// Convert pnode to vnode
	static vnode_t	*PXTOV(pxnode *pxp);

	//
	// Vnode operations dispatched from px_vnops.cc.
	//
	virtual int open(vnode **vpp, int flag, cred *cr) = 0;

	virtual int close(int flag, int count, offset_t offset, cred *cr) = 0;

	virtual int read(uio *uiop, int ioflag, cred *cr) = 0;

	virtual int write(uio *uiop, int ioflag, cred *cr) = 0;

	virtual int ioctl(int cmd, intptr_t arg, int flag, struct cred *cr,
		int *rvalp) = 0;

	// vop_setfl - not needed since generic fs_setfl() is OK.

	virtual int getattr(vattr *vap, int flags, cred *cr) = 0;

	virtual int setattr(vattr *vap, int flags, cred *cr) = 0;

	virtual int access(int mode, int flags, cred *cr) = 0;

	virtual int lookup(char *nm, vnode **vpp, pathname *pnp, int flags,
		vnode *rdir, cred *cr) = 0;

	virtual int create(char *name, vattr *vap, vcexcl excl, int mode,
		vnode **vpp, cred *cr, int flag) = 0;

	virtual int remove(char *nm, cred *cr) = 0;

	virtual int link(vnode *svp, char *tnm, cred *cr) = 0;

	virtual int rename(char *oldnm, vnode *newdvp, char *newnm,
		cred *cr) = 0;

	virtual int mkdir(char *nm, vattr *vap, vnode **vpp, cred *cr) = 0;

	virtual int rmdir(char *nm, vnode *cdir, cred *cr) = 0;

	virtual int readdir(uio *uiop, cred *cr, int &eof) = 0;

	virtual int symlink(char *nm, vattr *vap, char *target, cred *cr) = 0;

	virtual int readlink(uio *uiop, cred *cr) = 0;

	virtual int fsync(int syncflag, struct cred *cr) = 0;

	virtual void inactive() = 0;

	virtual int vop_fid(struct fid *fidp) = 0;

	virtual void rwlock(int write_lock) = 0;

	virtual void rwunlock(int write_lock) = 0;

	virtual int seek(offset_t ooff, offset_t *noffp) = 0;

	// cmp - not needed since generic fs_cmp() is OK.

#ifdef PSARC_2001_100 /* new s9 frlock interface */
	virtual int frlock(int cmd, struct flock64 *bfp, int flag,
		offset_t offset, flk_callback_t *, cred *cr) = 0;
#else
	virtual int frlock(int cmd, struct flock64 *bfp, int flag,
		offset_t offset, cred *cr) = 0;
#endif

	virtual int vop_shrlock(int cmd, struct shrlock *bfp,
		int flag, cred *cr) = 0;

	virtual int space(int cmd, struct flock64 *bfp, int flag,
		offset_t offset, cred *cr) = 0;

	virtual int realvp(vnode** vpp) = 0;

	virtual int getpage(offset_t off, size_t len, uint_t *protp,
		page_t *plarr[], size_t plsz, seg *segp, caddr_t addr,
		seg_rw rw, cred *cr) = 0;

	virtual int putpage(offset_t off, size_t len, int flags, cred *cr) = 0;

	virtual int map(offset_t off, as *as, caddr_t *addrp, size_t len,
		uchar_t prot, uchar_t maxprot, uint_t flags, cred *cr) = 0;

	virtual int addmap(offset_t off, as *as, caddr_t addr, size_t len,
		uchar_t prot, uchar_t maxprot, uint_t flags, cred *cr) = 0;

	virtual int delmap(offset_t off, as *as, caddr_t addr, size_t len,
		uint_t prot, uint_t maxprot, uint_t flags, cred *cr) = 0;

	// poll - not implemented yet XXX.

	// dump - not supported.

	virtual int pathconf(int cmd, ulong_t *valp, cred *cr) = 0;

	// pageio - not supported.

	// dumpctl - not supported.

	virtual void dispose(page_t *pp, int fl, int dn, cred *cr) = 0;

	virtual int setsecattr(vsecattr_t *vsap, int flag, cred *cr) = 0;

	virtual int getsecattr(vsecattr_t *vsap, int flag, cred *cr) = 0;

private:
	// Disallowed operations.
	pxnode & operator=(const pxnode &rsh);
	pxnode(const pxnode &rsh);
};

extern struct vnodeops	*px_vnodeopsp;

#include <pxfs/client/pxnode_in.h>

#endif	// PXNODE_H
