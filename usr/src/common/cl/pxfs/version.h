//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)version.h	1.6	08/05/20 SMI"

//
//		PXFS Version 1
//
// This file will intentionally be included multiple times for a single compile
// This supports cases where a piece of software has to deal with multiple
// versions of PXFS.
//

//
// Identify pxfs idl files for this version.
// Used for include statements.
//

#ifdef PXFS_IDL
#undef PXFS_IDL
#endif

#define	PXFS_IDL(NAME) <h/NAME ## _v1.h>

/* Identify pxfs IDL module for this version */

#ifdef PXFS_VER
#undef PXFS_VER
#endif

#define	PXFS_VER pxfs_v1

// Identify repl pxfs IDL module for this version

#ifdef REPL_PXFS_VER
#undef REPL_PXFS_VER
#endif

#define	REPL_PXFS_VER repl_pxfs_v1
