//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _FS_COLLECTION_IMPL_H
#define	_FS_COLLECTION_IMPL_H

#pragma ident	"@(#)fs_collection_impl.h	1.22	08/05/20 SMI"

#include <h/pxfs.h>
#include <orb/object/adapter.h>

//
// fs_collection_impl - there is one object of this type per node.
// This object acts as a local agent for the network lock manager,
// when the NLM wants to know the lock situation for a particular sysid.
//
class fs_collection_impl : public McServerof<fs::fs_collection> {
public:
	fs_collection_impl();

	~fs_collection_impl();

	void		_unreferenced(unref_t arg);

	static int	startup();

	static int	shutdown();

	bool		has_file_locks(int32_t sysid, int32_t chklck,
	    Environment &_environment);

private:
	// Disallow assignments and pass by value.
	fs_collection_impl(const fs_collection_impl &);
	fs_collection_impl &operator = (fs_collection_impl &);
};

#endif	// _FS_COLLECTION_IMPL_H
