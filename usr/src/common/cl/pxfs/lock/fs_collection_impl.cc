//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fs_collection_impl.cc	1.33	08/05/20 SMI"

#include <sys/flock.h>

#include <h/pxfs.h>
#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>

#include <pxfs/lock/lock_debug.h>
#include <pxfs/lock/fs_collection_impl.h>

//
// Effects: creates a fs_collection object and registers it with the
//   name server under the name 'fscollection.nodenumber.'
//
// static
int
fs_collection_impl::startup()
{
	Environment			e;
	CORBA::Exception		*ex;
	naming::naming_context_var	ctxp;
	char				name[22];

	fs::fs_collection_var	fs_collection_v =
		(new fs_collection_impl)->get_objref();

	// Format of name in name server is "fscollection.[node #]"
	os::sprintf(name, "fscollection.%d", orb_conf::node_number());
	ctxp = ns::root_nameserver();
	ctxp->rebind(name, fs_collection_v, e);
	if ((ex = e.exception()) != NULL) {
		// Ignore name not found exceptions which are normal.
		if (naming::not_found::_exnarrow(ex) == NULL) {
			LOCK_DBPRINTF(
			    LOCK_TRACE_FSCOLL,
			    LOCK_RED,
			    ("fs_collection:startup rebind failed\n"));
			return (ECOMM);
		}
	}
	return (0);
}

//
// Effects: unregisters the fs_collection object with the name server.
//
// static
int
fs_collection_impl::shutdown()
{
	Environment			e;
	naming::naming_context_var	ctxp;
	char				name[22];

	os::sprintf(name, "fscollection.%d", orb_conf::node_number());
	ctxp = ns::root_nameserver();			// get name server ref
	ctxp->unbind(name, e);
	return (0);
}

//
// Effects: This constructor creates an instance of a fs_collection_impl
//   object.
//
fs_collection_impl::fs_collection_impl()
{
}

//
// Effects: This destructor destroys a fs_collection_impl object.
//
fs_collection_impl::~fs_collection_impl()
{
}

void
fs_collection_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		//
		// _last_unref() should always be true since we don't use 0->1.
		// If we did, we would need some sort of lock here and
		// around the call to the name server to remove the reference
		// it holds.
		//
		ASSERT(0);
		return;
	}

	delete this;
}

bool
fs_collection_impl::has_file_locks(int32_t sysid, int32_t chklck, Environment &)
{
	int result = flk_sysid_has_locks(sysid, chklck);
	return (result != 0);
}
