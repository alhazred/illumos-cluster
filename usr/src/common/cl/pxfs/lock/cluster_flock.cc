//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cluster_flock.cc	1.6	08/05/20 SMI"

#include <pxfs/lock/cluster_flock.h>

extern void flk_state_transition_notify(
    lock_descriptor_t *lock, int old_state, int new_state);

//
// cl_flk_state_transition_notify - called by the local lock manager
// whenever the state of a PXFS lock changes.
//
// The function first determines which pxfs version is active.
//
// The called function dispatches the notifications to the
// appropriate function.
//
extern "C"
void
cl_flk_state_transition_notify(lock_descriptor_t *lock, int old_state,
    int new_state)
{
	ASSERT(IS_PXFS(lock));

	fobj_abstract	*fobj_ab_p = CL_FLK_GET_CONTEXT(lock)->fobj_abstract_p;

	ASSERT(fobj_ab_p->get_fobj_version() == fobj_abstract::VERSION_1);

	flk_state_transition_notify(lock, old_state, new_state);
}
