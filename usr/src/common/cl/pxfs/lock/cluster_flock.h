/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLUSTER_FLOCK_H
#define	_CLUSTER_FLOCK_H

#pragma ident	"@(#)cluster_flock.h	1.5	08/05/20 SMI"

#include <sys/flock_impl.h>
#include <sys/flock.h>
#include <sys/vfs.h>

#include <h/pxfs.h>
#include <h/pxfs_v1.h>
#include <pxfs/common/fobj_abstract.h>

/*
 * Structure to store locking information.  Note that this structure must fit
 * into 'flock.l_pad' which is defined as 'long l_pad[4]'.
 */
typedef struct lock_context {
	fobj_abstract	*fobj_abstract_p;

	union {
		fs::pxfs_llm_callback_ptr	cb_obj;
		pxfs_v1::pxfs_llm_callback_ptr	cb_obj_v1;
	};
} lock_context_t;

#define	CL_FLK_GET_CONTEXT(lock) \
	((lock_context_t *)(((lock)->l_flock).l_pad))

#endif	/* _CLUSTER_FLOCK_H */
