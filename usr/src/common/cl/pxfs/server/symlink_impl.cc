//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)symlink_impl.cc	1.13	08/05/20 SMI"

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/pathname.h>
#include <sys/debug.h>
#include <sys/uio.h>
#include <sys/file.h>

#include <sys/os.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/symlink_impl.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/fs_base_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//
// Constructor for the unreplicated CORBA implementation.
//
symlink_norm_impl::symlink_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp) :
	symlink_ii(fsp, vp, PXFS_VER::fobj_symbolic_link, fidp)
{
	set_fobj_ii(this);
}

//
// Constructor for the primary replicated CORBA implementation.
//
symlink_repl_impl::symlink_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp) :
    mc_replica_of<PXFS_VER::symbolic_link>(fsp->get_serverp()),
    symlink_ii(fsp, vp, PXFS_VER::fobj_symbolic_link, fidp)
{
	set_fobj_ii(this);
}

//
// Constructor for the secondary replicated CORBA implementation.
//
symlink_repl_impl::symlink_repl_impl(fs_ii *fsp,
    const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::symbolic_link_ptr obj) :
	symlink_ii(fsp, fobjid, PXFS_VER::fobj_symbolic_link),
	mc_replica_of<PXFS_VER::symbolic_link>(obj)
{
	set_fobj_ii(this);
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we lock the
// appropriate bucket lock while checking for this case.
// This kind of file object is always the primary.
// If we win the race, then
// do what is needed to delete the object.
//
void
symlink_norm_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	if (!fobj_released) {
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	}
	if (_last_unref(arg)) {
		unreferenced(bktno, false);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we acquire the appropriate
// locks while checking for this case. If we win the race, then
// do what is needed to delete the object.
//
void
symlink_repl_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	bool	fidlist_locked = false;

	if (fobj_released) {
		// No locking needed because not on any list

	} else if (get_fsp()->is_secondary()) {
		//
		// A secondary keeps objects on the fid list.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();
	} else if (primary_ready) {
		//
		// The file object is a ready primary.
		//
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	} else {
		//
		// The file object has not become a primary yet.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();

		int		error;
		vnode_t		*vnodep = fid_to_vnode(error);
		if (vnodep != NULL) {
			//
			// Grab the bucket lock.
			// It is possible someone may have just made this file
			// object a ready primary. But this will work in
			// in either case.
			//
			bktno = fs_implp->vp_to_bucket(vnodep);
			fs_implp->fobjlist_locks[bktno].wrlock();

			// Drop the hold from fid_to_vnode
			VN_RELE(vnodep);
		}
	}

	if (_last_unref(arg)) {
		//
		// Note that unreference processing will drop
		// the appropriate locks.
		//
		unreferenced(bktno, fidlist_locked);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
	if (fidlist_locked) {
		fs_implp->fidlock.unlock();
	}
}

void
symlink_ii::readlink(PXFS_VER::symbolic_link::target_t_out target,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t 	*crp = solobj_impl::conv(credobj);
	uio_t	auio;
	iovec_t	iov;
	PXFS_VER::symbolic_link::target_t	*targetbuf =
	    new PXFS_VER::symbolic_link::target_t(MAXPATHLEN);

	iov.iov_base = (caddr_t)targetbuf->buffer();
	iov.iov_len = MAXPATHLEN;

	auio.uio_iov = &iov;
	auio.uio_iovcnt = 1;
	auio.uio_loffset = 0;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_resid = MAXPATHLEN;
	auio.uio_fmode = 0;

#if	SOL_VERSION >= __s11
	int error = VOP_READLINK(get_vp(), &auio, crp, NULL);	//lint !e666
#else
	int error = VOP_READLINK(get_vp(), &auio, crp);		//lint !e666
#endif

	if (error == 0) {
		targetbuf->length((uint_t)(MAXPATHLEN - auio.uio_resid));
		target = targetbuf;
	} else {
		delete targetbuf;
		pxfslib::throw_exception(_environment, error);
	}
}

//
// symlink_norm_impl methods
//

handler *
symlink_norm_impl::get_handler()
{
	return (_handler());
}

// Return a new fobj CORBA reference to this
PXFS_VER::fobj_ptr
symlink_norm_impl::get_fobjref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
REPL_PXFS_VER::fs_replica_ptr
symlink_norm_impl::get_ckpt()
{
	ASSERT(0);
	return (REPL_PXFS_VER::fs_replica::_nil());
}

//
// Add a commit checkpoint.
//
void
symlink_norm_impl::commit(Environment &)
{
}

// Methods supporting IDL operations
/* fobj */

PXFS_VER::fobj_type_t
symlink_norm_impl::get_fobj_type(Environment &_environment)
{
	return (symlink_ii::get_fobj_type(_environment));
}

void
symlink_norm_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
symlink_norm_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
symlink_norm_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::access(accmode, accflags, credobj, _environment);
}

void
symlink_norm_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
symlink_norm_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::get_secattributes(sattr, secattrflag, credobj,
	    _environment);
} //lint !e1746

void
symlink_norm_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::set_secattributes(sattr, secattrflag, credobj,
	    _environment);
}

void
symlink_norm_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::pathconf(cmd, result, credobj, _environment);
}

void
symlink_norm_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	symlink_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
symlink_norm_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result,
	    credobj, _environment);
}

void
symlink_norm_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	symlink_ii::frlock(cmd, lock_info, flag, off, credobj, cb,
	    _environment);
}

void
symlink_norm_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	symlink_ii::frlock_cancel_request(cb, _environment);
}

void
symlink_norm_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	symlink_ii::frlock_execute_request(cb, _environment);
}

void
symlink_norm_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
symlink_norm_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::remove_locks(cpid, sysid, credobj, _environment);
}

void
symlink_norm_impl::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	fobjplus_ii::cache_new_client(binfo, client1_p, client2_p,
	    _environment);
} //lint !e1746

void
symlink_repl_impl::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_new_client(binfo, client1_p, client2_p,
	    _environment);
} //lint !e1746

void
symlink_norm_impl::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	fobjplus_ii::cache_remove_client(client_p, _environment);
}

void
symlink_norm_impl::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_get_all_attr(credobj, rights, attributes,
	    seqnum, server_incarn, _environment);
}

void
symlink_norm_impl::cache_write_all_attr(sol::vattr_t &attributes,
    int32_t attrflags, bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_write_all_attr(attributes, attrflags, discard, sync,
	    credobj, server_incarn, _environment);
}

void
symlink_norm_impl::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	fobjplus_ii::cache_attr_drop_token(server_incarn, _environment);
}

void
symlink_norm_impl::cache_set_attributes(sol::vattr_t &wb_attributes,
    int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_set_attributes(wb_attributes, wb_attrflags,
	    attributes, attrflags, credobj, seqnum, server_incarn,
	    _environment);
}

void
symlink_norm_impl::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	fobjplus_ii::cache_access(accmode, accflags, credobj, allowed,
	    seqnum, _environment);
}

/* symbolic_link */

void
symlink_norm_impl::readlink(PXFS_VER::symbolic_link::target_t_out target,
    solobj::cred_ptr credobj, Environment &_environment)
{
	symlink_ii::readlink(target, credobj, _environment);
} //lint !e1746

// End of methods supporting IDL operations

//
// symlink_repl_impl methods
//

handler *
symlink_repl_impl::get_handler()
{
	return (_handler());
}

// Return a new fobj CORBA reference to this
PXFS_VER::fobj_ptr
symlink_repl_impl::get_fobjref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
REPL_PXFS_VER::fs_replica_ptr
symlink_repl_impl::get_ckpt()
{
	return ((repl_pxfs_server*)(get_provider()))->
	    get_checkpoint_fs_replica();
}

//
// Add a commit checkpoint.
//
void
symlink_repl_impl::commit(Environment &e)
{
	add_commit(e);
}

// Methods supporting IDL operations
/* fobj */

PXFS_VER::fobj_type_t
symlink_repl_impl::get_fobj_type(Environment &_environment)
{
	if (is_dead(_environment)) {
		return (PXFS_VER::fobj_fobj);
	}
	return (symlink_ii::get_fobj_type(_environment));
}

void
symlink_repl_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
symlink_repl_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
symlink_repl_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::access(accmode, accflags, credobj, _environment);
}

void
symlink_repl_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
symlink_repl_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::get_secattributes(sattr, secattrflag, credobj,
	    _environment);
} //lint !e1746

void
symlink_repl_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::set_secattributes(sattr, secattrflag, credobj,
	    _environment);
}

void
symlink_repl_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::pathconf(cmd, result, credobj, _environment);
}

void
symlink_repl_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
symlink_repl_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result,
	    credobj, _environment);
}

void
symlink_repl_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::frlock(cmd, lock_info, flag, off, credobj, cb,
	    _environment);
}

void
symlink_repl_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::frlock_cancel_request(cb, _environment);
}

void
symlink_repl_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::frlock_execute_request(cb, _environment);
}

void
symlink_repl_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
symlink_repl_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::remove_locks(cpid, sysid, credobj, _environment);
}

void
symlink_repl_impl::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_remove_client(client_p, _environment);
}

void
symlink_repl_impl::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_get_all_attr(credobj, rights, attributes,
	    seqnum, server_incarn, _environment);
}

void
symlink_repl_impl::cache_write_all_attr(sol::vattr_t &attributes,
    int32_t attrflags, bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_write_all_attr(attributes, attrflags, discard, sync,
	    credobj, server_incarn, _environment);
}

void
symlink_repl_impl::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_attr_drop_token(server_incarn, _environment);
}

void
symlink_repl_impl::cache_set_attributes(sol::vattr_t &wb_attributes,
    int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_set_attributes(wb_attributes, wb_attrflags,
	    attributes, attrflags, credobj, seqnum, server_incarn,
	    _environment);
}

void
symlink_repl_impl::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_access(accmode, accflags, credobj, allowed,
	    seqnum, _environment);
}

/* symbolic_link */

void
symlink_repl_impl::readlink(PXFS_VER::symbolic_link::target_t_out target,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	symlink_ii::readlink(target, credobj, _environment);
} //lint !e1746

// End of methods supporting IDL operations
