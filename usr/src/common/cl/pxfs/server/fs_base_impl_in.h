//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fs_base_impl_in.h	1.6	08/05/20 SMI"

//
// release_info_t inline methods
//

inline
release_info_t::release_info_t()
{
	vp = NULL;
	delete_fidp = NULL;
	delete_num = 0;
}

inline
release_info_t::~release_info_t()
{
	if (delete_fidp != NULL) {
		//
		// Note that this was allocated in fobj_ii::fobj_ii() and
		// transferred to us by
		// fobj_ii::cleanup_and_get_release_info().
		//
		kmem_free(delete_fidp, delete_fidp->fid_len +
		    sizeof (ushort_t));
	}
}

//
// fs_base_ii inline methods
//

inline bool
fs_base_ii::is_secondary() const
{
	return (!is_primary);
}

inline bool
fs_base_ii::suspend_locking_ckpts() const
{
	//
	// is_external_nlm_event is set by the fs_collection object during
	// an NLM state transition.
	//
	return (_suspend_locking_ckpts | is_external_nlm_event);
}

//
// Hashing function used to hash into allfobj_list[].
//
inline int
fs_base_ii::vp_to_bucket(vnode_t *vp)
{
	uintptr_t a = (uintptr_t)vp;
	return ((int)(((a >> 6) + (a >> 10) + (a >> 12)) % allfobj_buckets));
}

//
// Increment the do_delete count
//
inline void
fs_base_ii::inc_dodelete_cnt()
{
	do_delete_cnt_lock.lock();
	do_delete_cnt++;
	do_delete_cnt_lock.unlock();
}

//
// Decrement the do_delete count and broadcast if the count is zero.
//
inline void
fs_base_ii::dec_dodelete_cnt()
{
	do_delete_cnt_lock.lock();
	do_delete_cnt--;
	if (do_delete_cnt == 0) {
		do_delete_wait_cv.broadcast();
	}
	do_delete_cnt_lock.unlock();
}
