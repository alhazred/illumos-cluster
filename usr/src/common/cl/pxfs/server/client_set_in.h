//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)client_set_in.h	1.9	08/05/20 SMI"

inline
client_set::client_entry::client_entry() :
	_DList::ListElem(this),
	refcnt(0)
{
}

inline
client_set::client_entry::client_entry(ID_node &client_node,
    PXFS_VER::fobj_client_ptr clint_p) :
	_DList::ListElem(this),
	client_p(clint_p),
	node(client_node),
	refcnt(0)
{
	PXFS_VER::fobj_client::_duplicate(client_p);
}

inline bool
client_set::client_entry::is_dead()
{
	return (CORBA::is_nil(client_p));
}

inline bool
client_set::client_entry::is_zombie()
{
	return (node.ndid > NODEID_MAX);
}

inline void
client_set::client_entry::clear()
{
	CORBA::release(client_p);
	client_p = nil;
}

inline void
client_set::client_entry::hold()
{
	ASSERT(refcnt < UINT_MAX);
	os::atomic_add_32(&refcnt, 1);
}

//
// release - decrements the reference count.
// We do not destroy the object when the count goes to zero.
//
inline void
client_set::client_entry::release()
{
	ASSERT(refcnt > 0);
	os::atomic_add_32(&refcnt, -1);
}


inline
client_set::iterator::iterator(IntrList<client_entry, _DList> &client_list) :
	iter(client_list)
{
}

inline
client_set::iterator::~iterator()
{
}

inline client_set::client_entry *
client_set::iterator::get_current()
{
	return (iter.get_current());
}

inline void
client_set::iterator::advance()
{
	iter.advance();
}

inline
client_set::client_set() :
	purge_in_progress(false)
{
}

// Return a bit mask for the set of clients
inline nodeset
client_set::client_nodeset()
{
	return (clients);
}
