//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fobj_impl_in.h	1.6	08/05/20 SMI"

inline bool
fobj_ii::is_replicated() const
{
	return (fs_implp->is_replicated());
}

inline bool
fobj_ii::is_active() const
{
	return (primary_ready);
}

inline bool
fobj_ii::is_released() const
{
	return (fobj_released);
}

inline void
fobj_ii::set_secondary()
{
	ASSERT(primary_ready);
	VN_RELE(back_obj.vnodep);
	back_obj.vnodep = NULL;
	primary_ready = false;
}

inline fs_ii *
fobj_ii::get_fsp() const
{
	ASSERT(fobjtype != PXFS_VER::fobj_io);
	return ((fs_ii *)fs_implp);
}

inline void
fobj_ii::set_vp(vnode_t *vp)
{
	ASSERT(!primary_ready);
	back_obj.vnodep = vp;
}

inline vnode_t *
fobj_ii::get_vp() const
{
	return (back_obj.vnodep);
}

inline fid_t *
fobj_ii::get_fidp() const
{
	return (back_obj.fidp);
}

inline frange_lock *
fobj_ii::get_frange_lock() const
{
	return (fr_lockp);
}

//
// This is defined to match the "make filters" output. Otherwise,
// we could make the CORBA interface call get_ftype() without the
// environment.
//
inline PXFS_VER::fobj_type_t
fobj_ii::get_fobj_type(Environment &)
{
	return (fobjtype);
}

inline PXFS_VER::fobj_type_t
fobj_ii::get_ftype() const
{
	return (fobjtype);
}

inline uint64_t
fobj_ii::get_delete_num() const
{
	return (delete_num);
}

inline void
fobj_ii::set_delete_num(uint64_t num)
{
	delete_num = num;
}

inline void
fobj_ii::range_lock()
{
}

inline void
fobj_ii::range_unlock()
{
}

inline void
fobj_ii::set_fobj_ii(PXFS_VER::fobj_ptr objp)
{
	objp->_handler()->set_cookie((void *)this);
}
