//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FOBJ_IMPL_H
#define	FOBJ_IMPL_H

#pragma ident	"@(#)fobj_impl.h	1.11	08/05/20 SMI"

#include <sys/vnode.h>
#include <sys/vfs.h>		// for fid_t

#include <h/repl_dc.h>
#include <h/addrspc.h>
#include <sys/sol_version.h>
#include <sys/list_def.h>
#include <orb/infrastructure/copy.h>

#include "../version.h"
#include <pxfs/common/fobj_abstract.h>
#include PXFS_IDL(pxfs)
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/client_set.h>
#include <pxfs/server/cl_flock.h>

// Forward declaration of global namespace class
class handler;

// Forward declarations.
class fobj_ii;

// Type for list of fobj_ii objects.
typedef IntrList<fobj_ii, _DList> fobj_ii_list_t;

//
// Lock Order Hierarchy for File Objects
//
// This section provides a partial enumeration of the
// order in which locks must be acquired for file objects.
//
// Note: not all locks are defined in this file.
// Some locks are defined in child classes.
//
// The list starts with the lock that must be acquired first.
//
// Lock   Lock Name	File Defined		Description
// level  Name
// --	---------	------------		-----------
// 1	range_lock	file_impl		protects page data
// 1	dir_lock	unixdir_impl		protects directory info
// 2	attr_lock	fobj_impl		protects file attributes
// 3	list_lock	client_set		protects list of caching clients
//
// Note: locks at the same level, such as range_lock and dir_lock do
// not exist for the same file, and thus do not conflict.

//
// fobj_ii - this class provides support to individual files
// of any type. This class has code that is shared
// between file objects belonging to ordinary and highly
// available file systems.
//
class fobj_ii :
	public _DList::ListElem,
	public fobj_abstract
{
	friend class frange_lock;
public:
	//
	// Note that the constructors are declared below in the protected
	// section.
	//

	virtual	~fobj_ii();
	void	unreferenced(int bktno, bool fidlist_locked);

	virtual fobj_version	get_fobj_version();

	// Accessor functions to avoid use of friend.
	fs_ii			*get_fsp() const;
	vnode_t			*get_vp() const;
	fid_t			*get_fidp() const;
	frange_lock		*get_frange_lock() const;
	PXFS_VER::fobj_type_t	get_ftype() const;
	uint64_t		get_delete_num() const;
	void			set_delete_num(uint64_t num);

	// Return a new fobj CORBA reference to this.
	virtual PXFS_VER::fobj_ptr	get_fobjref() = 0;

	virtual handler		*get_handler() = 0;

	//
	// Return a CORBA pointer to the checkpoint interface.
	// Note that this can't be called for fobj_io objects.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt() = 0;

	//
	// Return a CORBA pointer to the I/O checkpoint interface.
	// Note that this can only be called for fobj_io objects.
	//
	virtual repl_dc::repl_dev_server_ckpt_ptr	get_io_ckpt();

	// Add a commit checkpoint.
	virtual	void	commit(Environment &e) = 0;

	//
	// Set of virtual functions to access implementations in file_impl.
	// Note that the lock order is range_lock() then lock_attributes()
	//
	virtual void	range_lock();
	virtual void	range_unlock();

	// Return true if this is a replicated object.
	bool		is_replicated() const;

	// Return true if this object has a valid vnode.
	bool		is_active() const;

	//
	// Return true if this object has been released
	// (see fs_base_ii::remove_fobj_locked()).
	//
	bool		is_released() const;

	//
	// Return true if this is a dead replicated file system object
	// (i.e., there was a fatal error in convert_to_primary() or
	// convert_to_secondary() but the replica manager still thinks
	// the service is alive).
	// If true, the environment is also set to return an exception.
	//
	virtual bool	is_dead(Environment &env);

	// Returns whether the client could cache information
	virtual bool	can_cache();

	virtual void	undo_register_client(Environment &env);

	//
	// This method gets invoked on the secondary via a checkpoint by
	// frlock() and remove_locks() on the primary.
	//
	void		ckpt_locks(const REPL_PXFS_VER::lock_info_seq_t &locks);

	// Helper function for repl_pxfs_server::ckpt_fobj_state().
	void		ckpt_fobj_state(uint64_t id);

	// Helper function for dumping state to a new secondary.
	virtual void	dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    PXFS_VER::fobj_ptr fobjp, Environment &env);

	// Convert an fobj from secondary to primary representation.
	virtual int	convert_to_primary();

	// Convert an fobj from primary to secondary representation.
	virtual int	convert_to_secondary();

	// Convert an fobj from secondary to spare representation.
	virtual void	convert_to_spare();

	// Obtain the vnode for this file based upon the FID
	vnode_t		*fid_to_vnode(int &error);

	//
	// Downgrade attribute caches to not conflict with 'attr_rights'.
	//
	virtual sol::error_t	downgrade_attr_all(
			    PXFS_VER::attr_rights req_rights,
			    bool inval_access_check, nodeid_t skip_node);

	// Function to convert CORBA fobj to fobj_ii*
	static fobj_ii	*get_fobj_ii(PXFS_VER::fobj_ptr objp);

	// Helper functions to release the underlying vnode.
	void		cleanup_and_get_release_info(release_info_t &rinfo);

	static void	do_delete(fs_base_ii *fsbp,
			    const release_info_t &rinfo);

	// Create bind info for the fobj.
	virtual PXFS_VER::fobj_client_ptr	get_bind_info(
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client_p, cred_t *credp,
	    Environment &env);

	// Common code for fobj_ii and fobjplus_ii.
	void	get_secattr_common(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &env);

	//
	// Helper function for _FIOISBUSY.
	// Returns
	//	1 =+ busy
	//	0 == not busy
	//	-1 == error
	//
	virtual int	is_it_busy();

	//
	// Call to test for active locks so we can decide when to convert fids
	// i.e. where the fobj has no associated locks can be converted lazily
	//
	bool	has_active_locks();

protected:
	// Unreplicated or primary constructor for the internal implementation.
	fobj_ii(fs_base_ii *fsp, vnode_t *vnodep, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation.
	fobj_ii(fs_base_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_type_t ftype);

	//
	// Function used by _impl constructor to initialize the value returned
	// by get_fobj_ii().
	//
	void	set_fobj_ii(PXFS_VER::fobj_ptr objp);

	// Helper functions for io_repl_impl::convert_to_primary().
	void	set_vp(vnode_t *vnodep);
	void	set_secondary();

	// Methods supporting IDL operations for fobj

	virtual PXFS_VER::fobj_type_t		get_fobj_type(Environment &env);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags, solobj::cred_ptr credobj, Environment &env);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj, Environment &env);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj, Environment &env);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &env);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);
	//

private:
#ifdef DEBUG
	// Testing routine.
	int	testioctl(int32_t iocmd, sol::intptr_t arg, int32_t &result,
	    copy_args &args, cred_t *credp);
#endif

private:
	// This is used to save the "file name" in the "deleted directory".
	uint64_t		delete_num;

	// Synchronizes creation of file range lock support object
	os::mutex_t		fr_lock;

public:
	//
	// Lock must be held when changing attributes for this file
	//
	// This lock is held for very long periods of time.
	// An adaptive mutex would spin for long periods of time.
	// So we use a rwlock to avoid the spin.
	//
	os::rwlock_t		attr_lock;

	//
	// File system or device server for this fobj.
	//
	fs_base_ii		*fs_implp;

	//
	// The file range lock support consumes considerable space.
	// So we encapsulate the file range lock in a class, and
	// only enable the support when needed.
	//
	frange_lock		*fr_lockp;

protected:
	//
	// On a nonreplicated or primary node, we use the vnodep to reference
	// the underlying object.  On a secondary node, we use the fid.
	//
	struct {
		fid_t		*fidp;		// fid data for switchover
		vnode_t		*vnodep;	// underlying file object
	} back_obj;

	// The specific type of fobj.
	PXFS_VER::fobj_type_t		fobjtype;

	//
	// True when primary is ready
	// A real vnode exists for this object,
	// and client information is current.
	//
	bool			primary_ready;

	//
	// Set to true if fobj has been released
	// (see fs_base_ii::remove_fobj_locked()).
	// Protected by fs_base_ii::allfobj_lock.
	//
	bool			fobj_released;

private:
	//
	// Set to true if in the unreferenced routine - prevents checkpointing
	// in unref() context..
	//
	bool			processing_unref;

private:
	// Disallow assignments and pass by value.
	fobj_ii(const fobj_ii &);
	fobj_ii &operator = (fobj_ii &);
};

//
// Note: there are no fobj_norm_impl, fobj_repl_impl classes providing the
// CORBA interface because no fobj's ever get instantiated.  Only the
// derived CORBA classes (e.g., io) are instantiated.
//

#include <pxfs/server/fobj_impl_in.h>

// Returns true if vnode represents a special file
#define	IS_SPECIAL(vnodep)	((vnodep)->v_type == VCHR || \
				(vnodep)->v_type == VBLK || \
				(vnodep)->v_type == VFIFO)

#endif	// FOBJ_IMPL_H
