//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fobjplus_impl_in.h	1.8	08/05/20 SMI"

//
// get_fplus_objref - Return a new fobjplus CORBA reference.
//
inline PXFS_VER::fobjplus_ptr
fobjplus_ii::get_fplus_objref()
{
	return ((PXFS_VER::fobjplus_ptr)get_fobjref());
}

inline void
fobjplus_ii::do_purge_dead_clients()
{
	clientset.do_purge_dead_clients(this);
}

inline void
fobjplus_ii::set_attr_rights(nodeid_t node, PXFS_VER::attr_rights rights)
{
	fobjplus_priv_lock.lock();
	set_attr_rights_locked(node, rights);
	fobjplus_priv_lock.unlock();
}
