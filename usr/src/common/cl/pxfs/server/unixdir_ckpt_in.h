//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)unixdir_ckpt_in.h	1.9	08/05/20 SMI"

//
// Return the checkpointed state for whether the file existed
// before the directory operation or not.
// This is called only on the new primary.
//
inline bool
unixdir_state::file_exists() const
{
	return (exists);
}

//
// This function gets the checkpointed value of the target fobj.
// This is called only on the primary.
//
inline PXFS_VER::fobj_ptr
unixdir_state::get_target_fobj()
{
	return (PXFS_VER::fobj::_duplicate(targetobj));
}

//
// Return the checkpointed error value in the state.
// This is called only on the primary.
//
inline sol::error_t
unixdir_state::get_error() const
{
	return (error);
}

//
// Record the checkpointed error value in the state.
// This is called only on the secondary.
//
inline void
unixdir_state::ckpt_error_return(sol::error_t err)
{
	cur_state = COMMITTED;
	error = err;
}

// Get the Id used when renaming a file object about to be removed
inline uint64_t
unixdir_state::get_delete_id()
{
	return (delete_id);
}

//
// This function gets the checkpointed value of the return fobj.
// This is called only on the primary.
//
inline PXFS_VER::fobj_ptr
unixdir_state::get_ret_fobj()
{
	return (PXFS_VER::fobj::_duplicate(retobj));
}

inline void
unixdir_state::get_fobjinfo(PXFS_VER::fobj_info &finfo)
{
	finfo = retinfo;
}

inline void
unixdir_state::set_fobjinfo(const PXFS_VER::fobj_info &finfo)
{
	retinfo = finfo;
}

//
// This function sets the checkpointed value for the return fobj.
// This is called only on the secondary.
//
inline void
unixdir_state::ckpt_fobj_return(PXFS_VER::fobj_ptr fobjp,
    const PXFS_VER::fobj_info &finfo)
{
	retobj = PXFS_VER::fobj::_duplicate(fobjp);
	retinfo = finfo;
	cur_state = COMMITTED;
}

//
// Handle checkpoint (commit) after deleting a file.
// This is called only on the secondary.
//
inline void
unixdir_state::ckpt_delete_fobj(uint64_t deletion_id)
{
	ASSERT(!CORBA::is_nil(targetobj));
	fobj_ii		*fobjp = fobj_ii::get_fobj_ii(targetobj);
	fobjp->set_delete_num(deletion_id);
	fobjp->get_fsp()->set_deletecnt(deletion_id);
	cur_state = COMMITTED;
}

//
// get_unixdir_state - obtains the unixdir transaction state
//
// This function returns the transaction state progress
// (START, CHECKPOINTED, COMMITTED).
//
// The saved state pointer (or NULL if START) is also returned via 'statep_ret'.
//
inline unixdir_state::progress
unixdir_state::get_unixdir_state(Environment &env, unixdir_state *&statep_ret)
{
	// Check to see if we have saved state
	primary_ctx	*ctxp = primary_ctx::extract_from(env);

	//
	// Since this can be called from the non-replicated code, the
	// transaction context can be NULL.
	//
	unixdir_state	*saved_statep = NULL;
	if (ctxp != NULL) {
		saved_statep = (unixdir_state *)ctxp->get_saved_state();
	}

	if (saved_statep == NULL) {
		// This is the start of a new transaction.
		statep_ret = NULL;
		return (START);
	}

	//
	// We have saved state so this is a retry operation from a failed
	// primary (old) on us (the new primary).
	//
	statep_ret = saved_statep;
	return (saved_statep->cur_state);
}
