//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fsmgr_server_impl.cc	1.6	08/05/20 SMI"

#include <sys/flock.h>
#include <sys/debug.h>

#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>
#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/server/fsmgr_server_impl.h>
#include <pxfs/server/repl_pxfs_server.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

fsmgr_server_ii::fsmgr_server_ii(fs_ii *fsp,
    PXFS_VER::fsmgr_client_ptr clientmgr,
    nodeid_t nid) :
	fs_iip(fsp), prov_common(clientmgr), cleanedup(false)
{
	fs_iip->hold();
	ASSERT(nid != NODEID_UNKNOWN);
	nodeid = nid;
}

fsmgr_server_ii::~fsmgr_server_ii()
{
	fs_iip->rele();
}	//lint !e1740

fsmgr_server_norm_impl::fsmgr_server_norm_impl(fs_ii *fsp,
	PXFS_VER::fsmgr_client_ptr clientmgr, nodeid_t nid) :
	fsmgr_server_ii(fsp, clientmgr, nid)
{
	// LINTED: Call to virtual function within a contructor or destructor.
	_handler()->set_cookie((void *)(fsmgr_server_ii *)this);
}

//
// Constructor for the primary replicated CORBA implementation
//
fsmgr_server_repl_impl::fsmgr_server_repl_impl(fs_ii *fsp,
    PXFS_VER::fsmgr_client_ptr clientmgr, nodeid_t nid) :
	fsmgr_server_ii(fsp, clientmgr, nid),
    mc_replica_of<PXFS_VER::fsmgr_server>(fsp->get_serverp())
{
	// LINTED: Call to virtual function within a contructor or destructor.
	_handler()->set_cookie((void *)(fsmgr_server_ii *)this);
}

//
// Constructor for the secondary replicated CORBA implementation
//
fsmgr_server_repl_impl::fsmgr_server_repl_impl(fs_ii *fsp,
    PXFS_VER::fsmgr_client_ptr clientmgr, nodeid_t nid,
    PXFS_VER::fsmgr_server_ptr obj) :
	fsmgr_server_ii(fsp, clientmgr, nid),
	mc_replica_of<PXFS_VER::fsmgr_server>(obj)
{
	// LINTED: Call to virtual function within a contructor or destructor.
	_handler()->set_cookie((void *)(fsmgr_server_ii *)this);
}

void
fsmgr_server_norm_impl::_unreferenced(unref_t arg)
{
	// Lock the provider list so new references aren't generated.
	fs_iip->get_fsmgr_set().lock_provider_list();

	// If this is the last reference, remove ourself and clean up.
	if (_last_unref(arg)) {
		// Remove ourself from the provider set and unlock.
		fs_iip->get_fsmgr_set().remove_locked(this);
		fs_iip->get_fsmgr_set().unlock_provider_list();

		unreferenced();
		return;
	}

	fs_iip->get_fsmgr_set().unlock_provider_list();
}

void
fsmgr_server_repl_impl::_unreferenced(unref_t arg)
{
	// Lock the provider list so new references aren't generated.
	fs_iip->get_fsmgr_set().lock_provider_list();

	// If this is the last reference, remove ourself and clean up.
	if (_last_unref(arg)) {
		// Remove ourself from the provider set and unlock.
		fs_iip->get_fsmgr_set().remove_locked(this);
		fs_iip->get_fsmgr_set().unlock_provider_list();

		unreferenced();
		return;
	}

	fs_iip->get_fsmgr_set().unlock_provider_list();
}

//
// This is called when the client that has this file system mounted,
// has crashed or released its CORBA reference to us (unmount).
//
void
fsmgr_server_ii::unreferenced()
{
	FAULTPT_PXFS(FAULTNUM_PXFS_FSMGRUNREF_S_O, FaultFunctions::generic);

	cleanup_llm_locks();

	delete this;
}

//
// Return a new PXFS_VER::fsmgr_server CORBA reference.
// This method should be locked by the caller.
//
PXFS_VER::fsmgr_server_ptr
fsmgr_server_ii::get_fsmgrref()
{
	ASSERT(fs_iip->get_fsmgr_set().provider_list_lock_held());
	CORBA::Object_ptr obj_p = get_provref();
	ASSERT(!CORBA::is_nil(obj_p));
	//
	// We cheat and avoid the _narrow() since we know this is a
	// fsmgr_server object.
	//
	return ((PXFS_VER::fsmgr_server_ptr)obj_p);
}

//
// Return a fsmgr_server_ii * from the CORBA pointer.
//
fsmgr_server_ii *
fsmgr_server_ii::get_fsmgr_server_ii(CORBA::Object_ptr objp)
{
	if (CORBA::is_nil(objp)) {
		return (NULL);
	}
	void *p = objp->_handler()->get_cookie();
	ASSERT(p != NULL);
	return ((fsmgr_server_ii *)(p));
}

//
// Called to cleanup the locks held by this fsmgr_server's
// corresponding client in the LLM.  This call is idempotent.
//
void
fsmgr_server_ii::cleanup_llm_locks()
{
	cleanup_lock.lock();

	if (cleanedup) {
		cleanup_lock.unlock();
		return;
	}

	cleanedup = true;

	//
	// Delete locks held by other nodes.
	// We shouldn't need to clean up locks on local file systems since
	// unmount can only happen if all vnodes are inactive which will
	// have called cleanlocks().
	//
	if (nodeid != orb_conf::node_number()) {
		//
		// XXX this may take a while so we might want a separate
		// thread to do cleanup.
		// XXX Also, since we are removing stuff based on the nodeid
		// of the pxfs client, it could have rebooted and added
		// some locks before we got this unreferenced()
		// Remove locks from the `granted_list', `processed_list', and
		// the LLM.
		//
		fs_iip->cleanup_locks(nodeid);
	}

	if (!fs_iip->is_secondary()) {
		//
		// On the primary, we must change the state of the NLM server
		// in the registry object maintained by the LLM to indicate that
		// this NLM is no longer known by this LLM (unregistered).
		//
		cl_flk_change_nlm_state_to_unknown((int)nodeid);
	}

	cleanup_lock.unlock();
}

//
// Called by the FS object to pre-empt the cleanup of locks by stale
// fsmgr_server objects.  See comments in fs_ii::fsmgr_factory().
//
void
fsmgr_server_ii::mark_llm_cleanedup()
{
	cleanup_lock.lock();
	cleanedup = true;
	cleanup_lock.unlock();
}

//
// Helper function for dumping state to a new secondary.
//
void
fsmgr_server_ii::dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
    Environment &env)
{
	fs_iip->get_fsmgr_set().lock_provider_list();
	PXFS_VER::fsmgr_server_var servermgr = get_fsmgrref();
	fs_iip->get_fsmgr_set().unlock_provider_list();
	ckptp->ckpt_new_fsmgr(servermgr, get_clientmgr(), nodeid, env);
}
