//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fs_dependent_impl.cc	1.15	08/06/16 SMI"

#include <sys/mntent.h>

#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/fs_dependent_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/ufs_dependent_impl.h>
#include <pxfs/server/hsfs_dependent_impl.h>

#ifndef VXFS_DISABLED
#include <pxfs/server/vxfs_dependent_impl.h>
#endif

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

extern "C" void set_server_copyops(void);

fs_dependent_impl default_fs_dependent_impl;

// Constructor.
fs_dependent_impl::fs_dependent_impl()
{
	// Empty
}

// Virtual destructor.
fs_dependent_impl::~fs_dependent_impl()
{
	// Empty
}

void
fs_dependent_impl::new_fobj(fobj_ii *)
{
	// Empty
}

// We care about this only for UFS. Return 0 for other filesystems.
int
fs_dependent_impl::fs_preprocess(vnode_t *, u_offset_t, size_t *,
    fdbuffer_t *, int, cred_t *)
{
	return (0);
}

//
// Called when the FS is mounted using new mount options (via the
// MS_REMOUNT flag to mount).
//
void
fs_dependent_impl::set_mntopts(const char *)
{
	// Empty
}

//
// Function called by fobj_ii::cascaded_ioctl() to process fs-specific ioctls.
// Returns true if the ioctl was processed in this function, and false
// if not.
//
bool
fs_dependent_impl::process_cascaded_ioctl(sol::nodeid_t,
    fobj_ii *, int32_t, sol::intptr_t, int32_t, cred_t *, int *,
    int &, Environment &)
{
	return (false);
}

void
fs_dependent_impl::replay_ioctl(fobj_ii *, int32_t, sol::intptr_t, int32_t,
    int32_t &, int &)
{
}

//
// Function called by fs_ii::convert_to_primary() to do any fs-specific
// functions.  This default function simply returns with no errors.
//
int
fs_dependent_impl::convert_to_primary(fs_ii *)
{
	return (0);
}

void
fs_dependent_impl::freeze_primary(const char *)
{
}

// Helper function for dumping state to a new secondary.
void
fs_dependent_impl::dump_state(REPL_PXFS_VER::fs_replica_ptr, Environment &)
{
}

//
// Helper function called from repl_pxfs_server::ckpt_lockfs_state() to
// checkpoint state of the UFS _FIOLFS ioctl call.  The default implementation
// is empty, and should never get called.
//
void
fs_dependent_impl::ckpt_lockfs_state(uint64_t, uint64_t, uint64_t,
    const char *)
{
	ASSERT(0);
}

//
// Checkpoint tunefs parameters for VxFS.
//
void
fs_dependent_impl::ckpt_vx_tunefs(const REPL_PXFS_VER::vx_tunefs_t &)
{
	ASSERT(0);
}

//
// Static function that returns a fs-specific instance of
// 'fs_dependent_impl', if required.  This function is called from
// the 'fs_ii' constructor.
//
fs_dependent_impl *
fs_dependent_impl::get_fs_dependent_impl(fs_ii *fs_implp, const char *fstype,
    const char *mntoptions)
{
	if (strcmp(fstype, MNTTYPE_UFS) == 0) {
		return (new ufs_dependent_impl(fs_implp, mntoptions));
	} else if (strcmp(fstype, MNTTYPE_HSFS) == 0) {
		return (new hsfs_dependent_impl(fs_implp));
#ifndef VXFS_DISABLED
	} else if (strcmp(fstype, "vxfs") == 0) {
		return (new vxfs_dependent_impl(fs_implp));
#endif
	}

	return (&default_fs_dependent_impl);
}

void
fs_dependent_impl::free_fs_dependent_impl(fs_dependent_impl *fs_dep_implp)
{
	if (fs_dep_implp != &default_fs_dependent_impl)
		delete fs_dep_implp;
}

int
fs_dependent_impl::kernel_ioctl(vfs_t *vfsp, int operation,
    intptr_t arg)
{
	vnode_t *root_vp;
	int error = 0, ret_val = 0;

	ASSERT(vfsp != NULL);

	//
	// Get the root vnode.
	//
	// The read lock must be held across the call to VFS_ROOT()
	// to prevent a concurrent unmount from destroying the vfs.
	//
	vfs_lock_wait(vfsp);
	error = VFS_ROOT(vfsp, &root_vp);
	vfs_unlock(vfsp);
	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_RED,
		    ("fs_dependent_impl::kernel_ioctl: "
		    "VFS_ROOT() error %d\n", error)); // XXX
		return (error);
	}
	//
	// Set the copy_args 'pid' value to '0', to indicate that the
	// kernel is performing the ioctl (see orb/copy.cc).
	//
	copy_args args(orb_conf::node_number(), 0);
	copy::setcontext(&args);

	//
	// Convert the copyops functions to the mc_copyops
	// functions as defined in the orb
	//
	bool restore_ops = false;
#if SOL_VERSION >= __s10
	if (!copyops_installed(curthread)) {
#else
	if (get_copyops(curthread) == &default_copyops) {
#endif
		set_server_copyops();
		restore_ops = true;
	}

	//
	// Make the ioctl call - use kcred because this ioctl has
	// already suceeded on the old primary, and it must succeed
	// during a switchover/failover.
	//
#if	SOL_VERSION >= __s11
	error = VOP_IOCTL(root_vp, operation, arg,
	    FKIOCTL, kcred, &ret_val, NULL);
#else
	error = VOP_IOCTL(root_vp, operation, arg,
	    FKIOCTL, kcred, &ret_val);
#endif

	// Release the held vnode.
	VN_RELE(root_vp);

	//
	// Restore copyops to default_copyops
	//
	if (restore_ops) {
		remove_copyops(curthread);
	}

	// Trap future references.
	copy::setcontext(NULL);

	return (error);
}

int
fs_dependent_impl::fs_alloc_data(vnode_t *, u_offset_t, size_t *, fdbuffer_t *,
    int, cred_t *)
{
	return (EIO);
}

int
fs_dependent_impl::fs_rdwr_data(vnode_t *, u_offset_t, size_t,
    size_t, fdbuffer_t *, int, cred_t *)
{
	return (EIO);
}

//
// If the file system isn't a log based file system or directory operations
// aren't synchronous, override this default function.
//
int
fs_dependent_impl::fs_fsync(vnode_t *, cred_t *)
{
	return (0);
}

//
// If the file system isn't a log based file system or directory operations
// aren't synchronous, override this default function.
//
int
fs_dependent_impl::do_fsync(vnode_t *vnodep, cred_t *credp)
{
#if	SOL_VERSION >= __s11
	return (VOP_FSYNC(vnodep, FSYNC, credp, NULL));
#else
	return (VOP_FSYNC(vnodep, FSYNC, credp));
#endif
}

int
fs_dependent_impl::sync_if_necessary(os::hrtime_t &, vnode *, cred_t *)
{
	return (0);
}

//
// Return 'true' if the device this filesystem is mounted on is a
// lofi device; else return false.
//
bool
fs_dependent_impl::device_is_lofi(struct vfs *vfsp)
{
	dev_t dev;
	major_t maj;
	char *device_name;

	maj = getmajor(vfsp->vfs_dev);
	device_name = ddi_major_to_name(maj);

	if (strncmp(device_name, LOFI_DRIVER_NAME,
			sizeof (LOFI_DRIVER_NAME) + 1) == 0) {
		return (true);
	} else {
		return (false);
	}
}
