//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)client_set.cc	1.15	08/05/20 SMI"

#include <orb/invo/common.h>
#include <orb/member/members.h>
#include <orb/refs/unref_threadpool.h>

#include "../version.h"
#include <pxfs/server/client_set.h>
#include <pxfs/server/fobjplus_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//
// Constructor
// Use reference counting to ensure that the object remains in existence
// until task processing completes.
//
client_set_purge_task::client_set_purge_task(fobjplus_ii *fobjplus_iip) :
	fobjplusp(fobjplus_iip),
	fobjplus_p(fobjplus_iip->get_fplus_objref())
{
}

//
// execute - this does the actual work for the task to clean up dead clients.
// Any client entry with a nil client reference represents a dead client.
//
void
client_set_purge_task::execute()
{
	fobjplusp->do_purge_dead_clients();
	CORBA::release(fobjplus_p);
	delete this;
}

//
// destructor - must clean up the client list
//
client_set::~client_set()
{
	ASSERT(clients.is_empty());
	ASSERT(client_list.empty());
}

//
// empty - unconditionally discard all clients.
// At this point we can ignore privileges held by clients,
// because that information will no longer be used (at least not
// without re-initialization).
//
void
client_set::empty()
{
	client_entry	*clientp;

	list_lock.wrlock();

	clients.empty();

	while ((clientp = client_list.reapfirst()) != NULL) {

		CORBA::release(clientp->client_p);

		if (clientp == &first_client) {
			//
			// This is the pre-allocated client entry
			//
			first_client.node.ndid = NODEID_UNKNOWN;
			first_client.client_p = PXFS_VER::fobj_client::_nil();
		} else {
			delete clientp;
		}
	}
	list_lock.unlock();
}

//
// insert - add a client for the specified node.
//
// This ensures that only one client per node is a member of the set.
// Attempts to add multiple clients for the same node are rejected.
//
// Returns the client member of the set for the specified node,
// with the reference count incremented. The recipient must release
// the fobj_client object reference when done. The reference count
// is incremented while holding the lock to prevent the race condition
// where someone deregisters the client just after someone else gets
// the object reference and before incrementing the reference count.
//
// This method assumes that orphan clients have already been purged.
//
PXFS_VER::fobj_client_ptr
client_set::insert(ID_node &client_node, PXFS_VER::fobj_client_ptr client_p)
{
	if (CORBA::is_nil(client_p)) {
		//
		// Client does not support caching.
		// Do not enter in client_set
		//
		return (PXFS_VER::fobj_client::_nil());
	}
	list_lock.wrlock();

	PXFS_VER::fobj_client_ptr	clientmember_p;
	client_entry			*clientp;

	if (clients.contains(client_node.ndid)) {
		//
		// Already have a client for this node
		//
		client_list.atfirst();
		while ((clientp = client_list.get_current()) != NULL) {
			//
			// It is possible that the current list element
			// may be deleted. So advance now to be safe.
			//
			client_list.advance();

			if (clientp->is_zombie()) {
				//
				// Found a zombie
				//
				if (clientp->refcnt == 0) {
					//
					// Only cleanup unused zombies
					// A zombie is not in the nodeset.
					// But we must remove it from the list.
					//
					CORBA::release(clientp->client_p);
					(void) client_list.erase(clientp);

					if (clientp == &first_client) {
						//
						// Pre-allocated client entry
						//
						first_client.node.ndid =
						    NODEID_UNKNOWN;
						first_client.client_p =
						    PXFS_VER::fobj_client::
						    _nil();
					} else {
						delete clientp;
					}
				}
				continue;
			}

			if (ID_node::match(client_node, clientp->node)) {
				//
				// Found a live client for the node
				//
				ASSERT(members::the().still_alive(
				    clientp->node));
				ASSERT(!CORBA::is_nil(clientp->client_p));

				clientmember_p = clientp->client_p;

				//
				// Increment reference count for object
				// being passed to the caller
				//
				(void) PXFS_VER::fobj_client::
				    _duplicate(clientmember_p);

				list_lock.unlock();
				return (clientmember_p);
			}
		}
	}

	//
	// If first_client is unintialized then, we initialise it to client_p.
	// In such a case we need to  explicitly increment the reference count
	// of client_p.
	// However, if first_client is already initialized, we create a
	// new client_entry object. And the constructor of client_entry
	// takes care of incrementing the reference count of client_p.
	//
	if (first_client.node.ndid == NODEID_UNKNOWN) {
		(void) PXFS_VER::fobj_client::_duplicate(client_p);

		//
		// The pre-allocated client_entry is unused
		//
		first_client.node = client_node;
		first_client.client_p = client_p;
		first_client.refcnt = 0;

		client_list.append(&first_client);
	} else {
		//
		// Create a client entry
		//
		client_list.append(new client_entry(client_node, client_p));
	}

	clients.add_node(client_node.ndid);

	//
	// Increment reference count for object being passed to the caller
	//
	(void) PXFS_VER::fobj_client::_duplicate(client_p);

	list_lock.unlock();
	return (client_p);
}

//
// remove - the specified client is removed from the list of clients.
//
// The method removes all client privileges for specified client.
//
// The process of removing client privileges and removing the client
// from the list of clients must be atomic. Otherwise another thread
// could change the client privileges in between the two steps.
//
void
client_set::remove(fobjplus_ii *fobjplusp, ID_node &client_node)
{
	client_entry	*clientp;

	//
	// Must acquire locks in accordance with the lock hierarchy.
	// We do not know whether the clients have any privileges;
	// but better safe than deadlocked.
	//
	list_lock.wrlock();
	fobjplusp->acquire_token_locks();

	client_list.atfirst();
	while ((clientp = client_list.get_current()) != NULL) {
		//
		// It is possible that the current list element
		// may be deleted, if dead client is found. So
		// advance now to be safe.
		//
		client_list.advance();

		//
		// Do matching using incarnation number
		// in order to filter out orphan requests.
		//
		if (ID_node::match(client_node, clientp->node)) {
			//
			// Found a match
			// Kick the client off the list
			//
			fobjplusp->remove_client_privileges(clientp->node.ndid);
			clients.remove_node(clientp->node.ndid);

			if (clientp->refcnt != 0) {
				//
				// The client_entry is still in use.
				// Convert to zombie.
				//
				clientp->node.ndid += NODEID_MAX + NODEID_MAX;

			} else {
				//
				// Cleanup the client_entry now.
				//
				CORBA::release(clientp->client_p);

				(void) client_list.erase(clientp);

				if (clientp == &first_client) {
					//
					// Pre-allocated client entry
					//
					first_client.node.ndid = NODEID_UNKNOWN;
					first_client.client_p =
					    PXFS_VER::fobj_client::_nil();
				} else {
					delete clientp;
				}
			}
			break;
		}
	}
	list_lock.unlock();
	fobjplusp->release_token_locks();
}

//
// remove - the client entry corresponding to the specified
// fobj_client_ptr is removed from the list of clients.
//
// The method removes all client privileges for specified client.
//
// The process of removing client privileges and removing the client
// from the list of clients must be atomic. Otherwise another thread
// could change the client privileges in between the two steps.
//
void
client_set::remove(fobjplus_ii *fobjplusp,
	PXFS_VER::fobj_client_ptr fobj_client_p)
{
	client_entry	*clientp;

	//
	// Must acquire locks in accordance with the lock hierarchy.
	// We do not know whether the clients have any privileges;
	// but better safe than deadlocked.
	//
	list_lock.wrlock();
	fobjplusp->acquire_token_locks();

	client_list.atfirst();
	while ((clientp = client_list.get_current()) != NULL) {
		//
		// It is possible that the current list element
		// may be deleted, if dead client is found. So
		// advance now to be safe.
		//
		client_list.advance();

		//
		// Purge any dead clients that have not yet been purged
		//
		if (CORBA::is_nil(clientp->client_p)) {
			(void) client_list.erase(clientp);

			fobjplusp->remove_client_privileges(clientp->node.ndid);
			clients.remove_node(clientp->node.ndid);

			if (clientp == &first_client) {
				//
				// Pre-allocated client entry
				//
				first_client.node.ndid = NODEID_UNKNOWN;
				first_client.client_p =
				PXFS_VER::fobj_client::_nil();
			} else {
				delete clientp;
			}
			continue;
		}

		//
		// Do matching using a direct comparison of the passed-in
		// fobj_client pointer with the stored pointer.
		//
		if (clientp->client_p->_equiv(fobj_client_p)) {
			//
			// Found a match
			// Kick the client off the list
			//
			fobjplusp->remove_client_privileges(clientp->node.ndid);
			clients.remove_node(clientp->node.ndid);

			if (clientp->refcnt != 0) {
				//
				// The client_entry is still in use.
				// Convert to zombie.
				//
				clientp->node.ndid += NODEID_MAX + NODEID_MAX;

			} else {
				//
				// Cleanup the client_entry now.
				//
				CORBA::release(clientp->client_p);

				(void) client_list.erase(clientp);

				if (clientp == &first_client) {
					//
					// Pre-allocated client entry
					//
					first_client.node.ndid = NODEID_UNKNOWN;
					first_client.client_p =
					    PXFS_VER::fobj_client::_nil();
				} else {
					delete clientp;
				}
			}
			break;
		}
	}
	list_lock.unlock();
	fobjplusp->release_token_locks();
}

//
// purge_dead_clients - queue the cleanup of dead clients to a threadpool
// in order to avoid lock order violation deadlocks.
//
void
client_set::purge_dead_clients(fobjplus_ii *fobjplusp)
{
	list_lock.wrlock();
	if (!purge_in_progress) {
		purge_in_progress = true;
		//
		// The unref_threadpool can handle very large numbers
		// of tasks. A dead client results from a dead node.
		// So there will most likely be large numbers of these tasks
		// after a node failure.
		//
		unref_threadpool::the().defer_processing(
		    new client_set_purge_task(fobjplusp));
	}
	list_lock.unlock();
}

//
// do_purge_dead_clients - does the actual work of purging dead clients.
// A dead client may have privileges that must be rescinded.
//
void
client_set::do_purge_dead_clients(fobjplus_ii *fobjplusp)
{
	client_entry	*clientp;

	//
	// Must acquire locks in accordance with the lock hierarchy.
	// We do not know whether the clients have any privileges;
	// but better safe than deadlocked.
	//
	list_lock.wrlock();
	fobjplusp->acquire_token_locks();

	client_list.atfirst();
	while ((clientp = client_list.get_current()) != NULL) {
		//
		// It is possible that the current list element
		// may be deleted, if dead client is found. So
		// advance now to be safe.
		//
		client_list.advance();

		if (clientp->is_zombie()) {
			//
			// Found a zombie
			//
			if (clientp->refcnt == 0) {
				//
				// Only cleanup unused zombies
				// A zombie is not in the nodeset.
				// But we do have to remove it from the list.
				//
				CORBA::release(clientp->client_p);
				(void) client_list.erase(clientp);

				if (clientp == &first_client) {
					//
					// Pre-allocated client entry
					//
					first_client.node.ndid = NODEID_UNKNOWN;
					first_client.client_p =
					    PXFS_VER::fobj_client::_nil();
				} else {
					delete clientp;
				}
			}
		} else if (CORBA::is_nil(clientp->client_p)) {
			//
			// Found a match
			// Kick the dead client off the list
			//
			fobjplusp->remove_client_privileges(clientp->node.ndid);
			clients.remove_node(clientp->node.ndid);

			(void) client_list.erase(clientp);

			if (clientp == &first_client) {
				//
				// This is the pre-allocated client entry
				//
				first_client.node.ndid = NODEID_UNKNOWN;
			} else {
				delete clientp;
			}
		}
	}
	purge_in_progress = false;
	list_lock.unlock();
	fobjplusp->release_token_locks();
}

//
// find_and_purge_dead_clients - finds any client that is not
// a member of the cluster and purges that client.
// A dead client may have privileges that must be rescinded.
//
void
client_set::find_and_purge_dead_clients(fobjplus_ii *fobjplusp)
{
	client_entry	*clientp;

	//
	// Must acquire locks in accordance with the lock hierarchy.
	// We do not know whether the clients have any privileges;
	// but better safe than deadlocked.
	//
	list_lock.wrlock();
	fobjplusp->acquire_token_locks();

	client_list.atfirst();
	while ((clientp = client_list.get_current()) != NULL) {
		//
		// It is possible that the current list element
		// may be deleted. So advance now to be safe.
		//
		client_list.advance();

		if (clientp->is_zombie()) {
			//
			// Found a zombie
			//
			if (clientp->refcnt == 0) {
				//
				// Only cleanup unused zombies
				// A zombie is not in the nodeset.
				// But we do have to remove it from the list.
				//
				CORBA::release(clientp->client_p);
				(void) client_list.erase(clientp);

				if (clientp == &first_client) {
					//
					// Pre-allocated client entry
					//
					first_client.node.ndid = NODEID_UNKNOWN;
					first_client.client_p =
					    PXFS_VER::fobj_client::_nil();
				} else {
					delete clientp;
				}
			}
		} else if (!members::the().still_alive(clientp->node)) {
			//
			// Kick the dead client off the list
			//
			fobjplusp->remove_client_privileges(clientp->node.ndid);
			clients.remove_node(clientp->node.ndid);

			(void) client_list.erase(clientp);

			if (clientp == &first_client) {
				//
				// This is the pre-allocated client entry
				//
				first_client.node.ndid = NODEID_UNKNOWN;
			} else {
				delete clientp;
			}
		}
	}
	list_lock.unlock();
	fobjplusp->release_token_locks();
}

//
// get_client - Return the client object reference for the specified node
//
// The list lock must be held during this call and until the caller
// stops using the client object reference.
//
PXFS_VER::fobj_client_ptr
client_set::get_client(ID_node &client_node)
{
	ASSERT(list_lock.lock_held());
	client_set::client_entry	*entryp;

	client_set::iterator		iter(client_list);

	while (NULL != (entryp = iter.get_current())) {
		if (ID_node::match(client_node, entryp->node)) {
			return (entryp->client_p);
		}
		iter.advance();
	}
	// No client was found for specified node
	return (PXFS_VER::fobj_client::_nil());
}
