//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)unixdir_impl.cc	1.39	08/05/20 SMI"

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/pathname.h>
#include <sys/debug.h>
#include <sys/uio.h>
#include <sys/file.h>
#include <sys/errno.h>
#include <sys/sysmacros.h>
#include <sys/fs/snode.h>
#include <sys/stat.h>
#include <sys/dirent.h>

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#define	_LEAST_PRIVS
#endif

#if defined(_LEAST_PRIVS)
#include <sys/policy.h>
#endif

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/unixdir_impl.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fs_base_impl.h>
#include <pxfs/server/symlink_impl.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/unixdir_ckpt.h>
#include <pxfs/client/pxfobj.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

bool cfs_enable_local_opt = true;

//
// Unreplicated or primary constructor for the internal implementation.
//
unixdir_ii::unixdir_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
    fid_t *fidp) :
	fobjplus_ii(fsp, vp, ftype, fidp)
{
}

//
// Secondary constructor for the internal implementation.
//
unixdir_ii::unixdir_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t ftype) :
	fobjplus_ii(fsp, fobjid, ftype)
{
}

unixdir_ii::~unixdir_ii()
{
}

//
// prepare_recover - initialize data structures prior to recovering
// information from clients.
//
void
unixdir_ii::prepare_recover()
{
	cached_dir_info.empty();

	fobjplus_ii::prepare_recover();
}

//
// recovered_state - we find out the data privileges of the client.
//
void
unixdir_ii::recovered_state(PXFS_VER::recovery_info &recovery)
{
	fobjplus_ii::recovered_state(recovery);

	if (recovery.data_rights != 0) {
		//
		// The client has cached directory information.
		//
		cached_dir_info.add_node(recovery.ndid);
	}
}

//
// Constructor for the unreplicated CORBA implementation.
//
unixdir_norm_impl::unixdir_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp) :
	unixdir_ii(fsp, vp, PXFS_VER::fobj_unixdir, fidp)
{
	set_fobj_ii(this);
}

//
// Constructor for the primary replicated CORBA implementation.
//
unixdir_repl_impl::unixdir_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp) :
	unixdir_ii(fsp, vp, PXFS_VER::fobj_unixdir, fidp),
	mc_replica_of<PXFS_VER::unixdir>(fsp->get_serverp())
{
	set_fobj_ii(this);
}

//
// Constructor for the secondary replicated CORBA implementation.
//
unixdir_repl_impl::unixdir_repl_impl(fs_ii *fsp,
    const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::unixdir_ptr obj) :
	unixdir_ii(fsp, fobjid, PXFS_VER::fobj_unixdir),
	mc_replica_of<PXFS_VER::unixdir>(obj)
{
	set_fobj_ii(this);
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we lock the
// appropriate bucket lock while checking for this case.
// This kind of file object is always the primary.
// If we win the race, then
// do what is needed to delete the object.
//
void
unixdir_norm_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	if (!fobj_released) {
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	}
	if (_last_unref(arg)) {
		unreferenced(bktno, false);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we acquire the appropriate
// locks while checking for this case. If we win the race, then
// do what is needed to delete the object.
//
void
unixdir_repl_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	bool	fidlist_locked = false;

	if (fobj_released) {
		// No locking needed because not on any list

	} else if (get_fsp()->is_secondary()) {
		//
		// A secondary keeps objects on the fid list.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();
	} else if (primary_ready) {
		//
		// The file object is a ready primary.
		//
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	} else {
		//
		// The file object has not become a primary yet.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();

		int		error;
		vnode_t		*vnodep = fid_to_vnode(error);
		if (vnodep != NULL) {
			//
			// Grab the bucket lock.
			// It is possible someone may have just made this file
			// object a ready primary. But this will work in
			// in either case.
			//
			bktno = fs_implp->vp_to_bucket(vnodep);
			fs_implp->fobjlist_locks[bktno].wrlock();

			// Drop the hold from fid_to_vnode
			VN_RELE(vnodep);
		}
	}

	if (_last_unref(arg)) {
		if (is_active()) {
			//
			// Sync. out any non-durable state in the directory,
			// so that the next time it is looked up, we will be
			// returning durable data.  See CLARC/2000/018 for
			// details.
			//
			(void) fs_implp->get_fs_dep_implp()->sync_if_necessary(
			    mod_time, get_vp(), kcred);
		}
		//
		// Note that unreference processing will drop
		// the appropriate locks.
		//
		unreferenced(bktno, fidlist_locked);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
	if (fidlist_locked) {
		fs_implp->fidlock.unlock();
	}
}

//
// Look up a name in this directory.
//
void
unixdir_ii::lookup(const char *nm, PXFS_VER::fobj_out fobj,
    PXFS_VER::fobj_info &fobjinfo, PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, Environment &env)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	vnode_t		*targetvp = NULL;
	int		error;
	PXFS_VER::fobj_ptr	fobjp2;

	bool		need_transaction = !(cfs_enable_local_opt &&
			    env.is_local_client());

	//
	// Debugging test to make sure we don't hit a null vnodep.
	// This typically happens if we are performing an operation on
	// the secondary instead of the primary.
	//
	ASSERT(get_vp() != NULL);

	//
	// Note that we don't lock the directory attributes since
	// VOP_LOOKUP() does not change them. We do lock directory
	// so that we return consistent results if caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary.
	//
	dir_lock.rdlock();

	//
	// Perform a lookup in the underlying file system and if a vnode
	// is found, find (or create) the corresponding fobj object.
	//
	if (get_delete_num() != 0) {
		//
		// The directory has been removed so don't do lookup.
		//
		error = ENOENT;
	} else {
#if	SOL_VERSION >= __s11
		error = VOP_LOOKUP(get_vp(), (char *)nm, &targetvp,
		    NULL, 0, NULL, credp, NULL, NULL, NULL);
#else
		error = VOP_LOOKUP(get_vp(), (char *)nm, &targetvp,
		    NULL, 0, NULL, credp);
#endif
		//
		// If this is a remote request for a lookup, make sure the
		// directory contents are on disk.  See CLARC/2000/018 for more.
		//
		if ((error == 0) && need_transaction) {
			error = fs_implp->get_fs_dep_implp()->
			    sync_if_necessary(mod_time, get_vp(), credp);
			if (error != 0) {
				VN_RELE(targetvp);
			}
		}
	}

	dir_lock.unlock();

	if (error == 0 || error == ENOENT) {
		nodeid_t	ndid = env.get_src_node().ndid;

		//
		// Show directory can have cached directory info.
		// This is safe even if we have already done this.
		//
		dir_token_lock.lock();
		cached_dir_info.add_node(ndid);
		dir_token_lock.unlock();
	}

	client2_p = PXFS_VER::fobj_client::_nil();

	if (error == 0) {
		//
		// For special files, we return objects for realvp's only.
		// The actual object representing the device or fifo
		// will be constructed during the time of open.
		// If VOP_REALVP() returns a non zero value, it means that
		// the underlying file system does not have a special
		// notion of a realvp and therefore the targetvp is itself
		// the correct vp.
		//
		if (IS_SPECIAL(targetvp)) {
			vnode_t		*realvp;
#if	SOL_VERSION >= __s11
			if (VOP_REALVP(targetvp, &realvp, NULL) == 0) {
#else
			if (VOP_REALVP(targetvp, &realvp) == 0) {
#endif
				VN_HOLD(realvp);
				VN_RELE(targetvp);
				targetvp = realvp;
			}
		}
		//
		// Don't allow the deleted directory to be seen or
		// we will need to handle locking with pxfs clients.
		// Note that creating a file with the deleted directory name
		// will fail with EXISTS but users won't be able to see it
		// with 'ls'.
		//
		if (VN_CMP(targetvp, get_fsp()->get_deleted())) {
			VN_RELE(targetvp);
			fobjp2 = PXFS_VER::fobj::_nil();
			fobjinfo.ftype = PXFS_VER::fobj_fobj;
			pxfslib::throw_exception(env, ENOENT);
		} else {
			fobjp2 = get_fsp()->find_fobj(targetvp, fobjinfo, env);
			VN_RELE(targetvp);

			//
			// Fault point for testing concurrent lookup collision
			//
			// @@@ FAULT_POINT_LOOKUP_COLLISION

			if (CORBA::is_nil(fobjp2)) {
				pxfslib::throw_exception(env, ENOTSUP);
			} else {
				fobj_ii		*fobj_iip =
				    fobj_ii::get_fobj_ii(fobjp2);
				//
				// The lookup can occur after a switchover,
				// and a FID conversion may be needed.
				//
				if (fobj_iip->is_replicated()) {
					(void) fobj_iip->is_dead(env);
					if (env.exception()) {
						PXFS_DBPRINTF(
						    PXFS_TRACE_UNIXDIR,
						    PXFS_RED,
						    ("lookup(%p) is_dead %p "
						    "error\n",
						    this, fobj_iip));
						env.clear();
						CORBA::release(fobjp2);
						fobjp2 = PXFS_VER::fobj::_nil();
						pxfslib::throw_exception(env,
						    EIO);
						return;
					}
				}
				//
				// fobj_client object reference returned
				// by get_bind_info() already has the
				// reference count incremented.
				//
				client2_p = fobj_iip->
				    get_bind_info(binfo, client1_p, kcred, env);
			}
		}
	} else {
		fobjp2 = PXFS_VER::fobj::_nil();
		fobjinfo.ftype = PXFS_VER::fobj_fobj;
		pxfslib::throw_exception(env, error);
	}

	fobj = fobjp2;
}

//
// create_fobj - creates a file object
//
// The overall task here is to:
//	If committed, return checkpointed results.
//	Acquire the directory attribute lock.
//	Lookup target name, acquire target attribute lock if present.
//	Downgrade (flush) both pages and attributes of the target if needed
//	depending on the exclflag and AT_SIZE bit of va_mask.
//	Downgrade (flush) cached directory attributes.
//	Acquire the directory lock.
//	Do the underlying vnode operation (VOP_XXX).
//	Downgrade (flush) cached directory lookups.
//	Release the directory lock.
//	Release target attribute lock if held.
//	Release the directory attribute lock.
//
void
unixdir_ii::create_fobj(const char *nm,
    const sol::vattr_t		&attr,
    sol::vcexcl_t		exclflag,
    int32_t			mode,
    PXFS_VER::fobj_out		fobj,
    PXFS_VER::fobj_info		&fobjinfo,
    PXFS_VER::bind_info 	&binfo,
    PXFS_VER::fobj_client_ptr	client1_p,
    PXFS_VER::fobj_client_out	client2_p,
    solobj::cred_ptr		credobj,
    int32_t			flag,
    Environment			&env)
{
	cred_t			*credp = solobj_impl::conv(credobj);
	vattr_t			*vap = (vattr_t *)&conv(attr);
	vnode_t			*targetvp;
	PXFS_VER::fobj_ptr	targetfobj = PXFS_VER::fobj::_nil();
	fobj_ii			*fobj_iip = NULL;
	int			error;
	bool			file_exists = false;
	os::hrtime_t		defer_time = 0;

	client2_p = PXFS_VER::fobj_client::_nil();

	//
	// Check to see if we need this call to be transactional.  We use this
	// information to determine whether to checkpoint enough information
	// to handle a retry, and whether the results of this call need to
	// be durable.  See CLARC/2000/018 for more.
	//
	bool	need_transaction = !(cfs_enable_local_opt &&
	    env.is_local_client());

	// XXX Return error until these vnode types are supported.
	if ((vap->va_mask & AT_TYPE) && vap->va_type == VDOOR) {
		pxfslib::throw_exception(env, ENOTSUP);
		return;
	}

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);
	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		fobj = statep->get_ret_fobj();
		statep->get_fobjinfo(fobjinfo);
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		} else {
			ASSERT(!CORBA::is_nil(fobj));
			fobj_ii		*new_iip = fobj_ii::get_fobj_ii(fobj);
			//
			// May need FID conversion
			//
			if (is_replicated()) {
				(void) new_iip->is_dead(env);
				if (env.exception()) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_UNIXDIR,
					    PXFS_RED,
					    ("create_fobj(%p) is_dead %p "
					    "error #0\n",
					    this, new_iip));
					env.clear();
					CORBA::release(fobj);
					fobj = PXFS_VER::fobj::_nil();
					pxfslib::throw_exception(env, EIO);
					return;
				}
			}
			//
			// fobj_client object reference returned by
			// get_bind_info() already has the reference
			// count incremented.
			//
			client2_p = new_iip->
			    get_bind_info(binfo, client1_p, kcred, env);
		}
		return;
	}
	if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

		targetfobj = statep->get_target_fobj();
		if (!CORBA::is_nil(targetfobj)) {
			//
			// The file existed on the failed primary.
			// So reacquire the lock on the target file
			// and continue.
			//
			file_exists = true;
			statep->get_fobjinfo(fobjinfo);
			fobj_iip = fobj_ii::get_fobj_ii(targetfobj);
			//
			// Must force a FID conversion since
			// we are executing on the primary
			//
			if (is_replicated()) {
				(void) fobj_iip->is_dead(env);
				if (env.exception()) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_UNIXDIR,
					    PXFS_RED,
					    ("create_fobj(%p) is_dead %p "
					    "error #1\n",
					    this, fobj_iip));
					env.clear();
					get_ckpt()->ckpt_error_return(EIO, env);
					pxfslib::throw_exception(env, EIO);
					return;
				}
			}
			attr_lock.wrlock();
			fobj_iip->range_lock();
			fobj_iip->attr_lock.wrlock();
		} else {
			attr_lock.wrlock();
		}
		goto checkpointed;
	}
	ASSERT(statep == NULL);

	//
	// This execution path is taken by the original primary, or by
	// a new primary that didn't receive a checkpoint message.
	//

	//
	// We hold the fobj attribute lock of the directory
	// so the directory contents don't change between the
	// VOP_LOOKUP() and the VOP_CREATE() below.
	// Note that we may be acquiring the attribute lock on both the
	// directory and a file within the directory. This lock order
	// must be used to prevent deadlocks.
	//
	attr_lock.wrlock();

	//
	// VOP_CREATE will truncate an existing regular file if
	// we are not trying to do an exclusive create.
	// We need to make sure the PXFS layer doesn't try to change
	// the file size while the underlying file system code
	// changes the file size. Note that we can't use client
	// side PXFS operations to change the file size since it is the
	// VOP_CREATE() call that changes the file size. We need
	// to do the same kind of locking and attribute cache
	// flushing around the VOP_CREATE() call that
	// fobj_ii::set_attributes() does around VOP_SETATTR().
	//
	if ((vap->va_mask & AT_SIZE) && (vcexcl) exclflag == NONEXCL) {
		// Check to see if the file we are creating already exists.
#if	SOL_VERSION >= __s11
		error = VOP_LOOKUP(get_vp(), (char *)nm, &targetvp, NULL, 0,
		    NULL, credp, NULL, NULL, NULL);
#else
		error = VOP_LOOKUP(get_vp(), (char *)nm, &targetvp, NULL, 0,
		    NULL, credp);
#endif
		if (error == 0) {
			file_exists = true;
		}
		if (error == 0 && targetvp->v_type == VREG) {
			//
			// Find or create the corresponding pxfs object
			// so we can lock the file properly.
			//
			targetfobj = get_fsp()->find_fobj(targetvp, fobjinfo,
			    env);
			VN_RELE(targetvp);

			//
			// XXX eventually pxfs needs to support all vnode types
			// pxfs supports regular data files, so we
			// should always succeed here.
			//
			if (CORBA::is_nil(targetfobj)) {
				error = ENOTSUP;
				goto create_return;
			}

			fobj_iip = fobj_ii::get_fobj_ii(targetfobj);
			ASSERT(fobj_iip != NULL);

			//
			// Must force a FID conversion since
			// we are executing on the primary
			//
			if (is_replicated()) {
				(void) fobj_iip->is_dead(env);
				if (env.exception()) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_UNIXDIR,
					    PXFS_RED,
					    ("create_fobj(%p) is_dead %p "
					    "error #2\n",
					    this, fobj_iip));
					env.clear();
					if (need_transaction) {
						get_ckpt()->ckpt_error_return(
						    EIO, env);
					}
					pxfslib::throw_exception(env, EIO);
					attr_lock.unlock();
					return;
				}
			}

			//
			// fobj_client object reference returned by
			// get_bind_info_no_attr() already has the reference
			// count incremented.
			//
			// Do not get attributes at this time,
			// because we are about to invalidate the attributes
			// and the client is not yet able to accept downgrade
			// commands.
			//
			client2_p = ((fobjplus_ii *)fobj_iip)->
			    get_bind_info_no_attr(binfo, client1_p, kcred, env);

			//
			// VOP_CREATE() may reduce the size of the file
			// (for O_TRUNC case.) The change in size requires
			// invalidation of pages and associated attribute
			// and cache write tokens. Here we acquire the locks
			// needed to ensure that the file will not be accessed/
			// read/written during the course of create/truncate.
			// The attribute token and the file pages are flushed
			// here by calling file_ii::downgrade_all() and
			// downgrade_attr_all(). However, the file pages are
			// invalidated later, if VOP_CREATE() succeeds.
			// Otherwise, a pending async_page_out() request could
			// come just after VOP_CREATE() but before truncate()
			// and end up confused with the size set to 0 by
			// VOP_CREATE().
			//
			fobj_iip->range_lock();

			//
			// A newly created file client can not accept
			// downgrade commands, but at the same time it
			// does not have any privileges. Thus the system
			// will skip it.
			//
			// If there already was a file client on the
			// requesting node, the system
			// will safely downgrade its caches.
			//
			((file_ii *)fobj_iip)->downgrade_all(
			    (sol::u_offset_t)0, file_ii::FLUSH_BACK, 0, env);

			fobj_iip->attr_lock.wrlock();

			error = pxfslib::get_err(env);
			if (error) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_UNIXDIR,
				    PXFS_RED,
				    ("create_fobj(%p) downgrade_all failed."
				    " file %s, fobj_iip %p, error %d\n",
				    this, nm, fobj_iip, error));
				goto create_return;
			}
			error = fobj_iip->downgrade_attr_all(
			    PXFS_VER::attr_write, false, 0);
			if (error) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_UNIXDIR,
				    PXFS_RED,
				    ("create_fobj(%p) downgrade_attr_all "
				    " failed. file %s, fobj_iip %p, "
				    "error %d\n", this, nm, fobj_iip, error));
				goto create_return;
			}

			if (need_transaction && is_replicated()) {
				//
				// Checkpoint the existence of the file
				// which will need to be locked if failover.
				//
				get_ckpt()->ckpt_target(targetfobj, fobjinfo,
				    env);

				//
				// The only possible exception is a software
				// version mismatch exception.
				//
				ASSERT(!env.exception());
			}
		} else if (error == 0) {
			VN_RELE(targetvp);
			if (need_transaction && is_replicated()) {
				//
				// Checkpoint the existence of the target
				// name (but not a file).
				//
				get_ckpt()->ckpt_entry_state(true, env);

				//
				// The only possible exception is a software
				// version mismatch exception.
				//
				ASSERT(!env.exception());
			}
		} else if (error == ENOENT) {
			if (need_transaction && is_replicated()) {
				//
				// Checkpoint that the target name doesn't
				// exist.
				//
				get_ckpt()->ckpt_entry_state(false, env);

				//
				// The only possible exception is a software
				// version mismatch exception.
				//
				ASSERT(!env.exception());
			}
		} else {
			//
			// Some other lookup error occured.
			// In this case, we don't send any checkpoints
			// since we aren't changing the file system.
			//
			attr_lock.unlock();
			pxfslib::throw_exception(env, error);
			return;
		}
	} else if (need_transaction) {
		//
		// Lookup the target name and checkpoint
		// whether it exists or not.
		//
		unixdir_state::do_ckpt_entry(this, nm, credp, env);

		//
		// The only possible exception is a software
		// version mismatch exception.
		//
		ASSERT(!env.exception());
	}

checkpointed:

	//
	// At this point, we are either the original primary or a new
	// primary.
	//
	// Invariant: either targetfobj==nil, fobj_iip==NULL, no fobj lock,
	// or targetfobj==fobj to return, fobj_iip != NULL, fobj lock held.
	//
	ASSERT((fobj_iip != NULL && !CORBA::is_nil(targetfobj) &&
	    fobj_iip->attr_lock.write_held()) ||
	    (fobj_iip == NULL && CORBA::is_nil(targetfobj)));

	//
	// Invalidate attribute caches for this directory so we
	// see the new modify time. We continue to hold the attribute
	// lock so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_CREAT_S_B, FaultFunctions::generic);

	// Create the vnode in the underlying file system.
	targetvp = NULL;
	if (get_delete_num() != 0) {
		//
		// The directory has been removed so don't create file.
		//
		error = ENOENT;
	} else {
#if	SOL_VERSION >= __s11
		error = VOP_CREATE(get_vp(), (char *)nm, vap, conv(exclflag),
		    mode, &targetvp, credp, (int)flag, NULL, NULL);
#else
		error = VOP_CREATE(get_vp(), (char *)nm, vap, conv(exclflag),
		    mode, &targetvp, credp, (int)flag);
#endif
		if (error == 0) {
			if (need_transaction) {
				//
				// Must sync log before operation completes
				//
				mod_time = os::gethrtime();
			} else {
				defer_time = os::gethrtime();
			}
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_CREAT_S_A, FaultFunctions::generic);

	if (statep != NULL && statep->server_did_op(error, true)) {
		//
		// The original primary did the operation successfully.
		// We need to get the vnode of the new file.
		//
#if	SOL_VERSION >= __s11
		error = VOP_LOOKUP(get_vp(), (char *)nm, &targetvp, NULL, 0,
		    NULL, credp, NULL, NULL, NULL);
#else
		error = VOP_LOOKUP(get_vp(), (char *)nm, &targetvp, NULL, 0,
		    NULL, credp);
#endif
		//
		// The target better be there for the lookup, or else
		// something is seriously messed up.
		//
		ASSERT(error == 0);
		ASSERT(fobj_iip == NULL);
	}

	//
	// Invalidate any cached DNLC entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	// When the file already existed before the VOP_CREATE,
	// the DNLC entries do not have to be invalidated.
	//
	if (error == 0 && !file_exists) {
		nodeid_t	ndid = env.get_src_node().ndid;
		inval_name(ndid, nm);

		//
		// Show that the directory can have cached directory info.
		// This is safe even if we have already done this.
		//
		dir_token_lock.lock();
		cached_dir_info.add_node(ndid);
		dir_token_lock.unlock();
	}
	dir_lock.unlock();

create_return:

	//
	// At this point we need to find or create the associated fobj object
	// and return the error status if any.
	//
	// Invariant: either
	// a) fobj_iip != NULL, targetfobj != nil, targetfobj locked, or
	// b) fobj_iip == NULL, targetfobj == nil, no lock on targetfobj
	// In case b, either there is either an error, or targetvp != NULL.
	//
	ASSERT((fobj_iip != NULL && !CORBA::is_nil(targetfobj) &&
	    fobj_iip->attr_lock.write_held()) ||
	    (fobj_iip == NULL && CORBA::is_nil(targetfobj)));

	if (error == 0) {
		if (fobj_iip != NULL) {
			ASSERT(targetvp == fobj_iip->get_vp());
			VN_RELE(targetvp);
			fobj = targetfobj; // transfer reference
			fobj_iip->attr_lock.unlock();
			if (fobjtype == PXFS_VER::fobj_file) {
				//
				// Only a regular data file can have pages.
				// If the file did not previously exist,
				// no clients will have a data token
				// and will not be contacted.
				//
				error = ((file_ii *)fobj_iip)->
				    truncate(vap->va_size, env);
				if (error) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_UNIXDIR,
					    PXFS_RED,
					    ("create_fobj(%p) truncate file %s"
					    " failed fobj_iip %p error %d\n",
					    this, nm, fobj_iip, error));
				    if (need_transaction && is_replicated()) {
					get_ckpt()->
					    ckpt_error_return(error, env);
					env.clear();
				    }
				    pxfslib::throw_exception(env, error);
				    fobj_iip->range_unlock();
				    attr_lock.unlock();
				    return;
				}
				//
				// The client almost always will want the
				// file attributes. So let's get the
				// attributes now after we have finished
				// modifying attributes.
				//
				fobj_iip->attr_lock.wrlock();
				((fobjplus_ii *)fobj_iip)->get_attr_common(
				    credp,
				    PXFS_VER::attr_write,
				    binfo._u.bind_fobj.attr,
				    binfo._u.bind_fobj.attr_seqnum, true, env);
				fobj_iip->attr_lock.unlock();
				if (env.exception()) {
					//
					// Unable to get attributes
					//
					env.clear();
					//
					// Don't return valid attributes.
					//
					binfo._u.bind_fobj.rights =
					    PXFS_VER::attr_none;
				} else {
					binfo._u.bind_fobj.rights =
					    PXFS_VER::attr_write;
				}
			}
			fobj_iip->range_unlock();

			os::hrtime_t	time_m;
			if (need_transaction && is_replicated()) {
				//
				// Use the latest modification
				// time for deciding when to
				// flush the log.
				//
				time_m = mod_time;
				if (fobj_iip->can_cache() &&
				    ((fobjplus_ii *)fobj_iip)->mod_time >
				    time_m) {
					time_m = ((fobjplus_ii *)
					    fobj_iip)->mod_time;
				}
			}
			//
			// After getting attributes cannot acquire the attr_lock
			// again without risk of deadlock.
			//
			if (mod_time < defer_time) {
				mod_time = defer_time;
			}
			attr_lock.unlock();
			if (need_transaction && is_replicated()) {
				//
				// flush the log
				//
				error = fs_implp->get_fs_dep_implp()->
				    sync_if_necessary(time_m,
				    fobj_iip->get_vp(), credp);

				if (error != 0) {
					get_ckpt()->ckpt_error_return(
					    error, env);
					pxfslib::throw_exception(env, error);
					return;
				}

				// Optimized commit checkpoint.
				commit(env);
			}
		} else {
			ASSERT(targetvp != NULL);
			//
			// For special files, we return objects for realvp's
			// only. The actual object representing the device
			// or fifo will be constructed during the time of
			// open.
			//
			if (IS_SPECIAL(targetvp)) {
				vnode_t *realvp;
#if	SOL_VERSION >= __s11
				if (VOP_REALVP(targetvp, &realvp, NULL) == 0) {
#else
				if (VOP_REALVP(targetvp, &realvp) == 0) {
#endif
					VN_HOLD(realvp);
					VN_RELE(targetvp);
					targetvp = realvp;
				}
			}
			fobj = get_fsp()->find_fobj(targetvp, fobjinfo, env);

			attr_lock.unlock();

			VN_RELE(targetvp);

			// XXX eventually pxfs needs to support all vnode types
			if (CORBA::is_nil(fobj)) {
				if (need_transaction && is_replicated()) {
					//
					// Checkpoint (commit) return object.
					//
					get_ckpt()->ckpt_error_return(ENOTSUP,
					    env);
					env.clear();
				}
				pxfslib::throw_exception(env, ENOTSUP);

			} else {
				fobj_ii		*fobj_targp =
				    fobj_ii::get_fobj_ii(fobj);
				if (is_replicated()) {
					//
					// May need FID conversion
					//
					(void) fobj_targp->is_dead(env);
					if (env.exception()) {
						PXFS_DBPRINTF(
						    PXFS_TRACE_UNIXDIR,
						    PXFS_RED,
						    ("create_fobj(%p) is_dead "
						    "%p error #3\n",
						    this, fobj_targp));
						env.clear();
						CORBA::release(fobj);
						fobj = PXFS_VER::fobj::_nil();
						if (need_transaction) {
							get_ckpt()->
							    ckpt_error_return(
							    EIO, env);
						}
						pxfslib::throw_exception(env,
						    EIO);
						return;
					}

					if (need_transaction) {
						//
						// Use the latest modification
						// time for deciding when to
						// flush the log.
						//
						os::hrtime_t time_m = mod_time;
						if (fobj_targp->can_cache() &&
						    ((fobjplus_ii *)fobj_targp)
						    ->mod_time > time_m) {
							time_m =
							    ((fobjplus_ii *)
							    fobj_targp)->
							    mod_time;
						}
						error = fs_implp->
						    get_fs_dep_implp()->
						    sync_if_necessary(
						    time_m, get_vp(), credp);
						if (error != 0) {
							get_ckpt()->
							    ckpt_error_return(
							    error, env);
							pxfslib::
							    throw_exception(env,
							    error);
							return;
						}

						//
						// Checkpoint (commit)
						// return object.
						//
						get_ckpt()->ckpt_fobj_return(
						    fobj, fobjinfo, env);
					}
				}
				//
				// Must do all work requiring the attr_lock
				// before getting attributes. Otherwise
				// deadlock can result.
				//
				attr_lock.wrlock();
				if (mod_time < defer_time) {
					mod_time = defer_time;
				}
				attr_lock.unlock();

				//
				// fobj_client object reference returned by
				// get_bind_info() already has the reference
				// count incremented.
				//
				client2_p = fobj_targp->
				    get_bind_info(binfo, client1_p, kcred, env);

			}
		}
	} else {
		if (fobj_iip != NULL) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UNIXDIR,
			    PXFS_AMBER,
			    ("create_fobj(%p) cleanup %s fobj_iip %p err %d\n",
			    this, nm, fobj_iip, error));
			fobj_iip->attr_lock.unlock();
			fobj_iip->range_unlock();
			if (client1_p->_equiv(client2_p)) {
				//
				// An error means the operation failed.
				// The above test means that a new
				// client was registered. This new client
				// registration must be undone.
				//
				fobj_iip->undo_register_client(env);
			}
			CORBA::release(targetfobj);
		}
		attr_lock.unlock();
		if (need_transaction && is_replicated()) {
			//
			// Checkpoint (commit) the return error.
			//
			get_ckpt()->ckpt_error_return(error, env);
			env.clear();
		}
		pxfslib::throw_exception(env, error);
	}
	FAULTPT_PXFS(FAULTNUM_PXFS_CREAT_S_E, FaultFunctions::generic);
}

//
// remove_fobj - remove a file object.
//
// There are multiple ways to support this operation:
//
// 1. A file in an HA file system can be directly deleted
// when the following conditions hold:
//	1. The invocation is local (this code path skips checkpointing),
//	2. There is exactly one client file object,
//	3. There are no references to the proxy file vnode belonging
//		to the client file object.
//	4. The client file object supports client side caching (we have to
//		have a CORBA object reference to the client file object).
//
//	This code path is considerably faster than the alternative for
//	files in an HA file system
//
// 2. Otherwise, in an HA file system
// move the deleted file to a "deleted" directory so they will
// still be in the file system.  This is to handle unlinked, open files
// which need to still exist after we switch to the secondary.
//
// 3. If a file is in a non-HA file system, pxfs deletes the file directly.
//
void
unixdir_ii::remove_fobj(const char *namep,
    solobj::cred_ptr credobj, Environment &env)
{
	cred_t			*credp = solobj_impl::conv(credobj);
	vnode_t			*fobj_vp;
	fobj_ii			*fobj_iip;
	unixdir_ii		*dir_iip = NULL;
	PXFS_VER::fobj_info	fobjinfo;
	PXFS_VER::fobj_var	fobjobj;
	int			error;
	vattr			oldvattr;
	uint64_t		delete_id = 0;
	os::hrtime_t		defer_time = 0;
	int			namelen;

	//
	// This check is required to avoid a cyclic deadlock (described below)
	// where the node panics.
	// - Assume the directory structure "/global/fs1/a/b/c/"
	// - We have 2 threads doing the following :
	// - Thread 1: Is trying to delete  ".." of dir "c" ie. dir "b"
	//   (unixdir_ii::remove_fobj())
	//	- It takes attr_lock of "c"
	//	- It takes dir_lock of "c"
	//	- Waiting for attr_lock of "b"
	// - Thread 2: Deleting the directory "c"
	//   (unixdir_ii::remove_dir())
	//	- It takes the attr_lock of "b" (parent dir).
	//	- Tries to take dir_lock of "c"
	//
	namelen = (int)os::strlen(namep);

	if (namep[0] == '.') {
		if (namelen == 1) {
			pxfslib::throw_exception(env, EINVAL);
			return;
		} else if (namelen == 2 && namep[1] == '.') {
			pxfslib::throw_exception(env, EEXIST);
			return;
		}
	}

	//
	// This variable does not have any relationship to checkpoint
	// transactions. Instead this variable is used to force file
	// meta-information to storage.
	//
	bool	need_transaction = !(cfs_enable_local_opt &&
	    env.is_local_client());

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	// If it is committed, return the result from the old primary.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);
	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		}
		return;
	} else if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

		//
		// The file existed on the failed primary
		//
		fobjobj = statep->get_target_fobj();
		ASSERT(!CORBA::is_nil(fobjobj));

		delete_id = statep->get_delete_id();
		ASSERT(delete_id != 0);

		fobj_iip = fobj_ii::get_fobj_ii(fobjobj);
		ASSERT(fobj_iip != NULL);

		if (is_replicated()) {
			//
			// Must ensure that server file object is ready
			//
			if (fobj_iip->is_dead(env)) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_UNIXDIR,
				    PXFS_RED,
				    ("remove_fobj(%p)ckpt is_dead %p file %s\n",
				    this, fobj_iip, namep));
				env.clear();
				error = EIO;
				//
				// Checkpoint (commit) the error.
				//
				get_ckpt()->ckpt_error_return(error, env);
				env.clear();
				goto remove_end;
			}
		}
		attr_lock.wrlock();
		goto checkpointed;
	}
	ASSERT(statep == NULL);

	//
	// If the deleted directory pointer is NULL, umount may have failed
	// and we will disallow deletions from this filesystem.
	//
	if (is_replicated() && (get_fsp()->get_deleted() == NULL)) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_UNIXDIR,
		    PXFS_RED,
		    ("remove_fobj(%p) deleted dir NUll fs %p\n",
		    this, get_fsp()));
		error = EIO;
		goto remove_end;
	}

	attr_lock.wrlock();
	//
	// We need to downgrade (flush) the attribute cache for any
	// files that are linked to the one being deleted.
	// Note: we assume that VOP_LOOKUP() on different links to the
	// same file yields the same vnode.
	//
#if	SOL_VERSION >= __s11
	error = VOP_LOOKUP(get_vp(), (char *)namep, &fobj_vp, NULL, 0, NULL,
	    credp, NULL, NULL, NULL);
#else
	error = VOP_LOOKUP(get_vp(), (char *)namep, &fobj_vp, NULL, 0, NULL,
	    credp);
#endif
	if (error != 0) {
		attr_lock.unlock();
		goto remove_end;
	}
	// Downgrade the attributes for the realvp not the specvp.
	if (IS_SPECIAL(fobj_vp)) {
		vnode_t		*realvp;
#if	SOL_VERSION >= __s11
		if (VOP_REALVP(fobj_vp, &realvp, NULL) == 0) {
#else
		if (VOP_REALVP(fobj_vp, &realvp) == 0) {
#endif
			VN_HOLD(realvp);
			VN_RELE(fobj_vp);
			fobj_vp = realvp;
		}
	}
	//
	// Get a fobj for the file being deleted
	// so that we can lock its file attributes.
	// We have this directory locked but if there is another link to
	// the file, the attributes can be changed via the other link.
	//
	fobjobj = get_fsp()->find_fobj(fobj_vp, fobjinfo, env);
	VN_RELE(fobj_vp);
	if (CORBA::is_nil(fobjobj)) {
		error = ENOTSUP;	// PXFS does not support all file types
		attr_lock.unlock();
		goto remove_end;	// Don't need to checkpoint error
	}

	fobj_iip = fobj_ii::get_fobj_ii(fobjobj);
	ASSERT(fobj_iip != NULL);

	if (is_replicated()) {
		//
		// Must ensure that server file object is ready
		//
		if (fobj_iip->is_dead(env)) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UNIXDIR,
			    PXFS_RED,
			    ("remove_fobj(%p) is_dead %p file %s\n",
			    this, fobj_iip, namep));
			env.clear();
			error = EIO;
			attr_lock.unlock();
			goto remove_end;
		}
		ASSERT(fobj_iip->is_active());
		//
		// Attempt to remove the file
		//
		if (remove_fobj_fastpath(namep, fobj_iip, credp, env)) {
			//
			// The file was processed by the fast path
			//
			attr_lock.unlock();
			FAULTPT_PXFS(FAULTNUM_PXFS_UNLINK_S_E,
			    FaultFunctions::generic);
			return;
		}
		// Cannot use the fast path for file removal

		//
		// Determine unique number to use for renaming this directory.
		//
		delete_id = get_fsp()->get_deletecnt();

		//
		// Checkpoint the existence of the file.
		//
		if (need_transaction) {
			get_ckpt()->ckpt_target_remove(fobjobj, fobjinfo,
			    delete_id, env);
		} else {
			get_ckpt()->ckpt_deletecnt(delete_id, env);
		}

		//
		// The only possible exception is a software
		// version mismatch exception.
		//
		ASSERT(!env.exception());
	}

checkpointed:

	//
	// Invalidate attribute caches for this directory so we
	// see the new modify time. We continue to hold the attribute
	// lock so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();

	fobj_iip->attr_lock.wrlock();

	//
	// Flush cached attributes for the file so the new link count is seen.
	//
	(void) fobj_iip->downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	if (fobj_iip->get_ftype() == PXFS_VER::fobj_unixdir) {
		//
		// Handle removing a directory using unlink instead of rmdir.
		// Only root can remove a directory.
		//
#if defined(_LEAST_PRIVS)
		if (secpolicy_fs_linkdir(credp, fobj_vp->v_vfsp) != 0) {
#else
		if (! suser(credp)) {
#endif
			error = EPERM;
			goto remove_unlock;
		}

		//
		// We always write lock the parent directory
		// before trying to lock a child directory so there should be
		// no deadlock possible (i.e., we always get the locks in the
		// same order).
		//
		dir_iip = (unixdir_ii *)fobj_iip;
		dir_iip->dir_lock.wrlock();

		// Check for a non-empty directory.
		if (!dir_iip->is_empty()) {
			error = EEXIST;
			goto remove_unlock;
		}

		//
		// Flush all cache entries for the directory we are removing.
		// This will flush the parent directory's entry too.
		//
		dir_iip->downgrade_dir_all();
	}

	//
	// Don't try to rename the file if it is in the deleted directory.
	//
	if (is_replicated() && !VN_CMP(get_vp(), get_fsp()->get_deleted())) {
		//
		// If there is another hard link to this file, we will
		// still be able to find the file via its file ID
		// so we don't need to rename it.
		//
		oldvattr.va_mask = AT_NLINK;
#if	SOL_VERSION >= __s11
		error = VOP_GETATTR(fobj_iip->get_vp(), &oldvattr, 0, credp,
		    NULL);
#else
		error = VOP_GETATTR(fobj_iip->get_vp(), &oldvattr, 0, credp);
#endif
		if (error != 0) {
			goto remove_unlock;
		}

		FAULTPT_PXFS(FAULTNUM_PXFS_UNLINK_S_B, FaultFunctions::generic);

		if (oldvattr.va_nlink > 1 && dir_iip == NULL) {
#if	SOL_VERSION >= __s11
			error = VOP_REMOVE(get_vp(), (char *)namep, credp,
			    NULL, 0);
#else
			error = VOP_REMOVE(get_vp(), (char *)namep, credp);
#endif
			if (error == 0) {
				if (need_transaction) {
					error =
					    get_fsp()->get_fs_dep_implp()->
					    fs_fsync(get_vp(), credp);
				} else {
					mod_time = os::gethrtime();
				}
			}
		} else {
			//
			// Unfortunately, since we had to create a fobj_ii,
			// we can't easily tell if there are any clients which
			// have a reference to the file.
			//
			// We rename the file so that if there is a failover,
			// the secondary will still be able to lookup the file
			// and create a fobj.
			//
			fobj_iip->set_delete_num(delete_id);
			vnode_t		*delvp = get_fsp()->get_deleted();
			ASSERT(delvp != NULL);

			char tbuf[21]; // Needs to hold uint64_t
			os::sprintf(tbuf, "%lld", delete_id);

			//
			// XXX We need to check in case the deleted
			// directory disappears.
			// XXX Note that if the file is open, the link
			// count will continue to be one; on NFS it stays
			// one, on UFS it is zero.
			// XXX This code should optimize the case
			// where there is no reference to the file
			// and it can just be deleted.
			//
#if	SOL_VERSION >= __s11
			error = VOP_RENAME(get_vp(), (char *)namep, delvp, tbuf,
			    credp, NULL, 0);
#else
			error = VOP_RENAME(get_vp(), (char *)namep, delvp, tbuf,
			    credp);
#endif
			if (error == 0) {
				if (need_transaction) {
					error = get_fsp()->get_fs_dep_implp()->
					    fs_fsync(get_vp(), credp);
				} else {
					mod_time = os::gethrtime();
				}
			}
		}

		FAULTPT_PXFS(FAULTNUM_PXFS_UNLINK_S_A, FaultFunctions::generic);

		if (statep != NULL && statep->server_did_op(error, false)) {
			//
			// VOP_XXX() says it doesn't exist now, but
			// saved state says it did exist before the
			// original primary tried to remove it so the
			// original op must have succeeded. We return
			// success.
			//
			error = 0;
		}
	} else {
		// Just a normal unreplicated PXFS remove.
#if	SOL_VERSION >= __s11
		error = VOP_REMOVE(get_vp(), (char *)namep, credp, NULL, 0);
#else
		error = VOP_REMOVE(get_vp(), (char *)namep, credp);
#endif
	}

remove_unlock:

	if (dir_iip != NULL) {
		dir_iip->dir_lock.unlock();
	}
	fobj_iip->attr_lock.unlock();

	//
	// Invalidate any cached entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	if (error == 0) {
		inval_name(env.get_src_node().ndid, namep);
	}
	dir_lock.unlock();
	attr_lock.unlock();

	if (need_transaction && is_replicated()) {
		if (error == 0) {
			// Checkpoint (commit) the rename.
			get_ckpt()->ckpt_delete_fobj(fobj_iip->get_delete_num(),
			    env);
		} else {
			//
			// Checkpoint (commit) the return error.
			//
			get_ckpt()->ckpt_error_return(error, env);
		}
		env.clear();
	}

remove_end:

	FAULTPT_PXFS(FAULTNUM_PXFS_UNLINK_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(env, error);
	}
}

//
// remove_fobj_fastpath - This method
// removes an HA file instead of renaming it to a file in the deleted directory
// when the necessary conditions hold.
//
bool
unixdir_ii::remove_fobj_fastpath(const char *namep,
    fobj_ii *fobj_iip, cred_t *credp, Environment &env)
{
	//
	// Check if the file can be removed instead of being renamed
	// to a file in the deleted directory.
	// Currently directories cannot be fast-removed.
	//
	if (!(env.is_local_client() &&
	    fobj_iip->can_cache() &&
	    fobj_iip->get_ftype() != PXFS_VER::fobj_unixdir)) {
		// Cannot use fast path
		return (false);
	}

	//
	// At this point we know that we have a file that supports caching.
	//
	fobjplus_ii	*fobjplus_iip = (fobjplus_ii *)fobj_iip;

	dir_lock.wrlock();

	//
	// Examine the proxy file clients of the file being removed
	// to determine whether the number of clients and node location
	// allow a fast remove.
	//
	if (!fobjplus_iip->fast_remove_test(env.get_src_node())) {
		dir_lock.unlock();

		// Cannot use fast path
		return (false);
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_UNIXDIR,
	    PXFS_GREEN,
	    ("remove_fobj_fast(%p) remove file %s type %d\n",
	    this, namep, fobj_iip->get_ftype()));

	int	error = 0;

	//
	// Invalidate the attribute caches for this directory.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	fobj_iip->attr_lock.wrlock();

	vattr		oldvattr;

	oldvattr.va_mask = AT_NLINK;
#if	SOL_VERSION >= __s11
	error = VOP_GETATTR(fobj_iip->get_vp(), &oldvattr, 0, credp, NULL);
#else
	error = VOP_GETATTR(fobj_iip->get_vp(), &oldvattr, 0, credp);
#endif

	if (error != 0 || oldvattr.va_nlink > 1) {
		//
		// When the file has multiple links,
		// the file will exist after this remove operation.
		// So we need to invalidate attributes.
		//
		// In case of error, assume multiple links exist.
		//
		(void) fobj_iip->downgrade_attr_all(PXFS_VER::attr_write,
		    false, 0);
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_UNLINK_S_B, FaultFunctions::generic);
	//
	// These conditions are true:
	//    1. There are no vnode references to the file on the client,
	//    2. The client and server are co-located, and
	//    3. There are no references to this file on another node.
	// Remove the file instead of renaming it to a file in the
	// deleted directory since it is not required to be available
	// after a failover/switchover (because of the conditions above).
	//
#if	SOL_VERSION >= __s11
	error = VOP_REMOVE(get_vp(), (char *)namep, credp, NULL, 0);
#else
	error = VOP_REMOVE(get_vp(), (char *)namep, credp);
#endif
	if (error == 0) {
	    mod_time = os::gethrtime();
	}
	FAULTPT_PXFS(FAULTNUM_PXFS_UNLINK_S_A, FaultFunctions::generic);

	fobj_iip->attr_lock.unlock();

	//
	// Since the file is not active on any other node,
	// there is no need to invalidate the DNLC entry for the file.
	//

	dir_lock.unlock();

	if (error) {
		pxfslib::throw_exception(env, error);
	}
	// The file removal operation was processed
	return (true);
}

//
// create_symlink
//
// The overall task here is to:
//	If committed, return checkpointed results.
//	Acquire the directory attribute lock.
//	Downgrade (flush) cached directory attributes.
//	Acquire the directory's lock.
//	Do the underlying vnode operation (VOP_XXX).
//	Downgrade (flush) cached directory lookups.
//	Release the directory's lock.
//	Release the directory attribute lock.
//
void
unixdir_ii::create_symlink(const char *nm, const sol::vattr_t &attr,
    const char *path, solobj::cred_ptr credobj,
    Environment &env)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	int	error;

	//
	// This variable does not have any relationship to checkpoint
	// transactions. Instead this variable is used to force file
	// meta-information to storage.
	//
	bool	need_transaction = !(cfs_enable_local_opt &&
	    env.is_local_client());

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	// If it is committed, return the result from the old primary.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);
	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		}
		return;
	}
	attr_lock.wrlock();

	if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

	} else if (need_transaction) {
		//
		// Checkpoint the existence of the link.
		//
		unixdir_state::do_ckpt_entry(this, nm, credp, env);

		//
		// The only possible exception is a software
		// version mismatch exception.
		//
		ASSERT(!env.exception());
	}

	//
	// Invalidate attribute caches for this directory so we
	// see the new modify time. We continue to hold the attribute
	// lock so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_SYMLINK_S_B, FaultFunctions::generic);

	if (get_delete_num() != 0) {
		//
		// The directory has been removed so don't do symlink.
		//
		error = ENOENT;
	} else {
		// Create the symlink in the underlying file system.
#if	SOL_VERSION >= __s11
		error = VOP_SYMLINK(get_vp(), (char *)nm,
		    (vattr_t *)&attr, (char *)path, credp, NULL, 0);
#else
		error = VOP_SYMLINK(get_vp(), (char *)nm,
		    (vattr_t *)&attr, (char *)path, credp);
#endif
		if (error == 0) {
			if (need_transaction) {
				error = get_fsp()->get_fs_dep_implp()->
				    fs_fsync(get_vp(), credp);
			} else {
				mod_time = os::gethrtime();
			}
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_SYMLINK_S_A, FaultFunctions::generic);

	if (statep != NULL && statep->server_did_op(error, true)) {
		//
		// Saved state says file did not exist before the original
		// primary tried to create the symbolic link yet
		// VOP_SYMLINK() says the file exists so the original
		// primary op must have succeeded. Since UFS doesn't
		// guarantee that the data for the symbolic link was
		// written to disk atomically, we have to verify the
		// operation fully completed.
		//
		error = check_symlink(nm, attr, path, credp);
	} else if (error == 0 && is_replicated()) {
		//
		// Make sure the contents are on disk before sending the
		// checkpoint.
		//
		vnode_t	*lvp;
#if	SOL_VERSION >= __s11
		error = VOP_LOOKUP(get_vp(), (char *)nm, &lvp, NULL, 0, NULL,
		    credp, NULL, NULL, NULL);
#else
		error = VOP_LOOKUP(get_vp(), (char *)nm, &lvp, NULL, 0, NULL,
		    credp);
#endif
		if (error == 0) {
#if	SOL_VERSION >= __s11
			error = VOP_FSYNC(lvp, FSYNC, credp, NULL);
#else
			error = VOP_FSYNC(lvp, FSYNC, credp);
#endif
			VN_RELE(lvp);
		}
	}

	//
	// Invalidate any cached entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	if (error == 0) {
		inval_name(env.get_src_node().ndid, nm);
	}
	dir_lock.unlock();

	attr_lock.unlock();

	if (need_transaction && is_replicated()) {
		//
		// Checkpoint (commit) the return error.
		//
		if (error == 0) {
			// Optimized commit checkpoint.
			commit(env);
		} else {
			get_ckpt()->ckpt_error_return(error, env);
		}
		env.clear();
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_SYMLINK_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(env, error);
	}
}

//
// Check to see if a symbolic link was created correctly.
// At this point we know the VOP_SYMLINK() was tried and returned EEXIST.
//
int
unixdir_ii::check_symlink(const char *nm, const sol::vattr_t &attributes,
    const char *path, cred_t *credp)
{
	vnode_t	*lvp;
	uio_t	auio;
	iovec_t	iov;
	int	error;
	char	*cp;

	// Get vnode pointer to the symlink.
#if	SOL_VERSION >= __s11
	error = VOP_LOOKUP(get_vp(), (char *)nm, &lvp, NULL, 0, NULL, credp,
	    NULL, NULL, NULL);
#else
	error = VOP_LOOKUP(get_vp(), (char *)nm, &lvp, NULL, 0, NULL, credp);
#endif
	if (error != 0) {
		return (error);
	}
	ASSERT(lvp->v_type == VLNK);

	iov.iov_base = cp = new char [MAXPATHLEN];
	iov.iov_len = MAXPATHLEN;

	auio.uio_iov = &iov;
	auio.uio_iovcnt = 1;
	auio.uio_loffset = 0;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_resid = MAXPATHLEN;
	auio.uio_fmode = 0;

	// Read the symlink and see if its what we expected.
#if	SOL_VERSION >= __s11
	error = VOP_READLINK(lvp, &auio, credp, NULL);
#else
	error = VOP_READLINK(lvp, &auio, credp);
#endif
	VN_RELE(lvp);
	if (error == 0 &&
	    strlen(path) == (size_t)(MAXPATHLEN - auio.uio_resid) &&
	    // LINTED: Possibly passing a null pointer to function strncmp()
	    strncmp(path, cp, (size_t)(MAXPATHLEN - auio.uio_resid)) == 0) {
		//
		// Everyting is OK so return no error.
		//
		delete [] cp;
		return (0);
	}

	delete [] cp;

	// Remove the corrupted symlink.
#if	SOL_VERSION >= __s11
	error = VOP_REMOVE(get_vp(), (char *)nm, credp, NULL, 0);
#else
	error = VOP_REMOVE(get_vp(), (char *)nm, credp);
#endif
	if (error != 0) {
		return (error);
	}

	// Create the symlink in the underlying file system (again).
#if	SOL_VERSION >= __s11
	error = VOP_SYMLINK(get_vp(), (char *)nm, (vattr_t *)&attributes,
	    (char *)path, credp, NULL, 0);
#else
	error = VOP_SYMLINK(get_vp(), (char *)nm, (vattr_t *)&attributes,
	    (char *)path, credp);
#endif
	if (error != 0) {
		return (error);
	}

	// Do a lookup so we can flush the link data to disk.
#if	SOL_VERSION >= __s11
	error = VOP_LOOKUP(get_vp(), (char *)nm, &lvp, NULL, 0, NULL, credp,
	    NULL, NULL, NULL);
#else
	error = VOP_LOOKUP(get_vp(), (char *)nm, &lvp, NULL, 0, NULL, credp);
#endif
	if (error == 0) {
#if	SOL_VERSION >= __s11
		error = VOP_FSYNC(lvp, FSYNC, credp, NULL);
#else
		error = VOP_FSYNC(lvp, FSYNC, credp);
#endif
		VN_RELE(lvp);
	}
	return (error);
}

//
// create_dir - create a new directory object in this directory
//
// The overall task here is to:
//	If committed, return checkpointed results.
//	Acquire the directory attribute lock.
//	Downgrade (flush) cached directory attributes.
//	Acquire the directory's lock.
//	Do the underlying vnode operation (VOP_XXX).
//	Downgrade (flush) cached directory lookups.
//	Release the directory's lock.
//	Release the directory attribute lock.
//
void
unixdir_ii::create_dir(const char *dirnm, const sol::vattr_t &attr,
    PXFS_VER::fobj_out dir, PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, Environment &env)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	vnode_t	*targetvp = 0;
	int	error;

	client2_p = PXFS_VER::fobj_client::_nil();

	// See comments for create_fobj.
	bool	need_transaction = !(cfs_enable_local_opt &&
		    env.is_local_client());

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	// If it is committed, return the result from the old primary.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);
	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		} else {
			dir = statep->get_ret_fobj();
			statep->get_fobjinfo(fobjinfo);
			ASSERT(!CORBA::is_nil(dir));
			fobj_ii		*dir_iip = fobj_ii::get_fobj_ii(dir);
			//
			// May need FID conversion
			//
			if (is_replicated()) {
				(void) dir_iip->is_dead(env);
				if (env.exception()) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_UNIXDIR,
					    PXFS_RED,
					    ("create_dir(%p) is_dead %p "
					    "error\n",
					    this, dir_iip));
					env.clear();
					CORBA::release(dir);
					dir = PXFS_VER::fobj::_nil();
					pxfslib::throw_exception(env, EIO);
					return;
				}
			}
			//
			// After a node failure, someone else could have
			// done a lookup on the newly created directory
			// before the retry was processed. So a fobj_client
			// can already exist if this sequence of events occurs.
			//
			// fobj_client object reference returned by
			// get_bind_info() already has the reference
			// count incremented.
			//
			client2_p = dir_iip->
			    get_bind_info(binfo, client1_p, kcred, env);
		}
		return;
	}
	attr_lock.wrlock();
	if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

	} else if (need_transaction) {
		//
		// Checkpoint the existence of the directory.
		//
		unixdir_state::do_ckpt_entry(this, dirnm, credp, env);

		//
		// The only possible exception is a software version
		// mismatch exception.
		//
		ASSERT(!env.exception());
	}
	//
	// Invalidate attribute caches for this directory so we
	// see the new modify time. We continue to hold the attribute
	// lock so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_MKDIR_S_B, FaultFunctions::generic);

	if (get_delete_num() != 0) {
		//
		// The directory has been removed so don't create dir.
		//
		error = ENOENT;
	} else {
		// Create the directory in the underlying file system.
#if	SOL_VERSION >= __s11
		error = VOP_MKDIR(get_vp(), (char *)dirnm,
		    (vattr_t *)&attr, &targetvp, credp, NULL, 0, NULL);
#else
		error = VOP_MKDIR(get_vp(), (char *)dirnm,
		    (vattr_t *)&attr, &targetvp, credp);
#endif
		if (error == 0) {
			if (need_transaction) {
				error = get_fsp()->get_fs_dep_implp()->fs_fsync(
				    get_vp(), credp);
			} else {
				mod_time = os::gethrtime();
			}
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_MKDIR_S_A, FaultFunctions::generic);

	if (statep != NULL && statep->server_did_op(error, true)) {
		//
		// The create operation says the new directory exists,
		// but the checkpointed primary state says it doesn't.
		// So the create operation on the old primary
		// must have succeeded.  So we return success.
		//
		// We need to get the vnode of the new directory.
		//
#if	SOL_VERSION >= __s11
		error = VOP_LOOKUP(get_vp(), (char *)dirnm, &targetvp, NULL, 0,
		    NULL, credp, NULL, NULL, NULL);
#else
		error = VOP_LOOKUP(get_vp(), (char *)dirnm, &targetvp, NULL, 0,
		    NULL, credp);
#endif
	}

	//
	// Invalidate any cached entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	if (error == 0) {
		nodeid_t	ndid = env.get_src_node().ndid;
		inval_name(ndid, dirnm);

		//
		// Show that the directory can have cached directory info.
		// This is safe even if we have already done this.
		//
		dir_token_lock.lock();
		cached_dir_info.add_node(ndid);
		dir_token_lock.unlock();
	}
	dir_lock.unlock();

	//
	// If the directory vnode has been successfully created, create
	// the associated unixdir object.
	//
	if (error == 0) {
		dir = get_fsp()->find_fobj(targetvp, fobjinfo, env);

		attr_lock.unlock();

		VN_RELE(targetvp);
		if (CORBA::is_nil(dir)) {
			//
			// If dir fobj is NULL it means that
			// we encountered a vnode of unknown type
			// during lookup, probably due to a corruption
			// in the underlying file system. So return ENOTSUP
			//
			error = ENOTSUP;
		} else {
			fobj_ii		*fobj_iip = fobj_ii::get_fobj_ii(dir);

			//
			// May need FID conversion
			//
			if (fobj_iip->is_replicated()) {
				(void) fobj_iip->is_dead(env);
				if (env.exception()) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_UNIXDIR,
					    PXFS_RED,
					    ("create_dir(%p) is_dead %p "
					    "error #1\n",
					    this, fobj_iip));
					env.clear();
					CORBA::release(dir);
					dir = PXFS_VER::fobj::_nil();
					if (need_transaction) {
						get_ckpt()->ckpt_error_return(
						    EIO, env);
					}
					pxfslib::throw_exception(env, EIO);
					return;
				}
			}

			//
			// fobj_client object reference returned by
			// get_bind_info() already has the reference
			// count incremented.
			//
			client2_p = fobj_iip->
			    get_bind_info(binfo, client1_p, kcred, env);
		}
	} else {
		attr_lock.unlock();
	}
	if (error == 0) {
		if (need_transaction && is_replicated()) {
			//
			// Checkpoint (commit) the return object.
			//
			get_ckpt()->ckpt_fobj_return(dir, fobjinfo, env);
			env.clear();
		}
	} else {
		if (need_transaction && is_replicated()) {
			//
			// Checkpoint (commit) the return error.
			//
			get_ckpt()->ckpt_error_return(error, env);
			env.clear();
		}
		pxfslib::throw_exception(env, error);
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_MKDIR_S_E, FaultFunctions::generic);
}

//
// remove_dir - remove a directory object from this directory
//
// The overall task here is to:
//	If committed, return checkpointed results.
//	Acquire the parent directory attribute lock.
//	Downgrade (flush) parent cached directory attributes.
//	Acquire the parent directory's lock.
//	Acquire target directory's lock.
//	Downgrade (flush) cached target directory lookups.
//	Do the underlying vnode operation (VOP_XXX).
//	Release target directory's lock.
//	Downgrade (flush) cached directory lookups.
//	Release the parent directory's lock.
//	Release the parent directory attribute lock.
//
void
unixdir_ii::remove_dir(const char *dirnm, PXFS_VER::unixdir_ptr cur_dir,
    solobj::cred_ptr credobj, Environment &env)
{
	cred_t			*credp = solobj_impl::conv(credobj);
	vnode_t			*cdir;
	vnode_t			*dir_vp;
	unixdir_ii		*dir_iip;
	PXFS_VER::fobj_info		fobjinfo;
	PXFS_VER::fobj_var		dir_fobj;
	int			error;
	uint64_t		delete_id = 0;
	int			namelen;

	//
	// This check is required to avoid a cyclic deadlock (described below)
	// where the node panics.
	// - Assume the directory structure "/global/fs1/a/b/c/"
	// - We have 2 threads doing the following :
	// - Thread 1: Is trying to delete  ".." of dir "c" ie. dir "b"
	//   (unixdir_ii::remove_fobj())
	//	- It takes attr_lock of "c"
	//	- It takes dir_lock of "c"
	//	- Waiting for attr_lock of "b"
	// - Thread 2: Deleting the directory "c"
	//   (unixdir_ii::remove_dir())
	//	- It takes the attr_lock of "b" (parent dir).
	//	- Tries to take dir_lock of "c"
	//
	namelen = (int)os::strlen(dirnm);

	if (dirnm[0] == '.') {
		if (namelen == 1) {
			pxfslib::throw_exception(env, EINVAL);
			return;
		} else if (namelen == 2 && dirnm[1] == '.') {
			pxfslib::throw_exception(env, EEXIST);
			return;
		}
	}

	//
	// This variable does not have any relationship to checkpoint
	// transactions. Instead this variable is used to force file
	// meta-information to storage.
	//
	bool	need_transaction = !(cfs_enable_local_opt &&
	    env.is_local_client());

	//
	// Convert the CORBA current directory object reference into
	// a local vnode pointer.
	// 'cur_dir' should be nil if
	// 1) the current directory is not a PXFS directory, or
	// 2) the current directory is not in the same file system as this
	// directory.
	//
	if (CORBA::is_nil(cur_dir)) {
		cdir = NULL;
	} else {
		unixdir_ii	*cdir_iip =
				    (unixdir_ii *)fobj_ii::get_fobj_ii(cur_dir);

		//
		// Ensure that the current directory is primary ready
		//
		if (is_replicated()) {
			(void) cdir_iip->is_dead(env);
			if (env.exception()) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_UNIXDIR,
				    PXFS_RED,
				    ("remove_dir(%p) is_dead %p error\n",
				    this, cdir_iip));
				env.clear();
				if (need_transaction) {
					get_ckpt()->ckpt_error_return(EIO, env);
				}
				pxfslib::throw_exception(env, EIO);
				return;
			}
		}
		//
		// Obtaining the vnode in this manner does not change
		// the reference count of the vnode.
		//
		cdir = cdir_iip->get_vp();
	}

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	// If it is committed, return the result from the old primary.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);

	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		}
		return;

	} else if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

		dir_fobj = statep->get_target_fobj();
		ASSERT(!CORBA::is_nil(dir_fobj));

		delete_id = statep->get_delete_id();
		ASSERT(delete_id != 0);

		attr_lock.wrlock();
		goto checkpointed;
	}
	ASSERT(statep == NULL);

	//
	// If the deleted directory pointer is NULL, umount may have failed
	// and we will disallow deletions from this filesystem.
	//
	if (is_replicated() && (get_fsp()->get_deleted() == NULL)) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_UNIXDIR,
		    PXFS_RED,
		    ("remove_dir(%p) deleted directory fs %p is NULL\n",
		    this, get_fsp()));
		error = EIO;
		goto remove_end;
	}

	attr_lock.wrlock();

	//
	// Determine whether the target directory file exists.
	//
#if	SOL_VERSION >= __s11
	error = VOP_LOOKUP(get_vp(), (char *)dirnm, &dir_vp, NULL, 0, NULL,
	    credp, NULL, NULL, NULL);
#else
	error = VOP_LOOKUP(get_vp(), (char *)dirnm, &dir_vp, NULL, 0, NULL,
	    credp);
#endif
	if (error != 0) {
		// Cannot remove a directory if we cannot find it.
		attr_lock.unlock();
		goto remove_end;
	}
	if (dir_vp->v_type != VDIR) {
		attr_lock.unlock();
		VN_RELE(dir_vp);
		error = ENOTDIR;
		goto remove_end;	// Don't need to checkpoint error
	}
	//
	// Create a fobj so we can lock the directory while we change
	// the state on all the proxy clients.
	//
	dir_fobj = get_fsp()->find_fobj(dir_vp, fobjinfo, env);
	VN_RELE(dir_vp);
	if (CORBA::is_nil(dir_fobj)) {
		attr_lock.unlock();
		error = ENOTSUP;	// XXX need to support all file types
		goto remove_end;
	}
	if (is_replicated()) {
		//
		// Determine unique number to use for renaming this directory.
		//
		delete_id = get_fsp()->get_deletecnt();

		//
		// Checkpoint directory existence and the id for renaming it.
		// Must use the same id for renaming on any retry.
		//
		if (need_transaction) {
			get_ckpt()->ckpt_target_remove(dir_fobj, fobjinfo,
			    delete_id, env);
		} else {
			get_ckpt()->ckpt_deletecnt(delete_id, env);
		}

		//
		// The only possible exception is a software
		// version mismatch exception.
		//
		ASSERT(!env.exception());
	}

checkpointed:

	//
	// Invalidate attribute caches for parent directory so we
	// see the new modify time. We continue to hold the attribute
	// lock so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();

	//
	// XXX This cast could be unsafe if the file system factory can
	// create directory objects which don't inherit from unixdir_ii.
	//
	ASSERT(fobj_ii::get_fobj_ii(dir_fobj)->get_ftype() ==
	    PXFS_VER::fobj_unixdir);
	dir_iip = (unixdir_ii *)fobj_ii::get_fobj_ii(dir_fobj);

	//
	// Ensure that the directory being removed is primary ready
	//
	if (is_replicated()) {
		(void) dir_iip->is_dead(env);
		if (env.exception()) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UNIXDIR,
			    PXFS_RED,
			    ("remove_dir(%p) is_dead %p error #2\n",
			    this, dir_iip));
			env.clear();
			error = EIO;

			//
			// Need to temporarily lock this
			// so that cleanup can unlock
			//
			dir_iip->dir_lock.wrlock();
			goto remove_unlock;
		}
	}
	//
	// Directories can't have hard links to them like files can
	// so we shouldn't need to flush the attributes for the directory
	// being removed.
	//

	//
	// We always write lock the parent directory
	// before trying to lock a child directory. So there should be
	// no deadlock possible (i.e., we always get the locks in the same
	// order).
	//
	dir_iip->dir_lock.wrlock();

	//
	// Flush all DNLC cache entries for the directory we are removing.
	//
	dir_iip->downgrade_dir_all();

	//
	// Don't try to rename the directory if it is in the deleted directory.
	//
	if (is_replicated() &&
	    !VN_CMP(get_vp(), get_fsp()->get_deleted())) {
		//
		// Check that directory to be removed is not the current
		// directory.
		//
		if (VN_CMP(dir_iip->get_vp(), cdir)) {
			error = EINVAL;
			goto remove_unlock;
		}

		//
		// Check this directory's permissions since they won't be
		// checked by VOP_RENAME(kcred).
		//
#if	SOL_VERSION >= __s11
		error = VOP_ACCESS(get_vp(), VWRITE | VEXEC, 0, credp, NULL);
#else
		error = VOP_ACCESS(get_vp(), VWRITE | VEXEC, 0, credp);
#endif
		if (error != 0) {
			goto remove_unlock;
		}
		// Check for a non-empty directory.
		if (!dir_iip->is_empty()) {
			error = EEXIST;
			goto remove_unlock;
		}

		//
		// Unfortunately, since we had to create a fobj_ii,
		// we can't easily tell if there are any clients which
		// have a reference to the file.
		//
		// We rename the file so that if there is a failover,
		// the secondary will still be able to lookup the file
		// and create a fobj.
		//
		dir_iip->set_delete_num(delete_id);
		vnode_t		*delvp = get_fsp()->get_deleted();
		ASSERT(delvp != NULL);

		char		tbuf[21];	// Needs to hold uint64_t
		os::sprintf(tbuf, "%lld", delete_id);

		//
		// XXX This code should optimize the case
		// where there is no reference to the directory
		// and it can just be deleted.
		//

		FAULTPT_PXFS(FAULTNUM_PXFS_RMDIR_S_B, FaultFunctions::generic);

#if	SOL_VERSION >= __s11
		error = VOP_RENAME(get_vp(), (char *)dirnm, delvp, tbuf, kcred,
		    NULL, 0);
#else
		error = VOP_RENAME(get_vp(), (char *)dirnm, delvp, tbuf, kcred);
#endif
		if (error == 0) {
			if (need_transaction) {
				error = get_fsp()->get_fs_dep_implp()->
				    fs_fsync(get_vp(), credp);
			} else {
				mod_time = os::gethrtime();
			}
		}

		FAULTPT_PXFS(FAULTNUM_PXFS_RMDIR_S_A, FaultFunctions::generic);

		if (statep != NULL && statep->server_did_op(error, false)) {
			//
			// VOP_RENAME() says directory doesn't exist now, but
			// saved state says directory did exist
			// before the original primary tried to rename
			// the directory. So the original rename
			// must have succeeded. We return success.
			//
			error = 0;
		}
	} else {
		// Just a normal unreplicated PXFS remove.
#if	SOL_VERSION >= __s11
		error = VOP_RMDIR(get_vp(), (char *)dirnm, cdir, credp,
		    NULL, 0);
#else
		error = VOP_RMDIR(get_vp(), (char *)dirnm, cdir, credp);
#endif
	}

remove_unlock:

	dir_iip->dir_lock.unlock();

	//
	// Invalidate any cached entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	if (error == 0) {
		inval_name(env.get_src_node().ndid, dirnm);
	}
	dir_lock.unlock();

	attr_lock.unlock();

	if (need_transaction && is_replicated()) {
		if (error == 0) {
			// Checkpoint (commit) the rename.
			get_ckpt()->ckpt_delete_fobj(dir_iip->get_delete_num(),
			    env);
		} else {
			//
			// Checkpoint (commit) the return error.
			//
			get_ckpt()->ckpt_error_return(error, env);
		}
		env.clear();
	}

remove_end:

	FAULTPT_PXFS(FAULTNUM_PXFS_RMDIR_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(env, error);
	}
}

void
unixdir_ii::readdir(PXFS_VER::unixdir::diroff_t &, sol::size_t,
    PXFS_VER::unixdir::direntrylist_t_out, bool &, solobj::cred_ptr,
    Environment &_environment)
{
	// XXX - not implemented yet
	pxfslib::throw_exception(_environment, ENOTSUP);
}

//
// unixdir_ii::readdir_raw - read the contents of a directory in the binary
//	format. The format is not portable across CPU's with incompatible
//	data format.
//
void
unixdir_ii::readdir_raw(PXFS_VER::unixdir::diroff_t &offset, sol::size_t maxlen,
    PXFS_VER::unixdir::rawdir_t_out rawdir, bool &eof_flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	int	eof = 0;
	uio_t	auio;
	iovec_t	iov;
	int	error = 0;

	//
	// This variable does not have any relationship to checkpoint
	// transactions. Instead this variable is used to force file
	// meta-information to storage.
	//
	bool	need_transaction = !(cfs_enable_local_opt &&
	    _environment.is_local_client());

	bool	deleted_directory_found = false;

	PXFS_VER::unixdir::rawdir_t	*rawdirp =
	    new PXFS_VER::unixdir::rawdir_t((uint_t)maxlen, (uint_t)maxlen);

	auio.uio_iov = &iov;
	auio.uio_iovcnt = 1;
	auio.uio_loffset = (offset_t)offset;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_llimit = (rlim64_t)RLIM_INFINITY;
	auio.uio_fmode = FREAD;

readdir_again:

	auio.uio_resid = (ssize_t)maxlen;

	iov.iov_base = (caddr_t)rawdirp->buffer();
#if defined(_LP64)
	iov.iov_len = maxlen;
#else
	iov.iov_len = (long)maxlen;
#endif

	//
	// Invalidate attribute caches for this directory so we
	// see the new access time. We hold the fobj attribute lock
	// so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	attr_lock.wrlock();
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock the directory so that we return consistent results
	// if caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.rdlock();

	if (get_delete_num() != 0) {
		//
		// The directory has been removed so don't return contents.
		//
		error = 0;
		eof = 1;
	} else {
		if (need_transaction) {
			//
			// If this is a remote request for a lookup, make sure
			// the directory contents are on disk.  See
			// CLARC/2000/018 for more.
			//
			error = fs_implp->get_fs_dep_implp()->sync_if_necessary(
			    mod_time, get_vp(), credp);
		}
		if (error == 0) {
#if SOL_VERSION >= __s10
			VOP_RWLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
			VOP_RWLOCK(get_vp(), 0);
#endif
#if	SOL_VERSION >= __s11
			error = VOP_READDIR(get_vp(), &auio, credp, &eof,
			    NULL, 0);
#else
			error = VOP_READDIR(get_vp(), &auio, credp, &eof);
#endif
#if SOL_VERSION >= __s10
			VOP_RWUNLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
			VOP_RWUNLOCK(get_vp(), 0);
#endif
		}
	}

	dir_lock.unlock();

	attr_lock.unlock();

	if (error == 0) {
		rawdirp->length((uint_t)(maxlen - (size_t)auio.uio_resid));

		// Fix up results to hide the deleted directory.
		if (is_replicated() && (get_vp()->v_flag & VROOT) &&
		    !deleted_directory_found) {

			size_t		dir_count = rawdirp->length();

			struct		dirent64 *dp =
			    (struct dirent64 *)rawdirp->buffer();

			// Initialize previous dirent offset
			off64_t		*prev_d_off = &(dp->d_off);

			while (dir_count >= sizeof (struct dirent64)) {
				ASSERT(dp->d_reclen != 0);
				dir_count -= dp->d_reclen;
				if (strcmp(dp->d_name, fs_ii::deleted_dir)) {
					//
					// Found the deleted directory
					//
					prev_d_off = &(dp->d_off);
					dp = (struct dirent64 *)((char *)dp +
					    dp->d_reclen);
					continue;
				}

				//
				// We found the entry, fix up the results.
				//
				if ((rawdirp->length() == dp->d_reclen) &&
				    !eof) {
					//
					// The deleted directory is the ONLY
					// entry in this buffer, and there's
					// more data to follow. We can't just
					// hide it (and return nothing), as this
					// causes the above layers to think
					// this is the end of the directory.
					// Call VOP_READDIR() again from the
					// current offset, and pass the
					// results back directly without
					// entering this check for the deleted
					// directory (we know the results won't
					// contain "._")
					//
					deleted_directory_found = true;
					goto readdir_again;
				}
				//
				// Set the length before we do the copy
				// and change the value of d_reclen.
				// We also change the previous dirent's d_off
				// so that it has the same value as the
				// deleted directory. This is to make the data
				// passed back consistent, and avoids
				// confusing NFS (which uses d_off values)
				//
				rawdirp->length((uint_t)(maxlen -
				    (size_t)auio.uio_resid - dp->d_reclen));
				*prev_d_off = dp->d_off;
				if (dir_count != 0) {
					ovbcopy((char *)dp + dp->d_reclen, dp,
					    dir_count);
				}
				break;
			}
		}
		offset = (PXFS_VER::unixdir::diroff_t)auio.uio_loffset;
		eof_flag = (bool)eof;
		rawdir = rawdirp;
	} else {
		delete rawdirp;
		pxfslib::throw_exception(_environment, error);
	}
}

//
// link_fobj - link a file object
//
// The overall task here is to:
//	If committed, return checkpointed results.
//	Acquire the directory attribute lock.
//	Downgrade (flush) cached directory attributes.
//	Acquire the directory's lock.
//	Acquire target attribute lock.
//	Downgrade (flush) cached target attributes.
//	Do the underlying vnode operation (VOP_XXX).
//	Release target attribute lock.
//	Downgrade (flush) cached directory lookups.
//	Release the directory's lock.
//	Release the directory attribute lock.
//
void
unixdir_ii::link_fobj(PXFS_VER::fobj_ptr fobj, const char *newnm,
    solobj::cred_ptr credobj, Environment &env)
{
	Environment	e;
	cred_t		*credp = solobj_impl::conv(credobj);
	fobj_ii		*fobj_iip;
	int		error;

	//
	// This variable does not have any relationship to checkpoint
	// transactions. Instead this variable is used to force file
	// meta-information to storage.
	//
	bool		need_transaction = !(cfs_enable_local_opt &&
	    env.is_local_client());

	//
	// Convert the CORBA fobj pointer into a local implementation pointer.
	// The fobjobj has to be a local fobj.
	//
	fobj_iip = fobj_ii::get_fobj_ii(fobj);
	//
	// Must force a FID conversion since
	// we are executing on the primary
	//
	if (is_replicated()) {
		(void) fobj_iip->is_dead(env);
		if (env.exception()) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UNIXDIR,
			    PXFS_RED,
			    ("link_fobj(%p) is_dead %p error\n",
			    this, fobj_iip));
			env.clear();
			if (need_transaction) {
				get_ckpt()->ckpt_error_return(EIO, env);
			}
			return;
		}
	}
	if (VN_CMP(fobj_iip->get_vp(), get_vp())) {
		//
		// Both objects can be the same so be sure to not
		// acquire the lock twice.
		//
		fobj_iip = NULL;
	}

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	// If it is committed, return the result from the old primary.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);
	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		}
		return;

	}

	//
	// We need to acquire the write lock for the attributes of
	// both the directory and the file we are creating a new
	// link to since the VOP_LINK() will modify the attributes of
	// both (directory modify time and va_nlink of the file).
	// Note that the file could be any file in this file system and
	// not necessarily in the same directory that we are creating the
	// new link in.
	// Since we are acquiring the attribute lock on both a
	// directory and a file, we must lock the directory before locking
	// the file since this is the order defined by create_fobj().
	//
	attr_lock.wrlock();
	if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

	} else if (need_transaction) {
		//
		// Since the client had to do a lookup in order
		// to give us an fobj, we know the source file exists.
		// We checkpoint whether the target exists so
		// if the target exists the first time, we
		// return with EEXIST; on a retry, we skip this check
		// and if VOP_LINK() returns EEXIST, we know the
		// operation succeeded on original primary.
		// XXX optimization: return EEXIST if lookup succeeds
		// here.
		//
		unixdir_state::do_ckpt_entry(this, newnm, credp, env);

		//
		// The only possible exception is a software
		// version mismatch exception.
		//
		ASSERT(!env.exception());
	}

	//
	// Invalidate attribute caches for this directory so we
	// see the new modify time. We continue to hold the attribute
	// lock so no other thread can read the directory attributes until the
	// underlying file system operation has modified the attributes.
	//
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();

	//
	// Invalidate attribute caches for the object we are linking to.
	//
	if (fobj_iip != NULL) {
		fobj_iip->attr_lock.wrlock();

		if (fobj_iip->get_delete_num() != 0) {
			//
			// The source fobj has been removed so don't allow
			// the link.
			//
			error = ENOENT;
			goto link_return;
		}

		(void) fobj_iip->downgrade_attr_all(PXFS_VER::attr_write,
		    false, 0);
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_LINK_S_B, FaultFunctions::generic);

	if (get_delete_num() != 0) {
		//
		// This directory has been removed so don't allow the link.
		//
		error = ENOENT;
		goto link_return;
	} else {
#if	SOL_VERSION >= __s11
		error = VOP_LINK(get_vp(),
		    fobj_iip != NULL ? fobj_iip->get_vp() : get_vp(),
		    (char *)newnm, credp, NULL, 0);
#else
		error = VOP_LINK(get_vp(),
		    fobj_iip != NULL ? fobj_iip->get_vp() : get_vp(),
		    (char *)newnm, credp);
#endif
		if (error == 0) {
			if (need_transaction) {
				error = get_fsp()->get_fs_dep_implp()->fs_fsync(
				    get_vp(), credp);
			} else {
				mod_time = os::gethrtime();
			}
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_LINK_S_A, FaultFunctions::generic);

	if (statep != NULL && statep->server_did_op(error, true)) {
		//
		// Saved state says link did not exist before the original
		// primary tried to create it but the VOP_LINK above says
		// the link exists so the original op must have succeeded.
		// We return success.
		//
		error = 0;
	}

link_return:

	//
	// Release lock on the file since it isn't needed for the invalidate
	// below.
	//
	if (fobj_iip != NULL) {
		fobj_iip->attr_lock.unlock();
	}
	//
	// Invalidate any cached entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	if (error == 0) {
		nodeid_t	ndid = env.get_src_node().ndid;
		inval_name(ndid, newnm);

		//
		// Show that the directory can have cached directory info.
		// This is safe even if we have already done this.
		//
		dir_token_lock.lock();
		cached_dir_info.add_node(ndid);
		dir_token_lock.unlock();
	}
	dir_lock.unlock();

	attr_lock.unlock();

	if (need_transaction && is_replicated()) {
		//
		// Checkpoint (commit) the return error.
		//
		if (error == 0) {
			// Optimized commit checkpoint.
			commit(env);
		} else {
			get_ckpt()->ckpt_error_return(error, env);
		}
		env.clear();
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_LINK_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(env, error);
	}
}

//
// rename_fobj - rename a file object
//
// The overall task here is to:
//	If committed, return checkpointed results.
//	Acquire the source & destination directory attribute locks.
//	Downgrade (flush) cached source & destination directory attributes.
//	Acquire the source & destination directory locks.
//	Do the underlying vnode operation (VOP_XXX).
//	Downgrade (flush) cached source & destination directory lookups.
//	Release the source & destination directory locks.
//	Release the source & destination directory attribute locks.
//
void
unixdir_ii::rename_fobj(const char *sourcenm,	// Name of file being renamed
    PXFS_VER::unixdir_ptr target_dir,		// New directory for file
    const char *targetnm,			// New name for file
    solobj::cred_ptr credobj,			// User credentials
    PXFS_VER::unixdir_ptr sourceobj,		// Non-null when renaming a dir
    Environment &env)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	unixdir_ii	*dest_unixdir_iip;
	fobj_ii		*dest_fobj_iip;
	unixdir_ii	*src_iip = NULL;
	int		error;

	//
	// This variable does not have any relationship to checkpoint
	// transactions. Instead this variable is used to force file
	// meta-information to storage.
	//
	bool	need_transaction = !(cfs_enable_local_opt &&
	    env.is_local_client());

	dest_fobj_iip = fobj_ii::get_fobj_ii(target_dir);

	//
	// vn_rename() will check that the source and destination directories
	// are in the same file system. If for some reason PXFS allows
	// the target directory to be on another node, we return EXDEV
	// as if the destination directory is in another file system.
	//
	if (dest_fobj_iip == NULL) {
		pxfslib::throw_exception(env, EXDEV);
		return;
	}
	//
	// Ensure that the destination directory server object
	// is primary ready
	//
	if (is_replicated()) {
		(void) dest_fobj_iip->is_dead(env);
		if (env.exception()) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UNIXDIR,
			    PXFS_RED,
			    ("rename_fobj(%p) is_dead %p error\n",
			    this, dest_fobj_iip));
			env.clear();
			if (need_transaction) {
				get_ckpt()->ckpt_error_return(EIO, env);
			}
			pxfslib::throw_exception(env, EIO);
			return;
		}
	}

	ASSERT(dest_fobj_iip->get_ftype() == PXFS_VER::fobj_unixdir);
	dest_unixdir_iip = (unixdir_ii *)dest_fobj_iip;

	//
	// Obtain the pxfs file system rename lock in order to prevent deadlocks
	// (e.g., a rename from directory A to B and B to A at the same time).
	//
	get_fsp()->lock_renames();

	//
	// We must determine if this is a new transaction, a transaction
	// in progress, or a committed transaction, and act appropriately.
	// If it is committed, return the result from the old primary.
	//
	unixdir_state			*statep;
	unixdir_state::progress		state =
	    unixdir_state::get_unixdir_state(env, statep);

	if (state == unixdir_state::COMMITTED) {
		//
		// Return the saved return values
		// from the original (failed) primary.
		//
		get_fsp()->unlock_renames();
		if (statep->get_error() != 0) {
			pxfslib::throw_exception(env, statep->get_error());
		}
		return;

	}
	attr_lock.wrlock();

	if (state == unixdir_state::CHECKPOINTED) {
		//
		// Regardless of whether this is a local invocation now,
		// we have to commit the transaction.
		//
		need_transaction = true;

	} else if (need_transaction) {
		//
		// Checkpoint the existence of the file.
		//
		unixdir_state::do_ckpt_entry(this, sourcenm, credp, env);

		//
		// The only possible exception is a software
		// version mismatch exception.
		//
		ASSERT(!env.exception());
	}

	//
	// We need to acquire the write lock for the attributes of
	// both the source and destination directories since
	// VOP_RENAME() will change the contents of both directories.
	// We also must not lock the destination directory a second time
	// if the destination directory is the same as the source directory.
	//
	if (dest_unixdir_iip != this) {
		dest_unixdir_iip->attr_lock.wrlock();
	}

	// Invalidate attribute caches for the source directory.
	(void) downgrade_attr_all(PXFS_VER::attr_write, false, 0);

	// Invalidate attribute caches for the destination directory.
	if (dest_unixdir_iip != this) {
		(void) dest_unixdir_iip->
		    downgrade_attr_all(PXFS_VER::attr_write, false, 0);
	}

	//
	// Check to see if the new name exists in the target directory
	// to check that it isn't a mount point.
	// Note that the underlying file system will make a similar
	// check but since its vnode isn't linked into the name space
	// (only the PXFS proxy vnode is), the test isn't valid.
	//
	bool			doing_dir = false;
	PXFS_VER::pvnode	targetpv;
	vnode_t			*targetvp;
#if	SOL_VERSION >= __s11
	error = VOP_LOOKUP(dest_unixdir_iip->get_vp(), (char *)targetnm,
	    &targetvp, NULL, 0, NULL, credp, NULL, NULL, NULL);
#else
	error = VOP_LOOKUP(dest_unixdir_iip->get_vp(), (char *)targetnm,
	    &targetvp, NULL, 0, NULL, credp);
#endif
	if (error == 0) {
		if (targetvp->v_type == VDIR) {
			//
			// Find the proxy vnode for this (underlying) vnode.
			// Note that this is one of the few places where
			// pxfs server depends on pxfs client.
			//
			targetpv.fileobj = get_fsp()->find_fobj(targetvp,
			    targetpv.fobjinfo, env);
			VN_RELE(targetvp);
			ASSERT(!CORBA::is_nil(targetpv.fileobj));
			targetpv.vfs = get_fsp()->get_fsref();
			targetvp = pxfobj::unpack_vnode(targetpv);
			ASSERT(targetvp != NULL);

			if (vn_vfswlock(targetvp)) {
				VN_RELE(targetvp);
				error = EBUSY;
				goto done;
			}
			if (vn_ismntpt(targetvp)) {
				vn_vfsunlock(targetvp);
				VN_RELE(targetvp);
				error = EBUSY;
				goto done;
			}
			doing_dir = true;
		} else {
			VN_RELE(targetvp);
		}
	}
	//
	// Ignore errors from the lookup operation.
	// If there is no file, then the file cannot be busy.
	//

	//
	// We lock out lookups so that we return consistent results if
	// directory name caching is enabled.
	// Since we already have the attribute lock on both directories,
	// there shouldn't be any deadlock possible on the directory lock.
	// This lock does not need to be checkpointed in the HA case since
	// it is only used to order events on the primary and invalidations
	// are idempotent.
	//
	dir_lock.wrlock();
	if (dest_unixdir_iip != this) {
		dest_unixdir_iip->dir_lock.wrlock();
	}
	if (!CORBA::is_nil(sourceobj)) {
		src_iip = (unixdir_ii *)fobj_ii::get_fobj_ii(sourceobj);
		//
		// Ensure that the directory being renamed is primary ready
		//
		if (is_replicated()) {
			(void) src_iip->is_dead(env);
			if (env.exception()) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_UNIXDIR,
				    PXFS_RED,
				    ("rename_fobj(%p) is_dead src %p error\n",
				    this, src_iip));
				env.clear();

				if (doing_dir) {
					// Release mount lock.
					vn_vfsunlock(targetvp);
					VN_RELE(targetvp);
				}

				error = EIO;
				goto done;
			}
		}
		if (src_iip != this && src_iip != dest_unixdir_iip) {
			src_iip->attr_lock.wrlock();
			src_iip->dir_lock.wrlock();
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_RENAME_S_B, FaultFunctions::generic);

	if (dest_unixdir_iip->get_delete_num() != 0) {
		//
		// The destination directory has been removed so don't
		// rename file to that directory.
		//
		error = ENOENT;
	} else {
#if	SOL_VERSION >= __s11
		error = VOP_RENAME(get_vp(), (char *)sourcenm,
		    dest_unixdir_iip->get_vp(), (char *)targetnm, credp,
		    NULL, 0);
#else
		error = VOP_RENAME(get_vp(), (char *)sourcenm,
		    dest_unixdir_iip->get_vp(), (char *)targetnm, credp);
#endif
		if (need_transaction) {
			if (error == 0) {
				error = get_fsp()->get_fs_dep_implp()->
				    fs_fsync(get_vp(), credp);
			}
			if (error == 0 && doing_dir) {
				error = get_fsp()->get_fs_dep_implp()->
				    fs_fsync(targetvp, credp);
			}
		} else {
			mod_time = os::gethrtime();
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_RENAME_S_A, FaultFunctions::generic);

	if (doing_dir) {
		// Release mount lock.
		vn_vfsunlock(targetvp);
		VN_RELE(targetvp);
	}

	if (statep != NULL && statep->server_did_op(error, false)) {
		//
		// Saved state says file did exist before the original
		// primary tried to rename it, but the VOP_RENAME above says
		// the source does not exist. So the earlier rename must have
		// succeeded.  We return success.
		//
		error = 0;
	}

	//
	// Invalidate any cached entries while we are still holding the
	// attribute lock on the directory so client caches are kept
	// consistent. We do this here as a call back to simplify the
	// HA code. The invalidate must be idempotent for this to work
	// (we don't checkpoint the invalidate or do extra HA locking).
	// Note that we only need to do the invalidate if the directory
	// contents are changed.
	//
	if (error == 0) {
		nodeid_t	ndid = env.get_src_node().ndid;

		// Invalidate source name in this directory.
		inval_name(ndid, sourcenm);

		//
		// Show that the destination directory
		// can have cached directory info.
		// This is safe even if we have already done this.
		//
		dest_unixdir_iip->dir_token_lock.lock();
		dest_unixdir_iip->cached_dir_info.add_node(ndid);
		dest_unixdir_iip->dir_token_lock.unlock();

		// Invalidate target name in destination directory.
		dest_unixdir_iip->inval_name(0, targetnm);

		//
		// Invalidate the ".." entry in the renamed directory
		// when the renamed directory winds up in a different
		// directory.
		//
		if (src_iip != NULL && dest_unixdir_iip != this) {
			src_iip->inval_name(0, "..");
		}
	}

	if (src_iip != NULL &&
	    src_iip != this &&
	    src_iip != dest_unixdir_iip) {
		src_iip->dir_lock.unlock();
		src_iip->attr_lock.unlock();
	}
	if (dest_unixdir_iip != this) {
		dest_unixdir_iip->dir_lock.unlock();
	}
	dir_lock.unlock();

done:
	attr_lock.unlock();

	get_fsp()->unlock_renames();

	if (dest_unixdir_iip != this) {
		dest_unixdir_iip->attr_lock.unlock();
	}

	if (need_transaction && is_replicated()) {
		//
		// Checkpoint (commit) the return error.
		//
		if (error == 0) {
			// Optimized commit checkpoint.
			commit(env);
		} else {
			get_ckpt()->ckpt_error_return(error, env);
		}
		env.clear();
	}
	FAULTPT_PXFS(FAULTNUM_PXFS_RENAME_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(env, error);
	}
}

//
// Convert an fobj from secondary to spare and delete ourself.
//
void
unixdir_ii::convert_to_spare()
{
	fobj_ii::convert_to_spare();
}

void
unixdir_ii::acquire_token_locks()
{
	dir_token_lock.lock();

	fobjplus_ii::acquire_token_locks();
}

void
unixdir_ii::release_token_locks()
{
	fobjplus_ii::release_token_locks();

	dir_token_lock.unlock();
}

//
// remove_client_privileges - the client died. So remove any privileges.
// The client may not have any privileges.
//
void
unixdir_ii::remove_client_privileges(nodeid_t node_id)
{
	ASSERT(dir_token_lock.lock_held());

	cached_dir_info.remove_node(node_id);

	fobjplus_ii::remove_client_privileges(node_id);
}

//
// Helper function for dumping state to a new secondary.
// This is called on the primary only.
//
void
unixdir_ii::dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
    PXFS_VER::fobj_ptr fobjp,
    Environment &env)
{
	fobj_ii::dump_state(ckptp, fobjp, env);
}

//
// Check to see if the directory is empty or not.
//
bool
unixdir_ii::is_empty()
{
	int	eof;
	uio_t	auio;
	iovec_t	iov;

	const size_t maxlen = 1024;
	char *rawdirp = new char [maxlen];

	auio.uio_iov = &iov;
	auio.uio_iovcnt = 1;
	auio.uio_loffset = 0;
	auio.uio_segflg = UIO_SYSSPACE;
	auio.uio_llimit = (rlim64_t)RLIM_INFINITY;
	auio.uio_fmode = FREAD;
again:
	auio.uio_resid = maxlen;
	iov.iov_base = rawdirp;
	iov.iov_len = maxlen;

	//
	// XXX We use kcred since directory permissions might allow
	// removal but not reading. This won't work for NFS.
	//
#if SOL_VERSION >= __s10
	VOP_RWLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWLOCK(get_vp(), 0);
#endif
#if	SOL_VERSION >= __s11
	int error = VOP_READDIR(get_vp(), &auio, kcred, &eof, NULL, 0);
#else
	int error = VOP_READDIR(get_vp(), &auio, kcred, &eof);
#endif
#if SOL_VERSION >= __s10
	VOP_RWUNLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWUNLOCK(get_vp(), 0);
#endif
	if (error != 0) {
		// XXX can't tell if it is empty.
		delete [] rawdirp;
		return (false);
	}

	size_t		dir_count = maxlen - (size_t)auio.uio_resid;
	struct dirent64 *dp = (struct dirent64 *)rawdirp;
	while (dir_count >= sizeof (struct dirent64)) {
		//lint -e415 -e416
		if (dp->d_name[0] == '.' &&
		    (dp->d_name[1] == '\0' ||
		    (dp->d_name[1] == '.' && dp->d_name[2] == '\0'))) {
			//lint +e415 +e416
			ASSERT(dir_count >= dp->d_reclen);
			dir_count -= dp->d_reclen;
		} else {
			delete [] rawdirp;
			return (false);
		}
		dp = (struct dirent64 *)((char *)dp + dp->d_reclen);
	}
	if (!eof && maxlen != (size_t)auio.uio_resid) {
		goto again;
	}
	delete [] rawdirp;
	return (true);
}
//
// Downgrade all client-side directory caches (i.e., purge them).
//
void
unixdir_ii::downgrade_dir_all()
{
	ASSERT(dir_lock.lock_held());

	Environment	e;
	bool		dead_clients = false;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// skip this dead client
			//
			dead_clients = true;
			continue;
		}

		if (!cached_dir_info.contains(entryp->node.ndid)) {
			continue;
		}

		//
		// The client could go away when we drop the list_lock.
		// So we use a smart pointer to ensure that the client
		// remains in existence.
		//
		PXFS_VER::fobj_client_var	client_v =
		    PXFS_VER::fobj_client::_duplicate(
		    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		// Invalidate directory information on the client
		client_v->dir_inval_all(e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			entryp->clear();
			dead_clients = true;
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}
}

//
// inval_name - Tell clients other than the requesting client
// to invalidate a name lookup entry.
//
// Note: this routine must be idempotent for the HA locking scheme to
// work correctly.
//
void
unixdir_ii::inval_name(nodeid_t skip_client_node, const char *name)
{
	ASSERT(dir_lock.lock_held());

	Environment	e;
	bool		dead_clients = false;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// skip this dead client
			//
			dead_clients = true;
			continue;
		}

		if (entryp->node.ndid == skip_client_node) {
			continue;
		}

		if (!cached_dir_info.contains(entryp->node.ndid)) {
			continue;
		}

		//
		// The client could go away when we drop the list_lock.
		// So we use a smart pointer to ensure that the client
		// remains in existence.
		//
		PXFS_VER::fobj_client_var	client_v =
		    PXFS_VER::fobj_client::_duplicate(
		    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		// Invalidate directory information on the client
		client_v->dir_inval_entry(name, e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			entryp->clear();
			dead_clients = true;
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}
}

//
// unixdir_norm_impl methods
//

handler *
unixdir_norm_impl::get_handler()
{
	return (_handler());
}

// Return a new fobj CORBA reference to this
PXFS_VER::fobj_ptr
unixdir_norm_impl::get_fobjref()
{
	return (get_objref());
}

// Return a new unixdir CORBA reference to this
PXFS_VER::unixdir_ptr
unixdir_norm_impl::get_unixdirref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
REPL_PXFS_VER::fs_replica_ptr
unixdir_norm_impl::get_ckpt()
{
	ASSERT(0);
	return (REPL_PXFS_VER::fs_replica::_nil());
}

//
// Add a commit checkpoint.
//
void
unixdir_norm_impl::commit(Environment &)
{
}

//
// Interface operations from unixdir
//

/* fobj */

PXFS_VER::fobj_type_t
unixdir_norm_impl::get_fobj_type(Environment &_environment)
{
	return (unixdir_ii::get_fobj_type(_environment));
}

void
unixdir_norm_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
unixdir_norm_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
unixdir_norm_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::access(accmode, accflags, credobj, _environment);
}

void
unixdir_norm_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
unixdir_norm_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::get_secattributes(sattr, secattrflag, credobj,
	    _environment);
} //lint !e1746

void
unixdir_norm_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::set_secattributes(sattr, secattrflag, credobj,
	    _environment);
}

void
unixdir_norm_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::pathconf(cmd, result, credobj, _environment);
}

void
unixdir_norm_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	unixdir_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
unixdir_norm_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result,
	    credobj, _environment);
}

void
unixdir_norm_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	unixdir_ii::frlock(cmd, lock_info, flag, off, credobj, cb,
	    _environment);
}

void
unixdir_norm_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	unixdir_ii::frlock_cancel_request(cb, _environment);
}

void
unixdir_norm_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	unixdir_ii::frlock_execute_request(cb, _environment);
}

void
unixdir_norm_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
unixdir_norm_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::remove_locks(cpid, sysid, credobj, _environment);
}

void
unixdir_norm_impl::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	fobjplus_ii::cache_new_client(binfo, client1_p, client2_p,
	    _environment);
} //lint !e1746

void
unixdir_norm_impl::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	fobjplus_ii::cache_remove_client(client_p, _environment);
}

void
unixdir_norm_impl::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_get_all_attr(credobj, rights, attributes,
	    seqnum, server_incarn, _environment);
}

void
unixdir_norm_impl::cache_write_all_attr(sol::vattr_t &attributes,
    int32_t attrflags, bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_write_all_attr(attributes, attrflags, discard, sync,
	    credobj, server_incarn, _environment);
}

void
unixdir_norm_impl::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	fobjplus_ii::cache_attr_drop_token(server_incarn, _environment);
}

void
unixdir_norm_impl::cache_set_attributes(sol::vattr_t &wb_attributes,
    int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_set_attributes(wb_attributes, wb_attrflags,
	    attributes, attrflags, credobj, seqnum, server_incarn,
	    _environment);
}

void
unixdir_norm_impl::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	fobjplus_ii::cache_access(accmode, accflags, credobj, allowed,
	    seqnum, _environment);
}

/* unixdir */

void
unixdir_norm_impl::lookup(const char *nm, PXFS_VER::fobj_out fobj_o,
    PXFS_VER::fobj_info &fobjinfo, PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::lookup(nm, fobj_o, fobjinfo, binfo,
	    client1_p, client2_p, credobj, _environment);
} //lint !e1746

void
unixdir_norm_impl::cascaded_create_fobj(const char *nm,
    const sol::vattr_t &attr, sol::vcexcl_t exclflag, int32_t mode,
    PXFS_VER::fobj_out fobj_o,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, int32_t flag, Environment &_environment)
{
	unixdir_ii::create_fobj(nm, attr, exclflag, mode, fobj_o, fobjinfo,
	    binfo, client1_p, client2_p, credobj, flag, _environment);
} //lint !e1746

void
unixdir_norm_impl::remove_fobj(const char *nm,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::remove_fobj(nm, credobj, _environment);
}

void
unixdir_norm_impl::create_symlink(const char *nm, const sol::vattr_t &attr,
    const char *targetpath, solobj::cred_ptr credobj,
    Environment &_environment)
{
	unixdir_ii::create_symlink(nm, attr, targetpath, credobj,
	    _environment);
}

void
unixdir_norm_impl::rename_fobj(const char *sourcenm,
    PXFS_VER::unixdir_ptr target_dir, const char *targetnm,
    solobj::cred_ptr credobj,  PXFS_VER::unixdir_ptr sourceobj,
    Environment &_environment)
{
	unixdir_ii::rename_fobj(sourcenm, target_dir, targetnm, credobj,
	    sourceobj, _environment);
}

void
unixdir_norm_impl::link_fobj(PXFS_VER::fobj_ptr fobj_p, const char *targetnm,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::link_fobj(fobj_p, targetnm, credobj, _environment);
}

void
unixdir_norm_impl::create_dir(const char *dirnm, const sol::vattr_t &attr,
    PXFS_VER::fobj_out newdir,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::create_dir(dirnm, attr, newdir, fobjinfo,
	    binfo, client1_p, client2_p, credobj, _environment);
} //lint !e1746

void
unixdir_norm_impl::remove_dir(const char *dirnm, PXFS_VER::unixdir_ptr cur_dir,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::remove_dir(dirnm, cur_dir, credobj, _environment);
}

void
unixdir_norm_impl::readdir(PXFS_VER::unixdir::diroff_t &offset,
    sol::size_t maxlen,
    PXFS_VER::unixdir::direntrylist_t_out entries, bool &eof_flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::readdir(offset, maxlen, entries, eof_flag, credobj,
	    _environment);
} //lint !e1746

void
unixdir_norm_impl::readdir_raw(PXFS_VER::unixdir::diroff_t &offset,
    sol::size_t maxlen, PXFS_VER::unixdir::rawdir_t_out rawdir, bool &eof_flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	unixdir_ii::readdir_raw(offset, maxlen, rawdir, eof_flag, credobj,
	    _environment);
} //lint !e1746

//
// unixdir_repl_impl methods
//

handler *
unixdir_repl_impl::get_handler()
{
	return (_handler());
}

// Return a new fobj CORBA reference to this
PXFS_VER::fobj_ptr
unixdir_repl_impl::get_fobjref()
{
	return (get_objref());
}

// Return a new unixdir CORBA reference to this
PXFS_VER::unixdir_ptr
unixdir_repl_impl::get_unixdirref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
REPL_PXFS_VER::fs_replica_ptr
unixdir_repl_impl::get_ckpt()
{
	return ((repl_pxfs_server*)(get_provider()))->
	    get_checkpoint_fs_replica();
}

//
// Add a commit checkpoint.
//
void
unixdir_repl_impl::commit(Environment &e)
{
	add_commit(e);
}

//
// Interface operations from unixdir
//

/* fobj */

PXFS_VER::fobj_type_t
unixdir_repl_impl::get_fobj_type(Environment &_environment)
{
	if (is_dead(_environment)) {
		return (PXFS_VER::fobj_fobj);
	}
	return (unixdir_ii::get_fobj_type(_environment));
}

void
unixdir_repl_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
unixdir_repl_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
unixdir_repl_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::access(accmode, accflags, credobj, _environment);
}

void
unixdir_repl_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
unixdir_repl_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::get_secattributes(sattr, secattrflag, credobj,
	    _environment);
} //lint !e1746

void
unixdir_repl_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::set_secattributes(sattr, secattrflag, credobj,
	    _environment);
}

void
unixdir_repl_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::pathconf(cmd, result, credobj, _environment);
}

void
unixdir_repl_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
unixdir_repl_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result,
	    credobj, _environment);
}

void
unixdir_repl_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::frlock(cmd, lock_info, flag, off, credobj, cb,
	    _environment);
}

void
unixdir_repl_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::frlock_cancel_request(cb, _environment);
}

void
unixdir_repl_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::frlock_execute_request(cb, _environment);
}

void
unixdir_repl_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
unixdir_repl_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::remove_locks(cpid, sysid, credobj, _environment);
}

void
unixdir_repl_impl::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_new_client(binfo, client1_p, client2_p,
	    _environment);
} //lint !e1746

void
unixdir_repl_impl::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_remove_client(client_p, _environment);
}

void
unixdir_repl_impl::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	//
	// cache_get_all_attr can trigger a third level invocation for a
	// cache downgrade. Check with the fs server whether we can proceed
	// with this invocation. If we can, the active invocation count
	// will be incremented by one. We must decrement active invocation
	// count after the invocation completes.
	//
	if (is_dead(_environment) ||
	    get_fsp()->freeze_prepare_in_progress(_environment)) {
		return;
	}
	fobjplus_ii::cache_get_all_attr(credobj, rights, attributes,
	    seqnum, server_incarn, _environment);

	//
	// Decrement outstanding invocation count and wakeup any
	// threads waiting for invocation count to become zero.
	//
	get_fsp()->decrement_invo_count();
}

void
unixdir_repl_impl::cache_write_all_attr(sol::vattr_t &attributes,
    int32_t attrflags, bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_write_all_attr(attributes, attrflags, discard, sync,
	    credobj, server_incarn, _environment);
}

void
unixdir_repl_impl::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_attr_drop_token(server_incarn, _environment);
}

void
unixdir_repl_impl::cache_set_attributes(sol::vattr_t &wb_attributes,
    int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_set_attributes(wb_attributes, wb_attrflags,
	    attributes, attrflags, credobj, seqnum, server_incarn,
	    _environment);
}

void
unixdir_repl_impl::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_access(accmode, accflags, credobj, allowed,
	    seqnum, _environment);
}

/* unixdir */

void
unixdir_repl_impl::lookup(const char *nm, PXFS_VER::fobj_out fobj_o,
    PXFS_VER::fobj_info &fobjinfo, PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	unixdir_ii::lookup(nm, fobj_o, fobjinfo, binfo,
	    client1_p, client2_p, credobj, _environment);
} //lint !e1746

void
unixdir_repl_impl::cascaded_create_fobj(const char *nm,
    const sol::vattr_t &attr, sol::vcexcl_t exclflag, int32_t mode,
    PXFS_VER::fobj_out fobj_o,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, int32_t flag, Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	unixdir_ii::create_fobj(nm, attr, exclflag, mode, fobj_o, fobjinfo,
	    binfo, client1_p, client2_p, credobj, flag, _environment);
} //lint !e1746

//
// If the environment variable passed in indicates that this call is a
// retry and there is an uncommitted transaction state, then this method
// returns true, otherwise it returns false.
//
bool
unixdir_repl_impl::is_retry(Environment &env)
{
	primary_ctx *ctxp = primary_ctx::extract_from(env);

	ASSERT(ctxp != NULL);

	return ((ctxp->get_saved_state() == NULL) ? false : true);
}

void
unixdir_repl_impl::remove_fobj(const char *nm,
    solobj::cred_ptr credobj, Environment &_environment)
{
	bool	skip_freeze_prepare;

	//
	// remove_fobj can trigger a third level invocation due to
	// invalidation of the file object in the clients. Check with
	// the fs server whether we can proceed with this invocation.
	// If we can, the active invocation count will be incremented
	// by one. We must decrement active invocation count after the
	// invocation completes.
	//
	// A service freeze request waits for all uncommitted transactions
	// to drain before proceeding from freeze_primary_prepare() to the
	// freeze_primary() state. Consider the case where the current
	// primary did a check-point and died before the invocation
	// returned to the client. The PxFS secondary for that file-system
	// has an uncommitted transaction now. That transaction will close
	// or drain only when the retry by the client completes.
	//
	// The PxFS client's retry will reach the new primary when the
	// repl_pxfs_server object's state is still 'FREEZING'. The retry
	// thread will now block till repl_pxfs_server's state changes to
	// 'FROZEN'. That will never happen since this retry thread has to
	// complete execution to decrement the uncommitted transaction
	// count. We have a deadlock.
	//
	// To prevent the deadlock we bypass freeze_prepare_in_progress()
	// check on threads doing a retry and has uncommitted state.
	//
	skip_freeze_prepare = is_retry(_environment);
	if (is_dead(_environment) ||
	    (!skip_freeze_prepare &&
	    get_fsp()->freeze_prepare_in_progress(_environment))) {
		return;
	}

	unixdir_ii::remove_fobj(nm, credobj, _environment);

	//
	// Decrement outstanding invocation count and wakeup any
	// threads waiting for invocation count to become zero if we
	// did not skip freeze_prepare_in_progress().
	//
	if (!skip_freeze_prepare) {
		get_fsp()->decrement_invo_count();
	}
}

void
unixdir_repl_impl::create_symlink(const char *nm, const sol::vattr_t &attr,
    const char *targetpath, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::create_symlink(nm, attr, targetpath, credobj,
	    _environment);
}

void
unixdir_repl_impl::rename_fobj(const char *sourcenm,
    PXFS_VER::unixdir_ptr target_dir, const char *targetnm,
    solobj::cred_ptr credobj,  PXFS_VER::unixdir_ptr sourceobj,
    Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	unixdir_ii::rename_fobj(sourcenm, target_dir, targetnm, credobj,
	    sourceobj, _environment);
}

void
unixdir_repl_impl::link_fobj(PXFS_VER::fobj_ptr fobj_p, const char *targetnm,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	unixdir_ii::link_fobj(fobj_p, targetnm, credobj, _environment);
}

void
unixdir_repl_impl::create_dir(const char *dirnm, const sol::vattr_t &attr,
    PXFS_VER::fobj_out newdir,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	unixdir_ii::create_dir(dirnm, attr, newdir, fobjinfo,
	    binfo, client1_p, client2_p, credobj, _environment);
} //lint !e1746

void
unixdir_repl_impl::remove_dir(const char *dirnm, PXFS_VER::unixdir_ptr cur_dir,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	unixdir_ii::remove_dir(dirnm, cur_dir, credobj, _environment);
}

void
unixdir_repl_impl::readdir(PXFS_VER::unixdir::diroff_t &offset,
    sol::size_t maxlen,
    PXFS_VER::unixdir::direntrylist_t_out entries, bool &eof_flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::readdir(offset, maxlen, entries, eof_flag, credobj,
	    _environment);
} //lint !e1746

void
unixdir_repl_impl::readdir_raw(PXFS_VER::unixdir::diroff_t &offset,
    sol::size_t maxlen, PXFS_VER::unixdir::rawdir_t_out rawdir, bool &eof_flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	unixdir_ii::readdir_raw(offset, maxlen, rawdir, eof_flag, credobj,
	    _environment);
} //lint !e1746
