//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)file_impl.cc	1.44	09/04/08 SMI"

#include <sys/thread.h>
#include <sys/pathname.h>
#include <sys/debug.h>
#include <sys/uio.h>
#include <sys/file.h>
#include <sys/filio.h>
#include <sys/errno.h>
#include <sys/fcntl.h>

#include <sys/mc_probe.h>
#include <sys/sol_version.h>
#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/server/client_set.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/fobj_trans_states.h>
#include <pxfs/server/async_io.h>
#include <pxfs/server/fs_dependent_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

extern "C" void vn_fsio_done(fdbuffer_t *fdb, void *ioarg, struct buf *bp);

#define	IGNORE_FILE_SIZE	(size_t)0

#define	VOP_RDWR_DATA	get_fsp()->get_fs_dep_implp()->fs_rdwr_data
#define	VOP_ALLOC_DATA	get_fsp()->get_fs_dep_implp()->fs_alloc_data
#define	FS_PREPROCESS   get_fsp()->get_fs_dep_implp()->fs_preprocess

//
// Unreplicated or primary constructor for the internal implementation.
//
file_ii::file_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
    fid_t *fidp) :
	fobjplus_ii(fsp, vp, ftype, fidp),
	cachedata_flag(true)
{
}

//
// Secondary constructor for the internal implementation.
//
file_ii::file_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t ftype) :
	fobjplus_ii(fsp, fobjid, ftype),
	cachedata_flag(true)
{
}

file_ii::~file_ii()
{
}

//
// Constructor for the unreplicated CORBA implementation.
//
file_norm_impl::file_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp) :
	file_ii(fsp, vp, PXFS_VER::fobj_file, fidp)
{
	set_fobj_ii(this);
}

//
// Constructor for the primary replicated CORBA implementation.
//
file_repl_impl::file_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp) :
	file_ii(fsp, vp, PXFS_VER::fobj_file, fidp),
	mc_replica_of<PXFS_VER::file>(fsp->get_serverp())
{
	set_fobj_ii(this);
}

//
// Constructor for the secondary replicated CORBA implementation.
//
file_repl_impl::file_repl_impl(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::file_ptr obj) :
	file_ii(fsp, fobjid, PXFS_VER::fobj_file),
	mc_replica_of<PXFS_VER::file>(obj)
{
	set_fobj_ii(this);
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we lock the
// appropriate bucket lock while checking for this case.
// This kind of file object is always the primary.
// If we win the race, then
// do what is needed to delete the object.
//
void
file_norm_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	if (!fobj_released) {
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	}
	if (_last_unref(arg)) {
		unreferenced(bktno, false);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we acquire the appropriate
// locks while checking for this case. If we win the race, then
// do what is needed to delete the object.
//
void
file_repl_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	bool	fidlist_locked = false;

	if (fobj_released) {
		// No locking needed because not on any list

	} else if (get_fsp()->is_secondary()) {
		//
		// A secondary keeps objects on the fid list.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();
	} else if (primary_ready) {
		//
		// The file object is a ready primary.
		//
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	} else {
		//
		// The file object has not become a primary yet.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();

		int		error;
		vnode_t		*vnodep = fid_to_vnode(error);
		if (vnodep != NULL) {
			//
			// Grab the bucket lock.
			// It is possible someone may have just made this file
			// object a ready primary. But this will work in
			// in either case.
			//
			bktno = fs_implp->vp_to_bucket(vnodep);
			fs_implp->fobjlist_locks[bktno].wrlock();

			// Drop the hold from fid_to_vnode
			VN_RELE(vnodep);
		}
	}

	if (_last_unref(arg)) {
		//
		// Note that unreference processing will drop
		// the appropriate locks.
		//
		unreferenced(bktno, fidlist_locked);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
	if (fidlist_locked) {
		fs_implp->fidlock.unlock();
	}
}

bool
file_ii::is_cached()
{
	return (cachedata_flag);
}

//
// Truncate file size on all clients.
//
int
file_ii::truncate(u_offset_t newsize, Environment &env)
{
	ASSERT(range_rwlock.write_held());

	// Delete pages past the new end of file from all attached caches.
	downgrade_all((sol::u_offset_t)newsize, DELETE_RANGE, 0, env);
	return (pxfslib::get_err(env));
}

//
// Truncate does not inherit from fobj!
//
void
file_ii::cascaded_truncate(sol::u_offset_t size, Environment &_environment)
{
	vattr_t vap;

	vap.va_mask = AT_SIZE;
	vap.va_size = size;
	int error = set_size(&vap, _environment);
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// set_size - change the size of the file.
// This is effectively a write operation.
// So hold the range_lock to prevent any other concurrent data changes.
//
int
file_ii::set_size(vattr_t *vap, Environment &env)
{
	ASSERT(vap->va_mask & AT_SIZE);

	range_lock();

	//
	// Change the file size and eliminate any pages past new end-of-file
	//
	int	error = truncate(vap->va_size, env);

	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("file_ii:set_size(%p) len %llx err %d\n",
		    this, vap->va_size, error));
		range_unlock();
		return (error);
	}

	attr_lock.wrlock();
	//
	// Truncate changes the file size, which is a file attribute.
	// So must invalidate all cached attributes.
	// Truncate differs from the write operation, in that the client
	// does not have to be the one holding write permissions
	// on the file attributes.
	//
	error = downgrade_attr_all(PXFS_VER::attr_write, false, 0);
	if (error == 0) {
		//
		// It is possible for a file's mode to be changed to read only
		// after it has been opened with write access. If a client then
		// tries to set the size of the file, the normal access
		// checking done in VOP_SETATTR would fail with EACCES,
		// although it should be legal for it to do so. To get around
		// this, we supply root's credentials (kcred) rather
		// than the client's credentials. Note that this relies upon
		// the calling routine(s) having checked that the file has
		// been opened with write access.
		//
		error = setattr(vap, 0, kcred);
	}
	attr_lock.unlock();

	if (error == 0) {
		//
		// If the attributes have not already been flushed to disk,
		// flush the attributes to storage.
		//
		error = get_fsp()->get_fs_dep_implp()->
		    sync_if_necessary(mod_time, get_vp(), kcred);
	}

	range_unlock();
	return (error);
}

//
// alloc_check_for_pageout
// -Checks whether allocation is needed for this [async]page_out.
// There is an optimization for appending writes to avoid invoking bmap()
// for allocation if the write falls within extended file range. In this case
// the client file size will not be in sync with the underlying file system.
// So, when the portion of the file which falls in the extended range is paged
// out, not everything will be written to the disk (the data beyond the
// server side file size is skipped). The problem happens only at the end of
// the file (because, that is where the extended file range optimization is
// applicable).
// This function checks whether we need to set (actually synchronize) the file
// size. It checks if the current range (offset, length) that is being paged
// out is beyond the EOF. If so, there is a need to sync the file size.
//
int
file_ii::alloc_check_for_pageout(sol::u_offset_t offset, sol::size_t length,
    sol::size_t client_file_size, cred_t *credp, bool &alloc_required)
{
	int error = 0;
	ASSERT(client_file_size >= offset);

	//
	// XXX - We need a clean way of detecting if a call to FS_PREPROCESS()
	//	is really needed. The past attempts to detect this based on
	//	the vattr.va_size from VOP_GETATTR() on the backing vnode
	//	was not robust enough. There are race cases between
	//	sync-attr and async_page_out() which lead to data-corruption.
	//
	//	Hence, at present we just return TRUE until we come up with a
	//	cleaner implementation.
	//
	alloc_required = true;

	return (error);
} //lint !e715

//
// Flush cached data pages and attributes from all clients except the
// specified node
//
int
file_ii::flush_caches(nodeid_t node_id, Environment &env)
{
	ASSERT(range_rwlock.write_held());
	downgrade_all((sol::u_offset_t)0, FLUSH_BACK, node_id, env);
	int	error = pxfslib::get_err(env);
	if (!error) {
		error = inval_all_attrs(node_id, env);
	}
	return (error);
}

//
// inval_all_attrs - Invaldidate all attributes from all nodes,
// except for the specified node.
//
int
file_ii::inval_all_attrs(nodeid_t node_id, Environment &env)
{
	ASSERT(range_rwlock.write_held());
	attr_lock.wrlock();

	int	error = downgrade_attr_all(PXFS_VER::attr_write, false,
		    node_id);

	attr_lock.unlock();
	if (error == 0) {
		error = get_fsp()->get_fs_dep_implp()->
		    sync_if_necessary(mod_time, get_vp(), kcred);
	}
	return (error);
}

//
// fsync - force information to stable storage
//
void
file_ii::fsync(int32_t syncflag, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (!(syncflag & FNODSYNC)) {
		//
		// Write back data from all attached caches.
		// First we lock all pages; there should be no
		// deadlock possible.
		//
		// Force write-back of dirty pages.
		//
		range_lock();
		downgrade_all((sol::u_offset_t)0, WRITE_BACK, 0, _environment);
		range_unlock();
	}

	cred_t	*credp = solobj_impl::conv(credobj);

	// Downgrade attributes to read-only to sync them.
	attr_lock.wrlock();
	int	error = downgrade_attr_all(PXFS_VER::attr_read, false, 0);
	attr_lock.unlock();
	if (error == 0) {
		if (syncflag == FNODSYNC) {
			error = get_fsp()->get_fs_dep_implp()->
			    sync_if_necessary(mod_time, get_vp(), credp);
		} else {
			//
			// Need to sync attributes. So sync everything.
			//
			error = get_fsp()->get_fs_dep_implp()->
			    do_fsync(get_vp(), credp);
		}
	}

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// convert_to_primary
// Convert fid to vnode.  This is to convert a secondary to become a new
// primary.  Note that fid and vnodep share a union.  This routine will either
// kmem_free fid and assign to vnodep, or it will return an error and
// leave fid unchanged. If an error is returned, the object should be left
// marked as a secondary.
//
int
file_ii::convert_to_primary()
{
	ASSERT(is_replicated());
	ASSERT(!primary_ready);

	if (back_obj.fidp == NULL) {
		// This is a "dead" object.
		return (EBADF);
	}

	struct vfs	*underlying_vfsp = get_fsp()->get_vfsp();
	if (underlying_vfsp == NULL) {
		//
		// become_primary() for this filesystem failed.
		// see repl_pxfs_server::become_primary(). It sets vfsp
		// to NULL if the underlying filesystem's mount fails.
		//
		return (EIO);
	}
	vnode_t		*vnodep;
	int error = VFS_VGET(underlying_vfsp, &vnodep, back_obj.fidp);
	if (error) {
		return (error);
	} else if (vnodep == NULL) {
		return (EINVAL);
	}
	back_obj.vnodep = vnodep;

	recover_client_file_state(true);

	primary_ready = true;

	if (!cachedata_flag) {
		//
		// Replay the directio ioctl.
		//
		int	result;
		fs_implp->get_fs_dep_implp()->replay_ioctl(this, _FIODIRECTIO,
		    (sol::intptr_t)DIRECTIO_ON, 0, result, error);
	}

	if (fr_lockp != NULL) {
		fr_lockp->replay_active_locks();
	}
	return (0);
}

//
// dump_state
// Helper function for dumping state to a new secondary.
// This is called on the primary only.
//
void
file_ii::dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
    PXFS_VER::fobj_ptr fobjp, Environment &env)
{
	if (!cachedata_flag) {
		//
		// The secondary assumes that page caching is enabled.
		// So only need to cache when page caching is not enabled.
		//
		//
		// Its safe to get a reference to ourself without locking
		// since we are in the middle of an IDL call and
		// _unrefereneced() won't be delivered until after we return.
		//
		PXFS_VER::file_var	file_v = get_fileref();
		get_ckpt()->ckpt_cachedata_flag(file_v, cachedata_flag, env);
		env.clear();
	}
	fobj_ii::dump_state(ckptp, fobjp, env);
}

void
file_ii::acquire_token_locks()
{
	token_state_lock.lock();

	fobjplus_ii::acquire_token_locks();
}

void
file_ii::release_token_locks()
{
	fobjplus_ii::release_token_locks();

	token_state_lock.unlock();
}

//
// remove_client_privileges - the client died. So remove any privileges.
// The client may not have any privileges.
//
void
file_ii::remove_client_privileges(nodeid_t node_id)
{
	ASSERT(token_state_lock.lock_held());

	read_token.remove_node(node_id);

	//
	// Before removing write privilege, drain pending requests from
	// this client by taking the content lock as a writer. Any thread
	// which has reached page_out would either have a read lock and
	// thus will finish the pageout before we proceed or that request
	// will become an orphan.
	//
	content_rwlock.wrlock();
	write_token.remove_node(node_id);
	content_rwlock.unlock();

	fobjplus_ii::remove_client_privileges(node_id);
}

// virtual method
void
file_ii::range_lock()
{
	range_rwlock.wrlock();
}

// virtual method
void
file_ii::range_unlock()
{
	range_rwlock.unlock();
}

//
// page_in - Request the pages in the specified range.
// Pages are returned through the interface in 'pglobj'.
//
// If the request would result in a deadlock,
// returns EDEADLK.
//
void
file_ii::page_in(sol::u_offset_t offset, sol::size_t length,
    PXFS_VER::acc_rights_t access_rights,
    bulkio::inout_pages_ptr &pglobj, bulkio::file_hole_list_out holes,
    solobj::cred_ptr credobj, Environment &_environment)
{
	MC_PROBE_0(file_ii_page_in_start, "clustering pxfs", "");

	cred_t		*credp = solobj_impl::conv(credobj);
	fdbuffer_t	*fdb = bulkio_impl::conv(pglobj);

	MC_PROBE_0(file_ii_page_in_conv, "clustering pxfs", "");

	int	error;
	size_t	len = length;

	nodeid_t	node_id = _environment.get_src_node().ndid;

	//
	// This could be an orphan, if so we throw an
	// exception and ignore the request
	//
	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("page_in(%p) orphan node %d\n", this, node_id));
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	CL_PANIC(read_token.contains(node_id));

	FAULTPT_PXFS(FAULTNUM_PXFS_PAGEIN_S_B, FaultFunctions::generic);

	MC_PROBE_0(file_ii_page_in_stats, "clustering pxfs", "");

	//
	// Extending file size via setattr can create pages mapping the
	// extended size if a UFS fragment was promoted. Those pages will
	// be written to disk and destroyed by setattr(). VOP_RDWR_DATA()
	// bypasses the page cache. VOP_RDWR_DATA(read) can read bad data
	// from disk if the page created by a setattr has not yet been
	// written to disk.
	//
	// To fix the above corruption we wait for attribute changes to
	// complete by requesting the attribute lock as a reader.
	//
	attr_lock.rdlock();

	//
	// Don't call VOP_ALLOC_DATA() for fastwrites.
	// Disk block allocation happens only during a pageout.
	//
	if ((access_rights == PXFS_VER::acc_rw) &&
	    !(get_fsp()->fastwrite_enabled())) {
		CL_PANIC(write_token.contains(node_id));
		error = VOP_ALLOC_DATA(get_vp(), offset, &len, fdb,
		    B_READ, credp);
	} else {
		error = VOP_RDWR_DATA(get_vp(), offset, len,
		    IGNORE_FILE_SIZE, fdb, B_READ, credp);
	}

	// Allow attribute changes.
	attr_lock.unlock();

	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("page_in(%p) error %d\n",
		    this, error));

		_environment.exception(new sol::op_e(error));
		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_PAGEIN_S_A, FaultFunctions::generic);

	MC_PROBE_0(file_ii_page_in_vop, "clustering pxfs", "");

	bulkio::file_hole_list	*fhl = new bulkio::file_hole_list;

	MC_PROBE_0(file_ii_page_in_new, "clustering pxfs", "");

	holes = fhl;

	fdb_holes_t	*fdh;
	fdb_holes_t	*fdh1;

	if ((fdh = fdb_get_holes(fdb)) != NULL) {

		// Count the holes.
		uint_t	cnt = 0;

		fdh = fdb_get_holes(fdb);
		ASSERT(fdh);

		for (fdh1 = fdh; fdh1; fdh1 = fdh1->next_hole) {
			cnt++;
		}
		fhl->length(cnt);
		bulkio::file_hole	*fdh_list = fhl->buffer();
		int			i = 0;
		for (fdh1 = fdh; fdh1; fdh1 = fdh1->next_hole, i++) {
			fdh_list[i].off = fdh1->off + offset;
			fdh_list[i].len = fdh1->len;
		}
	}

	MC_PROBE_0(file_ii_page_in_end, "clustering pxfs", "");
}

//
// async_page_in - the client is asynchronously requesting pages
//
void
file_ii::async_page_in(pxfs_aio::aio_callback_ptr aiock,
    sol::u_offset_t offset, sol::size_t length,
    PXFS_VER::acc_rights_t access_rights,
    bulkio::in_aio_pages_ptr pglobj,
    solobj::cred_ptr credobj, Environment &_environment)
{
	MC_PROBE_0(file_ii_async_page_in_start, "clustering pxfs", "");

	cred_t		*credp = solobj_impl::conv(credobj);
	fdbuffer_t	*fdb = bulkio_impl::conv(pglobj);
	int		error;
	size_t		len = length;

	nodeid_t	node_id = _environment.get_src_node().ndid;

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("async_page_in(%p) orphan node %d\n", this, node_id));
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	CL_PANIC(read_token.contains(node_id));

	MC_PROBE_0(file_ii_async_page_in_stats, "clustering pxfs", "");

	io_async	*ios = new io_async(pglobj, aiock, fdb, offset);

	MC_PROBE_0(file_ii_async_page_in_new, "clustering pxfs", "");

	fdb_set_iofunc(fdb, vn_fsio_done, (void *)ios, 0);

	MC_PROBE_0(file_ii_async_page_in_iofunc, "clustering pxfs", "");

	FAULTPT_PXFS(FAULTNUM_PXFS_ASYNC_PAGEIN_S_B, FaultFunctions::generic);

	// This routine is never called with RW privileges.
	ASSERT(access_rights != PXFS_VER::acc_rw);

	//
	// Extending file size via setattr can create pages mapping the
	// extended size if a UFS fragment was promoted. Those pages will
	// be written to disk and destroyed by setattr(). VOP_RDWR_DATA()
	// bypasses the page cache. VOP_RDWR_DATA(read) can read bad data
	// from disk if the page created by a setattr has not yet been
	// written to disk.
	//
	// To fix the above corruption we wait for attribute changes to
	// complete by requesting the attribute lock as a reader.
	//
	attr_lock.rdlock();

	error = VOP_RDWR_DATA(get_vp(), offset, length,
		    IGNORE_FILE_SIZE, fdb, B_READ | B_ASYNC, credp);

	MC_PROBE_0(file_ii_async_page_in_vop, "clustering pxfs", "");

	FAULTPT_PXFS(FAULTNUM_PXFS_ASYNC_PAGEIN_S_A, FaultFunctions::generic);

	// Allow attribute changes.
	attr_lock.unlock();

	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("async_page_in(%p) error %d (%p)\n",
		    this, error, fdb->fd_parentbp));
		//
		// The ios object is deleted via the async callback from
		// VOP_RDWR_DATA(). No need to delete it here.
		//
		_environment.exception(new sol::op_e(error));
	}
	MC_PROBE_0(file_ii_async_page_in_end, "clustering pxfs", "");
}

//
// reset_mtime
// When fastwrites is enabled enabled the disk-block allocation happens
// during [async_]page_out() as it calls FS_PREPROCESS() and that modifies
// the underlying inode and hence MTIME is updated. This wipes out any
// MTIME modifications that were done after the write(2) but, before the
// pages were flushed.
//
// Hence, this routine restores the original mtime if the cached dirty_mtime
// is the same as the timestamp passed by the [async_]page_out().
//
// Return value : If we updated the mtime we return a true.
//
bool
file_ii::reset_mtime(int64_t timestamp, cred_t *credp,
Environment &_environment)
{
	bool ret = false;

	attr_lock.wrlock();

	//
	// If these pages have the same mtime as the dirty_time
	// then, we need to restore the inode's mtime back to that
	// mtime. Because, the FS_PREPROCESS() would have
	// set the mtime to current time which is incorrect.
	//
	// We need to set the attributes in these conditions :
	// - If these pages have the same mtime as the dirty_time.
	//   ie. there has been no other attribute change on client
	//   and we need to retain the client's mtime.
	// - (dirty_mtime == -1) ie. this is probably a new primary
	//   (after a failover) hence,
	//
	if ((dirty_mtime == timestamp) || (dirty_mtime == -1)) {
		vattr attr;
		int32_t attrflags = AT_MTIME;
		int error;

		ret = true;

		attr.va_mtime.tv_sec = (time_t)timestamp / NANOSEC;
		attr.va_mtime.tv_nsec = (long)timestamp % NANOSEC;
		attr.va_uid = 0;
		attr.va_mask = AT_MTIME;

		error = setattr((vattr *)&attr, attrflags, credp);

		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_RED,
			    ("page_out(%p) VOP_SETATTR error %d\n",
			    this, error));
			_environment.exception(new sol::op_e(error));
		}
	}
	attr_lock.unlock();

	return (ret);
}

//
// page_out
// Write modified pages in the specified range. The cache
// gives up its caching rights for the pages (i.e. it makes
// the pages inaccessible during page_out and removes them
// after page_out completes).
//
// If the request would result in a deadlock,
// returns EDEADLK.
//
void
file_ii::page_out(sol::u_offset_t offset, sol::size_t length,
    sol::size_t client_file_size, int32_t flags,
    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    int64_t timestamp, bool, Environment &_environment)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	fdbuffer_t	*fdb = bulkio_impl::conv(pglobj);
	os::hrtime_t	alloc_time = (os::hrtime_t)timestamp;

#ifdef DEBUG
	if (!get_fsp()->get_recovery_mode() &&
	    !get_fsp()->fastwrite_enabled() &&
	    // Do the check for UFS filesystems only
	    (get_fsp()->get_vfsp() != NULL &&
	    get_fsp()->get_vfsp()->vfs_fsid.val[1] == 2)) {
		os::hrtime_t now = os::gethrtime();

		//
		// If we are doing normal writes and are not in recovery
		// mode, bmap timestamp should not be newer than current
		// time and it should not be very old either. We quantify
		// very old with an arbitrary value of 8 hours.
		//
		if (now < timestamp || (now - timestamp) > 28800000000000L) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_AMBER,
			    ("page_out(%p) ts mismatch bmap %llx now %llx\n",
			    this, timestamp, now));
		}
	}
#endif

	// Check if server-pull failed.
	if (!fdb) {
		_environment.exception(new sol::op_e(EIO));
		return;
	}

	//
	// This is a synchronous version of pageout.
	// B_ASYNC flag should NOT be passed in. Assertion
	// will catch this.
	//
	ASSERT(!(flags & B_ASYNC));

	//
	// Extending file size via setattr can create pages mapping the
	// extended size if a UFS fragment was promoted. Those pages will
	// be written to disk and destroyed by setattr(). VOP_RDWR_DATA()
	// bypasses the page cache. VOP_RDWR_DATA(write) could write data
	// to disk and then pages created by a setattr could be flushed
	// corrupting on-disk data.
	//
	// To fix the above corruption we wait for attribute changes to
	// complete by requesting the attribute lock as a reader.
	//
	attr_lock.rdlock();

	// stop other node(s) from changing data permissions.
	content_rwlock.rdlock();

	nodeid_t	node_id = _environment.get_src_node().ndid;

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("page_out(%p) orphan node %d\n", this, node_id));
		pxfslib::throw_exception(_environment, EIO);
		content_rwlock.unlock();

		// Allow attribute changes.
		attr_lock.unlock();
		return;
	}

	CL_PANIC(read_token.contains(node_id));
	CL_PANIC(write_token.contains(node_id));

	int error = 0;
	bool alloc_required = false;

	//
	// Allocate blocks if necessary.
	// We allocate in the following cases :
	// - If we are in recovery mode.
	//   There could have been a failover. The space allocation may have
	//   been lost. This is to support mounting UFS without syncdir.
	// - If this write had used the extended offset on the client side.
	//   Refer to pxreg::alloc_blocks() for details. This is an
	//   optimization for small-writes (writes < block-size).
	// - Fastwrites is enabled. With fastwrite enabled, the blocks are
	//   just allocated from the pool. However, the underlying filesystem
	//   still is not aware of this. Hence we need to call FS_PREPROCESS()
	//   to allocate the blocks.
	//
	if (get_fsp()->get_recovery_mode() || get_fsp()->fastwrite_enabled()) {
		alloc_required = true;
	} else {
		error = alloc_check_for_pageout(offset, length,
		    client_file_size, credp, alloc_required);
		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_RED, ("page_out(%p) alloc_check_for_pageout "
			    "error %d\n",
			    this, error));
			_environment.exception(new sol::op_e(error));
			content_rwlock.unlock();

			// Allow attribute changes.
			attr_lock.unlock();
			return;
		}
	}

	if (alloc_required) {
		// Truncate allocation request to actual filesize.
		size_t  alloc_len;
		if (offset + length > client_file_size) {
			//
			// Shorten the allocation to the file size known
			// by the client
			//
			alloc_len = (size_t)(client_file_size - offset);
		} else {
			alloc_len = length;
		}

		//
		// If FS_PREPROCESS returns EDEADLK,
		// retry the call since we should
		// make forward progress after a while.
		//
		int	preprocess_count = 0;

retry_fs_preprocess_page_out:

		//
		// Make sure storage block(s) are allocated for the page(s).
		// Metadata will change, we must make sure that a metadata
		// sync happens to get the changes onto the disk. Update the
		// timestamp to ensure sync_fs is called.
		//
		alloc_time = os::gethrtime();
		error = VOP_ALLOC_DATA(get_vp(), offset, &alloc_len,
		    NULL, 0, credp);
		if (error == EDEADLK) {
			// Sleep for 3 clock ticks.
			delay((clock_t)3);
			preprocess_count++;
			goto retry_fs_preprocess_page_out;
		}

		if (preprocess_count) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_AMBER,
			    ("page_out(%p) vp %p EDEADLK * %d\n",
			    this, get_vp(), preprocess_count));
			preprocess_count = 0;
		}

		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_RED, ("page_out(%p) FS_PREPROCESS error %d\n",
			    this, error));
			_environment.exception(new sol::op_e(error));
			content_rwlock.unlock();

			// Allow attribute changes.
			attr_lock.unlock();
			return;
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_PAGEOUT_S_B, FaultFunctions::generic);

	error = VOP_RDWR_DATA(get_vp(), offset, length,
	    client_file_size, fdb, flags, credp);

	//
	// Drop locks
	// It is okay to drop content_rwlock because when VOP_RDWR_DATA
	// returns, data is safe either on disk or in memory.
	//
	content_rwlock.unlock();
	attr_lock.unlock();

	//
	// ENOSPC can happen only if VOP_RDWR_DATA writes to a hole.
	// VOP_RDWR_DATA should never write to a hole. Blocks should have been
	// pre-allocated.
	//
	// When the syncdir mount option is not used, it is possible to
	// lose block allocations during a failover. The filesystem goes into
	// recovery mode and re-allocates blocks to account for the lost
	// allocations. During this time, nothing prevents a client from
	// consuming blocks and filling up the filesystem before recovery is
	// complete. This can, in theory, result in ENOSPC during pageout.
	//
	ASSERT(error != ENOSPC);

	FAULTPT_PXFS(FAULTNUM_PXFS_PAGEOUT_S_A, FaultFunctions::generic);

	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("page_out(%p) error %d\n",
		    this, error));
		_environment.exception(new sol::op_e(error));
		return;
	}

	//
	// We need to sync out the attributes if fastwrites is enabled.
	//
	if (get_fsp()->fastwrite_enabled()) {
		bool sync_needed = false;

		sync_needed = reset_mtime(timestamp, credp, _environment);

		//
		// With fastwrites alloc_time could be older because of a
		// setattr() call which set the MTIME back into history.
		// In such cases we would wrongly endup skipping a necessary
		// sync. Hence, we check with mod_time as well and pass
		// whichever is latest.
		//
		if (sync_needed && (alloc_time < mod_time)) {
			alloc_time = mod_time;
		}
	}

	//
	// If a VOP_FSYNC(FNODSYNC) hasn't pushed out the UFS log deltas
	// created, do so.
	//
	error = get_fsp()->get_fs_dep_implp()->
	    sync_if_necessary(alloc_time, get_vp(), credp);
	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("page_out(%p) sync error %d\n",
		    this, error));
		_environment.exception(new sol::op_e(error));
		return;
	}
}

//
// async_page_out - the client is asynchronously writing pages
//
void
file_ii::async_page_out(pxfs_aio::aio_callback_ptr aiock,
    sol::u_offset_t offset, sol::size_t length, sol::size_t client_file_size,
    int32_t flags, bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    int64_t timestamp, bool, Environment &_environment)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	fdbuffer_t	*fdb = bulkio_impl::conv(pglobj);
	os::hrtime_t	alloc_time = (os::hrtime_t)timestamp;

#ifdef DEBUG
	if (!get_fsp()->get_recovery_mode() &&
	    !get_fsp()->fastwrite_enabled() &&
	    // Do the check for UFS filesystems only
	    (get_fsp()->get_vfsp() != NULL &&
	    get_fsp()->get_vfsp()->vfs_fsid.val[1] == 2)) {
		os::hrtime_t now = os::gethrtime();

		//
		// If we are doing normal writes and are not in recovery
		// mode, bmap timestamp should not be newer than current
		// time and it should not be very old either. We quantify
		// very old with an arbitrary value of 8 hours.
		//
		if (now < timestamp || (now - timestamp) > 28800000000000L) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_AMBER,
			    ("async_page_out(%p) ts wrong bmap %llx now %llx\n",
			    this, timestamp, now));
		}
	}
#endif

	// Check if server-pull failed.
	if (!fdb) {
		_environment.exception(new sol::op_e(EIO));
		return;
	}

	// B_ASYNC flag should be set.
	ASSERT(flags & B_ASYNC);

	//
	// Extending file size via setattr can create pages mapping the
	// extended size if a UFS fragment was promoted. Those pages will
	// be written to disk and destroyed by setattr(). VOP_RDWR_DATA()
	// bypasses the page cache. VOP_RDWR_DATA(write) could write data
	// to disk and then pages created by a setattr could be flushed
	// corrupting on-disk data.
	//
	// To fix the above problem we wait for attribute change requests
	// to complete by requesting the attribute lock as a reader.
	//
	attr_lock.rdlock();

	// stop other node(s) from changing data permissions.
	content_rwlock.rdlock();

	nodeid_t	node_id = _environment.get_src_node().ndid;

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("async_page_out(%p) orphan node %d\n", this, node_id));
		pxfslib::throw_exception(_environment, EIO);
		content_rwlock.unlock();

		// Allow attribute changes.
		attr_lock.unlock();
		return;
	}

	CL_PANIC(read_token.contains(node_id));
	CL_PANIC(write_token.contains(node_id));

	// XXX io_async() should use u_offset_t and size_t instead of uint_t.
	io_async	*ios = new io_async(pglobj, aiock,
	    offset, length, (fs_ii *)fs_implp);
	fdb_set_iofunc(fdb, vn_fsio_done, (void *)ios, 0);

	int	error = 0;
	bool alloc_required = false;

	//
	// Allocate blocks if necessary.
	// We allocate in the following cases :
	// - If we are in recovery mode.
	//   There could have been a failover. The space allocation may have
	//   been lost. This is to support mounting UFS without syncdir.
	// - If this write had used the extended offset on the client side.
	//   Refer to pxreg::alloc_blocks() for details. This is an
	//   optimization for small-writes (writes < block-size).
	// - Fastwrites is enabled. With fastwrite enabled, the blocks are
	//   just allocated from the pool. However, the underlying filesystem
	//   still is not aware of this. Hence we need to call FS_PREPROCESS()
	//   to allocate the blocks.
	//
	if (get_fsp()->get_recovery_mode() || get_fsp()->fastwrite_enabled()) {
		alloc_required = true;
	} else {
		error = alloc_check_for_pageout(offset, length,
		    client_file_size, credp, alloc_required);
		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_RED, ("async_page_out(%p) "
			    "alloc_check_for_pageout error %d\n",
			    this, error));
			_environment.exception(new sol::op_e(error));
			content_rwlock.unlock();

			// Allow attribute changes.
			attr_lock.unlock();

			//
			// Destroy io async object as we haven't scheduled
			// the i/o request. Else we leak memory and the
			// aio_callback_impl object in the client will wait
			// for ever for i/o completion callback.
			//
			delete ios;
			return;
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_CHECK_RECOVERY_S_B, FaultFunctions::generic);

	if (get_fsp()->get_recovery_mode() || get_fsp()->fastwrite_enabled() ||
	    alloc_required) {
		// Truncate allocation request to actual file size.
		size_t	alloc_len;
		if (offset + length > client_file_size) {
			//
			// Shorten the allocation to the size known
			// to the client
			//
			alloc_len = (size_t)(client_file_size - offset);
		} else {
			alloc_len = length;
		}

		//
		// If FS_PREPROCESS returns EDEADLK,
		// retry the call since we should
		// make forward progress after a while.
		//
		int	preprocess_count = 0;

retry_fs_preprocess_async_page_out:

		//
		// Make sure storage block(s) are allocated for the page(s).
		// Metadata will change, we must make sure that a metadata
		// sync happens to get the changes onto the disk. Update the
		// timestamp to ensure sync_fs is called.
		//
		error = VOP_ALLOC_DATA(get_vp(), offset, &alloc_len,
		    NULL, 0, credp);
		alloc_time = os::gethrtime();

		if (error == EDEADLK) {
			// Sleep for 3 clock ticks.
			delay((clock_t)3);
			preprocess_count++;
			goto retry_fs_preprocess_async_page_out;
		}

		if (preprocess_count) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_AMBER,
			    ("async_page_out(%p) vp %p EDEADLK * %d\n",
			    this, get_vp(), preprocess_count));
			preprocess_count = 0;
		}

		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_RED,
			    ("async_page_out(%p)FS_PREPROCESS error %d "
			    "offset %lld len %lld\n",
			    this, error, offset, alloc_len));
			_environment.exception(new sol::op_e(error));
			content_rwlock.unlock();

			// Allow attribute changes.
			attr_lock.unlock();

			//
			// Destroy io async object as we haven't scheduled
			// the i/o request. Else we leak memory and the
			// aio_callback_impl object in the client will wait
			// for ever for i/o completion callback.
			//
			delete ios;
			return;
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_ASYNC_PAGEOUT_S_A, FaultFunctions::generic);

	error = VOP_RDWR_DATA(get_vp(), offset, length,
	    client_file_size, fdb, flags, credp);

	//
	// Drop locks
	// It is okay to drop content_rwlock because when VOP_RDWR_DATA
	// returns, data is safe either on disk or in memory.
	//
	content_rwlock.unlock();
	attr_lock.unlock();

	//
	// ENOSPC can happen when VOP_RDWR_DATA writes to a hole. VOP_RDWR_DATA
	// should never write to a hole as blocks should have been
	// pre-allocated. But, when the syncdir mount option is not used, it is
	// possible to lose block allocations during a failover.
	//
	ASSERT(error != ENOSPC);

	FAULTPT_PXFS(FAULTNUM_PXFS_ASYNC_PAGEOUT_S_B, FaultFunctions::generic);

	if (error) {
		//
		// Flag the error here but the callback mechanism will
		// asynchronously return the error via the client callback
		// mechanism and then cleanup the io_async object.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("async_page_out(%p) error %d\n",
		    this, error));
		_environment.exception(new sol::op_e(error));
	}

	//
	// We need to sync out the attributes if fastwrites is enabled.
	//
	if (get_fsp()->fastwrite_enabled()) {
		bool sync_needed = false;

		sync_needed = reset_mtime(timestamp, credp, _environment);

		//
		// With fastwrites alloc_time could older because of a
		// setattr() call which set the MTIME back into history.
		// In such cases we would wrongly endup skipping a necessary
		// sync. Hence, we check with mod_time as well and pass
		// whichever is latest.
		//
		if (sync_needed && (alloc_time < mod_time)) {
			alloc_time = mod_time;
		}
	}

	//
	// If a VOP_FSYNC(FNODSYNC) hasn't pushed out the UFS log deltas
	// created, do so.
	//
	error = get_fsp()->get_fs_dep_implp()->
	    sync_if_necessary(alloc_time, get_vp(), credp);
	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_RED,
		    ("async_page_out(%p) sync error %d\n",
		    this, error));
		_environment.exception(new sol::op_e(error));
		return;
	}
}

//
// sync
// Synchronize dirty pages with the pager. The cache
// manager retains pages in read-write mode. Syncflags
// controls the semantics of the operation.
//
void
file_ii::sync(sol::u_offset_t, sol::size_t, int32_t, bulkio::in_pages_ptr,
    solobj::cred_ptr, Environment &)
{
	//
	// XXX What should be done here? Perhaps just kick the fsflush.
	//
}

//
// write_lock - obtains permission to hold data with read and write permission.
//
// Note: if we change this code to lock file ranges instead of covering
// the whole file, we will need to rethink the locking between
// file attributes and range_lock() since the write token is used
// to protect against file size changes in the client cache.
//
void
file_ii::write_lock(uint32_t &server_incarn, Environment &_environment)
{
	range_lock();

	server_incarn = get_fsp()->get_server_incn();

	nodeid_t	node_id =  _environment.get_src_node().ndid;
	(void) inval_all_attrs(node_id, _environment);
	downgrade_all((sol::u_offset_t)0, FLUSH_BACK, node_id, _environment);

	if (_environment.exception()) {
		range_unlock();
		return;
	}

	token_state_lock.lock();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("write_lock(%p) orphan node %d\n", this, node_id));
		range_unlock();
		pxfslib::throw_exception(_environment, EIO);
		token_state_lock.unlock();
		return;
	}

#ifdef DEBUG
	//
	// Either nobody has the write token or
	// this node already has the write token.
	//
	nodeset		test_token;
	test_token.add_node(node_id);
	ASSERT(write_token.is_empty() ||
	    write_token.equal(test_token));
	ASSERT(read_token.is_empty() ||
	    read_token.equal(test_token));
	ASSERT(clientset.clients.contains(node_id));
#endif
	read_token.add_node(node_id);
	write_token.add_node(node_id);

	PXFS_DBPRINTF(
	    PXFS_TRACE_FILE,
	    PXFS_GREEN,
	    ("write_lock(%p) node %d r %llx w %llx c %llx\n",
	    this, node_id, read_token.bitmask(), write_token.bitmask(),
	    clientset.clients.bitmask()));

	token_state_lock.unlock();
	range_unlock();
}

//
// read_lock - obtains permission to hold data with read permission.
//
void
file_ii::read_lock(uint32_t &server_incarn, Environment &_environment)
{
	range_lock();

	server_incarn = get_fsp()->get_server_incn();

	nodeid_t	node_id =  _environment.get_src_node().ndid;
	downgrade_all((sol::u_offset_t)0, DENY_WRITES, node_id, _environment);

	if (_environment.exception()) {
		range_unlock();
		return;
	}

	token_state_lock.lock();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("read_lock(%p) orphan node %d\n", this, node_id));
		range_unlock();
		pxfslib::throw_exception(_environment, EIO);
		token_state_lock.unlock();
		return;
	}

#ifdef DEBUG
	//
	// Either nobody has the write token or
	// this node already has the write token.
	// Other nodes can have a read token.
	//
	nodeset		test_token;
	test_token.add_node(node_id);
	ASSERT(write_token.is_empty() ||
	    write_token.equal(test_token));
	ASSERT(clientset.clients.contains(node_id));
#endif
	read_token.add_node(node_id);

	PXFS_DBPRINTF(
	    PXFS_TRACE_FILE,
	    PXFS_GREEN,
	    ("read_lock(%p) node %d r %llx w %llx c %llx\n",
	    this, node_id, read_token.bitmask(), write_token.bitmask(),
	    clientset.clients.bitmask()));

	token_state_lock.unlock();
	range_unlock();
}

//
// bmap - allocates space at the specified offset and size.
//
void
file_ii::bmap(sol::u_offset_t	offset,
    sol::size_t			&length,
    sol::vattr_t		&attributes,
    sol::nodeid_t		&server_node,
    int64_t			&timestamp,
    PXFS_VER::server_status_t	&status,
    solobj::cred_ptr		credobj,
    Environment			&_environment)
{
	MC_PROBE_0(file_ii_bmap_start, "clustering pxfs", "");

	fdbuffer_t	*fdb = NULL;
	cred_t		*credp = solobj_impl::conv(credobj);
	size_t		len = length;
	int		error;

	server_node = orb_conf::node_number();

	if (get_fsp()->fastwrite_enabled()) {
		status = get_fsp()->switch_to_fastpath(_environment);
		if (_environment.exception() != NULL) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_RED,
			    ("bmap(%p) switch_to_fastpath() error %d\n",
			    this, pxfslib::get_err(_environment)));
			return;
		}

		if (status == PXFS_VER::GREENZONE) {
			return;
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_BMAP_S_B, FaultFunctions::generic);

	int	vop_alloc_count = 0;
	do {
		error = VOP_ALLOC_DATA(get_vp(), offset, &len, fdb, 0, credp);

		if (error != EDEADLK) {
			break;
		}
		//
		// The operation failed because of a UFS Log deadlock error.
		// we can do a retry.
		//
		vop_alloc_count++;

		// Sleep for 3 clock ticks.
		delay((clock_t)3);
	} while (true);

	if (vop_alloc_count) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FILE,
		    PXFS_AMBER,
		    ("bmap(%p) EDEADLK * %d\n",
		    this, vop_alloc_count));
	}

	MC_PROBE_0(file_ii_bmap_ALLOC, "clustering pxfs", "");

	FAULTPT_PXFS(FAULTNUM_PXFS_BMAP_S_A, FaultFunctions::generic);

	if (error != 0) {
		_environment.exception(new sol::op_e(error));
	} else {
		// Record the time of the space allocation
		timestamp = os::gethrtime();

		// Return the modified attributes back to the client.
		vattr	*vap = (vattr *)&attributes;

#if	SOL_VERSION >= __s11
		error = VOP_GETATTR(get_vp(), vap, 0, credp, NULL);
#else
		error = VOP_GETATTR(get_vp(), vap, 0, credp);
#endif

		if (error != 0) {
			_environment.exception(new sol::op_e(error));
		} else {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_GREEN,
			    ("bmap(%p) off %llx length %x len %lx siz %llx\n",
			    this, offset, length, len, vap->va_size));

			length = len;
		}
	}
	MC_PROBE_0(file_ii_bmap_end, "clustering pxfs", "");
}

//
// downgrade_all - force all clients to carry out the specified action
// except for the one client, which is usually the requesting client.
//
void
file_ii::downgrade_all(sol::u_offset_t off,
    downgrade_action action, nodeid_t skip_node, Environment &env)
{
	bool		dead_clients = false;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// Do not contact the dead client,
			// but ensure that the client has no privileges
			// in this area
			//
			token_state_lock.lock();
			read_token.remove_node(entryp->node.ndid);

			//
			// Drain pending requests from this client before
			// revoking write permission for this node.
			//
			content_rwlock.wrlock();
			write_token.remove_node(entryp->node.ndid);
			content_rwlock.unlock();

			token_state_lock.unlock();
			dead_clients = true;
			continue;
		}

		if (entryp->node.ndid == skip_node) {
			continue;
		}

		// Tell the client to take the specified action
		downgrade(off, action, entryp, env);

		if (env.exception() != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(env.exception())) {
				env.clear();
				//
				// The client is gone.
				// Mark the client as dead.
				//
				token_state_lock.lock();
				read_token.remove_node(entryp->node.ndid);

				//
				// Drain pending requests from this client
				// before revoking write permission for
				// this node.
				//
				content_rwlock.wrlock();
				write_token.remove_node(entryp->node.ndid);
				content_rwlock.unlock();

				token_state_lock.unlock();
				entryp->clear();
				dead_clients = true;
			} else {
				//
				// The downgrade operation was unsuccessful.
				//
				break;
			}
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}
}

//
// downgrade -  tell the specified client to carry out the specified action
// with respect to the client cache of page data.
//
// Any exception on the client invocation is returned to the caller.
// An exception can be caused by either a node death or a failure of
// the client to carry out the requested operation.
//
// The client data token privileges are only changed upon successful
// completion of the requested action.
//
void
file_ii::downgrade(sol::u_offset_t off, downgrade_action action,
    client_set::client_entry *entryp, Environment &env)
{
	nodeid_t	node_id	= entryp->node.ndid;

	token_state_lock.lock();

	if (!read_token.contains(node_id)) {
		//
		// Client cache doesn't have any pages in this range.
		//
		ASSERT(!write_token.contains(node_id));
		token_state_lock.unlock();
		return;
	}

	Environment	e;
	int		error = 0;

	//
	// The client could go away when we drop the list_lock.
	// So we use a smart pointer to ensure that the client
	// remains in existence.
	//
	PXFS_VER::fobj_client_var	client_v =
	    PXFS_VER::fobj_client::_duplicate(
	    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

	//
	// Note: we handle exceptions on cache operations by recording that
	// the client has no cache rights on any pages.
	//
	switch (action) {
	case WRITE_BACK:
		if (write_token.contains(node_id)) {
			token_state_lock.unlock();

			// Do not hold the clientset lock across an invocation
			entryp->hold();
			clientset.list_lock.unlock();

			//
			// Client flushes data back to server and
			// retains privileges
			//
			client_v->data_write_back(e);

			clientset.list_lock.rdlock();
			entryp->release();

			//
			// It is ok to ignore an error (exception) from
			// write_back() since the client continues to
			// retain its READ and WRITE token.
			//
			error = pxfslib::get_err(e);
			PXFS_DBPRINTF(PXFS_TRACE_FILE,
			    (error ? PXFS_RED : PXFS_GREEN),
			    ("downgrade(%p) WRITE_BACK node %d err %d\n",
			    this, node_id, error));
		} else {
			token_state_lock.unlock();
		}
		break;

	case DELETE_RANGE:
		token_state_lock.unlock();

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		//
		// Client deletes data past the specified offset.
		//
		client_v->data_delete_range(off,
		    get_fsp()->get_server_incn(), e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception()) {
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				//
				// Client node gone. Propagate error to caller
				//
				env.system_exception(CORBA::COMM_FAILURE(0,
				    CORBA::COMPLETED_NO));
				PXFS_DBPRINTF(
				    PXFS_TRACE_FILE,
				    PXFS_RED,
				    ("downgrade(%p) DELETE_RANGE node %d died "
				    "off %llx\n",
				    this, node_id, off));
			} else {
				ASSERT(!e.exception()->is_system_exception());
				//
				// Client encountered error.
				// Propagate error to caller.
				//
				error = pxfslib::get_err(e);
				pxfslib::throw_exception(env, error);
				PXFS_DBPRINTF(
				    PXFS_TRACE_FILE,
				    PXFS_RED,
				    ("downgrade(%p) DELETE_RANGE node %d "
				    "off %llx err %d\n",
				    this, node_id, off, error));
			}
		} else {
			if (entryp->is_zombie()) {
				break;
			}
			//
			// DELETE_RANGE succeeded
			// The client data token privileges change
			// only upon successful operation.
			//
			token_state_lock.lock();
			read_token.remove_node(node_id);
			write_token.remove_node(node_id);
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_GREEN,
			    ("downgrade(%p) DELETE_RANGE node %d off %llx\n",
			    this, node_id, off));
			token_state_lock.unlock();
		}
		break;

	case FLUSH_BACK:
		token_state_lock.unlock();

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		//
		// Client pushes all dirty pages to server
		// and discards all cached data.
		//
		client_v->data_write_back_and_delete(
		    get_fsp()->get_server_incn(), e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception()) {
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				//
				// Client node gone. Propagate error to caller
				//
				env.system_exception(CORBA::COMM_FAILURE(0,
				    CORBA::COMPLETED_NO));
				PXFS_DBPRINTF(
				    PXFS_TRACE_FILE,
				    PXFS_RED,
				    ("downgrade(%p) FLUSH_BACK node %d died\n",
				    this, node_id));
			} else {
				ASSERT(!e.exception()->is_system_exception());
				//
				// Client encountered error.
				// Propagate error to caller.
				//
				error = pxfslib::get_err(e);
				pxfslib::throw_exception(env, error);
				PXFS_DBPRINTF(
				    PXFS_TRACE_FILE,
				    PXFS_RED,
				    ("downgrade(%p) FLUSH_BACK node %d "
				    "err %d\n",
				    this, node_id, error));
			}
		} else {
			if (entryp->is_zombie()) {
				break;
			}
			//
			// FLUSH_BACK succeeded
			// The client data token privileges change
			// only upon successful operation.
			//
			token_state_lock.lock();
			read_token.remove_node(node_id);
			write_token.remove_node(node_id);
			PXFS_DBPRINTF(
			    PXFS_TRACE_FILE,
			    PXFS_GREEN,
			    ("downgrade(%p) FLUSH_BACK node %d\n",
			    this, node_id));
			token_state_lock.unlock();
		}
		break;

	case DENY_WRITES:
		if (write_token.contains(node_id)) {
			token_state_lock.unlock();

			// Do not hold the clientset lock across an invocation
			entryp->hold();
			clientset.list_lock.unlock();

			//
			// Client pushes dirty pages to server,
			// and downgrades data privileges to read only.
			//
			client_v->data_write_back_downgrade_read_only(
			    get_fsp()->get_server_incn(), e);

			clientset.list_lock.rdlock();
			entryp->release();

			if (e.exception()) {
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					//
					// Client node gone.
					// Propagate error to caller
					//
					env.system_exception(
					    CORBA::COMM_FAILURE(0,
					    CORBA::COMPLETED_NO));
					PXFS_DBPRINTF(
					    PXFS_TRACE_FILE,
					    PXFS_RED,
					    ("downgrade(%p) DENY_WRITES node "
					    "%d died\n",
					    this, node_id));
				} else {
					ASSERT(!e.exception()->
					    is_system_exception());
					//
					// Client encountered error.
					// Propagate error to caller.
					//
					// We are in a very tricky situation.
					// One client wants to read this file.
					// So we called a second client that
					// has the write token. We've not been
					// able to downgrade the
					// second client to read-only, possibly
					// due to lack of space for the first
					// client to write back its dirty pages.
					// See bug 4808748. We return an error
					// to the client trying to read the
					// file.
					//
					error = pxfslib::get_err(e);
					pxfslib::throw_exception(env, error);
					PXFS_DBPRINTF(
					    PXFS_TRACE_FILE,
					    PXFS_RED,
					    ("downgrade(%p) DENY_WRITES node "
					    "%d err %d\n",
					    this, node_id, error));
				}
			} else {
				if (entryp->is_zombie()) {
					break;
				}
				//
				// DENY_WRITES succeeded
				// The client data token privileges change
				// only upon successful operation.
				//
				token_state_lock.lock();
				write_token.remove_node(node_id);
				PXFS_DBPRINTF(
				    PXFS_TRACE_FILE,
				    PXFS_GREEN,
				    ("downgrade(%p) DENY_WRITES node %d\n",
				    this, node_id));
				token_state_lock.unlock();
			}
		} else {
			token_state_lock.unlock();
		}
		break;

	default:
		token_state_lock.unlock();
		ASSERT(0);
	}
	e.clear();
}

void
file_ii::ckpt_cachedata_flag(bool onoff)
{
	cachedata_flag = onoff;
}

//
// enable_cachedata - the client want to map pages
// and then cache information. The client believes
// that this is not currently allowed.
//
void
file_ii::enable_cachedata(Environment &env)
{
	range_lock();

	if (cachedata_flag) {
		//
		// Caching is already allowed.
		// Multiple clients may want to enable caching simultaneously.
		//
		range_unlock();
		return;
	}

	int	error;
	int	result;

	fs_dependent_impl	*fs_dep_implp = fs_implp->get_fs_dep_implp();

	fs_dep_implp->replay_ioctl(this, _FIODIRECTIO,
	    (sol::intptr_t)DIRECTIO_OFF, 0, result, error);

	ASSERT(error == 0);

	cachedata_flag = true;
	if (is_replicated()) {
		//
		// Its safe to get a reference to ourself without locking
		// since we are in the middle of an IDL call and
		// _unrefereneced() won't be delivered until after we return.
		//
		PXFS_VER::file_var	file_v = get_fileref();
		get_ckpt()->ckpt_cachedata_flag(file_v, cachedata_flag, env);
		env.clear();
	}

	Environment	e;
	bool		dead_clients = false;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// skip this dead client
			//
			dead_clients = true;
			continue;
		}

		//
		// The client could go away when we drop the list_lock.
		// So we use a smart pointer to ensure that the client
		// remains in existence.
		//
		PXFS_VER::fobj_client_var	client_v =
		    PXFS_VER::fobj_client::_duplicate(
		    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		// Tell the client to enable caching
		client_v->data_change_cache_flag(true, e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			entryp->clear();
			dead_clients = true;
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}

	range_unlock();
}

//
// set_cachedata_flag
// Disable or enable data page caching on all clients using this file.
// Helper function for ioctl(_FIODIRECTIO).
// The range_lock() should be held before calling this.
//
int
file_ii::set_cachedata_flag(bool onoff, Environment &env)
{
	// LINTED: Boolean argument to equal/not equal
	if (cachedata_flag == onoff) {
		// Easy case, flag didn't change.
		return (0);
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FILE,
	    PXFS_GREEN,
	    ("set_cachedata_flag(%p) %d\n",
	    this, onoff));

	//
	// If directio is ON, onoff = 0.
	// cachedataflag = 1, directio = off, onoff = 1
	// cachedataflag = 0, directio = on, onoff = 0
	// REMINDER: caches have to be flushed before directio flag
	// can be set on the clients. This is because, once the
	// directio flag is set on the clients (indicating that
	// caching is disabled), the clients expect to see no cached
	// pages. The server will lock out reads/writes on the clients
	// ONLY for the duration of setting the cachedata_flag on the
	// client side. Next write/read will see the latest value of
	// cachedata_flag.
	//
	if (!onoff) {
		// We are disabling caching so flush data.
		int error = flush_caches(0, env);
		if (error != 0) {
			return (error);
		}
	}
	Environment	e;
	bool		dead_clients = false;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// skip this dead client
			//
			dead_clients = true;
			continue;
		}

		//
		// The client could go away when we drop the list_lock.
		// So we use a smart pointer to ensure that the client
		// remains in existence.
		//
		PXFS_VER::fobj_client_var	client_v =
		    PXFS_VER::fobj_client::_duplicate(
		    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		//
		// Notify all proxy vnodes of the new value.
		//
		client_v->data_change_cache_flag(onoff, e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			entryp->clear();
			dead_clients = true;
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}

	cachedata_flag = onoff;
	if (is_replicated()) {
		//
		// Its safe to get a reference to ourself without locking
		// since we are in the middle of an IDL call and
		// _unrefereneced() won't be delivered until after we return.
		// Note: since this might occur during initialization,
		// we can't rely on 'fobj_obj' being initialized.
		//
		PXFS_VER::file_var file_v = get_fileref();
		get_ckpt()->ckpt_cachedata_flag(file_v, onoff, env);
		env.clear();
	}

	return (0);
}

void
file_ii::uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	uio_t	*uiop = bulkio_impl::conv(uioobj);

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_B, FaultFunctions::generic);

	uiop->uio_resid = (ssize_t)len;

#if	SOL_VERSION >= __s10
	VOP_RWLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWLOCK(get_vp(), 0);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_AFTER_RWLOCK,
	    FaultFunctions::generic);

#if	SOL_VERSION >= __s10
	int error = VOP_READ(get_vp(), uiop, ioflag, credp, NULL);
#else
	int error = VOP_READ(get_vp(), uiop, ioflag, credp);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_A, FaultFunctions::generic);

#if	SOL_VERSION >= __s10
	VOP_RWUNLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWUNLOCK(get_vp(), 0);
#endif

	len -= (sol::size_t)uiop->uio_resid;

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
file_ii::uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj, int32_t ioflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	uio_t	*uiop = bulkio_impl::conv(uioobj);

	//
	// Check for an exception during server-pull
	// We signal an exception by returning NULL for uiop
	//
	if (uiop == NULL) {
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_B, FaultFunctions::generic);

	uiop->uio_resid = (ssize_t)len;
	if (uiop->uio_iov == NULL) {
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	// It is possible for us to block behind dio_writes.rdlock()
	// only to have caching turned on once we aquired the lock.
	//
	// At that point, we return EAGAIN and have the caller try again.

	dio_writes.rdlock();

	if (is_cached()) {
		pxfslib::throw_exception(_environment, EAGAIN);
		dio_writes.unlock();
		return;
	}

#if	SOL_VERSION >= __s10
	VOP_RWLOCK(get_vp(), V_WRITELOCK_TRUE, NULL);
#else
	VOP_RWLOCK(get_vp(), 1);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_AFTER_RWLOCK,
		FaultFunctions::generic);

	//
	// To ensure essential metadata integrity, append the FDSYNC
	// flag to ioflag when client and server are not colocated
	// together. See bug report 4810418 for more information.
	//
#if	SOL_VERSION >= __s10
	int	error = VOP_WRITE(get_vp(), uiop,
	    _environment.is_local_client() ? ioflag : ioflag | FDSYNC,
	    credp, NULL);
#else
	int	error = VOP_WRITE(get_vp(), uiop,
	    _environment.is_local_client() ? ioflag : ioflag | FDSYNC,
	    credp);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_A, FaultFunctions::generic);

#if	SOL_VERSION >= __s10
	VOP_RWUNLOCK(get_vp(), V_WRITELOCK_TRUE, NULL);
#else
	VOP_RWUNLOCK(get_vp(), 1);
#endif

	dio_writes.unlock();

	len -= (sol::size_t)uiop->uio_resid;

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
file_ii::space(int32_t, const sol::flock64_t &, int32_t, sol::u_offset_t,
    solobj::cred_ptr, Environment &_environment)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FILE,
	    PXFS_RED,
	    ("space(%p) not yet implemented\n",
	    this));
	pxfslib::throw_exception(_environment, ENOTSUP);
}

//
// prepare_recover - initialize data structures prior to recovering
// information from clients.
//
void
file_ii::prepare_recover()
{
	read_token.empty();
	write_token.empty();

	fobjplus_ii::prepare_recover();
}

//
// recovered_state - we find out the data privileges of the client.
//
// Pages are pushed to the server in invocation requests, and not replies.
// So we do not have to recover pages lost in flight.
//
void
file_ii::recovered_state(PXFS_VER::recovery_info &recovery)
{
	fobjplus_ii::recovered_state(recovery);

	if ((recovery.data_rights & (PXFS_READ_TOKEN | PXFS_WRITE_TOKEN)) ==
	    (PXFS_READ_TOKEN | PXFS_WRITE_TOKEN)) {
		//
		// Client has both read and write data privileges
		//
		ASSERT(read_token.is_empty());
		ASSERT(write_token.is_empty());
		read_token.add_node(recovery.ndid);
		write_token.add_node(recovery.ndid);

	} else if ((recovery.data_rights & PXFS_READ_TOKEN) != 0) {
		//
		// Client has only read data privileges
		//
		ASSERT(write_token.is_empty());
		read_token.add_node(recovery.ndid);
	} else {
		//
		// Client has no data privileges
		//
		ASSERT((recovery.data_rights & PXFS_WRITE_TOKEN) == 0);
	}
}

//
// file_norm_impl methods
//

handler *
file_norm_impl::get_handler()
{
	return (_handler());
}

// Return a new PXFS_VER::fobj CORBA reference to this
PXFS_VER::fobj_ptr
file_norm_impl::get_fobjref()
{
	return (get_objref());
}

// Return a new PXFS_VER::file CORBA reference to this
PXFS_VER::file_ptr
file_norm_impl::get_fileref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
REPL_PXFS_VER::fs_replica_ptr
file_norm_impl::get_ckpt()
{
	ASSERT(0);
	return (REPL_PXFS_VER::fs_replica::_nil());
}

//
// Add a commit checkpoint.
//
void
file_norm_impl::commit(Environment &)
{
}

/* fobj */
PXFS_VER::fobj_type_t
file_norm_impl::get_fobj_type(Environment &_environment)
{
	return (file_ii::get_fobj_type(_environment));
}

void
file_norm_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
file_norm_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
file_norm_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::access(accmode, accflags, credobj, _environment);
}

void
file_norm_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
file_norm_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::get_secattributes(sattr, secattrflag, credobj, _environment);
} //lint !e1746

void
file_norm_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag,
    solobj::cred_ptr credobj,
    Environment &_environment)
{
	file_ii::set_secattributes(sattr, secattrflag, credobj, _environment);
}

void
file_norm_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::pathconf(cmd, result, credobj, _environment);
}

void
file_norm_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	file_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
file_norm_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result, credobj,
	    _environment);
}

void
file_norm_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	file_ii::frlock(cmd, lock_info, flag, off, credobj, cb, _environment);
}

void
file_norm_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	file_ii::frlock_cancel_request(cb, _environment);
}

void
file_norm_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	file_ii::frlock_execute_request(cb, _environment);
}

void
file_norm_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
file_norm_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::remove_locks(cpid, sysid, credobj, _environment);
}

void
file_norm_impl::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	fobjplus_ii::cache_new_client(binfo, client1_p, client2_p,
	    _environment);
} //lint !e1746

void
file_norm_impl::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	fobjplus_ii::cache_remove_client(client_p, _environment);
}

void
file_norm_impl::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_get_all_attr(credobj, rights, attributes,
	    seqnum, server_incarn, _environment);
}

void
file_norm_impl::cache_write_all_attr(sol::vattr_t &attributes,
    int32_t attrflags, bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_write_all_attr(attributes, attrflags, discard, sync,
	    credobj, server_incarn, _environment);
}

void
file_norm_impl::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	fobjplus_ii::cache_attr_drop_token(server_incarn, _environment);
}

void
file_norm_impl::cache_set_attributes(sol::vattr_t &wb_attributes,
    int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	fobjplus_ii::cache_set_attributes(wb_attributes, wb_attrflags,
	    attributes, attrflags, credobj, seqnum, server_incarn,
	    _environment);
}

void
file_norm_impl::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	fobjplus_ii::cache_access(accmode, accflags, credobj, allowed,
	    seqnum, _environment);
}

void
file_norm_impl::uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::uioread(len, uioobj, ioflag, credobj, _environment);
}

void
file_norm_impl::uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::uiowrite(len, uioobj, ioflag, credobj, _environment);
}

void
file_norm_impl::cascaded_fsync(int32_t syncflag, solobj::cred_ptr credobj,
    Environment &_environment)
{
	file_ii::fsync(syncflag, credobj, _environment);
}

void
file_norm_impl::space(int32_t cmd, const sol::flock64_t &lock_info,
    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
    Environment &_environment)
{
	file_ii::space(cmd, lock_info, flag, off, credobj, _environment);
}

//
// file IDL operations
//

void
file_norm_impl::page_in(sol::u_offset_t offset, sol::size_t length,
    PXFS_VER::acc_rights_t access_rights,
    bulkio::inout_pages_ptr &pglobj, bulkio::file_hole_list_out holes,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::page_in(offset, length, access_rights, pglobj, holes, credobj,
	    _environment);
} //lint !e1746

void
file_norm_impl::async_page_in(pxfs_aio::aio_callback_ptr aiock,
    sol::u_offset_t offset, sol::size_t length,
    PXFS_VER::acc_rights_t access_rights, bulkio::in_aio_pages_ptr pglobj,
    solobj::cred_ptr credobj, Environment &_environment)
{
	file_ii::async_page_in(aiock, offset, length, access_rights,
	    pglobj, credobj, _environment);
}

void
file_norm_impl::page_out(sol::u_offset_t offset, sol::size_t length,
    sol::size_t client_file_size,
    int32_t flags, bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    int64_t timestamp, bool size_in_sync, Environment &_environment)
{
	file_ii::page_out(offset, length, client_file_size, flags, pglobj,
	    credobj, timestamp, size_in_sync, _environment);
}

void
file_norm_impl::async_page_out(pxfs_aio::aio_callback_ptr aiock,
    sol::u_offset_t offset, sol::size_t length,
    sol::size_t client_file_size, int32_t flags,
    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    int64_t timestamp, bool size_in_sync, Environment &_environment)
{
	file_ii::async_page_out(aiock, offset, length,
	    client_file_size, flags, pglobj, credobj,
	    timestamp, size_in_sync, _environment);
}

void
file_norm_impl::sync(sol::u_offset_t offset, sol::size_t length,
    int32_t syncflags, bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    Environment &_environment)
{
	file_ii::sync(offset, length, syncflags, pglobj, credobj,
	    _environment);
}

void
file_norm_impl::cascaded_write_lock(uint32_t &server_incarn,
    Environment &_environment)
{
	file_ii::write_lock(server_incarn, _environment);
}

void
file_norm_impl::cascaded_read_lock(uint32_t &server_incarn,
    Environment &_environment)
{
	file_ii::read_lock(server_incarn, _environment);
}

void
file_norm_impl::cascaded_truncate(sol::u_offset_t size,
    Environment &_environment)
{
	//
	// It is possible for a file's mode to be changed to read only
	// after it has been opened with write access. If a client then
	// tries to set the size of the file, the normal access
	// checking done in VOP_SETATTR would fail with EACCES,
	// although it should be legal for it to do so. To get around
	// this, file_ii::set_size() supplies root's credentials (kcred)
	// rather than the client's credentials. Therefore we no longer
	// need to supply the user's credentials (credobj) to truncate().
	//
	file_ii::cascaded_truncate(size, _environment);
}

void
file_norm_impl::bmap(sol::u_offset_t	offset,
    sol::size_t			&length,
    sol::vattr_t		&attributes,
    sol::nodeid_t		&server_node,
    int64_t			&timestamp,
    PXFS_VER::server_status_t	&status,
    solobj::cred_ptr		credobj,
    Environment			&_environment)
{
	file_ii::bmap(offset, length, attributes,
	    server_node, timestamp, status, credobj, _environment);
}

void
file_norm_impl::enable_cachedata(Environment &_environment)
{
	file_ii::enable_cachedata(_environment);
}

//
// file_repl_impl methods
//

handler *
file_repl_impl::get_handler()
{
	return (_handler());
}

// Return a new PXFS_VER::fobj CORBA reference to this
PXFS_VER::fobj_ptr
file_repl_impl::get_fobjref()
{
	return (get_objref());
}

// Return a new PXFS_VER::file CORBA reference to this
PXFS_VER::file_ptr
file_repl_impl::get_fileref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
REPL_PXFS_VER::fs_replica_ptr
file_repl_impl::get_ckpt()
{
	return ((repl_pxfs_server*)(get_provider()))->
	    get_checkpoint_fs_replica();
}

//
// Add a commit checkpoint.
//
void
file_repl_impl::commit(Environment &e)
{
	add_commit(e);
}

/* fobj */

PXFS_VER::fobj_type_t
file_repl_impl::get_fobj_type(Environment &_environment)
{
	if (is_dead(_environment)) {
		return (PXFS_VER::fobj_fobj);
	}
	return (file_ii::get_fobj_type(_environment));
}

void
file_repl_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
file_repl_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
file_repl_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::access(accmode, accflags, credobj, _environment);
}

void
file_repl_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
file_repl_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::get_secattributes(sattr, secattrflag, credobj, _environment);
} //lint !e1746

void
file_repl_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::set_secattributes(sattr, secattrflag, credobj, _environment);
}

void
file_repl_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::pathconf(cmd, result, credobj, _environment);
}

void
file_repl_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
file_repl_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result, credobj,
	    _environment);
}

void
file_repl_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::frlock(cmd, lock_info, flag, off, credobj, cb, _environment);
}

void
file_repl_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::frlock_cancel_request(cb, _environment);
}

void
file_repl_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::frlock_execute_request(cb, _environment);
}

void
file_repl_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
file_repl_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::remove_locks(cpid, sysid, credobj, _environment);
}

void
file_repl_impl::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_new_client(binfo, client1_p, client2_p,
	    _environment);
} //lint !e1746

void
file_repl_impl::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_remove_client(client_p, _environment);
}

void
file_repl_impl::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_get_all_attr(credobj, rights, attributes,
	    seqnum, server_incarn, _environment);
}

void
file_repl_impl::cache_write_all_attr(sol::vattr_t &attributes,
    int32_t attrflags, bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_write_all_attr(attributes, attrflags, discard, sync,
	    credobj, server_incarn, _environment);
}

void
file_repl_impl::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_attr_drop_token(server_incarn, _environment);
}

void
file_repl_impl::cache_set_attributes(sol::vattr_t &wb_attributes,
    int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_set_attributes(wb_attributes, wb_attrflags,
	    attributes, attrflags, credobj, seqnum, server_incarn,
	    _environment);
}

void
file_repl_impl::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fobjplus_ii::cache_access(accmode, accflags, credobj, allowed,
	    seqnum, _environment);
}

void
file_repl_impl::uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::uioread(len, uioobj, ioflag, credobj, _environment);
}

void
file_repl_impl::uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::uiowrite(len, uioobj, ioflag, credobj, _environment);
}

void
file_repl_impl::cascaded_fsync(int32_t syncflag, solobj::cred_ptr credobj,
    Environment &_environment)
{
	//
	// Check with the fs server whether we can proceed with the
	// cascaded invocation. If we can, the active invocation count
	// will be incremented by one. We must decrement active
	// invocation count after the cascaded invocation completes.
	//
	if (is_dead(_environment) ||
	    get_fsp()->freeze_prepare_in_progress(_environment)) {
		return;
	}

	file_ii::fsync(syncflag, credobj, _environment);

	//
	// Decrement outstanding invocation count and wakeup any
	// threads waiting for invocation count to become zero.
	//
	get_fsp()->decrement_invo_count();
}

void
file_repl_impl::space(int32_t cmd, const sol::flock64_t &lock_info,
    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::space(cmd, lock_info, flag, off, credobj, _environment);
}

void
file_repl_impl::page_in(sol::u_offset_t offset, sol::size_t length,
    PXFS_VER::acc_rights_t access_rights,
    bulkio::inout_pages_ptr &pglobj, bulkio::file_hole_list_out holes,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::page_in(offset, length, access_rights, pglobj, holes, credobj,
	    _environment);
} //lint !e1746

void
file_repl_impl::async_page_in(pxfs_aio::aio_callback_ptr aiock,
    sol::u_offset_t offset, sol::size_t length,
    PXFS_VER::acc_rights_t access_rights, bulkio::in_aio_pages_ptr pglobj,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::async_page_in(aiock, offset, length, access_rights,
	    pglobj, credobj, _environment);
}

void
file_repl_impl::page_out(sol::u_offset_t offset, sol::size_t length,
    sol::size_t client_file_size,
    int32_t flags, bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    int64_t timestamp, bool size_in_sync, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::page_out(offset, length, client_file_size, flags, pglobj,
	    credobj, timestamp, size_in_sync, _environment);
}

void
file_repl_impl::async_page_out(pxfs_aio::aio_callback_ptr aiock,
    sol::u_offset_t offset, sol::size_t length,
    sol::size_t client_file_size, int32_t flags,
    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    int64_t timestamp, bool size_in_sync, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::async_page_out(aiock, offset, length,
	    client_file_size, flags, pglobj,
	    credobj, timestamp, size_in_sync, _environment);
}

void
file_repl_impl::sync(sol::u_offset_t offset, sol::size_t length,
    int32_t syncflags, bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::sync(offset, length, syncflags, pglobj, credobj,
	    _environment);
}

void
file_repl_impl::cascaded_write_lock(uint32_t &server_incarn,
    Environment &_environment)
{
	//
	// Check with the fs server whether we can proceed with the
	// cascaded invocation. If we can, the active invocation count
	// will be incremented by one. We must decrement active
	// invocation count after the cascaded invocation completes.
	//
	if (is_dead(_environment) ||
	    get_fsp()->freeze_prepare_in_progress(_environment)) {
		return;
	}

	file_ii::write_lock(server_incarn, _environment);

	//
	// Decrement outstanding invocation count and wakeup any
	// threads waiting for invocation count to become zero.
	//
	get_fsp()->decrement_invo_count();

}

void
file_repl_impl::cascaded_read_lock(uint32_t &server_incarn,
    Environment &_environment)
{
	//
	// Check with the fs server whether we can proceed with the
	// cascaded invocation. If we can, the active invocation count
	// will be incremented by one. We must decrement active
	// invocation count after the cascaded invocation completes.
	//
	if (is_dead(_environment) ||
	    get_fsp()->freeze_prepare_in_progress(_environment)) {
		return;
	}

	file_ii::read_lock(server_incarn, _environment);

	//
	// Decrement outstanding invocation count and wakeup any
	// threads waiting for invocation count to become zero.
	//
	get_fsp()->decrement_invo_count();
}

void
file_repl_impl::cascaded_truncate(sol::u_offset_t size,
    Environment &_environment)
{
	//
	// Check with the fs server whether we can proceed with the
	// cascaded invocation. If we can, the active invocation count
	// will be incremented by one. We must decrement active
	// invocation count after the cascaded invocation completes.
	//
	if (is_dead(_environment) ||
	    get_fsp()->freeze_prepare_in_progress(_environment)) {
		return;
	}

	//
	// It is possible for a file's mode to be changed to read only
	// after it has been opened with write access. If a client then
	// tries to set the size of the file, the normal access
	// checking done in VOP_SETATTR would fail with EACCES,
	// although it should be legal for it to do so. To get around
	// this, file_ii::set_size() supplies root's credentials (kcred)
	// rather than the client's credentials. Therefore we no longer
	// need to supply the user's credentials (credobj) to truncate().
	//
	file_ii::cascaded_truncate(size, _environment);

	//
	// Decrement outstanding invocation count and wakeup any
	// threads waiting for invocation count to become zero.
	//
	get_fsp()->decrement_invo_count();
}

void
file_repl_impl::bmap(sol::u_offset_t	offset,
    sol::size_t			&length,
    sol::vattr_t		&attributes,
    sol::nodeid_t		&server_node,
    int64_t			&timestamp,
    PXFS_VER::server_status_t	&status,
    solobj::cred_ptr		credobj,
    Environment			&_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::bmap(offset, length, attributes, server_node,
	    timestamp, status, credobj, _environment);
}

void
file_repl_impl::enable_cachedata(Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	file_ii::enable_cachedata(_environment);
}
