//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fs_base_impl.cc	1.6	08/05/20 SMI"

#include <sys/vnode.h>

#include <sys/os.h>

#include "../version.h"
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/fs_base_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// finial pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

// Initialize static boolean used to communicate NLM status
bool fs_base_ii::is_external_nlm_event = false;

//
// Constructor.
//
fs_base_ii::fs_base_ii(uint_t size) :
	_DList::ListElem(this), allfobj_buckets(size),
	_suspend_locking_ckpts(false)
{
	is_primary = true;
	do_delete_cnt = 0;

	allfobj_list = new fobj_ii_list_t[allfobj_buckets];
	fobjlist_locks = new os::rwlock_t[allfobj_buckets];
}

//
// Destructor.
//
fs_base_ii::~fs_base_ii()
{
	delete [] allfobj_list;
	delete [] fobjlist_locks;
}

//
// Add the fobj to the allfobj_list[].
// This should only be called on the secondary.
//
bool
fs_base_ii::add_to_list(fobj_ii *fobjp)
{
	vnode_t		*vnodep = fobjp->get_vp();
	fobj_ii_list_t	&hbkt = allfobj_list[vp_to_bucket(vnodep)];
	fobj_ii		*fobj_iip;

	// Check to be sure there are no duplicates.
	fobj_ii_list_t::ListIterator iter(hbkt);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (VN_CMP(vnodep, fobj_iip->get_vp())) {	//lint !e666
			return (false);
		}
	}

	// Add fobj to the list of all fobj's.
	hbkt.prepend(fobjp);
	return (true);
}

//
// Helper function to remove an fobj from
// the fidlist. The fobj moves from the
// fidlist to the allfobj_list during
// FID conversion. (See also
// fobj_ii::add_to_list())
//
void
fs_base_ii::remove_from_fidlist(fobj_ii *fobjp)
{
	ASSERT(fidlock.write_held());
	(void) fidlist.erase(fobjp);
}

//
// remove_fobj_locked
// Search the vnode hash table or fid list for the given vnode pointer
// and remove it (it should always be found).
// This should only be called with the lock held.
//
void
fs_base_ii::remove_fobj_locked(fobj_ii *fobjp, release_info_t &rinfo)
{
	if (fobjp->is_released()) {
		return;
	}

	if (!fobjp->is_active()) {
		//
		// There is no valid vnode for this object.
		// One of the following is true:
		// 1. this is a secondary,
		// 2. primary had an error converting from secondary, or
		// 3. primary never converted
		//	(lazy fid conversion never happened).
		//
		ASSERT(fidlock.write_held());
		bool	removed = fidlist.erase(fobjp);
		ASSERT(removed);
		return;
	}

	vnode_t		*vnodep = fobjp->get_vp();
	ASSERT(vnodep != NULL);

	int		bktno = vp_to_bucket(vnodep);
	ASSERT(fobjlist_locks[bktno].write_held());

	fobj_ii		*fobj_iip;
	fobj_ii_list_t	&hbkt = allfobj_list[bktno];

	fobj_ii_list_t::ListIterator	iter(hbkt);
	while ((fobj_iip = iter.get_current()) != NULL) {
		iter.advance();
		if (fobj_iip == fobjp) {
			// Remove fobj from allfobj_list.
			(void) hbkt.erase(fobj_iip);

			// Get release info while holding bucket lock.
			fobj_iip->cleanup_and_get_release_info(rinfo);
			return;
		}
	}
	ASSERT(0);
}

//
// Methods used by fobj through fs_implp.
//
fs_dependent_impl *
fs_base_ii::get_fs_dep_implp() const
{
	ASSERT(0);
	return (NULL);
}

//
// Called at module _init() time. Return an errno.
//
int
fs_base_ii::startup()
{
	return (0);
}

//
// Static method used by the fs_collection object to communicate that
// lockd/statd have been terminated by a user generated signal for
// example.
//
void
fs_base_ii::notify_external_nlm_event(bool has_transitioned)
{
	is_external_nlm_event = has_transitioned;
}
