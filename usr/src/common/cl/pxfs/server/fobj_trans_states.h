//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FOBJ_TRANS_STATES_H
#define	FOBJ_TRANS_STATES_H

#pragma ident	"@(#)fobj_trans_states.h	1.7	08/05/20 SMI"

#include <h/replica.h>
#include <h/repl_dc.h>
#include <repl/service/replica_tmpl.h>

#include "../version.h"
#include <pxfs/server/fs_impl.h>

//
// This transaction object stores state pertaining to a UFS _FIOLFS ioctl being
// issued on a file.
//
class fobj_lockfs_state : public transaction_state {

public:

	fobj_lockfs_state(fs_repl_impl *fsp,
	    uint64_t lf_lock, uint64_t lf_flags,
	    uint64_t lf_key, const char *lf_comment);

	~fobj_lockfs_state();

	// Called when state is unreferenced because client is dead.
	void orphaned(Environment &);

	// Called when transaction is commited on the primary i.e. when
	// the primary calls add_commit(), or when the framework knows
	// that the invocation has completed on the primary.
	void committed();

	// Called when the operation fails on the primary and the
	// failure needs to be reported.
	void report_failure(sol::error_t err);

	// Accessor functions
	void get_args(struct lockfs *lockfsp, int *state);
	sol::error_t get_error();

	enum {
	    INITIAL = 1,
	    COMMITED = 2,
	    CANCELLED = 3,
	    ORPHANED = 4
	};

	// This static function is a helper function that registers
	// a 'fobj_lockfs_state' state object with the environment
	// variable passed in.
	static void register_new_state(fs_repl_impl *fsp, uint64_t lf_lock,
	    uint64_t lf_flags, uint64_t lf_key, const char *lf_comment,
	    Environment &env);

	// This static helper function calls the reprt_failure() method
	// on the state object registered with the environment
	// variable passed in.
	static void report_failure(sol::error_t, Environment &);

	// If the environment variable passed in indicates that this
	// call is a retry, then this function returns the state
	// object, otherwise it returns NULL.
	static fobj_lockfs_state *retry(Environment &env);

private:

	fs_repl_impl *_fsp;
	uint64_t _lf_lock;
	uint64_t _lf_flags;
	uint64_t _lf_key;
	CORBA::String_var _lf_comment;

	int _state;
	sol::error_t _err;
};

//
// This state object is used to know if it is a
// retry or not. It doesn't store any other info.
//
class fobj_retry_state : public transaction_state {
public:
	fobj_retry_state() {};
	~fobj_retry_state() {};
	void orphaned(Environment &) {};
	void committed() {};
	static void register_new_state(Environment &env);
	static bool retry(Environment &env);
private:
	int _state;
};

#endif	/* FOBJ_TRANS_STATES_H */
