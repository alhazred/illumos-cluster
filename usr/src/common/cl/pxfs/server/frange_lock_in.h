//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)frange_lock_in.h	1.6	08/05/20 SMI"

inline bool
frange_lock::has_active_locks()
{
	return (!active_locks.empty());
}

//
// Helper function to lock the processed list.
//
inline void
frange_lock::lock_processed_list()
{
	processed_list_lock.lock();
}

//
// Helper function to unlock the processed list.
//
inline void
frange_lock::unlock_processed_list()
{
	processed_list_lock.unlock();
}

//
// Helper function to add a lock to the processed list.
//
inline void
frange_lock::add_to_processed_list(lock_descriptor_t *lock)
{
	ASSERT(processed_list_lock.lock_held());
	processed_list.prepend(lock);
}

//
// Helper function to remove a lock from the processed list.
//
inline bool
frange_lock::remove_from_processed_list(lock_descriptor_t *lock)
{
	bool found;

	processed_list_lock.lock();
	found = processed_list.erase(lock);
	processed_list_lock.unlock();

	return (found);
}
