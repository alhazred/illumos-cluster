//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef HSFS_DEPENDENT_IMPL_H
#define	HSFS_DEPENDENT_IMPL_H

#pragma ident	"@(#)hsfs_dependent_impl.h	1.7	08/05/20 SMI"

#include "../version.h"
#include <pxfs/server/fs_dependent_impl.h>

//
// Definition of the hsfs-specific class that does any required special handling
// for hsfs files.
//
class hsfs_dependent_impl : public fs_dependent_impl {

public:

	// Constructor.
	hsfs_dependent_impl(fs_ii *);

	// Destructor.
	~hsfs_dependent_impl();

	int fs_alloc_data(vnode_t *vp, u_offset_t offset, size_t *len,
	    fdbuffer_t *fdb, int flags, cred_t *cr);

	int fs_rdwr_data(vnode_t *vp, u_offset_t offset, size_t len,
	    size_t client_file_size, fdbuffer_t *fdb, int flags, cred_t *cr);
};

#endif	// HSFS_DEPENDENT_IMPL_H
