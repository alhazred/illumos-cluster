//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_pxfs_server_in.h	1.4	08/05/20 SMI"

//
// repl_pxfs_server inline methods
//

//
// Mark this file system as unmounted.
//
inline void
repl_pxfs_server::mark_fs_unmounted()
{
	fs_is_unmounted = true;
}

//
// Tell whether this file system has been unmounted
//
inline bool
repl_pxfs_server::is_fs_unmounted()
{
	return (fs_is_unmounted);
}

//
// Decrement the count of invocations this server has issued. If the
// count becomes zero, signal any threads waiting.
//
inline void
repl_pxfs_server::decrement_invo_count()
{
	active_invo_lock.lock();

	ASSERT(active_invo_count > 0);
	if (--active_invo_count == 0) {
		active_invo_cv.signal();
	}

	active_invo_lock.unlock();
}
