/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PROV_COMMON_IN_H
#define	_PROV_COMMON_IN_H

#pragma ident	"@(#)prov_common_in.h	1.6	08/05/20 SMI"

//
// Return a new reference to the cache object
// (needs a CORBA::release()).
//
inline CORBA::Object_ptr
prov_common::get_cacheref()
{
	return (CORBA::Object::_duplicate(cache_obj));
}

//
// Return a CORBA pointer to the cache object
// without making a new reference (doesn't need a CORBA::release()).
//
inline CORBA::Object_ptr
prov_common::get_cacheptr() const
{
	return (cache_obj);
}

inline void
prov_common_set::lock_provider_list()
{
	list_lock.wrlock();
}

inline void
prov_common_set::unlock_provider_list()
{
	list_lock.unlock();
}

inline int
prov_common_set::provider_list_lock_held()
{
	return (list_lock.lock_held());
}

template<class Prov> inline
prov_set<Prov>::prov_set()
{
}

template<class Prov> inline
prov_set<Prov>::~prov_set()
{
}

template<class Prov> inline Prov *
prov_set<Prov>::insert_locked(Prov *provp)
{
	return ((Prov *)prov_common_set::insert_locked(provp));
}

template<class Prov> inline Prov *
prov_set<Prov>::reapfirst()
{
	return ((Prov *)prov_common_set::reapfirst());
}

template<class Prov> inline
prov_iter<Prov>::prov_iter(prov_set<Prov> &set) :
	prov_common_iter(set)
{
}

template<class Prov> inline
prov_iter<Prov>::~prov_iter()
{
}

template<class Prov> inline Prov *
prov_iter<Prov>::nextp()
{
	return ((Prov *)prov_common_iter::nextp());
}

#endif	/* _PROV_COMMON_IN_H */
