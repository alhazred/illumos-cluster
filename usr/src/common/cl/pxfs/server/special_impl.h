//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// special_impl.h - Implementation class for the special interface defined
// in interfaces/pxfs.idl
//

#ifndef SPECIAL_IMPL_H
#define	SPECIAL_IMPL_H

#pragma ident	"@(#)special_impl.h	1.7	08/05/20 SMI"

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/server/fobj_impl.h>

//
// Declaration of internal implementation object.
//
class special_ii : public fobj_ii {
public:
	virtual ~special_ii();

protected:
	// Unreplicated or primary constructor for the internal implementation
	special_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation
	special_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_type_t ftype);

	void fsync(int32_t syncflag, solobj::cred_ptr credobj,
	    Environment &_environment);
	//
};


//
// Unreplicated CORBA implementation of special
//
class special_norm_impl :
	public McServerof<PXFS_VER::special>,
	public special_ii
{
public:
	special_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);
	~special_norm_impl();

	virtual void		_unreferenced(unref_t arg);

	// Return a new fobj CORBA reference to this
	virtual PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t	get_fobj_type(Environment &env);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	/* special */

	virtual void	fsync(int32_t syncflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	// End of methods supporting IDL operations
};

//
// Replicated CORBA implementation of special
//
class special_repl_impl :
	public mc_replica_of<PXFS_VER::special>,
	public special_ii {
public:
	// Primary constructor
	special_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);

	// Secondary constructor
	special_repl_impl(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
		PXFS_VER::special_ptr obj);

	~special_repl_impl();

	virtual void		_unreferenced(unref_t arg);

	// Return a new PXFS_VER::fobj CORBA reference to this
	virtual PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	// Return a CORBA pointer to the checkpoint interface
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t	get_fobj_type(Environment &env);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	fsync(int32_t syncflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	// End of methods supporting IDL operations
};

#include <pxfs/server/special_impl_in.h>

#endif	// SPECIAL_IMPL_H
