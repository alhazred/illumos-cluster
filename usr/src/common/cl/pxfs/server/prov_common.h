/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef PROV_COMMON_H
#define	PROV_COMMON_H

#pragma ident	"@(#)prov_common.h	1.7	08/05/20 SMI"

//
// The prov_common class is intended to be used as a base class for
// all PXFS cache provider implementations. The basic functions are
// keeping a list of the providers and handle locking.
// XXX This should be replaced with generic list code.
//

#include <sys/os.h>
#include <sys/list_def.h>
#include <orb/invo/common.h>

#include "../version.h"

// Forward declarations.
class prov_common_iter;

//
// This should be used as a base class for provider objects.
//
class prov_common {
	friend class prov_common_set;
	friend class prov_common_iter;

public:
	prov_common(CORBA::Object_ptr obj);
	virtual ~prov_common();

	// Return a new reference to this object (needs a CORBA::release()).
	virtual CORBA::Object_ptr get_provref() = 0;

	//
	// Return a new reference to the cache object
	// (needs a CORBA::release()).
	//
	CORBA::Object_ptr get_cacheref();

	//
	// Return a CORBA pointer to the cache object
	// without making a new reference (doesn't need a CORBA::release()).
	//
	CORBA::Object_ptr get_cacheptr() const;

private:
	prov_common *nextp;		// linked list of all providers

	CORBA::Object_var cache_obj;	// cache object for this provider

	// Disallowed operations.
	prov_common &operator = (const prov_common &);
	prov_common(const prov_common &);
};

//
// This is the base class for lists of providers.
// The list elements should be derived from the prov_common class above.
//
class prov_common_set {
	friend class prov_common_iter;

public:
	prov_common_set();
	~prov_common_set();

	// Lock routines.
	void lock_provider_list();	// lock the provider list
	void unlock_provider_list();	// unlock the provider list
	int provider_list_lock_held();	// true if lock is held

	//
	// If there is an existing entry, return a pointer to it and don't
	// insert 'provp'. Otherwise, insert 'provp' into the list and
	// return NULL.
	//
	prov_common *insert_locked(prov_common *provp);

	// Remove provider from the list.
	void remove_locked(prov_common *provp);

	//
	// Remove the first provider in the list and return it or return
	// NULL.
	//
	prov_common *reapfirst();

	// Return true if list is empty while locked.
	bool is_empty_locked();

private:
	// The list lock needs to be a rwlock_t so we can send checkpoints.
	os::rwlock_t list_lock;
	prov_common *firstp;		// first provider in list
	prov_common_iter *iter_list;	// list of active iterators

	// Disallowed operations.
	prov_common_set &operator = (const prov_common_set &);
	prov_common_set(const prov_common_set &);
};

//
// This is the base class for iterating through a list of providers.
//
class prov_common_iter {
	friend prov_common_set;

public:
	prov_common_iter(prov_common_set &_prov_set);
	~prov_common_iter();

	// Return a pointer to the next provider in the list or NULL.
	prov_common *nextp();

	// Return a CORBA reference to the next provider in the list or nil.
	CORBA::Object_ptr nextref();

private:
	prov_common_set &pset;		// reference to the set we are iterating
	prov_common *next_provp;	// pointer to the "next" provider
	prov_common_iter *next_iterp;	// list of all iterators

	// Disallowed operations.
	prov_common_iter &operator = (const prov_common_iter &);
	prov_common_iter(const prov_common_iter &);
};

//
// A template is needed to make the cast of the return value type-safe.
//
template<class Prov> class prov_set : public prov_common_set {
public:
	prov_set();
	~prov_set();

	//
	// If there is an existing entry, return a pointer to it and
	// don't insert 'provp'. Otherwise, insert 'provp' at the head of the
	// list and return NULL.
	//
	Prov *insert_locked(Prov *provp);

	//
	// Remove the first provider in the list and return it or return
	// NULL.
	//
	Prov *reapfirst();

private:
	// Disallowed operations.
	prov_set &operator = (const prov_set &);
	prov_set(const prov_set &);
};

//
// This is used to iterate through the list of provider objects.
// Note that the nextp() function does not return a CORBA reference to
// the provider object so this should only be used when _unreferenced()
// is blocked by the HA framework such as dumping state to a new secondary.
//
template<class Prov> class prov_iter : public prov_common_iter {
public:
	prov_iter(prov_set<Prov> &set);
	~prov_iter();

	// Return a pointer to the next provider in the list or NULL.
	Prov *nextp();

private:
	// Disallowed operations.
	prov_iter &operator = (const prov_iter &);
	prov_iter(const prov_iter &);
};

#include <pxfs/server/prov_common_in.h>

#endif	/* PROV_COMMON_H */
