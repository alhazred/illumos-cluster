//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_pxfs_server.cc	1.22	09/03/03 SMI"

#include <sys/pathname.h>
#include <sys/fcntl.h>
#include <sys/mount.h>
#include <sys/dnlc.h>

#include <sys/sol_conv.h>
#include <nslib/ns.h>
#include <solobj/solobj_impl.h>
#include <h/repl_pxfs.h>

#include "../version.h"
#include <pxfs/mount/mount_debug.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fsmgr_server_impl.h>
#include <pxfs/server/unixdir_impl.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/symlink_impl.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/server/unixdir_ckpt.h>
#include <pxfs/server/fobj_trans_states.h>

#ifndef VXFS_DISABLED
#include <pxfs/server/vxfs_dependent_impl.h>
#endif

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//
// This struct is used for building a table for supporting the mapping
// from version protocol spec file version number to the various IDL
// interface versions it represents.  The table will be a two dimensional
// array indexed by major/minor vp version.
//
typedef struct {		// idl interfaces
	int	fs;		// pxfs_v1.idl
	int	fs_ckpt;	// repl_pxfs_v1.idl
} pxfs_ver_map_t;

//
// These are the current maximum indexes used for accessing the vp to idl
// version table.
//
const int	PXFS_VP_MAX_MAJOR = 2;
const int	PXFS_VP_MAX_MINOR = 2;

//
// pxfs_ver_map_t is a struct type which has has an entries for each IDL
// interface which is being versioned. For a given VP major and minor version,
// we get the IDL version of those interfaces.
//
// Note: this corresponds to the versions supported by the old
// version of pxfs in pxfs_v1.idl and repl_pxfs_v1.idl
//
pxfs_ver_map_t pxfs_vp_to_idl[PXFS_VP_MAX_MAJOR + 1][PXFS_VP_MAX_MINOR +1] = {
	{   { 0, 0 },		// VP Version 0.0 defined for indexing
	    { 0, 0 },		// VP Version 0.1   "      "     "
	    { 0, 0 }  },	// VP Version 0.2   "      "     "
	{   { 0, 0 },		// VP Version 1.0
	    { 0, 0 },		// VP Version 1.1
	    { 0, 0 }  },	// VP Version 1.2
	{   { 0, 0 },		// VP Version 2.0 Object Consolidation
	    { 3, 3 },		// VP Version 2.1 RU support for 6496894/6493901
	    { 3, 4 }  }		// VP Version 2.2 RU support for 6785071
};

//
// If the PxFS version is incremented, the values below should be
// modified to reflect the highest supported version which will be
// used to register callbacks.
//
const int current_pxfs_major_version = 2;
const int current_pxfs_minor_version = 2;

// Initialize the static member variable unique_integer
int repl_pxfs_server::unique_integer = 0;

//
// Class repl_pxfs_server.
//

//
// Create a repl_pxfs_server object.
//
repl_pxfs_server::repl_pxfs_server(vnode_t *mvp, const sol::mounta &ma,
    cred_t *cr, const char *id) :
	mountdata(ma),
	repl_server<REPL_PXFS_VER::fs_replica>(ma.spec, id),
	_ckpt_proxy(NULL),
	replica_state(NOT_PRIMARY),
	active_invo_count(0)
{
	ASSERT(mountdata.flags & MS_SYSSPACE);
	ASSERT((mountdata.flags & MS_REMOUNT) == 0);

	//
	// We set MS_NOSPLICE so that the underlying file system isn't
	// linked into the file system name space.
	// XXX We also force the MS_OVERLAY flag on to suppress the
	// mvp->v_count == 1 EBUSY check in the file system code.
	//
	mountdata.flags |= MS_NOSPLICE | MS_OVERLAY;

	mnt_vp = mvp;
	VN_HOLD(mnt_vp);

	crp = cr;
	crhold(crp);

	fsp = NULL;
	mnt_error = 0;
	fs_is_unmounted = false;

	//
	// Initialize with invalid versions. Correct values will
	// be obtained by a query of the version manager.
	//
	current_version.major_num = 0;
	current_version.minor_num = 0;
	pending_version.major_num = 0;
	pending_version.minor_num = 0;
}

repl_pxfs_server::~repl_pxfs_server()
{
	if (mnt_vp != NULL) {
		VN_RELE(mnt_vp);
	}
	crfree(crp);

	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
} //lint !e1540 !e1740 pointers are neither freed nor zero'ed by destructor

REPL_PXFS_VER::fs_replica_ptr
repl_pxfs_server::get_checkpoint_fs_replica()
{
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	return (_ckpt_proxy);
}

//
// Helper function to get the mount error (if any).
//
int
repl_pxfs_server::get_mount_error() const
{
	return (mnt_error);
}

//
// Become the primary.
// Note that previously we might have been newly created or
// a secondary that is switching to primary.
//
void
repl_pxfs_server::become_primary(const replica::repl_name_seq &,
    Environment &_environment)
{
	// Show that we are the primary
	active_invo_lock.lock();
	replica_state = PRIMARY;
	active_invo_lock.unlock();

	ASSERT(mountdata.flags & MS_SYSSPACE);
	ASSERT((mountdata.flags & MS_REMOUNT) == 0);
	ASSERT(mountdata.flags & MS_OVERLAY);
	ASSERT(mountdata.flags & MS_NOSPLICE);

	// First, initialize the checkpoint proxy.
	version_lock.wrlock();

	// Callback may have occured when this replica was a secondary
	if (pending_version.major_num != 0) {
		current_version = pending_version;
	}

	// Save the current _ckpt_proxy and release it after we get a new one
	REPL_PXFS_VER::fs_replica_ptr old_ckpt_p = _ckpt_proxy;

	CORBA::type_info_t *typ = REPL_PXFS_VER::fs_replica::_get_type_info(
	    pxfs_vp_to_idl[current_version.major_num]
		[current_version.minor_num].fs_ckpt);
	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = REPL_PXFS_VER::fs_replica::_narrow(tmp_ckpt_v);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the old ckpt_proxy.
	CORBA::release(old_ckpt_p);
	old_ckpt_p = REPL_PXFS_VER::fs_replica::_nil();

	//
	// If we were a secondary, there may be an uprocessed upgrade
	// callback pending.
	//
	if (fsp != NULL) {
		if (pending_version.major_num != 0) {
			pending_version.major_num = 0;

			// Update the server reference.
			typ = PXFS_VER::filesystem::_get_type_info(
			    pxfs_vp_to_idl[current_version.major_num]
				[current_version.minor_num].fs);
			fs_v = fsp->get_objref(typ);

			// Checkpoint current version.
			_ckpt_proxy->ckpt_service_version(
			    current_version.major_num,
			    current_version.minor_num, _environment);
		}
	}

	//
	// If a switchover and unmount happen simultaneously, the two
	// threads can race each other. The unmount thread asks the clients
	// (including the secondary) to unmount and clear their respective
	// vfs pointers. At this point the file system is not dead. The
	// switchover can proceed and try to make the existing secondary a
	// primary. This secondary's vfs has been cleared by the unmount
	// and we will panic.
	//
	// We resolve this race using the 'fs_is_unmounted' flag. If it is
	// set, we have commenced unmounting or has partly unmounted the
	// filesystem. In that case we return after logging an error. The
	// switchover will fail. The swithover will have to be retried.
	//
	if (fs_is_unmounted) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("repl_pxfs_server::become_primary %s already unmounted\n",
		    (const char *)mountdata.dir)); //lint !e1776
		_environment.clear();

		char		nodenum[32];

		(void) sprintf(nodenum, "Node (%u)", orb_conf::node_number());
		os::sc_syslog_msg msg(SC_SYSLOG_GLOBAL_MOUNT_TAG,
		    nodenum, NULL);
		//
		// SCMSGS
		// @explanation
		// This is an error due to a simultaneous switchover and
		// unmount. The switchover fails and unmount succeeds.
		// @user_action
		// The switchover has to be retried.
		//
		msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "switchover failed since the file system at %s is "
		    "being unmounted.", (const char *)mountdata.dir);
		version_lock.unlock();
		return;
	}

	if (mnt_error != 0) {
		ASSERT(fsp != NULL);
		ASSERT(fsp->get_vfsp() == NULL);
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("repl_pxfs_server::become_primary %s err %d\n",
		    (const char *)mountdata.dir, mnt_error));
		//
		// We have already tried to mount the file system and
		// got an error or some other unrecoverable situation.
		// Wait until we are unmounted and mounted again.
		//
		version_lock.unlock();
		return;
	}

	//
	// Set the MS_NOCHECK flag if this is a failover or switchover
	// (i.e., the PXFS file system is already mounted). This flag is used
	// to tell the underlying file system to suppress checking for other
	// mounted file systems with the same device since otherwise it would
	// see the PXFS proxy and think the device is already mounted.
	//
	if (fsp != NULL) {
		//
		// Check if we are being called after a failure of a call to
		// become_secondary().
		//
		if (fsp->get_vfsp() != NULL) {
			mnt_error = fsp->convert_to_primary(false);
			if (mnt_error != 0) {
				//
				// SCMSGS
				// @explanation
				// The file system specified in the message
				// could not be hosted on the node the message
				// came from.
				// @user_action
				// Check /var/adm/messages to make sure there
				// were no device errors. If not, contact your
				// authorized Sun service provider to
				// determine whether a workaround or patch is
				// available.
				//
				(void) fsp->msg().log(SC_SYSLOG_WARNING,
				    MESSAGE, "Switchover (%s) error (%d) after "
				    "failure to become secondary",
				    (const char *)(mountdata.dir), mnt_error);
				fsp->get_checkpoint()->ckpt_mnt_error(mnt_error,
				    _environment);
				ASSERT(_environment.exception() == NULL);
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_REPLICA,
				    MOUNT_RED,
				    ("repl_pxfs_server::become_primary %s "
				    "become_secondary err %d\n",
				    (const char *)mountdata.dir, mnt_error));
			}
			version_lock.unlock();
			return;
		}

		mountdata.flags |= MS_NOCHECK;

		//
		// Run the user-level commands needed to prepare the device
		// we are going to mount (does the fsck if necessary).
		//
		char name[20];
		Environment e;
		naming::naming_context_var ctxp = ns::root_nameserver();
		os::sprintf(name, "ha_mounter.%d", orb_conf::node_number());
		CORBA::Object_var obj = ctxp->resolve(name, e);
		if (e.exception()) {
			e.clear();
			//
			// SCMSGS
			// @explanation
			// The file system specified in the message could not
			// be hosted on the node the message came from. Check
			// to see if the user program "clexecd" is running on
			// that node.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) fsp->msg().log(SC_SYSLOG_WARNING, MESSAGE,
			    "Switchover error (%s): cannot "
			    "find clexecd", (const char *)(mountdata.dir));
			mnt_error = EINVAL;
			fsp->get_checkpoint()->ckpt_mnt_error(mnt_error,
			    _environment);
			ASSERT(_environment.exception() == NULL);
			version_lock.unlock();
			return;
		}
		repl_pxfs::ha_mounter_var	mounter =
		    repl_pxfs::ha_mounter::_narrow(obj);
		ASSERT(!CORBA::is_nil(mounter));
		mounter->mount(mountdata.spec, mountdata.fstype,
		    fsp->get_options(), e);
		if (e.exception()) {
			e.clear();
			//
			// SCMSGS
			// @explanation
			// The file system specified in the message could not
			// be hosted on the node the message came from because
			// an fsck on the file system revealed errors.
			// @user_action
			// Unmount the PXFS file system (if mounted), fsck the
			// device, and then mount the PXFS file system again.
			//
			(void) fsp->msg().log(SC_SYSLOG_WARNING, MESSAGE,
			    "Switchover error (%s): failed to fsck disk",
			    (const char *)(mountdata.dir));
			mnt_error = EINVAL;
			fsp->get_checkpoint()->ckpt_mnt_error(mnt_error,
			    _environment);
			ASSERT(_environment.exception() == NULL);
			version_lock.unlock();
			return;
		}
	} else {
		mountdata.flags &= ~MS_NOCHECK;

		//
		// We turn off MS_GLOBAL, as we are mounting the underlying
		// filesystem locally. With Solaris 9 build 58, Solaris
		// disables mount in progress checks if MS_GLOBAL is specified.
		// We have to make sure that MS_GLOBAL is turned off here, as we
		// want these checks to be made here. These checks make sure
		// that if a global mount and a local mount happen
		// concurrently, and are trying to mount the same device,
		// on different mount-points, only one of them succeeds.
		//
		mountdata.flags &= ~MS_GLOBAL;
	}

	//
	// We could end up with mnt_vp == NULL in cases where this
	// repl_pxfs_server was just being added to the repl_prov_list
	// and a reconfig triggered by a failure could come in and
	// shut this down (refer to rm_state_machine::cleanup_providers()).
	//
	// Hence, we re-initialize it if necessary.
	//
	if (mnt_vp == NULL) {
		vnode_t *vp = NULL;
		int	error = lookupname(((sol::mounta &)mountdata).dir,
		    UIO_SYSSPACE, FOLLOW, NULL, &vp);

		if (error != 0) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_REPLICA,
			    MOUNT_RED,
			    ("repl_pxfs_server::become_primary %s err %d"
			    "lookupname() failed\n",
			    (const char *)mountdata.dir, error));
			pxfslib::throw_exception(_environment, error);
			return;
		}
		mnt_vp = vp;
		VN_HOLD(mnt_vp);
	}

	dnlc_purge_vp(mnt_vp);

	int datalen;

#ifndef VXFS_DISABLED
	if (strcmp(mountdata.fstype, "vxfs") == 0) {
		datalen = vxfs_dependent_impl::vxfs_fixup_args(mountdata,
		    (fsp == NULL) ? vxfs_dependent_impl::VX_MOUNT :
				    vxfs_dependent_impl::VX_FAILOVER);
		if (datalen == -1) {
			pxfslib::throw_exception(_environment, ENOENT);
			return;
		}
	} else {
		datalen = (int)mountdata.data.length();
	}
#else
	datalen = (int)mountdata.data.length();
#endif

	//
	// Mount the underlying file system but don't link it into the
	// name space.
	//
	vfs_t *vfsp = NULL;
	struct mounta mnta;
	char *options;
	mnta.spec = mountdata.spec;
	mnta.dir = mountdata.dir;
	mnta.flags = mountdata.flags;
	mnta.fstype = mountdata.fstype;
	mnta.dataptr = (char *)mountdata.data.buffer();
	mnta.datalen = datalen;
	int len;
	if (mnta.flags & MS_OPTIONSTR) {
		len = (int)mountdata.options.length();
		//lint -e571 This is ok to loose the sign in this cast.
		options = new char [(size_t)len];
		//lint +e571
		mnta.optptr = os::strcpy(options,
		    (const char *)mountdata.options.buffer());
		mnta.optlen = len;
		//
		// Strip "global" from the options list,
		// if it happens to be specified.
		// This is because the underlying mount
		// is a local mount.
		//
		(void) pxfslib::exists_mntopt(options, "global", true);
	} else {
		len = MAX_MNTOPT_STR;
		//lint -e571 This is ok to loose the sign in this cast.
		options = new char [(size_t)len];
		//lint +e571
		mnta.optptr = NULL;
		mnta.optlen = 0;
	}
#ifdef _FAULT_INJECTION
	void *f_argp;
	uint32_t f_argsize;
	if (fault_triggered(FAULTNUM_PXFS_DOMOUNT, &f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		mnt_error = *((int *)f_argp);
	} else
#endif
		mnt_error = domount(mnta.fstype, &mnta, mnt_vp, crp, &vfsp);
	if (mnt_error == 0 && (mnta.flags & MS_OPTIONSTR) == 0) {
		mnt_error = vfs_buildoptionstr(&vfsp->vfs_mntopts, options,
		    len);
	}
	if (!mnt_error) {
		(void) pxfslib::exists_mntopt(options, "noglobal", true);
		(void) strcat(options, ",global");	//lint !e668
	}
	if (mnt_error != 0) {
		delete [] options;
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("repl_pxfs_server::become_primary %s domount err %d\n",
		    (const char *)mountdata.dir, mnt_error));
		//
		// Create a "dead" file system object just to return
		// the error from domount().
		//
		if (fsp == NULL) {
			fsp = new fs_repl_impl(NULL, mountdata.fstype,
			    mountdata.spec, NULL, this);
			typ = PXFS_VER::filesystem::_get_type_info(
			    pxfs_vp_to_idl[current_version.major_num]
				[current_version.minor_num].fs);
			fs_v = fsp->get_objref(typ);
			fsp->get_checkpoint()->ckpt_new_fsobj(fs_v, NULL,
			    _environment);
			ASSERT(_environment.exception() == NULL);
		} else {
			//
			// We failed while re-mounting the FS after a
			// switchover/ failover.  Syslog this fact.
			//

			//
			// SCMSGS
			// @explanation
			// The file system specified in the message could not
			// be hosted on the node the message came from.
			// @user_action
			// Check /var/adm/messages to make sure there were no
			// device errors. If not, contact your authorized Sun
			// service provider to determine whether a workaround
			// or patch is available.
			//
			(void) fsp->msg().log(SC_SYSLOG_WARNING, MESSAGE,
			    "Switchover error (%s): failed to mount FS (%d)",
			    (const char *)(mountdata.dir), mnt_error);
		}
		fsp->get_checkpoint()->ckpt_mnt_error(mnt_error, _environment);
		ASSERT(_environment.exception() == NULL);
		version_lock.unlock();
		return;
	}

	//
	// If this is the first time, we need to create the root fs object.
	// Locking is handled by the replica manager framework.
	//
	bool firsttime;
	if (fsp == NULL) {
		fsp = new fs_repl_impl(vfsp, mountdata.fstype,
		    mountdata.spec, options, this);
		typ = PXFS_VER::filesystem::_get_type_info(
		    pxfs_vp_to_idl[current_version.major_num]
			[current_version.minor_num].fs);
		fs_v = fsp->get_objref(typ);
		fsp->get_checkpoint()->ckpt_new_fsobj(fs_v, options,
		    _environment);
		ASSERT(_environment.exception() == NULL);
		firsttime = true;
	} else {
		fsp->set_vfsp(vfsp);
		firsttime = false;
	}
	delete [] options;

	mnt_error = fsp->convert_to_primary(firsttime);
	if (mnt_error != 0) {
		if (!firsttime) {
			//
			// We failed while re-mounting the FS after a
			// switchover/ failover.  Syslog this fact.
			//

			//
			// SCMSGS
			// @explanation
			// The file system specified in the message could not
			// be hosted on the node the message came from.
			// @user_action
			// Check /var/adm/messages to make sure there were no
			// device errors. If not, contact your authorized Sun
			// service provider to determine whether a workaround
			// or patch is available.
			//
			(void) fsp->msg().log(SC_SYSLOG_WARNING, MESSAGE,
			    "Switchover (%s) error (%d) converting to primary",
			    (const char *)(mountdata.dir), mnt_error);
		}
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("repl_pxfs_server::become_primary %s convert_to_primary "
		    "err %d\n", (const char *)mountdata.dir, mnt_error));
		fsp->get_checkpoint()->ckpt_mnt_error(mnt_error, _environment);
		ASSERT(_environment.exception() == NULL);
	}
	version_lock.unlock();
}

//
// Become the secondary.
// This is called on the primary in order to do a switchover.
//
void
repl_pxfs_server::become_secondary(Environment &_environment)
{
	ASSERT(fsp != NULL);

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::become_secondary %s\n",
	    (const char *)mountdata.dir));

	// Show that we are not the primary
	active_invo_lock.lock();
	replica_state = NOT_PRIMARY;
	active_invo_lock.unlock();

	if (fsp->convert_to_secondary() != 0) {
		//
		// If there is an error converting from primary to secondary,
		// we raise an exception to the HA framework notifying this
		// replica can't become a secondary. The HA framework will
		// call become_primary() on this replica and we will mark
		// the file system as "dead". We handle errors this way
		// because the HA framework doesn't support checkpoints
		// in become_secondary() and Solaris 2.7 doesn't support
		// forced unmount. With forced unmount, we could be sure
		// the file system wasn't accessing the disk on this node
		// and allow the switchover to proceed.
		//
		_environment.exception(new replica::become_secondary_failed);
	}

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the multi_ckpt_handler.
	version_lock.wrlock();
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
	version_lock.unlock();
}

void
repl_pxfs_server::add_secondary(replica::checkpoint_ptr sec_chkpt,
    const char *, Environment &_environment)
{
	REPL_PXFS_VER::fs_replica_var ckpt =
		REPL_PXFS_VER::fs_replica::_narrow(sec_chkpt);
	ASSERT(!CORBA::is_nil(ckpt));

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::add_secondary %s\n",
	    (const char *)mountdata.dir));
	if (fsp != NULL) {
		// Create a new fs on the secondary before we dump our state.
		ckpt->ckpt_new_fsobj(fs_v, fsp->get_options(), _environment);
		ASSERT(_environment.exception() == NULL);

		// Dump current state to the new secondary.
		if (mnt_error != 0) {
			ckpt->ckpt_mnt_error(mnt_error, _environment);
			ASSERT(_environment.exception() == NULL);
		}

		fsp->dump_state(ckpt, _environment);
		_environment.clear();
	}
}

void
repl_pxfs_server::remove_secondary(const char *, Environment &)
{
}

//
// This routine is called by server methods that issues invocations to
// clients which result in the client(s) issuing invocations back to
// the server.
//
// We count the invocations that we allow to proceed.
//
// When freezing the primary, we hold these invocations here.
//
// When frozen, we return the invocation with an exception.
//
bool
repl_pxfs_server::check_freeze(Environment &env)
{
	active_invo_lock.lock();

	switch (replica_state) {

	case NOT_PRIMARY:	// This should not be possible
		ASSERT(0);
		// Fall through and pretend that we are the primary.

	case PRIMARY:	// We are not preparing for a freeze.
			// Allow the invocation to proceed

		ASSERT(active_invo_count >= 0);
		ASSERT(active_invo_count != INT_MAX);

		active_invo_count++;
		active_invo_lock.unlock();

		return (false);
			//
	case FREEZING:	// Block any invocation that could result
			// in invocations back to this primary.
			// That would result in deadlock.
			// The clients currently allow invocations to
			// proceed to the server. So hold on to the
			// invocation until the client is ready to
			// block the invocation.
			//
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_AMBER,
		    ("repl_pxfs_server:(%p) freezing invo from node %d\n",
		    this, env.get_src_node().ndid));
		while (replica_state == FREEZING) {
			active_invo_cv.wait(&active_invo_lock);
		}
		ASSERT(replica_state == FROZEN);
		// Fall through
	case FROZEN:
		env.system_exception(
		    CORBA::PRIMARY_FROZEN(0, CORBA::COMPLETED_NO));
		active_invo_lock.unlock();
		return (true);

	};
}

//
// This method is called by the HA framework when the service is about to
// be frozen. The freeze will proceed only when this method returns. We
// take advantage of this serialization to bring our state to a stable one
// by waiting for outstanding invocation from the server to complete and
// preventing any new invocations from being launched.
//
void
repl_pxfs_server::freeze_primary_prepare(Environment &)
{
	//
	// Hold invocation count lock to prevent new invocations from
	// coming in.
	//
	active_invo_lock.lock();
	replica_state = FREEZING;

	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_AMBER,
	    ("repl_pxfs_server(%p): %d outstanding nested server invocations\n",
	    this, active_invo_count));

	while (active_invo_count > 0) {
		// Wait for active invocations to signal completion.
		active_invo_cv.wait(&active_invo_lock);
	}

	active_invo_lock.unlock();
}

//
// Note: either add_secondary() and unfreeze_primary() or
// become_secondary() will be called after this returns.
// Invocations and calls to _unreferenced() will be blocked by the HA
// framework after we return from here.
//
// repl_pxfs_server(replica::repl_prov::freeze_primary, _environment)
void
repl_pxfs_server::freeze_primary(Environment &)
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::freeze_primary %s\n",
	    (const char *)mountdata.dir));

	// Wake up any invocations that were blocked during freezing
	active_invo_lock.lock();
	replica_state = FROZEN;
	active_invo_cv.broadcast();
	active_invo_lock.unlock();

	fsp->freeze_primary((const char *)mountdata.dir);
}

void
repl_pxfs_server::unfreeze_primary(Environment &)
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::unfreeze_primary %s\n",
	    (const char *)mountdata.dir));

	// Show that invocations are allowed
	active_invo_lock.lock();
	replica_state = PRIMARY;
	active_invo_lock.unlock();
}

void
repl_pxfs_server::become_spare(Environment &)
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::become_spare %s\n",
	    (const char *)mountdata.dir));
	if (fsp != NULL) {
		fs_v = PXFS_VER::filesystem::_nil();
		fsp->convert_to_spare();
		fsp = NULL;
	}
	mnt_error = 0;
}

//
// This is called on the primary when the service is requested to shutdown.
//
void
repl_pxfs_server::shutdown(Environment &_environment)
{
	// Unregister the upgrade callback.
	upgrade_callback_unregister();

	// Return busy if there was no unmount.
	if (mnt_error == 0 && fsp != NULL && fsp->get_vfsp() != NULL) {
		_environment.exception(new replica::service_busy);
		return;
	}
	fs_v = PXFS_VER::filesystem::_nil();
	if (mnt_vp != NULL) {
		VN_RELE(mnt_vp);
		mnt_vp = NULL;
	}
}

//
// This is called on the primary when the service is forced to shutdown.
//
// Virtual function forced_shutdown in base class generic_repl_server
// returns a Solaris error code (if forced shutdown not supported by
// specific HA service), else returns 0(Success)
//
uint32_t
repl_pxfs_server::forced_shutdown(Environment &)
{
	// Unregister the upgrade callback.
	upgrade_callback_unregister();

	fs_v = PXFS_VER::filesystem::_nil();
	if (mnt_vp != NULL) {
		VN_RELE(mnt_vp);
		mnt_vp = NULL;
	}
	return (0);
}

//
// This is called on a spare when the service is requested to shutdown.
//
void
repl_pxfs_server::shutdown_spare(replica::repl_prov_shutdown_type,
    Environment &)
{
	fs_v = PXFS_VER::filesystem::_nil();
	if (mnt_vp != NULL) {
		VN_RELE(mnt_vp);
		mnt_vp = NULL;
	}
}

CORBA::Object_ptr
repl_pxfs_server::get_root_obj(Environment &)
{
	ASSERT(fsp != NULL);
	version_lock.wrlock();
	CORBA::type_info_t *typ = PXFS_VER::filesystem::_get_type_info(
	    pxfs_vp_to_idl[current_version.major_num]
		[current_version.minor_num].fs);
	version_lock.unlock();
	return (fsp->get_objref(typ));
}

//
// Set the initial version number.
//
void
repl_pxfs_server::set_version(const version_manager::vp_version_t &v)
{
	//
	// We could have had a callback between the time that we called
	// register_upgrade_callbacks() and the time that it returned and
	// set tmp_version. The callback may have set a newer version than
	// v, so don't clobber it.
	//
	version_lock.wrlock();
	if (current_version.major_num < v.major_num ||
	    (current_version.major_num == v.major_num &&
	    current_version.minor_num < v.minor_num)) {
		current_version = v;
	}
	version_lock.unlock();
}

void
repl_pxfs_server::upgrade_callback_register(const sol::mounta &ma)
{
	char *service_name = os::strdup(ma.spec);
	char unique_callback_name[1024];
	char unique_str[32];
	char *vpname = "pxfs";
	version_manager::vp_version_t callback_limit;
	version_manager::vp_version_t cur_version;
	Environment e;

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::upgrade_callback_register: this = %p "
	    "mountpnt = %s\n", this, (const char *)ma.dir));

	//
	// Have the filesystem replica register with the Version Manager
	// for upgrade callbacks.  Pass a string which can be use to
	// build a unique callback name.  This will eliminate races when
	// unmounting and then mounting the same filesystem.
	//
	os::atomic_add_32((uint32_t *)&unique_integer, 1);
	(void) os::itoa(unique_integer, unique_str, 10);

	// Generate for callback registration
	os::sprintf(unique_callback_name, "%s", service_name);
	os::sprintf(unique_callback_name + os::strlen(service_name), "%s",
	    ":");
	os::sprintf(unique_callback_name + os::strlen(service_name) +1, "%s",
	    unique_str);

	// Get a pointer to the local version manager
	version_manager::vm_admin_var vmgr_v = vm_util::get_vm(NODEID_UNKNOWN);

	// Build a UCC for support of version upgrade callbacks
	version_manager::ucc_seq_t ucc_seq(1, 1);
	ucc_seq[0].ucc_name = os::strdup(unique_callback_name);
	ucc_seq[0].vp_name = os::strdup(vpname);
	version_manager::string_seq_t fseq(1, 1);
	fseq[0] = os::strdup(service_name);
	ucc_seq[0].freeze = fseq;

	//
	// Create a version upgrade callback object for the fs replica
	//
	replica::repl_prov_var repl_srvr_v = generic_repl_server::_narrow(
	    this->generic_repl_server::get_objref());
	repl_srvr_v->_handler()->set_cookie((void *)this);
	callback_object_v = (new fs_version_callback_impl(
	    replica::repl_prov::_duplicate(
		repl_srvr_v)))->get_objref();

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::upgrade_callback_reg: this = %p\n", this));

	//
	// Register the callback object with the Version Manager. The
	// tmp_version will be returned.  The version lock is not held
	// since the replica is not yet registered with the HA framework
	// so there cannot be a call to become_primary.
	//
	// If the running version is less than the callback_limit
	// a callback will be registerd, otherwise a callback is not
	// registered (currently no way to tell).  The current running
	// version is returned regardless.
	//
	callback_limit.major_num = current_pxfs_major_version;
	callback_limit.minor_num = current_pxfs_minor_version;
	vmgr_v->register_upgrade_callbacks(ucc_seq, callback_object_v,
	    callback_limit, cur_version, e);
	if (e.exception()) {
		callback_object_v = version_manager::upgrade_callback::_nil();
		e.exception()->print_exception("register_upgrade_callbacks:");
		e.clear();
		return;
	}
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::upgrade_callback_reg after reg: this = %p\n",
	    this));

	// establish the current version in the replica
	set_version(cur_version);
}

void
repl_pxfs_server::upgrade_callback_unregister()
{
	Environment e;

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::upgrade_callback_unregister: this = %p\n",
	    this));

	version_lock.wrlock();
	if (!CORBA::is_nil(callback_object_v)) {
		version_manager::vm_admin_var vmgr_v =
		    vm_util::get_vm(NODEID_UNKNOWN);
		vmgr_v->unregister_upgrade_callbacks(
		    callback_object_v, e);
		if (e.exception()) {
			ASSERT(0);
			e.clear();
		}
		callback_object_v = version_manager::upgrade_callback::_nil();
	}
	version_lock.unlock();
}

//
// Process an upgrade callback from the version manager.
// This call can happen before the HA replica is registered with the
// replica manager (i.e., the HA sevice is not started yet or is a spare),
// or as primary or secondary. The callback is not synchronized with
// calls to become_primary(), become_secondary(), etc. and failovers can
// happen while the service is frozen so we need to make sure that the
// "upgrade work" is done if there are node failures.
// If this replica is the primary, we do the work and send a checkpoint
// to indicate the work is complete. If this replica is a secondary and
// the callback happens before we get the checkpoint from the primary,
// we record that the callback happened so that become_primary() can
// do the "upgrade work" and send the checkpoint. If this replica is a
// secondary and the callback happens after the checkpoint is received,
// we ignore the callback. The checkpoint routine on the secondary clears
// the "flag" that the callback sets so no extra work is done in
// become_primary() if the old primary fails after completing the upgrade
// callback work.
//
void
repl_pxfs_server::upgrade_callback(const version_manager::vp_version_t &v,
    Environment &e)
{
	CORBA::type_info_t *typ;

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::upgrade_callback: this = %p "
	    "fsp = %p\n", this, fsp));

	//
	// A nil callback object indicates that a callback unregister was
	// done.
	//
	if (CORBA::is_nil(callback_object_v)) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_GREEN,
		    ("repl_pxfs_server::upgrade_callback: nil cb object\n"));
		return;
	}


	//
	// Note that upgrade callbacks are not synchronized with
	// calls to become_primary(), add_secondary(), etc.
	// Getting this lock makes sure the replica state doesn't change.
	//
	version_lock.wrlock();
	if (current_version.major_num > v.major_num ||
	    (current_version.major_num == v.major_num &&
	    current_version.minor_num >= v.minor_num)) {
		// Version isn't changing so just return.
		version_lock.unlock();
		callback_object_v = version_manager::upgrade_callback::_nil();
		return;
	}

	//
	// If this replica is not the primary, set the pending version
	// to be used if there is a failover before the primary checkpoints
	// a new version.
	//
	if (CORBA::is_nil(_ckpt_proxy)) {
		// If this replica is a secondary, set the pending version.
		if (fsp != NULL) {
			pending_version = v;
		} else {
			// Replica must be a spare.
			current_version = v;
		}
		version_lock.unlock();
		callback_object_v = version_manager::upgrade_callback::_nil();
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_GREEN,
		    ("repl_pxfs_server::upgrade_callback: not primary\n"));
		return;
	}

	//
	// Check for primary with a "dead" filesystem
	//
	if (fsp == NULL) {
		version_lock.unlock();
		callback_object_v = version_manager::upgrade_callback::_nil();
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_GREEN,
		    ("repl_pxfs_server::upgrade_callback: NULL fsp\n"));
		return;
	}

	// We are the primary and the version has changed.
	current_version = v;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("repl_pxfs_server::upgrade_callback: this = %p "
	    "new version ->  major =  %d minor = %d\n", this,
	    current_version.major_num, current_version.minor_num));

	//
	// Switch the checkpoint interface to the new protocol. Save
	// the current _ckpt_proxy and release it after we get a new one
	//
	REPL_PXFS_VER::fs_replica_ptr old_ckpt_p = _ckpt_proxy;
	typ = REPL_PXFS_VER::fs_replica::_get_type_info(
	    pxfs_vp_to_idl[current_version.major_num]
		[current_version.minor_num].fs_ckpt);

	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = REPL_PXFS_VER::fs_replica::_narrow(tmp_ckpt_v);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the old ckpt_proxy.
	CORBA::release(old_ckpt_p);
	old_ckpt_p = REPL_PXFS_VER::fs_replica::_nil();

	// Update the server reference.
	typ = PXFS_VER::filesystem::_get_type_info(
	    pxfs_vp_to_idl[current_version.major_num]
		[current_version.minor_num].fs);
	fs_v = fsp->get_objref(typ);

	//
	// Create and add a primary context so the provider can send
	// checkpoints while the service is frozen.
	// XXX change the primary_ctx::invoke_env type.
	//
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, e);

	// Checkpoint the current version number.
	_ckpt_proxy->ckpt_service_version(
	    current_version.major_num, current_version.minor_num, e);

	e.trans_ctxp = NULL;

	//
	// This reference isn't needed any more since we won't need
	// to do an unregister of the callback object.
	//
	callback_object_v = version_manager::upgrade_callback::_nil();

	version_lock.unlock();
}

//
// Checkpoint the creation of a new filesystem (fs_obj).
//
void
repl_pxfs_server::ckpt_new_fsobj(PXFS_VER::filesystem_ptr fs_obj,
    const char *mntoptions, Environment &)
{
	// Create the shadow fs object.
	if (fsp == NULL) {
		fsp = new fs_repl_impl(mountdata.fstype, mountdata.spec,
		    mntoptions, this, fs_obj);
		fs_v = PXFS_VER::filesystem::_duplicate(fs_obj);
	}
}

//
// Checkpoint a new service version
//
void
repl_pxfs_server::ckpt_service_version(unsigned short new_major,
    unsigned short new_minor, Environment &)
{
	version_lock.wrlock();
	current_version.major_num = new_major;
	current_version.minor_num = new_minor;
	pending_version.major_num = 0;
	version_lock.unlock();
}

//
// Checkpoint a failure in become_primary() or become_secondary().
//
void
repl_pxfs_server::ckpt_mnt_error(sol::error_t error, Environment &)
{
	ASSERT(fsp != NULL);
	mnt_error = error;
}

//
// Checkpoint the creation of new fobjs corresponding to the fobjs
// in the sequence.
//
void
repl_pxfs_server::ckpt_dump_fobj_list(const REPL_PXFS_VER::fobj_ckpt_seq_t &seq,
    Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_dump_fobj_list(seq);
}

//
// Checkpoint the creation of a new fobj which has the specified
// fid (used for switchover for primary) and type.
//
void
repl_pxfs_server::ckpt_new_fobj(PXFS_VER::fobj_ptr obj,
    const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t type,
    Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_new_fobj(obj, fobjid, type);
}

//
// Checkpoint the creation of a new fsmgr_server object for
// detecting client crashes.
//
void
repl_pxfs_server::ckpt_new_fsmgr(PXFS_VER::fsmgr_server_ptr servermgr,
    PXFS_VER::fsmgr_client_ptr clientmgr, sol::nodeid_t nodeid, Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_new_fsmgr(servermgr, clientmgr, nodeid);
}

//
// Update the mount arguments after a remount (see fs_ii::remount()).
// We assume that the VFS_MOUNT() (with MS_REMOUNT set) contains the
// complete mount information and doesn't depend on the history of previous
// of previous calls to VFS_MOUNT(). If this isn't true, we would need to
// save a list of all parameters and replay all the VFS_MOUNT() calls
// on failover.
//
void
repl_pxfs_server::set_mountargs(const sol::mounta &ma)
{
	mountdata = ma;
	//
	// We set MS_NOSPLICE so that the underlying file system isn't
	// linked into the file system name space.
	// XXX We also force the MS_OVERLAY flag on to suppress the
	// mvp->v_count == 1 EBUSY check in the file system code.
	// We clear MS_REMOUNT since we will be doing a mount instead
	// of a remount after a failover.
	//
	mountdata.flags = MS_NOSPLICE | MS_OVERLAY | (ma.flags & ~MS_REMOUNT);
}

//
// Checkpoint blocks allocated by the server.
//
void
repl_pxfs_server::ckpt_blocks_allocated(
    const repl_pxfs_v1::blocks_allocated_t &current_allocations,
    PXFS_VER::blkcnt_t blocks_free_cnt, Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_blocks_allocated(current_allocations, blocks_free_cnt);
}

//
// Checkpoint the server status to secondary.
//
void
repl_pxfs_server::ckpt_server_status(PXFS_VER::server_status_t status,
    Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_server_status(status);
}

//
// Checkpoint the changes to mount arguments and options due to a remount.
//
void
repl_pxfs_server::ckpt_remount(const sol::mounta &ma, const char *mntoptions,
    Environment &)
{
	set_mountargs(ma);
	fsp->set_options(mntoptions);
}

//
// Checkpoint a change in the active locks of a file.
//
void
repl_pxfs_server::ckpt_locks(PXFS_VER::fobj_ptr obj,
    const REPL_PXFS_VER::lock_info_seq_t &locks, Environment &)
{
	fobj_ii *fobj_iip = fobj_ii::get_fobj_ii(obj);
	fobj_iip->ckpt_locks(locks);
}

//
// Checkpoint the existence of a file entry under the current mini-transaction.
// This method is invoked on the secondary by the primary.
// This state can be used later to determine if the primary completed an
// operation or not.
//
void
repl_pxfs_server::ckpt_entry_state(bool exists, Environment &_environment)
{
	transaction_state *state = new unixdir_state(exists);
	state->register_state(_environment);
	if (_environment.exception()) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("repl_pxfs_server::ckpt_entry_state: client died\n"));
		_environment.clear();
	}
}

//
// Checkpoint the existence of a file to be locked
// prior to operating on it.  This used with mini-transactions
// to handle failure mid-way through an operation.
//
void
repl_pxfs_server::ckpt_target(PXFS_VER::fobj_ptr obj,
    const PXFS_VER::fobj_info &fobjinfo, Environment &_environment)
{
	transaction_state	*state = new unixdir_state(obj, fobjinfo);
	state->register_state(_environment);

	// If the client dies, there is nothing to clean up.
	_environment.clear();
}

//
// Checkpoint the existence of a file to be locked
// prior to removing it.  This used with mini-transactions
// to handle failure mid-way through an operation.
//
void
repl_pxfs_server::ckpt_target_remove(PXFS_VER::fobj_ptr obj,
    const PXFS_VER::fobj_info &fobjinfo, uint64_t delete_id,
    Environment &_environment)
{
	transaction_state	*state = new unixdir_state(obj,
				    fobjinfo, delete_id);

	state->register_state(_environment);

	// If the client dies, there is nothing to clean up.
	_environment.clear();
}

//
// Checkpoint an object return under the current mini-transaction.
// This return value is used to return the results of a stale operation.
//
void
repl_pxfs_server::ckpt_fobj_return(PXFS_VER::fobj_ptr ret_obj,
    const PXFS_VER::fobj_info &ret_info, Environment &_environment)
{
	secondary_ctx *ctxp = secondary_ctx::extract_from(_environment);
	unixdir_state *saved_state =
	    (unixdir_state *)ctxp->get_saved_state();
	//
	// ckpt_entry_state() or ckpt_target() should have been called first
	// so there should always be a transaction state object for this
	// checkpoint operation.
	//
	ASSERT(saved_state != NULL);
	saved_state->ckpt_fobj_return(ret_obj, ret_info);
}

//
// Checkpoint an error return under the current mini-transaction.
// This error return is used to return the results of a stale operation.
//
void
repl_pxfs_server::ckpt_error_return(sol::error_t error,
    Environment &_environment)
{
	secondary_ctx *ctxp = secondary_ctx::extract_from(_environment);
	unixdir_state *saved_state =
	    (unixdir_state *)ctxp->get_saved_state();
	//
	// If ckpt_entry_state() nor ckpt_target() has not been called first,
	// this is both the start and commit for this operation.
	//
	if (saved_state == NULL) {
		transaction_state *state = new unixdir_state(error);
		state->register_state(_environment);
		// If the client dies, there is nothing to clean up.
		_environment.clear();
	} else {
		saved_state->ckpt_error_return(error);
	}
}

//
// Checkpoint the deletion of an fobj.
//
void
repl_pxfs_server::ckpt_delete_fobj(uint64_t delete_id,
    Environment &_environment)
{
	secondary_ctx *ctxp = secondary_ctx::extract_from(_environment);
	unixdir_state *saved_state =
	    (unixdir_state *)ctxp->get_saved_state();
	//
	// ckpt_entry_state() or ckpt_target() should have been called first
	// so there should always be a transaction state object for this
	// checkpoint operation.
	//
	ASSERT(saved_state != NULL);
	saved_state->ckpt_delete_fobj(delete_id);
}

//
// ckpt_deletecnt - updates the secondary file system with the number
// used by the primary for renames of deleted files.
//
void
repl_pxfs_server::ckpt_deletecnt(uint64_t delete_id, Environment &)
{
	fsp->set_deletecnt(delete_id);
}

//
// This checkpoint is used by a primary to bring a secondary up to date on
// filesystem locking state.
//
void
repl_pxfs_server::ckpt_lockfs_info(uint64_t lf_lock, uint64_t lf_flags,
    uint64_t lf_key, const char *lf_comment, Environment &)
{
	ASSERT(fsp != NULL);
	fsp->get_fs_dep_implp()->ckpt_lockfs_state(lf_lock, lf_flags, lf_key,
	    lf_comment);
}

//
// Checkpoint the beginning of a lockfs call
//
void
repl_pxfs_server::ckpt_lockfs_start(uint64_t lf_lock, uint64_t lf_flags,
    uint64_t lf_key, const char *lf_comment, Environment &_environment)
{
	ASSERT(fsp != NULL);
	fobj_lockfs_state::register_new_state(fsp, lf_lock, lf_flags, lf_key,
	    lf_comment, _environment);
}

//
// Checkpoint failure of a lockfs call.
//
void
repl_pxfs_server::ckpt_lockfs_failure(sol::error_t err,
    Environment &_environment)
{
	ASSERT(fsp != NULL);
	fobj_lockfs_state::report_failure(err, _environment);
}

void
repl_pxfs_server::ckpt_cachedata_flag(PXFS_VER::file_ptr file_p, bool flag,
    Environment &)
{
	file_ii	*file_iip = (file_ii *)fobj_ii::get_fobj_ii(file_p);
	file_iip->ckpt_cachedata_flag(flag);
}

//
// Checkpoint the state of a file
// (this is used to dump state to new secondaries).
//
void
repl_pxfs_server::ckpt_fobj_state(PXFS_VER::fobj_ptr obj, uint64_t delete_id,
    Environment &)
{
	fobj_ii	*fobj_iip = fobj_ii::get_fobj_ii(obj);
	fobj_iip->ckpt_fobj_state(delete_id);
}

//
// Checkpoint tunefs parameters for VxFS.
//
void
repl_pxfs_server::ckpt_vx_tunefs(const REPL_PXFS_VER::vx_tunefs_t &tunefs,
    Environment &)
{
	ASSERT(fsp != NULL);
	fsp->get_fs_dep_implp()->ckpt_vx_tunefs(tunefs);
}

void
repl_pxfs_server::ckpt_remove_file_locks_by_sysid(int32_t sysid,
    Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_remove_file_locks_by_sysid(sysid);
}

void
repl_pxfs_server::ckpt_remove_file_locks_by_nlmid(int32_t nlmid,
    Environment &)
{
	ASSERT(fsp != NULL);
	fsp->ckpt_remove_file_locks_by_nlmid(nlmid);
}

void
repl_pxfs_server::ckpt_fs_is_unmounted(Environment &)
{
	fs_is_unmounted = true;
}

fs_version_callback_impl::fs_version_callback_impl(
    replica::repl_prov_ptr replica_p) :
	prov_v(replica_p)
{
}

fs_version_callback_impl::~fs_version_callback_impl()
{
}

void
fs_version_callback_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	delete this;
}


// Call the provider to update the version and checkpoint it.
void
fs_version_callback_impl::do_callback(const char *,
    const version_manager::vp_version_t &current_version, Environment &e)
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("fs_version_callback_impl::do_callback: this = %p\n", this));

	// Call the provider to update the version and checkpoint it.
	void *p = prov_v->_handler()->get_cookie();
	((repl_pxfs_server *)p)->upgrade_callback(current_version, e);
}
