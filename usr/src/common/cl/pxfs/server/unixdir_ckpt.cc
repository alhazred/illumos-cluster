//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)unixdir_ckpt.cc	1.9	08/05/20 SMI"

#include <sys/types.h>
#include <sys/debug.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <sys/sol_conv.h>

#include "../version.h"
#include PXFS_IDL(repl_pxfs)
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/unixdir_ckpt.h>
#include <pxfs/server/unixdir_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//
// This constructor is used to checkpoint the existence of a directory entry
// prior to operating on it.
//
//lint -e1401
unixdir_state::unixdir_state(bool _exists) :
	transaction_state()
{
	//
	// The current state is CHECKPOINTED because the secondary has
	// received the first checkpoint message which creates this
	// state object.
	//
	cur_state = CHECKPOINTED;

	exists = _exists;
	error = 0;
}

//
// This constructor is used to checkpoint the existence of a directory entry
// to be locked prior to operating on it.
//
unixdir_state::unixdir_state(PXFS_VER::fobj_ptr obj,
    const PXFS_VER::fobj_info &finfo) :
	transaction_state()
{
	//
	// The current state is CHECKPOINTED because the secondary has
	// received the first checkpoint message which creates this
	// state object.
	//
	cur_state = CHECKPOINTED;

	// This is also the return object if we get a commit().
	targetobj = PXFS_VER::fobj::_duplicate(obj);

	retinfo = finfo;
	exists = true;
	error = 0;
}

//
// This constructor is used to checkpoint the existence of a directory entry
// to be locked prior to removing it.
//
unixdir_state::unixdir_state(PXFS_VER::fobj_ptr obj,
    const PXFS_VER::fobj_info &finfo,
    uint64_t deletion_id) :
	transaction_state(),
	delete_id(deletion_id)
{
	//
	// The current state is CHECKPOINTED because the secondary has
	// received the first checkpoint message which creates this
	// state object.
	//
	cur_state = CHECKPOINTED;

	// This is also the return object if we get a commit().
	targetobj = PXFS_VER::fobj::_duplicate(obj);

	retinfo = finfo;
	exists = true;
	error = 0;
}
//lint +e1401

//
// This constructor is used to checkpoint and commit an error in one operation.
//
//lint -e1401
unixdir_state::unixdir_state(sol::error_t err) :
	transaction_state()
{
	cur_state = COMMITTED;
	exists = false;
	error = err;
}
//lint +e1401

//
// Destructor.
//
unixdir_state::~unixdir_state()
{
}

//
// Client has died so throw away any saved state.
// This can happen asynchronously to checkpoints so locking is required.
//
void
unixdir_state::orphaned(Environment &)
{
}

//
// This is called on a secondary to record that the primary has
// successfully finished a mini-transaction.
// This should only be called when there is only a zero errno value to
// return.
// This happens synchronously to checkpoints so locking is not required.
//
void
unixdir_state::committed()
{
	cur_state = COMMITTED;

	retobj = targetobj;
	// error was set to zero in the constructor
}

//
// Function to checkpoint existence of a file for a mini-transaction.
// This function does a VOP_LOOKUP of the specified name and sends the
// status to the secondary with a ckpt_entry_state().  This state is then
// stored as a mini-transaction state so if we fail over to the secondary we
// can retrieve the state and tell if the primary succeeded in its operation.
//
// This function should only send a checkpoint if the file system is replicated
// and a checkpoint has not been sent already.  Note that if there are
// multiple failures, we need to keep the state from the first checkpoint, not
// send another.
//
void
unixdir_state::do_ckpt_entry(unixdir_ii *udirp, const char *nm, cred_t *credp,
    Environment &env)
{
	if (!udirp->is_replicated()) {
		return;
	}

	// Test to see if the file exists.
	bool	fexists = false;
	vnode_t *tmpvp;

	//lint -e666
#if	SOL_VERSION >= __s11
	int err = VOP_LOOKUP(udirp->get_vp(), (char *)nm, &tmpvp,
	    NULL, 0, NULL, credp, NULL, NULL, NULL);
#else
	int err = VOP_LOOKUP(udirp->get_vp(), (char *)nm, &tmpvp,
	    NULL, 0, NULL, credp);
#endif
	//lint +e666

	if (err == 0) {
		fexists = true;
		VN_RELE(tmpvp);
	}

	// Send the checkpoint.
	udirp->get_ckpt()->ckpt_entry_state(fexists, env);
}

//
// Return true if the original primary server did the operation successfully
// (but the secondary / new primary didn't receive the committed checkpoint).
//
bool
unixdir_state::server_did_op(int op_err, bool exists_after_op)
{
	//
	// If the error the VOP_XXX operation returned is the error that
	// happens if the operation is done twice, and the checkpointed
	// state of the file existence on the original primary is different
	// from the state after the operation, then the primary must have
	// completed the operation successfully.
	//
	int	err_to_ignore = exists_after_op ? EEXIST : ENOENT;
	return (op_err == err_to_ignore && (int)exists != (int)exists_after_op);
}
