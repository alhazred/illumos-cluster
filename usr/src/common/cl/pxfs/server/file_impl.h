//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	FILE_IMPL_H
#define	FILE_IMPL_H

#pragma ident	"@(#)file_impl.h	1.25	08/07/07 SMI"

//
// file_impl.h - Implementation class supporting the regular data file.
//

#include <sys/types.h>
#include <sys/vfs.h>
#include <sys/vnode.h>

#include <h/px_aio.h>
#include <sys/os.h>
#include <sys/nodeset.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include PXFS_IDL(repl_pxfs)
#include <pxfs/server/fobjplus_impl.h>

//
// file_ii - file object for a regular file that
// can support memory mapped pages. This class has code that is shared
// between regular files belonging to ordinary and highly
// available file systems.
//
class file_ii : public fobjplus_ii {
public:
	//
	// Downgrade action is an argument to downgrade_all().
	//
	enum downgrade_action {
		DELETE_RANGE, FLUSH_BACK, DENY_WRITES, WRITE_BACK
	};

	virtual ~file_ii();

	virtual void	range_lock();
	virtual void	range_unlock();

	int		truncate(u_offset_t newsize, Environment &env);

	int		inval_all_attrs(nodeid_t node_id, Environment &env);

	// Set the initial value for page caching. There are no clients yet.
	void		init_cachedata_flag(bool onoff);

	// Turn client data page caching on or off.
	int		set_cachedata_flag(bool onoff, Environment &env);

	// Returns whether data page caching is on or off.
	virtual bool	is_cached();

	// Convert an fobj from secondary to primary representation.
	virtual int	convert_to_primary();

	// Helper function for dumping state to a new secondary.
	virtual void	dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
			    PXFS_VER::fobj_ptr fobjp, Environment &env);

	void		ckpt_cachedata_flag(bool onoff);

	virtual void	acquire_token_locks();
	virtual void	release_token_locks();

	virtual void	remove_client_privileges(nodeid_t node_id);

	int		flush_caches(nodeid_t node_id, Environment &env);

	void		downgrade_all(sol::u_offset_t off,
		    downgrade_action action,
		    nodeid_t skip_node, Environment &env);

protected:
	// Unreplicated or primary constructor for the internal implementation.
	file_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation.
	file_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_type_t ftype);

	// Return a new PXFS_VER::file CORBA reference to this.
	virtual PXFS_VER::file_ptr	get_fileref() = 0;

	void		fsync(int32_t syncflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	//
	// Methods supporting IDL operations file
	//

	//
	// Interface operations of the server supporting
	// client side caching of data
	//

	void	page_in(sol::u_offset_t offset, sol::size_t length,
	    PXFS_VER::acc_rights_t access_rights,
	    bulkio::inout_pages_ptr &pglobj,
	    bulkio::file_hole_list_out holes, solobj::cred_ptr credobj,
	    Environment &_environment);

	void	async_page_in(pxfs_aio::aio_callback_ptr aiock,
	    sol::u_offset_t offset, sol::size_t length,
	    PXFS_VER::acc_rights_t access_rights,
	    bulkio::in_aio_pages_ptr pglobj,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	page_out(sol::u_offset_t offset, sol::size_t length,
	    sol::size_t client_file_size, int32_t flags,
	    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
	    int64_t timestamp, bool size_in_sync, Environment &_environment);

	void	async_page_out(pxfs_aio::aio_callback_ptr aiock,
	    sol::u_offset_t offset, sol::size_t length,
	    sol::size_t client_file_size, int32_t flags,
	    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
	    int64_t timestamp, bool size_in_sync, Environment &_environment);

	void	sync(sol::u_offset_t offset, sol::size_t length,
	    int32_t syncflags,
	    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	void	write_lock(uint32_t &server_incarn,
	    Environment &_environment);

	void	read_lock(uint32_t &server_incarn,
	    Environment &_environment);

	void	cascaded_truncate(sol::u_offset_t size,
	    Environment &_environment);

	void	bmap(sol::u_offset_t	offset,
	    sol::size_t		&length,
	    sol::vattr_t	&attributes,
	    sol::nodeid_t	&server_node,
	    int64_t		&timestamp,
	    PXFS_VER::server_status_t &status,
	    solobj::cred_ptr	credobj,
	    Environment		&_environment);

	void	enable_cachedata(Environment &_environment);

	void	uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	void	uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	void	space(int32_t cmd, const sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    Environment &_environment);

	// End of Idl operations

	virtual void	prepare_recover();

	virtual void	recovered_state(PXFS_VER::recovery_info &recovery);

private:
	int	set_size(vattr_t *attr, Environment &env);

	void	downgrade(sol::u_offset_t off, downgrade_action action,
		    client_set::client_entry *entryp, Environment &env);

	//
	// This function is used to check whether we need to sync/set
	// the file size (via VOP_ALLOC). This is to address the inconsistent
	// file size between client and server during optimization.
	//
	int	alloc_check_for_pageout(sol::u_offset_t offset,
		    sol::size_t length, sol::size_t client_file_size,
		    cred_t *credp, bool &alloc_required);

	bool	reset_mtime(int64_t timestamp, cred_t *credp,
		    Environment &_environment);

public:
	// This is used for the directio transition cases.  We grab a
	// rdlock for the:
	//
	//	VOP_RWLOCK()
	//	VOP_WRITE()
	//	VOP_RWUNLOCK()
	//
	// sequence in uiowrite() -- then a wrlock in
	// ufs_dependent_impl::process_cascaded_ioctl().
	//
	// See 4404484.
	//
	os::rwlock_t	dio_writes;

private:
	os::rwlock_t range_rwlock;	// range lock

	enum {
		PXFS_READ_TOKEN = 0x1,
		PXFS_WRITE_TOKEN = 0x2
	};

	//
	// Protects read and write token state information.
	// The range lock must be acquired before the token_state_lock.
	//
	os::mutex_t	token_state_lock;

	// Bit mask identifying nodes holding read token
	nodeset		read_token;

	// Bit mask identifying node holding write token
	nodeset		write_token;

	//
	// content_rwlock protects write permission granted to a node
	// via the write_token. It must be taken as a writer when
	// permissions have to be modified and the issuing node is
	// dead and as a reader when the page is being written.
	//
	// After a node failure there can be pending writes from the
	// failed node which have passed the the orphan check. To
	// force those requests to drain, content_rwlock is taken as a
	// writer before revoking the node's token.
	//
	// Hierarchy: range_lock, attr_lock and token_state_lock are
	// above this - ie. should be acquired before; list_lock comes
	// below this.
	//
	os::rwlock_t	content_rwlock;

	//
	// The clients can cache data when flag is true.
	// Protected by range_lock()
	//
	bool		cachedata_flag;

	// Disallow assignments and pass by value.
	file_ii(const file_ii &);
	file_ii &operator = (file_ii &);
};

//
// Unreplicated CORBA implementation of file.
//
class file_norm_impl : public McServerof<PXFS_VER::file>, public file_ii {
public:
	file_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);

	virtual void		_unreferenced(unref_t arg);

	// Return a new PXFS_VER::fobj CORBA reference to this.
	virtual PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	// Return a new file CORBA reference to this.
	PXFS_VER::file_ptr	get_fileref();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t
	    get_fobj_type(Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void
	    frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void
	    frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	virtual void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	virtual void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	//
	// file IDL operations
	//

	virtual void	uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	cascaded_fsync(int32_t syncflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	space(int32_t cmd, const sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	page_in(sol::u_offset_t offset, sol::size_t length,
	    PXFS_VER::acc_rights_t access_rights,
	    bulkio::inout_pages_ptr &pglobj,
	    bulkio::file_hole_list_out holes,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	async_page_in(pxfs_aio::aio_callback_ptr aiock,
	    sol::u_offset_t offset, sol::size_t length,
	    PXFS_VER::acc_rights_t access_rights,
	    bulkio::in_aio_pages_ptr pglobj,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	page_out(sol::u_offset_t offset, sol::size_t length,
	    sol::size_t client_file_size,
	    int32_t flags, bulkio::in_pages_ptr pglobj,
	    solobj::cred_ptr credobj,
	    int64_t timestamp, bool size_in_sync, Environment &_environment);

	virtual void	async_page_out(pxfs_aio::aio_callback_ptr aiock,
	    sol::u_offset_t offset, sol::size_t length,
	    sol::size_t client_file_size, int32_t flags,
	    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
	    int64_t timestamp, bool size_in_sync, Environment &_environment);

	virtual void	sync(sol::u_offset_t offset, sol::size_t length,
	    int32_t syncflags, bulkio::in_pages_ptr pglobj,
	    solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	cascaded_write_lock(uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cascaded_read_lock(uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cascaded_truncate(sol::u_offset_t size,
	    Environment &_environment);

	virtual void	bmap(sol::u_offset_t	offset,
	    sol::size_t		&length,
	    sol::vattr_t	&attributes,
	    sol::nodeid_t	&server_node,
	    int64_t		&timestamp,
	    PXFS_VER::server_status_t &status,
	    solobj::cred_ptr	credobj,
	    Environment		&_environment);

	virtual void	enable_cachedata(Environment &_environment);
	// End of methods supporting IDL operations

private:
	// Disallow assignments and pass by value.
	file_norm_impl(const file_norm_impl &);
	file_norm_impl &operator = (file_norm_impl &);
};

//
// Replicated CORBA implementation of file.
//
class file_repl_impl :
	public mc_replica_of<PXFS_VER::file>,
	public file_ii
{
public:
	// Primary constructor.
	file_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);

	// Secondary constructor.
	file_repl_impl(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::file_ptr obj);

	virtual void		_unreferenced(unref_t arg);

	// Return a new fobj CORBA reference to this
	virtual PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	// Return a new file CORBA reference to this
	PXFS_VER::file_ptr	get_fileref();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t
	    get_fobj_type(Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void
	    frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void
	    frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	virtual void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	virtual void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	//
	// file IDL operations
	//

	virtual void	uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	cascaded_fsync(int32_t syncflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	space(int32_t cmd, const sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	page_in(sol::u_offset_t offset, sol::size_t length,
	    PXFS_VER::acc_rights_t access_rights,
	    bulkio::inout_pages_ptr &pglobj,
	    bulkio::file_hole_list_out holes,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	async_page_in(pxfs_aio::aio_callback_ptr aiock,
	    sol::u_offset_t offset, sol::size_t length,
	    PXFS_VER::acc_rights_t access_rights,
	    bulkio::in_aio_pages_ptr pglobj,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	page_out(sol::u_offset_t offset, sol::size_t length,
	    sol::size_t client_file_size,
	    int32_t flags, bulkio::in_pages_ptr pglobj,
	    solobj::cred_ptr credobj,
	    int64_t timestamp, bool size_in_sync, Environment &_environment);

	virtual void	async_page_out(pxfs_aio::aio_callback_ptr aiock,
	    sol::u_offset_t offset, sol::size_t length,
	    sol::size_t client_file_size, int32_t flags,
	    bulkio::in_pages_ptr pglobj, solobj::cred_ptr credobj,
	    int64_t timestamp, bool size_in_sync, Environment &_environment);

	virtual void	sync(sol::u_offset_t offset, sol::size_t length,
	    int32_t syncflags, bulkio::in_pages_ptr pglobj,
	    solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	cascaded_write_lock(uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cascaded_read_lock(uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cascaded_truncate(sol::u_offset_t size,
	    Environment &_environment);

	virtual void	bmap(sol::u_offset_t	offset,
	    sol::size_t		&length,
	    sol::vattr_t	&attributes,
	    sol::nodeid_t	&server_node,
	    int64_t		&timestamp,
	    PXFS_VER::server_status_t &status,
	    solobj::cred_ptr	credobj,
	    Environment		&_environment);

	virtual void	enable_cachedata(Environment &_environment);

	// End of methods supporting IDL operations

private:
	// Disallow assignments and pass by value.
	file_repl_impl(const file_repl_impl &);
	file_repl_impl &operator = (file_repl_impl &);
};

#include <pxfs/server/file_impl_in.h>

#endif	// FILE_IMPL_H
