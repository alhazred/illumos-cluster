//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)io_impl.cc	1.11	08/05/20 SMI"

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/pathname.h>
#include <sys/debug.h>
#include <sys/uio.h>
#include <sys/file.h>
#include <sys/errno.h>
#include <sys/fcntl.h>
#include <sys/vnode.h>

#include <sys/os.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/io_trans_states.h>
#include <pxfs/server/fobj_trans_states.h>
#include <pxfs/server/async_io.h>
#include <pxfs/server/fs_base_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

extern "C" int pxfs_aio_done(struct buf *bp);

//
// Unreplicated or primary constructor for the internal implementation.
//
io_ii::io_ii(fs_base_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
    fid_t *fidp) :
	fobj_ii(fsp, vp, ftype, fidp),
	primary_failed(false)
{
	ASSERT(vp != NULL);
}

//
// Secondary constructor for the internal implementation.
//
io_ii::io_ii(fs_base_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t ftype) :
	fobj_ii(fsp, fobjid, ftype),
	primary_failed(false)
{
}

io_ii::~io_ii()
{
}

//
// is_dead - Return true if this is a dead replicated file system object
// (i.e., there was a fatal error in convert_to_primary() or
// convert_to_secondary() but the replica manager still thinks
// the service is alive).
// Note that this routine is only called on the primary.
// If we return true, the environment is also set to return an exception.
//
bool
io_ii::is_dead(Environment &env)
{
	if (fobj_released) {
		//
		// Note that EIO is correct, the "disk" failed.
		// ESTALE is returned by NFS when VFS_VGET() returns error.
		//
		pxfslib::throw_exception(env, EIO);
		return (true);
	}

	if (primary_failed) {
		//
		// An attempt to make this a primary object failed
		//
		pxfslib::throw_exception(env, EIO);
		return (true);
	}

	return (false);
}

//
// Perform an open protocol on an unreplicated device object.
//
void
io_ii::open(int32_t flags, PXFS_VER::fobj_out open_io_fobj,
    PXFS_VER::fobj_info &fobjinfo,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t	*crp = solobj_impl::conv(credobj);
	vnode_t *newvp = get_vp();

	//
	// Perform open protocol on the underlying vnode.
	// 'newvp' is held here and released if there is an error.
	// If the open returns a new vnode, 'newvp' is released and
	// the new 'newvp' is returned held.
	//
	VN_HOLD(newvp);
#if	SOL_VERSION >= __s11
	int	error = VOP_OPEN(&newvp, flags, crp, NULL);
#else
	int	error = VOP_OPEN(&newvp, flags, crp);
#endif
	if (error == 0) {
		//
		// We call the version specific device server
		// find_fobj method to get an object reference
		// to an io object that encapsulates 'newvp'.
		// If no such object exists this call creates it.
		// Note that fs_implp is a reference to a
		// device_server/fs_base_ii object, and is
		// obtained from the device_server_ii
		// reference passed to the io_[norm|repl]_impl
		// constructors (below).
		//
		open_io_fobj =
		    ((device_server *)fs_implp)->find_fobj(
		    newvp, NULL, credobj, fobjinfo, _environment);
		if (CORBA::is_nil(open_io_fobj)) {
			pxfslib::throw_exception(_environment, ENOTSUP);
		}
	} else {
		open_io_fobj = PXFS_VER::fobj::_nil();
		pxfslib::throw_exception(_environment, error);
	}
	VN_RELE(newvp);
}

//
// io_ii::close - perform a close protocol on a previously open
// io object.
//
void
io_ii::close(int32_t flags, sol::u_offset_t off, solobj::cred_ptr credobj,
    Environment &_environment)
{
	cred_t	*crp = solobj_impl::conv(credobj);
#if	SOL_VERSION >= __s11
	int	error = VOP_CLOSE(get_vp(), flags, 1, (offset_t)off, crp, NULL);
#else
	int	error = VOP_CLOSE(get_vp(), flags, 1, (offset_t)off, crp);
#endif
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
io_ii::uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj, int32_t ioflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t	*crp = solobj_impl::conv(credobj);
	uio_t	*uiop = bulkio_impl::conv(uioobj);

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_B, FaultFunctions::generic);

	uiop->uio_resid = (ssize_t)len;
#if SOL_VERSION >= __s10
	VOP_RWLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWLOCK(get_vp(), 0);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_AFTER_RWLOCK,
		FaultFunctions::generic);

#if	SOL_VERSION >= __s10
	int error = VOP_READ(get_vp(), uiop, ioflag, crp, NULL);
#else
	int error = VOP_READ(get_vp(), uiop, ioflag, crp);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_A, FaultFunctions::generic);

#if SOL_VERSION >= __s10
	VOP_RWUNLOCK(get_vp(), V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWUNLOCK(get_vp(), 0);
#endif
	len -= (sol::size_t)uiop->uio_resid;

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOREAD_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
io_ii::uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj, int32_t ioflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t	*crp = solobj_impl::conv(credobj);
	uio_t	*uiop = bulkio_impl::conv(uioobj);

	//
	// Check for an exception during server-pull
	// We signal an exception by returning NULL for uiop
	//
	if (uiop == NULL) {
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_B, FaultFunctions::generic);

	uiop->uio_resid = (ssize_t)len;
	if (uiop->uio_iov == NULL) {
		pxfslib::throw_exception(_environment, EIO);
		return;
	}
#if SOL_VERSION >= __s10
	VOP_RWLOCK(get_vp(), V_WRITELOCK_TRUE, NULL);
#else
	VOP_RWLOCK(get_vp(), 1);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_AFTER_RWLOCK,
		FaultFunctions::generic);
	//
	// To ensure essential metadata integrity, append the FDSYNC
	// flag to ioflag when client and server are not colocated
	// together. See bug report 4810418 for more information.
	//
#if	SOL_VERSION >= __s10
	int	error = VOP_WRITE(get_vp(), uiop,
	    _environment.is_local_client() ? ioflag : ioflag | FDSYNC,
	    crp, NULL);
#else
	int	error = VOP_WRITE(get_vp(), uiop,
	    _environment.is_local_client() ? ioflag : ioflag | FDSYNC,
	    crp);
#endif

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_A, FaultFunctions::generic);
#if SOL_VERSION >= __s10
	VOP_RWUNLOCK(get_vp(), V_WRITELOCK_TRUE, NULL);
#else
	VOP_RWUNLOCK(get_vp(), 1);
#endif
	len -= (sol::size_t)uiop->uio_resid;

	FAULTPT_PXFS(FAULTNUM_PXFS_UIOWRITE_S_E, FaultFunctions::generic);

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
io_ii::aioread(pxfs_aio::aio_callback_ptr aiock, bulkio::in_aio_ptr aioobj,
    solobj::cred_ptr credobj, Environment &_environment)
{
	major_t			maj;
	dev_t			dev;
	struct cb_ops		*cb;
	cred_t			*crp = solobj_impl::conv(credobj);
	struct aio_req		*aiop = bulkio_impl::aconv(aioobj, aiock);
	int			error;
	struct aio_req_t	*reqp;

	reqp = (aio_req_t *)aiop->aio_private;
	struct buf	*bp = &reqp->aio_req_buf;
	if (bp->b_flags & B_ERROR) {
		//
		// This is the case where the client has not
		// been able to lock the pages.  The server
		// should not start the io since this would
		// break the aio semantics in the client.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_IO,
		    PXFS_RED,
		    ("io_ii::aioread b_error %d\n", bp->b_error));

		_environment.exception(new sol::op_e(bp->b_error));
		return;
	}

	io_async	*ios = new io_async(reqp, aiock, aioobj);

	//
	// We are going to use aphysio() in base solaris
	// In aphysio(), if b_iodone field is non-NULL, the
	// b_iodone and b_forw fields are not altered.
	//
	bp->b_iodone = pxfs_aio_done;
	bp->b_forw = (struct buf *)ios;

	dev = get_vp()->v_rdev;

	maj = getmajor(dev);

	cb = devopsp[maj]->devo_cb_ops;

	//
	// devo_rev indicates old drivers which do not have async i/o
	// The device has to be a block device. aio is not supported
	// for devices like tty indicated by NULL strategy routines
	//
	if ((devopsp[maj]->devo_rev < 3) || (cb->cb_rev < 1) ||
	    (cb->cb_strategy == NULL) || (cb->cb_aread == NULL)) {
		error = EIO;
	} else {
		//
		// Workaround for bug 4293175. It's important that did_aio()
		// is called before cb_aread.
		//
		bulkio_impl::did_aio(aioobj);
		error = cb->cb_aread(dev, aiop, crp);
	}

	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_IO,
		    PXFS_RED,
		    ("io_ii::aioread error %d\n", error));

		_environment.exception(new sol::op_e(error));

		//
		// b_flags is set to B_ERROR only. The reasons are
		// 1. The pages have not been locked, when we get
		// an error for the cb_aread() call.
		// 2. This will reset all the flags that might
		// have been set by aphysio() for initialization.
		// 3. In bulkio layer, checking for any of the
		// other flags set by aphysio like B_PHYS can tell
		// us if the pages have been locked or not.
		// This is a hack but it works.
		//
		bp->b_flags = B_ERROR;
		bp->b_error = error;

		//
		// The cb_aread has returned an error. This
		// implies that io has not been started.
		// So there is not going to be a callback.
		// Delete the holder object which will release
		// the references to the callback and bulkio
		// objects.
		//
		delete ios;
	}
}

void
io_ii::aiowrite(pxfs_aio::aio_callback_ptr aiock, bulkio::in_aio_ptr aioobj,
    solobj::cred_ptr credobj, Environment &_environment)
{
	major_t		maj;
	dev_t		dev;
	struct cb_ops	*cb;
	int		error;
	cred_t		*crp = solobj_impl::conv(credobj);

	struct aio_req	*aiop = bulkio_impl::aconv(aioobj, aiock);
	ASSERT(aiop != NULL);

	struct aio_req_t	*reqp;

	reqp = (aio_req_t *)aiop->aio_private;
	struct buf	*bp = &reqp->aio_req_buf;
	if (bp->b_flags & B_ERROR) {
		//
		// Client could not lock the pages. The server should
		// not start the io since it would then break the aio
		// semantics.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_IO,
		    PXFS_RED,
		    ("io_ii::aiowrite b_error %d\n", bp->b_error));

		_environment.exception(new sol::op_e(bp->b_error));
		return;
	}

	io_async	*ios = new io_async(reqp, aiock, aioobj);

	//
	// In aphysio(), if the b_iodone field is non-NULL,
	// then b_iodone and b_forw fields are not altered.
	//
	bp->b_iodone = pxfs_aio_done;
	bp->b_forw = (struct buf *)ios;

	dev = get_vp()->v_rdev;
	maj = getmajor(dev);

	cb = devopsp[maj]->devo_cb_ops;

	//
	// devo_rev indicates old drivers which do not have async i/o
	// The device has to be a block device. aio is not supported
	// for devices like tty indicated by NULL strategy routines
	//
	if ((devopsp[maj]->devo_rev < 3) || (cb->cb_rev < 1) ||
	    (cb->cb_strategy == NULL) || (cb->cb_awrite == NULL)) {

		// XXX Verify that the error value being returned is right
		error = EIO;
	} else {
		//
		// Workaround for bug 4293175. It is important to call
		// did_aio() before cb_awrite().
		//
		bulkio_impl::did_aio(aioobj);
		error = cb->cb_awrite(dev, aiop, crp);
	}

	if (error) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_IO,
		    PXFS_RED,
		    ("io_ii::aiowrite error %d\n", error));

		_environment.exception(new sol::op_e(error));
		//
		// See comments on the read call above for
		// explanation.
		//
		bp->b_flags = B_ERROR;
		bp->b_error = error;

		//
		// The callback will not happen since the io
		// has not been started. Delete the holder object
		// so that the references to callback and bulkio
		// objects are released.
		//
		delete ios;
	}
}

void
io_ii::fsync(int32_t syncflag, solobj::cred_ptr credobj,
    Environment &_environment)
{
	cred_t	*crp = solobj_impl::conv(credobj);

#if	SOL_VERSION >= __s11
	int	error = VOP_FSYNC(get_vp(), syncflag, crp, NULL);
#else
	int	error = VOP_FSYNC(get_vp(), syncflag, crp);
#endif
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// io_norm_impl methods
//

//
// Constructor for the unreplicated CORBA implementation.
//
io_norm_impl::io_norm_impl(device_server_ii *dsp, vnode_t *vp, fid_t *fidp) :
	io_ii(&dsp->device_serv, vp, PXFS_VER::fobj_io, fidp)
{
	set_fobj_ii(this);
}

//
// Destructor for the unreplicated CORBA implementation.
//
io_norm_impl::~io_norm_impl()
{
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// fs_ii::find_fobj() can reclaim this object so we lock the
// appropriate bucket lock while checking for this case.
// This kind of file object is always the primary.
// If we win the race, then
// do what is needed to delete the object.
//
void
io_norm_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	if (!fobj_released) {
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	}
	if (_last_unref(arg)) {
		unreferenced(bktno, false);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
}

handler *
io_norm_impl::get_handler()
{
	return (_handler());
}

// Methods supporting IDL operations
/* fobj */

PXFS_VER::fobj_type_t
io_norm_impl::get_fobj_type(Environment &_environment)
{
	return (io_ii::get_fobj_type(_environment));
}

void
io_norm_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
io_norm_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
io_norm_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::access(accmode, accflags, credobj, _environment);
}

void
io_norm_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
io_norm_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::get_secattributes(sattr, secattrflag, credobj, _environment);
} //lint !e1746

void
io_norm_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::set_secattributes(sattr, secattrflag, credobj, _environment);
}

void
io_norm_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::pathconf(cmd, result, credobj, _environment);
}

void
io_norm_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	io_ii::getfobjid(fobjid, _environment);
} //lint !e1746

void
io_norm_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result,
	    credobj, _environment);
}

void
io_norm_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	io_ii::frlock(cmd, lock_info, flag, off, credobj, cb, _environment);
}

void
io_norm_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	io_ii::frlock_cancel_request(cb, _environment);
}

void
io_norm_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	io_ii::frlock_execute_request(cb, _environment);
}

void
io_norm_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
io_norm_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::remove_locks(cpid, sysid, credobj, _environment);
}

/* io */

void
io_norm_impl::open(int32_t flags, PXFS_VER::fobj_out open_io_fobj,
    PXFS_VER::fobj_info &fobjinfo, solobj::cred_ptr credobj,
    Environment &_environment)
{
	io_ii::open(flags, open_io_fobj, fobjinfo, credobj, _environment);
} //lint !e1746

void
io_norm_impl::close(int32_t flags, sol::u_offset_t off,
    solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::close(flags, off, credobj, _environment);
}

void
io_norm_impl::uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::uioread(len, uioobj, ioflag, credobj, _environment);
}

void
io_norm_impl::aioread(pxfs_aio::aio_callback_ptr aiock,
    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
    Environment &_environment)
{
	io_ii::aioread(aiock, aioobj, credobj, _environment);
}

void
io_norm_impl::uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	io_ii::uiowrite(len, uioobj, ioflag, credobj, _environment);
}
void

io_norm_impl::aiowrite(pxfs_aio::aio_callback_ptr aiock,
    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
    Environment &_environment)
{
	io_ii::aiowrite(aiock, aioobj, credobj, _environment);
}

void
io_norm_impl::cascaded_fsync(int32_t syncflag, solobj::cred_ptr credobj,
    Environment &_environment)
{
	io_ii::fsync(syncflag, credobj, _environment);
}

// End of methods supporting IDL operations

//
// io_repl_impl methods
//

//
// Constructor for the primary replicated CORBA implementation.
//
io_repl_impl::io_repl_impl(device_server_ii *dsp, vnode_t *vp,
    const PXFS_VER::pvnode &pvn, cred_t *crp, fid_t *fidp) :
	io_ii(&dsp->device_serv, vp, PXFS_VER::fobj_io, fidp),
	mc_replica_of<PXFS_VER::io>(dsp->get_serverp())
{
	io_construct(pvn, crp);
}

//
// Constructor for the secondary replicated CORBA implementation.
//
io_repl_impl::io_repl_impl(device_server_ii *dsp,
    const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::io_ptr obj, const PXFS_VER::pvnode &pvn, cred_t *crp) :
	io_ii(&dsp->device_serv, fobjid, PXFS_VER::fobj_io),
	mc_replica_of<PXFS_VER::io>(obj)
{
	io_construct(pvn, crp);
}

//
// Common code for the primary and the secondary constructors.
//
void
io_repl_impl::io_construct(const PXFS_VER::pvnode &pvn, cred_t *crp)
{
	set_fobj_ii(this);

	real_pvnode = pvn;
	credp = crp;
	crhold(credp);
}

//
// Destructor for replicated objects.
//
io_repl_impl::~io_repl_impl()
{
	crfree(credp);
	open_params_list.dispose();
}

//
// _unreferenced - called when the CORBA reference count reaches zero.
// device_server_ii::find_fobj() can reclaim this object
// so we acquire the appropriate locks while checking for this case.
// If we win the race, then do what is needed to delete the object.
// Note that IO objects do not do lazy fid conversion.
//
void
io_repl_impl::_unreferenced(unref_t arg)
{
	int	bktno = INT_MAX;
	bool	fidlist_locked = false;

	if (fobj_released) {
		// No locking needed because not on any list

	} else if (primary_ready) {
		//
		// The file object is a ready primary.
		//
		bktno = fs_implp->vp_to_bucket(get_vp());
		fs_implp->fobjlist_locks[bktno].wrlock();
	} else {
		//
		// The file object has not become a primary yet.
		//
		fidlist_locked = true;
		fs_implp->fidlock.wrlock();
	}

	if (_last_unref(arg)) {
		if (is_active()) {
			//
			// Iterate over the open_params_list and
			// call VOP_CLOSE() for each element in the list.
			// This makes sure that there are no "dangling"
			// open()'s on the underlying device when its
			// io_repl_impl object is destroyed.
			//
			open_params_t	*paramsp;
			for (open_params_list.atfirst();
			    (paramsp = open_params_list.get_current()) != NULL;
			    open_params_list.advance()) {
#if	SOL_VERSION >= __s11
				int	error = VOP_CLOSE(get_vp(),
				    paramsp->flags, 1, (offset_t)0,
				    paramsp->crp, NULL);
#else
				int	error = VOP_CLOSE(get_vp(),
				    paramsp->flags, 1, (offset_t)0,
				    paramsp->crp);
#endif
				if (error != 0) {
				    //
				    // close() failed - continue with a warning.
				    //
				    PXFS_DBPRINTF(
					PXFS_TRACE_IO,
					PXFS_RED,
					("close(%p) dev %x returned %d\n",
					this, get_vp()->v_rdev, error));
				}
			}
		}
		//
		// Note that unreference processing will drop
		// the appropriate locks.
		//
		unreferenced(bktno, fidlist_locked);
		return;
	}
	if (bktno != INT_MAX) {
		fs_implp->fobjlist_locks[bktno].unlock();
	}
	if (fidlist_locked) {
		fs_implp->fidlock.unlock();
	}
}

//
// Convert this object into a primary.
// Called from device_server_impl::convert_to_primary().
//
// Note that this routine should not be called from
// fs_ii::convert_to_primary(). This works because
// io_repl_impl::convert_to_primary() replaces fobj_ii::convert_to_primary().
//
int
io_repl_impl::convert_to_primary()
{
	//
	// Create the specvp associated with this io object.
	// Returns a held vnode.
	//
	vnode_t *svp = device_server_ii::make_specvp(real_pvnode, credp);
	if (svp == NULL) {
		return (EDOM);
	}

	//
	// Replay device opens.
	// Locking is handled by the HA framework.
	//
	open_params_t *p;

	for (open_params_list.atfirst();
	    (p = open_params_list.get_current()) != NULL;
	    open_params_list.advance()) {
		//
		// Perform open protocol on the underlying vnode.
		// 'newvp' is held here and released if there is an error.
		// If the open returns a new vnode, 'newvp' is released and
		// the new 'newvp' is returned held.
		//
		vnode_t	*newvp = svp;
		VN_HOLD(newvp);
#if	SOL_VERSION >= __s11
		int	error = VOP_OPEN(&newvp, p->flags, p->crp, NULL);
#else
		int	error = VOP_OPEN(&newvp, p->flags, p->crp);
#endif
		bool	same = (newvp == svp);
		VN_RELE(newvp);
		if (error != 0 || !same) {
			//
			// The error is propagated all the way up to the
			// HA Framework, which then tries to convert another
			// secondary provider (if any) for this device
			// service to primary.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_IO,
			    PXFS_RED,
			    ("open(%p) dev %x failed convert_to_primary %d\n",
			    this, svp->v_rdev, error));
			primary_failed = true;
			return (error == 0 ? ENXIO : error);
		}
	}

	//
	// We set primary_failed to false explicitly here to take
	// care of this situation: suppose the replica failed to
	// become a primary because of a transient error. Later,
	// if an attempt is made to make it the primary and it
	// succeeds, we'll still have primary_failed set to true.
	// This is not correct, because the replica is now a good
	// primary. But I/O's to this device will still fail because
	// of the check for primary_failed in is_dead(). We set
	// primary_failed to false to take care of such a case.
	//
	primary_failed = false;

	//
	// Initialize the underlying vnode pointer for the fobj_ii object.
	// Note that we transfer the hold from svp to fobj_ii.
	//
	set_vp(svp);
	primary_ready = true;

	if (fr_lockp != NULL) {
		fr_lockp->replay_active_locks();
	}

	return (0);
}

//
// Called to convert a primary io_repl object into a secondary.
//
int
io_repl_impl::convert_to_secondary()
{
	ASSERT(is_active());

	//
	// Iterate through the open parameters, and call VOP_CLOSE() for each.
	// Locking is handled by the HA framework.
	//
	open_params_t *p;

	for (open_params_list.atfirst();
	    (p = open_params_list.get_current()) != NULL;
	    open_params_list.advance()) {

#if	SOL_VERSION >= __s11
		int error = VOP_CLOSE(get_vp(), p->flags, 1, (offset_t)0,
		    p->crp, NULL);
#else
		int error = VOP_CLOSE(get_vp(), p->flags, 1, (offset_t)0,
		    p->crp);
#endif
		if (error != 0) {
			// close() failed - continue with a warning.
			PXFS_DBPRINTF(
			    PXFS_TRACE_IO,
			    PXFS_RED,
			    ("close() of dev %x returned %d\n",
			    get_vp()->v_rdev, error));
		}
	}

	// Remove all the file-locks associated with this device.
	if (fr_lockp != NULL) {
		fr_lockp->remove_llm_locks();
	}

	// Release the vnode.
	set_secondary();

	return (0);
}

//
// Convert an fobj from secondary to spare representation.
//
void
io_repl_impl::convert_to_spare()
{
	fobj_ii::convert_to_spare();
}

//
// This method is used to checkpoint an io object to a secondary that is being
// added or to checkpoint an io object that has just been created to current
// secondaries.
// Return exceptions to our caller to deal with them.
//
void
io_repl_impl::dump_io_state(repl_dc::repl_dev_server_ckpt_ptr ckpt,
    Environment &env)
{
	PXFS_VER::io_var io_obj = get_objref();
	solobj::cred_var credobj = solobj_impl::conv(credp);

	ckpt->ckpt_new_io_object_v1(io_obj, real_pvnode, credobj, env);
	if (env.exception()) {
		return;
	}

	open_params_t *p;

	for (open_params_list.atfirst();
	    (p = open_params_list.get_current()) != NULL;
	    open_params_list.advance()) {
		credobj = solobj_impl::conv(p->crp);
		ckpt->ckpt_open_v1(io_obj, p->flags, credobj, env);
		if (env.exception()) {
			return;
		}
	}

	//
	// Checkpoint all the file record locks on this file.
	//
	if (fr_lockp == NULL) {
		return;
	}
	uint_t	len = fr_lockp->active_locks.count();
	if (len == 0) {
		return;
	}
	REPL_PXFS_VER::lock_info_seq_t	locks(len, len);
	REPL_PXFS_VER::lock_info_t	*tmp;
	uint_t				i = 0;
	for (fr_lockp->active_locks.atfirst();
	    (tmp = fr_lockp->active_locks.get_current()) != NULL;
	    fr_lockp->active_locks.advance()) {
		ASSERT(i < len);
		locks[i] = *tmp;
		i++;
	}

	ASSERT(i == len);

	ckpt->ckpt_locks_v1(io_obj, locks, env);
	env.clear();
}

handler *
io_repl_impl::get_handler()
{
	return (_handler());
}

//
// Called on the primary and secondary replicas to store the parameters of an
// open call for later replays.
//
void
io_repl_impl::store_open_params(int flags, cred_t *crp)
{
	open_params_t *p = new open_params_t(flags, crp);

	// We append to the end of the list to preserve the order for replays.
	open_params_list.append(p);
}

//
// Called on the primary and secondary replicas to erase the parameters of an
// open call once the corresponding close is called.
//
void
io_repl_impl::erase_open_params(int flags, cred_t *crp)
{
	open_params_t *p;

	const int close_bits = O_RDONLY | O_WRONLY | O_RDWR;

	for (open_params_list.atfirst();
	    ((p = open_params_list.get_current()) != NULL);
	    open_params_list.advance()) {
		if ((p->flags & close_bits) == (flags & close_bits) &&
		    crcmp(p->crp, crp) == 0) {
			// Found it. Remove from list.
			(void) open_params_list.erase(p);
			delete p;
			return;
		}
	}

	// MUST find a match.
	ASSERT(0);
}

//
// Constructor for list element used to store VOP_OPEN() parameters.
//
io_repl_impl::open_params_t::open_params_t(int _flags, cred_t *_crp) :
	_SList::ListElem(this)
{
	flags = _flags;
	crp = _crp;
	crhold(crp);
}

io_repl_impl::open_params_t::~open_params_t()
{
	crfree(crp);
}

//
// This checkpoint routine is called on the secondary as a result of an open
// being done on the primary.
//
void
io_repl_impl::ckpt_open(int32_t flags, solobj::cred_ptr credobj,
    Environment &env)
{
	//
	// Store the open parameters so that opens and closes can be
	// replayed during a failover/switchover.
	//
	store_open_params(flags, solobj_impl::conv(credobj));

	fobj_retry_state::register_new_state(env);
}

//
// This checkpoint routine is called on the secondary as a result of an close
// being done on the primary.
//
void
io_repl_impl::ckpt_close(int32_t flags, solobj::cred_ptr credobj,
    sol::error_t err, Environment &env)
{
	// Remove the previously stored open parameters.
	erase_open_params(flags, solobj_impl::conv(credobj));

	io_close_state::register_new_state(env, err);
}

//
// Static function called from dev_server_repl_server::ckpt_ioctl_begin().
//
void
io_repl_impl::ckpt_ioctl_begin(Environment &env)
{
	io_ioctl_state::register_new_state(env);
}

//
// Static function called from dev_server_repl_server::ckpt_ioctl_end().
//
void
io_repl_impl::ckpt_ioctl_end(sol::error_t err, int32_t result,
    Environment &env)
{
	io_ioctl_state::set_ioctl_results(env, err, result);
}

// Methods supporting IDL operations
/* fobj */

PXFS_VER::fobj_type_t
io_repl_impl::get_fobj_type(Environment &_environment)
{
	if (is_dead(_environment)) {
		return (PXFS_VER::fobj_fobj);
	}
	return (io_ii::get_fobj_type(_environment));
}

void
io_repl_impl::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::get_attributes(attrmask, attr, credobj, _environment);
}

void
io_repl_impl::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::set_attributes(attr, attrflags, credobj, _environment);
}

void
io_repl_impl::access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::access(accmode, accflags, credobj, _environment);
}

void
io_repl_impl::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::get_secattr_cnt(aclcnt, dfaclcnt, secattrflag, credobj,
	    _environment);
}

void
io_repl_impl::get_secattributes(PXFS_VER::secattr_out sattr,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::get_secattributes(sattr, secattrflag, credobj, _environment);
} //lint !e1746

void
io_repl_impl::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::set_secattributes(sattr, secattrflag, credobj, _environment);
}

void
io_repl_impl::pathconf(int32_t cmd, sol::uintptr_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::pathconf(cmd, result, credobj, _environment);
}

void
io_repl_impl::getfobjid(PXFS_VER::fobjid_t_out fobjid,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::getfobjid(fobjid, _environment);
} //lint !e1746

//
// Perform an ioctl operation on a replicated io object.
// We cannot allow incomplete ioctls to be retried on a new primary because
// this is potentially harmful.  So we use a state object to figure out if an
// ioctl is a retry and if it is an retry that was interrupted before
// completion on the primary, we return (EINTR).
//
void
io_repl_impl::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid,
    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	bool done;
	sol::error_t error;
	if (io_ioctl_state::retry(_environment, done, error, result)) {
		if (done) {
			pxfslib::throw_exception(_environment, error);
			return;
		}
		// ioctl() was interrupted before completion.
		pxfslib::throw_exception(_environment, EINTR);
		return;
	}

	get_checkpoint()->ckpt_ioctl_begin(_environment);
	if (_environment.exception()) {
		// This node is shutting down.  Just return.
		_environment.clear();
		pxfslib::throw_exception(_environment, EINTR);
		return;
	}

	io_ii::cascaded_ioctl(nodeid, cpid, iocmd, arg, flag, result, credobj,
	    _environment);
	error = pxfslib::get_err(_environment);
	_environment.clear();

	get_checkpoint()->ckpt_ioctl_end(error, result, _environment);
	_environment.clear();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
io_repl_impl::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::frlock(cmd, lock_info, flag, off, credobj, cb, _environment);
}

void
io_repl_impl::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::frlock_cancel_request(cb, _environment);
}

void
io_repl_impl::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::frlock_execute_request(cb, _environment);
}

void
io_repl_impl::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::shrlock(cmd, lock_info, flag, credobj, _environment);
}

void
io_repl_impl::remove_locks(sol::pid_t cpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::remove_locks(cpid, sysid, credobj, _environment);
}

/* io */

//
// Perform an open on a replicated io object.  This differs from opening a
// non-replicated io object in that the replicated open implementation
// needs to checkpoint the open to secondary replicas.
//
void
io_repl_impl::open(int32_t flags, PXFS_VER::fobj_out open_io_fobj,
    PXFS_VER::fobj_info &fobjinfo, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	if (fobj_retry_state::retry(_environment)) {
		//
		// If a state object exists, all the work has already been
		// done.  Just return the appropriate values.
		//
		open_io_fobj = get_objref();
		pxfs_misc::init_fobjinfo(fobjinfo, get_ftype(), get_vp(),
		    get_fidp());
		return;
	}

	cred_t	*crp = solobj_impl::conv(credobj);
	vnode_t *newvp = get_vp();

	//
	// Perform open protocol on the underlying vnode.
	// 'newvp' is held here and released if there is an error.
	// If the open returns a new vnode, 'newvp' is released and
	// the new 'newvp' is returned held.
	// We also have to control the order in which opens are done
	// so the checkpoints are replayed in the same order.
	//
	VN_HOLD(newvp);
	open_params_lock.wrlock();
#if	SOL_VERSION >= __s11
	int	error = VOP_OPEN(&newvp, flags, crp, NULL);
#else
	int	error = VOP_OPEN(&newvp, flags, crp);
#endif

	if (error == 0) {
		//
		// There is currently a requirement that an open on an HA
		// io object returns the same object. This is needed
		// since if a new vnode were returned (like a stream
		// device), we wouldn't have the same state on the secondary
		// by doing a specvp() and VOP_OPEN() since the
		// device driver doesn't checkpoint its state.
		//
		if (!VN_CMP(get_vp(), newvp)) {
			pxfslib::throw_exception(_environment, ENXIO);
		} else {
			//
			// Return a new reference to ourself.
			//
			PXFS_VER::io_ptr io_obj = get_objref();
			open_io_fobj = io_obj;
			pxfs_misc::init_fobjinfo(fobjinfo, get_ftype(),
			    get_vp(), get_fidp());

			//
			// Store the open parameters so that opens and closes
			// can be replayed during a failover/switchover.
			//
			store_open_params(flags, crp);

			get_checkpoint()->ckpt_open_v1(io_obj, flags,
			    credobj, _environment);
			_environment.clear();
		}
	} else {
		open_io_fobj = PXFS_VER::fobj::_nil();
		pxfslib::throw_exception(_environment, error);
	}
	open_params_lock.unlock();
	VN_RELE(newvp);
}

//
// Perform a close on a replicated io object.  This differs from the
// non-replicated case in the sending of a checkpoint message.
//
void
io_repl_impl::close(int32_t flags, sol::u_offset_t off,
    solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	if (io_close_state::retry(_environment)) {
		return;
	}

	cred_t	*crp = solobj_impl::conv(credobj);
	int	error;

	open_params_lock.wrlock();

#if	SOL_VERSION >= __s11
	error = VOP_CLOSE(get_vp(), flags, 1, (offset_t)off, crp, NULL);
#else
	error = VOP_CLOSE(get_vp(), flags, 1, (offset_t)off, crp);
#endif

	// Remove the previously stored open parameters.
	erase_open_params(flags, crp);

	//
	// Checkpoint the close. Note that close() is a successful call even
	// if it returns with an error; the error return is more an indication
	// of state than a failure of the call.
	//
	PXFS_VER::io_var	io_obj = get_objref();

	get_checkpoint()->ckpt_close_v1(io_obj, flags, credobj, error,
	    _environment);

	_environment.clear();

	open_params_lock.unlock();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
io_repl_impl::uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::uioread(len, uioobj, ioflag, credobj, _environment);
}

void
io_repl_impl::aioread(pxfs_aio::aio_callback_ptr aiock,
    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::aioread(aiock, aioobj, credobj, _environment);
}

void
io_repl_impl::uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
    int32_t ioflag, solobj::cred_ptr credobj, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::uiowrite(len, uioobj, ioflag, credobj, _environment);
}

void
io_repl_impl::aiowrite(pxfs_aio::aio_callback_ptr aiock,
    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	io_ii::aiowrite(aiock, aioobj, credobj, _environment);
}

void
io_repl_impl::cascaded_fsync(int32_t syncflag, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	io_ii::fsync(syncflag, credobj, _environment);
}

// End of methods supporting IDL operations
