//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FOBJPLUS_IMPL_H
#define	FOBJPLUS_IMPL_H

#pragma ident	"@(#)fobjplus_impl.h	1.20	08/07/07 SMI"

#include <sys/vnode.h>

#include <sys/os.h>
#include <sys/nodeset.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/server/client_set.h>
#include <pxfs/server/fobj_impl.h>

//
// This task supports downgrade of attributes
//
class clear_attr_dirty_task : public defer_task {
public:
	clear_attr_dirty_task::clear_attr_dirty_task(
	    fobjplus_ii *_fobjp,
	    PXFS_VER::fobj_client_var _client_v,
	    uint32_t _server_incarn);

	void execute();

private:
	PXFS_VER::fobj_client_var	client_v;
	uint32_t			server_incarn;
	fobjplus_ii			*fobjp;
	PXFS_VER::fobjplus_var		fobj_v;
};

//
// fobjplus_ii - this class adds support for client file objects that
// cache information. Any client file object that caches information
// at a minimum can cache file attributes and the result of access
// operations.
//
class fobjplus_ii : public fobj_ii {
	friend class clear_attr_dirty_task;
public:
	// Constructor is protected.

	virtual ~fobjplus_ii();
	void	unreferenced(int bktno, bool fidlist_locked);

	// Return a new fobj CORBA reference.
	virtual PXFS_VER::fobj_ptr	get_fobjref() = 0;

	// Return a new fobjplus CORBA reference.
	PXFS_VER::fobjplus_ptr	get_fplus_objref();

	// Create bind info.
	virtual PXFS_VER::fobj_client_ptr	get_bind_info(
	    PXFS_VER::bind_info		&binfo,
	    PXFS_VER::fobj_client_ptr	client_p,
	    cred_t			*credp,
	    Environment			&env);

	// Create bind info.
	PXFS_VER::fobj_client_ptr		get_bind_info_no_attr(
	    PXFS_VER::bind_info		&binfo,
	    PXFS_VER::fobj_client_ptr	client_p,
	    cred_t			*credp,
	    Environment			&env);

	// Returns whether the client could cache information
	virtual bool	can_cache();

	// Returns whether data page caching is on or off.
	virtual bool	is_cached();

	//
	// Downgrade attribute caches to not conflict with 'req_rights'.
	//
	virtual sol::error_t	downgrade_attr_all(
	    PXFS_VER::attr_rights req_rights, bool inval_access_check,
	    nodeid_t skip_node);

	//
	// Helper function to write attributes.
	// Call with lock_attributes() held.
	//
	int	setattr(vattr *vattrp, int attrflags, cred_t *credp);

	//
	// Downgrade the client's rights to a value that doesn't conflict with
	// the requested rights.
	// Note that this can cause attributes to be updated (write back cache).
	//
	sol::error_t	downgrade_attr(
		    client_set::client_entry	*entryp,
		    PXFS_VER::attr_rights	req_rights,
		    bool			inval_access_check,
		    Environment			&env);

	// Clear the attr_dirty flag on a client.
	void clear_attr_dirty(PXFS_VER::fobj_client_var client_v,
	    uint32_t server_incarn);

	// Common code for get_all_attr() and fobj_ii::get_bind_info().
	void	get_attr_common(cred_t *crp, PXFS_VER::attr_rights rights,
		    sol::vattr_t &attributes, uint64_t &seqnum,
		    bool get_list_lock, Environment &env);

	// Convert an fobjplus from secondary to primary representation.
	virtual int	convert_to_primary();

	// Convert an fobjplus from primary to secondary representation.
	virtual int	convert_to_secondary();

	//
	// Return true if this is a dead replicated file system object
	// (i.e., there was a fatal error in convert_to_primary() or
	// convert_to_secondary() but the replica manager still thinks
	// the service is alive).
	// If true, the environment is also set to return an exception.
	//
	virtual bool		is_dead(Environment &env);

	//
	// Helper function for _FIOISBUSY.
	// Returns
	//	1 =+ busy
	//	0 == not busy
	//	-1 == error
	//
	virtual int	is_it_busy();

	bool		fast_remove_test(ID_node &src_node);

	void		do_purge_dead_clients();

	virtual void	acquire_token_locks();
	virtual void	release_token_locks();

	virtual void	remove_client_privileges(nodeid_t node_id);

	virtual void	undo_register_client(Environment &env);

protected:
	// Unreplicated or primary constructor for the internal implementation.
	fobjplus_ii(fs_base_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation.
	fobjplus_ii(fs_base_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_type_t ftype);

	// Methods supporting IDL operations

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj, Environment &env);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	// End of methods supporting IDL operations

	//
	// Downgrade attribute caches to not conflict with 'req_rights'.
	//
	sol::error_t	downgrade_attr_all_common(
	    PXFS_VER::attr_rights req_rights, bool inval_access_check,
	    nodeid_t skip_node, bool get_list_lock);

	void		client_gone(nodeid_t client_node);

	virtual void	prepare_recover();

	void		clear_obsolete_attr(ID_node node_dirty);

	virtual void	recover_client_file_state(bool become_primary_time);

	virtual void	recovered_state(PXFS_VER::recovery_info &recovery);

private:
	//
	// Type for the return value from get_cache_call().
	// This is used to select which operation to invoke on the
	// cache when downgrading the cache's rights.
	//
	enum cache_call {
		CACHE_NOOP,		// no invocation needed
		CACHE_DROP,		// downgrade from read to nothing
		CACHE_DOWNGRADE,	// write back and downgrade to readonly
		CACHE_RELEASE,		// write back and release token
		CACHE_ERR		// impossible entry
	};

	// Invalidate access check cache.
	void		inval_access_all(nodeid_t skip_node);

	//
	// Return the type of call necessary to transition to the desired
	// rights.
	//
	cache_call	get_cache_call(PXFS_VER::attr_rights newrights,
			    PXFS_VER::attr_rights oldrights);

	//
	// Calculate new cache rights based on the rights currently held
	// by the cache and the rights requested by another cache.
	//
	PXFS_VER::attr_rights	new_rights(PXFS_VER::attr_rights current_rights,
			    PXFS_VER::attr_rights other_req_rights);

	static char	*cachecall_to_str(cache_call c);

	// Record the attribute privilegs for the specified node
	void		set_attr_rights(nodeid_t node,
			    PXFS_VER::attr_rights rights);
	void		set_attr_rights_locked(nodeid_t node,
			    PXFS_VER::attr_rights rights);

	PXFS_VER::attr_rights	get_attr_rights(nodeid_t node);

protected:
	// This set identifies the clients for this file
	client_set	clientset;

public:
	os::hrtime_t	mod_time;
	os::hrtime_t	dirty_mtime;

private:
	//
	// The data set members identifying which nodes
	// hold attributes or access cache info should
	// only be changed with holding the fobjplus_priv_lock
	//
	os::mutex_t	fobjplus_priv_lock;

	os::mutex_t	wait_for_clear_lock;
	os::condvar_t	wait_for_clear_cv;
	bool		wait_for_attr_clear;

	// Bit mask identifying nodes holding the read token.
	nodeset		attr_read_token;

	//
	// Bit mask identifying nodes holding the write token.
	// A node holding the attribute write token does
	// not hold the attribute read token at the same time.
	// The write token grants both read and write privileges.
	//
	nodeset		attr_write_token;

	// Bit mask identifying nodes having active access cache entries
	nodeset		access_cached;

	//
	// All of the remaining data members should only be changed while
	// holding the attributes lock attr_lock
	//

	//
	// The attribute sequence number is a property of the attribute token.
	// The attribute sequence number is incremented when
	// a new attribute write token is granted.
	// Any existing attribute tokens will be invalidated
	// before a new attribute write token is granted.
	//
	// The truncate operation changes the file size, which
	// is a file attribute. PXFS does not require that the client
	// obtain file attributes when issuing the truncate command.
	// Yet, the truncate operation must invalidate all cached file
	// attributes. So the truncate operation increments the
	// attribute sequence number.
	//
	// The sequence number is used to identify obsolete information.
	// A client could request valid information from the server
	// and before the reply reaches the client,
	// an invalidation request from the server could arrive.
	// The client discards any information with a sequence number
	// older than the highest sequence number from the server.
	//
	uint64_t	attr_seqnum;

private:
	//
	// The access sequence number changes when access control
	// information changes.
	//
	// The sequence number is used to identify obsolete information.
	// A client could request valid information from the server
	// and before the reply reaches the client,
	// an invalidation request from the server could arrive.
	// The client discards any information with a sequence number
	// older than the highest sequence number from the server.
	//
	uint64_t	access_seqnum;

	//
	// This value records the number of node joins the last
	// time we checked for orphans.
	//
	uint32_t	number_joins;

	// Disallow assignments and pass by value.
	fobjplus_ii(const fobjplus_ii &);
	fobjplus_ii &operator = (fobjplus_ii &);
};

#include <pxfs/server/fobjplus_impl_in.h>

#endif	// FOBJPLUS_IMPL_H
