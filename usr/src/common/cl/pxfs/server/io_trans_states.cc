//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)io_trans_states.cc	1.6	08/05/20 SMI"

#include "../version.h"
#include <pxfs/server/io_trans_states.h>

//lint -e1401
io_ioctl_state::io_ioctl_state()
{
	_done = false;
}
//lint +e1401

void
io_ioctl_state::register_new_state(Environment &env)
{
	// If this checkpoint is being sent while dumping state, don't
	// register state with the framework.
	if (secondary_ctx::extract_from(env) == NULL) {
		return;
	}

	io_ioctl_state *state = new io_ioctl_state();

	state->register_state(env);

	// The HA framework guarantees that no exception will be raised in
	// this call
	ASSERT(!env.exception());
}

//
// This static method is called on the secondary to set the results of the
// ioctl call.
//
void
io_ioctl_state::set_ioctl_results(Environment &env,
    sol::error_t err, int32_t result)
{
	secondary_ctx *ctxp = secondary_ctx::extract_from(env);
	io_ioctl_state *state =
	    (io_ioctl_state *) ctxp->get_saved_state();

	ASSERT(state != NULL);

	state->set_results(err, result);
}

//
// This static method is called on the primary to check for a retry.
//
bool
io_ioctl_state::retry(Environment &env, bool &done,
    sol::error_t &err, int32_t &result)
{
	primary_ctx *ctxp = primary_ctx::extract_from(env);
	io_ioctl_state *state =
	    (io_ioctl_state *) ctxp->get_saved_state();

	if (state != NULL) {
		// We are in a retry.
		done = state->operation_done(err, result);
		return (true);
	}

	return (false);
}

bool
io_ioctl_state::operation_done(sol::error_t &err, int32_t &result)
{
	if (_done) {
		err = _err;
		result = _result;
	}
	return (_done);
}

void
io_ioctl_state::set_results(sol::error_t err, int32_t result)
{
	_err = err;
	_result = result;
}

void
io_ioctl_state::orphaned(Environment &)
{
	// Nothing to do here
}

void
io_ioctl_state::commited()
{
	// Nothing to do here
}

io_close_state::io_close_state(sol::error_t err)
{
	_err = err;
}

sol::error_t
io_close_state::get_error()
{
	return (_err);
}

void
io_close_state::register_new_state(Environment &env,
    sol::error_t err)
{
	// If this checkpoint is being sent while dumping state, don't
	// register state with the framework.
	if (secondary_ctx::extract_from(env) == NULL) {
		return;
	}

	io_close_state *state = new io_close_state(err);

	state->register_state(env);

	// The HA framework guarantees that no exception will be raised in
	// this call
	ASSERT(!env.exception());
}

// If the environment variable passed in indicates that this call is a retry,
// then this function returns true, otherwise it returns false.
bool
io_close_state::retry(Environment &env)
{
	primary_ctx *ctxp = primary_ctx::extract_from(env);

	io_close_state *state =
	    (io_close_state *) ctxp->get_saved_state();

	if (state != NULL) {
		// We are in a retry; return the error as an exception.
		sol::error_t err = state->get_error();
		if (err != 0)
			env.exception(new sol::op_e(err));
		return (true);
	}

	return (false);
}

void
io_close_state::orphaned(Environment &)
{
	// Client died.  Simply return, rollback will be handled by the
	// _unreferenced() call.
}

void
io_close_state::commited()
{
	// We do not use this.
}
