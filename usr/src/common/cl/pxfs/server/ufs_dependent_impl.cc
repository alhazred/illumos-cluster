/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1983, 1984, 1985, 1986, 1987, 1988, 1989 AT&T	*/
/*	  All Rights Reserved	  */

/*
 * Portions of this source code were derived from Berkeley 4.3 BSD
 * under license from the Regents of the University of California.
 */

#pragma ident	"@(#)ufs_dependent_impl.cc	1.23	08/05/20 SMI"

#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/filio.h>
#include <sys/lockfs.h>
#include <sys/mntent.h>
#include <sys/vnode.h>
#include <sys/fs/ufs_filio.h>

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#define	_LEAST_PRIVS
#endif
#if defined(_LEAST_PRIVS)
#include <sys/policy.h>
#endif

#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/ufs_dependent_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/fobj_trans_states.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//lint -e666
// PXFS do an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

// Constructor.
ufs_dependent_impl::ufs_dependent_impl(fs_ii *fsp, const char *mntoptions)
{
	_fsp = fsp;

	_lockfs_info.lf_comment = NULL;
	_locking_on = false;

	ufs_dependent_impl::set_mntopts(mntoptions);

	last_sync_time = os::gethrtime();
}

// Virtual destructor.
ufs_dependent_impl::~ufs_dependent_impl()
{
	// Free up lockfs comment, if any.
	delete [] _lockfs_info.lf_comment;
} //lint !e1540 pointers are neither freed nor zero'ed by destructor

//
// Called by the constructor, or when the FS is mounted using new mount
// options (via the MS_REMOUNT flag to mount).
//
void
ufs_dependent_impl::set_mntopts(const char *mntoptions)
{
	_forcedirectio_on = false;
	_syncdir_on = false;
	_nocto_on = false;

	if (mntoptions == NULL) {
		return;
	}

	//
	// Look for the "forcedirectio" option.  If this option is true,
	// then directio mode is always enabled for open files in this
	// filesystem.  We flag the mntoption using the '_forcedirectio_on'
	// variable, and then turn off caching by setting 'cachedata' to
	// false in new_fobj(), which is called every time a new fobj is
	// created for this FS.
	//
	if (pxfslib::exists_mntopt(mntoptions, MNTOPT_FORCEDIRECTIO, false)) {
		//
		// Found the "forcedirectio" mount option.  Set the boolean
		// variable for use in 'new_fobj'.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("FS %x mounted with forcedirectio\n", _fsp));
		_forcedirectio_on = true;
	}

	if (pxfslib::exists_mntopt(mntoptions, MNTOPT_SYNCDIR, false)) {
		_syncdir_on = true;
	}

	if (pxfslib::exists_mntopt(mntoptions, MNTOPT_NOCTO, true)) {
		_nocto_on = true;
	}
}

//
// Function called by the fs_impl object right after it creates a
// new fobj object.  This function is meant to set fs-specific
// parameters for the fobj - in UFS's case, it would set the 'cachedata'
// flag based on whether the UFS mount was done with the "forcedirectio"
// option turned on.
//
void
ufs_dependent_impl::new_fobj(fobj_ii *fobjp)
{
	ASSERT(_fsp == fobjp->get_fsp());

	if (_forcedirectio_on) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("Setting cachedata to false for %x\n", fobjp));
		//
		// This UFS filesystem is mounted with the "forcedirectio"
		// mount option.  Take the directio path by default.
		//
		if (fobjp->get_ftype() == PXFS_VER::fobj_file) {
			//
			// Only file objects care about directio
			//
			file_ii		*filep = (file_ii *)fobjp;
			filep->init_cachedata_flag(false);
		}
	}
}

//
// Called from page_out and async_page_out. Retry VOP_ALLOC_DATA to ensure
// there's space on disk before writing data out. We should really do this
// only for files that are open across a failover. However, we don't have
// enough state today to know which files were open across a failover.
//
int
ufs_dependent_impl::fs_preprocess(vnode_t *vp, u_offset_t offset, size_t *len,
	    fdbuffer_t *fdb, int flags, cred_t *credp)
{
	if (!_syncdir_on) {
		int error;
		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("fs_alloc_data(fs_preprocess): vp %p off %llx len %lx\n",
		    vp, offset, *len));
		error = fs_alloc_data(vp, offset, len, fdb, flags, credp);
		if (error) {
			return (error);
		}
		//
		// We need to sync out UFS's log to disk to prevent bug
		// 4362944. The bug occurs because a pageout can complete,
		// but the inode may not have been updated with block
		// allocation information when using pxfs without syncdir.
		// So after a failover, we have a "holy" file although pages
		// have been written to disk. By flushing UFS's log to disk,
		// we ensure this file's inode has been updated on disk.
		//
		error = sync_if_necessary(os::gethrtime(), vp, credp);
		return (error);
	} else {
		return (0);
	}
}

//
// Function called by fobj_ii::cascaded_ioctl() to process ufs-specific ioctls.
// Returns true if the ioctl was processed in this function, and false
// if not.
//
// Keep in mind, that due to 4408967 (switchover/failover of locked filesystem
// hangs), we now do our _freeze_in_progress() check here, on a case by case
// basis (vs. how it was done in the past, were we assumed all ioctls were
// cascaded.
//
// Cascaded ioctls (where we depend on another invocation -- an example would be
// the directio case, where we need to flush everyone's cache) need this check
// to prevent deadlock.
//
// Others, like the lockfs ioctl (lockfs -u, in particular, was the motivation
// behind 4408967), are not cascaded and need to make it through to the
// underlying UFS filesystem so we can unblock freeze_primary.
//
bool
ufs_dependent_impl::process_cascaded_ioctl(sol::nodeid_t,
    fobj_ii *fobjp, int32_t iocmd, sol::intptr_t arg, int32_t flag,
    cred_t *crp, int *result, int &error, Environment &env)
{
	struct lockfs	fs_lockfs;

	ASSERT(_fsp == fobjp->get_fsp());

	fs_lockfs.lf_comment = NULL;

	switch (iocmd) {
	case _FIOSATIME:
		error = ioctl_fiosatime(fobjp, arg, flag, result,
		    crp, env);
		return (true);

	case _FIOLFS:
		// Special handling is required for _FIOLFS only if PXFS is HA
		if (_fsp->is_replicated()) {
			if (!((fs_repl_impl *)_fsp)->_freeze_in_progress(
			    &env)) {

				// We only sync when it is a write lock
#if defined(_LEAST_PRIVS)
				error = get_lockfs_user_params(arg, flag,
				    crp, fs_lockfs, fobjp->get_vp());
#else
				error = get_lockfs_user_params(arg, flag,
				    crp, fs_lockfs);
#endif
				if (error) {
					if (fs_lockfs.lf_comment != NULL) {
						delete [] fs_lockfs.lf_comment;
					}
					return (true);
				}

				if (LOCKFS_WLOCK == fs_lockfs.lf_lock) {
					_fsp->sync_fs(crp);
#if	SOL_VERSION >= __s11
					error = VOP_IOCTL(fobjp->get_vp(),
					    _FIOFFS, arg, flag, crp, result,
					    NULL);
#else
					error = VOP_IOCTL(fobjp->get_vp(),
					    _FIOFFS, arg, flag, crp, result);
#endif
				}
			}
			env.clear();

			error = ioctl_fiolfs(fobjp, arg, flag, result, crp,
			    env);
			return (true);
		}
		break;

	case _FIOFFS:
		if (_fsp->is_replicated() &&
		    ((fs_repl_impl *)_fsp)->_freeze_in_progress(&env)) {
				return (true);
		}
		_fsp->sync_fs(crp);
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result, NULL);
#else
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result);
#endif
		return (true);

	case _FIOSDIO:
		if (_fsp->is_replicated() &&
		    ((fs_repl_impl *)_fsp)->_freeze_in_progress(&env)) {
				return (true);
		}
		_fsp->sync_fs(crp);
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result, NULL);
#else
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result);
#endif
		return (true);

	case _FIODIRECTIO:
		if (_fsp->is_replicated() &&
		    ((fs_repl_impl *)_fsp)->_freeze_in_progress(&env)) {
				return (true);
		}
		fobjp->range_lock();
		FAULTPT_PXFS(FAULTNUM_PXFS_FIODIRECTIO_S_B,
		    FaultFunctions::generic);
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result, NULL);
#else
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result);
#endif
		if (error == 0) {
			ASSERT(arg == DIRECTIO_ON || arg == DIRECTIO_OFF);

			if (_forcedirectio_on && arg == DIRECTIO_OFF) {
				//
				// If this FS was mounted with "forcedirectio",
				// and some app. is trying to turn off direct
				// io, ignore that call.  This is what UFS
				// does.
				//
			} else {
				if (fobjp->get_ftype() == PXFS_VER::fobj_file) {
					//
					// Only file objects care about directio
					//
					file_ii	*filep = (file_ii *)fobjp;
					filep->dio_writes.wrlock();
					error = filep->set_cachedata_flag(
					    arg == DIRECTIO_OFF, env);
					filep->dio_writes.unlock();
				}
			}
		}
		FAULTPT_PXFS(FAULTNUM_PXFS_FIODIRECTIO_S_A,
		    FaultFunctions::generic);
		fobjp->range_unlock();
		return (true);

	case _FIOLOGDISABLE:
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result, NULL);
#else
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, crp,
		    result);
#endif
		return (true);

	case _FIOISBUSY: {
		//
		// Contract-private interface for Legato.
		// No ioctl is sent down to the underlying filesystem
		// because, it will always return failure to this
		// legato specific ioctl. Instead, pxfs tries to find
		// out if this file has been opened on pxfs clients.
		//
		uint32_t ret_val;
#if defined(_LEAST_PRIVS)
		if (secpolicy_fs_config(crp, (fobjp->get_vp())->v_vfsp) != 0) {
#else
		if (! suser(crp)) {
#endif
			error = EPERM;
		} else {
			ret_val = (uint32_t)fobjp->is_it_busy();
			if (suword32((int *)arg, ret_val)) {
				error = EFAULT;
			} else {
				error = 0;
			}
		}
		return (true);
	}

	case _FIOTUNE:
		// Tune the file system's atrributes.
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg,
			    flag, crp, result, NULL);
#else
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg,
			    flag, crp, result);
#endif
		FAULTPT_PXFS(FAULTNUM_PXFS_FIOTUNE_S, FaultFunctions::generic);
		if ((error == 0) && fobjp->is_replicated()) {
			//
			// If the ioctl was successful, sync metadata
			// so that the in-memory version of the superblock
			// is committed to disk.
			//
#if	SOL_VERSION >= __s11
			error = VOP_FSYNC(fobjp->get_vp(), FNODSYNC, crp, NULL);
#else
			error = VOP_FSYNC(fobjp->get_vp(), FNODSYNC, crp);
#endif
		}
		return (true);
	default:
		break;
	}

	// The ioctl was not processed in a fs-specific manner.
	return (false);
}

void
ufs_dependent_impl::replay_ioctl(fobj_ii *fobjp, int32_t iocmd,
    sol::intptr_t arg, int32_t flag, int32_t &result, int &error)
{
	//
	// Set the copy_args 'pid' value to '0', to indicate that the
	// kernel is performing the ioctl (see orb/copy.cc).
	//
	copy_args args(orb_conf::node_number(), 0);
	copy::setcontext(&args);

	switch (iocmd) {
	case _FIODIRECTIO:
		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("replaying directio ioctl\n"));
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, kcred,
			    &result, NULL);
#else
		error = VOP_IOCTL(fobjp->get_vp(), iocmd, arg, flag, kcred,
			    &result);
#endif
		if (error) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UFS,
			    PXFS_RED,
			    ("error replaying directio ioctl %d\n", error));
		}
		return;

	default:
		break;
	}

	// Trap future references
	copy::setcontext(NULL);
}

//
// Function called by fs_ii::convert_to_primary() to do any fs-specific
// conversions.
//
int
ufs_dependent_impl::convert_to_primary(fs_ii *fsp)
{
	int error = 0;

	// Replay the filesystem lock, if any existed.
	if (_locking_on) {
		// Make a local copy.
		struct lockfs lkfs;
		lkfs = _lockfs_info;

		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("ufs_dependent_impl::convert_to_primary: "
		    "lkfs = %p", &lkfs));
		error = fs_dependent_impl::kernel_ioctl(fsp->get_vfsp(),
		    _FIOLFS, (intptr_t)&lkfs);
		if (error != 0) {
			//
			// This is fatal because a locked filesystem is now
			// unlocked after a *transparent* failure or switchover.
			// We should not become the primary.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_UFS,
			    PXFS_RED,
			    ("ufs_dependent_impl::"
			    "convert_to_primary: replaying of lock failed "
			    "- error %d\n", error));
			return (error);
		}
	}
	fiolog_t arg;

	arg.nbytes_requested = 0;
	arg.nbytes_actual = 0;
	arg.error = FIOLOG_ENONE;

	if (!(fsp->device_is_lofi())) {
		error = fs_dependent_impl::kernel_ioctl(fsp->get_vfsp(),
		    _FIOLOGENABLE, (intptr_t)&arg);

		if (error != 0) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_UFS,
			    PXFS_RED,
			    ("ufs_dependent_impl::"
			    "convert_to_primary: logging ioctl"
			    "failed - error %d\n", error));
			return (error);
		}
	}

	//
	// Force the file system information back to the server
	// during recovery mode.
	//
	common_threadpool::the().defer_processing(new fs_recovery_task(
	    fsp, fsp->get_server_incn()));

	return (0);
}

void
ufs_dependent_impl::freeze_primary(const char *fs_name)
{
	//
	// Try to grab the _lockfs_lock mutex. This mutex could be protecting
	// the _locking_on boolean.
	//
	_lockfs_lock.lock();
	while (_locking_on) {
		char nodename[32];

		(void) sprintf(nodename, "Node (%u)", orb_conf::node_number());

		os::sc_syslog_msg msg(SC_SYSLOG_FILESYSTEM_TAG,
		    nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// The file system has been locked with the _FIOLFS ioctl. It
		// is necessary to perform an unlock _FIOLFS ioctl. The
		// growfs(1M) or lockfs(1M) command may be responsible for
		// this lock.
		// @user_action
		// An _FIOLFS LOCKFS_ULOCK ioctl is required to unlock the
		// file system.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "Filesystem (%s) is locked and cannot be frozen",
		    fs_name);
		_fiolfs_cv.wait(&_lockfs_lock);
	}
	_lockfs_lock.unlock();
}

//
// Helper function for dumping state to a new secondary.
//
void
ufs_dependent_impl::dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
    Environment &env)
{
	if (_locking_on) {
		ckptp->ckpt_lockfs_info((uint64_t)_lockfs_info.lf_lock,
		    (uint64_t)_lockfs_info.lf_flags,
		    (uint64_t)_lockfs_info.lf_key,
		    _lockfs_info.lf_comment, env);
		env.clear();
	}
}

//
// ioctl_fiosatime - Helper function to perform the '_FIOSATIME' ufs ioctl.
// The UFS _FIOSATIME ioctl sets the access time of the file it is performed
// on.  We need to downgrade the attribute caches before letting the ioctl
// through to UFS.  Also, since UFS only does the update in-memory, PXFS needs
// to perform a sync on the file if the ioctl completes successfully.
//
sol::error_t
ufs_dependent_impl::ioctl_fiosatime(fobj_ii *fobjp, sol::intptr_t arg,
    int32_t flag, int *ret_val, cred_t *crp, Environment &)
{
	int	error;
	vnode_t *vp = fobjp->get_vp();

	//
	// The _FIOSATIME sets the access time attribute of the vnode.  Before
	// we issue the ioctl, we need to downgrade the attribute caches just
	// like in fobj_ii::set_attributes()
	//
	fobjp->attr_lock.wrlock();

	error = fobjp->downgrade_attr_all(PXFS_VER::attr_write, false, 0);
	if (error != 0) {
		fobjp->attr_lock.unlock();
		return (error);
	}

	// Issue the ioctl.
#if	SOL_VERSION >= __s11
	error = VOP_IOCTL(vp, _FIOSATIME, arg, flag, crp, ret_val, NULL);
#else
	error = VOP_IOCTL(vp, _FIOSATIME, arg, flag, crp, ret_val);
#endif

	if ((error == 0) && fobjp->is_replicated()) {
		//
		// If the ioctl was successful, write out the updated value to
		// disk so that if there is a failover/switchover, the
		// attribute value will remain consistent.
		// XXX This is currently needed because UFS logging writes
		// the log asynchronously.
		// XXX is FDSYNC really correct ?
		//
		error = do_fsync(vp, crp);
	}

	fobjp->attr_lock.unlock();
	return (error);
}

//
// Helper function to perform the '_FIOLFS' ufs ioctl.
// The UFS _FIOLFS ioctl performs file-system locking.  If the fs being locked
// is a HA fs, then PXFS needs to keep track of the current locking state of the
// fs, and further needs to replay this lock when a failover/switchover of the
// filesystem happens.  This implies that we need to checkpoint the fs locking
// error over to the PXFS secondaries - this is done in this routine and
// also when a new secondary is added to this service.
//
sol::error_t
ufs_dependent_impl::ioctl_fiolfs(fobj_ii *fobjp, sol::intptr_t arg,
    int32_t flag, int *ret_val, cred_t *crp, Environment &env)
{
	int error;
	struct lockfs fs_lockfs;
	fobj_lockfs_state *state_obj;
	int state;
	sol::error_t err;
	vnode_t *vp = fobjp->get_vp();

	ASSERT(fobjp->get_fsp() == _fsp);
	ASSERT(fobjp->is_replicated());

	if ((state_obj = fobj_lockfs_state::retry(env)) != NULL) {
		//
		// This call is a retry.  Get the stored data from the state
		// object.  Note that it is possible to reconstruct 'fs_lockfs'
		// from the arguments to ioctl_fiolfs, but that is slower than
		// just copying it out of the state object.
		//
		state_obj->get_args(&fs_lockfs, &state);

		switch (state) {
		case fobj_lockfs_state::INITIAL:
			// Continue with the processing.
			break;

		case fobj_lockfs_state::COMMITED:
			//
			// The ioctl has already been replayed - we need to
			// return success.  But (sigh!) it's not as easy as
			// that - we also need to copy out the appropriate
			// value of lf_key to the user.
			// 'lf_key' is a value stored by UFS associated
			// with the current lock - UFS uses it to provide some
			// protection against multiple threads doing locking on
			// a filesystem without knowing about each other.  We,
			// of course, need to support this so that the lockfs
			// protocol is truly transparent in a HA PXFS
			// filesystem.
			//
			err = set_lockfs_user_params(arg, flag,
			    (uint64_t)_lockfs_info.lf_key);
			return (err);

		case fobj_lockfs_state::CANCELLED:
			//
			// The ioctl has completed before - just return the
			// error code.
			//
			return (state_obj->get_error());

		default:
			break;
		}
	}

	//
	// Copy the lockfs structure in from user space.
	// Note that the get_lockfs_user_params() call returns an
	// allocated buffer in fs_lockfs.lf_comment - this memory is
	// freed before returning from this routine if there is an
	// error.  If there is no error, the fs_lockfs parameter
	// (along with the allocated memory) is stored in the fs_ii
	// object by the store_lockfs_params() call below, and
	// subsequently freed by the fs object.
	//
#if defined(_LEAST_PRIVS)
	err = get_lockfs_user_params(arg, flag, crp, fs_lockfs,
	    fobjp->get_vp());
#else
	err = get_lockfs_user_params(arg, flag, crp, fs_lockfs);
#endif
	if (err != 0) {
		return (err);
	}

	//
	// this is for 4413957 -- because we call VOP_RENAME() instead of
	// VOP_REMOVE() and can't easily check for UFS delete lock enabled
	//
	if (LOCKFS_DLOCK == fs_lockfs.lf_lock) {
		return (ENOTSUP);
	}

	//
	// Checkpoint all the parameters over to the secondaries.  The
	// secondaries create a transaction object to store all the
	// parameters and then the transaction object waits for the
	// commit checkpoint to store the parameters with the fs
	// secondaries.
	//
	fobjp->get_ckpt()->ckpt_lockfs_start((uint64_t)fs_lockfs.lf_lock,
	    (uint64_t)fs_lockfs.lf_flags, (uint64_t)fs_lockfs.lf_key,
	    fs_lockfs.lf_comment, env);
	env.clear();

	//
	// We need to serialize the lockfs ioctls on a per-filesystem basis
	// so that we know which lock is curently in effect on the underlying
	// filesystem.  This guards for the race-condition if two lockfs
	// calls get into UFS and the last one out of UFS is not the last one
	// checkpointed across to the secondaries.
	//
	_lockfs_lock.lock();

#if	SOL_VERSION >= __s11
	error = VOP_IOCTL(vp, _FIOLFS, arg, flag, crp, ret_val, NULL);
#else
	error = VOP_IOCTL(vp, _FIOLFS, arg, flag, crp, ret_val);
#endif

	if (error == 0) {
		//
		// Success.  We must now store the parameters we used to
		// lock the filesystem on the primary, and also commit this
		// transaction on the secondaries.  We do this to enable PXFS
		// to replay the ioctl during a switchover/failover
		//
		// there are 2 cases: lockfs -u and everyone else
		//
		if (LOCKFS_ULOCK == fs_lockfs.lf_lock) {
			_locking_on = false;
		} else {
			_locking_on = true;
		}
		store_lockfs_params(fs_lockfs);
		fobjp->commit(env);
		env.clear();
		_fiolfs_cv.broadcast();
	} else {
		//
		// The ioctl failed.  Cancel the transaction on the secondaries
		// Note that we are sending the error code over - this is
		// returned to the user if there is a retry of this call.
		//
		fobjp->get_ckpt()->ckpt_lockfs_failure(error, env);
		env.clear();
		delete [] fs_lockfs.lf_comment;
	}

	_lockfs_lock.unlock();

	return (error);
}

//
// Helper function to copy the user parameters to the _FIOLFS ioctl into kernel
// space.
//
#if defined(_LEAST_PRIVS)
sol::error_t
ufs_dependent_impl::get_lockfs_user_params(sol::intptr_t uarg, int32_t flag,
    cred_t *crp, struct lockfs &lkfs, vnode_t *vp)
#else
sol::error_t
ufs_dependent_impl::get_lockfs_user_params(sol::intptr_t uarg, int32_t flag,
    cred_t *crp, struct lockfs &lkfs)
#endif
{
	char *comment;

	//
	// NOTE: This code is adapted from the ufs code that handles the
	// '_FIOLFS' ioctl in ufs_vnops.c.  If that code changes in any way,
	// this should change with it.
	//
#if defined(_LEAST_PRIVS)
	if (secpolicy_fs_config(crp, vp->v_vfsp) != 0) {
#else
	if (! suser(crp)) {
#endif
		return (EPERM);
	}

	if ((flag & DATAMODEL_MASK) == DATAMODEL_NATIVE) {
		if (copyin((caddr_t)uarg, &lkfs, sizeof (struct lockfs))) {
			return (EFAULT);
		}
	}
#ifdef _SYSCALL32_IMPL
	else {
		struct lockfs32 lkfs32;
		/* Translate ILP32 lockfs to LP64 lockfs */
		if (copyin((caddr_t)uarg, &lkfs32, sizeof (struct lockfs32)))
			return (EFAULT);
		lkfs.lf_lock = (ulong_t)lkfs32.lf_lock;
		lkfs.lf_flags = (ulong_t)lkfs32.lf_flags;
		lkfs.lf_key = (ulong_t)lkfs32.lf_key;
		lkfs.lf_comlen = (ulong_t)lkfs32.lf_comlen;
		lkfs.lf_comment = (caddr_t)lkfs32.lf_comment;
	}
#endif /* _SYSCALL32_IMPL */

	if (lkfs.lf_comlen) {
		if (lkfs.lf_comlen > LOCKFS_MAXCOMMENTLEN) {
			return (ENAMETOOLONG);
		}
		comment = new char[lkfs.lf_comlen];
		if (copyin(lkfs.lf_comment, comment, lkfs.lf_comlen)) {
			delete [] comment;
			return (EFAULT);
		}
		lkfs.lf_comment = comment;
	} else {
		lkfs.lf_comment = NULL;
	}

	return (0);
}

//
// Helper function used to copy out the current value of 'lf_key' to the user.
// This function only needs to be called if a retry comes in for a committed
// transaction.
//
sol::error_t
ufs_dependent_impl::set_lockfs_user_params(sol::intptr_t uarg, int32_t flag,
    uint64_t lf_key)
{
	//
	// This function is modelled after ufs handling of the _FIOLFS ioctl.
	// If you plan on making any changes here, look at ufs first.
	//
	if ((flag & DATAMODEL_MASK) == DATAMODEL_NATIVE) {
		struct lockfs lkfs;

		// Copy in the arguments.
		if (copyin((caddr_t)uarg, &lkfs, sizeof (struct lockfs))) {
			return (EFAULT);
		}
		// Set the key value to the current value.
		lkfs.lf_key = (ulong_t)lf_key;

		// ... and copy out.
		(void) copyout(&lkfs, (caddr_t)uarg, sizeof (struct lockfs));
	}
#ifdef _SYSCALL32_IMPL
	else {
		// Do the same thing in 32 bits.
		struct lockfs32 lkfs32;

		// Copy in the arguments.
		if (copyin((caddr_t)uarg, &lkfs32, sizeof (struct lockfs32)))
			return (EFAULT);

		// Set the key value to the current value.
		lkfs32.lf_key = (uint32_t)lf_key;

		// ... and copy out.
		(void) copyout(&lkfs32, (caddr_t)uarg,
		    sizeof (struct lockfs32));
	}
#endif /* _SYSCALL32_IMPL */

	return (0);
}

//
// This is where the ckpt_lockfs_state() checkpoint ends up.
// We store the fs locking parameters in the '_lockfs_info' member.
// 'lockfs_info' is used during failovers/switchovers to replay the locking
// ioctl.
//
void
ufs_dependent_impl::ckpt_lockfs_state(uint64_t lf_lock, uint64_t lf_flags,
    uint64_t lf_key, const char *lf_comment)
{
	ASSERT(_fsp->is_replicated());
	ASSERT(_fsp->is_secondary());

	if (LOCKFS_ULOCK != lf_lock) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("locked via ckpt_lockfs_state()\n"));
		_locking_on = true;
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_UFS,
		    PXFS_GREEN,
		    ("unlocked via ckpt_lockfs_state()\n"));
		_locking_on = false;
	}

	delete [] _lockfs_info.lf_comment;

	_lockfs_info.lf_lock = (ulong_t)lf_lock;
	_lockfs_info.lf_flags = (ulong_t)lf_flags;
	_lockfs_info.lf_key = (ulong_t)lf_key;
	if (lf_comment == NULL) {
		_lockfs_info.lf_comlen = 0;
		_lockfs_info.lf_comment = NULL;
	} else {
		_lockfs_info.lf_comlen = strlen(lf_comment) + 1;
		_lockfs_info.lf_comment = new char[_lockfs_info.lf_comlen];
		(void) strcpy(_lockfs_info.lf_comment, lf_comment);
	}
}

//
// Called from fobj_ii::ioctl_fiolfs to set the locking parameters of the
// underlying filesystem.
//
void
ufs_dependent_impl::store_lockfs_params(struct lockfs &lkfs)
{
	ASSERT(_lockfs_lock.lock_held());
	ASSERT(_fsp->is_replicated());
	ASSERT(!_fsp->is_secondary());

	delete [] _lockfs_info.lf_comment;

	_lockfs_info = lkfs;
}

extern "C" int ufs_alloc_data(vnode_t *vp, u_offset_t offset, size_t *len,
    fdbuffer_t *fdb, int flags, cred_t *cr);
extern "C" int ufs_rdwr_data(vnode_t *vp, u_offset_t offset, size_t len,
    fdbuffer_t *fdb, int flags, cred_t *cr);

int
ufs_dependent_impl::fs_alloc_data(vnode_t *vp, u_offset_t offset, size_t *len,
    fdbuffer_t *fdb, int flags, cred_t *cr)
{
	//
	// If this is a NFS thread, unset the T_DONTPEND flag so that
	// ufs_alloc_data remains asynchronous.
	//
	bool nfs_thread = (bool)(curthread->t_flag & T_DONTPEND);

	if (nfs_thread) {
		curthread->t_flag &= ~T_DONTPEND;
	}
	PXFS_DBPRINTF(
	    PXFS_TRACE_UFS,
	    PXFS_GREEN,
	    ("fs_alloc_data: vp %p off %llx len %lx\n",
	    vp, offset, *len));
	int error = ufs_alloc_data(vp, offset, len, fdb, flags, cr);
	if (nfs_thread) {
		curthread->t_flag |= T_DONTPEND;
	}

	return (error);
}

int
ufs_dependent_impl::fs_rdwr_data(vnode_t *vp, u_offset_t offset, size_t len,
    size_t, fdbuffer_t *fdb, int flags, cred_t *cr)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_UFS,
	    PXFS_GREEN,
	    ("fs_rdwr_data: %s%s vp %p off %llx len %lx\n",
	    flags & B_ASYNC ? "a" : "",
	    flags & B_READ ? "read" : "write",
	    vp, offset, len));

	return (ufs_rdwr_data(vp, offset, len, fdb, flags, cr));
}

//
// fs_fsync - when "syncdir" option isn't enabled and this is an HA file system,
// we need to flush the UFS log to disk so we don't lose file meta-data
// after a failover.
//
int
ufs_dependent_impl::fs_fsync(vnode_t *vnodep, cred_t *credp)
{
	int		error = 0;

	if (!_fsp->is_replicated() || _syncdir_on) {
		// Nothing to do.
		return (0);
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_SYNCDIR, FaultFunctions::generic);
	//
	// We store the time we started doing the FSYNC and assign it to
	// last_sync_time only after VOP_FSYNC completes. This will make
	// threads that came in when a VOP_FSYNC was in progress to wait
	// until VOP_FSYNC completion.
	//
	os::hrtime_t	tmp_last_sync_time = os::gethrtime();

	//
	// ufs_alloc_data() creates async log transactions. ufs_fsync() is
	// optimized for NFS threads to skip fsync if the last transaction
	// for that thread is already commited. PxFS depends on *all*
	// transactions created before ufs_fsync was called being commited.
	// The optimization for NFS breaks this guarantee. To work around
	// the above problem, we clear T_DONTPEND flag on this thread to
	// make this thread look like a non-NFS thread.
	//
	bool nfs_thread = (bool)(curthread->t_flag & T_DONTPEND);
	if (nfs_thread) {
		curthread->t_flag &= ~T_DONTPEND;
	}
#if	SOL_VERSION >= __s11
	error = VOP_FSYNC(vnodep, FNODSYNC, credp, NULL);
#else
	error = VOP_FSYNC(vnodep, FNODSYNC, credp);
#endif
	if (nfs_thread) {
		curthread->t_flag |= T_DONTPEND;
	}

	last_sync_time = tmp_last_sync_time;

	return (error);
}

//
// do_fsync - sync all information to disk.
//
int
ufs_dependent_impl::do_fsync(vnode_t *vnodep, cred_t *credp)
{
	int		error;
	sync_lock.wrlock();

	//
	// We store the time we started doing VOP_FSYNC and assign it
	// to last_sync_time only after VOP_FSYNC completes. This will
	// make threads that came in when a VOP_FSYNC was in progress
	// to wait until VOP_FSYNC completion.
	//
	os::hrtime_t	tmp_last_sync_time = os::gethrtime();

	//
	// If this is a NFS thread, unset the T_DONTPEND flag so that
	// the sync operation will not be skipped by optimizations
	// for NFS that do not apply to Pxfs.
	//
	bool nfs_thread = (bool)(curthread->t_flag & T_DONTPEND);

	if (nfs_thread) {
		curthread->t_flag &= ~T_DONTPEND;
	}
#if	SOL_VERSION >= __s11
	error = VOP_FSYNC(vnodep, FSYNC, credp, NULL);
#else
	error = VOP_FSYNC(vnodep, FSYNC, credp);
#endif
	if (nfs_thread) {
		curthread->t_flag |= T_DONTPEND;
	}

	last_sync_time = tmp_last_sync_time;

	sync_lock.unlock();
	return (error);
}

//
// Sync. out the in-memory log if we don't know that it was pushed to disk
// after 'mod_time'.
//
int
ufs_dependent_impl::sync_if_necessary(os::hrtime_t &mod_time, vnode *vnodep,
    cred_t *credp)
{
	int	error = 0;

	if (mod_time >= last_sync_time) {
		sync_lock.wrlock();
		if (mod_time >= last_sync_time) {
			error = fs_fsync(vnodep, credp);
		}
		sync_lock.unlock();
	}

	return (error);
}
