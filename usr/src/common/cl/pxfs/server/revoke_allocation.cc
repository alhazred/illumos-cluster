//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)revoke_allocation.cc	1.7	08/05/20 SMI"

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/debug.h>
#include <sys/errno.h>

#include <sys/os.h>
#include <h/bulkio.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include PXFS_IDL(pxfs)
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/revoke_allocation.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

threadpool revoke_allocation_threadpool(true, 16, "revoke_allocation");

//lint -e1401
revoke_allocation_task::revoke_allocation_task(
    fs_ii *fsp,
    PXFS_VER::filesystem_ptr fs_p,
    uint32_t server_incn,
    PXFS_VER::fsmgr_client_var fsmgr_client_v) :
	fs_v(fs_p),
	server_incarn(server_incn)
{
	fs_iip = fsp;
	clientmgr_v = fsmgr_client_v;
}
//lint +e1401

//lint -e1740
revoke_allocation_task::~revoke_allocation_task()
{
}
//lint +e1740

//
// This is called when the pool of threads process the queued request.
// Notify the client that it needs to sync its dirty pages.
// Once all the clients are have synced their pages inform that sync is
// completed.
//
void
revoke_allocation_task::execute()
{
	// Revoke all reservations from all the clients.
	solobj::cred_var	credobj = solobj_impl::conv(kcred);
	Environment		env;

	PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_AMBER,
	    ("revoke_task(%p) execute start revoke_in_progress\n",
	    this));

	clientmgr_v->revoke_allocation(credobj, env);
	env.clear();

	//
	// Find the version of pxfs service.
	//
	version_manager::vp_version_t	pxfs_version;
	(void) pxfslib::get_running_version("pxfs", &pxfs_version);

	if (((pxfs_version.major_num == 2) && (pxfs_version.minor_num > 0)) ||
	    (pxfs_version.major_num > 2)) {

		fs_v->revoke_completed(server_incarn, env);

	} else {
		//
		// Fallback to old approach since revoke_completed is not
		// supported by fs_impl.
		//
		fs_iip->revoke_completed_main();
	}

	PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_AMBER,
	    ("revoke_task(%p) execute end revoke_in_progress\n",
	    this));

	delete this;
}
