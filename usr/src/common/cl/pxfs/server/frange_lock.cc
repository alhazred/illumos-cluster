//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)frange_lock.cc	1.9	08/09/04 SMI"

#include <sys/errno.h>
#include <sys/flock.h>
#include <sys/kmem.h>

#include <sys/sol_conv.h>
#include <orb/infrastructure/orb_conf.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/cl_flock.h>
#include <pxfs/server/fs_base_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

// Initialize static per node lock
os::rwlock_t	fs_base_ii::per_node_lock;

#ifdef	DEBUG
os::mutex_t cb_count_lock;
int cb_count = 0;
#endif

frange_lock::frange_lock(fobj_ii *fobjiip) :
	fobj_iip(fobjiip)
{
}

frange_lock::~frange_lock()
{
	active_locks.dispose();
	cb_obj_list.dispose();
	processed_list.dispose();

} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// This checkpoint gets invoked on the secondary by frlock() and remove_locks()
// on the primary.
//
void
frange_lock::ckpt_locks(const REPL_PXFS_VER::lock_info_seq_t &locks)
{
	active_locks_lock.wrlock();
	new_locks(locks);
	active_locks_lock.unlock();
}

//
// Effect: Processes the lock depending on its state.  Called by
// frlock_execute_request().
//
sol::error_t
frange_lock::process_pxfs_lock(lock_descriptor_t *lock, bool dead_client)
{
	int error;

	graph_t	*gp = lock->l_graph;
	mutex_enter(&gp->gp_mutex);

	// Process the lock.

	// Granted locks from dead clients are marked as interrupted.
	if (dead_client && (IS_GRANTED(lock))) {
		flk_set_state(lock, FLK_INTERRUPTED_STATE);
	}

	//
	// If the request is an NLM server lock request and
	// the request has not yet been granted, and the NLM
	// state of the lock request is not NLM_UP (NLM server
	// is shutting down), then cancel the sleeping lock
	// and return error ENOLCK that will encourage the
	// client to retransmit.
	//
	if (IS_LOCKMGR(lock) && !IS_NLM_UP(lock) && !IS_GRANTED(lock)) {
		flk_cancel_sleeping_lock(lock, 1);
		error = ENOLCK;

	} else if (IS_INTERRUPTED(lock)) {
		// We got a signal, or act like we did.
		flk_cancel_sleeping_lock(lock, 1);
		error = EINTR;

	} else if (IS_CANCELLED(lock)) {
		// Cancelled if some other thread has closed the file.
		flk_cancel_sleeping_lock(lock, 1);
		error = EBADF;

	} else {
		ASSERT(!dead_client);
		// Granted lock - just process it.
		processed_list_lock.lock();
		(void) processed_list.erase(lock);
		processed_list_lock.unlock();

		REMOVE_SLEEP_QUEUE(lock);
		error = flk_execute_request(lock);
	}

	mutex_exit(&gp->gp_mutex);
	return (error);
}

//
// This static function gets called when a lock is added to the active list
// of the LLM on this node.  We need to store this lock, and checkpoint
// it to all the secondaries.  We do this by adding it to the
// 'current_locks' list, which will be processed when 'end_locking' is
// called.
//
void
frange_lock::lock_active(lock_descriptor_t *lock)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)active(%d)(%d, %d)(sysid=0x%x),(pid=%d)\n",
	    this,  (int)lock->l_type, (int)lock->l_start, (int)lock->l_end,
	    lock->l_flock.l_sysid, lock->l_flock.l_pid));

	if (fobj_iip->fs_implp->suspend_locking_ckpts()) {
		//
		// We are the new primary in the middle of a
		// switchover/failover - these locks are being replayed and do
		// not need to be checkpointed.
		//
		return;
	}

	REPL_PXFS_VER::lock_info_t	*new_lock = get_lock_info(lock);
	new_lock->state = REPL_PXFS_VER::added;

	ASSERT(active_locks_lock.write_held() ||
	    fobj_iip->fs_implp->llm_lock.write_held());
	current_locks.append(new_lock);
}

//
// This static function gets called when a lock is deleted from the active list
// of the LLM on this node.  We need to remove this lock from our 'active_list',
// and chekpoint this removal to the secondaries.  We do this by adding it to
// 'current_locks', which will be processed when 'end_locking' is called.
//
void
frange_lock::lock_deleted(lock_descriptor_t *lock)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)deleted (%d) (%d, %d)\n",
	    this, (int)lock->l_type, (int)lock->l_start, (int)lock->l_end));

	if (fobj_iip->fs_implp->suspend_locking_ckpts()) {
		//
		// We are in the middle of cleaning up locks held by a dead
		// PXFS client.  These locks do not need to be checkpointed
		// here.
		//
		return;
	}

	REPL_PXFS_VER::lock_info_t	*del_lock = get_lock_info(lock);
	del_lock->state = REPL_PXFS_VER::deleted;

	ASSERT(active_locks_lock.write_held() ||
	    fobj_iip->fs_implp->llm_lock.write_held());
	current_locks.append(del_lock);
}

//
// Returns true if the given callback object is used by one of the locks in
// 'processed_list'.
//
bool
frange_lock::is_in_processed_list(PXFS_VER::pxfs_llm_callback_ptr cb)
{
	lock_context_t			*lock_contextp;
	PXFS_VER::pxfs_llm_callback_ptr	lock_cb;
	lock_descriptor_t		*lock;
	bool				found = false;

	processed_list_lock.lock();

	for (processed_list.atfirst();
	    (lock = processed_list.get_current()) != NULL;
	    processed_list.advance()) {
		lock_contextp = CL_FLK_GET_CONTEXT(lock);
		lock_cb = lock_contextp->cb_obj_v1;
		ASSERT(!CORBA::is_nil(lock_cb));
		if (lock_cb->_equiv(cb)) {
			found = true;
			break;
		}
	}

	processed_list_lock.unlock();

	return (found);
}

//
// Call made on the primary and the secondary to clean up locks originating from
// a particular node when the node dies.  This call results from an invocation
// of fsmgr_server::unreferenced() by the framework.
//
void
frange_lock::cleanup_locks(nodeid_t id)
{
	REPL_PXFS_VER::lock_info_t *current_lock;

	active_locks_lock.wrlock();

	active_locks.atfirst();
	while ((current_lock = active_locks.get_current()) != NULL) {
		active_locks.advance();
		//lint -e737
		if (GETPXFSID(current_lock->l_flock.l_sysid) == (int)id) {
			//lint +e737
			PXFS_DBPRINTF(
			    PXFS_TRACE_FLK,
			    PXFS_GREEN,
			    ("frange_lock(%p)cleanup_locks (%d) (%d, %d)\n",
			    this,
			    (int)current_lock->l_type,
			    (int)current_lock->l_start,
			    (int)current_lock->l_end));
			(void) active_locks.erase(current_lock);
			delete(current_lock);
		}
	}

	remove_cb_objects_by_node(id);

	active_locks_lock.unlock();
}

void
frange_lock::remove_file_locks_by_sysid(int32_t sysid)
{
	REPL_PXFS_VER::lock_info_t *current_lock;

	active_locks_lock.wrlock();

	active_locks.atfirst();
	while ((current_lock = active_locks.get_current()) != NULL) {
		active_locks.advance();
		//lint -e737
		if ((int32_t)(current_lock->l_flock.l_sysid) == sysid) {
			//lint +e737
			PXFS_DBPRINTF(
			    PXFS_TRACE_FLK,
			    PXFS_GREEN,
			    ("frange_lock(%p)remove_file_locks_by_sysid (%d)"
			    "(%d, %d)\n",
			    this,
			    (int)current_lock->l_type,
			    (int)current_lock->l_start,
			    (int)current_lock->l_end));
			(void) active_locks.erase(current_lock);
			delete(current_lock);
		}
	}

	active_locks_lock.unlock();

	remove_cb_objects_by_sysid(sysid);
}

void
frange_lock::remove_file_locks_by_nlmid(int32_t nlmid)
{
	REPL_PXFS_VER::lock_info_t *current_lock;

	active_locks_lock.wrlock();

	active_locks.atfirst();
	while ((current_lock = active_locks.get_current()) != NULL) {
		active_locks.advance();
		//
		// One condition must be satisfied for a lock to be regarded
		// as an NLM lock: The "l_state" of the lock descriptor must
		// be set to LOCKMGR_LOCK. To identify whether a particular
		// NLM server running on a node is the node through which the
		// client requested the NLM lock, it is sufficient to examine
		// the upper two bytes of the "l_sysid" field in the lock
		// structure (embedded in the descriptor), extract the node id
		// and check to see whether it's equal to the argument "nlmid."
		//
		// If the lock in question is in fact an NLM lock AND
		// the upper 2 bytes of the sysid are non-zero and equal
		// to the argument "nlmid" then this lock should be removed
		// from the internal PXFS-maintained list of locks. Note
		// that it is NOT sufficient to test whether the upper two
		// bytes of the sysid field are set because PXFS local
		// locks also bear the nodeid in those bytes.
		//
		//lint -e737
		if ((current_lock->l_state & LOCKMGR_LOCK) &&
		    GETNLMID(current_lock->l_flock.l_sysid) == nlmid) {
			//lint +e737

			PXFS_DBPRINTF(
			    PXFS_TRACE_FLK,
			    PXFS_GREEN,
			    ("frange_lock(%p)remove_file_locks_by_nlmid "
			    "(pid=%d,nlmid=%d,sysid=0x%x,"
			    "state=0x%x)(type=%d)(start=%d,end=%d)\n",
				this,
				current_lock->l_flock.l_pid,
				nlmid,
				current_lock->l_flock.l_sysid,
				current_lock->l_state,
				(int)current_lock->l_type,
				(int)current_lock->l_start,
				(int)current_lock->l_end));
			(void) active_locks.erase(current_lock);
			delete(current_lock);
		}
	}

	active_locks_lock.unlock();

	remove_cb_objects_by_nlmid(nlmid);
}

//
// Add an entry to 'cb_obj_list'.
//
void
frange_lock::add_cb_object(PXFS_VER::pxfs_llm_callback_ptr cb_obj,
    int32_t sysid, bool nlmlock)
{
	cb_obj_info_t *tmp = new cb_obj_info_t;

	tmp->cb_obj = PXFS_VER::pxfs_llm_callback::_duplicate(cb_obj);
	tmp->sysid = sysid;
	tmp->nlmlock = nlmlock;

	cb_obj_list_lock.lock();
	cb_obj_list.append(tmp);
	cb_obj_list_lock.unlock();

#ifdef	DEBUG
	cb_count_lock.lock();
	cb_count++;
	cb_count_lock.unlock();
#endif
}

//
// Remove an entry from 'cb_obj_list'.
//
void
frange_lock::remove_cb_object(PXFS_VER::pxfs_llm_callback_ptr cb_obj)
{
	cb_obj_info_t *tmp;

	cb_obj_list_lock.lock();
	cb_obj_list.atfirst();
	while ((tmp = cb_obj_list.get_current()) != NULL) {
		cb_obj_list.advance();
		if (cb_obj->_equiv(tmp->cb_obj)) {
			(void) cb_obj_list.erase(tmp);
			delete tmp;
#ifdef	DEBUG
			cb_count_lock.lock();
			cb_count--;
			cb_count_lock.unlock();
#endif
			break;
		}
	}
	cb_obj_list_lock.unlock();
}

//
// Remove entries by sysid from 'cb_obj_list'.
//
void
frange_lock::remove_cb_objects_by_sysid(int32_t sysid)
{
	cb_obj_info_t *tmp;

	cb_obj_list_lock.lock();
	cb_obj_list.atfirst();
	while ((tmp = cb_obj_list.get_current()) != NULL) {
		//
		// Advance the current pointer of the list here instead of in
		// the while loop because we may erase 'tmp' below, and if
		// 'tmp' is the current object, we'll end up skipping an entry.
		//
		cb_obj_list.advance();
		if (sysid == tmp->sysid) {
			(void) cb_obj_list.erase(tmp);
			delete tmp;
#ifdef	DEBUG
			cb_count_lock.lock();
			cb_count--;
			cb_count_lock.unlock();
#endif
		}
	}
	cb_obj_list_lock.unlock();
}

//
// Remove entries by nlmid from 'cb_obj_list'.
//
void
frange_lock::remove_cb_objects_by_nlmid(int32_t nlmid)
{
	cb_obj_info_t *tmp;

	cb_obj_list_lock.lock();
	cb_obj_list.atfirst();
	while ((tmp = cb_obj_list.get_current()) != NULL) {
		//
		// Advance the current pointer of the list here instead of in
		// the while loop because we may erase 'tmp' below, and if
		// 'tmp' is the current object, we'll end up skipping an entry.
		//
		cb_obj_list.advance();

		//
		// If the callback object is for a lock that is BOTH an NLM
		// lock (first conjunction) and the "nlmid" matches the
		// upper two bytes of the sysid (second conjunction) then
		// the object is truly on behalf of a remote lock request.
		//
		//lint -e737
		if (tmp->nlmlock && (nlmid == GETNLMID(tmp->sysid))) {
			//lint +e737
			(void) cb_obj_list.erase(tmp);
			delete tmp;
#ifdef	DEBUG
			cb_count_lock.lock();
			cb_count--;
			cb_count_lock.unlock();
#endif
		}
	}
	cb_obj_list_lock.unlock();
}

//
// Remove entries by nodeid from 'cb_obj_list'.
//
void
frange_lock::remove_cb_objects_by_node(nodeid_t id)
{
	cb_obj_info_t *tmp;

	cb_obj_list_lock.lock();
	cb_obj_list.atfirst();
	while ((tmp = cb_obj_list.get_current()) != NULL) {
		//
		// Advance the current pointer of the list here instead of in
		// the while loop because we may erase 'tmp' below, and if
		// 'tmp' is the current object, we'll end up skipping an entry.
		//
		cb_obj_list.advance();

		//lint -e737
		if (id == GETPXFSID(tmp->sysid)) {
			//lint +e737

			(void) cb_obj_list.erase(tmp);
			delete tmp;
#ifdef	DEBUG
			cb_count_lock.lock();
			cb_count--;
			cb_count_lock.unlock();
#endif
		}
	}
	cb_obj_list_lock.unlock();
}

//
// Search 'cb_obj_list'.
//
bool
frange_lock::is_in_cb_object_list(PXFS_VER::pxfs_llm_callback_ptr cb_obj)
{
	cb_obj_info_t *tmp;

	cb_obj_list_lock.lock();
	for (cb_obj_list.atfirst(); (tmp = cb_obj_list.get_current()) != NULL;
	    cb_obj_list.advance()) {
		if (cb_obj->_equiv(tmp->cb_obj)) {
			cb_obj_list_lock.unlock();
			return (true);
		}
	}
	cb_obj_list_lock.unlock();
	return (false);
}

//
// Helper function called from convert_to_primary().  This function goes
// through the list of active locks and replays them on the LLM.
//
void
frange_lock::replay_active_locks()
{
	if (active_locks.count() == 0) {
		return;
	}

	REPL_PXFS_VER::lock_info_t	*current_lock;

	// Get the lock graph for this vnode, initializing if necessary
	graph_t		*gp = flk_get_lock_graph(fobj_iip->get_vp(),
			    FLK_INIT_GRAPH);
	ASSERT(gp != NULL);

	mutex_enter(&gp->gp_mutex);

	active_locks_lock.wrlock();

	for (active_locks.atfirst();
	    (current_lock = active_locks.get_current()) != NULL;
	    active_locks.advance()) {
		lock_descriptor_t *l = get_lock_descriptor(current_lock);
		l->l_graph = gp;
		(void) flk_execute_request(l);
	}

	active_locks_lock.unlock();

	mutex_exit(&gp->gp_mutex);
}

//
// Helper function called from 'unreferenced' to cleanup all locks for this
// file from the LLM.
//
void
frange_lock::remove_all_locks()
{
	ASSERT(fobj_iip->primary_ready);

	Environment		env;
	REPL_PXFS_VER::lock_info_t	*curr_lock;
	pid_t			l_pid;
	pid_t			prev_pid = 0;
	int			l_sysid;
	int			prev_sysid = 0;

	start_locking();

	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)remove_all_locks\n", this));

	//
	// Among the PXFS lists, locks move from the 'active_locks' list to
	// the 'granted_list' to the 'processed_list'.  So we need to clean
	// them up in that order to guarantee that all the lists are empty
	// in the end.
	//

	// Cleanup all the active locks first.
	ASSERT(active_locks_lock.lock_held());
	active_locks.atfirst();
	while ((curr_lock = active_locks.get_current()) != NULL) {
		l_pid = curr_lock->l_flock.l_pid;
		//
		// Use zero if the lock is not a NLM and is local, otherwise
		// use the given sysid.
		//
		if (!(curr_lock->l_state & LOCKMGR_LOCK) &&
		    (GETPXFSID(curr_lock->l_flock.l_sysid) ==
		    orb_conf::node_number())) {
			l_sysid = 0;
		} else {
			l_sysid = curr_lock->l_flock.l_sysid;
		}

		//
		// There are timing windows where we may have some locks in PXFS
		// space that are not in the LLM (if _unref is called at the
		// same time as 'nlm_pxfs::set_nlm_status').  If this is the
		// case, just clear the locks from here.
		// XXX Print out a warning for now in case there are situations
		// we can't see yet.
		//
		if ((prev_pid != 0) && (l_pid == prev_pid) &&
		    (l_sysid == prev_sysid)) {
			cmn_err(CE_WARN, "Could not cleanup locks for "
			    "(%d, %d)", (int)l_pid, (int)l_sysid);
			(void) active_locks.erase(curr_lock);
			delete(curr_lock);
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_FLK,
		    PXFS_GREEN,
		    ("frange_lock(%p)remove_all_locks pid=%d, sysid=0x%x\n",
		    this, l_pid, l_sysid));

		cleanlocks(fobj_iip->back_obj.vnodep, l_pid, l_sysid);

		//
		// 'end_locking' will cleanup from 'active_locks' all the
		// locks deleted by 'cleanlocks'.
		//
		end_locking(env);

		// Start over, looking for the next lock owner.
		start_locking();
		active_locks.atfirst();
		prev_pid = l_pid;
		prev_sysid = l_sysid;
	}

	// Cleanup the locks in the 'granted_list'.
	remove_granted_locks(fobj_iip);

	// Now, cleanup all the processed locks.
	lock_descriptor_t	*lock;
	processed_list_lock.lock();
	while ((lock = processed_list.reapfirst()) != NULL) {
		processed_list_lock.unlock();
		PXFS_DBPRINTF(
		    PXFS_TRACE_FLK,
		    PXFS_GREEN,
		    ("frange_lock(%p)remove_all_locks process_pxfs_lock (%p)\n",
		    this, lock));
		(void) process_pxfs_lock(lock, true);
		processed_list_lock.lock();
	}
	processed_list_lock.unlock();
	ASSERT(processed_list.empty());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)remove_all_locks end\n", this));

	end_locking(env);
}

//
// Helper function called from 'convert_to_secondary' to cleanup all locks for
// this device from the LLM.
//
void
frange_lock::remove_llm_locks()
{
	ASSERT(fobj_iip->primary_ready);
	ASSERT(fobj_iip->fs_implp->suspend_locking_ckpts());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)remove_llm_locks begin\n",
	    this));

	//
	// Among the PXFS lists, locks move from the 'active_locks' list to
	// the 'granted_list' to the 'processed_list'.  So we need to clean
	// them up in that order to guarantee that all the lists are empty
	// in the end.
	//

	// Cleanup all the active locks first.
	REPL_PXFS_VER::lock_info_t	*curr_lock;
	active_locks.atfirst();
	while ((curr_lock = active_locks.get_current()) != NULL) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FLK,
		    PXFS_GREEN,
		    ("frange_lock(%p)remove_llm_locks pid=%d, sysid=0x%x\n",
		    this, (int)curr_lock->l_flock.l_pid,
		    (int)curr_lock->l_flock.l_sysid));

		cleanlocks(fobj_iip->back_obj.vnodep, curr_lock->l_flock.l_pid,
		    curr_lock->l_flock.l_sysid);
		active_locks.advance();
	}

	// Cleanup the locks in the 'granted_list'.
	remove_granted_locks(fobj_iip);

	// Now, cleanup all the processed locks.
	lock_descriptor_t	*lock;
	processed_list_lock.lock();
	while ((lock = processed_list.reapfirst()) != NULL) {
		processed_list_lock.unlock();
		PXFS_DBPRINTF(
		    PXFS_TRACE_FLK,
		    PXFS_GREEN,
		    ("frange_lock(%p)remove_llm_locks call process_pxfs_lock "
		    "(%p)\n",
		    this, lock));
		(void) process_pxfs_lock(lock, true);
		processed_list_lock.lock();
	}
	processed_list_lock.unlock();
	ASSERT(processed_list.empty());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)remove_llm_locks end\n", this));
}

//
// For HA objects, this routine is called to add locks to the active lock list
// for this file.  new_locks() gets called by end_locking() after the
// VOP_FRLOCK() call returns successfully on the primary, and from
// ckpt_locks() on the secondary.
//
void
frange_lock::new_locks(const REPL_PXFS_VER::lock_info_seq_t &locks)
{
	ASSERT(active_locks_lock.write_held() ||
	    fobj_iip->fs_implp->llm_lock.write_held());

	REPL_PXFS_VER::lock_info_t	*tmp_lock = NULL;

	//
	// Walk the array of locks passed in, adding to or deleting from the
	// list, as appropriate.
	//
	for (uint_t i = 0; i < locks.length(); i++) {
		if (locks[i].state == REPL_PXFS_VER::added) {
			//
			// Allocate a new lock_info_t structure.
			// The corresponding delete is done in remove_lock().
			//
			for (active_locks.atfirst();
			    (tmp_lock = active_locks.get_current()) != NULL;
			    active_locks.advance()) {
				if ((tmp_lock->l_start == locks[i].l_start) &&
				    (tmp_lock->l_end == locks[i].l_end) &&
				    (tmp_lock->l_flock.l_sysid ==
					locks[i].l_flock.l_sysid) &&
				    (tmp_lock->l_flock.l_pid ==
					locks[i].l_flock.l_pid)) {
					// Found it - break out.
					break;
				}
			}

			if (tmp_lock == NULL) {
				tmp_lock = new REPL_PXFS_VER::lock_info_t;
				*tmp_lock = locks[i];
				active_locks.append(tmp_lock);
			}
		} else {
			ASSERT(locks[i].state == REPL_PXFS_VER::deleted);
			remove_lock(locks[i]);
		}
	}
}

//
// Remove the given lock from 'active_list'.
//
void
frange_lock::remove_lock(const REPL_PXFS_VER::lock_info_t &lock)
{
	ASSERT(active_locks_lock.write_held() ||
	    fobj_iip->fs_implp->llm_lock.write_held());

	REPL_PXFS_VER::lock_info_t	*tmp_lock;

	//
	// Walk the list of active locks, looking for and removing the one
	// that is identical to the one passed in.
	//
	for (active_locks.atfirst();
	    (tmp_lock = active_locks.get_current()) != NULL;
	    active_locks.advance()) {
		if ((tmp_lock->l_start == lock.l_start) &&
		    (tmp_lock->l_end == lock.l_end) &&
		    (tmp_lock->l_flock.l_sysid == lock.l_flock.l_sysid) &&
		    (tmp_lock->l_flock.l_pid == lock.l_flock.l_pid)) {
			(void) active_locks.erase(tmp_lock);
			delete(tmp_lock);
			return;
		}
	}
}

//
// All PXFS calls that go to the LLM are bracketed between
// start_locking() and end_locking().  In start_locking(), we serialize all
// calls to the LLM on a per-file basis, and also get a lock to make sure that
// the filesystem is not cleaning up at this point (via an invocation to
// cl_flk_delete_pxfs_locks made by fsmgr::unreferenced() when a client node
// goes away).
//
void
frange_lock::start_locking()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)start_locking\n", this));

	//
	// This rdlock makes it so that either the NLM-PXFS cleanup is in
	// progress OR locking on individual files is in progress, but never
	// both.  Having both these things happen at the same time confuses
	// the LLM-PXFS callback machinery.  If we did not have this, then
	// locks being cleaned up would be checkpointed over to the secondary
	// as part of some other LLM transaction.
	//
	fs_base_ii::per_node_lock.rdlock();

	//
	// This rdlock makes it so that either the LLM locks from a particular
	// node are being cleaned up (via fsmgr::unreferenced()), OR locking
	// on individual files is in progress, but never both.  Having both
	// these things happen at the same time confuses the LLM-PXFS callback
	// machinery.  If we did not have this, then locks being cleaned up
	// would be checkpointed over to the secondary as part of some other
	// LLM transaction.
	//
	fobj_iip->fs_implp->llm_lock.rdlock();

	//
	// Serialize locking on a per-file basis, so that we can send
	// checkpoints over to the secondary in the right order.  The LLM does
	// this serialization anyway, so we don't degrade performance by much.
	//
	active_locks_lock.wrlock();

	ASSERT(current_locks.empty());
}

//
// This function releases the appropriate locks (held in
// start_locking()), and cleans up lock contexts that were deleted in
// this LLM call.
//
void
frange_lock::end_locking(Environment &env)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FLK,
	    PXFS_GREEN,
	    ("frange_lock(%p)end_locking\n", this));

	ASSERT(active_locks_lock.write_held());

	//
	// Checkpoint the added/deleted locks over to the secondary.
	//
	if (!current_locks.empty()) {
		uint_t				len = current_locks.count();
		REPL_PXFS_VER::lock_info_seq_t	locks(len, len);
		REPL_PXFS_VER::lock_info_t	*tmp;
		uint_t				i = 0;
		while ((tmp = current_locks.reapfirst()) != NULL) {
			ASSERT(i < len);
			locks[i] = *tmp;
			delete tmp;
			i++;
		}

		ASSERT(i == len);
		ASSERT(current_locks.empty());

		new_locks(locks);

		if (fobj_iip->is_replicated() && !fobj_iip->processing_unref) {
			PXFS_VER::fobj_var	fobj_v =
			    fobj_iip->get_fobjref();
			if (fobj_iip->get_ftype() == PXFS_VER::fobj_io) {
				fobj_iip->get_io_ckpt()->
				    ckpt_locks_v1(fobj_v, locks, env);
			} else {
				fobj_iip->get_ckpt()->
				    ckpt_locks(fobj_v, locks, env);
			}
			env.clear();
		}
	}

	active_locks_lock.unlock();

	fobj_iip->fs_implp->llm_lock.unlock();

	fs_base_ii::per_node_lock.unlock();
}

//
// Helper function that converts from type 'lock_descriptor_t' to type
// 'lock_info_t'.
//
REPL_PXFS_VER::lock_info_t *
frange_lock::get_lock_info(lock_descriptor_t *lock)
{
	REPL_PXFS_VER::lock_info_t	*new_lock =
	    new REPL_PXFS_VER::lock_info_t;

	new_lock->l_status = lock->l_status;
	new_lock->l_type = lock->l_type;
	new_lock->l_state = lock->l_state;
	new_lock->l_start = lock->l_start;
	new_lock->l_end = lock->l_end;
	new_lock->l_flock = conv(lock->l_flock);

	// Translate the local PXFS lock sysid value (0) into the actual sysid.
	if (new_lock->l_flock.l_sysid == 0) {
		new_lock->l_flock.l_sysid =
		    pxfslib::set_nodeid(0, orb_conf::node_number());
	}

	return (new_lock);
}

//
// Helper function that converts from type 'lock_info_t' to type
// 'lock_descriptor_t'.
//
lock_descriptor_t *
frange_lock::get_lock_descriptor(REPL_PXFS_VER::lock_info_t *lock_info_p)
{
	lock_descriptor_t	*l = (lock_descriptor_t *)
	    kmem_zalloc(sizeof (lock_descriptor_t), KM_SLEEP);

	cv_init(&l->l_cv, NULL, CV_DRIVER, NULL);
	l->l_edge.edge_in_next = &l->l_edge;
	l->l_edge.edge_in_prev = &l->l_edge;
	l->l_edge.edge_adj_next = &l->l_edge;
	l->l_edge.edge_adj_prev = &l->l_edge;
	l->pvertex = -1;
	l->l_status = FLK_INITIAL_STATE;

	l->l_vnode = fobj_iip->get_vp();
	l->l_type = lock_info_p->l_type;
	l->l_state = lock_info_p->l_state;
	l->l_start = lock_info_p->l_start;
	l->l_end = lock_info_p->l_end;
	l->l_flock = conv(lock_info_p->l_flock);

	l_fobjp(&(l->l_flock)) = fobj_iip;

	// For local PXFS locks, return a sysid of 0
	if (l->l_flock.l_sysid ==
	    pxfslib::set_nodeid(0, orb_conf::node_number())) {
		l->l_flock.l_sysid = 0;
	}

	return (l);
}

//
// Entry point into the PXFS client for the VOP_FRLOCK() call.
//
void
frange_lock::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	cred_t		*crp = solobj_impl::conv(credobj);
	flock64_t	*flkp = &conv(lock_info);
	int		error;
	Environment	e;

	PXFS_VER::pxfs_llm_callback_ptr		cb_obj =
	    PXFS_VER::pxfs_llm_callback::_nil();

	//
	// XXX Don't allow mandatory locks to be set until
	// page_in() and page_out() no longer call
	// VOP_READ()/VOP_WRITE() to do I/O on the file in the underlying
	// file system. For file systems like UFS, the I/O will block forever
	// if there is a mandatory lock set.
	//
	vattr_t va;
	switch (cmd) {
	case F_SETLKW:
		if (!CORBA::is_nil(cb)) {
			cb_obj = PXFS_VER::pxfs_llm_callback::_duplicate(cb);
			PXFS_DBPRINTF(
			    PXFS_TRACE_FLK,
			    PXFS_GREEN,
			    ("frange_lock(%p)frlock cb_obj (%p) created\n",
			    this, cb));

			if (is_in_cb_object_list(cb_obj)) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_FLK,
				    PXFS_GREEN,
				    ("frange_lock(%p)frlock retry case\n",
				    this));
				//
				// This is a retry, caused by racing
				// invalidations and ::frlock requests.  See
				// BugId 4305423.
				//
				_environment.exception(new PXFS_VER::blocked);

				//
				// It is possible that this is a woken-up lock
				// and we've lost the notification on the
				// callback object.  Re-send the notification
				// if this is a woken-up lock.
				//
				if (is_in_processed_list(cb_obj)) {
					PXFS_DBPRINTF(
					    PXFS_TRACE_FLK,
					    PXFS_GREEN,
					    ("frange_lock(%p)frlock retry "
					    "case - waking up client",
					    this));
					cb_obj->wakeup(e);
					e.clear();
				}
				CORBA::release(cb_obj);
				return;
			}
		}

		/* FALLTHROUGH */
	case F_SETLK:

		if (flag & F_PXFSLOCK) {
			va.va_mask = AT_MODE;
#if	SOL_VERSION >= __s11
			error = VOP_GETATTR(fobj_iip->back_obj.vnodep, &va, 0,
			    crp, NULL);
#else
			error = VOP_GETATTR(fobj_iip->back_obj.vnodep, &va, 0,
			    crp);
#endif
			if (error != 0) {
				pxfslib::throw_exception(_environment, error);
				return;
			}
			if (MANDMODE(va.va_mode)) {
				pxfslib::throw_exception(_environment, ENOTSUP);
				return;
			}
		}
		break;

	case F_GETLK:
	case F_HASREMOTELOCKS:
		break;

	default:
		pxfslib::throw_exception(_environment, ENOTSUP);
		return;
	}

	//
	// Store the callback object in the lock structure.
	// If VOP_FRLOCK() returns (-1), then this reference is released
	// either in frlock_execute_request() or in frlock_cancel_request().
	//
	(((lock_context_t *)((flkp)->l_pad))->cb_obj_v1) = cb_obj;
	l_fobjp(flkp) = fobj_iip;

	//
	// For local PXFS locks, we use a sysid of zero within the base
	// kernel lock code to minimize the changes to the base kernel.
	// We convert zero back to the sysid used by the PXFS client
	// so that if the primary is switched to the secondary, the
	// client won't see the sysid change.
	//
	if (flkp->l_sysid == pxfslib::set_nodeid(0, orb_conf::node_number())) {
		flkp->l_sysid = 0;
	}

	start_locking();

#if	SOL_VERSION >= __s11
	error = VOP_FRLOCK(fobj_iip->back_obj.vnodep, cmd, flkp, flag,
	    (offset_t)off, NULL, crp, NULL);
#else
#if	SOL_VERSION >= __s9	/* new s9 frlock interface */
	error = VOP_FRLOCK(fobj_iip->back_obj.vnodep, cmd, flkp, flag,
	    (offset_t)off, NULL, crp);
#else
	error = VOP_FRLOCK(fobj_iip->back_obj.vnodep, cmd, flkp, flag,
	    (offset_t)off, crp);
#endif
#endif
	if (flkp->l_sysid == 0) {
		flkp->l_sysid = pxfslib::set_nodeid(0, orb_conf::node_number());
	}

	end_locking(_environment);

	if (error == -1) {
		//
		// Return a 'blocked' exception to the client.  The reference to
		// the callback object will be freed by frlock_cancel_request()
		// or frlock_execute_request().
		//
		_environment.exception(new PXFS_VER::blocked);

		//
		// Store another reference to this callback object on
		// 'cb_obj_list', used to detect retries.
		//
		ASSERT(!CORBA::is_nil(cb));

		//
		// If the "flag" says that the lock gotten is truly an NLM
		// lock, set "nlmlock" variable to true, else false.
		// The reason for this argument is that the "l_sysid" passed
		// in does NOT contain enough to distinguish a local lock
		// from an NLM lock.
		//
		bool nlmlock = (flag & F_REMOTELOCK) ? true : false;

		//lint -e737
		add_cb_object(cb, flkp->l_sysid, nlmlock);
		//lint +e737

		return;
	}

	if (!CORBA::is_nil(cb_obj)) {
		// Callback object is unused - release the reference.
		PXFS_DBPRINTF(
		    PXFS_TRACE_FLK,
		    PXFS_GREEN,
		    ("frange_lock(%p)frlock - cb_obj (%p) being released\n",
		    this, cb));
		CORBA::release(cb_obj);
	}

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// Effect: Cancels the request in the underlying local lock manager.  The
//	request is identified by its vp, and its callback object.  The request
//	may also be in the 'granted_list', or in the 'processed_list', in which
//	case it needs to be removed from there.
//
void
frange_lock::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	lock_descriptor			*lock;
	PXFS_VER::pxfs_llm_callback_ptr	lock_cb;
	graph_t				*gp;
	lock_context_t			*lock_contextp;

	// Remove the callback object from the retry list.
	remove_cb_object(cb);

	//
	// Note that it is possible that we do not find the sleeping lock in
	// the LLM.  This will only happen if the sleeping lock was woken up
	// by a signal on the client during a PXFS server switchover.  This
	// is harmless, we can just return.
	//

	start_locking();

	gp = flk_get_lock_graph(fobj_iip->back_obj.vnodep, FLK_USE_GRAPH);

	if (gp == NULL) {
		goto out;
	}

	mutex_enter(&gp->gp_mutex);

	SET_LOCK_TO_FIRST_SLEEP_VP(gp, lock, fobj_iip->back_obj.vnodep);

	if (lock) {
		while (lock->l_vnode == fobj_iip->back_obj.vnodep) {
			// All locks on this vnode must be PXFS locks
			ASSERT(IS_PXFS(lock));

			// Check if the callback object compares
			lock_contextp = CL_FLK_GET_CONTEXT(lock);
			lock_cb = lock_contextp->cb_obj_v1;

			ASSERT(!CORBA::is_nil(lock_cb));

			if (cb->_equiv(lock_cb)) {
				//
				// Cancel the lock.  The LLM callback for the
				// lock being deleted will erase the entry
				// from the PXFS lists if the lock has already
				// been woken up.  It will also release the
				// reference to the callback object via a
				// callback into PXFS with a state change
				// notification from (INTERRUPTED) to (DEAD).
				//
				flk_set_state(lock, FLK_INTERRUPTED_STATE);
				flk_cancel_sleeping_lock(lock, 1);

				break;
			}

			lock = lock->l_next;
		}
	}

	mutex_exit(&gp->gp_mutex);

out:
	end_locking(_environment);
}

//
// Effect: Executes the request in the underlying local lock manager.  The
//	request is identified by its callback object, and must be in the list
//	of locks that have been processed.
//
void
frange_lock::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	lock_descriptor_t	*lock;
	bool			found = false;
	lock_context_t		*lock_contextp = NULL;
	int			error;

	PXFS_VER::pxfs_llm_callback_ptr		lock_cb =
	    PXFS_VER::pxfs_llm_callback::_nil();

	// Remove the callback object from the retry list.
	remove_cb_object(cb);

	start_locking();

	processed_list_lock.lock();

	for (processed_list.atfirst();
	    (lock = processed_list.get_current()) != NULL;
	    processed_list.advance()) {
		lock_contextp = CL_FLK_GET_CONTEXT(lock);
		lock_cb = lock_contextp->cb_obj_v1;
		ASSERT(!CORBA::is_nil(lock_cb));
		if (lock_cb->_equiv(cb)) {
			//
			// Found the lock.  We do not remove it from the
			// processed lock list here - that will be done when
			// the lock transitions to the dead state (in
			// 'cl_flk_state_notify_woken_lock_deleted') or to the
			// active state (in 'frange_lock::process_pxfs_lock').
			//
			found = true;
			break;
		}
	}

	processed_list_lock.unlock();

	if (!found) {
		//
		// This should only happen if there is a switchover or a
		// failover between the time of a wakeup() notification being
		// issued to the client, and the client calling
		// frlock_execute_request()
		// We raise an exception and the client retries the original
		// locking request.
		//
		end_locking(_environment);
		_environment.exception(new PXFS_VER::invalid_cb);
		return;
	}

	error = process_pxfs_lock(lock, false);

	end_locking(_environment);

	//
	// For interrupted and cancelled locks, process_pxfs_lock() calls
	// flk_cancel_sleeping_lock().  This results in a state change from
	// (CANCELLED, INTERRUPTED) to (DEAD), and a corresponding callback
	// into PXFS space that releases the CORBA reference to the callback
	// object.
	// However, for granted locks, we still need to release the reference.
	//
	if (error == 0) {
		// Lock is granted.  Release the CORBA reference.
		CORBA::release(lock_cb);
	} else {
		pxfslib::throw_exception(_environment, error);
	}
}
