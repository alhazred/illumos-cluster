//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)symlink_impl_in.h	1.6	08/05/20 SMI"

//
// Unreplicated or primary constructor for the internal implementation
//
inline
symlink_ii::symlink_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
    fid_t *fidp) :
	fobjplus_ii(fsp, vp, ftype, fidp)
{
}

//
// Secondary constructor for the internal implementation
//
inline
symlink_ii::symlink_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t ftype) :
	fobjplus_ii(fsp, fobjid, ftype)
{
}

inline
symlink_ii::~symlink_ii()
{
}

inline
symlink_norm_impl::~symlink_norm_impl()
{
}

inline
symlink_repl_impl::~symlink_repl_impl()
{
}
