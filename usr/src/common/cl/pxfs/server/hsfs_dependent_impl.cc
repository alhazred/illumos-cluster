//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hsfs_dependent_impl.cc	1.6	08/05/20 SMI"

#include <sys/uio.h>
#include <sys/vmsystm.h>

#include "../version.h"
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/hsfs_dependent_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// finial pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

// Constructor.
hsfs_dependent_impl::hsfs_dependent_impl(fs_ii *)
{
	// Empty
}

// Virtual destructor.
hsfs_dependent_impl::~hsfs_dependent_impl()
{
	// Empty
}

int
hsfs_dependent_impl::fs_alloc_data(vnode_t *, u_offset_t, size_t *,
    fdbuffer_t *, int, cred_t *)
{
	ASSERT(0);
	return (ENOSPC);
}

int
hsfs_dependent_impl::fs_rdwr_data(vnode_t *vp, u_offset_t offset, size_t len,
    size_t, fdbuffer_t *fdb, int flags, cred_t *cr)
{
	ASSERT(flags & B_READ);

	// We only support FDB_PAGEIO currently.
	if (fdb->fd_type != FDB_PAGEIO) {
		return (EIO);
	}

	//
	// B_ASYNC is not supported, but we go ahead and do a sync
	// read.  The client has already allocated the pages and sent
	// the request across, might as well spend the time neccessary
	// to honor its request.
	//

	page_t *pp = fdb->fd_un.pages;
	size_t	lenr;
	ssize_t	resid;
	int error = 0;

	while (error == 0 && len > 0) {

		//
		// Map the page and perform VOP_READ().
		// XXX This can fail and we should decide how to deal with
		// the case when ppmapin() fails.
		//
		caddr_t	pageaddr = ppmapin(pp, PROT_WRITE, (caddr_t)-1);

		lenr = len > PAGESIZE ? PAGESIZE : len;

		error = vn_rdwr(UIO_READ, vp, pageaddr, (ssize_t)lenr,
		    (offset_t)offset, UIO_SYSSPACE, 0, (rlim64_t)0,
		    cr, &resid);

		ppmapout(pageaddr);
		pp = pp->p_next;
		offset += PAGESIZE;
		len -= lenr;
	}

	if (flags & B_ASYNC) {
		fdb_ioerrdone(fdb, error);
		return (0);
	}

	return (error);
}
