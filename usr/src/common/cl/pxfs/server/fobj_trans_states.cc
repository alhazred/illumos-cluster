//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fobj_trans_states.cc	1.6	08/05/20 SMI"

#include <sys/types.h>
#include <sys/lockfs.h>

#include "../version.h"
#include <pxfs/server/fobj_trans_states.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// finial pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

fobj_lockfs_state::fobj_lockfs_state(fs_repl_impl *fsp, uint64_t lf_lock,
    uint64_t lf_flags, uint64_t lf_key, const char *lf_comment) :
    _fsp(fsp), _lf_lock(lf_lock), _lf_flags(lf_flags), _lf_key(lf_key),
    _lf_comment(lf_comment), _state(fobj_lockfs_state::INITIAL), _err(0)
{
}

// Destructor.  Nothing to do here.
fobj_lockfs_state::~fobj_lockfs_state()
{
	ASSERT(_state != fobj_lockfs_state::INITIAL);
} //lint !e1540 pointers are neither freed nor zero'ed by destructor

//
// Static helper function called from repl_pxfs_server to register a state
// object of type 'fobj_lockfs_state' with the framework.
//
void
fobj_lockfs_state::register_new_state(fs_repl_impl *fsp, uint64_t lf_lock,
	uint64_t lf_flags, uint64_t lf_key, const char *lf_comment,
	Environment &env)
{
	// If this checkpoint is being sent while dumping state, don't
	// register state with the framework.
	if (secondary_ctx::extract_from(env) == NULL) {
		return;
	}

	// Create a new state object ...
	fobj_lockfs_state *state = new fobj_lockfs_state(fsp, lf_lock,
	    lf_flags, lf_key, lf_comment);

	// ... and register it
	state->register_state(env);

	// The HA framework guarantees that no exception will be raised in
	// this call
	ASSERT(!env.exception());
}

//
// Static helper function to report failure of an on-going lockfs transaction
//
void
fobj_lockfs_state::report_failure(sol::error_t err, Environment &env)
{
	// Get the state object from the environment
	secondary_ctx *ctxp = secondary_ctx::extract_from(env);

	fobj_lockfs_state *state = (fobj_lockfs_state *)ctxp->get_saved_state();
	ASSERT(state != NULL);

	// Cancel the transaction
	state->report_failure(err);
}

//
// If the environment variable passed in indicates that this call is a retry,
// then this function returns the state object, otherwise it returns NULL.
//
fobj_lockfs_state *
fobj_lockfs_state::retry(Environment &env)
{
	primary_ctx *ctxp = primary_ctx::extract_from(env);

	ASSERT(ctxp != NULL);

	return (fobj_lockfs_state *)(ctxp->get_saved_state());
}

//
// This function is invoked by the framework when both the primary and the
// client that made the call die.
//
void
fobj_lockfs_state::orphaned(Environment &)
{
	// Primary and client died.  Set state and return, there is nothing
	// to do.
	_state = fobj_lockfs_state::ORPHANED;
}

//
// Called on the state object if the primary sends the ckpt_lockfs_failure()
// checkpoint.
//
void
fobj_lockfs_state::report_failure(sol::error_t err)
{
	ASSERT(_state == fobj_lockfs_state::INITIAL);
	_state = fobj_lockfs_state::CANCELLED;
	_err = err;
}

//
// Called on the state object by the framework to commit the transaction.
//
void
fobj_lockfs_state::committed()
{
	ASSERT((_state == fobj_lockfs_state::INITIAL) ||
	    (_state == fobj_lockfs_state::CANCELLED));

	if (_state == fobj_lockfs_state::INITIAL) {
		// Successful lockfs call - set locking parameters in the
		// fs object
		_fsp->get_fs_dep_implp()->ckpt_lockfs_state(_lf_lock,
		    _lf_flags, _lf_key, (const char *)_lf_comment);
		_state = fobj_lockfs_state::COMMITED;
	}
}

//
// Accessor function
//
void
fobj_lockfs_state::get_args(struct lockfs *lockfsp, int *state)
{
	lockfsp->lf_lock = (ulong_t)_lf_lock;
	lockfsp->lf_flags = (ulong_t)_lf_flags;
	lockfsp->lf_key = (ulong_t)_lf_key;
	lockfsp->lf_comment = (char *)_lf_comment;
	if (lockfsp->lf_comment == NULL) {
		lockfsp->lf_comlen = 0;
	} else {
		lockfsp->lf_comlen = strlen(lockfsp->lf_comment);
	}
	*state = _state;
}

//
// Accessor function
//
sol::error_t
fobj_lockfs_state::get_error()
{
	return (_err);
}

void
fobj_retry_state::register_new_state(Environment &env)
{
	// If this checkpoint is being sent while dumping state, don't
	// register state with the framework.
	if (secondary_ctx::extract_from(env) == NULL) {
		return;
	}

	// Create a new state object
	fobj_retry_state *state = new fobj_retry_state();

	// register the state

	state->register_state(env);

	ASSERT(!env.exception());
}

bool
fobj_retry_state::retry(Environment &env)
{
	primary_ctx *ctxp = primary_ctx::extract_from(env);
	fobj_retry_state *state = (fobj_retry_state *)
					ctxp->get_saved_state();
	if (state != NULL) {
		return (true);
	}
	return (false);
}
