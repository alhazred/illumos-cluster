//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef CLIENT_SET_H
#define	CLIENT_SET_H

#pragma ident	"@(#)client_set.h	1.14	08/05/20 SMI"

#include <sys/os.h>
#include <sys/list_def.h>
#include <sys/nodeset.h>

#include "../version.h"
#include PXFS_IDL(pxfs)

// Forward declarations
class fobjplus_ii;

//
// client_set_purge_task - we encountered a dead client.
// This task purges the client set of dead clients.
// This task enables a different thread to process the cleanup,
// and thus avoid lock order hierarchy violations that can result in deadlock.
//
class client_set_purge_task : public defer_task {
public:
	client_set_purge_task(fobjplus_ii *fobjplus_iip);

	virtual void		execute();

private:
	fobjplus_ii		*fobjplusp;
	PXFS_VER::fobjplus_ptr	fobjplus_p;

	// Make the default constructor unavailable
	client_set_purge_task();
};

//
// client_set - supports the maintenance
// and iteration over a set of fobj_client objects.
//
class client_set {
public:

	//
	// client_entry - there is one of these things per client.
	// This assumes that there is at most one client object per node,
	// which will be true for internal PXFS clients.
	//
	class client_entry :
		public _DList::ListElem
	{
		friend class client_set;
	public:
		client_entry();

		client_entry(ID_node &client_node,
		    PXFS_VER::fobj_client_ptr client_p);

		bool	is_dead();

		bool	is_zombie();

		void	clear();

		void	hold();

		void	release();

		//
		// Object reference to client.
		// Note that a zombie retains a valid client reference.
		// We need to ensure that any client continues to
		// exist while someone is trying invocations.
		//
		PXFS_VER::fobj_client_ptr	client_p;

		//
		// This is the node hosting the client.
		// A node number > NODEID_MAX represents a zombie entry.
		//
		ID_node				node;

		//
		// When a thread traverses the client list
		// and makes an invocation, the thread must drop
		// the lock. To keep the entry in existance, the
		// thread increments the refcnt while making the invocation.
		//
		uint32_t			refcnt;
	};

	//
	// The programmer must acquire the list_lock when
	// using this iterator to prevent the list from changing.
	//
	class iterator {
	public:
		iterator(IntrList<client_entry, _DList>	&client_list);
		~iterator();

		client_entry	*get_current();

		void		advance();

	private:
		// Make the default constructor unavailable
		iterator();

		IntrList<client_entry, _DList>::ListIterator iter;
	};

	client_set();
	~client_set();

	void	empty();

	PXFS_VER::fobj_client_ptr	insert(ID_node &client_node,
					    PXFS_VER::fobj_client_ptr client_p);

	void	remove(fobjplus_ii *fobjplusp, ID_node &client_node);

	void	remove(fobjplus_ii *fobjplusp,
		    PXFS_VER::fobj_client_ptr client_p);

	// Purge any client entry with a NIL object reference
	void	purge_dead_clients(fobjplus_ii *fobjplusp);

	void	do_purge_dead_clients(fobjplus_ii *fobjplusp);

	void	find_and_purge_dead_clients(fobjplus_ii *fobjplusp);

	// Return a bit mask for the set of clients
	nodeset	client_nodeset();

	// Return the client object reference for the specified node
	PXFS_VER::fobj_client_ptr	get_client(ID_node &client_node);

	//
	// Protects changes in the list of clients
	// A read lock must be held when traversing the client list.
	// A write lock must be held when changing the client list.
	//
	os::rwlock_t	list_lock;

	IntrList<client_entry, _DList>	client_list;

	// Bitmask identifying clients by node
	nodeset		clients;

protected:

	//
	// A server will have at least one client, at least when
	// doing useful work. File sharing is uncommon. So optimize
	// for the common case where there is one client.
	// we pre-allocate the entry for the first client.
	//
	client_entry	first_client;

	bool		purge_in_progress;
};

#include <pxfs/server/client_set_in.h>

#endif	// CLIENT_SET_H
