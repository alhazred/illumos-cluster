//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// unixdir_impl.h - Implementation class for unixdir interface defined
//	in interfaces/pxfs.idl
//

#ifndef UNIXDIR_IMPL_H
#define	UNIXDIR_IMPL_H

#pragma ident	"@(#)unixdir_impl.h	1.15	08/05/20 SMI"

#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fobjplus_impl.h>
#include <pxfs/server/client_set.h>

//
// Forward declarations.
//
class repl_pxfs_server;

//
// unixdir_ii - this is the server side support for
// a directory file. This class has code that is shared
// between directories belonging to ordinary and highly
// available file systems.
//
class unixdir_ii : public fobjplus_ii {
public:
	virtual ~unixdir_ii();

	//
	// Create a CORBA unixdir_ptr reference from this object
	// Return a new unixdir CORBA reference to this.
	//
	virtual PXFS_VER::unixdir_ptr	get_unixdirref() = 0;

	// Helper function for dumping state to a new secondary.
	virtual void	dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    PXFS_VER::fobj_ptr fobjp, Environment &env);

	// Convert an fobj from secondary to spare representation.
	virtual void	convert_to_spare();

	virtual void	acquire_token_locks();
	virtual void	release_token_locks();

	virtual void	remove_client_privileges(nodeid_t node_id);

	//
	// Interface operations for unixdir
	//

	void	lookup(const char *nm, PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	create_fobj(const char *nm,
	    const sol::vattr_t &attr, sol::vcexcl_t exclflag, int32_t mode,
	    PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, int32_t flag, Environment &_environment);

	void	remove_fobj(const char *nm,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	create_symlink(const char *nm, const sol::vattr_t &attr,
	    const char *targetpath, solobj::cred_ptr credobj,
	    Environment &_environment);

	void	rename_fobj(const char *sourcenm,
	    PXFS_VER::unixdir_ptr target_dir,
	    const char *targetnm, solobj::cred_ptr credobj,
	    PXFS_VER::unixdir_ptr sourceobj, Environment &_environment);

	void	link_fobj(PXFS_VER::fobj_ptr fobj, const char *targetnm,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	create_dir(const char *dirnm, const sol::vattr_t &attr,
	    PXFS_VER::fobj_out newdir, PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	remove_dir(const char *dirnm, PXFS_VER::unixdir_ptr cur_dir,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	readdir(PXFS_VER::unixdir::diroff_t &offset, sol::size_t maxlen,
	    PXFS_VER::unixdir::direntrylist_t_out entries, bool &eof_flag,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	readdir_raw(PXFS_VER::unixdir::diroff_t &offset,
	    sol::size_t maxlen,
	    PXFS_VER::unixdir::rawdir_t_out rawdir, bool &eof_flag,
	    solobj::cred_ptr credobj, Environment &_environment);

	// End of Idl operations

	virtual void	prepare_recover();

	virtual void	recovered_state(PXFS_VER::recovery_info &recovery);

protected:
	// Unreplicated or primary constructor for the internal implementation.
	unixdir_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation.
	unixdir_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
		PXFS_VER::fobj_type_t ftype);

private:
	// Check to see if a symbolic link was created correctly.
	int		check_symlink(const char *nm,
			    const sol::vattr_t &attributes,
			    const char *path, cred_t *crp);

	// Downgrade all client-side directory caches (i.e., purge them).
	void		downgrade_dir_all();

	//
	// Invalidate one file name from client lookup caches,
	// except for the specified node to skip.
	//
	void		inval_name(nodeid_t skip_client_node, const char *name);

	// Check to see if the directory is empty or not.
	bool		is_empty();

	bool		remove_fobj_fastpath(const char *nm, fobj_ii *fobj_iip,
			    cred_t *credp, Environment &env);
private:
	//
	// Bit mask identifying nodes holding cached directory info
	//
	nodeset		cached_dir_info;

protected:
	// Synchronize lookup and directory content changes for server.
	os::rwlock_t	dir_lock;

	// Controls acces to cached_dir_info
	os::mutex_t	dir_token_lock;

	// Disallow assignments and pass by value.
	unixdir_ii(const unixdir_ii &);
	unixdir_ii &operator = (unixdir_ii &);
};

//
// Unreplicated CORBA implementation of unixdir.
//
class unixdir_norm_impl :
	public McServerof<PXFS_VER::unixdir>,
	public unixdir_ii
{
public:
	unixdir_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);

	virtual void		_unreferenced(unref_t arg);

	// Return a new PXFS_VER::fobj CORBA reference to this.
	PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler	*get_handler();

	// Return a new PXFS_VER::unixdir CORBA reference to this.
	PXFS_VER::unixdir_ptr get_unixdirref();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	//
	// Interface operations from unixdir
	//

	/* fobj */

	virtual PXFS_VER::fobj_type_t
			get_fobj_type(Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	virtual void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	virtual void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	/* unixdir */

	virtual void	lookup(const char *nm, PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo, PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	cascaded_create_fobj(const char *nm,
	    const sol::vattr_t &attr, sol::vcexcl_t exclflag, int32_t mode,
	    PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo, PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, int32_t flag, Environment &_environment);

	virtual void	remove_fobj(const char *nm,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	create_symlink(const char *nm, const sol::vattr_t &attr,
	    const char *targetpath, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	rename_fobj(const char *sourcenm,
	    PXFS_VER::unixdir_ptr target_dir, const char *targetnm,
	    solobj::cred_ptr credobj,  PXFS_VER::unixdir_ptr sourceobj,
	    Environment &_environment);

	virtual void	link_fobj(PXFS_VER::fobj_ptr fobj, const char *targetnm,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	create_dir(const char *dirnm, const sol::vattr_t &attr,
	    PXFS_VER::fobj_out newdir, PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_dir(const char *dirnm,
	    PXFS_VER::unixdir_ptr cur_dir,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	readdir(PXFS_VER::unixdir::diroff_t &offset,
	    sol::size_t maxlen,
	    PXFS_VER::unixdir::direntrylist_t_out entries, bool &eof_flag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	readdir_raw(PXFS_VER::unixdir::diroff_t &offset,
	    sol::size_t maxlen, PXFS_VER::unixdir::rawdir_t_out rawdir,
	    bool &eof_flag, solobj::cred_ptr credobj,
	    Environment &_environment);

	// Disallow assignments and pass by value.
	unixdir_norm_impl(const unixdir_norm_impl &);
	unixdir_norm_impl &operator = (unixdir_norm_impl &);
};

//
// Replicated CORBA implementation of unixdir.
//
class unixdir_repl_impl :
	public mc_replica_of<PXFS_VER::unixdir>,
	public unixdir_ii
{
public:
	// Primary constructor.
	unixdir_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);

	// Secondary constructor.
	unixdir_repl_impl(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
		PXFS_VER::unixdir_ptr obj);

	virtual void		_unreferenced(unref_t arg);

	// Return a new fobj CORBA reference to this.
	PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler	*get_handler();

	// Return a new unixdir CORBA reference to this.
	PXFS_VER::unixdir_ptr	get_unixdirref();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	//
	// Interface operations from unixdir
	//

	/* fobj */

	virtual PXFS_VER::fobj_type_t
			get_fobj_type(Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	virtual void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	virtual void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	/* unixdir */

	virtual void	lookup(const char *nm, PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	cascaded_create_fobj(const char *nm,
	    const sol::vattr_t &attr, sol::vcexcl_t exclflag, int32_t mode,
	    PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, int32_t flag, Environment &_environment);

	virtual void	remove_fobj(const char *nm,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	create_symlink(const char *nm, const sol::vattr_t &attr,
	    const char *targetpath, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	rename_fobj(const char *sourcenm,
	    PXFS_VER::unixdir_ptr target_dir, const char *targetnm,
	    solobj::cred_ptr credobj,  PXFS_VER::unixdir_ptr sourceobj,
	    Environment &_environment);

	virtual void	link_fobj(PXFS_VER::fobj_ptr fobj, const char *targetnm,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	create_dir(const char *dirnm, const sol::vattr_t &attr,
	    PXFS_VER::fobj_out newdir, PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_dir(const char *dirnm,
	    PXFS_VER::unixdir_ptr cur_dir,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	readdir(PXFS_VER::unixdir::diroff_t &offset,
	    sol::size_t maxlen,
	    PXFS_VER::unixdir::direntrylist_t_out entries, bool &eof_flag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	readdir_raw(PXFS_VER::unixdir::diroff_t &offset,
	    sol::size_t maxlen, PXFS_VER::unixdir::rawdir_t_out rawdir,
	    bool &eof_flag, solobj::cred_ptr credobj,
	    Environment &_environment);

	// Disallow assignments and pass by value.
	unixdir_repl_impl(const unixdir_repl_impl &);
	unixdir_repl_impl &operator = (unixdir_repl_impl &);

	// Check if the invocation that passed 'env' is a retry and it
	// has uncommitted state.
	bool is_retry(Environment &env);
};

#endif	// UNIXDIR_IMPL_H
