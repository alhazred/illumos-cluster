//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// io_impl.h - Implementation class for the io interface defined
//	in interfaces/pxfs.idl.
//

#ifndef IO_IMPL_H
#define	IO_IMPL_H

#pragma ident	"@(#)io_impl.h	1.7	08/05/20 SMI"

#include <h/px_aio.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include <pxfs/device/device_server_impl.h>
#include PXFS_IDL(pxfs)
#include <pxfs/server/fobj_impl.h>

//
//
// class "io" supports raw devices in the file system name
// space that are accessed through read and write operations.
//
class io_ii : public fobj_ii {
public:
	// Constructors are protected.

	virtual ~io_ii();

	//
	// Return true if this is a dead replicated file system object
	// (i.e., there was a fatal error in convert_to_primary() or
	// convert_to_secondary() but the replica manager still thinks
	// the service is alive).
	// If true, the environment is also set to return an exception.
	//
	virtual bool	is_dead(Environment &env);

protected:
	// Unreplicated or primary constructor for the internal implementation.
	io_ii(fs_base_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation.
	io_ii(fs_base_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_type_t ftype);

	// io::=
	void open(int32_t flags, PXFS_VER::fobj_out open_io_fobj,
	    PXFS_VER::fobj_info &fobjinfo, solobj::cred_ptr credobj,
	    Environment &_environment);

	void close(int32_t flags, sol::u_offset_t off, solobj::cred_ptr credobj,
	    Environment &_environment);

	void uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	void aioread(pxfs_aio::aio_callback_ptr aiock,
	    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	void uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	void aiowrite(pxfs_aio::aio_callback_ptr aiock,
	    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	void fsync(int32_t syncflag, solobj::cred_ptr credobj,
	    Environment &_environment);
	//

	//
	// True means that there has been an attempt to convert this
	// object to become a primay and that the attempt failed.
	//
	bool	primary_failed;
};

//
// Unreplicated CORBA implementation of io.
//
class io_norm_impl :
    public McServerof<PXFS_VER::io>,
    public io_ii
{
public:
	io_norm_impl(device_server_ii *dsp, vnode_t *vp, fid_t *fidp);
	~io_norm_impl();

	virtual void		_unreferenced(unref_t arg);

	// Return a new PXFS_VER::fobj CORBA reference to this
	PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t	get_fobj_type(
	    Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	/* io */

	virtual void	open(int32_t flags, PXFS_VER::fobj_out open_io_fobj,
	    PXFS_VER::fobj_info &fobjinfo, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	close(int32_t flags, sol::u_offset_t off,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	aioread(pxfs_aio::aio_callback_ptr aiock,
	    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	aiowrite(pxfs_aio::aio_callback_ptr aiock,
	    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	cascaded_fsync(int32_t syncflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	// End of methods supporting IDL operations
};

//
// Replicated CORBA implementation of io.
//
class io_repl_impl :
	public mc_replica_of<PXFS_VER::io>,
	public io_ii
{

public:
	// Primary constructor.
	io_repl_impl(device_server_ii *dsp, vnode_t *vp,
	    const PXFS_VER::pvnode &pvnode, cred_t *crp, fid_t *fidp);

	// Secondary constructor.
	io_repl_impl(device_server_ii *dsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::io_ptr obj, const PXFS_VER::pvnode &pvnode, cred_t *crp);

	~io_repl_impl();

	virtual void	_unreferenced(unref_t arg);

	// Return the dev_t of the real_pvnode.
	dev_t		get_realdev() const;

	//
	// Return a new fobj CORBA reference to the fs server serving this
	// object.
	//
	PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	REPL_PXFS_VER::fs_replica_ptr get_ckpt();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the I/O checkpoint interface.
	//
	repl_dc::repl_dev_server_ckpt_ptr get_io_ckpt();
	repl_dc::repl_dev_server_ckpt_ptr get_checkpoint();

	// Add a commit checkpoint.
	virtual void commit(Environment &e);

	void ckpt_io_object();

	void dump_io_state(repl_dc::repl_dev_server_ckpt_ptr ckpt,
	    Environment &env);

	void ckpt_open(int32_t flags, solobj::cred_ptr credobj,
	    Environment &env);

	void ckpt_close(int32_t flags, solobj::cred_ptr credobj,
	    sol::error_t err, Environment &env);

	static void ckpt_ioctl_begin(Environment &env);

	static void ckpt_ioctl_end(sol::error_t err, int32_t result,
	    Environment &env);

	// Convert an fobj from secondary to primary representation.
	int convert_to_primary();

	// Convert an fobj from secondary to primary representation.
	int convert_to_secondary();

	// Convert an fobj from secondary to spare representation.
	void convert_to_spare();

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t	get_fobj_type(
	    Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	/* io */

	virtual void	open(int32_t flags, PXFS_VER::fobj_out open_io_fobj,
	    PXFS_VER::fobj_info &fobjinfo, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	close(int32_t flags, sol::u_offset_t off,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	uioread(sol::size_t &len, bulkio::inout_uio_ptr &uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	aioread(pxfs_aio::aio_callback_ptr aiock,
	    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	uiowrite(sol::size_t &len, bulkio::in_uio_ptr uioobj,
	    int32_t ioflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	aiowrite(pxfs_aio::aio_callback_ptr aiock,
	    bulkio::in_aio_ptr aioobj, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	cascaded_fsync(int32_t syncflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	// End of methods supporting IDL operations

private:
	// Add an entry to 'open_params_list'.
	void	store_open_params(int flags, cred_t *crp);

	// Remove an entry from 'open_params_list'.
	void	erase_open_params(int flags, cred_t *crp);

	// Shared code for primary & secondary constructors.
	void	io_construct(const PXFS_VER::pvnode &pvnode, cred_t *crp);

	// Structure for saving list of VOP_OPEN() parameters.
	class open_params_t : public _SList::ListElem {
	public:
		open_params_t(int flags, cred_t *crp);
		~open_params_t();

		// Public data members.
		int flags;
		cred_t *crp;
	};

	// List of parameters passed to open - used to replay the opens.
	IntrList<open_params_t, _SList>		open_params_list;

	// Protects stored_open_params.
	os::rwlock_t				open_params_lock;

	// Stores the packed vnode of the real vp for this device object.
	PXFS_VER::pvnode	real_pvnode;

	// Stores the cred used to make the specvp() call.
	cred_t			*credp;
};

#include <pxfs/server/io_impl_in.h>

#endif	// IO_IMPL_H
