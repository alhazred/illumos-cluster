//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FS_BASE_IMPL_H
#define	FS_BASE_IMPL_H

#pragma ident	"@(#)fs_base_impl.h	1.6	08/05/20 SMI"

//
// Common code for device_server_ii and fs_ii.
//

#include <sys/vfs.h>

#include <sys/refcnt.h>

#include "../version.h"
#include PXFS_IDL(repl_pxfs)

// Forward declarations.
class fobj_ii;
class fs_dependent_impl;
class repl_pxfs_server;

// Type for list of fobj_ii objects.
typedef IntrList<fobj_ii, _DList> fobj_ii_list_t;

//
// This is used to store some data for releasing vnode pointers
// and deleting files in the "deleted" directory so that we can
// drop the allfobj_list bucket lock (the fobj could be deleted after the lock
// is dropped so we can't keep a pointer to it to get the info...).
//
class release_info_t {
public:
	release_info_t();
	~release_info_t();

	// Note: data members are public.

	// Vnode to be VN_RELE()'ed.
	vnode_t	*vp;

	// File ID (transferred from fobj_ii).
	fid_t *delete_fidp;

	//
	// This is set to non-zero if the file is renamed to the "deleted"
	// directory needs to be deleted when all references are released.
	// The number is the name of the file in the deleted directory.
	// It is protected by lock_attributes().
	//
	uint64_t delete_num;

	// The specific type of fobj.
	PXFS_VER::fobj_type_t ftype;
};

//
// fs_base_ii - base class supporting the server side for a PXFS
// file system object
//
// Lock Order Hierarchy
//
// the fidlock must be acquired before any of the fobjlist_locks.
//
class fs_base_ii : public refcnt, public _DList::ListElem {
public:
	// Called at module _init() time. Return an errno.
	static int	startup();

	// Constructor is protected.

	// Destructor.
	virtual ~fs_base_ii();

	// Return true if this a secondary object.
	bool	is_secondary() const;

	//
	// Add the given object to allfobj_list[].
	// Assumes bucket lock is held or called from secondary.
	//
	bool	add_to_list(fobj_ii *fobjp);

	//
	// Remove the given fobj from the fidlist[]
	//
	void	remove_from_fidlist(fobj_ii *fobjp);

	//
	// This is called to remove the fobj from allfobj_list
	// and initialize 'rinfo' while the list is locked.
	//
	void	remove_fobj_locked(fobj_ii *fobjp, release_info_t &rinfo);

	// Methods used by fobj through fs_implp.
	virtual fs_dependent_impl	*get_fs_dep_implp() const;
	virtual bool			is_replicated() const = 0;

	//
	// Static method to facilitate communication between fs_collection
	// and fobj during lock deletion.
	//
	static void	notify_external_nlm_event(bool has_transitioned);

	//
	// Returns true whenever an NLM state change occurs via the
	// fs_collection object. For example, when the NLM is externally
	// shutdown.
	//
	static bool	is_external_nlm_event;

	//
	// Returns true during a failover/switchover when no checkpointing of
	// locks is necessary.
	//
	bool	suspend_locking_ckpts() const;

	// Increment the do_delete count
	void	inc_dodelete_cnt();

	// Decrement the do_delete count
	void	dec_dodelete_cnt();

	// Hash function from vnode to index in allfobj_list[].
	int	vp_to_bucket(vnode_t *vp);

#ifdef DEBUGGER_PRINT
	int	mdb_walk_files(fs_base_ii *fs_base_iip);
#endif

	enum base_type_t {
		BASE_UNKNOWN		= 0,
		BASE_FILE_SYSTEM	= 1,
		BASE_DEVICE_SERVER	= 2
	};

	virtual base_type_t	get_base_type() = 0;

protected:
	// Constructor.
	fs_base_ii(uint_t size = MAX_HASH_DEFAULT);

public:
	// Lock used to block access from PXFS into the LLM during clean ups.
	os::rwlock_t		llm_lock;

	// Per node lock used to block access when an NLM state change occurs.
	static os::rwlock_t	per_node_lock;

	// Lock array that provides a lock for each bucket of allfobj_list
	os::rwlock_t		*fobjlist_locks;

	// Protects fidlist
	os::rwlock_t		fidlock;

protected:
	//
	// Number of objects for which have non zero delete_num, but do_delete()
	// has yet to be called on them. This is protected by the
	// do_delete_cnt_lock mutex.
	//
	uint_t		do_delete_cnt;

	//
	// Lock and condition variable to keep count of objects for which
	// do_delete() is pending.
	//
	os::mutex_t	do_delete_cnt_lock;
	os::condvar_t	do_delete_wait_cv;

	//
	// Default size of hash table for list of all fobj_ii objects.
	// This is only used by the device server. For filesystems, we
	// override to a tunable value that is usually significantly larger.
	//
	enum { MAX_HASH_DEFAULT = 256 };

	//
	// All active fobj_ii objects on the primary node
	// are maintained in a hash table. Each bucket
	// is a list of vnodes.
	// The hash key is based on the vnode pointer.
	// This hash table is empty on secondary nodes.
	//
	uint_t		allfobj_buckets;	// size of hash for this fs
	fobj_ii_list_t	*allfobj_list;		// each entry contains the list
						// of fobj's for that bucket.

	//
	// List of all fobjs on secondary node. This list can also have "dead"
	// objects here on the primary node if there was an error during
	// a switchover or fobjs which have been "released" but haven't
	// been _unreferenced() yet (see fobj_release_all()). This last
	// case can happen in non-HA file systems too.
	//
	fobj_ii_list_t	fidlist;

	//
	// This variable indicates if we are in the middle of a
	// failover/switchover.  It is set/unset in convert_to_primary().
	//
	bool	_suspend_locking_ckpts;

	// True if this object is not replicated or is the primary.
	bool	is_primary;

private:
	// Disallowed operations.
	fs_base_ii & operator = (const fs_base_ii &);
	fs_base_ii(const fs_base_ii &);
};

#include <pxfs/server/fs_base_impl_in.h>

#endif // FS_BASE_IMPL_H
