//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _FSMGR_SERVER_IMPL_IN_H
#define	_FSMGR_SERVER_IMPL_IN_H

#pragma ident	"@(#)fsmgr_server_impl_in.h	1.6	08/05/20 SMI"

//
// Helper function to get fsmgr_client object from prov_common base class.
// We don't need a narrow() since we know the CORBA::Object is a fsmgr_client.
//
inline PXFS_VER::fsmgr_client_ptr
fsmgr_server_ii::get_clientmgr() const
{
	return ((PXFS_VER::fsmgr_client_ptr)get_cacheptr());
}

// Accessor function
inline nodeid_t
fsmgr_server_ii::get_nodeid() const
{
	return (nodeid);
}

inline
fsmgr_server_norm_impl::~fsmgr_server_norm_impl()
{
}

inline
fsmgr_server_repl_impl::~fsmgr_server_repl_impl()
{
}

//
// Return a new CORBA reference to this
// (CORBA::release() required)).
//
inline CORBA::Object_ptr
fsmgr_server_norm_impl::get_provref()
{
	return (get_objref());
}

//
// Return a new CORBA reference to this
// (CORBA::release() required)).
//
inline CORBA::Object_ptr
fsmgr_server_repl_impl::get_provref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
inline REPL_PXFS_VER::fs_replica_ptr
fsmgr_server_repl_impl::get_ckpt()
{
	return ((repl_pxfs_server*)(get_provider()))->
	    get_checkpoint_fs_replica();
}

#endif	// _FSMGR_SERVER_IMPL_IN_H
