//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef REVOKE_ALLOCATION_H
#define	REVOKE_ALLOCATION_H

#pragma ident	"@(#)revoke_allocation.h	1.6	08/05/20 SMI"

#include "../version.h"

#include <sys/list_def.h>
#include <sys/threadpool.h>
#include <orb/object/adapter.h>

#include PXFS_IDL(pxfs)
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fsmgr_server_impl.h>

//
// Class for revoke allocation task.
//
class revoke_allocation_task : public defer_task {
public:
	~revoke_allocation_task();
	void execute();
	void revoke_allocation_execute();
	revoke_allocation_task(fs_ii *fsp, PXFS_VER::filesystem_ptr fs_p,
	    uint32_t server_incn, PXFS_VER::fsmgr_client_var client_v);
private:
	fs_ii				*fs_iip;
	PXFS_VER::filesystem_var	fs_v;
	uint32_t			server_incarn;
	PXFS_VER::fsmgr_client_var	clientmgr_v;
};

#endif	// REVOKE_ALLOCATION_H
