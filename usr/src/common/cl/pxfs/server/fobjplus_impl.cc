//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fobjplus_impl.cc	1.44	09/04/08 SMI"

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/acl.h>
#include <sys/file.h>

#include <sys/nodeset.h>
#include <sys/sol_conv.h>
#include <sys/sol_version.h>
#include <orb/infrastructure/orb_conf.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxnode.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/fobjplus_impl.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/fsmgr_server_impl.h>
#include <pxfs/server/fs_base_impl.h>

//
// Undocumented tunables for throttling the clear_attr_dirty
// threadpool.
//
int clear_attr_dirty_threadpool_min = 20;
int clear_attr_dirty_threadpool_max = 50;

//
// Threadpool for clearing attr_dirty on the clients.
//
threadpool clear_attr_dirty_threadpool(true,
	clear_attr_dirty_threadpool_min,
	"clear_attr_dirty_threadpool",
	clear_attr_dirty_threadpool_max);

clear_attr_dirty_task::clear_attr_dirty_task(
    fobjplus_ii *_fobjp, PXFS_VER::fobj_client_var _client_v,
    uint32_t _server_incarn):
	fobjp(_fobjp),
	server_incarn(_server_incarn),
	client_v(_client_v)
{
	//
	// Take a reference of the fobjplus object to prevent it from getting
	// unreferenced.
	//
	fobj_v = fobjp->get_fplus_objref();
}

//
// clear_attr_dirty_task clears the attr_dirty flag the client node
// and signals threads that are waiting to flush dirty attributes.
//
void
clear_attr_dirty_task::execute()
{
	Environment env;

	fobjp->clear_attr_dirty(client_v, server_incarn);
	delete this;
}

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

//
// Table that determines the rights that this attribute cache
// on this node should have so as not to conflict with the
// attribute rights requested by another client node.
//
// "current_rights"		rights currently held by the
//					attribute cache of this node
// "other_requested_rights"	rights another node wants to acquire
//					for its attribute cache
// "new rights"			rights that the attribute cache of this node
//					will hold because of this action
//
// newrights_tab[current_rights][other_requested_rights] -> new rights
//
static uchar_t	newrights_tab[3][3] = {
	// row order:		none, read, write
	// column order:	none, read, write
	{ PXFS_VER::attr_none,  PXFS_VER::attr_none, PXFS_VER::attr_none },
	{ PXFS_VER::attr_read,  PXFS_VER::attr_read, PXFS_VER::attr_none },
	{ PXFS_VER::attr_write, PXFS_VER::attr_read, PXFS_VER::attr_none }
};

//
// Unreplicated or primary constructor for the internal implementation.
//
fobjplus_ii::fobjplus_ii(fs_base_ii *fsp, vnode_t *vnodep,
    PXFS_VER::fobj_type_t ftype, fid_t *fidp) :
	fobj_ii(fsp, vnodep, ftype, fidp),
	mod_time(0),
	dirty_mtime(0),
	attr_seqnum(0),
	access_seqnum(0),
	wait_for_attr_clear(false),
	number_joins(get_fsp()->get_number_joins())
{
}

//
// Secondary constructor for the internal implementation.
//
fobjplus_ii::fobjplus_ii(fs_base_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t ftype) :
	fobj_ii(fsp, fobjid, ftype),
	mod_time(0),
	dirty_mtime(0),
	attr_seqnum(0),
	access_seqnum(0),
	wait_for_attr_clear(false),
	number_joins(0)
{
}

//
// fobjplus_ii destructor
//
fobjplus_ii::~fobjplus_ii()
{
}

//
// unreferenced
// This is called once we know that no more CORBA references can be handed
// out.
//
void
fobjplus_ii::unreferenced(int bktno, bool fidlist_locked)
{
	ASSERT(fs_implp->get_base_type() == fs_base_ii::BASE_FILE_SYSTEM);
	if (((fs_ii *)fs_implp)->get_vfsp() == NULL) {
		//
		// Normally, the system destroys proxy file objects before
		// unmounting the underlying file system. However, the
		// forced unmount operation can unmount the underlying
		// file system even though proxy file objects exist.
		// The normal cleanup will not occur in this case.
		// So we just force the cleanup of all client information.
		//
		clientset.empty();
	} else {
		//
		// A client node may have died, which means that the client node
		// would not have had an opportunity to leave the client set.
		// We purge any such clients now in order to avoid memory leaks.
		//
		clientset.find_and_purge_dead_clients(this);
	}
	ASSERT(clientset.clients.is_empty());

	fobj_ii::unreferenced(bktno, fidlist_locked);
}

// Returns whether the client could cache information
bool
fobjplus_ii::can_cache()
{
	return (true);
}

bool
fobjplus_ii::is_cached()
{
	return (false);
}

//
// get_bind_info
// This type of fobj supports caching.
// Obtain information about the file object and return the information
// to the caller through the bind_info structure.
//
PXFS_VER::fobj_client_ptr
fobjplus_ii::get_bind_info(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client_p, cred_t *credp, Environment &env)
{
	PXFS_VER::fobj_client_ptr	clientmember_p =
	    PXFS_VER::fobj_client::_nil();

	ID_node		&client_node = env.get_src_node();

	if (CORBA::is_nil(client_p)) {
		//
		// No client specified.
		// Do not provide information for client.
		//
		binfo._d_ = PXFS_VER::bt_none;
		return (clientmember_p);
	}

	//
	// This may be the first time the file object has been touched
	// after a switchover/failover. So we need to ensure that
	// any needed fid conversion has been done.
	//
	if (is_replicated()) {
		(void) is_dead(env);
	}

	// Provide real information for client node
	binfo._d_ = PXFS_VER::bt_fobj;

	// Get current data page caching and mmap() status.
	binfo._u.bind_fobj.cachedata = is_cached();

	//
	// Ensure that there are no orphan clients for this node.
	//
	uint32_t	fs_number_joins = get_fsp()->get_number_joins();
	if (number_joins != fs_number_joins) {
		number_joins = fs_number_joins;

		clientset.find_and_purge_dead_clients(this);
	}

	//
	// Return any already registered client for this node,
	// or register this new client and use it.
	//
	clientmember_p = clientset.insert(client_node, client_p);

	if (!client_p->_equiv(clientmember_p)) {
		//
		// We are using an already registered client
		//
		// Do not get attributes.
		// Do not provide information for client.
		//
		binfo._d_ = PXFS_VER::bt_none;
	} else {
		//
		// This is a new client with no privileges.
		//
		// We grant the client write rights to the attributes
		// and return the value of the attributes to the caller.
		// The client will initialize the cache with the passed
		// attributes.
		//
		// Piggy-backing attributes
		// on the lookup_bind call saves an RPC call that would be
		// required in most cases because VOP_LOOKUP() is almost always
		// followed by VOP_GETATTR().
		//
		// Note that the client may receive an invalidation
		// *before* it is initialized. It is the responsibility
		// of the client to detect such a case and discard the
		// attributes during initialization.
		//
		attr_lock.wrlock();
		get_attr_common(credp, PXFS_VER::attr_write,
		    binfo._u.bind_fobj.attr,
		    binfo._u.bind_fobj.attr_seqnum, true, env);
		attr_lock.unlock();
		if (env.exception()) {
			env.clear();
			//
			// Don't return valid attributes.
			//
			binfo._u.bind_fobj.rights = PXFS_VER::attr_none;
		} else {
			binfo._u.bind_fobj.rights = PXFS_VER::attr_write;
		}
	}
	//
	// Warning: since the locks are not held, the debug trace info
	// may report after some other attribute action.
	//
	PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:g_b_i(%p)node %d r %llx w %llx c %llx\n",
	    this, env.get_src_node().ndid, attr_read_token.bitmask(),
	    attr_write_token.bitmask(), clientset.clients.bitmask()));

	return (clientmember_p);
}

//
// get_bind_info_no_attr
// This type of fobj supports caching.
// Obtain information about the file object and return the information
// to the caller through the bind_info structure.
//
PXFS_VER::fobj_client_ptr
fobjplus_ii::get_bind_info_no_attr(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client_p, cred_t *credp, Environment &env)
{
	PXFS_VER::fobj_client_ptr	clientmember_p =
	    PXFS_VER::fobj_client::_nil();

	ID_node		&client_node = env.get_src_node();

	if (CORBA::is_nil(client_p)) {
		//
		// No client specified.
		// Do not provide information for client.
		//
		binfo._d_ = PXFS_VER::bt_none;
		return (clientmember_p);
	}

	//
	// This may be the first time the file object has been touched
	// after a switchover/failover. So we need to ensure that
	// any needed fid conversion has been done.
	//
	if (is_replicated()) {
		(void) is_dead(env);
	}

	// Provide real information for client node
	binfo._d_ = PXFS_VER::bt_fobj;

	// Get current data page caching and mmap() status.
	binfo._u.bind_fobj.cachedata = is_cached();

	//
	// Don't return valid attributes.
	//
	binfo._u.bind_fobj.rights = PXFS_VER::attr_none;

	//
	// Ensure that there are no orphan clients for this node.
	//
	uint32_t	fs_number_joins = get_fsp()->get_number_joins();
	if (number_joins != fs_number_joins) {
		number_joins = fs_number_joins;

		clientset.find_and_purge_dead_clients(this);
	}

	//
	// Return any already registered client for this node,
	// or register this new client and use it.
	//
	clientmember_p = clientset.insert(client_node, client_p);

	if (!client_p->_equiv(clientmember_p)) {
		//
		// We are using an already registered client
		//
		// Do not get attributes.
		// Do not provide information for client.
		//
		binfo._d_ = PXFS_VER::bt_none;
	}
	//
	// Warning: since the locks are not held, the debug trace info
	// may report after some other attribute action.
	//
	PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:g_b_i_n_a(%p)node %d r %llx w %llx c %llx\n",
	    this, env.get_src_node().ndid, attr_read_token.bitmask(),
	    attr_write_token.bitmask(), clientset.clients.bitmask()));

	return (clientmember_p);
}

//
// convert_to_primary -  This is to convert a secondary to become a new
// primary. If an error is returned, the object should be left
// marked as a secondary.
//
int
fobjplus_ii::convert_to_primary()
{
	ASSERT(is_replicated());
	ASSERT(!primary_ready);

	int		error;
	vnode_t		*vnodep = fid_to_vnode(error);

	if (vnodep == NULL) {
		PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS,
		    PXFS_RED,
		    ("fobj+:convert_to_primary(%p) fid_to_vnode err %d\n",
		    this, error));

		return (error);
	}

	back_obj.vnodep = vnodep;

	recover_client_file_state(true);

	dirty_mtime = -1;

	primary_ready = true;

	if (fr_lockp != NULL) {
		fr_lockp->replay_active_locks();
	}
	return (0);
}

//
// convert_to_secondary
//
int
fobjplus_ii::convert_to_secondary()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:convert_to_secondary(%p)\n",
	    this));

	// A secondary should not hold reference to clients.
	clientset.empty();

	return (fobj_ii::convert_to_secondary());
}

//
// cache_call
// Based on the current and new value of attributes, determine
// what call we need to invoke on the cache object.
//
inline fobjplus_ii::cache_call
fobjplus_ii::get_cache_call(PXFS_VER::attr_rights newrights,
    PXFS_VER::attr_rights oldrights)
{
	//
	// Table that looks up the action needed to transit
	// from old_token_rights to new_token_rights.
	//
	// tab[new_token_rights][old_token_rights] -> action
	//
	static uchar_t	cache_call_tab[3][3] = {
		//  none	  read		    write
		{ CACHE_NOOP, CACHE_DROP,	CACHE_RELEASE },	// none
		{ CACHE_ERR,  CACHE_NOOP, 	CACHE_DOWNGRADE },	// read
		{ CACHE_ERR,  CACHE_ERR, 	CACHE_NOOP }		// write
	};

	cache_call	action =
	    (cache_call)cache_call_tab[newrights][oldrights];
	ASSERT(action != CACHE_ERR);
	return (action);
}

//
// new_rights
// Calculate new cache rights based on the rights currently held
// by the cache and the rights requested by another cache.
//
inline PXFS_VER::attr_rights
fobjplus_ii::new_rights(PXFS_VER::attr_rights current_rights,
    PXFS_VER::attr_rights other_req_rights)
{
	ASSERT(current_rights <= PXFS_VER::attr_write);
	ASSERT(other_req_rights <= PXFS_VER::attr_write);

	return ((PXFS_VER::attr_rights)
	    newrights_tab[current_rights][other_req_rights]);
}

char *
fobjplus_ii::cachecall_to_str(cache_call c)
{
	switch (c) {
	case CACHE_NOOP:
		return ("nop");

	case CACHE_DROP:
		return ("drop");

	case CACHE_DOWNGRADE:
		return ("downgrade");

	case CACHE_RELEASE:
		return ("release");

	case CACHE_ERR:
		return ("cache_err");

	default:
		return ("bogus value");
	}
}

//
// set_attr_rights_locked -
// record the attribute privileges for the specified node
// The assertions enforce the single write token / multiple read token rule.
// A node having the write token also has read privileges,
// but does not simultaneously hold the read token.
//
void
fobjplus_ii::set_attr_rights_locked(nodeid_t node, PXFS_VER::attr_rights rights)
{
	ASSERT(fobjplus_priv_lock.lock_held());
	ASSERT(clientset.clients.contains(node));

	if (rights == PXFS_VER::attr_none) {
		attr_read_token.remove_node(node);
		attr_write_token.remove_node(node);

	} else if (rights == PXFS_VER::attr_read) {
		attr_read_token.add_node(node);
		attr_write_token.remove_node(node);
		ASSERT(attr_write_token.is_empty());

	} else {
		ASSERT(rights == PXFS_VER::attr_write);
#ifdef DEBUG
		//
		// Either nobody has the write token or
		// this node already has the write token
		//
		nodeset		test_token;
		test_token.add_node(node);
		ASSERT(attr_write_token.is_empty() ||
		    attr_write_token.equal(test_token));
#endif
		attr_read_token.remove_node(node);
		attr_write_token.add_node(node);
		ASSERT(attr_read_token.is_empty());
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:set_attr_rights(%p) node %d r %llx w %llx c %llx\n",
	    this, node, attr_read_token.bitmask(), attr_write_token.bitmask(),
	    clientset.clients.bitmask()));
}

PXFS_VER::attr_rights
fobjplus_ii::get_attr_rights(nodeid_t node)
{
	ASSERT(attr_lock.lock_held());

	ASSERT(!(attr_read_token.contains(node) &&
	    attr_write_token.contains(node)));

#ifdef DEBUG
	if (attr_read_token.contains(node) || attr_write_token.contains(node))
		ASSERT(clientset.clients.contains(node));
#endif

	if (attr_write_token.contains(node)) {
		ASSERT(attr_read_token.is_empty());
		return (PXFS_VER::attr_write);

	} else if (attr_read_token.contains(node)) {
		ASSERT(attr_write_token.is_empty());
		return (PXFS_VER::attr_read);
	}
	return (PXFS_VER::attr_none);
}

//
// downgrade - the client's rights to a value that doesn't conflict with
// the requested rights.
//
// The client returns any cached dirty information to the server
// as part of the downgrade process.
//
// The thread doing this call should not be the same thread
// that is doing cache_get_all_attr() since this call can block on the
// client waiting for cache_get_all_attr() to return.
//
// Communications failures with the client are handled by the calling method.
//
sol::error_t
fobjplus_ii::downgrade_attr(client_set::client_entry *entryp,
    PXFS_VER::attr_rights req_rights, bool inval_access_check, Environment &env)
{
	ASSERT(attr_lock.write_held());

	//
	// The client could go away when we drop the list_lock.
	// So we use a smart pointer to ensure that the client
	// remains in existence.
	//
	PXFS_VER::fobj_client_var	client_v =
	    PXFS_VER::fobj_client::_duplicate(
	    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

	nodeid_t	client_node = entryp->node.ndid;

	vattr		attr;

	attr.va_mask = 0;

	sol::error_t	error = 0;
	bool		clear_attr_dirty = false;

	PXFS_VER::attr_rights	oldrights = get_attr_rights(client_node);
	PXFS_VER::attr_rights	newrights = new_rights(oldrights, req_rights);

	fobjplus_ii::cache_call	opcode = get_cache_call(newrights, oldrights);

	//
	// Note: if the client node crashes, the operation can fail.
	// We recover any token the cache may have held here.
	//
	switch (opcode) {
	case CACHE_DROP:
		PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS, PXFS_GREEN,
		    ("fobj+:downgrade_attr(%p) node %d %s %d to %d\n",
		    this, client_node, cachecall_to_str(opcode),
		    oldrights, newrights));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		//
		// Transition privilege from read to none
		//
		client_v->attr_release(attr_seqnum, access_seqnum, env);

		clientset.list_lock.rdlock();
		entryp->release();

		if (env.exception() == NULL) {
			if (entryp->is_zombie()) {
				break;
			}
			fobjplus_priv_lock.lock();
			attr_read_token.remove_node(client_node);
			ASSERT(attr_write_token.is_empty());
			if (inval_access_check) {
				access_cached.remove_node(client_node);
			}
			fobjplus_priv_lock.unlock();
			break;
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_RED,
		    ("fobj+:downgrade_attr(%p) node %d %s to %d\n",
		    this, client_node, cachecall_to_str(opcode),
		    PXFS_VER::attr_none));

		break;

	case CACHE_RELEASE:
		PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS, PXFS_GREEN,
		    ("fobj+:downgrade_attr(%p) node %d %s %d to %d\n",
		    this, client_node, cachecall_to_str(opcode),
		    oldrights, newrights));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		//
		// Transition privilege from write to none
		//
		client_v->attr_write_back_and_release(attr_seqnum,
		    access_seqnum, conv(attr), env);

		clientset.list_lock.rdlock();
		entryp->release();

		if (env.exception() == NULL) {
			if (attr.va_mask != 0) {
				//
				// After a node failure, we recover
				// any dirty attributes and recover
				// correct token privileges.
				//
				ASSERT(attr_write_token.contains(client_node));
				error = setattr(&attr, 0, kcred);
				clear_attr_dirty = true;
			}
			if (entryp->is_zombie()) {
				break;
			}
			fobjplus_priv_lock.lock();
			ASSERT(attr_read_token.is_empty());
			attr_write_token.remove_node(client_node);
			if (inval_access_check) {
				access_cached.remove_node(client_node);
			}
			fobjplus_priv_lock.unlock();
			break;
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_RED,
		    ("fobj+:downgrade_attr(%p) node %d %s to %d\n",
		    this, client_node, cachecall_to_str(opcode),
		    PXFS_VER::attr_none));

		break;

	case CACHE_DOWNGRADE:
		PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS, PXFS_GREEN,
		    ("fobj+:downgrade_attr(%p) node %d %s %d to %d\n",
		    this, client_node, cachecall_to_str(opcode),
		    oldrights, newrights));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		//
		// Transition privilege from write to read
		//
		client_v->attr_write_back_and_downgrade(attr_seqnum,
		    access_seqnum, conv(attr), env);

		clientset.list_lock.rdlock();
		entryp->release();

		if (env.exception() == NULL) {
			if (attr.va_mask != 0) {
				ASSERT(attr_write_token.contains(client_node));
				error = setattr(&attr, 0, kcred);
				clear_attr_dirty = true;
			}
			if (entryp->is_zombie()) {
				break;
			}
			fobjplus_priv_lock.lock();
			//
			// Only downgrade privileges if the client
			// has not dropped privileges.
			//
			if (attr_write_token.contains(client_node)) {
				ASSERT(attr_read_token.is_empty());
				attr_write_token.remove_node(client_node);
				attr_read_token.add_node(client_node);
			}
			if (inval_access_check) {
				access_cached.remove_node(client_node);
			}
			fobjplus_priv_lock.unlock();
			break;
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_RED,
		    ("fobj+:downgrade_attr(%p) node %d %s to %d\n",
		    this, client_node, cachecall_to_str(opcode),
		    PXFS_VER::attr_read));

		break;

	case CACHE_NOOP:
		if (inval_access_check &&
		    access_cached.contains(client_node)) {
			// Do not hold the clientset lock across an invocation
			entryp->hold();
			clientset.list_lock.unlock();

			client_v->access_inval_cache(access_seqnum, env);

			clientset.list_lock.rdlock();
			entryp->release();

			if (env.exception() == NULL) {
				if (entryp->is_zombie()) {
					break;
				}
				fobjplus_priv_lock.lock();
				access_cached.remove_node(client_node);
				fobjplus_priv_lock.unlock();
				break;
			}
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJPLUS,
			    PXFS_RED,
			    ("fobj+:downgrade_attr(%p) node %d inval access\n",
			    this, client_node));
		}
		break;

	case CACHE_ERR:
	default:
		os::panic("fobjplus_ii:: invalid cache_call");
	}

	//
	// Avoid making an invocation to clear dirty attributes to the client
	// if it is not supported. This is supported from version 1
	//
	if (PXFS_VER::fobj_client::_version(client_v) < 1) {
		return (error);
	}
	if (clear_attr_dirty) {
		//
		// Do not allow attribute changes until the system has
		// acknowleged receipt of the dirty attributes. In the case
		// of a pxfs server failover, the new pxfs server will find
		// the old dirty attributes and write them to storage.
		// Thus we delay further changes to attributes until after
		// this threat is over.
		//
		// This work can be done while the other node does work that
		// can include preparing changes to attributes. This async
		// behavior improves performance.
		//
		wait_for_clear_lock.lock();
		ASSERT(wait_for_attr_clear == false);
		wait_for_attr_clear = true;
		wait_for_clear_lock.unlock();

		clear_attr_dirty_threadpool.defer_processing(
		    new clear_attr_dirty_task(this, client_v,
		    get_fsp()->get_server_incn()));
	}

	return (error);
}

//
// Clear attr_dirty on the client and signal threads waiting for
// attribute flush.
//
void
fobjplus_ii::clear_attr_dirty(PXFS_VER::fobj_client_var client_v,
    uint32_t server_incarn)
{
	Environment env;

	client_v->clear_attr_dirty(server_incarn, env);

	//
	// Permit attribute flush to proceed.
	//
	wait_for_clear_lock.lock();

	if (server_incarn != get_fsp()->get_server_incn()) {
		//
		// This task was created for an obsolete server.
		// ignore this task.
		//
		wait_for_clear_lock.unlock();
		return;
	}
	ASSERT(wait_for_attr_clear);
	wait_for_attr_clear = false;
	wait_for_clear_cv.broadcast();
	wait_for_clear_lock.unlock();

	if (env.exception() != NULL) {
		//
		// The only possible exception is a COMM or VERSION
		// exception failure.
		//
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()) ||
		    CORBA::VERSION::_exnarrow(env.exception()));

		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:clear_attr_dirty(%p) Client node death while "
		    "clearing attr_dirty. Exception: %s\n",
		    this, env.exception()->_name()));
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_GREEN,
		    ("fobj+:clear_attr_dirty(%p) attr_dirty cleared on client "
		    "node.\n", this));
	}
}

//
// setattr - writes file attributes.  Call with lock_attributes() held.
//
int
fobjplus_ii::setattr(vattr *vattrp, int attrflags, cred_t *credp)
{
	ASSERT(attr_lock.write_held());

	//
	// Wait for any pending attr_dirty to be cleared.
	//
	wait_for_clear_lock.lock();
	while (wait_for_attr_clear) {
		wait_for_clear_cv.wait(&wait_for_clear_lock);
	}
	wait_for_clear_lock.unlock();

	vnode_t	*vnodep = get_vp();

#if SOL_VERSION >= __s10
	int error = VOP_SETATTR(vnodep, vattrp, attrflags, credp, NULL);
#else
	int error = VOP_SETATTR(vnodep, vattrp, attrflags, credp);
#endif
	if (error == 0) {
		// Show that file attributes were modified.
		mod_time = os::gethrtime();

		//
		// If the mtime was updated then, cache it so that it can
		// be reapplied after the pages are flushed when fastwrites
		// are enabled. Refer to comments in pxreg::setattr().
		//
		if ((get_fsp()->fastwrite_enabled()) &&
		    (vattrp->va_mask & (AT_MTIME|AT_MODE|AT_UID|AT_GID))) {
			dirty_mtime = vattrp->va_mtime.tv_sec * NANOSEC
			    + vattrp->va_mtime.tv_nsec;
		}
	}

	if (vn_has_cached_data(vnodep)) {
		//
		// File systems can place pages in the page cache after the
		// file size has been extended via the VOP_SETATTR operation.
		// These pages have to be written to disk.
		// In the case of UFS, it creates and caches the last block
		// of the file. We have to flush this.
		// XXX There's a subtle problem here. We are calling
		// VOP_PUTPAGE() with B_INVAL|B_FORCE. This says, "try and
		// write out the page to disk but destroy it anyway even
		// if there are errors". This seems risky if there are errors.
		// The alternative is to pass in the flag B_FREE which ensures
		// that the page is not freed if there is an error. However,
		// if the page is written out successfully, the page is "freed"
		// by adding it to the pagecache list. But that's not what we
		// want. We actually want it to be "destroyed".
		// So (B_INVAL|B_FREE) seems like the right flags to use here.
		// Note that we can't set flags to B_INVAL and B_FREE together.
		// That would cause bug 4310795.
		//
#if	SOL_VERSION >= __s11
		error = VOP_PUTPAGE(vnodep, (offset_t)0, (size_t)0,
		    B_INVAL | B_FORCE, credp, NULL);
#else
		error = VOP_PUTPAGE(vnodep, (offset_t)0, (size_t)0,
		    B_INVAL | B_FORCE, credp);
#endif

		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_GREEN,
		    ("fobj+:setattr(%p) pages flushed due to file extension\n",
		    this));
	}
	return (error);
}

//
// fobj_ii::set_secattributes - set the secure attributes (ACLs)
// of a fobj object.
// This is used when attribute caching is enabled on the pxfs client.
//
void
fobjplus_ii::set_secattributes(const PXFS_VER::secattr &sattr,
    int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	vsecattr_t	vsec;
	int		error;
	vnode_t		*vnodep = get_vp();

	vsec.vsa_mask = sattr.mask;
	if (sattr.mask & VSA_ACL) {
		vsec.vsa_aclcnt = (int)sattr.acl_ent.length();
		vsec.vsa_aclentp = sattr.acl_ent.buffer();
	} else {
		vsec.vsa_aclcnt = 0;
		vsec.vsa_aclentp = NULL;
	}
	if (sattr.mask & VSA_DFACL) {
		vsec.vsa_dfaclcnt = (int)sattr.acl_def_ent.length();
		vsec.vsa_dfaclentp = sattr.acl_def_ent.buffer();
	} else {
		vsec.vsa_dfaclcnt = 0;
		vsec.vsa_dfaclentp = NULL;
	}

	attr_lock.wrlock();

	// All previous access checks are now invalid
	access_seqnum++;

	//
	// All attributes and access caches must be invalidated
	//
	error = downgrade_attr_all_common(PXFS_VER::attr_write, true, 0, true);

	if (error == 0) {
#if SOL_VERSION >= __s10
		VOP_RWLOCK(vnodep, V_WRITELOCK_TRUE, NULL);
#else
		VOP_RWLOCK(vnodep, 1);
#endif
#if	SOL_VERSION >= __s11
		error = VOP_SETSECATTR(vnodep, &vsec, secattrflag, credp, NULL);
#else
		error = VOP_SETSECATTR(vnodep, &vsec, secattrflag, credp);
#endif
#if SOL_VERSION >= __s10
		VOP_RWUNLOCK(vnodep, V_WRITELOCK_TRUE, NULL);
#else
		VOP_RWUNLOCK(vnodep, 1);
#endif
	}

	if (error == 0) {
		error = get_fsp()->get_fs_dep_implp()->fs_fsync(vnodep, credp);
	}
	attr_lock.unlock();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// cache_new_client - the client needs to connect again to the server object.
// The previously given fobj_client went away. The client already knows
// fixed information about the file object, such as file type.
//
void
fobjplus_ii::cache_new_client(PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	client2_p = get_bind_info_no_attr(binfo, client1_p, kcred,
	    _environment);
}

void
fobjplus_ii::acquire_token_locks()
{
	fobjplus_priv_lock.lock();
}

void
fobjplus_ii::release_token_locks()
{
	fobjplus_priv_lock.unlock();
}

//
// remove_client_privileges - the client died. So remove privileges.
// The client may not have any privileges.
//
void
fobjplus_ii::remove_client_privileges(nodeid_t node_id)
{
	ASSERT(fobjplus_priv_lock.lock_held());
	attr_read_token.remove_node(node_id);
	attr_write_token.remove_node(node_id);
	access_cached.remove_node(node_id);
}

//
// cache_remove_client - the client is going away.
//
void
fobjplus_ii::cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
    Environment &_environment)
{
	//
	// Remove the client from the list and drop any privileges atomically.
	//
	clientset.remove(this, client_p);

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:c_r_c(%p) client_p %p node %d r %llx w %llx c %llx\n",
	    this, client_p, _environment.get_src_node().ndid,
	    attr_read_token.bitmask(), attr_write_token.bitmask(),
	    clientset.clients.bitmask()));
}

//
// cache_get_all_attr
// Acquire the read or write token and return the file attributes to the cache.
//
void
fobjplus_ii::cache_get_all_attr(solobj::cred_ptr credobj,
    PXFS_VER::attr_rights rights, sol::vattr_t &attributes, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);

	server_incarn = get_fsp()->get_server_incn();

	attr_lock.wrlock();
	get_attr_common(credp, rights, attributes, seqnum, true, _environment);
	attr_lock.unlock();
}

//
// get_attr_common - ensures that no other node has conflicting attribute
// permissions. Records the attribute permissions of the requesting node.
// Obtains the full set of attributes from the underling file system.
//
void
fobjplus_ii::get_attr_common(cred_t *credp,
    PXFS_VER::attr_rights req_rights, sol::vattr_t &attributes,
    uint64_t &seqnum, bool get_list_lock, Environment &env)
{
	ASSERT(attr_lock.write_held());

	ASSERT(req_rights == PXFS_VER::attr_read ||
	    req_rights == PXFS_VER::attr_write);

	int			error;
	nodeid_t		client_node = env.get_src_node().ndid;
	PXFS_VER::attr_rights	old_rights = get_attr_rights(client_node);

	if (env.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:g_a_c(%p) orphan node %d\n",
		    this, client_node));
		pxfslib::throw_exception(env, EIO);
		return;
	}

	if (req_rights > old_rights) {
		//
		// Attribute rights are changing.
		// Downgrade all other client rights to non-conflicting
		// values. We wait until all clients have responded or died.
		//
		error = downgrade_attr_all_common(req_rights, false,
		    client_node, get_list_lock);
		if (error) {
			seqnum = attr_seqnum;
			env.exception(new sol::op_e(error));
			return;
		}
		set_attr_rights(client_node, req_rights);

		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_GREEN,
		    ("fobj+:get_attr(%p) node %d rights %d to %d seq %llu"
		    " clients %llx\n",
		    this, client_node, old_rights, req_rights, attr_seqnum,
		    clientset.clients.bitmask()));
	}
	seqnum = attr_seqnum;

	// Always return all file attributes
	vattr	*vap = (vattr *)&attributes;
	vap->va_mask = AT_ALL;
#if	SOL_VERSION >= __s11
	error = VOP_GETATTR(get_vp(), vap, 0, credp, NULL);
#else
	error = VOP_GETATTR(get_vp(), vap, 0, credp);
#endif
	if (error) {
		env.exception(new sol::op_e(error));
	}
}

//
// cache_write_all_attr - update the attributes with information
// from the client node. Drop attribute privileges if "discard" is true.
//
// The server may have downgraded the client's attribute privileges while
// this request was in flight. So do not assert that the client has privileges.
//
void
fobjplus_ii::cache_write_all_attr(sol::vattr_t &attributes, int32_t attrflags,
    bool discard, bool sync, solobj::cred_ptr credobj,
    uint32_t server_incarn, Environment &_environment)
{
	vattr	*vattrp = (vattr *)&attributes;
	cred_t	*credp = solobj_impl::conv(credobj);

	nodeid_t	client_node = _environment.get_src_node().ndid;

	attr_lock.wrlock();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:c_w_a_a(%p) orphan node %d\n",
		    this, client_node));
		attr_lock.unlock();
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	if (server_incarn != get_fsp()->get_server_incn()) {
		//
		// The request was intended for a previous server.
		// The state recovery process will pick up any dirty
		// attributes. Ignore this request.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:c_w_a_a(%p) stale request node %d\n",
		    this, client_node));
		attr_lock.unlock();
		pxfslib::throw_exception(_environment, ECANCELED);
		return;
	}

	if (get_attr_rights(client_node) != PXFS_VER::attr_write) {
		//
		// Some other provider stole the write token from this provider
		// while this call was in flight.  The data from this client
		// has come in with the invalidation request, so it is safe to
		// return success. The client should see that an invalidation
		// came in and not install the attributes we return.
		//
		if (discard) {
			//
			// The client is dropping the file attribute token
			//
			fobjplus_priv_lock.lock();
			attr_read_token.remove_node(client_node);
			fobjplus_priv_lock.unlock();
		}
		attr_lock.unlock();
		return;
	}

	int	error = setattr(vattrp, attrflags, credp);

	if (discard) {
		//
		// The client is dropping the file attribute token
		//
		fobjplus_priv_lock.lock();
		attr_read_token.remove_node(client_node);
		attr_write_token.remove_node(client_node);
		fobjplus_priv_lock.unlock();

	} else if (error == 0) {
		//
		// Return the current attributes (usually a_ctime is updated).
		//
		vattrp->va_mask = AT_ALL;
#if	SOL_VERSION >= __s11
		error = VOP_GETATTR(get_vp(), vattrp, 0, credp, NULL);
#else
		error = VOP_GETATTR(get_vp(), vattrp, 0, credp);
#endif
	}

	attr_lock.unlock();
	if (error != 0) {
		_environment.exception(new sol::op_e(error));
	} else if (sync && !_environment.is_local_client()) {
		//
		// If the attributes have not already been flushed to disk,
		// flush the attributes to storage.
		//
		error = get_fsp()->get_fs_dep_implp()->
		    sync_if_necessary(mod_time, get_vp(), kcred);
	}
}

//
// cache_attr_drop_token - drop all attribute privileges for this client.
// The client had no dirty information.
//
// The server may have downgraded the client's attribute privileges while
// this request was in flight. So do not assert that the client has privileges.
//
void
fobjplus_ii::cache_attr_drop_token(uint32_t server_incarn,
    Environment &_environment)
{
	nodeid_t	client_node = _environment.get_src_node().ndid;

	fobjplus_priv_lock.lock();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:c_a_d_t(%p) orphan node %d\n",
		    this, client_node));
	} else if (server_incarn != get_fsp()->get_server_incn()) {
		//
		// The request was intended for a previous server.
		// The state recovery process notes that
		// the attributes have been dropped. Ignore this request.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:c_a_d_t(%p) stale request node %d\n",
		    this, client_node));
	} else {
		//
		// The client is dropping any file attribute token
		//
		attr_read_token.remove_node(client_node);
		attr_write_token.remove_node(client_node);
	}
	fobjplus_priv_lock.unlock();
}

//
// cache_set_attributes - the client can issue this request regardless
// as to whether the client possesses attribute write permission.
//
// Write any cached dirty attributes with kernel credentials and no flags.
// Write the requested attributes with user credentials and specified flags.
// The possibility of different flags leads to the separate VOP_SETATTR
// operations.
//
// Return the full set of new attributes with the attribute write permission.
//
void
fobjplus_ii::cache_set_attributes(
    sol::vattr_t &wb_attributes, int32_t wb_attrflags,
    const sol::vattr_t &attributes, int32_t attrflags,
    solobj::cred_ptr credobj, uint64_t &seqnum,
    uint32_t &server_incarn, Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	vnode_t	*vnodep = get_vp();
	int	error;
	bool	must_inval_access;

	nodeid_t	client_node = _environment.get_src_node().ndid;

	server_incarn = get_fsp()->get_server_incn();

	//
	// Each thread on a given node will use locks to allow only one
	// call to proceed at a time. We use lock_attributes()
	// to make sure only one request from each node can proceed at a time.
	//
	attr_lock.wrlock();

	if (_environment.is_orphan()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_AMBER,
		    ("fobj+:c_s_a(%p) orphan node %d\n",
		    this, client_node));
		attr_lock.unlock();
		pxfslib::throw_exception(_environment, EIO);
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:set_attributes(%p) mask %x wb mask %x rights %d\n",
	    this, attributes.va_mask, wb_attributes.va_mask,
	    get_attr_rights(client_node)));

	// Wait for any pending attr_dirty to be cleared.
	wait_for_clear_lock.lock();
	while (wait_for_attr_clear) {
		wait_for_clear_cv.wait(&wait_for_clear_lock);
	}
	wait_for_clear_lock.unlock();

	if (attributes.va_mask & (AT_GID | AT_UID | AT_MODE)) {
		//
		// When these file attributes change,
		// the access cache must be invalidated.
		// The client already invalidated its access cache.
		//
		access_seqnum++;
		must_inval_access = true;
	} else {
		must_inval_access = false;
	}

	if (get_attr_rights(client_node) == PXFS_VER::attr_write) {
		seqnum = attr_seqnum;
		//
		// Client cache still has the atttribute write token.
		// So write back any dirty attributes.
		//
		if (wb_attributes.va_mask != 0) {
			//
			// The client should not be caching
			// these types of dirty info.
			// NOTE: The file mode change can be cached on client
			// when write happens by non super user to setuid
			// executable file. (see pxfobjplus::clear_suid()).
			//
			ASSERT((wb_attributes.va_mask &
			    (AT_GID | AT_UID)) == 0);
#if SOL_VERSION >= __s10
			error = VOP_SETATTR(vnodep, (vattr *)&wb_attributes,
			    wb_attrflags, kcred, NULL);
#else
			error = VOP_SETATTR(vnodep, (vattr *)&wb_attributes,
			    wb_attrflags, kcred);
#endif
			//
			// Do not need to force the attributes to storage,
			// because we will do that after the next VOP_SETATTR
			// in this method.
			//
			if (error) {
				attr_lock.unlock();
				_environment.exception(new sol::op_e(error));
				return;
			}
		}
		//
		// Downgrade access caches on all other nodes.
		// We have to do this on all other nodes even if they
		// don't have cached attributes.
		//
		if (must_inval_access) {
			inval_access_all(client_node);
		}
	} else {
		//
		// The client needs a new write token.
		// Downgrade all other client rights to non-conflicting
		// values. We will block waiting for a client cache to release
		// its token (or crash) in downgrade().
		//
		error = downgrade_attr_all_common(PXFS_VER::attr_write,
		    must_inval_access, client_node, true);

		seqnum = attr_seqnum;

		if (error) {
			attr_lock.unlock();
			_environment.exception(new sol::op_e(error));
			return;
		}
		//
		// Successfully revoked every other node's privileges.
		// Now grant write privilege.
		//
		set_attr_rights(client_node, PXFS_VER::attr_write);
	}
	error = setattr((vattr *)&attributes, attrflags, credp);
	if (error == 0) {
		//
		// If the mtime was updated then, cache it so that it can
		// be reapplied after the pages are flushed when fastwrites
		// are enabled. Refer to comments in pxreg::setattr().
		//
		if ((get_fsp()->fastwrite_enabled()) &&
		    (attributes.va_mask & (AT_MTIME|AT_MODE|AT_UID|AT_GID))) {
			dirty_mtime = attributes.va_mtime.tv_sec * NANOSEC
			    + attributes.va_mtime.tv_nsec;
		}

		error = get_fsp()->get_fs_dep_implp()->fs_fsync(vnodep, credp);
	}
	if (error) {
		attr_lock.unlock();
		_environment.exception(new sol::op_e(error));
		return;
	}
	//
	// Always return all attributes
	//
	vattr	*vap = (vattr *)&wb_attributes;
	vap->va_mask = AT_ALL;
#if	SOL_VERSION >= __s11
	error = VOP_GETATTR(vnodep, vap, 0, credp, NULL);
#else
	error = VOP_GETATTR(vnodep, vap, 0, credp);
#endif
	if (error) {
		attr_lock.unlock();
		_environment.exception(new sol::op_e(error));
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:set_attributes(%p) mask %x seqnum %llu\n",
	    this, attributes.va_mask, attr_seqnum));

	attr_lock.unlock();
}

//
// cache_access - Check access permissions on a fobj object.
//
void
fobjplus_ii::cache_access(int32_t accmode, int32_t accflags,
    solobj::cred_ptr credobj,
    bool &allowed, uint64_t &seqnum, Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);

	attr_lock.rdlock();

	//
	// Note that we don't need to downgrade attributes to read-only
	// since changes to the UID, GID, and mode are write through.
	//
#if	SOL_VERSION >= __s11
	int	error = VOP_ACCESS(get_vp(), accmode, accflags, credp, NULL);
#else
	int	error = VOP_ACCESS(get_vp(), accmode, accflags, credp);
#endif

	//
	// We can't use an exception to return EACCES since
	// the 'seqnum' out parameter won't be returned.
	// Instead, we return a boolean for error == 0 or error == EACCES
	// and an exception for any other case.
	//
	if (error != 0 && error != EACCES) {
		attr_lock.unlock();
		pxfslib::throw_exception(_environment, error);
		return;
	}

	allowed = (error == 0) ? true : false;

	nodeid_t	client_node = _environment.get_src_node().ndid;
	access_cached.add_node(client_node);

	seqnum = access_seqnum;

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:access(%p) %d access_seqnum %llu\n",
	    this, allowed, access_seqnum));

	attr_lock.unlock();
}

//
// client_gone - remove privileges for node
//
void
fobjplus_ii::client_gone(nodeid_t client_node)
{
	fobjplus_priv_lock.lock();
	attr_read_token.remove_node(client_node);
	attr_write_token.remove_node(client_node);
	access_cached.remove_node(client_node);
	fobjplus_priv_lock.unlock();
}

//
// downgrade_attr_all
// Force all attribute caches other than the 'skip_node' to downgrade
// their rights to values not colliding with the requested value 'req_rights'.
//
sol::error_t
fobjplus_ii::downgrade_attr_all(PXFS_VER::attr_rights req_rights,
    bool inval_access_check, nodeid_t skip_node)
{
	return (downgrade_attr_all_common(req_rights, inval_access_check,
	    skip_node, true));
}

//
// downgrade_attr_all_common
// Force all attribute caches other than the 'skip_node' to downgrade
// their rights to values not colliding with the requested value 'req_rights'.
//
sol::error_t
fobjplus_ii::downgrade_attr_all_common(PXFS_VER::attr_rights req_rights,
    bool inval_access_check, nodeid_t skip_node, bool get_list_lock)
{
	ASSERT(attr_lock.write_held());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:downgrade_attr_all(%p) skip_node %d req %d\n",
	    this, skip_node, req_rights));

	Environment	e;
	bool		dead_clients = false;
	sol::error_t	error;

	//
	// Only one client can have the attribute write token.
	// So only one client can write back dirty attributes.
	// Thus there should never be more than one error.
	//
	// Note that communication failures are handled separately.
	//
	sol::error_t	first_error = 0;

	//
	// A new sequence number is needed so that clients will
	// recognize this as a valid attribute invalidation.
	//
	attr_seqnum++;

	if (get_list_lock) {
		clientset.list_lock.rdlock();
	} else {
		ASSERT(clientset.list_lock.lock_held());
	}

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// Do not contact the dead client,
			// but ensure that the client has no privileges
			// in this area
			//
			client_gone(entryp->node.ndid);
			dead_clients = true;
			continue;
		}

		if (entryp->node.ndid == skip_node) {
			continue;
		}

		// Tell the client to take the specified action
		error = downgrade_attr(entryp,
		    req_rights, inval_access_check, e);

		if (error != 0 && first_error == 0) {
			first_error = error;
		}
		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJPLUS,
			    PXFS_GREEN,
			    ("fobj+:downgrade_attr_all(%p) dead node %d\n",
			    this, entryp->node.ndid));

			client_gone(entryp->node.ndid);
			entryp->clear();
			dead_clients = true;
		}
	}
	if (get_list_lock) {
		clientset.list_lock.unlock();
		//
		// Can only purge dead clients when the list lock is not held.
		// Can safely clean up the dead clients later.
		//
		if (dead_clients) {
			clientset.purge_dead_clients(this);
		}
	}
	return (first_error);
}

//
// inval_access_all
// Force all clients other than the skip_node to invalidate
// their client-side access check cache.
//
void
fobjplus_ii::inval_access_all(nodeid_t skip_node)
{
	ASSERT(attr_lock.write_held());

	Environment	e;
	bool		dead_clients = false;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (; NULL != (entryp = iter.get_current()); iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// Do not contact the dead client,
			// but ensure that the client has no privileges
			// in this area
			//
			client_gone(entryp->node.ndid);
			dead_clients = true;
			continue;
		}

		if (entryp->node.ndid == skip_node) {
			continue;
		}

		if (!access_cached.contains(entryp->node.ndid)) {
			continue;
		}

		//
		// The client could go away when we drop the list_lock.
		// So we use a smart pointer to ensure that the client
		// remains in existence.
		//
		PXFS_VER::fobj_client_var	client_v =
		    PXFS_VER::fobj_client::_duplicate(
		    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		// Tell the client to take the specified action
		client_v->access_inval_cache(access_seqnum, e);

		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_GREEN,
		    ("fobj+:inval_access_all(%p) node %d\n",
		    this, entryp->node.ndid));

		clientset.list_lock.rdlock();
		entryp->release();

		if (entryp->is_zombie()) {
			continue;
		}
		access_cached.remove_node(entryp->node.ndid);

		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJPLUS,
			    PXFS_GREEN,
			    ("fobj+:inval_access_all(%p) dead node %d\n",
			    this, entryp->node.ndid));

			client_gone(entryp->node.ndid);
			entryp->clear();
			dead_clients = true;
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}
}

//
// is_dead
// Return true if this is a dead replicated file system object
// (i.e., there was a fatal error in convert_to_primary() or
// convert_to_secondary() but the replica manager still thinks
// the service is alive).
// Note that this routine is only called on the primary but the object
// may be marked as not primary_ready if it couldn't be converted to primary
// or it was released in fs_ii::unmount().
// If we return true, the environment is also set to return an exception.
//
// After using the FID to obtain a real vnode, this method
// reconstructs client information by querying the clients
//
bool
fobjplus_ii::is_dead(Environment &env)
{
	if (fobj_released) {
		//
		// Note that EIO is correct, the "disk" failed.
		// ESTALE is returned by NFS when VFS_VGET() returns error.
		//
		pxfslib::throw_exception(env, EIO);
		return (true);
	}

	if (!primary_ready) {
		int		error;
		vnode_t		*vnodep = fid_to_vnode(error);

		if (vnodep == NULL) {
			PXFS_DBPRINTF(PXFS_TRACE_FOBJPLUS,
			    PXFS_RED,
			    ("fobj+:is_dead(%p) fid_to_vnode failed\n",
			    this));
			pxfslib::throw_exception(env, error);
			return (true);
		}
		//
		// The first primary ready test was just a hint.
		// We have to do the real test under the lock.
		// This happens only once after a failover,
		// so it is the rare case.
		//
		// We use the fidlock because when the file object is
		// not primary_ready the file object resides on the fidlist.
		//
		fs_implp->fidlock.wrlock();
		if (primary_ready) {
			//
			// Another won the race.
			//
			ASSERT(back_obj.vnodep != NULL);

			// Will not use the vnode that was found
			VN_RELE(vnodep);
		} else {
			//
			// In this case, we are executing on the
			// primary and the fobj is not backed by
			// a vnode. Therefore, perform just-in-time
			// fid conversion and then allow invocations.
			//
			int		bktno = fs_implp->vp_to_bucket(vnodep);
			fs_implp->fobjlist_locks[bktno].wrlock();

			// Transfer the hold on the vnode to the file object
			back_obj.vnodep = vnodep;

			get_fsp()->remove_from_fidlist(this);
			bool	added = get_fsp()->add_to_list(this);
			ASSERT(added);

			recover_client_file_state(false);

			primary_ready = true;
			fs_implp->fobjlist_locks[bktno].unlock();
		}
		fs_implp->fidlock.unlock();
	}
	return (get_fsp()->is_dead(env));
}

//
// recover_client_file_state - the server file object needs to recover
// after a node failure. This method queries each node containing
// a proxy file system for information belonging to a corresponding
// client file object.
//
// The old server may have died after a client had placed dirty attributes
// in an invocation reply. A reply invocation is not HA and hence can be lost.
// Any such dirty attributes have to be recovered.
//
// To do this, the client places a copy of the attributes in its attr_dirty
// field before shipping the reply. The recovery mechanism retrieves this
// copy and writes it out when necessary.
//
// The server asynchronusly clears the attr_dirty field. When this asynchronous
// action is pending, other nodes are blocked from flushing
// dirty attributes. This ensures that only one node can have attr_dirty set
// for a file at any given time.
//
// The recovery mechanism takes advantage of the object reference knowledge
// in the infrastructure. Only a node with an object reference can have
// a PXFS client for the file. The recovery mechanism also takes advantage
// of communication patterns, PXFS clients communicate with the server
// and not with other PXFS clients; which limits how PXFS clients obtain
// file object references. The third important fact is that no PXFS client
// can do anything with a file object reference, including obtain one,
// without recovery being processed first.
//
void
fobjplus_ii::recover_client_file_state(bool become_primary_time)
{
	PXFS_VER::recovery_info	*recoveryp = NULL;
	vattr			*rec_attrp;

	fsmgr_server_ii		*fsmgrp;
	CORBA::Object_var	obj_v;
	Environment		e;

	vattr			latest_attr;
	latest_attr.va_mask = 0;

	uint64_t		latest_seqnum = 0;
	uint64_t		latest_dirty_seqnum = 0;

	ID_node			node_dirty;

	attr_lock.wrlock();
	fobjplus_priv_lock.lock();

	// Initialize data structures prior to obtaining info from clients
	prepare_recover();

	//
	// Marshal the fid into a form that can be shipped
	//
	const fid_t			*fidp = get_fidp();
	PXFS_VER::fobj_fid_t		fobj_fid;
	fobj_fid.fobjid_len = fidp->fid_len;
	bcopy(fidp->fid_data, fobj_fid.fobjid_data, (size_t)fidp->fid_len);

	//
	// Find out which nodes hold references.
	// This will use a replica_handler, because we do not do
	// recovery for file objects that are not HA.
	//
	nodeset			refset;
	if (become_primary_time) {
		//
		// After a failover or switchover, the system clears information
		// about clients and requests that all clients reconnect.
		// The reference information is not useful until all
		// clients have reconnected. During this time PXFS converts
		// to primary status those file objects with active locks.
		// PXFS converts other file objects at a later time when
		// the reference information about clients is valid.
		// Since the reference information is not valid yet,
		// query all of the pxfs clients.
		//
		// Warning: this will work, but performance will scale
		// poorly with large numbers of nodes. Two factors mitigate
		// the situation. File locks are not used heavily.
		// Most clusters have few nodes, typically 2 nodes.
		//
		refset.set((membership_bitmask_t)0xffffffffffffffff);
	} else {
		((replica_handler *)get_handler())->
		    identify_nodes_holding_refs(refset);
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    refset.is_empty() ? PXFS_AMBER : PXFS_GREEN,
	    ("fobj+:recover_c_f_s(%p) refset %llx\n",
	    this, refset.bitmask()));

	prov_common_iter	fsmgr_iter(get_fsp()->get_fsmgr_set());
	while (!CORBA::is_nil(obj_v = fsmgr_iter.nextref())) {
		fsmgrp = fsmgr_server_ii::get_fsmgr_server_ii(obj_v);

		if (!refset.contains(fsmgrp->get_nodeid())) {
			//
			// The node does not have an object reference,
			// which means that it cannot have a PXFS client
			// for this file object.
			//
			continue;
		}
		fsmgrp->get_clientmgr()->recover_state(fobj_fid, recoveryp, e);
		if (e.exception()) {
			//
			// The client is dead or has no entry.
			// The node may have an object reference in
			// checkpointed information only.
			//
			e.clear();
		} else {
			ASSERT(fsmgrp->get_nodeid() == recoveryp->ndid);
			//
			// Recover the state from this client
			//
			recovered_state(*recoveryp);

			rec_attrp = (vattr *)&(recoveryp->attr);

			if (latest_seqnum < recoveryp->attr_seqnum) {
				//
				// Found the latest attribute sequence number
				// so far.
				//
				latest_seqnum = recoveryp->attr_seqnum;
			}

			if (recoveryp->dirty_seqnum != 0) {
				if (recoveryp->dirty_seqnum <
				    latest_dirty_seqnum) {
					//
					// These dirty attributes are obsolete.
					// They should be cleared.
					//
					clear_obsolete_attr(
					    ID_node(recoveryp->ndid,
						    recoveryp->incn));
				} else if (latest_dirty_seqnum != 0) {
					//
					// The earlier recorded 'latest' dirty
					// attributes are obsolete.
					// They should be cleared.
					//
					clear_obsolete_attr(node_dirty);
				}
			}

			if (latest_dirty_seqnum < recoveryp->dirty_seqnum) {
				//
				// This is the latest dirty attribute sequence
				// number so far.
				//
				latest_dirty_seqnum = recoveryp->dirty_seqnum;
				latest_attr = *rec_attrp;
				node_dirty = ID_node(recoveryp->ndid,
						    recoveryp->incn);
			}
		}
		delete recoveryp;
		recoveryp = NULL;
	}

	if (latest_seqnum != 0 && latest_seqnum == latest_dirty_seqnum) {
		//
		// This client has the latest sequence number. It also has
		// attr_dirty set. This means the attributes it shipped to the
		// old server could have been lost. We have to write out the
		// recovered dirty attributes.
		//

		ASSERT(latest_attr.va_mask != 0);

		vnode_t	*vnodep = get_vp();
		int	error;

		error = setattr(&latest_attr, 0, kcred);
		if (error == 0) {
			error = get_fsp()->get_fs_dep_implp()->
			    fs_fsync(vnodep, kcred);
		}

		// There is no way to recover at this point
		ASSERT(error == 0);

		if (latest_attr.va_mask & (AT_GID | AT_UID | AT_MODE)) {
			//
			// Need to invalidate all client access caches
			inval_access_all(0);
		}
	}

	if (latest_dirty_seqnum != 0) {
		//
		// The dirty attributes are obsolete now. Request the client
		// node to clear them.
		//
		clear_obsolete_attr(node_dirty);
	}

	//
	// After a switchover there must be clients.
	//
	// After a failover there is a case where there may be no clients.
	// If the only client was on the dead node and there were file locks,
	// then the code will recover state in order to replay all locks.
	//
	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    (clientset.clients.is_empty() ? PXFS_AMBER : PXFS_GREEN),
	    ("fobj+:recover_c_f_s(%p) clients %llx\n",
	    this, clientset.clients.bitmask()));
	//
	// Check for consistency.
	// If a client exists, must be on both the client list and the nodeset.
	// Cannot have a client with privileges when there are no clients.
	//
	ASSERT(!(!clientset.clients.is_empty() &&
	    clientset.client_list.empty()));
	ASSERT(!(clientset.clients.is_empty() &&
	    !clientset.client_list.empty()));
	ASSERT(!(clientset.clients.is_empty() &&
	    (!attr_read_token.is_empty() ||
	    !attr_write_token.is_empty() ||
	    !access_cached.is_empty())));
	fobjplus_priv_lock.unlock();
	attr_lock.unlock();
}

//
// clear_obsolete_attr - clear obsolete attr_dirty on a client.
//
void
fobjplus_ii::clear_obsolete_attr(ID_node node_dirty)
{
	Environment env;

	clientset.list_lock.rdlock();

	//
	// Avoid making an invocation to clear dirty attributes to the client
	// if it is not supported. This is supported from version 1
	//
	if (PXFS_VER::fobj_client::_version(
	    clientset.get_client(node_dirty)) < 1) {
		clientset.list_lock.unlock();
		return;
	}

	//
	// The client could go away when we drop the list_lock.
	// We use a smart pointer to ensure that the client is
	// valid while asking it to clear attr dirty flag.
	//
	PXFS_VER::fobj_client_var	client_v =
	    PXFS_VER::fobj_client::_duplicate(clientset.get_client(node_dirty));

	clientset.list_lock.unlock();

	// Ask client to clear dirty flag
	client_v->clear_attr_dirty(get_fsp()->get_server_incn(), env);

	//
	// The only possible exception is a node failure or version exception.
	//
	ASSERT(env.exception() == NULL ||
	    CORBA::COMM_FAILURE::_exnarrow(env.exception()) ||
	    CORBA::VERSION::_exnarrow(env.exception()));

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:recover_c_f_s(%p) attr_dirty cleared on client "
	    "node (%d).\n",
	    this, node_dirty.ndid));
}

//
// prepare_recover - initialize data structures prior to recovering
// information from clients.
//
void
fobjplus_ii::prepare_recover()
{
	ASSERT(attr_lock.write_held());
	ASSERT(fobjplus_priv_lock.lock_held());
	ASSERT(clientset.client_list.empty());
	attr_read_token.empty();
	attr_write_token.empty();
	access_cached.empty();
	attr_seqnum = 0;
	access_seqnum = 0;

	//
	// A clear_attr_dirty task that was created by an earlier incarnation
	// of the PxFS server can race with a task that was created by the
	// current incarnation and could incorrectly clear the flag,
	// wait_for_attr_clear. To prevent this from happening, we hold
	// wait_for_clear_lock while clearing this flag. The clear_attr_dirty
	// task also holds this lock and checks if the server incarnation that
	// created it is the current one before proceeding to clear the flag.
	//
	wait_for_clear_lock.lock();
	wait_for_attr_clear = false;
	wait_for_clear_lock.unlock();
}

//
// recovered_state - recover the state from one client
//
void
fobjplus_ii::recovered_state(PXFS_VER::recovery_info &recovery)
{
	ASSERT(attr_lock.write_held());
	ASSERT(fobjplus_priv_lock.lock_held());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:recovered_state(%p) node %d\n",
	    this, recovery.ndid));

	ID_node		client_node(recovery.ndid, recovery.incn);

	//
	// Register the client.
	// The method "insert" returns fobj_client object reference duplicated,
	// so we release the fobj_client object reference here, as we are
	// finished with the reference.
	//
	// No other activity is allowed on the file object until
	// after state recovery completes. So there should be no
	// lock contention at this point.
	//
	ASSERT(!CORBA::is_nil(recovery.client_p));
	CORBA::release(clientset.insert(client_node, recovery.client_p));

	set_attr_rights_locked(client_node.ndid, recovery.at_rights);

	if (attr_seqnum < recovery.attr_seqnum) {
		attr_seqnum = recovery.attr_seqnum;
	}
	if (access_seqnum < recovery.ac_seqnum) {
		access_seqnum = recovery.ac_seqnum;
	}
	if (recovery.ac_cached) {
		access_cached.add_node(client_node.ndid);
	}
}

//
// is_it_busy
//	Return values
//		1 == busy
//		0 == not busy
//		-1 == error
//
// Called from _FIOISBUSY ioctl. To be used only by the
// Legato Networker Product. This ioctl provides no guarantees.
// The file could be opened immediately after the ioctl completes.
//
// Note that this virtual function is overloaded for those
// file types that can support this operation.
//
int
fobjplus_ii::is_it_busy()
{
	Environment	e;
	bool		dead_clients = false;
	uint32_t	open_count = 0;

	clientset.list_lock.rdlock();

	client_set::client_entry	*entryp;

	client_set::iterator		iter(clientset.client_list);

	//
	// Iterate across all clients on the list.
	// We do not remove any clients while traversing the list.
	// However, some other thread may remove a client during this
	// list traversal. The current list entry will remain on the list,
	// though it could become a zombie. The next entry on the list
	// may be changed when the lock is not held.
	//
	for (;
	    NULL != (entryp = iter.get_current()) && open_count == 0;
	    iter.advance()) {
		if (entryp->is_zombie()) {
			//
			// Do not contact a zombie, which has no privileges.
			//
			dead_clients = true;
			continue;
		}

		if (entryp->is_dead()) {
			//
			// Do not contact the dead node.
			// Not holding attribute lock. Clean up later.
			//
			dead_clients = true;
			continue;
		}

		//
		// The client could go away when we drop the list_lock.
		// So we use a smart pointer to ensure that the client
		// remains in existence.
		//
		PXFS_VER::fobj_client_var	client_v =
		    PXFS_VER::fobj_client::_duplicate(
		    (PXFS_VER::fobj_client_ptr)(entryp->client_p));

		// Do not hold the clientset lock across an invocation
		entryp->hold();
		clientset.list_lock.unlock();

		client_v->get_open_count(open_count, e);

		clientset.list_lock.rdlock();
		entryp->release();

		if (e.exception() != NULL) {
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(e.exception()));
			e.clear();
			//
			// The client is gone.
			// Mark the client as dead.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJPLUS,
			    PXFS_GREEN,
			    ("fobj+:is_it_busy(%p) dead node %d\n",
			    this, entryp->node.ndid));

			// Not holding attribute lock. Clean up later.
			dead_clients = true;
		}
	}
	clientset.list_lock.unlock();

	if (dead_clients) {
		clientset.purge_dead_clients(this);
	}
	return ((int)open_count);
}

//
// fast_remove_test - returns true when
// either there are no proxy file clients.
// or the following conditions are all true:
//	1. Only one node has a proxy file client
//	2. The client is on the source node
//	3. The proxy file client is on the local node
//	4. The proxy vnode open count on the source node is <=0
//	   (i.e there is no process on source node is using the file)
//
bool
fobjplus_ii::fast_remove_test(ID_node &src_node)
{
	// Do not allow client set of the file to change
	clientset.list_lock.rdlock();

	nodeset		client_nodeset(clientset.client_nodeset());

	if (client_nodeset.is_empty()) {
		//
		// There are no clients
		//
		clientset.list_lock.unlock();
		return (true);
	}

	nodeset		file_clients;
	file_clients.add_node(orb_conf::local_nodeid());

	if (!client_nodeset.equal(file_clients)) {
		//
		// The client proxy file is not local
		//
		clientset.list_lock.unlock();
		return (false);
	}

	//
	// There is only one client proxy file object and it is local.
	//
	Environment		e;
	uint32_t		open_count;

	//
	// The client could go away when we drop the list_lock.
	// So we use a smart pointer to ensure that the client
	// remains in existence.
	//
	PXFS_VER::fobj_client_var client_v = PXFS_VER::fobj_client::_duplicate(
	    clientset.get_client(src_node));
	ASSERT(!CORBA::is_nil(client_v));

	// Do not hold this lock across an invocation
	clientset.list_lock.unlock();

	client_v->get_vnode_count(open_count, e);
	if (e.exception()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJPLUS,
		    PXFS_RED,
		    ("fast_remove_test(%p) get_open_count exception\n",
		    this));
		//
		// Since we cannot contact the client,
		// Do not use fast path.
		//
		e.clear();
		return (false);
	}
	ASSERT(open_count >= 0);

	//
	// The v_count returned by get_vnode_count() is actual number
	// of holds by process(es) since the entry is already purged from dnlc
	// as part of file removal operation.
	//
	return (open_count == 0);
}

//
// undo_register_client - the default routine for those file objects
// that support client side caching.
//
// virtual
void
fobjplus_ii::undo_register_client(Environment &env)
{

	//
	// Remove the client from the list and drop any privileges atomically.
	//
	clientset.remove(this, env.get_src_node());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJPLUS,
	    PXFS_GREEN,
	    ("fobj+:u_r_c(%p) node %d r %llx w %llx c %llx\n",
	    this, env.get_src_node().ndid,
	    attr_read_token.bitmask(), attr_write_token.bitmask(),
	    clientset.clients.bitmask()));
}
