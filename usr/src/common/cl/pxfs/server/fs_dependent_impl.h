//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FS_DEPENDENT_IMPL_H
#define	FS_DEPENDENT_IMPL_H

#pragma ident	"@(#)fs_dependent_impl.h	1.10	08/05/20 SMI"

#include <sys/fdbuffer.h>
#include <sys/lofi.h>
#include <sys/sunddi.h>

#include <h/sol.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/server/repl_pxfs_server.h>

// Forward declarations.
class fs_ii;
class fobj_ii;

//
// Definition of a fs-specific class that does any required special handling
// of fobj and fs calls.  Currently, the only use of this is in the
// fobj_ii::cascaded_ioctl() call.
//
class fs_dependent_impl {

public:

	// Constructor.
	fs_dependent_impl();

	// Virtual destructor.
	virtual ~fs_dependent_impl();

	//
	// Function called by the fs_impl object right after it creates a
	// new fobj object.  This function is meant to set fs-specific
	// parameters for the fobj - in UFS's case, it would set the 'cachedata'
	// flag based on whether the UFS mount was done with the "forcedirectio"
	// option turned on.
	//
	virtual void new_fobj(fobj_ii *fobjp);

	//
	// Called when the FS is mounted using new mount options (via the
	// MS_REMOUNT flag to mount).
	//
	virtual void set_mntopts(const char *);

	//
	// Function called by fobj_ii::cascaded_ioctl() to process fs-specific
	// ioctls.
	// Returns true if the ioctl was processed in this function, and false
	// if not.
	//
	virtual bool process_cascaded_ioctl(sol::nodeid_t nodeid,
	    fobj_ii *fobjp, int32_t iocmd, sol::intptr_t arg, int32_t flag,
	    cred_t *crp, int *result, int &status, Environment &env);

	//
	// Helper function called from repl_pxfs_server::ckpt_lockfs_state()
	// to checkpoint state of the UFS _FIOLFS ioctl call.
	//
	virtual void ckpt_lockfs_state(uint64_t lf_lock, uint64_t lf_flags,
	    uint64_t lf_key, const char *lf_comment);

	virtual void ckpt_vx_tunefs(const REPL_PXFS_VER::vx_tunefs_t &tunefs);

	//
	// Called from fs_ii::convert_to_primary() AFTER the previously
	// opened fobj's have been converted.
	//
	virtual int convert_to_primary(fs_ii *fsp);

	//
	// Called to alert the user of a potential deadlock situation
	// when trying to freeze the primary during an _FIOLFS ioctl.
	//
	virtual void freeze_primary(const char *fs_name);

	// Helper function for dumping state to a new secondary.
	virtual void dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
		Environment &env);

	virtual void replay_ioctl(fobj_ii *fobjp, int32_t iocmd,
	    sol::intptr_t arg, int32_t flag, int32_t &result, int &status);

	//
	// Static function that returns a fs-specific instance of
	// 'fs_dependent_impl', if required.  This function is called from
	// the 'fs_impl' constructor.
	//
	static fs_dependent_impl *get_fs_dependent_impl(fs_ii *fsp,
	    const char *fstype, const char *mntoptions);

	//
	// Helper function to perform ioctls with kernel arguments.
	//
	int kernel_ioctl(vfs_t *vfsp, int operation, intptr_t arg);

	//
	// This function frees the instance allocated by
	// get_fs_dependent_impl().
	//
	static void free_fs_dependent_impl(fs_dependent_impl *);

	//
	// Support for extended vnode operations.
	//
	virtual int fs_alloc_data(vnode_t *vp, u_offset_t offset, size_t *len,
	    fdbuffer_t *fdb, int flags, cred_t *cr);
	virtual int fs_rdwr_data(vnode_t *vp, u_offset_t offset, size_t len,
	    size_t client_file_size, fdbuffer_t *fdb, int flags, cred_t *cr);

	// Call VOP_FSYNC() if file system needs it.
	virtual int	fs_fsync(vnode_t *vp, cred_t *credp);

	// Sync with FSYNC option
	virtual int	do_fsync(vnode *vp, cred_t *credp);

	//
	// Calls fs_fsync if FS meta data not been synced since 'mod_time'.
	//
	virtual int	sync_if_necessary(os::hrtime_t &mod_time, vnode *vp,
	    cred_t *credp);
	//
	// To support mounting UFS without syncdir.
	//
	virtual int fs_preprocess(vnode_t *vp, u_offset_t offset, size_t *len,
	    fdbuffer_t *fdb, int flags, cred_t *cr);

	// Check if this file system is mounted on a lofi device.
	bool	device_is_lofi(struct vfs *vfsp);
};

#endif	// FS_DEPENDENT_IMPL_H
