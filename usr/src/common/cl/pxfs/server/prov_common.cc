//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)prov_common.cc	1.6	08/05/20 SMI"

//
// The prov_common class is intended to be used as a base class for
// all PXFS cache provider implementations. The basic functions are
// keeping a list of the providers and storing a handle for getting
// a reference to the pxfscacher from the name server which can
// be used to create new cache objects.
//

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/prov_common.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// finial pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

prov_common::prov_common(CORBA::Object_ptr obj)
{
	nextp = NULL;
	cache_obj = CORBA::Object::_duplicate(obj);
}

prov_common::~prov_common()
{
	//
	// This will catch users who call delete without first removing the
	// provider from the list.
	//
	ASSERT(nextp == NULL);

	// cache_obj release is done by CORBA::Object_var destructor.
}

prov_common_set::prov_common_set()
{
	firstp = NULL;
	iter_list = NULL;
}

prov_common_set::~prov_common_set()
{
	ASSERT(firstp == NULL);
	ASSERT(iter_list == NULL);
}

//
// If there is an existing entry, return a pointer to it and don't
// insert 'provp'.
// Otherwise, insert 'provp' at the head of the list and return NULL.
// The lock should be held on entry.
//
prov_common *
prov_common_set::insert_locked(prov_common *provp)
{
	prov_common *p;

	ASSERT(list_lock.lock_held());

	//
	// Search the list for a duplicate entry.
	//
	for (p = firstp; p != NULL; p = p->nextp) {
		if (p->cache_obj->_equiv(provp->cache_obj)) {
			return (p);
		}
	}

	//
	// Since we are inserting at the head of the list,
	// this doesn't affect any active iterators.
	//
	ASSERT(provp->nextp == NULL);
	provp->nextp = firstp;
	firstp = provp;

	return (NULL);
}

//
// Remove the provider from the list of providers.
// The lock should be held on entry.
//
void
prov_common_set::remove_locked(prov_common *provp)
{
	prov_common *p, **prevpp;
	prov_common_iter *iterp;

	ASSERT(list_lock.lock_held());

	//
	// Search the list of active iterators and fix the "next"
	// provider to be returned if it is being deleted.
	//
	for (iterp = iter_list; iterp != NULL; iterp = iterp->next_iterp) {
		if (iterp->next_provp == provp) {
			iterp->next_provp = provp->nextp;
		}
	}

	// Remove provider from the list.
	for (prevpp = &firstp; (p = *prevpp) != NULL; prevpp = &p->nextp) {
		if (p == provp) {
			//
			// We found it.
			//
			*prevpp = p->nextp;
			p->nextp = NULL;
			return;
		}
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_PROVCOMMON,
	    PXFS_GREEN,
	    ("prov_common_set::remove_locked %p not found\n", provp));
	ASSERT(0);
}

//
// Remove the first provider in the list and return it or return NULL.
//
prov_common *
prov_common_set::reapfirst()
{
	list_lock.wrlock();
	if (firstp == NULL) {
		list_lock.unlock();
		return (NULL);
	}

	// Remove first element from list.
	prov_common *provp = firstp;
	firstp = provp->nextp;
	provp->nextp = NULL;

	//
	// Search the list of active iterators and fix the "next"
	// provider to be returned if it is being deleted.
	//
	prov_common_iter *iterp;
	for (iterp = iter_list; iterp != NULL; iterp = iterp->next_iterp) {
		if (iterp->next_provp == provp) {
			iterp->next_provp = firstp;
		}
	}
	list_lock.unlock();

	return (provp);
}

//
// Return true if the list is empty.
// This should only be called with the provider list locked.
//
bool
prov_common_set::is_empty_locked()
{
	ASSERT(list_lock.lock_held());
	return (firstp == NULL);
}

prov_common_iter::prov_common_iter(prov_common_set &_pset) :
	pset(_pset)
{
	//
	// Insert ourself into the list of active iterators and init "next".
	//
	pset.list_lock.wrlock();
	next_provp = pset.firstp;
	next_iterp = pset.iter_list;
	pset.iter_list = this;
	pset.list_lock.unlock();
}

prov_common_iter::~prov_common_iter()
{
	prov_common_iter *p, **prevpp;

	// Remove ourself from the list of iterators.
	pset.list_lock.wrlock();
	for (prevpp = &pset.iter_list; (p = *prevpp) != NULL;
	    prevpp = &p->next_iterp) {
		if (p == this) {
			*prevpp = p->next_iterp;
			pset.list_lock.unlock();
			return;
		}
	}
	pset.list_lock.unlock();
	ASSERT(0);
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

//
// Return a pointer to the next item in the list or NULL if at the end
// and advance to the next item after that.
//
prov_common *
prov_common_iter::nextp()
{
	pset.list_lock.wrlock();
	prov_common *rv = next_provp;
	if (rv != NULL) {
		next_provp = rv->nextp;
	}
	pset.list_lock.unlock();
	return (rv);
}

//
// Return a new CORBA reference to the next item in the list or
// nil if at the end and advance to the next item after that.
//
CORBA::Object_ptr
prov_common_iter::nextref()
{
	CORBA::Object_ptr obj_p = CORBA::Object::_nil();

	pset.list_lock.wrlock();
	prov_common *rv = next_provp;
	if (rv != NULL) {
		obj_p = rv->get_provref();
		next_provp = rv->nextp;
	}
	pset.list_lock.unlock();

	return (obj_p);
}
