//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FS_IMPL_H
#define	FS_IMPL_H

#pragma ident	"@(#)fs_impl.h	1.22	09/03/03 SMI"

#include <sys/vfs.h>
#include <sys/dirent.h>

#include <sys/os.h>
#include <orb/object/adapter.h>
#include <repl/service/replica_tmpl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include PXFS_IDL(pxfs)
#include PXFS_IDL(repl_pxfs)
#include <pxfs/server/prov_common.h>
#include <pxfs/server/fs_dependent_impl.h>
#include <pxfs/server/fs_base_impl.h>

//
// Definitions for indexing the kstat array of name-value pairs :
// The tail end of the array contains the entries for deleted_dir stats,
// which may or may not exist depending on flag settings. By default, the
// flag settings are turned off.
// Future addition of name-value pairs will go before
// FS_STATS_DEL_DIR_START.
//
#define	FS_STATS_FOBJ_FILE		0
#define	FS_STATS_FOBJ_UNIXDIR		FS_STATS_FOBJ_FILE + 1
#define	FS_STATS_FOBJ_SYMLINK		FS_STATS_FOBJ_UNIXDIR + 1
#define	FS_STATS_FOBJ_PROCFILE		FS_STATS_FOBJ_SYMLINK + 1
#define	FS_STATS_MAX_NUM		FS_STATS_FOBJ_PROCFILE + 1

// Forward declarations.
class fobj_ii;
class fsmgr_server_ii;
class repl_pxfs_server;

//
// Class used to store client fsmgr object pointer. This class is used
// only by fs_recovery_task.
//
class fsmgr_client_elem : public _SList::ListElem {
public:
	fsmgr_client_elem(PXFS_VER::fsmgr_client_var fsmgrclient_v);

	~fsmgr_client_elem();

	//
	// Public data members.
	//
	PXFS_VER::fsmgr_client_var	fsmgr_client_v;

private:
	// Disallowed operations.
	fsmgr_client_elem & operator = (const fsmgr_client_elem &);
	fsmgr_client_elem(const fsmgr_client_elem &);
};

//
// This task supports a flush of data to storage during recovery.
//
class fs_recovery_task : public defer_task {
public:
	fs_recovery_task(fs_ii *fs_iip, uint32_t server_incn);

	void execute();

private:
	fs_ii				*fs_iip;
	PXFS_VER::filesystem_var	fs_v;
	uint32_t			server_incarn;

	//
	// Singly list which stores all the fs manager client objects of
	// the filesystem for which this recovery task was created for.
	//
	IntrList<fsmgr_client_elem, _SList>	fsmgr_client_list;
};


//
// This is the base class for implementing both replicated and non-replicated
// file systems.  The code is shared to reduce duplication and maintenance.
//
class fs_ii : public fs_base_ii {
public:
	// Type for fobj factory functions.
	typedef fobj_ii * (*fobj_ii_factory_t)(fs_ii *fsp, vnode_t *vp,
	    fid_t *fidp);

	// Constructor is "protected".

	virtual ~fs_ii();
	void unreferenced();

	virtual base_type_t	get_base_type();

	// Return a new fs CORBA reference to this.
	virtual PXFS_VER::filesystem_ptr	get_fsref() = 0;

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt() = 0;

	// Helper function to access the syslog message object.
	os::sc_syslog_msg			&msg();

	//
	// Called from fsmgr::unreferenced() when a client node crashes, and
	// all locks originating from there have to be cleaned up.
	//
	void	cleanup_locks(nodeid_t nodeid);

	void	set_nlm_status(int32_t nlmid, PXFS_VER::nlm_status status,
	    Environment &_environment);

	virtual void	get_reservation(PXFS_VER::blkcnt_t &refill_count,
	    PXFS_VER::server_status_t &status,
	    Environment &_environment);

	void	remove_file_locks(int32_t sysid,
	    Environment &_environment);

	//
	// Return the underlying file system's vfs pointer.
	// This can return NULL if this object is a secondary
	// or the underlying file system has been unmounted,
	// such as by forced unmount.
	//
	vfs_t	*get_vfsp() const;

	// Set the underlying file system's vfs pointer.
	void	set_vfsp(vfs_t *vfsp);

	// Get the 'fs_dependent_impl' object associated with this fs.
	fs_dependent_impl	*get_fs_dep_implp() const;

	// Accessor function to avoid use of friend.
	prov_set<fsmgr_server_ii>	&get_fsmgr_set();

	// Function to convert CORBA filesystem_ptr to fs_ii*.
	static fs_ii	*get_fs_ii(PXFS_VER::filesystem_ptr fs_obj);

	//
	// Function used by _impl constructor to initialize the value returned
	// by get_fs_ii().
	//
	void	set_fs_ii(PXFS_VER::filesystem_ptr fs_obj);

	// Return true if this file system is replicated.
	bool	is_replicated() const;

	uint32_t	get_server_incn();

	uint32_t	get_number_joins();

	// Return true if operating in recovery mode
	bool	get_recovery_mode();

	// Set recovery mode
	void	set_recovery_mode(bool mode);

	static void	fs_recovery(void *argp);

	//
	// Return true if this is a dead replicated file system
	// (i.e., there was a fatal error in convert_to_primary() or
	// convert_to_secondary but the replica manager still thinks
	// the service is alive).
	// If true, the environment is also set to return an exception.
	//
	bool	is_dead(Environment &env);

	// Return true if the primary is being prepared to be frozen.
	bool	freeze_prepare_in_progress(Environment &env);

	PXFS_VER::server_status_t switch_to_fastpath(Environment &_environment);

	void ckpt_dump_fobj_list(
	    const REPL_PXFS_VER::fobj_ckpt_seq_t &fobj_list);

	void ckpt_new_fobj(PXFS_VER::fobj_ptr fob,
	    const PXFS_VER::fobjid_t &fid,
	    PXFS_VER::fobj_type_t type);

	// Helper function called from repl_pxfs_server::ckpt_new_fsmgr().
	void	ckpt_new_fsmgr(PXFS_VER::fsmgr_server_ptr servermgr,
	    PXFS_VER::fsmgr_client_ptr clientmgr, sol::nodeid_t nodeid);

	void	ckpt_remove_file_locks_by_sysid(int32_t sysid);
	void	ckpt_remove_file_locks_by_nlmid(int32_t nlmid);

	bool fastwrite_enabled();
	void ckpt_blocks_allocated(const repl_pxfs_v1::blocks_allocated_t tmp,
	    PXFS_VER::blkcnt_t blocks_free_cnt);
	void ckpt_server_status(PXFS_VER::server_status_t status);

	void dump_multiple_fobjs(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    REPL_PXFS_VER::fobj_ckpt_seq_t *seq, int count,
	    Environment &env);

	void dump_hash_list_fobjs(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    Environment &env);

	void dump_fobjs_serially(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    Environment &env);

	void dump_fid_list_fobjs(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    Environment &env);

	void dump_fid_list_serially(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    Environment &env);

	// Helper function for dumping state to a new secondary.
	virtual void	dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
	    Environment &env);

	// Helper function for converting from secondary to primary.
	int	convert_to_primary(bool firsttime);

	// Helper function for converting from primary to secondary.
	int	convert_to_secondary();

	// Helper function for converting from secondary to spare.
	void	convert_to_spare();

	// Accessor function to return the replica server pointer.
	repl_pxfs_server	*get_serverp() const;

	// Accessor function to return the deleted directory vnode pointer.
	vnode_t		*get_deleted() const;

	// Accessor function to return/set the next deleted file number.
	uint64_t	get_deletecnt();
	void		set_deletecnt(uint64_t cnt);

	// Accessor function to return the mount option string.
	const char	*get_options() const;
	void		set_options(const char *mntopts);

	//
	// Find a fobj object corresponding to a given vnode and return
	// a new CORBA reference to it.
	// If the fobj object is not found, a new implementation object
	// is created based on the factory function.
	//
	PXFS_VER::fobj_ptr	find_fobj(vnode_t *vp,
	    PXFS_VER::fobj_info &fobjinfo,
	    Environment &env);

	//
	// Lock for all rename operations so we don't have deadlocks
	// trying to get all the provider's locks on the directories.
	//
	void	lock_renames();
	void	unlock_renames();

	// Create a new fsmgr_server.
	PXFS_VER::fsmgr_server_ptr	fsmgr_factory(
	    PXFS_VER::fsmgr_client_ptr clientmgr,
	    nodeid_t nodeid, Environment &env);

	// Sync this file system on each pxfs client node.
	void	sync_fs(cred_t *crp);

	// Add fobj to the list of vnodes to be released and/or deleted.
	static void	reap_vnode(fobj_ii *fobjp);

	static int	startup();

	// Increment the count for the specified fobj type
	void	kstat_fobj_type_increment(PXFS_VER::fobj_type_t ftype);

	// Decrement the count for the specified fobj type
	void	kstat_fobj_type_decrement(PXFS_VER::fobj_type_t ftype);

	// helper functions to track pending aios on this file system.
	void	increment_aio_pending();
	void	decrement_aio_pending();
	void	wait_for_aio_callbacks();

	// helper functions to track revoke tasks on this file system.
	void	increment_revoke();
	int64_t decrement_revoke();

	//
	// Helper function to track outstanding invocations on this
	// file system.
	//
	void decrement_invo_count();

	// Check if this file system is mounted on a lofi device.
	bool	device_is_lofi();

#ifndef VXFS_DISABLED
	void vxfs_set_fastwrite_flag();
#endif

protected:
	//
	// Constructor for the internal implementation;
	// use when using mount system call (e.g. mount -g) to
	// establish global mounts.
	//
	fs_ii(vfs_t *vfsp, const char *fstype, fobj_ii_factory_t factory,
	    const char *spec, const char *options, repl_pxfs_server *serverp);

	//
	// These factory functions are used to create an fobj.
	// "fobj_ii_{norm,repl}_factory" should not be called directly.
	// The factory is passed to the filesystem on creation and is used
	// by find_fobj(), which is the high-level function used to create a
	// new implementation object for the underlying file system object
	// represented by the vnode "vp".
	// Note that fobj_ii_repl_sec_factory is not interchangeable with
	// the other factories; it is used to create the secondary-side replica
	// of a replicated fobj and is called by ckpt_new_fobj.  It takes a
	// fid instead of a vnode.
	//
	static fobj_ii *fobj_ii_norm_factory(fs_ii *fsp, vnode_t *vp,
	    fid_t *fidp);
	static fobj_ii *fobj_ii_repl_factory(fs_ii *fsp, vnode_t *vp,
	    fid_t *fidp);

	// PXFS_VER::filesystem IDL operations

	void	getroot(PXFS_VER::fobj_out rootobj,
	    PXFS_VER::fobj_info &rootinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	void	get_statistics(sol::statvfs64_t &stat,
	    Environment &_environment);

	void	get_mntinfo(PXFS_VER::fs_info &info,
	    CORBA::String_out mntoptions,
	    Environment &_environment);

	void	sync(int32_t syncflag, Environment &_environment);

	void	sync_recovery(uint32_t server_incarn,
	    Environment &_environment);

	void	getfobj(const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	PXFS_VER::fsmgr_server_ptr	bind_fs(
	    PXFS_VER::fsmgr_client_ptr clientmgr,
	    sol::nodeid_t nodeid, uint32_t &server_incn,
	    uint32_t &fsbsize, bool &fastwrite_flag,
	    Environment &_environment);

	void	remount(PXFS_VER::fobj_ptr mntpnt, const sol::mounta &ma,
	    solobj::cred_ptr credobj, uint32_t &vfsflags,
	    CORBA::String_out mntoptions, Environment &_environment);

	void	unmount(int32_t flags, solobj::cred_ptr credobj,
	    Environment &_environment);
	//

private:
	// Create the deleted directory.
	int	make_deleted(bool firsttime);

	// Remove files from the deleted directory.
	int	rmfiles(vnode_t *vp);

	// Unmount the underlying file system.
	void	forced_unmount();

	//
	// Helper function called by convert_to_secondary to release all locks
	// on the underlying LLM.
	//
	void	release_all_locks();

	// Helper function to reconstruct block allocation table.
	void reconstruct_blocks_allocated();

	void launch_revoke_tasks();

	//
	// Tell all pxfs file system clients about a new file system primary.
	//
	void	new_primary_notification();

	//
	// Helper function to clean up the LLM of locks held by any stale
	// fsmgr_server objects that have not got an '_unref' notification yet.
	//
	void	process_stale_fsmgrs(nodeid_t nodeid);

	void	remove_file_locks_locked_by_sysid(int32_t sysid);
	void	remove_file_locks_locked_by_nlmid(int32_t nlmid);

	// Disallow assignments and pass by value.
	fs_ii(const fs_ii &);
	fs_ii &operator = (fs_ii &);

public:
	// Name of the deleted directory in the root directory.
	static const char	deleted_dir[];

	int64_t		revoke_in_progress;

	//
	// This lock protects the following members :
	// blocks_allocated[], blocks_free, blocks_free_high_w_mark,
	// blocks_free_low_w_mark, fs_status and revoke_in_progress.
	//
	os::mutex_t	block_allocation;

private:
	// Factory function to create new fobjs from vnodes.
	fobj_ii_factory_t	rootfactory;

	//
	// Underlying wrapped file system.
	// vfs == NULL indicates that this is a secondary or a primary
	// that had an error trying to mount the underlying file system.
	// Note that we rely on the HA/ORB framework for locking when
	// changing this pointer.
	//
	vfs_t		*vfs;

	// Lock used to serialize unmount invocations.
	os::rwlock_t	unmount_lock;

	// Cached fobj for the root vnode.
	PXFS_VER::fobj_var	rootfobj;

	// The fs-specific part of 'fs_ii'.
	fs_dependent_impl	*fs_dep_implp;

	// Pointer to the HA service implementation or NULL if not replicated.
	repl_pxfs_server	*pxfs_serverp;

	//
	// Mount option string returned from mount of underlying file system
	// if this is an HA mount.
	//
	CORBA::String_var	mnt_options;

	// Set of all fsmgr_servers.
	prov_set<fsmgr_server_ii>	servermgr_set;

	//
	// We move deleted replicated files to the deleted directory so they
	// will still be in the file system.  This is to handle unlinked, open
	// files which need to still exist after we switch to the secondary.
	// "deletecnt" holds the sequential count of deleted files.
	//
	vnode_t		*deleted;
	uint64_t	deletecnt;

public:
	void revoke_completed(uint32_t server_incn, Environment &_environment);
	void revoke_completed_main();
	void initialize_blocks(bool reset_blocks_free);

private:
	//
	// Data structures for file space reservation
	//
	bool			fastwrite;
	uint32_t		fs_fs_bsize; // Filesystem block size
	PXFS_VER::blkcnt_t	blocks_free; // Free blocks in filesystem
	PXFS_VER::blkcnt_t	blocks_allocated[NODEID_MAX+1];	// Client
								// reservations
	PXFS_VER::blkcnt_t	blocks_free_high_w_mark;
	PXFS_VER::blkcnt_t	blocks_free_low_w_mark;
	PXFS_VER::server_status_t	fs_status;

	//
	// When a file system transitions to REDONE or when a failover
	// has happened, all clients have to sync their dirty pages.
	// This is so the server can calculate exact free space
	// available in the filesystem. The request to clients is
	// issued via a revoke task thread launched for each client.
	// Only when the last client finishes it sync, should the
	// server proceed with free space calculation.
	//
	// We have to count the number of active revoke tasks to know
	// when they have finished executing. These members below help
	// to keep track of outstanding revoke tasks.
	//
	os::mutex_t	revoke_lock;

private:
	//
	// Lock for all rename operations so we don't have deadlocks
	// trying to get all the provider's locks on the directories.
	//
	os::mutex_t	rename_lock;

	//
	// File system server incarnation number.
	// The system sets this value upon becoming primary.
	//
	uint32_t	server_incn;

	//
	//
	//
	uint32_t	number_joins;

	// Syslog message object for logging messages.
	os::sc_syslog_msg	sysmsg;

	// Lock to serialize kstat creation and deletion
	static os::mutex_t	pxfs_kstat_create_delete;

	// Kstat structure
	kstat_t			*stats;

	//
	// Global mount sequence number:
	// This is used to name a kstat uniquely, for a specific
	// mount of a given filesystem. This number is prefixed to
	// the device name, to produce the unique name.
	//
	static int		mount_seq;

	//
	// A new file system primary begins operation in recovery mode
	// until information from clients has been flushed to the server.
	// Space allocations done by the previous server may have been lost.
	//
	bool			recovery_mode;

	//
	// When we do a switchover the underlying file system is force
	// unmounted. If a client had asked for AIO writes and those are
	// pending, then they will never complete after the unmount. The
	// pages associated with the AIOs will remain i/o locked on the
	// client forever.
	//
	// The members below keep track of pending aios. Before doing a
	// switchover we ensure that the aio call back count is zero.
	//
	os::mutex_t	aio_cbk_lock;
	os::condvar_t	aio_cbk_cv;
	int64_t		aio_cbk_count;
};

//
// Unreplicated CORBA implementation of PXFS_VER::filesystem
//
class fs_norm_impl :
	public fs_ii,
	public McServerof<PXFS_VER::filesystem>
{
public:
	//
	// Constructor used when using mount system call (e.g. mount -g) to
	// establish global mounts.
	//
	fs_norm_impl(vfs_t *vfsp, const char *fstype, const char *spec,
	const char *options);

	void _unreferenced(unref_t arg);

	// Return a new PXFS_VER::fs CORBA reference to this.
	PXFS_VER::filesystem_ptr get_fsref();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	REPL_PXFS_VER::fs_replica_ptr get_ckpt();

	// This is called  to get a new reference
	virtual void	_generic_method(CORBA::octet_seq_t &,
	    CORBA::object_seq_t &, Environment &);

	// PXFS_VER::filesystem IDL operations

	virtual void	getroot(PXFS_VER::fobj_out rootobj,
	    PXFS_VER::fobj_info &rootinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	get_statistics(sol::statvfs64_t &stat,
	    Environment &_environment);

	virtual void	get_mntinfo(PXFS_VER::fs_info &info,
	    CORBA::String_out mntoptions, Environment &_environment);

	virtual void	sync(int32_t syncflag, Environment &_environment);

	virtual void	sync_recovery(uint32_t server_incarn,
	    Environment &_environment);

	virtual void revoke_completed(uint32_t server_incn,
	    Environment &_environment);

	virtual void	getfobj(const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual PXFS_VER::fsmgr_server_ptr	bind_fs(
	    PXFS_VER::fsmgr_client_ptr clientmgr,
	    sol::nodeid_t nodeid, uint32_t &server_incn,
	    uint32_t &fsbsize, bool &fastwrite_flag,
	    Environment &_environment);

	virtual void	remount(PXFS_VER::fobj_ptr mntpnt,
	    const sol::mounta &ma,
	    solobj::cred_ptr credobj, uint32_t &vfsflags,
	    CORBA::String_out mntoptions, Environment &_environment);

	virtual void	unmount(int32_t flags, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	set_nlm_status(int32_t nlmid,
	    PXFS_VER::nlm_status status,
	    Environment &_environment);

	virtual void	get_reservation(PXFS_VER::blkcnt_t &refill_count,
	    PXFS_VER::server_status_t &status,
	    Environment &_environment);

	virtual void	remove_file_locks(int32_t sysid,
	    Environment &_environment);

	// end of methods supporting IDL operations

	// Disallow assignments and pass by value.
	fs_norm_impl(const fs_norm_impl &);
	fs_norm_impl &operator = (fs_norm_impl &);
};

//
// Replicated CORBA implementation of PXFS_VER::filesystem
//
class fs_repl_impl :
	public mc_replica_of<PXFS_VER::filesystem>,
	public fs_ii
{
public:
	//
	// Primary constructor; used when using the mount system call
	// (e.g., mount -g) to establish global mounts.
	//
	fs_repl_impl(vfs_t *vfsp, const char *fstype, const char *spec,
	    const char *options, repl_pxfs_server *serverp);

	// Secondary constructor.
	fs_repl_impl(const char *fstype, const char *spec, const char *options,
	    repl_pxfs_server *serverp, PXFS_VER::filesystem_ptr obj);

	void _unreferenced(unref_t arg);

	// Return a new CORBA reference to this.
	PXFS_VER::filesystem_ptr	get_fsref();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	//
	// Ultimately calls ufs_dependent_impl::freeze_primary
	//
	void		fs_repl_impl::freeze_primary(const char *fs_name);

	// This is called  to get a new reference
	virtual void	_generic_method(CORBA::octet_seq_t &,
	    CORBA::object_seq_t &, Environment &);

	/* filesystem */

	virtual void	getroot(PXFS_VER::fobj_out rootobj,
	    PXFS_VER::fobj_info &rootinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	get_statistics(sol::statvfs64_t &stat,
	    Environment &_environment);

	virtual void	get_mntinfo(PXFS_VER::fs_info &info,
	    CORBA::String_out mntoptions, Environment &_environment);

	virtual void	sync(int32_t syncflag, Environment &_environment);

	virtual void	sync_recovery(uint32_t server_incarn,
	    Environment &_environment);

	virtual void revoke_completed(uint32_t server_incn,
	    Environment &_environment);

	virtual void	getfobj(const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_out fobj,
	    PXFS_VER::fobj_info &fobjinfo,
	    PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual PXFS_VER::fsmgr_server_ptr	bind_fs(
	    PXFS_VER::fsmgr_client_ptr clientmgr,
	    sol::nodeid_t nodeid, uint32_t &server_incn,
	    uint32_t &fsbsize, bool &fastwrite_flag,
	    Environment &_environment);

	virtual void	remount(PXFS_VER::fobj_ptr mntpnt,
	    const sol::mounta &ma,
	    solobj::cred_ptr credobj, uint32_t &vfsflags,
	    CORBA::String_out mntoptions, Environment &_environment);

	virtual void	unmount(int32_t flags, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	set_nlm_status(int32_t nlmid,
	    PXFS_VER::nlm_status status,
	    Environment &_environment);

	virtual void	get_reservation(PXFS_VER::blkcnt_t &refill_count,
	    PXFS_VER::server_status_t &status,
	    Environment &_environment);

	virtual void	remove_file_locks(int32_t sysid,
	    Environment &_environment);

	// end of methods supporting IDL operations

	// Disallow assignments and pass by value.
	fs_repl_impl(const fs_repl_impl &);
	fs_repl_impl &operator = (fs_repl_impl &);

	// Checkpoint accessor function
	REPL_PXFS_VER::fs_replica_ptr	get_checkpoint();
};

#include <pxfs/server/fs_impl_in.h>

#ifndef VXFS_DISABLED
#include <pxfs/server/fs_impl_vxfs_in.h>
#endif

#endif	// FS_IMPL_H
