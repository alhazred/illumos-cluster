//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fs_impl_in.h	1.16	08/05/20 SMI"

inline
fsmgr_client_elem::fsmgr_client_elem(PXFS_VER::fsmgr_client_var fsmgrclient_v) :
	_SList::ListElem(this),
	fsmgr_client_v(fsmgrclient_v)
{
}

inline
fsmgr_client_elem::~fsmgr_client_elem()
{
}

inline bool
fs_ii::is_replicated() const
{
	return (pxfs_serverp != NULL ? true : false);
}

//
// Return the replica server pointer.
//
inline repl_pxfs_server *
fs_ii::get_serverp() const
{
	return (pxfs_serverp);
}

inline uint32_t
fs_ii::get_server_incn()
{
	return (server_incn);
}

inline uint32_t
fs_ii::get_number_joins()
{
	return (number_joins);
}

//
// Return true if operating in recovery mode
//
inline bool
fs_ii::get_recovery_mode()
{
	return (recovery_mode);
}

//
// Set recovery mode
//
inline void
fs_ii::set_recovery_mode(bool mode)
{
	recovery_mode = mode;
}

//
// Return the deleted directory vnode pointer.
//
inline vnode_t *
fs_ii::get_deleted() const
{
	return (deleted);
}

//
// Return the next deleted file number.
//
inline uint64_t
fs_ii::get_deletecnt()
{
	return (os::atomic_add_64_nv(&deletecnt, 1));
}

//
// Set the next deleted file number.
//
inline void
fs_ii::set_deletecnt(uint64_t cnt)
{
	//
	// Note that unixdir_ii::remove_fobj_common() and
	// unixdir_ii::remove_dir_common() checkpoint the delete count
	// for the file but the order between these two checkpoints isn't
	// guaranteed so we just make sure only the highest value is saved.
	//
	// Note we rely on uint64_t being large enough that it will never wrap.
	//
	if (deletecnt < cnt)
		deletecnt = cnt;
}

//
// Return the underlying file system's vfs pointer.
// This can return NULL if this object is a secondary
// or the underlying file system has been unmounted,
// such as by forced unmount.
//
inline vfs_t *
fs_ii::get_vfsp() const
{
	return (vfs);
}

//
// Get the 'fs_dependent_impl' object associated with this fs.
//
inline fs_dependent_impl *
fs_ii::get_fs_dep_implp() const
{
	return (fs_dep_implp);
}

//
// Return the mount option string.
//
inline const char *
fs_ii::get_options() const
{
	return (mnt_options);
}

//
// Set the mount option string.
//
inline void
fs_ii::set_options(const char *new_opts)
{
	mnt_options = os::strdup(new_opts);
	fs_dep_implp->set_mntopts(mnt_options);
}

inline prov_set<fsmgr_server_ii>&
fs_ii::get_fsmgr_set()
{
	return (servermgr_set);
}

// Locking functions for rename operations.
inline void
fs_ii::lock_renames()
{
	rename_lock.lock();
}

inline void
fs_ii::unlock_renames()
{
	rename_lock.unlock();
}

inline os::sc_syslog_msg &
fs_ii::msg()
{
	return (sysmsg);
}

inline bool
fs_ii::fastwrite_enabled()
{
	return (fastwrite);
}

inline void
fs_ii::ckpt_server_status(PXFS_VER::server_status_t status)
{
	fs_status = status;
	//
	// If we are switching to REDZONE reset all the allocations
	// that were made to the clients.
	//
	if (fs_status == PXFS_VER::SWITCH_TO_REDZONE) {
		bzero(blocks_allocated,
		    sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));
	}
}

//
// Wait for pending AIOs on this file system to complete.
//
inline void
fs_ii::wait_for_aio_callbacks()
{
	aio_cbk_lock.lock();
	while (aio_cbk_count > 0) {
		//
		// Wait for pending aio_callbacks to signal
		//
		aio_cbk_cv.wait(&aio_cbk_lock);
	}
	aio_cbk_lock.unlock();
}

// increment the aio counter by one
inline void
fs_ii::increment_aio_pending()
{
	aio_cbk_lock.lock();

	ASSERT(aio_cbk_count >= 0);
	ASSERT(aio_cbk_count != INT_MAX);

	aio_cbk_count++;
	aio_cbk_lock.unlock();
}

// decrement aio counter by one and signal if no AIOs are pending
inline void
fs_ii::decrement_aio_pending()
{
	aio_cbk_lock.lock();

	ASSERT(aio_cbk_count > 0);

	if (--aio_cbk_count == 0) {
		aio_cbk_cv.signal();
	}
	aio_cbk_lock.unlock();
}

// increment revoke task count for this file system by one
inline void
fs_ii::increment_revoke()
{
	revoke_lock.lock();

	ASSERT(revoke_in_progress >= 0);
	ASSERT(revoke_in_progress != INT_MAX);

	revoke_in_progress++;
	revoke_lock.unlock();
}

//
// Decrement revoke task count by one and return the current count.
// Note that we return the count after releasing the lock. There could
// be more revoke tasks launched meanwhile. Those tasks cannot have
// been launched by the same invocation as that launched this thread.
// That is because we hold the 'block_allocation' lock while launching
// revoke tasks. Meanwhile clients are blocked on 'block_reservation'
// lock. Thus it is safe to return the revoke task count without
// holding revoke_lock.
//
inline int64_t
fs_ii::decrement_revoke()
{
	int64_t revoke_count_now = 0;

	revoke_lock.lock();

	ASSERT(revoke_in_progress > 0);

	revoke_count_now = --revoke_in_progress;
	revoke_lock.unlock();

	// return the count we had while holding revoke_lock.
	return (revoke_count_now);
}

//
// If we are a replicated service ask the server to decrement outstanding
// invocation count.
//
inline void
fs_ii::decrement_invo_count()
{
	if (pxfs_serverp != NULL) {
		pxfs_serverp->decrement_invo_count();
	}
}

//
// Returns "true" if this file system is mounted on a lofi device
// and false otherwise.
//
inline bool
fs_ii::device_is_lofi()
{
	return (get_fs_dep_implp()->device_is_lofi(get_vfsp()));
}

// Return a new fs CORBA reference to this
inline PXFS_VER::filesystem_ptr
fs_norm_impl::get_fsref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
inline REPL_PXFS_VER::fs_replica_ptr
fs_norm_impl::get_ckpt()
{
	ASSERT(0);
	return (REPL_PXFS_VER::fs_replica::_nil());
}

// Return a new fs CORBA reference to this
inline PXFS_VER::filesystem_ptr
fs_repl_impl::get_fsref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
inline REPL_PXFS_VER::fs_replica_ptr
fs_repl_impl::get_ckpt()
{
	return (get_checkpoint());
}

inline REPL_PXFS_VER::fs_replica_ptr
fs_repl_impl::get_checkpoint()
{
	return ((repl_pxfs_server*)(get_provider()))->
	    get_checkpoint_fs_replica();
}

inline void
fs_repl_impl::freeze_primary(const char *fs_name)
{
	fs_ii::get_fs_dep_implp()->freeze_primary(fs_name);
}
