//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// symlink_impl.h - Implementation class for supporting symbolic links.
//

#ifndef SYMLINK_IMPL_H
#define	SYMLINK_IMPL_H

#pragma ident	"@(#)symlink_impl.h	1.12	08/05/20 SMI"

#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/server/fobjplus_impl.h>

//
// Declaration of internal implementation object for a symbolic link.
//
class symlink_ii : public fobjplus_ii {
public:
	virtual ~symlink_ii();

protected:
	// Unreplicated or primary constructor for the internal implementation.
	symlink_ii(fs_ii *fsp, vnode_t *vp, PXFS_VER::fobj_type_t ftype,
	    fid_t *fidp);

	// Secondary constructor for the internal implementation
	symlink_ii(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
	    PXFS_VER::fobj_type_t ftype);

	// symbolic_link::=
	virtual void	readlink(PXFS_VER::symbolic_link::target_t_out target,
	    solobj::cred_ptr credobj, Environment &_environment);
	//
};

//
// Declaration of IDL interface object.
//
//
// Unreplicated CORBA implementation of symbolic_link.
//
class symlink_norm_impl :
	public McServerof<PXFS_VER::symbolic_link>,
	public symlink_ii
{
public:
	symlink_norm_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);
	~symlink_norm_impl();
	virtual void		_unreferenced(unref_t arg);

	// Return a new PXFS_VER::fobj CORBA reference to this.
	virtual PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t
	    get_fobj_type(Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	virtual void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	virtual void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	/* symbolic_link */

	virtual void	readlink(PXFS_VER::symbolic_link::target_t_out target,
	    solobj::cred_ptr credobj, Environment &_environment);

	// End of methods supporting IDL operations
};

//
// Replicated CORBA implementation of symbolic_link
//
class symlink_repl_impl :
	public mc_replica_of<PXFS_VER::symbolic_link>,
	public symlink_ii
{
public:
	// Primary constructor.
	symlink_repl_impl(fs_ii *fsp, vnode_t *vp, fid_t *fidp);

	// Secondary constructor.
	symlink_repl_impl(fs_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
		PXFS_VER::symbolic_link_ptr obj);

	~symlink_repl_impl();

	virtual void		_unreferenced(unref_t arg);

	// Return a new PXFS_VER::fobj CORBA reference to this.
	virtual PXFS_VER::fobj_ptr	get_fobjref();

	virtual handler		*get_handler();

	//
	// Return a CORBA pointer (no CORBA::release() required)
	// to the checkpoint interface.
	//
	virtual REPL_PXFS_VER::fs_replica_ptr	get_ckpt();

	// Add a commit checkpoint.
	virtual void	commit(Environment &e);

	// Methods supporting IDL operations
	/* fobj */

	virtual PXFS_VER::fobj_type_t
	    get_fobj_type(Environment &_environment);

	virtual void	get_attributes(uint32_t attrmask, sol::vattr_t &attr,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_attributes(const sol::vattr_t &attr,
	    int32_t attrflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	get_secattributes(PXFS_VER::secattr_out sattr,
	    int32_t secattrflag,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	set_secattributes(const PXFS_VER::secattr &sattr,
	    int32_t secattrflag, solobj::cred_ptr credobj,
	    Environment &_environment);

	virtual void	pathconf(int32_t cmd, sol::uintptr_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	getfobjid(PXFS_VER::fobjid_t_out fobjid,
	    Environment &_environment);

	virtual void	cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t pid,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, int32_t &result,
	    solobj::cred_ptr credobj, Environment &_environment);

	virtual void	frlock(int32_t cmd, sol::flock64_t &lock_info,
	    int32_t flag, sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	virtual void	frlock_cancel_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	frlock_execute_request(
	    PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	virtual void	shrlock(int32_t cmd, sol::shrlock_t &lock_info,
	    int32_t flag, solobj::cred_ptr credobj, Environment &_environment);

	virtual void	remove_locks(sol::pid_t pid, sol::lsysid_t sysid,
	    solobj::cred_ptr credobj, Environment &_environment);

	//
	// Interface operations of the server supporting
	// client side caching of attributes and access info.
	//

	virtual void	cache_new_client(PXFS_VER::bind_info &binfo,
	    PXFS_VER::fobj_client_ptr client1_p,
	    PXFS_VER::fobj_client_out client2_p,
	    Environment &_environment);

	virtual void	cache_remove_client(PXFS_VER::fobj_client_ptr client_p,
	    Environment &_environment);

	virtual void	cache_get_all_attr(solobj::cred_ptr credobj,
	    PXFS_VER::attr_rights rights, sol::vattr_t &attributes,
	    uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_write_all_attr(sol::vattr_t &attributes,
	    int32_t attrflags, bool discard, bool sync,
	    solobj::cred_ptr credobj, uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_attr_drop_token(uint32_t server_incarn,
	    Environment &_environment);

	virtual void	cache_set_attributes(sol::vattr_t &wb_attributes,
	    int32_t wb_attrflags,
	    const sol::vattr_t &attributes, int32_t attrflags,
	    solobj::cred_ptr credobj, uint64_t &seqnum, uint32_t &server_incarn,
	    Environment &_environment);

	virtual void	cache_access(int32_t accmode, int32_t accflags,
	    solobj::cred_ptr credobj, bool &allowed, uint64_t &seqnum,
	    Environment &_environment);

	/* symbolic_link */

	virtual void	readlink(PXFS_VER::symbolic_link::target_t_out target,
	    solobj::cred_ptr credobj, Environment &_environment);

	// End of methods supporting IDL operations
};

#include <pxfs/server/symlink_impl_in.h>

#endif	// SYMLINK_IMPL_H
