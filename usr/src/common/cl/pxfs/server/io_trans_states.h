//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef IO_TRANS_STATES_H
#define	IO_TRANS_STATES_H

#pragma ident	"@(#)io_trans_states.h	1.6	08/05/20 SMI"

#include <h/replica.h>
#include <h/repl_dc.h>
#include <repl/service/replica_tmpl.h>

#include "../version.h"

class io_ioctl_state : public transaction_state {
public:
	io_ioctl_state();

	static void register_new_state(Environment &env);

	//
	// If the environment variable passed in indicates that
	// this call is a retry, then this function returns true,
	// otherwise it returns false.
	// If `done' is true, the operation completed and
	// the results are returned in the parameters passed into this
	// function.
	// If `done' is false, the operation did not complete.
	//
	static bool retry(Environment &env, bool &done,
	    sol::error_t &err, int32_t &result);

	static void set_ioctl_results(Environment &env,
	    sol::error_t err, int32_t result);

	//
	// If this function returns true, the operation completed and
	// the results are returned in the parameters passed into this
	// function.
	// If this function returns false, the operation did not
	// complete
	//
	bool operation_done(sol::error_t &err, int32_t &result);

	// Called to set the results of the operation
	void set_results(sol::error_t err, int32_t result);

	// Called when state is unreferenced because client is dead.
	void orphaned(Environment &);

	//
	// Called when transaction is commited on the primary i.e. when
	// the primary calls add_commit(), or when the framework knows
	// that the invocation has completed on the primary.
	//
	void commited();

private:
	sol::error_t _err;
	int32_t _result;
	bool _done;
};

//
// This state object is used to store the error code from a close in case of
// a retry.  See io_repl_impl::close().
//
class io_close_state : public transaction_state {

public:

	io_close_state(sol::error_t err);

	static void register_new_state(Environment &env,
	    sol::error_t err);

	//
	// If the environment variable passed in indicates that
	// this call is a retry, then this function returns true,
	// otherwise it returns false.  If this is a retry and there
	// was an error, it also sets the error exception.
	//
	static bool retry(Environment &env);

	// Called when state is unreferenced because client is dead.
	void orphaned(Environment &);

	//
	// Called when transaction is commited on the primary i.e. when
	// the primary calls add_commit(), or when the framework knows
	// that the invocation has completed on the primary.
	//
	void commited();

	sol::error_t get_error();

private:

	sol::error_t _err;
};

#endif	// IO_TRANS_STATES_H
