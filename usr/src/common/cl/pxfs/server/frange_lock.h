//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FRANGE_LOCK_H
#define	FRANGE_LOCK_H

#pragma ident	"@(#)frange_lock.h	1.7	08/05/20 SMI"

#include <sys/flock_impl.h>	// for lock_descriptor_t

#include <h/sol.h>
#include <h/solobj.h>
#include <sys/os.h>
#include <sys/list_def.h>
#include <orb/invo/common.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include PXFS_IDL(repl_pxfs)

// Forward declaration
class fobj_ii;

//
// frange_lock - this class encapsulates all of the support
// for a file range lock.
//
class frange_lock {
public:
	frange_lock(fobj_ii *fobjiip);
	~frange_lock();

	//
	// This method gets invoked on the secondary via a checkpoint by
	// frlock() and remove_locks() on the primary.
	//
	void		ckpt_locks(const REPL_PXFS_VER::lock_info_seq_t &locks);

	//
	// Return true if there are any active locks.
	//
	bool	has_active_locks();

	// Helper function that processes a woken-up lock.
	sol::error_t	process_pxfs_lock(lock_descriptor_t *lock,
	    bool dead_client);

	//
	// Function called on the primary as a result of a PXFS lock
	// becoming active in the LLM.
	//
	void	lock_active(lock_descriptor_t *);

	//
	// Function called on the primary as a result of an active PXFS lock
	// being deleted in the LLM.
	//
	void	lock_deleted(lock_descriptor_t *);

	// Helper functions to access the 'processed_list'.
	void	lock_processed_list();
	void	unlock_processed_list();
	void	add_to_processed_list(lock_descriptor_t *lock);
	bool	remove_from_processed_list(lock_descriptor_t *lock);
	bool	is_in_processed_list(PXFS_VER::pxfs_llm_callback_ptr cb);

	//
	// Call made on the primary and the secondary to cleanup locks
	// originating from a particular node when the node dies.
	//
	void	cleanup_locks(nodeid_t);

	void	remove_file_locks_by_sysid(int32_t sysid);
	void	remove_file_locks_by_nlmid(int32_t nlmid);

	// Calls to manipulate 'cb_obj_list'.
	void	add_cb_object(PXFS_VER::pxfs_llm_callback_ptr cb_obj,
		    int32_t sysid,
		    bool nlmlock);

	void	remove_cb_object(PXFS_VER::pxfs_llm_callback_ptr cb_obj);
	void	remove_cb_objects_by_node(nodeid_t id);
	void	remove_cb_objects_by_sysid(int32_t sysid);
	void	remove_cb_objects_by_nlmid(int32_t nlmid);
	bool	is_in_cb_object_list(PXFS_VER::pxfs_llm_callback_ptr cb_obj);

	//
	// 'start_locking' and 'end_locking' are the boundaries of a PXFS
	// lock manager transaction with the LLM.
	//
	void	start_locking();
	void	end_locking(Environment &env);

	void frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
	    sol::u_offset_t off, solobj::cred_ptr credobj,
	    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment);

	void frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	void frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
	    Environment &_environment);

	//
	// Helper function on a new primary called to replay all its active
	// locks.
	//
	void	replay_active_locks();

	//
	// Helper function called from 'unreferenced' to cleanup all locks
	// for this file from the LLM.
	//
	void	remove_all_locks();
	void	remove_llm_locks();

private:
	//
	// Helper functions that handle addition and deletion of locks from
	// 'active_locks' list.
	//
	void	new_locks(const REPL_PXFS_VER::lock_info_seq_t &locks);
	void	remove_lock(const REPL_PXFS_VER::lock_info_t &lock);

	//
	// Helper functions to convert between 'lock_descriptor_t' and
	// 'lock_info_t'.
	//
	REPL_PXFS_VER::lock_info_t	*get_lock_info(lock_descriptor_t *);
	lock_descriptor_t		*get_lock_descriptor(
					    REPL_PXFS_VER::lock_info_t *);

public:
	//
	// For HA fobjs, this list stores the list of active locks on this file.
	// These locks need to be replayed on the new primary during a
	// switchover/failover.
	//
	SList<REPL_PXFS_VER::lock_info_t>	active_locks;

private:
	typedef struct cb_obj_info {
		// Callback object
		PXFS_VER::pxfs_llm_callback_var cb_obj;

		// sysid of NLM client
		int32_t sysid;

		// true if this is an NLM lock, false otherwise
		bool nlmlock;
	} cb_obj_info_t;

	//
	// List of cb objects sleeping on the primary.  This list only
	// exists on the primary, and serves to identify retries of F_SETLKW
	// lock requests.
	//
	SList<cb_obj_info_t>	cb_obj_list;

	// Protects cb_obj_list.
	os::mutex_t		cb_obj_list_lock;

	//
	// On the primary, 'granted_list' (see cl_flock.cc) stores the
	// previously blocked lock requests that have now been granted and
	// await the client being notified.  After this notification is
	// delivered, the lock is pushed to the 'processed_list' of the fobj
	// that the lock belongs to.
	//
	SList<lock_descriptor_t>	processed_list;
	os::mutex_t			processed_list_lock;

	//
	// 'current_locks_list' stores a list of HA PXFS locks that have been
	// modified in the current LLM transaction.
	// XXX We should make this an array for faster LLM operation.
	//
	SList<REPL_PXFS_VER::lock_info_t>	current_locks;

	// Protects 'current_locks' and 'active_locks'.
	os::rwlock_t			active_locks_lock;

	// Link to the corresponding fobj
	fobj_ii				*fobj_iip;
};

#include <pxfs/server/frange_lock_in.h>

#endif	// FRANGE_LOCK_H
