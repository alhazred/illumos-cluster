//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef FSMGR_SERVER_IMPL_H
#define	FSMGR_SERVER_IMPL_H

#pragma ident	"@(#)fsmgr_server_impl.h	1.7	08/05/20 SMI"

#include <orb/object/adapter.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include PXFS_IDL(repl_pxfs)
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/prov_common.h>

//
// This class is used to generate _unreferenced()
// when a file system client dies.
//
class fsmgr_server_ii : public prov_common {
public:
	virtual ~fsmgr_server_ii();
	void	unreferenced();

	// Return a new PXFS_VER::fsmgr_server CORBA reference to this object.
	PXFS_VER::fsmgr_server_ptr	get_fsmgrref();

	// Return a fsmgr_server_ii * from the CORBA pointer.
	static fsmgr_server_ii	*get_fsmgr_server_ii(CORBA::Object_ptr objp);

	//
	// Helper function to get clientmgr object from prov_common base class
	// without making a new reference (doesn't need a CORBA::release()).
	//
	PXFS_VER::fsmgr_client_ptr	get_clientmgr() const;

	//
	// Helper function - gets the nodeid of the client this server is
	// associated with.
	//
	nodeid_t	get_nodeid() const;

	// Helper function for dumping state to a new secondary.
	void dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp, Environment &env);

	//
	// Called to cleanup the locks held by this fsmgr_server's
	// corresponding client in the LLM.  This call is idempotent.
	//
	void	cleanup_llm_locks();

	//
	// Called by the FS object to pre-empt the cleanup of locks by stale
	// fsmgr_server objects.  See comments in fs_ii::fsmgr_factory().
	//
	void	mark_llm_cleanedup();

protected:
	fs_ii * const	fs_iip;		// file system we serve

	fsmgr_server_ii(fs_ii *fsp, PXFS_VER::fsmgr_client_ptr clientmgr,
		nodeid_t nid);

private:
	nodeid_t	nodeid;		// nodeid for removing locks

	os::mutex_t	cleanup_lock;
	bool		cleanedup;
};

//
// Unreplicated CORBA implementation of PXFS_VER::fsmgr_server
//
class fsmgr_server_norm_impl : public fsmgr_server_ii,
    public McServerof<PXFS_VER::fsmgr_server> {
public:
	fsmgr_server_norm_impl(fs_ii *fsp, PXFS_VER::fsmgr_client_ptr clientmgr,
	    nodeid_t nid);

	~fsmgr_server_norm_impl();

	void	_unreferenced(unref_t arg);

	// Return a new CORBA reference to this object.
	CORBA::Object_ptr get_provref();
};

//
// Replicated CORBA implementation of PXFS_VER::fsmgr_server
//
class fsmgr_server_repl_impl :
	public mc_replica_of<PXFS_VER::fsmgr_server>,
	public fsmgr_server_ii {
public:
	// Primary constructor
	fsmgr_server_repl_impl(fs_ii *fsp, PXFS_VER::fsmgr_client_ptr clientmgr,
		nodeid_t nid);

	// Secondary constructor
	fsmgr_server_repl_impl(fs_ii *fsp, PXFS_VER::fsmgr_client_ptr clientmgr,
		nodeid_t nid, PXFS_VER::fsmgr_server_ptr obj);

	~fsmgr_server_repl_impl();

	void	_unreferenced(unref_t arg);

	// Return a new CORBA reference to this object.
	CORBA::Object_ptr get_provref();

	REPL_PXFS_VER::fs_replica_ptr	get_ckpt();
};

#include <pxfs/server/fsmgr_server_impl_in.h>

#endif	// FSMGR_SERVER_IMPL_H
