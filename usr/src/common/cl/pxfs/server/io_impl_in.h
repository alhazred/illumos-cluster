//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)io_impl_in.h	1.6	08/05/20 SMI"

// Return a new fobj CORBA reference to this
inline PXFS_VER::fobj_ptr
io_norm_impl::get_fobjref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
inline REPL_PXFS_VER::fs_replica_ptr
io_norm_impl::get_ckpt()
{
	ASSERT(0);
	return (REPL_PXFS_VER::fs_replica::_nil());
}

//
// Add a commit checkpoint.
//
inline void
io_norm_impl::commit(Environment &)
{
}

// Return a new PXFS_VER::fobj CORBA reference to this
inline PXFS_VER::fobj_ptr
io_repl_impl::get_fobjref()
{
	return (get_objref());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
inline REPL_PXFS_VER::fs_replica_ptr
io_repl_impl::get_ckpt()
{
	ASSERT(0);
	return (REPL_PXFS_VER::fs_replica::_nil());
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the I/O checkpoint interface.
//
inline repl_dc::repl_dev_server_ckpt_ptr
io_repl_impl::get_io_ckpt()
{
	return ((device_replica_impl*)(get_provider()))->
	    get_checkpoint_repl_dc();
}

inline repl_dc::repl_dev_server_ckpt_ptr
io_repl_impl::get_checkpoint()
{
	return ((device_replica_impl*)(get_provider()))->
	    get_checkpoint_repl_dc();
}


//
// Add a commit checkpoint.
//
inline void
io_repl_impl::commit(Environment &e)
{
	add_commit(e);
}

//
// Return the dev_t of the real_pvnode.
//
inline dev_t
io_repl_impl::get_realdev() const
{
	return (real_pvnode.fobjinfo.vrdev);
}
