//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef CL_FLOCK_H
#define	CL_FLOCK_H

#pragma ident	"@(#)cl_flock.h	1.6	08/05/20 SMI"

#include <sys/flock_impl.h>
#include <sys/flock.h>
#include <sys/vfs.h>

#include <pxfs/lock/cluster_flock.h>
#include "../version.h"

// forward declarations
class fobj_ii;

#define	l_fobjp(flockp)		\
	(((lock_context_t *)((flockp)->l_pad))->fobj_abstract_p)

void remove_granted_locks(fobj_ii *fobj_iip);

#endif	// CL_FLOCK_H
