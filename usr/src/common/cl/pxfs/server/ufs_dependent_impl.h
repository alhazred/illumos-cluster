//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef UFS_DEPENDENT_IMPL_H
#define	UFS_DEPENDENT_IMPL_H

#pragma ident	"@(#)ufs_dependent_impl.h	1.8	08/05/20 SMI"

#include <sys/lockfs.h>

#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#define	_LEAST_PRIVS
#endif
#if defined(_LEAST_PRIVS)
#include <sys/policy.h>
#endif

#include "../version.h"
#include <pxfs/server/fs_dependent_impl.h>

//
// Definition of the ufs-specific class that does any required special handling
// for ufs files.  Currently, the only use of this is in the
// fobj_ii::cascaded_ioctl() call.
//
class ufs_dependent_impl : public fs_dependent_impl {

public:

	// Constructor.
	ufs_dependent_impl(fs_ii *, const char *mntoptions);

	// Destructor.
	~ufs_dependent_impl();

	//
	// Function called by the fs_impl object right after it creates a
	// new fobj object.  This function is meant to set fs-specific
	// parameters for the fobj - in UFS's case, it would set the 'cachedata'
	// flag based on whether the UFS mount was done with the "forcedirectio"
	// option turned on.
	//
	void	new_fobj(fobj_ii *fobjp);

	//
	// Called when the FS is mounted using new mount options (via the
	// MS_REMOUNT flag to mount).
	//
	void	set_mntopts(const char *);

	//
	// Function called by fobj_ii::cascaded_ioctl() to process fs-specific
	// ioctls.
	// Returns true if the ioctl was processed in this function, and false
	// if not.
	//
	bool	process_cascaded_ioctl(sol::nodeid_t nodeid, fobj_ii *fobjp,
	    int32_t iocmd, sol::intptr_t arg, int32_t flag, cred_t *crp,
	    int *result, int &status, Environment &env);

	void	replay_ioctl(fobj_ii *fobjp, int32_t iocmd, sol::intptr_t arg,
	    int32_t flag, int32_t &result, int &status);

	// Helper function called from repl_pxfs_server::ckpt_lockfs_state().
	void	ckpt_lockfs_state(uint64_t lf_lock, uint64_t lf_flags,
	    uint64_t lf_key, const char *lf_comment);

	//
	// Called from fs_impl::convert_to_primary() AFTER the previously
	// opened fobj's have been constructed.
	//
	int	convert_to_primary(fs_ii *fsp);

	void	freeze_primary(const char *fs_name);

	// Helper function for dumping state to a new secondary.
	void	dump_state(REPL_PXFS_VER::fs_replica_ptr, Environment &);

	int	fs_alloc_data(vnode_t *vp, u_offset_t offset, size_t *len,
	    fdbuffer_t *fdb, int flags, cred_t *cr);

	int	fs_rdwr_data(vnode_t *vp, u_offset_t offset, size_t len,
	    size_t client_file_size, fdbuffer_t *fdb, int flags, cred_t *cr);

	int	fs_fsync(vnode_t *vp, cred_t *crp);

	int	do_fsync(vnode_t *vp, cred_t *crp);

	int	sync_if_necessary(os::hrtime_t &mod_time, vnode *vp,
	    cred_t *cr);

	// Function called from page_out to retry VOP_ALLOC_DATA.
	int	fs_preprocess(vnode_t *vp, u_offset_t offset, size_t *len,
	    fdbuffer_t *fdb, int flags, cred_t *cr);

private:
	// Helper function to process the _FIOSATIME ioctl.
	sol::error_t	ioctl_fiosatime(fobj_ii *fobjp, sol::intptr_t arg,
	    int32_t flag, int *ret_val, cred_t *crp, Environment &_environment);

	// Helper function to process the _FIOLFS ioctl.
	sol::error_t	ioctl_fiolfs(fobj_ii *fobjp, sol::intptr_t arg,
	    int32_t flag, int *ret_val, cred_t *crp, Environment &env);

	//
	// Helper functions used to copy data in and out of user-space for the
	// _FIOLFS ioctl.
	//
#if defined(_LEAST_PRIVS)
	sol::error_t	get_lockfs_user_params(sol::intptr_t uarg, int32_t flag,
	    cred_t *crp, struct lockfs &lockfs, vnode_t *vp);
#else
	sol::error_t	get_lockfs_user_params(sol::intptr_t uarg, int32_t flag,
	    cred_t *crp, struct lockfs &lockfs);
#endif
	sol::error_t	set_lockfs_user_params(sol::intptr_t uarg, int32_t flag,
	    uint64_t lf_key);

	//
	// Function to set the locking parameters of the underlying filesystem.
	// This value is used during failovers/switchovers to replay the locking
	// ioctl.
	//
	void	store_lockfs_params(struct lockfs &lockfs);

	fs_ii		*_fsp;

	// Lock used to serialize _FIOLFS ioctl calls.
	os::mutex_t	_lockfs_lock;

	//
	// Notifies fs_dependent_impl::freeze_primary()
	// when it's time to wakeup.
	//
	os::condvar_t	_fiolfs_cv;

	// Stored filesystem locking state.
	struct lockfs	_lockfs_info;
	bool		_locking_on;

	bool		_forcedirectio_on;
	bool		_syncdir_on;
	bool		_nocto_on;

	//
	// Last time this FS was sync'ed out in terms of what gethrtime()
	// returns.
	//
	os::hrtime_t	last_sync_time;
	os::rwlock_t	sync_lock;
};

#endif	// UFS_DEPENDENT_IMPL_H
