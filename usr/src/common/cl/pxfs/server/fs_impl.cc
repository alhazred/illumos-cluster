/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1983, 1984, 1985, 1986, 1987, 1988, 1989 AT&T	*/
/*	  All Rights Reserved	  */

/*
 * University Copyright- Copyright (c) 1982, 1986, 1988
 * The Regents of the University of California
 * All Rights Reserved
 *
 * University Acknowledgment- Portions of this document are derived from
 * software developed by the University of California, Berkeley, and its
 * contributors.
 */

#pragma ident	"@(#)fs_impl.cc	1.39	09/03/05 SMI"

#include <sys/errno.h>
#include <sys/types.h>
#include <sys/thread.h>
#include <sys/file.h>
#include <sys/flock.h>
#include <sys/sysmacros.h>
#include <sys/vfs.h>
#include <sys/mount.h>
#include <sys/mntent.h>
#include <sys/dnlc.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <kstat.h>

#include <sys/os.h>
#include <sys/sol_conv.h>
#include <sys/sol_version.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_misc.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/unixdir_impl.h>
#include <pxfs/server/symlink_impl.h>
#include <pxfs/server/special_impl.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/fsmgr_server_impl.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/revoke_allocation.h>

#ifndef VXFS_DISABLED
#include <pxfs/server/vxfs_dependent_impl.h>
#endif

#if SOL_VERSION >= __s10
#ifndef	BUG_4954775
#define	BUG_4954775
#endif
#endif

extern int pxfs_fastwrite_enabled;

extern threadpool revoke_allocation_threadpool;

//
// Tells the server what percentage of total free blocks to
// reserve for a client. This is an internal tunable. The
// current value was arrived at by experiments.
//
int initial_alloc_percentage = 5;

//
// These are the high and low watermarks to swithover to slowpath or
// switch back to fastpath.
// We switch to slowpath when the filesystem is 90% full ie. 10% free.
// We switch back to fastpath when the filesystem is < 80% full ie. 20% free.
// NOTE: It is recommended that these values don't be set too
// low. We don't want to switch to slowpath sooner than
// necessary. No testing has been done for lower values.
//
int high_watermark = 10;
int low_watermark = 20;

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// The classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//
// Tunable for hash size of allfobj_list. This can be safely changed
// during system operation; however, the value will only be read during
// file system object creation (i.e. mount). Very large values could
// lead to excessive memory consumption for sites with many file systems.
// Small values may degrade performance by changing search operations into
// linear searches.  This value should only need to be tuned when the
// the system is subjected to either extreme numbers of file systems or
// file systems with very large numbers of open files. Prime values are
// recommended.
//
uint_t pxfs_allfobj_buckets = 2917;

//
// Directory name for deleted files that still have an active vnode reference.
//
const char fs_ii::deleted_dir[] = "._";

//
// Global mount sequence number: for creating unique kstat names
//
int fs_ii::mount_seq = 0;

// Maximum fobj's that can be checkpoint-created in one go
int max_fobj_ckpt = 1024;

//
// Constructor for fs_recovery_task.
//
// The fs_recovery_task is created by convert_to_primary() when a node
// becomes the new primary. If we issue sync_filesystem() on clients
// via the filesystem's server object, those calls would be CORBA
// invocations. Switchovers will have to wait for the call to finish.
// A sync_filesystem() can in turn wait for a switchover to complete.
// We would then deadlock as this is a case where an HA service making
// an invocation to itself.
//
// To avoid the deadlock we issue sync_filesystem() via a stored fs
// manager client reference. By the time the task starts executing we
// could already have done multiple switchovers. To identify those
// cases we compare the current incarnation number of the fs server to
// the incarnation number stored in the recovery task. If the numbers
// don't match the recovery task is aborted.
//
fs_recovery_task::fs_recovery_task(fs_ii * fs_p, uint32_t server_incn):
	fs_iip(fs_p),
	server_incarn(server_incn)
{
	fsmgr_server_ii		*fsmgrp;
	CORBA::Object_var	obj_v;
	fsmgr_client_elem	*fsmgr_client_elemp;

	//
	// Store the CORBA reference to the filesystem server object.
	// A switchover could occur before the task executes, and we
	// cannot obtain a reference when we are a secondary.
	//
	fs_v = fs_p->get_fsref();

	//
	// The iter object will lock the list and prevent modifications
	// to it while we are copying it.
	//
	prov_common_iter	fsmgr_iter_i(fs_iip->get_fsmgr_set());

	//
	// Now traverse the new primary's servermgr_set and for each
	// server, insert it's fs manager client pointer into this
	// fs_recovery_task's client manager list.
	//

	PXFS_VER::fsmgr_client_var fsmgrclient_v;

	while (!CORBA::is_nil(obj_v = fsmgr_iter_i.nextref())) {

		fsmgrp = fsmgr_server_ii::get_fsmgr_server_ii(obj_v);

		//
		// Create a new list element to hold this fsmgr server's
		// client manager object. We have to take a reference and use
		// a smart pointer as the fs client manager object can go away
		// if the node corresponding to it dies.
		//
		fsmgrclient_v =
		    PXFS_VER::fsmgr_client::_duplicate(fsmgrp->get_clientmgr());

		fsmgr_client_elemp = new fsmgr_client_elem(fsmgrclient_v);

		//
		// Insert the newly created element into the list. We needn't
		// lock the list as there is only one thread which would
		// operate on it at any given time. This element must be freed
		// when fs recovery task has called sync_filesystem() on the
		// client.
		//
		fsmgr_client_list.append(fsmgr_client_elemp);
	}
}

//
// execute - perform file system recovery after possible failover.
// We may have lost space allocations.
// This ships all info to server.
//
void
fs_recovery_task::execute()
{
	Environment		e;
	solobj::cred_var	credobj = solobj_impl::conv(kcred);
	fsmgr_client_elem	*fsmgr_client_elemp;

	//
	// Walk through the list of client fs managers and issue
	// sync_filesystem() on each.
	//
	// We will destroy each element as we scan the list.
	//
	while ((fsmgr_client_elemp = fsmgr_client_list.reapfirst()) != NULL) {
		//
		// If the server's incarnation number is different from the
		// incarnation number stored when this task was created,
		// atleast one switchover has happened after this recovery
		// task was created. We must not do a sync_filesystem(), the
		// current primary will do sync_filesystem().
		//
		if (server_incarn != fs_iip->get_server_incn()) {
			//
			// Free current element and the list itself as
			// we will not visit other elements.
			//
			delete fsmgr_client_elemp;
			fsmgr_client_list.dispose();
			break;
		}

		// Ask the client's fs manager to sync this filesystem.
		fsmgr_client_elemp->fsmgr_client_v->sync_filesystem(credobj, e);
		e.clear();

		// Delete the current element.
		delete fsmgr_client_elemp;
	}

	delete this;
}

//
// Internal implementation constructor.
//
// Invoking this constructor with a NULL vfsp indicates that
// we're the secondary or a primary that couldn't mount the file system.
//
//lint -e668 -e1732 -e1733
fs_ii::fs_ii(vfs_t *vfsp, const char *fstype, fobj_ii_factory_t factory,
    const char *spec, const char *options, repl_pxfs_server *serverp) :
	rootfactory(factory),
	vfs(vfsp),
	pxfs_serverp(serverp),
	deleted(NULL),
	deletecnt(0),
	number_joins(0),
	aio_cbk_count(0),
	server_incn(0),
	sysmsg(SC_SYSLOG_FILESYSTEM_TAG, spec, NULL),
	fs_base_ii(pxfs_allfobj_buckets),
	revoke_in_progress(0)
{
	//
	// Copy mount options (note: don't use CORBA::String_var constructor
	// since it doesn't make a copy).
	//
	mnt_options = options;

	//
	// Set the fastwrite flag here.
	// XXX : We can have a mount option as well to disable
	//	the fastwrite feature. And that verification can be done
	//	here. However, we would have to gothrough PSARC for this.
	//
	// Disable the fastwrite if pxfs_fastwrite_enabled == 0
	//

	fastwrite = (pxfs_fastwrite_enabled == 1);

#ifndef VXFS_DISABLED
	if (strcmp(fstype, "vxfs") == 0) {
		vxfs_set_fastwrite_flag();
	}
#endif

	//
	// Disable fastwrite when syncdir mount option is specified.
	// This is to keep the semantics of write(2) same when syndir is on
	// that block allocation is committed to disk when write(2) returns.
	//
	if ((mnt_options != NULL) &&
	    (pxfslib::exists_mntopt(mnt_options, MNTOPT_SYNCDIR, false))) {
		fastwrite = false;
	}

	//
	// Set blocks reserved for each client to zero initially.
	//
	bzero(blocks_allocated, sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));

	//
	// Initialize the fs_status to GREENZONE.
	//
	fs_status = PXFS_VER::GREENZONE;

	// Allocate an 'fs_dependent_impl' instance for this filesystem.
	fs_dep_implp = fs_dependent_impl::get_fs_dependent_impl(this, fstype,
	    options);


#ifdef PXFS_KSTATS_ENABLED

	//
	// Creation of kstat data structures:
	//
	// A kstat structure is identified by (module, instance, name)
	// Module is pxfs, and instance is 0.
	// Name identifies the fs here.
	//
	// Construct the name for the given fs.
	//

	char *stats_name = new char[KSTAT_STRLEN];

	if (mount_seq == 0xffff) {
		mount_seq = 0;
	} else {
		mount_seq++;
	}

	(void) sprintf(stats_name, "%04x:", mount_seq);
	(void) strncpy(stats_name+5, spec, (size_t)KSTAT_STRLEN-6);
	stats_name[KSTAT_STRLEN-1] = 0;

	//lint +e668 +e1732 +e1733

	//
	// Kstat structure is created for the filesystem,
	// synchronously with the global mount
	// of the filesystem, in fs_ii's constructor.
	//
	stats = kstat_create("pxfs", 0, stats_name, "filesystem",
	    KSTAT_TYPE_NAMED, FS_STATS_MAX_NUM, 0);

	if (stats != NULL) {

		// Initialize fobj type counts

		kstat_named_init(&(KSTAT_NAMED_PTR(stats)
		    [FS_STATS_FOBJ_FILE]), "Objects of type file",
		    KSTAT_DATA_UINT32);
		(KSTAT_NAMED_PTR(stats))[FS_STATS_FOBJ_FILE].value.ui32 = 0;

		kstat_named_init(&(KSTAT_NAMED_PTR(stats)
		    [FS_STATS_FOBJ_UNIXDIR]), "Objects of type unixdir",
		    KSTAT_DATA_UINT32);
		(KSTAT_NAMED_PTR(stats))[FS_STATS_FOBJ_UNIXDIR].value.ui32 = 0;

		kstat_named_init(&(KSTAT_NAMED_PTR(stats)
		    [FS_STATS_FOBJ_SYMLINK]), "Objects of type symlink",
		    KSTAT_DATA_UINT32);
		(KSTAT_NAMED_PTR(stats))[FS_STATS_FOBJ_SYMLINK].value.ui32 = 0;

		kstat_named_init(&(KSTAT_NAMED_PTR(stats)
		    [FS_STATS_FOBJ_PROCFILE]), "Objects of type procfile",
		    KSTAT_DATA_UINT32);
		(KSTAT_NAMED_PTR(stats))[FS_STATS_FOBJ_PROCFILE].value.ui32 = 0;

		// Initialization over. We can now install.
		kstat_install(stats);

	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_RED,
		    ("fs_ii:constructor(%p) cannot create kstat %s\n",
		    this, stats_name));
	}

	delete[] stats_name;

#else
	stats = NULL;
#endif

}

fs_ii::~fs_ii()
{
	// Free up the 'fs_dependent_impl' instance for this filesystem.
	fs_dependent_impl::free_fs_dependent_impl(fs_dep_implp);

	if (deleted != NULL) {
		VN_RELE(deleted);
	}
	if (vfs != NULL) {
		//
		// Normally, unmount() will unmount the underlying
		// file system; however, if the client node crashes after
		// the factory has constructed the filesystem object,
		// we will get _unreferenced() and need to clean up.
		//
		forced_unmount();
	}

#ifdef PXFS_KSTATS_ENABLED
	if (stats != NULL) {
		kstat_delete(stats);
	}
#endif
} //lint !e1740 pointers are neither freed nor zero'ed by destructor

// virtual
fs_base_ii::base_type_t
fs_ii::get_base_type()
{
	return (fs_base_ii::BASE_FILE_SYSTEM);
}

void
fs_ii::initialize_blocks(bool reset_blocks_free)
{
	statvfs64_t statvfs_buf;
	int error;

	//
	// Nothing to be done if fastwrite is disabled.
	//
	if (!fastwrite) {
		return;
	}

	block_allocation.lock();
	//
	// Use VFS_STATVFS to compute the number of
	// free disk blocks and block size for the underlying
	// filesystem.
	//
	error = VFS_STATVFS(vfs, &statvfs_buf);
	if (error) {
		// We don't expect VFS_STATVFS to fail here
		CL_PANIC("statvfs failed\n");
	}

	fs_fs_bsize = (int32_t)statvfs_buf.f_bsize;

	//
	// The numbers reported by VFS_STATVFS are in frags and not blocks.
	// Hence convert them to blocks.
	//
	uint64_t ratio = statvfs_buf.f_bsize/statvfs_buf.f_frsize;

	//
	// Here we calculate the 10% of the total FS size.
	// The idea is to switch to slowpath when we are 90% full (ie. 100-10)
	// ie. When we have only 10% of the total blocks (for non-super user)
	// available/free.
	//
	PXFS_VER::blkcnt_t blocks_reserved_su;
	blocks_reserved_su = (statvfs_buf.f_bfree - statvfs_buf.f_bavail);
	blocks_free_high_w_mark =
	    (statvfs_buf.f_blocks - blocks_reserved_su) * high_watermark / 100;
	blocks_free_low_w_mark =
	    (statvfs_buf.f_blocks - blocks_reserved_su) * low_watermark / 100;

	blocks_reserved_su = blocks_reserved_su / ratio;
	blocks_free_high_w_mark = blocks_free_high_w_mark / ratio;
	blocks_free_low_w_mark = blocks_free_low_w_mark / ratio;

	if (reset_blocks_free) {
		blocks_free = statvfs_buf.f_bavail / ratio;
	}

	PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_GREEN,
	    ("fs_ii:initialize_blocks(%p) f_blocks = %lld f_bfree = %lld "
	    "f_bavail = %lld blocks_hwm = %lld blocks_lwm = %lld "
	    "blocks_free = %lld\n",
	    this, statvfs_buf.f_blocks/ratio, statvfs_buf.f_bfree/ratio,
	    statvfs_buf.f_bavail/ratio, blocks_free_high_w_mark,
	    blocks_free_low_w_mark, blocks_free));

	block_allocation.unlock();
}

//
// Constructor for the unreplicated CORBA implementation.
//
fs_norm_impl::fs_norm_impl(vfs_t *vfsp, const char *fstype, const char *spec,
    const char *options) :
	fs_ii(vfsp, fstype, fobj_ii_norm_factory, spec, options, NULL)
{
	set_fs_ii(this);

	initialize_blocks(true);
}

//
// Constructor for primary replicated CORBA implementation.
//
fs_repl_impl::fs_repl_impl(vfs_t *vfsp, const char *fstype, const char *spec,
    const char *options, repl_pxfs_server *serverp) :
	fs_ii(vfsp, fstype, fobj_ii_repl_factory, spec, options, serverp),
	mc_replica_of<PXFS_VER::filesystem>(serverp)
{
	set_fs_ii(this);
}

//
// Constructor for the secondary replicated CORBA implementation.
//
fs_repl_impl::fs_repl_impl(const char *fstype, const char *spec,
    const char *options, repl_pxfs_server *serverp,
    PXFS_VER::filesystem_ptr obj) :
	fs_ii((vfs_t *)NULL, fstype, fobj_ii_repl_factory, spec, options,
	    serverp),
	mc_replica_of<PXFS_VER::filesystem>(obj)
{
	set_fs_ii(this);
	is_primary = false;
}

void
fs_norm_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	unreferenced();
}

void
fs_repl_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	unreferenced();
}

void
fs_ii::unreferenced()
{
	//
	// Since we have pointers to fsmgr_server and fobj_ii objects
	// and they each have a pointer to us, we need to wait until
	// _unreferenced() calls have been made on those objects before
	// deleting ourself. Since _unreferenced() can be called on
	// any object in any order, the last call must delete this object.
	//
	rele();
}

//
// Get fs_ii* from the filesystem_ptr
// This returns NULL if the input object is nil.
// When the object was constructed, we set the handler's cookie to point
// to the fs_ii.
//
fs_ii *
fs_ii::get_fs_ii(PXFS_VER::filesystem_ptr fs_obj)
{
	if (CORBA::is_nil(fs_obj)) {
		return (NULL);
	}
	void *p = fs_obj->_handler()->get_cookie();
	return ((fs_ii *)p);
}

//
// This is called to set the cookie so that get_fs_ii() (above) can
// return the fs_ii*.
//
void
fs_ii::set_fs_ii(PXFS_VER::filesystem_ptr fs_obj)
{
	fs_obj->_handler()->set_cookie((void *)this);
}

//
// Set the underlying file system's vfs pointer.
//
void
fs_ii::set_vfsp(vfs_t *vfsp)
{
	ASSERT(vfs == NULL);
	vfs = vfsp;
}

//
// Return true if this is a dead replicated file system
// (i.e., there was a fatal error in convert_to_primary() or
// convert_to_secondary but the replica manager still thinks
// the service is alive).
// If true, the environment is also set to return an exception.
//
bool
fs_ii::is_dead(Environment &env)
{
	if (pxfs_serverp != NULL && pxfs_serverp->get_mount_error() != 0) {
		pxfslib::throw_exception(env, EIO);
		return (true);
	}
	return (false);
}

//
// Check with the replicated pxfs server if a freeze prepare has been
// initiated. If this has started we should not do any more cascaded
// invocations. We return 'true' and raise the 'PRIMARY_FROZEN' system
// exception in that case. The invocation will then be retried from
// the client after unfreeze.
//
bool
fs_ii::freeze_prepare_in_progress(Environment &env)
{
	return (pxfs_serverp != NULL && pxfs_serverp->check_freeze(env));
}

// Increment the count for the specified fobj type
void
fs_ii::kstat_fobj_type_increment(PXFS_VER::fobj_type_t ftype)
{
#ifdef PXFS_KSTATS_ENABLED

	if (stats != NULL) {
		switch (ftype) {
		case PXFS_VER::fobj_file:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_FILE].value.ui32++;
			break;

		case PXFS_VER::fobj_unixdir:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_UNIXDIR].value.ui32++;
			break;

		case PXFS_VER::fobj_symbolic_link:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_SYMLINK].value.ui32++;
			break;

		case PXFS_VER::fobj_procfile:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_PROCFILE].value.ui32++;
			break;

		default:
			break;

		}
	}

#endif
}

// Decrement the count for the specified fobj type
void
fs_ii::kstat_fobj_type_decrement(PXFS_VER::fobj_type_t ftype)
{
#ifdef PXFS_KSTATS_ENABLED

	if (stats != NULL) {
		switch (ftype) {
		case PXFS_VER::fobj_file:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_FILE].value.ui32--;
			break;

		case PXFS_VER::fobj_unixdir:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_UNIXDIR].value.ui32--;
			break;

		case PXFS_VER::fobj_symbolic_link:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_SYMLINK].value.ui32--;
			break;

		case PXFS_VER::fobj_procfile:
			(KSTAT_NAMED_PTR(stats))
			    [FS_STATS_FOBJ_PROCFILE].value.ui32--;
			break;

		default:
			break;

		}
	}

#endif
}

//
// getroot - provide the fobj supporting the root directory for
// this file system along with supporting information.
//
void
fs_ii::getroot(PXFS_VER::fobj_out rootobj, PXFS_VER::fobj_info &rootinfo,
    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p, Environment &_environment)
{
	fobj_ii		*rootfobj_iip;

	client2_p = PXFS_VER::fobj_client::_nil();

	if (CORBA::is_nil(rootfobj)) {
		vnode_t	*vnodep;

		ASSERT(vfs != NULL);
		//
		// The read lock must be held across the call to VFS_ROOT()
		// to prevent a concurrent unmount from destroying the vfs.
		//
		vfs_lock_wait(vfs);
		int	error = VFS_ROOT(vfs, &vnodep);
		vfs_unlock(vfs);
		if (error != 0) {
			pxfslib::throw_exception(_environment, error);
			return;
		}
		ASSERT(vnodep != NULL);
		rootfobj = find_fobj(vnodep, rootinfo, _environment);
		VN_RELE(vnodep);

		if (CORBA::is_nil(rootfobj)) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FS,
			    PXFS_RED,
			    ("fs_ii:getroot(%p) can't find rootfobj\n",
			    this));
			pxfslib::throw_exception(_environment, EIO);
			return;
		}
		rootfobj_iip = fobj_ii::get_fobj_ii(rootfobj);

		//
		// A FID conversion may be needed.
		// After a switchover, the first getroot operation will
		// see that the rootfobj is NIL and then find the
		// unconverted root directory file object in the FID list.
		//
		if (is_replicated()) {
			(void) rootfobj_iip->is_dead(_environment);
			if (_environment.exception()) {
				return;
			}
		}
		ASSERT(rootfobj_iip->is_active());
	} else {
		rootfobj_iip = fobj_ii::get_fobj_ii(rootfobj);

		//
		// Must force a FID conversion since
		// we are executing on the primary
		//
		if (is_replicated()) {
			(void) rootfobj_iip->is_dead(_environment);
			if (_environment.exception()) {
				return;
			}
		}
		pxfs_misc::init_fobjinfo(rootinfo, rootfobj_iip->get_ftype(),
		    rootfobj_iip->get_vp(), rootfobj_iip->get_fidp());

		ASSERT(rootfobj_iip->is_active());
	}
	rootobj = PXFS_VER::fobj::_duplicate(rootfobj);

	//
	// fobj_client object reference returned by get_bind_info()
	// already has the reference count incremented.
	//
	client2_p = rootfobj_iip->
	    get_bind_info(binfo, client1_p, kcred, _environment);
}

void
fs_ii::get_statistics(sol::statvfs64_t &stat, Environment &_environment)
{
	ASSERT(vfs != NULL);
	int error = VFS_STATVFS(vfs, &conv(stat));
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// get_mntinfo - provides information about the real file system
// underlying PXFS:
//	A) mount options
//	B) information from the vfs struct.
//
void
fs_ii::get_mntinfo(PXFS_VER::fs_info &info, CORBA::String_out mntoptions,
    Environment &_environment)
{
	//
	// If the domount() failed in repl_pxfs_server::become_primary(),
	// our vfs pointer could be NULL. We return the error in this case.
	//
	if (pxfs_serverp != NULL && pxfs_serverp->get_mount_error() != 0) {
		pxfslib::throw_exception(_environment,
		    pxfs_serverp->get_mount_error());
		return;
	}
	ASSERT(vfs != NULL);

	// XXX:	should be...	info.fsid = vfs->vfs_fsid;
	info.fsid.val[0] = vfs->vfs_fsid.val[0];
	info.fsid.val[1] = vfs->vfs_fsid.val[1];

	(void) os::strcpy(info.fstype, vfssw[vfs->vfs_fstype].vsw_name);
	info.fsbsize = vfs->vfs_bsize;
	info.fsdev = vfs->vfs_dev;
	info.fsflag = vfs->vfs_flag;

	// Return a copy of the mount options.
	mntoptions = os::strdup(mnt_options);
}

void
fs_ii::sync(int32_t, Environment &)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:sync(%p) called\n",
	    this));
}

void
fs_ii::sync_recovery(uint32_t server_incarn, Environment &_environment)
{
	if (server_incn != server_incarn) {
		//
		// This is a request for recovery for an incarnation
		// of the primary that is no longer the primary.
		//
		return;
	}
	// Sync the file system information on all nodes
	sync_fs(kcred);

	// Show no longer in recovery mode
	recovery_mode = false;
}

//
// getfobj
// This is similar to lookup() except that we
// are returning a fobj based on the file ID instead of a name in a
// directory.
//
void
fs_ii::getfobj(const PXFS_VER::fobjid_t &fobjid, PXFS_VER::fobj_out fobj,
    PXFS_VER::fobj_info &fobjinfo, PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p, PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	fid_t	tfid;
	vnode_t	*vnodep;

	client2_p = PXFS_VER::fobj_client::_nil();

	//
	// Zero out the data part of the fid.  UFS will ignore the value of
	// 'fid_len', so zeroing out the data is the only safe thing to do.
	//
	bzero(tfid.fid_data, sizeof (tfid.fid_data));
	tfid.fid_len = (ushort_t)fobjid.length();
	ASSERT(tfid.fid_len <= sizeof (tfid.fid_data));
	if (tfid.fid_len > 0) {
		bcopy(fobjid.buffer(), tfid.fid_data, (size_t)tfid.fid_len);
	}

	ASSERT(vfs != NULL);
	int	error = VFS_VGET(vfs, &vnodep, &tfid);
	if (error == 0 && vnodep != NULL) {

		fobj = find_fobj(vnodep, fobjinfo, _environment);

		VN_RELE(vnodep);

		if (is_not_nil(fobj)) {
			fobj_ii		*fobj_iip = fobj_ii::get_fobj_ii(fobj);

			//
			// A FID conversion may be needed.
			// After a switchover, the system may find the
			// unconverted file object in the FID list.
			//
			if (is_replicated()) {
				(void) fobj_iip->is_dead(_environment);
			}

			//
			// fobj_client object reference returned by
			// get_bind_info() already has the reference
			// count incremented.
			//
			client2_p = fobj_iip->get_bind_info(binfo,
			    client1_p, kcred, _environment);

		} else {
			//
			// There is no fobj
			//
			binfo._d_ = PXFS_VER::bt_none;
		}
	} else {
		//
		// Out parameters are not marshalled back on an exception.
		//
		pxfslib::throw_exception(_environment, EINVAL);
	}
}

//
// bind_fs - provide a server side fsmgr to bind to the client side fsmgr.
// This connects a new proxy file system to the server side file system.
//
PXFS_VER::fsmgr_server_ptr
fs_ii::bind_fs(PXFS_VER::fsmgr_client_ptr clientmgr, sol::nodeid_t nodeid,
    uint32_t &server_incarn, uint32_t &fsbsize, bool &fastwrite_flag,
    Environment &_environment)
{
	server_incarn = server_incn;
	fsbsize = (int32_t)fs_fs_bsize;
	fastwrite_flag = fastwrite;

	return (fsmgr_factory(clientmgr, nodeid, _environment));
}

//
// Remount a global file system.
// Note that this can be called more than once if there is a mount_server
// failover.
//
void
fs_ii::remount(PXFS_VER::fobj_ptr mntpnt, const sol::mounta &ma,
    solobj::cred_ptr credobj, uint32_t &vfsflags, CORBA::String_out mntoptions,
    Environment &_environment)
{
	ASSERT(vfs != NULL);

	int error = vfs_lock(vfs);
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
		return;
	}
	// Save old value.
	vfsflags = vfs->vfs_flag;

	//
	// Duplicate work done in domount().
	//
	vfs->vfs_flag |= VFS_REMOUNT;
	vfs->vfs_flag &= ~VFS_RDONLY;

	FAULTPT_PXFS(FAULTNUM_PXFS_REMOUNT_S_B, FaultFunctions::generic);

	int datalen;

#ifndef VXFS_DISABLED
	if (strcmp(ma.fstype, "vxfs") == 0) {
		datalen = vxfs_dependent_impl::vxfs_fixup_args(ma,
		    vxfs_dependent_impl::VX_REMOUNT);
		if (datalen == -1) {
			pxfslib::throw_exception(_environment, ENOENT);
			return;
		}
	} else {
		datalen = (int)ma.data.length();
	}
#else
	datalen = (int)ma.data.length();
#endif

	fobj_ii *fobjp = fobj_ii::get_fobj_ii(mntpnt);
	//
	// Must force a FID conversion since
	// we are executing on the primary
	//
	if (is_replicated()) {
		(void) fobjp->is_dead(_environment);
		if (_environment.exception()) {
			return;
		}
	}
	cred_t *credp = solobj_impl::conv(credobj);
	struct mounta mnta;
	char *options;
	mnta.spec = ((sol::mounta &)ma).spec;
	mnta.dir = ((sol::mounta &)ma).dir;
	mnta.flags = ma.flags;
	mnta.fstype = ((sol::mounta &)ma).fstype;
	mnta.dataptr = (char *)ma.data.buffer();
	mnta.datalen = datalen;
	int len;
	if (mnta.flags & MS_OPTIONSTR) {
		len = (int)ma.options.length();
		//lint -e571 This is ok to loose the sign in this cast.
		options = new char [(size_t)len];
		//lint +e571
		mnta.optptr = os::strcpy(options,
		    (const char *)ma.options.buffer());
		mnta.optlen = len;
		//
		// Strip "global" from the options list,
		// if it happens to be specified.
		// This is because the underlying mount
		// is a local mount.
		//
		(void) pxfslib::exists_mntopt(options, "global", true);
	} else {
		len = MAX_MNTOPT_STR;
		//lint -e571 This is ok to loose the sign in this cast.
		options = new char [(size_t)len];
		//lint +e571
		mnta.optptr = NULL;
		mnta.optlen = 0;
	}

	error = VFS_MOUNT(vfs, fobjp->get_vp(), &mnta, credp);

	FAULTPT_PXFS(FAULTNUM_PXFS_REMOUNT_S_A, FaultFunctions::generic);

	if (error == 0) {
		//
		// Processing for the options string that will be used in
		// /etc/mnttab:
		//
		if ((mnta.flags & MS_OPTIONSTR) != 0) {
			//
			// The underlying filesystem explicitly returned
			// a list of keywords for the accepted options.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_FS,
			    PXFS_GREEN,
			    ("fs_impl:remount(%p) accepted options = %s\n",
			    this, mnta.optptr));
			//
			// Today the underlying filesystem writes the accepted
			// options' keywords to the buffer passed to it.
			// The buffer passed in has the options being requested.
			// The underlying filesystem makes in-place changes
			// to this buffer. However, it is possible that a
			// different buffer is returned in future. The following
			// code would serve as a safety net for this case.
			//
			if (options != mnta.optptr) {
				delete [] options;
				options = mnta.optptr;
				PXFS_DBPRINTF(
				    PXFS_TRACE_FS,
				    PXFS_GREEN,
				    ("fs_impl:remount(%p) new option buffer\n",
				    this));
			}
		} else {
			//
			// The underlying filesystem returned accepted
			// options' information implicitly. The information
			// about accepted options has been recorded in the
			// VFS structure. We build the list of keywords
			// for the accepted options from the information
			// stored in the VFS structure.
			//
			error = vfs_buildoptionstr(&vfs->vfs_mntopts, options,
			    len);
			PXFS_DBPRINTF(
			    PXFS_TRACE_FS,
			    PXFS_GREEN,
			    ("fs_impl:remount(%p) no optionstr, options vfs = "
			    "%s\n",
			    this, options));
		}
		//
		// We add the 'global' keyword to the list as this is
		// a global mount. The underlying mount was local, hence
		// 'noglobal' would have been returned. We delete 'noglobal'
		// from the list.
		//
		(void) pxfslib::exists_mntopt(options, "noglobal", true);
		//lint -e668
		size_t new_len = strlen(options) \
			    + strlen(",global") + 1;
		//lint -e571 ok to loose the sign in this cast.
		if (new_len > (size_t)len) {
		//lint +e571
			char *new_options = new char [new_len];
			(void) strcpy(new_options, options);
			(void) strcat(new_options, ",global");
			delete [] options;
			options = new_options;
		} else {
			(void) strcat(options, ",global");
		}
		//lint +e668
	}

	//
	// If we are switching from read-only to read/write,
	// create the deleted directory.
	//
	if (error == 0 && is_replicated() && (vfsflags & VFS_RDONLY) != 0) {
		//
		// Note: if there is an error creating the deleted
		// directory, we have to revert to read-only or
		// the system will panic when a file is deleted.
		// The underlying file system will be read/write
		// since we can't revert to read-only.
		//
		error = make_deleted(true);
	}

	if (error == 0) {
		// Return new value.
		vfs->vfs_flag &= ~VFS_REMOUNT;
		vfsflags = vfs->vfs_flag | VFS_PXFS;
		mntoptions = options;
		set_options(options);

		if (is_replicated()) {
			//
			// Update the mount argument data that
			// is saved in repl_pxfs_server for
			// mounting the file system after failovers.
			//
			pxfs_serverp->set_mountargs(ma);
			get_ckpt()->ckpt_remount(ma, options, _environment);
			ASSERT(_environment.exception() == NULL);
		}
	} else {
		// Restore old value.
		vfs->vfs_flag = vfsflags;
		pxfslib::throw_exception(_environment, error);
		delete [] options;
	}

	vfs_unlock(vfs);

	FAULTPT_PXFS(FAULTNUM_PXFS_REMOUNT_S_E, FaultFunctions::generic);
}

//
// Unmount a global file system.
// The mount point should be locked on all nodes with
// mount_server::prepare_unmount() prior to calling this.
//
// Supports forced unmount.
//
void
fs_ii::unmount(int32_t flags, solobj::cred_ptr credobj,
    Environment &_environment)
{
	uint_t	i = 0;

	//
	// Protect against the mount service primary crashing and
	// calling us a second time before the first call finishes.
	//
	unmount_lock.wrlock();

	if (vfs == NULL || _environment.is_orphan()) {
		// Already unmounted or the request is an orphan
		unmount_lock.unlock();
		return;
	}

	//
	// Take the lock so that there are no transitions happening
	// via revoke_allocation().
	//
	if (fastwrite) {
		block_allocation.lock();
	}

	//
	// Release cached root pointers.
	//
	rootfobj = PXFS_VER::fobj::_nil();

	//
	// Release the underlying vnode for all fobj objects
	// so the VFS_UNMOUNT() will succeed. This should be OK
	// for a normal unmount (not forced unmount) since in this case
	// mount_server_impl::prepare_unmount() guarantees that there
	// are no proxy vnodes and locks the client proxy file systems
	// from creating new ones in pxfobj::unpack_vnode() (meaning
	// that there should be no new invocations on the fobj and only
	// _unreferenced() is allowed).
	//
	// If this is a forced unmount, we skip all this since VFS_UNMOUNT()
	// should succeed with active file objects (fobj's) . The pxfs
	// subsystem cleans up the file objects after the unmount returns.
	//
	if ((flags & MS_FORCE) == 0) {
		fobj_ii		*fobjp;

		//
		// There could be files in the fidlist that have
		// not been converted to vnodes.
		// If there are fids that require do_delete processing
		// we now convert them and clean them up.
		//
		if (is_replicated()) {
			//
			// Holding fidlock will synchronize this
			// thread with unreference
			//
			fidlock.wrlock();
			fobj_ii_list_t::ListIterator iter(fidlist);
			while ((fobjp = iter.get_current()) != NULL) {
				int err = 0;
				iter.advance();
				if (fobjp->get_delete_num() != 0 &&
				    (err = fobjp->convert_to_primary()) == 0) {
					release_info_t	rinfo;
					(void) fidlist.erase(fobjp);
					fobjp->cleanup_and_get_release_info(
					    rinfo);
					fobj_ii::do_delete(this, rinfo);
				}
				if (err) {
					// Log this error and ignore it.
					PXFS_DBPRINTF(
					    PXFS_TRACE_FS,
					    PXFS_AMBER,
					    ("fs_ii:unmount(%p) fid conversion"
					    "failed for fobj (%p).\n",
					    this, fobjp));
				}
			}
			fidlock.unlock();
		}

		//
		// Cleanup allfobj_list
		//
		for (i = 0; i < allfobj_buckets; i++) {
			fobjlist_locks[i].wrlock();
			fobj_ii_list_t	&hbkt = allfobj_list[i];
			while ((fobjp = hbkt.reapfirst()) != NULL) {
				// Get release info while holding allfobj_lock.
				release_info_t	rinfo;
				fobjp->cleanup_and_get_release_info(rinfo);

				fobjlist_locks[i].unlock();
				fobj_ii::do_delete(this, rinfo);
				fobjlist_locks[i].wrlock();
			}
			fobjlist_locks[i].unlock();
		}

		//
		// Wait for do_delete to be called on all the objects for which
		// delete_num is non zero.
		//
		do_delete_cnt_lock.lock();
		while (do_delete_cnt != 0) {
			do_delete_wait_cv.wait(&do_delete_cnt_lock);
		}
		do_delete_cnt_lock.unlock();

		//
		// Now release the vnode pointer to the deleted directory.
		// If the underlying mount fails, we will no longer allow the
		// user to remove files from this filesystem.
		//
		if (deleted != NULL) {
			VN_RELE(deleted);
			deleted = NULL;
		}
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_UNMOUNT_S_B, FaultFunctions::generic);

	//
	// Note: we need to pass the force flag when present to dounmount()
	// since the underlying file system vnode could be in use after
	// get_vp() was called and before it has been released.
	//
	cred_t	*credp = solobj_impl::conv(credobj);
	int	error = dounmount(vfs, flags, credp);

	FAULTPT_PXFS(FAULTNUM_PXFS_UNMOUNT_S_A, FaultFunctions::generic);

	//
	// We checkpoint unmount success. If this node fails, the secondary
	// will become primary only if the checkpoint isn't set. Otherwise
	// unmount will succeed and the become_primary will fail. Since the
	// become_primary for this fs and device isn't necessary until the
	// next mount or a explicit device import, we ignore the failure.
	//
	if (error != 0) {
		//
		// At this point, the vnode pointer for the '._' directory
		// will be null. Previously, we tried to recreate the
		// directory. However, we found that for certain hardware
		// errors VOP_LOOKUP ops could fail. As a result, we now
		// disable attempts to remove files once this error is
		// returned.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_RED,
		    ("fs_ii:unmount(%p) error %d\n",
		    this, error));

		pxfslib::throw_exception(_environment, error);
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_GREEN,
		    ("fs_ii:unmount(%p) success\n",
		    this));

		vfs = NULL;
		//
		// Tell the secondary(ies) and current replica that
		// this filesystem is now unmounted.
		//
		if (is_primary && is_replicated()) {
			// the secondary
			get_ckpt()->ckpt_fs_is_unmounted(_environment);

			// and the replica
			pxfs_serverp->mark_fs_unmounted();
		}
	}

	if (fastwrite) {
		block_allocation.unlock();
	}

	unmount_lock.unlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_UNMOUNT_S_E, FaultFunctions::generic);
}

//
// Create a new fsmgr_server.
//
PXFS_VER::fsmgr_server_ptr
fs_ii::fsmgr_factory(PXFS_VER::fsmgr_client_ptr clientmgr, nodeid_t nodeid,
    Environment &env)
{
	PXFS_VER::fsmgr_server_ptr servermgr;
	fsmgr_server_ii *p;

	//
	// Show that a new node joined.
	//
	os::atomic_add_32(&number_joins, 1);

	if (is_replicated()) {
		// Create a new replicated fsmgr_server object.
		fsmgr_server_repl_impl *fsmgrp =
		    new fsmgr_server_repl_impl(this, clientmgr, nodeid);

		process_stale_fsmgrs(nodeid);
		servermgr_set.lock_provider_list();
		p = servermgr_set.insert_locked(fsmgrp);
		if (p != NULL) {
			//
			// We found a duplicate for this client already in the
			// provider list. This can happen if the secondary
			// received the checkpoint and then a failover occured.
			//
			servermgr = p->get_fsmgrref();
			servermgr_set.unlock_provider_list();

			// Delete unused provider.
			delete fsmgrp;
		} else {
			servermgr = fsmgrp->get_fsmgrref();

			//
			// Send checkpoint so secondary can create a
			// fsmgr_server too.
			//
			get_ckpt()->ckpt_new_fsmgr(servermgr, clientmgr,
			    nodeid, env);
			ASSERT(env.exception() == NULL);

			//
			// Need to retain provider list lock during
			// checkpointing which is made possibly now that
			// the lock is an os::rwlock_t.
			//
			servermgr_set.unlock_provider_list();
		}
	} else {
		// Create a new unreplicated fsmgr_server object.
		fsmgr_server_norm_impl *fsmgrp =
		    new fsmgr_server_norm_impl(this, clientmgr, nodeid);

		servermgr_set.lock_provider_list();
		p = servermgr_set.insert_locked(fsmgrp);
		ASSERT(p == NULL);

		// Get a CORBA reference to the new fsmgr_server.
		servermgr = fsmgrp->get_objref();
		servermgr_set.unlock_provider_list();
	}

	return (servermgr);
}

//
// Checkpoint the blocks_allocated and the blocks_free to the secondaries.
//
void
fs_ii::ckpt_blocks_allocated(
    const repl_pxfs_v1::blocks_allocated_t blocks_allocated_p,
    PXFS_VER::blkcnt_t blocks_free_cnt)
{
	bcopy(blocks_allocated_p.blocks, blocks_allocated,
	    sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));
	blocks_free = blocks_free_cnt;

}

//
// Create a new fsmgr_server.
//
void
fs_ii::ckpt_new_fsmgr(PXFS_VER::fsmgr_server_ptr servermgr,
    PXFS_VER::fsmgr_client_ptr clientmgr, sol::nodeid_t nodeid)
{
	// Create a new replicated fsmgr_server object.
	fsmgr_server_repl_impl *fsmgrp =
	    new fsmgr_server_repl_impl(this, clientmgr, nodeid, servermgr);
	servermgr_set.lock_provider_list();
	fsmgr_server_ii *p = servermgr_set.insert_locked(fsmgrp);
	servermgr_set.unlock_provider_list();
	if (p != NULL) {
		//
		// We found a duplicate for this client already in the
		// provider list. This can happen if the secondary
		// received the checkpoint and then a failover occured.
		// Delete unused provider.
		//
		delete fsmgrp;
	}
}

//
// Create a new (unreplicated) implementation object.
//
fobj_ii *
fs_ii::fobj_ii_norm_factory(fs_ii *fsp, vnode_t *vp, fid_t *fidp)
{
	fobj_ii	*p;

	// Create a fobj_ii for this vnode.
	switch (vp->v_type) {
	case VDIR:
		p = (unixdir_ii *)new unixdir_norm_impl(fsp, vp, fidp);
		break;

	case VREG:
		p = (file_ii *)new file_norm_impl(fsp, vp, fidp);
		break;

	case VLNK:
		p = (symlink_ii *)new symlink_norm_impl(fsp, vp, fidp);
		break;

	case VBLK:
	case VCHR:
	case VFIFO:
		//
		// We return a special file for the above vnode types.
		// Note these are actually realvp's and the caller
		// should make sure they send only realvp's to this
		// factory routine.
		// For the actual device objects the factory provided
		// by device server should be used.
		//
		p = (special_ii *)new special_norm_impl(fsp, vp, fidp);
		break;

	case VSOCK:
		//
		// XXX - A socket is not a regular file.
		// pxfs does not provide cluster wide access to a socket.
		// The regular file support object is being used,
		// even though not everything applies. At some point
		// this should be replaced.
		//
		p = (file_ii *)new file_norm_impl(fsp, vp, fidp);
		break;

	case VNON:
	case VDOOR:
	case VPROC:
	case VBAD:
#ifdef BUG_4954775
	case VPORT:
#endif
	default:
		p = NULL;
		break;
	}

	//
	// Make the FS-dependent 'new_fobj' invocation,
	// only if p is not NULL.
	//
	if (p != NULL)
		(fsp->get_fs_dep_implp())->new_fobj(p);

	return (p);
}

//
// Create a primary implementation of a replicated object.
//
fobj_ii *
fs_ii::fobj_ii_repl_factory(fs_ii *fsp, vnode_t *vp, fid_t *fidp)
{
	fobj_ii	*p;

	// Create a fobj_ii for this vnode.
	switch (vp->v_type) {
	case VDIR:
		p = new unixdir_repl_impl(fsp, vp, fidp);
		break;

	case VREG:
		p = new file_repl_impl(fsp, vp, fidp);
		break;

	case VLNK:
		p = new symlink_repl_impl(fsp, vp, fidp);
		break;

	case VBLK:
	case VCHR:
	case VFIFO:
		p = new special_repl_impl(fsp, vp, fidp);
		break;

	case VSOCK:
		//
		// XXX - A socket is not a regular file.
		// pxfs does not provide cluster wide access to a socket.
		// The regular file support object is being used,
		// even though not everything applies. At some point
		// this should be replaced.
		//
		p = new file_repl_impl(fsp, vp, fidp);
		break;

	case VNON:
	case VDOOR:
	case VPROC:
	case VBAD:
#ifdef BUG_4954775
	case VPORT:
#endif
	default:
		p = NULL;
		break;
	}

	//
	// Make the FS-dependent 'new_fobj' invocation,
	// only if successfully created a new object.
	//
	if (p != NULL)
		(fsp->get_fs_dep_implp())->new_fobj(p);

	return (p);
}

//
// Create a new file object on the PxFS secondary for each file object
// in the sequence. Each file object is identified by it's fid and
// type along with a CORBA reference to the instance in the primary.
//
void
fs_ii::ckpt_dump_fobj_list(const REPL_PXFS_VER::fobj_ckpt_seq_t &fobj_list)
{
	for (int index = 0; index < fobj_list.length(); index++) {
		ckpt_new_fobj(fobj_list[index].obj,
		    fobj_list[index].fid,
		    fobj_list[index].type);
	}
}

//
// ckpt_new_fobj - Create a secondary server file object.
//
void
fs_ii::ckpt_new_fobj(PXFS_VER::fobj_ptr fobj_p,
    const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t type)
{
	fobj_ii		*fobj_iip;

	//
	// We check if the cookie for this CORBA reference is NULL. If its not
	// NULL, it indicates that the implementation object has already been
	// created on this secondary, and this checkpoint is a replay after a
	// failover.
	//
	if (fobj_ii::get_fobj_ii(fobj_p) != NULL) {
		// This checkpoint is a replay after a failover.
		return;
	}

	// Create a fobj_ii for this vnode.
	switch (type) {
	case PXFS_VER::fobj_unixdir: {
		PXFS_VER::unixdir_var	dir_v =
		    PXFS_VER::unixdir::_narrow(fobj_p);

		ASSERT(!CORBA::is_nil(dir_v));
		fobj_iip = new unixdir_repl_impl(this, fobjid, dir_v);
		break;
	}

	case PXFS_VER::fobj_file: {
		PXFS_VER::file_var	file_v =
		    PXFS_VER::file::_narrow(fobj_p);

		ASSERT(!CORBA::is_nil(file_v));
		fobj_iip = new file_repl_impl(this, fobjid, file_v);
		break;
	}

	case PXFS_VER::fobj_symbolic_link: {
		PXFS_VER::symbolic_link_var	sym_v =
		    PXFS_VER::symbolic_link::_narrow(fobj_p);

		ASSERT(!CORBA::is_nil(sym_v));
		fobj_iip = new symlink_repl_impl(this, fobjid, sym_v);
		break;
	}

	case PXFS_VER::fobj_special: {
		PXFS_VER::special_var		spec_v =
		    PXFS_VER::special::_narrow(fobj_p);

		ASSERT(!CORBA::is_nil(spec_v));
		fobj_iip = new special_repl_impl(this, fobjid, spec_v);
		break;
	}

	default:
		os::panic("fs_ii::ckpt_new_fobj: unsupported type %d\n", type);
		fobj_iip = NULL;
		break;
	}

	if (fobj_iip != NULL) {
		//
		// Complete any file system specific file initialization
		//
		fs_dep_implp->new_fobj(fobj_iip);

		//
		// Add the fobj to the conversion list.
		//
		fidlock.wrlock();
		fidlist.prepend(fobj_iip);
		fidlock.unlock();
	}
}

//
// find_fobj - Find the fobj object corresponding to the specified vnode,
// and return a new CORBA reference to it.
// If the fobj object is not found, a new implementation object is created
// based on the factory function.
//
// PXFS places file object on the Fidlist when the node is a secondary.
// After the node becomes a primary, PXFS lazily converts file objects
// to the primary ready state and places the file object on the hash table.
// Thus the more common case is for the file object to be on the hash table.
//
PXFS_VER::fobj_ptr
fs_ii::find_fobj(vnode_t *vnodep, PXFS_VER::fobj_info &fobjinfo,
    Environment &env)
{
	ASSERT(vnodep != NULL);
	ASSERT(rootfactory != NULL);

	//
	// Get the file ID for constructing a new object.
	// We do this here so we don't have to delete the fobj if there is
	// an error.
	//
	fid_t	*fidp = new fid_t;
	fidp->fid_len = MAXFIDSZ;
	bzero(fidp->fid_data, (size_t)MAXFIDSZ);
#if	SOL_VERSION >= __s11
	int	error = VOP_FID(vnodep, fidp, NULL);
#else
	int	error = VOP_FID(vnodep, fidp);
#endif
	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_RED,
		    ("fs_ii:find_fobj(%p) vp = %p VOP_FID error %d\n",
		    this, vnodep, error));
		fobjinfo.ftype = PXFS_VER::fobj_fobj;
		pxfslib::throw_exception(env, error);
		delete fidp;
		return (PXFS_VER::fobj::_nil());
	}

	//
	// TODO: change to hash table on FID.
	// uint_t b_idx = pxfslib::hash_devt_fid(get_vfsp()->vfs_dev, fidp, 0);
	// b_idx = b_idx;
	//
	fobj_ii			*fobj_iip;
	fobj_ii			*newp;
	PXFS_VER::fobj_ptr	fobjp;
	fid_t			*fidp1;

	//
	// Check to see if the file object is in the hash table.
	//
	int				bktno = vp_to_bucket(vnodep);
	fobjlist_locks[bktno].rdlock();
	fobj_ii_list_t			&hbkt = allfobj_list[bktno];
	fobj_ii_list_t::ListIterator	iter(hbkt);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (VN_CMP(vnodep, fobj_iip->get_vp())) {	//lint !e666
			// XXX Found it. Should have kstat for this case.
			fobjp = fobj_iip->get_fobjref();
			fobjlist_locks[bktno].unlock();
			delete fidp;

			// Return the fobj information.
			pxfs_misc::init_fobjinfo(fobjinfo,
			    fobj_iip->get_ftype(),
			    vnodep, fobj_iip->get_fidp());
			return (fobjp);
		}
	}
	fobjlist_locks[bktno].unlock();

	//
	// Check to see if the file object is in the fidlist.
	//
	fidlock.rdlock();
	fobj_ii_list_t::ListIterator	myiter(fidlist);
	for (; (fobj_iip = myiter.get_current()) != NULL; myiter.advance()) {
		fidp1 = fobj_iip->get_fidp();
		if (bcmp(fidp->fid_data, fidp1->fid_data,
		    (size_t)fidp->fid_len) == 0) {
			fobjp = fobj_iip->get_fobjref();
			fidlock.unlock();
			delete fidp;
			//
			// Return the fobj information.
			//
			pxfs_misc::init_fobjinfo(fobjinfo,
			    fobj_iip->get_ftype(),
			    vnodep, fobj_iip->get_fidp());
			return (fobjp);
		}
	}
	fidlock.unlock();

	//
	// Construct a new object.
	//
	newp = rootfactory(this, vnodep, fidp);
	if (newp == NULL) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_RED,
		    ("fs_ii:find_fobj(%p) Unsupported vnode type %d\n",
		    this, vnodep->v_type)); // XXX
		fobjinfo.ftype = PXFS_VER::fobj_fobj;
		delete fidp;
		return (PXFS_VER::fobj::_nil());
	}

	//
	// We have to lock the hash list until we get a new CORBA reference;
	// otherwise, we have a race problem in _unreferenced() when it
	// calls _last_unref().
	//
	fobjlist_locks[bktno].wrlock();

	iter.reinit(hbkt);
	// Check to be sure object wasn't added while lock wasn't held.
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (VN_CMP(vnodep, fobj_iip->get_vp())) {	//lint !e666
			fobjp = fobj_iip->get_fobjref();
			fobjlist_locks[bktno].unlock();

			// Delete the unused new object.
			delete newp;
			delete fidp;

			// Return the fobj information.
			pxfs_misc::init_fobjinfo(fobjinfo,
			    fobj_iip->get_ftype(),
			    vnodep, fobj_iip->get_fidp());
			return (fobjp);
		}
	}

	//
	// Return a new CORBA reference to this implementation object.
	// The hash list itself doesn't keep a CORBA reference to the object
	// so that we will get notified via _unreferenced() when the last
	// CORBA::release() happens.
	//
	fobj_iip = newp;
	fobjp = fobj_iip->get_fobjref();
	hbkt.prepend(fobj_iip);

	if (is_replicated()) {
		//
		// Checkpoint the fobj creation to the secondary.
		// XXX We may want to move this from here to the caller of
		// find_fobj() to optimize the number of checkpoints
		// sent.
		//
		PXFS_VER::fobjid_t	fobjid(fidp->fid_len, fidp->fid_len,
		    (uint8_t *)fidp->fid_data, false);
		get_ckpt()->ckpt_new_fobj(fobjp, fobjid, fobj_iip->get_ftype(),
		    env);
		env.clear();
	}
	fobjlist_locks[bktno].unlock();

	delete fidp;

	// Return the fobj information.
	pxfs_misc::init_fobjinfo(fobjinfo, fobj_iip->get_ftype(), vnodep,
	    fobj_iip->get_fidp());
	return (fobjp);
}

//
// dump_multiple_fobjs
//
// Dumps multiple fobj objects to the newly joined secondary. One
// checkpoint identifies all of the fobj objects that must be created
// on the newly joined secondary node. We dump the state of the fobj
// objects serially in individual checkpoints. The only exception
// possible is version exception.
//
void
fs_ii::dump_multiple_fobjs(REPL_PXFS_VER::fs_replica_ptr ckptp,
    REPL_PXFS_VER::fobj_ckpt_seq_t *fobj_list,
    int fobj_count, Environment &env)
{
	// Adjust sequence length before sending it to the secondary
	fobj_list->length(fobj_count);
	ckptp->ckpt_dump_fobj_list(*fobj_list, env);

	ASSERT(env.exception() == NULL);

	//
	// There was a checkpoint failure. fobj's may not be created.
	// We should not try to dump the state of fobjs.
	//
	if (env.exception() != NULL) {
		return;
	}

	//
	// Now we can dump state of fobjs created by the checkpoint
	// above. A majority of the fobjs created above will not have
	// state to dump and hence going through them sequentially is
	// not a performance hit.
	//
	int index;
	fobj_ii *fobjp;

	for (index = 0; index < fobj_count; index++) {
		env.clear();
		fobjp = fobj_ii::get_fobj_ii((*fobj_list)[index].obj);
		fobjp->dump_state(ckptp, (*fobj_list)[index].obj, env);
		if (env.exception() != NULL) {
			return;
		}
	}
}

//
// dump_hash_list_fobjs
//
// Traverses all fobj hash lists and dumps fobj objects to the newly
// joined secondary in bunches of up to size 'max_fobj_ckpt'.
//
void
fs_ii::dump_hash_list_fobjs(REPL_PXFS_VER::fs_replica_ptr ckptp,
    Environment &env)
{
	PXFS_VER::fobjid_t fobjid;
	PXFS_VER::fobj_var fobj_v;
	fobj_ii *fobj_iip;
	int fobj_index = 0;

	REPL_PXFS_VER::fobj_ckpt_seq_t *fobj_list =
	    new REPL_PXFS_VER::fobj_ckpt_seq_t(max_fobj_ckpt, max_fobj_ckpt);
	if (fobj_list == NULL) {
		CL_PANIC(!"Out of memory");
	}

	//
	// Dump all active fobj objects.
	// Note that this could cause another call to _unreferenced().
	//
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].rdlock();
		fobj_ii_list_t &hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator iter(hbkt);

		for (; (fobj_iip = iter.get_current()) != NULL;
		    iter.advance()) {
			//
			// Add each fobj into the sequence; whenever we build
			// up enough fobjs in the sequence to reach maximum
			// capacity of the sequence, checkpoint the sequence.
			//
			fobjid.load(fobj_iip->get_fidp()->fid_len,
			    fobj_iip->get_fidp()->fid_len,
			    (uint8_t *)fobj_iip->get_fidp()->fid_data, false);

			(*fobj_list)[fobj_index].obj = fobj_iip->get_fobjref();
			(*fobj_list)[fobj_index].fid = fobjid;
			(*fobj_list)[fobj_index].type = fobj_iip->get_ftype();

			if (++fobj_index == max_fobj_ckpt) {
				dump_multiple_fobjs(ckptp, fobj_list,
						    fobj_index, env);
				if (env.exception() != NULL) {
					fobjlist_locks[i].unlock();
					delete fobj_list;
					return;
				}
				fobj_index = 0;
			}
		}
		fobjlist_locks[i].unlock();
	}

	// If the fobj sequence is not empty, checkpoint pending fobjs
	if (fobj_index != 0) {
		dump_multiple_fobjs(ckptp, fobj_list, fobj_index, env);
	}

	delete fobj_list;
}

//
// dump_fobjs_serially
//
// Traverses fobj hash lists and dumps fobj objects serially to the
// newly joined secondary for each fobj.
//
void
fs_ii::dump_fobjs_serially(REPL_PXFS_VER::fs_replica_ptr ckptp,
    Environment &env)
{
	PXFS_VER::fobjid_t	fobjid;
	fobj_ii		*fobj_iip;
	PXFS_VER::fobj_var	fobj_v;

	//
	// Dump all active fobj objects.
	// Note that this could cause another call to _unreferenced().
	//
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].rdlock();
		fobj_ii_list_t &hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator iter(hbkt);
		for (; (fobj_iip = iter.get_current()) != NULL;
		    iter.advance()) {
			//
			// We have to watch the order of checkpoints
			// since we need to create the fobj before
			// we dump its state!
			//
			fobjid.load(fobj_iip->get_fidp()->fid_len,
			    fobj_iip->get_fidp()->fid_len,
			    (uint8_t *)fobj_iip->get_fidp()->fid_data, false);
			fobj_v = fobj_iip->get_fobjref();
			ckptp->ckpt_new_fobj(fobj_v, fobjid,
			    fobj_iip->get_ftype(), env);
			ASSERT(env.exception() == NULL);

			// Now we can dump the fobj state.
			fobj_iip->dump_state(ckptp, fobj_v, env);
			if (env.exception() != NULL) {
				fobjlist_locks[i].unlock();
				return;
			}
		}
		fobjlist_locks[i].unlock();
	}
}

//
// dump_fid_list_fobjs
//
// Dumps fobj objects on the fidlist to the newly joined secondary in
// bunches up to size 'max_fobj_ckpt'.
//
void
fs_ii::dump_fid_list_fobjs(REPL_PXFS_VER::fs_replica_ptr ckptp,
    Environment &env)
{
	PXFS_VER::fobjid_t fobjid;
	PXFS_VER::fobj_var fobj_v;
	fobj_ii *fobj_iip;
	int fobj_index = 0;

	REPL_PXFS_VER::fobj_ckpt_seq_t *fobj_list =
	    new REPL_PXFS_VER::fobj_ckpt_seq_t(max_fobj_ckpt, max_fobj_ckpt);
	if (fobj_list == NULL) {
		CL_PANIC(!"Out of memory");
	}

	fobj_ii_list_t::ListIterator iter(fidlist);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (fobj_iip->get_fidp() == NULL) {
			fobjid.length(0);
		} else {
			fobjid.load(fobj_iip->get_fidp()->fid_len,
			    fobj_iip->get_fidp()->fid_len,
			    (uint8_t *)fobj_iip->get_fidp()->fid_data, false);
		}

		(*fobj_list)[fobj_index].obj = fobj_iip->get_fobjref();
		(*fobj_list)[fobj_index].fid = fobjid;
		(*fobj_list)[fobj_index].type = fobj_iip->get_ftype();

		if (++fobj_index == max_fobj_ckpt) {
			dump_multiple_fobjs(ckptp, fobj_list, fobj_index, env);
			if (env.exception() != NULL) {
				delete fobj_list;
				return;
			}
			fobj_index = 0;
		}
	}

	// If the fobj sequence is not empty, checkpoint pending fobjs
	if (fobj_index != 0) {
		dump_multiple_fobjs(ckptp, fobj_list, fobj_index, env);
	}

	delete fobj_list;
}

//
// dump_fid_list_serially
//
// Dump the fobj objects on the fidlist to the newly joined secondary
// serially.
//
void
fs_ii::dump_fid_list_serially(REPL_PXFS_VER::fs_replica_ptr ckptp,
    Environment &env)
{
	PXFS_VER::fobjid_t fobjid;
	PXFS_VER::fobj_var fobj_v;
	fobj_ii *fobj_iip;

	fobj_ii_list_t::ListIterator iter(fidlist);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (fobj_iip->get_fidp() == NULL) {
			fobjid.length(0);
		} else {
			fobjid.load(fobj_iip->get_fidp()->fid_len,
			    fobj_iip->get_fidp()->fid_len,
			    (uint8_t *)fobj_iip->get_fidp()->fid_data, false);
		}
		fobj_v = fobj_iip->get_fobjref();
		ckptp->ckpt_new_fobj(fobj_v, fobjid, fobj_iip->get_ftype(),
		    env);
		ASSERT(env.exception() == NULL);

		// Now we can dump the fobj state.
		fobj_iip->dump_state(ckptp, fobj_v, env);
		if (env.exception() != NULL) {
			return;
		}
	}
}

//
// dump_state
//
// Top level function for dumping the entire state of the file system
// primary to a newly joined secondary.
//
void
fs_ii::dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp, Environment &env)
{
	//
	// If we have unmounted tell the secondaries, and thus the new
	// potential primary, about this and return. No need to do
	// anything more.
	//
	if (pxfs_serverp->is_fs_unmounted()) {
		get_ckpt()->ckpt_fs_is_unmounted(env);
		return;
	}

	repl_pxfs_v1::blocks_allocated_t tmp_blocks_allocated;
	//
	// Checkpoint the blocks allocated to various clients
	// and the fs_status which indicates whether we are in fastpath
	// or the slowpath.
	//
	if (fastwrite) {
		bcopy(blocks_allocated, tmp_blocks_allocated.blocks,
		    sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));
		ckptp->ckpt_blocks_allocated(tmp_blocks_allocated, blocks_free,
		    env);
		ckptp->ckpt_server_status(fs_status, env);
	}

	// Checkpoint the number used for renaming deleted files
	ckptp->ckpt_deletecnt(deletecnt, env);
	ASSERT(env.exception() == NULL);

	// Create fsmgr_server objects on secondary.
	fsmgr_server_ii		*fsmgrp;
	prov_iter<fsmgr_server_ii> fsmgr_iter(servermgr_set);
	while ((fsmgrp = fsmgr_iter.nextp()) != NULL) {
		fsmgrp->dump_state(ckptp, env);
		if (env.exception() != NULL) {
			return;
		}
	}

	// Find the version of pxfs service.
	version_manager::vp_version_t	pxfs_version;
	(void) pxfslib::get_running_version("pxfs", &pxfs_version);

	if (((pxfs_version.major_num == 2) && (pxfs_version.minor_num >= 2)) ||
	    (pxfs_version.major_num >= 3)) {

		// Dump the fobj's in the hash table in bunches
		dump_hash_list_fobjs(ckptp, env);

		// Dump unconverted fobj's on the fid list in bunches
		fidlock.rdlock();
		dump_fid_list_fobjs(ckptp, env);
		fidlock.unlock();
	} else {
		// Dump the fobj's in the hash table serially.
		dump_fobjs_serially(ckptp, env);

		// Dump uncoverted objects onn the fid list serially.
		fidlock.rdlock();
		dump_fid_list_serially(ckptp, env);
		fidlock.unlock();
	}

	if (env.exception() != NULL) {
		// Only version exception is legal.
		ASSERT(!CORBA::VERSION::_exnarrow(env.exception()));
		CL_PANIC(!"PxFS Version mismatch");
	}

	fs_dep_implp->dump_state(ckptp, env);
}

//
// This function launches revoke_task for all the clients of this FS.
//
void
fs_ii::launch_revoke_tasks()
{
	ASSERT(block_allocation.lock_held());

	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_AMBER,
	    ("fs_ii:launch_revoke_tasks(%p)"
	    " Launching revoke_allocation_tasks\n",
	    this));

	// Revoke all reservations from all the clients.
	fsmgr_server_ii		  *fsmgrp;
	CORBA::Object_var	   obj_v;
	revoke_allocation_task	  *revoke_taskp;
	PXFS_VER::fsmgr_client_var client_v;

	prov_common_iter fsmgr_iter(servermgr_set);
	while (!CORBA::is_nil(obj_v = fsmgr_iter.nextref())) {
		// Get the fs manager server pointer.
		fsmgrp = fsmgr_server_ii::get_fsmgr_server_ii(obj_v);

		// Revoke tasks store a reference to the fs manager client.
		client_v = PXFS_VER::fsmgr_client::_duplicate(
		    fsmgrp->get_clientmgr());

		// Create a new revoke task...
		revoke_taskp = new revoke_allocation_task(this, get_fsref(),
		    get_server_incn(), client_v);

		// ..increment active revoke task count..
		increment_revoke();

		// ... and set it up for execution.
		revoke_allocation_threadpool.defer_processing(revoke_taskp);

		PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_AMBER,
		    ("fs_ii:launch_revoke_tasks(%p) revoke_in_progress %d\n",
		    this, revoke_in_progress));
	}
}

void
fs_ii::reconstruct_blocks_allocated()
{
	//
	// Nothing needs to be done if fastwrite is disabled.
	//
	if (!fastwrite) {
		return;
	}

	block_allocation.lock();

	if (fs_status == PXFS_VER::SWITCH_TO_REDZONE) {
		//
		// Clear the old reservation of all nodes.
		//
		bzero(blocks_allocated,
		    sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));
		launch_revoke_tasks();
	}
	block_allocation.unlock();
}

//
// Helper function for converting from secondary to primary.
// 'firsttime' is true if this is the first time we are the primary.
//
int
fs_ii::convert_to_primary(bool firsttime)
{

	ASSERT(vfs != NULL);
	ASSERT(CORBA::is_nil(rootfobj));

	int error;

	recovery_mode = false;

	//
	// Calculate the incarnation number for this PXFS file system primary
	//
	timespec_t	tv;
	gethrestime(&tv);
	if (tv.tv_sec == INCN_UNKNOWN) {
		tv.tv_sec = 1;
	}

	//
	// The server incarnation number is reset when the server becomes a
	// secondary. If this node is becoming a primary for the first time or
	// if it is transitioning from secondary to primary, the old server
	// incarnation number should be zero.
	//
	// Multiple calls to become_primary without an intervening
	// become_secondary could cause bad things to happen. This assert would
	// enable us to catch this event, if it were to happen.
	//
	ASSERT(server_incn == 0);
	server_incn = (uint32_t)tv.tv_sec;

	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:convert_to_primary(%p) incn %d\n",
	    this, server_incn));

	// We are the primary regardless if there is an error later.
	is_primary = true;

	// Inform all the fsmgr_client objects about the new PXFS primary
	new_primary_notification();

	//
	// Make a directory to hold files that are unlinked but still open.
	//
	if ((vfs->vfs_flag & VFS_RDONLY) == 0) {
		//
		// The read lock must be held across the call to make_deleted()
		// to prevent a concurrent unmount from destroying the vfs.
		//
		vfs_lock_wait(vfs);
		error = make_deleted(firsttime);
		vfs_unlock(vfs);
		if (error != 0) {
			forced_unmount();
			return (error);
		}
	}

	//
	// Loop through all the fobj's to convert from secondary form
	// (fobjid) to primary form (vnode) any that have locks.
	// Other fobj's will be converted when needed.
	//
	fobj_ii		*fobj_iip;
	_suspend_locking_ckpts = true;
	fidlock.wrlock();
	fobj_ii_list_t::ListIterator iter(fidlist);
	while ((fobj_iip = iter.get_current()) != NULL) {
		iter.advance();
		if (fobj_iip->has_active_locks()) {
			//
			// The active list lock is
			// populated so we must
			// convert while IDL calls are
			// suspended.
			//
			error = fobj_iip->convert_to_primary();
			if (error == 0) {
				(void) fidlist.erase(fobj_iip);
				bool added = add_to_list(fobj_iip);
				ASSERT(added);
				continue;
			}
			//
			// There was an error converting this fobj to primary.
			// Leave it marked as a secondary and try to convert
			// the rest.
			//
			PXFS_DBPRINTF(
			    PXFS_TRACE_FS,
			    PXFS_RED,
			    ("fs_ii:convert_to_primary(%p) fobj_ii %p error "
			    "%d\n",
			    this, fobj_iip, error));
		}
	}
	fidlock.unlock();
	_suspend_locking_ckpts = false;

	if (!firsttime) {
		//
		// A failover means that we should do recovery mode.
		// This could be a failover or a switchover.
		// Assume the worst.
		//
		recovery_mode = true;
	}

	// Allow fs-specific conversions, if any.
	error = fs_dep_implp->convert_to_primary(this);
	if (error != 0) {
		recovery_mode = false;
		forced_unmount();
		return (error);
	}

	ASSERT(vfs);

	//
	// Initialise the fs_fs_bsize. Also, set the blocks_free,
	// if this is the firsttime the filesystem is mounted.
	//
	initialize_blocks(firsttime);

	//
	// Reset the number of threads involved in revoke task to zero while
	// the current fs replica is becoming primary.
	// We should not depend on the current revoke_in_progress count to
	// track number of threads doing revoke task job. The revoke task can
	// be scheduled after switchover is completed where the fs replica
	// becomes seconday or spare. And those revoke tasks which are not
	// meant for the current primary will be simply returned from the
	// current primary without decrementing revoke_in_progress count.
	revoke_in_progress = 0;

	if (!firsttime) {
		// after a failover or switchover
		reconstruct_blocks_allocated();
	}

	return (0);
}

//
// Make a directory to hold files that are unlinked but still open.
// The vfs read lock must be held across the call make_deleted()
// to prevent a concurrent unmount from destroying the vfs.
//
// XXX We make the directory writeable by everyone so that
// the rename operation will succeed in all cases. This has
// the disadvantage that this is a directory that everyone
// can write and put their own files into. If kcred is used for
// the rename, it might fail when using NFS for the underlying
// file system since kcred might not have write permission on
// the source directory.
//
int
fs_ii::make_deleted(bool firsttime)
{
	vnode_t	*tempfilep = NULL;
	uint_t	i = 0;
	int	error = 0;

	vattr_t attr;
	attr.va_type = VDIR;
	attr.va_mode = 0777;
	attr.va_mask = AT_TYPE | AT_MODE;

	vattr_t tmpfileattr;
	tmpfileattr.va_type = VREG;
	tmpfileattr.va_mode = 0666;
	tmpfileattr.va_mask = AT_TYPE | AT_MODE;

	// ASSERT(vfs_lock_held(vfs));
	//
	// Get the root vnode.
	//
	vnode_t *vp;
	error = VFS_ROOT(vfs, &vp);
	if (error != 0) {
		//
		// SCMSGS
		// @explanation
		// The file system is corrupt or was not mounted correctly.
		// @user_action
		// Run fsck, and mount the affected file system again.
		//
		(void) msg().log(SC_SYSLOG_WARNING, MESSAGE,
			"Couldn't get the root vnode: error (%d)", error);
		return (error);
	}

	//
	// Create 8K of space in the directory so that we don't return
	// ENOSPC from deletes that happen in this FS.
	//
	// The temporary filename should be as long as possible
	// to minimize the number of files created. Thus the
	// name is MAXNAMELEN characters.
	//
	//lint -e747
	char *tempnm = (char *)kmem_alloc((size_t)MAXNAMELEN, KM_SLEEP);
	//lint +e747
	for (i = 0; i < MAXNAMELEN - 1; i++) {
		tempnm[i] = 'x';
	}
	tempnm[MAXNAMELEN - 1] = '\0';

#if	SOL_VERSION >= __s11
	error = VOP_LOOKUP(vp, (char *)deleted_dir, &deleted,
	    NULL, 0, NULL, kcred, NULL, NULL, NULL);
#else
	error = VOP_LOOKUP(vp, (char *)deleted_dir, &deleted,
	    NULL, 0, NULL, kcred);
#endif
	if (error == ENOENT && firsttime) {
		// Create the directory
#if	SOL_VERSION >= __s11
		error = VOP_MKDIR(vp, (char *)deleted_dir, &attr,
		    &deleted, kcred, NULL, 0, NULL);
#else
		error = VOP_MKDIR(vp, (char *)deleted_dir, &attr,
		    &deleted, kcred);
#endif

		// First, create two files...
		if (error == 0) {
			// Filename xxxx...
#if	SOL_VERSION >= __s11
			error = VOP_CREATE(deleted, tempnm,
			    &tmpfileattr, NONEXCL, 0,
			    &tempfilep, kcred, 0, NULL, NULL);
#else
			error = VOP_CREATE(deleted, tempnm,
			    &tmpfileattr, NONEXCL, 0,
			    &tempfilep, kcred, 0);
#endif
		}
		if (error == 0) {
			VN_RELE(tempfilep);
			tempnm[0] = 'y';
			// Filename yxxx...
#if	SOL_VERSION >= __s11
			error = VOP_CREATE(deleted, tempnm,
			    &tmpfileattr, NONEXCL, 0,
			    &tempfilep, kcred, 0, NULL, NULL);
#else
			error = VOP_CREATE(deleted, tempnm,
			    &tmpfileattr, NONEXCL, 0,
			    &tempfilep, kcred, 0);
#endif
		}

		// ... then delete them.
		if (error == 0) {
			VN_RELE(tempfilep);
			// Filename yxxx...
#if	SOL_VERSION >= __s11
			error = VOP_REMOVE(deleted, tempnm, kcred, NULL, 0);
#else
			error = VOP_REMOVE(deleted, tempnm, kcred);
#endif
		}
		if (error == 0) {
			tempnm[0] = 'x';
			// Filename xxxx...
#if	SOL_VERSION >= __s11
			error = VOP_REMOVE(deleted, tempnm, kcred, NULL, 0);
#else
			error = VOP_REMOVE(deleted, tempnm, kcred);
#endif
		}
	} else if (error == 0 && firsttime) {
		//
		// Directory already exists, and this is the initial mount.
		// Cleanup the ._ directory.
		//
		error = rmfiles(deleted);
		if (error == 0) {
			struct vattr va;

			//
			// Set mode for deleted dir such that everyone
			// has read/write/execute permissions
			//
			va.va_mask = AT_MODE;
			va.va_mode = S_IRWXU | S_IRWXG | S_IRWXO;

#if SOL_VERSION >= __s10
			error = VOP_SETATTR(deleted, &va, 0, kcred, NULL);
#else
			error = VOP_SETATTR(deleted, &va, 0, kcred);
#endif
		}
	}

	VN_RELE(vp);
	//lint -e747
	kmem_free(tempnm, (size_t)MAXNAMELEN);
	//lint +e747
	if (error != 0) {
		//
		// SCMSGS
		// @explanation
		// While mounting this file system, PXFS was unable to create
		// some directories that it reserves for internal use.
		// @user_action
		// If the error is 28(ENOSPC), then mount this FS
		// non-globally, make some space, and then mount it globally.
		// If there is some other error, and you are unable to correct
		// it, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msg().log(SC_SYSLOG_WARNING, MESSAGE,
			"Error creating deleted directory: error (%d)", error);
	}
	return (error);
}

//
// Remove all files from the deleted directory.
//
int
fs_ii::rmfiles(vnode_t *dvp)
{
	int	eof;
	uio_t	tuio;
	iovec_t	iov;

	const size_t maxlen = 1024;
	char *rawdirp = new char [maxlen];

	tuio.uio_iov = &iov;
	tuio.uio_iovcnt = 1;
	tuio.uio_loffset = 0;
	tuio.uio_segflg = UIO_SYSSPACE;
	tuio.uio_llimit = (rlim64_t)RLIM_INFINITY;
	tuio.uio_fmode = FREAD;
again:
	tuio.uio_resid = maxlen;
	iov.iov_base = rawdirp;
	iov.iov_len = maxlen;

	// Read the directory contents.
#if SOL_VERSION >= __s10
	VOP_RWLOCK(dvp, V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWLOCK(dvp, 0);
#endif
#if	SOL_VERSION >= __s11
	int error = VOP_READDIR(dvp, &tuio, kcred, &eof, NULL, 0);
#else
	int error = VOP_READDIR(dvp, &tuio, kcred, &eof);
#endif
#if SOL_VERSION >= __s10
	VOP_RWUNLOCK(dvp, V_WRITELOCK_FALSE, NULL);
#else
	VOP_RWUNLOCK(dvp, 0);
#endif
	if (error != 0) {
		// XXX can't tell if it is empty.

		//
		// SCMSGS
		// @explanation
		// The file system is unable to create temporary copies of
		// deleted files.
		// @user_action
		// Mount the affected file system as a local file system, and
		// ensure that there is no file system entry with name "._" at
		// the root level of that file system. Alternatively, run fsck
		// on the device to ensure that the file system is not
		// corrupt.
		//
		(void) msg().log(SC_SYSLOG_WARNING, MESSAGE,
		    "Couldn't read deleted directory: error (%d)", error);
		delete [] rawdirp;
		return (error);
	}
	size_t cnt = maxlen - (size_t)tuio.uio_resid;
	struct dirent64 *dp = (struct dirent64 *)rawdirp;
	while (cnt >= sizeof (struct dirent64)) {
		// Skip '.' and '..'.
		//lint -e415 -e416
		if (dp->d_name[0] == '.' &&
		    (dp->d_name[1] == '\0' ||
		    (dp->d_name[1] == '.' && dp->d_name[2] == '\0'))) {
			//lint +e415 +e416
			ASSERT(cnt >= dp->d_reclen);
			cnt -= dp->d_reclen;
			dp = (struct dirent64 *)((char *)dp + dp->d_reclen);
			continue;
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_GREEN,
		    ("fs_ii:rmfiles(%p) '%s' ino %lld off %lld len %d\n",
		    this, dp->d_name, dp->d_ino, dp->d_off, (int)dp->d_reclen));
		//
		// Remove file (or directory even if its not empty).
		//
#if	SOL_VERSION >= __s11
		error = VOP_REMOVE(dvp, dp->d_name, kcred, NULL, 0);
#else
		error = VOP_REMOVE(dvp, dp->d_name, kcred);
#endif
		if (error != 0) {
			//
			// SCMSGS
			// @explanation
			// The file system is unable to create temporary
			// copies of deleted files.
			// @user_action
			// Mount the affected file system as a local file
			// system, and ensure that there is no file system
			// entry with name "._" at the root level of that file
			// system. Alternatively, run fsck on the device to
			// ensure that the file system is not corrupt.
			//
			(void) msg().log(SC_SYSLOG_WARNING, MESSAGE,
			    "Couldn't remove deleted directory file, "
			    "'%s' error: (%d)",
			    dp->d_name, error);
		}
		ASSERT(cnt >= dp->d_reclen);
		cnt -= dp->d_reclen;
		dp = (struct dirent64 *)((char *)dp + dp->d_reclen);
	}
	if (!eof && maxlen != (size_t)tuio.uio_resid) {
		goto again;
	}
	delete [] rawdirp;
	return (0);
}

//
// Unmount the underlying file system.
//
void
fs_ii::forced_unmount()
{
	//
	// XXX kcred.
	//
	ASSERT(vfs != NULL);
	(void) dounmount(vfs, MS_FORCE, kcred);
	vfs = NULL;
}

//
// Called from fsmgr::unreferenced() when a client node crashes, and all locks
// originating from there have to be cleaned up.
//
void
fs_ii::cleanup_locks(nodeid_t id)
{
	fobj_ii		*fobj_iip;

	if (!is_secondary()) {
		//
		// On the primary, we first remove all the locks from the LLM
		// associated with node 'id', and then walk the fobj list to
		// remove the active locks.
		// The order in which we grab locks here is important - when
		// processing fobj_ii::unref(), the order is
		// allfobj_list bucket locks > llm_lock,
		// so we maintain that here.
		//

		//
		// Hold 'unmount_lock' to make sure 'vfs' does not become NULL
		// while we are using it.
		//
		unmount_lock.rdlock();
		if (vfs == NULL) {
			//
			// This FS has been unmounted - just return success,
			// no cleanup is required.
			//
			unmount_lock.unlock();
			return;
		}
		for (uint_t i = 0; i < allfobj_buckets; i++) {
			fobjlist_locks[i].wrlock();
		}
		llm_lock.wrlock();
		_suspend_locking_ckpts = true;
		cl_flk_delete_pxfs_locks(vfs, (int)id);
		for (uint_t i = 0; i < allfobj_buckets; i++) {
			fobj_ii_list_t &hbkt = allfobj_list[i];
			fobj_ii_list_t::ListIterator	iter(hbkt);
			for (; (fobj_iip = iter.get_current()) != NULL;
			    iter.advance()) {
				if (fobj_iip->fr_lockp != NULL) {
					fobj_iip->fr_lockp->cleanup_locks(id);
				}
			}
		}
		_suspend_locking_ckpts = false;
		llm_lock.unlock();
		for (uint_t i = 0; i < allfobj_buckets; i++) {
			fobjlist_locks[i].unlock();
		}
		unmount_lock.unlock();
	} else {
		//
		// On the secondary, we walk the fobj list and remove the active
		// locks.
		//
		fidlock.rdlock();
		fobj_ii_list_t::ListIterator	iter(fidlist);
		for (; (fobj_iip = iter.get_current()) != NULL;
		    iter.advance()) {
			if (fobj_iip->fr_lockp != NULL) {
				fobj_iip->fr_lockp->cleanup_locks(id);
			}
		}
		fidlock.unlock();
	}
}

//
// Helper function to clean up the LLM of locks held by any stale fsmgr_server
// objects that have not got an '_unref' notification yet.
//
void
fs_ii::process_stale_fsmgrs(nodeid_t nodeid)
{
	fsmgr_server_ii *p;
	CORBA::Object_var obj_v;

	bool firsttime = true;

	prov_common_iter fsmgr_iter(servermgr_set);
	while (!CORBA::is_nil(obj_v = fsmgr_iter.nextref())) {
		p = fsmgr_server_ii::get_fsmgr_server_ii(obj_v);
		if (p->get_nodeid() == nodeid) {
			if (firsttime) {
				p->cleanup_llm_locks();
				firsttime = false;
			} else {
				p->mark_llm_cleanedup();
			}
		}
	}
}

//
// Called on a primary when it is being switched to a secondary during a
// switchover.
//
void
fs_ii::release_all_locks()
{
	fsmgr_server_ii *p;
	nodeid_t id;

	ASSERT(vfs != NULL);

	prov_iter<fsmgr_server_ii> fsmgr_iter(servermgr_set);
	while ((p = fsmgr_iter.nextp()) != NULL) {
		id = p->get_nodeid();
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_GREEN,
		    ("fs_ii:release_all_locks(%p) cl_flk_delete_pxfs_locks"
		    " %d\n",
		    this, (int)id));
		cl_flk_delete_pxfs_locks(vfs, (int)id);
	}
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:release_all_locks(%p) cl_flk_delete_pxfs_locks %d\n",
	    this, (int)0));
	cl_flk_delete_pxfs_locks(vfs, 0);
}

//
// Call made on this FS object to set the state of the NLM for locks held
// by this filesystem.  Called via set_nlm_status, which is called by
// the Solaris NLM.
//
void
fs_ii::set_nlm_status(int32_t nlmid, PXFS_VER::nlm_status status,
    Environment &_environment)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:set_nlm_status(%p)(%d, %d)\n",
	    this, (int)nlmid, (int)status));

	fs_base_ii::per_node_lock.wrlock();

	fs_base_ii::notify_external_nlm_event(true);

	switch (status) {
	case PXFS_VER::nlm_up:
		cl_flk_set_nlm_status(nlmid, FLK_NLM_UP);
		break;

	case PXFS_VER::nlm_shutting_down:
		cl_flk_set_nlm_status(nlmid, FLK_NLM_SHUTTING_DOWN);
		break;

	case PXFS_VER::nlm_down:
		cl_flk_set_nlm_status(nlmid, FLK_NLM_DOWN);
		break;

	default:
		ASSERT(0);
	}  // end switch

	fs_base_ii::notify_external_nlm_event(false);

	fs_base_ii::per_node_lock.unlock();

	if (status == PXFS_VER::nlm_down) {
		//
		// We have removed all active locks for 'nlmid' from the LLM.
		// Do the same with the PXFS lists.
		//
		remove_file_locks_locked_by_nlmid(nlmid);
		if (is_replicated()) {
			get_ckpt()->ckpt_remove_file_locks_by_nlmid(nlmid,
			    _environment);
		}
	}
}

//
// Call made on this FS object to remove file locks held by this filesystem on
// behalf of a particular NLM.  Called via remove_file_locks, which
// is called by the Solaris NLM.
//
void
fs_ii::remove_file_locks(int32_t sysid,
    Environment &_environment)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:remove_file_locks(%p)(%d)\n",
	    this, (int)sysid));
	//
	// We have to get allfobj_list bucket locks first,
	// since fobj_ii::unreferenced()
	// tries to grab the bucket lock and per_node_lock.
	// Before doing that grab the fidlock as it is higher
	// in the lock ordering.
	//

	fidlock.rdlock();

	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].wrlock();
	}

	fs_base_ii::per_node_lock.wrlock();

	fs_base_ii::notify_external_nlm_event(true);

	cl_flk_remove_locks_by_sysid(sysid);
	remove_file_locks_locked_by_sysid(sysid);
	fidlock.unlock();
	if (is_replicated()) {
		get_ckpt()->ckpt_remove_file_locks_by_sysid(sysid,
		    _environment);
	}

	fs_base_ii::notify_external_nlm_event(false);

	fs_base_ii::per_node_lock.unlock();

	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].unlock();
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii::remove_file_locks(%p)(%d)\n",
	    this, (int)sysid));
}

//
// Remove all active locks associated with 'nlmid'.
//
void
fs_ii::remove_file_locks_locked_by_nlmid(int32_t nlmid)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:remove_file_locks_locked_by_nlmid(%p)(%d)\n",
	    this, (int)nlmid));

	// Remove all file locks with 'sysid' from the fobj objects.
	fobj_ii		*fobj_iip;
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].wrlock();
		fobj_ii_list_t	&hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator	iter(hbkt);
		for (; (fobj_iip = iter.get_current()) != NULL;
		    iter.advance()) {
			if (fobj_iip->fr_lockp != NULL) {
				fobj_iip->fr_lockp->
				    remove_file_locks_by_nlmid(nlmid);
			}
		}
		fobjlist_locks[i].unlock();
	}
	fidlock.wrlock();
	fobj_ii_list_t::ListIterator	iter(fidlist);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (fobj_iip->fr_lockp != NULL) {
			fobj_iip->fr_lockp->remove_file_locks_by_nlmid(nlmid);
		}
	}
	fidlock.unlock();
}

//
// Remove all active locks associated with 'sysid'.
//
void
fs_ii::remove_file_locks_locked_by_sysid(int32_t sysid)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:remove_file_locks_locked_by_sysid(%p)(%d)\n",
	    this, (int)sysid));

	// Remove all file locks with 'sysid' from the fobj objects.

#ifdef DEBUG
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		ASSERT(is_secondary() || fobjlist_locks[i].lock_held());
	}
	ASSERT(fidlock.lock_held());
#endif

	fobj_ii		*fobj_iip;
	for (uint_t i = 0; i < allfobj_buckets; i++) {
		fobj_ii_list_t &hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator iter(hbkt);
		for (; (fobj_iip = iter.get_current()) != NULL;
		    iter.advance()) {
			if (fobj_iip->fr_lockp != NULL) {
				fobj_iip->fr_lockp->
				    remove_file_locks_by_sysid(sysid);
			}
		}
	}
	fobj_ii_list_t::ListIterator iter(fidlist);
	for (; (fobj_iip = iter.get_current()) != NULL; iter.advance()) {
		if (fobj_iip->fr_lockp != NULL) {
			fobj_iip->fr_lockp->remove_file_locks_by_sysid(sysid);
		}
	}
}

//
// Remove all active locks associated with 'nlmid'.
//
void
fs_ii::ckpt_remove_file_locks_by_nlmid(int32_t nlmid)
{
	remove_file_locks_locked_by_nlmid(nlmid);
}

//
// Remove all active locks associated with 'sysid'.
//
void
fs_ii::ckpt_remove_file_locks_by_sysid(int32_t sysid)
{
	fidlock.rdlock();
	remove_file_locks_locked_by_sysid(sysid);
	fidlock.unlock();
}

//
// new_primary_notification - Tell all pxfs file system clients about
// a new file system primary.
//
void
fs_ii::new_primary_notification()
{
	fsmgr_server_ii		*fsmgr_p;
	Environment		e;

	prov_iter<fsmgr_server_ii>	fsmgr_iter(servermgr_set);
	while ((fsmgr_p = fsmgr_iter.nextp()) != NULL) {
		fsmgr_p->get_clientmgr()->
		    new_file_system_primary(server_incn, e);
		e.clear();
	}
}

//
// Helper function for converting from primary to secondary.
// This is called after the service is frozen and all IDL invocations
// have completed (new IDL invocations and _unreferenced() are blocked).
//
int
fs_ii::convert_to_secondary()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_GREEN,
	    ("fs_ii:convert_to_secondary(%p)\n",
	    this));

	uint_t i = 0;

	if (vfs == NULL) {
		//
		// The initial mount must have failed.
		// Just become secondary.
		//
		server_incn = 0;
		return (0);
	}

	//
	// When we do a switchover the underlying file system is force
	// unmounted. If a client had asked for AIO writes and those are
	// pending, then they will never complete after the unmount. The
	// pages associated with the AIOs will remain i/o locked on the
	// client forever.
	//
	// We keep a count of pending AIOs. The last AIO callback to
	// complete will signal us.
	//
	wait_for_aio_callbacks();

	if (fastwrite) {
		block_allocation.lock();
	}

	//
	// Release all file locks from the LLM, and also inform all the
	// fsmgr_client objects to replay their sleeping locks.
	//
	_suspend_locking_ckpts = true;
	release_all_locks();
	_suspend_locking_ckpts = false;

	//
	// We need to convert all fobj's vnode pointers to fid pointers
	// and release the VN_HOLD on the underlying vnode.
	//
	fobj_ii		*fobj_iip;
	fidlock.wrlock();
	for (i = 0; i < allfobj_buckets; i++) {
		fobjlist_locks[i].wrlock();
		fobj_ii_list_t &hbkt = allfobj_list[i];
		fobj_ii_list_t::ListIterator iter(hbkt);
		while ((fobj_iip = iter.get_current()) != NULL) {
			iter.advance();
			(void) fobj_iip->convert_to_secondary();
			(void) hbkt.erase(fobj_iip);
			fidlist.prepend(fobj_iip);
		}
		fobjlist_locks[i].unlock();
	}
	fidlock.unlock();
	is_primary = false;

	// Release cached vnode pointers.
	if (deleted != NULL) {
		VN_RELE(deleted);
		deleted = NULL;
	}
	rootfobj = PXFS_VER::fobj::_nil();

	// We are the secondary regardless if there is an error later.

	int error;
#ifdef _FAULT_INJECTION
	void		*f_argp;
	uint32_t	f_argsize;
	if (fault_triggered(FAULTNUM_PXFS_DOUNMOUNT, &f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (int));
		error = *((int *)f_argp);
	} else
#endif
		error = dounmount(vfs, MS_FORCE, kcred);	// XXX kcred.

	if (fastwrite) {
		block_allocation.unlock();
	}

	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_RED,
		    ("fs_ii:convert_to_secondary(%p) unmount err %d\n",
		    this, error));
		return (error);
	}
	vfs = NULL;
	server_incn = 0;

	return (0);
}

//
// Helper function for converting from secondary to spare.
// This is called after the service is frozen and all IDL invocations
// have completed (new IDL invocations and _unreferenced() are blocked).
//
void
fs_ii::convert_to_spare()
{
	ASSERT(vfs == NULL);

	fobj_ii *p;

	//
	// We need to delete all fobj's and data since they won't get
	// _unreferenced().
	//
	// On PXFS secondary all fobj objects are on fidlist and fs_sec_fidhash
	// hash table, so we need to remove them from both.
	//

	fidlock.wrlock();
	fobj_ii_list_t::ListIterator iter(fidlist);
	while ((p = iter.get_current()) != NULL) {
		iter.advance();
		(void) fidlist.erase(p);
		p->convert_to_spare();
	}
	fidlock.unlock();

	fsmgr_server_ii *fsmgrp;
	while ((fsmgrp = servermgr_set.reapfirst()) != NULL) {
		delete fsmgrp;
	}

	delete this;
}

//
// Sync this file system on each pxfs client node.
//
void
fs_ii::sync_fs(cred_t *credp)
{
	solobj::cred_var	credobj = solobj_impl::conv(credp);
	fsmgr_server_ii		*fsmgrp;
	CORBA::Object_var	obj_v;
	Environment		e;

	prov_common_iter	fsmgr_iter(servermgr_set);
	while (!CORBA::is_nil(obj_v = fsmgr_iter.nextref())) {
		fsmgrp = fsmgr_server_ii::get_fsmgr_server_ii(obj_v);
		fsmgrp->get_clientmgr()->sync_filesystem(credobj, e);
		e.clear();
	}
}

void
fs_ii::revoke_completed(uint32_t server_incarn,
    Environment &_environment)
{
	if (server_incn != server_incarn) {
		//
		// This is a request for recovery for an incarnation
		// of the primary that is no longer the primary.
		//
		return;
	}
	fs_ii::revoke_completed_main();
}

void
fs_ii::revoke_completed_main()
{
	PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_AMBER,
	    ("fs::revoke_completed_main(%p) revoke_in_progress=%d\n",
	    this, revoke_in_progress));

	block_allocation.lock();

	//
	// If this is the last client to complete the revoke, inform
	// all the clients about the completion and change the status
	// of the server. The count being zero only indicates that
	// revoke tasks from *one* request to do revocation has
	// completed. There could be other tasks already active.
	//
	if (decrement_revoke() == 0) {
		PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_AMBER,
		    ("fs::revoke_completed_main(%p) Inform clients\n", this));

		fs_status = PXFS_VER::REDZONE;

		//
		// Inform all the clients that the switching to slowpath is
		// completed.
		//
		fsmgr_server_ii		*fsmgrp;
		CORBA::Object_var	obj_v;
		Environment		env;

		prov_common_iter	fsmgr_iter1(servermgr_set);
		while (!CORBA::is_nil(obj_v = fsmgr_iter1.nextref())) {
			fsmgrp = fsmgr_server_ii::get_fsmgr_server_ii(obj_v);
			fsmgrp->get_clientmgr()->inform_server_status(
			    fs_status, env);
			env.clear();
		}
		//
		// Checkpoint status change to secondary.
		//
		primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, env);

		if (is_replicated()) {
			get_ckpt()->ckpt_server_status(fs_status, env);
			ASSERT(env.exception() == NULL);
			env.clear();
		}
		env.trans_ctxp = NULL;
	}
	block_allocation.unlock();
}

void
fs_ii::get_reservation(PXFS_VER::blkcnt_t &refill_count,
    PXFS_VER::server_status_t &status,
    Environment &_environment)
{

	PXFS_VER::blkcnt_t total_allocated = 0;
	PXFS_VER::blkcnt_t current_blocks_free = 0;

	sol::nodeid_t src_node = _environment.get_src_node().ndid;

	block_allocation.lock();

	if ((fs_status == PXFS_VER::SWITCH_TO_REDZONE) ||
	    (fs_status == PXFS_VER::REDZONE)) {
		refill_count = 0;
		status = fs_status;
		block_allocation.unlock();
		return;
	}

	//
	// XXX is there a smarter way to tell the max number of nodes
	// in a cluster ?
	//
	for (int i = 1; i <= NODEID_MAX; i++)
		total_allocated += blocks_allocated[i];

	ASSERT(blocks_free >= total_allocated);
	current_blocks_free = blocks_free - total_allocated;

	//
	// XXX Need better heuristics here to allocate exponentially larger
	// amounts of disk space if one node is frequently requesting space.
	//
	// Compute the number of blocks that can be reserved for this node.
	//
	refill_count = initial_alloc_percentage * current_blocks_free/100;

	PXFS_DBPRINTF(PXFS_TRACE_FS, PXFS_AMBER,
	    ("fs_ii:get_res(%p) tot %lld, fr %lld, hi wm %lld, rfil %lld\n",
	    this, total_allocated, current_blocks_free,
	    blocks_free_high_w_mark, refill_count));

	//
	// Check if the current allocation causes the server to go into REDZONE.
	//
	if ((current_blocks_free - refill_count) <= blocks_free_high_w_mark) {
		//
		// Server entered REDZONE.
		//
		fs_status = PXFS_VER::SWITCH_TO_REDZONE;

		//
		// Checkpoint status change to secondary.
		//
		if (is_replicated()) {
			get_ckpt()->ckpt_server_status(fs_status, _environment);
			_environment.clear();
		}
		//
		// Clear the old reservation of all nodes.
		//
		bzero(blocks_allocated,
		    sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));

		PXFS_DBPRINTF(
		    PXFS_TRACE_FS,
		    PXFS_AMBER,
		    ("fs_ii:get_reservation(%p) Switching to REDZONE\n",
		    this));

		//
		// Launch the revoke_tasks which will revoke reservations
		// from all the clients.
		//
		launch_revoke_tasks();
		refill_count = 0;
		status = fs_status;
		block_allocation.unlock();
		return;
	}

	PXFS_DBPRINTF(
	    PXFS_TRACE_FS,
	    PXFS_AMBER,
	    ("fs_ii:get_reservation(%p) node=%d refill_count=%lld\n",
	    this, src_node, refill_count));

	blocks_allocated[src_node] += refill_count;


	//
	// Checkpoint space allocated to client node so that we can
	// reconstruct this allocation after a failover.
	//
	if (is_replicated()) {
		repl_pxfs_v1::blocks_allocated_t tmp_blocks_allocated;
		bcopy(blocks_allocated, tmp_blocks_allocated.blocks,
			sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));
		get_ckpt()->ckpt_blocks_allocated(tmp_blocks_allocated,
		    blocks_free, _environment);
		_environment.clear();
	}
	status = fs_status;
	block_allocation.unlock();
}

//
// This routine checks if we can switch back to fastpath ie.GREENZONE
//
PXFS_VER::server_status_t
fs_ii::switch_to_fastpath(Environment &_environment)
{
	PXFS_VER::server_status_t status;
	statvfs64_t statvfs_buf;

	block_allocation.lock();
	status = fs_status;

	if (vfs == NULL) {
		//
		// Filesystem has been unmounted, return EIO
		//
		_environment.exception(new sol::op_e(EIO));
		block_allocation.unlock();
		return (status);
	}

	//
	// We are already in the process of switching to GREEZONE.
	// Nothing more to do.
	//
	if (status == PXFS_VER::GREENZONE) {
		block_allocation.unlock();
		return (status);
	}
	//
	// Use VFS_STATVFS to compute the number of
	// free disk blocks and block size for the underlying
	// filesystem.
	//
	ASSERT(vfs != NULL);
	if (VFS_STATVFS(vfs, &statvfs_buf)) {
		// We don't expect VFS_STATVFS to fail here
		CL_PANIC("statvfs failed\n");
	}

	//
	// The numbers reported by VFS_STATVFS are in frags and not blocks.
	//
	uint64_t ratio = statvfs_buf.f_bsize/statvfs_buf.f_frsize;

	//
	// Check if the available blocks is more than low-watermark.
	// If yes, it is time to switch back to GREENZONE
	//
	if (!((statvfs_buf.f_bavail/ratio) > blocks_free_low_w_mark)) {
		status = fs_status;
		block_allocation.unlock();
		return (status);
	}

	//
	// Server can be in GREENZONE. Inform all the clients.
	//
	fs_status = PXFS_VER::GREENZONE;
	blocks_free = statvfs_buf.f_bavail / ratio;

	//
	// Clear the old reservation of all nodes.
	//
	bzero(blocks_allocated, sizeof (PXFS_VER::blkcnt_t) * (NODEID_MAX+1));

	Environment	env;

	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, env);

	if (is_replicated()) {
		repl_pxfs_v1::blocks_allocated_t tmp_blocks_allocated;
		bcopy(blocks_allocated, tmp_blocks_allocated.blocks,
		    sizeof (uint64_t) * (NODEID_MAX+1));
		get_ckpt()->ckpt_blocks_allocated(tmp_blocks_allocated,
		    blocks_free, env);
		ASSERT(env.exception() == NULL);
		env.clear();
		get_ckpt()->ckpt_server_status(fs_status, env);
		ASSERT(env.exception() == NULL);
		env.clear();
	}
	env.trans_ctxp = NULL;

	status = fs_status;
	block_allocation.unlock();
	return (status);
}

//
// fs_norm_impl methods
//

/* filesystem */

void
fs_norm_impl::getroot(PXFS_VER::fobj_out rootobj, PXFS_VER::fobj_info &rootinfo,
    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p, Environment &_environment)
{
	fs_ii::getroot(rootobj, rootinfo, binfo, client1_p,
	    client2_p, _environment);
} //lint !e1746

void
fs_norm_impl::get_statistics(sol::statvfs64_t &stat,
    Environment &_environment)
{
	fs_ii::get_statistics(stat, _environment);
}

void
fs_norm_impl::get_mntinfo(PXFS_VER::fs_info &info, CORBA::String_out mntoptions,
    Environment &_environment)
{
	fs_ii::get_mntinfo(info, mntoptions, _environment);
} //lint !e1746

void
fs_norm_impl::sync(int32_t syncflag, Environment &_environment)
{
	fs_ii::sync(syncflag, _environment);
}

void
fs_norm_impl::sync_recovery(uint32_t, Environment &)
{
	//
	// Non-HA file systems do not failover or switchover.
	// So there is nothing to do.
	//
}

void
fs_norm_impl::getfobj(const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_out fobj_o,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	fs_ii::getfobj(fobjid, fobj_o, fobjinfo, binfo,
	    client1_p, client2_p, _environment);
} //lint !e1746

//
// This is called  to get a new reference
//
void
fs_norm_impl::_generic_method(CORBA::octet_seq_t &,
    CORBA::object_seq_t &objs, Environment &e)
{
	objs[0] = get_objref();
}

PXFS_VER::fsmgr_server_ptr
fs_norm_impl::bind_fs(PXFS_VER::fsmgr_client_ptr clientmgr,
    sol::nodeid_t nodeid, uint32_t &server_incarn,
    uint32_t &fsbsize, bool &fastwrite_flag,
    Environment &_environment)
{
	return (fs_ii::bind_fs(clientmgr, nodeid, server_incarn, fsbsize,
	    fastwrite_flag, _environment));
}

void
fs_norm_impl::remount(PXFS_VER::fobj_ptr mntpnt, const sol::mounta &ma,
    solobj::cred_ptr credobj, uint32_t &vfsflags,
    CORBA::String_out mntoptions,
    Environment &_environment)
{
	fs_ii::remount(mntpnt, ma, credobj, vfsflags, mntoptions,
	    _environment);
} //lint !e1746

void
fs_norm_impl::unmount(int32_t flags, solobj::cred_ptr credobj,
    Environment &_environment)
{
	fs_ii::unmount(flags, credobj, _environment);
}

void
fs_norm_impl::set_nlm_status(int32_t nlmid, PXFS_VER::nlm_status status,
    Environment &_environment)
{
	fs_ii::set_nlm_status(nlmid, status, _environment);
}

void
fs_norm_impl::remove_file_locks(int32_t sysid,
    Environment &_environment)
{
	fs_ii::remove_file_locks(sysid, _environment);
}

void
fs_norm_impl::get_reservation(PXFS_VER::blkcnt_t &refill_block_count,
    PXFS_VER::server_status_t &status,
    Environment &_environment)
{
	fs_ii::get_reservation(refill_block_count, status, _environment);
}

void
fs_norm_impl::revoke_completed(uint32_t server_incarn,
    Environment &_environment)
{
	fs_ii::revoke_completed(server_incarn, _environment);
}

// end of methods supporting IDL operations

//
// fs_repl_impl methods
//

/* filesystem */

void
fs_repl_impl::getroot(PXFS_VER::fobj_out rootobj, PXFS_VER::fobj_info &rootinfo,
    PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p, Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	fs_ii::getroot(rootobj, rootinfo, binfo, client1_p,
	    client2_p, _environment);
} //lint !e1746

//
// This is called  to get a new reference.  Doing get_objref() here would
// get the highest reference version that was compiled.  We want the
// highest reference which is currently committed so we use this indirect
// way.
//
void
fs_repl_impl::_generic_method(CORBA::octet_seq_t &,
    CORBA::object_seq_t &objs, Environment &e)
{
	objs[0] = get_serverp()->get_root_obj(e);
}

void
fs_repl_impl::get_statistics(sol::statvfs64_t &stat,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::get_statistics(stat, _environment);
}

void
fs_repl_impl::get_mntinfo(PXFS_VER::fs_info &info, CORBA::String_out mntoptions,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::get_mntinfo(info, mntoptions, _environment);
} //lint !e1746

void
fs_repl_impl::sync(int32_t syncflag, Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::sync(syncflag, _environment);
}

void
fs_repl_impl::sync_recovery(uint32_t server_incarn,
	    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::sync_recovery(server_incarn, _environment);

}

void
fs_repl_impl::getfobj(const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_out fobj_o,
    PXFS_VER::fobj_info &fobjinfo,
    PXFS_VER::bind_info &binfo,
    PXFS_VER::fobj_client_ptr client1_p,
    PXFS_VER::fobj_client_out client2_p,
    Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	fs_ii::getfobj(fobjid, fobj_o, fobjinfo, binfo,
	    client1_p, client2_p, _environment);
} //lint !e1746

PXFS_VER::fsmgr_server_ptr
fs_repl_impl::bind_fs(PXFS_VER::fsmgr_client_ptr clientmgr,
    sol::nodeid_t nodeid, uint32_t &server_incarn,
    uint32_t &fsbsize, bool &fastwrite_flag,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return (PXFS_VER::fsmgr_server::_nil());
	}
	return (fs_ii::bind_fs(clientmgr, nodeid, server_incarn,
	    fsbsize, fastwrite_flag, _environment));
}

void
fs_repl_impl::remount(PXFS_VER::fobj_ptr mntpnt, const sol::mounta &ma,
    solobj::cred_ptr credobj, uint32_t &vfsflags,
    CORBA::String_out mntoptions,
    Environment &_environment)
{
	if (is_dead(_environment) || _freeze_in_progress(&_environment)) {
		return;
	}
	fs_ii::remount(mntpnt, ma, credobj, vfsflags, mntoptions,
	    _environment);
} //lint !e1746

void
fs_repl_impl::unmount(int32_t flags, solobj::cred_ptr credobj,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::unmount(flags, credobj, _environment);
}

void
fs_repl_impl::set_nlm_status(int32_t nlmid, PXFS_VER::nlm_status status,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::set_nlm_status(nlmid, status, _environment);
}

void
fs_repl_impl::remove_file_locks(int32_t sysid,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::remove_file_locks(sysid, _environment);
}

void
fs_repl_impl::get_reservation(PXFS_VER::blkcnt_t &refill_block_count,
    PXFS_VER::server_status_t &status,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}
	fs_ii::get_reservation(refill_block_count, status, _environment);
}

void
fs_repl_impl::revoke_completed(uint32_t server_incarn,
    Environment &_environment)
{
	if (is_dead(_environment)) {
		return;
	}

	fs_ii::revoke_completed(server_incarn, _environment);
}

// end of methods supporting IDL operations
