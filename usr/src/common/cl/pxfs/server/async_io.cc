//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)async_io.cc	1.10	08/05/20 SMI"

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/pathname.h>
#include <sys/debug.h>
#include <sys/uio.h>
#include <sys/file.h>
#include <sys/errno.h>
#include <sys/fcntl.h>
#include <sys/fdbuffer.h>

#include <sys/os.h>
#include <h/bulkio.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include PXFS_IDL(pxfs)
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/server/async_io.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// Ths classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

threadpool async_io_threadpool(true, 5, "asyncio", 25);

//
// The aiodone routine on the server side. Queue the completion to
// the pool of threads so the interrupt thread will not block.
// Note: this is called in the context of the interrupt thread.
//
extern "C" int
pxfs_aio_done(struct buf *bp)
{
	//
	// If bp is ever mapped in by the disk drivers or the md driver,
	// make sure that it is mapped out before we destroy bp.
	//
	if (bp->b_flags & B_REMAPPED) {
		bp_mapout(bp);
	}
	io_async *ios = (io_async *)bp->b_forw;
	ASSERT(ios != NULL);
	async_io_threadpool.defer_processing(ios);
	return (0);
}

//lint -e1401
io_async::io_async(aio_req_t *r, CORBA::Object_ptr cbk,
    bulkio::in_aio_ptr aobj)
{
	reqp = r;
	aiocbk = pxfs_aio::aio_callback::_narrow(cbk);
	ASSERT(aiocbk != NULL);
	inaioobj = bulkio::in_aio::_duplicate(aobj);
	type = AIO_RAW;
	pglistobj = NULL;
	aiopglistobj = NULL;
	owning_fsp = NULL;
}
//lint +e1401

//lint -e1740
io_async::~io_async()
{
	CORBA::release(aiocbk);
	if (inaioobj) {
		CORBA::release(inaioobj);
	}
	if (pglistobj) {
		CORBA::release(pglistobj);
	}
	if (aiopglistobj) {
		CORBA::release(aiopglistobj);
	}

	// Decrement aio count, signal waiter(s) if count is zero
	if (owning_fsp != NULL) {
		owning_fsp->decrement_aio_pending();
	}
}
//lint +e1740

//
// This is called when the pool of threads process the queued completed
// io request. Notify the client using the callback object.
// For read cases, pass the bulkio object since it has the
// data that is read from the disk. The unmarshal on the client
// will put the data in the user buffer on the client node.
//
void
io_async::execute()
{
	if (type == AIO_RAW) {
		aio_raw_execute();
	} else if (type == AIO_PAGEOUT) {
		aio_pageout_execute();
	} else if (type == AIO_PAGEIN) {
		aio_pagein_execute();
	} else {
		ASSERT(0);
	}
}

void
io_async::aio_raw_execute()
{
	int		err = 0;
	Environment	e;
	struct buf	*bp = &reqp->aio_req_buf;

	if (bp->b_flags & B_ERROR) {
		if (bp->b_error) {
			err = bp->b_error;
		} else {
			err = EIO;
		}
		PXFS_DBPRINTF(
		    PXFS_TRACE_ASYNC_IO,
		    PXFS_RED,
		    ("aio_raw_execute err %d\n",
		    err));
	}

	if ((bp->b_flags & B_WRITE) == 0) {
		aiocbk->aio_read_complete(inaioobj, err, e);
	} else {
		aiocbk->aio_write_complete(err, e);
	}
	if (e.exception()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_ASYNC_IO,
		    PXFS_RED,
		    ("aio_raw_execute aio_done exception ?\n"));
	}
	delete this;
}

//
// This is the asynchronous pageout completion code.
// The second ioasync constructor is called from async_page_out().
//
//lint -e1401 -e1541
io_async::io_async(bulkio::in_pages_ptr pglobj, CORBA::Object_ptr cbk,
		sol::u_offset_t full, sol::size_t done, fs_ii *fsp)
{
	pglistobj = bulkio::in_pages::_duplicate(pglobj);
	if (!CORBA::is_nil(cbk)) {
		aiocbk = pxfs_aio::aio_callback::_narrow(cbk);
	}
	start_len = full;
	done_len = done;
	error = 0;
	type = AIO_PAGEOUT;
	inaioobj = NULL;
	aiopglistobj = NULL;

	// The file system for which this aio is being scheduled
	owning_fsp = fsp;
	owning_fsp->increment_aio_pending();
}

//
// This is the constructor for asynchronous pagein.
// This is called only from async_page_in().
//
io_async::io_async(bulkio::in_aio_pages_ptr pglobj, CORBA::Object_ptr cbk,
    fdbuffer_t *fdb, u_offset_t off)
{

	aiopglistobj = bulkio::in_aio_pages::_duplicate(pglobj);
	if (!CORBA::is_nil(cbk)) {
		aiocbk = pxfs_aio::aio_callback::_narrow(cbk);
	}
	start_len = 0;
	done_len = 0;
	error = 0;
	fdbuf = fdb;
	type = AIO_PAGEIN;
	inaioobj = NULL;
	pglistobj = NULL;
	offset = off;
	owning_fsp = NULL;
}
//lint +e1401 +e1541

//
// This is equivalent to iodone() routine called from fdbuf layer.
// Called from interrupt context for pageout completions.
// So cannot do any kind of blocking calls here.
// Debug print statements can do memory allocations,
// and hence are not allowed here.
//
extern "C" void
vn_fsio_done(fdbuffer_t *fdb, void *ios_obj, struct buf *)
{
	int		error;

	io_async	*ios = (io_async *)ios_obj;

	if ((error = fdb_get_error(fdb)) != 0) {
		ios->set_error(error);
	}

	// defer_processing() will call the execute() function.
	async_io_threadpool.defer_processing(ios);
}

//
// This function is called by defer_processing() routines
// of the threadpool.
//
void
io_async::aio_pageout_execute()
{
	Environment	e;

	PXFS_DBPRINTF(
	    PXFS_TRACE_ASYNC_IO,
	    error ? PXFS_RED : PXFS_GREEN,
	    ("aio_pageout_execute off %llx len %llx err %d\n",
	    start_len, done_len, error));

	//
	// XXX: start_len and done_len should be passed back properly,
	// without loss of precision. IDL changes will be needed for that.
	// Guess: This is working for larger than 4G file now because the
	// completion routine interprets the wrong value from the 32bit
	// cast as a fragment and processes the result accordingly.
	//
	aiocbk->aio_pageout_complete((uint32_t)start_len,
				(uint32_t)done_len, error, e);
	if (e.exception()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_ASYNC_IO,
		    PXFS_RED,
		    ("aio_pageout_execute exception\n"));
	}
	delete this;
}

void
io_async::aio_pagein_execute()
{
	Environment e;

	PXFS_DBPRINTF(
	    PXFS_TRACE_ASYNC_IO,
	    error ? PXFS_RED : PXFS_GREEN,
	    ("aio_pagein_execute %d\n",
	    error));

	bulkio::file_hole_list fhl;

	ASSERT(fdbuf != NULL);

	fdb_holes_t *fdh, *fdh1;

	if ((fdh = fdb_get_holes(fdbuf)) != NULL) {

		// Count the holes.
		uint_t cnt = 0;

		fdh = fdb_get_holes(fdbuf);
		ASSERT(fdh);

		for (fdh1 = fdh; fdh1; fdh1 = fdh1->next_hole)
			cnt++;

		fhl->length(cnt);
		bulkio::file_hole *fdh_list = fhl->buffer();
		uint_t i = 0;
		for (fdh1 = fdh; fdh1; fdh1 = fdh1->next_hole, i++) {
			fdh_list[i].off = fdh1->off + offset;
			fdh_list[i].len = fdh1->len;

			PXFS_DBPRINTF(
			    PXFS_TRACE_ASYNC_IO,
			    PXFS_GREEN,
			    ("aio_pagein_execute(%p) hole off %llx len %lx\n",
			    this, fdh_list[i].off, fdh_list[i].len));
		}
	}

	aiocbk->aio_pagein_complete(aiopglistobj, fhl, error, e);

	if (e.exception()) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_ASYNC_IO,
		    PXFS_RED,
		    ("aio_pagein_execute exception\n"));
	}
	delete this;
}
