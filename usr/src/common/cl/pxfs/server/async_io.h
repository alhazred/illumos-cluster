//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef ASYNC_IO_H
#define	ASYNC_IO_H

#pragma ident	"@(#)async_io.h	1.8	08/05/20 SMI"

#include "../version.h"

#include <sys/aio_impl.h>

#include <h/px_aio.h>
#include <sys/list_def.h>
#include <sys/threadpool.h>
#include <orb/object/adapter.h>

#include PXFS_IDL(pxfs)
#include <pxfs/server/fobj_impl.h>

//
// Class for async IO completed requests. Stores the
// inout uioobj for read cases and in uio obj for write case.
// also has the callback object.
//
class io_async : public defer_task {
public:
	~io_async();
	void execute();
	void aio_raw_execute();
	void aio_pageout_execute();
	void aio_pagein_execute();
	io_async(aio_req_t *, CORBA::Object_ptr, bulkio::in_aio_ptr);

	io_async(bulkio::in_pages_ptr, CORBA::Object_ptr, sol::u_offset_t,
		sol::size_t, fs_ii *);

	io_async(bulkio::in_aio_pages_ptr, CORBA::Object_ptr, fdbuffer_t *,
	    u_offset_t);
	aio_req_t *get_reqp() { return reqp; }
	void set_error(int err) { error = err; }
	enum { AIO_RAW = 1, AIO_PAGEOUT = 2, AIO_PAGEIN = 3};
private:
	aio_req_t *reqp;
	pxfs_aio::aio_callback_ptr aiocbk;
	bulkio::in_aio_ptr inaioobj;
	int type;

	bulkio::in_pages_ptr pglistobj;
	sol::u_offset_t start_len;
	sol::size_t done_len;
	int error;

	bulkio::in_aio_pages_ptr aiopglistobj;
	fdbuffer *fdbuf;
	u_offset_t offset;

	// The filesystem on which this aio is being done
	fs_ii	*owning_fsp;
};

#endif	// ASYNC_IO_H
