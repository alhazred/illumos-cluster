//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// unixdir_ckpt.h - fs::unixdir checkpointing
//

#ifndef UNIXDIR_CKPT_H
#define	UNIXDIR_CKPT_H

#pragma ident	"@(#)unixdir_ckpt.h	1.10	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <repl/service/transaction_state.h>

#include "../version.h"
#include PXFS_IDL(pxfs)
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/unixdir_impl.h>
#include <pxfs/server/repl_pxfs_server.h>

//
// This is the class for the unixdir checkpointing object.
// On the original primary, we don't have any remote state, so we only
// use the static methods.
// On the secondary / new primary we hold the state from the original primary
// in the state object.
//
class unixdir_state : public transaction_state {
public:
	enum progress {START, CHECKPOINTED, COMMITTED};

	// Constructor for normal operations.
	unixdir_state(bool _exists);

	// Constructor when file object existance must be recorded
	unixdir_state(PXFS_VER::fobj_ptr obj, const PXFS_VER::fobj_info &finfo);

	// Constructor when file object existance must be recorded
	unixdir_state(PXFS_VER::fobj_ptr obj, const PXFS_VER::fobj_info &finfo,
	    uint64_t deletion_id);

	// Constructor to checkpoint and commit an error.
	unixdir_state(sol::error_t err);

	// Destructor.
	virtual ~unixdir_state();

	//
	// Routines to implement the transaction_state template.
	// These can happen asynchronously to checkpoints so locking is
	// required.
	//
	virtual void	orphaned(Environment &e);
	virtual void	committed();

	// Return true if the checkpoint state says the file exists
	bool		file_exists() const;

	// Get a new reference to the target fobj.
	PXFS_VER::fobj_ptr	get_target_fobj();

	// Get and set the error value.
	sol::error_t	get_error() const;
	void		ckpt_error_return(sol::error_t err);

	// Get the Id used when renaming a file object about to be removed
	uint64_t	get_delete_id();

	// Get a new reference to the return fobj.
	PXFS_VER::fobj_ptr	get_ret_fobj();

	// Get and set the return fobjinfo
	void		get_fobjinfo(PXFS_VER::fobj_info &finfo);
	void		set_fobjinfo(const PXFS_VER::fobj_info &finfo);

	// Handle checkpoint (commit) after creating a file.
	void		ckpt_fobj_return(PXFS_VER::fobj_ptr fobjp,
			    const PXFS_VER::fobj_info &finfo);

	// Handle checkpoint (commit) after deleting a file.
	void		ckpt_delete_fobj(uint64_t deletion_id);

	//
	// Return true if the original primary server did the operation
	// successfully.
	//
	bool		server_did_op(int op_err, bool exists_after_op);

	//
	// Obtains the unixdir transaction state if this is a retry.
	// This function returns the progress (START, CHECKPOINTED, COMMITTED).
	//
	static progress		get_unixdir_state(Environment &env,
	    unixdir_state *&statep_ret);

	//
	// This function should be called by the primary before creating
	// a file. It will do a VOP_LOOKUP() and checkpoint the existence
	// of a file.
	//
	static void		do_ckpt_entry(unixdir_ii *udirp, const char *nm,
	    cred_t *credp, Environment &env);

private:
	// Current state of progress in mini-transation.
	progress	cur_state;

	// Checkpointed existence of name to be operated on in this directory.
	bool		exists;

	// Pointer to the file or directory to be operated on in this directory.
	PXFS_VER::fobj_var	targetobj;

	// Error return value, if any
	int		error;

	// Identifier to use when renaming a file
	uint64_t	delete_id;

	// Object return value, if any
	PXFS_VER::fobj_var	retobj;
	PXFS_VER::fobj_info	retinfo;
};

#include <pxfs/server/unixdir_ckpt_in.h>

#endif	// UNIXDIR_CKPT_H
