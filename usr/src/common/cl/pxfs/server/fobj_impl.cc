//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fobj_impl.cc	1.21	09/01/25 SMI"

#include <sys/errno.h>
#include <sys/types.h>
#include <sys/thread.h>
#include <sys/file.h>
#include <sys/sysmacros.h>
#include <sys/conf.h>
#include <sys/systm.h>
#include <sys/vfs.h>
#include <sys/fcntl.h>
#include <sys/flock.h>
#include <sys/share.h>
#include <sys/copyops.h>
#include <sys/acl.h>
#include <sys/flock_impl.h>
#include <sys/dnlc.h>
#include <sys/scsi/impl/uscsi.h>
#include <sys/filio.h>

#include <sys/sol_version.h>
#include <sys/os.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>
#include <orb/infrastructure/copy.h>
#include <orb/infrastructure/orb_conf.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/client/pxnode.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/server/unixdir_impl.h>
#include <pxfs/server/symlink_impl.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/fsmgr_server_impl.h>
#include <pxfs/server/fs_base_impl.h>

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//

//lint -e666
// PXFS is an extensive user of the inline function get_vp() within
// the vnode macros:
//	error = VOP_LOOKUP(get_vp(), ... );
// There are no side effects to calling get_vp() repeatedly, flexelint
// does not know that, but we do.
//

//
// Constructor for unreplicated or primary object.
//
fobj_ii::fobj_ii(fs_base_ii *fsp, vnode_t *vnodep, PXFS_VER::fobj_type_t ftype,
    fid_t *fidp) :
	_DList::ListElem(this),
	delete_num(0),
	fs_implp(fsp),
	fr_lockp(NULL),
	fobjtype(ftype),
	primary_ready(true),
	fobj_released(false),
	processing_unref(false)
{
	ASSERT(fs_implp != NULL);
	fs_implp->hold();

	back_obj.vnodep = vnodep;
	VN_HOLD(back_obj.vnodep);

	ASSERT(fidp != NULL);
	back_obj.fidp = (fid_t *)kmem_alloc(fidp->fid_len + sizeof (ushort_t),
	    KM_SLEEP);
	bcopy(fidp->fid_data, back_obj.fidp->fid_data, (size_t)fidp->fid_len);
	back_obj.fidp->fid_len = fidp->fid_len;

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJ,
	    PXFS_GREEN,
	    ("fobj_ii(%p) ftype %d primary\n",
	    this, ftype));

#ifdef PXFS_KSTATS_ENABLED
	// Update kstat count for various fobj types
	if ((ftype != PXFS_VER::fobj_io) && (ftype != PXFS_VER::fobj_device)) {
		((fs_ii *)fsp)->kstat_fobj_type_increment(ftype);
	}
#endif
}

//
// Constructor on the secondary.
//
fobj_ii::fobj_ii(fs_base_ii *fsp, const PXFS_VER::fobjid_t &fobjid,
    PXFS_VER::fobj_type_t ftype) :
	_DList::ListElem(this),
	delete_num(0),
	fs_implp(fsp),
	fr_lockp(NULL),
	fobjtype(ftype),
	primary_ready(false),
	fobj_released(false),
	processing_unref(false)
{
	ASSERT(fs_implp != NULL);
	fs_implp->hold();

	back_obj.vnodep = NULL;
	//
	// Make a copy of the fobjid.
	//
	size_t fidlen = fobjid->length();
	ASSERT(fidlen > 0 && fidlen <= MAXFIDSZ);
	ASSERT(fidlen <= MAXFIDSZ);
	back_obj.fidp = (fid_t *)kmem_alloc(fidlen + sizeof (ushort_t),
	    KM_SLEEP);
	back_obj.fidp->fid_len = (ushort_t)fidlen;
	bcopy(fobjid->buffer(), back_obj.fidp->fid_data, fidlen);

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJ,
	    PXFS_GREEN,
	    ("fobj_ii(%p) ftype %d secondary\n",
	    this, ftype));

#ifdef PXFS_KSTATS_ENABLED
	// Update kstat count for various fobj types
	if ((ftype != PXFS_VER::fobj_io) && (ftype != PXFS_VER::fobj_device)) {
		((fs_ii *)fsp)->kstat_fobj_type_increment(ftype);
	}
#endif
}

fobj_ii::~fobj_ii()
{
#ifdef PXFS_KSTATS_ENABLED
	// Update kstat count for interested fobj types
	if ((fobjtype != PXFS_VER::fobj_io) &&
	    (fobjtype != PXFS_VER::fobj_device)) {
		((fs_ii *)get_fsp())->kstat_fobj_type_decrement(fobjtype);
	}
#endif

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJ,
	    PXFS_GREEN,
	    ("~fobj_ii(%p)\n", this));

	// On the secondary object, we need to cleanup the active locks.
#ifdef	DEBUG
	if (primary_ready && fr_lockp != NULL) {
		ASSERT(!fr_lockp->has_active_locks());
	}
#endif
	delete fr_lockp;

	fs_implp->rele();

	// Free file ID memory.
	if (back_obj.fidp != NULL) {
		kmem_free(back_obj.fidp, back_obj.fidp->fid_len +
		    sizeof (ushort_t));
		back_obj.fidp = NULL;
	}

	if (!primary_ready) {
		// This must be a released or secondary object.
		return;
	}

	//
	// Normally fs_ii::unmount() or _unreferenced() should have
	// cleared this but we handle the case where a newly created
	// fobj is deleted in {fs_ii,device_server_ii}::find_fobj().
	//
	if (back_obj.vnodep != NULL) {
		//
		// pxfs is done with this vnode, and we would like to
		// purge it from the dnlc. But due to the implementation
		// of dnlc_purge_vp(), this is prohibitively expensive. So
		// for now, we leave the entry for the dnlc code to clean
		// up later.
		//
		VN_RELE(back_obj.vnodep);
	}
}

//
// unreferenced - we know that no more CORBA references can be handed out.
//
// PXFS uses the unreferenced processing to complete the removal of a
// file that was renamed to the special directory for files being removed.
//
// The system can safely destroy an HA file object that has not yet undergone
// lazy FID conversion as long as the file is not being removed.
//
// At the completion of this processing, if the file object is no longer in use,
// the system destroys the file object.
//
void
fobj_ii::unreferenced(int bktno, bool fidlist_locked)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJ,
	    PXFS_GREEN,
	    ("fobj_ii:unref(%p)\n", this));

	if (delete_num && !primary_ready && !fobj_released &&
	    !fs_implp->is_secondary()) {
		//
		// In order to complete the file deletion process,
		// the file object must be in the primary ready state.
		// Specifically, PXFS needs the vnode of the underlying
		// file system.
		//
		ASSERT(is_replicated());
		ASSERT(fs_implp->fidlock.lock_held());

		int		error;
		vnode_t		*vnodep = fid_to_vnode(error);

		if (vnodep == NULL) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJ,
			    PXFS_RED,
			    ("unreferenced(%p) no vnode error %d\n",
			    this, error));
		} else {
			int	buketno = fs_implp->vp_to_bucket(vnodep);
			if (bktno != INT_MAX) {
				ASSERT(bktno == buketno);
			} else {
				bktno = buketno;
				fs_implp->fobjlist_locks[bktno].wrlock();
			}

			// Transfer the hold on the vnode to the file object
			ASSERT(back_obj.vnodep == NULL);
			back_obj.vnodep = vnodep;

			get_fsp()->remove_from_fidlist(this);
			bool	added = get_fsp()->add_to_list(this);
			ASSERT(added);

			//
			// We know that there are no clients.
			// So there is no client state to recover.
			// FID conversion is done at failover/switchover time
			// if any locks are held. So we know there are no locks.
			//
			primary_ready = true;
		}
	}

	// On the primary object, we need to cleanup locks.
	if (primary_ready) {
		processing_unref = true;
		if (fr_lockp != NULL) {
			fr_lockp->remove_all_locks();
		}
		processing_unref = false;
	}

	//
	// When fobj_released is true, unmount has already
	// removed the file object from any file system list,
	// either the fid or allfobj list, and will call do_delete.
	// In this case no further work is needed.
	//
	// When fobj_released is false, we remove
	// the file object from any file system list, either the fid
	// or allfobj list. We also call do_delete after releasing the
	// appropriate lock(s).
	// In the case where fobj_released is false the call to
	// remove_fobj_locked will call cleanup_and_get_release_info() which
	// will set fobj_released to true.
	//
	release_info_t	rinfo;
	bool		do_release = !fobj_released;
	if (do_release) {
		//
		// Upon return from this call fobj_released will be true
		//
		fs_implp->remove_fobj_locked(this, rinfo);
	}
	if (bktno != INT_MAX) {
		ASSERT(fs_implp->fobjlist_locks[bktno].write_held());
		fs_implp->fobjlist_locks[bktno].unlock();
	}
	if (fidlist_locked) {
		ASSERT(fs_implp->fidlock.write_held());
		fs_implp->fidlock.unlock();
	}
	if (do_release) {
		do_delete(fs_implp, rinfo);
	}
	delete this;
}

//
// Helper function for fs_base_ii::remove_fobj_locked()
// to release the underlying vnode without waiting for _unreferenced().
// The work is split into two parts to avoid holding locks while calling
// do_delete() below. The constraint is that the fobj has to be either
// deleted or checkpointed in dump_state() yet we don't want to hold
// the lock which synchronizes these two things while we call VN_RELE()
// or delete the file. So, we return the info with the lock held,
// delete the fobj, unlock, then call VN_RELE() and do_delete().
//
void
fobj_ii::cleanup_and_get_release_info(release_info_t &rinfo)
{
	ASSERT(primary_ready);
	ASSERT(back_obj.vnodep != NULL);
	ASSERT(!fobj_released);
	ASSERT(delete_num == 0 ||
	    is_replicated() && get_ftype() != PXFS_VER::fobj_io);

	PXFS_DBPRINTF(
	    PXFS_TRACE_FOBJ,
	    PXFS_GREEN,
	    ("fobj_ii:get_release_info(%p) ftype %d delete_num %llu\n",
	    this, get_ftype(), delete_num));

	//
	// Cleanup all the file locks for this vnode.  Do this here because
	// the LLM holds pointers to the fobj we are about to destroy.
	// See BugId 4363259.
	//
	if (fr_lockp != NULL) {
		fr_lockp->remove_all_locks();
	}

	// Return the underlying vnode (transferring our hold to rinfo).
	rinfo.vp = back_obj.vnodep;
	rinfo.delete_fidp = back_obj.fidp;
	rinfo.delete_num = delete_num;
	rinfo.ftype = fobjtype;

	//
	// If delete_num is non zero increment the count of number of objects
	// for which do_delete() is still pending. This count will get
	// decremented in fobj_ii::do_delete().
	//
	if (delete_num != 0) {
		get_fsp()->inc_dodelete_cnt();
	}
	back_obj.vnodep = NULL;
	back_obj.fidp = NULL;
	primary_ready = false;
	fobj_released = true;
}

//
// Helper function to remove a file from the deleted directory
// after the last reference has been released (only applies to
// replicated file systems).
//
// static
void
fobj_ii::do_delete(fs_base_ii *fsbp, const release_info_t &rinfo)
{
	if (rinfo.vp != NULL) {
		//
		// pxfs is done with this vnode, and we would like to
		// purge it from the dnlc.  But due to the implementation
		// of dnlc_purge_vp(), this is prohibitively expensive. So
		// for now, we leave the entry for the dnlc code to clean
		// up later.
		//
		VN_RELE(rinfo.vp);
	}
	if (rinfo.delete_num == 0) {
		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_FOBJ_DEL_B, FaultFunctions::generic);

	// Assertion checks that the cast of fsbp is safe.
	ASSERT(rinfo.ftype != PXFS_VER::fobj_io);
	fs_ii	*fsp = (fs_ii *)fsbp;

	int	error;
	char	tbuf[21];	// Needs to hold uint64_t
	os::sprintf(tbuf, "%lld", rinfo.delete_num);
	if (rinfo.ftype == PXFS_VER::fobj_unixdir) {
#if	SOL_VERSION >= __s11
		error = VOP_RMDIR(fsp->get_deleted(), tbuf, NULL, kcred,
		    NULL, 0);
#else
		error = VOP_RMDIR(fsp->get_deleted(), tbuf, NULL, kcred);
#endif
	} else {
#if	SOL_VERSION >= __s11
		error = VOP_REMOVE(fsp->get_deleted(), tbuf, kcred, NULL, 0);
#else
		error = VOP_REMOVE(fsp->get_deleted(), tbuf, kcred);
#endif
	}
	if (error == 0) {
		error = fsp->get_fs_dep_implp()->fs_fsync(fsp->get_deleted(),
		    kcred);
	}
	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJ,
		    PXFS_RED,
		    ("fobj_ii:do_delete '%s' ftype %d error %d\n",
		    tbuf, rinfo.ftype, error));
	}

	//
	// Decrement the count of objects for which delete_num is non zero and
	// do_delete() is pending.
	//
	fsp->dec_dodelete_cnt();

	FAULTPT_PXFS(FAULTNUM_PXFS_FOBJ_DEL_A, FaultFunctions::generic);
}

//
// Get fobj_ii from the fobj
// This returns NULL if the input objp is NULL.
// When the object was constructed, we set the handler's cookie to point
// to the fobj_ii.
//
// WARNING: this method accesses the CORBA object directly. On accessing
// fobj_ii's on the primary, it is necessary to check if this is a
// replicated file system and if so a call must be made to
// is_dead(). This is because the fobj may require just-in-time
// FID conversion.
//
// The code should look like the following:
//
//	fobj_ii *fobj_iip;
//	fobj_iip = fobj_ii::get_fobj_ii(PXFS_VER::fobj_ptr objp)
//	if (is_replicated()) {
//		fobj_iip->is_dead(env);
//	}
//
// There are numerous examples in server/unixdir_impl.cc
//
fobj_ii *
fobj_ii::get_fobj_ii(PXFS_VER::fobj_ptr objp)
{
	if (CORBA::is_nil(objp)) {
		return (NULL);
	}
	void *p = objp->_handler()->get_cookie();
	return ((fobj_ii *)p);
}

// Returns whether the client could cache information
bool
fobj_ii::can_cache()
{
	return (false);
}

//
// Return a CORBA pointer (no CORBA::release() required)
// to the I/O checkpoint interface.
//
repl_dc::repl_dev_server_ckpt_ptr
fobj_ii::get_io_ckpt()
{
	ASSERT(0);
	return (repl_dc::repl_dev_server_ckpt::_nil());
}

//
// downgrade_attr_all
// The default is that attributes are not cached,
// and in this case there is nothing to invalidate.
//
sol::error_t
fobj_ii::downgrade_attr_all(PXFS_VER::attr_rights, bool, nodeid_t)
{
	return (0);
}

void
fobj_ii::shrlock(int32_t cmd, sol::shrlock_t &lock_info, int32_t flag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	struct shrlock to_linfo;

	//
	// Copy fields of lock_info to to_linfo, so that we have what amounts
	// to the original struct.
	//
	shr_unflatten(lock_info, &to_linfo);

#if	SOL_VERSION >= __s11
	cred_t	*credp = solobj_impl::conv(credobj);
	int error = VOP_SHRLOCK(back_obj.vnodep, cmd, &to_linfo, flag, credp,
	    NULL);
#else
#if	SOL_VERSION >= __s10
	cred_t	*credp = solobj_impl::conv(credobj);
	int error = VOP_SHRLOCK(back_obj.vnodep, cmd, &to_linfo, flag, credp);
#else
	int error = VOP_SHRLOCK(back_obj.vnodep, cmd, &to_linfo, flag);
#endif
#endif
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
	//
	// If fields "access", "deny", "sysid", or "pid" changed during
	// invocation, copy their values back to "lock_info."  This simulates
	// an in-out parameter.
	//
	shr_copyback(&to_linfo, lock_info);
}

//
// Remove locks that might be held by the given pid and sysid.
// This is needed since the code in the underlying file system that calls
// cleanlocks() during VOP_CLOSE() uses the pid of the calling thread, which
// is the pid of the thread handling this remote invocation rather than the
// the pid of the process on the client node which actually has the lock.
//
void
fobj_ii::remove_locks(sol::pid_t ownerpid, sol::lsysid_t sysid,
    solobj::cred_ptr credobj, Environment &_environment)
{
	struct shrlock	linfo;

	// Use zero if the lock is local; otherwise, use the given sysid.
	int lsysid = (pxfslib::get_nodeid(sysid) == orb_conf::node_number()) ?
	    0 : (int)sysid;

	if (fr_lockp != NULL) {
		fr_lockp->start_locking();
		cleanlocks(back_obj.vnodep, ownerpid, lsysid);
		fr_lockp->end_locking(_environment);
	} else {
		//
		// Make it so that either the NLM-PXFS cleanup is in
		// progress OR locking on individual files is in progress,
		// but never both. See start_locking() for more details.
		//
		fs_base_ii::per_node_lock.rdlock();

		//
		// Make it so that either the LLM locks from a particular
		// node are being cleaned up (via fsmgr::unreferenced()),
		// OR locking on individual files is in progress,
		// but never both. See start_locking() for more details
		//
		fs_implp->llm_lock.rdlock();

		cleanlocks(back_obj.vnodep, ownerpid, lsysid);
		fs_implp->llm_lock.unlock();
		fs_base_ii::per_node_lock.unlock();
	}

	linfo.s_access = 0;
	linfo.s_deny = 0;
	linfo.s_sysid = lsysid;
	linfo.s_pid = ownerpid;
	linfo.s_own_len = 0;
	linfo.s_owner = NULL;

#if	SOL_VERSION >= __s11
	cred_t	*credp = solobj_impl::conv(credobj);
	(void) VOP_SHRLOCK(back_obj.vnodep, F_UNSHARE, &linfo, FREAD | FWRITE,
	    credp, NULL);
#else
#if	SOL_VERSION >= __s10
	cred_t	*credp = solobj_impl::conv(credobj);
	(void) VOP_SHRLOCK(back_obj.vnodep, F_UNSHARE, &linfo, FREAD | FWRITE,
	    credp);
#else
	(void) VOP_SHRLOCK(back_obj.vnodep, F_UNSHARE, &linfo, FREAD | FWRITE);
#endif
#endif
}

//
// fobj_ii::access - check access permissions on a fobj object.
//
void
fobj_ii::access(int32_t accmode, int32_t accflags, solobj::cred_ptr credobj,
    Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);
#if	SOL_VERSION >= __s11
	int	error = VOP_ACCESS(back_obj.vnodep, accmode, accflags, credp,
	    NULL);
#else
	int	error = VOP_ACCESS(back_obj.vnodep, accmode, accflags, credp);
#endif
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// fobj_ii::set_attributes - set the attributes of a fobj object.
// This is used when attribute caching is disabled on the pxfs client.
//
void
fobj_ii::set_attributes(const sol::vattr_t &attr, int32_t attrflags,
    solobj::cred_ptr credobj, Environment &_environment)
{
	vattr	*vattrp = (vattr *)&attr;
	cred_t	*credp = solobj_impl::conv(credobj);
	vnode_t	*vnodep = get_vp();

	//
	// File truncates are handled from pxreg::setattr() which calls
	// truncate() rather than here.
	//
	ASSERT(get_ftype() != PXFS_VER::fobj_file ||
	    (vattrp->va_mask & AT_SIZE) == 0);

	attr_lock.wrlock();
	int	error;
#if SOL_VERSION >= __s10
	error = VOP_SETATTR(vnodep, vattrp, attrflags, credp, NULL);
#else
	error = VOP_SETATTR(vnodep, vattrp, attrflags, credp);
#endif
	if (error == 0) {
		error = get_fsp()->get_fs_dep_implp()->fs_fsync(vnodep, credp);
	}
	attr_lock.unlock();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// fobj_ii::get_attributes - get the attributes of a fobj object.
// This is used when attribute caching is disabled on the pxfs client.
//
void
fobj_ii::get_attributes(uint32_t attrmask, sol::vattr_t &attr,
    solobj::cred_ptr credobj, Environment &_environment)
{
	vattr	*vattrp = (vattr *)&attr;
	cred_t	*credp = solobj_impl::conv(credobj);

	vattrp->va_mask = attrmask;

	attr_lock.rdlock();

#if	SOL_VERSION >= __s11
	int	error = VOP_GETATTR(back_obj.vnodep, vattrp, 0, credp, NULL);
#else
	int	error = VOP_GETATTR(back_obj.vnodep, vattrp, 0, credp);
#endif

	//
	// This is a workaround for bug id 4303169. We check to see if the
	// vnode is a special device vnode. If so, we null out the nanosec
	// time values in keeping with Solaris specfs.
	//
	if (error == 0) {
		if (IS_SPECIAL(back_obj.vnodep)) {
			vattrp->va_atime.tv_nsec = 0;
			vattrp->va_mtime.tv_nsec = 0;
			vattrp->va_ctime.tv_nsec = 0;
		}
	}

	attr_lock.unlock();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// fobj_ii::set_secattributes - set the secure attributes (ACLs)
// of a fobj object.
// This is used when attribute caching is disabled on the pxfs client.
//
void
fobj_ii::set_secattributes(const PXFS_VER::secattr &sattr, int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	vsecattr_t	vsec;
	int		error;
	vnode_t		*vnodep = get_vp();

	vsec.vsa_mask = sattr.mask;
	if (sattr.mask & VSA_ACL) {
		vsec.vsa_aclcnt = (int)sattr.acl_ent.length();
		vsec.vsa_aclentp = sattr.acl_ent.buffer();
	} else {
		vsec.vsa_aclcnt = 0;
		vsec.vsa_aclentp = NULL;
	}
	if (sattr.mask & VSA_DFACL) {
		vsec.vsa_dfaclcnt = (int)sattr.acl_def_ent.length();
		vsec.vsa_dfaclentp = sattr.acl_def_ent.buffer();
	} else {
		vsec.vsa_dfaclcnt = 0;
		vsec.vsa_dfaclentp = NULL;
	}

	attr_lock.wrlock();

#if	SOL_VERSION >= __s10
	VOP_RWLOCK(vnodep, V_WRITELOCK_TRUE, NULL);
#else
	VOP_RWLOCK(vnodep, 1);
#endif
#if	SOL_VERSION >= __s11
	error = VOP_SETSECATTR(vnodep, &vsec, secattrflag, credp, NULL);
#else
	error = VOP_SETSECATTR(vnodep, &vsec, secattrflag, credp);
#endif
#if	SOL_VERSION >= __s10
	VOP_RWUNLOCK(vnodep, V_WRITELOCK_TRUE, NULL);
#else
	VOP_RWUNLOCK(vnodep, 1);
#endif
	if (error == 0) {
		error = get_fsp()->get_fs_dep_implp()->fs_fsync(vnodep, credp);
	}
	attr_lock.unlock();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// fobj_ii::get_secattr_cnt - get the number of secure attributes (ACLs)
// of a fobj object.
//
void
fobj_ii::get_secattr_cnt(int32_t &aclcnt, int32_t &dfaclcnt,
    int32_t secattrflag, solobj::cred_ptr credobj, Environment &_environment)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	vsecattr_t	vsec;
	int		error;

#ifdef FSI
	if (vn_matchopval(back_obj.vnodep, VOPNAME_GETSECATTR, NULL)) {
#else
	if (back_obj.vnodep->v_op->vop_getsecattr == NULL) {
#endif
		pxfslib::throw_exception(_environment, ENOSYS);
		return;
	}

	bzero(&vsec, sizeof (vsecattr_t));
	vsec.vsa_mask = VSA_ACLCNT | VSA_DFACLCNT;

	//
	// Note: acquiring the attribute_lock isn't needed here since
	// the underlying file system will do whatever locking
	// is needed to return a consistent result.
	//
#if	SOL_VERSION >= __s11
	error = VOP_GETSECATTR(back_obj.vnodep, &vsec, secattrflag, credp,
	    NULL);
#else
	error = VOP_GETSECATTR(back_obj.vnodep, &vsec, secattrflag, credp);
#endif

	//lint -e571
	if ((aclcnt = vsec.vsa_aclcnt) != 0 && vsec.vsa_aclentp) {
		kmem_free(vsec.vsa_aclentp,
		    (size_t)vsec.vsa_aclcnt * sizeof (aclent_t));
	}
	if ((dfaclcnt = vsec.vsa_dfaclcnt) != 0 && vsec.vsa_dfaclentp) {
		kmem_free(vsec.vsa_dfaclentp,
		    (size_t)vsec.vsa_dfaclcnt * sizeof (aclent_t));
	}
	//lint +e571

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// fobj_ii::get_secattributes - get the secure attributes (ACLs)
// of a fobj object
//
//lint -e1746
void
fobj_ii::get_secattributes(PXFS_VER::secattr_out sattr, int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &_environment)
{
	get_secattr_common(sattr, secattrflag, credobj, _environment);
}
//lint +e1746

//
// get_secattr_common
// Common code for fobj_ii and fobjplus
//
void
fobj_ii::get_secattr_common(PXFS_VER::secattr_out sattr, int32_t secattrflag,
    solobj::cred_ptr credobj, Environment &env)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	vsecattr_t	vsec;
	int		error;

	if (back_obj.vnodep->v_op->vop_getsecattr == NULL) {
		pxfslib::throw_exception(env, ENOSYS);
		return;
	}

	bzero(&vsec, sizeof (vsecattr_t));
	vsec.vsa_mask = VSA_ACL | VSA_ACLCNT | VSA_DFACL | VSA_DFACLCNT;

	//
	// Note: acquiring the attribute_lock isn't needed here since
	// the underlying file system will do whatever locking
	// is needed to return a consistent result.
	//
#if	SOL_VERSION >= __s11
	error = VOP_GETSECATTR(back_obj.vnodep, &vsec, secattrflag, credp,
	    NULL);
#else
	error = VOP_GETSECATTR(back_obj.vnodep, &vsec, secattrflag, credp);
#endif

	PXFS_VER::secattr	*sattrp = new PXFS_VER::secattr;
	sattr = sattrp;
	sattrp->mask = vsec.vsa_mask;

	if (error != 0) {
		pxfslib::throw_exception(env, error);
		return;
	}

	//lint -e571
	if (vsec.vsa_aclcnt != 0 && vsec.vsa_aclentp != NULL) {
		sattrp->acl_ent.length((uint_t)vsec.vsa_aclcnt);
		bcopy(vsec.vsa_aclentp, sattrp->acl_ent.buffer(),
		    (size_t)vsec.vsa_aclcnt * sizeof (aclent_t));
		kmem_free(vsec.vsa_aclentp,
		    (size_t)vsec.vsa_aclcnt * sizeof (aclent_t));
	}
	if (vsec.vsa_dfaclcnt != 0 && vsec.vsa_dfaclentp != NULL) {
		sattrp->acl_def_ent.length((uint_t)vsec.vsa_dfaclcnt);
		bcopy(vsec.vsa_dfaclentp, sattrp->acl_def_ent.buffer(),
		    (size_t)vsec.vsa_dfaclcnt * sizeof (aclent_t));
		kmem_free(vsec.vsa_dfaclentp,
		    (size_t)vsec.vsa_dfaclcnt * sizeof (aclent_t));
	}
	//lint +e571
}

//
// fobj_ii::pathconf - get the value of a configurable limit or option
//
void
fobj_ii::pathconf(int32_t cmd, sol::uintptr_t &result, solobj::cred_ptr credobj,
    Environment &_environment)
{
	cred_t	*credp = solobj_impl::conv(credobj);
	ulong_t value;
#if	SOL_VERSION >= __s11
	int	error = VOP_PATHCONF(back_obj.vnodep, cmd, &value, credp, NULL);
#else
	int	error = VOP_PATHCONF(back_obj.vnodep, cmd, &value, credp);
#endif
	result = (sol::uintptr_t)value;
	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

void
fobj_ii::cascaded_ioctl(sol::nodeid_t nodeid, sol::pid_t cpid, int32_t iocmd,
    sol::intptr_t arg, int32_t flag, int32_t &result, solobj::cred_ptr credobj,
    Environment &_environment)
{
	cred_t		*credp = solobj_impl::conv(credobj);
	copy_args	args(nodeid, cpid);
	int		error = 0;
	int		ret_val = 0;

#ifdef DEBUG
	error = testioctl(iocmd, arg, result, args, credp);
	if (error != EDOM) {
		if (error != 0) {
			pxfslib::throw_exception(_environment, error);
		}
		return;
	}
	error = 0;
#endif

	// Set the context in which the ioctl takes place.
	copy::setcontext(&args);

	//
	// See if the ioctl needs fs-specific handling
	// (but don't handle ioctl's to devices).
	//
	if (get_ftype() != PXFS_VER::fobj_io) {
		fs_dependent_impl *fs_dep_implp =
		    fs_implp->get_fs_dep_implp();

		if (fs_dep_implp->process_cascaded_ioctl(nodeid, this, iocmd,
		    arg, flag, credp, &ret_val, error, _environment)) {

			// Ioctl has been processed - just return.

			// Trap future references.
			copy::setcontext(NULL);
			result = ret_val;

			if (error != 0) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_FOBJ,
				    PXFS_GREEN,
				    ("fobj_ii:ioctl(%p) %x err %d\n",
				    this, iocmd, error));
				pxfslib::throw_exception(_environment, error);
			}
			return;
		}
	}

	//
	// Return ENOTTY for unsupported remote ioctls.
	// Currenly, we check for uscsicmd only.
	//
	if ((iocmd == USCSICMD) && (orb_conf::local_nodeid() != nodeid)) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJ,
		    PXFS_GREEN,
		    ("fobj_ii:Unsupported ioctl(%p) %x on Args %p \t %x .. \n",
		    this, iocmd, arg, arg));
		result = ENOTTY;
		return;
	}

	//
	// Do not allow enabling logging for the underlying filesystem
	// if the filesystem is mounted on a lofi device.
	//
	if (iocmd == _FIOLOGENABLE && get_fsp()->device_is_lofi()) {
		result = ENOTSUP;
		return;
	}

	//
	// Note: this operation is not checkpointed. If the ioctl()
	// is not idempotent, it should be handled in a file system
	// specific manner which can send appropriate checkpoints
	// (see above).
	//
#if	SOL_VERSION >= __s11
	error = VOP_IOCTL(back_obj.vnodep, iocmd, arg, flag, credp,
	    &ret_val, NULL);
#else
	error = VOP_IOCTL(back_obj.vnodep, iocmd, arg, flag, credp,
	    &ret_val);
#endif

#ifdef DEBUG
	// Trap future references.
	copy::setcontext(NULL);
#endif
	result = ret_val;

	if (error != 0) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_FOBJ,
		    PXFS_GREEN,
		    ("fobj_ii:ioctl(%p) %x err %d\n",
		    this, iocmd, error));
		pxfslib::throw_exception(_environment, error);
	}
}

//
// is_it_busy
//	Return values
//		1 == busy
//		0 == not busy
//		-1 == error
//
// Called from _FIOISBUSY ioctl. To be used only by the
// Legato Networker Product. This ioctl provides no guarantees.
// The file could be opened immediately after the ioctl completes.
//
// Note that this virtual function is overloaded for those
// file types that can support this operation.
//
int
fobj_ii::is_it_busy()
{
	return (-1);
}

//
// Entry point into the PXFS client for the VOP_FRLOCK() call.
//
void
fobj_ii::frlock(int32_t cmd, sol::flock64_t &lock_info, int32_t flag,
    sol::u_offset_t off, solobj::cred_ptr credobj,
    PXFS_VER::pxfs_llm_callback_ptr cb, Environment &_environment)
{
	fr_lock.lock();
	if (fr_lockp == NULL) {
		//
		// This is the first time file range locking is used,
		// so create the support object.
		//
		fr_lockp = new frange_lock(this);
	}
	fr_lock.unlock();

	fr_lockp->frlock(cmd, lock_info, flag, off, credobj, cb, _environment);
}

//
// Effect: Cancels the request in the underlying local lock manager.  The
//	request is identified by its vnode, and its callback object. The request
//	may also be in the 'granted_list', or in the 'processed_list', in which
//	case it needs to be removed from there.
//
void
fobj_ii::frlock_cancel_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	ASSERT(fr_lockp != NULL);
	fr_lockp->frlock_cancel_request(cb, _environment);
}

//
// Effect: Executes the request in the underlying local lock manager.  The
//	request is identified by its callback object, and must be in the list
//	of locks that have been processed.
//
void
fobj_ii::frlock_execute_request(PXFS_VER::pxfs_llm_callback_ptr cb,
    Environment &_environment)
{
	ASSERT(fr_lockp != NULL);
	fr_lockp->frlock_execute_request(cb, _environment);
}

//
// This checkpoint gets invoked on the secondary by frlock() and remove_locks()
// on the primary.
//
void
fobj_ii::ckpt_locks(const REPL_PXFS_VER::lock_info_seq_t &locks)
{
	fr_lock.lock();
	if (fr_lockp == NULL) {
		//
		// This is the first time file range locking is used,
		// so create the support object.
		// The file range lock object is destroyed
		// when this object is destroyed
		//
		fr_lockp = new frange_lock(this);	//lint !e423
	}
	fr_lock.unlock();

	fr_lockp->ckpt_locks(locks);
}

//
// Get the FID of the fobj
// fobjid will be the result, or NULL if there is an error
//
void
fobj_ii::getfobjid(PXFS_VER::fobjid_t_out fobjid, Environment &_environment)
{
	PXFS_VER::fobjid_t *fobjidp = NULL;

	if (back_obj.fidp != NULL) {
		fobjidp = new PXFS_VER::fobjid_t(back_obj.fidp->fid_len,
		    back_obj.fidp->fid_len);
		bcopy(back_obj.fidp->fid_data, fobjidp->buffer(),
		    (size_t)back_obj.fidp->fid_len);
	} else {
		pxfslib::throw_exception(_environment, EINVAL);
	}

	fobjid = fobjidp;
}

//
// get_bind_info
// This type of fobj does not cache information.
//
PXFS_VER::fobj_client_ptr
fobj_ii::get_bind_info(PXFS_VER::bind_info &binfo, PXFS_VER::fobj_client_ptr,
    cred_t *, Environment &)
{

	// Identify type for union
	binfo._d_ = PXFS_VER::bt_fobj;

	// Get current data page caching and mmap() status.
	binfo._u.bind_fobj.cachedata = false;

	binfo._u.bind_fobj.rights = PXFS_VER::attr_none;

	return (nil);
}

//
// Checkpoint the state of a file
// (this is used to dump state to new secondaries).
//
void
fobj_ii::ckpt_fobj_state(uint64_t id)
{
	delete_num = id;
}

//
// convert_to_primary -  This converts a secondary to become a new
// primary. If an error is returned, the object should be left
// marked as a secondary.
//
int
fobj_ii::convert_to_primary()
{
	ASSERT(is_replicated());
	ASSERT(!primary_ready);

	if (back_obj.fidp == NULL) {
		// This is a "dead" object.
		return (EBADF);
	}

	struct vfs	*underlying_vfsp = get_fsp()->get_vfsp();
	if (underlying_vfsp == NULL) {
		//
		// become_primary() for this filesystem failed.
		// see repl_pxfs_server::become_primary(). It sets vfsp
		// to NULL if the underlying filesystem's mount fails.
		//
		return (EIO);
	}
	vnode_t		*vnodep;
	int error = VFS_VGET(underlying_vfsp, &vnodep, back_obj.fidp);
	if (error) {
		return (error);
	} else if (vnodep == NULL) {
		return (EINVAL);
	}
	back_obj.vnodep = vnodep;
	primary_ready = true;

	if (fr_lockp != NULL) {
		fr_lockp->replay_active_locks();
	}
	return (0);
}

//
// fid_to_vnode - This function obtains the vnode
// for the file identifier (FID) of this object.
// This function is idempotent.
//
// A secondary only has the FID,
// while a primary has both the FID and vnode pointer.
//
// On converting a secondary to a primary, files with associated
// locks will be obtain a vnode pointer via
// fobj_ii::convert_to_primary(). All other files
// obtain a vnode pointer during the first invocation access via
// is_dead().
//
// Returns the vnode with a hold
//
vnode_t *
fobj_ii::fid_to_vnode(int &error)
{
	if (back_obj.fidp == NULL) {
		// This is a "dead" object.
		error = EBADF;
		return (NULL);
	}

	struct vfs	*underlying_vfsp = get_fsp()->get_vfsp();
	if (underlying_vfsp == NULL) {
		//
		// become_primary() for this filesystem failed.
		// see repl_pxfs_server::become_primary(). It sets vfsp
		// to NULL if the underlying filesystem's mount fails.
		//
		error = EIO;
		return (NULL);
	}
	vnode_t		*vnodep;
	error = VFS_VGET(underlying_vfsp, &vnodep, back_obj.fidp);
	if (error) {
		return (NULL);
	} else if (vnodep == NULL) {
		error = EINVAL;
		return (NULL);
	}
	error = 0;
	return (vnodep);
}

//
// convert_to_secondary - This is used on the primary during switchover
// to transition from a primary state to a secondary state.
//
int
fobj_ii::convert_to_secondary()
{
	ASSERT(is_replicated());
	ASSERT(primary_ready);
	ASSERT(get_ftype() != PXFS_VER::fobj_io);

	//
	// A secondary file object does not have a vnode
	//
	VN_RELE(back_obj.vnodep);
	back_obj.vnodep = NULL;

	primary_ready = false;
	return (0);
}

//
// Convert an fobj from secondary to spare and delete ourself.
//
void
fobj_ii::convert_to_spare()
{
	ASSERT(is_replicated());

	delete this;
}

//
// is_dead
// Return true if this is a dead replicated file system object
// (i.e., there was a fatal error in convert_to_primary() or
// convert_to_secondary() but the replica manager still thinks
// the service is alive).
// Note that this routine is only called on the primary but the object
// may be marked as '!primary_ready if it couldn't be converted to primary
// or it was released in fs_ii::unmount().
// If we return true, the environment is also set to return an exception.
//
bool
fobj_ii::is_dead(Environment &env)
{
	if (fobj_released) {
		//
		// Note that EIO is correct, the "disk" failed.
		// ESTALE is returned by NFS when VFS_VGET() returns error.
		//
		pxfslib::throw_exception(env, EIO);
		return (true);
	}

	if (!primary_ready) {
		int		error;
		vnode_t		*vnodep = fid_to_vnode(error);

		if (vnodep == NULL) {
			PXFS_DBPRINTF(PXFS_TRACE_FOBJ,
			    PXFS_RED,
			    ("fobj_ii:fid_to_vnode(%p) error %d\n",
			    this, error));
			pxfslib::throw_exception(env, error);
			return (true);
		}
		//
		// The first primary ready test was just a hint.
		// We have to do the real test under the lock.
		// This happens only once after a failover,
		// so it is the rare case.
		//
		// We use the fidlock because when the file object is
		// not primary_ready the file object resides on the fidlist.
		//
		fs_implp->fidlock.wrlock();
		if (primary_ready) {
			//
			// Another thread won the race.
			//
			ASSERT(back_obj.vnodep != NULL);

			// Will not use the vnode that was found
			VN_RELE(vnodep);
		} else {
			//
			// In this case, we are executing on the
			// primary and the fobj is not backed by
			// a vnode. Therefore, perform just-in-time
			// fid conversion and then allow invocations.
			//
			int		bktno = fs_implp->vp_to_bucket(vnodep);
			fs_implp->fobjlist_locks[bktno].wrlock();

			// Transfer the hold on the vnode to the file object
			back_obj.vnodep = vnodep;

			get_fsp()->remove_from_fidlist(this);
			bool	added = get_fsp()->add_to_list(this);
			ASSERT(added);
			primary_ready = true;
			fs_implp->fobjlist_locks[bktno].unlock();
		}
		fs_implp->fidlock.unlock();
	}
	return (get_fsp()->is_dead(env));
}

//
// Helper function for dumping state to a new secondary.
// This is called on the primary only.
//
void
fobj_ii::dump_state(REPL_PXFS_VER::fs_replica_ptr ckptp,
    PXFS_VER::fobj_ptr fobjp,
    Environment &env)
{
	ASSERT(is_replicated());

	//
	// Checkpoint the current state if it differs from the defaults
	// set after creating the fobj on the new secondary.
	//
	if (delete_num != 0) {
		//
		// The secondary assumes that the delete_num is zero.
		// So only have to checkpoint when that is not the case.
		//
		ckptp->ckpt_fobj_state(fobjp, delete_num, env);
		if (env.exception() != NULL) {
			return;
		}
	}

	//
	// Checkpoint all the file record locks on this file.
	//
	if (fr_lockp == NULL) {
		return;
	}
	uint_t	len = fr_lockp->active_locks.count();
	if (len == 0) {
		return;
	}
	REPL_PXFS_VER::lock_info_seq_t		locks(len, len);
	REPL_PXFS_VER::lock_info_t		*tmp;
	uint_t					i = 0;
	for (fr_lockp->active_locks.atfirst();
	    (tmp = fr_lockp->active_locks.get_current()) != NULL;
	    fr_lockp->active_locks.advance()) {
		ASSERT(i < len);
		locks[i] = *tmp;
		i++;
	}

	ASSERT(i == len);

	ckptp->ckpt_locks(fobjp, locks, env);
}

//
// This helper function is used to determine
// whether or not a file must be converted
// from a FID to a vnode during
// fs_ii::become_primary(). Files with
// active locks will be converted.
//
bool
fobj_ii::has_active_locks()
{
	return (fr_lockp != NULL && fr_lockp->has_active_locks());
}

#ifdef DEBUG

#define	IOCTLNUM 72499 // Random number; must match testprogs/ioctltest.cc
#define	LOCAL_OFFSET 5
#define	BUFLEN 32
#define	TESTSTR "This is the test string"

//
// This code tests the cross-domain copyin/out/str routines, since they
// are very hard to test otherwise.  If the ioctl isn't a test ioctl, this
// routine returns EDOM (we don't want to return something that an ioctl
// error might actually return).
//
int
fobj_ii::testioctl(int32_t iocmd, sol::intptr_t arg, int32_t &result,
    copy_args &args, cred_t *)
{
	//
	// Test ioctls starting at IOCTLNUM go through the pxfs code, while
	// ioctls starting at IOCTLNUM+LOCALOFFSET go through the old code.
	//
	if (iocmd >= IOCTLNUM + LOCAL_OFFSET &&
	    iocmd < IOCTLNUM + 2 * LOCAL_OFFSET) {
		iocmd -= LOCAL_OFFSET;
	}

	if (iocmd == IOCTLNUM) {
		//
		// Test copyin.  We copy in BUFLEN bytes and return a hash
		// of the data.
		//
		char tbuf[BUFLEN];
		copy::setcontext(&args);
		int err = ::copyin((void *)arg, (void *)tbuf, (size_t)BUFLEN);
#ifdef DEBUG
		// Trap future references.
		copy::setcontext(NULL);
#endif
		result = 0;
		for (int i = 0; i < BUFLEN; i++) {
			//
			// Return hash of the data.  Note: we want to
			// return a positive result so it won't get
			// interpreted as an error.
			//
			result = (result + tbuf[i] * (i + 1)) & 0x7fffffff;
		}
		return (err ? EFAULT : 0);

	} else if (iocmd == IOCTLNUM + 1) {
		//
		// Test copyinstr.  We copy in a string of maxlen BUFLEN
		// and return a hash of the string.
		//
		char tbuf[BUFLEN];
		size_t len;
		copy::setcontext(&args);
		int err = ::copyinstr((char *)arg, tbuf, (size_t)BUFLEN, &len);
#ifdef DEBUG
		// Trap future references.
		copy::setcontext(NULL);
#endif
		if (err) {
			return (err);
		}
		if (len != strlen(tbuf) + 1) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJ,
			    PXFS_RED,
			    ("fobj_ii:testioctl(%p) copyinstr %d vs %d\n",
			    this, len, strlen(tbuf)));
			return (EINVAL);
		}
		result = 0;
		for (int i = 0; i < (int)len; i++) {
			// Return hash of the data
			result = (result + tbuf[i] * (i + 1)) & 0x7fffffff;
		}
		return (err ? EFAULT : 0);
	} else if (iocmd == IOCTLNUM + 2) {
		// Test copyout.  We copy out TESTSTR and return 0.
		copy::setcontext(&args);
		int err = ::copyout((void *)TESTSTR, (void *)arg,
			strlen(TESTSTR) + 1);
#ifdef DEBUG
		// Trap future references.
		copy::setcontext(NULL);
#endif
		result = 0;
		return (err ? EFAULT : 0);

	} else if (iocmd == IOCTLNUM + 3) {
		// Test copyoutstr.  We copy out TESTSTR and return 0.
		size_t len;
		copy::setcontext(&args);
		int err = ::copyoutstr(TESTSTR, (char *)arg, (size_t)BUFLEN,
		    &len);
#ifdef DEBUG
		// Trap future references.
		copy::setcontext(NULL);
#endif
		if (err) {
			return (EFAULT);
		}
		if (len != strlen(TESTSTR) + 1) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_FOBJ,
			    PXFS_RED,
			    ("fobj_ii:testioctl(%p) copyoutstr %d vs %d\n",
			    this, len, strlen(TESTSTR) + 1));
			return (EINVAL);
		}
		result = 0;
		return (err);
	} else {
		return (EDOM);
	}
}
#endif /* TESTIOCTL */

//
// get_fobj_version - identify version
//
// virtual method
fobj_abstract::fobj_version
fobj_ii::get_fobj_version()
{
	return (fobj_abstract::VERSION_1);
}

//
// undo_register_client - the default routine for those file objects
// that do not support client side caching.
//
// virtual
void
fobj_ii::undo_register_client(Environment &env)
{
}
