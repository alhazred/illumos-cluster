/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _BITMAP_H
#define	_BITMAP_H

#pragma ident	"@(#)bitmap.h	1.9	08/05/20 SMI"

#include <sys/os.h>

template<int SIZE> class bitmap {
public:
	bitmap()		{ bzero(&arr[0], sizeof (arr)); }
	~bitmap()		{ /* empty */ }
	void set(uint_t ind)	{ arr[byte(ind)] |= bit(ind); }
	void clear(uint_t ind)	{ arr[byte(ind)] &= ~bit(ind); }
	int test(uint_t ind)	{ return (arr[byte(ind)] & bit(ind) ? 1 : 0); }

private:
	// Select the numeric type to be used for storing bitmap.
	typedef	uchar_t elem;
	enum { ELEM_NBITS = 8 };
	enum { ELEM_SHIFT = 3 };
	enum { ELEM_MASK = (1 << ELEM_SHIFT) - 1 };

	elem arr[(SIZE + ELEM_NBITS - 1) / ELEM_NBITS];

	static uint_t byte(uint_t ind) { return (ind >> ELEM_SHIFT); }
	static uint_t bit(uint_t ind)  { return (1 << (ind & ELEM_MASK)); }
};

#endif	/* _BITMAP_H */
