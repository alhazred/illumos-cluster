//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _MOUNT_REPLICA_IMPL_H
#define	_MOUNT_REPLICA_IMPL_H

#pragma ident	"@(#)mount_replica_impl.h	1.25	08/05/20 SMI"

#include <repl/service/replica_tmpl.h>
#include <h/pxfs.h>
#include <h/repl_pxfs.h>
#include <sys/os.h>
#include <pxfs/common/pxfslib.h>

// Forward declarations.
class mount_server_impl;

//
// This struct is used for building a table for supporting the mapping
// from version protocol spec file version number to the various IDL
// interface versions it represents.  The table will be a two dimensional
// array indexed by major/minor vp version.
//
typedef struct {		// idl interfaces
	int	ms;		// fs::mount_server
	int	ms_ckpt;	// repl_pxfs::mount_replica
} mount_ver_map_t;

//
// These are the current maximum indexes used for accessing the vp to idl
// version table.
//
const int	MOUNT_VP_MAX_MAJOR = 2;
const int	MOUNT_VP_MAX_MINOR = 2;

//
// HA service for recording mount commands.
// There is one service for the whole cluster and is never shutdown.
//
class mount_replica_impl : public repl_server<repl_pxfs::mount_replica> {
public:
	mount_replica_impl(const char *id);
	~mount_replica_impl();

	// Required functions for replica framework.
	// replica::repl_prov::=
	void become_secondary(Environment &_environment);

	void add_secondary(replica::checkpoint_ptr sec_chkpt,
	    const char *secondary_name, Environment &_environment);

	void remove_secondary(const char *secondary_name,
	    Environment &_environment);

	void freeze_primary_prepare(Environment &_environment);

	void freeze_primary(Environment &_environment);

	void unfreeze_primary(Environment &_environment);

	void become_primary(const replica::repl_name_seq &secondary_names,
	    Environment &_environment);

	void become_spare(Environment &_environment);

	void shutdown(Environment &_environment);

	CORBA::Object_ptr get_root_obj(Environment &_environment);
	//

	// repl_pxfs::mount_replica::=
	void ckpt_new_server(fs::mount_server_ptr server,
	    Environment &_environment);

	void ckpt_add_client(fs::mount_client_died_ptr clobj,
	    fs::mount_client_ptr client, sol::nodeid_t nodeid,
	    bool shutdown, Environment &_environment);

	void ckpt_remove_client(fs::mount_client_ptr client,
	    Environment &_environment);

	void ckpt_mount_start(const sol::mounta &ma,
	    fs::mount_client_ptr client, Environment &_environment);

	void ckpt_mount_start_v1(const sol::mounta &ma,
	    fs::mount_client_ptr client, Environment &_environment);

	void ckpt_mount_err(sol::error_t error, Environment &_environment);

	void	ckpt_mount(
	    fs::filesystem_ptr		fs,
	    const fs::fs_info		&fsinfo,
	    const sol::mounta		&ma,
	    const char			*mntoptions,
	    bool			dev_is_ha,
	    const char			*dev_name,
	    const sol::nodeid_seq_t	&dev_nids,
	    Environment			&_environment);

	void	ckpt_mount_v1(
	    pxfs_v1::filesystem_ptr	fs,
	    const pxfs_v1::fs_info	&fsinfo,
	    const sol::mounta		&ma,
	    const char			*mntoptions,
	    bool			dev_is_ha,
	    const char			*dev_name,
	    const sol::nodeid_seq_t	&dev_nids,
	    Environment			&_environment);

	void	ckpt_mount_middle(
	    fs::filesystem_ptr		fs,
	    const fs::fs_info		&fsinfo,
	    const char			*mntoptions,
	    bool			dev_is_ha,
	    const char			*dev_name,
	    const sol::nodeid_seq_t	&dev_nids,
	    Environment			&_environment);

	void	ckpt_mount_middle_v1(
	    pxfs_v1::filesystem_ptr	fs,
	    const pxfs_v1::fs_info	&fsinfo,
	    const char			*mntoptions,
	    bool			dev_is_ha,
	    const char			*dev_name,
	    const sol::nodeid_seq_t	&dev_nids,
	    Environment			&_environment);

	void	ckpt_remount_middle(
	    fs::filesystem_ptr	fs,
	    uint32_t		vfsflags,
	    const char		*mntoptions,
	    Environment		&_environment);

	void	ckpt_remount_middle_v1(
	    pxfs_v1::filesystem_ptr	fs,
	    uint32_t			vfsflags,
	    const char			*mntoptions,
	    Environment			&_environment);

	void	ckpt_unmount_start(
	    fs::filesystem_ptr		fs,
	    solobj::cred_ptr		credobj,
	    fs::mount_client_ptr	client,
	    bool			is_shutdown,
	    Environment			&_environment);

	void ckpt_unmount_start_1(
	    fs::filesystem_ptr		fs,
	    int32_t			flags,
	    solobj::cred_ptr		credobj,
	    fs::mount_client_ptr	client,
	    bool			is_shutdown,
	    Environment			&_environment);

	void	ckpt_unmount_start_v1(
	    pxfs_v1::filesystem_ptr	fs,
	    int32_t			flags,
	    solobj::cred_ptr		credobj,
	    fs::mount_client_ptr	client,
	    bool			is_shutdown,
	    Environment			&_environment);

	void ckpt_unmount_middle(sol::error_t error,
	    Environment &_environment);

	void ckpt_unmount_notified(Environment &_environment);

	void ckpt_unmount_end(Environment &_environment);

	void ckpt_unmount_shutdown(fs::mount_client_ptr client,
	    Environment &_environment);

	void ckpt_devlock(fs::mount_client_ptr client, sol::nodeid_t nodeid,
	    const char *dev_name, Environment &_environment);

	void ckpt_devunlock(const char *dev_name, Environment &_environment);

	void ckpt_get_dc_callback(fs::dc_callback_ptr cb,
	    Environment &_environment);

	void ckpt_notify_change(sol::dev_t gdev,
	    const sol::nodeid_seq_t &dev_nids, Environment &_environment);

	void ckpt_service_version(unsigned short new_major,
	    unsigned short new_minor, Environment &);

	void ckpt_upgrade_client_list(fs::mount_client_ptr client,
	    sol::nodeid_t nodeid, Environment &_environment);

	void ckpt_upgrade_devlock_list(const char *dev_name,
	    fs::mount_client_ptr client, Environment &_environment);
	//

	// These are called when the pxfs server module is loaded or unloaded.
	static int startup();
	static int shutdown();

	// Checkpoint accessor function
	repl_pxfs::mount_replica_ptr	get_checkpoint_mount_replica();

	//
	// mount_version_callback_impl::do_callback is called by the
	// version manager.  That method calls here to pass on the version
	// update.
	//
	void upgrade_callback(const version_manager::vp_version_t &,
	    Environment &);

	// Set the initial version number.
	void set_version(const version_manager::vp_version_t &);

	//
	// These are both protected by version_lock.
	// current_version is the versioned protocol number the version
	// manager has told us we should be running as.
	// pending_version is set when a secondary gets a callback before
	// the primary sends a checkpoint so a failover during upgrade
	// commit is processed correctly.
	//
	version_manager::vp_version_t current_version;
	version_manager::vp_version_t pending_version;

	//
	// This lock protects 'current_version', _ckpt_proxy and
	// provides locking between upgrade callbacks and become_primary().
	// It needs to be a rwlock since we make checkpoint calls while
	// holding the lock.
	//
	os::rwlock_t version_lock;

private:
	// Pointer to the root HA object for this service.
	mount_server_impl		*mount_serverp;

	//
	// XXX CORBA reference to root HA object to prevent _unrefernced()
	// from being called until get_root_obj() is called.
	//
	fs::mount_server_var		mount_server_v;

	// Checkpoint proxy
	repl_pxfs::mount_replica_ptr	_ckpt_proxy;
};

class mount_version_callback_impl :
    public McServerof<version_manager::upgrade_callback> {
public:
	mount_version_callback_impl(mount_replica_impl &p);
	~mount_version_callback_impl();

	void _unreferenced(unref_t);

	// IDL methods.
	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &_environment);

private:
	mount_replica_impl &prov;
};

#endif	// _MOUNT_REPLICA_IMPL_H
