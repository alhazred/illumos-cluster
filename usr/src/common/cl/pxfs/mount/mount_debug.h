//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _MOUNT_DEBUG_H
#define	_MOUNT_DEBUG_H

#pragma ident	"@(#)mount_debug.h	1.5	08/05/20 SMI"

#include <sys/dbg_printf.h>

#define	MOUNT_DBGBUF

#ifdef	MOUNT_DBGBUF
extern dbg_print_buf mount_dbg;
#define	MOUNT_DBG(a)	mount_dbg.dbprintf a
#else
#define	MOUNT_DBG(a)
#endif

const int	MOUNT_GREEN = 3;
const int	MOUNT_AMBER = 2;
const int	MOUNT_RED = 1;

const int	MOUNT_TRACE_CLIENT		= 0x00000001;
const int	MOUNT_TRACE_SERVER		= 0x00000002;
const int	MOUNT_TRACE_REPLICA		= 0x00000004;

extern uint_t	mount_trace_options;
extern int	mount_trace_level;

//
// MOUNT_DBPRINTF - record mount trace information.
// Inputs
//	option - enables trace if set in mount_trace_options
//	level - Debug level, currently red, amber and green
//	args - The arguments to the print statements must be enclosed
//		with parenthesis, as in the following example:
//		("pxfs action %d\n", action_number)
//
// Note that the trailing "else" statement in the macro removes
// ambiguity when the macro is nested in another "if" statement.
//
#ifdef MOUNT_DBGBUF
#define	MOUNT_DBPRINTF(option, level, args) \
if (((option & mount_trace_options) &&\
	(level <= mount_trace_level)) ||\
	(level == MOUNT_RED)) {\
		MOUNT_DBG(args); \
	} else
#else
#define	MOUNT_DBPRINTF(option, level, args)
#endif	/* MOUNT_DBGBUF */

#endif	// _MOUNT_DEBUG_H
