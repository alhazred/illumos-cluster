//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _MOUNT_CLIENT_IMPL_H
#define	_MOUNT_CLIENT_IMPL_H

#pragma ident	"@(#)mount_client_impl.h	1.34	08/05/20 SMI"

#include <h/pxfs.h>
#include <h/pxfs_v1.h>
#include <h/solobj.h>
#include <orb/object/adapter.h>

#include <pxfs/client/pxvfs.h>

//
// mount_client_impl - one object exists on every node in the cluster.
// This object supports starting and stopping the pxfs client software
// for each mounted file system. This includes mounting and umounting
// the proxy file system.
//
class mount_client_impl : public McServerof<fs::mount_client> {
public:
	void		_unreferenced(unref_t arg);

	//
	// Activate() registers this node's mount_client with the
	// mount_server and thereby imports all extant globally mounted
	// file systems.
	//
	// It is distinct from startup because module load time occurs before
	// the prerequisites for the global name space have been established.
	//
	static int	activate();

	//
	// Return true if activate() has been called.
	//
	static bool	is_activated();

	//
	// Notify intent to mount a device.
	// Device is "locked" until it is either mounted or the requesting
	// node dies.
	//
	static int	devlock(int cmd, struct pathname *devpnp);

	//
	// Return a pointer to the mount server.
	// Note that the returned pointer is not duplicated and therefore
	// should not be released.
	//
	static fs::mount_server_ptr	get_server();

	//
	// Return a reference to the local mount client.
	// Note that the returned pointer is duplicated and therefore
	// should be released.
	//
	static fs::mount_client_ptr	get_client_ref();

	void _generic_method(CORBA::octet_seq_t &,
	    CORBA::object_seq_t &, Environment &);

	// For rolling upgrade support
	static void			update_mount_server_ref();

	//
	// Return a pointer to the local mount client.
	//
	static mount_client_impl	*get_client();

	// + fs::mount_client::*
	/* mount_client */
	void	lock_mountpoint(const char *mountpoint, int32_t mntflags,
	    Environment &_environment);

	void	unlock_mountpoint(const char *mountpoint,
	    Environment &_environment);

	void	instantiate(const sol::mounta &ma, sol::uintptr_t mvp,
	    solobj::cred_ptr credobj, fs::filesystem_out fs,
	    fs::fs_info &fsinfo, CORBA::String_out mntoptions,
	    Environment &_environment);

	void	instantiate_v1(const sol::mounta &ma, sol::uintptr_t mvp,
	    solobj::cred_ptr credobj, pxfs_v1::filesystem_out fs,
	    pxfs_v1::fs_info &fsinfo, CORBA::String_out mntoptions,
	    Environment &_environment);

	void	instantiate_ha(const sol::mounta &ma, sol::uintptr_t mvp,
	    solobj::cred_ptr credobj, const char *dev_name,
	    Environment &_environment);

	void	instantiate_ha_v1(const sol::mounta &ma, sol::uintptr_t mvp,
	    solobj::cred_ptr credobj, const char *dev_name,
	    Environment &_environment);

	void	reinstantiate_ha(const sol::mounta &ma,
	    fs::filesystem_ptr fs,
	    solobj::cred_ptr credobj, const char *dev_name,
	    Environment &_environment);

	void	reinstantiate_ha_v1(const sol::mounta &ma,
	    pxfs_v1::filesystem_ptr fs,
	    solobj::cred_ptr credobj, const char *dev_name,
	    Environment &_environment);

	void	prepare_unmount(fs::filesystem_ptr fs, solobj::cred_ptr credobj,
	    Environment &_environment);

	void	prepare_unmount_1(fs::filesystem_ptr fs, int32_t flags,
	    solobj::cred_ptr credobj, bool skip_purge,
	    Environment &_environment);

	void	prepare_unmount_v1(pxfs_v1::filesystem_ptr fs, int32_t flags,
	    solobj::cred_ptr credobj, bool skip_purge,
	    Environment &_environment);

	void	unmount_failed(Environment &_environment);

	void	unmount_failed_1(bool skip, Environment &_environment);

	void	add_notify_locked(const sol::mounta &ma, const char *mntoptions,
	    fs::filesystem_ptr fs, const fs::fs_info &fsinfo,
	    Environment &_environment);

	void	add_notify_locked_v1(const sol::mounta &ma,
	    const char *mntoptions,
	    pxfs_v1::filesystem_ptr fs, const pxfs_v1::fs_info &fsinfo,
	    Environment &_environment);

	void	add_notify(const sol::mounta &ma, const char *mntoptions,
	    bool is_ha_repl, const char *dev_name, fs::filesystem_ptr fs,
	    const fs::fs_info &fsinfo, Environment &_environment);

	void	add_notify_v1(const sol::mounta &ma, const char *mntoptions,
	    bool is_ha_repl, const char *dev_name, pxfs_v1::filesystem_ptr fs,
	    const pxfs_v1::fs_info &fsinfo, Environment &_environment);

	void	remove_notify(const char *mountpoint, const char *special,
	    bool updatemtab, Environment &_environment);

	void	remove_notify_1(const char *mountpoint, bool unlink_vfs,
	    Environment &_environment);

	void	remove_client(const char *mountpoint, const char *special,
	    bool is_ha_repl, const char *dev_name, fs::filesystem_ptr fs,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	remove_client_v1(const char *mountpoint, const char *special,
	    bool is_ha_repl, const char *dev_name, pxfs_v1::filesystem_ptr fs,
	    solobj::cred_ptr credobj, Environment &_environment);

	void	set_flags(const sol::mounta &ma, const char *mntoptions,
	    fs::filesystem_ptr fs, uint32_t vfsflags,
	    Environment &_environment);

	void	set_flags_v1(const sol::mounta &ma, const char *mntoptions,
	    pxfs_v1::filesystem_ptr fs, uint32_t vfsflags,
	    Environment &_environment);

	void	upgrade_mount_client(fs::mount_server_ptr mountserver_p,
	    Environment &_environment);
	// +

private:
	enum mount_ver_t {VERSION_0, VERSION_1};

	// Private so we can ensure one per node.
	mount_client_impl();
	~mount_client_impl();

	//
	// Common code for add_notify() and add_notify_local().
	//
	void	add_notify_common_v1(const sol::mounta &ma,
	    const char *mntoptions,
	    pxfs_v1::filesystem_ptr fs, const pxfs_v1::fs_info &fsinfo,
	    vnode_t *coveredvp);

	//
	// Common code for instantiate_ha() and reinstantiate_ha().
	//
	void	instantiate_ha_common(const sol::mounta &ma, vnode_t *vp,
	    solobj::cred_ptr credobj, const char *dev_name,
	    mount_ver_t mount_ver, Environment &_environment);

	void	unmount_failed_v1(bool skip);

	void	remove_notify_v1(const char *mountpoint, bool unlink_vfs);

	//
	// Reference to the mount server.
	//
	fs::mount_server_var		server;

	// Reference to server object for detecting if this client crashes.
	fs::mount_client_died_var	keepalive;

	// Locked mount point vnode or NULL if no lock.
	vnode_t				*mntvp;

	// Protects 'mntvp'.
	os::mutex_t			mount_lock;

	// Serialize mount server retry attempts to add_notify
	os::mutex_t			add_notify_lock;

	//
	// Record the pxvfs when an unmount is in progress.
	// Only one unmount can occur at a time.
	//
	pxvfs	*unmount_pxvfs_v1_p;

	// Protects 'unmount_pxvfsp'.
	os::mutex_t			unmount_lock;

	//
	// Pointer to the mount client for this node (see activate()).
	//
	static mount_client_impl	*this_mount_client;
	static os::mutex_t		mount_client_lock; // protects above
};

int pxfs_mount_client_startup();
int pxfs_mount_client_shutdown();

#endif	// _MOUNT_CLIENT_IMPL_H
