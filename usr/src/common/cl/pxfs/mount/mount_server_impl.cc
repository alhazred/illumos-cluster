//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mount_server_impl.cc	1.83	08/05/20 SMI"

#include <sys/errno.h>

#include <sys/types.h>
#include <sys/thread.h>
#include <sys/file.h>
#include <sys/mount.h>
#include <sys/pathname.h>
#include <sys/sysmacros.h>

#include <sys/os.h>
#include <sys/sol_conv.h>
#include <solobj/solobj_impl.h>

#include <pxfs/mount/mount_server_impl.h>
#include <pxfs/mount/mount_replica_impl.h>
#include <pxfs/mount/mount_debug.h>
#include <pxfs/server/fobj_impl.h>

//
// The number of seconds devlock waits for, before timing out.
// This is an undocumented tunable.
//
int pxfs_devlock_timeout = 5;

// VP to IDL interface version mapping for Mount subsystem
extern mount_ver_map_t
    mount_vp_to_idl[MOUNT_VP_MAX_MAJOR +1][MOUNT_VP_MAX_MINOR +1];

//lint -e1512
//
// Warning(1512) destructor for base class is not virtual -- In a
// final pass through all the classes, we have found a class that is
// the base class of a derivation and has a destructor but the
// destructor is not virtual. It is conventional for inherited classes
// to have virtual destructors so that is it safe to 'delete' a
// pointer to a base class.
//
// The classes prov_common_iter and prov_common_setin the file
// prov_common.h have to be changed to have virtual destructors.
//

//
// fs_elem methods
//

fs_elem::fs_elem(pxfs_v1::filesystem_ptr fs_p, const pxfs_v1::fs_info &finfo,
    const sol::mounta &md, const char *options, bool is_ha, const char *name,
    const sol::nodeid_seq_t &nids) :
	_DList::ListElem(this),
	fs_v1_info(finfo),
	ma(md),
	dev_is_ha(is_ha),
	dev_nids(nids),
	fs_elem_ver(VERSION_1)
{
	ASSERT(!CORBA::is_nil(fs_p));
	fs_v1_ptr = pxfs_v1::filesystem::_duplicate(fs_p);

	// Make a copy, don't use String_var constructor.
	mntoptions = options;
	dev_name = name;
}

fs_elem::~fs_elem()
{
}

//
// devlock_elem methods
//

//
// Constructor for "device lock".
//
devlock_elem::devlock_elem(fs::mount_client_ptr c, sol::nodeid_t nodeid,
    const char *name) :
	_SList::ListElem(this)
{
	owner = fs::mount_client::_duplicate(c);
	spec = os::strdup(name);
	nwaiters = 0;
	ownerid = nodeid;
	unlocked = false;
}

devlock_elem::~devlock_elem()
{
	delete [] spec;
}

//
// mount_client_elem methods
//

//
// Create a list element to save the mount_client reference and nodeid.
// Primary constructor.
//
mount_client_elem::mount_client_elem(mount_server_impl *server,
    fs::mount_client_ptr client, sol::nodeid_t nid,
    mount_replica_impl *replp) :
	mc_replica_of<fs::mount_client_died>(replp),
	_SList::ListElem(this)
{
	serverp = server;
	clientptr = fs::mount_client::_duplicate(client);
	nodeid = nid;
	shutdown = false;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("mount_client_elem newP %p\n", this));
}

//
// Create a list element to save the mount_client reference and nodeid.
// Secondary constructor.
//
mount_client_elem::mount_client_elem(mount_server_impl *server,
    fs::mount_client_ptr client, sol::nodeid_t nid, bool is_shutdown,
    fs::mount_client_died_ptr obj) :
	mc_replica_of<fs::mount_client_died>(obj),
	_SList::ListElem(this)
{
	serverp = server;
	clientptr = fs::mount_client::_duplicate(client);
	nodeid = nid;
	shutdown = is_shutdown;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("mount_client_elem newS %p s %d\n",
	    this, shutdown));
}

mount_client_elem::~mount_client_elem()
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("~mount_client_elem %p\n", this));
	serverp = NULL; // for lint
}

//
// This is called if the mount client crashes or is halted.
//
void
mount_client_elem::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	if (!CORBA::is_nil(clientptr)) {
		//
		// Note: we don't delete ourself here
		// (see mount_server_impl::client_died()).
		//
		serverp->client_died(this);
	} else {
		//
		// mount_server::remove_client() has already removed
		// this from the list so just delete ourself.
		//
		delete this;
	}
}

//
// mount_state methods
//

//
// Constructor for mount_state.
//
mount_state::mount_state(const sol::mounta &md, fs::mount_client_ptr client_p,
    mount_server_impl &srvr, mount_ver_t ver) :
	ma(md),
	error(0),
	is_remount(false),
	mount_ver(ver),
	server(srvr)
{
	ASSERT(!CORBA::is_nil(client_p));
	mountpoint_lock_c = fs::mount_client::_duplicate(client_p);
	fs_v1_info.fsflag = 0; // for lint
}

mount_state::~mount_state()
{
}

//
// The client has crashed so there will be no retry of the mount().
// Unlock the mount points we have already locked.
//
void
mount_state::orphaned(Environment &env)
{
	(void) server.mount_orphaned(this, true, env);
}

//
// This is called on the secondary to complete the mount transaction.
//
void
mount_state::committed()
{
	mountpoint_lock_c = fs::mount_client::_nil();
}

//
// unmount_state methods
//

//
// Constructor for unmount_state for recording unmounts during remove_client().
// Note that client can be nil.
//
unmount_state::unmount_state(pxfs_v1::filesystem_ptr fs_p,
    int32_t umflags,
    solobj::cred_ptr cred_p,
    mount_server_impl &srvr,
    fs::mount_client_ptr client_p) :
	flags(umflags),
	state(START),
	error(0),
	unmount_ver(VERSION_1),
	server(srvr)
{
	ASSERT(!CORBA::is_nil(fs_p));
	fs_v1_obj = pxfs_v1::filesystem::_duplicate(fs_p);

	skip = fs::mount_client::_duplicate(client_p);
	credobj = solobj::cred::_duplicate(cred_p);
}

unmount_state::~unmount_state()
{
}

//
// This is called on the (new) primary after a failover of the mount_server
// and a crash of the mount_client node.
//
void
unmount_state::orphaned(Environment &e)
{
	server.unmount_orphaned(this, false, e);
}

//
// This is called on the secondary when add_commit() is called on the primary.
//
void
unmount_state::committed()
{
	ASSERT(CORBA::is_nil(fs_v1_obj));
}

//
// dc_callback_impl methods
//

//
// Primary constructor.
//
dc_callback_impl::dc_callback_impl(mount_server_impl &srvr,
    mount_replica_impl *serverp) :
	mc_replica_of<fs::dc_callback>(serverp),
	server(srvr)
{
	// LINTED: Call to virtual function within a contructor or destructor.
	_handler()->set_cookie((void *)this);
}

//
// Secondary constructor.
//
dc_callback_impl::dc_callback_impl(mount_server_impl &srvr,
    fs::dc_callback_ptr obj) :
	mc_replica_of<fs::dc_callback>(obj),
	server(srvr)
{
	// LINTED: Call to virtual function within a contructor or destructor.
	_handler()->set_cookie((void *)this);
}

dc_callback_impl::~dc_callback_impl()
{
}

void
dc_callback_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	delete this;
}

// dc_callback_impl(fs::dc_callback::notify_change)
void
dc_callback_impl::notify_change(sol::dev_t gdev,
    const sol::nodeid_seq_t &nodes, Environment &_environment)
{
	server.notify_change(gdev, nodes, _environment);
}

// dc_callback_impl(fs::dc_callback::still_active, _environment)
bool
dc_callback_impl::still_active(sol::dev_t gdev, Environment &)
{
	return (server.still_active(gdev));
}

//
// mount_server_impl methods
//

//
// Return a CORBA pointer (no CORBA::release() required)
// to the checkpoint interface.
//
repl_pxfs::mount_replica_ptr
mount_server_impl::get_checkpoint()
{
	return ((mount_replica_impl*)(get_provider()))->
	    get_checkpoint_mount_replica();
}

//
// Primary constructor.
//
mount_server_impl::mount_server_impl(mount_replica_impl *serverp) :
	mc_replica_of<fs::mount_server>(serverp)
{
	repl_serverp = serverp;
	primary = true;
	frozen = false;
	currentmnt = (char *)NULL;
}

//
// Secondary constructor.
//
mount_server_impl::mount_server_impl(mount_replica_impl *serverp,
    fs::mount_server_ptr obj) :
	mc_replica_of<fs::mount_server>(obj)
{
	repl_serverp = serverp;
	primary = false;
	frozen = false;
	currentmnt = (char *)NULL;
}

//
// Destructor.
//
mount_server_impl::~mount_server_impl()
{
	ASSERT(client_list.empty());
	ASSERT(fs_list.empty());
}

void
mount_server_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	//
	// XXX Should wait for all _unreferenced() from
	// mount_client_elem and dc_callback_impl but since this service
	// is never shut down, _unreferenced() should never get called.
	//
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:_unreferenced\n"));
	delete this;
}

//
// This is called  to get a new reference.  Doing get_objref() here would
// get the highest reference version that was compiled.  We want the
// highest reference which is currently committed so we use this indirect
// way.
//
void
mount_server_impl::_generic_method(CORBA::octet_seq_t &,
    CORBA::object_seq_t &objs, Environment &e)
{
	objs[0] = repl_serverp->get_root_obj(e);
}


//
// Called from mount_replica_impl when switching to primary.
//
void
mount_server_impl::convert_to_primary()
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server: primary\n"));

	primary = true;

#ifdef DEBUG
	//
	// There should be no threads waiting for a device lock
	// (see comment for devunlock()).
	//
	devlock_elem	*dep;
	for (devlock_list.atfirst();
	    (dep = devlock_list.get_current()) != NULL;
	    devlock_list.advance()) {
		ASSERT(dep->nwaiters == 0);
	}
#endif
}

//
// Called from mount_replica_impl when switching to secondary.
//
void
mount_server_impl::convert_to_secondary()
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server: secondary\n"));

	primary = false;

#ifdef DEBUG
	//
	// There should be no threads waiting for a device lock
	// (see comment for devunlock()).
	//
	devlock_elem	*dep;
	for (devlock_list.atfirst();
	    (dep = devlock_list.get_current()) != NULL;
	    devlock_list.advance()) {
		ASSERT(dep->nwaiters == 0);
	}
#endif
}

//
// Called from mount_replica_impl when switching to spare.
//
void
mount_server_impl::convert_to_spare()
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server: spare\n"));

	if (!CORBA::is_nil(dc_callback_obj)) {
		dc_callback_impl	*dc_callbackp = (dc_callback_impl *)
		    dc_callback_obj->_handler()->get_cookie();
		dc_callback_obj = fs::dc_callback::_nil();
		delete dc_callbackp;
	}

	client_list.dispose();
	fs_list.dispose();
	devlock_list.dispose();
	delete this;
}

//
// This is called if a mount_client dies
// (called from mount_client_elem::_unreferenced()).
//
void
mount_server_impl::client_died(mount_client_elem *cep)
{
	devlock_elem	*dep;

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:client_died: %p nid %d\n",
	    (void *)cep, cep->nodeid));

	if (!primary) {
		//
		// _unreferenced() and checkpoints are synchronized on
		// the secondary so we don't need to lock the list.
		//
		// Remove any locks the client held.
		//
		devlock_list.atfirst();
		while ((dep = devlock_list.get_current()) != NULL) {
			devlock_list.advance();
			if (cep->clientptr->_equiv(dep->owner)) {
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_AMBER,
				    ("server:client_died "
				    "unlock %p waiters %d\n",
				    dep, dep->nwaiters));
				(void) devlock_list.erase(dep);
				delete dep;
			}
		}
		(void) client_list.erase(cep);
		delete cep;
		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_CLIENT_DIED, FaultFunctions::generic);

	//
	// Remove any locks the client held.
	//
	devlock_list_lock.wrlock();
	devlock_list.atfirst();
	while ((dep = devlock_list.get_current()) != NULL) {
		devlock_list.advance();
		if (cep->clientptr->_equiv(dep->owner)) {
			//
			// No need to checkpoint this since the
			// secondary will get _unreferenced() too.
			//
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_AMBER,
			    ("server:client_died unlock %p waiters %d\n",
			    dep, dep->nwaiters));
			(void) devlock_list.erase(dep);

			dep->waiter_lock.lock();
			if (dep->nwaiters != 0) {
				// This wakes up all waiting threads.
				dep->unlocked = true;
				dep->waiter_cv.broadcast();
				dep->waiter_lock.unlock();

				// The last waiter will do the delete.
			} else {
				dep->waiter_lock.unlock();
				delete dep;
			}
		}
	}
	devlock_list_lock.unlock();

	//
	// Note: there is no checkpoint since the secondary gets
	// _unreferenced() when we delete the object.
	//
	client_list_lock.wrlock();
	(void) client_list.erase(cep);
	delete cep;
	client_list_lock.unlock();
}

//
// Upgrade mount_client references during Rolling Upgrade commit.
//
void
mount_server_impl::upgrade_client_reference(Environment &_environment)
{
	CORBA::octet_seq_t	data;
	CORBA::object_seq_t	objs(1, 1);
	Environment		e;
	mount_client_elem	*cep;
	devlock_elem		*dep;

	//
	// Create a primary context so the provider can send
	// checkpoints while the service is frozen.
	// XXX change the primary_ctx::invoke_env type.
	//
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT,
	    _environment);

	// Update the client reference in client list.
	client_list_lock.wrlock();

	client_list.atfirst();
	while ((cep = client_list.get_current()) != NULL) {
		client_list.advance();
		cep->clientptr->_generic_method(data, objs, e);
		if (e.exception()) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:upgrade_client_reference"
			    "exception when upgrading client reference"
			    "%p in client list on %d errno %d\n",
			    (void *)cep, cep->nodeid,
			    e.exception()->exception_enum()));
			e.clear();
		} else {
			cep->clientptr = fs::mount_client::_narrow(objs[0]);

			//
			// Send a checkpoint to the secondaries telling
			// them to use the new version mount_client object
			// reference.
			//
			get_checkpoint()->
			    ckpt_upgrade_client_list(cep->clientptr,
			    cep->nodeid, _environment);
			ASSERT(_environment.exception() == NULL);

			//
			// Upgrade the mount_client to use the new version
			// of the mount_server.
			//
			cep->clientptr->upgrade_mount_client(get_objref(), e);

			if (e.exception()) {
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_RED,
				    ("server:upgrade_client_reference"
				    "exception when upgrading server reference"
				    "for %p on %d error %d\n",
				    (void *)cep, cep->nodeid,
				    e.exception()->exception_enum()));
				e.clear();
			}
		}
	}

	client_list_lock.unlock();

	// Update the client reference in device lock list.
	devlock_list_lock.wrlock();

	devlock_list.atfirst();
	while ((dep = devlock_list.get_current()) != NULL) {
		devlock_list.advance();
		dep->owner->_generic_method(data, objs, e);
		if (e.exception()) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:upgrade_client_reference"
			    "exception when upgrading client reference"
			    "%p in devlock list on %d errno %d\n",
			    (void *)dep, dep->ownerid,
			    e.exception()->exception_enum()));
			e.clear();
		} else {
			dep->owner = fs::mount_client::_narrow(objs[0]);

			//
			// Send a checkpoint to the secondaries telling
			// them to use the new version mount_client object
			// reference.
			//
			get_checkpoint()->
			    ckpt_upgrade_devlock_list(dep->spec, dep->owner,
			    _environment);
			ASSERT(_environment.exception() == NULL);

			//
			// Upgrade the mount_client to use the new version
			// of the mount_server. The mount_client may have
			// already been upgraded. This invocation is
			// idempotent. So that is not a problem.
			//
			dep->owner->upgrade_mount_client(get_objref(), e);

			if (e.exception()) {
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_RED,
				    ("server:upgrade_client_reference"
				    "exception when upgrading server reference"
				    "for %p on %d error %d\n",
				    (void *)dep, dep->ownerid,
				    e.exception()->exception_enum()));
				e.clear();
			}
		}
	}

	devlock_list_lock.unlock();

	_environment.trans_ctxp = NULL;
}

//
// Add a client to the list of mount_server clients, replaying the extant set
// of global mounts to bring it into consistency with the rest of the cluster.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::add_client(fs::mount_client_ptr client_p,
    sol::nodeid_t nodeid,
    fs::mount_client_died_out clobj, Environment &_environment)
{
	mount_client_elem		*ncep = new mount_client_elem(this,
					    client_p, nodeid, repl_serverp);
	mount_client_elem		*cep;
	fs::mount_client_died_ptr	clobjp;
	fs_elem				*fep;
	fs_elem				*ofep;
	Environment			e;
	sol::error_t			err;
	bool				need_fs_status;
	bool				attempt_unmount;
	uint32_t			i;
	solobj::cred_var		credobj = solobj_impl::conv(kcred);

	// Check to see if we have saved state.
	primary_ctx	*ctxp = primary_ctx::extract_from(_environment);
	unmount_state	*statep;
	if (ctxp != NULL &&
	    (statep = (unmount_state *)ctxp->get_saved_state()) != NULL) {
		//
		// Since we have saved state, we know the original
		// primary sent the ckpt_unmount_start() checkpoint.
		// We finish the unmount process from where we left off.
		//
		unmount_orphaned(statep, false, _environment);
	}

	client_list_lock.wrlock();

	//
	// Check to see if the mount client we are adding is already in
	// the list. If it is there, it means this is a retry after
	// a mount server failover. Since we got the checkpoint, we
	// know that the mounts have been replayed on the client.
	// Note that we may have an old entry for the same node
	// in the list until mount_client_elem::_unreferenced()
	// is processed (which is why we search the list by object
	// reference rather than nodeid).
	//
	if ((cep = find_client(client_p)) != NULL) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:add_client found %p\n", cep));
		clobj = cep->get_objref();
		goto done;
	}

	fs_list_lock.wrlock();

	//
	// If the joining node has a direct connection to the device for a
	// a filesystem which is currently globally mounted, but that
	// filesystem is not currently mounted locally  (NOT_AVAILABLE),
	// then we attempt to unmount that filesystem (anticipating that
	// it will be mounted locally by the joining node).
	//
	// An HA filesystem is considered AVAILABLE if there is a
	// node with a primary or secondary filesystem replica.
	//
	// A non-HA filesytem is considered AVAILABLE if the node with
	// the connection to the device is already running in the cluster.
	// So if the joining node has a direct connection we attempt an
	// unmount.
	//
	for (fs_list.atlast(); (fep = fs_list.get_current()) != NULL; ) {
		//
		// Move the 'current' pointer in the list away from 'fep',
		// so that the fs_list.erase() in 'unmount_common' does not
		// move the pointer.
		//
		fs_list.retreat();

		// Check for joining node being connected to the device.
		need_fs_status = false;
		attempt_unmount = false;
		if (fep->dev_is_ha) {
			for (i = 0; i < fep->dev_nids.length(); i++) {
				if (nodeid == fep->dev_nids[i]) {
					need_fs_status = true;
					break;
				}
			}
		} else {
			if (nodeid == fep->dev_nids[0])
				attempt_unmount = true;
		}

		if (attempt_unmount || (need_fs_status &&
		    (get_fs_status(fep) == NOT_AVAILABLE))) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_AMBER,
			    ("server:add_client unmount %s\n",
			    (const char *)fep->ma.dir));

			//
			// We try to unmount as many file systems as possible
			// but don't return an error at the end so the client
			// isn't removed from the global name space.
			//
			ASSERT(fep->fs_elem_ver == fs_elem::VERSION_1);
			//
			// At this point we will not do a forced
			// unmount. So the flags are empty.
			//
			get_checkpoint()->ckpt_unmount_start_v1(
			    fep->fs_v1_ptr, 0, credobj,
			    fs::mount_client::_nil(), false,
			    _environment);
			ASSERT(_environment.exception() == NULL);
			err = unmount_common_1(fs::mount_client::_nil(),
			    fep, 0, credobj, unmount_state::START, 0,
			    NULL, _environment);
			if (err != 0) {
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_RED,
				    ("server:add_client unmount %s err %d\n",
				    (const char *)fep->ma.dir, err));
			}
		}
	}

	//
	// Replay the extant set of mounts on the new client.
	// If any replayed mount fails, return an exception.
	// Note that we count on the list being properly ordered, so that
	// mount dependencies are respected.
	//
	for (fs_list.atfirst();
	    (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {
		//
		// Compute whether an HA replica needs to be started.
		//
		bool	is_ha_repl = false;
		if (fep->dev_is_ha) {
			for (i = 0; i < fep->dev_nids.length(); i++) {
				if (nodeid == fep->dev_nids[i]) {
					is_ha_repl = true;
				}
			}
		}

		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:add_client add_notify %s ha %d\n",
		    (const char *)fep->ma.dir, is_ha_repl));

		ASSERT(fep->fs_elem_ver == fs_elem::VERSION_1);
		client_p->add_notify_v1(fep->ma, fep->mntoptions,
		    is_ha_repl, fep->dev_name,
		    fep->fs_v1_ptr, fep->fs_v1_info, e);
		if (e.exception() == NULL) {
			continue;
		}

		//
		// We have to undo the work done so far.
		// The list of filesystems is traversed in reverse order.
		//
		sol::error_t	error = pxfslib::get_err(e);
		e.clear();
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:add_client add_notify %s error %d\n",
		    (const char *)fep->ma.dir, error));
		ofep = fep;
		while (fs_list.retreat(),
		    (fep = fs_list.get_current()) != NULL) {
			//
			// Recompute whether an HA replica was started.
			//
			is_ha_repl = false;
			if (fep->dev_is_ha) {
				for (i = 0; i < fep->dev_nids.length(); i++) {
					if (nodeid == fep->dev_nids[i]) {
						is_ha_repl = true;
					}
				}
			}

			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:add_client remove_client %s ha %d\n",
			    (const char *)fep->ma.dir, is_ha_repl));
			ASSERT(fep->fs_elem_ver == fs_elem::VERSION_1);
			client_p->remove_client_v1(fep->ma.dir,
			    fep->ma.spec, is_ha_repl, fep->dev_name,
			    fep->fs_v1_ptr, credobj, e);
			if (e.exception()) {
				//
				// We added it but can't remove it. The
				// exception should be node died
				// or EBUSY for remove_client problems.  The
				// latter will be resolved with the
				// implementation of forced unmount.
				//
				if (CORBA::COMM_FAILURE::_exnarrow(
				    e.exception())) {
					MOUNT_DBPRINTF(
					    MOUNT_TRACE_SERVER,
					    MOUNT_RED,
					    ("server:add_client "
					    "exception when removing "
					    "%s on %d COMM\n",
					    (const char *)fep->ma.dir,
					    ncep->nodeid));
				} else {
					err = pxfslib::get_err(e);
					MOUNT_DBPRINTF(
					    MOUNT_TRACE_SERVER,
					    MOUNT_RED,
					    ("server:add_client "
					    "exception when removing "
					    "%s on %d errno %d\n",
					    (const char *)fep->ma.dir,
					    ncep->nodeid, err));
				}
				e.clear();
			}
		}

		//
		// Propagate the exception back to our caller.
		//
		fs_list_lock.unlock();
		_environment.exception(new mount_err(error, ofep->ma.dir));
		clobj = fs::mount_client_died::_nil();
		goto done;
	}

	fs_list_lock.unlock();

	//
	// Checkpoint this operation so that the secondary can
	// create a mount_client_elem too (see ckpt_add_client() below).
	// Everything else is OK to repeat.
	// Note: we insert onto the head of the list so the most recent
	// entry is the valid entry (a stale entry could exist until
	// mount_client_elem::_unreferenced() is processed).
	//
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:add_client %p nid %d\n",
	    ncep, ncep->nodeid));
	client_list.prepend(ncep);
	cep = ncep;
	ncep = NULL;
	clobjp = cep->get_objref();
	get_checkpoint()->ckpt_add_client(clobjp, client_p, nodeid, false,
	    _environment);
	ASSERT(_environment.exception() == NULL);

	//
	// Return a reference to the client so we can detect if it crashes.
	//
	clobj = clobjp;

done:
	client_list_lock.unlock();

	if (ncep != NULL) {
		delete ncep;
	}
}

//
// Add a client to the list of mount_server clients.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_add_client(fs::mount_client_died_ptr clobj,
    fs::mount_client_ptr client_p, sol::nodeid_t nodeid, bool shutdown)
{
	//
	// Check to see if the mount client we are adding is already in
	// the list.
	//
	if (find_client(client_p) != NULL) {
		return;
	}

	mount_client_elem	*cep = new mount_client_elem(this, client_p,
	    nodeid, shutdown, clobj);
	client_list.prepend(cep);
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:ckpt_add_client %p nid %d shut %d\n",
	    cep, cep->nodeid, cep->shutdown));
}

//
// This method is never called.
//
void
mount_server_impl::remove_client(fs::mount_client_ptr,
    solobj::cred_ptr, Environment &)
{
}

//
// Remove a client from the list of mount_server clients.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_remove_client(fs::mount_client_ptr client_p)
{
	mount_client_elem	*cep;

	if ((cep = find_client(client_p)) != NULL) {
		//
		// We found the guy we're looking for.
		//
		(void) client_list.erase(cep);
		cep->clientptr = fs::mount_client::_nil();
	}
}

//
// Create a proxy file system, link it into the global name space,
// and unlock the mount point on all other nodes.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::mount(const sol::mounta &ma, sol::uintptr_t mvp,
    solobj::cred_ptr credobj, fs::mount_client_ptr client_p, bool dev_is_ha,
    const char *dev_name, const sol::nodeid_seq_t &dev_nids,
    fs::filesystem_out fs_obj, fs::fs_info &fsinfo,
    CORBA::String_out mntoptions, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Create a proxy file system, link it into the global name space,
// and unlock the mount point on all other nodes.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::mount_v1(const sol::mounta &ma, sol::uintptr_t mvp,
    solobj::cred_ptr credobj, fs::mount_client_ptr client_p, bool dev_is_ha,
    const char *dev_name, const sol::nodeid_seq_t &dev_nids,
    pxfs_v1::filesystem_out fs_obj, pxfs_v1::fs_info &fsinfo,
    CORBA::String_out mntoptions, Environment &_environment)
{
	ASSERT(ma.flags & MS_SYSSPACE);

	// Check to see if we have saved state.
	primary_ctx	*ctxp = primary_ctx::extract_from(_environment);
	mount_state	*statep;
	if (ctxp != NULL &&
	    (statep = (mount_state *)ctxp->get_saved_state()) != NULL) {
		//
		// This is a retry on a new primary after a failover.
		// If the previous mount() was committed, we are done.
		//
		ASSERT(statep->mount_ver == mount_state::VERSION_1);
		if (mount_orphaned(statep, false, _environment)) {
			if (_environment.exception() == NULL) {
				// Return values from the saved state.
				ASSERT(!CORBA::is_nil(statep->fs_v1_ptr));
				fs_obj = pxfs_v1::filesystem::_duplicate(
				    statep->fs_v1_ptr);
				fsinfo = statep->fs_v1_info;
				mntoptions = os::strdup(statep->mntoptions);
			} else {
				fs_obj = pxfs_v1::filesystem::_nil();
				mntoptions = (char *)NULL;
			}
			return;
		}

		//
		// Since we have saved state, we know the original
		// primary sent the start checkpoint
		// so we don't need to do it again here.
		//
		ASSERT(statep->mountpoint_lock_c->_equiv(client_p));

		client_list_lock.wrlock();
	} else {
		//
		// This is the start of a new mount, not a retry.
		// Checkpoint the start of locking the mount points so
		// we can clean up if both the client and primary fail.
		//
		client_list_lock.wrlock();
		get_checkpoint()->ckpt_mount_start_v1(ma, client_p,
		    _environment);
		if (_environment.exception()) {
			client_list_lock.unlock();
			fs_obj = pxfs_v1::filesystem::_nil();
			mntoptions = (char *)NULL;
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount ckpt failed\n"));
			_environment.clear();
			pxfslib::throw_exception(_environment, EIO);
			return;
		}
	}

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:mount %s %s c %p\n",
	    (const char *)ma.spec, (const char *)ma.dir, (void *)client_p));

	//
	// Check to see if the special device is already mounted.
	//
	fs_elem			*fep;
	sol::error_t		error;
	mount_client_elem	*cep;
	Environment		e;

	fs_list_lock.wrlock();
	if ((const char *)ma.spec != NULL) {
		//
		// Remove the device lock entry.
		// Note that a failed mount unlocks the device too.
		//
		devlock_list_lock.wrlock();
		devlock_elem	*dep = find_devlock(ma.spec);
		if (dep != NULL) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_AMBER,
			    ("server:mount unlock %p waiters %d\n",
			    dep, dep->nwaiters));
			(void) devlock_list.erase(dep);
			get_checkpoint()->ckpt_devunlock(ma.spec, _environment);
			_environment.clear();

			dep->waiter_lock.lock();
			if (dep->nwaiters != 0) {
				// This wakes up all waiting threads.
				dep->unlocked = true;
				dep->waiter_cv.broadcast();
				dep->waiter_lock.unlock();

				// The last waiter will do the delete.
			} else {
				dep->waiter_lock.unlock();
				delete dep;
			}
		}
		devlock_list_lock.unlock();

		if ((fep = find_fs(ma.spec)) != NULL) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount found %s\n",
			    (const char *)ma.spec));
			error = EBUSY;
			goto err;
		}
	}

	//
	// Lock the mount point on all client nodes except the
	// requesting node (since it already has the mount point locked).
	// Note that if two nodes try to mount to the same mount point:
	// Nodes A and B locally lock the mount point (vn_vfswlock()).
	// Node A gets here, tries to lock node B's mount point, gets EBUSY,
	//   releases client_list_lock.
	// Node B gets here, tries to lock node A's mount point, gets EBUSY.
	// Both node's locally unlock their mount point and return EBUSY
	// from the mount system call.
	//
	error = lock_mntpnt(client_p, ma.dir, ma.flags, _environment);
	if (error != 0) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:mount can't lock mntpnt %s error %d\n",
		    (const char *)ma.dir, error));

		fs_list_lock.unlock();
		client_list_lock.unlock();

		//
		// Propagate the exception back to our caller.
		//
		pxfslib::throw_exception(_environment, error);
		fs_obj = pxfs_v1::filesystem::_nil();
		mntoptions = (char *)NULL;
		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_MOUNT_S_B, FaultFunctions::generic);

	//
	// Instantiate the file system on the nodes which have direct
	// connections to the block device.
	//
	if (dev_is_ha) {
		bool	started = false;
		for (uint32_t i = 0; i < dev_nids.length(); i++) {
			// Find the mount client pointer for device node i.
			cep = find_client(dev_nids[i]);
			if (cep == NULL) {
				//
				// The device node isn't up
				// at the moment.
				//
				continue;
			}
			cep->clientptr->instantiate_ha_v1(ma,
			    cep->clientptr->_equiv(client_p) ? mvp : NULL,
			    credobj, dev_name, e);
			if (e.exception() == NULL) {
				started = true;
				continue;
			}
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				//
				// The node crashed after we locked the
				// mount point OK. Just skip it and
				// try to start replicas on other nodes.
				//
				e.clear();
				continue;
			}
			// XXX What to do?
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount instantiate_ha "
			    "returned exception, can't start HA service %s\n",
			    (const char *)ma.spec));
			e.clear();
		}
		if (!started) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:mount can't start service %s\n",
			    (const char *)ma.spec));
			error = ENXIO;
			goto err;
		}
		//
		// Get the root HA file system object.
		//
		replica::service_admin_var	sa =
		    pxfslib::get_service_admin_ref("mount_server_impl::mount",
			(const char *)ma.spec, e);
		if (e.exception()) {
			error = EIO;
			e.clear();
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount get_service_admin_ref(%s) except\n",
			    (const char *)ma.spec));
			//
			// XXX Need to shut down this service but
			// can't get the service_admin object to do it.
			//
			goto err;
		}
		CORBA::Object_var obj = sa->get_root_obj(e);
		if (e.exception()) {
			error = pxfslib::get_err(e);
			e.clear();
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount get_root_obj(%s) error %d\n",
			    (const char *)ma.spec, error));

			// Need to shut down this service.
			sa->shutdown_service(false, e);
			e.clear();
			goto err;
		}
		pxfs_v1::filesystem_ptr fsobj =
		    pxfs_v1::filesystem::_narrow(obj);
		ASSERT(!CORBA::is_nil(fsobj));

		//
		// Get the file system info.
		// XXX Note that the mount_server has a temporary dependency
		// on the file system service for this IDL invocation.
		//
		fsobj->get_mntinfo(fsinfo, mntoptions, e);
		if (e.exception()) {
			error = pxfslib::get_err(e);
			e.clear();
			CORBA::release(fsobj);
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount get_mntinfo(%s) error %d\n",
			    (const char *)ma.spec, error));

			// Need to shut down this service.
			sa->shutdown_service(false, e);
			e.clear();
			goto err;
		}

		//
		// Note that we transfer the CORBA reference
		// to the return value (i.e., don't release fsobj).
		//
		fs_obj = fsobj;
	} else {
		//
		// Find the mount client pointer for the node which has
		// the device.
		//
		cep = find_client(dev_nids[0]);
		if (cep == NULL) {
			error = ENXIO;
			goto err;
		}
		cep->clientptr->instantiate_v1(ma,
		    cep->clientptr->_equiv(client_p) ? mvp : NULL,
		    credobj, fs_obj, fsinfo, mntoptions, e);
		if (e.exception() != NULL) {
			error = pxfslib::get_err(e);
			e.clear();
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:mount instantiate(%s) err %d\n",
			    (const char *)ma.spec, error));
		err:
			fs_list_lock.unlock();

			// Checkpoint the error before unlocking mount points.
			get_checkpoint()->ckpt_mount_err(error, _environment);
			_environment.clear();

			//
			// Unlock the mount points we have already locked.
			//
			unlock_mntpnt(client_p, NULL, ma.dir, _environment);

			client_list_lock.unlock();
			pxfslib::throw_exception(_environment, error);
			fs_obj = pxfs_v1::filesystem::_nil();
			mntoptions = (char *)NULL;
			return;
		}
	}

	//
	// Add a new file system element to the list and checkpoint it.
	//
	fep = new fs_elem((pxfs_v1::filesystem_ptr)fs_obj, fsinfo, ma,
	    mntoptions, dev_is_ha, dev_name, dev_nids);
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:mount add %s fep %p\n",
	    (const char *)ma.spec, fep));

	ASSERT(find_fs(fs_obj) == NULL);

	//
	// We should only append to the end of the list to maintain
	// mount ordering.
	//
	fs_list.append(fep);

	//
	// Checkpoint the successful instantiation of the file system.
	//
	get_checkpoint()->ckpt_mount_middle_v1(fs_obj, fsinfo, mntoptions,
	    dev_is_ha, dev_name, dev_nids, _environment);
	if (_environment.exception()) {
		//
		// The only possible exception for checkpoints is
		// a VERSION exception, which represents a programming
		// error. This is the first use of a new version checkpoint.
		// So we will check for a programming mistake.
		//
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:mount fep %p ckpt_mount_middle_v1 except %d\n",
		    fep, _environment.exception()->exception_enum()));
		ASSERT(0);
		_environment.clear();
	}

	fs_list_lock.unlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_MOUNT_S_A, FaultFunctions::generic);

	//
	// At this point we are committed to completing the mount without
	// errors.  Notify each client of the addition.
	//
	mount_end_v1(client_p, ma, mntoptions, fs_obj, fsinfo, false,
	    _environment);

	client_list_lock.unlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_MOUNT_S_E, FaultFunctions::generic);
}

//
// Checkpoint the start of a mount or remount.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_mount_start_v1(const sol::mounta &ma,
    fs::mount_client_ptr client_p, Environment &env)
{
	transaction_state	*statep =
	    new mount_state(ma, client_p, *this, mount_state::VERSION_1);
	statep->register_state(env);
	env.clear();
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:ckpt_mount_start_v1 %s\n",
	    (const char *)ma.dir));
}

//
// Checkpoint a mount or remount error.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_mount_err(sol::error_t error, Environment &env)
{
	//
	// We must have saved state because this cannot be the first checkpoint
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	ASSERT(ctxp != NULL);
	mount_state	*statep = (mount_state *)ctxp->get_saved_state();
	ASSERT(statep != NULL);
	ASSERT(statep->mount_ver == mount_state::VERSION_1);

	//
	// Save the error value.
	// This also marks the start of unlocking the mount points.
	//
	ASSERT(CORBA::is_nil(statep->fs_v1_ptr));
	statep->error = error;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_RED,
	    ("server:ckpt_mount_err %d\n", error));
}

//
// Add a fs_elem to the list of globally mounted file systems.
// This is used to dump state to a newly joining secondary.
//
void
mount_server_impl::ckpt_mount_v1(pxfs_v1::filesystem_ptr fs_obj,
    const pxfs_v1::fs_info &fsinfo, const sol::mounta &ma,
    const char *mntoptions,
    bool dev_is_ha, const char *dev_name, const sol::nodeid_seq_t &dev_nids)
{
	//
	// Add fs_elem if it isn't already there.
	// This can happen if the primary failed after sending this
	// checkpoint and the operation was retried on the new primary.
	//
	ASSERT(!CORBA::is_nil(fs_obj));
	if (find_fs(fs_obj) == NULL) {
		fs_elem		*fep = new fs_elem(fs_obj, fsinfo, ma,
		    mntoptions, dev_is_ha, dev_name, dev_nids);
		fs_list.append(fep);
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:ckpt_mount_v1 add %s fep %p\n",
		    (const char *)ma.spec, fep));
	}
}

//
// Checkpoint the creation of a new file system object.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_mount_middle_v1(pxfs_v1::filesystem_ptr fs_obj,
    const pxfs_v1::fs_info &fsinfo, const char *mntoptions, bool dev_is_ha,
    const char *dev_name, const sol::nodeid_seq_t &dev_nids, Environment &env)
{
	//
	// We must have saved state because this cannot be the first checkpoint
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	ASSERT(ctxp != NULL);
	mount_state	*statep = (mount_state *)ctxp->get_saved_state();
	ASSERT(statep != NULL);

	statep->fs_v1_ptr = pxfs_v1::filesystem::_duplicate(fs_obj);
	statep->fs_v1_info = fsinfo;
	statep->mntoptions = mntoptions;

	//
	// Add fs_elem if it isn't already there.
	// This can happen if the primary failed after sending this checkpoint
	// and the operation was retried on the new primary.
	//
	ASSERT(!CORBA::is_nil(fs_obj));
	fs_elem		*fep = find_fs(fs_obj);
	if (fep == NULL) {
		fep = new fs_elem(fs_obj, fsinfo, statep->ma, mntoptions,
		    dev_is_ha, dev_name, dev_nids);
		fs_list.append(fep);
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_AMBER,
		    ("server:ckpt_mount_middle_v1 add %s fep %p\n",
		    (const char *)statep->ma.spec, fep));
	}
}

//
// Helper routine for mount_state::orphaned() to clean up mount() or remount().
// Return true if the operation is completed.
//
bool
mount_server_impl::mount_orphaned(mount_state *statep, bool orph,
    Environment &env)
{
	if (CORBA::is_nil(statep->mountpoint_lock_c)) {
		//
		// We have seen the add_commit() so just return an error
		// if needed.
		//
		if (statep->error != 0 && !orph) {
			pxfslib::throw_exception(env, statep->error);
		}
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:mount_orphaned committed %s\n",
		    (const char *)statep->ma.dir));
		return (true);
	}

	ASSERT(statep->mount_ver == mount_state::VERSION_1);
	if (!CORBA::is_nil(statep->fs_v1_ptr)) {
		//
		// We have seen ckpt_mount_middle() or
		// ckpt_remount_middle(), but not the add_commit().
		// Make sure all mount clients have unlocked the
		// mount point and updated /etc/mnttab.
		//
		ASSERT(statep->error == 0);
		client_list_lock.wrlock();
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:mount_orphaned v1 do end %s\n",
		    (const char *)statep->ma.dir));
		mount_end_v1(statep->mountpoint_lock_c, statep->ma,
		    statep->mntoptions, statep->fs_v1_ptr,
		    statep->fs_v1_info, statep->is_remount, env);
		client_list_lock.unlock();
		return (true);
	}
	if (statep->error != 0 || orph) {
		//
		// We have seen ckpt_mount_err() or mount_state::orphaned()
		// but not the add_commit().
		// Make sure mount points are unlocked.
		//
		client_list_lock.wrlock();
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:mount_orphaned error %d\n",
		    statep->error));

		//
		// Make sure file service is shut down.
		//
		fs_list_lock.wrlock();
		Environment	e;
		replica::service_admin_var	sa =
		    pxfslib::get_service_admin_ref("mount_server_impl::mount",
			(const char *)statep->ma.spec, e);
		if (e.exception() == NULL) {
			sa->shutdown_service(false, e);
		}
		e.clear();
		fs_list_lock.unlock();

		unlock_mntpnt(statep->mountpoint_lock_c, NULL, statep->ma.dir,
		    env);
		client_list_lock.unlock();

		if (statep->error != 0 && !orph) {
			pxfslib::throw_exception(env, statep->error);
		}
		return (true);
	}

	//
	// Only the start checkpoint has been seen
	//
	return (false);
}

//
// Notify all mount clients of a new mount.
//
void
mount_server_impl::mount_end_v1(fs::mount_client_ptr skip,
    const sol::mounta &ma, const char *mntoptions,
    pxfs_v1::filesystem_ptr fs_obj, const pxfs_v1::fs_info &fsinfo,
    bool is_remount, Environment &env)
{
	mount_client_elem	*cep;
	Environment		e;

	ASSERT(client_list_lock.write_held());

	client_list.atfirst();
	while ((cep = client_list.get_current()) != NULL) {
		client_list.advance();

		// Is this the requesting node?
		if (cep->clientptr->_equiv(skip)) {
			continue;
		}

		if (is_remount) {
			//
			// Set the proxy vfs_t flags and release the
			// mount point lock on this node.
			//
			cep->clientptr->set_flags_v1(ma, mntoptions, fs_obj,
			    fsinfo.fsflag, e);
		} else {
			//
			// Create a new proxy vfs_t, link it into the name
			// space, and release the mount point lock on this
			// node.
			//
			cep->clientptr->add_notify_locked_v1(ma, mntoptions,
			    fs_obj, fsinfo, e);
		}

		if (e.exception() == NULL) {
			continue;
		}
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			e.clear();
			continue;
		}
		ASSERT(0); // XXX
		e.clear();
	}

	add_commit(env);
}

//
// Perform a global remount.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::remount(fs::filesystem_ptr fs_obj, fs::fobj_ptr mntpnt,
    const sol::mounta &ma, solobj::cred_ptr credobj,
    fs::mount_client_ptr client_p,
    uint32_t &vfsflags, CORBA::String_out mntoptions, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Perform a global remount.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::remount_v1(pxfs_v1::filesystem_ptr fs_obj,
    pxfs_v1::fobj_ptr mntpnt,
    const sol::mounta &ma, solobj::cred_ptr credobj,
    fs::mount_client_ptr client_p,
    uint32_t &vfsflags, CORBA::String_out mntoptions, Environment &_environment)
{
	ASSERT(ma.flags & MS_SYSSPACE);

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:remount %s\n", (const char *)ma.dir));

	// Check to see if we have saved state.
	primary_ctx	*ctxp = primary_ctx::extract_from(_environment);
	mount_state	*statep;
	if (ctxp != NULL &&
	    (statep = (mount_state *)ctxp->get_saved_state()) != NULL) {
		//
		// This is a retry on a new primary after a failover.
		// If the previous remount() was committed, we are done.
		//
		ASSERT(statep->mount_ver == mount_state::VERSION_1);
		if (mount_orphaned(statep, false, _environment)) {
			if (_environment.exception() == NULL) {
				// Return values from the saved state.
				vfsflags = statep->fs_v1_info.fsflag;
				mntoptions = os::strdup(statep->mntoptions);
			} else {
				mntoptions = (char *)NULL;
			}
			return;
		}

		//
		// Since we have saved state, we know the original
		// primary sent the start checkpoint
		// so we don't need to do it again here.
		//
		ASSERT(statep->mountpoint_lock_c->_equiv(client_p));

		check_multiple_remounts(ma.dir, _environment);
		if (_environment.exception()) {
			mntoptions = (char *)NULL;
			return;
		}

		client_list_lock.wrlock();
	} else {
		check_multiple_remounts(ma.dir, _environment);
		if (_environment.exception()) {
			mntoptions = (char *)NULL;
			return;
		}

		//
		// This is the start of a new remount, not a retry.
		// Checkpoint the start of locking the mount points so
		// we can clean up if both the client and primary fail.
		//
		client_list_lock.wrlock();
		get_checkpoint()->ckpt_mount_start_v1(ma, client_p,
		    _environment);
		if (_environment.exception()) {
			//
			// We are no longer a member of the cluster,
			// don't proceed.
			//
			client_list_lock.unlock();
			_environment.clear();
			mntoptions = (char *)NULL;

			current_mount_lock.lock();

			delete [] currentmnt;
			currentmnt = (char *)NULL;
			currentmnt_cv.broadcast();

			current_mount_lock.unlock();
			return;
		}
	}

	//
	// Lock the mount point on all client nodes except the
	// requesting node (since it already has the mount point locked).
	//
	sol::error_t error = lock_mntpnt(client_p, ma.dir, ma.flags,
	    _environment);
	if (error != 0) {
		client_list_lock.unlock();

		//
		// Propagate the exception back to our caller.
		//
		pxfslib::throw_exception(_environment, error);
		mntoptions = (char *)NULL;

		current_mount_lock.lock();

		delete [] currentmnt;
		currentmnt = (char *)NULL;
		currentmnt_cv.broadcast();

		current_mount_lock.unlock();

		return;
	}

	FAULTPT_PXFS(FAULTNUM_PXFS_REMOUNT_S_B, FaultFunctions::generic);

	//
	// Get the file system info.
	// XXX Note that the mount_server has a temporary dependency
	// on the file system service for this IDL invocation.
	//
	Environment e;
	fs_obj->remount(mntpnt, ma, credobj, vfsflags, mntoptions, e);
	if (e.exception()) {
		// Checkpoint the error before unlocking mount points.
		error = pxfslib::get_err(e);
		e.clear();
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:remount error %d\n", error));
		get_checkpoint()->ckpt_mount_err(error, _environment);
		_environment.clear();

		//
		// Unlock the mount points we have already locked.
		//
		unlock_mntpnt(client_p, NULL, ma.dir, _environment);

		client_list_lock.unlock();
		pxfslib::throw_exception(_environment, error);
		mntoptions = (char *)NULL;

		current_mount_lock.lock();

		delete [] currentmnt;
		currentmnt = (char *)NULL;
		currentmnt_cv.broadcast();

		current_mount_lock.unlock();
		return;
	}

	//
	// Update the option string stored in fs_elem.
	// XXX Note that we attempt to construct the mounta data that
	// is the combined result of "mount -o ro" merged with
	// "mount -o remount" but its possible the remount changed or set
	// other options. We may need to save args for both mount and remount,
	// and then in add_client() replay the mount and the remount rather
	// than try to do just one mount with all the right args.
	//
	fs_list_lock.wrlock();

	fs_elem		*fep = find_fs(fs_obj);
	ASSERT(fep != NULL);
	fep->ma.flags = ma.flags & ~MS_REMOUNT;
	fep->ma.data = ma.data;
	fep->mntoptions = os::strdup(mntoptions);
	fep->fs_v1_info.fsflag = vfsflags;

	//
	// Checkpoint this operation so that the secondary can
	// update its state.
	//
	get_checkpoint()->ckpt_remount_middle_v1(fs_obj, vfsflags, mntoptions,
	    _environment);
	_environment.clear();

	fs_list_lock.unlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_REMOUNT_S_A, FaultFunctions::generic);

	//
	// Notify each client with the new vfsflags and mntoptions.
	// Note that at this point we are committed to updating everything.
	//
	mount_end_v1(client_p, ma, mntoptions, fs_obj, fep->fs_v1_info, true,
	    _environment);

	current_mount_lock.lock();

	delete [] currentmnt;
	currentmnt = (char *)NULL;
	currentmnt_cv.broadcast();

	current_mount_lock.unlock();

	client_list_lock.unlock();

	FAULTPT_PXFS(FAULTNUM_PXFS_REMOUNT_S_E, FaultFunctions::generic);
}

//
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_remount_middle_v1(pxfs_v1::filesystem_ptr fs_obj,
    uint32_t vfsflags, const char *mntoptions, Environment &env)
{
	//
	// We must have saved state because this cannot be the first checkpoint
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	ASSERT(ctxp != NULL);
	mount_state	*statep = (mount_state *)ctxp->get_saved_state();
	ASSERT(statep != NULL);

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:ckpt_remount_middle_v1 vfsflags %x options %s\n",
	    vfsflags, mntoptions));
	statep->is_remount = true;
	statep->fs_v1_ptr = pxfs_v1::filesystem::_duplicate(fs_obj);
	statep->fs_v1_info.fsflag = vfsflags;
	statep->mntoptions = mntoptions;

	fs_elem		*fep = find_fs(fs_obj);
	ASSERT(fep != NULL);
	fep->ma.flags = statep->ma.flags & ~MS_REMOUNT;
	fep->ma.data = statep->ma.data;
	fep->mntoptions = mntoptions;
	fep->fs_v1_info.fsflag = vfsflags;
}

//
// Helper routine to lock the mount point on all client nodes except 'skip'.
// Return an exception if the lock can't be acquired on all nodes.
// This operation should be idempotent since it can be retried on a new primary.
//
sol::error_t
mount_server_impl::lock_mntpnt(fs::mount_client_ptr skip,
    const char *mountpoint, int32_t mntflags, Environment &env)
{
	mount_client_elem *cep;
	sol::error_t error;
	Environment e;

	ASSERT(client_list_lock.write_held());

	client_list.atfirst();
	while ((cep = client_list.get_current()) != NULL) {
		client_list.advance();

		// The node requesting the mount has already done the locking
		if (cep->clientptr->_equiv(skip)) {
			continue;
		}

		// Try to get the lock on this node.
		cep->clientptr->lock_mountpoint(mountpoint, mntflags, e);
		if (e.exception() == NULL)
			continue;
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			e.clear();
			continue;
		}

		error = pxfslib::get_err(e);
		e.clear();
		get_checkpoint()->ckpt_mount_err(error, env);
		ASSERT(env.exception() == NULL);

		// Unlock the mount points we have already locked.
		unlock_mntpnt(skip, cep, mountpoint, env);
		return (error);
	}

	return (0);
}

//
// Helper routined to unlock clients that have already been locked successfully.
// The client_list_lock should be held before calling this.
//
void
mount_server_impl::unlock_mntpnt(fs::mount_client_ptr skip,
    mount_client_elem *endp, const char *mountpoint, Environment &env)
{
	mount_client_elem	*cep;
	Environment		e;

	ASSERT(client_list_lock.write_held());

	client_list.atfirst();
	while ((cep = client_list.get_current()) != endp) {
		client_list.advance();

		// Should we skip this client?
		if (cep->clientptr->_equiv(skip)) {
			continue;
		}

		cep->clientptr->unlock_mountpoint(mountpoint, e);

		if (e.exception() == NULL) {
			continue;
		}
		if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
			e.clear();
			continue;
		}

		// XXX something better we could do?
		ASSERT(0);
		e.clear();
	}

	add_commit(env);
}

//
// Unmount a global file system by locking all mount points on all
// client nodes except the requesting node, flushing the DNLC, etc.
// Return an exception if the lock can't be acquired on all nodes or
// if there are still active proxy vnodes.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::unmount(fs::filesystem_ptr fs_obj, solobj::cred_ptr credobj,
    fs::mount_client_ptr client_p, sol::nodeid_t nodeid, bool is_shutdown,
    Environment &_environment)
{
	CL_PANIC(0);
}

//
// Forced Unmount support version
//
// Unmount a global file system by locking all mount points on all
// client nodes except the requesting node, flushing the DNLC, etc.
// Return an exception if the lock can't be acquired on all nodes or
// if there are still active proxy vnodes.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::unmount_1(fs::filesystem_ptr fs_obj, int32_t flags,
    solobj::cred_ptr credobj, fs::mount_client_ptr c, sol::nodeid_t nodeid,
    bool is_shutdown, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Unmount a global file system by locking all mount points on all
// client nodes except the requesting node, flushing the DNLC, etc.
// Return an exception if the lock can't be acquired on all nodes or
// if there are still active proxy vnodes.
// This operation should be idempotent since it can be retried on a new primary.
//
void
mount_server_impl::unmount_v1(pxfs_v1::filesystem_ptr fs_obj, int32_t flags,
    solobj::cred_ptr credobj, fs::mount_client_ptr client_p,
    sol::nodeid_t nodeid, bool is_shutdown, Environment &_environment)
{
	ASSERT(!CORBA::is_nil(fs_obj));
	ASSERT(!CORBA::is_nil(client_p));

	// Check to see if we have saved state.
	primary_ctx	*ctxp = primary_ctx::extract_from(_environment);
	unmount_state	*statep;
	if (ctxp != NULL &&
	    (statep = (unmount_state *)ctxp->get_saved_state()) != NULL) {
		//
		// Since we have saved state, we know the original
		// primary sent the ckpt_unmount_start() checkpoint.
		// We finish the unmount process from where we left off.
		//
		ASSERT(statep->unmount_ver == unmount_state::VERSION_1);
		unmount_orphaned(statep, true, _environment);
		return;
	}

	fs_elem			*fep;
	Environment		e;
	sol::error_t		error;

	client_list_lock.wrlock();
	fs_list_lock.wrlock();

	//
	// Since this is not a retry, we should find the file system entry
	// unless:
	// Node A starts unmounting file system F, gets list locks above.
	// Node B starts unmounting file system F, blocks waiting above.
	// Node B crashes.
	// Node A's unmount completes OK (since it either got the lock on B
	// or it got ECOMM and skipped B).
	// Node B's unmount unblocks above and we don't find the file system.
	//
	fep = find_fs(fs_obj);
	if (fep == NULL) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:unmount_v1 nid %d shutdown %d can't find FS\n",
		    nodeid, is_shutdown));
		fs_list_lock.unlock();
		client_list_lock.unlock();
		pxfslib::throw_exception(_environment, EINVAL);
		return;
	}
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:unmount_v1 %s nid %d shutdown %d fep %p\n",
	    (const char *)fep->ma.dir, nodeid, is_shutdown, fep));

	//
	// Ok, start an unmount.
	//
	get_checkpoint()->ckpt_unmount_start_v1(fs_obj, flags, credobj,
	    client_p, is_shutdown, _environment);
	ASSERT(_environment.exception() == NULL);

	error = unmount_common_1(client_p, fep, flags, credobj,
	    unmount_state::START, 0, NULL, _environment);

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    (error ? MOUNT_RED : MOUNT_GREEN),
	    ("server:unmount_v1 fep %p err %d\n",
	    fep, error));

	fs_list_lock.unlock();
	client_list_lock.unlock();

	if (error != 0) {
		pxfslib::throw_exception(_environment, error);
	}
}

//
// Checkpoint that a node is being shut down.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_unmount_shutdown(fs::mount_client_ptr client)
{
	mount_client_elem *cep = find_client(client);
	if (cep != NULL) {
		cep->shutdown = true;
	}
}

//
// Checkpoint the start of an unmount.
// Add_commit() should be called to finish the unmount process.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_unmount_start_v1(pxfs_v1::filesystem_ptr fs_obj,
    int32_t flags, solobj::cred_ptr credobj, fs::mount_client_ptr client,
    bool is_shutdown, Environment &env)
{
	//
	// We might have saved state from a previous ckpt_unmount_start().
	// if so, reuse the same transaction state; otherwise, create one.
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	unmount_state	*statep = NULL;
	if (ctxp == NULL ||
	    (statep = (unmount_state *)ctxp->get_saved_state()) == NULL) {
		statep = new unmount_state(fs_obj, flags, credobj, *this,
		    client);
		statep->register_state(env);
		env.clear();
	} else {
		ASSERT(!CORBA::is_nil(fs_obj));
		statep->skip = fs::mount_client::_duplicate(client);
		statep->fs_v1_obj = pxfs_v1::filesystem::_duplicate(fs_obj);
		statep->credobj = solobj::cred::_duplicate(credobj);
		statep->state = unmount_state::START;
		statep->error = 0;
		statep->service_name = (char *)NULL;
	}

	if (is_shutdown) {
		mount_client_elem	*cep = find_client(client);
		if (cep != NULL && !cep->shutdown) {
			cep->shutdown = true;
		}
	}
#ifdef DEBUG
	fs_elem		*fep = find_fs(fs_obj);
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:ckpt_unmount_start_v1 %s\n",
	    (const char *)fep->ma.dir));
#endif
}

//
// Helper routine for unmount_state::orphaned() to unlock mount points.
// This is also called on the primary when recovering from a failover.
//
void
mount_server_impl::unmount_orphaned(unmount_state *statep, bool ret_err,
    Environment &env)
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_AMBER,
	    ("server:unmount_orphaned %d\n", ret_err));
	if (statep->state == unmount_state::COMMITTED) {
		//
		// We saw the last checkpoint so we are done.
		// Return the error if requested and there is one.
		//
		if (ret_err && statep->error != 0) {
			pxfslib::throw_exception(env, statep->error);
		}
		return;
	}

	//
	// The file system is still mounted so unmount it.
	//
	client_list_lock.wrlock();
	fs_list_lock.wrlock();
	fs_elem		*fep;

	ASSERT(statep->unmount_ver == unmount_state::VERSION_1);
	fep = find_fs(statep->fs_v1_obj);
	statep->error = unmount_common_1(statep->skip, fep,
	    statep->flags, statep->credobj, statep->state,
	    statep->error, statep->service_name, env);
	fs_list_lock.unlock();
	client_list_lock.unlock();

	if (ret_err && statep->error != 0) {
		pxfslib::throw_exception(env, statep->error);
	}
}

//
// Version for forced unmount support
//
// Common code for add_client(), remove_client(), unmount(), and
// unmount_orphaned().
// The job is to unmount the file system 'fep' from all nodes, possibly
// skipping steps that have already been performed.
// Both client_list_lock and fs_list_lock should be held.
// If there is no error, 'fep' is removed from the list of all file systems.
//
sol::error_t
mount_server_impl::unmount_common_1(fs::mount_client_ptr skip, fs_elem *fep,
    int32_t flags, solobj::cred_ptr credobj, unmount_state::state_t state,
    sol::error_t error, const char *service_name, Environment &env)
{
	mount_client_elem	*cep;
	mount_client_elem	*end_cep = NULL;
	fs_elem			*delete_fep = NULL;
	bool			skip_purge;
	Environment		e;

	ASSERT(client_list_lock.write_held());
	ASSERT(fs_list_lock.write_held());
	ASSERT(fep->fs_elem_ver == fs_elem::VERSION_1);

	switch (state) {
	case unmount_state::START:
		//
		// Make sure the mount point is locked and there
		// are no active vnodes on each client.
		//
		client_list.atfirst();
		while ((cep = client_list.get_current()) != NULL) {
			client_list.advance();

			//
			// Note that for the node which called the umount
			// system call we must skip most of the unmount
			// preparation (filesystem locking and vnode cache
			// purging) since that has already been done there.
			//
			skip_purge = false;
			if (cep->clientptr->_equiv(skip)) {
				skip_purge = true;
			}

			// Try to get the lock on this node.
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:prepare_unmount_1 %s on %d flags = %x\n",
			    (const char *)fep->ma.dir, cep->nodeid, flags));
			cep->clientptr->prepare_unmount_v1(
			    fep->fs_v1_ptr, flags, credobj, skip_purge, e);
			if (e.exception() == NULL) {
				continue;
			}
			if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_RED,
				    ("server:prepare_unmount_1 "
				    "%s on %d COMM_FAILURE\n",
				    (const char *)fep->ma.dir, cep->nodeid));
				e.clear();
				continue;
			}

			error = pxfslib::get_err(e);
			e.clear();
			//
			// Unlock clients that have already been
			// locked successfully.
			//
			end_cep = cep;
			get_checkpoint()->ckpt_unmount_middle(error, env);
			ASSERT(env.exception() == NULL);
			goto notify;
		}

		FAULTPT_PXFS(FAULTNUM_PXFS_UNMOUNT_C_B,
		    FaultFunctions::generic);

		//
		// Try to unmount the file system.
		// XXX Temporary dependency on the file system here.
		//
		error = 0;
		fep->fs_v1_ptr->unmount(flags, credobj, e);

		if (e.exception()) {
			if (CORBA::VERSION::_exnarrow(e.exception())) {
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_RED,
				    ("server:unmount_common_1 "
				    "fs version exception %d\n"));
				error = EBUSY;
			} else if (CORBA::COMM_FAILURE::_exnarrow(
			    e.exception())) {
				//
				// If the server for a file system is dead,
				// and no clients have active vnodes, we
				// allow the unmount to happen.
				//
				error = 0;
			} else {
				error = pxfslib::get_err(e);
				//
				// For a forced unmount we always set error
				// to zero (except for ENOTSUP) so the global
				// unmount will always succeed. ENOTSUP is the
				// legitimate return from underlying file
				// systems not supporting forced unmount.
				//
				if ((flags & MS_FORCE) && (error != ENOTSUP)) {
					MOUNT_DBPRINTF(
					    MOUNT_TRACE_SERVER,
					    MOUNT_RED,
					    ("server:unmount_common_1"
					    "forced unmount error %d\n",
					    error));
					error = 0;
				}
			}
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:unmount_common_1 fs exception %d\n",
			    error));
			e.clear();
		}

		get_checkpoint()->ckpt_unmount_middle(error, env);
		ASSERT(env.exception() == NULL);
		// FALL THROUGH

	case unmount_state::UNMOUNTED:
	notify:
		//
		// Notify mount clients of the unmount result.
		//
		client_list.atfirst();
		while ((cep = client_list.get_current()) != end_cep) {
			client_list.advance();

			//
			// Notify clients of unmount success or failure so
			// they can proceed appropriately.
			// In order to handle forced unmounts correctly,
			// we need to notify all clients that the unmount
			// succeeded.
			//
			// Note that the node which called the
			// umount() system call will unlink the vfs_t from the
			// file system name space so that node is treated
			// differently from the others.
			//
			if (error == 0) {
				cep->clientptr->remove_notify_1(fep->ma.dir,
				    !cep->clientptr->_equiv(skip), e);
			} else {
				if (!cep->clientptr->_equiv(skip)) {
					cep->clientptr->
					    unmount_failed_1(false, e);
				} else {
					cep->clientptr->
					    unmount_failed_1(true, e);
				}
			}
			e.clear();
		}

		//
		// Tell the secondary that all clients have been notified
		// before shutting down the file system service so that
		// the HA object isn't marshalled after the shutdown.
		//
		if (error == 0) {
			if (fep->dev_is_ha) {
				// Note: service_name shares string with "fep".
				service_name = (const char *)fep->ma.spec;
			}

			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:unmount_common_1 remove %p\n",
			    fep));
			//
			// Note that erase() does an advance() if
			// fep == get_current().
			//
			(void) fs_list.erase(fep);
			delete_fep = fep;
		}

		FAULTPT_PXFS(FAULTNUM_PXFS_UNMOUNT_C_A,
		    FaultFunctions::generic);

		get_checkpoint()->ckpt_unmount_notified(env);
		ASSERT(env.exception() == NULL);
		// FALLTHROUGH

	case unmount_state::NOTIFIED:
		//
		// Shut down the FS service.
		//
		if (service_name != NULL) {
			replica::service_admin_var sa =
			    pxfslib::get_service_admin_ref(
				"mount_server_impl::unmount_common_1",
				service_name, e);
			if (e.exception()) {
				//
				// XXX Need to shut down this service but
				// can't get the service_admin object to do it.
				//
				MOUNT_DBPRINTF(
				    MOUNT_TRACE_SERVER,
				    MOUNT_RED,
				    ("server:unmount_common_1 "
				    "get_service_admin_ref exception\n"));
				e.clear();
			} else {
				if ((flags & MS_FORCE) == 0) {
					sa->shutdown_service(false, e);
				} else {
					sa->shutdown_service(true, e);
				}
				if (e.exception()) {
					MOUNT_DBPRINTF(
					    MOUNT_TRACE_SERVER,
					    MOUNT_RED,
					    ("server:unmount_com_1 "
					    "shutdown_service() exception\n"));
					e.clear();
				}
			}
			FAULTPT_PXFS(FAULTNUM_PXFS_UNMOUNT_C_E,
			    FaultFunctions::generic);
			get_checkpoint()->ckpt_unmount_end(env);
			ASSERT(env.exception() == NULL);
		}
		break;

	default:
		ASSERT(0);
	}

	if (delete_fep != NULL) {
		//
		// We delay deleting fep until now since service_name could
		// be pointing to the string that would be freed.
		//
		delete delete_fep;
	}

	return (error);
}

//
// Checkpoint that either the mount points have been locked
// and the file system has been unmounted or there was an error.
// We have yet to notify the client nodes of the result.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_unmount_middle(sol::error_t error, Environment &env)
{
	//
	// We should have saved state since ckpt_unmount_start() is
	// supposed to be called first.
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	ASSERT(ctxp != NULL);
	unmount_state	*statep = (unmount_state *)ctxp->get_saved_state();
	ASSERT(statep != NULL);

	//
	// state == UNMOUNTED means that 'error' is valid and
	// don't retry to lock mount points or unmount the file system again.
	//
	statep->state = unmount_state::UNMOUNTED;
	statep->error = error;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:ckpt_unmount_middle error %d\n", error));
}

//
// Checkpoint that all the client nodes have been notified (if there was
// no unmount error) before shutting down the file system service.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_unmount_notified(Environment &env)
{
	//
	// We should have saved state since ckpt_unmount_start() is
	// supposed to be called first.
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	ASSERT(ctxp != NULL);
	unmount_state	*statep = (unmount_state *)ctxp->get_saved_state();
	ASSERT(statep != NULL);
	ASSERT(statep->unmount_ver == unmount_state::VERSION_1);

	if (CORBA::is_nil(statep->fs_v1_obj)) {
		// This is a duplicate checkpoint after a failover.
		return;
	}
	if (statep->error == 0) {
		fs_elem		*fep;
		fep = find_fs(statep->fs_v1_obj);
		ASSERT(fep != NULL);
		//
		// state == NOTIFIED means that the mount points have
		// been unlocked and that the only remaining task is
		// to shut down the file system service.
		//
		if (fep->dev_is_ha) {
			statep->state = unmount_state::NOTIFIED;
			statep->service_name = os::strdup(fep->ma.spec);
		} else {
			statep->state = unmount_state::COMMITTED;
		}

		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:ckpt_unmount_notified remove %p\n",
		    fep));
		(void) fs_list.erase(fep);
		delete fep;
	} else {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:ckpt_unmount_notified\n"));
		statep->state = unmount_state::COMMITTED;
	}
	statep->fs_v1_obj = pxfs_v1::filesystem::_nil();
}

//
// Checkpoint that unmount is complete. We can't use a commit() call since
// this may be called more than once when unmounting multiple file systems.
//
void
mount_server_impl::ckpt_unmount_end(Environment &env)
{
	//
	// We should have saved state since ckpt_unmount_start() is
	// supposed to be called first.
	//
	secondary_ctx	*ctxp = secondary_ctx::extract_from(env);
	ASSERT(ctxp != NULL);
	unmount_state	*statep = (unmount_state *)ctxp->get_saved_state();
	ASSERT(statep != NULL);

	statep->state = unmount_state::COMMITTED;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:ckpt_unmount_end\n"));
}

//
// Notify intent to mount a device.
// The server attempts to get a "lock" on the device
// and returns success if the file system is not mounted
// and no other node has the lock. The device remains locked
// until it is either mounted or the requesting node dies.
//
// mount_server_impl(fs::mount_server::devlock)
void
mount_server_impl::devlock(fs::mount_client_ptr c, sol::nodeid_t nodeid,
    const char *dev_name, Environment &_environment)
{
	fs_list_lock.wrlock();
	devlock_list_lock.wrlock();

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:devlock nid %d %s\n",
	    nodeid, dev_name));

	// Check to see if the device is locked.
	devlock_elem	*dep;
	while ((dep = find_devlock(dev_name)) != NULL) {
		if (dep->ownerid == nodeid) {
			//
			// The owner asked for the same lock again.
			// This could be a retry after a failover.
			// Either way, we allow it.
			//
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_AMBER,
			    ("server:devlock repeat\n"));
			devlock_list_lock.unlock();
			fs_list_lock.unlock();
			return;
		}

		//
		// Check to see if the service was frozen and make the
		// caller try again (so the IDL call completes and
		// the switchover can proceed).
		//
		if (frozen) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:devlock frozen\n"));
			devlock_list_lock.unlock();
			fs_list_lock.unlock();
			pxfslib::throw_exception(_environment, EAGAIN);
			return;
		}

		//
		// Check to see if the owner of the lock is blocked waiting.
		// This is a simple deadlock check which doesn't support
		// A waiting for B which is waiting for C even though this
		// case isn't a deadlock.
		//
		if (find_devlock_waiter(dep->ownerid)) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:devlock deadlock\n"));
			devlock_list_lock.unlock();
			fs_list_lock.unlock();
			pxfslib::throw_exception(_environment, EDEADLOCK);
			return;
		}

		//
		// Wait until the lock is released, then return
		// either lock granted/not granted if the FS is
		// not mounted/mounted. This is so we don't have
		// the situation where node A gets the lock, node B
		// doesn't get the lock (silently not attempting to mount FS),
		// then node A crashes without completing the mount.
		//
		os::condvar_t::wait_result	res = os::condvar_t::NORMAL;
		dep->waiter_lock.lock();
		dep->nwaiters++;
		dep->waiters.set(nodeid - 1);
		devlock_list_lock.unlock();
		fs_list_lock.unlock();
		while (res == os::condvar_t::NORMAL &&
		    !dep->unlocked && !frozen) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:devlock waiting %s\n",
			    dev_name));

			// Wait for pxfs_devlock_timeout seconds
			os::systime timeout;
			timeout.setreltime(pxfs_devlock_timeout * 1000000);
			res = dep->waiter_cv.timedwait_sig(&dep->waiter_lock,
			    &timeout);

			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:devlock wakeup %d u %d frozen %d\n",
			    res, dep->unlocked, frozen));
		}
		dep->waiters.clear(nodeid - 1);
		bool	last_waiter = (--dep->nwaiters == 0 && dep->unlocked);
		dep->waiter_lock.unlock();

		if (last_waiter) {
			delete dep;
		}

		if (res == os::condvar_t::TIMEDOUT) {
			//
			// The wait timed out. We return ETIMEDOUT.
			// The client prints a syslog message the first time it
			// sees ETIMEDOUT. It retries until it sees something
			// other than ETIMEDOUT and EAGAIN.
			//
			pxfslib::throw_exception(_environment, ETIMEDOUT);
			return;
		} else if (res == os::condvar_t::SIGNALED) {
			pxfslib::throw_exception(_environment, EINTR);
			return;
		}
		fs_list_lock.wrlock();
		devlock_list_lock.wrlock();
	}

	// Check to see if the device is mounted.
	if (find_fs(dev_name) != NULL) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:devlock busy\n"));
		devlock_list_lock.unlock();
		fs_list_lock.unlock();
		pxfslib::throw_exception(_environment, EBUSY);
		return;
	}

	//
	// Add a new device lock element to the list and checkpoint it.
	//
	dep = new devlock_elem(c, nodeid, dev_name);
	devlock_list.prepend(dep);
	get_checkpoint()->ckpt_devlock(c, nodeid, dev_name, _environment);
	ASSERT(_environment.exception() == NULL);
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:devlock added %p\n", dep));

	FAULTPT_PXFS(FAULTNUM_PXFS_DEVLOCK, FaultFunctions::generic);

	devlock_list_lock.unlock();
	fs_list_lock.unlock();
}

//
// Checkpoint a new device lock.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_devlock(fs::mount_client_ptr client,
    sol::nodeid_t nodeid, const char *dev_name)
{
	if (find_devlock(dev_name) == NULL) {
		//
		// Add a new device lock element to the list.
		//
		devlock_elem *dep = new devlock_elem(client, nodeid, dev_name);
		devlock_list.prepend(dep);
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:ckpt_devlock added nid %d %s %p\n",
		    nodeid, dev_name, dep));
	}
}

//
// Unlock a device.
// Note that if there are any nodes waiting for a device lock, it will
// prevent switchover of the mount service since there will be an uncompleted
// IDL call (which won't return until the lock is released).
//
void
mount_server_impl::devunlock(const char *dev_name, Environment &_environment)
{
	devlock_list_lock.wrlock();

	// Check to see if the device is locked.
	devlock_elem	*dep = find_devlock(dev_name);
	if (dep != NULL) {
		//
		// Remove lock element from the list and checkpoint it.
		//
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:devunlock %s unlock %p waiters %d\n",
		    dev_name, dep, dep->nwaiters));
		(void) devlock_list.erase(dep);
		get_checkpoint()->ckpt_devunlock(dev_name, _environment);
		ASSERT(_environment.exception() == NULL);

		FAULTPT_PXFS(FAULTNUM_PXFS_DEVUNLOCK, FaultFunctions::generic);

		dep->waiter_lock.lock();
		if (dep->nwaiters != 0) {
			// This wakes up all waiting threads.
			dep->unlocked = true;
			dep->waiter_cv.broadcast();
			dep->waiter_lock.unlock();

			// The last waiter will do the delete.
		} else {
			dep->waiter_lock.unlock();
			delete dep;
		}
	}

	devlock_list_lock.unlock();
}

//
// Checkpoint a device unlock.
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_devunlock(const char *dev_name)
{
	devlock_elem	*dep = find_devlock(dev_name);
	if (dep != NULL) {
		//
		// Remove device lock element from the list.
		//
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_GREEN,
		    ("server:ckpt_devunlock %s unlock %p\n",
		    dev_name, dep));
		(void) devlock_list.erase(dep);
		delete dep;
	}
}

//
// Get the nodeid of the node holding the lock on a device
//
void
mount_server_impl::get_devlock_owner(const char *dev_name,
    sol::nodeid_t &lock_owner, Environment &_environment)
{
	devlock_elem	*dep;

	devlock_list_lock.wrlock();

	// Check to see if the device is locked and has a owner
	dep = find_devlock(dev_name);
	if (dep == NULL) {
		lock_owner = 0;
	} else {
		lock_owner = dep->ownerid;
	}

	devlock_list_lock.unlock();

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:devlock owner (%d) %s\n",
	    lock_owner, dev_name));
}

//
// Wake up any threads waiting for a device lock so a swithover can happen.
// See comment in devlock() above.
//
void
mount_server_impl::freeze_primary()
{
	devlock_elem	*dep;
	devlock_list_lock.wrlock();
	frozen = true;
	for (devlock_list.atfirst();
	    (dep = devlock_list.get_current()) != NULL;
	    devlock_list.advance()) {
		dep->waiter_lock.lock();
		if (dep->nwaiters != 0) {
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:freeze wakeup %p waiters %d\n",
			    dep, dep->nwaiters));
			// This wakes up all waiting threads.
			dep->waiter_cv.broadcast();
		}
		dep->waiter_lock.unlock();
	}
	devlock_list_lock.unlock();
}

//
// Clear frozen state.
//
void
mount_server_impl::unfreeze_primary()
{
	devlock_list_lock.wrlock();
	frozen = false;
	devlock_list_lock.unlock();
}

//
// Return a handle to the DCS configuration callback object.
// The callback object is called by DCS to notify us when the
// device configuration changes.
//
fs::dc_callback_ptr
mount_server_impl::get_dc_callback(Environment &_environment)
{
	fs_list_lock.wrlock();
	if (CORBA::is_nil(dc_callback_obj)) {
		dc_callback_obj =
		    (new dc_callback_impl(*this, repl_serverp))->get_objref();
		get_checkpoint()->ckpt_get_dc_callback(dc_callback_obj,
		    _environment);
		ASSERT(_environment.exception() == NULL);
	}
	fs_list_lock.unlock();

	return (fs::dc_callback::_duplicate(dc_callback_obj));
}

//
// Helper function for checkpointing state on a secondary.
//
void
mount_server_impl::ckpt_get_dc_callback(fs::dc_callback_ptr cb)
{
	if (CORBA::is_nil(dc_callback_obj)) {
		// Note that the "delete dcp;" is done by _unreferenced().
		(void) new dc_callback_impl(*this, cb);
		dc_callback_obj = fs::dc_callback::_duplicate(cb);
	}
}

//
// Helper routine to update device configuration changes.
//
void
mount_server_impl::notify_change(sol::dev_t gdev,
    const sol::nodeid_seq_t &nodes, Environment &env)
{
	mount_client_elem	*cep;
	fs_elem			*fep;
	Environment		e;

	//
	// Search the list of globally mounted file systems for the device.
	//
	client_list_lock.wrlock();
	fs_list_lock.wrlock();
	for (fs_list.atfirst(); (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {

		ASSERT(fep->fs_elem_ver == fs_elem::VERSION_1);
		if (fep->fs_v1_info.fsdev != gdev) {
			continue;
		}
		//
		// We found the file system, start replicas on the new
		// nodes. Note that we don't try to shut down replicas
		// if the connection is removed.
		//
		for (uint32_t i = 0; i < nodes.length(); i++) {
			bool	fnd = false;
			for (uint32_t j = 0; j < fep->dev_nids.length(); j++) {
				if (nodes[i] == fep->dev_nids[j]) {
					fnd = true;
					break;
				}
			}
			if (fnd) {
				continue;
			}

			// Find the mount client pointer for device node i.
			cep = find_client(nodes[i]);
			if (cep == NULL || cep->shutdown) {
				//
				// The device node isn't up
				// at the moment.
				//
				continue;
			}
			// XXX kcred use.
			solobj::cred_var	credobj =
			    solobj_impl::conv(kcred);
			cep->clientptr->reinstantiate_ha_v1(fep->ma,
			    fep->fs_v1_ptr, credobj, fep->dev_name, e);
			//
			// We do the best we can to start a replica for
			// the new device connection but if there are errors,
			// we can't really do anything about it here. There
			// is a syslog message that is printed when a
			// new replica is started but if there was an error,
			// there may be no report of it.
			// XXX We could print a syslog error message here.
			//
			e.clear();
		}
		fep->dev_nids = nodes;
		FAULTPT_PXFS(FAULTNUM_PXFS_DEVICE_CHANGED,
		    FaultFunctions::generic);
		get_checkpoint()->ckpt_notify_change(gdev, nodes, env);
		ASSERT(env.exception() == NULL);
	}
	fs_list_lock.unlock();
	client_list_lock.unlock();
}

//
// Checkpoint a change in the disk connection list.
//
void
mount_server_impl::ckpt_notify_change(sol::dev_t gdev,
    const sol::nodeid_seq_t &dev_nids)
{
	fs_elem		*fep;

	//
	// Search the list of globally mounted file systems for the device.
	//
	for (fs_list.atfirst(); (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {
		if (fep->fs_v1_info.fsdev != gdev) {
			continue;
		}
		fep->dev_nids = dev_nids;
	}
}

//
// Helper routine to check if device is in use.
//
bool
mount_server_impl::still_active(sol::dev_t gdev)
{
	fs_elem	*fep;
	bool	inuse = false;

	//
	// Search the list of globally mounted file systems for the device.
	//
	fs_list_lock.wrlock();
	for (fs_list.atfirst(); (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {
		if (fep->fs_v1_info.fsdev == gdev) {
			inuse = true;
			break;
		}
	}
	fs_list_lock.unlock();

	return (inuse);
}

//
// Helper function for dumping state to a new secondary.
//
void
mount_server_impl::dump_state(repl_pxfs::mount_replica_ptr ckptp,
    Environment &env)
{
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_SERVER,
	    MOUNT_GREEN,
	    ("server:dump_state\n"));

	// Create a new mount server on the secondary.
	CORBA::type_info_t *typ = fs::mount_server::_get_type_info(
	    mount_vp_to_idl[repl_serverp->current_version.major_num]
		[repl_serverp->current_version.minor_num].ms);
	fs::mount_server_var fsls = get_objref(typ);
	ckptp->ckpt_new_server(fsls, env);
	if (env.exception()) {
#ifdef DEBUG
		env.exception()->print_exception
		    ("mount_server_impl:dump_state "
		    "ckpt_new_server");
#endif
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:dump_state(%p): "
		    "exception '%s' while calling ckpt_new_server().\n",
		    this, env.exception()->_name()));
		ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()));
		return;
	}
	//
	// Dump the current list of mount clients.
	//
	mount_client_elem	*cep;

	for (client_list.atfirst(); (cep = client_list.get_current()) != NULL;
	    client_list.advance()) {
		//
		// Note that the mount_client_elem can get a second
		// call to _unreferenced() if 'unref' is set since we
		// get a new reference here and then release it.
		// We need to be sure to not delete the object until
		// _unreferenced() is called again.
		//
		fs::mount_client_died_var	clobj = cep->get_objref();
		ckptp->ckpt_add_client(clobj, cep->clientptr, cep->nodeid,
		    cep->shutdown, env);
		if (env.exception()) {
#ifdef DEBUG
			env.exception()->print_exception
			    ("mount_server_impl:dump_state "
			    "ckpt_add_client");
#endif
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:dump_state(%p): "
			    "exception '%s' while calling ckpt_add_client().\n",
			    this, env.exception()->_name()));
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()));
			return;
		}
	}

	//
	// Dump the current list of mounted file systems.
	//
	fs_elem		*fep;
	for (fs_list.atfirst(); (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {
		ASSERT(fep->fs_elem_ver == fs_elem::VERSION_1);
		ckptp->ckpt_mount_v1(fep->fs_v1_ptr, fep->fs_v1_info,
		    fep->ma,
		    fep->mntoptions, fep->dev_is_ha, fep->dev_name,
		    fep->dev_nids, env);
		if (env.exception()) {
#ifdef DEBUG
			env.exception()->print_exception
			    ("mount_server_impl:dump_state "
			    "ckpt_mount");
#endif
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:dump_state(%p): "
			    "exception '%s' while calling ckpt_mount().\n",
			    this, env.exception()->_name()));
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()));
			return;
		}
	}

	//
	// Dump the list of device locks.
	//
	devlock_elem	*dep;
	for (devlock_list.atfirst();
	    (dep = devlock_list.get_current()) != NULL;
	    devlock_list.advance()) {
		ckptp->ckpt_devlock(dep->owner, dep->ownerid, dep->spec, env);
		if (env.exception()) {
#ifdef DEBUG
			env.exception()->print_exception
			    ("mount_server_impl:dump_state "
			    "ckpt_dev_lock");
#endif
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_RED,
			    ("server:dump_state(%p): "
			    "exception '%s' while calling ckpt_devlock().\n",
			    this, env.exception()->_name()));
			ASSERT(CORBA::COMM_FAILURE::_exnarrow(env.exception()));
			return;
		}
	}

	//
	// Create a dc_callback object if needed.
	//
	if (!CORBA::is_nil(dc_callback_obj)) {
		ckptp->ckpt_get_dc_callback(dc_callback_obj, env);
	}
}

//
// Helper routine to find a mount_client_elem given a node number.
// The client_list_lock should be held before calling this on the primary.
//
mount_client_elem *
mount_server_impl::find_client(nodeid_t nid)
{
	mount_client_elem	*cep;

	ASSERT(!primary || client_list_lock.write_held());

	client_list.atfirst();
	while ((cep = client_list.get_current()) != NULL) {
		client_list.advance();
		if (cep->nodeid == nid) {
			return (cep);
		}
	}
	return (NULL);
}

//
// Helper routine to find a mount_client_elem given a mount client.
// The client_list_lock should be held before calling this on the primary.
//
mount_client_elem *
mount_server_impl::find_client(fs::mount_client_ptr c)
{
	mount_client_elem	*cep;

	ASSERT(!primary || client_list_lock.write_held());

	client_list.atfirst();
	while ((cep = client_list.get_current()) != NULL) {
		client_list.advance();
		if (cep->clientptr->_equiv(c)) {
			return (cep);
		}
	}
	return (NULL);
}

//
// Return the fs_elem for fs or NULL if not in fs_list.
// Must be called with the fs_list_lock held if called on the primary.
//
fs_elem *
mount_server_impl::find_fs(pxfs_v1::filesystem_ptr fsptr)
{
	fs_elem		*fep;

	ASSERT(!primary || fs_list_lock.write_held());

	for (fs_list.atfirst(); (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {
		if (!CORBA::is_nil(fep->fs_v1_ptr) &&
		    fep->fs_v1_ptr->_equiv(fsptr)) {
			return (fep);
		}
	}

	return (NULL);
}

//
// Return the fs_elem for fs or NULL if not in fs_list.
// Must be called with the fs_list_lock held if called on the primary.
//
fs_elem *
mount_server_impl::find_fs(const char *spec)
{
	fs_elem *fep;

	ASSERT(!primary || fs_list_lock.write_held());

	for (fs_list.atfirst(); (fep = fs_list.get_current()) != NULL;
	    fs_list.advance()) {
		if (strcmp(fep->ma.spec, spec) == 0) {
			return (fep);
		}
	}

	return (NULL);
}

//
// Return the devlock_elem or NULL if not in devlock_list.
// Must be called with the devlock_list_lock held if called on the primary.
//
devlock_elem *
mount_server_impl::find_devlock(const char *spec)
{
	devlock_elem *dep;

	ASSERT(!primary || devlock_list_lock.write_held());

	for (devlock_list.atfirst();
	    (dep = devlock_list.get_current()) != NULL;
	    devlock_list.advance()) {
		if (strcmp(dep->spec, spec) == 0) {
			return (dep);
		}
	}

	return (NULL);
}

//
// Return true if the given node is blocked waiting for any lock.
// Must be called with the devlock_list_lock held if called on the primary.
//
bool
mount_server_impl::find_devlock_waiter(sol::nodeid_t nodeid)
{
	devlock_elem *dep;

	ASSERT(!primary || devlock_list_lock.write_held());

	for (devlock_list.atfirst();
	    (dep = devlock_list.get_current()) != NULL;
	    devlock_list.advance()) {
		if (dep->waiters.test(nodeid - 1)) {
			return (true);
		}
	}

	return (false);
}

//
// Helper function to catch the case where a user issues
// multiple remount commands at the same time.
//
void
mount_server_impl::check_multiple_remounts(const char *pathp,
    Environment &_environment)
{
	char 	*pathmax = NULL, *pathmin = NULL;
	size_t	minlen = 0;

	current_mount_lock.lock();

	while (currentmnt) {
		if (strlen(pathp) > strlen(currentmnt)) {
			pathmax = (char *)pathp; // const string
			pathmin = currentmnt;
		} else {
			pathmax = currentmnt;
			pathmin = (char *)pathp; // const string
		}

		minlen = strlen(pathmin);

		// Use strncmp() so that nested mounts can be compared
		if (strncmp(pathmin, pathmax, minlen) == 0 &&
		    (pathmax[minlen] == '/' || pathmax[minlen] == '\0')) {
			sol::error_t error = EBUSY;

			pxfslib::throw_exception(_environment, error);

			current_mount_lock.unlock();

			return;
		}
		currentmnt_cv.wait(&current_mount_lock);

	}
	currentmnt = os::strdup(pathp);

	current_mount_lock.unlock();
}

//
// An HA filesystem is considered available if there is at least one existing
// primary or secondary replica. This function return availability status.
//
//lint -e1038
mount_server_impl::fs_status_t
mount_server_impl::get_fs_status(fs_elem *fep)
{
//lint +e1038
	Environment e;
	ASSERT(fep->dev_is_ha);

	// Get a reference to the service admin.
	replica::service_admin_var sa =
	    pxfslib::get_service_admin_ref("mount_server_impl::mount",
		(const char *)fep->ma.spec, e);
	if (e.exception()) {
		e.exception()->print_exception
		    ("mount_server_impl:get_fs_status "
		    "get_service_admin_ref");
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:get_fs_status "
		    "get_service_admin_ref(%s) exception\n",
		    (const char *)fep->ma.spec));
		return (ERROR);
	}
	e.clear();

	//
	// Get information about the filesystem replicas
	// from the service admin.
	//
	replica::repl_prov_seq_var repl_provs;
	sa->get_repl_provs(repl_provs, e);
	if (e.exception()) {
		e.exception()->print_exception
		    ("mount_sever_impl:get_fs_status "
		    "get_repl_provs");
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("sever:get_fs_status "
		    "get_repl_provs returned exception\n"));
		return (ERROR);
	}

	//
	// Check if primary or secondary replica exists,
	//
	uint_t len = repl_provs.length();
	for (uint_t i = 0; i < len; i++) {
		if ((repl_provs[i].curr_state
		    == replica::AS_PRIMARY) ||
		    (repl_provs[i].curr_state
		    == replica::AS_SECONDARY)) {
			//
			// From the service admin we found out that,
			// there is a potential replica that could
			// serve out the filesystem.
			//
			//
			MOUNT_DBPRINTF(
			    MOUNT_TRACE_SERVER,
			    MOUNT_GREEN,
			    ("server:get_fs_status "
			    "%s can host %s\n",
			    (const char*)repl_provs[i]
			    .repl_prov_desc,
			    (const char *)fep->ma.spec));
			return (AVAILABLE);
			}
		}
	return (NOT_AVAILABLE);
}

//
// Checkpoint the upgrade of a mount_client in client list.
//
void
mount_server_impl::ckpt_upgrade_client_list(fs::mount_client_ptr
    client_p, sol::nodeid_t nodeid)
{
	mount_client_elem	*cep;

	if ((cep = find_client(nodeid)) != NULL) {
		//
		// We found the guy we're looking for.
		//
		cep->clientptr = fs::mount_client::_duplicate(client_p);
	} else {
		//
		// Notify that we are not able to find the guy.
		//
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:ckpt_upgrade_client_list"
		    "Failed to find required mount_client_elem"
		    "with nodeid %d\n", nodeid));
	}
}

//
// Checkpoint the upgrade of a mount_client in devlock list.
//
void
mount_server_impl::ckpt_upgrade_devlock_list(const char *dev_name,
    fs::mount_client_ptr client_p)
{
	devlock_elem		*dep;

	if ((dep = find_devlock(dev_name)) != NULL) {
		//
		// We found the guy we're looking for.
		//
		dep->owner = fs::mount_client::_duplicate(client_p);
	} else {
		//
		// Notify that we are not able to find the guy.
		//
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_SERVER,
		    MOUNT_RED,
		    ("server:ckpt_upgrade_devlock_list"
		    "Failed to find required devlock_elem"
		    "with devname %s\n", dev_name));
	}
}
