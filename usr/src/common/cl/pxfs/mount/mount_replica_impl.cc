//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mount_replica_impl.cc	1.45	08/05/20 SMI"

#include <sys/vm_util.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>
#include <pxfs/common/pxfslib.h>
#include <pxfs/mount/mount_replica_impl.h>
#include <pxfs/mount/mount_server_impl.h>
#include <pxfs/mount/mount_debug.h>

//
// The struct  has an entry for each IDL interface which is being versioned. For
// a given VP major and minor version, we get the IDL version of those
// interfaces.
//
mount_ver_map_t mount_vp_to_idl[MOUNT_VP_MAX_MAJOR + 1][MOUNT_VP_MAX_MINOR +1] =
{
	{   { 0, 0 },		// VP Version 0.0 defined for indexing
	    { 0, 0 },		// VP Version 0.1   "      "     "
	    { 0, 0 }  },	// VP Version 0.2   "      "     "
	{   { 0, 0 },		// VP Version 1.0
	    { 1, 1 },		// VP Version 1.1
	    { 1, 1 }  },	// VP Version 1.2
	{   { 2, 2 },		// VP Version 2.0 Object Consolidation
	    { 2, 2},		// VP Version 2.1 currently unused
	    { 2, 2 }  }		// VP Version 2.2 currently unused
};

//
// Create a mount_replica_impl object.
//
mount_replica_impl::mount_replica_impl(const char *id) :
	repl_server<repl_pxfs::mount_replica>("mount", id),
	_ckpt_proxy(NULL)
{
	mount_serverp = NULL;

	//
	// We initialize the current version to an invalid version.
	// mount_replica_impl::startup()  will query the version manager
	// for the correct values.
	//
	current_version.major_num = 0;
	current_version.minor_num = 0;
	pending_version.major_num = 0;
	pending_version.minor_num = 0;
}

mount_replica_impl::~mount_replica_impl()
{
	mount_serverp = NULL;
	_ckpt_proxy = NULL;
}

repl_pxfs::mount_replica_ptr
mount_replica_impl::get_checkpoint_mount_replica()
{
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	return (_ckpt_proxy);
}

//
// Become the primary.
// Note that previously we might have been newly created or
// a secondary that is switching to primary.
//
void
mount_replica_impl::become_primary(const replica::repl_name_seq &,
    Environment &_environment)
{
	//
	// The following environment is for checkpointing to the secondary
	// We use a different environment for checkpointing because
	// mount_server_impl::upgrade_client_reference() clears the
	// environment context passed to it.
	//
	Environment		ckpt_environment;

	//
	// Create a primary context so the provider can send
	// checkpoints while the service is frozen.
	// XXX change the primary_ctx::invoke_env type.
	//
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT,
	    ckpt_environment);

	// First, initialize the checkpoint proxy.
	version_lock.wrlock();

	// Callback may have occured when this replica was a secondary
	if (pending_version.major_num != 0) {
		current_version = pending_version;
	}
	CORBA::type_info_t	*typ = repl_pxfs::mount_replica::_get_type_info(
	    mount_vp_to_idl[current_version.major_num]
		[current_version.minor_num].ms_ckpt);
	replica::checkpoint_var		tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_pxfs::mount_replica::_narrow(tmp_ckpt_v);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	//
	// If we were a secondary.
	//
	if (mount_serverp != NULL) {
		//
		// If there was an unprocessed upgrade callback pending,
		// finish the work and send a checkpoint to notify the
		// other secondaries.
		//
		if (pending_version.major_num != 0) {
			pending_version.major_num = 0;

			// Update the server reference.
			typ = fs::mount_server::_get_type_info(
			    mount_vp_to_idl[current_version.major_num]
				[current_version.minor_num].ms);
			mount_server_v = mount_serverp->get_objref(typ);

			// Update the client reference in the mount_server_impl
			mount_serverp->upgrade_client_reference(_environment);

			// Checkpoint current version.
			_ckpt_proxy->ckpt_service_version(
			    current_version.major_num,
			    current_version.minor_num, ckpt_environment);
			ckpt_environment.clear();
		}
		version_lock.unlock();

		mount_serverp->convert_to_primary();

		//
		// Show that the context is no longer there,
		// so that the destructor will see that everything is clean.
		//
		ckpt_environment.trans_ctxp = NULL;

		return;
	}

	//
	// Create a new mount server implementation object.
	//
	mount_serverp = new mount_server_impl(this);
	typ = fs::mount_server::_get_type_info(
	    mount_vp_to_idl[current_version.major_num]
		[current_version.minor_num].ms);
	mount_server_v = mount_serverp->get_objref(typ);

	// Checkpoint the creation of the first mount server object.
	mount_serverp->get_checkpoint()->ckpt_new_server(mount_server_v,
	    ckpt_environment);
	ckpt_environment.clear();

	//
	// If an old primary failed while an upgrade callback was in
	// progress and before the service version was checkpointed, we
	// do the service version checkpointing now.
	//
	if (pending_version.major_num != 0) {
		pending_version.major_num = 0;
		_ckpt_proxy->ckpt_service_version(
		    current_version.major_num,
		    current_version.minor_num, ckpt_environment);
	}
	version_lock.unlock();

	//
	// Show that the context is no longer there,
	// so that the destructor will see that everything is clean.
	//
	ckpt_environment.trans_ctxp = NULL;
}

//
// Become the secondary.
//
void
mount_replica_impl::become_secondary(Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->convert_to_secondary();

	version_lock.wrlock();
	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
	version_lock.unlock();
}

void
mount_replica_impl::add_secondary(replica::checkpoint_ptr sec_chkpt,
    const char *, Environment &_environment)
{
	ASSERT(mount_serverp != NULL);

	repl_pxfs::mount_replica_var ckpt =
		repl_pxfs::mount_replica::_narrow(sec_chkpt);
	ASSERT(!CORBA::is_nil(ckpt));

	// dump current state to the new secondary
	mount_serverp->dump_state(ckpt, _environment);
	if (_environment.exception()) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("replica:exception '%s' in dump_state() \n",
		    _environment.exception()->_name()));
	}
	_environment.clear();
}

void
mount_replica_impl::remove_secondary(const char *, Environment &)
{
}

void
mount_replica_impl::freeze_primary_prepare(Environment &)
{
}

void
mount_replica_impl::freeze_primary(Environment &)
{
	//
	// Start freezing. The situation we are trying to prevent is
	// where a call to devlock() is waiting for the lock and
	// then a swithover is requested. The switchover won't complete
	// until the devlock() returns but that can't happen since the
	// devunlock() is blocked until the switchover completes.
	//
	if (mount_serverp != NULL) {
		mount_serverp->freeze_primary();
	}
}

void
mount_replica_impl::unfreeze_primary(Environment &)
{
	if (mount_serverp != NULL) {
		mount_serverp->unfreeze_primary();
	}
}

void
mount_replica_impl::become_spare(Environment &)
{
	if (mount_serverp != NULL) {
		// Drop our reference to the object before doing a delete.
		mount_server_v = fs::mount_server::_nil();
		mount_serverp->convert_to_spare();
		//
		// mount_serverp is cleaned up in convert_to_spare(),
		// so the next line is not a memory leak.
		//lint -e423
		mount_serverp = NULL;
		//lint +e423
	}
}

//
// This is called on the primary when the service is requested to shutdown.
//
void
mount_replica_impl::shutdown(Environment &_environment)
{
	// We should never shut down.
	_environment.exception(new replica::service_busy);

	//
	// Since we are returning an exception, we will remain primary,
	// so don't release _ckpt_proxy.
	//
}

//
// This is called on the primary to return a reference to the root
// HA object (in this case, fs::mount_server).
//
CORBA::Object_ptr
mount_replica_impl::get_root_obj(Environment &)
{
	ASSERT(mount_serverp != NULL);
	version_lock.wrlock();
	CORBA::type_info_t *typ = fs::mount_server::_get_type_info(
	    mount_vp_to_idl[current_version.major_num]
		[current_version.minor_num].ms);
	version_lock.unlock();
	return (mount_serverp->get_objref(typ));
}

//
// Process an upgrade callback from the version manager.
// This call can happen before the HA replica is registered with the
// replica manager (i.e., the HA sevice is not started yet or is a spare),
// or as primary or secondary. The callback is not synchronized with
// calls to become_primary(), become_secondary(), etc. and failovers can
// happen while the service is frozen so we need to make sure that the
// "upgrade work" is done if there are node failures.
// If this replica is the primary, we do the work and send a checkpoint
// to indicate the work is complete. If this replica is a secondary and
// the callback happens before we get the checkpoint from the primary,
// we record that the callback happened so that become_primary() can
// do the "upgrade work" and send the checkpoint. If this replica is a
// secondary and the callback happens after the checkpoint is received,
// we ignore the callback. The checkpoint routine on the secondary clears
// the "flag" that the callback sets so no extra work is done in
// become_primary() if the old primary fails after completing the upgrade
// callback work.
//
void
mount_replica_impl::upgrade_callback(
    const version_manager::vp_version_t &version, Environment &e)
{
	CORBA::type_info_t *typ;

	//
	// Note that upgrade callbacks are not synchronized with
	// calls to become_primary(), add_secondary(), etc.
	// Getting this lock makes sure the replica state doesn't change.
	//
	version_lock.wrlock();
	if (current_version.major_num > version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num >= version.minor_num)) {
		// Version isn't changing so just return.
		version_lock.unlock();
		return;
	}

	//
	// If this replica is not the primary, set the pending version
	// to be used if there is a failover before the primary checkpoints
	// a new version.
	//
	if (CORBA::is_nil(_ckpt_proxy)) {
		// If this replica is a secondary, set the pending version.
		if (mount_serverp != NULL) {
			pending_version = version;
		} else {
			// Replica must be a spare.
			current_version = version;
		}
		version_lock.unlock();
		return;
	}

	// We are the primary and the version has changed.
	current_version = version;
	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("replica:upgrade_callback major = %d minor = %d ",
	    current_version.major_num, current_version.minor_num));

	//
	// Switch the checkpoint interface to the new protocol. Save
	// the current _ckpt_proxy and release it after we get a new one
	//
	repl_pxfs::mount_replica_ptr old_ckpt_p = _ckpt_proxy;

	typ = repl_pxfs::mount_replica::_get_type_info(
	    mount_vp_to_idl[current_version.major_num]
		[current_version.minor_num].ms_ckpt);

	MOUNT_DBPRINTF(
	    MOUNT_TRACE_REPLICA,
	    MOUNT_GREEN,
	    ("replica:upgrade_callback type_info_t ptr = %x",
	    typ));

	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = repl_pxfs::mount_replica::_narrow(tmp_ckpt_v);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	CORBA::release(old_ckpt_p);

	// Update the server reference.
	typ = fs::mount_server::_get_type_info(
	    mount_vp_to_idl[current_version.major_num]
		[current_version.minor_num].ms);
	mount_server_v = mount_serverp->get_objref(typ);

	// Update the client reference in the mount_server_impl
	mount_serverp->upgrade_client_reference(e);

	//
	// Create and add a primary context so the provider can send
	// checkpoints while the service is frozen.
	// XXX change the primary_ctx::invoke_env type.
	//
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, e);

	// Checkpoint the current version number.
	_ckpt_proxy->ckpt_service_version(
	    current_version.major_num,
	    current_version.minor_num, e);

	e.trans_ctxp = NULL;

	version_lock.unlock();
}

//
// Checkpoint the creation of a new mount server.
//
void
mount_replica_impl::ckpt_new_server(fs::mount_server_ptr server,
    Environment &)
{
	//
	// Create a new mount server (we are a secondary).
	//
	if (mount_serverp == NULL) {
		//lint -e423
		mount_serverp = new mount_server_impl(this, server);
		mount_server_v = fs::mount_server::_duplicate(server);
		//lint +e423
	}
}

//
// Checkpoint the addition of a new mount client.
//
void
mount_replica_impl::ckpt_add_client(fs::mount_client_died_ptr clobj,
    fs::mount_client_ptr client, sol::nodeid_t nodeid, bool is_shutdown,
    Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_add_client(clobj, client, nodeid, is_shutdown);
}

//
// Checkpoint the removal of a mount_client.
//
void
mount_replica_impl::ckpt_remove_client(fs::mount_client_ptr client,
    Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_remove_client(client);
}

//
// Checkpoint the start of a mount or remount.
//
void
mount_replica_impl::ckpt_mount_start(const sol::mounta &ma,
    fs::mount_client_ptr client, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Checkpoint the start of a mount or remount.
//
void
mount_replica_impl::ckpt_mount_start_v1(const sol::mounta &ma,
    fs::mount_client_ptr client, Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_mount_start_v1(ma, client, _environment);
}

//
// Checkpoint a mount error.
// This is used to complete a ckpt_mount_start().
//
void
mount_replica_impl::ckpt_mount_err(sol::error_t error,
    Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_mount_err(error, _environment);
}

//
// Checkpoint the addition of a new file system (mount).
// This is used to dump state to a newly joining secondary.
//
void
mount_replica_impl::ckpt_mount(fs::filesystem_ptr fsptr,
    const fs::fs_info &fsinfo, const sol::mounta &ma, const char *mntoptions,
    bool dev_is_ha, const char *dev_name,
    const sol::nodeid_seq_t &dev_nids, Environment &)
{
	CL_PANIC(0);
}

//
// Checkpoint the addition of a new file system (mount).
// This is used to dump state to a newly joining secondary.
//
void
mount_replica_impl::ckpt_mount_v1(pxfs_v1::filesystem_ptr fsptr,
    const pxfs_v1::fs_info &fsinfo, const sol::mounta &ma,
    const char *mntoptions, bool dev_is_ha, const char *dev_name,
    const sol::nodeid_seq_t &dev_nids, Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_mount_v1(fsptr, fsinfo, ma, mntoptions, dev_is_ha,
	    dev_name, dev_nids);
}

//
// Checkpoint the creation of a new file system object.
//
void
mount_replica_impl::ckpt_mount_middle(fs::filesystem_ptr fsptr,
    const fs::fs_info &fsinfo, const char *mntoptions, bool dev_is_ha,
    const char *dev_name, const sol::nodeid_seq_t &dev_nids,
    Environment &_environment)
{
	CL_PANIC(0);
}

//
// Checkpoint the creation of a new file system object.
//
void
mount_replica_impl::ckpt_mount_middle_v1(pxfs_v1::filesystem_ptr fsptr,
    const pxfs_v1::fs_info &fsinfo, const char *mntoptions, bool dev_is_ha,
    const char *dev_name, const sol::nodeid_seq_t &dev_nids,
    Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_mount_middle_v1(fsptr, fsinfo, mntoptions,
	    dev_is_ha, dev_name, dev_nids, _environment);
}

//
// Checkpoint the changes to vfs_flag and /etc/mnttab due to a remount.
//
void
mount_replica_impl::ckpt_remount_middle(fs::filesystem_ptr fsptr,
    uint32_t vfsflags, const char *mntoptions, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Checkpoint the changes to vfs_flag and /etc/mnttab due to a remount.
//
void
mount_replica_impl::ckpt_remount_middle_v1(pxfs_v1::filesystem_ptr fsptr,
    uint32_t vfsflags, const char *mntoptions, Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_remount_middle_v1(fsptr, vfsflags, mntoptions,
	    _environment);
}

//
// Checkpoint the start of an unmount.
//
void
mount_replica_impl::ckpt_unmount_start(fs::filesystem_ptr fsptr,
    solobj::cred_ptr credobj, fs::mount_client_ptr client,
    bool is_shutdown, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Checkpoint the start of an unmount.
//
// mount_replica_impl(repl_pxfs::mount_replica::ckpt_unmount_start)
void
mount_replica_impl::ckpt_unmount_start_1(fs::filesystem_ptr fsptr,
    int32_t flags, solobj::cred_ptr credobj, fs::mount_client_ptr client,
    bool is_shutdown, Environment &_environment)
{
	CL_PANIC(0);
}

//
// Checkpoint the start of an unmount.
//
void
mount_replica_impl::ckpt_unmount_start_v1(pxfs_v1::filesystem_ptr fsptr,
    int32_t flags, solobj::cred_ptr credobj, fs::mount_client_ptr client,
    bool is_shutdown, Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_unmount_start_v1(fsptr, flags,
	    credobj, client, is_shutdown, _environment);
}

//
// Checkpoint that the mount points have been locked
// and the file system has been unmounted but the
// client nodes haven't been notified yet.
//
void
mount_replica_impl::ckpt_unmount_middle(sol::error_t error,
    Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_unmount_middle(error, _environment);
}

//
// Checkpoint that all the client nodes have been
// notified (if there was no unmount error) before shutting
// down the file system service.
//
void
mount_replica_impl::ckpt_unmount_notified(Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_unmount_notified(_environment);
}

//
// Checkpoint that all the client nodes have been notified
// (if there was an unmount error) or the file system
// service has been shut down. We can't use a commit() call
// since this may be called more than once when unmounting
// multiple file systems.
//
void
mount_replica_impl::ckpt_unmount_end(Environment &_environment)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_unmount_end(_environment);
}

//
// Checkpoint a node being shut down as part of unmount().
//
void
mount_replica_impl::ckpt_unmount_shutdown(fs::mount_client_ptr client,
    Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_unmount_shutdown(client);
}

//
// Checkpoint a new device lock.
//
void
mount_replica_impl::ckpt_devlock(fs::mount_client_ptr client,
    sol::nodeid_t nodeid, const char *dev_name, Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_devlock(client, nodeid, dev_name);
}

//
// Checkpoint a device unlock.
//
void
mount_replica_impl::ckpt_devunlock(const char *dev_name, Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_devunlock(dev_name);
}

//
// Checkpoint the creation of a dc_callback object.
//
void
mount_replica_impl::ckpt_get_dc_callback(fs::dc_callback_ptr cb,
    Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_get_dc_callback(cb);
}

//
// Checkpoint a change in the disk connection list.
//
void
mount_replica_impl::ckpt_notify_change(sol::dev_t gdev,
    const sol::nodeid_seq_t &dev_nids, Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_notify_change(gdev, dev_nids);
}

//
// Checkpoint a new service version
//
void
mount_replica_impl::ckpt_service_version(unsigned short new_major,
    unsigned short new_minor, Environment &)
{
	version_lock.wrlock();
	current_version.major_num = new_major;
	current_version.minor_num = new_minor;
	pending_version.major_num = 0;
	version_lock.unlock();
}

//
// Checkpoint the upgrade of a mount_client in client list.
//
void
mount_replica_impl::ckpt_upgrade_client_list(fs::mount_client_ptr client,
    sol::nodeid_t nodeid, Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_upgrade_client_list(client, nodeid);
}

//
// Checkpoint the upgrade of a mount_client in devlock list.
//
void
mount_replica_impl::ckpt_upgrade_devlock_list(const char *dev_name,
    fs::mount_client_ptr client, Environment &)
{
	ASSERT(mount_serverp != NULL);
	mount_serverp->ckpt_upgrade_devlock_list(dev_name, client);
}

//
// Start up the mount service.  We create a replica on every node
// so that the service is always available.
//
int
mount_replica_impl::startup()
{
	Environment e;
	char *svc_desc = "mount";
	char *vpname = "px_mount";

	// Create a new mount replica for This node.
	char id[20];
	os::sprintf(id, "%u", orb_conf::node_number());
	mount_replica_impl *repl = new mount_replica_impl(id);

	// Get a pointer to the local version manager
	version_manager::vm_admin_var vmgr_v = vm_util::get_vm(NODEID_UNKNOWN);

	// Build a UCC for support of version upgrade callbacks
	version_manager::ucc_seq_t ucc_seq(1, 1);
	ucc_seq[0].ucc_name = os::strdup(svc_desc);
	ucc_seq[0].vp_name = os::strdup(vpname);
	version_manager::string_seq_t fseq(1, 1);
	fseq[0] = os::strdup(svc_desc);
	ucc_seq[0].freeze = fseq;

	// Create a version upgrade callback object for this mount replica.
	version_manager::vp_version_t		callback_limit;
	version_manager::vp_version_t		cur_version;
	version_manager::upgrade_callback_var	cbobj =
	    (new mount_version_callback_impl(*repl))->get_objref();

	//
	// Register the callback object with the Version Manager. The
	// tmp_version will be returned.  The version lock is not held
	// since the replica is not yet registered with the HA framework
	// so there can not be a call to become_primary. The current version
	// is returned regardless.
	//
	// If the running version is less than the callback_limit
	// a callback will be registerd, otherwise a callback is not
	// registered (currently no way to tell).  The current running
	// version is returned regardless.
	//
	callback_limit.major_num = 2;
	callback_limit.minor_num = 0;
	vmgr_v->register_upgrade_callbacks(ucc_seq, cbobj, callback_limit,
	    cur_version, e);
	if (e.exception()) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("replica:startup: version exception"
		    "on attemping to register for upgrade callback, id %s",
		    id));
		return (1);
	}

	// establish the current version in the provider
	repl->set_version(cur_version);

	repl->register_with_rm(1, true, e);
	if (e.exception()) {
		MOUNT_DBPRINTF(
		    MOUNT_TRACE_REPLICA,
		    MOUNT_RED,
		    ("replica:startup exception register with RM id %s\n",
		    id));
		e.clear();
		return (EIO);
	}
	return (0);
}

int
mount_replica_impl::shutdown()
{
	return (0);
}

//
// Set the initial version number.
//
void
mount_replica_impl::set_version(const version_manager::vp_version_t &version)
{
	//
	// We could have had a callback between the time that we called
	// register_upgrade_callbacks() and the time that it returned and
	// set tmp_version. The callback may have set a newer version than
	// current version, so don't clobber it.
	//
	version_lock.wrlock();
	if (current_version.major_num < version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num < version.minor_num)) {
		current_version = version;
	}
	version_lock.unlock();
}



mount_version_callback_impl::mount_version_callback_impl(
    mount_replica_impl &mount_replica) :
	prov(mount_replica)
{
}

mount_version_callback_impl::~mount_version_callback_impl()
{
}

void
mount_version_callback_impl::_unreferenced(unref_t arg)
{
	if (!_last_unref(arg)) {
		// _last_unref() should always be true since we don't use 0->1.
		ASSERT(0);
		return;
	}
	delete this;
}

void
mount_version_callback_impl::do_callback(const char *,
    const version_manager::vp_version_t &current_version, Environment &e)
{
	// Call the provider to update the version and checkpoint it.
	prov.upgrade_callback(current_version, e);
}
