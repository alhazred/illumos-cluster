//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// bulkio_impl_pages.cc - implementation of the class bulkio_impl::in_pages
//

#pragma ident	"@(#)bulkio_impl_pages.cc	1.8	08/05/20 SMI"

#include <sys/sol_version.h>
#include <sys/os.h>
#include <sys/mc_probe.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/bulkio/bulkio_holder_impl.h>

#if SOL_VERSION >= __s9
#define	S9_BP_CLEANUP
#endif

// called from bulkio_impl_aio_pages also
void recv_pages_common(struct buf *, service &);

// XXX For improved performance, we should make holder a static object
// and reuse it to make the server pull invocation in all bulkio objects.
// This is safe since the holder object contains no state.

//
// class bulkio_impl::in_pages
// for synchonous and async pageouts.
//


//
// Called only on the server side.
//
CORBA::Object_ptr
bulkio_impl::in_pages::unmarshal(service &b)
{
	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_PAGES_UNMARSHAL_S,
			FaultFunctions::generic);
	return (new bulkio_impl::in_pages_srvr(b)->get_objref());
}

// Called only on the server side when a previous unmarshal fails.

void
bulkio_impl::in_pages::unmarshal_cancel(service &b)
{
	(void) b.get_unsigned_long();
	(void) b.get_unsigned_longlong();
	(void) b.get_long();
	if ((uint_t)b.get_octet() == bulkio_impl::CLIENT_PUSH) {
		b.get_off_line_cancel();
	} else {
		(void) b.get_pointer();
		b.get_object_cancel();
	}
}

//
// class bulkio_impl::in_pages_clnt
// Called only on the client side by a conv_in(list_of_pages) in PXFS code.
//
bulkio_impl::in_pages_clnt::in_pages_clnt(page_t *pp, u_offset_t off,
    uint_t len, int flgs) :
	_pp(pp),
	_off(off),
	_len(len),
	_flags(flgs),
	_io_in_progress(false),
	_fdbuf(NULL)
{
	_reply_buf = (replyio_server_t *)0;
	pbuf = (PpBuf *)0;
}

bulkio_impl::in_pages_clnt::~in_pages_clnt()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_PAGES,
	    PXFS_GREEN,
	    ("destructor for in_pages_clnt %p\n", this));
	if (_fdbuf != NULL) {
		fdb_free(_fdbuf);
	}
	//
	// We want to check that any data being copied either to or from
	// the transport is complete.
	//
	ASSERT(!_io_in_progress || (waitcv.get_state() == os::notify_t::DONE));
} //lint !e1740 some pointers neither freed nor zero'ed by destructor

void
bulkio_impl::in_pages::_unreferenced(unref_t)
{
	delete this;
}

//
// in_pages_clnt::marshal
//
// Sequence of marshalled objects
// unsigned_long(_len);
// unsigned_longlong(_off);
// long(_flags);
// octet(CLIENT_PUSH or SERVER_PULL)
// if (CLIENT_PUSH) put_off_line(pbuf);
// if (SERVER_PULL) put_pointer(*this) followed by put_object(holder_obj)
//
void
bulkio_impl::in_pages_clnt::marshal(service &b)
{
	MC_PROBE_0(bulkio_in_pages_c_marsh_start, "clustering pxfs bulkio", "");

	Environment *e = b.get_env();
	bool rbuf_support = false;

	ASSERT(!e->is_nonblocking());

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_PAGES,
	    PXFS_GREEN,
	    ("in_page_clnt marshal object %p\n", this));

	b.put_unsigned_long(_len);
	b.put_unsigned_longlong(_off);
	b.put_long(_flags);

	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_PAGES_MARSHAL_C,
		FaultFunctions::generic);

	// check_reply_buf_support does not use
	// the data pointer that's passed in as a parameter.
	//

	rbuf_support = b.check_reply_buf_support((size_t)_len,
	    PAGE_TYPE, (void *)_pp, CL_OP_WRITE);

	if (!rbuf_support) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_PAGES,
		    PXFS_GREEN,
		    ("in_pages client-push %p\n", this));

		//
		// If this is a re-marshal, wait to be signalled for the
		// previous put_offline or put_offline_reply.
		//

		if (_io_in_progress) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_PAGES,
			    PXFS_AMBER,
			    ("in_pages_clnt %p remarshal\n",
			    this));
			(void) waitcv.reinit(os::SLEEP);
		}

		// CSTYLED
		pbuf = new PpBuf(_pp, _len, &waitcv);

		// client push
		b.put_octet(bulkio_impl::CLIENT_PUSH);
		_io_in_progress = true;
		b.put_off_line(pbuf, PAGE_ALIGN);
	} else {
		// server pull
		// Put a pointer to myself and a holder object using
		// which I can invoke server pull.
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_PAGES,
		    PXFS_GREEN,
		    ("in_pages server-pull %p\n", this));
		b.put_octet(bulkio_impl::SERVER_PULL);
		b.put_pointer((void *)this);

		// _io_in_progress is set during server-pull marshal
		// in pages_write_obj_clnt::marshal().
	}
	MC_PROBE_0(bulkio_in_pages_c_marsh_end, "clustering pxfs bulkio", "");
}

fdbuffer_t *
bulkio_impl::in_pages_clnt::get_buf()
{
	MC_PROBE_0(bulkio_in_pages_c_get_buf_start, "clustering pxfs bulkio",
	    "");

	// XX If doing direct DMA, setup buffer as an address
	size_t len = _len;
	ASSERT((_flags & B_WRITE));

	// create fdb for the pages.
	len = roundup(len, DEV_BSIZE);
	_fdbuf = fdb_page_create(_pp, len, FDB_WRITE);

	MC_PROBE_0(bulkio_in_pages_c_get_buf_end, "clustering pxfs bulkio", "");
	return (_fdbuf);
}

void
bulkio_impl::in_pages_clnt::wait_on_sema()
{

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_PAGES,
	    PXFS_GREEN,
	    ("object %p wait_on_sema\n", this));
	if (_io_in_progress) {
		waitcv.wait();
	}

	//
	// We need this to prevent wait_on_sema's from hanging during
	// asynchronous pageout callback and cleanup on the client.
	//

	_io_in_progress = false;
}

//
// class bulkio_impl::in_pages_srvr
//
// called by in_pages::unmarshal()
// essentially this constructor does the unmarshalling of data marshalled
// by in_pages_clnt::marshal(). Refer to the sequence of marshalled data
// in in_pages_clnt::marshal() to understand in what order to unmarshal data
// here.
//
bulkio_impl::in_pages_srvr::in_pages_srvr(service &b) :
	_fdbuf(NULL)
{
	MC_PROBE_0(bulkio_in_pages_s_con_start, "clustering pxfs bulkio", "");
	struct seg kseg;

	_reply_buf = (replyio_server_t *)0;
	kseg.s_as = &kas;

	_len = b.get_unsigned_long();
	_off = b.get_unsigned_longlong();
	_flags = b.get_long();

	// Create enough space for incoming pages.

	_pp = page_create_va(_vn.vp, _off, (size_t)_len, PG_WAIT, &kseg, NULL);
	ASSERT(_pp != NULL);
	MC_PROBE_0(bulkio_in_pages_s_con_page, "clustering pxfs bulkio", "");

	struct buf buffer;
	bioinit(&buffer);

	//
	// B_KERNBUF obsoleted by S9
	//
#ifdef S9_BP_CLEANUP
	buffer.b_flags = B_PAGEIO;
#else
	buffer.b_flags = B_KERNBUF | B_PAGEIO;
#endif
	buffer.b_bcount = _len;
	buffer.b_bufsize = _len;
	buffer.b_pages = _pp;

	_unmarshal_exception = false;

	uint_t iotype = (uint_t)b.get_octet();
	if (iotype == bulkio_impl::SERVER_PULL) {

		// Unmarshal pointer to the client side bulkio object
		// that has the pages we want to pull.

		void *in_clnt_pages_obj = b.get_pointer();
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_PAGES,
		    PXFS_GREEN,
		    ("in_pages_srvr received"
		    " server-pull for object %p\n", in_clnt_pages_obj));

		nodeid_t nid = b.get_src_node().ndid;
		if (CORBA::is_nil(node_array[nid])) {
			(void) bulkio_impl::resolve_holder(nid);
		}

		node_array_lock.rdlock();
		bulkio_holder::holder_var wrapper =
			node_array[nid];
		node_array_lock.unlock();

		_unmarshal_exception = false;

		bulkio::pages_write_obj_var wrobj =
		    new bulkio_impl::pages_write_obj_srvr
				(&buffer, in_clnt_pages_obj)->get_objref();

		// Nested invocation requires its own Environment
		Environment	e;

		if (wrapper->_handler()->server_dead()) {
			//
			// Holder reference is bad
			// => rebind to a new reference
			//
			(void) bulkio_impl::resolve_holder(nid);
			if (!CORBA::is_nil(node_array[nid])) {
				node_array_lock.rdlock();
				wrapper = node_array[nid];
				node_array_lock.unlock();
			}
		}
		wrapper->pages_srvr_pull(wrobj.INOUT(), e);
		//
		// Data will be read into the reply buffer
		// in pages_write_obj_srvr::recv_data() which is
		// called from pages_write_obj::unmarshal().
		//
		if (e.exception()) {
			_unmarshal_exception = true;
			//
			// Our framework doesn't handle exceptions in unmarshal
			// so we clear the exception.
			//
			e.clear();
		}
		MC_PROBE_0(bulkio_in_pages_s_con_pull, "clustering pxfs bulkio",
		    "");
	} else {
		ASSERT(iotype == bulkio_impl::CLIENT_PUSH);
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_PAGES,
		    PXFS_GREEN,
		    ("in_pages_srvr client-push\n"));
		recv_pages_common(&buffer, b);
		MC_PROBE_0(bulkio_in_pages_s_con_push, "clustering pxfs bulkio",
		    "");
	}
	//
	// This buffer should not have been marked ASYNC, else biodone could
	// be deleting the pages
	//
	ASSERT((buffer.b_flags & B_ASYNC) == 0);
	biodone(&buffer);
	MC_PROBE_0(bulkio_in_pages_s_con_end, "clustering pxfs bulkio", "");
}

bulkio_impl::in_pages_srvr::~in_pages_srvr()
{
	MC_PROBE_0(bulkio_in_pages_s_des_start, "clustering pxfs bulkio",
		    "");
	if (_fdbuf != NULL) {
		fdb_free(_fdbuf);
	}
	while (_pp != NULL) {
		page_t *pp = _pp;
		page_sub(&_pp, pp);
		page_io_unlock(pp);
		page_destroy(pp, 0);
	}
	MC_PROBE_0(bulkio_in_pages_s_des_end, "clustering pxfs bulkio",
		    "");
}

void
bulkio_impl::in_pages_srvr::wait_on_sema()
{
	// Shouldn't happen
	ASSERT(0);
}

void
bulkio_impl::in_pages_srvr::marshal(service &)
{
	ASSERT(0);		// Should never be called
}

fdbuffer_t *
bulkio_impl::in_pages_srvr::get_buf()
{
	size_t len = _len;

	//
	// Return NULL if _unmarshal_exception is true.
	// This is a way to signal PXFS server that the page_out failed.
	// The only way this can happen is if there was an exception
	// during in_pages_srvr::unmarshal as a result of the server-pull
	// invocation failing.
	//

	if (_unmarshal_exception) {
		return (NULL);
	}

	ASSERT((_flags & B_WRITE));

	len = roundup(len, DEV_BSIZE);
	_fdbuf = fdb_page_create(_pp, len, FDB_WRITE);

	ASSERT(_fdbuf != NULL);
	return (_fdbuf);
}

CORBA::Object_ptr
bulkio_impl::inout_pages::unmarshal(service &b)
{
	FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_PAGES_UNMARSHAL_S,
			FaultFunctions::generic);
	switch (b.get_octet()) {
	case REQUEST:
		FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_PAGES_UNMRSHL_REQ,
			FaultFunctions::generic);
		return (new bulkio_impl::inout_pages_srvr(b)->get_objref());

	case REPLY: {
		FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_PAGES_UNMRSHL_REP,
			FaultFunctions::generic);
		bulkio_impl::inout_pages_clnt *pagesobj =
		    (bulkio_impl::inout_pages_clnt *)b.get_pointer();

		MC_PROBE_0(bulkio_inout_pages_torecv, "clustering pxfs bulkio",
		    "");
		pagesobj->recv_pages(b);
		MC_PROBE_0(bulkio_inout_pages_recv, "clustering pxfs bulkio",
		    "");

		// The _duplicate is to counteract the release done in
		// get_param for an inout object. This retains the reference
		// count so that the object does not get destroyed prematurely
		return (bulkio::inout_pages::_duplicate(pagesobj));
	}

	default:
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

void
bulkio_impl::inout_pages::unmarshal_cancel(service &b)
{
	switch (b.get_octet()) {
	case REQUEST: {
		// similar to constructor for inout_pages_srvr
		(void) b.get_pointer();
		(void) b.get_unsigned_long();
		(void) b.get_unsigned_longlong();
		(void) b.get_long();
		(void) b.get_unsigned_long();
		if (b.get_octet() == bulkio_impl::REPLY_OPT) {
			b.get_reply_buffer_cancel();
		}
		break;
	}
	case REPLY: {
		bulkio_impl::inout_pages_clnt *pagesobj =
		    (bulkio_impl::inout_pages_clnt *)b.get_pointer();
		pagesobj->recv_cancel(b);
		break;
	}
	default:
		ASSERT(0);
		break;
	}
}

//
// class bulkio_impl::inout_pages_clnt
//

bulkio_impl::inout_pages_clnt::inout_pages_clnt(page_t *pp, u_offset_t off,
    uint_t len, int flags, uint_t prot) :
	_pp(pp),
	_off(off),
	_len(len),
	_flags(flags),
	_prot(prot),
	_reply_buf(NULL),
	_fdbuf(NULL)
{
	bioinit(&_buf);

	//
	// B_KERNBUF obsoleted by S9
	//
#ifdef S9_BP_CLEANUP
	_buf.b_flags = B_PAGEIO;
#else
	_buf.b_flags = B_KERNBUF | B_PAGEIO;
#endif
	_buf.b_bcount = len;
	_buf.b_bufsize = len;
	_buf.b_pages = pp;
}

bulkio_impl::inout_pages_clnt::~inout_pages_clnt()
{
	// Should have called release_pages before destructor
	ASSERT(_reply_buf == NULL);
	ASSERT(_fdbuf == NULL);
}

// This routine is called just before the caller does a pvn_*_done to
// free the pages that were passed in during the constructor
// This routine should guarantee that the bulkio object or the transport buffers
// either do not have a pointer to the pages or will not access the pages
// after this
// If reply I/O is being used, a reply buffer would indicate that the
// transport may be writing data into these pages. Doing a release() on
// the reply buffer will guarantee that the transport will not write to
// these pages anymore. See implementation in tcp_transport.cc where a
// generation number is used for example and the lock in replyio_client_t
// synchronizes/waits for any ongoing writes to complete
void
bulkio_impl::inout_pages_clnt::release_pages()
{
	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}

	if (_fdbuf != NULL) {
		fdb_free(_fdbuf);
		_fdbuf = NULL;
	}

	// This biodone corresponds to the bioinit done in the constructor
	// Should not have the _ASYNC flag set
	ASSERT((_buf.b_flags & B_ASYNC) == 0);
	biodone(&_buf);

	// Set _pp to NULL, to catch any further uses of these pages which
	// the caller expects to free soon
	_pp = NULL;
}

void
bulkio_impl::inout_pages::_unreferenced(unref_t)
{
	delete this;
}

void
bulkio_impl::inout_pages_clnt::marshal(service &b)
{
	MC_PROBE_0(bulkio_inout_pages_c_marsh_start, "clustering pxfs bulkio",
	    "");
	FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_PAGES_MARSHAL_C,
			FaultFunctions::generic);

	b.put_octet(bulkio_impl::REQUEST);
	b.put_pointer((void *)this);
	b.put_unsigned_long(_len);
	b.put_unsigned_longlong(_off);
	b.put_long(_flags);
	b.put_unsigned_long(_prot);
	// Get preallocated reply buffer from transport, for buf _buf

	// If this object is being remarshalled, release the old reply buffer
	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}
	if (b.check_reply_buf_support((size_t)_len, PAGE_TYPE,
	    NULL, CL_OP_READ)) {
		b.put_octet(bulkio_impl::REPLY_OPT);
		_reply_buf = b.add_reply_buffer(&_buf, PAGE_ALIGN);
	} else {
		b.put_octet(bulkio_impl::NO_REPLY_OPT);
	}
	MC_PROBE_0(bulkio_inout_pages_c_marsh_end, "clustering pxfs bulkio",
	    "");
}

fdbuffer_t *
bulkio_impl::inout_pages_clnt::get_buf()
{
	MC_PROBE_0(bulkio_inout_pages_c_get_buf_start, "clustering pxfs bulkio",
	    "");
	size_t len = _len;
	ASSERT((_flags & B_READ));
	ASSERT((_flags & B_WRITE) == 0);

	len = roundup(len, DEV_BSIZE);
	_fdbuf = fdb_page_create(_pp, len, FDB_READ);

	MC_PROBE_0(bulkio_inout_pages_c_get_buf_end, "clustering pxfs bulkio",
	    "");
	return (_fdbuf);
}

void
bulkio_impl::inout_pages_clnt::recv_pages(service &b)
{
	if (_reply_buf != NULL) {
		b.get_offline_reply(_reply_buf);
		_reply_buf = NULL;
	} else {
		recv_pages_common(&_buf, b);
	}
}

void
bulkio_impl::inout_pages_clnt::recv_cancel(service &b)
{
	if (_reply_buf != NULL) {
		b.get_offline_reply_cancel(_reply_buf);
		_reply_buf = NULL;
	} else {
		b.get_off_line_cancel();
	}
}

//
// class bulkio_impl::inout_pages_srvr
//
bulkio_impl::inout_pages_srvr::inout_pages_srvr(service &b) :
	_io_in_progress(false)
{
	MC_PROBE_0(bulkio_inout_pages_s_c_start, "clustering pxfs bulkio",
	    "");

	struct seg kseg;

	kseg.s_as = &kas;

	_clntobj = b.get_pointer();
	_len = b.get_unsigned_long();
	_off = b.get_unsigned_longlong();
	_flags = b.get_long();
	_prot = b.get_unsigned_long();

	_pp = page_create_va(_vn.vp, _off, (size_t)_len, PG_WAIT, &kseg, NULL);
	MC_PROBE_0(bulkio_inout_pages_s_c_page, "clustering pxfs bulkio",
	    "");

	_reply_buf = NULL;

	if (b.get_octet() == bulkio_impl::REPLY_OPT) {
		_reply_buf = b.get_reply_buffer();
		ASSERT(_reply_buf);
	}
	_fdbuf = NULL;
	MC_PROBE_0(bulkio_inout_pages_s_c_end, "clustering pxfs bulkio",
	    "");
}

bulkio_impl::inout_pages_srvr::~inout_pages_srvr()
{
	MC_PROBE_0(bulkio_inout_pages_s_d_start, "clustering pxfs bulkio",
	    "");

	// Wait for any io in progress to complete
	if (_io_in_progress) {
		waitcv.wait();
	}
	MC_PROBE_0(bulkio_inout_pages_s_d_wait, "clustering pxfs bulkio",
	    "");

	// Release reply buffer
	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}
	MC_PROBE_0(bulkio_inout_pages_s_d_rel, "clustering pxfs bulkio",
	    "");

	if (_fdbuf != NULL) {
		fdb_free(_fdbuf);
	}
	MC_PROBE_0(bulkio_inout_pages_s_d_free, "clustering pxfs bulkio",
	    "");

	while (_pp != NULL) {
		page_t *pp = _pp;
		page_sub(&_pp, pp);
		page_io_unlock(pp);
		page_destroy(pp, 0);
	}
	MC_PROBE_0(bulkio_inout_pages_s_d_end, "clustering pxfs bulkio",
	    "");
}

void
bulkio_impl::inout_pages_srvr::marshal(service &b)
{
	Environment *e = b.get_env();
	ASSERT(!e->is_nonblocking());

	FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_PAGES_MARSHAL_S,
			FaultFunctions::generic);

	b.put_octet(bulkio_impl::REPLY);
	b.put_pointer(_clntobj);

	// If already created a reply io, this is a remarshal and
	// we wait for the previous one to complete before starting
	// new one.
	if (_io_in_progress) {
		(void) waitcv.reinit(os::SLEEP);
	}

	MC_PROBE_0(bulkio_inout_pages_s_marshal_tonew, "clustering pxfs bulkio",
	    "");
	PpBuf *pbuf = new PpBuf(_pp, _len, &waitcv);
	MC_PROBE_0(bulkio_inout_pages_s_marshal_new, "clustering pxfs bulkio",
	    "");

	_io_in_progress = true;

	if (_reply_buf != NULL) {
		// Use put_off_line version that does reply optimization
		b.put_offline_reply(pbuf, _reply_buf);
	} else {
		b.put_off_line(pbuf, PAGE_ALIGN);
	}
	MC_PROBE_0(bulkio_inout_pages_s_marshal_end, "clustering pxfs bulkio",
	    "");
}

void
bulkio_impl::inout_pages_srvr::release_pages()
{
	// Called only on the client and not on the server
	ASSERT(!"inout_pages_srvr::release_pages() called");
}


fdbuffer_t *
bulkio_impl::inout_pages_srvr::get_buf()
{
	size_t len = _len;
	ASSERT((_flags & B_READ));
	ASSERT((_flags & B_WRITE) == 0);

	len = roundup(len, DEV_BSIZE);
	_fdbuf = fdb_page_create(_pp, len, FDB_READ);

	return (_fdbuf);
}

void
recv_pages_common(struct buf *bp, service &b)
{
	uint_t templen = b.get_off_line_hdr();
	ASSERT(templen == bp->b_bcount);

	bp_mapin(bp);
	b.get_off_line_bytes(bp->b_un.b_addr, templen);
	bp_mapout(bp);
}

//
// Parameters are a buf pointer that we will use to add a pull reply buffer and
// the original client side bulkio object pointer
// Always called on the server side before the server-pull invocation starts.
//
bulkio_impl::pages_write_obj_srvr::pages_write_obj_srvr(struct buf *srvr_buf,
	    void *clnt_obj)
{
	_srvr_buf = srvr_buf;
	_reply_buffer = NULL;
	_orig_pages_clnt = clnt_obj;
}


CORBA::Object_ptr
bulkio_impl::pages_write_obj::unmarshal(service &b)
{
	switch (b.get_octet()) {
	case REQUEST: {
		// Always called on the client side
		return (new bulkio_impl::pages_write_obj_clnt(b)->get_objref());
	}
	case REPLY: {
		// Always called on the server side
		bulkio_impl::pages_write_obj_srvr *wrobj =
		    (bulkio_impl::pages_write_obj_srvr *)b.get_pointer();
		wrobj->recv_data(b);
		// The _duplicate is to counteract the release done in
		// get_param for an inout object. This retains the reference
		// count so that the object does not get destroyed prematurely
		return (bulkio::pages_write_obj::_duplicate(wrobj));
	}
	default:
		ASSERT(0);
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

// Marshalling server-pull request to client
// Sequence of marshalled data:
// octet(REQUEST)
// pointer
// pointer
// reply_buffer

void
bulkio_impl::pages_write_obj_srvr::marshal(service &b)
{
	b.put_octet(bulkio_impl::REQUEST);
	// pointer to client side bulkio object
	b.put_pointer((void *)_orig_pages_clnt);
	b.put_pointer((void *)this);
	_reply_buffer = b.add_reply_buffer(_srvr_buf, PAGE_ALIGN);
	ASSERT(_reply_buffer != NULL);
}	

// Destructor for pages_write_obj_srvr
bulkio_impl::pages_write_obj_srvr::~pages_write_obj_srvr()
{
	if (_reply_buffer != NULL) {
		_reply_buffer->release();
		_reply_buffer = NULL;
	}
}

// recv_data is invoked on the server side by pages_write_obj::unmarshal
// It is used to copy data into the appropriate place only for server-pull.

void
bulkio_impl::pages_write_obj_srvr::recv_data(service &b)
{

	// It is critical to set _reply_buffer to NULL after
	// a get_offline_reply because the function releases
	// _reply_buffer. Doing a second release later causes
	// a panic.

	if (_reply_buffer != NULL) {
		b.get_offline_reply(_reply_buffer);
		_reply_buffer = NULL;
	}
}

// recv_cancel is called from the REPLY path of unmarshal_cancel
// only for server-pull

void
bulkio_impl::pages_write_obj_srvr::recv_cancel(service &b)
{

	// _reply_buffer must be set to NULL after get_offline_reply_cancel()
	// to prevent release being called on _reply_buffer a second time.

	if (_reply_buffer != NULL) {
		b.get_offline_reply_cancel(_reply_buffer);
		_reply_buffer = NULL;
	}
}

void
bulkio_impl::pages_write_obj::_unreferenced(unref_t)
{
	delete this;
}

// Called by the REQUEST path of pages_write_obj::unmarshal()

bulkio_impl::pages_write_obj_clnt::pages_write_obj_clnt(service &b)
{
	_clnt_pages_obj = b.get_pointer();

	_ppbuf = (PpBuf *)0;
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_PAGES,
	    PXFS_GREEN,
	    ("pages_write_obj_clnt unmarshal of client"
			    " object %p\n", _clnt_pages_obj));

	_server_side_write_obj = b.get_pointer();
	_clnt_reply_buffer = b.get_reply_buffer();
	ASSERT(_clnt_reply_buffer != NULL);
}

bulkio_impl::pages_write_obj_clnt::~pages_write_obj_clnt()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_PAGES,
	    PXFS_GREEN,
	    ("pages_write_obj destructor %p\n", _clnt_pages_obj));

	if (_clnt_reply_buffer) {
		_clnt_reply_buffer->release();
	}
}

// Invoked when the client marshals back data to the server in reply
// to a server-pull invocation.
// Sequence of marshalled data:
// octet(REPLY)
// pointer
// reply_buffer

void
bulkio_impl::pages_write_obj_clnt::marshal(service &b)
{
	b.put_octet(bulkio_impl::REPLY);
	// put a pointer to the server side bulkio object
	b.put_pointer((void *)_server_side_write_obj);

	ASSERT(_clnt_reply_buffer != NULL);

	//
	// create_ppbuf() does two things:
	// (1) It waits for any previous marshal to complete before creating
	// a new PpBuf.
	// (2) It sets _io_in_progress to true.
	//
	// The reason for setting it in this convoluted manner is to prevent
	// the client from hanging if the server dies before server-pull is
	// initiated. That would happen if we set io_in_progress to true
	// in in_pages_clnt::marshal().
	//
	// A second reason to set _io_in_progress() here is to make this
	// marshal routine safe w.r.t. re-marshals during path failures.
	// create_ppbuf() does a waitcv.reinit() if _io_in_progress
	// is true.
	//

	_ppbuf = ((bulkio_impl::in_pages_clnt *)
		    _clnt_pages_obj)->create_ppbuf();

	b.put_offline_reply(_ppbuf, _clnt_reply_buffer);
}

PpBuf *
bulkio_impl::in_pages_clnt::create_ppbuf()
{
	//
	// Use a lock to check _io_in_progress since we can have a race
	// between two server-pull objects calling the same in_pages_clnt
	// object to create_ppbuf(). This causes bug 4303446.
	//
	// XXX A clean fix would be to rewrite this bulkio handler so that
	// _waitcv and _io_in_progress are local to the server-pull object
	// as is the case for uio and aio handlers.
	//

	_iop.lock();
	if (_io_in_progress) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_PAGES,
		    PXFS_AMBER,
		    ("create ppbuf remarshal(%p)\n", this));
		(void) waitcv.reinit(os::SLEEP);
	}
	_io_in_progress = true;
	_iop.unlock();

	// CSTYLED
	return(new PpBuf(_pp, _len, &waitcv));
}

// Unmarshal whatever the normal unmarshal would have removed from
// the service object's recstream.

void
bulkio_impl::pages_write_obj::unmarshal_cancel(service &b)
{
	switch (b.get_octet()) {
	case REQUEST: {
		(void) b.get_pointer();
		(void) b.get_pointer();
		b.get_reply_buffer_cancel();
		break;
	}
	case REPLY: {
		(void) b.get_pointer();
		bulkio_impl::pages_write_obj_srvr *wrobj =
		    (bulkio_impl::pages_write_obj_srvr *) b.get_pointer();
		wrobj->recv_cancel(b);
		break;
	}
	default:
		ASSERT(0);
	}
}
