//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
// bulkio_impl.in - definition of inline functions
//

#pragma ident	"@(#)bulkio_impl_in.h	1.6	08/05/20 SMI"

inline
bulkio_impl::exception::exception(int error)
{
	_error = error;
}

inline
bulkio_impl::exception::~exception()
{
}

inline bulkio::in_uio_ptr
bulkio_impl::conv_in(uio_t *uiop, size_t)
{
	return (new in_uio_clnt(uiop)->get_objref());
}

inline uio_t *
bulkio_impl::conv(bulkio::in_uio_ptr uioobj)
{
	return (((bulkio_impl::in_uio *)uioobj)->get_uio());
}

inline bulkio::inout_uio_ptr
bulkio_impl::conv_inout(uio_t *uiop, size_t)
{
	return (new inout_uio_clnt(uiop)->get_objref());
}

inline uio_t *
bulkio_impl::conv(bulkio::inout_uio_ptr uioobj)
{
	return (((bulkio_impl::inout_uio *)uioobj)->get_uio());
}

inline bulkio::in_aio_ptr
bulkio_impl::aconv_in(struct aio_req *aiop, int)
{
	return (new in_aio_clnt(aiop)->get_objref());
}

inline struct aio_req *
bulkio_impl::aconv(bulkio::in_aio_ptr aioobj, CORBA::Object_ptr aiock)
{
	return (((bulkio_impl::in_aio *)aioobj)->get_aio(aiock));
}

inline void
bulkio_impl::aio_cancel(bulkio::in_aio_ptr aioobj)
{
	((bulkio_impl::in_aio *)aioobj)->aio_cancel();
}

inline void
bulkio_impl::did_aio(bulkio::in_aio_ptr aioobj)
{
	((bulkio_impl::in_aio *)aioobj)->did_aio();
}	


inline bulkio::in_pages_ptr
bulkio_impl::conv_in(page_t *pp, u_offset_t off,
    uint32_t len, int flags)
{
	return (new in_pages_clnt(pp, off, len, flags)->get_objref());
}

inline fdbuffer_t *
bulkio_impl::conv(bulkio::in_pages_ptr pages_obj)
{
	return (((bulkio_impl::in_pages *)pages_obj)->get_buf());
}

inline bulkio::inout_pages_ptr
bulkio_impl::conv_inout(page_t *pp, u_offset_t off,
	uint32_t len, int32_t flags, uint32_t protp)
{
	return (new inout_pages_clnt(pp, off, len, flags, protp)->get_objref());
}

inline fdbuffer_t *
bulkio_impl::conv(bulkio::inout_pages_ptr pages_obj)
{
	return (((bulkio_impl::inout_pages *)pages_obj)->get_buf());
}

inline bulkio::in_aio_pages_ptr
bulkio_impl::aconv_in_pgs(page_t *pp, u_offset_t off,
	uint32_t len, int flags)
{
	return (new in_aio_pages_clnt(pp, off, len, flags)->get_objref());
}

inline fdbuffer_t *
bulkio_impl::conv(bulkio::in_aio_pages_ptr pages_obj)
{
	return (((bulkio_impl::in_aio_pages *)pages_obj)->get_buf());
}

inline void
bulkio_impl::wait_on_sema(bulkio::in_pages_ptr pages_obj)
{
	(((bulkio_impl::in_pages *)pages_obj)->wait_on_sema());
}

inline void
bulkio_impl::release_pages(bulkio::in_aio_pages_ptr pages_obj)
{
	(((bulkio_impl::in_aio_pages *)pages_obj)->release_pages());
}

inline void
bulkio_impl::release_pages(bulkio::inout_pages_ptr pages_obj)
{
	(((bulkio_impl::inout_pages *)pages_obj)->release_pages());
}

inline
bulkio_impl::in_aio::in_aio()
{
}

inline
bulkio_impl::in_aio::~in_aio()
{
}

inline
bulkio_impl::in_uio::in_uio()
{
}

inline
bulkio_impl::in_uio::~in_uio()
{
}

inline
bulkio_impl::inout_uio::inout_uio()
{
}

inline
bulkio_impl::inout_uio::~inout_uio()
{
}

inline
bulkio_impl::in_pages::in_pages()
{
}

inline
bulkio_impl::in_pages::~in_pages()
{
}

inline
bulkio_impl::in_aio_pages::in_aio_pages()
{
}

inline
bulkio_impl::in_aio_pages::~in_aio_pages()
{
}

inline
bulkio_impl::inout_pages::inout_pages()
{
}

inline
bulkio_impl::inout_pages::~inout_pages()
{
}

inline
bulkio_impl::uio_write_obj::uio_write_obj()
{
}

inline
bulkio_impl::uio_write_obj::~uio_write_obj()
{
}

inline
bulkio_impl::aio_write_obj::aio_write_obj()
{
}

inline
bulkio_impl::aio_write_obj::~aio_write_obj()
{
}

inline
bulkio_impl::pages_write_obj::pages_write_obj()
{
}

inline
bulkio_impl::pages_write_obj::~pages_write_obj()
{
}

static inline bulkio_impl::aio_write_obj_clnt *
bulkio_impl::aio_write_obj_clnt::get_impl_obj(bulkio::aio_write_obj_ptr p)
{
	return ((bulkio_impl::aio_write_obj_clnt *)p);
}

static inline bulkio_impl::uio_write_obj_clnt *
bulkio_impl::uio_write_obj_clnt::get_impl_obj(bulkio::uio_write_obj_ptr p)
{
	return ((bulkio_impl::uio_write_obj_clnt *)p);
}

static inline bulkio_impl::in_aio_clnt *
bulkio_impl::in_aio_clnt::get_impl_obj(bulkio::in_aio_ptr p)
{
	return ((bulkio_impl::in_aio_clnt *)p);
}
