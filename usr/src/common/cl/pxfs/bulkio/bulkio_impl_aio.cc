//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// bulkio_impl_aio.cc - implementation of aio interfaces
//

#pragma ident	"@(#)bulkio_impl_aio.cc	1.7	08/05/20 SMI"

#include <sys/aio_impl.h>

#include <sys/sol_version.h>
#include <sys/os.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/bulkio/bulkio_holder_impl.h>

#if SOL_VERSION >= __s9
#define	S9_BP_CLEANUP
#endif

CORBA::Object_ptr
bulkio_impl::in_aio::unmarshal(service &b)
{
	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_UNMARSHAL_S,
			FaultFunctions::generic);
	switch (b.get_octet()) {
	case REQUEST:
		FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_UNMARSHAL_REQ,
			FaultFunctions::generic);
		// This is on the server side
		return (new bulkio_impl::in_aio_srvr(b)->get_objref());

	case REPLY: {
		// Only for reads. On the client side.
		FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_UNMARSHAL_REP,
			FaultFunctions::generic);
		bulkio_impl::in_aio_clnt *aioclnt =
			(bulkio_impl::in_aio_clnt *)b.get_pointer();
		aioclnt->recv_data(b);

		//
		// The in_aio object is passed as an "in" parameter
		// to the aio_callback_impl object. While PXFS has
		// loaded a REPLY flag, this is actually
		// an invocation request, and specifically the callback
		// request. When the invocation
		// finishes, the infrastructure does a release on that object.
		// We perform a duplicate in order to counteract this release
		// so that the net effect is no change in reference count.
		//
		return (bulkio::in_aio::_duplicate(aioclnt));
	}
	default:
		ASSERT(0);
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

void
bulkio_impl::in_aio::unmarshal_cancel(service &b)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_AMBER,
	    ("in_aio::unmarshal_cancel\n"));
	switch (b.get_octet()) {
	    case REQUEST: {
		// similar to constructor for aio_srvr
		(void) b.get_pointer();
		(void) b.get_longlong();
		(void) b.get_longlong();
		(void) b.get_short();
		(void) b.get_unsigned_long();
		(void) b.get_long();
		int rtype = b.get_long();
		(void) b.get_long();
		if ((rtype & B_WRITE) != 0) {
			b.get_off_line_cancel();
		} else {
			ASSERT((rtype & B_READ) != 0);
			if (b.get_octet() == bulkio_impl::REPLY_OPT) {
				b.get_reply_buffer_cancel();
			}
		}
		break;
	    }
	    case REPLY: {
		bulkio_impl::in_aio_clnt *aioclnt =
			(bulkio_impl::in_aio_clnt *)b.get_pointer();
		aioclnt->recv_cancel(b);
		break;
	    }
	    default:
		ASSERT(0);
		break;
	}
}

void
bulkio_impl::in_aio::_unreferenced(unref_t)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("in_aio obj (%p) got unref\n", this));
	delete this;
}

bulkio_impl::in_aio_clnt::~in_aio_clnt()
{

	//
	// We need this check to catch any unfortunate situations
	// where aiop was set to NULL in PXFS and we somehow missed
	// calling this object's destructor from aio_callback or
	// aio_cancel paths. We'll put an assert for now.
	//
	ASSERT(_real_destructor_was_called);
	if (!_real_destructor_was_called) {
		this->real_destructor();
		_real_destructor_was_called = true;
	}
}

void
bulkio_impl::in_aio_clnt::real_destructor()
{
	// Safety check
	ASSERT(!_real_destructor_was_called);

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("in_aio_clnt real_destructor (%p)\n", this));

	if (!CORBA::is_nil(_client_write_obj)) {
		bulkio_impl::aio_write_obj_clnt::get_impl_obj(
		    _client_write_obj)->real_destructor();
	}

	// It is safe not to mapout the buf here and in fact the correct
	// thing to do since aio_done or aio_cancel will do a bp_mapout

	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}
	//
	// Workaround for a bug where we hang if we first go remote, fail in
	// remote try, then  unmarshal and try locally.
	// Bug id: 4293175
	//
	if (_is_locked && _did_local_aio) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_AMBER,
		    ("(%p)unlocking pages because_is_locked"
		    " and _did_local_aio\n", this));
		as_pageunlock(_save_as, _save_pplist, _save_iov_base,
		    _save_bcount, (_save_flags & B_READ) ? S_WRITE : S_READ);
		_is_locked = false;
		_save_pplist = NULL;
	}
	_real_destructor_was_called = true;
}

//
// An in_aio_clnt object is constructed as a result of
// an aconv_in() in pxchr::pxfs_aiorw()
// Note that the name of this function is confusing when compared to uio
// because in_aio_clnt is created for both reads as well as writes.
// This has to do with the CORBA notion of an in parameter. Asynchronous
// reads or writes require only an "in" parameter to be sent from client
// to server. The result of the operation is marshalled back by a separate
// operation that is asynchronous with respect to the initial request.

// in_aio_clnt also takes in an os::notify_t. This is so that aio_callback
// can wait for TCP to get done with transmitting aio buffers and signal
// that it is safe to unlock pages and bp_mapout aio buffers.

bulkio_impl::in_aio_clnt::in_aio_clnt(struct aio_req *a)
{
	struct uio *uiop = a->aio_uio;
	ASSERT((uiop->uio_segflg == UIO_USERSPACE) ||
		(uiop->uio_segflg == UIO_SYSSPACE));
	ASSERT(uiop->uio_iovcnt == 1);
	if (uiodup(uiop, &_uio, &_iov, DEF_IOV_MAX)) {
		ASSERT(0);
	}
	_aio.aio_private = a->aio_private;
	_aio.aio_uio = &_uio;
	_is_locked = false;

	_reply_buf = NULL;

	_write_was_mapped_in = false;
	_real_destructor_was_called = false;

	// The _save, _did_local variables are needed to workaround bug 4293175.

	_did_local_aio = false;
	_save_pplist = NULL;
	_save_as = NULL;
	_save_flags = 0;
	_save_bcount = 0;
	_save_iov_base = 0;
}

void
bulkio_impl::in_aio_clnt::recv_data(service &b)
{
	// This is actually resid being marshalled back from the server.
	uint_t total_bytes = (uint_t)b.get_unsigned_long();

	ASSERT(total_bytes == (size_t)_iov.iov_len);

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("(%p)recv_data: rbuf %p\n", this, _reply_buf));
	if (_reply_buf != NULL) {
		b.get_offline_reply(_reply_buf);
		_reply_buf = NULL;
	} else {
		aio_req_t *reqp = (aio_req_t *)_aio.aio_private;
		ASSERT(reqp != NULL);
		struct buf *bp = &reqp->aio_req_buf;
		ASSERT((bp->b_flags & B_READ) != 0);
		if ((bp->b_flags & B_REMAPPED) == 0) {
			// only mapin if not already mapped in
			bp_mapin(bp);
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_GREEN,
			    ("(%p)recv_data in bp_mapin\n",
			    this));
			b.get_off_line_bytes(bp->b_un.b_addr,
				    b.get_off_line_hdr());
			bp_mapout(bp);
		} else {
			b.get_off_line_bytes(bp->b_un.b_addr,
				b.get_off_line_hdr());
		}
	}
}

void
bulkio_impl::in_aio_clnt::recv_cancel(service &b)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("(%p)recv_cancel\n", this));
	(void) b.get_unsigned_long();
	if (_reply_buf != NULL) {
		b.get_offline_reply_cancel(_reply_buf);
		_reply_buf = NULL;
	} else {
		b.get_off_line_cancel();
	}
}

void
bulkio_impl::in_aio_clnt::marshal(service &b)
{
	int		error = 0;
	aio_req_t	*reqp = (aio_req_t *)_aio.aio_private;
	struct buf	*bp = &reqp->aio_req_buf;

	// If remarshalling, release the previous reply buffer
	// This is only for the read case
	if (_reply_buf != NULL) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_GREEN,
		    ("in_aio_clnt (%p) remarshal\n", this));
		_reply_buf->release();
		_reply_buf = NULL;
	}

	// The following is only for the write case. Look at
	// bulkio_impl_uio in_uio_clnt::marshal() for an explanation.

	if (!CORBA::is_nil(_client_write_obj)) {
		bulkio_impl::aio_write_obj_clnt::get_impl_obj(
		    _client_write_obj)->wait_for_io_completion();
	}

	// can't do nonblocking invocations with user data
	ASSERT(!b.get_env()->is_nonblocking());

	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_MARSHAL_C, FaultFunctions::generic);


	// This is a request from the client to the server
	b.put_octet(bulkio_impl::REQUEST);

	// marshal stuff that's common to reads and writes.

	b.put_pointer((void *)this);

	b.put_longlong(_uio.uio_llimit);
	b.put_longlong(_uio._uio_offset._f);
	b.put_short(_uio.uio_fmode);
	b.put_unsigned_long((uint32_t)_uio.uio_resid);
	b.put_long(_uio.uio_iovcnt);

	// For async io, iovcnt is always 1 and _iov.iov_len == uio_resid

	ASSERT(_uio.uio_iovcnt == 1);
	ASSERT(_uio.uio_resid == _iov.iov_len);
	ASSERT((size_t)_uio.uio_resid == bp->b_bcount);

	// If the data is re-marshalled (which can happen if
	// the device is a HA-device or on ORB retry), we don't want to try
	// to re-lock the pages.

	if (!_is_locked) {
		// async io object. pagelock the pages
		// and then marshal. Note : aiodone expects the
		// pages to be locked.

		proc_t *p = bp->b_proc;
		iovec_t *iov = _uio.uio_iov;
		page_t **pplist = NULL;
		caddr_t a;
		size_t c;
		struct as *ads;

		bp->b_forw = (struct buf *)reqp;

		if (p) {
			ads = p->p_as;
		} else {
			ads = &kas;
		}

		// Important assumption with async io is the fact
		// that there is only one iov. There is no equivalent
		// of readv or writev for async io

		// Set the fields in buf based on aio_req
		// We can safely do so if the buffer has not been remapped
		CL_PANIC((bp->b_flags & B_REMAPPED) == 0);
		a = bp->b_un.b_addr = iov->iov_base;
		c = bp->b_bcount = (size_t)iov->iov_len;

		//
		// B_KERNBUF is obsoleted by S9
		//
#ifdef S9_BP_CLEANUP
		bp->b_flags |= (B_PHYS|B_BUSY);
#else
		bp->b_flags |= (B_PHYS|B_KERNBUF|B_BUSY);
#endif

		error = as_pagelock(ads, &pplist, a, c, (bp->b_flags & B_READ) ?
			S_WRITE : S_READ);

		if (error) {
			// This is a bad situation. The client cannot
			// lock the pages. Cannot fail the marshalling.
			// sending this error to the server which will
			// just return the error to the io call is a
			// better solution.

			// We don't want to do a server-pull in this
			// situation so we'll stick with client-push.

			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_RED,
			    ("(%p)aio_clnt,marshal,pagelock failed\n", this));
			bp->b_flags |= B_ERROR;
			bp->b_error = error;
			bp->b_flags &= ~(B_BUSY|B_WANTED|B_PHYS|B_SHADOW);
		} else {
			_is_locked = true;
			_save_iov_base = iov->iov_base;
			_save_bcount = bp->b_bcount;
			_save_pplist = pplist;
			_save_flags = bp->b_flags;
			_save_as = ads;

			//
			// AIO_PAGELOCKDONE indicates to aphysio_unlock() on
			// the client-side that it needs to perform an
			// as_pageunlock().
			//
			reqp->aio_req_flags |= AIO_PAGELOCKDONE;
		}
		bp->b_shadow = pplist;
		if (pplist != NULL) {
			_save_flags |= B_SHADOW;
			bp->b_flags |= B_SHADOW;
		}
		bp->b_resid = bp->b_bcount;
	}

	// Just to make sure that we are using a good bp
	ASSERT(bp->b_resid == bp->b_bcount);

	b.put_long(bp->b_flags);
	b.put_long(bp->b_error);
	if ((bp->b_flags & B_WRITE) != 0) {

		// Check if reply_buf support is available to do a server-pull.
		bool value = b.check_reply_buf_support((size_t)_uio.uio_resid,
		    UIO_TYPE, (void *)&_uio, CL_OP_WRITE);

		// Use client push if no reply-buf support OR
		// we couldn't lock pages down on the client earlier.

		if ((!value) || (bp->b_flags & B_ERROR)) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_GREEN,
			    ("(%p)aioclient-push\n", this));
			// client-push
			b.put_octet(bulkio_impl::CLIENT_PUSH);
			b.put_off_line(new UioBuf(&_uio));
		} else {
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_GREEN,
			    ("(%p)aioserver-pull\n", this));
			// server-pull
			b.put_octet(bulkio_impl::SERVER_PULL);
			//
			// map in aio buffer into kernel context if
			// aio data is in USERSPACE
			//
			if (_uio.uio_segflg == UIO_USERSPACE) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_BULKIO_AIO,
				    PXFS_GREEN,
				    ("in_aio_clnt (%p) bp"
				    " (%p) with REMAPPED = %d\n", this, bp,
				    bp->b_flags & B_REMAPPED));

				//
				// Check if buffer already mapped in.
				// It is possible that the aio buffer
				// was already mapped in by the process
				// that is trying to do an aio_write.
				// We use the _write_was_mapped_in
				// flag to avoid mapping out a buffer
				// that was already mapped in
				//
				if (!(bp->b_flags & B_REMAPPED)) {
					_write_was_mapped_in = true;
					bp_mapin(bp);
				}
			}
			//
			// A holder object will be referenced on the server so
			// we can access the aio struct on the client side when
			// we get back to the client from the server as part
			// of server-pull.
			//
		}
	} else {
		// aio-read
		b.put_octet(bulkio_impl::AIO_READ);

		ASSERT((bp->b_flags & B_READ) != 0);
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_GREEN,
		    ("(%p)aioread client marshal\n", this));

		ASSERT(_reply_buf == NULL);
		// Don't use reply optimization for zero-length buffers
		if (_uio.uio_resid > 0 && b.check_reply_buf_support(
		    (size_t)_uio.uio_resid, UIO_TYPE, (void *)&_uio,
		    CL_OP_READ)) {
			b.put_octet(bulkio_impl::REPLY_OPT);
			_reply_buf = b.add_reply_buffer(bp, DEFAULT_ALIGN);
		} else {
			b.put_octet(bulkio_impl::NO_REPLY_OPT);
		}
	}
}

void
bulkio_impl::aio_write_obj_clnt::wait_for_io_completion()
{
	if (_sp_io_in_progress) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_AMBER,
		    ("(%p) aio_write_obj_clnt: waiting for"
		    " io_completion\n", _clnt_obj));
		waitcv.wait();
	}
}

struct aio_req *
bulkio_impl::in_aio_clnt::get_aio(CORBA::Object_ptr)
{
	// LINTED: Exposing low access member 'in_aio_clnt::_aio'
	return (&_aio);
}

struct aio_req *
bulkio_impl::in_aio_srvr::get_aio(CORBA::Object_ptr)
{
	// LINTED: Exposing low access member 'in_aio_srvr::_aio'
	return (&_aio);
}

void
bulkio_impl::in_aio_srvr::aio_cancel()
{
	ASSERT(0); // Never called
}

//
// Constructed on the server side for both reads and writes unlike the uio
// case in which in_uio_srvr is constructed only for writes.
//
bulkio_impl::in_aio_srvr::in_aio_srvr(service &b) :
	_reply_buf(NULL),
	_io_in_progress(false),
	_server_pull_failed(false)
{
	//
	// Unmarshal stuff that's common to reads and writes.
	//
	_orig_aio_clnt = b.get_pointer();
	_uio.uio_llimit = b.get_longlong();
	_uio._uio_offset._f = b.get_longlong();
	_uio.uio_fmode = b.get_short();
	_uio.uio_segflg = UIO_SYSSPACE;
	_uio.uio_resid = (ssize_t)b.get_unsigned_long();
	_uio.uio_iovcnt = b.get_long();
	ASSERT(_uio.uio_iovcnt == 1);
	_iov.iov_base = (caddr_t)kmem_alloc((size_t)_uio.uio_resid, KM_SLEEP);

	// For aio, only 1 iov and iov_len is same as uio_resid
	_iov.iov_len = _uio.uio_resid;
	_uio.uio_iov = &_iov;
	_reqtype = b.get_long();
	_berror = b.get_long();

	uint_t iotype = (uint_t)b.get_octet();

	if ((_reqtype & B_WRITE) != 0) {
		if (iotype == bulkio_impl::CLIENT_PUSH) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_GREEN,
			    ("(%p) aio client-push"
			    " received\n", _orig_aio_clnt));

			// Get the data from the buffers for writes
			b.get_off_line_bytes(_iov.iov_base,
			    b.get_off_line_hdr());
		} else {
			ASSERT(iotype == bulkio_impl::SERVER_PULL);
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_GREEN,
			    ("(%p) aio server-pull"
			    " received\n", _orig_aio_clnt));

			nodeid_t nid = b.get_src_node().ndid;
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_AIO,
			    PXFS_GREEN,
			    ("aio server-pull"
			    "source node = %d\n", nid));

			if (CORBA::is_nil(node_array[nid])) {
				(void) bulkio_impl::resolve_holder(nid);
			}

			// server-pull
			node_array_lock.rdlock();
			bulkio_holder::holder_var wrapper =
				node_array[nid];
			node_array_lock.unlock();

			bulkio::aio_write_obj_var wrobj =
			    new bulkio_impl::aio_write_obj_srvr(&_iov,
			    _orig_aio_clnt)->get_objref();

			_server_pull_failed = false;

			// Nested invocation needs its own Environment
			Environment	e;

			if (wrapper->_handler()->server_dead()) {
				//
				// Holder reference is bad
				// => rebind to a new reference
				//
				(void) bulkio_impl::resolve_holder(nid);
				if (!CORBA::is_nil(node_array[nid])) {
					node_array_lock.rdlock();
					wrapper = node_array[nid];
					node_array_lock.unlock();
				}
			}

			// initiate server-pull
			wrapper->aio_srvr_pull(wrobj.INOUT(), e);

			if (e.exception()) {
				PXFS_DBPRINTF(
				    PXFS_TRACE_BULKIO_AIO,
				    PXFS_RED,
				    ("exception in server-pull unmarshal\n"));
				//
				// Not good. The client died during server-pull
				// or something bad happened on the client.
				// Since unmarshal can't return an exception,
				// we'll need to make this write a null-op.
				// We'll do this by setting the aio's buffer
				// flag to indicate an error.
				// pxfs server code will see this error and
				// return an exception to the client which
				// is probably dead anyway.
				//

				_server_pull_failed = true;

				//
				// Do not propagate back this exception.
				//
				e.clear();
			}
		}
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_GREEN,
		    ("aio_srvr %p read\n", _orig_aio_clnt));
		ASSERT((_reqtype & B_READ) != 0);
		ASSERT(iotype == bulkio_impl::AIO_READ);
		if (b.get_octet() == bulkio_impl::REPLY_OPT) {
			_reply_buf = b.get_reply_buffer();
		} else {
			_reply_buf = NULL;
		}
	}
	_aio.aio_uio = &_uio;
	_reqp = (struct aio_req_t *)kmem_zalloc(sizeof (struct aio_req_t),
		KM_SLEEP);
	_reqp->aio_req.aio_uio = &(_reqp->aio_req_uio);
	int err = uiodup(_aio.aio_uio, _reqp->aio_req.aio_uio, &_req_iov, 1);
	ASSERT(err == 0);
	_reqp->aio_req.aio_private = _reqp;
	_aio.aio_private = (void *)_reqp;
	struct buf *bp = &_reqp->aio_req_buf;

	//
	// Set the b_proc field here. aphysio() has been modified
	// so that it doesn't try to set the b_proc. With this modification,
	// aio retries which happens through a kernel thread can work.
	//
	bp->b_proc = curproc;

	if (_server_pull_failed) {
		// The client failed during server-pull
		bp->b_flags = B_ERROR;
		bp->b_error = EIO;
	}

	if (_berror) {
		//
		// The client has not been able to lock the pages down.
		// So set the b_error. The implementation of aioread/aiowrite
		// invocations on the server will take care of this condition.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_RED,
		    ("in_aio_srvr(%p) with error %d\n",
		    _orig_aio_clnt, _berror));
		bp->b_flags = B_ERROR;
		bp->b_error = _berror;
	}
}

bulkio_impl::in_aio_srvr::~in_aio_srvr()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("aio_srvr (%p) destructor\n", _orig_aio_clnt));
	// Wait for any reply io in progress to complete
	if (_io_in_progress) {
		waitcv.wait();
	}

	// Release reply buffer
	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}

	struct buf *bp = &_reqp->aio_req_buf;

	// This is a hack. There is no way of knowing otherwise
	// whether as_pagelock() was successful or not on the server. If
	// as_pagelock() fails, then it will reset the B_PHYS
	// flag. The strategy() routines will always return zero indicating
	// success to original call. Since we zalloc reqp structure
	// b_flags is zero, until explicitly set by aphysio(). So, if
	// B_PHYS is set, then as_pagelock() has been successful.

	if (bp->b_flags & B_PHYS) {
		as_pageunlock(bp->b_proc->p_as, bp->b_shadow, _iov.iov_base,
		    bp->b_bcount, (bp->b_flags & B_READ) ? S_WRITE : S_READ);
	}
	kmem_free(_iov.iov_base, (size_t)_iov.iov_len);
	kmem_free(_reqp, sizeof (aio_req_t));
}

void
bulkio_impl::in_aio_srvr::marshal(service &b)
{
	ASSERT(!b.get_env()->is_nonblocking());

	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_MARSHAL_S,
		FaultFunctions::generic);

	b.put_octet(bulkio_impl::REPLY);
	b.put_pointer((void *)_orig_aio_clnt);
	b.put_unsigned_long((uint32_t)_uio.uio_resid);
	ASSERT(_uio.uio_resid == _iov.iov_len);
	// Should be going through this code only for aioread.

	// If already created a reply io, this is a remarshal and
	// we wait for the previous one to complete before starting
	// new one.
	if (_io_in_progress) {
		(void) waitcv.reinit(os::SLEEP);
	}

	Buf *_reply_data = new nil_Buf((uchar_t *)_iov.iov_base,
				(uint_t)_iov.iov_len, (uint_t)_iov.iov_len,
				Buf::HEAP, &waitcv);
	_io_in_progress = true;

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("aio_srvr(%p)marshal read\n", _orig_aio_clnt));
	if (_reply_buf != NULL) {
		b.put_offline_reply(_reply_data, _reply_buf);
	} else {
		b.put_off_line(_reply_data);
	}
}

//
// This is a routine called to unlock the pages and release any
// references to the bulkio object being held. (like in read).
//
void
bulkio_impl::in_aio_clnt::aio_cancel()
{
	aio_req_t *reqp = (aio_req_t *)_aio.aio_private;
	struct buf *bp = &reqp->aio_req_buf;

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("in_aio_clnt (%p) aio_cancel\n", this));
	if (_is_locked) {
		//
		// To fix bug 4316433, we used saved values for as, pplist etc.
		// The reason we can't use the bp's b_addr field is because
		// Solaris might have modified the value if the aio was a local
		// aio.
		//
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_AMBER,
		    ("(%p) aio_cancel unlocking\n", this));
		as_pageunlock(_save_as, _save_pplist, _save_iov_base,
		    _save_bcount, (_save_flags & B_READ) ? S_WRITE : S_READ);
		//
		// AIO_PAGELOCKDONE indicates to aphysio_unlock() on
		// the client-side that it needs to perform an
		// as_pageunlock().
		//
		reqp->aio_req_flags &= ~AIO_PAGELOCKDONE;
		bp->b_flags &= ~(B_BUSY|B_WANTED|B_PHYS|B_SHADOW);
		bp->b_shadow = NULL;
		_is_locked = false;
		_save_pplist = NULL;
	}

	if (_write_was_mapped_in) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_GREEN,
		    ("(%p) aio_cancel:bp_mapout\n", this));
		bp_mapout(bp);
		_write_was_mapped_in = false;
	}
}

//
// Lock pages is called if we are retrying after the first try failed and
// an aio_cancel() was called. aio_cancel() would have unlocked pages.

void
bulkio_impl::in_aio_clnt::lock_pages()
{
	if (_is_locked) {
		return;
	}
	int error = 0;
	aio_req_t *reqp = (aio_req_t *)_aio.aio_private;
	struct buf *bp = &reqp->aio_req_buf;
	proc_t *p = bp->b_proc;
	iovec_t *iov = _uio.uio_iov;
	page_t **pplist = NULL;
	caddr_t a;
	size_t c;
	struct as *ads;

	if (p) {
		ads = p->p_as;
	} else {
		ads = &kas;
	}
	a = iov->iov_base;
	c = bp->b_bcount;

	//
	// Try to lock pages. If we fail, retry in a loop.
	// This is a workaround for bug 4257446. Look at the comments
	// in pxchr.cc for details on when this bug occurs.
	//

	// XXX Is using _save_iov_len safer than relying on bp's b_addr ?

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("in_aio_clnt (%p) lock_pages()\n", this));
	while (!_is_locked) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_GREEN,
		    ("(%p) trying to lock pages\n", this));
		error = as_pagelock(ads, &pplist, a, c, (bp->b_flags & B_READ) ?
			S_WRITE : S_READ);

		// sleep for 10 seconds if as_pagelock fails

		if (!error) {
			_is_locked = true;
		} else {
			//lint -e747
			os::usecsleep(10000000);
			//lint +e747
		}
	}
}

CORBA::Object_ptr
bulkio_impl::aio_write_obj::unmarshal(service &b)
{

	switch (b.get_octet()) {
	case REQUEST: {
		return (new bulkio_impl::aio_write_obj_clnt(b)->get_objref());
	}
	case REPLY: {
		bulkio_impl::aio_write_obj_srvr *wrobj =
		    (bulkio_impl::aio_write_obj_srvr *)b.get_pointer();
		wrobj->recv_data(b);

		//
		// The _duplicate is to counteract the release done in
		// get_param for an inout object. This retains the reference
		// count so that the object does not get destroyed prematurely
		//
		return (bulkio::aio_write_obj::_duplicate(wrobj));
	}
	default:
		ASSERT(0);
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

// The constructor called as a part of unmarshal() call on the
// client side.

//lint -e1541
bulkio_impl::aio_write_obj_clnt::aio_write_obj_clnt(service &b)
{
	bulkio_impl::in_aio_clnt *derived;
	_clnt_obj = NULL;

	// We don't handle the is_orphan() case. So panic if that's true.
	// The check prevents us from using a corrupt pointer back to the
	// original in_aio_clnt object.

	CL_PANIC(!b.get_env()->is_orphan());
	// Save a pointer to in_uio_clnt
	derived = (bulkio_impl::in_aio_clnt *)b.get_pointer();
	_clnt_obj = derived;
	_aio = derived->get_private_aio();
	derived->set_reference(this);

	_server_write_obj = b.get_pointer();
	_reply_buffer = b.get_reply_buffer();
	ASSERT(_reply_buffer != NULL);
	_sp_io_in_progress = false;
}
//lint +e1541

struct aio_req *
bulkio_impl::in_aio_clnt::get_private_aio()
{
	return (&_aio);
}

void
bulkio_impl::in_aio_clnt::set_reference(void *thisptr)
{
	if (!CORBA::is_nil(_client_write_obj)) {
		//
		// set_reference() is called again on the same in_aio_clnt
		// object during a failover because a new server-pull
		// invocation is initiated by the new server. So we check for
		// this and clean up state in the old server-pull object.
		//
		bulkio_impl::aio_write_obj_clnt::get_impl_obj(
		    _client_write_obj)->real_destructor();
	}

	bulkio_impl::aio_write_obj_clnt *p =
	    (bulkio_impl::aio_write_obj_clnt *)thisptr;

	//
	// We have to be really careful
	// since get_objref() in this case doesn't
	// increment the ref count unlike other classes.
	//

	//lint -e1061
	_client_write_obj =
	    bulkio_impl::aio_write_obj_clnt::_duplicate(p->get_objref());
	//lint +e1061
}

// At this point, server is doing a pull and has specified
// the destination for the client's data as a reply buffer
// The client has to copy the data to the reply buffer.

void
bulkio_impl::aio_write_obj_clnt::marshal(service &b)
{
	ASSERT(!b.get_env()->is_nonblocking());

	_clnt_obj_lock.lock();
	if (_sp_io_in_progress) {
		(void) waitcv.reinit(os::SLEEP);
	}
	if (_clnt_obj == NULL) {
		_clnt_obj_lock.unlock();
		return;
	}
	_clnt_obj_lock.unlock();

	b.put_octet(bulkio_impl::REPLY);
	b.put_pointer((void *)_server_write_obj);

	aio_req_t *reqp = (aio_req_t *)_aio->aio_private;
	struct buf *bp = &reqp->aio_req_buf;


	//lint -e712 -e747
	Buf *reply_data = new nil_Buf(
		    (uchar_t *)(bp->b_un.b_addr),
		    (uint_t)bp->b_bcount,
		    (uint_t)bp->b_bcount,
		    Buf::HEAP, &waitcv);
	//lint +e712 +e747

	_sp_io_in_progress = true;

	ASSERT(_reply_buffer != NULL);
	ASSERT(reply_data != NULL);

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("(%p) marshalling s-pull data\n", _clnt_obj));

	b.put_offline_reply(reply_data, _reply_buffer);
	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_SP_MARSHAL_C,
			FaultFunctions::generic);
}

bulkio_impl::aio_write_obj_clnt::~aio_write_obj_clnt()
{
}

void
bulkio_impl::aio_write_obj_clnt::real_destructor()
{
	_clnt_obj_lock.lock();
	if (_sp_io_in_progress) {
		waitcv.wait();
	}
	_clnt_obj = NULL;
	_clnt_obj_lock.unlock();

	if (_reply_buffer != NULL) {
		_reply_buffer->release();
		_reply_buffer = NULL;
	}

}

bulkio_impl::aio_write_obj_srvr::aio_write_obj_srvr(iovec_t *iov, void *orig)
{
	struct buf *bp = &_buf;
	bioinit(bp);
	_reply_buffer = (replyio_client_t *)0;
	//
	// B_KERNBUF obsoleted by S9
	//
#ifdef S9_BP_CLEANUP
	bp->b_flags = B_PHYS;
#else
	bp->b_flags = B_KERNBUF | B_PHYS;
#endif
	bp->b_resid = bp->b_bcount = (size_t)iov->iov_len;
	bp->b_un.b_addr = iov->iov_base;
	_orig_aio_clnt = orig;
	_reply_buffer = NULL;
}

void
bulkio_impl::aio_write_obj_srvr::marshal(service &b)
{
	b.put_octet(bulkio_impl::REQUEST);
	b.put_pointer((void *)_orig_aio_clnt);
	b.put_pointer((void *)this);
	_reply_buffer = b.add_reply_buffer(&_buf, DEFAULT_ALIGN);
	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_AIO_SP_MARSHAL_S,
			FaultFunctions::generic);
	ASSERT(_reply_buffer != NULL);
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("(%p)aio_write_obj_srvr marshal\n",
		    _orig_aio_clnt));
}

bulkio_impl::aio_write_obj_srvr::~aio_write_obj_srvr()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("(%p)server write obj destructor\n", _orig_aio_clnt));
	if (_reply_buffer != NULL) {
		_reply_buffer->release();
		_reply_buffer = NULL;
	}
	biodone(&_buf);
}

void
bulkio_impl::aio_write_obj_srvr::recv_data(service &b)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("(%p)server write obj receiving reply io\n",
			    _orig_aio_clnt));
	b.get_offline_reply(_reply_buffer);
	_reply_buffer = NULL;
}

//
// Called from
// aio_write_obj::unmarshal, REPLY path
//
void
bulkio_impl::aio_write_obj_srvr::recv_cancel(service &b)
{

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("write_obj_srvr %p recv_cancel\n",
		    _orig_aio_clnt));
	if (_reply_buffer) {
		b.get_offline_reply_cancel(_reply_buffer);
		_reply_buffer = NULL;
	}
}


void
bulkio_impl::aio_write_obj::_unreferenced(unref_t)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("aio_write_obj got unref\n"));
	delete this;
}

void
bulkio_impl::aio_write_obj::unmarshal_cancel(service &b)
{

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("aio_write_obj::unmarshal_cancel\n"));
	switch (b.get_octet()) {
	case REQUEST: {
		(void) b.get_pointer();
		(void) b.get_pointer();
		b.get_reply_buffer_cancel();
		break;
	}
	case REPLY: {
		bulkio_impl::aio_write_obj_srvr *wrobj =
		    (bulkio_impl::aio_write_obj_srvr *)b.get_pointer();
		wrobj->recv_cancel(b);
		break;
	}
	default:
		ASSERT(0);
	}
}

//
// The did_aio() methods are used to distinguish between local and remote
// reads/writes. This is necessary to workaround bug 4293175.
//
void
bulkio_impl::in_aio_clnt::did_aio()
{
	//
	// Assert that we don't expect to be called twice if we are on
	// a node that's local to the aioread/aiowrite.
	//
	ASSERT(!_did_local_aio);
	if (_write_was_mapped_in) {
		aio_req_t *reqp = (aio_req_t *)_aio.aio_private;
		struct buf *bp = &reqp->aio_req_buf;

		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_AIO,
		    PXFS_AMBER,
		    ("mapping out(%p) in did_aio\n", this));
		bp_mapout(bp);
		_write_was_mapped_in = false;
	}
	_did_local_aio = true;
}

void
bulkio_impl::in_aio_srvr::did_aio()
{
	// no-op
}
