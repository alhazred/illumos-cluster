//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// bulkio_impl_pgio_vn.cc - implementation of the class bulkio_impl::pgio_vn
//

#pragma ident	"@(#)bulkio_impl_pgio_vn.cc	1.10	08/05/20 SMI"

#include <sys/types.h>
#include <sys/t_lock.h>
#include <sys/param.h>
#include <sys/systm.h>
#include <sys/sysmacros.h>
#include <sys/kmem.h>
#include <sys/signal.h>
#include <sys/user.h>
#include <sys/proc.h>
#include <sys/disp.h>
#include <sys/buf.h>
#include <sys/pathname.h>
#include <sys/vfs.h>
#include <sys/vnode.h>
#include <sys/file.h>
#include <sys/uio.h>
#include <sys/conf.h>
#include <sys/debug.h>
#include <pxfs/common/pxfslib.h>

#include <sys/os.h>

#include "../version.h"
#include <pxfs/bulkio/bulkio_impl.h>

static struct vfs pgio_vfs;

#ifdef FSI

//
// Page data must always be connected to a vnode. When we ship
// a page to the server, sun cluster must have a vnode to which
// the page can be connected. Therefore, bulkio subsystem creates
// a dummy vnode with dummy vnodeops.
//
struct vnodeops *pgio_vnodeopsp = NULL;

//
// Operations of the dummy vnode is not supported.
//
static int
pgio_nosys()
{
	return (ENOSYS);
}

//
// Dummy vnodeops template for bulkio subsystem.
//
fs_operation_def_t pgio_vnodeops_template[] = {
	VOPNAME_OPEN, pgio_nosys,
	VOPNAME_CLOSE, pgio_nosys,
	VOPNAME_READ, pgio_nosys,
	VOPNAME_WRITE, pgio_nosys,
	VOPNAME_IOCTL, pgio_nosys,
	VOPNAME_SETFL, pgio_nosys,
	VOPNAME_GETATTR, pgio_nosys,
	VOPNAME_SETATTR, pgio_nosys,
	VOPNAME_ACCESS, pgio_nosys,
	VOPNAME_LOOKUP, pgio_nosys,
	VOPNAME_CREATE, pgio_nosys,
	VOPNAME_REMOVE, pgio_nosys,
	VOPNAME_LINK, pgio_nosys,
	VOPNAME_RENAME, pgio_nosys,
	VOPNAME_MKDIR, pgio_nosys,
	VOPNAME_RMDIR, pgio_nosys,
	VOPNAME_READDIR, pgio_nosys,
	VOPNAME_SYMLINK, pgio_nosys,
	VOPNAME_READLINK, pgio_nosys,
	VOPNAME_FSYNC, pgio_nosys,
	VOPNAME_INACTIVE, pgio_nosys,
	VOPNAME_FID, pgio_nosys,
	VOPNAME_RWLOCK, pgio_nosys,
	VOPNAME_RWUNLOCK, pgio_nosys,
	VOPNAME_SEEK, pgio_nosys,
	VOPNAME_CMP, pgio_nosys,
	VOPNAME_FRLOCK, pgio_nosys,
	VOPNAME_SPACE, pgio_nosys,
	VOPNAME_REALVP, pgio_nosys,
	VOPNAME_GETPAGE, pgio_nosys,
	VOPNAME_PUTPAGE, pgio_nosys,
	VOPNAME_MAP, pgio_nosys,
	VOPNAME_ADDMAP, pgio_nosys,
	VOPNAME_DELMAP, pgio_nosys,
	VOPNAME_POLL, pgio_nosys,
	VOPNAME_DUMP, pgio_nosys,
	VOPNAME_PATHCONF, pgio_nosys,
	VOPNAME_PAGEIO, pgio_nosys,
	VOPNAME_DUMPCTL, pgio_nosys,
	VOPNAME_DISPOSE, pgio_nosys,
	VOPNAME_GETSECATTR, pgio_nosys,
	VOPNAME_SETSECATTR, pgio_nosys,
	VOPNAME_SHRLOCK, pgio_nosys,
	NULL, NULL
};

int
pgio_vn_init()
{
	int error;

	error = vn_make_ops("pgio", pgio_vnodeops_template,
	    &pgio_vnodeopsp);
	return (error);
}

void
pgio_vn_uninit()
{
	if (pgio_vnodeopsp != NULL) {
		vn_freevnodeops(pgio_vnodeopsp);
	}
}

#endif

bulkio_impl::pgio_vn::pgio_vn()
{
#ifdef FSI
	vp = vn_alloc(KM_SLEEP);
	VN_SET_VFS_TYPE_DEV(vp, &pgio_vfs, VNON, 0);
	(void) vn_setops(vp, pgio_vnodeopsp);
#else
	vp = (vnode_t *)this;
	VN_INIT(vp, &pgio_vfs, VNON, 0);
	vp->v_op = NULL;
#endif
}

bulkio_impl::pgio_vn::~pgio_vn()
{
#ifdef FSI
	vn_free(vp);
#endif
	vp = NULL;
}

void
bulkio_impl::pgio_vn::set_vdata(void *ptr)
{
	vp->v_data = (caddr_t)ptr;
}
