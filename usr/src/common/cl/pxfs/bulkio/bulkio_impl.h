//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//  bulkio_impl.h - header file for implementation of bulkio interface types
//

#ifndef BULKIO_IMPL_H
#define	BULKIO_IMPL_H

#pragma ident	"@(#)bulkio_impl.h	1.7	08/05/20 SMI"

#include <sys/thread.h>
#include <vm/page.h>
#include <vm/pvn.h>
#include <sys/buf.h>
#include <sys/fdbuffer.h>
#include <sys/aio_impl.h>
#include <sys/stropts.h>

#include <h/bulkio.h>
#include <sys/sol_version.h>
#include <orb/invo/corba.h>
#include <orb/object/adapter.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>

#include "../version.h"
#include <pxfs/bulkio/bulkio_handler.h>
#include <pxfs/bulkio/bulkio_holder_impl.h>

#if SOL_VERSION >= __s10
#ifndef FSI
#define	FSI // PSARC 2001/679 Storage Infrastructure: File System Interfaces
#endif
#endif

extern bulkio_holder::holder_var this_bulkio_holder;
extern bulkio_holder::holder_var node_array[];
extern os::rwlock_t node_array_lock;

//
// AIO_PAGELOCKDONE is defined for Solaris 9.  It may also be defined
// through a patch for Solaris 8.
//
#if !defined AIO_PAGELOCKDONE
#define	AIO_PAGELOCKDONE	0x200
#endif

class bulkio_impl {
public:
	class in_pages;

	// exception passing a UNIX error code
	class exception {
	public:
		exception(int error);
		~exception();
		int _error;
	private:
		// Disallow assignments and pass by value
		exception(const exception &);
		exception &operator = (exception &);
	};

	// direction of data transfer
	enum direction { REQUEST, REPLY };

	enum type    { CLIENT_PUSH, SERVER_PULL, AIO_READ, NO_REPLY_OPT,
	    REPLY_OPT};

	// pgio_vn owns pages used as I/O buffer on server
#ifdef FSI
	class pgio_vn
#else
	class pgio_vn : public vnode
#endif
	{
	public:
		pgio_vn();
		~pgio_vn();

		void set_vdata(void *ptr);

		vnode_t *vp;
	private:
		// Disallow assignments and pass by value
		pgio_vn(const pgio_vn &);
		pgio_vn &operator = (pgio_vn &);
	};

	// Object used to send raw I/O data to a server.
	class in_uio : public impl_spec<bulkio::in_uio, bulkio_handler_inuio> {
	public:
		virtual ~in_uio();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		virtual uio_t *get_uio() = 0;
		void _unreferenced(unref_t);
	protected:
		in_uio();
	private:
		// Disallow assignments and pass by value
		in_uio(const in_uio &);
		in_uio &operator = (in_uio &);
	};

	class in_aio : public impl_spec<bulkio::in_aio, bulkio_handler_inaio> {
	public:
		virtual ~in_aio();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		virtual struct aio_req *get_aio(CORBA::Object_ptr) = 0;
		virtual void aio_cancel() = 0;
		virtual void did_aio() = 0;
		void _unreferenced(unref_t);
	protected:
		in_aio();
	private:
		// Disallow assignments and pass by value
		in_aio(const in_aio &);
		in_aio &operator = (in_aio &);
	};


	class uio_write_obj : public
	    impl_spec<bulkio::uio_write_obj, bulkio_handler_inoutuiowrite> {
	public:
		virtual ~uio_write_obj();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		void _unreferenced(unref_t);
	protected:
		uio_write_obj();
	private:
		// Disallow assignments and pass by value
		uio_write_obj(const uio_write_obj &);
		uio_write_obj &operator = (uio_write_obj &);
	};

	class pages_write_obj :	public
	    impl_spec<bulkio::pages_write_obj, bulkio_handler_inoutpageswrite> {
	public:
		virtual ~pages_write_obj();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		void _unreferenced(unref_t);
	protected:
		pages_write_obj();
	private:
		// Disallow assignments and pass by value
		pages_write_obj(const pages_write_obj &);
		pages_write_obj &operator = (pages_write_obj &);
	};

	class aio_write_obj : public
	    impl_spec<bulkio::aio_write_obj, bulkio_handler_inoutaiowrite> {
	public:
		virtual ~aio_write_obj();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		void _unreferenced(unref_t);
	protected:
		aio_write_obj();
	private:
		// Disallow assignments and pass by value
		aio_write_obj(const aio_write_obj &);
		aio_write_obj &operator = (aio_write_obj &);
	};


	// Object used to receive raw I/O data into specified buffers.
	class inout_uio : public
	    impl_spec<bulkio::inout_uio, bulkio_handler_inoutuio> {
	public:
		virtual ~inout_uio();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		virtual uio_t *get_uio() = 0;
		void _unreferenced(unref_t);
	protected:
		inout_uio();
	private:
		// Disallow assignments and pass by value
		inout_uio(const inout_uio &);
		inout_uio &operator = (inout_uio &);
	};

	//
	// The class uio_clnt_cmn is a common base class for the
	// 'in_uio_clnt' and 'inout_uio_clnt' class. Its main purpose
	// is to avoid code duplication by factoring out common code.
	//
	class uio_clnt_cmn {
	public:
		uio_clnt_cmn(uio_t *uiop);
		~uio_clnt_cmn();

	protected:
		uio_t		_uio;

		//
		// _dup_uio is used to pass a duplicate version of _uio
		// if we are trying to read/write locally. This is to fix
		// bug 4293629.
		//
		uio_t		_dup_uio;

		struct as	*_asp;
		iovec_t		_iov[DEF_IOV_MAX];

		// _dup_iov is used to fix bug 4293629.
		iovec_t		_dup_iov[DEF_IOV_MAX];

		replyio_client_t *_reply_buf[DEF_IOV_MAX];
		bool		is_locked[DEF_IOV_MAX];

		void marshal(service&);
	private:
		// Disallow assignments and pass by value
		uio_clnt_cmn(const uio_clnt_cmn &);
		uio_clnt_cmn &operator = (uio_clnt_cmn &);
	};

	//
	// The class uio_srvr_cmn is a common base class for the
	// 'in_uio_srvr' and 'inout_uio_srvr' class. Its main purpose
	// is to avoid code duplication by factoring out common code.
	//
	class uio_srvr_cmn {
	public:
		uio_srvr_cmn(service& b, bool);
		~uio_srvr_cmn();

	protected:
		static void unmarshal_cancel(service& b);
		uio_t		_uio;
		iovec_t		_iov[DEF_IOV_MAX];
		replyio_server_t *_reply_buf[DEF_IOV_MAX];
		bool		_io_in_progress[DEF_IOV_MAX];
		os::notify_t	waitcv[DEF_IOV_MAX];
		uio_t		orig_uio;	// store copy of original uio
		iovec_t		orig_iov[DEF_IOV_MAX];
	private:
		// Disallow assignments and pass by value
		uio_srvr_cmn(const uio_srvr_cmn &);
		uio_srvr_cmn &operator = (uio_srvr_cmn &);
	};

	// Client side implementation of 'in_uio'.
	class in_uio_clnt : public in_uio, private uio_clnt_cmn {
	public:
		in_uio_clnt(uio_t *uiop);
		~in_uio_clnt();
		void marshal(service& b);
		uio_t *get_uio();
		struct buf *get_clnt_buf(int bufnum);
		void set_reference(void *);
	private:

		bulkio::uio_write_obj_var _client_write_obj;

		// _clnt_buf is used to map in user-space or kernel uio data
		// into the kernel's address space so that it can be
		// used when the server requests data from the client during
		// write-pull.

		struct buf	_clnt_buf[DEF_IOV_MAX];

		// Disallow assignments and pass by value
		in_uio_clnt(const in_uio_clnt &);
		in_uio_clnt &operator = (in_uio_clnt &);
	};

	// Server side implementation of 'in_uio'.
	class in_uio_srvr : public in_uio, private uio_srvr_cmn {
	public:
		in_uio_srvr(service& b);
		~in_uio_srvr();
		void marshal(service& b);
		uio_t *get_uio();
	private:
		// Disallow assignments and pass by value
		in_uio_srvr(const in_uio_srvr &);
		in_uio_srvr &operator = (in_uio_srvr &);

		bool	_unmarshal_exception;
	};

	class in_aio_clnt : public in_aio {
	public:
		~in_aio_clnt();
		in_aio_clnt(struct aio_req *aiop);
		void marshal(service& b);
		struct aio_req *get_aio(CORBA::Object_ptr);
		struct aio_req *get_private_aio();
		void aio_cancel();
		void did_aio();
		void lock_pages();
		void recv_data(service& b);
		void recv_cancel(service& b);
		void set_reference(void *);
		static bulkio_impl::in_aio_clnt *
		    get_impl_obj(bulkio::in_aio_ptr);
		void real_destructor();
	private:
		bulkio::aio_write_obj_var _client_write_obj;

		struct aio_req		_aio;
		uio_t   		_uio;
		iovec_t			_iov;
		//
		// The _save variables are needed to workaround
		// bug 4293175.
		//
		caddr_t			_save_iov_base;
		size_t			_save_bcount;
		page_t			**_save_pplist;
		struct as		*_save_as;
		int 			_save_flags;
		bool			_is_locked;
		replyio_client_t	*_reply_buf;
		bool			_write_was_mapped_in;
		bool			_did_local_aio;
		bool			_real_destructor_was_called;

		// Disallow assignments and pass by value
		in_aio_clnt(const in_aio_clnt &);
		in_aio_clnt &operator = (in_aio_clnt &);
	};

	class in_aio_srvr : public in_aio {
	public:
		in_aio_srvr(service& b);
		~in_aio_srvr();
		void marshal(service& b);
		struct aio_req *get_aio(CORBA::Object_ptr);
		void aio_cancel();
		void did_aio();
	private:
		void		*_orig_aio_clnt;
		struct aio_req	_aio;
		aio_req_t	*_reqp;
		uio_t   	_uio;
		iovec_t 	_iov;
		iovec_t 	_req_iov;
		int		_reqtype;
		int		_berror;
		replyio_server_t *_reply_buf;
		bool		_io_in_progress;
		os::notify_t	waitcv;
		bool		_server_pull_failed;

		// Disallow assignments and pass by value
		in_aio_srvr(const in_aio_srvr &);
		in_aio_srvr &operator = (in_aio_srvr &);
	};

	// Client side implementation of 'inout_uio'.
	class inout_uio_clnt : public inout_uio, private uio_clnt_cmn {
	public:
		inout_uio_clnt(uio_t *);
		~inout_uio_clnt();
		void marshal(service &);
		void reply_opt(service &);
		uio_t *get_uio();
		void recv_data(service &);
		void recv_cancel(service &);
		void unlock_userdata();
	private:
		struct buf 	_buf[DEF_IOV_MAX];

		// Disallow assignments and pass by value
		inout_uio_clnt(const inout_uio_clnt &);
		inout_uio_clnt &operator = (inout_uio_clnt &);
	};


	// Server side implementation of 'inout_uio'.
	class inout_uio_srvr : public inout_uio, private uio_srvr_cmn {
	public:
		inout_uio_srvr(service& b);
		~inout_uio_srvr();
		void marshal(service& b);
		uio_t *get_uio();
	private:
		void *_orig_uio_clnt;

		// Disallow assignments and pass by value
		inout_uio_srvr(const inout_uio_srvr &);
		inout_uio_srvr &operator = (inout_uio_srvr &);
	};

	// Client side implementation of 'uio_write_obj'
	class uio_write_obj_clnt : public uio_write_obj {
	public:
		uio_write_obj_clnt(service &b);
		~uio_write_obj_clnt();
		void marshal(service& b);
		void real_destructor();
		void wait_for_io_completion();
		static bulkio_impl::uio_write_obj_clnt *
		    get_impl_obj(bulkio::uio_write_obj_ptr);
	private:
		void *_orig_write_obj;
		uio_t *_uiop;

		// Mirror _clnt_buf in the class in_uio_clnt

		struct buf	*_clnt_buf[DEF_IOV_MAX];

		replyio_server_t *_reply_buf[DEF_IOV_MAX];
		bool _io_in_progress[DEF_IOV_MAX];
		os::notify_t waitcv[DEF_IOV_MAX];
		void *_clnt_obj;

		// Disallow assignments and pass by value
		uio_write_obj_clnt(const uio_write_obj_clnt &);
		uio_write_obj_clnt &operator = (uio_write_obj_clnt &);
	};

	// Server side implementation of 'uio_write_obj'
	class uio_write_obj_srvr : public uio_write_obj {
	public:
		~uio_write_obj_srvr();
		uio_write_obj_srvr(uio_t *uiosrvr, void *orig);
		void marshal(service& b);
		void recv_data(service &b);
		void recv_cancel(service &b);
	private:
		void		*_orig_uio_clnt;
		uio_t		*_uiop;
		replyio_client_t *_reply_buf[DEF_IOV_MAX];
		struct buf	_buf[DEF_IOV_MAX];

		// Disallow assignments and pass by value
		uio_write_obj_srvr(const uio_write_obj_srvr &);
		uio_write_obj_srvr &operator = (uio_write_obj_srvr &);
	};

	// aio client and server write-pull objects

	class aio_write_obj_clnt : public aio_write_obj {
	public:
		aio_write_obj_clnt(service &b);
		~aio_write_obj_clnt();
		void marshal(service& b);
		void wait_for_io_completion();
		void real_destructor();
		static bulkio_impl::aio_write_obj_clnt *
		    get_impl_obj(bulkio::aio_write_obj_ptr);
	private:
		// Lock that is used to ensure mutual exclusion while
		// accessing _clnt_obj;
		os::mutex_t _clnt_obj_lock;
		void *_clnt_obj;

		// pointer to  aio_write_obj_srvr
		void *_server_write_obj;

		struct aio_req	*_aio;
		replyio_server_t *_reply_buffer;
		os::notify_t	waitcv;
		bool		_sp_io_in_progress;

		// Disallow assignments and pass by value
		aio_write_obj_clnt(const aio_write_obj_clnt &);
		aio_write_obj_clnt &operator = (aio_write_obj_clnt &);
	};

	// Server side implementation of 'aio_write_obj'
	class aio_write_obj_srvr : public aio_write_obj {
	public:
		~aio_write_obj_srvr();
		aio_write_obj_srvr(iovec_t *iov, void *orig);
		void marshal(service& b);
		void recv_data(service &b);
		void recv_cancel(service &b);
	private:
		void		*_orig_aio_clnt;
		replyio_client_t *_reply_buffer;
		struct buf	_buf;

		// Disallow assignments and pass by value
		aio_write_obj_srvr(const aio_write_obj_srvr &);
		aio_write_obj_srvr &operator = (aio_write_obj_srvr &);
	};

	class pages_write_obj_clnt : public pages_write_obj {
	public:
		pages_write_obj_clnt(service &b);
		~pages_write_obj_clnt();
		void marshal(service& b);

	private:
		// pointer to pages_write_obj_srvr
		void *_server_side_write_obj;

		// save a pointer to in_pages_clnt
		// which we'll need during create_ppbuf();
		void *_clnt_pages_obj;

		// points to in_pages_clnt::pbuf
		PpBuf *_ppbuf;

		replyio_server_t *_clnt_reply_buffer;

		// Disallow assignments and pass by value
		pages_write_obj_clnt(const pages_write_obj_clnt &);
		pages_write_obj_clnt &operator = (pages_write_obj_clnt &);
	};

	// Server side implementation of 'pages_write_obj'
	class pages_write_obj_srvr : public pages_write_obj {
	public:
		~pages_write_obj_srvr();
		pages_write_obj_srvr(struct buf *srvr_buf, void *clnt_obj);
		void marshal(service& b);
		void recv_data(service &b);
		void recv_cancel(service &b);
	private:
		// pointer to client-side pages_write_obj_clnt
		void		*_orig_pages_clnt;

		// reply buffer on server side
		replyio_client_t *_reply_buffer;

		struct buf	*_srvr_buf;

		// Disallow assignments and pass by value
		pages_write_obj_srvr(const pages_write_obj_srvr &);
		pages_write_obj_srvr &operator = (pages_write_obj_srvr &);
	};

	// Object used to send data from page frames to a server.
	class in_pages : public
	    impl_spec<bulkio::in_pages, bulkio_handler_in_pages> {
	public:
		virtual ~in_pages();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		virtual fdbuffer_t *get_buf() = 0;
		virtual void wait_on_sema() = 0;
		void _unreferenced(unref_t);
	protected:
		in_pages();
	private:
		// Disallow assignments and pass by value
		in_pages(const in_pages &);
		in_pages &operator = (in_pages &);
	};

	// Object used to receive data into specified page frames.
	class inout_pages : public
	    impl_spec<bulkio::inout_pages, bulkio_handler_inout_pages> {
	public:
		virtual ~inout_pages();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		virtual fdbuffer_t *get_buf() = 0;
		void _unreferenced(unref_t);

		//
		// Called just before calling pvn_*_done on the pages
		// that were passed to the bulkio object constructor
		// This call with either wait till all references to
		// the pages are dropped or will ensure that nobody will
		// references the pages after this call
		//
		virtual void release_pages() = 0;
	protected:
		inout_pages();
	private:
		// Disallow assignments and pass by value
		inout_pages(const inout_pages &);
		inout_pages &operator = (inout_pages &);
	};

	class in_aio_pages : public
	    impl_spec<bulkio::in_aio_pages, bulkio_handler_in_aio_pages> {
	public:
		virtual ~in_aio_pages();
		virtual void marshal(service& b) = 0;
		static CORBA::Object_ptr unmarshal(service& b);
		static void unmarshal_cancel(service& b);
		virtual fdbuffer_t *get_buf() = 0;
		void _unreferenced(unref_t);

		//
		// Called just before calling pvn_*_done on the pages
		// that were passed to the bulkio object constructor
		// This call with either wait till all references to
		// the pages are dropped or will ensure that nobody will
		// references the pages after this call
		//
		virtual void release_pages() = 0;
	protected:
		in_aio_pages();
	private:
		// Disallow assignments and pass by value
		in_aio_pages(const in_aio_pages &);
		in_aio_pages &operator = (in_aio_pages &);
	};


	// Client side implementation of 'in_pages'.
	class in_pages_clnt : public in_pages {
	public:
		in_pages_clnt(page_t *, u_offset_t, uint_t, int);
		~in_pages_clnt();
		PpBuf *create_ppbuf();
		void marshal(service& b);
		fdbuffer_t *get_buf();
		void send_pages(service& b);
		void wait_on_sema();

	private:
		page_t 		*_pp;
		u_offset_t	_off;
		uint_t		_len;
		int		_flags;
		replyio_server_t *_reply_buf;	// buffer for sending reply
		os::mutex_t	_iop; // lock for _io_in_progress
		bool		_io_in_progress;
		os::notify_t	waitcv;
		fdbuffer_t	*_fdbuf;

		PpBuf		*pbuf;
		// Disallow assignments and pass by value
		in_pages_clnt(const in_pages_clnt &);
		in_pages_clnt &operator = (in_pages_clnt &);
	};

	// Server side implementation of 'in_pages'.
	class in_pages_srvr : public in_pages {
	public:
		in_pages_srvr(service& b);
		~in_pages_srvr();
		void marshal(service& b);
		fdbuffer_t *get_buf();
		void recv_pages(service& b);
		void wait_on_sema();

	private:
		pgio_vn		_vn;
		page_t		*_pp;
		u_offset_t	_off;
		uint_t		_len;
		int		_flags;
		fdbuffer_t	*_fdbuf;
		replyio_server_t *_reply_buf;	// buffer for receiving reply
		bool		_unmarshal_exception;

		// Disallow assignments and pass by value
		in_pages_srvr(const in_pages_srvr &);
		in_pages_srvr &operator = (in_pages_srvr &);
	};

	// Client side implementation of 'inout_pages'.
	class inout_pages_clnt : public inout_pages {
	public:
		inout_pages_clnt(page_t *, u_offset_t, uint_t, int, uint_t);
		~inout_pages_clnt();
		void marshal(service& b);
		fdbuffer_t *get_buf();
		void recv_pages(service& b);
		void recv_cancel(service& b);
		void release_pages();

	private:
		page_t 		*_pp;		// pages to receive data into
		u_offset_t	_off;
		uint_t		_len;
		int		_flags;
		uint_t		_prot;
		replyio_client_t *_reply_buf;	// buffer for receiving reply
		struct buf	_buf;		// buf struct
		fdbuffer_t	*_fdbuf;

		// Disallow assignments and pass by value
		inout_pages_clnt(const inout_pages_clnt &);
		inout_pages_clnt &operator = (inout_pages_clnt &);
	};

	// Server side implementation of 'inout_pages'.
	class inout_pages_srvr : public inout_pages {
	public:
		inout_pages_srvr(service&);
		~inout_pages_srvr();
		void marshal(service&);
		void put_pages(page_t **);
		fdbuffer_t *get_buf();
		void send_pages(service&);
		void release_pages();

	private:
		void		*_clntobj;
		pgio_vn		_vn;
		page_t 		*_pp;
		u_offset_t	_off;
		uint_t		_len;
		int 		_flags;
		uint_t		_prot;
		replyio_server_t *_reply_buf;	// buffer for sending reply
		bool		_io_in_progress;
		os::notify_t	waitcv;
		fdbuffer_t	*_fdbuf;

		// Disallow assignments and pass by value
		inout_pages_srvr(const inout_pages_srvr &);
		inout_pages_srvr &operator = (inout_pages_srvr &);
	};

	// Client side implementation of in_aio_pages
	class in_aio_pages_clnt : public in_aio_pages {
	public:
		in_aio_pages_clnt(page_t *, u_offset_t, uint_t, int);
		~in_aio_pages_clnt();
		void marshal(service& b);
		fdbuffer_t *get_buf();
		void recv_pages(service& b);
		void recv_cancel(service& b);
		void release_pages();
	private:
		page_t		*_pp;
		u_offset_t	_off;
		uint_t		_len;
		int		_flags;
		replyio_client_t *_reply_buf;
		struct buf	_buf;
		fdbuffer_t	*_fdbuf;

		// Disallow assignments and pass by value
		in_aio_pages_clnt(const in_aio_pages_clnt &);
		in_aio_pages_clnt &operator = (in_aio_pages_clnt &);
        };

	// Server side implemention of in_aio_pages
	class in_aio_pages_srvr : public in_aio_pages {
	public:
		in_aio_pages_srvr(service& b);
		~in_aio_pages_srvr();
		void marshal(service& b);
		void put_pages(page_t **ppp);
		fdbuffer_t *get_buf();
		void send_pages(service& b);
		void release_pages();

	private:
		void		*_clntobj;
		pgio_vn		_vn;
		page_t		*_pp;
		u_offset_t	_off;
		uint_t		_len;
		int		_flags;
		replyio_server_t *_reply_buf;
		bool		_io_in_progress;
		os::notify_t	waitcv;
		fdbuffer_t	*_fdbuf;

		// Disallow assignments and pass by value
		in_aio_pages_srvr(const in_aio_pages_srvr &);
		in_aio_pages_srvr &operator = (in_aio_pages_srvr &);
	};

	//
	// Client side creates an 'in_uio_ptr' object for a given
	// uio structure. XXX iolen to be removed, equal to uiop->resid
	//
	static bulkio::in_uio_ptr	conv_in(uio_t *uiop, size_t iolen);

	// Server side calls get_uio to obtain a uio structure.
	static uio_t			*conv(bulkio::in_uio_ptr uioobj);

	//
	// Client side creates an 'inout_uio_ptr' object for a given
	// uio structure. XXX iolen to be removed
	//
	static bulkio::inout_uio_ptr	conv_inout(uio_t *uiop, size_t iolen);

	// Server side calls conv to obtain a uio structure.
	static uio_t			*conv(bulkio::inout_uio_ptr uioobj);

	//
	// Client side calls conv_in to create an 'in_pages_ptr' for
	// a list of pages that are to be sent to the server.
	//
	static bulkio::in_pages_ptr	conv_in(page_t *pp, u_offset_t off,
	    uint_t len, int flags);

	// Server side calls conv to obtain a list of received pages.
	static fdbuffer_t		*conv(bulkio::in_pages_ptr pages_obj);

	//
	// Client side calls conv_inout with a list of pages into which
	// data will be received.
	//
	static bulkio::inout_pages_ptr	conv_inout(page_t *pp, u_offset_t off,
	    uint_t len, int flags = 0, uint_t prot = 0);

	// Server side calls conv to get a list of pages that are suitable
	// for sending data to client.
	static fdbuffer_t	*conv(bulkio::inout_pages_ptr pages_obj);

	static bulkio::in_aio_pages_ptr		aconv_in_pgs(page_t *pp,
	    u_offset_t off, uint_t len,	int flags = 0);

	static fdbuffer_t	*conv(bulkio::in_aio_pages_ptr pages_obj);

	//
	// XXX wait_on_sema and release_pages should be idl methods
	// This would eliminate the need for the static methods
	// Or alternatively callers could keep a pointer to the implementation
	// and make a calling using that pointer instead of the object reference
	//
	static void	wait_on_sema(bulkio::in_pages_ptr pages_obj);

	static void	release_pages(bulkio::in_aio_pages_ptr pages_obj);

	static void	release_pages(bulkio::inout_pages_ptr pages_obj);

	static bulkio::in_aio_ptr	aconv_in(struct aio_req *aiop,
					    int flags);

	static struct aio_req		*aconv(bulkio::in_aio_ptr aioobj,
		CORBA::Object_ptr aiock);

	static void	aio_cancel(bulkio::in_aio_ptr aioobj);
	static void	did_aio(bulkio::in_aio_ptr aioobj);
	static int	resolve_holder(unsigned int nid);
	static int	startup();
}; // bulkio_impl

#include <pxfs/bulkio/bulkio_impl_in.h>

#endif	/* BULKIO_IMPL_H */
