//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)bulkio_holder_impl.cc	1.7	08/05/20 SMI"

#include "../version.h"
#include <pxfs/bulkio/bulkio_holder_impl.h>

// Static Variables
holder_impl *holder_impl::holder_instance = 0;

// Global Variables
bulkio_holder::holder_var this_bulkio_holder =
	bulkio_holder::holder::_nil();

bulkio_holder::holder_var node_array[NODEID_MAX];
//
// Since the node_array consists of smart pointers
// and that the smart pointer assignment is not
// atomic, we need to protect the array with a lock
//
// See bug id: 4801372 for context.
//
os::rwlock_t	node_array_lock;

holder_impl*
holder_impl::get_instance()
{
	if (!holder_instance) {
		holder_instance = new holder_impl;
	}

	return (holder_instance);
}

holder_impl::~holder_impl()
{
	// Empty
}

void
holder_impl::_unreferenced(unref_t cookie)
{
	//
	// This method should not be called. However,
	// if a _fini() method was ever added to the
	// PxFS module code, it would be possible to
	// unreference the bulkio_holder object. At
	// the moment, there is no such _fini method.
	//

	if (_last_unref(cookie)) {
		delete this;
	} else {
		ASSERT(0);
	}
}

void
holder_impl::uio_srvr_pull(bulkio::uio_write_obj_ptr &, Environment &)
{
}

void
holder_impl::pages_srvr_pull(bulkio::pages_write_obj_ptr &, Environment &)
{
}

void
holder_impl::aio_srvr_pull(bulkio::aio_write_obj_ptr &, Environment &)
{
}
