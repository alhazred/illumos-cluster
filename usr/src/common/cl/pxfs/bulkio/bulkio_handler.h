//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef BULKIO_HANDLER_H
#define	BULKIO_HANDLER_H

#pragma ident	"@(#)bulkio_handler.h	1.6	08/05/20 SMI"

#include <orb/handler/counter_handler.h>
#include <orb/invo/invocation.h>

#include "../version.h"

class bulkio_handler_kit : public handler_kit {
public:
	bulkio_handler_kit() : handler_kit(BULKIO_HKID_V1) {}
	~bulkio_handler_kit() {}

	CORBA::Object_ptr unmarshal(service &, generic_proxy::ProxyCreator,
		    CORBA::TypeId);

	void	unmarshal_cancel(service &);
	static bulkio_handler_kit &the();
	static int initialize();
	static void shutdown();
private:
	static bulkio_handler_kit *the_bulkio_handler_kit;
};

class bulkio_handler : public counter_handler {
public:
	// type of bulkio transfer
	enum bulkio_type {
		IN_UIO, INOUT_UIO,
		IN_PAGES, INOUT_PAGES,
		INOUT_UIOWRITE, IN_AIO, IN_AIO_PAGES,
		INOUT_AIOWRITE, INOUT_PAGESWRITE
	};

	static bulkio_handler_kit HK;

	virtual hkit_id_t	handler_id();

	bulkio_handler(CORBA::Object_ptr objp, bulkio_type ty) :
		_type(ty), _obj(objp) {}

	virtual			~bulkio_handler();

	// XXX put BUM methods

	virtual void		*narrow(CORBA::Object_ptr, CORBA::TypeId,
				    generic_proxy::ProxyCreator);

	virtual void		*narrow_multiple_inherit(CORBA::Object_ptr,
				    CORBA::TypeId, generic_proxy::ProxyCreator);

	virtual void		marshal(service&, CORBA::Object_ptr);

protected:
	bulkio_type	_type;
	CORBA::Object_ptr _obj;
};

class bulkio_handler_inuio : public bulkio_handler {
public:
	bulkio_handler_inuio(CORBA::Object_ptr o) :
		bulkio_handler(o, IN_UIO) {}

	virtual		~bulkio_handler_inuio();
};

class bulkio_handler_inoutuio : public bulkio_handler {
public:
	bulkio_handler_inoutuio(CORBA::Object_ptr o) :
		bulkio_handler(o, INOUT_UIO) {}

	virtual		~bulkio_handler_inoutuio();
};

class bulkio_handler_inaio : public bulkio_handler {
public:
	bulkio_handler_inaio(CORBA::Object_ptr o) :
		bulkio_handler(o, IN_AIO) {}

	virtual		~bulkio_handler_inaio();
};

class bulkio_handler_inoutuiowrite : public bulkio_handler {
public:
	bulkio_handler_inoutuiowrite(CORBA::Object_ptr o) :
		bulkio_handler(o, INOUT_UIOWRITE) {}

	virtual		~bulkio_handler_inoutuiowrite();
};

class bulkio_handler_inoutaiowrite : public bulkio_handler {
public:
	bulkio_handler_inoutaiowrite(CORBA::Object_ptr o) :
		bulkio_handler(o, INOUT_AIOWRITE) {}

	virtual		~bulkio_handler_inoutaiowrite();
};

class bulkio_handler_inoutpageswrite : public bulkio_handler {
public:
	bulkio_handler_inoutpageswrite(CORBA::Object_ptr o) :
		bulkio_handler(o, INOUT_PAGESWRITE) {}

	virtual		~bulkio_handler_inoutpageswrite();
};

class bulkio_handler_in_pages : public bulkio_handler {
public:
	bulkio_handler_in_pages(CORBA::Object_ptr o) :
		bulkio_handler(o, IN_PAGES) {}

	virtual		~bulkio_handler_in_pages();
};

class bulkio_handler_inout_pages : public bulkio_handler {
public:
	bulkio_handler_inout_pages(CORBA::Object_ptr o) :
		bulkio_handler(o, INOUT_PAGES) {}

	virtual		~bulkio_handler_inout_pages();
};

class bulkio_handler_in_aio_pages : public bulkio_handler {
public:
	bulkio_handler_in_aio_pages(CORBA::Object_ptr o) :
		bulkio_handler(o, IN_AIO_PAGES) {}

	virtual		~bulkio_handler_in_aio_pages();
};

#endif	/* BULKIO_HANDLER_H */
