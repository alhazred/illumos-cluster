//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// bulkio_impl.cc - source file for implementation of bulkio methods
//

#pragma ident	"@(#)bulkio_impl.cc	1.6	08/05/20 SMI"

#include "../version.h"
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/bulkio/bulkio_handler.h>

int
bulkio_impl::resolve_holder(unsigned int nid)
{
	//
	// In this case we would resolve with Nameserver
	// and cache the reference in the array
	//
	char name[20];
	Environment env;
	CORBA::Object_var obj;
	naming::naming_context_var ctxp = ns::root_nameserver();

	os::sprintf(name, "bulkio_holder.%d", nid);
	obj = ctxp->resolve(name, env);
	if (env.exception()) {
		env.exception()->print_exception(
		    "bulkio_holder::resolve_holder");

		os::panic("Unable to resolve nid %s\n",
		    name);
	}

	node_array_lock.wrlock();
	node_array[nid] = bulkio_holder::holder::_narrow(obj);
	node_array_lock.unlock();

	return (0);
}

int
bulkio_impl::startup()
{
	int err;
	//
	// Initialize Global Variables
	//
	if ((err = bulkio_handler_kit::initialize()) != 0) {
		return (err);
	}

	this_bulkio_holder = holder_impl::get_instance()->get_objref();

	int i;

	node_array_lock.wrlock();
	for (i = 0; i < NODEID_MAX; i++) {
		node_array[i] = bulkio_holder::holder::_nil();
	}
	node_array_lock.unlock();

	//
	// Register this_bulkio_holder with Nameservice
	//
	Environment env;
	char holder_name[20];

	os::sprintf(holder_name, "bulkio_holder.%d", orb_conf::node_number());
	naming::naming_context_var ctxv = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));
	ctxv->rebind(holder_name, this_bulkio_holder, env);
	if (env.exception()) {
		env.clear();
		return (1);
	}

	return (0);
}

//
// Called after the pxfs loadable module has been loaded.
//
extern "C" int
pxfs_bulkio_startup()
{
	return (bulkio_impl::startup());
}
