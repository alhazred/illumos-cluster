/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef BULKIO_HOLDER_IMPL_H
#define	BULKIO_HOLDER_IMPL_H

#pragma ident	"@(#)bulkio_holder_impl.h	1.7	08/05/20 SMI"

#include <h/bulkio.h>
#include <orb/object/adapter.h>

#include "../version.h"
#include <pxfs/bulkio/bulkio_handler.h>

class holder_impl : public McServerof<bulkio_holder::holder> {
public:
	static holder_impl* get_instance();
	virtual void    _unreferenced(unref_t cookie);

	void uio_srvr_pull(bulkio::uio_write_obj_ptr &wrobj,
	    Environment &_environment);
	void pages_srvr_pull(bulkio::pages_write_obj_ptr &wrobj,
	    Environment &_environment);
	void aio_srvr_pull(bulkio::aio_write_obj_ptr &wrobj,
	    Environment &_environment);

protected:
	holder_impl() {}
	holder_impl(const holder_impl&) {}
	holder_impl& operator= (const holder_impl);
	virtual		~holder_impl();

private:
	static holder_impl* holder_instance;
	static holder_impl* this_bulkio_holder;
};

#endif	/* BULKIO_HOLDER_IMPL_H */
