//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// bulkio_impl_aio_pages.cc - implementation of the class
// bulkio_impl::in_aio_pages
//

#pragma ident	"@(#)bulkio_impl_aio_pages.cc	1.7	08/05/20 SMI"

#include <sys/sol_version.h>
#include <sys/os.h>
#include <orb/fault/fault_injection.h>

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>

#if SOL_VERSION >= __s9
#define	S9_BP_CLEANUP
#endif

extern void recv_pages_common(struct buf *, service &);

CORBA::Object_ptr
bulkio_impl::in_aio_pages::unmarshal(service &b)
{
	FAULTPT_PXFS(FAULTNUM_BULKIO_AIO_PAGES_UNMARSHAL_S,
			FaultFunctions::generic);
	switch (b.get_octet()) {
	case REQUEST:
		FAULTPT_PXFS(FAULTNUM_BULKIO_AIO_PAGES_UNMRSHL_REQ,
			FaultFunctions::generic);
		return (new bulkio_impl::in_aio_pages_srvr(b)->get_objref());

	case REPLY: {
		bulkio_impl::in_aio_pages_clnt *pagesobj =
		    (bulkio_impl::in_aio_pages_clnt *)b.get_pointer();

		FAULTPT_PXFS(FAULTNUM_BULKIO_AIO_PAGES_UNMRSHL_REP,
			FaultFunctions::generic);

		pagesobj->recv_pages(b);
		//
		// The in_aio_pages object is passed as an "in" parameter
		// to the aio_callback_impl object. While PXFS has
		// loaded a REPLY flag, this is actually
		// an invocation request, and specifically the callback
		// request. When the invocation
		// finishes, the infrastructure does a release on that object.
		// We perform a duplicate in order to counteract this release
		// so that the net effect is no change in reference count.
		//
		return (bulkio::in_aio_pages::_duplicate(pagesobj));
	}

	default:
		ASSERT(0);
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

void
bulkio_impl::in_aio_pages::unmarshal_cancel(service &b)
{
	switch (b.get_octet()) {
	case REQUEST: {
		// similar to constructor for in_aio_pages_srvr
		(void) b.get_pointer();
		(void) b.get_unsigned_long();
		(void) b.get_unsigned_longlong();
		(void) b.get_long();
		if (b.get_octet() == bulkio_impl::REPLY_OPT) {
			b.get_reply_buffer_cancel();
		}
		break;
	}
	case REPLY: {
		bulkio_impl::in_aio_pages_clnt *pagesobj =
		    (bulkio_impl::in_aio_pages_clnt *)b.get_pointer();
		pagesobj->recv_cancel(b);
		break;
	}
	default :
		ASSERT(0);
		break;
	}
}

//
// class bulkio_impl::in_aio_pages_clnt
//

bulkio_impl::in_aio_pages_clnt::in_aio_pages_clnt(page_t *pp, u_offset_t off,
    uint_t len, int flags):
	_pp(pp),
	_off(off),
	_len(len),
	_flags(flags),
	_reply_buf(NULL),
	_fdbuf(NULL)
{
	bioinit(&_buf);
	//
	// B_KERNBUF is obsoleted by S9
	//
#ifdef S9_BP_CLEANUP
	_buf.b_flags = B_PAGEIO;
#else
	_buf.b_flags = B_KERNBUF | B_PAGEIO;
#endif
	_buf.b_bcount = len;
	_buf.b_bufsize = len;
	_buf.b_pages = pp;
}

bulkio_impl::in_aio_pages_clnt::~in_aio_pages_clnt()
{
	// Should have called release_pages() before doing CORBA::release or
	// destroying the bulkio object
	ASSERT(_reply_buf == NULL);
	ASSERT(_fdbuf == NULL);
}

// This routine is called just before the caller does a pvn_*_done to
// free the pages that were passed in during the constructor
// This routine should guarantee that the bulkio object or the transport buffers
// either do not have a pointer to the pages or will not access the pages
// after this
// If reply I/O is being used, a reply buffer would indicate that the
// transport may be writing data into these pages. Doing a release() on
// the reply buffer will guarantee that the transport will not write to
// these pages anymore. See implementation in tcp_transport.cc where a
// generation number is used for example and the lock in replyio_client_t
// synchronizes/waits for any ongoing writes to complete

void
bulkio_impl::in_aio_pages_clnt::release_pages()
{
	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}

	if (_fdbuf != NULL) {
		fdb_free(_fdbuf);
		_fdbuf = NULL;
	}

	// This biodone is for the bioinit done in the constructor
	// It should not have the ASYNC flag set
	ASSERT((_buf.b_flags & B_ASYNC) == 0);
	biodone(&_buf);

	// Set _pp to NULL, to catch any further uses of these pages which
	// the caller expects to free soon
	_pp = NULL;
}

void
bulkio_impl::in_aio_pages::_unreferenced(unref_t)
{
	delete this;
}

void
bulkio_impl::in_aio_pages_clnt::marshal(service &b)
{

	FAULTPT_PXFS(FAULTNUM_BULKIO_AIO_PAGES_MARSHAL_C,
			FaultFunctions::generic);
	b.put_octet(bulkio_impl::REQUEST);
	b.put_pointer((void *)this);
	b.put_unsigned_long(_len);
	b.put_unsigned_longlong(_off);
	b.put_long(_flags);
	// In case of retry, release the previous reply_buf
	if (_reply_buf != NULL) {
		_reply_buf->release();
	}
	if (b.check_reply_buf_support((size_t)_len, PAGE_TYPE,
	    NULL, CL_OP_READ)) {
		b.put_octet(bulkio_impl::REPLY_OPT);
		_reply_buf = b.add_reply_buffer(&_buf, PAGE_ALIGN);
	} else {
		b.put_octet(bulkio_impl::NO_REPLY_OPT);
	}
}

fdbuffer_t *
bulkio_impl::in_aio_pages_clnt::get_buf()
{
	size_t len = _len;
	ASSERT((_flags & B_READ));
	ASSERT((_flags & B_WRITE) == 0);

	len = roundup(len, DEV_BSIZE);
	_fdbuf = fdb_page_create(_pp, len, FDB_READ);

	return (_fdbuf);
}

void
bulkio_impl::in_aio_pages_clnt::recv_pages(service &b)
{
	if (_reply_buf != NULL) {
		b.get_offline_reply(_reply_buf);
		_reply_buf = NULL;
	} else {
		recv_pages_common(&_buf, b);
	}
}

void
bulkio_impl::in_aio_pages_clnt::recv_cancel(service &b)
{
	if (_reply_buf != NULL) {
		b.get_offline_reply_cancel(_reply_buf);
		_reply_buf = NULL;
	} else {
		b.get_off_line_cancel();
	}
}

//
// class bulkio_impl::in_aio_pages_srvr
//
bulkio_impl::in_aio_pages_srvr::in_aio_pages_srvr(service &b) :
	_io_in_progress(false),
	_fdbuf(NULL)
{
	struct seg kseg;

	kseg.s_as = &kas;

	_clntobj = b.get_pointer();
	_len = b.get_unsigned_long();
	_off = b.get_unsigned_longlong();
	_flags = b.get_long();
	_pp = page_create_va(_vn.vp, _off, (size_t)_len, PG_WAIT, &kseg, NULL);
	_reply_buf = NULL;
	if (b.get_octet() == bulkio_impl::REPLY_OPT) {
		_reply_buf = b.get_reply_buffer();
	}
}

bulkio_impl::in_aio_pages_srvr::~in_aio_pages_srvr()
{
	// Wait for any io in progress to complete
	if (_io_in_progress) {
		waitcv.wait();
	}

	// Release reply buffer
	if (_reply_buf != NULL) {
		_reply_buf->release();
		_reply_buf = NULL;
	}

	if (_fdbuf != NULL) {
		fdb_free(_fdbuf);
	}

	while (_pp != NULL) {
		page_t *pp = _pp;
		page_sub(&_pp, pp);
		page_io_unlock(pp);
		page_destroy(pp, 0);
	}
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_AIO,
	    PXFS_GREEN,
	    ("server bulkio aio obj DELETED \n"));
}

void
bulkio_impl::in_aio_pages_srvr::release_pages()
{
	// Called only on the client and not on the server
	ASSERT(!"in_aio_pages_srvr::release_pages() called");
}

void
bulkio_impl::in_aio_pages_srvr::marshal(service &b)
{
	Environment *e = b.get_env();
	ASSERT(!e->is_nonblocking());

	FAULTPT_PXFS(FAULTNUM_BULKIO_AIO_PAGES_MARSHAL_S,
			FaultFunctions::generic);

	b.put_octet(bulkio_impl::REPLY);
	b.put_pointer(_clntobj);

	// If already there is an io in progress, this is a remarshal and
	// we wait for the previous one to complete before starting
	// new one.
	if (_io_in_progress) {
		(void) waitcv.reinit(os::SLEEP);
	}

	Buf *pbuf = new PpBuf(_pp, _len, &waitcv);
	_io_in_progress = true;

	if (_reply_buf) {
		// Use put_off_line version that does reply optimization
		b.put_offline_reply(pbuf, _reply_buf);
	} else {
		b.put_off_line(pbuf, PAGE_ALIGN);
	}
}

fdbuffer_t *
bulkio_impl::in_aio_pages_srvr::get_buf()
{
	size_t len = _len;
	ASSERT((_flags & B_READ));
	ASSERT((_flags & B_WRITE) == 0);

	len = roundup(len, DEV_BSIZE);
	_fdbuf = fdb_page_create(_pp, len, FDB_READ);

	return (_fdbuf);
}
