//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// bulkio_impl_uio.cc - implementation of in_uio and inout_uio interfaces
//

#pragma ident	"@(#)bulkio_impl_uio.cc	1.7	08/05/20 SMI"

#include <sys/sol_version.h>
#include <sys/mc_probe.h>
#include <sys/os.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>

#include "../version.h"
#include <pxfs/common/pxfslib.h>
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>
#include <pxfs/bulkio/bulkio_holder_impl.h>

#if SOL_VERSION >= __s9
#define	S9_BP_CLEANUP
#endif

//
// class bulkio_impl::in_uio
//

CORBA::Object_ptr
bulkio_impl::in_uio::unmarshal(service &b)
{
	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_UIO_UNMARSHAL_S,
			FaultFunctions::generic);
	// Always called from server side
	return (new bulkio_impl::in_uio_srvr(b)->get_objref());
}

void
bulkio_impl::in_uio::unmarshal_cancel(service &b)
{
	// Always called on server side
	// Similar to constructor of in_uio_srvr(false)
	(void) b.get_longlong();
	(void) b.get_longlong();
	(void) b.get_short();
	(void) b.get_unsigned_long();
	int niov = b.get_long();
	for (int i = 0; i < niov; i++) {
		(void) b.get_unsigned_long();
	}
	switch (b.get_octet()) {
	case CLIENT_PUSH :
		b.get_off_line_cancel();
		break;
	case SERVER_PULL :
		(void) b.get_pointer();
		b.get_object_cancel();
		break;
	default :
		ASSERT(0);
		break;
	}
}

//
// class bulkio_impl::inout_uio
//

CORBA::Object_ptr
bulkio_impl::inout_uio::unmarshal(service &b)
{
	FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_UIO_UNMARSHAL_S,
			FaultFunctions::generic);

	switch (b.get_octet()) {
	case REQUEST:
		FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_UIO_UNMARSHAL_REQ,
			FaultFunctions::generic);
		return (new bulkio_impl::inout_uio_srvr(b)->get_objref());

	case REPLY: {
		FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_UIO_UNMARSHAL_REP,
			FaultFunctions::generic);
		bulkio_impl::inout_uio_clnt *uioobj =
		    (bulkio_impl::inout_uio_clnt *)b.get_pointer();

		MC_PROBE_0(bulkio_inout_uio_torecv, "clustering pxfs bulkio",
		    "");
		uioobj->recv_data(b);
		MC_PROBE_0(bulkio_inout_uio_recv, "clustering pxfs bulkio",
		    "");
		//
		// The _duplicate is to counteract the release done in
		// get_param for an inout object. This retains the reference
		// count so that the object does not get destroyed prematurely
		//
		return (bulkio::inout_uio::_duplicate(uioobj));
	}

	default:
		ASSERT(0);		// Should not happen
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

void
bulkio_impl::inout_uio::unmarshal_cancel(service &b)
{
	switch (b.get_octet()) {
	case REQUEST: {
		int i;
		// similar to constructor for
		// bulkio_impl::uio_srvr_cmn(true);
		(void) b.get_longlong();
		(void) b.get_longlong();
		(void) b.get_short();
		(void) b.get_unsigned_long();
		int niov = b.get_long();
		for (i = 0; i < niov; i++) {
			(void) b.get_unsigned_long();
		}
		// reply optimization stuff
		for (i = 0; i < niov; i++) {
			// We marshal one boolean per iovec to indicate
			// whether or not to call get_reply_buffer
			if (b.get_octet() == bulkio_impl::REPLY_OPT) {
				b.get_reply_buffer_cancel();
			}
		}
		(void) b.get_pointer();
		break;
	}

	case REPLY: {
		bulkio_impl::inout_uio_clnt *uioobj =
		    (bulkio_impl::inout_uio_clnt *)b.get_pointer();
		uioobj->recv_cancel(b);
		break;
	}
	default:
		ASSERT(0);
		break;
	}
}

//
// class bulkio_impl::in_uio_clnt
//

bulkio_impl::in_uio_clnt::in_uio_clnt(uio_t *uiop) :
	uio_clnt_cmn(uiop)
{
}	//lint !e1401


bulkio_impl::in_uio_clnt::~in_uio_clnt()
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("in_uio_clnt destructor called\n"));

	if (!CORBA::is_nil(_client_write_obj)) {
		bulkio_impl::uio_write_obj_clnt::get_impl_obj(
		    _client_write_obj)->real_destructor();
	}

	// is_locked[i] would be true only if we used server-pull
	// Unlock pages holding uio data and release buffers

	for (int i = 0; i < _uio.uio_iovcnt; i++) {
		if (is_locked[i]) {
			as_pageunlock(_asp, _clnt_buf[i].b_shadow,
				_uio.uio_iov[i].iov_base,
				(unsigned long)_uio.uio_iov[i].iov_len, S_READ);
			if (_uio.uio_segflg == UIO_USERSPACE) {
				bp_mapout(&_clnt_buf[i]);
			}
			biodone(&_clnt_buf[i]);
			is_locked[i] = false;
		}
	}
}

void
bulkio_impl::in_uio::_unreferenced(unref_t)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("in_uio unref\n"));
	delete this;
}

void
bulkio_impl::in_uio_clnt::marshal(service &b)
{
	MC_PROBE_0(bulkio_in_uio_c_marsh_start, "clustering pxfs bulkio", "");

	// can't do nonblocking invocations with user data
	ASSERT(!b.get_env()->is_nonblocking());

	FAULTPT_PXFS(FAULTNUM_BULKIO_IN_UIO_MARSHAL_C, FaultFunctions::generic);

	//
	// All this is needed only for the server-pull case
	// We need to put the uio data into a buf struct
	// mapped into the kernel's address space so that
	// we can access this data when the request for data arrives
	// from the server.
	// Note that the request arrives in the kernel's context.
	// This code is similar to inout_uio_clnt.
	//
	// _uio was initialized by uiodup() in uio_clnt_cmn()
	//
	struct iovec *iov = _uio.uio_iov;
	struct proc *procp;
	ssize_t c;
	page_t **pplist;
	int i;
	ssize_t length = _uio.uio_resid;

	if (_uio.uio_segflg == UIO_USERSPACE) {
		procp = ttoproc(curthread);
		_asp = procp->p_as;
	} else {
		procp = NULL;
		_asp = &kas;
	}
	//
	// We need this messy reference from in_uio_clnt to the client-side
	// object created during server-pull because of the following.
	// Sometimes, the invocation completes without TCP (Solaris layer,
	// not tcp-transport) receiving acks for messages. At the same time,
	// there is no ordering between when in_uio_clnt's destructor is
	// called and when uio_write_obj_clnt's destructor is called.
	// Now all these objects, as well as TCP, point to same buffer. So
	// if destructors complete in wrong order OR if TCP hasn't received
	// acks and the data it points to has been destroyed, we get a
	// panic.
	// To avoid this, in_uio_clnt holds a reference to uio_write_obj_clnt.
	// To wait for TCP to get done with transmission of data,
	// in_uio_clnt, in effect, calls uio_write_obj_clnt's destructor -- to
	// wait for TCP to finish using server-pull data buffers.
	// Then in_uio_clnt frees the reference to uio_write_obj_clnt.
	// Things get more complicated if there's a re-marshal in the
	// mean time due to path or server failures. That's what is checked
	// by the code in the next few lines.
	//
	// Check if we have a reference to uio_write_obj_clnt. If so,
	// we are in a re-marshal. Either a path has failed or the server
	// has died. So we need to wait for a signal to let
	// uio_write_obj_clnt's put_offline_reply() complete.
	//
	if (!CORBA::is_nil(_client_write_obj)) {
		bulkio_impl::uio_write_obj_clnt::get_impl_obj(
		    _client_write_obj)->wait_for_io_completion();
	}

	bool value = b.check_reply_buf_support((size_t)_uio.uio_resid,
			    UIO_TYPE, (void *)&_uio, CL_OP_WRITE);

	int error;

	if (value) {
		for (i = 0; i < _uio.uio_iovcnt; i++, iov++) {
			struct buf *bp = &_clnt_buf[i];
			c = MIN(iov->iov_len, length);

			//
			// Check if pages have already been locked due to
			// previous marshalling tries. If zero length, skip
			// reply optimization.
			//
			if ((c > 0) && !is_locked[i]) {
				if ((error = as_pagelock(_asp, &pplist,
				    iov->iov_base, (unsigned long)iov->iov_len,
				    S_READ)) == 0) {

					is_locked[i] = true;
					bioinit(bp);
					//
					// B_KERNBUF is obsoleted by S9
					//
#ifdef S9_BP_CLEANUP
					bp->b_flags = B_PHYS;
#else
					bp->b_flags = B_KERNBUF | B_PHYS;
#endif
					bp->b_proc = procp;
					bp->b_un.b_addr = iov->iov_base;
					bp->b_shadow = pplist;

					//lint -e732
					bp->b_bcount = c;

					//lint +e732
					if (pplist != NULL) {
						bp->b_flags |= B_SHADOW;
					}
					bp->b_resid = bp->b_bcount;
					if (_uio.uio_segflg == UIO_USERSPACE) {
						bp_mapin(bp);
					}
				} else {

					//
					// as_pagelock() failed.
					// Return an exception and let PXFS
					// handle it.
					// We have to be careful to
					// make it seem like the marshal
					// did not occur, before throwing
					// an exception. In this case
					// we are safe since we haven't
					// put anything on the sendstream
					// or mapped in or locked pages.

					b.get_env()->system_exception(
					    CORBA::BAD_PARAM((uint_t)error,
					    CORBA::COMPLETED_NO));

				}
			}
			ASSERT(length >= c);
			length -= c;
		}
	}

	bulkio_impl::uio_clnt_cmn::marshal(b);
	MC_PROBE_0(bulkio_in_uio_c_marsh_cmn, "clustering pxfs bulkio", "");

	if (!value) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_UIO,
		    PXFS_GREEN,
		    ("Starting client push on client\n"));
		b.put_octet(bulkio_impl::CLIENT_PUSH);
		//
		// The UioBuf must be freed and data copied out by the
		// transport before sendstream::send returns. Other user
		// context will be lost
		//
		b.put_off_line(new UioBuf(&_uio));
	} else {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_UIO,
		    PXFS_GREEN,
		    ("Starting server pull on client\n"));
		//
		// This is a server pull type of write in progress.
		// So we create a holder object and send
		// it over to the server so that the server
		// can invoke a method and pull the data
		// from the client
		//
		b.put_octet(bulkio_impl::SERVER_PULL);
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_UIO,
		    PXFS_GREEN,
		    ("Client object pointer= %p\n", this));
		b.put_pointer((void *)this);
	}
	MC_PROBE_0(bulkio_in_uio_c_marsh_end, "clustering pxfs bulkio", "");
}

//
// It is unsafe to do multiple get_uio()'s. We could modify this function
// to pass in a dup_uio and dup_iov.
//
uio_t *
bulkio_impl::in_uio_clnt::get_uio()
{
	//
	// We need to do an uiodup() since Solaris physio modifies iov_base.
	// This matters to us only when the client and server are co-located
	// on the same node.
	//

	if (uiodup(&_uio, &_dup_uio, _dup_iov, DEF_IOV_MAX)) {
		CL_PANIC(!"Cannout uiodup in in_uio_clnt::get_uio()");
	}
	return (&_dup_uio);
}

// Return the address of _clnt_buf[bufnum] belonging to
// this object. Used only for server-pull.

struct buf *
bulkio_impl::in_uio_clnt::get_clnt_buf(int bufnum)
{
	return (&_clnt_buf[bufnum]);
}

//
// class bulkio_impl::in_uio_srvr
//

bulkio_impl::in_uio_srvr::in_uio_srvr(service &b) :
	uio_srvr_cmn(b, false)
{
	MC_PROBE_0(bulkio_in_uio_s_con_start, "clustering pxfs bulkio", "");

	uint_t temp;
	_unmarshal_exception = false;

	if ((temp = (uint_t)b.get_octet()) == bulkio_impl::CLIENT_PUSH) {
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_UIO,
		    PXFS_GREEN,
		    ("in_uio_srvr CLIENT_PUSH\n"));
		//
		// We currenly only allocate a single buf for all of the uio
		// which is the same orig_iov[0].iov_base
		//
		b.get_off_line_bytes(orig_iov[0].iov_base,
		    b.get_off_line_hdr());
		MC_PROBE_0(bulkio_in_uio_s_con_push, "clustering pxfs bulkio",
		    "");
	} else {
		// This is a large write. The client has sent
		// in the uio skeleton with just iov information
		// and no data. The server gets the holder object
		// and invokes uio_srvr_pull() which is a method
		// on the holder object. The uio_write_obj which
		// is a bulkio object is specified as a INOUT()
		// variable to the uio_srvr_pull() call.
		// The uio_write_obj will save the original
		// in_uio_clnt object sent by the client.
		//
		ASSERT(temp == bulkio_impl::SERVER_PULL);
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO_UIO,
		    PXFS_GREEN,
		    ("in_uio_srvr SERVER_PULL\n"));

		void *orig_uio_clnt = b.get_pointer();

		nodeid_t nid = b.get_src_node().ndid;
		if (CORBA::is_nil(node_array[nid])) {
			(void) bulkio_impl::resolve_holder(nid);
		}

		node_array_lock.rdlock();
		bulkio_holder::holder_var wrapper =
			node_array[nid];
		node_array_lock.unlock();

		bulkio::uio_write_obj_var wrobj =
		    new bulkio_impl::uio_write_obj_srvr(&orig_uio,
					orig_uio_clnt)->get_objref();

		// Nested invocation requires its own Environment
		Environment	e;

		if (wrapper->_handler()->server_dead()) {
			//
			// Holder reference is bad
			// => rebind to a new reference
			//
			(void) bulkio_impl::resolve_holder(nid);
			if (!CORBA::is_nil(node_array[nid])) {
				node_array_lock.rdlock();
				wrapper = node_array[nid];
				node_array_lock.unlock();
			}
		}

		wrapper->uio_srvr_pull(wrobj.INOUT(), e);
		if (e.exception()) {
			_unmarshal_exception = true;
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_UIO,
			    PXFS_RED,
			    ("uio_srvr (%p) got exception in unmarshal\n",
			    this));
			//
			// Our framework doesn't handle exceptions in
			// unmarshal so we need to clear the exception.
			//
			e.clear();
		}
		MC_PROBE_0(bulkio_in_uio_s_con_pull, "clustering pxfs bulkio",
		    "");
	}
	MC_PROBE_0(bulkio_in_uio_s_con_end, "clustering pxfs bulkio", "");
}

bulkio_impl::in_uio_srvr::~in_uio_srvr()
{
}

void
bulkio_impl::in_uio_srvr::marshal(service &)
{
	ASSERT(0);		// Should not happen
}

uio_t *
bulkio_impl::in_uio_srvr::get_uio()
{
	if (!_unmarshal_exception) {
		return (&_uio);
	} else
		return (NULL);
}

//
// class bulkio_impl::inout_uio_clnt
//

bulkio_impl::inout_uio_clnt::inout_uio_clnt(uio_t *uiop) :
	uio_clnt_cmn(uiop)
{
}	//lint !e1401

bulkio_impl::inout_uio_clnt::~inout_uio_clnt()
{
	unlock_userdata();
}

void
bulkio_impl::inout_uio::_unreferenced(unref_t)
{
	delete this;
}

void
bulkio_impl::inout_uio_clnt::marshal(service &b)
{
	MC_PROBE_0(bulkio_inout_uio_c_marsh_start, "clustering pxfs bulkio",
	    "");
	b.put_octet(bulkio_impl::REQUEST);

	FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_UIO_MARSHAL_C,
		FaultFunctions::generic);

	// Do the common part of marshalling
	bulkio_impl::uio_clnt_cmn::marshal(b);
	reply_opt(b);
	b.put_pointer((void *)this);
	MC_PROBE_0(bulkio_inout_uio_c_marsh_end, "clustering pxfs bulkio",
	    "");
}

void
bulkio_impl::inout_uio_clnt::recv_data(service &b)
{
	uint_t total_bytes = b.get_unsigned_long();
	ASSERT(_uio.uio_resid <= INT_MAX);
	ASSERT(total_bytes <= (uint_t)_uio.uio_resid);
	uint_t nbytes, recv_bytes = 0;
	struct iovec *iov = _iov;
	for (int i = 0; recv_bytes < total_bytes; i++, iov++) {
		ASSERT(i < _uio.uio_iovcnt);
		nbytes = MIN((uint_t)iov->iov_len, total_bytes - recv_bytes);
		recv_bytes += nbytes;
		ASSERT((nbytes == (uint_t)iov->iov_len) ||
		    (recv_bytes == total_bytes));
		if (_reply_buf[i] != NULL) {
			b.get_offline_reply(_reply_buf[i]);
			_reply_buf[i] = NULL;
		} else {
			if (_uio.uio_segflg == UIO_USERSPACE) {
				b.get_off_line_u_bytes(iov->iov_base, nbytes);
			} else {
				ASSERT(_uio.uio_segflg == UIO_SYSSPACE);
				b.get_off_line_bytes(iov->iov_base, nbytes);
			}
		}
	}
	unlock_userdata();
	// make sure resid etc are correctly set to indicate no
	// data was transfered. In this case - _uio is left unchanged to
	// what was passed in.
}

void
bulkio_impl::inout_uio_clnt::recv_cancel(service &b)
{
	uint_t total_bytes = b.get_unsigned_long();
	ASSERT((ssize_t)total_bytes <= _uio.uio_resid);

	uint_t nbytes, recv_bytes = 0;
	struct iovec *iov = _iov;
	for (int i = 0; recv_bytes < total_bytes; i++, iov++) {
		ASSERT(i < _uio.uio_iovcnt);
		nbytes = MIN((uint_t)iov->iov_len, total_bytes - recv_bytes);
		recv_bytes += nbytes;
		ASSERT((nbytes == (uint_t)iov->iov_len) ||
		    (recv_bytes == total_bytes));
		if (_reply_buf[i] != NULL) {
			b.get_offline_reply_cancel(_reply_buf[i]);
			_reply_buf[i] = NULL;
		} else {
			b.get_off_line_cancel();
		}
	}
	unlock_userdata();
	// make sure resid etc are correctly set to indicate no
	// data was transfered. In this case - _uio is left unchanged to
	// what was passed in.
}

//
// It is unsafe to do multiple get_uio()'s. We could modify this function
// to pass in a dup_uio and dup_iov.
//
uio_t *
bulkio_impl::inout_uio_clnt::get_uio()
{
	//
	// We need a uiodup() since Solaris physio modifies iov_len. This
	// matters only if the client and server are co-located on the same
	// node.
	//

	if (uiodup(&_uio, &_dup_uio, _dup_iov, DEF_IOV_MAX)) {
		CL_PANIC(!"Cannout uiodup in inout_uio_clnt::get_uio()");
	}
	return (&_dup_uio);
}

//
// class bulkio_impl::inout_uio_srvr
//
bulkio_impl::inout_uio_srvr::inout_uio_srvr(service &b) :
	uio_srvr_cmn(b, true)
{
	_orig_uio_clnt = b.get_pointer();
}

bulkio_impl::inout_uio_srvr::~inout_uio_srvr()
{
}

void
bulkio_impl::inout_uio_srvr::marshal(service &b)
{
	MC_PROBE_0(bulkio_inout_uio_s_marshal_start, "clustering pxfs bulkio",
	    "");
	ASSERT(!b.get_env()->is_nonblocking());

	FAULTPT_PXFS(FAULTNUM_BULKIO_INOUT_UIO_MARSHAL_S,
		FaultFunctions::generic);

	b.put_octet(bulkio_impl::REPLY);
	b.put_pointer((void *)_orig_uio_clnt);

	ASSERT(orig_uio.uio_resid >= _uio.uio_resid);
	uint_t total_bytes = (uint_t)(orig_uio.uio_resid - _uio.uio_resid);
	b.put_unsigned_long(total_bytes);

	uint_t nbytes, sent_bytes = 0;
	struct iovec *iov = orig_iov;

	for (int i = 0; sent_bytes < total_bytes; i++, iov++) {
		ASSERT(i < orig_uio.uio_iovcnt);

		// If already there is an io in progress,
		// this is a remarshal and we wait for the previous one
		// to complete before starting new one.
		if (_io_in_progress[i]) {
			(void) waitcv[i].reinit(os::SLEEP);
		}

		nbytes = MIN((uint_t)iov->iov_len, total_bytes - sent_bytes);
		Buf *_reply_data = new nil_Buf((uchar_t *)(iov->iov_base),
				nbytes, nbytes, Buf::HEAP, &waitcv[i]);

		MC_PROBE_0(bulkio_inout_uio_s_marshal_new,
		    "clustering pxfs bulkio", "");
		_io_in_progress[i] = true;

		if (_reply_buf[i] != NULL) {
			b.put_offline_reply(_reply_data, _reply_buf[i]);
			MC_PROBE_0(bulkio_inout_uio_s_marshal_reply,
			    "clustering pxfs bulkio", "");
		} else {
			b.put_off_line(_reply_data);
			MC_PROBE_0(bulkio_inout_uio_s_marshal_line,
			    "clustering pxfs bulkio", "");
		}
		sent_bytes += nbytes;
	}
	MC_PROBE_0(bulkio_inout_uio_s_marshal_end, "clustering pxfs bulkio",
	    "");
}

uio_t *
bulkio_impl::inout_uio_srvr::get_uio()
{
	// LINTED: Exposing low access member 'inout_uio_srvr::_uio'
	return (&_uio);
}

//
// class bulkio_impl::uio_clnt_cmn
//

void
bulkio_impl::uio_clnt_cmn::marshal(service &b)
{
	b.put_longlong(_uio.uio_llimit);
	b.put_longlong(_uio._uio_offset._f);
	b.put_short(_uio.uio_fmode);
	b.put_unsigned_long((uint32_t)_uio.uio_resid);
	b.put_long(_uio.uio_iovcnt);
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("(%p) uio_clnt_cmn marshal\n", this));
	for (int i = 0; i < _uio.uio_iovcnt; i++) {
		b.put_unsigned_long((uint32_t)_iov[i].iov_len);
	}
}

void
bulkio_impl::inout_uio_clnt::reply_opt(service &b)
{
	struct iovec *iov = _uio.uio_iov;
	struct proc *procp;
	ssize_t c;
	page_t **pplist;
	int i;
	ssize_t length = _uio.uio_resid;

	if (_uio.uio_segflg == UIO_USERSPACE) {
		procp = ttoproc(curthread);
		_asp = procp->p_as;
	} else {
		procp = NULL;
		_asp = &kas;
	}

	for (i = 0; i < _uio.uio_iovcnt; i++, iov++) {
		struct buf *bp = &_buf[i];
		c = MIN(iov->iov_len, length);

		//
		// Check if pages have already been locked due to previous
		// marshalling tries. If zero length, skip reply optimization
		//
		if ((c > 0) && !is_locked[i]) {
			int	result = as_pagelock(_asp, &pplist,
			    iov->iov_base, (size_t)iov->iov_len, S_WRITE);
			if (result == 0) {
				// Page lock succeeded
				is_locked[i] = true;

				bioinit(bp);
				//
				// B_KERNBUF is obsoleted by S9
				//
#ifdef S9_BP_CLEANUP
				bp->b_flags = B_PHYS;
#else
				bp->b_flags = B_KERNBUF | B_PHYS;
#endif
				bp->b_proc = procp;
				bp->b_un.b_addr = iov->iov_base;
				bp->b_shadow = pplist;
				bp->b_bcount = (size_t)c;
				if (pplist != NULL) {
					bp->b_flags |= B_SHADOW;
				}
				bp->b_resid = bp->b_bcount;
			} else {
				//
				// as_pagelock() failed.
				// Return an exception and let PXFS handle it.
				// Any locked pages will be unlocked by
				// the destructor of inout_uio_clnt.
				//
				b.get_env()->system_exception(
					    CORBA::BAD_PARAM((uint_t)result,
					    CORBA::COMPLETED_NO));
				PXFS_DBPRINTF(
				    PXFS_TRACE_BULKIO_UIO,
				    PXFS_RED,
				    ("inout_uio_clnt::reply_opt "
				    "as_pagelock failed error %d size %d\n",
				    result, iov->iov_len));
			}
		}
		if (is_locked[i] && b.check_reply_buf_support(
		    (size_t)_uio.uio_resid, UIO_TYPE, (void *)&_uio,
		    CL_OP_READ)) {
			// Avoid use of add_reply_buffer for 0 lengths
			ASSERT(bp->b_bcount > 0);
			if (_reply_buf[i] != NULL) {
				// in case of retry, release the previous buffer
				_reply_buf[i]->release();
				_reply_buf[i] = NULL;
			}

			// Verify we have a good struct buf
			ASSERT(bp->b_error == 0);
			ASSERT(bp->b_resid == bp->b_bcount);

			// Indicate that add_reply_buffer was done
			b.put_octet(bulkio_impl::REPLY_OPT);
			// uio data is not guaranteed to be page-aligned.
			_reply_buf[i] = b.add_reply_buffer(bp, DEFAULT_ALIGN);
		} else {
			ASSERT(_reply_buf[i] == NULL);
			// Indicate that no reply_buffer was added
			b.put_octet(bulkio_impl::NO_REPLY_OPT);
		}
		ASSERT(length >= c);
		length -= c;
	}
}

void
bulkio_impl::inout_uio_clnt::unlock_userdata()
{
	for (int i = 0; i < _uio.uio_iovcnt; i++) {
		if (is_locked[i]) {
			if (_reply_buf[i] != NULL) {
				_reply_buf[i]->release();
				_reply_buf[i] = NULL;
			}
			as_pageunlock(_asp, _buf[i].b_shadow,
				_uio.uio_iov[i].iov_base,
				(size_t)_uio.uio_iov[i].iov_len, S_WRITE);
			biodone(&_buf[i]);
			is_locked[i] = false;
		} else {
			ASSERT(_reply_buf[i] == NULL);
		}
	}
}

bulkio_impl::uio_clnt_cmn::uio_clnt_cmn(uio_t *uiop) :
	_asp(NULL)
{
	ASSERT((uiop->uio_segflg == UIO_USERSPACE) ||
		(uiop->uio_segflg == UIO_SYSSPACE));

	ASSERT(uiop->uio_iovcnt <= DEF_IOV_MAX);

	(void) uiodup(uiop, &_uio, _iov, DEF_IOV_MAX);

	for (int i = 0; i < DEF_IOV_MAX; i++) {
		_reply_buf[i] = NULL;
		is_locked[i] = false;
	}
}	//lint !e1401

bulkio_impl::uio_clnt_cmn::~uio_clnt_cmn()
{
}

bulkio_impl::uio_srvr_cmn::uio_srvr_cmn(service &b, bool check_reply_opt)
{
	MC_PROBE_0(bulkio_uio_srvr_cmn_c_start, "clustering pxfs bulkio",
	    "");
	int niov;
	char *iobuf = NULL;

	_uio.uio_llimit = b.get_longlong();
	_uio._uio_offset._f = b.get_longlong();
	_uio.uio_fmode = b.get_short();
	_uio.uio_segflg = UIO_SYSSPACE;
	_uio.uio_resid = (ssize_t)b.get_unsigned_long();
	niov = _uio.uio_iovcnt = b.get_long();
	ASSERT(niov <= DEF_IOV_MAX);
	_uio.uio_iov = _iov;

	// XXX If alignment is important, should allocate sep buffers for each
	// iovec.

	// We sleep until space is available on the server side.
	// Alternately we could do a non-blocking kmem_alloc and retry the
	// write from the client side.

	if (_uio.uio_resid > 0) {
		iobuf = (char *)kmem_alloc((size_t)_uio.uio_resid, KM_SLEEP);
	}

	ssize_t offset = 0;
	int i;
	for (i = 0; i < niov; i++) {
		_io_in_progress[i] = false;
		_reply_buf[i] = NULL;

#if defined(_LP64)
		_iov[i].iov_len = b.get_unsigned_long();
#else
		_iov[i].iov_len = (long)b.get_unsigned_long();
#endif
		if (offset < _uio.uio_resid) {
			ASSERT(iobuf != NULL);
			_iov[i].iov_base = &iobuf[offset];
			offset += _iov[i].iov_len;
		} else {
			_iov[i].iov_base = NULL;
		}
	}
	//
	// We take a copy of the original uio struct as the PXFS servers
	// modify _uio which they get access through get_uio()
	// Should not fail as we already checked niov
	//
	(void) uiodup(&_uio, &orig_uio, orig_iov, DEF_IOV_MAX);

	if (!check_reply_opt)
		return;

	// If doing reply optimization get the reply_buffers
	for (i = 0; i < niov; i++) {
		// We marshal one boolean per iovec to indicate whether or
		// not to call get_reply_buffer
		if (b.get_octet() == bulkio_impl::REPLY_OPT) {
			_reply_buf[i] = b.get_reply_buffer();
		}
	}
	MC_PROBE_0(bulkio_uio_srvr_cmn_c_end, "clustering pxfs bulkio",
	    "");
}	//lint !e1741

bulkio_impl::uio_srvr_cmn::~uio_srvr_cmn()
{
	MC_PROBE_0(bulkio_uio_srvr_cmn_d_start, "clustering pxfs bulkio",
	    "");
	int i;

	for (i = 0; i < orig_uio.uio_iovcnt; i++) {
		// Wait for any reply io in progress to complete
		if (_io_in_progress[i]) {
			waitcv[i].wait();
		}
		if (_reply_buf[i] != NULL) {
			_reply_buf[i]->release();
			_reply_buf[i] = NULL;
		}
	}
	MC_PROBE_0(bulkio_uio_srvr_cmn_d_rel, "clustering pxfs bulkio",
	    "");
	// We allocate only a single buffer for all of the uio data and
	// that is in orig_iov[0]
	if (orig_uio.uio_resid > 0) {
		kmem_free(orig_iov[0].iov_base, (size_t)orig_uio.uio_resid);
	}
	MC_PROBE_0(bulkio_uio_srvr_cmn_d_end, "clustering pxfs bulkio",
	    "");
}

CORBA::Object_ptr
bulkio_impl::uio_write_obj::unmarshal(service &b)
{

	switch (b.get_octet()) {
	case REQUEST: {
		return (new bulkio_impl::uio_write_obj_clnt(b)->get_objref());
	}
	case REPLY: {
		bulkio_impl::uio_write_obj_srvr *wrobj =
		    (bulkio_impl::uio_write_obj_srvr *)b.get_pointer();
		wrobj->recv_data(b);
		// The _duplicate is to counteract the release done in
		// get_param for an inout object. This retains the reference
		// count so that the object does not get destroyed prematurely
		return (bulkio::uio_write_obj::_duplicate(wrobj));
	}
	default:
		ASSERT(0);
		b.get_env()->system_exception(
			CORBA::INV_OBJREF(0, CORBA::COMPLETED_NO));
		return (CORBA::Object::_nil());
	}
}

void
bulkio_impl::uio_write_obj_srvr::recv_data(service &b)
{
	struct iovec *iov = _uiop->uio_iov;
	for (int i = 0; i < _uiop->uio_iovcnt; i++, iov++) {
		// sender skips 0 length iovs
		if (iov->iov_len == 0)
			continue;

		if (_reply_buf[i] != NULL) {
			b.get_offline_reply(_reply_buf[i]);
			_reply_buf[i] = NULL;
		} else {
			// We assume here that _uiop points at a uio struct
			// created by uio_srvr_cmn which allocates memory
			// by kmem_alloc. So we can directly use the addresses
			// in get_off_line_bytes.
			uint_t len = b.get_off_line_hdr();

			ASSERT(len <= (size_t)iov->iov_len);
			ASSERT(iov->iov_base == _buf[i].b_un.b_addr);
			b.get_off_line_bytes(iov->iov_base, len);
		}
	}
}

void
bulkio_impl::uio_write_obj_srvr::recv_cancel(service &b)
{
	for (int i = 0; i < _uiop->uio_iovcnt; i++) {
		if (_reply_buf[i] != NULL) {
			b.get_offline_reply_cancel(_reply_buf[i]);
			_reply_buf[i] = NULL;
		} else {
			b.get_off_line_cancel();
		}
	}
}

void
bulkio_impl::uio_write_obj::unmarshal_cancel(service &b)
{

	switch (b.get_octet()) {
	case REQUEST: {
		// Similar to uio_write_obj_clnt constructor
		uio_t *p;

		p = ((bulkio_impl::in_uio_clnt *)b.get_pointer())->get_uio();
		(void) b.get_pointer();

		for (int i = 0; i < p->uio_iovcnt; i++) {
			b.get_reply_buffer_cancel();
		}
		break;
	}
	case REPLY: {
		bulkio_impl::uio_write_obj_srvr *wrobj =
		    (bulkio_impl::uio_write_obj_srvr *)b.get_pointer();
		wrobj->recv_cancel(b);
		break;
	}
	default:
		ASSERT(0);
		break;
	}
}

void
bulkio_impl::uio_write_obj_clnt::wait_for_io_completion()
{
	// This can only happen if in_uio_clnt was being
	// remarshalled.

	int i;
	iovec_t *iov = _uiop->uio_iov;

	for (i = 0; i < _uiop->uio_iovcnt; i++, iov++) {
		// skip 0 length iovs
		if (iov->iov_len == 0) {
			ASSERT(!_io_in_progress[i]);
			continue;
		}

		if (_io_in_progress[i]) {
			waitcv[i].wait();
		}
	}
}

// At this point, server is doing a pull and has specified
// the destination for the client's data as a reply buffer
// The client has to copy the data to the reply buffer.

void
bulkio_impl::uio_write_obj_clnt::marshal(service &b)
{
	ASSERT(!b.get_env()->is_nonblocking());

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("marshalling server-pull data to client\n"));

	int i;
	iovec_t *iov = _uiop->uio_iov;
	b.put_octet(bulkio_impl::REPLY);
	b.put_pointer((void *)_orig_write_obj);

	for (i = 0; i < _uiop->uio_iovcnt; i++, iov++) {
		// skip 0 length iovs
		if (iov->iov_len == 0) {
			ASSERT(!_io_in_progress[i]);
			continue;
		}

		// If already there is an io in progress,
		// this is a remarshal and we wait for the previous one
		// to complete before starting new one.
		if (_io_in_progress[i]) {
			(void) waitcv[i].reinit(os::SLEEP);
		}

		//lint -e747 -e712
		Buf *reply_data = new nil_Buf(
			    (uchar_t *)(_clnt_buf[i]->b_un.b_addr),
			    (uint_t)_clnt_buf[i]->b_bcount,
			    (uint_t)_clnt_buf[i]->b_bcount,
			    Buf::HEAP, &waitcv[i]);
		//lint +e747 +e712
		_io_in_progress[i] = true;

		if (_reply_buf[i] != NULL) {
			b.put_offline_reply(reply_data, _reply_buf[i]);
		} else {
			// If we did not get a reply buffer, then send
			// the data the normal way. We lose the optimization
			// but is better than panicing.
			b.put_off_line(reply_data, DEFAULT_ALIGN);
		}
	}
}

// The server is marshalling the data as a part of uio_srvr_pull()
// call. The add_reply_buffer() returns a reply buffer. It is
// expected that add_reply_buffer() will eventually succeed.

void
bulkio_impl::uio_write_obj_srvr::marshal(service &b)
{
	int i;

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("marshalling srver-pull request to client\n"));
	b.put_octet(bulkio_impl::REQUEST);
	b.put_pointer((void *)_orig_uio_clnt);
	b.put_pointer((void *)this);

	for (i = 0; i < _uiop->uio_iovcnt; i++) {
		if (_reply_buf[i] != NULL) {
			// in case of remarshall, release the previous reply_buf
			_reply_buf[i]->release();
			_reply_buf[i] = NULL;
		}
		_reply_buf[i] = b.add_reply_buffer(&_buf[i], DEFAULT_ALIGN);
		// If add_reply_buffer fails, the data will come by the
		// normal path and we will not have used the optimization
	}
}

void
bulkio_impl::in_uio_clnt::set_reference(void *thisptr)
{
	if (!CORBA::is_nil(_client_write_obj)) {
		//
		// set_reference() is called again on the same in_uio_clnt
		// object during a failover because a new server-pull
		// invocation is initiated by the new server. So we check for
		// this and clean up state in the old server-pull object.
		//
		bulkio_impl::uio_write_obj_clnt::get_impl_obj(
		    _client_write_obj)->real_destructor();
	}

	bulkio_impl::uio_write_obj_clnt *p =
	    (bulkio_impl::uio_write_obj_clnt *)thisptr;

	//
	// We take over the reference count held by 'p'.  '_client_write_obj'
	// is of type '_var', so its destructor will take care of dropping the
	// reference.
	//
	// We have to be really careful here since get_objref() in this case
	// doesn't increment the ref count unlike other classes.
	//


	//lint -e1061
	_client_write_obj =
	    bulkio_impl::uio_write_obj_clnt::_duplicate(p->get_objref());
	//lint +e1061
}

// The constructor called as a part of unmarshal() call on the
// client side.

bulkio_impl::uio_write_obj_clnt::uio_write_obj_clnt(service &b)
{
	bulkio_impl::in_uio_clnt *derived;

	// Save a pointer to in_uio_clnt
	_clnt_obj = derived = (bulkio_impl::in_uio_clnt *)b.get_pointer();

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("Client pointer returned from server is %x\n",
	    _clnt_obj));

	_orig_write_obj = b.get_pointer();

	_uiop = derived->get_uio();

	derived->set_reference(this);

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("And uio_iovcnt is %d\n", _uiop->uio_iovcnt));

	for (int i = 0; i < _uiop->uio_iovcnt; i++) {
		_reply_buf[i] = b.get_reply_buffer();
		_io_in_progress[i] = false;
		_clnt_buf[i] = derived->get_clnt_buf(i);
	}
}

bulkio_impl::uio_write_obj_clnt::~uio_write_obj_clnt()
{
	// We don't do anything in the
	// destructor since this function is really called
	// by in_uio_clnt::~in_uio_clnt()
}

void
bulkio_impl::uio_write_obj_clnt::real_destructor()
{
	int i;

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("uio_write_obj_clnt destructor uio_iovcnt %d\n",
	    _uiop->uio_iovcnt));

	for (i = 0; i < _uiop->uio_iovcnt; i++) {
		// Wait for any reply io in progress to complete
		if (_io_in_progress[i]) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_UIO,
			    PXFS_GREEN,
			    ("write_obj_clnt destructor wait io completion\n"));
			waitcv[i].wait();
		}
		// Release reply buffer
		if (_reply_buf[i] != NULL) {
			PXFS_DBPRINTF(
			    PXFS_TRACE_BULKIO_UIO,
			    PXFS_GREEN,
			    ("write_obj_clnt i %d repbuf %x\n",
			    i, _reply_buf[i]));

			_reply_buf[i]->release();
			_reply_buf[i] = NULL;
		}
	}

	// Make sure we panic if there's
	// an unexpected in_uio_clnt access at this point by setting
	// _clnt_obj to NULL

	_clnt_obj = NULL;
}	

bulkio_impl::uio_write_obj_srvr::uio_write_obj_srvr(uio_t *uiosrvr, void *orig)
{
	int i;

	_uiop = uiosrvr;
	_orig_uio_clnt = orig;
	for (i = 0; i < DEF_IOV_MAX; i++) {
		_reply_buf[i] = NULL;
	}

	ASSERT(_uiop->uio_segflg == UIO_SYSSPACE);
	struct iovec *iov = _uiop->uio_iov;
	for (i = 0; i < _uiop->uio_iovcnt; i++, iov++) {
		struct buf *bp = &_buf[i];
		bioinit(bp);

	//
	// B_KERNBUF is obsoleted by S9
	//
#ifdef S9_BP_CLEANUP
		bp->b_flags = B_PHYS;
#else
		bp->b_flags = B_KERNBUF | B_PHYS;
#endif
		bp->b_resid = bp->b_bcount = (size_t)iov->iov_len;
		bp->b_un.b_addr = iov->iov_base;
	}
}	//lint !e1741

bulkio_impl::uio_write_obj_srvr::~uio_write_obj_srvr()
{
	int i;

	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("uio_write_obj_srvr destructor\n"));

	for (i = 0; i < DEF_IOV_MAX; i++) {
		if (_reply_buf[i] != NULL) {
			_reply_buf[i]->release();
			_reply_buf[i] = NULL;
		}
	}
	for (i = 0; i < _uiop->uio_iovcnt; i++) {
		biodone(&_buf[i]);
	}
}

void
bulkio_impl::uio_write_obj::_unreferenced(unref_t)
{
	PXFS_DBPRINTF(
	    PXFS_TRACE_BULKIO_UIO,
	    PXFS_GREEN,
	    ("uio_write_obj received unref\n"));
	delete this;
}
