//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)bulkio_handler.cc	1.7	08/05/20 SMI"

#include "../version.h"
#include <pxfs/lib/pxfs_debug.h>
#include <pxfs/bulkio/bulkio_impl.h>

bulkio_handler_kit *bulkio_handler_kit::the_bulkio_handler_kit = NULL;

bulkio_handler::~bulkio_handler()
{
} //lint !e1540 _obj neither freed nor zero'ed by destructor

hkit_id_t
bulkio_handler::handler_id()
{
	return (bulkio_handler_kit::the().get_hid());
}

void *
bulkio_handler::narrow(CORBA::Object_ptr, CORBA::TypeId,
    generic_proxy::ProxyCreator)
{
	return (NULL);
}

void *
bulkio_handler::narrow_multiple_inherit(CORBA::Object_ptr, CORBA::TypeId,
    generic_proxy::ProxyCreator)
{
	return (NULL);
}

void
bulkio_handler::marshal(service& se, CORBA::Object_ptr obj)
{
	se.put_octet(_type);
	switch (_type) {
	case bulkio_handler::IN_UIO:
		((bulkio_impl::in_uio *)obj)->marshal(se);
		break;

	case bulkio_handler::INOUT_UIO:
		((bulkio_impl::inout_uio *)obj)->marshal(se);
		break;

	case bulkio_handler::IN_AIO:
		((bulkio_impl::in_aio *)obj)->marshal(se);
		break;

	// Stuff for server-pull begins here

	case bulkio_handler::INOUT_UIOWRITE:
		((bulkio_impl::uio_write_obj *)obj)->marshal(se);
		break;

	case bulkio_handler::INOUT_AIOWRITE:
		((bulkio_impl::aio_write_obj *)obj)->marshal(se);
		break;

	case bulkio_handler::INOUT_PAGESWRITE:
		((bulkio_impl::pages_write_obj *)obj)->marshal(se);
		break;

	// Stuff for server-pull ends here

	case bulkio_handler::IN_PAGES:
		((bulkio_impl::in_pages *)obj)->marshal(se);
		break;

	case bulkio_handler::INOUT_PAGES:
		((bulkio_impl::inout_pages *)obj)->marshal(se);
		break;

	case bulkio_handler::IN_AIO_PAGES:
		((bulkio_impl::in_aio_pages *)obj)->marshal(se);
		break;

	default:
		se.get_env()->system_exception(CORBA::INV_OBJREF(0,
			CORBA::COMPLETED_MAYBE));
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO,
		    PXFS_RED,
		    ("bulkio_impl::marshal - bad type\n"));
		break;
	}
}

CORBA::Object_ptr
bulkio_handler_kit::unmarshal(service &se, generic_proxy::ProxyCreator,
    CORBA::TypeId)
{
	bulkio_handler::bulkio_type type;

	type = (bulkio_handler::bulkio_type)se.get_octet();
	switch (type) {
	case bulkio_handler::IN_UIO:
		return (bulkio_impl::in_uio::unmarshal(se));

	case bulkio_handler::INOUT_UIO:
		return (bulkio_impl::inout_uio::unmarshal(se));

	case bulkio_handler::IN_AIO:
		return (bulkio_impl::in_aio::unmarshal(se));

	case bulkio_handler::INOUT_UIOWRITE:
		return (bulkio_impl::uio_write_obj::unmarshal(se));

	case bulkio_handler::INOUT_AIOWRITE:
		return (bulkio_impl::aio_write_obj::unmarshal(se));

	case bulkio_handler::INOUT_PAGESWRITE:
		return (bulkio_impl::pages_write_obj::unmarshal(se));

	case bulkio_handler::IN_PAGES:
		return (bulkio_impl::in_pages::unmarshal(se));

	case bulkio_handler::INOUT_PAGES:
		return (bulkio_impl::inout_pages::unmarshal(se));

	case bulkio_handler::IN_AIO_PAGES:
		return (bulkio_impl::in_aio_pages::unmarshal(se));

	default:
		se.get_env()->system_exception(CORBA::INV_OBJREF(0,
			CORBA::COMPLETED_MAYBE));
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO,
		    PXFS_RED,
		    ("bulkio_impl::unmarshal_cancel - bad type\n"));
		return (CORBA::Object::_nil());
	}
}

void
bulkio_handler_kit::unmarshal_cancel(service& se)
{
	bulkio_handler::bulkio_type type;

	type = (bulkio_handler::bulkio_type)se.get_octet();
	switch (type) {
	case bulkio_handler::IN_UIO:
		bulkio_impl::in_uio::unmarshal_cancel(se);
		break;

	case bulkio_handler::INOUT_UIO:
		bulkio_impl::inout_uio::unmarshal_cancel(se);
		break;

	case bulkio_handler::IN_AIO:
		bulkio_impl::in_aio::unmarshal_cancel(se);
		break;

	case bulkio_handler::INOUT_UIOWRITE:
		bulkio_impl::uio_write_obj::unmarshal_cancel(se);
		break;

	case bulkio_handler::INOUT_AIOWRITE:
		bulkio_impl::aio_write_obj::unmarshal_cancel(se);
		break;

	case bulkio_handler::INOUT_PAGESWRITE:
		bulkio_impl::pages_write_obj::unmarshal_cancel(se);
		break;

	case bulkio_handler::IN_PAGES:
		bulkio_impl::in_pages::unmarshal_cancel(se);
		break;

	case bulkio_handler::INOUT_PAGES:
		bulkio_impl::inout_pages::unmarshal_cancel(se);
		break;

	case bulkio_handler::IN_AIO_PAGES:
		bulkio_impl::in_aio_pages::unmarshal_cancel(se);
		break;

	default:
		PXFS_DBPRINTF(
		    PXFS_TRACE_BULKIO,
		    PXFS_RED,
		    ("bulkio_impl::unmarshal - bad type\n"));
		break;
	}
}

//
// Called by bulkio_impl::startup routine to dynamically instantiate
// bulkio_handler_kit.
//
int
bulkio_handler_kit::initialize()
{
	ASSERT(the_bulkio_handler_kit == NULL);
	the_bulkio_handler_kit = new bulkio_handler_kit();
	if (the_bulkio_handler_kit == NULL) {
		return (ENOMEM);
	}
	return (0);
}

void
bulkio_handler_kit::shutdown()
{
	if (the_bulkio_handler_kit != NULL) {
		delete the_bulkio_handler_kit;
		the_bulkio_handler_kit = NULL;
	}
}

//
// This method returns reference to bulkio_handler_kit instance.
//
bulkio_handler_kit&
bulkio_handler_kit::the()
{
	ASSERT(the_bulkio_handler_kit != NULL);
	return (*the_bulkio_handler_kit);
}

bulkio_handler_inuio::~bulkio_handler_inuio()
{
}

bulkio_handler_inoutuio::~bulkio_handler_inoutuio()
{
}

bulkio_handler_inaio::~bulkio_handler_inaio()
{
}

bulkio_handler_inoutuiowrite::~bulkio_handler_inoutuiowrite()
{
}

bulkio_handler_inoutaiowrite::~bulkio_handler_inoutaiowrite()
{
}

bulkio_handler_inoutpageswrite::~bulkio_handler_inoutpageswrite()
{
}

bulkio_handler_in_pages::~bulkio_handler_in_pages()
{
}

bulkio_handler_inout_pages::~bulkio_handler_inout_pages()
{
}

bulkio_handler_in_aio_pages::~bulkio_handler_in_aio_pages()
{
}
