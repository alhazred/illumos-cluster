//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scsi2_qd.cc	1.20	09/04/29 SMI"

#include <quorum/scsi2/scsi2_qd.h>
#include <quorum/common/quorum_generic.h>

//lint -e545

//
// get_scsi2_qd_impl is the access point for the type_registry to modlookup
// in the scsi2 module.
//
static quorum::quorum_device_type_ptr
get_scsi2_qd_impl(cmm_trace_level level)
{
	return ((new quorum_device_scsi2_impl(level))->get_objref());
}

//
// Constructor
//
quorum_device_scsi2_impl::quorum_device_scsi2_impl(
    cmm_trace_level level)
{
	vnode_ptr = NULL;
	retry = false;
	sblkno = INV_BLKNO;
	pathname = NULL;
	failfast_enabled = false;
	trace_level = level;
}

//
// Default constructor.
// Should not be called.
//
// Lint warns that some class members are not initialized by constructor.
//lint -e1401
quorum_device_scsi2_impl::quorum_device_scsi2_impl()
{
	ASSERT(0);
}
//lint +e1401

//
// Destructor
//
quorum_device_scsi2_impl::~quorum_device_scsi2_impl()
{
	CL_PANIC(vnode_ptr == NULL);
	delete [] pathname;
	pathname = NULL;
}

//
// Unreferenced
//
void
#ifdef DEBUG
quorum_device_scsi2_impl::_unreferenced(unref_t cookie)
#else
quorum_device_scsi2_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI2(%s)::_unreferenced.\n", pathname));

	//
	// Quorum objects are instantiated in the kernel
	// while the callers instantiating those objects can reside
	// in kernel or userland. If a userland client dies
	// after calling quorum_open() but before calling quorum_close(),
	// then the sole reference held by the client for the quorum
	// object is gone without a call to quorum_close().
	//
	// So we call quorum_close() here to clean-up state.
	// quorum_close() is implemented to be idempotent
	// and hence it is safe to call quorum_close() here.
	//
	// This object is going away; so ignore return value
	// and exception from quorum_close().
	//
	Environment e;
	(void) quorum_close(e);
	e.clear();

	delete this;
}

//
// Disable SCSI failfast
//
void
quorum_device_scsi2_impl::disable_failfast()
{
	int error = 0;

	if (!failfast_enabled) {
		// Failfast already diabled.
		return;
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI2(%s): disable_failfast\n", pathname));

	ASSERT(vnode_ptr != NULL);

	// Passing zero as the poll period disables failfast
	error = quorum_scsi_enable_failfast(vnode_ptr, 0);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI2(%s): disable_failfast failed with error %d.\n",
		    pathname, error));
		return;
	}

	failfast_enabled = false;
}

//
// set_message_tracing_level
//
void
quorum_device_scsi2_impl::set_message_tracing_level(
    uint32_t new_trace_level, Environment &e)
{
	quorum_device_generic_impl::set_message_tracing_level(
	    new_trace_level, e);
}

//
// get_message_tracing_level
//
uint32_t
quorum_device_scsi2_impl::get_message_tracing_level(Environment &e)
{
	return (quorum_device_generic_impl::get_message_tracing_level(e));
}

//
// quorum_open
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_open(const char *devname, const bool scrub,
    const quorum::qd_property_seq_t &qd_property, Environment &e)
{
	return (quorum_device_generic_impl::quorum_open(devname, scrub,
	    qd_property, e));
}

//
// quorum_close
//
// Disable failfast, if enabled.
// Invoke the quorum_close() method of parent class quorum_device_generic_impl
// which will undo any action done by quorum_open().
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_close(Environment &e)
{
	if (failfast_enabled) {
		disable_failfast();
	}
	return (quorum_device_generic_impl::quorum_close(e));
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster. The quorum_amnesia_level enum
// is defined in quorum_impl.h
//
uint32_t
quorum_device_scsi2_impl::quorum_supports_amnesia_prot(Environment &e)
{
	return (quorum_device_generic_impl::quorum_supports_amnesia_prot(e));
}

//
// quorum_reserve
//
// Place this node's reservation key on the device.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key,
    Environment				&e)
{
	return (quorum_device_generic_impl::quorum_reserve(my_key, e));
}

//
// quorum_read_reservations
//
// Read all of the reservations on the device. Currently this is
// hard coded to only return a single reservation.
//

quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_read_reservations(
    quorum::reservation_list_t		&resv_list,
    Environment			&e)
{
	return (
	    quorum_device_generic_impl::quorum_read_reservations(resv_list, e));
}

//
// quorum_preempt
//
// Remove the victim key (both registration and reservation) from the device
// and place a reservation using my_key. If victim_key is NULL, that means
// that we need to release any mutual exclusion that we hold on the device.
// In that case, call release.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&e)
{
	return (
	    quorum_device_generic_impl::quorum_preempt(my_key, victim_key, e));
}

//
// quorum_register
//
// Write this node's key on the device as a registration.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&e)
{
	return (quorum_device_generic_impl::quorum_register(my_key, e));
}

//
// quorum_read_keys
//
// Read the registration keys on the device. Fill in the reg_list up to a
// maximum of reg_list->listsize keys. Set reg_list->listlen to the number
// of total keys that are on the device.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_read_keys(
    quorum::reservation_list_t		&reg_list,
    Environment			&e)
{
	return (quorum_device_generic_impl::quorum_read_keys(reg_list, e));
}

//
// quorum_remove
//
// Remove the device.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_remove(const char *devname,
    const quorum::qd_property_seq_t &properties,
    Environment			&e)
{
	return (quorum_device_generic_impl::quorum_remove(
	    devname, properties, e));
}

//
// quorum_enable_failfast
//
// Enable sd failfast on the device.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_enable_failfast(
    Environment			&)
{
	int error = 0;

	if (failfast_enabled) {
		// Failfast already enabled. Simply return success.
		return (quorum::QD_SUCCESS);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI2(%s): quorum_enable_failfast\n", pathname));

	ASSERT(vnode_ptr != NULL);

	error = quorum_scsi_enable_failfast(vnode_ptr, SCSI2_POLL_PERIOD);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI2(%s): quorum_enable_failfast failed with "
		    "error %d.\n", pathname, error));
		//
		// Any error in enable failfast should be treated as a
		// reservation conflict.
		//
		return (quorum::QD_EACCES);
	}

	failfast_enabled = true;

	return (quorum::QD_SUCCESS);
}

//
// quorum_reset
//
// Reset the device.
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_reset(
    Environment			&)
{
	int	error;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI2(%s): quorum_reset_bus called.\n", pathname));

	error = quorum_scsi_reset_bus(vnode_ptr);
	if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI2(%s): quorum_reset_bus: quorum_scsi_reset_bus "
		    "retd error %d.\n", pathname, error));
		return (quorum::QD_EIO);
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_data_read
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_data_read(
	uint64_t		location,
	CORBA::String_out	data,
	Environment		&)
{
	int error;
	error = quorum_scsi_data_read(vnode_ptr, sblkno, location, *(&data),
	    (uint64_t)0, false);

	if (error == EACCES) {
		return (quorum::QD_EACCES);
	} else if (error == 0) {
		return (quorum::QD_SUCCESS);
	} else {
		return (quorum::QD_EIO);
	}
}

//
// quorum_data_write
//
quorum::quorum_error_t
quorum_device_scsi2_impl::quorum_data_write(
	uint64_t		location,
	const char		*data,
	Environment		&)
{
	int error;
	error = quorum_scsi_data_write(vnode_ptr, sblkno, location, data,
	    (uint64_t)0, false);

	if (error == EACCES) {
		return (quorum::QD_EACCES);
	} else if (error == 0) {
		return (quorum::QD_SUCCESS);
	} else {
		return (quorum::QD_EIO);
	}
}

//
// reservation_tkown
//
// Forcefully acquire exclusive access rights to a SCSI2
// quorum device. Used in conjunction with PGRE.
//
//
int
quorum_device_scsi2_impl::reservation_tkown()
{
	int		error = 0;
	int		retval = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCTKOWN,
	    (intptr_t)NULL, &retval);

	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI2(%s): reservation_tkown: ioctl MHIOCTKOWN "
		    "failed with error (%d).\n", pathname, error));
	}

	return (error);
}

//
// reservation_release
//
// Relinquish exclusive rights to the SCSI2 disk.
//
//
int
quorum_device_scsi2_impl::reservation_release()
{
	int		error = 0;
	int		retval = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCRELEASE,
	    (intptr_t)NULL, &retval);

	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI2(%s): reservation_release: ioctl MHIOCRELEASE "
		    "failed with error %d.\n", pathname, error));
	}

	return (error);
}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
int
quorum_device_scsi2_impl::check_fi(char	*qop_name)
{
	return (quorum_device_generic_impl::check_fi(qop_name));
}
#endif // FAULT_CMM && _KERNEL_ORB

//
// Functions to support module load and linkage.
//
static struct modlmisc modlmisc = {
	&mod_miscops, "SCSI2 device type module",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL },
};

char _depends_on[] =
	"misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

int
_init()
{
	int error = 0;

	if ((error = mod_install(&modlinkage)) != 0) {
		CMM_TRACE(("ERROR: could not mod_install\n"));
		return (error);
	}

	CMM_TRACE(("SCSI2: module loading.\n"));

	_cplpl_init();		/* C++ initialization */

	device_type_registry_impl::register_device_type(QD_SCSI2,
	    get_scsi2_qd_impl);

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

//lint +e545
