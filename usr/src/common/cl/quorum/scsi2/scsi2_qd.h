/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSI2_QD_H
#define	_SCSI2_QD_H

#pragma ident	"@(#)scsi2_qd.h	1.13	08/10/01 SMI"

/* Interfaces to quorum device implementation of SCSI-2 devices. */

#include <quorum/common/quorum_generic.h>
#include <cmm/cmm_debug.h>

// Poll period for failfast thread
#define	SCSI2_POLL_PERIOD	2000

class quorum_device_scsi2_impl :
	public McServerof <quorum::quorum_device_type>,
	quorum_device_generic_impl {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//

	virtual quorum::quorum_error_t		quorum_open(
	    const char				*devname,
	    const bool				scrub,
	    const quorum::qd_property_seq_t	&,
	    Environment				&);

	virtual uint32_t quorum_supports_amnesia_prot(Environment &);

	virtual quorum::quorum_error_t quorum_close(Environment &);

	virtual quorum::quorum_error_t		quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_reservations(
	    quorum::reservation_list_t		&resv_list,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&victim_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_keys(
	    quorum::reservation_list_t		&reg_list,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_read(
		uint64_t		location,
		CORBA::String_out	data,
		Environment		&);

	virtual quorum::quorum_error_t	quorum_data_write(
		uint64_t		location,
		const char		*data,
		Environment		&);

	virtual quorum::quorum_error_t 		quorum_remove(
	    const char				*devname,
	    const quorum::qd_property_seq_t	&qd_props,
	    Environment				&);

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

	quorum_device_scsi2_impl(cmm_trace_level);
	quorum_device_scsi2_impl();
	~quorum_device_scsi2_impl();

private:
	int	reservation_release();
	int	reservation_tkown();

	// Disable SCSI failfast
	void	disable_failfast();

	// Function for fault injection testing.
	int	check_fi(char*);

	// True if failfast has been enabled
	bool	failfast_enabled;
};

#endif	/* _SCSI2_QD_H */
