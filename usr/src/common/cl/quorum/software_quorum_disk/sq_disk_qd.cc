//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sq_disk_qd.cc	1.8	09/04/29 SMI"

#include <quorum/software_quorum_disk/sq_disk_qd.h>
#include <quorum/common/quorum_generic.h>
#include <sys/clconf_int.h>

//
// This is the polling period used by the software fencing thread
// for each software quorum shared disk. The fencing thread for a sq disk
// checks, once every polling period, to see if the disk still has
// the current node's registration key on it. If not, the fencing thread
// panics the current node.
//
// This polling period is tunable. It can be set in the /etc/system file.
// This is for use by Sun Personnel only.
//
extern unsigned int software_fencing_polling_period;

//
// get_sq_disk_qd_impl is the access point for the type_registry to modlookup
// in the sq_disk module.
//
static quorum::quorum_device_type_ptr
get_sq_disk_qd_impl(cmm_trace_level level)
{
	CMM_SELECTIVE_TRACE(level, CMM_GREEN,
	    ("clq_sq_disk: Returning new quorum device type.\n"));
	return ((new quorum_device_sq_disk_impl(level))->get_objref());
}

//
// Constructor
//
quorum_device_sq_disk_impl::quorum_device_sq_disk_impl(
    cmm_trace_level level) :
    software_fencing(level)
{
	vnode_ptr = NULL;
	retry = false;
	sblkno = INV_BLKNO;
	pathname = NULL;
	failfast_enabled = false;
	callback_registration_list.listsize = NODEID_MAX;
	callback_registration_list.keylist.length(NODEID_MAX);
	trace_level = level;
	access_status = true;
	err_msg_counter = 0;

	for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		local_key.key[i] = 0;
	}
	callback_error = quorum::QD_SUCCESS;

	char nodename[8];
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	//
	// Lint throws informational message that memory is allocated
	// and assigned to a data member, but there is no
	// assignment or copy constructor defined for the class;
	// and that this could lead to problems as default assignment
	// and copy constructors cannot handle such cases correctly.
	// We ignore this message as we do not use assignment or copy
	// for this class.
	//lint -e1732 -e1733
	syslog_msgp = new os::sc_syslog_msg(
	    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
	//lint +e1732 +e1733

	// If null, system is low on memory
	CL_PANIC(syslog_msgp != NULL);
}

//
// Default constructor.
// Should not be used.
//
// Lint warns that some class members are not initialized by constructor.
//lint -e1401
quorum_device_sq_disk_impl::quorum_device_sq_disk_impl()
{
	ASSERT(0);
}
//lint +e1401

//
// Destructor
//
quorum_device_sq_disk_impl::~quorum_device_sq_disk_impl()
{
	CL_PANIC(vnode_ptr == NULL);

	delete [] pathname;
	pathname = NULL;
	delete syslog_msgp;
	syslog_msgp = NULL;
}

//
// Unreferenced
//
void
#ifdef DEBUG
quorum_device_sq_disk_impl::_unreferenced(unref_t cookie)
#else
quorum_device_sq_disk_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SQ_DISK(%s)::_unreferenced.\n", pathname));

	//
	// Quorum objects are instantiated in the kernel
	// while the callers instantiating those objects can reside
	// in kernel or userland. If a userland client dies
	// after calling quorum_open() but before calling quorum_close(),
	// then the sole reference held by the client for the quorum
	// object is gone without a call to quorum_close().
	//
	// So we call quorum_close() here to clean-up state.
	// quorum_close() is implemented to be idempotent
	// and hence it is safe to call quorum_close() here.
	//
	// This object is going away; so ignore return value
	// and exception from quorum_close().
	//
	Environment e;
	(void) quorum_close(e);
	e.clear();

	shutdown_failfast();
	delete this;
}

char *
quorum_device_sq_disk_impl::get_pathname()
{
	return (pathname);
}

//
// set_message_tracing_level
//
void
quorum_device_sq_disk_impl::set_message_tracing_level(
    uint32_t new_trace_level, Environment &e)
{
	quorum_device_generic_impl::set_message_tracing_level(
	    new_trace_level, e);
}

//
// get_message_tracing_level
//
uint32_t
quorum_device_sq_disk_impl::get_message_tracing_level(Environment &e)
{
	return (quorum_device_generic_impl::get_message_tracing_level(e));
}

//
// quorum_open
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_open(const char *devname, const bool scrub,
    const quorum::qd_property_seq_t &qd_property, Environment &e)
{
	return (quorum_device_generic_impl::quorum_open(devname, scrub,
	    qd_property, e));
}

//
// quorum_close
//
// Disable failfast, if enabled.
// Invoke the quorum_close() method of parent class quorum_device_generic_impl
// which will undo any action done by quorum_open().
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_close(Environment &e)
{
	if ((vnode_ptr != NULL) && (failfast_enabled)) {
		disable_failfast();
		failfast_enabled = false;
	}
	return (quorum_device_generic_impl::quorum_close(e));
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster. The quorum_amnesia_level enum
// is defined in quorum_impl.h
//
uint32_t
quorum_device_sq_disk_impl::quorum_supports_amnesia_prot(Environment &e)
{
	return (quorum_device_generic_impl::quorum_supports_amnesia_prot(e));
}

//
// quorum_reserve
//
// Place this node's reservation key on the device.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key,
    Environment				&e)
{
	return (quorum_device_generic_impl::quorum_reserve(my_key, e));
}

//
// quorum_read_reservations
//
// Read all of the reservations on the device. Currently this is
// hard coded to only return a single reservation.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_read_reservations(
    quorum::reservation_list_t		&resv_list,
    Environment			&e)
{
	return (quorum_device_generic_impl::quorum_read_reservations(
	    resv_list, e));
}

//
// quorum_preempt
//
// Remove the victim key (both registration and reservation) from the device
// and place a reservation using my_key. If victim_key is NULL, that means
// that we need to release any mutual exclusion that we hold on the device.
// In that case, call release.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&e)
{
	return (quorum_device_generic_impl::quorum_preempt(
	    my_key, victim_key, e));
}

//
// quorum_register
//
// Write this node's key on the device as a registration.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&e)
{
	return (quorum_device_generic_impl::quorum_register(my_key, e));
}

//
// quorum_read_keys
//
// Read the registration keys on the device. Fill in the reg_list up to a
// maximum of reg_list->listsize keys. Set reg_list->listlen to the number
// of total keys that are on the device.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_read_keys(
    quorum::reservation_list_t		&reg_list,
    Environment			&e)
{
	return (quorum_device_generic_impl::quorum_read_keys(reg_list, e));
}

//
// quorum_read_keys_internal
//
// Read the registration keys on the device. Fill in the reg_list up to a
// maximum of reg_list->listsize keys. Set reg_list->listlen to the number
// of total keys that are on the device.
// The caller can specify whether or not to trace message on success.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_read_keys_internal(
    quorum::reservation_list_t		&reg_list,
    cmm_trace_level			trace_level_to_use,
    Environment			&e)
{
	return (quorum_device_generic_impl::quorum_read_keys_internal(
	    reg_list, trace_level_to_use, e));
}

//
// quorum_remove
//
// Remove the device.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_remove(const char *devname,
    const quorum::qd_property_seq_t &properties,
    Environment			&e)
{
	return (quorum_device_generic_impl::quorum_remove(
	    devname, properties, e));
}

//
// quorum_enable_failfast
//
// Enable software fencing failfast for the device.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_enable_failfast(
    Environment			&)
{
	uint64_t keyval = 0;

	if (failfast_enabled) {
		// Failfast already enabled. Simply return success
		return (quorum::QD_SUCCESS);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SQ_DISK(%s): quorum_enable_failfast\n", pathname));

	if (clconf_get_local_key(&keyval) != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("quorum_device_sq_disk_impl: "
		    "Failed to get local node key."));
		return (quorum::QD_EIO);
	}

	for (int i = MHIOC_RESV_KEY_SIZE-1; i >= 0; i--) {
		local_key.key[i] = (char)(keyval & 0xff);
		keyval >>= 8;
	}

	if (initialize_failfast()) {
		return (quorum::QD_EIO);
	}

	enable_failfast();

	failfast_enabled = true;

	return (quorum::QD_SUCCESS);
}

//
// quorum_reset
//
// Reset the device.
// The reset operation for a software quorum disk does nothing intentionally
// as reset operation is not relevant to software quorum.
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_reset(
    Environment			&)
{
	return (quorum::QD_SUCCESS);
}

//
// quorum_data_read
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_data_read(
	uint64_t		location,
	CORBA::String_out	data,
	Environment		&)
{
	int error;
	error = quorum_scsi_data_read(vnode_ptr, sblkno, location, *(&data),
	    (uint64_t)0, false);

	if (error == EACCES) {
		return (quorum::QD_EACCES);
	} else if (error == 0) {
		return (quorum::QD_SUCCESS);
	} else {
		return (quorum::QD_EIO);
	}
}

//
// quorum_data_write
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::quorum_data_write(
	uint64_t		location,
	const char		*data,
	Environment		&)
{
	int error;
	error = quorum_scsi_data_write(vnode_ptr, sblkno, location, data,
	    (uint64_t)0, false);

	if (error == EACCES) {
		return (quorum::QD_EACCES);
	} else if (error == 0) {
		return (quorum::QD_SUCCESS);
	} else {
		return (quorum::QD_EIO);
	}
}

//
// reservation_tkown
//
// For software quorum disks, the fencing is done by
// the software fencing polling thread for the software quorum disk.
// The software quorum disk polls the device every polling period.
// So the take ownership operation here should just sleep for
// twice the polling period, so that the polling thread is sure
// to have run at least once in this period.
//
int
quorum_device_sq_disk_impl::reservation_tkown()
{
	// Sleep for twice the software fencing thread's polling period
	os::usecsleep(
	    2 * ((os::usec_t)software_fencing_polling_period) * MICROSEC);
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SQ_DISK(%s): reservation_tkown: sleep complete\n", pathname));

	return (0);
}

//
// reservation_release
//
// This operation is not applicable for software quorum.
//
int
quorum_device_sq_disk_impl::reservation_release()
{
	return (0);
}

//
// Software fencing thread calls this method to check if
// the software quorum device is still accessible
// (i.e., whether this node's keys are still present on the device.)
//
quorum::quorum_error_t
quorum_device_sq_disk_impl::callback_failfast()
{
	// Try to read registration keys
	Environment env;
	callback_registration_list.listlen = 0;

	//
	// This method is executed every polling period, and quorum_read_keys
	// would print CMM trace buffer messages every time, even on success.
	// To avoid that, we use quorum_read_keys_internal() and
	// pass the message tracing level as CMM_AMBER, so that
	// quorum_read_keys_internal() does not trace message on success.
	//
	callback_error = quorum_read_keys_internal(
	    callback_registration_list, CMM_AMBER, env);
	if (env.exception() != NULL) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED, ("callback_failfast: "
		    "quorum_read_keys_internal returned an error\n"));
		env.exception()->print_exception(
		    (char *)"quorum_read_keys_internal:");
	}
	ASSERT(env.exception() == NULL);

	//
	// Process reservation status.
	//
	if (callback_error == quorum::QD_SUCCESS) {
		//
		// If this is a success after failures to read keys,
		// then log a message.
		//
		if (!access_status) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
			    ("callback_failfast: quorum_read_keys_internal for "
			    "quorum disk %s succeeded after some failures.\n",
			    pathname));
			//
			// SCMSGS
			// @explanation
			// The quorum subsystem was able to read keys from
			// a quorum device after several failures.
			// This type of erroneous behavior might occur
			// because of unexpected transient errors
			// from the device driver or disk.
			// @user_action
			// Check for firmware or driver updates.
			//
			(void) syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "clq_sq_disk: callback_failfast: succeeded in "
			    "reading keys on quorum device %s "
			    "after some failures.", pathname);
		}

		//
		// Set access status to true signifying success,
		// and reset error msg counter.
		//
		access_status = true;
		err_msg_counter = 0;

		// Look for local node key.
		bool found_my_key = false;
		for (uint_t i = 0;
		    i < callback_registration_list.listlen; i++) {
			quorum::registration_key_t *current_key;
			current_key = (quorum::registration_key_t *)
				(&callback_registration_list.keylist[i]);
			if (match_idl_keys(current_key,
			    (quorum::registration_key_t *)(&local_key))) {
				found_my_key = true;
				break;
			}
		}
		if (!found_my_key) {
			char nodename[8];
			os::sprintf(nodename, "%d", orb_conf::local_nodeid());
			os::sc_syslog_msg syslog_msg(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// Unable to locate this node's registration key
			// on the quorum device. This node might have been
			// preempted by another node. Because this node was
			// expelled from the cluster, the node now halts.
			// @user_action
			// Correct the problem that caused the node
			// to be expelled from the cluster.
			// The most common cause is a network communication
			// failure between this node and other members of
			// the cluster. After completing repairs, reboot the
			// node back into the cluster.
			//
			(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
			    "clq_sq_disk: Preempted from quorum device %s.",
			    pathname);

			return (quorum::QD_EACCES);	// Not reached
		} else {
			return (quorum::QD_SUCCESS);
		}
	}

	//
	// If we were not able to read the registration keys from
	// the quorum device, then we treat all such error cases as EIO.
	//

	//
	// We log error msgs only the first time failure happens after
	// a success, and once every hour if failures continue happening.
	//
	err_msg_counter++;
	unsigned int max_counter_val_per_hour =
	    3600/software_fencing_polling_period;

	if (err_msg_counter >= max_counter_val_per_hour) {
		//
		// Reset counter to zero, and return without printing
		// error messages. If an error happens again the immediate
		// next time around, then the error msgs would be printed.
		//
		err_msg_counter = 0;
		access_status = false;
		return (quorum::QD_EIO);
	}

	if (err_msg_counter != 1) {
		//
		// This is not the first failure after a success,
		// neither is it the first time in an hour in a series of
		// failures happening. So we do not print error msgs.
		//
		access_status = false;
		return (quorum::QD_EIO);
	}

	//
	// First time failure after success, or first error in an hour of
	// continuous failures. Log error messages.
	//
	access_status = false;
	CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
	    ("callback_failfast: quorum_read_keys_internal failed "
	    "with error %d.\n", callback_error));
	//
	// SCMSGS
	// @explanation
	// The quorum subsystem was not able to read the keys from
	// the quorum device. Therefore, the quorum subsystem could not
	// determine if this node is part of the operational cluster.
	// This read operation was expected to succeed.
	// Errors similar to these might occur because of
	// unexpected behavior from the device driver or disk.
	// @user_action
	// Check for disk failure or contact the device manufacturer
	// for firmware updates.
	//
	(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "clq_sq_disk: callback_failfast error: quorum error code %d "
	    "obtained while trying to read keys on quorum device %s.",
	    callback_error, pathname);
	return (quorum::QD_EIO);
}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
int
quorum_device_sq_disk_impl::check_fi(char *qop_name)
{
	return (quorum_device_generic_impl::check_fi(qop_name));
}
#endif // FAULT_CMM && _KERNEL_ORB

//
// Functions to support module load and linkage.
//
static struct modlmisc modlmisc = {
	&mod_miscops, "SQ_DISK device type module",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL },
};

char _depends_on[] =
	"misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

int
_init()
{
	int error;

	if ((error = mod_install(&modlinkage)) != 0) {
		CMM_TRACE(("ERROR: could not mod_install\n"));
		return (error);
	}

	CMM_TRACE(("SQ_DISK: module loading.\n"));

	_cplpl_init();		/* C++ initialization */

	device_type_registry_impl::register_device_type(
	    QD_SQ_DISK, get_sq_disk_qd_impl);

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
