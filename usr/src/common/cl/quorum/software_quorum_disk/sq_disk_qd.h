/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SQ_DISK_QD_H
#define	_SQ_DISK_QD_H

#pragma ident	"@(#)sq_disk_qd.h	1.4	08/10/01 SMI"

/*
 * Interfaces to quorum device implementation of Software Quorum for
 * shared disks.
 */

#include <quorum/common/quorum_generic.h>
#include <quorum/common/software_fencing.h>
#include <cmm/cmm_debug.h>

class quorum_device_sq_disk_impl :
	public McServerof <quorum::quorum_device_type>,
	software_fencing,
	quorum_device_generic_impl {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//
	virtual quorum::quorum_error_t		quorum_open(
	    const char				*devname,
	    const bool				scrub,
	    const quorum::qd_property_seq_t	&,
	    Environment				&);

	virtual quorum::quorum_error_t quorum_close(Environment &);

	virtual uint32_t quorum_supports_amnesia_prot(Environment&);

	virtual quorum::quorum_error_t		quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_reservations(
	    quorum::reservation_list_t		&resv_list,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&victim_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_keys(
	    quorum::reservation_list_t		&reg_list,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_read(
		uint64_t		location,
		CORBA::String_out	data,
		Environment		&);

	virtual quorum::quorum_error_t	quorum_data_write(
		uint64_t		location,
		const char		*data,
		Environment		&);

	virtual quorum::quorum_error_t		quorum_remove(
	    const char				*devname,
	    const quorum::qd_property_seq_t	&qd_props,
	    Environment				&);

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

	quorum_device_sq_disk_impl(cmm_trace_level);
	quorum_device_sq_disk_impl();
	~quorum_device_sq_disk_impl();

protected:
	quorum::quorum_error_t quorum_read_keys_internal(
	    quorum::reservation_list_t &reg_list,
	    cmm_trace_level trace_level_to_use, Environment &);

private:
	int reservation_release();
	int reservation_tkown();

	quorum::quorum_error_t callback_failfast();

	char *get_pathname();

	quorum::registration_key_t local_key;

	quorum::reservation_list_t callback_registration_list;
	quorum::quorum_error_t callback_error;

	// Function for fault injection testing.
	int	check_fi(char*);

	// True if failfast has been enabled
	bool	failfast_enabled;

	// Set to true if we could read keys from device, else set to false
	bool access_status;

	//
	// Counter is incremented whenever the software fencing thread
	// for this software quorum disk fails to read the registration keys
	// from the disk.
	// Error messages regarding failures to read keys are printed
	// only once per hour, to avoid CMM trace buffer overflow
	// and syslog overflow. Once the counter reaches its maximum value
	// in an hour, the counter is reset so that another immediate failure
	// will log error messages, and thenafter one more possible hour of
	// errors and counter increments can happen without logging error msgs.
	// Once the keys are read successfully, the counter is reset.
	//
	unsigned int err_msg_counter;

	os::sc_syslog_msg *syslog_msgp;
};

#endif	/* _SQ_DISK_QD_H */
