/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _QUORUM_IMPL_H
#define	_QUORUM_IMPL_H

#pragma ident	"@(#)quorum_impl.h	1.46	09/04/29 SMI"

#include <orb/object/adapter.h>
#include <h/quorum.h>
#include <sys/os.h>
#include <sys/quorum_int.h>
#include <sys/nodeset.h>
#include <cmm/cmm_config.h>

//
// Implementation Class for quorum_algorithm (defined in quorum.idl)
//

//
// Note that the vm_comm calls into this code while holding its own lock,
// putting the ordering of the vm_comm lock before this one.  If the quorum
// algorithm needs to be versioned, care will have to be taken to avoid
// deadlock.  The issues are entirely local to a given node, not distributed.
//

class quorum_algorithm_impl : public McServerof < quorum::quorum_algorithm > {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//
	void update_membership(
		quorum::membership_bitmask_t,
		Environment&);

	void acquire_quorum_devices(
		quorum::membership_bitmask_t,
		quorum::quorum_result_t&,
		Environment&);

	bool check_cluster_quorum(
		const quorum::cluster_quorum_results_t&,
		Environment&);

	bool check_ccr_quorum(
		const quorum::ccr_id_list_t&,
		Environment&);

	void ccr_refreshed(Environment&);

	void cmm_reconfig_done(Environment&);

	bool partition_can_have_quorum(
		membership_bitmask_t,
		Environment&);

	uint32_t minimum_cluster_quorum(Environment&);

	uint32_t current_cluster_votes(Environment&);

	uint32_t current_node_votes(nodeid_t, Environment&);

	uint32_t max_votes_of_partition(
		membership_bitmask_t,
		Environment&);

	void quorum_get_status(quorum::quorum_status_t_out, Environment&);
	void quorum_get_immediate_status(
	    quorum::quorum_status_t_out, Environment&);
	void quorum_release_status(quorum::quorum_status_t_out, Environment&);

	quorum::quorum_config_error_t quorum_check_transition(
		const quorum::node_config_list_t&,
		const quorum::quorum_device_config_list_t&,
		Environment&);

	void quorum_table_updated(Environment &);

	void quorum_device_scrub(const char *, Environment &);

	//
	// The following routine is called from ORB::initialize
	// to initialize the quorum algorithm implementation.
	//
	static int quorum_algorithm_init();

	//
	// Returns the reservation key for the local node
	//
	static mhioc_resv_key_t	get_my_reservation_key();

private:

	//
	// Synchronization variables to disallow changing the quorum
	// configuration while in use.
	//
	os::mutex_t 		_quorum_config_mutex;
	os::condvar_t 		_quorum_config_cv;
	long			_quorum_config_use_count;

	//
	// Synchronization variables to prevent quorum_check_transition
	// during times when the quorum device owners have not yet been
	// set. Added to fix bugid 4399064 which hit a race condition.
	// The bool is set to true when the quorum_algorithm has info
	// about the owners of the quorum devices. It is false whenever
	// this information is set to NODEID_UNKNOWN during reconfiguration.
	// This information is protected by the _quorum_config_mutex and
	// threads can wait by calling config_read_wait and giving the
	// _qd_owners_cv.
	//

	os::condvar_t		_qd_owners_cv;
	bool			_are_qd_owners_set;

	//
	// Mutex to serialize access to quorum disks. Since disk ioctls
	// are blocking calls, code paths that can be invoked from
	// interrupt and _unreferenced threads should not attempt to
	// acquire this mutex.
	//
	os::mutex_t 		_ioctl_mutex;

	nodeid_t		_my_nodeid;
	nodeid_t		_max_nodeid;
	uint_t			_max_qid;
	uint_t			_total_configured_votes;
	uint_t			_current_cluster_votes;
	bool			_cluster_has_quorum;
	bool			_ccr_is_refreshed;
	bool			_we_have_been_preempted;
	bool			_quorum_table_updated;
	quorum::membership_bitmask_t	_last_known_cluster_members;
	// Bitmask of devices owned by current cluster members.
	quorum::membership_bitmask_t	_cluster_owned_devices;
	quorum_table_t		*_quorum_table;

	quorum::device_type_registry_var	_type_registry_v;

	//
	// sc_syslog_msg used for generating syslog messages
	// when quorum info changes for nodes or quorum devices.
	//
	os::sc_syslog_msg	*_quorum_syslog_msgp;

	//
	// The lists in the following two structs are allocated when the
	// CCR quorum table is read. This eliminates the need to dynamically
	// allocate memory during reconfigurations.
	//
	quorum::reservation_list_t	idl_reservation_list;
	quorum::reservation_list_t	idl_registration_list;

	// The one and only key for this node.
	quorum::registration_key_t	*my_key;

	typedef struct quorum_info_struct {
		char				*pathname;
		nodeid_t			owner;
		quorum::quorum_device_type_var	qd_v;

		// False if access to the quorum device failed
		bool				accessible;
	} quorum_info_t;

	//
	// The following are pointers to arrays. Rather than pre-allocating
	// maximal-sized arrays, we allocate the arrays after finding the
	// actual number of elements (nodes or quorum devices) in the
	// cluster.
	//
	quorum_info_t		*_quorum_devices; // one for each device

	//
	// Although a membership_bitmask_t is usually used to provide a
	// bitmask for nodes, in this case, it is providing a bitmask for
	// quorum devices. Each bitmask keeps track of the quorum devices
	// which a particular node owns (has a reservation key on).
	//
	membership_bitmask_t	*_quorum_devices_owned; // one for each node

	//
	// Using a membership_bitmask_t to keep track of quorum devices
	// rather than nodes. Only one of these needed to keep track of
	// quorum devices on which this node has placed a registration key.
	//
	membership_bitmask_t	_quorum_devices_registered;

	//
	// Prevent creation of instances except through the static function
	// quorum_algorithm_init(). Also, prevent assignment, copy and
	// pass by value.
	//
	quorum_algorithm_impl(quorum_table_t &);
	quorum_algorithm_impl(const quorum_algorithm_impl &);
	~quorum_algorithm_impl();
	quorum_algorithm_impl & operator = (quorum_algorithm_impl &);

	//
	// quorum configuration lock and unlock routines
	//
	// The first two routines are used by readers of the
	// quorum config information - to ensure that this
	// information is not modified while there exist readers
	// of this infomation.
	// The next two routines are used by writers of this
	// information. A writer may call the lock() routine
	// multiple times (ex. if ccr_refreshed() is called
	// from initialize_quorum_info()), so the lock() routine
	// first checks to see if the caller has the lock already
	// held, and only tries to acquire the lock if not.
	// It returns the output of this check as its return
	// value, which is passed in the unlock() routine and
	// is used to decide if the unlock should be done or not.
	//
	// NOTE: A reader must never attempt to convert into
	// a writer, else it will block forever waiting for
	// the number of readers to become zero.
	//
	void			config_read_lock();
	void			config_read_unlock();
	void			config_read_wait(os::condvar_t*);
	bool			config_write_lock_if_not_locked();
	void			config_write_unlock(bool);

	//
	// functions to initialize and deallocate quorum info
	//
	void initialize_quorum_info(quorum_table_t&);
	void initialize_node_quorum_info(quorum_table_t&);
	void quorum_device_added(
	    const quorum_device_info_t *, bool&, quorum_info_t *&);
	void quorum_device_removed(const quorum_device_info_t *, bool);
	bool initialize_device_quorum_info(quorum_table_t&, quorum_info_t *&);
	void process_device_quorum_info();
	void delete_quorum_info();

	//
	// routine to take ownership of a quorum device if possible
	//
	bool take_ownership(quorum::quorum_device_id_t, const nodeset&);

	//
	// routine to take ownership of a quorum device after a node
	// registers itself on a QD.
	//
	bool take_ownership_after_register(
	    quorum::quorum_device_id_t, const nodeset&);

	//
	// private function to do the actual preemption of non-cluster
	// members and reserve the device if no nodes to preempt.
	// (Called by the take_ownership*() methods)
	//
	int preempt_nodes_and_reserve(
	    quorum::quorum_device_id_t, const nodeset&, const nodeset&);

	//
	// private function to acquire quorum devices
	// (to be called with lock held)
	//
	void acquire_quorum_devices_lock_held(
		quorum::membership_bitmask_t, quorum::quorum_result_t&);

	//
	// function to check the quorum configuration to see if a given
	// node is connected to a given quorum device.
	//
	bool			_node_connected_to_quorum_device(
					nodeid_t,
					quorum::quorum_device_id_t);

	//
	// function to compute the number of votes held by a given set of
	// nodes and quorum devices
	//
	uint_t			vote_count(
					const nodeset& nodes,
					const nodeset& devices);

	//
	// Functions to print out info into the trace buffer.
	//
	void print_quorum_table(const quorum_table_t& qt);
	void print_quorum_status(const quorum::quorum_status_t& qstatus);

	// Method to determine if there is a valid (cluster member) owner.
	int find_resv_owner(quorum::quorum_device_id_t,
	    const nodeset&, nodeid_t&);

	//
	// Method to determine if any nodes to preempt and owner if not already
	// determined.
	//
	void find_owner_and_nodes_to_preempt(quorum::quorum_device_id_t,
	    const nodeset&, nodeset&, nodeid_t&, bool);

	//
	// BugId 4298040
	// The following method is invoked right before trying to
	// acquire quorum devices as part of the begin_state of a
	// reconfiguration. It performs bus resets for non-fibre QDs
	// that this node is connected to provided these QDs are also
	// connected to nodes that are no longer in the membership.
	// This bus reset is necessary for cases when a node is
	// powered off in the middle of a SCSI transaction, resulting
	// in the target (the disk drive) retrying it reselection of
	// the initiator continually and saturating the bus, thus
	// preventing the non-powered off nodes from doing anything
	// useful on that bus.
	//
	void reset_quorum_devices_bus(
		quorum::membership_bitmask_t);

	//
	// This method gets the quorum status of:
	// (i) all cluster nodes as recorded in the quorum algo impl object
	// (ii) quorum devices (passed in argument)
	// The quorum devices is considered as the set of online quorum devices.
	//
	void quorum_get_status_common(quorum::quorum_status_t_out,
	    quorum::membership_bitmask_t, Environment &);

	//
	// Try to access an erstwhile inaccessible quorum device.
	// Try opening the quorum device, reading keys from it,
	// registering this node's keys on the device and taking ownership.
	// If any of the operation fails, then return false signifying
	// that the quorum device still cannot be marked online;
	// else return true signifying that the quorum device is online now,
	// and also syslog messages and publish cluster event
	// for the quorum device becoming online.
	//
	bool offline_qd_is_online_now(quorum_device_id_t);

	//
	// An erstwhile online quorum device is not accessible now.
	// Disable failfast on it, close the device and mark it inaccessible.
	// Also syslog messages and publish cluster event saying that
	// the QD was online but is now offline.
	//
	void online_qd_is_offline_now(quorum_device_id_t);

	//
	// This method tries to do this :
	// for each quorum device configured,
	// (1) if quorum device is marked inaccessible,
	// try opening it, and if the open succeeds, then mark it accessible.
	// (2) read keys on the quorum device
	// (3) register local node's key on the quorum device
	// (4) try to take ownership of the quorum device
	// (5) enable failfast for the quorum device
	// If any of the above operations fail for a quorum device,
	// this method marks that quorum device as inaccessible and closes it.
	//
	// The caller of this method passes in a caller string
	// to trace in CMM trace buffer messages.
	//
	void bring_qds_online(const char *caller_namep);

	//
	// Query the Disk Path Monitor for the status of a shared disk.
	// Returns true if the status is OK; false otherwise.
	//
	bool get_disk_status_from_dpm(const char *disk_namep,
	    quorum::membership_bitmask_t nodes_with_configured_paths,
	    nodeset current_nodes);
};

// Macro that returns a node's SCSI-3 reservation key
#define	RESERVATION_KEY(nodeid) \
	(_quorum_table->nodes.list[(nodeid)-1].reservation_key)

// Macro that returns my node's SCSI-3 reservation key
#define	MY_RESERVATION_KEY RESERVATION_KEY(_my_nodeid)

// Macro that checks if our reservation key was previously placed on
// a given quorum device
#define	MY_KEY_WAS_ON_DEVICE(qid) \
	(_quorum_devices_registered & (1LL << ((qid)-1)))

//
// Function to determine the required number of votes for quorum
// out of a specified number of total votes.
//
int quorum(int total_votes);

#endif	/* _QUORUM_IMPL_H */
