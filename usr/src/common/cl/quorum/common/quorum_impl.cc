//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)quorum_impl.cc	1.128	09/04/29 SMI"

#ifdef _KERNEL_ORB // quorum is used only by the kernel CMM

#include <sys/rsrc_tag.h>
#include <sys/file.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_ns_int.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_impl.h>
#include <h/dpm.h>
#include <quorum/common/quorum_impl.h>
#include <quorum/common/quorum_util.h>
#include <sys/cl_efi.h>
#include <sys/clconf_int.h>

//
// This module implements the cluster quorum algorithm.
//

//
// There is exactly one instance of the quorum algorithm object per node.
//
static quorum::quorum_algorithm_ptr the_quorum_algorithm_p = nil;
static quorum_algorithm_impl	*the_quorump = NULL;
//
// Constructor
//
quorum_algorithm_impl::quorum_algorithm_impl(quorum_table_t& qt)
{
	uint_t	i;
	naming::naming_context_var	ctx_v;
	Environment			e;

	_my_nodeid = orb_conf::local_nodeid();
	_max_nodeid = 0;
	_max_qid = 0;
	_total_configured_votes = 0;
	_current_cluster_votes = 0;
	_cluster_has_quorum = false;
	_ccr_is_refreshed = false;
	_we_have_been_preempted = false;
	_last_known_cluster_members = 0;
	_quorum_table_updated = false;
	_cluster_owned_devices = 0;
	_quorum_config_use_count = 0;
	_are_qd_owners_set = false;

	_quorum_table = NULL;
	_quorum_devices = NULL;
	_quorum_devices_owned = NULL;
	_quorum_devices_registered = 0;

	idl_reservation_list.listsize = 0;
	idl_reservation_list.listlen = 0;

	idl_registration_list.listsize = 0;
	idl_registration_list.listlen = 0;

	char nodename[8];
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());
	_quorum_syslog_msgp =
	    new os::sc_syslog_msg(SC_SYSLOG_CMM_NODE_TAG, nodename, NULL);

	//
	// Lookup the type registry in the local nameserver and
	// keep a reference to it.
	//
	CMM_TRACE(("quorum_impl: Attempting to get ref to type registry\n"));
	ctx_v = ns::local_nameserver();
	CORBA::Object_var tr_v = ctx_v->resolve("type_registry", e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// This is an internal error during node initialization, and
		// the system can not continue.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "INTERNAL ERROR CMM: Cannot resolve type registry "
		    "object in local name server.");
		return;
	}

	_type_registry_v = quorum::device_type_registry::_narrow(tr_v);

	CMM_TRACE(("device_type_registry retrieved from nameserver.\n"));

	initialize_quorum_info(qt);

	my_key = (quorum::registration_key_t *)(&MY_RESERVATION_KEY);
}

//
// Destructor
//
quorum_algorithm_impl::~quorum_algorithm_impl()
{
	bool	lock_already_held;

	CMM_TRACE(("In destructor of quorum_algorithm_impl.\n"));

	//
	// Delete info that is dynamically allocated for each quorum config
	// (deleted when quorum config changes).
	//
	lock_already_held = config_write_lock_if_not_locked();
	delete_quorum_info();
	config_write_unlock(lock_already_held);

	delete _quorum_syslog_msgp;

	//
	// Lint complains about pointer fields that are deallocated in
	// delete_quorum_info().
	//
	/*CSTYLED*/
} /*lint !e1740 */

//
// Unreferenced
//
void
#ifdef DEBUG
quorum_algorithm_impl::_unreferenced(unref_t cookie)
#else
quorum_algorithm_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_TRACE(("In quorum_algorithm_impl::_unreferenced.\n"));
	delete this;
}

//
// IDL Interface Functions
//

//
// Explicitly set the cluster membership
// that is known to the quorum algorithm.
//
void
quorum_algorithm_impl::update_membership(
	membership_bitmask_t cluster_members,
	Environment &)
{
	CMM_TRACE(("update_membership(0x%llx) called.\n",
		cluster_members));
	_last_known_cluster_members = cluster_members;
}

//
// acquire_quorum_devices : try to grab as many quorum devices as possible
//
// 1. Check if the current partition is able to acquire quorum if all
//    quorum devices can be acquired. If not, grab none.
// 2. Grab the quorum lock and call an internal function to acquire
//    quorum devices.
//
void
quorum_algorithm_impl::acquire_quorum_devices(
	membership_bitmask_t cluster_members,
	quorum::quorum_result_t& quorum_result,
	Environment &e)
{
	bool		lock_already_held;

	CMM_TRACE(("acquire_quorum_devices(0x%llx) called.\n",
		cluster_members));

	//
	// If the current partition cannot have quorum even if it
	// acquired all quorum devices that the member nodes are
	// connected to, then no attempt should be made to acquire
	// quorum devices. This eliminates unnecessary preempts of
	// potential surviving partitions by doomed partitions.
	//
	if (!partition_can_have_quorum(cluster_members, e)) {
		quorum_result.nodeid = _my_nodeid;
		quorum_result.quorum_devices_owned = 0;
		quorum_result.we_have_been_preempted = _we_have_been_preempted;
	} else {
		lock_already_held = config_write_lock_if_not_locked();
		CL_PANIC(!lock_already_held);

		//
		// If the cluster membership hasn't changed, then there is no
		// need to do any work on the quorum devices.
		// We are guaranteed that if a previous CMM reconfiguration
		// was aborted after having set the _last_known_cluster_members,
		// then the acquire_quorum_devices would have completed as
		// part of that CMM reconfiguration.
		//
		if ((_last_known_cluster_members != cluster_members) ||
		    (_quorum_table_updated)) {
			//
			// Update Membership.
			//
			update_membership(cluster_members, e);
			_quorum_table_updated = false;

			_ioctl_mutex.lock();
			//
			// BugId 4298040 - do a bus reset for all
			// non-fibre QDs that are connected to this node
			// and connected to a node that we think is down.
			//
			reset_quorum_devices_bus(cluster_members);

			acquire_quorum_devices_lock_held(
				cluster_members, quorum_result);

			_ioctl_mutex.unlock();
		}
		config_write_unlock(lock_already_held);
	}
}


//
// check_cluster_quorum : called by CMM automaton
//
bool
quorum_algorithm_impl::check_cluster_quorum(
	const quorum::cluster_quorum_results_t& quorum_result_list,
	Environment &)
{
	nodeset			node_set;
	nodeset			device_set;
	quorum_device_id_t	qid;
	bool			return_value;
	uint_t			i;
	bool			lock_already_held;
	nodeid_t		*orig_qd_owners = new nodeid_t[_max_qid + 1];
	ASSERT(orig_qd_owners);

	// This method is a writer of config info (the owner field).
	lock_already_held = config_write_lock_if_not_locked();

	int required_quorum_votes = quorum(_total_configured_votes);
	if (required_quorum_votes == -1) {
		//
		// At this time, we are doing CMM reconfiguration and
		// thus checking cluster quorum to determine whether
		// we should stay up or not.
		// If we cannot determine what is the required number
		// of quorum votes for survival under the membership
		// properties being used by the cluster,
		// then we should halt the node.
		//
		CMM_TRACE((
		    "check_cluster_quorum: quorum(%d) returned -1\n",
		    _total_configured_votes));
		//
		// SCMSGS
		// @explanation
		// Quorum software could not determine the number of
		// quorum votes that are required for survival
		// under the current membership properties
		// being used by the cluster.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Could not determine how many votes "
		    "constitute quorum.");
	}

	_cluster_owned_devices = 0;

	//
	// If no list is passed in, simply return the current cluster
	// quorum status.
	//
	if ((&quorum_result_list == NULL) ||
	    (quorum_result_list.length() == 0)) {
		return_value =
		    (_current_cluster_votes >= (uint_t)required_quorum_votes);
		goto done;
	}

	//
	// Initialize the quorum ownership array. It is possible that
	// previous owners have died.
	//
	for (qid = 1; qid <= _max_qid; qid++) {
		//
		// Store the orignal quorum info so that we can compare and
		// get the state changes later.
		//
		orig_qd_owners[qid] = _quorum_devices[qid].owner; //lint !e661
		_quorum_devices[qid].owner = NODEID_UNKNOWN;
	}

	for (i = 0; i < quorum_result_list.length(); i++) {

		membership_bitmask_t current_owned_devices =
			quorum_result_list[i].quorum_devices_owned;
		nodeid_t nodeid = quorum_result_list[i].nodeid;

		CL_PANIC((nodeid >= 1) && (nodeid <= _max_nodeid));

		//
		// If this node thinks that our partition has
		// been preempted by a competing partition,
		// we should halt to minimize competition for
		// quorum devices.
		//
		if (quorum_result_list[i].we_have_been_preempted) {
			CMM_TRACE(("our partition has been preempted.\n"));
			//
			// SCMSGS
			// @explanation
			// The cluster partition to which this node belongs
			// has been preempted by another partition during a
			// reconfiguration. The preempted partition will
			// abort. If a cluster gets divided into two or more
			// disjoint subclusters, exactly one of these must
			// survive as the operational cluster. The surviving
			// cluster forces the other subclusters to abort by
			// grabbing enough votes to grant it majority quorum.
			// This is referred to as preemption of the losing
			// subclusters.
			// @user_action
			// There may be other related messages that may
			// indicate why quorum was lost. Determine why quorum
			// was lost on this node partition, resolve the
			// problem and reboot the nodes in this partition.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Our partition has been preempted.");
			return_value = false;
			goto done;
		}

		//
		// Remember the devices owned by this node for use in
		// check_ccr_quorum.
		//
		_quorum_devices_owned[nodeid] = current_owned_devices;

		node_set.add_node(nodeid);
		_cluster_owned_devices |= current_owned_devices;

		//
		// If this node owns any quorum devices, update the
		// quorum_owners array.
		//
		if (current_owned_devices) {

			nodeset current_device_set(current_owned_devices);

			for (qid = 1; qid <= _max_qid; qid++) {
				if (current_device_set.contains(qid)) {
					//
					// nodeid claims ownership of qid. At
					// most one node in the cluster can
					// assume ownership of a device.
					// However, due to race conditions when
					// nodes register with the device, two
					// nodes may end up thinking they are
					// the owners, although it is the
					// lowest nodeid that is the actual
					// owner. Set the owner to the lowest
					// nodeid that thinks it is the owner.
					//
					if ((_quorum_devices[qid].owner ==
					    NODEID_UNKNOWN) ||
					    (_quorum_devices[qid].owner >
					    nodeid)) {
						_quorum_devices[qid].owner =
									nodeid;
					}
				}
			}
		}
	}

	device_set.set(_cluster_owned_devices);

	_current_cluster_votes = vote_count(node_set, device_set);

	CMM_TRACE(("check_cluster_quorum(): "
		"nodes=0x%llx, devices=0x%llx, votes=%d, "
		"total configured votes = %d, required quorum = %d\n",
		node_set.bitmask(), _cluster_owned_devices,
		_current_cluster_votes, _total_configured_votes,
		required_quorum_votes));

	if (_current_cluster_votes >= (uint_t)required_quorum_votes) {
		_cluster_has_quorum = true;
		CMM_DEBUG_TRACE(("We have quorum.\n"));
		return_value = true;
	} else {
		CMM_DEBUG_TRACE(("WE DO NOT HAVE QUORUM.\n"));
		return_value = false;
	}

done:
	//
	// If we reach this point, we have assigned ownership to
	// all QD's, and it is safe to allow quorum_check_transition.
	//
	nodeset	cluster_members(_last_known_cluster_members);
	if ((&quorum_result_list != NULL) &&
		(quorum_result_list.length() != 0)) {
		_are_qd_owners_set = true;
		_qd_owners_cv.broadcast();

		//
		// check if any existing quorum devices have a change of state.
		//
		for (qid = 1; qid <= _max_qid; qid++) {
			if ((orig_qd_owners[qid] == NODEID_UNKNOWN) &&
			    //lint -e661
			    (_quorum_devices[qid].owner != NODEID_UNKNOWN)) {
			    //lint +e661
				// This quorum device has become online
				if (_my_nodeid != _quorum_devices[qid].owner) {
					//
					// Only the owner will publish the
					// event.
					//
					continue;
				}
				//
				// When a qd's vote count is getting decremented
				// ( like when a scconf -rq is done ) to 0, dont
				// send any event. This is because pathname will
				// be NULL in such a case. Hence check for this
				// case by looking if qd_v is set to NIL or not
				//
				if (CORBA::is_nil(_quorum_devices[qid].qd_v)) {
					continue;
				}

				//
				// We are the owner of this quorum device, we
				// will publish a sysevent
				//
				(void) sc_publish_event(
				    ESC_CLUSTER_QUORUM_STATE_CHANGE,
				    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_QUORUM_NAME, SE_DATA_TYPE_STRING,
				    _quorum_devices[qid].pathname,
				    CL_OLD_STATE, SE_DATA_TYPE_STRING,
				    "offline",
				    CL_NEW_STATE, SE_DATA_TYPE_STRING, "online",
				    NULL);
			} else if ((orig_qd_owners[qid] != NODEID_UNKNOWN) &&
			    //lint -e661
			    (_quorum_devices[qid].owner == NODEID_UNKNOWN)) {
			    //lint +e661
				// This quorum device has become offline

				//
				// find out the lowest nodeid in the current
				// cluster membership.
				//
				nodeid_t	lowest_nid;
				for (lowest_nid = 1; lowest_nid <= _max_nodeid;
				    lowest_nid++) {
					if (cluster_members.contains(
					    lowest_nid)) {
						// This is the lowest node id
						break;
					}
				}

				if (_my_nodeid != lowest_nid) {
					//
					// if we are not the node with the
					// lowest nodeid in current cluster
					// membership, we will not publish
					// this sysevent.
					//
					continue;
				}

				//
				// Since the quorum has become offline, we
				// need to find out the qd name from
				// _quorum_table.
				//
				quorum_device_info_t	*quorum_info_p = NULL;
				for (i = 0;
				    i < _quorum_table->quorum_devices.listlen;
				    i++) {

					quorum_info_p = &_quorum_table->
					    quorum_devices.list[i];

					if (quorum_info_p->qid == qid) {
						// found the qd info
						break;
					}
				}
				ASSERT(i <
				    _quorum_table->quorum_devices.listlen);
				ASSERT(quorum_info_p);

				// publish sysevent
				(void) sc_publish_event(
				    ESC_CLUSTER_QUORUM_STATE_CHANGE,
				    CL_EVENT_PUB_CMM, CL_EVENT_SEV_ERROR,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_QUORUM_NAME, SE_DATA_TYPE_STRING,
				    quorum_info_p->gdevname,
				    CL_OLD_STATE, SE_DATA_TYPE_STRING, "online",
				    CL_NEW_STATE, SE_DATA_TYPE_STRING,
				    "offline",
				    NULL);
			}
		}
	}

	delete [] orig_qd_owners;
	config_write_unlock(lock_already_held);

	return (return_value);
}

//
// check_ccr_quorum : called by CCR transaction server to see if the
// set of CCR data server nodes constitutes a quorum.
//
bool
quorum_algorithm_impl::check_ccr_quorum(
	const quorum::ccr_id_list_t& ccr_id_list,
	Environment &)
{
	nodeid_t		nid;
	membership_bitmask_t	cluster_devices = 0;
	nodeset			node_set;
	nodeset			device_set;
	uint_t			votes;
	bool			return_value;
	uint_t			i;

	// This method is a reader of quorum config information.
	config_read_lock();

	int required_quorum_votes = quorum(_total_configured_votes);
	if (required_quorum_votes == -1) {
		//
		// We could not determine what is the required number
		// of quorum votes for survival under the membership
		// properties being used by the cluster.
		//
		CMM_TRACE((
		    "check_ccr_quorum: quorum(%d) returned -1\n",
		    _total_configured_votes));
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Could not determine how many votes "
		    "constitute quorum.");
	}

	for (i = 0; i < ccr_id_list.length(); i++) {

		//
		// The CCR passes a list of node IDs of the
		// CCR data servers.
		//
		nid = (nodeid_t)ccr_id_list[i];
		CL_PANIC((nid >= 1) && (nid <= _max_nodeid));

		node_set.add_node(nid);
		cluster_devices |= _quorum_devices_owned[nid];
	}

	device_set.set(cluster_devices);

	votes = vote_count(node_set, device_set);

	CMM_TRACE(("check_ccr_quorum(): "
		"nodes=0x%llx, devices=0x%llx, votes=%d, "
		"total configured votes = %d; required quorum = %d\n",
		node_set.bitmask(), cluster_devices, votes,
		_total_configured_votes, required_quorum_votes));

	if (votes >= (uint_t)required_quorum_votes) {
		CMM_DEBUG_TRACE(("CCR has quorum.\n"));
		return_value = true;
	} else {
		CMM_DEBUG_TRACE(("CCR DOES NOT HAVE QUORUM.\n"));
		return_value = false;
	}

	config_read_unlock();

	return (return_value);
}

//
// ccr_refreshed
//
// This routine is called by the local CCR data server to indicate that the
// CCR is now up to date. This is done when the CCR data server has completed
// CCR recovery. This node can now place its reservation key on all
// quorum devices this node is connected to.
//
void
quorum_algorithm_impl::ccr_refreshed(Environment &)
{
	bool lock_already_held;

	CMM_TRACE(("ccr_refreshed called\n"));

	//
	// Since this routine issues SCSI-3 IOCTLs, obtain the ioctl
	// lock to serialize access to the quorum devices.
	//
	lock_already_held = config_write_lock_if_not_locked();
	_ioctl_mutex.lock();

	_ccr_is_refreshed = true;

	bring_qds_online("ccr_refreshed");

	_ioctl_mutex.unlock();
	config_write_unlock(lock_already_held);
}

//
// cmm_reconfig_done
//
// This method is called by the CMM automaton once it enters
// the CMM_END_WAIT state.
// This method tries to do this :
// for each quorum device configured,
// (1) if quorum device is marked inaccessible,
// try opening it, and if the open succeeds, then mark it accessible.
// (2) read keys on the quorum device
// (3) register local node's key on the quorum device
// (4) try to take ownership of the quorum device
// (5) enable failfast for the quorum device
//
// This method does tasks similar to ccr_refreshed() with some differences.
// ccr_refreshed() is done on the first CMM reconfiguration.
// This method is a no-op on the first CMM reconfiguration;
// it does its task on every subsequent CMM reconfiguration.
//
void
quorum_algorithm_impl::cmm_reconfig_done(Environment &)
{
	bool lock_already_held;

	lock_already_held = config_write_lock_if_not_locked();
	_ioctl_mutex.lock();

	if (!_ccr_is_refreshed) {
		//
		// ccr_refreshed() has not been called ever.
		// That means its the first CMM reconfiguration
		// and ccr_refreshed() is yet to do its task.
		// So we do not need to do anything.
		// This is a small optimization.
		//
		_ioctl_mutex.unlock();
		config_write_unlock(lock_already_held);
		return;
	}

	bring_qds_online("cmm_reconfig_done");

	_ioctl_mutex.unlock();
	config_write_unlock(lock_already_held);
}

//
// This is a common method used by ccr_refreshed() and cmm_reconfig_done().
// This method tries to do this :
// for each quorum device configured,
// (1) if quorum device is marked inaccessible,
// try opening it, and if the open succeeds, then mark it accessible.
// (2) read keys on the quorum device
// (3) register local node's key on the quorum device
// (4) try to take ownership of the quorum device
// (5) enable failfast for the quorum device
// If any of the above operations fail for a quorum device,
// this method marks that quorum device as inaccessible and closes it.
//
// The caller of this method passes in a caller string
// to trace in CMM trace buffer messages.
//
void
quorum_algorithm_impl::bring_qds_online(const char *caller_namep)
{
	quorum_device_id_t qid;

	ASSERT(_quorum_config_mutex.lock_held());
	ASSERT(_ioctl_mutex.lock_held());

	CMM_TRACE(("%s called bring_qds_online: started\n", caller_namep));

	for (qid = 1; qid <= _max_qid; qid++) {

		Environment env;
		bool found_my_key = false;
		quorum::quorum_error_t q_error = quorum::QD_SUCCESS;

		//
		// During initialization of quorum device info,
		// we check if this node has a path to a quorum device,
		// and if so, then we try to instantiate a CORBA object
		// for that quorum device, and then try to open the device.
		// This is provided that the QD is not in maintenance state
		// (QD in maintenance state means its votecount would be 0).
		//
		// If the instantiation of the CORBA object for a quorum
		// device failed, or if this node does not have a path to
		// a quorum device, then we would have a nil CORBA reference
		// to the quorum device. We skip such a quorum device.
		//
		// If the open of the device failed earlier, we would have
		// marked the device as not accessible. But we would still
		// have the CORBA reference to the quorum device.
		// We try re-opening the quorum device now;
		// the quorum device might be accessible now.
		//

		if (CORBA::is_nil(_quorum_devices[qid].qd_v)) {
			//
			// Either no QD is configured with this qid,
			// or this QD is in maintenance state,
			// or we failed to instantiate a CORBA object
			// for this quorum device earlier, or this node
			// does not have a path to this quorum device.
			// We skip this quorum device.
			//
			ASSERT(!_quorum_devices[qid].accessible);
			continue;
		}

		if (!_quorum_devices[qid].accessible) {
			//
			// Quorum device is marked non-accessible.
			// Try opening it now.
			// Call open using the reference.
			// This will also perform any necessary initialization.
			//
			quorum::qd_property_seq_t props;
			q_error = _quorum_devices[qid].qd_v->quorum_open(
			    _quorum_devices[qid].pathname, false, props, env);
			if (env.exception()) {
				CMM_TRACE(("%s: Exception returned "
				    "from quorum_open\n", caller_namep));
				env.exception()->print_exception(
				    (char *)"quorum_open: ");
			}
			// quorum_open() does not throw any exceptions
			CL_PANIC(env.exception() == NULL);

			//
			// We had earlier tried to open the device and
			// had failed. We failed to open the device yet again.
			// So we will keep the device marked as not accessible.
			// But, we will not drop the reference to the device.
			// We will re-try opening this quorum device
			// during our next CMM reconfiguration.
			//
			if (q_error != quorum::QD_SUCCESS) {
				ASSERT(!_quorum_devices[qid].accessible);
				CMM_TRACE(("%s: quorum_open failed for QD '%s' "
				    "(qid %d) with error %d.\n.", caller_namep,
				    _quorum_devices[qid].pathname,
				    qid, (int)q_error));

				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "CMM: Open failed for quorum device %s "
				    "with error %d.",
				    _quorum_devices[qid].pathname,
				    (int)q_error);

				(void) _quorum_devices[qid].qd_v->
				    quorum_close(env);
				// quorum_close() does not raise any exceptions
				ASSERT(env.exception() == NULL);

				// Skip this quorum device.
				continue;
			}

			// Mark the quorum device as accessible
			_quorum_devices[qid].accessible = true;
		}

		//
		// Read the reservation keys on the device and check if
		// the key is already on the device. If we have a device
		// which we haven't been able to access because we were
		// fenced, the read keys command will retry the device
		// initialization steps.
		//
		q_error = _quorum_devices[qid].qd_v->quorum_read_keys(
		    idl_registration_list, env);
		if (env.exception() != NULL) {
			CMM_TRACE(("%s: Exception returned "
			    "from quorum_read_keys\n", caller_namep));
			env.exception()->print_exception(
			    (char *)"quorum_read_keys: ");
		}
		// quorum_read_keys() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
		if (q_error != quorum::QD_SUCCESS) {
			//
			// Attempt at reading keys from this device has failed.
			// Mark the device as not accessible.
			//
			CMM_TRACE(("%s: quorum_read_keys for QD '%s' (qid=%d) "
			    "returned error %d, marking QD as not accessible\n",
			    caller_namep, _quorum_devices[qid].pathname,
			    qid, (int)q_error));
			//
			// SCMSGS
			// @explanation
			// An error was encountered while trying to read
			// reservation keys on the specified quorum device.
			// @user_action
			// There may be other related messages on this and
			// other nodes connected to this quorum device that
			// may indicate the cause of this problem. Refer to
			// the quorum disk repair section of the
			// administration guide for resolving this problem.
			//
			(void) _quorum_syslog_msgp->log(
				SC_SYSLOG_WARNING, MESSAGE,
				"CMM: Reading reservation keys from quorum "
				"device %s failed with error %d.",
				_quorum_devices[qid].pathname, (int)q_error);

			q_error = _quorum_devices[qid].qd_v->quorum_close(env);
			// quorum_close() does not throw any exceptions
			CL_PANIC(env.exception() == NULL);
			if (q_error != quorum::QD_SUCCESS) {
				CMM_TRACE(("%s: quorum_close error %d for "
				    "QD '%s'.\n", caller_namep, (int)q_error,
				    _quorum_devices[qid].pathname));
			}

			// Mark the quorum device as not accessible
			_quorum_devices[qid].accessible = false;

			// Skip the quorum device
			continue;
		}

		//
		// Mark a flag depending on whether the key is already present.
		// We do different function calls while taking ownership,
		// based on this flag.
		//
		for (uint_t i = 0; i < idl_registration_list.listlen; i++) {
			quorum::registration_key_t	*m_key;
			m_key = (quorum::registration_key_t *)
			    (&idl_registration_list.keylist[i]);
			if (match_idl_keys(m_key, (quorum::registration_key_t *)
			    (&MY_RESERVATION_KEY))) {
				CMM_TRACE(("%s: My key found on QD %d\n",
				    caller_namep, qid));
				found_my_key = true;
				break;
			}
		}

		//
		// We try to register our key on the device regardless of
		// whether it is already there or not.
		// This avoids problems with multipathing and one
		// path being down but coming up later. See bug 4855298.
		//
		q_error = _quorum_devices[qid].qd_v->
		    quorum_register(*my_key, env);
		if (env.exception()) {
			CMM_TRACE(("%s: quorum_register returned an "
			    "exception\n", caller_namep));
			env.exception()->print_exception(
			    (char *)"quorum_register: ");
		}
		// quorum_register() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
		if (q_error != quorum::QD_SUCCESS) {
			//
			// Attempt to register key on this device has
			// failed. Mark the device as not accessible.
			//
			CMM_TRACE(("%s: quorum_register for QD '%s' "
			    "(qid=%d) returned error %d, marking QD as "
			    "not accessible\n", caller_namep,
			    _quorum_devices[qid].pathname, qid, (int)q_error));
			//
			// SCMSGS
			// @explanation
			// An error was encountered while trying to
			// place the local node's reservation key on
			// the specified quorum device. This node will
			// ignore this quorum device.
			// @user_action
			// There may be other related messages on this
			// and other nodes connected to this quorum
			// device that may indicate the cause of this
			// problem. Refer to the quorum disk repair
			// section of the administration guide for
			// resolving this problem.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Registering reservation key on "
			    "quorum device %s failed with error %d.",
			    _quorum_devices[qid].pathname, (int)q_error);

			q_error = _quorum_devices[qid].qd_v->quorum_close(env);
			// quorum_close() does not throw any exceptions
			CL_PANIC(env.exception() == NULL);
			if (q_error != quorum::QD_SUCCESS) {
				CMM_TRACE(("%s: quorum_close error %d "
				    "for QD '%s'.\n", caller_namep,
				    (int)q_error,
				    _quorum_devices[qid].pathname));
			}

			// Mark the quorum device as not accessible
			_quorum_devices[qid].accessible = false;

			// Skip this quorum device
			continue;
		}

		if (!found_my_key) {
			//
			// Key was not present. We registered our key just now.
			//
			nodeset member_set(_last_known_cluster_members);
			CMM_TRACE(("%s: Registration key placed on QD %d\n",
			    caller_namep, qid));

			//
			// If this nodeid is the owner of the QD,
			// then clear that information.
			//
			if (_quorum_devices[qid].owner == _my_nodeid) {
				_quorum_devices[qid].owner = NODEID_UNKNOWN;
			}

			//
			// Bug 4339781: Successfully registered my key.
			// Check if non-cluster members on the QD, and
			// preempt if so. Also, place reservation on
			// QD if this node turns out to be the owner.
			//
			if (take_ownership_after_register(qid, member_set)) {
				_quorum_devices_owned[_my_nodeid] |=
				    1LL << (qid-1);
			}

		} else {
			//
			// Key was present. Try to take ownership.
			//
			nodeset member_set(_last_known_cluster_members);
			//
			// If this nodeid is the owner of the QD,
			// then clear that information.
			//
			if (_quorum_devices[qid].owner == _my_nodeid) {
				_quorum_devices[qid].owner = NODEID_UNKNOWN;
			}

			if (take_ownership(qid, member_set)) {
				_quorum_devices_owned[_my_nodeid] |=
				    1LL << (qid-1);
			}
		}

		//
		// Update the bitmask of quorum devices on which we have
		// our key registered : either the key was already on
		// the device or was registered above.
		//
		_quorum_devices_registered |= 1LL << (qid - 1);

		//
		// Enable failfast if key was already on the device,
		// or key got registered above.
		// The operation of enabling failfast is idempotent
		// for all types of quorum devices.
		//
		q_error = _quorum_devices[qid].qd_v->
		    quorum_enable_failfast(env);
		if (env.exception()) {
			CMM_TRACE(("%s: quorum_enable_failfast "
			    "returned an exception\n", caller_namep));
			env.exception()->print_exception(
			    (char *)"quorum_enable_failfast: ");
		}
		// quorum_enable_failfast() does not throw any exception
		CL_PANIC(env.exception() == NULL);
		if (q_error != quorum::QD_SUCCESS) {
			//
			// Attempt to enable failfast on this device has
			// failed. Mark the device as not accessible.
			//
			CMM_TRACE(("%s: quorum_enable_failfast for QD '%s' "
			    "(qid=%d) returned error %d, marking QD as "
			    "not accessible\n", caller_namep,
			    _quorum_devices[qid].pathname, qid, (int)q_error));
			//
			// SCMSGS
			// @explanation
			// An attempt to enable failfast on the specified
			// quorum device failed.
			// @user_action
			// Check if the specified quorum disk has failed.
			// This message may also be logged when a node is
			// booting up and has been preempted from the cluster,
			// in which case no user action is necessary.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Enabling failfast on quorum device %s "
			    "failed.", _quorum_devices[qid].pathname);

			q_error = _quorum_devices[qid].qd_v->quorum_close(env);
			if (env.exception()) {
				CMM_TRACE(("%s: quorum_close returned "
				    "an exception\n", caller_namep));
				env.exception()->print_exception(
				    (char *)"quorum_close: ");
			}
			// quorum_close() does not throw any exceptions
			CL_PANIC(env.exception() == NULL);
			if (q_error != quorum::QD_SUCCESS) {
				CMM_TRACE(("%s: quorum_close error %d for "
				    "QD '%s'.\n", caller_namep, (int)q_error,
				    _quorum_devices[qid].pathname));
			}

			// Mark the quorum device as not accessible
			_quorum_devices[qid].accessible = false;

			// Skip this quorum device
			continue;
		}

		// The device must have been marked accessible at this point
		ASSERT(_quorum_devices[qid].accessible);

		//
		// For the given quorum device, update our information
		// about the devices owned by the cluster.
		//
		_cluster_owned_devices |= 1LL << (qid -1);
	}

	CMM_TRACE(("%s called bring_qds_online: finished\n", caller_namep));
}

//
// Returns true if the partition represented by "partition" can achieve
// quorum if it acquired all quorum devices its members are connected to.
//
bool
quorum_algorithm_impl::partition_can_have_quorum(
	membership_bitmask_t	partition,
	Environment &e)
{
	bool			quorum_possible;

	//
	// This method is a reader, as is the method it calls. Need
	// to acquire a read-lock before calling the method since
	// we use _total_configured_votes here
	// which needs to be protected by this lock.
	//
	config_read_lock();

	int required_quorum_votes = quorum(_total_configured_votes);
	if (required_quorum_votes == -1) {
		//
		// At this time, we are doing CMM reconfiguration and
		// hence checking if our partition can have quorum.
		// If we cannot determine what is the required number of
		// quorum votes for survival under the membership properties
		// being used by the cluster, then we should halt the node.
		//
		CMM_TRACE(("partition_can_have_quorum: "
		    "quorum(%d) returned -1\n", _total_configured_votes));
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Could not determine how many votes "
		    "constitute quorum.");
	}

	quorum_possible = (max_votes_of_partition(partition, e) >=
		(uint32_t)required_quorum_votes);
	config_read_unlock();

	CMM_TRACE(("partition(0x%llx) can%s have quorum\n", partition,
		quorum_possible ? "" : "not"));

	return (quorum_possible);
}

//
// This function returns the number of votes required for the cluster to
// have quorum.
//
uint32_t
quorum_algorithm_impl::minimum_cluster_quorum(Environment &)
{
	int required_quorum_votes = quorum(_total_configured_votes);
	if (required_quorum_votes == -1) {
		//
		// We could not determine what is the required number
		// of quorum votes for survival under the membership
		// properties being used by the cluster.
		//
		CMM_TRACE((
		    "minimum_cluster_quorum: quorum(%d) returned -1\n",
		    _total_configured_votes));
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Could not determine how many votes "
		    "constitute quorum.");
	}

	return (uint32_t)(required_quorum_votes);
}

//
// This function returns the number of votes currently controlled by the
// cluster.
//
uint32_t
quorum_algorithm_impl::current_cluster_votes(Environment &)
{
	return (uint32_t)(_current_cluster_votes);
}

//
// This function returns the number of votes currently
// contributed by the node specified in the argument
//
uint32_t
quorum_algorithm_impl::current_node_votes(nodeid_t nodeid, Environment &)
{
	nodeset			node_set;
	nodeset			device_set;
	uint32_t		votes;

	// This method is a reader of the quorum configuration information.
	config_read_lock();

	node_set.add_node(nodeid);
	device_set.set(_quorum_devices_owned[nodeid]);

	votes = vote_count(node_set, device_set);

	config_read_unlock();

	return (votes);
}

//
// This function returns the number of votes that can be controlled by
// a given partition if the members of the partition are able to acquire
// all quorum devices connected to them.
//
uint32_t
quorum_algorithm_impl::max_votes_of_partition(
	membership_bitmask_t partition,
	Environment &)
{
	nodeset			node_set;
	nodeset			device_set;
	nodeid_t		nid;
	quorum_device_id_t	qid;
	uint32_t		votes;

	// This method is a reader of the quorum configuration information.
	config_read_lock();

	node_set.set(partition);

	for (nid = 1; nid <= _max_nodeid; nid++) {
		if (node_set.contains(nid)) {
			for (qid = 1; qid <= _max_qid; qid++)
				if (_node_connected_to_quorum_device(nid, qid))
					device_set.add_node(qid);
		}
	}

	votes = vote_count(node_set, device_set);

	config_read_unlock();

	return (votes);
}

//
// An erstwhile online quorum device is not accessible now.
// Disable failfast on it, close the device and mark it inaccessible.
// Also syslog a message and publish a cluster event saying
// that the QD was online but is now offline.
//
void
quorum_algorithm_impl::online_qd_is_offline_now(quorum_device_id_t qid)
{
	Environment env;

	ASSERT(_quorum_config_mutex.lock_held());
	ASSERT(_ioctl_mutex.lock_held());

	// Quorum device was accessible earlier
	ASSERT(_quorum_devices[qid].accessible);

	// Quorum device has a valid CORBA object reference
	ASSERT(is_not_nil(_quorum_devices[qid].qd_v));

	//
	// Close the quorum device.
	//
	// Disabling failfast
	// ------------------
	// - For NAS quorum and Quorum Server, enabling failfast is a no-op;
	// so no need to disable failfast for these quorum device types
	// - For SCSI2/SCSI3 quorum devices, enabling failfast means
	// enabling driver failfast. The close operation on such quorum devices
	// disables the driver failfast.
	// - For software quorum devices, enabling failfast means
	// that a software fencing polling thread runs.
	// The close operation on the software quorum device disables
	// this failfast polling thread.
	//
	// We ignore any error in closing the quorum device,
	// as we are anyway marking the device as inaccessible.
	//
	(void) _quorum_devices[qid].qd_v->quorum_close(env);
	// quorum_close() does not throw any exceptions
	CL_PANIC(env.exception() == NULL);
	_quorum_devices[qid].accessible = false;

	CMM_TRACE(("Erstwhile online QD %s (qid %d) is offline now\n",
	    _quorum_devices[qid].pathname, qid));

	//
	// SCMSGS
	// @explanation
	// An erstwhile online quorum device has become inaccessible now.
	// This node will mark the quorum device as inaccessible.
	// The quorum device is considered offline now.
	// @user_action
	// Check if something has gone wrong with the quorum device.
	// The quorum device could have failed or the path to the device
	// could have broken down. Refer to the quorum disk repair section
	// of the administration guide for resolving this problem.
	//
	(void) _quorum_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "CMM: Erstwhile online quorum device %s (qid %d) "
	    "is inaccessible now.", _quorum_devices[qid].pathname, qid);
	// Publish cluster event saying erstwhile online QD became offline
	(void) sc_publish_event(ESC_CLUSTER_QUORUM_STATE_CHANGE,
	    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_QUORUM_NAME, SE_DATA_TYPE_STRING, _quorum_devices[qid].pathname,
	    CL_OLD_STATE, SE_DATA_TYPE_STRING, "online",
	    CL_NEW_STATE, SE_DATA_TYPE_STRING, "offline", NULL);
}

//
// Try to access an erstwhile inaccessible quorum device.
// Try opening the quorum device, reading keys from it,
// registering this node's keys on the device and taking ownership.
// If any of the operation fails, then return false signifying
// that the quorum device still cannot be marked online.
// Else return true signifying that the quorum device is online now;
// also mark the quorum device as accessible, syslog a message and
// publish a cluster event for the quorum device becoming online.
//
// Note that this method does not trace messages on failure
// of any operation on the quorum device.
// The device is offline anyway; so it could happen that the device
// is really bad. In that case, any attempt to access it will be unsuccessful.
// If we trace messages for failures for such a device,
// we will have many unnecessary messages in the trace buffer.
// Instead, we only trace a message and syslog a message if an offline
// quorum device becomes online.
//
bool
quorum_algorithm_impl::offline_qd_is_online_now(quorum_device_id_t qid)
{
	Environment env;
	bool found_my_key = false;
	quorum::quorum_error_t q_error = quorum::QD_SUCCESS;

	ASSERT(_quorum_config_mutex.lock_held());
	ASSERT(_ioctl_mutex.lock_held());

	// Quorum device was not accessible earlier
	ASSERT(!_quorum_devices[qid].accessible);

	// Quorum device has a valid CORBA object reference
	ASSERT(is_not_nil(_quorum_devices[qid].qd_v));

	//
	// Quorum device is marked non-accessible.
	// Try opening it now.
	// Call open using the reference.
	// This will also perform any necessary initialization.
	//
	quorum::qd_property_seq_t props;
	q_error = _quorum_devices[qid].qd_v->quorum_open(
	    _quorum_devices[qid].pathname, false, props, env);
	// quorum_open() does not throw any exceptions
	CL_PANIC(env.exception() == NULL);

	if (q_error != quorum::QD_SUCCESS) {
		// Return failure signifying that QD is not online
		ASSERT(!_quorum_devices[qid].accessible);
		(void) _quorum_devices[qid].qd_v->quorum_close(env);
		// quorum_close() does not raise any exceptions
		ASSERT(env.exception() == NULL);
		return (false);
	}

	//
	// Read the reservation keys on the device and check if
	// the key is already on the device. If we have a device
	// which we haven't been able to access because we were
	// fenced, the read keys command will retry the device
	// initialization steps.
	//
	q_error = _quorum_devices[qid].qd_v->quorum_read_keys(
	    idl_registration_list, env);
	// quorum_read_keys() does not throw any exceptions
	CL_PANIC(env.exception() == NULL);
	if (q_error != quorum::QD_SUCCESS) {
		//
		// Attempt to read keys from the device has failed.
		// Mark the device as not accessible.
		// Close the quorum device.
		// Return failure signifying that QD is not online.
		//
		q_error = _quorum_devices[qid].qd_v->quorum_close(env);
		// quorum_close() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
		//
		// We ignore any error in closing the quorum device,
		// as we are anyway marking the device as inaccessible.
		//
		_quorum_devices[qid].accessible = false;
		return (false);
	}

	//
	// Mark a flag saying whether the local node's key is already present.
	// We will use different functions to take ownership,
	// based on this flag.
	//
	for (uint_t i = 0; i < idl_registration_list.listlen; i++) {
		quorum::registration_key_t	*m_key;
		m_key = (quorum::registration_key_t *)
		    (&idl_registration_list.keylist[i]);
		if (match_idl_keys(m_key, (quorum::registration_key_t *)
		    (&MY_RESERVATION_KEY))) {
			found_my_key = true;
			break;
		}
	}

	//
	// We try to register our key on the device regardless of
	// whether it is already there or not.
	// This avoids problems with multipathing and one
	// path being down but coming up later. See bug 4855298.
	//
	q_error = _quorum_devices[qid].qd_v->quorum_register(*my_key, env);
	// quorum_register() does not throw any exceptions
	CL_PANIC(env.exception() == NULL);
	if (q_error != quorum::QD_SUCCESS) {
		//
		// Attempt at registering key on this device failed.
		// Mark the device as not accessible.
		// Close the quorum device.
		// Return failure signifying that QD is not online.
		//
		q_error = _quorum_devices[qid].qd_v->quorum_close(env);
		// quorum_close() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
		//
		// We ignore any error in closing the quorum device,
		// as we are anyway marking the device as inaccessible.
		//
		_quorum_devices[qid].accessible = false;
		return (false);
	}

	if (!found_my_key) {
		//
		// Key was not present; we just now registered our key.
		//
		nodeset member_set(_last_known_cluster_members);
		//
		// If this nodeid is the owner of the QD,
		// then clear that information.
		//
		if (_quorum_devices[qid].owner == _my_nodeid) {
			_quorum_devices[qid].owner = NODEID_UNKNOWN;
		}

		//
		// Successfully registered my key.
		// Check if keys of non-cluster members are present on the QD,
		// and preempt such keys if they exist.
		// Also, place reservation on QD
		// if this node turns out to be the owner.
		//
		if (take_ownership_after_register(qid, member_set)) {
			_quorum_devices_owned[_my_nodeid] |= 1LL << (qid-1);
		}
	} else {
		//
		// Key was present. Try to take ownership.
		//
		nodeset member_set(_last_known_cluster_members);
		//
		// If this nodeid is the owner of the QD,
		// then clear that information.
		//
		if (_quorum_devices[qid].owner == _my_nodeid) {
			_quorum_devices[qid].owner = NODEID_UNKNOWN;
		}

		if (take_ownership(qid, member_set)) {
			_quorum_devices_owned[_my_nodeid] |= 1LL << (qid-1);
		}
	}

	//
	// Update the bitmask of quorum devices on which we have
	// our key registered : either the key was already on
	// the device or was registered above.
	//
	_quorum_devices_registered |= 1LL << (qid - 1);

	//
	// Enable failfast if key was already on the device,
	// or key got registered above.
	// The operation of enabling failfast is idempotent
	// for all types of quorum devices.
	//
	q_error = _quorum_devices[qid].qd_v->quorum_enable_failfast(env);
	// quorum_enable_failfast() does not throw any exception
	CL_PANIC(env.exception() == NULL);
	if (q_error != quorum::QD_SUCCESS) {
		//
		// Attempt at enabling failfast on this device failed.
		// Mark the device as not accessible.
		// Close the quorum device.
		// Return failure signifying that QD is not online.
		//
		q_error = _quorum_devices[qid].qd_v->quorum_close(env);
		// quorum_close() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
		//
		// We ignore any error in closing the quorum device,
		// as we are anyway marking the device as inaccessible.
		//
		_quorum_devices[qid].accessible = false;
		return (false);
	}

	//
	// For the given quorum device, update our information
	// about the devices owned by the cluster.
	//
	_cluster_owned_devices |= 1LL << (qid -1);

	//
	// An erstwhile non-accessible quorum device has become accessible.
	// Mark the quorum device as accessible.
	// Syslog a message and also generate a cluster event saying
	// the quorum device has come online; the cluster event is required
	// for consumption by Sun Cluster Manager (GUI).
	//
	_quorum_devices[qid].accessible = true;
	CMM_TRACE(("Erstwhile inaccessible QD %s (qid %d) is online now\n",
	    _quorum_devices[qid].pathname, qid));
	//
	// SCMSGS
	// @explanation
	// An erstwhile inaccessible quorum device has become accessible now.
	// This node has registered its key on the quorum device.
	// One of the cluster nodes has taken up ownership of the device.
	// The quorum device is considered online now.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) _quorum_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: Erstwhile inaccessible quorum device %s (qid %d) "
	    "is online now.", _quorum_devices[qid].pathname, qid);
	(void) sc_publish_event(ESC_CLUSTER_QUORUM_STATE_CHANGE,
	    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_QUORUM_NAME, SE_DATA_TYPE_STRING, _quorum_devices[qid].pathname,
	    CL_OLD_STATE, SE_DATA_TYPE_STRING, "offline",
	    CL_NEW_STATE, SE_DATA_TYPE_STRING, "online",
	    NULL);

	return (true);
}

//
// This function returns the current status of the nodes and quorum devices
// as seen by the quorum algorithm.
//
// The caller passes a pointer to a quorum::quorum_status_t IDL struct.
// This function dynamically allocates the quorum::quorum_status_t struct,
// which includes sequences for the list of nodes and quorum devices.
//
// It is the caller's responsibility to deallocate the memory for the struct.
// The memory for the lists associated with the struct will be freed
// automatically by the destructor.
//
void
quorum_algorithm_impl::quorum_get_status(
	quorum::quorum_status_t_out quorum_status,
	Environment &e)
{
	quorum_get_status_common(quorum_status, _cluster_owned_devices, e);
}

//
// This function is a counterpart of quorum_get_status() method.
// The difference is that unlike quorum_get_status(),
// this method checks the immediate/current status of the quorum devices
// and then reports back their status to the caller.
// In other words, this method tries to access any erstwhile non-accessible
// quorum device to see what is the present status of that quorum device,
// and not what its status was during the last CMM reconfiguration
// (as reported by quorum algorithm implementation object).
//
// Note that even if there is a difference between the immediate status
// of a quorum device and its status during last CMM reconfiguration
// (as reported by the quorum algorithm impl object), this method DOES NOT
// trigger a CMM reconfiguration to count in the new status of quorum devices.
// But, if there is such a difference in status of a quorum device,
// then this method will syslog appropriate messages and publish cluster events
// required for consumption by Sun Cluster Manager (GUI).
//
// CMM reconfigurations are expensive; so this method does not trigger
// a reconfiguration because an erstwhile online quorum device became offline,
// or an erstwhile offline quorum device became online.
//
// This IDL method differs from quorum_get_status() in another way :
// it throws a retry_needed exception if a CMM reconfiguration is in progress
// due to which the method could not get the status;
// the caller should retry calling this IDL method in that case.
// Else this IDL method does not throw the retry_needed exception,
// signifying that there was no CMM reconfiguration in progress
// and hence this method was able to do its work.
//
void
quorum_algorithm_impl::quorum_get_immediate_status(
	quorum::quorum_status_t_out quorum_status,
	Environment &e)
{
	quorum_device_id_t		qid;
	bool				lock_already_held;
	quorum::membership_bitmask_t	online_quorum_devices = 0;

	//
	// Grab the quorum config lock.
	// Check to see if CMM reconfiguration is in progress.
	// If yes, then leave lock, raise retry_needed exception and return.
	//
	lock_already_held = config_write_lock_if_not_locked();
	_ioctl_mutex.lock();

	if (cmm_impl::the().get_current_state() != CMM_END_WAIT) {
		//
		// CMM reconfiguration is in progress;
		// leave lock, raise retry_needed exception and return.
		//
		_ioctl_mutex.unlock();
		config_write_unlock(lock_already_held);
		quorum::retry_needed *exceptionp = new quorum::retry_needed();
		ASSERT(exceptionp != NULL);
		e.exception(exceptionp);
		return;
	}

	for (qid = 1; qid <= _max_qid; qid++) {

		Environment env;
		quorum::quorum_error_t q_error = quorum::QD_SUCCESS;

		if (CORBA::is_nil(_quorum_devices[qid].qd_v)) {
			//
			// Either no QD is configured with this qid,
			// or this QD is in maintenance state,
			// or we failed to instantiate a CORBA object
			// for this quorum device earlier, or this node
			// does not have a path to this quorum device.
			// We skip this quorum device; do not try to
			// open/access the device.
			//
			// However, we are reporting the status of
			// quorum devices here. It is possible that
			// we failed to access this quorum device,
			// but some other node in cluster membership
			// is owning it. If that is the case,
			// we mark this device as online to report status
			// as this device is marked online in the quorum
			// information recorded in the quorum algorithm;
			// and we have no better way to know the current
			// status of the device since we cannot access
			// the quorum device from the current node.
			//

			if (_cluster_owned_devices & (1LL << (qid - 1))) {
				online_quorum_devices |= 1LL << (qid -1);
			}

			ASSERT(!_quorum_devices[qid].accessible);
			continue;
		}

		if (!_quorum_devices[qid].accessible) {
			//
			// Try accessing any quorum device that is marked
			// not accessible. It is possible that an erstwhile
			// non-accessible quorum device has become
			// accessible now.
			//
			if (!offline_qd_is_online_now(qid)) {
				//
				// We had earlier tried to open the device and
				// had failed. We failed to open the device
				// yet again. So we will keep the device marked
				// as not accessible. But, we will not drop
				// the reference to the device.
				// We will re-try opening this quorum device
				// during our next CMM reconfiguration.
				//
				ASSERT(!_quorum_devices[qid].accessible);
				// Skip this quorum device.
				continue;
			} else {
				//
				// The quorum device should have been
				// marked accessible now.
				//
				ASSERT(_quorum_devices[qid].accessible);
			}
		} else {

			//
			// Check the current status of an erstwhile online QD.
			// We change the message tracing level of the QD
			// temporarily while reading keys from the QD,
			// and then restore back the original tracing level.
			// This is because we are operating on the kernel
			// quorum device CORBA objects which normally have
			// their tracing level set to CMM_GREEN; which means
			// an operation like reading keys will trace
			// all messages into CMM trace buffer.
			// We do not want to do that;
			// instead we just want to trace errors as this
			// method could be called multiple times.
			//

			// Get the current tracing level of the QD object
			cmm_trace_level trace_level = (cmm_trace_level)
			    (_quorum_devices[qid].qd_v->
			    get_message_tracing_level(env));
			// This IDL method does not throw any exceptions
			CL_PANIC(env.exception() == NULL);

			//
			// Set the tracing level to CMM_AMBER;
			// trace unusual events and errors only
			//
			_quorum_devices[qid].qd_v->set_message_tracing_level(
			    CMM_AMBER, env);
			// This IDL method does not throw any exceptions
			CL_PANIC(env.exception() == NULL);

			//
			// Read the keys from the QD
			//
			quorum::reservation_list_t key_list;
			key_list.listlen = 0;
			key_list.listsize = NODEID_MAX;
			key_list.keylist.length(NODEID_MAX);
			q_error = _quorum_devices[qid].qd_v->
			    quorum_read_keys(key_list, env);
			// quorum_read_keys() does not throw any exceptions
			CL_PANIC(env.exception() == NULL);

			//
			// First reset the tracing level of the QD object
			// to the original value. Then check for the return
			// code from the read keys operation.
			// Thus any further action will trace messages
			// according to the original tracing level.
			//
			_quorum_devices[qid].qd_v->set_message_tracing_level(
			    trace_level, env);
			// This IDL method does not throw any exceptions
			CL_PANIC(env.exception() == NULL);

			// Now check for the return code of read keys operation
			if (q_error != quorum::QD_SUCCESS) {
				//
				// An erstwhile online quorum device is not
				// accessible now. Disable failfast on it,
				// close the device and mark it inaccessible.
				// Also syslog a message and publish
				// a cluster event saying that the QD was
				// online but is now offline.
				//
				online_qd_is_offline_now(qid);

				//
				// Skip this quorum device
				// while reporting online quorum devices.
				//
				continue;
			}

			//
			// Check if registration key for this node is found
			// on quorum device
			//
			uint_t index;
			bool reg_key_found = false;
			for (index = 0; index < key_list.listlen; index++) {
				quorum::registration_key_t *reg_key;
				reg_key = (quorum::registration_key_t *)
				    (&key_list.keylist[index]);
				if (null_idl_key(reg_key)) {
					continue; // ignore null key
				}
				if (match_idl_keys(reg_key,
				    (quorum::registration_key_t *)
				    (&RESERVATION_KEY(_my_nodeid)))) {
					reg_key_found = true;
					break;
				}
			}

			if (!reg_key_found) {
				ASSERT(_quorum_devices[qid].pathname != NULL);
				CMM_TRACE(("Registration key for my nodeid %d "
				    "is not found on qd %s (qid = %d)\n",
				    _my_nodeid, _quorum_devices[qid].pathname,
				    qid));
				//
				// An erstwhile online quorum device is not
				// accessible now. Disable failfast on it,
				// close the device and mark it inaccessible.
				// Also syslog a message and publish
				// a cluster event saying that the QD was
				// online but is now offline.
				//
				online_qd_is_offline_now(qid);

				//
				// Skip this quorum device
				// while reporting online quorum devices.
				//
				continue;
			}
		}

		//
		// Consider this device to be marked with
		// online status for the caller
		//
		online_quorum_devices |= 1LL << (qid -1);
	}

	_ioctl_mutex.unlock();
	config_write_unlock(lock_already_held);

	// Get quorum status of nodes and quorum votes for quorum devices
	quorum_get_status_common(quorum_status, online_quorum_devices, e);
}

//
// This method gets the quorum status of:
// (i) all cluster nodes as reported in quorum algo impl object
// (ii) quorum devices (passed in argument)
// The quorum devices is considered as the set of online quorum devices.
//
void
quorum_algorithm_impl::quorum_get_status_common(
	quorum::quorum_status_t_out quorum_status,
	quorum::membership_bitmask_t quorum_devices,
	Environment &)
{
	nodeset		cluster_members(_last_known_cluster_members);
	nodeset		devices(quorum_devices);
	uint_t		num_entries, entrynum, i, j;

	quorum::quorum_status_t	*quorum_status_p = new quorum::quorum_status_t;
	quorum_status = quorum_status_p;

	// This method is a reader of the config information.
	config_read_lock();

	int required_quorum_votes = quorum(_total_configured_votes);
	if (required_quorum_votes == -1) {
		//
		// We could not determine what is the required number
		// of quorum votes for survival under the membership
		// properties being used by the cluster.
		//
		CMM_TRACE((
		    "quorum_get_status_common: quorum(%d) returned -1\n",
		    _total_configured_votes));
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Could not determine how many votes "
		    "constitute quorum.");
	}

	quorum_status_p->current_votes = _current_cluster_votes;
	quorum_status_p->configured_votes = _total_configured_votes;
	quorum_status_p->votes_needed_for_quorum =
	    (uint32_t)required_quorum_votes;

	//
	// Copy node status info.
	//

	//
	// The quorum table node array could have have "holes".
	// If the reservation key of node i is NULL, then node i is not
	// configured. Count the number of nodes that are configured.
	//
	num_entries = 0;
	for (i = 0; i < _quorum_table->nodes.listlen; i++) {
		if (!null_mhioc_key(
		    _quorum_table->nodes.list[i].reservation_key))
			num_entries++;
	}
	quorum_status_p->nodelist.length(num_entries);

	entrynum = 0;
	for (i = 0; i < _quorum_table->nodes.listlen; i++) {

		quorum::node_status_t	*status;
		node_info_t	*info;

		info = &_quorum_table->nodes.list[i];
		if (null_mhioc_key(info->reservation_key))
			continue; // node i is not configured

		status = &quorum_status_p->nodelist[entrynum];

		status->config.nid = info->nodenum;
		for (j = 0; j < MHIOC_RESV_KEY_SIZE; j++)
			status->config.reservation_key.key[j] =
				info->reservation_key.key[j];
		status->config.votes_configured = info->votes;
		if (cluster_members.contains(info->nodenum))
			status->state = quorum::QUORUM_STATE_ONLINE;
		else
			status->state = quorum::QUORUM_STATE_OFFLINE;

		entrynum++;
	}

	//
	// Copy quorum device status info.
	//

	//
	// The quorum device array could have have "holes".
	// If the gdevname of QD i is NULL, then QD i is not configured.
	//
	// Bug 4321176 - If the votecount is 0, then QD is not configured.
	// Count the number of QDs that are configured.
	//
	num_entries = 0;
	for (i = 0; i < _quorum_table->quorum_devices.listlen; i++) {
		if ((_quorum_table->quorum_devices.list[i].gdevname != NULL) &&
		    (_quorum_table->quorum_devices.list[i].votes != 0))
			num_entries++;
	}
	quorum_status_p->quorum_device_list.length(num_entries);

	entrynum = 0;
	for (i = 0; i < _quorum_table->quorum_devices.listlen; i++) {

		quorum::quorum_device_status_t	*status;
		quorum_device_info_t		*info;

		info = &_quorum_table->quorum_devices.list[i];
		if (info->gdevname == NULL)
			continue; // QD i is not configured
		if (info->votes == 0)
			continue; // QD with 0 votecounts do not count

		status = &quorum_status_p->quorum_device_list[entrynum];

		status->config.qid = info->qid;
		status->config.votes_configured = info->votes;
		status->config.gdevname = (const char *)info->gdevname;
		status->config.nodes_with_enabled_paths =
			info->nodes_with_enabled_paths;
		status->config.nodes_with_configured_paths =
			info->nodes_with_configured_paths;
		if (devices.contains(info->qid)) {
			status->state = quorum::QUORUM_STATE_ONLINE;
			status->reservation_owner =
				_quorum_devices[info->qid].owner;
		} else {
			status->state = quorum::QUORUM_STATE_OFFLINE;
			status->reservation_owner = NODEID_UNKNOWN;
		}

		entrynum++;
	}

	config_read_unlock();
}

//
// This function should be called when the quorum configuration
// needs to be updated. It does a validity check of the
// proposed configuration and returns an error code if the
// quorum changes are illegal or unsafe.
//
// Note that if this is called from quorum_table_updated (as part of
// ccr_refreshed) any error codes returned will be ignored. This is because
// many types of changes (large, multiple, etc.) are allowed as part of
// ccr_refreshed since the ccr_refreshed may be communicating a number of
// quorum operations that occured while this node was out of the cluster.
//
// Checks for QUORUM_CONFIG_WILL_LOSE_QUORUM must always be made
// *BEFORE* checks for QUORUM_CONFIG_LARGE_CHANGE and
// QUORUM_CONFIG_MULTIPLE_CHANGES.
//
// Returns:
//	QUORUM_CONFIG_NO_CHANGE
//		there is no change in the quorum configuration
//	QUORUM_CONFIG_SAFE_CHANGE
//		one or more changes, but the changes are safe; any number of
//		additions/deletions of 0-vote nodes/devices and no more than
//		one vote change; largest vote change is <= 1
//	QUORUM_CONFIG_LARGE_CHANGE
//		largest vote change is > 1
//	QUORUM_CONFIG_MULTIPLE_CHANGES
//		the largest vote change is by one vote, but there is more
//		than one such change
//	QUORUM_CONFIG_WILL_LOSE_QUORUM
//		the change will result in the cluster losing quorum and
//		aborting
//	QUORUM_CONFIG_ILLEGAL
//		if the proposed change is illegal (such as invalid IDs etc.)
//
quorum::quorum_config_error_t
quorum_algorithm_impl::quorum_check_transition(
	const quorum::node_config_list_t&		nodelist,
	const quorum::quorum_device_config_list_t&	devicelist,
	Environment &)
{
	bool				config_changed = false;
	nodeset				current_nodes(
						_last_known_cluster_members);
	nodeid_t			nid;
	quorum_device_id_t		qid;
	uint_t				max_index,
					new_configured_votes = 0,
					new_owned_votes = 0,
					vote_change,
					num_changes = 0,
					max_change = 0,
					ovote,
					nvote,
					i;

	//
	// new_required_quorum_votes is not unsigned int, as it stores
	// the return value of quorum() function, which can return -1
	// in case of any error.
	//
	int				new_required_quorum_votes = 0;

	quorum::quorum_config_error_t	error = quorum::QUORUM_CONFIG_NO_CHANGE;

	CMM_TRACE(("quorum_check_transition called; proposed config:\n"));

	for (i = 0; i < nodelist.length(); i++) {
		struct mhioc_resv_key new_key;
		for (uint_t j = 0; j < MHIOC_RESV_KEY_SIZE; j++)
			new_key.key[j] = nodelist[i].reservation_key.key[j];
		if (null_mhioc_key(new_key))
			continue; // hole in node IDs
		CMM_TRACE(("node %d: vote = %d\n",
			nodelist[i].nid, nodelist[i].votes_configured));
	}
	for (i = 0; i < devicelist.length(); i++) {
		if ((const char *)devicelist[i].gdevname == NULL)
			continue; // hole in QIDs
		// Although the quorum algorithm does not fence QDs
		// that are configured with a votecount of 0, trace
		// the existence of such QDs for debugging purposes.
		CMM_TRACE(("QD %d: vote = %d, name = %s, "
			"nodes_with_configured_paths = 0x%llx\n",
			devicelist[i].qid, devicelist[i].votes_configured,
			(const char *)devicelist[i].gdevname,
			devicelist[i].nodes_with_configured_paths));
	}

	// This method is a reader of the quorum configuration information.
	config_read_lock();

	while (!_are_qd_owners_set) {
		config_read_wait(&_qd_owners_cv);
	}
	//
	// First check the proposed config for consistency.
	//
	if ((nodelist.length() < 1) ||
	    (nodelist.length() > NODEID_MAX) ||
	    (devicelist.length() > MAX_QUORUM_DEVICES)) {
		error = quorum::QUORUM_CONFIG_ILLEGAL;
		goto done;
	}
	for (i = 0; i < nodelist.length(); i++) {
		nid = nodelist[i].nid;
		struct mhioc_resv_key new_key;

		for (uint_t j = 0; j < MHIOC_RESV_KEY_SIZE; j++)
			new_key.key[j] = nodelist[i].reservation_key.key[j];
		//
		// Node IDs should be in the valid range. Also, for
		// node ID holes, the vote count should be 0.
		//
		if ((nid == NODEID_UNKNOWN) || (nid > NODEID_MAX) ||
		    (null_mhioc_key(new_key) &&
		    (nodelist[i].votes_configured != 0))) {
			error = quorum::QUORUM_CONFIG_ILLEGAL;
			goto done;
		}
	}
	for (i = 0; i < devicelist.length(); i++) {
		qid = devicelist[i].qid;
		//
		// QIDs should be in the valid range. Also, for
		// QID holes, the vote count should be 0.
		//
		if ((qid == 0) || (qid > MAX_QUORUM_DEVICES) ||
		    (((const char *)devicelist[i].gdevname == NULL) &&
		    (devicelist[i].votes_configured != 0))) {
			error = quorum::QUORUM_CONFIG_ILLEGAL;
			goto done;
		}
	}

	//
	// Now, check the proposed config against the existing config.
	//

	max_index = nodelist.length();
	max_index = (max_index > _max_nodeid) ? max_index : _max_nodeid;

	for (i = 0; i < max_index; i++) {
		if (i < nodelist.length()) {
			nid = nodelist[i].nid;
			nvote = nodelist[i].votes_configured;
			if (nid > _max_nodeid) {
			    ovote = 0; // newly configured node
			    config_changed = true;
			} else {
			    struct mhioc_resv_key *old_key, new_key;

			    ovote = _quorum_table->nodes.list[nid-1].votes;

			    //
			    // Check if the reservation key has changed.
			    // This can happen if a node is removed, creating
			    // a node ID "hole" or a node ID hole is being
			    // plugged with a new node.
			    //
			    // We do not allow the key of a configured node
			    // to be changed.
			    //
			    old_key = &RESERVATION_KEY(nid);
			    for (uint_t j = 0; j < MHIOC_RESV_KEY_SIZE; j++)
				new_key.key[j] = nodelist[i].
				    reservation_key.key[j];

			    if (!match_mhioc_keys(*old_key, new_key)) {
				config_changed = true;
				if (null_mhioc_key(new_key)) {
				    CMM_TRACE(("Deleting node %d\n", nid));
				    if (nvote != 0) {
					error = quorum::QUORUM_CONFIG_ILLEGAL;
					goto done;
				    }
				} else if (null_mhioc_key(*old_key))
				    CMM_TRACE(("Adding node %d\n", nid));
				else {
				    CMM_TRACE(("Changing key for node %d\n",
					nid));
				    error = quorum::QUORUM_CONFIG_ILLEGAL;
				    goto done;
				}
			    }
			}
		} else {
			nid = _quorum_table->nodes.list[i].nodenum;
			ovote = _quorum_table->nodes.list[i].votes;
			nvote = 0; // node being removed
			config_changed = true;
		}

		new_configured_votes += nvote;
		if (current_nodes.contains(nid))
			new_owned_votes += nvote;

		if (ovote == nvote)
			continue;

		CMM_TRACE(("Changing vote for node %d "
			"from %d to %d\n", nid, ovote, nvote));

		config_changed = true;
		num_changes++;
		vote_change = (nvote > ovote) ? (nvote-ovote) : (ovote-nvote);
		if (vote_change > max_change)
			max_change = vote_change;
	}

	max_index = devicelist.length();
	max_index = (max_index > _max_qid) ? max_index : _max_qid;

	//
	// Only compute QD contribution to quorum if the node has information
	// about quorum device ownership. Hold onto the ownership lock until
	// finished with check.
	//

	for (i = 0; i < max_index; i++) {
		if (i < devicelist.length()) {
			qid = devicelist[i].qid;
			nvote = devicelist[i].votes_configured;
			if (qid > _max_qid) {
			    ovote = 0; // newly configured device
			    CMM_TRACE(("Adding QD %d\n", qid));
			    config_changed = true;
			    if (nvote != 0) {
				error = quorum::QUORUM_CONFIG_ILLEGAL;
				goto done;
			    }
			} else {
			    //
			    // Existing device ID
			    //
			    quorum_device_info_t *old_config =
				    &_quorum_table->quorum_devices.list[qid-1];
			    const char *old_name, *new_name;

			    old_name = (const char *)old_config->gdevname;
			    new_name = (const char *)devicelist[i].gdevname;

			    if ((old_name == NULL) && (new_name == NULL)) {
				//
				// No device at this device ID ==> "hole" in
				// configured device IDs
				//
				continue;
			    }

			    ovote = old_config->votes;

			    if (old_name == NULL) {
				CMM_TRACE(("Adding QD %d\n", qid));
				config_changed = true;
				if (nvote != 0) {
				    error = quorum::QUORUM_CONFIG_ILLEGAL;
				    goto done;
				}
			    } else if (new_name == NULL) {
				CMM_TRACE(("Deleting QD %d\n", qid));
				config_changed = true;
				if (nvote != 0) {
				    error = quorum::QUORUM_CONFIG_ILLEGAL;
				    goto done;
				}
			    } else if (os::strcmp(old_name, new_name) != 0) {
				    CMM_TRACE(("Changing gdevname for "
					"QD %d from %s to %s\n", qid,
					old_name, new_name));
				    error = quorum::QUORUM_CONFIG_ILLEGAL;
				    goto done;
			    }

			    if ((old_config->nodes_with_configured_paths !=
				devicelist[i].nodes_with_configured_paths) &&
				(ovote != 0)) {
				//
				// Changes in QD connectivity are not
				// currently allowed.
				//
				CMM_TRACE(("Changing connectivity for "
				    "QD %d from 0x%llx to 0x%llx\n", qid,
				    old_config->nodes_with_configured_paths,
				    devicelist[i].nodes_with_configured_paths));
				error = quorum::QUORUM_CONFIG_ILLEGAL;
				goto done;
			    }

			}
		} else {
			qid = _quorum_table->quorum_devices.list[i].qid;
			ovote = _quorum_table->quorum_devices.list[i].votes;
			nvote = 0; // device being removed
			config_changed = true;
		}

		new_configured_votes += nvote;

		if (qid <= _max_qid) {
			//
			// If the device is currently owned by one of the nodes
			// in the cluster, we can count its votes.
			//
			if (_quorum_devices[qid].owner != NODEID_UNKNOWN) {
				new_owned_votes += nvote;
			}

			//
			// Adding a QD for the first time. We won't have owner
			// information about it, since we don't fence QD's with
			// a vote of 0. Call the disk path monitoring daemon
			// to get information about whether we are likely to
			// be able to acquire the device.
			//
			else if ((ovote == 0) && (nvote >= 1)) {
				//
				// If here, we are sure that the quorum device
				// information exists in _quorum_table.
				// Find out if the quorum device is
				// a shared disk or not; we might need to query
				// Disk Path Monitor if the quorum device
				// is a shared disk.
				//
				// Check the "access_mode" specified for
				// the quorum device in the infrastructure
				// table in CCR. If "access_mode" is not
				// specified, check the "type" specified
				// for the quorum device in
				// the infrastructure table in CCR.
				//
				char *type_strp = NULL;
				quorum_device_info_t *old_configp =
				    &_quorum_table->quorum_devices.list[qid-1];
				if (old_configp->access_mode != NULL) {
					type_strp = old_configp->access_mode;
				} else {
					CL_PANIC(old_configp->type != NULL);
					type_strp = old_configp->type;
				}
				if ((os::strcmp(type_strp, "scsi2") == 0) ||
				    (os::strcmp(type_strp, "scsi3") == 0) ||
				    (os::strcmp(type_strp, "sq_disk") == 0)) {
					if (get_disk_status_from_dpm(
					    (const char *)devicelist[i].
					    gdevname, devicelist[i].
					    nodes_with_configured_paths,
					    current_nodes)) {
						// Count the vote.
						CMM_TRACE(("Status OK. "
						    "Adding vote\n"));
						new_owned_votes += nvote;
					}
				}
			}
		}

		if (ovote == nvote) {
			continue;
		}

		CMM_TRACE(("Changing vote for QD %d from %d to %d\n",
		    qid, ovote, nvote));

		config_changed = true;
		num_changes++;
		vote_change = (nvote > ovote) ? (nvote-ovote) : (ovote-nvote);
		if (vote_change > max_change) {
			max_change = vote_change;
		}
	}

	new_required_quorum_votes = quorum(new_configured_votes);
	if (new_required_quorum_votes == -1) {
		// Error
		CMM_TRACE(("quorum_check_transition: quorum(%d) returned -1\n",
		    new_configured_votes));
		error = quorum::QUORUM_CONFIG_ILLEGAL;
	} else if (new_owned_votes < (uint_t)new_required_quorum_votes) {
		error = quorum::QUORUM_CONFIG_WILL_LOSE_QUORUM;
	} else if (max_change > 1) {
		error = quorum::QUORUM_CONFIG_LARGE_CHANGE;
	} else if (num_changes > 1) {
		error = quorum::QUORUM_CONFIG_MULTIPLE_CHANGES;
	} else if (config_changed) {
		error = quorum::QUORUM_CONFIG_SAFE_CHANGE;
	}

done:

	config_read_unlock();

	CMM_TRACE(("quorum_check_transition returning %d\n", error));
	return (error);
}

//
// Query the Disk Path Monitor for the status of a shared disk.
// Returns true if the status is OK; false otherwise.
//
bool
quorum_algorithm_impl::get_disk_status_from_dpm(const char *disk_namep,
    quorum::membership_bitmask_t nodes_with_configured_paths,
    nodeset current_nodes)
{
	CMM_TRACE(("get_disk_status_from_dpm: Attempting to get status of "
	    "disk %s from DPM\n", disk_namep));

	disk_path_monitor::dpm_disk_status_seq dpm_seq;
	disk_path_monitor::dpm_var dpm_v;
	bool is_alive;

	//
	// Choose a node to check the monitor status.
	// Start with the lowest numbered node that is connected to the QD
	// and look until we find one that has a valid connection.
	//
	// We don't check all connected nodes now. That will be implemented
	// as part of future quorum changes once disk path monitoring
	// is fully implemented including callbacks.
	//
	nodeset	n(nodes_with_configured_paths);
	nodeid_t id = 0;
	bool connection_found = false;

	while ((!connection_found) && ((id = id + 1) <= NODEID_MAX)) {
		if ((!n.contains(id)) || (!current_nodes.contains(id))) {
			continue;
		}

		// Get the reference to the monitor.
		char dpmd_name[10];
		os::sprintf(dpmd_name, "DPM.%d", (int)id);
		CMM_TRACE(("get_disk_status_from_dpm: "
		    "Looking up monitor %s\n", dpmd_name));

		naming::naming_context_var ctx_v = ns::root_nameserver();
		Environment e;
		CORBA::Object_var dpm_obj_v = ctx_v->resolve(dpmd_name, e);
		if (e.exception()) {
			// Assume that the monitor isn't running.
			CMM_TRACE(("get_disk_status_from_dpm: "
			    "Unable to resolve dpmd in nameserver.\n"));
			continue;
		}

		dpm_v = disk_path_monitor::dpm::_narrow(dpm_obj_v);

		// Check that the monitor is alive.
		is_alive = dpm_v->is_alive(e);
		if (e.exception()) {
			CMM_TRACE(("get_disk_status_from_dpm: "
			    "Exception checking alive status\n"));
			continue;
		} else if (!is_alive) {
			//
			// XXX Check for all nodes of the cluster
			// XXX once DPM monitoring is available.
			//
			CMM_TRACE(("get_disk_status_from_dpm: "
			    "DPM daemon is dead\n"));
			continue;
		}

		//
		// Construct the query sequence.
		// Do some string manipulation here since
		// the quorum algorithm stores disks in the dxs2
		// for VTOC disks format or simply dX for EFI disks format
		// and DPM expects queries of the form dxs0.
		//
		dpm_seq.length(1);
		dpm_seq[0].disk_path = (const char *)os::strdup(disk_namep);
#ifndef SC_EFI_SUPPORT
		dpm_seq[0].disk_path[
		    (unsigned)os::strlen(dpm_seq[0].disk_path) - 1] = '0';
#else
		size_t disk_path_len = os::strlen(dpm_seq[0].disk_path);
		if (dpm_seq[0].disk_path[
		    (unsigned)disk_path_len - 2] == 's') {
			dpm_seq[0].disk_path[
			    (unsigned)disk_path_len - 1] = '0';
		} else {
			os:strcat(dpm_seq[0].disk_path, "s0");
		}
#endif
		// DPM_PATH_UNKNOWN is 1<<3.
		dpm_seq[0].status = 1<<3;

		dpm_v->dpm_get_monitoring_status(dpm_seq, B_FALSE, e);
		if (e.exception()) {
			// Check for error.
			CMM_TRACE(("get_disk_status_from_dpm: "
			    "Exception in call to get status\n"));
			continue;
		}

		// Status of 1 == DPM_PATH_OK.
		if (dpm_seq[0].status & 1) {
			connection_found = true;
		} else {
			CMM_TRACE(("get_disk_status_from_dpm: "
			    "DPM returned status %d\n", dpm_seq[0].status));
		}
	}

	CMM_TRACE(("get_disk_status_from_dpm: returning status %s "
	    "for disk %s\n", (connection_found ? "OK" : "NOT OK"), disk_namep));
	return (connection_found);
}

//
// This function is called when the CCR infrastructure
// table (containing the quorum configuration) is updated.
// This function first checks if the quorum configuration has
// changed. If so, it forces the node to read the new configuration
// and generate events for the changes. The node that initiates the
// config change (through scconf) forces a CMM reconfiguration after
// the change has been committed on all the cluster nodes. This
// reconfiguration forces the cluster to collectively process the
// new quorum config info. If the cluster then loses quorum,
// it will abort. This is one reason why the quorum_check_transition()
// function should be called before committing a quorum configuration
// change. Another reason for calling quorum_check_transition() before
// this routine is called is that this routine panics if the quorum
// configuration cannot be read.
//
void
quorum_algorithm_impl::quorum_table_updated(Environment &e)
{
	quorum_table_t				*qt;
	quorum::node_config_list_t		nodelist;
	quorum::quorum_device_config_list_t	devicelist;
	int					error;
	uint_t					i, j;

	CMM_TRACE(("quorum_table_updated called\n"));

	//
	// Check if the quorum configuration has changed.
	//
	error = clconf_get_quorum_table(NULL /* recently updated */, &qt);
	if (error) {
		//
		// quorum_check_transition() must have been called before
		// this routine is called. That routine verifies that the
		// quorum table can be read. If we get an error now, the
		// quorum configuration must have been (manually?) changed
		// since quorum_check_transition() was called. It is OK
		// to panic here.
		//

		//
		// SCMSGS
		// @explanation
		// The specified error was encountered while trying to read
		// the quorum information from the CCR. This is probably
		// because the CCR tables were modified by hand, which is an
		// unsupported operation. The node will panic.
		// @user_action
		// Reboot the node in non-cluster (-x) mode and restore the
		// CCR tables from the other nodes in the cluster or from
		// backup. Reboot the node back in cluster mode. The problem
		// should not reappear.
		//
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Unable to read quorum information. "
		    "Error = %d.", error);
	}

	nodelist.length(qt->nodes.listlen);
	devicelist.length(qt->quorum_devices.listlen);
	for (i = 0; i < qt->nodes.listlen; i++) {
		nodelist[i].nid = qt->nodes.list[i].nodenum;
		for (j = 0; j < MHIOC_RESV_KEY_SIZE; j++) {
			nodelist[i].reservation_key.key[j] =
				qt->nodes.list[i].reservation_key.key[j];
		}
		nodelist[i].votes_configured = qt->nodes.list[i].votes;
	}
	for (i = 0; i < qt->quorum_devices.listlen; i++) {
		devicelist[i].qid = qt->quorum_devices.list[i].qid;
		devicelist[i].votes_configured =
			qt->quorum_devices.list[i].votes;
		devicelist[i].gdevname = (const char *)
			qt->quorum_devices.list[i].gdevname;
		devicelist[i].nodes_with_enabled_paths =
			qt->quorum_devices.list[i].
			nodes_with_enabled_paths;
		devicelist[i].nodes_with_configured_paths =
			qt->quorum_devices.list[i].
			nodes_with_configured_paths;
	}

	// Check to see if this method was called as a result of quorum
	// change or just some other infrastrcuture file change.
	error = quorum_check_transition(nodelist, devicelist, e);

	// quorum_check_transition() does not do anything with e above,
	// so that should be null.
	CL_PANIC(e.exception() == NULL);

	// If no change in quorum, return.
	// The quorum_check_transition() may return errors at this point,
	// but that is ok here because the quorum changes may be because
	// of CCR recovery in which case the changes may not be "safe".
	// For online updates, the check method is called *before* making
	// the change to the CCR so those changes are already checked.
	if (error == quorum::QUORUM_CONFIG_NO_CHANGE) {
		CMM_TRACE(("No changes to quorum config\n"));
		clconf_release_quorum_table(qt);
		return;
	}

	//
	// Now, re-initialize the quorum configuration using the new table.
	//
	_quorum_table_updated = true;
	initialize_quorum_info(*qt);

	//
	// If this function is called because the CCR we booted with is
	// stale and has been updated using a more recent CCR from another
	// node, force a reconfiguration so that this node can get info about
	// quorum devices this node is not connected to. If this function is
	// called due to an operator running scconf to add a quorum disk,
	// the reconfiguration is forced by scconf.
	//
	if (!_ccr_is_refreshed) {
		cmm::control_var cmm_cntl_v = cmm_ns::get_control(cmm_name);
		cmm_cntl_v->reconfigure(e);
		CL_PANIC(e.exception() == NULL);  // Should never have error.
	}
}


//
// This functionality REMOVED as part of Quorum API. Functionality is
// now provided through clconf calling the type_registry.
//
// This function is called by scconf to scrub a SCSI device to be configured
// as a dual ported device. The scrubbing op will initialize the PGRE section
// on the alternate cylinders on the disk.
//
void
quorum_algorithm_impl::quorum_device_scrub(
    const char 	*,
    Environment	&)
{
	CL_PANIC(0);
}


//
// static functions
//

//
// This static function is called from ORB::initialize and is responsible
// for allocating the one and only quorum algorithm object that exists
// in each node. This function also binds the name of the quorum
// algorithm object to the local name server.
//
// static
int
quorum_algorithm_impl::quorum_algorithm_init()
{
	naming::naming_context_var	ctx_v;
	quorum_table_t			*quorum_table;
	os::sc_syslog_msg		*syslog_msgp;
	char				nodename[8];
	int				error;
	Environment			e;

	CMM_TRACE(("quorum_algorithm_init called\n"));
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());
	syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_CMM_NODE_TAG,
		nodename, NULL);

	if (the_quorum_algorithm_p != NULL) {
		//
		// SCMSGS
		// @explanation
		// This is an internal error during node initialization, and
		// the system can not continue.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"INTERNAL ERROR CMM: quorum_algorithm_init "
			"called already.");
		delete syslog_msgp;
		return (EINVAL);
	}

	//
	// Check the validity of the quorum configuration.
	//
	error = clconf_get_quorum_table(NULL /* current */, &quorum_table);
	if (error) {
		//
		// SCMSGS
		// @explanation
		// The node encountered an internal error during
		// initialization of the quorum subsystem object.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"INTERNAL ERROR CMM: clconf_get_quorum_table() "
			"returned error %d.", error);
		delete syslog_msgp;
		return (EINVAL);
	}

	the_quorump = new quorum_algorithm_impl(*quorum_table);
	the_quorum_algorithm_p = the_quorump->get_objref();

	CMM_TRACE(("Binding quorum to nameserver\n"));

	//
	// Bind the quorum algorithm object to the local name server
	// so that other modules can look it up.
	//
	ctx_v = ns::local_nameserver();
	ctx_v->rebind(quorum_name, the_quorum_algorithm_p, e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// There was an error while binding the quorum subsystem
		// object to the local name server.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"INTERNAL ERROR CMM: Cannot bind quorum algorithm "
			"object to local name server.");
		delete syslog_msgp;
		CORBA::release(the_quorum_algorithm_p);
		return (EINVAL);
	}

	CMM_TRACE(("'quorum' bound to local name server\n"));

	delete syslog_msgp;
	return (0);
}

//
// This function will return the reservation key for
// the local node.
//
// static
//
mhioc_resv_key_t
quorum_algorithm_impl::get_my_reservation_key()
{
	ASSERT(the_quorump != NULL);

	mhioc_resv_key_t mykey;

	nodeid_t nid = the_quorump->_my_nodeid;

	if (null_mhioc_key(the_quorump->_quorum_table->
	    nodes.list[nid - 1].reservation_key)) {
		//
		// If this assert is hit, means that the
		// key is not yet initialized in the quorum
		// subsystem.
		//
		ASSERT(0);
	}

	return (the_quorump->_quorum_table->
	    nodes.list[nid - 1].reservation_key);
}

//
// Internal use functions
//

//
// Function to read the quorum configuration from the CCR and
// initialize related variables.
//
// Called when the quorum configuration is changed (by updating the
// CCR infrastructure table) as well as when the node is booting.
//
void
quorum_algorithm_impl::initialize_quorum_info(
	quorum_table_t& new_qt)
{
	char				name_str[8];
	bool				quorum_devices_accessible;
	uint_t				i;
	bool				lock_already_held;

	CMM_TRACE(("In initialize_quorum_info\n"));
	lock_already_held = config_write_lock_if_not_locked();

	//
	// There must be at least the local node in the quorum config.
	//
	CL_PANIC(new_qt.nodes.listlen > 0);

	//
	// Compare the old configuration and the new configuration
	// and generate events for the differences.
	//

	// Check the cluster nodes.
	initialize_node_quorum_info(new_qt);

	// Count the node votes
	_total_configured_votes = 0;
	for (i = 0; i < new_qt.nodes.listlen; i++) {
		_total_configured_votes += new_qt.nodes.list[i].votes;
	}

	//
	// We are about to remove ownership information about quorum
	// devices. So, force all calls to quorum_check_transition to
	// wait until ownership information is restored.
	//
	if (_max_qid != 0) {
		_are_qd_owners_set = false;
	}

	// Check the quorum devices and obtain references to new devices added.
	quorum_info_t *new_quorum_devices = NULL;
	quorum_devices_accessible =
	    initialize_device_quorum_info(new_qt, new_quorum_devices);

	// Old quorum info is no longer needed. Deallocate it.
	CMM_TRACE(("Deleting old quorum info\n"));
	delete_quorum_info();

	// Now, re-initialize the quorum configuration using the new table.
	_quorum_table = &new_qt;
	_max_nodeid = _quorum_table->nodes.listlen;
	_max_qid = _quorum_table->quorum_devices.listlen;

	CL_PANIC(_max_nodeid > 0);
	CL_PANIC((_my_nodeid > 0) && (_my_nodeid <= _max_nodeid));

	_cluster_owned_devices = 0;

	_quorum_devices_owned = new membership_bitmask_t[_max_nodeid + 1];
	for (i = 1; i <= _max_nodeid; i++) {
		_quorum_devices_owned[i] = 0;
	}

	_quorum_devices = new_quorum_devices;

	if (_max_qid != 0) {
		idl_reservation_list.listsize = 1;
		idl_reservation_list.listlen = 0;
		idl_reservation_list.keylist.length(1);

		idl_registration_list.listsize = _max_nodeid;
		idl_registration_list.listlen = 0;
		idl_registration_list.keylist.length(_max_nodeid);
	}

	my_key = (quorum::registration_key_t *)(&MY_RESERVATION_KEY);

	print_quorum_table(*_quorum_table);

	//
	// If any quorum devices have been added, we should try to
	// place reservation keys on the devices (if our CCR is up
	// to date) and try to take ownership of those devices if
	// possible. Taking ownership at this point will eliminate
	// the need to do so during the next reconfiguration.
	//
	// Bug 4321324 - However only do the above is the node is
	// already part of the cluster as opposed to just booting up.
	// In the case of a node booting up, it may find itself
	// registered, but may get preempted from the QD if the node
	// has not talked to the other nodes (which have formed a
	// cluster without this node as part of the first reconfig).
	// Then when this node starts talking to those cluster
	// members, and another reconfig is started, it will find
	// itself preempted from the QD and will falsely conclude
	// that the proposed membership has been preempted leading
	// to the cluster aborting. Note that this kind of problem
	// can not happen with online updates, since then the node
	// is already a cluster member, and its preemption from any
	// QD implies that it has been ousted from cluster membership
	// and must reboot in order to re-join.
	//
	if (_ccr_is_refreshed) {
		//
		// This can only be true if we are in this routine
		// due to an online update to the CCR quorum config.
		//
		if (quorum_devices_accessible) {
			//
			// One or more quorum devices have become
			// accessible for the first time, so this
			// node can register its keys on those
			// devices.
			//
			Environment e;
			ccr_refreshed(e);
			CL_PANIC(e.exception() == NULL);
		}

		//
		// This method has reset the quorum devices ownership
		// information above, assuming that the reconfiguration
		// triggered because of the online update will recompute
		// this information. However, that reconfiguration will
		// take place asynchronously, and may not take place
		// in time when this ownership information is needed
		// next (say by the next quorum_check_transition() for
		// incrementing the votecount from 1 to 2). To update
		// the ownership information in a timely manner, do it
		// at this point. Fix for bug 4337814.
		// XXX - Consider reevaluating need for this due
		// to more general fix for bug 4399064.
		//

		quorum::quorum_result_t		quorum_result;

		//
		// Online update implies that the node is part of
		// the cluster. Ie the automaton must have invoked
		// acquire_quorum_devices() from begin_state, that
		// method in turn sets _last_known_cluster_members.
		//
		CL_PANIC(_last_known_cluster_members);

		_ioctl_mutex.lock();
		acquire_quorum_devices_lock_held(
			_last_known_cluster_members, quorum_result);
		_ioctl_mutex.unlock();

		_quorum_devices_owned[_my_nodeid] =
			quorum_result.quorum_devices_owned;
	}
	config_write_unlock(lock_already_held);
}

//
// Function to initialize quorum-related node information.
//
// Called when the quorum configuration is changed (by updating the
// CCR infrastructure table) as well as when the node is booting.
//
void
quorum_algorithm_impl::initialize_node_quorum_info(
	quorum_table_t& new_qt)
{
	uint_t	i;
	char	*nodename = NULL;

	CMM_TRACE(("initialize_node_quorum_info called\n"));

	// allocate memory for storing nodenames
	nodename = new char [CL_MAX_LEN+1];
	ASSERT(nodename != NULL);

	//
	// Check node quorum info
	//
	for (i = 1; i <= NODEID_MAX; i++) {
		if ((i > _max_nodeid) && (i > new_qt.nodes.listlen)) {
			break; // no more nodes to look at
		}

		clconf_get_nodename(i, nodename);

		if ((i > _max_nodeid) &&
		    (!null_mhioc_key(new_qt.nodes.list[i-1].reservation_key))) {
			//
			// New node added to configuration
			//

			//
			// SCMSGS
			// @explanation
			// The specified node with the specified votecount has
			// been added to the cluster.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) _quorum_syslog_msgp->log(SC_SYSLOG_NOTICE, ADDED,
			    "CMM: Node %s (nodeid = %d) with votecount = %d "
			    "added.", nodename, i,
			    new_qt.nodes.list[i-1].votes);

			// publish sysevent
			(void) sc_publish_event(ESC_CLUSTER_NODE_CONFIG_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
			    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			    CL_EVENT_CONFIG_ADDED,
			    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32,
			    new_qt.nodes.list[i-1].votes,
			    NULL);
		} else if ((i > new_qt.nodes.listlen) &&
		    (!null_mhioc_key(RESERVATION_KEY(i)))) {
			//
			// Node deleted from configuration
			//

			//
			// SCMSGS
			// @explanation
			// The specified node with the specified votecount has
			// been removed from the cluster.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_NOTICE, REMOVED,
			    "CMM: Node %s (nodeid = %d) with votecount = %d "
			    "removed.", nodename, i,
			    _quorum_table->nodes.list[i-1].votes);

			// publish sysevent
			(void) sc_publish_event(ESC_CLUSTER_NODE_CONFIG_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
			    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			    CL_EVENT_CONFIG_REMOVED,
			    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32,
			    _quorum_table->nodes.list[i-1].votes,
			    NULL);
		} else if ((i <= _max_nodeid) && (i <= new_qt.nodes.listlen)) {
			// An existing node/hole information has been changed.
			node_info_t *old_node, *new_node;
			old_node = &_quorum_table->nodes.list[i-1];
			new_node = &new_qt.nodes.list[i-1];

			// No action reqd if a previous hole is still a hole.
			if ((null_mhioc_key(old_node->reservation_key)) &&
			    (null_mhioc_key(new_node->reservation_key))) {
				continue;
			} else if (match_mhioc_keys(old_node->reservation_key,
			    new_node->reservation_key)) {
				//
				// Non-null keys match => existing node
				// Check if node vote count has changed.
				//
				if (old_node->votes != new_node->votes) {
					//
					// SCMSGS
					// @explanation
					// The specified node's votecount has
					// been changed as indicated.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) _quorum_syslog_msgp->log(
					    SC_SYSLOG_NOTICE, PROPERTY_CHANGED,
					    "CMM: Votecount changed from %d to "
					    "%d for node %s.", old_node->votes,
					    new_node->votes, nodename);

					// publish sysevent
					(void) sc_publish_event(
					    ESC_CLUSTER_NODE_CONFIG_CHANGE,
					    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_NODE_NAME, SE_DATA_TYPE_STRING,
					    nodename,
					    CL_CONFIG_ACTION,
					    SE_DATA_TYPE_UINT32,
					    CL_EVENT_CONFIG_VOTE_CHANGE,
					    CL_VOTE_COUNT,
					    SE_DATA_TYPE_UINT32,
					    new_node->votes,
					    NULL);
				}
			} else if (null_mhioc_key(old_node->reservation_key)) {
				//
				// A node has been added to a "hole", ie a
				// new node has been added to configuration
				//
				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_NOTICE, ADDED,
				    "CMM: Node %s (nodeid = %d) with "
				    "votecount = %d added.",
				    nodename, i, new_node->votes);

				// publish sysevent
				(void) sc_publish_event(
				    ESC_CLUSTER_NODE_CONFIG_CHANGE,
				    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
				    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
				    CL_EVENT_CONFIG_ADDED,
				    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32,
				    new_node->votes,
				    NULL);
			} else if (null_mhioc_key(new_node->reservation_key)) {
				//
				// A node hole has been created, ie a
				// node has been deleted from configuration
				//
				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_NOTICE, REMOVED,
				    "CMM: Node %s (nodeid = %d) with "
				    "votecount = %d removed.",
				    nodename, i, old_node->votes);

				// publish sysevent
				(void) sc_publish_event(
				    ESC_CLUSTER_NODE_CONFIG_CHANGE,
				    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
				    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
				    CL_EVENT_CONFIG_REMOVED,
				    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32,
				    old_node->votes,
				    NULL);
			} else {
				//
				// Reservation key changed. This is not a
				// supported change, hence panic.
				//

				//
				// SCMSGS
				// @explanation
				// The reservation key for the specified node
				// was changed. This can only happen due to
				// the CCR infrastructure being changed by
				// hand, which is not a supported operation.
				// The system can not continue, and the node
				// will panic.
				// @user_action
				// Boot the node in non-cluster (-x) mode,
				// recover a good copy of the file
				// /etc/cluster/ccr/infrastructure from one of
				// the cluster nodes or from backup, and then
				// boot this node back in cluster mode. If all
				// nodes in the cluster exhibit this problem,
				// then boot them all in non-cluster mode,
				// make sure that the infrastructure files are
				// the same on all of them, and boot them back
				// in cluster mode. The problem should not
				// happen again.
				//
				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_PANIC, MESSAGE,
				    "CMM: Reservation key changed from %s "
				    "to %s for node %s (id = %d).",
				    old_node->reservation_key,
				    new_node->reservation_key,
				    nodename, i);
			}
		} else {
			// New or existing hole - ignore.
		}
	}

	delete [] nodename;
}

//
// When the quorum algorithm object refreshes its quorum configuration
// information from CCR, this method is called for each new quorum device
// that is found in the CCR but was not a part of the older quorum configuration
// information in the quorum algorithm object.
//
// This method adds the device to the quorum configuration information
// in the quorum algorithm object, only if the votecount of the device
// is non-zero. A CORBA quorum device type object reference is also obtained
// from the type registry for the new quorum device. The quorum algorithm object
// stores this reference as part of its quorum configuration information.
// This method also adds on the votecount of the new quorum device to the total
// configured votecount maintained in the quorum configuration information.
//
// This method syslogs messages and publishes sysevents
// for the quorum device addition.
//
void
quorum_algorithm_impl::quorum_device_added(
    const quorum_device_info_t *new_qd,
    bool &quorum_devices_accessible,
    quorum_info_t *&new_quorum_devices)
{
	nodeset			nodes_with_paths;
	quorum_device_id_t	qid;

	//
	//
	// Bug 4321176 - Ignore QDs with votecount of 0 since
	// the quorum algorithm will not do the fencing for these.
	//
	// Only consider this QD if its votecount > 0.
	// Currently the votecount for a just added QD can not be > 0
	// since scconf first adds the device to the infrastructure file,
	// and then increments the votecount one by one.
	// But it could change in future to do the first increment
	// with the addition of the QD to the infrastructure file.
	// Also when a node is booting up, it will see the QDs already
	// configured as new ones while booting up.
	//
	if (new_qd->votes == 0) {
		return;
	}

	nodes_with_paths.set(new_qd->nodes_with_configured_paths);
	qid = new_qd->qid;
	_total_configured_votes += new_qd->votes;

	//
	// Does the local node have a configured path?
	// This should be true for all nodes to which the QD is ported
	// by scconf, both during bootup and for online updates,
	// since scconf does not allow adding configured paths later.
	//
	if (nodes_with_paths.contains(_my_nodeid)) {

		// This node has a path to this quorum device.
		quorum_devices_accessible = true;

		//
		// Open the device.
		//
		// This quorum device operation does not need to be protected
		// by the ioctl_mutex since no other thread can be doing
		// any quorum_ops on this QD.
		//
		new_quorum_devices[qid].pathname = new_qd->gdevname;

		//
		// We use the access_mode specified for the quorum device
		// in the infrastructure table in CCR. If access_mode is not
		// specified, we use the type specified for the quorum
		// device in the infrastructure table in CCR.
		//
		char *type_str = NULL;
		if (new_qd->access_mode != NULL) {
			type_str = new_qd->access_mode;
		} else {
			CL_PANIC(new_qd->type != NULL);
			type_str = new_qd->type;
		}

		//
		// Ask the type registry for a handle to this
		// quorum device type
		//
		Environment env;
		new_quorum_devices[qid].qd_v =
		    _type_registry_v->get_quorum_device(type_str, env);
		// get_quorum_device() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
		if (CORBA::is_nil(new_quorum_devices[qid].qd_v)) {
			CMM_TRACE(("quorum_device_added: get_quorum_device "
			    "did not return valid QD reference "
			    "for type %s.\n", type_str));
			//
			// SCMSGS
			// @explanation
			// The kernel quorum module for this type
			// of quorum device was not loaded, or
			// an instance of the quorum device type object
			// was not created.
			// The node ignores this quorum device
			// until corrective action is taken.
			// @user_action
			// The most likely reason for this problem is that
			// the system is missing the quorum module for this
			// type of device. Determine whether the component
			// is missing. If so, then install the missing
			// quorum module. The quorum device can then be
			// brought online either by rebooting the entire
			// cluster, or by unconfiguring the quorum device and
			// then configuring the same quorum device.
			// If the appropriate quorum module is present,
			// contact your authorized Sun service provider to
			// determine if a workaround or patch is available.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Unable to instantiate a quorum device "
			    "type object of type '%s' for qid %d "
			    "with gdevname '%s'.", type_str,
			    qid, new_quorum_devices[qid].pathname);
			return;
		}

		//
		// Call open using the reference.
		// This will also perform any necessary initialization.
		//
		quorum::quorum_error_t q_error = quorum::QD_SUCCESS;
		quorum::qd_property_seq_t props;
		q_error = new_quorum_devices[qid].qd_v->quorum_open(
		    new_qd->gdevname, false, props, env);
		if (env.exception()) {
			CMM_TRACE(("quorum_device_added: "
			    "Exception returned from quorum_open\n"));
			env.exception()->print_exception(
			    (char *)"quorum_open: ");
		}
		// quorum_open() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);

		//
		// Opening a quorum device could fail due to a variety
		// of reasons : either the path to the device is broken,
		// or the device is still initializing.
		//
		// We retry multiple times while trying to open a quorum device.
		// A failure (error code is not QD_SUCCESS) still could mean
		// different things. It is possible that the device is still
		// initializing or we are still fenced.
		// We will mark the quorum device as not accessible for now.
		// However, we do not drop the reference to the device.
		// Later on, we will retry opening on the same reference.
		//
		if (q_error != quorum::QD_SUCCESS) {
			// Mark as not accessible
			new_quorum_devices[qid].accessible = false;

			CMM_TRACE((
			    "Open failed for QD %d with gdevname '%s'.\n.",
			    qid, new_quorum_devices[qid].pathname));

			//
			// SCMSGS
			// @explanation
			// The open operation on the specified quorum device
			// failed, and this node will mark the quorum device
			// as inaccessible for now.
			// @user_action
			// The quorum device has failed or the path to
			// this device may be broken. Refer to the quorum disk
			// repair section of the administration guide
			// for resolving this problem.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Open failed for quorum device %s "
			    "with error %d.",
			    new_quorum_devices[qid].pathname,
			    (int)q_error);

			(void) new_quorum_devices[qid].qd_v->quorum_close(env);
			// quorum_close() does not raise any exceptions
			ASSERT(env.exception() == NULL);

			return;
		}

		// Mark the quorum device as accessible
		new_quorum_devices[qid].accessible = true;
	}

	//
	// SCMSGS
	// @explanation
	// The specified quorum device with the specified votecount
	// and configured paths bitmask has been added to the cluster.
	// The quorum subsystem treats a quorum device in maintenance state
	// as being removed from the cluster, so this message will be logged
	// when a quorum device is taken out of maintenance state as well as
	// when it is actually added to the cluster.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) _quorum_syslog_msgp->log(SC_SYSLOG_NOTICE, ADDED,
	    "CMM: Quorum device %d (%s) added; votecount = %d, "
	    "bitmask of nodes with configured paths = 0x%llx.",
	    qid, new_qd->gdevname, new_qd->votes,
	    new_qd->nodes_with_configured_paths);

	//
	// Publish sysevent.
	//
	// Lint throws informational message that converting string literal
	// to char * is not const safe.
	//lint -e1776
	(void) sc_publish_event(ESC_CLUSTER_QUORUM_CONFIG_CHANGE,
	    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_QUORUM_NAME, SE_DATA_TYPE_STRING, new_qd->gdevname,
	    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_ADDED,
	    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32, new_qd->votes, NULL);
	//lint +e1776
}

//
// When the quorum algorithm object refreshes its quorum config info from CCR,
// this method is called for each quorum device that has been removed from
// the CCR, but was a part of the older quorum config info maintained
// in the quorum algo object.
//
// This method does not do anything if the previous votecount
// of the removed quorum device was zero.
// Else it releases the quorum device CORBA reference from the quorum config
// info in the quorum algo object, syslogs messages and publishes sysevents
// for the quorum device removal.
//
// There are cases when a node has stale CCR, and so initializes quorum
// info from the stale CCR while booting up. It could happen that after CCR
// is refreshed, the node sees that a new quorum device has replaced
// an old quorum device with the same qid. In that case, it should be
// treated as the removal of the quorum device, and addition of the new
// quorum device (provided the votecount of the new quorum device is >0).
// The only caveat is that the removal of the old quorum device from
// the quorum config info in this case would not syslog messages or publish
// events related to a quorum device removal, as it is not an actual
// quorum device removal, but just a case of a node having stale CCR
// sync'ing up. The stale_ccr flag parameter passed in is set to true
// by the caller if its this stale CCR scenario.
//
void
quorum_algorithm_impl::quorum_device_removed(
    const quorum_device_info_t *old_qd, bool stale_ccr)
{
	quorum_device_id_t	qid = old_qd->qid;

	//
	// Only consider the QD as being removed NOW
	// if its original votecount > 0.
	// This cannot happen currently since scconf first decrements
	// the votecount to 0, then removes QD from the infrastructure file.
	// But in future, scconf may do the last step with the
	// last decrement of the votecount to 0.
	//
	if (old_qd->votes == 0) {
		return;
	}

	//
	// If we hold a reference to the quorum device,
	// close the quorum device if it was accessible,
	// and then release the reference.
	//
	if (!CORBA::is_nil(_quorum_devices[qid].qd_v)) {
		// If the quorum device was accessible, then we close it.
		if (_quorum_devices[qid].accessible) {
			Environment	env;
			(void) _quorum_devices[qid].qd_v->quorum_close(env);
			if (env.exception()) {
				CMM_TRACE((
				    "quorum_close returned an exception\n"));
				env.exception()->print_exception(
				    (char *)"quorum_close: ");
				env.clear();
			}
			CL_PANIC(env.exception() == NULL);
		}

		// Release the reference to the disk
		_quorum_devices[qid].qd_v = quorum::quorum_device_type::_nil();
	}

	if (stale_ccr) {
		// Return without publishing events or syslogging msgs
		return;
	}

	//
	// SCMSGS
	// @explanation
	// The specified quorum device with the specified votecount
	// has been removed from the cluster.
	// A quorum device being placed in maintenance state is equivalent
	// to it being removed from the quorum subsystem's perspective,
	// so this message will be logged when a quorum device is put
	// in maintenance state as well as when it is actually removed.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) _quorum_syslog_msgp->log(SC_SYSLOG_NOTICE, REMOVED,
		"CMM: Quorum device %d (%s) with votecount = %d removed.",
		qid, old_qd->gdevname, old_qd->votes);

	//
	// Publish sysevent.
	//
	// Lint throws informational message that converting string literal
	// to char * is not const safe.
	//lint -e1776
	(void) sc_publish_event(ESC_CLUSTER_QUORUM_CONFIG_CHANGE,
	    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_QUORUM_NAME, SE_DATA_TYPE_STRING, old_qd->gdevname,
	    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_REMOVED,
	    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32, old_qd->votes, NULL);
	//lint +e1776
}

//
// Returns true if either the gdevname, access_mode or type
// for the quorum device has changed
//
bool
qd_config_changed(
    const quorum_device_info_t *old_qd, const quorum_device_info_t *new_qd)
{
	//
	// Check gdevname
	//
	// gdevname must be specified
	CL_PANIC(old_qd->gdevname != NULL);
	CL_PANIC(new_qd->gdevname != NULL);
	if (os::strcmp(old_qd->gdevname, new_qd->gdevname) != 0) {
		// gdevname has changed
		return (true);
	}

	// Check access_mode
	if ((old_qd->access_mode && !new_qd->access_mode) ||
	    (!old_qd->access_mode && new_qd->access_mode)) {
		// access_mode has changed
		return (true);
	}
	if (old_qd->access_mode && new_qd->access_mode &&
	    (os::strcmp(old_qd->access_mode, new_qd->access_mode) != 0)) {
		// access_mode has changed
		return (true);
	}

	// Check type
	if ((old_qd->type && !new_qd->type) ||
	    (!old_qd->type && new_qd->type)) {
		// type has changed
		return (true);
	}
	if (old_qd->type && new_qd->type &&
	    (os::strcmp(old_qd->type, new_qd->type) != 0)) {
		// type has changed
		return (true);
	}

	// neither of gdevname, access_mode or type has changed
	return (false);
}

//
// Function to initialize quorum-device information.
//
// Called when the quorum configuration is changed (by updating the
// CCR infrastructure table) as well as when the node is booting.
//
bool
quorum_algorithm_impl::initialize_device_quorum_info(
	quorum_table_t &new_qt, quorum_info_t *&new_quorum_devices)
{
	node_bitmask_t		my_bitmask = (1LL << (_my_nodeid-1));
	node_bitmask_t		old_configured, new_configured;
	quorum_device_info_t	*old_qd, *new_qd;
	bool			quorum_devices_accessible = false;
	uint_t			i;
	uint_t			new_max_qid;

	CMM_TRACE(("initialize_device_quorum_info called\n"));

	// Allocate and initialize various arrays.
	new_max_qid = new_qt.quorum_devices.listlen;
	if (new_max_qid == 0) {
		new_quorum_devices = NULL;
	} else {
		new_quorum_devices = new quorum_info_t[new_max_qid + 1];
		for (i = 1; i <= new_max_qid; i++) {
			new_quorum_devices[i].pathname = NULL;
			new_quorum_devices[i].owner = NODEID_UNKNOWN;
			new_quorum_devices[i].qd_v =
			    quorum::quorum_device_type::_nil();
			new_quorum_devices[i].accessible = false;
		}
	}

	for (i = 1; i <= MAX_QUORUM_DEVICES; i++) {
		//
		// Bug 4321176
		// The quorum algorithm will only consider (ie fence)
		// QDs that have a votecount > 0. It will treat the
		// ones with votecount of 0 as essentially QD holes
		// from the fencing perspective until the votecount
		// increases.
		//

		if ((i > _max_qid) &&
		    (i > new_qt.quorum_devices.listlen)) {
			break; // no more quorum devices to look at
		}

		if ((i > _max_qid) &&
		    (new_qt.quorum_devices.list[i-1].gdevname != NULL)) {
			// New quorum device added to configuration
			new_qd = &new_qt.quorum_devices.list[i-1];
			quorum_device_added(new_qd,
			    quorum_devices_accessible, new_quorum_devices);

		} else if ((i > new_qt.quorum_devices.listlen) &&
		    (_quorum_table->quorum_devices.list[i-1].gdevname
		    != NULL)) {
			// Quorum device deleted from configuration.
			old_qd = &_quorum_table->quorum_devices.list[i-1];
			quorum_device_removed(old_qd, false);

		} else if ((i <= _max_qid) &&
		    (i <= new_qt.quorum_devices.listlen)) {

			// An existing qd/hole information has been changed.
			old_qd = &_quorum_table->quorum_devices.list[i-1];
			new_qd = &new_qt.quorum_devices.list[i-1];
			old_configured = old_qd->nodes_with_configured_paths;
			new_configured = new_qd->nodes_with_configured_paths;

			// No action reqd if a previous hole is still a hole.
			if ((old_qd->gdevname == NULL) &&
			    (new_qd->gdevname == NULL)) {
				continue;

			} else if (old_qd->gdevname == NULL) {
				// QD has been added.
				quorum_device_added(
				    new_qd, quorum_devices_accessible,
				    new_quorum_devices);

			} else if (new_qd->gdevname == NULL) {
				// QD has been removed.
				quorum_device_removed(old_qd, false);

			} else if (!qd_config_changed(old_qd, new_qd)) {
				// Same gdevname, access_mode and type
				if (old_configured != new_configured) {
					//
					// This happens only for a QD remove
					// operation - scconf removes the
					// configured paths before doing
					// its first commit.
					//

					//
					// SCMSGS
					// @explanation
					// The number of configured paths to
					// the specified quorum device has
					// been changed as indicated. The
					// connectivity information is
					// depicted as bitmasks.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) _quorum_syslog_msgp->log(
					    SC_SYSLOG_NOTICE, PROPERTY_CHANGED,
					    "CMM: Connectivity of quorum "
					    "device %d (%s) has been changed "
					    "from 0x%llx to 0x%llx.",
					    i, new_qd->gdevname,
					    old_configured, new_configured);

					//
					// If the local node's path has got
					// configured, then set to true
					// quorum_devices_accessible.
					//
					if (!(old_configured & my_bitmask) &&
					    (new_configured & my_bitmask)) {
						//
						// This can not happen currently
						// since scconf does not allow
						// config'ed paths to be added.
						// Leave it in since do not want
						// to make assumptions about how
						// scconf works.
						//
						quorum_devices_accessible
						    = true;
					}
				}

				//
				// No change in votes. No configuration has
				// been changed for the quorum device.
				// Do not instantiate new quorum device objects.
				// Just make a copy of the old reference
				// before it is released.
				// We also release the old reference here
				// so that later on, while deleting old quorum
				// info, the quorum reference is not closed.
				//
				if (old_qd->votes == new_qd->votes) {
					quorum_device_id_t qid = new_qd->qid;
					new_quorum_devices[qid].pathname =
					    new_qd->gdevname;
					new_quorum_devices[qid].qd_v =
					    _quorum_devices[qid].qd_v;
					_quorum_devices[qid].qd_v =
					    quorum::quorum_device_type::_nil();
					new_quorum_devices[qid].accessible =
					    _quorum_devices[qid].accessible;
					_total_configured_votes +=
					    new_qd->votes;
					continue;
				}

				//
				// The votes have changed.
				// If new vote is 0 => QD removal.
				// If old vote is 0 => QD addition.
				//
				if (old_qd->votes == 0) {
					// QD added.
					quorum_device_added(
					    new_qd, quorum_devices_accessible,
					    new_quorum_devices);
				} else if (new_qd->votes == 0) {
					// QD removed.
					quorum_device_removed(old_qd, false);
				} else {
					//
					// Do not instantiate new quorum device
					// objects. Just make a copy of the old
					// reference before it is released.
					// We also release the old reference
					// here so that later on, while deleting
					// old quorum info, the quorum reference
					// is not closed.
					//
					quorum_device_id_t qid = new_qd->qid;
					new_quorum_devices[qid].pathname =
					    new_qd->gdevname;
					new_quorum_devices[qid].qd_v =
					    _quorum_devices[qid].qd_v;
					_quorum_devices[qid].qd_v =
					    quorum::quorum_device_type::_nil();
					new_quorum_devices[qid].accessible =
					    _quorum_devices[qid].accessible;
					_total_configured_votes +=
					    new_qd->votes;

					//
					// SCMSGS
					// @explanation
					// The votecount for the specified
					// quorum device has been changed as
					// indicated.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) _quorum_syslog_msgp->log(
					    SC_SYSLOG_NOTICE, PROPERTY_CHANGED,
					    "CMM: Votecount changed from %d to "
					    "%d for quorum device %d (%s).",
					    old_qd->votes, new_qd->votes, i,
					    new_qd->gdevname);

					//
					// Publish sysevent.
					//
					// Lint throws informational message
					// that converting string literal
					// to char * is not const safe.
					//lint -e1776
					(void) sc_publish_event(
					    ESC_CLUSTER_QUORUM_CONFIG_CHANGE,
					    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_QUORUM_NAME, SE_DATA_TYPE_STRING,
					    old_qd->gdevname,
					    CL_CONFIG_ACTION,
					    SE_DATA_TYPE_UINT32,
					    CL_EVENT_CONFIG_PROP_CHANGED,
					    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32,
					    new_qd->votes, NULL);
					//lint +e1776
				}
			} else {
				//
				// gdevname or access_mode or type of a QD
				// has been changed.
				// The only time this can happen is if
				// while this node was down, the original
				// QD was removed, and a new one with the
				// same qid was added or the configuration
				// of the same old QD has changed
				// (different access_mode/type).
				// For that case, if the votecount is >0,
				// then this is a QD addition case.
				//

				//
				// Note that this will happen if CCR on
				// this node is stale, and so while booting up,
				// the node has initialized its quorum info
				// with old quorum config from stale CCR.
				// Now after CCR refresh, it is seen that
				// another quorum device has replaced
				// a previously existing quorum device
				// with the same qid or the configuration
				// (access_mode/type) of the old quorum device
				// has changed. So we need to remove
				// the old quorum device and add the new
				// quorum device if votecount of new
				// quorum device is >0.
				//
				// Being a stale CCR sync-up case, we
				// pass true to quorum_device_removed()
				//
				quorum_device_removed(old_qd, true);

				//
				// SCMSGS
				// @explanation
				// The configuration (name/access_mode/type) of
				// the specified quorum device was
				// changed as indicated. This change might occur
				// if the previous quorum device was removed
				// from the cluster while this node was down,
				// and a new device was added to the cluster and
				// assigned the same ID as the old device.
				// This configuration change might also occur
				// if the configuration (access_mode or type) of
				// the old quorum device was changed.
				// @user_action
				// This message is informational; no user
				// action is required.
				//
				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_NOTICE, PROPERTY_CHANGED,
				    "CMM: Quorum device %d (old name %s) "
				    "configuration has changed.",
				    i, old_qd->gdevname);
				//
				// Publish sysevent.
				//
				// Lint throws informational message
				// that converting string literal
				// to char * is not const safe.
				//lint -e1776
				(void) sc_publish_event(
				    ESC_CLUSTER_QUORUM_CONFIG_CHANGE,
				    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
				    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				    CL_QUORUM_NAME, SE_DATA_TYPE_STRING,
				    old_qd->gdevname,
				    CL_CONFIG_ACTION,
				    SE_DATA_TYPE_UINT32,
				    CL_EVENT_CONFIG_REMOVED,
				    CL_VOTE_COUNT, SE_DATA_TYPE_UINT32,
				    old_qd->votes,
				    NULL);
				//lint +e1776

				quorum_device_added(
				    new_qd, quorum_devices_accessible,
				    new_quorum_devices);
			}
		} else {
			// Existing or new hole - no action required.
		}
	}
	return (quorum_devices_accessible);
}

//
// Function to deallocate dynamically allocated quorum info.
// Must be called with the _quorum_config_mutex lock held.
//
void
quorum_algorithm_impl::delete_quorum_info()
{
	uint_t i;

	CL_PANIC(_quorum_config_mutex.lock_held());
	CL_PANIC(_quorum_config_use_count == 0);

	delete [] _quorum_devices_owned;
	_quorum_devices_owned = NULL;

	// If there are no quorum devices, release the quorum table and return.
	if (_max_qid == 0) {
		clconf_release_quorum_table(_quorum_table);
		_quorum_table = NULL;
		return;
	}

	for (i = 1; i <= _max_qid; i++) {
		if (!CORBA::is_nil(_quorum_devices[i].qd_v)) {
			// Close the quorum device if it was accessible
			if (_quorum_devices[i].accessible) {
				Environment	env;
				(void) _quorum_devices[i].qd_v->quorum_close(
				    env);
				if (env.exception()) {
					CMM_TRACE(("quorum_close returned an "
					    "exception\n"));
					env.exception()->print_exception(
					    (char *)"quorum_close: ");
				}
				// quorum_close() does not throw exceptions
				CL_PANIC(env.exception() == NULL);
			}
			// Release the reference to the disk
			_quorum_devices[i].qd_v =
			    quorum::quorum_device_type::_nil();
		}
	}
	delete [] _quorum_devices;
	_quorum_devices = NULL;

	clconf_release_quorum_table(_quorum_table);
	_quorum_table = NULL;
}

//
// Lock the quorum configuration so that the in-memory version is
// not modified. The CCR copy can be modified when this lock is
// held, but the in-memory version will not be updated until this
// lock is released.
//
// We simply increment a use count here and disallow modifications
// until the use count goes to 0.
//
// This is used by the readers of this information.
//
void
quorum_algorithm_impl::config_read_lock()
{
	_quorum_config_mutex.lock();
	CL_PANIC(_quorum_config_use_count >= 0);
	_quorum_config_use_count++;
	_quorum_config_mutex.unlock();
}

//
// Function to decrement the quorum configuration use count. When
// the use count goes down to 0, we signal any threads that are
// waiting for this event. The waiting threads are writers of the
// quorum configuration information.
//
// This is used by the readers of this information.
//
void
quorum_algorithm_impl::config_read_unlock()
{
	_quorum_config_mutex.lock();
	CL_PANIC(_quorum_config_use_count > 0);
	_quorum_config_use_count--;
	if (_quorum_config_use_count == 0)
		_quorum_config_cv.broadcast();
	_quorum_config_mutex.unlock();
}

//
// Function to wait on a condition variable, giving the config
// mutex. This should be used by threads wanting to be woken to
// read when a cv is signaled.
//
// Must be called with read lock held.
//
void
quorum_algorithm_impl::config_read_wait(os::condvar_t *cv)
{
	_quorum_config_mutex.lock();
	CL_PANIC(_quorum_config_use_count > 0);
	_quorum_config_use_count--;
	if (_quorum_config_use_count == 0)
		_quorum_config_cv.broadcast();
	CL_PANIC(cv);
	cv->wait(&_quorum_config_mutex);
	_quorum_config_use_count++;
	_quorum_config_mutex.unlock();
}

//
// Routine to acquire a write lock for the quorum config info.
//
bool
quorum_algorithm_impl::config_write_lock_if_not_locked()
{

	CL_PANIC(!_ioctl_mutex.lock_held());

	//
	// Check if the lock is already held by this thread,
	// and return a true indicating that lock is already held.
	//
	if (_quorum_config_mutex.lock_held())
		return (true);
	else {
		// If lock not already held, try to acquire it.
		_quorum_config_mutex.lock();
		while (_quorum_config_use_count > 0)
			_quorum_config_cv.wait(&_quorum_config_mutex);
		return (false);
	}
}

//
// Routine to release the write lock.
//
//
void
quorum_algorithm_impl::config_write_unlock(bool lock_was_already_held)
{
	if (!lock_was_already_held)
		_quorum_config_mutex.unlock();
}

//
// Routine that attempts to take ownership of an unowned quorum
// device. If a current cluster member has a reservation on the
// device, that node is the owner. The SCSI-3 spec allows only
// one reservation to be placed per device. If no reservation exists,
// the owner is the lowest numbered member node with a
// reservation key on the device.
//
// If the current node is the owner, then this routine places a
// reservation if not already present and preempts reservation keys
// of nodes that are not current cluster members.
// For a dual-ported quorum device (using PGRE), if the other
// node is alive, then the current owner releases its SCSI2
// reservation - this will allow the other node access rights
// to the disk so it can do PGRE.
//
// Arguments:
// ----------
// "qid" is the quorum device #.
// "member_set" is the set of current members in the cluster.
//
// Return Values:
// --------------
// true - if this node turns out to be the owner of the QD,
// false - if this node is not the owner of the QD.
//
bool
quorum_algorithm_impl::take_ownership(
	quorum::quorum_device_id_t	qid,
	const nodeset&			member_set)
{
	nodeset			preempt_nodes;
	nodeid_t		owner_id = NODEID_UNKNOWN;
	nodeid_t		nid;
	int			error;
	bool			reg_key_found = false;
	uint_t			index;

	//
	// If we already know that the device is owned by a current cluster
	// member other than the local node, we need to do nothing.
	//
	nid = _quorum_devices[qid].owner;
	if ((nid != _my_nodeid) &&
	    (nid != NODEID_UNKNOWN) &&
	    member_set.contains(nid))
		return (false); // already owned by another cluster member

	//
	// Check to see if this quorum device is configured to act as an
	// amnesia protection device. If it is not configured to be an amnesia
	// protection, then go ahead and register our key. If the device is
	// configured to be an amnesia protection, then we must not write our
	// key until after the CCR recovery happens. In that case, the register
	// will be called from ccr_refreshed.
	//
	// We only want to do this the first time the node is accessing the
	// quorum device, because we don't want to re-register after our
	// key was legitimately preempted from the device. So, we only check
	// the amnesia protection if idl_registration_list.listlen is 0, which
	// it should be during the first boot, but not afterwards.
	//
	if (idl_registration_list.listlen == 0) {
		Environment e;
		uint32_t amn_level =
		    _quorum_devices[qid].qd_v->quorum_supports_amnesia_prot(e);
		if (e.exception() != NULL) {
			CMM_TRACE(("ERROR: Exception returned from "
			    "quorum_supports_amnesia_prot\n"));
			e.exception()->print_exception(
			    "supports_amnesia_failfast: ");
		}
		CL_PANIC(e.exception() == NULL);  // Should never have error.

		if (amn_level == AMN_NO_SUPPORT) {
			//
			// Even though our key is not registered, we are going
			// to go ahead and register our key now.
			//
			CMM_TRACE(("Overriding amnesia protection.\n"));
			int reg_error = _quorum_devices[qid].qd_v->
			    quorum_register(*my_key, e);
			if (reg_error) {
				CMM_TRACE(("quorum_register(qid=%d) returned "
				    "error %d\n", qid, reg_error));
			}
		}
	}

	//
	// Read the reservation keys on the device and check if
	// the key is already on the device.
	//
	Environment	env;

	CMM_TRACE(("take_ownership: Calling quorum_read_keys\n"));
	idl_registration_list.listlen = 0;
	error = _quorum_devices[qid].qd_v->quorum_read_keys(
	    idl_registration_list,
	    env);
	if (env.exception() != NULL) {
		CMM_TRACE(("take_ownership:quorum_read_keys returned an "
		    "exception.\n"));
		env.exception()->print_exception(
		    "read_keys: ");
	}
	CL_PANIC(env.exception() == NULL);
	if (error) {
		CMM_TRACE(("quorum_read_keys(qid=%d) returned error %d.\n",
		    qid, error));
		(void) _quorum_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Reading reservation keys from quorum device "
		    "%s failed with error %d.",
		    _quorum_devices[qid].pathname, error);
		goto done;
	}
	CMM_TRACE(("Completed quorum_read_keys\n"));

	//
	// Check if registration key for this node is found
	// on quorum device
	//
	for (index = 0; index < idl_registration_list.listlen; index++) {
		quorum::registration_key_t	*reg_key;
		reg_key = (quorum::registration_key_t *)
		    (&idl_registration_list.keylist[index]);
		if (null_idl_key(reg_key)) {
			continue; // ignore null key
		}
		if (match_idl_keys(reg_key, (quorum::registration_key_t *)
		    (&RESERVATION_KEY(_my_nodeid)))) {
			reg_key_found = true;
			break;
		}
	}

	if (!reg_key_found) {
		//
		// If our key was on the device before, but is no longer
		// present, this implies that we have been preempted by
		// another viable partition.
		//
		if (MY_KEY_WAS_ON_DEVICE(qid)) {
			CMM_TRACE(("preempted from QD %d.\n", qid));
			//
			// SCMSGS
			// @explanation
			// This node's reservation key was on the
			// specified quorum device, but is no longer
			// present, implying that this node
			// has been preempted by another cluster
			// partition. If a cluster gets divided into two
			// or more disjoint subclusters, exactly one
			// of these must survive as the operational
			// cluster. The surviving cluster forces the
			// other subclusters to abort by grabbing
			// enough votes to grant it majority quorum.
			// This is referred to as preemption of the
			// losing subclusters.
			// @user_action
			// There may be other related messages that
			// may indicate why quorum was lost.
			// Determine why quorum was lost on this
			// node, resolve the problem and reboot this
			// node.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: This node has been preempted from quorum "
			    "device %s.", _quorum_devices[qid].pathname);
			_we_have_been_preempted = true;
		}
		// no need to proceed reading reservation keys
		goto done;
	}

	//
	// If the device already has a reservation, check if the
	// reservation is held by a cluster member. If so, that node
	// is the owner of the device. If multiple nodes hold a
	// reservation (allowed by the earliest SCSI-3 specs), choose the
	// lowest numbered node as the owner.
	//
	if (find_resv_owner(qid, member_set, owner_id)) {
		// Error reading resv list on QD.
		goto done;
	}

	//
	// Go through the reservation keys on the device. If there
	// are keys by non-members, add those members to the preempt
	// set. If the device owner has not been determined yet,
	// select the lowest numbered member that has its reservation
	// key on the device as the owner. Also, find out if the
	// local node is registered, so failfast can be enabled -
	// this is indicated by setting the last arg to false.
	//
	if (idl_registration_list.listlen) {
		find_owner_and_nodes_to_preempt(
		    qid, member_set, preempt_nodes, owner_id, false);
	}

	if (owner_id == NODEID_UNKNOWN) {
		if (idl_registration_list.listlen == 0) {
			CMM_TRACE(("device %d has no keys\n", qid));
		} else {
			CMM_TRACE(("QD %d has no keys "
			    "of current members; device held by "
			    "members 0x%llx\n", qid,
			    preempt_nodes.bitmask()));

			if ((preempt_nodes.bitmask() != 0) &&
			    !_cluster_has_quorum) {

				    //
				    // The cluster probably ran before without
				    // any of the current cluster members.
				    // The CCR may be out of date on these
				    // nodes. Print out a message for the
				    // operator.
				    //
				    char	*msg_string;
				    int		num_down_nodes_with_key = 0;

				    msg_string =
					new char[(uint_t)(8 * _max_nodeid)];
				    ASSERT(msg_string != NULL);
				    msg_string[0] = '\0';

				    for (nid = 1; nid <= _max_nodeid; nid++)
					if (preempt_nodes.contains(nid)) {
					    if (num_down_nodes_with_key != 0) {
						(void) strcat(msg_string, ", ");
					    }
					    num_down_nodes_with_key++;
					    os::sprintf(&msg_string[os::strlen(
						msg_string)], "%d", nid);
					}

				    CL_PANIC(num_down_nodes_with_key > 0);

				    //
				    // SCMSGS
				    // @explanation
				    // This node does not have its reservation
				    // key on the specified quorum device,
				    // which has been reserved by the
				    // specified node or nodes that the local
				    // node can not communicate with. This
				    // indicates that in the last incarnation
				    // of the cluster, the other nodes were
				    // members whereas the local node was not,
				    // indicating that the CCR on the local
				    // node may be out-of-date. In order to
				    // ensure that this node has the latest
				    // cluster configuration information, it
				    // must be able to communicate with at
				    // least one other node that was a member
				    // of the previous cluster incarnation.
				    // These nodes holding the specified
				    // quorum device may either be down or
				    // there may be up but the interconnect
				    // between them and this node may be
				    // broken.
				    // @user_action
				    // If the nodes holding the specified
				    // quorum devices are up, then fix the
				    // interconnect between them and this node
				    // so that communication between them is
				    // restored. If the nodes are indeed down,
				    // boot one of them.
				    //
				    (void) _quorum_syslog_msgp->log(
					SC_SYSLOG_NOTICE, MESSAGE,
					"CMM: Quorum device %d (gdevname "
					"%s) can not be acquired by the "
					"current cluster members. This "
					"quorum device is held by node%s "
					"%s. ", qid,
					_quorum_devices[qid].pathname,
					num_down_nodes_with_key > 1 ? "s":"",
					msg_string);

				    delete [] msg_string;
			}
		}
	} else if (owner_id == _my_nodeid) {
		//
		// I am the owner of this device. If there are keys
		// to be preempted, I must do the preempting. If no
		// keys to be preempted, I need to reserve the QD.
		//
		if (preempt_nodes_and_reserve(qid,
		    preempt_nodes, member_set)) {
			//
			// We cannot take ownership of the device
			// since the reservation failed.
			//
			owner_id = NODEID_UNKNOWN;
		}
	}

done:

	CMM_TRACE(("take_ownership: device=%d: owner_id=%d, "
	    "preempt_nodes=0x%llx\n",
	    qid, owner_id, preempt_nodes.bitmask()));

	return (owner_id == _my_nodeid);
}

//
// Private function to acquire quorum devices. This should be called with
// the quorum configuration lock and the ioctl lock held.
//
// This function does the following:
//
// For each quorum device that was not accessible earlier
// but is connected to this node (will have a non-nil CORBA reference),
//		try to open the device
//		if the open succeeds,
//			mark the quorum device as accessible
// The above action ensures that for every quorum device that was
// not accessible (but to which this node has a connecting path) during
// the previous CMM reconfiguration, the current CMM reconfiguration
// will try to open the device and mark it accessible on success.
//
// Then for each quorum device that is accessible
// and that this node is connected to,
//		read the reservation keys
//		select the lowest numbered node with a key as the owner
//		if this node is the owner
//			for each node not in the partition,
//				if the dead node's key is on the device
//					remove the dead node's keys
//				if the preempt fails
//					give up;
//					another partition won the race;
//					defer to it
//		claim the device as being owned by this node
//
void
quorum_algorithm_impl::acquire_quorum_devices_lock_held(
	membership_bitmask_t cluster_members,
	quorum::quorum_result_t& quorum_result)
{
	membership_bitmask_t	owned_devices = 0;
	nodeset			member_set(cluster_members);
	quorum_device_id_t	qid;

	CL_PANIC(_quorum_config_mutex.lock_held());
	CL_PANIC(_ioctl_mutex.lock_held());

	for (qid = 1; qid <= _max_qid; qid++) {

		//
		// If we have learned (when examining a lower-numbered
		// quorum device or in an earlier reconfiguration) that
		// we been preempted by another partition, we should not
		// try to acquire any more quorum devices.
		//
		if (_we_have_been_preempted) {
			break;
		}

		//
		// During initialization of quorum device info,
		// we check if this node has a path to a quorum device,
		// and if so, then we try to instantiate a CORBA object
		// for that quorum device, and then try to open the device.
		// This is provided the QD is not in maintenance state
		// (QD in maintenance state means its votecount would be 0).
		//
		// If the instantiation of the CORBA object for a quorum
		// device failed, or if this node does not have a path to
		// a quorum device, then we would have a nil CORBA reference
		// to the quorum device. We skip such a quorum device.
		//
		// If the open of the device failed earlier, we would have
		// marked the device as not accessible. But we would still
		// have the CORBA reference to the quorum device.
		// We try re-opening the quorum device now;
		// the quorum device might be accessible now.
		//
		Environment env;
		quorum::quorum_error_t q_error = quorum::QD_SUCCESS;
		if (CORBA::is_nil(_quorum_devices[qid].qd_v)) {
			//
			// Either no QD is configured with this qid,
			// or this QD is in maintenance state,
			// or we failed to instantiate a CORBA object
			// for this quorum device earlier, or this node
			// does not have a path to this quorum device.
			// We skip this quorum device.
			//
			ASSERT(!_quorum_devices[qid].accessible);
			continue;
		}

		if (!_quorum_devices[qid].accessible) {
			//
			// Quorum device is marked non-accessible.
			// Try opening it now.
			// Call open using the reference.
			// This will also perform any necessary initialization.
			//
			quorum::qd_property_seq_t props;
			q_error = _quorum_devices[qid].qd_v->quorum_open(
			    _quorum_devices[qid].pathname, false, props, env);
			if (env.exception()) {
				CMM_TRACE(("acquire_quorum_devices_lock_held: "
				    "Exception returned from quorum_open\n"));
				env.exception()->print_exception(
				    (char *)"quorum_open: ");
			}
			// quorum_open() does not throw any exceptions
			CL_PANIC(env.exception() == NULL);

			//
			// We had earlier tried to open the device and
			// had failed. We failed to open the device yet again.
			// So we will keep the device marked as not accessible.
			// But, we will not drop the reference to the device.
			// We will re-try opening this quorum device
			// during our next CMM reconfiguration.
			//
			if (q_error != quorum::QD_SUCCESS) {
				ASSERT(!_quorum_devices[qid].accessible);
				CMM_TRACE(("acquire_quorum_devices_lock_held: "
				    "quorum_open failed for QD %s (qid %d) "
				    "with error %d.\n.",
				    _quorum_devices[qid].pathname,
				    qid, (int)q_error));

				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "CMM: Open failed for quorum device %s "
				    "with error %d.",
				    _quorum_devices[qid].pathname,
				    (int)q_error);

				(void) _quorum_devices[qid].qd_v->
				    quorum_close(env);
				// quorum_close() does not raise any exceptions
				ASSERT(env.exception() == NULL);

				// Skip this quorum device.
				continue;
			}

			// Mark the quorum device as accessible
			_quorum_devices[qid].accessible = true;
		}

		//
		// If the quorum device is accessible,
		// then the reference has to be non-nil.
		//
		ASSERT(is_not_nil(_quorum_devices[qid].qd_v));

		if (take_ownership(qid, member_set)) {
			owned_devices |= 1LL << (qid-1);
		}
	}

	quorum_result.nodeid = _my_nodeid;
	quorum_result.quorum_devices_owned = owned_devices;
	quorum_result.we_have_been_preempted = _we_have_been_preempted;

	CMM_TRACE(("acquire_quorum_devices_lock_held "
		"returing {nodeid = %d, owned_devices = 0x%llx, "
		"preempted = %d}\n",
		quorum_result.nodeid, quorum_result.quorum_devices_owned,
		quorum_result.we_have_been_preempted));
}

//
// Returns true if node nid has a configured connection to quorum
// device qid and false otherwise.
//
// This function should be called with the quorum config lock held.
//
bool
quorum_algorithm_impl::_node_connected_to_quorum_device(
	nodeid_t		nid,
	quorum_device_id_t	qid)
{
	nodeset			nodes_with_paths;

	nodes_with_paths.set(_quorum_table->quorum_devices.list[qid-1].
		nodes_with_configured_paths);

	return (nodes_with_paths.contains(nid));
}

//
// Returns the number of votes represented by a given set of nodes and
// quorum devices.
//
// This function should be called with the quorum config lock held.
//
uint_t
quorum_algorithm_impl::vote_count(
	const nodeset&	nodes,
	const nodeset&	devices)
{
	uint_t		votes = 0;
	uint_t		i;

	//
	// Add the votes of the nodes.
	//
	for (i = 1; i <= _max_nodeid; i++)
		if (nodes.contains(i))
			votes += _quorum_table->nodes.list[i-1].votes;

	//
	// Add the votes of the quorum devices.
	//
	for (i = 1; i <= _max_qid; i++)
		if (devices.contains(i))
			votes += _quorum_table->quorum_devices.list[i-1].votes;

	return (votes);
}

//
// Print out the quorum table into the trace buffer.
//
void
quorum_algorithm_impl::print_quorum_table(const quorum_table_t& qt)
{
	uint_t		total_votes = 0;
	uint_t		i;

	CMM_TRACE(("\n----------Quorum Info Table-----------\n"));
	CMM_TRACE(("List of nodes:\n"));
	for (i = 0; i < qt.nodes.listlen; i++) {

		node_info_t	*node_info_p = &qt.nodes.list[i];

		if (null_mhioc_key(node_info_p->reservation_key)) {
			//
			// This node is not configured. This node ID
			// represents a hole in node IDs.
			//
			CL_PANIC(node_info_p->votes == 0);
			continue; // skip this node
		}

		total_votes += node_info_p->votes;

		CMM_TRACE(("   Node %d: votes = %d, key = 0x%llx\n",
			node_info_p->nodenum,
			node_info_p->votes,
			mhioc_key_to_int64_key(node_info_p->reservation_key)));
	}
	CMM_TRACE(("List of quorum devices:\n"));
	for (i = 0; i < qt.quorum_devices.listlen; i++) {

		quorum_device_info_t	*quorum_info_p =
			&qt.quorum_devices.list[i];

		if (quorum_info_p->gdevname == NULL) {
			//
			// This quorum device is not configured. This QID
			// represents a hole in QIDs.
			//
			CL_PANIC(quorum_info_p->votes == 0);
			continue; // skip this device
		}

		// Print QDs with votes of 0 also. Even though DCS handles
		// their fencing and these are not really considered as
		// QDs until their votecount increases, it will be useful
		// to print these here for debugging purposes.

		total_votes += quorum_info_p->votes;

		CMM_TRACE(("   Quorum device %d: gdevname = '%s', "
			"votes = %d, nodes_with_configured_paths = 0x%llx\n",
			quorum_info_p->qid,
			quorum_info_p->gdevname,
			quorum_info_p->votes,
			quorum_info_p->nodes_with_configured_paths));
	}
	CMM_TRACE(("Total number of configured votes = %d.\n", total_votes));
	CMM_TRACE(("\n--------End of Quorum Info Table--------\n\n"));
}

//
// Print out the status of the quorum objects into the trace buffer.
//
void
quorum_algorithm_impl::print_quorum_status(
	const quorum::quorum_status_t& qstatus)
{
	uint_t	i;

	CMM_TRACE(("\n*** Cluster Quorum Information : ***\n\n"));
	CMM_TRACE(("Total configured votes : %d.\n", qstatus.configured_votes));
	CMM_TRACE(("Current cluster votes : %d.\n", qstatus.current_votes));
	CMM_TRACE(("Votes needed for quorum : %d.\n",
		qstatus.votes_needed_for_quorum));

	CMM_TRACE(("Node quorum info:\n"));
	for (i = 0; i < qstatus.nodelist.length(); i++) {
		CMM_TRACE(("\tNode %ld : votes_configured = %ld, "
			"state = %s, reservation_key = 0x%llx\n",
			qstatus.nodelist[i].config.nid,
			qstatus.nodelist[i].config.votes_configured,
			(qstatus.nodelist[i].state ==
			quorum::QUORUM_STATE_ONLINE) ? "UP" : "DOWN",
			idl_key_to_int64_key((
				const quorum::registration_key_t *)
				&qstatus.nodelist[i].config.reservation_key)));
	}

	if (qstatus.quorum_device_list.length() == 0) {
		CMM_TRACE(("No quorum devices configured in this cluster.\n"));
	} else {
		CMM_TRACE(("Quorum device info:\n"));
		for (i = 0; i < qstatus.quorum_device_list.length(); i++) {

			quorum::quorum_device_status_t	qp;
			char				*qname;

			qp = qstatus.quorum_device_list[i];
			qname = qp.config.gdevname;

			CMM_TRACE(("\tQuorum device %d : "
				"global device name = '%s', "
				"votes_configured = %d, state = %s, "
				"reservation owner = node %d\n",
				qp.config.qid,
				qname,
				qp.config.votes_configured,
				(qstatus.nodelist[i].state == quorum::
				QUORUM_STATE_ONLINE) ? "ONLINE" : "OFFLINE",
				qp.reservation_owner));
		}
	}
}

//
// take_ownership_after_register
//
// This method is called by ccr_refreshed(), if the node did not have
// its key on the device until now and was successful in registering
// its key. This method checks if there are non-member keys on the
// device and preempts them, if so and if this node is the owner. If
// there are no nodes to be preempted, this node simply reserves the
// device if it is the owner.
//
// Arguments:
// ----------
// "qid" is the quorum device #.
// "member_set" is the set of current members in the cluster.
//
// Return Values:
// --------------
// true - if this node turns out to be the owner of the QD,
// false - if this node is not the owner of the QD.
//
bool
quorum_algorithm_impl::take_ownership_after_register(
    quorum::quorum_device_id_t	qid,
    const nodeset&		member_set)
{
	nodeset			preempt_nodes;
	nodeid_t		owner_id = NODEID_UNKNOWN;
	nodeid_t		nid;

	// Called from ccr_refreshed(), so cluster must have quorum.
	CL_PANIC(_cluster_has_quorum);

	// If device owned by a current cluster member, then do nth.
	nid = _quorum_devices[qid].owner;
	if ((nid != NODEID_UNKNOWN) && member_set.contains(nid))
		return (false);

	//
	// Read the resv list to find if the reservation held by a
	// cluster member. If so, that node is the owner of the device.
	// If multiple nodes hold a reservation (allowed by the earliest
	// SCSI-3 specs), choose the lowest numbered node as the owner.
	//
	// From the resv_list, determine the ownerid.
	//

	if (find_resv_owner(qid, member_set, owner_id)) {
		// Error reading resv list on the QD.
		return (false);
	}

	//
	// If an owner has been found, it can not be the local node.
	// It is a current cluster member, no need to do anything.
	//
	if (owner_id != NODEID_UNKNOWN) {
		CL_PANIC(owner_id != _my_nodeid);
		return (false);
	}

	//
	// This method has been called after this node just successfully
	// registered its keys on the device - ie it had done a readkeys()
	// and not found its keys. So the idl_registration_list contains
	// all keys (other than the local node's) found on the device.
	//
	// Go through the reservation keys on the device. If there
	// are keys by non-members, add those members to the preempt
	// set. If the device owner has not been determined yet,
	// select the lowest numbered member that has its reservation
	// key on the device as the owner.
	//
	if (idl_registration_list.listlen) {
		find_owner_and_nodes_to_preempt(
			qid, member_set, preempt_nodes, owner_id, true);
	} else if (owner_id == NODEID_UNKNOWN) {
		// No keys until this node registered.
		owner_id = _my_nodeid;
	}

	//
	// The owner_id field must be set by now, because even if there is
	// no other cluster member key on the device, the local node's key
	// is, so it will be the owner in that case.
	//
	CL_PANIC(owner_id != NODEID_UNKNOWN);

	//
	// If some other node is the owner, that node will take care of
	// preempting. Return.
	//
	if (owner_id != _my_nodeid) {
		return (false);
	} else {
		//
		// The local node is the owner. If preempt_nodes is non-empty,
		// do the preempting now. If no nodes to preempt, reserve the
		// device.
		//
		CMM_TRACE(("This node is the owner of QD %d\n", qid));

		//
		// Being here implies that when this node first came up,
		// it found no cluster member keys on the QD, and must
		// have syslog'ed that. Log the event that this node is
		// now registered with the QD.
		//

		//
		// SCMSGS
		// @explanation
		// When this node was booting up, it had found only
		// non-cluster member keys on the specified device. After
		// joining the cluster and having its CCR recovered, this node
		// has been able to register its keys on this device and is
		// its owner.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) _quorum_syslog_msgp->log(
			SC_SYSLOG_NOTICE, MESSAGE,
			"CMM: Registered key on and acquired quorum "
			"device %d (gdevname %s).",
			qid, _quorum_devices[qid].pathname);

		if (preempt_nodes_and_reserve(
		    qid, preempt_nodes, member_set)) {
			//
			// We cannot take ownership of the device
			// since the reservation failed.
			//
			owner_id = NODEID_UNKNOWN;
		}
	}

	CMM_TRACE(("take_ownership_after_register: device=%d: "
	    "owner_id=%d, preempt_nodes=0x%llx\n", qid, owner_id,
	    preempt_nodes.bitmask()));

	return (owner_id == _my_nodeid);
}


//
// preempt_nodes_and_reserve()
//
// This method is called by the take_ownership*() methods to perform
// preemption of the specified keys (of non cluster members) from the
// specified quorum device. If there are no nodes to be preempted, just
// place the local node's reservation on the device and call preempt
// with no nodes to preempt. This will release any held mutual exclusion
// mechanisms.
//
#ifdef _KERNEL
int
quorum_algorithm_impl::preempt_nodes_and_reserve(
    quorum::quorum_device_id_t	qid,
    const nodeset&		preempt_nodes,
    const nodeset&		member_set)
#else
int
quorum_algorithm_impl::preempt_nodes_and_reserve(
    quorum::quorum_device_id_t	qid,
    const nodeset&		preempt_nodes,
    const nodeset&		member_set)
#endif
{
	int			error = 0;
	nodeid_t		nid;
	Environment		env;

	CMM_TRACE(("Quorum: preempt_nodes_and_reserve called\n"));

	//
	// Since the owner field of quorum information is going to
	// be set here, the config lock should be held.
	//
	CL_PANIC(_quorum_config_mutex.lock_held());

	//
	// I am the owner of this device. If there are keys
	// to be preempted, I must do the preempting.
	//
	if (preempt_nodes.bitmask() != 0) {
		for (nid = 1; nid <= _max_nodeid; nid++) {
			//
			// Preempt nid if it is a dead node and has
			// a reservation key on the device.
			//
			if (preempt_nodes.contains(nid)) {
				quorum::registration_key_t	*v_key;
				v_key = (quorum::registration_key_t*)
				    (&RESERVATION_KEY(nid));

				error = _quorum_devices[qid].qd_v->
				    quorum_preempt(*my_key, *v_key, env);
				if (env.exception()) {
					CMM_TRACE(("preempt_nodes_and_reserve: "
					    "quorum_preempt returned an "
					    "exception.\n"));
					env.exception()->print_exception(
					    "preempt: ");
				}
				CL_PANIC(env.exception() == NULL);
				if (error) {
					// Preempt failed.
					CMM_TRACE(("quorum_preempt"
					    "(qid=%d),nid=%d) returned "
					    "error %d\n", qid, nid, error));
					//
					// SCMSGS
					// @explanation
					// This node was unable to preempt the
					// specified node from the quorum
					// device, indicating that the
					// partition to which the local node
					// belongs has been preempted and will
					// abort. If a cluster gets divided
					// into two or more disjoint
					// subclusters, exactly one of these
					// must survive as the operational
					// cluster. The surviving cluster
					// forces the other subclusters to
					// abort by grabbing enough votes to
					// grant it majority quorum. This is
					// referred to as preemption of the
					// losing subclusters.
					// @user_action
					// There may be other related messages
					// that may indicate why the partition
					// to which the local node belongs has
					// been preempted. Resolve the problem
					// and reboot the node.
					//
					(void) _quorum_syslog_msgp->log(
					    SC_SYSLOG_WARNING, MESSAGE,
					    "CMM: Preempting node %d from "
					    "quorum device %s failed with "
					    "error %d.", nid,
					    _quorum_devices[qid].pathname,
					    error);
					if (error == quorum::QD_EACCES) {
						//
						// Bug 4344983.
						// We can only conclude that
						// we have been preempted if
						// the error is QD_EACCES. This
						// will probably never happen
						// since failfast is enabled,
						// and the node would panic
						// with a Resv Conflict by now.
						//
						_we_have_been_preempted = true;
					}
					return (error);
				}
			}
		}
	} else if (idl_reservation_list.listlen == 0) {
		//
		// There are no reservations on the device. Since I am
		// the owner, I must place a reservation for the
		// cluster.
		//
		error = _quorum_devices[qid].qd_v->quorum_reserve(*my_key, env);
		if (env.exception()) {
			CMM_TRACE(("preempt_nodes_and_reserve: quorum_reserve "
			    "returned an exception\n"));
			env.clear();
		}
		if (error) {
			CMM_TRACE(("quorum_reserve(qid=%d) returned "
			    "error %d\n", qid, error));
			//
			// SCMSGS
			// @explanation
			// An error was encountered while trying to place a
			// reservation on the specified quorum device, hence
			// this node can not take ownership of this quorum
			// device.
			// @user_action
			// There may be other related messages on this and
			// other nodes connected to this quorum device that
			// may indicate the cause of this problem. Refer to
			// the quorum disk repair section of the
			// administration guide for resolving this problem.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Placing reservation on quorum "
			    "device %s failed.", _quorum_devices[qid].pathname);
		}
	} else {
		//
		// This corresponds to the case of me being the owner
		// already (ie having a reservation for the cluster)
		// and having had no nodes to preempt. In the case that
		// the quorum device is using a mutual exclusion primitive
		// to control access to the device, we should release it
		// now if the other node connected to the device is now in
		// the cluster membership.
		//
		bool other_node_alive = false;
		for (nid = 1; nid <= _max_nodeid; nid++) {
			if (nid == _my_nodeid)
				continue;
			if (_node_connected_to_quorum_device(
			    nid, qid) &&
			    member_set.contains(nid)) {
				other_node_alive = true;
				break;
			}
		}
		if (other_node_alive) {
			quorum::registration_key_t	v_key;
			for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
				v_key.key[i] = 0;
			}
			error = _quorum_devices[qid].qd_v->quorum_preempt(
			    *my_key, v_key, env);
			if (env.exception()) {
				CMM_TRACE(("preempt_nodes_and_reserve: "
				    "quorum_preempt of NULL key "
				    "returned an exception\n"));
				env.clear();
			}
			if (error) {
				CMM_TRACE(("quorum_preempt(qid=%d) "
				    "of NULL key failed.\n", qid));
				//
				// Lint throws informational message that
				// there is a conceivable use of a NULL
				// _quorum_devices below. This would not
				// be the case, as we allocate new memory
				// everytime quorum config changes.
				// So ignore this message
				//lint -e794
				//
				// SCMSGS
				// @explanation
				// This node encountered an error while trying
				// to release exclusive access to the
				// specified quorum device. The quorum code
				// will either retry this operation or will
				// ignore this quorum device.
				// @user_action
				// There may be other related messages that
				// may provide more information regarding the
				// cause of this problem.
				//
				(void) _quorum_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "CMM: Issuing a NULL Preempt "
				    "failed on quorum device %s with "
				    "error %d.",
				    _quorum_devices[qid].pathname, error);
				//lint +e794
			}
		}
	}

	//
	// If we are taking over this device from another node or
	// for the first time, log an event.nodeid_t
	//
	if (error == 0) {
		if (_quorum_devices[qid].owner != _my_nodeid) {
			//
			// SCMSGS
			// @explanation
			// The specified node has taken ownership of the
			// specified quorum device.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) _quorum_syslog_msgp->log(
			    SC_SYSLOG_NOTICE, PROPERTY_CHANGED,
			    "CMM: Quorum device %s: owner set to node %d.",
			    _quorum_devices[qid].pathname, _my_nodeid);
			_quorum_devices[qid].owner = _my_nodeid;
		}
	}
	return (error);
}

//
// find_resv_owner()
//
// Reads the reservation list on a quorum device, checks
// this list for a valid nodeid (cluster member), and returns
// that as the ownerid.
//
// Arguments:
// qid - quorum device id
// member_set - current cluster members
// owner_id - qd owner
//
// Return values:
// 0 - if no error in reading resv list
// !0 - error in reading resv list
//
int
quorum_algorithm_impl::find_resv_owner(
    quorum::quorum_device_id_t	qid,
    const nodeset&		member_set,
    nodeid_t&			owner_id)
{
	nodeid_t	nid;
	uint_t		i;
	int		error;

	owner_id = NODEID_UNKNOWN;

	//
	// Read the resv list to find if the reservation held by a
	// cluster member. If so, that node is the owner of the device.
	// If multiple nodes hold a reservation (allowed by the earliest
	// SCSI-3 specs), choose the lowest numbered node as the owner.
	//
	Environment env;
	CMM_TRACE(("find_resv_owner: Calling quorum_read_reservations\n"));
	idl_reservation_list.listlen = 0;
	error = _quorum_devices[qid].qd_v->quorum_read_reservations(
	    idl_reservation_list, env);

	if (env.exception()) {
		CMM_TRACE(("find_resv_owner: quorum_read_reservations "
		    "returned an exception\n"));
	}

	if (error) {
		CMM_TRACE(("quorum_read_reservations(qid=%d) returned error "
		    "%d.\n", qid, error));
		return (error);
	}

	// Sanity check.
	if (idl_reservation_list.listlen == 0)
		return (error);

	ASSERT(idl_reservation_list.listlen == 1);
	for (nid = 1; nid <= _max_nodeid; nid++) {
		for (i = 0; i < idl_reservation_list.listsize; i++) {
			struct mhioc_resv_key	*key;
			key = (struct mhioc_resv_key *)
			    &(idl_reservation_list.keylist[i].key); //lint !e545
			if (match_mhioc_keys(*key, RESERVATION_KEY(nid)) &&
			    member_set.contains(nid) &&
			    _node_connected_to_quorum_device(nid, qid)) {
				owner_id = nid;
				break;
			}
		}
		// If owner already found, no need to search further.
		if (owner_id != NODEID_UNKNOWN)
			break;
	}
	return (error);
}


//
// find_owner_and_nodes_to_preempt()
//
// This method determines if there are any non-cluster members in the
// idl_registration_list, and constructs a list containing these.
// If the owner arg is not set, then it also finds the lowest nodeid
// cluster member that is in the idl_registration_list.
// This method is called during general reconfiguration and after
// a node registers its keys on a QD. The just_registered flag indicates
// which case this is, and is used to take actions correspondingly.
//
void
quorum_algorithm_impl::find_owner_and_nodes_to_preempt(
    quorum::quorum_device_id_t	qid,
    const nodeset&		member_set,
    nodeset& 			preempt_nodes,
    nodeid_t&			owner_id,
    bool			just_registered)
{
	uint_t			i;
	nodeid_t		nid;

	// Return if registration list is empty
	if (idl_registration_list.listlen == 0) {
		CMM_TRACE(("find owner and nodes to preempt: empty list\n"));
		return;
	}

	//
	// Go through the reservation keys on the device. If there
	// are keys by non-members, add those members to the preempt
	// set. If the device owner has not been determined yet,
	// select the lowest numbered member that has its reservation
	// key on the device as the owner.
	//
	for (nid = 1; nid <= _max_nodeid; nid++) {
		// The local node just registered, so not on the list.
		if (just_registered && (nid == _my_nodeid)) {
			// The local node must be connected to the QD.
			CL_PANIC(_node_connected_to_quorum_device(nid, qid));
			if (owner_id == NODEID_UNKNOWN) {
				owner_id = nid;
			}
			// Go on to the next nodeid.
			continue;
		}

		for (i = 0; i < idl_registration_list.listlen; i++) {
			quorum::registration_key_t	*m_key;
			m_key = (quorum::registration_key_t *)
			    (&idl_registration_list.keylist[i]);
			if (null_idl_key(m_key)) {
				continue; // ignore null key
			}
			if (match_idl_keys(m_key, (quorum::registration_key_t *)
			    (&RESERVATION_KEY(nid)))) {
				if (!member_set.contains(nid)) {
					preempt_nodes.add_node(nid);
				} else if ((owner_id == NODEID_UNKNOWN) &&
				    _node_connected_to_quorum_device(
					nid, qid)) {
					owner_id = nid;
				}
				// Match found for this nid. Go to next.
				break;
			}
		}
	}
}


//
// reset_quorum_devices_bus()
//
// This method is called by acquire_quorum_devices() before it
// tries to access QDs, to reset the QD buses. This resetting
// of buses need only be done for non-fibre disks.
//
// If there are multiple QDs per bus, then this method will
// perform the bus reset that many times.
//
void
quorum_algorithm_impl::reset_quorum_devices_bus(
    membership_bitmask_t cluster_members)
{
	quorum_device_id_t	qid;
	nodeid_t		nid;
	nodeset			nodes_in_cluster;
	nodeset			nodes_with_paths;
	int			error;
	Environment		env;

	CMM_TRACE(("reset_quorum_devices_bus(0x%llx) called.\n",
	    cluster_members));

	nodes_in_cluster.set(cluster_members);

	for (qid = 1; qid <= _max_qid; qid++) {
		//
		// If this node is connected to this QD and it is
		// connected to a node that it can not talk to, do
		// a bus reset corresp to this QD.
		//

		//
		// The device has been marked not accessible, which means
		// an open of the device failed earlier.
		// So we skip this device.
		//
		if (!_quorum_devices[qid].accessible) {
			continue;
		}

		// An accessible QD will have a non-nil CORBA reference
		ASSERT(is_not_nil(_quorum_devices[qid].qd_v));

		// Get the nodes connected to this quorum device.
		nodes_with_paths.set(_quorum_table->quorum_devices.
		    list[qid-1].nodes_with_configured_paths);

		CL_PANIC(nodes_with_paths.contains(_my_nodeid));

		// Who else is connected to this qd, and are they up?
		for (nid = 1; nid <= _max_nodeid; nid++) {
			if (nid == _my_nodeid)
				continue;

			if ((!nodes_in_cluster.contains(nid)) &&
			    (nodes_with_paths.contains(nid))) {
				CMM_TRACE(("Node %d is not in cluster. "
				    "Calling quorum_reset "
				    "(qid=%d)\n", nid, qid));
				error = _quorum_devices[qid].qd_v->quorum_reset(
				    env);
				if (env.exception()) {
					CMM_TRACE(("quorum_reset_bus: quorum_"
					    "reset returned an exception\n"));
					env.exception()->print_exception(
					    "quorum_reset: ");
				}
				CL_PANIC(env.exception() == NULL);
				if (error) {
					CMM_TRACE(("quorum_reset(qid=%d) "
					    "failed.\n", qid));
					//
					// SCMSGS
					// @explanation
					// When a node connected to a quorum
					// device goes down, the surviving
					// node tries to reset the quorum
					// device. The reset can have
					// different effects on different
					// device types.
					// @user_action
					// Check to see if the device
					// identified above is accessible from
					// the node the message was seen on.
					// If it is accessible, then contact
					// your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					//
					(void) _quorum_syslog_msgp->log(
					    SC_SYSLOG_WARNING, MESSAGE,
					    "CMM: Resetting quorum "
					    "device %s failed.",
					    _quorum_devices[qid].pathname);
				} else {
					CMM_TRACE(("quorum_reset_bus(qid=%d) "
					    "succeeded.\n", qid));
				}
				//
				// No need to proceed with this QD since
				// we have attempted to reset it, and
				// there is no need to re-do the same thing.
				// This comment assumes of course that the
				// QD is connected to >2 nodes. If connected
				// to exactly two nodes, no need to look
				// for other nodes connected anyway.
				//
				break;
			}
		}
	}
}

//
// This function is passed a particular number of votes V and it returns
// the number of votes that constitutes quorum out of a total of V votes
// under the current membership properties being used by the cluster.
//
// Different types of membership properties have different definitions of
// how many votes constitutes quorum out of a particular number of votes.
// There can be individual validation checks for certain
// membership properties.
// For example, certain membership properties could impose restrictions
// on the total number of votes.
// This function does such validations and returns -1 in case of
// validation failure.
//
int
quorum(int total_votes)
{
	int quorum_votes = 0;

	if (total_votes < 0) {
		CMM_TRACE(("quorum() returning -1 : "
		    "total_votes = %d is less than 0\n", total_votes));
		return (-1);
	}

#if	defined(WEAK_MEMBERSHIP) && defined(_KERNEL)
	conf.membership_info_lock.lock();
	if (conf.multiple_partitions) {
		if ((total_votes == 1) || (total_votes == 2)) {
			quorum_votes = 1;
		} else if (total_votes == 0) {
			quorum_votes = 0;
		} else {
			//
			// We do not allow multiple partitions to survive
			// on a cluster having more than 2 nodes
			// (more than 2 votes).
			//
			CMM_TRACE((
			    "quorum() returning -1 : total_votes = %d, "
			    "multiple_partitions = true\n", total_votes));
			quorum_votes = -1;
		}
	} else {
		quorum_votes = (total_votes/2) + 1;
	}
	conf.membership_info_lock.unlock();

#else
	quorum_votes = (total_votes/2) + 1;
#endif

	return (quorum_votes);
}

#endif // _KERNEL_ORB
