/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SOFTWARE_FENCING_H
#define	_SOFTWARE_FENCING_H

#pragma ident	"@(#)software_fencing.h	1.4	08/09/02 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <h/quorum.h>
#include <cmm/cmm_debug.h>

/*
 * Software fencing support for software quorum implementation
 * -----------------------------------------------------------
 * For software quorum devices, we do not use hardware fencing.
 * Instead fencing is done by cluster software.
 * A software fencing thread is instantiated for each software quorum
 * device. The task of the thread is to wake up every polling period
 * (which is tunable) and try to read the current node's PGRe keys
 * registered on the software quorum device. If the node's keys cannot
 * be found, then it means that the node has been expelled from
 * the cluster, and hence the software fencing thread halts the node.
 */
class software_fencing {
protected:
	software_fencing(cmm_trace_level);
	software_fencing();
	virtual ~software_fencing();

	// Creates the polling thread, if not already done so.
	int initialize_failfast();

	// Puts the polling thread in enabled(active) state.
	void enable_failfast();

	// Puts the polling thread in disabled (inactive) state.
	void disable_failfast();

	// Signals the polling thread to exit, and waits till it dies.
	void shutdown_failfast();

private:
	virtual quorum::quorum_error_t callback_failfast() = 0;
	virtual char *get_pathname() = 0;

	static void worker_thread(void *);
	void polling_thread();

	/*
	 * The state_lock protects the thr_state (thread state) data.
	 * Any code that changes the thr_state must grab this lock.
	 *
	 * The polling thread holds this lock while doing its task
	 * of checking keys on the quorum disk.
	 * The polling thread leaves this lock before sleeping or exiting.
	 */
	os::mutex_t	state_lock;
	os::condvar_t	state_cv;

	os::mutex_t	oplock;

	bool initialized;

	//
	// DISABLED the thread is disabled.
	// ENABLED  the thread is enabled.
	// STOP the thread has been asked to stop.
	// DONE the thread processed the STOP request and exited.
	//
	enum fencing_thread_state {ENABLED, DISABLED, STOP, DONE};
	fencing_thread_state thr_state;

	cmm_trace_level software_fencing_trace_level;

	os::sc_syslog_msg *syslog_msgp;
};

#endif	/* _SOFTWARE_FENCING_H */
