//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)quorum_generic.cc	1.8	09/04/29 SMI"

#ifdef _KERNEL // quorum is used only by the kernel CMM

#include <quorum/common/quorum_generic.h>

//
// IDL method to set the message tracing level of this quorum device
//
void
quorum_device_generic_impl::set_message_tracing_level(
    uint32_t new_trace_level, Environment &)
{
	trace_level = (cmm_trace_level)new_trace_level;
}

//
// IDL method to get the message tracing level of this quorum device
//
uint32_t
quorum_device_generic_impl::get_message_tracing_level(Environment &)
{
	return ((uint32_t)trace_level);
}

//
// quorum_open
//
// Opens a quorum device by a local path name and gets a handle
// of type vnode_t *. This handle is used in all other calls.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_open(const char *devname, const bool scrub,
    const quorum::qd_property_seq_t &, Environment &)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s)::quorum_open\n", devname));

	int error = 0;
	const int	max_retries = 5;	// # of times to retry vn_open.
						// Perhaps this should be made
						// settable through /etc/system.
	int		retries_remaining;

	//
	// Make a copy of the pathname for debugging purposes.
	// A particular quorum device object is associated with
	// a single configured quorum device always.
	// It is possible that an earlier quorum_open attempt
	// has initialized the pathname.
	// If so, we do not need to duplicate the string again.
	//
	if (pathname == NULL) {
		// First open attempt
		pathname = os::strdup(devname);
	} else {
		CL_PANIC(os::strcmp(pathname, devname) == 0);
	}

	//
	// TEMPORARY: The following retry code is added to work
	// around a bug in the ssd driver or the driver framework
	// which results in a small window of time right after the
	// root file system is mounted during which vn_open may
	// return an ENXIO error. If we get this error, retry a
	// few times.
	//
	// It turns out that the vn_open code could fail so early.
	// We do not trust the driver code to return correct error codes.
	// So we retry a number of times in case of failure.
	// In case the error returned is EACCES, then we do not retry
	// and return QD_EACCES which means we will retry
	// after reconfiguration completes.
	// However, if the vn_open keeps on returning other errors
	// even after a few retries, then :
	// (i) if error is EACCES, we return QD_EACCES which means
	// we will retry after reconfiguration completes,
	// (ii) if any other error, we return QD_EIO which means
	// the device reference is released.
	//
	// Use FNDELAY flag for filemode (3rd arg) below so that vn_open
	// succeeds even if the disk is reserved by a SCSI2 reservation.
	//
	retries_remaining = max_retries;
	do {
		error = vn_open((char *)devname, UIO_SYSSPACE,
		    (FREAD | FNDELAY), 0, &vnode_ptr, CRCREAT, 0);
		retries_remaining--;
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s): quorum_open - "
			    "vn_open returned error %d\n", pathname, error));
		}
		if ((error == 0) || (error == EACCES)) {
			break;
		}
		os::usecsleep((os::usec_t)MICROSEC);	// sleep 1 second
	} while (retries_remaining);
	if (error) {
		vnode_ptr = NULL;
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_open - vn_open failed "
		    "after %d trials\n", pathname,
		    max_retries - retries_remaining));
		if (error == EACCES) {
			return (quorum::QD_EACCES);
		} else {
			return (quorum::QD_EIO);
		}
	} else if (retries_remaining != max_retries) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QUORUM_GENERIC(%s): quorum_open - "
		    "vn_open returned 0 after %d trials\n",
		    pathname, max_retries - retries_remaining));
	}

	//
	// If the scrub boolean is set to true, this is the first time we have
	// accessed a new quorum device, and we need to scrub it.
	//
	if (scrub) {
		error = quorum_scrub(vnode_ptr);
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s): quorum_open got error "
			    "%d trying to scrub device.\n", pathname, error));
		}
	}

	// Now we need to set up the reserved sectors for quorum info.
	error = quorum_disk_initialize_reserved(vnode_ptr, sblkno, trace_level);

	//
	// Accessing node expects an EACCES if device is fenced out
	// but driver might return an EIO, either case we retry
	// Here EIO is mapped intentionally to QD_EACCES so that
	// quorum_open is retried later rather than giving up
	//
	if (error == EACCES || error == EIO) {
		//
		// Our first open of the QD has failed. We'll retry this
		// later, when the fist read_keys is issued.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_open: Failed to "
		    "initialize reserved sectors. Returned %d."
		    " Will retry later.\n", pathname, error));

		return (quorum::QD_EACCES);
	}

	if (error == 0) {
		//
		// Because we are also going to be using PGRe for key
		// management, we need to initialize the PGRe variables
		// on storage. If this call fails, but the initialize
		// reserved call succeeded, then we know there is a real
		// problem, and we won't retry the access.
		//
		error = quorum_pgre_initialize(vnode_ptr, sblkno, trace_level);

		if (error != 0) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s) quorum_open: Failed to "
			    "initialize pgre data.\n", pathname));
			return (quorum::QD_EIO);
		}
	} else {
		// A real error. Print error messages.
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s) open: got an unexpected error\n",
		    pathname));
		return (quorum::QD_EIO);
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_close
//
// Close the vnode off and release the pointer.
//
// Callers of quorum_open() and quorum_close() should ensure
// that for a particular quorum device object, quorum_close()
// must be invoked between two successive quorum_open() calls
// for the same quorum device object.
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_close(Environment &)
{
	if (vnode_ptr != NULL) {
#if	SOL_VERSION >= __s11
		(void) VOP_CLOSE(vnode_ptr, FREAD, 1, (offset_t)0, kcred, NULL);
#else
		(void) VOP_CLOSE(vnode_ptr, FREAD, 1, (offset_t)0, kcred);
#endif
		VN_RELE(vnode_ptr);
		vnode_ptr = NULL;
	}

	sblkno = INV_BLKNO;
	return (quorum::QD_SUCCESS);
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster. The quorum_amnesia_level enum
// is defined in quorum_impl.h
//
// Lint throws informational message that this method could be declared const.
// We do not want to do that because the classes that derive from
// this generic class implement the quorum device types, and
// the IDL method in the derived class corresponding to this method
// is not declared const. So if we make this method const, the compiler
// would throw out warnings.
//lint -e1762
uint32_t
quorum_device_generic_impl::quorum_supports_amnesia_prot(Environment &)
{
	return (AMN_SC30_SUPPORT);
}
//lint +e1762

//
// quorum_reserve
//
// Place this node's reservation key on the device.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	os::sc_syslog_msg	*syslog_msgp;
	char			nodename[8];

	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	if (vnode_ptr == NULL) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("QUORUM_GENERIC(%s): quorum_reserve - "
		    "returning EIO due to NULL vnode_ptr.\n", pathname));
		return (quorum::QD_EIO);
	}

	// Make sure a zero key is not being written

	uint64_t	zero_key = 0x0;
	quorum::registration_key_t	zero_reg_key;

	int64_key_to_idl_key(zero_key, &zero_reg_key);
	CL_PANIC(!match_idl_keys(&zero_reg_key, &my_key));

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_reserve called for key 0x%llx.\n",
	    pathname, idl_key_to_int64_key(&my_key)));

	int error;
	quorum::registration_key_t key_on_disk;

	// First check if this key has been registered.
	uint64_t our_sblkno = PGRE_GET_NODE_BLKNO(sblkno,
	    orb_conf::local_nodeid());

	error = quorum_pgre_key_read(vnode_ptr, our_sblkno,
	    &key_on_disk);
	if (error != 0) {
		syslog_msgp = new os::sc_syslog_msg(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// An error was encountered when reserving
		// the specified quorum device.
		// The registration keys could not be read
		// from the quorum device, before attempting
		// to reserve it. This error occurs because of
		// unexpected behavior from the device driver,
		// the multipathing driver, or the device's firmware.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine if a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "QUORUM_GENERIC: quorum_reserve error: "
		    "reading registration keys on quorum device %s "
		    "failed with error code %d.", pathname, error);

		delete syslog_msgp;
		return (quorum::QD_EACCES);
	}

	if (match_idl_keys(&my_key, &key_on_disk) != 1) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("quorum_pgre_key_read() could not find key on disk.\n"));
		error = EACCES;
		syslog_msgp = new os::sc_syslog_msg(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// When reserving the quorum device,
		// the node's registration key was not found
		// on the device. This error occurs because of
		// unexpected behavior from the device driver,
		// the multipathing driver, or the device's firmware.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine if a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "QUORUM_GENERIC: quorum_reserve error: "
		    "this node's registration key was not found "
		    "on the quorum device %s.", pathname);

		delete syslog_msgp;
		return (quorum::QD_EACCES);
	}

	//
	// Write the reservation key only if
	// registration key is found
	//
	error = quorum_pgre_key_write(vnode_ptr,
	    sblkno, &my_key, true);
	if (error != 0) {
		syslog_msgp = new os::sc_syslog_msg(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// An error was encountered when
		// reserving the specified quorum device.
		// The reservation key could not be written
		// on the quorum device. This error occurs because of
		// unexpected behavior from the device driver,
		// the multipathing driver, or the device's firmware.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine if a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "QUORUM_GENERIC: quorum_reserve error: "
		    "writing reservation key on quorum device %s "
		    "failed with error code %d.", pathname, error);

		delete syslog_msgp;
		return (quorum::QD_EACCES);
	}

	//
	// Read back and check if the reservation key got written
	// correctly.
	//
	quorum::registration_key_t 	key_check;

	error = quorum_pgre_key_read(vnode_ptr, sblkno, &key_check);
	if (error == 0) {
		if (match_idl_keys(&key_check, &my_key) != 1) {

			syslog_msgp = new os::sc_syslog_msg(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// An error was encountered when placing
			// the node's reservation key on the specified
			// quorum device. The write operation of the key
			// succeeded, but the read operation to confirm
			// the presence of the key failed.
			// This error occurs because of
			// unexpected behavior from the device driver,
			// the multipathing driver, or the device's
			// firmware.
			// @user_action
			// Contact your authorized Sun service
			// provider to determine if a workaround or
			// patch is available.
			//
			(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "QUORUM_GENERIC: quorum_reserve error: "
			    "Read check found node's key is absent on "
			    "quorum device %s.", pathname);

			delete syslog_msgp;
			//
			// Write operation was successful, but
			// the read check failed.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s): quorum_reserve "
			    "operation failed to write key on device ",
			    pathname));
			return (quorum::QD_EIO);

		} else if (match_idl_keys(&zero_reg_key, &key_check)) {

			syslog_msgp = new os::sc_syslog_msg(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "QUORUM_GENERIC: quorum_reserve error: "
			    "Read check found node's key is absent on "
			    "quorum device %s.", pathname);

			delete syslog_msgp;
		} else {
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("QUORUM_GENERIC(%s): quorum_reserve "
			    "read check has succeeded\n", pathname));
		}
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_reserve wrote reservation "
	    "0x%llx to device.\n", pathname, idl_key_to_int64_key(&my_key)));

	return (quorum::QD_SUCCESS);
}

//
// quorum_read_reservations
//
// Read all of the reservations on the device. Currently this is
// hard coded to only return a single reservation.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_read_reservations(
    quorum::reservation_list_t		&resv_list,
    Environment			&)
{
	int error = 0;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_read_reservations\n", pathname));

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	if (this->check_fi((char *)"quorum_read_reservations") != 0) {
		return (quorum::QD_EIO);
	}
#endif // FAULT_CMM && _KERNEL_ORB

	if (resv_list.listsize < 1) {
		//
		// If the list isn't at least 1 big, return and this will
		// be retried.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("QUORUM_GENERIC(%s): "
		    "read_reservations passed 0 size list\n", pathname));
		resv_list.listlen = 1;
		return (quorum::QD_SUCCESS);
	}

	//
	// Assume only one reservation - which is what actually
	// happens even for the SCSI3 PGR version.
	//
	quorum::registration_key_t 	resv_key;

	resv_list.listlen = 0;
	error = quorum_pgre_key_read(vnode_ptr, sblkno, &resv_key);
	if (error == 0) {
		// Reservation is valid only if not a NULL_KEY.
		if (!null_idl_key(&resv_key)) {
			//
			// Lint throws warning when address of array is obtained
			//lint -e545
			bcopy(&(resv_key.key),
			    &(resv_list.keylist[0].key),
			    sizeof (quorum::registration_key_t));
			//lint +e545
			resv_list.listlen++;
		}
	}

	//
	// No special action is needed if no key is returned, because
	// there may simply be no reservation on the device.
	//

	if (resv_list.listlen > 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QUORUM_GENERIC(%s): quorum_read_reservations returned:\n",
		    pathname));

		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("resv[%d]: key=0x%llx\n", 1,
		    idl_key_to_int64_key(&(resv_list.keylist[0]))));
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_preempt
//
// Remove the victim key (both registration and reservation) from the device
// and place a reservation using my_key. If victim_key is NULL, that means
// that we need to release any mutual exclusion that we hold on the device.
// In that case, call release.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&)
{
	int		error = 0;

	ASSERT(vnode_ptr != NULL);

	//
	// If victim_key is NULL, issue a release.
	// Intuitively this corresponds to the case when we are not preempting
	// anyone and therefore everyone should have access to the device.
	// Since quorum modules use a mutual exclusion access control, this
	// is the mechanism for releasing the control.
	//
	if (null_idl_key(&victim_key)) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QUORUM_GENERIC(%s): quorum_preempt called with "
		    "NULL victim key, calling release.\n", pathname));
		error = this->reservation_release();

		if (error == EACCES) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s): quorum_preempt - "
			    "reservation_release returned error %d\n",
			    pathname, error));
			return (quorum::QD_EACCES);
		} else if (error != 0) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s): quorum_preempt - "
			    "reservation_release returned error %d\n",
			    pathname, error));
			return (quorum::QD_EIO);
		}

		// Write out our key in the resv area.
		error = quorum_pgre_key_write(vnode_ptr,
		    sblkno, &my_key, true);
		//
		// If victim_key was NULL, we just needed to do the release,
		// so go ahead and return now.
		//
		return (quorum::QD_SUCCESS);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_preempt called for key 0x%llx\n",
	    pathname, idl_key_to_int64_key(&victim_key)));

	uint64_t 			their_sblkno;
	quorum::registration_key_t 	key_on_disk;
	nodeid_t			my_nodeid;

	ASSERT(!null_idl_key(&my_key));
	ASSERT(!null_idl_key(&victim_key));	// handled above.

	my_nodeid = orb_conf::local_nodeid();
	uint64_t our_sblkno = PGRE_GET_NODE_BLKNO(sblkno, my_nodeid);

	//
	// All the disk-related steps for this preempt primitive needs
	// to be protected with mutual exclusion primitives. Use
	// Lamport's algorithm for implementing this distributed ME.
	//
	error = quorum_pgre_lock(vnode_ptr, sblkno, my_nodeid);
	if (error != 0) {
		// Log the error and return failure to preempt.
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_preempt: "
		    "quorum_pgre_lock() retd error %d.\n", pathname, error));
		return (quorum::QD_EACCES);
	}

	error = quorum_pgre_key_read(vnode_ptr, our_sblkno, &key_on_disk);
	if (error == 0) {
		if (match_idl_keys(&my_key, &key_on_disk) == 0) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
			    ("QUORUM_GENERIC(%s)quorum_preempt: "
			    "my_key not registered. my_key = 0x%llx, "
			    "key on disk = 0x%llx\n", pathname,
			    idl_key_to_int64_key(&my_key),
			    idl_key_to_int64_key(&key_on_disk)));
			error = EACCES;
		} else {
			//
			// Remove the victim_key both from resv and
			// register areas. Search for the victim_key.
			//
			error = quorum_pgre_key_find(
			    vnode_ptr, sblkno, &victim_key, their_sblkno);
			//
			// If victim_key is not registered, do not
			// treat that as an error cond, but just go
			// ahead and put resv. in.
			//
			if (error != 0) {
				if (error == ENOENT) {
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_AMBER,
					    ("QUORUM_GENERIC(%s): "
					    "quorum_preempt: victim_key not "
					    "registered.\n", pathname));
					// So as to put resv in anyway.
					error = 0;
				} else {
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("QUORUM_GENERIC(%s): "
					    "quorum_preempt: "
					    "quorum_pgre_key_find retd error "
					    "%d.\n", pathname, error));
				}
			} else {
				error = quorum_pgre_key_delete(
				    vnode_ptr, their_sblkno, false);
				if (error != 0) {
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("QUORUM_GENERIC(%s): "
					    "quorum_preempt: "
					    "quorum_pgre_key_delete "
					    "retd error %d.\n", pathname,
					    error));
				} else {
					//
					// Removing from resv area not
					// reqd, since it will be over-
					// written. So no need to check
					// for error here.
					//
					(void) quorum_pgre_key_delete(
					    vnode_ptr, sblkno, true);
				}
			}

			if (error == 0) {
				// Write out our key in the resv area.
				error = quorum_pgre_key_write(vnode_ptr,
				    sblkno, &my_key, true);
			}
		}
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_preempt: "
		    "quorum_pgre_key_read returned error %d.\n",
		    pathname, error));
	}

	if (error == 0) {
		//
		// If the preempt PGRE was successful, then at this
		// point, issue a QUORUM_GENERIC MHIOCTKOWN ioctl to prevent
		// the "losing" node from accessing the disk.
		//
		// This ioctl should be issued at this point since
		// only now have all the preempt subops succeeded,
		// and it is safe to take ownership.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QUORUM_GENERIC(%s): quorum_preempt: issuing a "
		    "MHIOCTKOWN.\n", pathname));
		error = this->reservation_tkown();
		if (error) {
			os::sc_syslog_msg	*syslog_msgp;
			char			nodename[8];

			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QUORUM_GENERIC(%s): quorum_preempt: "
			    "reservation_tkown() retd error %d.\n",
			    pathname, error));
			os::sprintf(nodename, "%d", orb_conf::local_nodeid());
			syslog_msgp = new os::sc_syslog_msg(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// This node encountered an error when issuing a
			// QUORUM_GENERIC Take Ownership operation
			// on a quorum device.
			// This error indicates that the node was
			// unsuccessful in preempting keys from the quorum
			// device, and the partition to which it
			// belongs was preempted. If a cluster is
			// divided into two or more disjoint subclusters,
			// one of these must survive as the
			// operational cluster. The surviving cluster forces
			// the other subclusters to abort by gathering enough
			// votes to grant it majority quorum. This action
			// is called "preemption of the losing subclusters".
			// @user_action
			// Other related messages identify the quorum device
			// where the error occurred. If an EACCES error occurs,
			// the QUORUM_GENERIC command might have failed because
			// of the SCSI3 keys on the quorum device.
			// Scrub the SCSI3 keys off the quorum device
			// and reboot the preempted nodes.
			//
			(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "QUORUM_GENERIC: quorum preempt error in CMM: "
			    "Error %d --- QUORUM_GENERIC Tkown ioctl failed on "
			    "quorum device %s.", error, pathname);

			delete syslog_msgp;
		}
	}

	//
	// Not checking for return code from the following call,
	// because not sure how to handle it, except to log it, which
	// is being done in the fn anyway.
	//
	(void) quorum_pgre_unlock(vnode_ptr, sblkno, my_nodeid);

	//
	// XXX The above sequence of if-else based on values retd from
	// error is pretty complicated. Is there a better way to write
	// this with all the errors being checked from all the scs2_*
	// calls? Also, what should be done if all else succeeds but
	// the last write fails? This fn will return an error, but
	// victim_key has been removed. Is the preempt supposed to do
	// all steps or none? This is probably a pathological case, and
	// the worst sideeffect is that some node's reservation got
	// deleted without a new owner taking over.
	//

	if (error == EACCES) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_preempt returning "
		    "QD_EACCES\n", pathname));
		return (quorum::QD_EACCES);
	} else if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_preempt got error %d, "
		    "so returning QD_EIO\n", pathname, error));
		return (quorum::QD_EIO);
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_register
//
// Write this node's key on the device as a registration.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	int		error;
	uint64_t	key = idl_key_to_int64_key(&my_key);
	uint64_t	zero_key = 0x0;
	quorum::registration_key_t	zero_reg_key;
	os::sc_syslog_msg	*syslog_msgp;
	char			nodename[8];

	os::sprintf(nodename, "%d",
	    orb_conf::local_nodeid());

	int64_key_to_idl_key(zero_key, &zero_reg_key);

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_register.\n", pathname));

	// Make sure a zero key is not being written

	CL_PANIC(!(key == 0x0));

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("   key=0x%llx\n", key));

	//
	// Write the key on the PGRE portion of the QUORUM_GENERIC disk. Need to
	// offset the sblkno with the nodeid, with each node taking up
	// a sector, and the first being taken up the group owner.
	// For unode, a sector will corresp to the size of the corresp
	// data structure/class that holds a node specific information.
	//
	uint64_t our_sblkno;

	// The starting point on disk to write is offset by nodeid.
	our_sblkno = PGRE_GET_NODE_BLKNO(sblkno,
	    orb_conf::local_nodeid());

	error = quorum_pgre_key_write(vnode_ptr, our_sblkno, &my_key, false);

	if (error == EACCES) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC: quorum_register, "
		    "quorum_pgre_key_write returned error %d\n", error));
		return (quorum::QD_EACCES);

	} else if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QUORUM_GENERIC: quorum_register, "
		    "quorum_pgre_key_write returned error %d\n", error));
		syslog_msgp = new os::sc_syslog_msg(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// An error was encountered when placing
		// the node's registration key on a specified
		// quorum device. This error occurs because of
		// unexpected behavior from the device driver,
		// the multipathing driver, or the device's firmware.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine if a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "QUORUM_GENERIC: quorum_register error: "
		    "writing registration key on quorum device %s "
		    "failed with error code %d.", pathname, error);

		delete syslog_msgp;
		return (quorum::QD_EIO);
	} else {
		//
		// Read back and check if the registration key got written
		// correctly.
		//
		quorum::registration_key_t 	key_check;

		error = quorum_pgre_key_read(vnode_ptr, our_sblkno,
			    &key_check);
		if (error == 0) {
			if (match_idl_keys(&key_check, &my_key) != 1) {

				syslog_msgp = new os::sc_syslog_msg(
				    SC_SYSLOG_CMM_QUORUM_TAG,
				    nodename, NULL);
				//
				// SCMSGS
				// @explanation
				// An error was encountered when placing
				// the node's registration key on a specified
				// quorum device. The write operation was
				// successful, but a read check to confirm the
				// presence of the key failed to match the key.
				// This error occurs because of unexpected
				// behavior from the device driver,
				// the multipathing driver, or
				// the device's firmware.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine if a workaround or
				// patch is available.
				//
				(void) syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "QUORUM_GENERIC: quorum_register error: "
				    "registration key read check failed on "
				    "quorum device %s.", pathname);

				delete syslog_msgp;
				//
				// Write operation returned success but
				// read check failed
				//
				CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
				    ("QUORUM_GENERIC(%s): quorum_register "
				    "operation failed to write key on device ",
				    pathname));
				return (quorum::QD_EIO);
			} else if (match_idl_keys(&key_check,
			    &zero_reg_key) == 1) {

				syslog_msgp = new os::sc_syslog_msg(
				    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
				(void) syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "QUORUM_GENERIC: quorum_register error: "
				    "registration key read check failed on "
				    "quorum device %s.", pathname);

				delete syslog_msgp;
				//
				// Write operation returned success but
				// read check failed
				//
				CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
				    ("QUORUM_GENERIC(%s): quorum_register "
				    "operation failed to write key on device ",
				    pathname));
				return (quorum::QD_EIO);
			} else {
				CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
				    ("QUORUM_GENERIC(%s): quorum_register "
				    "read check has succeeded\n",
				    pathname));
			}
		}
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QUORUM_GENERIC: quorum_register wrote key 0x%llx "
		    "to sblkno 0x%llx.\n", key, our_sblkno));
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_read_keys - IDL method
//
// Read the registration keys on the device. Fill in the reg_list up to a
// maximum of reg_list->listsize keys. Set reg_list->listlen to the number
// of total keys that are on the device.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_read_keys(
    quorum::reservation_list_t	&reg_list,
    Environment			&e)
{
	quorum::quorum_error_t q_err =
	    quorum_read_keys_internal(reg_list, trace_level, e);
	return (q_err);
}

//
// quorum_read_keys_internal
// - Protected method used by the class and its derivatives only
//
// Read the registration keys on the device. Fill in the reg_list up to a
// maximum of reg_list->listsize keys. Set reg_list->listlen to the number
// of total keys that are on the device.
//
quorum::quorum_error_t
quorum_device_generic_impl::quorum_read_keys_internal(
    quorum::reservation_list_t	&reg_list,
    cmm_trace_level		trace_level_to_use,
    Environment			&)
{
	uint_t		i;
	int		error;
	os::sc_syslog_msg	*syslog_msgp;
	char			nodename[8];

	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	if (sblkno == INV_BLKNO) {
		//
		// When we tried to access the device earlier, we were
		// fenced off. This is the retry point.
		//

		// Now we need to set up the reserved sectors for quorum info.
		error = quorum_disk_initialize_reserved(
		    vnode_ptr, sblkno, trace_level_to_use);

		if (error != 0) {
			CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_RED,
			    ("quorum_read_keys: returning error due to "
			    "failure to initialize reserved sectors.\n"));
			return (quorum::QD_EACCES);
		}

		//
		// Because we are also going to be using PGRe for key
		// management, we need to initialize the PGRe variables
		// on storage.
		//
		error = quorum_pgre_initialize(
		    vnode_ptr, sblkno, trace_level_to_use);

		if (error != 0) {
			CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_RED,
			    ("quorum_read_keys: returning error due to "
			    "failure to initialize pgre.\n"));
			return (quorum::QD_EACCES);
		}
	}

	//
	// Issue reads for all the valid entries on disk and fill in
	// the keylist data structure. Do this sector by sector here.
	//

	quorum::registration_key_t 	node_key;
	bool			need_retry = true;
	size_t 			key_size = sizeof (quorum::registration_key_t);

	//
	// We can guess about the number of keys on the device, but we don't
	// know until we've looked at all the sectors. If there are too many
	// keys to fit into the reg_list, we will resize the reg_list and
	// retry. This will eventually (usually after 1 iteration) result
	// in an appropriately sized list.
	//
	while (need_retry) {
		need_retry = false;
		reg_list.listlen = 0;

		// Node areas start from the 2nd sector.
		for (i = 1; i <= NODEID_MAX; i++) {
			error = quorum_pgre_key_read(vnode_ptr,
			    PGRE_GET_NODE_BLKNO(sblkno, i), &node_key);
			if ((error == 0) && (!null_idl_key(&node_key))) {
				// Fill in key only if key is not null.
				uint_t llen = reg_list.listlen;
				uint_t lsize = reg_list.listsize;
				// Fill in the keys only if space avail.
				if (llen < lsize) {
					bcopy(&node_key,
					    &(reg_list.keylist[llen]),
					    key_size);
				}
				reg_list.listlen++;

			} else if (error != 0 && error != EACCES) {
				syslog_msgp = new os::sc_syslog_msg(
				    SC_SYSLOG_CMM_QUORUM_TAG,
				    nodename, NULL);
				//
				// SCMSGS
				// @explanation
				// An error was encountered when reading
				// a node's registration key on a specified
				// quorum device. This error occurs because of
				// unexpected behavior from the device driver,
				// the multipathing driver, or
				// the device's firmware.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine if a workaround or
				// patch is available.
				//
				(void) syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "QUORUM_GENERIC: quorum_read_keys error: "
				    "Reading the registration keys failed on "
				    "quorum device %s with error %d.",
				    pathname, error);

				CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_RED,
				    ("QUORUM_GENERIC(%s): quorum_read_keys: "
				    "quorum_pgre_key_read() retd error %d.\n",
				    pathname, error));
				break;
			}
		}

		// Check to see if the list overflowed.
		if (reg_list.listlen > reg_list.listsize) {
			need_retry = true;
			reg_list.listsize = reg_list.listlen;
			reg_list.keylist.length(reg_list.listlen);
		}
	}

	if (error == EACCES) {
		CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_read_keys got error %d, "
		    "returning QD_EACCES.\n", pathname, error));
		return (quorum::QD_EACCES);
	} else if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_RED,
		    ("QUORUM_GENERIC(%s): quorum_read_keys got error %d, "
		    "returning QD_EIO.\n", pathname, error));
		return (quorum::QD_EIO);
	}

	CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_read_keys returned %d keys:\n",
	    pathname, reg_list.listlen));
	for (i = 0; i < reg_list.listlen; i++) {
		CMM_SELECTIVE_TRACE(trace_level_to_use, CMM_GREEN,
		    ("key[%d]: key=0x%llx\n", i,
		    idl_key_to_int64_key(&(reg_list.keylist[i]))));
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_remove
//
// Remove the device.
//
// Lint throws informational message that this method could be declared const.
// We do not want to do that because the classes that derive from
// this generic class implement the quorum device types, and
// the IDL method in the derived class corresponding to this method
// is not declared const. So if we make this method const, the compiler
// would throw out warnings.
//lint -e1762
quorum::quorum_error_t
quorum_device_generic_impl::quorum_remove(const char *devname,
    const quorum::qd_property_seq_t &,
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QUORUM_GENERIC(%s): quorum_remove\n", devname));

	return (quorum::QD_SUCCESS);
}
//lint +e1762

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
int
quorum_device_generic_impl::check_fi(char *qop_name)
{
	// Fault point will return failure
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    &fault_argp, &fault_argsize)) {
		char *exp_path = (char *)fault_argp;
		char *cur_path = pathname;
		if ((exp_path == NULL) ||
		    ((cur_path != NULL) && (strcmp(cur_path, exp_path) == 0))) {
			os::printf("==> Fault Injection failing %s operation\n",
			    qop_name);
			return (EIO);
		}
	}
	return (0);
}
#endif // FAULT_CMM && _KERNEL_ORB
#endif // _KERNEL
