/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _QUORUM_UTIL_H
#define	_QUORUM_UTIL_H

#pragma ident	"@(#)quorum_util.h	1.8	08/05/20 SMI"

#include <h/quorum.h>

//
// Utility functions for comparing mhioc and idl keys to each other and
// null key.
//
extern int
match_mhioc_keys(const mhioc_resv_key_t&, const mhioc_resv_key_t&);

extern int
match_idl_keys(const quorum::registration_key_t *,
    const quorum::registration_key_t *);

extern int
null_mhioc_key(const mhioc_resv_key_t&);

extern int
null_idl_key(const quorum::registration_key_t *);


#include <quorum/common/quorum_util_in.h>

extern uint64_t
key_value(quorum::registration_key_t &key);

#endif /* _QUORUM_UTIL_H */
