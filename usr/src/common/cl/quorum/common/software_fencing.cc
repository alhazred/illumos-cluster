//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)software_fencing.cc	1.5	08/09/02 SMI"

#include <quorum/common/software_fencing.h>
#include <cmm/cmm_debug.h>
#include <orb/infrastructure/clusterproc.h>

//
// Software fencing support for software quorum implementation
// -----------------------------------------------------------
// For software quorum devices, we do not use hardware fencing.
// Instead fencing is done by cluster software.
// A software fencing thread is instantiated for each software quorum
// device. The task of the thread is to wake up every polling period
// (which is tunable) and try to read the current node's PGRe keys
// registered on the software quorum device. If the node's keys cannot
// be found, then it means that the node has been expelled from
// the cluster, and hence the software fencing thread halts the node.
//

//
// Polling period (in seconds) for software fencing thread.
//
// This is the polling period used by the software fencing thread
// for each software quorum shared disk. The fencing thread for a sq disk
// checks, once every polling period, to see if the disk still has
// the current node's registration key on it. If not, the fencing thread
// panics the current node.
//
// This polling period is tunable. It can be set in the /etc/system file.
// This is for use by Sun Personnel only.
//
// XXX Should we have separate polling periods for sq_disk and sq_nas?
// XXX (NAS support could be implemented in future)
// XXX In that case, we have a problem since software_fencing class is generic
// XXX and common to both software quorum for shared disk and NAS.
//
unsigned int software_fencing_polling_period = 2; // seconds

//
// Constructor
//
software_fencing::software_fencing(cmm_trace_level level) :
	initialized(false),
	thr_state(DONE),
	software_fencing_trace_level(level)
{
	char nodename[8];
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());
	//
	// Lint throws informational message that memory is allocated
	// and assigned to a data member, but there is no
	// assignment or copy constructor defined for the class;
	// and that this could lead to problems as default assignment
	// and copy constructors cannot handle such cases correctly.
	// We ignore this message as we do not use assignment or copy
	// for this class.
	//lint -e1732 -e1733
	syslog_msgp = new os::sc_syslog_msg(
	    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
	//lint +e1732 +e1733
}

//
// Default constructor.
// Should not be used.
//
software_fencing::software_fencing() :
	initialized(false),
	thr_state(DONE),
	software_fencing_trace_level(CMM_GREEN),
	syslog_msgp(NULL)
{
	ASSERT(0);
}

//
// Destructor
//
software_fencing::~software_fencing()
{
	initialized = false;
	thr_state = DONE;
	delete syslog_msgp;
}

//
// initialize
//
// Start the polling thread.
//
// return 0 in case of success.
// return 1 in case of failure.
//
int
software_fencing::initialize_failfast()
{
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::initialize_failfast called.\n"));
	oplock.lock();

	if (!initialized) {
		thr_state = DISABLED;
		if (clnewlwp(software_fencing::worker_thread, (caddr_t)this, 0,
		    orb_conf::rt_classname(), NULL) != 0) {
			CMM_SELECTIVE_TRACE(
			    software_fencing_trace_level, CMM_RED,
			    ("software_fencing::initialize_failfast: "
			    "Failed to create thread."));
			thr_state = DONE;
			oplock.unlock();
			return (1);
		}
		initialized = true;
	}

	oplock.unlock();
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::initialize_failfast done.\n"));
	return (0);
}

//
// worker_thread
//
// Static entry point for the thread creation function.
//
void
software_fencing::worker_thread(void *arg)
{
	software_fencing *sfp = (software_fencing *)arg;
	sfp->polling_thread();
}

//
// polling_thread
//
// This function is the polling thread main loop. The polling thread
// wakes up periodically and invokes the failfast callback method.
//
void
software_fencing::polling_thread()
{
	os::systime sys_timeout;
	quorum::quorum_error_t failfast_status;

	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::polling_thread starting for quorum device %s\n",
	    this->get_pathname()));

	state_lock.lock();
	//
	// While the fencing thread is up
	// (in enabled or disabled polling state).
	//
	while ((thr_state == ENABLED) || (thr_state == DISABLED)) {

		while (thr_state == DISABLED) {
			CMM_SELECTIVE_TRACE(
			    software_fencing_trace_level, CMM_GREEN,
			    ("software_fencing::polling_thread waits "
			    " in disabled state.\n"));
			state_cv.wait(&state_lock);
		}
		if (thr_state == STOP) {
			break;
		}

		//
		// Check if the current node has access to the quorum device
		// signifying that it is part of the cluster membership.
		// If not, commit suicide.
		//
		failfast_status = this->callback_failfast();

		//
		// The state_lock is used to signal the polling thread.
		// callback_failfast() reads the keys from the quorum device.
		//
		// If the state_lock is not held by the polling thread when
		// doing callback_failfast(), a scenario could arise where :
		// - the polling thread has checked that it is enabled
		// and proceeds to read keys in callback_failfast(), and
		// - another thread marks the polling thread state as disabled,
		// signals the polling thread, and then proceeds to
		// nullify the vnode pointer for the quorum device.
		//
		// In that scenario, the polling thread can fail when
		// subsequently trying to read the keys using the vnode pointer.
		//
		// Hence, the polling thread needs to hold the state_lock
		// while doing callback_failfast(). This will ensure
		// that another thread cannot do any state change for
		// the polling thread, until after the polling thread has
		// finished its current action and is ready to sleep or
		// take other action as the case may be.
		//
		state_lock.unlock();

		if (failfast_status == quorum::QD_EACCES) {
			//
			// In QD_EACCES case, the node should panic in
			// the code of callback_failfast() itself.
			// So we cannot reach here.
			//
			CL_PANIC(0);
		}

		//
		// Sleep for polling duration period or until we are signalled
		// due to a thread state change.
		//
		state_lock.lock();
		if (thr_state == ENABLED) {
			if (software_fencing_polling_period < 1) {
				software_fencing_polling_period = 1;
			}
			sys_timeout.setreltime((os::usec_t)
			    software_fencing_polling_period * MICROSEC);
			// We ignore return value
			(void) state_cv.timedwait(&state_lock, &sys_timeout);
		}
	}
	thr_state = DONE;
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::polling_thread thread exiting.\n"));
	state_cv.signal();
	state_lock.unlock();
}

//
// enable_failfast
//
// Change polling thread state to failfast enabled.
//
void
software_fencing::enable_failfast()
{
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::enable_failfast called.\n"));
	oplock.lock();

	if (initialized) {
		state_lock.lock();
		if (thr_state != ENABLED) {
			thr_state = ENABLED;
			state_cv.signal();
		}
		state_lock.unlock();
	}

	oplock.unlock();
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::enable_failfast done.\n"));
}

//
// disable_failfast
//
// Change polling thread state to failfast disabled.
//
void
software_fencing::disable_failfast()
{
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::disable_failfast called.\n"));
	oplock.lock();

	if (initialized) {
		state_lock.lock();
		if (thr_state != DISABLED) {
			thr_state = DISABLED;
			state_cv.signal();
		}
		state_lock.unlock();
	}

	oplock.unlock();
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::disable_failfast done.\n"));
}

//
// shutdown
//
// Wait for the polling thread to exit.
//
void
software_fencing::shutdown_failfast()
{
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::shutdown_failfast called.\n"));
	oplock.lock();

	if (initialized) {
		state_lock.lock();
		if ((thr_state == ENABLED) || (thr_state == DISABLED)) {
			thr_state = STOP;
			state_cv.signal();
			//
			// Wait for the fencing thread to exit.
			//
			while (thr_state != DONE) {
				state_cv.wait(&state_lock);
			}
		}
		state_lock.unlock();
		initialized = false;
	}

	oplock.unlock();
	CMM_SELECTIVE_TRACE(software_fencing_trace_level, CMM_GREEN,
	    ("software_fencing::shutdown_failfast done.\n"));
}
