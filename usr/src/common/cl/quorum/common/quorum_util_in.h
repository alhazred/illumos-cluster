/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _QUORUM_UTIL_IN_H
#define	_QUORUM_UTIL_IN_H

#pragma ident	"@(#)quorum_util_in.h	1.4	08/05/20 SMI"


//
// Functions to convert keys to/from 64-bit integers
//

//
// mhioc_key_to_int64_key
//
inline uint64_t
mhioc_key_to_int64_key(const mhioc_resv_key_t& mhioc_key)
{
	ASSERT(sizeof (uint64_t) == sizeof (mhioc_resv_key_t));

#if defined(_LITTLE_ENDIAN)
	uint64_t int64_key = 0;

	for (int i = 0; i < sizeof (uint64_t); i++) {
		int64_key <<= 8;
		int64_key += mhioc_key.key[i];
	}

	return (int64_key);
#elif defined(_BIG_ENDIAN)
	return (*((uint64_t *)&mhioc_key));
#else
#error "No endianness defined"
#endif
}

//
// idl_key_to_int64_key
//
inline uint64_t
idl_key_to_int64_key(const quorum::registration_key_t *idl_key)
{
	ASSERT(idl_key != NULL);

	return (mhioc_key_to_int64_key(*((const mhioc_resv_key_t *) idl_key)));
}

//
// int64_key_to_mhioc_key
//
inline void
int64_key_to_mhioc_key(const uint64_t int64_key, mhioc_resv_key_t& mhioc_key)
{
	ASSERT(sizeof (uint64_t) == sizeof (mhioc_resv_key_t));

#if defined(_LITTLE_ENDIAN)
	uint64_t tmp_key = int64_key;

	for (int i = sizeof (uint64_t) - 1; i >= 0; i--) {
		mhioc_key.key[i] = (uchar_t)(tmp_key & 0xFF);
		tmp_key >>= 8;
	}
#elif defined(_BIG_ENDIAN)
	*((uint64_t *)&mhioc_key) = int64_key;
#else
#error "No endianness defined"
#endif
}

//
// int64_key_to_idl_key
//
inline void
int64_key_to_idl_key(const uint64_t int64_key,
			quorum::registration_key_t *idl_key)
{
	ASSERT(idl_key != NULL);

	int64_key_to_mhioc_key(int64_key, *((mhioc_resv_key_t *)idl_key));
}

#endif /* _QUORUM_UTIL_IN_H */
