/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _QUORUM_PGRE_H
#define	_QUORUM_PGRE_H

#pragma ident	"@(#)quorum_pgre.h	1.9	08/09/02 SMI"

#include <sys/param.h>
#include <h/cmm.h>
#include <h/ccr_data.h>
#include <cmm/cmm_debug.h>

/*
 * SELECTIVE MESSAGE TRACING
 * -------------------------
 * Some functions declared in this file take a message tracing level
 * as argument. Caller can specify the message tracing level.
 * The tracing level argument gives the flexibility of
 * tracing only certain level of messages and above,
 * and to avoid tracing all messages into the CMM trace buffer.
 * The default message tracing level is CMM_GREEN (full tracing).
 *
 * This flexibility is required, because some of these functions
 * are called repeatedly by periodic threads (eg, software fencing
 * polling thread, quorum monitor daemon), and so if all messages
 * are traced, then the CMM trace buffer will wrap around quite frequently.
 *
 * Note that all functions in this file do not have this feature.
 * Only those functions that are used by the periodic threads
 * such as the quorum monitor daemon and software fencing thread
 * have been provided with this feature. As and when other functions
 * are used by such periodic threads, more functions can be changed similarly.
 */

/*
 * The PGR Emulation (PGRE) area will be divided into (64 + 1) sectors,
 * with the first sector used for the group owner info, and the rest
 * for the 64 nodes info. In unode, each such sector can be simulated
 * by a corresp data structure. PGRE_NODE_AREA_SIZE represents the number
 * of sectors assigned per node.
 */
#define	PGRE_NODE_AREA_SIZE		1

#define	PGRE_GET_NODE_BLKNO(s, n)	(s + n * PGRE_NODE_AREA_SIZE)

#define	INV_BLKNO	(~((uint64_t)0))

//
// The PGRE area is in the following portion of the reserved cylinders
// (PSARC 1999/430):
// 	65 (NODEID_MAX + 1) sectors from the 2nd last track of
// 	the first alternate cylinder.
// where NODEID_MAX is the maximum number of nodes supported by SC3.x.
//
#define	PGRE_NUM_SECTORS	65

//
// Each PGRE node sector can currently contain 5 pieces of information,
//  register key, ME vars number and choosing, and ccr epoch and cmm sequence
//  number at the time of the last ccr update, in that order. The owner sector
// will just contain the key.
//
// The sector begins with an identifier which consists of the string
// "SunCluster Quorum Area: [Node # | Owner]\0", which is followed
// by the data. The last four bytes of the sector make up the checksum.
//

#define	PGRE_ID_LEAD_STRING1	"SunCluster Quorum Area: Owner  "
#define	PGRE_ID_LEAD_STRING2	"SunCluster Quorum Area: Node "
#define	PGRE_ID_SIZE		32
#define	PGRE_DATA_SIZE		(DEV_BSIZE - PGRE_ID_SIZE - 4)

typedef struct pgre_sector {
	uchar_t	pgres_id[PGRE_ID_SIZE];		// Id (see comments above).
	uchar_t pgres_data[PGRE_DATA_SIZE];	// PGRE data stored here.
	uchar_t pgres_checksum3;		// checksum (MSB).
	uchar_t	pgres_checksum2;
	uchar_t pgres_checksum1;
	uchar_t pgres_checksum0;		// checksum (LSB).
} pgre_sector_t;


#define	PGRE_GETCHKSUM(ps)	((ps)->pgres_checksum3 << 24) + \
				((ps)->pgres_checksum2 << 16) + \
				((ps)->pgres_checksum1 << 8) + \
				((ps)->pgres_checksum0)

#define	PGRE_FORMCHKSUM(c, ps)	(ps)->pgres_checksum3 = hibyte(hiword((c))); \
				(ps)->pgres_checksum2 = lobyte(hiword((c))); \
				(ps)->pgres_checksum1 = hibyte(loword((c))); \
				(ps)->pgres_checksum0 = lobyte(loword((c)));

// Lint override needed because lint does not know that the while(0) is fine.
#define	PGRE_CALCCHKSUM(c, ps, ip)  \
	do { \
		c = 0; \
		ip = (uint_t *)ps; \
		for (uint_t i = 0; \
			i < ((DEV_BSIZE-sizeof (int))/sizeof (int)); \
			i++) \
			c ^= ip[i]; \
	} while (0) //lint -e717


//
// Data struct for the PGRE data, for easier access than using pgres_data.
//
// pgre_number and pgre_choosing are used for implementation of Lamport's
//   algorithm to simulate the SCSI-3 reservation protocol.
//
// last_epoch and last_reconfig are the last ccr epoch and cmm reconfiguration
//   sequence number at the time of the last ccr update this node participated
//   in. They are used to give more information to the amnesia prevention
//   algorithm.
//
typedef struct pgre_data {
	quorum::registration_key_t pgre_key;
	uint64_t		pgre_number;
	bool			pgre_choosing;
	ccr_data::epoch_type	last_epoch;
	cmm::seqnum_t		last_reconfig;
} pgre_data_t;

/*
 * Initialize the mutual exclusion (ME) variables for this node.
 * Caller can specify a message tracing level.
 * Default tracing level is CMM_GREEN (full tracing).
 */
extern int
quorum_pgre_initialize(void *, uint64_t, cmm_trace_level level = CMM_GREEN);

/*
 * quorum_pgre_choosing_write
 *
 * Writes the value of "choosing" (Lamport's ME variable) on the node
 * area indicated by the starting block number in the PGRE reserved area.
 *
 * Caller can specify a message tracing level.
 * Default tracing level is CMM_GREEN (full tracing).
 */
extern int
quorum_pgre_choosing_write(
    void *, uint64_t, bool, cmm_trace_level level = CMM_GREEN);

/*
 * quorum_pgre_choosing_read
 *
 * Reads "choosing" from the node area indicated by the starting block
 * number in the PGRE reserved area.
 *
 * Caller can specify a message tracing level.
 * Default tracing level is CMM_GREEN (full tracing).
 */
extern int
quorum_pgre_choosing_read(
    void *, uint64_t, bool &, cmm_trace_level level = CMM_GREEN);

/*
 * quorum_pgre_number_write
 *
 * Writes the value of "number" (Lamport's ME variable) on the node
 * area indicated by the starting block number in the PGRE reserved area.
 *
 * Caller can specify a message tracing level.
 * Default tracing level is CMM_GREEN (full tracing).
 */
extern int
quorum_pgre_number_write(
    void *, uint64_t, uint64_t, cmm_trace_level level = CMM_GREEN);

/*
 * quorum_pgre_number_read
 *
 * Reads "number" from the node area indicated by the starting block
 * number in the PGRE reserved area.
 *
 * Caller can specify a message tracing level.
 * Default tracing level is CMM_GREEN (full tracing).
 */
extern int
quorum_pgre_number_read(
    void *, uint64_t, uint64_t &, cmm_trace_level level = CMM_GREEN);

/*
 * quorum_pgre_key_write
 *
 * Writes the specified key on the node area indicated by the starting
 * block number in the PGRE reserved area.
 *
 */
extern int
quorum_pgre_key_write(void *, uint64_t, const quorum::registration_key_t *,
    bool);

/*
 * quorum_pgre_key_read
 *
 * Reads the key from the node area indicated by the starting block
 * number in the PGRE reserved area.
 *
 */
extern int
quorum_pgre_key_read(void *, uint64_t, quorum::registration_key_t *);

/*
 * quorum_pgre_key_find
 *
 * Searches for the given key in the PGRE reserved area.
 * Fills in the starting block number of the corresp node area.
 *
 */
extern int
quorum_pgre_key_find(void *, uint64_t, const quorum::registration_key_t *,
    uint64_t &);

/*
 * quorum_pgre_key_delete
 *
 * Removes the key from the node area specified by the starting
 * block number - resets the key to null.
 *
 */
extern int
quorum_pgre_key_delete(void *, uint64_t, bool);

extern int
quorum_pgre_lock_errprnt(int, char *);

extern int
quorum_pgre_lock(void *, uint64_t, nodeid_t);

extern int
quorum_pgre_unlock(void *, uint64_t, nodeid_t);

#endif	/* _QUORUM_PGRE_H */
