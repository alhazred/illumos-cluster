//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)type_registry.cc	1.15	08/09/02 SMI"
#ifdef _KERNEL_ORB // quorum is used only by the kernel CMM

#include <sys/rsrc_tag.h>
#include <sys/file.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_ns_int.h>
#include <cmm/cmm_debug.h>
#include <quorum/common/type_registry.h>
#include <cmm/cmm_impl.h>
#include <h/dpm.h>

#ifndef _KERNEL
#include <quorum/unode/unode_qd.h>
#endif

//
// This module implements the cluster quorum device type registry.
//

//
// There is exactly one instance of the device type registry object per node.
//
device_type_registry_impl
*device_type_registry_impl::the_device_type_registryp = NULL;

//
// Constructor
//
device_type_registry_impl::device_type_registry_impl():
	_qd_num_entries(0),
	loading(false),
	waiting(false)
{
}

//
// Destructor
//
device_type_registry_impl::~device_type_registry_impl()
{
	for (uint_t i = 0; i < _qd_num_entries; i++) {
		delete [] _qd_table[i].qd_type;
	}
}

//
// Unreferenced
//
void
#ifdef DEBUG
device_type_registry_impl::_unreferenced(unref_t cookie)
#else
device_type_registry_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_TRACE(("In device_type_registry_impl::_unreferenced.\n"));
	delete this;
}

//
// Search the table for the given quorum device type and return the
// object contructor function or NULL if not found.
// This should be called with the lock held.
//
get_qd_impl
device_type_registry_impl::find_func(const char *type)
{
	// Search the table for the quorum device type.
	for (uint_t i = 0; i < _qd_num_entries; i++) {
		if (os::strcmp(_qd_table[i].qd_type, type) == 0) {
			// Return a new instance of the quorum device type.
			return (_qd_table[i].qd_function);
		}
	}
	return (NULL);
}

//
// Static Functions
//

//
// Called from ORB::initialize to start up the device type registry.
//
int
device_type_registry_impl::initialize()
{
	if (the_device_type_registryp != NULL) {
		return (EINVAL);
	}

	CMM_TRACE(("device_type_registry_impl::initialize()\n"));
	the_device_type_registryp = new device_type_registry_impl();

	// Bind the object to the local nameserver.
	naming::naming_context_var ctx_v = ns::local_nameserver();
	CORBA::Object_var obj_v = the_device_type_registryp->get_objref();
	Environment e;
	ctx_v->rebind("type_registry", obj_v, e);
	if (e.exception()) {
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CMM_NODE_TAG,
		    "temp_nodename", NULL);
		//
		// SCMSGS
		// @explanation
		// This is an internal error during node initialization, and
		// the system can not continue.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "INTERNAL ERROR CMM: Cannot bind device type registry "
		    "object to local name server.");
		CMM_TRACE(("INTERNAL ERROR CMM: Cannot bind device "
			"type registry object to local name server.\n"));
		return (EINVAL);
	} else {
		CMM_TRACE(("Registered device_type_registry in nameserver\n"));
	}
	return (0);
}

//
// This function is called by the _init() routine of modules loaded
// as part of the get_quorum_device call. The modules pass a function
// pointer to a function which creates a new instance of the quorum device.
// That pointer is then used in get_quorum_device to instantiate the object
// to return to the user.
//
void
device_type_registry_impl::register_device_type(const char *type,
    get_qd_impl funcp)
{
	CMM_TRACE(("device_type_registry_impl::register_device_type() "
	    "register type %s\n", type));

	ASSERT(funcp != NULL);

	device_type_registry_impl *registryp = the_device_type_registryp;
	ASSERT(registryp != NULL);

	registryp->lock.lock();
	if (registryp->_qd_num_entries >= MAX_QUORUM_DEVICES) {
		registryp->lock.unlock();
		CMM_TRACE(("device_type_registry_impl::register_device_type() "
		    "table full, cannot register type %s\n", type));
		ASSERT(0);
		return;
	}

	// Initialize the table entry.
	registryp->_qd_table[registryp->_qd_num_entries].qd_type =
		os::strdup(type);
	registryp->_qd_table[registryp->_qd_num_entries].qd_function = funcp;
	registryp->_qd_num_entries++;
	registryp->lock.unlock();
}

//
// IDL Interface Functions
//

quorum::quorum_device_type_ptr
device_type_registry_impl::get_quorum_device(
    const char *type, Environment &e)
{
	return (get_quorum_device_common(type, CMM_GREEN, e));
}

quorum::quorum_device_type_ptr
device_type_registry_impl::get_quorum_device_trace(
    const char *type, uint32_t trace_level, Environment &e)
{
	return (get_quorum_device_common(
	    type, (cmm_trace_level)trace_level, e));
}

//
// get_quorum_device_common
//
// First, check to see if we already have an instantiation function for
//   the device type. If so, instantiate a new object and return.
// Second, (if no function pointer) load the module which implements the
//   device type. As part of module load, the _init() function of the
//   module will call register_device_type to register its instantiation
//   function.
// Third, instantiate a new object and return.
//
// Note that this can be called from clconf_quorum_device_scrub() which is
// a user level C library (i.e., user level ORB).
//
quorum::quorum_device_type_ptr
device_type_registry_impl::get_quorum_device_common(
    const char *type, cmm_trace_level trace_level, Environment &)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("get_quorum_device called for type %s\n", type));

#ifdef _KERNEL
	lock.lock();
	//
	// If the module is already loaded, just return a new object.
	//
	get_qd_impl funcp = find_func(type);
	if (funcp != NULL) {
		lock.unlock();
		return (funcp(trace_level));
	}

	//
	// Wait if a module is being loaded by somebody else.
	//
	while (loading) {
		waiting = true;
		cv.wait(&lock);
		//
		// If the module is loaded after waiting, return a new object.
		//
		funcp = find_func(type);
		if (funcp != NULL) {
			lock.unlock();
			return (funcp(trace_level));
		}
	}
	loading = true;
	lock.unlock();

	//
	// Try to load the module since no entry was found.
	//
	char modname[64];
	os::snprintf(modname, sizeof (modname), "clq_%s", type);

	int err;
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("get_quorum_device: loading module %s\n", modname));
	if ((err = modload("misc", modname)) <= 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("Couldn't load module %s: %d\n", modname, err));
		lock.lock();
		loading = false;
		if (waiting) {
			waiting = false;
			cv.broadcast();
		}
		lock.unlock();
		return (NULL);
	}

	lock.lock();
	loading = false;
	if (waiting) {
		waiting = false;
		cv.broadcast();
	}

	// Search the table for the quorum device type.
	funcp = find_func(type);
	lock.unlock();
	if (funcp != NULL) {
		return (funcp(trace_level));
	}

	ASSERT(0);
	return (quorum::quorum_device_type::_nil());
#else
	//
	// This is a unode quorum device.
	//
	char modname[64];
	os::snprintf(modname, sizeof (modname), "clq_%s", type);
	CMM_TRACE(("type_registry: loading unode module %s\n", modname));
	return (get_unode_qd_impl());
#endif
}

#endif // _KERNEL_ORB
