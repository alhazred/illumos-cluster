//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)quorum_pgre.cc	1.5	08/09/02 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <sys/vnode.h>
#include <cmm/cmm_debug.h>
#include <quorum/common/quorum_pgre.h>
#include <sys/os.h>
#include <quorum/common/shared_disk_util.h>
#include <quorum/common/quorum_util.h>

#ifdef _KERNEL
//
// Initialize the mutual exclusion variables for this node.
// Caller can specify a message tracing level.
// Default tracing level is CMM_GREEN (full tracing).
//
int
quorum_pgre_initialize(
    void		*qhandle,
    uint64_t		sblkno,
    cmm_trace_level	trace_level)
{
	int		error;
	nodeid_t	my_nodeid;

	//
	// Initialize the mutual exclusion (ME) variables for
	// this node.
	//
	my_nodeid = orb_conf::local_nodeid();

	error = quorum_pgre_choosing_write(
	    qhandle,
	    PGRE_GET_NODE_BLKNO(sblkno, my_nodeid),
	    false,
	    trace_level);

	if (error != 0) {
		CMM_TRACE(("quorum_pgre_choosing_write() retd "
		    "error %d.\n", error));
		return (error);
	}

	error = quorum_pgre_number_write(
	    qhandle,
	    PGRE_GET_NODE_BLKNO(sblkno, my_nodeid),
	    (uint64_t)0,
	    trace_level);

	if (error != 0) {
		CMM_TRACE(("quorum_pgre_number_write() retd "
		    "error %d.\n", error));
		return (error);
	}

	return (error);
}

//
// quorum_pgre_choosing_write
//
// Writes the value of "choosing" (Lamport's ME variable) on the node
// area indicated by the starting block number in the PGRE reserved area.
//
// Caller can specify a message tracing level.
// Default tracing level is CMM_GREEN (full tracing).
//
int
quorum_pgre_choosing_write(
    void 		*qhandle,
    uint64_t		sblkno,
    bool		cval,
    cmm_trace_level	trace_level)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_alloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_choosing_write: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
		kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
		return (error);
	}

	// Change the key as specified above.
	pgre_data_ptr->pgre_choosing = cval;

	// Now write the contents of sector in the corresp blk on disk.
	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_choosing_write: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_pgre_choosing_write: wrote %d for sblkno (0x%llx).\n",
	    cval, sblkno));
	return (error);
}

//
// quorum_pgre_choosing_read
//
// Reads "choosing" from the node area indicated by the starting block
// number in the PGRE reserved area.
//
// Caller can specify a message tracing level.
// Default tracing level is CMM_GREEN (full tracing).
//
int
quorum_pgre_choosing_read(
    void		*qhandle,
    uint64_t		sblkno,
    bool		&cval,
    cmm_trace_level	trace_level)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_choosing_read: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
	} else {
		cval = pgre_data_ptr->pgre_choosing;
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_pgre_choosing_read: read %d for sblkno (0x%llx).\n",
	    cval, sblkno));

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}


//
// quorum_pgre_number_write
//
// Writes the value of "number" (Lamport's ME variable) on the node
// area indicated by the starting block number in the PGRE reserved area.
//
// Caller can specify a message tracing level.
// Default tracing level is CMM_GREEN (full tracing).
//
int
quorum_pgre_number_write(
    void 		*qhandle,
    uint64_t 		sblkno,
    uint64_t 		nval,
    cmm_trace_level	trace_level)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_number_write: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
		kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
		return (error);
	}

	// Change the number as specified above.
	pgre_data_ptr->pgre_number = nval;

	// Now write the contents of sector in the corresp blk on disk.
	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_number_write: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_pgre_number_write: wrote 0x%llx for sblkno (0x%llx).\n",
	    nval, sblkno));

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}


//
// quorum_pgre_number_read
//
// Reads "number" from the node area indicated by the starting block
// number in the PGRE reserved area.
//
// Caller can specify a message tracing level.
// Default tracing level is CMM_GREEN (full tracing).
//
int
quorum_pgre_number_read(
    void 		*qhandle,
    uint64_t 		sblkno,
    uint64_t 		&nval,
    cmm_trace_level	trace_level)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_number_read: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
	} else {
		nval = pgre_data_ptr->pgre_number;
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_pgre_number_read: read 0x%llx for sblkno (0x%llx).\n",
	    nval, sblkno));

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_pgre_key_write
//
// Writes the specified key on the node area indicated by the starting
// block number in the PGRE reserved area.
//
//
int
quorum_pgre_key_write(
    void 		*qhandle,
    uint64_t 		sblkno,
	const quorum::registration_key_t *key,
    bool		is_owner_area)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_alloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data read in, since
	// the read is so as to only change the portion that is needed
	// to be written. Last arg false indicates no need to check.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_key_write: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
		kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
		return (error);
	}

	// Change the key as specified above.
	pgre_data_ptr->pgre_key = *key;

	// Now write the contents of sector in the corresp blk on disk.
	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector,
	    is_owner_area);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_key_write: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_pgre_key_read
//
// Reads the key from the node area indicated by the starting block
// number in the PGRE reserved area.
// This function can be used by both kernelland and userland.
//
//
int
quorum_pgre_key_read(
    void 		*qhandle,
    uint64_t 		sblkno,
	quorum::registration_key_t *key)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);
	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_alloc((size_t)DEV_BSIZE, KM_SLEEP);

	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;
	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_key_read: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
	} else {
		*key = pgre_data_ptr->pgre_key;
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}



//
// quorum_pgre_key_find
//
// Searches for the given key in the PGRE reserved area.
// Fills in the starting block number of the corresp node area.
//
//
int
quorum_pgre_key_find(
    void 		*qhandle,
    uint64_t 		sblkno,
	const quorum::registration_key_t *key,
    uint64_t 		&key_sbn)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int 			error = 0;
	uint_t			i;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_alloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// Read in a sector at a time from the PGRE area until match
	// found. Look only at the node areas, not at the owner area.
	//
	for (i = 1; i < PGRE_NUM_SECTORS; i++) {
		error = quorum_scsi_sector_read(vnode_ptr,
		    PGRE_GET_NODE_BLKNO(sblkno, i), sector, true);

		if (error != 0) {
			CMM_TRACE(("quorum_pgre_key_find: quorum_scsi_sector"
			    "_read returned error (%d).\n", error));
			break;
		} else {
			if (match_idl_keys(key, &(pgre_data_ptr->pgre_key))) {
				CMM_TRACE(("quorum_pgre_key_find: match "
				    "found on disk for key.\n"));
				key_sbn = (uint64_t)
				    PGRE_GET_NODE_BLKNO(sblkno, i);
				break;
			}
		}
	}

	// If key not found, return ENOENT.
	if (i == PGRE_NUM_SECTORS) {
		error = ENOENT;
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}


//
// quorum_pgre_key_delete
//
// Removes the key from the node area specified by the starting
// block number - resets the key to nul, and leaves other fields as is.
//
//
int
quorum_pgre_key_delete(
    void 	*qhandle,
    uint64_t 	sblkno,
    bool	is_owner_area)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector = (pgre_sector_t *)kmem_alloc((size_t)DEV_BSIZE, KM_SLEEP);
	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data read in, since
	// the read is so as to only change the portion that is needed
	// to be written. Last arg false indicates no need to check.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_key_delete: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
		kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
		return (error);
	}

	// Zero out the key field.
	bzero((caddr_t)&(pgre_data_ptr->pgre_key),
	    sizeof (quorum::registration_key_t));

	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector,
	    is_owner_area);
	if (error != 0) {
		CMM_TRACE(("quorum_pgre_key_delete: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

int
quorum_pgre_lock_errprnt(int error, char *errmsg)
{
	CMM_TRACE(("quorum_pgre_lock: %s() retd error %d.\n",
	    errmsg, error));
	return (error);
}


//
// quorum_pgre_lock
//
// Does the "lock" equivalent of the distributed mutual exclusion (as per
// Lamport's algorithm) needed for PGRE on non SCSI-3 compliant disks.
//
// Returns only when the lock is acquired.
//
int
quorum_pgre_lock(void *qhandle, uint64_t sblkno, nodeid_t my_nodeid)
{
	uint_t		i;
	uint64_t	my_num, cur_num;
	bool 		keep_looping, cur_choosing;
	quorum::registration_key_t cur_key;
	uint64_t	their_blkno;
	int		error = 0;

	//
	// Need to check error returns from all calls here. In case of
	// an error from any of the pgre ops below, log that and return the
	// error.
	// XXX If some previous ops need undoing, like in clearing the value
	// of number if a subsequent error happens, not sure how to accomplish
	// that since clearing will also be a disk op, and disk ops are starting
	// to fail ...
	//

	// Step 1: choosing[my_nodeid] = 1;
	error = quorum_pgre_choosing_write(qhandle,
	    PGRE_GET_NODE_BLKNO(sblkno, my_nodeid), true);
	if (error != 0) {
		return (quorum_pgre_lock_errprnt(error,
		    "quorum_pgre_choosing_write"));
	}

	//
	// Step 2: number[my_nodeid] =
	// 1 + max(number[1], ..., number[max_nodeid]);
	//
	my_num = 0;
	for (i = 1; i <= NODEID_MAX; i++) {
		// Bypass nodes not registered.
		their_blkno = PGRE_GET_NODE_BLKNO(sblkno, i);
		error = quorum_pgre_key_read(qhandle, their_blkno, &cur_key);
		if (error != 0) {
			return (quorum_pgre_lock_errprnt(error,
			    "quorum_pgre_key_read"));
		}

		if (null_idl_key(&cur_key) == 1) {
			continue;
		}

		error = quorum_pgre_number_read(qhandle,
		    PGRE_GET_NODE_BLKNO(sblkno, i), cur_num);
		if (error != 0) {
			return (quorum_pgre_lock_errprnt(error,
			    "quorum_pgre_number_read"));
		}
		if (cur_num > my_num) {
			my_num = cur_num;
		}
	}
	my_num++;
	CMM_TRACE(("quorum_pgre_lock: my_num = 0x%llx.\n", my_num));
	error = quorum_pgre_number_write(qhandle,
	    PGRE_GET_NODE_BLKNO(sblkno, my_nodeid), my_num);

	if (error != 0) {
		return (quorum_pgre_lock_errprnt(error,
		    "quorum_pgre_number_write"));
	}

	// Step 3: choosing[my_nodeid] = 0;
	error = quorum_pgre_choosing_write(qhandle,
	    PGRE_GET_NODE_BLKNO(sblkno, my_nodeid), false);
	if (error != 0) {
		return (quorum_pgre_lock_errprnt(error,
		    "quorum_pgre_chooing_write"));
	}

	// Step 4: Iterate thru all other node values ...
	for (i = 1; i <= NODEID_MAX; i++) {
		if (i == my_nodeid) {
			continue;
		}
		//
		// Modification to Lamport's alg to acct for a node
		// dying w/o setting its choosing[] and number[] values
		// to 0: examine these values ONLY if node registered.
		//
		// NOTE: This check can be inserted here rather than in
		// each iteration of the two while loops below because
		// even if a node registers after the check at this point,
		// it is later in the queue (ie, its number[] is going
		// to be greater than this node's since it registered
		// after Step 2 above, and the 2nd while loop condition
		// is automatically broken. The first while loop is not
		// a problem because the check for the other node being
		// registered is eqvt to the check for choosing = false.
		//
		their_blkno = PGRE_GET_NODE_BLKNO(sblkno, i);

		error = quorum_pgre_key_read(qhandle, their_blkno, &cur_key);
		if (error != 0) {
			return (quorum_pgre_lock_errprnt(error,
			    "quorum_pgre_key_read"));
		}

		if (null_idl_key(&cur_key) == 1) {
			continue;
		}

		// Step 5: keep looping until choosing[their_nodeid] = false.
		cur_choosing = true;
		while (cur_choosing) {
			error = quorum_pgre_choosing_read(qhandle, their_blkno,
			    cur_choosing);
			if (error != 0) {
				return (quorum_pgre_lock_errprnt(error,
				    "quorum_pgre_choosing_read"));
			}

			if (cur_choosing) {
				CMM_TRACE(("quorum_pgre_lock: The value of "
				    "choosing[nodeid=%d] = true. Waiting ...\n",
				    i));
				// XXX Is the sleep int ok?
				os::usecsleep((os::usec_t)10000);
			}
		}

		//
		// Step 6: keep looping until number[their_nodeid] = 0 OR
		// 	number[their_nodeid] > number[my_nodeid] OR
		//	(number[their_nodeid] == number[my_nodeid] and
		//	my_nodied < their_nodeid).
		//
		keep_looping = true;
		while (keep_looping) {
			error = quorum_pgre_number_read(qhandle, their_blkno,
			    cur_num);
			if (error != 0) {
				return (quorum_pgre_lock_errprnt(error,
				    "quorum_pgre_number_read"));
			}

			if ((cur_num == 0) || (cur_num > my_num) ||
			    ((cur_num == my_num) && (i > my_nodeid))) {
				keep_looping = false;
			} else {
				CMM_TRACE(("quorum_pgre_lock: The value of "
				    "number[nodeid=%d] = 0x%llx, and my number "
				    "= 0x%llx. Waiting ...\n",
				    i, cur_num, my_num));
				os::usecsleep((os::usec_t)10000);
			}
		}
	}
	return (error);
}


//
// quorum_pgre_unlock
//
// Does the "unlock" eqvt of the distributed mutual exclusion (as per Lamport's
// algorithm) needed for PGRE on non SCSI-3 compliant disks.
//
//
int
quorum_pgre_unlock(void *qhandle, uint64_t sblkno, nodeid_t my_nodeid)
{
	int 			error = 0;

	error = quorum_pgre_number_write(qhandle,
	    PGRE_GET_NODE_BLKNO(sblkno, my_nodeid), (uint64_t)0);

	if (error != 0) {
		//
		// XXX This is a pretty bad situation ... that the ME var
		// "number" for this node was not reset to 0, and could
		// potentially cause other nodes to hang.
		// What is a good recovery action here? Panic the node?
		// Not sure that will help ...
		// Not sure this can happen unless the disk has gone bad
		// or sth. For now, just logging this.
		//
		// XXX The caller does not check the error code, so should
		// this even return it or make it returning a void?
		// I put it in here for uniformity.
		//
		os::sc_syslog_msg	*syslog_msgp;
		char			nodename[8];

		CMM_TRACE(("quorum_pgre_unlock: quorum_pgre_number_write() "
		    "failed with error %d.\n", error));

		os::sprintf(nodename, "%d", orb_conf::local_nodeid());
		syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_CMM_QUORUM_TAG,
		    nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// This node was unable to reset some information on the
		// quorum device. This will lead the node to believe that its
		// partition has been preempted. This is an internal error. If
		// a cluster gets divided into two or more disjoint
		// subclusters, exactly one of these must survive as the
		// operational cluster. The surviving cluster forces the other
		// subclusters to abort by grabbing enough votes to grant it
		// majority quorum. This is referred to as preemption of the
		// losing subclusters.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Unable to reset node information on quorum disk.");
		delete syslog_msgp;
	}

	return (error);
}

#endif // _KERNEL
