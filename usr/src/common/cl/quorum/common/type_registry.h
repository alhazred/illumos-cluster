/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _TYPE_REGISTRY_IMPL_H
#define	_TYPE_REGISTRY_IMPL_H

#pragma ident	"@(#)type_registry.h	1.7	08/09/02 SMI"

#include <orb/object/adapter.h>
#include <h/quorum.h>
#include <sys/os.h>
#include <sys/quorum_int.h>
#include <cmm/cmm_debug.h>

/*
 * Define the size of the type registry table.
 * This could be dynamic later.
 */
#define	QUORUM_DEVICE_NUM_TYPES	10

typedef quorum::quorum_device_type_ptr (*get_qd_impl)(cmm_trace_level);

//
// This structure is used to build a table of quorum device type names
// and the object which implements that type of device.
//
struct quorum_device_type_entry {
	char		*qd_type;
	get_qd_impl	qd_function;
};

//
// This class maintains the mapping from quorum device type to implementations.
//
class device_type_registry_impl :
    public McServerof <quorum::device_type_registry> {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//

	//
	// Return a new quorum device object of the given type.
	// Traces all messages.
	//
	quorum::quorum_device_type_ptr get_quorum_device(
		const char *type, Environment &);

	//
	// Return a new quorum device object of the given type.
	// Caller can specify message tracing level.
	//
	quorum::quorum_device_type_ptr get_quorum_device_trace(
		const char *type, uint32_t trace_level,
		Environment &);

	//
	// Register a quorum device type implementation.
	// This should be called by a quorum module when it is loaded.
	// The name should be a constant (i.e., it is not copied).
	//
	static void register_device_type(const char *type, get_qd_impl funcp);

	//
	// The following is called from ORB::initialize to create the single
	// instance of the device_type_registry_impl on this node.
	//
	static int initialize();

private:
	device_type_registry_impl();
	~device_type_registry_impl();

	//
	// Common function used by get_quorum_device()
	// and get_quorum_device_trace()
	//
	quorum::quorum_device_type_ptr get_quorum_device_common(
		const char *type, cmm_trace_level trace_level,
		Environment &);

	//
	// Search the table for the given quorum device type and return the
	// object contructor function or NULL if not found.
	// This should be called with the lock held.
	//
	get_qd_impl find_func(const char *type);

	//
	// Array of the known device types.
	// This could be a hash table or any other kind of table if
	// performance becomes an issue. Since the number of different
	// types of devices is currently quite small, a simple fixed size table
	// and linear search is used.
	//
	struct quorum_device_type_entry _qd_table[MAX_QUORUM_DEVICES];
	uint_t _qd_num_entries;

	static device_type_registry_impl *the_device_type_registryp;
	//
	// Locks used to protect the table and single thread module loading.
	//
	os::mutex_t lock;
	os::condvar_t cv;
	bool loading;		// True if loading a module.
	bool waiting;		// True if another thread is waiting to load.
};

#endif	/* _TYPE_REGISTRY_IMPL_H */
