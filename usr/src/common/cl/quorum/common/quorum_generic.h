/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _QUORUM_GENERIC_H
#define	_QUORUM_GENERIC_H

#pragma ident	"@(#)quorum_generic.h	1.4	08/10/01 SMI"

/* Interfaces to generic quorum device implementation. */

#include <orb/object/adapter.h>
#include <cmm/cmm_debug.h>
#include <h/quorum.h>
#include <quorum/common/type_registry.h>
#include <quorum/common/shared_disk_util.h>
#include <sys/modctl.h>
#include <quorum/common/quorum_pgre.h>
#include <quorum/common/quorum_util.h>
#include <orb/infrastructure/orb_conf.h>

// Included for file ioctls.
#include <sys/vnode.h>
#include <sys/file.h>

// Included for cplpl_init
#include <cplplrt/cplplrt.h>

class quorum_device_generic_impl {
public:

	//
	// Functions that implement the IDL interface.
	//
	quorum::quorum_error_t			quorum_open(
	    const char				*devname,
	    const bool				scrub,
	    const quorum::qd_property_seq_t	&,
	    Environment				&);

	uint32_t quorum_supports_amnesia_prot(Environment &);

	quorum::quorum_error_t quorum_close(Environment &);

	quorum::quorum_error_t			quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	quorum::quorum_error_t		quorum_read_reservations(
	    quorum::reservation_list_t	&resv_list,
	    Environment			&);

	quorum::quorum_error_t			quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&victim_key,
	    Environment				&);

	quorum::quorum_error_t			quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	quorum::quorum_error_t		quorum_read_keys(
	    quorum::reservation_list_t	&reg_list,
	    Environment			&);

	quorum::quorum_error_t			quorum_remove(
	    const char				*devname,
	    const quorum::qd_property_seq_t	&qd_props,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_read(
		uint64_t		location,
		CORBA::String_out	data,
		Environment		&);

	virtual quorum::quorum_error_t	quorum_data_write(
		uint64_t		location,
		const char		*data,
		Environment		&);

	virtual quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&) = 0;

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

protected:
	quorum::quorum_error_t quorum_read_keys_internal(
	    quorum::reservation_list_t &reg_list,
	    cmm_trace_level trace_level_to_use, Environment &);

	//
	// The vnode pointer for the dev]ice. This is set during the open
	// call and used for all successive calls.
	//
	vnode_t		*vnode_ptr;

	//
	// Information about the disk geometry. We get this once during
	// quorum_open and then hold onto it for all read and write calls.
	//
	uint64_t	sblkno;

	//
	// There will be times when the initial quorum_open call will not
	// be able to access the device due to fencing. The quorum algorithm
	// may then make future calls to the module, and we should retry
	// the reserved sector and pgre initialization.
	//
	bool		retry;

	//
	// The name of the device. Used mainly for debugging purposes.
	//
	char		*pathname;

	cmm_trace_level trace_level;

	//
	// Exclusive access functions as required for PGRe.
	//
	virtual int	reservation_release() = 0;
	virtual int	reservation_tkown() = 0;

	// Function for fault injection testing.
	int	check_fi(char*);
};

#endif	/* _QUORUM_GENERIC_H */
