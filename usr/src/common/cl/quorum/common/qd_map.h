/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _QD_MAP_H
#define	_QD_MAP_H

#pragma ident	"@(#)qd_map.h	1.9	08/05/20 SMI"

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)

// Container to map handles to paths (For Fault Injection Only)
class qd_map {
public:
	qd_map();
	~qd_map();

	// Add a qd -> path association to map
	void add_qd(void *handle, const char *path);

	// Find the path associated with a handle
	char *find_qd(void *handle);
private:
	typedef struct map {
		char	*path;
		void	*qd_handle;
	} list_cell;

	SList<list_cell>	mappings;
	os::mutex_t		list_lock;
};

#endif // FAULT_CMM && _KERNEL_ORB

#endif	/* _QD_MAP_H */
