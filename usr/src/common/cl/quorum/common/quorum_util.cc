//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)quorum_util.cc 1.9	08/05/20 SMI"

#include <quorum/common/quorum_util.h>

//
// match_mhioc_keys
// Utility function to compare two mhioc_resv_key_t objects.
// Returns 0 if they are different, 1 if they match.
//
int
match_mhioc_keys(const mhioc_resv_key_t& key1, const mhioc_resv_key_t& key2)
{
	for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		if (key1.key[i] != key2.key[i]) {
			return (0);
		}
	}
	return (1);
}

//
// match_idl_keys
// Utility function to compare two quorum::registration_key_t objects.
// Returns 0 if they are different, 1 if they match.
//
int
match_idl_keys(const quorum::registration_key_t *key1,
    const quorum::registration_key_t *key2)
{
	for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		if (key1->key[i] != key2->key[i]) {
			return (0);
		}
	}
	return (1);
}

//
// null_mhioc_key
// Returns 0 if the key is non-null, 1 if it is null
//
int
null_mhioc_key(const mhioc_resv_key_t& key)
{
	for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		if (key.key[i] != 0) {
			return (0);
		}
	}
	return (1);
}

//
// null_idl_key
// Returns 0 if the key is non-null, 1 if it is null
//
int
null_idl_key(const quorum::registration_key_t *key)
{
	for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		if (key->key[i] != 0) {
			return (0);
		}
	}
	return (1);
}

//
// key_value
// Translates a quorum::registration_key_t typed key
// into a numeric value suitable for display
//
uint64_t
key_value(quorum::registration_key_t &key)
{
	uint64_t keyval = 0;
	for (int j = 0; j < 8; j++) {
		keyval = (keyval << 8) + key.key[j];
	}
	return (keyval);
}
