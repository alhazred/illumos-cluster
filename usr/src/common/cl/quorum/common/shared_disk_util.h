/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SHARED_DISK_UTIL_H
#define	_SHARED_DISK_UTIL_H

#pragma ident	"@(#)shared_disk_util.h	1.9	08/10/01 SMI"

#include <h/quorum.h>
#include <quorum/common/quorum_pgre.h>
#include <sys/vnode.h>
#include <cmm/cmm_debug.h>

/*
 * SELECTIVE MESSAGE TRACING
 * -------------------------
 * Some functions declared in this file take a message tracing level
 * as argument. Caller can specify the message tracing level.
 * The tracing level argument gives the flexibility of
 * tracing only certain level of messages and above,
 * and to avoid tracing all messages into the CMM trace buffer.
 * The default message tracing level is CMM_GREEN (full tracing).
 *
 * This flexibility is required, because some of these functions
 * are called repeatedly by periodic threads (eg, software fencing
 * polling thread, quorum monitor daemon), and so if all messages
 * are traced, then the CMM trace buffer will wrap around quite frequently.
 *
 * Note that all functions in this file do not have this feature.
 * Only those functions that are used by the periodic threads
 * such as the quorum monitor daemon and software fencing thread
 * have been provided with this feature. As and when other functions
 * are used by such periodic threads, more functions can be changed similarly.
 */

//
// Since ioctl's may fail during events such as scsi bus resets, retries
// of these ioctl's need to be done.
//
#define	MAX_RETRIES	3	// Maxm number of retries for each ioctl.
#define	RETRY_SLEEP	2	// Sleep interval (in secs) between retries.

//
// For the Intel sd driver, we pass FNDELAY to vn_open, but we also need
// to pass FNDELAY to future ioctls in order to cause the sd driver to
// verify the disk geometry. Otherwise, all ioctls to Intel return EIO.
//
// For Sparc sd driver, if we pass FNDELAY to an ioctl, and this node is
// fenced, the ioctl will return EIO rather than EACCES. So, we should not
// pass FNDELAY to the Sparc driver on calls after vn_open.
//
// For now, we're using #ifdef's to workaround the driver inconsistency.
// There is a planned project in Solaris to unify the sd driver for Solaris
// and Intel in a single piece of code. Once that is in place, we should
// reevaluate the way this is handled in SC code.
//

#if defined(__i386) || defined(__amd64)
#define	SCQUORUM_IOCTL_FLAGS	(FKIOCTL | FNDELAY)
#else
#define	SCQUORUM_IOCTL_FLAGS	(FKIOCTL)
#endif // (__i386) || (__amd64)

//
// Utility functions for converting idl and mhioc lists.
//

extern int
convert_to_mhioc_list(mhioc_resv_desc_list_t &, quorum::reservation_list_t &);

extern int
convert_to_mhioc_reg_list(mhioc_key_list_t &, quorum::reservation_list_t &);

extern int
convert_to_idl_resv_list(mhioc_resv_desc_list_t &,
    quorum::reservation_list_t &);

extern int
convert_to_idl_reg_list(mhioc_key_list_t &, quorum::reservation_list_t &);

#ifdef _KERNEL
/*
 * Execute a given ioctl with the specified number of retries.
 *
 * Any errors will be returned to the caller.
 *
 * Note that if the ioctl returns an EACCES error code, it is unlikely that
 * after some retries, the ioctl would succeed. So, if the ioctl returns
 * EACCES, we directly return this error to the caller.
 * For any other error from ioctl, we retry the ioctl for
 * the specified number of retries.
 *
 * The caller is responsible for allocating and deallocating
 * any space associated with the ioctl.
 *
 * This assumes that the ioctl flags and kcred are the same for all ioctls.
 */
int quorum_ioctl_with_retries(vnode_t *, int, intptr_t, int *);

char *quorum_ioctl_desc(int);

/*
 * quorum_scsi_get_sblkno
 *
 * Gets the starting block number for the PGRE reserved area on
 * the alternate cylinders, and caches it for future use.
 *
 */
extern int
quorum_scsi_get_sblkno(vnode_t *, uint64_t&);

extern int
quorum_scsi_sector_read(vnode_t *, uint64_t, pgre_sector_t *, bool);

extern void
quorum_scsi_sectorid_write(pgre_sector_t *, bool);

extern int
quorum_scsi_sector_write(vnode_t *, uint64_t, pgre_sector_t *, bool);

/*
 * quorum_disk_initialize_reserved
 *
 * Gets the starting block number for the reserved area on
 * the alternate cylinders, and caches it for future use.
 *
 * Initializes the PGRE ME vars, choosing and number, for the
 * local node, in the reserved area on the alternate cylinders.
 *
 * Caller can specify the message tracing level.
 * The default message tracing level is CMM_GREEN (full tracing).
 */
extern int quorum_disk_initialize_reserved(
    vnode_t *, uint64_t&, cmm_trace_level trace_level = CMM_GREEN);

/*
 * quorum_scsi_enable_failfast
 *
 * Enables failfast on the quorum device indicated.
 */
extern int quorum_scsi_enable_failfast(vnode_t *, int);

extern int
quorum_scsi_reset_bus(vnode_t *);

/*
 * quorum_scsi_data_write
 *
 * Write the data buffer to the specified sblkno and offset.
 * offset + length must be < PGRE_DATA_SIZE
 */
extern int
quorum_scsi_data_write(vnode_t *, uint64_t, uint64_t, const char *, uint64_t,
    bool);

/*
 * quorum_scsi_data_read
 *
 * Read the data buffer from the specified sblkno and offset.
 * offset + length must be < PGRE_DATA_SIZE
 */
extern int
quorum_scsi_data_read(vnode_t *, uint64_t, uint64_t, char *, uint64_t,
    bool);

/*
 * quorum_scsi_er_pair_write
 *
 * Writes the specified {epoch number, CMM reconfiguration number} pair to
 * the specified PGRe sector of the disk.
 */
extern int
quorum_scsi_er_pair_write(vnode_t *, uint64_t, ccr_data::epoch_type,
    cmm::seqnum_t, bool);

/*
 * quorum_scsi_er_pair_read
 *
 * Reads the {epoch number, CMM reconfiguration number} pair from
 * the specified PGRe sector of the disk.
 */
extern int
quorum_scsi_er_pair_read(vnode_t *, uint64_t, ccr_data::epoch_type &,
    cmm::seqnum_t &, bool);

/*
 * quorum_scsi_sector_scrub
 *
 * Initializes the PGRE sector area in question.
 *
 */
extern int
quorum_scsi_sector_scrub(vnode_t *, uint64_t, bool);

extern int
quorum_scrub(vnode_t *);

#endif // _KERNEL

#endif /* _SHARED_DISK_UTIL_H */
