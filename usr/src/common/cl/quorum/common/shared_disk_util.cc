//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)shared_disk_util.cc	1.17	09/02/18 SMI"

#include <sys/scsi/scsi.h>
#include <quorum/common/shared_disk_util.h>
#include <sys/os.h>
#include <sys/dkio.h>
#include <sys/file.h>
#include <cmm/cmm_debug.h>
#include <orb/infrastructure/orb_conf.h>

// Needed for definition of IOCDID_ISFIBRE
#include <sys/didio.h>

// Needed to check if OS supports EFI
#include <sys/cl_efi.h>


//
// Convert the given quorum::reservation_list_t to a mhioc list format.
// This will automatically allocate enough space in the mhioc list for
// the list of keys from the reservation_list.
//
int
convert_to_mhioc_list(mhioc_resv_desc_list_t &mhioc_list,
    quorum::reservation_list_t &idl_list)
{
	mhioc_list.listlen = idl_list.listlen;
	mhioc_list.listsize = idl_list.listsize;

	// Now allocate space for the array of keys and fill them in.
	mhioc_list.list = new mhioc_resv_desc_t[idl_list.listsize];

	for (uint_t i = 0; i < idl_list.listsize; i++) {
		bcopy(idl_list.keylist[i].key, mhioc_list.list[i].key.key,
		    sizeof (mhioc_resv_key));
	}

	return (0);
}

//
// Convert the given quorum::reservation_list_t to a mhioc list format.
// This will automatically allocate enough space in the mhioc list for
// the list of keys fromt he reservation_list.
//
int
convert_to_mhioc_reg_list(mhioc_key_list_t &mhioc_list,
    quorum::reservation_list_t &idl_list)
{
	mhioc_list.listlen = idl_list.listlen;
	mhioc_list.listsize = idl_list.listsize;

	// Now allocate space for the array of keys and fill them in.
	mhioc_list.list = new mhioc_resv_key_t[idl_list.listsize];

	for (uint_t i = 0; i < idl_list.listsize; i++) {
		bcopy(idl_list.keylist[i].key, mhioc_list.list[i].key,
		    sizeof (mhioc_resv_key));
	}

	return (0);
}

//
// Convert the given mhioc_resv_desc_list_t to an idl list format.
// This will automatically resize the idl list to have enough space
// for the keys in the mhioc list.
//
int
convert_to_idl_resv_list(mhioc_resv_desc_list_t &mhioc_list,
    quorum::reservation_list_t &idl_list)
{
	if (idl_list.listsize != mhioc_list.listsize) {
		idl_list.keylist.length(mhioc_list.listsize);
	}

	idl_list.listsize = mhioc_list.listsize;
	idl_list.listlen = mhioc_list.listlen;

	for (uint_t i = 0; i < idl_list.listsize; i++) {
		bcopy(mhioc_list.list[i].key.key, idl_list.keylist[i].key,
		    sizeof (mhioc_resv_key));
	}

	return (0);
}

//
// Convert the given mhioc_key_list_t to an idl list format.
// This will automatically resize the idl list to have enough space
// for the keys in the mhioc list.
//
int
convert_to_idl_reg_list(mhioc_key_list_t &mhioc_list,
    quorum::reservation_list_t &idl_list)
{
	if (idl_list.listsize != mhioc_list.listsize) {
		idl_list.keylist.length(mhioc_list.listsize);
	}

	idl_list.listsize = mhioc_list.listsize;
	idl_list.listlen = mhioc_list.listlen;

	for (uint_t i = 0; i < idl_list.listsize; i++) {
		bcopy(mhioc_list.list[i].key, idl_list.keylist[i].key,
		    sizeof (mhioc_resv_key));
	}

	return (0);
}

#ifdef _KERNEL

//
// Utility function to print out an error message associated with
// an ioctl. This maps ioctl ID's to char *.
//
char *
quorum_ioctl_desc(int ioctl)
{
	switch (ioctl) {
		case MHIOCGRP_REGISTER:
			return ("MHIOCGRP_REGISTER");
		case MHIOCGRP_REGISTERANDIGNOREKEY:
			return ("MHIOCGRP_REGISTERANDIGNOREKEY");
		case MHIOCGRP_RESERVE:
			return ("MHIOCGRP_RESERVE");
		case MHIOCGRP_PREEMPTANDABORT:
			return ("MHIOCGRP_PREEMPTANDABORT");
		case MHIOCGRP_INKEYS:
			return ("MHIOCGRP_INKEYS");
		case MHIOCGRP_INRESV:
			return ("MHIOCGRP_INRESV");
		case MHIOCRELEASE:
			return ("MHIOCRELEASE");
		case MHIOCENFAILFAST:
			return ("MHIOCENFAILFAST");
		case MHIOCTKOWN:
			return ("MHIOCTKOWN");
		case DKIOCGGEOM:
			return ("DKIOCGGEOM");
		case DKIOCPARTINFO:
			return ("DKIOCPARTINFO");
#if SOL_VERSION >= __s11
		case DKIOCEXTPARTINFO:
			return ("DKIOCEXTPARTINFO");
#endif
		case DKIOCPARTITION:
			return ("DKIOCPARTITION");
		case USCSICMD:
			return ("USCSICMD");
		default:
			return ("<unknown>");
	}
}

//
// Execute a given ioctl with the specified number of retries.
//
// Any errors will be returned to the caller.
//
// Note that if the ioctl returns an EACCES error code, it is unlikely that
// after some retries, the ioctl would succeed. So, if the ioctl returns
// EACCES, we directly return this error to the caller.
// For any other error from ioctl, we retry the ioctl for
// the specified number of retries.
//
// The caller is responsible for allocating and deallocating
// any space associated with the ioctl.
//
// This assumes that the ioctl flags and kcred are the same for all ioctls.
//
int
quorum_ioctl_with_retries(vnode_t *vp, int ioctl, intptr_t arg1, int *retval)
{
	int error = 0;
	int retries_remaining = MAX_RETRIES;

#if	SOL_VERSION >= __s11
	error = VOP_IOCTL(vp, ioctl, arg1, SCQUORUM_IOCTL_FLAGS, kcred, retval,
	    NULL);
#else
	error = VOP_IOCTL(vp, ioctl, arg1, SCQUORUM_IOCTL_FLAGS, kcred, retval);
#endif
	while ((error != 0) && (error != EACCES) && (retries_remaining != 0)) {
		if (error) {
			CMM_TRACE(("quorum_ioctl_with_retries: ioctl %s "
			    "returned error (%d). Will retry in %d seconds.\n",
			    quorum_ioctl_desc(ioctl), error, RETRY_SLEEP));
			os::usecsleep((os::usec_t) (RETRY_SLEEP * MICROSEC));
		}
#if	SOL_VERSION >= __s11
		error = VOP_IOCTL(vp, ioctl, arg1, SCQUORUM_IOCTL_FLAGS, kcred,
		    retval, NULL);
#else
		error = VOP_IOCTL(vp, ioctl, arg1, SCQUORUM_IOCTL_FLAGS, kcred,
		    retval);
#endif
		retries_remaining--;
	}

	if (error) {
		CMM_TRACE(("quorum_ioctl_with_retries: ioctl %s "
		    "returned error (%d).\n", quorum_ioctl_desc(ioctl), error));
	}

	return (error);
}


#ifdef SC_EFI_SUPPORT
//
// get_efi_reserved
//
// Gets the starting block number of the EFI reserved partition.
//
static int
get_efi_reserved(vnode_t *vnode_ptr, uint64_t &reserved_start)
{
	int			error = 0;
	int			retval = 0;
	struct partition64	prt;
	struct uuid reserved	= EFI_RESERVED;

	prt.p_partno = 0;
	do {
		if ((error = quorum_ioctl_with_retries(vnode_ptr,
		    DKIOCPARTITION, (intptr_t)&prt, &retval)) == 0) {
			if (bcmp((void *)&prt.p_type,
			    (void *)&reserved, sizeof (reserved)) == 0) {
				reserved_start = (uint64_t)prt.p_start;
				return (0);
			}
			prt.p_partno++;
		}
	} while (error == 0);
	return (error);
}
#endif

//
// quorum_scsi_get_sblkno
//
// Gets the starting block number for the PGRE reserved area on
// the alternate cylinders, and caches it for future use.
//
int
quorum_scsi_get_sblkno(vnode_t *qhandle, uint64_t& sblkno)
{
	struct dk_geom	*disk_geom;
	uint64_t	ncyl, nblks_per_cyl;
	uint64_t	partition_offset = 0;
	int 		error = 0;
	int		retval = 0;
	bool		retry_flag = true;

	vnode_t		*vnode_ptr = (vnode_t *)qhandle;
	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	disk_geom =
	    (struct dk_geom *)kmem_alloc(sizeof (struct dk_geom), KM_SLEEP);

	//
	// The PGRE area is in the following portion of the reserved
	// cylinders (PSARC 1999/430):
	// 	PGRE_NUM_SECTORS sectors from the 2nd last track of
	// 	the first alternate cylinder.
	//
	// There is one set of reserved cylinders for each Solaris
	// partition on a disk. On SPARC architectures, dXs2 is the entire
	// Solaris partition, which is also the entire disk. On Intel
	// architectures, dXs2 is the entire (active) Solaris partition,
	// but there may be other partitions, and the first cylinder of the
	// disk will always be reserved for the fdisk partition table.
	//
	// To calculate the location of the PGRE area, we need to first
	// find the offset of the Solaris partition on the disk, and then
	// find the offset of the PGRE cylinders within the Solaris
	// partition.
	//

#if defined(__i386) || defined(__amd64)

	//
	// TODO: DKIOCEXTPARTINFO is currently supported only
	// for nevada (s11). Once its back ported to s10, remove
	// the conditional compilation for s11.
	//

#if SOL_VERSION >= __s11

	//
	// PSARC 2008/336 introduced support for booting up disks
	// which are more than 1Tb in size. This introduced the
	// new ioctl DKIOCEXTPARTINFO.  Cluster needs to support
	// quorum devices which are more than 1Tb in size. Hence,
	// we will first try DKIOEXTPARTINFO ioctl which replaces
	// the old ioctl DKIOCPARTINFO in order to get the partition
	// information of the disk.
	// Devices which do not support this new ioctl (DKIOCEXTPARTINFO)
	// will return error ENOTTY. For such devices we will
	// continue using the old ioctl DKIOCPARTINFO.
	//

	extpart_info ext_p_info;

	error = quorum_ioctl_with_retries(vnode_ptr, DKIOCEXTPARTINFO,
	    (intptr_t)&ext_p_info, &retval);

	if (error == ENOTTY) {
		//
		// Device does not support the DKIOCEXTPARTINFO
		// ioctl. Retry with the old ioctl DKIOCPARTINFO
		//
		retry_flag = true;
		CMM_TRACE((
		    "quorum_scsi_get_sblkno: ioctl DKIOCEXTPARTINFO "
		    "not supported. Retry with DKIOCPARTINFO (%d).\n",
		    error));
	} else {
		if (error != 0) {
			//
			// Some other error has occured.
			//
			CMM_TRACE((
			    "quorum_scsi_get_sblkno: ioctl DKIOCEXTPARTINFO "
			    "returned error (%d).\n", error));
			kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
			return (error);
		} else {
			partition_offset = ext_p_info.p_start;

			//
			// No need to retry the old ioctl
			//
			retry_flag = false;
		}
	}

#endif	// SOL_VERSION >= __s11

	part_info p_info;

	if (retry_flag) {
		// Try the old ioctl DKIOCPARTINFO
		error = quorum_ioctl_with_retries(vnode_ptr, DKIOCPARTINFO,
		    (intptr_t)&p_info, &retval);

		if (error != 0) {
			CMM_TRACE((
			    "quorum_scsi_get_sblkno: ioctl DKIOCPARTINFO "
			    "returned error (%d).\n", error));
			kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
			return (error);
		} else {
			partition_offset = p_info.p_start;
		}
	}

#endif // (__i386) || (__amd64)

	// First get the disk geometry.
	error = quorum_ioctl_with_retries(vnode_ptr, DKIOCGGEOM,
	    (intptr_t)disk_geom, &retval);

#ifdef SC_EFI_SUPPORT
	// ENOTSUP indicates an EFI disk
	if (error && error != ENOTSUP) {
#else
	if (error != 0) {
#endif
		CMM_TRACE(("quorum_scsi_get_sblkno: ioctl DKIOCGGEOM "
		    "returned error (%d).\n", error));
		kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
		return (error);
	}

#ifdef SC_EFI_SUPPORT
	// ENOTSUP indicates an EFI disk
	if (error != 0 && error == ENOTSUP) {
		uint64_t	efi_rsvstart_blk;
		if ((error =
		    get_efi_reserved(vnode_ptr, efi_rsvstart_blk)) != 0) {
			CMM_TRACE(("quorum_scsi2_get_sblkno: cant find"
			    " EFI V_RESERVED partition, return error (%d).\n",
			    error));
			kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
			return (error);
		}
		//
		// PSARC2001-570: sectors 15-79 of EFI V_RESERVED
		// partition is the SC PGRE area. Sector 0 is fabricated devid.
		//
		// ----------------------------------------------------------
		// ^   <-------- SC PGRE: 15-79 ----------->
		// |
		// EFI V_RESERVED partition start, in number of sectors
		//
		sblkno = efi_rsvstart_blk + SC_EFI_RESERVED_START;
		kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
		return (0);
	}
#endif

	//
	// This check is probably not necessary, but it is done
	// in the code for writing devids etc.
	//
	if (disk_geom->dkg_acyl < 2) {
		CMM_TRACE(("quorum_scsi_get_sblkno: Number of "
		    "alternate cylinders is less than 2.\n"));
		error = EINVAL;
		kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
		return (error);
	}

	//
	// To determine starting block number, consider the following
	// layout for the first alternate cylinder (not to scale):
	//
	//  -----------------------------------------------------
	// |			| First Alternate Cylinder	|
	// | Data Cylinders	|-------------------------------|
	// |			| unused| PGRE area |devid area |
	// |			|	| (65 sects)| (a track )|
	//  -----------------------------------------------------
	//

	// The total number of data cylinders + the first alt cylinder.
	ncyl = disk_geom->dkg_ncyl + disk_geom->dkg_acyl - 1;

	// The total number of sectors/blocks contained in a cylinder.
	nblks_per_cyl = (uint64_t)disk_geom->dkg_nsect * disk_geom->dkg_nhead -
	    disk_geom->dkg_apc;


	//
	// The starting block number =
	//	the total number of blocks incl the first alternate cyl
	//	- number of blocks reserved for the devid project
	//	  (= number of blocks on the last track)
	//	- number of blocks reserved for the SCSI-2 quorum project
	//	  (= PGRE_NUM_SECTORS).
	//
	sblkno = partition_offset + ncyl * nblks_per_cyl -
		disk_geom->dkg_nsect - PGRE_NUM_SECTORS;
	kmem_free((caddr_t)disk_geom, sizeof (struct dk_geom));
	return (error);
}

//
// quorum_scsi_sector_read
//
// Reads the blk of PGRE data specified into the pgre_sector_t
// area passed in (allocated).
//
int
quorum_scsi_sector_read(
    vnode_t		*vnode_ptr,
    uint64_t		blk,
    pgre_sector_t	*sector,
    bool		check_data)
{
	struct uscsi_cmd 	ucmd;
	union scsi_cdb		cdb;
	uint_t			chksum, *iptr;
	int			error = 0;
	int			retval = 0;

	bzero((caddr_t)&ucmd, sizeof (ucmd));
	bzero((caddr_t)&cdb, sizeof (cdb));
	ucmd.uscsi_flags = USCSI_READ;
	ucmd.uscsi_bufaddr = (caddr_t)sector;
	ucmd.uscsi_buflen = DEV_BSIZE;
	ucmd.uscsi_flags |= USCSI_RQENABLE;

	ucmd.uscsi_rqbuf =
	    (caddr_t)kmem_alloc((size_t)SENSE_LENGTH, KM_SLEEP);

	if (ucmd.uscsi_rqbuf == NULL) {
		return (ENOMEM);
	}

	ucmd.uscsi_rqlen = (uchar_t)SENSE_LENGTH;
	cdb.scc_cmd = SCMD_READ;

	if (blk < (2<<20)) {
		FORMG0ADDR(&cdb, (unsigned)blk); //lint !e704
		FORMG0COUNT(&cdb, (uchar_t)1);
		ucmd.uscsi_cdblen = CDB_GROUP0;
	} else {
		FORMG1ADDR(&cdb, (unsigned)blk); //lint !e704 !e734
		FORMG1COUNT(&cdb, (uchar_t)1); //lint !e572 !e778
		ucmd.uscsi_cdblen = CDB_GROUP1;
		cdb.scc_cmd |= SCMD_GROUP1;
	}

	ucmd.uscsi_cdb = (caddr_t)&cdb;

	error = quorum_ioctl_with_retries(vnode_ptr, USCSICMD, (intptr_t)&ucmd,
	    &retval);
	if (error != 0) {
		CMM_TRACE(("quorum_scsi_sector_read: ioctl USCSICMD "
		    "returned error (%d).\n", error));
		kmem_free(ucmd.uscsi_rqbuf, (size_t)SENSE_LENGTH);
		return (error);
	}

	//
	// Calculate and compare the checksum if check_data is true.
	// Also, validate the pgres_id string at the beg of the sector.
	//
	if (check_data) {
		PGRE_CALCCHKSUM(chksum, sector, iptr);

		// Compare the checksum.
		if (PGRE_GETCHKSUM(sector) != chksum) {
			CMM_TRACE(("quorum_scsi_sector_read: "
			    "checksum mismatch.\n"));
			kmem_free(ucmd.uscsi_rqbuf, (size_t)SENSE_LENGTH);
			return (EINVAL);
		}

		//
		// Validate the PGRE string at the beg of the sector.
		// It should contain PGRE_ID_LEAD_STRING[1|2].
		//
		if ((os::strncmp((char *)sector->pgres_id, PGRE_ID_LEAD_STRING1,
		    strlen(PGRE_ID_LEAD_STRING1)) != 0) &&
		    (os::strncmp((char *)sector->pgres_id, PGRE_ID_LEAD_STRING2,
		    strlen(PGRE_ID_LEAD_STRING2)) != 0)) {
			CMM_TRACE(("quorum_scsi_sector_read: pgre id "
			    "mismatch. The sector id is %s.\n",
			    sector->pgres_id));
			kmem_free(ucmd.uscsi_rqbuf, (size_t)SENSE_LENGTH);

			os::sc_syslog_msg *syslog_msgp;
			char nodename[8];
			os::sprintf(nodename, "%d", orb_conf::local_nodeid());
			syslog_msgp = new os::sc_syslog_msg(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			if (syslog_msgp == NULL) {
				// Memory allocation failure
				CL_PANIC(0);
			}
			//
			// SCMSGS
			// @explanation
			// The Persistent Group Reservation Emulation string
			// that is used to support a SCSI-2 quorum device is
			// missing. This typically happens when
			// someone formats a configured quorum device.
			// When someone adds an entire disk to a ZFS zpool,
			// ZFS will format the disk.
			// The act of formatting a disk destroys
			// information on the quorum disk that
			// is needed by the quorum subsystem.
			// @user_action
			// The corrective action is to unconfigure
			// the quorum device and then configure
			// the same quorum device.
			// If the above action does not correct the problem,
			// contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available for this error.
			//
			(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: PGRE string missing from quorum device\n");
			delete syslog_msgp;

			return (EINVAL);
		}

	}
	kmem_free(ucmd.uscsi_rqbuf, (size_t)SENSE_LENGTH);

	return (error);
}

//
// quorum_scsi_sectorid_write
//
// Writes the PGRE id string at the beginning of the sector.
//
void
quorum_scsi_sectorid_write(pgre_sector_t *sector, bool is_owner_area)
{
	if (is_owner_area) {
		(void) os::snprintf((char *)sector->pgres_id,
		    PGRE_ID_SIZE * sizeof (uchar_t),
		    "%s", PGRE_ID_LEAD_STRING1);
	} else {
		//
		// orb_conf calls need ORB support but user processes may
		// call this function in non-cluster mode as a result
		// calls to orb_conf are not made
		//
		(void) os::snprintf((char *)sector->pgres_id,
		    PGRE_ID_SIZE * sizeof (uchar_t),
		    "%s%2d", PGRE_ID_LEAD_STRING2, orb_conf::local_nodeid());
	}
}

//
// quorum_scsi_sector_write
//
// Writes the pgre_sector_t area passed in to the specified blk of
// PGRE area on the reserved cylinders.
//
int
quorum_scsi_sector_write(
    vnode_t		*vnode_ptr,
    uint64_t		blk,
    pgre_sector_t	*sector,
    bool		is_owner_area)
{
	struct uscsi_cmd 	ucmd;
	union scsi_cdb		cdb;
	uint_t			chksum, *iptr;
	int			error = 0;
	int			retval = 0;

	// Fill in the id field of the sector.
	quorum_scsi_sectorid_write(sector, is_owner_area);

	// Calculate the checksum.
	PGRE_CALCCHKSUM(chksum, sector, iptr);

	// Fill in the checksum.
	PGRE_FORMCHKSUM(chksum, sector);

	bzero((caddr_t)&ucmd, sizeof (ucmd));
	bzero((caddr_t)&cdb, sizeof (cdb));
	ucmd.uscsi_flags = USCSI_WRITE;
	ucmd.uscsi_bufaddr = (caddr_t)sector;
	ucmd.uscsi_buflen = DEV_BSIZE;
	ucmd.uscsi_flags |= USCSI_RQENABLE;

	ucmd.uscsi_rqbuf =
	    (caddr_t)kmem_alloc((size_t)SENSE_LENGTH, KM_SLEEP);

	ucmd.uscsi_rqlen = (uchar_t)SENSE_LENGTH;
	cdb.scc_cmd = SCMD_WRITE;

	if (blk < (2<<20)) {
		FORMG0ADDR(&cdb, (unsigned)blk); //lint !e704
		FORMG0COUNT(&cdb, (uchar_t)1);
		ucmd.uscsi_cdblen = CDB_GROUP0;
	} else {
		FORMG1ADDR(&cdb, (unsigned)blk); //lint !e704 !e734
		FORMG1COUNT(&cdb, (uchar_t)1); //lint !e572 !e778
		ucmd.uscsi_cdblen = CDB_GROUP1;
		cdb.scc_cmd |= SCMD_GROUP1;
	}

	ucmd.uscsi_cdb = (caddr_t)&cdb;

	error = quorum_ioctl_with_retries(vnode_ptr, USCSICMD, (intptr_t)&ucmd,
	    &retval);
	if (error) {
		CMM_TRACE(("quorum_scsi_sector_write: ioctl USCSICMD "
		    "returned error (%d).\n", error));
		kmem_free(ucmd.uscsi_rqbuf, (size_t)SENSE_LENGTH);
		return (error);
	}

	kmem_free(ucmd.uscsi_rqbuf, (size_t)SENSE_LENGTH);

	//
	// Validate the pgre id string after every write.
	//
	pgre_sector_t *sector_chk =
	    (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);
	if (sector_chk == NULL) {
		// Memory allocation failure
		CL_PANIC(0);
	}
	int error_chk =
	    quorum_scsi_sector_read(vnode_ptr, blk, sector_chk, true);

	if (error_chk != 0) {
		CMM_TRACE(("quorum_scsi_sector_write: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error_chk));
		os::sc_syslog_msg *syslog_msgp;
		char nodename[8];
		os::sprintf(nodename, "%d", orb_conf::local_nodeid());
		syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_CMM_QUORUM_TAG,
		    nodename, NULL);
		if (syslog_msgp == NULL) {
			// Memory allocation failure
			CL_PANIC(0);
		}
		//
		// SCMSGS
		// @explanation
		// After writing to a sector, an attempt to read the sector
		// for validation of the Persistent Group Reservation Emulation
		// (PGRE) ID string failed.
		// The error could be either in the driver or the hardware.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available
		// for this driver/hardware error.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: sector read for validation failed after write, "
		    "error code %d\n", error_chk);
		delete syslog_msgp;

		kmem_free((caddr_t)sector_chk, (size_t)DEV_BSIZE);
		return (error_chk);
	}

	//
	// Do a byte comparison of the data that we wrote
	// and the data that we just read.
	//
	if (bcmp(sector, sector_chk, (size_t)DEV_BSIZE)) {
		// Data mismatch
		CMM_TRACE(("quorum_scsi_sector_write: "
		    "quorum_scsi_sector_read successful, but data mismatch\n"));
		os::sc_syslog_msg *syslog_msgp;
		char nodename[8];
		os::sprintf(nodename, "%d", orb_conf::local_nodeid());
		syslog_msgp = new os::sc_syslog_msg(SC_SYSLOG_CMM_QUORUM_TAG,
		    nodename, NULL);
		if (syslog_msgp == NULL) {
			// Memory allocation failure
			CL_PANIC(0);
		}
		//
		// SCMSGS
		// @explanation
		// After writing to a sector, the sector was read
		// successfully. But the data just written and the data just
		// read do not match.
		// The error could be either in the driver or the hardware.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available
		// for this driver/hardware error.
		//
		(void) syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: data read does not match data just written.");
		delete syslog_msgp;

		kmem_free((caddr_t)sector_chk, (size_t)DEV_BSIZE);
		return (EINVAL);
	}

	kmem_free((caddr_t)sector_chk, (size_t)DEV_BSIZE);
	return (0);
}

//
// quorum_disk_initialize_reserved
//
// Determines the starting block number of the reserved area on the alternate
// cylinders for quorum info.
//
// Caller can specify the message tracing level.
// The default message tracing level is CMM_GREEN (full tracing).
//
int
quorum_disk_initialize_reserved(
    vnode_t *qhandle, uint64_t& sblkno, cmm_trace_level trace_level)
{
	int		error = 0;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_disk_initialize_reserved(%p) called.\n", qhandle));
	if (qhandle == NULL) {
		return (ENOENT);
	}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
/*
	if (check_fi(qhandle, "quorum_disk_initialize_reserved") != 0) {
		return (EIO);
	}
*/
#endif // FAULT_CMM && _KERNEL_ORB

	// Get the disk geom.
	error = quorum_scsi_get_sblkno(qhandle, sblkno);
	if (error != 0) {
		// EACCES will be returned if access rights were denied.
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("quorum_scsi_get_sblkno(%p) retd error %d.\n",
		    qhandle, error));
		// sblkno = INV_BLKNO;
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_disk_initialize_reserved(%p) retd sblkno (0x%llx).\n",
	    qhandle, sblkno));
	return (error);
}

//
// quorum_scsi_enable_failfast
//
// Enable failfast on the indicated quorum device.
//
int
quorum_scsi_enable_failfast(vnode_t *qhandle, int poll_period)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	int			error = 0;
	int			retval = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCENFAILFAST,
	    (intptr_t)&poll_period, &retval);
	if (error) {
		CMM_TRACE(("quorum_scsi_enable_failfast: ioctl MHIOCENFAILFAST "
		    "failed with error %d. \n", error));
	}

	return (error);
}

//
// quorum_scsi_reset_bus
//
// Reset the bus to which this QD is connected provided that disk
// is non-fibre.
//
//
int
quorum_scsi_reset_bus(vnode_t *qhandle)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	struct uscsi_cmd	ucmd;
	int			error = 0;
	int			retval = 0;


	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	//
	// A reset should only be done if the disk is non-fibre.
	// The IOCDID_ISFIBRE ioctl should not be retried if we
	// get an error.
	//
#if	SOL_VERSION >= __s11
	error = VOP_IOCTL(vnode_ptr, IOCDID_ISFIBRE, (intptr_t)NULL,
	    FKIOCTL, kcred, &retval, NULL);
#else
	error = VOP_IOCTL(vnode_ptr, IOCDID_ISFIBRE, (intptr_t)NULL,
	    FKIOCTL, kcred, &retval);
#endif

	//
	// If the disk is fibre (error = 0) or if there was an error
	// accessing the device (error != ENOENT), do not attempt to
	// reset the bus.
	//
	if (error != ENOENT) {
		if (error == 0) {
			CMM_TRACE(("quorum_scsi_reset_bus: QD is of type "
			    "fibre, bypassing bus reset.\n"));
		} else {
			CMM_TRACE(("quorum_scsi_reset_bus: ioctl "
			    "IOCDID_ISFIBRE failed with error (%d), "
			    "bypassing bus reset.\n", error));
		}
		return (error);
	}

	bzero((caddr_t)&ucmd, sizeof (ucmd));
	ucmd.uscsi_flags = USCSI_RESET_ALL;

	error = quorum_ioctl_with_retries(vnode_ptr, USCSICMD, (intptr_t)&ucmd,
	    &retval);
	if (error) {
		CMM_TRACE(("quorum_scsi_reset_bus: ioctl USCSICMD "
		    "returned error (%d).\n", error));
	}

	return (error);
}

//
// quorum_scsi_sector scrub
//
// Initializes the PGRE sector area in question.
//
//
int
quorum_scsi_sector_scrub(
    vnode_t 	*qhandle,
    uint64_t 	sblkno,
    bool	is_owner_area)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	int			error = 0;


	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector =
	    (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);

	bzero((caddr_t)sector, sizeof (pgre_sector_t)); // Redundant?
	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector,
	    is_owner_area);

	if (error != 0) {
		CMM_TRACE(("quorum_scsi_sector_scrub: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_scsi_data_write
//
// Writes the specified data to the disk, starting at sblkno, which should
// be in the PGRe reserved area.
//
// Data must be smaller than PGRE_DATA_SIZE.
//
int
quorum_scsi_data_write(
    vnode_t			*qhandle,
    uint64_t			sblkno,
    uint64_t			offset,
    const char			*data,
    uint64_t			length,
    bool			verbose)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);
	CL_PANIC(offset + length < PGRE_DATA_SIZE);

	// Allocate the sector buffer.
	sector =
	    (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_data_write: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
		kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
		return (error);
	}

	bcopy(data, sector->pgres_data + offset, (unsigned long)length);

	// Now write the contents of sector in the corresp blk on disk.
	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_scsi_data_write: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	if (verbose) {
		CMM_TRACE(("quorum_scsi_data_write: wrote data %s "
		    "for sblkno (0x%llx).\n",
		    data, sblkno));
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_scsi_data_read
//
// Reads "length" data from the PGRe area sblkno given, with the given offset.
// This is only intended to read data from a single sector, so length +
// offset must be less than the total size of the PGRe area for the sector.
//
int
quorum_scsi_data_read(
    vnode_t			*qhandle,
    uint64_t			sblkno,
    uint64_t			offset,
    char			*data,
    uint64_t			length,
    bool			verbose)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);
	CL_PANIC(offset + length < PGRE_DATA_SIZE);

	// Allocate the sector buffer.
	sector =
	    (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);

	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		CMM_TRACE(("quorum_scsi_data_read: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
	} else {
		bcopy(sector->pgres_data + offset, (void *)data,
		    (unsigned long)length);

		if (verbose) {
			CMM_TRACE(("quorum_scsi_data_read: read data %s "
			    "for sblkno (0x%llx).\n",
			    data, sblkno));
		}
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_scsi_er_pair_write
//
// Writes the specified {epoch number, CMM reconfiguration number} pair to
// the specified PGRe sector of the disk.
//
int
quorum_scsi_er_pair_write(
    vnode_t			*qhandle,
    uint64_t			sblkno,
    ccr_data::epoch_type	epoch,
    cmm::seqnum_t		seqnum,
    bool			verbose)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector =
	(pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);

	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_scsi_er_pair_write: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
		kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
		return (error);
	}

	// Change the [E,R] pair as specified above.
	pgre_data_ptr->last_epoch = epoch;
	pgre_data_ptr->last_reconfig = seqnum;

	// Now write the contents of sector in the corresp blk on disk.
	error = quorum_scsi_sector_write(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		CMM_TRACE(("quorum_scsi_er_pair_write: "
		    "quorum_scsi_sector_write returned error (%d).\n",
		    error));
	}

	if (verbose) {
		CMM_TRACE(("quorum_scsi_er_pair_write: wrote epoch %ld and "
		    "seqnum %llu for sblkno (0x%llx).\n",
		    epoch, seqnum, sblkno));
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_scsi_er_pair_read
//
// Reads the {epoch number, CMM reconfiguration number} pair from
// the specified PGRe sector of the disk.
//
int
quorum_scsi_er_pair_read(
    void			*qhandle,
    uint64_t			sblkno,
    ccr_data::epoch_type	&epoch,
    cmm::seqnum_t		&seqnum,
    bool			verbose)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Allocate the sector buffer.
	sector =
	    (pgre_sector_t *)kmem_zalloc((size_t)DEV_BSIZE, KM_SLEEP);

	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		CMM_TRACE(("quorum_scsi_er_pair_read: "
		    "quorum_scsi_sector_read returned error (%d).\n",
		    error));
	} else {
		epoch = pgre_data_ptr->last_epoch;
		seqnum = pgre_data_ptr->last_reconfig;

		if (verbose) {
			CMM_TRACE(("quorum_scsi_er_pair_read: read epoch %ld "
			    "and seqnum %llu for sblkno (0x%llx).\n",
			    epoch, seqnum, sblkno));
		}
	}

	kmem_free((caddr_t)sector, (size_t)DEV_BSIZE);
	return (error);
}

//
// quorum_scrub
//
// Scrubs the PGRE area on the alternate cylinders of a disk.
//
int
quorum_scrub(vnode_t *qhandle)
{
	int 		error = 0;
	uint64_t	sblkno;
	uint_t		i;

	CMM_TRACE(("quorum_scrub(%p) called.\n", qhandle));
	if (qhandle == NULL) {
		return (ENOENT);
	}

	error = quorum_scsi_get_sblkno(qhandle, sblkno);
	if (error != 0) {
		CMM_TRACE(("quorum_scrub: quorum_scsi_get_sblkno() "
		    "retd error %d.\n", error));
		return (error);
	}

	//
	// Initialize each sector one by one. Use PGRE_NUM_SECTORS
	// rather than NODEID_MAX since the former directly defines
	// the PGRE area.
	//

	// First sector is owner area.
	error = quorum_scsi_sector_scrub(qhandle, sblkno, true);
	if (error != 0) {
		CMM_TRACE(("quorum_scrub: quorum_scsi_sector_scrub() "
		    "for owner area retd error %d.\n", error));
		return (error);
	}

	for (i = 1; i < PGRE_NUM_SECTORS; i++) {
		error = quorum_scsi_sector_scrub(
		    qhandle,
		    PGRE_GET_NODE_BLKNO(sblkno, i), false);
		if (error != 0) {
			CMM_TRACE(("quorum_scrub: quorum_scsi_sector_scrub() "
			    "for node area (%d) retd error %d.\n", i, error));
			return (error);
		}

		error = quorum_scsi_er_pair_write(qhandle,
		    PGRE_GET_NODE_BLKNO(sblkno, i), (ccr_data::epoch_type) -1,
		    (cmm::seqnum_t) 0, false);
		if (error != 0) {
			CMM_TRACE(("quorum_scrub: quorum_scsi_er_pair_write() "
			    "for node area (%d) retd error %d.\n", i, error));
			return (error);
		}
	}
	return (error);
}


#endif // _KERNEL
