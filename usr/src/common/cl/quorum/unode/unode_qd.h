/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UNODE_QD_H
#define	_UNODE_QD_H

#pragma ident	"@(#)unode_qd.h	1.10	08/10/01 SMI"

/* Interfaces to quorum device implementation of unode devices. */

#include <orb/object/adapter.h>
#include <h/quorum.h>


class quorum_device_unode_impl :
	public McServerof < quorum::quorum_device_type > {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//

	quorum::quorum_error_t	quorum_open(
	    const char *devname,
	    const bool scrub,
	    const quorum::qd_property_seq_t &,
	    Environment &);

	uint32_t	quorum_supports_amnesia_prot(Environment &);

	quorum::quorum_error_t	quorum_close(Environment &);

	quorum::quorum_error_t	quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	quorum::quorum_error_t	quorum_read_reservations(
	    quorum::reservation_list_t		&resv_list,
	    Environment			&);

	quorum::quorum_error_t	quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&victim_key,
	    Environment				&);

	quorum::quorum_error_t	quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	quorum::quorum_error_t	quorum_read_keys(
	    quorum::reservation_list_t		&reg_list,
	    Environment			&);

	quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&);

	quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	quorum::quorum_error_t	quorum_data_read(
		uint64_t		location,
		CORBA::String_out	data,
		Environment		&);

	quorum::quorum_error_t	quorum_data_write(
		uint64_t		location,
		const char		*data,
		Environment		&);

	quorum::quorum_error_t	quorum_remove(
	    const char			*devname,
	    const quorum::qd_property_seq_t &qd_props,
	    Environment			&);

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

	quorum_device_unode_impl();
	~quorum_device_unode_impl();
private:

	//
	// The pointer to the device.
	//
	int		*qhandle;

	//
	// Information about the disk geometry. We get this once during
	// quorum_open and then hold onto it for all read and write calls.
	//
	uint64_t	sblkno;

	//
	// There will be times when the initial quorum_open call will not
	// be able to acces the device due to fencing. The quorum algorithm
	// may then make future calls to the module, and we should retry
	// the reserved sector and pgre initialization.
	//
	bool		retry;

	//
	// The name of the device. Used mainly for debugging purposes.
	//
	char		*pathname;

	//
	// Exclusive access functions as required for PGRe.
	//
	int	unode_release();
	int	unode_tkown();
	int	quorum_unode_get_sblkno();

	// Function for fault injection testing.
	int	check_fi(char*);

	// True if failfast has been enabled
	bool	failfast_enabled;
};

//
// Static accessor function to the constructor which is used by the
// type registry to modlookup.
//
static inline quorum::quorum_device_type_ptr get_unode_qd_impl() {
	return ((new quorum_device_unode_impl())->get_objref());
}


#endif	/* _UNODE_QD_H */
