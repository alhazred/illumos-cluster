//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)unode_qd.cc	1.14	09/04/29 SMI"

#include <fcntl.h>
#include <sys/modctl.h>
#include <h/quorum.h>
#include <quorum/unode/unode_qd.h>
#include <unode/qd.h>
#include <cmm/cmm_debug.h>
#include <quorum/common/qd_map.h>
#include <quorum/common/quorum_pgre.h>
#include <nslib/ns.h>
#include <quorum/common/quorum_util.h>

#if defined(FAULT_CMM)

// Global map to paths from handles (quorum device handles)
qd_map	qd_map_info;

// Class qd_map methods.

// Constructor sets head to NULL.
qd_map::qd_map()
{
}

// Destructor
qd_map::~qd_map()
{
	mappings.dispose();
}

void
qd_map::add_qd(void *handle, const char *path)
{
	list_lock.lock();

	list_cell	*new_cell = new list_cell;
	new_cell->path = os::strdup(path);
	new_cell->qd_handle = handle;
	mappings.append(new_cell);
	list_lock.unlock();
}

char *
qd_map::find_qd(void *handle)
{
	list_cell	*pos;
	char		*rslt = NULL;

	list_lock.lock();

	SList<list_cell>::ListIterator li(mappings);
	while ((pos = li.get_current()) != NULL) {
		if (handle == pos->qd_handle) {
			rslt = pos->path;
			break;
		}
		li.advance();
	}
	list_lock.unlock();
	return (rslt);
}

int
quorum_device_unode_impl::check_fi(char	*qop_name)
{
	// Fault point will return failure
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    &fault_argp, &fault_argsize)) {
		char *exp_path = (char *)fault_argp;
		char *cur_path = qd_map_info.find_qd((void *)qhandle);
		if ((exp_path == NULL) ||
		    ((cur_path != NULL) && (strcmp(cur_path, exp_path) == 0))) {
			os::printf("==> Fault Injection failing %s operation\n",
			    qop_name);
			return (EIO);
		}
	}
	return (0);
}

extern int 	errno;
#endif

// int
quorum::quorum_error_t
quorum_door_call(
		int		*qhandle,
		qd_command_vals	command,
		char		*in_args,
		size_t		in_size,
		char		*out_args,
		size_t		&out_size,
		uint64_t	fi_in,
		uint64_t	&fi_out)
{
	CMM_TRACE(("quorum_door_call called for operation %d\n", command));
#ifdef linux
	return (quorum::QD_EIO);
#else
	door_arg_t	darg;
	int 		fd = *((int *)qhandle);
	quorum::quorum_error_t ret = quorum::QD_SUCCESS;

#if defined(FAULT_CMM)
	// Fault point will return failure
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_DELAY,
	    &fault_argp, &fault_argsize)) {
		char *exp_path = (char *)fault_argp;
		char *cur_path = qd_map_info.find_qd(qhandle);
		if ((exp_path == NULL) ||
		    ((cur_path != NULL) && (strcmp(cur_path, exp_path) == 0))) {
			os::printf("==> Fault Injection delaying quorum "
			    "operation\n");
			os::usecsleep((os::usec_t) (5*1000000));
		}
	}
#endif // FAULT_CMM

	CMM_TRACE(("quorum_door_call: checkpoint 1\n"));

	darg.data_size =
	    sizeof (uint64_t) * 4 + (in_size > out_size ? in_size : out_size);
	uint64_t *args = (uint64_t *)new char[darg.data_size];

	args[0] = orb_conf::node_number();
	args[1] = 0;
	args[2] = (unsigned)command;
	args[3] = fi_in;

	if (in_size != 0) {
		bcopy(in_args, &(args[4]), in_size);
	}

	CMM_TRACE(("quorum_door_call: checkpoint 2\n"));
	darg.data_ptr = (char *)args;
	darg.rbuf = (char *)args;
	darg.rsize = darg.data_size;
	darg.desc_num = 0;
	darg.desc_ptr = NULL;

	CMM_TRACE(("quorum_door_call: making actual door_call\n"));
	int result = door_call(fd, &darg);

	if (result < 0) {
		// ret = QD_DOOR_CALL_FAILED;
		ret = quorum::QD_EIO;
	} else {
		ASSERT(darg.rsize <= darg.data_size);
		fi_out = args[1];
		darg.rsize -= 2 * sizeof (uint64_t);
		out_size = (out_size < darg.rsize) ? out_size : darg.rsize;
		if ((out_args != NULL) && (out_size > 0)) {
			bcopy(&(args[2]), out_args, out_size);
		}
		// ret = (int)args[0];
		if ((int)args[0] == EACCES) {
			ret = quorum::QD_EACCES;
		} else if ((int)args[0] == 0) {
			ret = quorum::QD_SUCCESS;
		} else {
			ret = quorum::QD_EACCES;
		}
	}

	delete [] args;
	return (ret);
#endif
}

//lint -e545

//
// Constructor
//
quorum_device_unode_impl::quorum_device_unode_impl()
{
	qhandle = NULL;
	retry = false;
	sblkno = INV_BLKNO;
	pathname = NULL;
	failfast_enabled = false;
}

//
// Destructor
//
quorum_device_unode_impl::~quorum_device_unode_impl()
{
	delete pathname;
}

//
// Unreferenced
//
void
#ifdef DEBUG
quorum_device_unode_impl::_unreferenced(unref_t cookie)
#else
quorum_device_unode_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_TRACE(("UNODE(%s)::_unreferenced.\n", pathname));

	//
	// Call quorum_close() here to clean-up state.
	// quorum_close() is implemented to be idempotent
	// and hence it is safe to call quorum_close() here.
	//
	// This object is going away; so ignore return value
	// and exception from quorum_close().
	//
	Environment e;
	(void) quorum_close(e);
	e.clear();

	delete this;
}

//
// IDL method to set the message tracing level of this quorum device
// This method is a no-op for unode qd.
//
void
quorum_device_unode_impl::set_message_tracing_level(uint32_t, Environment &)
{
}

//
// IDL method to get the message tracing level of this quorum device
// The unode qd object has no tracing level associated with it.
// So we return the maximum CMM_GREEN tracing level.
//
uint32_t
quorum_device_unode_impl::get_message_tracing_level(Environment &)
{
	return ((uint32_t)CMM_GREEN);
}

//
// quorum_open
//
// In the simulation of quorum_open, we get a descriptor for the door, and
// then call it, which makes sure that it's a valid door, and that there is
// no internal logic currently in the quorum device that wants to cause this
// operation to fail.
//
quorum::quorum_error_t
quorum_device_unode_impl::quorum_open(const char *devname, const bool scrub,
    const quorum::qd_property_seq_t &, Environment &)
{
	CMM_TRACE(("UNODE(%s)::quorum_open\n", devname));

#ifdef linux
	return (quorum::QD_EIO);
#else
	//
	// Make a copy of the pathname for debugging purposes.
	// A particular quorum device object is associated with
	// a single configured quorum device always.
	// It is possible that an earlier quorum_open attempt
	// has initialized the pathname.
	// If so, we do not need to duplicate the string again.
	//
	if (pathname == NULL) {
		// First open attempt
		pathname = os::strdup(devname);
	} else {
		CL_PANIC(os::strcmp(pathname, devname) == 0);
	}

	uint64_t	fi_ret;
	size_t		ret_size = 0;

	qhandle = new int;

	*qhandle = open((const char *) devname, O_RDONLY);

	if (*qhandle < 0) {
		os::printf("UNODE: quorum_open: open('%s') failed with "
		    "errno = %d.\n", devname, errno);
		return (quorum::QD_EIO);
	}

	if (quorum_door_call(qhandle, QD_SCSI_OPEN, NULL, (size_t)0, NULL,
	    ret_size, 0ULL, fi_ret) != QD_OK) {
		CMM_TRACE(("Returning EIO from quorum_open\n"));
		return (quorum::QD_EIO);
	}

	//
	// If the scrub boolean is set to true, this is the first time we have
	// accessed a new quorum device, and we need to scrub it.
	//
	if (scrub) {
		//
		// This is a no-op here since scconf does not work in unode.
		// Also, when qd comes up, it does the scrubbing automatically.
		//
	}

	//
	// Because we are also going to be using PGRe for key
	// management, we need to initialize the PGRe variables
	// on storage. If this call fails, but the initialize
	// reserved call succeeded, then we know there is a real
	// problem, and we won't retry the access.
	//
	int error = quorum_unode_get_sblkno();

	if (error != 0) {
		CMM_TRACE(("UNODE(%s) quorum_open: Failed to initialize"
		    "  pgre data.\n", pathname));
		return (quorum::QD_EIO);
	}
	return (quorum::QD_SUCCESS);
#endif
}

//
// quorum_close
//
// Close the vnode off and release the pointer.
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
quorum_device_unode_impl::quorum_close(Environment &)
{
#ifndef linux
	if (qhandle != NULL) {
		if (*qhandle >= 0) {
			(void) close(*(int *)qhandle);
		}
		free(qhandle);
		qhandle = NULL;
	}
#endif
	return (quorum::QD_SUCCESS);
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster. The quorum_amnesia_level enum
// is defined in quorum_impl.h
//
uint32_t
quorum_device_unode_impl::quorum_supports_amnesia_prot(Environment &)
{
	return (AMN_SC30_SUPPORT);
}

//
// quorum_reserve
//
// Place this node's reservation key on the device.
//

quorum::quorum_error_t
quorum_device_unode_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	uint64_t	fi_ret;
	size_t		ret_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	return (quorum_door_call(qhandle, QD_SCSI3_RESERVE,
	    (char *)&my_key, sizeof (my_key),
	    NULL, ret_size, 0ULL, fi_ret));
	CMM_TRACE(("UNODE(%s): quorum_reserve wrote reservation 0x%llx to "
	    "device.\n", pathname, idl_key_to_int64_key(&my_key)));
	return (quorum::QD_SUCCESS);
#endif
}

//
// quorum_read_reservations
//
// Read all of the reservations on the device. Currently this is
// hard coded to only return a single reservation.
//

quorum::quorum_error_t
quorum_device_unode_impl::quorum_read_reservations(
    quorum::reservation_list_t		&resv_list,
    Environment			&)
{
	CMM_TRACE(("UNODE(%s): quorum_read_reservations\n", pathname));
#ifdef linux
	return (quorum::QD_EIO);
#else

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	if (this->check_fi("quorum_read_reservations") != 0) {
		return (quorum::QD_EIO);
	}
#endif // FAULT_CMM && _KERNEL_ORB

	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	size_t		ret_size;
	uint64_t	fi_ret;

	ASSERT(qhandle != NULL);

	// Convert the idl format list to an mhioc format.
	mhioc_resv_desc_list_t	resvlist;
	(void) convert_to_mhioc_list(resvlist, resv_list);

	uint64_t max_resvs = resvlist.listsize;
	ret_size = (size_t)max_resvs * sizeof (mhioc_resv_desc_t) +
		sizeof (uint64_t);
	char *out_args = new char[ret_size];

	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI3_READRESV,
	    (char *)&max_resvs, sizeof (uint64_t), out_args, ret_size,
	    0ULL, fi_ret);

	if (ret == QD_OK) {
		uint64_t num_resvs;
		bcopy(out_args, &num_resvs, sizeof (uint64_t));
		resvlist.listlen = (uint32_t)num_resvs;
		bcopy(out_args + sizeof (uint64_t), resvlist.list,
		    ret_size - sizeof (uint64_t));
	} else {
		resvlist.listlen = 0;
	}

	os::printf("quorum_readresvs returns %d reservations:\n",
	    resvlist.listlen);
	for (uint_t i = 0; i < resvlist.listlen; i++) {
		os::printf("resv[%d]: key=0x%llx, type=%d, scope=%d, "
		    "scope_addr=0x%lx\n", i,
		    mhioc_key_to_int64_key(*((mhioc_resv_key_t *)
			&resvlist.list[i].key)),
		    resvlist.list[i].type, resvlist.list[i].scope,
		    resvlist.list[i].scope_specific_addr);
	}

	// Convert list back to idl format.
	(void) convert_to_idl_resv_list(resvlist, resv_list);
	delete [] resvlist.list;

	delete [] out_args;
#endif
	return (quorum::QD_SUCCESS);
}

//
// quorum_preempt
//
// Remove the victim key from the device and place a reservation using
// my_key. If victim_key is NULL, that means that we need to release any
// mutual exclusion that we hold on the device. In that case, call
// release.
//

quorum::quorum_error_t
quorum_device_unode_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	uint64_t	fi_ret;
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	int		error;
	size_t		ret_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	//
	// If victim_key is NULL, issue a release.
	// Intuitively this corresponds to the case when we are not preempting
	// anyone and therefore everyone should have access to the device.
	//
	if (null_idl_key(&victim_key)) {
		CMM_TRACE(("UNODE(%s): quorum_preempt called with NULL victim "
		    "key, calling release.\n", pathname));
		error = this->unode_release();

		if (error == EACCES) {
			CMM_TRACE(("UNODE(%s): quorum_preempt - unode_release "
			    "returned error %d\n", pathname, error));
			return (quorum::QD_EACCES);
		} else if (error != 0) {
			CMM_TRACE(("UNODE(%s): quorum_preempt - unode_release "
			    "returned error %d\n", pathname, error));
			return (quorum::QD_EIO);
		}

		// Write out our key in the resv area.
		ret = quorum_door_call(qhandle, QD_SCSI3_RESERVE,
		    (char *)&my_key, sizeof (my_key),
		    NULL, ret_size, 0ULL, fi_ret);

		//
		// If victim_key was NULL, we just needed to do the release,
		// so go ahead and return now.
		//
		return (ret);
	}

	mhioc_resv_key_t	keys[2];

	scsi_pgr_util::copy(keys[0], *((mhioc_resv_key_t *)&my_key));
	scsi_pgr_util::copy(keys[1], *((mhioc_resv_key_t *)&victim_key));

	ret = quorum_door_call(qhandle, QD_SCSI3_PREEMPT,
	    (char *)keys, sizeof (keys), NULL, ret_size, 0ULL, fi_ret);

	return (ret);
#endif
}

//
// quorum_register
//
// Write this node's key on the device as a registration.
//

quorum::quorum_error_t
quorum_device_unode_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	uint64_t	fi_ret;
	size_t		ret_size = 0;

	CMM_TRACE(("UNODE(%s): quorum_register.\n", pathname));

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	return (quorum_door_call(qhandle, QD_SCSI3_REGISTER,
	    (char *)&my_key, sizeof (my_key), NULL, ret_size, 0ULL, fi_ret));
#endif
}

//
// quorum_read_keys
//
// Read the registration keys on the device. Fill in the reg_list up to a
// maximum of reg_list->listsize keys. Set reg_list->listlen to the number
// of total keys that are on the device.
//

quorum::quorum_error_t
quorum_device_unode_impl::quorum_read_keys(
    quorum::reservation_list_t		&reg_list,
    Environment			&)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t		ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		ret_size;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	mhioc_key_list_t	keylist;
	(void) convert_to_mhioc_reg_list(keylist, reg_list);

	uint64_t max_keys = keylist.listsize;

	// ret: (number of keys + the actual keys)
	ret_size = (size_t)max_keys * sizeof (mhioc_resv_key_t) +
		sizeof (uint64_t);
	char *out_args = new char[ret_size];

	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI3_READKEYS,
		(char *)&max_keys, sizeof (max_keys), out_args, ret_size,
		0ULL, fi_ret);

	if (ret == QD_OK) {
		uint64_t num_keys;
		bcopy(out_args, &num_keys, sizeof (uint64_t));
		keylist.listlen = (uint32_t)num_keys;
		bcopy(out_args + sizeof (uint64_t), keylist.list,
			ret_size - sizeof (uint64_t));
	} else {
		keylist.listlen = 0;
	}

	os::printf("quorum_readkeys returns %d keys:\n", keylist.listlen);
	for (uint_t i = 0; i < keylist.listlen; i++) {
		os::printf("key[%d]=0x%llx\n", i,
			mhioc_key_to_int64_key(keylist.list[i]));
	}

	// Convert the list back to idl format.
	(void) convert_to_idl_reg_list(keylist, reg_list);
	delete [] keylist.list;

	delete [] out_args;

	return (ret);
#endif
}

//
// quorum_remove
//
// Remove the device.
//
quorum::quorum_error_t
quorum_device_unode_impl::quorum_remove(const char *devname,
    const quorum::qd_property_seq_t &,
    Environment			&)
{
	CMM_TRACE(("UNODE(%s): quorum_remove\n", devname));

	return (quorum::QD_SUCCESS);
}

//
// quorum_enable_failfast
//
// Enable sd failfast on the device.
//
quorum::quorum_error_t
quorum_device_unode_impl::quorum_enable_failfast(
    Environment			&)
{
	if (failfast_enabled) {
		// Failfast already enabled. Simply return success.
		return (quorum::QD_SUCCESS);
	}

	CMM_TRACE(("UNODE(%s): quorum_enable_failfast\n", pathname));
	failfast_enabled = true;
	return (quorum::QD_SUCCESS);
}

quorum::quorum_error_t
quorum_device_unode_impl::quorum_reset(
    Environment			&)
{
	CMM_TRACE(("UNODE(%s): quorum_reset_bus called.\n", pathname));

	return (quorum::QD_SUCCESS);
}

quorum::quorum_error_t
quorum_device_unode_impl::quorum_data_read(
	uint64_t,
	CORBA::String_out,
	Environment		&)
{
	return (quorum::QD_SUCCESS);
}

quorum::quorum_error_t
quorum_device_unode_impl::quorum_data_write(
	uint64_t,
	const char	*,
	Environment		&)
{
	return (quorum::QD_SUCCESS);
}

int
quorum_device_unode_impl::quorum_unode_get_sblkno()
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t 		ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		out_size;
	char		*out_args;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	// Although the sblkno is always going to be 0 in unode,
	// need to make a door call to simulate it, since this
	// disk access may fail if the other node has not issued
	// a MHIOCRELEASE ioctl yet.
	out_size = sizeof (uint64_t);
	out_args = new char[out_size];
	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI2_GET_SBLKNO, NULL,
	    (size_t)0, out_args, out_size, 0ULL, fi_ret);
	if (ret == quorum::QD_SUCCESS) {
		bcopy(out_args, &sblkno, out_size);
	}

	delete [] out_args;
	return (ret);
#endif
}


int
quorum_scsi2_key_write(
    int 		*qhandle,
    uint64_t 		sblkno,
    mhioc_resv_key_t 	*key,
    bool)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t		ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		in_size;
	char		*in_args;
	size_t		out_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	in_size = sizeof (uint64_t) + sizeof (mhioc_resv_key_t);
	in_args = new char[in_size];
	ASSERT(in_args != NULL);

	//lint -e669
	bcopy(&sblkno, in_args, sizeof (uint64_t));
	bcopy(key, in_args + sizeof (uint64_t), sizeof (mhioc_resv_key_t));

	ret = quorum_door_call(qhandle, QD_SCSI2_KEY_WRITE, in_args,
	    in_size, NULL, out_size, 0ULL, fi_ret);

	delete [] in_args;
	return (ret);
#endif
}


int
quorum_scsi2_key_read(
    int 		*qhandle,
    uint64_t 		sblkno,
    mhioc_resv_key_t 	*key)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		out_size;
	char		*out_args;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	out_size = sizeof (mhioc_resv_key_t);
	out_args = new char[out_size];
	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI2_KEY_READ, (char *)&sblkno,
	    sizeof (sblkno), out_args, out_size, 0ULL, fi_ret);

	if (ret == QD_OK) {
		bcopy(out_args, key, sizeof (*key));
	}

	delete [] out_args;
	return (quorum::QD_SUCCESS);
#endif
}


int
quorum_scsi2_key_find(
    int 		*qhandle,
    uint64_t		sblkno,
    mhioc_resv_key_t	*key,
    uint64_t		&key_sbn)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		in_size, out_size;
	char		*in_args, *out_args;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	in_size = sizeof (uint64_t) + sizeof (mhioc_resv_key_t);
	in_args = new char[in_size];
	ASSERT(in_args != NULL);
	bcopy(&sblkno, in_args, sizeof (uint64_t));
	bcopy(key, in_args + sizeof (uint64_t), sizeof (mhioc_resv_key_t));

	out_size = sizeof (uint64_t);
	out_args = new char[out_size];
	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI2_KEY_FIND, in_args,
	    in_size, out_args, out_size, 0ULL, fi_ret);

	if (ret == QD_OK) {
		bcopy(out_args, &key_sbn, sizeof (uint64_t));
	}

	if (ret == QD_KEY_NOT_REGISTERED) {
		// Change the ret to ENOENT, which the caller will check for.
		ret = quorum::QD_EACCES;
	}

	delete [] out_args;
	return (ret);
#endif
}


int
quorum_scsi2_key_delete(
    int 	*qhandle,
    uint64_t 	sblkno,
    bool)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	uint64_t	fi_ret;
	size_t		out_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	return (quorum_door_call(qhandle, QD_SCSI2_KEY_DELETE, (char *)&sblkno,
	    sizeof (sblkno), NULL, out_size, 0ULL, fi_ret));
#endif
}


int
quorum_scsi2_choosing_write(
    int 	*qhandle,
    uint64_t 	sblkno,
    bool 	cval)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		in_size;
	char		*in_args;
	size_t		out_size = 0;
	uint64_t	cval64 = cval;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	in_size = 2 * sizeof (uint64_t);
	in_args = new char[in_size];
	ASSERT(in_args != NULL);

	bcopy(&sblkno, in_args, sizeof (uint64_t));
	bcopy(&cval64, in_args + sizeof (uint64_t), sizeof (uint64_t));

	ret = quorum_door_call(qhandle, QD_SCSI2_CHOOSING_WRITE, in_args,
	    in_size, NULL, out_size, 0ULL, fi_ret);

	delete [] in_args;
	return (ret);
#endif
}


int
quorum_scsi2_choosing_read(
    int	*qhandle,
    uint64_t	sblkno,
    bool	&cval)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		out_size;
	char		*out_args;
	uint64_t	cval64;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	out_size = sizeof (uint64_t);
	out_args = new char[out_size];
	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI2_CHOOSING_READ,
	    (char *)&sblkno, sizeof (sblkno), out_args, out_size,
	    0ULL, fi_ret);

	if (ret == QD_OK) {
		bcopy(out_args, &cval64, out_size);
		cval = (bool) cval64;
	}

	delete [] out_args;
	return (quorum::QD_SUCCESS);
#endif
}


int
quorum_scsi2_number_write(
    int 	*qhandle,
    uint64_t 	sblkno,
    uint64_t 	nval)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		in_size;
	char		*in_args;
	size_t		out_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	in_size = 2 * sizeof (uint64_t);
	in_args = new char[in_size];
	ASSERT(in_args != NULL);

	bcopy(&sblkno, in_args, sizeof (uint64_t));
	bcopy(&nval, in_args + sizeof (uint64_t), sizeof (uint64_t));

	ret = quorum_door_call(qhandle, QD_SCSI2_NUMBER_WRITE, in_args,
	    in_size, NULL, out_size, 0ULL, fi_ret);

	delete [] in_args;
	return (ret);
#endif
}


int
quorum_scsi2_number_read(
    int 	*qhandle,
    uint64_t 	sblkno,
    uint64_t 	&nval)
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	quorum::quorum_error_t	ret = quorum::QD_SUCCESS;
	uint64_t	fi_ret;
	size_t		out_size;
	char		*out_args;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	out_size = sizeof (uint64_t);
	out_args = new char[out_size];
	ASSERT(out_args != NULL);

	ret = quorum_door_call(qhandle, QD_SCSI2_NUMBER_READ,
	    (char *)&sblkno, sizeof (sblkno), out_args, out_size,
	    0ULL, fi_ret);

	if (ret == QD_OK) {
		bcopy(out_args, &nval, out_size);
	}

	delete [] out_args;
	return (quorum::QD_SUCCESS);
#endif
}

int
quorum_device_unode_impl::unode_tkown()
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	uint64_t	fi_ret;
	size_t		ret_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	return (quorum_door_call(qhandle, QD_SCSI2_TKOWN, NULL, (size_t)0, NULL,
	    ret_size, 0Ull, fi_ret));
#endif
}


int
quorum_device_unode_impl::unode_release()
{
#ifdef linux
	return (quorum::QD_EIO);
#else
	uint64_t	fi_ret;
	size_t		ret_size = 0;

	// qhandle can not be NULL.
	ASSERT(qhandle != NULL);

	return (quorum_door_call(qhandle, QD_SCSI2_RELEASE, NULL, (size_t)0,
	    NULL, ret_size, 0Ull, fi_ret));
#endif
}
