/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSI3_QD_H
#define	_SCSI3_QD_H

#pragma ident	"@(#)scsi3_qd.h	1.14	09/02/01 SMI"

/* Interfaces to quorum device implementation of SCSI-3 devices. */

#include <orb/object/adapter.h>
#include <h/quorum.h>
#include <quorum/common/shared_disk_util.h>
#include <cmm/cmm_debug.h>

class quorum_device_scsi3_impl :
	public McServerof <quorum::quorum_device_type> {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//

	virtual quorum::quorum_error_t		quorum_open(
	    const char				*devname,
	    const bool				scrub,
	    const quorum::qd_property_seq_t	&,
	    Environment				&);

	virtual quorum::quorum_error_t quorum_close(Environment &);

	virtual uint32_t quorum_supports_amnesia_prot(Environment&);

	virtual quorum::quorum_error_t		quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_reservations(
	    quorum::reservation_list_t		&resv_list,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&victim_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_keys(
	    quorum::reservation_list_t		&reg_list,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_read(
		uint64_t		location,
		CORBA::String_out	data,
		Environment		&);

	virtual quorum::quorum_error_t	quorum_data_write(
		uint64_t		location,
		const char		*data,
		Environment		&);

	virtual quorum::quorum_error_t		quorum_remove(
	    const char				*devname,
	    const quorum::qd_property_seq_t 	&qd_props,
	    Environment				&);

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

	quorum_device_scsi3_impl(cmm_trace_level);
	quorum_device_scsi3_impl();
	~quorum_device_scsi3_impl();

private:
	int internal_read_keys(mhioc_key_list_t *keylist);

	int internal_preemptandabort_key(mhioc_resv_key_t my_key,
	    mhioc_resv_key_t victim_key);

	// Scrub all keys on the disk
	int internal_scrub_keys();

	// Disable SCSI failfast
	void	disable_failfast();

	// Pointer to the physical disk. Used in ioctls.
	vnode_t		*vnode_ptr;

	// Local copy of the name of the device.
	char		*pathname;

	cmm_trace_level trace_level;

	// Function for fault injection testing.
	int	check_fi(char*);

	// True if failfast has been enabled
	bool	failfast_enabled;
};

#endif	/* _SCSI3_QD_H */
