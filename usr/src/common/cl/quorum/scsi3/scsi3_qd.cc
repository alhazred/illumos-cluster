//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scsi3_qd.cc	1.23	09/04/29 SMI"

#include <quorum/common/type_registry.h>
#include <quorum/scsi3/scsi3_qd.h>
#include <cmm/cmm_debug.h>
#include <orb/infrastructure/orb_conf.h>
#include <quorum/common/quorum_impl.h>

// Included for file ioctls.
#include <sys/vnode.h>
#include <sys/file.h>

// Included for cplpl_init
#include <cplplrt/cplplrt.h>

#include <quorum/common/quorum_util.h>

// Poll period for failfast thread
#define	SCSI3_POLL_PERIOD	INT_MAX	// no need to poll for SCSI-3

//
// get_scsi3_qd_impl is the access point for the type_registry to modlookup
// in the scsi3 module.
//
static quorum::quorum_device_type_ptr
get_scsi3_qd_impl(cmm_trace_level level)
{
	CMM_SELECTIVE_TRACE(level, CMM_GREEN,
	    ("clq_scsi3: Returning new quorum device type.\n"));
	return ((new quorum_device_scsi3_impl(level))->get_objref());
}

//
// Constructor
//
quorum_device_scsi3_impl::quorum_device_scsi3_impl(
    cmm_trace_level level)
{
	vnode_ptr = NULL;
	pathname = NULL;
	failfast_enabled = false;
	trace_level = level;
}

//
// Default constructor.
// Should not be used.
//
// Lint warns that some class members are not initialized by constructor.
//lint -e1401
quorum_device_scsi3_impl::quorum_device_scsi3_impl()
{
	ASSERT(0);
}
//lint +e1401

//
// Destructor
//
quorum_device_scsi3_impl::~quorum_device_scsi3_impl()
{
	CL_PANIC(vnode_ptr == NULL);

	delete [] pathname;
	pathname = NULL;
}

//
// Unreferenced
//
void
#ifdef DEBUG
quorum_device_scsi3_impl::_unreferenced(unref_t cookie)
#else
quorum_device_scsi3_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("quorum_device_scsi3_impl(%s)::_unreferenced.\n", pathname));

	//
	// Quorum objects are instantiated in the kernel
	// while the callers instantiating those objects can reside
	// in kernel or userland. If a userland client dies
	// after calling quorum_open() but before calling quorum_close(),
	// then the sole reference held by the client for the quorum
	// object is gone without a call to quorum_close().
	//
	// So we call quorum_close() here to clean-up state.
	// quorum_close() is implemented to be idempotent
	// and hence it is safe to call quorum_close() here.
	//
	// This object is going away; so ignore return value
	// and exception from quorum_close().
	//
	Environment e;
	(void) quorum_close(e);
	e.clear();

	delete this;
}

//
// Disable SCSI failfast
//
void
quorum_device_scsi3_impl::disable_failfast()
{
	int error = 0;

	if (!failfast_enabled) {
		// Failfast already disabled.
		return;
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) disable_failfast\n", pathname));

	ASSERT(vnode_ptr != NULL);

	// Passing zero as the polling period disables failfast
	error = quorum_scsi_enable_failfast(vnode_ptr, 0);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) disable_failfast failed with error %d.\n",
		    pathname, error));
		return;
	}

	failfast_enabled = false;
}

//
// IDL method to set the message tracing level of this quorum device
//
void
quorum_device_scsi3_impl::set_message_tracing_level(
    uint32_t new_trace_level, Environment &)
{
	trace_level = (cmm_trace_level)new_trace_level;
}

//
// IDL method to get the message tracing level of this quorum device
//
uint32_t
quorum_device_scsi3_impl::get_message_tracing_level(Environment &)
{
	return ((uint32_t)trace_level);
}

//
// quorum_open
//
// Opens a quorum device by a local path name and gets a handle
// of type vnode_t *. This handle is used in all other calls.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_open(const char *devname, const bool scrub,
    const quorum::qd_property_seq_t &, Environment &)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s)::quorum_open.\n", devname));

	int error = 0;

	const int	max_retries = 5;	// # of times to retry vn_open.
						// Perhaps this should be made
						// settable through /etc/system.
	int		retries_remaining;

	//
	// A particular quorum device object is associated with
	// a single configured quorum device always.
	// It is possible that an earlier quorum_open attempt
	// has initialized the pathname.
	// If so, we do not need to duplicate the string again.
	//
	if (pathname == NULL) {
		// First open attempt
		pathname = os::strdup(devname);
	} else {
		CL_PANIC(os::strcmp(pathname, devname) == 0);
	}

	//
	// TEMPORARY: The following retry code is added to work
	// around a bug in the ssd driver or the driver framework
	// which results in a small window of time right after the
	// root file system is mounted during which vn_open may
	// return an ENXIO error. If we get this error, retry a
	// few times.
	//
	// It turns out that the vn_open code could fail so early.
	// We do not trust the driver code to return correct error codes.
	// So we retry a number of times in case of failure.
	// In case the error returned is EACCES, then we do not retry
	// and return QD_EACCES which means we will retry
	// after reconfiguration completes.
	// However, if the vn_open keeps on returning other errors
	// even after a few retries, then :
	// (i) if error is EACCES, we return QD_EACCES which means
	// we will retry after reconfiguration completes,
	// (ii) if any other error, we return QD_EIO which means
	// the device reference is released.
	//
	// Use FNDELAY flag for filemode (3rd arg) below so that vn_open
	// succeeds even if the disk is reserved by a SCSI2 reservation.
	//
	retries_remaining = max_retries;
	do {
		error = vn_open((char *)devname, UIO_SYSSPACE,
		    (FREAD | FNDELAY), 0, &vnode_ptr, CRCREAT, 0);
		retries_remaining--;
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s) quorum_open - vn_open returned "
			    "error %d\n", devname, error));
		}
		if ((error == 0) || (error == EACCES)) {
			break;
		}
		os::usecsleep((os::usec_t)MICROSEC); // sleep 1 second
	} while (retries_remaining);
	if (error) {
		vnode_ptr = NULL;
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_open - vn_open failed "
		    "after %d trials\n", devname,
		    max_retries - retries_remaining));
		if (error == EACCES) {
			return (quorum::QD_EACCES);
		} else {
			return (quorum::QD_EIO);
		}
	} else if (retries_remaining != max_retries) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s): quorum_open - vn_open() returned 0 "
		    "after %d trials\n", devname,
		    max_retries - retries_remaining));
	}

	//
	// If the scrub boolean is set to true, this is the first time we have
	// accessed a new quorum device, and we need to scrub it.
	//
	if (scrub) {
		error = internal_scrub_keys();
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s): quorum_open got error %d trying "
			    "to scrub device.\n", pathname, error));
			if (error == EACCES) {
				return (quorum::QD_EACCES);
			} else {
				return (quorum::QD_EIO);
			}
		}
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_close
//
// Close the vnode off and release the pointer.
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_close(Environment &)
{
	if (vnode_ptr != NULL) {
		if (failfast_enabled) {
			disable_failfast();
		}

#if	SOL_VERSION >= __s11
		(void) VOP_CLOSE(vnode_ptr, FREAD, 1, (offset_t)0, kcred, NULL);
#else
		(void) VOP_CLOSE(vnode_ptr, FREAD, 1, (offset_t)0, kcred);
#endif
		VN_RELE(vnode_ptr);
		vnode_ptr = NULL;
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster. The quorum_amnesia_level enum
// is defined in quorum_impl.h
//
uint32_t
quorum_device_scsi3_impl::quorum_supports_amnesia_prot(Environment &)
{
	return (AMN_SC30_SUPPORT);
}

//
// Makes a group reservation on a quorum device. This reservation is
// checked by the device when an actual IO operation is issued to the
// device by a volume manager. The quorum algorithm does not issue
// any IO operations on the quorum device. The ioctl is retried only
// if there is no reservation on the device. Returns success even if the
// ioctl fails, if a reservation appears on the device. This is done to
// handle the multipathing case in which the RESERVE is placed on the
// disk, but we get back a failure for the ioctl. (perhaps a path
// failed at just the right time). If the mp driver has failed to an
// alternate path, the retrying the RESERVE ioctl should fail.
//
// For the unode implementation, this routine can be a no-op until we
// start using unode to simulate physical disks for volume managers.
// However, the routine should check if a registration with the
// key has been made already.
//
// Returns 0 on success and EACCES if "my_key" has not been registered.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	mhioc_resv_desc_t	resv_desc;
	int			error = 0;
	int			read_error = 0;
	int			retval = 0;
	quorum::quorum_error_t	err_ret = quorum::QD_EACCES;

	char nodename[8];
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	quorum::reservation_list_t	resvlist;
	resvlist.keylist.length(1);
	resvlist.listsize = 1;

	mhioc_resv_key_t	*my_mhioc_key;
	my_mhioc_key = (mhioc_resv_key_t *)(&my_key);

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	// Make sure a zero key is not being written

	uint64_t	zero_key = 0x0;
	quorum::registration_key_t	zero_reg_key;

	int64_key_to_idl_key(zero_key, &zero_reg_key);
	CL_PANIC(!match_idl_keys(&zero_reg_key, &my_key));

	resv_desc.key = *my_mhioc_key;
	resv_desc.type = SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	resv_desc.scope = SCSI3_SCOPE_LOGICALUNIT;
	resv_desc.scope_specific_addr = 0;

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_RESERVE,
	    (intptr_t)&resv_desc, &retval);

	//
	// Do the read key check if there is no error
	// or any error other than EACCESS.
	//
	if (error != EACCES) {
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s): quorum_reserve: ioctl "
			    "MHIOCGRP_RESERVE failed with error %d."
			    "Checking to see if key was written "
			    "anyway.\n", pathname, error));
		}
		Environment e;
		// If the device has this node's reservation, return success.
		read_error = this->quorum_read_reservations(resvlist, e);
		if (e.exception()) {
			// This is local invocation, so ignore exceptions.
			e.clear();
		}
		//
		// Look through the list, looking for my_key.
		// Right now, the quorum code always assumes a single
		// reservation, so just check the 0 entry.
		//
		if ((!read_error) && (resvlist.listlen > 0)) {
			// We expect at most one reservation key
			ASSERT(!(resvlist.listlen > 1));

			if (match_idl_keys(&(resvlist.keylist[0]), &my_key)) {
				if (error) {

					os::sc_syslog_msg msgobj(
					    SC_SYSLOG_CMM_QUORUM_TAG, nodename,
					    NULL);
					//
					// SCMSGS
					// @explanation
					// The ioctl to place node's reservation
					// key on the specified quorum device
					// returned failure. This is unexpected
					// behaviour from the device driver,
					// multipathing driver or device
					// firmware. Cluster software read the
					// reservation key and found that node's
					// key is present on the device.
					// @user_action
					// No user action is required since
					// the read check has succeeded.

					(void) msgobj.log(SC_SYSLOG_WARNING,
					    MESSAGE,
					    "clq_scsi3: Ioctl MHIOCGRP_RESERVE "
					    "returned failure (error = %d) but "
					    "reservation key was found on"
					    "quorum device %s", error,
					    pathname);

					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_AMBER,
					    ("SCSI3(%s): quorum_reserve: "
					    "reserve ioctl returned error(%d), "
					    "but reservation was present. "
					    "Returning success.\n", pathname,
					    error));
				} else {
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_GREEN,
					    ("SCSI3(%s): quorum_reserve: "
					    "placed reservation key "
					    "successfuly.\n ",
					    pathname));
				}
				err_ret = quorum::QD_SUCCESS;
			} else {
				if (error == 0) {
					//
					// Ioctl returned success but the
					// reservation key is not present
					//
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("SCSI3(%s): quorum_reserve: "
					    "reserve ioctl returned success "
					    "but reservation was not present."
					    "Returning failure.\n", pathname));

					os::sc_syslog_msg msgobj(
					    SC_SYSLOG_CMM_QUORUM_TAG, nodename,
					    NULL);
					//
					// SCMSGS
					// @explanation
					// An error was encountered while trying
					// to place node's reservation key on
					// specified quorum device. This error
					// occurs because of unexpected
					// behaviour from the device driver or
					// multi-pathing driver or device's
					// firmware.
					// @user_action
					// Please look for updates in device
					// driver, multi-pathing driver or
					// device firmware. If no updates
					// are available please contact Sun
					// device driver support team.
					(void) msgobj.log(SC_SYSLOG_WARNING,
					    MESSAGE,
					    "clq_scsi3: Ioctl MHIOCGRP_RESERVE "
					    "returned success but the"
					    "reservation key was not found on "
					    "quorum device %s", pathname);

					ASSERT(0);

				} else {
					os::sc_syslog_msg msgobj(
					    SC_SYSLOG_CMM_QUORUM_TAG, nodename,
					    NULL);
					//
					// SCMSGS
					// @explanation
					// An error was encountered while trying
					// to place node's reservation key on
					// specified quorum device. Cluster
					// software read the reservation key
					// and found another node's key present.
					// This error
					// occurs because of unexpected
					// behaviour from the device driver or
					// multi-pathing driver or device's
					// firmware.
					// @user_action
					// Please look for updates in device
					// driver, multi-pathing driver or
					// device firmware. If no updates
					// are available please contact Sun
					// device driver support team.
					(void) msgobj.log(SC_SYSLOG_WARNING,
					    MESSAGE,
					    "clq_scsi3: Ioctl MHIOCGRP_RESERVE "
					    "returned error(%d) also the"
					    "reservation key was not found on "
					    "quorum device %s", error,
					    pathname);

					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("SCSI3(%s) quorum_reserve: "
					    "failed to place reservation - "
					    "errno %d.\n",
					    pathname, error));
				}
				err_ret = quorum::QD_EIO;
			}
		}
		if (resvlist.listlen > 1) {
			//
			// We always expect only one reservation key to be
			// present on the device. More than one reservation
			// key present on the device is unexpected and is
			// reported in syslog.
			//


			os::sc_syslog_msg msgobj(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// More than one reservation key was found
			// on the specified quorum device. This
			// occurs because of unexpected
			// behaviour from the device driver or
			// multi-pathing driver or device's
			// firmware.
			// @user_action
			// Please look for updates in device
			// driver, multi-pathing driver or
			// device firmware. If no updates
			// are available please contact Sun
			// device driver support team.
			(void) msgobj.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "clq_scsi3: More than one "
			    "reservation key was found on"
			    "quorum device %s", pathname);

			CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
			    ("SCSI3(%s) quorum_reserve: More than "
			    "one reservation key found on "
			    " the specified quorum device.\n", pathname));
		}
		if (err_ret != quorum::QD_EACCES) {
			return (err_ret);
		}
	}


	CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
	    ("SCSI3(%s) quorum_reserve: Attempt to place "
	    "reservation failed with EACCES.\n", pathname));
	return (quorum::QD_EACCES);
}

//
// Fills in "resv_list" with the reservations currently stored on a
// quorum device.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_read_reservations(
    quorum::reservation_list_t	&resv_list,
    Environment			&)
{
	int			error;
	int			retval = 0;
	mhioc_inresvs_t		inresvs;
	quorum::quorum_error_t quorum_error = quorum::QD_SUCCESS;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_read_reservations called\n", pathname));

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	if (this->check_fi("quorum_read_reservations") != 0) {
		return (quorum::QD_EIO);
	}
#endif // FAULT_CMM && _KERNEL_ORB

	mhioc_resv_desc_list_t	resvlist;
	(void) convert_to_mhioc_list(resvlist, resv_list);

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	inresvs.li = &resvlist;

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_INRESV,
	    (intptr_t)&inresvs, &retval);

	while (resvlist.listlen > resvlist.listsize) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s) quorum_read_reservations: "
		    "MHIOCGRP_INRESV returned listlen > listsize. "
		    "Retrying ioctl with resized cache.\n", pathname));
		delete [] resvlist.list;
		resvlist.listsize = resvlist.listlen;
		resvlist.list = new mhioc_resv_desc_t[resvlist.listlen];
		resvlist.listlen = 0;

		// Retry the ioctl.
		error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_INRESV,
		    (intptr_t)&inresvs, &retval);
	}

	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_read_reservations: ioctl "
		    "MHIOCGRP_INRESV failed with error %d. \n",
		    pathname, error, RETRY_SLEEP));

		delete [] resvlist.list;
		if (error == EACCES) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s) quorum_read_reservations got "
			    "EACCES.\n", pathname));
			quorum_error = quorum::QD_EACCES;
		} else {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s) quorum_read_reservations "
			    "got error %d.\n", pathname));
			quorum_error = quorum::QD_EIO;
		}
		char nodename[8];
		os::sprintf(nodename, "%d", orb_conf::local_nodeid());
		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// An error was encountered while trying to read reservations
		// on the specified quorum device.
		// @user_action
		// There may be other related messages on this and other nodes
		// connected to this quorum device that may indicate the cause
		// of this problem. Refer to the quorum disk repair section of
		// the administration guide for resolving this problem.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clq_scsi3: Error %d from MHIOCGRP_INRESV --- "
		    "failed to read reservations from quorum device %s.",
		    error, pathname);
		return (quorum_error);
	}

	//
	// Copy the reservations to the idl data structure to return,
	// and then delete the memory that was allocated in
	// convert_to_mhioc_list.
	//
	(void) convert_to_idl_resv_list(resvlist, resv_list);
	delete [] resvlist.list;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3: quorum_read_reservations returned:\n"));
	for (uint_t i = 0; i < resv_list.listlen; i++) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("resv[%d]: key=0x%llx\n", i,
		    idl_key_to_int64_key(&(resv_list.keylist[i]))));
	}

	return (quorum::QD_SUCCESS);

}

//
// Removes a reservation key from a quorum device. This is used to
// preempt the reservation of another node when that node is declared
// down.
//
// If a NULL victim_key is passed in, then we have no work to do, so just
// return.
//
// Returns EACCES if "my_key" is not registered. Otherwise, 0 is
// returned to indicate that "victim_key" has been removed.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&)
{
	int			error = 0;
	int			retval = 0;
	uint_t			i;
	mhioc_preemptandabort_t	preempt;
	mhioc_resv_key_t	*m_my_key, *m_victim_key;

	if (null_idl_key(&victim_key)) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("SCSI3(%s): quorum_preempt called with NULL victim "
		    "key, ignoring.\n", pathname));
		return (quorum::QD_SUCCESS);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_preempt called.\n", pathname));

	m_my_key = (mhioc_resv_key_t *)(&my_key);
	m_victim_key = (mhioc_resv_key_t *)(&victim_key);

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	preempt.resvdesc.key = *m_my_key;
	preempt.victim_key = *m_victim_key;

	preempt.resvdesc.type =
		SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	preempt.resvdesc.scope = SCSI3_SCOPE_LOGICALUNIT;
	preempt.resvdesc.scope_specific_addr = 0;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("Preempting victim_key %llx for my key %llx\n",
	    mhioc_key_to_int64_key(*m_victim_key),
	    mhioc_key_to_int64_key(*m_my_key)));

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_PREEMPTANDABORT,
	    (intptr_t)&preempt, &retval);

	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_preempt: ioctl "
		    "MHIOCGRP_PREEMPTANDABORT failed with error %d.\n",
		    pathname, error));
	}

	//
	// If the victim's key is not on the device, (some
	// implementations of) the firmware returns a reservation
	// conflict (EACCES). This could happen due to a retry
	// if the original command succeeded in removing the key.
	// Fiber channel LIPs (Loop Initialization Primitives)
	// resulting from node reboots can cause the original
	// command to time out and the retry to get the reservation
	// conflict error.
	//
	// Therefore, if we get an EACCES error, check if the
	// command actually worked. If so, return success.
	//
	if (error == EACCES) {
		mhioc_inkeys_t		inkeys;
		mhioc_key_list_t	keylist;
		int			found_my_key = 0;
		int			found_victim_key = 0;
		int			new_error;

		inkeys.li = (mhioc_key_list_t *)&keylist;
		keylist.listsize = NODEID_MAX;
		keylist.list = new mhioc_resv_key_t[keylist.listsize];

		new_error = quorum_ioctl_with_retries(vnode_ptr,
		    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);

		while (keylist.listlen > keylist.listsize) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("SCSI3(%s) quorum_preempt: "
			    "MHIOCGRP_INKEYS returned listlen > "
			    "listsize. Retrying ioctl with resized "
			    "list.\n", pathname));
			delete [] keylist.list;
			keylist.listsize = keylist.listlen;
			keylist.list = new mhioc_resv_key_t[keylist.listlen];
			keylist.listlen = 0;

			// Retry inkeys ioctl.
			new_error = quorum_ioctl_with_retries(vnode_ptr,
			    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);
		}

		if (new_error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s) quorum_preempt: ioctl "
			    "MHIOCGRP_INKEYS failed with error %d.\n",
			    pathname, new_error, RETRY_SLEEP));
		} else {
			//
			// Check if my key is present AND victim's key is
			// absent. If not, the preempt has failed.
			//
			for (i = 0; i < keylist.listlen; i++) {
				if (match_mhioc_keys(keylist.list[i],
				    *m_victim_key)) {
					found_victim_key = 1;
					break; // no need to search further
				} else if (match_mhioc_keys(keylist.list[i],
				    *m_my_key)) {
					found_my_key = 1;
				}
			}
		}
		if (found_my_key && !found_victim_key) {
			//
			// The command worked. Ignore the error.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
			    ("SCSI3(%s) quorum_preempt: ioctl "
			    "MHIOCGRP_PREEMPTANDABORT returned EACCES, "
			    "but actually worked; error ignored.\n", pathname));
			error = 0;
		}

		delete [] keylist.list;
	}

	if (error == EACCES) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_preempt got EACCES.\n", pathname));
		return (quorum::QD_EACCES);
	} else if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_preempt got error %d.\n",
		    pathname, error));
		return (quorum::QD_EIO);
	}

	return (quorum::QD_SUCCESS);
}

//
// Associates a reservation key for the node with a quorum device.
// When a node dies, its key can be removed by the surviving cluster
// by using the quorum_preempt command. If the whole cluster is shut
// down or crashes, the keys stay with the quorum device even if the
// quorum device is power cycled.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	mhioc_register_t	register_struct;
	int			error;
	uint_t			i, j;
	int			retval = 0;
	char			nodename[8];
	uint64_t		reg_key_const;
	mhioc_inkeys_t		inkeys;
	mhioc_key_list_t	keylist;
	int			new_error;
	int			found_my_key = 0;
	bool			replaced_my_key = false;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_register called\n", pathname));

	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	// Make sure a zero key is not being written

	uint64_t		zero_key = 0x0;
	quorum::registration_key_t	zero_reg_key;

	int64_key_to_idl_key(zero_key, &zero_reg_key);
	CL_PANIC(!match_idl_keys(&zero_reg_key, &my_key));

	reg_key_const = ((uint64_t)idl_key_to_int64_key(&my_key))
	    & ((uint64_t)0xffffffff00000000LL);

	mhioc_resv_key_t	*m_my_key = (mhioc_resv_key_t *)(&my_key);

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	bzero(register_struct.oldkey.key, (size_t)MHIOC_RESV_KEY_SIZE);

	register_struct.newkey = *m_my_key;
	register_struct.aptpl = B_TRUE; // persist across power failures

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_REGISTER,
	    (intptr_t)&register_struct, &retval);

	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_register: ioctl MHIOCGRP_REGISTER "
		    "failed with error %d.\n", pathname, error));
	}

	//
	// The register command will fail if this node had a key on the
	// device already. Check if this is the case. If so, we can
	// replace the old key with the new key.
	//
	if (error) {

		keylist.listsize = NODEID_MAX;
		keylist.list = new mhioc_resv_key_t[keylist.listsize];
		keylist.listlen = 0;
		inkeys.li = &keylist;

		new_error = quorum_ioctl_with_retries(vnode_ptr,
		    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);

		while (keylist.listlen > keylist.listsize) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("SCSI3(%s) quorum_register: "
			    "MHIOCGRP_INKEYS returned listlen > "
			    "listsize. Retrying ioctl with resized "
			    "list.\n", pathname));
			delete [] keylist.list;
			keylist.listsize = keylist.listlen;
			keylist.list = new mhioc_resv_key_t[keylist.listlen];
			keylist.listlen = 0;

			// Retry the inkeys ioctl
			new_error = quorum_ioctl_with_retries(vnode_ptr,
			    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);
		}

		if (new_error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s) quorum_register: ioctl "
			    "MHIOCGRP_INKEYS failed with error %d. \n",
			    pathname, new_error));
		} else {
			//
			// Check all keys on the quorum device.
			// If we find our key, then we replace that old key
			// with the new one.
			// We need to do this action because if we are using
			// multipathing solution to provide redundant paths to
			// the quorum device, then we should be registering
			// the node's key for all the online host bus adapters
			// or initiators through which this node is connected
			// to the quorum device.
			// It is possible that a host bus adapter or initiator
			// was not working earlier, but is working now;
			// and so we try to register the keys (even though
			// the keys are already present) so that the keys
			// will get registered for all host bus adapters or
			// initiators that are online now.
			//
			for (i = 0; i < keylist.listlen; i++) {

				// Check for zero key

				uint64_t	keylist_key;
				quorum::registration_key_t	keylist_key_idl;

				keylist_key = mhioc_key_to_int64_key(
				    keylist.list[i]);
				int64_key_to_idl_key(keylist_key,
				    &keylist_key_idl);

				if (match_idl_keys(&zero_reg_key,
				    &keylist_key_idl)) {

					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("SCSI3(%s) quorum_register: "
					    "Zero key found on this QD.\n",
					    pathname));

					os::sc_syslog_msg msgobj(
					    SC_SYSLOG_CMM_QUORUM_TAG, nodename,
					    NULL);
					//
					// SCMSGS
					// @explanation
					// One of the registration keys is
					// found to be zero value on
					// specified quorum device. This error
					// occurs because of unexpected
					// behaviour from the device driver or
					// multi-pathing driver or device's
					// firmware.
					// @user_action
					// Unconfigure the quorum device and
					// scrub it and then configure it back
					// as a quorum device.
					// Please also look for updates to
					// the device driver, multi-pathing
					// driver or device firmware.
					// If no updates are available,
					// please contact Sun device driver
					// support team.
					//
					(void) msgobj.log(SC_SYSLOG_WARNING,
					    MESSAGE,
					    "clq_scsi3: One of the key was "
					    "found to be zero value on "
					    "quorum device %s.",
					    pathname);

					// Skip this key
					continue;
				}

				// Check for a valid key value
				if ((keylist_key &
				    (uint64_t)0xffffffff00000000LL)
				    != reg_key_const) {

					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("SCSI3(%s) quorum_register: "
					    "Invalid key 0x%llx found "
					    "on this QD.\n", pathname,
					    keylist_key));

					os::sc_syslog_msg msgobj(
					    SC_SYSLOG_CMM_QUORUM_TAG, nodename,
					    NULL);

					//
					// SCMSGS
					// @explanation
					// One of the registration keys is
					// found to be invalid value on
					// specified quorum device. This error
					// occurs because of unexpected
					// behaviour from the device driver or
					// multi-pathing driver or device's
					// firmware.
					// @user_action
					// Unconfigure the quorum device and
					// scrub it and then configure it back
					// as a quorum device.
					// Please also look for updates to
					// the device driver, multi-pathing
					// driver or device firmware.
					// If no updates are available,
					// please contact Sun device driver
					// support team.
					//
					(void) msgobj.log(SC_SYSLOG_WARNING,
					    MESSAGE,
					    "clq_scsi3: One of the key 0x%llx "
					    "was found to be invalid value on "
					    "quorum device %s.", keylist_key,
					    pathname);

					// Skip this key
					continue;
				}

				//
				// Check if this key on the device is my key.
				// If not, skip it.
				//
				if (!match_idl_keys(&keylist_key_idl,
				    &my_key)) {
					// Not my key; skip it
					continue;
				}

				// We found our key on the device
				found_my_key = 1;
				CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
				    ("SCSI3(%s) quorum_register: My key 0x%llx "
				    "found on this quorum device\n", pathname,
				    mhioc_key_to_int64_key(
				    register_struct.newkey)));

				register_struct.oldkey = keylist.list[i];

				new_error = quorum_ioctl_with_retries(vnode_ptr,
				    MHIOCGRP_REGISTER,
				    (intptr_t)&register_struct, &retval);

				if (new_error) {
					//
					// We failed to replace our old key
					// with the new key.
					//
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_RED,
					    ("SCSI3(%s) quorum_register: "
					    "ioctl MHIOCGRP_REGISTER failed "
					    "with error %d when trying to "
					    "replace old key 0x%llx with "
					    "new key 0x%llx.\n", pathname,
					    new_error, mhioc_key_to_int64_key(
					    register_struct.oldkey),
					    mhioc_key_to_int64_key(
					    register_struct.newkey)));
				} else {
					//
					// We succeeded in replacing our old key
					// with the new key.
					//
					replaced_my_key = true;
					CMM_SELECTIVE_TRACE(
					    trace_level, CMM_GREEN,
					    ("SCSI3(%s) quorum_register: "
					    "Replaced old key 0x%llx with "
					    "new key 0x%llx\n", pathname,
					    mhioc_key_to_int64_key(
					    register_struct.oldkey),
					    mhioc_key_to_int64_key(
					    register_struct.newkey)));
					error = 0;
					break; // no need to try further
				}
			}

			if (!found_my_key) {
				CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
				    ("SCSI3(%s) quorum_register: My key 0x%llx "
				    "not found on this quorum device\n",
				    pathname, mhioc_key_to_int64_key(
				    register_struct.newkey)));

				//
				// If we did not find our key,
				// then we could not have replaced it as well.
				//
				ASSERT(!replaced_my_key);

			} else if (found_my_key && !replaced_my_key) {
				// We found our key but could not replace it
				CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
				    ("SCSI3(%s) quorum_register: "
				    "Could not replace old key 0x%llx "
				    "with new key 0x%llx.\n", pathname,
				    mhioc_key_to_int64_key(
				    register_struct.oldkey),
				    mhioc_key_to_int64_key(
				    register_struct.newkey)));
			} else {
				//
				// We found our key and replaced it as well.
				// We have already logged trace messages
				// for these actions in code above.
				// No need to do anything here.
				//
				ASSERT(found_my_key && replaced_my_key);
			}
		}

		delete [] keylist.list;
	}

	// Read back the keys for confirmation

	keylist.listsize = NODEID_MAX;
	keylist.list = new mhioc_resv_key_t[keylist.listsize];
	keylist.listlen = 0;
	inkeys.li = &keylist;

	new_error = quorum_ioctl_with_retries(vnode_ptr,
	    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);

	while (keylist.listlen > keylist.listsize) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s) quorum_register: "
		    "MHIOCGRP_INKEYS returned listlen > "
		    "listsize. Retrying ioctl with resized "
		    "list.\n", pathname));

		delete [] keylist.list;
		keylist.listsize = keylist.listlen;
		keylist.list = new mhioc_resv_key_t[keylist.listlen];
		keylist.listlen = 0;

		// Retry the inkeys ioctl
		new_error = quorum_ioctl_with_retries(vnode_ptr,
		    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);
	}

	if (new_error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_register: ioctl "
		    "MHIOCGRP_INKEYS failed with error %d "
		    "during read check of keys\n",
		    pathname, new_error));
	} else {
		// Search for my key in the list

		found_my_key = 0;
		for (i = 0; i < keylist.listlen; i++) {
			// Check for my key in keylist

			uint64_t		keylist_key;
			quorum::registration_key_t	keylist_key_idl;

			keylist_key = mhioc_key_to_int64_key(
			    keylist.list[i]);
			int64_key_to_idl_key(keylist_key,
			    &keylist_key_idl);

			if (match_idl_keys(&keylist_key_idl, &my_key)) {
				found_my_key = 1;
			}
			// Check for duplicate keys

			for (j = 0; j < keylist.listlen; j++) {
				if (i != j) {
					uint64_t	keylist_key2;
					quorum::registration_key_t
					    keylist_key_idl2;

					keylist_key2 = mhioc_key_to_int64_key(
					    keylist.list[j]);
					int64_key_to_idl_key(keylist_key2,
					    &keylist_key_idl2);

					if (match_idl_keys(&keylist_key_idl,
					    &keylist_key_idl2)) {

						os::sc_syslog_msg msgobj(
						    SC_SYSLOG_CMM_QUORUM_TAG,
						    nodename, NULL);
						//
						// SCMSGS
						// @explanation
						// Duplicate keys found on
						// specified quorum device.
						// This error occurs
						// because of unexpected
						// behaviour from device
						// driver or multi-pathing
						// driver or device's firmware.
						// @user_action
						// Please look for updates in
						// device driver, multi-pathing
						// driver or device firmware.
						// If no updates are available
						// please contact Sun device
						// driver support team.
						//
						(void) msgobj.log(
						    SC_SYSLOG_WARNING, MESSAGE,
						    "clq_scsi3: Duplicate keys "
						    "0x%llx, found in "
						    "registration key list of "
						    "quorum device %s.",
						    mhioc_key_to_int64_key(
						    register_struct.oldkey),
						    pathname);
					}
				}
			}
		}

		if ((error == 0) && (found_my_key == 0)) {
			//
			// Either ioctl to register node's key was successful
			// the first time itself; or we replaced our old key
			// with the new key. Hence error is marked zero.
			// So we consider that registration of key succeeded.
			// But we failed to find our key on the device
			// after that.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("SCSI3(%s) quorum_register: Ioctl to register "
			    "node's key returned success but read check found "
			    "that key is not present on this QD.\n", pathname));

			os::sc_syslog_msg msgobj(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename,
			    NULL);
			//
			// SCMSGS
			// @explanation
			// Ioctl to register node's key on
			// specified quorum device returned success
			// but read check found that key is not present.
			// This error
			// occurs because of unexpected
			// behaviour from the device driver or
			// multi-pathing driver or device's
			// firmware.
			// @user_action
			// Please look for updates in device
			// driver, multi-pathing driver or
			// device firmware. If no updates
			// are available please contact Sun
			// device driver support team.
			//
			(void) msgobj.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "clq_scsi3: Ioctl to register node's "
			    "key returned success but read check found "
			    "that key is not present on "
			    "quorum device %s.",
			    pathname);
		}
	}

	if (error == EACCES) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_register got EACCES.\n", pathname));
		return (quorum::QD_EACCES);
	} else if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_register got error %d.\n",
		    pathname, error));
		return (quorum::QD_EIO);
	}

	delete [] keylist.list;
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_register: Registered key 0x%llx.\n",
	    pathname, my_key));
	return (quorum::QD_SUCCESS);
}

//
// Fills in "reg_list" with the keys currently stored on a quorum device.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_read_keys(
    quorum::reservation_list_t		&reg_list,
    Environment			&)
{
	int			error;
	int			retval = 0;
	mhioc_inkeys_t		inkeys;
	mhioc_key_list_t	keylist;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s): quorum_read_keys\n", pathname));

	(void) convert_to_mhioc_reg_list(keylist, reg_list);

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	inkeys.li = (mhioc_key_list_t *)&keylist;

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_INKEYS,
	    (intptr_t)&inkeys, &retval);

	while (keylist.listlen > keylist.listsize) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s) quorum_read_keys: MHIOCGRP_INKEYS"
		    " returned listlen > listsize. Retrying ioctl "
		    "with resized list.\n", pathname));
		delete [] keylist.list;
		keylist.listsize = keylist.listlen;
		keylist.list = new mhioc_resv_key_t[keylist.listlen];
		keylist.listlen = 0;

		// Retry the inkeys ioctl
		error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_INKEYS,
		    (intptr_t)&inkeys, &retval);
	}

	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_read_keys: ioctl "
		    "MHIOCGRP_INKEYS failed with error %d.\n",
		    pathname, error));
	} else {

		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s): quorum_read_keys returned %d keys\n",
		    pathname, keylist.listlen));

		(void) convert_to_idl_reg_list(keylist, reg_list);
		delete [] keylist.list;

		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s): quorum_read_keys returned:\n", pathname));
		for (uint_t i = 0; i < reg_list.listlen; i++) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("key[%d]: key=0x%llx\n",
			    i, idl_key_to_int64_key(&(reg_list.keylist[i]))));
		}
	}

	if (error == EACCES) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_read_keys got EACCES.\n", pathname));
		return (quorum::QD_EACCES);
	} else if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_read_keys got error %d.\n",
		    pathname, error));
		return (quorum::QD_EIO);
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_remove
//
// Remove the device.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_remove(const char *devname,
    const quorum::qd_property_seq_t &,
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_remove\n", devname));

	return (quorum::QD_SUCCESS);
}

//
// quorum_enable_failfast
//
// Turns on failfast monitoring by the sd driver.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_enable_failfast(
    Environment			&)
{
	int error = 0;

	if (failfast_enabled) {
		// Failfast already enabled. Simply return success.
		return (quorum::QD_SUCCESS);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_enable_failfast\n", pathname));

	ASSERT(vnode_ptr != NULL);

	error = quorum_scsi_enable_failfast(vnode_ptr, SCSI3_POLL_PERIOD);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_enable_failfast failed with error %d.\n",
		    pathname, error));
		return ((error == EACCES) ? quorum::QD_EACCES : quorum::QD_EIO);
	}

	failfast_enabled = true;

	return (quorum::QD_SUCCESS);
}

//
// Reset the SCSI bus.
//

quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_reset(
    Environment			&)
{
	int	error;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("SCSI3(%s) quorum_reset called.\n", pathname));

	error = quorum_scsi_reset_bus(vnode_ptr);
	if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) quorum_reset: quorum_scsi_reset_bus() "
		    "retd error %d.\n", pathname, error));
		return ((error == EACCES) ? quorum::QD_EACCES : quorum::QD_EIO);
	}

	return (quorum::QD_SUCCESS);
}

//
// Read a particular block of data from the reserved sectors of the device.
//

quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_data_read(
	uint64_t		location,
	CORBA::String_out	data,
	Environment		&)
{
	//
	// We should never reach here.
	// Not supported for SCSI3
	//
	ASSERT(0);
	return (quorum::QD_EIO);
}

//
// Write a block of data to the reserved sectors of the quorum
// device.
//
quorum::quorum_error_t
quorum_device_scsi3_impl::quorum_data_write(
	uint64_t		location,
	const char 		*data,
	Environment		&)
{
	//
	// We should never reach here.
	// Not supported for SCSI3
	//
	ASSERT(0);
	return (quorum::QD_EIO);
}

//
// Function used internally to read the keys off the device.
// This is provided to avoid an additional translation of
// mhioc style keys to idl style keys.
//

int
quorum_device_scsi3_impl::internal_read_keys(
    mhioc_key_list_t	*keylist)
{
	int retval;
	mhioc_inkeys_t		inkeys;
	inkeys.li = keylist;

	int error = quorum_ioctl_with_retries(vnode_ptr,
	    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);

	while (keylist->listlen > keylist->listsize) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("SCSI3(%s) internal_read_keys: "
		    "MHIOCGRP_INKEYS returned listlen > "
		    "listsize. Retrying ioctl with resized "
		    "list.\n", pathname));
		delete [] keylist->list;
		keylist->listsize = keylist->listlen;
		keylist->list = new mhioc_resv_key_t[keylist->listlen];
		keylist->listlen = 0;

		// Retry inkeys ioctl.
		error = quorum_ioctl_with_retries(vnode_ptr,
		    MHIOCGRP_INKEYS, (intptr_t)&inkeys, &retval);
	}
	return (error);
}

//
// This routine will scrub the victim key from the
// qourum device.
//
int quorum_device_scsi3_impl::internal_preemptandabort_key(
    mhioc_resv_key_t my_key,
    mhioc_resv_key_t victim_key)
{
	int error			= 0;
	int retval			= 0;
	mhioc_preemptandabort_t		preempt;

	char nodename[8];
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	// The vnode_ptr should be non-NULL.
	CL_PANIC(vnode_ptr != NULL);

	//
	// If my_key is same as victim_key,
	// the MHIOCGRP_PREEMPTANDABORT ioctl may hang
	// Hence, do nothing and return.
	//
	if (match_mhioc_keys(my_key, victim_key)) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) internal_preemptandabort_keys: Trying to "
		    "prempt and abort my own key. Do nothing and return. "
		    "my_key = %llx victim_key =%llx\n",
		    pathname,
		    mhioc_key_to_int64_key(my_key),
		    mhioc_key_to_int64_key(victim_key)));
		// Do nothing and return success.
		return (0);
	}

	preempt.resvdesc.key = my_key;
	preempt.victim_key = victim_key;
	preempt.resvdesc.type =	SCSI3_RESV_WRITEEXCLUSIVEREGISTRANTSONLY;
	preempt.resvdesc.scope = SCSI3_SCOPE_LOGICALUNIT;
	preempt.resvdesc.scope_specific_addr = 0;

	error = quorum_ioctl_with_retries(vnode_ptr, MHIOCGRP_PREEMPTANDABORT,
	    (intptr_t)&preempt, &retval);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("SCSI3(%s) internal_preemptandabort_keys: ioctl "
		    "MHIOCGRP_PREEMPTANDABORT failed with error %d. "
		    "Key = %llx\n",
		    pathname, error,
		    mhioc_key_to_int64_key(victim_key)));
	}

	//
	// If the victim's key is not on the device, (some
	// implementations of) the firmware returns a reservation
	// conflict (EACCES). This could happen due to a retry
	// if the original command succeeded in removing the key.
	// Fiber channel LIPs (Loop Initialization Primitives)
	// resulting from node reboots can cause the original
	// command to time out and the retry to get the reservation
	// conflict error.
	//
	// Therefore, if we get an EACCES error, check if the
	// command actually worked. If so, return success.
	//
	if (error == EACCES) {
		int				error_readkeys = 0;
		bool				found_my_key = false;
		bool				found_victim_key = false;
		mhioc_key_list_t		keylist;

		keylist.listsize = NODEID_MAX;
		keylist.listlen = 0;
		keylist.list = new mhioc_resv_key_t[NODEID_MAX];
		if (keylist.list == NULL) {
			// No memory
			CL_PANIC(0);
		}
		// Read all the keys
		error_readkeys = internal_read_keys(&keylist);
		if (error_readkeys) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
			    "SCSI3(%s): "
			    "internal_preemptandabort_keys failed doing "
			    "MHIOCGRP_INKEYS Error = %d Key = %llx\n",
			    pathname, error,
			    mhioc_key_to_int64_key(victim_key)));

			os::sc_syslog_msg msgobj(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// The ioctl to read keys from the specified
			// quorum device failed. This is unexpected
			// behaviour from the device driver,
			// multipathing driver or device
			// firmware.
			// @user_action
			// Use a different device as a quorum device.
			//
			(void) msgobj.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "clq_scsi3: Ioctl MHIOCGRP_INKEYS "
			    "returned failure (error = %d) for "
			    "quorum device %s", error_readkeys,
			    pathname);

			delete [] keylist.list;
			return (error_readkeys);
		}

		// Search for victim key
		for (int i = 0; i < keylist.listlen; i++) {
			if (match_mhioc_keys(keylist.list[i], victim_key)) {
				found_victim_key = true;
				break;
			}
		}

		// Search for my key
		for (int i = 0; i < keylist.listlen; i++) {
			if (match_mhioc_keys(keylist.list[i], my_key)) {
				found_my_key = true;
				break;
			}
		}

		if (found_my_key && !found_victim_key) {
			//
			// The command worked. Ignore the error.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
			    ("SCSI3(%s) internal_preemptandabort_keys: ioctl "
			    "MHIOCGRP_PREEMPTANDABORT returned EACCES, "
			    "but actually worked; error ignored. "
			    "Key = %llx\n",
			    pathname,
			    mhioc_key_to_int64_key(victim_key)));
			error = 0;
		}

		// Free the allocated memory
		delete [] keylist.list;
	}

	if (error != 0) {
		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// The ioctl to scrub keys from the specified
		// quorum device failed. This is unexpected
		// behaviour from the device driver,
		// multipathing driver or device
		// firmware.
		// @user_action
		// Use a different device as a quorum device.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING,
		    MESSAGE,
		    "clq_scsi3: Ioctl MHIOCGRP_PREEMPTANDABORT "
		    "returned failure (error = %d) for "
		    "quorum device %s Key = %llx", error,
		    pathname, mhioc_key_to_int64_key(victim_key));
	}

	return (error);
}

//
// This routine will scrub all keys from the quorum device
//
int
quorum_device_scsi3_impl::internal_scrub_keys()
{
	int error 			= 0;
	int retval			= 0;
	mhioc_registerandignorekey_t	regign;
	mhioc_register_t		register_struct;
	mhioc_resv_key_t		mykey;
	mhioc_key_list_t		keylist;
	mhioc_preemptandabort_t 	pre;

	char nodename[8];
	os::sprintf(nodename, "%d", orb_conf::local_nodeid());

	mykey = quorum_algorithm_impl::get_my_reservation_key();
	bzero(register_struct.oldkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
	bzero(register_struct.newkey.key, (size_t)MHIOC_RESV_KEY_SIZE);

	keylist.listsize = NODEID_MAX;
	keylist.listlen = 0;
	keylist.list = new mhioc_resv_key_t[NODEID_MAX];
	if (keylist.list == NULL) {
		// No memory
		CL_PANIC(0);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN, (
	    "SCSI3(%s): internal_scrub_keys: mykey = 0x%llx\n",
	    pathname, mhioc_key_to_int64_key(mykey)));


	// Read all the keys
	error = internal_read_keys(&keylist);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
		    "SCSI3(%s): internal_scrub_keys failed doing "
		    "MHIOCGRP_INKEYS Error = %d\n",
		    pathname, error));

		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// The ioctl to read keys from the specified
		// quorum device failed. This is unexpected
		// behaviour from the device driver,
		// multipathing driver or device
		// firmware.
		// Use a different device as a quorum device.
		//
		// No need to add the SCMSG tag as this has
		// been declared before.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING,
		    MESSAGE,
		    "clq_scsi3: Ioctl MHIOCGRP_INKEYS "
		    "returned failure (error = %d) for "
		    "quorum device %s", error,
		    pathname);

		delete [] keylist.list;
		return (error);
	}

	//
	// If there are no keys, then nothing to scrub
	//
	if (keylist.listlen == 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN, (
		    "SCSI3(%s): internal_scrub_keys No keys present "
		    "MHIOCGRP_INKEYS Error = %d\n",
		    pathname, error));
		delete [] keylist.list;
		return (error);
	}

	// Make sure that this node is registered with the disk
	regign.newkey = mykey;
	regign.aptpl = B_TRUE;
	error = quorum_ioctl_with_retries(vnode_ptr,
	    MHIOCGRP_REGISTERANDIGNOREKEY, (intptr_t)&regign, &retval);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
		    "SCSI3(%s): internal_scrub_keys failed doing "
		    "MHIOCGRP_REGISTERANDIGNOREKEY Error = %d\n",
		    pathname, error));

		register_struct.newkey = mykey;
		register_struct.aptpl = B_TRUE;
		error = quorum_ioctl_with_retries(vnode_ptr,
		    MHIOCGRP_REGISTER, (intptr_t)&register_struct, &retval);
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
			    "SCSI3(%s): internal_scrub_keys failed doing "
			    "MHIOCGRP_REGISTER Error = %d\n",
			    pathname, error));

			os::sc_syslog_msg msgobj(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// The ioctl to register key for the specified
			// quorum device failed. This is unexpected
			// behaviour from the device driver,
			// multipathing driver or device
			// firmware.
			// @user_action
			// Use a different device as a quorum device.
			//
			(void) msgobj.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "clq_scsi3: Ioctl MHIOCGRP_REGISTER "
			    "returned failure (error = %d) for "
			    "quorum device %s", error,
			    pathname);

			//
			// Could not register with the disk. This disk
			// can not be used as a quorum device.
			//
			delete [] keylist.list;
			return (error);
		}
	}

	// preempt everyone else's keys
	for (int i = 0; i < keylist.listlen; i++) {
		error = internal_preemptandabort_key(mykey, keylist.list[i]);
		if (error) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
			    "SCSI3(%s): internal_scrub_keys failed to scrub "
			    "keys from disk Error = %d Key = %llx\n",
			    pathname, error,
			    mhioc_key_to_int64_key(keylist.list[i])));

			os::sc_syslog_msg msgobj(
			    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
			//
			// SCMSGS
			// @explanation
			// Failed to scrub keys for the specified
			// quorum device. This is unexpected
			// behaviour from the device driver,
			// multipathing driver or device
			// firmware.
			// @user_action
			// Use a different device as a quorum device.
			//
			(void) msgobj.log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "clq_scsi3: Failed to scrub key %llx"
			    " (error = %d) for quorum device %s\n",
			    mhioc_key_to_int64_key(keylist.list[i]),
			    error, pathname);

			//
			// Could not scrub keys from the disk. This disk
			// can not be used as a quorum device.
			//
			delete [] keylist.list;
			return (error);
		}
	}
	delete [] keylist.list;

	// Remove my key
	register_struct.oldkey = mykey;
	bzero(register_struct.newkey.key, (size_t)MHIOC_RESV_KEY_SIZE);
	register_struct.aptpl = B_TRUE;

	error = quorum_ioctl_with_retries(vnode_ptr,
	    MHIOCGRP_REGISTER, (intptr_t)&register_struct, &retval);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
		    "SCSI3(%s): internal_scrub_keys failed to remove my key "
		    "MHIOCGRP_REGISTER Error = %d "
		    "Old Key = %llx New Key = %llx\n",
		    pathname, error,
		    mhioc_key_to_int64_key(register_struct.oldkey),
		    mhioc_key_to_int64_key(register_struct.newkey)));

		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// Failed to scrub the local nodes key for the specified
		// quorum device. This is unexpected
		// behaviour from the device driver,
		// multipathing driver or device
		// firmware.
		// @user_action
		// Use a different device as a quorum device.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING,
		    MESSAGE,
		    "clq_scsi3: Failed to scrub local nodes key "
		    " (error = %d) for "
		    "quorum device %s", error,
		    pathname);

		//
		// Could not scrub my key from the disk. This disk
		// can not be used as a quorum device.
		//
		return (error);
	}

	//
	// Read all the keys again and verify that no key
	// exists.
	//

	mhioc_key_list_t		keylist_check;

	keylist_check.listsize = NODEID_MAX;
	keylist_check.listlen = 0;
	keylist_check.list = new mhioc_resv_key_t[NODEID_MAX];
	if (keylist_check.list == NULL) {
		// No memory
		CL_PANIC(0);
	}

	// Read the keys
	error = internal_read_keys(&keylist_check);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
		    "SCSI3(%s): internal_scrub_keys failed doing "
		    "MHIOCGRP_INKEYS for verifying key scrub. "
		    "Error = %d\n",
		    pathname, error));

		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// The ioctl to read keys from the specified
		// quorum device failed. This is unexpected
		// behaviour from the device driver,
		// multipathing driver or device
		// firmware.
		// Use a different device as a quorum device.
		//
		// No need to add the SCMSG tag as this
		// has been already declared.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING,
		    MESSAGE,
		    "clq_scsi3: Ioctl MHIOCGRP_INKEYS "
		    "returned failure (error = %d) for "
		    "quorum device %s", error,
		    pathname);

		delete [] keylist_check.list;
		return (error);
	}

	//
	// Verify that there are 0 keys. If more than 0
	// keys are found, return error.
	//
	if (keylist_check.listlen > 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED, (
		    "SCSI3(%s): internal_scrub_keys found keys "
		    "despite scrubbing all the keys. "
		    "Error = %d Num Keys = %d\n",
		    pathname, error, keylist_check.listlen));

		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// The ioctl to read keys from the specified
		// quorum device returned more than 0 number
		// of keys. This is unexpected
		// behaviour as the quorum device had been
		// scrubbed.
		// @user_action
		// Use a different device as a quorum device.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING,
		    MESSAGE,
		    "clq_scsi3: Ioctl MHIOCGRP_INKEYS "
		    "returned %d keys for "
		    "quorum device %s. Expected 0 keys.",
		    keylist_check.listlen, pathname);

		//
		// Key still exists. This means the scrub
		// has failed and hence return error.
		// This disk can not be used as a quorum
		// device.
		//
		delete [] keylist_check.list;
		return (ERANGE);
	}

	// Free the memory
	delete [] keylist_check.list;

	// Success
	return (error);
}

//
// Function to detect if the function should return an
// error as part of fault injection testing.
//

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
int
quorum_device_scsi3_impl::check_fi(char	*qop_name)
{
	// Fault point will return failure
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    &fault_argp, &fault_argsize)) {
		char *exp_path = (char *)fault_argp;
		char *cur_path = pathname;
		if ((exp_path == NULL) ||
		    ((cur_path != NULL) && (strcmp(cur_path, exp_path) == 0))) {
			os::printf("==> Fault Injection failing %s operation\n",
			    qop_name);
			return (EIO);
		}
	}
	return (0);
}
#endif // FAULT_CMM && _KERNEL_ORB

//
// Functions to support module load and linkage.
//

static struct modlmisc modlmisc = {
	&mod_miscops, "SCSI3 device type module",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL },
};

char _depends_on[] =
	"misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

int
_init()
{
	int error;

	if ((error = mod_install(&modlinkage)) != 0) {
		CMM_TRACE(("ERROR: could not mod_install\n"));
		return (error);
	}

	CMM_TRACE(("SCSI3: module loading.\n"));

	_cplpl_init();		/* C++ initialization */

	device_type_registry_impl::register_device_type(QD_SCSI3,
	    get_scsi3_qd_impl);

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
