//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qs_socket.cc	1.30	09/02/13 SMI"

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <quorum/quorum_server/qs_socket.h>
#include <orb/infrastructure/orb_conf.h>
#include <quorum/quorum_server/qd_qs_int.h>
#include <netinet/tcp.h>
#ifdef _KERNEL
#include <sys/uio.h>
#include <sys/stream.h>
#include <sys/strsubr.h>
#include <sys/strsun.h>
#include <sys/socketvar.h>
#endif

#define	MSECS_PER_SEC 1000

//
// Global variables (for tuning purpose).
//
int connect_initial_max_attempt = 0;
int connect_initial_time_out = 0;
int connect_initial_retry_sleep = 0;
int connect_blocking_attempt_increment = 0;
int reconnect_max_attempt = 0;
int reconnect_time_out = 0;
int reconnect_retry_sleep = 0;

//
// By setting TCP_CONN_ABORT_THRESHOLD to CONNECT_INITIAL_TIME_OUT = 5
// seconds, we expect that soconnect will block up to 9 seconds, as TCP
// could internally issue 2 connection transmissions before the threshold
// is reached:
//
// (re)transmission  internal timer   absolute time
//
//	initial		3 seconds	3 seconds
//	second		6 seconds	9 seconds
//
// We want to put a 30 seconds limit to our initial connection attempt,
// while considering two distinct scenarios:
//
// [1] the route to the server is not up yet:
//
// - soconnect returns imediately with ENETUNREACH.
// - we should have a short sleep and retry often.
//
// [2] the remote host is down
//
// - soconnect will hit the 9 seconds timeout
//   and return EHOSTUNREACH or ETIMEDOUT.
// - we should not sleep and retry a few times.
//
// To satisfy a 30 second total connection attempt time:
//
// in [1] we will: retry up to 15 times, sleep 2 second in between.
// in [2] we will: retry up to 3 times, block 9 seconds everytime.
//
#define	CONNECT_INITIAL_MAX_ATTEMPT	15
#define	CONNECT_INITIAL_TIME_OUT	5 /* seconds */
#define	CONNECT_INITIAL_RETRY_SLEEP	2 /* seconds */
//
// Number of attempts consumed by a blocking connect.
//
#define	CONNECT_BLOCKING_ATTEMPT_INCREMENT 8
//
// Retry and timing values that apply
// to subsequent reconnection attempt:
//
#define	RECONNECT_MAX_ATTEMPT	1
#define	RECONNECT_TIME_OUT	5  /* seconds */
#define	RECONNECT_RETRY_SLEEP	0  /* seconds */

//
// Constructor.
//
qs_socket::qs_socket(cmm_trace_level level):
	quorum_server_name(NULL),
	port(0),
	addr_family(0),
	sizeof_sockaddr(0),
	sock_vp(NULL),
	so_node(NULL),
	trace_level(level)
{
	bzero(&num_ipaddr, sizeof (num_ipaddr));
	bzero(&address, sizeof (address));
	quorum_server_name = STRDUP("");
}

//
// Default constructor.
// Should not be used.
//
// Lint warns that some class members are not initialized by constructor.
//lint -e1401
qs_socket::qs_socket()
{
	ASSERT(0);
}
//lint +e1401

//
// Destructor.
//
qs_socket::~qs_socket()
{
	if (so_node != NULL) {
#if	SOL_VERSION >= __s11
		(void) VOP_CLOSE(SOTOV(so_node), FREAD|FWRITE, 1, (offset_t)0,
		    CRED(), NULL);
#else	// SOL_VERSION >= __s11
		(void) VOP_CLOSE(SOTOV(so_node), FREAD|FWRITE, 1, (offset_t)0,
		    CRED());
#endif	// SOL_VERSION >= __s11
		VN_RELE(SOTOV(so_node));
		so_node = NULL;
	}
	sock_vp = NULL;
	delete quorum_server_name;
	quorum_server_name = NULL;
}

//
// host_to_network_v6
//
// This function takes a host ordered IPv6 address
// and converts it to a network ordered IPv6 address.
//
void
qs_socket::host_to_network_v6(qs_inaddr_t *ip_addr)
{
#if defined(__i386) || defined(__amd64)
	int index;
	uint16_t *addr_ptr = (uint16_t*)ip_addr->_adr6._S6_un._S6_u8;

	for (index = 0;
	    index < sizeof (struct in6_addr) / sizeof (uint16_t);
	    index++) {
		addr_ptr[index] = htons(addr_ptr[index]);
	}
#endif // (__i386) || (__amd64)
} /*lint !e715 */ /* Function parameter not used on sparc */

//
// set_values
//
// This function initializes the socket, then binds it to given port
// and connect it.
//
int
qs_socket::set_values(const char *qsname, const char *ipaddrp,
    const uint16_t in_port)
{
	delete quorum_server_name;
	quorum_server_name = STRDUP(qsname);

	//
	// Retries and timeouts.
	//
	if (connect_initial_max_attempt == 0) {
		connect_initial_max_attempt = CONNECT_INITIAL_MAX_ATTEMPT;
	}
	if (connect_initial_time_out == 0) {
		connect_initial_time_out = CONNECT_INITIAL_TIME_OUT;
	}
	if (connect_initial_retry_sleep == 0) {
		connect_initial_retry_sleep = CONNECT_INITIAL_RETRY_SLEEP;
	}
	if (connect_blocking_attempt_increment == 0) {
		connect_blocking_attempt_increment =
			CONNECT_BLOCKING_ATTEMPT_INCREMENT;
	}
	if (reconnect_max_attempt == 0) {
		reconnect_max_attempt = RECONNECT_MAX_ATTEMPT;
	}
	if (reconnect_time_out == 0) {
		reconnect_time_out = RECONNECT_TIME_OUT;
	}
	if (reconnect_retry_sleep == 0) {
		reconnect_retry_sleep = RECONNECT_RETRY_SLEEP;
	}

	qs_inaddr_t tmp_ipaddr;
	qs_inaddr_t tmp2_ipaddr;

	if (inet_pton(AF_INET, (char *)ipaddrp, (void *)&tmp_ipaddr) == 1) {
		//
		// We are using an IPv4 address.
		//
		addr_family = AF_INET;
		//
		// Put address in network order.
		//
		num_ipaddr._adr4.s_addr = htonl(tmp_ipaddr._adr4.s_addr);
	} else if (inet_pton(AF_INET6, (char *)ipaddrp,
	    (void *)&tmp_ipaddr) == 1) {
		//
		// We are using an IPv6 address.
		//
		if (IN6_IS_ADDR_V4MAPPED(&(tmp_ipaddr._adr6))) {
			//
			// The address is actually an IPv4 mapped IPv6 address.
			// We will convert it back to an IPv4 address.
			//
			addr_family = AF_INET;
			IN6_V4MAPPED_TO_INADDR(&tmp_ipaddr._adr6,
			    &tmp2_ipaddr._adr4);
			//
			// Put address in network order.
			//
			num_ipaddr._adr4.s_addr =
			    htonl(tmp2_ipaddr._adr4.s_addr);
		} else {
			//
			// IP V6 address case.
			//
			addr_family = AF_INET6;
			//
			// Put address in network order.
			//
			host_to_network_v6(&tmp_ipaddr);
			num_ipaddr = tmp_ipaddr;
		}
	} else {
		return (1);
	}

	port = in_port;

	if (addr_family == AF_INET) {
		sizeof_sockaddr = (socklen_t)sizeof (struct sockaddr_in);
		address.v4.sin_family = addr_family;
		address.v4.sin_addr = num_ipaddr._adr4;
		address.v4.sin_port = htons(port);
	} else {
		sizeof_sockaddr = (socklen_t)sizeof (struct sockaddr_in6);
		address.v6.sin6_family = addr_family;
		address.v6.sin6_addr = num_ipaddr._adr6;
		address.v6.sin6_port = htons(port);
	}

	return (connect_with_retries(connect_initial_time_out,
	    connect_initial_max_attempt, connect_initial_retry_sleep));
}

//
// connect_with_retries
//
// This function tries to connect the socket. It retries on failure
// to establish the connection before giving up.
//
int
qs_socket::connect_with_retries(const int connect_time_out,
const int max_attempt_to_connect, const int retry_connect_sleep)
{
	int current_attempt = 1;
	int res = connect(current_attempt == max_attempt_to_connect,
	    connect_time_out);

	while ((res != 0) && (current_attempt < max_attempt_to_connect)) {
		//
		// If error was not ETIMEDOUT then we sleep
		// for a while before attempting to reconnect.
		//
		if ((res != ETIMEDOUT) && (res != EHOSTUNREACH)) {
			//
			// Sleep before retry.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
			    ("QS(%s): Will retry in %d seconds.\n",
			    quorum_server_name, retry_connect_sleep));
			os::usecsleep(((long)retry_connect_sleep * MICROSEC));
			current_attempt++;
		} else {
			//
			// No sleep as we blocked long enough.
			//
			current_attempt += connect_blocking_attempt_increment;
			if (current_attempt > max_attempt_to_connect) {
				current_attempt = max_attempt_to_connect;
			}
		}
		//
		// Retrying.
		//
		res = connect(current_attempt == max_attempt_to_connect,
		    connect_time_out);
	}
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): connect_with_retries returns %d\n",
	    quorum_server_name, res));

	return (res);
}

//
// connect
//
// This function tries exactly once to establish a connection.
//
int
qs_socket::connect(const bool log_error, const int connect_time_out)
{
	int error = 0;

	//
	// We are not supposed to call connect if there is
	// an existing socket opened (until close() is called).
	// Should we anyway here just call close and then
	// proceed to the connect ? or just return ?
	//
	ASSERT(so_node == NULL);
#if	SOL_VERSION >= __s11
	struct sockparams	*sock_sp;

	error = solookup(addr_family, SOCK_STREAM, 0, &sock_sp);
	if (!sock_sp) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s):solookup returned error %d\n",
		    quorum_server_name, error));
		return (-1);
	}
	so_node = socreate(sock_sp, addr_family, SOCK_STREAM, 0,
	    SOV_SOCKSTREAM, &error);
#else	// SOL_VERSION >= __s11
	sock_vp = solookup(addr_family, SOCK_STREAM, 0, NULL, &error);
	if (!sock_vp) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s):solookup returned error %d\n",
		    quorum_server_name, error));
		return (-1);
	}
	so_node = socreate(sock_vp, addr_family, SOCK_STREAM, 0,
	    SOV_SOCKSTREAM, NULL, &error);
#endif	// SOL_VERSION >= __s11
	if (!so_node) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s):socket create returned error %d\n",
		    quorum_server_name, error));
		return (-1);
	}

	//
	// Set the timeout to give up connection establishment to a
	// reasonable value.
	//
	int cabort_timeout = connect_time_out * MSECS_PER_SEC;
	if (sosetsockopt(so_node, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD,
	    (void *)&cabort_timeout, (t_uscalar_t)sizeof (cabort_timeout))
	    != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s): sosetsockopt failed\n", quorum_server_name));
		close();
		return (-1);
	}
	error = soconnect(so_node, (struct sockaddr *)&address,
	    (socklen_t)sizeof_sockaddr, 0, 0);
	if (error) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s): soconnect failed with error = %d\n",
		    quorum_server_name, error));
		if (log_error) {
			os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "",
			    NULL);
			//
			// SCMSGS
			// @explanation
			// This node encountered an error while trying to
			// connect to a quorum server host.
			// @user_action
			// Check Sun Cluster quorum server administration
			// documentation. Make sure that all Sun Cluster nodes
			// can communicate with the Sun Cluster Quorum Server.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CMM: Connection to quorum server %s failed with "
			    "error %d.", quorum_server_name, error);
		}
		close();
		return (error);
	}
	// XXX Need setsockopt for keep_alive or linger options ?
	return (0);
}

//
// is_connected
//
// Returns true if the socket is currently connected.
//
bool
qs_socket::is_connected() {
	return (so_node != NULL);
}

//
// send
//
// This function takes a message header and optional data
// and sends it on the socket.
//
int
qs_socket::send(qd_qs_msghdr *mhdr, void *data)
{
	struct nmsghdr	hdr;
	struct uio	uios;
	struct iovec	vec[2];

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): qs_socket::send msghdr=%p, data=%p\n",
	    quorum_server_name, mhdr, data));

	//
	// Reset cookie string in header.
	//
	mhdr->reset_cookie();

	vec[0].iov_base = (char *)mhdr;
	vec[0].iov_len = MSGHDR_SIZE;

	if (data) {
		vec[1].iov_base = (char *)data;
		vec[1].iov_len = (long)mhdr->length;
	} else {
		ASSERT(mhdr->length == 0);
	}
	qs_sockaddr sa;
	bcopy(&address.v4, &sa.v4, (unsigned long)sizeof_sockaddr);

#ifdef CONNECT_NOT_CALLED
	hdr.msg_name = (char *)&sa.v4;
	hdr.msg_namelen = sizeof_sockaddr;
#else
	hdr.msg_name = NULL;
	hdr.msg_namelen = 0;
#endif
	hdr.msg_iov = vec;
	hdr.msg_iovlen = (data) ? 2 : 1;
	hdr.msg_control = NULL;
	hdr.msg_controllen = 0;
	hdr.msg_flags = 0;

	uios.uio_loffset = 0;
	uios.uio_iov = vec;
	uios.uio_iovcnt = (data) ? 2 : 1;
	uios.uio_resid = (long)MSGHDR_SIZE + (long)mhdr->length;
	uios.uio_segflg = UIO_SYSSPACE;
	uios.uio_limit = 0;
	uios.uio_fmode = 0;

#ifdef QS_SOCKET_DEBUG_PRINT
	os::printf(
	    "\t cluster[%s] t_str[%s] nid[%ld]\n"
	    "\t msgnum[%u] length[%u] type[%u] incn[%u] error[%u] v[%u.%u]"
	    "\n",
	    mhdr->cluster_name,
	    mhdr->type_str,
	    mhdr->get_nid(),
	    mhdr->get_msg_num(),
	    mhdr->get_length(),
	    mhdr->get_msg_type(),
	    mhdr->get_incn(),
	    mhdr->get_error(),
	    (unsigned int)mhdr->get_version_major(),
	    (unsigned int)mhdr->get_version_minor());
#endif

	int error;
	//
	// Not supposed to send before opening the socket.
	//
	ASSERT(so_node != NULL);
	error = sosendmsg(so_node, &hdr, &uios);
	if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s): sendmsg failed with error = %d\n",
		    quorum_server_name, error));
		return (error);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): sendmsg returned %d\n", quorum_server_name, error));
	return (0);
}

//
// recv
//
// Receive message from socket. Returns the received message header
// an data content if any.
//
int
qs_socket::recv(uint_t, qd_qs_msghdr *msghdrp, void *data)
{
	return (recv(msghdrp, data));
}

//
// recv
//
// Receive message from socket. Returns the received message header
// an data content if any.
//
int
qs_socket::recv(qd_qs_msghdr *msghdrp, void *data)
{
	struct nmsghdr	hdr;
	struct uio	uios;
	struct iovec	vec[2];
	int		ret;

	vec[0].iov_base = (char *)msghdrp;
	vec[0].iov_len = MSGHDR_SIZE;
	if (msghdrp->length) {
		vec[1].iov_base = (char *)data;
		vec[1].iov_len = (long)msghdrp->length;
	}
	//
	// Set flags in the msghdr structure to peek and return
	// without blocking.
	//
	hdr.msg_iov = NULL;
	hdr.msg_iovlen = 0;

	qs_sockaddr sa;

	hdr.msg_name = (char *)&sa.v4;
	hdr.msg_namelen = sizeof_sockaddr;
	hdr.msg_flags = 0; // MSG_PEEK | MSG_DONTWAIT;
	hdr.msg_control = NULL;
	hdr.msg_controllen = 0;

	uios.uio_loffset = 0;
	uios.uio_iov = vec;
	uios.uio_iovcnt = (msghdrp->length) ? 2 : 1;
	uios.uio_resid = (long)MSGHDR_SIZE + (long)msghdrp->length;
	uios.uio_segflg = UIO_SYSSPACE;
	uios.uio_limit = 0;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): recvmsg uio_resid=%d\n",
	    quorum_server_name, uios.uio_resid));

	//
	// Not supposed to receive before opening the socket.
	//
	ASSERT(so_node != NULL);

	int eagain_number = 0;
	while ((ret = sorecvmsg(so_node, &hdr, &uios)) != 0) {
		if (ret != EAGAIN) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("QS(%s) 1:sorecvmsg returned error. %d\n",
			    quorum_server_name, ret));
			return (ret);
		} else {
			eagain_number++;
		}
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): recv icncn= %ld, local =%ld "
	    "(eagain: %d)\n", quorum_server_name, msghdrp->get_incn(),
	    orb_conf::local_incarnation(), eagain_number));
	return (ret);
}

//
// close
//
void
qs_socket::close(void)
{
	if (so_node != NULL) {
#if	SOL_VERSION >= __s11
		(void) VOP_CLOSE(SOTOV(so_node), FREAD|FWRITE, 1, (offset_t)0,
		    CRED(), NULL);
#else	// SOL_VERSION >= __s11
		(void) VOP_CLOSE(SOTOV(so_node), FREAD|FWRITE, 1, (offset_t)0,
		    CRED());
#endif	// SOL_VERSION >= __s11
		VN_RELE(SOTOV(so_node));
		so_node = NULL;
		sock_vp = NULL;
	}
}

//
// reopen
//
// Called when connection is lost with the server.
//
int
qs_socket::reopen()
{
	ASSERT(so_node == NULL);
	return (connect_with_retries(reconnect_time_out,
	    reconnect_max_attempt, reconnect_retry_sleep));
}
