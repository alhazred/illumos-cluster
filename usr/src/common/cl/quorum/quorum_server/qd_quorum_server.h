/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QUORUM_SERVER_H
#define	_QUORUM_SERVER_H

#pragma ident	"@(#)qd_quorum_server.h	1.19	08/10/01 SMI"

/* Interface for Quorum Server */

#include <orb/object/adapter.h>
#include <h/quorum.h>
#include <quorum/quorum_server/qs_socket.h>
#include <quorum/quorum_server/qd_qs_msg.h>
#include <cmm/cmm_debug.h>

//
// This class implements the Quorum API for
// the Quorum Server type of quorum devices.
//
class qd_quorum_server_impl :
	public McServerof <quorum::quorum_device_type> {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the Generic
	// Quorum IDL interface.
	//
	virtual quorum::quorum_error_t		quorum_open(
	    const char				*devname,
	    const bool				scrub,
	    const quorum::qd_property_seq_t	&,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_close(
	    Environment				&);

	virtual uint32_t quorum_supports_amnesia_prot(Environment &);

	virtual quorum::quorum_error_t		quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_read_reservations(
	    quorum::reservation_list_t		&resv_list,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&vict_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_read_keys(
	    quorum::reservation_list_t	&reg_list,
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_read(
	    uint64_t			location,
	    CORBA::String_out		data,
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_write(
	    uint64_t			location,
	    const char			*data,
	    Environment			&);

	virtual quorum::quorum_error_t		quorum_remove(
	    const char				*devname,
	    const quorum::qd_property_seq_t	&qd_props,
	    Environment				&);

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

	qd_quorum_server_impl(cmm_trace_level);
	qd_quorum_server_impl();
	~qd_quorum_server_impl();

private:
	//
	// Helper function for quorum_open.
	//
	quorum::quorum_error_t	open_device(bool do_scrub = false);

	//
	// Helper function for wait_until_done_with_retries.
	//
	bool check_error_and_retry_communication(
	    int &error,
	    uint_t &current_attempt,
	    os::systime &sys_timeout);

	//
	// Initialize quorum server kernel module.
	//
	quorum::quorum_error_t  initialize();

	//
	// Helper function for quorum keys read operations.
	//
	quorum::quorum_error_t	read_keys(qd_qs_msghdr *hdrp,
	    quorum::reservation_list_t	&key_list);

	//
	// Read quorum server configuration from CCR.
	//
	quorum::quorum_error_t	read_config_from_ccr();
	uint32_t get_next_msgnum();

	//
	// Helper function that checks the validity of a
	// reply message.
	//
	bool is_valid_reply();

	//
	// Re-initialize message body for reuse.
	//
	void reinit_msg(uint32_t msgnum, qd_qs_msg_type type);

	//
	// Signals worker thread that there is a message
	// to process and wait for operation completion or time-out.
	//
	void wait_until_done_with_retries();

	//
	// Worker thread (thread that handles messages send/receive
	// requests to/from quorum server daemon) related methods.
	//
	static void worker_thread(void *);
	void work();

	//
	// Properties stored in CCR.
	//
	char		*clustername;
	char		*devnamep;
	char		*ipaddrp;
	uint16_t	port;
	uint_t		timeout; // seconds

	//
	// Message number related attributes.
	//
	uint32_t	msgnum;
	uint32_t	msgrecd;

	//
	// Cluster Id.
	//
	uint32_t clusterid;

	//
	// Socket, message header, data buffer and lenght attributes
	// used for communication.
	//
	qs_socket	socket;
	qd_qs_msghdr	*msghdr;
	void		*data;
	uint32_t	send_data_len;
	uint32_t	recv_data_len;

	//
	// Failfast enabled/disabled flag.
	//
	bool		ff_enabled;

	//
	// Serializes access to data(msgnum, socket) and timer thread.
	//
	os::mutex_t	oplock;

	//
	// Timer thread locks, cv and state attributes.
	//
	os::mutex_t	timer_lock;
	os::condvar_t	timer_cv;
	bool		do_work;
	os::threadid_t	tid;
	//
	// Timer thread state definition:
	//
	// RUN   the thread is running.
	// READY the thread is ready to process a new request.
	// STOP  the thread has been asked to stop.
	// DONE  the thread processed the STOP request and exited.
	//
	enum timer_thread_state {RUN, READY, STOP, DONE};
	timer_thread_state thr_state;

	cmm_trace_level trace_level;

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	// Function for fault injection testing.
	int	check_fi(char*);
#endif
};

#include <quorum/quorum_server/qd_quorum_server_in.h>

#endif /* _QUORUM_SERVER_H */
