//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	QD_QS_MSG_IN_H
#define	QD_QS_MSG_IN_H

#pragma ident	"@(#)qd_qs_msg_in.h	1.23	08/05/20 SMI"

#ifdef _KERNEL
#include <orb/infrastructure/orb_conf.h>
#endif // _KERNEL

//
// Constructor.
//
inline
qd_qs_msghdr::qd_qs_msghdr():
    msg_num(0),
    type(QD_OPEN),
    length(0),
    clid(0),
    version_major(htons((uint16_t)SCQSD_VERSION_MAJOR)),
    version_minor(htons((uint16_t)SCQSD_VERSION_MINOR)),
    error((dq_qs_error)htonl((uint32_t)QD_QS_SUCCESS)),
#ifdef _KERNEL
    nid(htonl(orb_conf::local_nodeid())),
    incn(htonl(orb_conf::local_incarnation()))
#else
    nid(0),
    incn(0)
#endif // _KERNEL
{
	STRCPY(type_str, type_to_str());
	bzero((void *)&op_data, sizeof (op_data));
	op_data.scrub = 0;
	op_data.data_length = 0;
	reset_cookie();
}

//
// Constructor.
//
inline
qd_qs_msghdr::qd_qs_msghdr(const char *cname, uint32_t clusterid,
    uint32_t num, qd_qs_msg_type t, uint32_t len) :
	msg_num(htonl(num)),
	clid(htonl(clusterid)),
	type((qd_qs_msg_type)htonl(t)),
	length(htonl(len)),
	version_major(htons((uint16_t)SCQSD_VERSION_MAJOR)),
	version_minor(htons((uint16_t)SCQSD_VERSION_MINOR)),
	error((dq_qs_error)htonl((uint32_t)QD_QS_SUCCESS)),
#ifdef _KERNEL
	nid(htonl(orb_conf::local_nodeid())),
	incn(htonl(orb_conf::local_incarnation()))
#else
	nid(0),
	incn(0)
#endif // _KERNEL
{
	STRCPY(type_str, type_to_str());
	bzero(cluster_name, 128);
	STRCPY(cluster_name, cname);
	bzero((void *)&op_data, sizeof (op_data));
	op_data.scrub = 0;
	op_data.data_length = 0;
	reset_cookie();
}

//
// reinit
//
// Re-initialize message header for reuse.
//
inline void
qd_qs_msghdr::reinit(uint32_t num, qd_qs_msg_type t, uint32_t len)
{
	msg_num = htonl(num);
	type = (qd_qs_msg_type)htonl(t);
	length = htonl(len);
	error = QD_QS_SUCCESS;
	STRCPY(type_str, type_to_str());
}

//
// Get methods.
//
inline qd_qs_msg_type
qd_qs_msghdr::get_msg_type()
{
	return ((qd_qs_msg_type)ntohl(type));
}

inline uint32_t
qd_qs_msghdr::get_msg_num()
{
	return (ntohl(msg_num));
}

inline uint32_t
qd_qs_msghdr::get_cluster_id()
{
	return (ntohl(clid));
}

inline nodeid_t
qd_qs_msghdr::get_nid()
{
	return ((nodeid_t)ntohl(nid));
}

inline uint32_t
qd_qs_msghdr::get_incn()
{
	return (ntohl(incn));
}

inline uint32_t
qd_qs_msghdr::get_port()
{
	return (ntohl(op_data.port));
}

inline dq_qs_error
qd_qs_msghdr::get_error()
{
	return ((dq_qs_error)ntohl(error));
}

inline uint32_t
qd_qs_msghdr::get_length()
{
	return (ntohl(length));
}

inline	uint16_t
qd_qs_msghdr::get_version_major()
{
	return (ntohs(version_major));
}

inline	uint16_t
qd_qs_msghdr::get_version_minor()
{
	return (ntohs(version_minor));
}

inline uint32_t
qd_qs_msghdr::get_data_length()
{
	return (ntohl(op_data.data_length));
}

//
// Set methods.
//
inline void
qd_qs_msghdr::set_port(uint32_t new_port)
{
	op_data.port = htonl(new_port);
}

inline void
qd_qs_msghdr::set_error(dq_qs_error err)
{
	error = (dq_qs_error)htonl(err);
}

inline void
qd_qs_msghdr::set_length(uint32_t l)
{
	length = htonl(l);
}

inline void
qd_qs_msghdr::set_data_length(uint32_t l)
{
	op_data.data_length = htonl(l);
}

//
// Maps quorum server errors to quorum interface error.
//
inline
quorum::quorum_error_t
qs_to_quorum_error(dq_qs_error err) {
	switch (err) {
	case QD_QS_SUCCESS:
		return (quorum::QD_SUCCESS);
	case QD_QS_EACCES:
		return (quorum::QD_EACCES);
	case QD_QS_EIO:
	case QD_QS_ENOENT:
	default:
		return (quorum::QD_EIO);
	}
}

//
// Set cookie value.
//
inline void
qd_qs_msghdr::set_cookie()
{
	STRCPY(cookie, SCQSD_MESSAGE_COOKIE_STRING);
}

//
// Reset cookie value.
//
inline void
qd_qs_msghdr::reset_cookie()
{
	STRCPY(cookie, SCQSD_MESSAGE_COOKIE_RESET);
}

//
// Returns true if cookie value is set in message header.
//
inline bool
qd_qs_msghdr::is_cookie_set()
{
	return (!STRCMP(cookie, SCQSD_MESSAGE_COOKIE_STRING));
}

//
// Message type to string.
//
inline char *
qd_qs_msghdr::type_to_str()
{
	// On amd64 htonl changed the type value,
	// so it must be reversed back before being used.
	switch (ntohl(type)) {
	case QD_NO_MSG:
		return ("QD_NO_MSG");
		break;
	case QD_OPEN:
		return ("QD_OPEN");
		break;
	case QD_RESERVE:
		return ("QD_RESERVE");
		break;
	case QD_READ_RESERVATIONS:
		return ("QD_READ_RESERVATIONS");
		break;
	case QD_PREEMPT:
		return ("QD_PREEMPT");
		break;
	case QD_READ_KEYS:
		return ("QD_READ_KEYS");
		break;
	case QD_ENABLE_FF:
		return ("QD_ENABLE_FF");
		break;
	case QD_RESET:
		return ("QD_RESET");
		break;
	case QD_REGISTER:
		return ("QD_REGISTER");
		break;
	case QD_DATA_READ:
		return ("QD_DATA_READ");
		break;
	case QD_DATA_WRITE:
		return ("QD_DATA_WRITE");
		break;
	case QD_STATUS:
		return ("QD_STATUS");
		break;
	case QD_REMOVE:
		return ("QD_REMOVE");
		break;
	case QD_STOP:
		return ("QD_STOP");
		break;
	case QD_HOST:
		return ("QD_HOST");
		break;
	case QD_CLOSE:
		return ("QD_CLOSE");
		break;
	default:
		return ("QD_UNKNOWN");
		break;
	}
}

#endif // QD_QS_MSG_IN_H
