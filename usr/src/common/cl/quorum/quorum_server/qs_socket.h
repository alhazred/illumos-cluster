/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QS_SOCKET_H
#define	_QS_SOCKET_H

#pragma ident	"@(#)qs_socket.h	1.12	08/09/02 SMI"

#include <sys/socket.h>
#include <quorum/quorum_server/qd_qs_msg.h>
#include <sys/vnode.h>
#include <cmm/cmm_debug.h>

typedef union qsa {
	struct in_addr	_adr4;
	struct in6_addr	_adr6;
} qs_inaddr_t;

typedef union saddr {
	struct sockaddr_in 	v4;
	struct sockaddr_in6	v6;
} qs_sockaddr;

//
// This class provides support for quorum server kernel module
// socket based communications with the Quorum Server process running
// on a host external to the cluster.
//
class qs_socket {
public:
	qs_socket(cmm_trace_level);
	qs_socket();
	~qs_socket();

	//
	// Initialize socket, bind it to given port and connect.
	//
	int	set_values(
		    const char *qsname,
		    const char *ipaddrp,
		    const uint16_t in_port);

	//
	// Send message on socket.
	//
	int	send(qd_qs_msghdr *mhdr, void *data = NULL);

	//
	// Receive message from socket.
	//
	int	recv(uint_t msgnum, qd_qs_msghdr *msghdr, void *data = NULL);
	int	recv(qd_qs_msghdr *msghdr, void *data);

	//
	// Close the socket.
	//
	void	close(void);

	//
	// Return socket connection status.
	//
	bool	is_connected();

	//
	// Reopen the socket.
	//
	int	reopen();

	//
	// Converts IPv6 address from host order to network order.
	//
	void	host_to_network_v6(qs_inaddr_t *ip_addr);

protected:
	//
	// Connect the socket, retry on failure.
	//
	int	connect_with_retries(
		    const int connect_time_out,
		    const int max_attempt_to_connect,
		    const int retry_connect_sleep);

	//
	// Connect the socket.
	//
	int	connect(const bool log_error, const int connect_time_out);

	//
	// Quorum server name (we use it in debug messages).
	//
	char	*quorum_server_name;

	//
	// Socket related attributes.
	//
	qs_inaddr_t	num_ipaddr;
	uint16_t	port;
	uint16_t	addr_family;
	qs_sockaddr	address;
	socklen_t	sizeof_sockaddr;
	vnode_t		*sock_vp;
	struct sonode	*so_node;

	cmm_trace_level trace_level;
};

// #include <quorum/quorum_server/qs_socket_in.h>

#endif /* _QS_SOCKET_H */
