/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QD_QS_MSG_H
#define	_QD_QS_MSG_H

#pragma ident	"@(#)qd_qs_msg.h	1.19	08/05/20 SMI"

#include <h/quorum.h>
#include <quorum/quorum_server/qd_qs_int.h>

#define	SCQSD_VERSION_MAJOR 1
#define	SCQSD_VERSION_MINOR 0
#define	SCQSD_MESSAGE_COOKIE_STRING "scqsd"
#define	SCQSD_MESSAGE_COOKIE_RESET "reset"
#define	SCQSD_MESSAGE_COOKIE_SIZE 6 // including '\0' character

//
// Define message types used to communicate with quorum server.
//
enum qd_qs_msg_type {
	QD_NO_MSG = 0,		// 0
	QD_OPEN,		// 1
	QD_RESERVE,		// 2
	QD_READ_RESERVATIONS,	// 3
	QD_PREEMPT,		// 4
	QD_READ_KEYS,		// 5
	QD_ENABLE_FF,		// 6
	QD_RESET,		// 7
	QD_REGISTER,		// 8
	QD_DATA_READ,		// 9
	QD_DATA_WRITE,		// 10
	QD_STATUS,		// 11
	QD_REMOVE,		// 12
	QD_STOP,		// 13
	QD_HOST,		// 14
	QD_CLOSE		// 15
};

//
// Define errors used when communicating with quorum server.
//
enum dq_qs_error {
	QD_QS_SUCCESS,		// maps to QD_SUCCESS
	QD_QS_EACCES,		// maps to QD_EACCES
	QD_QS_EIO,		// maps to QD_EIO
	QD_QS_ENOENT = 100,	// from there, add more semantic
	QD_QS_INVALID
};

//
// Maps quorum server errors to quorum interface error.
//
quorum::quorum_error_t
qs_to_quorum_error(dq_qs_error err);

//
// Define message header.
//
class qd_qs_msghdr {
public:
	qd_qs_msghdr();
	qd_qs_msghdr(const char *cname, uint32_t clusterid,
	    uint32_t num, qd_qs_msg_type t, uint32_t len = 0);

	//
	// Re-initialize message header for reuse.
	//
	void reinit(uint32_t num, qd_qs_msg_type t,
	    uint32_t len = 0);

	//
	// Get methods.
	//
	qd_qs_msg_type		get_msg_type();
	uint32_t		get_port();
	dq_qs_error		get_error();
	uint32_t		get_length();
	uint32_t		get_data_length();
	uint32_t		get_msg_num();
	nodeid_t		get_nid();
	uint32_t		get_incn();
	uint16_t		get_version_major();
	uint16_t		get_version_minor();
	uint32_t		get_cluster_id();

	//
	// Set methods.
	//
	void			set_port(uint32_t newport);
	void			set_error(dq_qs_error err);
	void			set_length(uint32_t l);
	void			set_data_length(uint32_t l);

	//
	// Message cookie manipulation functions.
	//
	void			set_cookie();
	void			reset_cookie();
	bool			is_cookie_set();

	//
	// Message type to string.
	//
	char 			*type_to_str();

	//
	// Message header data: cluster name, message number, type,
	// type string, version information, data length, error,
	// node id, incarnation number, cluster id.
	//
	char 			cluster_name[128]; // 128 bytes
	uint32_t		msg_num;	// 4 bytes
	qd_qs_msg_type		type;		// 4 bytes
	char			type_str[22];   // 22 bytes
	char			cookie[SCQSD_MESSAGE_COOKIE_SIZE];
	//
	// Version information.
	//
	uint16_t		version_major;	// 2 bytes
	uint16_t		version_minor;	// 2 bytes
	//
	// Length is the actual number of bytes of data being transferred.
	//
	uint32_t		length; 	// 4 bytes
	dq_qs_error		error;		// 4 bytes
	nodeid_t		nid;		// 4 bytes
	uint32_t		incn;		// 4 bytes
	uint32_t		clid;		// 4 bytes
	union {					// 8 bytes
		//
		// Message header optional data: scrub flag,
		// registration key, victim key, number of keys
		// stored in message data section.
		//
		bool 				scrub;		// 1 byte
		quorum::registration_key_t	key;		// 8 bytes
		//
		// Victim key is included in order to avoid allocating memory
		// on the quorum server for this operation.
		//
		quorum::registration_key_t	victim_key;	// 8 bytes
		uint32_t			port;  		// 4 bytes
		//
		// Data length is used for specifying the number of
		// items to be read. Used mostly for reading keys.
		//
		uint32_t			data_length;	// 4 bytes
	} op_data;
};

#define	MSGHDR_SIZE	sizeof (qd_qs_msghdr)

#include <quorum/quorum_server/qd_qs_msg_in.h>

#endif /* _QD_QS_MSG_H */
