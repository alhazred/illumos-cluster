//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qd_quorum_server.cc	1.38	09/04/29 SMI"

#include <cmm/cmm_debug.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/clconf_int.h>
#include <orb/infrastructure/clusterproc.h>

#include <sys/quorum_int.h>

#include <quorum/common/quorum_util.h>
#include <quorum/common/type_registry.h>
#include <quorum/quorum_server/qd_quorum_server.h>
#include <quorum/quorum_server/qd_qs_msg.h>
#include <quorum/quorum_server/qd_qs_int.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/qd_userd.h>

//
// Global variables (for tuning purpose).
//
uint_t communication_max_attempt = 0;
uint_t communication_timeout_default = 0;

//
// Retry and timing values that apply
// to communication attempt:
//
#define	COMMUNICATION_MAX_ATTEMPT	2
#define	COMMUNICATION_TIMEOUT_DEFAULT	5 /* seconds */

static int get_ipaddress(char *hostname, char *ip);
static int do_door_call(char *cmd, door_arg_t **dargp);

//
// get_qd_impl is the access point for the type_registry to modlookup
// in the quorum_server module.
//
quorum::quorum_device_type_ptr
get_qd_quorum_server_impl(cmm_trace_level level) {
	CMM_SELECTIVE_TRACE(level, CMM_GREEN,
	    ("clq_quorum_server: Returning new quorum device.\n"));
	return ((new qd_quorum_server_impl(level))->get_objref());
}

//
// Constructor.
//
qd_quorum_server_impl::qd_quorum_server_impl(cmm_trace_level level) :
	clustername(NULL),
	devnamep(NULL),
	ipaddrp(NULL),
	port(0),
	timeout(COMMUNICATION_TIMEOUT_DEFAULT),
	msgnum(0),
	msgrecd(0),
	clusterid(0),
	socket(level),
	msghdr(NULL),
	data(NULL),
	send_data_len(0),
	recv_data_len(0),
	ff_enabled(false),
	do_work(false),
	tid(0),
	thr_state(DONE),
	trace_level(level)
{
}

//
// Default constructor.
// Should not be used.
//
// Lint warns that some class members are not initialized by constructor.
//lint -e1401
qd_quorum_server_impl::qd_quorum_server_impl()
{
	ASSERT(0);
}
//lint +e1401

//
// Destructor.
//
qd_quorum_server_impl::~qd_quorum_server_impl()
{
	delete devnamep;
	devnamep = NULL;

	//
	// Either quorum_close() has freed the memory pointed to by
	// these pointers and set these pointers to NULL,
	// or we do so here.
	// Any code that does a free/delete on these pointers
	// must set them to NULL after the free/delete operation;
	// else we will end up doing free/delete multiple times.
	//
	delete clustername;
	clustername = NULL;
	delete ipaddrp;
	ipaddrp = NULL;
	delete msghdr;
	msghdr = NULL;

	//
	// The attribute 'data' is not deleted. It is used to point
	// to the optional data to be sent in the message if any.
	// Allocation/Free is done within the interface implementation.
	//
	data = NULL;

	// The thread would have already been cleaned up
	ASSERT(tid == 0);
}

//
// Unreferenced.
//
void
#ifdef DEBUG
qd_quorum_server_impl::_unreferenced(unref_t cookie)
#else
qd_quorum_server_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("qd_quorum_server_impl(%s)::_unreferenced.\n", devnamep));

	//
	// Quorum objects are instantiated in the kernel
	// while the callers instantiating those objects can reside
	// in kernel or userland. If a userland client dies
	// after calling quorum_open() but before calling quorum_close(),
	// then the sole reference held by the client for the quorum
	// object is gone without a call to quorum_close().
	//
	// So we call quorum_close() here to clean-up state.
	// quorum_close() is implemented to be idempotent.
	//
	// The removal of quorum server just instantiates a quorum
	// device object and calls quorum_remove() on it, and then
	// releases the sole reference to this instantiated object.
	// One important point to note is that the quorum server daemon
	// does not expect a QD_CLOSE request after a QD_REMOVE request.
	// So the quorum_remove() operation closes any open connection
	// to the quorum server node after doing its work, and then it
	// also deletes the msghdr; as a result a quorum_close() call
	// here would not attempt to send a QD_CLOSE message to
	// the quorum server node, and would just clean-up other
	// data structures instead.
	//
	// Hence it is safe to call quorum_close() here.
	// This object is going away; so ignore return value
	// and exception from quorum_close().
	//
	Environment e;
	(void) quorum_close(e);
	e.clear();

	delete this;
}

//
// initialize
//
// This function initializes the quorum server. It
// gets the device information from CCR, then instanciates a new
// message header object to be re-used for all communication
// calls later on, and then creates a worker thread that will
// be dedicated to handle message communications (send/receive)
// with the quorum server process on the remote host.
//
quorum::quorum_error_t
qd_quorum_server_impl::initialize() {

	clconf_cluster_t *clconf = clconf_cluster_get_current();
	clconf_obj_t *nd = (clconf_obj_t *)clconf;

	const char *cname =    clconf_obj_get_name((clconf_obj_t *)nd);
	const char *idstring = clconf_obj_get_property((clconf_obj_t *)nd,
	    "cluster_id");

	ASSERT(cname);
	ASSERT(idstring);

	clconf_obj_release((clconf_obj_t *)clconf);

	//
	// Retry and timeout.
	//
	if (communication_max_attempt == 0) {
		communication_max_attempt = COMMUNICATION_MAX_ATTEMPT;
	}
	if (communication_timeout_default == 0) {
		communication_timeout_default = COMMUNICATION_TIMEOUT_DEFAULT;
	}
	timeout = communication_timeout_default;

	//
	// Convert the cluster id string into a
	// numeric value.
	//
	const char *keystr = idstring;
	if ((keystr[0] == '0') && (keystr[1] == 'x')) {
		keystr = &(keystr[2]);
	}
	for (int i = 0; i < 8; i++) {
		char current_char = keystr[i];
		if ((current_char >= '0') && (current_char <= '9')) {
			clusterid = (clusterid << 4) +
				(uchar_t)(current_char - '0');
		} else if ((current_char >= 'A') && (current_char <= 'F')) {
			clusterid = (clusterid << 4) + 0xa +
				(uchar_t)(current_char - 'A');
		} else if ((current_char >= 'a') && (current_char <= 'f')) {
			clusterid = (clusterid << 4) + 0xa +
				(uchar_t)(current_char - 'a');
		} else {
			break;
		}
	}

	clustername = os::strdup(cname);
	if (clustername == NULL) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("initialize: Failed to allocate memory for clustername."));
		return (quorum::QD_EIO);
	}

	//
	// Creates a message header object with our cluster name,
	// cluster id.
	//
	msghdr = new qd_qs_msghdr(clustername, clusterid, 0, QD_NO_MSG);
	if (msghdr == NULL) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("initialize: Failed to allocate memory for msghdr."));
		return (quorum::QD_EIO);
	}

	timer_lock.lock();
	thr_state = RUN;
	//
	// Creates a worker thread to manage node to quorum
	// host communications.
	//
	if (clnewlwp(qd_quorum_server_impl::worker_thread, (caddr_t)this, 0,
	    orb_conf::rt_classname(), NULL) != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("initialize: Failed to create worker thread."));
		thr_state = DONE;
		timer_lock.unlock();
		return (quorum::QD_EIO);
	}
	timer_lock.unlock();

	return (quorum::QD_SUCCESS);
}

//
// IDL method to set the message tracing level of this quorum device
//
void
qd_quorum_server_impl::set_message_tracing_level(
    uint32_t new_trace_level, Environment &)
{
	trace_level = (cmm_trace_level)new_trace_level;
}

//
// IDL method to get the message tracing level of this quorum device
//
uint32_t
qd_quorum_server_impl::get_message_tracing_level(Environment &)
{
	return ((uint32_t)trace_level);
}

//
// quorum_open
//
// Opens a quorum device at given IP address and port.
// This handle is used in all other calls.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_open(const char *name, const bool scrub,
    const quorum::qd_property_seq_t &qd_props, Environment &)
{
	quorum::quorum_error_t	qerr = quorum::QD_SUCCESS;

	oplock.lock();

	//
	// A particular quorum device object is associated with
	// a single configured quorum device always.
	// It is possible that an earlier quorum_open attempt
	// has initialized the pathname.
	// If so, we do not need to duplicate the string again.
	//
	if (devnamep == NULL) {
		// First open attempt
		devnamep = os::strdup(name);
	} else {
		CL_PANIC(os::strcmp(devnamep, name) == 0);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s)::quorum_open scrub(%d).\n", devnamep, scrub));

	if (qd_props.length() == 0) {
		if ((qerr = read_config_from_ccr()) != quorum::QD_SUCCESS) {
			oplock.unlock();
			return (qerr);
		}
	} else {
		//
		// There are two ways that the quorum_open function
		// can get extra information about the quorum
		// device. One is to read the information from the
		// CCR, this is the normal behavior. The other is in
		// the case that the device is not yet in the CCR, as
		// when the device is being scrubbed during QD add
		// from scconf. In that case, the information will be
		// packed up into the qd_props by the clconf.
		//

		// Get the properties list for the filer name and lun id.
		for (uint_t i = 0; i < qd_props.length(); i++) {
			if (os::strcmp(qd_props[i].name,
			    PROP_QDEV_IPADDR) == 0) {
				ipaddrp = os::strdup(qd_props[i].value);
			}
			if (os::strcmp(qd_props[i].name, PROP_QDEV_PORT) == 0) {
				port = (uint16_t)os::atoi(qd_props[i].value);
			}
			if (os::strcmp(qd_props[i].name,
			    PROP_QDEV_TIMEOUT) == 0) {
				timeout = (uint_t)os::atoi(qd_props[i].value);
			}
		}
	}

	//
	// Have to do some copying around because ipaddrp may have
	// been read from the CCR and may be pointing a valid value
	// already.
	//
	char tmp_ip[INET6_ADDRSTRLEN];
	if (get_ipaddress(ipaddrp, tmp_ip) != 0) {
		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);
		//
		// SCMSGS
		// @explanation
		// The quorum server hostname could not be resolved to an
		// address.
		// @user_action
		// Add the quorum server hostname to the /etc/hosts file,
		// /etc/inet/ipnodes file, or both. Verify that the settings
		// in the /etc/nsswitch.conf file include "files" for host
		// lookup.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Quorum server %s cannot be reached. "
		    "Check if qshost %s is specified in /etc/hosts file, "
		    "/etc/inet/ipnodes file, or both.",
		    devnamep, ipaddrp);
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("CMM: Quorum server %s cannot be reached. "
		    "Check if qshost %s is specified in /etc/hosts file, "
		    "/etc/inet/ipnodes file, or both.",
		    devnamep, ipaddrp));
		oplock.unlock();
		return (quorum::QD_EIO);
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("get_ipaddress returned %s\n", tmp_ip));
		delete ipaddrp;
		ipaddrp = os::strdup(tmp_ip);
	}

	//
	qerr = initialize();
	if (qerr != quorum::QD_SUCCESS) {
		oplock.unlock();
		return (qerr);
	}
	//
	quorum::quorum_error_t error = open_device(scrub);

	oplock.unlock();

	if (error == quorum::QD_SUCCESS) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("quorum_open returns %d\n", error));
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("quorum_open returns %d\n", error));
	}

	return (error);
}

//
// open_device
//
// Helper function for quorum_open. It initializes the socket
// connection with the Quorum Server and sends a quorum_open
// requests (forwarding the scrub option).
//
quorum::quorum_error_t
qd_quorum_server_impl::open_device(bool do_scrub)
{
	ASSERT(ipaddrp);
	ASSERT(oplock.lock_held());

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): ip=%s, port=%d, timeout=%d.",
	    devnamep, ipaddrp, port, timeout));

	//
	// Initialize socket connection for given IP address and port.
	//
	if (socket.set_values(devnamep, ipaddrp, port) != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("open_device got error from set_values\n"));
		return (quorum::QD_EIO);
	}

	msghdr->reinit(get_next_msgnum(), QD_OPEN);
	msghdr->op_data.scrub = do_scrub;

	wait_until_done_with_retries();

	quorum::quorum_error_t qerr = qs_to_quorum_error(msghdr->get_error());

	return (qerr);
}

//
// quorum_close
//
// This function sends a close message to the Quorum Server and
// then closes the current connection.
//
// A quorum device is closed when marked inaccessible after QD_EACCES,
// when old quorum info is unallocated during boot or ccr refresh, or
// after a successful quorum scrub by scconf/clconf.
//
// A quorum device is opened when a new quorum information is read
// during boot or ccr refresh, or when scconf/clconf initiates a
// quorum scrub.
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_close(Environment &)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): quorum_close.\n", devnamep));

	oplock.lock();

	//
	// msghdr can be null when quorum_open has returned with an error.
	// Hence check for this condition before using it.
	// If msghdr is null, then the socket would already be closed.
	//
	if (msghdr != NULL) {
		// Send close request to server
		msghdr->reinit(get_next_msgnum(), QD_CLOSE);
		wait_until_done_with_retries();

		socket.close();
	}

	//
	// The communication thread could exist regardless
	// of whether msghdr is NULL or not; in fact, quorum_remove()
	// deletes msghdr and sets it to NULL after doing its work,
	// but the communication thread is still present.
	// So we ensure that we make the communication thread exit here.
	//
	// The worker thread shouldn't still be doing any work.
	// Either it should be waiting for work (READY)
	// or it would not have started at all.
	// Ask the communication thread to exit.
	//
	timer_lock.lock();
	ASSERT(thr_state != RUN);
	if (thr_state == READY) {
		thr_state = STOP;
		timer_cv.signal();
		// Wait for the communication thread to exit.
		while (thr_state != DONE) {
			timer_cv.wait(&timer_lock);
		}
	}
	CL_PANIC(thr_state == DONE);	// ensure that thread is gone
	tid = 0;
	do_work = false;
	timer_lock.unlock();

	//
	// Free the memory pointed to by these pointers
	// and set these pointers to NULL.
	// We must set them to NULL after the free/delete operation;
	// else the destructor of this object will do the free/delete again.
	//
	delete clustername;
	clustername = NULL;
	delete ipaddrp;
	ipaddrp = NULL;
	delete msghdr;
	msghdr = NULL;

	//
	// The attribute 'data' is not deleted. It is used to point
	// to the optional data to be sent in the message if any.
	// Allocation/Free is done within the interface implementation.
	//
	data = NULL;

	port = 0;
	timeout = COMMUNICATION_TIMEOUT_DEFAULT;
	msgnum = 0;
	msgrecd = 0;
	clusterid = 0;
	send_data_len = 0;
	recv_data_len = 0;
	// Currently, there is no failfast for quorum server
	ff_enabled = false;

	oplock.unlock();
	return (quorum::QD_SUCCESS);
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster.
//
uint32_t
qd_quorum_server_impl::quorum_supports_amnesia_prot(Environment &)
{
	return (AMN_SC30_SUPPORT);
}

//
// quorum_reserve
//
// Makes a group reservation on a quorum device.
// Returns 0 on success and EACCES if "my_key" has not been registered.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key, Environment &)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_reserve\n", devnamep));

	quorum::quorum_error_t	qerr;

	oplock.lock();

	msghdr->reinit(get_next_msgnum(), QD_RESERVE);

	// send and recv data length is zero.
	msghdr->op_data.key = my_key;

	wait_until_done_with_retries();

	qerr = qs_to_quorum_error(msghdr->get_error());

	oplock.unlock();

	if (qerr == quorum::QD_SUCCESS) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QS(%s) quorum_reserve returning %d\n", devnamep, qerr));
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s) quorum_reserve returning %d\n", devnamep, qerr));
	}

	return (qerr);
}

//
// quorum_read_reservations
//
// Fills in "resv_list" with the reservations currently stored on a
// quorum device.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_read_reservations(
    quorum::reservation_list_t	&resv_list,
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_read_reservations called\n", devnamep));

	oplock.lock();

	msghdr->reinit(get_next_msgnum(), QD_READ_RESERVATIONS);

	quorum::quorum_error_t err = read_keys(msghdr, resv_list);

	oplock.unlock();

	return (err);
}

//
// quorum_preempt
//
// Removes a reservation key from a quorum device. This is used to
// preempt the reservation of another node when that node is declared
// down.
//
// If a NULL victim_key is passed in, then we have no work to do, so just
// return.
//
// Returns EACCES if "my_key" is not registered. Otherwise, 0 is
// returned to indicate that "victim_key" has been removed.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_preempt called\n", devnamep));

	oplock.lock();
	msghdr->reinit(get_next_msgnum(), QD_PREEMPT);

	msghdr->op_data.key = my_key;
	msghdr->op_data.victim_key = victim_key;

	wait_until_done_with_retries();

	quorum::quorum_error_t qerr = qs_to_quorum_error(
	    msghdr->get_error());

	if (qerr == quorum::QD_SUCCESS) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QS(%s) quorum_preempt ret=%d\n", devnamep, qerr));
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s) quorum_preempt ret=%d\n", devnamep, qerr));
	}

	oplock.unlock();

	return (qerr);
}

//
// quorum_register
//
// Associates a reservation key for the node with a quorum device.
// When a node dies, its key can be removed by the surviving cluster
// by using the quorum_preempt command. If the whole cluster is shut
// down or crashes, the keys stay with the quorum device even if the
// quorum device is power cycled.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_register called\n", devnamep));
	oplock.lock();

	msghdr->reinit(get_next_msgnum(), QD_REGISTER);
	msghdr->op_data.key = my_key;

	wait_until_done_with_retries();

	quorum::quorum_error_t qerr = qs_to_quorum_error(
	    msghdr->get_error());

	if (qerr == quorum::QD_SUCCESS) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QS(%s) quorum_register returning %d\n", devnamep, qerr));
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s) quorum_register returning %d\n", devnamep, qerr));
	}

	oplock.unlock();
	return (qerr);
}

//
// quorum_read_keys
//
// Fills in the registration list with the keys currently
// stored on a quorum device.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_read_keys(
    quorum::reservation_list_t		&reg_list,
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): quorum_read_keys\n", devnamep));

	oplock.lock();
	msghdr->reinit(get_next_msgnum(), QD_READ_KEYS);

	quorum::quorum_error_t err = read_keys(msghdr, reg_list);

	oplock.unlock();

	return (err);
}

//
// quorum_remove
//
// Remove the device.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_remove(const char *name,
    const quorum::qd_property_seq_t &qd_props,
    Environment			&)
{
	quorum::quorum_error_t	qerr = quorum::QD_SUCCESS;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_remove.\n", name));

	oplock.lock();

	delete devnamep;
	devnamep = os::strdup(name);

	if (qd_props.length() == 0) {
		if ((qerr = read_config_from_ccr()) != quorum::QD_SUCCESS) {
			oplock.unlock();
			return (qerr);
		}
	} else {
		// Get the properties list for the filer name and lun id.
		for (uint_t i = 0; i < qd_props.length(); i++) {
			if (os::strcmp(qd_props[i].name,
				PROP_QDEV_IPADDR) == 0) {
				ipaddrp = os::strdup(qd_props[i].value);
			}
			if (os::strcmp(qd_props[i].name, PROP_QDEV_PORT) == 0) {
				port = (uint16_t)os::atoi(qd_props[i].value);
			}
			if (os::strcmp(qd_props[i].name,
				PROP_QDEV_TIMEOUT) == 0) {
				timeout = (uint_t)os::atoi(qd_props[i].value);
			}
		}
	}

	//
	// Have to do some copying around because ipaddrp may have
	// been read from the CCR and may be pointing a valid value
	// already.
	//
	char tmp_ip[INET6_ADDRSTRLEN];
	if (get_ipaddress(ipaddrp, tmp_ip) != 0) {
		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Quorum server %s cannot be reached. "
		    "Check if qshost %s is specified in /etc/hosts file, "
		    "/etc/inet/ipnodes file, or both.",
		    devnamep, ipaddrp);
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("CMM: Quorum server %s cannot be reached. "
		    "Check if qshost %s is specified in /etc/hosts file, "
		    "/etc/inet/ipnodes file, or both.",
		    devnamep, ipaddrp));
		oplock.unlock();
		return (quorum::QD_EIO);
	} else {
		delete ipaddrp;
		ipaddrp = os::strdup(tmp_ip);
	}

	qerr = initialize();
	if (qerr != quorum::QD_SUCCESS) {
		oplock.unlock();
		return (qerr);
	}

	ASSERT(ipaddrp);

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): ip=%s, port=%d, timeout=%d\n", devnamep, ipaddrp,
	    port, timeout));

	// this will open the socket
	if (socket.set_values(devnamep, ipaddrp, port) != 0) {
		oplock.unlock();
		return (quorum::QD_EIO);
	}

	msghdr->reinit(get_next_msgnum(), QD_REMOVE);

	wait_until_done_with_retries();

	qerr = qs_to_quorum_error(msghdr->get_error());

	//
	// We can close the socket now...
	// scconf remove -> clconf_quorum_remove -> quorum_remove()
	// clconf_quorum_remove does not issue a quorum_close().
	//
	// The server does not expect a close message
	// after a remove.
	//
	// Close Socket
	//
	socket.close();

	//
	// Also delete msghdr so that quorum_close() called, when
	// this object gets _unreferenced(), does not attempt
	// to send a close message to the quorum server daemon.
	//
	delete msghdr;
	msghdr = NULL;

	oplock.unlock();
	return (qerr);
}

//
// quorum_enable_failfast
//
// Turns on failfast monitoring.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_enable_failfast(
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_enable_failfast\n", devnamep));

	if (ff_enabled) {
		return (quorum::QD_SUCCESS);
	}
	// Panic the node if we have been pre-empted
	ff_enabled = true;
	//
	// Create thread that polls the QS. It calls read_reservation
	// and if this node's key is missing, the thread panics the
	// node.
	//
	// Optionally, in addition to polling, we can have the QS send
	// an Out-Of-Band message to this node if the this node has been
	// preempted.
	//
	return (quorum::QD_SUCCESS);
}

//
// quorum_reset
//
// This quorum API method is provided as an access point
// for scsi specific bus reset operation. We do not have
// anything to do for Quorum Server.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_reset(
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) quorum_reset called.\n", devnamep));
	return (quorum::QD_SUCCESS);
}

//
// quorum_data_read
//
// Read a particular block of data from the device.
// Nothing to do for Quorum Server.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_data_read(
	uint64_t,
	CORBA::String_out,
	Environment		&)
{
	return (quorum::QD_SUCCESS);
}

//
// quorum_data_write
//
// Write a block of data on the device.
// Nothing to do for Quorum Server.
//
quorum::quorum_error_t
qd_quorum_server_impl::quorum_data_write(
	uint64_t,
	const char 		*,
	Environment		&)
{
	return (quorum::QD_SUCCESS);
}

//
// read_config_from_ccr
//
// Internal Utility methods.
// Read the current quorum server device configuration from
// CCR database, among which the Quorum Server address and port,
// and the communication timeout.
//
quorum::quorum_error_t
qd_quorum_server_impl::read_config_from_ccr()
{
	clconf_cluster_t *clconf = clconf_cluster_get_current();
	clconf_iter_t	*currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t	*qdev = NULL;
	const char	*portp = NULL;

	// Iterate over the quorum devices to find this one.
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {
		const char *name = clconf_obj_get_name(qdev);
		if (name != NULL && os::strcmp(name, devnamep) == 0) {
			break;
		}
		clconf_iter_advance(currqdevsi);
	}
	if (qdev == NULL) {
		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);
		//
		// SCMSGS
		// @explanation
		// In order to configure a quorum server as a quorum device,
		// the module must be able to read certain information from
		// the CCR. Because the necessary information in unavailable,
		// the device will be in a failed state.
		// @user_action
		// Remove the device using scconf, and try adding it again.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Could not find device information in "
		    "CCR. Quorum device %s will be unavailable.", devnamep);
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("CMM: Could not find device information in "
		    "CCR. Quorum device %s will be unavailable.", devnamep));
		return (quorum::QD_EIO);
	}

	// Get the ipaddress and port number from CCR
	const char *valuep = clconf_obj_get_property(qdev, PROP_QDEV_IPADDR);
	if (valuep) {
		ipaddrp = os::strdup(valuep);
		if (ipaddrp == NULL) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("read_config_from_ccr: Failed to "
			    "allocate memory for clustername."));
			return (quorum::QD_EIO);
		}
	}

	portp = clconf_obj_get_property(qdev, PROP_QDEV_PORT);

	if (qdevsi != (clconf_iter_t *)0) {
		clconf_iter_release(qdevsi);
	}

	if ((portp == NULL) || (ipaddrp == NULL)) {
		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Could not find device information in "
		    "CCR. Quorum device %s will be unavailable.", devnamep);
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("CMM: Could not find device information in "
		    "CCR. Quorum device %s will be unavailable.", devnamep));
		return (quorum::QD_EIO);
	} else {
		port = (uint16_t)os::atoi(portp);
	}

	// Get the timeout string if it is specified.
	const char *timeout_string =
		clconf_obj_get_property(qdev, PROP_QDEV_TIMEOUT);
	if (timeout_string == NULL) {
		// No timeout specified, so default to 30 seconds.
		timeout = communication_timeout_default;
	} else {
		timeout = (uint_t)os::atoi(timeout_string);
	}

	return (quorum::QD_SUCCESS);
}

//
// get_next_msgnum
//
// Returns a new message number.
//
uint32_t
qd_quorum_server_impl::get_next_msgnum()
{
	uint32_t cur_num = ++msgnum;
	if (msgnum == 0) {
		if (msgrecd == 0) {
			//
			// We have'nt received reply for the 0th
			// message we sent.  and we have sent 2^32-1
			// messages since then.  No point in sending
			// more messages.
			//
			CL_PANIC(0);
		}
	}
	return (cur_num);
}

//
// is_valid_reply
//
// Check if the reply we received is consistent with the
// request that was sent to the Quorum Server. Among the
// parameters received, we check the cluster id, node incarnation
// number, node id, message number, version and cookie.
//
// Also, if we actually received a reply, then the error
// status should not be invalid.
//
bool
qd_quorum_server_impl::is_valid_reply()
{
	if ((msghdr->get_cluster_id() != clusterid) ||
	    (msghdr->get_incn() != orb_conf::local_incarnation()) ||
	    (msghdr->get_nid() != orb_conf::local_nodeid()) ||
	    (msghdr->get_msg_num() != msgnum) ||
	    (msghdr->get_version_major() !=
		(uint16_t)SCQSD_VERSION_MAJOR) ||
	    (msghdr->get_version_minor() !=
		(uint16_t)SCQSD_VERSION_MINOR) ||
	    (!msghdr->is_cookie_set()) ||
	    (msghdr->get_error() == QD_QS_INVALID)) {
		return (false);
	}
	return (true);
}

//
// wait_until_done_with_retries
//
// This function handles a remote communication call to the Quorum Server.
// First, the socket communication is established, if needed. Then, it signals
// worker thread that there is a message to process (i.e., send and wait
// for reply). Then it waits (with retries) for the operation completion
// or time-out.
//
void
qd_quorum_server_impl::wait_until_done_with_retries()
{
	uint_t current_attempt = 1;
	os::systime sys_timeout;
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s): wait_until_done_with_retries called\n", devnamep));

	//
	// Rules and expectations for socket state...
	//
	// Sockets are opened in the following scenarios:
	//
	// - quorum_open call
	// - quorum_remove call
	// - wait_until_done_with_retries reconnect after a failure
	// - wait_until_done_with_retries called while there was no
	// current connection (e.g., because previously quorum_close
	// had been called after reception of a QD_EIO error))
	//
	// Sockets are closed in the following scenarios:
	//
	// - quorum_open (following a QD_EIO error)
	// - quorum_close
	// - quorum_remove
	// - wait_until_done_with_retries (TIMEOUT | QD_EIO | invalid)
	//
	// quorum_close() sends a close request to server.
	//
	if (!socket.is_connected()) {
		if (socket.reopen() != 0) {
			msghdr->set_error(QD_QS_EIO);
			return;
		}
	}

	timer_lock.lock();
	//
	// Make sure commnunication thread is ready.
	//
	ASSERT((thr_state != STOP) && (thr_state != DONE));
	while (thr_state != READY) {
		timer_cv.wait(&timer_lock);
	}
	thr_state = RUN;
	//
	// msghdr has been setup. signal the worker_thread to
	// communicate with the QS. Start a timer and wait until the
	// operation has completed or the timer expires.
	//
	do_work = true;
	timer_cv.signal();
	sys_timeout.setreltime((long)timeout * MICROSEC);
	int error = timer_cv.timedwait(&timer_lock, &sys_timeout);
	//
	// Send/Receive operation status:
	//
	// - Interrupted (TIMEOUT)
	// - Failed to send/receive (invalid reply)
	// - Operation failed (QD_QS_EIO)
	// - Successful operation (QD_QS_EACCES|QD_QS_SUCCESS|QD_QS_ENOENT)
	//
	while (((error == os::condvar_t::TIMEDOUT) ||
	    (!is_valid_reply()) ||
	    (msghdr->get_error() == QD_QS_EIO)) &&
	    (current_attempt < communication_max_attempt)) {
		//
		// Check status of previous communication attempt
		// and retry if appropriate.
		//
		if (!check_error_and_retry_communication(error,
		    current_attempt, sys_timeout)) {
			break;
		}
	}
	//
	// At this point communication is either a success
	// or a failure (after retries).
	//

	//
	// If we timed out on last communication retry.
	//
	if ((error == os::condvar_t::TIMEDOUT) && (do_work)) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("QS(%s): op timed out. Sending SIGINT to %x.\n",
		    devnamep, tid));
		do_work = false;
		clsendlwpsig(tid);
	}

	// Wait for the worker thread to go to sleep waiting for more work
	ASSERT((thr_state != STOP) && (thr_state != DONE));
	while (thr_state != READY) {
		timer_cv.wait(&timer_lock);
	}

	//
	// Log a message in case we attempted to access
	// a Quorum Device for which the Quorum Server
	// has no configuration information.
	//
	if (msghdr->get_error() == QD_QS_ENOENT) {
		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);
		//
		// SCMSGS
		// @explanation
		// The quorum device is unknown to the quorum server. This
		// quorum device may have been unconfigured on the quorum
		// server.
		// @user_action
		// Remove the device using scconf, and try adding it again.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Quorum device %s does not appear "
		    "to be configured on quorum server. "
		    "This quorum device will be unavailable.", devnamep);
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("CMM: Quorum device %s does not appear "
		    "to be configured on quorum server. "
		    "This quorum device will be unavailable.", devnamep));
	}
	//
	// Failures to send and receive (time-out without result,
	// or send/receive error, or protocol error) are mapped to
	// a quorum interface EIO.
	//
	if (((error == os::condvar_t::TIMEDOUT) && (do_work)) ||
	    (!is_valid_reply()) ||
	    (msghdr->get_error() == QD_QS_EIO) ||
	    (msghdr->get_error() == QD_QS_ENOENT)) {
		//
		// We could include additional wait to allow
		// the worker thread to set error number in
		// msghdr. but that will be just wasteful.
		//
		msghdr->set_error(QD_QS_EIO);
		do_work = false;
		socket.close();
	}
	timer_lock.unlock();
}

//
// check_error_and_retry_communication
//
// This function is a helper for wait_until_done_with_retries().
// It checks the previous communication attempt status and if
// needed, makes a new communication attempt with the quorum server.
//
// This function returns false if no further attempt to communicate
// should be done (communication succeeded or reconnection failed).
// It returns true otherwise.
//
bool
qd_quorum_server_impl::check_error_and_retry_communication(int &error,
    uint_t &current_attempt, os::systime &sys_timeout)
{
	// Should be holding the timer lock
	ASSERT(timer_lock.lock_held());

	if ((!do_work) && (is_valid_reply()) &&
	    (msghdr->get_error() != QD_QS_EIO)) {
		//
		// Timer expired but communication succeeded.
		// We are done.
		//
		error = 0;
		return (false);
	} else if (error == os::condvar_t::TIMEDOUT) {
		//
		// Timer expired before communication done.
		// We interrupt the communication thread.
		// We will try again.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("QS(%s): op timed out. "
		    "Sending SIGINT to %x.\n",
		    devnamep, tid));
		do_work = false;
		clsendlwpsig(tid);
	} else if (!is_valid_reply()) {
		//
		// Communication actually failed.
		// The reply message is not valid.
		// We will try again.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s): invalid reply...\n", devnamep));
		do_work = false;
	} else if (msghdr->get_error() == QD_QS_EIO) {
		//
		// We got IO error as a reply.
		// We will try again.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s): got IO error...\n", devnamep));
		do_work = false;
	}
	//
	// Wait for the worker thread to go to sleep waiting
	// for more work, and then  attempt to reconnect.
	//
	ASSERT((thr_state != STOP) && (thr_state != DONE));
	while (thr_state != READY) {
		timer_cv.wait(&timer_lock);
	}
	timer_lock.unlock();
	socket.close();

	if (socket.reopen() != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("QS(%s): failed to re-open socket. "
		    "Giving up.\n", devnamep));
		timer_lock.lock();
		return (false);
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("QS(%s): re-opened socket, "
		    "Retrying call\n", devnamep));
		current_attempt++;
	}
	//
	// Make sure commnunication thread is ready.
	//
	timer_lock.lock();
	ASSERT(thr_state == READY);
	thr_state = RUN;
	//
	// Prepare message for resending
	// (same msg number, same type, same error).
	//
	msghdr->set_length(0);
	//
	// Retrying communication.
	//
	do_work = true;
	timer_cv.signal();
	sys_timeout.setreltime((long)timeout * MICROSEC);
	error = timer_cv.timedwait(&timer_lock, &sys_timeout);
	return (true);
}

//
// read_keys
//
// Helper function for quorum keys read operations. This
// function sends a read_key request to the Quorum Server
// and returns back the key list.
//
quorum::quorum_error_t
qd_quorum_server_impl::read_keys(qd_qs_msghdr *hdrp,
    quorum::reservation_list_t	&key_list)
{
	quorum::quorum_error_t qerr = quorum::QD_SUCCESS;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("QS(%s) read_keys: requesting %d\n", devnamep, key_list.listsize));
	//
	// send data length is ZERO.
	// key list size set by set_data_length()
	// recv data length is num_keys requested * size of key
	//
	if (key_list.listsize == 0) {
		return (quorum::QD_SUCCESS);
	}

	hdrp->set_data_length(key_list.listsize);

	quorum::registration_key_t *keys =
	    new quorum::registration_key_t[key_list.listsize];

	recv_data_len = (uint32_t)sizeof (quorum::registration_key_t) *
		key_list.listsize;

	data = (void *)keys;

	wait_until_done_with_retries();

	qerr = qs_to_quorum_error(msghdr->get_error());

	if (qerr == quorum::QD_SUCCESS) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("QS(%s) read_keys: actual num_keys=%d\n",
		    devnamep, hdrp->get_data_length()));

		// Copy the keys back to caller
		unsigned int copy_keys_recd = 0;
		if (key_list.listsize <= hdrp->get_data_length()) {
			copy_keys_recd = key_list.listsize;
		} else {
			copy_keys_recd = hdrp->get_data_length();
		}

		for (uint_t i = 0; i < copy_keys_recd; i++) {
			key_list.keylist[i] = keys[i];
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("keys[%d] = 0x%llx\n", i, key_value(keys[i])));
		}
		// Return the actual number of keys
		key_list.listlen = hdrp->get_data_length();
	}

	delete keys;

	if (qerr != quorum::QD_SUCCESS) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		("QS(%s) read_keys ret=%d\n", devnamep, qerr));
	} else {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		("QS(%s) read_keys ret=%d\n", devnamep, qerr));
	}

	return (qerr);
}


//
// do_door_call
//
// Invokes the given cmd line and returns door_srg.
// Caller must free the door_arg.
//
int
do_door_call(char *cmd_line, door_arg_t **darg)
{
	*darg = new door_arg_t;
	if (!*darg) {
		CMM_TRACE(("Failed to allocate memory\n"));
		return (1);
	}

	(*darg)->data_ptr = cmd_line;
	(*darg)->data_size = strlen(cmd_line) + 1;
	(*darg)->desc_ptr = NULL;
	(*darg)->desc_num = 0;

	//
	// The door arg needs to have a return buffer, because if it doesn't,
	// the call will fail (with EINTR). The return buffer also needs to
	// be 64-bit aligned in 64-bit kernels. However, if the buffer is
	// too small, the door call will automatically resize it. So, we
	// allocate a ulong_t, which is guaranteed to be aligned, and then
	// use it as the buffer. In most cases, this will be resized and
	// deallocated by the call.
	//
	ulong_t retbuf;
	(*darg)->rbuf = (char*) (&retbuf);
	(*darg)->rsize = sizeof (ulong_t);

	int error = 0;
	door_handle_t door_handle;

	error = door_ki_open(QD_USERD_DOOR, &door_handle);
	if (error != 0) {
		CMM_TRACE(("door_ki_open returned error %d\n", error));
		return (error);
	}
	//
	// Mask signals before doing the door_ki_upcall.
	// The only signal we'll allow is SIGINT, which the main quorum
	// thread will use to wake this thread up if the user process hangs.
	//
	k_sigset_t new_mask, old_mask;
	sigfillset(&new_mask);
	sigdelset(&new_mask, SIGINT); /*lint !e572 !e778 */
	sigreplace(&new_mask, &old_mask);

	// Make the actual door upcall.
	error = door_ki_upcall(door_handle, (*darg));
	if (error != 0) {
		CMM_TRACE(("internal_door_call returned error %d\n",
		    error));
		delete darg;
		sigreplace(&old_mask, NULL);
		return (error);
	}

	// Restore the thread signal property.
	sigreplace(&old_mask, NULL);
	return (error);
}

//
// worker_thread
//
void
qd_quorum_server_impl::worker_thread(void *arg)
{
	qd_quorum_server_impl *qdp = (qd_quorum_server_impl *)arg;
	qdp->work();
}

//
// work
//
// Worker thread main loop. Thread handles messages send/receive
// requests to/from quorum server daemon.
//
void
qd_quorum_server_impl::work()
{
	timer_lock.lock();
	tid = os::threadid();
	thr_state = READY;
	timer_cv.signal();
	while ((thr_state == RUN) || (thr_state == READY)) {
		while ((!do_work) &&
		    ((thr_state == RUN)|| (thr_state == READY))) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("worker_thread(%s): Waiting for work.\n",
			    devnamep));
			timer_cv.wait(&timer_lock);
		}
		if (thr_state == STOP) {
			break;
		}
		//
		// do_work = true. Release this lock so that
		// wait_until_done_with_retries() can grab it
		// to signal us if the timer expires.
		//
		timer_lock.unlock();
		//
		// Other thread is timing us...
		// In the end, either
		//
		// - We are interrupted
		// - We failed to send (QD_QS_INVALID|QD_QS_EIO)
		// - The operation failed (QD_QS_EIO)
		// - Operation succeeds (QD_EACCES|QD_SUCCESS|QD_QS_ENOENT)
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("worker_thread(%s): Got work to do.\n", devnamep));

		if (socket.send(msghdr, (send_data_len) == 0 ? NULL : data)
		    != 0) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("worker_thread(%s): failed to send.\n", devnamep));
			msghdr->set_error(QD_QS_EIO);
		} else {
			//
			// Getting around 6319145 sockfs...
			//
			// Server socket being closed can lead
			// recv to appear to succeed...
			//
			// error will be reset to QD_QS_SUCCESS
			// or QD_QS_EACCES if call is succesful
			//
			msghdr->set_error(QD_QS_INVALID);
			//
			//
			//
			msghdr->length = recv_data_len;
			int ret = socket.recv(0, msghdr, data);
			if (ret != 0) {
				CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
				    ("worker_thread(%s): recv data "
				    "failed.\n", devnamep));
				msghdr->set_error(QD_QS_EIO);
			} else if (recv_data_len != 0) {
				CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
				    ("worker_thread(%s): recv data length %d\n",
				    devnamep, msghdr->length));
			}
		}
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("worker_thread(%s): msghdr->error = %d.\n",
		    devnamep, msghdr->get_error()));

		timer_lock.lock();
		if (!do_work) {
			//
			// If sorecvmsg is interrupted, it still
			// returns error = 0 as per this code from
			// uts/common/fs/sockfs/socktpi.c.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("worker_thread(%s): interrupted.\n", devnamep));
			clclearlwpsig();
		} else {
			//
			// Else we finished before the timer expired.
			// We signal the quorum operation thread.
			//
			CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
			    ("worker_thread(%s): work done.\n", devnamep));
			do_work = false;
		}
		thr_state = READY;
		timer_cv.signal();
	}
	thr_state = DONE;
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("worker_thread(%s): exiting.\n", devnamep));
	timer_cv.signal();
	timer_lock.unlock();
}

//
// get_ipaddress
//
// This function takes an hostname as parameter and returns back the
// IP address fopr that host. In order to do so, it uses the service
// of a user land helper command.
//
int
get_ipaddress(char *hostname, char *ip)
{
	qs_inaddr_t num_ipaddr;
	if (inet_pton(AF_INET6, (char *)hostname, (void *)&num_ipaddr) == 1) {
		(void) os::strcpy(ip, hostname);
		return (0);
	}

	if (inet_pton(AF_INET, (char *)hostname, (void *)&num_ipaddr) == 1) {
		(void) os::strcpy(ip, hostname);
		return (0);
	}
	// Else this is a hostname string and need to find the ipaddress.
	char		cmd_line[256];
	door_arg_t	*dargp = NULL;

	os::sprintf(cmd_line, "/usr/cluster/lib/sc/qd_qs_helper -i %s ",
	    hostname);
	int error = do_door_call(cmd_line, &dargp);
	if (error != 0) {
		return (error);
	}

	//
	// Cast to int because we do not expect the return codes to be
	// 64-bit values.
	//
	error = (int)((qd_userd_ret_t *)dargp->data_ptr)->cmd_ret;
	// Check if qd_qs_helper failed to for any reason
	if (os::strncmp("FAIL:", ((qd_userd_ret_t *)dargp->data_ptr)->full_buf,
		(unsigned long)5) == 0) {
		error = 1;
		ip[0] = 0;
	} else {
		(void) os::strcpy(ip,
		    ((qd_userd_ret_t *)dargp->data_ptr)->full_buf);
		char *pos = os::strstr(ip, ",");
		if (pos == NULL) {
			error = 1;
			ip[0] = 0;
		} else {
			*pos = '\0';
		}
	}

	delete dargp;
	return (error);
}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
//
// check_fi
//
// Function to detect if the function should return an
// error as part of fault injection testing.
//
int
qd_quorum_server_impl::check_fi(char	*qop_name)
{
	// Fault point will return failure
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    &fault_argp, &fault_argsize)) {
		char *exp_path = (char *)fault_argp;
		const char *cur_path = devnamep;
		if ((exp_path == NULL) ||
		    ((cur_path != NULL) && (strcmp(cur_path, exp_path) == 0))) {
			os::printf("==> Fault Injection failing %s operation\n",
			    qop_name);
			return (quorum::QD_EIO);
		}
	}
	return (0);
}
#endif // FAULT_CMM && _KERNEL_ORB
