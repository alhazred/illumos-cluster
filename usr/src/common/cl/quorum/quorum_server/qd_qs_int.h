/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	QD_QS_INT_H
#define	QD_QS_INT_H

#pragma ident	"@(#)qd_qs_int.h	1.11	08/05/20 SMI"

#define	SCQD_SCCONF_QD_TYPE	"quorum_server"
#define	PROP_QDEV_IPADDR	"qshost"
#define	PROP_QDEV_PORT		"port"
#define	PROP_QDEV_SECKEY	"secure"
#define	PROP_QDEV_TIMEOUT	"timeout"

#define	DEFAULT_TIMEOUT_MIN	1
#define	DEFAULT_TIMEOUT_MAX	10000

#ifdef _KERNEL

#include <cmm/cmm_debug.h>

#define	STRDUP	os::strdup
#define	STRSTR	os::strstr
#define	STRCPY	os::strcpy
#define	STRCMP	os::strcmp

#else

#define	CMM_TRACE(a)	printf##a
#define	STRDUP		strdup
#define	STRSTR		strstr
#define	STRCPY		strcpy
#define	STRCMP		strcmp

#endif

#endif // QD_QS_H
