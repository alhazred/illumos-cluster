//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qd_qs_int.cc	1.5	08/05/20 SMI"

#include <quorum/quorum_server/qd_qs_int.h>
#include <orb/infrastructure/orb_conf.h>

qd_qs_msghdr::qd_qs_msghdr() :
	msgnum(0),
	type(0),
	length(0),
	error(0),
	src_node(orb_conf::current_node)
{

}

qd_qs_msghdr::qd_qs_msghdr(const qd_qs_msghdr &msg_header)
{
	CL_PANIC(msgnum == msg_header.msgnum);
	CL_PANIC(type == msg_header.type);
	CL_PANIC(src_node == msg_header.src_node);
	error = msg_header.error;
}
