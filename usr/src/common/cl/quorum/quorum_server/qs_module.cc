//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qs_module.cc	1.9	08/09/02 SMI"

#include <cmm/cmm_debug.h>
// Included for cplpl_init
#include <cplplrt/cplplrt.h>

#include <quorum/common/type_registry.h>

//
// Static accessor function to the constructor which is used by the
// type registry to modlookup.
//
extern quorum::quorum_device_type_ptr
    get_qd_quorum_server_impl(cmm_trace_level);

//
// Functions to support module load and linkage.
//
static struct modlmisc modlmisc = {
	&mod_miscops, "QS device type module"
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL },
};

char _depends_on[] =
	"misc/cl_runtime"	// C++ runtime
	" misc/cl_load"		// Clustering
	" misc/cl_orb"
	" misc/cl_haci"
	" misc/cl_quorum"
	" misc/cl_comm"
	" fs/specfs"
	" fs/sockfs"		// socket filesystem
	" strmod/ip"		// inet_pton
	;

int
_init()
{
	int error = 0;
	if ((error = mod_install(&modlinkage)) != 0) {
		CMM_TRACE(("ERROR: could not mod_install\n"));
	} else {
		CMM_TRACE(("QS: module loaded successfully\n"));
	}

	_cplpl_init();		/* C++ initialization */

	device_type_registry_impl::register_device_type("quorum_server",
	    get_qd_quorum_server_impl);


	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
