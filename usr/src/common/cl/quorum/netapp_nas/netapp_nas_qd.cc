//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)netapp_nas_qd.cc	1.25	09/04/29 SMI"

#include <sys/modctl.h>
#include <h/quorum.h>
#include <quorum/common/type_registry.h>
#include <orb/infrastructure/orb_conf.h>
#include <quorum/netapp_nas/netapp_nas_qd.h>
#include <cmm/cmm_debug.h>
#include <quorum/common/quorum_pgre.h>
#include <nslib/ns.h>
#include <quorum/common/quorum_util.h>
#include <sys/clconf_int.h>
#include <quorum/common/quorum_impl.h>
#include <sys/qd_userd.h>

// Included for file ioctls.
#include <sys/vnode.h>

// Included for signal passing
#include <orb/infrastructure/clusterproc.h>

// Included for cplpl_init
#include <cplplrt/cplplrt.h>

#define	TIMEOUT_DEFAULT	30

//
// The following is used to convert the strings representing the keys
// to hex numbers.
//
#ifndef DIGIT
#define	    NUM2DIG(c)	((c) - '0')	/* numeric */
#define	    LCA2DIG(c)	((c) + 10 - 'a')    /* lower case alpha */
#define	    UCA2DIG(c)	((c) + 10 - 'A')    /* upper case alpha */

#define	    DIGIT(c)	(os::ctype::is_digit(c) ? NUM2DIG(c) :	\
		(os::ctype::is_lower(c) ? LCA2DIG(c) : UCA2DIG(c)))
#endif	/* DIGIT */

// static

quorum::quorum_device_type_ptr
get_netapp_nas_qd_impl(cmm_trace_level level) {
	return ((new quorum_device_netapp_nas_impl(level))->get_objref());
}

//
// execute_door_call_thread
//
// Function argument to thread_create. This does some reference work and
// then calls the door_call_thread() function which can access local
// (non-static) variables.
//
void
quorum_device_netapp_nas_impl::execute_door_call_thread(void *arg)
{
	quorum_device_netapp_nas_impl *obj =
	    (quorum_device_netapp_nas_impl *)arg;
	obj->door_call_thread();
}

//
// door_call_thread
//
// This is the main function for the thread which will execute the
// ki_door_upcall. The function waits until signalled by the cmd_cv,
// then processes, then signals the ret_cv.
//
void
quorum_device_netapp_nas_impl::door_call_thread()
{
	lck.lock();
	door_call_thread_id = os::threadid();

	// Wake up any threads in quorum_open that are waiting.
	ret_cv.signal();

	while (!stop_door_call_thread) {
		// Wait until there is a command ready for execution.
		cmd_cv.wait(&lck);

		//
		// The _unreferenced call uses the cmd_cv to signal the
		// door call thread to wake up and die, so we need to check
		// the stop condition again, otherwise we would issue an
		// unnecessary call to internal_door_upcall.
		//
		if (stop_door_call_thread) {
			break;
		}

		// Pass the command to the qd_userd.
		internal_door_upcall();

		// Signal the quorum thread that there is data available.
		ret_cv.signal();

		//
		// In case the thread was sent SIGINT to cancel, clear
		// signals.
		//
		clclearlwpsig();
	}

	//
	// Reset the thread id as an indicator to any thread waiting
	// in quorum_close for this door call thread to exit,
	// and then wake up the thread waiting in quorum_close.
	//
	door_call_thread_id = (os::threadid_t)0;
	ret_cv.signal();
	lck.unlock();
}

//
// internal_door_upcall
//
// This function does the actual work of the call to the qd_userd process.
// The cmd_line to pass to the qd_userd should already be writing in cmd_line,
// and this function should write the result of the command to cmd_ret.
//
void
quorum_device_netapp_nas_impl::internal_door_upcall()
{

	ASSERT(cmd_line[0] != '\0');

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("internal_door_upcall for cmd = \n%s\n", cmd_line));

	door_arg_t *darg;
	darg = new door_arg_t;
	darg->data_ptr = cmd_line;
	darg->data_size = strlen(cmd_line) + 1;
	darg->desc_ptr = NULL;
	darg->desc_num = 0;

	//
	// The door arg needs to have a return buffer, because if it doesn't,
	// the call will fail (with EINTR). The return buffer also needs to
	// be 64-bit aligned in 64-bit kernels. However, if the buffer is
	// too small, the door call will automatically resize it. So, we
	// allocate a ulong_t, which is guaranteed to be aligned, and then
	// use it as the buffer. In most cases, this will be resized and
	// deallocated by the call.
	//
	ulong_t retbuf;
	darg->rbuf = (char*) (&retbuf);
	darg->rsize = sizeof (ulong_t);

	door_error = quorum::QD_SUCCESS;
	int error = 0;

	//
	// Mask signals before doing the door_ki_upcall.
	// The only signal we'll allow is SIGINT, which the main quorum
	// thread will use to wake this thread up if the user process hangs.
	//
	k_sigset_t new_mask, old_mask;
	sigfillset(&new_mask);
	sigdelset(&new_mask, SIGINT); /*lint !e572 !e778 */
	sigreplace(&new_mask, &old_mask);

	// Make the actual door upcall.
	error = door_ki_upcall(door_handle, darg);
	if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): internal_door_call"
		    "returned error %d\n", pathnamep, door_error));
		delete darg;
		door_error = quorum::QD_EIO;
		sigreplace(&old_mask, NULL);
		return;
	}

	// Restore the thread signal property.
	sigreplace(&old_mask, NULL);

	// CMM_TRACE(("data_ptr is \n%s\n", (char *)darg->data_ptr));

	// Copy the output from the door call into the return buffer.
	os::sprintf(cmd_ret, "%s",
	    ((qd_userd_ret_t *)darg->data_ptr)->full_buf);

	//
	// Check to make sure that the string "Test completed successfully"
	// appeared in the output. If it didn't, then there was some error,
	// and we'll mark that here so that we don't need to check in every
	// function that calls this.
	//
	if (os::strstr(cmd_ret, "RESERVATION CONFLICT") != NULL) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("door_call got reservation conflict.\n"));
		door_error = quorum::QD_EACCES;
	} else if (os::strstr(cmd_ret, "Test completed successfully") == NULL) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("String %s not found in string %s\n",
		    "Test completed successfully", cmd_ret));
		door_error = quorum::QD_EIO;
	}

	delete darg;
}

//
// Constructor
//
// This is mostly meaningless. We do all the actual construction as part
// of the quorum_open call.
//
quorum_device_netapp_nas_impl::quorum_device_netapp_nas_impl(
    cmm_trace_level level) :
    door_call_thread_id((os::threadid_t) 0),
    door_error(quorum::QD_SUCCESS),
    stop_door_call_thread(false),
    pathnamep(NULL),
    inamep(NULL),
    filer_namep(NULL),
    lun_idp(NULL),
    timeout(0),
    door_handle((door_handle_t)0),
    failfast_enabled(false),
    trace_level(level)
{
	bzero(cmd_line, sizeof (cmd_line));
	bzero(cmd_ret, sizeof (cmd_ret));
}

//
// Default constructor.
// Should not be called.
//
// Lint warns that some class members are not initialized by constructor.
//lint -e1401
quorum_device_netapp_nas_impl::quorum_device_netapp_nas_impl()
{
	ASSERT(0);
}
//lint +e1401

//
// Destructor
//
quorum_device_netapp_nas_impl::~quorum_device_netapp_nas_impl()
{
	delete [] pathnamep;
	pathnamep = NULL;

	//
	// Either the following strings wouldn't have been allocated at all
	// or they have been freed in quorum_close().
	//
	ASSERT(inamep == NULL);
	ASSERT(filer_namep == NULL);
	ASSERT(lun_idp == NULL);
}

//
// Unreferenced
//
void
#ifdef DEBUG
quorum_device_netapp_nas_impl::_unreferenced(unref_t cookie)
#else
quorum_device_netapp_nas_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s)::_unreferenced.\n", pathnamep));

	//
	// Quorum objects are instantiated in the kernel
	// while the callers instantiating those objects can reside
	// in kernel or userland. If a userland client dies
	// after calling quorum_open() but before calling quorum_close(),
	// then the sole reference held by the client for the quorum
	// object is gone without a call to quorum_close().
	//
	// So we call quorum_close() here to clean-up state.
	// quorum_close() is implemented to be idempotent
	// and hence it is safe to call quorum_close() here.
	//
	// This object is going away; so ignore return value
	// and exception from quorum_close().
	//
	Environment e;
	(void) quorum_close(e);
	e.clear();

	delete this;
}

//
// Function to trigger the door call execution and deal with timeouts.
// Must be called with lock held and return with lock held.
//
void
quorum_device_netapp_nas_impl::trigger_door_call_with_timeouts(char *errstrp)
{
	os::systime	sys_timeout;
	os::usec_t	max_delay = timeout * 1000000; /*lint !e647 !e713 */
	sys_timeout.setreltime(max_delay);

	cmd_cv.signal();
	if (ret_cv.timedwait(&lck, &sys_timeout) == os::condvar_t::TIMEDOUT) {
		//
		// The door operation did not complete within the timeout.
		// Set EIO error.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): %s door call timed out. Aborting "
		    "QD access.\n", pathnamep, errstrp));
		clsendlwpsig(door_call_thread_id);
		door_error = quorum::QD_EIO;
	}

}

//
// IDL method to set the message tracing level of this quorum device
//
void
quorum_device_netapp_nas_impl::set_message_tracing_level(
    uint32_t new_trace_level, Environment &)
{
	trace_level = (cmm_trace_level)new_trace_level;
}

//
// IDL method to get the message tracing level of this quorum device
//
uint32_t
quorum_device_netapp_nas_impl::get_message_tracing_level(Environment &)
{
	return ((uint32_t)trace_level);
}

//
// quorum_open
//
// Opens a quorum device by a local path name and gets a handle
// to the door. This handle is used in all other calls.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_open(const char *devname,
    const bool scrub, const quorum::qd_property_seq_t &qd_props, Environment &)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s)::quorum_open\n", devname));

	//
	// Make a copy of the pathnamep for debugging purposes.
	// A particular quorum device object is associated with
	// a single configured quorum device always.
	// It is possible that an earlier quorum_open attempt
	// has initialized the pathname.
	// If so, we do not need to duplicate the string again.
	//
	if (pathnamep == NULL) {
		// First open attempt
		pathnamep = os::strdup(devname);
	} else {
		CL_PANIC(os::strcmp(pathnamep, devname) == 0);
	}

	// Initialize the door to qd_userd
	int error = 0;
	error = door_ki_open(QD_USERD_DOOR, &door_handle);
	if (error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("Netapp: door_ki_open returned error %d\n", error));
		return (quorum::QD_EIO);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_open got door handle\n", pathnamep));

	// Get nodeid/nodename info to construct the initiator string.
	nodeid_t my_nodeid = orb_conf::local_nodeid();
	char my_nodename[CL_MAX_LEN + 1];
	clconf_get_nodename(my_nodeid, my_nodename);

	//
	// Set up the initiator name for future use.
	// This is hard-coded based on the initiator setting which will be
	// part of the NetApp setup procedure.
	//
	char temp_inamep[4096];
	os::sprintf(temp_inamep,
	    "iqn.1986-05.com.sun:cluster-iscsi-quorum.%s", my_nodename);
	inamep = os::strdup(temp_inamep);

	//
	// Set the timeout to 30, which is the default. If there is a user-
	// specified value, it will modify timeout below.
	//
	timeout = TIMEOUT_DEFAULT;

	//
	// There are two ways that the quorum_open function can get extra
	// information about the quorum device. One is to read the information
	// from the CCR, this is the normal behavior. The other is in the
	// case that the device is not yet in the CCR, as when the device is
	// being scrubbed during QD add from scconf. In that case, the
	// information will be packed up into the qd_props by the clconf.
	//

	if (qd_props.length() == 0) {
		clconf_cluster_t *clconf = clconf_cluster_get_current();
		clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
		clconf_obj_t *qdev;
		const char *name;

		// Iterate over the quorum devices to find this one.
		qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
		while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {
			name = clconf_obj_get_name(qdev);
			if (name != NULL && os::strcmp(name, pathnamep) == 0) {
				break;
			}
			clconf_iter_advance(currqdevsi);
		}
		if (qdev == NULL) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("NETAPP_NAS(%s): quorum_open couldn't find "
			    "QD data in CCR for device. Device unavailable.\n",
			    pathnamep));
			os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "",
			    NULL);
			//
			// SCMSGS
			// @explanation
			// The CCR does not contain the necessary
			// configuration information for the NetApp NAS
			// device. The device will not be available.
			// @user_action
			// Remove the device using scconf, and try adding it
			// again.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "clq_netapp: quorum open error: "
			    "No matching device found in CCR "
			    "--- quorum device %s will be unavailable.",
			    pathnamep);

			if (qdevsi != (clconf_iter_t *)0) {
				clconf_iter_release(qdevsi);
			}

			return (quorum::QD_EIO);
		}

		// Get the filer_namep and lun_idp from the CCR.
		filer_namep = os::strdup(
			clconf_obj_get_property(qdev, "filer"));
		lun_idp = os::strdup(clconf_obj_get_property(qdev, "lun_id"));

		if (qdevsi != (clconf_iter_t *)0) {
			clconf_iter_release(qdevsi);
		}

		if ((lun_idp == NULL) || (filer_namep == NULL)) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("NETAPP_NAS(%s): quorum_open couldn't find "
			    "QD data in CCR for device. Device unavailable.\n",
			    pathnamep));
			os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "",
			    NULL);
			const char *missing_items = (lun_idp == NULL
			    ? (filer_namep == NULL
				? "lun_id and filer" : "lun_id")
			    : "filer");
			//
			// SCMSGS
			// @explanation
			// The configuration information obtained from the CCR
			// for the NetApp NAS device was missing one or more
			// items of required data. The device will not be
			// available.
			// @user_action
			// Remove the device using scconf, and try adding it
			// again.
			//
			(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "clq_netapp: quorum open error: "
			    "No %s data for quorum device was found in CCR "
			    "--- quorum device %s will be unavailable.",
			    missing_items,
			    pathnamep);

			return (quorum::QD_EIO);
		}

		// Get the timeout string if it is specified.
		const char *timeout_string =
		    clconf_obj_get_property(qdev, "timeout");
		if (timeout_string == NULL) {
			// No timeout specified, so default to 30 seconds.
			timeout = TIMEOUT_DEFAULT;
		} else {
			timeout = (uint_t)os::atoi(timeout_string);
		}

	} else {

		// Get the properties list for the filer name and lun id.
		for (uint_t i = 0; i < qd_props.length(); i++) {
			if (os::strcmp(qd_props[i].name, "filer") == 0) {
				filer_namep = os::strdup(qd_props[i].value);
			}
			if (os::strcmp(qd_props[i].name, "lun_id") == 0) {
				lun_idp = os::strdup(qd_props[i].value);
			}
			if (os::strcmp(qd_props[i].name, "timeout") == 0) {
				timeout = (uint_t)os::atoi(qd_props[i].value);
			}
		}

		if ((filer_namep == NULL) || (lun_idp == NULL)) {
			CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
			    ("NETAPP_NAS(%s): quorum_open was not passed "
			    "the needed information from clconf.\n",
			    pathnamep));
			return (quorum::QD_EIO);
		}
	}

	lck.lock();

	// Set up variables to control when the thread goes away.
	stop_door_call_thread = false;

	//
	// Create the thread which will handle the door calls to the
	// qd_userd. We must use clnewlwp, because that creates threads
	// which are children of the cluster process.
	//
	if (clnewlwp(quorum_device_netapp_nas_impl::execute_door_call_thread,
	    (caddr_t)this, 0, orb_conf::rt_classname(), NULL) != 0) {

		os::sc_syslog_msg msg(SC_SYSLOG_FRAMEWORK_TAG, "", NULL);
		//
		// SCMSGS
		// @explanation
		// An internal error occurred when creating a thread to
		// communicate with the NetApp NAS quorum device. The device
		// will not be available.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clq_netapp: quorum open error: "
		    "Failed to create door_call_thread "
		    "--- quorum device %s will be unavailable.",
		    pathnamep);
	}

	lck.unlock();

	//
	// If the scrub boolean is set to true, this is the first time we have
	// accessed a new quorum device, and we need to scrub it.
	//
	if (scrub) {

		//
		// First, we need to get the key for this node, because the
		// Netapp binary requires that as an argument.
		//
		quorum_table_t		*qt;
		int cl_error = clconf_get_quorum_table(NULL, &qt);
		if (cl_error) {
			//
			// If we are not able to read the table, we must
			// abort.
			//
			return (quorum::QD_EIO);
		}

		uint64_t key = 0;
		for (uint_t i = 0; i < qt->nodes.listlen; i++) {
			if (qt->nodes.list[i].nodenum == my_nodeid) {
				key = mhioc_key_to_int64_key(qt->nodes.list[i].
						reservation_key);
				break;
			}
		}

		// Construct the command.
		os::sprintf(cmd_line, "%s -t %s -I %s -l %s -r 0x%llx"
		    " cl 2>&1", NETAPP_BINARY, filer_namep, inamep, lun_idp,
		    key);

		internal_door_upcall();
		if (door_error == quorum::QD_EACCES) {
			//
			// If this node didn't have a registration on the device
			// scrub would return EACCES. All we really care about
			// is that there are no keys on the device, so go ahead
			// and look for keys, and if there aren't any, return
			// success.
			//
			quorum::reservation_list_t reg_list;
			reg_list.keylist.length(1);
			reg_list.listsize = 1;
			reg_list.listlen = 0;
			Environment e;
			quorum::quorum_error_t new_error = quorum::QD_SUCCESS;

			// Look for registrations on the device.
			new_error = quorum_read_keys(reg_list, e);
			ASSERT(e.exception() == NULL); // local invo.
			if (new_error != quorum::QD_SUCCESS) {
				return (new_error);
			}
			if (reg_list.listlen != 0) {
				return (quorum::QD_EIO);
			}

			// Now look for reservations.
			new_error = quorum_read_reservations(reg_list, e);
			ASSERT(e.exception() == NULL); // local invo.
			if (new_error != quorum::QD_SUCCESS) {
				return (new_error);
			}
			if (reg_list.listlen != 0) {
				return (quorum::QD_EIO);
			}

			//
			// There aren't any keys or reserations on the
			// device. It's safe to return success.
			//
			door_error = quorum::QD_SUCCESS;
		}

		return (door_error);
	}

	//
	// We don't want the quorum_open call to return until the door call
	// thread is definitely up and processing. Wait here if the thread id
	// hasn't been set yet.
	//
	lck.lock();
	while (door_call_thread_id == (os::threadid_t) 0) {
		ret_cv.wait(&lck);
	}
	lck.unlock();

	return (quorum::QD_SUCCESS);
}

//
// quorum_close
//
// Tell the door call thread to exit.
// Do wait until the thread really exits.
//
// quorum_close() must always be idempotent
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_close(Environment &)
{
	// Tell the door call thread to shutdown.
	lck.lock();
	stop_door_call_thread = true;
	ret_cv.broadcast();
	cmd_cv.broadcast();
	//
	// We don't want the quorum_close call to return until the door call
	// thread has exited. Wait here if the thread id is still set.
	//
	while (door_call_thread_id != (os::threadid_t) 0) {
		ret_cv.wait(&lck);
	}

	//
	// The door call thread has exited.
	// Release the door handle to qd_userd.
	// Free any strings allocated, leave lock and return.
	// Do not free the 'pathnamep' which is used by multiple methods
	// in trace messages; destructor will free that string.
	//
	if (door_handle != (door_handle_t)0) {
		door_ki_rele(door_handle);
		door_handle = (door_handle_t)0;
	}

	// Reset the data members
	door_error = quorum::QD_SUCCESS;
	stop_door_call_thread = false;
	timeout = 0;
	failfast_enabled = false;
	bzero(cmd_line, sizeof (cmd_line));
	bzero(cmd_ret, sizeof (cmd_ret));

	delete [] inamep;
	inamep = NULL;
	delete [] filer_namep;
	filer_namep = NULL;
	delete [] lun_idp;
	lun_idp = NULL;

	lck.unlock();

	return (quorum::QD_SUCCESS);
}

//
// quorum_supports_amnesia_prot
//
// Returns a value specifying the degree to which this device supports the
// various amnesia protocols in Sun Cluster. The quorum_amnesia_level enum
// is defined in quorum_impl.h
//
uint32_t
quorum_device_netapp_nas_impl::quorum_supports_amnesia_prot(Environment &)
{
	return (AMN_SC30_SUPPORT);
}

//
// quorum_reserve
//
// Place this nodes key on the device as a reservation. Will fail if this
// node does not already have a registration.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_reserve(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	uint64_t key = idl_key_to_int64_key(&my_key);

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_reserve called for key 0x%llx.\n",
	    pathnamep, key));

	lck.lock();

	// Construct the command.
	os::sprintf(cmd_line, "%s -t %s -I %s -l %s -r 0x%llx rv 2>&1",
	    NETAPP_BINARY, filer_namep, inamep, lun_idp, key);

	trigger_door_call_with_timeouts("quorum_reserve");

	lck.unlock();

	if (door_error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): internal_door_upcall returned "
		    "error %d\n", pathnamep, door_error));
		return (door_error);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_reserve wrote reservation 0x%llx to "
	    "device.\n", pathnamep, key));
	return (quorum::QD_SUCCESS);
}

//
// quorum_read_reservations
//
// Read the reservations off the NetApp filer.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_read_reservations(
    quorum::reservation_list_t		&resv_list,
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_read_reservations\n", pathnamep));

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	if (this->check_fi("quorum_read_reservations") != 0) {
		return (quorum::QD_EIO);
	}
#endif // FAULT_CMM && _KERNEL_ORB

	if (resv_list.listsize < 1) {
		//
		// If the list isn't at least 1 big, return and this will
		// be retried.
		//
		CMM_SELECTIVE_TRACE(trace_level, CMM_AMBER,
		    ("NETAPP_NAS(%s): read_reservations passed 0 size "
		    "list\n", pathnamep));
		resv_list.listlen = 1;
		return (quorum::QD_SUCCESS);
	}

	lck.lock();

	// Construct the command.
	os::sprintf(cmd_line, "%s -t %s -I %s -l %s rr 2>&1",
	    NETAPP_BINARY, filer_namep, inamep, lun_idp);

	trigger_door_call_with_timeouts("quorum_read_reservations");

	lck.unlock();

	if (door_error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): internal_door_upcall returned "
		    "error %d\n", pathnamep, door_error));
		char nodename[8];
		os::sprintf(nodename, "%d", orb_conf::local_nodeid());
		os::sc_syslog_msg msgobj(
		    SC_SYSLOG_CMM_QUORUM_TAG, nodename, NULL);
		//
		// SCMSGS
		// @explanation
		// An error was encountered while trying to read reservations
		// on the specified quorum device.
		// @user_action
		// There may be other related messages on this and other nodes
		// connected to this quorum device that may indicate the cause
		// of this problem. Refer to the quorum disk repair section of
		// the administration guide for resolving this problem.
		//
		(void) msgobj.log(SC_SYSLOG_WARNING, MESSAGE,
		    "clq_netapp: Error %d from quorum_read_reservations --- "
		    "failed to read reservations from quorum device %s.",
		    door_error, pathnamep);
		return (door_error);
	}

	//
	// Parse the return data.
	// This should be of the form:
	//  key: 12:34:56:78:9a:bc:de:12
	//  key: 01:23:45:67:89:ab:cd:de
	//  Test completed successfully
	//
	uint_t base = 16;
	uint_t digit1, digit2;
	char *cmdp = cmd_ret;

	resv_list.listlen = 0;

	for (uint_t i = 0; i < resv_list.listsize; i++) {
		char text = *cmdp;
		if (text == 'T') {
			// Start of "Test completed successfully"
			break;
		}
		if (text == 'k') {
			// Start of "key:"
			cmdp += 5;
		}
		for (uint_t j = 0; j < 8; j++) {
			text = *cmdp;
			// Get each of the octets in the registration key.
			if ((digit1 = DIGIT(text)) >= base) { /*lint !e732 */
				// Serious error, bail out.
				break;
			}
			if ((*(cmdp+1) != ':') && (*(cmdp+1) != ' ') &&
			    (*(cmdp+1) != '\n')) {
				digit2 = DIGIT(*(cmdp+1)); /*lint !e732 */
				if (digit2 >= base) {
					// Error, bail out.
					break;
				}
				cmdp += 3;
			} else {
				//
				// This is a case of 12:a:34, so we only
				// have a single digit in the octet.
				//
				digit2 = digit1;
				digit1 = 0;
				cmdp += 2;
			}
			if (j == 0) {
				resv_list.listlen++;
			}
			resv_list.keylist[i].key[j] =
			    (uint8_t)(digit1 * base + digit2);
		}
	}

	//
	// No special action is needed if no key is returned, because
	// there may simply be no reservation on the device.
	//

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_read_reservations returned %d keys:\n",
	    pathnamep, resv_list.listlen));
	for (uint_t i = 0; i < resv_list.listlen; i++) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("key[%d]: key=0x%llx\n", i,
		    idl_key_to_int64_key(&(resv_list.keylist[i]))));
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_preempt
//
// Remove the victim_key from the filer registrations and reservations.
// Add my_key as a reservation. Will fail if my_key is not already registered.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_preempt(
    const quorum::registration_key_t	&my_key,
    const quorum::registration_key_t	&victim_key,
    Environment				&)
{

	uint64_t m_key = idl_key_to_int64_key(&my_key);
	uint64_t v_key = idl_key_to_int64_key(&victim_key);

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_preempt called for key 0x%llx\n",
	    pathnamep, v_key));

	lck.lock();

	// Construct the command.
	os::sprintf(cmd_line, "%s -t %s -I %s -l %s -r 0x%llx -s 0x%llx "
	    "pt 2>&1", NETAPP_BINARY, filer_namep, inamep, lun_idp, m_key,
	    v_key);

	trigger_door_call_with_timeouts("quorum_preempt");

	lck.unlock();

	if (door_error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): internal_door_upcall returned error %d\n",
		    pathnamep, door_error));
		return (door_error);
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_register
//
// Place the node's key on the device as a registration.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_register(
    const quorum::registration_key_t	&my_key,
    Environment				&)
{
	uint64_t key = 0;

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_register.\n", pathnamep));

	key = idl_key_to_int64_key(&my_key);
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN, ("   key=0x%llx\n", key));

	lck.lock();

	// Construct the command.
	os::sprintf(cmd_line, "%s -t %s -I %s -l %s -s 0x%llx -a ri 2>&1",
	    NETAPP_BINARY, filer_namep, inamep, lun_idp, key);

	trigger_door_call_with_timeouts("quorum_register");

	lck.unlock();

	if (door_error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): internal_door_upcall returned "
		    "error %d\n", pathnamep, door_error));
		return (door_error);
	}

	return (quorum::QD_SUCCESS);
}

//
// Read all registration keys off the device.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_read_keys(
    quorum::reservation_list_t		&reg_list,
    Environment			&)
{

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_read_keys\n", pathnamep));

	lck.lock();

	// Construct the command.
	os::sprintf(cmd_line, "%s -t %s -I %s -l %s rk 2>&1",
	    NETAPP_BINARY, filer_namep, inamep, lun_idp);

	trigger_door_call_with_timeouts("quorum_read_keys");

	lck.unlock();

	if (door_error != 0) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_RED,
		    ("NETAPP_NAS(%s): internal_door_upcall returned "
		    "error %d\n", pathnamep, door_error));
		return (door_error);
	}

	//
	// Parse the return data.
	// This should be of the form:
	//  key: 12:34:56:78:9a:bc:de:12
	//  key: 01:23:45:67:89:ab:cd:de
	//  Test completed successfully
	//
	uint_t base = 16;
	uint_t digit1, digit2;
	char *cmdp = cmd_ret;

	reg_list.listlen = 0;
	for (uint_t i = 0; i < reg_list.listsize; i++) {
		char text = *cmdp;
		if (text == 'T') {
			// Start of "Test completed successfully"
			break;
		}
		if (text == 'k') {
			// Start of "key:"
			cmdp += 5;
		}
		for (uint_t j = 0; j < 8; j++) {
			text = *cmdp;
			// Get each of the octets in the registration key.
			if ((digit1 = DIGIT(text)) >= base) { /*lint !e732 */
				// Serious error, bail out.
				break;
			}
			if ((*(cmdp+1) != ':') && (*(cmdp+1) != ' ') &&
			    (*(cmdp+1) != '\n')) {
				digit2 = DIGIT(*(cmdp+1)); /*lint !e732 */
				if (digit2 >= base) {
					// Error, bail out.
					break;
				}
				cmdp += 3;
			} else {
				//
				// This is a case of 12:a:34, so we only
				// have a single digit in the octet.
				//
				digit2 = digit1;
				digit1 = 0;
				cmdp += 2;
			}
			if (j == 0) {
				reg_list.listlen++;
			}
			reg_list.keylist[i].key[j] =
			    (uint8_t)(digit1 * base + digit2);
		}
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_read_keys returned %d keys:\n",
	    pathnamep, reg_list.listlen));
	for (uint_t i = 0; i < reg_list.listlen; i++) {
		CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
		    ("key[%d]: key=0x%llx\n", i,
		    idl_key_to_int64_key(&(reg_list.keylist[i]))));
	}

	return (quorum::QD_SUCCESS);
}

//
// quorum_remove
//
// Remove the device.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_remove(const char *devname,
    const quorum::qd_property_seq_t &,
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_remove\n", pathnamep));

	return (quorum::QD_SUCCESS);
}

//
// quorum_enable_failfast
//
// ignored for this device.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_enable_failfast(
    Environment			&)
{
	if (failfast_enabled) {
		// Failfast already enabled. Simply return success.
		return (quorum::QD_SUCCESS);
	}

	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_enable_failfast\n", pathnamep));
	failfast_enabled = true;
	return (quorum::QD_SUCCESS);
}

//
// quorum_reset
//
// ignored for this device
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_reset(
    Environment			&)
{
	CMM_SELECTIVE_TRACE(trace_level, CMM_GREEN,
	    ("NETAPP_NAS(%s): quorum_reset called.\n", pathnamep));

	return (quorum::QD_SUCCESS);
}

//
// quorum_data_read
//
// Currently ignored for this device.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_data_read(
	uint64_t,
	CORBA::String_out,
	Environment		&)
{
	return (quorum::QD_SUCCESS);
}

//
// quorum_data_write
//
// Currently ignored for this device.
//
quorum::quorum_error_t
quorum_device_netapp_nas_impl::quorum_data_write(
	uint64_t,
	const char 		*,
	Environment		&)
{
	return (quorum::QD_SUCCESS);
}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
int
quorum_device_netapp_nas_impl::check_fi(char	*qop_name)
{
	// Fault point will return failure
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_FAIL,
	    &fault_argp, &fault_argsize)) {
		char *exp_path = (char *)fault_argp;
		char *cur_path = pathnamep;
		if ((exp_path == NULL) ||
		    ((cur_path != NULL) && (strcmp(cur_path, exp_path) == 0))) {
			os::printf("==> Fault Injection failing %s operation\n",
			    qop_name);
			return (EIO);
		}
	}
	return (0);
}
#endif // FAULT_CMM && _KERNEL_ORB

static struct modlmisc modlmisc = {
	&mod_miscops, "NETAPP_NAS device type module",
};

static struct modlinkage modlinkage = {
	MODREV_1, { (void *) &modlmisc, NULL, NULL, NULL },
};

char _depends_on[] =
	"misc/cl_runtime misc/cl_load misc/cl_orb "
	"misc/cl_haci misc/cl_quorum misc/cl_comm";

int
_init()
{
	int error = 0;
	if ((error = mod_install(&modlinkage)) != 0) {
		CMM_TRACE(("ERROR: could not mod_install\n"));
	} else {
		CMM_TRACE(("NETAPP_NAS: module loaded successfully\n"));
	}
	_cplpl_init();		/* C++ initialization */
	//
	// Register this device type instantiation function
	// with the type_registry.
	//
	device_type_registry_impl::register_device_type(QD_NETAPP_NAS,
	    &get_netapp_nas_qd_impl);

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}
