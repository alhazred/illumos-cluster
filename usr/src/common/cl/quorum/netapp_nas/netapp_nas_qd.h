/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _NETAPP_NAS_QD_H
#define	_NETAPP_NAS_QD_H

#pragma ident	"@(#)netapp_nas_qd.h	1.13	09/02/10 SMI"

/* Interfaces to quorum device implementation of NetApp NAS devices. */

#include <sys/door.h>
#include <orb/object/adapter.h>
#include <h/quorum.h>
#include <h/repl_pxfs.h>
#include <quorum/common/shared_disk_util.h>
#include <cmm/cmm_debug.h>

// Name of Netapp binary. Also hard coded in scqd_netappnas.c
#define	NETAPP_BINARY		"/usr/sbin/NTAPquorum"

#define	QD_NETAPP_NAS	"netapp_nas"

//
// Static accessor function to the constructor which is used by the
// type registry to modlookup.
//
static quorum::quorum_device_type_ptr get_netapp_nas_qd_impl();

class quorum_device_netapp_nas_impl :
	public McServerof <quorum::quorum_device_type> {
public:
	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//

	virtual quorum::quorum_error_t		quorum_open(
	    const char				*devname,
	    const bool				scrub,
	    const quorum::qd_property_seq_t	&qd_props,
	    Environment				&);

	virtual uint32_t quorum_supports_amnesia_prot(Environment &);

	virtual quorum::quorum_error_t quorum_close(Environment &);

	virtual quorum::quorum_error_t		quorum_reserve(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_read_reservations(
	    quorum::reservation_list_t	&resv_list,
	    Environment			&);

	virtual quorum::quorum_error_t		quorum_preempt(
	    const quorum::registration_key_t	&my_key,
	    const quorum::registration_key_t	&victim_key,
	    Environment				&);

	virtual quorum::quorum_error_t		quorum_register(
	    const quorum::registration_key_t	&my_key,
	    Environment				&);

	virtual quorum::quorum_error_t	quorum_read_keys(
	    quorum::reservation_list_t	&reg_list,
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_enable_failfast(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_reset(
	    Environment			&);

	virtual quorum::quorum_error_t	quorum_data_read(
		uint64_t		location,
		CORBA::String_out	data,
		Environment		&);

	virtual quorum::quorum_error_t	quorum_data_write(
		uint64_t		location,
		const char		*data,
		Environment		&);

	virtual quorum::quorum_error_t		quorum_remove(
	    const char				*devname,
	    const quorum::qd_property_seq_t	&qd_props,
	    Environment				&);

	virtual void			set_message_tracing_level(
	    uint32_t			new_trace_level,
	    Environment			&);

	virtual uint32_t		get_message_tracing_level(
	    Environment			&);

	quorum_device_netapp_nas_impl(cmm_trace_level);
	quorum_device_netapp_nas_impl();
	~quorum_device_netapp_nas_impl();

private:

	// Function for fault injection testing.
	int	check_fi(char*);

	// Function to make door calls to userland binary.
	void	internal_door_upcall();

	// Function to start worker thread.
	static void	execute_door_call_thread(void *);

	// Thread function to listen for upcall requests.
	void door_call_thread();

	// Function to trigger the door call execution and deal with timeouts.
	void	trigger_door_call_with_timeouts(char *);

	//
	// Condition variable for communciation between main quorum thread
	// and door thread.
	//
	os::condvar_t	cmd_cv;
	os::condvar_t	ret_cv;
	os::mutex_t	lck;

	// Information about the door call thread.
	os::threadid_t	door_call_thread_id;

	//
	// Variables to pass information to and from the door call thread and
	// the main quorum thread.
	//
	quorum::quorum_error_t	door_error;
	char	cmd_line[1024*64];
	char	cmd_ret[1024*64];

	// Boolean to tell the thread to stop.
	bool	stop_door_call_thread;

	// The name of the device. Used mainly for debugging purposes.
	char		*pathnamep;

	// The name of the initiator
	char		*inamep;

	// The name of the filer. (rsh'able name)
	char		*filer_namep;

	// The name of the lun id.
	char		*lun_idp;

	// The time after which the userland process should timeout.
	uint_t		timeout;

	// The door to communicate with qd_userd
	door_handle_t	door_handle;

	// True if failfast has been enabled
	bool		failfast_enabled;

	cmm_trace_level trace_level;
};

#endif	/* _NETAPP_NAS_QD_H */
