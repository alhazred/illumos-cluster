/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CMM_NS_H
#define	_CMM_NS_H

#pragma ident	"@(#)cmm_ns.h	1.17	08/06/05 SMI"

#include <h/cmm.h>
#include <h/ff.h>
#include <sys/sol_version.h>
#include <orb/invo/corba.h>

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <h/membership.h>
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

// Utilities to get the objects exported by the CMM.
// These take a name parameter which identifies the CMM to be accessed.  The
// default is the Kernel CMM.
//
// XXX The interface uses string parameters because we could have multiple
// userland CMM clusters, each with a unique name.  If we decide to never
// support multiple userland CMM clusters, then we should change this interface
// to use an enum parameter (representing kernel or user).
class cmm_ns {
public:
	static cmm::callback_registry_ptr get_callback_registry(
		const char *cmm_idp = "Kernel CMM",
		nodeid_t n = NODEID_UNKNOWN);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// Get a reference to a membership manager, identified by the name arg,
	// that pertains to the same cluster as the caller
	//
	static membership::manager_ptr get_membership_manager_ref(
		const char *membership_namep,
		nodeid_t n = NODEID_UNKNOWN);

	//
	// Get a reference to a membership manager, identified by the name arg,
	// that pertains to the cluster specified as argument
	//
	static membership::manager_ptr get_membership_manager_ref_zc(
		const char *cluster_contextp,
		const char *membership_namep,
		nodeid_t n = NODEID_UNKNOWN);

	// Get a reference to the sole membership engine per base node
	static membership::engine_ptr get_membership_engine_ref(
		nodeid_t n = NODEID_UNKNOWN);

	// Get a reference to the sole membership API object per base node
	static membership::api_ptr get_membership_api_ref(
		nodeid_t n = NODEID_UNKNOWN);
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	static cmm::control_ptr get_control(
		const char *cmm_idp = "Kernel CMM",
		nodeid_t n = NODEID_UNKNOWN);

	static quorum::quorum_algorithm_ptr get_quorum_algorithm(
		const char *cmm_idp = "Kernel CMM",
		nodeid_t n = NODEID_UNKNOWN);

	// No cmm_id here, since there is only one per node.
	static quorum::device_type_registry_ptr get_device_type_registry(
		nodeid_t n = NODEID_UNKNOWN);

	// No cmm_id here, since there is only one per node.
	static ff::failfast_admin_ptr get_ff_admin(nodeid_t n = NODEID_UNKNOWN);

	// No cmm_id here, since there is only one per node.
	static cmm::kernel_ucmm_agent_ptr get_kernel_ucmm_agent(nodeid_t n =
	    NODEID_UNKNOWN);

private:
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	static CORBA::Object_ptr lookup_binding(const char *ctxp,
	    const char *namep, nodeid_t n, const char *cluster_contextp = NULL);
#else
	static CORBA::Object_ptr lookup_binding(const char *ctxp,
	    const char *namep, nodeid_t n);
#endif
};

#endif	/* _CMM_NS_H */
