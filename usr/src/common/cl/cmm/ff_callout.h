/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/*	  All Rights Reserved   */


#ifndef _FF_CALLOUT_H
#define	_FF_CALLOUT_H

#pragma ident	"@(#)ff_callout.h	1.8	08/05/20 SMI"

#include <sys/t_lock.h>
#include <sys/os.h>

class ff_callout_table;

//
// The callout mechanism provides general-purpose event scheduling:
// an arbitrary function is called in a specified amount of time.
//
class ff_callout {
public:
	ff_callout();
	~ff_callout();

public:
	friend class ff_callout_table;

private:
	//
	// c_lbnext and c_lbprev are protected by ff_callout_table bucket lock
	// ie. ct_hashlock[] of the respective bucket.
	//
	ff_callout	*c_lbnext;	// next in lbolt hash
	ff_callout	*c_lbprev;	// prev in lbolt hash
	clock_t		c_runtime;	// absolute run time
	void		(*c_func)(caddr_t);	// function to call
	caddr_t		c_arg;		// argument to function
	unsigned 	c_active : 1;	// timeout is active
};

#define	CALLOUT_BUCKETS		32		/* MUST be a power of 2 */
#define	CALLOUT_BUCKET_MASK	(CALLOUT_BUCKETS - 1)
#define	CALLOUT_HASH(x)		((x) & CALLOUT_BUCKET_MASK)
#define	CALLOUT_LBHASH(x)	CALLOUT_HASH(x)

/*
 * All of the state information associated with a callout table.
 * The fields are ordered with cache performance in mind.
 */
class ff_callout_table {
public:
	ff_callout_table();
	~ff_callout_table();

	void timeout(ff_callout *, void (*func)(caddr_t), caddr_t, clock_t);
	void untimeout(ff_callout *);

	static void		per_tick_processing();
private:
	os::mutex_t	ct_lock;	/* protects all callout state */
	clock_t		ct_curtime;	/* current time; tracks lbolt */
	clock_t		ct_runtime;	/* the callouts we're running now */
	ff_callout 	*ct_lbhash[CALLOUT_BUCKETS];	/* lbolt hash chains */
	os::mutex_t	ct_hashlock[CALLOUT_BUCKETS];	/* lock per bucket */

	void		lock() { ct_lock.lock(); }
	void		unlock() { ct_lock.unlock(); }

	void		hash_insert(ff_callout **, ff_callout *);
	void		hash_delete(ff_callout **, ff_callout *);

};

extern ff_callout_table ff_callo_tbl; // the global callout table for ff

#endif	/* _FF_CALLOUT_H */
