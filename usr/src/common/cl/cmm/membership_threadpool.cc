//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)membership_threadpool.cc	1.3	08/07/21 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <sys/os.h>
#include <cmm/membership_threadpool.h>

extern os::sc_syslog_msg *cmm_syslog_msgp; // for syslog messages

// The membership threadpool
threadpool *membership_threadpool::the_membership_threadpool = NULL;

//
// Specify the initial number of threads for the membership threadpool.
// The number of threads changes dynamically for this threadpool.
//
const int	membership_threadpool_size = 6;

// The membership callbacks threadpool
threadpool *callback_threadpool::the_callback_threadpool = NULL;

//
// Specify the initial number of threads for the membership callbacks
// threadpool. The number of threads changes dynamically for this threadpool.
//
const int	callback_threadpool_size = 6;

//
// Create the membership threadpool and initialize the number of threads.
//
int
membership_threadpool::initialize()
{
	ASSERT(the_membership_threadpool == NULL);

	the_membership_threadpool
	    = new threadpool(true, 0, "membership threadpool");
	if (the_membership_threadpool == NULL) {
		//
		// SCMSGS
		// @explanation
		// The membership threadpool could not be created.
		// This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership: Unable to create membership threadpool.");
		return (ENOMEM);
	}
	if (the_membership_threadpool->change_num_threads(
	    membership_threadpool_size) == 0) {
		delete the_membership_threadpool;
		//
		// SCMSGS
		// @explanation
		// Threads could not be added to the membership threadpool.
		// This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership: Unable to change number of threads "
		    "in membership threadpool.");
		return (ENOMEM);
	}
	return (0);
}

void
membership_threadpool::shutdown()
{
	//
	// No guarantee this won't race against calls to the().  We assume
	// when shutdown is called all clients of the membership
	// threadpool have been shut down.
	//

	//
	// In the kernel, ORB::initialize might have failed
	// before initializing the_membership_threadpool.
	//
	if (the_membership_threadpool == NULL) {
		return;
	}

	the_membership_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
	the_membership_threadpool->shutdown();

	delete the_membership_threadpool;
	the_membership_threadpool = NULL;
}

//
// Create the membership callbacks threadpool
// and initialize the number of threads.
//
int
callback_threadpool::initialize()
{
	ASSERT(the_callback_threadpool == NULL);

	the_callback_threadpool
	    = new threadpool(true, 0, "membership callbacks threadpool");
	if (the_callback_threadpool == NULL) {
		//
		// SCMSGS
		// @explanation
		// The membership callback threadpool could not be created.
		// This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership: Unable to create "
		    "membership callback threadpool.");
		return (ENOMEM);
	}
	if (the_callback_threadpool->change_num_threads(
	    callback_threadpool_size) == 0) {
		delete the_callback_threadpool;
		//
		// SCMSGS
		// @explanation
		// Threads could not be added to the membership callback
		// threadpool. This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership: Unable to change number of threads "
		    "in membership callback threadpool.");
		return (ENOMEM);
	}
	return (0);
}

void
callback_threadpool::shutdown()
{
	//
	// No guarantee this won't race against calls to the().  We assume
	// when shutdown is called all clients of the membership callbacks
	// threadpool have been shut down.
	//

	//
	// In the kernel, ORB::initialize might have failed
	// before initializing the_callback_threadpool.
	//
	if (the_callback_threadpool == NULL) {
		return;
	}

	the_callback_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
	the_callback_threadpool->shutdown();

	delete the_callback_threadpool;
	the_callback_threadpool = NULL;
}
#endif	// SOL_VERSION >= __s10
