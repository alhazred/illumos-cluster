/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  cmm_impl.h
 *
 *  Header file for cluster membership monitor.
 *
 */

#ifndef _CMM_IMPL_H
#define	_CMM_IMPL_H

#pragma ident	"@(#)cmm_impl.h	1.49	09/04/13 SMI"

#include <cmm/automaton_impl.h>
#include <cmm/callback_registry_impl.h>
#include <cmm/control_impl.h>
#include <h/ff.h>
#include <h/quorum.h>
#include <sys/cl_events.h>
#include <orb/infrastructure/common_threadpool.h>
#include <h/ccr.h>
#include <sys/clconf_int.h>

typedef enum { EXIT, DONT_EXIT } dec_ref_arg_t;

#define	ABORT_FF_STR	"_abort_ff"
#define	CALLBACK_FF_STR	"_callback_ff"

#ifdef	_KERNEL
#define	INFRASTRUCTURE_TABLE_NAME	"infrastructure"
#define	CCR_DIR_OBJ_NAME		"ccr_directory"
#endif

extern "C" os::threadid_t get_transition_thread_id();

extern char *cmm_name;
extern char *abort_ff_name;
extern char *callback_ff_name;

//
// The CMM Leader waits this amount of time in seconds for each node to
// register its CMM version in the CCR. Then the CMM Leader checks to see
// whether all nodes can use the latest version.
//
#define	CMM_VERSION_REG_TIME	2

//
// CMM threads are created in the real time scheduling class.
// CMM_MAXPRI is the largest priority used for CMM threads relative
// to the base priority of the RT class.
//
extern pri_t CMM_MAXPRI;

extern os::sc_syslog_msg	*cmm_syslog_msgp; // for syslog messages

#ifdef	_KERNEL
//
// The callback that CMM registers with CCR to get notifications
// about updates to the infrastructure table.
// On receiving this callback, CMM reads in the membership configuration
// information from the infrastructure table and updates its local copy
// of membership configuration data.
//
class infr_cb_impl_for_cmm : public McServerof<ccr::callback> {
public:
	void did_update(
	    const char *table_namep, ccr::ccr_update_type, Environment &);
	void did_update_zc(
	    const char *clusterp, const char *table_namep,
	    ccr::ccr_update_type, Environment &);
	void did_recovery(
	    const char *table_namep, ccr::ccr_update_type, Environment &);
	void did_recovery_zc(
	    const char *clusterp, const char *table_namep,
	    ccr::ccr_update_type, Environment &);
	void _unreferenced(unref_t);
};
#endif

class cmm_impl {
	friend class automaton_impl;
	friend class control_impl;
	friend class path_manager;
public:
	~cmm_impl() {}
	static cmm_impl &the();
	static void initialize_config();
	static cmm::automaton_ptr cmm_init(char *, nodeid_t, cmm::comm_ptr,
	    incarnation_num);
	static void register_with_pm();
	static void unregister_with_pm();
	static void cmm_fini(void);
	void dec_ref(dec_ref_arg_t);
	void getcluststate(struct clust_state *csp) {
	    _autm.getcluststate(csp); }
	uint_t  get_max_step_number();
	void set_max_step_number(uint_t);

	static bool cmm_create_thread(void (*func)(void *), void *arg,
	    pri_t pri);
	bool cluster_shutdown();
	void cluster_shutdown_commit();
	bool get_cluster_shutdown();
	bool get_cluster_shutdown_intent();
	void update_node_status(nodeid_t, incarnation_num, cmm::timeout_t);

	bool is_old_version();
	bool version_changed();
	//
	//  Functions to switch between old and new versions of CMM.
	//
	void set_old_version();
	void set_new_version();

	//
	// Functions to retrieve and record the CMM version in the CCR.
	//
	uint_t	get_cmm_global_version() const;
	void	record_cmm_global_version() const;
	void	record_cmm_version_for_node() const;
	void	register_cmm_version(const cmm::membership_t&);

	//
	// Functions to initiate the task of recording the CMM version
	// in the CCR.
	//
	void initiate_record_cmm_version_for_node() const;
	void initiate_record_cmm_global_version() const;

	// Get the automaton primary.
	nodeid_t get_primary();

	//
	// Functions to do the message conversions.
	//
	void convert_msg_to_new(cmm::message_t&);
	void convert_msg_to_old(cmm::message_t&);

#ifdef DEBUG
	void stop_cluster(void);
#endif

	cmm_automaton_state_t get_current_state();

	static os::threadid_t get_transition_thread_id();

#ifdef	_KERNEL
	//
	// This method is invoked by the CCR callback object
	// registered by CMM, and hence this method is public.
	//
	void read_membership_info_from_ccr();
#endif

#if	defined(WEAK_MEMBERSHIP) && defined(_KERNEL)
	//
	// This method can be used to check if the cluster
	// is currently running with membership properties
	// that allow multiple partitions to survive.
	//
	bool are_multiple_partitions_allowed();
#endif

private:
	cmm_impl(char *, cmm::comm_ptr, quorum::quorum_algorithm_ptr, nodeid_t,
	    incarnation_num);

	automaton_impl		_autm;
	callback_registry_impl	_cb_registry;
	control_impl		_control;
	cmm::comm_ptr		_commp;
	bool			_cluster_shutdown_intent;
	bool			_cluster_shutdown;

	// For automaton_impl step_state use
	cmm::membership_t	_current_mbrs;
	cmm::seqnum_t		_current_seqnum;
	uint_t			_current_step;
	cmm_automaton_state_t	_current_state;

	//
	// node_status_struct is shared between the path_manager and
	// the node_status_thread. It is used by the path_manager to
	// record remote node (un)reachability even, and to signal
	// the node_status_thread to process the event.
	// Access to node_status array and seqnum are protected by
	// status_lock and status_cv.
	//
	typedef struct {
		incarnation_num	incn;
		cmm::timeout_t	timeout;
	} node_status_struct;
	node_status_struct	node_status[NODEID_MAX + 1];
	uint32_t		seqnum;
	os::mutex_t		status_lock;
	os::condvar_t		status_cv;

	void initialize();
	void start();
	static void transitions_thread_start(void *);
	void transitions_thread();
	void state_postprocessing(cmm_automaton_state_t,
	    cmm_automaton_event_t, os::hrtime_t);
	void stop_sequence();

	static void abort_thread_start(void *);
	void abort_thread();
	void abort_node();
	void signal_abort_thread();

	static void node_status_thread_start(void *);
	void node_status_thread();

	void abort_ff_create();
	void abort_ff_arm();
	void abort_ff_disarm();

	//
	// Utility functions to retrieve and record CMM versions.
	//
	uint_t	get_cmm_version(const char *) const;
	uint_t	get_cmm_version_for_node(const nodeid_t) const;
	void	record_cmm_version(const char *) const;

#ifdef	_KERNEL
	//
	// Methods to register/unregister CMM for infrastructure table
	// change callbacks from CCR.
	//
	void register_cmm_for_infr_callback();
	void unregister_cmm_for_infr_callback();
#endif

	os::mutex_t cmm_cluster_shutdown_lock;
	os::mutex_t cmm_abort_lock;
	os::condvar_t cmm_abort_cv;
	int need_abort_transition;
	int stop_needs_return;
	int do_abort;
	int _refcnt;
	//
	// The number of steps we execute during a transition is specified
	// by our clients. We take the largest number specified by our
	// clients and use that as max_step_number.
	//
	uint_t max_step_number;

	//
	// The current CMM version.
	//
	uint_t	cmm_version;
	os::mutex_t version_lock;

	//
	// Flag to record if the version changed during the current
	// reconfiguration. This flag is set in the BEGIN state when
	// CMM transitions from verion 0 to version 1. It is reset
	// when CMM enters CMM_STEP_3. The VM returns the highest
	// running version for a version protocol when CMM is running
	// version 0 or if this flag is set.
	//
	bool version_change;

	nodeset versions_set;

	//
	// The following failfast unit is used to make sure that aborts
	// happen in a timely manner.
	//
	ff::failfast_ptr abort_ff;

	static cmm_impl *the_cmm_impl;

	//
	// Variable for storing the transitions thread id
	//
	static os::threadid_t transition_thread_id;

#ifdef	_KERNEL

	// Reference to the CCR directory object in kernel
	ccr::directory_ptr ccr_dir_p;

	// Reference to the CCR callback object registered by CMM
	ccr::callback_ptr infr_cb_p;
#endif

	//
	// Hide copy constructor and assignment operator.
	//
	cmm_impl(const cmm_impl &);
	cmm_impl &operator = (cmm_impl &);
};

//
// Implementation class for registering the CMM version in the CCR.
//
class update_cmm_version_task : public defer_task {
public:
	update_cmm_version_task(bool);
	~update_cmm_version_task();
	void execute();
private:
	bool glb_task;
};

#ifdef _KERNEL_ORB

//
// Implementation Class for kernel_ucmm_agent (defined in cmm.idl)
//

class kernel_ucmm_agent_impl : public McServerof < cmm::kernel_ucmm_agent > {

public:

	kernel_ucmm_agent_impl();

	void _unreferenced(unref_t);

	//
	// Functions that implement the IDL interface
	//

	void keep_remote_ucmm_proxy_reference(
		const char			*name,
		sol::nodeid_t			remote_nodeid,
		cmm::remote_ucmm_proxy_ptr	remote_proxy,
		Environment			&e);

	void release_remote_ucmm_proxy_references(
		const char			*cluster_namep,
		Environment			&e);

private:

	typedef struct cmm_proxy_struct {
		char				*ucmm_name;
		cmm::remote_ucmm_proxy_ptr	ref[NODEID_MAX+1];
		struct cmm_proxy_struct		*next;
	} cmm_proxy_t;

	//
	// Array to store remote UCMM proxy references
	//
	cmm_proxy_t	*proxyp;
	os::mutex_t	cmm_plist_lock;	// protects cmm proxy list
};

#endif // _KERNEL_ORB

#endif	/* _CMM_IMPL_H */
