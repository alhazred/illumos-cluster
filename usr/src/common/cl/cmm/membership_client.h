/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  membership_client.h
 *
 *  Header file for membership client.
 *
 */

#ifndef	_MEMBERSHIP_CLIENT_H
#define	_MEMBERSHIP_CLIENT_H

#pragma ident	"@(#)membership_client.h	1.5	08/07/21 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <sys/os.h>
#include <h/membership.h>
#include <orb/object/adapter.h>

#define	CLUSTER_NAME_MAX	ZONENAME_MAX

//
// We do not have arbritary processes as clients of
// process membership. rgmd and ucmmd are the only two processes
// that use this feature. So, we limit the length of the names
// by which the membership infrastructure would identify the processes.
// This is just to avoid doing memory allocations for variable sized strings
// or have large character arrays for names.
//
#define	PROCESS_NAME_MAX_LEN	10

//
// Maximum length of a cluster id string.
// The max number of clusters (zones) is 8192.
// But the cluster id is a uint32_t
//
#define	CLID_MAX_LEN	33

#define	MEMBERSHIP_TYPE_MAX_LEN	30

//
// Names of the memberships to be maintained consist of :
// (i) the cluster id
// (ii) the process name, if any
// (iii) some characters for the type of membership
// (iv) some buffer characters (spaces etc.)
// So this is a reasonable size for the membership name.
//
#define	MEMBERSHIP_NAME_MAX_LEN	\
	(MEMBERSHIP_TYPE_MAX_LEN + CLID_MAX_LEN + PROCESS_NAME_MAX_LEN + 30)

int construct_membership_name(
    char *namep, membership::membership_type type,
    const char *cl_namep, const char *proc_namep);

int construct_membership_name(
    char *namep, membership::membership_type type,
    uint_t clid, const char *proc_namep);

typedef void (*mem_callback_funcp_t)(
    const char *, const char *, const cmm::membership_t, const cmm::seqnum_t);

//
// This class implements the membership client interface.
//
// Membership clients can use the membership subsystem's IDL interfaces
// to register for membership change callbacks.
// For doing so, membership clients need to create a CORBA object
// of this type and register the reference with the membership subsystem
// so that the membership subsystem uses the CORBA reference to
// call back the membership clients.
//
// Clients that need to register with membership subsystem for
// membership change callbacks need to instantiate this class,
// by calling the constructor with their callback function as a parameter.
// Then the clients can use the membership API interfaces to
// register a reference of the instantiated object.
//
class membership_client_impl :
    public McServerof< membership::client > {

public :
	membership_client_impl(
	    const char *, membership::membership_type,
	    const char *, mem_callback_funcp_t);
	membership_client_impl(
	    const char *, membership::membership_type,
	    const char *, const char *, mem_callback_funcp_t);
	const char *get_client_name() const;
	mem_callback_funcp_t get_callback_funcp() const;

	// IDL method
	void membership_callback(
	    const cmm::membership_t&, cmm::seqnum_t, Environment&);

private :
	membership_client_impl();
	~membership_client_impl();
	void _unreferenced(unref_t);

	// Hide copy constructor and assignment operator.
	membership_client_impl(const membership_client_impl &);
	membership_client_impl &operator = (membership_client_impl &);

	char *client_namep;
	char cluster_name[CLUSTER_NAME_MAX];
	char membership_name[MEMBERSHIP_NAME_MAX_LEN];
	mem_callback_funcp_t callback_funcp;
};

#include <cmm/membership_client_in.h>

#endif	/* (SOL_VERSION >= __s10) */
#endif	/* _MEMBERSHIP_CLIENT_H */
