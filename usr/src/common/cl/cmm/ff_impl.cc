//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ff_impl.cc	1.62	08/07/17 SMI"

#include <sys/os.h>

#ifdef _KERNEL
#include <sys/uadmin.h>
#endif

#include <cmm/ff_impl.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns_int.h>
#include <sys/ddi.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_impl.h>
#include <cmm/cmm_config.h>
#include <orb/transport/path_manager.h>
#include <orb/infrastructure/cl_sched.h>
#include <errno.h>
#include <sys/door.h>

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
#include <sys/hashtable.h>
#define	RUNNING "running"
#define	SHUTDOWN "shutdown"
// #include <cmm/membership_engine_impl.h>
// #include <cmm/membership_api.h>
#include <cmm/cmm_ns.h>
extern sol::zoneid_t zonename_to_zoneid(const char *);
#endif


//
// This module implements the failfast functionality.
//

//
// The failfast timeouts are run in interrupt context, and since
// zone_shutdown() does various cv_timedwait_sig() and its variants, its good
// to avoid calling zone_shutdown() in interrupt context.
// So, we do the following :
// (1)	If a ff_impl failfast unit for a global zone client gets unreferenced,
//	then we follow the normal process
// (2)	If a ff_impl failfast unit for a non-global zone client gets
//	unreferenced, then we create a server thread that will execute the
//	timeout function and thus shutdown the zone properly.
//
// The server thread shuts the zone based on the zone_id and zone_uniqid.
//
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
extern "C" void server_thread(void *);
static void reboot_zone(zoneid_t, uint64_t);
#endif

static void ff_timedout(caddr_t arg);

//
// Internal flag used by the failfast driver to identify infinite timeouts.
//
const timeout_id_t FF_INFID = (timeout_id_t)0;

//
// Internal flag used by the failfast driver to identify finite timeouts.
//
// XXX Is this safe?
//
const timeout_id_t FF_FINID = (timeout_id_t)1;

//
// The clock interrupt handler calls ff_clock_callout every clock tick
// so that we can see if we need to timeout any failfast units.
// this routine address is put into the global pointer cmm_clock_callout
// so that the clock interrupt handler can use this function pointer and
// call us.
// See usr/src/uts/common/os/clock.c
//
extern "C" void (*cmm_clock_callout)(void);

//
// The failfast handler will call the function pointed to by
// the following function pointer every clock tick. The clock interrupt
// handler calls the failfast handler and the failfast handler will call
// this routine. This routine address is set by the CMM automaton
//
extern void (*cmm_automaton_callout)(void); // in automaton_impl.cc

//
// The real time priority for scheduling the failfastd
//
static pri_t	failfastd_rt_sched_priority = 0;

ff_admin_impl *ff_admin_impl::the_ff_adminp = NULL;

//
// No.of times of clock ticks after which we want interrupt to do per_tick
// processing
//
uint_t clk_delay_factor = 2;


#if (SOL_VERSION >= __s10) && defined(_KERNEL)
// Global variables and helper functions for zones failfast


//
// We allow failfast to be disabled during development.
//

// If false, then non-global zone cannot enable failfast
bool node_level_failfast_enabled = true;

// If this flag is true, then global zone failfast is enabled.
bool global_zone_failfast_enabled = true;

// This lock serializes access to all the following data for non-global zones.
os::mutex_t	non_global_zones_failfast_lock;

unsigned int disabled_zones_list_length = 1000;

//
// FF_STATE_DISABLED :
//		Signifies that failfast is disabled for the non-global zone
//
// FF_STATE_ENABLED :
//		Signifies that failfast is enabled for the non-global zone
//

typedef enum failfast_state {
    FF_STATE_DISABLED = 1,
    FF_STATE_ENABLED}	failfast_state_t;

//
// This is the failfast entry type in the hashtable.
// failfast_state : This is the zone level failfast control.
// failfast_unit_count : This is the number of failfast clients in the zone.
// zone_is_going_down : Denotes if a zone is going down.
//	Set to true when a failfast unit fires, so that other failfast units
//	for the zone do not fire again.
//
struct failfast_entry_t {
	failfast_state_t	failfast_state;
	unsigned int		failfast_unit_count;
	bool			zone_is_going_down;
};

//
// Data structure that has zone_uniqids and disabled flag for all
// non-global zones running on the system.
//
hashtable_t<failfast_entry_t *, uint64_t>
    failfast_disabled_zones(disabled_zones_list_length);

//
// Return the value of zone level failfast control for the non-global zone,
// that corresponds to the given zone unique id.
//
// Note that this routine should be called after checking that the zone
// exists in the hashtable. If the zone does not exist in the hashtable,
// this routine would assert.
//
failfast_state_t
failfast_is_disabled(uint64_t zone_uniqid)
{
	failfast_entry_t *failfast_entry =
	    failfast_disabled_zones.lookup(zone_uniqid);
	ASSERT(failfast_entry);
	return (failfast_entry->failfast_state);
}

// Returns true if the zone is present in the hashtable
bool
zone_is_present_in_hashtab(uint64_t zone_uniqid)
{
	if (failfast_disabled_zones.lookup(zone_uniqid)) {
		return (true);
	} else {
		return (false);
	}
}

//
// Disable zone level failfast control for the non-global zone
// corresponding to the given zone id.
//
// If the zone entry exists in the hashtable,
// this routine disables the zone level failfast control in the zone entry.
// Else, the routine silently returns without any action.
//
void
zone_failfast_disable(uint64_t zone_uniqid)
{
	// Lock should be held before calling this routine
	ASSERT(non_global_zones_failfast_lock.lock_held());

	// Disable the non-global zone failfast disabled flag in the hashtable.
	failfast_entry_t *failfast_entry = NULL;
	failfast_entry = failfast_disabled_zones.lookup(zone_uniqid);

	if (failfast_entry) {
		failfast_entry->failfast_state = FF_STATE_DISABLED;
	}
}

//
// Enable zone level failfast control for the non-global zone
// corresponding to the given zone id.
//
// If the zone entry exists in the hashtable,
// this routine enables the zone level failfast control in the zone entry.
// Else, the routine silently returns without any action.
//
void
zone_failfast_enable(uint64_t zone_uniqid)
{
	// Lock should be held before calling this routine
	ASSERT(non_global_zones_failfast_lock.lock_held());

	// Enable the non-global zone failfast disabled flag in the hashtable.
	failfast_entry_t *failfast_entry = NULL;
	failfast_entry = failfast_disabled_zones.lookup(zone_uniqid);

	if (failfast_entry) {
		failfast_entry->failfast_state = FF_STATE_ENABLED;
	}
}

//
// If the zone entry exists in the hashtable,
// this routine decrements the failfast_unit_count in the zone entry.
// If the failfast_unit_count drops to zero,
// the zone entry is removed from the hashtable,
// and the memory allocated is freed.
//
// If the zone entry does not exist in the hashtable,
// this routine silently returns without action.
//
void
zone_failfast_remove(uint64_t zone_uniqid)
{
	// Lock should be held before calling this routine
	ASSERT(non_global_zones_failfast_lock.lock_held());

	// Remove the non-global zone entry from the hashtable.
	failfast_entry_t *failfast_entry = NULL;
	failfast_entry = failfast_disabled_zones.lookup(zone_uniqid);

	ASSERT(failfast_entry); // zone should exist in the hash table

	//
	// If the zone entry is present in the hashtable,
	// then the count of failfast units in the zone should be positive.
	//
	ASSERT(failfast_entry->failfast_unit_count > 0);

	(failfast_entry->failfast_unit_count)--;

	if (failfast_entry->failfast_unit_count == 0) {
		// Last failfast unit for the zone
		failfast_entry =
		    failfast_disabled_zones.remove(zone_uniqid);
		delete failfast_entry;
	}
}

//
// If the entry for a zone (corresponding to zone_uniqid) does not exist
// in the hashtable, then this routine allocates memory for a new entry,
// and adds the entry to the hashtable.
//
// If the entry for the zone exists in the hashtable,
// this routine increments the failfast_unit_count, signifying that
// another failfast client in the zone has registered with failfast.
//
void
zone_failfast_add(uint64_t zone_uniqid)
{
	failfast_entry_t *failfast_entry = NULL;
	// Lock should be held before calling this routine
	ASSERT(non_global_zones_failfast_lock.lock_held());

	failfast_entry = failfast_disabled_zones.lookup(zone_uniqid);

	if (failfast_entry) {
		// Zone entry exists in hash table
		(failfast_entry->failfast_unit_count)++;

		// Catch unsigned integer overflow
		ASSERT(failfast_entry->failfast_unit_count);
	} else {
		// Zone entry does not exist in hash table
		failfast_entry = new failfast_entry_t;
		failfast_entry->failfast_state = FF_STATE_ENABLED;
		failfast_entry->failfast_unit_count = 1;
		failfast_entry->zone_is_going_down = false;
		failfast_disabled_zones.add(failfast_entry, zone_uniqid);
	}
}

//
// If a zone entry exists in the hashtable,
// then this routine returns true if the zone is going down.
// If the zone entry does not exist in the hashtable,
// this routine asserts.
// Hence, note that the callee of this routine should check that
// the zone entry exists in the hashtable before calling this routine.
//
bool
zone_is_going_down(uint64_t zone_uniqid)
{
	ASSERT(non_global_zones_failfast_lock.lock_held());
	failfast_entry_t *failfast_entry =
	    failfast_disabled_zones.lookup(zone_uniqid);
	ASSERT(failfast_entry);
	return (failfast_entry->zone_is_going_down);
}

//
// If a zone entry exists in the hashtable,
// then this routine marks in the zone entry that the zone is going down.
// If the zone entry does not exist in the hashtable,
// this routine asserts.
// Hence, note that the callee of this routine should check that
// the zone entry exists in the hashtable before calling this routine.
//
void
mark_zone_is_going_down(uint64_t zone_uniqid)
{
	ASSERT(non_global_zones_failfast_lock.lock_held());
	failfast_entry_t *failfast_entry =
	    failfast_disabled_zones.lookup(zone_uniqid);
	ASSERT(failfast_entry);
	failfast_entry->zone_is_going_down = true;
}

// Helper function to get zone_uniqid, given the zone_id
int64_t
zoneid_to_zoneuniqid(zoneid_t zone_id)
{
	uint64_t zone_uniqid;
	zone_t *zone_ref = zone_find_by_id(zone_id);
	if (zone_ref == NULL) {
		return (-1);
	}
	zone_uniqid = zone_ref->zone_uniqid;
	zone_rele(zone_ref);
	return ((int64_t)zone_uniqid);
}

//
// Returns true if the non-global zone with the given zone_uniqid is running,
// i.e., if the zone exists and its status is < ZONE_IS_SHUTTING_DOWN
//
bool
zone_is_running(zoneid_t zone_id, uint64_t zone_uniqid)
{
	zone_t *zone_ref = zone_find_by_id(zone_id);
	if (zone_ref == NULL) {
		CMM_TRACE(("zone id %d does not correspond "
		    "to any zone\n", zone_id));
		return (false);
	}

	if (zone_uniqid != zone_ref->zone_uniqid) {
		CMM_TRACE(("Failfast: Zone id %d doesn't correspond to zone "
		    "unique id %d; zone might have been rebooted\n",
		    zone_id, zone_uniqid));
		zone_rele(zone_ref);
		return (false);
	}

	zone_status_t status = zone_status_get(zone_ref);
	zone_rele(zone_ref);

	if (status >= ZONE_IS_SHUTTING_DOWN) {
		return (false);
	} else {
		return (true);
	}
}

// Function to reboot the zone having the given zone_id and zone_uniqid
static void
reboot_zone(zoneid_t zone_id, uint64_t zone_uniqid)
{
	zone_t *zone_ref = zone_find_by_id(zone_id);
	char zone_name[ZONENAME_MAX + 1];

	if (zone_ref == NULL) {
		CMM_TRACE(("reboot_zone: zone id %d does not correspond "
		    "to any zone\n", zone_id));
		return;
	}

	// Do nothing in case the zone is already down.
	if (zone_ref->zone_status >= ZONE_IS_DOWN) {
		zone_rele(zone_ref);
		return;
	}

	(void) os::strcpy(zone_name, zone_ref->zone_name);
	zone_rele(zone_ref);


	CMM_TRACE(("Failfast: Rebooting zone %s with zone_id %d\n",
	    zone_name, zone_id));

	//
	// 'zoneadmd' daemon creates a door at a well-known location,
	// and acts as the door server. To reboot a zone, we can make
	// a door upcall to the door for the target zone, and 'zoneadmd'
	// will reboot the zone for us.
	//

	// Arguments required for the zone reboot request to 'zoneadmd'
	zone_cmd_arg_t zarg;
	zarg.cmd = Z_REBOOT;
	zarg.uniqid = zone_uniqid;
	(void) os::strcpy(zarg.locale, "C");

	char doorpath[PATH_MAX + 1];
	door_handle_t door;
	door_arg_t darg;

	// Path to the file, acting as the door
	if (os::snprintf(doorpath, (size_t)(PATH_MAX + 1),
	    ZONE_DOOR_PATH, zone_name) >= PATH_MAX) {
		// NULL terminate on overflow
		doorpath[PATH_MAX] = '\0';
	}

	if (long error_code = door_ki_open(doorpath, &door)) {
		CMM_TRACE((
		    "Failfast : Failed to reboot zone %s (zone ID %d) "
		    "as opening of 'zoneadmd' door failed with error "
		    "code %d. Failfast will halt the zone now.\n",
		    zone_name, zone_id, error_code));

		//
		// SCMSGS
		// @explanation
		// Failfast failed to reboot zone, because the door to
		// the zoneadmd daemon could not be opened.
		// Failfast will halt the zone as a result.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast : Failed to reboot zone %s (zone ID %d) "
		    "as opening of 'zoneadmd' door failed with error "
		    "code %d. Failfast will halt the zone now.\n",
		    zone_name, zone_id, error_code);

		// Zone reboot failed, try to halt the zone.
		error_code =
		    zone(ZONE_SHUTDOWN, (void *)zone_id, nil, nil, nil);
		if (error_code) {
			CMM_TRACE(("Failfast : Zone halt failed "
			    "for zone %s with zone_id %d, error %d\n",
			    zone_name, zone_id, error_code));
		}

		return;
	}

	// Door upcall data
	darg.data_ptr = (char *)&zarg;
	darg.data_size = sizeof (zarg);
	darg.desc_ptr = NULL;
	darg.desc_num = 0;
	darg.rbuf = (char *)&zarg;
	darg.rsize = sizeof (zarg);

	// Make the door upcall
	if (long error_code = door_ki_upcall(door, &darg) != 0) {
		door_ki_rele(door);
		CMM_TRACE((
		    "Failfast : Failed to reboot zone %s (zone ID %d) "
		    "as door upcall to 'zoneadmd' door failed with "
		    "error code %d. Failfast will halt the zone now.\n",
		    zone_name, zone_id, error_code));

		//
		// SCMSGS
		// @explanation
		// Failfast failed to reboot zone, because door upcall
		// to zoneadmd daemon failed.
		// Failfast will halt the zone as a result.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast : Failed to reboot zone %s (zone ID %d) "
		    "as door upcall to 'zoneadmd' door failed with "
		    "error code %d. Failfast will halt the zone now.\n",
		    zone_name, zone_id, error_code);

		// Zone reboot failed, try to halt the zone.
		error_code =
		    zone(ZONE_SHUTDOWN, (void *)zone_id, nil, nil, nil);
		if (error_code) {
			CMM_TRACE(("Failfast : Zone halt failed "
			    "for zone %s with zone_id %d, error %d\n",
			    zone_name, zone_id, error_code));
		}

		return;
	}

	// Release the reference to the door.
	door_ki_rele(door);
}

struct zone_info {
	zoneid_t zone_id;
	uint64_t zone_uniqid;
};

//
// If a method running inside a non-global zone wants to failfast
// the same zone, then we might end up in a deadlock for the zone.
// In such a case, we spawn a thread that executes the following routine.
//
static void
reboot_zone_thread(void *infop)
{
	zone_info *zone_infop = (zone_info *)infop;

	//
	// Check that we are failfasting the zone generation
	// that was intended to be failfasted, and not any
	// future generation of the same zone.
	// zone_uniqid uniquely identifies a zone generation.
	//
	if (zone_is_running(zone_infop->zone_id, zone_infop->zone_uniqid)) {
		reboot_zone(zone_infop->zone_id, zone_infop->zone_uniqid);
	}

	delete zone_infop;
}

//
// If a method running inside a non-global zone wants to failfast
// the same zone, then we might end up in a deadlock for the zone.
// In such a case, we spawn a thread that executes the following routine.
// The thread calls this routine to failfast the zone.
//
bool
failfast_current_zone(zoneid_t zone_id, uint64_t zone_uniqid)
{
	//
	// Command inside a zone is trying to failfast the same zone.
	// Allocate the memory and pass the zone info.
	// The thread created would free up the memory.
	//
	zone_info *zone_infop = new zone_info;

	if (zone_infop == NULL) {
		CMM_TRACE(("Failed to allocate memory for failfasting zone"));

		zone_t *zone_ref = zone_find_by_id(zone_id);
		char zone_name[ZONENAME_MAX + 1];
		(void) os::strcpy(zone_name, zone_ref->zone_name);
		zone_rele(zone_ref);

		//
		// SCMSGS
		// @explanation
		// A data structure that is needed to failfast a zone
		// could not be created. This might be due to
		// lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to allocate memory for failfasting "
		    "zone %s with zone ID %d\n",
		    zone_name, zone_id);

		return (false);
	}

	zone_infop->zone_id = zone_id;
	zone_infop->zone_uniqid = zone_uniqid;

	if (!cmm_impl::cmm_create_thread(
	    reboot_zone_thread, (void *)zone_infop, CMM_MAXPRI - 1)) {

		CMM_TRACE(("Unable to create server thread "
		    "for immediate failfast\n"));

		zone_t *zone_ref = zone_find_by_id(zone_id);
		char zone_name[ZONENAME_MAX + 1];
		(void) os::strcpy(zone_name, zone_ref->zone_name);
		zone_rele(zone_ref);

		//
		// SCMSGS
		// @explanation
		// A server thread that is needed for failfasting a zone could
		// not be created. This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Unable to create server thread for immediate failfast "
		    "of zone %s with zone ID %d\n",
		    zone_name, zone_id);

		delete zone_infop;
		return (false);
	}
	return (true);
}

#else	// (SOL_VERSION >= __s10) && defined(_KERNEL)

//
// We allow failfast to be disabled during development.
//
bool failfast_disabled = false;

#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)


//
// Convert a millisecond value to hertz.
//
clock_t
ff_impl::ff_millisec_to_hz(int msec)
{
	clock_t	secs, usecs;

	secs = (clock_t)msec / MILLISEC;
	usecs = ((clock_t)msec % MILLISEC) * 1000;

	return (secs * drv_usectohz((clock_t)MICROSEC) + drv_usectohz(usecs));
}

//
// Utility routine that, given a string, figures out what the
// corresponding failfast mode is. If no match is found, PANIC
// is returned as the default.
//
ff::ffmode
ff_impl::ff_string_to_mode(char *name)
{
	if (strcmp(name, "OFF") == 0) {
		return (ff::FFM_OFF);
	}

	if (strcmp(name, "PANIC") == 0) {
		return (ff::FFM_PANIC);
	}

	if (strcmp(name, "DEFERRED_PANIC") == 0) {
		return (ff::FFM_DEFERRED_PANIC);
	}

	if (strcmp(name, "PROXY_PANIC") == 0) {
		return (ff::FFM_PROXY_PANIC);
	}

	if (strcmp(name, "HALT") == 0) {
		return (ff::FFM_HALT);
	}

	if (strcmp(name, "DEBUG") == 0) {
		return (ff::FFM_DEBUG);
	}

	if (strcmp(name, "PROM") == 0) {
		return (ff::FFM_PROM);
	}

	CMM_TRACE(("ff_string_to_mode called with invalid string '%s'; "
	    "returning ff::FFM_PANIC.\n", name));
	//
	// SCMSGS
	// @explanation
	// An invalid value was supplied for the failfast mode. The software
	// will use the default PANIC mode instead.
	// @user_action
	// Contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		"Failfast: Invalid failfast mode %s specified. "
		"Returning default mode PANIC.", name);
	return (ff::FFM_PANIC);
}

//
// Arm a failfast unit with a timeout that happens in millisecs number of
// milliseconds. If the configured mode is OFF, do not arm. Just return.
//
void
ff_impl::arm(int32_t millisecs, Environment &)
{
	ASSERT(ff_armed == 0);

	timeout_id_t old_timeout_id;

	if (ff_mode == ff::FFM_OFF) {
		return;
	}

	lock();
	//
	// Make the timeout happen immediately if the time delta is zero.
	//
	if (millisecs == 0) {
		(this->*ff_timeoutf)();
	}

	old_timeout_id = ff_timeout_id;

	ff_armed = 1;
	//
	// We need to drop the lock here to prevent a deadlock if the timeout
	// expires.
	//
	unlock();

	if (millisecs == ff_FF_INFINITE) {
		ff_timeout_id = FF_INFID;
		//
		// If the previous timeout was a finite timeout, we need
		// to disarm it.
		//
		if (old_timeout_id == FF_FINID) {
			ff_callo_tbl.untimeout(&ff_callout_block);
		}

		//
		// If this arm corresponds to FFM_DEFERRED_PANIC or
		// FFM_PROXY_PANIC, then increment the counter that
		// keeps track of this number.
		//
		if ((ff_mode == ff::FFM_DEFERRED_PANIC) ||
		    (ff_mode == ff::FFM_PROXY_PANIC)) {
			ff_admin_impl::the().ff_armed_lock.lock();
			ff_admin_impl::the().ff_armed_count++;
			ff_admin_impl::the().ff_armed_lock.unlock();
		}
	} else {
		//
		// The FFM_DEFERRED_PANIC mode is used only to detect death
		// of userland processes that generate an _unreferenced().
		// Failfast units with this mode are expected to be armed
		// with an infinite timeout.
		// The same applies to FFM_PROXY_PANIC, which is also used
		// only to detect death of userland processes, except that
		// in this case, the failfast object is created on behalf
		// of the client via a proxy process (ex. clexecd).
		//

		ASSERT(ff_mode != ff::FFM_DEFERRED_PANIC);
		ASSERT(ff_mode != ff::FFM_PROXY_PANIC);

		millisecs += (int32_t)conf.failfast_gracetime;

		//
		// ff_call_tbl.timeout will automatically cancel any
		// previous timeout, as it has to reuse the callout block
		// for this new timeout.
		//
		ff_callo_tbl.timeout(&ff_callout_block, ff_timedout,
		    (caddr_t)this, ff_millisec_to_hz(millisecs));

		// note that a finite timeout exists against this failfast unit
		ff_timeout_id = FF_FINID;
	}
}

//
// Disarm a failfast unit. If the configured mode is OFF, arm would not have
// armed it. So disarm doesn't have to do anything in that case.
//
void
ff_impl::disarm(Environment&)
{
	if (ff_mode == ff::FFM_OFF) {
		return;
	}

	lock();
	do_disarm();
	unlock();
}

//
// The real disarm method. Gets called if the client invokes disarm explicitly
// or if the client drops all references while the failfast unit is armed.
//
void
ff_impl::do_disarm()
{
	//
	// We need to drop the lock here to prevent a deadlock if the timeout
	// expires.
	//
	// When a timeout expires, the timeout code has the following locking
	// order :
	//	acquire lock on callout table
	//		acquire lock on failfast unit
	//
	// Here I am holding a lock on the failfast unit. Now if I call
	// untimeout without releasing that lock, I will be violating the
	// locking order and can cause deadlock. Hence let us first release
	// the lock on the failfast unit.
	//
	if (ff_armed) {
		if (ff_timeout_id != FF_INFID) {
			unlock();
			ff_callo_tbl.untimeout(&ff_callout_block);
			lock();
		}
		//
		// Decrement the counter that keeps track of the
		// number of armed failfast units with mode
		// FFM_DEFERRED_PANIC or FFM_PROXY_PANIC.
		//
		if ((ff_mode == ff::FFM_DEFERRED_PANIC) ||
		    (ff_mode == ff::FFM_PROXY_PANIC)) {
			ff_admin_impl::the().ff_armed_lock.lock();
			ff_admin_impl::the().ff_armed_count--;
			ff_admin_impl::the().ff_armed_lock.unlock();
		}

		ff_armed = 0;
	}
}

//
// This is the function that gets called first when a timeout happens.
// The argument it gets indicates the failfast unit that encountered the
// timeout. This function then calls the unit_timedout method of the
// indicated failfast unit.
//
static void
ff_timedout(caddr_t arg)
{
	ff_impl *ff_unit = (ff_impl *)((void *)arg);
	ff_unit->unit_timedout();

	//
	// This routine is called if a callout block timeout
	// expires. So, after the timeout function is called above, its safe
	// to delete the ff_impl object.
	//
	delete ff_unit;
}

//
// If a failfast unit is set in mode FFM_DEFERRED_PANIC,
// this is the method that will get invoked when such a failfast unit
// encounters a timeout.
//
void
ff_impl::stop_node_deferred_panic()
{
	stop_node_deferred_panic_int(conf.failfast_panic_delay);
}

//
// If a failfast unit is set in mode FFM_PROXY_PANIC,
// this is the method that will get invoked when such a failfast unit
// encounters a timeout.
//
void
ff_impl::stop_node_proxy_panic()
{
	stop_node_deferred_panic_int(conf.failfast_proxy_panic_delay);
}

//
// If failfast unit is set in mode FFM_DEBUG, this is the method that will
// get invoked when such a failfast unit encounters a timeout.
//
void
ff_impl::stop_node_debug()
{
	char *panicbuf;
	if (ff_from_unref == 1) {
		panicbuf = "unreferenced while Armed";
	} else {
		panicbuf = "";
	}

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	CMM_TRACE(("Failfast: timeout - unit \"%s\"%s\"%d\"%s",
	    ff_unit_name, zone_name, zone_id, panicbuf));
	//
	// SCMSGS
	// @explanation
	// A failfast client has encountered a timeout and is going to panic
	// the node or zone.
	// @user_action
	// There might be other related messages on this node that can help
	// diagnose the problem. Resolve the problem and reboot the node or
	// zone if the node or zone panic is unexpected.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "Failfast: timeout - unit \"%s\"%s\"%s.",
	    ff_unit_name, zone_name, panicbuf);
#else
	CMM_TRACE(("Failfast: timeout - unit \"%s\"%s",
	    ff_unit_name, panicbuf));
	//
	// SCMSGS
	// @explanation
	// A failfast client has encountered a timeout and is going to panic
	// the node.
	// @user_action
	// There may be other related messages on this node which may help
	// diagnose the problem. Resolve the problem and reboot the node if
	// node panic is unexpected.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "Failfast: timeout - unit \"%s\"%s.", ff_unit_name, panicbuf);
#endif
}

//
// If failfast unit is set in mode FFM_PROM, this is the method that will
// get invoked when such a failfast unit encounters a timeout.
//
void
ff_impl::stop_node_prom()
{
	char *panicbuf;

	CMM_TRACE(("Now in ff_impl::stop_node_prom.\n"));

	if (ff_from_unref == 1) {
		panicbuf = "unreferenced while Armed";
	} else {
		panicbuf = "";
	}

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	CMM_TRACE(("Failfast timeout - unit \"%s\" of "
	    "zone \"%s\" (zone ID %d) %s",
	    ff_unit_name, zone_name, zone_id, panicbuf));
#else
	CMM_TRACE(("Failfast timeout - unit \"%s\" %s",
	    ff_unit_name, panicbuf));
#endif

#ifdef _KERNEL
#if (SOL_VERSION >= __s10)
	if (zone_id == GLOBAL_ZONEID) {
		// Panic the node if triggering for global zone
		cmn_err(CE_PANIC,
		    "Failfast timeout - unit \"%s\" of "
		    "zone \"%s\" (zone ID %d) %s",
		    ff_unit_name, zone_name, zone_id, panicbuf);
	} else {
		// Failfast the zone if triggering for a non-global zone

		//
		// SCMSGS
		// @explanation
		// The specified failfast unit has timed out,
		// and is going to panic the zone.
		// @user_action
		// There might be other related messages on this node that can
		// help diagnose the problem. Resolve the problem and reboot
		// the zone if the zone panic is unexpected.
		//
		(void) cmm_syslog_msgp->log(
		    SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast timeout - unit \"%s\" of "
		    "zone \"%s\" (zone ID %d) %s",
		    ff_unit_name, zone_name, zone_id, panicbuf);

		//
		// Check that we are failfasting the zone generation
		// that was intended to be failfasted, and not any
		// future generation of the same zone.
		// zone_uniqid uniquely identifies a zone generation.
		//
		if (zone_is_running(zone_id, zone_uniqid)) {
			reboot_zone(zone_id, zone_uniqid);
		}
	}

#else
	cmn_err(CE_PANIC, "Failfast timeout - unit \"%s\" %s",
	    ff_unit_name, panicbuf);

#endif	// SOL_VERSION >= __s10
#endif	// _KERNEL

}



//
// ff_impl methods : Zones and non-zones
//

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
//
// The server thread created when a ff_impl failfast unit for a non-global zone
// gets unreferenced.
//
void
server_thread(void *ff_ptr)
{
	((ff_impl *)ff_ptr)->_unref();
}

//
// Constructor
//
// Create a failfast unit and initialize it. The function to be called if
// the failfast unit is armed it set to	the default function, which is
// determined by the configured failfast mode. If the configured failfast
// mode is OFF, we set the function to should_not_happen, and the failfast
// unit will not be armed, as arm will just return if the configured mode
// is OFF.
//
ff_impl::ff_impl(const char *name, ff::ffmode mode,
    const zoneid_t zoneid, const uint64_t zone_uniq_id) :
	ff_armed(0),
	ff_from_unref(0),
	zone_id(zoneid),
	zone_uniqid(zone_uniq_id)
{
	(void) os::strcpy(ff_unit_name, name);

	ff_mode = mode;

	switch (mode) {
	case ff::FFM_DEBUG:
		ff_timeoutf = &ff_impl::stop_node_debug;
		break;
	case ff::FFM_PROM:
		ff_timeoutf = &ff_impl::stop_node_prom;
		break;
	case ff::FFM_HALT:
		ff_timeoutf = &ff_impl::stop_node_halt;
		break;
	case ff::FFM_PANIC:
		ff_timeoutf = &ff_impl::stop_node_panic;
		break;
	case ff::FFM_DEFERRED_PANIC:
		ff_timeoutf = &ff_impl::stop_node_deferred_panic;
		break;
	case ff::FFM_PROXY_PANIC:
		ff_timeoutf = &ff_impl::stop_node_proxy_panic;
		break;
	default:
		ff_timeoutf = &ff_impl::ff_impl_shouldnt_happen;
		break;
	}
	ff_timeout_id = NULL;
	deferred_panic_flag = false;
	bzero(deferred_panic_buf, (size_t)FF_DEFERRED_PANIC_BUF_SIZE);

	zone_t *zone_ref = zone_find_by_id(zoneid);
	(void) os::strcpy(zone_name, zone_ref->zone_name);
	zone_rele(zone_ref);

	if (zone_id != GLOBAL_ZONEID) {
		// Add to the hashtable only for non-global zone failfast
		non_global_zones_failfast_lock.lock();
		zone_failfast_add(zone_uniqid);
		non_global_zones_failfast_lock.unlock();
	}

	CMM_DEBUG_TRACE(("ff_impl constructor : "
	    "%p %s zoneid %d zonename %s zone_uniqid %d\n",
	    this, ff_unit_name, zone_id, zone_name, zone_uniqid));
}

//
// Destructor
//
// Do a consistency check and make sure we are not destroying the failfast unit
// while it has a timeout ticking.
//
//lint -e1740
//
// lint complains that functions that might throw exceptions should not be
// invoked in destructors, as destructors are not supposed to throw exceptions.
//lint -e1551
ff_impl::~ff_impl()
{
	ff_timeout_id = NULL;	// Keep lint happy
	ff_timeoutf = NULL;	// Keep lint happy
	if (ff_armed) {
		if (zone_id == GLOBAL_ZONEID) {
			//
			// SCMSGS
			// @explanation
			// The specified failfast unit was destroyed while it
			// was still armed.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "Failfast: Destroying failfast unit %s "
			    "while armed.", ff_unit_name);
		} else {
			//
			// SCMSGS
			// @explanation
			// The specified failfast unit was destroyed while it
			// was still armed.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "Failfast: Destroying failfast unit %s "
			    "while armed in zone \"%s\" (zone ID %d)",
			    ff_unit_name, zone_name, zone_id);

			//
			// Check that we are failfasting the zone generation
			// that was intended to be failfasted, and not any
			// future generation of the same zone.
			// zone_uniqid uniquely identifies a zone generation.
			//
			if (zone_is_running(zone_id, zone_uniqid)) {
				// Non-global zone - Rebooting zone
				CMM_TRACE(("Failfast : Zone %s (zone id %d) "
				    "rebooting\n", zone_name, zone_id));
				//
				// SCMSGS
				// @explanation
				// Rebooting zone with the specified zone ID,
				// as the specified failfast unit was destroyed
				// while it was still armed.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or patch is
				// available.
				//
				(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE,
				    MESSAGE, "Failfast : Zone %s (zone ID %d) "
				    "rebooting\n", zone_name, zone_id);
				reboot_zone(zone_id, zone_uniqid);
			}
		}
	}

	if (zone_id != GLOBAL_ZONEID) {
		//
		// This failfast unit is getting deleted.
		// Update hashtable with this information.
		//
		non_global_zones_failfast_lock.lock();
		zone_failfast_remove(zone_uniqid);
		non_global_zones_failfast_lock.unlock();
	}
}
//lint +e1551
//lint +e1740

//
// When a failfast unit times out, this method gets called. We lock the failfast
// unit, see if it is armed, check if failfast is enabled for the current zone,
// and if so, invoke the method that is determined
// by the mode of the failfast unit. We could find that the failfast unit is
// not armed when we come here because of a race between disarm and
// the delivery of a timeout.
//
void
ff_impl::unit_timedout()
{
	lock();
	if (ff_armed) {

		if (node_level_failfast_enabled) {
			if (zone_id == GLOBAL_ZONEID) {

				if (global_zone_failfast_enabled) {
					//
					// Failfast is enabled.
					// Call timeout function.
					//
					(this->*ff_timeoutf)();
				} else {
					CMM_DEBUG_TRACE((
					    "Failfast: Zone-level failfast "
					    "controls for zone %s (zone_id %d) "
					    "disallow the firing "
					    "of a failfast.\n",
					    zone_name, zone_id));
				}
			} else {
				non_global_zones_failfast_lock.lock();

				//
				// If failfast is disabled for this non-global
				// zone, or if the zone is not running,
				// or if the zone is going down,
				// then do not call the timeout function.
				//
				if (zone_is_running(zone_id, zone_uniqid) &&
				    !zone_is_going_down(zone_uniqid)) {
					if (failfast_is_disabled(zone_uniqid)
					    == FF_STATE_ENABLED) {

						//
						// Disable failfast for this
						// zone so that other
						// failfast units for this
						// zone do not trigger
						//
						zone_failfast_disable(
						    zone_uniqid);

						//
						// Mark that the zone is going
						// down
						//
						mark_zone_is_going_down(
						    zone_uniqid);

						non_global_zones_failfast_lock
						    .unlock();
						(this->*ff_timeoutf)();
						non_global_zones_failfast_lock
						    .lock();

					} else {
						CMM_DEBUG_TRACE((
						    "Failfast: Zone-level "
						    "failfast controls for "
						    "zone %s (zone_id %d) "
						    "disallow the firing of "
						    "a failfast.\n",
						    zone_name, zone_id));

					}
				}

				non_global_zones_failfast_lock.unlock();
			}

		} else {
			CMM_DEBUG_TRACE((
			    "Failfast: Node-level failfast controls for "
			    "zone %s (zone_id %d) disallow the firing of "
			    "a failfast.\n", zone_name, zone_id));
		}

		ff_armed = 0;
	}
	unlock();
}

//
// If the failfast unit is armed when it gets unreferenced,
// disarm the failfast unit and call the timeout function
// indicating that the failfast unit was unreferenced while it
// was armed. At the end, just delete the failfast unit.
//
void
#ifdef DEBUG
ff_impl::_unreferenced(unref_t cookie)
#else
ff_impl::_unreferenced(unref_t)
#endif
{
	//
	// The failfast unit has become unreferenced.
	//
	ASSERT(_last_unref(cookie));

	if (zone_id == GLOBAL_ZONEID) {
		//
		// If the ff_impl failfast unit is for the global zone,
		// then we call the _unref() routine, that calls the
		// timeout handler if the failfast unit is armed.
		//

		_unref();

	} else {
		//
		// If the ff_impl failfast unit is for a non-global zone,
		// we create a thread that will do the _unref() processing
		// for this ff_impl object.
		// Its better to create a new thread to take over the task,
		// as in case of a deferred panic for a non-global zone,
		// the thread might have to block for a specified time delay,
		// and then failfast the zone.
		//

		if (!cmm_impl::cmm_create_thread(
		    server_thread, (void *)this, CMM_MAXPRI - 1)) {

			CMM_TRACE(("Unable to create server thread for "
			    "failfast: rebooting zone %s with zone id %d\n",
			    zone_name, zone_id));
			//
			// SCMSGS
			// @explanation
			// A server thread that is needed for failfast to work
			// properly could not be created. This might be due to
			// lack of memory.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			    "Unable to create server thread for failfast: "
			    "rebooting zone %s with zone ID %d\n",
			    zone_name, zone_id);

			//
			// Check that we are failfasting the zone generation
			// that was intended to be failfasted, and not any
			// future generation of the same zone.
			// zone_uniqid uniquely identifies a zone generation.
			//
			if (zone_is_running(zone_id, zone_uniqid)) {
				reboot_zone(zone_id, zone_uniqid);
			}
		}
	}
}

//
// This is the common routine, used by the unreferenced code for
// the failfast unit and by the server thread code, to do the unref
// processing, execute timeout function, and then call the destructor
//
void
ff_impl::_unref()
{
	lock();
	if ((ff_mode != ff::FFM_OFF) && ff_armed) {
		do_disarm();
		ff_from_unref = 1;

/*
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
		// XXX
		// XXX This is a temporary measure taken to notify CZMM
		// XXX when a zone goes down. The actual solution is to
		// XXX include the notification in the BrandZ callback
		// XXX for shutdown of virtual cluster.
		// XXX
		uint32_t clid;
		if ((zone_id != GLOBAL_ZONEID) &&
		    (clconf_get_cluster_id(zone_name, &clid) == 0)) {
			if (os::strcmp(ff_unit_name, "failfastd") == 0) {
				Environment e;
				membership::engine_ptr engine_p =
				    cmm_ns::get_membership_engine_ref();
				ASSERT(!CORBA::is_nil(engine_p));
				char *event_namep =
				    create_mem_obj_event_name(
				    ZONE_CLUSTER_MEMBER_DOWN, zone_name, NULL);
				membership::engine_event_info info;
				info.event_namep = event_namep;
				engine_p->deliver_event_to_engine(
				    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT,
				    info, e);
				ASSERT(e.exception() == NULL);
				CORBA::release(engine_p);
			}
		}
#endif
*/

		if (node_level_failfast_enabled) {
			if (zone_id == GLOBAL_ZONEID) {

				if (global_zone_failfast_enabled) {
					//
					// Failfast is enabled.
					// Call timeout function.
					//
					(this->*ff_timeoutf)();
				} else {
					CMM_DEBUG_TRACE((
					    "Failfast: Zone-level failfast "
					    "controls for "
					    "zone %s (zone_id %d) disallow "
					    "the firing of a failfast.\n",
					    zone_name, zone_id));
				}

			} else {
				non_global_zones_failfast_lock.lock();
				//
				// If failfast is disabled for this non-global
				// zone, or if the zone is not running,
				// or if the zone is going down,
				// then do not call the timeout function.
				//

				if (zone_is_running(zone_id, zone_uniqid) &&
				    !zone_is_going_down(zone_uniqid)) {

					if (failfast_is_disabled(zone_uniqid)
					    == FF_STATE_ENABLED) {

						//
						// Disable failfast for this
						// zone, so that other failfast
						// units for this zone do not
						// trigger.
						//
						zone_failfast_disable(
						    zone_uniqid);

						//
						// Mark that the zone is going
						// down.
						//
						mark_zone_is_going_down(
						    zone_uniqid);

						non_global_zones_failfast_lock
						    .unlock();
						(this->*ff_timeoutf)();
						non_global_zones_failfast_lock
						    .lock();

					} else {
						CMM_DEBUG_TRACE((
						    "Failfast: Zone-level "
						    "failfast controls for "
						    "zone %s (zone_id %d) "
						    "disallow the firing of "
						    "a failfast.\n",
						    zone_name, zone_id));
					}
				}

				non_global_zones_failfast_lock.unlock();
			}

		} else {
			CMM_DEBUG_TRACE((
			    "Failfast: Node-level failfast controls for "
			    "zone %s (zone_id %d) disallow the firing of "
			    "a failfast.\n", zone_name, zone_id));
		}

	}
	unlock();

	//
	// If deferred_panic_flag is not set, then this ff_impl object won't
	// be used for deferred panic, and hence its ok to delete this object.
	//
	if (!deferred_panic_flag) {
		delete this;
	}
}

//
// This is the method that failfast units that were created when the configured
// failfast mode is OFF will use. This method will never be invoked because
// if the mode is OFF, arm will never really set a timeout.
//
void
ff_impl::ff_impl_shouldnt_happen()
{
	if (zone_id != GLOBAL_ZONEID) {

		CMM_TRACE(("In ff_impl::ff_impl_shouldnt_happen() : "
		    "Rebooting zone %s with zone_id %d\n", zone_name, zone_id));
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred in the failfast software.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "INTERNAL ERROR Failfast: ff_impl_shouldnt_happen. "
		    "Rebooting zone %s with zone_id %d", zone_name, zone_id);

		//
		// Check that we are failfasting the zone generation
		// that was intended to be failfasted, and not any
		// future generation of the same zone.
		// zone_uniqid uniquely identifies a zone generation.
		//
		if (zone_is_running(zone_id, zone_uniqid)) {
			reboot_zone(zone_id, zone_uniqid);
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred in the failfast software.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "INTERNAL ERROR Failfast: ff_impl_shouldnt_happen.");
	}
}

//
// If a failfast unit is set in mode FFM_DEFERRED_PANIC or FFM_PROXY_PANIC,
// this is the method that will get invoked when such a failfast unit
// encounters a timeout.
//
// When this timeout happens, the node should panic after a delay
// (configurable through the CCR). Here we take appropriate action to
// failfast the node/zone after the specified delay, based on whether the
// failfast unit is for the global zone (same as node) or a non-global zone.
//
void
ff_impl::stop_node_deferred_panic_int(cmm::timeout_t tout)
{
	int delay_seconds = tout/MILLISEC;

	ASSERT(ff_from_unref == 1);

	CMM_TRACE(("Failfast: \"%s\" of zone \"%s\" (zone id %d) died;"
	    " this zone will panic in %ld seconds\n",
	    ff_unit_name, zone_name, zone_id, delay_seconds));

	//
	// This ff_impl failfast unit was armed for deferred panic, and its
	// been unreferenced. So, the mode is changed to FFM_PANIC and
	// the timeout function is changed to ff_impl::stop_node_panic().
	//
	ff_timeoutf = &ff_impl::stop_node_panic;
	deferred_panic_flag = true;
	ff_mode = ff::FFM_PANIC;

	(void) os::snprintf(
	    deferred_panic_buf, (size_t)FF_DEFERRED_PANIC_BUF_SIZE,
	    "Aborting zone \"%s\" (zone ID %d) because "
	    "\"%s\" died %ld seconds ago",
	    zone_name, zone_id, ff_unit_name, delay_seconds);

	unlock();
	if (zone_id == GLOBAL_ZONEID) {
		//
		// If the ff_impl failfast unit is for the global zone,
		// then it is armed, so that a callout block is put
		// for this failfast unit. Thus, when the callout block
		// timeout expires, the panic timeout function
		// would be called for this ff_impl failfast unit,
		// which will take appropriate action based on zone_id
		// and zone_uniqid
		//

		Environment e;
		arm(tout, e);

	} else {
		//
		// If the ff_impl failfast unit is for a non-global zone,
		// we do not need to use the CMM timeout mechanism.
		// We simply sleep for the specified timeout delay,
		// and then call the timeout handler, which has been already
		// changed to point to the panic timeout function.
		// Thus, we failfast the non-global zone in the current thread
		// context itself, and do not use the CMM interrupt driven
		// timeout mechanism.
		// Shutting down a zone involves checking credentials,
		// and since the CMM interrupt thread doesn't have its
		// credentials properly set, its better to do the failfast
		// in the current thread itself, rather than changing the
		// credentials of the interrupt thread.
		//

		lock();
		ff_armed = 1;
		unlock();

		ff_timeout_id = FF_FINID;
		os::usecsleep((os::usec_t)((os::usec_t)tout * MILLISEC));

		lock();
		(this->*ff_timeoutf)();

		//
		// Reset ff_armed so that destructor does not find
		// the failfast unit armed anymore
		//
		ff_armed = 0;
		unlock();

		deferred_panic_flag = false;

	}

	lock();
}

//
// If failfast unit is set in mode FFM_PANIC, this is the method that will
// get invoked when such a failfast unit encounters a timeout.
//
void
ff_impl::stop_node_panic()
{
	//
	// The ff_impl failfast unit was armed for deferred panic, and the
	// deferred timeout has expired. So, failfast the node/zone
	// and use the deferred_panic_buf.
	//
	if (deferred_panic_flag) {
		//
		// Bug 4353082 - Check to see if the number of
		// armed failfast units with mode FFM_DEFERRED_PANIC
		// or FFM_PROXY_PANIC (recorded in the variable
		// ff_admin_impl::ff_armed_count) is zero. This
		// would indirectly indicate that the system is likely
		// being shutdown (since all shutdown commands
		// eventually kill all userland processes).
		// In this case, use mdboot instead of panic. Use
		// panic only if an individual userland daemon has
		// died. This fix is required by PSARC/2000/280.
		//

		ff_admin_impl::the().ff_armed_lock.lock();
		if (ff_admin_impl::the().ff_armed_count != 0) {
			ff_admin_impl::the().ff_armed_lock.unlock();
			CMM_TRACE(("Failfast: %s.\n", deferred_panic_buf));

			if (zone_id == GLOBAL_ZONEID) {
				//
				// SCMSGS
				// @explanation
				// A failfast client has encountered a
				// deferred panic timeout and is going to
				// panic the node or zone. This might happen
				// if a critical userland process, as
				// identified by the message, dies
				// unexpectedly.
				// @user_action
				// Check for core files of the process after
				// rebooting the node or zone and report these
				// files to your authorized Sun service
				// provider.
				//
				(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC,
				    MESSAGE, "Failfast: %s.",
				    deferred_panic_buf);
			} else {
				//
				// Check that we are failfasting the zone
				// generation that was intended to be
				// failfasted, and not any future generation
				// of the same zone. zone_uniqid uniquely
				// identifies a zone generation.
				//
				if (zone_is_running(zone_id, zone_uniqid)) {
					(void) cmm_syslog_msgp->log(
					    SC_SYSLOG_NOTICE,
					    MESSAGE, "Failfast: %s.",
					    deferred_panic_buf);
					reboot_zone(zone_id, zone_uniqid);
				}
			}

		} else {
			ff_admin_impl::the().ff_armed_lock.unlock();
			CMM_TRACE(("Failfast: Halting because all "
			    "userland daemons have died.\n"));

			if (zone_id == GLOBAL_ZONEID) {

				// Global zone - Halting node
				CMM_TRACE(("Failfast (stop_node_panic): "
				    "halting node\n"));
				os::prom_printf("Failfast: Halting because all"
				    " userland daemons have died.\n");

#if (SOL_VERSION >= __s11)
				mdboot(A_SHUTDOWN, AD_HALT, "", B_FALSE);
#else
				mdboot(A_SHUTDOWN, AD_HALT, "");
#endif	// SOL_VERSION >= __s11

			} else {
				// Non-global zone - Rebooting zone

				//
				// Check that we are failfasting the zone
				// generation that was intended to be
				// failfasted, and not any future generation
				// of the same zone. zone_uniqid uniquely
				// identifies a zone generation.
				//
				if (zone_is_running(zone_id, zone_uniqid)) {
					CMM_TRACE((
					    "Failfast (stop_node_panic): "
					    "Zone %s (zone id %d) rebooting\n",
					    zone_name, zone_id));

					//
					// SCMSGS
					// @explanation
					// Rebooting zone with the specified
					// zone ID, as the failfast client died.
					// @user_action
					// Check for core files of the client
					// process after you reboot the node or
					// zone and report these files to your
					// authorized Sun service provider.
					//
					(void) cmm_syslog_msgp->log(
					    SC_SYSLOG_NOTICE, MESSAGE,
					    "Failfast (stop_node_panic): "
					    "Zone %s (zone ID %d) rebooting\n",
					    zone_name, zone_id);

					reboot_zone(zone_id, zone_uniqid);
				}
			}

		}

		return;
	}

	char *panicbuf;

	if (ff_from_unref == 1) {
		panicbuf = "unreferenced while Armed";
	} else {
		panicbuf = "";
	}

	CMM_TRACE(("Failfast: timeout - unit \"%s\"%s\"%d\"%s\n", ff_unit_name,
	    zone_name, zone_id, panicbuf));
	(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "Failfast: timeout - unit \"%s\"%s\"%s.", ff_unit_name,
	    zone_name, panicbuf);

	if (zone_id == GLOBAL_ZONEID) {
		CMM_TRACE(("Failfast: timeout - unit \"%s\"%s\n",
		    ff_unit_name, panicbuf));
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Failfast: timeout - unit \"%s\"%s.",
		    ff_unit_name, panicbuf);
	} else {
		// Non-global zone - Rebooting zone

		//
		// Check that we are failfasting the zone generation that was
		// intended to be failfasted, and not any future generation
		// of the same zone. zone_uniqid uniquely identifies
		// a zone generation.
		//
		if (zone_is_running(zone_id, zone_uniqid)) {
			CMM_TRACE(("Failfast (stop_node_panic): "
			    "Zone %s (zone id %d) rebooting\n",
			    zone_name, zone_id));
			(void) cmm_syslog_msgp->log(
			    SC_SYSLOG_NOTICE, MESSAGE,
			    "Failfast (stop_node_panic): "
			    "Zone %s (zone ID %d) rebooting\n",
			    zone_name, zone_id);

			reboot_zone(zone_id, zone_uniqid);
		}
	}
}

//
// If failfast unit is set in mode FFM_HALT, this is the method that will
// get invoked when such a failfast unit encounters a timeout.
//
void
ff_impl::stop_node_halt()
{
	char buffer[ff_MAX_FF_NAME_SIZE + 50];
	char *panicbuf;

	if (ff_from_unref == 1) {
		panicbuf = "unreferenced while Armed";
	} else {
		panicbuf = "";
	}

	os::sprintf(buffer, "Failfast timeout - unit \"%s\" of "
	    "zone \"%s\" (zone ID %d) %s\n", ff_unit_name, zone_name,
	    zone_id, panicbuf);

	CMM_TRACE((buffer));

	if (zone_id == GLOBAL_ZONEID) {

		// Global zone - Halting node
#if (SOL_VERSION >= __s11)
		mdboot(A_SHUTDOWN, AD_HALT, buffer, B_FALSE);
#else
		mdboot(A_SHUTDOWN, AD_HALT, buffer);
#endif	// SOL_VERSION >= __s11

	} else {

		// Non-global zone - Rebooting zone

		//
		// Check that we are failfasting the zone generation that was
		// intended to be failfasted, and not any future generation
		// of the same zone. zone_uniqid uniquely identifies
		// a zone generation.
		//
		if (zone_is_running(zone_id, zone_uniqid)) {
			CMM_TRACE(("Failfast (stop_node_halt): "
			    "Zone %s (zone id %d) rebooting\n",
			    zone_name, zone_id));
			(void) cmm_syslog_msgp->log(
			    SC_SYSLOG_NOTICE, MESSAGE,
			    "Failfast (stop_node_panic): "
			    "Zone %s (zone ID %d) rebooting\n",
			    zone_name, zone_id);

			reboot_zone(zone_id, zone_uniqid);
		}
	}

}

#else	// (SOL_VERSION >= __s10) && defined(_KERNEL)

//
// Constructor
//
// Create a failfast unit and initialize it. The function to be called
// if the failfast unit is armed it set to the default function, which is
// determined by the configured failfast mode. If the configured failfast
// mode is OFF, we set the function to should_not_happen, and the failfast
// unit will not be armed, as arm will just return if the configured mode
// is OFF.
//
ff_impl::ff_impl(const char *name, ff::ffmode mode) :
	ff_armed(0),
	ff_from_unref(0)
{
	(void) os::strcpy(ff_unit_name, name);

	ff_mode = mode;

	switch (mode) {
	case ff::FFM_DEBUG:
		ff_timeoutf = &ff_impl::stop_node_debug;
		break;
	case ff::FFM_PROM:
		ff_timeoutf = &ff_impl::stop_node_prom;
		break;
	case ff::FFM_HALT:
		ff_timeoutf = &ff_impl::stop_node_halt;
		break;
	case ff::FFM_PANIC:
		ff_timeoutf = &ff_impl::stop_node_panic;
		break;
	case ff::FFM_DEFERRED_PANIC:
		ff_timeoutf = &ff_impl::stop_node_deferred_panic;
		break;
	case ff::FFM_PROXY_PANIC:
		ff_timeoutf = &ff_impl::stop_node_proxy_panic;
		break;
	default:
		ff_timeoutf = &ff_impl::ff_impl_shouldnt_happen;
		break;
	}
	ff_timeout_id = NULL;
	deferred_panic_flag = false;
	bzero(deferred_panic_buf, (size_t)FF_DEFERRED_PANIC_BUF_SIZE);
}

//
// Destructor
//
// Do a consistency check and make sure we are not destroying the failfast unit
// while it has a timeout ticking.
//
// lint complains that functions that might throw exceptions should not be
// invoked in destructors, as destructors are not supposed to throw exceptions.
//lint -e1551
ff_impl::~ff_impl()
{
	ff_timeout_id = NULL;	 // Keep lint happy
	ff_timeoutf = NULL;	 // Keep lint happy
	if (ff_armed) {
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Failfast: Destroying failfast unit %s while armed.",
		    ff_unit_name);
	}
}
//lint +e1551

//
// When a failfast unit times out, this method gets called. We lock the failfast
// unit, see if it is armed, check if failfast is enabled,
// and if so, invoke the method that is determined
// by the mode of the failfast unit. We could find that the failfast unit is
// not armed when we come here because of a race between disarm and
// the delivery of a timeout.
//
void
ff_impl::unit_timedout()
{
	lock();
	if (ff_armed) {
		if (!failfast_disabled) {
			(this->*ff_timeoutf)();
		}
		ff_armed = 0;
	}
	unlock();
}

//
// If the failfast unit is armed when it gets unreferenced,
// disarm the failfast unit and call the timeout function
// indicating that the failfast unit was unreferenced while
// it was armed. At the end, just delete the failfast unit.
//
void
#ifdef DEBUG
ff_impl::_unreferenced(unref_t cookie)
#else
ff_impl::_unreferenced(unref_t)
#endif
{
	//
	// The failfast unit has become unreferenced. We can delete the
	// associated failfast object now. But first let us check to see if
	// a failfast timer is active for this failfast unit.
	// If so, we will disarm it and call the timeout handler with
	// the ff_from_unref flag set.
	//
	// We need to lock the failfast unit as a timeout could happen while
	// we are here.
	//
	lock();

	ASSERT(_last_unref(cookie));

	if ((ff_mode != ff::FFM_OFF) && ff_armed) {
		do_disarm();
		ff_from_unref = 1;
		if (!failfast_disabled) {
			(this->*ff_timeoutf)();
		}
	}
	unlock();

	//
	// If deferred_panic_flag is not set, then this ff_impl object won't
	// be used for deferred panic, and hence its ok to delete this object.
	//
	if (!deferred_panic_flag) {
		delete this;
	}
}

//
// This is the method that failfast units that were created when the configured
// failfast mode is OFF will use. This method will never be invoked because
// if the mode is OFF, arm will never really set a timeout.
//
// lint suggests making this method const.
//lint -e1762
void
ff_impl::ff_impl_shouldnt_happen()
{
	(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "INTERNAL ERROR Failfast: ff_impl_shouldnt_happen.");
}
//lint +e1762

//
// If a failfast unit is set in mode FFM_DEFERRED_PANIC or FFM_PROXY_PANIC,
// this is the method that will get invoked when such a failfast unit
// encounters a timeout.
//
// When this timeout happens, the node should panic after a delay
// (configurable through the CCR). Here we arm the special failfast device
// created for this.
//
void
ff_impl::stop_node_deferred_panic_int(cmm::timeout_t tout)
{
	int delay_seconds = tout/MILLISEC;

	ASSERT(ff_from_unref == 1);

	CMM_TRACE(("Failfast: \"%s\" died; this node will panic "
	    "in %ld seconds\n", ff_unit_name, delay_seconds));

	//
	// This ff_impl failfast unit was armed for deferred panic, and
	// its been unreferenced. So, the mode is changed to FFM_PANIC and
	// the timeout handler is made to point at the panic timeout function,
	// and then the failfast unit is armed, so that a callout block is put
	// for this failfast unit. Thus, when the callout block timeout
	// expires, the panic timeout function would be called for this
	// ff_impl failfast unit, which will take appropriate action
	// based on zone_id and zone_uniqid
	//
	ff_timeoutf = &ff_impl::stop_node_panic;
	deferred_panic_flag = true;
	ff_mode = ff::FFM_PANIC;

	(void) os::snprintf(
	    deferred_panic_buf, (size_t)FF_DEFERRED_PANIC_BUF_SIZE,
	    "Aborting because \"%s\" died %ld seconds ago",
	    ff_unit_name, delay_seconds);

	unlock();
	Environment e;
	arm(tout, e);
	lock();
}

//
// If failfast unit is set in mode FFM_PANIC, this is the method that will
// get invoked when such a failfast unit encounters a timeout.
//
void
ff_impl::stop_node_panic()
{
	//
	// The ff_impl failfast unit was armed for deferred panic, and the
	// deferred timeout has expired. So, failfast the node and
	// use the deferred_panic_buf.
	//
	if (deferred_panic_flag) {
		//
		// Bug 4353082 - Check to see if the number of
		// armed failfast units with mode FFM_DEFERRED_PANIC
		// or FFM_PROXY_PANIC (recorded in the variable
		// ff_admin_impl::ff_armed_count) is zero. This
		// would indirectly indicate that the system is likely
		// being shutdown (since all shutdown commands
		// eventually kill all userland processes).
		// In this case, use mdboot instead of panic. Use
		// panic only if an individual userland daemon has
		// died. This fix is required by PSARC/2000/280.
		//

#ifdef _KERNEL
		ff_admin_impl::the().ff_armed_lock.lock();
		if (ff_admin_impl::the().ff_armed_count != 0) {
			ff_admin_impl::the().ff_armed_lock.unlock();
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC,
			    MESSAGE, "Failfast: %s.", deferred_panic_buf);

		} else {
			ff_admin_impl::the().ff_armed_lock.unlock();
			CMM_TRACE(("Failfast: Halting because all "
			    "userland daemons have died.\n"));
			os::prom_printf("Failfast: Halting because all "
			    "userland daemons have died.\n");

#if (SOL_VERSION >= __s11)
			mdboot(A_SHUTDOWN, AD_HALT, "", B_FALSE);
#else
			mdboot(A_SHUTDOWN, AD_HALT, "");
#endif	// SOL_VERSION >= __s11

			return;
		}

#else	// _KERNEL

		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Failfast: %s.", deferred_panic_buf);
		return;

#endif	// _KERNEL

	}

	char *panicbuf;

	if (ff_from_unref == 1) {
		panicbuf = "unreferenced while Armed";
	} else {
		panicbuf = "";
	}

	CMM_TRACE(("Failfast: timeout - unit \"%s\"%s",
	    ff_unit_name, panicbuf));
	(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "Failfast: timeout - unit \"%s\"%s.", ff_unit_name, panicbuf);
}

//
// If failfast unit is set in mode FFM_HALT, this is the method that will
// get invoked when such a failfast unit encounters a timeout.
//
void
ff_impl::stop_node_halt()
{
	char buffer[ff_MAX_FF_NAME_SIZE + 50];
	char *panicbuf;

	if (ff_from_unref == 1) {
		panicbuf = "unreferenced while Armed";
	} else {
		panicbuf = "";
	}

	os::sprintf(buffer, "Failfast timeout - unit \"%s\" %s",
	    ff_unit_name, panicbuf);

	CMM_TRACE((buffer));

#ifdef _KERNEL
#if (SOL_VERSION >= __s11)
	mdboot(A_SHUTDOWN, AD_HALT, buffer, B_FALSE);
#else
	mdboot(A_SHUTDOWN, AD_HALT, buffer);
#endif	// SOL_VERSION >= __s11
#endif	// _KERNEL

}

#endif // (SOL_VERSION >= __s10) && defined(_KERNEL)


//
// Failfast Admin support
//

//
// Failfast admin constructor.
//
ff_admin_impl::ff_admin_impl() :
	failfast_now_ptr(nil)
{
	intr_run_time = 0;

	// Initialize number of armed userland failfast units to 0.
	ff_armed_count = 0;

	// Initialize the statistics
	clk_count = 0;
	intr_count = 0;

	//
	// This assertion is needed to make sure that interrupts do not
	// compete with clock to do this, instead we want the interrupts
	// to do this only if clock is delayed by some amount of time.
	// If clk_delay_factor is 2, then it means that clock is delayed by
	// "clock tick" time.
	//
	ASSERT(clk_delay_factor > 1);

	intr_run_delta = (hrtime_t)clk_delay_factor *
		drv_hztousec((clock_t)1) * 1000LL;
}

//
// Object initialization. Sets cmm_clock_callout so that the clock handler
// starts calling us every tick.  Also creates a failfast unit to fail the
// node immediately when failfast_now is called.
//
// This is the only ff_admin_impl object that exists in this node. Each
// node has one object of this type. It is used to create other ff_impl
// objects that clients can then use to arm and disarm failfast timers.
// The global admin object cannot be allocated statically. Hence,
// ORB::initialize calls a static function in the failfast_admin class,
// and that static function creates the global admin object.
//
int
ff_admin_impl::initialize_int()
{
	failfast_now_ptr = (new ff_impl("failfast_now", conf.ff_mode))->
	    get_objref();

	if (CORBA::is_nil(failfast_now_ptr)) {
		return (ENOMEM);
	}

	//
	// Make the clock call us every tick.
	// XXX When do we reset this pointer? If the orb fails to load,
	//	it still does not seem to have a _fini. Hence, when the
	//	orb is running in kernel mode, ORB::shutdown does not seem
	//	to get called even if the orb fails to load.
	//

	cmm_clock_callout = (void(*)(void))ff_clock_callout;

	return (0);
}

//
// Failfast admin destroyer.
//
// lint complains that functions that might throw exceptions should not be
// invoked in destructors, as destructors are not supposed to throw exceptions.
//lint -e1551
ff_admin_impl::~ff_admin_impl()
{
	cmm_clock_callout = NULL;
	CORBA::release(failfast_now_ptr);
	failfast_now_ptr = nil;
}
//lint +e1551

//
// Failfast admin unreferenced method. Called during clean shutdown.
//
void
#ifdef DEBUG
ff_admin_impl::_unreferenced(unref_t cookie)
#else
ff_admin_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Interface Methods
//

//
// Create a failfast object and return its reference to the caller.
// The caller can then use the returned failfast object to arm it, disarm it
// and other failfast operations.
//
// Creation simply involves invoking the new operator on the failfast object
// and returning a reference to it by calling the returned failfast object's
// get_objref.
//
ff::failfast_ptr
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
ff_admin_impl::ff_create(const char *name, ff::ffmode mode, Environment &e)
#else
ff_admin_impl::ff_create(const char *name, ff::ffmode mode, Environment &)
#endif
{
	ff_impl *new_ff_ptr;

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	new_ff_ptr =
	    new ff_impl(name, mode, e.get_zone_id(), e.get_zone_uniqid());
#else
	new_ff_ptr = new ff_impl(name, mode);
#endif

	return (new_ff_ptr->get_objref());
}

//
// The method below can be used to trigger a failfast immediately. One does
// not need a failfast unit in order to trigger such a failfast.
//
#if (SOL_VERSION >= __s10) && !defined(UNODE)
#ifdef _KERNEL

//
// A global zone entity can use this interface to create a failfast object
// for a non-global zone (virtual cluster)
//
// Create a failfast object and return its reference to the caller.
// The caller can then use the returned failfast object to arm it, disarm it
// and other failfast operations.
//
// Creation simply involves invoking the new operator on the failfast object
// and returning a reference to it by calling the returned failfast object's
// get_objref.
//
ff::failfast_ptr
ff_admin_impl::vc_ff_create(
    const char *cl_namep, const char *namep, ff::ffmode mode, Environment &e)
{
	ff_impl *new_ff_ptr;

	sol::zoneid_t zoneid = ::zonename_to_zoneid(cl_namep);
	if (zoneid == -1) {
		// Zone might not be up
		return (ff::failfast::_nil());
	}

	int64_t zone_uniqid = zoneid_to_zoneuniqid(zoneid);
	if (zone_uniqid == -1) {
		// Zone might not be up
		return (ff::failfast::_nil());
	}

	if (e.get_zone_id() != GLOBAL_ZONEID) {
		if ((e.get_zone_id() != zoneid) ||
		    (e.get_zone_uniqid() != zone_uniqid)) {
			// Zone boundary violation
			ff::zone_violation *exceptionp = new ff::zone_violation;
			exceptionp->current_zone = e.get_zone_id();
			exceptionp->target_zone = zoneid;
			e.exception(exceptionp);
			return (ff::failfast::_nil());
		}
	}

	new_ff_ptr =
	    new ff_impl(namep, mode, zoneid, zone_uniqid);
	return (new_ff_ptr->get_objref());
}

bool
ff_admin_impl::failfast_now(sol::zoneid_t zone_id, Environment &e)
{
	if (!node_level_failfast_enabled) {
		// Node-level failfast is disabled. So return.
		return (false);
	}

	int64_t zone_uniqid = zoneid_to_zoneuniqid(zone_id);

	//
	// Currently we do not allow a command in a non-global zone
	// to failfast another zone on the same node
	//
	if (e.get_zone_uniqid() != GLOBAL_ZONEUNIQID) {
		if (zone_uniqid != (int64_t)e.get_zone_uniqid()) {
			ff::zone_violation *exceptionp = new ff::zone_violation;
			exceptionp->current_zone = e.get_zone_id();
			exceptionp->target_zone = zone_id;
			e.exception(exceptionp);
			return (false);
		}
	}

	//
	// If the zone_id argument corresponds to global zone,
	// then the node is 'failfast'ed immediately.
	// If the zone_id argument corresponds to a non-global zone,
	// then the zone is 'failfast'ed immediately.
	//
	// Note that there is a possible security threat with this method.
	// That is, some non-global zone thread on node A can get
	// the ff_admin_impl object reference for node B, and then invoke
	// this method. In that case, since the Environment is not passed
	// across for remote node invocations, so this method invocation gets
	// an Environment with its zone information set as the global zone
	// (on node B). So, node B can be aborted by a non-global zone thread
	// on node A.
	//

	if (zone_id != GLOBAL_ZONEID) {

		non_global_zones_failfast_lock.lock();

		//
		// If failfast is disabled for the non-global zone,
		// or if the zone is not running, then do not failfast the zone.
		//
		if (zone_is_present_in_hashtab((uint64_t)zone_uniqid)) {

			if (failfast_is_disabled((uint64_t)zone_uniqid) ==
			    FF_STATE_DISABLED) {
				non_global_zones_failfast_lock.unlock();
				return (false);
			}

			if (zone_is_running(zone_id, (uint64_t)zone_uniqid)) {

				//
				// Disable failfast for this zone so that other
				// failfast units for this zone do not trigger
				//
				zone_failfast_disable((uint64_t)zone_uniqid);
				mark_zone_is_going_down((uint64_t)zone_uniqid);

				non_global_zones_failfast_lock.unlock();

				if (zone_uniqid ==
				    (int64_t)e.get_zone_uniqid()) {

					//
					// Command inside the current zone wants
					// to failfast the same zone.
					//
					if (!failfast_current_zone(
					    zone_id, (uint64_t)zone_uniqid)) {
						return (false);
					}
				} else {
					reboot_zone(
					    zone_id, (uint64_t)zone_uniqid);
				}

				non_global_zones_failfast_lock.lock();
			}

			non_global_zones_failfast_lock.unlock();

		} else {
			//
			// Zone is not present in the hash table.
			// No need to update failfast hashtable.
			// Simply failfast the zone if the zone is running.
			//
			non_global_zones_failfast_lock.unlock();

			if (zone_is_running(zone_id, (uint64_t)zone_uniqid)) {

				if (zone_uniqid ==
				    (int64_t)e.get_zone_uniqid()) {
					//
					// Command inside the current zone wants
					// to failfast the same zone.
					//
					if (!failfast_current_zone(
					    zone_id, (uint64_t)zone_uniqid)) {
						return (false);
					}
				} else {
					reboot_zone(
					    zone_id, (uint64_t)zone_uniqid);
				}
			}
		}

	} else {
		// Global zone :  Arm the special failfast object
		failfast_now_ptr->arm(0, e);
	}

	return (true);
}

// These interfaces are not present for unode compilation

// Set the node level failfast control for this node
void
ff_admin_impl::set_node_level_failfast_control(bool on, Environment&)
{
	node_level_failfast_enabled = on;
}

// Return the node level failfast control state for this node
bool
ff_admin_impl::get_node_level_failfast_control(Environment&)
{
	return (node_level_failfast_enabled);
}

// Set the zone level failfast control for a zone (global or non-global)
void
ff_admin_impl::set_zone_level_failfast_control(
    bool on, sol::zoneid_t target_zone_id, Environment &e)
{
	int64_t target_zone_uniqid;

	if (target_zone_id == GLOBAL_ZONEID) {
		// A non-global zone cannot set the global zone's controls.
		if (e.get_zone_id() != GLOBAL_ZONEID) {
			ff::zone_violation *exceptionp = new ff::zone_violation;
			exceptionp->current_zone = e.get_zone_id();
			exceptionp->target_zone = target_zone_id;
			e.exception(exceptionp);
			return;
		}
		global_zone_failfast_enabled = on;

	} else {
		target_zone_uniqid = zoneid_to_zoneuniqid(target_zone_id);
		if (target_zone_uniqid == -1) {	// zone does not exist
			return;
		}
		//
		// Currently we do not allow a command in a non-global zone
		// to change the failfast control of another zone
		//
		if (e.get_zone_uniqid() != GLOBAL_ZONEUNIQID) {
			if (target_zone_uniqid !=
			    (int64_t)e.get_zone_uniqid()) {
				ff::zone_violation *exceptionp =
				    new ff::zone_violation;
				exceptionp->current_zone = e.get_zone_id();
				exceptionp->target_zone = target_zone_id;
				e.exception(exceptionp);
				return;
			}
		}

		non_global_zones_failfast_lock.lock();

		if (on) {
			zone_failfast_enable((uint64_t)target_zone_uniqid);
		} else {
			zone_failfast_disable((uint64_t)target_zone_uniqid);
		}

		non_global_zones_failfast_lock.unlock();
	}
}

//
// Return the zone level failfast control value
// for a zone (global or non-global).
//
bool
ff_admin_impl::get_zone_level_failfast_control(
    sol::zoneid_t target_zone_id, Environment &e)
{
	int64_t target_zone_uniqid;
	failfast_state_t return_value;

	if (target_zone_id == GLOBAL_ZONEID) {
		if (e.get_zone_id() != GLOBAL_ZONEID) {
			//
			// A non-global zone cannot query the zone level
			// failfast control status of the global zone.
			// Return false as the default value.
			//
			ff::zone_violation *exceptionp = new ff::zone_violation;
			exceptionp->current_zone = e.get_zone_id();
			exceptionp->target_zone = target_zone_id;
			e.exception(exceptionp);
			return (false);
		}
		return (global_zone_failfast_enabled);

	} else {
		target_zone_uniqid = zoneid_to_zoneuniqid(target_zone_id);
		if (target_zone_uniqid == -1) {	// zone does not exist
			// return false by default
			return (false);
		}
		//
		// Currently we do not allow a command in a non-global zone
		// to query the failfast control status of another zone
		//
		if (e.get_zone_uniqid() != GLOBAL_ZONEUNIQID) {
			if (target_zone_uniqid !=
			    (int64_t)e.get_zone_uniqid()) {
				// Return false as the default value
				ff::zone_violation *exceptionp =
				    new ff::zone_violation;
				exceptionp->current_zone = e.get_zone_id();
				exceptionp->target_zone = target_zone_id;
				e.exception(exceptionp);
				return (false);
			}
		}

		non_global_zones_failfast_lock.lock();

		if (zone_is_present_in_hashtab((uint64_t)target_zone_uniqid)) {

			return_value =
			    failfast_is_disabled((uint64_t)target_zone_uniqid);
			non_global_zones_failfast_lock.unlock();

			if (return_value == FF_STATE_ENABLED) {
				return (true);
			} else {
				return (false);
			}
		} else {
			//
			// Zone entry is not present in hashtable.
			// Return false as the default value
			//
			non_global_zones_failfast_lock.unlock();
			return (false);
		}
	}
}

// Returns -1 if zone does not exist, else returns the zone id
sol::zoneid_t
ff_admin_impl::zonename_to_zoneid(const char *zone_name, Environment&)
{
	return (::zonename_to_zoneid(zone_name));
}

#else	// _KERNEL

//
// These IDL interfaces are compiled for userland,
// but there should not be any userland ff_admin_impl objects.
// So, these methods simply assert, if called.
//
void
ff_admin_impl::failfast_now(sol::zoneid_t zone_id, Environment&)
{
	ASSERT(0);	// Should not be reached
}

void
ff_admin_impl::set_node_level_failfast_control(bool on, Environment&)
{
	ASSERT(0);	// Should not be reached
}

bool
ff_admin_impl::get_node_level_failfast_control(Environment&)
{
	ASSERT(0);	// Should not be reached
}

void
ff_admin_impl::set_zone_level_failfast_control(
    bool on, sol::zoneid_t target_zone_id, Environment&)
{
	ASSERT(0);	// Should not be reached
}

bool
ff_admin_impl::get_zone_level_failfast_control(
    sol::zoneid_t target_zone_id, Environment&)
{
	ASSERT(0);	// Should not be reached
}

sol::zoneid_t
ff_admin_impl::zonename_to_zoneid(const char *zone_name, Environment&)
{
	ASSERT(0);	// Should not be reached
}

#endif	// _KERNEL

#else	// (SOL_VERSION >= __s10) && !defined(UNODE)

void
ff_admin_impl::failfast_now(Environment &e)
{
	// Arm the special failfast object
	failfast_now_ptr->arm(0, e);
}
//
// The method below can be used to disable failfasts temporarily.
// They will be disabled until enable_failfasts() is called.
// The return value indicates whether the call had an effect.
//
bool
ff_admin_impl::disable_failfast(Environment &)
{
	if (!failfast_disabled) {
		failfast_disabled = true;
		return (true);
	} else {
		return (false);
	}
}

//
// The method below can be used to re-enable failfasts after they have been
// disabled by disable_failfast().  The return value indicates whether call
// had an effect
//
bool
ff_admin_impl::enable_failfast(Environment &)
{
	if (failfast_disabled) {
		failfast_disabled = false;
		return (true);
	} else {
		return (false);
	}
}

#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)


#ifdef _KERNEL
//
// The following method sets Real Time priority for the caller thread
// (we assume here that the caller process is single threaded, and
// so, 'proctot' macro gives the kthread_t pointer from proc_t pointer)
//
// Processes running inside a non-global zone cannot change their
// priority to Real Time. That is why the priority of the caller process
// is set in this routine and not in the caller process itself.
//
int
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
ff_admin_impl::set_rt_priority(int pid_arg, int rtpri, Environment &e)
#else
ff_admin_impl::set_rt_priority(int pid_arg, int rtpri, Environment &)
#endif
{
	pcparms_t	pc_parms;
	pcpri_t		pc_pri;
	pri_t		maxpri;
	rtparms_t	*rt_parmsp = (rtparms_t *)(pc_parms.pc_clparms);
	int		error	= 0;

	if (getcid("RT", &pc_parms.pc_cid) != 0 ||
	    CL_GETCLPRI(&sclass[pc_parms.pc_cid], &pc_pri) != 0) {

		CMM_TRACE(("set_rt_priority : RT class not "
		    "configured in this system\n"));
		//
		// SCMSGS
		// @explanation
		// Real time class is not configured in this system, but
		// a task needs to run with real time priority.
		// @user_action
		// Configure real time priority class for the system.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast : Real Time class not configured "
		    "in this system");
		return (-1);
	} else {
		CMM_TRACE(("pc_pri.pc_clpmax = %d  pc_pri.pc_clpmin %d\n",
		    pc_pri.pc_clpmax, pc_pri.pc_clpmin));
		maxpri = pc_pri.pc_clpmax - pc_pri.pc_clpmin;
	}

	mutex_enter(&pidlock);
	proc_t *caller_processp = prfind(pid_arg);

	if (rtpri > maxpri) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for RT : thread %p zone %d\n", rtpri,
		    maxpri, proctot(caller_processp), e.get_zone_id()));
#else
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for RT : thread %p\n", rtpri, maxpri,
		    proctot(caller_processp)));

#endif // SOL_VERSION
		mutex_exit(&pidlock);
		return (-1);
	}

	rtkparms_t *rtpp = (rtkparms_t *)pc_parms.pc_clparms;
	rtpp->rt_pri = rtpri;
	//
	// Set the time quantum to the deafult.
	//
	rtpp->rt_tqntm = RT_TQDEF;
	rtpp->rt_tqsig = 0;
	//
	// Enable changing of priority, time quantum and time quantum
	// signal for RT.
	//
	rtpp->rt_cflags = RT_DOPRI | RT_DOTQ | RT_DOSIG;

	mutex_enter(&caller_processp->p_lock);
	error = parmsset(&pc_parms, proctot(caller_processp));
	mutex_exit(&caller_processp->p_lock);

	mutex_exit(&pidlock);

	if (error != 0) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("Unable to set scheduling params "
		    "for RT thread : thread %p errno %d zone %d\n",
		    proctot(caller_processp), error, e.get_zone_id()));
#else
		CMM_TRACE(("Unable to set scheduling params "
		    "for RT thread : thread %p errno %d\n",
		    proctot(caller_processp), error));
#endif
		return (-1);
	}
	return (0);
}

//
// This function sets the priority of the calling thread to be in
// time shared scheduling class.
//
int
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
ff_admin_impl::set_ts_priority(int pid_arg, int tspri, Environment &e)
#else
ff_admin_impl::set_ts_priority(int pid_arg, int tspri, Environment &)
#endif
{
	pcparms_t	pc_parms;
	pcpri_t		pc_pri;
	pri_t		maxpri;
	int		error = 0;
	tsparms_t	*ts_parmsp = (tsparms_t *)(pc_parms.pc_clparms);

	if ((getcid("TS", &pc_parms.pc_cid) != 0) ||
	    (CL_GETCLPRI(&sclass[pc_parms.pc_cid], &pc_pri) != 0)) {
		CMM_TRACE(("set_ts_priority : TS class not "
		    "configured in this system\n"));
		//
		// SCMSGS
		// @explanation
		// Time shared class is not configured in this system, but
		// a task needs to run in time shared scheduling class.
		// @user_action
		// Configure Time shared priority class for the system.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast : Time shared class not configured "
		    "in this system");
		return (-1);
	} else {
		CMM_TRACE(("pc_pri.pc_clpmax = %d  pc_pri.pc_clpmin %d\n",
		    pc_pri.pc_clpmax, pc_pri.pc_clpmin));
		maxpri = pc_pri.pc_clpmax - pc_pri.pc_clpmin;
	}

	mutex_enter(&pidlock);
	proc_t *caller_processp = prfind(pid_arg);

	if (tspri > maxpri) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for TS : thread %p zone %d\n", tspri,
		    maxpri, proctot(caller_processp), e.get_zone_id()));
#else
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for TS : thread %p\n", tspri, maxpri,
		    proctot(caller_processp)));

#endif // SOL_VERSION
		mutex_exit(&pidlock);
		return (-1);
	}

	ts_parmsp->ts_uprilim = tspri;
	ts_parmsp->ts_upri = tspri;

	mutex_enter(&caller_processp->p_lock);
	error = parmsset(&pc_parms, proctot(caller_processp));
	mutex_exit(&caller_processp->p_lock);
	mutex_exit(&pidlock);

	if (error != 0) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("Unable to set scheduling params "
		    "for TS thread : thread %p zone %d\n",
		    proctot(caller_processp), e.get_zone_id()));
#else
		CMM_TRACE(("Unable to set scheduling params "
		    "for TS thread : thread %p\n", proctot(caller_processp)));
#endif
		return (-1);
	}

	return (0);
}

//
// This function sets the priority of the calling thread to be in
// Fair share scheduling class.
//
int
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
ff_admin_impl::set_fss_priority(int pid_arg, int fsspri, Environment &e)
#else
ff_admin_impl::set_fss_priority(int pid_arg, int fsspri, Environment &)
#endif
{
	pcparms_t	pc_parms;
	pcpri_t		pc_pri;
	pri_t		maxpri;
	int		error = 0;

	fssparms_t *fss_parmsp = (fssparms_t *)(pc_parms.pc_clparms);

	if (getcid("FSS", &pc_parms.pc_cid) != 0 ||
	    CL_GETCLPRI(&sclass[pc_parms.pc_cid], &pc_pri) != 0) {
		CMM_TRACE(("set_FSS_priority : FSS class not "
		    "configured in this system\n"));
		//
		// SCMSGS
		// @explanation
		// Fair share scheduling class is not configured in this
		// system, but a task needs to run in fair share scheduling
		// class.
		// @user_action
		// Configure fair share priority class for the system.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast : Fair share class not configured "
		    "in this system");
		return (-1);
	} else {
		CMM_TRACE(("pc_pri.pc_clpmax = %d  pc_pri.pc_clpmin %d\n",
		    pc_pri.pc_clpmax, pc_pri.pc_clpmin));
		maxpri = pc_pri.pc_clpmax - pc_pri.pc_clpmin;
	}

	proc_t *caller_processp = prfind(pid_arg);
	mutex_enter(&pidlock);

	if (fsspri > maxpri) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for FSS : thread %p zone %d\n", fsspri,
		    maxpri, proctot(caller_processp), e.get_zone_id()));
#else
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for FSS : thread %p\n", fsspri, maxpri,
		    proctot(caller_processp)));

#endif // SOL_VERSION
		mutex_exit(&pidlock);
		return (-1);
	}

	fss_parmsp->fss_uprilim = fsspri;
	fss_parmsp->fss_upri = fsspri;

	mutex_enter(&caller_processp->p_lock);
	error = parmsset(&pc_parms, proctot(caller_processp));
	mutex_exit(&caller_processp->p_lock);
	mutex_exit(&pidlock);

	if (error != 0) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("Unable to set scheduling params "
		    "for FSS thread : thread %p zone %d\n",
		    proctot(caller_processp), e.get_zone_id()));
#else
		CMM_TRACE(("Unable to set scheduling params "
		    "for FSS thread : thread %p\n", proctot(caller_processp)));
#endif
		return (-1);
	}

	return (0);
}

//
// This function sets the priority of the calling thread to be in
// time shared scheduling class.
//
int
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
ff_admin_impl::set_fx_priority(int pid_arg, int fxpri, Environment &e)
#else
ff_admin_impl::set_fx_priority(int pid_arg, int fxpri, Environment &)
#endif
{
	pcparms_t	pc_parms;
	pcpri_t		pc_pri;
	pri_t		maxpri;
	int		error = 0;

	if (getcid("FX", &pc_parms.pc_cid) != 0 ||
	    CL_GETCLPRI(&sclass[pc_parms.pc_cid], &pc_pri) != 0) {
		CMM_TRACE(("set_FX_priority : FX class not "
		    "configured in this system\n"));
		//
		// SCMSGS
		// @explanation
		// Fixed scheduling class is not configured in this system,
		// but a task needs to run in fixed scheduling class.
		// @user_action
		// Configure Fixed scheduling class for the system.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failfast : Fixed priority class not configured "
		    "in this system");
		return (-1);
	} else {
		CMM_TRACE(("pc_pri.pc_clpmax = %d  pc_pri.pc_clpmin %d\n",
		    pc_pri.pc_clpmax, pc_pri.pc_clpmin));
		maxpri = pc_pri.pc_clpmax - pc_pri.pc_clpmin;
	}

	mutex_enter(&pidlock);
	proc_t *caller_processp = prfind(pid_arg);

	if (fxpri > maxpri) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for FX : thread %p zone %d\n", fxpri,
		    maxpri, proctot(caller_processp), e.get_zone_id()));
#else
		CMM_TRACE(("The priority %d specified is greater than the"
		    " max priority %d for FX : thread %p\n", fxpri, maxpri,
		    proctot(caller_processp)));

#endif // SOL_VERSION
		mutex_exit(&pidlock);
		return (-1);
	}

	fxkparms_t *fx_parmsp = (fxkparms_t *)pc_parms.pc_clparms;
	fx_parmsp->fx_uprilim = fxpri;
	fx_parmsp->fx_upri = fxpri;
	//
	// Set time quantum to default.
	//
	fx_parmsp->fx_tqntm = FX_TQDEF;
	//
	// Enable changing of priority and time quantum for fixed priority
	// scheduling class.
	//
	fx_parmsp->fx_cflags = FX_DOUPRILIM | FX_DOUPRI | FX_DOTQ;

	mutex_enter(&caller_processp->p_lock);
	error = parmsset(&pc_parms, proctot(caller_processp));
	mutex_exit(&caller_processp->p_lock);
	mutex_exit(&pidlock);

	if (error != 0) {
#if (SOL_VERSION >= __s10)
		CMM_TRACE(("Unable to set scheduling params "
		    "for FX thread : thread %p errno %d zone %d\n",
		    proctot(caller_processp), error, e.get_zone_id()));
#else
		CMM_TRACE(("Unable to set scheduling params "
		    "for FX thread : thread %p errno %d\n",
		    proctot(caller_processp), error));
#endif
		return (-1);
	}

	return (0);
}

#else	// _KERNEL
//
// User level emulation of setting real time priority for caller
// thread. Uses priocntl
//

//
// get process scheduling class
//
static idtype_t
get_classid(char *classnamep, pid_t pid_arg)
{
	pcinfo_t pc_info;

	(void) strcpy(pc_info.pc_clname, classnamep);

	if ((priocntl(P_PID, pid_arg, PC_GETCID, (char *)&pc_info)) == -1) {
		ASSERT(0);
	}

	return ((idtype_t)pc_info.pc_cid);
}


//
// The following method sets Real Time priority for the caller thread
// Used by failfastd only
//
int
ff_admin_impl::set_rt_priority(int pid_arg, int rtprio, Environment&)
{
	pcinfo_t	pc_info;
	pcparms_t	pc_parms;
	pri_t		maxpri	= 0;
	rtparms_t	*rt_parmsp = (rtparms_t *)(pc_parms.pc_clparms);

	(void) strcpy(pc_info.pc_clname, "RT");

	if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
	    != -1) {
		rtinfo_t *rt_info = (rtinfo_t *)(pc_info.pc_clinfo);
		maxpri = rt_info->rt_maxpri;
	} else {
		return ((int)errno);
	}

	pc_parms.pc_cid = pc_info.pc_cid;

	if (rtprio > maxpri) {
		return (-1);
	}
	rt_parmsp->rt_pri = rtprio;

	//
	// Set the timeslice to default.
	//
	rt_parmsp->rt_tqnsecs = RT_TQDEF;
	rt_parmsp->rt_tqsecs  = 0;

	if ((priocntl(P_PID, pid_arg, PC_SETPARMS,
	    (char *)&pc_parms)) == -1) {
		return ((int)errno);
	}

	return (0);
}

//
// This method sets time shared priority for the called thread.
//
int
ff_admin_impl::set_ts_priority(int pid_arg, int tsprio, Environment &)
{
	pcinfo_t	pc_info;
	pcparms_t	pc_parms;
	pri_t		maxpri;
	tsparms_t	*tsp = (tsparms_t *)(pc_parms.pc_clparms);

	(void) strcpy(pc_info.pc_clname, "TS");

	if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
	    != -1) {
		tsinfo_t *ts_info = (tsinfo_t *)(pc_info.pc_clinfo);
		maxpri = ts_info->ts_maxupri;
	} else {
		return ((int)errno);
	}

	if (tsprio >  maxpri) {
		return (-1);
	}

	pc_parms.pc_cid = pc_info.pc_cid;

	tsp->ts_uprilim = tsprio;
	tsp->ts_upri	= tsprio;

	if (priocntl(P_PID, P_MYID, PC_SETPARMS,
		(char *)&pc_parms) != 0) {
		return ((int)errno);
	}
	return (0);
}

//
// This method sets the Fair share priority for the called thread.
//
int
ff_admin_impl::set_fss_priority(int pid_arg, int fssprio, Environment &)
{
	pcinfo_t	pc_info;
	pcparms_t	pc_parms;
	pri_t		maxpri;
	fssparms_t	*fsp = (fssparms_t *)(pc_parms.pc_clparms);

	(void) strcpy(pc_info.pc_clname, "FSS");

	if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
	    != -1) {
		fssinfo_t *fss_info = (fssinfo_t *)(pc_info.pc_clinfo);
		maxpri = fss_info->fss_maxupri;
	} else {
		return ((int)errno);
	}

	if (fssprio >  maxpri) {
		return (-1);
	}

	pc_parms.pc_cid = pc_info.pc_cid;

	fsp->fss_uprilim = fssprio;
	fsp->fss_upri	= fssprio;

	if (priocntl(P_PID, P_MYID, PC_SETPARMS,
		(char *)&pc_parms) != 0) {
		return ((int)errno);
	}
	return (0);
}

//
// This method sets the scheduling class to Fixed priority for the
// called thread.
//
int
ff_admin_impl::set_fx_priority(int pid_arg, int fxprio, Environment &)
{
	pcinfo_t	pc_info;
	pcparms_t	pc_parms;
	pri_t		maxpri;
	fxparms_t	*fxp = (fxparms_t *)(pc_parms.pc_clparms);

	(void) strcpy(pc_info.pc_clname, "FX");

	if (priocntl((idtype_t)0, (id_t)0, PC_GETCID, (char *)&pc_info)
	    != -1) {
		fxinfo_t *fx_info = (fxinfo_t *)(pc_info.pc_clinfo);
		maxpri = fx_info->fx_maxupri;
	} else {
		return ((int)errno);
	}

	if (fxprio >  maxpri) {
		return (-1);
	}

	pc_parms.pc_cid = pc_info.pc_cid;

	fxp->fx_uprilim = fxprio;
	fxp->fx_upri	= fxprio;
	//
	// Set the timeslice to default.
	//
	fxp->fx_tqnsecs = FX_TQDEF;
	fxp->fx_tqsecs  = 0;

	if (priocntl(P_PID, P_MYID, PC_SETPARMS,
		(char *)&pc_parms) != 0) {
		return ((int)errno);
	}
	return (0);
}
#endif	/* _KERNEL */

//
// Internal use methods
//

//
// This method is called from ORB::initialize and is responsible for
// allocating the one and only ff_admin object that exists in each node.
// It also makes the failfast functionality known to the rest of
// the world by adding this object to the local name server.  It also
// initializes the ff_impl static variables.
//
int
ff_admin_impl::initialize()
{
	int err = 0;

	the_ff_adminp = new ff_admin_impl();

	if (the_ff_adminp == NULL) {
		return (ENOMEM);
	}

	if ((err = the_ff_adminp->initialize_int()) != 0) {
		delete the_ff_adminp;
		return (err);
	}

	ff::failfast_admin_var tmp_ref = the_ff_adminp->get_objref();
	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	if (CORBA::is_nil(ctxp)) {
		//
		// TODO:  Need to decide what to do for
		// release code. For debug code, assert
		//
		ASSERT(0);
	}

	Environment e;

	ctxp->bind(ff_admin_name, tmp_ref, e);
	if (e.exception()) {
		e.exception()->print_exception("bind:");
		e.clear();
		return (EIO);
	}

	return (0);
}

//
// Called during shutdown after the CMM has been stopped.
//
void
ff_admin_impl::shutdown()
{
	Environment e;
	naming::naming_context_var ctxp = ns::local_nameserver_c1();
	if (CORBA::is_nil(ctxp)) {
		//
		// TODO:  Need to decide what to do for
		// release code. For debug code, assert
		//
		ASSERT(0);
	}
	ctxp->unbind(ff_admin_name, e);
	e.clear();
}

//
// The clock interrupt handler calls this routine every clock tick. This
// happens because we put the address of this routine into the global pointer
// cmm_clock_callout, which is the pointer that the clock interrupt handler
// uses to make this callback. In this routine, we will see if we need to
// timeout any failfast unit. We will also call callback routines for the
// the CMM automaton and the Path Manager if they have set their respective
// function pointers.
//
void
ff_admin_impl::ff_clock_callout(void)
{
	the().sc_per_tick_processing(CLOCK_THRD);
}

//
// This routine is called by both clock and interrupt (through
// hb_threadpool::send_heartbeat()) to do the per_tick processing.
// Interrupt will execute this only if clock got delayed by some time
// controlled by clk_delay_factor variable. It does not compete with
// clock. Interrupts do speculative check first and then re-check it
// after acquiring lock.
//
void
ff_admin_impl::sc_per_tick_processing(callout_caller_t who)
{
	void (*funcp)(void);
	hrtime_t cur_time = os::gethrtime();

	if (who == INTRPT_THRD) {
		if (cur_time < intr_run_time) {
			return;
		}
	}

	//
	// Try to acquire the per_tick_lock. If the lock can not be acquired,
	// this means that someone is already doing the per tick processing
	// so we can return as we are sure that the processing is being done
	// (either by a clock interrupt or a network interrupt) and we do not
	// want the clock and network interrupts to compete.
	//
	if (per_tick_lock.try_lock() == 0) {
		return;
	}

	if (who == INTRPT_THRD) {
		if (cur_time < intr_run_time) {
			per_tick_lock.unlock();
			return;
		}
		intr_count++;
	} else {
		clk_count++;
	}

	// update the intr_run_time to a future time
	intr_run_time = cur_time + intr_run_delta;

	//
	// Call the ff_callout_table per tick processing to see if we
	// need to timeout any failfast units.
	//
	ff_callout_table::per_tick_processing();

	// call the automaton per tick processing routine (if funcp is valid).
	if ((funcp = cmm_automaton_callout) != NULL) {
		(*funcp)();
	}

	per_tick_lock.unlock();
}
