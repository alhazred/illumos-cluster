//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)callback_registry_impl.cc	1.71	08/05/20 SMI"

#include <orb/fault/fault_injection.h>

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/common_threadpool.h>
#include <cmm/cmm_debug.h>
#include <cmm/callback_registry_impl.h>
#include <cmm/cmm_impl.h>
#include <cmm/cmm_ns.h>
#include <h/ff.h>

#ifdef CMM_VERSION_0
//
// Used to activate the old or the new set of CMM callback
// steps.
//
void (*select_states_v0_funcp)() = NULL;
void (*select_states_v1_funcp)() = NULL;

#endif // CMM_VERSION_0

//
// When timing client callbacks, the CMM should use a timeout a little
// larger than the timeout used by the clients because the CMM starts its
// timer before the client does. We initialize the cumulative timeout with
// the following timing delta to accomplish this.
//
#define	CMM_TIMEOUT_DELTA	500	/* milliseconds */

class callback_sync;
class thread_args;

class cb {

	friend class callback_registry_impl; // accesses cmm_callback_thread

public:
	cb() : _error(false), _counter(0) {};
	cb(int32_t count);
	virtual ~cb();

	virtual void 	call(thread_args *, Environment&) = 0;
	void		exec_callbacks(callback_sync &, cmm::timeout_t,
			    os::hrtime_t start_time = 0,
			    char *step_name = NULL);
	bool		get_error();	// return true on error
protected:

private:
	os::mutex_t _lock;	// lock protecting everything

	//
	// All callbacks share a common cb object on the stack of the function
	// that calls cb::exec_callbacks().  The _error field gets initialized
	// to "no error" prior to running the callbacks.  _error may get set to
	// "error" by any or all of the simultaneously running callbacks,
	// indicating that some error occurred.
	//
	bool _error;  // one or more callbacks returned with error

	os::condvar_t	_completion_cv;	// wait for all callbacks to complete,
	int32_t		_counter;	// i.e., when _counter reaches 0

	static void 	cmm_callback_thread(void *);
	void 		do_callback(thread_args *);
	void		callback_timedout();
};

class cb_start : public cb {
public:
	cb_start() {};
	cb_start(int32_t count) : cb(count) {};
	void call(thread_args *, Environment&);
};

class cb_stop : public cb {
public:
	cb_stop() {};
	cb_stop(int32_t count) : cb(count) {};
	void call(thread_args *, Environment&);
};

class cb_return : public cb {
public:
	cb_return() {};
	cb_return(int32_t count) : cb(count) {};
	void call(thread_args *, Environment&);
};

class cb_abort : public cb {
public:
	cb_abort() {};
	cb_abort(int32_t count) : cb(count) {};
	void call(thread_args *, Environment&);
};

class cb_set_reconf_data : public cb {
private:
	cmm::seqnum_t			_seqnum;
	cmm::membership_t		_mbrship;
public:
	/*CSTYLED*/
	//lint -esym(1712, cb_set_reconf_data); no default constructor
	cb_set_reconf_data(int32_t count, cmm::seqnum_t sn,
	    const cmm::membership_t &membership)
		: cb(count), _seqnum(sn), _mbrship(membership) {};
	void call(thread_args *, Environment&);
};

class cb_step : public cb {
private:
	uint_t				_step_num;
public:
	cb_step() : _step_num(0) {};
	cb_step(int32_t count, uint_t step)
		: cb(count), _step_num(step) {};
	void call(thread_args *, Environment&);
};

//
// callback_sync is a class used to simplify the communication between
// the calling thread (transitions_thread or abort_thread) and the
// threads (instances of cb::cmm_callback_thread) that actually do the
// callbacks.  The threads that do the callbacks share an instance of
// this class, either cbsyncs::normal() or cbsyncs::abort().  That group
// of threads wait on "cv" until work is to be done, at which point
// curr_callback and callback_num have been set up and the cv is
// broadcasted to wake up all of the threads in the group.
//

class callback_sync {

	//
	// Both friend classes access the lock and boolean variables.
	//
	friend class callback_registry_impl;
	friend class cb;
public:
	callback_sync();
	~callback_sync();
	void complete_initialization();
	void add_client_info(cmm::callback_ptr callback_p);
	void set_timer(cmm::timeout_t timeout);	// arm failfast timer
	void reset_timer();			// disarm failfast timer
	void add_next_callback(cb *cbp);
	cb *get_next_callback(int32_t *last_callback_num);

private:
	bool		step_callbacks_in_progress;
	bool		need_to_terminate_callbacks;
	//
	// if terminating callbacks, this is the sequence number of
	// the reconfiguration under which these callbacks were
	// executing.
	//
	cmm::seqnum_t	seqnum_to_be_terminated;
	os::condvar_t	cv;
	os::mutex_t	lck;
	cb		*curr_callback;
	int32_t		callback_num;
	ff::failfast_ptr _callback_ff;	// failfast unit to time out
					// callbacks
	uint_t		num_waiters;	// The current number of cv waiters
	//
	// chain to store client callback structs. This is used to
	// call the client terminate_step functions when the CMM automaton
	// needs to start a new reconfiguration quickly.
	//
	typedef struct client_info {
		cmm::callback_ptr	callback_p;
		struct client_info	*next;
	} client_info_t;
	client_info_t			*client_info_chain;

	//
	// Method to invoke all the client terminate_trans callbacks.
	//
	void	terminate_callbacks();
};

//
// Class to defer the deletion of the callback_sync objects in
// cbsyncs::shutdown(). cbsyncs::shutdown() is called from
// callback_registry_impl::_unreferenced() and is not allowed to make
// blocking calls.
//
class cbs_unref_task : public defer_task {
public:
	void execute();
};

class cbsyncs {
public:
	static void initialize();
	static void shutdown();
	static void shutdown_int();
	static callback_sync &normal();
	static callback_sync &abort();
private:
	static cbs_unref_task *cbs_u;
	static callback_sync *the_normal_callbacks;
	static callback_sync *the_abort_callbacks;
};

cbs_unref_task *cbsyncs::cbs_u = NULL;
callback_sync *cbsyncs::the_normal_callbacks = NULL;
callback_sync *cbsyncs::the_abort_callbacks = NULL;

void
cbsyncs::initialize()
{
	CL_PANIC((the_normal_callbacks == NULL) &&
	    (the_abort_callbacks == NULL) && (cbs_u == NULL));
	the_normal_callbacks = new callback_sync;
	the_abort_callbacks = new callback_sync;
	cbs_u = new cbs_unref_task;
	ASSERT((the_normal_callbacks != NULL) &&
	    (the_abort_callbacks != NULL) && (cbs_u != NULL));
}

void
cbsyncs::shutdown()
{
	CL_PANIC(cbs_u != NULL);
	cbs_unref_task *cbs_tmp = cbs_u;
	cbs_u = NULL;
	common_threadpool::the().defer_processing(cbs_tmp);
}

void
cbsyncs::shutdown_int()
{
	CL_PANIC((the_normal_callbacks != NULL) &&
	    (the_abort_callbacks != NULL));
	CL_PANIC(cbs_u == NULL);
	delete the_normal_callbacks;
	the_normal_callbacks = NULL;
	delete the_abort_callbacks;
	the_abort_callbacks = NULL;
}

callback_sync &
cbsyncs::normal()
{
	CL_PANIC(the_normal_callbacks != NULL);
	return (*the_normal_callbacks);
}

callback_sync &
cbsyncs::abort()
{
	CL_PANIC(the_abort_callbacks != NULL);
	return (*the_abort_callbacks);
}

void
cbs_unref_task::execute()
{
	cbsyncs::shutdown_int();
}

//
// constructor for callback_sync.
//
callback_sync::callback_sync() :
	step_callbacks_in_progress(false),
	need_to_terminate_callbacks(false),
	seqnum_to_be_terminated(0),
	curr_callback(NULL), callback_num(0),
	_callback_ff(ff::failfast::_nil()),
	num_waiters(0),
	client_info_chain(NULL)
{
}

//
// destructor for callback_sync.
//
callback_sync::~callback_sync()
{
	client_info_t	*client_info_p, *prev;

	if (_callback_ff != ff::failfast::_nil()) {
		//
		// Failfast unit has been created. It needs to be
		// dismantled.
		//
		Environment e;

		_callback_ff->disarm(e);
		CL_PANIC(e.exception() == NULL);

		CORBA::release(_callback_ff);

		_callback_ff = ff::failfast::_nil();
	}
	//
	// Deallocate client info chain if it exists.
	//
	client_info_p = client_info_chain;
	while (client_info_p) {
		prev = client_info_p;
		client_info_p = client_info_p->next;
		delete prev;
	}
	client_info_chain = NULL;

	// Wait for the last cv waiters to drain out.
	lck.lock();
	while (num_waiters > 0)
		cv.wait(&lck);
}

//
// This method is called to complete initialization steps that are
// not done in the constructor due to dependency constraints.
//
// This method creates a failfast unit if it has not been created already.
// It is created here rather than in the callback_sync constructor since
// the constructor is invoked before the failfast administration unit is
// set up and hence the callback_sync constructor cannot invoke the
// ff_create method.
//
// The reason for keeping the failfast unit in this class rather than
// than in the cb class is to create a failfast unit just once and to
// use it during the callbacks. We want to avoid memory allocations
// caused by creation of new objects during the execution of transitions.
//
void
callback_sync::complete_initialization()
{
	if (_callback_ff == ff::failfast::_nil()) {
		//
		// Failfast unit has not been created.
		//
		Environment e;

		ff::failfast_admin_var ff_admin_v = cmm_ns::get_ff_admin();
		_callback_ff = ff_admin_v->ff_create(callback_ff_name,
			conf.ff_mode, e);
		CL_PANIC(e.exception() == NULL);
	}
}

//
// This method is called to add a client callback struct to the chain of
// client info structs. If the CMM needs to start a new reconfiguration,
// this info is used to signal client callbacks to terminate.
//
void
callback_sync::add_client_info(cmm::callback_ptr callback_p)
{
	client_info_t	*client_info_p = new client_info_t;

	client_info_p->callback_p = callback_p;
	client_info_p->next = client_info_chain;
	client_info_chain = client_info_p;
}

//
// Set the failfast timer.
//
void
callback_sync::set_timer(cmm::timeout_t tmout)
{
	Environment e;
	_callback_ff->arm(tmout, e);
	CL_PANIC(e.exception() == NULL);
}

//
// Reset the failfast timer.
//
void
callback_sync::reset_timer()
{
	Environment e;
	_callback_ff->disarm(e);
	CL_PANIC(e.exception() == NULL);
}

//
// called by cmm threads (transitions_thread or abort_thread) to signal
// the running of callbacks.  Only one thread calls this with any given
// instance of a callback_sync object.  That thread will wait in
// cb::exec_callbacks until all callbacks complete.
//

void
callback_sync::add_next_callback(cb *cbp)
{
	lck.lock();
	curr_callback = cbp;	// point all callbacks to the common "cb"
	callback_num++;		// the next callback should be run now
	cv.broadcast();		// wakeup all of the callback threads
	lck.unlock();
}

//
// called by callback threads to wait for a signal from a cmm thread
//

cb *
callback_sync::get_next_callback(int32_t *last_callback_num)
{
	cb *retval;

	lck.lock();
	num_waiters++;
	CL_PANIC(callback_num >= *last_callback_num);
	while (callback_num == *last_callback_num)
		cv.wait(&lck);
	CL_PANIC(callback_num == *last_callback_num + 1);
	*last_callback_num = callback_num;
	retval = curr_callback;
	num_waiters--;
	// For shutdown
	if (num_waiters == 0)
		cv.signal();
	lck.unlock();

	return (retval);
}

//
// Method called from a cb instance to invoke the client terminate_trans
// callbacks.
//
void
callback_sync::terminate_callbacks()
{
	client_info_t	*client_info_p;
	Environment	e;

	CMM_TRACE(("Terminating CMM client callbacks\n"));

	for (client_info_p = client_info_chain; client_info_p != NULL;
	    client_info_p = client_info_p->next) {
		int version =
		    cmm::callback::_version(client_info_p->callback_p);
		if (version == 0) {
			CMM_TRACE(("Invoking version 0 interface \n"));
			client_info_p->callback_p->terminate_step(e);
		} else {
			CMM_TRACE(("Invoking version 1 interface \n"));
			client_info_p->callback_p->terminate_step_this_seqnum(
			    seqnum_to_be_terminated, e);
		}
	}
	//
	// Since this is a local invocation, we do not expect any
	// exception. We can get version exception if mismatched
	// software is installed on the same node. See bug 4839131.
	//
	if (e.exception() && CORBA::VERSION::_exnarrow(e.exception())) {
		//
		// SCMSGS
		// @explanation
		// Interface version of an instance of userland CMM does not
		// match the kernel version.
		// @user_action
		// Ensure that complete software disribution for a given
		// release has been installed using recommended install
		// procedures. Retry installing same packages if previous
		// install had been aborted. If the problem persists, contact
		// your authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Callback interface versions do not match.");
	}
	//
	// Now that we have called the terminate_step callbacks, we
	// can turn off the flag requesting this.
	//
	need_to_terminate_callbacks = false;
}

//
// thread_args is a class used to store the arguments passed by clients of
// the callback registry through the callback_registry_impl::add method.
// Two threads (for normal callback and abort callback) are created for each
// invocation of the add method and a separate instance of the thread_args
// class is passed to each thread to keep track of the per-thread arguments.
//

class thread_args {
public:
	bool			exit_now;	// hook for thread to exit
	//lint -e1725
	callback_sync		&cbsp;
	//lint +e1725
	cmm::callback_ptr	cb_obj;
	char			*name;	// to distinguish among the various
					// callback threads (for debugging)
	cmm::callback_info	thr_cbinfo; // callback info for this thread

	thread_args() : exit_now(false), cbsp(cbsyncs::normal()), cb_obj(nil),
		name(NULL) {};
	thread_args(callback_sync &cbptr, cmm::callback_ptr obj,
			const cmm::callback_info *cbinfo, char *cb_name);

	~thread_args();
};

//
// constructor for thread_args
//
thread_args::thread_args(callback_sync &cbptr, cmm::callback_ptr obj,
		const cmm::callback_info *cbinfo, char *cb_name) :
			exit_now(false), cbsp(cbptr),
			cb_obj(cmm::callback::_duplicate(obj)),
			name(cb_name),
			thr_cbinfo(*cbinfo)
{
}

//
// destructor for thread_args
//
thread_args::~thread_args() {
	CORBA::release(cb_obj);
	cb_obj = nil;
	delete [] name;
}

callback_registry_impl::callback_registry_impl(cmm_impl *cmm_implp) :
    _cmmp(cmm_implp), _count(0), _already_started(false)
{
	//
	// Initialize the cbreg_cbinfo member.
	//
#ifdef CMM_VERSION_0
	use_cmm_v0_steps = false;
#endif // CMM_VERSION_0

	cbreg_cbinfo = new cmm::callback_info();
	cbreg_cbinfo->step_timeouts.length(0);
	cbreg_cbinfo->num_steps = 0;
	cbreg_cbinfo->return_timeout = CMM_TIMEOUT_DELTA;
	cbreg_cbinfo->abort_timeout = CMM_TIMEOUT_DELTA;
	cbreg_cbinfo->stop_timeout = CMM_TIMEOUT_DELTA;

	cbsyncs::initialize();
}

//lint -e1740
callback_registry_impl::~callback_registry_impl()
{
	// Make lint happy
	_cmmp = NULL;

	delete cbreg_cbinfo;
}
//lint +e1740

void
#ifdef DEBUG
callback_registry_impl::_unreferenced(unref_t cookie)
#else
callback_registry_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));

	// kill callback threads
	cbsyncs::normal().add_next_callback((cb *)NULL);
	cbsyncs::abort().add_next_callback((cb *)NULL);

	cbsyncs::shutdown();

	_cmmp->dec_ref(DONT_EXIT);	// unreference the_cmm
}

bool
callback_registry_impl::start_trans(os::hrtime_t start_time)
{
	// prohibit additional calls to add() per cmm.idl
	_already_started = true;

	cb_start cb_var(_count); 	// no timeout for start transition
	cb_var.exec_callbacks(cbsyncs::normal(), 0 /* no start timeout */,
	    start_time, "start");
	return (cb_var.get_error());
}

bool
callback_registry_impl::stop_trans()
{
	cb_stop	cb_var(_count);
	cb_var.exec_callbacks(cbsyncs::normal(), cbreg_cbinfo->stop_timeout);
	return (cb_var.get_error());
}

void
callback_registry_impl::abort_trans()
{
	cb_abort cb_var(_count);
	cb_var.exec_callbacks(cbsyncs::abort(), cbreg_cbinfo->abort_timeout);
}

void
callback_registry_impl::set_reconf_data(cmm::seqnum_t sn,
    const cmm::membership_t &membership)
{
	cb_set_reconf_data cb_var(_count, sn, membership);
	cb_var.exec_callbacks(cbsyncs::normal(), 0);
}

bool
callback_registry_impl::step_trans(uint_t stepnum, os::hrtime_t start_time)
{
	cb_step	cb_var(_count, stepnum);
	char	step_name[CL_MAX_LEN + 1];

#ifdef _FAULT_INJECTION
	void *f_argp;
	uint32_t f_argsize;
	uint32_t trigger_step_num;

	if (fault_triggered(FAULTNUM_CMM_STEP_TRANS, &f_argp, &f_argsize)) {
		ASSERT(f_argsize == sizeof (uint32_t));
		trigger_step_num = *((uint32_t *)f_argp);
		if (trigger_step_num == stepnum)
			return (1);  /* any non zero value */
	}
#endif

	//
	// Starting a step transition which can block waiting for client
	// callback routines to complete.
	//
	cbsyncs::normal().lck.lock();
	cbsyncs::normal().step_callbacks_in_progress = true;
	cbsyncs::normal().lck.unlock();

	os::sprintf(step_name, "step%d", stepnum);

#ifdef CMM_VERSION_0
	//
	// The step timeouts are initialized when the CMM callbacks
	// are registered. Initialization is for the new version
	// of the CMM with the new set of callback steps. We may be
	// executing the old set of steps here. If so, we have to
	// correctly assign the timeout value for the steps. All the
	// steps in the old and the new set use a default value
	// except for signal_cbc_rmm_cleanup_step. In the old set the
	// corresponding step with an infinite timeout is the
	// rmm_cleanup_step. The code block below makes sure of this.
	//
	uint_t curr_timeout = 0;
	_lock.lock();
	if ((use_cmm_v0_steps == true) && (stepnum == 9)) {
		curr_timeout = 0;
	} else {
		curr_timeout = cbreg_cbinfo->step_timeouts[stepnum - 1];
	}
	_lock.unlock();

	cb_var.exec_callbacks(cbsyncs::normal(), curr_timeout, start_time,
	    step_name);
#else
	cb_var.exec_callbacks(cbsyncs::normal(),
	    cbreg_cbinfo->step_timeouts[stepnum - 1], start_time, step_name);

#endif // CMM_VERSION_0


	cbsyncs::normal().lck.lock();
	cbsyncs::normal().step_callbacks_in_progress = false;
	cbsyncs::normal().lck.unlock();

	return (cb_var.get_error());
}

bool
callback_registry_impl::return_trans(os::hrtime_t start_time)
{
	cb_return cb_var(_count);

	//
	// Now that we are in the return transition, the flag to terminate
	// step callbacks should be turned off.
	//
	cbsyncs::normal().need_to_terminate_callbacks = false;

	cb_var.exec_callbacks(cbsyncs::normal(), cbreg_cbinfo->return_timeout,
	    start_time, "return");

	return (cb_var.get_error());
}

//
// Method called from the CMM automaton (interrupt threads) to signal the
// need to start another reconfiguration, requiring the current callback
// to terminate if it is running.
//
// Only normal callbacks need to be terminated. If we are in an
// abort transition, the cluster is aborting and there is no need to
// terminate the callbacks.
//
void
callback_registry_impl::terminate_trans(cmm::seqnum_t current_seq)
{
	cb	*curr_callback;

	cbsyncs::normal().lck.lock();

	cbsyncs::normal().need_to_terminate_callbacks = true;
	cbsyncs::normal().seqnum_to_be_terminated = current_seq;

	if (cbsyncs::normal().step_callbacks_in_progress) {
		CMM_TRACE(("Signalling CMM client callbacks to terminate\n"));
		curr_callback = cbsyncs::normal().curr_callback;
		curr_callback->_lock.lock();
		curr_callback->_completion_cv.signal();
		curr_callback->_lock.unlock();
	}

	cbsyncs::normal().lck.unlock();
}

//
// Add a callback object to the list of CMM callbacks.  This takes the
// form of creation of two threads, one for normal callbacks invoked by
// the transitions_thread, and one for abort callbacks which could come
// from either the transitions_thread or the abort_thread.
//

// XXX There needs to be a way to cause the threads created here to be
// destroyed on shutdown
void
callback_registry_impl::add(const char *cb_name, cmm::callback_ptr callback_p,
	const cmm::callback_info &cbinfo, Environment& e)
{
	thread_args *thr_args1, *thr_args2;
	uint_t i;

	_lock.lock();
	if (_already_started) {	// prohibit add() if CMM has started
		e.exception(new cmm::not_start_state());
		_lock.unlock();
		return;
	}

	//
	// Check the parameters passed in. Currently, the only parameter
	// checked is "num_steps" in the callback_info struct.
	//
	if (cbinfo.num_steps > automaton_impl::max_cmm_transition_steps()) {
		//
		// SCMSGS
		// @explanation
		// The number of steps specified during registering a CMM
		// callback exceeds the allowable maximum. This is an internal
		// error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Number of steps specified in registering "
			"callback = %d; should be <= %d.", cbinfo.num_steps,
			automaton_impl::max_cmm_transition_steps);
		e.exception(new cmm::invalid_parameter());
		_lock.unlock();
		return;
	}

	cbsyncs::normal().complete_initialization();
	cbsyncs::abort().complete_initialization();

	//
	// Add the callback info to cbsyncs::normal() so that blocking
	// callbacks can be terminated when the CMM needs to start a
	// new reconfiguration.
	//
	cbsyncs::normal().add_client_info(callback_p);

	char *cbname = new char[os::strlen(cb_name) + 1];
	(void) os::strcpy(cbname, cb_name);

	thr_args1 = new thread_args(cbsyncs::normal(), callback_p, &cbinfo,
		cbname);

	//
	// Run normal callbacks at two less than max priority.
	// (Comments on thread priorities appear in cmm_impl.cc).
	//
	if (!cmm_impl::cmm_create_thread(cb::cmm_callback_thread, thr_args1,
	    CMM_MAXPRI - 2)) {
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Unable to create %s thread.", "normal callback");
		delete thr_args1;
		e.exception(new cmm::no_resources());
		_lock.unlock();
		return;
	}

	char *abort_cbname = new char[os::strlen(cb_name) + 1 +
	    os::strlen(" abort") + 1];
	os::sprintf(abort_cbname, "%s abort", cb_name);

	thr_args2 = new thread_args(cbsyncs::abort(), callback_p, &cbinfo,
								abort_cbname);

	//
	// Run abort callbacks at one less than max priority.
	// (Comments on thread priorities appear in cmm_impl.cc).
	//
	if (!cmm_impl::cmm_create_thread(cb::cmm_callback_thread, thr_args2,
	    CMM_MAXPRI - 1)) {
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Unable to create %s thread.", "abort callback");
		delete thr_args2;
		e.exception(new cmm::no_resources());

		// the thread that was created
		// above is to exit on first run
		thr_args1->exit_now = true;
		_lock.unlock();
		return;
	}

	//
	// Set the max_step_number in the cmm.
	//
	_cmmp->set_max_step_number(cbinfo.num_steps);

	_count++;

	//
	// Update the registry's callback info struct to include the timeouts
	// specified by this client. We add the timeouts specified by all
	// the clients to get a cumulative timeout to be used by the
	// callback registry to monitor client callbacks.
	//
	// If any client specifies no timeout (timeout value of 0) for any
	// of the transitions, this means that the client expects an
	// infinite timeout to be used. In that case, we set the timeout to 0.
	//
	// Note that all transition timeouts are initialized to a non-zero
	// value (CMM_TIMEOUT_DELTA).
	//

	if ((cbreg_cbinfo->return_timeout == 0) || (cbinfo.return_timeout == 0))
		cbreg_cbinfo->return_timeout = 0;
	else
		cbreg_cbinfo->return_timeout += cbinfo.return_timeout;

	if ((cbreg_cbinfo->abort_timeout == 0) || (cbinfo.abort_timeout == 0))
		cbreg_cbinfo->abort_timeout = 0;
	else
		cbreg_cbinfo->abort_timeout += cbinfo.abort_timeout;

	if ((cbreg_cbinfo->stop_timeout == 0) || (cbinfo.stop_timeout == 0))
		cbreg_cbinfo->stop_timeout = 0;
	else
		cbreg_cbinfo->stop_timeout += cbinfo.stop_timeout;

	if (cbreg_cbinfo->num_steps < cbinfo.num_steps) {
		//
		// This client has more steps than we currently have values
		// for. We need to make room for the new steps.
		//
		cbreg_cbinfo->step_timeouts.length(cbinfo.num_steps);

		//
		// Initialize the step timeouts for the new steps.
		//
		for (i = cbreg_cbinfo->num_steps; i < cbinfo.num_steps; i++)
			cbreg_cbinfo->step_timeouts[i] = CMM_TIMEOUT_DELTA;
		cbreg_cbinfo->num_steps = cbinfo.num_steps;
	}
	for (i = 0; i < cbinfo.num_steps; i++) {
		if ((cbreg_cbinfo->step_timeouts[i] == 0) ||
		    (cbinfo.step_timeouts[i] == 0))
			cbreg_cbinfo->step_timeouts[i] = 0;
		else
			cbreg_cbinfo->step_timeouts[i] +=
				cbinfo.step_timeouts[i];
	}

	_lock.unlock();
}

#ifdef CMM_VERSION_0

//
// During Rolling upgrade, we need to execute the old set of CMM
// callback steps that doesn't include version manager support.
// The old set of steps is activated here.
//
void
callback_registry_impl::set_cmm_v0_steps()
{
	_lock.lock();
	if (use_cmm_v0_steps) {
		//
		// We are already running the old set of steps.
		// No work to be done here.
		//
		_lock.unlock();
		return;
	}
	CMM_TRACE(("callback registry: set version to 0\n"));
	use_cmm_v0_steps = true;
#ifdef	_KERNEL_ORB
	ASSERT(select_states_v0_funcp != NULL);
	(*select_states_v0_funcp)();
#endif //  _KERNEL_ORB
	_lock.unlock();
}

//
// After the rolling upgrade is complete, we move forward and execute
// and execute the new set of CMM callback steps. The new set of CMM
// steps is activated here.
//
void
callback_registry_impl::set_cmm_v1_steps()
{
	_lock.lock();
	if (!use_cmm_v0_steps) {
		//
		// We are already running the new set of steps.
		// No work to be done here.
		//
		_lock.unlock();
		return;
	}
	CMM_TRACE(("callback registry: set version to 1\n"));
	use_cmm_v0_steps = false;
#ifdef	_KERNEL_ORB
	ASSERT(select_states_v1_funcp != NULL);
	(*select_states_v1_funcp)();
#endif //  _KERNEL_ORB
	_lock.unlock();
}

#endif CMM_VERSION_0

//
// Class cb and its derived classes
//

cb::cb(int32_t count) :
    _error(false), _counter(count)
{
}

cb::~cb()
{
}

//
// This method arranges for all the callback routines for a specific automaton
// transition to be invoked. It first signals the client callback threads and
// then waits for all callbacks to complete before returning.
//
// To ensure that the callbacks complete in time, this method uses two
// timers - a thread synchronization timer and a failfast timer provided
// by the callback_sync object. The failfast timer is a fallback mechanism
// to catch failures of the thread timer. A timeout of the thread timer
// causes the node to abort. A timeout of the failfast timer causes the
// node to panic.
//
// The thread timeout should be for a shorter duration than the failfast
// timeout. The delta is provided by the grace time added by the failfast
// timer to the specified timeout.
//
// Note that the timeout value passed in is the sum of the timeout values
// specified by all the clients for their respective callbacks.
//
void
cb::exec_callbacks(callback_sync &cbsp, cmm::timeout_t tmout,
    os::hrtime_t start_time, char *step_name)
{
	FAULTPT_KCMM(FAULTNUM_CMM_CALLBACK_REGISTRY_EXEC_CALLBACKS_1,
	    FaultFunctions::generic);

	cbsp.add_next_callback(this);

	_lock.lock();

	if (tmout) {
		//
		// A timeout is specified. Must set a timer to ensure
		// that the callbacks complete in time.
		//

		os::systime sys_timeout;

		//
		// Start cbsp's timer to monitor the thread timer.
		//
		cbsp.set_timer(tmout);

		CL_PANIC(tmout < INT_MAX/1000);
		sys_timeout.setreltime(tmout * (os::usec_t)1000); // in usecs

		while (_counter != 0) { // wait for all callbacks to complete
			//
			// If the CMM automaton has asked to terminate the
			// callbacks, do so.
			//
			if (cbsp.need_to_terminate_callbacks)
				cbsp.terminate_callbacks();

			if (_completion_cv.timedwait(&_lock, &sys_timeout) ==
			    os::condvar_t::TIMEDOUT) {
				if (step_name != NULL) {
					// need to publish a sysevent
					(void) sc_publish_event(
					    ESC_CLUSTER_CMM_RECONFIG,
					    CL_EVENT_PUB_CMM,
					    CL_EVENT_SEV_ERROR,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_STEP_NAME, SE_DATA_TYPE_STRING,
					    step_name,
					    CL_START_TIME, SE_DATA_TYPE_TIME,
					    start_time,
					    CL_DURATION, SE_DATA_TYPE_TIME,
					    (os::gethrtime() - start_time),
					    CL_RESULT_CODE, SE_DATA_TYPE_UINT32,
					    CL_RC_TIMEOUT,
					    NULL);
				}
				callback_timedout();

				//
				// We don't need the failfast timer anymore.
				// We are now aborting the node and the
				// abort transition is watched by an abort
				// failfast timer.
				//
				cbsp.reset_timer();

				//
				// Since the cb object is deallocated
				// soon after we return from here, we
				// must wait for all threads accessing
				// the cb object to complete their
				// processing before returning. We can
				// now wait indefinitely since we are
				// aborting anyway.
				//
				while (_counter != 0)
					_completion_cv.wait(&_lock);

				goto done;

			}
		}
		//
		// Now that the call has completed, reset cbsp's timer.
		//
		cbsp.reset_timer();
	} else {
		//
		// No timeout specified (as in start transition); wait
		// indefinitely.
		//
		while (_counter != 0) { // wait for all callbacks to complete
			//
			// If the CMM automaton has asked to terminate the
			// callbacks, do so.
			//
			if (cbsp.need_to_terminate_callbacks)
				cbsp.terminate_callbacks();

			_completion_cv.wait(&_lock);
		}
	}

done:
	_lock.unlock();

	FAULTPT_KCMM(FAULTNUM_CMM_CALLBACK_REGISTRY_EXEC_CALLBACKS_2,
	    FaultFunctions::generic);
}

bool
cb::get_error()
{
	bool retval;	// return true on error

	_lock.lock();	// paranoia
	retval = _error;
	_lock.unlock();
	return (retval);
}

//
// This function implements threads that wait until there is another
// callback to run.  The NULL callback or the thread_args exit_now
// flag indicate that "it's time to quit".
//

void
cb::cmm_callback_thread(void *arg)
{
	thread_args *thr_args = (thread_args *)arg;
	int32_t my_num = 0;
	cb *cbp;

	while (!thr_args->exit_now &&
		((cbp = thr_args->cbsp.get_next_callback(&my_num)) != NULL))
		cbp->do_callback(thr_args);

	delete thr_args;
	// thread exits on return
}

//
// Make the "call", check for an error, and wakeup the caller if
// we're the last callback thread to complete.
//

void
cb::do_callback(thread_args *args)
{
	Environment e;

	call(args, e);

	_lock.lock();
	if (e.exception()) {
		_error = true;
		e.clear();
	}
	if (--_counter == 0)
		_completion_cv.signal();
	_lock.unlock();
}

//
// This method is invoked to indicate that the callback routine(s) initiated
// did not complete in the expected amount of time. The corrective action
// taken is to cause the node to abort.
//
void
cb::callback_timedout()
{
	Environment e;

	//
	// One or more callbacks timed out. Abort the node after printing an
	// appropriate message.
	//
	CMM_TRACE(("CMM reconfiguration callback timed out; node aborting.\n"));
	//
	// SCMSGS
	// @explanation
	// One or more CMM client callbacks timed out and the node will be
	// aborted.
	// @user_action
	// There may be other related messages on this node which may help
	// diagnose the problem. Resolve the problem and reboot the node if
	// node failure is unexpected. If unable to resolve the problem,
	// contact your authorized Sun service provider to determine whether a
	// workaround or patch is available.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		"CMM: Reconfiguration callback timed out; node aborting.");

	//
	// Now signal the node to abort.
	//
	cmm::control_var cmm_cntl_v = cmm_ns::get_control(cmm_name);
	cmm_cntl_v->abort(e);
	CL_PANIC(e.exception() == NULL);
}

void
cb_start::call(thread_args *args, Environment& e)
{
	args->cb_obj->start_trans(e);
}

void
cb_stop::call(thread_args *args, Environment& e)
{
	args->cb_obj->stop_trans(args->thr_cbinfo.stop_timeout, e);
}

void
cb_return::call(thread_args *args, Environment& e)
{
#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(FAULTNUM_CMM_CALLBACK_REGISTRY_RETURN_CALL,
	    &fault_argp, &fault_argsize)) {
		// Timeout should be in msec
		cmm::timeout_t	tmout;
		tmout = args->thr_cbinfo.return_timeout + CMM_TIMEOUT_DELTA;
		tmout += 100;
		os::warning("FI sleep in return callback: %lu msecs", tmout);
		os::usecsleep(tmout * (os::usec_t)1000);
	}
#endif // FAULT_CMM && _KERNEL_ORB

	args->cb_obj->return_trans(args->thr_cbinfo.return_timeout, e);
}

void
cb_abort::call(thread_args *args, Environment& e)
{
	args->cb_obj->abort_trans(args->thr_cbinfo.abort_timeout, e);
}

void
cb_set_reconf_data::call(thread_args *args, Environment& e)
{
	args->cb_obj->set_reconf_data(_seqnum, _mbrship, e);
}

void
cb_step::call(thread_args *args, Environment& e)
{
	CORBA::Exception *ex;
	cmm::timeout_t timeout_val;

	if (_step_num > args->thr_cbinfo.num_steps)
		return;

	//
	// _step_num goes from 1 to num_steps while the index into the
	// sequence goes 0 through num_steps - 1. Hence we index
	// step_timeouts using (_step_num - 1).
	//
	timeout_val = args->thr_cbinfo.step_timeouts[(_step_num - 1)];

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	void		*fault_argp;
	uint32_t	fault_argsize;

	if (fault_triggered(FAULTNUM_CMM_CALLBACK_REGISTRY_STEP_CALL,
	    &fault_argp, &fault_argsize)) {
		CL_PANIC(fault_argsize == sizeof (int32_t));
		// Argument is the step to sleep in
		uint_t step = *(uint_t *)fault_argp;

		if (step == _step_num) {
			// Make callback thread sleep for timeout time
			// timeout_val should be in msec
			os::warning("FI sleep in step (%d) callback: %lu msecs",
			    step, timeout_val + CMM_TIMEOUT_DELTA + 100);
			os::usecsleep((os::usec_t)1000 *
			    (timeout_val + CMM_TIMEOUT_DELTA + 100));
		}
	}
#endif // FAULT_CMM && _KERNEL_ORB

	args->cb_obj->step_trans(_step_num, timeout_val, e);
	if ((ex = e.exception()) != NULL) {
		if (cmm::force_return::_exnarrow(ex)) {
			//
			// step_trans() call failed ealier, probably due to a
			// problem on a remote node. Reconfigure and return
			// success, assuming that the CMM will resolve the
			// problem.
			//
			e.clear();
			CMM_TRACE(("CMM reconfiguration step %d was forced "
				"to return.\n", _step_num));
			//
			// SCMSGS
			// @explanation
			// One of the CMM reconfiguration step transitions
			// failed, probably due to a problem on a remote node.
			// A reconfiguration is forced assuming that the CMM
			// will resolve the problem.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
				"CMM: Reconfiguration step %d was forced "
				"to return.", _step_num);
			cmm::control_var cmm_cntl_v =
			    cmm_ns::get_control(cmm_name);
			cmm_cntl_v->reconfigure(e);
		}
	}
}
