//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * ucmm_api.cc
 *
 *	Interfaces to the userland CMM
 */


#pragma ident	"@(#)ucmm_api.cc	1.41	08/06/23 SMI"

#include <h/cmm.h>
#include <orb/infrastructure/orb_conf.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_impl.h>
#include <cmm/ucmm_api.h>
#include <cmm/ucmm_impl.h>
#include <cmm/ucmm_comm_impl.h>
#include <sys/sc_syslog_msg.h>

//
// Function to initialize the UCMM.
//
// Called from daemons that need UCMM functionality.
//
// Current callers are:
//
// (1) RGM daemon (rgmd)
// (2) OPS ucmm daemon (ucmmd)
//
#if (SOL_VERSION >= __s10)
int
ucmm_initialize(const char *ucmm_name, const char *syslog_tag,
	//
	// Ignore lint info message about making ucmm_callback
	// "const reference"; following that advice gets lint warning
	// in the add() call further down!
	//
    const cmm::callback_var ucmm_callback, //lint -e1746
    const cmm::callback_info &cbinfo,
    boolean_t use_process_membership)
#else
int
ucmm_initialize(const char *ucmm_name, const char *syslog_tag,
	//
	// Ignore lint info message about making ucmm_callback
	// "const reference"; following that advice gets lint warning
	// in the add() call further down!
	//
    const cmm::callback_var ucmm_callback, //lint -e1746
    const cmm::callback_info &cbinfo)
#endif
{
	sc_syslog_msg_handle_t handle;

	// Create the UCMM object.
	ucmm_impl *ucmm = new ucmm_impl(ucmm_name, syslog_tag);

	// Create the UCMM communication object.
	cmm::comm_var comm_v = (new cm_comm_impl())->get_objref();

	// Complete the ucmm object initialization.
	ucmm->init(comm_v);

	// Register for reconfiguration callbacks.
	cmm::callback_registry_var cmm_callback_registryp =
		cmm_ns::get_callback_registry(ucmm_name);
	CL_PANIC(!CORBA::is_nil(cmm_callback_registryp));
	Environment e;
	cmm_callback_registryp->add(ucmm_name, ucmm_callback, cbinfo, e);
	if (e.exception()) {
		e.clear();
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR CMM: Failure registering callbacks.");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}

	// Start the UCMM sender thread.
	if (thr_create(NULL, 0, cm_comm_impl::sender_thread_start, NULL,
	    THR_BOUND, NULL) != 0) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error. This is caused by inadequate memory
		// on the system.
		// @user_action
		// Add more memory to the system. If that does not resolve the
		// problem, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR CMM: Failure creating sender thread.");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}

#if (SOL_VERSION >= __s10)
	using_process_membership = use_process_membership;
	if (use_process_membership) {
		ucmm_impl::the().setup_process_membership();
		ucmm_impl::the().wait_for_first_boot();
	}
#endif

	// Start the UCMM automaton.
	cmm::control_var cmm_controlp = cmm_ns::get_control(ucmm_name);
	cmm_controlp->start(e);
	if (e.exception()) {
		e.clear();
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR CMM: Failure starting CMM.");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
	return (0);
}

// Read the cluster state
void
ucmm_getcluststate(struct clust_state *csp)
{
	cmm_impl::the().getcluststate(csp);
}

// Stop or abort a set of nodes.
void
ucmm_stop_abort(nodeset_t stopset, nodeset_t abortset)
{
	Environment e;
	cmm::control_ptr cmm_controlp = nil;
	const char *ucmm_name = ucmm_impl::the().ucmm_name;

	for (uint_t i = 1; i <= NODEID_MAX; i++) {

		if (i == orb_conf::local_nodeid())
			continue;

		CL_PANIC(CORBA::is_nil(cmm_controlp));

		if (nodeset_contains(abortset, i)) {
			cmm_controlp = cmm_ns::get_control(ucmm_name, i);
			if (!CORBA::is_nil(cmm_controlp)) {
				cmm_controlp->abort(e);
				CORBA::release(cmm_controlp);
				cmm_controlp = nil;
				if (e.exception())
					// we don't care about this exception
					e.clear();
			}
		} else if (nodeset_contains(stopset, i)) {
			cmm_controlp = cmm_ns::get_control(ucmm_name, i);
			if (!CORBA::is_nil(cmm_controlp)) {
				cmm_controlp->stop(e);
				CORBA::release(cmm_controlp);
				cmm_controlp = nil;
				if (e.exception())
					// we don't care about this exception
					e.clear();
			}
		}
	}

	CL_PANIC(CORBA::is_nil(cmm_controlp));

	// Stop or abort local node last
	if (nodeset_contains(abortset, orb_conf::local_nodeid())) {
		cmm_controlp = cmm_ns::get_control(ucmm_name);
		CL_PANIC(!CORBA::is_nil(cmm_controlp));
		cmm_controlp->abort(e);
		CORBA::release(cmm_controlp);
		if (e.exception())
			// we don't care about this exception
			e.clear();
	} else if (nodeset_contains(stopset, orb_conf::local_nodeid())) {
		cmm_controlp = cmm_ns::get_control(ucmm_name);
		CL_PANIC(!CORBA::is_nil(cmm_controlp));
		cmm_controlp->stop(e);
		CORBA::release(cmm_controlp);
		if (e.exception())
			// we don't care about this exception
			e.clear();
	}
}

// Force the UCMM to run another reconfiguration.
void
ucmm_reconfigure()
{
	Environment e;
	cmm::control_var cmm_controlp =
		cmm_ns::get_control(ucmm_impl::the().ucmm_name);
	cmm_controlp->reconfigure(e);
	if (e.exception())
		// we don't care about this exception
		e.clear();
}

// Get the initial view numbers of the cluster nodes
int
ucmm_getinitialviewnumber(nodeid_t nodeid, cmm::seqnum_t &initialviewnumber)
{
	Environment e;
	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	ASSERT(!CORBA::is_nil(cmm_cntl_v));
	int res = cmm_cntl_v->cmm_getinitialviewnumber(nodeid,
	    initialviewnumber, e);
	ASSERT(!e.exception());
	return (res);
}

#if (SOL_VERSION >= __s10)
void
ucmm_step1_upgrade_callback_action()
{
	ucmm_impl::the().step1_upgrade_callback_action();
}

void
ucmm_step2_upgrade_callback_action()
{
	ucmm_impl::the().step2_upgrade_callback_action();
}
#endif
