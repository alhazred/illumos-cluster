/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_MEMBERSHIP_THREADPOOL_H
#define	_MEMBERSHIP_THREADPOOL_H

#pragma ident	"@(#)membership_threadpool.h	1.3	08/07/21 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <sys/threadpool.h>

// The membership threadpool
class membership_threadpool {
public:
	static int initialize();
	static void shutdown();
	static threadpool &the();
private:
	static threadpool *the_membership_threadpool;
};

// The membership callbacks threadpool
class callback_threadpool {
public:
	static int initialize();
	static void shutdown();
	static threadpool &the();
private:
	static threadpool *the_callback_threadpool;
};
#endif	// (SOL_VERSION >= __s10)

#include <cmm/membership_threadpool_in.h>

#endif	/* _MEMBERSHIP_THREADPOOL_H */
