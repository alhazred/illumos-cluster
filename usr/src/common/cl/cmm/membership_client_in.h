/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * membership_client_in.h
 */

#ifndef	_MEMBERSHIP_CLIENT_IN_H
#define	_MEMBERSHIP_CLIENT_IN_H

#pragma ident	"@(#)membership_client_in.h	1.4	08/07/21 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

inline const char *
membership_client_impl::get_client_name() const
{
	return (client_namep);
}

inline mem_callback_funcp_t
membership_client_impl::get_callback_funcp() const
{
	return (callback_funcp);
}

#endif	// (SOL_VERSION >= __s10)
#endif	// _MEMBERSHIP_CLIENT_IN_H
