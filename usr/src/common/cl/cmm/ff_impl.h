/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _FF_IMPL_H
#define	_FF_IMPL_H

#pragma ident	"@(#)ff_impl.h	1.23	08/07/17 SMI"

#include <h/ff.h>
#include <orb/object/adapter.h>
#include <orb/invo/common.h>
#include <cmm/ff_callout.h>
#include <orb/infrastructure/cl_sched.h>

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
#include <sys/zone.h>
#include <sys/thread.h>
#include <sys/cred_impl.h>
#include <sys/uadmin.h>
#endif

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#else
#undef	UNODE
#endif

class ff_impl : public McServerof<ff::failfast> {
public:
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	ff_impl(const char *, ff::ffmode,
	    const zoneid_t zone = GLOBAL_ZONEID,
	    const uint64_t zone_uniq_id = GLOBAL_ZONEUNIQID);
#else
	ff_impl(const char *, ff::ffmode);
#endif

	~ff_impl();

	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//
	void    arm(int32_t, Environment&);
	void    disarm(Environment&);

	//
	// When the timeout happens, the following method is invoked.
	//
	void    unit_timedout();

	//
	// The following is just a utility that convers millisecs to
	// hz.
	//
	static clock_t ff_millisec_to_hz(int);

	//
	// The following is just a utility to figure out ff::mode when it is
	// read in as a string from the configuration file.
	//
	static ff::ffmode ff_string_to_mode(char *);

	//
	// This is the common routine, used by the unreferenced code for
	// the failfast unit and by the server thread code, to do the unref
	// processing, execute timeout function, and then call the destructor
	//
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	void _unref();
#endif

private:
	//
	// State of the fail-fast unit
	//
	char 		ff_unit_name[ff_MAX_FF_NAME_SIZE];
	timeout_id_t	ff_timeout_id;
	os::mutex_t 	ff_lock;
	unsigned	ff_armed : 1; // there is a timeout ticking
	unsigned	ff_from_unref : 1; // unit deleted while armed
	ff::ffmode	ff_mode;
	void		(ff_impl::*ff_timeoutf)(void);

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
	//
	// Zone Information added
	//
	// Every zone has a zone id and a zone name associated with it.
	// Every time a zone boots up, it is assigned a zone id, and hence
	// zone ids assigned to a zone change over time. Though it is unlikely,
	// but it just might happen that a zone gets the same zone id in
	// two of its generations.
	// However, Solaris has a global static variable, called zone_uniqid,
	// in the kernel code, which identifies a zone generation uniquely.
	// Every time a zone boots up and zched is run, this static variable
	// is incremented in the kernel, and the zone gets this updated
	// zone_uniqid. So, we can use the zone_uniqid of a zone to identify
	// it uniquely.
	//
	zoneid_t	zone_id;
	uint64_t	zone_uniqid;
	char		zone_name[ZONENAME_MAX + 1];
#endif

	// Set when the deferred panic is armed
	bool		deferred_panic_flag;
#define	FF_DEFERRED_PANIC_BUF_SIZE	ff_MAX_FF_NAME_SIZE + 100
	char deferred_panic_buf[FF_DEFERRED_PANIC_BUF_SIZE];


	//
	// Every failfast unit has a failfast callout block associated
	// with it. This is the failfast callout block that is used for
	// this failfast unit.
	//
	ff_callout	ff_callout_block;

	//
	// lock and unlock routines
	//
	void		lock()   { ff_lock.lock(); }
	void		unlock() { ff_lock.unlock(); }

	//
	// Routines that get activated on timeout
	//
	void		stop_node_panic();
	void		stop_node_deferred_panic();
	void		stop_node_proxy_panic();
	void		stop_node_deferred_panic_int(cmm::timeout_t);
	void		stop_node_halt();
	void		stop_node_debug();
	void		stop_node_prom();

	//
	// Internal support routines
	//
	void		do_disarm();
	void		ff_impl_shouldnt_happen();

	// Disallow assignments and pass by value
	ff_impl(const ff_impl &);
	ff_impl &operator = (ff_impl &);
};

class ff_admin_impl : public McServerof<ff::failfast_admin> {
	friend class ff_impl;
public:
	enum callout_caller_t { CLOCK_THRD, INTRPT_THRD };
	ff_admin_impl();
	~ff_admin_impl();
	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//
	ff::failfast_ptr ff_create(const char *, ff::ffmode, Environment&);

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	ff::failfast_ptr vc_ff_create(
	    const char *, const char *, ff::ffmode, Environment&);
	bool failfast_now(sol::zoneid_t, Environment&);
	void set_node_level_failfast_control(bool, Environment&);
	bool get_node_level_failfast_control(Environment&);
	void set_zone_level_failfast_control(
	    bool, sol::zoneid_t, Environment&);
	bool get_zone_level_failfast_control(sol::zoneid_t, Environment&);
	sol::zoneid_t zonename_to_zoneid(const char*, Environment&);

#else
	void failfast_now(Environment&);
	bool disable_failfast(Environment&);
	bool enable_failfast(Environment&);
#endif

	//
	// The following methods set the scheduling class for the caller
	// thread (we assume here that the caller process
	// is single threaded, and so, 'proctot' macro gives the kthread_t
	// pointer from proc_t pointer). The scheduling classes are
	// Real Time (RT), Time shared (TS), Fair share (FSS) and Fixed (FX).
	//
	int set_rt_priority(int, int, Environment&);
	int set_ts_priority(int, int, Environment&);
	int set_fss_priority(int, int, Environment&);
	int set_fx_priority(int, int, Environment&);

	//
	// internal use methods
	//
	static int initialize();	// called from ORB::initialize()
	int initialize_int();		// called from initialize()
	static void shutdown();		// called from ORB::shutdown()
	static void ff_clock_callout(void);   // executed every clock tick
	static ff_admin_impl &the(void);

	// called by clock every clock tick. Also called from
	// hb_threadpool::send_heartbeat
	void sc_per_tick_processing(callout_caller_t who);

private:
	ff::failfast_ptr failfast_now_ptr;
	// Disallow assignments and pass by value
	ff_admin_impl(const ff_admin_impl &);
	ff_admin_impl &operator = (ff_admin_impl &);

	static ff_admin_impl *the_ff_adminp;
	hrtime_t	intr_run_delta;		// delta added to gethrtime
						// to determine that the clock
						// is delayed.

	// clock_callout and pertick_processing related
	os::mutex_t	per_tick_lock;		// lock for pertick_processing
	hrtime_t 	intr_run_time;		// time when intr should do
						// the per_tick_processing

	// Statistics
	uint64_t		clk_count;	// no. of times clk succeeded
	uint64_t		intr_count;	// no. of times intr succeeded

	// Counter to keep track of userland daemons that have failfast
	// armed and associated lock.
	os::mutex_t		ff_armed_lock;	// to protect counter below
	int 			ff_armed_count;
};

#include <cmm/ff_impl_in.h>

#endif	/* _FF_IMPL_H */
