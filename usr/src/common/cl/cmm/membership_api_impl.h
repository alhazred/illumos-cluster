/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  membership_api_impl.h
 *
 *  Header file for CMM Membership API
 *
 */

#ifndef	_MEMBERSHIP_API_IMPL_H
#define	_MEMBERSHIP_API_IMPL_H

#pragma ident	"@(#)membership_api_impl.h	1.3	08/07/21 SMI"

//
// This code is not compiled into unode
//

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <h/membership.h>
#include <orb/object/adapter.h>

class membership_api_impl :
    public McServerof< membership::api > {

public :
	// Static method that does the initialization
	static void initialize();

	// Static method that does the shutdown
	static void shutdown();

	// Returns a pointer to the sole object of this type
	static membership_api_impl *the();

	// IDL methods for registering clients
	void register_for_cluster_membership(
	    membership::client_ptr, const char *, Environment&);
	void register_for_process_membership(
	    membership::client_ptr, const char *, const char *, Environment&);
	void register_for_process_zone_membership(
	    membership::client_ptr, const char *, const char *, Environment&);

	// IDL methods for unregistering clients
	void unregister_for_cluster_membership(
	    membership::client_ptr, const char *, Environment&);
	void unregister_for_process_membership(
	    membership::client_ptr, const char *, const char *, Environment&);
	void unregister_for_process_zone_membership(
	    membership::client_ptr, const char *, const char *, Environment&);

	// IDL methods for getting a snapshot of the membership
	void get_cluster_membership(
	    cmm::membership_t&, cmm::seqnum_t&, const char *, Environment&);
	void get_process_membership(
	    cmm::membership_t&, cmm::seqnum_t&,
	    const char *, const char *, Environment&);
	void get_process_zone_membership(
	    cmm::membership_t&, cmm::seqnum_t&,
	    const char *, const char *, Environment&);

private :
	membership_api_impl();
	~membership_api_impl();
	void _unreferenced(unref_t);

	// Hide copy constructor and assignment operator.
	membership_api_impl(const membership_api_impl &);
	membership_api_impl &operator = (membership_api_impl &);

	bool sanity_checks(const char *, Environment &) const;
	void register_client(
	    membership::client_ptr, membership::membership_type,
	    const char *, const char *, Environment &) const;
	void unregister_client(
	    membership::client_ptr, membership::membership_type,
	    const char *, const char *, Environment &) const;
	void get_membership(
	    cmm::membership_t &, cmm::seqnum_t &, membership::membership_type,
	    const char *, const char *, Environment &) const;

	// Pointer to the only membership API object
	static membership_api_impl *the_membership_apip;
};

#endif	// (SOL_VERSION >= __s10)
#endif	/* _MEMBERSHIP_API_IMPL_H */
