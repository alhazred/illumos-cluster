//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)ff_signal.cc	1.4	08/05/20 SMI"

#include <signal.h>
#include <stdlib.h>

#include <cmm/ff_signal.h>

// Location of failfast object reference
ff::failfast_var	*failfast_var_for_signalp = NULL;

//
// ff_signal_handler - We want to force the
// application to drop the failfast object reference in order
// to trigger the failfast in case the process termination hangs,
// which is possible.
//
// This signal handler always causes the process to exit.
// If the default behavior was to dump core, this handler
// will dump core prior to exit.
//
void
ff_signal_handler(int sig)
{
	//
	// Drop the reference to the failfast object
	// and thus trigger the failfast.
	//
	*failfast_var_for_signalp = ff::failfast::_nil();

	switch (sig) {
	case SIGQUIT:
	case SIGILL:
	case SIGTRAP:
	case SIGABRT:
	case SIGEMT:
	case SIGFPE:
	case SIGBUS:
	case SIGSEGV:
	case SIGSYS:
	case SIGXCPU:
	case SIGXFSZ:
		// Dump core
		abort();

		// Fall through

	default:
		exit(sig);

		// Should not reach this point
		ASSERT(0);	//lint !e527
	}
}

//
// ff_register_signal_handler -
// register a Utility signal handler that triggers failfast
//
// When the signal is caught, the process will exit.
// If the signal has a default behavior of dumping core, this
// will continue to happen.
//
// Requires on input:
//	sig		- one catchable signal
//	failfast_varp	- the address of a smart pointer for the failfast object
//
void
ff_register_signal_handler(int sig, ff::failfast_var *failfast_varp)
{
	// Must specify a signal
	ASSERT(sig > 0);
	ASSERT(sig <= SIGRTMAX);

	// The following signals should not be specified
	ASSERT(sig != SIGKILL && sig != SIGSTOP && sig != SIGWAITING &&
	    sig != SIGCANCEL && sig != SIGLWP);

	// Must specify the location of the failfast object reference
	ASSERT(failfast_varp != NULL);

	//
	// This function can be used for multiple signals,
	// and hence can be called multiple times.
	// However, each time this function is called, the same
	// failfast object reference must be used.
	//
	ASSERT(failfast_var_for_signalp == NULL ||
	    failfast_var_for_signalp == failfast_varp);
	failfast_var_for_signalp = failfast_varp;

	// Register the signal handler for the specified signal
	if (signal(sig, ff_signal_handler) == SIG_ERR) {
		ASSERT(0);
	}
}
