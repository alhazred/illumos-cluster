/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CALLBACK_REGISTRY_IMPL_H
#define	_CALLBACK_REGISTRY_IMPL_H

#pragma ident	"@(#)callback_registry_impl.h	1.34	08/05/20 SMI"

//
// This file contains the implementation object for the CMM callback registry.
// All this class directly does is provide an idl interface to register a set
// of callbacks, and an internal interface used by the transitions_thread and
// the abort_thread to initiate the registered callbacks.
//
// The only direct state kept in this class is the number of callbacks
// registered (needed for knowing when all of the callbacks have completed)
// and a boolean indicating whether or not the callbacks have ever been run
// (to catch callers trying to register their callbacks too late).
//
// The rest of the state for the cmm_callback_registry is maintained in
// threads it creates in the add() method and other static state defined
// in the C++ file implementing this.
//

#include <h/cmm.h>
#include <orb/object/adapter.h>
#include <cmm/cmm_version.h>

class cmm_impl;
class cb;

class callback_registry_impl : public McServerof < cmm::callback_registry > {

	friend class cmm_impl;		// caller of step_trans(), etc.
	friend class automaton_impl;	// caller of terminate_trans()

public:
	callback_registry_impl(cmm_impl *);

	virtual		~callback_registry_impl();

	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//
	void add(const char *cb_name, cmm::callback_ptr cb,
			const cmm::callback_info &cbinfo, Environment& e);

private:
	//
	// Methods called from the transitions_thread
	//
	bool	start_trans(os::hrtime_t);
	void	set_reconf_data(cmm::seqnum_t, const cmm::membership_t &);
	bool	step_trans(uint_t, os::hrtime_t);
	bool	return_trans(os::hrtime_t);
	bool	stop_trans();
	void	abort_trans();

#ifdef CMM_VERSION_0
	//
	// Methods to change the set of CMM callback steps
	//
	void set_cmm_v0_steps();
	void set_cmm_v1_steps();

#endif // CMM_VERSION_0

	//
	// Method called from the CMM automaton (interrupt threads)
	// to signal the need to start another reconfiguration, requiring
	// the current transition to be terminated.
	//
	void	terminate_trans(cmm::seqnum_t);

	//
	// Hide default constructor, copy constructor and assignment operator.
	//
	callback_registry_impl();
	callback_registry_impl(const callback_registry_impl &);
	callback_registry_impl & operator = (callback_registry_impl &);

	//
	// Implementation declarations
	//
	cmm_impl   *_cmmp;
	os::mutex_t _lock;		// for protection of _count during add()
	int32_t	    _count;		// number of callbacks registered
	bool	    _already_started;	// to detect late add() calls
	cmm::callback_info	*cbreg_cbinfo;	// to store aggregate callback
						// info for the registry
#ifdef	CMM_VERSION_0
	bool use_cmm_v0_steps;		// Flag to use old set of callback
					// steps.
#endif // CMM_VERSION_0
};

#endif	/* _CALLBACK_REGISTRY_IMPL_H */
