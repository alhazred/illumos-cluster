//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)membership_manager_impl.cc	1.10	08/11/12 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <sys/clconf.h>
#include <cmm/cmm_impl.h>
#include <cmm/membership_manager_impl.h>
#include <cmm/membership_engine_impl.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_ns.h>
#include <nslib/ns.h>
#include <cmm/membership_threadpool.h>
#include <sys/cl_events.h>
#include <ccr/common.h>

#define	I_AM_LEADER (leader_cmm == orb_conf::local_nodeid())

//
// If a switch-case statement does not have a 'case' block for one of
// the possible values of the variable being checked for the 'switch',
// then lint complains although a 'default' case is defined.
// So we mask this lint complaint here.
//lint -e788

// Names of the states for membership state machine
static const char *
membership_state_machine_state_names[REMOVE_STATE + 1] = {
	"INVALID_STATE_DOWN",
	"INVALID_STATE_UP",
	"INITIAL_STATE",
	"UP_TRANSIT_STATE",
	"UP_PENDING_STATE",
	"UP_OBSOLETE_STATE",
	"DOWN_PENDING_STATE",
	"MEMBER_STATE",
	"NOT_MEMBER_STATE",
	"SHUTTING_DOWN_STATE",
	"REMOVE_STATE"
};

// Names of the events for membership state machine
static const char *
membership_state_machine_event_names[MEMBER_REMOVED + 1] = {
	"MEMBER_INVALID_EVENT",
	"MEMBER_NOT_IN_LIST",
	"MEMBER_IN_LIST",
	"MEMBER_OBSOLETE_IN_LIST",
	"MEMBER_DOWN_PENDING",
	"MEMBER_GOES_DOWN",
	"MEMBER_COMES_UP",
	"MEMBER_SHUTTING_DOWN",
	"MEMBER_REMOVED"
};

extern nodeid_t leader_cmm;
extern os::mutex_t latest_node_seqnum_lock;
extern cmm::seqnum_t latest_node_seqnum;
extern cmm::membership_t latest_node_membership;

bool new_base_cluster_reconfig_started(cmm::seqnum_t);

//
// Membership reconfiguration algorithm :
// ---------------------------------------
//
// There are three basic ways in which any membership reconfigures.
// (A) Reconfiguration due to a kernel CMM reconfiguration
// (B) Reconfiguration of the membership due to one or more members
// changing state, and not due to a kernel CMM reconfiguration
// (C) A special reconfiguration called shutdown reconfiguration
// when membership has to be shutdown
//
// The process for (A) above is a superset of the process for (B) or (C);
// in other words, (A) --> some prior steps followed by (B), or some prior
// steps followed by (C)
//
//
// (A) Reconfiguration due to a Kernel CMM reconfiguration
// ----------------------------------------------------
// (1) Membership engine delivers event to membership manager
// that kernel CMM has reconfigured
// (2) Leader membership manager obtains references to its peers
// (3) Leader membership manager queries each peer about
// its latest state (incarnation) and the latest seqnum the peer
// knows about
// (4) Leader membership manager builds a membership copy (M)
// consisting of the incarnations collected in step 3.
// It also considers the peer with the highest seqnum (S) as
// the "latest node" peer, in the sense that the "latest node" peer
// is supposed to have the information about the previous reconfiguration
// (basically, that peer's membership and seqnum must have been distributed
// the last time the membership reconfiguration took place)
// (5) Leader membership manager asks the "latest node" peer for its
// copy of the distributed membership (M'), and also for its shutdown flag.
// (6) Leader checks if the "latest node" peer has its shutdown flag set.
// If so, it means a shutdown reconfiguration was aborted due to
// the kernel CMM reconfiguration. Hence, leader drives a shutdown reconfig.
// If not, then leader checks if M is same as M' :
// (i) If (M == M'), it means that no members have changed state
// while the kernel CMM reconfiguration happened.
// Hence the leader membership manager drives process for (B) with M and S.
// (ii) If (M != M'), it means that one or more members changed state.
// Hence the leader membership manager drives process for (B) with M and (S+1).
//
// The above process is termed as "leader-data-collection".
//
//
// (B) Reconfiguration of membership when members change state
// without kernel CMM reconfiguration :
// ------------------------------------------------------------
// (1) Leader membership manager obtains references to peers, if not obtained.
// (2) If the membership type has a pre-membership-update step to be executed
// before the membership update happens, then the leader membership manager
// asks each peer to execute the pre-membership-update step.
// eg, Fencing is done in this pre-membership-update step
// for zone cluster membership
// (3) Then the leader membership manager asks each peer to update its own
// copy of membership and seqnum with the latest copy sent
//
//
// (C) Shutdown reconfiguration
// -----------------------------
// (1) Leader membership manager obtaines references to peers, if not obtained
// (2) If the membership type has a pre-shutdown-step to be executed,
// then the leader membership manager asks each peer to execute it.
// eg, Fencing is done in this pre-shutdown-step for zone cluster membership
// (3) Then the leader membership manager executes shutdown step on each peer
// which sets the shutdown flag, and nullifies distributed membership data
// (4) The leader membership manager executes finish-shutdown-step
// on each peer, which :
// (i) first does an optional pre-finish-shutdown step if specified by
// the membership type. eg, for zone cluster membership, this step has to ensure
// that the zone has actually shut down.
// (ii) then marks shutdown process complete by resetting shutdown flag
//


//
// Names of the event types delivered by engine to the membership managers.
//
const char *mem_manager_event_names[
    membership::NUM_OF_MEM_MANAGER_EVENT_TYPES] = {
	"BASE_CLUSTER_MEMBERSHIP_CHANGE",
	"NODE_UP",
	"NODE_DOWN",
	"PROCESS_UP",
	"PROCESS_DOWN",
	"DUMMY_MEM_MANAGER_EVENT_A",
	"DUMMY_MEM_MANAGER_EVENT_B",
	"DUMMY_MEM_MANAGER_EVENT_C",
	"DUMMY_MEM_MANAGER_EVENT_D"
};

//
// Construct the name of an event using the properties of the event passed in,
// and populate the string passed. The caller provides the memory.
// Return values :
// ENOENT : if no such cluster exists
// EINVAL : any other error
// 0 : success
//
int
construct_mem_manager_event_name_common(
    char *namep, membership::mem_manager_event event,
    const char *clid_strp, const char *proc_namep)
{
	switch (event) {
	case membership::BASE_CLUSTER_MEMBERSHIP_CHANGE:
		os::sprintf(namep, "%s : clid %s",
		    mem_manager_event_names[event], clid_strp);
		break;

	case membership::NODE_UP:
		os::sprintf(namep, "%s : clid %s",
		    mem_manager_event_names[event], clid_strp);
		break;

	case membership::NODE_DOWN:
		os::sprintf(namep, "%s : clid %s",
		    mem_manager_event_names[event], clid_strp);
		break;

	case membership::PROCESS_UP:
		os::sprintf(namep, "%s : clid %s, process %s",
		    mem_manager_event_names[event], clid_strp, proc_namep);
		break;

	case membership::PROCESS_DOWN:
		os::sprintf(namep, "%s : clid %s, process %s",
		    mem_manager_event_names[event], clid_strp, proc_namep);
		break;

	case membership::DUMMY_MEM_MANAGER_EVENT_A:
	case membership::DUMMY_MEM_MANAGER_EVENT_B:
	case membership::DUMMY_MEM_MANAGER_EVENT_C:
	case membership::DUMMY_MEM_MANAGER_EVENT_D:
		// Not used currently
		return (EINVAL);

	default:
		ASSERT(0);
		return (EINVAL);	/*lint !e527 */
	}

	return (0);
}

int
construct_mem_manager_event_name(
    char *namep, membership::mem_manager_event event,
    uint_t clid, const char *proc_namep)
{
	int num_digits = 0;
	char clid_str[CLID_MAX_LEN];

	num_digits = os::itoa((int)clid, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		return (EINVAL);
	}

	return (construct_mem_manager_event_name_common(
	    namep, event, clid_str, proc_namep));
}

int
construct_mem_manager_event_name(
    char *namep, membership::mem_manager_event event,
    const char *cl_namep, const char *proc_namep)
{
	uint_t clid = 0;
	int num_digits = 0;
	char clid_str[CLID_MAX_LEN];

	if (clconf_get_cluster_id((char *)cl_namep, &clid)) {
		return (ENOENT);
	}

	num_digits = os::itoa((int)clid, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		return (EINVAL);
	}

	return (construct_mem_manager_event_name_common(
	    namep, event, clid_str, proc_namep));
}

// Threadpool task used for leader data collection
class leader_data_collection_task : public defer_task {
public:
	leader_data_collection_task(membership_manager_impl *,
	    cmm::seqnum_t, const cmm::membership_t &);
	leader_data_collection_task();
	~leader_data_collection_task();
	void execute();
private:
	// Seqnum and membership of base cluster after kernel CMM reconfig
	cmm::seqnum_t seqnum;
	cmm::membership_t membership;

	// Membership manager which has to do the task
	membership_manager_impl *mem_managerp;
};

// Constructor
leader_data_collection_task::leader_data_collection_task(
    membership_manager_impl *managerp, cmm::seqnum_t new_seqnum,
    const cmm::membership_t &new_membership)
{
	seqnum = new_seqnum;
	membership = new_membership;
	mem_managerp = managerp;
}

// Default Constructor
leader_data_collection_task::leader_data_collection_task()
{
	// This constructor should not be used
	ASSERT(0);

	// lint complains about unreachable code
	//lint -e527
	seqnum = 0;
	for (unsigned int index = 0; index <= NODEID_MAX; index++) {
		membership.members[index] = INCN_UNKNOWN;
	}
	mem_managerp = NULL;
	//lint +e527
}

// Destructor
leader_data_collection_task::~leader_data_collection_task()
{
	mem_managerp = NULL;
}

// Execution of leader data collection task
void
leader_data_collection_task::execute()
{
	ASSERT(I_AM_LEADER);
	mem_managerp->leader_data_collection(seqnum, membership);
}

//
// Membership reconfiguration is done as part of a deferred threadpool task.
// Membership manager on the "leader" node schedules a membership
// reconfiguration task on a threadpool. This data is stored in the task.
//
struct membership_reconfig_data_t {

	// The membership manager for which reconfiguration will occur
	membership_manager_impl *mem_managerp;

	// Proposed new membership and seqnum
	cmm::membership_t membership;
	cmm::seqnum_t seqnum;

	//
	// Base cluster seqnum and membership
	// when the membership reconfiguration task
	// is put on the threadpool queue.
	//
	cmm::seqnum_t base_cluster_seqnum;
	cmm::membership_t base_cluster_membership;

	// Shutdown flag for the membership manager
	bool shutdown;
};

class membership_reconfig_task : public defer_task {
public:
	membership_reconfig_task(membership_manager_impl *,
	    const cmm::membership_t &, cmm::seqnum_t,
	    const cmm::membership_t &, cmm::seqnum_t, bool);
	membership_reconfig_task();
	~membership_reconfig_task();
	void execute();

	//
	// The unique address tag for this defer_task
	// is the address of the membership manager
	//
	virtual void *get_address_tag();

private:
	membership_reconfig_data_t data;
};

// Constructor
membership_reconfig_task::membership_reconfig_task(
    membership_manager_impl *managerp,
    const cmm::membership_t &new_membership, cmm::seqnum_t new_seqnum,
    const cmm::membership_t &base_cluster_membership,
    cmm::seqnum_t base_cluster_seqnum, bool shutdown)
{
	ASSERT(managerp);
	data.mem_managerp = managerp;
	data.membership = new_membership;
	data.seqnum = new_seqnum;
	data.base_cluster_membership = base_cluster_membership;
	data.base_cluster_seqnum = base_cluster_seqnum;
	data.shutdown = shutdown;
}

// Default Constructor
membership_reconfig_task::membership_reconfig_task()
{
	// This constructor should not be used.
	ASSERT(0);

	// lint complains about unreachable code
	//lint -e527
	data.mem_managerp = NULL;
	for (unsigned int index = 0; index <= NODEID_MAX; index++) {
		data.membership.members[index] = INCN_UNKNOWN;
		data.base_cluster_membership.members[index] = INCN_UNKNOWN;
	}
	data.seqnum = 0;
	data.base_cluster_seqnum = 0;
	data.shutdown = false;
	//lint +e527
}

// Destructor
membership_reconfig_task::~membership_reconfig_task()
{
}

// Execution of membership reconfiguration task
void
membership_reconfig_task::execute()
{
	//
	// Membership reconfig tasks are put on the threadpool
	// on the "leader" node only.
	//
	ASSERT(I_AM_LEADER);

	if (data.shutdown) {
		// Shutdown reconfiguration
		data.mem_managerp->shutdown_reconfiguration(
		    data.seqnum, data.base_cluster_seqnum,
		    data.base_cluster_membership);
	} else {
		// Membership reconfiguration
		data.mem_managerp->drive_reconfiguration(
		    data.membership, data.seqnum,
		    data.base_cluster_membership,
		    data.base_cluster_seqnum);
	}
}

//
// The unique address tag for this defer_task
// is the address of the membership manager
//
void *
membership_reconfig_task::get_address_tag()
{
	return ((void *)(data.mem_managerp));
}

//
// Returns true if the argument is older than the latest seqnum
// for the membership manager
//
bool
membership_manager_impl::newer_reconfig_started(cmm::seqnum_t check_seqnum)
{
	latest_reconfig_lock.lock();
	bool retval = ((latest_seqnum > check_seqnum)? true: false);
	latest_reconfig_lock.unlock();
	return (retval);
}

// Returns true if the membership is empty
bool
membership_is_empty(const cmm::membership_t &membership_to_test)
{
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership_to_test.members[nid] != INCN_UNKNOWN) {
			return (false);
		}
	}
	return (true);
}

// Returns true if the two memberships differ
bool
membership_has_changed(
    const cmm::membership_t &old_membership,
    const cmm::membership_t &new_membership)
{
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (old_membership.members[nid] !=
		    new_membership.members[nid]) {
			return (true);
		}
	}
	return (false);
}

extern const char *membership_type_names[];

//
// Construct the name of the membership manager to be used
// in CMM trace buffer messages and syslog messages.
// The membership manager name contains the cluster ID and
// not the cluster name. We should have the cluster name specified
// in trace/syslog messages.
// Callers of this method should free the memory for the returned string.
//
// static
char *
membership_manager_impl::construct_name_to_log(
    membership::membership_type mem_type,
    const char *cl_namep, const char *proc_namep)
{
	char *namep = NULL;
	size_t len = 0;

	if ((mem_type == membership::PROCESS_UP_PROCESS_DOWN) ||
	    (mem_type == membership::PROCESS_UP_ZONE_DOWN)) {
		// Process membership type

		// The length calculated below includes a few extra chars.
		len = os::strlen("mem_type %s, cluster %s, process %s") +
		    MEMBERSHIP_TYPE_MAX_LEN + os::strlen(cl_namep) +
		    os::strlen(proc_namep) + 1;
		namep = new char[len];
		if (namep == NULL) {
			return (NULL);
		}
		os::sprintf(namep, "mem_type %s, cluster %s, process %s",
		    membership_type_names[mem_type], cl_namep, proc_namep);
	} else {
		// Cluster membership type

		// The length calculated below includes a few extra chars.
		len = os::strlen("mem_type %s, cluster %s") +
		    MEMBERSHIP_TYPE_MAX_LEN + os::strlen(cl_namep) + 1;
		namep = new char[len];
		if (namep == NULL) {
			return (NULL);
		}
		os::sprintf(namep, "mem_type %s, cluster %s",
		    membership_type_names[mem_type], cl_namep);
	}
	return (namep);
}

// Constructor
membership_manager_impl::membership_manager_impl(
    membership::membership_type mem_type, bool gen_event,
    const char *cl_namep, const char *proc_namep)
{
	state_lock.lock();

	type = mem_type;
	generate_event = gen_event;

	current_state = INVALID_STATE_DOWN;

	membership_info_received_from_leader = false;
	leader_data_collection_underway = false;

	initiate_fencing_flag = false;
	pre_mem_update_flag = false;
	pre_mem_shutdown_flag = false;
	first_boot = true;

	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		membership.members[nid] = INCN_UNKNOWN;
	}
	seqnum = 0;
	shutdown = false;

	ASSERT(cl_namep);
	(void) os::strcpy(cluster_name, cl_namep);
	if (clconf_get_cluster_id((char *)cl_namep, &cluster_id)) {
		//
		// Cluster must have a valid cluster ID.
		// Only membership subsystem can create membership
		// manager objects. Hence, garbage cluster names
		// cannot be passed to this constructor.
		//
		ASSERT(0);
	}

	if ((type == membership::PROCESS_UP_PROCESS_DOWN) ||
	    (type == membership::PROCESS_UP_ZONE_DOWN)) {
		ASSERT(proc_namep);
	}
	if (proc_namep) {
		(void) os::strcpy(process_name, proc_namep);
	} else {
		// Mark the name as null
		process_name[0] = '\0';
	}

	int err = construct_membership_name(
	    mem_manager_name, type, cluster_id, proc_namep);
	if (err == ENOENT) {
		// Cluster must exist with a valid cluster ID
		ASSERT(0);
	} else if (err == EINVAL) {
		// Should not happen
		ASSERT(0);
	} else if (err != 0) {
		// Cannot happen
		ASSERT(0);
	}

	name_to_log = membership_manager_impl::construct_name_to_log(
	    mem_type, cl_namep, proc_namep);
	ASSERT(name_to_log);

	latest_incn = INCN_UNKNOWN;
	latest_incn_known_to_state_machine = INCN_UNKNOWN;
	latest_reconfig_lock.lock();
	latest_seqnum = 0;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		latest_membership.members[nid] = INCN_UNKNOWN;
	}
	latest_reconfig_lock.unlock();

	event_actions[LEADER_DATA_COLLECTION].funcp =
	    &membership_manager_impl::base_cluster_membership_change;
	event_actions[MEMBER_CAME_UP].funcp =
	    &membership_manager_impl::member_came_up;
	event_actions[MEMBER_WENT_DOWN].funcp =
	    &membership_manager_impl::member_went_down;

	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		peers[nid] = membership::manager::_nil();
	}

	// Handlers
	handlers[INVALID_STATE_DOWN] =
				&membership_manager_impl::invalid_state_down;
	handlers[INVALID_STATE_UP] =
				&membership_manager_impl::invalid_state_up;
	handlers[INITIAL_STATE] =
				&membership_manager_impl::initial_state;
	handlers[UP_TRANSIT_STATE] =
				&membership_manager_impl::up_transit_state;
	handlers[UP_PENDING_STATE] =
				&membership_manager_impl::up_pending_state;
	handlers[UP_OBSOLETE_STATE] =
				&membership_manager_impl::up_obsolete_state;
	handlers[DOWN_PENDING_STATE] =
				&membership_manager_impl::down_pending_state;
	handlers[MEMBER_STATE] =
				&membership_manager_impl::member_state;
	handlers[NOT_MEMBER_STATE] =
				&membership_manager_impl::not_member_state;
	handlers[SHUTTING_DOWN_STATE] =
				&membership_manager_impl::shutting_down_state;

	state_lock.unlock();
}

//
// Do a bunch of initialization tasks for this object.
// In other words, after this method completes,
// this membership manager's functionality is up and running.
//
bool
membership_manager_impl::start(void)
{
	Environment e;
	naming::naming_context_var ctx_v =
	    naming::naming_context::_nil();
	membership::manager_var obj_v = membership::manager::_nil();

	// Bind reference in local nameserver
	obj_v = this->get_objref();
	ASSERT(!CORBA::is_nil(obj_v));
	ctx_v = ns::local_nameserver_cz(cluster_name);
	while (CORBA::is_nil(ctx_v)) {
		os::usecsleep((long)1000);	// 1ms
		ctx_v = ns::local_nameserver_cz(cluster_name);
	}
	ctx_v->bind(mem_manager_name, obj_v, e);
	if (e.exception() != NULL) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "could not bind ref in lns\n", name_to_log));
		// XXX Check exceptions from bind call
		return (false);
	}

	create_state_machine();
	register_with_engine();
	return (true);
}

//
// Executing this method stops the functionality of this membership manager.
// This method does a bunch of tasks that ensure that the object can be
// safely removed hereafter.
// Returns true on success, false on failure
//
bool
membership_manager_impl::stop(void)
{
	state_lock.lock();

	//
	// Ensure that there are no pending tasks on membership reconfig
	// threadpool or callback threadpool.
	// If any, such tasks are purged from the threadpools.
	//
	// This membership is in the process of being removed.
	// If any up/down notifications have reached non-leader nodes,
	// then that means one or more of the member components
	// are still not shut down, in which case, the CLI will not
	// allow the removal of the membership.
	// (Note that CLI is used to delete zone cluster and this triggers
	// deletion of zone cluster membership and any other process
	// membership related to the zone cluster.
	// By then the CLI will have shut down the zone cluster;
	// this would have shut down the zone cluster membership and
	// process memberships related to the zone cluster).
	// So we can be assured that there are no up/down notifications
	// on the non-leader nodes to be processed.
	//

	membership_threadpool::the().
	    remove_pending_with_address_tag((void *)this);

	callback_threadpool::the().
	    remove_pending_with_address_tag((void *)this);

	//
	// Signal the state machine that the membership is being removed.
	// The state machine would auto destroy itself.
	//

	// Allocate the event packet structure to push on to event list
	event_packet_t *ev_packetp = new event_packet_t;
	//
	// If we are unable to allocate this small amount of memory
	// required for the event packet structure, then this node has
	// serious memory shortage. Syslog a message and panic the node.
	//
	if (ev_packetp == NULL) {
		//
		// SCMSGS
		// @explanation
		// Data structures required for membership reconfiguration
		// could not be created. This hints at real shortage of memory.
		// @user_action
		// Lack of memory can lead to other problems on the node.
		// This node will now halt to avoid more problems.
		// Add more memory to the node and boot it back.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Could not create event packet "
		    "for membership (%s); node is low on memory", name_to_log);
	}

	// Put in event packet data
	ev_packetp->event = MEMBER_REMOVED;

	//
	// Other data in event packet is not required for MEMBER_REMOVED.
	// Initialize to default values.
	//
	ev_packetp->latest_incn = INCN_UNKNOWN;
	ev_packetp->seqnum = 0;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		ev_packetp->membership.members[nid] = INCN_UNKNOWN;
	}
	event_list.append(ev_packetp);
	state_cv.signal();
	state_lock.unlock();

	// Unregister from membership engine
	unregister_with_engine();

	// Wait here for the state machine to remove itself.
	state_lock.lock();
	while (current_state != REMOVE_STATE) {
		state_lock.unlock();
		os::usecsleep((long)1000);	// 1ms
		state_lock.lock();
	}

	//
	// Try to unregister from local nameserver.
	// Since nameserver and membership subsystems clean up asynchronously
	// their own data in the case of zone cluster deletion, there is a race
	// between membership and nameserver. So, here we could find that
	// the context does not exist, or maybe the context exists but
	// the object ref is not found in the nameserver; in these cases,
	// we can assume that the reference in the nameserver is gone.
	//
	Environment e;
	CORBA::Exception *exp = NULL;
	naming::naming_context_var ctx_v = naming::naming_context::_nil();
	ctx_v = ns::local_nameserver_cz(cluster_name);
	if (CORBA::is_nil(ctx_v)) {
		// Context has already gone away
		state_lock.unlock();
		return (true);
	}

	ctx_v->unbind(mem_manager_name, e);
	exp = e.exception();
	if (exp == NULL) {
		state_lock.unlock();
		return (true);
	}

	if (CORBA::INV_OBJREF::_exnarrow(exp)) {
		// Context has already gone away
		e.clear();
		state_lock.unlock();
		return (true);
	}

	if (naming::not_found::_exnarrow(exp)) {
		// lint suggests using dynamic_cast to downcast
		//lint -e1774
		if (((naming::not_found *)exp)->why == naming::not_object) {
		//lint +e1774
			// Object reference has already gone away
			e.clear();
			state_lock.unlock();
			return (true);
		}
	}

	//
	// If we reach here, it means an exception happened
	// that was not expected.
	//
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "unbind ref from lns failed\n", name_to_log));

	state_lock.unlock();
	return (false);
}

// Create state machine thread
void
membership_manager_impl::create_state_machine()
{
	if (!cmm_impl::cmm_create_thread(
	    membership_manager_impl::start_state_machine_thread,
	    (void *)this, CMM_MAXPRI - 1)) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "could not create state machine thread\n", name_to_log));
		//
		// SCMSGS
		// @explanation
		// A thread required for membership could not be created.
		// This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership : Could not create state machine thread "
		    "for membership (%s).", name_to_log);
		CL_PANIC(0);
	}

	//
	// Wait here till state machine goes out of INVALID_STATE_DOWN,
	// and wakes us up in INITIAL_STATE. The state machine would hold
	// lock, transition from INVALID_STATE_DOWN to INITIAL_STATE (or beyond
	// in case of multiple events) and then release lock.
	// So if current state of state machine is not INVALID_STATE_DOWN,
	// then we can assume that state machine has started up properly.
	//
	state_lock.lock();
	while (current_state == INVALID_STATE_DOWN) {
		state_cv.wait(&state_lock);
	}
	state_lock.unlock();
}

//
// Destructor
//
// lint says some functions used in destructor may throw exceptions.
//lint -e1551
membership_manager_impl::~membership_manager_impl()
{
	membership::client_ptr client_p;

	membership_clients_lock.lock();
	while ((client_p = membership_clients.reapfirst()) != NULL) {
		CORBA::release(client_p);
		client_p = membership::client::_nil();
	}
	membership_clients_lock.unlock();

	if (name_to_log) {
		delete [] name_to_log;
	}
	name_to_log = NULL;
}
//lint +e1551

void
#ifdef DEBUG
membership_manager_impl::_unreferenced(unref_t cookie)
#else
membership_manager_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

bool
membership_manager_impl::match_manager_info(
    membership::membership_type mem_type,
    const char *cl_namep, const char *proc_namep)
{
	if (type != mem_type) {
		return (false);
	}

	if (os::strcmp(cluster_name, cl_namep) != 0) {
		return (false);
	}

	if (os::strcmp(process_name, "") != 0) {
		// We have a process name
		if (!proc_namep) {
			return (false);
		}
		if (os::strcmp(process_name, proc_namep) != 0) {
			return (false);
		}
	} else {
		// We do not have a process name
		if (proc_namep) {
			return (false);
		}
	}

	return (true);
}

bool
membership_manager_impl::leader_initialization(
    cmm::seqnum_t base_cluster_seqnum,
    const cmm::membership_t &base_cluster_membership)
{
	ASSERT(I_AM_LEADER);
	state_lock.lock();
	bool retval =
	    get_peer_refs(base_cluster_seqnum, base_cluster_membership);
	state_lock.unlock();
	return (retval);
}

bool
membership_manager_impl::leader_fini()
{
	ASSERT(I_AM_LEADER);
	nodeid_t local_nid = orb_conf::local_nodeid();
	state_lock.lock();
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		//
		// This action is done on the leader manager
		// to release the CORBA references to its peers.
		// If the leader releases its own reference,
		// then the leader manager object might itself
		// get unreferenced before it completes its task.
		// So if we are the leader, we do not release
		// our own reference.
		//
		if (nid == local_nid) {
			continue;
		}

		if (!CORBA::is_nil(peers[nid])) {
			CORBA::release(peers[nid]);
		}
	}
	state_lock.unlock();

	//
	// Now that we are done with all cleanup, we can safely
	// release the reference to this leader manager object.
	// This leader manager object will receive a unreference
	// notification and will delete itself.
	//
	if (!CORBA::is_nil(peers[local_nid])) {
		CORBA::release(peers[local_nid]);
	}

	return (true);
}

bool
membership_manager_impl::get_peer_refs(cmm::seqnum_t base_cluster_seqnum,
    const cmm::membership_t &base_cluster_membership)
{
	ASSERT(state_lock.lock_held());

	// Get references to peers, if not already done so
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_base_cluster_reconfig_started(base_cluster_seqnum)) {
			return (false);
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (base_cluster_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		if (!CORBA::is_nil(peers[nid])) {
			continue;
		}

		peers[nid] = cmm_ns::get_membership_manager_ref_zc(
		    cluster_name, mem_manager_name, nid);
		while (CORBA::is_nil(peers[nid])) {
			MEMBERSHIP_TRACE(("Membership Manager (%s) : "
			    "nil ref, node %d, waiting...\n",
			    name_to_log, nid));
			if (new_base_cluster_reconfig_started(
			    base_cluster_seqnum)) {
				return (false);
			}
			os::usecsleep((long)1000);	// 1ms
			peers[nid] = cmm_ns::get_membership_manager_ref_zc(
			    cluster_name, mem_manager_name, nid);
		}
	}
	return (true);
}

//
// "Leader" drives a reconfiguration of distributed membership data.
// Essentially process for (B), as described in block comment
// about algorithm
//
void
membership_manager_impl::drive_reconfiguration(
    const cmm::membership_t &new_membership, cmm::seqnum_t new_seqnum,
    const cmm::membership_t &node_membership, cmm::seqnum_t node_seqnum)
{
	Environment e;
	CORBA::Exception *exp = NULL;

	// Check that we are the "leader"
	ASSERT(I_AM_LEADER);

	state_lock.lock();

	//
	// If a new base cluster reconfiguration has started,
	// then return without further progress.
	//
	if (new_base_cluster_reconfig_started(node_seqnum)) {
		state_lock.unlock();
		return;
	}

	//
	// If another reconfiguration has triggered for this
	// membership manager, return without further progress.
	//
	if (newer_reconfig_started(new_seqnum)) {
		state_lock.unlock();
		return;
	}

	//
	// In some cases, the new seqnum could be same as the old seqnum.
	// Such scenario might occur during a node reconfiguration,
	// when the "leader" needs to push the membership data to all peers,
	// without changing seqnum of the membership manager.
	//
	ASSERT(new_seqnum >= seqnum);

	//
	// If a shutdown was in progress for this membership manager,
	// then wait for the shutdown to complete.
	//
	while (shutdown) {
		// Leave lock for the shutdown to complete
		state_lock.unlock();
		os::usecsleep((os::usec_t)1000);	// 1ms
		state_lock.lock();
	}

	MEMBERSHIP_TRACE(("Membership Manager (%s) : reconfiguration started "
	    "(seqnum %d)\n", name_to_log, new_seqnum));

	if (!get_peer_refs(node_seqnum, node_membership)) {
		//
		// Failed to get peer refs, or a new base cluster CMM
		// reconfiguration triggered.
		//
		state_lock.unlock();
		return;
	}

	// Execute pre_mem_update step, if specified by the membership type
	if (pre_mem_update_flag) {
		pre_mem_update(new_membership, new_seqnum, node_seqnum);
	}

	//
	// If a new base cluster reconfiguration has started,
	// then return without further progress.
	//
	if (new_base_cluster_reconfig_started(node_seqnum)) {
		state_lock.unlock();
		return;
	}

	//
	// If another reconfiguration has triggered for this
	// membership manager, return without further progress.
	//
	if (newer_reconfig_started(new_seqnum)) {
		state_lock.unlock();
		return;
	}

	// Execute update-membership-step
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {

		//
		// If a new base cluster reconfiguration has started,
		// then return without further progress.
		//
		if (new_base_cluster_reconfig_started(node_seqnum)) {
			state_lock.unlock();
			return;
		}

		//
		// If another reconfiguration has triggered for this
		// membership manager, return without further progress.
		//
		if (newer_reconfig_started(new_seqnum)) {
			state_lock.unlock();
			return;
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (node_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		// Update membership on each peer
		peers[nid]->update_membership_step(
		    new_membership, new_seqnum, e);

		if ((exp = e.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Manager (%s) : "
				    "COMM_FAILURE exception for nodeid %d "
				    "at update_membership_step, "
				    "aborting reconfig (seqnum %d)\n",
				    name_to_log, nid, new_seqnum));
				e.clear();
				state_lock.unlock();
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "unexpected exception %s at "
				    "update_membership_step\n",
				    name_to_log, exp->_name()));
				ASSERT(0);
				//lint -e527
				e.clear();
				state_lock.unlock();
				return;
				//lint +e527
			}
		}
	}

	MEMBERSHIP_TRACE(("Membership Manager (%s) : reconfiguration done "
	    "(seqnum = %d)\n", name_to_log, new_seqnum));
	state_lock.unlock();
}

//
// Data collection by leader after kernel CMM reconfiguration
//
void
membership_manager_impl::leader_data_collection(
    cmm::seqnum_t new_node_seqnum, const cmm::membership_t &new_node_membership)
{
	Environment e;
	CORBA::Exception *exp = NULL;
	cmm::seqnum_t collected_seqnum = 0;
	nodeid_t latest_node = NODEID_UNKNOWN;
	cmm::membership_t collected_membership;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		collected_membership.members[nid] = INCN_UNKNOWN;
	}

	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "Leader querying states of peers\n", name_to_log));

	state_lock.lock();
	// Release previously held references if any
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (!CORBA::is_nil(peers[nid])) {
			CORBA::release(peers[nid]);
			peers[nid] = membership::manager::_nil();
		}
	}

	if (!get_peer_refs(new_node_seqnum, new_node_membership)) {
		//
		// Failed to get peer refs, or a new base cluster CMM
		// reconfiguration triggered.
		//
		state_lock.unlock();
		return;
	}
	state_lock.unlock();

	// Query latest state of peers
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_base_cluster_reconfig_started(new_node_seqnum)) {
			return;
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (new_node_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		cmm::seqnum_t peer_seqnum = 0;
		sol::incarnation_num peer_incn = INCN_UNKNOWN;
		peers[nid]->query_latest_state(peer_seqnum, peer_incn, e);
		if ((exp = e.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "COMM_FAILURE exception from "
				    "query_latest_state() on nodeid %d, "
				    "aborting leader_data_collection() "
				    "(base cluster seqnum %d)\n",
				    name_to_log, nid, new_node_seqnum));
				e.clear();
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "unexpected exception %s at "
				    "leader_data_collection()\n",
				    name_to_log, exp->_name()));
				ASSERT(0);
				//lint -e527
				e.clear();
				return;
				//lint +e527
			}
		}

		MEMBERSHIP_TRACE((
		    "Membership Manager (%s) : received from node %d "
		    "that peer has incn %d\n", name_to_log, nid, peer_incn));

		// Store incarnation of peer
		collected_membership.members[nid] = peer_incn;

		//
		// If the peer, from which we collected data,
		// had a seqnum greater than the highest seqnum
		// that the leader has collected till now,
		// then consider this newly obtained seqnum as
		// the highest seqnum known. It is also
		// likely that the same peer had the latest
		// membership data.
		//
		if (peer_seqnum > collected_seqnum) {
			collected_seqnum = peer_seqnum;
			latest_node = nid;
		}
	}

	//
	// Get the latest known membership from the peer
	// that is supposed to have the latest data.
	//
	bool shutdown_flag = false;
	cmm::membership_t old_membership;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		old_membership.members[nid] = INCN_UNKNOWN;
	}

	//
	// If latest_node is zero, it means there has been
	// no prior reconfiguration. Seqnum is zero.
	// So we can consider the old membership to be empty.
	// In that case, we do not need to query the latest_node.
	//
	if (latest_node != 0) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "asking peer on node %d for latest membership data\n",
		    name_to_log, latest_node));

		peers[latest_node]->get_distributed_membership(
		    old_membership, shutdown_flag, e);

		if ((exp = e.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "COMM_FAILURE exception while "
				    "asking latest node %d, "
				    "aborting leader_data_collection() "
				    "(base cluster seqnum %d)\n",
				    name_to_log, latest_node, new_node_seqnum));
				e.clear();
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "unexpected exception %s while asking "
				    "latest node %d\n", name_to_log,
				    exp->_name(), latest_node));
				ASSERT(0);
				//lint -e527
				e.clear();
				return;
				//lint +e527
			}
		}
	}

	if (shutdown_flag) {
		//
		// The membership manager was being shutdown when
		// the base cluster reconfiguration happened.
		// Finish the shutdown reconfiguration.
		//

		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "driving unfinished shutdown reconfig\n", name_to_log));

		latest_reconfig_lock.lock();
		ASSERT(latest_seqnum <= collected_seqnum);
		latest_seqnum = collected_seqnum;
		for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
			latest_membership.members[nid] = INCN_UNKNOWN;
		}
		latest_reconfig_lock.unlock();

		shutdown_reconfiguration(collected_seqnum,
		    new_node_seqnum, new_node_membership);

	} else {
		//
		// We have two memberships now :
		// (1) the peer that is supposed to have the latest data
		//	has some membership data
		// (2) we have built a membership from the incarnations
		//	collected from all peers
		// If these two memberships differ, then we can assume
		// that:
		// (i) some peers changed state during
		// the base cluster reconfiguration, or
		// (ii) maybe the previous reconfiguration
		// was aborted, due to the base cluster
		// reconfiguration, before the latest membership
		// could have been intimated to any peer.
		// So, we should reconfigure the membership
		// with an increase in seqnum.
		//
		// On the other hand, if the two memberships do not
		// differ, then we can assume that the peer that is
		// considered to have the latest membership data,
		// does actually have the latest data.
		// Perhaps a reconfiguration was aborted,
		// due to the base cluster reconfiguration,
		// after the latest membership was intimated
		// to that peer and thats how it has the latest data.
		// Thus we only need to push this membership data to
		// all peers without any increase in seqnum.
		//
		if (membership_has_changed(
		    old_membership, collected_membership)) {
			MEMBERSHIP_TRACE(("Membership Manager (%s) : "
			    "membership changed after kernel CMM reconfig\n",
			    name_to_log));
			++collected_seqnum;
		} else {
			MEMBERSHIP_TRACE(("Membership Manager (%s) : "
			    "no change in membership "
			    "after kernel CMM reconfig", name_to_log));
		}

		//
		// Update the latest seqnum and membership data
		// that leader knows about.
		//
		latest_reconfig_lock.lock();
		latest_seqnum = collected_seqnum;
		latest_membership = collected_membership;
		latest_reconfig_lock.unlock();

		// Reconfigure the membership
		drive_reconfiguration(
		    collected_membership, collected_seqnum,
		    new_node_membership, new_node_seqnum);
	}
	leader_data_collection_underway = false;
}

//
// "Leader" invokes this IDL method on the peer that is
// supposed to have the latest membership information.
//
void
membership_manager_impl::get_distributed_membership(
    cmm::membership_t &old_membership, bool &shutdown_flag, Environment &)
{
	MEMBERSHIP_TRACE(("Membership Manager (%s) : Leader thinks "
	    "I have latest membership data\n", name_to_log));

	state_lock.lock();
	old_membership = membership;
	shutdown_flag = shutdown;
	state_lock.unlock();
}

//
// "Leader" calls this IDL method on all peers
// to update the membership information.
//
void
membership_manager_impl::update_membership_step(
    const cmm::membership_t &new_membership,
    cmm::seqnum_t new_seqnum, Environment &)
{
	MEMBERSHIP_TRACE(("Membership Manager (%s) : Notified by leader "
	    "to execute update-membership-step (seqnum %d)\n",
	    name_to_log, new_seqnum));

	if (!I_AM_LEADER) {
		state_lock.lock();
	}

	//
	// The reconfiguration of membership started on the "leader", which
	// means that the shutdown flag is cleared on the "leader".
	// If the shutdown flag is set, then it means I am not the "leader".
	// The membership data is locked on the "leader" only, through the
	// entire process of reconfiguration. So we can hold locks and wait,
	// without causing deadlocks. Wait till the shutdown flag is cleared.
	//
	while (shutdown) {
		ASSERT(!I_AM_LEADER);
		// Leave lock to allow the shutdown flag to be cleared.
		state_lock.unlock();
		os::usecsleep((os::usec_t)1000);	// 1ms
		state_lock.lock();
	}

	ASSERT(state_lock.lock_held());
	ASSERT(!shutdown);

	//
	// In some cases, the new seqnum could be same as the old seqnum.
	// Such scenario might occur during a node reconfiguration,
	// when the "leader" needs to push the membership data
	// to all peers, without changing seqnum.
	//
	ASSERT(seqnum <= new_seqnum);

	// Allocate the event packet structure to push on to event list
	event_packet_t *ev_packetp = new event_packet_t;
	if (ev_packetp == NULL) {
		//
		// Unable to allocate small amount of memory;
		// serious memory shortage
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Could not create event packet "
		    "for membership (%s); node is low on memory", name_to_log);
	}

	//
	// There is locking and sleeping involved here, in order to communicate
	// with the state machine thread. Ideally, if
	// a new base cluster reconfiguration starts, we should abort any
	// pending stuff in the current reconfiguration task and return back.
	// But since, signalling the state machine is not expected to take long,
	// we can afford to continue to complete the task to signal
	// the state machine thread properly before we return.
	//
	nodeid_t local_nid = orb_conf::local_nodeid();
	if (new_membership.members[local_nid] == INCN_UNKNOWN) {
		//
		// The local member component is marked down (INCN_UNKNOWN)
		// in the membership delivered by the leader membership manager.
		//
		ev_packetp->event = MEMBER_NOT_IN_LIST;

	} else if ((new_membership.members[local_nid] != INCN_UNKNOWN) &&
	    (latest_incn != INCN_UNKNOWN) &&
	    (new_membership.members[local_nid] == latest_incn)) {
		//
		// Latest incarnation of local member component
		// (which is not INCN_UNKNOWN) is reflected in the membership
		// delivered by the leader membership manager.
		//
		ev_packetp->event = MEMBER_IN_LIST;

	} else if ((new_membership.members[local_nid] != INCN_UNKNOWN) &&
	    (latest_incn != INCN_UNKNOWN) &&
	    (new_membership.members[local_nid] != latest_incn)) {
		//
		// The local member component has a "up" latest incarnation.
		// The leader membership manager has delivered membership
		// in which the local member component is marked "up".
		// But these two "up" incarnations do not match.
		//
		ev_packetp->event = MEMBER_OBSOLETE_IN_LIST;

	} else if ((new_membership.members[local_nid] != INCN_UNKNOWN) &&
	    (latest_incn == INCN_UNKNOWN)) {
		//
		// Latest incarnation of local member component is
		// INCN_UNKNOWN, but the local member component is marked "up"
		// in the membership delivered by the leader membership manager.
		//
		ev_packetp->event = MEMBER_DOWN_PENDING;

	} else {
		// Must not be reached
		ASSERT(0);

		// lint complains that code is unreachable
		//lint -e527

		//
		// Directly transition to one of the invalid states -
		// either INVALID_STATE_DOWN or INVALID_STATE_UP -
		// using a MEMBER_INVALID_EVENT.
		// Empty out event queue and put the MEMBER_INVALID_EVENT
		// on to the event queue.
		//
		event_packet_t *iter_ev_packetp = NULL;
		while ((iter_ev_packetp = event_list.reapfirst()) != NULL) {
			delete iter_ev_packetp;
		}

		ev_packetp->event = MEMBER_INVALID_EVENT;

		//
		// The state machine will enter one of the invalid states -
		// either INVALID_STATE_UP or INVALID_STATE_DOWN -
		// as a result of this invalid event.
		// Trace a message into the trace buffer and
		// syslog a warning message as well.
		//
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "MEMBER_INVALID_EVENT occurred on receiving membership "
		    "from leader, current state is %s, leader says incn = %d, "
		    "local incn known to state machine = %d\n", name_to_log,
		    membership_state_machine_state_names[current_state],
		    new_membership.members[local_nid],
		    latest_incn_known_to_state_machine));
		//
		// SCMSGS
		// @explanation
		// An invalid event has occurred for the state machine
		// of a particular membership.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership : invalid event occurred for "
		    "the state machine of '%s' membership on receiving "
		    "membership from leader, current state is %s, "
		    "leader says incarnation = %d, local incarnation "
		    "known to state machine = %d.", name_to_log,
		    membership_state_machine_state_names[current_state],
		    new_membership.members[local_nid],
		    latest_incn_known_to_state_machine);

		//lint +e527
	}

	// Store the membership data sent by leader into the event packet
	ev_packetp->seqnum = new_seqnum;
	ev_packetp->membership = new_membership;

	//
	// In this scenario for state machine event generation,
	// the leader has delivered membership data. So we only populate
	// that data into the event packet. The latest incarnation field
	// in the event packet is not required for these kind of events
	// and hence is initialized to default value.
	//
	ev_packetp->latest_incn = INCN_UNKNOWN;

	// Put event packet on to the event list (at the end of the list)
	event_list.append(ev_packetp);

	//
	// There is only one state lock, shared
	// by state machine and reconfiguration task.
	// We signal the condition variable here,
	// but the state machine would be able to run
	// only after we release state lock.
	// On non-leaders, the state lock is released now.
	// But on leader, the state lock would be released
	// only after reconfiguration task is complete.
	// Effectively, this means the state machine on leader
	// would be the last to wake up and change states
	// when a membership reconfiguration is being done.
	//
	state_cv.signal();
	if (!I_AM_LEADER) {
		state_lock.unlock();
	}
}

//
// Request a membership manager on local base node
// to start the shutdown of this membership.
// The local membership manager will, in turn, request
// the "leader" manager for this membership to start the shutdown process.
//
// Returns true on success, false on failure.
//
bool
membership_manager_impl::start_shutdown(Environment&)
{
	//
	// If leader-data-collection reconfig is underway,
	// wait until it finishes
	//
	if (I_AM_LEADER) {
		while (leader_data_collection_underway) {
			os::usecsleep((long)1000);	// 1ms
		}
	}

	// Get reference to "leader" membership manager
	membership::manager_var leader_v =
	    cmm_ns::get_membership_manager_ref_zc(
	    cluster_name, mem_manager_name, leader_cmm);
	if (CORBA::is_nil(leader_v)) {
		//
		// This can happen only if the "leader" has gone away
		// owing to the deletion of this membership in progress.
		// We would get deleted soon.
		// So we must return without progress.
		//
		return (false);
	}

	Environment env;
	leader_v->tell_leader_to_shutdown(env);
	if (env.exception()) {
		ASSERT(0);
		return (false);	/*lint !e527 */
	}

	return (true);
}

//
// This method is invoked on the "leader" manager for a membership,
// as part of which the "leader" drives all peers through the shutdown process.
//
void
membership_manager_impl::tell_leader_to_shutdown(Environment &e)
{
	ASSERT(I_AM_LEADER);

	MEMBERSHIP_TRACE(("Membership Manager (%s) : request to shutdown "
	    "received from node %d\n", name_to_log, e.get_src_node().ndid));

	latest_node_seqnum_lock.lock();
	cmm::seqnum_t base_cluster_seqnum = latest_node_seqnum;
	cmm::membership_t base_cluster_membership = latest_node_membership;
	latest_node_seqnum_lock.unlock();

	state_lock.lock();
	shutdown = true;

	latest_reconfig_lock.lock();
	latest_seqnum += 1;
	cmm::seqnum_t shutdown_seqnum = latest_seqnum;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		latest_membership.members[nid] = INCN_UNKNOWN;
	}
	latest_reconfig_lock.unlock();

	if (!get_peer_refs(base_cluster_seqnum, base_cluster_membership)) {
		//
		// Failed to get peer refs, or a new base cluster CMM
		// reconfiguration triggered.
		//
		state_lock.unlock();
		return;
	}
	state_lock.unlock();

	MEMBERSHIP_TRACE(("Membership Manager (%s) : shutdown started\n",
	    name_to_log));

	//
	// First the leader membership manager ensures that
	// no reconfiguration tasks for this membership remain pending
	// on the membership threadpool.
	// If any, such tasks are purged from the threadpool.
	//
	membership_threadpool::the().
	    remove_pending_with_address_tag((void *)this);

	//
	// Shutdown is benevolent, in the sense that,
	// say shutdown was aborted due to a base cluster reconfig,
	// and then the shutdown reconfig happens after kernel CMM reconfigures.
	// The leader asks every peer to shutdown. A peer could have already
	// shutdown before the base cluster reconfig triggered
	// or maybe the peer is newly booted up.
	// Even in such cases, shutdown does no harm to the peer.
	//

	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {

		//
		// If a new base cluster reconfiguration has started,
		// then return without further progress.
		//
		if (new_base_cluster_reconfig_started(base_cluster_seqnum)) {
			return;
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (base_cluster_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		Environment env;
		CORBA::Exception *exp = NULL;
		peers[nid]->shutdown_step(shutdown_seqnum, env);
		if ((exp = env.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "COMM_FAILURE exception for nodeid %d "
				    "in shutdown_step, aborting "
				    "tell_leader_to_shutdown() "
				    "(base cluster seqnum %d)\n",
				    name_to_log, nid, base_cluster_seqnum));
				env.clear();
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "unexpected exception %s "
				    "at shutdown_step\n",
				    name_to_log, exp->_name()));
				ASSERT(0);
				//lint -e527
				env.clear();
				return;
				//lint +e527
			}
		}
	}

	cmm::membership_t shutdown_membership;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		shutdown_membership.members[nid] = INCN_UNKNOWN;
	}

	//
	// Put a shutdown task on the threadpool queue.
	// When leader executes the task, it will drive the shutdown
	// reconfiguration that will ensure that the shutdown is complete
	// on all membership managers for this membership.
	//
	membership_reconfig_task *taskp =
	    new membership_reconfig_task(
	    this, shutdown_membership, shutdown_seqnum,
	    base_cluster_membership, base_cluster_seqnum, true);
	membership_threadpool::the().defer_processing(taskp);
}

//
// The "leader" manager of a membership drives a shutdown reconfiguration
// of the membership by executing this method.
//
void
membership_manager_impl::shutdown_reconfiguration(cmm::seqnum_t shutdown_seqnum,
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership)
{
	ASSERT(I_AM_LEADER);

	state_lock.lock();
	shutdown = true;

	latest_reconfig_lock.lock();
	ASSERT(latest_seqnum == shutdown_seqnum);
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		latest_membership.members[nid] = INCN_UNKNOWN;
	}
	latest_reconfig_lock.unlock();

	MEMBERSHIP_TRACE(("Membership Manager (%s) : shutdown reconfiguration "
	    "started (shutdown_seqnum %d)\n", name_to_log, shutdown_seqnum));

	if (!get_peer_refs(node_seqnum, node_membership)) {
		//
		// Failed to get peer refs, or a new base cluster CMM
		// reconfiguration triggered.
		//
		state_lock.unlock();
		return;
	}
	state_lock.unlock();

	//
	// Shutdown is benevolent, in the sense that,
	// say shutdown was aborted due to a base cluster reconfig,
	// and then the shutdown reconfig happens after kernel CMM reconfigures.
	// The leader asks every peer to shutdown. A peer could have already
	// shutdown before the base cluster reconfig triggered
	// or maybe the peer is newly booted up.
	// Even in such cases, shutdown does no harm to the peer.
	//

	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {

		//
		// If a new base cluster reconfiguration has started,
		// then return without further progress.
		//
		if (new_base_cluster_reconfig_started(node_seqnum)) {
			return;
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (node_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		Environment env;
		CORBA::Exception *exp = NULL;
		peers[nid]->finish_shutdown_step(shutdown_seqnum, env);
		if ((exp = env.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "COMM_FAILURE exception for nodeid %d "
				    "at finish_shutdown_step(), "
				    "aborting shutdown_reconfiguration() "
				    "(base cluster seqnum %d)\n",
				    name_to_log, nid, node_seqnum));
				env.clear();
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Manager (%s) : "
				    "unexpected exception %s at "
				    "finish_shutdown_step()\n",
				    name_to_log, exp->_name()));
				ASSERT(0);
				//lint -e527
				env.clear();
				return;
				//lint +e527
			}
		}
	}

	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "shutdown reconfiguration done\n", name_to_log));
}

void
membership_manager_impl::shutdown_step(
    cmm::seqnum_t shutdown_seqnum, Environment &)
{
	MEMBERSHIP_TRACE(("Membership Manager (%s) : shutting down "
	    "(shutdown_seqnum %d)\n", name_to_log, shutdown_seqnum));

	state_lock.lock();

	shutdown = true;

	if (pre_mem_shutdown_flag) {
		pre_mem_shutdown();
	}

	// Allocate event packet to put on to event list
	event_packet_t *ev_packetp = new event_packet_t;
	if (ev_packetp == NULL) {
		//
		// could not allocate small amount of memory;
		// serious memory shortage
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Could not create event packet "
		    "for membership (%s); node is low on memory", name_to_log);
	}

	// Populate the event packet with required information
	ev_packetp->event = MEMBER_SHUTTING_DOWN;
	ev_packetp->seqnum = shutdown_seqnum;
	// Fill in default values for data that is not required
	ev_packetp->latest_incn = INCN_UNKNOWN;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		ev_packetp->membership.members[nid] = INCN_UNKNOWN;
	}

	//
	// Push event packet on to the event list,
	// and signal state machine to shut down.
	//
	event_list.append(ev_packetp);
	state_cv.signal();
	state_lock.unlock();
}

void
membership_manager_impl::finish_shutdown_step(
    cmm::seqnum_t shutdown_seqnum, Environment &)
{
	state_lock.lock();
	ASSERT(seqnum == shutdown_seqnum);

	//
	// We do not finish shutdown until the local membership manager
	// component completely goes down.
	// Once the membership component on the local node goes down fully,
	// the membership engine delivers this 'down' event to the membership
	// managers on the local node. The membership managers deliver
	// a state machine event to the state machine saying that the member
	// component went down.
	//
	// We wait here until the state machine processes all its pending
	// state machine events and enters INITIAL_STATE.
	// Note that since this is a shutdown action in progress,
	// we know the state machine should enter the INITIAL_STATE
	// after the local member component is completely down.
	//
	// So we leave state lock for state machine to do its work
	// and enter INITIAL_STATE.
	//
	while (current_state != INITIAL_STATE) {
		state_cv.wait(&state_lock);
	}

	latest_reconfig_lock.lock();
	latest_incn = INCN_UNKNOWN;
	if (I_AM_LEADER) {
		ASSERT(latest_seqnum == shutdown_seqnum);
	} else {
		latest_seqnum = 0;
	}
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		latest_membership.members[nid] = INCN_UNKNOWN;
	}
	latest_reconfig_lock.unlock();

	// Reset the shutdown flag to mark that shutdown is complete now
	ASSERT(shutdown);
	shutdown = false;

	MEMBERSHIP_TRACE(("Membership Manager (%s) : shutdown completed "
	    "(shutdown_seqnum %d)\n", name_to_log, shutdown_seqnum));
	if (generate_event) {
		//
		// Generate event and syslog message for shutdown
		// Other parameters passed are "don't care" values
		//
		cmm::membership_t old_membership;
		for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
			old_membership.members[nid] = INCN_UNKNOWN;
		}
		publish_events_and_syslog(
		    true, seqnum, old_membership, membership);
	}
	state_lock.unlock();
}

//
// This IDL method is invoked on "leader" to notify the "leader"
// that a peer changed state.
//
void
membership_manager_impl::notify_leader(
    sol::incarnation_num incn,
    cmm::seqnum_t base_cl_seqnum_when_state_changed,
    Environment &e)
{
	ASSERT(I_AM_LEADER);
	MEMBERSHIP_TRACE(("Membership Manager (%s) : leader notified that "
	    "peer on node %d changed state, new incn = %d, "
	    "base cluster seqnum = %d\n", name_to_log,
	    e.get_src_node().ndid, incn, base_cl_seqnum_when_state_changed));

	while (leader_data_collection_underway) {
		os::usecsleep((os::usec_t)1000);	// 1ms
	}

	// If shutdown is in progress, wait till it finishes
	while (1) {
		state_lock.lock();
		if (!shutdown) {
			break;
		}
		state_lock.unlock();
		os::usecsleep((os::usec_t)1000);	// 1ms
	}
	ASSERT(state_lock.lock_held());

	//
	// Check to see if the current base cluster seqnum matches
	// the base cluster seqnum passed in. The base cluster seqnum
	// passed in as argument is when the membership state change
	// event triggered.
	//
	// If the current base cluster seqnum does not match
	// the base cluster seqnum passed in as argument,
	// then it means that after the membership state change event
	// triggered, a base cluster reconfiguration has started.
	// Hence, the leader does not need to process this state
	// change event; the base cluster reconfiguration will in turn
	// trigger reconfigurations for every membership manager
	// and that will include the new incarnation for the state change.
	//
	// If the current base cluster seqnum matches
	// the base cluster seqnum passed in as argument,
	// then the leader knows that a new base cluster reconfiguration
	// has not triggered, and it can safely proceed to schedule
	// a reconfiguration task for the new incarnation due to
	// the state change event triggered.
	// The leader also stores this base cluster seqnum
	// in the membership manager reconfiguration task data.
	//

	// Get the current base cluster membership and seqnum
	cmm::seqnum_t base_cluster_seqnum;
	cmm::membership_t base_cluster_membership;
	latest_node_seqnum_lock.lock();
	base_cluster_seqnum = latest_node_seqnum;
	base_cluster_membership = latest_node_membership;
	latest_node_seqnum_lock.unlock();

	//
	// Compare the current base cluster seqnum with the passed in
	// base cluster seqnum
	//
	ASSERT(base_cl_seqnum_when_state_changed <= base_cluster_seqnum);
	if (base_cl_seqnum_when_state_changed < base_cluster_seqnum) {
		//
		// A new base cluster reconfiguration has started
		// after the state change event triggered.
		// The leader ignores this state change as
		// the base cluster reconfiguration will trigger
		// membership reconfigurations where the leader
		// will anyway query the latest states of peers, and
		// do the reconfiguration for the new incarnation.
		//
		MEMBERSHIP_TRACE(("Membership Manager (%s) : notify_leader() "
		    "returning without scheduling reconfig task, "
		    "as new base cluster reconfig has started, "
		    "base_cl_seqnum arg = %d, current base cl seqnum = %d\n",
		    name_to_log, base_cl_seqnum_when_state_changed,
		    base_cluster_seqnum));
		state_lock.unlock();
		return;
	}

	//
	// Calculate the new membership and seqnum.
	// Update the latest seqnum data.
	//
	latest_reconfig_lock.lock();
	if (latest_membership.members[e.get_src_node().ndid] == incn) {
		//
		// We already know about this incarnation.
		// May be the leader-data-collection made the "leader"
		// aware of this incarnation. Return without progress.
		//
		latest_reconfig_lock.unlock();
		state_lock.unlock();
		return;
	}
	latest_membership.members[e.get_src_node().ndid] = incn;
	cmm::membership_t membership_copy = latest_membership;
	latest_seqnum += 1;
	cmm::seqnum_t seqnum_copy = latest_seqnum;
	latest_reconfig_lock.unlock();

	// Initiate fencing if the membership manager type wants it
	if (initiate_fencing_flag) {
		initiate_fencing(base_cluster_seqnum);
	}

	// Put a task on the threadpool queue
	membership_reconfig_task *taskp =
	    new membership_reconfig_task(
	    this, membership_copy,
	    seqnum_copy, base_cluster_membership,
	    base_cluster_seqnum, false);
	membership_threadpool::the().defer_processing(taskp);

	state_lock.unlock();
}

//
// Apart from the distributed membership data (which is updated
// on the instruction of the "leader"), each peer also maintains
// local data pertaining to the states of the membership components
// present on the local base node.
// "Leader" calls this IDL method on each peer to query this local
// data about the states of membership components.
//
void
membership_manager_impl::query_latest_state(
    cmm::seqnum_t &query_seqnum,
    sol::incarnation_num &query_incn, Environment &)
{
	query_incn = latest_incn;
	state_lock.lock();
	query_seqnum = seqnum;
	state_lock.unlock();

	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "query_latest_state : incn %d, seqnum %d\n",
	    name_to_log, query_incn, query_seqnum));
}

//
// The engine uses this IDL method to notify a membership manager
// on the local base node about a membership change event.
// The local membership manager then notifies the "leader" about it.
//
void
membership_manager_impl::local_membership_change_event(
    const char *event_namep, cmm::seqnum_t base_cl_seqnum)
{
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "received event '%s' from engine, base cluster seqnum = %d\n",
	    name_to_log, event_namep, base_cl_seqnum));

	membership_manager_impl::handler_for_event_action_t funcp = NULL;
	for (unsigned int index = 0; index < NUM_ACTIONS; index++) {
		if (os::strlen(event_actions[index].event_name) == 0) {
			continue;
		}
		if (os::strcmp(event_actions[index].event_name,
		    event_namep) == 0) {
			funcp = event_actions[index].funcp;
			break;
		}
	}
	ASSERT(funcp);

	// Invoke handler to do appropriate action based on the event received
	(this->*funcp)(base_cl_seqnum);
}

void
membership_manager_impl::base_cluster_membership_change(
    cmm::seqnum_t base_cl_seqnum)
{
	if (!I_AM_LEADER) {
		return;
	}

	cmm::membership_t base_cl_membership;
	latest_node_seqnum_lock.lock();
	if (base_cl_seqnum != latest_node_seqnum) {
		MEMBERSHIP_TRACE((
		    "Membership Manager (%s) : base_cluster_membership_change()"
		    " returning without processing base cl seqnum %d, "
		    "current base cl seqnum = %d\n", name_to_log,
		    base_cl_seqnum, latest_node_seqnum));
		latest_node_seqnum_lock.unlock();
		return;
	}
	base_cl_membership = latest_node_membership;
	latest_node_seqnum_lock.unlock();

	MEMBERSHIP_TRACE(("Membership Manager (%s) : leader notified that "
	    "Kernel CMM has reconfigured, base cluster seqnum %d\n",
	    name_to_log, base_cl_seqnum));

	// Initiate fencing if the membership manager type wants it
	if (initiate_fencing_flag) {
		initiate_fencing(base_cl_seqnum);
	}

	// Put a task on the threadpool queue
	leader_data_collection_underway = true;
	leader_data_collection_task *taskp =
	    new leader_data_collection_task(
	    this, base_cl_seqnum, base_cl_membership);
	membership_threadpool::the().defer_processing(taskp);
}

void
membership_manager_impl::member_came_up(cmm::seqnum_t base_cl_seqnum)
{
	if (first_boot) {
		first_boot = false;
	}

	//
	// Normally, a member component should come up and go down
	// in succession, repeated over. There should not be consecutive
	// 'coming-up' events. If such things are seen, then we should
	// ignore them. This is because notifications about a local
	// member component coming up are sent by the local member
	// component itself, and consecutive 'coming-up' events
	// hint at wrong behaviour from the local member components.
	// Syslog a message and trace a message into the trace buffer,
	// and ignore the notification.
	//
	if (latest_incn != INCN_UNKNOWN) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "Consecutive 'coming-up' notifications received\n",
		    name_to_log));
		//
		// SCMSGS
		// @explanation
		// The membership subsystem received consecutive notifications
		// saying that a local member component came up.
		// Normally, a notification about the local member component
		// going down should arrive between two notifications that say
		// that the local member component came up.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership : Consecutive 'coming-up' notifications "
		    "received by membership (%s).", name_to_log);

		// Ignore this notification
		return;
	}

	//
	// A member could be coming up, while the membership
	// is shutting down. In such a case, do not allow the member
	// to come up in membership until the shutdown finishes.
	// Note that since this method invocation on the engine
	// is synchronous, so the entity doing this invocation
	// will not progress till this invocation finishes.
	//
	while (1) {
		state_lock.lock();
		bool shutdown_flag = shutdown;
		state_lock.unlock();
		if (!shutdown_flag) {
			break;
		}
		os::usecsleep((os::usec_t)1000);	// 1ms
	}

	//
	// The membership component has come up.
	// Give a new incarnation number.
	//
	timespec_t tv;
	gethrestime(&tv);

	// Calculate local incarnation number, based on gethrestime
	if (tv.tv_sec == INCN_UNKNOWN) {
		tv.tv_sec = 1;
	}

	state_lock.lock();
	latest_incn = (sol::incarnation_num)tv.tv_sec;
	ASSERT(latest_incn != INCN_UNKNOWN);

	// Allocate event packet to put on to event list
	event_packet_t *ev_packetp = new event_packet_t;
	if (ev_packetp == NULL) {
		//
		// could not allocate small amount of memory;
		// serious memory shortage
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Could not create event packet "
		    "for membership (%s); node is low on memory", name_to_log);
	}

	// Populate the event packet with required information
	ev_packetp->event = MEMBER_COMES_UP;
	ev_packetp->latest_incn = latest_incn;
	// Fill in default values for data that is not required
	ev_packetp->seqnum = 0;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		ev_packetp->membership.members[nid] = INCN_UNKNOWN;
	}

	// Push event packet on to the event list, and signal state machine
	event_list.append(ev_packetp);
	state_cv.signal();
	state_lock.unlock();

	//
	// Check to see if a new base cluster reconfiguration has started.
	// If so, ignore the state change.
	// We have marked the new incarnation above.
	// The new base cluster reconfiguration	will trigger new membership
	// reconfigurations which will take care of the state change event.
	//
	latest_node_seqnum_lock.lock();
	ASSERT(base_cl_seqnum <= latest_node_seqnum);
	if (base_cl_seqnum != latest_node_seqnum) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "member_came_up() returning prematurely "
		    "as new base cluster reconfig has started, "
		    "base_cl_seqnum arg = %d, current base cl seqnum = %d\n",
		    name_to_log, base_cl_seqnum, latest_node_seqnum));
		latest_node_seqnum_lock.unlock();
		return;
	}
	latest_node_seqnum_lock.unlock();

	member_changed_state(latest_incn, base_cl_seqnum);
}

void
membership_manager_impl::member_went_down(cmm::seqnum_t base_cl_seqnum)
{
	//
	// Normally, a member component should come up and go down
	// in succession, repeated over.
	// There should not be consecutive 'going-down' events.
	// If such things are seen, then we should ignore them.
	// Syslog a message and trace a message into the trace buffer,
	// and ignore the notification.
	//
	// There is one scenario where we could get a 'down' notification
	// for a local member component that is already 'down'.
	// When a member component is ready to be part of the membership
	// (eg, zone has completely booted up, or process has completed
	// initialization and is ready), then the member component tells
	// the membership subsystem that it has come up.
	// If we reach here, it could mean the member component died
	// before it could reach the point where it tells
	// membership subsystem that it has come up.
	// So, we can ignore this notification.
	//
	if (latest_incn == INCN_UNKNOWN) {

		//
		// A special action for zone cluster membership.
		// When the zone boots up for the first time after installation,
		// Solaris reboots the zone during sysid configuration, before
		// the zone is all the way booted up. So, the first time a zone
		// cluster zone boots up, the zone cluster membership will get
		// a 'down' notification although the zone is already down.
		// We do not trace or syslog warning messages this first time
		// for zone cluster membership manager.
		// Next time onwards, we log messages if we get a 'down'
		// notification for an already down membership component.
		//
		if (type == membership::ZONE_CLUSTER) {
			if (first_boot) {
				first_boot = false;
				return;
			}
		} else {
			// Not used for other membership types
			first_boot = false;
		}

		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "Got a 'down' notification for member component "
		    "is already down\n", name_to_log));
		//
		// SCMSGS
		// @explanation
		// The membership subsystem received a notification
		// saying that a local member component went down.
		// But the membership subsystem already knew that
		// the local member component is down.
		// Normally, a notification about the local member component
		// coming up should arrive before a notification that says
		// that the local member component went down.
		// (1) Either the local member component went down before
		// it could tell membership subsystem that it has come up, or
		// (2) the membership code has a bug.
		// @user_action
		// Contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership : Received a 'down' notification "
		    "for local member component of membership (%s), "
		    "that was already down.", name_to_log);

		// Ignore this notification
		return;
	}
	ASSERT(!first_boot);

	// XXX
	// XXX The cluster shutdown intent flag in CMM is only set
	// XXX when the base cluster or base node is brought
	// XXX down using scshutdown. Even in cases of reboot or other
	// XXX unsupported methods of bringing a base node down,
	// XXX we should not tell leader about the member component going
	// XXX down. The base cluster reconfiguration after the
	// XXX base node is brought down, should trigger another
	// XXX zone cluster reconfiguration afterwards.
	// XXX

	//
	// If the cluster is shutting down,
	// do not tell leader or trigger another reconfiguration.
	//
	if (cmm_impl::the().get_cluster_shutdown_intent()) {
		return;
	}

	state_lock.lock();
	latest_incn = INCN_UNKNOWN;

	// Allocate event packet to put on to event list
	event_packet_t *ev_packetp = new event_packet_t;
	if (ev_packetp == NULL) {
		//
		// could not allocate small amount of memory;
		// serious memory shortage
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Could not create event packet "
		    "for membership (%s); node is low on memory", name_to_log);
	}

	// Populate the event packet with required information
	ev_packetp->event = MEMBER_GOES_DOWN;
	ev_packetp->latest_incn = latest_incn;
	// Fill in default values for data that is not required
	ev_packetp->seqnum = 0;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		ev_packetp->membership.members[nid] = INCN_UNKNOWN;
	}

	// Push event packet on to the event list, and signal state machine
	event_list.append(ev_packetp);
	state_cv.signal();
	state_lock.unlock();

	//
	// Check to see if a new base cluster reconfiguration has started.
	// If so, ignore the state change.
	// We have marked the new incarnation above.
	// The base cluster reconfiguration will trigger new membership
	// reconfigurations which will take care of the state change event.
	//
	latest_node_seqnum_lock.lock();
	ASSERT(base_cl_seqnum <= latest_node_seqnum);
	if (base_cl_seqnum != latest_node_seqnum) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "member_went_down() returning prematurely "
		    "as new base cluster reconfig has started, "
		    "base_cl_seqnum arg = %d, current base cl seqnum = %d\n",
		    name_to_log, base_cl_seqnum, latest_node_seqnum));
		latest_node_seqnum_lock.unlock();
		return;
	}
	latest_node_seqnum_lock.unlock();

	//
	// If a membership component is going down because
	// the membership manager is shutting down,
	// then do not tell leader or trigger another reconfiguration.
	//
	state_lock.lock();
	if (shutdown) {
		//
		// Membership component went down as
		// membership manager was being shut down.
		//
		state_lock.unlock();
		return;
	}
	state_lock.unlock();

	member_changed_state(latest_incn, base_cl_seqnum);
}

void
membership_manager_impl::member_changed_state(
    sol::incarnation_num incn, cmm::seqnum_t base_cl_seqnum)
{
	MEMBERSHIP_TRACE(("Membership Manager (%s) : telling leader that "
	    "peer on node %d changed state, base cluster seqnum = %d\n",
	    name_to_log, orb_conf::local_nodeid(), base_cl_seqnum));

	//
	// Check to see if a new base cluster reconfiguration has started.
	// If so, ignore the state change. The base cluster reconfiguration
	// will trigger new membership reconfigurations which will take care
	// of the state change event.
	//
	latest_node_seqnum_lock.lock();
	ASSERT(base_cl_seqnum <= latest_node_seqnum);
	if (base_cl_seqnum != latest_node_seqnum) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "member_changed_state() returning without notifying "
		    "leader, as new base cluster reconfig has started, "
		    "base_cl_seqnum arg = %d, current base cl seqnum = %d\n",
		    name_to_log, base_cl_seqnum, latest_node_seqnum));
		latest_node_seqnum_lock.unlock();
		return;
	}
	latest_node_seqnum_lock.unlock();

	//
	// This call could get held up in notify leader,
	// if the leader data collection is happening.
	// It will wait until the leader-data-collection reconfig is done.
	// Note that since this method invocation on the engine
	// is synchronous, so the entity doing this invocation
	// will not progress till this invocation finishes.
	//
	membership::manager_var leader_v = membership::manager::_nil();
	leader_v = cmm_ns::get_membership_manager_ref_zc(
		    cluster_name, mem_manager_name, leader_cmm);
	while (CORBA::is_nil(leader_v)) {
		os::usecsleep((long)1000);	// 1ms
		leader_v = cmm_ns::get_membership_manager_ref_zc(
		    cluster_name, mem_manager_name, leader_cmm);
	}
	Environment e;
	leader_v->notify_leader(incn, base_cl_seqnum, e);
	CORBA::Exception *exp = e.exception();
	if (exp != NULL) {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			MEMBERSHIP_TRACE((
			    "Membership Manager (%s) : COMM_FAILURE exception "
			    "while notifying leader nodeid %d\n",
			    name_to_log, leader_cmm));
			e.clear();
			return;
		} else {
			// Some other exception
			MEMBERSHIP_TRACE(("Membership Manager (%s) : "
			    "unexpected exception %s while notifying leader\n",
			    name_to_log, exp->_name()));
			ASSERT(0);
			//lint -e527
			e.clear();
			return;
			//lint +e527
		}
	}
}

void
membership_manager_impl::initiate_fencing(cmm::seqnum_t)
{
	// Should not happen
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "base class initiate fencing\n", name_to_log));
	ASSERT(0);
}

void
membership_manager_impl::pre_mem_update(const cmm::membership_t &,
    cmm::seqnum_t, cmm::seqnum_t)
{
	// Should not happen
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "base class pre mem update\n", name_to_log));
	ASSERT(0);
}

void
membership_manager_impl::pre_mem_shutdown()
{
	// Should not happen
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "base class pre shutdown\n", name_to_log));
	ASSERT(0);
}

// Returns -1 if zone does not exist, else returns the zone id
sol::zoneid_t
zonename_to_zoneid(const char *zone_namep)
{
	zoneid_t target_zone_id;

	zone_t *zone_refp = zone_find_by_name((char*)zone_namep);
	if (zone_refp == NULL) {
		return (-1);
	}

	target_zone_id = zone_refp->zone_id;
	zone_rele(zone_refp);
	return (target_zone_id);
}

//
// Get a snapshot of membership and seqnum from membership manager
//
void
membership_manager_impl::get_membership(
    cmm::membership_t &membership_snapshot,
    cmm::seqnum_t &seqnum_snapshot, Environment &)
{
	state_lock.lock();
	membership_snapshot = membership;
	seqnum_snapshot = seqnum;
	state_lock.unlock();
}

//
// Adds a client to the membership clients list of this membership manager.
//
void
membership_manager_impl::register_client(
    membership::client_ptr client_p, Environment &e)
{
	membership::client_ptr new_client_p;

	if (CORBA::is_nil(client_p)) {
		e.exception(new membership::api_usage_error());
	}

	membership_clients_lock.lock();
	SList<membership::client>::ListIterator iter(membership_clients);
	for (membership::client_ptr iter_client_p = membership::client::_nil();
	    (iter_client_p = iter.get_current()) != NULL; iter.advance()) {
		if (client_p->_equiv(iter_client_p)) {
			// Already registered; throw exception
			e.exception(new membership::already_registered());
			membership_clients_lock.unlock();
			return;
		}
	}

	new_client_p = membership::client::_duplicate(client_p);
	membership_clients.append(new_client_p);
	membership_clients_lock.unlock();
}

//
// Removes a client from the membership clients list of this membership manager.
//
void
membership_manager_impl::unregister_client(
    membership::client_ptr client_p, Environment &e)
{
	membership::client_ptr del_client_p;
	bool found = false;

	if (CORBA::is_nil(client_p)) {
		e.exception(new membership::api_usage_error());
	}

	membership_clients_lock.lock();
	SList<membership::client>::ListIterator iter(membership_clients);
	for (; (del_client_p = iter.get_current()) != NULL; iter.advance()) {
		if (client_p->_equiv(del_client_p)) {
			// Match found; remove it
			membership_clients.erase(del_client_p);
			CORBA::release(del_client_p);
			found = true;
			break;
		}
	}

	membership_clients_lock.unlock();
	if (!found) {
		e.exception(new membership::no_such_client());
	}
}

//
// The membership state machine thread : states and state transitions
// ------------------------------------------------------------------
// There exists a state machine thread for each membership manager.
// When the membership manager reconfigures along with its peers,
// this reconfiguration generates various membership state events
// and the state machine thread transitions to different states
// according to these events.
//
// Description of states
// ---------------------
// (1) INVALID_STATE_DOWN :
//	# This is the invalid state where local member component is down.
//	# All other state variables are undefined (no guaranteed values).
//	# Occurence of MEMBER_INVALID_EVENT makes the state machine
//	enter this state if the local member component is down,
//	no matter what the previous state was.
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_COMES_UP		INVALID_STATE_UP
//	  MEMBER_SHUTTING_DOWN		INITIAL_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_DOWN
//
// (2) INVALID_STATE_UP :
//	# This is the invalid state where local member component is up.
//	# All other state variables are undefined (no guaranteed values).
//	# Occurence of MEMBER_INVALID_EVENT makes the state machine
//	enter this state if the local member component is up,
//	no matter what the previous state was.
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_GOES_DOWN		INVALID_STATE_DOWN
//	  MEMBER_SHUTTING_DOWN		SHUTTING_DOWN_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_UP
//
// (3) INITIAL_STATE :
//	# The state machine thread starts out in this state.
//	After membership shutdown, the state machine enters this state as well.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is INCN_UNKNOWN;
//		in other words, local member component (zone/process) is down.
//		(ii) official membership (as reconfigured by leader)
//		is empty (all incarnations are INCN_UNKNOWN)
//		(iii) there is no membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_COMES_UP		UP_TRANSIT_STATE
//	  MEMBER_NOT_IN_LIST		NOT_MEMBER_STATE
//	  MEMBER_DOWN_PENDING		DOWN_PENDING_STATE
//	  MEMBER_SHUTTING_DOWN		INITIAL_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_DOWN
//
// (4) UP_TRANSIT_STATE :
//	# The membership component (zone or process) on the local base node,
//	corresponding to the state machine, has come up, and is ready to be
//	part of the membership. The state machine is waiting for the leader
//	to reconfigure a membership, that includes the new 'up' incarnation
//	of the local membership component.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is not INCN_UNKNOWN;
//		in other words, local member component (zone/process) is up.
//		(ii) there is no membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_IN_LIST		MEMBER_STATE
//	  MEMBER_NOT_IN_LIST		UP_PENDING_STATE
//	  MEMBER_GOES_DOWN		INITIAL_STATE
//	  MEMBER_SHUTTING_DOWN		SHUTTING_DOWN_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_UP
//
// (5) UP_PENDING_STATE :
//	# While waiting for the local membership component's newest
//	'up' incarnation to be part of the official membership reconfigured by
//	the leader membership manager, the state machine thread receives
//	a reconfiguration from the leader in which the local membership
//	component's incarnation is marked down. Hence the state machine
//	considers that the membership reconfiguration which will include
//	the new 'up' incarnation is 'pending'.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is not INCN_UNKNOWN;
//		in other words, local member component (zone/process) is up.
//		(ii) the local member component is marked down
//		in the official membership reconfigured
//		by the leader membership manager.
//		(iii) we have received membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_OBSOLETE_IN_LIST	UP_OBSOLETE_STATE
//	  MEMBER_NOT_IN_LIST		UP_PENDING_STATE
//	  MEMBER_IN_LIST		MEMBER_STATE
//	  MEMBER_GOES_DOWN		NOT_MEMBER_STATE
//	  MEMBER_SHUTTING_DOWN		SHUTTING_DOWN_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_UP
//
// (6) UP_OBSOLETE_STATE :
//	# While waiting for the local membership component's newest
//	'up' incarnation (say INCN) to be part of the official membership
//	reconfigured by the leader membership manager, the state machine thread
//	receives a reconfiguration from the leader in which the local membership
//	component's incarnation is marked 'up' with an incarnation
//	different from INCN.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is not INCN_UNKNOWN;
//		in other words, local member component (zone/process) is up.
//		(ii) the local member component is marked up
//		in the official membership reconfigured by
//		the leader membership manager, but with an incarnation that
//		is different from the latest 'up' incarnation.
//		(iii) we have received membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_OBSOLETE_IN_LIST	UP_OBSOLETE_STATE
//	  MEMBER_NOT_IN_LIST		UP_PENDING_STATE
//	  MEMBER_IN_LIST		MEMBER_STATE
//	  MEMBER_GOES_DOWN		DOWN_PENDING_STATE
//	  MEMBER_SHUTTING_DOWN		SHUTTING_DOWN_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_UP
//
// (7) DOWN_PENDING_STATE :
//	# The membership component (zone or process) on the local base node,
//	corresponding to the state machine, has gone down. The state machine
//	is waiting for the leader to reconfigure a membership, in which
//	the local membership component's incarnation is marked INCN_UNKNOWN.
//	In the latest known official membership reconfigured by leader,
//	the local member component is still marked 'up' (not INCN_UNKNOWN).
//	Hence, the reconfiguration to mark the local member component 'down'
//	in the official membership is still 'pending'.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is INCN_UNKNOWN;
//		in other words, local member component (zone/process) is down.
//		(ii) the local member component is marked 'up' in the official
//		membership reconfigured by the leader membership manager.
//		(iii) we have received membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_NOT_IN_LIST		NOT_MEMBER_STATE
//	  MEMBER_DOWN_PENDING		DOWN_PENDING_STATE
//	  MEMBER_COMES_UP		UP_OBSOLETE_STATE
//	  MEMBER_SHUTTING_DOWN		INITIAL_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_DOWN
//
// (8) MEMBER_STATE :
//	# The local membership component is 'up' and it is marked 'up'
//	with the same 'up' incarnation in the official membership
//	reconfigured by the leader membership manager.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is not INCN_UNKNOWN;
//		in other words, local member component (zone/process) is up.
//		(ii) the local member component is marked 'up' in the official
//		membership reconfigured by the leader membership manager,
//		with an incarnation same as the latest 'up' incarnation
//		known locally to state machine.
//		(iii) we have received membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_IN_LIST		MEMBER_STATE
//	  MEMBER_GOES_DOWN		DOWN_PENDING_STATE
//	  MEMBER_SHUTTING_DOWN		SHUTTING_DOWN_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_UP
//
// (9) NOT_MEMBER_STATE :
//	# The local membership component is 'down' and it is marked 'down'
//	in the official membership reconfigured by the leader.
//	# Values of properties in this state :
//		(i) the latest incarnation of the local member component
//		(as known by the state machine) is INCN_UNKNOWN;
//		in other words, local member component (zone/process) is down.
//		(ii) the local member component is marked 'down' in the official
//		membership reconfigured by the leader membership manager.
//		(iii) we have received membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_NOT_IN_LIST		NOT_MEMBER_STATE
//	  MEMBER_DOWN_PENDING		DOWN_PENDING_STATE
//	  MEMBER_COMES_UP		UP_PENDING_STATE
//	  MEMBER_SHUTTING_DOWN		INITIAL_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_DOWN
//
// (10) SHUTTING_DOWN_STATE :
//	# The membership is being shutdown; shutdown process has started
//	  for this membership.
//	# Values of properties in this state :
//		(i) the shut down process is in progress; the local member
//		component has not gone down completely, and so its latest
//		incarnation locally is not INCN_UNKNOWN.
//		(ii) the shutdown flag is true; when a shutdown is in progress
//		the shutdown flag is marked throughout the shut down process
//		and no new reconfiguration or state change is entertained
//		by the membership subsystem for this membership, until
//		the shutdown completes.
//		(iii) we received membership information from the leader
//	# Transitions after entering this state :
//	  Possible events		Next state
//	  ---------------		-----------
//	  MEMBER_GOES_DOWN		INITIAL_STATE
//	  MEMBER_REMOVED		REMOVE_STATE
//	  Any other event		INVALID_STATE_UP
//
// (11) REMOVE_STATE :
//	# This is the final state, the membership is being removed completely.
//	  For example, the state machine for a zone cluster membership
//	  enters this state when the zone cluster is being removed.
//
//
// Why does every state have a valid transition to REMOVE_STATE
// on MEMBER_REMOVED event?
// -------------------------------------------------------------
//
// Normally, a member component will first be marked 'down'
// in any membership before the membership gets deleted.
// This would mean that the membership state machine should
// have a valid transition to REMOVE_STATE only from :
// - either INITIAL_STATE or NOT_MEMBER_STATE (completely down states)
// - or from one of INVALID_STATE_UP or INVALID_STATE_DOWN
// to allow removal of a membership whose state machine has entered
// an invalid state.
//
// However, proxy rgmds for zone clusters do not conform with the above model.
// For zone clusters that are not hosted on all base cluster nodes,
// proxy rgmds run on base cluster nodes that do not host the zone.
// These proxy rgmds are marked 'up' in rgmd process membership
// for such a zone cluster.
// The idea of process-up-zone-down membership is to mark
// an incarnation 'down' when the corresponding zone dies.
// Since there is no zone associated with the proxy rgmd
// on the same node, the incarnation of proxy rgmd in rgmd process
// membership is never marked 'down', after it has been marked 'up'
// once (unless the base node having the proxy rgmd goes down).
// As a result of this strange scenario for proxy rgmds in
// rgmd process membership, the proxy rgmds are still marked 'up'
// in process membership after the entire zone cluster has halted.
//
// When an attempt is made to delete such a zone cluster,
// membership state machine for rgmd process membership receives
// a MEMBER_REMOVED event to delete itself, according to design.
// This membership state machine on a proxy rgmd node could be
// in a state different from either of the states mentioned above
// (INITIAL_STATE, NOT_MEMBER_STATE, INVALID_STATE_UP,
// INVALID_STATE_DOWN) since it is not marked 'down' and has not
// done an invalid transition yet.
// Now, if the state machine does not have a valid transition
// from any other state on receiving MEMBER_REMOVED event,
// then the state machine will enter an invalid state,
// thereby causing problems.
//
// The state machine is generic for all kinds of membership.
// Due to the special scenario of process membership mentioned above,
// we allow the state machine to have a valid transition
// from any previous state to the REMOVE_STATE on receiving
// MEMBER_REMOVED event.
//
// This additional transition is solely for the scenario of
// process membership mentioned above:
// the scenario being that the nodeid is not configured as part
// of zone cluster, but the process membership state machine is
// in one of the 'up' states and is processing a MEMBER_REMOVED event.
//
// Ideally, we should do a sanity check to allow such a transition
// only in this special case, and not in any other membership case.
// However, notice that membership subsystem receives the notification
// of zone cluster deletion from clconf, only after
// the zone cluster configuration has been cleaned up from CCR/clconf.
// Hence when the membership state machine receives a MEMBER_REMOVED event,
// it cannot query CCR/clconf to check whether or not the nodeid is configured
// to host a zone for the zone cluster; thus, the sanity check
// cannot be done to ensure that the special added transition is done
// only for a 'proxy rgmd' type of scenario.
// -----------------------------------------------------------------------
//
//
// The reconfiguration of a membership manager signals the associated state
// machine thread that some membership state change event has happened and that
// the state machine thread should make the appropriate state transition.
// However, since this "signalling" is done by the use of condition variables,
// there is no guarantee that the state machine thread will be scheduled
// after every event to be able to process the state transition.
// Hence, we store membership state change events in an event list (queue).
// The state machine processes events by reaping them off the head of the list.
// It is possible that there is a backlog of events on the event list
// to be processed by the state machine. Once the state machine wakes up and
// gets scheduled, it processes all the events on the event list one after
// another until no events remain on the event list. Then the state machine
// sleeps until another event happens.
//

//
// State transition table for state machine thread
//
static membership_state_machine_state_t
    state_transition_table[SHUTTING_DOWN_STATE + 1][MEMBER_REMOVED + 1];

static void
membership_manager_impl::init_state_transition_table()
{
	//
	// Initialize entire table with one of the invalid states.
	// If the local member component is up in a state,
	// then fill the state's row with INVALID_STATE_UP.
	// If the local member component is down in a state,
	// then fill the state's row with INVALID_STATE_DOWN.
	//
	for (unsigned int event = 0; event <= MEMBER_REMOVED; event++) {
		state_transition_table[INVALID_STATE_DOWN][event]
		    = INVALID_STATE_DOWN;
		state_transition_table[INVALID_STATE_UP][event]
		    = INVALID_STATE_UP;
		state_transition_table[INITIAL_STATE][event]
		    = INVALID_STATE_DOWN;
		state_transition_table[UP_TRANSIT_STATE][event]
		    = INVALID_STATE_UP;
		state_transition_table[UP_PENDING_STATE][event]
		    = INVALID_STATE_UP;
		state_transition_table[UP_OBSOLETE_STATE][event]
		    = INVALID_STATE_UP;
		state_transition_table[DOWN_PENDING_STATE][event]
		    = INVALID_STATE_DOWN;
		state_transition_table[MEMBER_STATE][event]
		    = INVALID_STATE_UP;
		state_transition_table[NOT_MEMBER_STATE][event]
		    = INVALID_STATE_DOWN;
		state_transition_table[SHUTTING_DOWN_STATE][event]
		    = INVALID_STATE_UP;
	}

	// Transitions from INVALID_STATE_DOWN
	state_transition_table[INVALID_STATE_DOWN][MEMBER_COMES_UP]
	    = INVALID_STATE_UP;
	state_transition_table[INVALID_STATE_DOWN][MEMBER_SHUTTING_DOWN]
	    = INITIAL_STATE;
	state_transition_table[INVALID_STATE_DOWN][MEMBER_REMOVED]
	    = REMOVE_STATE;

	// Transitions from INVALID_STATE_UP
	state_transition_table[INVALID_STATE_UP][MEMBER_GOES_DOWN]
	    = INVALID_STATE_DOWN;
	state_transition_table[INVALID_STATE_UP][MEMBER_SHUTTING_DOWN]
	    = SHUTTING_DOWN_STATE;
	state_transition_table[INVALID_STATE_UP][MEMBER_REMOVED]
	    = REMOVE_STATE;

	// Transitions from INITIAL_STATE
	state_transition_table[INITIAL_STATE][MEMBER_COMES_UP]
	    = UP_TRANSIT_STATE;
	state_transition_table[INITIAL_STATE][MEMBER_NOT_IN_LIST]
	    = NOT_MEMBER_STATE;
	state_transition_table[INITIAL_STATE][MEMBER_DOWN_PENDING]
	    = DOWN_PENDING_STATE;
	state_transition_table[INITIAL_STATE][MEMBER_SHUTTING_DOWN]
	    = INITIAL_STATE;
	state_transition_table[INITIAL_STATE][MEMBER_REMOVED] = REMOVE_STATE;

	// Transitions from UP_TRANSIT_STATE
	state_transition_table[UP_TRANSIT_STATE][MEMBER_IN_LIST] = MEMBER_STATE;
	state_transition_table[UP_TRANSIT_STATE][MEMBER_NOT_IN_LIST]
	    = UP_PENDING_STATE;
	state_transition_table[UP_TRANSIT_STATE][MEMBER_GOES_DOWN]
	    = INITIAL_STATE;
	state_transition_table[UP_TRANSIT_STATE][MEMBER_SHUTTING_DOWN]
	    = SHUTTING_DOWN_STATE;
	state_transition_table[UP_TRANSIT_STATE][MEMBER_REMOVED] = REMOVE_STATE;

	// Transitions from UP_PENDING_STATE
	state_transition_table[UP_PENDING_STATE][MEMBER_OBSOLETE_IN_LIST]
	    = UP_OBSOLETE_STATE;
	state_transition_table[UP_PENDING_STATE][MEMBER_NOT_IN_LIST]
	    = UP_PENDING_STATE;
	state_transition_table[UP_PENDING_STATE][MEMBER_IN_LIST]
	    = MEMBER_STATE;
	state_transition_table[UP_PENDING_STATE][MEMBER_GOES_DOWN]
	    = NOT_MEMBER_STATE;
	state_transition_table[UP_PENDING_STATE][MEMBER_SHUTTING_DOWN]
	    = SHUTTING_DOWN_STATE;
	state_transition_table[UP_PENDING_STATE][MEMBER_REMOVED] = REMOVE_STATE;

	// Transitions from UP_OBSOLETE_STATE
	state_transition_table[UP_OBSOLETE_STATE][MEMBER_OBSOLETE_IN_LIST]
	    = UP_OBSOLETE_STATE;
	state_transition_table[UP_OBSOLETE_STATE][MEMBER_NOT_IN_LIST]
	    = UP_PENDING_STATE;
	state_transition_table[UP_OBSOLETE_STATE][MEMBER_IN_LIST]
	    = MEMBER_STATE;
	state_transition_table[UP_OBSOLETE_STATE][MEMBER_GOES_DOWN]
	    = DOWN_PENDING_STATE;
	state_transition_table[UP_OBSOLETE_STATE][MEMBER_SHUTTING_DOWN]
	    = SHUTTING_DOWN_STATE;
	state_transition_table[UP_OBSOLETE_STATE][MEMBER_REMOVED]
	    = REMOVE_STATE;

	// Transitions from DOWN_PENDING_STATE
	state_transition_table[DOWN_PENDING_STATE][MEMBER_NOT_IN_LIST]
	    = NOT_MEMBER_STATE;
	state_transition_table[DOWN_PENDING_STATE][MEMBER_DOWN_PENDING]
	    = DOWN_PENDING_STATE;
	state_transition_table[DOWN_PENDING_STATE][MEMBER_COMES_UP]
	    = UP_OBSOLETE_STATE;
	state_transition_table[DOWN_PENDING_STATE][MEMBER_SHUTTING_DOWN]
	    = INITIAL_STATE;
	state_transition_table[DOWN_PENDING_STATE][MEMBER_REMOVED]
	    = REMOVE_STATE;

	// Transitions from MEMBER_STATE
	state_transition_table[MEMBER_STATE][MEMBER_IN_LIST] = MEMBER_STATE;
	state_transition_table[MEMBER_STATE][MEMBER_GOES_DOWN]
	    = DOWN_PENDING_STATE;
	state_transition_table[MEMBER_STATE][MEMBER_SHUTTING_DOWN]
	    = SHUTTING_DOWN_STATE;
	state_transition_table[MEMBER_STATE][MEMBER_REMOVED] = REMOVE_STATE;

	// Transitions from NOT_MEMBER_STATE
	state_transition_table[NOT_MEMBER_STATE][MEMBER_NOT_IN_LIST]
	    = NOT_MEMBER_STATE;
	state_transition_table[NOT_MEMBER_STATE][MEMBER_DOWN_PENDING]
	    = DOWN_PENDING_STATE;
	state_transition_table[NOT_MEMBER_STATE][MEMBER_COMES_UP]
	    = UP_PENDING_STATE;
	state_transition_table[NOT_MEMBER_STATE][MEMBER_SHUTTING_DOWN]
	    = INITIAL_STATE;
	state_transition_table[NOT_MEMBER_STATE][MEMBER_REMOVED]
	    = REMOVE_STATE;

	// Transitions from SHUTTING_DOWN_STATE
	state_transition_table[SHUTTING_DOWN_STATE][MEMBER_GOES_DOWN]
	    = INITIAL_STATE;
	state_transition_table[SHUTTING_DOWN_STATE][MEMBER_REMOVED]
	    = REMOVE_STATE;
}

// static
void
membership_manager_impl::start_state_machine_thread(void *argp)
{
	((membership_manager_impl *)argp)->state_machine_thread();
	// Thread exiting
}

void
membership_manager_impl::state_machine_thread(void)
{
	while (1) {
		state_lock.lock();

		// The previous state for INITIAL_STATE, the first time around
		membership_state_machine_state_t previous_state
		    = INVALID_STATE_DOWN;

		// Beginning state of state machine
		current_state = INITIAL_STATE;

		// Dummy previous event to pass to INITIAL_STATE, the first time
		membership_state_machine_event_t previous_event =
		    MEMBER_INVALID_EVENT;

		while (current_state != REMOVE_STATE) {

			//
			// Trace a message to print the current state.
			// Call the state handler that represents
			// the current state and perform state validation.
			//
			MEMBERSHIP_TRACE((
			    "Membership Manager (%s) : state %s\n", name_to_log,
			    membership_state_machine_state_names[
			    current_state]));
			membership_manager_impl::state_handler_t
			    state_handler = handlers[current_state];
			(this->*state_handler)(previous_state, previous_event);

			// Wait for the next event
			event_packet_t *next_ev_packetp =
			    get_next_event_or_wait();

			//
			// A new event occurred.
			// Process the event packet.
			// Processing the event packet will update
			// the state machine's state variables based on
			// the event packet data;
			// it will also return the next state based on
			// the current state and the event that occurred.
			//
			membership_state_machine_state_t next_state =
			    event_processing(next_ev_packetp);

			// Set up variables to be used the next time in the loop
			previous_state = current_state;
			current_state = next_state;
			previous_event = next_ev_packetp->event;

			//
			// We have processed the event packet;
			// free up its memory
			//
			delete next_ev_packetp;
		}

		// Membership manager is being deleted
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "state machine removed\n", name_to_log));
		state_lock.unlock();
		break;
	}
}

//
// Process the event information from the event packet.
// This will update the state machine's state variables based on
// the event packet data; it will also return the next state based on
// the current state and the event that occured.
//
membership_state_machine_state_t
membership_manager_impl::event_processing(const event_packet_t *ev_packetp)
{
	ASSERT(state_lock.lock_held());
	ASSERT(ev_packetp != NULL);

	switch (ev_packetp->event) {
	case MEMBER_INVALID_EVENT:
		//
		// The state machine will enter one of the invalid states -
		// either INVALID_STATE_UP or INVALID_STATE_DOWN.
		// Syslog the current state for debugging purposes.
		// Trace a message into the trace buffer as well.
		//

		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "MEMBER_INVALID_EVENT being processed, "
		    "current state is %s\n", name_to_log,
		    membership_state_machine_state_names[current_state]));

		//
		// SCMSGS
		// @explanation
		// The membership state machine for this membership encountered
		// an invalid event.
		// @user_action
		// In this scenario, more syslog messages will appear.
		// Search for these subsequent syslog messages and
		// follow the suggested recovery steps.
		// Also contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership : State machine for '%s' encountered "
		    "an invalid event, currently in '%s'.", name_to_log,
		    membership_state_machine_state_names[current_state]);

		break;

	case MEMBER_NOT_IN_LIST:
	case MEMBER_IN_LIST:
	case MEMBER_OBSOLETE_IN_LIST:
	case MEMBER_DOWN_PENDING:
		//
		// In these event scenarios, the leader membership manager
		// has delivered membership data during reconfiguration,
		// and one of these events has been generated based on whether :
		// - the local member is marked down in delivered membership
		// (MEMBER_NOT_IN_LIST)
		// - the state machine knows that the local member is up
		// and the same incarnation is marked up in delivered membership
		// (MEMBER_IN_LIST)
		// - the state machine knows that the local member is up
		// and the delivered membership also has the local member
		// marked up; but these 'up' incarnations do not match
		// (MEMBER_OBSOLETE_IN_LIST)
		// - the state machine knows that the local member is down
		// but the delivered membership has the local member marked up
		// (DOWN_PENDING)
		//
		// Update our local copy of delivered membership and
		// seqnum with the membership data delivered by leader.
		// Publish cluster events as required.
		//
		// Pass the flag, that says if shutdown is happening, to false.
		//
		received_membership(
		    ev_packetp->membership, ev_packetp->seqnum, false);
		break;

	case MEMBER_GOES_DOWN:
		// Local member went down
		ASSERT(ev_packetp->latest_incn == INCN_UNKNOWN);
		latest_incn_known_to_state_machine = INCN_UNKNOWN;

		//
		// If state machine is in SHUTTING_DOWN_STATE,
		// then this event would make it to enter INITIAL_STATE
		// signalling that shut down is complete.
		// So we reset this flag to false.
		//
		if (current_state == SHUTTING_DOWN_STATE) {
			membership_info_received_from_leader = false;
		}

		break;

	case MEMBER_COMES_UP:
		// Local member came up
		ASSERT(ev_packetp->latest_incn != INCN_UNKNOWN);
		latest_incn_known_to_state_machine = ev_packetp->latest_incn;
		break;

	case MEMBER_SHUTTING_DOWN:
		//
		// Membership is being shut down.
		// Update our local membership copy with null membership.
		// Update our local seqnum copy with the shutdown seqnum
		// delivered in the event packet.
		// Also mark that we received new membership (NULL membership)
		// from the leader membership manager.
		//
		// However, we do not publish cluster events or
		// syslog messages now. Once the shutdown is complete,
		// these actions would be taken.
		//
		// Pass the flag, that says if shutdown is happening, to true.
		//
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (ev_packetp->membership.members[nid]
			    != INCN_UNKNOWN) {
				ASSERT(0);
			}
		}
		received_membership(
		    ev_packetp->membership, ev_packetp->seqnum, true);

		//
		// If state machine knows that local membership component
		// is down (either NOT_MEMBER_STATE or DOWN_PENDING_STATE),
		// then this event would make it to enter INITIAL_STATE
		// signalling that shut down is complete.
		// (During a shut down, the state machine enters
		// SHUTTING_DOWN_STATE only if it knows that the local
		// membership component is up and is shutting down now.)
		// So we reset this flag to false.
		//
		if (latest_incn_known_to_state_machine == INCN_UNKNOWN) {
			membership_info_received_from_leader = false;
		}

		break;

	case MEMBER_REMOVED:
		// Membership is being removed; nothing to do here
		break;

	default:
		// Must not be reached
		CL_PANIC(0);
	}

	//
	// The processing of an event sets the state variables according
	// to the information in the event.
	// Once the state machine enters a particular state,
	// the state handler ensures that the values of the state variables
	// are what they should be in that particular state.
	// This ensures that the state machine is working correctly.
	//
	// However, the invalid states (INVALID_STATE_UP and INVALID_STATE_DOWN)
	// are a special case.
	// Once something 'invalid' happens, the state machine enters
	// one of these states. So the state handlers for these invalid states
	// do not check for the values of these state variables;
	// in fact, these invalid states represent an invalid transition or
	// invalid values of one or more state variables.
	// We allow the recovery of a state machine from an invalid state
	// through a shutdown of the membership and thus the state machine
	// moves out of an invalid state to one of the valid states.
	// So for an event that would make the state machine transition out of
	// an invalid state into a valid state, we need to set the values of the
	// state variables to what is expected in that particular valid state.
	//
	if (current_state == INVALID_STATE_DOWN) {
		if (ev_packetp->event == MEMBER_SHUTTING_DOWN) {
			//
			// The state machine will transition
			// to INITIAL_STATE. So set the state variables
			// to what is expected in INITIAL_STATE.
			//
			membership_info_received_from_leader = false;
			latest_incn_known_to_state_machine = INCN_UNKNOWN;
			//
			// For a MEMBER_SHUTTING_DOWN event, the membership
			// sent by the leader consists of INCN_UNKNOWN
			// for all nodes. This membership has already been
			// copied in to the membership snap shot; so we
			// do not need to update it any more.
			//
			for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
				if (membership.members[nid] != INCN_UNKNOWN) {
					ASSERT(0);
				}
			}
		}

	} else if (current_state == INVALID_STATE_UP) {
		if (ev_packetp->event == MEMBER_SHUTTING_DOWN) {
			//
			// The state machine will transition
			// to SHUTTING_DOWN_STATE. So set the state variables
			// to what is expected in SHUTTING_DOWN_STATE.
			//
			membership_info_received_from_leader = true;
			//
			// We are in INVALID_STATE_UP.
			// We enter INVALID_STATE_UP if something 'invalid'
			// occurs when we are in a state where the local
			// member component is known locally as 'up'.
			// So we can assume that the latest incarnation
			// of the local member component, that the state
			// machine knows about, cannot be INCN_UNKNOWN.
			//
			ASSERT(latest_incn_known_to_state_machine
			    != INCN_UNKNOWN);
			//
			// We got a SHUTTING_DOWN event, which means
			// the leader is driving a shutdown procedure.
			// That would have set the shutdown flag to true.
			//
			ASSERT(shutdown);
		}
	}

	// Get the next state to return
	membership_state_machine_state_t next_state =
	    state_transition_table[current_state][ev_packetp->event];

	// Return what has been determined as the next state
	return (next_state);
}

//
// Gets the first event off the event list and returns it.
//
// If the event list is empty, then this method waits
// until at least one event packet is available on the list;
// and then returns the first element off the list.
//
event_packet_t *
membership_manager_impl::get_next_event_or_wait()
{
	ASSERT(state_lock.lock_held());
	event_packet_t *ev_packetp = NULL;
	do {
		// Get the first event from the list of events
		ev_packetp = event_list.reapfirst();
		if (ev_packetp == NULL) {
			//
			// No events present on the list.
			// Wait here until we are signalled due to some event.
			//
			state_cv.wait(&state_lock);
		}
	} while (ev_packetp == NULL);

	// At least one event needs to be processed
	ASSERT(ev_packetp != NULL);
	return (ev_packetp);
}

//
// Certain conditions must be true in a state of the state machine.
// If the validation of such conditions fail, the state machine empties out
// the event queue, and puts a MEMBER_INVALID_EVENT on to the queue,
// so that the state machine can directly transition to one of the invalid
// states next - either INVALID_STATE_UP or INVALID_STATE_DOWN.
// Also trace and syslog messages saying that the validation failed.
//
void
membership_manager_impl::state_validation_failed(const char *failure_reasonp)
{
	ASSERT(state_lock.lock_held());

	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "validation of state %s failed; reason '%s'\n", name_to_log,
	    membership_state_machine_state_names[current_state],
	    failure_reasonp));
	//
	// SCMSGS
	// @explanation
	// Validation of a state failed for the state machine of
	// the specified membership. The state machine will transition
	// to an invalid state.
	// @user_action
	// Contact your authorized Sun service provider to determine
	// whether a workaround or patch is available.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "Membership : validation of state %s failed for '%s' membership; "
	    "reason '%s'.", membership_state_machine_state_names[current_state],
	    name_to_log, failure_reasonp);

	// Empty out event queue
	event_packet_t *ev_packetp = NULL;
	while ((ev_packetp = event_list.reapfirst()) != NULL) {
		delete ev_packetp;
	}

	// Allocate the event packet structure to push on to event list
	ev_packetp = new event_packet_t;
	if (ev_packetp == NULL) {
		//
		// Unable to allocate small amount of memory;
		// serious memory shortage
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Could not create event packet "
		    "for membership (%s); node is low on memory", name_to_log);
	}

	// Put MEMBER_INVALID_EVENT on the queue
	ev_packetp->event = MEMBER_INVALID_EVENT;

	// Data that is not required anymore; put in zero/NULL values
	ev_packetp->seqnum = 0;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		ev_packetp->membership.members[nid] = INCN_UNKNOWN;
	}
	ev_packetp->latest_incn = INCN_UNKNOWN;

	// Put event packet on to the event list (at the end of the list)
	event_list.append(ev_packetp);
}

void
membership_manager_impl::invalid_state_down(
    membership_state_machine_state_t previous_state,
    membership_state_machine_event_t previous_event)
{
	membership_manager_impl::invalid_state_common(
	    previous_state, previous_event);
}

void
membership_manager_impl::invalid_state_up(
    membership_state_machine_state_t previous_state,
    membership_state_machine_event_t previous_event)
{
	membership_manager_impl::invalid_state_common(
	    previous_state, previous_event);
}

// Common actions for any invalid state
void
membership_manager_impl::invalid_state_common(
    membership_state_machine_state_t previous_state,
    membership_state_machine_event_t previous_event)
{
	// Should not be reached.
	ASSERT(0);

	//
	// lint says the following is unreachable code
	//lint -e527
	//

	ASSERT(state_lock.lock_held());

	//
	// Either cluster membership or process membership related to a cluster
	// (zone cluster or base cluster) has become invalid owing to
	// something bad that has happened !!!
	// This should not happen.
	//
	// If this membership is the cluster membership for a zone cluster
	// or is the process membership for a process related to a zone cluster,
	// then syslog a message asking the user to shut down the zone cluster
	// and then boot it back.
	//
	// If this membership is the cluster membership for the base cluster
	// or is the process membership for a process related to
	// the base cluster, then we need to halt the local base node.
	//

	if (os::strcmp(cluster_name, DEFAULT_CLUSTER) == 0) {
		//
		// Membership is related to base cluster; panic the node
		//

		//
		// SCMSGS
		// @explanation
		// The membership state machine for this membership related
		// to global cluster has entered an invalid state.
		// The local global-cluster node will go down.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : State machine for '%s' "
		    "entered an invalid state %s; previous state was %s, "
		    "previous event was %s.", name_to_log,
		    membership_state_machine_state_names[current_state],
		    membership_state_machine_state_names[previous_state],
		    membership_state_machine_event_names[previous_event]);
	} else {
		//
		// Membership is related to zone cluster; syslog a message
		// asking the user to shut down the zone cluster and
		// then boot it back.
		//

		//
		// SCMSGS
		// @explanation
		// The membership state machine for this membership related
		// to a zone cluster has entered an invalid state.
		// @user_action
		// Shut down the zone cluster and then boot it back.
		// This series of actions will clear the immediate problem.
		// Also contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership : State machine for '%s' related to "
		    "zone cluster '%s' entered an invalid state %s; "
		    "previous state was %s, previous event was %s.",
		    name_to_log, cluster_name,
		    membership_state_machine_state_names[current_state],
		    membership_state_machine_state_names[previous_state],
		    membership_state_machine_event_names[previous_event]);
	}

	//lint +e527
}

void
membership_manager_impl::initial_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have not received any membership information
	// from the leader membership manager
	//
	if (membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is true");
	}

	// Check that local member is down
	if (latest_incn_known_to_state_machine != INCN_UNKNOWN) {
		char reason[200];
		os::sprintf(reason, "latest_incn_known_to_state_machine = %d",
		    latest_incn_known_to_state_machine);
		state_validation_failed((const char *)reason);
	}

	//
	// Ensure that the official membership is NULL;
	// that is all incarnations are marked INCN_UNKNOWN.
	//
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			char reason[200];
			os::sprintf(reason,
			    "membership incarnation for nodeid %d = %d",
			    membership.members[nid]);
			state_validation_failed((const char *)reason);
			break;
		}
	}

	//
	// Wake up any thread waiting for us to enter INITIAL_STATE.
	//
	// (1) When the state machine is created, the thread that
	// creates this state machine will wait	until the state machine
	// moves out of INVALID_STATE_DOWN when it starts off,
	// and enters INITIAL_STATE.
	//
	// (2) In case of a shutdown, the shutdown reconfiguration thread
	// waits for the state machine to enter INITIAL_STATE as well.
	//
	state_cv.signal();
}

void
membership_manager_impl::up_transit_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have not received any membership information
	// from the leader membership manager
	//
	if (membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is true");
	}

	// Check that local member component is up.
	if (latest_incn_known_to_state_machine == INCN_UNKNOWN) {
		state_validation_failed(
		    "latest_incn_known_to_state_machine = INCN_UNKNOWN");
	}
}

void
membership_manager_impl::up_pending_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have received membership information
	// from the leader membership manager
	//
	if (!membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is false");
	}

	// Check that local member component is up.
	if (latest_incn_known_to_state_machine == INCN_UNKNOWN) {
		state_validation_failed(
		    "latest_incn_known_to_state_machine = INCN_UNKNOWN");
	}

	//
	// Ensure that we are marked down in the official membership
	// delivered by the leader
	//
	if (membership.members[orb_conf::local_nodeid()] != INCN_UNKNOWN) {
		char reason[200];
		os::sprintf(reason,
		    "membership incarnation for local node = %d",
		    membership.members[orb_conf::local_nodeid()]);
		state_validation_failed((const char *)reason);
	}
}

void
membership_manager_impl::up_obsolete_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have received membership information
	// from the leader membership manager
	//
	if (!membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is false");
	}

	// Check that local member component is up.
	if (latest_incn_known_to_state_machine == INCN_UNKNOWN) {
		state_validation_failed(
		    "latest_incn_known_to_state_machine = INCN_UNKNOWN");
	}

	//
	// Ensure that we are marked up in the official membership
	// delivered by the leader membership manager,
	// but with a different incarnation than
	// the latest local incarnation known to the state machine.
	//
	if (membership.members[orb_conf::local_nodeid()] == INCN_UNKNOWN) {
		state_validation_failed(
		    "membership incarnation for local node = INCN_UNKNOWN");
	}
	if (membership.members[orb_conf::local_nodeid()]
	    == latest_incn_known_to_state_machine) {
		char reason[200];
		os::sprintf(reason,
		    "membership incarnation for local node = %d, "
		    "latest_incn_known_to_state_machine = %d",
		    membership.members[orb_conf::local_nodeid()],
		    latest_incn_known_to_state_machine);
		state_validation_failed((const char *)reason);
	}
}

void
membership_manager_impl::down_pending_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have received membership information
	// from the leader membership manager
	//
	if (!membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is false");
	}

	// Check that local member component is down.
	if (latest_incn_known_to_state_machine != INCN_UNKNOWN) {
		char reason[200];
		os::sprintf(reason, "latest_incn_known_to_state_machine = %d",
		    latest_incn_known_to_state_machine);
		state_validation_failed((const char *)reason);
	}

	//
	// Ensure that we are marked up in the official membership
	// delivered by the leader membership manager.
	//
	if (membership.members[orb_conf::local_nodeid()] == INCN_UNKNOWN) {
		state_validation_failed(
		    "membership incarnation for local node = INCN_UNKNOWN");
	}
}

void
membership_manager_impl::member_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have received membership information
	// from the leader membership manager
	//
	if (!membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is false");
	}

	// Check that local member component is up.
	if (latest_incn_known_to_state_machine == INCN_UNKNOWN) {
		state_validation_failed(
		    "latest_incn_known_to_state_machine = INCN_UNKNOWN");
	}

	//
	// Ensure that we are marked up in the official membership
	// delivered by the leader membership manager, with the same incarnation
	// as the latest local incarnation known to the state machine.
	//
	if (membership.members[orb_conf::local_nodeid()] == INCN_UNKNOWN) {
		state_validation_failed(
		    "membership incarnation for local node = INCN_UNKNOWN");
	}
	if (membership.members[orb_conf::local_nodeid()]
	    != latest_incn_known_to_state_machine) {
		char reason[200];
		os::sprintf(reason,
		    "membership incarnation for local node = %d, "
		    "latest_incn_known_to_state_machine = %d",
		    membership.members[orb_conf::local_nodeid()],
		    latest_incn_known_to_state_machine);
		state_validation_failed((const char *)reason);
	}
}

void
membership_manager_impl::not_member_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have received membership information
	// from the leader membership manager
	//
	if (!membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is false");
	}

	// Check that local member component is down.
	if (latest_incn_known_to_state_machine != INCN_UNKNOWN) {
		char reason[200];
		os::sprintf(reason, "latest_incn_known_to_state_machine = %d",
		    latest_incn_known_to_state_machine);
		state_validation_failed((const char *)reason);
	}

	//
	// Ensure that we are marked down in the official membership
	// delivered by the leader membership manager.
	//
	if (membership.members[orb_conf::local_nodeid()] != INCN_UNKNOWN) {
		char reason[200];
		os::sprintf(reason,
		    "membership incarnation for local node = %d",
		    membership.members[orb_conf::local_nodeid()]);
		state_validation_failed((const char *)reason);
	}
}

void
membership_manager_impl::shutting_down_state(
    membership_state_machine_state_t,
    membership_state_machine_event_t)
{
	ASSERT(state_lock.lock_held());

	//
	// Check that we have received membership information
	// from the leader membership manager
	//
	if (!membership_info_received_from_leader) {
		state_validation_failed(
		    "membership_info_received_from_leader is false");
	}

	// Check that the shutdown flag is still marked true
	if (!shutdown) {
		state_validation_failed("shutdown flag is false");
	}

	//
	// We are in the process of shutting down, but
	// we have not yet completely shut down.
	// So the local member component should still be up.
	//
	if (latest_incn_known_to_state_machine == INCN_UNKNOWN) {
		state_validation_failed(
		    "latest_incn_known_to_state_machine = INCN_UNKNOWN");
	}
}

//
// The "leader" has informed us about membership and seqnum data.
// If cluster events are to be generated for this membership,
// publish such events, and also syslog messages
// if any new nodes have joined or if any nodes have died.
//
void
membership_manager_impl::received_membership(
    const cmm::membership_t &cl_membership,
    cmm::seqnum_t cl_seqnum, bool membership_is_shutting_down)
{
	ASSERT(state_lock.lock_held());
	cmm::membership_t old_membership = membership;

	membership = cl_membership;
	seqnum = cl_seqnum;
	membership_info_received_from_leader = true;

	//
	// We received membership from the leader membership manager;
	// schedule callbacks to registered membership clients.
	//
	callback_clients();

	if (membership_is_shutting_down) {
		//
		// This method was called due to a shut down in progress.
		// Do not publish cluster events now; they will be
		// published once the shut down is complete.
		//
		return;
	}

	if (!generate_event) {
		//
		// If membership type does not want events to be generated,
		// then do not generate events; return here.
		//
		return;
	}

	// Publish cluster events and syslog messages for the reconfiguration.
	publish_events_and_syslog(false, seqnum, old_membership, membership);
}

void
membership_manager_impl::publish_events_and_syslog(
    bool, cmm::seqnum_t, cmm::membership_t, cmm::membership_t)
{
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "base class publish_events_and_syslog called\n", name_to_log));
	ASSERT(0);
}

//
// Membership Callback tasks to be scheduled on the callback threadpool
//

// Constructor
membership_callback_task::membership_callback_task(
    const char *namep, membership_manager_impl *managerp,
    membership::client_ptr ptr,
    const cmm::membership_t &members, cmm::seqnum_t seq)
{
	ASSERT(namep);
	(void) os::strcpy(data.membership_name, namep);
	data.mem_managerp = managerp;
	data.client_p = membership::client::_duplicate(ptr);
	data.membership = members;
	data.seqnum = seq;
}

//
// Destructor
//
// lint complains that functions that might throw exceptions should not be
// invoked in destructors, as destructors are not supposed to throw exceptions.
//lint -e1551
membership_callback_task::~membership_callback_task()
{
	CORBA::release(data.client_p);
	data.client_p = membership::client::_nil();
}
//lint +e1551

void
membership_callback_task::execute()
{
	Environment e;
	data.client_p->membership_callback(data.membership, data.seqnum, e);
	CORBA::Exception *exceptionp = e.exception();
	if (exceptionp == NULL) {
		return;
	}

	if (CORBA::INV_OBJREF::_exnarrow(exceptionp)) {
		MEMBERSHIP_TRACE(("Membership: INV_OBJREF exception "
		    "for client %p while executing callback for "
		    "membership '%s'\n", data.client_p, data.membership_name));
		// Unregister this client
		Environment env;
		data.mem_managerp->unregister_client(data.client_p, env);
		CORBA::Exception *exp = env.exception();
		if (exp == NULL) {
			e.clear();
			return;
		} else if (membership::no_such_client::_exnarrow(exp)) {
			//
			// Another callback task executing in parallel
			// for the same client has removed this client
			// from membership callback clients list.
			//
			e.clear();
			return;
		} else {
			ASSERT(0);
		}
	} else {
		// Some other exception
		MEMBERSHIP_TRACE(("CMM Membership : Unknown exception\n"));
		ASSERT(0);
		e.clear();	/*lint !e527 */
		return;	/*lint !e527 */
	}

	// Should not reach here
	ASSERT(0);	/*lint !e527 */
}

//
// The unique address tag for this defer_task
// is the address of the membership manager
//
void *
membership_callback_task::get_address_tag()
{
	return ((void *)(data.mem_managerp));
}

//
// Issue callbacks to clients registered for membership change callbacks
// for this membership manager.
//
void
membership_manager_impl::callback_clients()
{
	//
	// "Issuing" callbacks here does not mean synchronous activity.
	// The callbacks would be scheduled onto a threadpool.
	// XXX Since the callback threadpool is not per zone cluster,
	// XXX the old callbacks that are pending on the threadpool
	// XXX cannot be removed from the pending list.
	// But note that callbacks in progress are not
	// aborted. So it is possible that clients could be getting callbacks
	// for different seqnums of the cluster at the same time.
	// The client needs to ensure its own data consistency, by using
	// some sort of locking of its data.
	//
	membership::client_ptr client_p;
	membership_clients_lock.lock();

	SList<membership::client>::ListIterator iter(membership_clients);
	for (; (client_p = iter.get_current()) != NULL; iter.advance()) {
		MEMBERSHIP_TRACE(("Membership Manager (%s) : "
		    "scheduling callbacks, seqnum %d, client ref %p\n",
		    name_to_log, seqnum, client_p));
		membership_callback_task *taskp = new membership_callback_task(
		    mem_manager_name, this, client_p, membership, seqnum);
		callback_threadpool::the().defer_processing(taskp);
	}
	membership_clients_lock.unlock();
}

void
membership_manager_impl::register_with_engine()
{
	membership_engine_impl *enginep = membership_engine_impl::the();
	ASSERT(enginep);

	for (unsigned int ind = 0; ind < NUM_ACTIONS; ind++) {
		if (os::strlen(event_actions[ind].event_name) != 0) {
			// XXX ignoring return value of method call?
			// XXX how to recover in case of failure?
			//lint -e534
			(void) enginep->register_mem_manager(
			    this, event_actions[ind].event_name);
			//lint +e534
		}
	}
}

void
membership_manager_impl::unregister_with_engine()
{
	membership_engine_impl *enginep = membership_engine_impl::the();
	ASSERT(enginep);

	for (unsigned int ind = 0; ind < NUM_ACTIONS; ind++) {
		if (os::strlen(event_actions[ind].event_name) != 0) {
			// XXX ignoring return value of method call?
			// XXX how to recover in case of failure?
			//lint -e534
			enginep->unregister_mem_manager(
			    this, event_actions[ind].event_name);
			//lint +e534
		}
	}
}

void
membership_manager_impl::fill_membership_manager_info(
    membership::membership_manager_info &data)
{
	data.mem_type = type;
	data.cluster_id = cluster_id;

	if (os::strcmp(process_name, "") == 0) {
		// No process name
		data.process_namep = (char *)NULL;
	} else {
		data.process_namep = os::strdup(process_name);
	}
}

//
// Implementation of different types of membership
//

// Base Cluster membership type object
base_cluster_membership_manager_impl::base_cluster_membership_manager_impl() :
    membership_manager_impl(membership::BASE_CLUSTER, false,
    DEFAULT_CLUSTER, NULL)
{
	int err = 0;
	err = construct_mem_manager_event_name(
	    event_actions[LEADER_DATA_COLLECTION].event_name,
	    membership::BASE_CLUSTER_MEMBERSHIP_CHANGE,
	    DEFAULT_CLUSTER, NULL);
	if (err) {
		ASSERT(0);
	}
	event_actions[MEMBER_CAME_UP].event_name[0] = '\0';
	event_actions[MEMBER_WENT_DOWN].event_name[0] = '\0';
}

base_cluster_membership_manager_impl::~base_cluster_membership_manager_impl()
{
}

// Zone Cluster membership type object
zone_cluster_membership_manager_impl::zone_cluster_membership_manager_impl(
    const char *cl_namep) :
    membership_manager_impl(membership::ZONE_CLUSTER, true, cl_namep, NULL)
{
	initiate_fencing_flag = true;
	pre_mem_update_flag = true;
	pre_mem_shutdown_flag = true;
	fencing_underway = false;

	int err = 0;
	err = construct_mem_manager_event_name(
	    event_actions[LEADER_DATA_COLLECTION].event_name,
	    membership::BASE_CLUSTER_MEMBERSHIP_CHANGE,
	    DEFAULT_CLUSTER, NULL);
	if (err) {
		ASSERT(0);
	}
	err = construct_mem_manager_event_name(
	    event_actions[MEMBER_CAME_UP].event_name,
	    membership::NODE_UP, cl_namep, NULL);
	if (err) {
		ASSERT(0);
	}
	err = construct_mem_manager_event_name(
	    event_actions[MEMBER_WENT_DOWN].event_name,
	    membership::NODE_DOWN, cl_namep, NULL);
	if (err) {
		ASSERT(0);
	}
}

zone_cluster_membership_manager_impl::~zone_cluster_membership_manager_impl()
{
}

//
// The caller waits till the ZONE_CLUSTER membership manager is doing fencing.
// Returns false if a base cluster CMM reconfiguration triggers while waiting.
// Returns true otherwise, once ZONE_CLUSTER membership manager
// has done its fencing task.
//
void
zone_cluster_membership_manager_impl::wait_till_fencing_underway()
{
	fencing_underway_lock.lock();
	while (fencing_underway) {
		fencing_underway_cv.wait(&fencing_underway_lock);
	}
	fencing_underway_lock.unlock();
}

void
zone_cluster_membership_manager_impl::initiate_fencing(
    cmm::seqnum_t node_seqnum)
{
	// Initiate fencing for zone cluster
	MEMBERSHIP_TRACE(("Membership Manager (%s) : Initiate fencing\n",
	    name_to_log));
	if (new_base_cluster_reconfig_started(node_seqnum)) {
		return;
	}
	fencing_underway_lock.lock();
	fencing_underway = true;
	fencing_underway_lock.unlock();
}

void
zone_cluster_membership_manager_impl::pre_mem_update(
    const cmm::membership_t &, cmm::seqnum_t, cmm::seqnum_t)
{
	MEMBERSHIP_TRACE((
	    "Membership Manager (%s) : Do fencing step\n", name_to_log));
	fencing_underway_lock.lock();

	//
	// XXX Put/remove fence locks based on membership for zone cluster.
	// XXX Call into device subsystem to do the actual fencing.
	//

	// Fencing task is done; wake up any waiting reconfigurations
	fencing_underway = false;
	fencing_underway_cv.broadcast();
	fencing_underway_lock.unlock();
}

void
zone_cluster_membership_manager_impl::pre_mem_shutdown()
{
	// Put fence locks for all members, similar to kernel CMM shutdown
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "Fencing before shutdown\n", name_to_log));
}

void
zone_cluster_membership_manager_impl::publish_events_and_syslog(
    bool shutdown_flag, cmm::seqnum_t,
    cmm::membership_t old_membership, cmm::membership_t new_membership)
{
	if (shutdown_flag) {
		//
		// SCMSGS
		// @explanation
		// The zone cluster has been shutdown completely on this node.
		// @user_action
		// This message is for information. No action is needed.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "Membership : Cluster '%s' shutdown completed.",
		    cluster_name);

		// Get the nodename of the local node of the zone cluster
		clconf_cluster_t *zc_clconfp =
		    clconf_cluster_get_vc_current(cluster_name);
		ASSERT(zc_clconfp != NULL);
		const char *nodenamep = clconf_cluster_get_nodename_by_nodeid(
		    zc_clconfp, clconf_get_local_nodeid());

		//
		// If a zone for the zone cluster exists on this base node,
		// then publish the shutdown event for the zone.
		//
		if (nodenamep != NULL) {
			// Publish the event
			(void) sc_publish_zc_event(
			    cluster_name, ESC_CLUSTER_NODE_STATE_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodenamep,
			    CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			    CL_REASON_CMM_NODE_SHUTDOWN, NULL);
		}

		clconf_obj_release((clconf_obj_t *)zc_clconfp);
		return;
	}

	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {

		if (old_membership.members[nid] ==
		    new_membership.members[nid]) {
			// No change in node status
			continue;
		}

		// Get the nodename of the zone cluster node
		clconf_cluster_t *zc_clconfp =
		    clconf_cluster_get_vc_current(cluster_name);
		ASSERT(zc_clconfp != NULL);
		const char *nodenamep =
		    clconf_cluster_get_nodename_by_nodeid(zc_clconfp, nid);
		ASSERT(nodenamep != NULL);

		if ((old_membership.members[nid] == INCN_UNKNOWN) &&
		    (new_membership.members[nid] != INCN_UNKNOWN)) {
			//
			// SCMSGS
			// @explanation
			// A node has joined the zone cluster.
			// @user_action
			// This message is for information. No action is needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "Membership : Node '%s' (node id %d) "
			    "of cluster '%s' joined.",
			    nodenamep, nid, cluster_name);
			(void) sc_publish_zc_event(
			    cluster_name, ESC_CLUSTER_NODE_STATE_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodenamep,
			    CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			    CL_REASON_CMM_NODE_JOINED, NULL);

		} else if ((old_membership.members[nid] != INCN_UNKNOWN) &&
		    (new_membership.members[nid] == INCN_UNKNOWN)) {
			//
			// SCMSGS
			// @explanation
			// A node has left the zone cluster.
			// @user_action
			// This message is for information. No action is needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "Membership : Node '%s' (node id %d) "
			    "of cluster '%s' died.",
			    nodenamep, nid, cluster_name);
			(void) sc_publish_zc_event(
			    cluster_name, ESC_CLUSTER_NODE_STATE_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodenamep,
			    CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			    CL_REASON_CMM_NODE_LEFT, NULL);

		} else {
			ASSERT(old_membership.members[nid] != INCN_UNKNOWN);
			ASSERT(new_membership.members[nid] != INCN_UNKNOWN);
			ASSERT(old_membership.members[nid]
			    != new_membership.members[nid]);

			//
			// A node of the zone cluster left and rejoined.
			// Consider that the member died and came up.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "Membership : Node '%s' (node id %d) "
			    "of cluster '%s' died.",
			    nodenamep, nid, cluster_name);
			(void) sc_publish_zc_event(
			    cluster_name, ESC_CLUSTER_NODE_STATE_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodenamep,
			    CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			    CL_REASON_CMM_NODE_LEFT, NULL);

			(void) cmm_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
			    "Membership : Node '%s' (node id %d) "
			    "of cluster '%s' joined.",
			    nodenamep, nid, cluster_name);
			(void) sc_publish_zc_event(
			    cluster_name, ESC_CLUSTER_NODE_STATE_CHANGE,
			    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodenamep,
			    CL_REASON_CODE, SE_DATA_TYPE_UINT32,
			    CL_REASON_CMM_NODE_JOINED, NULL);
		}
		clconf_obj_release((clconf_obj_t *)zc_clconfp);
	}
}

// Process-up-process-down membership manager type
process_up_process_down_membership_manager_impl::
    process_up_process_down_membership_manager_impl(
    const char *cl_namep, const char *proc_namep) :
    membership_manager_impl(membership::PROCESS_UP_PROCESS_DOWN, false,
    cl_namep, proc_namep)
{
	int err = 0;
	err = construct_mem_manager_event_name(
	    event_actions[LEADER_DATA_COLLECTION].event_name,
	    membership::BASE_CLUSTER_MEMBERSHIP_CHANGE,
	    DEFAULT_CLUSTER, NULL);
	if (err) {
		ASSERT(0);
	}
	err = construct_mem_manager_event_name(
	    event_actions[MEMBER_CAME_UP].event_name,
	    membership::PROCESS_UP, cl_namep, proc_namep);
	if (err) {
		ASSERT(0);
	}
	err = construct_mem_manager_event_name(
	    event_actions[MEMBER_WENT_DOWN].event_name,
	    membership::PROCESS_DOWN, cl_namep, proc_namep);
	if (err) {
		ASSERT(0);
	}
}

process_up_process_down_membership_manager_impl::
	~process_up_process_down_membership_manager_impl()
{
}

// Process-up-zone-down membership manager type
process_up_zone_down_membership_manager_impl::
    process_up_zone_down_membership_manager_impl(
    const char *cl_namep, const char *proc_namep,
    zone_cluster_membership_manager_impl *zc_mem_mgrp) :
    membership_manager_impl(membership::PROCESS_UP_ZONE_DOWN, false,
    cl_namep, proc_namep)
{
	int err = 0;
	err = construct_mem_manager_event_name(
	    event_actions[LEADER_DATA_COLLECTION].event_name,
	    membership::BASE_CLUSTER_MEMBERSHIP_CHANGE,
	    DEFAULT_CLUSTER, NULL);
	if (err) {
		ASSERT(0);
	}
	err = construct_mem_manager_event_name(
	    event_actions[MEMBER_CAME_UP].event_name,
	    membership::PROCESS_UP, cl_namep, proc_namep);
	if (err) {
		ASSERT(0);
	}
	err = construct_mem_manager_event_name(
	    event_actions[MEMBER_WENT_DOWN].event_name,
	    membership::NODE_DOWN, cl_namep, NULL);
	if (err) {
		ASSERT(0);
	}

	//
	// The pre_mem_update step makes a reconfiguration of this type of
	// membership wait until the zone cluster membership reconfiguration
	// for the related zone cluster does fencing. For base cluster,
	// the fencing is done by the kernel CMM during its own
	// reconfiguration. So the pre_mem_update step is not executed
	// for process-up-zone-down memberships related to the base cluster.
	//
	if (os::strcmp(cl_namep, DEFAULT_CLUSTER) != 0) {
		pre_mem_update_flag = true;
		//
		// Keep a reference to the ZONE_CLUSTER membership manager
		// for this zone cluster
		//
		ASSERT(zc_mem_mgrp != NULL);
		zc_mem_managerp = zc_mem_mgrp;
	} else {
		// Base cluster related membership manager
		ASSERT(zc_mem_mgrp == NULL);
		pre_mem_update_flag = false;
		zc_mem_managerp = NULL;
	}
}

process_up_zone_down_membership_manager_impl::
	~process_up_zone_down_membership_manager_impl()
{
	zc_mem_managerp = NULL;
}

void
process_up_zone_down_membership_manager_impl::pre_mem_update(
    const cmm::membership_t &, cmm::seqnum_t, cmm::seqnum_t)
{
	//
	// This step makes this membership reconfiguration wait until
	// the zone cluster membership reconfiguration for the related
	// zone cluster does fencing. For base cluster, the fencing
	// is done by the kernel CMM during its own reconfiguration.
	// So this step is not executed for process-up-zone-down memberships
	// related to the base cluster.
	//
	ASSERT(os::strcmp(cluster_name, DEFAULT_CLUSTER) != 0);

	//
	// Wait for the zone cluster membership manager for this cluster
	// to mark that it has finished its fencing task.
	//
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "Wait till fencing is done,\n", name_to_log));
	zc_mem_managerp->wait_till_fencing_underway();
	MEMBERSHIP_TRACE(("Membership Manager (%s) : "
	    "wait for fencing is done\n", name_to_log));
}
//lint +e788
#endif	// (SOL_VERSION >= __s10)
