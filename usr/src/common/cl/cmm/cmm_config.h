/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CMM_CONFIG_H
#define	_CMM_CONFIG_H

#pragma ident	"@(#)cmm_config.h	1.43	09/04/13 SMI"

/*
 * cmm_config.h
 *
 *	header file for CMM configuration info
 */


#include <h/cmm.h>
#include <h/ff.h>
#include <sys/clconf_int.h>

#ifdef	_KERNEL
/* Include header for SList */
#include <sys/list_def.h>
#endif

/*
 * Static cluster configuration information structure. It is filled at
 * startup time from the CCR.
 */
typedef struct {
	ff::ffmode	ff_mode;
	cmm::timeout_t	failfast_gracetime;
	cmm::timeout_t	failfast_panic_delay;
	cmm::timeout_t	failfast_proxy_panic_delay;
	cmm::timeout_t	node_fenceoff_timeout;
	cmm::timeout_t	boot_delay;
	cmm::timeout_t  node_halt_timeout;

#ifdef	_KERNEL
	/*
	 * The following lock protects access to the membership configuration
	 * information, like multiple_partitions and ping_targets,
	 * that is stored in this structure.
	 */
	os::mutex_t		membership_info_lock;
#ifdef	WEAK_MEMBERSHIP
	bool			multiple_partitions;
#endif
	/* Ping targets are IP addresses, that are stored here as strings */
	SList<char>		ping_targets;
#endif
} confinfo_t;

extern confinfo_t conf;

#endif	/* _CMM_CONFIG_H */
