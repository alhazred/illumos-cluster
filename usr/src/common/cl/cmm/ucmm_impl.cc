//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ucmm_impl.cc	1.53	08/10/13 SMI"

#include <h/naming.h>
#include <nslib/ns.h>
#include <sys/sc_syslog_msg.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_impl.h>
#include <cmm/ucmm_impl.h>

#if (SOL_VERSION >= __s10)
#include <zone.h>
#include <h/membership.h>
#include <cmm/cmm_ns.h>
#include <cmm/membership_client.h>
#include <cmm/ucmm_comm_impl.h>
#include <sys/vc_int.h>
#include <sys/cladm_int.h>
#include <cmm/ucmm_api.h>

boolean_t using_process_membership = B_FALSE;

static cmm::membership_t new_reconfig_cb_membership;
static cmm::seqnum_t new_reconfig_cb_seqnum = 0;
static cmm::seqnum_t old_reconfig_cb_seqnum = 0;
static bool membership_changed = false;
static os::mutex_t membership_callback_lock;
static os::condvar_t membership_callback_cv;

static os::mutex_t first_boot_lock;
static os::condvar_t first_boot_cv;

// Data used during rolling upgrade (RU)
static nodeset_t before_ru_nodeset = 0;
static bool ucmm_ru_transition_in_progress = false;
#define	UCMM_RU_TRANSITION_SLEEP_PERIOD	10	// In seconds

membership::client_ptr client_p = membership::client::_nil();

void membership_callback(const char *, const char *,
    const cmm::membership_t, const cmm::seqnum_t);
static void membership_callback_internal(
    const cmm::membership_t, const cmm::seqnum_t);
#endif

// implementation file for userland cmm module

static ucmm_impl *the_ucmm_impl = NULL;

// Constructor
ucmm_impl::ucmm_impl(const char *name, const char *tag) :
    ucmm_name(name), syslog_tag(tag)
{
	CL_PANIC(the_ucmm_impl == NULL);
	the_ucmm_impl = this;

#if (SOL_VERSION >= __s10)
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		new_reconfig_cb_membership.members[nid] = INCN_UNKNOWN;
	}

	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		peer_incarnations[nid] = INCN_UNKNOWN;
	}
#endif
}

//
// Function that returns a pointer to the single instance of the
// ucmm_impl class.
//
ucmm_impl &
ucmm_impl::the()
{
	CL_PANIC(the_ucmm_impl != NULL);
	return (*the_ucmm_impl);
}

//
// Complete initialization of the userland CMM. This involves:
//
// 1. Get an incarnation number for this ucmm (from the time of day).
// 2. Initialize the automaton by calling cmm_impl::cmm_init().
// 3. Register the ucmm with the root name server.
//
void
ucmm_impl::init(cmm::comm_ptr ucomm)
{
	sc_syslog_msg_handle_t handle;

	struct timeval tv;
	(void) gettimeofday(&tv, NULL);
	if (tv.tv_sec == 0)	// 0 conflicts with uninitialized
		tv.tv_sec = 1;
	local_incarnation = (incarnation_num) tv.tv_sec;

#if (SOL_VERSION >= __s10)
	peer_incarnations[orb_conf::local_nodeid()] = local_incarnation;
#endif

	_automp = cmm_impl::cmm_init((char *)ucmm_name,
	    orb_conf::node_number(), ucomm, local_incarnation);

	// Register this ucmm with the root name server
	naming::naming_context_var ctxp = ns::root_nameserver();
	char buff[30];
	(void) sprintf(buff, "%s%d", ucmm_name, orb_conf::node_number());
	cmm::userland_cmm_var cmmp = get_objref();
	Environment e;
	ctxp->rebind(buff, cmmp, e);
	if (e.exception()) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "CMM: Unable to bind <%s> to nameserver.", buff);
		sc_syslog_msg_done(&handle);
		abort();
	}
}

//
// IDL method called by a remote CMM to get the references to the local
// CMM. It is called when the remote CMM wants to send CMM status messages
// to this node.
//
// The caller (remote CMM) passes in its nodeid and incarnation number.
// This function creates a remote_ucmm_proxy object with that info and gives
// the caller a reference to it. This function also returns a reference to
// the local automaton and the local incarnation number.
//
// When the remote node dies, the local node gets an _unreferenced for
// the remote_ucmm_proxy object created by this function since the remote
// node has the only reference to it.
//
// This function also informs the local automaton that the remote node has
// become "reachable". This is for informational purposes and corresponds
// to similar work done by the Path Manager for the kernel CMM.
//
void
ucmm_impl::ucmm_handshake(
	nodeid_t nodeid_in, sol::incarnation_num incn_in,
	cmm::automaton_out auto_out, cmm::remote_ucmm_proxy_out proxy_out,
	Environment&)
{
	CMM_TRACE(("ucmm_handshake called by node %ld, "
		"incn %ld\n", nodeid_in, incn_in));
	CL_PANIC(nodeid_in >= 1 && nodeid_in <= NODEID_MAX);
	CL_PANIC(nodeid_in != orb_conf::node_number());
	auto_out = cmm::automaton::_duplicate(_automp);

	proxy_out = (new remote_ucmm_proxy_impl(nodeid_in, incn_in))->
	    get_objref();

#if (SOL_VERSION >= __s10)
	peer_incarnations[nodeid_in] = incn_in;
#endif

	//
	// Give a heads up to the local CMM automaton that the remote node
	// has become reachable.
	//
	Environment e;
	_automp->node_is_reachable(nodeid_in, incn_in, e);
	if (e.exception())
		e.clear();	// we don't care about this exception
}

//
// Indicates that a remote node has gone down. Called when the
// remote_ucmm_proxy object created for the remote node gets an
// _unreferenced().
//
// All this routine needs to do is call the automaton's node_is_unreachable
// method.
//
void
ucmm_impl::node_is_down(sol::nodeid_t nodeid, sol::incarnation_num incn)
{
	Environment e;
	_automp->node_is_unreachable(nodeid, incn, 0 /* no timeout */, e);
	if (e.exception())
		e.clear();	// we don't care about this exception
}

void
ucmm_impl::_unreferenced(unref_t cookie)
{
	// Maybe when we do clean shutdown.
	(void) _last_unref(cookie);
}

#if (SOL_VERSION >= __s10)
void
ucmm_impl::step1_upgrade_callback_action()
{
	ucmm_ru_transition_in_progress = true;
	before_ru_nodeset = 0;
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (peer_incarnations[nid] != INCN_UNKNOWN) {
			nodeset_add(before_ru_nodeset, nid);
		}
	}
	CMM_TRACE(("step1_upgrade_callback_action: "
	    "before_ru_nodeset = %llx\n", before_ru_nodeset));
	setup_process_membership();
}

void
ucmm_impl::step2_upgrade_callback_action()
{
	// Set the flag saying that we are using process membership
	CMM_TRACE(("In step2_upgrade_callback_action"));
	using_process_membership = B_TRUE;
	membership_callback_lock.lock();
	membership_callback_cv.signal();
	membership_callback_lock.unlock();
}

void
membership_callback(const char *cluster_namep, const char *mem_obj_namep,
    const cmm::membership_t new_membership, const cmm::seqnum_t new_seqnum)
{
	nodeset_t mem_nodeset = 0;
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_membership.members[nid] != INCN_UNKNOWN) {
			nodeset_add(mem_nodeset, nid);
		}
	}
	CMM_TRACE(("received process membership callback : nodeset %lld, "
	    "seqnum %d\n", mem_nodeset, (uint32_t)new_seqnum));

	membership_callback_internal(new_membership, new_seqnum);
}

static void
membership_callback_internal(
    const cmm::membership_t new_membership, const cmm::seqnum_t new_seqnum)
{
	membership_callback_lock.lock();
	if (new_reconfig_cb_seqnum <= new_seqnum) {
		//
		// Received new seqnum is not stale.
		//
		new_reconfig_cb_membership = new_membership;
		new_reconfig_cb_seqnum = new_seqnum;
		membership_changed = true;
		membership_callback_cv.signal();
	}
	membership_callback_lock.unlock();
}

// static
void *
ucmm_impl::membership_callback_thread_start(void *)
{
	the_ucmm_impl->membership_callback_thread();
	// NOTREACHED
	return (NULL);
}

void
ucmm_impl::ru_transition(cmm::membership_t &old_membership)
{
	CL_PANIC(!membership_callback_lock.lock_held());

	sleep(UCMM_RU_TRANSITION_SLEEP_PERIOD);

	membership_callback_lock.lock();
	nodeset_t after_ru_nodeset = 0;
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_reconfig_cb_membership.members[nid] != INCN_UNKNOWN) {
			nodeset_add(after_ru_nodeset, nid);
		}
	}
	CMM_TRACE(("ru_transition : after_ru_nodeset = %llx\n",
	    after_ru_nodeset));
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (nodeset_contains(before_ru_nodeset, nid) &&
		    nodeset_contains(after_ru_nodeset, nid)) {
			old_membership.members[nid] =
			    new_reconfig_cb_membership.members[nid];
		} else if (!nodeset_contains(before_ru_nodeset, nid) &&
		    !nodeset_contains(after_ru_nodeset, nid)) {
			old_membership.members[nid] = INCN_UNKNOWN;
		} else if (nodeset_contains(before_ru_nodeset, nid) &&
		    !nodeset_contains(after_ru_nodeset, nid)) {
			old_membership.members[nid] =
			    peer_incarnations[nid];
		} else if (!nodeset_contains(before_ru_nodeset, nid) &&
		    nodeset_contains(after_ru_nodeset, nid)) {
			old_membership.members[nid] = INCN_UNKNOWN;
		} else {
			// Cannot happen
			CL_PANIC(0);
		}
	}
	ucmm_ru_transition_in_progress = false;
	membership_changed = true;
	membership_callback_lock.unlock();
}

void
ucmm_impl::membership_callback_thread()
{
	cmm::membership_t old_membership;
	cmm::seqnum_t old_seqnum = 0;

	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		old_membership.members[nid] = INCN_UNKNOWN;
	}

	for (;;) {
		membership_callback_lock.lock();
		while (!using_process_membership || !membership_changed) {
			membership_callback_cv.wait(&membership_callback_lock);
		}

		// Rolling upgrade transition
		if (ucmm_ru_transition_in_progress) {
			membership_callback_lock.unlock();
			ru_transition(old_membership);
			CL_PANIC(!membership_callback_lock.lock_held());
			continue;
		}

		cmm::membership_t new_membership = new_reconfig_cb_membership;
		cmm::seqnum_t new_seqnum = new_reconfig_cb_seqnum;
		membership_changed = false;
		membership_callback_lock.unlock();

		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (nid == orb_conf::local_nodeid()) {
				continue;
			}

			if ((old_membership.members[nid] != INCN_UNKNOWN) &&
			    (new_membership.members[nid] == INCN_UNKNOWN)) {
				// Node went down
				if (!cm_comm_impl::the().
				    is_automaton_ref_nil(nid)) {
					//
					// We had a reference to
					// the remote node's automaton;
					// as we had sent a handshake to
					// the remote node
					//
					cm_comm_impl::the().node_is_down(nid);
				}
				if (peer_incarnations[nid]
				    != INCN_UNKNOWN) {
					//
					// Remote node had sent a handshake
					// to us, so we had included it
					// in the automaton's membership
					// (CMM-style automaton).
					//
					ucmm_impl::the().node_is_down(
					    nid, peer_incarnations[nid]);
				}
				CMM_TRACE(("Node %d incn %d went down\n",
				    nid, old_membership.members[nid]));

			} else if (
			    (old_membership.members[nid] == INCN_UNKNOWN) &&
			    (new_membership.members[nid] != INCN_UNKNOWN)) {
				// Node came up
				if (cm_comm_impl::the().
				    is_automaton_ref_nil(nid)) {
					cm_comm_impl::the().node_is_up(nid);
					CMM_TRACE(("Node %d came up\n", nid));
				}

			} else if (old_membership.members[nid] !=
			    new_membership.members[nid]) {
				//
				// Node has rebooted once or more
				// XXX What to do? Simulate reboot by
				// XXX saying node is down, and then
				// XXX node is up?
				//
				CMM_TRACE(("Node %d incn %d rebooted\n",
				    nid, old_membership.members[nid]));
				if (!cm_comm_impl::the().
				    is_automaton_ref_nil(nid)) {
					//
					// We had a reference to
					// the remote node's automaton;
					// as we had sent a handshake to
					// the remote node
					//
					cm_comm_impl::the().node_is_down(nid);
				}
				if (peer_incarnations[nid]
				    != INCN_UNKNOWN) {
					//
					// Remote node had sent a handshake
					// to us, so we had included it
					// in the automaton's membership
					// (CMM-style automaton).
					//
					ucmm_impl::the().node_is_down(
					    nid, peer_incarnations[nid]);
				}
				cm_comm_impl::the().node_is_up(nid);

			} else {
				ASSERT(old_membership.members[nid]
				    == new_membership.members[nid]);
			}
		}

		membership_callback_lock.lock();
		old_reconfig_cb_seqnum = new_seqnum;
		membership_callback_lock.unlock();

		old_seqnum = new_seqnum;
		old_membership = new_membership;

		//
		// The UCMM initialization, after registering for membership
		// callbacks and telling that it has come up,
		// will be waiting for the first membership callback
		// to arrive. Signal it so that it can proceed.
		//
		first_boot_lock.lock();
		first_boot_cv.signal();
		first_boot_lock.unlock();
	}
}

//
// Tell the membership subsystem to start maintaining
// process membership for ucmmd.
//
void
ucmm_impl::tell_membership_subsystem_to_create_membership(
    char *zonenamep, uint32_t clid, char *process_namep)
{
	//
	// XXX Make sure that the zonenamep and process_namep below
	// XXX are not freed multiple times.
	//
	membership::engine_var engine_v = cmm_ns::get_membership_engine_ref();
	ASSERT(!CORBA::is_nil(engine_v));
	membership::engine_event_info data;
	data.mem_manager_info.mem_type = membership::PROCESS_UP_ZONE_DOWN;
	data.mem_manager_info.cluster_id = clid;

	//
	// Casting as char* while assigning to string field does not
	// allocate fresh memory
	//
	data.mem_manager_info.process_namep = os::strdup(process_namep);
	Environment e;
	CORBA::Exception *exp = NULL;
	//
	// Deliver creation event to membership engine.
	// If engines are reconfiguring, this call would wait.
	// If engine returns a retry exception, then this code
	// would retry the creation request to engine.
	//
	while (1) {
		engine_v->deliver_event_to_engine(
		    membership::CREATE_MEMBERSHIP, data, e);
		exp = e.exception();
		if (exp == NULL) {
			break;
		}
		if (membership::retry_create_or_delete::_exnarrow(exp)) {
			// Retry creation
			CMM_TRACE(("Received retry_create_or_delete "
			    "exception while requesting membership engine "
			    "to create PROCESS_UP_ZONE_DOWN membership "
			    "for <%s, zone %s>\n", process_namep, zonenamep));
		} else {
			CMM_TRACE(("Received unknown exception "
			    "while requesting membership engine "
			    "to create PROCESS_UP_ZONE_DOWN membership "
			    "for <%s, zone %s>\n", process_namep, zonenamep));
			ASSERT(0);
		}
		e.clear();
	}
}

void
ucmm_impl::register_for_membership_callbacks(
    char *zonenamep, char *process_namep)
{
	Environment e;
	CORBA::Exception *exp = NULL;
	sc_syslog_msg_handle_t handle;
	mem_callback_funcp_t funcp = NULL;

	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	if (CORBA::is_nil(membership_api_v)) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error. The userland CMM could not
		// obtain a reference to the membership API object.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "CMM: Unable to get reference to membership API object.");
		sc_syslog_msg_done(&handle);
		abort();
	}

	funcp = &(::membership_callback);
	membership::client_ptr client_p = (new membership_client_impl(
	    ucmm_name, membership::PROCESS_UP_ZONE_DOWN, zonenamep,
	    process_namep, funcp))->get_objref();
	membership_api_v->register_for_process_zone_membership(
	    client_p, zonenamep, process_namep, e);
	if ((exp = e.exception()) != NULL) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error. The userland CMM could not
		// register for membership change callbacks.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "CMM: Unable to register for membership change callbacks.");
		e.clear();
		sc_syslog_msg_done(&handle);
		abort();
	}
}

//
// Tell membership subsytem that we are up and running.
//
void
ucmm_impl::tell_membership_subsystem_that_we_are_up(
    char *zonenamep, uint32_t clid, char *process_namep)
{
	Environment e;
	CORBA::Exception *exp = NULL;
	sc_syslog_msg_handle_t handle;

	membership::engine_var engine_v = cmm_ns::get_membership_engine_ref();
	if (CORBA::is_nil(engine_v)) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error. The userland CMM could not
		// obtain a reference to the membership engine object.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "CMM: Unable to get reference to membership engine.");
		sc_syslog_msg_done(&handle);
		abort();
	}

	membership::engine_event_info event_info;
	event_info.mem_manager_info.mem_type = membership::PROCESS_UP_ZONE_DOWN;
	event_info.mem_manager_info.cluster_id = clid;
	//
	// Casting as char* while assigning to string field does not
	// allocate fresh memory
	//
//	event_info.mem_manager_info.process_namep = (char *)ucmm_name;
//	event_info.mem_manager_info.process_namep = (char *)UCMMD_PROC_NAME;
	event_info.mem_manager_info.process_namep = os::strdup(UCMMD_PROC_NAME);
	event_info.event = membership::PROCESS_UP;
	engine_v->deliver_event_to_engine(
	    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT, event_info, e);
	if ((exp = e.exception()) != NULL) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error. The userland CMM could not
		// tell the membership subsytem that it has come up.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "CMM: Unable to notify membership subsystem.");
		e.clear();
		sc_syslog_msg_done(&handle);
		abort();
	}
}

void
ucmm_impl::setup_process_membership()
{
	uint32_t clid = 0;
	sc_syslog_msg_handle_t handle;
	char zonename[ZONENAME_MAX];

	// Start the membership callback thread.
	if (thr_create(NULL, 0, ucmm_impl::membership_callback_thread_start,
	    NULL, THR_BOUND, NULL) != 0) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an
		// internal initialization error.
		// A thread could not be created.
		// This is caused by inadequate memory on the system.
		// @user_action
		// Add more memory to the system. If that does not
		// resolve the problem, contact your authorized
		// Sun service provider to determine whether
		// a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR CMM: Failure creating "
		    "membership callback thread.");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}

	// Get current zone name
	if (getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX) == -1) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// An instance of the userland CMM encountered an internal
		// initialization error. The name of the zone, in which
		// the userland CMM is running, could not be obtained.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "CMM: Unable to get current zone name.");
		sc_syslog_msg_done(&handle);
		abort();
	}

	//
	// Convert the zone name (zone cluster name) to a cluster id
	// XXX early in its bootup, ucmmd should check if it is being
	// XXX launched inside a non-global zone that is not part of
	// XXX any zone cluster. If so, it should not start up.
	//
	cz_id_t czid;
	czid.clid = 0;
	(void) strcpy(czid.name, zonename);
	if (_cladm(CL_CONFIG, CL_GET_ZC_ID, &czid)) {
		// Must be able to convert cluster name to cluster id
		ASSERT(0);
	}

	if (getzoneid() != GLOBAL_ZONEID) {
		if (czid.clid < MIN_CLUSTER_ID) {
			ASSERT(0);
		}
	}

	// Tell the membership subsystem to create the membership
	tell_membership_subsystem_to_create_membership(
	    zonename, czid.clid, UCMMD_PROC_NAME);

	// Register with the membership subsystem for membership callbacks
	register_for_membership_callbacks(zonename, UCMMD_PROC_NAME);

	// Tell the membership subsystem that we are up and running
	tell_membership_subsystem_that_we_are_up(
	    zonename, czid.clid, UCMMD_PROC_NAME);
}

void
ucmm_impl::wait_for_first_boot()
{
	first_boot_lock.lock();
	while (1) {
		membership_callback_lock.lock();
		cmm::seqnum_t first_seqnum = old_reconfig_cb_seqnum;
		membership_callback_lock.unlock();
		if (first_seqnum > 0) {
			break;
		}

		// Wait for the first membership callback
		first_boot_cv.wait(&first_boot_lock);
	}

	//
	// The first reconfig callback happened.
	// Leave lock, and return.
	//
	first_boot_lock.unlock();
}
#endif

remote_ucmm_proxy_impl::remote_ucmm_proxy_impl(
	sol::nodeid_t id,
	sol::incarnation_num incn)
{
	nodeid = id;
	incarnation = incn;
}

void
#ifdef DEBUG
remote_ucmm_proxy_impl::_unreferenced(unref_t cookie)
#else
remote_ucmm_proxy_impl::_unreferenced(unref_t)
#endif
{
	CL_PANIC(the_ucmm_impl);
	CMM_TRACE(("_unreferenced on "
	    "remote_ucmm_proxy(nodeid=%ld,incn=%ld)\n",
	    nodeid, incarnation));
#ifdef DEBUG
	CL_PANIC(_last_unref(cookie));
#endif

#if (SOL_VERSION >= __s10)
	if (!using_process_membership) {
		the_ucmm_impl->node_is_down(nodeid, incarnation);
	}
#else
	the_ucmm_impl->node_is_down(nodeid, incarnation);
#endif

	delete this;
}
