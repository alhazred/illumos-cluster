/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UCMM_COMM_IMPL_H
#define	_UCMM_COMM_IMPL_H

#pragma ident	"@(#)ucmm_comm_impl.h	1.35	08/10/13 SMI"

// header file for comm module for userland CMM

#include <h/cmm.h>
#include <orb/object/adapter.h>
#include <sys/nodeset.h>

typedef uint64_t cm_msg_number;
//
// sender_info keeps track of information about each of the other nodes.
// The structure members are defined as follows:
//
//	my_message_number	local message number count for him
//
typedef struct user_sender_info {
	cm_msg_number		my_message_number;
} user_sender_info_t;


class cm_comm_impl : public McServerof<cmm::comm> {

public:

	cm_comm_impl();
	~cm_comm_impl() {}

	//
	// Function that returns a pointer to the single instance of the
	// ucmm_comm_impl class.
	//
	static cm_comm_impl& the();

	//
	// Function called by the boot delay thread to wait until initial
	// handshake is complete.
	//
	void wait_till_init_handshake_done();

	static void *sender_thread_start(void *arg);

	//
	// Method that implements the IDL interface
	//
	void send_all(
		const cmm::message_t &msg,
		quorum::membership_bitmask_t destination_nodes,
		Environment&);

	//
	// This method is used only in kernel cmm in debug mode.
	//
	void send_stop_all(
		quorum::membership_bitmask_t destination_nodes,
		Environment&);

	void	_unreferenced(unref_t cookie)
		{ (void) _last_unref(cookie); } // Must fix for clean shutdown.

#if (SOL_VERSION >= __s10)
	void node_is_up(sol::nodeid_t nid);
	void node_is_down(sol::nodeid_t nid);
	bool is_automaton_ref_nil(sol::nodeid_t nid);
#endif

private:

	// State of the CMM COMM
	typedef enum { ENABLED, ABORTING, ABORTED } cmm_comm_state_t;
	cmm_comm_state_t	_state;	 // enabled, disabled, etc.

	os::mutex_t		lck;
	void	lock()		{ lck.lock(); }
	void	unlock()	{ lck.unlock(); }

	cmm::message_t msgs[2];	// one for automaton, one for sender.
	int	sender_index;   // last message sent by sender; sending
				// of msgs[sender_index] has either
				// completed or is now "in progress".
	int	latest_index;   // latest message from the automaton;
				// msgs[latest_index] is copied from
				// the msg argument of send_all(),
				// atomically.

	// sender_thread data
	typedef enum { SLEEP, WAKEUP, DIE } sender_thread_arg_t;
	os::condvar_t   	sender_cv;
	sender_thread_arg_t	sender_thread_arg;

	// set of nodes to send messages to
	user_sender_info_t	sender_info[NODEID_MAX + 1];
	nodeset			destination_node_set;

	//
	// The automatonp array needs NODEID_MAX+1 entries because arrays start
	// at zero but we use node ids in the range 1..NODEID_MAX.
	//
	//
	os::mutex_t			automatonp_array_lock;
	cmm::automaton_var		automatonp[NODEID_MAX+1];

	cmm::kernel_ucmm_agent_var	kernel_ucmm_agent_p;

	//
	// Keep track of whether an initial handshake with other CMMs
	// has been done.
	//
	bool				init_handshake_done;
	os::mutex_t			init_handshake_lock;
	os::condvar_t			init_handshake_cv;

	//
	// Local functions
	//
	void	sender_thread();
	void	signal_sender_thread(sender_thread_arg_t);
	void	broadcast_msg();
	void	lookup_node_info(sol::nodeid_t);
};

#endif	/* _UCMM_COMM_IMPL_H */
