/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  membership_threadpool_in.h
 *
 */

#ifndef _MEMBERSHIP_THREADPOOL_IN_H
#define	_MEMBERSHIP_THREADPOOL_IN_H

#pragma ident	"@(#)membership_threadpool_in.h	1.3	08/07/21 SMI"

#if (SOL_VERSION >= __s10)
inline threadpool &
membership_threadpool::the()
{
	ASSERT(the_membership_threadpool != NULL);
	return (*the_membership_threadpool);
}

inline threadpool &
callback_threadpool::the()
{
	ASSERT(the_callback_threadpool != NULL);
	return (*the_callback_threadpool);
}
#endif	// (SOL_VERSION >= __s10)
#endif	/* _MEMBERSHIP_THREADPOOL_IN_H */
