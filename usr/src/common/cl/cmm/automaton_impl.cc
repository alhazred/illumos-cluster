//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)automaton_impl.cc	1.164	09/04/13 SMI"

//
// 	automaton_impl.cc
//
//	This file implements the CMM "automaton" interface between the transport
//	independent and transport dependent components of the Cluster Membership
//	Monitor.
//
//	This file also implements the automaton.  The automaton establishes
//	a cluster-wide agreement on the cluster membership and synchronizes
//	the nodes during cluster reconfigurations.
//
// 	This file is a modified version of the SPARCcluster PDB cluster
//	membership monitor automaton.
//

#include <orb/fault/fault_injection.h>
#include <sys/clconf_int.h>
#include <sys/rsrc_tag.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/clusterproc.h>
#ifdef _KERNEL_ORB
#include <orb/transport/path_manager.h>
#include <sys/cl_comm_support.h>
#endif
#ifdef _KERNEL
#include <sys/tuneable.h>
#endif
#include <cmm/cmm_impl.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_ns.h>
#include <cmm/ucmm_api.h>

#ifndef _KERNEL_ORB
#include <cmm/ucmm_comm_impl.h>
#endif

#ifdef	_KERNEL
#define	PING_CMD	"/usr/sbin/ping"
#include <sys/qd_userd.h>
#endif

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
#include <cmm/membership_engine_impl.h>
#endif

#ifdef	_KERNEL
//
// This serves as an override control while determining
// whether to do ping health checks or not.
// If the user sets this to false (can be set in /etc/system
// or mdb), then CMM will not attempt ping health checks.
//
unsigned int ping_check_enabled = 1;
#endif

// accessed with the _state_lock held and used to obtain nodename used
// in syslog messages.

static char nodename[CL_MAX_LEN+1];
//
// The clock interrupt handler will call the function pointed to by
// this pointer during every clock tick.
//
void (*cmm_automaton_callout)(void) = NULL;

//
// Maximum amount of time between successive runs of cmm_timer_thread
// in seconds. Default is 30 seconds.
//
//lint -e528
static uint64_t cmm_max_timer_period = 30;
//lint +e528

//
// Amount of time in seconds cmm_timer_thread sleeps between two
// successive runs. Must be less than cmm_max_timer_period.
// Default is 15 seconds.
//
static uint64_t cmm_timer_period = 15;

//
// The timeout_handle_thread runs every cmm_sleep_interval milliseconds.
// Default is 500 milliseconds.
//
static uint64_t cmm_sleep_interval = 500;

//
// Shorthands for frequently used state fields.
//
#define	MYMEMBERSHIPSET	_nodes[_mynodeid].membership
#define	MYSEQNUM	_nodes[_mynodeid].reconfig_seqnum
#define	MYIVN		_nodes[_mynodeid].initial_view_number
#define	STATEVEC	_nodes[_mynodeid].state_vector
#define	MYSTATE		STATEVEC[_mynodeid]
#define	FIRST_TIME_UP	_rebooting_nodes.contains(_mynodeid)

//
// Description of the CMM algorithm
//
//	PANAMA CANAL ANALOGY:
//
// A set of nodes trying to reconfigure are like a set of ships that
// must pass thru the Panama canal at the same time.
//
// The fist thing they do is buy a ticket (SEQUENCE #).  They must
// all have the same ticket (SEQUENCE #) to be able to pass together.
//
// At the beginning, they all gather at the entrance in the Gulf of
// Mexico (step CMM_BEGIN).  Once all of them have gathered, the latch
// to the first holding area opens.  They move into the 1st area (STEP 1).
// When they are all in STEP 1, the latch between the entrance and
// step 1 closes and the water level adjusts (we run the transition
// programs for step 1).   Now the door between the 1st and 2nd area
// opens and everyone moves to the 2nd area (STEP_2).  The latch between
// holding areas 1 and 2 closes and the water level adjust again
// (sync_with_others and run the transition steps for STEP_2).  Then the
// latch between areas 2 and 3 opens and the ships proceed.  This process
// repeats until everyone gets to the calm of the Pacific (CMM_END).
//

// 	SOME DETAILS:
//
// When the latch between holding area X and X+1 opens, there is a
// small transitionary period where some nodes are in step X and at
// step X+1.  However, no node can go to step X+2 until all nodes get
// to step X+1.
//
// No ship should get isolated from others (no node may get two steps
// behind others).  If it does, something is wrong.  i.e. when a latch
// closes (a sync_with_others returns success), no ship should still be
// in the previous step.
//
// If a ship gets isolated, or a new ship shows up at the entrance,
// everyone needs to go back to the entrance and use the new ticket
// (CMM_BEGIN state with a new SEQ # = prev SEQ # + 1).  As soon as a
// ship detects such a condition it changes its state to CMM_RETURN and
// starts to head back.  It does not wait for any other ship -- the
// ships do not necessarily head back together.  Remember that they
// won't have a new ticket (SEQ #) until they get to the entrance.
//

//	RULES OF RECONFIGURATION:
//
// The following represent allowable states in which no node is assumed to
// be in a bad state:
//
// 1) All node have the same sequence number N, and are in step X or X+1.
//
// 2) A subset of nodes are coming up.  They are in step CMM_BEGIN.  Their
//    seq # may be uninitialized.
//
// 3) A subset of nodes have sequence number N+1 and are in state CMM_BEGIN.
//    These nodes have detected a recent state change and are trying to
//    initiate a reconfiguration.
//
// 3) A subset of nodes have sequence number N and are in state CMM_RETURN.
//    These nodes have detected a recent state change and are trying to
//    go back to state CMM_BEGIN, where they will initiate a reconfiguration.
//
// Any node not in one of the above states is considered to have a bad/stale
// state and is expected to abort.
//
// -- This algorithm is not pro-active about shutting down other nodes with
//	stale sequence numbers.  In other words, when node A receives a
//	message from a node B with a stale sequence number, node A does
//	not force node B to abort.  Node A broadcasts its state to node B
//	and allows node B to shut itself down.  Being pro-active has the
//	disadvantage of sometimes erroneously downing other nodes.
//
// -- This algorithm is aggressive about trying to re-configure new nodes
//	into the cluster as soon as possible.  When it is detected that
//	a new node wants to join the cluster, we try to initiate a new
//	re-configuration as soon as possible.  This even includes aborting
//	re-configurations which are currently in progress (however, the
//	current reconfiguration step is allowed to complete).  The limiting
//	factor is the rate at which nodes can die and come back up.  Bad
//	cluster software can cause infinite re-configurations.
//
// The following describes the specific action that should be taken when
// a node receives an incoming message from another node (the discussion
// here is limited to the detection of a stale state):

//
// seqnum --> sequence number of the node who sent the message
// myseq --> sequence number of the node receiving the message
// myseq and state are both changed atomically.  Seqnum is incremented
//    and the state is set to CMM_BEGIN.
// While in state CMM_BEGIN, additional reconfiguration requests are ignored.
//
// 1) When a node starts up, set myseq = seqnum and go on.
//    Note, myseq may still be incorrect if the incoming message was from
//    a node with a stale seqnum.
//
// 2) a) if seqnum > myseq +1  and local node was part of previous membership
//	 but other node was not, then sending node has a stale sequence number.
//	 local node will send its state information to remote node and
//	 remote node will abort itself based on clause 2 b.
//
//    b) if myseq > seqnum - 1  and local was NOT part of previous membership
//	 but other node is part of previous membership, then local
//	 node has a stale sequence number. Abort local node.
//
// 3) if seqnum == myseq + 1 and other in CMM_BEGIN.
//    Someone detected an unstable membership and is trying to re-configure.
//    Set myseq = seqnum and goto CMM_BEGIN state.
//
// 4) if seqnum == myseq + 1 and other NOT in CMM_BEGIN.
//    My state is stale. Abort node.
//
// 5) if seqnum == myseq, and other is 2 or more steps ahead, Abort node.
//    Note, this excludes the CMM_RETURN state, which implies that whoever is
//    in CMM_RETURN state is going back to re-configure.
//
// 6) if seqnum == myseq - 1, and I'm in CMM_BEGIN.  It's ok.  The other
//    node probably does not know that a new re-configuration is starting.
//    I'll send it my state.  It will catch up as soon ..
//
// 7) If the sending node is not part of the MEMBERSHIPSET, and it is "up"
//    (i.e. its state is not unknown, down, not configured, ...)
//    go back and re-configure.
//
// 8) Otherwise, send your state to the node.  That node should detect
//    that its state is stale and abort.  Some examples are:
//	if seqnum < myseq - 1.  This is the likely fate of nodes that
//		have been isolated from the network for a while.
//	if seqnum == myseq - 1, and I'm not about to re-configure.
//	if seqnum == myseq, and I'm 2 or more steps ahead.
//

//
// 	POSSIBLE CAUSES OF A RE-CONFIGURATION:
//
// One can be reconfiguring for one of three reasons:
//
// Node startup.  Start with sequence number 0, and keep updating your
// sequence number until the initial sync in CMM_BEGIN succeeds.
//
// You are the "initiator".  You detected a state change or were asked to
// reconfigure by an outside entity (e.g. reconf_ener) and initiated a
// re-config.  Increment your sequence number and broadcast your state.
// NOTE:  It's perfectly fine to have more that one initiator.
//
// You are not the initiator.  You were forced to reconfigure because you
// received a message from a node that is at a higher sequence number.
// Update the sequence number to match the received message and broadcast
// your state.
//
// The bitmask "_rebooting_nodes" is used to distinguish nodes in the start-up
// case.  "initiator" or not is not explicitly distinguished in the code.
// But rather, we maintain the "max_reconfig_seqnum" of all the nodes.
// If max_reconfig_seqnum <= MYSEQNUM, then we increment MYSEQNUM.
// Otherwise, we must have been forced to reconfigure due to a message
// from a node with a higher sequence number.  In this case we just set
// MYSEQNUM to max_reconfig_seqnum.
//

//	AUTOMATON STATE VARIABLES:
//
// _state_lock:
//	Serializes access to all automaton's state variables.
//
// _state_change_cv
//	Condition variable used by threads to wait for a change in the state
//	variables.
//
// state_vector[]
//	_nodes[X].state_vector[Y] represents the most recent information that
// 	node X has about node Y.
//
// 	The state in _nodes[X].state_vector[Y] on node Z represents the most
//	recent information about node Y that node X sent to node Z.
//
// membership
// 	_nodes[i].membership contains the current cluster membership as proposed
// 	by node i. The memberships must converge before the nodes can initiate
// 	the reconfiguration steps.
//
// _membership
// 	_membership stores cluster membership established before step1 of
// 	the most recent cluster reconfiguration. This is necessary because
// 	the incarnation numbers may change when nodes attempt to join the
//	cluster.
//
// reconfig_seqnum
// 	_nodes[i].reconfig_seqnum contains the most recent reconfiguration
//	sequence number used by node i.
//
// 	Reconfiguration sequence numbers are used to prevent old messages
// 	from interfering with the reconfiguration protocol. The current
//	sequence number is passed with every message. The receiver of the
//	message compares the number with its reconfiguration number and will
//	abort the current reconfiguration procedure if the two numbers don't
//	match. The nodes synchronize the values of their reconfiguration
//	numbers when the nodes are in CMM_BEGIN.
//
// 	Note that besides using the sequence numbers internally to identify old
// 	messages, the sequence numbers are passed to the cluster application
// 	programs through the cm_getstate() API call. In order to guarantee
// 	that the sequence numbers are monotonic (per the API), each node
// 	maintains the most recently used sequence number value in stable
//	storage. THIS IS NOT DONE CURRENTLY. THE VALUE IS INITIALIZED TO
//	ZERO DURING EACH CLUSTER INSTANTIATION.
//
// _max_reconfig_seqnum
//	The highest sequence number currently in use in the system as seen by
//	the local node. Refer to the earlier discussion on how it is used in
//	maintaining sequence numbers.
//

// _current_step
// 	_current_step stores the current step number during the cluster
// 	reconfiguration procedure. It is not used internally by the automaton
// 	and it is maintained only for the getcluststate inquiry.
//
// _rebooting_nodes
//	A bitmask which indicates the set of nodes trying to join the cluster
//	after being in the down state for some time.  A rebooting node's
//	sequence number is expected to be stale.  The node is exempt from
// 	the typical sequence number checks (i.e. it won't be aborted) until
// 	its sequence number is updated. The rebooting state is cleared
//	when the node finds that the cluster has reached operational quorum.
//
// heard_from
//	This is a bitmask used to keep track of the connectivity
//	matrix during a reconfiguration. _nodes[i].heard_from is a bitmask
//	received from node i that represents the nodes that node i has
//	received status messages from during the current reconfiguration.
//	The array is reset at the beginning of each reconfiguration.
//	This array is used to check if every member of the cluster
//	is connected to every member. If there are disconnects, this
//	array is used to decide which nodes should be halted to achieve
//	full connectivity in the remaining cluster.
//
// join_occurred
//	This is a bitmask which is used to keep track of the nodes which
//	are joining the cluster. This information is used in the begin_state
//	to identify nodes which are joining. Then the join_occurred bitmask
//	is cleared in the connectivity state.
//
//
// FORCE_RECONFIGURATION
// 	This flag is set when the reconfigure method of the CMM control
//	interface is invoked. This will force the transitions_thread to
// 	execute the "return" transition.
//
// STOP_LOCAL_NODE
//	A flag that is set when the cluster monitor is requested to stop.
// 	Once this is set, it cannot be revoked.
//
// NODE_IS_ABORTING
//	A flag that is set when the abort transition is initiated.
// 	The automaton ignores incoming messages during the abort transition.
//

//
// Ring buffer to store CMM traces.
//
#ifdef	CMM_DBGBUF
uint32_t cmm_dbg_size = 64 * 1024;
dbg_print_buf	cmm_dbg_buf(cmm_dbg_size);
#endif

cmm_automaton_event_t
automaton_impl::state_machine_invalid_state()
{
	ASSERT(0);
	return (CMM_INVALID_EVENT);
}

//
// Automaton State "Start"
//
cmm_automaton_event_t
automaton_impl::state_machine_start_state()
{
	_state_lock.lock();
	check_stop_abort();
	MYSTATE = CMM_START;
	MYMEMBERSHIPSET.set(row_to_live_nodes(_mynodeid));
	_dump_state(1);
	broadcast_msg();
	_state_lock.unlock();

	//
	// Create the boot_delay thread with the lowest CMM thread priority.
	//
	if (!cmm_impl::cmm_create_thread(boot_delay_thread, this,
		CMM_MAXPRI-3)) {
		//
		// SCMSGS
		// @explanation
		// The CMM was unable to create its specified thread and the
		// system can not continue. This is caused by inadequate
		// memory on the system.
		// @user_action
		// Add more memory to the system. If that does not resolve the
		// problem, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Unable to create %s thread.", "boot delay");
	}
	return (CMM_PROCEED);
}

//
// Automaton State "Begin"
//
// Synchronize with other nodes before a cluster reconfiguration, and
// establish the cluster membership for the reconfiguration.  The state
// will complete only when the cluster becomes stable.
//
cmm_automaton_event_t
automaton_impl::state_machine_begin_state()
{
	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_1,
	    FaultFunctions::generic);

	Environment e;
	swo_stat_t	state;
	cmm_automaton_event_t retval = CMM_STABLE;

	_state_lock.lock();
	_primary = NODEID_UNKNOWN;

	MYSTATE = CMM_BEGIN;
	_current_step = 0;
	_automaton_flags &= ~FORCE_RECONFIGURATION;
	_dump_state(1);

	_nodes[_mynodeid].join_occurred.add_node(_mynodeid);
	_disagree_join_set.empty();

	//
	// Increment our sequence number and clear
	// connectivity matrix.
	//
	incr_seqnum();

	//
	// Set the CMM version.
	//
	set_version();

	//
	// Check to see if there is an intent for cluster shutdown.
	// If so, then this is committed.
	// There is an intent for cluster shutdown if
	// _cluster_shutdown_intent is set.
	// cluster shutdown is committed if _cluster_shutdown is set.
	//
	if (cmm_impl::the().get_cluster_shutdown_intent()) {
		CMM_TRACE(("Shutdown intent set, commiting now..\n"));
		cmm_impl::the().cluster_shutdown_commit();
	}
	refresh_begin_state();

	while ((state = sync_with_others_in_begin()) == SWO_UNSTABLE) {
		CMM_DEBUG_TRACE(("begin_state - unstable\n"));
		_state_change_cv.wait(&_state_lock);
		refresh_begin_state();
	}

	if (state == SWO_FORCE) {
		//
		// Got a node_is_down in begin state or
		// the FORCE_RECONFIGURATION flag is set.
		// CMM reconfiguration needs to be restarted.
		// We do not directly return from the if block,
		// because we want to give precedence to
		// cluster shutdown.
		//
		CMM_DEBUG_TRACE(("begin_state - got SWO_FORCE\n"));
		retval = CMM_UNSTABLE;
	}

	//
	// Clear the membership change flag.
	// We do this because we want to detect membership change
	// from this point.
	//
	_membership_change_flag = false;

	//
	// If the intent flag was set when we were already in the begin
	// state, we commit the shutdown in the receive_msg function.
	//
	if (cmm_impl::the().get_cluster_shutdown()) {
		retval = CMM_CLUSTER_SHUTDOWN;
		CMM_TRACE(("cluster shutdown has been set\n"));
	}

#ifdef	_KERNEL_ORB

	if (((disagreements_in_join_set()) || (let_partition_wait())) &&
	    (retval == CMM_STABLE)) {
		//
		// There are unresolved connectivity issues.
		// Or, there is a smaller partition which
		// needs to wait for the other partition before
		// proceeding.
		//

		// NOTE: Since the detection delay could vary from node to
		// node depending on types of transport, the maximum used
		// on this node may be different from the maximum on other
		// nodes. Variations in this could result in a down node
		// being considered disconnected and live nodes being
		// pruned out. Since the probability of this is very low,
		// we ignore this.
		//
		// max_detection_delay is in milliseconds.
		//
		os::usec_t	max_detection_delay = 1000;

		os::systime	sys_timeout;

		for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
			os::usec_t detection_delay =
			    (os::usec_t)pm_get_max_delay_funcp(i);
			if (detection_delay > max_detection_delay) {
				max_detection_delay = detection_delay;
			}
		}

		//
		// BugId: 4291140. The argument to setreltime() below is of
		// type os::usec_t which happens to be typedef'ed to clock_t
		// which in turn is typedef'ed to long.
		// The max number that can be represented by a long is
		// LONG_MAX. This implies that the max value that
		// can be allowed for max_detecion_delay is LONG_MAX/1000.
		//
		if (max_detection_delay > LONG_MAX/1000) {
			//
			// SCMSGS
			// @explanation
			// The maximum of the node down detection delays is
			// larger than the allowable maximum. The maximum
			// allowed will be used as the actual maximum in this
			// case.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_WARNING,
			    MESSAGE, "CMM: Max detection delay specified is "
			    "%ld which is larger than the max allowed %ld.",
			    max_detection_delay, LONG_MAX/1000);
			max_detection_delay = LONG_MAX/1000;
		}

		CMM_TRACE(("Disagreements in begin_state. "
		    "Waiting for %d millisecs.\n",
		    max_detection_delay));

		sys_timeout.setreltime(max_detection_delay *
		    (MICROSEC / MILLISEC));

		//
		// path_timeout is in seconds. If the path_timeout is
		// less than 1 second then set it to 1 second. Since
		// max_detection_delay is in milliseconds, hence dividing
		// by MILLISEC gives the result in seconds.
		//
		int path_timeout = (int)(max_detection_delay / MILLISEC);
		if (path_timeout < 1) {
			//
			// Minimum timeout should be set at 1 sec
			//
			path_timeout = 1;
		}

		//
		// In one second interval check for membership
		// changes till a maximum time period of
		// the path manager timeout.
		//
		for (int i = 1; i <= path_timeout; i++) {
			//
			// Leave the lock
			//
			_state_lock.unlock();

			CMM_TRACE(("begin_state - sleeping for 1 sec\n"));

			//
			// Sleep for 1 second
			//
			os::usecsleep((os::usec_t)1000000);

			//
			// Grab the lock
			//
			_state_lock.lock();

			//
			// Check if membership has changed
			// while we were sleeping.
			//
			if (_membership_change_flag) {
				//
				// Membership has changed. The cluster has
				// become unstable.
				// Restart the cmm reconfiguration.
				//
				CMM_TRACE(("begin_state - "
				    "Membership has become unstable\n"));
				retval = CMM_UNSTABLE;
				check_stop_abort();
				break;
			}
		} // End of for loop

		//
		// If retval still stays set to CMM_STABLE then the
		// following comment descibes the scenario we are in.
		//
		// At this point, we have a STABLE membership and
		// remaining disconnects (if any) are valid and will
		// be resolved in the connectivity state.
		//
		// We waited long enough so that we know the
		// non connected paths will not transition to up.
		//
		// We can transition to connectivity state if cluster
		// shutdown is not in progress.
		//

	}

#endif	// _KERNEL_ORB

	//
	// Check if our node is healthy.
	// If our node is not seeing any other nodes having non-zero
	// vote count, then the health of our node should be checked.
	// If our node is unhealthy, we should bring down our node
	// to prevent it from taking other nodes down.
	//
	// If we reach here, it means the local node is proceeding
	// with its reconfiguration beyond the begin state.
	// The state sync has happened already for begin state
	// and so we have waited enough to allow other nodes
	// booting up to contact us. So at this point,
	// we can determine whether we are not seeing any nodes
	// that have non-zero vote counts.
	//
	check_node_health();

	_state_lock.unlock();

	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_BEGIN_STATE_2,
	    FaultFunctions::generic);

	return (retval);
}

//
// Automaton State "Connectivity"
//
// In the connectivity state we check that full connectivity
// exists between all the nodes of the cluster.
// If the cluster becomes "unstable" while waiting for all nodes to
// reach this state, the function returns CMM_UNSTABLE
//
cmm_automaton_event_t
automaton_impl::state_machine_connectivity_state()
{
	Environment		e;
	swo_stat_t		state;

	_state_lock.lock();

	ASSERT(is_previous(CMM_CONNECTIVITY, MYSTATE));
	MYSTATE = CMM_CONNECTIVITY;
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	_dump_state(1);

	//
	// Print the cluster membership to cmm debug buffer
	//
	CMM_TRACE(("connectivity state: membership = 0x%llx\n",
		_nodes[_mynodeid].membership.bitmask()));

	//
	// Print the connectivity matrix to cmm debug buffer
	//
	CMM_TRACE(("Connectivity matrix while in connectivity_state:\n"));
	for (int i = 1; i <= NODEID_MAX; i++) {
		if (_nodes[i].heard_from.bitmask() != 0) {
			CMM_TRACE(("\theard_from[%ld] : 0x%llx\n",
			    i, _nodes[i].heard_from.bitmask()));
		}
	}

	broadcast_msg();

	while ((state = sync_with_others()) == SWO_WAIT) {
		_state_change_cv.wait(&_state_lock);
		check_stop_abort();
	}

	if (state == SWO_UNSTABLE) {
		_state_lock.unlock();
		return (CMM_UNSTABLE);
	}

	//
	// Clear the join_occurred
	//
	for (int i = 1; i <= NODEID_MAX; i++) {
		_nodes[i].join_occurred.empty();
	}

	//
	// Check if all live nodes are able to communicate with one
	// another. It is possible that the local node is selected
	// to be aborted if it is disconnected from another node.
	//
	check_connectivity();
	check_stop_abort();
	//
	// Since some nodes may have been declared down due to lack
	// of full connectivity, we must update our membership set
	// and broadcast the new state.
	//
	MYMEMBERSHIPSET.set(row_to_live_nodes(_mynodeid));
	broadcast_msg();

	_state_lock.unlock();

	return (CMM_STABLE);
}

//
// Common code shared by quorum_acquisition and quorum_acquisition_shutdown
// states. This is used only for fault injection.
//
#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
void
automaton_impl::quorum_acquisition_common()
{
	// Fault point will introduce (# of up and down nodes * 30) sec. delay.
	void		*fault_argp;
	uint32_t	fault_argsize;
	nodeid_t	node;
	uint_t num_up_nodes = 0;	// # nodes in current partition
	uint_t num_down_nodes = 0;	// # nodes potentially split brained

	//
	// We expect all the nodes are in the cluster at this moment
	// for the test purpose.
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		if (MYMEMBERSHIPSET.contains(node)) {
			// node is in current membership
			num_up_nodes++;
		}

		if (STATEVEC[node] == S_DOWN) {
			// node is considered down (not dead)
			num_down_nodes++;
		}
	}

	if (fault_triggered(
	    FAULTNUM_CMM_QUORUM_DEVICE_DELAY,
	    &fault_argp, &fault_argsize)) {
		os::printf("==> Fault Injection introduces %d sec. delay\n",
			(num_up_nodes + num_down_nodes) * 30);
		os::usecsleep((os::usec_t)
			(num_up_nodes + num_down_nodes) * 30 * 1000000);
	}

}
#endif


//
// Automaton State "Quorum Acquisition"
// In this state, we acquire the quorum device.
// If the cluster becomes "unstable" while waiting for all nodes to
// reach this state, the function returns CMM_UNSTABLE
//
cmm_automaton_event_t
automaton_impl::state_machine_quorum_acquisition_state()
{
	Environment		e;
	swo_stat_t		state;

	_state_lock.lock();

	ASSERT(is_previous(CMM_QACQUISITION, MYSTATE));
	MYSTATE = CMM_QACQUISITION;
	ASSERT(!cmm_impl::the().get_cluster_shutdown());
	_dump_state(1);

	broadcast_msg();

	while ((state = sync_with_others()) == SWO_WAIT) {
		_state_change_cv.wait(&_state_lock);
		check_stop_abort();
	}

	if (state == SWO_UNSTABLE) {
		_state_lock.unlock();
		return (CMM_UNSTABLE);
	}
	//
	// In case of split brains, allow larger partitions to win
	// the race for quorum devices.
	//
	delay_in_case_of_split_brain();

	_state_lock.unlock();

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)

	quorum_acquisition_common();

#endif // FAULT_CMM && _KERNEL_ORB

	if (!CORBA::is_nil(_quorum_p)) {
		_quorum_p->acquire_quorum_devices(MYMEMBERSHIPSET.bitmask(),
		    _nodes[_mynodeid].quorum_result[_mynodeid], e);
	} else {
		CMM_DEBUG_TRACE(("_quorum_p is nil\n"));
	}

	return (CMM_STABLE);
}

cmm_automaton_event_t
automaton_impl::state_machine_quorum_acquisition_shutdown_state()
{
	Environment	e;
	swo_stat_t	state;

	_state_lock.lock();

	ASSERT(is_previous(CMM_SQACQUIRE, MYSTATE));
	MYSTATE = CMM_SQACQUIRE;
	_dump_state(1);

	broadcast_msg();

	while ((state = sync_with_others()) == SWO_WAIT) {
		_state_change_cv.wait(&_state_lock);
		check_stop_abort();
	}

	if (state == SWO_UNSTABLE) {
		_state_lock.unlock();
		return (CMM_UNSTABLE);
	}

	_state_lock.unlock();

	if (!CORBA::is_nil(_quorum_p)) {
		_quorum_p->update_membership(MYMEMBERSHIPSET.bitmask(), e);
	} else {
		CMM_DEBUG_TRACE(("_quorum_p is nil\n"));
	}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)

	quorum_acquisition_common();

#endif // FAULT_CMM && _KERNEL_ORB

	return (CMM_STABLE);
}

//
// Automaton State "End"
//
// This state is entered after a successful reconfiguration.
//
// The local node will remain in this state until one of the following
// conditions is detected:
//
//	1. One of the other participants has left the "end" state.
//	2. The cluster membership has become unstable.
//	3. The local node is required to stop, or is aborting.
//	4. The force_reconfiguration flag has been set.
//
cmm_automaton_event_t
automaton_impl::state_machine_end_state()
{
#if defined(_KERNEL_ORB)
	// Take some action at the end of the reconfiguration loop
	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_END_STATE,
	    FaultFunctions::generic);
#endif

	CMM_TRACE(("end state: seq_num = %lld, membership = 0x%llx\n",
		MYSEQNUM, _nodes[_mynodeid].membership.bitmask()));

	_state_lock.lock();
	_primary = NODEID_UNKNOWN;

	while (sync_with_others() != SWO_UNSTABLE) {

#if (SOL_VERSION >= __s10) && defined(_KERNEL)
		// XXX what check to do for cmm old version
		// XXX and when to initialize membership subsystem?

		//
		// Initialize the membership subsystem.
		// This method is idempotent.
		// Furthermore, if we are not running a software version
		// that has membership API support, then this call would not
		// initialize membership subsystem, and return immediately.
		// So we can safely call this method everytime we are in
		// end state, to make sure that we initialize the membership
		// subsystem when required (basically when we start to run
		// the software version supporting membership API.)
		//
		if (membership_subsystem_init()) {
			//
			// Notify membership engine about base cluster
			// membership change
			//
			membership_engine_impl *enginep =
			    membership_engine_impl::the();
			ASSERT(enginep != NULL);
			enginep->automaton_callback(_membership, MYSEQNUM);
		}
#endif

#ifdef	_KERNEL_ORB
		//
		// Call into quorum subsystem to try registering keys
		// on QDs that do not have them, and taking ownership
		// of quorum devices that do not have ownership still.
		//
		Environment env;
		_quorum_p->cmm_reconfig_done(env);
		// cmm_reconfig_done() does not throw any exceptions
		CL_PANIC(env.exception() == NULL);
#endif

		_state_change_cv.wait(&_state_lock);
		check_stop_abort();
	}
	_state_lock.unlock();
	return (CMM_UNSTABLE);
}

//
// Automaton State "Return"
//
// Node transitioning back to Begin state. Informs other members
// about it.
//
cmm_automaton_event_t
automaton_impl::state_machine_return_state()
{
	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_RETURN_STATE_1,
	    FaultFunctions::generic);

	_state_lock.lock();
	check_abort();
	// Note that we must *not* call check_stop_abort() here
	MYSTATE = CMM_RETURN;
	_primary = NODEID_UNKNOWN;
	_dump_state(1);
	broadcast_msg();
	_state_lock.unlock();

	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_RETURN_STATE_2,
	    FaultFunctions::generic);
	return (CMM_PROCEED);
}

//
// Automaton State "Qcheck"
//
// State to check the cluster quorum. Wait for all nodes to reach this
// state before checking quorum. As part of this synchronization,
// local node collects quorum result from other nodes.
// If the cluster has quorum, then the function returns CMM_PROCEED
// and reconfiguration proceeds. If the cluster does not have quorum,
// what we do depends on whether the cluster is just forming or is already
// operational. In the former case, we wait in this state until a state change
// signal is received that requires us to start a new reconfiguration.
// If the cluster previously had quorum, we cause the node to abort
// by returning CMM_ABORT.
//
// If the cluster becomes "unstable" while waiting for all nodes to
// reach this state, the function returns CMM_RECONFIGURE which
// causes a new reconfiguration.
//
cmm_automaton_event_t
automaton_impl::state_machine_qcheck_state()
{
	swo_stat_t		state;
	cmm::membership_t	incarnation_copy;
	cmm_automaton_event_t  	return_value = CMM_PROCEED;
	bool			we_have_quorum = true;
	nodeid_t		i;
	bool 			cluster_shutdown;

	//
	// Allocate the sequence before grabbing the lock since it will do a
	// memory allocation.
	//
	quorum::cluster_quorum_results_t quorum_result_list(NODEID_MAX);

	_state_lock.lock();
	check_stop_abort();

	//
	// Select lowest nodeid node as primary.
	//
	if (_primary == NODEID_UNKNOWN) {
		for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
			if (MYMEMBERSHIPSET.contains(node)) {
				_primary = node;
				break;
			}
		}
		CMM_DEBUG_TRACE(("qcheck_state: new primary nodeid(%ld)\n",
			_primary));
	}
	ASSERT(_primary != NODEID_UNKNOWN);

	ASSERT(is_previous(CMM_QCHECK, MYSTATE));
	MYSTATE = CMM_QCHECK;
	_dump_state(1);
	//
	// If I am joining store my initial view number
	//
	if (MYIVN == INCN_UNKNOWN) {
		MYIVN = MYSEQNUM;
	}

	//
	// If I am not primary, I notify I am ready to synchronize
	// on the current step.
	//
	if ((_primary != NODEID_UNKNOWN) && (_primary != _mynodeid)) {
		send_msg(_primary);
	} else {
		//
		// Primary node does not send any message yet.
		//
	}

	while ((state = sync_with_others()) == SWO_WAIT) {
		_state_change_cv.wait(&_state_lock);
		check_stop_abort();
	}

	if (state == SWO_UNSTABLE) {
		_primary = NODEID_UNKNOWN;
		_state_lock.unlock();
		return (CMM_RECONFIGURE);
	} else {
		//
		// If I am primary I trigger the synchronization
		// for the current step.
		//
		if (_primary == _mynodeid) {
			broadcast_msg();
		} else {
			//
			// Non primary nodes do not send here.
			//
		}
	}

	//
	// Before releasing the state lock, save a copy of the incarnation
	// numbers of the nodes. If we proceed to step 1, we will use the
	// saved incarnation numbers to report to the clients. This is
	// necessary because the node incarnations could be updated by
	// incoming messages.
	//
	for (i = 1; i <= NODEID_MAX; i++) {
		incarnation_copy.members[i] = _nodes[i].incarnation;
	}

	_state_lock.unlock();
	cluster_shutdown = cmm_impl::the().get_cluster_shutdown();

	//
	// All nodes have reached the qcheck state. Now, we must check the
	// cluster quorum. This step is skipped during cluster shutdown.
	// We always pretend to have quorum during shutdown. This is part
	// of the fix for 5068618 that prevents nodes from panic due to
	// loss of quorum during shutdown.
	//

	if (!cluster_shutdown && !CORBA::is_nil(_quorum_p)) {
		uint_t		listnum = 0;
		Environment	e;

		//
		// Build the sequence of quorum results from the current
		// members of the cluster.
		//
		for (i = 1; i <= NODEID_MAX; i++) {
			if (MYMEMBERSHIPSET.contains(i)) {
				quorum_result_list[listnum++] =
					_nodes[i].quorum_result[i];
			}
		}
		quorum_result_list.length(listnum);
		we_have_quorum =
			_quorum_p->check_cluster_quorum(quorum_result_list, e);
	} else if (cluster_shutdown) {
		return_value = CMM_CLUSTER_SHUTDOWN;
		CMM_TRACE(("Shutdown in progress, Assuming quorum\n"));
	}

	if (we_have_quorum) {

		_state_lock.lock();

		// If we are fencing off down nodes, mark the fact that we
		// have acquired quorum so that the down nodes can be safely
		// declared dead when the respective timers expire.
		//
		for (i = 1; i <= NODEID_MAX; i++) {
			if (_nodes[i].fenceoff_timer != 0) {
				_nodes[i].must_reacquire_quorum = false;
			}
		}

		if (FIRST_TIME_UP) {
			_rebooting_nodes.empty();
			CMM_TRACE(("cluster has reached quorum\n"));
			//
			// SCMSGS
			// @explanation
			// Enough nodes are operational to obtain a majority
			// quorum; the cluster is now moving into operational
			// state.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_NOTICE,
			    MESSAGE, "CMM: Cluster has reached quorum.");
		}

		_state_lock.unlock();

		if (return_value != CMM_CLUSTER_SHUTDOWN) {
			return_value = CMM_PROCEED;
		}

		char *nodenm = new char [CL_MAX_LEN+1];

		//
		// Store the incarnation numbers of the current cluster
		// members in _membership. Also, log a message for each
		// membership change.
		//
		for (i = 1; i <= NODEID_MAX; i++) {
			incarnation_num		incn;

			incn = MYMEMBERSHIPSET.contains(i) ?
				incarnation_copy.members[i] : INCN_UNKNOWN;
			if (incn != _membership.members[i]) {

				clconf_get_nodename(i, nodenm);

				if (incn == INCN_UNKNOWN) {
					//
					// SCMSGS
					// @explanation
					// The specified node has gone down in
					// that communication with it has been
					// lost.
					// @user_action
					// The cause of the failure should be
					// resolved and the node should be
					// rebooted if node failure is
					// unexpected.
					//
					(void) _nodes[i].msgp->log(
					    SC_SYSLOG_NOTICE, STATE_CHANGED,
					    "CMM: Node %s (nodeid = %ld) is "
					    "down.", nodenm, i);

					//
					// publish sysevent but not during
					// shutdown to avoid inter-node
					// communication.
					//
					if (cluster_shutdown) {
						_membership.members[i] = incn;
						//
						// If the node is not member
						// reset its initial view number
						//
						if (_membership.members[i]
						    == INCN_UNKNOWN) {
							_nodes[i].
							    initial_view_number
							    = INCN_UNKNOWN;
						}
						continue;
					}

					(void) sc_publish_event(
					    ESC_CLUSTER_NODE_STATE_CHANGE,
					    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_NODE_NAME, SE_DATA_TYPE_STRING,
					    nodenm,
					    CL_REASON_CODE,
					    SE_DATA_TYPE_UINT32,
					    CL_REASON_CMM_NODE_LEFT,
					    NULL);
				} else {
					ASSERT(!cluster_shutdown);
					//
					// SCMSGS
					// @explanation
					// The specified node has come up and
					// joined the cluster. A node is
					// assigned a unique incarnation
					// number each time it boots up.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) _nodes[i].msgp->log(
					    SC_SYSLOG_NOTICE, STATE_CHANGED,
					    "CMM: Node %s (nodeid = %ld) is "
					    "up; new incarnation number "
					    "= %ld.", nodenm, i, incn);

					// publish sysevent
					(void) sc_publish_event(
					    ESC_CLUSTER_NODE_STATE_CHANGE,
					    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_NODE_NAME, SE_DATA_TYPE_STRING,
					    nodenm,
					    CL_REASON_CODE,
					    SE_DATA_TYPE_UINT32,
					    CL_REASON_CMM_NODE_JOINED,
					    NULL);
				}
			}
			_membership.members[i] = incn;

			//
			// If the node is not member
			// reset its initial view number
			//
			if (_membership.members[i] == INCN_UNKNOWN) {
				_nodes[i].initial_view_number = INCN_UNKNOWN;
			}
		}
		delete [] nodenm;

	} else if (FIRST_TIME_UP) {
		ASSERT(!cluster_shutdown);
		//
		// Cluster is forming and we don't have quorum yet.
		// Wait for a state change and cause a reconfiguration if
		// membership is unstable.
		//
		CMM_TRACE(("cluster doesn't have operational quorum yet; "
			"waiting for quorum\n"));
		//
		// SCMSGS
		// @explanation
		// Not enough nodes are operational to obtain a majority
		// quorum; the cluster is waiting for more nodes before
		// starting.
		// @user_action
		// If nodes are booting, wait for them to finish booting and
		// join the cluster. Boot nodes that are down.
		//
		(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "CMM: Cluster doesn't have operational quorum yet; "
		    "waiting for quorum.");

		_state_lock.lock();
		_primary = NODEID_UNKNOWN;
		while (sync_with_others() != SWO_UNSTABLE) {
			_state_change_cv.wait(&_state_lock);
			check_stop_abort();
			broadcast_msg();
		}
		_state_lock.unlock();

		return_value = CMM_RECONFIGURE;

	} else {
		//
		// The cluster had quorum before. We just lost it.
		// The cluster needs to abort.
		//
		CMM_TRACE(("cluster lost operational quorum; aborting\n"));
		FAULTPT_KCMM(FAULTNUM_CMM_STOP_ABORT, FaultFunctions::generic);
		//
		// SCMSGS
		// @explanation
		// Not enough nodes are operational to maintain a majority
		// quorum, causing the cluster to fail to avoid a potential
		// split brain.
		// @user_action
		// The nodes should rebooted.
		//
		(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Cluster lost operational quorum; aborting.");
		_state_lock.lock();
		_primary = NODEID_UNKNOWN;
		_state_lock.unlock();
		return_value = CMM_ABORT;
	}
	return (return_value);
}

//
// Automaton State "Step"
//
// Called before the local node executes the "step(N)" transition.
// The local node sets its state to step(n) and waits until all
// other nodes have reached step(n). If the cluster membership
// becomes unstable, state returns "CMM_UNSTABLE".
//
cmm_automaton_event_t
automaton_impl::state_machine_step_state()
{
	//
	// _current_step stores the current synchronization step number.
	// It is used only by getclusterstate(). Number should match
	// current cmm callback number.
	//
	// MYSTATE stores the current CMM automaton state.
	//
	//
	// Get current step number from cmm_impl.
	//
	// At this point MYSTATE still corresponds to
	// the previous automaton state. As we are not yet
	// in sync with the rest of the nodes for the current
	// state.
	//
	// If we are called as part of step N, then
	// MYSTATE matches state for step N-1.
	//
	uint_t step = cmm_impl::the_cmm_impl->_current_step;

	cmm::membership_t& curr_membership =
		cmm_impl::the_cmm_impl->_current_mbrs;
	cmm::seqnum_t& seqnum = cmm_impl::the_cmm_impl->_current_seqnum;

	swo_stat_t	state;
	cmm_automaton_event_t		retval = CMM_UNSTABLE;

	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_STEP_STATE_1,
	    FaultFunctions::generic);

	_state_lock.lock();
	//
	// If the previous step failed, the callback mechanism would
	// have requested a reconfiguration or an abort transition
	// (depending on whether the problem can be recovered from or
	// not). In that case, we should not go on to the next state.
	//
	check_stop_abort();
	if (sync_with_others() != SWO_UNSTABLE) {

		MYSTATE = (uint16_t)cmm_impl::the_cmm_impl->get_current_state();
		_dump_state(1);

		//
		// If I am not primary and a primary exists, I notify I am
		// ready to synchronize on the current step. Otherwise
		// primary node does not send any message yet.
		//
		if ((_primary != NODEID_UNKNOWN) && (_primary != _mynodeid)) {
			send_msg(_primary);
		}

		while ((state = sync_with_others()) == SWO_WAIT) {
			_state_change_cv.wait(&_state_lock);
			check_stop_abort();
		}

		if (state == SWO_SYNC) {
			retval = CMM_STABLE;

			//
			// Update _current_step.
			// Meaningful only for getcluststate().
			//
			_current_step = step;

			curr_membership = _membership;
			seqnum  = MYSEQNUM;

			//
			// If I am primary I trigger the synchronization
			// for the current step.
			//
			if (_primary == _mynodeid) {
				broadcast_msg();
			} else {
				//
				// Non primary nodes do not send here.
				//
			}
		} else {
			_primary = NODEID_UNKNOWN;
		}
	}

#ifdef CMM_VERSION_0
	//
	// If we are executing the old set of steps, we have to transition
	// to CMM_END_SYNC state after step 11.
	//
	if ((step == (CMM_STEP_11 - CMM_STEP_1 + 1)) &&
	    cmm_impl::the().is_old_version() && (retval == CMM_STABLE)) {
		CMM_TRACE(("Returning old steps event\n"));
		retval = CMM_OLD_STEPS_EVENT;
	}
#endif // CMM_VERSION_0

	_state_lock.unlock();

	FAULTPT_KCMM(FAULTNUM_CMM_AUTOMATON_STEP_STATE_2,
	    FaultFunctions::generic);

	return (retval);
}

//
// State machine initilization.
//
void
automaton_impl::initialize_machine(uint_t step_number)
{
	ASSERT(step_number <= CMM_MAX_TRANSITION_NUM);
	//
	// Attach state handlers.
	//
	cmm_handlers[CMM_START] = &automaton_impl::state_machine_start_state;
	cmm_handlers[CMM_BEGIN] = &automaton_impl::state_machine_begin_state;
	cmm_handlers[CMM_CONNECTIVITY] =
	    &automaton_impl::state_machine_connectivity_state;
	cmm_handlers[CMM_QACQUISITION] =
	    &automaton_impl::state_machine_quorum_acquisition_state;
	cmm_handlers[CMM_QCHECK] = &automaton_impl::state_machine_qcheck_state;
	cmm_handlers[CMM_STEP_1] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_2] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_3] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_4] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_5] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_6] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_7] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_8] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_9] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_10] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_11] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_12] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_STEP_13] = &automaton_impl::state_machine_step_state;

#ifdef _KERNEL_ORB
	cmm_handlers[CMM_SQACQUIRE] =
	    &automaton_impl::state_machine_quorum_acquisition_shutdown_state;
	cmm_handlers[CMM_SHUTDOWN_STEP_1] =
	    &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_SHUTDOWN_STEP_2] =
	    &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_SHUTDOWN_STEP_3] =
	    &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_SHUTDOWN_STEP_4] =
	    &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_SHUTDOWN_STEP_5] =
	    &automaton_impl::state_machine_step_state;
#endif

	cmm_handlers[CMM_END_SYNC] = &automaton_impl::state_machine_step_state;
	cmm_handlers[CMM_END_WAIT] = &automaton_impl::state_machine_end_state;
	cmm_handlers[CMM_RETURN] = &automaton_impl::state_machine_return_state;
	cmm_handlers[CMM_INVALID_STATE] =
		&automaton_impl::state_machine_invalid_state;

	//
	// Mark all state/event transition as invalid.
	//
	for (int state  = CMM_FIRST_STATE; state <= CMM_LAST_STATE; state++) {
		for (int event = CMM_FIRST_EVENT; event <= CMM_LAST_EVENT;
			event++) {
			cmm_transition[state][event] = CMM_INVALID_STATE;
		}
	}
	//
	// Then define valid transitions from:
	//
	// start
	//
	cmm_transition[CMM_START][CMM_PROCEED] = CMM_BEGIN;
	//
	// begin
	//
	cmm_transition[CMM_BEGIN][CMM_STABLE] = CMM_CONNECTIVITY;
	cmm_transition[CMM_BEGIN][CMM_UNSTABLE] = CMM_RETURN;
	//
	// connectivity
	//
	cmm_transition[CMM_CONNECTIVITY][CMM_STABLE] = CMM_QACQUISITION;
	cmm_transition[CMM_CONNECTIVITY][CMM_UNSTABLE] = CMM_RETURN;
	//
	// quorum acquisition
	//
	cmm_transition[CMM_QACQUISITION][CMM_STABLE] = CMM_QCHECK;
	cmm_transition[CMM_QACQUISITION][CMM_UNSTABLE] = CMM_RETURN;
	//
	// qcheck
	//
	cmm_transition[CMM_QCHECK][CMM_PROCEED] = CMM_STEP_1;
	cmm_transition[CMM_QCHECK][CMM_RECONFIGURE] = CMM_RETURN;

#ifdef _KERNEL_ORB
	//
	// begin during shutdown.
	//
	cmm_transition[CMM_BEGIN][CMM_CLUSTER_SHUTDOWN] =
	    CMM_SQACQUIRE;
	//
	//  quorum acquisition during shutdown.
	//
	cmm_transition[CMM_SQACQUIRE][CMM_STABLE] =
	    CMM_QCHECK;
	cmm_transition[CMM_SQACQUIRE][CMM_UNSTABLE] =
	    CMM_RETURN;
	//
	// qcheck during shutdown.
	//
	cmm_transition[CMM_QCHECK][CMM_CLUSTER_SHUTDOWN]
	    = CMM_SHUTDOWN_STEP_1;
	//
	// Initialize CMM step states transition table. For Kernel CMM,
	// initializing shutdown step states transition table is handled
	// later and therefore need to be excluded.
	// step_number is the number of ORB step states.
	//
	last_step = CMM_STEP_1 + (cmm_automaton_state_t)step_number -
	    NUM_SHUTDOWN_STATES - 1;
	//
	// shutdown step states, step 1 to NUM_SHUTDOWN_STATES.
	//
	for (int state = CMM_SHUTDOWN_STEP_1; state < CMM_SHUTDOWN_STEP_5;
	    state++) {
		cmm_transition[state][CMM_STABLE] = (cmm_automaton_state_t)
		    (state + 1);
		cmm_transition[state][CMM_UNSTABLE] = CMM_RETURN;
	}
	cmm_transition[CMM_SHUTDOWN_STEP_5][CMM_STABLE] = CMM_END_SYNC;
	cmm_transition[CMM_SHUTDOWN_STEP_5][CMM_UNSTABLE] = CMM_RETURN;
#else
	last_step = CMM_STEP_1 + (cmm_automaton_state_t)step_number - 1;
#endif
	for (int state = CMM_STEP_1; state < last_step; state++) {
		cmm_transition[state][CMM_STABLE] = (cmm_automaton_state_t)
		    (state + 1);
		cmm_transition[state][CMM_UNSTABLE] = CMM_RETURN;
	}
	cmm_transition[last_step][CMM_STABLE] = CMM_END_SYNC;
	cmm_transition[last_step][CMM_UNSTABLE] = CMM_RETURN;

#ifdef CMM_VERSION_0
	//
	// If we are executing the old set of steps, we have to transition
	// to CMM_END_SYNC state after step 11.
	//
	cmm_transition[CMM_STEP_11][CMM_OLD_STEPS_EVENT] = CMM_END_SYNC;
#endif // CMM_VERSION_0
	//
	// end sync and wait
	//
	cmm_transition[CMM_END_SYNC][CMM_STABLE] = CMM_END_WAIT;
	cmm_transition[CMM_END_SYNC][CMM_UNSTABLE] = CMM_RETURN;
	cmm_transition[CMM_END_WAIT][CMM_UNSTABLE] = CMM_RETURN;
	//
	// return
	//
	cmm_transition[CMM_RETURN][CMM_PROCEED] = CMM_BEGIN;
}

//
// Variable to control tracing and printing under CMM_DEBUG
// Initialize to CMM_TRACE_ARGS to turn on tracing only.
//
uint_t		cmm_trace_mask = CMM_TRACE_ARGS;

automaton_impl::automaton_impl(cmm::comm_ptr commp,
	quorum::quorum_algorithm_ptr quorump, nodeid_t ndid,
	incarnation_num incn) : _commp(cmm::comm::_duplicate(commp)),
				_quorum_p(quorum::quorum_algorithm::
				_duplicate(quorump))
{
	nodeid_t i, j;
	Environment e;

	//
	// Initialize the state of the configured nodes to S_UNKNOWN
	// and the state of the unconfigured nodes to S_DEAD.
	//
	for (i = 0; i <= NODEID_MAX; i++) {
		for (j = 0; j <= NODEID_MAX; j++) {
			_nodes[i].state_vector[j] = S_DEAD;
		}
	}
	for (i = 0; i <= NODEID_MAX; i++) {
		if (orb_conf::node_configured(i)) {
			for (j = 0; j <= NODEID_MAX; j++)
				if (orb_conf::node_configured(j)) {
					_nodes[i].state_vector[j] = S_UNKNOWN;
					_nodes[i].heard_from_vector[j].empty();
					_nodes[i].quorum_result[j].nodeid = i;
					_nodes[i].quorum_result[j].
						quorum_devices_owned = 0;
					_nodes[i].quorum_result[j].
						we_have_been_preempted = false;
				}
		}
	}

	for (i = 0; i <= NODEID_MAX; i++) {

		char	nodenm[8];

		os::sprintf(nodenm, "%d", i);

		_membership.members[i] = INCN_UNKNOWN;

		_nodes[i].incarnation = INCN_UNKNOWN;
		_nodes[i].reconfig_seqnum = 0;
		_nodes[i].heard_from.empty();
		_nodes[i].membership.empty();
		_nodes[i].msgp = new os::sc_syslog_msg(
		    SC_SYSLOG_CMM_NODE_TAG, nodenm, NULL);
		_nodes[i].fenceoff_timer = (hrtime_t)0;
		_nodes[i].delta_timer = false;
		_nodes[i].must_reacquire_quorum = false;

		_nodes[i].initial_view_number = INCN_UNKNOWN;

		_nodes[i].join_occurred.empty();

	}

	_current_step = 0;
	_automaton_flags = 0;
	_primary = NODEID_UNKNOWN;

	ff::failfast_admin_var ff_admin_v = cmm_ns::get_ff_admin();
	CL_PANIC(!CORBA::is_nil(ff_admin_v));

	_mynodeid = ndid;
	_nodes[_mynodeid].incarnation = incn;
	_max_reconfig_seqnum = MYSEQNUM = 0;
	_disagree_join_set.empty();
	_rebooting_nodes.empty();
	_rebooting_nodes.add_node(_mynodeid);

	_membership_change_flag = false;


	if (clnewlwp(cmm_timer_thread_start, this, orb_conf::rt_maxpri(),
	    orb_conf::rt_classname(), NULL) != 0) {
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Unable to create %s thread.", "cmm_timer");
	}

	if (clnewlwp(timeout_handler_thread_start, this, orb_conf::rt_maxpri(),
	    orb_conf::rt_classname(), NULL) != 0) {
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Unable to create %s thread.", "timeout_handler");
	}

	_dump_state(1);
}

//lint -e1740
automaton_impl::~automaton_impl()
{
	// The reference is released in method _unreferenced
	ASSERT(_commp == cmm::comm::_nil());

	// The reference is released in method _unreferenced
	ASSERT(_quorum_p == quorum::quorum_algorithm::_nil());
}
//lint +e1740

void
#ifdef DEBUG
automaton_impl::_unreferenced(unref_t cookie)
#else
	automaton_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));

	CORBA::release(_commp);
	_commp = cmm::comm::_nil();
	CORBA::release(_quorum_p);
	_quorum_p = quorum::quorum_algorithm::_nil();

	cmm_automaton_callout = NULL;

	cmm_impl::the_cmm_impl->dec_ref(DONT_EXIT);	// unreference the_cmm
}

//
// Cheap but approximate conversion of a time period in milliseconds to the
// same time period in nanoseconds. It uses a 20-bit left shift to simulate
// a multiplication by a million.
//
/*CSTYLED*/
/*lint -emacro(572, MILLISECS_TO_NANOSECS); 20 bit shift is OK */
#define	MILLISECS_TO_NANOSECS(msecs)	((hrtime_t)(((hrtime_t)(msecs)) << 20))

// Converts seconds to microseconds
#define	SECS_TO_MICROSECS(secs) ((os::usec_t)((secs) << 20))

//
// Converts seconds to nanoseconds
//
// For some reason lint thinks the following macro is not referenced.
//
//lint -e750
#define	SECS_TO_NANOSECS(secs) ((hrtime_t)(((hrtime_t)(secs)) << 30))
//lint +e750

// static
void
automaton_impl::cmm_timer_thread_start(void *arg)
{
	automaton_impl *autmp = (automaton_impl *)arg;
	autmp->cmm_timer_thread();
	// NOTREACHED
}

//
// Wakes from every cmm_timer_period seconds and records the current time
// in last_update_time. Notice that realtime timeout mechanism is used
// here. Realtime callouts are handled by softints and are not subject to
// priority inversions as normal callouts are (which are handled by taskq
// threads, see cv_timedwait(9F)).
//
// See automaton_impl::per_tick_processing() for more on how last_update_time
// is used.
//
void
automaton_impl::cmm_timer_thread()
{
	for (;;) {
		timer_lock.lock();
		last_update_time = os::gethrtime();
		timer_lock.unlock();
		os::usecsleep(SECS_TO_MICROSECS(cmm_timer_period));
	}
	// NOTREACHED
}

// static
void
automaton_impl::timeout_handler_thread_start(void *arg)
{
	automaton_impl *autmp = (automaton_impl *)arg;
	autmp->timeout_handler_thread();
	// NOTREACHED
}

//
// The automaton uses timers to fence off unreachable nodes that may be
// split brained.
//
void
automaton_impl::timeout_handler_thread()
{
	nodeid_t node;
	hrtime_t now;

	for (;;) {
		now = os::gethrtime();
		_state_lock.lock();

		for (node = 1; node <= NODEID_MAX; node++) {

			if ((_nodes[node].fenceoff_timer == 0) ||
			    (now < _nodes[node].fenceoff_timer)) {
				continue; // no timer or timer not expired
			}

			//
			// Timer expired; need to take action.
			//
			if (_nodes[node].delta_timer) {
				//
				// If the timer is for the local node, this is
				// for the shutdown failfast case. The local
				// node should panic. BugId 4283729.
				//
				if (node == _mynodeid) {
					//
					// SCMSGS
					// @explanation
					// The node could not complete its
					// shutdown sequence within the halt
					// timeout, and is aborting to enable
					// another node to safely take over
					// its services.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) cmm_syslog_msgp->log(
					    SC_SYSLOG_PANIC, MESSAGE, "CMM: "
					    "Shutdown timer expired. Halting.");
				}

				//
				// The timer that expired is the delta timer.
				// We can now declare the node dead.
				//
				CMM_TRACE(("Delta timer expired for"
				    " node %ld\n", node));
				node_is_dead(node);
			} else {
				//
				// Split brain timer went off. Need to check if
				// we acquired quorum after the node was
				// declared down. If not, we must halt
				// immediately to prevent data corruption due
				// to split brain.
				//
				if (_nodes[node].must_reacquire_quorum) {
					CMM_TRACE(("Halting to prevent split "
					    "brain with node %ld\n", node));
					(void) cmm_syslog_msgp->\
					    log(SC_SYSLOG_PANIC, MESSAGE,
					    "CMM: Halting to prevent "
					    "split brain with node %ld.",
					    node);
				} else {
					CMM_TRACE(("Split brain timer expired"
					    " for node %ld; starting delta"
					    " timer\n", node));
					//
					// We have reacquired quorum. We must
					// now start the delta timer before
					// declaring the other node dead.
					//
					_nodes[node].fenceoff_timer = now +
					    MILLISECS_TO_NANOSECS(
						_nodes[node].detection_delay);
					_nodes[node].delta_timer = true;
				}
			}
		}
		_state_lock.unlock();
		os::usecsleep((os::usec_t)(cmm_sleep_interval * 1000));
	}
}

//
// Update the automaton state when the the 'stop' or 'abort' method of the
// CMM control interface is invoked.
//
void
automaton_impl::stop()
{
	_state_lock.lock();

	//
	// Ignore the message if it is aborting.
	//
	if (_automaton_flags & NODE_IS_ABORTING) {
		_state_lock.unlock();
		return;
	}

	_automaton_flags |= STOP_LOCAL_NODE; /* stop the local node */

	//
	// Log the request.
	//
	CMM_TRACE(("node being shut down\n"));
	//
	// SCMSGS
	// @explanation
	// This node is being shut down.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "CMM: Node being shut down.");

	// publish sysevent
	clconf_get_nodename(_mynodeid, nodename);
	(void) sc_publish_event(ESC_CLUSTER_NODE_STATE_CHANGE,
	    CL_EVENT_PUB_CMM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
	    CL_REASON_CODE, SE_DATA_TYPE_UINT32, CL_REASON_CMM_NODE_SHUTDOWN,
	    NULL);
	_state_change_cv.broadcast();

	_state_lock.unlock();
}

void
automaton_impl::abort()
{
	_state_lock.lock();

	//
	// Ignore the message if it is aborting.
	//
	if (_automaton_flags & NODE_IS_ABORTING) {
		_state_lock.unlock();
		return;
	}

	_automaton_flags |= NODE_IS_ABORTING; /* abort the local node */
	cmm_impl::the_cmm_impl->signal_abort_thread();

	//
	// Log the request.
	//
	CMM_TRACE(("node being aborted from cluster\n"));
	//
	// SCMSGS
	// @explanation
	// This node is being excluded from the cluster.
	// @user_action
	// Node should be rebooted if required. Resolve the problem according
	// to other messages preceding this message.
	//
	(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_WARNING, MESSAGE,
	    "CMM: Node being aborted from the cluster.");
	_state_change_cv.broadcast();

	_state_lock.unlock();
}

#ifdef DEBUG
#ifdef _KERNEL_ORB
//
// Sends messages to all the nodes to bring down all the nodes
// that are part of the cluster.
//
void
automaton_impl::stop_cluster()
{
	CMM_TRACE(("Received cluster stop. Broadcast msg to nodes\n"));
	bool lock_held = false;
	//
	// Make sure we dont try acquiring this lock again if we
	// already hold it.
	//
	if (!_state_lock.lock_held()) {
		lock_held = true;
		_state_lock.lock();
	}
	broadcast_stop_msg();
	if (lock_held) {
		_state_lock.unlock();
	}
}
#else // _KERNEL_ORB
void
automaton_impl::stop_cluster()
{
	ASSERT(0);
}
#endif // _KERNEL_ORB
#endif // DEBUG

//
// Update the automaton state when a message is received from another node.
//
void
automaton_impl::receive_msg(const cmm::message_t& msg, nodeid_t from,
    Environment&)
{
	nodeid_t		i;
	int			state_changed = 0;
	int			force_sender = 0;
	nodeset			ns(msg.curr_membership);
	nodeset			msg_rebootees(msg.rebooting_nodes);
	cmm::seqnum_t		myseqnum;
	incarnation_num		my_incn,
		his_incn,
		my_view_of_his_incn,
		his_view_of_my_incn;
	bool			cluster_shutdown;
	cmm::message_t		rcv_msg = msg;

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	void			*generic_fault_argp = NULL;
	uint32_t		generic_fault_argsize = 0;
	void			*fault_argp = NULL;
	uint32_t		fault_argsize = 0;
#endif

	_state_lock.lock();
#ifndef _KERNEL_ORB
	//
	// For userland CMM we have to do the message conversion here
	// as the message communication happens directly through
	// invocations to this function.
	//
	// If the message is an old version message, convert to new message
	// format here.
	//
	convert_msg_to_new(rcv_msg);
#endif // _KERNEL_ORB

	CMM_DEBUG_TRACE(("receive_msg from[%ld]\n", from));

	//
	// The order of checks in this function is important.  Don't try
	// to rearrange them.
	//
	if (MYSTATE <= S_ABORTED) {
		goto done;		// ignore the message
	}

	//
	// Ignore stale messages either coming from or directed to
	// older incarnations of the local node or the remote node.
	//
	myseqnum = MYSEQNUM;
	my_incn = _nodes[_mynodeid].incarnation;
	his_incn = rcv_msg.incarnations[from];
	my_view_of_his_incn = _nodes[from].incarnation;
	his_view_of_my_incn = rcv_msg.incarnations[_mynodeid];
	cluster_shutdown = rcv_msg.cluster_shutdown;

	if (his_incn == my_view_of_his_incn) {
		//
		// Continuing communication with the same incarnation.
		// Check if I have already declared the other node down.
		// If so, ignore the rest of the message, but let him know
		// that I have eliminated him.
		//
		if (cluster_shutdown) {
			//
			// If the shutdown flag has not been set and we are not
			// in begin state, we just set the cluster shutdown
			// intent as we want to be sure we are in begin state
			// before we commit. Setting the cluster shutdown intent
			// will force a reconfiguration. If in BEGIN state, we
			// commit the cluster shutdown i.e. set the
			// _cluster_shutdown flag.
			//
			if (!cmm_impl::the().get_cluster_shutdown()) {
				CMM_TRACE(("node %ld had initiated shutdown\n",
				    from));
				if (MYSTATE == CMM_BEGIN) {
					CMM_TRACE(
					    ("commit shutdown in begin "
					    "state\n"));
					cmm_impl::the().
						cluster_shutdown_commit();
				} else {
					//
					// Release the lock as this routine
					// needs it again.
					//
					_state_lock.unlock();
					(void) cmm_impl::the().
						cluster_shutdown();
					_state_lock.lock();
				}
			}
		}
		if (STATEVEC[from] <= S_DOWN) {
			CMM_DEBUG_TRACE(("force_sender as sender declared "
			    "me down\n"));
			force_sender = 1;
			goto done;
		} else {
			//
			// If he has declared me down, I continue to ignore
			// rest of the message . Justification for this action
			// is Bugid 4498092.
			// Following are the cases when a local node is
			// declared down by a remote node.
			// a. node_is_unreachable:
			//    local node is not reachable by remote node, so
			//    it will not get declaring down message anyway.
			// b. local node is shutting:
			//    when a remote node identifies that local node is
			//    shutting down, it declares local node down.
			//    Ignoring this message is OK as local node
			//    is shutting down anyway.
			//
			// c. when a node X says node Y is down:
			//    In this case when a node Z(local node) gets a
			//    message from node X saying node Y is down, then
			//    node Z will declare node Y to be down only if it
			//    has not directly heard from Y. As a result
			//    node Y can ignore force down  rule since it cannot
			//    communicate with Z anyway.
			//
			// d. when a node is not configured or down:
			//    consider a 3 node cluster, node 3 is still down.
			//    during the boot delay period node 2 declares node
			//    3 down. since node 3 is down it will not get
			//    "declaring me down" message.
			//
			// e. processing partial disconnects:
			//    local node gets "declared me down" message when
			//    remote node automaton decides that local
			//    node should be removed from the cluster in order
			//    to preserve full connectivity. In this case
			//    ignoring this message is fine because the result
			//    of this is local node is able to see remote node
			//    in its connectivity matrix and its automaton will
			//    abort itself inorder to preserve full
			//    connectivity.
			//
			if ((his_view_of_my_incn == my_incn) &&
			    (rcv_msg.state_list[_mynodeid] <= S_DOWN)) {
				CMM_TRACE(("node %ld has declared me down;",
				    from));
				goto done; // ignore the rest of the message
			}
		}
	} else if (his_incn < my_view_of_his_incn) {
		//
		// Message from an earlier incarnation. Ignore.
		//
		CMM_TRACE(("ignoring stale msg from node %ld; "
		    "current inc# = %ld, msg inc# = %ld\n",
		    from, my_view_of_his_incn, his_incn));
		goto done;
	} else if (ns.contains(_mynodeid) &&
	    (his_view_of_my_incn < my_incn)) {
		//
		// Message for my prior incarnation. Ignore.
		//
		CMM_TRACE(("stale msg from node %ld for my prior "
		    "incarnation; current inc# = %ld, msg inc# = %ld\n",
		    from, my_incn, his_view_of_my_incn));
		goto done;
	} else if (my_view_of_his_incn < his_incn) {
		//
		// Start of new communications between the current
		// incarnations. One of us must be coming up for the
		// first time or aborting. Otherwise, there was a split
		// brain situation before this message was received.
		//
		CL_PANIC(FIRST_TIME_UP ||
		    msg_rebootees.contains(from) ||
		    (MYSTATE < CMM_BEGIN) ||
		    (rcv_msg.state_list[from] < CMM_BEGIN));

		CMM_TRACE(("received first message from node %ld; "
		    "inc# = %ld, previous inc# = %ld; seqnum = %lld\n",
		    from, his_incn, my_view_of_his_incn, rcv_msg.seqnum));

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
		if (fault_triggered(FAULTNUM_CMM_NODE_JOIN_HEARSAY_3,
		    &generic_fault_argp, &generic_fault_argsize)) {
			//
			// If the fault point FAULTNUM_CMM_NODE_JOIN_HEARSAY_3
			// is armed for the fault scenario
			// "node join hearsay : delay node_is_reachable()",
			// it means we want node_is_reachable()
			// to call node_is_up() for the new node, eventually
			// after its delay fault action is complete.
			// Hence we ignore this first message from the
			// newly 'up' node, and do not mark it up as of now.
			// Once node_is_reachable() marks the node up,
			// the automaton will restart reconfiguration
			// and it will get messages from the newly up node.
			//
			// We do not expect any other node to be newly booting
			// up, in this fault scenario, except for the node
			// that node_is_reachable() is being delayed for.
			//
			CMM_TRACE(("node join hearsay fault scenario armed : "
			    "ignoring first msg from node %d\n", from));
			goto done;
		}
#endif

		if (cluster_shutdown) {
			//
			// If the shutdown flag has not set and we are not in
			// begin state, we just set the cluster shutdown intent
			// as we want to be sure we are in begin state before we
			// commit. Setting the cluster shutdown intent will
			// force a reconfiguration. If in BEGIN state, we commit
			// the cluster shutdown i.e. set the _cluster_shutdown
			// flag.
			//
			if (!cmm_impl::the().get_cluster_shutdown()) {
				CMM_TRACE(("node %ld had initiated shutdown\n",
				    from));
				if (MYSTATE == CMM_BEGIN) {
					CMM_TRACE(("commit shutdown "
					    "while in begin state\n"));
					cmm_impl::the().
						cluster_shutdown_commit();
				} else {
					//
					// Release the lock as this routine
					// needs it again.
					//
					_state_lock.unlock();
					(void) cmm_impl::the().
						cluster_shutdown();
					_state_lock.lock();
				}
			}
		}
		node_is_up(from, his_incn);
	}

	if ((_automaton_flags & NODE_IS_ABORTING) || MYSTATE == S_STOP) {
		goto done;		// ignore the message
	}

	//
	// BugId 4283729
	// If the remote node reports that it has completed the "stop"
	// or the "abort" transition, set the state of the node to
	// S_DEAD after conf.node_halt_timeout rather than marking
	// it dead immediately. There is no need for the split
	// brain or delta timeouts since the remote node is reporting
	// that it has completed the "stop" or "abort" transition.
	// However, since the remote node shutdown sequence enables
	// a failfast timer with a timeout of conf.node_halt_timeout
	// right before sending the msgs out, it can only be safely
	// declared to be dead after waiting for a conf.node_halt_timeout
	// at this end.
	//
	if (rcv_msg.state_list[from] == S_STOPPED ||
	    rcv_msg.state_list[from] == S_ABORTED) {

		CMM_TRACE(("Node %ld says it has %s\n", from,
		    _state_string(rcv_msg.state_list[from])));
		node_is_shutting_down(from);

		// Ignore the rest of the message.
		goto done;
	}

	//
	// Ignore the rest of the message if seqnum is stale.
	// The following condition checks for 3 of the possible cases when a
	// sequence number could be stale.
	// 1. when myseqnum is greater than othernode seqnum and the local node
	//    was part of previous membership (if myseqnum is greater than
	//    othernode seqnum and the local node is a rebooting node,
	//    it should be treated as valid message as othernode might be
	//    busy on its own tasks with old seqnum so that it does not have
	//    chance to sync up the new seqnum with the rest of the nodes.
	//    Otherwise, If the local node is booting up, and if the message
	//    is dropped, it might not receive message from othernode with
	//    up-to-date seqnum during boot delay period. Then, the local
	//    node might abort itself using rebootee's rule. So, the message
	//    should be valid if myseqnum is greater than othernode seqnum
	//    and the local node is booting up. BugId 4771342).
	// 2. when othernode is not a rebooting node and has same sequence
	//    number as myseqnum and it is atleast 2 states behind local node.
	//    Going by the rules of reconfiguration this is violation of case 1.
	//
	// 3. When othernode has a sequence number higher than
	//    myseqnum, but local node WAS part of previous membership and
	//    other node WAS NOT part of the previous membership.
	//    other node will ABORT itself based on this condition.
	//    Justification for the changes is Bug 4497254.
	//
	//
	// Send the current state out so that the node with the stale
	// sequence number can immediately synchronize its sequence number,
	// or abort.
	//
	if (((rcv_msg.seqnum < myseqnum) && (!FIRST_TIME_UP)) ||
	    ((! msg_rebootees.contains(from)) &&
	    (rcv_msg.seqnum == myseqnum) &&
	    ((uint_t)rcv_msg.state_list[from] >= (uint_t)CMM_BEGIN) &&
	    ((rcv_msg.state_list[from] < (uint_t)MYSTATE) &&
	    (!is_previous(MYSTATE, rcv_msg.state_list[from])))) ||
	    ((!FIRST_TIME_UP) &&
		((uint_t)MYSTATE >= (uint_t)CMM_BEGIN ||
		    MYSTATE == CMM_RETURN) &&
		    (rcv_msg.seqnum > myseqnum + 1) &&
		    (msg_rebootees.contains(from)))) {
		CMM_DEBUG_TRACE(("force_sender as sender has stale seqnum\n"));
		force_sender = 1;
		goto done;
	}

	if (_nodes[from].reconfig_seqnum != rcv_msg.seqnum) {
		_nodes[from].reconfig_seqnum = rcv_msg.seqnum;
		CMM_DEBUG_TRACE(("state_changed as sender has new seqnum\n"));
		state_changed = 1;
	}

	//
	// If the sequence number of the sender is higher than that of
	// the local node, the local node will need to return to the CMM_BEGIN
	// state to resynchronize.
	//
	if (rcv_msg.seqnum > myseqnum) {
		if (rcv_msg.seqnum > _max_reconfig_seqnum) {
			_max_reconfig_seqnum = rcv_msg.seqnum;
		}
		CMM_TRACE(("state_changed as sender seqnum greater "
		    "than mine. Sender = %ld\n", from));
		state_changed = 1;
	}

	//
	// ---- Start to process message functional content ---
	//

	//
	// Now process the initial view number sent in the message.
	//
	_nodes[from].initial_view_number = rcv_msg.initial_view_number;

	//
	// Now process the state vector sent in the message.
	//

	//
	// If we are in the CMM_BEGIN , update the
	// connectivity matrix and rebootee bitmask.
	//
	if (MYSTATE == CMM_BEGIN) {
		if (!_nodes[_mynodeid].heard_from.contains(from) &&
		    //
		    // Don't count his message unless he acknowledges me.
		    // This covers two cases:
		    // (1) I am rebooting and he sends me a message
		    // before he hears from me;
		    // (2) he has declared me (my current incarnation)
		    // down for some reason.
		    //
		    (his_view_of_my_incn == my_incn)) {
			_nodes[_mynodeid].heard_from.add_node(from);
			_nodes[_mynodeid].heard_from_vector[_mynodeid].set(
			    _nodes[_mynodeid].heard_from);
			CMM_DEBUG_TRACE(("state_changed as sender does not "
			    "know me\n"));
			state_changed = 1;
		}
		if (_nodes[from].heard_from.bitmask() != rcv_msg.heard_from) {
			_nodes[from].heard_from.set(rcv_msg.heard_from);
			_nodes[_mynodeid].heard_from_vector[from].set(
			    rcv_msg.heard_from);
			CMM_DEBUG_TRACE(("state_changed as updated "
			    "heard_from\n"));
			state_changed = 1;
		}
		_rebooting_nodes.add_nodes(msg_rebootees);
	}

	//
	// Update the node state
	//
	if (_nodes[from].join_occurred.bitmask() != rcv_msg.join_occurred) {
		_nodes[from].join_occurred.set(rcv_msg.join_occurred);
		CMM_DEBUG_TRACE(("state_changed as updated join_occurred\n"));
		state_changed = 1;
	}

	//
	// Copy the state vector into the appropriate row in the local
	// node's state matrix, noting any change.
	//
	for (i = 1; i <= NODEID_MAX; i++) {

		_nodes[from].heard_from_vector[i].set(
		    rcv_msg.heard_from_vector[i]);
		if (_nodes[from].state_vector[i] == rcv_msg.state_list[i]) {
			continue;
		}

		_nodes[from].state_vector[i] = rcv_msg.state_list[i];
		CMM_DEBUG_TRACE(("state_changed as updated state vector\n"));
		state_changed = 1;

		incarnation_num	incn = rcv_msg.incarnations[i];

		//
		// If the sender is telling us about an older incarnation
		// of i, ignore the info.
		//
		if (incn < _nodes[i].incarnation) {
			continue;
		}

		//
		// The local node does not change the local node view
		// of other nodes based upon what other nodes say.
		// We trace this information for debugging purposes only.
		//
		if ((STATEVEC[i] == S_UNKNOWN) &&
		    (rcv_msg.state_list[i] == S_DOWN)) {
			// We always know our own state !!!
			CL_PANIC(i != _mynodeid);
			CMM_TRACE(("Node %ld says node %ld is down\n",
			    from, i));

		} else if ((STATEVEC[i] == S_DOWN) &&
		    (rcv_msg.state_list[i] == S_DEAD)) {
			// Our own state cannot be S_DOWN !!!
			CL_PANIC(i != _mynodeid);
			CMM_TRACE(("Node %ld says node %ld is dead\n",
			    from, i));

		} else {
			if ((_nodes[i].incarnation < incn) &&
			    (rcv_msg.state_list[i] > S_DOWN)) {
				//
				// We trace a message for easier debugging;
				// but we ignore any message about a newer
				// incarnation until we hear from the newer
				// incarnation directly.
				//
				CMM_TRACE(("Node %ld says node %ld has a new "
				    "inc#; old inc# = %ld,"
				    "new inc# = %ld, old state = %s\n",
				    from, i, _nodes[i].incarnation, incn,
				    _state_string(STATEVEC[i])));
			}
		}

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
		if (fault_triggered(FAULTNUM_CMM_NODE_DEPART_HEARSAY_2,
		    &generic_fault_argp, &generic_fault_argsize)) {
			//
			// This fault point is used to test
			// the following node depart hearsay scenario only :
			// Node C is rebooted. Remote node B will send message
			// to local node A to tell that node C has gone down.
			// But local node A will not consider node C as down
			// until node_is_unreachable() for node C
			// is called on node A.
			//
			// Get the data about hearsay nodeids
			// (nodeB and nodeC) from the fault arguments.
			// If this CMM message received has been sent by nodeB,
			// and nodeB thinks nodeC is down,
			// then we fire the fault point.
			//

			// Get the fault data we want from generic fault args
			FaultFunctions::get_fault_data_from_generic_arg(
			    generic_fault_argp, &fault_argp, fault_argsize);
			FaultFunctions::mc_sema_with_data_arg_t *mc_sema_argsp =
			    (FaultFunctions::mc_sema_with_data_arg_t *)
			    fault_argp;

			// Check that we are getting the data that we expect
			ASSERT(mc_sema_argsp->shared_data_len == (uint32_t)
			    sizeof (FaultFunctions::delay_for_hearsay_arg_t));
			FaultFunctions::delay_for_hearsay_arg_t *argsp =
			    (FaultFunctions::delay_for_hearsay_arg_t *)(
			    mc_sema_argsp->shared_data);

			//
			// If nodeB is telling us that nodeC went down,
			// then fire the fault point.
			//
			if ((argsp->nodeB == from) && (argsp->nodeC == i) &&
			    (rcv_msg.state_list[i] == S_DOWN)) {
				FAULTPT_KCMM(FAULTNUM_CMM_NODE_DEPART_HEARSAY_2,
				    FaultFunctions::generic);
			}
		}
#endif

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
		if (fault_triggered(FAULTNUM_CMM_NODE_JOIN_HEARSAY_2,
		    &generic_fault_argp, &generic_fault_argsize)) {
			//
			// This fault point is used to test
			// the following node join hearsay scenario only :
			// Node C boots up. Remote node B will send message
			// to local node A to tell that node C
			// has come up with a new incn.
			// But local node A will not consider node C as up
			// with a new incn, until node_is_reachable()
			// for node C is called on node A.
			//
			// Get the data about hearsay nodeids
			// (nodeB and nodeC) from the fault arguments.
			// If this CMM message received has been sent by nodeB,
			// and nodeB thinks nodeC is up with a new incn,
			// then we fire the fault point.
			//

			// Get the fault data we want from generic fault args
			FaultFunctions::get_fault_data_from_generic_arg(
			    generic_fault_argp, &fault_argp, fault_argsize);
			FaultFunctions::mc_sema_with_data_arg_t *mc_sema_argsp =
			    (FaultFunctions::mc_sema_with_data_arg_t *)
			    fault_argp;

			// Check that we are getting the data that we expect
			ASSERT(mc_sema_argsp->shared_data_len == (uint32_t)
			    sizeof (FaultFunctions::delay_for_hearsay_arg_t));
			FaultFunctions::delay_for_hearsay_arg_t *argsp =
			    (FaultFunctions::delay_for_hearsay_arg_t *)(
			    mc_sema_argsp->shared_data);

			//
			// If nodeB is telling us that nodeC is up
			// with a new incn, then fire the fault point.
			//
			if ((argsp->nodeB == from) && (argsp->nodeC == i) &&
			    (_nodes[i].incarnation < incn) &&
			    (rcv_msg.state_list[i] > S_DOWN)) {
				FAULTPT_KCMM(FAULTNUM_CMM_NODE_JOIN_HEARSAY_2,
				    FaultFunctions::generic);
			}
		}
#endif

	}

	//
	// Update the membership set (nodeset) proposed by node 'from',
	// noting any change.
	//
	if (!_nodes[from].membership.equal(ns)) {
		_nodes[from].membership.set(ns);
		CMM_DEBUG_TRACE(("state_changed as updated membership set\n"));
		state_changed = 1;
	}

	//
	// The local node updates its view of the current state of
	// the remote node.
	//
	if (STATEVEC[from] != _nodes[from].state_vector[from]) {
		STATEVEC[from] = _nodes[from].state_vector[from];

		//
		// If the sender is in the CMM_QCHECK state or a later
		// state, the quorum_result field must be valid.
		// We should copy it if our own state needs it.
		//
		if ((STATEVEC[from] >= CMM_QCHECK) && (MYSTATE <= CMM_QCHECK)) {

			if (from == _primary) {
				CMM_DEBUG_TRACE(("recd quorum_result "
				    "from primary [%ld]\n",
				    _primary));
				for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
					if (!MYMEMBERSHIPSET.contains(n)) {
						continue;
					}
					_nodes[n].quorum_result[n].nodeid = n;
					_nodes[n].quorum_result[n].
						quorum_devices_owned =
						rcv_msg.quorum_devices_owned[n];
					_nodes[n].quorum_result[n].
						we_have_been_preempted =
						rcv_msg.
						we_have_been_preempted[n];
					CMM_DEBUG_TRACE(("recd quorum_result "
					    "from node %ld: "
					    "{%ld, 0x%llx, %d}\n",
					    from, n,
					    rcv_msg.quorum_devices_owned[n],
					    rcv_msg.we_have_been_preempted[n]));
				}
			} else {
				_nodes[from].quorum_result[from].nodeid = from;
				_nodes[from].quorum_result[from].
					quorum_devices_owned =
					rcv_msg.quorum_devices_owned[from];
				_nodes[from].quorum_result[from].
					we_have_been_preempted =
					rcv_msg.we_have_been_preempted[from];
				CMM_DEBUG_TRACE(("recd quorum_result from node "
				    "%ld: {%ld, 0x%llx, %d}\n",
				    from, from,
				    rcv_msg.quorum_devices_owned[from],
				    rcv_msg.we_have_been_preempted[from]));
			}
		}

		//
		// If we are receiving a synchronization step message from
		// the primary node, then we have to update our state
		// vector with the latest state views from the primary.
		//
		if ((STATEVEC[from] >= CMM_QCHECK) && (MYSTATE >= CMM_QCHECK)) {
			if (from == _primary) {
				CMM_DEBUG_TRACE(("update state vectors from "
				    "primary [%ld]\n", _primary));
				for (nodeid_t n = 1; n <= NODEID_MAX; n++) {
					if (!MYMEMBERSHIPSET.contains(n)) {
						continue;
					}
					//
					// The state view that we have is
					// obsolete as during synchronization
					// step, nodes only send state update
					// to the primary node. We use the
					// primary node state view from the
					// message to update our state vector.
					//
					STATEVEC[n] = (uint16_t)
						_nodes[from].state_vector[n];
				}
			}
		}

		//
		// We force the broadcast of the local state vector
		// only if we are not doing cluster reconfiguration.
		// Forcing the broadcast during reconfigurations would
		// result in an unnecessarily high number of messages.
		//
		if (_nodes[from].state_vector[from] < CMM_QCHECK) {
			CMM_DEBUG_TRACE(("force_sender as sender state "
			    "< CMM_QCHECK\n"));
			force_sender = 2;
		}
	}

done:
	if (state_changed) {
		_state_change_cv.broadcast();
		_dump_state(0);
		//
		// In CMM_BEGIN state, the condition variable signal
		// above results in messages to be exchanged as part
		// of the begin state refresh. So there is no need
		// to keep force_sender set.
		//
		if ((force_sender) && (MYSTATE == CMM_BEGIN)) {
			CMM_DEBUG_TRACE(("reset force_sender\n"));
			force_sender = 0;
		}
	}

	if (force_sender == 1) {
		//
		// We forward our current state view to initial sender
		// to let him know that either:
		//
		// i)  we declared it down,
		// ii) it has a stale sequence number.
		//
		send_msg(from);
	} else if (force_sender == 2) {
		//
		// We broadcast our state vector update.
		//
		broadcast_msg();
	}

	_state_lock.unlock();
}

//
// Mark a node dead.
//
// This routine is called by the automaton after it has determined that
// that the remote node has been fenced off and is safe to take over from.
// This can happen under the following circumstances:
//
// (1)	A message has been received from another node indicating that
//	the automaton has shut down, and the timer used for ensuring
//	the shutting down of a remote node, has gone off.
// (2)	The timer used for marking the remote node in the S_DEAD state has
//	gone off and our partition has acquired quorum after the timer
//	was started.
//
// This routine should be called with the state lock held.
//
void
automaton_impl::node_is_dead(nodeid_t nodeid)
{
	nodeid_t	i;

	ASSERT(_state_lock.lock_held());

	if (STATEVEC[nodeid] != S_DEAD) {
		//
		// If the node has not been marked down yet (scenario 2),
		// do so now.
		//
		if (STATEVEC[nodeid] != S_DOWN) {
			node_is_down(nodeid);
		}

		STATEVEC[nodeid] = S_DEAD;

		CMM_TRACE(("node %ld is dead.\n", nodeid));

		clconf_get_nodename(nodeid, nodename);

		//
		// SCMSGS
		// @explanation
		// The specified node has died. It is guaranteed to be no
		// longer running and it is safe to take over services from
		// the dead node.
		// @user_action
		// The cause of the node failure should be resolved and the
		// node should be rebooted if node failure is unexpected.
		//
		(void) _nodes[nodeid].msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "CMM: Node %s (nodeid = %d) is dead.",
		    nodename, nodeid);
		//
		// Stop the fenceoff timer for the node if it is running.
		//
		stop_fenceoff_timer(nodeid);

		//
		// Remove all indications of the liveness of the node
		// from the connectivity matrix.
		//
		for (i = 1; i <= NODEID_MAX; i++) {
			_nodes[i].heard_from.remove_node(nodeid);
			_nodes[_mynodeid].heard_from_vector[i].remove_node(
			    nodeid);
		}

		if (!cmm_impl::the().get_cluster_shutdown()) {
			_state_change_cv.broadcast();
		}

		_dump_state(0);
		broadcast_msg();
	}
}

//
// Mark a node down.
//
// This routine is called by the automaton whenever it wants to mark
// a node down. This could be when the transport subsystem indicates
// that a node is unreachable, when a message is received from a remote
// node indicating that it is shutting down, or when the
// automaton decides to declare a node down to preserve full connectivity.
//
// This routine should be called with the state lock held.
//
void
automaton_impl::node_is_down(nodeid_t nodeid)
{
	nodeid_t	i;

	ASSERT(_state_lock.lock_held());

	if ((STATEVEC[nodeid] <= S_DOWN) || (MYSTATE <= S_ABORTED)) {
		return; // Old or irrelevant news! Ignore.
	}

	STATEVEC[nodeid] = S_DOWN;

	CMM_TRACE(("node %ld is down\n", nodeid));

	//
	// The local node now doesn't know the remote node's view
	// of the cluster state.
	//
	for (i = 1; i <= NODEID_MAX; i++) {
		_nodes[nodeid].state_vector[i] = S_UNKNOWN;
		_nodes[nodeid].heard_from_vector[i].empty();
	}

	//
	// Remove the down node from our connectivity vector
	// and ignore the connectivity vector reported by it.
	//
	_nodes[_mynodeid].heard_from.remove_node(nodeid);
	_nodes[_mynodeid].heard_from_vector[_mynodeid].remove_node(nodeid);
	_nodes[nodeid].heard_from.empty();

	//
	// If we are in a step state, terminate any CMM client callbacks
	// that may be in progress. This allows us to quickly start a new
	// reconfiguration to process this event.
	//
	if ((MYSTATE >= CMM_STEP_1) && (MYSTATE < CMM_END_SYNC)) {
		cmm_impl::the_cmm_impl->_cb_registry.terminate_trans(MYSEQNUM);
	}

	if (MYSTATE == CMM_BEGIN) {
		//
		// While in begin_state, node_is_down got called.
		// This node lost connectivity with some other node in the
		// cluster. The cmm reconfiguration needs to start from
		// the beginning again. Hence, set the force_unstable
		// flag to true.
		//
		_automaton_flags |= FORCE_RECONFIGURATION;
		CMM_DEBUG_TRACE(("Setting FORCE_RECONFIGURATION flag "
		    "node_is_down\n"));
	}

	_primary = NODEID_UNKNOWN;
	_membership_change_flag = true;
	_nodes[_mynodeid].join_occurred.remove_node(nodeid);

	_state_change_cv.broadcast();
	_dump_state(0);

}

//
// Mark a node up.
//
// This routine is called by the automaton whenever it wants to mark
// a node as having come up. This could be when the transport subsystem
// indicates that a node has become reachable or when a message is
// received from a remote node with a new incarnation number.
//
// If the node is currently marked up (with a prior incarnation number),
// we mark the node down and and then up.
//
// This routine should be called with the state lock held.
//
// When a cluster shutdown is in progress the reconfiguration process is not
// triggered (because the cluster is stopping, nodes coming up are ignored).
//
void
automaton_impl::node_is_up(nodeid_t nodeid, sol::incarnation_num incn)
{
	bool		cluster_shutdown;

	ASSERT(_state_lock.lock_held());

	// Get the cluster shutdown flag to know if the scshutdown command
	// is in progress
	cluster_shutdown = cmm_impl::the().get_cluster_shutdown();

	if (cluster_shutdown) {
		CMM_TRACE(("Cluster shutdown is in progress: "
			"skipping node_is_up\n"));
		return;
	}

	//
	// If local node has been marked any of the following states,
	// S_DEAD, S_DOWN, S_UNKNOWN, S_STOPPED, S_ABORTED
	// or the incarnation number is strictly older,
	// then ignore the notification.
	//
	if ((MYSTATE <= S_ABORTED) ||
	    (incn < _nodes[nodeid].incarnation)) {
		return; // Old or irrelevant news! Ignore.
	}

	CMM_TRACE(("node %ld is up; new incn = %ld\n", nodeid, incn));

	if ((STATEVEC[nodeid] > S_DOWN) &&
	    (STATEVEC[nodeid] != S_UNKNOWN)) {
		node_is_down(nodeid);
	}

	STATEVEC[nodeid] = S_UNKNOWN;
	_nodes[nodeid].incarnation = incn;

	//
	// Stop the fenceoff timer for the node if it is running.
	// Since the node has rebooted, it is safe to take over
	// from the previous incarnation of the node.
	//
	stop_fenceoff_timer(nodeid);

	//
	// If we are in a step state, terminate any CMM client
	// callbacks that may be in progress. This allows us to
	// quickly start a new reconfiguration to integrate the
	// new node.
	//
	if ((MYSTATE >= CMM_STEP_1) && (MYSTATE < CMM_END_SYNC)) {
		cmm_impl::the_cmm_impl->\
			_cb_registry.terminate_trans(MYSEQNUM);
	}

	_primary = NODEID_UNKNOWN;
	_membership_change_flag = true;
	_nodes[_mynodeid].join_occurred.add_node(nodeid);

	_state_change_cv.broadcast();
	_dump_state(0);

}

//
// Tell the automaton that the local node stopped receiving messages from
// a remote node.  This condition is signaled by the communication subsystem
// (Path Manager) when communication to the remote node fails on all paths
// to the remote node.
//
void
automaton_impl::node_is_unreachable(
    nodeid_t		nodeid,
    sol::incarnation_num	incn,
    cmm::timeout_t		detection_delay,
    Environment&)
{
	_state_lock.lock();

	CMM_TRACE(("node_is_unreachable(%ld,%ld,%ld) called\n",
		nodeid, incn, detection_delay));

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	void		*generic_fault_argp = NULL;
	uint32_t	generic_fault_argsize = 0;
	void		*fault_argp = NULL;
	uint32_t	fault_argsize = 0;
	bool		blocked_for_hearsay = false;

	if (fault_triggered(FAULTNUM_CMM_NODE_DEPART_HEARSAY_1,
	    &generic_fault_argp, &generic_fault_argsize)) {
		//
		// This fault point is used to test
		// the following node depart hearsay scenario only :
		// Node C is rebooted. Remote node B will send message
		// to local node A to tell that node C has gone down.
		// But local node A will not consider node C as down
		// until node_is_unreachable() for node C is called on node A.
		//
		// Get the data about hearsay nodeids (nodeB and nodeC)
		// from the fault arguments. If this unreachable notification
		// is for nodeC, then we fire the fault point.
		//

		// Get the fault data we want from generic fault args
		FaultFunctions::get_fault_data_from_generic_arg(
		    generic_fault_argp, &fault_argp, fault_argsize);
		FaultFunctions::mc_sema_with_data_arg_t *mc_sema_argsp =
		    (FaultFunctions::mc_sema_with_data_arg_t *)fault_argp;

		// Check that we are getting the data that we expect
		ASSERT(mc_sema_argsp->shared_data_len == (uint32_t)
		    sizeof (FaultFunctions::delay_for_hearsay_arg_t));
		FaultFunctions::delay_for_hearsay_arg_t *argsp =
		    (FaultFunctions::delay_for_hearsay_arg_t *)
		    (mc_sema_argsp->shared_data);

		if (argsp->nodeC == nodeid) {
			//
			// We could block (sema_p) at this fault point to wait
			// for nodeB to tell us that nodeC went down.
			// So leave lock for other threads
			// to work on automaton_impl object.
			//
			_state_lock.unlock();

			FAULTPT_KCMM(FAULTNUM_CMM_NODE_DEPART_HEARSAY_1,
			    FaultFunctions::generic);

			blocked_for_hearsay = true;

			//
			// We want to introduce a delay here so that
			// we have a situation where other nodes think
			// nodeC has gone down but local nodeA has not
			// yet marked nodeC as down.
			// As a result, local node automaton will
			// wait in begin state until local nodeA
			// marks nodeC as down (indefinite wait).
			// So we can introduce an arbitrary delay here;
			// say sleep for 5 seconds.
			//
			os::usecsleep((os::usec_t)(5 * MICROSEC));

			// Grab lock before proceeding with operation
			_state_lock.lock();
		}
	}
#endif

	if ((STATEVEC[nodeid] <= S_DOWN) ||
	    (incn < _nodes[nodeid].incarnation) ||
	    (MYSTATE <= S_ABORTED)) {
		_state_lock.unlock();
		return; // Old or irrelevant news! Ignore.
	}

	_nodes[nodeid].detection_delay = detection_delay;

	//
	// Call the internal routine to mark a node down.
	//
	node_is_down(nodeid);

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
	// Let the Fault Injection Framework know that a node went
	// unknown
	FaultFunctions::signal_unknown(nodeid);
#endif

	//
	// A split brain timer must be started to declare the
	// non-communicating node dead. If this node had operational
	// quorum, it is expected that it should be able to re-assert
	// quorum in the fenceoff timeout.
	//
	// If this node is booting and not had operational quorum yet,
	// then there is no need to start the split brain timer since
	// it was never part of the cluster (it cannot run into split
	// brain situation with the node that has become unreachable).
	//
	if (!FIRST_TIME_UP) {
		start_split_brain_timer(nodeid);
	}

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
	//
	// This is related to the node depart hearsay fault scenario
	// where nodeB tells us that nodeC went down,
	// but we do not mark nodeC as down
	// until after we get a node_is_unreachable() call for node C.
	// If we reach here, it means we have completed our delay
	// in node_is_unreachable() for node C.
	// Here we mark that this event occurred first before
	// the automaton completes its begin state action.
	//
	// We do not currently block at this fault point.
	// So we do not need to drop the lock.
	// If a new test case expects us to block here,
	// then we should leave lock and then fire the fault point.
	//
	if (blocked_for_hearsay) {
		FAULTPT_KCMM(FAULTNUM_CMM_NODE_DEPART_HEARSAY_3,
		    FaultFunctions::generic);
	}
#endif

	_state_lock.unlock();
}

//
// Tell the automaton that the local node has started receiving messages from
// a remote node that was previously marked unreachable. This call is made
// by the Path Manager when connection is established with the remote node.
//
// This routine simply calls node_is_up().
//
void
automaton_impl::node_is_reachable(nodeid_t nodeid, sol::incarnation_num incn,
    Environment&)
{
	_state_lock.lock();

	CMM_TRACE(("node_is_reachable(%ld,%ld); current incn = %ld\n",
		nodeid, incn, _nodes[nodeid].incarnation));

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	void		*generic_fault_argp = NULL;
	uint32_t	generic_fault_argsize = 0;
	void		*fault_argp = NULL;
	uint32_t	fault_argsize = 0;
	bool		blocked_for_hearsay = false;

	if (fault_triggered(FAULTNUM_CMM_NODE_JOIN_HEARSAY_1,
	    &generic_fault_argp, &generic_fault_argsize)) {
		//
		// This fault point is used to test
		// the following node join hearsay scenario only :
		// Node C boots up. Remote node B will send message to
		// local node A saying that node C has come up with a new incn.
		// But local node A will not consider node C
		// as up with a new incn, until node_is_reachable()
		// for node C is called on node A.
		//
		// Get the data about hearsay nodeids (nodeB and nodeC)
		// from the fault arguments. If this reachable notification
		// is for nodeC, then we fire the fault point.
		//

		// Get the fault data we want from generic fault args
		FaultFunctions::get_fault_data_from_generic_arg(
		    generic_fault_argp, &fault_argp, fault_argsize);
		FaultFunctions::mc_sema_with_data_arg_t *mc_sema_argsp =
		    (FaultFunctions::mc_sema_with_data_arg_t *)fault_argp;

		// Check that we are getting the data that we expect
		ASSERT(mc_sema_argsp->shared_data_len == (uint32_t)
		    sizeof (FaultFunctions::delay_for_hearsay_arg_t));
		FaultFunctions::delay_for_hearsay_arg_t *argsp =
		    (FaultFunctions::delay_for_hearsay_arg_t *)
		    (mc_sema_argsp->shared_data);

		if (argsp->nodeC == nodeid) {
			//
			// We could block (sema_p) at this fault point
			// waiting for nodeB to tell us that nodeC is up.
			// So leave lock for other threads
			// to work on the automaton_impl object.
			//
			_state_lock.unlock();

			FAULTPT_KCMM(FAULTNUM_CMM_NODE_JOIN_HEARSAY_1,
			    FaultFunctions::generic);

			blocked_for_hearsay = true;

			//
			// We introduce a delay here to have the situation
			// where other nodes think that nodeC has booted up
			// but local node has not marked nodeC as up.
			// The effect would be that local node automaton
			// gets held up in begin state due to disagreements
			// with other nodes on the joining node set,
			// and hence waits for the max_detection_delay period.
			// So lets do a sleep for half of
			// the max_detection_delay period.
			// max_detection_delay is in milliseconds.
			//
			unsigned long max_detection_delay = 1000;
			for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
				unsigned long detection_delay =
				    (unsigned long)pm_get_max_delay_funcp(i);
				if (detection_delay > max_detection_delay) {
					max_detection_delay = detection_delay;
				}
			}
			if (max_detection_delay > LONG_MAX/1000) {
				max_detection_delay = LONG_MAX/1000;
			}
			os::usecsleep((os::usec_t)
			    ((max_detection_delay * (MICROSEC/MILLISEC)) / 2));

			// Grab lock before proceeding with operation
			_state_lock.lock();
		}
	}
#endif

	node_is_up(nodeid, incn);

	clconf_get_nodename(nodeid, nodename);

	//
	// SCMSGS
	// @explanation
	// The cluster can communicate with the specified node. A node becomes
	// reachable before it is declared up and having joined the cluster.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) _nodes[nodeid].msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CMM: Node %s (nodeid: %ld, incarnation #: %ld) has become "
	    "reachable.", nodename, nodeid, incn);

#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
	//
	// This is related to the node join hearsay fault scenario
	// where nodeB tells us that nodeC is up with a new incn,
	// but we do not mark nodeC as up with a new incn,
	// until after we get a node_is_reachable() call for node C.
	// If we reach here, it means we have completed our delay
	// in node_is_reachable() for node C.
	// Here we mark that this event occurred first before
	// the automaton completes its begin state action.
	//
	// We do not currently block at this fault point.
	// So we do not need to drop the lock.
	// If a new test case expects us to block here,
	// then we should leave lock and then fire the fault point.
	//
	if (blocked_for_hearsay) {
		FAULTPT_KCMM(FAULTNUM_CMM_NODE_JOIN_HEARSAY_3,
		    FaultFunctions::generic);
	}
#endif

	_state_lock.unlock();
}

//
// Mark a node as shutting down.
//
// The safe_to_takeover_from() functionality is used to inform apps
// that they are guaranteed that the other node is dead. Nodes which
// shutdown cleanly run the stop transition and then guarantee that
// they will be dead after conf.node_halt_timeout.
//
// This routine is called by the automaton when a message is received
// from the remote node indicating that it has shut down. The shutdown
// sequence on a node enables a failfast timer with a timeout equal
// to conf.node_halt_timeout. The local node should also impose the
// same timeout before declaring the node dead, thereby preventing
// a window where the local node thinks that the remote node is dead,
// but the remote node is not dead. If this timeout were not imposed on
// the local node then the local node would immediately place the remote
// node in S_DEAD state and then a data service would get a "true" from
// the safe_to_takeove() method invoked for the remote node. This would
// lead to the data service being active on both nodes simultaneosly.
//
// This routine should be called with the state lock held.
//
void
automaton_impl::node_is_shutting_down(nodeid_t nodeid)
{

	ASSERT(_state_lock.lock_held());

	CMM_TRACE(("node_is_shutting_down(%ld) called\n", nodeid));

	if (STATEVEC[nodeid] != S_DEAD) {
		start_halt_timer(nodeid);
	}
}


//
// Send automaton's state variables to other node(s).
//
void
automaton_impl::send_msg(nodeid_t nodeid)
{
	cmm::message_t msg;
	nodeset destination_nodes;
	Environment e;

	ASSERT(_state_lock.lock_held());

	CMM_DEBUG_TRACE(("send msg to nodeid[%ld]\n", nodeid));
	destination_nodes.add_node(nodeid);
	prepare_msg(msg);
	_commp->send_all(msg, destination_nodes.bitmask(), e);
}

void
automaton_impl::broadcast_msg()
{
	cmm::message_t msg;
	nodeset destination_nodes;
	Environment e;

	ASSERT(_state_lock.lock_held());

	CMM_DEBUG_TRACE(("broadcast msg to all\n"));
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if ((STATEVEC[i] != S_DEAD) && (i != _mynodeid)) {
			//
			// destination nodes
			//
			destination_nodes.add_node(i);
		}
	}
	prepare_msg(msg);
	_commp->send_all(msg, destination_nodes.bitmask(), e);
}

#ifdef DEBUG
#ifdef	_KERNEL_ORB
//
// Send cluster_stop message to all the other nodes.
//
void
automaton_impl::broadcast_stop_msg(void)
{
	nodeset destination_nodes;
	Environment e;
	nodeid_t node;
	ASSERT(_state_lock.lock_held());

	CMM_DEBUG_TRACE(("broadcast cluster stop msg to all\n"));
	for (node = 1; node <= NODEID_MAX; node++) {
		if ((STATEVEC[node] != S_DEAD) && (node != _mynodeid)) {
			//
			// destination nodes
			//
			destination_nodes.add_node(node);
		}
	}
	_commp->send_stop_all(destination_nodes.bitmask(), e);
}
#else // _KERNEL_ORB
void
automaton_impl::broadcast_stop_msg(void)
{
	ASSERT(0);
}
#endif // _KERNEL_ORB
#endif // DEBUG

void
automaton_impl::prepare_msg(cmm::message_t& msg)
{
	ASSERT(_state_lock.lock_held());

	msg.seqnum = MYSEQNUM;
	msg.curr_membership = MYMEMBERSHIPSET.bitmask();
	msg.heard_from = _nodes[_mynodeid].heard_from.bitmask();
	msg.rebooting_nodes = _rebooting_nodes.bitmask();
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		msg.quorum_devices_owned[i] =
			_nodes[i].quorum_result[i].quorum_devices_owned;
		msg.we_have_been_preempted[i] =
			_nodes[i].quorum_result[i].we_have_been_preempted;
		msg.incarnations[i] = _nodes[i].incarnation;
		msg.state_list[i] = STATEVEC[i];
		//
		// add heard_from_vector information
		//
		msg.heard_from_vector[i] =
			_nodes[i].heard_from.bitmask();
	}
	//
	// add the join_occurred information
	//
	msg.join_occurred = _nodes[_mynodeid].join_occurred.bitmask();

	//
	// add cluster shutdown flag
	//
	msg.cluster_shutdown = cmm_impl::the().get_cluster_shutdown();
	//
	// add initial_view_number information
	//
	msg.initial_view_number = MYIVN;
}

//
// The following *_state functions are called from the transitions thread.
//

//
// Called before the "stop" transition.
//
void
automaton_impl::stop_state()
{
	_state_lock.lock();
	// XXX _membership.empty();
	check_abort();
	MYSTATE = S_STOP;
	_dump_state(1);
	_state_change_cv.broadcast();
	broadcast_msg();
	_state_lock.unlock();
}

//
// Called when the "stop" transition has completed.
//
void
automaton_impl::stopped_state()
{
	_state_lock.lock();
	check_abort();
	MYSTATE = S_STOPPED;
	//
	// BugId 4283729
	// After the local node's stop transition is called, the
	// shutdown sequence goes to sleep for SHUTDOWN_CMM_LAG
	// time interval. After this sleep, the remaining shutdown
	// steps will be executed. The other nodes need to be able
	// to safely assume that this node has completed this
	// sequence and indeed shutdown after a given timeout
	// subsequent to receiving the shutdown msg from this
	// node. To ensure this guarantee, start a corresponding
	// failfast timer at this node, and use the failfast timeout
	// as the wait interval at the other nodes. If the shutdown
	// is not completed within this time interval, this node will
	// panic, and the other node can safely takeover its services
	// at that point. Use a pseudo-failfast mechanism here since
	// at this point in the shutdown sequence, failfast has been
	// disabled.
	//
	CMM_TRACE(("Starting local shutdown failfast timer\n"));
	start_halt_timer(_mynodeid);

	_dump_state(1);
	broadcast_msg();
	_state_lock.unlock();
}

//
// Called at the beginning of the "abort" transition".
//
void
automaton_impl::abort_state()
{
	_state_lock.lock();
	// XXX _membership.empty();
	MYSTATE = S_ABORT;
	_automaton_flags |= NODE_IS_ABORTING;
	_dump_state(1);
	_state_change_cv.broadcast();
	broadcast_msg();
	_state_lock.unlock();
}

//
// Called when the "abort" transition has completed.
//
void
automaton_impl::aborted_state()
{
	_state_lock.lock();
	MYSTATE = S_ABORTED;
	_dump_state(1);
	broadcast_msg();
	_state_lock.unlock();
}

//
// Return to the "begin" state and re-execute the cluster reconfiguration
// steps.  automaton_impl::reconfigure is called when the reconfigure
// method of the CMM control interface is invoked.
//
void
automaton_impl::reconfigure()
{
	_state_lock.lock();

	// allow "cmm_ctl -r" to dump state info
	uint_t save = cmm_trace_mask;
	cmm_trace_mask = (uint_t)-1; // turn on everything
	_dump_state(0);
	cmm_trace_mask = save;

	if (MYSTATE > CMM_BEGIN) {
		_automaton_flags |= FORCE_RECONFIGURATION;
		_state_change_cv.broadcast();
	}
	_state_lock.unlock();
}

//
// This function contains the common checks for synchronizing
// the local node with other cluster member nodes.
//
// The return value designates the following conditions:
//	SWO_SYNC - all live nodes have reached the current state
//	SWO_WAIT - some nodes haven't yet reached the current state
//	SWO_UNSTABLE - the cluster membership has become unstable
//		(when a node has died or a new node is joining the cluster,
//		 or the reconfiguration sequence numbers don't match, or the
//		 proposed memberships don't match).
//	SWO_FORCE - got a node_is_down notification in begin state.
//	SWO_CONTINUE - To continue performing other checks to detect
//			stability of the cluster.
//

automaton_impl::swo_stat_t
automaton_impl::sync_with_others_common()
{
	uint16_t curr;
	nodeid_t  node;

	//
	// Make sure that the automaton state lock is held.
	//
	ASSERT(_state_lock.lock_held());

	curr = MYSTATE;

	CMM_DEBUG_TRACE(("sync_with_others: [%s][%s][%s][%s]-[0x%llx]\n",
	    _state_string(STATEVEC[1]),
	    _state_string(STATEVEC[2]),
	    _state_string(STATEVEC[3]),
	    _state_string(STATEVEC[4]),
	    MYMEMBERSHIPSET.bitmask()));
	//
	// Check that all node members agree on the reconfiguration
	// sequence number.
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		if (MYMEMBERSHIPSET.contains(node) &&
		    _nodes[node].reconfig_seqnum != MYSEQNUM) {
			CMM_DEBUG_TRACE(("	: 1 - node %ld "
			    "has a different seq#; mine = %lld; "
			    "his = %lld.\n",
			    node, MYSEQNUM, _nodes[node].reconfig_seqnum));
			return (SWO_UNSTABLE);
		}
	}

	if (curr < CMM_BEGIN || curr > CMM_END_SYNC) {
		//
		// Local node is in one of the following states
		// S_DEAD, S_DOWN, S_UNKNOWN, S_STOPPED, S_ABORTED,
		// S_STOP, S_ABORT, CMM_EXIT, CMM_START, CMM_RETURN,
		// CMM_END_WAIT. These states are not stable states.
		//
		CMM_DEBUG_TRACE(("     : 2 \n"));
		return (SWO_UNSTABLE); 	// when node is aborting
	}

	if (_automaton_flags & FORCE_RECONFIGURATION) {
		//
		// The FORCE_RECONFIGURATION flag is set
		//
		CMM_DEBUG_TRACE(("     : 3- force reconfiguration\n"));
		return (SWO_FORCE);
	}

	//
	// Check for nodes in S_UNKNOWN state.
	//
	// This condition stays UNSTABLE during first boot until
	// we have decided on a state for every nodes, i.e., got a
	//
	// a. node_is_reachable(node_i),
	// b. receive_msg(node_k) says node_i is down,
	// c. boot delay timer expires (marks all node_i as DEAD)
	//
	// After the cmm reconfiguration is complete, when we start
	// the next cmm reconfiguration, the state vectors are not
	// cleared. Hence we know what states the nodes were in the
	// last reconfiguration at the begining of the new
	// reconfiguration.
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		if (STATEVEC[node] == S_UNKNOWN) {
			CMM_DEBUG_TRACE(("     : 4 - "
			    "node %ld unknown \n", node));
			return (SWO_UNSTABLE);
		}
	}


	//
	// Check that the set of currently live nodes agrees with
	// the value of MYMEMBERSHIPSET.
	// This condition will detect node join and node depart
	// events.
	//
	nodeset livenodes(row_to_live_nodes(_mynodeid));
	if (!livenodes.equal(MYMEMBERSHIPSET)) {
		CMM_DEBUG_TRACE(("     : 5 \n"));
		return (SWO_UNSTABLE);
	}

	//
	// Continue to check for other conditions.
	//
	return (SWO_CONTINUE);
}

//
// Synchronize the local node with other cluster member nodes in begin state.
//
automaton_impl::swo_stat_t
automaton_impl::sync_with_others_in_begin()
{
	uint16_t state;
	nodeid_t  node;
	swo_stat_t retval;

	//
	// Make sure that the automaton state lock is held
	//
	ASSERT(_state_lock.lock_held());

	//
	// Make sure we are in begin state
	//
	ASSERT(MYSTATE == CMM_BEGIN);

	retval = sync_with_others_common();

	if (retval != SWO_CONTINUE) {
		//
		// We have already identified that the cluster
		// is unstable. No need to check further.
		//
		return (retval);
	}

	//
	// Check if the connectivity matrix is complete.
	//
	// This condition is UNSTABLE until we hear from (i.e., received
	// a message from) all the nodes we do not see as DEAD nor DOWN.
	//
	// In particular, this condition is UNSTABLE between the period
	// of time where where we got a node_is_up(nodeA) but did not get
	// a receive_msg(nodeA) yet.
	//
	// Also, this condition is UNSTABLE during initial cluster boot
	// while some nodes status is still UNKNOWN.
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		if (STATEVEC[node] <= S_DOWN) {
			continue;
		}
		//
		// If we have not heard from the node, return unstable.
		//
		if (!_nodes[_mynodeid].heard_from.contains(node)) {
			CMM_DEBUG_TRACE(("     : 6 - "
			    "I have not heard from node %ld\n", node));
			return (SWO_UNSTABLE);
		}
		//
		// We are waiting for the other nodes to report
		// full information about the nodes of the cluster
		//
		for (nodeid_t node_j = 1; node_j <= NODEID_MAX; node_j++) {
			if (_nodes[node].state_vector[node_j] <= S_DOWN) {
				continue;
			}
			if (!_nodes[node].heard_from.contains(node_j)) {
				CMM_DEBUG_TRACE(("     : 6 - "
				    "node %ld has not heard from "
				    "node %ld\n", node, node_j));
				return (SWO_UNSTABLE);
			}
		}
	}

	//
	// Since we are in begin state the _primary should
	// not have been selected.
	//
	ASSERT(_primary == NODEID_UNKNOWN);

	CMM_DEBUG_TRACE(("     : Global sync... in begin\n"));

	for (node = 1; node <= NODEID_MAX; node++) {
		if (!MYMEMBERSHIPSET.contains(node)) {
			continue;
		}

		state = STATEVEC[node];
		retval = state_sync(state);
		if (retval == SWO_WAIT || retval == SWO_UNSTABLE) {
			//
			// SWO_WAIT:
			// Found a node which is
			// lagging behind.
			// Need to wait for the node.
			// 	OR
			// SWO_UNSTABLE:
			// Membership has changed or a new
			// reconfiguration has been started
			// by some other node.
			//
			return (retval);
		}
	}

	return (retval);

}

//
// Synchronize the local node with other cluster member nodes.
// This function should not be used to synchronize while in begin state.
//
automaton_impl::swo_stat_t
automaton_impl::sync_with_others()
{
	uint16_t state;
	nodeid_t  node;
	swo_stat_t retval;

	//
	// Make sure that the automaton state lock is held.
	//
	ASSERT(_state_lock.lock_held());

	//
	// Make sure we are not in begin state
	//
	ASSERT(MYSTATE != CMM_BEGIN);

	retval = sync_with_others_common();

	if (retval == SWO_FORCE) {
		//
		// The force reconfiguration flag was set.
		// We need to restart the automaton reconfiguration.
		//
		return (SWO_UNSTABLE);
	}

	if (retval != SWO_CONTINUE) {
		//
		// We have already identified that the cluster
		// is unstable. No need to check further.
		//
		return (retval);
	}

	//
	// Now scan the states of the live nodes to check if they all
	// reached the current state.  Note that we return SWO_UNSTABLE
	// if we detect a node in a state indicating that the node is
	// not following the lock-step reconfiguration protocol (this
	// happens, for instance, when a node is leaving the cluster.
	//
	// If message from primary then follow primary as we do not
	// receive individual cmm messages from other node! (state vector
	// will be out of sync for a while but we do not use this info
	// anyway for sync_with_other decision) and any major event
	// (node join/leave) will trigger cross exchange and state refresh
	//
	if ((_primary != NODEID_UNKNOWN) && (_primary != _mynodeid) &&
	    (STATEVEC[_primary] >= CMM_QCHECK)) {
		CMM_DEBUG_TRACE(("     : Follow primary... \n"));

		state = STATEVEC[_primary];
		retval = state_sync(state);

	} else {
		CMM_DEBUG_TRACE(("     : Global sync...\n"));
		for (node = 1; node <= NODEID_MAX; node++) {
			if (!MYMEMBERSHIPSET.contains(node)) {
				continue;
			}

			state = STATEVEC[node];
			retval = state_sync(state);
			if (retval == SWO_WAIT || retval == SWO_UNSTABLE) {
				//
				// SWO_WAIT:
				// Found a node which is
				// lagging behind.
				// Need to wait for the node.
				//	 OR
				// SWO_UNSTABLE:
				// Membership has changed or a new
				// reconfiguration has been started
				// by some other node
				//
				return (retval);
			}
		}
	}

	return (retval);
}

//
// Synchronize local state with states of other nodes
// in the cluster.
// We check that all nodes are following the lock-step
// reconfiguration protocol.
//
// Takes input as the state as seen by the local node
// for the other nodes.
//
automaton_impl::swo_stat_t
automaton_impl::state_sync(uint16_t state)
{
	uint16_t curr;
	int	must_wait = 0;
	swo_stat_t retval;

	//
	// Make sure that the automaton state lock is held.
	//
	ASSERT(_state_lock.lock_held());

	curr = MYSTATE;

	//
	// If the state of the node precedes my state and not
	// the immediate predecessor then return UNSTABLE.
	//
	if ((state < curr) && !is_previous(curr, state)) {
		CMM_DEBUG_TRACE(("	: 7- [SWO_UNSTABLE]\n"));
		return (SWO_UNSTABLE);
	} else if ((state > curr) && (!is_next(curr, state))) {
		//
		// node i is ahead of us. This is possible if
		// our node is just coming up and the other
		// node has not recognized us yet. It is also
		// possible that we have been eliminated due
		// to disconnects. In that case, we will come
		// to the same conclusion soon.
		//
		if (FIRST_TIME_UP) {
			CMM_DEBUG_TRACE(("	: 8- "
			    "[SWO_UNSTABLE]\n"));
			return (SWO_UNSTABLE);
		} else {
			//
			// We have been eliminated. Proceed
			// and we should discover it soon.
			//
			CMM_TRACE(("state_sync: node %ld "
			    "has eliminated us'.\n", _primary));
		}
	}

	if (is_previous(curr, state)) {
		must_wait = 1;
	}


	if (must_wait) {
		retval = SWO_WAIT;
		CMM_DEBUG_TRACE(("     : [SWO_WAIT]\n"));
	} else {
		retval = SWO_SYNC;
		CMM_DEBUG_TRACE(("     : [SWO_SYNC]\n"));
	}

	return (retval);
}

//
// Checks if CMM state 'state' precedes state 'mystate'.
// Returns true if 'state' is the immediate predecessor of 'mystate'.
//
bool
automaton_impl::is_previous(uint16_t mystate,
    uint16_t state)
{
	uint16_t i;
	ASSERT(state != CMM_INVALID_STATE);
	ASSERT(mystate != CMM_INVALID_STATE);
	for (i = 0; i < CMM_NUM_EVENT; ++i) {
		if (cmm_transition[state][i] == mystate) {
			//
			// There is a valid transition for event i
			//
			return (true);
		}
	}
	return (false);
}

//
// Checks if CMM state 'state' succeeds state 'mystate'.
// Returns true if 'state' is the immediate successor of 'mystate'.
//
bool
automaton_impl::is_next(uint16_t mystate,
    uint16_t state)
{
	uint16_t i;
	ASSERT(state != CMM_INVALID_STATE);
	ASSERT(mystate != CMM_INVALID_STATE);
	for (i = 0; i < CMM_NUM_EVENT; ++i) {
		if (cmm_transition[mystate][i] == state) {
			//
			// There is a valid transition for event i
			//
			return (true);
		}
	}
	return (false);
}

//
// Calculate the set of live nodes from a state matrix row.  This function is
// used to determine the set of nodes that the node 'row' considers to be alive.
//
nodeset
automaton_impl::row_to_live_nodes(const nodeid_t row)
{
	nodeid_t  i;
	nodeset rv;

	ASSERT(_state_lock.lock_held());

	for (i = 1; i <= NODEID_MAX; i++) {
		if (_nodes[row].state_vector[i] > S_UNKNOWN) {
			rv.add_node(i);
		}
	}

	return (rv);
}

//
// Return the current step number.  Return 0 if the local node is not
// in the CMM_END_SYNC state or in any of the step(N) states.
//
uint_t
automaton_impl::get_current_step()
{
	uint16_t mystate = MYSTATE;

	return ((mystate > CMM_QCHECK) && ((mystate < CMM_END_WAIT))?
	    _current_step : 0);
}

//
// Check if the local node has received the stop command or is aborting.
// This check is performed only by the transitions thread before returning
// from the automaton functions.
//
void
automaton_impl::check_stop_abort()
{
	ASSERT(_state_lock.lock_held());
	//
	// Abort has precedence over stop.
	//
	check_abort();

	if (_automaton_flags & STOP_LOCAL_NODE) {
		_state_lock.unlock();
		cmm_impl::the_cmm_impl->stop_sequence();
	}
}

//
// Check if the local node is aborting.
// This check is called only by the transitions thread.
//
void
automaton_impl::check_abort()
{
	ASSERT(_state_lock.lock_held());
	if (_automaton_flags & NODE_IS_ABORTING) {
		//
		// The caller (transitions_thread) must exit because
		// abort_thread is aborting the node.
		//
		_state_lock.unlock();
		cmm_impl::the_cmm_impl->dec_ref(EXIT);
	}
}


//
// Clear the connectivity matrix and the rebootee bitmask.
// These will be rebuilt as messages are received from other nodes.
//
void
automaton_impl::clear_connectivity_matrix()
{
	int		_first_time_up = FIRST_TIME_UP;
	nodeid_t	i;

	for (i = 1; i <= NODEID_MAX; i++) {
		_nodes[i].heard_from.empty();
	}

	for (i = 0; i <= NODEID_MAX; i++) {
		if (orb_conf::node_configured(i)) {
			for (nodeid_t j = 0; j <= NODEID_MAX; j++) {
				if (orb_conf::node_configured(j)) {
					_nodes[i].heard_from_vector[j].empty();
				}
			}
		}
	}

	//
	// We don't need to hear from ourselves.
	//
	_nodes[_mynodeid].heard_from.add_node(_mynodeid);
	_nodes[_mynodeid].heard_from_vector[_mynodeid].add_node(_mynodeid);

	_rebooting_nodes.empty();
	if (_first_time_up) {
		_rebooting_nodes.add_node(_mynodeid);
	}
}

//
// bool function that returns true if the cluster is fully connected
// and false if there are disconnects.
//
bool
automaton_impl::no_disconnects()
{
	nodeid_t	i;

	for (i = 1; i <= NODEID_MAX; i++) {
		if (_nodes[_mynodeid].heard_from.contains(i) &&
		    !_nodes[_mynodeid].heard_from.equal(_nodes[i].heard_from)) {
			return (false);
		}
	}
	//
	// All nodes I heard from have heard about the same set of nodes
	// as mine. There is thus no disconnect.
	//
	return (true);
}

//
// Increment the reconfiguration sequence number.
//
void
automaton_impl::incr_seqnum()
{
	if (_max_reconfig_seqnum > MYSEQNUM) {
		MYSEQNUM = _max_reconfig_seqnum;
	} else {
		MYSEQNUM += 1;
		_max_reconfig_seqnum = MYSEQNUM;
	}
	CMM_TRACE(("seqnum = %lld\n", MYSEQNUM));
	//
	// A new sequence number requires us to start building the
	// connectivity matrix anew.
	//
	clear_connectivity_matrix();
}

//
// Update the status in the begin state to start another
// round of synchronization.
//
void
automaton_impl::refresh_begin_state()
{
	check_stop_abort();
	MYMEMBERSHIPSET.set(row_to_live_nodes(_mynodeid));
	if (_max_reconfig_seqnum > MYSEQNUM) {
		incr_seqnum();
	}
	broadcast_msg();
}

//
// We check for the disagreements that exist between the
// nodes of the cluster. The disagreemnt here is calculated
// by  populating the _disagree_join_set
// Returns false is there are no disagreements
// Returns true if disagreements exist.
//
bool
automaton_impl::disagreements_in_join_set()
{

	bool		retval = false;
	nodeid_t	node;
	nodeid_t local_nodeid = orb_conf::local_nodeid();

	//
	// Lock should be held while calling this function.
	//
	ASSERT(_state_lock.lock_held());

	for (node = 1; node <= NODEID_MAX; node++) {

		//
		// No need to check for local node
		//
		if (node == local_nodeid) {
			continue;
		}

		//
		// No need to check for nodes which are not configured
		//
		if (!orb_conf::node_configured(node)) {
			continue;
		}

		//
		// In order to populate the node in the join set
		// we need to check
		// 1. Somebody else is saying that node is up and
		// local node has not heard from it.
		//
		if (!_nodes[local_nodeid].heard_from.contains(node)) {
			for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
				if (_nodes[i].heard_from.contains(node)) {
					CMM_DEBUG_TRACE((
					    "Remote Node %d can talk to Target"
					    " Node %d but local Node %d can"
					    " not talk\n",
					    i, node, local_nodeid));
					_disagree_join_set.add_node(node);
				}

			}
		}

	}

	if (_disagree_join_set.is_empty()) {
		//
		// Disagreements does not exist amongst the nodes.
		//
		CMM_DEBUG_TRACE(
		    ("Disagreements don't exist amongst the nodes.\n"));
		retval = false;
	} else {
		//
		// Disagreements exist amongst the nodes.
		//
		CMM_DEBUG_TRACE(("Disagreements exist amongst the nodes.\n"));
		retval = true;
	}

	return (retval);
}

//
// This function will decide if the partition
// in which the local node belongs should wait
// for the other partition in case of a split
// brain scenario
//
// Return Value:
//	true: 	This partition needs to wait
//		for the other partition before
//		it can proceed.
//	false:	This partition need not wait
//		for the other partition. It can
//		proceed
//
bool
automaton_impl::let_partition_wait()
{
	bool		retval = true;
	nodeid_t	node;
	nodeid_t 	local_nodeid = orb_conf::local_nodeid();

	// The number of nodes configured in the cluster
	uint32_t	num_nodes = 0;

	//
	// The number of nodes that the local node can
	// talk to including itself.
	//
	uint32_t	local_num_nodes = 0;

	//
	// Lock should be held while calling this function.
	//
	ASSERT(_state_lock.lock_held());

	//
	// Calculate the number of nodes configured
	// in the cluster. This is static membership
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		if (orb_conf::node_configured(node)) {
			// Increment the number of nodes
			num_nodes++;
		}
	}

	ASSERT(num_nodes > 0);
	ASSERT(num_nodes <= NODEID_MAX);

	//
	// Calculate  the number of nodes that the local
	// node can talk to. If there is a network partition
	// this will be the number of nodes belonging to
	// the partition to which the local node belongs
	// Note that the local node can always talk to
	// itself and hence contributes 1
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		if (_nodes[local_nodeid].heard_from.contains(node)) {
			local_num_nodes++;
		}
	}

	ASSERT(local_num_nodes > 0);
	ASSERT(local_num_nodes <= NODEID_MAX);

	//
	// For 4 node clusters, we will follow the
	// even division rule. (n/2)
	// For clusters from 5 to 64 nodes, we skew
	// the allowed size of partition to a little
	// less than half. We do this becuase the
	// smaller partition has enough nodes to handle
	// a second failure and also it is faster.
	// Hence, the cluster cmm reconfiguration
	// is quicker and there is less service
	// outage.
	//
	if (num_nodes <= 2) {
		retval = false;
	} else if (num_nodes <= 4) {
		if (local_num_nodes >= 2) {
			retval = false;
		}
	} else if (num_nodes <= 8) {
		if (local_num_nodes >= 3) {
			retval = false;
		}
	} else if (num_nodes <= 16) {
		if (local_num_nodes >= 6) {
			retval = false;
		}
	} else if (num_nodes <= 24) {
		if (local_num_nodes >= 10) {
			retval = false;
		}
	} else if (num_nodes <= 32) {
		if (local_num_nodes >= 14) {
			retval = false;
		}
	} else if (num_nodes <= 40) {
		if (local_num_nodes >= 18) {
			retval = false;
		}
	} else if (num_nodes <= 48) {
		if (local_num_nodes >= 22) {
			retval = false;
		}
	} else if (num_nodes <= 56) {
		if (local_num_nodes >= 26) {
			retval = false;
		}
	} else if (num_nodes <= 64) {
		if (local_num_nodes >= 30) {
			retval = false;
		}
	}

	CMM_DEBUG_TRACE(
	    ("automaton_impl::let_partition_wait: num_nodes = %d "
	    "local_num_node = %d retval = %d\n",
	    num_nodes, local_num_nodes, retval));

	return (retval);
}


#ifdef _KERNEL_ORB
// timer and incomplete connectivity handling for fencing off
// down nodes

//
// Check if the cluster is fully connected. If not, mark nodes down
// so that a fully-connected node remains.
//
void
automaton_impl::check_connectivity()
{
	nodeid_t	i, j;
	nodeset		i_membership;

	ASSERT(_state_lock.lock_held());

	CMM_DEBUG_TRACE(("check_connectivity() called.\n"));

	if (no_disconnects()) {
		return;
	}

	//
	// There are disconnects.
	//
	CMM_TRACE(("Connectivity matrix:\n"));
	for (i = 1; i <= NODEID_MAX; i++) {
		if (_nodes[i].heard_from.bitmask() != 0) {
			CMM_TRACE(("\theard_from[%ld] : 0x%llx\n",
				i, _nodes[i].heard_from.bitmask()));
		}
	}

	//
	// First, handle disconnects of our node with live nodes that
	// are not in our partition; that is, nodes that we are not
	// hearing from, but some other node in our partition has
	// heard from.
	//
	for (i = 1; i <= NODEID_MAX; i++) {
		if (!orb_conf::node_configured(i) ||
		    _nodes[_mynodeid].heard_from.contains(i)) {
			continue;
		}
		//
		// We have not heard from node i. Has any other node in
		// our partition heard from it?
		//
		for (j = 1; j <= NODEID_MAX; j++) {
			if (_nodes[j].heard_from.contains(i)) {
				//
				// Node j has heard from node i.
				// Therefore, we must be disconnected from
				// node i. Either node i or our node must
				// abort.
				//
#ifndef RFE_4833107
				//
				// still old version logic.
				//
				i_membership.empty();
				process_disconnect(_mynodeid, i,
				    _nodes[_mynodeid].heard_from,
				    i_membership);
#else
				i_membership.set(
				    _nodes[j].heard_from_vector[i]);
				CMM_TRACE(("\nnode %d's membership= "
					"0x%llx as seen by  node %d", i,
					i_membership.bitmask(), j));
				CMM_TRACE(("\nmynode = %d's membership="
					"0x%llx ", _mynodeid,
					_nodes[_mynodeid].
					heard_from.bitmask()));
				process_disconnect(_mynodeid, i,
				    _nodes[_mynodeid].heard_from,
				    i_membership);
#endif // RFE_4833107
				if (_automaton_flags & NODE_IS_ABORTING) {
					return; // going to abort soon
				} else {
					break; // handle the next down node
				}
			}
		}
	}

handle_remote_disconnects:

	//
	// Now, handle disconnects among members of our partition.
	//
	for (i = 1; i <= NODEID_MAX; i++) {

		if (!_nodes[_mynodeid].heard_from.contains(i)) {
			continue; // not a node in our partition
		}
		//
		// Node i is in our partition (i could be _mynodeid).
		// Is any other node in our partition unable to communicate
		// with it?
		//
		for (j = 1; j <= NODEID_MAX; j++) {
			if (_nodes[_mynodeid].heard_from.contains(j) &&
			    !_nodes[j].heard_from.contains(i)) {
				//
				// Node j which is in our partition says
				// that it hasn't heard from node i.
				// Either node i or node j must abort.
				//
				CMM_TRACE(("\nnode %d's membership= 0x%llx "
					"node %d membership = 0x%llx\n", i,
					_nodes[i].heard_from.bitmask(), j,
					_nodes[j].heard_from.bitmask()));
				process_disconnect(i, j, _nodes[i].heard_from,
				    _nodes[j].heard_from);

				if (_automaton_flags & NODE_IS_ABORTING) {
					return; // going to abort soon
				} else {
					//
					// Since our partition is now smaller,
					// we must process the disconnects
					// again.
					//
					goto handle_remote_disconnects;
				}
			}
		}
	}
}

//
// Handle the disconnect between nodes m and n. m could be the local node.
// One of the two nodes must abort. We apply the following rules to select
// a node to abort.
//
// Rule 1: If one node is rebooting and the other was a member of the cluster,
//	   the node that is rebooting must abort.
// Rule 2: The node with greater control of quorum device votes survives and
//	   the other node aborts.
// Rule 3: The node with higher node number aborts.
//
void
automaton_impl::process_disconnect(nodeid_t m, nodeid_t n,
    nodeset, nodeset)
{
	int		m_aborts = 0, n_aborts = 0;
	uint32_t	m_votes, n_votes;
	char 		*rule; // rule applied for aborting one node
	Environment	e;

	CL_PANIC(m != n);

	rule = "rebootee";
	if (_rebooting_nodes.contains(m) && !_rebooting_nodes.contains(n)) {
		m_aborts = 1;
		goto done;
	} else if (!_rebooting_nodes.contains(m) &&
	    _rebooting_nodes.contains(n)) {
		n_aborts = 1;
		goto done;
	}

	rule = "quorum";

	// don't access _quorum_p in userland CMM
	if (!CORBA::is_nil(_quorum_p)) {
		//
		// Give up _state_lock before calling quorum object methods.
		// It is safe to give up the state lock because the quorum
		// code provides its own locking, so it will give a
		// consistent snapshot of the quorum state. Also, this
		// (transitions) thread is the one that requests quorum-
		// related changes during reconfiguration, so there is no
		// possibility of conflict in that respect.
		//
		_state_lock.unlock();

#ifndef RFE_4833107
		//
		// still old version logic.
		//
		m_votes = _quorum_p->max_votes_of_partition(1LL << (m-1), e);
		n_votes = _quorum_p->max_votes_of_partition(1LL << (n-1), e);
#else
		//
		// New Rule 2: The node which belongs to a partition with
		// more votes survives and the other node aborts.
		//
		//
		// void
		// automaton_impl::process_disconnect(nodeid_t m, nodeid_t n,
		// nodeset m_membership, nodeset n_membership)
		//
		membership_bitmask_t	m_bitmask, n_bitmask;
		// calculate votes of partition nodes m and n belong to.
		m_bitmask = m_membership.bitmask();
		n_bitmask = n_membership.bitmask();
		m_votes = _quorum_p->max_votes_of_partition(m_bitmask, e);
		n_votes = _quorum_p->max_votes_of_partition(n_bitmask, e);
#endif // RFE_4833107

		// Acquire the lock back.
		_state_lock.lock();

		if (m_votes < n_votes) {
			m_aborts = 1;
			goto done;
		} else if (m_votes > n_votes) {
			n_aborts = 1;
			goto done;
		}
	}

	rule = "node number";
	if (m > n) {
		m_aborts = 1;
	} else {
		n_aborts = 1;
	}

done:
	CL_PANIC(m_aborts ^ n_aborts); // exactly one node must abort

	if ((m == _mynodeid) && m_aborts) {
		CMM_TRACE(("disconnected from node %ld; "
			"aborting using %s rule.\n", n, rule));
		//
		// SCMSGS
		// @explanation
		// Due to a connection failure between the local and the
		// specified node, the local node must be halted to avoid a
		// "split brain" configuration. The CMM used the specified
		// rule to decide which node to fail. Rules are: rebootee: If
		// one node is rebooting and the other was a member of the
		// cluster, the node that is rebooting must abort. quorum: The
		// node with greater control of quorum device votes survives
		// and the other node aborts. node number: The node with
		// higher node number aborts.
		// @user_action
		// The cause of the failure should be resolved and the node
		// should be rebooted if node failure is unexpected.
		//
		(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Disconnected from node %ld; "
		    "aborting using %s rule.", n, rule);
		_automaton_flags |= NODE_IS_ABORTING;
		cmm_impl::the_cmm_impl->signal_abort_thread();
	} else {
		//
		// A remote node (that is marked up by some node) will abort.
		//
		nodeid_t aborting_node = m_aborts ? m : n;

		CMM_TRACE(("CMM: nodes %ld and %ld are "
			"disconnected from each other; node %ld will abort "
			"using %s rule.\n", m, n, aborting_node, rule));
		//
		// SCMSGS
		// @explanation
		// Due to a connection failure between the two specified
		// non-local nodes, one of the nodes must be halted to avoid a
		// "split brain" configuration. The CMM used the specified
		// rule to decide which node to fail. Rules are: rebootee: If
		// one node is rebooting and the other was a member of the
		// cluster, the node that is rebooting must abort. quorum: The
		// node with greater control of quorum device votes survives
		// and the other node aborts. node number: The node with
		// higher node number aborts.
		// @user_action
		// The cause of the failure should be resolved and the node
		// should be rebooted if node failure is unexpected.
		//
		(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "CMM: Nodes %ld and %ld are "
		    "disconnected from each other; node %ld will abort "
		    "using %s rule.", m, n, aborting_node, rule);
		//
		// Mark the aborting node down.
		//
		node_is_down(aborting_node);
	}
}


//
// Start the split brain timer for a node that has become unreachable,
// provided the local node has not already received a message from
// the remote node indicating that it is shutting down. The split brain
// timer is used to wait for the node to die in case it is split brained.
//
// This routine should be called with the state lock held.
//
void
automaton_impl::start_split_brain_timer(nodeid_t nodeid)
{
	Environment		e;

	ASSERT(_state_lock.lock_held());
	CL_PANIC(nodeid != _mynodeid);

	//
	// BugId 4616302.
	// If the local node has already received a message from the
	// remote node indicating that the remote node is shutting
	// down, then the halt timer has already been armed. There
	// is no need to arm the split brain timer in this case.
	//
	if (_nodes[nodeid].fenceoff_timer != 0) {
		CL_PANIC(_nodes[nodeid].delta_timer);
		CMM_TRACE(("Halt timer is already armed, not starting "
			"split brain timer for node %ld\n", nodeid));
		return;
	}


	CMM_TRACE(("Starting split brain timer for node %ld\n", nodeid));

	_nodes[nodeid].fenceoff_timer = os::gethrtime() +
		MILLISECS_TO_NANOSECS(conf.node_fenceoff_timeout);
	_nodes[nodeid].delta_timer = false;
	_nodes[nodeid].must_reacquire_quorum = true;

	//
	// If the clock callback routine is not enabled, enable it now.
	//
	if (cmm_automaton_callout == NULL) {
		cmm_automaton_callout = clock_callback;
	}
}

//
// Stop the fenceoff timer for a node. Either the node indicated that
// it has shut down or another node told us that the node can be considered
// to be dead.
//
// This routine should be called with the state lock held.
//
void
automaton_impl::stop_fenceoff_timer(nodeid_t nodeid)
{
	bool	timer_still_required = false;

	ASSERT(_state_lock.lock_held());

	if (_nodes[nodeid].fenceoff_timer == 0) {
		return;
	}

	CMM_TRACE(("Stopping fenceoff timer for node %ld\n", nodeid));

	_nodes[nodeid].fenceoff_timer = (hrtime_t)0;

	//
	// If no timer is now active, stop the clock interrupt from calling
	// the automaton routine during each clock tick.
	//
	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		if (_nodes[node].fenceoff_timer != 0) {
			timer_still_required = true;
			break;
		}
	}
	if (!timer_still_required) {
		cmm_automaton_callout = NULL;
	}
}

//
// Set the shutdown halt timer for a node.
// When a node starts to shutdown, it uses this mechanism to ensure
// that it has shutdown after this timeout. The other nodes also use
// this same timeout for the shutting down node, after which they
// can declare the shutting down node to be in the S_DEAD state.
//
// This routine should be called with the state lock held.
//
void
automaton_impl::start_halt_timer(nodeid_t nodeid)
{

	ASSERT(_state_lock.lock_held());
	//
	// BugId 4616302
	// It is possible for a shutdown message from a remote node
	// to be received after a node_is_unreachable() is called
	// for it. In such a case, the split brain timer can be
	// disarmed since there is no possibility of a split brain
	// situation with a node that is shutting down. As part of
	// disarming the split brain timer, set the corresponding
	// must_reacquire flag to false.
	//
	// BugId 4624563
	// It is also possible for a remote node to send its shutdown
	// message more than once. The later/dup msgs should be
	// ignored.
	//
	// The difference between arming the split brain timer and the
	// halt timer is the setting of the delta_timer flag. Use that
	// flag to determine if a split brain timer or a halt timer
	// has been armed previously, if the value of fenceoff_timer is
	// non-zero. Note: This flag is also set to true in the case of
	// split brain scenarios, at the end of the split brain timeout,
	// when the detection delay comes into play. Considering that
	// the split brain timeout is in the order of mins and the halt
	// timer is in the order of 5 secs, there is a very low (almost
	// nil) probability of being in this method (due to a node
	// shutdown message) and the flag being true as a result of the
	// detection delay kicking in. Even if that does happen, ignoring
	// the later shutdown msg will not result in any incorrectness,
	// since the flag is already true, and the node is going to be
	// declared dead at the end of the detection delay interval,
	// which has the same order of magnitude as the halt timeout.
	//
	if (_nodes[nodeid].fenceoff_timer != 0) {
		CL_PANIC(nodeid != _mynodeid);

		if (_nodes[nodeid].delta_timer) {
			CMM_TRACE(("Halt timer is already armed for "
				"node %ld.\n", nodeid));
			return;
		} else {
			CMM_TRACE(("Split brain timer is already "
				"armed for node %ld, disarming it.\n",
				nodeid));
			_nodes[nodeid].must_reacquire_quorum = false;
		}
	}

	CMM_TRACE(("Starting halt timer for the %s node %ld\n",
		(nodeid == _mynodeid) ? "local": "", nodeid));

	_nodes[nodeid].fenceoff_timer = os::gethrtime() +
		MILLISECS_TO_NANOSECS(conf.node_halt_timeout);
	_nodes[nodeid].delta_timer = true;

	//
	// If the clock callback routine is not enabled, enable it now.
	//
	if (cmm_automaton_callout == NULL) {
		cmm_automaton_callout = clock_callback;
	}
}

//
// This method is called from the clock interrupt. The automaton uses timers
// in various places. The check below is to ensure that realtime callout
// processing is working correctly on this node. It is better to abort this
// node from the cluster otherwise as so many things will break anyway.
//
// The checking performed ensures that the cmm_timer_thread ran at least once
// during the last cmm_max_timer_period seconds.
//
void
automaton_impl::per_tick_processing()
{
	timer_lock.lock();
	hrtime_t current_time = os::gethrtime();

	if (current_time - last_update_time >
	    SECS_TO_NANOSECS(cmm_max_timer_period)) {
		//
		// SCMSGS
		// @explanation
		// This node is being aborted from the cluster as the CMM's
		// realtime callout monitor thread did not run for more than
		// the maximum allowed period. This is caused by heavy
		// interrupt processing load or a blocked interrupt.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Halting because realtime callout processing is"
		    " not working on this node. current_time %lld "
		    "last_update_time %lld", current_time, last_update_time);
	}
	timer_lock.unlock();
}

//
// Called from the clock interrupt by the failfast handler.
//
// static
void
automaton_impl::clock_callback()
{
	cmm_impl::the_cmm_impl->_autm.per_tick_processing();
}

//
// Check the health of the local node.
// If this node is unhealthy, it kills itself.
// Not all health checks are done in every reconfiguration.
//
// The health checks made are:
// (1) Does this node have a reasonable amount of free physical memory
// for allocating swap?
// (2) Is freemem above throttlefree?
// (3) Can this node 'ping' the IP addresses configured in CCR as ping targets?
//
// These health checks are done only if all the following conditions are true :
// - the reconfiguration must not be a shutdown reconfiguration
// - there is a remote node that has a non-zero configured vote count
// and that remote node is not in the same partition as the local node
// - either the local node is booting up for the first time;
// if not, then the local node sees that a remote node has left membership
// since the last reconfiguration
//
// Must be called with the state lock held.
//
void
automaton_impl::check_node_health()
{
	bool			need_to_check_node_health = false;
	bool			non_zero_vote_node_unreachable = false;
	nodeid_t		node;
	extern pgcnt_t		throttlefree;
	quorum::quorum_status_t	*qstatp;
	Environment		env;

	ASSERT(_state_lock.lock_held());

	CMM_TRACE(("in check_node_health\n"));

	//
	// We perform the health checks only under certain conditions.
	//

	//
	// Condition 1
	// -----------
	// If the CMM is shutting down (effectively means node
	// is shutting down), do not perform the health checks.
	// Check if either shutdown intent flag or shutdown commit flag
	// is set to determine if it is a shutdown reconfiguration.
	//
	if (cmm_impl::the().get_cluster_shutdown_intent() ||
	    cmm_impl::the().get_cluster_shutdown()) {
		return;
	}

	//
	// Condition 2
	// -----------
	// If all of the configured nodes with non-zero votes
	// are in the partition with the local node,
	// then we do not perform the health check.
	//

	// Get the quorum votes configuration
	_quorum_p->quorum_get_status(qstatp, env);
	ASSERT(env.exception() == NULL);	// No exception expected

	//
	// Check if there exists a node configured with non-zero votes,
	// that is not part of our membership bitmask.
	//
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (!orb_conf::node_configured(nid)) {
			// Node not configured; ignore
			continue;
		}

		if (MYMEMBERSHIPSET.contains(nid)) {
			// Node is part of membership view; ignore
			continue;
		}

		//
		// Configured node is not part of membership view;
		// search in quorum status for its configured vote count.
		//
		uint_t vote = 0;
		nodeid_t list_nid = 0;
		for (; list_nid < qstatp->nodelist.length(); list_nid++) {
			if (nid == qstatp->nodelist[list_nid].config.nid) {
				// Found node in list
				vote = qstatp->nodelist[list_nid].
				    config.votes_configured;
				break;
			}
		}
		//
		// If we do not find the configured nodeid in the list,
		// something is terribly wrong.
		//
		CL_PANIC(list_nid < qstatp->nodelist.length());

		if (vote != 0) {
			//
			// Found node that is not in membership view
			// and has a non-zero configured vote count.
			// Set flag and break out of loop.
			//
			non_zero_vote_node_unreachable = true;
			break;
		}
	}
	// Free the quorum status
	delete qstatp;

	if (!non_zero_vote_node_unreachable) {
		// All vote contributing nodes are in our partition
		return;
	}

	//
	// Condition 3
	// -----------
	// The aim of the health check is to prevent unhealthy nodes
	// from winning the race against healthy nodes to form cluster.
	// So we check if there is a possibility of another partition
	// racing with us; in other words, we do the health check
	// if we think some node has departed.
	//
	// Thus if no node has left since the last membership reconfiguration,
	// we don't need to check our node's health. New nodes might
	// be joining or some configuration changes could have
	// resulted in the reconfiguration.
	//
	// However if we are booting for the first time,
	// then we perform the health check.
	//
	// _membership has previous reconfiguration's membership incarnations.
	//
	if (FIRST_TIME_UP) {
		need_to_check_node_health = true;
	} else {
		for (node = 1; node <= NODEID_MAX; node++) {
			if (_membership.members[node] != INCN_UNKNOWN) {
				if (!MYMEMBERSHIPSET.contains(node)) {
					// Node has departed from membership
					need_to_check_node_health = true;
					break;
				}
			}
		}
	}
	if (!need_to_check_node_health) {
		return;
	}

#if defined(_KERNEL) && defined(DEBUG)

	void simulate_out_of_memory_condition(void);

	_state_lock.unlock();
	simulate_out_of_memory_condition();
	_state_lock.lock();

#endif // _KERNEL && DEBUG

#ifdef _KERNEL // do not do memory checks in unode

	// Do we have enough resident memory to run?
	if ((availrmem < tune.t_minarmem) || (freemem < throttlefree)) {

		//
		// Low on memory. Keep trying for a while to see if
		// memory gets freed. Sleep 1 second and try again.
		// Try for a duration = the maximum path manager node
		// down detection delay.
		//

		//
		// Compute delay.
		//
		uint32_t max_path_manager_delay = 0;	// in milliseconds
		uint32_t path_manager_delay;		// in milliseconds
		for (node = 1; node <= NODEID_MAX; node++) {
			if (STATEVEC[node] == S_DOWN) {
				path_manager_delay =
				    pm_get_max_delay_funcp(node);
				if (path_manager_delay > max_path_manager_delay)
					max_path_manager_delay =
					    path_manager_delay;
			}
		}

		//
		// Sleep for 1 second at a time and check if memory
		// has been freed.
		//
		for (uint_t i = 0; i < max_path_manager_delay/MILLISEC; i++) {

			CMM_TRACE(("check_node_health sleeping for 1 sec; "
			    "availrmem = %d, tune.t_minarmem = %d, "
			    "freemem = %d, throttlefree = %d\n",
			    availrmem, tune.t_minarmem,
			    freemem, throttlefree));

			_state_lock.unlock();
			os::usecsleep((os::usec_t)MICROSEC);
			_state_lock.lock();

			//
			// Return if we have enough resident memory now.
			//
			if ((availrmem > tune.t_minarmem) &&
			    (freemem > throttlefree)) {
				return;
			}
		}

		//
		// we were unable to recapture required memory
		//

		//
		// SCMSGS
		// @explanation
		// The local node does not have sufficient resident physical
		// memory, due to which it may declare other nodes down. To
		// prevent this action, the local node is going to halt.
		// @user_action
		// There may be other related messages that may indicate the
		// cause for the node having reached the low memory state.
		// Resolve the problem and reboot the node. If unable to
		// resolve the problem, contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Halting because this node is severely "
		    "short of resident physical memory; availrmem "
		    "= %ld pages, tune.t_minarmem = %ld pages, "
		    "freemem = %ld pages, throttlefree = %ld pages.",
		    availrmem, tune.t_minarmem, freemem, throttlefree);
	}

#endif // _KERNEL

#ifdef	_KERNEL
	//
	// PING check
	// ----------
	// If ping check is enabled, ping the configured targets
	// as a health check.
	//
	if (ping_check_enabled) {
		ping_health_check();
	}
#endif

}

//
// In case of potential split brains, allow larger partitions to win
// the race to quorum devices. Should do nothing if there are no
// quorum devices configured.
//
// If there are more nodes down than in the current partition, this
// function sleeps as many seconds as the are down nodes. Otherwise,
// the function returns without sleeping. In the computation of down
// nodes, nodes that are declared dead are not counted.
//
// This function should be called before trying to grab quorum disks.
//
// Must be called with the state lock held.
//
//
void
automaton_impl::delay_in_case_of_split_brain()
{
	uint_t num_down_votes = 0;	// # votes potentially split brained
	uint_t num_up_votes = 0;	// # votes in current partition
	nodeid_t node;
	nodeid_t	i;

	ASSERT(_state_lock.lock_held());

	CMM_TRACE(("in delay_in_case_of_split_brain\n"));

	//
	// If the cluster is booting (all members are rebootees), there is
	// no need to delay. The delay is meant for split brain situations
	// that develop after the cluster is operational.
	//
	if (MYMEMBERSHIPSET.equal(_rebooting_nodes)) {
		return;
	}

	quorum::quorum_status_t *qp;
	Environment e;

	// Give up _state_lock before calling quorum object methods.
	_state_lock.unlock();

	//
	// We don't need to check the exception because it is a local call
	// that doesn't throw any exceptions.
	//
	_quorum_p->quorum_get_status(qp, e);

	// Reacquire _state_lock.
	_state_lock.lock();

	//
	// Check if there are quorum device configured. We only need to delay
	// if there could be a race for a QD.
	//
	uint_t num_quorum_devices;
	num_quorum_devices = qp->quorum_device_list.length();

	if (num_quorum_devices == 0) {
		delete qp;
		return; // no need to delay
	}

	//
	// Count the quorum votes held by members of this partition and
	// votes held by down nodes.
	// When quorum_get_status constructs the nodelist, it only adds
	// nodes that are configured. So, if there is a hole in the node
	// numbering, the node numbers will not correspond to the ordering
	// of the nodelist. So, use an index i to iterate through the nodelist,
	// but explicitly get the nodeid for each element in the list to check
	// for membership and status.
	//
	for (i = 0; i < qp->nodelist.length(); i++) {
		node = qp->nodelist[i].config.nid;
		if (MYMEMBERSHIPSET.contains(node)) {
			// Node is in current membership.
			num_up_votes += qp->nodelist[i].config.
				votes_configured;
		} else if (STATEVEC[node] == S_DOWN) {
			// Node is considered down. (not dead)
			num_down_votes += qp->nodelist[i].config.
				votes_configured;
		}
	}

	CMM_TRACE(("delay_in_case_of_split_brain: %d votes up, %d down.\n",
		num_up_votes, num_down_votes));

	delete qp;

	if (num_up_votes >= num_down_votes) {
		return; // no need to delay
	} else {
		//
		// There are quorum devices configured, and there are more
		// votes held by down nodes than are in our partition.
		// So, there could be a race for the quorum devices. Let the
		// larger partition win by sleeping for a while.
		//

		//
		// SCMSGS
		// @explanation
		// In the case of potential split brain scenarios, the CMM
		// allows larger partitions to win the race to acquire quorum
		// devices by forcing the smaller partitions to sleep for a
		// time period proportional to the number of nodes not in that
		// partition.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) _nodes[_mynodeid].msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		    "CMM: Reconfiguration delaying for %d milliseconds to "
		    "allow larger partitions to win race for quorum "
		    "devices.", num_down_votes);

		_state_lock.unlock();
		// Sleeping for 1 millisec for each node that is down
		os::usecsleep((os::usec_t)num_down_votes * MILLISEC);
		_state_lock.lock();
	}
}

#else // !_KERNEL_ORB

void
automaton_impl::check_connectivity()
{
	ASSERT(_state_lock.lock_held());

	//
	// In userland, if there are disconnects, then block until
	// that situation is fixed by the kernel CMM and the
	// cluster is reduced to a completely connected graph.
	//
	while (!no_disconnects()) {
		// Wait till there is a state change.
		_state_change_cv.wait(&_state_lock);
	}
}

void
automaton_impl::start_split_brain_timer(nodeid_t)
{
	// no-op in userland CMM
}

void
automaton_impl::stop_fenceoff_timer(nodeid_t)
{
	// no-op in userland CMM
}

void
automaton_impl::start_halt_timer(nodeid_t)
{
	// no-op in userland CMM
}


// static
void
automaton_impl::clock_callback()
{
	// no-op in userland CMM
}

void
automaton_impl::check_node_health()
{
	// no-op in userland CMM
}

void
automaton_impl::delay_in_case_of_split_brain()
{
	// no-op in userland CMM
}

#endif // _KERNEL_ORB

//
// Boot_delay thread. It waits for some time (boot_delay) and declares
// down nodes that are configured but not yet booted. The state of
// such nodes is initialized to S_UNKNOWN. Until they are declared down,
// the automaton will not advance past the CMM_BEGIN state. The Path
// Manager and the userland node monitor start watching a node only after
// the  starts talking to us.
//
// static
void
automaton_impl::boot_delay_thread(void *arg)
{
	automaton_impl *automp = (automaton_impl *)arg;

	CMM_TRACE(("boot_delay thread starting\n"));
	automp->boot_delay_work();
	CMM_TRACE(("boot_delay thread exiting\n"));
}

//
// Perform the work for the boot_delay thread.
//
void
automaton_impl::boot_delay_work()
{
	os::systime	boot_delay; 		/* in microseconds */
	nodeid_t	i;
	nodeset		local_heard_from;	/* copy of nodes heard from */
	bool		heard_from_changed = true;
	bool		nodes_unheard_from = true;
	Environment	e;
#ifndef _KERNEL_ORB
	bool		first_iter = true;
#endif

	local_heard_from.empty();

	//
	// BugId 4294221
	// If two nodes are unable to communicate while booting up,
	// the PM will not time out the connection and call
	// node_is_unreachable() since the PM has initialized path
	// states to be unreachable. To fix the above bug, CMM should
	// declare a node down if it has not heard from the remote
	// node directly after the boot delay expires. However, this
	// will include race conditions where the remote node is just
	// coming up and in the process of contacting the local node
	// - in such cases, the remote node would be declared down
	// even though the path is ok. This situation can be improved
	// by using an "iterative" boot delay approach, where as long
	// as there is a new node directly heard from in an iteration,
	// the local node goes through another boot delay iteration
	// to hear from the not-heard-from nodes, if any. If in an
	// iteration, there are no new nodes heard from, then the local
	// node may go ahead and declare the unheard nodes down. When
	// considering unheard-from nodes, only the ones indirectly
	// heard from (ie valid incno's) are considered.
	//
	// The "heard_from" bitmask maintained for the nodes by the
	// automaton can not be used in the above determination since
	// a reconfiguration may happen while this thread is sleeping,
	// and that will reset this bitmask. A remote node's state
	// can be used instead - the state of a remote node (B) stays
	// as S_UNKNOWN when the local node (A) is coming up until it
	// hears from node B, and it is also set to S_UNKNOWN when
	// node B goes down, and comes back up and talks to a third
	// node (C) which tells node A about node B coming
	// up. The check therefore for state being S_UNKNOWN
	// also includes the second condition, but that check is
	// still ok, since if the rebooting node does not contact
	// the local node directly within the boot delay timeout,
	// the PM will declare it unreachable anyway.
	//
	// If a remote node was not heard from by any other node in the
	// cluster, it will be declared down in the first iteration.
	// If that remote node comes up later, that state change for it
	// will be handled correctly by the automaton since the remote
	// node will have a new incno etc.
	//
	while ((heard_from_changed) && (nodes_unheard_from)) {
		heard_from_changed = false;
		nodes_unheard_from = false;

		// Sleep until boot_delay expires.
		os::usecsleep((os::usec_t)
		    (conf.boot_delay * (os::usec_t)(MICROSEC / MILLISEC)));

#ifndef _KERNEL_ORB
		//
		// BugId 4365390
		// In userland, wait until initial handshake is complete
		// before declaring non-communicating nodes down.
		// This is only necessary in userland since in this case,
		// there is no quorum determination to ensure that at most
		// one cluster survives in case of communication delays
		// between nodes. Only need to do this during the first iter.
		//
		if (first_iter) {
			cm_comm_impl::the().wait_till_init_handshake_done();
			first_iter = false;
		}
#endif

		// Acquire _state_lock before looking at node states.
		_state_lock.lock();

		//
		// Mark down nodes that we have still not heard from. There is
		// no need to start the fenceoff timer (as done in the
		// node_is_unreachable function). Fence off actions need to be
		// taken only if a node was connected to us and then broke off.
		// BugId 4277170: If we heard about a node indirectly from
		// another node, check if the node is configured. If not, we
		// have a misconfiguration problem and we should halt.
		//
		for (i = 1; i <= NODEID_MAX; i++) {
			// The node_is_down() is not necessary except
			// in the first boot delay iteration, but it
			// does not matter to leave it in there, since
			// node_is_down() will return if the state was
			// already S_DOWN or lower.
			if (_nodes[i].incarnation == INCN_UNKNOWN) {
				node_is_down(i);
				continue;
			}
			if (!orb_conf::node_configured(i)) {
				//
				// We must have heard about this node from a
				// third node. We have no configuration info
				// about this node.
				//
				CL_PANIC(STATEVEC[i] == S_UNKNOWN);
				//
				// SCMSGS
				// @explanation
				// The local node has no configuration
				// information about the specified node. This
				// indicates a misconfiguration problem in the
				// cluster. The
				// /etc/cluster/ccr/global/infrastructure
				// table on this node may be out of date with
				// respect to the other nodes in the cluster.
				// @user_action
				// Correct the misconfiguration problem or
				// update the infrastructure table if out of
				// date, and reboot the nodes. To update the
				// table, boot the node in non-cluster (-x)
				// mode, restore the table from the other
				// nodes in the cluster or backup, and boot
				// the node back in cluster mode.
				//
				(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC,
				    MESSAGE, "CMM: Halting because this node "
				    "has no configuration info about node %ld "
				    "which is currently configured in the "
				    "cluster and running.", i);
			} else if (STATEVEC[i] != S_UNKNOWN) {
				if (!local_heard_from.contains(i)) {
					//
					// If we had not heard directly from
					// this node in the last iteration but
					// have heard from it now then we can
					// have another iteration, provided
					// there are nodes that others have
					// heard from directly but we have not.
					//
					local_heard_from.add_node(i);
					heard_from_changed = true;
				}
			} else {
				// We have not heard directly from the node.
				nodes_unheard_from = true;
			}
		}

		if (!heard_from_changed && nodes_unheard_from) {
			//
			// If there were no new nodes heard from, and there
			// are nodes we were waiting to hear from, declare
			// the latter ones down. This is the last boot
			// iteration.
			//
			for (i = 1; i <= NODEID_MAX; i++) {
				if ((_nodes[i].incarnation != INCN_UNKNOWN) &&
				    (STATEVEC[i] == S_UNKNOWN)) {
					node_is_down(i);
				}
			}
		}
		_state_lock.unlock();
	}
}

#ifdef CMM_VERSION_0
//
// Convert the old CMM message format to the new CMM message
// format.
//
//lint -e1764
void
automaton_impl::convert_msg_to_new(cmm::message_t& msg) const
{
#ifndef _KERNEL_ORB
	//
	// Kernel CMM has different message types for the old and the
	// new versions of CMM messages. This is not the case for userland
	// CMM (ucmm). It may so happen that ucmm may send a version
	// 0 message as it was early to miss the CMM version commit
	// but ucmm on the local node may think it is version 1. Similarly
	// ucmm may send a version 1 message, when we are expecting
	// version 0.
	// The CMM end_sync state is 57 in version 0 and 59 in version 1.
	// We explictly check the value of end_sync and end_wait states to
	// determine the message version.
	//
	const int CMM_END_SYNC_V0 = CMM_END_SYNC - 2;
	const int CMM_END_WAIT_V0 = CMM_END_WAIT - 2;
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		//
		// The CMM_STEP_11 state has the same value in both version
		// 0 and version 1 CMM. The state number will differ only
		// after CMM_STEP_11
		//
		if ((msg.state_list[i] == CMM_END_SYNC_V0) ||
		    (msg.state_list[i] == CMM_END_WAIT_V0)) {
			msg.state_list[i] += 2;
		}
	}
#else // _KERNEL_ORB
	//
	// Convert the CMM state of version 0 to the corresponding
	// state in version 1 CMM.
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		//
		// The CMM_STEP_11 state has the same value in both version
		// 0 and version 1 CMM. The state number will differ only
		// after CMM_STEP_11
		//
		if (msg.state_list[i] > CMM_STEP_11) {
			msg.state_list[i] += 2;
		}
	}
#endif // _KERNEL_ORB
}
//lint +e1764

//
// Convert the new CMM message format to the old CMM message
// format.
//
//lint -e1764
void
automaton_impl::convert_msg_to_old(cmm::message_t &msg) const
{
	//
	// Convert the CMM state of version 1 to the corresponding
	// state in version 0 CMM.
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		//
		// The CMM_STEP_11 state has the same value in both version
		// 0 and version 1 CMM. The state number will differ only
		// after CMM_STEP_11
		//
		if (msg.state_list[i] > CMM_STEP_11) {
			msg.state_list[i] -= 2;
		}
	}
}
//lint +e1764
#endif // CMM_VERSION_0

//
// Return a string representing the automaton state
//
const char *
automaton_impl::_state_string(uint16_t state) const
{
	static char *print_step_tab[] = {
		"step1", "step2", "step3", "step4",
		"step5", "step6", "step7", "step8",
		"step9", "step10", "step11", "step12",
		"step13", "step14", "step15", "step16",
		"step17", "step18"
	};

	switch (state) {
	case S_UNKNOWN:
		return ("unknown");
	case S_DOWN:
		return ("down");
	case S_DEAD:
		return ("dead");
	case S_STOP:
		return ("stop");
	case S_STOPPED:
		return ("stopped");
	case S_ABORT:
		return ("abort");
	case S_ABORTED:
		return ("aborted");
	case CMM_START:
		return ("start");
	case CMM_BEGIN:
		return ("begin");
	case CMM_CONNECTIVITY:
		return ("connectivity");
	case CMM_QACQUISITION:
		return ("quorum_acquisition");
	case CMM_SQACQUIRE:
		return ("quorum_shutdown_acquisition");
	case CMM_QCHECK:
		return ("quorum_check");
	case CMM_END_SYNC:
		return ("end_sync");
	case CMM_END_WAIT:
		return ("end_wait");
	case CMM_RETURN:
		return ("return");
	default:
#ifdef	_KERNEL_ORB
		if ((state >= CMM_STEP_1) && (state <= CMM_SHUTDOWN_STEP_5)) {
#else
		if ((state >= CMM_STEP_1) && (state <= CMM_STEP_11)) {
#endif
			int ind = state - CMM_STEP_1;
			return (print_step_tab[ind]);
		} else {
			return ("???");
		}
	}
}

//
// Print out the current automaton state matrix.
//
void
automaton_impl::_print_matrix()
{
	nodeid_t i, j;

	os::printf("matrix\n");
	for (i = 1; i <= NODEID_MAX; i++) {
		if (orb_conf::node_configured(i)) {
			os::printf("<%ld> ", i);
			for (j = 1; j <= NODEID_MAX; j++) {
				if (orb_conf::node_configured(j)) {
					os::printf("[%ld]%s ", j,
					    _state_string(
					    _nodes[i].state_vector[j]));
				}
			}
			os::printf("\n");
		}
	}
	os::printf("\n");
}

//
// Print out the local node's row of the automaton state matrix.
//
void
automaton_impl::_print_row()
{
	nodeid_t i;
	uint16_t *row;

	os::printf("row <%ld> ", _mynodeid);
	row = &STATEVEC[0];
	for (i = 1; i <= NODEID_MAX; i++) {
		if (orb_conf::node_configured(i)) {
			os::printf("[%ld]%s ", i, _state_string(row[i]));
		}
	}
	os::printf(" my.membership 0x%llx\n", MYMEMBERSHIPSET.bitmask());
	os::printf("\n");
}

//
// Print out the current status of the automaton. If "print_state" is
// set, trace the name of the current state of the local automaton
// into the trace buffer. This flag should be set only when the state
// of the LOCAL automaton changes.
//
inline void
automaton_impl::_dump_state(int print_state)
{
	if (print_state) {
		CMM_TRACE(("in %s state\n", _state_string(MYSTATE)));
		CMM_DEBUG_TRACE(("===> prim[%ld]mem[0x%llx]"
			"state[%s][%s][%s][%s]===\n",
			_primary,
			MYMEMBERSHIPSET.bitmask(),
			_state_string(STATEVEC[1]),
			_state_string(STATEVEC[2]),
			_state_string(STATEVEC[3]),
			_state_string(STATEVEC[4])));
	}
#ifdef CMM_DEBUG
	if (cmm_trace_mask & CMM_PRINT_AUTM_MATRIX) {
		_print_matrix();
	} else if (cmm_trace_mask & CMM_PRINT_AUTM_ROW) {
		_print_row();
	}
#endif
}

// Get the cluster state
void
automaton_impl::getcluststate(struct clust_state *csp)
{
	nodeset		configured_nodes;
	nodeid_t	highest_nodeid = NODEID_UNKNOWN;
	nodeid_t	node;

	csp->rp_local_nodeid = orb_conf::local_nodeid();
	csp->rp_max_step = cmm_impl::the_cmm_impl->get_max_step_number();
	csp->rp_curr_state = MYSTATE;
	csp->rp_curr_step = get_current_step();
	csp->rp_curr_members = MYMEMBERSHIPSET.bitmask();
	for (node = 1; node <= NODEID_MAX; node++) {
		if (orb_conf::node_configured(node)) {
			configured_nodes.add_node(node);
			highest_nodeid = node;
		}
	}
	csp->rp_nnodes = highest_nodeid;
	csp->rp_all_nodes = configured_nodes.bitmask();
	csp->rp_rseqnum = MYSEQNUM;
}

//
// Get the initial view number for a cluster node
// Return EINVAL if the nodeid is out of range
// Return EAGAIN if a reconfig is in progress
//
int
automaton_impl::getivn(nodeid_t nodeid, cmm::seqnum_t &initialviewnumber)
{
	if ((nodeid == 0) || (nodeid > NODEID_MAX)) {
		return (EINVAL);
	}

	_state_lock.lock();
	if (MYSTATE <= CMM_QCHECK) {
		_state_lock.unlock();
		return (EAGAIN);
	}

	initialviewnumber = _nodes[nodeid].initial_view_number;
	_state_lock.unlock();
	return (0);
}

void
automaton_impl::get_membership(cmm::membership_t& membership)
{
	membership = _membership;
}


//
// Check the global CMM version in the CCR. If the global version
// is found to be older than the current version, set the flag
// so that the CMM uses the old version.
//
void
automaton_impl::set_version()
{
	//
	// If the CMM version is set to the latest version, there
	// is no need to read the CMM version from the CCR.
	//
	if (cmm_impl::the().is_old_version()) {
		// Read the CMM version from the CCR.
		uint_t curr_cmm_version =
		    cmm_impl::the().get_cmm_global_version();
		ASSERT(curr_cmm_version <= CMM_VERSION);
		if (curr_cmm_version < CMM_VERSION) {
			//
			// Use the old version.
			//
			CMM_TRACE(("global version found = %d, current "
			    "version = %d\n", curr_cmm_version, CMM_VERSION));
			cmm_impl::the().set_old_version();
		} else {
			//
			// The global version now matches the CMM version
			// on the local node.
			//
			_state_lock.unlock();
			cmm_impl::the().set_new_version();
			_state_lock.lock();
		}
	}
}

//
// Return the node with the automaton primary.
//
nodeid_t
automaton_impl::get_primary()
{
	nodeid_t primary_nid;
	_state_lock.lock();
	primary_nid = _primary;
	_state_lock.unlock();
	return (primary_nid);
}

//
// Function to check if it is safe to take over data services from a node
// that has been declared down.  It is safe to take over from a node that
// is down once we can be certain that the node has died. If the node is
// partitioned, then the CMM waits for a timeout to ensure that the node
// has time to run the quorum algorithm and abort itself.
//
// If the node is marked up, this function returns true. This assumes that
// the node rebooted and rejoined the cluster after the caller found that
// the node was down.
//
// If the node is marked down, the function returns true if the node has
// been declared dead (in the S_DEAD state) and false otherwise. If the
// "wait_until_dead" input parameter is true and the node is down, the
// function waits until the node is marked dead or has been known to have
// rebooted and then returns true.
//
bool
automaton_impl::safe_to_takeover_from(nodeid_t node, bool wait_until_dead)
{
	bool    retval = true; // we return false only in one case

	_state_lock.lock();

	if ((_membership.members[node] == INCN_UNKNOWN) &&
	    (STATEVEC[node] != S_DEAD)) {
		//
		// Node is down (not a cluster member) and is not dead yet.
		// Check if we need to wait before returning.
		//
		if (!wait_until_dead) {
			retval = false;
		} else {
			//
			// Wait for a state change indicating that the
			// node is marked S_DEAD or has become a cluster
			// member or has rebooted (has a new incarnation #).
			// Note that if the node is rebooting when this
			// function is called, its incarnation number may
			// already have been updated and we may only see the
			// node becoming a cluster memeber.
			//
			sol::incarnation_num    old_incarnation =
				_nodes[node].incarnation;

			while ((STATEVEC[node] != S_DEAD) &&
			    (_membership.members[node] == INCN_UNKNOWN) &&
			    (_nodes[node].incarnation == old_incarnation))
				_state_change_cv.wait(&_state_lock);
		}
	}

	_state_lock.unlock();

	return (retval);
}

//
// Function to return the maximum number of transition steps supported
// by the CMM automaton. This is a static value.
//
// static
uint_t
automaton_impl::max_cmm_transition_steps(void)
{
	return (CMM_MAX_TRANSITION_NUM);
}

#if defined(_KERNEL) && defined(DEBUG)

//
// Debug variables and functions to simulate the node running out of memory
// to test the function check_node_health().
//

// Struct to store chain of allocated memory
typedef struct cmm_memory_chunk {
	void *memory;
	size_t size;
	cmm_memory_chunk *next;
} cmm_memory_chunk_t;

int		cmm_debug_eat_memory = 0;
size_t		cmm_debug_max_chunk_size = 0;
os::usec_t	cmm_debug_memory_sleep = 0;

//
// Function to grab all available memory on the node. Memory is grabbed in
// chunks. We start with the specified chunk size. If that fails, we
// start dividing it by 2 and trying to allocate that size. We keep trying
// all the way down to 1 byte allocations.
//
void
memory_grabber_thread(void *)
{
	CMM_TRACE(("memory_grabber_thread starting\n"));

	if (cmm_debug_eat_memory) {

		cmm_memory_chunk_t *cmm_mem_chunks = NULL;
		cmm_memory_chunk_t *chunk;
		uint_t num_chunks_allocated = 0;
		uint_t num_chunks_freed = 0;
		size_t min_chunk_size = cmm_debug_max_chunk_size;
		size_t max_chunk_size = 0;

		CMM_TRACE(("going to grab all memory; freemem=%d, "
		    "availrmem=%d, tune.t_minarmem = %d; "
		    "max chunk size %d bytes\n",
		    freemem, availrmem, tune.t_minarmem,
		    cmm_debug_max_chunk_size));

		size_t chunk_size = cmm_debug_max_chunk_size;

		for (;;) {
			chunk = (cmm_memory_chunk_t *)kmem_alloc(
			    sizeof (cmm_memory_chunk_t), KM_NOSLEEP);
			if (chunk == NULL) {
				goto ate_up_memory;
			}
			// Insert chunk at the head of the cmm_mem_chunks.
			chunk->next = cmm_mem_chunks;
			cmm_mem_chunks = chunk;
			chunk->memory = kmem_alloc(chunk_size, KM_NOSLEEP);
			while ((chunk->memory == NULL) && (chunk_size >= 2)) {
				CMM_TRACE(("kmem_alloc(%d) failed; trying %d\n",
				    chunk_size, chunk_size >> 1));
				chunk_size = chunk_size >> 1;
				chunk->memory = kmem_alloc(chunk_size,
				    KM_NOSLEEP);
			}
			if (chunk->memory == NULL) {
				chunk->size = 0;
				goto ate_up_memory;
			} else {
				chunk->size = chunk_size;
				if (chunk_size > max_chunk_size) {
					max_chunk_size = chunk_size;
				}
				if (chunk_size < min_chunk_size) {
					min_chunk_size = chunk_size;
				}
				num_chunks_allocated++;
			}
		}
ate_up_memory:
		CMM_TRACE(("grabbed %d chunks varying in size from %d to "
		    "%d bytes; freemem=%d, availrmem=%d, "
		    "tune.t_minarmem = %d; sleeping for %d seconds\n",
		    num_chunks_allocated, min_chunk_size, max_chunk_size,
		    freemem, availrmem, tune.t_minarmem,
		    cmm_debug_memory_sleep));

		//
		// Sleep for the specified time and release the memory.
		//
		os::usecsleep(cmm_debug_memory_sleep * MICROSEC);

		CMM_TRACE(("memory_grabber_thread woke up; freeing memory\n"));

		chunk = cmm_mem_chunks;
		while (chunk != NULL) {
			cmm_memory_chunk_t *tmp = chunk;
			chunk = chunk->next;
			if (tmp->memory != NULL) {
				kmem_free(tmp->memory, tmp->size);
			}
			kmem_free(tmp, sizeof (cmm_memory_chunk_t));
			num_chunks_freed++;
		}

		CL_PANIC(num_chunks_allocated == num_chunks_freed);

		CMM_TRACE(("memory_grabber_thread freed allocated memory; "
		    "freemem=%d, availrmem=%d, tune.t_minarmem = %d\n",
		    freemem, availrmem, tune.t_minarmem));
	}

	CMM_TRACE(("memory_grabber_thread exiting\n"));
}

void
simulate_out_of_memory_condition(void)
{
	if (cmm_debug_eat_memory) {
		//
		// Create a thread to grab all memory and sleep
		//
		if (!cmm_impl::cmm_create_thread(memory_grabber_thread, NULL,
			CMM_MAXPRI-1))
			CMM_TRACE(("Unable to create memory_grabber_thread\n"));
		//
		// Sleep 1 second to allow the created thread to run and
		// grab memory.
		//
		os::usecsleep((os::usec_t)1 * MICROSEC);
	}
}
#endif // _KERNEL && DEBUG

#ifdef	_KERNEL
//
// This method does the 'ping' health check for the local node;
// it checks whether or not every ping target configured in CCR
// as a health check is reachable or not.
//
// This method does a door upcall to the qd_userd
// (in userland) for executing the ping command.
//
// If this method could not do the door upcall to qd_userd, or
// if this method could do the door upcall but the ping target
// IP address is not reachable, then this method halts the local node.
//
void
automaton_impl::ping_health_check()
{
	int error = 0;

	conf.membership_info_lock.lock();

	// Get a handle to the door
	door_handle_t door_handle;
	error = door_ki_open(QD_USERD_DOOR, &door_handle);
	if (error != 0) {
		//
		// If we cannot get a handle to the door to qd_userd,
		// then we cannot do the ping checks;
		// hence we halt the node here.
		//
		CMM_TRACE((
		    "ping_ip_addr: door_ki_open of %s returned error %d\n",
		    QD_USERD_DOOR, error));
		//
		// SCMSGS
		// @explanation
		// The automaton could not get a handle to the door
		// to qd_userd daemon and hence cannot do the 'ping'
		// health checks. So the local node is going to halt.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "CMM: Halting this node because door_ki_open of %s "
		    "returned error %d.\n", QD_USERD_DOOR, error);
	}

	// Iterate over the list of ping targets, and ping each one
	SList<char>::ListIterator ip_addrs_iter(conf.ping_targets);
	for (char *ip_addr_strp = NULL;
	    (ip_addr_strp = ip_addrs_iter.get_current()) != NULL;
	    ip_addrs_iter.advance()) {
		CMM_TRACE(("ping_ip_addr: To ping %s\n", ip_addr_strp));

		char cmd_line[1024];
		os::sprintf(cmd_line, "%s %s", PING_CMD, ip_addr_strp);
		door_arg_t darg;
		darg.data_ptr = cmd_line;
		darg.data_size = os::strlen(cmd_line) + 1;
		darg.desc_ptr = NULL;
		darg.desc_num = 0;
		//
		// The door arg needs to have a return buffer,
		// because if it doesn't, the call will fail (with EINTR).
		// The return buffer also needs to be 64-bit aligned
		// in 64-bit kernels. However, if the buffer is too small,
		// the door call will automatically resize it. So, we allocate
		// a ulong_t, which is guaranteed to be aligned, and then
		// use it as the buffer. In most cases, this will be resized
		// and deallocated by the call.
		//
		ulong_t retbuf;
		darg.rbuf = (char*) (&retbuf);
		darg.rsize = sizeof (ulong_t);

		// Mask signals before doing the door_ki_upcall.
		k_sigset_t new_mask, old_mask;
		sigfillset(&new_mask);
		sigreplace(&new_mask, &old_mask);

		// Make the actual door upcall.
		error = door_ki_upcall(door_handle, &darg);

		// Restore the thread signal property.
		sigreplace(&old_mask, NULL);

		if (error != 0) {
			//
			// If we fail to make the door upcall to qd_userd,
			// then we cannot do the ping checks;
			// hence we halt the node here.
			//
			CMM_TRACE(("ping_ip_addr: door_ki_upcall to qd_userd "
			    "returned error %d\n", error));
			//
			// SCMSGS
			// @explanation
			// The automaton could not do a door upcall to qd_userd
			// daemon and hence cannot do the 'ping' health checks.
			// So the local node is going to halt.
			// @user_action
			// Contact your authorized Sun service provider
			// to determine whether a workaround or patch
			// is available.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "CMM: Halting this node because door_ki_upcall "
			    "to qd_userd returned error %d.\n", error);
		}

		//
		// Cast to int because we do not expect the return code
		// of 'ping' command to require more than 32 bits.
		//
		error = (int)((qd_userd_ret_t *)(darg.data_ptr))->cmd_ret;
		CMM_TRACE(("ping_ip_addr: ret code of ping %d, output : %s\n",
		    error, ((qd_userd_ret_t *)(darg.data_ptr))->full_buf));

		// Free the buffer; we no longer need it
		kmem_free(darg.data_ptr, darg.data_size);

		//
		// We just check the return value of ping command.
		// If any one ping target is not reachable,
		// we halt the node.
		//
		if (error != 0) {
			// a ping target is not reachable
			CMM_TRACE(("Halting node as ping target IP %s "
			    "is not reachable\n", ip_addr_strp));
			//
			// SCMSGS
			// @explanation
			// A ping target configured as a health check in CCR
			// was not reachable from the local node.
			// So the local node is going to halt.
			// @user_action
			// Check to ensure that the local node is connected
			// to the network correctly and then reboot the node.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "CMM: Halting this node because the ping target "
			    "IP address %s, that is configured as "
			    "a health check, is not reachable.", ip_addr_strp);
		}
	}

	// Release the door handle
	door_ki_rele(door_handle);

	conf.membership_info_lock.unlock();
}
#endif
