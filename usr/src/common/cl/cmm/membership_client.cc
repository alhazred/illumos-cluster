//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)membership_client.cc	1.7	08/07/26 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <sys/os.h>
#include <cmm/membership_client.h>
#include <h/ccr.h>
#include <ccr/common.h>
#include <nslib/ns.h>

#ifdef _KERNEL
#include <sys/zone.h>
#else	// Userland
#include <zone.h>
#include <sys/cladm_int.h>
#endif

//
// Names of various membership types supported.
//
const char *membership_type_names[membership::NUM_OF_MEM_TYPES] = {
	"BASE_CLUSTER",
	"ZONE_CLUSTER",
	"PROCESS_UP_PROCESS_DOWN",
	"PROCESS_UP_ZONE_DOWN",
	"DUMMY_MEMBERSHIP_TYPE_A",
	"DUMMY_MEMBERSHIP_TYPE_B",
	"DUMMY_MEMBERSHIP_TYPE_C",
	"DUMMY_MEMBERSHIP_TYPE_D"
};

//
// Get the name of the cluster in which the code is running.
// Returns DEFAULT_CLUSTER for global zone and non-cluster-wide zones.
// Returns zonename for cluster-wide zones (virtual clusters).
//
// lint complains that 'clid' local stack variable is not accessed.
//lint -e550
char *
get_current_cluster_name()
{
	Environment		e;
	CORBA::Exception	*exp = NULL;
	uint_t			clid;
	char			*cl_namep = new char[ZONENAME_MAX];
	ccr::directory_ptr	dir_ptr;

	if (getzoneid() == GLOBAL_ZONEID) {
		(void) os::strcpy(cl_namep, DEFAULT_CLUSTER);
		return (cl_namep);
	}

	// Get a reference to the directory object
	naming::naming_context_var ctxp = ns::local_nameserver();
	if (CORBA::is_nil(ctxp)) {
#ifdef DEBUG
		os::warning("can't find local name server");
#endif
		return (NULL);
	}
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
#ifdef DEBUG
		os::warning("ccr_directory not found in name server");
#endif
		e.clear();
		return (NULL);
	}
	dir_ptr = ccr::directory::_narrow(obj);

	// Get current zonename
#ifdef _KERNEL_ORB
	zone_t *zonep = zone_find_by_id(getzoneid());
	if ((zonep == NULL) || (zonep->zone_name == NULL)) {
		if (zonep != NULL) {
			zone_rele(zonep);
		}
		return (NULL);
	}
	(void) os::strcpy(cl_namep, zonep->zone_name);
	zone_rele(zonep);
#else	// _KERNEL_ORB
	if (getzonenamebyid(getzoneid(), cl_namep,
	    (unsigned long)ZONENAME_MAX) == -1) {
		return (NULL);
	}
#endif	// _KERNEL_ORB

	// Get the cluster id
	clid = dir_ptr->zc_getidbyname(cl_namep, e);
	if ((exp = e.exception()) != NULL) {
		if (ccr::invalid_zc_access::_exnarrow(exp)) {
			ASSERT(0);
		} else if (ccr::no_such_zc::_exnarrow(exp)) {
			// Non-cluster-wide zone
			(void) os::strcpy(cl_namep, DEFAULT_CLUSTER);
		}
		e.clear();
	}

	return (cl_namep);
}
//lint +e550

//
// Construct the name of a membership based on its properties
// and populate the string passed. The caller provides the memory.
// Return values :
// ENOENT : if no such cluster exists
// EINVAL : any other error
// 0 : success
//
int
construct_membership_name_common(
    char *namep, membership::membership_type type,
    const char *clid_strp, const char *proc_namep)
{
	switch (type) {
	case membership::BASE_CLUSTER:
		os::sprintf(namep, "%s : clid %s",
		    membership_type_names[type], clid_strp);
		break;

	case membership::ZONE_CLUSTER:
		os::sprintf(namep, "%s : clid %s",
		    membership_type_names[type], clid_strp);
		break;

	case membership::PROCESS_UP_PROCESS_DOWN:
		os::sprintf(namep, "%s : clid %s, process %s",
		    membership_type_names[type], clid_strp, proc_namep);
		break;

	case membership::PROCESS_UP_ZONE_DOWN:
		os::sprintf(namep, "%s : clid %s, process %s",
		    membership_type_names[type], clid_strp, proc_namep);
		break;

	case membership::DUMMY_MEMBERSHIP_TYPE_A:
	case membership::DUMMY_MEMBERSHIP_TYPE_B:
	case membership::DUMMY_MEMBERSHIP_TYPE_C:
	case membership::DUMMY_MEMBERSHIP_TYPE_D:
		// Not used currently
		return (EINVAL);

	default:
		ASSERT(0);
		return (EINVAL);	/*lint !e527 */
	}

	return (0);
}

int
construct_membership_name(char *namep, membership::membership_type type,
    const char *cl_namep, const char *proc_namep)
{
	uint_t clid = 0;
	int num_digits = 0;
	char clid_str[CLID_MAX_LEN];
	int ret = 0;

#ifdef _KERNEL
	ret = clconf_get_cluster_id((char *)cl_namep, &clid);
	if (ret) {
		return (ret);
	}
#else
	// Userland
	cz_id_t czid;
	czid.clid = 0;
	(void) os::strcpy(czid.name, cl_namep);
	ret = os::cladm(CL_CONFIG, CL_GET_ZC_ID, &czid);
	if (ret) {
		return (ret);
	}
	clid = czid.clid;
#endif

	num_digits = os::itoa((int)clid, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster id to string
		return (EINVAL);
	}

	return (construct_membership_name_common(
	    namep, type, clid_str, proc_namep));
}

int
construct_membership_name(char *namep, membership::membership_type type,
    uint_t clid, const char *proc_namep)
{
	int num_digits = 0;
	char clid_str[CLID_MAX_LEN];

	num_digits = os::itoa((int)clid, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster id to string
		return (EINVAL);
	}

	return (construct_membership_name_common(
	    namep, type, clid_str, proc_namep));
}

//
// cl_membership_client_impl methods
//
membership_client_impl::membership_client_impl()
{
	client_namep = NULL;
	callback_funcp = NULL;
	cluster_name[0] = '\0';
	membership_name[0] = '\0';

	// The other form of the constructor should be used
	ASSERT(0);
}

//
// Clients need not specify cluster name when using this constructor.
// The constructor uses the current cluster name when a cluster name
// is not specified.
//
membership_client_impl::membership_client_impl(
    const char *clientnamep, membership::membership_type mem_type,
    const char *proc_namep, mem_callback_funcp_t funcp)
{
	client_namep = os::strdup(clientnamep);
	char *cl_namep = get_current_cluster_name();
	if (cl_namep == NULL) {
		ASSERT(0);
	}
	(void) os::strcpy(cluster_name, cl_namep);
	delete [] cl_namep;
	if (construct_membership_name(
	    membership_name, mem_type, cluster_name, proc_namep) != 0) {
		ASSERT(0);
	}
	callback_funcp = funcp;
}

//
// Clients can specify cluster name they are interested in.
// If a non-global zone client tries to use this method
// for a cluster different from the cluster that the client resides in,
// then this method would fail an assertion.
//
membership_client_impl::membership_client_impl(
    const char *clientnamep, membership::membership_type mem_type,
    const char *cl_namep, const char *proc_namep, mem_callback_funcp_t funcp)
{
	client_namep = os::strdup(clientnamep);
	char *namep = get_current_cluster_name();
	if (getzoneid() != GLOBAL_ZONEID) {
		if (os::strcmp(cl_namep, namep) != 0) {
			ASSERT(0);
		}
	}
	delete [] namep;
	(void) os::strcpy(cluster_name, cl_namep);
	if (construct_membership_name(
	    membership_name, mem_type, cl_namep, proc_namep) != 0) {
		ASSERT(0);
	}
	callback_funcp = funcp;
}

membership_client_impl::~membership_client_impl()
{
	delete [] client_namep;
	client_namep = NULL;
	callback_funcp = NULL;
}

void
#ifdef DEBUG
membership_client_impl::_unreferenced(unref_t cookie)
#else
membership_client_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

void
membership_client_impl::membership_callback(
    const cmm::membership_t &new_membership,
    cmm::seqnum_t new_seqnum, Environment &)
{
	(*callback_funcp)(cluster_name, membership_name,
	    new_membership, new_seqnum);
}

#endif	// (SOL_VERSION >= __s10)
