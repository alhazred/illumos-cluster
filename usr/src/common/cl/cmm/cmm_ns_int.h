/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CMM_NS_INT_H
#define	_CMM_NS_INT_H

#pragma ident	"@(#)cmm_ns_int.h	1.8	08/06/05 SMI"

#include <sys/sol_version.h>

static const char *cmm_callback_registry_name = "cmm callback_registry";

#if (SOL_VERSION >= __s10)
static const char *membership_engine_namep = "membership engine";
static const char *membership_api_namep = "membership api";
#endif	// (SOL_VERSION >= __s10)

static const char *cmm_control_name = "cmm control";
static const char *ff_admin_name = "ff admin";
static const char *quorum_name = "quorum";
static const char *kernel_ucmm_agent_name = "kernel ucmm agent";

#endif	/* _CMM_NS_INT_H */
