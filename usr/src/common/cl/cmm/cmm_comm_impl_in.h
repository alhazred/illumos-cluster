/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  cmm_comm_impl_in.h
 *
 */

#ifndef	_CMM_COMM_IMPL_IN_H
#define	_CMM_COMM_IMPL_IN_H

#pragma ident	"@(#)cmm_comm_impl_in.h	1.4	08/05/20 SMI"

inline void
cmm_message::_put(MarshalStream &wms)
{
	wms.put_bytes((void *)comm_messagep, sizeof (comm_message_t));
	wms.put_bytes((void *)messagep, sizeof (cmm::message_t));
}

inline void
cmm_message::_get(MarshalStream &rms)
{
	rms.get_bytes((void *)comm_messagep, sizeof (comm_message_t));
	rms.get_bytes((void *)messagep, sizeof (cmm::message_t));
}

#endif	/* _CMM_COMM_IMPL_IN_H */
