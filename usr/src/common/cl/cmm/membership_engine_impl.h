/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * membership_engine_impl.h
 */

#ifndef	_MEMBERSHIP_ENGINE_IMPL_H
#define	_MEMBERSHIP_ENGINE_IMPL_H

#pragma ident	"@(#)membership_engine_impl.h	1.4	08/07/21 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <h/membership.h>
#include <orb/object/adapter.h>
#include <cmm/membership_manager_impl.h>
#include <cmm/membership_client.h>
#include <sys/hashtable.h>
#include <sys/vm_util.h>

#define	MEMBERSHIP_VP_NAME	"membership"
#define	MEMBERSHIP_UCC_NAME	"membership_upgrade_callback"
#define	MEMBERSHIP_API_AVAILABLE_VP_MAJOR_NUM	1
#define	MEMBERSHIP_API_AVAILABLE_VP_MINOR_NUM	4
#define	MEMBERSHIP_VP_MAX_MAJOR	1
#define	MEMBERSHIP_VP_MAX_MINOR	4

//
// Overview of Membership Engine :
// -------------------------------
// The membership subsystem has a sole membership engine object per base node.
// The engine receives membership change trigger events (such as zone cluster
// nodes coming up or going down, processes coming up or going down,
// base cluster CMM reconfiguration happening) from various entities
// such as zone cluster brand halt callbacks, processes (like rgmd/ucmmd)
// themselves, and the kernel CMM automaton.
// The engine delivers these events to the membership manager objects
// that are registered for such events.
// The engine is also responsible for creation/deletion of membership managers
// for base cluster membership, zone cluster membership and process membership.
// The engine does these creations/deletions when cluster nodes boot up,
// when zone clusters are dynamically created or deleted,
// and when process memberships are created or deleted.
//

//
// Returns true if we are running a software version
// that should have the membership subsystem present.
// Returns false otherwise.
//
bool membership_api_support_available();

//
// The membership engine supports dynamic creation/deletion of zone clusters.
// When a zone cluster is added/removed on a running base cluster,
// the cluster_directory table is updated in CCR.
// The membership subsystem gets a callback from clconf for this event.
// A CCR change thread (part of membership subsystem) waits for these events,
// and wakes up once it is signalled by the clconf callback. It can also
// be signalled to exit if the membership subsystem is shutting down.
//
enum ccr_change_thread_event {
    ZONE_CLUSTER_ADDED, ZONE_CLUSTER_REMOVED, CCR_CHANGE_THREAD_EXIT};

//
// This method is used to initialize the membership subsystem :
// create the membership engine and start some threads/threadpools
// required for the membership subsystem.
//
bool membership_subsystem_init();

//
// This method is used to shutdown the membership subsystem.
//
void membership_subsystem_fini();

// CCR calls back into membership subsystem to say when it is refreshed.
void ccr_is_ready_callback_for_membership();

//
// The implementation class for the membership engine
//
class membership_engine_impl :
    public McServerof< membership::engine > {

public :
	// Static method that does the initialization
	static void initialize();

	// Static method that does the shutdown
	static void shutdown();

	// Returns a pointer to the sole object of this type
	static inline membership_engine_impl *the() {
		return (the_membership_engine_implp);
	}

	//
	// Register a membership manager for an event.
	// Returns true on success, false on failure.
	//
	bool register_mem_manager(membership_manager_impl *, const char *);

	//
	// Unregister a membership manager for an event.
	// Returns true on success, false on failure.
	//
	bool unregister_mem_manager(membership_manager_impl *, const char *);

	//
	// After kernel CMM reconfigures, the automaton calls back
	// into the membership subsystem, providing the latest
	// base cluster membership and seqnum.
	//
	void automaton_callback(const cmm::membership_t &, cmm::seqnum_t);

	//
	// Callback delivered from clconf when zone clusters are created/deleted
	//
	void clconf_callback_to_membership(
	    const char *, ccr_change_thread_event) const;

	// The leader engine drives the engine reconfiguration using this method
	void leader_drives_engine_reconfig(
	    cmm::seqnum_t, const cmm::membership_t &);

	//
	// Get a pointer to a particular membership manager
	// from the table of membership managers maintained by the engine
	//
	membership_manager_impl *get_membership_manager(
	    const membership::membership_manager_info &);

	// IDL methods
	void membership_create(
	    const membership::membership_manager_info &, Environment &);
	void membership_delete(
	    const membership::membership_manager_info &, Environment &);
	void leader_delivers_cl_mem_list(
	    const membership::membership_manager_info_seq &, Environment &);
	void leader_delivers_proc_mem_list(
	    const membership::membership_manager_info_seq &, Environment &);
	void deliver_event_to_engine(membership::engine_event,
	    const membership::engine_event_info &, Environment &);
	void request_to_leader(membership::engine_event,
	    const membership::membership_manager_info &, Environment &);
	void query_list_of_proc_memberships(
	    membership::membership_manager_info_seq_out, Environment &);
	void finish_engine_reconfiguration(
	    cmm::seqnum_t, const cmm::membership_t &, Environment &);

private :
	membership_engine_impl();
	~membership_engine_impl();
	void _unreferenced(unref_t);

	// Hide copy constructor and assignment operator.
	membership_engine_impl(const membership_engine_impl &);
	membership_engine_impl &operator = (membership_engine_impl &);

	//
	// Get a pointer to a particular membership manager
	// from the table of membership managers maintained by the engine.
	// These are private methods to be used by code for membership engine.
	// The global lock on the membership managers table must be held
	// in reader or writer mode, while calling these methods.
	//
	membership_manager_impl *get_membership_manager_internal(
	    membership::membership_type, const char *, const char *);
	membership_manager_impl *get_membership_manager_internal(
	    const membership::membership_manager_info &);

	//
	// Thread that waits for the kernel CMM automaton to signal
	// that a kernel CMM reconfiguration has ended and that
	// the base cluster kernel CMM membership is stable now.
	//
	static void start_automaton_change_thread(void *);

	//
	// Thread that waits for the kernel clconf to signal
	// that a zone cluster has been created/deleted.
	//
	static void start_ccr_change_thread(void *);

	// Create a membership manager based on the data provided
	void create_mem_manager(const membership::membership_manager_info &);

	// Delete a membership manager based on the data provided
	void delete_mem_manager(const membership::membership_manager_info &);

	//
	// On the "leader" engine, this method is used to drive
	// the creation/deletion of a membership manager.
	//
	bool drive_creation_or_deletion(membership::engine_event,
	    const membership::membership_manager_info &,
	    cmm::seqnum_t, const cmm::membership_t &, Environment &);

	// Dispatch events to membership managers registered for the events
	void dispatch_events_to_managers(const membership::engine_event_info &);

	// Update references to peer engines
	bool update_peer_refs(cmm::seqnum_t, const cmm::membership_t &);

	// Subroutines to handle steps of engine reconfiguration
	bool engine_agreement_on_cl_mem_list(
	    cmm::seqnum_t, const cmm::membership_t &) const;
	bool engine_agreement_on_proc_mem_list(
	    cmm::seqnum_t, const cmm::membership_t &);
	bool leader_drive_finish_reconfig_step(
	    cmm::seqnum_t, const cmm::membership_t &) const;

	// Pointer to the sole membership engine object on this node
	static membership_engine_impl *the_membership_engine_implp;

	// The leader engine keeps references to its peer engines
	membership::engine_ptr engine_refs[NODEID_MAX+1];

	//
	// The engine maintains a list of membership managers created.
	// Pointers to the membership managers are stored in a hashtable
	// using the cluster name (related to the membership manager)
	// as the key.
	//
	struct membership_manager_entry {
		// List of membership managers associated with a cluster
		SList<membership_manager_impl> manager_list;
	};
	string_hashtable_t<membership_manager_entry *> membership_managers;
	//
	// Global lock on the managers table used when adding table entries.
	// This is different from the per-entry lock, in that
	// this lock is used when creating the first entry for a cluster,
	// while the per-entry lock is used when manipulating the list of
	// managers in an entry.
	//
	os::rwlock_t membership_managers_global_rwlock;

	//
	// The engine maintains a mapping between :
	// (i) membership change events that can happen
	// (eg, base cluster membership changes, member of a zone cluster
	// comes up or goes down, process comes up or goes down)
	// (ii) membership managers that are interested in such events.
	//
	// When the membership engine receives such an event,
	// it informs the membership managers that have registered
	// for the event.
	//
	struct event_to_manager_mapping_entry {
		// List of membership managers interested in the event
		SList<membership_manager_impl> manager_list;
		// Lock to protect the list of objects
		os::mutex_t entry_lock;
	};
	string_hashtable_t<event_to_manager_mapping_entry *>
	    event_to_manager_mapping;
	//
	// Global lock on the mapping table used when adding table entries.
	// This is different from the per-entry lock, in that
	// this lock is used when creating the first entry for an event,
	// while the per-entry lock is used when manipulating the list of
	// objects in an entry.
	//
	os::mutex_t event_to_manager_mapping_global_lock;

	//
	// The membership engine creates/deletes membership managers.
	// When a dynamic creation/deletion of a membership manager request is
	// made to the engine on the local base node, the local engine requests
	// the engine on the leader node to drive the creation/deletion.
	// The engine on the leader node guides all engines
	// through the creation/deletion process.
	// Multiple engines could be requesting leader engine
	// to create/delete the same membership managers.
	// The leader engine allows only one among the requests to succeed.
	// These data structures mark that a creation/deletion request
	// for a particular membership is in progress, so that
	// other requests for the same membership manager creation/deletion
	// just return back without progress.
	//
	string_hashtable_t<os::mutex_t *> create_or_delete_in_progress;
	os::mutex_t create_or_delete_in_progress_lock;

	// True if engine reconfiguration is happening
	bool engine_reconfig_underway;

	//
	// We have a thread that wakes up when the automaton signals
	// a base cluster CMM reconfiguration (in end state).
	// The thread then notifies the engine that a node CMM reconfiguration
	// has ended, and base cluster node membership is stable now.
	//
	enum automaton_callback_thread_event {
	    AUTOMATON_MEMBERSHIP_CHANGE, AUTOMATON_CALLBACK_THREAD_EXIT};
	static automaton_callback_thread_event autm_event;
	// Lock and condition variable for the thread
	static os::mutex_t	automaton_change_lock;
	static os::condvar_t	automaton_change_cv;

	//
	// A CCR change thread exists on each base node.
	// The CCR change thread wakes up when a zone cluster is dynamically
	// created/deleted. The CCR change thread uses this data.
	//
	struct ccr_change_thread_data_t {
		char zone_cluster_name[CLUSTER_NAME_MAX];
		ccr_change_thread_event event;
		bool task_to_do;
	};
	static struct ccr_change_thread_data_t ccr_change_thread_data;
	// Lock and condition variable for CCR change thread
	static os::mutex_t	ccr_change_lock;
	static os::condvar_t	ccr_change_cv;
};

class membership_upgrade_callback:
	public McServerof<version_manager::upgrade_callback>
{
public:
	membership_upgrade_callback();
	~membership_upgrade_callback();

	void _unreferenced(unref_t unref);

	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &);
};

#endif	/* (SOL_VERSION >= __s10) */
#endif	/* _MEMBERSHIP_ENGINE_IMPL_H */
