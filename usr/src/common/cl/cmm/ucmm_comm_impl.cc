//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ucmm_comm_impl.cc	1.49	08/10/13 SMI"

#include <orb/invo/common.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <sys/sc_syslog_msg.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_impl.h>
#include <cmm/cmm_debug.h>
#include <cmm/ucmm_impl.h>
#include <cmm/ucmm_comm_impl.h>
#include <vm/versioned_protocol.h>
#include <sys/vm_util.h>

// Implementation file for comm module for userland CMM

static cm_comm_impl *the_cm_comm_impl = NULL;

cm_comm_impl::cm_comm_impl() : _state(ENABLED),
    sender_index(0), sender_thread_arg(SLEEP),
    init_handshake_done(false)
{
	CL_PANIC(the_cm_comm_impl == NULL);
	the_cm_comm_impl = this;
	bzero(sender_info, sizeof (sender_info));
}

cm_comm_impl &
cm_comm_impl::the(void)
{
	CL_PANIC(the_cm_comm_impl != NULL);
	return (*the_cm_comm_impl);
}

//
// wait_till_init_handshake_done
//
// Makes the caller block until this uCMM has been able to perform
// its initial handshake (with either success or failure) with all
// other non-S_DEAD nodes in the cluster.
//
// Called by the boot delay thread.
//
void
cm_comm_impl::wait_till_init_handshake_done()
{
	sc_syslog_msg_handle_t handle;
	(void) sc_syslog_msg_initialize(&handle,
	    ucmm_impl::the().syslog_tag, "");

	init_handshake_lock.lock();
	// Wait until init_handshake_done is true.
	while (!init_handshake_done) {
		CMM_TRACE(("Waiting for initial handshake to complete ...\n"));
		//
		// SCMSGS
		// @explanation
		// The userland CMM has not been able to complete its initial
		// handshake protocol with its counterparts on the other
		// cluster nodes, and will only be able to join the cluster
		// after this is completed.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
		    "CMM: Waiting for initial handshake to complete.");
		init_handshake_cv.wait(&init_handshake_lock);
	}
	init_handshake_lock.unlock();

	sc_syslog_msg_done(&handle);
}

//
// IDL interface implementation functions
//

//
// Called by the userland CMM automaton to send status messages during
// reconfigurations.
//
// Copy the message to be sent and signal the sender thread.
//
void
cm_comm_impl::send_all(
	const cmm::message_t &msg,
	quorum::membership_bitmask_t destination_nodes,
	Environment&)
{
	lock();

	// Don't use msgs[sender_index]. This simple calculation is "0"
	// when sender_index is "1", and is "1" when sender_index is "0".
	latest_index = 1 - sender_index;

	// Note: structure copy (cannot use bcopy on "const" argument)
	msgs[latest_index] = msg;

	//
	// Update destination set with new destination nodes.
	//
	nodeset new_destination_nodes(destination_nodes);
	destination_node_set.add_nodes(new_destination_nodes);

	//
	// If the message indicates that our state is S_ABORTED or
	// S_STOPPED, initiate the freeing up of our resources.
	//
	uint16_t automaton_state = msg.state_list[orb_conf::node_number()];
	if (((automaton_state == S_ABORTED) ||
	    (automaton_state == S_STOPPED)) &&
	    (_state == ENABLED)) {
		_state = ABORTING;
	}

	//
	// Increase message number for new destination nodes.
	//
	for (nodeid_t to_nodeid = 1; to_nodeid <= NODEID_MAX; to_nodeid++) {
		if (new_destination_nodes.contains(to_nodeid)) {
			sender_info[to_nodeid].my_message_number++;
		}
	}

	signal_sender_thread(WAKEUP);

	unlock();
}

//
// This interface is used only by kernel CMM, should not be
// called by userland cmm.
//
void
cm_comm_impl::send_stop_all(
	quorum::membership_bitmask_t,
	Environment&)
{
	ASSERT(0);
}

//
// Thread to send the automaton status messages.
//
// static
void *
cm_comm_impl::sender_thread_start(void *)
{
	the_cm_comm_impl->sender_thread();
	// NOTREACHED
	return (0);
}

//
// The sender_thread is awakened to do a broadcast every msg_interval.
//
void
cm_comm_impl::sender_thread()
{
	//
	// Initialize the automaton pointers.
	//
	automatonp_array_lock.lock();
	for (nodeid_t nodeid = 0; nodeid <= NODEID_MAX; nodeid++) {
		automatonp[nodeid] = cmm::automaton::_nil();
	}
	automatonp_array_lock.unlock();

	//
	// Get the pointer to the kernel_ucmm_agent object.
	//
	kernel_ucmm_agent_p = cmm_ns::get_kernel_ucmm_agent();

	lock();
	for (;;) {
		switch (sender_thread_arg) {
		case SLEEP:
			sender_cv.wait(&lck);
			break;
		case DIE:
			_state = ABORTED;
			goto die;
		case WAKEUP:
		default:
			//
			// If we're aborting, we can die after sending the
			// status message. Otherwise, we go back to sleep
			// after sending the messages.
			//
			// Since we drop the lock before sending the message,
			// the automaton could ask for more work before we
			// examine the value of sender_thread_arg the next
			// time through the loop.
			//
			if (_state == ABORTING) {
				sender_thread_arg = DIE;
			} else {
				sender_thread_arg = SLEEP;
			}
			unlock();
			broadcast_msg();
			//
			// Set init_handshake_done to true if not
			// already so. No need to lock for reading
			// purposes since we are the only writer.
			//
			// XXX Post process membership, the handshake is done
			// XXX when a membership callback is received.
			// XXX In fact, the broadcast_msg() does not
			// XXX do any handshake, and could return
			// XXX before sending messages to all nodes
			// XXX in membership, if it finds that a new membership
			// XXX callback has happened.
			// XXX In any case, since the handshake is done
			// XXX by this point, it is fine.
			// XXX
			if (!init_handshake_done) {
				init_handshake_lock.lock();
				init_handshake_done = true;
				init_handshake_cv.signal();
				init_handshake_lock.unlock();
			}
			lock();
		}
	}
die:
	unlock();

	CMM_TRACE(("sender thread exiting\n"));

	//
	// Let cmm_impl clean up before shutting down.
	//
	cmm_impl::cmm_fini();

	thr_exit((void *)-1);
}

//
// Wake up the sender_thread.  The argument can be one of:
//
//	SLEEP - go back to sleep
//	WAKEUP - broadcast status message
//	DIE - exit, the node is leaving the cluster.
//
void
cm_comm_impl::signal_sender_thread(sender_thread_arg_t arg)
{
	CL_PANIC(lck.lock_held());
	//
	// Pass the word on to the sender_thread.
	//
	sender_thread_arg = arg;
	sender_cv.signal();
}

//
// Send the automaton status message to all accessible remote nodes.
// If we get an exception when sending to a node, the incarnation we are
// sending to must have left the cluster. In that case, we must inform
// the automaton of that fact.
//
void
cm_comm_impl::broadcast_msg(void)
{
	//
	// During rolling upgrade some nodes will be running the the old
	// version of UCMM, convert the message so that all the nodes running
	// old versions of UCMM will understand and interpret the message.
	//
	bool is_old_version = true;
	cmm::message_t tmp_msg;

	//
	// First check if the CMM has been upgraded to cmm_version=1
	// If not, then there is no point using the Rolling Upgrade
	// framework, as it is only active after cmm_version=1. Instead
	// just send the messages in the old format.
	//
	if (!(cmm_impl::the().is_old_version())) {
		version_manager::vp_version_t running_version;
		version_manager::vm_admin_var vm_v = vm_util::get_vm();

		if (!CORBA::is_nil(vm_v)) {
			Environment ex;
			vm_v->get_running_version("ucmm", running_version, ex);
			ASSERT(!ex.exception());
			if (running_version.major_num >= 2) {
				is_old_version = false;
			}
		} else {
			CMM_TRACE(("cm_comm_impl:version manager null"
			    " reference\n"));
		}
	}

	automatonp_array_lock.lock();
	for (nodeid_t to_nodeid = 1; to_nodeid <= NODEID_MAX; to_nodeid++) {

		if (!destination_node_set.contains(to_nodeid)) {
			continue;
		}

		CL_PANIC(to_nodeid != orb_conf::node_number());

#if (SOL_VERSION >= __s10)
		// XXX
		// XXX During rolling upgrade, we should not do this.
		// XXX We should proceed to lookup info for other nodes.
		// XXX
		if (using_process_membership &&
		    (CORBA::is_nil(automatonp[to_nodeid]))) {
			//
			// In the membership callback, the automaton refs
			// are collected for all nodes in membership.
			// But the automaton tries to send messages
			// to any node that is not "dead".
			// Hence there could be destination nodes that
			// are "down" but not "dead" (automaton terminology)
			// So we skip those nodes.
			// XXX Is there any reason why we should lookup
			// XXX "down" nodes and try to send them messages?
			// XXX Why does automaton try to send messages
			// XXX to "down" nodes?
			//
			// Another scenario that could happen :
			// The automaton references are not protected
			// by any locks. So a membership callback could
			// nullify a reference, while a broadcast_msg
			// is in progress. So the membership might have changed.
			//
			continue;
		}
#endif

		if (CORBA::is_nil(automatonp[to_nodeid])) {
			//
			// We do not have the remote node's
			// address yet. Look up the name of
			// the node in the name server.
			//
			lookup_node_info(to_nodeid);
		}

		bool	must_retry = false;	// Default to no retry
		do {
			lock();
			sender_index = latest_index;
			cm_msg_number	current_message_number =
				sender_info[to_nodeid].my_message_number;
			unlock();

			if (CORBA::is_nil(automatonp[to_nodeid])) {
				lock();
				if (current_message_number ==
				    sender_info[to_nodeid].my_message_number) {
					destination_node_set.
						remove_node(to_nodeid);
				}
				unlock();
				must_retry = false;
			} else {
				// lookup says node is exists
				Environment e;

				tmp_msg = msgs[sender_index];
				if (is_old_version) {
					cmm_impl::the().convert_msg_to_old(
					    tmp_msg);
				}

				automatonp[to_nodeid]->receive_msg(
				    tmp_msg,
				    orb_conf::node_number(), e);

				if (e.exception()) {
					e.clear();
#if (SOL_VERSION >= __s10)
					if (using_process_membership) {
						//
						// We had a reference to the
						// automaton for a remote node,
						// but the node has probably
						// gone down. Since a
						// reconfiguration would occur
						// anyway, we return without
						// sending any more messages.
						//
						automatonp_array_lock.unlock();
						return;
					}
#endif

					//
					// The node has gone down after
					// we looked it up. If the node
					// has rebooted, it may have a new
					// reference. Let us look it up again.
					//
					lookup_node_info(to_nodeid);
					//
					// We may have new info to contact
					// the node. Let us try sending again.
					//
					must_retry = true;
				} else	{
					lock();
					if (current_message_number ==
					    sender_info[to_nodeid].
					    my_message_number) {
						destination_node_set.
							remove_node(to_nodeid);
					}
					unlock();
					must_retry = false;
				}
			}
		} while (must_retry);
	}
	automatonp_array_lock.unlock();
}

#if (SOL_VERSION >= __s10)
void
cm_comm_impl::node_is_down(sol::nodeid_t nodeid)
{
	CMM_TRACE(("NODE IS DOWN called for node %d\n", nodeid));
	automatonp_array_lock.lock();
	automatonp[nodeid] = cmm::automaton::_nil();
	automatonp_array_lock.unlock();
}

void
cm_comm_impl::node_is_up(sol::nodeid_t nodeid)
{
	automatonp_array_lock.lock();
	lookup_node_info(nodeid);
	automatonp_array_lock.unlock();
}

//
// Return true if we don't have a reference to the automaton
// on a specified nodeid. Return false otherwise.
//
bool
cm_comm_impl::is_automaton_ref_nil(sol::nodeid_t nodeid)
{
	automatonp_array_lock.lock();
	bool retval = CORBA::is_nil(automatonp[nodeid]);
	automatonp_array_lock.unlock();
	return (retval);
}
#endif

//
// Get a remote node's automaton pointer through the kernel_ucmm_agent.
//
void
cm_comm_impl::lookup_node_info(sol::nodeid_t nodeid)
{
	CMM_TRACE(("LOOKUP NODE INFO called for node %d\n", nodeid));

	Environment e;

	//
	// Having the lock could result in distributed deadlock.
	//
	ASSERT(!lck.lock_held());

	cmm::userland_cmm_var		remote_cmmp;
	cmm::remote_ucmm_proxy_ptr	remote_proxyp;
	naming::naming_context_var	ctxp = ns::root_nameserver();
	char				name[30];

	if (nodeid == orb_conf::node_number())
		return; // no need to look up my own info

	os::sprintf(name, "%s%d", ucmm_impl::the().ucmm_name, nodeid);
	CORBA::Object_ptr obj_p = cmm::userland_cmm::_nil();

	ASSERT(automatonp_array_lock.lock_held());

	obj_p = ctxp->resolve(name, e);

	if (e.exception()) {
		//
		// Remote node is not registered in the name server.
		//
		e.clear();
		automatonp[nodeid] = cmm::automaton::_nil();
		return;
	}

	if (CORBA::is_nil(obj_p)) {
		// Name lookup error; cannot happen
		automatonp[nodeid] = cmm::automaton::_nil();
		return;
	}

	remote_cmmp = cmm::userland_cmm::_narrow(obj_p);
	CORBA::release(obj_p);

	remote_cmmp->ucmm_handshake(orb_conf::node_number(),
	    ucmm_impl::the().local_incarnation,
	    automatonp[nodeid], remote_proxyp, e);

	if (e.exception()) {
		//
		// Although the remote node's name is in the name server,
		// it is no longer up.
		//
		e.clear();
		automatonp[nodeid] = cmm::automaton::_nil();
		return;

	} else {


#if (SOL_VERSION >= __s10)
		if (using_process_membership) {
			//
			// Post process membership, we do not use remote
			// ucmm proxy references.
			// XXX To support rolling upgrade, we can use remote
			// XXX ucmm proxy references when the upgrade is not
			// XXX complete. So to add rolling upgrade support,
			// XXX check if version is new, and if so, return.
			//
			return;
		}
#endif

		//
		// Successfully retrieved remote node's references.
		// Pass the proxy reference to the kernel agent to keep
		// until the node dies.
		//
		CMM_TRACE(("calling keep_remote_ucmm_proxy_reference("
		    "node=%ld,remote_proxyp=%p); automaptonp==%p\n",
		    nodeid, remote_proxyp));

		kernel_ucmm_agent_p->keep_remote_ucmm_proxy_reference(
		    ucmm_impl::the().ucmm_name, nodeid, remote_proxyp, e);
		CL_PANIC(e.exception() == NULL);
		CORBA::release(remote_proxyp);
		return;
	}
}
