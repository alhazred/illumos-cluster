//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _UCMM_API_H
#define	_UCMM_API_H

#pragma ident	"@(#)ucmm_api.h	1.8	08/06/23 SMI"

/* Interfaces to the userland CMM */

#include <h/cmm.h>
#include <sys/sol_version.h>

/*
 * bitmask definition and operations for node membership
 *
 * XXX: should switch to using the nodeset class defined in sys/nodeset.h
 */

typedef quorum::membership_bitmask_t	nodeset_t;

/* largest node number supported */
#define	MAXNODES	NODEID_MAX

/* bitmask representing no cluster members */
#define	EMPTY_NODESET	(nodeset_t)0

/* macros for manipulating nodeset bit map */
#define	nodeset_create(nodeid)  (1LL << (nodeid-1))
#define	nodeset_add(ns, nodeid)  (ns) |= (1LL << (nodeid-1))
#define	nodeset_delete(ns, nodeid)  (ns) &= ~(1LL << (nodeid-1))
#define	nodeset_contains(ns, nodeid) ((ns) & (1LL << (nodeid-1)))
#define	nodeset_equal(x, y) ((x) == (y))
#define	nodeset_union(x, y) ((x) | (y))
#define	nodeset_intersect(x, y) ((x) & (y))
#define	nodeset_subtract(x, y)  ((x) & ~(y))

/* struct to return current cluster state */
struct clust_state {
	nodeid_t 	rp_nnodes;
	nodeid_t 	rp_local_nodeid;
	uint_t		rp_max_step;
	uint_t		rp_curr_step;
	uint_t		rp_curr_state;
	cmm::seqnum_t	rp_rseqnum;
	nodeset_t	rp_curr_members;
	nodeset_t	rp_all_nodes;
};

/* initialize ucmm */
#if (SOL_VERSION >= __s10)
extern int ucmm_initialize(const char *ucmm_name, const char *syslog_tag,
    const cmm::callback_var ucmm_callback, const cmm::callback_info &cbinfo,
    boolean_t use_process_membership);
#else
extern int ucmm_initialize(const char *ucmm_name, const char *syslog_tag,
    const cmm::callback_var ucmm_callback, const cmm::callback_info &cbinfo);
#endif

/* get current cluster state */
extern void ucmm_getcluststate(struct clust_state *);

/* stop or abort one or more nodes in the cluster */
extern void ucmm_stop_abort(nodeset_t stopset, nodeset_t abortset);

/* force another cluster reconfiguration */
extern void ucmm_reconfigure();

/* get the initial view numbers for a cluster node */
extern int ucmm_getinitialviewnumber(
    nodeid_t nodeid, cmm::seqnum_t &initialviewnumber);

#if (SOL_VERSION >= __s10)
extern void ucmm_step1_upgrade_callback_action();
extern void ucmm_step2_upgrade_callback_action();
#endif	/* (SOL_VERSION >= __s10) */

#endif	/* _UCMM_API_H */
