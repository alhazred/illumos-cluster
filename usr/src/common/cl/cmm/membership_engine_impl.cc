//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)membership_engine_impl.cc	1.12	08/11/13 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <cmm/cmm_impl.h>
#include <sys/clconf_int.h>
#include <cmm/membership_engine_impl.h>
#include <cmm/membership_threadpool.h>
#include <cmm/membership_api_impl.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_ns_int.h>
#include <nslib/ns.h>
#include <sys/vc_int.h>
#include <ccr/common.h>

// XXX Many places in this code check for engine reconfig in progress
// XXX by sleeping for 1ms and checking and so on.
// XXX Maybe we can replace those by some cv broadcast mechanism
// XXX maybe have a common engine_reconfig_underway_cv?

// Macro to check if this base node is the "leader" node
#define	I_AM_LEADER (leader_cmm == orb_conf::local_nodeid())

//
// Ring buffer to store membership traces.
//
#ifdef	MEMBERSHIP_DBGBUF
uint32_t membership_dbg_size = 64 * 1024;
dbg_print_buf	membership_dbg_buf(membership_dbg_size);
#endif

// Leader node id
nodeid_t leader_cmm;

// True after membership subsystem has been initialized
static bool membership_subsystem_initialized = false;

// True after membership versioning has been initialized
static bool membership_versioning_initialized = false;

//
// Private internal function to initialize the membership subsystem
// by creating/setting up the required threadpools and data structures.
//
static bool membership_subsystem_init_internal(void);

//
// Once CCR is refreshed, CCR calls back into membership subsystem
// to signal that CCR is ready to be read.
// This flag is then marked true.
//
bool is_ccr_ready = false;

// Static data members of the membership_engine_impl class

membership_engine_impl::automaton_callback_thread_event
    membership_engine_impl::autm_event =
    membership_engine_impl::AUTOMATON_MEMBERSHIP_CHANGE;
os::mutex_t membership_engine_impl::automaton_change_lock;
os::condvar_t membership_engine_impl::automaton_change_cv;

membership_engine_impl::ccr_change_thread_data_t
    membership_engine_impl::ccr_change_thread_data;
os::mutex_t membership_engine_impl::ccr_change_lock;
os::condvar_t membership_engine_impl::ccr_change_cv;

// The membership engine - one per base node
membership_engine_impl *
    membership_engine_impl::the_membership_engine_implp = NULL;

// Protects the current version data
os::mutex_t membership_version_lock;
// Current version of the membership subsystem
static version_manager::vp_version_t membership_current_version;
// Highest version possible for the membership subsystem
static version_manager::vp_version_t membership_highest_version =
	{MEMBERSHIP_VP_MAX_MAJOR, MEMBERSHIP_VP_MAX_MINOR};

//
// This data records the seqnum and membership of base cluster
// received from automaton's callback.
//
os::mutex_t latest_node_seqnum_lock;
cmm::seqnum_t latest_node_seqnum = 0;
cmm::membership_t latest_node_membership;

//
// A mapping of zone names to zone ids for the zone clusters
// that are up and running on this base node.
//
// When the membership engine gets a ZSD_DESTROY callback from Solaris
// once a zone is completely down, Solaris code passes in the zone id
// of the zone as an argument to the callback function.
// At that point in time, we cannot convert the zone id to a zone name,
// since zone_find_by_id() interface will not give us a non-NULL zone reference.
// So, we store a mapping of zone names and ids for 'up' zone clusters
// in advance.
//
// When the engine receives a zone 'up' notification,
// we store the zone name and the zone id into this data structure.
//
// When the engine receives a zone 'down' notification (Solaris ZSD_DESTROY
// callback), we use the zone id provided by the Solaris callback to
// get the corresponding zone name from this mapping.
// Then we remove the zone entry from this mapping, as the zone has gone down.
//
// This hashtable is indexed by the zone id as the key, and the value stored
// is the zone cluster name (same as the zone name).
//
os::mutex_t running_zone_clusters_lock;
static hashtable_t<char *, zoneid_t> running_zone_clusters;

// Zone state change callback key to be registered with Solaris
static zone_key_t zsd_callback_key;

// Prototype of function to be invoked by Solaris for zone destroy callbacks
static void zone_destroyed(zoneid_t, void *);

//
// This thread wakes up when the automaton signals (in automaton's end state)
// that base cluster CMM reconfiguration is done. This thread then notifies
// the membership engine that the base cluster CMM reconfiguration has ended,
// and base cluster node membership is stable now.
//
void
membership_engine_impl::start_automaton_change_thread(void *)
{
	cmm::seqnum_t last_seqnum = 0;
	cmm::seqnum_t new_seqnum = 0;
	cmm::membership_t new_membership;

	automaton_change_lock.lock();
	while (autm_event != AUTOMATON_CALLBACK_THREAD_EXIT) {

		//
		// Get a copy of the latest seqnum and membership
		// that the automaton has informed us about.
		//
		latest_node_seqnum_lock.lock();
		new_seqnum = latest_node_seqnum;
		new_membership = latest_node_membership;
		latest_node_seqnum_lock.unlock();

		//
		// Wait in this loop while any of these conditions is true :
		// (i) I am not leader, or
		// (ii) I am leader, but the last seqnum I processed
		// is the same as the new seqnum received.
		//
		// So the only time I get out of this loop is when
		// I am the leader and I have received a new seqnum that
		// is different from the last seqnum I processed, and hence
		// I have to drive the engine reconfig for the new seqnum.
		//
		while (!I_AM_LEADER || (new_seqnum == last_seqnum)) {
			//
			// No new kernel CMM reconfiguration has happened
			// since the last reconfiguration that we saw.
			// Sleep until new reconfiguration happens.
			//
			automaton_change_cv.wait(&automaton_change_lock);
			if (autm_event == AUTOMATON_CALLBACK_THREAD_EXIT) {
				break;
			}
			latest_node_seqnum_lock.lock();
			new_seqnum = latest_node_seqnum;
			new_membership = latest_node_membership;
			latest_node_seqnum_lock.unlock();
		}

		// A new kernel CMM reconfiguration has happened
		ASSERT(automaton_change_lock.lock_held());
		automaton_change_lock.unlock();

		//
		// We do not hold the lock while driving engine reconfig
		// because the engine reconfig could take long.
		//
		// Even if multiple automaton callbacks happen while
		// this thread is driving an engine reconfig, the latest
		// callback data would be considered, which is correct behaviour
		// for engine reconfiguration.
		//
		membership_engine_impl::the()->
		    leader_drives_engine_reconfig(new_seqnum, new_membership);

		//
		// Update local data to mark that we have
		// taken action for this seqnum
		//
		last_seqnum = new_seqnum;

		// Grab the lock
		automaton_change_lock.lock();
	}

	// Exiting
	automaton_change_lock.unlock();
}

//
// Returns true if a new kernel CMM reconfiguration has happened
// since the seqnum passed as argument
//
bool
new_base_cluster_reconfig_started(cmm::seqnum_t seqnum)
{
	latest_node_seqnum_lock.lock();
	bool retval = ((latest_node_seqnum > seqnum)? true: false);
	latest_node_seqnum_lock.unlock();
	return (retval);
}

// XXX
// XXX can this method fail because VM is not yet ready?
// XXX (something to do with cmm old version)?
// XXX
// XXX membership engine reconfig tries to look up remote node
// XXX local nameserver. will that be a problem if naming upgrade is not
// XXX complete during rolling upgrade scenario?
// XXX
bool
membership_initialize_versioning()
{
	Environment e;

	if (membership_versioning_initialized) {
		return (true);
	}

	// Initialize current version to 1.0
	membership_version_lock.lock();
	membership_current_version.major_num = 1;
	membership_current_version.minor_num = 0;
	membership_version_lock.unlock();

	// Get a pointer to the local version manager
	version_manager::vm_admin_var vm_adm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_adm_v)) {
		MEMBERSHIP_TRACE(("Membership engine : "
		    "Got nil reference to version manager.\n"));
		//
		// SCMSGS
		// @explanation
		// Got a nil reference to the version manager on the local node.
		// @user_action
		// No user action needed.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership: Got nil reference to version manager.");
	}

	// create our callback object
	membership_upgrade_callback *cbp = new membership_upgrade_callback();
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();

	//
	// Create our ucc sequence.  We have only one ucc.
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	version_manager::ucc_seq_t ucc_seq(1, 1);
	ucc_seq[0].ucc_name = os::strdup(MEMBERSHIP_UCC_NAME);
	ucc_seq[0].vp_name = os::strdup(MEMBERSHIP_VP_NAME);

	//
	// Actually register the callbacks and retrieve the current version.
	//
	version_manager::vp_version_t cur_version;
	vm_adm_v->register_upgrade_callbacks(
	    ucc_seq, cb_v, membership_highest_version, cur_version, e);
	if (e.exception() != NULL) {
		// We are not expecting any exception
		e.exception()->print_exception((char *)
		    "Membership: Failed to register upgrade callbacks");
		e.clear();
		MEMBERSHIP_TRACE(("Membership: Failed to register "
		    "membership upgrade callbacks\n"));
		return (false);
	}

	membership_version_lock.lock();
	if (membership_current_version.major_num < cur_version.major_num ||
	    (membership_current_version.major_num == cur_version.major_num &&
	    membership_current_version.minor_num < cur_version.minor_num)) {
		membership_current_version = cur_version;
	}
	// print out the version, for debugging purposes
	MEMBERSHIP_TRACE(("Current membership version: %d.%d\n",
	    membership_current_version.major_num,
	    membership_current_version.minor_num));
	membership_version_lock.unlock();

	membership_versioning_initialized = true;
	return (true);
}

//
// This method is used to initialize the membership subsystem :
// create the membership engine and start some threads/threadpools
// required for the membership subsystem.
//
// Return true if we do complete the initialization of membership
// subsystem, or if membership subsystem has already been initialized
// earlier.
// Return false if we fail to initialize versioning, or if we find
// that we are not running a software version that supports
// membership subsystem.
//
bool
membership_subsystem_init()
{
	if (!membership_initialize_versioning()) {
		// Failed to initialize versioning
		return (false);
	}

	if (!membership_api_support_available()) {
		//
		// We are not yet running a software version
		// that should have membership subsystem present.
		//
		return (false);
	}

	if (membership_subsystem_initialized) {
		// We have already initialized membership subsystem
		return (true);
	}

	bool retval = membership_subsystem_init_internal();
	return (retval);
}

static bool
membership_subsystem_init_internal(void)
{
	Environment e;

	membership_manager_impl::init_state_transition_table();

	naming::naming_context_var ctx_v = ns::local_nameserver_c1();

	if (membership_threadpool::initialize() != 0) {
		CL_PANIC(0);
	}
	if (callback_threadpool::initialize() != 0) {
		CL_PANIC(0);
	}

	// Create the sole membership engine
	membership_engine_impl::initialize();
	membership::engine_var engine_ref_v =
	    membership_engine_impl::the()->get_objref();

	// Create the sole membership api object
	membership_api_impl::initialize();
	membership::api_var mem_api_ref_v =
	    membership_api_impl::the()->get_objref();

	// Bind a reference to the membership api object in the nameserver
	ctx_v->bind(membership_api_namep, mem_api_ref_v, e);
	CL_PANIC(e.exception() == NULL);

	//
	// We bind a reference to the local node's membership engine
	// in the nameserver at the end of the initialization
	// of the membership subsystem. That is because once engine
	// is bound in nameserver, we are assured that all membership
	// subsystem on this node is ready for use.
	// We need to take such care as other nodes might try to
	// look up the membership engine of our local node
	// from the nameserver, and hence our membership subsystem
	// should be ready by the time they get a reference
	// to the membership engine on this local node.
	//

	// Bind a reference to the membership engine in the nameserver
	ctx_v->bind(membership_engine_namep, engine_ref_v, e);
	CL_PANIC(e.exception() == NULL);

	// Register with Solaris for zone destroy callbacks
	zone_key_create(&zsd_callback_key, NULL, NULL, zone_destroyed);

	membership_subsystem_initialized = true;
	MEMBERSHIP_TRACE(("Membership: Subsystem initialized\n"));
	return (true);
}

//
// This method is used to shutdown the membership subsystem.
//
void
membership_subsystem_fini()
{
	if (!membership_api_support_available()) {
		//
		// We are not yet running a software version
		// that should have membership subsystem present.
		//
		return;
	}

	if (!membership_subsystem_initialized) {
		// We have not initialized membership subsystem
		return;
	}

	membership_threadpool::shutdown();
	callback_threadpool::shutdown();

	Environment e;
	naming::naming_context_var ctx_v = ns::local_nameserver_c1();

	ctx_v->unbind(membership_engine_namep, e);
	CL_PANIC(e.exception() == NULL);

	ctx_v->unbind(membership_api_namep, e);
	CL_PANIC(e.exception() == NULL);

	membership_engine_impl::shutdown();
	membership_api_impl::shutdown();

	MEMBERSHIP_TRACE(("Membership: Subsystem has been shut down\n"));
}

//
// Function to receive zone state change callbacks from Solaris.
// When a zone goes down completely, Solaris gives us a ZSD_DESTROY callback.
// At this point in time, the file systems in the zone have been unmounted
// and the network interfaces in the zone have been unplumbed, the processes
// have been killed as well. So we can safely consider the zone as down
// and deliver the zone 'down' event to the membership managers registered
// for such an event.
//
static void
zone_destroyed(zoneid_t zone_id, void *)
{
	char *remove_zone_namep = NULL;

	running_zone_clusters_lock.lock();
	char *zone_namep = running_zone_clusters.lookup(zone_id);
	if (zone_namep == NULL) {
		//
		// We could land up in this scenario for two reasons :
		// (1) The zone is not a 'cluster' brand zone;
		// ignore this callback as membership only deals with
		// 'cluster' brand zones.
		// (2) It is possible that the 'cluster' brand zone
		// went down before it came up fully and notified
		// membership subsystem that it came up.
		// To know the zone was a 'cluster' brand zone,
		// we need to query clconf; but we do not have
		// the zone name to query clconf.
		// So we have no other option but to return;
		// no need to reconfigure membership
		// as the zone was never up in the membership.
		//
		// We cannot trace a debug message here;
		// if we do that, we will be tracing messages for
		// every non-global zone death on the system.
		//
		running_zone_clusters_lock.unlock();
		return;
	}

	MEMBERSHIP_TRACE(("Membership engine : zone_destroyed() received "
	    "zone destroy callback for zone %s\n", zone_namep));

	//
	// Reaching here means the zone id was on the mapping
	// of running zone clusters. That means we should be able
	// to convert the zone cluster name to a cluster ID.
	//
	uint_t cluster_id = 0;
	if (clconf_get_cluster_id(zone_namep, &cluster_id)) {
		MEMBERSHIP_TRACE(("Membership engine : unable to get "
		    "cluster ID for cluster '%s'\n", zone_namep));
		//
		// SCMSGS
		// @explanation
		// Membership subsystem was unable to convert a zone cluster
		// name to a cluster ID when processing a Solaris notification
		// that the specified zone went down.
		// @user_action
		// If the zone belongs to a valid zone cluster, reboot
		// the machine hosting the zone.
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available
		// to prevent future occurrences of this problem.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership: Unable to get cluster ID for cluster '%s'.",
		    zone_namep);

		//
		// We cannot send the event to the membership engine
		// without the cluster ID; we can remove the zone
		// from the mapping and then we return here.
		//
		remove_zone_namep = running_zone_clusters.remove(zone_id);
		running_zone_clusters_lock.unlock();
		ASSERT(remove_zone_namep == zone_namep); // Sanity checking
		delete [] zone_namep;
		remove_zone_namep = zone_namep = NULL;
		return;
	}

	//
	// Now that we have obtained the cluster ID using the zone cluster name,
	// we can remove the zone from the mapping.
	//
	remove_zone_namep = running_zone_clusters.remove(zone_id);
	running_zone_clusters_lock.unlock();
	ASSERT(remove_zone_namep == zone_namep);	// Sanity checking

	//
	// Allocate an event info data structure to dispatch
	// to membership managers
	//
	membership::engine_event_info event_info;
	event_info.mem_manager_info.mem_type = membership::ZONE_CLUSTER;
	event_info.mem_manager_info.cluster_id = cluster_id;
	event_info.mem_manager_info.process_namep = (char *)NULL;
	event_info.event = membership::NODE_DOWN;

	Environment e;
	membership::engine_var engine_v = cmm_ns::get_membership_engine_ref();
	ASSERT(!CORBA::is_nil(engine_v));
	engine_v->deliver_event_to_engine(
	    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT, event_info, e);
	if (e.exception() != NULL) {
		//
		// This is very unlikely; we are telling the membership
		// engine on the local base node that a local zone went down.
		//
		const char *exp_namep = (e.exception()->_name())?
		    (e.exception()->_name()) : "NULL";
		//
		// SCMSGS
		// @explanation
		// Failed to notify the cluster membership infrastructure
		// that a zone cluster node went down.
		// Support for the zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership : Failure in notifying membership subsystem "
		    "that zone %s went down, exception <%s> thrown.",
		    zone_namep, exp_namep);

		e.clear();
	}

	delete [] zone_namep;
	zone_namep = remove_zone_namep = NULL;
}

//
// When a zone cluster is created/deleted, clconf gets a callback
// and it then in turn notifies the membership subsystem by calling this method.
// Here we signal the CCR change thread to wake up.
//
void
membership_engine_impl::clconf_callback_to_membership(
    const char *cl_namep, ccr_change_thread_event update) const
{
	char *textp = NULL;

	ccr_change_lock.lock();
	if (update == ZONE_CLUSTER_ADDED) {
		textp = (char *)"add";
	} else {
		ASSERT(update == ZONE_CLUSTER_REMOVED);
		textp = (char *)"delete";
	}
	MEMBERSHIP_TRACE(("Membership: Callback from clconf "
	    "to %s zone cluster %s\n", textp, cl_namep));
	(void) os::strcpy(ccr_change_thread_data.zone_cluster_name, cl_namep);
	ccr_change_thread_data.event = update;
	ccr_change_thread_data.task_to_do = true;
	ccr_change_cv.signal();
	ccr_change_lock.unlock();
}

//
// The CCR change thread.
//
// When a zone cluster is created/deleted, clconf delivers callback.
// The CCR change thread wakes up and delivers the event to the engine.
//
// static void
void
membership_engine_impl::start_ccr_change_thread(void *)
{
	Environment e;
	CORBA::Exception *exp = NULL;
	membership::engine_var engine_v = membership::engine::_nil();

	ccr_change_lock.lock();
	while (1) {
		ccr_change_cv.wait(&ccr_change_lock);
		//
		// The lock is held through the creation/deletion process.
		// So callbacks from clconf would be handled serially.
		//
		ASSERT(ccr_change_lock.lock_held());

		if (!ccr_change_thread_data.task_to_do) {
			// False wake-up; we have nothing to do
			continue;
		}

		if (ccr_change_thread_data.event == CCR_CHANGE_THREAD_EXIT) {
			MEMBERSHIP_TRACE((
			    "Membership: CCR change thread exiting\n"));
			break;
		}

		//
		// An optimization :
		// No need to deliver zone cluster membership creation/deletion
		// event to engine if the local node is not the leader node.
		// The CCR change for zone cluster creation/deletion happens
		// on every node, and hence the CCR change thread on the leader
		// node would anyway notify the leader engine about the event.
		//
		if (!I_AM_LEADER) {
			// We cannot do the task assigned
			ccr_change_thread_data.task_to_do = false;
			continue;
		}

		if (ccr_change_thread_data.event == ZONE_CLUSTER_ADDED) {
			MEMBERSHIP_TRACE(("Membership: CCR change thread "
			    "woken up; create zc %s\n",
			    ccr_change_thread_data.zone_cluster_name));
		} else {
			ASSERT(ccr_change_thread_data.event ==
			    ZONE_CLUSTER_REMOVED);
			MEMBERSHIP_TRACE(("Membership: CCR change thread "
			    "woken up; delete zc %s\n",
			    ccr_change_thread_data.zone_cluster_name));
		}

		engine_v = cmm_ns::get_membership_engine_ref();
		// The engine must be registered in nameserver by now
		ASSERT(!CORBA::is_nil(engine_v));

		//
		// We try to get the cluster ID of the zone cluster.
		// clconf first updates its own data structures before
		// telling membership subsystem about the zone cluster
		// creation/deletion.
		// In the case of zone cluster creation, clconf would have
		// the cluster ID for the zone cluster, and so we can query
		// clconf to get the cluster ID.
		// In the case of zone cluster deletion, clconf would already
		// have deleted its data structures for the zone cluster
		// and hence cannot provide us with the cluster ID for
		// the zone cluster. So, in this case, the engine gets the
		// cluster ID from the membership manager for the zone cluster
		// membership.
		//
		uint_t clid = 0;
		if (ccr_change_thread_data.event == ZONE_CLUSTER_ADDED) {
			if (clconf_get_cluster_id(
			    ccr_change_thread_data.zone_cluster_name, &clid)) {
				//
				// clconf first updates its data structures
				// before notifying membership about
				// a zone cluster creation. So, at this point,
				// clconf must have the cluster ID
				// for the zone cluster.
				//
				ASSERT(0);

				// lint complains that code is unreachable
				//lint -e527

				// lint complains that loop is not reachable
				//lint -e827
				MEMBERSHIP_TRACE(("Membership engine : "
				    "CCR change thread could not get "
				    "cluster ID for zc %s\n",
				    ccr_change_thread_data.zone_cluster_name));
				//lint +e827

				// We cannot do the task assigned
				ccr_change_thread_data.task_to_do = false;
				continue;
				//lint +e527
			}
		} else {
			ASSERT(ccr_change_thread_data.event ==
			    ZONE_CLUSTER_REMOVED);
			const char *cl_namep =
			    ccr_change_thread_data.zone_cluster_name;
			membership_engine_impl *enginep =
			    membership_engine_impl::the();
			enginep->membership_managers_global_rwlock.rdlock();
			membership_manager_impl *mem_managerp =
			    enginep->get_membership_manager_internal(
			    membership::ZONE_CLUSTER,
			    cl_namep, (const char *)NULL);
			if (mem_managerp == NULL) {
				//
				// In the process of a zone cluster deletion,
				// clconf notifies membership subsystem about
				// the deletion.
				// All membership deletions are driven
				// by the leader engine.
				// Various engines try to request the leader
				// for the deletion. The first request
				// received by the leader engine goes through.
				// It may happen that by the time this engine
				// receives a callback from clconf and tries
				// to contact the leader engine, the deletion
				// of the membership might have already been
				// completed by the engines. If we cannot
				// find the membership manager object
				// in the local engine's data structures,
				// it means the deletion has been done,
				// and so we do not take any action.
				//
				MEMBERSHIP_TRACE(("Membership engine : "
				    "CCR change thread could not get "
				    "manager object for zc %s\n",
				    ccr_change_thread_data.zone_cluster_name));
				// We cannot do the task assigned
				ccr_change_thread_data.task_to_do = false;
				enginep->
				    membership_managers_global_rwlock.unlock();
				continue;
			}
			clid = mem_managerp->cluster_id;
			enginep->membership_managers_global_rwlock.unlock();
		}

		// Prepare data to be passed on to engine
		membership::engine_event_info data;
		data.mem_manager_info.mem_type = membership::ZONE_CLUSTER;
		data.mem_manager_info.cluster_id = clid;
		data.mem_manager_info.process_namep = (char *)NULL;

		membership::engine_event event;
		// lint complains about unused enum constant in switch
		//lint -e788
		switch (ccr_change_thread_data.event) {
		case ZONE_CLUSTER_ADDED:
			event = membership::CREATE_MEMBERSHIP;
			break;
		case ZONE_CLUSTER_REMOVED:
			event = membership::DELETE_MEMBERSHIP;
			break;
		default:
			ASSERT(0);
			// lint complains about unreachable code
			//lint -e527
			// We cannot do the task assigned
			ccr_change_thread_data.task_to_do = false;
			continue;
			//lint +e527
		}
		//lint +e788

		//
		// Deliver event (creation/deletion) to engine.
		// If engines are reconfiguring, this call would wait.
		// If engine returns a retry exception, then this code
		// would retry the creation/deletion request to engine.
		//
		while (1) {
			engine_v->deliver_event_to_engine(event, data, e);
			exp = e.exception();
			if (exp == NULL) {
				break;
			}
			if (membership::retry_create_or_delete::_exnarrow(
			    exp)) {
				// Retry creation or deletion
				MEMBERSHIP_TRACE((
				    "Membership engine : CCR change "
				    "thread received retry_create_or_delete "
				    "exception from engine, cluster %s\n",
				    ccr_change_thread_data.zone_cluster_name));
			} else {
				MEMBERSHIP_TRACE((
				    "Membership engine : CCR change "
				    "thread received unexpected exception %s "
				    "from engine, cluster %s\n", exp->_name(),
				    ccr_change_thread_data.zone_cluster_name));
				ASSERT(0);
			}
			e.clear();
		}

		// Mark that the task has been done
		ccr_change_thread_data.task_to_do = false;
	}

	// Exiting
	ccr_change_thread_data.task_to_do = false;
	ccr_change_lock.unlock();
}

membership_manager_impl *
membership_engine_impl::get_membership_manager_internal(
    membership::membership_type mem_type,
    const char *cl_namep, const char *proc_namep)
{
	membership_manager_entry *entryp = NULL;
	membership_manager_impl *mem_managerp = NULL;

	//
	// This is a read operation, so caller should be holding
	// either read or write lock
	//
	ASSERT(membership_managers_global_rwlock.lock_held());

	string_hashtable_t<membership_manager_entry *>::iterator
	    iter(membership_managers);
	while ((entryp = iter.get_current()) != NULL) {
		iter.advance();
		SList<membership_manager_impl>::ListIterator
		    list_iter(entryp->manager_list);
		while ((mem_managerp = list_iter.get_current()) != NULL) {
			list_iter.advance();
			if (mem_managerp->match_manager_info(
			    mem_type, cl_namep, proc_namep)) {
				// Found
				return (mem_managerp);
			}
		}
	}
	// Not found
	return (NULL);
}

//
// Register a membership manager for an event.
// Returns true on success, false on failure.
//
bool
membership_engine_impl::register_mem_manager(
    membership_manager_impl *mem_managerp, const char *event_namep)
{
	ASSERT(event_namep);
	ASSERT(mem_managerp);

	event_to_manager_mapping_entry *entryp =
	    event_to_manager_mapping.lookup(event_namep);
	if (!entryp) {
		// No entry exists for the event.
		entryp = new event_to_manager_mapping_entry;
		entryp->entry_lock.lock();

		event_to_manager_mapping_global_lock.lock();
		event_to_manager_mapping.add(entryp, event_namep);
		event_to_manager_mapping_global_lock.unlock();
	} else {
		entryp->entry_lock.lock();
	}

	SList<membership_manager_impl>::ListIterator iter(entryp->manager_list);
	for (membership_manager_impl *iter_managerp = NULL;
	    (iter_managerp = iter.get_current()) != NULL; iter.advance()) {
		if (os::strcmp(mem_managerp->mem_manager_name,
		    iter_managerp->mem_manager_name) == 0) {
			// Already exists
			ASSERT(0);
			// lint complains about unreachable code
			//lint -e527
			entryp->entry_lock.unlock();
			return (false);
			//lint +e527
		}
	}
	entryp->manager_list.append(mem_managerp);
	entryp->entry_lock.unlock();

	MEMBERSHIP_TRACE(("Membership engine : registered manager <%s> "
	    "for event <%s>\n", mem_managerp->mem_manager_name, event_namep));
	return (true);
}

//
// Unregister a membership manager for an event.
// Returns true on success, false on failure.
//
bool
membership_engine_impl::unregister_mem_manager(
    membership_manager_impl *mem_managerp, const char *event_namep)
{
	ASSERT(event_namep);
	ASSERT(mem_managerp);

	event_to_manager_mapping_entry *entryp =
	    event_to_manager_mapping.lookup(event_namep);
	if (!entryp) {
		ASSERT(0);
		return (false);	/*lint !e527 */
	}

	entryp->entry_lock.lock();
	SList<membership_manager_impl>::ListIterator iter(entryp->manager_list);

	bool exists = false;
	membership_manager_impl *iter_managerp = NULL;
	for (; (iter_managerp = iter.get_current()) != NULL; iter.advance()) {
		if (os::strcmp(mem_managerp->mem_manager_name,
		    iter_managerp->mem_manager_name) == 0) {
			// Exists
			exists = true;
			break;
		}
	}
	if ((!exists) || (mem_managerp != iter_managerp)) {
		MEMBERSHIP_TRACE((
		    "Membership engine : unregister of manager <%s> for "
		    "event <%s> failed, as it has been already unregistered\n",
		    mem_managerp->mem_manager_name, event_namep));
		ASSERT(0);
		entryp->entry_lock.unlock();	/*lint !e527 */
		return (false);	/*lint !e527 */
	}

	if (!entryp->manager_list.erase(mem_managerp)) {
		entryp->entry_lock.unlock();
		return (false);
	}
	entryp->entry_lock.unlock();

	MEMBERSHIP_TRACE(("Membership engine : unregistered manager <%s> "
	    "for event <%s>\n", mem_managerp->mem_manager_name, event_namep));
	return (true);
}

//
// Engine on local base node uses this method to request the engine on
// leader node to drive the creation/deletion of a membership manager.
//
void
membership_engine_impl::request_to_leader(
    membership::engine_event event_triggered,
    const membership::membership_manager_info &manager_info, Environment &e)
{
	ASSERT(I_AM_LEADER);
	ASSERT((event_triggered == membership::CREATE_MEMBERSHIP) ||
	    (event_triggered == membership::DELETE_MEMBERSHIP));

	// Wait for any engine reconfiguration to finish
	while (engine_reconfig_underway) {
		os::usecsleep((os::usec_t)1000);	// 1ms
	}

	// Grab a write lock for the membership managers table
	membership_managers_global_rwlock.wrlock();

	MEMBERSHIP_TRACE((
	    "Membership engine : request_to_leader() to %s membership "
	    "<mem_type = %d, clid = %d, process name = %s>\n",
	    ((event_triggered == membership::CREATE_MEMBERSHIP)?
	    "create" : "delete"), manager_info.mem_type,
	    manager_info.cluster_id, (const char *)manager_info.process_namep));

	latest_node_seqnum_lock.lock();
	cmm::seqnum_t node_seqnum = latest_node_seqnum;
	cmm::membership_t node_membership = latest_node_membership;
	latest_node_seqnum_lock.unlock();

	// Let the leader drive the creation/deletion
	if (!drive_creation_or_deletion(
	    event_triggered, manager_info, node_seqnum, node_membership, e)) {
		//
		// Either exception thrown, or another request
		// for the same creation/deletion did the task.
		//
		membership_managers_global_rwlock.unlock();
		return;
	}

	//
	// The creation/deletion is finished now.
	// But zone cluster membership deletion is a special case.
	// For the deletion of zone cluster membership, we also delete
	// the process memberships associated with the zone cluster,
	// as the zone cluster is being deleted.
	//

	if (event_triggered == membership::CREATE_MEMBERSHIP) {
		membership_managers_global_rwlock.unlock();
		return;
	}
	if (manager_info.mem_type != membership::ZONE_CLUSTER) {
		membership_managers_global_rwlock.unlock();
		return;
	}

	//
	// We reach here if it is a case of zone cluser membership deletion.
	// Also note that of all the requests to the leader for deletion
	// of a particular membership, only one would proceed till this stage.
	// This is because only one request does the deletion of the
	// zone cluster membership, and the rest find that the zone cluster
	// membership has gone and hence they return without further progress.
	//
	char clid_str[CLID_MAX_LEN];
	int num_digits = os::itoa((int)manager_info.cluster_id, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		ASSERT(0);
		membership_managers_global_rwlock.unlock();	/*lint !e527 */
		return;	/*lint !e527 */
	}

	membership_manager_entry *entryp = membership_managers.lookup(clid_str);
	if (entryp == NULL) {
		//
		// The zone cluster membership manager was the only membership
		// manager object on the list of membership managers related
		// to the zone cluster. So, the deletion of the zone cluster
		// membership manager has resulted in the removal of the entry
		// for the zone cluster from the membership managers hashtable.
		//
		membership_managers_global_rwlock.unlock();
		return;
	}

	// Drive deletion of all membership managers for the cluster
	membership_manager_impl *mem_managerp = NULL;
	SList<membership_manager_impl>::ListIterator iter(entryp->manager_list);
	mem_managerp = iter.get_current();
	while (mem_managerp != NULL) {
		membership::membership_manager_info mem_manager_info;
		mem_managerp->fill_membership_manager_info(mem_manager_info);

		//
		// Get to the next membership manager before
		// we delete the current one
		//
		iter.advance();
		mem_managerp = iter.get_current();

		if (!drive_creation_or_deletion(membership::DELETE_MEMBERSHIP,
		    mem_manager_info, node_seqnum, node_membership, e)) {
			membership_managers_global_rwlock.unlock();
			return;
		}
	}

	membership_managers_global_rwlock.unlock();
}

//
// On the "leader" engine, this method is used to drive
// the creation/deletion of a membership manager.
// Returns true if it actually succeeded creating/deleting the membership.
//
bool
membership_engine_impl::drive_creation_or_deletion(
    membership::engine_event event_triggered,
    const membership::membership_manager_info &manager_info,
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership,
    Environment &e)
{
	membership_manager_impl *mem_managerp = NULL;

	ASSERT(I_AM_LEADER);
	ASSERT(membership_managers_global_rwlock.write_held());

	//
	// A request for creation/deletion of the membership manager
	// might have completed. So, check if the object exists or not.
	//
	char *mem_manager_namep = NULL;
	mem_managerp = get_membership_manager_internal(manager_info);
	if (event_triggered == membership::CREATE_MEMBERSHIP) {
		if (mem_managerp) {
			// Object already created
			return (false);
		}
		char *cluster_namep = NULL;
		if (clconf_get_cluster_name(
		    manager_info.cluster_id, &cluster_namep)) {
			//
			// clconf must have the cluster name for the cluster ID.
			// If not, it means the zone cluster was deleted
			// while membership is trying to create the membership
			// data structures for the zone cluster.
			//
			if (cluster_namep) {
				delete [] cluster_namep;
			}
			return (false);
		}
		mem_manager_namep =
		    membership_manager_impl::construct_name_to_log(
		    manager_info.mem_type, cluster_namep,
		    manager_info.process_namep);
		ASSERT(mem_manager_namep);
		delete [] cluster_namep;
	} else {
		ASSERT(event_triggered == membership::DELETE_MEMBERSHIP);
		if (!mem_managerp) {
			// Object already deleted
			return (false);
		}
		mem_manager_namep = os::strdup(mem_managerp->name_to_log);
	}

	// Start the creation/deletion of membership manager
	MEMBERSHIP_TRACE(("Membership engine : Leader engine starting %s of "
	    "membership manager <%s>\n",
	    ((event_triggered == membership::CREATE_MEMBERSHIP)?
	    "creation" : "deletion"), mem_manager_namep));

	//
	// The leader engine now asks every peer engine
	// to create/delete the membership manager.
	//
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {

		if (new_base_cluster_reconfig_started(node_seqnum)) {
			// Return retry exception.
			e.exception(new membership::retry_create_or_delete());
			delete [] mem_manager_namep;
			return (false);
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (CORBA::is_nil(engine_refs[nid])) {
			continue;
		}

		Environment env;
		CORBA::Exception *exp = NULL;
		if (event_triggered == membership::CREATE_MEMBERSHIP) {
			engine_refs[nid]->membership_create(manager_info, env);
		} else {
			ASSERT(event_triggered ==
			    membership::DELETE_MEMBERSHIP);
			engine_refs[nid]->membership_delete(manager_info, env);
		}

		if ((exp = env.exception()) != NULL) {
			const char *str = ((event_triggered ==
			    membership::CREATE_MEMBERSHIP)? "create": "delete");
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE "
				    "while telling peer engine (node %d) to %s "
				    "membership <%s>\n", nid, str,
				    mem_manager_namep));
				// Return retry exception.
				e.exception(
				    new membership::retry_create_or_delete());
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "unexpected exception %s while "
				    "telling peer engine (node %d) "
				    "to %s membership <%s>\n",
				    exp->_name(), nid, str, mem_manager_namep));
				ASSERT(0);
			}
			env.clear();
			delete [] mem_manager_namep;
			return (false);
		}
	}

	if (event_triggered == membership::DELETE_MEMBERSHIP) {
		//
		// If we deleted a membership, then we ask that particular
		// membership manager on the leader node to do its shutdown.
		// In its shutdown, the leader manager for the membership
		// releases the references to its peers.
		// We do this after all the membership managers for the
		// particular membership have stopped their state machines
		// and have basically shutdown.
		// We release the references at the end, because if we release
		// the references before all membership managers have done
		// their shutdown, then the manager objects could get
		// unreferenced and get deleted.
		//
		if (!mem_managerp->leader_fini()) {
			MEMBERSHIP_TRACE(("Membership Engine : "
			    "leader fini for manager '%s' failed\n",
			    mem_manager_namep));
		}
	} else {
		ASSERT(event_triggered == membership::CREATE_MEMBERSHIP);
		//
		// If we created a membership, then we ask the new membership
		// manager created on the leader node to do its initialization.
		// In its initialization, the leader manager for the membership
		// keeps references to its peers.
		//
		mem_managerp = get_membership_manager_internal(manager_info);
		// Engine just created the membership manager object
		ASSERT(mem_managerp != NULL);
		if (!mem_managerp->leader_initialization(
		    node_seqnum, node_membership)) {
			MEMBERSHIP_TRACE(("Membership Engine : "
			    "leader initialization for manager '%s' failed\n",
			    mem_manager_namep));
		}
	}

	//
	// We do not delete the locks in create/delete hashtable.
	// Any future creation/deletion of the same membership would not need to
	// create the lock as it is already present; and would simply try
	// to grab the lock.
	// XXX Think of a good way to clean them up.
	//
	delete [] mem_manager_namep;
	return (true);
}

//
//
// "Leader" engine communicates to all engines
// a list of memberships to be created or deleted.
//
// (1) During engine reconfiguration, the "leader" engine
// queries each engine for a list of membership managers
// that the engine knows of. The "leader" then forms
// a list of all known membership managers, and sends
// the sequence to each engine, so that the latter
// can create the membership managers that it was unaware of.
//
// (2) When a membership manager is created dynamically
// (zone cluster creation/deletion, process membership
// creation/deletion), the "leader" directs all engines
// to create or delete the membership.
//
// The engine on local base node follows instructions
// from leader engine to create/delete membership managers.
//
void
membership_engine_impl::membership_create(
    const membership::membership_manager_info &manager_info, Environment &)
{
	// Leader already holds write lock when creating/deleting membership
	if (!I_AM_LEADER) {
		membership_managers_global_rwlock.wrlock();
	}
	if (get_membership_manager_internal(manager_info) != NULL) {
		ASSERT(0);
	}

	// Object does not exist; create it
	create_mem_manager(manager_info);
	if (!I_AM_LEADER) {
		membership_managers_global_rwlock.unlock();
	}
}

void
membership_engine_impl::membership_delete(
    const membership::membership_manager_info &manager_info, Environment &)
{
	// Leader already holds write lock when creating/deleting membership
	if (!I_AM_LEADER) {
		membership_managers_global_rwlock.wrlock();
	}
	if (get_membership_manager_internal(manager_info) == NULL) {
		ASSERT(0);
	}

	// Object exists; delete it
	delete_mem_manager(manager_info);
	if (!I_AM_LEADER) {
		membership_managers_global_rwlock.unlock();
	}
}

bool
manager_info_match(const membership::membership_manager_info &info_1,
    const membership::membership_manager_info &info_2)
{
	if (info_1.mem_type != info_2.mem_type) {
		return (false);
	}

	if (info_1.cluster_id != info_2.cluster_id) {
		return (false);
	}

	if (((const char *)(info_1.process_namep) &&
	    !(const char *)(info_2.process_namep)) ||
	    (!(const char *)(info_1.process_namep) &&
	    (const char *)(info_2.process_namep))) {
		// One name is NULL but the other is not
		return (false);
	} else if ((const char *)(info_1.process_namep) &&
	    (const char *)(info_2.process_namep)) {
		// Neither name is NULL, compare strings
		if (os::strcmp((const char *)(info_1.process_namep),
		    (const char *)(info_2.process_namep)) != 0) {
			return (false);
		}
	}

	return (true);
}

//
// The leader engine has delivered this engine a list of cluster memberships
// to be maintained. It is considered to be the "truth copy" of
// cluster memberships to be maintained.
// (i) First, this engine iterates through its own list of memberships
// to see if there exists any cluster membership which is present on
// this engine but not on the leader's list.
// If so, delete that cluster membership on this engine.
// (ii) Then, iterate through the leader's list of cluster memberships to see
// if the leader's list contains any cluster membership which this engine
// is not aware of. If so, then create that cluster membership on this engine.
//
// lint complains that variable 'managerp' is not accessed
//lint -e550
void
membership_engine_impl::leader_delivers_cl_mem_list(
    const membership::membership_manager_info_seq &leader_cl_mem_list,
    Environment &e)
{
	ASSERT(engine_reconfig_underway);

	MEMBERSHIP_TRACE((
	    "Membership engine : leader delivered cluster membership list\n"));

	// If CCR is refreshed, proceed; else throw exception and return
	if (!is_ccr_ready) {
		MEMBERSHIP_TRACE(("Membership engine : CCR not yet refreshed, "
		    "so throwing membership::ccr_not_refreshed exception\n"));
		e.exception(new membership::ccr_not_refreshed());
		return;
	}
	ASSERT(is_ccr_ready);

	membership_managers_global_rwlock.wrlock();
	unsigned int leader_list_count = leader_cl_mem_list.length();

	//
	// Delete any extra cluster memberships that the local engine knows
	// about but the leader does not know about.
	//
	string_hashtable_t<membership_manager_entry *>::iterator
	    iter(membership_managers);
	SList<membership_manager_impl> managers_to_be_deleted;
	for (membership_manager_entry *entryp = NULL;
	    (entryp = iter.get_current()) != NULL; iter.advance()) {
		SList<membership_manager_impl>::ListIterator
		    list_iter(entryp->manager_list);
		for (membership_manager_impl *mem_managerp = NULL;
		    ((mem_managerp = list_iter.get_current()) != NULL);
		    list_iter.advance()) {
			//
			// For each cluster membership maintained on this list,
			// check if it is present in the leader's list.
			//
			if ((mem_managerp->type != membership::BASE_CLUSTER) &&
			    (mem_managerp->type != membership::ZONE_CLUSTER)) {
				// Not a cluster membership type
				continue;
			}
			membership::membership_manager_info info;
			mem_managerp->fill_membership_manager_info(info);
			bool found = false;
			for (unsigned int index = 0;
			    index < leader_list_count; index++) {
				if (manager_info_match(
				    info, leader_cl_mem_list[index])) {
					found = true;
					break;
				}
			}
			if (!found) {
				managers_to_be_deleted.append(mem_managerp);
			}
		}
	}

	//
	// We now have the list of cluster memberships to be deleted
	// on this engine. Iterate over the list and do the deletions.
	//
	SList<membership_manager_impl>::ListIterator
	    del_iter(managers_to_be_deleted);
	for (membership_manager_impl *managerp = NULL;
	    ((managerp = del_iter.get_current()) != NULL); del_iter.advance()) {
		membership::membership_manager_info info;
		managerp->fill_membership_manager_info(info);
		delete_mem_manager(info);
	}

	//
	// We are done with the list of membership managers to be deleted.
	// So lets empty out the list. When this method returns, the list
	// will be destroyed.
	//
	membership_manager_impl *managerp = NULL;
	while ((managerp = managers_to_be_deleted.reapfirst()) != NULL) {
	}

	//
	// Now find out if the leader's list contains any cluster memberships
	// that this engine does not know about. Create those memberships.
	//
	for (unsigned int index = 0; index < leader_list_count; index++) {
		//
		// For each membership manager info in the leader's list,
		// check if the membership manager exists on this engine or not.
		//
		if (get_membership_manager_internal(
		    leader_cl_mem_list[index]) != NULL) {
			continue;
		} else {
			create_mem_manager(leader_cl_mem_list[index]);
		}
	}
	membership_managers_global_rwlock.unlock();
}
//lint +e550

//
// The leader engine has delivered this engine a list of process memberships
// to be maintained. It is considered to be the "truth copy" of
// process memberships to be maintained.
// Note that this call happens in a step of the engine reconfiguration.
// At this step, it is ensured that there are no process memberships
// present on this engine that the leader does not know about.
// Hence we do not have to delete any extra memberships here.
// Only iterate through the leader's list of process memberships to see
// if the leader's list contains any process membership which this engine
// is not aware of. If so, then create that process membership on this engine.
//
void
membership_engine_impl::leader_delivers_proc_mem_list(
    const membership::membership_manager_info_seq &leader_proc_mem_list,
    Environment &e)
{
	ASSERT(engine_reconfig_underway);
	MEMBERSHIP_TRACE((
	    "Membership engine : leader delivered process membership list\n"));

	// If CCR is refreshed, proceed; else throw exception and return
	if (!is_ccr_ready) {
		MEMBERSHIP_TRACE(("Membership engine : CCR not yet refreshed, "
		    "so throwing membership::ccr_not_refreshed exception\n"));
		e.exception(new membership::ccr_not_refreshed());
		return;
	}
	ASSERT(is_ccr_ready);

	membership_managers_global_rwlock.wrlock();
	unsigned int leader_list_count = leader_proc_mem_list.length();

	//
	// Delete any extra process memberships that the local engine knows
	// about but the leader does not know about.
	//
	string_hashtable_t<membership_manager_entry *>::iterator
	    iter(membership_managers);
	SList<membership_manager_impl> managers_to_be_deleted;
	for (membership_manager_entry *entryp = NULL;
	    (entryp = iter.get_current()) != NULL; iter.advance()) {
		SList<membership_manager_impl>::ListIterator
		    list_iter(entryp->manager_list);
		for (membership_manager_impl *mem_managerp = NULL;
		    ((mem_managerp = list_iter.get_current()) != NULL);
		    list_iter.advance()) {
			//
			// For each process membership maintained on this list,
			// check if it is present in the leader's list.
			//
			if ((mem_managerp->type !=
			    membership::PROCESS_UP_ZONE_DOWN) &&
			    (mem_managerp->type !=
			    membership::PROCESS_UP_PROCESS_DOWN)) {
				// Not a process membership type
				continue;
			}
			membership::membership_manager_info info;
			mem_managerp->fill_membership_manager_info(info);
			bool found = false;
			for (unsigned int index = 0;
			    index < leader_list_count; index++) {
				if (manager_info_match(
				    info, leader_proc_mem_list[index])) {
					found = true;
					break;
				}
			}
			if (!found) {
				managers_to_be_deleted.append(mem_managerp);
			}
		}
	}

	//
	// We now have the list of process memberships to be deleted
	// on this engine. Iterate over the list and do the deletions.
	//
	SList<membership_manager_impl>::ListIterator
	    del_iter(managers_to_be_deleted);
	for (membership_manager_impl *managerp = NULL;
	    ((managerp = del_iter.get_current()) != NULL); del_iter.advance()) {
		membership::membership_manager_info info;
		managerp->fill_membership_manager_info(info);
		delete_mem_manager(info);
	}

	//
	// Now find out if the leader's list contains any process memberships
	// that this engine does not know about. Create those memberships.
	//
	for (unsigned int index = 0; index < leader_list_count; index++) {
		//
		// For each membership manager info in the leader's list,
		// check if the membership manager exists on this engine or not.
		//
		if (get_membership_manager_internal(
		    leader_proc_mem_list[index]) != NULL) {
			continue;
		} else {
			create_mem_manager(leader_proc_mem_list[index]);
		}
	}
	membership_managers_global_rwlock.unlock();
}

// Private method to create a membership manager based on the data provided
void
membership_engine_impl::create_mem_manager(
    const membership::membership_manager_info &manager_info)
{
	membership_manager_impl *mem_managerp = NULL;
	zone_cluster_membership_manager_impl *zc_mem_mgrp = NULL;

	ASSERT(membership_managers_global_rwlock.write_held());

	char mem_manager_name[MEMBERSHIP_NAME_MAX_LEN];
	int err = construct_membership_name(
	    mem_manager_name, manager_info.mem_type,
	    manager_info.cluster_id, manager_info.process_namep);
	if (err) {
		//
		// This is an internal method used by the engine.
		// Before calling this method, checks have been done
		// to make sure that the manager info is valid.
		// So we should not fail here.
		//
		ASSERT(0);
	}

	char clid_str[CLID_MAX_LEN];
	int num_digits = os::itoa((int)manager_info.cluster_id, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		ASSERT(0);
		return;	/*lint !e527 */
	}

	membership_manager_entry *entryp = membership_managers.lookup(clid_str);
	if (!entryp) {
		// No entry exists for the cluster.
		entryp = new membership_manager_entry;
		membership_managers.add(entryp, clid_str);
	}

	SList<membership_manager_impl>::ListIterator iter(entryp->manager_list);
	for (membership_manager_impl *iter_managerp = NULL;
	    (iter_managerp = iter.get_current()) != NULL; iter.advance()) {
		if (os::strcmp(iter_managerp->mem_manager_name,
		    mem_manager_name) == 0) {
			// Object already exists in list
			ASSERT(0);
			return;	/*lint !e527 */
		}
	}

	char *cluster_namep = NULL;
	if (clconf_get_cluster_name(manager_info.cluster_id, &cluster_namep)) {
		ASSERT(0);
		// lint complains that code is unreachable
		//lint -e527
		if (cluster_namep) {
			delete [] cluster_namep;
		}
		return;
		//lint +e527
	}

	//
	// Creation of membership manager also registers the object
	// in the appropriate local nameserver, and also starts
	// the state machine for the object. The constructor
	// of the object registers it with the engine.
	//
	// Creation of zone cluster membership data structures happens
	// when the zone cluster is configured. A member of the zone cluster
	// cannot change state until the zone cluster is installed.
	// Creation of process membership objects are done before
	// the processes are created, so a member cannot change state while
	// the process membership data structures are created.
	// Thus, a membership change event cannot trigger while
	// the membership is being created and before the membership manager
	// registers with the engine for events.
	//
	MEMBERSHIP_TRACE(("Membership engine : Adding manager <%s>\n",
	    mem_manager_name));
	// lint complains about unused enum constant in switch
	//lint -e788
	switch (manager_info.mem_type) {
	case (membership::BASE_CLUSTER) :
		ASSERT(manager_info.process_namep == NULL);
		mem_managerp = new base_cluster_membership_manager_impl();
		// XXX ignoring ret val of start()? how to recover else?
		(void) mem_managerp->start();
		break;

	case (membership::ZONE_CLUSTER) :
		ASSERT(manager_info.process_namep == NULL);
		mem_managerp =
		    new zone_cluster_membership_manager_impl(cluster_namep);
		// XXX ignoring ret val of start()? how to recover else?
		(void) mem_managerp->start();
		break;

	case (membership::PROCESS_UP_PROCESS_DOWN) :
		ASSERT(manager_info.process_namep != NULL);
		mem_managerp =
		    new process_up_process_down_membership_manager_impl(
		    cluster_namep, manager_info.process_namep);
		// XXX ignoring ret val of start()? how to recover else?
		(void) mem_managerp->start();
		break;

	case (membership::PROCESS_UP_ZONE_DOWN) :
		ASSERT(manager_info.process_namep != NULL);
		if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) != 0) {
			// Zone cluster related process-up-zone-down membership
			membership_manager_impl *managerp =
			    get_membership_manager_internal(
			    membership::ZONE_CLUSTER, cluster_namep, NULL);
			if (managerp == NULL) {
				//
				// The ZONE_CLUSTER membership manager
				// must be present
				//
				ASSERT(0);
			}
			// lint suggests using dynamic cast for this operation
			//lint -e1774
			zc_mem_mgrp =
			    (zone_cluster_membership_manager_impl *)managerp;
			//lint +e1774
		} else {
			zc_mem_mgrp = NULL;
		}
		mem_managerp =
		    new process_up_zone_down_membership_manager_impl(
		    cluster_namep, manager_info.process_namep, zc_mem_mgrp);
		// XXX ignoring ret val of start()? how to recover else?
		(void) mem_managerp->start();
		break;

	default :
		ASSERT(0);
		delete [] cluster_namep;	/*lint !e527 */
		return;	/*lint !e527 */
	}
	//lint +e788

	//
	// Object has been created. Now add it to the list
	// of membership managers maintained by engine.
	//
	entryp->manager_list.append(mem_managerp);
	delete [] cluster_namep;
}

// Private method to delete a membership manager based on the data provided
void
membership_engine_impl::delete_mem_manager(
    const membership::membership_manager_info &manager_info)
{
	ASSERT(membership_managers_global_rwlock.write_held());

	char mem_manager_name[MEMBERSHIP_NAME_MAX_LEN];
	int err = construct_membership_name(
	    mem_manager_name, manager_info.mem_type,
	    manager_info.cluster_id, manager_info.process_namep);
	if (err) {
		//
		// This is an internal method used by the engine.
		// Before calling this method, checks have been done
		// to make sure that the manager info is valid.
		// So we should not fail here.
		//
		ASSERT(0);
	}

	MEMBERSHIP_TRACE(("Membership engine : Leader engine says to remove "
	    "membership manager <%s>\n", mem_manager_name));

	char clid_str[CLID_MAX_LEN];
	int num_digits = os::itoa((int)manager_info.cluster_id, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		ASSERT(0);
		return;	/*lint !e527 */
	}

	membership_manager_entry *entryp = membership_managers.lookup(clid_str);
	if (!entryp) {
		ASSERT(0);
		return;	/*lint !e527 */
	}

	SList<membership_manager_impl>::ListIterator iter(entryp->manager_list);
	membership_manager_impl *mem_managerp = NULL;
	bool found = false;
	while ((mem_managerp = iter.get_current()) != NULL) {
		iter.advance();
		if (os::strcmp(mem_managerp->mem_manager_name,
		    mem_manager_name) == 0) {
			found = true;
			break;
		}
	}
	if (!found) {
		ASSERT(0);
		return;	/*lint !e527 */
	}

	// Stop the membership manager.
	if (!mem_managerp->stop()) {
		MEMBERSHIP_TRACE(("Membership engine : "
		    "Could not stop object <%s>\n", mem_manager_name));
		// Still proceed to clean up the engine data structures
	}

	// Clean up engine data structures related to the membership manager
	if (!entryp->manager_list.erase(mem_managerp)) {
		// Should not happen
		CL_PANIC(0);
	}

	MEMBERSHIP_TRACE(("Membership engine : Removed object <%s>\n",
	    mem_manager_name));

	//
	// If the object list is empty,
	// delete the entry from the hashtable.
	//

	if (!entryp->manager_list.empty()) {
		return;
	}

	membership_manager_entry *rem_entryp =
	    membership_managers.remove(clid_str);

	ASSERT(entryp == rem_entryp);
	delete rem_entryp;
	entryp = rem_entryp = NULL;
}

//
// Constructor
//
membership_engine_impl::membership_engine_impl()
{
	engine_reconfig_underway = false;

	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		engine_refs[nid] = membership::engine::_nil();
	}

	ccr_change_thread_data.task_to_do = false;
}

//
// The engine needs to read zone cluster configuration data from CCR.
// It uses clconf interfaces to read the CCR. The membership engine
// initializes before the CCR is sync'ed among the base nodes live in
// the cluster. The engine does not read zone cluster configuration data
// during its initialization to avoid reading possibly stale CCR data.
// Once CCR is sync'ed, the engine receives a callback here,
// and then it can read the CCR.
//
void
ccr_is_ready_callback_for_membership()
{
	is_ccr_ready = true;
}

//
// Idempotent initialization method for membership engine
//
// static
void
membership_engine_impl::initialize()
{
	if (the_membership_engine_implp != NULL) {
		return;
	}

	the_membership_engine_implp = new membership_engine_impl();

	//
	// The membership engine object is essential, and is created
	// during CMM initialization. So it is unlikely
	// that we would have insufficient memory.
	//
	if (the_membership_engine_implp == NULL) {

		MEMBERSHIP_TRACE(("Membership engine : "
		    "Could not create engine object.\n"));
		//
		// SCMSGS
		// @explanation
		// Could not create the membership engine object.
		// This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on
		// the node. You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership: Could not create "
		    "membership engine object.");
	}
	if (!cmm_impl::cmm_create_thread(
	    membership_engine_impl::start_ccr_change_thread,
	    NULL, CMM_MAXPRI - 1)) {
		MEMBERSHIP_TRACE(("Membership engine : unable to create "
		    "CCR callback thread for membership engine.\n"));
		//
		// SCMSGS
		// @explanation
		// A thread that supports dynamic addition/removal
		// of zone clusters to the membership subsystem
		// could not be created.
		// Membership for dynamically added or removed zone clusters
		// would not work properly.
		// This might be due to lack of memory.
		// @user_action
		// Lack of memory might lead to other problems on the node.
		// You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Membership: Unable to create CCR callback thread "
		    "for membership subsystem. Zone cluster membership "
		    "support would not be available fully");
	}

	if (!cmm_impl::cmm_create_thread(
	    membership_engine_impl::start_automaton_change_thread,
	    NULL, CMM_MAXPRI - 1)) {
		MEMBERSHIP_TRACE(("Membership engine : unable to create "
		    "automaton callback thread for membership engine.\n"));
		//
		// SCMSGS
		// @explanation
		// A thread that is part of the membership subsystem
		// could not be created.
		// This might be due to lack of memory.
		// Membership subsystem will not work properly.
		// @user_action
		// Lack of memory might lead to other problems on
		// the node. You must free up memory on the node.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
		    "Membership: Unable to create automaton callback thread "
		    "for membership subsystem.");
	}
}

//
// Destructor
//
// lint complains that functions that might throw exceptions should not be
// invoked in destructors, as destructors are not supposed to throw exceptions.
//lint -e1551
membership_engine_impl::~membership_engine_impl()
{
	// XXX
	// XXX Find out if hashtable and SList have methods
	// XXX to empty out the table/list at one go.
	// XXX Think if we can use them here.
	// XXX

	// XXX Should we shutdown individual membership managers?

	// Shutdown the CCR change thread
	ccr_change_lock.lock();
	ccr_change_thread_data.event = CCR_CHANGE_THREAD_EXIT;
	ccr_change_cv.signal();
	ccr_change_lock.unlock();

	// Shutdown the automaton callback thread
	automaton_change_lock.lock();
	autm_event = AUTOMATON_CALLBACK_THREAD_EXIT;
	automaton_change_cv.signal();
	automaton_change_lock.unlock();
}
//lint +e1551

// static
void
membership_engine_impl::shutdown()
{
	delete the_membership_engine_implp;
	the_membership_engine_implp = NULL;
}

void
#ifdef DEBUG
membership_engine_impl::_unreferenced(unref_t cookie)
#else
membership_engine_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Method used to deliver an event to the engine on local base node.
// The events could be creation/deletion of membership manager
// or a membership change trigger.
//
void
membership_engine_impl::deliver_event_to_engine(
    membership::engine_event event_triggered,
    const membership::engine_event_info &event_info, Environment &e)
{
	int err = 0;
	bool action_completed = false;
	Environment env;
	CORBA::Exception *exp = NULL;
	char mem_manager_name[MEMBERSHIP_NAME_MAX_LEN];
	membership::engine_var leader_engine_v = membership::engine::_nil();

	while (engine_reconfig_underway) {
		os::usecsleep((os::usec_t)1000);	// 1ms
	}

	switch (event_triggered) {
	case membership::CREATE_MEMBERSHIP :
		MEMBERSHIP_TRACE((
		    "Membership engine : engine received CREATE_MEMBERSHIP\n"));
		membership_managers_global_rwlock.rdlock();
		if (get_membership_manager_internal(
		    event_info.mem_manager_info) != NULL) {
			// Membership is already being maintained
			membership_managers_global_rwlock.unlock();
			return;
		}
		membership_managers_global_rwlock.unlock();

		err = construct_membership_name(mem_manager_name,
		    event_info.mem_manager_info.mem_type,
		    event_info.mem_manager_info.cluster_id,
		    event_info.mem_manager_info.process_namep);
		if (err) {
			// We should not fail here.
			ASSERT(0);
		}

		leader_engine_v = cmm_ns::get_membership_engine_ref(leader_cmm);
		if (CORBA::is_nil(leader_engine_v)) {
			//
			// Must not happen, as engine is created
			// during CMM initialization.
			//
			CL_PANIC(0);
		}

		leader_engine_v->request_to_leader(
		    event_triggered, event_info.mem_manager_info, env);
		if ((exp = env.exception()) != NULL) {
			if (membership::retry_create_or_delete::_exnarrow(
			    exp)) {
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "retry exception while requesting leader "
				    "to create membership <%s>\n",
				    mem_manager_name));
				env.clear();
				// Return retry exception
				e.exception(
				    new membership::retry_create_or_delete());
				return;
			} else if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE "
				    "while requesting leader to create "
				    "membership <%s>\n", mem_manager_name));
				env.clear();
				// Return retry exception
				e.exception(
				    new membership::retry_create_or_delete());
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE((
				    "Membership Engine : unexpected exception "
				    "%s while requesting leader to create "
				    "membership <%s>\n", exp->_name(),
				    mem_manager_name));
				ASSERT(0);
				//lint -e527
				env.clear();
				return;
				//lint +e527
			}
		}

		//
		// No exception happened; check to ensure that the membership
		// data structures were created on local membership engine.
		//
		action_completed = false;
		while (!action_completed) {
			membership_managers_global_rwlock.rdlock();
			if (get_membership_manager_internal(
			    event_info.mem_manager_info) != NULL) {
				// Data structures created on local engine
				action_completed = true;
			}
			membership_managers_global_rwlock.unlock();
			if (!action_completed) {
				// Sleep for some time, and then retry
				MEMBERSHIP_TRACE(("Membership engine : "
				    "sleeping as creation incomplete\n"));
				os::usecsleep((os::usec_t)1000);	// 1ms
				MEMBERSHIP_TRACE(("Membership engine : "
				    "woke up for creation check retry\n"));
			}
		}
		// Now we are sure that the creation has completed

		break;

	case membership::DELETE_MEMBERSHIP :
		MEMBERSHIP_TRACE((
		    "Membership engine : engine received DELETE_MEMBERSHIP\n"));
		membership_managers_global_rwlock.rdlock();
		if (get_membership_manager_internal(
		    event_info.mem_manager_info) == NULL) {
			//
			// Membership is not being maintained
			// or is already deleted
			//
			MEMBERSHIP_TRACE((
			    "XXX engine: manager already deleted\n"));
			membership_managers_global_rwlock.unlock();
			return;
		}
		membership_managers_global_rwlock.unlock();

		err = construct_membership_name(mem_manager_name,
		    event_info.mem_manager_info.mem_type,
		    event_info.mem_manager_info.cluster_id,
		    event_info.mem_manager_info.process_namep);
		if (err) {
			// We should not fail here.
			ASSERT(0);
		}

		leader_engine_v = cmm_ns::get_membership_engine_ref(leader_cmm);
		if (CORBA::is_nil(leader_engine_v)) {
			//
			// Must not happen, as engine is created
			// during CMM initialization.
			//
			CL_PANIC(0);
		}

		leader_engine_v->request_to_leader(
		    event_triggered, event_info.mem_manager_info, env);
		if ((exp = env.exception()) != NULL) {
			if (membership::retry_create_or_delete::_exnarrow(
			    exp)) {
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "retry exception while requesting leader "
				    "to delete membership <%s>\n",
				    mem_manager_name));
				env.clear();
				// Return retry exception
				e.exception(
				    new membership::retry_create_or_delete());
				return;
			} else if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE "
				    "while requesting leader to delete "
				    "membership <%s>\n", mem_manager_name));
				env.clear();
				// Return retry exception
				e.exception(
				    new membership::retry_create_or_delete());
				return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE((
				    "Membership Engine : unexpected exception "
				    "%s while requesting leader to delete "
				    "membership <%s>\n", exp->_name(),
				    mem_manager_name));
				ASSERT(0);
				//lint -e527
				env.clear();
				return;
				//lint +e527
			}
		}

		//
		// No exception happened; check to ensure that the membership
		// data structures were deleted on local membership engine.
		//
		action_completed = false;
		while (!action_completed) {
			membership_managers_global_rwlock.rdlock();
			if (get_membership_manager_internal(
			    event_info.mem_manager_info) == NULL) {
				// Data structures deleted on local engine
				action_completed = true;
			}
			membership_managers_global_rwlock.unlock();
			if (!action_completed) {
				// Sleep for some time, and then retry
				MEMBERSHIP_TRACE(("Membership engine : "
				    "sleeping as deletion incomplete\n"));
				os::usecsleep((os::usec_t)1000);	// 1ms
				MEMBERSHIP_TRACE(("Membership engine : "
				    "woke up for deletion check retry\n"));
			}
		}
		// Now we are sure that the deletion has completed

		break;

	case membership::DELIVER_MEMBERSHIP_CHANGE_EVENT :

		if ((event_info.event == membership::NODE_UP) &&
		    (event_info.mem_manager_info.cluster_id !=
		    BASE_CLUSTER_ID)) {

			//
			// This is a zone 'up' notification for
			// a zone belonging to a zone cluster.
			// Store zone id and zone name in to
			// the mapping of running zone clusters
			//

			// Get the zone cluster name
			char *cluster_namep = NULL;
			if (clconf_get_cluster_name(
			    event_info.mem_manager_info.cluster_id,
			    &cluster_namep)) {
				//
				// clconf must have the cluster name for this
				// cluster ID.
				//
				MEMBERSHIP_TRACE(("Membership engine : "
				    "unable to get cluster name for "
				    "cluster ID %d\n",
				    event_info.mem_manager_info.cluster_id));
				//
				// SCMSGS
				// @explanation
				// Membership subsystem was unable to get
				// the cluster name for a cluster ID.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or
				// patch is available.
				//
				(void) cmm_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "Membership: Unable to get cluster name "
				    "for cluster ID %d.\n",
				    event_info.mem_manager_info.cluster_id);

				return;
			}

			// Get the zone id from the zone cluster name
			zone_t *zone_refp = zone_find_by_name(cluster_namep);
			if (zone_refp == NULL) {
				//
				// We must be able to get a reference to
				// the zone structure from Solaris.
				// The zone is not yet down.
				//
				MEMBERSHIP_TRACE(("Membership engine : "
				    "unable to get zone reference from Solaris "
				    "for running zone %s\n", cluster_namep));
				//
				// SCMSGS
				// @explanation
				// Membership subsystem was unable to get
				// the zone reference structure from Solaris
				// for a zone that is running.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or
				// patch is available.
				//
				(void) cmm_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "Membership: Unable to get zone reference "
				    "from Solaris for running zone %s.",
				    cluster_namep);

				delete [] cluster_namep;
				return;
			}

			if (zone_status_get(zone_refp) != ZONE_IS_RUNNING) {
				//
				// The zone had come up at the time when
				// this event was generated for the engine.
				// At this point in time, the zone may have
				// failed. We have not yet finished processing
				// the 'coming up' event. We will process
				// this 'coming up' event, and will assume that
				// we will get a zone death event afterwards.
				//
				MEMBERSHIP_TRACE(("Membership engine : "
				    "Got a 'up' notification for zone %s "
				    "that is not running\n", cluster_namep));
			}

			// Add the zone to the mapping of running zone clusters.
			running_zone_clusters_lock.lock();
			running_zone_clusters.add(
			    cluster_namep, zone_refp->zone_id);
			MEMBERSHIP_TRACE(("Membership engine : Added %s to "
			    "running_zone_clusters\n", cluster_namep));
			running_zone_clusters_lock.unlock();

			zone_rele(zone_refp);
		}

		//
		// If we can grab the read lock, then we know no creations
		// or deletions of membership managers are in progress.
		// So, we can safely inform the membership managers
		// that have registered for this event.
		//
		membership_managers_global_rwlock.rdlock();
		dispatch_events_to_managers(event_info);
		membership_managers_global_rwlock.unlock();
		break;

	default :
		ASSERT(0);
		break;	/*lint !e527 */
	}
}

void
membership_engine_impl::dispatch_events_to_managers(
    const membership::engine_event_info &event_info)
{
	char event_name[MEMBERSHIP_MANAGER_EVENT_NAME_MAX_LEN];

	int err = construct_mem_manager_event_name(event_name, event_info.event,
	    event_info.mem_manager_info.cluster_id,
	    event_info.mem_manager_info.process_namep);
	if (err == ENOENT) {
		// Cluster must exist with a valid cluster ID
		ASSERT(0);
	} else if (err == EINVAL) {
		// Should not happen
		ASSERT(0);
	} else if (err != 0) {
		// Cannot happen
		ASSERT(0);
	}

	MEMBERSHIP_TRACE((
	    "Membership engine : received event <%s>\n", event_name));

	latest_node_seqnum_lock.lock();
	cmm::seqnum_t node_seqnum = latest_node_seqnum;
	latest_node_seqnum_lock.unlock();

	event_to_manager_mapping_entry *entryp =
	    event_to_manager_mapping.lookup(event_name);
	if (!entryp) {
		return;
	}
	entryp->entry_lock.lock();

	SList<membership_manager_impl>::ListIterator
	    iter(entryp->manager_list);

	//
	// Engine delivers an event to all membership managers registered
	// for the event. The membership managers schedule their own
	// reconfiguration tasks. So, the reconfigurations of various
	// membership managers happen asynchronously and in parallel.
	//
	// However, owing to fencing considerations, there is a special case
	// for PROCESS_UP_ZONE_DOWN membership managers.
	// A PROCESS_UP_ZONE_DOWN membership manager should reconfigure only
	// after the ZONE_CLUSTER membership manager for its zone cluster
	// finishes putting fence locks in nameserver.
	//
	// To address this special case, the engine does this on receiving
	// an event :
	// (1) It iterates through the list of membership managers registered
	// for the event, and delivers the event to a manager on the list
	// if its not of type PROCESS_UP_ZONE_DOWN.
	// (2) Then the engine iterates through the same membership managers
	// list registered for the event, and delivers the event to
	// the PROCESS_UP_ZONE_DOWN membership managers on the list.
	//
	// As a result of this action, if ZONE_CLUSTER and PROCESS_UP_ZONE_DOWN
	// membership managers are registered for the same event,
	// then the ZONE_CLUSTER membership manager knows about the event
	// before the PROCESS_UP_ZONE_DOWN membership managers.
	//
	// Note that PROCESS_UP_ZONE_DOWN memberships related to zone clusters
	// only need this special case behaviour.
	// The base cluster's PROCESS_UP_ZONE_DOWN memberships do not need
	// this behaviour. This is because the action of putting fence locks
	// during a base cluster reconfiguration is done by the kernel CMM,
	// which reconfigures before the membership engines become aware of
	// a base cluster CMM reconfiguration. So, the BASE_CLUSTER membership
	// manager does not need to put fence locks for base cluster membership,
	// before the PROCESS_UP_ZONE_DOWN membership managers related to
	// the base cluster do their reconfiguration.
	// However, the engine can follow the same protocol of dispatching
	// events to non-PROCESS_UP_ZONE_DOWN membership managers first.
	// In that case, its just that PROCESS_UP_ZONE_DOWN membership manager
	// related to a base cluster will receive the event after
	// the BASE_CLUSTER membership manager knows about the event; but
	// that does not cause any harm.
	//

	//
	// First iteration to dispatch events to non-PROCESS_UP_ZONE_DOWN
	// membership managers
	//
	for (membership_manager_impl *iter_managerp = NULL;
	    (iter_managerp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (iter_managerp->type != membership::PROCESS_UP_ZONE_DOWN) {
			iter_managerp->local_membership_change_event(
			    event_name, node_seqnum);
		}
	}

	//
	// Second iteration to dispatch events to PROCESS_UP_ZONE_DOWN
	// membership managers
	//
	iter.reinit(entryp->manager_list);
	for (membership_manager_impl *iter_managerp = NULL;
	    (iter_managerp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (iter_managerp->type == membership::PROCESS_UP_ZONE_DOWN) {
			iter_managerp->local_membership_change_event(
			    event_name, node_seqnum);
		}
	}

	entryp->entry_lock.unlock();
}

//
// After kernel CMM reconfigures, the automaton calls back
// into the membership subsystem, providing the latest
// base cluster membership and seqnum.
//
// XXX Notice that the leader node could get the automaton callback
// XXX before other engines, and hence could start talking to
// XXX non-leader engines before non-leader engines have
// XXX updated their latest_node* data etc in this method.
// XXX Will it be a problem?
void
membership_engine_impl::automaton_callback(
    const cmm::membership_t &new_membership, cmm::seqnum_t new_seqnum)
{
	nodeid_t nid;

	automaton_change_lock.lock();

	if (autm_event == AUTOMATON_CALLBACK_THREAD_EXIT) {
		//
		// Membership subsystem is shutting down.
		// No more reconfigurations would be done. Return.
		//
		automaton_change_lock.unlock();
		return;
	}

	autm_event = AUTOMATON_MEMBERSHIP_CHANGE;

	// Update the latest base cluster seqnum
	latest_node_seqnum_lock.lock();
	ASSERT(latest_node_seqnum <= new_seqnum);
	if (latest_node_seqnum == new_seqnum) {
		latest_node_seqnum_lock.unlock();
		automaton_change_lock.unlock();
		return;
	}
	latest_node_seqnum = new_seqnum;
	latest_node_membership = new_membership;
	latest_node_seqnum_lock.unlock();

	engine_reconfig_underway = true;

	for (nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_membership.members[nid] != INCN_UNKNOWN) {
			break;
		}
	}
	leader_cmm = nid;

	if (nid == orb_conf::local_nodeid()) {
		// I am leader. Signal the automaton callback thread
		automaton_change_cv.signal();
		automaton_change_lock.unlock();
	} else {
		automaton_change_lock.unlock();
	}
}

//
// Release any references for peer engines that are held.
// Get a reference for each peer engine.
// Returns true on success, false on failure.
//
bool
membership_engine_impl::update_peer_refs(
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership)
{
	// Release any old peer references held
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (!CORBA::is_nil(engine_refs[nid])) {
			CORBA::release(engine_refs[nid]);
			engine_refs[nid] = membership::engine::_nil();
		}
	}

	// Wait for root nameserver to initialize
	while (!ns::is_root_ns_initialized()) {
		if (new_base_cluster_reconfig_started(node_seqnum)) {
			return (false);
		}
		os::usecsleep((os::usec_t)1000);	// 1 ms
	}

	// Get references to all peer engines
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_base_cluster_reconfig_started(node_seqnum)) {
			return (false);
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (node_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		engine_refs[nid] = cmm_ns::get_membership_engine_ref(nid);
		while (CORBA::is_nil(engine_refs[nid])) {
			//
			// Wait until we obtain a reference.
			// The nameserver initialization might be happening,
			// or maybe the peer membership engines have not
			// yet finished initialization.
			//
			if (new_base_cluster_reconfig_started(node_seqnum)) {
				return (false);
			}
			os::usecsleep((os::usec_t)1000);	// 1 ms
			engine_refs[nid] =
			    cmm_ns::get_membership_engine_ref(nid);
		}
	}

	return (true);
}

//
// The leader engine drives the engine reconfiguration using this method
//
// lint complains that a local stack variable is not accessed, although
// the variable is used.
//lint -e550
void
membership_engine_impl::leader_drives_engine_reconfig(
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership)
{
	//
	// Here is what the "leader" engine does :
	//
	// (1) Update references to the peer engines
	//
	// (2) Query clconf for a list of zone clusters configured.
	// Build a sequence of zone cluster membership info structs,
	// consisting of the zone clusters and the base cluster info.
	// Send this list to every engine. Each engine follows logic
	// to make sure that its list of cluster memberships
	// agrees with the leader's list. For details of the logic,
	// see comments at the subroutine for the step.
	//
	// (3) Ask each peer engine for a list of process memberships
	// that the latter knows about. Collect all this queried data,
	// and build a master list of process memberships to be maintained.
	// Send this master list of process memberships to every peer engine.
	// Each engine follows logic to make sure that its list of process
	// memberships agrees with the leader's list. For details of the logic,
	// see comments at the subroutine for the step.
	//
	// At this point, all engines agree on the same list of
	// memberships to be maintained.
	//
	// (4) In the finish engine reconfiguration step, each engine updates
	// the latest incarnation of the "base cluster" membership manager
	// on its node with the incarnation as delivered by automaton.
	// This is done because when a membership change event triggers
	// for which a membership manager is registered, the membership manager
	// receives the event, and updates its local latest information.
	// However, for "base cluster" membership manager, the update
	// has to happen after kernel CMM reconfigures. So, engine does this
	// special case for "base cluster" membership managers.
	//
	// (5) Then the leader engine delivers a "base cluster" membership
	// change event to all membership managers on the leader node.
	// This is because every membership registers for "base cluster" event.
	// As a result, all the membership managers start their
	// "leader-data-collection" step and possible reconfiguration.
	//

	ASSERT(I_AM_LEADER);
	MEMBERSHIP_TRACE((
	    "Membership engine : Leader engine drive reconfig starts\n"));

	// Step 1 : update references held to the peer engines
	if (!update_peer_refs(node_seqnum, node_membership)) {
		return;
	}

	// Step 2 : engine agreement on list of cluster memberships
	if (!engine_agreement_on_cl_mem_list(node_seqnum, node_membership)) {
		return;
	}

	// Step 3 : engine agreement on list of process memberships
	if (!engine_agreement_on_proc_mem_list(node_seqnum, node_membership)) {
		return;
	}

	// Step 4 : ask each peer engine to do the finish reconfig step
	if (!leader_drive_finish_reconfig_step(node_seqnum, node_membership)) {
		return;
	}

	//
	// Step 5 :
	// Only on "leader" engine, deliver base cluster membership change event
	// to all membership managers
	//
	uint_t clid = 0;
	if (clconf_get_cluster_id((char *)DEFAULT_CLUSTER, &clid)) {
		// Must be able to convert global cluster name to cluster ID
		ASSERT(0);
	}
	membership::engine_event_info event_info;
	event_info.mem_manager_info.mem_type = membership::BASE_CLUSTER;
	event_info.mem_manager_info.cluster_id = clid;
	event_info.mem_manager_info.process_namep = (char *)NULL;
	event_info.event = membership::BASE_CLUSTER_MEMBERSHIP_CHANGE;
	Environment env;
	CORBA::Exception *exp = NULL;
	engine_refs[orb_conf::local_nodeid()]->deliver_event_to_engine(
	    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT, event_info, env);
	if ((exp = env.exception()) != NULL) {
		//
		// We are calling the method on ourselves.
		// And this is not a create/delete membership event delivery.
		// So do not expect any exception to happen.
		//
		ASSERT(0);
	}

	MEMBERSHIP_TRACE((
	    "Membership engine : Leader engine drive reconfig ends\n"));
}
//lint +e550

//
// Step 2 :
// Query clconf for a list of zone clusters configured.
// This list is the "truth copy" of the list of
// zone clusters configured.
// Send the list to every peer engine;
// Each engine does this :
// (i) if the engine has membership data structures for a zone cluster
// which is not present in the list from leader engine,
// then the engine deletes that membership.
// (ii) if leader's list contains any zone cluster membership that
// the engine does not know of, it creates membership data structures
// for that zone cluster membership.
//
// As a result of this step, all engines agree on the list of
// zone cluster membership maintained. Further, there is another
// intended side effect of this step. Process memberships are
// deleted when the related zone cluster goes away, which means
// when the related zone cluster membership is deleted.
// Since in this step, each engine deletes any zone cluster memberships
// that it knows about but the leader engine does not know about,
// so after this step, there are no process membership data structures
// left on various engines that were not supposed to be deleted.
// In other words, if we consider the set of process memberships
// on each engine, the union of such sets is the truth copy
// of the set of process memberships that need to be maintained.
// Returns true on success, false on failure.
//
bool
membership_engine_impl::engine_agreement_on_cl_mem_list(
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership) const
{
	unsigned int index = 0;
	bool retry = false;

	ASSERT(I_AM_LEADER);
	MEMBERSHIP_TRACE((
	    "Membership engine : engine reconfig, leader at step 2\n"));

	// If CCR is ready, read CCR; else wait
	while (!is_ccr_ready) {
		os::usecsleep((os::usec_t)1000);	// 1ms
	}
	ASSERT(is_ccr_ready);

	//
	// We want to a build a sequence of membership_manager_info structures
	// that we can send to every engine. The sequence will have info for
	// all the zone clusters and the base cluster.
	//
	unsigned int zone_cluster_count = clconf_cluster_get_num_clusters();
	MEMBERSHIP_TRACE(("Membership engine : zone cluster count = %d\n",
	    zone_cluster_count));

	// One (for the base cluster) more than the num of zone clusters
	unsigned int seq_length = zone_cluster_count + 1;
	membership::membership_manager_info_seq leader_cl_list(
	    seq_length, seq_length);

	char **zone_clustersp = clconf_cluster_get_cluster_names();
	for (index = 0; index < zone_cluster_count; index++) {
		leader_cl_list[index].mem_type = membership::ZONE_CLUSTER;
		uint_t clid = 0;
		if (clconf_get_cluster_id(zone_clustersp[index], &clid)) {
			// Must be able to convert cluster name to cluster ID
			ASSERT(0);
		}
		leader_cl_list[index].cluster_id = clid;
		leader_cl_list[index].process_namep = (char *)NULL;
		delete [] (zone_clustersp[index]);
	}
	delete [] zone_clustersp;

	//
	// "base cluster" is not part of the zone cluster list that clconf
	// API provides. So, add "base cluster" to the master list directly.
	//
	ASSERT(index == zone_cluster_count);
	uint_t clid = 0;
	if (clconf_get_cluster_id((char *)DEFAULT_CLUSTER, &clid)) {
		// Must be able to convert global cluster name to cluster ID
		ASSERT(0);
	}
	leader_cl_list[index].mem_type = membership::BASE_CLUSTER;
	leader_cl_list[index].cluster_id = clid;
	leader_cl_list[index].process_namep = (char *)NULL;

	// Send the sequence to each peer engine
	do {
		retry = false;
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (new_base_cluster_reconfig_started(node_seqnum)) {
				return (false);
			}

			if (!orb_conf::node_configured(nid)) {
				continue;
			}

			if (node_membership.members[nid] == INCN_UNKNOWN) {
				continue;
			}

			ASSERT(!CORBA::is_nil(engine_refs[nid]));
			Environment e;
			CORBA::Exception *exp = NULL;
			engine_refs[nid]->leader_delivers_cl_mem_list(
			    leader_cl_list, e);
			if ((exp = e.exception()) == NULL) {
				continue;
			}

			if (membership::ccr_not_refreshed::_exnarrow(exp)) {
				//
				// The CCR on the target node is not yet
				// refreshed. So retry after some wait.
				//
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "ccr_not_refreshed exception "
				    "while delivering cluster membership list "
				    "to peer engine, node %d\n", nid));
				e.clear();
				retry = true;
				break;

			} else if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE "
				    "while delivering cluster membership list "
				    "to peer engine, node %d\n", nid));
				e.clear();
				return (false);
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "unexpected exception %s while delivering "
				    "cluster membership list to peer engine, "
				    "node %d\n", exp->_name(), nid));
				ASSERT(0);
				//lint -e527
				e.clear();
				return (false);
				//lint +e527
			}
		}

		if (retry) {
			os::usecsleep((long)1000);	// 1ms
		} else {
			break;
		}
	} while (retry);

	return (true);
}

//
// Step 3 :
// The leader engine queries each engine about the process memberships
// the latter knows about. The leader engine builds a "truth copy" of
// process memberships to be maintained, which consists of all
// the collected memberships. Then the leader engine sends this list
// to each engine. Each engine checks to see if the leader's list contains
// any process membership that it itself does not know of; if so, the engine
// creates membership data structures for that process membership.
// Returns true on success, false on failure
//
// This method uses 'goto' for common return actions
// (where it frees memory as well).
// lint complains that use of 'goto' is deprecated.
//lint -e801
bool
membership_engine_impl::engine_agreement_on_proc_mem_list(
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership)
{
	int err = 0;
	nodeid_t nid = 0;
	bool retval = true;
	unsigned int index = 0;
	unsigned int count = 0;
	Environment e;
	CORBA::Exception *exp = NULL;
	char mem_manager_name[MEMBERSHIP_NAME_MAX_LEN];
	membership::membership_manager_info_seq *leader_proc_listp = NULL;
	membership::membership_manager_info *infop = NULL;
	bool retry = false;

	ASSERT(I_AM_LEADER);
	MEMBERSHIP_TRACE((
	    "Membership engine : engine reconfig, leader at step 3\n"));

	// Data structure to accumulate the master list of membership managers
	string_hashtable_t<membership::membership_manager_info *>
	    collected_data;
	string_hashtable_t<membership::membership_manager_info *>::iterator
	    iter(collected_data);

	// Query each peer about the process membership managers it knows about
	for (nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_base_cluster_reconfig_started(node_seqnum)) {
			retval = false;
			goto free_mem_and_return;
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (node_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		ASSERT(!CORBA::is_nil(engine_refs[nid]));

		membership::membership_manager_info_seq *data_seqp = NULL;
		engine_refs[nid]->query_list_of_proc_memberships(data_seqp, e);
		if ((exp = e.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE "
				    "while querying process membership list "
				    "from peer engine, node %d\n", nid));
				e.clear();
				retval = false;
				goto free_mem_and_return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "unexpected exception %s while querying "
				    "process membership list from peer engine, "
				    "node %d\n", exp->_name(), nid));
				ASSERT(0);
				//lint -e527
				e.clear();
				retval = false;
				goto free_mem_and_return;
				//lint +e527
			}
		}

		// Populate master list with the queried data
		for (index = 0; index < data_seqp->length(); index++) {
			membership::membership_manager_info &src_data =
			    (*data_seqp)[index];

			// XXX Testing
			//
			// At this stage, the engines have completed
			// the cluster list agreement step and so all engines
			// agree about the cluster memberships they know about.
			// So, if the peer knows about a process membership that
			// pertains to a cluster which is not present in
			// the list of cluster memberships on this engine,
			// it means the cluster has been deleted
			// and hence no process membership for that cluster
			// must remain. So, do not add such a process
			// membership to the master list.
			//
			membership::membership_manager_info cl_mem_mgr_info;
			if (src_data.cluster_id ==  BASE_CLUSTER_ID) {
				cl_mem_mgr_info.mem_type =
				    membership::BASE_CLUSTER;
			} else {
				cl_mem_mgr_info.mem_type =
				    membership::ZONE_CLUSTER;
			}
			cl_mem_mgr_info.cluster_id = src_data.cluster_id;
			cl_mem_mgr_info.process_namep = (const char *)NULL;
			membership_managers_global_rwlock.rdlock();
			if (get_membership_manager_internal(
			    cl_mem_mgr_info) == NULL) {
				membership_managers_global_rwlock.unlock();
				continue;
			}
			membership_managers_global_rwlock.unlock();
			// XXX Testing

			err = construct_membership_name(
			    mem_manager_name, src_data.mem_type,
			    src_data.cluster_id, src_data.process_namep);
			if (err) {
				//
				// We are querying process memberships that
				// exist on various engines, and that means
				// the manager info for the various process
				// memberships queried must be valid.
				// So we should not fail here.
				//
				ASSERT(0);
			}

			if (collected_data.lookup(mem_manager_name) == NULL) {
				infop = new membership::membership_manager_info;
				ASSERT(infop);
				infop->mem_type = src_data.mem_type;
				infop->cluster_id = src_data.cluster_id;
				ASSERT((char *)(src_data.process_namep));
				infop->process_namep = os::strdup(
				    (char *)(src_data.process_namep));
				ASSERT((char *)(infop->process_namep));
				collected_data.add(infop, mem_manager_name);
			}
		}
		delete data_seqp;
	}

	//
	// We have the master list of memberships to be maintained.
	// Construct a sequence out of this master list.
	// The sequence has to be sent to all the peer engines.
	//
	count = collected_data.count();
	if (count == 0) {
		return (true);
	}

	leader_proc_listp =
	    new membership::membership_manager_info_seq(count, count);
	ASSERT(leader_proc_listp);
	for (index = 0; index < count; index++) {
		membership::membership_manager_info &mgr_info =
		    (*leader_proc_listp)[index];
		mgr_info.process_namep = (char *)NULL;
	}
	iter.reinit(&collected_data);

	//
	// lint thinks that re-assignment to 'infop' would cause a memory leak.
	// But 'infop' is previously used just as a temp variable to allocate
	// memory and then create the 'collected_data' hashtable.
	// We finally free the 'collected_data' data structure.
	// So we ignore the lint warning here.
	//lint -e423
	for (infop = NULL, index = 0; ((infop = iter.get_current()) != NULL);
	    iter.advance(), index++) {
		MEMBERSHIP_TRACE(("XXX proc mem mgr info : %d, %d, %s\n",
		    infop->mem_type, infop->cluster_id,
		    (char *)(infop->process_namep)));
		membership::membership_manager_info &mgr_info =
		    (*leader_proc_listp)[index];
		mgr_info.mem_type = infop->mem_type;
		mgr_info.cluster_id = infop->cluster_id;
		mgr_info.process_namep =
		    os::strdup((char *)infop->process_namep);
	}
	//lint +e423
	ASSERT(index == count);

	// Send the sequence to each peer engine
	do {
		retry = false;
		for (nid = 1; nid <= NODEID_MAX; nid++) {
			if (new_base_cluster_reconfig_started(node_seqnum)) {
				retval = false;
				goto free_mem_and_return;
			}

			if (!orb_conf::node_configured(nid)) {
				continue;
			}

			if (node_membership.members[nid] == INCN_UNKNOWN) {
				continue;
			}

			ASSERT(!CORBA::is_nil(engine_refs[nid]));
			engine_refs[nid]->leader_delivers_proc_mem_list(
			    (*leader_proc_listp), e);
			if ((exp = e.exception()) == NULL) {
				continue;
			}

			if (membership::ccr_not_refreshed::_exnarrow(exp)) {
				//
				// The CCR on the target node is not yet
				// refreshed. So retry after some wait.
				//
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "ccr_not_refreshed exception "
				    "while delivering process membership list "
				    "to peer engine, node %d\n", nid));
				e.clear();
				retry = true;
				break;

			} else if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE "
				    "while delivering process membership list "
				    "to peer engine, node %d\n", nid));
				e.clear();
				retval = false;
				goto free_mem_and_return;
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "unexpected exception %s while delivering "
				    "process membership list to peer engine, "
				    "node %d\n", exp->_name(), nid));
				ASSERT(0);
				//lint -e527
				e.clear();
				retval = false;
				goto free_mem_and_return;
				//lint +e527
			}
		}

		if (retry) {
			os::usecsleep((long)1000);	// 1ms
		} else {
			break;
		}
	} while (retry);

free_mem_and_return:
	// The master list is no longer needed. Free the memory.
	iter.reinit(&collected_data);
	while ((infop = iter.get_current()) != NULL) {
		iter.advance();
		delete infop;
	}
	collected_data.dispose();
	ASSERT(collected_data.empty());

	delete leader_proc_listp;
	return (retval);
}
//lint +e801

//
// Leader engine asks every peer engine to execute the finish reconfig step.
// Returns true on success, false on failure.
//
bool
membership_engine_impl::leader_drive_finish_reconfig_step(
    cmm::seqnum_t node_seqnum, const cmm::membership_t &node_membership) const
{
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (new_base_cluster_reconfig_started(node_seqnum)) {
			return (false);
		}

		if (!orb_conf::node_configured(nid)) {
			continue;
		}

		if (node_membership.members[nid] == INCN_UNKNOWN) {
			continue;
		}

		ASSERT(!CORBA::is_nil(engine_refs[nid]));
		Environment e;
		CORBA::Exception *exp = NULL;
		engine_refs[nid]->finish_engine_reconfiguration(
		    node_seqnum, node_membership, e);
		if ((exp = e.exception()) != NULL) {
			if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
				MEMBERSHIP_TRACE((
				    "Membership Engine : COMM_FAILURE while "
				    "calling finish_engine_reconfiguration() "
				    "on peer engine, node %d\n", nid));
				e.clear();
				return (false);
			} else {
				// Some other exception
				MEMBERSHIP_TRACE(("Membership Engine : "
				    "unexpected exception %s while calling "
				    "finish_engine_reconfiguration() "
				    "on peer engine, node %d\n",
				    exp->_name(), nid));
				ASSERT(0);
				//lint -e527
				e.clear();
				return (false);
				//lint +e527
			}
		}
	}
	return (true);
}

//
// During engine reconfiguration, the "leader" engine queries each engine
// for a list of process membership managers that the latter knows of.
//
void
membership_engine_impl::query_list_of_proc_memberships(
    membership::membership_manager_info_seq_out proc_mem_seq_out, Environment &)
{
	MEMBERSHIP_TRACE((
	    "Membership engine : leader querying process memberships list\n"));

	membership_managers_global_rwlock.rdlock();

	//
	// Build the list of in-memory process-related memberships maintained
	//

	// First get the count of such process memberships
	unsigned int count = 0;
	string_hashtable_t<membership_manager_entry *>::iterator
	    iter(membership_managers);
	for (membership_manager_entry *entryp = NULL;
	    ((entryp = iter.get_current()) != NULL); iter.advance()) {
		SList<membership_manager_impl>::ListIterator
		    list_iter(entryp->manager_list);
		for (membership_manager_impl *mem_managerp = NULL;
		    ((mem_managerp = list_iter.get_current()) != NULL);
		    list_iter.advance()) {
			//
			// If the membership is a process membership type,
			// then increment count.
			//
			if ((mem_managerp->type ==
			    membership::PROCESS_UP_PROCESS_DOWN) ||
			    (mem_managerp->type ==
			    membership::PROCESS_UP_ZONE_DOWN)) {
				count++;
			}
		}
	}

	// Now build an IDL sequence of such process memberships
	proc_mem_seq_out =
	    new membership::membership_manager_info_seq(count, count);
	membership::membership_manager_info_seq &data_seq = *proc_mem_seq_out;
	iter.reinit(&membership_managers);
	unsigned int index = 0;
	for (membership_manager_entry *entryp = NULL;
	    ((entryp = iter.get_current()) != NULL); iter.advance()) {
		SList<membership_manager_impl>::ListIterator
		    list_iter(entryp->manager_list);
		for (membership_manager_impl *mem_managerp = NULL;
		    ((mem_managerp = list_iter.get_current()) != NULL);
		    list_iter.advance()) {
			//
			// If the membership is a process membership type,
			// then fill in this membership info in the sequence.
			//
			if ((mem_managerp->type ==
			    membership::PROCESS_UP_PROCESS_DOWN) ||
			    (mem_managerp->type ==
			    membership::PROCESS_UP_ZONE_DOWN)) {
				mem_managerp->fill_membership_manager_info(
				    data_seq[index]);
				index++;
			}
		}
	}
	ASSERT(index == count);
	membership_managers_global_rwlock.unlock();
}

//
// If engines are reconfiguring, then this method would wait
// until engine reconfiguration completes.
//
membership_manager_impl *
membership_engine_impl::get_membership_manager(
    const membership::membership_manager_info &manager_info)
{
	if (!membership_api_support_available()) {
		return (NULL);
	}

	// Wait for any engine reconfiguration to finish
	while (engine_reconfig_underway) {
		os::usecsleep((os::usec_t)1000);	// 1ms
	}
	membership_managers_global_rwlock.rdlock();
	membership_manager_impl *mem_managerp =
	    get_membership_manager_internal(manager_info);
	membership_managers_global_rwlock.unlock();
	return (mem_managerp);
}

//
// Get a pointer to a particular membership manager
// from the table of membership managers maintained by the engine
//
membership_manager_impl *
membership_engine_impl::get_membership_manager_internal(
    const membership::membership_manager_info &manager_info)
{
	ASSERT(membership_managers_global_rwlock.lock_held());

	// Convert the cluster ID to a string
	int num_digits = 0;
	char clid_str[CLID_MAX_LEN];
	num_digits = os::itoa((int)manager_info.cluster_id, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		return (NULL);
	}

	// Construct the name of the membership manager
	char mem_manager_name[MEMBERSHIP_NAME_MAX_LEN];
	int err = construct_membership_name(
	    mem_manager_name, manager_info.mem_type,
	    manager_info.cluster_id, manager_info.process_namep);
	if (err) {
		if (err == ENOENT) {
			// Invalid cluster ID
			ASSERT(0);
		} else if (err == EINVAL) {
			// Should not happen
			ASSERT(0);
		} else if (err != 0) {
			// Cannot happen
			ASSERT(0);
		}
		return (NULL);
	}

	//
	// Search for the membership manager in the table of
	// membership managers maintained by the engine
	//
	membership_manager_entry *cluster_entryp =
	    membership_managers.lookup(clid_str);
	if (cluster_entryp == NULL) {
		return (NULL);
	}

	SList<membership_manager_impl>::ListIterator
	    iter(cluster_entryp->manager_list);
	membership_manager_impl *mem_managerp = NULL;
	while ((mem_managerp = iter.get_current()) != NULL) {
		iter.advance();
		if (os::strcmp(mem_managerp->mem_manager_name,
		    mem_manager_name) == 0) {
			// Found
			return (mem_managerp);
		}
	}

	// Not found
	return (NULL);
}

//
// Instruction from the "leader" engine to each engine
// for finishing the engine reconfiguration.
// In the finish engine reconfiguration step, each engine updates
// the latest incarnation of the "base cluster" membership manager
// on its node with the incarnation as delivered by automaton.
// This is done because when a membership change event triggers
// for which a membership manager is registered, the membership manager
// receives the event, and updates its local latest information.
// However, for "base cluster" membership manager, the update
// has to happen after kernel CMM reconfigures. So, engine does this
// special case for "base cluster" membership managers.
//
void
membership_engine_impl::finish_engine_reconfiguration(cmm::seqnum_t,
    const cmm::membership_t &node_membership, Environment &)
{
	bool found = false;
	membership_manager_impl *managerp = NULL;

	uint_t clid = 0;
	if (clconf_get_cluster_id((char *)DEFAULT_CLUSTER, &clid)) {
		// Must be able to convert global cluster name to cluster ID
		ASSERT(0);
	}

	char manager_name[MEMBERSHIP_NAME_MAX_LEN];
	int err = construct_membership_name(
	    manager_name, membership::BASE_CLUSTER, clid, NULL);
	if (err) {
		//
		// We are trying to construct the name for the base cluster
		// membership manager. No failure is expected.
		//
		ASSERT(0);
	}

	char clid_str[CLID_MAX_LEN];
	int num_digits = os::itoa((int)clid, clid_str);
	if ((num_digits <= 0) || (num_digits > CLID_MAX_LEN)) {
		// Error while converting cluster ID to string
		ASSERT(0);
		return;	/*lint !e527 */
	}

	membership_manager_entry *entryp = membership_managers.lookup(clid_str);
	ASSERT(entryp);

	SList<membership_manager_impl>::ListIterator iter(entryp->manager_list);
	for (managerp = NULL;
	    ((managerp = iter.get_current()) != NULL); iter.advance()) {
		if (os::strcmp(managerp->mem_manager_name, manager_name) == 0) {
			found = true;
			break;
		}
	}
	if (!found) {
		ASSERT(0);
	}

	bool first_boot = false;
	if (managerp->latest_incn == INCN_UNKNOWN) {
		first_boot = true;
	}

	managerp->latest_incn =
	    node_membership.members[orb_conf::local_nodeid()];

	// Signal the state machine for the object
	if (first_boot) {
		// Allocate event packet to put on to event list
		event_packet_t *ev_packetp = new event_packet_t;
		//
		// Could not allocate small amount of memory;
		// serious memory shortage
		//
		CL_PANIC(ev_packetp != NULL);
		// Populate the event packet with required information
		ev_packetp->event = MEMBER_COMES_UP;
		ev_packetp->latest_incn = managerp->latest_incn;

		// Fill in default values for data that is not required
		ev_packetp->seqnum = 0;
		for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
			ev_packetp->membership.members[nid] = INCN_UNKNOWN;
		}

		managerp->state_lock.lock();
		managerp->event_list.append(ev_packetp);
		managerp->state_cv.signal();
		managerp->state_lock.unlock();
	}

	engine_reconfig_underway = false;
	MEMBERSHIP_TRACE((
	    "Membership engine : finish_engine_reconfiguration done\n"));
}

// ***********************
// Rolling upgrade support
// ***********************

bool
membership_api_support_available()
{
	bool retval = false;
	membership_version_lock.lock();
	if ((membership_current_version.major_num > 1) ||
	    ((membership_current_version.major_num == 1) &&
	    (membership_current_version.minor_num >= 4))) {
		retval = true;
	} else {
		retval = false;
	}
	membership_version_lock.unlock();
	return (retval);
}

//
// membership_upgrade_callback class methods
//
membership_upgrade_callback::membership_upgrade_callback()
{
}

membership_upgrade_callback::~membership_upgrade_callback()
{
}

void
#ifdef DEBUG
membership_upgrade_callback::_unreferenced(unref_t cookie)
#else
membership_upgrade_callback::_unreferenced(unref_t)
#endif
{
	// This object does not support multiple 0->1 transitions.
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Callback routine registered with version manager.
//
void
membership_upgrade_callback::do_callback(const char *,
    const version_manager::vp_version_t &new_version, Environment &)
{
	//
	// If we get a upgrade callback for a version that is not
	// strictly higher than our current version,
	// then we ignore such a callback.
	// This is because we could get multiple upgrade callbacks.
	//
	membership_version_lock.lock();
	if ((membership_current_version.major_num < new_version.major_num) ||
	    (membership_current_version.major_num == new_version.major_num &&
	    membership_current_version.minor_num < new_version.minor_num)) {
		// This callback is for a higher version
		MEMBERSHIP_TRACE((
		    "membership_upgrade_callback::do_callback() - "
		    "current membership version: %d.%d, "
		    "upgrade callback version: %d.%d\n",
		    membership_current_version.major_num,
		    membership_current_version.minor_num,
		    new_version.major_num, new_version.minor_num));
		membership_version_lock.unlock();
	} else {
		// This callback is not for a higher version; ignore it.
		MEMBERSHIP_TRACE((
		    "membership_upgrade_callback::do_callback() - "
		    "current membership version: %d.%d, "
		    "upgrade callback version: %d.%d, "
		    "ignoring stale version upgrade callback\n",
		    membership_current_version.major_num,
		    membership_current_version.minor_num,
		    new_version.major_num, new_version.minor_num));
		membership_version_lock.unlock();
		return;
	}

	//
	// If we are running a version that does not support membership API,
	// then now initialize membership subsystem since we received
	// an upgrade callback.
	//
	if (!membership_api_support_available()) {

		CL_PANIC(!membership_subsystem_initialized);
		if (!membership_subsystem_init_internal()) {
			//
			// Either :
			// (i) we failed to initialize membership subsystem, or
			// (ii) we are not running a software version that
			// supports membership subsystem, in which case we
			// should not have received the upgrade callback.
			//
			MEMBERSHIP_TRACE(("membership_upgrade_callback: "
			    "could not initialize membership subsystem\n"));
			//
			// SCMSGS
			// @explanation
			// Could not initialize membership subsystem.
			// @user_action
			// No user action needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "Membership: Could not initialize "
			    "membership subsystem.");
		}

		// Query automaton membership
		cmm::control_var cmm_ctrl_v = cmm_ns::get_control();
		if (CORBA::is_nil(cmm_ctrl_v)) {
			MEMBERSHIP_TRACE(("membership_upgrade_callback: "
			    "could not find CMM control interface\n"));
			//
			// SCMSGS
			// @explanation
			// Got a nil reference to the CMM control object
			// on the local node.
			// @user_action
			// No user action needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "Membership: Got nil reference to "
			    "CMM control object.");
		}
		cmm::cluster_state_t cmm_state;
		Environment env;
		cmm_ctrl_v->get_cluster_state(cmm_state, env);
		if (env.exception() != NULL) {
			env.exception()->print_exception((char *)"Membership: "
			    "could not get cluster state from CMM\n");
			MEMBERSHIP_TRACE(("membership_upgrade_callback: "
			    "could not get cluster state from CMM\n"));
			//
			// SCMSGS
			// @explanation
			// Could not get cluster state from CMM.
			// @user_action
			// No user action needed.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "Membership: Could not get "
			    "cluster state from CMM.");
		}

		//
		// Pass the base cluster membership and seqnum obtained from CMM
		// to the engine. Let the engines start their reconfiguration.
		// This reconfiguration is asynchronous. Membership api calls
		// will stall until engines finish their reconfiguration.
		//
		membership_engine_impl::the()->automaton_callback(
		    cmm_state.members, cmm_state.curr_seqnum);

	} else {
		//
		// We are already running the software version
		// that supports membership subsystem.
		// So no need to do initialize membership and reconfigure.
		//
		MEMBERSHIP_TRACE(("membership_upgrade_callback: "
		    "already running membership api, "
		    "no need to initialize membership subsystem\n"));
	}

	// Update the current version
	membership_version_lock.lock();
	membership_current_version = new_version;
	MEMBERSHIP_TRACE(("membership_upgrade_callback: "
	    "Current membership version: %d.%d\n",
	    membership_current_version.major_num,
	    membership_current_version.minor_num));
	membership_version_lock.unlock();
}
#endif	// (SOL_VERSION >= __s10)
