/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CMM_DEBUG_H
#define	_CMM_DEBUG_H

#pragma ident	"@(#)cmm_debug.h	1.11	08/09/02 SMI"

#include <orb/debug/haci_debug.h>
#include <sys/sol_version.h>

//
// Macros to print and trace CMM and quorum debug info.
//

//
// Tracing level
// -------------
// CMM and quorum modules log many messages into the CMM Trace Buffer.
// These levels control which messages are logged and which are not.
//
// Certain CMM and quorum interfaces provide the caller with
// the flexibility to specify a particular message tracing level.
//
enum cmm_trace_level {
	CMM_NONE = 0,	// No logging
	CMM_RED,	// An error happened
	CMM_AMBER,	// Not necessarily an error,
			// but something unusual happened
	CMM_GREEN	// Normal activity tracing (useful for verbose tracing)
};

//
// A global control on what is the minimal level of CMM and quorum trace
// messages that should be logged.
//
// A caller could set its own level of tracing when it instantiates
// a quorum device object.
//
// The message tracing macros determine whether to use the global minimal
// tracing level or the caller's specified tracing level.
// If the global minimal tracing level is higher than the caller's tracing
// level, then the global minimal tracing level is used;
// else the caller's tracing level is used.
// In other words, it means a caller can only increase the amount
// of tracing but not decrease it, by specifying its own tracing level
// to be used for CMM and quorum interfaces.
//
extern cmm_trace_level cmm_global_minimal_trace_level;

//
// The global variable cmm_trace_mask controls the amount of tracing
// and debug printing.
//
extern uint_t cmm_trace_mask;

//
// Masks that control the level of tracing and printing
//
#define	CMM_TRACE_ARGS		0x1	/* trace specified values */
#define	CMM_PRINT_ARGS		0x2	/* print specified values */
#define	CMM_PRINT_AUTM_ROW	0x4	/* print automaton local row */
#define	CMM_PRINT_AUTM_MATRIX	0x8	/* print automaton state matrix */

#if defined(HACI_DBGBUFS) && !defined(NO_CMM_DBGBUF)
#define	CMM_DBGBUF
#endif

#ifdef	CMM_DBGBUF
extern dbg_print_buf		cmm_dbg_buf; // trace buffer
#define	CMM_DBGBUF_PRINT(a)	HACI_DEBUG(ENABLE_CMM_DBG, cmm_dbg_buf, a)
#else
#define	CMM_DBGBUF_PRINT(a)
#endif

//
// Avoid some FlexeLint messages when using the print and trace macros.
// FlexeLint prints informational message #717 when using do...while(0)
// and warning #666 when a macro uses an argument more than once and the
// argument passed in is a function (even if the function is declared
// const).
//
/*CSTYLED*/
/*lint	-esym(666, CMM_PRINT, CMM_TRACE, CMM_SELECTIVE_TRACE)
	-emacro(717, CMM_PRINT, CMM_TRACE, CMM_SELECTIVE_TRACE)
*/

//
// Used to trace and print sufficiently important and unusual events.
//
#define	CMM_PRINT(args)	do { \
	CMM_DBGBUF_PRINT(args); \
	os::printf args; \
} while (0)

//
// CMM_TRACE is used to trace unusual CMM events into the trace buffer.
// It also prints out the message on the console under DEBUG.
// Both actions can be controlled by setting cmm_trace_mask to appropriate
// values.
//
#ifdef	DEBUG
#define	CMM_TRACE(args) do { \
	if (cmm_trace_mask & CMM_TRACE_ARGS) \
		CMM_DBGBUF_PRINT(args); \
	if (cmm_trace_mask & CMM_PRINT_ARGS) \
		os::printf args; \
} while (0)
#else
#define	CMM_TRACE(args) do { \
	if (cmm_trace_mask & CMM_TRACE_ARGS) \
		CMM_DBGBUF_PRINT(args); \
} while (0)
#endif

//
// Used to print under DEBUG; always causes tracing into the trace buffer
//
#ifdef	DEBUG
#define	CMM_DEBUG_PRINT(args)	CMM_PRINT(args)
#else
#define	CMM_DEBUG_PRINT(args)	CMM_DBGBUF_PRINT(args)
#endif

//
// Used for detailed tracing and printing when CMM_DEBUG is turned on
//
#ifdef CMM_DEBUG
#define	CMM_DEBUG_TRACE(args) CMM_TRACE(args)
#else
#define	CMM_DEBUG_TRACE(args)
#endif

//
// This macro determines whether to use the global minimal tracing level
// or the caller's specified tracing level.
// If the global minimal tracing level is higher than the caller's tracing
// level, then the global minimal tracing level is used;
// else the caller's tracing level is used.
// In other words, it means a caller can only increase the amount
// of tracing but not decrease it, by specifying its own tracing level
// to be used for CMM and quorum interfaces.
//
#define	CMM_RESULTANT_TRACE_LEVEL(caller_trace_level)	\
	((cmm_global_minimal_trace_level > caller_trace_level)?	\
	cmm_global_minimal_trace_level:	caller_trace_level)

//
// Used to trace messages selectively - depending on what is the global minimal
// tracing level and what is the caller's specified tracing level.
//
#define	CMM_SELECTIVE_TRACE(caller_trace_level, msg_level, args)	do { \
	ASSERT(msg_level > CMM_NONE);	\
	if (msg_level <= CMM_RESULTANT_TRACE_LEVEL(caller_trace_level))	{ \
		CMM_TRACE(args);	\
	} \
} while (0)

//
// Trace buffer for membership engine, managers, API code
//
#if (SOL_VERSION >= __s10) && defined(_KERNEL)
#if defined(HACI_DBGBUFS) && !defined(NO_MEMBERSHIP_DBGBUF)
#define	MEMBERSHIP_DBGBUF
#endif

#ifdef	MEMBERSHIP_DBGBUF
extern dbg_print_buf		membership_dbg_buf; // trace buffer
#define	MEMBERSHIP_DBGBUF_PRINT(a)	\
	HACI_DEBUG(ENABLE_CMM_DBG, membership_dbg_buf, a)
#else
#define	MEMBERSHIP_DBGBUF_PRINT(a)
#endif

#define	MEMBERSHIP_TRACE(args)	MEMBERSHIP_DBGBUF_PRINT(args);
#endif	// (SOL_VERSION >= __s10) && defined(_KERNEL)

#endif	/* _CMM_DEBUG_H */
