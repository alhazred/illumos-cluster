//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CMM_VERSION_H
#define	_CMM_VERSION_H

#pragma ident	"@(#)cmm_version.h	1.3	08/05/20 SMI"

//
// Current CMM version number.
//
#define	CMM_VERSION 1

//
// We also support version 0 of the CMM.
//
#ifndef CMM_VERSION_0
#define	CMM_VERSION_0
#endif // CMM_VERSION_0

#endif	/* _CMM_VERSION_H */
