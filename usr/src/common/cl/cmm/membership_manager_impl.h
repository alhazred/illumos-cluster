/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  membership_manager_impl.h
 *
 */

#ifndef	_MEMBERSHIP_MANAGER_IMPL_H
#define	_MEMBERSHIP_MANAGER_IMPL_H

#pragma ident	"@(#)membership_manager_impl.h	1.4	08/10/13 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <h/membership.h>
#include <cmm/membership_client.h>
#include <orb/object/adapter.h>
#include <sys/hashtable.h>

#define	NAME_TO_LOG_LEN	\
	(CLUSTER_NAME_MAX + PROCESS_NAME_MAX_LEN + 20)

#define	MEMBERSHIP_MANAGER_EVENT_TYPE_MAX_LEN	50

//
// Names of the events that membership engine delivers to membership managers
// consist of :
// (i) the cluster id
// (ii) the process name, if any
// (iii) some characters for the type of event
// (iv) some buffer characters (spaces etc.)
// So this is a reasonable size for the event name.
//
#define	MEMBERSHIP_MANAGER_EVENT_NAME_MAX_LEN	\
	(MEMBERSHIP_MANAGER_EVENT_TYPE_MAX_LEN + \
	CLID_MAX_LEN + PROCESS_NAME_MAX_LEN + 30)

int construct_mem_manager_event_name(
    char *namep, membership::mem_manager_event event,
    uint_t clid, const char *proc_namep);

int construct_mem_manager_event_name(
    char *namep, membership::mem_manager_event event,
    const char *cl_namep, const char *proc_namep);

// State machine states
typedef enum membership_state_machine_state {
	INVALID_STATE_DOWN = 0,
	INVALID_STATE_UP,
	INITIAL_STATE,
	UP_TRANSIT_STATE,
	UP_PENDING_STATE,
	UP_OBSOLETE_STATE,
	DOWN_PENDING_STATE,
	MEMBER_STATE,
	NOT_MEMBER_STATE,
	SHUTTING_DOWN_STATE,
	REMOVE_STATE	// Final state, no state handler associated
} membership_state_machine_state_t;

// State machine events for transition between states
typedef enum membership_state_machine_event {

	// Something happened that should not have happened!!!
	MEMBER_INVALID_EVENT = 0,

	//
	// Leader delivers "official membership list".
	// Local membership manager is not a member.
	//
	MEMBER_NOT_IN_LIST,

	//
	// Leader delivers "official membership list".
	// Local membership manager is a member with its latest incarnation
	// reflected in the "official membership list".
	//
	MEMBER_IN_LIST,

	//
	// Leader delivers "official membership list".
	// Local membership manager is a member with obsolete incarnation.
	// In other words, the latest "up" incarnation (not INCN_UNKNOWN)
	// of the local membership manager is not reflected in the "official
	// membership list"; the official membership reflects some other
	// "up" incarnation instead.
	//
	MEMBER_OBSOLETE_IN_LIST,

	//
	// Leader delivers "official membership list".
	// Local membership manager is a member with an "up" incarnation,
	// but the local membership manager has actually gone down
	// (latest incarnation is INCN_UNKNOWN).
	//
	MEMBER_DOWN_PENDING,

	// Local member goes down.
	MEMBER_GOES_DOWN,

	// Local member comes up.
	MEMBER_COMES_UP,

	// Membership being shut down.
	MEMBER_SHUTTING_DOWN,

	// Membership being removed.
	MEMBER_REMOVED
} membership_state_machine_event_t;

//
// Structure of an event packet put on to the event list
// that is processed by a membership state machine.
//
typedef struct event_packet {
	membership_state_machine_event_t	event;
	sol::incarnation_num			latest_incn;
	cmm::seqnum_t				seqnum;
	cmm::membership_t			membership;
} event_packet_t;

class membership_engine_impl;

//
// Implementation class for generic membership manager
//
class membership_manager_impl :
    public McServerof< membership::manager > {

    friend membership_engine_impl;

public :
	membership_manager_impl(membership::membership_type, bool,
	    const char *, const char *);
	~membership_manager_impl();

	static void init_state_transition_table();

	static char *construct_name_to_log(membership::membership_type mem_type,
	    const char *cl_namep, const char *proc_namep);

	bool leader_initialization(cmm::seqnum_t, const cmm::membership_t &);
	bool leader_fini();
	bool match_manager_info(membership::membership_type,
	    const char *, const char *);

	void fill_membership_manager_info(
	    membership::membership_manager_info&);

	void leader_data_collection(cmm::seqnum_t, const cmm::membership_t &);
	void local_membership_change_event(const char *, cmm::seqnum_t);
	void drive_reconfiguration(
	    const cmm::membership_t &, cmm::seqnum_t,
	    const cmm::membership_t &, cmm::seqnum_t);
	void shutdown_reconfiguration(
	    cmm::seqnum_t, cmm::seqnum_t, const cmm::membership_t &);
	void register_client(membership::client_ptr, Environment &);
	void unregister_client(membership::client_ptr, Environment &);
	void get_membership(
	    cmm::membership_t &, cmm::seqnum_t &, Environment &);

	// IDL methods
	void notify_leader(sol::incarnation_num, cmm::seqnum_t, Environment &);
	void update_membership_step(
	    const cmm::membership_t &, cmm::seqnum_t, Environment &);
	void query_latest_state(
	    cmm::seqnum_t &, sol::incarnation_num &, Environment &);
	void get_distributed_membership(
	    cmm::membership_t &, bool &, Environment &);
	bool start_shutdown(Environment &);
	void tell_leader_to_shutdown(Environment &);
	void shutdown_step(cmm::seqnum_t, Environment &);
	void finish_shutdown_step(cmm::seqnum_t, Environment &);

protected :
	// Handler methods for states of the state machine
	void invalid_state_down(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void invalid_state_up(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void initial_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void up_transit_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void up_pending_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void up_obsolete_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void down_pending_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void member_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void not_member_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);
	void shutting_down_state(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);

	// Common actions to be taken by any invalid state
	void invalid_state_common(
	    membership_state_machine_state_t,
	    membership_state_machine_event_t);

	//
	// Certain conditions must be true in a state of the state machine.
	// If the validation of such conditions fail, the state machine
	// empties out the event queue, and puts a MEMBER_INVALID_EVENT on to
	// the queue, so that the state machine can directly transition to
	// one of the invalid states - INVALID_STATE_UP or INVALID_STATE_DOWN.
	// Also trace and syslog messages saying that the validation failed.
	//
	void state_validation_failed(const char *);

	static void start_state_machine_thread(void *);

	// The state machine thread
	void state_machine_thread(void);

	void create_state_machine();

	bool start(void);
	bool stop(void);

	bool get_peer_refs(cmm::seqnum_t, const cmm::membership_t &);

	//
	// Register with membership engine, for receiving notifications
	// when certain membership change event types occur
	// (eg, zone/process coming up or going down).
	// Each type of membership manager specifies which
	// events it is interested in.
	//
	void register_with_engine();

	//
	// Unregister from membership engine, for all the events
	// that the object registered for.
	//
	void unregister_with_engine();

	//
	// Internal method called when membership is received from leader.
	//
	// A boolean argument says whether the membership is shutting down
	// or not. When this method is called for a membership being shut down,
	// this method does not publish cluster events; event generation
	// is done after the shut down is complete.
	//
	// Each type of membership specifies whether cluster events should be
	// published for its reconfiguration. If events should be published,
	// then this method publishes the appropriate cluster events,
	// and syslogs the membership changes as well.
	//
	void received_membership(
	    const cmm::membership_t &, cmm::seqnum_t, bool);

	// Schedule membership change callback tasks on a threadpool
	void callback_clients();

	// Returns true if the argument is older than the latest seqnum
	bool newer_reconfig_started(cmm::seqnum_t);

	//
	// Used by the state machine to get an event from the event list.
	//
	// This method gets the first event off the event list and returns it.
	// If the event list is empty, then this method waits until at least
	// one event is available on the list; and then returns
	// the first element off the list.
	//
	event_packet_t *get_next_event_or_wait();

	//
	// Process the event information from the event packet.
	// This will update the state machine's state variables based on
	// the event packet data; it will also return the next state based on
	// the current state and the event that occured.
	//
	membership_state_machine_state_t event_processing(
	    const event_packet_t *);

	//
	// Lock and cv to protect internal state, event list,
	// and membership data
	//
	os::mutex_t	state_lock;
	os::condvar_t	state_cv;

	// State of the membership manager
	membership_state_machine_state_t current_state;

	//
	// Event list which stores the events that
	// the state machine has to process.
	//
	SList<event_packet_t> event_list;

	//
	// Flag is set to true when this membership manager receives
	// a copy of "official membership data" from the leader
	// membership manager.
	// Hence this flag is false until we receive membership
	// information from leader.
	// It is set to false once the shutdown of this membership
	// is complete.
	//
	bool membership_info_received_from_leader;

	//
	// State handlers for state machine's states.
	//
	typedef void
	    (membership_manager_impl::*state_handler_t)(
	    membership_state_machine_state_t, membership_state_machine_event_t);
	state_handler_t handlers[SHUTTING_DOWN_STATE + 1];

	// Type of membership manager
	membership::membership_type type;

	//
	// Each type of membership is interested to know
	// when kernel CMM reconfigures, when its member comes up
	// and when its member goes down. It takes various actions
	// based on the event. The event names and associated actions
	// are stored in a data structure, so that when the engine
	// delivers an event, the membership manager executes
	// the action that it has associated with the event.
	// Note that the event names will be different for
	// different instances of membership managers, although
	// all membership managers of a given type are interested
	// in the same type of events.
	//
	enum actions {
		LEADER_DATA_COLLECTION = 0,
		MEMBER_CAME_UP,
		MEMBER_WENT_DOWN,
		NUM_ACTIONS
	};
	typedef void (membership_manager_impl::*handler_for_event_action_t)
	    (cmm::seqnum_t);

	struct event_actions_t {
		char event_name[MEMBERSHIP_MANAGER_EVENT_NAME_MAX_LEN];
		handler_for_event_action_t funcp;
	};
	// The data structure to store event names and associated actions
	event_actions_t event_actions[NUM_ACTIONS];

	// Methods that server the real action handlers
	void base_cluster_membership_change(cmm::seqnum_t);
	void member_came_up(cmm::seqnum_t);
	void member_went_down(cmm::seqnum_t);
	void member_changed_state(sol::incarnation_num, cmm::seqnum_t);

	//
	// If true, cluster events should be generated
	// once membership changes for this object.
	//
	bool generate_event;

	//
	// Membership manager types can optionally specify
	// if they want to do fencing.
	//
	bool initiate_fencing_flag;
	virtual void initiate_fencing(cmm::seqnum_t);

	//
	// Membership object types can optionally specify
	// a pre-membership-update-step. A membership type
	// can have its own implementation of the virtual method.
	//
	bool pre_mem_update_flag;
	virtual void pre_mem_update(
	    const cmm::membership_t &, cmm::seqnum_t, cmm::seqnum_t);

	//
	// Membership object types can optionally specify
	// a function to be called before the actual membership
	// is shutdown. A membership type can have
	// its own implementation of the virtual method.
	//
	bool pre_mem_shutdown_flag;
	virtual void pre_mem_shutdown();

	//
	// True till the first boot of the membership manager.
	//
	bool first_boot;

	//
	// Method to print syslog messages and generate cluster events.
	// A membership type can have its own implementation.
	//
	virtual void publish_events_and_syslog(
	    bool, cmm::seqnum_t, cmm::membership_t, cmm::membership_t);

	// Membership clients for this object
	SList<membership::client> membership_clients;

	// Lock to protect membership clients
	os::mutex_t membership_clients_lock;

	// Name of membership manager
	char mem_manager_name[MEMBERSHIP_NAME_MAX_LEN];

	//
	// Name of the membership manager to be used
	// in CMM trace buffer messages and syslog messages.
	// The membership manager name contains the cluster id and not
	// the cluster name. We should have the cluster name specified
	// in trace/syslog messages.
	//
	char *name_to_log;
	//
	// Each membership manager is related to
	// a particular cluster. The membership manager
	// is registered in the local nameserver context
	// of the cluster.
	//
	uint_t cluster_id;
	char cluster_name[CLUSTER_NAME_MAX];

	//
	// Some types of membership managers are process membership related.
	// This is the name of the process.
	// For non-process-membership types, this is NULL.
	//
	char process_name[PROCESS_NAME_MAX_LEN];

	//
	// After a kernel CMM reconfiguration ends,
	// all memberships maintained undergo a leader-data-collection
	// reconfiguration, where leader objects query and collect
	// membership data from their peers, and based on that collected data,
	// the leaders schedule a reconfiguration task for their memberships.
	// This flag is marked true when that process is underway,
	// so that any new reconfiguration waits till this process is done.
	//
	bool leader_data_collection_underway;

	//
	// Once the leader membership manager tells that the membership
	// is being shut down, this flag is set on the local membership manager.
	//
	bool			shutdown;

	//
	// Official membership data that is delivered by
	// leader membership manager.
	// But these data act as state variables for the state machine.
	// When the leader delivers a new set of membership data,
	// that data is put in to an event packet to be processed by
	// the state machine. The processing of the event packet by
	// the state machine copies the official membership data
	// delivered by leader to these state variables.
	//
	cmm::membership_t	membership;
	cmm::seqnum_t		seqnum;

	//
	// State variable for the state machine that records the latest
	// incarnation of the local membership component that the state
	// machine has processed.
	//
	sol::incarnation_num	latest_incn_known_to_state_machine;

	//
	// Local member's latest incarnation.
	// Updated when a local member changes state.
	// This is marked immediately after a local member changes state;
	// so that we have recorded the latest incarnation of
	// the local membership component.
	// During the leader-data-collection step, the leader queries
	// the latest incarnation of the local membership component;
	// the local membership manager sends this incarnation.
	//
	sol::incarnation_num latest_incn;

	//
	// Once a membership manager knows about a local member's
	// state change, it informs the "leader" about it.
	// The "leader" schedules a reconfiguration task
	// for this membership change trigger.
	// When a reconfiguration task starts executing,
	// if it finds that a newer reconfiguration task is scheduled
	// for the same membership manager, the older task aborts.
	// So, the "leader" needs to keep track of the latest
	// reconfiguration task scheduled for a membership manager.
	// Hence, the "leader" maintains this information
	// to store the latest membership info that it knows about.
	//
	// Leadership can change. We prefer keeping memory on all nodes,
	// rather than allocating memory only on the "leader" and releasing
	// memory on previous "leader" everytime leadership changes.
	//
	os::mutex_t latest_reconfig_lock;
	cmm::seqnum_t latest_seqnum;
	cmm::membership_t latest_membership;

	// "Leader" maintains references to all peers
	membership::manager_ptr peers[NODEID_MAX+1];

private :
	void _unreferenced(unref_t);

	// Prevent assignment, copy and pass by value.
	membership_manager_impl(const membership_manager_impl &);
	membership_manager_impl & operator = (membership_manager_impl &);
};

class base_cluster_membership_manager_impl :
    public membership_manager_impl {
public:
	base_cluster_membership_manager_impl();
	~base_cluster_membership_manager_impl();
};

class zone_cluster_membership_manager_impl :
    public membership_manager_impl {
public:
	zone_cluster_membership_manager_impl(const char *cl_namep);
	~zone_cluster_membership_manager_impl();
	void wait_till_fencing_underway();
private:
	void pre_mem_update(
	    const cmm::membership_t &, cmm::seqnum_t, cmm::seqnum_t);
	void pre_mem_shutdown();
	void publish_events_and_syslog(
	    bool, cmm::seqnum_t, cmm::membership_t, cmm::membership_t);
	void initiate_fencing(cmm::seqnum_t);

	bool fencing_underway;
	os::mutex_t fencing_underway_lock;
	os::condvar_t fencing_underway_cv;
};

// Process-up/process-down membership manager implementation
class process_up_process_down_membership_manager_impl :
    public membership_manager_impl {
public:
	process_up_process_down_membership_manager_impl(
	    const char *cl_namep, const char *proc_namep);
	~process_up_process_down_membership_manager_impl();
};

// Process-up/zone-down membership manager implementation
class process_up_zone_down_membership_manager_impl :
    public membership_manager_impl {
public:
	process_up_zone_down_membership_manager_impl(
	    const char *cl_namep, const char *proc_namep,
	    zone_cluster_membership_manager_impl *zc_mem_mgrp);
	~process_up_zone_down_membership_manager_impl();
private:
	void pre_mem_update(
	    const cmm::membership_t &, cmm::seqnum_t, cmm::seqnum_t);
	zone_cluster_membership_manager_impl *zc_mem_managerp;
};

// Data for membership change client callback threadpool
struct membership_callback_data_t {
	char membership_name[MEMBERSHIP_NAME_MAX_LEN];
	membership_manager_impl *mem_managerp;
	membership::client_ptr client_p;
	cmm::membership_t membership;
	cmm::seqnum_t seqnum;
};

// Membership change callback task that is scheduled on callback threadpool
class membership_callback_task : public defer_task {
public:
	membership_callback_task(
	    const char *, membership_manager_impl *, membership::client_ptr,
	    const cmm::membership_t &, cmm::seqnum_t);
	~membership_callback_task();
	void execute();

	//
	// The unique address tag for this defer_task
	// is the address of the membership manager
	//
	virtual void *get_address_tag();

private:
	membership_callback_data_t data;
};

#endif	/* (SOL_VERSION >= __s10) */
#endif	/* _MEMBERSHIP_MANAGER_IMPL_H */
