/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _AUTOMATON_IMPL_H
#define	_AUTOMATON_IMPL_H

#pragma ident	"@(#)automaton_impl.h	1.75	09/04/13 SMI"

#include <sys/os.h>
#include <h/cmm.h>
#include <h/quorum.h>
#include <orb/object/adapter.h>
#include <sys/nodeset.h>
#include <cmm/cmm_version.h>
#include <cmm/cmm_config.h>
#include <sys/clconf_int.h>

//
// State machine States and Events
//
//
// To allow rolling upgrades, the state values should not be
// modified after FCS of SC3.0.
//
// Note that the "lock-step states" (states that the nodes move
// through together) start with S_BEGIN. The numerical value of
// a lock-step state should be higher than the previous lock-step
// state by exactly 1.
//
// Some tests in automaton_impl.cc assume the relative values of
// the states. If the values are changed, these tests need to be
// revised.
//

#define	NUM_SHUTDOWN_STATES	5

typedef enum cmm_automaton_state {
	CMM_INVALID_STATE = 0,
	//
	// States not run yet by state machine.
	//
	S_DEAD		= 1,
	S_DOWN		= 2,
	S_UNKNOWN	= 3,
	S_STOPPED	= 4,
	S_ABORTED	= 5,
	S_STOP		= 6,
	S_ABORT		= 7,
	//
	CMM_EXIT	= 8,
	//
	// States run by state machine.
	//
	// States definition is constrained by
	//    1. arithmetic and assumptions on states numbers in legacy code
	//    2. backward compatibility requirement during rolling upgrade
	//
	CMM_START	= 20,
	CMM_RETURN	= 21,   // Position due to arithmetic on CMM_QCHECK,
				// CMM_BEGIN and CMM_STEP_1.
	CMM_BEGIN	= 40,   // Used for arithmetic in receive, reconfigure
				// sync_with_others.
	CMM_CONNECTIVITY = 41,  // To check the connectivity between all the
				// nodes of the cluster.
	CMM_QACQUISITION = 42,   // Acquire the quorum device.
	CMM_SQACQUIRE = 43,
	CMM_QCHECK	= 44,   // Used for arithmetic in receive, getivn and
				// get_current_step.
	CMM_SQCHECK = 45,
	CMM_STEP_1	= 46,   // Used for arithmetic in receive,
				// node_is_down, node_is_up, step_state and
				// max_cmm_transition_steps.
	CMM_STEP_2,
	CMM_STEP_3,
	CMM_STEP_4,
	CMM_STEP_5,
	CMM_STEP_6,
	CMM_STEP_7,
	CMM_STEP_8,
	CMM_STEP_9,
	CMM_STEP_10,
	CMM_STEP_11,
	CMM_STEP_12,
	CMM_STEP_13,
#ifdef _KERNEL_ORB
	//
	// CMM step states during shutdown.
	//
	CMM_SHUTDOWN_STEP_1,
	CMM_SHUTDOWN_STEP_2,
	CMM_SHUTDOWN_STEP_3,
	CMM_SHUTDOWN_STEP_4,
	CMM_SHUTDOWN_STEP_5,
#endif

	CMM_END_SYNC,	// Equivalent of former S_END state, aka CMM_STEP_14.
			// S_END was defined as S_STEP(cmax_step_number + 1).
	CMM_END_WAIT,	// Not a lock-step (synch) state.
			// Wait in this state for unstability.
	CMM_UPPER_LIMIT = 255, // Leave some space in enum.
	//
	// Aliases.
	//
	CMM_FIRST_STATE = CMM_INVALID_STATE,
	CMM_LAST_STATE = CMM_END_WAIT,
	CMM_TNUM_STATE = CMM_LAST_STATE  + 1,
#ifdef _KERNEL_ORB
	CMM_LAST_SHUTDOWN_STEP = CMM_SHUTDOWN_STEP_5,
#endif
	//
	// Used for arithmetic in max_cmm_transition_steps.
	//
#ifdef _KERNEL_ORB
	CMM_MAX_STATE = CMM_SHUTDOWN_STEP_5
#else
	CMM_MAX_STATE = CMM_STEP_13
#endif
} cmm_automaton_state_t;

//
// Defines step numbers.
//
#ifdef _KERNEL_ORB
#define	CMM_FIRST_SHUTDOWN_STEP_NUM (CMM_SHUTDOWN_STEP_1 - CMM_STEP_1 + 1)
#endif
#define	CMM_FIRST_STEP_NUM	1
#define	CMM_END_SYNC_STEP_NUM (CMM_END_SYNC - CMM_STEP_1 + 1)
#define	CMM_MAX_TRANSITION_NUM (CMM_MAX_STATE - CMM_STEP_1 + 1)

typedef enum {
	CMM_INVALID_EVENT,
	//
	CMM_STABLE,
	CMM_UNSTABLE,
	CMM_RECONFIGURE,
	CMM_PROCEED,
	CMM_CLUSTER_SHUTDOWN,
#ifdef CMM_VERSION_0
	CMM_OLD_STEPS_EVENT,
#endif // CMM_VERSION_0
	CMM_ABORT,
	//
	CMM_FIRST_EVENT = CMM_INVALID_EVENT,
	CMM_LAST_EVENT = CMM_ABORT,
	CMM_NUM_EVENT = CMM_LAST_EVENT + 1
} cmm_automaton_event_t;

//
// CMM Automaton Implementation Class
//

class automaton_impl : public McServerof < cmm::automaton > {

	friend class cmm_impl;
#ifdef _KERNEL_ORB
	friend class cmm_comm_impl;
#else
	friend class cm_comm_impl;
#endif

public:
	automaton_impl(cmm::comm_ptr, quorum::quorum_algorithm_ptr, nodeid_t,
	    incarnation_num);

	virtual		~automaton_impl();

	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//
	void	receive_msg(const cmm::message_t&, nodeid_t, Environment&);

	void	node_is_reachable(nodeid_t, sol::incarnation_num, Environment&);
	void	node_is_unreachable(nodeid_t, sol::incarnation_num,
			cmm::timeout_t, Environment&);

	//
	// Methods used by the control interface
	//
	void 	stop();
	void	abort();
	void 	reconfigure();
	void	get_membership(cmm::membership_t &members);
	bool	safe_to_takeover_from(nodeid_t node, bool wait_until_dead);
#ifdef DEBUG
	void	stop_cluster();
#endif

	//
	// State machine interface.
	//
	typedef cmm_automaton_event_t 	(automaton_impl::*cmm_handler_t)();
	cmm_handler_t	cmm_handlers[CMM_TNUM_STATE];
	cmm_automaton_state_t	cmm_transition[CMM_TNUM_STATE][CMM_NUM_EVENT];
	void initialize_machine(uint_t step_number);

	//
	// State machine state functions.
	//
	cmm_automaton_event_t state_machine_invalid_state();
	cmm_automaton_event_t state_machine_start_state();
	cmm_automaton_event_t state_machine_begin_state();
	cmm_automaton_event_t state_machine_connectivity_state();
	cmm_automaton_event_t state_machine_quorum_acquisition_state();
	cmm_automaton_event_t state_machine_quorum_acquisition_shutdown_state();
	cmm_automaton_event_t state_machine_qcheck_state();
	cmm_automaton_event_t state_machine_qcheck_shutdown_state();
	cmm_automaton_event_t state_machine_step_state();
	cmm_automaton_event_t state_machine_end_state();
	cmm_automaton_event_t state_machine_return_state();

	//
	// These methods are still called by cmm impl (out of state
	// machine context for now).
	//
	void 	stop_state();
	void 	stopped_state();
	void 	abort_state();
	void 	aborted_state();

	//
	// Get the state of the cluster.  This call is used for Solstice HA
	// support.
	//
	void	getcluststate(struct clust_state *csp);

	//
	// Get the initial view numbers
	//
	int	getivn(nodeid_t nodeid, cmm::seqnum_t &initialviewnumber);

	//
	// Function to return the maximum number of step transitions supported
	// by the CMM automaton.
	//
	static uint_t max_cmm_transition_steps(void);

	//
	// Implementation declarations
	//

	//
	// Return code from sync_with_others()
	//
	// The return value designates the following conditions:
	//	SWO_SYNC - all live nodes have reached the current state
	//	SWO_WAIT - some nodes haven't yet reached the current state
	//	SWO_UNSTABLE - the cluster membership has become unstable
	//		(when a node has died or a new node is joining the
	//		cluster, or the reconfiguration sequence numbers
	//		don't match, or the proposed memberships don't match).
	//	SWO_FORCE - got a node_is_down notification in begin state or
	//		    an administrator ordered a reconfiguration.
	//	SWO_CONTINUE - To continue performing other checks to detect
	//			stability of the cluster.
	//
	typedef enum {
		SWO_UNSTABLE,
		SWO_WAIT,
		SWO_SYNC,
		SWO_FORCE,
		SWO_CONTINUE
	} swo_stat_t;

private:

	os::mutex_t	_state_lock;
	os::condvar_t	_state_change_cv;

	nodeid_t	_mynodeid;
	cmm::seqnum_t	_max_reconfig_seqnum;
	cmm::comm_ptr	_commp;
	uint_t		_current_step;
	nodeid_t	_primary;
	int		last_step;

	//
	// A set of flags used by the automaton. The meaning of these
	// flags is explained in the large comment at the beginning of
	// automaton_impl.cc.
	//
	typedef enum {
		FORCE_RECONFIGURATION	= 0x0001,
		STOP_LOCAL_NODE		= 0x0002,
		NODE_IS_ABORTING	= 0x0004
	} automaton_flags_t;

	uint_t			_automaton_flags;

	quorum::quorum_algorithm_ptr _quorum_p;

	//
	// Set of nodes which other nodes can see but local node can't
	//
	nodeset			_disagree_join_set;

	//
	// This flag is used to detect membership changes that occur
	// while we are in the begin state.
	// When the flag is set (true) means membership has changed
	//
	bool			_membership_change_flag;

	nodeset 		_rebooting_nodes;
	cmm::membership_t	_membership;

	// Last time cmm_timer_thread ran
	hrtime_t		last_update_time;

	// timer_lock protects last_update_time
	os::mutex_t		timer_lock;

	typedef struct node_info_struct {
		incarnation_num		incarnation;
		cmm::seqnum_t		reconfig_seqnum;
		cmm::timeout_t		detection_delay;
		nodeset			heard_from;
		nodeset			membership;
		os::sc_syslog_msg	*msgp;
		hrtime_t		fenceoff_timer;
		bool			delta_timer;
		bool			must_reacquire_quorum;
		quorum::quorum_result_t	quorum_result[NODEID_MAX + 1];
		uint16_t		state_vector[NODEID_MAX + 1];
		nodeset			heard_from_vector[NODEID_MAX + 1];
		cmm::seqnum_t		initial_view_number;
		nodeset			join_occurred;
	} node_info_t;

	node_info_t		_nodes[NODEID_MAX + 1];

	//
	// Hide the default constructor. Also, prevent assignment, copy and
	// pass by value.
	//
	automaton_impl();
	automaton_impl(const automaton_impl &);
	automaton_impl & operator = (automaton_impl &);

	//
	// Local functions
	//
	void	node_is_dead(nodeid_t nodeid);
	void	node_is_down(nodeid_t nodeid);
	void	node_is_up(nodeid_t nodeid, sol::incarnation_num incn);
	void	node_is_shutting_down(nodeid_t nodeid);
	swo_stat_t sync_with_others();
	swo_stat_t sync_with_others_in_begin();
	swo_stat_t sync_with_others_common();
	swo_stat_t state_sync(uint16_t state);
	uint_t 	get_current_step();
#ifdef DEBUG
	void broadcast_stop_msg(void);
#endif
	void	broadcast_msg();
	void	send_msg(nodeid_t nodeid);
	void	prepare_msg(cmm::message_t& msg);
	void 	check_stop_abort();
	void 	check_abort();
	bool	is_previous(uint16_t, uint16_t);
	bool	is_next(uint16_t, uint16_t);
	void	clear_connectivity_matrix();
	bool	no_disconnects();
	bool	disagreements_in_join_set();
	bool	let_partition_wait();
	void	check_connectivity();
#ifdef _KERNEL_ORB
	void	process_disconnect(nodeid_t m, nodeid_t n,
		    nodeset m_membership, nodeset n_membership);
#endif
	nodeset	row_to_live_nodes(const nodeid_t row);
	void 	incr_seqnum();
	void	refresh_begin_state();
	void	start_split_brain_timer(nodeid_t nodeid);
	void	stop_fenceoff_timer(nodeid_t nodeid);
	void 	start_halt_timer(nodeid_t nodeid);

#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
	void	quorum_acquisition_common();
#endif

	void		per_tick_processing();
	static void	clock_callback();

	static void	boot_delay_thread(void *);
	void		boot_delay_work();

	void		check_node_health();
#ifdef	_KERNEL
	void		ping_health_check();
#endif
	void		delay_in_case_of_split_brain();

	static void	cmm_timer_thread_start(void *);
	void		cmm_timer_thread();

	static void	timeout_handler_thread_start(void *);
	void		timeout_handler_thread();

	void		set_version();
	nodeid_t	get_primary();

#ifdef	CMM_VERSION_0
	void		convert_msg_to_old(cmm::message_t&) const;
	void		convert_msg_to_new(cmm::message_t&) const;
#endif // CMM_VERSION_0

	void 		_print_row();
	void 		_print_matrix();
	const char	*_state_string(uint16_t state) const;
	inline void	_dump_state(int print_state);
};

#endif	/* _AUTOMATON_IMPL_H */
