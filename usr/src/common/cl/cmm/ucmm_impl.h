/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _UCMM_IMPL_H
#define	_UCMM_IMPL_H

#pragma ident	"@(#)ucmm_impl.h	1.23	08/06/23 SMI"

#include <h/ff.h>
#include <h/cmm.h>
#include <orb/object/adapter.h>
#include <sys/sol_version.h>

#if (SOL_VERSION >= __s10)
#define	UCMMD_PROC_NAME	"ucmmd"
extern boolean_t using_process_membership;
#endif

// header file for userland cmm module

class ucmm_impl : public McServerof<cmm::userland_cmm> {
public:
	ucmm_impl(const char *ucmm_name, const char *syslog_tag);
	~ucmm_impl() {}

	//
	// Function that returns a pointer to the single instance of the
	// ucmm_impl class.
	//
	static ucmm_impl &the();

	//
	// IDL interface
	//
	void ucmm_handshake(
		sol::nodeid_t			nodeid_in,
		sol::incarnation_num		incn_in,
		cmm::automaton_out		auto_out,
		cmm::remote_ucmm_proxy_out	proxy_out,
		Environment&			_environment);

	// Function to complete initialization of the ucmm
	void init(cmm::comm *);

	//
	// Function called from the _unreferenced of the remote_ucmm_proxy
	// object to indicate that a remote node has left the cluster
	//
	void	node_is_down(sol::nodeid_t nodeid, sol::incarnation_num incn);

#if (SOL_VERSION >= __s10)
	static void *membership_callback_thread_start(void *);
	void membership_callback_thread();
	void setup_process_membership();
	void wait_for_first_boot();

	// Rolling upgrade support
	void step1_upgrade_callback_action();
	void step2_upgrade_callback_action();
	void ru_transition(cmm::membership_t &old_membership);
#endif

	void	_unreferenced(unref_t);

	const char		*ucmm_name;	// name of UCMM
	const char		*syslog_tag;	// tag for logging messages
	sol::incarnation_num	local_incarnation; // local ucmm incn number

private:

#if (SOL_VERSION >= __s10)
	// Private helper methods
	void tell_membership_subsystem_to_create_membership(
	    char *zonenamep, uint32_t clid, char *process_namep);
	void register_for_membership_callbacks(
	    char *zonenamep, char *process_namep);
	void tell_membership_subsystem_that_we_are_up(
	    char *zonenamep, uint32_t clid, char *process_namep);

	//
	// We have two types of incarnations involved :
	// (i) The incarnation number of the ucmm_impl object is initialized
	// when the object is created.
	// (ii) The process membership subsystem maintains its own set of
	// incarnations; those incarnations being computed by
	// the process membership subsystem code itself when
	// a ucmmd tells process membership subsystem that it has come up.
	//
	// We have these two types of incarnations, as ucmm uses process
	// membership callbacks to know which peer came up or went down;
	// but also uses the automaton to drive its reconfiguration
	// which has the old logic of ucmm_impl incarnation.
	//
	// At some point in future, when we get rid of the automaton
	// from ucmmd, we can use the process membership incarnation only.
	//
	// Until then, we have to use the incarnation as mentioned in (i)
	// above for the internal reconfiguration of ucmm's automaton.
	// So, the process membership incarnation [type (ii) above]
	// is used just by the membership callback handler thread
	// to know when peer ucmmds joined/left process membership.
	// Based on that, the thread signals the internal automaton/ucmm_impl
	// using the automaton/ucmm_impl type of incarnation [type (i) above].
	//
	// Earlier, the type (i) incarnations were maintained in the
	// remote_ucmm_proxy objects. But after process membership support,
	// we do not consider a peer ucmmd as unreachable when we get
	// _unreferenced on the remote_ucmm_proxy object.
	// So we need a way to maintain a set of type (i) incarnations
	// for peer ucmmds, so that the process membership callback
	// handler thread can use them to signal the automaton/ucmm_impl
	// when a peer ucmmd goes down. The following data structure
	// stores such incarnations.
	//
	sol::incarnation_num	peer_incarnations[NODEID_MAX+1];
#endif

	cmm::automaton_var	_automp;	// local automaton
};

//
// The remote_ucmm_proxy object will get _unreferenced if the remote
// UCMM goes away.
//
class remote_ucmm_proxy_impl : public McServerof<cmm::remote_ucmm_proxy> {
public:

	remote_ucmm_proxy_impl(sol::nodeid_t nodeid, sol::incarnation_num incn);
	void	_unreferenced(unref_t);

private:
	sol::nodeid_t		nodeid;		// id of remote node
	sol::incarnation_num	incarnation;	// incn of remote node
};

#endif	/* _UCMM_IMPL_H */
