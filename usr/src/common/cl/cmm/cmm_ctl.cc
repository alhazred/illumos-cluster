//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cmm_ctl.cc	1.57	08/05/20 SMI"

// Utility to enable and disable monitoring by the CMM
// This enables debugging by allowing us to stop programs and examine
// their state without them being declared down.  Note that no new
// logical nodes (i.e. kernel ORBs or programs linked with the current
// userland ORB) should be started unless all ORBs can participate in
// reconfiguration, so any suspended programs should be resumed for the
// duration of the node join. Otherwise, the CMM will timeout the
// reconfiguration and will abort the node as soon as it is resumed.

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/os.h>
#include <sys/clconf_int.h>

#include <h/cmm.h>
#include <cmm/cmm_ns.h>
#include <orb/infrastructure/orb_conf.h>
#include <rgm/sczones.h>

#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#define	UNODE
#else
#undef	UNODE
#endif

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <zone.h>
#endif

static char	*last_path_elem(char *path);
static char	*program_name;
int		cmm_ctl(int argc, char * const argv[]);

void
usage()
{
	os::printf("usage: %s -[command] <optional arguments>\n", program_name);
	os::printf(
	    "The following provides the way to specify optional arguments : \n"
	    "\t-n <nodeid>	- node number, default to all nodes\n"
	    "\t-z <zone_name>	- name of zone, default to current zone\n\n"

	    "\tThe following specifies the possible commands.\n"
	    "\tOnly one command is allowed at a time.\n\n"

	    "\tThe following identifies the subcommands "
	    "that do not toggle state:\n"
	    "\ta	- abort node (must specify a single node)\n"
	    "\tg	- get cluster state\n"
	    "\tr	- trigger a reconfiguration\n"
	    "\ts	- stop node (must specify a single node)\n"
	    "\tt	- wait until safe_to_takeover_from a node "
	    "(must specify a single node)\n"
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	    "\tk	- failfast a node or a zone immediately\n"
	    "\t\t\tCan optionally specify a non-global zone\n"
	    "\t\t\tA different zone can only be specified from "
	    "the global zone.\n"
#else
	    "\tk	- failfast a node (default is local node) immediately\n"
#endif

	    "The following identifies the subcommands that toggle state:\n\n"
	    "\td	- disable transport path monitoring\n"
	    "\te	- enable transport path monitoring\n"
	    "\tp	- disable cluster-wide panic on specified nodes\n"
	    "\tP	- enable cluster-wide panic on specified nodes\n\n"

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	    "The following commands also operate on the specified zone.\n"
	    "The zone defaults to the current zone\n"
	    "The node defaults to all nodes\n"
	    "A different zone can only be specified from the global zone.\n"
	    "\tl	- disable zone level failfast control on specified "
	    "nodes\n"
	    "\tL	- enable zone level failfast control on specified "
	    "nodes\n"
#endif
	    "\tf	- disable node level failfast control on specified "
	    "nodes\n"
	    "\tF	- enable node level failfast control on specified "
	    "nodes\n");

	os::printf("See man page for detailed information\n");
}

//
// Extracts a node id from the command arguments.
// (1) If only one node id is specified, this routine returns the node id.
// (2) If multiple node ids are specified,
//	this routine prints an error message and exits.
// (3) If no option is specified, this routine returns 0 (default value).
// (4) If an option other than '-n' is given,
//	this routine prints an error message and exits.
//
nodeid_t
get_node_from_argument(int argc, char * const argv[])
{
	int		option;
	bool		read_n = false;
	nodeid_t	node_id = 0;

	while ((option = getopt(argc, argv, "n:")) != EOF) {
		switch (option) {
		case 'n':
			if (read_n) {
				os::printf("Bad usage : "
				    "Either specify one node or no nodes.\n");
				usage();
				exit(1);
			}
			read_n = true;
			node_id = (nodeid_t)atoi(optarg);
			break;
		default:
			os::printf("Bad argument\n");
			usage();
			exit(1);
		}
	}

	return (node_id);
}

#ifdef _KERNEL_ORB

int
unode_init()
{
	return (0);
}

#else

int
main(int argc, char * const argv[])
{
	int error = clconf_lib_init();

	if (error) {
		os::printf("%s: clconf_lib_init() returned error %d\n",
			argv[0], error);
		exit(1);
	}

	return (cmm_ctl(argc, argv));
}

#endif

void
print_cluster_state(cmm::cluster_state_t& cstate)
{
	os::printf("\n*** Current Cluster State ***\n\n");
	os::printf("\tLocal Node Id\t\t\t: %d\n", cstate.local_node_id);
	os::printf("\tHighest Node Id\t\t\t: %d\n", cstate.max_nodes);
	os::printf("\tCurrent reconfiguration seqnum\t: %llu\n",
						cstate.curr_seqnum);
	os::printf("\tMax reconfiguration step\t: %d\n", cstate.max_step);

	if (cstate.curr_step > cstate.max_step) {
		os::printf("\tCMM is in the end state.\n");

	} else if (cstate.curr_step == 0) {
		os::printf("\tCMM is in state %d.\n", cstate.curr_state);

	} else {
		os::printf("\tCMM is in reconfiguration step %d.\n",
			cstate.curr_step);

	}

	os::printf("\n\t*** Incarnation Numbers of Current Cluster "
		"Members ***\n\n");

	for (uint_t i = 1; i <= cstate.max_nodes; i++) {
		if (cstate.members.members[i] != INCN_UNKNOWN) {
			os::printf("\t\tNode %d\t: %d\n", i,
			    cstate.members.members[i]);
		}
	}

}

void
print_cluster_info(cmm::cluster_info_t& cinfo)
{
	clconf_cluster_t *cl = clconf_cluster_get_current();

	os::printf("\n*** Cluster Configuration Information ***\n\n");
	os::printf("\tMax Nodes Supported\t: %d\n", cinfo.max_nodes);
	os::printf("\tNode Fenceoff Timeout\t: %ld seconds\n",
		cinfo.node_fenceoff_timeout/MILLISEC);
	os::printf("\tBoot Delay\t\t: %d seconds\n",
		cinfo.boot_delay/MILLISEC);
	os::printf("\tNode Halt Timeout\t: %ld seconds\n",
		cinfo.node_halt_timeout/MILLISEC);

	//
	// Get other (CMM internal) configuration values from the CCR.
	//
	os::printf("\tFailfast grace time\t: %d seconds\n",
		clconf_cluster_get_failfast_grace_time(cl)/MILLISEC);
	os::printf("\tFailfast panic delay\t: %d seconds\n",
		clconf_cluster_get_failfast_panic_delay(cl)/MILLISEC);
	os::printf("\tOrb Stop Timeout\t: %d seconds\n",
		clconf_cluster_get_orb_stop_timeout(cl)/MILLISEC);
	os::printf("\tOrb Return Timeout\t: %d seconds\n",
		clconf_cluster_get_orb_return_timeout(cl)/MILLISEC);
	os::printf("\tOrb Abort Timeout\t: %d seconds\n",
		clconf_cluster_get_orb_abort_timeout(cl)/MILLISEC);
	os::printf("\tOrb Step Timeout\t: %d seconds\n",
		clconf_cluster_get_orb_step_timeout(cl)/MILLISEC);

	clconf_obj_release((clconf_obj_t *)cl);
}

void
print_quorum_status(quorum_status_t& qstatus)
{
	uint_t	i, j;

	os::printf("\n*** Cluster Quorum Information ***\n\n");
	os::printf("\tTotal configured votes\t: %d\n",
		qstatus.configured_votes);
	os::printf("\tCurrent cluster votes\t: %d\n", qstatus.current_votes);
	os::printf("\tVotes needed for quorum\t: %d\n",
		qstatus.votes_needed_for_quorum);

	os::printf("\n\t*** Node quorum info ***\n\n");
	for (i = 0; i < qstatus.num_nodes; i++) {
		//
		// The quorum status struct may include unconfigured nodes
		// if there are node number gaps. Do not print them.
		//
		uint64_t	keyval = 0;

		if (!orb_conf::node_configured(qstatus.nodelist[i].nid)) {
			continue;
		}

		for (j = 0; j < MHIOC_RESV_KEY_SIZE; j++) {
			keyval = (keyval << 8) + qstatus.nodelist[i].
				reservation_key.key[j];
		}

		os::printf("\t\tNode %ld\t: "
			"state = %s, votes_configured = %ld,\n"
			"\t\t\t  reservation_key = 0x%llx\n",
			qstatus.nodelist[i].nid,
			(qstatus.nodelist[i].state ==
			quorum::QUORUM_STATE_ONLINE) ? "UP" : "DOWN",
			qstatus.nodelist[i].votes_configured,
			keyval);
	}

	if (qstatus.num_quorum_devices == 0) {
		os::printf("\n\tNo quorum devices configured in this "
			"cluster.\n");
	} else {
		os::printf("\n\t*** Quorum device info ***\n\n");
		for (i = 0; i < qstatus.num_quorum_devices; i++) {

			quorum_device_status_t		*qp;
			char				*qname;

			qp = &qstatus.quorum_device_list[i];
			qname = qp->gdevname;

			if (qname == NULL) {
				continue; // device not configured
			}

			if (qp->state == quorum::QUORUM_STATE_ONLINE) {

				os::printf("\t\tQuorum device %d : "
				    "global device name = '%s',\n"
				    "\t\t\t\t  nodes with configured paths = "
				    "0x%llx,\n"
				    "\t\t\t\t  state = %s, "
				    "votes_configured = %d,\n"
				    "\t\t\t\t  reservation owner = node %d\n",
				    qp->qid, qname,
				    qp->nodes_with_configured_paths,
				    "ONLINE", qp->votes_configured,
				    qp->reservation_owner);

			} else {
				os::printf("\t\tQuorum device %d : "
				    "global device name = '%s',\n"
				    "\t\t\t\t  nodes with configured paths = "
				    "0x%llx,\n"
				    "\t\t\t\t  state = %s, "
				    "votes_configured = %d\n",
				    qp->qid, qname,
				    qp->nodes_with_configured_paths,
				    "OFFLINE", qp->votes_configured);
			}

		}
	}

	os::printf("\n");
}

void
get_cluster_state()
{
	cmm::control_var		ctrl_v;
	quorum::quorum_algorithm_var	quorum_v;
	cmm::cluster_state_t		cstate;
	cmm::cluster_info_t		cinfo;
	quorum_status_t			*qs;
	int				error;
	Environment			e;

	ctrl_v = cmm_ns::get_control();
	if (CORBA::is_nil(ctrl_v)) {
		os::printf("cannot access CMM control\n");
		return;
	}

	ctrl_v->get_cluster_state(cstate, e);
	if (e.exception()) {
		e.clear();
		os::printf("unsuccessful attempt to get cluster state\n");
		return;
	} else {
		print_cluster_state(cstate);
	}

	ctrl_v->get_cluster_conf_info(cinfo, e);
	if (e.exception()) {	// node not up
		e.clear();
		os::printf("unsuccessful attempt to get cluster info\n");
		return;
	} else {
		print_cluster_info(cinfo);
	}

	quorum_v = cmm_ns::get_quorum_algorithm(NULL);
	if (CORBA::is_nil(quorum_v)) {
		os::printf("cannot access quorum control\n");
		return;
	}

	error = cluster_get_quorum_status(&qs);
	if (error) {
		os::printf("clconf_get_quorum_status returned error %d\n",
		    error);
		return;
	} else {
		print_quorum_status(*qs);
		cluster_release_quorum_status(qs);
	}
}

int
check_node_number(nodeid_t n)
{
	cmm::control_var		ctrl_v;
	cmm::cluster_state_t		cstate;
	Environment			e;

	if (!orb_conf::node_configured(n)) {
		os::printf("%s: error: node number '%ld' is not "
		    "configured in cluster\n", program_name, n);
		return (1);
	}
	ctrl_v = cmm_ns::get_control();
	if (CORBA::is_nil(ctrl_v)) {
		os::printf("%s: error: could not access CMM control\n",
		    program_name);
		return (1);
	}
	ctrl_v->get_cluster_state(cstate, e);
	if (e.exception()) {
		e.clear();
		os::printf("%s: error: could not get cluster state\n",
		    program_name);
		return (1);
	} else if (cstate.members.members[n] == INCN_UNKNOWN) {
		os::printf("%s: error: node number '%ld' is not up\n",
		    program_name, n);
		return (1);
	}
	return (0);
}

//
// Checks if there is an error from calls to clconf_enable_cluster_wide_panic
// or clconf_disable_cluster_wide_panic and prints the appropriate error
// message.
//
static void
check_for_error(int error, nodeid_t nid, bool flag)
{
	const char *verb = flag ? "enable" : "disable";

	switch (error) {
	case 0:
		os::printf("cluster-wide panic %sd on node %ld\n", verb, nid);
		break;
	case EEXIST:
		os::printf("cluster-wide panic already %sd on node %ld\n",
		    verb, nid);
		break;
	case EBADRQC:
		os::printf("cluster-wide panic is applicable only on debug "
		    "cl_comm module\n");
		break;
	default:
		os::printf("Failed to %s cluster wide panic for node %ld, "
		    "return value = %ld\n", verb, nid, error);
	}
}

// Enable path monitoring on all cluster nodes
bool
cmm_ctl_enable_path_monitoring()
{
	int		error;
	bool		return_value = true;
	nodeid_t	node_id;
	nodeid_t	local_nodeid;

	local_nodeid = orb_conf::local_nodeid();

	//
	// Always enable on the local node first, in case a node has gone down
	// and we don't want to get caught invoking it.
	//
	error = clconf_enable_path_monitoring(local_nodeid);
	switch (error) {
	case 0:
		break;

	case EEXIST:
		os::printf("monitoring already enabled on node %ld\n",
		    local_nodeid);
		break;

	case ENOENT:
		os::printf("node %ld is not up\n", local_nodeid);
		return_value = false;
		break;

	case EIO:
		os::printf("enable path monitoring failed on node %ld\n",
		    local_nodeid);
		return_value = false;
		break;

	default:
		os::printf("unexpected error %d on node %ld\n",
		    error, local_nodeid);
		return_value = false;
		break;

	}

	for (node_id = 1; node_id <= NODEID_MAX; node_id++) {
		if (node_id == local_nodeid) {
			continue;
		}

		if (!orb_conf::node_configured(node_id)) {
			continue;
		}

		error = clconf_enable_path_monitoring(node_id);

		switch (error) {
		case 0:
			break;

		case EEXIST:
			os::printf("monitoring already enabled on node %ld\n",
			    node_id);
			break;

		case ENOENT:
			os::printf("node %ld is not up\n", node_id);
			return_value = false;
			break;
		case EIO:
			os::printf("enable path monitoring "
			    "failed on node %ld\n", node_id);
			return_value = false;
			break;

		default:
			os::printf("unexpected error %d on node %ld\n",
			    error, node_id);
			return_value = false;
			break;

		}
	}

	if (return_value) {
		os::printf("monitoring enabled\n");
	} else {
		os::printf("enable monitoring did not succeed on all nodes\n");
	}

	return (return_value);

}

// Disable path monitoring on all cluster nodes
bool
cmm_ctl_disable_path_monitoring()
{
	int		error;
	bool		return_value = true;
	nodeid_t	node_id;

	for (node_id = 1; node_id <= NODEID_MAX; node_id++) {

		if (!orb_conf::node_configured(node_id)) {
			continue;
		}

		error = clconf_disable_path_monitoring(node_id);

		switch (error) {
		case 0:
			break;

		case EEXIST:
			os::printf("monitoring already disabled on node %ld\n",
			    node_id);
			break;

		case ENOENT:
			os::printf("node %ld is not up\n", node_id);
			return_value = false;
			break;

		case EIO:
			os::printf("disable path monitoring "
			    "failed on node %ld\n", node_id);
			return_value = false;
			break;

		default:
			os::printf("unexpected error %d on node %ld\n",
			    error, node_id);
			return_value = false;
			break;

		}
	}

	if (return_value) {
		os::printf("monitoring disabled\n");
	} else {
		os::printf("disable monitoring did not succeed on all nodes\n");
	}

	return (return_value);

}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
//
// S10 and later (zones) :
// Set the node level failfast control to 'state'
//
// (1) If in global zone, set node level failfast control to 'state' for the
// specified cluster node. If no node is specifed, the node level failfast
// control on each cluster node is set to 'state'.
// (2) If in non-global zone, print error message and return.
// Node level failfast control should not be administered from non-global zones.
//
bool
cmm_ctl_set_node_level_failfast(bool state, int argc, char * const argv[])
{
	nodeid_t	node_id;
	Environment	e;

	if (getzoneid() != GLOBAL_ZONEID) {
		//
		// Command is issued inside a non-global zone.
		// Not allowed to administer node level failfast control.
		// Print error message and exit.
		//
		os::printf("Global failfast administration "
		    "is not allowed from non-global zones\n");
		return (false);
	}

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id is specified. Action is on all nodes.
		bool return_value = true;

		for (node_id = 1; node_id <= NODEID_MAX; node_id++) {

			if (!orb_conf::node_configured(node_id)) {
				continue;
			}

			ff::failfast_admin_var ff_admin_v =
			    cmm_ns::get_ff_admin(node_id);

			if (CORBA::is_nil(ff_admin_v)) {
				os::printf("node %ld is not up\n", node_id);
				return_value = false;
				continue;
			}

			ff_admin_v->set_node_level_failfast_control(state, e);

			if (e.exception()) {
				os::printf("node level failfast control"
				    " %s failed on node %ld\n",
				    ((state)?"enabling":"disabling"), node_id);
				return_value = false;
				continue;
			}

			os::printf("node level failfast control %s "
			    "on node %ld\n",
			    ((state)?"enabled":"disabled"), node_id);
		}

		return (return_value);

	} else {
		// Node_id is specified. Actions is taken on the specified node.
		if (!orb_conf::node_configured(node_id)) {
			os::printf("node %ld is not configured "
			    "as a cluster node\n", node_id);
			return (false);
		}

		ff::failfast_admin_var ff_admin_v =
		    cmm_ns::get_ff_admin(node_id);

		if (CORBA::is_nil(ff_admin_v)) {
			os::printf("node %ld is not up\n", node_id);
			return (false);
		}

		ff_admin_v->set_node_level_failfast_control(state, e);

		if (e.exception()) {
			os::printf("node level failfast control"
			    " %s failed on node %ld\n",
			    ((state)?"enabling":"disabling"), node_id);
			return (false);
		}

		os::printf("node level failfast control %s on node %ld\n",
		    ((state)?"enabled":"disabled"), node_id);

		return (true);
	}
}

//
// S10 and later (zones) :
// Set zone level failfast control to 'state'
//
// Default zone is current zone.
// If no node is specified, then the command takes action on all nodes.
// If the zone does not exist on the node,
// an error message is printed and the function returns.
//
// (1) If in global zone, set zone level failfast control to 'state' for the
// specified zone on the specified cluster node.
// (2) If in non-global zone, if the zone specified is same as the current zone,
// then the zone level failfast control is set to 'state' on the specified node.
// Else if the zone specified is not same as the current non-global zone,
// then print an error message and exit.
//
bool
cmm_ctl_set_zone_level_failfast(bool state, int argc, char * const argv[])
{
	bool		return_value = true;
	nodeid_t	node_id = 0;
	zoneid_t	zone_id = -1;
	char		*zonename = NULL;
	int		option;
	ff::failfast_admin_var ff_admin_v = ff::failfast_admin::_nil();

	bool read_n = false;
	bool read_z = false;

	while ((option = getopt(argc, argv, "n:z:")) != EOF) {

		switch (option) {
		case 'n':
			if (read_n) {
				os::printf("Bad usage : "
				    "Either specify one node or no nodes. "
				    "If no node is specified, then the "
				    "command takes action on all nodes.\n");
				usage();
				return (false);
			}
			read_n = true;
			node_id = (nodeid_t)atoi(optarg);
			break;
		case 'z':
			if (read_z) {
				os::printf("Bad usage : "
				    "Either specify one zone or no zones. "
				    "If no zone is specified, then the "
				    "command defaults to "
				    "the current zonename.\n");
				usage();
				return (false);
			}
			read_z = true;
			zonename = os::strdup(optarg);
			break;
		default:
			os::printf("Bad argument %c\n", option);
			usage();
			return (false);
		}
	}

	if ((zonename != NULL) && (os::strlen(zonename) != 0)) {
		//
		// Check that a different zone is not specified
		// in a non-global zone.
		//
		if (getzoneid() != GLOBAL_ZONEID) {
			char current_zone_name[ZONENAME_MAX];
			if (getzonenamebyid(getzoneid(), current_zone_name,
			    ZONENAME_MAX) < 0) {
				os::printf(
				    "Unable to retrieve current zone name\n");
				delete [] zonename;
				return (false);
			}

			if (strcmp(zonename, current_zone_name) != 0) {
				os::printf(
				    "Administration of a different zone "
				    "is not allowed from a non-global zone\n");
				delete [] zonename;
				return (false);
			}
		}
	} else {
		// Zone is not specified. Default is current zone.
		zonename = new char[ZONENAME_MAX];
		if (getzonenamebyid(getzoneid(), zonename,
		    ZONENAME_MAX) < 0) {
			os::printf("Unable to retrieve current zone name\n");
			delete [] zonename;
			return (false);
		}
	}

	Environment e;
	CORBA::Exception *exceptionp = NULL;

	if (node_id) {	// node id is specified

		if (!orb_conf::node_configured(node_id)) {
			os::printf(
			    "node %ld\n is not configured as a cluster node\n");
			return (false);
		}

		ff_admin_v = cmm_ns::get_ff_admin(node_id);
		if (CORBA::is_nil(ff_admin_v)) {
			os::printf("node %ld is not up\n", node_id);
			delete [] zonename;
			return (false);

		} else {
			if (strcmp(zonename, GLOBAL_ZONENAME) != 0) {
				// Non-global zone
				zone_id =
				    ff_admin_v->zonename_to_zoneid(zonename, e);
				if (e.exception()) {
					os::printf("zone level failfast control"
					    " %s failed for zone %s "
					    "on node %ld\n",
					    ((state)?"enabling":"disabling"),
					    zonename, node_id);
					delete [] zonename;
					return (false);
				}

				if (zone_id == -1) {
					// zone does not exist
					os::printf("zone %s does not exist "
					    "on node %ld\n", zonename, node_id);
					delete [] zonename;
					return (false);
				}
			} else {
				// Global zone has the same id on all nodes.
				zone_id = GLOBAL_ZONEID;
			}
		}

		ff_admin_v->set_zone_level_failfast_control(state, zone_id, e);

		exceptionp = e.exception();

		if (exceptionp != NULL) {
			ff::zone_violation *zone_exceptionp =
			    ff::zone_violation::_exnarrow(exceptionp);

			if (zone_exceptionp != NULL) {
				// Zone violation exception was thrown
				os::printf(
				    "zone %d tried to set zone level failfast "
				    "control for zone %d\n",
				    zone_exceptionp->current_zone,
				    zone_exceptionp->target_zone);
				e.clear();
			} else {
				// System exception was thrown
				os::printf("zone level failfast control "
				    " %s failed for zone %s on node %ld\n",
				    ((state)?"enabling":"disabling"),
				    zonename, node_id);
				return_value = false;
				e.clear();
			}
		} else {
			os::printf("zone level failfast control %s "
			    "for zone %s on node %ld\n",
			    ((state)?"enabled":"disabled"), zonename, node_id);
		}

	} else {
		// No nodes are specified. Command takes action on all nodes.
		for (node_id = 1; node_id <= NODEID_MAX; node_id++) {
			if (!orb_conf::node_configured(node_id)) {
				continue;
			}

			ff_admin_v = cmm_ns::get_ff_admin(node_id);

			if (CORBA::is_nil(ff_admin_v)) {
				os::printf("node %ld is not up\n", node_id);
				return_value = false;
				continue;

			}

			if (strcmp(zonename, GLOBAL_ZONENAME) != 0) {
				// Non-global zone
				zone_id =
				    ff_admin_v->zonename_to_zoneid(zonename, e);
				if (e.exception()) {
					os::printf("zone level failfast control"
					    " %s failed for zone %s "
					    "on node %ld\n",
					    ((state)?"enabling":"disabling"),
					    zonename, node_id);
					return_value = false;
					continue;
				}

				if (zone_id == -1) {
					// zone does not exist
					os::printf("zone %s does not exist "
					    "on node %ld\n",
					    zonename, node_id);
					return_value = false;
					continue;
				}
			} else {
				// Global zone has the same id on all nodes.
				zone_id = GLOBAL_ZONEID;
			}

			ff_admin_v->set_zone_level_failfast_control(
			    state, zone_id, e);

			exceptionp = e.exception();

			if (exceptionp != NULL) {
				ff::zone_violation *zone_exceptionp =
				    ff::zone_violation::_exnarrow(exceptionp);

				if (zone_exceptionp != NULL) {
					// Zone violation exception was thrown
					os::printf(
					    "zone %d tried to set zone level "
					    "failfast control for zone %d\n",
					    zone_exceptionp->current_zone,
					    zone_exceptionp->target_zone);
					e.clear();

				} else {
					// System exception was thrown
					os::printf(
					    "zone level failfast control %s "
					    "failed for zone %s on node %ld\n",
					    ((state)?"enabling":"disabling"),
					    zonename, node_id);
					return_value = false;
					e.clear();
					continue;
				}
			} else {
				os::printf("zone level failfast control %s "
				    "for zone %s on node %ld\n",
				    ((state)?"enabled":"disabled"),
				    zonename, node_id);
			}
		} // for loop ends here
	} // else block - "no nodes are specified"

	if (zonename) {
		delete [] zonename;
	}

	return (return_value);
}

//
// S10 and later (zones) :
// Default zone is current zone, and default node is current node.
// If zone does not exist on the node,
// then an error message is printed and the function exits.
//
// (1) If invoked in global zone, then:
//	(i) if a non-global zone is specified, failfast the zone on
//	the specified node, immediately
//	(ii) else failfast the cluster node corresponding to the node_id,
//	immediately. (same as failfasting the global zone)
// (2) If invoked in a non-global zone, then failfast the current
//	non-global zone on the specified node immediately.
//
bool
cmm_ctl_failfast_node(int argc, char * const argv[])
{
	Environment		e;
	int			option;
	nodeid_t		node_id = 0;
	zoneid_t		zone_id = -1;
	char			*zonename = NULL;
	ff::failfast_admin_var	ff_admin_v = ff::failfast_admin::_nil();
	bool			result;

	bool read_n = false;
	bool read_z = false;

	while ((option = getopt(argc, argv, "n:z:")) != EOF) {

		switch (option) {
		case 'n':
			if (read_n) {
				os::printf("Bad usage : "
				    "Either specify one node or no nodes. "
				    "If no node is specified, then the "
				    "command takes action "
				    "on the local node.\n");
				usage();
				return (false);
			}
			read_n = true;
			node_id = (nodeid_t)atoi(optarg);
			break;
		case 'z':
			if (read_z) {
				os::printf("Bad usage : "
				    "Either specify one zone or no zones. "
				    "If no zone is specified, then the "
				    "command defaults to "
				    "the current zonename.\n");
				usage();
				return (false);
			}
			read_z = true;
			zonename = os::strdup(optarg);
			break;
		default:
			os::printf("Bad argument\n");
			usage();
			return (false);
		}
	}

	if (node_id == 0) {
		node_id = orb_conf::local_nodeid();
	}

	if (check_node_number(node_id) != 0) {
		return (false);
	}

	if (zonename != NULL) {
		//
		// Check that a different zone is not specified
		// in a non-global zone.
		//
		if (getzoneid() != GLOBAL_ZONEID) {
			char current_zone_name[ZONENAME_MAX];
			if (getzonenamebyid(getzoneid(), current_zone_name,
			    ZONENAME_MAX) < 0) {
				os::printf(
				    "Unable to retrieve current zone name\n");
				delete [] zonename;
				return (false);
			}

			if (strcmp(zonename, current_zone_name) != 0) {
				os::printf(
				    "Administration of a different zone "
				    "is not allowed from a non-global zone\n");
				delete [] zonename;
				return (false);
			}
		}
	} else {
		// Zone is not specified. Default is current zone.
		zonename = new char[ZONENAME_MAX];
		if (getzonenamebyid(getzoneid(), zonename,
		    ZONENAME_MAX) < 0) {
			os::printf("Unable to retrieve current zone name\n");
			delete [] zonename;
			return (false);
		}
	}

	ff_admin_v = cmm_ns::get_ff_admin(node_id);
	if (CORBA::is_nil(ff_admin_v)) {
		os::printf("node %ld is not up\n", node_id);
		if (zonename) {
			delete [] zonename;
		}
		return (false);
	}

	if (strcmp(zonename, GLOBAL_ZONENAME) != 0) {
		// Target zone is a non-global zone
		zone_id = ff_admin_v->zonename_to_zoneid(zonename, e);
		if (e.exception()) {
			os::printf("zone level failfast administration "
			    "failed for zone %s on node %ld\n",
			    zonename, node_id);
			if (zonename) {
				delete [] zonename;
			}
			return (false);
		}

		if (zone_id == -1) {
			// zone does not exist
			os::printf("zone %s does not exist "
			    "on node %ld\n", zonename, node_id);
			delete [] zonename;
			return (false);
		}
	} else {
		// Target zone is the global zone
		zone_id = GLOBAL_ZONEID;
	}

	result = ff_admin_v->failfast_now(zone_id, e);

	if (e.exception() && !CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
		// Unexpected exception
		os::printf("invoking failfast_now for node %ld "
		    "had an exception\n", node_id);
		return (false);
	}

	if (zone_id == GLOBAL_ZONEID) {
		os::printf("'failfast_now' of node %ld %s\n", node_id,
		    ((result)? "succeeded": "failed"));
	} else {
		os::printf("'failfast_now' of zone %s on node %ld %s\n",
		    zonename, node_id, ((result)? "succeeded": "failed"));
	}

	return (true);
}

#else	// (SOL_VERSION >= __s10) && !defined(UNODE)

//
// Set the failfast control on the specified node to 'state'
// If no node is specified, then the action is taken on all nodes.
//
bool
cmm_ctl_set_node_level_failfast(bool state, int argc, char * const argv[])
{
	nodeid_t	node_id;
	int		error;

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id specified. Action is taken on all nodes.
		bool return_value = true;

		for (node_id = 1; node_id <= NODEID_MAX; node_id++) {
			if (!orb_conf::node_configured(node_id)) {
				continue;
			}

			error = (state)? (clconf_enable_failfast(node_id)):
			    (clconf_disable_failfast(node_id));

			switch (error) {
			case 0:
				os::printf("failfasts %s on node %ld\n",
				    ((state)?"enabled":"disabled"), node_id);
				break;

			case EEXIST:
				os::printf("failfasts already %s on node %ld\n",
				    ((state)?"enabled":"disabled"), node_id);
				break;

			case ENOENT:
				os::printf("node %ld is not up\n", node_id);
				return_value = false;
				break;

			case EIO:
				os::printf("%s failfast failed on node %ld\n",
				    ((state)?"enabling":"disabling"), node_id);
				return_value = false;
				break;

			default:
				os::printf("unexpected error %d on node %ld\n",
				    error, node_id);
				return_value = false;
				break;
			}
		}

		return (return_value);

	} else {
		// Node_id is specified. Action is taken on the specified node.
		if (!orb_conf::node_configured(node_id)) {
			os::printf("node %ld is not configured "
			    "as a cluster node\n", node_id);
			return (false);
		}

		error = (state)? (clconf_enable_failfast(node_id)):
		    (clconf_disable_failfast(node_id));

		switch (error) {
		case 0:
			os::printf("failfasts %s on node %ld\n",
			    ((state)?"enabled":"disabled"), node_id);
			return (true);

		case EEXIST:
			os::printf("failfasts already %s on node %ld\n",
			    ((state)?"enabled":"disabled"), node_id);
			return (true);

		case ENOENT:
			os::printf("node %ld is not up\n", node_id);
			return (false);

		case EIO:
			os::printf("%s failfast failed on node %ld\n",
			    ((state)?"enabling":"disabling"), node_id);
			return (false);

		default:
			os::printf("unexpected error %d on node %ld\n",
			    error, node_id);
			return (false);
		}
	}
}

//
// Failfast the specified node immediately.
// If no node is specified, then the local node is taken as the default.
//
bool
cmm_ctl_failfast_node(int argc, char * const argv[])
{
	Environment		e;
	nodeid_t		node_id = 0;
	ff::failfast_admin_var	ff_admin_v = ff::failfast_admin::_nil();

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id specified. Action is taken on local node.
		node_id = orb_conf::local_nodeid();
	}

	if (check_node_number(node_id) != 0) {
		return (false);
	}

	ff_admin_v = cmm_ns::get_ff_admin(node_id);
	if (CORBA::is_nil(ff_admin_v)) {
		os::printf("node %ld is not up\n", node_id);
		return (false);
	}

	ff_admin_v->failfast_now(e);

	if (e.exception() && !CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
		// Unexpected exception
		os::printf("invoking failfast_now for node %ld "
		    "had an exception\n", node_id);
		return (false);
	}

	os::printf("'failfast_now' of node %ld succeeded\n", node_id);
	return (true);
}

#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

bool
cmm_ctl_start_reconfiguration()
{
	Environment 		e;
	cmm::control_var	ctrl_v;

	ctrl_v = cmm_ns::get_control();
	ctrl_v->reconfigure(e);

	if (e.exception()) {
		os::printf("unsuccessful try to do a reconfiguration\n");
		return (false);
	} else {
		os::printf("reconfiguration was initiated\n");
		return (true);
	}
}

// Stop the cluster node corresponding to the given node id
bool
cmm_ctl_stop_node(int argc, char * const argv[])
{
	Environment 		e;
	cmm::control_var	ctrl_v;
	nodeid_t		node_id;

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id is specified. Must specify a node_id.
		os::printf("Bad usage: Must specify a node id.\n");
		usage();
		return (false);
	}

	if (check_node_number(node_id) != 0) {
		return (false);
	}

	ctrl_v = cmm_ns::get_control("Kernel CMM", node_id);
	ASSERT(!CORBA::is_nil(ctrl_v));
	ctrl_v->stop(e);

	if (e.exception() && !CORBA::COMM_FAILURE::_exnarrow(e.exception())) {

		// Unexpected exception
		os::printf("invoking 'stop' for node %ld had an exception\n",
		    node_id);
		return (false);
	}

	os::printf("'stop' of node %ld succeeded\n", node_id);
	return (true);
}

// Abort the cluster node corresponding to the given node id
bool
cmm_ctl_abort_node(int argc, char * const argv[])
{
	Environment 		e;
	cmm::control_var	ctrl_v;
	nodeid_t		node_id;

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id is specified. Must specify a node_id.
		os::printf("Bad usage: Must specify a node id.\n");
		usage();
		return (false);
	}

	if (check_node_number(node_id) != 0) {
		return (false);
	}

	ctrl_v = cmm_ns::get_control("Kernel CMM", node_id);
	ASSERT(!CORBA::is_nil(ctrl_v));
	ctrl_v->abort(e);

	if (e.exception() && !CORBA::COMM_FAILURE::_exnarrow(e.exception())) {

		// Unexpected exception
		os::printf("invoking 'abort' for node %ld had an exception\n",
		    node_id);
		return (false);
	}

	os::printf("'abort' of node %ld succeeded\n", node_id);
	return (true);
}

bool
cmm_ctl_safe_to_takeover_from(int argc, char * const argv[])
{
	Environment		e;
	cmm::control_var	ctrl_v;
	bool			safe;
	nodeid_t		node_id;

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id is specified. Must specify a node_id.
		os::printf("Bad usage: Must specify a node id.\n");
		usage();
		return (false);
	}

	ctrl_v = cmm_ns::get_control();
	ASSERT(!CORBA::is_nil(ctrl_v));

	safe = ctrl_v->safe_to_takeover_from(node_id, true, e);
	e.clear();
	if (e.exception() && !CORBA::COMM_FAILURE::_exnarrow(e.exception())) {

		// Unexpected exception
		os::printf("invoking safe_to_takeover for "
		    "node %ld had an exception\n", node_id);
		return (false);
	}

	if (!safe) {
		os::printf("NOT safe to take over from node %ld\n", node_id);
	} else {
		os::printf("safe to take over from node %ld\n", node_id);
	}

	return (true);
}

//
// Sets the clusterwide panic control to 'state' on the specified node.
// If no node is specified, then the action is taken on all nodes.
//
bool
cmm_ctl_set_clusterwide_panic_control(bool state, int argc, char * const argv[])
{
	nodeid_t	node_id;
	int		error;

	node_id = get_node_from_argument(argc, argv);

	if (node_id == 0) {
		// No node_id is specified. Action is taken on all nodes.
		for (node_id = 1; node_id <= NODEID_MAX; node_id++) {

			if (!orb_conf::node_configured(node_id)) {
				continue;
			}

			if (state) {
				error =
				    clconf_enable_cluster_wide_panic(node_id);
			} else {
				error =
				    clconf_disable_cluster_wide_panic(node_id);
			}

			check_for_error(error, node_id, state);

			//
			// If this is a bad request,
			// no need to try on other nodes.
			//
			if (error == EBADRQC) {
				return (false);
			}
		}

		return (true);

	} else {
		// Node_id is specified. Action is taken on the specified node.
		if (!orb_conf::node_configured(node_id)) {
			os::printf("Node %ld is not configured "
			    "as a cluster node\n");
			return (false);
		}

		if (state) {
			error = clconf_enable_cluster_wide_panic(node_id);
		} else {
			error = clconf_disable_cluster_wide_panic(node_id);
		}

		check_for_error(error, node_id, state);

		if (error == EBADRQC) {
			return (false);
		}

		return (true);
	}
}

// Called from main() or via unode_load.
int
cmm_ctl(int argc, char * const argv[])
{
	bool				did_something = false;
	bool				it_worked;
	int				option;

	program_name = last_path_elem(argv[0]);

	// Reset optind since we can be called multiple times from unode.
	optind = 1;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	option = getopt(argc, argv, "grdefFlLpPks:a:t:?");
#else
	option = getopt(argc, argv, "grdefFpPks:a:t:?");
#endif

	it_worked = true;

	switch (option) {

	case 'e':	// enable transport path monitoring
		it_worked = cmm_ctl_enable_path_monitoring();
		did_something = true;
		break;

	case 'd':	// disable transport path monitoring
		it_worked = cmm_ctl_disable_path_monitoring();
		did_something = true;
		break;

	case 'F':	// enable node level failfast on specified nodes
		it_worked = cmm_ctl_set_node_level_failfast(true, argc, argv);
		did_something = true;
		break;

	case 'f':	// disable node level failfast on specified nodes
		it_worked = cmm_ctl_set_node_level_failfast(false, argc, argv);
		did_something = true;
		break;

#if (SOL_VERSION >= __s10) && !defined(UNODE)

	case 'L':	// enable zone level failfast for the specified zone
			// on the specified node
		it_worked = cmm_ctl_set_zone_level_failfast(true, argc, argv);
		did_something = true;
		break;

	case 'l':	// disable zone level failfast for the specified zone
			// on the specified node
		it_worked = cmm_ctl_set_zone_level_failfast(false, argc, argv);
		did_something = true;
		break;

#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)

	case 'r':	// start reconfiguration
		it_worked = cmm_ctl_start_reconfiguration();
		did_something = true;
		break;

	case 'g':	// get cluster status
		get_cluster_state();
		did_something = true;
		break;

	case 's':	// stop a node
		it_worked = cmm_ctl_stop_node(argc, argv);
		did_something = true;
		break;

	case 'a':	// abort a node
		it_worked = cmm_ctl_abort_node(argc, argv);
		did_something = true;
		break;

	case 'k':	// Failfast node or zone immediately
		it_worked = cmm_ctl_failfast_node(argc, argv);
		did_something = true;
		break;

	case 't':	// safe_to_takeover_from
		it_worked = cmm_ctl_safe_to_takeover_from(argc, argv);
		did_something = true;
		break;

	case 'p':	// Disable cluster-wide panic on all nodes.
		it_worked =
		    cmm_ctl_set_clusterwide_panic_control(false, argc, argv);
		did_something = true;
		break;

	case 'P':	// Enable cluster-wide panic on all nodes.
		it_worked =
		    cmm_ctl_set_clusterwide_panic_control(true, argc, argv);
		did_something = true;
		break;

	case '?':
		usage();
		return (1);

	default:
		break;

	}

	if (!it_worked) {
		return (1);
	}

	if (!did_something) {
		usage();
		return (1);
	}

	return (0);
}


// Return a pointer to the last element in a file pathname;
static char *
last_path_elem(char *path)
{
	char    *q;

	for (q = path + os::strlen(path); q >= path; q--) {
		if (*q == '/') {
			return (q + 1);
		}
	}

	return (path);
}
