/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CMM_COMM_IMPL_H
#define	_CMM_COMM_IMPL_H

#pragma ident	"@(#)cmm_comm_impl.h	1.42	08/05/20 SMI"

#include <h/cmm.h>
#include <orb/object/adapter.h>
#include <sys/nodeset.h>
#include <cmm/cmm_config.h>

//
// A message number is used to detect stale or duplicate messages as well as
// to provide a way of checking whether or not other nodes have received the
// latest message broadcast by the CMM automaton.  Messages are retried until
// acknowledged or the other node is declared down. The number starts at 1
// and is incremented for every new message sent by the CMM automaton.
// It is not expected to wrap around.
//
typedef uint64_t cmm_msg_number;

//
// comm_message is additional information passed with each message sent by the
// automaton. This contains per-node information while the message sent by the
// automaton is the same to all nodes.
//
// Note: If multicast is used in the future, the msg_ack_incn and
//	 msg_ack_number fields will have to be replaced by a
//	 vector containing those values for each node.
//
typedef struct comm_message {
	cmm_msg_number		msg_sender_number;	// my message number
	sol::incarnation_num	msg_sender_incn;	// my incarnation_num
} comm_message_t;

//
// sender_info keeps track of information about each of the other nodes.
// The structure members are defined as follows:
//
//	last_sender_incn	last known incarnation of him
//	last_sender_number	last message number of his we have seen
//	my_message_number	local message number count for him
//
typedef struct sender_info {
	sender_info() : last_sender_incn(INCN_UNKNOWN),
	    last_sender_number(0) { }
	sol::incarnation_num	last_sender_incn;
	cmm_msg_number		last_sender_number;
	cmm_msg_number		my_message_number;
} sender_info_t;

typedef enum { SLEEP, WAKEUP, DIE } sender_thread_arg_t;

//
// CMM Communication Implementation Class
//

class cmm_comm_impl : public McServerof < cmm::comm > {

public:
	cmm_comm_impl();
	~cmm_comm_impl() {}
	void _unreferenced(unref_t);

	static cmm_comm_impl &the();

	static int	initialize();
	int initialize_int();

	// Called if the attempt to join the cluster fails and on shutdown.
	static void	shutdown();

	//
	// Method that implements the IDL interface
	//
	void send_all(
		const cmm::message_t &,
		quorum::membership_bitmask_t,
		Environment &);

	void send_stop_all(
		quorum::membership_bitmask_t,
		Environment &);

private:
	//
	// Callbacks
	//
	static void	cmm_rcv_callback_common(recstream *, nodeid_t, bool);

#ifdef	CMM_VERSION_0
	// Version 0 callback function.
	static void	cmm_rcv_callback(recstream *);
#endif // CMM_VERSION_0

	// Version 1 callback function.
	static void	cmm1_rcv_callback(recstream *);
#ifdef DEBUG
	static void	cmm_stop_msg_rcv_callback(recstream *);
#endif

	//
	// Local functions
	//
	void		receive_msg(const comm_message_t &,
			    cmm::message_t &, nodeid_t, bool);
	void		broadcast_msg();
	bool		send_msg(comm_message_t &, cmm::message_t &, nodeid_t);
	static void	sender_thread_start(void *arg);
	void		sender_thread();
	void		signal_sender_thread(sender_thread_arg_t);

	// State of the CMM COMM
	typedef enum { ENABLED, ABORTED } cmm_comm_state_t;
	cmm_comm_state_t	_state;

	cmm::automaton_ptr	_automp;	// interface automaton

	// Note that this lock must be dropped before calling into the automaton
	// This lock protects essentially all data in the class
	os::mutex_t	lck;
	void		lock()		{ lck.lock(); }
	void		unlock()	{ lck.unlock(); }

	cmm::message_t	msgs[2];	// one for automaton, one for sender.
	int		sender_index;	// last message sent by sender; sending
					// of msgs[sender_index] has either
					// completed or is now "in progress".
	int		latest_index;	// latest message from the automaton;
					// msgs[latest_index] is copied from
					// the msg argument of send_all(),
					// atomically.

	// sender_thread data
	os::condvar_t   sender_cv;
	sender_thread_arg_t sender_thread_arg;

	// reliable message data
	sender_info_t	sender_info[NODEID_MAX + 1];
	nodeset		destination_node_set;
	sol::incarnation_num my_incarnation;

	static cmm_comm_impl *the_cmm_comm_impl;

	cmm_comm_impl(const cmm_comm_impl &);
	cmm_comm_impl &operator = (cmm_comm_impl &);
};

//
// Class used to marshal/unmarshal CMM messages
//

class cmm_message {
public:
	cmm_message() { }

	void _put(MarshalStream &);
	void _get(MarshalStream &);

	comm_message_t	*comm_messagep;
	cmm::message_t	*messagep;
};

#include <cmm/cmm_comm_impl_in.h>
#endif	/* _CMM_COMM_IMPL_H */
