//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)control_impl.cc	1.47	08/05/20 SMI"

#include <orb/fault/fault_injection.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>
#include <cmm/control_impl.h>
#include <cmm/cmm_impl.h>
#include <cmm/cmm_comm_impl.h>
#include <sys/cl_comm_support.h>
#ifdef _KERNEL_ORB
#include <cmm/cmm_debug.h>
#include <orb/transport/path_manager.h>
#endif
#include <cmm/ucmm_api.h>

#if defined(DEBUG) && defined(_KERNEL)
extern bool cw_panic_enabled;
#endif

control_impl::control_impl(cmm_impl *cmm_implp) :
	_cmmp(cmm_implp)
{
}

control_impl::~control_impl()
{
	// Make lint happy
	_cmmp = NULL;
}

void
#ifdef DEBUG
control_impl::_unreferenced(unref_t cookie)
#else
control_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	_cmmp->dec_ref(DONT_EXIT);	// unreference the_cmm
}

// + control_impl(cmm::control::start)
void
control_impl::start(Environment &)
{
	_cmmp->start();
}

// + control_impl(cmm::control::stop)
void
control_impl::stop(Environment &)
{
	_cmmp->_autm.stop();
}

// + control_impl(cmm::control::abort)
void
control_impl::abort(Environment &)
{
	_cmmp->_autm.abort();
}

// + control_impl(cmm::control::reconfigure)
void
control_impl::reconfigure(Environment &)
{
	_cmmp->_autm.reconfigure();

#if defined(_KERNEL_ORB)
	FAULTPT_KCMM(FAULTNUM_CMM_CONTROL_RECONFIGURE,
	    FaultFunctions::generic);
#endif
}

// + control_impl(cmm::control::enable_monitoring)
bool
control_impl::enable_monitoring(Environment &)
{
#ifdef _KERNEL_ORB
	if (pm_enable_funcp()) {
		CMM_TRACE(("CMM monitoring re-enabled.\n"));
		//
		// SCMSGS
		// @explanation
		// Transport path monitoring has been enabled back in the
		// cluster, after being disabled.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Monitoring re-enabled.");
		return (true);
	} else
#endif
		return (false);
}

// + control_impl(cmm::control::disable_monitoring)
bool
control_impl::disable_monitoring(Environment &)
{
#ifdef _KERNEL_ORB
	if (pm_disable_funcp()) {
		CMM_TRACE(("CMM monitoring disabled.\n"));
		//
		// SCMSGS
		// @explanation
		// Transport path monitoring has been disabled in the cluster.
		// It is enabled by default.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Monitoring disabled.");
		return (true);
	}
#endif
	return (false);
}

//
// Return the current cluster state information to the caller. We make use
// of the getclusterstate method of the automaton class to retrieve most
// of the information.
//
void
control_impl::get_cluster_state(cmm::cluster_state_t &cstate, Environment &)
{
	struct clust_state cluststate;

	_cmmp->_autm.getcluststate(&cluststate);

	cstate.max_nodes = cluststate.rp_nnodes;
	cstate.local_node_id = cluststate.rp_local_nodeid;
	cstate.max_step = cluststate.rp_max_step;
	cstate.curr_step = cluststate.rp_curr_step;
	cstate.curr_state = cluststate.rp_curr_state;
	cstate.curr_seqnum = cluststate.rp_rseqnum;

	_cmmp->_autm.get_membership(cstate.members);
}

void
control_impl::get_cluster_conf_info(cmm::cluster_info_t &cinfo, Environment &)
{
	cinfo.max_nodes = NODEID_MAX;
	cinfo.node_fenceoff_timeout =
		(cmm::timeout_t)conf.node_fenceoff_timeout;
	cinfo.boot_delay = (cmm::timeout_t)conf.boot_delay;
	cinfo.node_halt_timeout =
		(cmm::timeout_t)conf.node_halt_timeout;
}

//
// Function to check if it is safe to take over data services from a node
// that has been declared down.
//
// The node state is maintained by the CMM automaton. This function simply
// calls the same interface provided for the CMM control interface by the
// CMM automaton.
//
bool
control_impl::safe_to_takeover_from(nodeid_t node, bool wait_until_dead,
	Environment &)
{
	//
	// Call the same interface in the CMM automaton.
	//
	return (_cmmp->_autm.safe_to_takeover_from(node, wait_until_dead));
}

//
// control_impl(cmm::control::cluster_shutdown)
//
bool
control_impl::cluster_shutdown(Environment &)
{
	if (cmm_impl::the().cluster_shutdown()) {
#ifdef KERNEL_ORB
		CMM_TRACE(("CMM Cluster shutdown in progress.\n"));
#endif
		return (true);
	}
	return (false);
}

//
// control_impl(cmm::control::get_cluster_shutdown)
//
bool
control_impl::get_cluster_shutdown(Environment &)
{
	return (cmm_impl::the().get_cluster_shutdown());
}

//
// control_impl(cmm::control::cmm_getinitialviewnumber)
//
int
control_impl::cmm_getinitialviewnumber(nodeid_t nodeid,
    cmm::seqnum_t &initialviewnumber, Environment &)
{
	return (_cmmp->_autm.getivn(nodeid, initialviewnumber));
}

//
// Enables the cluster-wide panic feature on this node.
// This is only applicable to debug kernel CMM module.
//	true  If debug kernel binary and cluster-wide panic is disabled.
//	false Otherwise.
//
bool
control_impl::cw_enable_panic(Environment &)
{
#if defined(DEBUG) && defined(_KERNEL)
	if (cw_panic_enabled) {
		return (false);
	} else {
		cw_panic_enabled = true;
		return (true);
	}
#else
	return (false);
#endif
}

//
// Disables the cluster-wide panic feature on this node.
// This is only applicable to debug kernel CMM module.
// Return value:
//	true  If debug kernel binary and cluster-wide panic is enabled.
//	false Otherwise.
//
bool
control_impl::cw_disable_panic(Environment &)
{
#if defined(DEBUG) && defined(_KERNEL)
	if (cw_panic_enabled) {
		cw_panic_enabled = false;
		return (true);
	} else {
		return (false);
	}
#else
	return (false);
#endif
}
