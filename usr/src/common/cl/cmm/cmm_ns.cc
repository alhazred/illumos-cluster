//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cmm_ns.cc	1.16	08/06/05 SMI"

#include <h/naming.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>
#include <cmm/cmm_ns_int.h>
#include <orb/infrastructure/orb_conf.h>

extern char *get_current_cluster_name();

CORBA::Object_ptr
#if (SOL_VERSION >= __s10) && !defined(UNODE)
cmm_ns::lookup_binding(const char *ctx, const char *name, nodeid_t n,
    const char *cluster_contextp)
#else
cmm_ns::lookup_binding(const char *ctx, const char *name, nodeid_t n)
#endif
{
	Environment e;
	naming::naming_context_var ctxp;
	char *full_name;

	if (n == NODEID_UNKNOWN)
		n = orb_conf::local_nodeid();

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	if (cluster_contextp) {
		ctxp = ns::local_nameserver_cz(cluster_contextp, n);
	} else {
		ctxp = ns::local_nameserver(n);
	}
#else
	ctxp = ns::local_nameserver(n);
#endif

	if (CORBA::is_nil(ctxp)) {
		return (ctxp);
	}

	if (ctx != NULL) {
		full_name = new char [os::strlen(ctx) + os::strlen(name) + 3];
		os::sprintf(full_name, "/%s/%s", ctx, name);
	} else {
		full_name = (char *)name;
	}

	CORBA::Object_ptr obj = ctxp->resolve(full_name, e);
	e.clear();
	if (ctx != NULL)
		delete [] full_name;
	return (obj);
}

cmm::callback_registry_ptr
cmm_ns::get_callback_registry(const char *cmm_id, nodeid_t n)
{
	CORBA::Object_var obj = lookup_binding(cmm_id,
	    cmm_callback_registry_name, n);
	return (cmm::callback_registry::_narrow(obj));
}

cmm::control_ptr
cmm_ns::get_control(const char *cmm_id, nodeid_t n)
{
	CORBA::Object_var obj = lookup_binding(cmm_id, cmm_control_name, n);
	return (cmm::control::_narrow(obj));
}

quorum::quorum_algorithm_ptr
cmm_ns::get_quorum_algorithm(const char *cmm_id, nodeid_t n)
{
	CORBA::Object_var obj = lookup_binding(cmm_id, quorum_name, n);
	return (quorum::quorum_algorithm::_narrow(obj));
}

quorum::device_type_registry_ptr
cmm_ns::get_device_type_registry(nodeid_t n)
{
	CORBA::Object_var obj = lookup_binding(NULL, "type_registry", n);
	return (quorum::device_type_registry::_narrow(obj));
}

ff::failfast_admin_ptr
cmm_ns::get_ff_admin(nodeid_t n)
{
	CORBA::Object_var obj = lookup_binding(NULL, ff_admin_name, n);
	return (ff::failfast_admin::_narrow(obj));
}

cmm::kernel_ucmm_agent_ptr
cmm_ns::get_kernel_ucmm_agent(nodeid_t n)
{
	CORBA::Object_var obj = lookup_binding(NULL, kernel_ucmm_agent_name, n);
	return (cmm::kernel_ucmm_agent::_narrow(obj));
}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
membership::manager_ptr
cmm_ns::get_membership_manager_ref(const char *membership_namep, nodeid_t n)
{
	char *cluster_contextp = get_current_cluster_name();
	ASSERT(cluster_contextp);
	if (cluster_contextp == NULL) {
		return (membership::manager::_nil());
	}
	CORBA::Object_var obj_v =
	    lookup_binding(NULL, membership_namep, n, cluster_contextp);
	delete [] cluster_contextp;
	return (membership::manager::_narrow(obj_v));
}

membership::manager_ptr
cmm_ns::get_membership_manager_ref_zc(
    const char *cluster_contextp, const char *membership_namep, nodeid_t n)
{
	CORBA::Object_var obj_v =
	    lookup_binding(NULL, membership_namep, n, cluster_contextp);
	return (membership::manager::_narrow(obj_v));
}

membership::engine_ptr
cmm_ns::get_membership_engine_ref(nodeid_t n)
{
	CORBA::Object_var obj_v =
	    lookup_binding(NULL, membership_engine_namep, n);
	return (membership::engine::_narrow(obj_v));
}

membership::api_ptr
cmm_ns::get_membership_api_ref(nodeid_t n)
{
	CORBA::Object_var obj_v =
	    lookup_binding(NULL, membership_api_namep, n);
	return (membership::api::_narrow(obj_v));
}
#endif	// (SOL_VERSION >= __s10) && !defined(UNODE)
