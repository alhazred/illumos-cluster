//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)membership_api_impl.cc	1.4	08/07/21 SMI"

#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)

#include <cmm/membership_api_impl.h>
#include <cmm/membership_manager_impl.h>
#include <cmm/membership_engine_impl.h>
#include <cmm/membership_client.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_ns.h>
#include <ccr/common.h>
#include <sys/clconf_int.h>

extern os::sc_syslog_msg *cmm_syslog_msgp; // for syslog messages

// The membership API - one per base node
membership_api_impl *membership_api_impl::the_membership_apip = NULL;

membership_api_impl::membership_api_impl()
{
}

// lint complains that some functions may throw exceptions in destructor
//lint -e1551
membership_api_impl::~membership_api_impl()
{
	delete the_membership_apip;
	the_membership_apip = NULL;
}
//lint +e1551

void
#ifdef DEBUG
membership_api_impl::_unreferenced(unref_t cookie)
#else
membership_api_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}

// static
membership_api_impl *
membership_api_impl::the()
{
	ASSERT(the_membership_apip != NULL);
	return (the_membership_apip);
}

// static
void
membership_api_impl::initialize()
{
	if (the_membership_apip == NULL) {
		the_membership_apip = new membership_api_impl;

		// The membership API object is essential.
		if (the_membership_apip == NULL) {

			MEMBERSHIP_TRACE(("Membership : Could not create "
			    "CMM membership API object. "
			    "Membership API would not work.\n"));
			//
			// SCMSGS
			// @explanation
			// Could not create the CMM membership API object.
			// This might be due to lack of memory.
			// @user_action
			// Lack of memory might lead to other problems
			// on the node. You must free up memory on the node.
			//
			(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
			    "Membership: Could not create CMM membership API "
			    "object. Membership API would not work.");
		}
	}
}

// static
void
membership_api_impl::shutdown()
{
	if (the_membership_apip != NULL) {
		delete the_membership_apip;
	}
}

bool
membership_api_impl::sanity_checks(
    const char *cluster_namep, Environment &e) const
{
	// Check if the cluster name passed is invalid
	uint_t clid;
	if (clconf_get_cluster_id((char *)cluster_namep, &clid) == ENOENT) {
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return (false);
	}

	// Check if the client is trying to violate cluster boundaries
	if (!check_cluster_access(cluster_namep, e)) {
		membership::cluster_boundary_violation *exceptionp =
		    new membership::cluster_boundary_violation;
		e.exception(exceptionp);
		return (false);
	}

	return (true);
}

//
// Client uses this IDL method to register for membership callbacks
// for a particular membership maintained.
//	(1) Global cluster clients can register for the global cluster
//	as well as any configured virtual cluster.
//	(2) Clients, in zones that are not part of any virtual cluster,
//	can register for the global cluster only.
//	(3) Clients in a virtual cluster can register
//	for their own cluster only.
//
void
membership_api_impl::register_client(
    membership::client_ptr client_p, membership::membership_type mem_type,
    const char *cluster_namep, const char *process_namep, Environment &e) const
{
	if (!sanity_checks(cluster_namep, e)) {
		return;
	}

	uint_t clid = 0;
	if (clconf_get_cluster_id((char *)cluster_namep, &clid)) {
		// We do not know any such configured cluster
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return;
	}

	membership::membership_manager_info manager_info;
	manager_info.mem_type = mem_type;
	manager_info.cluster_id = clid;
	manager_info.process_namep = os::strdup(process_namep);

	membership_manager_impl *managerp =
	    membership_engine_impl::the()->get_membership_manager(manager_info);
	if (managerp == NULL) {
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return;
	}

	managerp->register_client(client_p, e);
}

void
membership_api_impl::register_for_cluster_membership(
    membership::client_ptr client_p, const char *cluster_namep, Environment &e)
{
	membership::membership_type type;
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0) {
		type = membership::BASE_CLUSTER;
	} else {
		type = membership::ZONE_CLUSTER;
	}

	register_client(client_p, type, cluster_namep, NULL, e);
}

void
membership_api_impl::register_for_process_membership(
    membership::client_ptr client_p, const char *cluster_namep,
    const char *process_namep, Environment &e)
{
	register_client(client_p, membership::PROCESS_UP_PROCESS_DOWN,
	    cluster_namep, process_namep, e);
}

void
membership_api_impl::register_for_process_zone_membership(
    membership::client_ptr client_p, const char *cluster_namep,
    const char *process_namep, Environment &e)
{
	register_client(client_p, membership::PROCESS_UP_ZONE_DOWN,
	    cluster_namep, process_namep, e);
}

void
membership_api_impl::unregister_client(
    membership::client_ptr client_p, membership::membership_type mem_type,
    const char *cluster_namep, const char *process_namep, Environment &e) const
{
	if (!sanity_checks(cluster_namep, e)) {
		return;
	}

	uint_t clid = 0;
	if (clconf_get_cluster_id((char *)cluster_namep, &clid)) {
		// We do not know any such configured cluster
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return;
	}

	membership::membership_manager_info manager_info;
	manager_info.mem_type = mem_type;
	manager_info.cluster_id = clid;
	manager_info.process_namep = os::strdup(process_namep);

	membership_manager_impl *managerp =
	    membership_engine_impl::the()->get_membership_manager(manager_info);
	if (managerp == NULL) {
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return;
	}

	managerp->unregister_client(client_p, e);
}

void
membership_api_impl::unregister_for_cluster_membership(
    membership::client_ptr client_p, const char *cluster_namep, Environment &e)
{
	membership::membership_type type;
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0) {
		type = membership::BASE_CLUSTER;
	} else {
		type = membership::ZONE_CLUSTER;
	}

	unregister_client(client_p, type, cluster_namep, NULL, e);
}

void
membership_api_impl::unregister_for_process_membership(
    membership::client_ptr client_p, const char *cluster_namep,
    const char *process_namep, Environment &e)
{
	unregister_client(client_p, membership::PROCESS_UP_PROCESS_DOWN,
	    cluster_namep, process_namep, e);
}

void
membership_api_impl::unregister_for_process_zone_membership(
    membership::client_ptr client_p, const char *cluster_namep,
    const char *process_namep, Environment &e)
{
	unregister_client(client_p, membership::PROCESS_UP_ZONE_DOWN,
	    cluster_namep, process_namep, e);
}

void
membership_api_impl::get_membership(
    cmm::membership_t &membership_being_queried,
    cmm::seqnum_t &seqnum_being_queried,
    membership::membership_type mem_type, const char *cluster_namep,
    const char *process_namep, Environment &e) const
{
	if (!sanity_checks(cluster_namep, e)) {
		return;
	}

	uint_t clid = 0;
	if (clconf_get_cluster_id((char *)cluster_namep, &clid)) {
		// We do not know any such configured cluster
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return;
	}

	membership::membership_manager_info manager_info;
	manager_info.mem_type = mem_type;
	manager_info.cluster_id = clid;
	manager_info.process_namep = os::strdup(process_namep);

	membership_manager_impl *managerp =
	    membership_engine_impl::the()->get_membership_manager(manager_info);
	if (managerp == NULL) {
		membership::no_such_membership *exceptionp =
		    new membership::no_such_membership;
		e.exception(exceptionp);
		return;
	}

	managerp->get_membership(
	    membership_being_queried, seqnum_being_queried, e);
}

void
membership_api_impl::get_cluster_membership(
    cmm::membership_t &membership_being_queried,
    cmm::seqnum_t &seqnum_being_queried,
    const char *cluster_namep, Environment &e)
{
	membership::membership_type type;
	if (os::strcmp(cluster_namep, DEFAULT_CLUSTER) == 0) {
		type = membership::BASE_CLUSTER;
	} else {
		type = membership::ZONE_CLUSTER;
	}

	get_membership(membership_being_queried, seqnum_being_queried,
	    type, cluster_namep, NULL, e);
}

void
membership_api_impl::get_process_membership(
    cmm::membership_t &membership_being_queried,
    cmm::seqnum_t &seqnum_being_queried,
    const char *cluster_namep, const char *process_namep, Environment &e)
{
	get_membership(membership_being_queried, seqnum_being_queried,
	    membership::PROCESS_UP_PROCESS_DOWN,
	    cluster_namep, process_namep, e);
}

void
membership_api_impl::get_process_zone_membership(
    cmm::membership_t &membership_being_queried,
    cmm::seqnum_t &seqnum_being_queried,
    const char *cluster_namep, const char *process_namep, Environment &e)
{
	get_membership(membership_being_queried, seqnum_being_queried,
	    membership::PROCESS_UP_ZONE_DOWN,
	    cluster_namep, process_namep, e);
}

#endif	// (SOL_VERSION >= __s10)
