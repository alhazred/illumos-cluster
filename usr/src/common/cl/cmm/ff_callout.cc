/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)ff_callout.cc 1.12     08/05/20 SMI"

#include <sys/os.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/systm.h>
#include <cmm/ff_callout.h>
#include <sys/ddi.h>

//
// Failfast callout block constructor.
//
ff_callout::ff_callout() :
	c_lbnext(NULL),
	c_lbprev(NULL),
	c_runtime(0),
	c_func(NULL),
	c_arg(NULL),
	c_active(0)
{
}

//
// Failfast callout block destructor.
//
ff_callout::~ff_callout()
{
	//
	// Simply set pointer members to NULL to keep lint quiet.
	//
	c_lbnext = NULL;
	c_lbprev = NULL;
	c_func = NULL;
	c_arg = NULL;
}


//
// There is only one callout table for the failfast in the node.
// This is the ff_callout_table.
//
ff_callout_table ff_callo_tbl;

//
// Failfast callout table constructor. Initializes the hash bucket pointers
// to NULL.
//
ff_callout_table::ff_callout_table()
{
	clock_t lbolt_val;
	int error;

	for (uint_t i = 0; i < CALLOUT_BUCKETS; i++)
		ct_lbhash[i] = NULL;

	error = os::drv_getparm(LBOLT, &lbolt_val);
	CL_PANIC(error == 0);

	ct_runtime = ct_curtime = lbolt_val;
}

//
// Failfast callout table destructor.
//
ff_callout_table::~ff_callout_table()
{
	//
	// do nothing for now
	//
}

//
// Insert c_blk into the timeout hash chain. We insert the new element at the
// head of the chain.
//
// NOTE : The caller of this routine should take the respective bucket-lock
//	(ie. ct_hashlock[]) before calling this.
//
void
ff_callout_table::hash_insert(ff_callout **bucket_ptr, ff_callout *c_blk)
{
	ASSERT(ct_hashlock[CALLOUT_LBHASH(c_blk->c_runtime)].lock_held());

	ff_callout *cur_first_elem; // this is the current elem at the head

	cur_first_elem = *bucket_ptr;

	c_blk->c_lbnext = cur_first_elem; // our next is the current head elem
	c_blk->c_lbprev = NULL;  // we are at the head, so our prev is NULL

	if (cur_first_elem != NULL) {
		//
		// If the list is not empty, the current first element's
		// previous should be adjusted to point to us, as we will be
		// the new head element
		//
		cur_first_elem->c_lbprev = c_blk;
	}

	//
	// Make the hash bucket pointer point to us, as we are at the
	// head now.
	//
	*bucket_ptr = c_blk;
}

//
// Remove c_blk from the timeout hash chain and adjust the hash bucket chain
// pointer in the callout table if we are deleting the first element.
//
// NOTE : The caller of this routine should take the respective bucket-lock
//	(ie. ct_hashlock[]) before calling this.
//
void
ff_callout_table::hash_delete(ff_callout **bucket_ptr, ff_callout *c_blk)
{
	ASSERT(ct_hashlock[CALLOUT_LBHASH(c_blk->c_runtime)].lock_held());

	ff_callout *nextp = c_blk->c_lbnext; // our next element : could be NULL
	ff_callout *prevp = c_blk->c_lbprev; // our prev element : could be NULL

	if (nextp != NULL) {
		//
		// If the next element is not null, make it point to our
		// previous element, even if the previous element is null
		//
		nextp->c_lbprev = prevp;
	}

	if (prevp != NULL) {
		//
		// If our previous element is not null, make it point to our
		// next element, even if the next element is null
		//
		prevp->c_lbnext = nextp;
	} else {
		//
		// If our previous element is null, then we must be at the
		// head of the hash chain. So make the has bucket pointer
		// point to our next element, even if the next element is NULL.
		//
		*bucket_ptr = nextp;
	}
}

//
// Set a timeout associated with the callout block c_blk. If it already
// has a timeout associated with it, first remove that timeout and then
// arm the new one.
//
void
ff_callout_table::timeout(ff_callout *c_blk,
				void (*c_func)(caddr_t),
				caddr_t c_arg,
				clock_t delta)
{
	clock_t runtime;
	clock_t lbolt_val;
	int	error;

	//
	// First check to see if there is already a timeout associated
	// with this callout block. If so, cancel it.
	//
	if (c_blk->c_active) {
		runtime = c_blk->c_runtime;
		ct_hashlock[CALLOUT_LBHASH(runtime)].lock();
		hash_delete(&ct_lbhash[CALLOUT_LBHASH(runtime)], c_blk);
		ct_hashlock[CALLOUT_LBHASH(runtime)].unlock();
	}

	// Set the appropriate fields in the callout block.
	//
	if (delta <= 0)
		delta = 1;

	c_blk->c_func = c_func;
	c_blk->c_arg  = c_arg;

	error = os::drv_getparm(LBOLT, &lbolt_val);
	CL_PANIC(error == 0);

	runtime = c_blk->c_runtime = lbolt_val + delta;
	c_blk->c_active = 1;

	ct_hashlock[CALLOUT_LBHASH(runtime)].lock();
	hash_insert(&ct_lbhash[CALLOUT_LBHASH(runtime)], c_blk);
	ct_hashlock[CALLOUT_LBHASH(runtime)].unlock();

}

//
// Remove a timeout associated with the callout block c_blk
//
void
ff_callout_table::untimeout(ff_callout *c_blk)
{

	if (!c_blk->c_active) {
		//
		// Could this be a race between untimeout and the
		// timeout expiring? Just unlock and return.
		//
		return;
	}

	ct_hashlock[CALLOUT_LBHASH(c_blk->c_runtime)].lock();
	hash_delete(&ct_lbhash[CALLOUT_LBHASH(c_blk->c_runtime)], c_blk);
	ct_hashlock[CALLOUT_LBHASH(c_blk->c_runtime)].unlock();

	c_blk->c_active = 0;  // mark callout block as inactive

}

//
// The per_tick_processing routine is called every clock tick to do callout
// processing. Since failfast callouts are implemented only in Kernel mode,
// we do nothing if we are compiled without _KERNEL.
//
void
ff_callout_table::per_tick_processing()
{
	clock_t 	my_runtime, lbolt_val;
	ff_callout 	*callo_blk;
	int		error;

	ff_callo_tbl.lock();

	error = os::drv_getparm(LBOLT, &lbolt_val);
	CL_PANIC(error == 0);

	ff_callo_tbl.ct_curtime = lbolt_val;

	//
	// We could have missed some ticks. Hence execute the loop till
	// runtime catches up with curtime.
	//
	while (((my_runtime = ff_callo_tbl.ct_runtime) -
				ff_callo_tbl.ct_curtime) <= 0) {
		ff_callo_tbl.unlock();

		ff_callo_tbl.ct_hashlock[CALLOUT_LBHASH(my_runtime)].lock();

		callo_blk = ff_callo_tbl.ct_lbhash[CALLOUT_LBHASH(my_runtime)];

		while (callo_blk != NULL) {

			if (callo_blk->c_runtime != my_runtime) {
				callo_blk = callo_blk->c_lbnext;
				continue;
			}

			//
			// This callout block has reached its runtime.
			// Delete it from the hash and mark it as inactive.
			//
			ff_callo_tbl.hash_delete(&ff_callo_tbl.ct_lbhash[
					CALLOUT_LBHASH(my_runtime)], callo_blk);

			//
			// We drop the bucket lock here because, the
			// callout function could take long. And there could
			// be others who need to add new entries on this
			// bucket which should not be blocked.
			//
			ff_callo_tbl.ct_hashlock
			    [CALLOUT_LBHASH(my_runtime)].unlock();

			callo_blk->c_active = 0;

			//
			// Execute the callout function associated with this
			// callout block since its runtime has arrived.
			//
			(*callo_blk->c_func)(callo_blk->c_arg);

			//
			// We regrab the bucket lock and start from the
			// begining of the list. This is needed because, the
			// list could have been modified when we dropped the
			// bucket lock.
			//
			ff_callo_tbl.ct_hashlock
			    [CALLOUT_LBHASH(my_runtime)].lock();

			callo_blk =
			    ff_callo_tbl.ct_lbhash[CALLOUT_LBHASH(my_runtime)];
		}

		ff_callo_tbl.ct_hashlock[CALLOUT_LBHASH(my_runtime)].unlock();

		ff_callo_tbl.lock();
		ff_callo_tbl.ct_runtime++;
	}
	ff_callo_tbl.unlock();
}
