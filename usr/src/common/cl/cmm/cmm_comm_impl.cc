//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// 	cmm_comm_impl.cc
//
//	This file implements the CMM "comm" interface between the transport
//	independent and transport dependent components of the Cluster
//	Membership Monitor.
//
//	This version of code implements the dependent side of the interface
//	in a manner that is usable by both a dlpi style transport or a shm
//	style transport.  It is assumed that the shm "comm" implementation
//	will be rewritten to take full advantage of the hardware to perform
//	blindingly fast.
//

#pragma ident	"@(#)cmm_comm_impl.cc	1.123	08/05/20 SMI"

#include <sys/os.h>
#include <sys/ddi.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/flow/resource.h>
#include <orb/msg/orb_msg.h>
#include <cmm/cmm_comm_impl.h>
#include <cmm/cmm_debug.h>
#include <cmm/cmm_impl.h>
#include <cmm/cmm_ns.h>
#include <orb/debug/orb_stats_mgr.h>

//
// the_cmm_comm_impl is used by the static callback methods.
// It is assumed that there is only one object of this class per node.
//
cmm_comm_impl *cmm_comm_impl::the_cmm_comm_impl = NULL;

//
// IDL interface implementation functions
//

//
// If the transport implementation allows sending of messages from clock
// interrupt context, the messages may be sent from here, otherwise signal
// the sender thread to send the CMM automaton messages and return.
// This is a strong candidate for shm transport optimization.
//
void
cmm_comm_impl::send_all(
	const cmm::message_t &msg,
	quorum::membership_bitmask_t destination_nodes,
	Environment &)
{
	lock();

	// Don't use msgs[sender_index]. This simple calculation is "0"
	// when sender_index is "1", and is "1" when sender_index is "0".
	latest_index = 1 - sender_index;

	// Note: structure copy (cannot use bcopy on "const" argument)
	msgs[latest_index] = msg;

	//
	// Update destination set with new destination nodes.
	//
	nodeset new_destination_nodes(destination_nodes);
	destination_node_set.add_nodes(new_destination_nodes);

	//
	// Increase message number for new destination nodes.
	//
	for (nodeid_t to_nodeid = 1; to_nodeid <= NODEID_MAX; to_nodeid++) {
		if (new_destination_nodes.contains(to_nodeid)) {
			sender_info[to_nodeid].my_message_number++;
		}
	}

	signal_sender_thread(WAKEUP);

	unlock();
}

#ifdef DEBUG
#ifdef _KERNEL_ORB
void
cmm_comm_impl::send_stop_all(
	quorum::membership_bitmask_t destination_nodes,
	Environment &e)
{
	nodeid_t	to_nodeid;
	nodeset destination_nodes_set;
	invocation_mode invo_mode(
	    invocation_mode::ONEWAY |
	    invocation_mode::NONBLOCKING |
	    invocation_mode::UNRELIABLE);

	destination_nodes_set.set(destination_nodes);
	for (to_nodeid = 1; to_nodeid <= NODEID_MAX; to_nodeid++) {
		if (destination_nodes_set.contains(to_nodeid)) {
			//
			// Specifying INCN_UNKNOWN will send the message
			// to current incn
			//
			ID_node node(to_nodeid, INCN_UNKNOWN);
			//
			// Identify message resources
			//
			resources resource(resources::NO_FLOW_CONTROL, &e,
			    invo_mode, 0 /* header size */,
			    0 /* data size */,
			    node, NULL, STOP_MSG);
			//
			// Check whether the message can proceed now.
			// The sendstream is allocated when the msg can
			// proceed.
			//
			sendstream *send_streamp =
			    orb_msg::reserve_resources(&resource);
			//
			// This can fail for COMM_FAILURE as well
			// as WOULDBLOCK. If there was a failure, then
			// we won't be able to send the message, Ignore.
			//
			if (send_streamp != NULL) {
				orb_msg::send(send_streamp);
				send_streamp->done();
				if (e.exception() != NULL) {
					e.clear();
					CMM_TRACE(("Coudn't send cluster_stop"
					    " message to node %d\n",
					    to_nodeid));
				}
			} else {
				if (e.exception() != NULL) {
					e.clear();
				}
				CMM_TRACE(("Cant send cluster_stop message"
				    " to node %d\n", to_nodeid));
			}
		}
	}
}
#else // _KERNEL_ORB
//
// This interface is not used outside of kernel.
//
void
cmm_comm_impl::send_stop_all(
    quorum::membership_bitmask_t destination_nodes,
    Environment &)
{
	ASSERT(0);
} /*lint !e715 */
#endif // _KERNEL_ORB
#else // DEBUG
//
// This is not used in non-debug mode.
//
void
cmm_comm_impl::send_stop_all(
    quorum::membership_bitmask_t destination_nodes,
    Environment &)
{
	CL_PANIC(0);
} /*lint !e715 */
#endif // _DEBUG

//
// static
cmm_comm_impl &
cmm_comm_impl::the()
{
	CL_PANIC(the_cmm_comm_impl != NULL);
	return (*the_cmm_comm_impl);
}

//
// Initialization entry point for the kernel CMM, called from ORB::initialize
// after the transport is initialized.
//
// static
int
cmm_comm_impl::initialize()
{
	CL_PANIC(the_cmm_comm_impl == NULL);
	the_cmm_comm_impl = new cmm_comm_impl();
	if (the_cmm_comm_impl == NULL) {
		return (ENOMEM);
	}
	return (the_cmm_comm_impl->initialize_int());
}

//
// Constructor
//
cmm_comm_impl::cmm_comm_impl() : _state(ENABLED), _automp(nil),
    sender_index(0), latest_index(0), sender_thread_arg(SLEEP),
    my_incarnation(INCN_UNKNOWN)
{
	bzero(msgs, sizeof (msgs));
	bzero(sender_info, sizeof (sender_info));
}

int
cmm_comm_impl::initialize_int()
{
	int err;

	my_incarnation = orb_conf::local_incarnation();

	cmm::comm_var comm_v;
	comm_v = get_objref();
	_automp = cmm_impl::cmm_init("Kernel CMM", orb_conf::node_number(),
		comm_v, orb_conf::local_incarnation());

	//
	// Do not start the sender thread or register transport callbacks until
	// all CMM objects are initialized
	//

	//
	// Run sender_thread at the same priority as the transitions_thread
	//
	if (!cmm_impl::cmm_create_thread(sender_thread_start, this,
	    CMM_MAXPRI - 1)) {
		(void) cmm_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"CMM: Unable to create %s thread.", "sender");
		return (ENOMEM);
	}

#ifdef	CMM_VERSION_0
	// Enable receiving of cmm version 0 messages
	orb_msg::the().set_message_props(CMM_MSG, false,
	    orb_msg::THREADPOOL_CMM, cmm_rcv_callback);
#endif // CMM_VERSION_0

	// Enable receiving of CMM version 1 messages.
	orb_msg::the().set_message_props(CMM1_MSG, false,
	    orb_msg::THREADPOOL_CMM, cmm1_rcv_callback);

#ifdef  DEBUG
	//
	// Enable receiving of cmm cluster stop messages
	//
	orb_msg::the().set_message_props(STOP_MSG, true,
	    orb_msg::THREADPOOL_ORBUTIL, cmm_stop_msg_rcv_callback);
#endif // DEBUG

	//
	// CMM ready to process node reachability callbacks.
	// PM holds off on forming paths until this call, so that
	// CMM won't lose the callbacks.
	//
	cmm_impl::register_with_pm();

	return (0);
}

void
#ifdef DEBUG
cmm_comm_impl::_unreferenced(unref_t cookie)
#else
cmm_comm_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CL_PANIC(_state != ENABLED);
	CL_PANIC(the_cmm_comm_impl == this);
	delete this;
	the_cmm_comm_impl = NULL;
}

//
// Broadcast the CMM message to other nodes.
//
// The function currently doesn't use broadcast or multicast.
// We may want to give consideration to using either broadcast or multicast
// to support clusters with large number of nodes.
//
// This code assumes it is called from a single thread, currently the
// sender_thread.
//

void
cmm_comm_impl::broadcast_msg(void)
{
	comm_message_t	comm_msg;
	nodeid_t	to_nodeid;

	CL_PANIC(lck.lock_held());

	comm_msg.msg_sender_incn = my_incarnation;
	//
	// Send the message to all nodes in destination_node_set or
	// ack_node_set. If the node is only in ack_node_set, we only
	// need to send an ack, but we send the whole message with the
	// piggybacked ack anyway.
	//
	for (to_nodeid = 1; to_nodeid <= NODEID_MAX; to_nodeid++) {
		if (destination_node_set.contains(to_nodeid)) {
			CL_PANIC(to_nodeid != orb_conf::node_number());
			//
			// Always send the latest message.
			//
			// latest_index:
			//   Points to the latest message update if any.
			//
			// sender_index:
			//   Points to the message we are currently sending.
			//
			sender_index = latest_index;
			//
			// destination_node_set:
			//   Contains nodes we have been asked to send a
			//   message to.
			//
			// sender_info[].my_message_number:
			//   Contains a unique increasing message number
			//   per potential destination node.
			//
			// A node is added to destination_node_set whenever
			// we receive a new message request (send_all()).
			//
			// A node is removed from destination_node_set
			// whenever we failed to communicate with it
			// because it went down, or whenever the send
			// operation succeeded and we did not receive
			// a new send request for that same node.
			//
			// A node remains in the destination_node_set when
			// we failed to communicate with it due to transient
			// resource shortage or because we received a new
			// send request for that same node, in between.
			//
			cmm_msg_number	current_message_number =
				sender_info[to_nodeid].my_message_number;
			comm_msg.msg_sender_number =
				sender_info[to_nodeid].my_message_number;

			// (Because we release the lock, it is possible that
			// we receive a more recent request to send a message
			// to the node we are currently about to send to...).
			//
			unlock();
			//
			// Send message of type cmm::message_t.
			//
			bool err;
			err = send_msg(comm_msg, msgs[sender_index], to_nodeid);
			lock();
			if (err) {
				//
				// Send failed. If the current incarnation of
				// the node has been marked down by the
				// automaton, it can be removed from the
				// destination_node_set. It will be added back
				// to the set when we get a message after it
				// reboots.  When to_nodeid is booting, the
				// automaton may still have the previous
				// incarnation and indicate a down state.
				//
				uint16_t		state;
				sol::incarnation_num	incn;

				//
				// cmm::message_t message type
				//
				state = msgs[sender_index].
				    state_list[to_nodeid];
				incn = msgs[sender_index].
				    incarnations[to_nodeid];

				if ((state <= S_DOWN) &&
				    (incn == sender_info[to_nodeid].
					last_sender_incn)) {
					destination_node_set.remove_node(
						to_nodeid);
				}
			} else {
				//
				// If send was successfull and there is no
				// new message to send to the same node then
				// remove node from destination list.
				//
				if (current_message_number ==
				    sender_info[to_nodeid].my_message_number) {
					destination_node_set.
						remove_node(to_nodeid);
				}
			}
		}
	}
}

//
// Examine the CMM message received.  Pass it on to the automaton if the
// comm_message header passes a number of tests.
//
void
cmm_comm_impl::receive_msg(const comm_message_t &comm_msg,
    cmm::message_t &rcv_msg, nodeid_t from_nodeid, bool is_old_version)
{
	Environment	e;
	sender_info_t	*senderp;

	CL_PANIC((from_nodeid != NODEID_UNKNOWN) &&
		(from_nodeid <= NODEID_MAX) &&
		(from_nodeid != orb_conf::node_number()));

	lock();

	if (_state == ABORTED) {
		unlock();
		return;
	}

	//
	// Throw away messages from old incarnations of the remote node.
	//
	senderp = &sender_info[from_nodeid];
	if (comm_msg.msg_sender_incn < senderp->last_sender_incn) {
		unlock();
		if (is_old_version) {
#ifdef	CMM_VERSION_0
			orb_stats_mgr::the().inc_orphans(from_nodeid, CMM_MSG);
#else
			ASSERT(0);
#endif // CMM_VERSION_0
		} else {
			orb_stats_mgr::the().inc_orphans(from_nodeid, CMM1_MSG);
		}
		return;
	}

	//
	// Check if this is a newer incarnation of the node than what
	// we have seen before.
	//
	if (comm_msg.msg_sender_incn > senderp->last_sender_incn) {
		//
		// Update our information about that node.
		//
		senderp->last_sender_incn = comm_msg.msg_sender_incn;
	} else {
		//
		// current incarnation
		//
		// Out-of-order messages and messages received as duplicates
		// (perhaps over redundant paths) should not be passed to
		// the automaton.
		//
		if (comm_msg.msg_sender_number <= senderp->last_sender_number) {
			unlock();
			CMM_DEBUG_TRACE(("receive_msg: discarded "
				"out-of-order msg\n"));
			return;
		}
	}

	senderp->last_sender_number = comm_msg.msg_sender_number;

	unlock();
	//
	// If the message is an old version message, convert to new message
	// format before sending to the the automaton.
	//
	if (is_old_version) {
		cmm_impl::the().convert_msg_to_new(rcv_msg);
	}
	_automp->receive_msg(rcv_msg, from_nodeid, e);
}

//
// Common function used for both old and new version messages.
//
// static
void
cmm_comm_impl::cmm_rcv_callback_common(recstream *re, nodeid_t from_ndid,
    bool is_old_version)
{
	Environment	e;

	comm_message_t	comm_msg;

	cmm::message_t	rcv_msg;
	cmm_message	cmm_msg;

	// Extract the message into its parts
	cmm_msg.comm_messagep = &comm_msg;
	cmm_msg.messagep = &rcv_msg;
	cmm_msg._get(re->get_MainBuf());
	re->done();

	//
	// Process the received message.
	//
	the().receive_msg(comm_msg, rcv_msg, from_ndid, is_old_version);
}

#ifdef	CMM_VERSION_0
//
// cmm_rcv_callback() gets messages from the transport on a separate
// tcp connection reserved for CMM. This callback function handles
// CMM version 0 messages.
//
// static
void
cmm_comm_impl::cmm_rcv_callback(recstream *re)
{
	nodeid_t	from_ndid = re->get_src_node().ndid;
	//
	// If we have committed to run the new version, then discard
	// the messages in the old format.
	//
	if (!cmm_impl::the().is_old_version()) {
		CMM_TRACE(("Got the old version message from node %d\n",
		    from_ndid));
		re->done();
		return;
	}
	cmm_rcv_callback_common(re, from_ndid, true);
}
#endif // CMM_VERSION_0

//
// cmm1_rcv_callback() gets messages from the transport on a separate
// tcp connection reserved for CMM. This callback function handles
// CMM version 1 messages.
//
// static
void
cmm_comm_impl::cmm1_rcv_callback(recstream *re)
{
	uint_t		curr_cmm_version;
	nodeid_t	from_ndid = re->get_src_node().ndid;

	//
	// We get the version 1 messages from nodes only after
	// version 1 is committed.
	//
	if (cmm_impl::the().is_old_version()) {
		//
		// The new version has been committed but we haven't
		// realised this.
		//
		CMM_TRACE(("Node %d is using the new version %d, commit to "
		    "use the new version.\n", from_ndid, CMM_VERSION));
		cmm_impl::the().set_new_version();
	}
	cmm_rcv_callback_common(re, from_ndid, false);
}


#ifdef DEBUG
#ifdef _KERNEL_ORB
void
cmm_comm_impl::cmm_stop_msg_rcv_callback(recstream *re)
{
	nodeid_t from_ndid = re->get_src_node().ndid;
	//
	// SCMSGS
	// @explanation
	// This can be seen only on DEBUG binaries. When a CL_PANIC or ASSERT
	// failure happens on any cluster node, the other nodes are also
	// forced to go down to help in analysing the root cause.
	// @user_action
	// Save the cores from all the nodes to check for a potential bug.
	//
	(void) cmm_syslog_msgp->log(SC_SYSLOG_PANIC, MESSAGE,
	    "Got cluster_stop from cluster node %d\n", from_ndid);
}
#else // _KERNEL_ORB
//
// Not used outside of the kernel.
//
void
cmm_comm_impl::cmm_stop_msg_rcv_callback(recstream *re)
{
	ASSERT(0);
} /*lint !e715 */
#endif // _KERNEL_ORB
#endif // DEBUG

//
// Send a message to a remote node. Return true if the send fails
// and false otherwise.
//
bool
cmm_comm_impl::send_msg(comm_message_t &comm_msg, cmm::message_t &msg,
    nodeid_t to_nodeid)
{
	Environment	e;
	bool		is_old_version = cmm_impl::the().is_old_version();
	cmm::message_t	tmp_msg = msg;

	//
	// During rolling upgrade some nodes will be running the
	// the old version of CMM, convert the message so that all
	// the nodes running old versions of CMM will understand and
	// interpret the message.
	//
	if (is_old_version) {
		cmm_impl::the().convert_msg_to_old(tmp_msg);
	}

	//
	// CMM messages are of type Oneway, Nonblocking, Unreliable.
	// Note that the ORB guarantee the reliability of Oneway
	// Nonblocking messages anyway, regardless of the Unreliable
	// mode set.
	//
	invocation_mode	invo_mode(
				invocation_mode::ONEWAY |
				invocation_mode::NONBLOCKING |
				invocation_mode::UNRELIABLE);
	cmm_message	cmm_msg;

	//
	// Specifying INCN_UNKNOWN will send the message to current incn
	//
	ID_node	node(to_nodeid, INCN_UNKNOWN);

	//
	// Identify message resources
	//
#ifdef	CMM_VERSION_0
	resources	resource(resources::NO_FLOW_CONTROL, &e, invo_mode,
			0 /* header size */,
			(uint_t) (sizeof (comm_message_t) +
				sizeof (cmm::message_t)) /* data size */,
			node, NULL, is_old_version ? CMM_MSG : CMM1_MSG);
#else
	ASSERT(!is_old_version);
	resources	resource(resources::NO_FLOW_CONTROL, &e, invo_mode,
			0 /* header size */,
			(uint_t) (sizeof (comm_message_t) +
				sizeof (cmm::message_t)) /* data size */,
			node, NULL, CMM1_MSG);
#endif // CMM_VERSION_0

	//
	// Check whether the message can proceed now.
	// The send stream is allocated when the msg can proceed.
	//
	sendstream *send_stream = orb_msg::reserve_resources(&resource);
	if (send_stream == NULL) {
		// This can fail for COMM_FAILURE as well as WOULDBLOCK
		CL_PANIC(CORBA::COMM_FAILURE::_exnarrow(e.exception()) ||
			CORBA::WOULDBLOCK::_exnarrow(e.exception()));
		return (true);
	}
	cmm_msg.messagep = &tmp_msg;
	cmm_msg.comm_messagep = &comm_msg;
	cmm_msg._put(send_stream->get_MainBuf());
	CMM_DEBUG_TRACE(("===> orb_msg::send(%ld)\n", to_nodeid));
	orb_msg::send(send_stream);
	send_stream->done();
	// Return false if there is no exception, true if there is an exception
	return (e.exception() != NULL);
}

//
//	Cluster monitor sender thread.
//
// static
void
cmm_comm_impl::sender_thread_start(void *arg)
{
	cmm_comm_impl *commp = (cmm_comm_impl *)arg;
	commp->sender_thread();
	// thread exits
}

void
cmm_comm_impl::sender_thread()
{
	CMM_TRACE(("CMM sender_thread starting\n"));

	lock();
	for (;;) {
		switch (sender_thread_arg) {
		case SLEEP:
			sender_cv.wait(&lck);
			break;
		case DIE:
			goto die;
		case WAKEUP:
		default:
			broadcast_msg();
			//
			// If there is no more work to do, set arg to
			// SLEEP. Otherwise, wait for 0.3 second and retry.
			//
			if (destination_node_set.is_empty()) {
				sender_thread_arg = SLEEP;
			} else {
				os::systime retry_timeout;

				retry_timeout.setreltime(
				    (os::usec_t)MILLISEC * 300);

				CMM_DEBUG_TRACE(("CMM sender_thread "
					"timed sleep\n"));
				(void) sender_cv.timedwait(&lck,
					&retry_timeout);
			}
		}
	}
die:

	CMM_TRACE(("CMM sender_thread exiting\n"));
	CL_PANIC(_state == ABORTED);
	unlock();
	CORBA::release(_automp);
	_automp = cmm::automaton::_nil();
	cmm_impl::cmm_fini();
	// XXX We may already be unreferenced and deleted.  Who could be
	// holding another reference?
	revoke(); // revoke cmm_comm_impl
}

// Static, called from ORB::shutdown
void
cmm_comm_impl::shutdown()
{
	if (the_cmm_comm_impl == NULL) {
		return;
	}

	// Stop receiving callbacks from PM
	cmm_impl::unregister_with_pm();

	the_cmm_comm_impl->lock();
	the_cmm_comm_impl->signal_sender_thread(DIE);
	the_cmm_comm_impl->unlock();

	// Disable receiving of cmm messages
#ifdef	CMM_VERSION_0
	orb_msg::the().set_message_props(CMM_MSG, true,
	    orb_msg::THREADPOOL_ORBUTIL, NULL);
#endif // CMM_VERSION_0

	// Disable receiving of cmm version 1 messages
	orb_msg::the().set_message_props(CMM1_MSG, true,
	    orb_msg::THREADPOOL_ORBUTIL, NULL);
}

//
// Wake up the sender_thread.  The argument is defined as:
//
//	WAKEUP - do the normal "broadcast".
//	DIE - exit, the node is leaving the cluster.
//
void
cmm_comm_impl::signal_sender_thread(sender_thread_arg_t arg)
{
	CL_PANIC(lck.lock_held());
	if (arg == DIE) {
		_state = ABORTED;
	}
	//
	// Pass the word on to the sender_thread.
	//
	sender_thread_arg = arg;
	sender_cv.signal();
}
