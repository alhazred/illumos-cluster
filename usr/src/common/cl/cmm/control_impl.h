/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _CONTROL_IMPL_H
#define	_CONTROL_IMPL_H

#pragma ident	"@(#)control_impl.h	1.25	08/05/20 SMI"

#include <h/cmm.h>
#include <orb/object/adapter.h>

class cmm_impl;

class control_impl : public McServerof<cmm::control> {
private:
	//
	// Public interface
	//
public:
	control_impl(cmm_impl *cmm_implp);

	virtual		~control_impl();

	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//
	void    start(Environment&);
	void    stop(Environment&);
	void    abort(Environment&);
	void    reconfigure(Environment&);
	bool	enable_monitoring(Environment&);
	bool	disable_monitoring(Environment&);
	void	get_cluster_state(cmm::cluster_state_t&, Environment&);
	void	get_cluster_conf_info(cmm::cluster_info_t&, Environment&);
	bool	safe_to_takeover_from(nodeid_t, bool, Environment&);
	bool	cluster_shutdown(Environment&);
	bool	get_cluster_shutdown(Environment&);
	int 	cmm_getinitialviewnumber(nodeid_t, cmm::seqnum_t&,
		Environment&);
	bool	cw_disable_panic(Environment&);
	bool	cw_enable_panic(Environment&);

private:
	cmm_impl	*_cmmp;
};

#endif	/* _CONTROL_IMPL_H */
