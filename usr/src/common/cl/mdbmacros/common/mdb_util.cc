//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mdb_util.cc	1.3	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <mdb_util.h>

// Sufficient memory should be allocated by caller
// returns 0 if successful
// returns -1 if some memory read error occurs.
// returns 1 if not enough memory is allocated.
int
mdb_getstring(uintptr_t addr, char *str, size_t size)
{
	char ch;
	int i = 0;

	do {
		if (mdb_vread((void *)&ch, sizeof (ch), (uintptr_t)addr) != 1) {
			mdb_warn("failed read at address %p", addr);
			str[i] = 0;
			return (-1);
		}
		addr += 1;
		str[i++] = ch;
		if (i == (int)(size-1)) {
			str[i] = 0;
			return (1);
		}
	} while (ch != 0);
	return (0);
}
