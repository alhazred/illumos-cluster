/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1988 AT&T */
/*	  All Rights Reserved   */


#pragma ident	"@(#)mdb_os.cc	1.5	08/05/20 SMI"

#include <sys/os.h>
#include <sys/mdb_modapi.h>
#include <sys/varargs.h>
#include <sys/time.h>
#include <sys/cmn_err.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//
// This file has a couple of lint overrides for Warning 40 when using va_start
// This is because our lint does not understand the compiler builtin variable
// __builtin_va_alist.
//

// Returnt the internet address given a string of a decimal internet addres
in_addr_t
os::inet_addr(const char *cp)
{
	struct in_addr	foo;
	unsigned long	net, lna;

	// net and lna are in host byte order
	net = ::inet_network(cp);
	lna = ntohl(::inet_addr(cp));

	if ((net == (ulong_t)-1) || (lna == (ulong_t)-1))
		return ((uint_t)-1);

	// foo is in network byte order (as expected)
	foo = inet_makeaddr((in_addr_t)net, (in_addr_t)lna);
	return (foo._S_un._S_addr);
}

void
os::printf(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	mdb_printf(fmt, adx);
	va_end(adx);
}

int
os::snprintf(char *s, size_t n, const char *fmt, ...)
{
	va_list adx;
	int ret;

	va_start(adx, fmt);		//lint !e40
	ret = (int)::vsnprintf(s, n, fmt, adx);
	va_end(adx);
	return (ret);
}

void
os::prom_printf(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	mdb_printf(fmt, adx);
	va_end(adx);
}

void
os::panic(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	mdb_printf("PANIC--->  ");
	mdb_printf(fmt, adx);
	va_end(adx);
}

void
os::warning(const char *fmt, ...)
{
	va_list adx;

	va_start(adx, fmt);		//lint !e40
	mdb_printf(fmt, adx);
	va_end(adx);
}

os::sc_syslog_msg::sc_syslog_msg(const char	*,
				const char	*,
				void	*)
{
	msg_handle = NULL;
}

sc_syslog_msg_status_t
os::sc_syslog_msg::log(int, sc_event_type_t, const char	*format, ...)
{
	va_list	ap;
	sc_syslog_msg_status_t status = SC_SYSLOG_MSG_STATUS_GOOD;

	va_start(ap, (void *)format);		//lint !e40
	mdb_printf(format, ap);
	va_end(ap);

	return (status);
}

os::sc_syslog_msg::~sc_syslog_msg()
{
	msg_handle = NULL;		// for lint
}

void *operator
new(galsize_t len)
{
	return (mdb_alloc((size_t)len, UM_SLEEP));
}
void operator
delete(void *ptr)
{
	if (ptr != NULL)
		mdb_free(ptr, (unsigned long)4);
}


int
os::atoi(const char *p)
{
	int n;
	int c, neg = 0;

	if (!os::ctype::is_digit((int)(c = *p))) {
		while (os::ctype::is_space(c)) {
			c = *++p;
		}
		switch (c) {
		case '-':
			neg++;
			/* FALLTHROUGH */
		case '+':
			c = *++p;
			break;
		default:
			break;
		}
		if (!os::ctype::is_digit(c))
			return (0);
	}
	for (n = '0' - c; os::ctype::is_digit(c = *++p); ) {
		n *= 10; /* two steps to avoid unnecessary overflow */
		n += '0' - c; /* accum neg to avoid surprises at MAX */
	}
	return (neg ? n : -n);
}

int
os::itoa(int x, char *buffer, uint_t base)
{
	char local[80];
	static const char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";

	uint_t n, rem, mod;

	n = sizeof (local) - 1;

	local[n] = 0;

	if ((base > 35) || (base == 0))
		return (NULL);

	if (x < 0) {
		rem = (uint_t)-x;
	} else {
		rem = (uint_t)x;
	}

	do {
		switch (base) {
		case 2:
			mod = rem & 1;
			rem = rem >> 1;
			break;
		case 10:
			mod = rem % 10;
			rem = rem / 10;
			break;

		case 16:
			mod = rem & 15;
			rem = rem >> 4;
			break;
		default:
			mod = rem % base;
			rem = rem / base;
			break;
		}
		local[--n] = digits[mod];
	} while (rem != 0);

	if (x < 0) {
		local[--n] = '-';
	}

	(void) os::strcpy(buffer, local + n);

	return ((int)(sizeof (local) - n - 1));
}

size_t
os::strlen(const char *str)
{
	return (::strlen(str));
}

char *
os::strcpy(char *dst, const char *src)
{
	return (::strcpy(dst, src));
}

char *
os::strncpy(char *dst, const char *src, size_t n)
{
	return (::strncpy(dst, src, n));
}

char *
os::strdup(const char *string)
{
	if (string == NULL) {
		return (NULL);
	}
	char *s = new char[::strlen(string)+1];
	if (s == NULL) {
		return (NULL);
	}
	return (::strcpy(s, string));
}

int
os::strcmp(const char *s1, const char *s2)
{
	return (::strcmp(s1, s2));
}

int
os::strncmp(const char *s1, const char *s2, size_t n)
{
	return (::strncmp(s1, s2, n));
}

//
// Copied entirely from libc.
//
char *
os::strstr(const char *as1, const char *as2)
{
	const char *s1, *s2;
	const char *tptr;
	char c;

	s1 = as1;
	s2 = as2;

	if (s2 == NULL || *s2 == '\0')
		return ((char *)s1);
	c = *s2;

	while (*s1)
		if (*s1++ == c) {
			tptr = s1;
			while ((c = *++s2) == *s1++ && c)
				;
			if (c == 0)
				return ((char *)tptr - 1);
			s1 = tptr;
			s2 = as2;
			c = *s2;
		}

	return (NULL);
}

void
os::sprintf(char *sbuf, const char *fmt, ...)
{
	va_list ap;

	// lint complains of builtin variable __builtin_va_alist
	va_start(ap, fmt);			/*lint !e40 */
	(void) vsprintf(sbuf, fmt, ap);
	va_end(ap);
}
