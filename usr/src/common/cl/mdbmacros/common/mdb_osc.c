/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_osc.c	1.3	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/types.h>

void
atomic_add_32(uint32_t *target, int32_t delta)
{
	*target = (uint32_t)((int32_t)*target + delta);
}
uint32_t
atomic_add_32_nv(uint32_t *target, int32_t delta)
{
	*target = (uint32_t)((int32_t)*target + delta);
	return (*target);
}

int
assfail(const char *a, const char *f, int l)
{
	mdb_printf("%s %s %d\n", a, f, l);
	return (0);
}

void
_pure_error_()
{
	mdb_printf("pure virtual function called");
}

/*
 * This is used in ASSERT and CL_PANIC definitions. The assignment
 * here will result in the behavior defined by the assfail function
 * definition.
 */
int (*assertfunc)(const char *, const char *, int) = assfail;
