/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_init.c	1.23	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/sysinfo.h>
#include "mdb_cluster.h"

/*
 * MDB module linkage information:
 *
 * We declare a list of structures describing our dcmds, and a function
 * named _mdb_init to return a pointer to our module information.
 *
 * Any new dcmd must be added to the dcmds[].
 *
 */

static const mdb_dcmd_t dcmds[] = {
	/* dcmd_name, args, description, function_ptr */
#if defined(_KERNEL)
/*	{ "fs_server_files_v0", NULL,
	    "print server file objects for a fs_norm_impl or fs_repl_impl",
	    fs_server_files_v0, NULL}, */
	{ "fs_server_files_v1", NULL,
	    "print server file objects for a fs_norm_impl or fs_repl_impl",
	    fs_server_files_v1, NULL},
/*	{ "fs_client_files_v0", NULL, "print client file objects for a pxvfs",
	    fs_client_files_v0, NULL}, */
	{ "fs_client_files_v1", NULL, "print client file objects for a pxvfs",
	    fs_client_files_v1, NULL},
#endif
#if defined(_KERNEL) || defined(_KERNEL_ORB)
	{ "hxdoor_table", "[-s svcid]", "print hxdoors on this node ",
	    hxdoor_table, NULL},
	{ "rxdoor_table", NULL, "print rxdoor_table", rxdoor_table, NULL},
	{ "infra", NULL, "print infrastructure file", mdb_infra, NULL},
	{ "paths", "-v", "print paths from this node", mdb_paths, NULL},
	{ "peinfo", NULL, "print pathend info", pe_info, NULL},
	{ "hbtinfo", NULL, "print heartbeat threadpool info", hbt_info, NULL},
	{ "pminfo", NULL, "print path manager info", pm_info, NULL},
	{ "kstat", NULL, "print kstat info", ks_info, NULL},
	{ "tr_out_info",
	    NULL,
	    "print tr_out_ids of xdoor translated out to other nodes",
	    tr_out_info_print, NULL},
	{ "vpfiles", "[vp_name]", "print local version protocol file info",
	    vp_info, NULL},
	{ "vmstream", NULL, "print version manager stream info",
	    stream_info, NULL},
#else
	{ "rgm_state", "[-v]", "print rgm state structure", rgm_state, NULL},
	{ "rgm_rg", "[-v]", "print a resource group", rgm_rg, NULL},
	{ "rgm_rs", "[-v]", "print a resource", rgm_rs, NULL},
	{ "rgm_rt", "[-v]", "print a resource type", rgm_rt, NULL},
	{ "rgm_buf", "[-v]", "print the rgmd debug buffer",  rgm_buf, NULL},
#endif
	{ NULL }
};

static const mdb_walker_t walkers[] = {
#if defined(_KERNEL) || defined(_KERNEL_ORB)
	{"simple_kstat", "walk list of kstat structs",
	    ks_walk_init, ks_walk_step, ks_walk_fini, NULL },
#else
	{ "rg", "walk the list of resource groups",
	    rgm_rg_walk_init, rgm_rg_walk_step, rgm_rg_walk_fini, NULL },
	{ "rs", "walk the list of resources",
	    rgm_rs_walk_init, rgm_rs_walk_step, rgm_rs_walk_fini, NULL },
	{ "rt", "walk the list of resource types",
	    rgm_rt_walk_init, rgm_rt_walk_step, rgm_rt_walk_fini, NULL },
#endif
	{ NULL }
};

static const mdb_modinfo_t modinfo = {
	MDB_API_VERSION, dcmds, walkers
};

const mdb_modinfo_t *
_mdb_init(void)
{
	return (&modinfo);
}

void
_mdb_fini(void)
{
}
