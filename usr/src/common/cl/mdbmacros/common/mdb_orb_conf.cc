//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mdb_orb_conf.cc	1.5	08/05/20 SMI"

#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <sys/mdb_modapi.h>

#ifdef _KERNEL
#include <sys/disp.h>
#endif


// global variable declaration
#ifdef _KERNEL_ORB
ID_node orb_conf::current_node;
#else	// _KERNEL_ORB
nodeid_t orb_conf::current_node = NODEID_UNKNOWN;
#endif	// _KERNEL_ORB

pri_t orb_conf::maxpri = 0;
const char *orb_conf::rt_clname = NULL;

#ifdef _KERNEL_ORB
//
// In order to catch cases where nodes with incompatible builds are trying
// to form a cluster, we define the following strings during compilation
// A version string make up of the concatenation of the these is exchanged
// during initial path setup in the transports
// XX We can make this check more incompatibilities as we discover them
//

#endif

nodeid_t
clconf_get_nodeid()
{
	nodeid_t mynodeid;
	if (mdb_readsym((uintptr_t *)&mynodeid, sizeof (mynodeid), "mynodeid")
	    != (int)sizeof (mynodeid)) {
		mdb_warn("failed to read mynodeid\n");
		return (NODEID_UNKNOWN);
	}
	return (mynodeid);
}
int
orb_conf::configure()
{
	current_node.ndid = clconf_get_nodeid();
	if (current_node.ndid == NODEID_UNKNOWN) {
		return (EINVAL);
	}

	return (0);
}

// XX This code will go away once the tests are cleaned up
#if defined(_FAULT_INJECTION) && defined(_KERNEL_ORB)
nodeid_t
orb_conf::get_root_nodeid()
{
	// Since we dont support this in the CCR emulate this as
	// returning the lowest nodeid that is configured
	return (NODEID_UNKNOWN);
}
#endif

//
// Is the specified node configured cluster node
//
bool
orb_conf::node_configured(nodeid_t)
{
	return (false);
}
