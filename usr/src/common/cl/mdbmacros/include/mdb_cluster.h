/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MDB_CLUSTER_H
#define	_MDB_CLUSTER_H

#pragma ident	"@(#)mdb_cluster.h	1.18	08/05/20 SMI"

#if __cplusplus
extern "C" {
#endif

/*
 * This file contains prototypes for all functions that are called
 * by mdb in order to execute a dcmd or a walker.
 */

#if defined(_KERNEL)
extern int fs_server_files_v0(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int fs_server_files_v1(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int fs_client_files_v0(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int fs_client_files_v1(uintptr_t, uint_t, int, const mdb_arg_t *);
#endif

#if defined(_KERNEL) || defined(_KERNEL_ORB)
extern int hxdoor_table(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int rxdoor_table(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int mdb_infra(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int mdb_paths(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int pe_info(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int hbt_info(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int pm_info(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int ks_info(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int vp_info(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int stream_info(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int ks_walk_init(mdb_walk_state_t *wsp);
extern int ks_walk_step(mdb_walk_state_t *wsp);
extern void ks_walk_fini(mdb_walk_state_t *wsp);
extern int tr_out_info_print(uintptr_t, uint_t, int, const mdb_arg_t *);
#else
extern int rgm_state(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int rgm_rg(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int rgm_rs(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int rgm_rt(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int rgm_buf(uintptr_t, uint_t, int, const mdb_arg_t *);
extern int rgm_rg_walk_init(mdb_walk_state_t *);
extern int rgm_rg_walk_step(mdb_walk_state_t *);
extern void rgm_rg_walk_fini(mdb_walk_state_t *);
extern int rgm_rs_walk_init(mdb_walk_state_t *);
extern int rgm_rs_walk_step(mdb_walk_state_t *);
extern void rgm_rs_walk_fini(mdb_walk_state_t *);
extern int rgm_rt_walk_init(mdb_walk_state_t *);
extern int rgm_rt_walk_step(mdb_walk_state_t *);
extern void rgm_rt_walk_fini(mdb_walk_state_t *);
#endif

#if __cplusplus
}
#endif
#endif	/* _MDB_CLUSTER_H */
