/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  dbg_knewdel_in.h
 *
 */

#ifndef _DBG_KNEWDEL_IN_H
#define	_DBG_KNEWDEL_IN_H

#pragma ident	"@(#)dbg_knewdel_in.h	1.4	08/05/20 SMI"

#ifdef _KERNEL
#include <sys/mdb_modapi.h>

// We do debug check to verify that we are not doing blocking kmem allocations
// in nonblocking contexts
// XX We would like to say NB_ALL here to also catch cases where
// we do blocking memory allocations in _unreferenced. However,
// due to bugid 4247115, we cannot yet.
#define	NB_CHECK_LEVEL	(os::envchk::NB_INTR | os::envchk::NB_INVO)

inline void *
knewdel::operator new(galsize_t size, os::mem_alloc_type flag)
{
	int mdb_flag = UM_SLEEP;

	if (flag == os::NO_SLEEP)
		mdb_flag = UM_NOSLEEP;
	return (mdb_alloc((size_t)size, (int)flag));
}

inline void *
knewdel::operator new(galsize_t size, os::mem_zero_type zflag)
{
	if (zflag == os::ZERO)
		return (mdb_zalloc((size_t)size, UM_SLEEP));
	else
		return (mdb_alloc((size_t)size, UM_SLEEP));
}

inline void *
knewdel::operator new(galsize_t size, os::mem_alloc_type flag,
	os::mem_zero_type zflag)
{
	int mdb_flag = UM_SLEEP;

	if (flag == os::NO_SLEEP)
		mdb_flag = UM_NOSLEEP;

	if (zflag == os::ZERO)
		return (mdb_zalloc((size_t)size, mdb_flag));
	else
		return (mdb_alloc((size_t)size, mdb_flag));
}

inline void
knewdel::operator delete(void *ptr, galsize_t size)
{
	mdb_free(ptr, (size_t)size);
}

#endif  /* _KERNEL */

#endif	/* _DBG_KNEWDEL_IN_H */
