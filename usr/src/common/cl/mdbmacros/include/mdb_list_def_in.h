/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  mdb_list_def_in.h
 *
 */

#ifndef _MDB_LIST_DEF_IN_H
#define	_MDB_LIST_DEF_IN_H

#pragma ident	"@(#)mdb_list_def_in.h	1.4	08/05/20 SMI"

#include <sys/list_def.h>

template <class T>
inline T *
DList<T>::ListIterator::mdb_get_current() const
{
	_DList::ListElem *el;

	if (_current == NULL)
		return ((T *) NULL);
	else {
		el = (_DList::ListElem *)mdb_alloc(sizeof (_DList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)el, sizeof (_DList::ListElem),
		    (uintptr_t)_current) != sizeof (_DList::ListElem)) {
			mdb_warn("failed to read the element\n");
			return ((T *) NULL);
		}
		return ((T *)el->elem());
	}
}

template <class T>
inline T *
SList<T>::ListIterator::mdb_get_current() const
{
	_SList::ListElem *el;

	if (_current == NULL)
		return ((T *) NULL);
	else {
		el = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
								UM_SLEEP);
		if (mdb_vread((void *)el, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("failed to read the element\n");
			return ((T *) NULL);
		}
		return ((T *)el->elem());
	}
}

template <class T>
inline void
DList<T>::ListIterator::mdb_advance()
{
	_DList::ListElem *el;

	if (_current != NULL) {
		el = (_DList::ListElem *)mdb_alloc(sizeof (_DList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)el, sizeof (_DList::ListElem),
		    (uintptr_t)_current) != sizeof (_DList::ListElem)) {
			mdb_warn("failed to read the element\n");
			return;
		}
		_current = el->next();
	}
}

template <class T>
inline void
SList<T>::ListIterator::mdb_advance()
{
	_SList::ListElem *el;

	if (_current != NULL) {
		el = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)el, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("failed to read the element\n");
			return;
		}
		_current = el->next();
	}
}

#endif	/* _MDB_LIST_DEF_IN_H */
