/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  paths_list_defs_in.h
 *
 */

#ifndef _PATHS_LIST_DEF_IN_H
#define	_PATHS_LIST_DEF_IN_H

#pragma ident	"@(#)paths_list_def_in.h	1.5	08/05/20 SMI"

#include <sys/list_def.h>

//
// Due to difficulties/oddities of generating this templated code for mdb
// specifically, these versions are explicitly defined.
//

void
IntrList<path_group, _SList>::ListIterator::mdb_advance()
{
	_SList::ListElem *elemp;

	if (_current != NULL) {
		elemp = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("intr get current: failed to read path_group "
			    "element\n");
			mdb_free(elemp, sizeof (_SList::ListElem));
			return;
		}
		_current = elemp->next();

		mdb_free(elemp, sizeof (_SList::ListElem));
	}
}

path_group*
IntrList<path_group, _SList>::ListIterator::mdb_get_current() const
{
	_SList::ListElem *elemp;

	if (_current == NULL) {
		return ((path_group *) NULL);
	} else {
		elemp = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("intr get current: failed to read path_group "
			    "element\n");
			mdb_free(elemp, sizeof (_SList::ListElem));
			return ((path_group *) NULL);
		}
		path_group *pgpp = (path_group *)elemp->elem();
		mdb_free(elemp, sizeof (_SList::ListElem));

		return (pgpp);
	}
}

void
IntrList<pathend, _SList>::ListIterator::mdb_advance()
{
	_SList::ListElem *elemp;

	if (_current != NULL) {
		elemp = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("intr get current: failed to read pathend "
			    "element\n");
			mdb_free(elemp, sizeof (_SList::ListElem));
			return;
		}
		_current = elemp->next();

		mdb_free(elemp, sizeof (_SList::ListElem));
	}
}

pathend*
IntrList<pathend, _SList>::ListIterator::mdb_get_current() const
{
	_SList::ListElem *elemp;

	if (_current == NULL) {
		return ((pathend *) NULL);
	} else {
		elemp = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("intr get current: failed to read pathend "
			    "element\n");
			mdb_free(elemp, sizeof (_SList::ListElem));
			return ((pathend *) NULL);
		}
		pathend *pp = (pathend *)elemp->elem();
		mdb_free(elemp, sizeof (_SList::ListElem));
		return (pp);
	}
}

void
IntrList<pm_client, _SList>::ListIterator::mdb_advance()
{
	_SList::ListElem *elemp;

	if (_current != NULL) {
		elemp = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("intr get current: failed to read pm_client "
			    "element\n");
			mdb_free(elemp, sizeof (_SList::ListElem));
			return;
		}
		_current = elemp->next();

		mdb_free(elemp, sizeof (_SList::ListElem));
	}
}

pm_client*
IntrList<pm_client, _SList>::ListIterator::mdb_get_current() const
{
	_SList::ListElem *elemp;

	if (_current == NULL) {
		return ((pm_client *) NULL);
	} else {
		elemp = (_SList::ListElem *)mdb_alloc(sizeof (_SList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_SList::ListElem),
		    (uintptr_t)_current) != sizeof (_SList::ListElem)) {
			mdb_warn("intr get current: failed to read pm_client "
			    "element\n");
			mdb_free(elemp, sizeof (_SList::ListElem));
			return ((pm_client *) NULL);
		}
		pm_client *pmcp = (pm_client *)elemp->elem();
		mdb_free(elemp, sizeof (_SList::ListElem));

		return (pmcp);
	}
}

#endif	/* _PATHS_LIST_DEF_IN_H */
