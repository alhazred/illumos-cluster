/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MDB_CLCONF_IO_H
#define	_MDB_CLCONF_IO_H

#pragma ident	"@(#)mdb_clconf_io.h	1.7	08/05/20 SMI"

#include <clconf/clconf_io.h>
#include <clconf/clnode.h>

// The io interface class that reads the configuration data using
// CCR FILE interface.
class mdb_clconf_io : public clconf_io {
public:
	// Initialize the clconf tree. It will read from CCR, construct
	// the tree. Returns true on success, and false if fail.
	bool initialize();

	void shutdown();

	mdb_clconf_io(bool);
	~mdb_clconf_io();

	// for read.
	clconf_io::status open();
	void close();
	clconf_io::status read_one(char **, char **);

	// Readonly interface. No write.
	bool begin_transaction();
	bool commit_transaction();
	void abort_transaction();
	bool write(const char *, const char *);
	void *create_seq();
	void delete_seq(void *);
	int write_seq(void *);
	int32_t get_version();
	void print();

private:

	char **entries;
	int pos;
	uint_t count;
	// Stuff to support multiple threads calling initialize
	bool is_initialized;
	bool alloc_entries;
	os::mutex_t lck;

	mdb_clconf_io(const mdb_clconf_io &);
	mdb_clconf_io & operator = (const mdb_clconf_io &);

};

#endif	/* _MDB_CLCONF_IO_H */
