/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MDB_NAMES_H
#define	_MDB_NAMES_H

#pragma ident	"@(#)mdb_names.h	1.13	08/05/20 SMI"

/*
 * Mangled names used as arguments to mdb_readsym().
 * Prior to using the Forte Developer 7 compiler we
 * used SC4.2 mangled names.  We now use standard mode
 * in the kernel, which changes name mangling.
 *
 * This file maintains all mangled names used in the gate.
 */

#ifdef __cplusplus
extern "C" {
#endif

#define	MDB_G_VTAB "G__vtbl"

#define	MDB_THE_HEARTBEAT_THREADPOOL \
	"__1cNhb_threadpoolYthe_heartbeat_threadpool_"
#define	MDB_THE_PATH_MANAGER \
	"__1cMpath_managerQthe_path_manager_"
#define	MDB_THE_CL_CURRENT_TREE \
	"__1cPcl_current_treeTthe_cl_current_tree_"
#define	MDB_THE_RMAP \
	"__1cDrmaIthe_rmap_"
#define	MDB_CURRENT_NODE \
	"__1cIorb_confMcurrent_node_"
#define	MDB_THE_RXDOOR_MANAGER \
	"__1cOrxdoor_managerSthe_rxdoor_manager_"
#define	MDB_THE_TRANSLATE_MANAGER \
	"__1cNtranslate_mgrRthe_translate_mgr_"
#define	MDB_THE_VM_COMM \
	"__1cHvm_commLthe_vm_comm_"

#define	MDB_PXFOBJ_HASH "__1cFpxvfsLpxfobj_hash_"
#define	MDB_PXFOBJHSZ "__1cFpxvfsJpxfobjhsz_"

#ifdef __cplusplus
}
#endif

#endif	/* _MDB_NAMES_H */
