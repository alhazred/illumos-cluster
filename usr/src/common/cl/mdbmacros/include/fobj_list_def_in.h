//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _FOBJ_LIST_DEF_IN_H
#define	_FOBJ_LIST_DEF_IN_H

#pragma ident	"@(#)fobj_list_def_in.h	1.6	08/05/20 SMI"

#include <sys/list_def.h>

#include <pxfs/server/fobj_impl.h>

//
// Due to difficulties/oddities of generating this templated code for mdb
// specifically, these versions are explicitly defined.
//

void
IntrList<fobj_ii, _DList>::ListIterator::mdb_advance()
{
	_DList::ListElem	*elemp;

	if (_current != NULL) {
		elemp = (_DList::ListElem *)mdb_alloc(sizeof (_DList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_DList::ListElem),
		    (uintptr_t)_current) != sizeof (_DList::ListElem)) {
			mdb_warn("IntrList advance: failed to read "
			    "element %p\n", _current);
			mdb_free(elemp, sizeof (_DList::ListElem));
			return;
		}
		_current = elemp->next();

		mdb_free(elemp, sizeof (_DList::ListElem));
	}
}

fobj_ii *
IntrList<fobj_ii, _DList>::ListIterator::mdb_get_current()
    const
{
	_DList::ListElem	*elemp;

	if (_current == NULL) {
		return ((fobj_ii *) NULL);
	} else {
		elemp = (_DList::ListElem *)mdb_alloc(sizeof (_DList::ListElem),
		    UM_SLEEP);
		if (mdb_vread((void *)elemp, sizeof (_DList::ListElem),
		    (uintptr_t)_current) != sizeof (_DList::ListElem)) {
			mdb_warn("IntrList get current: failed to read "
			    "element %p\n", _current);
			mdb_free(elemp, sizeof (_DList::ListElem));
			return ((fobj_ii *) NULL);
		}
		fobj_ii	*fobj_iip = (fobj_ii *)elemp->elem();

		mdb_free(elemp, sizeof (_DList::ListElem));

		return (fobj_iip);
	}
}

#endif	// _FOBJ_LIST_DEF_IN_H
