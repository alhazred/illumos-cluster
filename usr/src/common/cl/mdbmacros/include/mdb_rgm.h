/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MDB_RGM_H
#define	_MDB_RGM_H

#pragma ident	"@(#)mdb_rgm.h	1.11	08/08/01 SMI"

#include <sys/mdb_modapi.h>
#include <mdb_cluster.h>
#include <rgm/rgm_common.h>
#include <rgm_state.h>
#include <rgm_proto.h>
#include <rgm_logical_nodeset.h>
#include <rgm/scha_strings.h>
#include <h/rgm.h>
#include <cmm/ucmm_api.h>

/*
 * This file contains declarations for the main functions
 * in the MDB module for the RGMD.
 */

#ifdef	__cplusplus
extern "C" {
#endif

#define	DEFBUFSIZE	4096			/* size of default buffer */
#define	DEFINDENT	(unsigned long) 0x2	/* size of default indent */

/* The global rgm state structure */
extern rgm_state_t *Rgm_state;	/* ptr to module's copy of state structure */
extern uintptr_t Rgm_state_addr;	/* rgmd's Rgm_state virtual address */

/* ptr to module's copy of lnManager */
extern LogicalNodeManager *lnManagerPtr;

/* printing variables */
extern char rgm_defbuf[DEFBUFSIZE];	/* default buffer for temp results */
extern int rgm_verbose;			/* enables debugging messages */

/* Note that support for the "printbrief" option is not yet implemented */
void rgm_print_state(boolean_t printbrief);
void rgm_print_rgs(uintptr_t addr, boolean_t printall, boolean_t printbrief);
void rgm_print_rts(uintptr_t addr, boolean_t printall, boolean_t printbrief);
void rgm_print_rss(uintptr_t addr, boolean_t printall, boolean_t printbrief);

int rgm_get_state(uintptr_t addr);
int rgm_get_lnManager();
void rgm_free_lnManager();

void rgm_get_node_name(char *nbuf, size_t bufsize);
void rgm_get_node_id(sol::nodeid_t *nid, size_t bufsize);

void rgm_get_logical_nodeset(LogicalNodeset *nset);

#ifdef	__cplusplus
}
#endif

#endif	/* _MDB_RGM_H */
