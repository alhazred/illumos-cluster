/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  dbg_os_in.h
 *
 */

#ifndef _DBG_OS_IN_H
#define	_DBG_OS_IN_H

#pragma ident	"@(#)dbg_os_in.h	1.6	08/05/20 SMI"

// These are the inline functions which work in both the kernel and
// in userland.  Kernel only functions are in sys/kos_in.h, and userland
// only functions are in sys/uos_in.h
#include <sys/mdb_modapi.h>

// In sys/os.h, galsize_t is only defined for the kernel.  We need to pick
// up this definition in userland in order to make use of the definitions
// of 'new' below.  Currently, no userland mdb modules make use of 'new'.
// The definitions below allow for other included code (ie, alloc_buffer
// in orb/buffers/Buf_in.h) to use 'new' with multiple arguments without
// compilation errors.
#ifndef _KERNEL
#if defined(_LP64) || !defined(_I32LPx)
typedef size_t galsize_t;
#else
typedef uint_t galsize_t;
#endif
#endif // _KERNEL

inline void *
/*CSTYLED*/
operator new(galsize_t len, os::mem_zero_type flag)
{
	if (flag == os::ZERO)
		return (mdb_zalloc((size_t)len, UM_SLEEP));
	else
		return (mdb_alloc((size_t)len, UM_SLEEP));
}

inline void *
/*CSTYLED*/
operator new(galsize_t len, os::mem_alloc_type flag)
{
	int mdb_flag = UM_SLEEP;

	if (flag == os::NO_SLEEP)
		mdb_flag = UM_NOSLEEP;
	return (mdb_alloc((size_t)len, mdb_flag));
}

inline void *
/*CSTYLED*/
operator new(galsize_t len, os::mem_alloc_type flag, os::mem_zero_type zflag)
{
	int mdb_flag = UM_SLEEP;

	if (flag == os::NO_SLEEP)
		mdb_flag = UM_NOSLEEP;
	if (zflag == os::ZERO)
		return (mdb_zalloc((size_t)len, mdb_flag));
	else
		return (mdb_alloc((size_t)len, mdb_flag));
}

//
// os::mutex_t
//
inline
os::mutex_t::mutex_t()
{
}

inline
os::mutex_t::~mutex_t()
{
}

inline void
os::mutex_t::lock()
{
}

inline int
os::mutex_t::try_lock()
{
	return (0);
}

inline void
os::mutex_t::unlock()
{
}

inline int
os::mutex_t::lock_held()
{
	return (0);
}

inline int
os::mutex_t::lock_not_held()
{
	return (0);
}

inline uint64_t
os::mutex_t::owner()
{
	return (0);
}


//
// os::rwlock_t
//
inline
os::rwlock_t::rwlock_t()
{
}

inline
os::rwlock_t::~rwlock_t()
{
}

inline void
os::rwlock_t::rdlock()
{
}

inline void
os::rwlock_t::wrlock()
{
}

inline int
os::rwlock_t::try_rdlock()
{
	return (0);
}

inline int
os::rwlock_t::try_wrlock()
{
	return (0);
}

inline void
os::rwlock_t::unlock()
{
}

//
// os::sem_t
//
inline
os::sem_t::sem_t(uint_t)
{
}

inline
os::sem_t::sem_t()
{
}

inline
os::sem_t::~sem_t()
{
}

inline void
os::sem_t::p()
{
}

inline int
os::sem_t::tryp()
{
	return (0);
}

inline void
os::sem_t::v()
{
}

inline void
os::systime::set(os::systime &other)
{
	_time = other._time;
}

//
// Methods for character type derivation
//
inline int
os::ctype::is_digit(int c)
{
	return ((c >= '0') && (c <= '9'));
}

inline int
os::ctype::is_alpha(int c)
{
	return (
	    (c >= 'a' && c <= 'z') ||
	    (c >= 'A' && c <= 'Z'));
}

inline int
os::ctype::is_xdigit(int c)
{
	return (
	    (c >= '0' && c <= '9') ||
	    (c >= 'a' && c <= 'f') ||
	    (c >= 'A' && c <= 'F'));
}

inline int
os::ctype::is_lower(int c)
{
	return ((c >= 'a') && (c <= 'z'));
}

inline int
os::ctype::is_upper(int c)
{
	return ((c >= 'A') && (c <= 'Z'));
}

inline int
os::ctype::is_space(int c)
{
	return (
	    c == ' ' || c == '\t' || c == '\n' ||
	    c == '\r' || c == '\v' || c == '\f');
}

#endif	/* _DBG_OS_IN_H */
