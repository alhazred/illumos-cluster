/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_rgm_walk.cc	1.4	08/05/20 SMI"

#include "mdb_rgm.h"

/*
 * This file contains the walkers for the rgm object types: RG, RS, and RT.
 *
 * More information on the MDB module for the RGMD is in mdb_rgm_readme.txt
 */


/*
 * Resource Group Walker
 */

/*
 * Initialize the RG walker by either using the given starting address,
 * or reading the value of the Rgm_state's RG list pointer.  We also allocate
 * an rglist_t for RG storage, and save this using the walk_data pointer.
 */
int
rgm_rg_walk_init(mdb_walk_state_t *wsp)
{
	if (wsp->walk_addr == NULL && Rgm_state == NULL &&
	    rgm_get_state(NULL) != 0)
		return (WALK_ERR);

	/* If we don't have a starting RG, use the first RG from Rgm_state. */
	if (wsp->walk_addr == NULL)
		wsp->walk_addr = (uintptr_t)Rgm_state->rgm_rglist;

	wsp->walk_data = mdb_alloc(sizeof (rglist_t), UM_SLEEP);
	return (WALK_NEXT);
}

/*
 * At each step, read an RG into our private storage, and then invoke
 * the callback function.  We terminate when we reach a NULL rgl_next pointer.
 */
int
rgm_rg_walk_step(mdb_walk_state_t *wsp)
{
	int status;

	if (wsp->walk_addr == NULL)
		return (WALK_DONE);

	if (mdb_vread(wsp->walk_data,
	    sizeof (rglist_t), wsp->walk_addr) == -1) {
		mdb_warn("failed to read rg at %p", wsp->walk_addr);
		return (WALK_DONE);
	}

	status = wsp->walk_callback(wsp->walk_addr, wsp->walk_data,
	    wsp->walk_cbdata);

	wsp->walk_addr = (uintptr_t)(((rglist_p_t)wsp->walk_data)->rgl_next);
	return (status);
}

/*
 * The walker's fini function is invoked at the end of each walk.  Since we
 * dynamically allocated an rglist_t in rg_walk_init, we must free it now.
 */
void
rgm_rg_walk_fini(mdb_walk_state_t *wsp)
{
	mdb_free(wsp->walk_data, sizeof (rglist_t));
}


/*
 * Resource Walker
 */

/* Constants for handling "nested" RS walk's step routine */
#define	RS_WALK_LAYERED			(void *) 1
#define	RS_WALK_NON_LAYERED		(void *) 2

/*
 * Initialize the RS walker by either using the given starting address (for
 * a NON-LAYERED, local resource walk, which will cover only the resources
 * located in the same RG) or by doing a LAYERED walk over all RGs to walk over
 * all of the resources in each RG.  We also allocate an rlist_t for RS
 * storage, and save this using the walk_data pointer.
 */
int
rgm_rs_walk_init(mdb_walk_state_t *wsp)
{
	/*
	 * If we don't have a starting RS, then we will walk through the
	 * RGs to get all resources.  Set the private walk_arg to indicate
	 * we are doing a LAYERED walk.
	 */
	if (wsp->walk_addr == NULL) {
		wsp->walk_arg = RS_WALK_LAYERED;

		if (mdb_layered_walk("rg", wsp) == -1) {
			mdb_warn("failed to layer rs walk on rg walk");
			return (WALK_ERR);
		}
	} else
		wsp->walk_arg = RS_WALK_NON_LAYERED;

	wsp->walk_data = mdb_alloc(sizeof (rlist_t), UM_SLEEP);
	return (WALK_NEXT);
}

/*
 * At each step, read an RS into our private storage, and then invoke
 * the callback function.
 *
 * However, if this is a LAYERED walk (walk_arg == RS_WALK_LAYERED), we
 * do all resources within the RG in a single call to the step function.  To
 * accomplish this, we manually iterate through all the resources of the
 * underlying RG in a single call to the step function.
 */
int
rgm_rs_walk_step(mdb_walk_state_t *wsp)
{
	int status;

	/*
	 * For a layered walk, we look for resources in the RG of our
	 * underlying, layered walk.
	 */
	if (wsp->walk_arg == RS_WALK_LAYERED) {
		rglist_p_t rgp = (rglist_p_t)wsp->walk_layer;
		wsp->walk_addr = (uintptr_t)rgp->rgl_resources;
	}

step_again:
	if (wsp->walk_addr == NULL) {
		/* For LAYERED walks, we keep looking for Rs in the next RG. */
		if (wsp->walk_arg == RS_WALK_LAYERED)
			return (WALK_NEXT);
		else
			return (WALK_DONE);
	}

	if (mdb_vread(wsp->walk_data, sizeof (rlist_t), wsp->walk_addr) == -1) {
		mdb_warn("failed to read rs at %p", wsp->walk_addr);
		return (WALK_DONE);
	}

	status = wsp->walk_callback(wsp->walk_addr, wsp->walk_data,
	    wsp->walk_cbdata);

	wsp->walk_addr = (uintptr_t)(((rlist_t *)wsp->walk_data)->rl_next);

	/* For LAYERED walks, step again for the next R in this RG. */
	if (status == WALK_NEXT && wsp->walk_arg == RS_WALK_LAYERED)
		goto step_again;

	return (status);
}

/*
 * The walker's fini function is invoked at the end of each walk.  Since we
 * dynamically allocated an rlist_t in rs_walk_init, we must free it now.
 */
void
rgm_rs_walk_fini(mdb_walk_state_t *wsp)
{
	mdb_free(wsp->walk_data, sizeof (rlist_t));
}


/*
 * Resource Type Walker
 */

/*
 * Initialize the RT walker by either using the given starting address,
 * or reading the value of the Rgm_state's RT list pointer.  We also allocate
 * an rgm_rt_t for RT storage, and save this using the walk_data pointer.
 */
int
rgm_rt_walk_init(mdb_walk_state_t *wsp)
{
	if (wsp->walk_addr == NULL && Rgm_state == NULL &&
	    rgm_get_state(NULL) != 0)
		return (WALK_ERR);

	/* If we don't have a starting RT, use the first RT from Rgm_state. */
	if (wsp->walk_addr == NULL)
		wsp->walk_addr = (uintptr_t)Rgm_state->rgm_rtlist;

	wsp->walk_data = mdb_alloc(sizeof (rgm_rt_t), UM_SLEEP);
	return (WALK_NEXT);
}

/*
 * At each step, read an RT into our private storage, and then invoke
 * the callback function.  We terminate when we reach a NULL rt_next pointer.
 */
int
rgm_rt_walk_step(mdb_walk_state_t *wsp)
{
	int status;

	if (wsp->walk_addr == NULL)
		return (WALK_DONE);

	if (mdb_vread(wsp->walk_data, sizeof (rgm_rt_t),
	    wsp->walk_addr) == -1) {
		mdb_warn("failed to read rt at %p", wsp->walk_addr);
		return (WALK_DONE);
	}

	status = wsp->walk_callback(wsp->walk_addr, wsp->walk_data,
	    wsp->walk_cbdata);

	wsp->walk_addr = (uintptr_t)(((rgm_rt_t *)wsp->walk_data)->rt_next);
	return (status);
}

/*
 * The walker's fini function is invoked at the end of each walk.  Since we
 * dynamically allocated an rgm_rt_t in rt_walk_init, we must free it now.
 */
void
rgm_rt_walk_fini(mdb_walk_state_t *wsp)
{
	mdb_free(wsp->walk_data, sizeof (rgm_rt_t));
}
