/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)mdb_rgm_print.cc	1.30	08/05/20 SMI"

#define	protected public
#include "mdb_rgm.h"
#include <rgm_state_strings.h>
#include <rgm_intention.h>
#include <map>
#include <sys/os.h>

/*
 * This file contains code to print out the rgmd's data structures and
 * map numerical values to symbolic strings.
 *
 * More information on the MDB module for the RGMD is in mdb_rgm_readme.txt
 */

//
// 32-bit word of option flags for intention
//
// Direct copies of the consts in rgm.idl. These definitions
// must be kept in sync with those in rgm.idl
//
typedef unsigned long   intention_flag_t;

const intention_flag_t INTENT_ACTION = (1 << 0);
const intention_flag_t INTENT_OK_FLAG = (1 << 1);
const intention_flag_t INTENT_SYSTEM_ONLY = (1 << 2);
const intention_flag_t INTENT_MANAGE_RG = (1 << 3);
const intention_flag_t INTENT_RUN_UPDATE = (1 << 4);
const intention_flag_t INTENT_RUN_FINI = (1 << 5);
const intention_flag_t INTENT_READ_RS = (1 << 6);

/* global vars for printing routines */
int rgm_verbose;
char rgm_defbuf[DEFBUFSIZE];

static void rgm_print_nodename(void);
static void rgm_print_nodeid(sol::nodeid_t pres);
static void rgm_print_logical_nodeset(LogicalNodeset *nset);

static int rgm_print_rg(rglist_p_t rgp, rglist_p_t rgp_val);
static void rgm_print_rg_ccrdata(rgm_rg_t *rg_ccr);
static void rgm_print_rgstate(rglist_p_t rgp);
static void rgm_print_affinities(rgm_affinities_t *affinities);
static int rgm_print_rs(rlist_p_t rsp, rlist_p_t rsp_val);
static void rgm_print_rsstate(rlist_p_t rs);

static void rgm_print_rs_ccrdata(rgm_resource_t *rs_ccr);

static int rgm_print_rt(rgm_rt_t *rtp, rgm_rt_t *rtp_val);
static void rgm_print_rt_upgrade(rgm_rt_upgrade_t *rtup, char *msg);

static void rgm_print_incarnations(sol::incarnation_num *node_inc,
    LogicalNodeset *curr_members);
static void rgm_print_timelist(rgm_timelist_t *tlist);
static void rgm_print_cluststate(struct clust_state *cluststate);
static void rgm_print_methods(rgm_methods_t *meth);
static void rgm_print_methinvoc(methodinvoc_t *meth);
static void rgm_print_namelist(namelist_t *nlp, boolean_t addnewline);
static void rgm_print_rdeplist(rdeplist_t *nlp, boolean_t addnewline);
static void rgm_print_namelistall(namelist_all_t *nlp, boolean_t addnewline);
static void rgm_print_propertylist(rgm_property_list_t *plp);
static void rgm_print_property(rgm_property_t *p);
static void print_props(mdb_props_t&);

static void rgm_print_paramtable(rgm_param_t **paramtable);
static void rgm_print_param(rgm_param_t *p);
static void rgm_print_paramdefault(rgm_param_t *p);
static void rgm_print_paramconstraints(rgm_param_t *p);
static void rgm_print_nodeidlist(nodeidlist_t *nlp, boolean_t addnewline);
static void rgm_print_nodeidlistall(nodeidlist_all_t *nlp,
    boolean_t addnewline);

/* routines for translating from an enum to a string */
static char *rgm_rgswitch_string(rg_switch_t rgswitch);
static char *rgm_restart_flg_string(rgm::r_restart_t rrflg);
static char *rgm_rgfrozen_string(rg_frozen_t rgfrozen);
static char *rgm_rgmode_string(scha_rgmode_t rgmode);
static char *rgm_initnodes_string(scha_initnodes_flag_t initnodes);
static char *rgm_sysdeftype_string(rgm_sysdeftype_t sysdeftype);
static char *rgm_tunable_string(rgm_tune_t t);
static char *rgm_proptype_string(scha_prop_type_t pt);
static char *rgm_extension_string(boolean_t x);
static char *rgm_bool_string(boolean_t b);

static char *mdb_pr_rfmstatus(rgm::rgm_r_fm_status val);
static char *mdb_pr_rstate(rgm::rgm_r_state val);
static char *mdb_pr_rgstate(rgm::rgm_rg_state val);

static void rgm_print_intention(struct slave_intention *);
static void rgm_print_intent_flags(rgm::intention_flag_t flags);
static const char *rgm_op_string(rgm::rgm_operation op);
static const char *rgm_entity_string(rgm::rgm_entity e);

static void mdb_update_rgstate(mdb_rgxlogs_t &rgx_logs,
	mdb_rgxlogs_t &rgx_back, std::map<rgm::lni_t, rg_xstate>&
	rgstate);
static void mdb_update_rsstate(mdb_rxlogs_t &rx_logs,
	mdb_rxlogs_t &rx_back, std::map<rgm::lni_t, r_xstate_t>& rsstate);

static const char *status_to_string(rgm::rgm_ln_state state_in);


/*
 * Print information from the Rgm_state structure. It requires calling
 * rgm_get_state() and rgm_get_lnManager() first.
 */
void
rgm_print_state(boolean_t printbrief)

{
	sol::nodeid_t nodeid;

	/* sometimes these symbols can't be resolved so they aren't printed */
	rgm_print_nodename();
	rgm_print_nodeid(Rgm_state->rgm_President);

	(void) mdb_inc_indent(DEFINDENT);

	mdb_printf("rgm_mutex owner 0x%x\n", Rgm_state->rgm_mutex.owner());

	mdb_printf("rgm_President %d\n", Rgm_state->rgm_President);

	mdb_printf("rgm_membership_generation %lld\n",
	    Rgm_state->rgm_membership_generation);

	mdb_printf("rgm_total_rgs %d\n", Rgm_state->rgm_total_rgs);
	mdb_printf("rgm_total_rs %d\n", Rgm_state->rgm_total_rs);

	mdb_printf("is_thisnode_booting %s\n",
	    rgm_bool_string(Rgm_state->is_thisnode_booting));

	mdb_printf("rgm_rglist %p\n", Rgm_state->rgm_rglist);

	mdb_printf("rgm_rtlist %p\n", Rgm_state->rgm_rtlist);

	mdb_printf("node_incarnations\n");

	rgm_print_incarnations(
	    (sol::incarnation_num *)Rgm_state->node_incarnations.members,
	    &Rgm_state->current_membership);

	mdb_printf("physical node membership:\n");
	rgm_print_logical_nodeset(&Rgm_state->current_membership);

	mdb_printf("static membership:\n");
	rgm_print_logical_nodeset(&Rgm_state->static_membership);

	mdb_printf("logical node membership:\n");
	rgm_print_logical_nodes();

	mdb_printf("ccr_is_read %s\n", rgm_bool_string(Rgm_state->ccr_is_read));
	mdb_printf("cached ccr cluster state\n");
	rgm_print_cluststate(&Rgm_state->thecluststate);

	mdb_printf("pres_reconfig %s\n", rgm_bool_string(
	    Rgm_state->pres_reconfig));

	mdb_printf("suppress_notification %s\n", rgm_bool_string(
	    Rgm_state->suppress_notification));

	/* print out detailed rgm object configuration */
	rgm_print_rgs((uintptr_t)Rgm_state->rgm_rglist, B_TRUE, printbrief);

	rgm_print_rts((uintptr_t)Rgm_state->rgm_rtlist, B_TRUE, printbrief);

	(void) mdb_dec_indent(DEFINDENT);
}

/*
 * Sometimes mdb cannot find the Nodename symbol, so we use ::status to get
 * it instead.
 */
void
rgm_print_nodename(void)
{
	rgm_defbuf[0] = '\0';
	rgm_get_node_name(rgm_defbuf, sizeof (rgm_defbuf));

	if (rgm_defbuf[0] != '\0')
		mdb_printf("rgmd from node %s\n", rgm_defbuf);
	else
		(void) mdb_eval("::status");
}


/*
 * Try to print the nodeid if possible.  This relies on the shared library
 * information being available, which might not be the case if you are
 * debugging the core on a system different than where the core originiated.
 *
 * If the value of pres is non-zero, we will print a string indicating if this
 * node is the president or not.
 */
void
rgm_print_nodeid(sol::nodeid_t pres)
{
	sol::nodeid_t nid = 0;

	rgm_get_node_id(&nid, sizeof (nid));

	if (nid == 0)
		return;

	if (pres == 0)
		mdb_printf("rgmd from nodeid %d\n", nid);
	else if (pres == nid)
		mdb_printf("rgmd from nodeid %d (the president node)\n", nid);
	else
		mdb_printf("rgmd from nodeid %d (a slave node)\n", nid);
}


/* Print information on all RGs or on one specific RG */
void
rgm_print_rgs(uintptr_t addr, boolean_t printall, boolean_t printbrief)
{
	rglist_t rg_val;

	if (printbrief == B_TRUE)
		mdb_printf("(FYI: printbrief is not yet supported)\n");

	/* use the rg walker to print all rgs */
	if (printall) {
		if (mdb_pwalk("rg",
		    (mdb_walk_cb_t)rgm_print_rg, (void *)NULL, addr) == -1)
			mdb_warn("failed to walk and print rgs");
	} else {
		if (mdb_vread(&rg_val, sizeof (rg_val), addr) == -1) {
			mdb_warn("Failed to read rg from %p", addr);
			return;
		}

		(void) rgm_print_rg((rglist_p_t)addr, &rg_val);
	}
}


/* Print the detailed information for the specified RG */
int
rgm_print_rg(rglist_p_t rgp, rglist_p_t rgp_val)
{
	sol::nodeid_t nodeid;

	mdb_printf("ResourceGroup %p\n", rgp);

	(void) mdb_inc_indent(DEFINDENT);

	rgm_print_rg_ccrdata(rgp_val->rgl_ccr);


	mdb_printf("rgl_xstate\n");
	(void) mdb_inc_indent(DEFINDENT);

	rgm_print_rgstate(rgp_val);

	(void) mdb_dec_indent(DEFINDENT);

	mdb_printf("rgl_switching %s\n",
	    rgm_rgswitch_string(rgp_val->rgl_switching));

	mdb_printf("rgl_failback_running %s\n",
	    rgm_bool_string(rgp_val->rgl_failback_running));

	mdb_printf("rgl_joining_nodes: ");
	rgm_get_logical_nodeset(&rgp_val->rgl_joining_nodes);
	rgm_print_logical_nodeset(&rgp_val->rgl_joining_nodes);
	mdb_printf("\n");

	mdb_printf("rgl_evac_nodes: ");
	rgm_get_logical_nodeset(&rgp_val->rgl_evac_nodes);
	rgm_print_logical_nodeset(&rgp_val->rgl_evac_nodes);
	mdb_printf("\n");

	mdb_printf("rgl_planned_on_nodes: ");
	rgm_get_logical_nodeset(&rgp_val->rgl_planned_on_nodes);
	rgm_print_logical_nodeset(&rgp_val->rgl_planned_on_nodes);
	mdb_printf("\n");

	mdb_printf("rgl_new_pres_rebalance %s\n",
	    rgm_bool_string(rgp_val->rgl_new_pres_rebalance));

	mdb_printf("rgl_delayed_rebalance %s\n",
	    rgm_bool_string(rgp_val->rgl_delayed_rebalance));

	mdb_printf("rgl_no_rebalance %s\n",
	    rgm_bool_string(rgp_val->rgl_no_rebalance));

	mdb_printf("rgl_no_triggered_rebalance %s\n",
	    rgm_bool_string(rgp_val->rgl_no_triggered_rebalance));

	mdb_printf("rgl_ok_to_start %s\n",
	    rgm_bool_string(rgp_val->rgl_ok_to_start));

	mdb_printf("rgl_is_frozen %s\n",
	    rgm_rgfrozen_string(rgp_val->rgl_is_frozen));

	mdb_printf("rgl_last_frozen %ld", rgp_val->rgl_last_frozen);
	if (rgp_val->rgl_last_frozen != 0)
		mdb_printf("  (%Y)\n", rgp_val->rgl_last_frozen);
	else
		mdb_printf("\n");

	mdb_printf("rgl_methods_in_flight %p\n",
	    rgp_val->rgl_methods_in_flight);
	rgm_print_methinvoc(rgp_val->rgl_methods_in_flight);

	mdb_printf("rgl_affinities: \n");
	rgm_print_affinities(&(rgp_val->rgl_affinities));
	mdb_printf("rgl_affinitents: \n");
	rgm_print_affinities(&(rgp_val->rgl_affinitents));

	mdb_printf("rgl_quiescing %s\n",
	    rgm_bool_string(rgp_val->rgl_quiescing));

	mdb_printf("rgl_total_rs %d\n", rgp_val->rgl_total_rs);
	mdb_printf("rgl_resources %p\n", rgp_val->rgl_resources);

	(void) mdb_inc_indent(DEFINDENT);

	/* do a local walk of this RG's resources */
	if (mdb_pwalk("rs", (mdb_walk_cb_t)rgm_print_rs,
	    (void *)NULL, (uintptr_t)rgp_val->rgl_resources) == -1)
		mdb_warn("failed to walk and print rs's");

	(void) mdb_dec_indent(DEFINDENT);

	(void) mdb_dec_indent(DEFINDENT);

	mdb_printf("\n");
	return (0);
}


/* Print the information stored in the CCR for the given RG */
void
rgm_print_rg_ccrdata(rgm_rg_t *rg_ccr)
{
	rgm_rg_t rgdata;

	mdb_printf("rgl_ccr %p\n", rg_ccr);
	(void) mdb_inc_indent(DEFINDENT);

	if (mdb_vread(&rgdata, sizeof (rgdata), (uintptr_t)rg_ccr) == -1) {
		mdb_warn("Failed to read the rg ccr data from %p", rg_ccr);
		return;
	}

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rgdata.rg_name) == -1) {
		mdb_warn("Failed to read the rg name from %p", rgdata.rg_name);
		return;
	}
	mdb_printf("rg_name %s\n", rgm_defbuf);

	mdb_printf("rg_unmanaged %s\n",
	    rgm_bool_string(rgdata.rg_unmanaged));

	mdb_printf("rg_suspended %s\n",
	    rgm_bool_string(rgdata.rg_suspended));

	mdb_printf("rg_nodelist ");
	rgm_print_nodeidlist(rgdata.rg_nodelist, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rg_max_primaries %d\n", rgdata.rg_max_primaries);

	mdb_printf("rg_desired_primaries %d\n", rgdata.rg_desired_primaries);

	mdb_printf("rg_failback %s\n",
	    rgm_bool_string(rgdata.rg_failback));

	mdb_printf("rg_dependencies ");
	rgm_print_namelist(rgdata.rg_dependencies, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rg_mode %s\n",
	    rgm_rgmode_string(rgdata.rg_mode));

	mdb_printf("rg_impl_net_depend %s\n",
	    rgm_bool_string(rgdata.rg_impl_net_depend));

	mdb_printf("rg_glb_rsrcused ");
	rgm_print_namelistall(&rgdata.rg_glb_rsrcused, B_FALSE);
	mdb_printf("\n");

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rgdata.rg_pathprefix) == -1) {
		mdb_warn("Failed to read the pathprefix from %p",
		    rgdata.rg_pathprefix);
		return;
	}
	mdb_printf("rg_pathprefix %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rgdata.rg_description) == -1) {
		mdb_warn("Failed to read the description from %p",
		    rgdata.rg_description);
		return;
	}
	mdb_printf("rg_description %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rgdata.rg_stamp) == -1) {
		mdb_warn("Failed to read the stamp from %p",
		    rgdata.rg_stamp);
		return;
	}
	mdb_printf("rg_stamp %s\n", rgm_defbuf);

	mdb_printf("rg_ppinterval %d\n", rgdata.rg_ppinterval);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rgdata.rg_project_name) == -1) {
		mdb_warn("Failed to read the project_name from %p",
		    rgdata.rg_project_name);
		return;
	}
	mdb_printf("rg_project_name %s\n", rgm_defbuf);

	mdb_printf("rg_affinities ");
	rgm_print_namelist(rgdata.rg_affinities, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rg_auto_start %s\n",
	    rgm_bool_string(rgdata.rg_auto_start));

	mdb_printf("rg_system %s\n",
	    rgm_bool_string(rgdata.rg_system));

	mdb_printf("rg_suspended %s\n",
	    rgm_bool_string(rgdata.rg_suspended));

	(void) mdb_dec_indent(DEFINDENT);
}


/*
 * Print the per-zone state of the RG on each of the current cluster zones.
 * Note, some fields are set only on slaves.
 */
void
rgm_print_rgstate(rglist_p_t rgdata)
{
	sol::nodeid_t nodeid;
	std::map<rgm::lni_t, rg_xstate> rgstate;
	std::map<rgm::lni_t, rg_xstate>::iterator it;

	mdb_update_rgstate(rgdata->log_rgxstate, rgdata->mdb_rgxback, rgstate);

	for (it = rgstate.begin(); it != rgstate.end(); ++it) {
		mdb_printf("rgx_state[%d] = %s\n", it->first,
			mdb_pr_rgstate(rgstate[it->first].rgx_state));

		mdb_printf("rgx_net_relative_restart[%d] = %s\n",
			it->first,
			rgm_bool_string(rgstate[it->first].
			rgx_net_relative_restart));

		mdb_printf("rgx_postponed_stop[%d] = %s\n", it->first,
			rgm_bool_string(rgstate[it->first].
			rgx_postponed_stop));

		mdb_printf("rgx_postponed_boot[%d] = %s\n", it->first,
			rgm_bool_string(rgstate[it->first].
			rgx_postponed_boot));

		mdb_printf("rgx_start_fail[%d] = ", it->first);
		rgm_print_timelist(rgstate[it->first].rgx_start_fail);
		mdb_printf("\n");
	}
}

/*
 * Updates the rgstate map from rgx_back and rgx_logs.
 * The rgx_logs represents the latest sets of changes on the rgstate
 * structures and hence rgstate map is updated the last using rgx_logs.
 */
void
mdb_update_rgstate(mdb_rgxlogs_t &rgx_logs, mdb_rgxlogs_t &rgx_back,
	std::map<rgm::lni_t, rg_xstate>& rgstate)
{
	mdb_rgxstate_t *addr = NULL;
	int i = 0, lni = 0;
	mdb_rgxstate_t rgxstate;


	if (rgx_back.size > 0) {
		addr = rgx_back.rgx_state;
		if (mdb_vread(&rgxstate, sizeof (mdb_rgxstate_t),
		    (uintptr_t)addr) == -1) {
			mdb_warn("Failed to read the RG state hackup info"
			    "ptr from %p", addr);
			return;
		}

		while (i < rgx_back.size) {
			lni = rgxstate.lni;
			rgstate[lni].rgx_state =
			    (rgm::rgm_rg_state)rgxstate.rgx_state;
			rgstate[lni].rgx_net_relative_restart =
			    rgxstate.rgx_net_relative_restart;
			rgstate[lni].rgx_start_fail =
			    rgxstate.rgx_start_fail;
			rgstate[lni].rgx_postponed_stop =
			    rgxstate.rgx_postponed_stop;
			rgstate[lni].rgx_postponed_boot =
			    rgxstate.rgx_postponed_boot;
			addr++;
			if (mdb_vread(&rgxstate, sizeof (mdb_rgxstate_t),
			    (uintptr_t)addr) == -1) {
				mdb_warn("Failed to read the RG state info"
				"ptr from %p", addr);
				return;
			}
			++i;
		}
	}
	i = 0;
	if (rgx_logs.size > 0) {
		addr = rgx_logs.rgx_state;
		if (mdb_vread(&rgxstate, sizeof (mdb_rgxstate_t),
		    (uintptr_t)addr) == -1) {
			mdb_warn("Failed to read the RG state Log info"
			    "ptr from %p", addr);
			return;
		}

		while (i < rgx_logs.size) {
			lni = rgxstate.lni;
			rgstate[lni].rgx_state =
			    (rgm::rgm_rg_state)rgxstate.rgx_state;
			rgstate[lni].rgx_net_relative_restart =
			    rgxstate.rgx_net_relative_restart;
			rgstate[lni].rgx_start_fail =
			    rgxstate.rgx_start_fail;
			rgstate[lni].rgx_postponed_stop =
			    rgxstate.rgx_postponed_stop;
			rgstate[lni].rgx_postponed_boot =
			    rgxstate.rgx_postponed_boot;
			addr++;
			if (mdb_vread(&rgxstate, sizeof (mdb_rgxstate_t),
			    (uintptr_t)addr) == -1) {
				mdb_warn("Failed to read the RG state info"
				"ptr from %p", addr);
				return;
			}
			++i;
		}
	}
}


void
rgm_print_affinities(rgm_affinities_t *affinities)
{
	(void) mdb_inc_indent(DEFINDENT);

	mdb_printf("strong positive: ");
	rgm_print_namelist(affinities->ap_strong_pos, B_FALSE);
	mdb_printf("\n");

	mdb_printf("weak positive: ");
	rgm_print_namelist(affinities->ap_weak_pos, B_FALSE);
	mdb_printf("\n");

	mdb_printf("strong negative: ");
	rgm_print_namelist(affinities->ap_strong_neg, B_FALSE);
	mdb_printf("\n");

	mdb_printf("weak negative: ");
	rgm_print_namelist(affinities->ap_weak_neg, B_FALSE);
	mdb_printf("\n");

	mdb_printf("strong positive with failover delegation: ");
	rgm_print_namelist(affinities->ap_failover_delegation, B_FALSE);
	mdb_printf("\n");

	(void) mdb_dec_indent(DEFINDENT);
}


/* Print information on all resources or on one specific resource */
void
rgm_print_rss(uintptr_t addr, boolean_t printall, boolean_t printbrief)
{
	rlist_t rs_val;

	if (printbrief == B_TRUE)
		mdb_printf("(FYI: printbrief is not yet supported)\n");

	if (printall) {
		if (mdb_pwalk("rs",
		    (mdb_walk_cb_t)rgm_print_rs, (void *)NULL, addr) == -1)
			mdb_warn("Failed to walk and print rss");
	} else {
		if (mdb_vread(&rs_val, sizeof (rs_val), addr) == -1) {
			mdb_warn("Failed to read rs from %p", addr);
			return;
		}

		(void) rgm_print_rs((rlist_p_t)addr, &rs_val);
	}
}


/* Print the detailed information for the specified resource */
int
rgm_print_rs(rlist_p_t rsp, rlist_p_t rsp_val)
{
	mdb_printf("Resource %p\n", rsp);

	(void) mdb_inc_indent(DEFINDENT);

	rgm_print_rs_ccrdata(rsp_val->rl_ccrdata);

	mdb_printf("rl_ccrtype %p\n", rsp_val->rl_ccrtype);

	mdb_printf("rl_rg %p\n", rsp_val->rl_rg);

	mdb_printf("rl_xstate\n");
	(void) mdb_inc_indent(DEFINDENT);
	rgm_print_rsstate(rsp_val);
	(void) mdb_dec_indent(DEFINDENT);

	mdb_printf("rl_is_deleted %s\n",
	    rgm_bool_string(rsp_val->rl_is_deleted));

	mdb_printf("rl_dependents.dp_weak ");
	rgm_print_rdeplist(rsp_val->rl_dependents.dp_weak, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rl_dependents.dp_offline_restart");
	rgm_print_rdeplist(rsp_val->rl_dependents.dp_offline_restart, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rl_dependents.dp_strong ");
	rgm_print_rdeplist(rsp_val->rl_dependents.dp_strong, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rl_dependents.dp_restart ");
	rgm_print_rdeplist(rsp_val->rl_dependents.dp_restart, B_FALSE);
	mdb_printf("\n");

	(void) mdb_dec_indent(DEFINDENT);
	return (0);
}


/* Print the information stored in the CCR for the given resource */
void
rgm_print_rs_ccrdata(rgm_resource_t *rs_ccr)
{
	rgm_resource_t rsdata;

	mdb_printf("rl_ccrdata %p\n", rs_ccr);
	(void) mdb_inc_indent(DEFINDENT);

	if (mdb_vread(&rsdata, sizeof (rsdata), (uintptr_t)rs_ccr) == -1) {
		mdb_warn("Failed to read the rs ccr data from %p", rs_ccr);
		return;
	}

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.r_name) == -1) {
		mdb_warn("Failed to read the rs name from %p", rsdata.r_name);
		return;
	}
	mdb_printf("r_name %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.r_type) == -1) {
		mdb_warn("Failed to read the rt name from %p", rsdata.r_type);
		return;
	}
	mdb_printf("r_type %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.r_rgname) == -1) {
		mdb_warn("Failed to read the rg name from %p", rsdata.r_rgname);
		return;
	}
	mdb_printf("r_rgname %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.rx_back.mdb_onoffswitch) == -1) {
		mdb_warn("Failed to read Onoff switch information");
		return;
	}

	mdb_printf("r_onoff_switch - Enabled on lni's %s\n",
	    rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.rx_back.mdb_monswitch) == -1) {
		mdb_warn("Failed to read Monitored switch information");
		return;
	}

	mdb_printf("r_monitored_switch - Enabled on lni's %s\n",
	    rgm_defbuf);

	mdb_printf("r_dependencies.dp_weak ");
	rgm_print_rdeplist(rsdata.r_dependencies.dp_weak, B_FALSE);
	mdb_printf("\n");

	mdb_printf("r_dependencies.dp_offline_restart");
	rgm_print_rdeplist(rsdata.r_dependencies.dp_offline_restart, B_FALSE);
	mdb_printf("\n");

	mdb_printf("r_dependencies.dp_strong ");
	rgm_print_rdeplist(rsdata.r_dependencies.dp_strong, B_FALSE);
	mdb_printf("\n");

	mdb_printf("r_dependencies.dp_restart ");
	rgm_print_rdeplist(rsdata.r_dependencies.dp_restart, B_FALSE);
	mdb_printf("\n");

	mdb_printf("r_properties %p\n", rsdata.r_properties);
	(void) mdb_inc_indent(DEFINDENT);
	rgm_print_propertylist(rsdata.r_properties);
	print_props(rsdata.rx_back.mdb_stdprops);
	(void) mdb_dec_indent(DEFINDENT);


	mdb_printf("r_ext_properties %p\n", rsdata.r_ext_properties);
	(void) mdb_inc_indent(DEFINDENT);
	rgm_print_propertylist(rsdata.r_ext_properties);
	print_props(rsdata.rx_back.mdb_extprops);
	(void) mdb_dec_indent(DEFINDENT);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.r_description) == -1) {
		mdb_warn("Failed to read the rs description from %p",
		    rsdata.r_description);
		return;
	}
	mdb_printf("r_description %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.r_project_name) == -1) {
		mdb_warn("Failed to read the rs project name from %p",
		    rsdata.r_project_name);
		return;
	}
	mdb_printf("r_project_name %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rsdata.r_type_version) == -1) {
		mdb_warn("Failed to read the rs type_version from %p",
		    rsdata.r_type_version);
		return;
	}
	mdb_printf("r_type_version %s\n", rgm_defbuf);

	(void) mdb_dec_indent(DEFINDENT);
}

/*
 * This function prints the standard/extension property
 * information. This function does'nt handle the properties of type
 * STRINGARRAY. rgm_print_propertylist function prints the property
 * of type STRINGARRAY.
 */
void
print_props(mdb_props_t& props)
{
	char **val = NULL;
	char **addr;
	char *str = NULL;
	int i = 0;
	if (props.size > 0) {
		addr = props.value;
		if (mdb_vread(&str, sizeof (char *),
		    (uintptr_t)addr) == -1) {
			mdb_warn("Failed to read the <prop> = <value>"
			    "ptr from %p", addr);
			return;
		}

		while (i < props.size) {
			if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
			    (uintptr_t)str) == -1) {
				mdb_warn("Failed to read the rs"
					" property value from %p", str);
				return;
			}
			mdb_printf("property = {<lni>:}<value>;.. : %s\n",
				rgm_defbuf);
			addr++;
			if (mdb_vread(&str, sizeof (char *), (uintptr_t)addr)
			    == -1) {
				mdb_warn("Failed to read the rs property"
				    " value from from %p", addr);
				return;
			}
			++i;
		}
	}
}


/* Print the state of the resource on each of the current cluster nodes */
void
rgm_print_rsstate(rlist_p_t rs)
{
	std::map<rgm::lni_t, r_xstate_t> rsstate;
	std::map<rgm::lni_t, r_xstate_t>::iterator it;
	int lni;

	mdb_update_rsstate(rs->log_rxstate, rs->mdb_rxback, rsstate);

	for (it = rsstate.begin(); it != rsstate.end(); ++it) {
		lni = it->first;
		if (rsstate[lni].rx_fm_status_msg != NULL) {
			if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
				(uintptr_t)rsstate[lni].
				rx_fm_status_msg) == -1) {
				mdb_warn("Failed to read fm_msg at %p",
					rsstate[lni].rx_fm_status_msg);
				return;
			}
		} else {
			(void) snprintf(rgm_defbuf,
				sizeof (rgm_defbuf), "NULL");
		}

		mdb_printf("rx_state[%d] = %s\n",
			lni, mdb_pr_rstate(rsstate[lni].rx_state));

		mdb_printf("rx_fm_status[%d] = %s: %s\n",
			lni,
			mdb_pr_rfmstatus(rsstate[lni].rx_fm_status),
			rgm_defbuf);

		mdb_printf("rx_restarting %s\n",
			rgm_bool_string(rsstate[lni].rx_restarting));

		mdb_printf("rx_rg_restarts = ");
		rgm_print_timelist(rsstate[lni].rx_rg_restarts);
		mdb_printf("\n");

		mdb_printf("last_restart_time=%d, "
		    "throttle_restart_counter=%d\n",
		    rsstate[lni].rx_last_restart_time,
		    rsstate[lni].rx_throttle_restart_counter);

		mdb_printf("rx_r_restarts = ");
		rgm_print_timelist(rsstate[lni].rx_r_restarts);
		mdb_printf("\n");

		mdb_printf("rx_blocked_restart = ");
		rgm_print_timelist(rsstate[lni].rx_blocked_restart);
		mdb_printf("\n");

		mdb_printf("rx_restart_flg %s\n",
			rgm_restart_flg_string(rsstate[lni].
			rx_restart_flg));

		mdb_printf("rx_ignore_failed_start %s\n",
			rgm_bool_string(rsstate[lni].
			rx_ignore_failed_start));

		mdb_printf("rx_ok_to_start %s\n",
			rgm_bool_string(rsstate[lni].rx_ok_to_start));
	}
	/* XXX add FYI state support once rgmd implements it */
}

/*
 * Updates the rsstate map from rx_back and rx_logs.
 * The rx_logs represents the latest sets of changes on the rgstate
 * structures and hence rstate map is updated the last using rx_logs.
 */
void
mdb_update_rsstate(mdb_rxlogs_t &rx_logs, mdb_rxlogs_t &rx_back,
	std::map<rgm::lni_t, r_xstate_t>& rstate)
{
	mdb_rxstate_t rxstate;
	int i = 0, lni = 0;
	mdb_rxstate_t *addr = NULL;

	if (rx_back.size > 0) {
		addr = rx_back.rx_state;
		if (mdb_vread(&rxstate, sizeof (mdb_rxstate_t),
		    (uintptr_t)addr) == -1) {
			mdb_warn("Failed to read the RS state hackup info"
			    "ptr from %p", addr);
			return;
		}

		while (i < rx_back.size) {
			lni = rxstate.lni;
			rstate[lni].rx_state =
			    (rgm::rgm_r_state)rxstate.rx_state;
			rstate[lni].rx_fm_status =
			    (rgm::rgm_r_fm_status)rxstate.rx_fm_status;
			rstate[lni].rx_restarting =
			    rxstate.rx_restarting;
			rstate[lni].rx_rg_restarts =
			    rxstate.rx_rg_restarts;
			rstate[lni].rx_last_restart_time =
			    rxstate.rx_last_restart_time;
			rstate[lni].rx_throttle_restart_counter =
			    rxstate.rx_throttle_restart_counter;
			rstate[lni].rx_r_restarts =
			    rxstate.rx_r_restarts;
			rstate[lni].rx_blocked_restart =
			    rxstate.rx_blocked_restart;
			rstate[lni].rx_restart_flg =
			    (rgm::r_restart_t)rxstate.rx_restart_flg;
			rstate[lni].rx_ignore_failed_start =
			    rxstate.rx_ignore_failed_start;
			rstate[lni].rx_ok_to_start =
			    rxstate.rx_ok_to_start;
			rstate[lni].rx_fm_status_msg =
			    rxstate.rx_fm_status_msg;
			addr++;
			if (mdb_vread(&rxstate, sizeof (mdb_rxstate_t),
			    (uintptr_t)addr) == -1) {
				mdb_warn("Failed to read the Resource state"
				" info ptr from %p", addr);
				return;
			}
			++i;
		}
	}
	i = 0;
	if (rx_logs.size > 0) {
		addr = rx_logs.rx_state;
		if (mdb_vread(&rxstate, sizeof (mdb_rxstate_t),
		    (uintptr_t)addr) == -1) {
			mdb_warn("Failed to read the RS state log info"
			    "ptr from %p", addr);
			return;
		}

		while (i < rx_logs.size) {
			lni = rxstate.lni;
			rstate[lni].rx_state =
			    (rgm::rgm_r_state)rxstate.rx_state;
			rstate[lni].rx_fm_status =
			    (rgm::rgm_r_fm_status)rxstate.rx_fm_status;
			rstate[lni].rx_restarting =
			    rxstate.rx_restarting;
			rstate[lni].rx_rg_restarts =
			    rxstate.rx_rg_restarts;
			rstate[lni].rx_last_restart_time =
			    rxstate.rx_last_restart_time;
			rstate[lni].rx_throttle_restart_counter =
			    rxstate.rx_throttle_restart_counter;
			rstate[lni].rx_r_restarts =
			    rxstate.rx_r_restarts;
			rstate[lni].rx_blocked_restart =
			    rxstate.rx_blocked_restart;
			rstate[lni].rx_restart_flg =
			    (rgm::r_restart_t)rxstate.rx_restart_flg;
			rstate[lni].rx_ignore_failed_start =
			    rxstate.rx_ignore_failed_start;
			rstate[lni].rx_ok_to_start =
			    rxstate.rx_ok_to_start;
			rstate[lni].rx_fm_status_msg =
			    rxstate.rx_fm_status_msg;
			addr++;
			if (mdb_vread(&rxstate, sizeof (mdb_rxstate_t),
			    (uintptr_t)addr) == -1) {
				mdb_warn("Failed to read the Resource state "
				"info ptr from %p", addr);
				return;
			}
			++i;
		}
	}
}

/* Print information on all RTs or on one specific RT */
void
rgm_print_rts(uintptr_t addr, boolean_t printall, boolean_t printbrief)
{
	rgm_rt_t rt_val;

	if (printbrief == B_TRUE)
		mdb_printf("(FYI: printbrief is not yet supported)\n");

	if (printall) {
		if (mdb_pwalk("rt",
		    (mdb_walk_cb_t)rgm_print_rt, (void *)NULL, addr) == -1)
			mdb_warn("Failed to walk and print rts");
	} else {
		if (mdb_vread(&rt_val, sizeof (rt_val), addr) == -1) {
			mdb_warn("Failed to read rt from %p", addr);
			return;
		}

		(void) rgm_print_rt((rgm_rt_t *)addr, &rt_val);
	}
}


/* Print the detailed information for the specified RT */
int
rgm_print_rt(rgm_rt_t *rtp, rgm_rt_t *rtp_val)
{
	mdb_printf("ResourceType %p\n", rtp);

	(void) mdb_inc_indent(DEFINDENT);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rtp_val->rt_name) == -1) {
		mdb_warn("Failed to read rt name at %p", rtp_val->rt_name);
		return (-1);
	}
	mdb_printf("rt_name %s\n", rgm_defbuf);

	if (rtp_val->rt_basedir != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)rtp_val->rt_basedir) == -1) {
			mdb_warn("Failed to read rt basedir at %p",
			    rtp_val->rt_basedir);
			return (-1);
		}
	} else
	    (void) snprintf(rgm_defbuf, sizeof (rgm_defbuf), "NULL");
	mdb_printf("rt_basedir %s\n", rgm_defbuf);

	(void) mdb_inc_indent(DEFINDENT);
	rgm_print_methods(&rtp_val->rt_methods);
	(void) mdb_dec_indent(DEFINDENT);

	mdb_printf("rt_single_inst %s\n",
	    rgm_bool_string(rtp_val->rt_single_inst));

	mdb_printf("rt_init_nodes %s\n",
	    rgm_initnodes_string(rtp_val->rt_init_nodes));

	mdb_printf("rt_instl_nodes ");
	rgm_print_nodeidlistall(&rtp_val->rt_instl_nodes, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rt_failover %s\n", rgm_bool_string(rtp_val->rt_failover));

	mdb_printf("rt_globalzone %s\n",
	    rgm_bool_string(rtp_val->rt_globalzone));

	mdb_printf("rt_system %s\n", rgm_bool_string(rtp_val->rt_system));

	mdb_printf("rt_sysdeftype %s\n",
	    rgm_sysdeftype_string(rtp_val->rt_sysdeftype));

	mdb_printf("rt_dependencies ");
	rgm_print_namelist(rtp_val->rt_dependencies, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rt_api_version %u\n", rtp_val->rt_api_version);

	if (rtp_val->rt_version != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)rtp_val->rt_version) == -1) {
			mdb_warn("Failed to read rt version at %p",
			    rtp_val->rt_version);
			return (-1);
		}
	} else
	    (void) snprintf(rgm_defbuf, sizeof (rgm_defbuf), "NULL");
	mdb_printf("rt_version %s\n", rgm_defbuf);

	mdb_printf("rt_pkglist ");
	rgm_print_namelist(rtp_val->rt_pkglist, B_FALSE);
	mdb_printf("\n");

	mdb_printf("rt_sc30 %s\n", rgm_bool_string(rtp_val->rt_sc30));
	if (!rtp_val->rt_sc30) {
		(void) mdb_inc_indent(DEFINDENT);
		rgm_print_rt_upgrade(rtp_val->rt_upgrade_from, "upgrade_from");
		rgm_print_rt_upgrade(rtp_val->rt_downgrade_to, "downgrade_to");
		(void) mdb_dec_indent(DEFINDENT);
	}

	mdb_printf("rt_paramtable %p\n", rtp_val->rt_paramtable);
	(void) mdb_inc_indent(DEFINDENT);
	rgm_print_paramtable(rtp_val->rt_paramtable);
	(void) mdb_dec_indent(DEFINDENT);

	if (rtp_val->rt_description != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)rtp_val->rt_description) == -1) {
			mdb_warn("Failed to read rt description at %p",
			    rtp_val->rt_description);
			return (-1);
		}
	} else
	    (void) snprintf(rgm_defbuf, sizeof (rgm_defbuf), "NULL");
	mdb_printf("rt_description %s\n", rgm_defbuf);

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)rtp_val->rt_stamp) == -1) {
		mdb_warn("Failed to read the stamp from %p",
		    rtp_val->rt_stamp);
		return (-1);
	}
	mdb_printf("rt_stamp %s\n", rgm_defbuf);

	mdb_printf("\n");

	(void) mdb_dec_indent(DEFINDENT);
	return (0);
}


/* Print the node incarnation data */
void
rgm_print_incarnations(sol::incarnation_num *node_inc,
    LogicalNodeset *curr_members)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni = 0;

	(void) mdb_inc_indent(DEFINDENT);

	/* Print out incarnations for nodes that were cluster members. */
	while ((lni = curr_members->nextServerNodeSet(lni + 1)) != 0) {
		mdb_printf("incarnation[%d] = %ul  (%Y)\n",
		    lni, node_inc[lni], node_inc[lni]);
	}

	(void) mdb_dec_indent(DEFINDENT);
}

/* Print the cached cmm cluster state data */
void
rgm_print_cluststate(struct clust_state *cluststate)
{
	(void) mdb_inc_indent(DEFINDENT);

	mdb_printf("rp_nnodes %d\n", cluststate->rp_nnodes);
	mdb_printf("rp_local_nodeid %d\n", cluststate->rp_local_nodeid);
	mdb_printf("rp_max_step %d\n", cluststate->rp_max_step);
	mdb_printf("rp_curr_step %d\n", cluststate->rp_curr_step);
	mdb_printf("rp_curr_state %d\n", cluststate->rp_curr_state);
	mdb_printf("rp_rseqnum %llu (0x%llx)\n", cluststate->rp_rseqnum,
		cluststate->rp_rseqnum);
	mdb_printf("rp_curr_members %llu (0x%llx)\n",
		cluststate->rp_curr_members, cluststate->rp_curr_members);

	(void) mdb_dec_indent(DEFINDENT);
}

/* Print out timelist data in the RG or resource structure */
void
rgm_print_timelist(rgm_timelist_t *tlist)
{
	rgm_timelist_t	tlist_val;

	while (tlist != NULL) {
		if (mdb_vread(&tlist_val, sizeof (rgm_timelist_t),
		    (uintptr_t)tlist) == -1) {
			mdb_warn("Failed to read the timelist data from %p",
			    tlist);
			return;
		}

		mdb_printf("%ld (%Y); ",
		    tlist_val.tl_time, tlist_val.tl_time);
		tlist = tlist_val.tl_next;
	}
}

/* Print out a list of upgrade_from or downgrade_to data */
void
rgm_print_rt_upgrade(rgm_rt_upgrade_t *rtulist, char *msg)
{
	rgm_rt_upgrade_t rtu_val;

	while (rtulist != NULL) {
		if (mdb_vread(&rtu_val, sizeof (rgm_rt_upgrade_t),
		    (uintptr_t)rtulist)  == -1) {
			mdb_warn("Failed to read the upgrade data from %p",
			    rtulist);
			return;
		}
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)rtu_val.rtu_version) == -1) {
			mdb_warn("Failed to read the version string from %p",
			    rtu_val.rtu_version);
			return;
		}

		mdb_printf("%s: rt_version=%s; Tunable=%s;\n",
		    msg, rgm_defbuf,
		    rgm_tunable_string(rtu_val.rtu_tunability));
		rtulist = rtu_val.rtu_next;
	}
}

/* Print out a list of system properties or extenstion properties. */
void
rgm_print_propertylist(rgm_property_list_t *plp)
{
	rgm_property_list_t pl;
	rgm_property_t prop;

	while (plp != NULL) {
		if (mdb_vread(&pl, sizeof (rgm_property_list_t),
		    (uintptr_t)plp) == -1) {
			mdb_warn("Failed to read the propertylist data from %p",
			    plp);
			return;
		}

		if (mdb_vread(&prop, sizeof (prop),
		    (uintptr_t)pl.rpl_property) == -1) {
			mdb_warn("Failed to read the property from %p",
			    pl.rpl_property);
			return;
		}

		rgm_print_property(&prop);
		plp = pl.rpl_next;
	}
}


/* Print a given property */
void
rgm_print_property(rgm_property_t *p)
{
	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)p->rp_key) == -1) {
		mdb_warn("Failed to read the property key string from %p",
		    p->rp_key);
		return;
	}

	mdb_printf("%s = ", rgm_defbuf);

	if (p->rp_type == SCHA_PTYPE_STRINGARRAY) {
		rgm_print_namelist(p->rp_array_values, B_FALSE);
		mdb_printf("\n");
	}
}


/* Print out the resource type paramtable */
void
rgm_print_paramtable(rgm_param_t **paramtable)
{
	rgm_param_t	*param_ptr;
	rgm_param_t	param_val;

	if (paramtable == NULL)
		return;

	if (mdb_vread(&param_ptr, sizeof (param_ptr),
	    (uintptr_t)paramtable) == -1) {
		mdb_warn("Failed to read the paramtable ptr from %p",
		    paramtable);
		return;
	}

	while (param_ptr != NULL) {
		if (mdb_vread(&param_val, sizeof (param_val),
		    (uintptr_t)param_ptr) == -1) {
			mdb_warn("Failed to read the paramtable value from %p",
			    param_ptr);
			return;
		}

		rgm_print_param(&param_val);

		paramtable++;
		if (mdb_vread(&param_ptr, sizeof (param_ptr),
		    (uintptr_t)paramtable) == -1) {
			mdb_warn("Failed to read the paramtable ptr from %p",
			    paramtable);
			return;
		}
	}
}


/* Print out one entry in the paramtable */
void
rgm_print_param(rgm_param_t *p)
{
	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)p->p_name) == -1) {
		mdb_warn("Failed to read the param name string from %p",
		    p->p_name);
		return;
	}
	mdb_printf("%s\n", rgm_defbuf);

	(void) mdb_inc_indent(DEFINDENT);

	mdb_printf("%s; Tunable=%s; Type=%s;\n",
	    rgm_extension_string(p->p_extension),
	    rgm_tunable_string(p->p_tunable),
	    rgm_proptype_string(p->p_type));

	rgm_print_paramconstraints(p);

	if (p->p_description != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)p->p_description) == -1) {
			mdb_warn("Failed to read param description at %p",
			    p->p_description);
			return;
		}
	} else
	    (void) snprintf(rgm_defbuf, sizeof (rgm_defbuf), "NULL");

	mdb_printf("p_description %s\n", rgm_defbuf);

	(void) mdb_dec_indent(DEFINDENT);
}


/* Print the dafault value for the given paramtable entry */
void
rgm_print_paramdefault(rgm_param_t *p)
{
	if (p->p_default_isset) {
		switch (p->p_type) {
		case SCHA_PTYPE_STRING:
		case SCHA_PTYPE_ENUM:
			if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
			    (uintptr_t)p->p_defaultstr) == -1) {
				mdb_warn("Failed to read the defaultstr string "
				    "from %p", p->p_defaultstr);
				return;
			}
			mdb_printf("Default=%s; ", rgm_defbuf);
			break;
		case SCHA_PTYPE_INT:
			mdb_printf("Default=%d; ", p->p_defaultint);
			break;
		case SCHA_PTYPE_BOOLEAN:
			mdb_printf("Default=%s; ",
			    rgm_bool_string(p->p_defaultbool));
			break;
		case SCHA_PTYPE_STRINGARRAY:
			mdb_printf("Default=");
			rgm_print_namelist(p->p_defaultarray, B_FALSE);
			mdb_printf("; ");
			break;
		case SCHA_PTYPE_UINT:
		case SCHA_PTYPE_UINTARRAY:
		default:
			mdb_printf("unexpected default type: %s; ",
			    rgm_proptype_string(p->p_type));
			break;
		}
	}
}


/* Print out the contraints for a given paramtable entry */
void
rgm_print_paramconstraints(rgm_param_t *p)
{
	mdb_printf("constraints: ");

	rgm_print_paramdefault(p);

	if (p->p_type == SCHA_PTYPE_ENUM) {
		mdb_printf("p_enumlist={ ");
		rgm_print_namelist(p->p_enumlist, B_FALSE);
		mdb_printf("}; ");
	}

	if (p->p_min_isset)
	    mdb_printf("p_min=%d; ", p->p_min);

	if (p->p_max_isset)
	    mdb_printf("p_max=%d; ", p->p_max);

	if (p->p_arraymin_isset)
	    mdb_printf("p_arraymin=%d; ", p->p_arraymin);

	if (p->p_arraymax_isset)
	    mdb_printf("p_arraymax=%d; ", p->p_arraymax);

	mdb_printf("\n");
}


/* Print the method information for a given RT */
void
rgm_print_methods(rgm_methods_t *meth)
{
	if (meth->m_start != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_start) == -1) {
			mdb_warn("Failed to read m_start from %p",
			    meth->m_start);
			return;
		}
		mdb_printf("m_start %s\n", rgm_defbuf);
	}

	if (meth->m_stop != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_stop) == -1) {
			mdb_warn("Failed to read m_stop from %p",
			    meth->m_stop);
			return;
		}
		mdb_printf("m_stop %s\n", rgm_defbuf);
	}

	if (meth->m_validate != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_validate) == -1) {
			mdb_warn("Failed to read m_validate from %p",
			    meth->m_validate);
			return;
		}
		mdb_printf("m_validate %s\n", rgm_defbuf);
	}

	/* skip prim_chng */

	if (meth->m_update != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_update) == -1) {
			mdb_warn("Failed to read m_update from %p",
			    meth->m_update);
			return;
		}
		mdb_printf("m_update %s\n", rgm_defbuf);
	}

	if (meth->m_init != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_init) == -1) {
			mdb_warn("Failed to read m_init from %p",
			    meth->m_init);
			return;
		}
		mdb_printf("m_init %s\n", rgm_defbuf);
	}

	if (meth->m_fini != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_fini) == -1) {
			mdb_warn("Failed to read m_fini from %p",
			    meth->m_fini);
			return;
		}
		mdb_printf("m_fini %s\n", rgm_defbuf);
	}

	if (meth->m_boot != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_boot) == -1) {
			mdb_warn("Failed to read m_boot from %p",
			    meth->m_boot);
			return;
		}
		mdb_printf("m_boot %s\n", rgm_defbuf);
	}

	/* skip monitor_init */

	if (meth->m_monitor_start != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_monitor_start) == -1) {
			mdb_warn("Failed to read m_monitor_start from %p",
			    meth->m_monitor_start);
			return;
		}
		mdb_printf("m_monitor_start %s\n", rgm_defbuf);
	}

	if (meth->m_monitor_stop != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_monitor_stop) == -1) {
			mdb_warn("Failed to read m_monitor_stop from %p",
			    meth->m_monitor_stop);
			return;
		}
		mdb_printf("m_monitor_stop %s\n", rgm_defbuf);
	}

	if (meth->m_monitor_check != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_monitor_check) == -1) {
			mdb_warn("Failed to read m_monitor_check from %p",
			    meth->m_monitor_check);
			return;
		}
		mdb_printf("m_monitor_check %s\n", rgm_defbuf);
	}

	if (meth->m_prenet_start != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_prenet_start) == -1) {
			mdb_warn("Failed to read m_prenet_start from %p",
			    meth->m_prenet_start);
			return;
		}
		mdb_printf("m_prenet_start %s\n", rgm_defbuf);
	}

	if (meth->m_postnet_stop != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)meth->m_postnet_stop) == -1) {
			mdb_warn("Failed to read m_postnet_stop from %p",
			    meth->m_postnet_stop);
			return;
		}
		mdb_printf("m_postnet_stop %s\n", rgm_defbuf);
	}
}


/* Print information on any methods that are in-flight for an RG */
void
rgm_print_methinvoc(methodinvoc_t *meth)
{
	methodinvoc_t *methinvo = meth;
	methodinvoc_t methinvo_val;

	(void) mdb_inc_indent(DEFINDENT);

	while (methinvo != NULL) {
		if (mdb_vread(&methinvo_val, sizeof (methinvo_val),
		    (uintptr_t)methinvo) == -1) {
			mdb_warn("Failed to read the methinvoc from %p",
			    methinvo);
			return;
		}

		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)methinvo_val.mi_tag) == -1) {
			mdb_warn("Failed to read the mi_tag name from %p",
			    methinvo_val.mi_tag);
			return;
		}
		mdb_printf("mi_tag %s\n", rgm_defbuf);

		(void) mdb_inc_indent(DEFINDENT);

		mdb_printf("mi_completed %s\n",
		    rgm_bool_string(methinvo_val.mi_completed));

		(void) mdb_dec_indent(DEFINDENT);

		methinvo = methinvo_val.mi_next;
	}

	(void) mdb_dec_indent(DEFINDENT);
}


/* Print out a value that is a namelist type */
void
rgm_print_namelist(namelist_t *nlp, boolean_t addnewline)
{
	namelist_t nl;

	while (nlp != NULL) {
		if (mdb_vread(&nl, sizeof (namelist_t),
		    (uintptr_t)nlp) == -1) {
			mdb_warn("Failed to read the namelist data from %p",
			    nlp);
			return;
		}

		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)nl.nl_name) == -1) {
			mdb_warn("Failed to read the namelist string from %p",
			    nl.nl_name);
			return;
		}

		mdb_printf("%s ", rgm_defbuf);
		if (addnewline)
			mdb_printf("\n");

		nlp = nl.nl_next;
	}
}

/* Print out a value that is a rdeplist type */
void
rgm_print_rdeplist(rdeplist_t *nlp, boolean_t addnewline)
{
	rdeplist_t nl;

	while (nlp != NULL) {
		if (mdb_vread(&nl, sizeof (rdeplist_t),
		    (uintptr_t)nlp) == -1) {
			mdb_warn("Failed to read the rdeplist data from %p",
			    nlp);
			return;
		}

		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)nl.rl_name) == -1) {
			mdb_warn("Failed to read the rdeplist string from %p",
			    nl.rl_name);
			return;
		}

		mdb_printf("%s ", rgm_defbuf);
		if (addnewline)
			mdb_printf("\n");

		nlp = nl.rl_next;
	}
}

/* Print out a value that is a namelist_all type */
void
rgm_print_namelistall(namelist_all_t *nlp, boolean_t addnewline)
{
	mdb_printf("(is_ALL_value = %s) ", rgm_bool_string(nlp->is_ALL_value));
	rgm_print_namelist(nlp->names, addnewline);
}


/* Print out a value that is a nodeidlist type */
void
rgm_print_nodeidlist(nodeidlist_t *nlp, boolean_t addnewline)
{
	nodeidlist_t nl;

	while (nlp != NULL) {
		if (mdb_vread(&nl, sizeof (nodeidlist_t),
		    (uintptr_t)nlp) == -1) {
			mdb_warn("Failed to read the nodeidlist data from %p",
			    nlp);
			return;
		}

		mdb_printf("%d ", nl.nl_lni);
		if (addnewline)
			mdb_printf("\n");

		nlp = nl.nl_next;
	}
}


/* Print out a value that is a nodeidlist_all type */
void
rgm_print_nodeidlistall(nodeidlist_all_t *nlp, boolean_t addnewline)
{
	mdb_printf("(is_ALL_value = %s) ", rgm_bool_string(nlp->is_ALL_value));
	rgm_print_nodeidlist(nlp->nodeids, addnewline);
}




/* Return a string for the switching state of an RG */
char *
rgm_rgswitch_string(rg_switch_t rgswitch)
{
	switch (rgswitch) {
	case SW_NONE:
		return ("SW_NONE");
	case SW_IN_PROGRESS:
		return ("SW_IN_PROGRESS");
	case SW_UPDATING:
		return ("SW_UPDATING");
	case SW_IN_FAILBACK:
		return ("SW_IN_FAILBACK");
	case SW_BUSTED_RECONF:
		return ("SW_BUSTED_RECONF");
	case SW_UPDATE_BUSTED:
		return ("SW_UPDATE_BUSTED");
	case SW_BUSTED_START:
		return ("SW_BUSTED_START");
	case SW_BUSTED_STOP:
		return ("SW_BUSTED_STOP");
	case SW_EVACUATING:
		return ("SW_EVACUATING");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown rg_switch_t state %d", rgswitch);
		return (rgm_defbuf);
	}
}


/* Return a string for the restart flag of a resource */
char *
rgm_restart_flg_string(rgm::r_restart_t rrflg)
{
	switch (rrflg) {
	case rgm::RR_NONE:
		return ("RR_NONE");
	case rgm::RR_STOPPING:
		return ("RR_STOPPING");
	case rgm::RR_STARTING:
		return ("RR_STARTING");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown r_restart_t %d", rrflg);
		return (rgm_defbuf);
	}
}


/* Return a string for the freeze state of an RG */
char *
rgm_rgfrozen_string(rg_frozen_t rgfrozen)
{
	switch (rgfrozen) {
	case FROZ_FALSE:
		return ("FROZ_FALSE");
	case FROZ_TRUE:
		return ("FROZ_TRUE");
	case FROZ_SET:
		return ("FROZ_SET");
	case FROZ_REMAINS:
		return ("FROZ_REMAINS");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown rg_frozen_t %d", rgfrozen);
		return (rgm_defbuf);
	}
}


/* Return a string for the RG mode */
char *
rgm_rgmode_string(scha_rgmode_t rgmode)
{
	switch (rgmode) {
	case RGMODE_NONE:
		return ("RGMODE_NONE");
	case RGMODE_FAILOVER:
		return ("RGMODE_FAILOVER");
	case RGMODE_SCALABLE:
		return ("RGMODE_SCALABLE");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown scha_rgmode_t %d", rgmode);
		return (rgm_defbuf);
	}
}


/* Return a string for the RT InitNodes property */
char *
rgm_initnodes_string(scha_initnodes_flag_t initnodes)
{
	switch (initnodes) {
	case SCHA_INFLAG_RG_PRIMARIES:
		return ("SCHA_INFLAG_RG_PRIMARIES");
	case SCHA_INFLAG_RT_INSTALLED_NODES:
		return ("SCHA_INFLAG_RT_INSTALLED_NODES");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown scha_initnodes_flag_t %d", initnodes);
		return (rgm_defbuf);
	}
}


/* Return a string for the system defined RTs */
char *
rgm_sysdeftype_string(rgm_sysdeftype_t sysdeftype)
{
	switch (sysdeftype) {
	case SYST_NONE:
		return ("SYST_NONE");
	case SYST_LOGICAL_HOSTNAME:
		return ("SYST_LOGICAL_HOSTNAME");
	case SYST_SHARED_ADDRESS:
		return ("SYST_SHARED_ADDRESS");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown rgm_sysdeftype_t %d", sysdeftype);
		return (rgm_defbuf);
	}
}

/* Return a string for the tunability of a property */
char *
rgm_tunable_string(rgm_tune_t t)
{
	switch (t) {
	case TUNE_NONE:
		return ("TUNE_NONE");
	case TUNE_AT_CREATION:
		return ("TUNE_AT_CREATION");
	case TUNE_ANYTIME:
		return ("TUNE_ANYTIME");
	case TUNE_WHEN_OFFLINE:
		return ("TUNE_WHEN_OFFLINE");
	case TUNE_WHEN_UNMANAGED:
		return ("TUNE_WHEN_UNMANAGED");
	case TUNE_WHEN_UNMONITORED:
		return ("TUNE_WHEN_UNMONITORED");
	case TUNE_WHEN_DISABLED:
		return ("TUNE_WHEN_DISABLED");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown rgm_tune_t %d", t);
		return (rgm_defbuf);
	}
}


/* Return a string for the type of a property */
char *
rgm_proptype_string(scha_prop_type_t pt)
{
	switch (pt) {
	case SCHA_PTYPE_STRING:
		return ("SCHA_PTYPE_STRING");
	case SCHA_PTYPE_INT:
		return ("SCHA_PTYPE_INT");
	case SCHA_PTYPE_BOOLEAN:
		return ("SCHA_PTYPE_BOOLEAN");
	case SCHA_PTYPE_ENUM:
		return ("SCHA_PTYPE_ENUM");
	case SCHA_PTYPE_STRINGARRAY:
		return ("SCHA_PTYPE_STRINGARRAY");
	case SCHA_PTYPE_UINTARRAY:
		return ("SCHA_PTYPE_UINTARRAY");
	case SCHA_PTYPE_UINT:
		return ("SCHA_PTYPE_UINT");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
		    "unknown scha_prop_type_t %d", pt);
		return (rgm_defbuf);
	}
}


/* Return a string for the property class */
char *
rgm_extension_string(boolean_t x)
{
	switch (x) {
	case B_TRUE:
		return ("Extension");
	case B_FALSE:
		return ("System_defined");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
			"unknown extension %d", x);
		return (rgm_defbuf);
	}
}


/* Return a string for a boolean type */
char *
rgm_bool_string(boolean_t b)
{
	switch (b) {
	case B_TRUE:
		return ("TRUE");
	case B_FALSE:
		return ("FALSE");
	default:
		(void) snprintf(rgm_defbuf, sizeof (rgm_defbuf),
			"unknown boolean_t %d", b);
		return (rgm_defbuf);
	}
}


//
// XXX - The following code is copied from rgm_util.cc because there wasn't a
// good way to share this code under the current gate structure.  It is
// unlikely that this code will need to change in the future, though the
// contents of the arrays that are iterated through below might change.
// Those arrays have been factored out into the rgm_state_strings.h file and
// are included in both the code for the rgmd server library and the code in
// the MDB module for the RGM.  The prototype for these functions is in
// rgm_proto.h.
//

//
// Convert RG state enum type to RG state string (for printing).
//
char *
mdb_pr_rgstate(rgm::rgm_rg_state val)
{
	uint_t i;

	for (i = 0; i < RG_ENTRIES; i++) {
		if (rgstatenames[i].rgm_val == val)
			return (rgstatenames[i].statename);
	}
	return (STATE_NOT_FOUND);
}

//
// Convert R state enum type to R state string (for printing).
//
char *
mdb_pr_rstate(rgm::rgm_r_state val)
{
	uint_t i;

	for (i = 0; i < R_ENTRIES; i++) {
		if (rstatenames[i].rgm_val == val)
			return (rstatenames[i].statename);
	}
	return (STATE_NOT_FOUND);
}

//
// Convert R FM status enum type to R FM status string (for printing).
//
char *
mdb_pr_rfmstatus(rgm::rgm_r_fm_status val)
{

	uint_t		i;

	for (i = 0; i < R_FM_ENTRIES; i++) {
		if (rfmstatusnames[i].rgm_val == val)
			return (rfmstatusnames[i].statusname);
	}
	return (FM_STATUS_NOT_FOUND);
}

void
rgm_print_logical_nodes()
{
	LogicalNode *lnPtr;
	char *ln;

	ln = (char *)mdb_alloc(sizeof (LogicalNode), UM_NOSLEEP);
	if (ln == NULL) {
		mdb_warn("Failed to allocate memory\n");
		return;
	}

	(void) mdb_inc_indent(DEFINDENT);
	if (!rgm_verbose)
		mdb_printf("Note: use -v to see UNCONFIGURED nodes\n");

	mdb_printf("nbEntries: %d\n", lnManagerPtr->array.nbEntries);
	mdb_printf("curEntries: %d\n", lnManagerPtr->array.curEntries);
	for (int i = 1; i <= lnManagerPtr->array.nbEntries; i++) {
		if (mdb_vread(&lnPtr, sizeof (LogicalNode *),
		    (uintptr_t)(lnManagerPtr->array.objs + i)) == -1) {
			mdb_warn("Failed to read the LogicalNode ptr from %p",
			    (lnManagerPtr->array.objs + i));
			return;
		}

		if (lnPtr == NULL) {
			continue;
		}
		if (mdb_vread((LogicalNode *)ln, sizeof (LogicalNode),
		    (uintptr_t)lnPtr) == -1) {
			mdb_warn("Failed to read the LogicalNode from %p",
			    lnPtr);
			return;
		}
		rgm_print_logical_node((LogicalNode *)ln);
	}
	mdb_free(ln, sizeof (LogicalNode));
	(void) mdb_dec_indent(DEFINDENT);
}

void
rgm_print_logical_node(LogicalNode *ln)
{
	// skip unconfigured nodes
	if (ln->status < rgm::LN_DOWN && !rgm_verbose) {
		return;
	}

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)ln->fullname) == -1) {
		mdb_warn("Failed to read the fullname string from %p",
		    ln->fullname);
		return;
	}
	mdb_printf("Logical Node %s\n", rgm_defbuf);
	(void) mdb_inc_indent(DEFINDENT);
	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)ln->nodename) == -1) {
		mdb_warn("Failed to read the nodename string from %p",
		    ln->nodename);
		return;
	}
	mdb_printf("Node %s (%d)\n", rgm_defbuf, ln->nodeid);
	if (ln->zonename != NULL) {
		if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
		    (uintptr_t)ln->zonename) == -1) {
			mdb_warn("Failed to read the zonename string from %p",
			    ln->zonename);
			return;
		}
		mdb_printf("Zone %s\n", rgm_defbuf);
	}
	mdb_printf("lni: %d\n", ln->lni);
	mdb_printf("incarnation: %d\n", ln->incarnation);
	mdb_printf("status: %s\n", status_to_string(ln->status));
	if (ln->status_saved) {
		mdb_printf("old_status: %s\n",
		    status_to_string(ln->old_status));
	} else {
		mdb_printf("status not saved\n");
	}

	mdb_printf("evacuating: %s\n", ln->evacuating ? "true" : "false");
	mdb_printf("evac_expires = %ld (%Y)\n",
	    ln->evac_expires, ln->evac_expires);

	if (ln->intention == NULL) {
		mdb_printf("intention is NULL\n");
	} else {
		struct slave_intention si;
		if (mdb_vread(&si,
		    sizeof (struct slave_intention),
		    (uintptr_t)ln->intention) == -1) {
			mdb_warn("Failed to read the slave_intention from %p",
			    ln->intention);
			return;
		}
		rgm_print_intention(&si);
	}

	(void) mdb_dec_indent(DEFINDENT);
}

void
rgm_print_logical_nodeset(LogicalNodeset *nset)
{
	rgm::lni_t lni = 0;

	while ((lni = nset->nextLniSet(lni + 1)) != 0) {
		mdb_printf("%d ", lni);
	}

	mdb_printf("\n");
}

//
// assumes the argument has already been vread, and points into debugger
// vm
void
rgm_print_intention(struct slave_intention *intent)
{

	mdb_printf("intention:\n");
	(void) mdb_inc_indent(DEFINDENT);

	mdb_printf("have_processed: %d\n", intent->have_processed);
	// vread the actual intention
	char *intent_buf = (char *)malloc(sizeof (rgm::rgm_intention));
	rgm::rgm_intention *real_intent = (rgm::rgm_intention *)intent_buf;
	if (mdb_vread(real_intent, sizeof (rgm::rgm_intention),
	    (uintptr_t)intent->intention) == -1) {
		mdb_warn("Failed to read the intention from %p",
		    intent->intention);
		return;
	}

	mdb_printf("Entity: %s\n", rgm_entity_string(real_intent->entity_type));
	mdb_printf("Op: %s\n", rgm_op_string(real_intent->operation_type));

	if (mdb_readstr(rgm_defbuf, sizeof (rgm_defbuf),
	    (uintptr_t)(const char *)real_intent->idlstr_entity_name) == -1) {
		mdb_warn("Failed to read the intention name from %p",
		    (const char *)real_intent->idlstr_entity_name);
		return;
	}
	mdb_printf("Name: %s\n", rgm_defbuf);
	rgm_print_intent_flags(real_intent->flags);

	(void) mdb_dec_indent(DEFINDENT);

}

const mdb_bitmask_t intent_flags[] = {
	{"ACTION", INTENT_ACTION, INTENT_ACTION},
	{"OK_FLAG", INTENT_OK_FLAG, INTENT_OK_FLAG},
	{"SYSTEM_ONLY", INTENT_SYSTEM_ONLY, INTENT_SYSTEM_ONLY},
	{"MANAGE_RG", INTENT_MANAGE_RG, INTENT_MANAGE_RG},
	{"RUN_FINI", INTENT_RUN_FINI, INTENT_RUN_FINI},
	{NULL, 0, 0}
};


void
rgm_print_intent_flags(rgm::intention_flag_t flags)
{
	mdb_printf("Flags: %b\n", flags, intent_flags);
}

const char *
rgm_op_string(rgm::rgm_operation op)
{
	switch (op) {
	case  rgm::RGM_OP_CREATE:
		return ("CREATE");
	case  rgm::RGM_OP_DELETE:
		return ("DELETE");
	case  rgm::RGM_OP_SYNCUP:
		return ("SYNCUP");
	case  rgm::RGM_OP_MANAGE:
		return ("MANAGE");
	case  rgm::RGM_OP_UNMANAGE:
		return ("UNMANAGE");
	case  rgm::RGM_OP_UPDATE:
		return ("UPDATE");
	case  rgm::RGM_OP_ENABLE_R:
		return ("ENABLE_R");
	case  rgm::RGM_OP_DISABLE_R:
		return ("DISABLE_R");
	case  rgm::RGM_OP_FORCE_ENABLE_R:
		return ("FORCE_ENABLE_R");
	case  rgm::RGM_OP_FORCE_DISABLE_R:
		return ("FORCE_DISABLE_R");
	case  rgm::RGM_OP_ENABLE_M_R:
		return ("ENABLE_M_R");
	case  rgm::RGM_OP_DISABLE_M_R:
		return ("DISABLE_M_R");
	default:
		return ("UNKNOWN");
	}
	return ("UNKNOWN");
}

const char *
rgm_entity_string(rgm::rgm_entity e)
{
	switch (e) {
	case rgm::RGM_ENT_RG:
		return ("RG");
	case rgm::RGM_ENT_RT:
		return ("RT");
	case  rgm::RGM_ENT_RS:
		return ("RS");
	default:
		return ("UNKNOWN");
	}

	return ("UNKNOWN");
}

static const char *status_strings[] = {"UNKNOWN", "UNCONFIGURED",
	"DOWN", "STUCK", "SHUTTING_DOWN", "UP"};

//
// suppress cstyle warning about line > 80 characters
// CSTYLED
#define	STATUS_STRINGS_SIZE (sizeof (status_strings) / sizeof (status_strings[0]))

static const char *
status_to_string(rgm::rgm_ln_state state_in)
{
	if (state_in >= STATUS_STRINGS_SIZE) {
		return (status_strings[0]);
	}
	return (status_strings[state_in]);
}
