/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_rgm_get.cc	1.10	08/05/20 SMI"

#define	protected public
#include "mdb_rgm.h"
#include "mdb_names.h"

/*
 * This file contains code to resolve specific symbols within the
 * rgmd (ie, the Rgm_state symbol).
 *
 * More information on the MDB module for the RGMD is in mdb_rgm_readme.txt
 */

/* The global rgm state structure */
rgm_state_t *Rgm_state = NULL; 	/* ptr to module's copy of state structure */
rgm_state_t Rgm_state_val;  /* module's local copy of the state structure */
uintptr_t Rgm_state_addr = NULL; /* rgmd's Rgm_state virtual address */

/* The global lnManager structure */
uintptr_t lnManager_addr = NULL; /* rgmd's lnManager virtual address */

/*
 * module's local copy -- can't declare it on stack because
 * we dont want to execute destructor on exit
 */
LogicalNodeManager *lnManagerPtr = NULL;

/*
 * Initializes the pointer to the global Rgm_state structure.
 *
 * If an address is passed in, we make use of the given address
 * instead of doing a symbol lookup of Rgm_state.  That way, even
 * if the core is slightly screwed up, we may still be able to print
 * the state structure if we can "guess" at its address.
 */
int
rgm_get_state(uintptr_t addr)
{
	if (addr == NULL) {
		if (mdb_readsym(&Rgm_state_addr,
		    sizeof (Rgm_state_addr), "Rgm_state") == -1) {
			mdb_warn("Failed to read variable Rgm_state");
			return (-1);
		}
	} else {
	    Rgm_state_addr = addr;
	}

	if (mdb_vread(&Rgm_state_val, sizeof (rgm_state_t),
	    Rgm_state_addr) == -1) {
		mdb_warn("Failed to read the contents Rgm_state from %p",
		    Rgm_state_addr);
		Rgm_state = NULL;
		Rgm_state_addr = NULL;
		return (-1);
	}

	if (rgm_verbose)
		mdb_printf("Rgm_state = %p; sizeof (rgm_state) = 0t%d\n",
		    Rgm_state_addr, sizeof (rgm_state_t));
	rgm_get_logical_nodeset(&Rgm_state_val.current_membership);
	rgm_get_logical_nodeset(&Rgm_state_val.static_membership);
	Rgm_state = &Rgm_state_val;
	return (0);
}

/*
 * Initializes the pointer to the global lnManager structure.
 */
int
rgm_get_lnManager()
{

	lnManagerPtr =
	    (LogicalNodeManager *)mdb_alloc(sizeof (LogicalNodeManager),
	    UM_NOSLEEP);

	if (mdb_readsym(&lnManager_addr,
	    sizeof (lnManager_addr), "lnManager") == -1) {
		mdb_warn("Failed to read variable lnManager");
		return (-1);
	}

	if (mdb_vread(lnManagerPtr, sizeof (LogicalNodeManager),
	    lnManager_addr) == -1) {
		mdb_warn("Failed to read the contents lnManager from %p",
		    lnManager_addr);
		return (-1);
	}

	if (rgm_verbose)
		mdb_printf("lnManager = %p; sizeof (lnManager) = 0t%d\n",
		    lnManager_addr, sizeof (LogicalNodeManager));

	return (0);
}

void
rgm_free_lnManager()
{
	mdb_free(lnManagerPtr, sizeof (LogicalNodeManager));
}

/* Get the name of the node where the core is from */
void
rgm_get_node_name(char *nbuf, size_t bufsize)
{
	uintptr_t ptr;

	if (bufsize < 1) {
		mdb_warn("Internal error: bufsize for nodename query was %d\n",
		    bufsize);
		return;
	}

	if (mdb_readsym(&ptr, sizeof (ptr), "Nodename") == -1) {
		/*
		 * Silently fail if we can't read the symbol.
		 * This is common when debugging an rgmd on a system
		 * other than where the core was produced.
		 */
		if (rgm_verbose)
			mdb_warn("FYI-only: Failed to read variable Nodename");

		nbuf[0] = '\0';
		return;
	}

	if (mdb_readstr(nbuf, bufsize, ptr) == -1) {
		if (rgm_verbose)
			mdb_warn("FYI-only: Failed to read Nodename from %p",
			    ptr);

		nbuf[0] = '\0';
		return;
	}
}


/* Get the nodeid of the node where the core is from */
void
rgm_get_node_id(sol::nodeid_t *nid, size_t bufsize)
{
	GElf_Sym	sym;
	sol::nodeid_t	*nid_addr = NULL;

	/*
	 * Lookup the value orb_conf::current_node.  These lookups can
	 * fail if the shared object information is not available, and
	 * we fail silently in that case
	 */
	if (mdb_lookup_by_obj(MDB_OBJ_EVERY, MDB_CURRENT_NODE,
	    &sym) == -1) {
		if (rgm_verbose)
			mdb_warn("FYI-only: Failed to lookup by obj "
			    MDB_CURRENT_NODE
			    " (orb_conf::current_node)");
		return;
	}
	nid_addr = (sol::nodeid_t *)sym.st_value; /*lint !e511: Size incompat */

	/* read the value of the nodeid */
	if (mdb_vread(nid, bufsize, (uintptr_t)nid_addr) == -1) {
		if (rgm_verbose)
			mdb_warn("FYI-only: Failed to vread for "
			    MDB_CURRENT_NODE
			    " (orb_conf::current_node) = %p", nid_addr);
		return;
	}
}

/*
 * LogicalNodeset class contains a protected data member words.
 * The function modifies the nset by allocating dynamic memory
 * for words and iteratively copying the contents.
 */
void
rgm_get_logical_nodeset(LogicalNodeset *nset)
{
	uint32_t words;
	uint32_t *wrds = NULL, *w_tmp = NULL;
	int i = 0;

	if (nset->words != NULL) {
		wrds = new uint32_t[nset->nbWords];
		for (i = 0; i < nset->nbWords; ++i) {
			w_tmp = nset->words + i;
			if (mdb_vread(&words, sizeof (uint32_t),
			(uintptr_t)w_tmp) == -1) {
				mdb_warn("Failed to get words value from %p",
				    nset->words);
				mdb_warn("Mdb might dump core when you try to"
				    " print the state infomation");
				return;
			}
			wrds[i] = words;
		}
		nset->words = wrds;
	}
}

//
// rgm_logical_nodeset.cc uses assertfail (via CL_PANIC).
// We don't want to draw in libclos, so provide a dummy definition here
//
int
assertfail(const char *assert_string, const char *file_name, int line_number)
{
	(void) fprintf(stderr, "Assertion failed: %s,\n\tfile: %s, line: %d\n",
	    assert_string, file_name, line_number);

	return (0);
}
