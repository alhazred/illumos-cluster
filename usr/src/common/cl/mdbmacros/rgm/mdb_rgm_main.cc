/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_rgm_main.cc	1.5	08/05/20 SMI"

#include "mdb_rgm.h"

/*
 * This file contains code for the entry points of the dcmds exported
 * in the module.
 *
 * More information on the MDB module for the RGMD is in mdb_rgm_readme.txt
 */


/* print all of the information from the rgm state structure */
int
rgm_state(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *argv)
{
	uintptr_t rgm_state_addr = NULL;
	boolean_t printbrief = B_FALSE;
	rgm_verbose = B_FALSE;

	/* XXX - support for the printbrief option is not yet implemented */
	if (mdb_getopts(argc, argv,
	    'b', MDB_OPT_SETBITS, B_TRUE, &printbrief,
	    'v', MDB_OPT_SETBITS, B_TRUE, &rgm_verbose) != argc)
		return (DCMD_USAGE);

	if (flags & DCMD_ADDRSPEC)
		rgm_state_addr = addr;

	if (rgm_get_state(rgm_state_addr) != 0) {
		mdb_warn("Failed to get Rgm_state\n");
		return (DCMD_ERR);
	}

	if (rgm_get_lnManager() != 0) {
		mdb_warn("Failed to get lnManager\n");
		return (DCMD_ERR);
	}

	rgm_print_state(printbrief);

	rgm_free_lnManager();

	return (DCMD_OK);
}


/* print information about the registered resource groups */
int
rgm_rg(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *argv)
{
	boolean_t printbrief = B_FALSE;
	rgm_verbose = B_FALSE;

	/* XXX - support for the printbrief option is not yet implemented */
	if (mdb_getopts(argc, argv,
	    'b', MDB_OPT_SETBITS, B_TRUE, &printbrief,
	    'v', MDB_OPT_SETBITS, B_TRUE, &rgm_verbose) != argc)
		return (DCMD_USAGE);

	if (flags & DCMD_ADDRSPEC)
		rgm_print_rgs(addr, B_FALSE, printbrief);
	else
		rgm_print_rgs(NULL, B_TRUE, printbrief);

	return (DCMD_OK);
}


/* print information about the registered resources */
int
rgm_rs(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *argv)
{
	boolean_t printbrief = B_FALSE;
	rgm_verbose = B_FALSE;

	/* XXX - support for the printbrief option is not yet implemented */
	if (mdb_getopts(argc, argv,
	    'b', MDB_OPT_SETBITS, B_TRUE, &printbrief,
	    'v', MDB_OPT_SETBITS, B_TRUE, &rgm_verbose) != argc)
		return (DCMD_USAGE);

	if (flags & DCMD_ADDRSPEC)
		rgm_print_rss(addr, B_FALSE, printbrief);
	else
		rgm_print_rss(NULL, B_TRUE, printbrief);

	return (DCMD_OK);
}


/* print information about the registered resource types */
int
rgm_rt(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *argv)
{
	boolean_t printbrief = B_FALSE;
	rgm_verbose = B_FALSE;

	/* XXX - support for the printbrief option is not yet implemented */
	if (mdb_getopts(argc, argv,
	    'b', MDB_OPT_SETBITS, B_TRUE, &printbrief,
	    'v', MDB_OPT_SETBITS, B_TRUE, &rgm_verbose) != argc)
		return (DCMD_USAGE);

	if (flags & DCMD_ADDRSPEC)
		rgm_print_rts(addr, B_FALSE, printbrief);
	else
		rgm_print_rts(NULL, B_TRUE, printbrief);

	return (DCMD_OK);
}


/* print the rgm debug buffer */
int
rgm_buf(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *argv)
{
	GElf_Sym sym;
	char **bufpp_addr = NULL;
	char *bufp_addr = NULL;
	char *buf_addr = NULL;
	char *sizebuf_ptr = NULL;
	char *dbg_buf;
	char *dbg_buf_end = NULL;
	char *head = NULL;
	char *tail = NULL;
	rgm_verbose = B_FALSE;
	int size = 0;

	if (mdb_getopts(argc, argv,
	    'v', MDB_OPT_SETBITS, B_TRUE, &rgm_verbose) != argc)
		return (DCMD_USAGE);

	/* XXX - Future: use address specified instead of doing lookup */
	if (flags & DCMD_ADDRSPEC)
		mdb_printf("Specifying address %p is not currently supported\n",
		    addr);

	/*
	 * Get the value of rgm_dbg_buf
	 */
	if (mdb_lookup_by_obj(MDB_OBJ_EVERY, "rgm_dbg_buf", &sym) == -1) {
		mdb_warn("failed to lookup by obj rgm_dbg_buf");
		return (DCMD_ERR);
	}

	bufpp_addr = (char **)sym.st_value; /*lint !e511 */

	/* read the value of the pointer */
	if (mdb_vread(&bufp_addr, sizeof (char *),
	    (uintptr_t)bufpp_addr) == -1) {
		mdb_warn("failed to vread for *rgm_dbg_buf = %p\n", bufp_addr);
		return (DCMD_ERR);
	}

	/*
	 * The debug buffer structure:
	 * See common/cl/sys/dbg_printf.hdbgbuf_info_t for more
	 * information
	 *
	 * char *buf: Start of buffer
	 * char *buf_end: Current pointer into buffer
	 * dbgbuf_info_t *c_ptr
	 * const unsigned int buf_size: Size of the buffer
	 *
	 */
	sizebuf_ptr = bufp_addr + (3 *sizeof (char *));

	/* read the size of the debug buffer */
	if (mdb_vread(&size, sizeof (uint_t), (uintptr_t)sizebuf_ptr) == -1) {
		mdb_warn("failed to vread the buffer size = %p\n", sizebuf_ptr);
		return (DCMD_ERR);
	}

	if (rgm_verbose) {
		mdb_printf("rgm_dbg_buf size(%p) = %d\n", sizebuf_ptr, size);
	}

	/* read the value of the pointer */
	if (mdb_vread(&buf_addr, sizeof (char *), (uintptr_t)bufp_addr) == -1) {
		mdb_warn("failed to vread for **rgm_dbg_buf = %p\n", buf_addr);
		return (DCMD_ERR);
	}

	dbg_buf = (char *)mdb_alloc(size, UM_NOSLEEP);

	/* read in the entire dbg buf */
	if (mdb_vread(dbg_buf, size, (uintptr_t)buf_addr) == -1) {
		mdb_warn("failed to vread for whole **rgm_dbg_buf = %p\n",
		    buf_addr);
		return (DCMD_ERR);
	}

	dbg_buf[size - 1] = '\0';
	dbg_buf_end = dbg_buf + size;

	if (rgm_verbose) {
		mdb_printf("rgm_dbg_buf=%p; *rgm_dbg_buf=%p; "
		    "ring buffer size 0x%x (0t%d)\n",
		    bufp_addr, buf_addr, size, size);
	}

	/* Start reading at the beginning of the rgm_dbg_buf array */
	head = dbg_buf;

	/* Find first NULL entry */
	while (*head != '\0' && head < dbg_buf_end) {
		head++;
	}

	tail = head;

	if (rgm_verbose) {
		mdb_printf("rgm_dbg_buf wraps at %p; rgm_dbg_buf ends at %p\n",
		    (tail - dbg_buf) + buf_addr + 1,
		    buf_addr + size);
	}

	/* Find next non-NULL entry */
	while (*head == '\0' && head < dbg_buf_end) {
		head++;
	}

	/* Print from the head to the buf_end if there is anything there */
	if (head < dbg_buf_end) {
		mdb_printf("%s\n", head);
	} else {
		if (rgm_verbose) {
			mdb_printf(" (there is nothing before the wrap)\n");
		}
	}

	if (rgm_verbose) {
		mdb_printf("----------- wrap to top of buffer ------------\n");
	}

	/* Print the buf beginning to the tail (first null) */
	mdb_printf("%s", dbg_buf);

	mdb_free(dbg_buf, size);

	return (DCMD_OK);
}
