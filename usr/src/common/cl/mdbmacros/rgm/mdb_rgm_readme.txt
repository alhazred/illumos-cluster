/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_rgm_readme.txt	1.2	08/05/20 SMI"

==================== MDB module for RGMD =============================
The functionality in the rgmd mdb module can be accessed by ::load'ing
/usr/cluster/dtk/lib/mdb/U/mdb_cluster.so , which is installed as part of the
SUNWscdtk package. There is some minimal documentation about the rgm module in
the mdb_cluster.so(5) manpage, which ships with the diagnostic toolkit package,
but the description and examples here are probably more helpful.

The tool mdbgen will generate mdb dcmds from .h files. mdbgen can be used to add
mdb support for printing structures that are not currently printed by the rgmd
module.
 
Supported DCMDS for the RGMD
::rgm_state

    [rgm_state_t *]::rgm_state [-v]

    Print out the state structure of the rgmd, including information on all
    resource groups, resources and resource types. Optionally, an rgm_state_t *
    can be specified, and that address will be used instead of doing a symbol
    lookup of the Rgm_state variable. The output printed here includes the
    information printed by scrgadm -pvv and scstat -g. Consult the header file
    rgm_state.h for information on the data displayed. The -v option enables
    printing of messages that might be helpful for debugging the module.

::rgm_rg

    [rglist_t *]::rgm_rg [-v]

    Print information about the resource groups that are configured, including
    the resources contained in each of them. Optionally, an rglist_t * can be
    specified, and only the information for the resource group at that address
    will be printed. The -v option enables printing of messages that might be
    helpful for debugging the module.

::rgm_rs

    [rlist_t *]::rgm_rs [-v]

    Print information about the resources that are configured. Optionally, an
    rlist_t * can be specified, and only the information for the resource at
    that address will be printed. The -v option enables printing of messages
    that might be helpful for debugging the module.

::rgm_rt

    [rgm_rt_t *]::rgm_rt [-v]

    Print information about the resource types that are configured. Optionally,
    an rgm_rt_t * can be specified, and only the information for the resource
    type at that address will be printed.  The -v option enables printing of
    messages that might be helpful for debugging the module.

::rgm_buf

    ::rgm_buf [-v]

    Print the rgmd debug buffer (rgm_dbg_buf), including the portion of the
    buffer before the buffer wraps.  The -v option ("verbose") prints out
    information about where the buffer is located in memory and where the
    buffer wraps.

Supported walkers for the RGMD
::walk rg

    [rglist_t *]::walk rg

    Walk over all resource groups. Optionally, an rglist_t * can be specified,
    and the walk will start with the resource group at that address and will
    continue over all resource groups that are reachable by following the given
    resource group's rgl_next pointer.

::walk rs

    [rlist_t *]::walk rs

    Walk over all resources. Optionally, an rlist_t * can be specified, and the
    walk will start with the resource at that address and will continue over all
    resources that are reachable by following the given resource's rl_next
    pointer. This means that a local walk of resources will be limited to the
    resources that are in the same resource group.

::walk rt

    [rgm_rt_t *]::walk rt

    Walk over all resource types. Optionally, an rgm_rt_t * can be specified,
    and the walk will start with the resource type at that address and will
    continue over all resource types that are reachable by following the given
    resource type's rt_next pointer.
     

Known issues and limitations
1.  We don't know if the core is from the rgmd president, which means that the
    states printed may not be valid.
    
    We have no way of knowing if the core is from the rgmd that was the
    president. This is relevant because the state information from the resources
    and resource groups on the president node contains the up-to-date state on
    all nodes. However, an rgmd core from the slave node only has the up-to-date
    state for its own node.
    
    Currently, I look at the current membership (Rgm_state->current_membership)
    and print out the state on all nodes that were cluster members, regardless
    if the core is from the president (where all these states would be valid)
    or from a slave (where some of these states would be invalid). The nodeid
    of the president is known (Rgm_state->rgm_President), so if you know that
    the core is not from the same nodeid as the president nodeid, then you can
    manually filter out the data that is invalid.

    I try to use the value of orb_conf::current_node to get the nodeid, but this
    can fail if the shared library information is not accessible.


2.  Sometimes mdb cannot resolve the symbol Nodename.

    On certain cores, mdb cannot find the Nodename symbol that is declared in
    rgm_util.cc. If the symbol can't be resolved, I print out the output of
    ::status which includes the nodename.


Future enhancements
    I would like to compress the output of the commands and have more
    flexibility in selecting what output is displayed.
    Please let us know if you have any specific requests or ideas in this area.
 
