/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)tr_manager.cc	1.7	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/sysinfo.h>
#include <orb/object/schema.h>
#include <sys/list_def.h>
#include <orb/xdoor/translate_mgr.h>
#include <orb/xdoor/Xdoor.h>
#include "tr_mgr_list_def_in.h"
#include "mdb_names.h"

//
// tr_out_info method needed only for mdb
//
uint_t
tr_out_info::mdb_get_xdoor_count()
{
	return (count);
}


//
// tr_out_info
//
//   mdb dcmd entry point Every node has a translate manager which
//   contains a list per node of xdoors translated out to other nodes
//   in the cluster.  This dcmd dumps the number of xdoors, their
//   tr_out_ids and the node numbers.
//
extern "C" int
tr_out_info_print(uintptr_t, uint_t, int, const mdb_arg_t *)
{
	translate_mgr	*tr_mgrp;
	GElf_Sym 	sym;

	if (mdb_lookup_by_name(MDB_THE_TRANSLATE_MANAGER,
	    &sym) != 0) {
		mdb_warn("Failed to find translate_manager\n");
		return (DCMD_ERR);
	}
#ifdef MDB_DEBUG
	else {
		mdb_printf("Found the_translate_manager at addr \n");
	}
#endif

	tr_mgrp = (translate_mgr *)mdb_alloc(sizeof (translate_mgr), UM_SLEEP);

	if (mdb_readvar(tr_mgrp, MDB_THE_TRANSLATE_MANAGER)
	    != (int)sizeof (translate_mgr)) {
		mdb_warn("Failed to find the_translate_manager\n");
		return (DCMD_ERR);
	}

#ifdef MDB_DEBUG
	else {
		mdb_printf("Read the_translate_manager successfully\n");
	}
#endif

	tr_mgrp->mdb_dump_tr_nodes();
	return (DCMD_OK);
}

//
// translate_mgr::mdb_dump_tr_nodes
//
//	Prints in_transit list for every node in the cluster
//
void
translate_mgr::mdb_dump_tr_nodes()
{
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if (tr_nodes[i].incn != 0) {
			mdb_printf("---------------------------------------\n");
			mdb_printf("Nodeid: %d Incarnation: %d\n", i,
			    tr_nodes[i].incn);
			mdb_printf("---------------------------------------\n");
			mdb_printf("  %-12s \t %-8s\n", "tr_outid", "# xdoors");
			mdb_printf("---------------------------------------\n");
			tr_nodes[i].mdb_dump_tr_out_info();
		}
	}
}

//
// translate_mgr::tr_node::mdb_dump_tr_out_info
//
//	Iterates over the in_transit list and prints tr_outids and number of
//	xdoors.
//
void
translate_mgr::tr_node::mdb_dump_tr_out_info()
{
	IntrList<tr_out_info, _DList>::ListIterator iter(in_transit);
	tr_out_info	*trp = NULL;

	tr_out_info 	*tr_out_infop = (tr_out_info *)mdb_alloc(
	    sizeof (tr_out_info), UM_SLEEP);

	while ((trp = iter.mdb_get_current()) != NULL) {
		if (mdb_vread((void *)tr_out_infop, sizeof (tr_out_info),
		    (uintptr_t)trp) != (ssize_t)sizeof (tr_out_info)) {
			mdb_warn("Failed to read tr_out_info\n");
			mdb_free(tr_out_infop, sizeof (tr_out_info));
			return;
		}
		mdb_printf("  %p \t %4d\n", trp,
		    tr_out_infop->mdb_get_xdoor_count());

		iter.mdb_advance();
	}
	mdb_free(tr_out_infop, sizeof (tr_out_info));
}
