//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mdb_fs.cc	1.8	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/sysinfo.h>

#include <sys/list_def.h>
#include "mdb_names.h"
#include "fobj_list_def_in.h"
#include "pxfobj_list_def_in.h"

#include <pxfs/server/fs_base_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/client/pxvfs.h>
#include <pxfs/client/pxfobj.h>

//
// mdb_fobj_ii_list - walk a list of fobj's.
// For each fobj print the address and type.
//
// Return zero upon success, otherwise a non-zero value
//
int
mdb_fobj_ii_list_v1(fobj_ii_list_t *fobj_ii_listp, fs_base_ii *fs_base_iip,
    int &file_count)
{
	fobj_ii		*fobjp;

	//
	// Note that we allocate space only for the base class,
	// as we only need info from the base class.
	//
	fobj_ii		*fobj_iip = (fobj_ii *)mdb_alloc(sizeof (fobj_ii),
					    UM_SLEEP);

	fobj_ii_list_t::ListIterator	myiter(fobj_ii_listp);

	while ((fobjp = myiter.mdb_get_current()) != NULL) {
		if (mdb_vread((void *)fobj_iip, sizeof (fobj_ii),
		    (uintptr_t)fobjp) != (ssize_t)sizeof (fobj_ii)) {
			mdb_warn("Failed to read fobj_ii\n");
			mdb_free(fobj_iip, sizeof (fobj_ii));
			return (1);	// return error
		}
		if (fs_base_iip != fobj_iip->fs_implp) {
			//
			// File does not belong to this file system.
			// Probably do not have a valid address for fs
			//
			mdb_warn("File %p has fs_implp %p instead of %p\n",
			    fobjp, fobj_iip->fs_implp, fs_base_iip);
			mdb_free(fobj_iip, sizeof (fobj_ii));
			return (1);	// return error
		}
		//
		// Output address of server file object and its type
		//
		mdb_printf("%p\t%d\n", fobjp, fobj_iip->get_ftype());
		file_count++;
		myiter.mdb_advance();
	}
	mdb_free(fobj_iip, sizeof (fobj_ii));
	return (0);		// return success
}

//
// fs_server_files - for both the fidlist and hash table
// print the address and type of the server file object.
//
// mdb dcmd entry point
//
extern "C" int
fs_server_files_v1(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *)
{
#ifdef MDB_DEBUG
	mdb_printf("fs_server_files addr 0x%x\n", addr);
#endif

	if ((flags & DCMD_ADDRSPEC) == 0) {
		mdb_warn("Failed to specify address of fs_impl\n");
		return (DCMD_ERR);
	}

	if (argc != 0) {
		mdb_warn("no arguments allowed\n");
		return (DCMD_ERR);
	}

	fs_base_ii	*fs_base_iip = (fs_base_ii *)mdb_alloc(
			    sizeof (fs_base_ii) + 8, UM_SLEEP);

	if (mdb_vread((void *)fs_base_iip, sizeof (fs_base_ii), addr)
	    != (ssize_t)sizeof (fs_base_ii)) {
		mdb_warn("Failed to read fs_base_ii\n");
		return (DCMD_ERR);
	}

	//
	// Check whether we are working with a valid file system object
	//

	GElf_Sym gelf_sym;
	char	 sym[MDB_SYM_NAMLEN];
	uintptr_t vtbl_ptr;

	if (mdb_vread((void *)&vtbl_ptr, sizeof (uintptr_t), addr)
	    != (ssize_t)sizeof (uintptr_t)) {
		mdb_warn("Failed to read fs vtbl ptr.\n");
		mdb_free(fs_base_iip, sizeof (fs_base_ii) + 8);
		return (DCMD_ERR);
	}

	// Read in the symbol it points to
	if (mdb_lookup_by_addr(vtbl_ptr, MDB_SYM_EXACT, sym,
	    (ssize_t)MDB_SYM_NAMLEN, &gelf_sym)) {
		mdb_warn("Failed fs vtbl symbol not found\n");
		mdb_free(fs_base_iip, sizeof (fs_base_ii) + 8);
		return (DCMD_ERR);
	}

#ifdef MDB_DEBUG
	mdb_printf("fs 0x%x vtbl 0x%x sym %s\n", addr, vtbl_ptr, sym);
#endif

	fs_base_ii	*base_iip;
	uintptr_t	base_addr;

	if (strstr(sym, "fs_repl_impl")) {
		//
		// The fs_base_ii class is embedded within the
		// fs_repl_impl class at an offset of +8
		//
		base_iip = (fs_base_ii *)((char *)fs_base_iip + 8);
		base_addr = addr + 8;

	} else if (strstr(sym, "fs_norm_impl")) {
		//
		// The fs_base_ii class is embedded within the
		// fs_norm_impl class at an offset of +0
		//
		base_iip = fs_base_iip;
		base_addr = addr;

	} else {
		mdb_warn("Failed vtbl neither fs_repl_impl nor fs_norm_impl\n");
		mdb_free(fs_base_iip, sizeof (fs_base_ii) + 8);
		return (DCMD_ERR);
	}

	int	result = base_iip->mdb_walk_files((fs_base_ii *)base_addr);
	mdb_free(fs_base_iip, sizeof (fs_base_ii) + 8);
	return (result);
}

//
// mdb_walk_files - generate a report identifying all of the active files
// belonging to this file system.
//
int
fs_base_ii::mdb_walk_files(fs_base_ii *fs_basep)
{
#ifdef MDB_DEBUG
	mdb_printf("mdb_walk_files this %p vtbl %x fidlist %p\n",
	    this, *((uint_t *)this), &fidlist);
#endif
	int	file_count = 0;

	//
	// Traverse the list of files that are not primary ready
	//
	mdb_printf("fidlist\n");
	mdb_printf("fobj_ii\tfobjtype\n");
	if (mdb_fobj_ii_list_v1(&fidlist, fs_basep, file_count) != 0) {
		return (DCMD_ERR);
	}
	mdb_printf("Total server files in fidlist: %d\n", file_count);

	file_count = 0;

	size_t	size_buckets = sizeof (fobj_ii_list_t) * allfobj_buckets;

	//
	// Read in the buckets for the hash table
	//
	fobj_ii_list_t	*bucketp = (fobj_ii_list_t *)mdb_alloc(size_buckets,
					    UM_SLEEP);

	if (mdb_vread((void *)bucketp, size_buckets, (uintptr_t)allfobj_list)
	    != (ssize_t)size_buckets) {
		mdb_warn("Failed to read allfobj_list\n");
		mdb_free(bucketp, size_buckets);
		return (DCMD_ERR);
	}

	//
	// Current bucket being traversed
	//
	fobj_ii_list_t  *curr_bucketp = bucketp;

	//
	// Traverse the list of files that are primary ready
	//
	mdb_printf("allfobj_list\n");
	mdb_printf("fobj_ii\t\tfobjtype\n");

	for (uint_t i = 0; i < allfobj_buckets; i++) {
		if (mdb_fobj_ii_list_v1(curr_bucketp, fs_basep,
		    file_count) != 0) {
			mdb_free(bucketp, size_buckets);
			return (DCMD_ERR);
		}
		curr_bucketp++;
	}
	mdb_printf("Total server files in hash table: %d\n", file_count);

	mdb_free(bucketp, size_buckets);
	return (DCMD_OK);
}

//
// mdb_pxfobj_list - walk the list of pxfobj's in this bucket.
// For each pxfobj print the address, type, and vnode reference count.
//
// Return zero upon success, otherwise a non-zero value
//
int
mdb_pxfobj_list_v1(pxfobj_list_t *pxfobj_listp, vfs_t *vfsp, int &file_count)
{
	pxfobj		*px_fobjp;

	//
	// Note that we allocate space only for the base class,
	// as we only need info from the base class.
	//
	pxfobj		*pxfobjp = (pxfobj *)mdb_alloc(sizeof (pxfobj),
					    UM_SLEEP);

	//
	// The vnode is contained within the pxfobj for Solaris 8 & 9.
	// Starting with Solaris 10 pxfobj points to the vnode.
	// In both cases it is safe to read in the vnode by itself.
	//
	vnode_t		*pxvnodep = (vnode_t *)mdb_alloc(sizeof (vnode_t),
					    UM_SLEEP);

	//
	// The address of the vnode in the target address space.
	//
	vnode_t		*vnode_addressp = NULL;

	pxfobj_list_t::ListIterator	myiter(pxfobj_listp);

	while ((px_fobjp = myiter.mdb_get_current()) != NULL) {
		if (mdb_vread((void *)pxfobjp, sizeof (pxfobj),
		    (uintptr_t)px_fobjp) != (ssize_t)sizeof (pxfobj)) {
			mdb_warn("Failed to read pxfobj %p\n", px_fobjp);
			mdb_free(pxfobjp, sizeof (pxfobj));
			mdb_free(pxvnodep, sizeof (vnode_t));
			return (1);	// return error
		}

#ifdef FSI
		// The proxy file object has a pointer to the vnode
		vnode_addressp = pxfobjp->get_vp();
#else // FSI not defined
		// The vnode is included in the proxy file object
		vnode_addressp = px_fobjp->get_vp();
#endif	// FSI

		if (mdb_vread((void *)pxvnodep, sizeof (vnode_t),
		    (uintptr_t)vnode_addressp) !=
		    (ssize_t)sizeof (vnode_t)) {
			mdb_warn("Failed to read vnode for pxfobj %p\n",
			    vnode_addressp);
			mdb_free(pxfobjp, sizeof (pxfobj));
			mdb_free(pxvnodep, sizeof (vnode_t));
			return (1);
		}
		if (vfsp == pxvnodep->v_vfsp) {
			//
			// File belongs to this proxy file system.
			//
			// Output address of client file object and its type
			//
			mdb_printf("%p\t%p\t%d\t%d\t%hx\n",
			    px_fobjp, vnode_addressp, pxvnodep->v_type,
			    pxvnodep->v_count, pxfobjp->get_pxflags());
			file_count++;
		}
		myiter.mdb_advance();
	}
	mdb_free(pxfobjp, sizeof (pxfobj));
	mdb_free(pxvnodep, sizeof (vnode_t));
	return (0);		// return success
}

//
// fs_client_files - print the address and type of each client file object.
//
// mdb dcmd entry point
//
extern "C" int
fs_client_files_v1(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *)
{
#ifdef MDB_DEBUG
	mdb_printf("fs_client_files addr 0x%x\n", addr);
#endif

	if ((flags & DCMD_ADDRSPEC) == 0) {
		mdb_warn("Failed to specify address of pxvfs\n");
		return (DCMD_ERR);
	}

	if (argc != 0) {
		mdb_warn("no arguments allowed\n");
		return (DCMD_ERR);
	}

	pxvfs	*pxvfsp = (pxvfs *)mdb_alloc(sizeof (pxvfs), UM_SLEEP);

	if (mdb_vread((void *)pxvfsp, sizeof (pxvfs), addr)
	    != (ssize_t)sizeof (pxvfs)) {
		mdb_warn("Failed to read pxvfs\n");
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

	//
	// Check whether we are working with a valid pxvfs
	//

	GElf_Sym gelf_sym;
	char	 sym[MDB_SYM_NAMLEN];
	uintptr_t vtbl_ptr;

	if (mdb_vread((void *)&vtbl_ptr, sizeof (uintptr_t), addr)
	    != (ssize_t)sizeof (uintptr_t)) {
		mdb_warn("Failed to read pxvfs vtbl ptr.\n");
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

	// Read in the symbol it points to
	if (mdb_lookup_by_addr(vtbl_ptr, MDB_SYM_EXACT, sym,
	    (ssize_t)MDB_SYM_NAMLEN, &gelf_sym)) {
		mdb_warn("Failed pxvfs vtbl symbol not found\n");
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

#ifdef MDB_DEBUG
	mdb_printf("pxvfs 0x%x vtbl 0x%x sym %s\n", addr, vtbl_ptr, sym);
#endif

	if (!strstr(sym, "pxvfs")) {
		mdb_warn("Failed vtbl not for pxvfs\n");
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

	vfs_t	*vfsp = pxvfsp->get_vfsp();

	//
	// Determine the size of the hash table
	//
	uint_t	hash_size;

	if (mdb_readsym((void *)&hash_size, sizeof (uint_t),
	    MDB_PXFOBJHSZ) != (int)sizeof (uint_t)) {
		mdb_warn("Failed to find and read pxfobjhsz\n");
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

#ifdef MDB_DEBUG
	mdb_printf("pxfobjhsz 0x%x sym %s\n", hash_size, MDB_PXFOBJHSZ);
#endif

	size_t	size_buckets = hash_size * sizeof (pxvfs::pxfobj_hash_bkt);

	//
	// Determine the address of the hash table
	//
	uintptr_t	addr_hash;

	if (mdb_readsym((void *)&addr_hash, sizeof (addr_hash),
	    MDB_PXFOBJ_HASH) != (int)sizeof (addr_hash)) {
		mdb_warn("Failed to find and read address in pxfobj_hash\n");
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

	//
	// Read in the buckets for the hash table
	//
	pxvfs::pxfobj_hash_bkt	*bucketp = (pxvfs::pxfobj_hash_bkt *)mdb_alloc(
		size_buckets, UM_SLEEP);

	if (mdb_vread((void *)bucketp, size_buckets, addr_hash)
	    != (ssize_t)size_buckets) {
		mdb_warn("Failed to read pxfobj_hash table\n");
		mdb_free(bucketp, size_buckets);
		mdb_free(pxvfsp, sizeof (pxvfs));
		return (DCMD_ERR);
	}

	int	file_count = 0;

	//
	// Current bucket being traversed
	//
	pxvfs::pxfobj_hash_bkt  *curr_bucketp = bucketp;

	//
	// Traverse the lists of client file objects on this node.
	// We are only interested in the files belonging to the
	// specified proxy file system.
	//
	mdb_printf("pxfobj\t\tvnode\t\tv_type\tv_count\tpxflags\n");

	for (uint_t i = 0; i < hash_size; i++) {
		if (mdb_pxfobj_list_v1(&curr_bucketp->hlist, vfsp, file_count)
		    != 0) {
			mdb_free(bucketp, size_buckets);
			mdb_free(pxvfsp, sizeof (pxvfs));
			return (DCMD_ERR);
		}
		curr_bucketp++;
	}

	mdb_printf("Total client files: %d\n", file_count);

	mdb_free(bucketp, size_buckets);
	mdb_free(pxvfsp, sizeof (pxvfs));
	return (DCMD_OK);
}
