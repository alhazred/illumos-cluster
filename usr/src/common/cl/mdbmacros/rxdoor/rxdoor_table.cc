/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rxdoor_table.cc	1.20	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/sysinfo.h>

#include <orb/handler/remote_handler.h>
#include <orb/object/schema.h>

#include <orb/xdoor/rxdoor_mgr.h>
#include <orb/xdoor/standard_xdoor.h>
#include <orb/xdoor/simple_xdoor.h>

#include <repl/rma/hxdoor.h>
#include <sys/list_def.h>
#include "rxdoor_list_def_in.h"
#include "rxdoor_table.h"
#include "mdb_names.h"

// Global variables
int		print_summary;

// counters for different type of handlers
static uint_t num_std;
static uint_t num_combo;
static uint_t num_bulkio;
static uint_t num_solobj;
static uint_t num_ckpt;
static uint_t num_repl;
static uint_t num_gtwy;

enum handler_type_t   { STD_CLIENT,
			STD_SERVER,
			COMBO,
			CKPT,
			BULKIO,
			SOLOBJ,
			REPLICA,
			GATEWAY,
			UNKWN};

// Function prototypes
static handler_type_t	mdb_get_handler_type_count(char *name);
static void		mdb_read_handler_at_addr(rxdoor *rxdp, xdoor_id id,
			    char *hdl_type, uintptr_t handlerp);
static ssize_t		mdb_get_xdoor_size(rxdoor *rxdp);

//
// rxdoor_table
//
//   mdb dcmd entry point
//
extern "C" int
rxdoor_table(uintptr_t, uint_t, int argc, const mdb_arg_t *argv)
{
	rxdoor_manager	*rxd_mgrp;
	uintptr_t	addr;

	// intializations
	print_summary = 0;
	num_std = 0;
	num_combo = 0;
	num_bulkio = 0;
	num_solobj = 0;
	num_ckpt = 0;
	num_repl = 0;
	num_gtwy = 0;

	// process args

	if (mdb_getopts(argc, argv,
	    's', MDB_OPT_SETBITS, TRUE, &print_summary, NULL) != argc) {
		return (DCMD_USAGE);
	}

	rxd_mgrp = (rxdoor_manager *)mdb_alloc(sizeof (rxdoor_manager),
	    UM_SLEEP);

	if (mdb_readsym(&addr, sizeof (addr),
	    MDB_THE_RXDOOR_MANAGER) != (int)sizeof (addr)) {
		mdb_warn("Failed to find the_rxdoor_manager\n");
		return (DCMD_ERR);
	}

#ifdef MDB_DEBUG
	mdb_printf("Found the_rxdoor_manager at addr 0x%x\n", addr);
#endif

	if (mdb_vread((void *)rxd_mgrp, sizeof (rxdoor_manager), addr)
	    != (ssize_t)sizeof (rxdoor_manager)) {
		mdb_warn("Failed to read rxdoor_manager\n");
		return (DCMD_ERR);
	} else {
		rxd_mgrp->mdb_dump_table();
		return (DCMD_OK);
	}
}
//
// mdb_get_handlers()
//
//    MDB helper function to return handler pointers.
//
void
rxdoor::mdb_get_handlers(handler **h1, handler **h2)
{
	*h1 = handlers[0];
	*h2 = handlers[1];
}

// mdb_dump_bucket
//
//   A bucket in rxdoor node points (logically) to a set of lists. Dumping
//   a bucket means dumping all lists associated with the bucket.
//   The lists contain xdoors.
//
ssize_t
rxdoor_node::mdb_dump_bucket(rx_list *listp, int *num_xdoorsp)
{
	ssize_t			memused = 0;
	rxdoor			*xdp = NULL;
	rx_list::ListIterator	li;
	int 			ndoors = 0;

	li.reinit(listp);
	//
	// Note here that we allocate size = rxdoor. Derived classes are
	// bigger than rxdoor base class. Since the info we print here
	// belongs to base class only, we do not bother reading the entire
	// actual rxdoor.If that changes, we need to allocate and read in the
	// correct size.
	//
	rxdoor *rxd = (rxdoor *)mdb_alloc(sizeof (rxdoor), UM_SLEEP);

	while ((xdp = li.mdb_get_current()) != NULL) {
		ssize_t sz = mdb_get_xdoor_size(xdp);

		memused += sz;

		if (mdb_vread((void *)rxd, sizeof (rxdoor),
		    (uintptr_t)xdp) != (ssize_t)sizeof (rxdoor)) {
			mdb_warn("Failed to read xdoor\n");
			mdb_free(rxd, sizeof (rxdoor));
			return (ssize_t(0));
		}
		ndoors++;
		//
		// Every xdoor has user handler and a kernel handler.
		// handlers[0] contains the user handler.
		// handlers[1] contains the kernel handler.
		// Most xdoors do not have both handlers. So far only
		// gateway handler has a user handler and a kernel handler.
		//
		handler *clienth;
		handler *serverh;

		rxd->mdb_get_handlers(&clienth, &serverh);
#ifdef MDB_DEBUG
		mdb_printf("Rxd = 0x%x\n", xdp);
#endif
		if (clienth) {
			mdb_read_handler_at_addr(xdp, rxd->server_xdoor,
			    "User", (uintptr_t)clienth);
		}

		if (serverh) {
			mdb_read_handler_at_addr(xdp, rxd->server_xdoor,
			    "Kern", (uintptr_t)serverh);
		}

		li.mdb_advance();
	}

	mdb_free(rxd, sizeof (rxdoor));

	*num_xdoorsp += ndoors;
	return (memused);
}

//
// mdb_dump_node
//
//   A node consists of an array of buckets; each bucket points to a set of
//   lists. To dump a node, dump all lists pointed to by all buckets.
//
ssize_t
rxdoor_node::mdb_dump_node()
{
	uint_t		i, j;
	ssize_t		memused = sizeof (rxdoor_node);
	rx_list		*listp = (rx_list *)mdb_alloc(
	    (num_lists * sizeof (rx_list)), UM_SLEEP);

	// read in lists array
	if (mdb_vread((void *)listp, num_lists * sizeof (rx_list),
	    (uintptr_t)lists) != (ssize_t)(num_lists * sizeof (rx_list))) {
		mdb_warn("Failed to read rxdoor_node list.\n");
		mdb_free(listp, num_lists * sizeof (rx_list));
		return (ssize_t(0));
	}


	if (!print_summary) {
		mdb_printf("Incarnation: %ld\n", node.incn);
		mdb_printf(PRINT_FORMAT, "Xdoor ", "   ID", "Handler",
		    "Handler", "Handler", "Object");
		mdb_printf(PRINT_FORMAT, "     ", "    ", "Type   ",
		    "Addr  ", "Name   ", " ");
		mdb_printf(LINE_LEN);
	}

	for (i = 0; i < num_buckets; i++) {
#ifdef MDB_DEBUG
		int empty = 1;
#endif
		int num_xdoors = 0;
		for (j = 0; j < lists_per_bucket; j++, listp++) {
			if (!listp->empty()) {
				memused += mdb_dump_bucket(listp,
				    &num_xdoors);
#ifdef MDB_DEBUG
				empty = 0;
				mdb_printf("Dumped list %d\n", j);
#endif
			}
		}

#ifdef MDB_DEBUG
		if (!empty && !print_summary) {
			mdb_printf("\tBucket = %d: num_xdoors = %d\n",
			    i, num_xdoors);
		}
#endif
	}
	mdb_free(listp, num_lists * sizeof (rx_list));

	// returns memory used by list elements in the list of xdoors.
	return (memused);
}

//
// mdb_node_is_empty
//
//    Function used by mdb to check if a node is empty.
//
bool
rxdoor_node::mdb_node_is_empty()
{
	uint_t		i, j;

	rx_list *this_list = (rx_list *)mdb_alloc(sizeof (rx_list),
	    UM_SLEEP);
	for (i = 0; i < num_buckets; i++) {
		for (j = 0; j < lists_per_bucket; j++) {

			(void) mdb_vread((void *)this_list, sizeof (rx_list),
			    (uintptr_t)&lists[j]);

			if (!this_list->empty()) {
				return (false);
			}
		}
	}
	mdb_free((void *)this_list, sizeof (rx_list));

	return (true);
}

//
// mdb_dump_table
//
//   Dumps rxdoor_table
//
void
rxdoor_hash_table::mdb_dump_table()
{
	ssize_t		rxdnd_mem = 0, totmem = 0;
	rxdoor_node	*rnp;
	rxdoor_node 	*rn = (rxdoor_node *)mdb_alloc(sizeof (rxdoor_node),
			UM_SLEEP);
	DList < rxdoor_node > *list_per_node = 0;

	// Dump information in the local_rx_node
	mdb_printf("Node: %d\n", local_rx_node.get_node().ndid);

	rxdnd_mem = local_rx_node.mdb_dump_node();
	if (!print_summary) {
		mdb_printf("\n\tMemused by this rxdoor_node: %ld\n\n",
		    rxdnd_mem);
	}
	totmem += rxdnd_mem;

	// Dump rxdoors at fixed addresses.
	mdb_printf("Dumping nodes containing simple_xdoors\n");
	nodeid_t ndid = 0;
	for (ndid = 1; ndid <= NODEID_MAX; ndid++) {

		if (mdb_vread((void *)rn, sizeof (rxdoor_node),
		    (uintptr_t)fixed_rx_nodes[ndid-1]) !=
		    (ssize_t)sizeof (rxdoor_node)) {
			mdb_printf("Failed to read rxdoor_node 0x%x\n",
			    fixed_rx_nodes[ndid-1]);
		} else if (!rn->mdb_node_is_empty()) {
				// variable memory used by each rxdoor node
			rxdnd_mem = rn->mdb_dump_node();
			if (!print_summary) {
				mdb_printf("\n\tMemused by this "
				    "rxdoor_node: %ld\n\n", rxdnd_mem);
			}
			totmem += rxdnd_mem;
		}
	}

	// Dump rxdoors at all other nodes.
	for (ndid = 1; ndid <= NODEID_MAX; ndid++) {

		list_per_node = &node_buckets[ndid -1];

		DList<rxdoor_node>::ListIterator iter(*list_per_node);

		rnp = iter.mdb_get_current();

		if (rnp && !print_summary) {
			mdb_printf("Node: %d\n", ndid);
		}

		while (rnp != NULL) {
			iter.mdb_advance();

			if (mdb_vread((void *)rn, sizeof (rxdoor_node),
			    (uintptr_t)rnp) != (ssize_t)sizeof (rxdoor_node)) {
				mdb_printf("Failed to read rxdoor_node 0x%x\n",
				    rnp);
			} else if (!rn->mdb_node_is_empty()) {
				// variable memory used by each rxdoor node
				rxdnd_mem = rn->mdb_dump_node();
				if (!print_summary) {
					mdb_printf("\n\tMemused by this "
					    "rxdoor_node: %ld\n\n", rxdnd_mem);
				}
				totmem += rxdnd_mem;
			}
			rnp = iter.mdb_get_current();
		}
	}

	mdb_free(rn, sizeof (rxdoor_node));

	mdb_printf("Memused by rxdoor_table = %ld\n", totmem);

	// Print summary information
	mdb_printf("\nHandler Summary\n");
	mdb_printf("---------------\n");
	mdb_printf("%-25s = %d\n", "Standard_handlers", num_std);
	mdb_printf("%-25s = %d\n", "Combo_handlers", num_combo);
	mdb_printf("%-25s = %d\n", "Bulkio_handlers", num_bulkio);
	mdb_printf("%-25s = %d\n", "Solobj_handlers", num_solobj);
	mdb_printf("%-25s = %d\n", "Ckpt_handlers", num_ckpt);
	mdb_printf("%-25s = %d\n", "Replica_handlers", num_repl);
	mdb_printf("%-25s = %d\n", "Gateway_handlers", num_gtwy);
}

//
// mdb_dump_table
//
//   Helper function for mdb to dump rxdoor_hash_table
//
void
rxdoor_manager::mdb_dump_table()
{
	table.mdb_dump_table();
}

//
// update_handler_count
//
//   function to update various counts to gather summary of number of
//   rxdoors associated with each type of handler.
//
handler_type_t
mdb_get_handler_type_count(char *name)
{
	if (strncmp(name, "stan", (ssize_t)4) == 0) {
		num_std++;
		if (strstr(name, "client") != 0) {
			return (STD_CLIENT);
		} else {
			return (STD_SERVER);
		}
	}

	if (strncmp(name, "combo", (ssize_t)4) == 0) {
		num_combo++;
		return (COMBO);
	}

	if (strncmp(name, "bulk", (ssize_t)4) == 0) {
		num_bulkio++;
		return (BULKIO);
	}

	if (strncmp(name, "solo", (ssize_t)4) == 0) {
		num_solobj++;
		return (SOLOBJ);
	}

	if (strncmp(name, "ckpt", (ssize_t)4) == 0) {
		num_ckpt++;
		return (CKPT);
	}

	if (strncmp(name, "repl", (ssize_t)4) == 0) {
		num_repl++;
		return (REPLICA);
	}

	if (strncmp(name, "gate", (ssize_t)4) == 0) {
		num_gtwy++;
		return (GATEWAY);
	}

	return (UNKWN);
}

//
// mdb_get_xdoor_size
//
//   Given an a pointer to an rxdoor, returns the size of that xdoor.
//
ssize_t
mdb_get_xdoor_size(rxdoor *rxdp)
{
	GElf_Sym gelf_sym;
	char	 sym[MDB_SYM_NAMLEN];
	uintptr_t vtbl_ptr;

	if (mdb_vread((void *)&vtbl_ptr, sizeof (uintptr_t),
	    (uintptr_t)rxdp) != (ssize_t)sizeof (uintptr_t)) {
		mdb_warn("Failed to read vtbl ptr.\n");
		return (0);
	}

	// read in the symbol it points to
	if (mdb_lookup_by_addr(vtbl_ptr, MDB_SYM_EXACT, sym,
	    (ssize_t)MDB_SYM_NAMLEN, &gelf_sym)) {
		(void) strcpy(sym, "Symbol not found at given address");
		return (0);
	}

	if (strstr(sym, "standard_xdoor_client"))
		return (sizeof (standard_xdoor_client));

	if (strstr(sym, "standard_xdoor_server"))
		return (sizeof (standard_xdoor_server));

	if (strstr(sym, "simple_xdoor_client"))
		return (sizeof (simple_xdoor_client));

	if (strstr(sym, "simple_xdoor_server"))
		return (sizeof (simple_xdoor_server));

	return (0);
}

//
// mdb_read_handler_at_addr
//
//   Given an a pointer to an object of type handler, returns a string
//   in sym_name indicating actual type of the object.
//   Returns string indicating failure if it cannot determine the name.
//
void
mdb_read_handler_at_addr(rxdoor *rxdp, xdoor_id id, char *hdl_type,
    uintptr_t handlerp)
{

	uintptr_t vtbl_ptr;
	GElf_Sym gelf_sym;
	char	 sym[MDB_SYM_NAMLEN];


	if (!handlerp) {
		return;
	}

	// handler is a pure virtual class. The first word is therefore 0.
	// The next non-zero word is the vtbl pointer belonging to the next
	// derived class.
	// There needs to be some other method to find the derived
	// class type.

	if (mdb_vread((void *)&vtbl_ptr, sizeof (uintptr_t),
	    (uintptr_t)handlerp) != (ssize_t)sizeof (uintptr_t)) {
		mdb_warn("Failed to read vtbl ptr.\n");
		return;
	}

#ifdef MDB_DEBUG
	mdb_printf("vtbl ptr = 0x%x\n", vtbl_ptr);
#endif

	// read in the symbol it points to
	if (mdb_lookup_by_addr(vtbl_ptr, MDB_SYM_EXACT, sym,
	    (ssize_t)MDB_SYM_NAMLEN, &gelf_sym)) {
		(void) strcpy(sym, "Symbol not found at given address");
		return;
	}
	//
	// Using cplus_demangle() and libdemangle is causing linking errors.
	// So try to do this ourselves.
	// The symbol name corresponding to a vtbl pointer has the format
	// __XXX<class name>G__vtbl. So try to remove these strings and
	// get the class name.
	//
	char *end_p = strstr(sym, MDB_G_VTAB);
	*end_p = '\0';

	char *sym_name = sym+5;

	//
	// Update count for the type of handler found
	//
	handler_type_t htype = mdb_get_handler_type_count(sym_name);

	mdb_printf(PTR_FORMAT "%8d ", rxdp, id);
	mdb_printf(" %-5s "PTR_FORMAT, hdl_type, handlerp);

	switch (htype) {
		case CKPT:
		case REPLICA:
		case STD_CLIENT:
		case STD_SERVER:
		case COMBO:
			mdb_printf("  %-25s\n", sym_name);
			break;
		case GATEWAY:
			mdb_printf("  %-10s\n", sym_name);
			break;
		case BULKIO:
		case SOLOBJ:
		case UNKWN:
		default:
			mdb_printf("  %-10s\n", sym_name);
			break;
	}
}
