/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RXDOOR_TABLE_H
#define	_RXDOOR_TABLE_H

#pragma ident	"@(#)rxdoor_table.h	1.4	08/05/20 SMI"

// Global variables
extern int		print_summary;

#ifdef _LP64

#define	PRINT_FORMAT	"%11s %8s  %-8s %-14s  %-25s %-15s\n"
#define	LINE_LEN	"--------------------------------------------"\
			"--------------------------------------------"\
			"------------\n"
#define	PTR_FORMAT	"%11p "
#else
#define	PRINT_FORMAT	"%8s %8s  %-8s %-10s  %-25s %-15s\n"
#define	LINE_LEN	"---------------------------------------------"\
			"--------------------------------------------\n"
#define	PTR_FORMAT	"%8p "
#endif

#endif	/* _RXDOOR_TABLE_H */
