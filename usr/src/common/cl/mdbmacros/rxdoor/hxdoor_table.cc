/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hxdoor_table.cc	1.7	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/sysinfo.h>

#include <orb/handler/remote_handler.h>
#include <orb/object/schema.h>
#include <orb/xdoor/rxdoor_mgr.h>
#include <repl/rma/rma.h>
#include <repl/rma/hxdoor_service.h>
#include <repl/rma/hxdoor.h>
#include <sys/list_def.h>
#include "rxdoor_table.h"
#include "mdb_names.h"

static uint_t brief = 0;

//
//   mdb dcmd entry point
//
extern "C" int
hxdoor_table(uintptr_t, uint_t, int argc, const mdb_arg_t *argv)
{
	rma		*the_rmap;
	uintptr_t	addr;
	uint_t		svc_id = 0; // mdb defines only uint, uint64
	int		ret;

	// HACK: Do not want to print node id and incarnation
	print_summary = 1;

	// process args
	svc_id = 0;
	brief = 0;
	if (mdb_getopts(argc, argv,
	    'b', MDB_OPT_SETBITS, TRUE, &brief,
	    's', MDB_OPT_UINTPTR, &svc_id, NULL) != argc) {
		return (DCMD_USAGE);
	}

	the_rmap = (rma *)mdb_alloc(sizeof (rma), UM_SLEEP);

	if (mdb_readsym(&addr, sizeof (addr), MDB_THE_RMAP) !=
	    (int)sizeof (addr)) {
		mdb_warn("Failed to find the_rmap\n");
		mdb_free(the_rmap, sizeof (rma));
		return (DCMD_ERR);
	}

#ifdef MDB_DEBUG
	mdb_printf("Found the_rma at addr 0x%x\n", addr);
#endif

	if (mdb_vread((void *)the_rmap, sizeof (rma), addr)
	    != (ssize_t)sizeof (rma)) {
		mdb_warn("Failed to read RMA\n");
		mdb_free(the_rmap, sizeof (rma));
		return (DCMD_ERR);
	} else {
		if (svc_id != 0) {
			ret = the_rmap->mdb_dump_service((ushort_t)svc_id);
		} else {
			ret = the_rmap->mdb_dump_all_services();
		}
	}
	mdb_free(the_rmap, sizeof (rma));

	return (ret);
}

int
rma::mdb_dump_all_services()
{
	hxdoor_service 				*curp = 0;
	DList<hxdoor_service>::ListIterator 	iter;

	hxdoor_service *hsvcp = (hxdoor_service *)mdb_alloc(
	    sizeof (hxdoor_service), UM_SLEEP);

	for (int i = 0; i < RMA_SERVICE_HASH_SIZE; i++) {
		if (services[i].empty()) {
			continue;
		}
		iter.reinit(services[i]);

		while ((curp = iter.mdb_get_current()) != NULL) {
			if (mdb_vread((void *)hsvcp, sizeof (hxdoor_service),
			    (uintptr_t)curp) !=
			    (ssize_t)sizeof (hxdoor_service)) {
				mdb_printf("Failed to read hxdoor_service "
				    "0x%x\n", curp);
				mdb_free(hsvcp, sizeof (hxdoor_service));
				return (DCMD_ERR);
			} else {
				(void) hsvcp->mdb_dump_service();
			}
			iter.mdb_advance();
		}
	}
	mdb_free(hsvcp, sizeof (hxdoor_service));
	return (DCMD_OK);
}

int
rma::mdb_dump_service(ushort_t id)
{
	hxdoor_service 				*curp = 0;
	DList<hxdoor_service>::ListIterator 	iter;

	hxdoor_service *hsvcp = (hxdoor_service *)mdb_alloc(
	    sizeof (hxdoor_service), UM_SLEEP);

	ushort_t which_serv = id % RMA_SERVICE_HASH_SIZE;

	iter.reinit(services[which_serv]);

	while ((curp = iter.mdb_get_current()) != NULL) {

		if (mdb_vread((void *)hsvcp, sizeof (hxdoor_service),
		    (uintptr_t)curp) !=
		    (ssize_t)sizeof (hxdoor_service)) {
			mdb_printf("Failed to read hxdoor_service "
			    "0x%x\n", curp);
			mdb_free(hsvcp, sizeof (hxdoor_service));
			return (DCMD_ERR);
		}
		if (hsvcp->get_service_id() == id) {
			(void) hsvcp->mdb_dump_service();
			mdb_free(hsvcp, sizeof (hxdoor_service));
			return (DCMD_OK);
		}
		iter.mdb_advance();
	}
	mdb_printf("Service id %d not does not exist.\n", id);
	mdb_free(hsvcp, sizeof (hxdoor_service));
	return (DCMD_OK);

}

int
hxdoor_service::mdb_dump_service()
{
	char svc_name[180];

	(void) mdb_readstr(svc_name, 180, (uintptr_t)_sname);

	mdb_printf(LINE_LEN);
	mdb_printf("Id: %d\nService: %s\n", servid, svc_name);
	mdb_printf("Primary Node: %d,%ld\n", get_node().ndid,
	    get_node().incn);
	mdb_printf("Service flags: 0x%x\n", hs_flags);

	if (brief)
		return (DCMD_OK);

	mdb_printf(LINE_LEN);
	mdb_printf(PRINT_FORMAT, "Xdoor ", "   ID", "Handler",
	    "Handler", "Handler", "Object");
	mdb_printf(PRINT_FORMAT, "     ", "    ", "Type   ",
	    "Addr  ", "Name   ", " ");
	mdb_printf(LINE_LEN);

	(void) mdb_dump_node();
	return (DCMD_OK);
}
