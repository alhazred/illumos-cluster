/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)vm_mgr.cc 1.6     08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <vm/vm_comm.h>

#include "vm_mgr.h"
#include "mdb_names.h"
#include "mdb_util.h"
#include "vm/vm_admin_impl.h"
#include "vm/versioned_protocol.h"
#include "vm/vm_stream.h"
#include "vm/vp_rhs_version.h"

static const char *vp_type_to_string(vp_type);
static const char *stream_type_to_string(vm_stream::stream_type);

//
// stream_info: Entry point for mdb command "<stream_addr>::vmstream"
//
// Dumps version manager stream information.
//
extern "C" int
stream_info(uintptr_t addr, uint_t flags, int, const mdb_arg_t *)
{

	//
	// check command line
	//
	if (!(flags & DCMD_ADDRSPEC)) {
		mdb_printf("Please specify an address...\n");
		return (DCMD_ERR);
	}

	//
	// allocate space for stream
	//
	vm_stream* vmst = (vm_stream *) mdb_alloc(sizeof (vm_stream),
	    UM_SLEEP);

	//
	// read versioned protocol stream
	//
	if (mdb_vread((void *) vmst, sizeof (vm_stream), addr) !=
	    (ssize_t)sizeof (vm_stream)) {
		mdb_warn("Failed to read vm stream...\n");
		mdb_free(vmst, sizeof (vm_stream));
		return (DCMD_ERR);
	}

	//
	// allocate space for stream header
	//
	vm_stream_header *vmshp = (vm_stream_header *)
		mdb_alloc(sizeof (vm_stream_header), UM_SLEEP);

	//
	// read versioned stream header
	//
	if (mdb_vread((void *) vmshp, sizeof (vm_stream_header),
	    (uintptr_t)(vmst->get_buffer())) !=
	    (ssize_t)sizeof (vm_stream_header)) {
		mdb_warn("Failed to read vm stream header...\n");
		mdb_free(vmst, sizeof (vm_stream));
		mdb_free(vmshp, sizeof (vm_stream_header));
		return (DCMD_ERR);
	}

	//
	// print info
	//
	mdb_printf("\nStream buffer at addr 0x%p", vmst->get_buffer());
	mdb_printf("\nStream size = %d", vmst->get_size());
	mdb_printf("\nStream type = %s", stream_type_to_string(
	    (vm_stream::stream_type)vmshp->the_type));
	mdb_printf("\nStream rv_seqnum = %llu", vmshp->rv_seqnum);
	mdb_printf("\nStream uv_seqnum = %llu", vmshp->uv_seqnum);

	//
	// invoke dump method on versioned protocol stream
	//
	int res = vmst->mdb_dump_stream((vm_stream::stream_type)
	    vmshp->the_type);

	mdb_free(vmst, sizeof (vm_stream));
	mdb_free(vmshp, sizeof (vm_stream_header));
	return (res);
}

char *
vm_stream::get_buffer() {
	return (_buffer);
}

size_t
vm_stream::get_size() {
	return (_size);
}

int
vm_stream::mdb_dump_stream(stream_type t)
{
	uint16_t tp;
	version_manager::vp_version_t rv, uv;
	char *vpn;

	read_from_beginning();

	//
	// get header info
	//
	while ((vpn = mdb_get_header(tp, rv, uv)) != NULL) {

		mdb_printf("\n\nHeader name[%s] type[%d] "
		    "rv[%d.%d] uv[%d.%d]\n",
		    vpn,
		    tp,
		    rv.major_num, rv.minor_num,
		    uv.major_num, uv.minor_num);

		mdb_free(vpn, sizeof (VM_MDB_STRING_MAX));

		//
		// RV streams do not contain dependencies
		//
		if (t != RV_STREAM_1) {
			//
			// retreive dependency triads
			//
			while (mdb_get_dependency()) {
				// loop
			};
		}
	}
	return (DCMD_OK);
}

char *
vm_stream::mdb_get_header(uint16_t &vt, version_manager::vp_version_t &rv,
    version_manager::vp_version_t &uv)
{
	//
	// read name
	//
	char *vm_name = mdb_get_string_ref();

	if (vm_name == NULL) {
		//
		// no more header
		//
		return (NULL);
	}

	//
	// read type
	//
	if (mdb_vread((void *) &vt, sizeof (vt),
	    (uintptr_t)read_point()) !=
	    (ssize_t)sizeof (vt)) {
		mdb_free(vm_name, sizeof (VM_MDB_STRING_MAX));
		mdb_warn("Failed to read header type...\n");
		return (NULL);
	}
	advance_read_point(sizeof (vt));

	//
	// read rv
	//
	if (mdb_vread((void *) &rv, sizeof (rv),
	    (uintptr_t)read_point()) !=
	    (ssize_t)sizeof (rv)) {
		mdb_free(vm_name, sizeof (VM_MDB_STRING_MAX));
		mdb_warn("Failed to read header rv...\n");
		return (NULL);
	}
	advance_read_point(sizeof (rv));

	//
	// read uv
	//
	if (mdb_vread((void *) &uv, sizeof (uv),
	    (uintptr_t)read_point()) !=
	    (ssize_t)sizeof (uv)) {
		mdb_free(vm_name, sizeof (VM_MDB_STRING_MAX));
		mdb_warn("Failed to read header uv...\n");
		return (NULL);
	}
	advance_read_point(sizeof (uv));

	return (vm_name);
}

int
vm_stream::mdb_get_dependency() {

	//
	// left hand
	//
	mdb_printf("\n");
	int res = mdb_retreive_triads();

	if (res == 0) {
		return (0);
	}

	//
	// middle
	//
	mdb_printf(" => ");

	//
	// right hand
	//
	char *vm_name = NULL;
	int found = 0;
	int should_stop = 0;

	do {
		//
		// read name
		//
		vm_name = mdb_get_string_ref();
		if (vm_name != NULL) {

			mdb_printf("[%s] ", vm_name);
			mdb_free(vm_name, sizeof (VM_MDB_STRING_MAX));

			//
			// right dependency
			//
			(void) mdb_retreive_triads();
			found = 1;
		} else {
			should_stop = 1;
		}
	} while (!should_stop);

	if (!found) {
		mdb_printf("(no dependency)");
	}

	return (1);
}

int
vm_stream::mdb_retreive_triads() {

	vm_triad_version t;
	int found = 0;

	do {
		if (mdb_vread((void *) &t, sizeof (t),
		    (uintptr_t)read_point()) !=
		    (ssize_t)sizeof (t)) {
			mdb_warn("Failed to read dependency triad...\n");
			return (0);
		}

		advance_read_point(sizeof (t));

		if (t.major_num != 0) {

			mdb_printf("\t[%d.%d-%d]",
			    t.major_num,
			    t.minor_min,
			    t.minor_max);
			found = 1;
		}
	} while (t.major_num != 0);

	return (found);
}

char *
vm_stream::mdb_get_string_ref()
{
	char *retval = (char *)mdb_alloc(VM_MDB_STRING_MAX, UM_SLEEP);

	int res = mdb_getstring(
	    (uintptr_t)read_point(),
	    retval,
	    VM_MDB_STRING_MAX);

	if (res != 0) {
		mdb_warn("Failed to get string...\n");
		mdb_free(retval, sizeof (VM_MDB_STRING_MAX));
		return (NULL);
	}

	size_t slen = os::strlen(retval);
	advance_read_point(slen + 1);

	if (slen == 0) {
		mdb_free(retval, sizeof (VM_MDB_STRING_MAX));
		return (NULL);
	} else {
		return (retval);
	}
}

//
// vp_info: Entry point for mdb command "::vpfiles"
//
// Dumps versioned protocol files table information.
//
extern "C" int
vp_info(uintptr_t, uint_t, int argc, const mdb_arg_t *argv)
{
	const char *given_vp_name = NULL;
	//
	// check input parameters if any
	//
	if ((argc >= 2) || ((argc == 1) &&
	    (argv->a_type != MDB_TYPE_STRING))) {
		return (DCMD_USAGE);
	}

	if (argc == 1) {
		given_vp_name = argv->a_un.a_str;
	}

	//
	// allocate space for versioned protocol comm
	//
	vm_comm  *vm_com = (vm_comm *) mdb_alloc(sizeof (vm_comm), UM_SLEEP);

	//
	// get address of versioned protocol comm
	//
	uintptr_t addr = NULL;
	if (mdb_readsym(&addr, sizeof (addr), MDB_THE_VM_COMM) !=
	    (int)sizeof (addr)) {
		mdb_warn("Failed to find the_vm_comm...\n");
		mdb_free(vm_com, sizeof (vm_comm));
		return (DCMD_ERR);
	}

	mdb_printf("\n(Found the_vm_comm at addr 0x%p)\n\n", addr);

	//
	// read versioned protocol comm
	//
	if (mdb_vread((void *) vm_com, sizeof (vm_comm), addr) !=
	    (ssize_t)sizeof (vm_comm)) {
		mdb_warn("Failed to read vm_comm...\n");
		mdb_free(vm_com, sizeof (vm_comm));
		return (DCMD_ERR);
	}

	//
	// invoke dump method on versioned protocol comm
	//
	int res = vm_com->mdb_dump_vps(given_vp_name);
	mdb_free(vm_com, sizeof (vm_comm));


	//
	// print additional vm_comm information
	//
	mdb_printf("\n(vm_comm btstrp_np_message stream at addr 0x%p)",
	    vm_com->mdb_get_btstrp_np_message());
	mdb_printf("\n(vm_comm btstrp_cl_message stream at addr 0x%p)",
	    vm_com->mdb_get_btstrp_cl_message());
	mdb_printf("\n(vm_comm cl_message stream at addr 0x%p)",
	    vm_com->mdb_get_cl_message());
	mdb_printf("\n(vm_comm cl_rv_message stream at addr 0x%p)",
	    vm_com->mdb_get_cl_rv_message());
	mdb_printf("\n(vm_comm btstrp_cl_rv_message stream at addr 0x%p)\n\n",
	    vm_com->mdb_get_btstrp_cl_rv_message());

	return (res);
}

vm_stream *
vm_comm::mdb_get_btstrp_np_message() {
	return (btstrp_np_message);
};

vm_stream *
vm_comm::mdb_get_btstrp_cl_message() {
	return (btstrp_cl_message);
};

vm_stream *
vm_comm::mdb_get_cl_message() {
	return (cl_message);
};

vm_stream *
vm_comm::mdb_get_cl_rv_message() {
	return (cl_rv_message);
};

vm_stream *
vm_comm::mdb_get_btstrp_cl_rv_message() {
	return (btstrp_cl_rv_message);
};

int
vm_comm::mdb_dump_vps(const char *given_vp_name)
{
	//
	// allocate space for versioned protocol admin
	//
	vm_admin_impl *vm_adm = (vm_admin_impl *)
		mdb_alloc(sizeof (vm_admin_impl), UM_SLEEP);

	//
	// read versioned protocol admin
	//
	if (mdb_vread((void *)vm_adm, sizeof (vm_admin_impl),
	    (uintptr_t)the_admin_impl) != (ssize_t)sizeof (vm_admin_impl)) {
		mdb_warn("Failed to read the_vm_admin_impl...\n");
		mdb_free(vm_adm, sizeof (vm_admin_impl));
		return (DCMD_ERR);
	}

	//
	// invoke dump method on versioned protocol admin
	//
	int res = vm_adm->mdb_dump_vps(given_vp_name);
	mdb_free(vm_adm, sizeof (vm_admin_impl));
	return (res);
}

int
vm_admin_impl::mdb_dump_vps(const char *given_vp_name)
{
	int found = 0;
	int onetime = (given_vp_name != NULL);

	if (!onetime) {
		mdb_printf("Total number of vps: [%d]\n", num_vps);
	}

	versioned_protocol *vp_a = NULL;

	//
	// allocate space for versioned protocol
	//
	versioned_protocol *vp = (versioned_protocol *)
		mdb_alloc(sizeof (versioned_protocol), UM_SLEEP);

	for (uint16_t i = 0; i < num_vps; i++) {
		//
		// read pointer to next versioned protocol
		//
		if (mdb_vread((void *)&vp_a, sizeof (versioned_protocol*),
		    (uintptr_t)&(vp_array[i])) != (ssize_t)
		    sizeof (versioned_protocol*)) {
			mdb_warn("Failed to read "
			    "versioned_protocol addr...\n");
			mdb_free(vp, sizeof (versioned_protocol));
			return (DCMD_ERR);
		}

		//
		// read versioned protocol
		//
		if (mdb_vread((void *)vp, sizeof (versioned_protocol),
		    (uintptr_t)vp_a) != (ssize_t)
		    sizeof (versioned_protocol)) {
			mdb_warn("Failed to read versioned_protocol...\n");
			mdb_free(vp, sizeof (versioned_protocol));
			return (DCMD_ERR);
		}

		if (given_vp_name == NULL) {
			mdb_printf("\nVersioned Protocol file [%d]:\n", i);
		}
		//
		// invoke dump method on versioned protocol
		//
		if (vp->mdb_dump_state(given_vp_name)) {
			found++;
		}

		if ((onetime) && (found)) {
			break;
		}
	}

	mdb_free(vp, sizeof (versioned_protocol));

	if ((given_vp_name != NULL) && (!found)) {
		mdb_printf("\tNo Version Protocol found"
		    " of name [%s].\n\n", given_vp_name);
	}

	return (DCMD_OK);
}

int
versioned_protocol::mdb_dump_state(const char *given_vp_name)
{
	int res = 0;
	//
	// allocate space for vp string attributes
	//
	char *desc = (char *)mdb_alloc(VM_MDB_STRING_MAX, UM_SLEEP);
	char *srce = (char *)mdb_alloc(VM_MDB_STRING_MAX, UM_SLEEP);
	char *name = (char *)mdb_alloc(VM_MDB_STRING_MAX, UM_SLEEP);

	//
	// read and print versioned protocol attributes
	//
	(void) mdb_getstring((uintptr_t)vp_name, name, VM_MDB_STRING_MAX);
	(void) mdb_getstring((uintptr_t)vp_description, desc,
	    VM_MDB_STRING_MAX);
	(void) mdb_getstring((uintptr_t)vp_source, srce, VM_MDB_STRING_MAX);

	if ((given_vp_name == NULL) || (strcmp(given_vp_name, name) == 0)) {

		res = 1;

		mdb_printf("\t name [%s]\n"
		    "\t desc [%s]\n"
		    "\t src  [%s]\n"
		    "\t type [%s]\n"
		    "\t attr [%s-%s-%s-%s]\n",
		    name,
		    desc,
		    srce,
		    vp_type_to_string(the_type),
		    (vp_attributes & VP_BOOTSTRAP) ? "VP_BOOTSTRAP" : "",
		    (vp_attributes & VP_CUSTOM) ? "VP_CUSTOM" : "",
		    (vp_attributes & VP_RUNNING) ? "VP_RUNNING" : "",
		    (vp_attributes & VP_PERSISTENT) ? "VP_PERSISTENT" : "");

		//
		// allocate space for version
		//
		version_manager::vp_version_t *v =
			(version_manager::vp_version_t *)
			mdb_alloc(sizeof (version_manager::vp_version_t),
			    UM_SLEEP);

		if (the_type != VP_BTSTRP_NODEPAIR &&
		    the_type != VP_NODEPAIR) {
			//
			// read and print single version
			// (if not a "NODEPAIR" vp)
			//
			// index "NODEID_UNKNOWN" is used to store version
			// when not a "NODEPAIR" vp
			//
			if (mdb_vread((void *)v,
			    sizeof (version_manager::vp_version_t),

			    (uintptr_t)&(running_version[NODEID_UNKNOWN])) !=
			    (ssize_t)sizeof (version_manager::vp_version_t)) {
				mdb_warn("Failed to read vp_version_t...\n");
			} else {
				mdb_printf("\t runv [%d.%d]\n",
				    v->major_num, v->minor_num);
			}
		} else {
			//
			// read and print node pair versions
			// for node ids 1 to NODEID_MAX
			//
			for (uint16_t i = 1; i <= NODEID_MAX; i++) {
				if (mdb_vread((void *)v,
				    sizeof (version_manager::vp_version_t),
				    (uintptr_t)&(running_version[i])) !=
				    (ssize_t)
				    sizeof (version_manager::vp_version_t)) {
					mdb_warn("Failed to read "
					    "vp_version_t...\n");
				} else if (v->major_num != 0) {
					mdb_printf("\t node [%d] "
					    "vers [%d.%d]\n", i,
					    v->major_num, v->minor_num);
				}
			}
		}
		mdb_free(v, sizeof (version_manager::vp_version_t));
	}

	mdb_free(desc, sizeof (VM_MDB_STRING_MAX));
	mdb_free(srce, sizeof (VM_MDB_STRING_MAX));
	mdb_free(name, sizeof (VM_MDB_STRING_MAX));

	return (res);
}

static const char*
vp_type_to_string(vp_type vt)
{
	const char *str;
	switch (vt) {
	case VP_UNINITIALIZED:
		str = "VP_UNINITIALIZED";
		break;
	case VP_BTSTRP_NODE:
		str = "VP_BTSTRP_NODE";
		break;
	case VP_BTSTRP_NODEPAIR:
		str = "VP_BTSTRP_NODEPAIR";
		break;
	case VP_BTSTRP_CLUSTER:
		str = "VP_BTSTRP_CLUSTER";
		break;
	case VP_NODE:
		str = "VP_NODE";
		break;
	case VP_NODEPAIR:
		str = "VP_NODEPAIR";
		break;
	case VP_CLUSTER:
		str = "VP_CLUSTER";
		break;
	case VP_TYPE_ARRAY_SIZE:
		str = "VP_TYPE_ARRAY_SIZE";
		break;
	default:
		str = "Unrecognized";
		break;
	}
	return (str);
}

static const char*
stream_type_to_string(vm_stream::stream_type st)
{
	const char *str;
	switch (st) {
	case vm_stream::UNINITIALIZED_STREAM:
		str = "UNINITIALIZED_STREAM";
		break;
	case vm_stream::VP_STREAM_1:
		str = "VP_STREAM_1";
		break;
	case vm_stream::RV_STREAM_1:
		str = "RV_STREAM_1";
		break;
	default:
		str = "Unrecognized";
		break;
	}
	return (str);
}
