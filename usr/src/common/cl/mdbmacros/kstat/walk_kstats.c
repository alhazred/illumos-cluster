/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)walk_kstats.c	1.7	08/05/20 SMI"

#include <sys/mdb_modapi.h>
#include <sys/sysinfo.h>
#include <sys/kstat.h>

int
ks_walk_init(mdb_walk_state_t *wsp)
{
	if (wsp->walk_addr == NULL &&
	    mdb_readvar(&wsp->walk_addr, "kstat_chain") == -1) {
		mdb_warn("failed to read 'kstat_chain'");
		return (WALK_ERR);
	}

	wsp->walk_data = mdb_alloc(sizeof (kstat_t), UM_SLEEP);
	return (WALK_NEXT);
}

int
ks_walk_step(mdb_walk_state_t *wsp)
{
	int status;

	if (wsp->walk_addr == NULL)
		return (WALK_DONE);

	if (mdb_vread(wsp->walk_data, sizeof (kstat_t), wsp->walk_addr) == -1) {
		mdb_warn("failed to read kstat at %p", wsp->walk_addr);
		return (WALK_DONE);
	}

	status = wsp->walk_callback(wsp->walk_addr, wsp->walk_data,
	    wsp->walk_cbdata);

	wsp->walk_addr = (uintptr_t)(((kstat_t *)wsp->walk_data)->ks_next);
	return (status);
}

void
ks_walk_fini(mdb_walk_state_t *wsp)
{
	mdb_free(wsp->walk_data, sizeof (kstat_t));
}

int
ks_info(uintptr_t addr, uint_t flags, int argc, const mdb_arg_t *argv)
{
	kstat_t ks;
	kstat_named_t *ks_named;
	uint_t i;
	uint64_t size;
	char *modname = NULL;
	char *ksname = NULL;

	if (argc != 0) {
		if (mdb_getopts(argc, argv,
		    'm', MDB_OPT_STR, &modname,
		    'n', MDB_OPT_STR, &ksname, NULL) != argc)
			return (DCMD_USAGE);
	}
	/*
	 *
	 */
	if (!(flags & DCMD_ADDRSPEC)) {
		if (mdb_walk_dcmd("simple_kstat", "kstat", argc, argv) == -1) {
			mdb_warn("failed to walk 'simple_kstat'");
			return (DCMD_ERR);
		}
		return (DCMD_OK);
	}
	if ((mdb_vread(&ks, sizeof (ks), addr)) != (ssize_t)sizeof (ks)) {
		mdb_warn("failed to read kstat at %p", addr);
		return (DCMD_ERR);
	}
	if (((modname == NULL) || (strcmp(modname, ks.ks_module) == 0)) &&
	    ((ksname == NULL) || (strcmp(ksname, ks.ks_name) == 0))) {
		mdb_printf("Module name:\t%s\n", ks.ks_module);
		mdb_printf("Name:\t%s\n", ks.ks_name);
		mdb_printf("Instance:\t0x%x\n", ks.ks_instance);
		mdb_printf("Class:\t%s\n", ks.ks_class);
		mdb_printf("ID:\t0x%x\n", ks.ks_kid);
		mdb_printf("Data size:\t0x%x\n", ks.ks_data_size);
		mdb_printf("# Data records:\t0x%x\n", ks.ks_ndata);
		mdb_printf("Flags:\t0x%x\n", ks.ks_flags);
		switch (ks.ks_type) {
		case KSTAT_TYPE_RAW:
			mdb_printf("TYPE:\tRAW\n");
			break;
		case KSTAT_TYPE_NAMED:
			mdb_printf("TYPE:\tNAME-VALUE PAIR\n");
			if (!(ks.ks_flags & KSTAT_FLAG_VIRTUAL)) {
				size = sizeof (kstat_named_t) * ks.ks_ndata;
				ks_named = (kstat_named_t *)mdb_alloc(
				    (size_t)size, UM_SLEEP);
				if (mdb_vread(ks_named, (size_t)size,
				    (uintptr_t)ks.ks_data) == (ssize_t)size) {
					for (i = 0; i < ks.ks_ndata; i++) {
						mdb_printf("%s\t\t",
						    ks_named[i].name);
						switch (ks_named[i].data_type) {
						case KSTAT_DATA_CHAR:
							mdb_printf("%d\n",
							    ks_named[i].value.
							    c[0]);
							break;
						case KSTAT_DATA_INT32:
							mdb_printf("%d\n",
							    ks_named[i].value.
							    i32);
							break;
						case KSTAT_DATA_UINT32:
							mdb_printf("%u\n",
							    ks_named[i].value.
							    ui32);
							break;
						case KSTAT_DATA_INT64:
							mdb_printf("%lld\n",
							    ks_named[i].value.
							    ll);
							break;
						case KSTAT_DATA_UINT64:
							mdb_printf("%llu\n",
							    ks_named[i].value.
							    ull);
							break;
						default:
							mdb_printf("%d\n",
							    ks_named[i].value.
							    i32);
							break;
						}
					}
				} else {
					mdb_warn("failed to read "
					    "kstat_named_t at %p",
					    ks.ks_data);
				}
			}
			break;
		case KSTAT_TYPE_INTR:
			mdb_printf("TYPE:\tINTERRUPT STATS\n");
			break;
		case KSTAT_TYPE_IO:
			mdb_printf("TYPE:\tI/O STATS\n");
			break;
		case KSTAT_TYPE_TIMER:
			mdb_printf("TYPE:\tEVENT TIMER\n");
			break;
		default:
			mdb_printf("TYPE:\tUnknown type 0x%x", ks.ks_type);
			break;
		}
		mdb_printf("Data size:\t0x%x\n", ks.ks_data_size);
		mdb_printf("# Data records:\t0x%x\n", ks.ks_ndata);
		mdb_printf("Flags:\t0x%x\n", ks.ks_flags);
		mdb_printf("*************************************************");
	}
	return (DCMD_OK);
}
