//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mdb_clconf.cc	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/rsrc_tag.h>
#include <clconf/clnode.h>
#include <orb/infrastructure/orb_conf.h>

#ifndef	linux
#define	in_addr_solaris	in_addr
#endif

//
// This file has set of functions that are taken from the origial file
// usr/src/common/cl/clconf/clconf.cc. I did not use the original file because
// it has a lot of quorum related functions which are not required here and
// ifdef'ing them out turned out to be messy. Otherwise this file should
// not have anything that is not there in the original file.
//

/* The endpoint fo a path */
typedef struct clconf_endpoint {
	nodeid_t nodeid;
	int adapterid;
	int portid;
} clconf_endpoint_t;

/* The path object */
/* end[0] is Local and end[1] is Remote. Use enum clconf_endtype_t as index */
struct clconf_path {
	clconf_endpoint_t end[2];
	uint_t quantum;
	uint_t timeout;
};

nodeid_t
clconf_get_local_nodeid()
{
	return (orb_conf::local_nodeid());
}

//
// clconf_get_nodename
//
//  Returns nodename for the given nodeid.
//  Space for nodename must be allocated by the client.
//  Call this function if you want one that does not blocking
//  memory allocation calls.
//

void
clconf_get_nodename(nodeid_t nodeid, char *nodename)
{
	const char *nm = NULL;

	CL_PANIC((nodeid >= 1) && (nodeid <= NODEID_MAX));

	clconf_cluster_t *cl = clconf_cluster_get_current();

	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl, CL_NODE,
	    (int)nodeid);

	nm = clconf_obj_get_name((clconf_obj_t *)nd);

	if (nm != NULL) {
		(void) strcpy(nodename, nm);
	} else {
		nodename[0] = '\0';
	}

	clconf_obj_release((clconf_obj_t *)cl);
}


/* Private helper function. Convert a group name to an obj type */
clconf_objtype_t
clconf_name_to_objtype(const char *name)
{
	if (os::strcmp(name, "cluster") == 0) {
		return (CL_CLUSTER);
	} else if (os::strcmp(name, "nodes") == 0) {
		return (CL_NODE);
	} else if (os::strcmp(name, "adapters") == 0) {
		return (CL_ADAPTER);
	} else if (os::strcmp(name, "ports") == 0) {
		return (CL_PORT);
	} else if (os::strcmp(name, "blackboxes") == 0) {
		return (CL_BLACKBOX);
	} else if (os::strcmp(name, "cables") == 0) {
		return (CL_CABLE);
	} else if (os::strcmp(name, "quorum_devices") == 0) {
		return (CL_QUORUM_DEVICE);
	} else {
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		name, NULL);
		(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "clconf: Invalid group name");
	}
	return (CL_CLUSTER);
}

/* helper function. Convert objtype to group name */
void
clconf_objtype_to_name(clconf_objtype_t type, char *name)
{
	switch (type) {
	case CL_CLUSTER:
		(void) os::strcpy(name, "cluster");
		break;
	case CL_NODE:
		(void) os::strcpy(name, "nodes");
		break;
	case CL_ADAPTER:
		(void) os::strcpy(name, "adapters");
		break;
	case CL_PORT:
		(void) os::strcpy(name, "ports");
		break;
	case CL_BLACKBOX:
		(void) os::strcpy(name, "blackboxes");
		break;
	case CL_CABLE:
		(void) os::strcpy(name, "cables");
		break;
	case CL_QUORUM_DEVICE:
		(void) os::strcpy(name, "quorum_devices");
		break;
	default:
		os::sc_syslog_msg syslog_msg(SC_SYSLOG_CCR_CLCONF_TAG,
		    name, NULL);
		(void) syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "clconf: Invalid clconf_obj type");
	}
}

/* initialize a static iterator */
clconf_iter_t *
clconf_siter_init(clconf_siter_t *siter, clconf_obj_t *obj,
    clconf_objtype_t childtype)
{
	// Makesure that our static iterator is big enough to contain the
	// real thing.
	ASSERT(sizeof (clconf_siter_t) >= sizeof (clconfiter));
	siter->ptr = NULL;
	clconfiter *iter = (clconfiter *)siter;

	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	char grpname[64];
	clconf_objtype_to_name(childtype, grpname);
	ccr_node *cr = node->get_named_child(grpname);
	if (cr == NULL) {
		return (NULL);
	}
	cr->init_iter(iter);
	return ((clconf_iter_t *)iter);
}

/* Advance the iterator */
void
clconf_iter_advance(clconf_iter_t *iter)
{
	ASSERT(iter != NULL);
	((clconfiter *)iter)->advance();
}

/* Get the current element, returns NULL when there is no more elements */
clconf_obj_t *
clconf_iter_get_current(clconf_iter_t *iter)
{
	if (iter == NULL) {
		return (NULL);
	}
	return ((clconf_obj_t *)((clconfiter *)iter)->get_current());
}

/* Get the number of elements in the iterator */
int
clconf_iter_get_count(clconf_iter_t *iter)
{
	if (iter == NULL) {
		return (0);
	}

	_SList::ListElem *cr;
	int count;

	cr = ((clconfiter *)iter)->get_current();
	for (count = 0; cr != NULL; cr = cr->next()) {
		count++;
	}
	return (count);
}

/* Release the iterator */
void
clconf_iter_release(clconf_iter_t *iter)
{
	delete ((clconfiter *)iter);
}

/* Helper function */
clconf_iter_t *
clconf_obj_get_iter(clconf_obj_t *obj, const char *grpname)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *cr = node->get_named_child(grpname);
	if (cr == NULL) {
		return (NULL);
	}
	return ((clconf_iter_t *)cr->get_iterator());
}

/*
 * Get the object's id as string. Every object has an id that distinguish
 * it from its siblings of the same type. It returns NULL if the id is not set.
 */
const char *
clconf_obj_get_idstring(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	return (((clnode *)obj)->get_name());
}

/*
 * Get the object's id. Every object has an id that distinguish it from
 * its siblings of the same type. It returns -1 if the id is not set.
 */
int
clconf_obj_get_id(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	return (os::atoi(((clnode *)obj)->get_name()));
}

/*
 * Set the object's id. Returns CL_INVALID_ID if the id is conflict with others
 */
clconf_errnum_t
clconf_obj_set_id(clconf_obj_t *obj, int id)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	char str[32];
	(void) os::itoa(id, str);
	if (!node->set_name(str)) {
		return (CL_INVALID_ID);
	}
	return (CL_NOERROR);
}

/*
 * Get the object's type.
 */
clconf_objtype_t
clconf_obj_get_objtype(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	if (node->get_parent() == NULL) {
		return (CL_CLUSTER);
	}
	return (clconf_name_to_objtype(node->get_parent()->get_name()));
}

// Callback to set the states.
int
clconf_obj_state_callback(ccr_node *c, void *state)
{
	// If the name is a number, it means we are on a node instead
	// of a group. We can set the state then.
	const char *nm = c->get_name();
	uint_t len = (uint_t)os::strlen(nm);
	for (uint_t i = 0; i < len; i++) {
		if (nm[i] < '0' || nm[i] > '9') {
			// not a number.
			return (1);
		}
	}

	ccr_node *st = c->get_named_child_create("state");
	ASSERT(st != NULL);
	(void) st->set_value((char *)state);

	return (1);
}

/*
 * Returns state of object. Returns NULL if state was never set.
 */
const char *
clconf_obj_get_state(clconf_obj_t *obj)
{
	if (obj == NULL) {
		return (NULL);
	}
	ccr_node *node = (ccr_node *)obj;
	node = node->get_named_child("state");
	if (node == NULL) {
		return (NULL);
	}
	return (node->get_value());
}

/*
 * Enable the clconf object.
 */
clconf_errnum_t
clconf_obj_enable(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *prt = node->get_parent();
	if (prt != NULL && !clconf_obj_enabled((clconf_obj_t *)
	    prt->get_parent())) {
		// The parent is disabled, fail.
		return (CL_PARENT_DISABLED);
	}
	(void) node->get_named_child_create("state")->set_value("enabled");
	return (CL_NOERROR);
}

/*
 * Disable the clconf object. This function disables the object
 * regardless of the state of either its parents or children.
 * The children (if any) will be left in the same state so that when
 * this is re-enabled, the state is preserved.
 */
clconf_errnum_t
clconf_obj_disable(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	(void) node->get_named_child_create("state")->set_value("disabled");
	return (CL_NOERROR);
}

/*
 * Whether this object is enabled. Returns 1 if true 0 if false.
 */
int
clconf_obj_enabled(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *st = node->get_named_child("state");
	if (st != NULL) {
		const char *v = st->get_value();
		if (os::strcmp(v, "disabled") == 0) {
			return (0);
		} else if (os::strcmp(v, "enabled") == 0) {
			return (1);
		} else
			return (0);	// Any other state is disabled!
	}
	// Default is enabled.
	return (1);
}

/*
 * Get the child with the specified type and id. Returns NULL if not found.
 */
clconf_obj_t *
clconf_obj_get_child(clconf_obj_t *obj, clconf_objtype_t type, int id)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	char tyname[20];
	clconf_objtype_to_name(type, tyname);

	char idstr[32];
	(void) os::itoa(id, idstr);

	ccr_node *child = node->get_named_child(tyname);
	if (child == NULL) {
		return (NULL);
	}
	child = child->get_named_child(idstr);
	return ((clconf_obj_t *)child);
}

/*
 * Get the parent. Returns NULL if used on a clconf_cluster.
 */
clconf_obj_t *
clconf_obj_get_parent(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	clconf_objtype_t objtype = clconf_obj_get_objtype(obj);
	CL_PANIC(objtype != CL_UNKNOWN_OBJ);
	if (objtype == CL_CLUSTER) {
		return (NULL);
	}
	clnode *node = (clnode *)obj;
	return ((clconf_obj_t *)node->get_parent()->get_parent());
}

/*
 * Get the specified property. Returns NULL if no such property.
 */
const char *
clconf_obj_get_property(clconf_obj_t *obj, const char *property_name)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	ccr_node *cr = node->get_named_child("properties");
	if (cr == NULL) {
		return (NULL);
	}
	ccr_node *prpt = cr->get_named_child(property_name);
	if (prpt == NULL) {
		return (NULL);
	}
	return (prpt->get_value());
}

/*
 * Returns the properties this object has. Returns NULL if it doesn't have
 * any properties.
 */
clconf_iter_t *
clconf_obj_get_properties(clconf_obj_t *obj)
{
	ASSERT(obj != NULL);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "properties"));
}

/*
 * Set the specified property. Create the property if it doesn't exist.
 * If property_value is NULL we will delete the property.
 * Returns CL_BADNAME if the property name is invalid.
 */
clconf_errnum_t
clconf_obj_set_property(clconf_obj_t *obj, const char *property_name,
    const char *property_value)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	if (property_value != NULL) {
		// setting a property.
		ccr_node *prts = node->get_named_child_create("properties");
		ASSERT(prts != NULL);
		ccr_node *prt = prts->get_named_child_create(property_name);
		if (prt == NULL) {
			return (CL_BADNAME);
		}
		(void) prt->set_value(property_value);
	} else {
		// removing a property.
		ccr_node *prts = NULL;
		ccr_node *prt = NULL;
		prts = node->get_named_child("properties");
		if (prts != NULL) {
			prt = prts->get_named_child(property_name);
		}
		if (prt != NULL) {
			(void) prts->remove(prt);
		}
	}
	return (CL_NOERROR);
}

/*
 * Returns name of object. Returns NULL if name was never set.
 */
const char *
clconf_obj_get_name(clconf_obj_t *obj)
{
	if (obj == NULL) {
		return (NULL);
	}
	ccr_node *node = (ccr_node *)obj;
	node = node->get_named_child("name");
	if (node == NULL) {
		return (NULL);
	}
	return (node->get_value());
}

/*
 * Set the name of the object.
 */
void
clconf_obj_set_name(clconf_obj_t *obj, const char *name)
{
	ASSERT(obj != NULL);
	clnode *node = (clnode *)obj;
	(void) node->get_named_child_create("name")->set_value(name);
}

/*
 * Returns the internal name of object. The internal name is the key string
 * used in CCR to identify this object. For example, "cluster.nodes.1"
 * is the internal name for node 1.
 */
char *
clconf_obj_get_int_name(clconf_obj_t *obj)
{
	return (((clnode *)obj)->get_full_name());
}

/*
 * checks the object to make sure it's properly filled out,
 * but doesn't check relationships to other objects. Returns
 * CL_INVALID_OBJ if the check fails and returns the detailed
 * info in the errorbuff.
 */
clconf_errnum_t
clconf_obj_check(clconf_obj_t *, char *, uint_t)
{
	/* Call other module to do the checking XXX */
	return (CL_NOERROR);
}

/*
 * Every clconf tree has a hold count. The tree will be deleted only when the
 * hold count reaches zero.
 */

/*
 * Obtain a hold on the tree. This garantees the tree doesn't go away.
 * When the receiver is done, it should do a release.
 */
void
clconf_obj_hold(clconf_obj_t *obj)
{
	((clnode *)obj)->hold();
}

/*
 * Release the hold we have on the object.
 */
void
clconf_obj_release(clconf_obj_t *obj)
{
	((clnode *)obj)->rele();
}

/*
 * deep copy an existing object and return with hold count = 1
 */
clconf_obj_t *
clconf_obj_deep_copy(clconf_obj_t *obj)
{
	clconf_obj_hold(obj);
	clnode *node = (clnode *)obj;
	// copy_tree will construct a new tree with a seperate clrefcnt
	// object that has count 1.
	clnode *new_tree = (clnode *)node->copy_tree();
	clconf_obj_release(obj);
	return ((clconf_obj_t *)new_tree);
}

/*
 * delete the object along with all its children.
 */
void
clconf_obj_deep_delete(clconf_obj_t *obj)
{
	((clnode *)obj)->delete_tree();
}

/*
 * Returns the incarnation number for the tree this object is in.
 */
uint_t
clconf_obj_get_incarnation(clconf_obj_t *obj)
{
	clnode *nd = (clnode *)obj;
	return (nd->get_incn());
}

/*
 * ***********************************************************************
 * Interfaces with a cluster object.
 * ***********************************************************************
 */

/*
 * Get the current cluster object.
 */
clconf_cluster_t *
clconf_cluster_get_current()
{
	clconf_cluster_t *cl = (clconf_cluster_t *)
	    cl_current_tree::the().get_root();
	ASSERT(cl != NULL);
	return (cl);
}

/*
 * Create an empty cluster object.
 */
clconf_cluster_t *
clconf_cluster_create()
{
	return ((clconf_cluster_t *)new clnode("-1"));
}

/* Get the iterators */
clconf_iter_t *
clconf_cluster_get_nodes(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "nodes"));
}

clconf_iter_t *
clconf_cluster_get_cables(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "cables"));
}

clconf_iter_t *
clconf_cluster_get_bbs(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "blackboxes"));
}

clconf_iter_t *
clconf_cluster_get_quorum_devices(clconf_cluster_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_CLUSTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "quorum_devices"));
}

/*
 * Add components to a cluster.
 * Returns CL_BADNAME if the component added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_cluster_add_node(clconf_cluster_t *cl, clconf_node_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("nodes", (clnode *)n));
}



clconf_errnum_t
clconf_cluster_add_cable(clconf_cluster_t *cl, clconf_cable_t *c)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("cables", (clnode *)c));
}


clconf_errnum_t
clconf_cluster_add_bb(clconf_cluster_t *cl, clconf_bb_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("blackboxes", (clnode *)n));
}

clconf_errnum_t
clconf_cluster_add_quorum_device(clconf_cluster_t *cl, clconf_quorum_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	return (((clnode *)cl)->add_to_group("quorum_devices", (clnode *)n));
}


/*
 * Remove components from a cluster.
 * CL_INVALID_TREE if the component to be removed is not found in the cluster.
 */
clconf_errnum_t
clconf_cluster_remove_node(clconf_cluster_t *cl, clconf_node_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_NODE);
	return (((clnode *)cl)->remove_from_group("nodes", (clnode *)n));
}



clconf_errnum_t
clconf_cluster_remove_cable(clconf_cluster_t *cl, clconf_cable_t *c)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)c) == CL_CABLE);
	return (((clnode *)cl)->remove_from_group("cables", (clnode *)c));
}


clconf_errnum_t
clconf_cluster_remove_bb(clconf_cluster_t *cl, clconf_bb_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_BLACKBOX);
	return (((clnode *)cl)->remove_from_group("blackboxes",
	    (clnode *)n));
}

clconf_errnum_t
clconf_cluster_remove_quorum_device(clconf_cluster_t *cl, clconf_quorum_t *n)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_QUORUM_DEVICE);
	return (((clnode *)cl)->remove_from_group("quorum_devices",
	    (clnode *)n));
}

// Helper function

const char *
clconf_cluster_get_cmm_property_str(clconf_cluster_t *cl, const char *prtname,
    const char *default_value)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)cl) == CL_CLUSTER);
	ccr_node *cm = ((clnode *)cl)->get_named_child("cmm");
	if (cm == NULL) {
		return (default_value);
	}
	ccr_node *pts = cm->get_named_child("properties");
	if (pts == NULL) {
		return (default_value);
	}
	ccr_node *pt = pts->get_named_child(prtname);
	if (pt == NULL) {
		return (default_value);
	}
	const char *prt = pt->get_value();
	if (prt == NULL || prt[0] == 0) {
		// Return default value.
		return (default_value);
	}
	return (prt);
}

int
clconf_cluster_get_cmm_property(clconf_cluster_t *cl, const char *prtname,
    int default_value)
{
	const char *v = clconf_cluster_get_cmm_property_str(cl, prtname, NULL);
	if (v == NULL) {
		return (default_value);
	}
	return (os::atoi(v));
}

int
clconf_cluster_get_msgretries(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "msgretries", 3));
}

int
clconf_cluster_get_orb_return_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_return_timeout",
	    30000 /* milliseconds */));
}

int
clconf_cluster_get_orb_stop_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_stop_timeout",
	    30000 /* milliseconds */));
}

int
clconf_cluster_get_orb_abort_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_abort_timeout",
	    30000 /* milliseconds */));
}

int
clconf_cluster_get_orb_step_timeout(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "orb_step_timeout",
	    120000 /* milliseconds */));
}

int
clconf_cluster_get_failfast_grace_time(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "failfast_grace_time",
	    10000 /* milliseconds */));
}

int
clconf_cluster_get_failfast_panic_delay(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "failfast_panic_delay",
	    30000 /* milliseconds */));
}

// This is in addition to the failfast_panic_delay

int
clconf_cluster_get_failfast_proxy_panic_delay(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl,
	    "proxy_failfast_panic_delay", 5000 /* milliseconds */));
}

int
clconf_cluster_get_node_fenceoff_timeout(clconf_cluster_t *cl)
{
	//
	// fenceoff_timeout is the timeout to decide that a
	// non-communicating node is dead and that it is safe to take over HA
	// services from that node. This is used to protect against data
	// corruption in case the cluster gets partitioned and the different
	// partitions take different amounts of time to run the quorum
	// algorithm to decide which partition should survive. This timeout
	// includes the time it takes a partitioned node to start a
	// reconfiguration, run the quorum algorithm and decide the outcome
	// of the quorum race. The winner of the quorum race waits for
	// node_fenceoff_timeout before declaring the non-communicating node
	// dead. If a node has not finished the quorum algorithm in this time,
	// it fails fast.
	//
	return (clconf_cluster_get_cmm_property(cl, "fenceoff_timeout",
	    300000 /* milliseconds */));
}

int
clconf_cluster_get_boot_delay(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property(cl, "boot_delay",
		60000 /* milliseconds */));
}

int
clconf_cluster_get_node_halt_timeout(clconf_cluster_t *cl)
{
	//
	// halt_timeout is the failfast timeout used by a shutting down node,
	// enabled right before it sends the shutting down message to other
	// nodes in the cluster. It is also the timeout used by the other nodes
	// for safely declaring the shutting down node to be in the dead state
	// after getting the shutting down message from that node.
	//
	return (clconf_cluster_get_cmm_property(cl, "halt_timeout",
	    5000 /* milliseconds */));
}


const char *
clconf_cluster_get_failfast_mode(clconf_cluster_t *cl)
{
	return (clconf_cluster_get_cmm_property_str(cl, "failfast_mode",
	    "PANIC"));
}

/* Returns NULL if the node is not found */
const char *
clconf_cluster_get_nodename_by_nodeid(clconf_cluster_t *cl, nodeid_t id)
{
	clconf_iter_t *iter = clconf_cluster_get_nodes(cl);
	clconf_obj_t *obj;

	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		CL_PANIC(clconf_obj_get_objtype(obj) == CL_NODE);
		if ((nodeid_t)clconf_obj_get_id(obj) == id) {
			clconf_iter_release(iter);
			return (clconf_obj_get_name(obj));
		}
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);
	return (NULL);
}

/*
 * Gets node id given the nodename
 * If nodename is NULL then it returns the local nodeid
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 */
nodeid_t
clconf_cluster_get_nodeid_by_nodename(char *nodename)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter;
	clconf_node_t		*node;
	nodeid_t nid;
	const char *name;

	if (nodename == NULL) {
		return (clconf_get_local_nodeid());
	}

	nid = (nodeid_t)NODEID_UNKNOWN;

	cl = clconf_cluster_get_current();
	ASSERT(cl != NULL);
	iter = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {
		name = clconf_obj_get_name((clconf_obj_t *)node);
		if (name != NULL && strcmp(name, nodename) == 0) {
			nid = (nodeid_t)clconf_obj_get_id(
			    (clconf_obj_t *)node);
			break;
		}
	}

	clconf_iter_release(iter);
	clconf_obj_release((clconf_obj_t *)cl);
	return (nid);
}

/*
 * Get the network address for the cluster. This is the private net number
 * assigned to the cluster.
 */
static struct in_addr_solaris	pvtnet_addr;

const struct in_addr *
clconf_get_cluster_network_addr()
{
	const char *ccrent_private_net_number;
	clconf_cluster_t *cl;

	if (pvtnet_addr._S_un._S_un_b.s_b1 == 0) /* first time ? */ {
		/*
		 * read this info from the ccr
		 */
		cl = clconf_cluster_get_current();
		ccrent_private_net_number =
		    clconf_obj_get_property((clconf_obj_t *)cl,
		    "private_net_number");
		clconf_obj_release((clconf_obj_t *)cl);
		if (ccrent_private_net_number == NULL) {
			/*
			 * Use a default value
			 */
			ccrent_private_net_number = CL_DEFAULT_NET_NUMBER;
		}
		ASSERT(ccrent_private_net_number != NULL);

		pvtnet_addr._S_un._S_addr =
			    os::inet_addr(ccrent_private_net_number);
		if (pvtnet_addr._S_un._S_addr == (in_addr_t)(-1)) {
			pvtnet_addr._S_un._S_addr = 0;
			return (NULL);
		}
	}

	ASSERT(pvtnet_addr._S_un._S_addr > 0);
	return ((const struct in_addr *)&pvtnet_addr);
}

/*
 * Determine whether we are using the flexible IP Addressing scheme
 */
int
flex_ip_address()
{
	const char *ccrent_private_subnet_netmask;
	clconf_cluster_t *cl;

	cl = clconf_cluster_get_current();
	ccrent_private_subnet_netmask =
	    clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_subnet_netmask");
	clconf_obj_release((clconf_obj_t *)cl);
	if (ccrent_private_subnet_netmask == NULL)
		return (0);
	else
		return (1);
}

/*
 * Get the logical IP address for the specified node. This is
 * determined as a property of the node.
 */
int
clconf_get_user_ipaddr(nodeid_t nodeid, struct in_addr *addr)
{
	const char 		*ccrent_user_ipaddr = NULL;
	clconf_cluster_t 	*cl;
	const struct in_addr	*mynet = NULL;

	if (nodeid < 1 || nodeid > NODEID_MAX)
		return (-1);

	/*
	 * read the userip network number info from the ccr
	 */
	if (flex_ip_address()) {
		cl = clconf_cluster_get_current();
		ccrent_user_ipaddr = clconf_obj_get_property(
				(clconf_obj_t *)cl,
				"private_user_net_number");
		clconf_obj_release((clconf_obj_t *)cl);
		if (ccrent_user_ipaddr == NULL) {
			return (-1);
		}

		// Convert the host part to host order for bitwise
		// operations
		addr->_S_un._S_addr =
			os::inet_addr(ccrent_user_ipaddr) | ntohl(nodeid);
		if (addr->_S_un._S_addr == (in_addr_t)(-1)) {
			addr->_S_un._S_addr = 0;
			return (-1);
		}

		return (0);
	} else {
		mynet = clconf_get_cluster_network_addr();
		if (mynet == NULL)
			return (-1);

		addr->_S_un._S_un_w.s_w1 = mynet->_S_un._S_un_w.s_w1;
		addr->_S_un._S_un_b.s_b3 = LOGICAL_PERNODE_B3;
		addr->_S_un._S_un_b.s_b4 = (uint8_t)nodeid;
		return (0);
	}

}

/*
 * Gets node id given the ip address, if the given ip address is the
 * private hostname of a node.
 * The ip address is specified in in_addr_t.
 *
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 */
nodeid_t
clconf_cluster_get_nodeid_for_private_ipaddr(in_addr_t ipaddr)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter;
	clconf_node_t		*node;
	struct in_addr		cipaddr;
	nodeid_t nid;
	nodeid_t id;

	nid = (nodeid_t)NODEID_UNKNOWN;

	cl = clconf_cluster_get_current();
	ASSERT(cl != NULL);
	iter = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter)) != NULL;
	    clconf_iter_advance(iter)) {

		id = (nodeid_t)clconf_obj_get_id((clconf_obj_t *)node);
		if ((clconf_get_user_ipaddr(id, &cipaddr) == 0) &&
		    (ipaddr == cipaddr._S_un._S_addr)) {
			nid = id;
			break;
		}
	}

	clconf_iter_release(iter);
	clconf_obj_release((clconf_obj_t *)cl);
	return (nid);
}

/*
 * Gets node id given the ip address.
 * The ip address is specified in in_addr_t.
 *
 * Returns NODEID_UNKNOWN (0) if the nodeid is not found
 *	returns nodeid (nid) and adapter id (adpid) as side-effects.
 *	User provides allocated ptrs for nid and adpid
 *	On success these ptrs are set to appropriate values.
 *	On failure nid contains NODEID_UNKNOWN and adpid is set to 0.
 */
nodeid_t
clconf_cluster_get_nodeid_for_ipaddr(nodeid_t *nid, int *adpid,
					in_addr_t ipaddr)
{
	clconf_cluster_t	*cl	= NULL;
	clconf_iter_t		*iter1, *iter2;
	clconf_node_t		*node;
	clconf_adapter_t	*adp;
	const char		*cl_ipaddr;
	in_addr_t		cipaddr;

	ASSERT(nid != NULL);
	ASSERT(adpid != NULL);
	ASSERT(ipaddr != NULL);

	*nid = (nodeid_t)NODEID_UNKNOWN;
	*adpid = 0;

	cl = clconf_cluster_get_current();
	ASSERT(cl != NULL);
	iter1 = clconf_cluster_get_nodes(cl);

	for (; (node = (clconf_node_t *)
	    clconf_iter_get_current(iter1)) != NULL;
	    clconf_iter_advance(iter1)) {

	    iter2 = clconf_node_get_adapters(node);

	    for (; (adp = (clconf_adapter_t *)
		clconf_iter_get_current(iter2)) != NULL;
		clconf_iter_advance(iter2)) {

		if ((cl_ipaddr = clconf_obj_get_property
		    ((clconf_obj_t *)adp, "ip_address")) == NULL)
		    continue;

		cipaddr = os::inet_addr(cl_ipaddr);
		if (cipaddr == (in_addr_t)-1) {
			continue;
		}

		if (ipaddr == cipaddr) {
		    *nid = (nodeid_t)clconf_obj_get_id((clconf_obj_t *)node);
		    *adpid = clconf_obj_get_id((clconf_obj_t *)adp);
		    clconf_iter_release(iter2);
		    clconf_iter_release(iter1);
		    clconf_obj_release((clconf_obj_t *)cl);
		    return (*nid);
		}
	    }
	    clconf_iter_release(iter2);
	}
	clconf_iter_release(iter1);
	clconf_obj_release((clconf_obj_t *)cl);
	return (*nid);
}

/*
 * Create a cluster node object.
 */
clconf_node_t *
clconf_node_create()
{
	return ((clconf_node_t *)new clnode("-1"));
}

/* Get the adapter iterator */
clconf_iter_t *
clconf_node_get_adapters(clconf_node_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_NODE);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "adapters"));
}

/*
 * Add an adapter to a node.
 * Returns CL_BADNAME if the adapter added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_node_add_adapter(clconf_node_t *n, clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_NODE);
	return (((clnode *)n)->add_to_group("adapters", (clnode *)ada));
}

/*
 * Remove an adapter from a node.
 * CL_INVALID_TREE if the adapter to be removed is not found in the node.
 */
clconf_errnum_t
clconf_node_remove_adapter(clconf_node_t *n, clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)n) == CL_NODE);
	return (((clnode *)n)->remove_from_group("adapters", (clnode *)ada));
}


/*
 * Get the private hostname of this node. The default would be
 * "<nodename>-priv"
 */
const char *
clconf_node_get_private_hostname(clconf_node_t *nd)
{
	const char *ret = clconf_obj_get_property((clconf_obj_t *)nd,
	    "private_hostname");
	if (ret == NULL) {
		// construct the hostname.
		const char *ndname = clconf_obj_get_name((clconf_obj_t *)nd);
		char *hstname = new char[os::strlen(ndname) +
		    os::strlen("-priv") + 1];
		os::sprintf(hstname, "%s-priv", ndname);

		// add the private hostname to the property.
		clconf_errnum_t err = clconf_obj_set_property(
		    (clconf_obj_t *)nd, "private_hostname", hstname);
		CL_PANIC(err == CL_NOERROR);
		ret = hstname;
	}
	return (ret);
}

/*
 * Properties that we can access through clconf_obj_get_property.
 * List properties here...
 */



/*
 * ***********************************************************************
 * Interfaces with an adapter object.
 * ***********************************************************************
 */

/*
 * Create a cluster adapter object.
 */
clconf_adapter_t *
clconf_adapter_create()
{
	return ((clconf_adapter_t *)new clnode("-1"));
}

/*
 * Returns the name of the device
 */
const char *
clconf_adapter_get_device_name(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	return (clconf_obj_get_property((clconf_obj_t *)ada, "device_name"));
}

/*
 * Some devices have two drivers, one for the device and one for the DLPI
 * driver implemented as a layered driver.
 * The latter is indicated with a "dlpi_device_name" property in the adapter
 * We expect that the instance number of the device and the DLPI device are
 * the same.
 * Returns the name of the DLPI device
 */
const char *
clconf_adapter_get_dlpi_device_name(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	//
	// First get the dlpi_device_name if one exists on the adapter
	// If none, then get the device name itself
	//
	const char *dn;
	dn = clconf_obj_get_property((clconf_obj_t *)ada, "dlpi_device_name");
	if (dn == NULL) {
		dn = clconf_adapter_get_device_name(ada);
	}
	return (dn);
}

/*
 * Returns the instance number of the device
 * Returns -1, if one does not exist
 */
int
clconf_adapter_get_device_instance(clconf_adapter_t *ada)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	const char *prpt = clconf_obj_get_property((clconf_obj_t *)ada,
	    "device_instance");
	if (prpt != NULL) {
		return (os::atoi(prpt));
	}
	return (-1);
}

/* Get the port iterator */
clconf_iter_t *
clconf_adapter_get_ports(clconf_adapter_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_ADAPTER);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "ports"));
}

/*
 * Add a port to an adapter.
 * Returns CL_BADNAME if the port added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_adapter_add_port(clconf_adapter_t *ada, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	return (((clnode *)ada)->add_to_group("ports", (clnode *)pt));
}

/*
 * Remove a port from an adapter. Returns
 * CL_INVALID_TREE if the port to be removed is not found in the adapter.
 */
clconf_errnum_t
clconf_adapter_remove_port(clconf_adapter_t *ada, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	return (((clnode *)ada)->remove_from_group("ports", (clnode *)pt));
}

/*
 * Get/set the transport type.
 */
const char *
clconf_adapter_get_transport_type(clconf_adapter_t *a)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)a) == CL_ADAPTER);
	return (clconf_obj_get_property((clconf_obj_t *)a, "transport_type"));
}

void
clconf_adapter_set_transport_type(clconf_adapter_t *ada, const char *type)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	(void) clconf_obj_set_property((clconf_obj_t *)ada, "transport_type",
	    type);
}

void
clconf_adapter_set_device_name(clconf_adapter_t *ada, const char *devn)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	(void) clconf_obj_set_property((clconf_obj_t *)ada, "device_name",
	    devn);
}

void
clconf_adapter_set_device_instance(clconf_adapter_t *ada, int inst)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)ada) == CL_ADAPTER);
	char n[32];
	(void) os::itoa(inst, n);
	(void) clconf_obj_set_property((clconf_obj_t *)ada, "device_instance",
	    n);
}

/*
 * ***********************************************************************
 * Interfaces with an port object.
 * ***********************************************************************
 */

/*
 * Create a cluster port object.
 */
clconf_port_t *
clconf_port_create()
{
	return ((clconf_port_t *)new clnode("-1"));
}

/*
 * ***********************************************************************
 * Interfaces with an black box object.
 * ***********************************************************************
 */

/*
 * Create a cluster bb object.
 */
clconf_bb_t *
clconf_bb_create()
{
	return ((clconf_bb_t *)new clnode("-1"));
}

/*
 * Set and get type.
 */
void
clconf_bb_set_type(clconf_bb_t *bb, const char *type)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	(void) clconf_obj_set_property((clconf_obj_t *)bb, "type", type);
}

const char *
clconf_bb_get_type(clconf_bb_t *bb)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	return (clconf_obj_get_property((clconf_obj_t *)bb, "type"));
}

/* Get the port iterator */
clconf_iter_t *
clconf_bb_get_ports(clconf_bb_t *obj)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)obj) == CL_BLACKBOX);
	return (clconf_obj_get_iter((clconf_obj_t *)obj, "ports"));
}

/*
 * Add a port to a black box.
 * Returns CL_BADNAME if the port added has a invalid name.
 * Returns CL_INVALID_ID if its id conflicts with others.
 * If the its id is not set and the add is successful, its id will be
 * set by the clconf to a proper value.
 */
clconf_errnum_t
clconf_bb_add_port(clconf_bb_t *bb, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	return (((clnode *)bb)->add_to_group("ports", (clnode *)pt));
}

/*
 * Remove a port from the black box. Returns
 * CL_INVALID_TREE if the port to be removed is not found in the bb.
 */
clconf_errnum_t
clconf_bb_remove_port(clconf_bb_t *bb, clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)bb) == CL_BLACKBOX);
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	return (((clnode *)bb)->remove_from_group("ports", (clnode *)pt));
}

/*
 * ***********************************************************************
 * Interfaces with an cable object.
 * ***********************************************************************
 */

/*
 * Create a cluster cable object.
 */
clconf_cable_t *
clconf_cable_create()
{
	return ((clconf_cable_t *)new clnode("-1"));
}

/*
 * Get and set port. End number can only be 1 or 2.
 */
clconf_port_t *
clconf_cable_get_endpoint(clconf_cable_t *cbp, int endnum)
{
	CL_PANIC(endnum == 1 || endnum == 2);
	const char *enddes = clconf_obj_get_property((clconf_obj_t *)cbp,
	    (endnum == 1 ? "end1" : "end2"));
	if (enddes == NULL)
		return (NULL);
	clconf_obj_t *cl = clconf_obj_get_parent((clconf_obj_t *)cbp);

	ccr_node *ret = ((clnode *)cl)->get_child_by_name(enddes);
	return ((clconf_port_t *)ret);
}

clconf_errnum_t
clconf_cable_set_endpoint(clconf_cable_t *cbp, int endnum, clconf_port_t *pt)
{
	char *fname = ((clnode *)pt)->get_full_name();

	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	CL_PANIC(endnum == 1 || endnum == 2);
	(void) clconf_obj_set_property((clconf_obj_t *)cbp,
	    (endnum == 1 ? "end1" : "end2"), fname);

	delete [] fname;
	return (CL_NOERROR);
}

/* Functions and data structures related to paths */

SList<clconf_path_t> *clconf_path_list = NULL;

/* This should be put into a C++ header file */
SList<clconf_path_t> &
clconf_path_get_all()
{
	ASSERT(clconf_path_list != NULL);
	return (*clconf_path_list);
}

void
clconf_path_set_endpoint(clconf_path_t *p, clconf_endtype_t et,
    clconf_port_t *pt)
{
	CL_PANIC(clconf_obj_get_objtype((clconf_obj_t *)pt) == CL_PORT);
	clconf_obj_t *ada = clconf_obj_get_parent((clconf_obj_t *)pt);
	clconf_obj_t *nd = clconf_obj_get_parent(ada);
	p->end[et].nodeid = (nodeid_t)clconf_obj_get_id(nd);
	p->end[et].adapterid = clconf_obj_get_id(ada);
	p->end[et].portid = clconf_obj_get_id((clconf_obj_t *)pt);
}

clconf_path_t *
clconf_path_create(clconf_port_t *p1, clconf_port_t *p2, uint_t q, uint_t t)
{
	clconf_path_t *p = new clconf_path_t;
	clconf_path_set_endpoint(p, CL_LOCAL, p1);
	clconf_path_set_endpoint(p, CL_REMOTE, p2);
	p->quantum = q;
	p->timeout = t;

	// Make sure p1 is a local port and p2 is remote.
	CL_PANIC(p->end[CL_LOCAL].nodeid == orb_conf::local_nodeid());
	CL_PANIC(p->end[CL_REMOTE].nodeid != orb_conf::local_nodeid());
	return (p);
}

clconf_path_t *
clconf_path_dup(clconf_path_t *p)
{
	clconf_path_t *pt = new clconf_path_t;
	(*pt) = (*p);
	return (pt);
}

void
clconf_path_release(clconf_path_t *p)
{
	delete p;
}

clconf_adapter_t *
clconf_path_get_adapter(clconf_path_t *p, clconf_endtype_t et,
    clconf_cluster_t *cl)
{
	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl, CL_NODE,
	    (int)p->end[et].nodeid);
	return ((clconf_adapter_t *)clconf_obj_get_child(nd, CL_ADAPTER,
	    p->end[et].adapterid));
}

/*
 * Constructs the adapter name from the given adapter object and returns
 * it as a string. Caller should free up the memory after its usage is
 * done. The adapter name is constructed as a concatenation of device_name
 * and device_instance.
 */
char *
clconf_get_adaptername(clconf_adapter_t *ap)
{
	const char *devname, *devinst;
	char *adpname;

	devname = clconf_adapter_get_device_name(ap);
	devinst = clconf_obj_get_property((clconf_obj_t *)ap,
	    "device_instance");
	ASSERT((devname != NULL) && (devinst != NULL));
	adpname = new char[strlen(devname)+strlen(devinst)+1];
	ASSERT(adpname != NULL);
	os::sprintf(adpname, "%s%s", devname, devinst);

	return (adpname);
}

clconf_port_t *
clconf_path_get_endpoint(clconf_path_t *p, clconf_endtype_t et,
    clconf_cluster_t *cl)
{
	clconf_obj_t *nd = clconf_obj_get_child((clconf_obj_t *)cl,
	    CL_NODE, (int)p->end[et].nodeid);
	clconf_obj_t *ada = clconf_obj_get_child(nd, CL_ADAPTER,
	    p->end[et].adapterid);
	clconf_obj_t *pt = clconf_obj_get_child(ada, CL_PORT,
	    p->end[et].portid);
	return ((clconf_port_t *)pt);
}

/*
 * Constructs the path name from the given path and cluster  objects and returns
 * it as a string. Caller should free up the memory after its usage is
 * done. pathname is constructed as local_adaptername - remote_adaptername
 * local_adaptername is local_nodename:adaptername
 * remote_adaptername is remote_nodename:adaptername
 */
char *
clconf_get_pathname(clconf_path_t *p, clconf_cluster_t *cl)
{
	char *path_name, *adpname;
	const char *nodename;
	char lpe_name[64], rpe_name[64];

	clconf_adapter_t *adp = clconf_path_get_adapter(p, CL_LOCAL, cl);
	ASSERT(adp);
	nodename = clconf_obj_get_name(clconf_obj_get_parent(
	    (clconf_obj_t *)adp));
	adpname = clconf_get_adaptername(adp);
	ASSERT((nodename != NULL) && (adpname != NULL));
	if (os::snprintf(lpe_name, 64UL, "%s:%s", nodename, adpname) >= 63)
		lpe_name[63] = '\0';
	delete [] adpname;

	adp = clconf_path_get_adapter(p, CL_REMOTE, cl);
	ASSERT(adp);
	nodename = clconf_obj_get_name(clconf_obj_get_parent(
	    (clconf_obj_t *)adp));
	adpname = clconf_get_adaptername(adp);
	ASSERT((nodename != NULL) && (adpname != NULL));
	if (os::snprintf(rpe_name, 64UL, "%s:%s", nodename, adpname) >= 63)
		rpe_name[63] = '\0';
	delete [] adpname;

	path_name = new char [strlen(lpe_name)+strlen(rpe_name)+4];
	ASSERT(path_name != NULL);
	os::sprintf(path_name, "%s - %s", lpe_name, rpe_name);
	return (path_name);
}

uint_t
clconf_path_get_quantum(clconf_path_t *p)
{
	return (p->quantum);
}

uint_t
clconf_path_get_timeout(clconf_path_t *p)
{
	return (p->timeout);
}

/* Get a path */
clconf_path_t *
clconf_cluster_get_path(int local_adapterid, nodeid_t remote_nodeid,
    int remote_adapterid)
{
	clconf_path_t *p;
	SList<clconf_path_t>::ListIterator iter(clconf_path_get_all());
	while ((p = iter.get_current()) != NULL) {
		if (p->end[CL_LOCAL].adapterid == local_adapterid &&
		    p->end[CL_REMOTE].nodeid == remote_nodeid &&
		    p->end[CL_REMOTE].adapterid == remote_adapterid) {
			return (p);
		}
		iter.advance();
	}
	return (NULL);
}

nodeid_t
clconf_path_get_remote_nodeid(clconf_path_t *p)
{
	return (p->end[CL_REMOTE].nodeid);
}

int
clconf_path_get_adapterid(clconf_path_t *p, clconf_endtype_t ed)
{
	return (p->end[ed].adapterid);
}

int
clconf_path_get_portid(clconf_path_t *p, clconf_endtype_t ed)
{
	return (p->end[ed].portid);
}

// Compares two paths. Return true if they are the same, false if not.
// Both paths should be local paths. We only compare the topology part
// of the path. Quantum and timeout are not included.
bool
clconf_path_equal(clconf_path_t *p1, clconf_path_t *p2)
{
	if (p1 == p2)
		return (true);

	if (p1 == NULL || p2 == NULL)
		return (false);

	CL_PANIC(p1->end[0].nodeid == p2->end[0].nodeid);

	return (p1->end[0].adapterid == p2->end[0].adapterid &&
	    p1->end[0].portid == p2->end[0].portid &&
	    p1->end[1].nodeid == p2->end[1].nodeid &&
	    p1->end[1].adapterid == p2->end[1].adapterid &&
	    p1->end[1].portid == p2->end[1].portid);
}

// This is for the convenience of TM only. TM holds the pointer to an old
// tree and it needs to find out the path associated with two ports.
// p1 is local, p2 is remote.
clconf_path_t *
clconf_get_path(clconf_port_t *p1, clconf_port_t *p2)
{
	// Construct a path using p1 and p2.
	clconf_path_t pth;
	clconf_path_set_endpoint(&pth, CL_LOCAL, p1);
	clconf_path_set_endpoint(&pth, CL_REMOTE, p2);

	// Search the current path list to find the specified path.
	clconf_path_t *p;
	SList<clconf_path_t>::ListIterator iter(clconf_path_get_all());
	while ((p = iter.get_current()) != NULL) {
		if (clconf_path_equal(p, &pth)) {
			return (p);
		}
		iter.advance();
	}
	return (NULL);
}
