/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)mdb_clconf_io.cc	1.12	08/05/20 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <clconf/clnode.h>
#include <sys/mdb_modapi.h>
#include <mdb_clconf_io.h>
#include "mdb_names.h"

// This file is similar to src/clconf/clconf_file_io.cc. Here we try to
// build clconf tree from the core dump.
bool
mdb_clconf_io::initialize()
{
	bool	error;

	if (is_initialized) {
		return (true);
	}
	if (cl_current_tree::initialize() != 0) {
		return (false);
	}
	cl_current_tree::the().set_io_interface(this);
	if (!cl_current_tree::the().read_ccr(&error)) {
		return (false);
	}
	is_initialized = true;
	return (true);
}


void
mdb_clconf_io::shutdown()
{
	if (!is_initialized) {
		return;
	}

	// Frees up the hold on the current tree. This should delete
	// the tree assuming everyone who did a hold did a rele.
	cl_current_tree::the().set_root(NULL);
	cl_current_tree::shutdown();

	is_initialized = false;
}

mdb_clconf_io::mdb_clconf_io(bool flag)
{
	entries = NULL;
	pos = 0;
	is_initialized = false;
	alloc_entries = flag;
	count = 0;
}

int32_t
mdb_clconf_io::get_version()
{
	return (0);
}

mdb_clconf_io::~mdb_clconf_io()
{
	entries = NULL;
	is_initialized = false;
}

clconf_io::status
mdb_clconf_io::open()
{
	uintptr_t addr;
	cl_current_tree *mdb_cl;
	clnode *rootnd;
	uint_t n_ent;

	// Read the symbol cl_current_tree::the_cl_current_tree from the
	// core file.
	if (mdb_readsym(&addr, sizeof (addr), MDB_THE_CL_CURRENT_TREE)
	    != (int)sizeof (addr)) {
		mdb_warn("Failed to read the_clcurrent_tree\n");
		return (C_FAIL);
	}
	// Allocate the structure and read the object from the core dump
	// into the newly allocated memory.
	mdb_cl = (cl_current_tree *)mdb_alloc(sizeof (cl_current_tree),
	    UM_SLEEP);

	if (mdb_vread((void *)mdb_cl, sizeof (cl_current_tree), addr)
	    != (int)sizeof (cl_current_tree)) {
		mdb_warn("failed to read cl_current_tree at %p", addr);
		return (C_FAIL);
	}
	// Get the root of the clconf tree and find out the number of
	// nodes in the tree so that we can allocate the required memory.
	if ((rootnd = mdb_cl->mdb_get_root()) == NULL)
		return (C_FAIL);

	count = rootnd->get_count();
	n_ent = 2 * count + 2;
	if (alloc_entries) {
		entries = (char **)
		    mdb_alloc((size_t)(n_ent*sizeof (char *)), UM_SLEEP);
		for (uint_t i = 0; i < n_ent; i++)
			entries[i] = NULL;
	}
	// Now read the all nodes from the core dump and construct
	// the clconf tree
	rootnd->get_nodes(entries, pos);
	pos = 0;

	if (alloc_entries)
		mdb_free(rootnd, sizeof (clnode));
	mdb_free(mdb_cl, sizeof (cl_current_tree));
	return (C_SUCCESS);
}

void
mdb_clconf_io::close()
{
	if (entries != NULL) {
		mdb_free(entries, (2*(size_t)count+2)*sizeof (char *));
		entries = NULL;
	}
}

// This method is similar to clconf_ccr::read(), clconf_file_io::read()
// Returns false when the end is reached.
clconf_io::status
mdb_clconf_io::read_one(char **key, char **value)
{
	if (entries == NULL)
		return (C_FAIL);
	if (entries[pos] == NULL)
		return (C_FAIL);

	*key = (char *)mdb_alloc((size_t)(strlen(entries[pos]) + 1),
	    UM_SLEEP);
	*value = (char *)mdb_alloc((size_t)(strlen(entries[pos+1]) + 1),
	    UM_SLEEP);

	(void) strcpy(*key, entries[pos++]);
	(void) strcpy(*value, entries[pos++]);

	mdb_free(entries[pos-2], (size_t)300);
	mdb_free(entries[pos-1], (size_t)100);
	return (C_SUCCESS);
}

void
mdb_clconf_io::print()
{
	int i = 0;
	if (entries != NULL) {
		while (entries[i] != NULL) {
			mdb_printf("%s", entries[i]);
			i++;
			if (i%2 == 0)
				mdb_printf("\n");
		}
	}
}

bool
mdb_clconf_io::begin_transaction()
{
	return (false);
}

bool
mdb_clconf_io::commit_transaction()
{
	ASSERT(0);
	return (false);
}

void
mdb_clconf_io::abort_transaction()
{
	ASSERT(0);
}

bool
mdb_clconf_io::write(const char *, const char *)
{
	ASSERT(0);
	return (false);
}

int
mdb_clconf_io::write_seq(void *)
{
	ASSERT(0);
	return (0);
}

void
mdb_clconf_io::delete_seq(void *)
{
	ASSERT(0);
}

void *
mdb_clconf_io::create_seq()
{
	ASSERT(0);
	return (NULL);
}
