//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mdb_infra.cc	1.4	08/05/20 SMI"

#include <sys/os.h>
#include <sys/mdb_modapi.h>
#include <sys/list_def.h>
#include <sys/clconf_int.h>
#include <clconf/clnode.h>
#include <mdb_util.h>
#include <mdb_clconf_io.h>
#include <mdb_cluster.h>

//
// name : get_fullname
// arguments: 	prnt - address of the parent node
// 		nm   - resultant name is kept in this.
//
// This method recursively finds out the fullname of the current node
// starting from the root. At each level the name is appended to finally
// construct the fullname.
//

void
get_fullname(uintptr_t prnt, char *nm)
{
	ccr_node *p;
	char nstr[100];

	if (prnt != 0) {
		p = (ccr_node *)mdb_alloc(sizeof (ccr_node), UM_SLEEP);
		(void) mdb_vread((void *)p, sizeof (ccr_node), prnt);
		get_fullname((uintptr_t)p->get_parent(), nm);
		(void) mdb_getstring((uintptr_t)p->get_name(), nstr,
		    (size_t)100);
		if (nm[0] != '\0')
			(void) strcat(nm, ".");
		(void) strcat(nm, nstr);
		mdb_free(p, sizeof (ccr_node));
	}
}

//
// get_count traverses the tree from the current node to find out the
// total number of nodes below that node. If called with root node object
// this method calculates the number of nodes in that tree.
//
uint_t
ccr_node::get_count()
{
	uintptr_t addr;
	ccr_node *child;
	uint_t cnt = 1;

	child = (ccr_node *)mdb_alloc(sizeof (ccr_node), UM_SLEEP);
	addr = (uintptr_t)children.first();
	while (addr != 0) {
		if (mdb_vread((void *)child, sizeof (ccr_node), addr)
		    != (int)sizeof (ccr_node)) {
			mdb_warn("failed to read ccrnode at %p", addr);
			break;
		} else {
			cnt += child->get_count();
		}
		addr = (uintptr_t)child->next();
	}
	mdb_free(child, sizeof (ccr_node));
	return (cnt);
}

void
clnode::get_nodes(char **buff, int &index)
{
	ccr_node::get_nodes(buff, index);
}

//
// get_nodes() traverses the tree from the current node and saves the
// node names in the "buff", "index" keeps track of the current pointer in
// the buff. This is called recursively to traverse the tree.
// If buff is NULL then the node names at each level are printed out
// instead of saving in the buff.
//
void
ccr_node::get_nodes(char **buff, int &index)
{
	uintptr_t addr;
	ccr_node *child;
	char *pstr, *str, *ky;
	int mode;

	mode = (buff == NULL) ? 1 : 0;
	pstr = (char *)mdb_alloc((size_t)200, (uint_t)UM_SLEEP);
	str = (char *)mdb_alloc((size_t)100, (uint_t)UM_SLEEP);
	ky = (char *)mdb_alloc((size_t)300, (uint_t)UM_SLEEP);
	(void) strcpy(pstr, "");
	get_fullname((uintptr_t)parent, pstr);
	if (name != NULL) {
		(void) mdb_getstring((uintptr_t)name, str, (size_t)100);
		os::sprintf(ky, "%s%s%s", pstr, (parent == 0)?"":".", str);
		if (mode)
			mdb_printf("%s", ky);
		else
			buff[index++] = ky;
	}
	if (value != NULL) {
		(void) mdb_getstring((uintptr_t)value, str, (size_t)100);
		if (mode)
			mdb_printf("\t%s", str);
		else
			buff[index++] = str;
	} else {
		index--;
		mdb_free(str, (size_t)100);
	}

	if (mode)
		mdb_printf("\n");
	child = (ccr_node *)mdb_alloc(sizeof (ccr_node), UM_SLEEP);
	addr = (uintptr_t)children.first();
	while (addr != 0) {
		if (mdb_vread((void *)child, sizeof (ccr_node), addr)
		    != (int)sizeof (ccr_node)) {
			mdb_warn("failed to read ccrnode at %p", addr);
			break;
		} else {
			child->get_nodes(buff, index);
		}
		addr = (uintptr_t)child->next();
	}
	mdb_free(child, sizeof (ccr_node));
	mdb_free(pstr, (size_t)200);
}

//
// mdb_get_root() returns the address of the "root"
//
clnode *
cl_current_tree::mdb_get_root()
{
	clnode *cln;

	cln = (clnode *)mdb_alloc(sizeof (clnode), UM_SLEEP);
	if (mdb_vread((void *)cln, sizeof (clnode), (uintptr_t)root)
	    != (int)sizeof (clnode)) {
		mdb_warn("failed to read clnode at %p", (uintptr_t)root);
		return (NULL);
	}
	return (cln);
}

//
// mdb_infra is an mdb entry point called when dcmd "::infra" is invoked
// to print the infrastructure file contents from the core dump that are
// stored in the form of clconf tree.
//
int
mdb_infra(uintptr_t, uint_t, int argc, const mdb_arg_t *)
{
	mdb_clconf_io *mcl;

	if (argc > 0)
		return (DCMD_USAGE);
	mcl = new mdb_clconf_io(false);
	mdb_printf("\t\t\tCurrent Infrastructure File Contents\n");
	if (! mcl->initialize()) {
		mdb_warn("Unable to initialize clconf in mdb\n");
		return (NULL);
	}
	mcl->shutdown();
	return (DCMD_OK);
}
