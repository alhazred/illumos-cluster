//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mdb_paths.cc	1.10	08/05/20 SMI"

#include <sys/os.h>
#include <sys/mdb_modapi.h>
#include <sys/list_def.h>
#include <sys/clconf_int.h>
#include <clconf/clnode.h>
#include <mdb_clconf_io.h>
#include <mdb_cluster.h>
#include <orb/transport/path_manager.h>
#include <orb/transport/transport.h>
#include <orb/transport/hb_threadpool.h>
#include <mdb_list_def_in.h>
#include <paths_list_def_in.h>
#include "mdb_names.h"

typedef struct clconf_endpoint {
	nodeid_t nodeid;
	int adapterid;
	int portid;	//lint -e754
} clconf_endpoint_t;


struct clconf_path {
	clconf_endpoint_t end[2];
	uint_t quantum;
	uint_t timeout;
};

//
// clconf_cluster_get_adpname_by_ids() finds the adapter name from the
// nodeid and adpater id by traversing the tree using node iterators and
// adapter iterators. If not found then NULL is returned.
//
const char *
clconf_cluster_get_adpname_by_ids(clconf_cluster_t *cl, uint_t nid, int aid)
{
	clconf_iter_t *iter = clconf_cluster_get_nodes(cl);
	clconf_obj_t *obj;

	while ((obj = clconf_iter_get_current(iter)) != NULL) {
		CL_PANIC(clconf_obj_get_objtype(obj) == CL_NODE);
		if (clconf_obj_get_id(obj) == (int)nid) {
			clconf_iter_t *aiter =
			    clconf_node_get_adapters((clconf_node_t *)obj);
			while ((obj = clconf_iter_get_current(aiter)) != NULL) {
				if (clconf_obj_get_id(obj) == (int)aid) {
					clconf_iter_release(iter);
					clconf_iter_release(aiter);
					return (clconf_obj_get_name(obj));
				}
				clconf_iter_advance(aiter);
			}
			clconf_iter_release(aiter);
		}
		clconf_iter_advance(iter);
	}
	clconf_iter_release(iter);
	return (NULL);
}


clconf_cluster_t *
mdb_getclconf()
{
	mdb_clconf_io *mcl;
	clconf_cluster_t *mclconf;

	mcl = new mdb_clconf_io(true);
	if (! mcl->initialize()) {
		mdb_warn("Unable to initialize clconf in mdb\n");
		return (NULL);
	}
	mclconf = clconf_cluster_get_current();
	if (mclconf == NULL) {
		mdb_warn("Could not get the clconf in mdb\n");
		return (NULL);
	}
	mcl->shutdown();
	return (mclconf);
}

//
// Prints pathname from the clconf_path_t structure by getting the
// clconf tree and looking into the tree.
//
void
print_pathname(clconf_path_t *cpath)
{
	clconf_cluster_t *mcl = mdb_getclconf();
	mdb_printf("%s:%s - %s:%s", clconf_cluster_get_nodename_by_nodeid(
					mcl, cpath->end[0].nodeid),
			clconf_cluster_get_adpname_by_ids(mcl,
				cpath->end[0].nodeid, cpath->end[0].adapterid),
			clconf_cluster_get_nodename_by_nodeid(
					mcl, cpath->end[1].nodeid),
			clconf_cluster_get_adpname_by_ids(mcl,
				cpath->end[1].nodeid, cpath->end[1].adapterid));
}

//
// print_buf prints unwrapped. We traverse in the buffer where we wrapped
// around and then print that part first and then print the recent part.
//
void
print_buf(char *buffer, uint32_t size)
{
	char *tbuf = buffer;
	for (int i = 0; i < (int)size; i++) {
		if (buffer[i] == '\0' && buffer[i+1] != '\0')
			break;
		else
			tbuf++;
	}
	tbuf++;
	mdb_printf("%s%s\n", tbuf, buffer);
}

//
// Prints all the useful pathend data.
//
void
pathend::debugger_print(int verbose)
{
	char *buffer;
	uint32_t bsize;
	uintptr_t bufc;
	clconf_path_t *m_pth;
	char	st_strings[][15] = {"Constructed", "InitErr", "Inititated",
				    "Up", "Cleaning", "Draining", "DelClean",
				    "Deleting", "Deleted"};
	char	reasons[][15] 	=  {"None", "EP Fault", "HB timeout", "Removed",
				    "Peer Dead", "Disconnected", "Revoked",
				    "Revoked FI", "Other/Unknown"};

	m_pth = (clconf_path_t *)mdb_alloc(sizeof (clconf_path_t), UM_SLEEP);
	if (mdb_vread((void *)m_pth, sizeof (clconf_path_t),
	    (uintptr_t)clconf_pathp) != (int)sizeof (clconf_path_t)) {
		mdb_warn("failed to read clconf_path_t at %p\n", clconf_pathp);
		mdb_free(m_pth, sizeof (clconf_path_t));
		return;
	}
	if (verbose)
		printf("\t\t+++++++++++++++++++++++++++++++++++++++++++++++\n");
	print_pathname(m_pth);

	mdb_printf("\t(%s)\n", st_strings[(int)p_state]);

	// If not verbose do not print anymore information
	if (!verbose) {
		mdb_free(m_pth, sizeof (clconf_path_t));
		return;
	}

	mdb_printf("Timeout = %d msecs \tQuantum=%d msecs\n",
		m_pth->timeout, m_pth->quantum);
	mdb_free(m_pth, sizeof (clconf_path_t));

	mdb_printf("last sent at =\t\t0x%llx\nlast scheduled at =\t0x%llx\n",
		last_send_time, last_sendsched_time);
	mdb_printf("Previous registration time = %llx\n", last_reg_hrtime);
	mdb_printf("Time registered =\t0x%llx\n", cur_reg_hrtime);

	if (drop_reason != 0) {
		mdb_printf("Time dropped =\t\t0x%llx\t", drop_hrtime);
		mdb_printf("Drop reason = %s\n", reasons[(int)drop_reason]);
	}
	if (mdb_readsym(&bsize, sizeof (bsize), "perpe_hb_dbg_size")
	    != (int)sizeof (bsize)) {
		mdb_warn("Failed to read the perpe_hb_dbg_size \n");
		return;
	}
	if (mdb_vread((void *)&bufc, sizeof (bufc), (uintptr_t)hb_dbgp)
	    != (int)sizeof (bufc)) {
		mdb_warn("Failed to read the hb_dbgp\n");
		return;
	}
	if (bufc == NULL) {
		return;
	}
	buffer = (char *)mdb_alloc((size_t)bsize, UM_SLEEP);
	if (mdb_vread((void *)buffer, (size_t)bsize, bufc) != (int)bsize) {
		mdb_warn("Failed to read the hb_dbgp contents\n");
		mdb_free(buffer, (size_t)bsize);
		return;
	}
	mdb_printf("---------Current debug buffer-------Begin-----");
	print_buf(buffer, bsize);
	mdb_printf("---------Current debug buffer--------End------");
	if (mdb_vread((void *)&bufc, sizeof (bufc), (uintptr_t)hb_dbgo)
	    != (int)sizeof (bufc)) {
		mdb_warn("Failed to read the hb_dbgp\n");
		mdb_free(buffer, (size_t)bsize);
		return;
	}
	if (bufc == NULL) {
		mdb_free(buffer, (size_t)bsize);
		return;
	}
	if (mdb_vread((void *)buffer, (size_t)bsize, bufc) != (int)bsize) {
		mdb_warn("Failed to read the hb_dbgo\n");
		mdb_free(buffer, (size_t)bsize);
		return;
	}
	if (buffer[0] != '\0') {
		mdb_printf("=========Previous debug buffer======Begin======");
		print_buf(buffer, bsize);
		mdb_printf("=========Previous debug buffer=======End=======");
	}
	mdb_printf("\n\n");
	mdb_free(buffer, (size_t)bsize);
}

//
// transport class has an array of pathends for each node this node is
// connected to. For each node that is connected to this node, get the
// list of pathends and for each pathend, print its information.
//
void
transport::print_pathends_info(int verbose)
{
	pathend *pp, *m_pp;

	// For each node this node is connected to
	for (int ndid = 1; ndid <= NODEID_MAX; ndid++) {
		IntrList<pathend, _SList>::ListIterator li(the_pathends[ndid]);

		// for each pathend connecting to a node.
		for (; (pp = li.mdb_get_current()) != NULL; li.mdb_advance()) {
			m_pp = (pathend *)mdb_alloc(sizeof (pathend), UM_SLEEP);
			if (mdb_vread((void *)m_pp, sizeof (pathend),
			    (uintptr_t)pp) != (int)sizeof (pathend)) {
				mdb_warn("Failed to read the pathend\n");
				mdb_free(m_pp, sizeof (pathend));
				return;
			}
			mdb_printf("Pathend = 0x%p\t", pp);
			m_pp->debugger_print(verbose);
			mdb_free(m_pp, sizeof (pathend));
		}
	}
}

//
// get_paths() routine, traverses through the list of transports that
// the path manager has and calls print_pathends_info in each transport.
//
void
path_manager::get_paths(int verbose)
{
	pm_client *pmcp;
	transport *trans;
	pmclist_t::ListIterator iter(transport_list);

	for (; (pmcp = iter.mdb_get_current()) != NULL; iter.mdb_advance()) {
		trans = (transport *)mdb_alloc(sizeof (transport), UM_SLEEP);
		if (mdb_vread((void *)trans, sizeof (transport),
		    (uintptr_t)pmcp) != (int)sizeof (transport)) {
			mdb_warn("Failed to read the transport\n");
			mdb_free(trans, sizeof (transport));
			return;
		}
		trans->print_pathends_info(verbose);
		mdb_free(trans, sizeof (transport));
	}
}


//
// Entry point for the mdb's dcmd "::paths". This first reads
// path_manager::the_path_manager address from the core file and then
// reads in the path manager object and then invokes its get_paths method
// to print the paths.
//
int
mdb_paths(uintptr_t, uint_t, int argc, const mdb_arg_t *argv)
{
	uintptr_t pmaddr;
	path_manager	*pm;
	int verbose = FALSE;

	if (mdb_getopts(argc, argv, 'v', MDB_OPT_SETBITS, TRUE, &verbose, NULL)
	    != argc)
		return (DCMD_USAGE);
	if (mdb_readsym(&pmaddr, sizeof (pmaddr),
	    MDB_THE_PATH_MANAGER) != (int)sizeof (pmaddr)) {
		mdb_warn("Failed to read the path manager\n");
		return (DCMD_ERR);
	}
	pm = (path_manager *)mdb_alloc(sizeof (path_manager), UM_SLEEP);
	if (mdb_vread((void *)pm, sizeof (path_manager), pmaddr) !=
	    (int)sizeof (path_manager)) {
		mdb_warn("Failed to read the path_manager\n");
		mdb_free(pm, sizeof (path_manager));
		return (DCMD_ERR);
	} else {
		pm->get_paths(verbose);
	}
	mdb_free(pm, sizeof (path_manager));
	return (DCMD_OK);
}

//
// Entry point for the mdb dcmd "peinfo". Invoked with pathend object
// read into memory.
//
int
pe_info(uintptr_t addr, uint_t flag, int, const mdb_arg_t *)
{
	if ((flag & DCMD_ADDRSPEC) == 0)
		return (DCMD_USAGE);

	pathend *pep = (pathend *)mdb_alloc(sizeof (pathend), UM_SLEEP);

	if (mdb_vread((void *)pep, sizeof (pathend), addr) !=
	    (int)sizeof (pathend)) {
		mdb_warn("failed to read pathend\n");
		mdb_free(pep, sizeof (pathend));
		return (DCMD_ERR);
	} else {
		pep->debugger_print(1);
	}
	mdb_free(pep, sizeof (pathend));
	return (DCMD_OK);
}

//
// Prints heartbeat threadpool related private data
//
void
hb_threadpool::debugger_print()
{
	uintptr_t hbt_dbg_addr;
	char *buffer;
	uint32_t bsize = 8192;

	mdb_printf("\t\t\tHB threadpool information\n");
	mdb_printf("\tNo. of threads = %d\n", nthreads);
	mdb_printf("\tNo. of heartbeats sent by interrupts = 0x%llx\n", int_hb);
	mdb_printf("\tNo. of heartbeats sent by RT threads = 0x%llx\n", rt_hb);
	mdb_printf("\tNo. of heartbeats pending in the list = %d\n", count);
	mdb_printf("\tNo. of heartbeats currently executing = %d\n", rt_pend);
	mdb_printf("\tTime to shutdown is %s\n",
			(time_to_shutdown) ? "true" : "false");
	if (mdb_readsym(&hbt_dbg_addr, sizeof (hbt_dbg_addr), "hbt_dbg") !=
			(int)sizeof (hbt_dbg_addr)) {
		mdb_warn("Failed to read the hbt_dbg \n");
		return;
	}
	buffer = (char *)mdb_alloc((size_t)bsize, UM_SLEEP);
	if (mdb_vread((void *)buffer, (size_t)bsize, hbt_dbg_addr) != 8192) {
		mdb_warn("Failed to read the hbt_dbg buffer\n");
		mdb_free(buffer, (size_t)bsize);
		return;
	}
	mdb_printf("\n----------------hbt_dbg buffer-----------------------\n");
	print_buf(buffer, bsize);
}

//
// Entry point for the mdb dcmd "::hbtinfo". Reads in the
// hb_threadpool::the_heartbeat_threadpool address and then reads in the
// hb_threadpool object and then debugger_print method is invoked
//
int
hbt_info(uintptr_t, uint_t, int, const mdb_arg_t *)
{
	hb_threadpool *hbt;
	uintptr_t hbtaddr;

	if (mdb_readsym(&hbtaddr, sizeof (hbtaddr),
	    MDB_THE_HEARTBEAT_THREADPOOL) !=
	    (int)sizeof (hbtaddr)) {
		mdb_warn("Failed to read the path manager\n");
		return (DCMD_ERR);
	}
	hbt = (hb_threadpool *)mdb_alloc(sizeof (hb_threadpool), UM_SLEEP);
	if (mdb_vread((void *)hbt, sizeof (hb_threadpool), hbtaddr) !=
	    (int)sizeof (hb_threadpool)) {
		mdb_warn("Failed to read the hb_threadpool\n");
		mdb_free(hbt, sizeof (hb_threadpool));
		return (DCMD_ERR);
	} else {
		hbt->debugger_print();
	}
	mdb_free(hbt, sizeof (hb_threadpool));
	return (DCMD_OK);
}

void
path_manager::debugger_print()
{
	uintptr_t pm_dbg_addr;
	char *buffer;
	uint32_t bsize = 32768;

	mdb_printf("\t\t\tPath Manager Information\n");
	mdb_printf("\thrlast = \t\t0x%llx\n", hrlast);
	mdb_printf("\tcurrent solt = \t\t%d\n", schedule_slot);
	mdb_printf("\thrtlen = \t\t%lld\n", hrtlen);
	mdb_printf("\tpmtlen = \t\t%ld\n", pmtlen);
	mdb_printf("\tmax_clockthread_delay = %lld nsecs\n",
						max_clockthread_delay);
#ifdef REPEAT_TO_CATCHUP
	mdb_printf("\tno.of times catchup done = \t%d\n", ncatchup);
#endif
	if (mdb_readsym(&pm_dbg_addr, sizeof (pm_dbg_addr), "pm_dbg") !=
			(int)sizeof (pm_dbg_addr)) {
		mdb_warn("Failed to read the pm_dbg \n");
		return;
	}
	buffer = (char *)mdb_alloc((size_t)bsize, UM_SLEEP);
	if (mdb_vread((void *)buffer, (size_t)bsize, pm_dbg_addr) !=
	    (int)bsize) {
		mdb_warn("Failed to read the pm_dbg buffer\n");
		mdb_free(buffer, (size_t)bsize);
		return;
	}
	mdb_printf("\n----------------pm_dbg buffer-----------------------\n");
	print_buf(buffer, bsize);
}

//
// Entry point for the mdb dcmd "pminfo". Reads in the path manager object
// from the core and prints its private data.
//
int
pm_info(uintptr_t, uint_t, int, const mdb_arg_t *)
{
	uintptr_t pmaddr;
	path_manager	*pm;

	if (mdb_readsym(&pmaddr, sizeof (pmaddr),
	    MDB_THE_PATH_MANAGER) != (int)sizeof (pmaddr)) {
		mdb_warn("Failed to read the path manager\n");
		return (DCMD_ERR);
	}
	pm = (path_manager *)mdb_alloc(sizeof (path_manager), UM_SLEEP);
	if (mdb_vread((void *)pm, sizeof (path_manager), pmaddr) !=
	    (int)sizeof (path_manager)) {
		mdb_warn("Failed to read the path_manager\n");
		mdb_free(pm, sizeof (path_manager));
		return (DCMD_ERR);
	} else {
		pm->debugger_print();
	}
	mdb_free(pm, sizeof (path_manager));
	return (DCMD_OK);
}
