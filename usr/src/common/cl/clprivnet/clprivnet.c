/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clprivnet.c	1.18	09/02/18 SMI"

/*
 * 			Overview
 *
 * The clprivnet device driver is implemented as a style2 DLPI compliant
 * Streams driver, that is, opening of the special file /dev/clprivnet creates
 * a new Stream [4], but an explicit attach primitive must subsequently be
 * used.
 *
 * The driver is installed at /kernel/drv for 32-bit kernels and
 * kernel/sparcv9/drv for 64-bit kernels. The configuration file is
 * clprivnet.conf. The driver is loaded as the result of an inter-module
 * dependency specified in the Sun Cluster cl_comm module.
 *
 * For cluster resident applications, the central component of the IP packet
 * distribution mechanism is the clprivnet Streams driver. This driver provides
 * a DLPI device driver interface for IP and serves as a proxy on behalf of the
 * physical subnet interfaces. The driver is incorporated in the lower part of
 * the IP protocol stack and controls the interface for Streams pertaining to
 * the per-node subnet (clusternode[x]-priv). The local per-node IP address is
 * configured on the clprivnet device. All packets pertaining to per-node
 * subnet addresses are directed by the IP layer to clprivnet and subsequently
 * redirected to drivers for the physical devices. The device selection
 * algorithm attempts to provide equal distribution among the physical
 * interfaces. Redirection is supported by a special Stream opened for each
 * physical device driver instance, using a private SAP. When redirecting a
 * packet, clprivnet uses this special Stream queue to deliver the packet to
 * physical interface driver. MAC addressing information used by IP for packet
 * encapsulation consists of clprivnet source and destination MAC addresses;
 * but in the process of redirection, physical MAC addresses (both source and
 * destination) are substituted. The physical MAC addresses are obtained from
 * internal cluster information which is distributed among all nodes. Incoming
 * packets are passed directly to the IP layer - no processing by clprivnet is
 * required. However, ARP protocol packets do require some receive side
 * processing by the clprivnet driver.
 *
 *
 * Per-node IP addresses are allocated and reserved for use by cluster resident
 * applications to form socket connections. These are IP Class B addresses
 * allocated as 173.16.193.n with n equal to nodeid and a netmask of
 * 255.255.255.0. Applications use the cluster nsswitch library to retrieve the
 * per-node addresses associated with host interfaces named clusternode[n]-priv
 *  where again n is the cluster nodeid.
 *
 * The per-node IP address for the local node is configured on the
 * /dev/clprivnet0 device. The existing cluster ifkconfig kernel level utility
 * is invoked from the ipconf module (ipconf.cc) initialization to do this.
 *
 * The Maximum Transmission Unit (MTU) is reported as part of the response
 * to DL_INFO_REQ. The value is that of pure Ethernet (1500) regardless of
 * the actual MTU of the physical interfaces to which packets are redirected.
 *
 * A /dev/clprivnet MAC address is used to satisfy the DL_PHYS_ADDR_REQ
 * primitive. An address length of six bytes is used as with Ethernet.
 * MAC addresses used in clprivnet link layer headers (for source and
 * destination) correspond to the cluster node ids.
 *
 * The clprivnet driver provides Solaris fast path support and IP can generate
 * packets as M_DATA message blocks which contain the clprivnet link layer
 * header. The driver can accept both fast path M_DATA message blocks and
 * M_PROTO DL_UNITDATA_REQ message blocks. In both cases an M_DATA message
 * block is transferred to the packet redirection.
 *
 * Outbound packets are transferred by the driver through a procedure call to
 * the packet redirection object in the cl_comm module. When the redirection
 * object initializes, it registers the address of the transfer procedure with
 * this driver.
 *
 * Inbound IP packets are delivered directly by the physical interfaces and are
 * not handled by the clprivnet driver. However, the operations of ARP have,
 * in general, a per-interface association. Therefore, to support interaction
 * with ARP in a standard and normal way, reception of ARP packets by the
 * clprivnet driver is provided. A packet receive procedure is called from the
 * redirection Stream module (clfpstr) and clprivnet sends the packet up to ARP.
 */

#include	<sys/types.h>
#include	<sys/errno.h>
#include	<sys/debug.h>
#include	<sys/stropts.h>
#include	<sys/stream.h>
#include	<sys/strlog.h>
#include	<sys/cmn_err.h>
#include	<sys/varargs.h>
#include	<sys/kmem.h>
#include	<sys/conf.h>
#include	<sys/stat.h>
#include	<sys/dlpi.h>
#include 	<sys/modctl.h>
#include	<sys/ddi.h>
#include	<sys/sunddi.h>
#include	<sys/strsun.h>
#include	"clprivnet.h"
#include	<netinet/in.h>
#include	<sys/cladm.h>

int clprivnet_modunload_ok = 0;


static void (*pkt_redirect)(uint_t, ushort_t, queue_t *, mblk_t *);
static void (*fragment_timeout)(queue_t *, mblk_t *);
static char (*dlpri_to_bband)(uint_t);

/*
 * Function prototypes.
 */
static	int clprivnet_attach(dev_info_t *, ddi_attach_cmd_t);
static	int clprivnet_detach(dev_info_t *, ddi_detach_cmd_t);
static	int clprivnet_info(dev_info_t *, ddi_info_cmd_t, void *, void **);
static	int clprivnet_open(queue_t *, dev_t *, int, int, cred_t *);
static	int clprivnet_close(queue_t *, int, cred_t *);
static	int clprivnet_wput(queue_t *, mblk_t *);
static	int clprivnet_wsrv(queue_t *);
static	void clprivnet_proto(queue_t *, mblk_t *);
static	void clprivnet_ioctl(queue_t *, mblk_t *);
static	void clprivnet_areq(queue_t *, mblk_t *);
static	void clprivnet_dreq(queue_t *, mblk_t *);
static	void clprivnet_dodetach(struct clprivnet_str *);
static	void clprivnet_breq(queue_t *, mblk_t *);
static	void clprivnet_ubreq(queue_t *, mblk_t *);
static	void clprivnet_ireq(queue_t *, mblk_t *);
static	void clprivnet_udreq(queue_t *, mblk_t *);
static  void clprivnet_pareq(queue_t *, mblk_t *);
static void clprivnet_ponreq(queue_t *, mblk_t *);
static void clprivnet_poffreq(queue_t *, mblk_t *);
static  void clprivnet_dl_ioc_hdr_info(queue_t *, mblk_t *);
static void localmedaddr(struct clprivnet_addr *);
static	struct clprivnet_str *clprivnet_accept(struct clprivnet_str *,
    struct clprivnet *, ushort_t, struct clprivnet_addr *);
static	struct clprivnet_str *clprivnet_paccept(struct clprivnet_str *,
    struct clprivnet *, ushort_t, struct clprivnet_addr *);
static	void clprivnet_sendup(struct clprivnet *, mblk_t *,
    struct clprivnet_str *(*)(struct clprivnet_str *, struct clprivnet *,
    ushort_t, struct clprivnet_addr *));
#ifdef  KSTAT
static  void    clprivnet_statinit(struct clprivnet_ *);
#endif
void clprivnet_set_pkt_redirect(void (*)(uint_t, ushort_t, queue_t *,
    mblk_t *));
void clprivnet_set_fragment_timeout(void (*)());
void clprivnet_set_local_nodeid(uint_t);

static void	bindack(queue_t *, mblk_t *, t_scalar_t, void *,
    t_uscalar_t, t_uscalar_t, t_uscalar_t);
static void	okack(queue_t *, mblk_t *, t_uscalar_t);
static void	errorack(queue_t *, mblk_t *, t_uscalar_t,
    t_uscalar_t, t_uscalar_t);
static void	uderrorind(queue_t *, mblk_t *, void *, t_uscalar_t,
		    t_uscalar_t, t_uscalar_t);
static void	physaddrack(queue_t *, mblk_t *, void *, t_uscalar_t);

/*
 * The local nodeid is used as the mac address for the clprivnet
 * pseudo interface.
 */
static uint_t clprivnet_local_nodeid = 0;



typedef enum	{
	REDIRECT	= 0,
	PROTO		= 1,
	IOCTL		= 2,
	STREAMS		= 3,
	DEVOPS		= 4,
	DEVCONFIG	= 5,
	AUTOCONFIG	= 6
} clprivnet_dbg_msg_t;

uchar_t clprivnet_debug_enabled[] = {
	0,	/* REDIRECT	*/
	0,	/* PROTO	*/
	0,	/* IOCTL	*/
	0,	/* STREAMS	*/
	0,	/* DEVOPS	*/
	0,	/* DEVCONFIG	*/
	0	/* AUTOCONFIG	*/
};

static int max_dbg_msgs = sizeof (clprivnet_debug_enabled) / sizeof (uchar_t);

#ifdef  DEBUG
static void clprivnet_dbprintf(clprivnet_dbg_msg_t, char *, ...);
#define	CLPRIVNET_DBPRINTF(arguments) clprivnet_dbprintf arguments
#else
#define	CLPRIVNET_DBPRINTF(arguments)
#endif  /* DEBUG */

#ifdef MAXIMUM
#undef MAXIMUM
#endif
#define	MAXIMUM(x, y)	((x) > (y) ? (x) : (y))

/*
 * Allocate and zero-out "number" structures
 * each of type "structure" in kernel memory.
 */
#define	GETSTRUCT(structure, number)   \
	((structure *) kmem_zalloc(\
		(sizeof (structure) * (number)), KM_NOSLEEP))

/*
 * Physical Medium broadcast address definition.
 */
static	struct clprivnet_addr	medbroadcastaddr = {0xff,
	0xff, 0xff, 0xff, 0xff, 0xff };


/*
 * Linked list of clprivnet structures
 * Only clprivnet0 is used - so there will only be one entry on the list
 */
static	struct clprivnet *clprivnet_listp = NULL;

/*
 * Linked list of active driver Streams.  The list lock is also used
 * for serialization of opens and closes.
 */
static	struct	clprivnet_str	*clprivnet_str_listp = NULL;
static	krwlock_t	clprivnet_str_list_lock;

t_uscalar_t clprivnet_mtu = CLPRIVNET_MTU;

/*
 *  DL_INFO_ACK template.
 */
static	dl_info_ack_t clprivnet_infoack = {
	DL_INFO_ACK,			/* dl_primitive			*/
	CLPRIVNET_MTU,			/* dl_max_sdu			*/
	0,				/* dl_min_sdu			*/
	(unsigned)CLPRIVNET_ADDRL,	/* dl_addr_length		*/
	DL_ETHER,			/* dl_mac_type			*/
	0,				/* dl_reserved			*/
	0,				/* dl_current_state		*/
	-2,				/* dl_sap_length		*/
	DL_CLDLS,			/* dl_service_mode		*/
	0,				/* dl_qos_length		*/
	0,				/* dl_qos_offset		*/
	0,				/* dl_range_length		*/
	0,				/* dl_range_offset		*/
	DL_STYLE2,			/* dl_provider_style		*/
	sizeof (dl_info_ack_t),		/* dl_addr_offset		*/
	DL_VERSION_2,			/* dl_version			*/
	ETHERADDRL,			/* dl_brdcst_addr_length	*/
	(unsigned)(sizeof (dl_info_ack_t) + (unsigned)CLPRIVNET_ADDRL),
					/* dl_bcst_adr_offset		*/
	0				/* dl_growth			*/
};


/*
 * Standard Streams declarations
 */

/*
 * Module identification and limit values
 */
static	struct	module_info	clprivnet_minfo = {
	CLPRIVNET_IDNUM,				/* mi_idnum  */
	CLPRIVNET_NAME,					/* mi_idname */
	CLPRIVNET_MINPSZ,				/* mi_minpsz */
	CLPRIVNET_MAXPSZ,				/* mi_maxpsz */
	CLPRIVNET_HIWAT,				/* mi_hiwat  */
	CLPRIVNET_LOWAT					/* mi_lowat  */
};


/*
 * The  qinit structure contains pointers  to  processing  procedures
 * for  a   QUEUE.  The functions clprivnet_open and
 * clprivnet_close are common for the module so are only defined in the
 * read queue.
 */
static	struct	qinit	clprivnet_rinit = {
	0,						/* qi_putp	*/
	0,						/* qi_srvp	*/
	(int (*)(void))clprivnet_open,			/* qi_qopen	*/
	(int (*)(void))clprivnet_close,			/* qi_qclose	*/
	0,						/* qi_qadmin 	*/
	&clprivnet_minfo,				/* qi_minfo 	*/
	NULL,						/* qi_mstat	*/
	NULL,						/* qi_rw	*/
	NULL,						/* qi_infop 	*/
	0						/* qi_struiot	*/
};
static	struct	qinit	clprivnet_winit = {
	(int (*)(void))clprivnet_wput,			/* qi_putp	*/
	(int (*)(void))clprivnet_wsrv,			/* qi_srvp	*/
	0,						/* qi_qopen	*/
	0,						/* qi_qclose	*/
	0,						/* qi_qadmin	*/
	&clprivnet_minfo,				/* qi_minfo	*/
	NULL,						/* qi_mstat	*/
	NULL,						/* qi_rw	*/
	NULL,						/* qi_infop	*/
	0						/* qi_struiot	*/
};


/*
 * streamtab contains addresses of qinit structures
 */
static struct	streamtab	clprivnetinfo = {
	&clprivnet_rinit,				/* st_rdinit	*/
	&clprivnet_winit,				/* st_wrinit	*/
	NULL,						/* st_muxrinit	*/
	NULL						/* st_muxwrinit	*/
};



/*
 * Module Loading/Unloading and Autoconfiguration declarations
 *
 *
 * cb_ops contains the driver entry points and is roughly equivalent
 * to the cdevsw and bdevsw  structures in previous releases.
 *
 * dev_ops contains, in addition to the pointer to cb_ops, the routines
 * that support loading and unloading the driver.
 *
 * Unsupported entry points are set to nodev, except for the poll
 * routine , which is set to nochpoll(), a routine that returns ENXIO.
 */

static struct cb_ops clprivnet_cb_ops = {
	nodev,			/* cb_open	*/
	nodev,			/* cb_close	*/
	nodev,			/* cb_strategy	*/
	nodev,			/* cb_print	*/
	nodev,			/* cb_dump	*/
	nodev,			/* cb_read	*/
	nodev,			/* cb_write	*/
	nodev,			/* cb_ioctl	*/
	nodev,			/* cb_devmap	*/
	nodev,			/* cb_mmap	*/
	nodev,			/* cb_segmap	*/
	nochpoll, 		/* cb_chpoll	*/
	ddi_prop_op, 		/* cb_prop_op	*/
	&clprivnetinfo, 	/* cb_stream	*/
	D_MP | D_NEW, 		/* cb_flag	*/
	CB_REV,
	nodev,
	nodev
};

/*
 * Device operations structure
 */
static struct dev_ops clprivnet_ops = {
	DEVO_REV,					/* devo_rev	*/
	0,						/* devo_refcnt	*/
	clprivnet_info,					/* devo_getinfo	*/
	nulldev,					/* devo_identify */
	nulldev,					/* devo_probe	*/
	clprivnet_attach,				/* devo_attach	*/
	clprivnet_detach,				/* devo_detach	*/
	nodev,						/* devo_reset	*/
	&clprivnet_cb_ops,				/* devo_cb_ops	*/
	(struct bus_ops *)NULL,				/* devo_bus_ops	*/
	NULL						/* devo_power	*/
};


/*
 * Module linkage information for the kernel.
 */
static struct modldrv modldrv = {
	&mod_driverops,					  /* type is driver */
	"Cluster Pseudo Streams DLPI Driver Version 2",   /* Description    */
	&clprivnet_ops					  /* driver ops	    */
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modldrv, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modldrv, NULL, NULL, NULL }
#endif
};



/*
 * Module Loading and Installation Routines .
 */


/*
 * Install the driver
 */
int
_init(void)
{
	int retval;

	CLPRIVNET_DBPRINTF((AUTOCONFIG, " _init: \n"));


	if ((cluster_bootflags & CLUSTER_BOOTED) == 0) {
		CLPRIVNET_DBPRINTF((AUTOCONFIG,
			"not booted in cluster mode\n"));
		return (EINVAL);
	}

	/* initialize global locks here */
	rw_init(&clprivnet_str_list_lock, "clprivnet_ streams linked list lock",
		RW_DRIVER, NULL);

	retval = mod_install(&modlinkage);
	if (retval)
		rw_destroy(&clprivnet_str_list_lock);

	CLPRIVNET_DBPRINTF((AUTOCONFIG, "mod_install retval = %x\n", retval));
	return (retval);
}


/*
 * Module Removal
 */
int
_fini(void)
{
	int	status;

	CLPRIVNET_DBPRINTF((AUTOCONFIG, " _fini: \n"));

	if ((status = mod_remove(&modlinkage)) != 0) {
		CLPRIVNET_DBPRINTF((AUTOCONFIG, " _fini mod_remove failed\n"));
		return (status);
	}

	rw_destroy(&clprivnet_str_list_lock);

	CLPRIVNET_DBPRINTF((AUTOCONFIG, " _fini return status = %d\n", status));
	return (status);
}

/*
 * Return Module Info.
 */
int
_info(struct modinfo *modinfop)
{
	int retval;
	CLPRIVNET_DBPRINTF((AUTOCONFIG, " _info: \n"));
	retval = mod_info(&modlinkage, modinfop);
	return (retval);
}




/*
 * Interface exists: make available by filling in network interface
 * record.  System will initialize the interface when it is ready
 * to accept packets.
 */
static int
clprivnet_attach(dev_info_t *dip, ddi_attach_cmd_t cmd)
{
	struct clprivnet	*clprivnet_p;

	CLPRIVNET_DBPRINTF((DEVCONFIG, " clprivnet_attach: \n"));

	if (cmd != DDI_ATTACH)
		return (DDI_FAILURE);

	/*
	 * Allocate soft data structure
	 */
	clprivnet_p = GETSTRUCT(struct clprivnet, 1);

	/*
	 * Stuff private info into dip.
	 */
	ddi_set_driver_private(dip, (caddr_t)clprivnet_p);
	clprivnet_p->clpn_dip = dip;

	/*
	 * Link this per-device structure in with the rest. For now there
	 * will only be one so no locking is needed.
	 */
	clprivnet_p->clpn_nextp = clprivnet_listp;
	clprivnet_listp = clprivnet_p;

	/*
	 * Get the clprivnet mac address - store in clprivnet_p->clpn_ouraddr.
	 * The cluster local nodeid is used as the mac address
	 */
	localmedaddr(&(clprivnet_p->clpn_ouraddr));

	/*
	 * Create the filesystem device node.
	 */
	/*lint -e732 */
	if (ddi_create_minor_node(dip, "clprivnet", S_IFCHR,
		ddi_get_instance(dip), DDI_NT_NET, CLONE_DEV) == DDI_FAILURE) {
		CLPRIVNET_DBPRINTF((DEVCONFIG, "create_minor_node_failed \n"));
		ddi_remove_minor_node(dip, NULL);
		goto bad;
	}

#ifdef  KSTAT
	clprivnet_statinit(clprivnet_p);
#endif  /* KSTAT */

	ddi_report_dev(dip);

	return (DDI_SUCCESS);

bad:
	ddi_set_driver_private(dip, NULL);
	kmem_free((caddr_t)clprivnet_p, sizeof (struct clprivnet));


	CLPRIVNET_DBPRINTF((DEVCONFIG, " clprivnet_attach failed\n"));

	return (DDI_FAILURE);
}


/*
 * Detach - Free resources allocated in attach
 */
static int
clprivnet_detach(dev_info_t *dip, ddi_detach_cmd_t cmd)
{
	struct clprivnet	*clprivnet_p;

	CLPRIVNET_DBPRINTF((DEVCONFIG, " clprivnet_detach: \n"));

	if (cmd != DDI_DETACH)
		return (DDI_FAILURE);

	clprivnet_p = (struct clprivnet *)ddi_get_driver_private(dip);
	ddi_set_driver_private(dip, NULL);
	kmem_free((caddr_t)clprivnet_p, sizeof (struct clprivnet));



	/* release minor node */
	ddi_remove_minor_node(dip, NULL);

	return (DDI_SUCCESS);

}

/*
 * Translate "dev_t" to a pointer to the associated "dev_info_t".
 */
static int
clprivnet_info(dev_info_t *dip, ddi_info_cmd_t infocmd, void *arg,
	void **result)
{
	struct clprivnet_str	*str_p;
	dev_t		dev = (dev_t)arg;
	int		rc;

	CLPRIVNET_DBPRINTF((DEVCONFIG, " clprivnet_info: \n"));
	rw_enter(&clprivnet_str_list_lock, RW_READER);

	dip = NULL;
	for (str_p = clprivnet_str_listp; str_p; str_p = str_p->cs_nextp) {
		if (str_p->cs_minor == getminor(dev)) {
			break;
		}
	}

	if (str_p && str_p->cs_clprivnetp)
		dip = str_p->cs_clprivnetp->clpn_dip;

	rw_exit(&clprivnet_str_list_lock);


	switch (infocmd) {
	case DDI_INFO_DEVT2DEVINFO:
		if (dip) {
			*result = (void *)dip;
			rc = DDI_SUCCESS;
		} else
			rc = DDI_FAILURE;
		break;

	case DDI_INFO_DEVT2INSTANCE:
		if (dip) {
			*result = (void *)ddi_get_instance(dip);
			rc = DDI_SUCCESS;
		} else
			rc = DDI_FAILURE;
		break;

	default:
		rc = DDI_FAILURE;
		break;
	}
	return (rc);
}



/*
 * Streams interface routines
 */

static int
clprivnet_open(queue_t *rq, dev_t *devp, int flag, int sflag, cred_t *credp)
{
	struct clprivnet_str	*clprivnet_str_p;
	struct clprivnet_str	**prev_clprivnet_str_p;
	minor_t		minordev;
	int		rc = 0;

	flag = flag;
	credp = credp;
	CLPRIVNET_DBPRINTF((DEVOPS, " clprivnet_open: \n"));
	/*
	 * Make sure this driver was not configured as a STREAMS module.
	 */
	ASSERT(sflag != MODOPEN);

	/* Serialize all driver opens and closes. */
	rw_enter(&clprivnet_str_list_lock, RW_WRITER);

	/*
	 * Determine minor device number.
	 * The list of clprivnet_str structures is kept sorted by minor number.
	 * The structure at the begining of the list has the lowest minor
	 * number and the structure at the end of the list has the highest
	 * minor number.  When traversing the list, if a gap in sequence is
	 * found or the list end is reached, a new structure is inserted
	 * with the appropriate minor number.
	 */
	prev_clprivnet_str_p = &clprivnet_str_listp;
	if (sflag == CLONEOPEN) {
		minordev = 0;
		for (; ((clprivnet_str_p = *prev_clprivnet_str_p) != NULL);
		    prev_clprivnet_str_p = &clprivnet_str_p->cs_nextp) {
			if (minordev < clprivnet_str_p->cs_minor)
				break;
			minordev++;
		}
		*devp = makedevice(getmajor(*devp), minordev);
	} else
		minordev = getminor(*devp);

	if (rq->q_ptr)
		goto done;

	/*
	 * allocate a private data structure for the Stream being opened
	 */
	if ((clprivnet_str_p = GETSTRUCT(struct clprivnet_str, 1)) == NULL) {
		rc = ENOMEM;
		rw_exit(&clprivnet_str_list_lock);
		return (rc);
	}

	clprivnet_str_p->cs_minor = minordev;
	clprivnet_str_p->cs_rq = rq;
	clprivnet_str_p->cs_state = DL_UNATTACHED;
	clprivnet_str_p->cs_sap = 0;
	clprivnet_str_p->cs_flags = 0;
	clprivnet_str_p->cs_clprivnetp = NULL;
	clprivnet_str_p->cs_mccount = 0;
	clprivnet_str_p->cs_mctab = NULL;
	mutex_init(&clprivnet_str_p->cs_lock, "clprivnet_ stream lock",
	    MUTEX_DRIVER, (void *)0);

	/*
	 * Link new entry into the list of active entries so the list
	 * is sorted from low to high minor numbers.
	 */
	clprivnet_str_p->cs_nextp = *prev_clprivnet_str_p;
	*prev_clprivnet_str_p = clprivnet_str_p;

	/*
	 * initialize the Stream queue pair private data structure pointers
	 */
	rq->q_ptr = WR(rq)->q_ptr = clprivnet_str_p;

	/*
	 * Disable our write-side service procedure.
	 */
	noenable(WR(rq));

done:
	rw_exit(&clprivnet_str_list_lock);

	/* enable the put and service routines of the driver */
	qprocson(rq);
	return (rc);
}

static int
clprivnet_close(queue_t *rq, int flag, cred_t *credp)
{
	struct clprivnet_str	*clprivnet_str_p;
	struct clprivnet_str	**prev_clprivnet_str_p;

	ASSERT(rq);
	ASSERT(rq->q_ptr);
	flag = flag;
	credp = credp;

	CLPRIVNET_DBPRINTF((DEVOPS, " clprivnet_close: \n"));
	/*
	* Finish processing any messages before clean up, since
	* some of them might depend on certain structures,
	* like cs_clprivnetp. See 6635689.
	*/
	qprocsoff(rq);

	clprivnet_str_p = (struct clprivnet_str *)rq->q_ptr;

	/*
	 * Implicit detach Stream from interface.
	 */
	if (clprivnet_str_p->cs_clprivnetp)
		clprivnet_dodetach(clprivnet_str_p);

	/* Serialize all driver opens and closes. */
	rw_enter(&clprivnet_str_list_lock, RW_WRITER);

	/*
	 * Unlink the per-Stream entry from the active list and free it.
	 */
	for (prev_clprivnet_str_p = &clprivnet_str_listp;
	    ((clprivnet_str_p = *prev_clprivnet_str_p) != NULL);
	    prev_clprivnet_str_p = &clprivnet_str_p->cs_nextp)
		if (clprivnet_str_p == (struct clprivnet_str *)rq->q_ptr)
			break;
	ASSERT(clprivnet_str_p);
	*prev_clprivnet_str_p = clprivnet_str_p->cs_nextp;

	mutex_destroy(&clprivnet_str_p->cs_lock);
	kmem_free(clprivnet_str_p, sizeof (struct clprivnet_str));

	rq->q_ptr = WR(rq)->q_ptr = NULL;

	rw_exit(&clprivnet_str_list_lock);
	return (0);
}

static void
clprivnet_ioctl(queue_t *wq, mblk_t *mp)
{
	struct iocblk	*iocp = (struct iocblk *)mp->b_rptr;
	struct clprivnet_str	*clprivnet_str_p;

	CLPRIVNET_DBPRINTF((IOCTL, " clprivnet_ioctl: \n"));

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	switch (iocp->ioc_cmd) {
	case DLIOCRAW:
		/* set raw M_DATA mode */
		clprivnet_str_p->cs_flags |= CLPRIVNET_RAW;
		miocack(wq, mp, 0, 0);
		break;

	case DL_IOC_HDR_INFO:
		/* M_DATA "fastpath" info request */
		clprivnet_dl_ioc_hdr_info(wq, mp);
		break;


	default:
		miocnak(wq, mp, 0, EINVAL);
		break;
	}
}

/*
 * M_DATA "fastpath" (per Stream) info request.
 * Following the M_IOCTL mblk should come a DL_UNITDATA_REQ mblk.
 * We ack with an M_IOCACK pointing to the original DL_UNITDATA_REQ mblk
 * followed by an mblk containing the raw ethernet header corresponding
 * to the destination.  Subsequently, we can receive M_DATA msgs which
 * start with this header.  This also allows for sending up M_DATA
 * messages with b_rptr pointing to network-layer data (starting
 * with the IP packet header).
 */
static void
clprivnet_dl_ioc_hdr_info(queue_t *wq, mblk_t *mp)
{
	mblk_t 			*nmp;
	struct clprivnet_str	*clprivnet_str_p;
	struct clprivnet_dladdr *dlap;
	dl_unitdata_req_t	*dludp;
	struct clprivnet_hdr	*headerp;
	struct clprivnet	*clprivnet_p;
	long			off, len;
	int 			minsize;
	size_t 			hdr_size;

	CLPRIVNET_DBPRINTF((IOCTL, " clprivnet_dl_ioc_hdr_info: \n"));

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;

	/*
	 * Sanity check the request.
	 */
	minsize = (int)sizeof (dl_unitdata_req_t) + CLPRIVNET_ADDRL;
	if ((mp->b_cont == NULL) ||
		(MBLKL(mp->b_cont) < minsize) ||
		(*((uint32_t *)mp->b_cont->b_rptr) != DL_UNITDATA_REQ) ||
		((clprivnet_p = clprivnet_str_p->cs_clprivnetp) == NULL)) {
		miocnak(wq, mp, 0, EINVAL);
		return;
	}

	/*
	 * Sanity check the DL_UNITDATA_REQ destination address
	 * offset and length values.
	 */
	dludp = (dl_unitdata_req_t *)mp->b_cont->b_rptr;
	off = (long)dludp->dl_dest_addr_offset;
	len = (long)dludp->dl_dest_addr_length;
	if (!MBLKIN(mp->b_cont, off, len) || (len != CLPRIVNET_ADDRL)) {
		miocnak(wq, mp, 0, EINVAL);
		return;
	}

	/*
	 * data link address pointer
	 */
	dlap = (struct clprivnet_dladdr *)(mp->b_cont->b_rptr + off);

	/*
	 * Allocate a new mblk to hold the mac layer info.  As described
	 * above, IP packets are transmitted by ovewriting the clprivnet
	 * header by the physical medium header, but ARP (rather non IP)
	 * transmissions require the clprivnet pseudo header to be
	 * encapsulated by a physical medium header. We allocate enough
	 * space to accommodate all MAC headers.
	 */
	if (dlap->dl_sap == ETHERTYPE_IP) {
		hdr_size = MAXIMUM(MAX_PHYSICAL_HDR_LEN, CLPRIVNET_HDR_LEN);
	} else {
		hdr_size = MAX_PHYSICAL_HDR_LEN + CLPRIVNET_HDR_LEN;
	}
	if ((nmp = allocb(hdr_size, BPRI_MED))
	    == NULL) {
		miocnak(wq, mp, 0, ENOMEM);
		return;
	}
	nmp->b_band = (*dlpri_to_bband)(dludp->dl_priority.dl_max);
	nmp->b_wptr += hdr_size;
	nmp->b_rptr = nmp->b_wptr - CLPRIVNET_HDR_LEN;

	/*
	 * Store pseudo header info. Physical medium header info is
	 * inserted before transmission on the wire.
	 */
	headerp = (struct clprivnet_hdr *)nmp->b_rptr;
	bcopy(&dlap->dl_addr, &headerp->p_dest,
	    sizeof (struct clprivnet_addr));
	bcopy(&clprivnet_p->clpn_ouraddr, &headerp->p_src,
	    sizeof (struct clprivnet_addr));
	headerp->p_type = htons(dlap->dl_sap);

	/*
	 * Link new mblk in after the "request" mblks.
	 */
	linkb(mp, nmp);

	/*
	 * set CLPRIVNET_FAST for this stream and return positive
	 * acknowledgment
	 */
	clprivnet_str_p->cs_flags |= CLPRIVNET_FAST;
	miocack(wq, mp, (int)msgsize(mp->b_cont), 0);
}

/*
 * Extracts destination nodeid and the ethertype from an mblk
 * holding clprivnet pseudo header. The ethertype is returned in
 * host order.
 */
static void
clprivnet_extract_header_info(mblk_t *bp, int *rndidp, ushort_t *ho_typep)
{
	struct clprivnet_hdr *headerp = (struct clprivnet_hdr *)bp->b_rptr;
	*rndidp = (int)(headerp->p_dest.low_order_byte);
	*ho_typep = ntohs(headerp->p_type);
}

/*
 * Calls into fpconf to transmit the packet on physical medium.
 * The mblk is in M_DATA format and already has the clprivnet pseudo
 * hdr stuffed in the front. If the type is ETHERTYPE_IP, the clprivnet
 * hdr will be overwritten by the physical medium header. Otherwise
 * the clprivnet header will be encapsulated within the physical
 * medium header.
 */
static void
clprivnet_redirect_packet(queue_t *wq, mblk_t *mp)
{
	int		rndid;
	ushort_t	ho_type;

	clprivnet_extract_header_info(mp, &rndid, &ho_type);

	/* Packets other than IP need to be encapsulated. */
	if (ho_type != ETHERTYPE_IP) {
		/*
		 * Clprivnet header has already been stuffed in. Leave it
		 * intact and change the ethertype to be passed to the upper
		 * layer to ETHERTYPE_CLPRIVNET so that the physical medium
		 * header that gets prepended carries the ETHERTYPE_CLPRIVNET
		 * as its packet type. The real packet type is in the
		 * encapsulated clprivnet header.
		 */
		ho_type = ETHERTYPE_CLPRIVNET;
	} else {
		/*
		 * No encapsulation needed, remove the clprivnet
		 * pseudo header.
		 */
		mp->b_rptr += sizeof (struct clprivnet_hdr);
	}

	(*pkt_redirect)(rndid, ho_type, wq, mp);
}

static int
clprivnet_wput(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str	*clprivnet_str_p;
	struct clprivnet	*clprivnet_p;

	CLPRIVNET_DBPRINTF((STREAMS, " clprivnet_wput: \n"));

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;

	switch (DB_TYPE(mp)) {
		case M_DATA:
			clprivnet_p = clprivnet_str_p->cs_clprivnetp;

			if (((clprivnet_str_p->cs_flags &
			    (CLPRIVNET_FAST|CLPRIVNET_RAW)) == 0) ||
			    (clprivnet_str_p->cs_state != DL_IDLE) ||
			    (clprivnet_p == NULL)) {
				merror(wq, mp, EPROTO);
				break;
			}
			/*
			 * If any msgs already enqueued or promiscuous mode
			 * is on, then enqueue the msg.  Otherwise pass it
			 * on now for transmission.
			 */
			if (wq->q_first || clprivnet_p->clpn_promisc_phys_cnt
			    != 0) {
				(void) putq(wq, mp);
				qenable(wq);
			} else {
				/* Pass the packet on for transmission */
				clprivnet_redirect_packet(wq, mp);
			}

			break;

		case M_PROTO:
		case M_PCPROTO:
			(void) putq(wq, mp);
			qenable(wq);
			break;

		case M_IOCTL:
			clprivnet_ioctl(wq, mp);
			break;

		case M_FLUSH:
			if (*mp->b_rptr & FLUSHW) {
				flushq(wq, FLUSHALL);
				*mp->b_rptr &= ~FLUSHW;
			}
			if (*mp->b_rptr & FLUSHR)
				qreply(wq, mp);
			else
				freemsg(mp);
			break;

		default:
			freemsg(mp);
			break;
	}
	return (0);
}

/*
 * Enqueue M_PROTO/M_PCPROTO (always) and M_DATA (sometimes) on the wq.
 *
 * M_DATA messages are enqueued on the wq *only* when the send side
 * is out of buffer space.  Once the send side resource is available again,
 * wsrv() is enabled and tries to send all the messages on the wq.
 */
static int
clprivnet_wsrv(queue_t *wq)
{
	struct clprivnet_str	*clprivnet_str_p;
	struct clprivnet	*clprivnet_p;
	mblk_t		*mp, *nmp;

	CLPRIVNET_DBPRINTF((STREAMS, " clprivnet_wsrv: \n"));

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	clprivnet_p = clprivnet_str_p->cs_clprivnetp;

	/*lint -e720 */
	while (mp = getq(wq))
		switch (DB_TYPE(mp)) {
			case	M_DATA:
				if (clprivnet_p) {
					/* if promiscuous, send copy up */
					if (clprivnet_p->clpn_promisc_phys_cnt)
						if ((nmp = copymsg(mp)) != NULL)
							clprivnet_sendup(
							    clprivnet_p,
							    nmp,
							    clprivnet_paccept);
					/* Pass packet on for transmission */
					clprivnet_redirect_packet(wq, mp);
				} else
					freemsg(mp);
				break;

			case	M_PROTO:
			case	M_PCPROTO:
				clprivnet_proto(wq, mp);
				break;
			case	M_PCSIG:
				(*fragment_timeout)(wq, mp);
				break;

			default: /* nothing is working at ths point */
				freemsg(mp);
				/* ASSERT(0); */
				break;
		}

	return (0);
}



static void
clprivnet_proto(queue_t *wq, mblk_t *mp)
{
	union DL_primitives	*dlp;
	struct clprivnet_str		*clprivnet_str_p;
	uint32_t		prim;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_proto: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	dlp = (union DL_primitives *)mp->b_rptr;
	prim = dlp->dl_primitive;

	mutex_enter(&clprivnet_str_p->cs_lock);

	switch (prim) {
		case	DL_UNITDATA_REQ:
			clprivnet_udreq(wq, mp);
			break;

		case	DL_ATTACH_REQ:
			clprivnet_areq(wq, mp);
			break;

		case	DL_DETACH_REQ:
			clprivnet_dreq(wq, mp);
			break;

		case	DL_BIND_REQ:
			clprivnet_breq(wq, mp);
			break;

		case	DL_UNBIND_REQ:
			clprivnet_ubreq(wq, mp);
			break;

		case	DL_INFO_REQ:
			clprivnet_ireq(wq, mp);
			break;

		case	DL_PROMISCON_REQ:
			clprivnet_ponreq(wq, mp);
			break;

		case	DL_PROMISCOFF_REQ:
			clprivnet_poffreq(wq, mp);
			break;

		case	DL_ENABMULTI_REQ:
			/* not supported */
			errorack(wq, mp, DL_ENABMULTI_REQ,
			    DL_NOTSUPPORTED, 0);
			break;

		case	DL_DISABMULTI_REQ:
			/* not supported */
			errorack(wq, mp, DL_DISABMULTI_REQ,
			    DL_NOTSUPPORTED, 0);
			break;

		case	DL_PHYS_ADDR_REQ:
			clprivnet_pareq(wq, mp);
			break;

		case	DL_SET_PHYS_ADDR_REQ:
			/* not supported */
			errorack(wq, mp, DL_SET_PHYS_ADDR_REQ,
				DL_NOTSUPPORTED, 0);
			break;

		default:
			errorack(wq, mp, prim, DL_UNSUPPORTED, 0);
			break;
	}

	mutex_exit(&clprivnet_str_p->cs_lock);
}





/*
 *	Generic DLPI interface routines
 *
 *
 *
 * DLPI Attach Request
 *
 * Request for an association of a physical point of attachment (ppa) with
 * a stream.  For the stream defined by wq, the stream private data
 * structure clprivnet_str is linked to the clprivnet device structure
 * for the ppa specified in the mblk.  For this driver the ppa will always
 * pertain to the device instance for clprivnet0.
 */
static void
clprivnet_areq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str		*clprivnet_str_p;
	union DL_primitives		*dlp;
	struct clprivnet		*clprivnet_p;
	uint_t				ppa;
	uint32_t promisc_phys_cnt = 	0;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_areq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	dlp = (union DL_primitives *)mp->b_rptr;

	if (MBLKL(mp) < (long)DL_ATTACH_REQ_SIZE) {
		errorack(wq, mp, DL_ATTACH_REQ, DL_BADPRIM, 0);
		return;
	}

	if (clprivnet_str_p->cs_state != DL_UNATTACHED) {
		errorack(wq, mp, DL_ATTACH_REQ, DL_OUTSTATE, 0);
		return;
	}

	if (clprivnet_str_p->cs_flags & CLPRIVNET_ALLPHYS)
		promisc_phys_cnt++;

	ppa = dlp->attach_req.dl_ppa;

	/*
	 * Valid ppa?  There will only be one device structure so
	 * no locking is done.
	 */
	for (clprivnet_p = clprivnet_listp; clprivnet_p; clprivnet_p =
	    clprivnet_p->clpn_nextp) {
		if (ppa == (uint_t)ddi_get_instance(clprivnet_p->clpn_dip)) {
			clprivnet_p->clpn_promisc_phys_cnt += promisc_phys_cnt;
			break;
		}
	}
	if (clprivnet_p == NULL) {
		errorack(wq, mp, dlp->dl_primitive, DL_BADPPA, 0);
		return;
	}


	/*
	 * Set link to device and update our state.
	 */
	clprivnet_str_p->cs_clprivnetp = clprivnet_p;
	clprivnet_str_p->cs_state = DL_UNBOUND;

	okack(wq, mp, DL_ATTACH_REQ);
}

/*
 * DLPI Detach Request
 */
static void
clprivnet_dreq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str	*clprivnet_str_p;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_dreq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	if (MBLKL(mp) < (long)DL_DETACH_REQ_SIZE) {
		errorack(wq, mp, DL_DETACH_REQ, DL_BADPRIM, 0);
		return;
	}

	if (clprivnet_str_p->cs_state != DL_UNBOUND) {
		errorack(wq, mp, DL_DETACH_REQ, DL_OUTSTATE, 0);
		return;
	}

	clprivnet_dodetach(clprivnet_str_p);
	okack(wq, mp, DL_DETACH_REQ);
}

/*
 * Detach a Stream from an interface.
 */
static void
clprivnet_dodetach(struct clprivnet_str *clprivnet_str_p)
{
	struct clprivnet_str	*t_clprivnet_str_p;
	struct clprivnet	*clprivnet_p;
	uint32_t promisc_phys_cnt = 0;

	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_dodetach: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	ASSERT(clprivnet_str_p->cs_clprivnetp);

	clprivnet_p = clprivnet_str_p->cs_clprivnetp;
	clprivnet_str_p->cs_clprivnetp = NULL;

	/* Disable promiscuous mode if on. */
	if (clprivnet_str_p->cs_flags & CLPRIVNET_ALLPHYS) {
		clprivnet_str_p->cs_flags &= ~CLPRIVNET_ALLPHYS;
		promisc_phys_cnt++;
	}

	/* Disable ALLSAP mode if on. */
	if (clprivnet_str_p->cs_flags & CLPRIVNET_ALLSAP) {
		clprivnet_str_p->cs_flags &= ~CLPRIVNET_ALLSAP;
	}


	/*
	 * Detach from device structure and change state
	 */
	rw_enter(&clprivnet_str_list_lock, RW_READER);
	for (t_clprivnet_str_p = clprivnet_str_listp; t_clprivnet_str_p;
	    t_clprivnet_str_p = t_clprivnet_str_p->cs_nextp)
		if (t_clprivnet_str_p->cs_clprivnetp == clprivnet_p)
			break;
	rw_exit(&clprivnet_str_list_lock);

	clprivnet_p->clpn_promisc_phys_cnt -= promisc_phys_cnt;

	clprivnet_str_p->cs_state = DL_UNATTACHED;
}


/*
 * DLPI Bind Request
 */
static void
clprivnet_breq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str		*clprivnet_str_p;
	union DL_primitives	*dlp;
	struct clprivnet		*clprivnet_p;
	struct clprivnet_dladdr	clprivnet_dladdr;
	ushort_t		sap;
	uint32_t		xidtest;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_breq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	if (MBLKL(mp) < (long)DL_BIND_REQ_SIZE) {
		errorack(wq, mp, DL_BIND_REQ, DL_BADPRIM, 0);
		return;
	}

	if (clprivnet_str_p->cs_state != DL_UNBOUND) {
		errorack(wq, mp, DL_BIND_REQ, DL_OUTSTATE, 0);
		return;
	}

	dlp = (union DL_primitives *)mp->b_rptr;
	clprivnet_p = clprivnet_str_p->cs_clprivnetp;
	sap = (ushort_t)dlp->bind_req.dl_sap;
	xidtest = dlp->bind_req.dl_xidtest_flg;

	ASSERT(clprivnet_p);

	if (xidtest) {
		errorack(wq, mp, DL_BIND_REQ, DL_NOAUTO, 0);
		return;
	}

#if SOL_VERSION >= __s11
	if (sap == 0 || sap > CLPRIVNETTYPE_MAX) {
		/*
		 * softmac probes us with sap == 0 and if
		 * it succeeds, it will own clprivnet and
		 * we cannot access it from our kernel modules.
		 */
#else
	if (sap > CLPRIVNETTYPE_MAX) {
#endif
		errorack(wq, mp, dlp->dl_primitive, DL_BADSAP, 0);
		return;
	}

	/*
	 * Save SAP value for this Stream and change state.
	 */
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_breq: sap = %x\n", sap));
	clprivnet_str_p->cs_sap = sap;
	clprivnet_str_p->cs_state = DL_IDLE;

	clprivnet_dladdr.dl_sap = sap;
	bcopy((caddr_t)&clprivnet_p->clpn_ouraddr,
	    (caddr_t)&clprivnet_dladdr.dl_addr, sizeof (struct clprivnet_addr));
	bindack(wq, mp, sap, (caddr_t)&clprivnet_dladdr,
	    (unsigned)CLPRIVNET_ADDRL, 0, 0);
}


static void
clprivnet_ubreq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str	*clprivnet_str_p;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_ubreq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	if (MBLKL(mp) < (long)DL_UNBIND_REQ_SIZE) {
		errorack(wq, mp, DL_UNBIND_REQ, DL_BADPRIM, 0);
		return;
	}

	if (clprivnet_str_p->cs_state != DL_IDLE) {
		errorack(wq, mp, DL_UNBIND_REQ, DL_OUTSTATE, 0);
		return;
	}

	clprivnet_str_p->cs_state = DL_UNBOUND;

	(void) putnextctl1(RD(wq), M_FLUSH, FLUSHRW);

	okack(wq, mp, DL_UNBIND_REQ);
}

static void
clprivnet_pareq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str	*clprivnet_str_p;
	union DL_primitives	*dlp;
	uint32_t		type;
	struct clprivnet	*clprivnet_p;
	struct clprivnet_addr	addr;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_pareq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	if (MBLKL(mp) < (long)DL_PHYS_ADDR_REQ_SIZE) {
		errorack(wq, mp, DL_PHYS_ADDR_REQ, DL_BADPRIM, 0);
		return;
	}

	dlp = (union DL_primitives *)mp->b_rptr;
	type = dlp->physaddr_req.dl_addr_type;
	clprivnet_p = clprivnet_str_p->cs_clprivnetp;

	if (clprivnet_p == NULL) {
		errorack(wq, mp, DL_PHYS_ADDR_REQ, DL_OUTSTATE, 0);
		return;
	}

	/*
	 * Since changing the medium address is not supported now, both types
	 * return the same.
	 */
	switch (type) {
		case	DL_FACT_PHYS_ADDR:
			localmedaddr(&addr);
			break;

		case	DL_CURR_PHYS_ADDR:
			bcopy((caddr_t)&clprivnet_p->clpn_ouraddr,
			    (caddr_t)&addr, sizeof (struct clprivnet_addr));
			break;

		default:
			errorack(wq, mp, DL_PHYS_ADDR_REQ,
				DL_NOTSUPPORTED, 0);
			return;
	}

	physaddrack(wq, mp, (caddr_t)&addr, ETHERADDRL);
}

static void
clprivnet_ireq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str		*clprivnet_str_p;
	dl_info_ack_t		*dlip;
	struct clprivnet_dladdr	*dlap;
	struct clprivnet_addr	*ep;
	size_t			size;
	struct clprivnet_addr macaddr;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_ireq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	if (MBLKL(mp) < (long)DL_INFO_REQ_SIZE) {
		errorack(wq, mp, DL_INFO_REQ, DL_BADPRIM, 0);
		return;
	}

	/*
	 * Exchange current msg for a DL_INFO_ACK.
	 */
	size = sizeof (dl_info_ack_t) + CLPRIVNET_ADDRL + ETHERADDRL;
	if ((mp = mexchange(wq, mp, size, M_PCPROTO, DL_INFO_ACK)) == NULL)
		return;

	/*
	 * Fill in the DL_INFO_ACK fields and reply.
	 */
	dlip = (dl_info_ack_t *)mp->b_rptr;
	*dlip = clprivnet_infoack;
	dlip->dl_max_sdu = clprivnet_mtu;
	dlip->dl_current_state = clprivnet_str_p->cs_state;
	dlap = (struct clprivnet_dladdr *)(mp->b_rptr + dlip->dl_addr_offset);
	dlap->dl_sap = clprivnet_str_p->cs_sap;
	/*
	 * If the bound SAP is not ETHERYPE_IP, subtract the sizeof
	 * the clprivent pseudo header from the MTU as non-IP packets
	 * carry a clprivnet pseudo header as well.
	 */
	if (dlap->dl_sap != ETHERTYPE_IP) {
		dlip->dl_max_sdu -= CLPRIVNET_HDR_LEN;
	}
	if (clprivnet_str_p->cs_clprivnetp) {
		bcopy((caddr_t)&clprivnet_str_p->cs_clprivnetp->clpn_ouraddr,
		    (caddr_t)&dlap->dl_addr, sizeof (struct clprivnet_addr));
	} else {
		macaddr.low_order_byte =
		    (uchar_t)clprivnet_local_nodeid;
		bcopy(&macaddr, (caddr_t)&dlap->dl_addr,
		    sizeof (struct clprivnet_addr));

	}
	ep = (struct clprivnet_addr *)
	    (mp->b_rptr + dlip->dl_brdcst_addr_offset);
	bcopy((caddr_t)&medbroadcastaddr, (caddr_t)ep,
		sizeof (struct clprivnet_addr));

	qreply(wq, mp);
}

static void
clprivnet_ponreq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str		*clprivnet_str_p;
	struct clprivnet *clprivnet_p;
	uint32_t promisc_phys_cnt = 0;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	clprivnet_p = clprivnet_str_p->cs_clprivnetp;

	if (MBLKL(mp) < (long)DL_PROMISCON_REQ_SIZE) {
		errorack(wq, mp, DL_PROMISCON_REQ, DL_BADPRIM, 0);
		return;
	}

	switch (((dl_promiscon_req_t *)mp->b_rptr)->dl_level) {
	case DL_PROMISC_PHYS:
		clprivnet_str_p->cs_flags |= CLPRIVNET_ALLPHYS;
		promisc_phys_cnt++;
		break;

	case DL_PROMISC_SAP:
		clprivnet_str_p->cs_flags |= CLPRIVNET_ALLSAP;
		break;

	default:
		errorack(wq, mp, DL_PROMISCON_REQ,
					DL_NOTSUPPORTED, 0);
		return;
	}

	if (clprivnet_p)
		clprivnet_p->clpn_promisc_phys_cnt += promisc_phys_cnt;
	okack(wq, mp, DL_PROMISCON_REQ);
}

static void
clprivnet_poffreq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str		*clprivnet_str_p;
	struct clprivnet *clprivnet_p;
	uint32_t	flag;
	uint32_t promisc_phys_cnt = 0;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	clprivnet_p = clprivnet_str_p->cs_clprivnetp;

	if (MBLKL(mp) < (long)DL_PROMISCOFF_REQ_SIZE) {
		errorack(wq, mp, DL_PROMISCOFF_REQ, DL_BADPRIM, 0);
		return;
	}

	switch (((dl_promiscoff_req_t *)mp->b_rptr)->dl_level) {
	case DL_PROMISC_PHYS:
		flag = CLPRIVNET_ALLPHYS;
		promisc_phys_cnt++;
		break;

	case DL_PROMISC_SAP:
		flag = CLPRIVNET_ALLSAP;
		break;

	default:
		errorack(wq, mp, DL_PROMISCOFF_REQ,
					DL_NOTSUPPORTED, 0);
		return;
	}

	if ((clprivnet_str_p->cs_flags & flag) == 0) {
		errorack(wq, mp, DL_PROMISCOFF_REQ, DL_NOTENAB, 0);
		return;
	}

	clprivnet_str_p->cs_flags &= ~flag;

	if (clprivnet_p)
		clprivnet_p->clpn_promisc_phys_cnt -= promisc_phys_cnt;

	okack(wq, mp, DL_PROMISCOFF_REQ);
}

/*
 * The cluster local nodeid is used for the mac address
 */

static void
localmedaddr(struct clprivnet_addr *addr)
{
	/* initialize to zero */
	struct clprivnet_addr macaddr;

	bzero(&macaddr, sizeof (struct clprivnet_addr));

	macaddr.low_order_byte = (uchar_t)clprivnet_local_nodeid;
	bcopy(&macaddr, addr, sizeof (struct clprivnet_addr));
}


static void
clprivnet_udreq(queue_t *wq, mblk_t *mp)
{
	struct clprivnet_str			*clprivnet_str_p;
	register struct clprivnet		*clprivnet_p;
	register dl_unitdata_req_t	*dludp;
	mblk_t				*nmp;
	struct clprivnet_dladdr		*dlap;
	struct clprivnet_hdr 		*headerp;
	long				off, len;
	size_t				hdr_size;
	mblk_t				*mp_tofree = NULL;

	clprivnet_str_p = (struct clprivnet_str *)wq->q_ptr;
	CLPRIVNET_DBPRINTF((PROTO, " clprivnet_udreq: "));
	CLPRIVNET_DBPRINTF((PROTO, " str_data_ptr = %x\n", clprivnet_str_p));

	clprivnet_p = clprivnet_str_p->cs_clprivnetp;

	dludp = (dl_unitdata_req_t *)mp->b_rptr;


	/*
	 * Validate destination address format.
	 */
	off = (long)dludp->dl_dest_addr_offset;
	len = (long)dludp->dl_dest_addr_length;
	if (!MBLKIN(mp, off, len) || (len != CLPRIVNET_ADDRL)) {
		uderrorind(wq, mp, (uchar_t *)(mp->b_rptr + off), (uint_t)len,
			DL_BADADDR, 0);
		return;
	}

	/*
	 * Error if no M_DATA follows.
	 */
	nmp = mp->b_cont;
	if (nmp == NULL) {
		uderrorind(wq, mp, (uchar_t *)(mp->b_rptr + off), (uint_t)len,
			DL_BADDATA, 0);
		return;
	}

	/* get address pointer */
	dlap = (struct clprivnet_dladdr *)(mp->b_rptr + off);

	/*
	 * Since ARP sends a SAP of 0 with the unitdata request
	 * we should use the SAP that we had bound to earlier.
	 * This will work since stream modules generally talk
	 * and listen to the same SAP. Please see bugid 4867139.
	 * This has been fixed in S10_32 so we can remove this code
	 * when we reference our gate to S10.
	 */
	if (dlap->dl_sap == 0)
		dlap->dl_sap = clprivnet_str_p->cs_sap;

	/*
	 * Compute haeader size needed to hold the mac layer info.  As
	 * described above, IP packets are transmitted by ovewriting the
	 * clprivnet header by the physical medium header, but ARP (rather
	 * non IP) transmissions require the clprivnet pseudo header to be
	 * encapsulated by a physical medium header. We allocate enough
	 * space to accommodate all MAC headers.
	 */
	if (dlap->dl_sap == ETHERTYPE_IP) {
		hdr_size = MAXIMUM(MAX_PHYSICAL_HDR_LEN, CLPRIVNET_HDR_LEN);
	} else {
		hdr_size = MAX_PHYSICAL_HDR_LEN + CLPRIVNET_HDR_LEN;
	}

	/*
	 * Create  header by either prepending it into the next (M_DATA)
	 * mblk (nmp) if possible, or reusing the M_PROTO block if not.
	 */
	if ((DB_REF(nmp) == 1) &&
		((size_t)MBLKHEAD(nmp) >= hdr_size) &&
		(((size_t)nmp->b_rptr & 0x1) == 0)) {
		/*
		 * Space available for ethernet header in M_DATA payload
		 */
		mp_tofree = mp;	/* Don't free yet, dlap points into this mp */
		mp = nmp;
		mp->b_rptr -= CLPRIVNET_HDR_LEN;
	} else if ((DB_REF(mp) == 1) && (size_t)MBLKSIZE(mp) >= hdr_size) {
		/*
		 * Space available in the request M_PROTO itself. Reuse it.
		 */
		DB_TYPE(mp) = M_DATA;
		mp->b_rptr = DB_BASE(mp);
		mp->b_wptr = mp->b_rptr + CLPRIVNET_HDR_LEN;
	} else {
		/*
		 * Space not available in the request M_PROTO and M_DATA blk.
		 * Allocate space.
		 */
		mp_tofree = mp;	/* Don't free yet, dlap points into this mp */
		mp = allocb(hdr_size, BPRI_MED);
		if (mp == NULL) {
			freemsg(nmp);
			return;
		}
		mp->b_wptr = mp->b_rptr + CLPRIVNET_HDR_LEN;
		mp->b_cont = nmp;
	}
	headerp = (struct clprivnet_hdr *)mp->b_rptr;
	bcopy((caddr_t)&dlap->dl_addr,
	    (caddr_t)&headerp->p_dest,
	    sizeof (struct clprivnet_addr));
	bcopy((caddr_t)&clprivnet_p->clpn_ouraddr,
		(caddr_t)&headerp->p_src,
		sizeof (struct clprivnet_addr));
	headerp->p_type = htons(dlap->dl_sap);
	mp->b_band = (*dlpri_to_bband)(dludp->dl_priority.dl_max);
	if (mp_tofree != NULL)
		freeb(mp_tofree);

	/*
	 * If promiscuous mode is on then send a copy of the packet upstream
	 */
	if (clprivnet_p->clpn_promisc_phys_cnt)
		if ((nmp = copymsg(mp)) != NULL)
			clprivnet_sendup(clprivnet_p, nmp, clprivnet_paccept);

	/*
	 * Pass packet on for transmission.
	 */
	clprivnet_redirect_packet(wq, mp);
}


#ifdef DEBUG
static void
clprivnet_dbprintf(clprivnet_dbg_msg_t type, char *fmt, ...)
{
	char msg_buffer[512];
	va_list ap;

	if (type < 0 || type >= max_dbg_msgs ||
		!clprivnet_debug_enabled[type])
		return;

	rw_enter(&clprivnet_str_list_lock, RW_WRITER);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */
	(void) vsprintf(msg_buffer, fmt, ap);
	cmn_err(CE_CONT, "clprivnet: %s", msg_buffer);
	va_end(ap);

	rw_exit(&clprivnet_str_list_lock);
}

#endif

/*
 * FPconf advises clprivnet about what mtu clprivnet should be using.
 */
void
clprivnet_set_mtu(t_uscalar_t new_mtu)
{
	CLPRIVNET_DBPRINTF((REDIRECT, "clprivnet_set_mtu %d -> %d\n",
	    clprivnet_mtu, new_mtu));
	clprivnet_mtu = new_mtu;
}

/*
 * The pkt_redirector object (fpconf.cc) registers a pointer
 * here  for the packet redirection function.
 */
void
clprivnet_set_pkt_redirect(void (*redirect_func)(uint_t, ushort_t, queue_t *,
    mblk_t *))
{
	CLPRIVNET_DBPRINTF((REDIRECT, "clprivnet_set_pkt: \n"));
	pkt_redirect = redirect_func;
}

/*
 * FPconf tells clprivnet about how to convert a IEEE user priority value into
 * mblk b_band.
 */
void
clprivnet_set_dlpri_to_bband(char (*conversion_func)(uint_t))
{
	dlpri_to_bband = conversion_func;
}

/*
 * The pkt_redirector object (fpconf.cc) registers a pointer
 * here  for the fragment timeout function.
 */
void
clprivnet_set_fragment_timeout(void (*timeout_func)())
{
	CLPRIVNET_DBPRINTF((REDIRECT, "clprivnet_set_timeout: \n"));
	fragment_timeout = timeout_func;
}

/*
 * The pkt_redirector object constructor provides the local nodeid
 * through this function.  This will be used for the local mac address.
 */
void
clprivnet_set_local_nodeid(uint_t nodeid)
{
	struct clprivnet	*clprivnet_p = clprivnet_listp;
	struct clprivnet_addr macaddr;

	bzero(&macaddr, sizeof (struct clprivnet_addr));

	CLPRIVNET_DBPRINTF((REDIRECT, "clprivnet_set_local_nodeid: "));
	CLPRIVNET_DBPRINTF((REDIRECT, "nodeid = %x\n", nodeid));
	clprivnet_local_nodeid = nodeid;

	if (clprivnet_p) {
		macaddr.low_order_byte =
		    (uchar_t)clprivnet_local_nodeid;
		bcopy(&macaddr, &(clprivnet_p->clpn_ouraddr),
		    sizeof (struct clprivnet_addr));
	}
}



static mblk_t *
addudind(mblk_t *mp, struct clprivnet_addr *srcp,
	struct clprivnet_addr *destp, int type)
{
	dl_unitdata_ind_t *dludindp;
	struct clprivnet_dladdr *dlap;
	mblk_t *nmp;
	ulong_t size;

	mp->b_rptr += sizeof (struct clprivnet_hdr);


	/*
	 * Allocate an M_PROTO mblk for the DL_UNITDATA_IND.
	 */

	size = sizeof (dl_unitdata_ind_t) + 2 * CLPRIVNET_ADDRL;
	if ((nmp = allocb(size, BPRI_LO)) == NULL) {
		freemsg(mp);
		return (NULL);
	}

	DB_TYPE(nmp) = M_PROTO;
	nmp->b_wptr = nmp->b_datap->db_lim;
	nmp->b_rptr = nmp->b_wptr - size;

	/*
	 * Construct a DL_UNITDATA_IND primitive.
	 */
	dludindp = (dl_unitdata_ind_t *)nmp->b_rptr;
	dludindp->dl_primitive = DL_UNITDATA_IND;
	dludindp->dl_dest_addr_length = CLPRIVNET_ADDRL;
	dludindp->dl_dest_addr_offset = sizeof (dl_unitdata_ind_t);
	dludindp->dl_src_addr_length = CLPRIVNET_ADDRL;
	dludindp->dl_src_addr_offset = sizeof (dl_unitdata_ind_t) +
	    CLPRIVNET_ADDRL;
	dludindp->dl_group_address = 0;

	dlap = (struct clprivnet_dladdr *)(nmp->b_rptr +
	    sizeof (dl_unitdata_ind_t));
	bcopy(destp, &dlap->dl_addr, sizeof (struct clprivnet_addr));
	dlap->dl_sap = (uint16_t)type;

	dlap = (struct clprivnet_dladdr *)(nmp->b_rptr +
	    sizeof (dl_unitdata_ind_t) + CLPRIVNET_ADDRL);
	bcopy(srcp, &dlap->dl_addr, sizeof (struct clprivnet_addr));
	dlap->dl_sap = (uint16_t)type;

	/*
	 * Link the M_PROTO and M_DATA together.
	 */
	nmp->b_cont = mp;
	return (nmp);
}

/*
 * Send packet upstream.
 */
static void
clprivnet_sendup(struct clprivnet *clprivnet_p, mblk_t *mp,
    struct clprivnet_str *(*acceptfunc)(struct clprivnet_str *,
    struct clprivnet *, ushort_t, struct clprivnet_addr *))
{
	struct clprivnet_hdr	*clprivnet_hdr;
	struct clprivnet_str	*str_p;
	struct clprivnet_str	*nstr_p;
	ushort_t		type;
	struct clprivnet_addr		src;
	struct clprivnet_addr		dest;
	mblk_t	*nmp;

	/*
	 * Assume mp->b_rptr points to clprivnet pseudo header.
	 */
	clprivnet_hdr = (struct clprivnet_hdr *)mp->b_rptr;
	dest = clprivnet_hdr->p_dest;
	src = clprivnet_hdr->p_src;
	type = ntohs(clprivnet_hdr->p_type);	/* host order */

	/*
	 * While holding a reader lock on the linked list of streams structures,
	 * attempt to match the address criteria for each stream
	 * and pass up the raw M_DATA ("fastpath") or a DL_UNITDATA_IND.
	 */

	rw_enter(&clprivnet_str_list_lock, RW_READER);

	/*lint -e746 */
	if ((str_p = (*acceptfunc)(clprivnet_str_listp, clprivnet_p,
	    type, &dest)) == NULL) {
		rw_exit(&clprivnet_str_list_lock);
		freemsg(mp);
		return;
	}
	/*
	 * Loop on matching open streams until (*acceptfunc)() returns NULL.
	 */
	/*lint -e746 */
	for (; nstr_p = (*acceptfunc)(str_p->cs_nextp, clprivnet_p, type,
	    &dest);
		str_p = nstr_p) {
		if (canputnext(str_p->cs_rq)) {
			if (nmp = dupmsg(mp)) {
				if (str_p->cs_flags & CLPRIVNET_FAST) {
					nmp->b_rptr +=
						sizeof (struct clprivnet_hdr);
					putnext(str_p->cs_rq, nmp);
				} else if (str_p->cs_flags & CLPRIVNET_RAW)
					putnext(str_p->cs_rq, nmp);
				else if ((nmp = addudind(nmp,
					&src, &dest, type))) {
						putnext(str_p->cs_rq, nmp);
				}
			}
		}
	}


	/*
	 * Do the last  one.
	 */
	if (canputnext(str_p->cs_rq)) {
		if (str_p->cs_flags & CLPRIVNET_FAST) {
			mp->b_rptr += sizeof (struct clprivnet_hdr);
			putnext(str_p->cs_rq, mp);
		} else if (str_p->cs_flags & CLPRIVNET_RAW)
			putnext(str_p->cs_rq, mp);
		else if ((mp = addudind(mp, &src, &dest, type))) {
			putnext(str_p->cs_rq, mp);
		}
	} else {
		freemsg(mp);
	}

	rw_exit(&clprivnet_str_list_lock);
}

/*
 * Test upstream destination sap and address match.
 */
static struct clprivnet_str *
clprivnet_accept(struct clprivnet_str *str_p, struct clprivnet *clprivnet_p,
    ushort_t type, struct clprivnet_addr *addrp)
{
	ushort_t sap;
	uint32_t flags;

	for (; str_p; str_p = str_p->cs_nextp) {
		sap = str_p->cs_sap;
		flags = str_p->cs_flags;
		if ((str_p->cs_clprivnetp == clprivnet_p) &&
		    CLPRIVNET_SAPMATCH(sap, type, flags)) {
			if ((!clprivnet_cmp(addrp,
			    &clprivnet_p->clpn_ouraddr)) ||
			    (!clprivnet_cmp(addrp, &medbroadcastaddr)) ||
			    (flags & CLPRIVNET_ALLPHYS)) {
				return (str_p);
			}
		}
	}
	return (NULL);
}

/*
 * Test upstream destination sap and address match for CLPRIVNET_ALLPHYS only.
 */
static struct clprivnet_str *
clprivnet_paccept(struct  clprivnet_str *str_p, struct clprivnet *clprivnet_p,
    ushort_t type, struct clprivnet_addr *addrp)
{
	ushort_t sap;
	uint32_t flags;
	addrp = addrp;

	for (; str_p; str_p = str_p->cs_nextp) {
		sap = str_p->cs_sap;
		flags = str_p->cs_flags;

		if ((str_p->cs_clprivnetp == clprivnet_p) &&
			CLPRIVNET_SAPMATCH(sap, type, flags) &&
			(flags & CLPRIVNET_ALLPHYS))
			return (str_p);
	}
	return (NULL);
}

/*
 * ARP mblks are delivered here from the fpconf fast_path Stream (set up
 * with a private SAP).  This is so that ARP packets will be seen as
 * arriving through the clprivnet interface. IP packets are delivered
 * directly to IP by the physical interfaces through the 0x800 SAP.
 */
void
clprivnet_pkt_recv(mblk_t *mp)
{

	CLPRIVNET_DBPRINTF((REDIRECT, "clprivnet_pkt_recv: \n"));
	switch (DB_TYPE(mp)) {
		case M_DATA:
			break;
		case M_PROTO:
			if (((union DL_primitives *)mp->b_rptr)->dl_primitive
			    == DL_UNITDATA_IND) {
				/* Trim off the leading M_PROTO mblk */
				mblk_t  *protomp = mp;
				mp = mp->b_cont;
				ASSERT(mp && DB_TYPE(mp) == M_DATA);
				freeb(protomp);
			}
			break;
		default:
			freeb(mp);
			return;
	}

	clprivnet_sendup(clprivnet_listp, mp, clprivnet_accept);

}

static void
bindack(
	queue_t		*wq,
	mblk_t		*mp,
	t_scalar_t	sap,
	void		*addrp,
	t_uscalar_t	addrlen,
	t_uscalar_t	maxconind,
	t_uscalar_t	xidtest)
{
	union DL_primitives	*dlp;
	size_t			size;

	size = sizeof (dl_bind_ack_t) + addrlen;
	if ((mp = mexchange(wq, mp, size, M_PCPROTO, DL_BIND_ACK)) == NULL)
		return;

	dlp = (union DL_primitives *)mp->b_rptr;
	dlp->bind_ack.dl_sap = sap;
	dlp->bind_ack.dl_addr_length = addrlen;
	dlp->bind_ack.dl_addr_offset = sizeof (dl_bind_ack_t);
	dlp->bind_ack.dl_max_conind = maxconind;
	dlp->bind_ack.dl_xidtest_flg = xidtest;
	bcopy(addrp, mp->b_rptr + sizeof (dl_bind_ack_t), (size_t)addrlen);

	qreply(wq, mp);
}

static void
okack(
	queue_t		*wq,
	mblk_t		*mp,
	t_uscalar_t	correct_primitive)
{
	union DL_primitives	*dlp;

	if ((mp = mexchange(wq, mp, sizeof (dl_ok_ack_t), M_PCPROTO,
	    DL_OK_ACK)) == NULL)
		return;
	dlp = (union DL_primitives *)mp->b_rptr;
	dlp->ok_ack.dl_correct_primitive = correct_primitive;
	qreply(wq, mp);
}

static void
errorack(
	queue_t		*wq,
	mblk_t		*mp,
	t_uscalar_t	error_primitive,
	t_uscalar_t	errno,
	t_uscalar_t	unix_errno)
{
	union DL_primitives	*dlp;

	if ((mp = mexchange(wq, mp, sizeof (dl_error_ack_t), M_PCPROTO,
	    DL_ERROR_ACK)) == NULL)
		return;
	dlp = (union DL_primitives *)mp->b_rptr;
	dlp->error_ack.dl_error_primitive = error_primitive;
	dlp->error_ack.dl_errno = errno;
	dlp->error_ack.dl_unix_errno = unix_errno;
	qreply(wq, mp);
}

static void
uderrorind(
	queue_t		*wq,
	mblk_t		*mp,
	void		*addrp,
	t_uscalar_t	addrlen,
	t_uscalar_t	errno,
	t_uscalar_t	unix_errno)
{
	union DL_primitives	*dlp;
	char			buffer[DLADDRL];
	size_t			size;

	if (addrlen > DLADDRL)
		addrlen = DLADDRL;

	bcopy(addrp, buffer, (size_t)addrlen);

	size = sizeof (dl_uderror_ind_t) + addrlen;

	if ((mp = mexchange(wq, mp, size, M_PCPROTO, DL_UDERROR_IND)) == NULL)
		return;

	dlp = (union DL_primitives *)mp->b_rptr;
	dlp->uderror_ind.dl_dest_addr_length = addrlen;
	dlp->uderror_ind.dl_dest_addr_offset = sizeof (dl_uderror_ind_t);
	dlp->uderror_ind.dl_unix_errno = unix_errno;
	dlp->uderror_ind.dl_errno = errno;
	bcopy((caddr_t)buffer,
	    (caddr_t)(mp->b_rptr + sizeof (dl_uderror_ind_t)),
	    (size_t)addrlen);
	qreply(wq, mp);
}

static void
physaddrack(
	queue_t		*wq,
	mblk_t		*mp,
	void		*addrp,
	t_uscalar_t	len)
{
	union DL_primitives	*dlp;
	size_t			size;

	size = sizeof (dl_phys_addr_ack_t) + len;
	if ((mp = mexchange(wq, mp, size, M_PCPROTO, DL_PHYS_ADDR_ACK)) == NULL)
		return;
	dlp = (union DL_primitives *)mp->b_rptr;
	dlp->physaddr_ack.dl_addr_length = len;
	dlp->physaddr_ack.dl_addr_offset = sizeof (dl_phys_addr_ack_t);
	bcopy(addrp, mp->b_rptr + sizeof (dl_phys_addr_ack_t), (size_t)len);
	qreply(wq, mp);
}
