/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CLPRIVNET_H
#define	_CLPRIVNET_H

#pragma ident	"@(#)clprivnet.h	1.10	08/05/20 SMI"

#include <sys/ethernet.h>
#include <clprivnet/clprivnet_export.h>

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Header for the Sun Cluster clprivnet Streams DLPI Driver
 */

/*
 * Although clprivnet MTU should really depend on the MTU of
 * the underlying interconnects (minimum of all configured interconnect
 * MTUs?), for now it is hardcoded at 1500 - the minimum MTU for the
 * known interconnects Ethernet and RSM interconnects. At this point we
 * are unable to take advantage of Ethernet Jumbo frames or large
 * SCI MTUs.
 */
#define	CLPRIVNET_MTU	1500

/*
 * clprivnet specific mac structures. Clprivnet MAC addresses
 * are same size as Ethernet MAC addresses. Clprivnet uses the
 * Ethernet MAC header format as well.
 */

struct	clprivnet_addr {
	uchar_t	clp_addr[ETHERADDRL];
#define	low_order_byte clp_addr[ETHERADDRL-1]
};

struct clprivnet_hdr {
	struct	clprivnet_addr	p_dest;
	struct	clprivnet_addr	p_src;
	ushort_t		p_type;
};

#define	CLPRIVNET_HDR_LEN	(sizeof (struct clprivnet_hdr))

/*
 * Private DLPI dlsap address format.
 */
struct clprivnet_dladdr {
	struct clprivnet_addr	dl_addr;
	ushort_t		dl_sap;
};

/*
 * Maximum size of a physical medium MAC header. Clprivnet uses
 * this as an estimate to preallocate space for any header that
 * the physical device might wish to add to packets xmitted by
 * clprivnet. For classical Ethernet it is 14, for Ethernet with
 * VLAN tag it is 18. SCI uses a header equal to 10 bytes.
 * Infiniband uses 24 bytes. This is only a hint.
 * Code will work correctly even if the hint is wrong.
 */
#define	MAX_PHYSICAL_HDR_LEN	24

/*
 * Maximum SAP type supported by clprivnet.
 */

#define	CLPRIVNETTYPE_MAX	((ushort_t)0xffff)


/*
 * XXX Maximum # of multicast address per Stream
 */

#define	DEVMAXMC	64
#define	DEVMCALLOC	(DEVMAXMC * sizeof (struct clprivnet_addr))



/*
 * XXX Definitions for modupsli_info.
 * Fill in values appropriate for your driver.
 * Contact USL to obtain a unique "registered" module ID number
 */

#define	CLPRIVNET_IDNUM	(999)	/* module ID number */
#define	CLPRIVNET_NAME	"clprivnet"	/* module name */
#define	CLPRIVNET_MINPSZ	(0)	/* min packet size */
#define	CLPRIVNET_MAXPSZ	INFPSZ	/* max packet size */
#define	CLPRIVNET_HIWAT	(16 * 1024)	/* hi-water mark */
#define	CLPRIVNET_LOWAT	(1)	/* lo-water mark */


/*
 * Per-Stream instance state information.
 *
 * Each instance is dynamically allocated at open() and free'd at
 * close(). Each per-stream instance points to at most one per-device
 * structure using the cs_clnetp field. All instances are threaded
 * together into one list of active instances ordered on minor device
 * number (low minor number to high).
 */

struct clprivnet_str {
	struct clprivnet_str *cs_nextp;	/* next in list */
	queue_t *cs_rq;			/* ptr to our rq */
	struct clprivnet *cs_clprivnetp; /* attached device */
	uint32_t cs_state;		/* current state */
	ushort_t cs_sap;		/* bound sap */
	uint32_t cs_flags;		/* misc flags */
	uint32_t  cs_mccount;		/* # enabled multicast addrs */
	struct clprivnet_addr *cs_mctab; /* table of multicast addrs */
	uint32_t cs_minor;		/* minor device number */
	kmutex_t cs_lock;		/* protect this struct */
};


/* per-stream flags */
#define	CLPRIVNET_FAST		0x01	/* M_DATA fast path mode */
#define	CLPRIVNET_RAW		0x02	/* M_DATA plain raw mode */
#define	CLPRIVNET_ALLPHYS	0x04	/* "promiscuous mode" */
#define	CLPRIVNET_ALLMULTI	0x08	/* enable all multicast addresses */
#define	CLPRIVNET_ALLSAP	0x10	/* enable all ether type values */

/*
 * Per-Device instance state information.
 *
 * Each instance is dynamically allocated on first attach.
 */
struct  clprivnet {
	struct clprivnet *clpn_nextp;		/* next in linked list */
	dev_info_t	*clpn_dip;		/* dev info */
	struct clprivnet_addr clpn_ouraddr; 	/* mac address */
	uint32_t	clpn_flags;		/* misc. flags */
	uint32_t	clpn_promisc_phys_cnt;	/* Promiscous streams open */

	/* counters to keep stats for netstat support */
	uint32_t clpn_opackets;			/* # packets sent */
	uint32_t clpn_oerrors;			/* # total output errors */
};

/* flags */
#define	CLPRIVNETRUNNING	0x01	/* chip is initialized */
#define	CLPRIVNETSLAVE	0x02	/* slave device (no DMA) */
#define	CLPRIVNETPROMISC	0x04	/* promiscuous mode enabled */


/*
 * XXX Define offset to receive descriptor.
 */

#define	CLPRIVNETHEADROOM	34


/*
 * Full DLSAP address length. This is a covenience macro. We use
 * ETHERADDRL where only the MAC address length is needed and
 * CLPRIVNET_ADDRL where the MAC address length plus the ether
 * type length is required. This is similar in style to ETHERADDRL
 * and HMEADDRL in the hme driver.
 */
#define	CLPRIVNET_ADDRL ((long)(sizeof (ushort_t) + ETHERADDRL))

#define	DLADDRL	(80)

#ifdef KSTAT

/*
 * "Export" a few of the error counters via the kstats mechanism.
 */
struct  clnetstat {
	struct kstat_named les_ipackets;
	struct kstat_named les_ierrors;
	struct kstat_named les_opackets;
	struct kstat_named les_oerrors;
	struct kstat_named les_collisions;
};

#endif /* KSTAT */

#define	CLPRIVNET_SAPMATCH(sap, type, flags) ((sap == type) ? 1 : \
	(flags & CLPRIVNET_ALLSAP) ? 1 : 0)


/*
 * Compare two clprivnet addresses - assumes that the two given
 * pointers can be referenced as shorts.  On architectures
 * where this is not the case, use bcmp instead.  Note that like
 * bcmp, we return zero if they are the SAME.
 */

#if defined(sparc) || defined(__sparc)
#define	clprivnet_cmp(a, b) (((short *)b)[2] != ((short *)a)[2] || \
	((short *)b)[1] != ((short *)a)[1] || \
	((short *)b)[0] != ((short *)a)[0])
#else
#define	clprivnet_cmp(a, b) (bcmp((caddr_t)a, (caddr_t)b, 6))
#endif



#ifdef __cplusplus
} /* extern "C" */
#endif




#endif	/* _CLPRIVNET_H */
