/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SL_SERVICE_H
#define	_SL_SERVICE_H

#pragma ident	"@(#)sl_service.h	1.74	09/02/13 SMI"

//
// Slave service module - definitions
//

#include <h/network.h>
#include <sys/os.h>
#include <cl_net/netlib.h>
#include <cl_net/bind_table.h>
#include <cl_net/sl_pdt.h>

#undef nil // <inet/common.h> redefines nil

#include <sys/disp.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <inet/common.h>
#include <inet/led.h>
#include <netinet/ip6.h>
#include <inet/ip.h>
#include <inet/tcp.h>
#include <inet/mi.h>


#ifndef _KERNEL
#define	timeout(x, y, z) (NULL)
#define	untimeout(x) (0)
#endif

#define	SL_LISTEN	0x01
#define	SL_UNLISTEN	0x02
#define	SL_BIND		0x04
#define	SL_UNBIND	0x08
#define	SL_CONNECT	0x10
#define	SL_DISCONNECT	0x20

//
// Local state of listeners
//
#define	SL_LOCAL_DOWN	0
#define	SL_LOCAL_UP	1

//
// Our tcp access function, along with macros for inspecting
// the information passed back to us. We will store the port numbers
// in host order and store the network addresses in network order
//

#if SOL_VERSION >= __s10
#ifndef _KERNEL
extern "C" int cl_tcp_walk_list(netstackid_t,
    int (*callback)(cl_tcp_info_t *, void *), void *);
#endif // _KERNEL
#else // SOL_VERSION >= __s10
extern "C" int cl_tcp_walk_list(int (*callback)(cl_tcp_info_t *,
    void *), void *);
#endif// SOL_VERSION >= __s10

//
// TCP_FADDR() and TCP_LADDR() now return the entire address field
// instead of just the V4 portion. The address family is given by
// TCP_VERSION(), which decides how the address field should be
// interpreted by the TCP walk list routine.
//
#define	TCP_FPORT(p) ntohs(((in_port_t)(p)->cl_tcpi_fport))
#define	TCP_FADDR(p) ((p)->cl_tcpi_faddr_v6)
#define	TCP_LPORT(p) ntohs(((in_port_t)(p)->cl_tcpi_lport))
#define	TCP_LADDR(p) ((p)->cl_tcpi_laddr_v6)
#define	TCP_STATE(p) ((p)->cl_tcpi_state)
#define	TCP_VERSION(p) ((p)->cl_tcpi_ipversion)

//
// This is our callback routine which in passed into cl_tcp_walk_list
// and subsequently repeatedly called by base Solaris with information
// about every network connection.
//
// We are given a cl_tcp_info_t structure (which comes from tcp.h).
//
extern "C" int get_conn_info(cl_tcp_info_t *, void *);

#define	FWDSEQ_INCR_LEN	500
#define	SL_COLLECT_SPECIFIC	1
#define	SL_COLLECT_ALL	-1

extern nodeid_t mc_node;

class sl_servicegrp_t;
class sl_serviceobj_t;

//
// type for list of slave service objects
//
typedef IntrList<sl_servicegrp_t, _DList> sl_servicegrplist_t;
typedef IntrList<sl_serviceobj_t, _DList> sl_serviceobjlist_intr_t;
typedef DList<sl_serviceobj_t> sl_serviceobjlist_t;

typedef DList<network::cid_t> cidlist_t;


typedef struct {
	uint_t index;
	network::fwd_t *fwd;
	sl_servicegrp_t *service;
	int flag;
} cb_object;

// Defines the states that the cleanup threads can be in
// CLEANUP_NONE : The thread has not been created
// CLEANUP_EXIT : The thread is being asked to exit
// CLEANUP_CREATE : Thread has been created
// CLEANUP_START : The thread has been asked to wake up and clean
// CLEANUP_RUNNING : The thread is doing the cleanup and busy.

#define	CLEANUP_NONE	0
#define	CLEANUP_EXIT	1
#define	CLEANUP_CREATE	2
#define	CLEANUP_START	3
#define	CLEANUP_RUNNING	4

typedef struct {
	os::condvar_t	rr_cleanup_cv;
	int		cleanup_state;
	os::mutex_t	rr_cleanup_lock;
} rr_cleanup;

typedef struct {
	os::mutex_t 	rr_timeout_lock;
	timeout_id_t    rr_timeout_id;
} udp_rr_timeout;

typedef struct {
	os::condvar_t   connct_cv;
	cidlist_t	cidlst;
	int		state;
	os::mutex_t	connct_lock;
} conn_post;

//
// Service class on slave nodes
//
class sl_servicegrp_t: public servicegrp {

public:

#ifdef	FUTURE
	static unsigned long
	dump_slinfo();
#endif

	sl_servicegrp_t(const network::group_t &group,
	    unsigned int nhash, network::lb_specifier_t policy);

	~sl_servicegrp_t();

	bool equal(const network::service_sap_t &nsap);
	bool ip_equal(const network::service_sap_t &nsap);
	bool matches(const network::service_sap_t &nsap);
	bool ip_matches(const network::service_sap_t &nsap);

#ifdef SCTP_SUPPORT
	bool is_sctp_service();
#endif

	//
	// Handlers for listeners coming up and down
	//
	void sl_local_registered(sl_serviceobj_t *sobj);
	void sl_local_deregistered(sl_serviceobj_t *sobj);

	//
	// Method to setup proxy pdts for specified global
	// network interface node
	//
	void sl_deregister_gifnode();

	//
	// Set the gifnode info in the serviceobjs
	//
	void sl_set_gifnode(const network::service_sap_t &nsap,
	    nodeid_t node);

	nodeid_t sl_find_gifnode(const network::service_sap_t &nsap);

	nodeid_array_t sl_find_all_gifnodes();

	//
	// Method to collect active connection information associated
	// with a pdt bucket
	//
	int sl_conninfo(const network::hashinfo_t& hashinfo,
	    const nodeid_t gifnode, network::fwd_t *fwdp, uint_t flag);

	void sl_dispatch_pernode_lst();

	//
	// Add forwarding info to the slave pdt. Used by set_primary_gifnode()
	//
	void sl_add_frwd_info(const network::service_sap_t& ssap,
	    const network::fwd_t& fwdp, uint_t flag);

	//
	// Remove forwarding info for this node.
	//
	void sl_clean_frwd_info(nodeid_t node);

	//
	// Remove forwarding info for this ssap.
	//
	void sl_clean_frwd_info_by_ssap(const network::service_sap_t& ssap);

	//
	// Stop accepting syns from now on.
	//
	void sl_drop_syns(uint32_t index);

	//
	// Set the PDT_INITIALIZED flag
	//
	void sl_init_complete();

	//
	// Return all IP addresses in the service group
	//
	bool getaddrs(uint8_t **addrp, uint_t *naddrs, size_t size);

	//
	// Internal method to initialize slave pdt
	//
	void sl_create_pdt(sl_pdt *table);

	bool sl_pdt_exists();
	void sl_unmarshal_pdtinfo(const network::pdtinfo_t& pdtinfo);

	//
	// Internal method to return pdt value given the sap value
	//
	unsigned int sl_input(network::cid_t& cid,
	    network::cid_action_t& action, uint32_t& index, uint_t flag);

	void sl_stop_forward(const network::cid_t& cid, nodeid_t node);

	bool sl_check_hashindex(const network::cid_t& cid, uint_t index);
	bool sl_is_forwarded(network::cid_t& cid);
	void sl_update_frwd_count(const network::cid_t& cid);

	//
	// Stickiness configuration helper routines
	//

	bool is_strong_sticky();
	bool is_wildcard_sticky();
	bool is_udp_sticky();
	bool is_generic_affinity();

	void sl_get_sticky_config(network::sticky_config_t& st);
	void sl_config_sticky(const network::sticky_config_t& st);
	void sl_config_pdt_sticky();

	// RR configuration function
	bool is_rr_enabled();
	void sl_get_rr_config(network::rr_config_t& rr);

	//
	// Proxy side routines for binding entry processing
	//

	// (Re)create proxy binding table.
	void sl_create_proxy_bind_table();

	// Connection open processing routine
	int sl_openconn(network::cid_t& cid);

	// Connection close processing routine
	void sl_closeconn(network::cid_t& cid);

	void sl_setstate(const network::cid_t& cid);

	void sl_resetall_usage_bit();
	// Return all binding entries (during GIF failover)
	void sl_get_bindinfo(nodeid_t new_gifnode, network::bind_t *bindp);

	void sl_create_proxy_bind_entry(const network::fwd_t& fwdp);

	//
	// GIF side processing routines for binding entries
	//

	// Add a list of binding entries (during GIF failovers)
	void sl_add_bindinfo(const network::bind_t& bindp, uint_t iter);

	void sl_add_pernodelst(uint_t iter);

	// Node crash handler routine
	void sl_clean_bindinfo(nodeid_t node);

	// Set state of the specified binding entry
	bool sl_setstate_binding(const network::inaddr_t& faddr,
	    const nodeid_t node, network::bind_state_t, boolean_t);

	void sl_config_rr(const network::rr_config_t& rr);
	//
	// List of sl_serviceobjs associated with this sl_servicegrp
	//
	sl_serviceobjlist_t sl_serviceobj_list;
	os::rwlock_t 	sl_serviceobj_list_lock;

	//
	// This lock is held only by threads updating the forwarding
	// list and the incoming interrupt thread in read mode.
	//
	os::rwlock_t	sl_frwd_list_lock;

	// Debugging methods
	//
	void sl_gns_bind(bool as_gif, network::gns_bindseq_t *, int &);
	void sl_marshal_pdt(network::pdtinfo_t *pdtinfo, Environment& e);
	void sl_pre_marshal_fwd(network::seqlong_t *cntseq, nodeid_t node);
	void sl_marshal_fwd(network::fwdseq_t *fwdinfo, nodeid_t node,
	    Environment& e);
	bool has_pdt();
	void sl_marshal_sgrp(network::grpinfo_t *srvinfo, Environment& e);
	void init_cleanup_thread();

	// Roundrobin UDP cleanup

	void handle_cleanup(int is_timeout = 0);
	void cancel_udp_timeout();

	void rr_clean_frwd_info(int is_timeout);
	void rr_clean_bind_info();

	void connect_call();

	// Roundrobin cleanup thread
	klwp_id_t	rr_thrid;
	klwp_id_t	cnct_thrid;

	rr_cleanup	cleanup_post;
	conn_post	connct_post;


protected:
	sl_pdt	*slpdt;

	sl_rr	*slrr;	// co-exists with slpdt to implement rr

	affinity_lst	*cid_lst;

	os::mutex_t sl_forward_lock;
	int *sl_forwarded_count;

	udp_rr_timeout	udp_timeout;

	// Proxy node binding table
	proxy_bind_table_t *sl_bindtab;

	// Sticky service configuration parameters
	network::sticky_config_t sl_sticky_config;

#ifdef SCTP_SUPPORT
	void collect_sctp_conn_info(network::fwd_t *fwdp);
#endif

	// Load balancing configuration parameters
	network::rr_config_t	sl_rr_config;

	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	sl_servicegrp_t(const sl_servicegrp_t &);
	sl_servicegrp_t &operator = (sl_servicegrp_t &);

	bool	accept_conn;

};

class sl_serviceobj_t: public serviceobj {
public:
	sl_serviceobj_t(sl_servicegrp_t &group,
	    const network::service_sap_t &ssap);

	sl_serviceobj_t(const network::service_sap_t &ssap);

	~sl_serviceobj_t();

	ulong_t	sl_flags;

	//
	// State of listener for the SAP of this sobj
	//
	uchar_t sl_local_state;

	//
	// Debugging methods
	//
	void sl_marshal_sobj(network::srvinfo_t *srvinfo, Environment& e);

	void set_service_grp(sl_servicegrp_t &group);

	//
	// The servicegrp which points at this object.
	//
	sl_servicegrp_t *sl_servicegrp;
};

#endif	/* _SL_SERVICE_H */
