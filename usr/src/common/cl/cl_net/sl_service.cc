//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sl_service.cc	1.115	09/02/13 SMI"

//
// Slave service module
//
#include <h/network.h>
#include <sys/os.h>

#include <h/network.h>
#include <sys/os.h>
#include <sys/threadpool.h>
#include <orb/infrastructure/orb_conf.h>

#include <cl_net/netlib.h>
#include <cl_net/sl_pdt.h>
#include <cl_net/sl_service.h>
#include <cl_net/mcobj_impl.h>

#include <sys/mc_probe.h>
#include <orb/infrastructure/clusterproc.h>

#include <sys/types.h>
#include <sys/conf.h>

#if (SOL_VERSION >= __s10) && defined(_KERNEL)

extern int clnet_version;
extern int (*cl_tcp_walk_list_v0)(int (*callback)(cl_tcp_info_t *,
    void *), void *);
extern int (*cl_tcp_walk_list_v1)(netstackid_t,
    int (*callback)(cl_tcp_info_t *, void *), void *);

#endif // (SOL_VERSION >= __s10) && defined(_KERNEL)

extern "C" void cleanup_thread(void);
extern "C" void timeout_func(void *);
extern "C" void cnct_disp(void);


extern mcobj_impl_t *mcobjp;
extern threadpool dispatch_conn_threadpool;

sl_serviceobj_t::sl_serviceobj_t(sl_servicegrp_t &group,
    const network::service_sap_t &ssap) : serviceobj(ssap)
{
	sl_flags = NULL;
	sl_local_state = SL_LOCAL_DOWN;
	sl_servicegrp = &group;
}

sl_serviceobj_t::sl_serviceobj_t(const network::service_sap_t &ssap) :
    serviceobj(ssap)
{
	sl_flags = NULL;
	sl_local_state = SL_LOCAL_DOWN;
	sl_servicegrp = NULL;
}

sl_serviceobj_t::~sl_serviceobj_t()
{
	sl_servicegrp = NULL;
}

void
sl_serviceobj_t::set_service_grp(sl_servicegrp_t &group)
{
	sl_servicegrp = &group;
}

sl_servicegrp_t::sl_servicegrp_t(const network::group_t &group,
    unsigned int numhash, network::lb_specifier_t policy) :
    servicegrp(group, numhash, policy)
{
	slpdt = NULL;

	slrr = NULL;

	sl_forwarded_count = new int[pdt_hash_size];
	for (uint_t i = 0; i < pdt_hash_size; i++) {
		sl_forwarded_count[i] = 0;
	}

	INIT_STICKY_CONFIG(sl_sticky_config);
	INIT_RR_CONFIG(sl_rr_config);

	sl_bindtab = NULL;
	if (is_strong_sticky()) {
		//
		// Need to create sl_bindtab to maintain the invariant
		// that a strong sticky group always has a sl_bindtab.
		// When we configure the SAPs later on, we would then
		// re-initialize sl_bindtab.
		//
		sl_create_proxy_bind_table();
	}

	rr_thrid = NULL;
	udp_timeout.rr_timeout_id = NULL;
	cnct_thrid = NULL;

	cleanup_post.cleanup_state = CLEANUP_NONE;
	connct_post.state = CLEANUP_NONE;
	cid_lst = NULL;

}

sl_servicegrp_t::~sl_servicegrp_t()
{

	if (sl_rr_config.rr_flags & network::LB_RR_UDP) {
		cancel_udp_timeout();
	}

	cleanup_post.rr_cleanup_lock.lock();
	if (cleanup_post.cleanup_state != CLEANUP_NONE) {
		cleanup_post.cleanup_state = CLEANUP_EXIT;
		cleanup_post.rr_cleanup_cv.signal();
	}
	cleanup_post.rr_cleanup_lock.unlock();

	connct_post.connct_lock.lock();
	if (connct_post.state != CLEANUP_NONE) {
		connct_post.state = CLEANUP_EXIT;
		connct_post.connct_cv.signal();
	}
	connct_post.connct_lock.unlock();

	cleanup_post.rr_cleanup_lock.lock();
	while (cleanup_post.cleanup_state != CLEANUP_NONE) {
		cleanup_post.cleanup_state = CLEANUP_EXIT;
		cleanup_post.rr_cleanup_cv.signal();
		cleanup_post.rr_cleanup_lock.unlock();
		os::usecsleep(100000L);
		cleanup_post.rr_cleanup_lock.lock();
	}
	cleanup_post.rr_cleanup_lock.unlock();

	connct_post.connct_lock.lock();
	while (connct_post.state != CLEANUP_NONE) {
		connct_post.state = CLEANUP_EXIT;
		connct_post.connct_cv.signal();
		connct_post.connct_lock.unlock();
		os::usecsleep(100000L);
		connct_post.connct_lock.lock();
	}
	connct_post.connct_lock.unlock();

	//
	// Should have emptied the serviceobj list before
	// destroying the group.
	//
	ASSERT(sl_serviceobj_list.empty());

	if (sl_bindtab) {
		delete sl_bindtab;
		sl_bindtab = NULL;			//lint !e423
	}
	if (slrr != NULL) {
		delete slrr;
		slrr = NULL;
	}
	if (slpdt != NULL) {
		delete slpdt;
		slpdt = NULL;
	}
	if (cid_lst != NULL) {
		delete cid_lst;
		cid_lst = NULL;
	}
	delete [] sl_forwarded_count;
}

void
sl_servicegrp_t::sl_deregister_gifnode()
{
	if (cid_lst != NULL) {
		delete cid_lst;
		cid_lst = NULL;
	}

	delete slpdt;
	slpdt = NULL;
}

void
sl_servicegrp_t::sl_dispatch_pernode_lst()
{
	if (cid_lst != NULL) {
		// Dispatch the pernodelst for the proxy nodes.
		// Stop the timer and handle the dispatch
		NET_DBG_BIND(("dispatch pernodelst for deregister\n"));
		cid_lst->stop_timeout();
		cid_lst->handle_dispatch(1);
		dispatch_conn_threadpool.quiesce(threadpool::QEMPTY);
	}
}

//
// Effects: Given argument "table" add this object to the slave
// service group as the slave PDT.  If "table" is NULL, then this
// routine has no effect.
//
// Parameters:
//   table (IN):  The slave PDT table to install.
//
void
sl_servicegrp_t::sl_create_pdt(sl_pdt *table)
{
	MC_PROBE_0(sl_init_pdt, "mc net slservice", "");

	NET_DBG(("sl_servicegrp_t::sl_create_pdt()\n"));
	if (slpdt == NULL && table != NULL) {
		slpdt = table;
	}

	ASSERT(slpdt != NULL);

	if (is_sticky(getpolicy()))
		slpdt->pdt_config_sticky(sl_sticky_config);

	if (is_rr_enabled()) {
		slpdt->set_rr_config(sl_rr_config);
	}

	if (is_generic_affinity()) {
		cid_lst = new affinity_lst();
	}


}


//
// Effects: This routine takes argument "pdtinfo" and unmarshals the
// data to populate the slave PDT table.
//
// Parameters:
//    pdtinfo (IN):  Information about the PDT that allows that PDT
// 			to be constructed as a copy of the master.
//
void
sl_servicegrp_t::sl_unmarshal_pdtinfo(const network::pdtinfo_t& pdtinfo)
{
	NET_DBG(("sl_servicegrp_t::sl_unmarshal_pdtinfo()\n"));

	if (is_rr_enabled()) {
		if (slrr != NULL) {
			delete slrr;
		}
		slrr = new sl_rr(pdtinfo);
	} else {
		(void) slpdt->unmarshal_pdtinfo(pdtinfo);
	}

	//
	// Let the pdt be in the uninitialized state for now.
	// We are not ready to accept packets until all the
	// forwarding information is downloaded to the
	// gifnode.
	//
	slpdt->pdt_setflag(PDT_UNINITIALIZED);
	NET_DBG(("PDT not YET READY\n"));
}

void
sl_servicegrp_t::sl_init_complete()
{
	NET_DBG(("PDT in the INITIALIZED state\n"));

	slpdt->pdt_setflag(PDT_INITIALIZED);
}

//
// Returns all IP addresses configured for the service group
//
bool
sl_servicegrp_t::getaddrs(uint8_t **addrp, uint_t *naddrs, size_t size)
{
	uint_t max_addr = (uint_t)(size / sizeof (network::inaddr_t));
	sl_serviceobj_t *sobj;
	bool r = true;

	network::inaddr_t *taddrp = (network::inaddr_t *)(*addrp);
	sl_serviceobj_list_lock.rdlock();

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	*naddrs = 0;
	for (; (sobj = iter.get_current()) != NULL;
	    iter.advance()) {
		if (*naddrs >= max_addr) {
			r = false;
			break;
		}
		*taddrp = sobj->getssap().laddr;

		taddrp ++;
		(*naddrs) ++;
	}

	sl_serviceobj_list_lock.unlock();

	return (r);
}


//
// For the benefit of unode we duplicate the things get_conn_info
// needs that are only defined in tcp.h and ip6.h when _KERNEL is
// defined.  However, in s81 Update 4 we no longer have access to ip6.h,
// so we also need this for the kernel side as well.
//

#ifndef _KERNEL
#define	s6_addr32		_S6_un._S6_u32
#define	V4_PART_OF_V6(v6)	v6.s6_addr32[3]
#define	cl_tcpi_laddr		V4_PART_OF_V6(cl_tcpi_laddr_v6)
#define	cl_tcpi_faddr		V4_PART_OF_V6(cl_tcpi_faddr_v6)
#endif

//
// TCP walk list routine helper to convert an IP address found during
// the TCP walk into a format suitable for subsequent processing. That
// is, if V4 address, make a V4-mapped of it. If V6, just copy it.
// The converted address is returned in addr.
//
static inline void
copy_tcpaddr_to_addr(cl_tcp_info_t *tcp, in6_addr_t& ipaddr,
    network::inaddr_t& addr)
{
	if (TCP_VERSION(tcp) == IPV4_VERSION) {
		IN6_IPADDR_TO_V4MAPPED(V4_PART_OF_V6(ipaddr),
		    (in6_addr_t *)&addr);
	} else {
		*(in6_addr_t *)&addr = ipaddr;
	}
}

//
// Appends SCTP connection info collected from connection table to the
// supplied list of fwdp. Only connections related to this service group
// are collected. This function is used while reconstructing forwarding list
//
#ifdef SCTP_SUPPORT
void
sl_servicegrp_t::collect_sctp_conn_info(network::fwd_t *fwdp)
{
	uint8_t *wfaddrp;
	network::inaddr_t taddr;
	conn_table_t *ct = mcobjp->mc_conntab;
	conn_entry_t *entry;

	for (uint_t index = 0; index < ct->conn_hashsize; index ++) {
		ct->conn_list_lock[index].rdlock();
		conn_list_t::ListIterator li(&ct->conn_list[index]);
		while ((entry = li.get_current()) != NULL) {
			li.advance();
			if (is_sticky_wildcard(getpolicy())) {
				if (!ip_matches(entry->cn_ssap))
					continue;
			} else {
				if (!matches(entry->cn_ssap))
					continue;
			}
			wfaddrp = entry->cn_faddrp;
			for (uint_t i = 0; i < entry->cn_nfaddrs; i++) {

				if ((uint32_t)fwdp->fw_ncid >=
				    fwdp->fw_cidseq.length()) {
					//
					// Grow the unbounded sequence by
					// increments of FWDSEQ_INCR_LEN.
					//
					fwdp->fw_cidseq.length((uint_t)fwdp->
					    fw_ncid + FWDSEQ_INCR_LEN);
				}

				copy_tcpaddr_to_addr(AF_INET6, wfaddrp, &taddr);
				fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].
				    ci_protocol = entry->cn_ssap.protocol;
				fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].
				    ci_lport = entry->cn_ssap.lport;
				fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].
				    ci_laddr = entry->cn_ssap.laddr;
				fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].
				    ci_fport = entry->cn_fport;
				fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].
				    ci_faddr = taddr;
				fwdp->fw_ncid++;
			}
		}
		ct->conn_list_lock[index].unlock();
	}
}
#endif  // SCTP_SUPPORT

extern "C" int
get_conn_info(cl_tcp_info_t *tcp, void *arg)
{
	network::service_sap_t ssap;

	if (TCP_VERSION(tcp) != IPV4_VERSION &&
	    TCP_VERSION(tcp) != IPV6_VERSION)
		return (0);

	ssap.protocol = IPPROTO_TCP;
	ssap.lport = TCP_LPORT(tcp);
	copy_tcpaddr_to_addr(tcp, TCP_LADDR(tcp), ssap.laddr);

	cb_object *cbobjp = (cb_object *)arg;

	// Make sure that this is an active connection

	if (TCP_STATE(tcp) < TCPS_SYN_RCVD) {
		return (0);
	}

	sl_servicegrp_t *servicep = cbobjp->service;

	//
	// matches() is a good matching function. This will match
	// the sap against any of the several saps in a servicegrp.
	// This way, it is not necessary to pass a sequence of saps
	// from the pdt server to collect the forwarding information.
	// All that is necessary is to pass a single sap of a servicegrp
	// so that we hit corresponding servicegrp on the slave and
	// everything else is automagically taken care of.
	//
	// Note that if this is a wildcard group, the local port should
	// not be matched (hence the use of ip_matches() instead). Doing
	// so would result in some connections not being counted for
	// wildcard groups, since the local port is most likely ephemeral
	// and is not recorded in any of our states.
	//
	if (is_sticky_wildcard(servicep->getpolicy())) {
		if (!servicep->ip_matches(ssap))
			return (0);
	} else {
		if (!servicep->matches(ssap))
			return (0);
	}

	//
	// We are not interested in connections that originate from a
	// scalable address. Return if this is one of them.
	//
	network::inaddr_t faddr;
	copy_tcpaddr_to_addr(tcp, TCP_FADDR(tcp), faddr);

	//
	// We want to ignore connections that originate from a scalable
	// address to a scalable address. These entries are useless for
	// both forwarding entries and binding entries, since they are not
	// for clients external to the cluster.
	// What's worse is that TCP doesn't seem to give us a balanced
	// number of connect() and disconnect() callbacks on these
	// connections. These connections would therefore be harmful for
	// the client affinity connection counting mechanism.
	//
	if (mcobjp->is_scalable_address(faddr)) {
		NET_DBG_BIND(("get_conn_info: ignoring faddr %s\n",
		    network_lib.ipaddr_to_string(faddr)));
		return (0);
	}

	//
	// Get the cid information.
	// Note that fwdp is an unbounded sequence.
	//
	network::fwd_t *fwdp = cbobjp->fwd;
	fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_protocol = IPPROTO_TCP;
	fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_lport = TCP_LPORT(tcp);
	fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_laddr = ssap.laddr;
	fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_fport = TCP_FPORT(tcp);
	fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_faddr = faddr;

	//
	// Check if the cid corresponds to the bucket being
	// changed. If the flag is set to PDT_COLLECT_SPECIFIC then
	// we change things on a bucket-by-bucket basis.  Otherwise, all
	// cids corresponding to a service are collected. The gif node
	// will do the sorting.

	if (cbobjp->flag == SL_COLLECT_SPECIFIC) {
		if (!servicep->sl_check_hashindex(
		    fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid], cbobjp->index))
			return (0);
	} else {
		ASSERT(cbobjp->flag == SL_COLLECT_ALL);
	}

	fwdp->fw_ncid++;

	if ((uint32_t)fwdp->fw_ncid >= fwdp->fw_cidseq.length()) {
		//
		// Grow the unbounded sequence by increments of
		// FWDSEQ_INCR_LEN. We hope will not have more than
		// FWDSEQ_INCR_LEN connections because growing involves
		// allocating more memory and copying information from
		// the old ptr to the new location.
		//
		fwdp->fw_cidseq.length((uint_t)fwdp->fw_ncid +
			FWDSEQ_INCR_LEN);
	}
	return (0);
}

//
// A helper function to mc_closeconn() to see if we need
// to make the stop_forwarding() call to the gif node and the
// pdt server or not.
//
bool
sl_servicegrp_t::sl_is_forwarded(network::cid_t& cid)
{
	uint_t index = pdt_hash_func(cid, getpolicy());

	//
	// Assumption is that this function will be used
	// only by the mc_closeconn to see if this particular
	// connection is getting forwarded or not. This way
	// the number of stop_forwarding() invocations to the
	// pdt server and the slave pdt can be reduced by not
	// doing the invocations if we are not in a forwarding
	// mode for this bucket.
	//

	sl_forward_lock.lock();

	ASSERT(sl_forwarded_count[index] >= 0);

	if (sl_forwarded_count[index] <= 0) {
		sl_forward_lock.unlock();
		return (false);
	} else {
		sl_forwarded_count[index]--;
		sl_forward_lock.unlock();
		return (true);
	}
}

bool
sl_servicegrp_t::sl_check_hashindex(const network::cid_t& cid,
    uint_t index)
{
	// Hash index is being directly calculated here since
	// pdt_hashindex() cannot be accessed from sl_servicegrp_t
	// and there might not be a proxy pdt anymore. So if the
	// hashing function CHANGES, update it here too.

	MC_PROBE_2(sl_check_hashindex, "mc net fr_equal", "",
	    tnf_string, ci_faddr, network_lib.ipaddr_to_string(cid.ci_faddr),
	    tnf_int, ci_fport, cid.ci_fport);

	if (pdt_hash_func(cid, getpolicy()) == index)
		return (true);
	else
		return (false);
}

void
sl_servicegrp_t::sl_update_frwd_count(const network::cid_t& cid)
{
	uint_t index = pdt_hash_func(cid, getpolicy());
	sl_forward_lock.lock();
	sl_forwarded_count[index]++;
	sl_forward_lock.unlock();
}

int
sl_servicegrp_t::sl_conninfo(const network::hashinfo_t& hashinfo,
    const uint32_t, network::fwd_t *fwdp, uint_t flag)
{
	cb_object cbobj;
	cbobj.fwd = fwdp;
	cbobj.index = hashinfo.index;
	cbobj.service = this;

	NET_DBG_FWD(("sl_servicegrp_t::sl_conninfo()\n"));

	if (flag == pdt::PDT_COLLECT_SPECIFIC) {
		cbobj.flag = SL_COLLECT_SPECIFIC;

		MC_PROBE_0(sl_conninfo_specific, "mc net mcfwd", "");
		NET_DBG_FWD(("  will collect specific info \n"));
	} else {
		cbobj.flag = SL_COLLECT_ALL;

		MC_PROBE_0(sl_conninfo_all, "mc net mcfwd", "");
		NET_DBG_FWD(("  collecting GENERAL info \n"));

		sl_forward_lock.lock();
		for (int i = 0; i < getnhash(); i++)
			sl_forwarded_count[i] = 0;
		sl_forward_lock.unlock();
	}

	fwdp->fw_ncid = 0;
	// Set the initial length of the sequence to FWDSEQ_INCR_LEN.
	fwdp->fw_cidseq.length(FWDSEQ_INCR_LEN);

#if SOL_VERSION >= __s10
#ifdef _KERNEL
	if (clnet_version > 0) {
		ASSERT(cl_tcp_walk_list_v1 != NULL);
		ASSERT(cl_tcp_walk_list_v0 == NULL);
		(void) (*cl_tcp_walk_list_v1)(-1, get_conn_info,
		    (void *)&cbobj);
	} else {
		ASSERT(cl_tcp_walk_list_v0 != NULL);
		ASSERT(cl_tcp_walk_list_v1 == NULL);
		(void) (*cl_tcp_walk_list_v0)(get_conn_info,
		    (void *)&cbobj);
	}
#else // _KERNEL
	(void) cl_tcp_walk_list(-1, get_conn_info, (void *)&cbobj);
#endif // _KERNEL
#else // SOL_VERSION >= __s10
	(void) cl_tcp_walk_list(get_conn_info, (void *)&cbobj);
#endif // SOL_VERSION >= __s10

#ifdef SCTP_SUPPORT
	sl_serviceobj_list_lock.rdlock();
	if (is_sctp_service()) {
		sl_serviceobj_list_lock.unlock();
		collect_sctp_conn_info(fwdp);
	} else {
		sl_serviceobj_list_lock.unlock();
	}
#endif

	if (fwdp->fw_ncid > 0) {
		if (cbobj.flag == SL_COLLECT_SPECIFIC) {
			//
			// Update forward count for the specific bucket
			//
			sl_forward_lock.lock();
			sl_forwarded_count[hashinfo.index] = fwdp->fw_ncid;
			sl_forward_lock.unlock();
		} else {
			//
			// Find the bucket for each cid, increment the
			// frwd count for that bucket.
			//
			for (uint_t i = 0; i < (uint_t)fwdp->fw_ncid; i++) {
				sl_update_frwd_count(fwdp->fw_cidseq[i]);
			}
		}
	}

	fwdp->fw_node = orb_conf::local_nodeid();
	fwdp->fw_cidseq.length((uint_t)fwdp->fw_ncid);

	MC_PROBE_1(sl_conninfo_all, "mc net mcfwd", "",
		tnf_int, fw_ncid, fwdp->fw_ncid);
	NET_DBG_FWD(("  final cid count = %d \n", fwdp->fw_ncid));

	return (0);
}

//
// Flag with DESTROY indicates the GIF node corres fwd list should be
// detroyed if already exists before adding the new entries. NODESTROY
// means the existing list should not be detroyed.
//
// FORCE signifies entries will be explicitly added to the fwd list even if
// forwarded node matches with the hash bucket's corresponding forwarded node.
// NOFORCE is the opposite
//
void
sl_servicegrp_t::sl_add_frwd_info(const network::service_sap_t& ssap,
    const network::fwd_t& fwdp, uint_t flag)
{
	nodeid_t myself = orb_conf::local_nodeid();
	bool *cidstate;
	nodeid_t gifnode;
	uint_t i, ncids;

	if (slpdt && fwdp.fw_ncid > 0) {
		//
		// Bugid 4624040:
		// fwdp is the list of all connections that belong to this
		// service group. But if this node is not the GIF for a
		// given SAP, we should not carry forwarding entries for
		// this SAP (since packets won't get to this node
		// anyway). Mark them "invalid" in cidstate so that they
		// won't be added to our PDT.
		//
		ncids = (uint_t)fwdp.fw_ncid;
		cidstate = new bool[ncids];
		bzero(cidstate, ncids * sizeof (bool));

		for (i = 0; i < ncids; i++) {
			if ((gifnode = sl_find_gifnode(ssap)) != 0 &&
			    gifnode == myself)
				cidstate[i] = true;
		}

		sl_frwd_list_lock.wrlock();
		slpdt->add_frwd_info(fwdp, cidstate, getpolicy(), flag);
		sl_frwd_list_lock.unlock();

		delete cidstate;
		cidstate = NULL;
	}
}

void
sl_servicegrp_t::sl_clean_frwd_info(nodeid_t node)
{
	if (slpdt) {
		sl_frwd_list_lock.wrlock();
		slpdt->remove_frwd_info(node);
		sl_frwd_list_lock.unlock();
	}
}

//
// Remove forwarding info for this ssap.
//
void
sl_servicegrp_t::sl_clean_frwd_info_by_ssap(const network::service_sap_t& ssap)
{
	if (slpdt) {
		sl_frwd_list_lock.wrlock();
		slpdt->remove_frwd_info_by_ssap(ssap);
		sl_frwd_list_lock.unlock();
	}
}

void
sl_servicegrp_t::sl_drop_syns(uint32_t index)
{
	if (slpdt)
		slpdt->drop_syns(index);
}



//
// Locking: sl_servicegrplist_lock.rdlock() and
//	sl_serviceobj_list_lock.wrlock() protected
//
void
sl_servicegrp_t::sl_local_registered(sl_serviceobj_t *)
{
	int up_count = 0;
	sl_serviceobj_t *sobj;

	if (!is_strong_sticky() || sl_sticky_config.st_timeout <= 0)
		return;

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->sl_local_state == SL_LOCAL_UP)
			up_count++;
	}

	if (up_count == 1) {
		//
		// First instance to come up. Set proper stickiness
		// timeout. (This is needed due to fault monitors that
		// restart listeners.)
		//
		ASSERT(sl_bindtab != NULL);
		sl_bindtab->config_timeout(sl_sticky_config.st_timeout);
		accept_conn = true;
	}
}

//
// Locking: sl_servicegrplist_lock.rdlock() and
//	sl_serviceobj_list_lock.wrlock() protected
//
void
sl_servicegrp_t::sl_local_deregistered(sl_serviceobj_t *)
{
	int up_count = 0;
	sl_serviceobj_t *sobj;

	if (!is_strong_sticky() || sl_sticky_config.st_timeout <= 0)
		return;

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->sl_local_state == SL_LOCAL_UP)
			up_count++;
	}

	if (up_count == 0) {
		//
		// Last instance to come down. Set proper stickiness
		// timeout. (This is needed due to fault monitors that
		// restart listeners.)
		//
		ASSERT(sl_bindtab != NULL);

		//
		// Set timeout to zero so that stickiness is gone as soon
		// as a client closes all its connections.
		//
		sl_bindtab->config_timeout(0);

		//
		// Timeout all entries that currently has a running timer,
		// so that stickiness is removed immediately rather than
		// waiting for the timers to expire.
		//
		sl_bindtab->timeout_all();
		accept_conn = false;
	}
}

bool
sl_servicegrp_t::is_strong_sticky()
{
	return (is_sticky(getpolicy()) &&
	    !(sl_sticky_config.st_flags & network::ST_WEAK)?
	    true: false);
}

bool
sl_servicegrp_t::is_wildcard_sticky()
{
	return (is_sticky_wildcard(getpolicy()));
}

bool
sl_servicegrp_t::is_generic_affinity()
{
	return (is_sticky(getpolicy()) &&
	    (sl_sticky_config.st_flags & network::ST_GENERIC) ? true :false);
}

#ifdef SCTP_SUPPORT
bool
sl_servicegrp_t::is_sctp_service()
{
	sl_serviceobj_t *sobj;

	ASSERT(sl_serviceobj_list_lock.lock_held());

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->getssap().protocol == IPPROTO_SCTP) {
			return (true);
		}
	}
	return (false);
}
#endif

bool
sl_servicegrp_t::is_udp_sticky()
{
	return (is_sticky(getpolicy()) &&
	    !(sl_sticky_config.st_flags & network::ST_UDP)?
	    true: false);
}

bool
sl_servicegrp_t::is_rr_enabled()
{
	bool ret = false;
	ret = (sl_rr_config.rr_flags & network::LB_RR) ? true: false;
	return (ret);
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected, so that the info
// returned here would not be partially updated, say because an
// sl_config_sticky() is underway.
//
void
sl_servicegrp_t::sl_get_sticky_config(network::sticky_config_t &st)
{
	st = sl_sticky_config;
}

//
// Locking: sl_servicegrplist_lock.wrlock() protected, since we could be
// deleting various tables in the service group, and we want to ensure all
// config info are updated atomically.
//
void
sl_servicegrp_t::sl_config_sticky(const network::sticky_config_t& st)
{

	// Save the parameters
	sl_sticky_config = st;

	if (is_sticky(getpolicy())) {
		bool st_weak = (st.st_flags & network::ST_WEAK? true: false);

		if (st_weak && sl_bindtab != NULL) {
			delete sl_bindtab;
			sl_bindtab = NULL;		//lint !e423
		} else if (!st_weak && sl_bindtab == NULL) {
			sl_create_proxy_bind_table();
			sl_bindtab->set_gifnodes(sl_find_all_gifnodes());
		}

		if (sl_bindtab != NULL) {
			sl_bindtab->set_generic_affinity(is_generic_affinity());
			sl_bindtab->config_timeout(st.st_timeout);
		}

		if (slpdt != NULL) {
			//
			// slpdt is not initialized if this node is not a
			// GIF node for the group.
			//
			slpdt->pdt_config_sticky(st);
			if (is_generic_affinity()) {
				if (cid_lst != NULL) {
					delete cid_lst;
					cid_lst = NULL;
				}
				cid_lst = new affinity_lst();
			}
		}
	}
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected, so that the info
// returned here would not be partially updated, say because a change is
// underway
//
void
sl_servicegrp_t::sl_get_rr_config(network::rr_config_t &rr)
{
	rr = sl_rr_config;
}
void
sl_servicegrp_t::sl_config_pdt_sticky()
{
	if (is_sticky(getpolicy())) {
		//
		// We should never be called if slpdt isn't set up yet.
		//
		ASSERT(slpdt != NULL);
		slpdt->pdt_config_sticky(sl_sticky_config);
	}
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
void
sl_servicegrp_t::sl_get_bindinfo(nodeid_t new_gifnode,
    network::bind_t *bindp)
{
	ASSERT(sl_bindtab != NULL);
	bindp->bn_nodeid = orb_conf::local_nodeid();
	sl_bindtab->get_elems(new_gifnode, bindp);
}

void
sl_servicegrp_t::sl_create_proxy_bind_entry(const network::fwd_t& fwdp)
{
	network::cid_t	cid;

	if (accept_conn == false) {
		return;
	}

	for (uint_t indx = 0; indx < (uint_t)fwdp.fw_ncid; indx++) {
		cid = fwdp.fw_cidseq[indx];
		(void) sl_bindtab->conn_open(cid, 1);
	}
}

//
// Create sl_bindtab and populate it with records of existing
// connections. If sl_bindtab already exists, delete it first.
// Existing connections must be recorded, so that their closes will
// properly decrement the counters we maintain in the proxy binding table.
// See 4701492.
// We will not record existing SCTP connections as SCTP does not maintain
// TIME-WAIT connections.
//
void
sl_servicegrp_t::sl_create_proxy_bind_table()
{
	cb_object cbobj;
	network::fwd_t fwd;



	if (sl_bindtab != NULL) {
		//
		// Any existing info must be deleted; otherwise,
		// connections could get double-counted, since this
		// function is called every time a service is created.
		//
		delete sl_bindtab;
	}

	sl_bindtab = new proxy_bind_table_t(bt_hash_size, sl_sticky_config,
	is_rr_enabled());

	if (sl_serviceobj_list.empty()) {
		//
		// No need to record any existing connections, since
		// we do not have any SAPs configured yet.
		//
		return;
	}

	//
	// Collect all connections of this group
	//
	fwd.fw_node = 0;
	fwd.fw_ncid = 0;
	fwd.fw_cidseq.length(FWDSEQ_INCR_LEN);

	cbobj.fwd = &fwd;
	cbobj.index = 0;
	cbobj.service = this;
	cbobj.flag = SL_COLLECT_ALL;

#if SOL_VERSION >= __s10
#ifdef _KERNEL
	if (clnet_version > 0) {
		ASSERT(cl_tcp_walk_list_v1 != NULL);
		ASSERT(cl_tcp_walk_list_v0 == NULL);
		(void) (*cl_tcp_walk_list_v1)(-1, get_conn_info,
		    (void *)&cbobj);
	} else {
		ASSERT(cl_tcp_walk_list_v0 != NULL);
		ASSERT(cl_tcp_walk_list_v1 == NULL);
		(void) (*cl_tcp_walk_list_v0)(get_conn_info,
		    (void *)&cbobj);
	}
#else // _KERNEL
	(void) cl_tcp_walk_list(-1, get_conn_info, (void *)&cbobj);
#endif // _KERNEL
#else // SOL_VERSION >= __s10
	(void) cl_tcp_walk_list(get_conn_info, (void *)&cbobj);
#endif // SOL_VERSION >= __s10

	//
	// "Re-play" all existing connections to record them
	//
	if (fwd.fw_ncid > 0) {
		for (uint_t i = 0; i < (uint_t)fwd.fw_ncid; i++) {
			(void) sl_bindtab->conn_open(fwd.fw_cidseq[i]);
		}
	}
}

//
// Locking: sl_servicegrplist_lock.wrlock() protected, since we could be
// deleting various tables in the service group, and we want to ensure all
// config info are updated atomically.
//
void
sl_servicegrp_t::sl_config_rr(const network::rr_config_t& rr)
{
	// if LB_RR_UDP is set save the flag.
	if (sl_rr_config.rr_flags & network::LB_RR_UDP) {
		sl_rr_config.rr_flags = rr.rr_flags | network::LB_RR_UDP;
	} else {
		sl_rr_config.rr_flags = rr.rr_flags;
	}
	sl_rr_config.rr_conn_threshold = rr.rr_conn_threshold;

	if (slpdt != NULL) {
		slpdt->set_rr_config(sl_rr_config);
	}

	if (sl_bindtab != NULL) {
		sl_bindtab->config_rr(is_rr_enabled());
	}
	if (is_rr_enabled()) {
		init_cleanup_thread();
	}
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
int
sl_servicegrp_t::sl_openconn(network::cid_t& cid)
{
	ASSERT(sl_bindtab != NULL);
	return (sl_bindtab->conn_open(cid));
}

//
// The function can get called in the interrupt context.
// Do not do any blocking calls in the function.
//
void
sl_servicegrp_t::sl_setstate(const network::cid_t& cid)
{
	network::cid_t	*newcid = NULL;

	// If GIF node set the state, else signal the handling thread to
	// to update the cid on the GIF node.
	//
	if (sl_pdt_exists())
		slpdt->setstate(cid, getpolicy(), network::CONN_ESTABLSHD);
	else {
		newcid = new(os::NO_SLEEP) network::cid_t;
		if (newcid == NULL) {
			return;
		}
		*newcid = cid;
		connct_post.connct_lock.lock();
		connct_post.cidlst.append_nosleep(newcid);
		connct_post.connct_cv.signal();
		connct_post.connct_lock.unlock();
	}

}	

void
sl_servicegrp_t::sl_resetall_usage_bit()
{
	slpdt->resetall_usage_bit();
}


//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
void
sl_servicegrp_t::sl_closeconn(network::cid_t& cid)
{
	ASSERT(sl_bindtab != NULL);
	sl_bindtab->conn_close(cid);
}


//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
void
sl_servicegrp_t::sl_add_bindinfo(const network::bind_t& bindp, uint_t iter)
{
	ASSERT(slpdt != NULL);
	//
	// No locks needed here. pdt_bindtab has its own locking. And
	// pdt_bindtab can't go away, since it is protected by
	// sl_servicegrplist_lock that the caller holds.
	//
	slpdt->pdt_add_bindinfo(bindp, iter);
}

void
sl_servicegrp_t::sl_add_pernodelst(uint_t iter)
{
	network::bind_t	bindp;

	ASSERT(slpdt != NULL);

	cid_lst->stop_timeout();
	for (uint_t indx = 1; indx < NODEID_MAX + 1; indx++) {
		cid_lst->get_pernode_lst(bindp, indx);
		slpdt->pdt_add_bindinfo(bindp, iter++);
	}
	cid_lst->handle_dispatch();
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
void
sl_servicegrp_t::sl_clean_bindinfo(nodeid_t node)
{
	ASSERT(slpdt != NULL);
	//
	// No locks needed here. pdt_bindtab has its own locking. And
	// pdt_bindtab can't go away, since it is protected by
	// sl_servicegrplist_lock that the caller holds.
	//
	slpdt->remove_bindinfo(node);
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
bool
sl_servicegrp_t::sl_setstate_binding(const network::inaddr_t& faddr,
    const nodeid_t node, network::bind_state_t state,
    boolean_t	is_node_registered)
{

	//
	// slpdt can be NULL as follows. set_primary_gifnode() is in
	// progress removing slpdt, while holding the PDT server
	// lock. This function is called however, from a timer expiration
	// on a proxy node that considers us one of the gifnodes. Since
	// there's no synchronization in general between the PDT server
	// and the proxy node, slpdt can be NULL during timer
	// expiration. Since the PDT server must later on rebuild slpdt,
	// which will then obtain the latest binding state from the proxy
	// node, a NULL slpdt is not an error condition.
	//
	if (slpdt != NULL) {
		//
		// No locks needed here. pdt_bindtab has its own locking. And
		// pdt_bindtab can't go away, since it is protected by
		// sl_servicegrplist_lock that the caller holds.
		//
		cid_elem_t	*tmp;
		network::inaddr_t tstaddr = faddr;
		//
		// Check for the client in the pernodelst. If a match is
		// found, do not remove the entry.
		//
		if (is_node_registered && is_generic_affinity() &&
		    ((state == network::BI_DEPRECATED) ||
		    (state == network::BI_CLOSED))) {
			if ((tmp = cid_lst->lookup_cid(tstaddr, node)) !=
			    NULL) {
				return (false);
			}
		}

		if (cid_lst != NULL) {
			cid_lst->clean_pernode_lst(node);
		}
		slpdt->pdt_setstate_binding(faddr, state);
	}
	return (true);
}

uint_t
sl_servicegrp_t::sl_input(network::cid_t& cid,
    network::cid_action_t& action, uint32_t& index, uint_t flag)
{
	uint_t node = NULLNODE;
	int ret;

	if (!slpdt) {
		//
		// No instance registered yet,
		// or no gifnode attached to this service.
		//
		action = network::CID_DROP;
		return (NULLNODE);
	}
	sl_frwd_list_lock.rdlock();
	node = slpdt->pdt_input(cid, getpolicy(), action, index, flag);
	sl_frwd_list_lock.unlock();

	if (is_rr_enabled() && action == network::CID_DISPATCH) {
		ASSERT(slrr != NULL);

		if (slrr->invalid_weights()) {
			action = network::CID_DROP;
			return (NULLNODE);
		}

		if (!(slpdt->incr_rr_count())) {
			cleanup_post.rr_cleanup_lock.lock();
			if (cleanup_post.cleanup_state != CLEANUP_RUNNING) {
				cleanup_post.cleanup_state = CLEANUP_START;
				cleanup_post.rr_cleanup_cv.signal();
			}
			cleanup_post.rr_cleanup_lock.unlock();
			NET_DBG(("Threshold on connection reached. \n"));
			NET_DBG(("Dropping the connection request. \n"));
			action = network::CID_DROP;
			return (NULLNODE);
		}
		node = slrr->next();
		ASSERT(node != NULLNODE);

		if (!is_strong_sticky()) {
			sl_frwd_list_lock.wrlock();
			ret = slpdt->insert_frwd_info(cid, node, index);
			sl_frwd_list_lock.unlock();
		} else {
			//
			// No extra locking needed, since binding table
			// has its internal locking.
			//
			ret = slpdt->insert_bind_info(cid, node);
		}
		if (ret != 0) {
			cleanup_post.rr_cleanup_lock.lock();
			cleanup_post.cleanup_state = CLEANUP_START;
			cleanup_post.rr_cleanup_cv.signal();
			cleanup_post.rr_cleanup_lock.unlock();
			NET_DBG(("Out of memory \n"));
			NET_DBG_RR(("Cleanup thread initiated\n"));
			node = NULLNODE;
			action = network::CID_DROP;
		}
	}
	//
	// For generic affinity add the client information in
	// the pernodelst which will be forwarded to the proxy node.
	//
	if ((action != network ::CID_DROP) &&
	    (is_generic_affinity()) &&
		((cid.ci_protocol == IPPROTO_UDP) ||
		(cid.ci_protocol == IPPROTO_AH) ||
		(cid.ci_protocol == IPPROTO_ESP))) {
		cid_elem_t	*newcid;
		if ((newcid = cid_lst->lookup_cid(cid.ci_faddr, node)) ==
		    NULL) {
			if ((newcid = cid_lst->get_cid_from_freelst()) !=
			    NULL) {
				newcid->cid = cid;
				cid_lst->append_lst(newcid, node);
			} else {
				NET_DBG(("Number of connections exceed"
				    "the maximum number of connections"
				    "for the timeout interval. Affinity"
				    "might be lost for client %s\n",
				    network_lib.ipaddr_to_string(
				    cid.ci_faddr)));
			}
		}
	}

	return (node);
}

void
sl_servicegrp_t::sl_stop_forward(const network::cid_t& cid, nodeid_t node)
{
	if (slpdt) {
		sl_frwd_list_lock.wrlock();
		slpdt->stop_forward(cid, getpolicy(), node);
		sl_frwd_list_lock.unlock();
	}
}


bool
sl_servicegrp_t::ip_equal(const network::service_sap_t &nsap)
{
	sl_serviceobj_t *sobj;

	//	ASSERT(sl_serviceobj_list_lock.lock_held());

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->ip_equal(nsap)) {
			return (true);
		}
	}

	return (false);
}

bool
sl_servicegrp_t::equal(const network::service_sap_t &nsap)
{
	sl_serviceobj_t *sobj;

	//	ASSERT(sl_serviceobj_list_lock.lock_held());

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);

	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->equal(nsap)) {
			return (true);
		}
	}

	return (false);
}

bool
sl_servicegrp_t::ip_matches(const network::service_sap_t &nsap)
{
	sl_serviceobj_t *sobj;

	sl_serviceobj_list_lock.rdlock();
	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->ip_matches(nsap)) {
			sl_serviceobj_list_lock.unlock();
			return (true);
		}
	}
	sl_serviceobj_list_lock.unlock();

	return (false);
}

bool
sl_servicegrp_t::matches(const network::service_sap_t &nsap)
{
	sl_serviceobj_t *sobj;

	sl_serviceobj_list_lock.rdlock();
	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->matches(nsap)) {
			sl_serviceobj_list_lock.unlock();
			return (true);
		}
	}
	sl_serviceobj_list_lock.unlock();

	return (false);
}

void
sl_servicegrp_t::sl_set_gifnode(const network::service_sap_t &ssap,
    nodeid_t node)
{
	sl_serviceobj_t *sobj;

	NET_DBG(("sl_servicegrp_t::sl_set_gifnode(node=%d)\n", node));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	sl_serviceobj_list_lock.wrlock();
	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->ip_equal(ssap)) {
		    NET_DBG(("  Gifnode is set to node %d for group %s\n",
			node, get_group_name()));
		    sobj->set_gifnode(node);
		}
	}
	sl_serviceobj_list_lock.unlock();

	if (is_strong_sticky()) {
		ASSERT(sl_bindtab != NULL);
		sl_bindtab->set_gifnodes(sl_find_all_gifnodes());
	}

}

nodeid_t
sl_servicegrp_t::sl_find_gifnode(const network::service_sap_t &ssap)
{
	sl_serviceobj_t *sobj;
	nodeid_t node = 0;

	sl_serviceobj_list_lock.rdlock();
	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->ip_equal(ssap)) {
			node = sobj->get_gifnode();
			break;
		}
	}
	sl_serviceobj_list_lock.unlock();
	return (node);
}

//
// Locking: sl_servicegrplist_lock.rdlock() protected
//
nodeid_array_t
sl_servicegrp_t::sl_find_all_gifnodes()
{
	sl_serviceobj_t *sobj;
	nodeid_array_t gnodes = 0;

	sl_serviceobj_list_lock.rdlock();

	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		SET_NODEID(gnodes, sobj->get_gifnode());
	}

	sl_serviceobj_list_lock.unlock();
	return (gnodes);
}

bool
sl_servicegrp_t::sl_pdt_exists()
{
	bool result;

	if (slpdt == NULL) {
		result = false;
	} else {
		result = true;
	}
	return (result);
}

void
sl_servicegrp_t::sl_pre_marshal_fwd(network::seqlong_t *cntseq, nodeid_t node)
{
	if (slpdt != NULL) {
		sl_frwd_list_lock.rdlock();
		slpdt->pre_marshal_fwd(*cntseq, node);
		sl_frwd_list_lock.unlock();
	}
}

void
sl_servicegrp_t::sl_gns_bind(bool as_gif,
    network::gns_bindseq_t *bindp, int &st_timeout)
{
	if (is_strong_sticky()) {
		if (as_gif) {
			st_timeout = 0;
			if (slpdt)
				slpdt->pdt_gns_bind(bindp);
		} else {
			st_timeout = sl_bindtab->get_timeout();
			sl_bindtab->gns_elems(bindp);
		}
	}
}

void
sl_servicegrp_t::sl_marshal_fwd(network::fwdseq_t *fwdinfo, nodeid_t node,
    Environment& e)
{
	if (slpdt != NULL) {
		sl_frwd_list_lock.rdlock();
		slpdt->marshal_fwd(*fwdinfo, node, e);
		sl_frwd_list_lock.unlock();
	}
}

void
sl_servicegrp_t::sl_marshal_pdt(network::pdtinfo_t *pdtinfo, Environment&)
{
	if (slpdt != NULL) {

		pdtinfo->group = sr_group;
		pdtinfo->policy = sr_policy;

		//
		// Convert to canonical form.
		//
		(void) slpdt->marshal_pdtinfo(*pdtinfo);
	}
}

void
sl_servicegrp_t::sl_marshal_sgrp(network::grpinfo_t *grpinfo, Environment& e)
{
	sl_serviceobj_t *sobj;
	uint_t cnt, j = 0;

	grpinfo->srv_nhash = (unsigned int)getnhash();
	grpinfo->srv_group = getgroup();
	grpinfo->srv_policy = getpolicy();
	grpinfo->srv_pdt = has_pdt();

	sl_get_sticky_config(grpinfo->srv_stconfig);

	sl_serviceobj_list_lock.rdlock();
	cnt = sl_serviceobj_list.count();

	//
	// If the list isn't long enough, have the caller try again.
	//
	if (cnt > grpinfo->srv_obj.length()) {
		sl_serviceobj_list_lock.unlock();
		e.exception(new sol::op_e(EAGAIN));
		return;
	}

	grpinfo->srv_obj.length(cnt);
	sl_serviceobjlist_t::ListIterator iter(sl_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		sobj->sl_marshal_sobj(&grpinfo->srv_obj[j++], e);
		//
		// marshal_sobj doesn't currently return an exception, but
		// we should detect memory problems and pass ENOMEM back
		// to the caller (someday).
		//
		if (e.exception()) {
			sl_serviceobj_list_lock.unlock();
			return;
		}
	}
	sl_serviceobj_list_lock.unlock();
}

void
sl_serviceobj_t::sl_marshal_sobj(network::srvinfo_t *srvinfo, Environment&)
{
	srvinfo->srv_gifnode = get_gifnode();
	srvinfo->srv_sap = getssap();
}

bool
sl_servicegrp_t::has_pdt()
{
	if (slpdt) {
		return (true);
	} else {
		return (false);
	}
}


void
sl_servicegrp_t::init_cleanup_thread()
{
	char *udp_stimeout;

#ifdef _KERNEL
	// Generic cleanup thread. Invoked when memory or connection
	// threshold is reached.
	cleanup_post.rr_cleanup_lock.lock();
	if (rr_thrid == NULL) {
		cleanup_post.cleanup_state = CLEANUP_CREATE;
		if (clnewlwp((void (*)(void *))cleanup_thread, this,
			MINCLSYSPRI, NULL, &rr_thrid) != 0) {
			NET_DBG_RR(("Cleanup thread could not be created\n"));
			cleanup_post.rr_cleanup_lock.unlock();
			return;
		}
	}
	cleanup_post.rr_cleanup_lock.unlock();
	NET_DBG_RR(("General cleanup started\n"));

	// Thread to post connect completion to the GIF node
	// to update the state of the connection.

	connct_post.connct_lock.lock();
	if (cnct_thrid == NULL) {
		connct_post.state = CLEANUP_CREATE;
		if (clnewlwp((void (*)(void *))cnct_disp, this,
		    0, NULL, &cnct_thrid) != 0) {
			NET_DBG_RR(("connect thread could not be created\n"));
			connct_post.connct_lock.unlock();
			return;
		}
	}
	connct_post.connct_lock.unlock();
#endif
	clconf_cluster_t *cl = clconf_cluster_get_current();
	udp_stimeout = (char *)clconf_obj_get_property((clconf_obj *)cl,
	    "udp_session_timeout");
	if (udp_stimeout != NULL) {
#ifdef _KERNEL
		clnet_rr_timeout = (clock_t)stoi(&udp_stimeout);
#else
		clnet_rr_timeout = (clock_t)atoi(udp_stimeout);
#endif
	}
	// Timeout thread to cleanup UDP connection
	if ((sl_rr_config.rr_flags & network::LB_RR_UDP)) {
		udp_timeout.rr_timeout_lock.lock();
		if (udp_timeout.rr_timeout_id == NULL) {
			udp_timeout.rr_timeout_id = timeout(timeout_func,
			    this, drv_usectohz((clnet_rr_timeout * 1000000)));
		}
		udp_timeout.rr_timeout_lock.unlock();
		NET_DBG_RR(("UDP cleanup thread started\n"));
	}
}

// Generic function to handle cleanup of the connections.
// The functions handles general cleanup and UDP timeout.
void
sl_servicegrp_t::handle_cleanup(int is_timeout)
{

	if (is_timeout && !is_strong_sticky()) {
		udp_timeout.rr_timeout_lock.lock();
	}

	if (is_strong_sticky()) {
		rr_clean_bind_info();
	} else {
		rr_clean_frwd_info(is_timeout);
	}

	if (is_timeout && !is_strong_sticky()) {
		udp_timeout.rr_timeout_id = timeout(timeout_func, this,
		    drv_usectohz((clnet_rr_timeout * 1000000)));
		udp_timeout.rr_timeout_lock.unlock();
	}
}

// Function to clean the frwd information.
// For TCP cleanup is on the state the connection is in and
// TCP connection can be in 3 state
// CONN_IDLE - The connection has been received
// CONN_SYN_RCVD - The SYN has been successfully processed and a forwarding
// entry has been created for the connection request.
// CONN_ESTABLSD- Connection has successfully completed the 3-way handshake and
// servicing node has accepted the connection.
// see sl_pdt::pdt_input on how the state is set.
// for UDP the cleanup is on the timeout.

void
sl_servicegrp_t::rr_clean_frwd_info(int is_timeout)
{
	network::fwdseq_t *fwdp;
	network::seqlong_t *cntseqp;
	uint_t i;
	nodeid_t maxnodeid;
	Environment e;


	if (!sl_pdt_exists()) {
		return;
	}

	fwdp = new (os::NO_SLEEP) network::fwdseq_t(NODEID_MAX);
	if (fwdp == NULL) {
		return;
	}
	network::fwdseq_t& fwdinfo = *fwdp;
	fwdinfo.length(NODEID_MAX);

	//
	// Initialize our cid sequences (empty)
	//
	for (i = 0; i < NODEID_MAX; i++) {
		fwdinfo[i].fw_cidseq.length(0);
		fwdinfo[i].fw_node = 0;
		fwdinfo[i].fw_ncid = 0;
	}
	//
	// cntseq is a temporary integer sequence used to hold the forwarding
	// list counts that are needed to size the fwdp fw_cidseq
	// sequences.
	//
	cntseqp = new (os::NO_SLEEP) network::seqlong_t(NODEID_MAX);
	if (cntseqp == NULL) {
		delete fwdp;
		return;
	}
	network::seqlong_t& cntseq = *cntseqp;
	cntseq.length(NODEID_MAX);

	for (i = 0; i < NODEID_MAX; i++) {
		cntseq[i] = 0;
	}

	sl_pre_marshal_fwd(cntseqp, 0);
	for (i = 0; i < NODEID_MAX; i++) {
		fwdinfo[i].fw_cidseq.length(cntseq[i]);
	}

	sl_marshal_fwd(fwdp, 0, e);

	if (e.exception()) {
		delete fwdp;
		delete cntseqp;
		return;
	}

	//
	// Now that we've finished, shrink the cid sequences down to
	// include only the forwarding information stored.
	//

	for (maxnodeid = 0, i = 0; i < NODEID_MAX; i++) {
		fwdinfo[i].fw_cidseq.length((uint_t)fwdinfo[i].fw_ncid);
		if ((fwdinfo[i].fw_ncid > 0) && (maxnodeid <= i)) {
			maxnodeid = i + 1;
		}
	}

	fwdinfo.length(maxnodeid);
	int remove;

	for (uint_t j = 0; j < fwdinfo.length(); j++) {
		//
		// We'll may get back empty sequence elements if we
		// specified a fwdnode (the prior elements will
		// be empty).
		//

		if ((fwdinfo[j].fw_ncid == 0)) {
			continue;
		}

		for (i = 0; i < (uint_t)fwdinfo[j].fw_ncid; i++) {
			remove = is_timeout  ?
			    fwdinfo[j].fw_cidseq[i].ci_dbit :
			    fwdinfo[j].fw_cidseq[i].ci_state;
			//
			// If conn_threshold or memory threshold is reached
			// clean all embryo TCP connections, else
			// clean unused UDP connections
			//
			if (((remove < network::CONN_ESTABLSHD) &&
			    !is_timeout &&
			    fwdinfo[j].fw_cidseq[i].ci_protocol ==
			    IPPROTO_TCP) || (!remove && is_timeout &&
			    fwdinfo[j].fw_cidseq[i].ci_protocol ==
			    IPPROTO_UDP)) {
					NET_DBG_RR((
					    "stop for addr %s, port %d\n",
					    network_lib.ipaddr_to_string(
					    fwdinfo[j].fw_cidseq[i].ci_faddr),
					    fwdinfo[j].fw_cidseq[i].ci_fport));
					sl_stop_forward(fwdinfo[j].fw_cidseq[i],
					    fwdinfo[j].fw_node);
			}
		}
	}
	sl_resetall_usage_bit();
	delete fwdp;
	delete cntseqp;
}

void
sl_servicegrp_t::rr_clean_bind_info()
{
	network::gns_bindseq_t *bindp;

	bindp = new network::gns_bindseq_t;

	if (sl_pdt_exists()) {
		uint_t length;
		slpdt->pdt_gns_bind(bindp);
		length = bindp->length();

		NET_DBG_RR(("bindlength is %d\n", length));
		for (uint_t i = 0; i < length; i++) {
			if ((*bindp)[i].gb_state == network::BI_UNINITIALISED) {
				slpdt->pdt_setstate_binding((
				    *bindp)[i].gb_faddr,
				    network::BI_CLOSED);
			}
		}
	}

	delete bindp;
}

void
sl_servicegrp_t::cancel_udp_timeout()
{
	timeout_id_t	tid;

	udp_timeout.rr_timeout_lock.lock();
	tid = udp_timeout.rr_timeout_id;
	udp_timeout.rr_timeout_id = 0;
	udp_timeout.rr_timeout_lock.unlock();
	(void) untimeout(tid);
	NET_DBG_BIND(("Timeout cancelled\n"));
}

void
sl_servicegrp_t::connect_call()
{

	network::cid_t  *cid;
	network::service_sap_t ssap;
	nodeid_t		gifnode;
	Environment		e;

	connct_post.cidlst.atfirst();
	while ((cid = connct_post.cidlst.get_current()) != NULL) {
		(void) connct_post.cidlst.erase(cid);
		connct_post.connct_lock.unlock();
		ssap.protocol = cid->ci_protocol;
		ssap.laddr = cid->ci_laddr;
		ssap.lport = cid->ci_lport;
		if ((gifnode = sl_find_gifnode(ssap)) != 0) {
			if (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(gifnode))) {
				network::mcobj_ptr gif =
				    mcobjp->mc_node_to_mcobj(gifnode);
				gif->mc_setstate_conn(*cid,
				    network::CONN_ESTABLSHD, e);
				e.clear();
			}
		}
		connct_post.connct_lock.lock();
		connct_post.cidlst.atfirst();
	}
	connct_post.connct_lock.unlock();
}
