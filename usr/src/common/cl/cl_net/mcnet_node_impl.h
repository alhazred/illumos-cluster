/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MCNET_NODE_IMPL_H
#define	_MCNET_NODE_IMPL_H

#pragma ident	"@(#)mcnet_node_impl.h	1.39	08/05/20 SMI"

//
// This file defines the implementation class routines that
// correspond to the Service Access Point Server interface
// (interface network::SAPServer) described in network.idl
//

#include <sys/ksynch.h>
#include <sys/os.h>

#include <h/network.h>
#include <orb/object/adapter.h>

#include <cl_net/netlib.h>

#include <repl/service/replica_tmpl.h>
#include <cl_net/pdts_prov_impl.h>

extern "C" void debug_enter(char *);

class pdts_prov_impl_t;

//
// Since we don't wish to tie the deferred task to the
// mcnet_node_impl object-- which must be deleted before the
// task is actually executed--we must therefore create a new
// object that has enough information to do the cleanup.
// Thus, we remember the node id of the node that
// failed, together with whether or not it was a gifnode.
// This class is also used for doing node cleanup at the secondaries;
// it will not distribute PDT changes to the slaves,
// since that will have already been done by the primary.
// Note that the execute() method is the workhorse.
//
class nodes_cleanup : public defer_task {
public:
	nodes_cleanup() { node = 0; isgif = false; local_pdtsp = NULL;
			isprimary = false; }
	~nodes_cleanup() {}
	void execute();

	void set_data(nodeid_t nid, bool gifnode, bool we_are_primary)
		{ node = nid; isgif = gifnode; isprimary = we_are_primary; }
	void set_pdtsp(pdts_impl_t *pdtsp) { local_pdtsp = pdtsp; }

	bool is_primary() { return isprimary; }
	nodeid_t get_node() { return node; }
	bool is_gifnode() { return isgif; }

private:
	nodeid_t	node;
	bool		isgif;
	pdts_impl_t	*local_pdtsp;
	bool		isprimary;
};

class mcnet_node_impl_t: public mc_replica_of < network::mcnet_node > {

public:

	// Constructor for primary
	mcnet_node_impl_t(pdts_prov_impl_t *server);

	// Constructor for secondary
	mcnet_node_impl_t(network::mcnet_node_ptr ref,
		pdts_prov_impl_t *server);

	// destructor
	~mcnet_node_impl_t();

	void _unreferenced(unref_t);

	// Copy constructor.
	mcnet_node_impl_t(const mcnet_node_impl_t &);
	// Assignment operator.
	mcnet_node_impl_t &operator = (mcnet_node_impl_t &);

	void set_cbref(network::mcobj_ptr &mcobjref);
	network::mcobj_ptr get_cbref();

	bool is_gifnode();
	void set_gifnode(bool gif);

	nodeid_t		node;	// needed by deregister_gifnode

	// Checkpoint accessor function.
	network::ckpt *get_checkpoint_network_ckpt();

private:
	network::mcobj_var	cbref_v;	// object reference to mcobj
	bool			gif_node;

	// This holds a pointer to the deferred task cleanup object, which
	// will be enqueued on the deferred task queue.
	nodes_cleanup		*mcnet_nodes_cleanup_task;

	// The HA provider object of which this object is a replica.
	pdts_prov_impl_t	*my_provider;
};

#include <cl_net/mcnet_node_impl_in.h>

#endif	/* _MCNET_NODE_IMPL_H */
