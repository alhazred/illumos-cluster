/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _BIND_TABLE_H
#define	_BIND_TABLE_H

#pragma ident	"@(#)bind_table.h	1.10	08/05/20 SMI"

#include <h/network.h>
#include <sys/os.h>

#undef nil // <inet/common.h> redefines nil

#include <sys/disp.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <inet/common.h>
#include <inet/led.h>
#include <netinet/ip6.h>
#include <inet/ip.h>
#include <inet/tcp.h>
#include <inet/mi.h>

//
// Value that indicates "no timeout"
//
#define	BIND_TIMEOUT_NONE	-1

class bind_elem_t;
class proxy_bind_table_t;
class closeconn_timer;
class closeconn_timer_manager;

//
// Structure to record what needs to be sent to the GIF nodes by the proxy
// node in managing binding entries.
//
class bind_event : public defer_task {
public:
	bind_event(bind_elem_t *elem);
	void execute();

	proxy_bind_table_t	*bt;
	uint_t			elem_index;
	bind_elem_t		*elem;
};

//
// Binding entry used on GIF nodes
//
class gif_bind_elem_t : public _DList::ListElem {
public:
	gif_bind_elem_t(network::inaddr_t& faddr, nodeid_t node);

	const char *to_string();

	network::inaddr_t	bi_faddr;	// Client IP address
	nodeid_t		bi_nodeid;	// Server node client binds to
	network::bind_state_t	bi_state;	// Binding state
};
typedef IntrList<gif_bind_elem_t, _DList> gif_bind_list_t;

//
// Binding table on GIF nodes. Consists of an array of locks, and a
// parallel array of gif_bind_list_t.
//
class gif_bind_table_t {
public:
	gif_bind_table_t(uint_t hashsize);
	~gif_bind_table_t();

	// Return nodeid faddr is bound to. 0 if faddr is not found.
	nodeid_t lookup(network::inaddr_t& faddr,
	    network::bind_state_t& state);

	//
	// Create a binding. If one exists, update with nodeid.
	// Return true if a new binding is actually created.
	//
	bool create(network::inaddr_t& faddr, nodeid_t node);

	//
	// Set state of the specified binding entry.
	// If BI_CLOSED, entry will be deleted.
	// If BI_DEPRECATED, entry is marked deprecated.
	// If BI_CONNECTED, entry is marked connected.
	// See sl_pdt::pdt_input for how these states are used.
	// Returns true if faddr is found
	//
	bool setstate(const network::inaddr_t& faddr, network::bind_state_t);

	// Remove all bindings for a server node
	void remove_node(nodeid_t node);

	// Remove all bindings, but leave table intact
	void clean_all();

	void gns_elems(network::gns_bindseq_t *);

private:
	gif_bind_elem_t *lookup_elem(const network::inaddr_t& faddr);

	uint_t			bind_hashsize;
	gif_bind_list_t		*bind_list;
	os::rwlock_t		*bind_list_lock;

	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	gif_bind_table_t(const gif_bind_table_t&);
	gif_bind_table_t& operator = (gif_bind_table_t&);
};


//
// Timer structure that records when an entry should expire and the
// connection-close that starts the timer.
//
// Timers are marked with a timestamp of the start time. The worker thread
// in closeconn_timer_manager that handles expirations adds the timeout
// period to the start time to determine if a timer has expired.
//
class closeconn_timer : public _DList::ListElem {
public:
	closeconn_timer(bind_elem_t *elem);

	bind_elem_t	*elem;		// Binding this timer is for
	uint_t		elem_index;
	time_t		timestamp;	// Time when timer starts
};
typedef IntrList<closeconn_timer, _DList> closeconn_timer_list_t;


//
// Beinding entry used on proxy nodes.
//
class bind_elem_t : public _DList::ListElem {
public:
	bind_elem_t(network::inaddr_t& faddr, nodeid_t node);
	~bind_elem_t();

	const char *to_string();

	network::inaddr_t	bi_faddr;	// Client IP address
	nodeid_t		bi_nodeid;	// Server node bound to
	network::service_sap_t	bi_ssap;
	uint_t			bi_count;	// Connection count
	ushort_t		bi_udp_usage;	// Usage for UDP clients
	nodeid_array_t		bi_interested;	// GIF nodes interested
	network::bind_state_t	bi_state;	// Binding state
	bind_event		bi_event;
	closeconn_timer		bi_timer;	// Associated timer
};

//
// The IntrList class must be used so that adding entries do not result in
// a memory allocation -- since we'll be adding elements in network
// interrupt context on the GIF node.
//
typedef IntrList<bind_elem_t, _DList> bind_list_t;

//
// Binding table on proxy nodes. Consists of an array of locks, and a
// parallel array of bind_list_t.
//
class proxy_bind_table_t {
public:
	proxy_bind_table_t(uint_t hashsize, const network::sticky_config_t &,
	    bool rr);
	~proxy_bind_table_t();

	void config_timeout(int timeout);
	int get_timeout();

	//
	// When the list of GIF nodes change for the associated service,
	// this table needs to know about it, through this call. The list
	// is used to decide which nodes to notify when an element is
	// being removed.
	//
	void set_gifnodes(nodeid_array_t gifnodes);

	//
	// Handler when a connection is established. That is, when TCP
	// takes the connection to the ESTABLISHED state.
	//
	int conn_open(network::cid_t& cid, ushort_t udp_usage = 0);

	//
	// Handler when a connection is closed. This is called when TCP
	// takes a connection into the CLOSED state.
	//
	void conn_close(network::cid_t& cid);

	//
	// Force timeout of all entries, except when BIND_TIMEOUT_NONE has
	// been configured as the timeout value.
	//
	void timeout_all();

	void event_handler(bind_elem_t *elem, uint_t index);

	//
	// Timeout handler when the named timer expires. Timer contains
	// information about which binding has timed out.
	//
	void timeout_handler(closeconn_timer *);

	//
	// Return a list of all binding elements. new_gifnode will be
	// registered as an "interested" gifnode in the gifnodes field of
	// the elements returned, and will get notified when these
	// elements are removed.
	//
	void get_elems(nodeid_t new_gifnode, network::bind_t *);

	// Return a list of elements for print_net_state
	void gns_elems(network::gns_bindseq_t *);

	void config_rr(bool is_rr);

	//
	// Function for generic_affinity
	//
	void add_udpsticky_timer();
	void remove_udpsticky_timer();
	void udp_cleanup_handler();
	void final_udp_cleanup_handler();
	void set_generic_affinity(bool is_gen_aff);

private:
	bind_elem_t *lookup_elem(network::inaddr_t& faddr);
	bind_elem_t *create_elem(network::inaddr_t& faddr, nodeid_t node);
	void remove_elem(bind_elem_t *elem, uint_t index);

	void add_closeconn_event(bind_elem_t *elem);
	void add_closeconn_timer(bind_elem_t *elem);

	nodeid_t	my_nodeid;
	nodeid_array_t	bind_gifnodes;

	int		bind_timeout;
	bool		rr_enabled;

	os::mutex_t	udp_timeout_lock;
	timeout_id_t	udp_tid;
	bool		generic_affinity;

	closeconn_timer_manager *timerman;

	uint_t		bind_hashsize;
	bind_list_t	*bind_list;
	os::rwlock_t	*bind_list_lock;

	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	proxy_bind_table_t(const proxy_bind_table_t&);
	proxy_bind_table_t& operator = (proxy_bind_table_t&);
};


//
// Timer management class. Proxy nodes use this to time out binding
// entries for sticky service with a positive timeout period. Each service
// group has one of these.
//
// Assumes (exploits) that timeouts are added in order. That is, when
// add_timeout() is called, it is always for an later expiration than any
// add_timeout() calls made earlier.
//
// to avoid having to wake up too often (and hogging the CPU), this object
// makes sure it doesn't sleep for less than (min_sleep) secs between
// successive timeouts. This means that timeout can go off at most
// (min_sleep) secs after the designated time in add_timeout().
//
class closeconn_timer_manager {
public:
	closeconn_timer_manager(proxy_bind_table_t *);
	~closeconn_timer_manager();

	//
	// Changes timeout period used to time out timers. Change takes
	// effect immediately. New value smaller than 0 will set it to 0.
	// A zero timeout means that timers expire right away as soon as
	// they are added. This is currently not used.
	//
	void config_timeout(time_t timeout_period);

	//
	// Setup a timeout that goes off ''secs'' seconds later. The timer
	// structure is passed to proxy_bind_table_t::timeout_handler() at
	// expiration. ''secs'' is set by timerman.
	//
	void add_timeout(closeconn_timer *timer);

	//
	// Remove a timeout previously setup with add_timeout(). Caller
	// must be prepared to handle an expiration that happens when this
	// call is in progress. (The expiration is processed on a
	// different thread, so the caller won't deadlock itself.)
	//
	void remove_timeout(closeconn_timer *timer);

	//
	// Expires all registered timeouts regardless of when they are
	// supposed to expire.
	//
	void purge_all();

	//
	// Internal routines for multiplexing the kernel timeout facility
	//
	void timeout_handler();
	void worker_thread();

private:
	// Our own timeout handler routine
	void process_timeouts();

	// Tell worker thread to timeout all timers
	void purge_all_timeouts();

	// Schedule a kernel timeout at time texpire
	void schedule_timeout(time_t texpire);

	// Cancel any outstanding kernel timer
	void cancel_timeout();

#define	WORKER_MIN_SLEEP	((time_t)5)	// Worker wake-up interval

#define	WORKER_TIMED_OUT	0x01	// Timeout happened
#define	WORKER_MORE_WORK	0x02	// New timeout scheduled
#define	WORKER_PURGE_ALL	0x04	// Time out all timers
#define	WORKER_EXIT		0x08	// Worker should exit

	proxy_bind_table_t *bindtab;	// Associated binding table
	time_t		min_sleep;	// Minimum sleep interval (secs)
	time_t		timeout_period;	// Timeout period for all timers

	closeconn_timer_list_t timerlist;	// Timer list
	uint_t		worker_flags;		// Worker actions
	timeout_id_t	tid;			// Internal timeout id
	time_t		tid_expire;		// Next expiration time
	os::mutex_t	worker_lock;
	os::condvar_t	worker_cv;

#ifdef _KERNEL
	klwp_id_t	worker_id;		// Timer processing thread
#else
	thread_t	worker_id;
#endif
};

#include <cl_net/bind_table_in.h>

#endif	/* _BIND_TABLE_H */
