//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)perf_policy.cc 1.49	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <sys/os.h>
#include <repl/service/replica_tmpl.h>
#include <sys/disp.h>
#include <h/repl_pxfs.h>
#include <sys/time.h>

#include <nslib/ns.h>
#include <h/network.h>
#include <cl_net/perf_policy.h>
#include <cl_net/lbobj.h>
#include <orb/fault/fault_injection.h>
#include <orb/infrastructure/clusterproc.h>

//
// Load-based load balancing
// okay is set to false if the constructor fails.
// In that case the caller must delete this object.
// Otherwise, the object deletes itself when it is unreferenced.
//

// Primary constructor
perf_policy::perf_policy(bool &okay) :
	mc_replica_of<perf_mon_policy>(
		pdts_impl_t::get_ha_provider())
{
	initialize(okay);

	Environment e;
	// Start up the balancing thread.
	perf_policy::unfreeze_primary(e);
	e.clear();
}

// Secondary constructor
perf_policy::perf_policy(bool &okay, network::perf_mon_policy_ptr obj) :
	mc_replica_of<network::perf_mon_policy>(obj)
{
	initialize(okay);
}

void
perf_policy::initialize(bool &okay)
{
	// Set the HA cookie to point to the lbobj_i, for use by
	// the checkpoint.
	_handler()->set_cookie((lbobj *)this);

	balance_tid = NULL;
	notify_frequency = 0;
	thread_done = false;
	obj_done = false;
	okay = true;

	// Set some reasonable defaults.
	// Reduce weight by 1 for scale load average points
	scale_rate = 100;
	// Don't decrease distribution if load is below floor points
	floor = 1000;
	// Keep weights between min_weight and max_weight
	min_weight = 10;
	max_weight = 200;
	quiet_width = 100;

	Environment e;
	curr_info = NULL;

	for (int i = 0; i < NODEID_MAX+1; i++) {
		node_loads[i] = 0;
	}
}

//
// Destructor for perf_policy.
// Note: destructor will block until balancing thread is finished with object.
//
perf_policy::~perf_policy()
{
	// Stop the balancing thread
	Environment e;
	perf_policy::freeze_primary(e);
	e.clear();
	delete curr_info;
}

// Checkpoint to a new secondary
void
perf_policy::add_secondary(network::ckpt_ptr ckptp, Environment &e)
{
	network::perf_mon_policy_var this_lb = get_objref();

	// Checkpoint the user policy object
	network::perf_mon_policy::perf_mon_parameters_t parameters;
	get_parameters(parameters, e);
	if (!e.exception()) {
		ckptp->ckpt_set_perf_mon_parameters(this_lb, parameters, e);
	}
	e.clear();
}

// Called by the UI to get the current distribution
network::dist_info_t *
perf_policy::get_distribution(Environment &_environment)
{
	network::dist_info_t *dist = get_distribution_ii(false, _environment);
	return (dist);
}

// Returns the current policy
network::lb_specifier_t
perf_policy::get_policy(Environment &)
{
	datalock.lock();
	network::lb_specifier_t policy_out = policy;
	datalock.unlock();
	return (policy_out);
}

// Get the object reference
network::lbobj_i *
perf_policy::get_objref_ii()
{
	return (get_objref());
}

//
// Set the load for the specified node.
//
void
perf_policy::set_load_info(nodeid_t nodeid, const node_metrics &metrics,
	Environment &_environment)
{
	datalock.lock();
	if (nodeid == 0 || nodeid > NODEID_MAX) {
		_environment.exception(new sol::op_e(EINVAL));
		// metrics.load is never < 0
	} else if (metrics.load > TOOBIG) {
		_environment.exception(new sol::op_e(EINVAL));
	} else {
		node_loads[nodeid] = metrics.load;
	}
	datalock.unlock();
}

// This is the thread that calls balance_load periodically.
void
perf_policy::balance_thread()
{
	datalock.lock();
	for (;;) {

		// Note that this object is deleted by the load balancer when
		// all the references go away.  This thread must make sure it
		// doesn't try to invoke the object after it has been deleted.
		// Thus, the communication and locking is a bit tricky here.
		// The lock is held while the object is being used, to
		// prevent the object from being deleted while we're using it.
		// The destructor sets obj_done when it wishes to delete it.
		// Then the thread sets thread_done when it detects obj_done
		// and is finished with the object.  This keeps the object
		// from disappearing from underneath this thread, which is
		// necessary because this method is in the object, as are
		// the locks.

		if (obj_done) {
			// Object wants to delete itself.
			thread_done = true;
			// We signal that we're done with the object.
			thread_done_cv.broadcast();
			break;
		}

		unsigned int freq = notify_frequency;
		if (freq == 0) {
			// Wait 5 seconds and see if initialized.
			freq = 5;
		} else {
			// Balance the load
			balance_load();
		}

		// Sleep for freq seconds, or until wakened.

		os::systime to;
		to.setreltime((long)freq * MICROSEC);
		(void) sleep_cv.timedwait(&datalock, &to);
	}
	datalock.unlock();
}

// Use the collected data to balance the load.
void
perf_policy::balance_load()
{
	ASSERT(datalock.lock_held());
	bool changed = false;
	uint32_t num_nodes = 0;
	if (curr_info != NULL) {
		num_nodes = curr_info->nodes.length();
		// If curr_info is NULL, will load new curr_info below.
	}

	uint32_t tot_load = 0;
	uint32_t num_up = 0;
	uint32_t i;
	for (i = 1; i <= num_nodes; i++) {
		if (curr_info->nodes[i] == network::instance_up &&
		    node_loads[i] > 0) {
			tot_load += (uint32_t)node_loads[i];
			num_up++;
		}
	}
	if (num_up == 0) {
		// Nothing to do;
		// Try getting some nodes for next time.
		Environment e;
		curr_info = get_distribution_ii(true, e);
		e.clear();
		return;
	}

	// Strategy: if a node's load is above the average,
	// decrease its fraction.
	// If a node's load is below the average, decrease its fraction.
	// The previous load is also stored, so in the future the load can
	// be adjusted based on the trend.

	for (i = 1; i <= num_nodes; i++) {
		if (curr_info->nodes[i] == network::instance_up &&
		    node_loads[i] > 0) {
			int delta = (((int)node_loads[i] * (int)num_up -
				(int)tot_load) / (int)num_up);
			// Delta is how far from the average this node's load
			// is; positive means too much load.

			if (delta < (int)quiet_width &&
					-delta < (int)quiet_width) {
				delta = 0;
			} else if (node_loads[i] < floor) {
				// Don't take load away from an unloaded node
				if (delta > 0) {
					delta = 0;
				}
			}

			int new_weight = (int)curr_info->weights[i] -
				delta / (int)scale_rate;
			if (new_weight < (int)min_weight) {
				new_weight =  (int)min_weight;
			} else if (new_weight > (int)max_weight) {
				new_weight =  (int)max_weight;
			}
			if (curr_info->weights[i] != (uint32_t)new_weight) {
				curr_info->weights[i] = (uint32_t)new_weight;
				changed = true;
			}
			ASSERT(curr_info->weights[i] != 0);
		}
	}

	// Tell PDT server to distribute this new table

set_distribution:

	Environment e;
	if (changed) {
		network::ckpt_ptr ckptp = get_checkpoint();
		network::lbobj_i_var this_lb = get_objref();
		set_distribution_ii(*curr_info, ckptp, this_lb, true, e);
	}

	if (e.exception()) {
		if (network::weighted_lbobj::state_changed::_exnarrow(
				e.exception())) {
			e.clear();
			delete curr_info;
			curr_info = get_distribution_ii(true, e);
			if (e.exception()) {
				curr_info = NULL;
				// Will try again next time through.
				e.clear();
				return;
			}
			// Try again, with the new distribution.
			goto set_distribution;
		} else {
			e.exception()->print_exception("perf_policy");
			e.clear();
		}
	}
}

// Function started by new thread.
void
perf_policy::lb_thread(void *arg)
{
	//
	// cast to perf_policy object
	// Note that since this is a new thread, it is running as a static
	// function, and can't access the class members directly.
	//
	perf_policy *obj = (perf_policy *)arg;
	obj->balance_thread();
}

void
#ifdef DEBUG
perf_policy::_unreferenced(unref_t cookie)
#else
perf_policy::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));

	delete this;
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		"load balancer deleted");
}

//
// Modify the load-balancer parameters
// Note: frequency of 0 will stop the balancer.
// Note: changing from a longer to shorter frequency won't wake up the
// balancer any sooner.
//
void
perf_policy::set_parameters(const perf_mon_parameters_t &parameters,
	Environment &e)
{

	datalock.lock();
	set_parameters_int(parameters, true);

	FAULTPT_NET(FAULTNUM_NET_SET_PARM_B, FaultFunctions::generic);

	network::ckpt_ptr ckptp = get_checkpoint();
	network::perf_mon_policy_var rt = get_objref();
	ckptp->ckpt_set_perf_mon_parameters(rt, parameters, e);
	e.clear();
	datalock.unlock();

	FAULTPT_NET(FAULTNUM_NET_SET_PARM_A, FaultFunctions::generic);

}

//
// Internal function to set parameters with lock held or not.
//
void
perf_policy::set_parameters_int(const perf_mon_parameters_t &parameters,
	bool locked)
{

	if (!locked) {
		datalock.lock();
	}

	notify_frequency = parameters.frequency;
	scale_rate = parameters.scale_rate;
	min_weight = parameters.min_weight;
	max_weight = parameters.max_weight;
	quiet_width = parameters.quiet_width;
	floor = parameters.floor;

	if (!locked) {
		datalock.unlock();
	}
}

// Get the load-balancer parameters.
void
perf_policy::get_parameters(
	network::perf_mon_policy::perf_mon_parameters_t &params, Environment &)
{
	datalock.lock();
	params.frequency = notify_frequency;
	params.scale_rate = scale_rate;
	params.min_weight = min_weight;
	params.max_weight = max_weight;
	params.quiet_width = quiet_width;
	params.floor = floor;
	datalock.unlock();
}

// Start the primary thread.
void
perf_policy::unfreeze_primary(Environment &)
{
	datalock.lock();
	obj_done = false;
	thread_done = false;
	ASSERT(balance_tid == NULL);

#ifdef _KERNEL
	if (clnewlwp((void (*)(void *))perf_policy::lb_thread,
	    this, MINCLSYSPRI, NULL, &balance_tid) != 0) {
#else
	if (clnewlwp(perf_policy::lb_thread, this, 0, NULL,
	    &balance_tid) != 0) {
#endif
		//
		// SCMSGS
		// @explanation
		// The system has run out of resources that is required to
		// create a thread. The system could not create the load
		// balancer thread.
		// @user_action
		// The service group is created with the default load
		// balancing policy. If rebalancing is required, free up
		// resources by shutting down some processes. Then delete the
		// service group and re-create it.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
			"load balancer thread failed to start for %s",
			servicegrp->get_group_name());
		// Return exception??
	}
	datalock.unlock();
}

void
perf_policy::freeze_primary_prepare(Environment &)
{
}

// Stop the primary threads
void
perf_policy::freeze_primary(Environment &)
{
	datalock.lock();
	// Setting obj_done indicates that the object wants the thread to die.
	obj_done = true;
	if (balance_tid != NULL) {
		// Wake up the thread if it's sleeping.
		sleep_cv.broadcast();
		// Now wait until the thread actually finishes.
		while (!thread_done) {
			thread_done_cv.wait(&datalock);
		}
		balance_tid = NULL;
	}

	//
	// At this point, the thread is exiting and will no longer use the
	// object, so the object can be deleted.
	//

	datalock.unlock();
}

//
// Checkpoint accessor function.
//
network::ckpt_ptr
perf_policy::get_checkpoint()
{
	return ((pdts_prov_impl_t*)(get_provider()))->
	    get_checkpoint_network_ckpt();
}
