//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)net_debug.cc	1.16	08/05/20 SMI"

#include <cl_net/net_debug.h>

//
// DBGBUFS is defined for both DEBUG and non-DEBUG (release)
// builds.  Through make options (such as EXTRA_CCFLAGS=-DNO_DBGBUFS)
// the debug buffers won't be compiled in.
//
#if defined(DBGBUFS) && !defined(NO_DBGBUFS)

//
// The debug buffers are always created (true means the buffer is
// created in the constructor rather than upon first use, which is
// the default).
//
// Debug buffer for all other networking info.
dbg_print_buf net_dbg(0x8000, UNODE_DBGOUT_ALL, true);	// size 32768 bytes
// Debug buffer for forwarding list info ONLY.
dbg_print_buf net_dbg_fwd(0x10000, UNODE_DBGOUT_ALL, true); // size 65536 bytes
// Debug buffer for binding table info ONLY. size == 65536 bytes
dbg_print_buf net_dbg_bind(0x10000, UNODE_DBGOUT_ALL, true);
// Debug buffer for RR info ONLY. size == 65536 bytes
dbg_print_buf net_dbg_rr(0x10000, UNODE_DBGOUT_ALL, true);

#ifdef SCTP_SUPPORT
// Debug buffer for connection table info ONLY. size == 65536 bytes
dbg_print_buf net_dbg_ct(0x10000, UNODE_DBGOUT_ALL, true);
#endif // SCTP_SUPPORT

#ifdef DEBUG
uint32_t clnet_net_dbg = 1;		// net debugging ON by default
uint32_t clnet_net_dbg_fwd = 1;		// forwarding list ON by default
uint32_t clnet_net_dbg_bind = 1;	// binding table ON by default
uint32_t clnet_net_dbg_rr = 1;
#ifdef SCTP_SUPPORT
uint32_t clnet_net_dbg_ct = 1;		// connection table ON by default
#endif  // SCTP_SUPPORT
#else	// non-debug kernel mode
uint32_t clnet_net_dbg = 1;		// net debugging ON by default
uint32_t clnet_net_dbg_fwd = 0;		// forwarding list OFF by default
uint32_t clnet_net_dbg_bind = 0;	// binding table OFF by default
uint32_t clnet_net_dbg_rr = 0;
#ifdef SCTP_SUPPORT
uint32_t clnet_net_dbg_ct = 0;		// connection table OFF by default
#endif  // SCTP_SUPPORT
#endif  // DEBUG

#endif	// defined(DBGBUFS) && !defined(NO_DBGBUFS)
