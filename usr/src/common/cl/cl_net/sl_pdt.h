/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SL_PDT_H
#define	_SL_PDT_H

#pragma ident	"@(#)sl_pdt.h	1.44	08/09/07 SMI"

//
// Slave side pdt module
//

#include <h/network.h>
#include <sys/os.h>
#include <sys/taskq.h>
#include <cl_net/netlib.h>
#include <cl_net/bind_table.h>


class cid_elem_t :public _DList::ListElem {
public :
	cid_elem_t();
	~cid_elem_t();
	network::cid_t	cid;
};

typedef IntrList<cid_elem_t, _DList> cidlst_t;
//
// Entries used by the GIF node for generic affinity to store the UDP
// client information to forward to the proxy node.
//
struct pernode_connlst {
	os::rwlock_t	cidlst_lock;
	cidlst_t	pernode_cidlst;
};

//
// Handles per node list of UDP clients for generic affinity. Consists an
// array of pernode_connlst
//
class affinity_lst {
public:
	affinity_lst();
	~affinity_lst();
	cid_elem_t  *get_cid_from_freelst();
	void get_pernode_lst(network::bind_t& bindp, nodeid_t node);
	cid_elem_t *lookup_cid(network::inaddr_t& faddr, nodeid_t nodeid);
	void append_lst(cid_elem_t *cid, nodeid_t node);
	void handle_dispatch(bool donot_free = 0);
	void dispatch_conn_node(nodeid_t node, bool donot_free);
	void add_to_freelist(nodeid_t node, network::fwd_t *fwdp);
	void clean_pernode_lst(nodeid_t node);
	void stop_timeout();
private:
	struct pernode_connlst pernodelst[NODEID_MAX + 1];
	os::mutex_t	clnt_freelst_lock;
	cidlst_t	conn_freelst;
	uint_t		clnt_freelst_count;
	os::mutex_t	cnct_dispatch_lock;
	timeout_id_t	cnct_dispatch_id;
};

class dispatch_conn :  public defer_task {
public:
	dispatch_conn();
	void execute();

	affinity_lst *alst;
	nodeid_t	node;
	bool		donot_free;
};


class sl_pdt: public pdt {
public:
	static unsigned long dump_slpdt(sl_pdt *);

	sl_pdt(uint_t nhashent);
	~sl_pdt();

	//
	// Configure stickiness behavior
	//
	void pdt_config_sticky(const network::sticky_config_t &);

	//
	// input processing for a network packet
	//
	unsigned int sl_pdt::pdt_input(network::cid_t& cid,
	    network::lb_specifier_t policy, network::cid_action_t& action,
	    uint32_t& index, uint_t flag);

	void stop_forward(const network::cid_t& cid,
	    network::lb_specifier_t policy, nodeid_t node);

	void drop_syns(uint32_t index);

	int insert_frwd_info(const network::cid_t& cid, nodeid_t node,
	    uint32_t index);

	void add_frwd_info(const network::fwd_t& fwdp, bool *fwdp_valid,
	    network::lb_specifier_t policy, uint_t flag);

	void remove_frwd_info(nodeid_t node);
	void remove_frwd_info_by_ssap(const network::service_sap_t& ssap);

	void pre_marshal_fwd(network::seqlong_t& cntseq, nodeid_t node);
	void marshal_fwd(network::fwdseq_t& fwdinfo, nodeid_t node,
	    Environment& e);

	// Insert one binding entry (for Round-robin)
	int insert_bind_info(const network::cid_t& cid, nodeid_t node);

	// Add bindings that are retrieved from proxy nodes
	void pdt_add_bindinfo(const network::bind_t&, uint_t iter);

	// Remove bindings for the specified server node
	void remove_bindinfo(nodeid_t node);

	// Set state of the specified binding entry
	void pdt_setstate_binding(const network::inaddr_t& faddr,
	    network::bind_state_t);

	// Return a list of existing bindings
	void pdt_gns_bind(network::gns_bindseq_t *);

	// RR function
	bool incr_rr_count();
	void decr_rr_count(uint_t delta = 1);

	void set_rr_config(network::rr_config_t &);

	void setstate(const network::cid_t&,  network::lb_policy_t,
	    network::conn_state_t state);

	void resetall_usage_bit();

private:
	pdte_frwd_t		**sl_pdte_frwd;

	// Binding table to keep track of bindings on the GIF nodes
	gif_bind_table_t	*pdt_bindtab;

	// Config parameters for sticky behavior
	bool			pdt_strong_sticky;
	bool			pdt_udp_sticky;
	bool			pdt_generic_sticky;

	// Config parameter for round robin
	network::rr_config_t    pdt_rr_config;

	// Round-robin book-keeping
	os::mutex_t		pdt_rr_lock;
	uint_t			pdt_rr_count;


	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	sl_pdt(const sl_pdt &);
	sl_pdt &operator = (sl_pdt &);
};

//
// Class to manage RR
//
class sl_rr {
public:
	sl_rr(const network::pdtinfo_t&);
	~sl_rr();

	uint_t next();

	bool invalid_weights();

private:
	int find_gcd(uint8_t num_elem, uint8_t *weight);
	os::mutex_t	rrlock;
	uint_t		total_weight;
	uint_t		ptr;
	uint8_t		*assign_vec;
};

#endif	/* _SL_PDT_H */
