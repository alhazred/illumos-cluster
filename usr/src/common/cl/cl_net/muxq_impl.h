/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_MUXQ_IMPL_H
#define	_MUXQ_IMPL_H

#pragma ident	"@(#)muxq_impl.h	1.53	08/05/20 SMI"

/*
 * Mux Queue Implementation
 */

/*
 * Unode notes: only a small portion of muxq_impl_t is used in unode
 * and none of fastpath and fragment handling, so _KERNEL is around
 * most of this file.  Sorry, it makes it a little uglier.
 */

#ifdef	__cplusplus
extern "C" {
#endif	/* __cplusplus */

#ifdef _KERNEL

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>
#include <sys/strsun.h>

#include <sys/socket.h>
#include <net/if.h>

#endif	/* _KERNEL */

#include <h/naming.h>
#include <h/streams.h>
#include <h/sol.h>
#include <h/network.h>

#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#ifdef	_KERNEL

#include <orb/object/adapter.h>
#include <orb/ip/fpconf.h>

#endif	/* _KERNEL */

#ifdef SCTP_SUPPORT
#include <netinet/sctp.h>
#endif  // SCTP_SUPPORT

#include <cl_net/netlib.h>

//
// Default number of fragment lists for hashed lookup of fragments
// Since the universe of <src-ip, ident> for fragments are expected to be
// uniform, using a power of 2 as the hash table size is good. No need to
// perform integer division in packet inerrupts.
//
#define	DEF_FRAG_HASH_SIZE	64

#ifdef SCTP_SUPPORT
extern "C" int cl_sctp_cookie_paddr(sctp_chunk_hdr_t *, in6_addr_t *);
#pragma weak cl_sctp_cookie_paddr
#endif  // SCTP_SUPPORT

class fpconf;

class muxq_impl_t {

public:
	muxq_impl_t();
	~muxq_impl_t();
	void _unreferenced(unref_t);

#ifdef _KERNEL

	void demux_data(queue_t *, mblk_t *);

	network::cid_action_t naccept_main(queue_t *, mblk_t *,
	    network::cid_t& cid, unsigned& flag, nodeid_t& ret_node);

	mblk_t *create_dlpi_hdr(size_t); // dl_unitdata_req M_PROTO hdr
	bool check_for_space(mblk_t *mp, size_t);
	void send_packet(mblk_t *new_mp, nodeid_t dst_node,
	    network::cid_t& cid);

	void frag_timer(queue_t *q, mblk_t *mp);

#endif	/* _KERNEL */

private:

#ifdef _KERNEL

	// Packet processing routine for IPV4
	network::cid_action_t naccept_v4(queue_t *, mblk_t *,
	    network::cid_t& cid, unsigned& flag, nodeid_t& ret_node);

	// Packet processing routine for IPV6
	network::cid_action_t naccept_v6(queue_t *, mblk_t *,
	    network::cid_t& cid, unsigned& flag, nodeid_t& ret_node);

	// Determine if address is scalable
	bool is_address_scalable(const in6_addr_t *addrp);

#endif	// _KERNEL

	// Dont allow the operators to be overloaded.
	// Disallow assignments and pass by value
	muxq_impl_t(const muxq_impl_t &);
	muxq_impl_t &operator = (muxq_impl_t &);
};


#endif	/* _MUXQ_IMPL_H */
