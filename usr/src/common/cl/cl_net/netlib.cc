//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)netlib.cc	1.118	08/05/20 SMI"

//
// Cluster networking library
//
#include <sys/os.h>
#include <h/naming.h>
#include <cl_net/netlib.h>
#include <cl_net/errprint.h>
#include <h/network.h>
#include <sys/rm_util.h>
#include <sys/hashtable.h>

extern "C" {
#include <sys/types.h>
#include <sys/stream.h>
#include <sys/ddi.h>
#include <netinet/in.h>
#include <arpa/inet.h>

}

#define	SRVC_HASHTABLE_SIZE	1009

uint32_t pdt_hash_size = DEF_PDT_HASH_SZ;
uint32_t bt_hash_size = DEF_BT_HASH_SZ;
#ifdef SCTP_SUPPORT
uint32_t cn_hash_size = DEF_CN_HASH_SZ;
#endif

uint32_t clnet_hashtable_size = SRVC_HASHTABLE_SIZE;

clock_t clnet_rr_timeout = 8 * 60; // default timeout is 8 min

//
// Do not change this value. It is used for developement purpose.
//
uint32_t sticky_quiet_period = 500000;

uint32_t clnet_v6_mode = 0;

void
upgrade_bindseq(const network::bind_t& bseq)
{
	if (!clnet_v6_mode) {
		for (uint_t i = 0; i < bseq.bn_nfaddr; i++) {
			upgrade_inaddr(bseq.bn_faddrseq[i]);
		}
	}
}

void
upgrade_fwdseq(const network::fwd_t& fseq)
{
	if (!clnet_v6_mode) {
		for (uint_t i = 0; i < (uint_t)fseq.fw_ncid; i++) {
			upgrade_inaddr(fseq.fw_cidseq[i].ci_faddr);
			upgrade_inaddr(fseq.fw_cidseq[i].ci_laddr);
		}
	}
}

//
// Forward Reference
//
class pdt;

//
// Effects: Returns the HA object reference to the PDT server; otherwise,
//   returns NULL if there's an error in getting the reference.
//
network::PDTServer_ptr
netlib::get_pdt_ref()
{
	CORBA::Object_var		objv;
	CORBA::Object_var		nobjv;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	controlp;
	Environment			e;

	// Get mutex lock.
	network_lib.pdt_ref_mutex.lock();

	//
	// Check to see if private variable is already set; if so, there's
	// nothing to do, so just return a duplicate of the reference.
	//
	if (!CORBA::is_nil(local_pdt_ref)) {
		// Release mutex lock.
		network_lib.pdt_ref_mutex.unlock();
		return (network::PDTServer::_duplicate(local_pdt_ref));
	}

	//
	// What we're doing here is to query the Replica Manager (RM) to
	// to find out who the primary is and return the object reference.
	// The RM knows who this is already, which obviates the need for us
	// to do the same.
	// Note that this behaves a a kind of synchronization point; you can't
	// get the ref until the HA object has finished its initialization.
	// Anyone needing a reference is forced to wait until the
	// initialization has completed and a provider has been named the
	// primary.
	//
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));
	controlp = rm_ref->get_control(network::PDTServer::NS_NAME, e);
	if (e.exception()) {
		e.exception()->print_exception("netlib::get_pdt_ref--"
			"Can't find control object for PDT server\n");
		e.clear();
		// Release mutex lock.
		network_lib.pdt_ref_mutex.unlock();
		return (network::PDTServer::_nil());
	}

	// Return the object reference of the current primary.
	nobjv = controlp->get_root_obj(e);
	if (e.exception()) {
		e.exception()->print_exception("netlib::get_pdt_ref--"
			"Can't get reference to PDT server object\n");
		e.clear();
		// Release mutex lock.
		network_lib.pdt_ref_mutex.unlock();
		return (network::PDTServer::_nil());
	}

	//
	// INVARIANT: A primary has been chosen and the HA PDT server
	// object has been completely initialized.
	//

	// Now get the real server object of the right type.
	local_pdt_ref = network::PDTServer::_narrow(nobjv);

	if (CORBA::is_nil(local_pdt_ref)) {
		// Still NULL.  Just return.
		cmn_err(CE_WARN, "netlib::get_pdt_ref--"
		    " is still null\n");
		// Release mutex lock.
		network_lib.pdt_ref_mutex.unlock();
		return (network::PDTServer::_nil());
	}

	// Release mutex lock.
	network_lib.pdt_ref_mutex.unlock();

	return (network::PDTServer::_duplicate(local_pdt_ref));
}

//
// Effects:  Given the protocol "proto" return the character string
//   that represents that protocol.  Returns the empty string ""
//   if "proto" corresponds to no known protocol.
//
char *
netlib::protocol_to_string(uint8_t proto)
{
	switch (proto) {
	case IPPROTO_ICMP:
		return ("ICMP");
	case IPPROTO_GGP:
		return ("GGP");
	case IPPROTO_TCP:
		return ("TCP");
	case IPPROTO_EGP:
		return ("EGP");
	case IPPROTO_PUP:
		return ("PUP");
	case IPPROTO_UDP:
		return ("UDP");
	case IPPROTO_IDP:
		return ("IDP");
	case IPPROTO_RSVP:
		return ("RSVP");
	case IPPROTO_HELLO:
		return ("HELLO");
	case IPPROTO_ND:
		return ("ND");
	case IPPROTO_EON:
		return ("EON");
	case IPPROTO_RAW:
		return ("RAW");
#ifdef SCTP_SUPPORT
	case IPPROTO_SCTP:
		return ("SCTP");
#endif
	default: return ("UNKNOWN PROTOCOL");
	}
}

//
// Effects:  Given the load-balancing policy "policy" return the
//   character string that represents that policy.
//
char *
netlib::lbpolicy_to_string(network::lb_policy_t policy)
{
	switch (policy) {
		case network::LB_WEIGHTED:
			return ("LB_WEIGHTED");
		case network::LB_PERF:
			return ("LB_PERF");
		case network::LB_USER:
			return ("LB_USER");
		case network::LB_ST:
			return ("LB_ST");
		case network::LB_SW:
			return ("LB_SW");
		default:
			return ("UNKNOWN POLICY");
	} // end switch
}

//
// Effects: Converts an IP address structure "ip4addr" to the string form and
//   returns the string.  If "ip4addr" is 0, then return "INADDR_ANY".
//   Parts of the following are lifted from nfs_dlinet.c in
//   usr/src/uts/common/fs/nfs.
// NOTE: The "ip4addr" is already in network byte order, so this will also
//   work on Intel.
//
// NOTE: This function does not allocate memory. The caller can call this
//   function at MOST twice in a single shot. For example:
//	cmn_err(CE_NOTE, "<%s, %s>",
//		network_lib.ip4addr_to_string(addr1),
//		network_lib.ip4addr_to_string(addr2));
//   works as expected. Adding one more address though will
//   cause one of the string buffers for addr1 or addr2 to be overwritten.
//
char *
netlib::ip4addr_to_string(uint32_t ip4addr)
{
	static char b[2][18];		// allocate 2 buffers
	static uint_t index = 0;	// index flips between 0 and 1
	unsigned char *p;
	char *bp;
	struct in_addr ipstruct;

	if (ip4addr == INADDR_ANY) {
		return ("INADDR_ANY");
	}

	//
	// Convert the unsigned long representation of an IP address
	// to a structure.
	//
	ipstruct._S_un._S_addr = ip4addr;

	//
	// Use b[index] for this current conversion. Flip 'index'
	// so that the other buffer will be used for the next conversion.
	//
	bp = b[index];
	index ^= 1;

	p = (unsigned char *)&ipstruct;
	(void) sprintf(bp, "%d.%d.%d.%d", p[0], p[1], p[2], p[3]);
	return (bp);
}

char *
netlib::ipaddr_to_string(const network::inaddr_t &addr)
{
	static char b[2][64];		// allocate 2 buffers
	static uint_t index = 0;	// index flips between 0 and 1
	char *bp;

	//
	// Use b[index] for this current conversion. Flip 'index'
	// so that the other buffer will be used for the next conversion.
	//
	bp = b[index];
	index ^= 1;

	if (IN6_IS_ADDR_V4MAPPED((in6_addr_t *)&addr)) {
		uchar_t *cp = (uchar_t *)(&addr.v6[3]);

		(void) sprintf(bp, "%d.%d.%d.%d",
		    cp[0], cp[1], cp[2], cp[3]);
	} else if (IN6_IS_ADDR_UNSPECIFIED((in6_addr_t *)&addr)) {
		(void) sprintf(bp, "::");
	} else {
		//
		// The "preferrable" form according to RFC 2373 is
		// 8 16-bit hex separated by ':'.
		//
		uint16_t *up = (uint16_t *)&addr.v6[0];
		(void) sprintf(bp, "%x:%x:%x:%x:%x:%x:%x:%x",
		    up[0], up[1], up[2], up[3], up[4], up[5], up[6], up[7]);
	}

	return (bp);
}


//
// Effects: Given the node id "nodeid" return the host name of the
// node to which the id refers.
//
void
netlib::get_nodename_from_nodeid(nodeid_t nodeid, char nodename[NODENAME_MAX])
{
	clconf_cluster_t *cl;

	cl = clconf_cluster_get_current();

	const char *nname = clconf_cluster_get_nodename_by_nodeid(cl,
		(nodeid_t)nodeid);

	(void) os::snprintf(nodename, (size_t)NODENAME_MAX, "%s", nname);

	clconf_obj_release((clconf_obj_t *)cl);
}


//
// Number of buckets to list on each probe line
//
#define	BKTS_PER_LINE 20
#define	TNF_BUFSIZ 512
#define	BKT_STRING 32

unsigned long
pdt::dump_pdtinfo(const network::pdtinfo_t *pdtinfo)
{
	uint_t	nent = pdtinfo->hashinfoseq.length();
	char hashbuf[TNF_BUFSIZ] = "";
	char seqbuf[TNF_BUFSIZ] = "";
	char bktstr[BKT_STRING] = "";
	uint_t bufcnt = 0;
	uint_t i;

	for (i = 0; i < nent; i++) {
		if (bufcnt == BKTS_PER_LINE) {
			MC_PROBE_2(pdtinfo, "mc net mcnetdump pdt", "",
				tnf_uint, bkt_start, i - bufcnt,
				tnf_string, hashval, hashbuf);
			MC_PROBE_1(pdtinfo, "mc net mcnetdump pdt", "",
				tnf_string, bktseq, seqbuf);
			bufcnt = 0;
			hashbuf[0] = '\0';
			seqbuf[0] = '\0';
		}
		//
		// Concatenate this buckets values
		//
		(void) sprintf(bktstr, "%d ", pdtinfo->hashinfoseq[i].index);
		(void) strcat(hashbuf, (const char *)bktstr);
		(void) sprintf(bktstr, "%d ", pdtinfo->hashinfoseq[i].seqnum);
		(void) strcat(seqbuf, (const char *)bktstr);
		bufcnt++;
	}
	//
	// Print remainder of bucket entries
	//
	if (bufcnt) {
		MC_PROBE_2(pdtinfo, "mc net mcnetdump pdt", "",
			tnf_uint, bkt_start, i - bufcnt,
			tnf_string, hashval, hashbuf);
		MC_PROBE_1(pdtinfo, "mc net mcnetdump pdt", "",
			tnf_string, bktseq, seqbuf);
	}

	return (0);
}

unsigned long
pdt::dump_pdtinfo(pdt *pdtp)
{
	uint_t nhash = pdtp->pdt_getnhash();
	char hashbuf[TNF_BUFSIZ];
	char seqbuf[TNF_BUFSIZ];
	char bktstr[BKT_STRING];
	uint_t bufcnt = 0;
	uint_t i;

	MC_PROBE_2(dump_pdtinfo, "mc net mcnetdump pdt", "",
		tnf_uint, nhash, nhash,
		tnf_uint, pdt_getflag, pdtp->pdt_getflag());

	hashbuf[0] = '\0';
	seqbuf[0] = '\0';
	bktstr[0] = '\0';

	if (!(pdtp->pdt_getflag() & PDT_INITIALIZED)) {
		return (0);
	}

	for (i = 0; i < nhash; i++) {
		if (bufcnt == BKTS_PER_LINE) {
			MC_PROBE_2(dump_pdtinfo, "mc net mcnetdump pdt", "",
				tnf_uint, bkt_start, i - bufcnt,
				tnf_string, hashval, hashbuf);
			MC_PROBE_1(dump_pdtinfo, "mc net mcnetdump pdt", "",
				tnf_string, bktseq, seqbuf);
			os::prom_printf("          hash values: %s\n",
				hashbuf);

			bufcnt = 0;
			hashbuf[0] = '\0';
			seqbuf[0] = '\0';
		}
		//
		// Concatenate this buckets values
		//
		(void) sprintf(bktstr, "%d ", pdtp->pdt_gethashval(i));
		(void) strcat(hashbuf, (const char *)bktstr);
		bufcnt++;
	}
	//
	// Print remainder of bucket entries
	//
	if (bufcnt) {
		MC_PROBE_2(dump_pdtinfo, "mc net mcnetdump pdt", "",
			tnf_uint, bkt_start, i - bufcnt,
			tnf_string, hashval, hashbuf);
		MC_PROBE_1(dump_pdtinfo, "mc net mcnetdump pdt", "",
			tnf_string, bktseq, seqbuf);
	}

	os::prom_printf("          hash values: %s\n", hashbuf);
	return (0);
}

//
// Base pdt object methods
//
pdt::pdt(uint_t n) : _DList::ListElem(this)
{
	MC_PROBE_1(pdt, "mc net pdt", "",
		tnf_uint, nhash, n);
	pdt_nhash = n;
	pdt_flag = 0;
	pdte_hash = new pdte_hash_t[pdt_hash_size];

	for (uint_t i = 0; i < n; i++) {
		pdte_hash[i].set_hashval(0);
		pdte_hash[i].set_seqnum(0);
	}
}

pdt::~pdt()
{
	delete [] pdte_hash;
}

//
// Base class method to marshal pdt information
//
int
pdt::marshal_pdtinfo(network::pdtinfo_t& pdtinfo)
{
	//
	// Allocate space in pdtinfo sequence
	//
	pdtinfo.nhash = pdt_nhash;
	pdtinfo.hashinfoseq.length(pdt_nhash);

	for (uint_t i = 0; i < pdt_nhash; i++) {
		network::hashinfo_t& hashinfo = pdtinfo.hashinfoseq[i];

		hashinfo.seqnum = (uint_t)pdt_getbktseq(i);
		hashinfo.index = i;
		hashinfo.old_hashval =
		    hashinfo.new_hashval = pdt_gethashval(i);
	}
	return (0);
}

//
// Base class method to unmarshal pdt information
//
int
pdt::unmarshal_pdtinfo(const network::pdtinfo_t& pdtinfo)
{
	const network::hashinfoseq_t& hashinfoseq = pdtinfo.hashinfoseq;
	uint_t	nent = hashinfoseq.length();

	ASSERT(pdt_nhash == pdtinfo.nhash);

	NET_DBG(("pdt::unmarshal_pdtinfo(group=%s)\n",
	    pdtinfo.group.resource));

	MC_PROBE_1(unmarshal_pdtinfo, "mc net mcnetdump pdt", "",
		tnf_int, dump_pdtinfo, (int)dump_pdtinfo(&pdtinfo));

	for (uint_t i = 0; i < nent; i++) {
		const network::hashinfo_t&	hashinfo = hashinfoseq[i];
		uint_t index = hashinfo.index;

		pdt_setbktseq(index, (int)hashinfo.seqnum);
		pdt_sethashval(index, hashinfo.new_hashval);
	}
	pdt_setflag(PDT_INITIALIZED);
	return (0);
}

//
// Base class method to compute hash index
//
int
pdt::pdt_hashindex(network::cid_t& cid)
{
	MC_PROBE_2(pdt_hashindex, "mc net pdt", "",
		tnf_string, faddr, network_lib.ipaddr_to_string(cid.ci_faddr),
		tnf_uint, fport, cid.ci_fport);

	if (pdt_nhash != 0)
		return (int)((SUMOF(cid.ci_faddr) + cid.ci_fport) % pdt_nhash);
	else
		return (0);
}

//
// Base class method to get hash value given a cid
//
unsigned int
pdt::pdt_hash(network::cid_t& cid)
{
	int hindex;

	hindex = pdt_hashindex(cid);
	MC_PROBE_3(pdt_hash, "mc net pdt", "",
		tnf_int, fport, cid.ci_fport,
		tnf_int, hindex, hindex,
		tnf_uint, pdt_nhash, pdt_nhash);
	return ((uint_t)(pdt_gethashval((uint_t)hindex)));
}

frwd_info_t::~frwd_info_t()
{
	frwd_cid_list_lock.lock();
	frwd_cid_list.atfirst();
	frwd_cid *holder;
	while ((holder = frwd_cid_list.get_current()) != NULL) {
		frwd_cid_list.advance();
		(void) frwd_cid_list.erase(holder);
		delete holder;
	}
	frwd_cid_list_lock.unlock();
}

//
// Class to create a new cid
//

frwd_cid::frwd_cid(const network::cid_t &cid) : _DList::ListElem(this)
{
	fr_cid.ci_protocol = cid.ci_protocol;
	fr_cid.ci_fport = cid.ci_fport;
	fr_cid.ci_faddr = cid.ci_faddr;
	fr_cid.ci_lport = cid.ci_lport;
	fr_cid.ci_laddr = cid.ci_laddr;
	fr_cid.ci_dbit  = cid.ci_dbit;
	fr_cid.ci_state = cid.ci_state;

	NET_DBG_FWD(("Created frwd_cid fport=%d, faddr=%s\n",
	    fr_cid.ci_fport,
	    network_lib.ipaddr_to_string(fr_cid.ci_faddr)));

	NET_DBG_FWD(("Incoming fport = %d, faddr = %s\n",
	    cid.ci_fport, network_lib.ipaddr_to_string(cid.ci_faddr)));
}

//
// Effects:  Adds a cid to the cid list, and returns true.
//   Else returns false if we cannot allocated the cid object.
//
bool
frwd_info_t::fr_setcid(const network::cid_t& cid)
{
	frwd_cid *fcid = new (os::NO_SLEEP) frwd_cid(cid);
	if (fcid == NULL) {
		NET_DBG_FWD(("frwd_info_t::fr_setcid--Could not "
		    "allocate frwd_cid.\n"));
		return (false);
	}
	frwd_cid_list_lock.lock();
	frwd_cid_list.append(fcid);
	frwd_cid_list_lock.unlock();
	return (true);
}

//
// Copies the list of cids into the sequence specified
//
void
frwd_info_t::fr_getcid(int ncids, network::cidseq_t& cidseq)
{
	frwd_cid_list_lock.lock();
	frwd_cid_list.atfirst();
	frwd_cid *holder;
	for (uint_t i = 0; i < (uint_t)ncids; i++) {
		holder = frwd_cid_list.get_current();
		ASSERT(holder);

		cidseq[i].ci_fport = holder->fr_cid.ci_fport;
		cidseq[i].ci_faddr = holder->fr_cid.ci_faddr;
		cidseq[i].ci_lport = holder->fr_cid.ci_lport;
		cidseq[i].ci_laddr = holder->fr_cid.ci_laddr;
		cidseq[i].ci_dbit  = holder->fr_cid.ci_dbit;
		cidseq[i].ci_state = holder->fr_cid.ci_state;

		frwd_cid_list.advance();
	}
	frwd_cid_list_lock.unlock();
}

//
// Checks if we have an entry corresponding to the cid being
// passed in, in the forwarding list.
// Change controls whether the state of the connection is updated
// or returned to the caller.
//
bool
frwd_info_t::fr_equal(const network::cid_t& cid,
network::conn_state_t &state, int change)
{
	bool ret_val = false;
	frwd_cid_list_lock.lock();
	frwd_cid_list.atfirst();
	frwd_cid *holder;
	while ((holder = frwd_cid_list.get_current()) != NULL) {
		MC_PROBE_4(fr_equal, "mc net mcfwd", "",
		    tnf_uint, fr_fport, holder->fr_cid.ci_fport,
		    tnf_uint, fport, cid.ci_fport,
		    tnf_string, fr_faddr,
		    network_lib.ipaddr_to_string(holder->fr_cid.ci_faddr),
		    tnf_string, faddr,
		    network_lib.ipaddr_to_string(cid.ci_faddr));
		if (holder->fr_cid.ci_fport == cid.ci_fport &&
		    addr6_are_equal(holder->fr_cid.ci_faddr, cid.ci_faddr)) {
			holder->fr_cid.ci_dbit = 1;
			if (change)
				holder->fr_cid.ci_state = state;
			else
				state = holder->fr_cid.ci_state;
			ret_val = true;
			break;
		}
		frwd_cid_list.advance();
	}
	frwd_cid_list_lock.unlock();
	return (ret_val);
}

void
pdte_frwd_t::pre_marshal_fwd(network::seqlong_t& cnt, nodeid_t node)
{
	frwd_info_list_t *listp;
	frwd_info_t *frwdp;
	nodeid_t lastnode;

	ASSERT(node <= NODEID_MAX);

	//
	// If the nodeid passed in is zero, they want information
	// for forwarding lists on all nodes.  Otherwise, simply
	// give them the one list they asked for.
	//
	if (node == 0) {
		lastnode = NODEID_MAX;
		node = 1;
	} else {
		lastnode = node;
	}

	for (; node <= lastnode; node++) {
		if ((listp = pernodelists[node]) != NULL) {
			//
			// Append the number of cids to the sequence needed
			//
			frwd_info_list_t::ListIterator citer(listp);
			while ((frwdp = citer.get_current()) != NULL) {
				cnt[node - 1] += frwdp->frwd_cid_list.count();
				citer.advance();
			}
		}
	}
}

void
pdte_frwd_t::marshal_fwd(network::fwdseq_t& fwd, nodeid_t node, Environment& e)
{
	frwd_info_list_t *listp;
	frwd_info_t *frwdp;
	frwd_cid *cidp;
	uint_t count;
	uint_t i;
	nodeid_t lastnode;

	ASSERT(node <= NODEID_MAX);

	//
	// If the nodeid passed in is zero, they want information
	// for forwarding lists on all nodes.  Otherwise, simply
	// give them the one list they asked for.
	//
	if (node == 0) {
		lastnode = NODEID_MAX;
		node = 1;
	} else {
		lastnode = node;
	}

	for (; node <= lastnode; node++) {
		if ((listp = pernodelists[node]) != NULL) {

			//
			// If we're extending the sequence, make sure
			// we zero out the cid sequences in between
			// where we are, and where we're going.
			//
			if (node > fwd.length()) {
				e.exception(new sol::op_e(EAGAIN));
				return;
			}

			//
			// Determine length of sequence needed
			//
			count = 0;
			frwd_info_list_t::ListIterator citer(listp);
			while ((frwdp = citer.get_current()) != NULL) {
				count += frwdp->frwd_cid_list.count();
				citer.advance();
			}

			//
			// fw_ncid holds the current end of the sequence we've
			// used.  We should already have enough elements to
			// contain all the forwarding entries, if not, the
			// forwarding list got longer, so just return EAGAIN
			// and have our caller readjust the sequences.
			//
			// i tells us where to start adding entries.
			//
			i = (uint_t)fwd[node - 1].fw_ncid;
			if (((uint_t)fwd[node - 1].fw_ncid + count) >
			    (uint_t)fwd[node - 1].fw_cidseq.length()) {
				e.exception(new sol::op_e(EAGAIN));
				return;
			}
			NET_DBG_FWD((
			    "pdte_frwd_t::marshal_fwd node=%d len=%d add=%d\n",
			    node, i, count));
			fwd[node - 1].fw_node = node;
			//
			// Accumulate the next forwarding entries
			//
			fwd[node - 1].fw_ncid = (int)(count + i);

			frwd_info_list_t::ListIterator iter(listp);
			while ((frwdp = iter.get_current()) != NULL) {
				frwd_cid_list_t::ListIterator
				    iter2(frwdp->frwd_cid_list);
				while ((cidp = iter2.get_current()) != NULL) {
					fwd[node - 1].fw_cidseq[i].ci_protocol
					    = cidp->fr_cid.ci_protocol;
					fwd[node - 1].fw_cidseq[i].ci_lport
					    = cidp->fr_cid.ci_lport;
					fwd[node - 1].fw_cidseq[i].ci_laddr
					    = cidp->fr_cid.ci_laddr;
					fwd[node - 1].fw_cidseq[i].ci_fport
					    = cidp->fr_cid.ci_fport;
					fwd[node - 1].fw_cidseq[i].ci_faddr
					    = cidp->fr_cid.ci_faddr;
					fwd[node - 1].fw_cidseq[i].ci_dbit
					    = cidp->fr_cid.ci_dbit;
					fwd[node - 1].fw_cidseq[i].ci_state
					    = cidp->fr_cid.ci_state;
					i++;
					iter2.advance();
				}
				iter.advance();
			}
		}
	}
}

pdte_frwd_t::pdte_frwd_t()
{
	//
	// Initialize pernodelist listhead objects
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++)
		pernodelists[i] = NULL;
	nfrwd = 0;
}

pdte_frwd_t::~pdte_frwd_t()
{
	frwd_info_t *frwdp;
	frwd_info_list_t *listp;

	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		listp = pernodelists[node];
		if (listp) {
			while ((frwdp = pernodelists[node]->reapfirst())
			    != NULL) {
				nfrwd--;
				delete frwdp;
			}
			delete listp;
		}
	}
}

//
// Check if a request needs to be forwarded
//
unsigned int
pdte_frwd_t::lookup_change(const network::cid_t& cid,
network::conn_state_t &state, int change)
{
	frwd_info_t	*frwdp;
	nodeid_t node;

	MC_PROBE_0(lookup, "mc net mcfwd", "");

	for (node = 1; node <= NODEID_MAX; node++) {
		if (pernodelists[node] == NULL)
			continue;

		frwd_info_list_t::ListIterator li(pernodelists[node]);
		while ((frwdp = li.get_current()) != NULL) {
			if (frwdp->fr_equal(cid, state, change)) {
				MC_PROBE_1(lookup, "mc net mcfwd", "",
				    tnf_uint, node, node);
				return (node);
			}
			li.advance();
		}
	}
	return (0);
}

void
pdte_frwd_t::resetall_usage_bit()
{
	frwd_info_list_t *listp;
	frwd_info_t *frwdp;
	frwd_cid *cidp;
	nodeid_t node;

	for (node = 1; node <= NODEID_MAX; node++) {
		if ((listp = pernodelists[node]) != NULL) {
			frwd_info_list_t::ListIterator iter(listp);
			while ((frwdp = iter.get_current()) != NULL) {
				frwd_cid_list_t::ListIterator
				    iter2(frwdp->frwd_cid_list);
				    while ((cidp = iter2.get_current()) !=
					NULL) {
					cidp->fr_cid.ci_dbit = 0;
					iter2.advance();
				}
				iter.advance();
			}
		}
	}
}

//
// Unmarshal forwarding information. The fwd_t can have forwarding information
// corresponding to more than one bucket. So, select cids corresponding to the
// index being passed in.
// This routine can also run at interrupt priority because of
// priority inversion.
//
void
pdte_frwd_t::unmarshal_selective_frwd_info(const network::cid_t& cid,
    nodeid_t node,
#if defined(DBGBUFS) && !defined(NO_DBGBUFS)
    int index)
#else
    int)
#endif
{
	frwd_info_list_t *listp;
	frwd_info_t *frwdp;

	bool creat_list = false;

	NET_DBG_FWD(("pdte_frwd_t::unmarshal_selective_frwd_info()\n"));
	NET_DBG_FWD(("   index = %d, node = %d\n", index, node));

	listp = pernodelists[node];

	if (listp == NULL) {
		//
		// This is place where listp and frwdp lists are created.
		// If there is a failure during the creation of either
		// of these structures, just return.
		//
		frwdp = new (os::NO_SLEEP) frwd_info_t();
		if (frwdp == NULL) {
			NET_DBG_FWD(("  Could not allocate frwd_info_t"
				"for index %d\n", index));
			return;
		}
		frwdp->fr_setnode(node);
		pernodelists[node] = new (os::NO_SLEEP) frwd_info_list_t;
		if (pernodelists[node] == NULL) {
			NET_DBG_FWD(("  mem allocation of"
			    "listp failed index %d\n", index));
			delete frwdp;
			return;
		}
		creat_list = true;
		nfrwd++;
		pernodelists[node]->prepend(frwdp->get_pernode_elem());
	} else {
		//
		// listp has already been created. Since listp always
		// has one element, frwdp can have only one value.
		// 3.next : should cleanup these lists
		//
		listp->atfirst();
		frwdp = listp->get_current();
		ASSERT(frwdp != NULL);
	}

	//
	// frwding list should be from the same node as th current
	// one.
	//
	ASSERT(frwdp->fr_getnode() == node);

	//
	// fr_setcid is the place where a new frwd entry is added to
	// the frwdp list. Since, nonblocking memory allocation is
	// done here, this can also fail.
	//

	if (frwdp->fr_setcid(cid)) {
		frwdp->fr_incrcids();
		NET_DBG_FWD(("  no of cids for index %d is %d\n",
		    index, frwdp->fr_getncids()));
	} else {

		//
		// fr_setcid had a memory allocation failure. So, if
		// listp and frwdp was created by this call, creat_list
		// is set to true. If it is set to true, then the listp
		// and frwdp structures have to be deleted. If it is set
		// to false, then no cleanup is necessary. stop_forward()
		// will do the necessary cleanup later.
		//

		if (creat_list) {
			listp = pernodelists[node];
			(void) listp->erase(frwdp->get_pernode_elem());
			nfrwd--;
			delete frwdp;
			delete listp;
			pernodelists[node] = NULL;
		}
	}

}

//
// clean up forwarding information associated with a per bucket
// per node. Normally called when we have "fresh" forwarding information
// for the same node and same bucket.
//
void
pdte_frwd_t::clean_frwd_info(nodeid_t node) {
	frwd_info_list_t *listp;
	frwd_info_t	*frwdp;

	NET_DBG_FWD(("pdte_frwd_t::clean_frwd_info()\n"));

	listp = pernodelists[node];

	if (listp == NULL)
		return;

	while ((frwdp = listp->reapfirst()) != NULL) {
		nfrwd--;
		delete frwdp;
	}

	delete listp;
	pernodelists[node] = NULL;

	NET_DBG_FWD(("  clean frwd node=%d, nfrwd=%d\n", node, nfrwd));
}

//
// clean up forwarding information associated with a ssap for every node.
// Normally called when we remove a service.
//
void
pdte_frwd_t::clean_frwd_info_by_ssap(const network::service_sap_t& ssap)
{
	frwd_info_list_t *listp;
	frwd_info_t	*frwdp;

	NET_DBG_FWD(("pdte_frwd_t::clean_frwd_info_by_ssap([%s,%s,%d])\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	for (nodeid_t node = 1; node <= NODEID_MAX; node++) {
		listp = pernodelists[node];
		if (listp) {
			listp->atfirst();
			while ((frwdp = listp->get_current()) != NULL) {
				// Advance iterator before doing erase
				listp->advance();

				frwdp->clean_frwd_cid(ssap);
				if (frwdp->fr_getncids() == 0) {
					(void) listp->erase(
						frwdp->get_pernode_elem());
					nfrwd--;
					delete frwdp;
					NET_DBG_FWD(\
					    (" deleting the empty list...\n"));
				}
			}
			if (listp->empty()) {
				delete listp;
				pernodelists[node] = NULL;
			}
		}
	}
}

//
// Stop forwarding. If the number of elements in the forwarding list
// for a node per bucket drops to zero, the list element is deleted
// from the list.
//
int
pdte_frwd_t::stop_forward(network::cid_t& cid, nodeid_t node)
{
	frwd_info_list_t *listp;
	frwd_info_t	*frwdp;

	listp = pernodelists[node];

	if (listp == NULL)
		return (0);

	NET_DBG_FWD(("pdte_frwd_t::stop_forward(node=%d)\n", node));

	listp->atfirst();
	while ((frwdp = listp->get_current()) != NULL) {
		// Advance iterator before doing erase
		listp->advance();
		if (frwdp->fr_delete(cid)) {
			if (frwdp->fr_getncids() == 0) {
				(void) listp->erase(
					frwdp->get_pernode_elem());
				nfrwd--;
				delete frwdp;
				NET_DBG_FWD(("deleting the empty list...\n"));
				if (listp->empty()) {
					NET_DBG_FWD(("deleting listp for %d\n",
					    node));
					delete listp;
					pernodelists[node] = NULL;
				}
				return (1);
			}
		}
	}
	return (0);
}

//
// Check if the pdte_frwd_t structure can be deleted. If all the
// lists have been deleted, we might as well delete the pdte_frwd_t
// structure.
//
bool
pdte_frwd_t::is_frwd_empty()
{
	ASSERT(nfrwd >= 0);
	if (nfrwd == 0)
		return (true);
	return (false);
}

//
// This function checks if the cid is in the forwarding list or
// not. If it is not in the forwarding list, it returns false.
// If the cid is in the forwarding list, the holder object is
// deleted from the forwarding list and the ret_val is set to true.
//
bool
frwd_info_t::fr_delete(network::cid_t& cid)
{
	bool ret_val = false;

	NET_DBG_FWD(("frwd_info_t::fr_delete()\n"));

	frwd_cid_list_lock.lock();
	frwd_cid_list.atfirst();
	frwd_cid *holder;

	while ((holder = frwd_cid_list.get_current()) != NULL) {
		frwd_cid_list.advance();
		if (holder->fr_cid.ci_fport == cid.ci_fport &&
		    addr6_are_equal(holder->fr_cid.ci_faddr, cid.ci_faddr)) {
			ret_val = true;
			(void) frwd_cid_list.erase(holder);
			delete holder;
			--fr_ncids;
			break;
		}
	}
	frwd_cid_list_lock.unlock();

	return (ret_val);
}

//
// This function checks if the ssap is in the forwarding list or
// not. If the ssap is in the forwarding list, the holder object is
// deleted from the forwarding list.
//
void
frwd_info_t::clean_frwd_cid(const network::service_sap_t& ssap)
{
	NET_DBG_FWD(("frwd_info_t::clean_frwd_cid([%s, %s, %d])\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	frwd_cid_list_lock.lock();
	frwd_cid_list.atfirst();
	frwd_cid *holder;

	while ((holder = frwd_cid_list.get_current()) != NULL) {
		frwd_cid_list.advance();
		if (holder->fr_cid.ci_lport == ssap.lport &&
		    addr6_are_equal(holder->fr_cid.ci_laddr, ssap.laddr)) {
			(void) frwd_cid_list.erase(holder);
			delete holder;
			--fr_ncids;
		}
	}
	frwd_cid_list_lock.unlock();
}


bool
servicegrp::is_group_equal(const network::group_t &group)
{
	if ((strcmp(sr_group.resource, group.resource) == 0))
		return (true);

	return (false);
}

servicegrp::servicegrp(const network::group_t &group,
    unsigned int nhash, network::lb_specifier_t policy) :
    _DList::ListElem(this)
{
	sr_nhash = (int)nhash;
	sr_policy = policy;
	sr_group = group;
}

serviceobj::serviceobj(const network::service_sap_t &ssap) :
    _DList::ListElem(this)
{
	sr_sap = ssap;
	sr_gifnode = 0;
}
