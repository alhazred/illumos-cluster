//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)conn_table.cc	1.3	08/05/20 SMI"

#include <sys/types.h>
#include <sys/conf.h>
#include <sys/modctl.h>

#include <h/network.h>
#include <sys/os.h>

#include <h/network.h>
#include <sys/os.h>
#include <sys/threadpool.h>

#include <cl_net/netlib.h>
#include <cl_net/conn_table.h>
#include <cl_net/sl_pdt.h>
#include <cl_net/sl_service.h>
#include <cl_net/mcobj_impl.h>

#include <sys/mc_probe.h>

#ifdef SCTP_SUPPORT
#define	conn_hash(handle)	(uint_t)(handle % conn_hashsize)

uint_t
cn_hashsize_in_range(uint_t hashsize)
{
	if (hashsize < MIN_CN_HASH_SZ)
		return (MIN_CN_HASH_SZ);
	if (hashsize > MAX_CN_HASH_SZ)
		return (MAX_CN_HASH_SZ);

	return (hashsize);
}

conn_table_t::conn_table_t(uint_t hashsize)
{
	conn_hashsize = cn_hashsize_in_range(hashsize);
	conn_list = new conn_list_t[conn_hashsize];
	conn_list_lock = new os::rwlock_t[conn_hashsize];
}

conn_table_t::~conn_table_t()
{
	//
	// Remove all entries first.
	//
	clean_all();

	delete [] conn_list;
	conn_list = NULL;
	delete [] conn_list_lock;
	conn_list_lock = NULL;
}

void
conn_table_t::list_rdlock(cl_sctp_handle_t handle)
{
	uint_t index = conn_hash(handle);
	conn_list_lock[index].rdlock();
}

void
conn_table_t::list_wrlock(cl_sctp_handle_t handle)
{
	uint_t index = conn_hash(handle);
	conn_list_lock[index].wrlock();
}

void
conn_table_t::list_unlock(cl_sctp_handle_t handle)
{
	uint_t index = conn_hash(handle);
	conn_list_lock[index].unlock();
}

conn_entry_t *
conn_table_t::lookup_entry(cl_sctp_handle_t handle)
{
	conn_entry_t *entry;
	uint_t index = conn_hash(handle);

	conn_list_lock[index].rdlock();

	conn_list_t::ListIterator li(&conn_list[index]);
	while ((entry = li.get_current()) != NULL) {
		if (entry->cn_handle == handle) {
			break;
		}
		li.advance();
	}
	conn_list_lock[index].unlock();
	return (entry);
}

//
void
conn_table_t::remove_entry(cl_sctp_handle_t handle)
{
	conn_entry_t *entry;
	uint_t index = conn_hash(handle);

	conn_list_lock[index].wrlock();

	conn_list_t::ListIterator li(&conn_list[index]);
	while ((entry = li.get_current()) != NULL) {
		if (entry->cn_handle == handle) {
			NET_DBG_CT(("  remove %s\n", entry->to_string()));
			(void) conn_list[index].erase(entry);
			delete entry;
			break;
		}
		li.advance();
	}
	conn_list_lock[index].unlock();
}

bool
conn_table_t::add_entry(cl_sctp_handle_t handle, sa_family_t addr_family,
    network::service_sap_t& ssap, uint8_t nfaddrs,
    uint8_t *faddrp, in_port_t fport)
{
	bool created = false;
	conn_entry_t *entry = NULL;
	uint_t index = conn_hash(handle);

	if ((entry = lookup_entry(handle)) == NULL) {
		entry = new conn_entry_t(handle, addr_family, ssap, nfaddrs,
		    faddrp, fport);
		conn_list_lock[index].wrlock();
		conn_list[index].append(entry);
		created = true;
		NET_DBG_CT(("Created %s\n", entry->to_string()));
	} else {
		//
		// Unexpected scenario. Stale handle present in connection
		// table. We have noway other than reusing the handle.
		// There may be memory leak caused by the old connection
		// (in the GIF node) which we don't handle.
		//
		conn_list_lock[index].wrlock();
		entry->cn_addr_family = addr_family;
		entry->cn_ssap = ssap;
		entry->cn_nfaddrs = nfaddrs;
		entry->cn_faddrp = faddrp;
		entry->cn_fport = fport;
		NET_DBG_CT(("An existing handle encountered: updated %s\n",
		    entry->to_string()));
	}
	conn_list_lock[index].unlock();
	return (created);
}

void
conn_table_t::clean_all()
{
	conn_entry_t *entry = NULL;
	uint_t index = 0;

	NET_DBG_CT(("conn_table_t::clean_all\n"));

	for (index = 0; index < conn_hashsize; index++) {
		conn_list_lock[index].wrlock();
		while ((entry = conn_list[index].reapfirst()) != NULL) {
			NET_DBG_CT(("  remove %s\n", entry->to_string()));
			delete entry;
		}
		conn_list_lock[index].unlock();
	}
}

const char *
conn_entry_t::to_string()
{
	char entry_str_buf[150];

	//
	// shows one address of server and client each
	//
	(void) sprintf(entry_str_buf, "<%lu, {[%s .., %d], [%s .., %d]}>",
	    cn_handle, network_lib.ipaddr_to_string(cn_ssap.laddr),
	    cn_ssap.lport, network_lib.ipaddr_to_string(
	    *(network::inaddr_t *) cn_faddrp), cn_fport);

	return (entry_str_buf);
}

void
conn_entry_t::append_addr(uint8_t *addrp, uint_t naddrs)
{
	uint8_t *taddrp = (uint8_t *)kmem_alloc(sizeof (network::inaddr_t) *
	    (naddrs + cn_nfaddrs), KM_SLEEP);
	bcopy(cn_faddrp, taddrp, cn_nfaddrs * sizeof (network::inaddr_t));
	bcopy(addrp, taddrp + cn_nfaddrs * sizeof (network::inaddr_t), naddrs *
	    sizeof (network::inaddr_t));
	kmem_free(cn_faddrp, cn_nfaddrs * sizeof (network::inaddr_t));
	cn_faddrp = taddrp;
	taddrp = NULL;
	cn_nfaddrs += naddrs;
}

void
conn_entry_t::deduct_addr(uint8_t *addrp, uint_t naddrs)
{
	network::inaddr_t *taddrp = (network::inaddr_t *) cn_faddrp;
	network::inaddr_t *t2addrp = taddrp;
	uint_t cnt = 0;
	bool found = false;
	for (uint_t i = 0; i < cn_nfaddrs; i++) {
		network::inaddr_t *paddrp = (network::inaddr_t *) addrp;
		for (uint_t j = 0; j < naddrs; j++) {
			if (addr6_are_equal(*paddrp, *t2addrp)) {
				found = true;
				break;
			}
			paddrp ++;
		}
		if (!found) {
			if (taddrp != t2addrp) {
				bcopy(t2addrp, taddrp,
				    sizeof (network::inaddr_t));
			}
			taddrp ++;
			cnt ++;
		}
		t2addrp ++;
	}
	kmem_free(cn_faddrp + cnt * sizeof (network::inaddr_t),
	    (size_t)((cn_nfaddrs - cnt) * sizeof (network::inaddr_t)));
	cn_nfaddrs = cnt;
}

#endif  // SCTP_SUPPORT
