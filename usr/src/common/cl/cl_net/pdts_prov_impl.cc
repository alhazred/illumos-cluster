//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pdts_prov_impl.cc	1.104	08/05/20 SMI"

#include <sys/types.h>
#include <h/network.h>
#include <sys/os.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/pdts_prov_impl.h>
#include <cl_net/perf_policy.h>
#include <cl_net/lbobj.h>
#include <cl_net/errprint.h>

//
// Effects: Constructor.
//
pdts_prov_impl_t::pdts_prov_impl_t(char *svc_id, const char *prov_id) :
	repl_server<network::ckpt> (svc_id, prov_id),
	pdts_initialized(false),
	prov_is_primary(false),
	pdtsp(NULL), _ckpt_proxy(NULL)
{
	NET_DBG(("pdts_prov_impl_t constructor\n"));
}

//
// Effects: Destructor.
//
pdts_prov_impl_t::~pdts_prov_impl_t()
{
	delete pdtsp;
	pdtsp = NULL;
	_ckpt_proxy = NULL;
}

// ************************************************************************
//
// Repl_server methods
//
// ************************************************************************

//
// Effects:  This routine is called on a secondary to make it a primary.
//   It is invoked on a null secondary to start the service or on an
//   active secondary to perform failover or switchover.
//
void
pdts_prov_impl_t::become_primary(const replica::repl_name_seq &, Environment &e)
{
	NET_DBG(("pdts_prov_impl_t::become_primary()\n"));

	// Set up the checkpoint proxy.
	replica::checkpoint_var tmp_ckpt_v =
	    set_checkpoint(network::ckpt::_get_type_info(0));
	_ckpt_proxy = network::ckpt::_narrow(tmp_ckpt_v);

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	if (!pdts_initialized) {
		// Create the PDT server and mcnet_node_array array.
		bool res = create_service();
		if (res) {
			pdts_initialized = true;
		} else {
			cmn_err(CE_WARN, "pdts_prob_impl_t::become_primary--"
				" problem creating all the PDT, and IFREG"
				" server objects\n");
			prov_is_primary = true;
			e.exception(new replica::repl_prov_failed);

			// When returning exception, release the checkpoint.
			CORBA::release(_ckpt_proxy);
			_ckpt_proxy = nil;

			return;
		}
	}

	prov_is_primary = true;
	// Invariant: This initialized variable is either all true
	// or all false.
}

//
// Effects: This routine is called on a frozen primary because a switchover
//   to another primary is being performed.  If the primary stores its
//   state in a different way from secondaries, then it must convert
//   its state to this secondary form.
// Note: In our case, this routine does nothing.
//
void
pdts_prov_impl_t::become_secondary(Environment &)
{
	NET_DBG(("pdts_prov_impl_t::become_secondary()\n"));

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

	prov_is_primary = false;
}

//
// Effects:  This routine is called on a frozen primary when a new secondary
//   is being added to the service.  As part of this call, the primary
//   must dump its state to the new secondary through the checkpoint
//   object reference "sec_chkpt" that is passed in.
//
void
pdts_prov_impl_t::add_secondary(replica::checkpoint_ptr sec_chkpt,
			    const char *,
			    Environment &e)
{
	NET_DBG(("pdts_prov_impl_t::add_secondary()\n"));

	// Get the checkpoint reference to the secondary being added.
	network::ckpt_var chkpt = network::ckpt::_narrow(sec_chkpt);
	ASSERT(!CORBA::is_nil(chkpt));

	ASSERT(pdtsp != NULL);
	ASSERT(!CORBA::is_nil(pdts_v));

	// 1.  Dump the PDT server object and its state
	if (pdtsp != NULL) {
		NET_DBG(("Dumping PDTS state...\n"));

		// Checkpoint the PDTS object reference to secondaries.
		NET_DBG(("   call ckpt_create_pdts()\n"));
		chkpt->ckpt_create_pdts(pdts_v, e);
		// Secondary failed.  Just log a warning and bail out.
		if (e.exception() != NULL) {
			cmn_err(CE_WARN, "ckpt_create_pdts() checkpoint "
			    "to specific secondary failed\n");
			e.clear();
			return;
		}

		// Dump the mcnet_node array if it exists.
		NET_DBG(("   call dump_mcnet_node_array()\n"));
		if (!pdtsp->dump_mcnet_node_array(sec_chkpt)) {
			return;
		}

		// Dump the master service list and all objects.
		NET_DBG(("   call dump_maservicegrplist()\n"));
		if (!pdtsp->dump_maservicegrplist(sec_chkpt)) {
			return;
		}
	} // end if

	// 2.  Dump the load-balancing objects.
	lbobj::add_secondary_all(chkpt, e);
	if (e.exception() != NULL) {
		return;
	}
	e.clear();
}

//
// Effects:  This routine is called on an active primary.  In most cases,
//   there is nothing to do.  The HA framework removes the secondary from
//   the group of secondaries that receives checkpoints.  The only reason
//   for a primary to do something in this call is if for some reason
//   the primary wants to keep track of its current secondaries.  This
//   should not be necessary, but may be useful for debugging.
// Note: In our case, we do nothing here.
//
void
pdts_prov_impl_t::remove_secondary(const char *, Environment &)
{
	NET_DBG(("pdts_prov_impl_t::remove_secondary()\n"));
}

void
pdts_prov_impl_t::freeze_primary_prepare(Environment &)
{
}

//
// Effects:  This method is called before adding secondary or new
//   primary taking over.  We must make sure no invocations to the
//   service will block waiting for another invocation or
//   _unreferenced().  To prevent deadlock here, we can wait for
//   requests to complete and lock out new ones.
//
void
pdts_prov_impl_t::freeze_primary(Environment &e)
{
	NET_DBG(("pdt_prov_impl_t::freeze_primary()\n"));
	lbobj::freeze_primary_all(e);
}

//
// Effects:  Reset state. Invocations and _unreferenced are enabled
//   after this returns.
//
void
pdts_prov_impl_t::unfreeze_primary(Environment &e)
{
	NET_DBG(("pdts_prov_impl_t::unfreeze_primary()\n"));
	lbobj::unfreeze_primary_all(e);
}

//
// Effects: This routine should clean up any state that might have been
//   dumped during an "add_secondary" call.  It is possible that the
//   primary fails during the "add_secondary" call, leaving the
//   secondary in an inconsistent state; it seems that the approach
//   taken is the convert the secondary back to a "null secondary."
//
void
pdts_prov_impl_t::become_spare(Environment &)
{

	NET_DBG(("pdts_prov_impl_t::become_spare()\n"));

	// Delete the PDT server implementation object.
	if (pdtsp != NULL) {
		delete pdtsp;
		pdtsp = NULL;
		pdts_initialized = false;
	} // end if
}

//
// Effects: Tells a spare that it is being made active.  This means that
//   either the service is starting and this is about to become the first
//   primary or that this provider is about to be added as a secondary.
//   There is nothing to do in this invocation; it is merely informative.
//   This function might be removed in future.
//
void
pdts_prov_impl_t::become_active(Environment &)
{
	NET_DBG(("pdts_prov_impl_t::become_active()\n"));
}

//
// Effects: This routine shuts down an HA service and frees storage.
//
void
pdts_prov_impl_t::shutdown(Environment &)
{
	NET_DBG(("pdts_prov_impl_t::shutdown()\n"));

	// Release the checkpoint proxy.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;

	// Not clear yet what this entails.
}

//
// Effects:  This method returns an object reference to the PDT server
//   object.  This is a highly available call, so that if the primary
//   fails when this invocation is in progress, then the RM will retry
//   the method on the next primary.
//
CORBA::Object_ptr
pdts_prov_impl_t::get_root_obj(Environment &)
{
	ASSERT(pdtsp != NULL);
	return (pdtsp->get_objref());
}

// ************************************************************************
//
// Checkpoint interface methods
//
// ************************************************************************

//
// Effects: This checkpoint routine creates a shadow copy of the
//   PDT server object and makes it part of the state of the
//   repl_server provider object.
//
// Parameters:
//   - pdts (IN):  Pointer to the PDT server implementation object.
//
void
pdts_prov_impl_t::ckpt_create_pdts(network::PDTServer_ptr pdts,
				Environment &)
{
	NET_DBG(("pdts_prov_impl_t::ckpt_create_pdts()\n"));
	if (pdtsp == NULL) {
		// Create a shadow copy of the PDT server object and
		// make it part of the state of the repl_server provider
		// object.
		pdtsp = new pdts_impl_t(pdts, this);
		pdts_v = network::PDTServer::_duplicate(pdts);
		pdts_initialized = true;
	}
}

//
// Effects: The checkpoint routine creates the mcnet_node_array array at the
//   secondaries, but doesn't create the individual mcnet_node_impl_t elements.
//
void
pdts_prov_impl_t::ckpt_create_mcnet_node_array(Environment &)
{
	NET_DBG(("pdts_prov_impl_t::ckpt_create_mcnet_node_array()\n"));

	if (mcnet_node_array == NULL) {
		unsigned int i;

		mcnet_node_array = new mcnet_node_impl_t*[NODEID_MAX+1];

		for (i = 1; i < NODEID_MAX+1; i++) {
			mcnet_node_array[i] = NULL;
		} // end for
	} // end if
}

//
// Effects: The checkpoint routine creates the individual mcnet_node object
//   at the secondaries and installs in the mcnet_node_array array at index
//   "nodeid."
//
// Parameters:
//   - servobjp (IN):  The object reference to the primary's mcnet_node
//   - nodeid (IN):    The node corresponding to this servojb.
//   - cbref (IN):	mcobj object reference at node "nodeid"
//   - gif (IN):	is this node a gif node (true or false)
//
void
pdts_prov_impl_t::ckpt_create_mcnet_node_and_dump_state(
		network::mcnet_node_ptr	mcnet_nodep,		// IN
		nodeid_t		nodeid,			// IN
		network::mcobj_ptr	cbref,  		// IN
		bool			gif, 			// IN
		Environment		&)
{
	ASSERT(mcnet_node_array != NULL);
	ASSERT(nodeid >= 1 && nodeid <= NODEID_MAX);

	if (mcnet_node_array[nodeid] == NULL) {
		// Create the mcnet_net object at the secondary.
		mcnet_node_array[nodeid] =
		    new mcnet_node_impl_t(mcnet_nodep, this);

		// Now initialize its fields.
		mcnet_node_array[nodeid]->node = nodeid;
		mcnet_node_array[nodeid]->set_gifnode(gif);
		mcnet_node_array[nodeid]->set_cbref(cbref);
	}
}

//
// Effects:  This checkpoint creates a master service object with
//   service access point "ssap" and load-balancing policy "policytype".
//
// Parameters:
//   - ssap (IN):	Service access point <protocol, ip adrss, port>
//   - nhash (IN):	Number of hash buckets in PDT
//   - policytype (IN):   policy type (LB_WEIGHTED, LB_PERF,
//			LB_RT, LB_USER)
//
void
pdts_prov_impl_t::ckpt_create_ma_servicegrp(
		const network::group_t		&g_name,	// IN
		uint32_t			nhash,		// IN
		network::lb_specifier_t		policytype,	// IN
		network::lbobj_i_ptr		lb,		// IN
		Environment			&)		// IN
{
	NET_DBG(("pdts_prov_impl_t::ckpt_create_ma_servicegrp\n"));

	ASSERT(pdtsp != NULL);
	pdtsp->dump_ma_servicegrp(g_name, nhash, policytype,
	    lb);
}

//
// Effects:  This checkpoint routine dumps the state of the master service
//   object itself identified by "ssap."
//
// Parameters:
//   - ssap (IN):	Service access point <protocol, ip addd, port>
//   - inslist (IN):    The list of of instances registered.
//   - ninst (IN):	The number of instances known to the master.
//   - gif_node (IN):   The global network interface node.
//   - load_dist (IN):  The current distribution of weights for the
//			load-balancer for this service object.
void
pdts_prov_impl_t::ckpt_dump_ma_service_state(
	const network::group_t		&group,			// IN
	const network::seqlong_t	&inslist,		// IN
	int32_t				ninst,			// IN
	uint32_t,							// IN
	const network::dist_info_t 	&load_dist,		// IN
	const network::seqlong_t	&configlist,		// IN
	int32_t				nconfig_inst,		// IN
	Environment			&)			// IN
{
	NET_DBG(("pdts_prov_impl_t::ckpt_ma_service_state()\n"));
	ASSERT(pdtsp != NULL);
	pdtsp->dump_ma_service_state(group, inslist, ninst,
		load_dist, configlist, nconfig_inst);
}

void
pdts_prov_impl_t::ckpt_dump_ma_serviceobj_state(
	const network::group_t		&group,			// IN
	const network::service_sap_t	&ssap,			// IN
	nodeid_t			gif_node, 		// IN
	const network::seqlong_t	&inslist,		// IN
	Environment			&)			// IN
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_ma_service_state()\n"));
	ASSERT(pdtsp != NULL);
	pdtsp->dump_ma_serviceobj_state(group, ssap, gif_node, inslist);
}

//
// Effects:  This checkpoint routine creates a new master PDT for the
//   master service object identified by "ssap."
//
// Parameters:
//   - node (IN):   Gif node of service where PDT will be created.
//   - ssap (IN):   Service access point <protocol, ip addd, port>
//
void
pdts_prov_impl_t::ckpt_create_ma_pdt(
			const network::group_t		&group,	// IN
			Environment			&)	// IN
{
	NET_DBG(("pdts_prov_impl_t::ckpt_create_ma_pdt()\n"));

	ASSERT(pdtsp != NULL);
	pdtsp->create_ma_pdt(group);
}

//
// Effects: This checkpoint routine dumps the state of a PDT table
//   whose associated master service object is identified by "ssap."
//
// Parameters:
//   - ssap (IN):   Service access point <protocol, ip addd, port>
//   - info (IN):   The pdtinfo struct that represents the contents of
//		a PDT in canonical form.
//
void
pdts_prov_impl_t::ckpt_dump_ma_pdtinfo(
				const network::group_t		&group,	// IN
				const network::pdtinfo_t	&info,	// IN
				Environment			&)	// IN
{
	NET_DBG(("pdts_prov_impl_t::ckpt_dump_ma_pdtinfo()\n"));

	ASSERT(pdtsp != NULL);
	pdtsp->create_pdt_state(group, info);
}

//
// Effects: This checkpoint routine dumps the config info of
//	a sticky service group identified by g_name.
//
// Parameters:
//	g_name:		Service group name
//	st:		Sticky config value
//
void
pdts_prov_impl_t::ckpt_dump_ma_sticky_config(
    const network::group_t		&g_name,	// IN
    const network::sticky_config_t	&st,		// IN
    Environment				&)		// IN
{
	network::ret_value_t retval = network::SUCCESS;

	NET_DBG(("pdts_prov_impl_t::ckpt_dump_ma_sticky_config"
	    "(%s, %d, %x)\n",
	    g_name.resource, st.st_timeout, st.st_flags));

	ASSERT(pdtsp != NULL);

	//
	// Don't care if the return is "changed" or "not changed", since
	// either way, the new config values are set.
	//
	(void) pdtsp->config_sticky_srv_grp1(g_name, st, &retval);

	ASSERT(retval == network::SUCCESS);
}


//
// Effects:  This checkpoint routine attaches the mcobj "mcobj_ref"
//   to the PDT server state on behalf of node "node."
//
// Parameters:
//   - node (IN):	The node where the mcobj resides.
//   - mcobj_ref (IN):	The object reference of the mcobj object.
//
void
pdts_prov_impl_t::ckpt_attach_mcobj(nodeid_t		node,		// IN
		network::mcobj_ptr			mcobj_ref,	// IN
		network::mcnet_node_ptr			servobjp,	// IN
		Environment				&e)		// IN
{
	NET_DBG(("pdts_prov_impl_t::ckpt_attach_mcobj()\n"));
	NET_DBG(("    node = %d, mcobj = 0x%x\n", node, mcobj_ref));

	// Create the secondary mcnet_node object here in the checkpoint,
	// if necessary.
	if (mcnet_node_array[node] == NULL) {
		mcnet_node_array[node] = new mcnet_node_impl_t(servobjp, this);
	}
	mcnet_node_array[node]->node = node;
	mcnet_node_array[node]->set_cbref(mcobj_ref);

	// Create a transaction state object in case of retries
	attach_mcobj_state *st = new attach_mcobj_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects:  This checkpoint routine causes state changes for a certain
//    master service object identified by "ssap" to register the
//    instance running at node "node."  The state changes result in
//    some data being returns, which we stick into a newly created
//    transaction state object for retries.  In the case of an HA
//    retry at pdts_impl_t::register_instance(), this data will be
//    returned in that routine's OUT parameter.
//
// Parameters:
//   - ssap (IN):	Service access point <protocol, ip adrss, port>
//   - node (IN):	The node where the instance is running.
//   - e (IN):		Environment variable for passing around txn state.
//
void
pdts_prov_impl_t::ckpt_register_inst(
				const network::service_sap_t &ssap,	// IN
				nodeid_t		node, 		// IN
				Environment		&e)		// IN
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_register_inst(%d)\n", node));

	ASSERT(pdtsp != NULL);
	bool found = pdtsp->register_instance1(ssap, node);

	// Create a transaction state object in case of retries.
	register_inst_state *st = new register_inst_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}

	if (!found) {
		return;
	}
}

//
// Effects:  This checkpoint routine causes state changes for a certain
//    master service object identified by "ssap" to deregister the
//    instance that was running at node "node."
//
// Parameters:
//   - ssap (IN):	Service access point <protocol, ip addd, port>
//   - node (IN):	The node where the instance was running.
//   - e (IN):		Environment variable for passing around txn state.
//
void
pdts_prov_impl_t::ckpt_deregister_inst(
			const network::service_sap_t &ssap,	// IN
			nodeid_t			node,	// IN
			Environment			&e)	// IN
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_deregister_inst(%d)\n", node));

	// Actually deregister the instance.
	ASSERT(pdtsp != NULL);
	bool found = pdtsp->deregister_instance1(ssap, node);
	if (!found) {
		return;
	}

	// Create a transaction state object in case of retries.
	deregister_inst_state *st = new deregister_inst_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects:  This checkpoint routine registers a "mux" with all master
//    service objects in the PDT server.  It invokes a routine locally
//    in the PDT server that causes the same state changes that occurred
//    at the primary without the necessity (and expense) of checkpointing
//    state changes themselves.  It creates a transaction state object in
//    the case of retries by the HA framework due to failures.
//
// Parameters:
//   - node (IN):	The node id of the gif node.
//   - e (IN):		Environment variable for passing around txn state.
//
void
pdts_prov_impl_t::ckpt_register_gifnode(nodeid_t		node,	// IN
					Environment		&e)	// IN
{
	NET_DBG(("pdts_prov_impl_t::ckpt_register_gifnode(%d)\n", node));

	ASSERT(pdtsp != NULL);

	pdtsp->register_gifnode1(node);

	// Create a transaction state object in case of retries.
	register_gifnode_state *st = new register_gifnode_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects:  This checkpoint routine makes the gifnode "node" the only one
//   attached to the scalable services that match "ssap."
//
// Parameters:
//   - ssap (IN):	Service access point <protocol, ip addd, port>
//   - node (IN): The node id of the node that is the GIF node.
//
void
pdts_prov_impl_t::ckpt_set_primary_gifnode(const network::service_sap_t &ssap,
		nodeid_t node, Environment &e)
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_set_primary_gifnode(%d)\n", node));

	ASSERT(pdtsp != NULL);
	pdtsp->set_primary_gifnode1(ssap, node, e);
	// XXX KCF
	e.clear();

	// Create a transaction state object in case of retries.
	set_primary_gifnode_state *st = new set_primary_gifnode_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects:  This checkpoint routine attaches the specified gifnode "node"
//   to services that are associated with this "ssap."
//
// Parameters:
//   - ssap (IN):	Service access point <protocol, ip addd, port>
//   - node (IN): The node id of the node that is the GIF node.
//
void
pdts_prov_impl_t::ckpt_attach_gifnode(const network::service_sap_t &ssap,
	nodeid_t node, Environment &e)
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_attach_gifnode(%d)\n", node));

	ASSERT(pdtsp != NULL);

	pdtsp->attach_gifnode1(ssap, node, e);
	e.clear();

	// Create a transaction state object in case of retries.
	attach_gifnode_state *st = new attach_gifnode_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

void
pdts_prov_impl_t::ckpt_set_ma_configlist(const network::group_t &gname,
		Environment &e)
{
	nodeid_t i;

	NET_DBG(("pdts_prov_impl_t::ckpt_set_ma_configlist()\n"));

	ASSERT(pdtsp != NULL);

	// Add all nodes to the group name ""
	for (i = 1; i <= NODEID_MAX; i++) {
		(void) pdtsp->add_nodeid(gname, i, e);
	}
}

void
pdts_prov_impl_t::ckpt_add_nodeid(
    const network::group_t	&g_name,	// IN
    nodeid_t			node,		// IN
    Environment			&e)		// IN
{
	network::ret_value_t retval;

	NET_DBG(("pdts_prov_impl_t::ckpt_add_nodeid(%d)\n", node));

	ASSERT(pdtsp != NULL);

	bool found = pdtsp->add_nodeid1(g_name, node, &retval);

	// Create a transaction state object in case of retries
	add_nodeid_state *st = new add_nodeid_state(found, retval);

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

void
pdts_prov_impl_t::ckpt_remove_nodeid(
    const network::group_t		&g_name,		// IN
    nodeid_t				node,			// IN
    Environment				&e)			// IN
{
	network::ret_value_t retval;
	bool pdt_updated;

	NET_DBG(("pdts_prov_impl_t::ckpt_remove_nodeid(%d)\n", node));

	ASSERT(pdtsp != NULL);

	retval = pdtsp->remove_nodeid1(g_name, node, pdt_updated);

	// Create a transaction state object in case of retries
	remove_nodeid_state *st =
	    new remove_nodeid_state(pdt_updated, retval);

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

void
pdts_prov_impl_t::ckpt_add_scalable_service(
    const network::group_t		&g_name,	// IN
    const network::service_sap_t	&ssap,		// IN
    Environment				&e)		// IN
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_add_scalable_service()\n"));

	ASSERT(pdtsp != NULL);

	pdtsp->register_service1(g_name, ssap);

	// Create a transaction state object in case of retries
	add_scalable_service_state *st = new add_scalable_service_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

void
pdts_prov_impl_t::ckpt_rem_scal_service(const network::group_t &gname,
	const network::service_sap_t &ssap, Environment &e)
{
	network::ret_value_t retval;
	bool updated_pdt;

	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_prov_impl_t::ckpt_rem_scal_service()\n"));

	ASSERT(pdtsp != NULL);

	retval = pdtsp->deregister_service1(gname, ssap, updated_pdt);

	// Create a transaction state object in case of retries
	rem_scal_service_state *st =
		new rem_scal_service_state(retval, ssap, updated_pdt);

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

void
pdts_prov_impl_t::ckpt_create_scal_srv_grp(const network::group_t &g_name,
		network::lb_policy_t policykind,
		network::lbobj_i_ptr primary_lb,
		Environment &e)
{
	network::ret_value_t retval;

	NET_DBG(("pdts_prov_impl_t::ckpt_create_scal_srv_grp()\n"));

	ASSERT(pdtsp != NULL);

	retval = pdtsp->create_scal_srv_grp1(g_name, policykind, primary_lb);

	// Create a transaction state object in case of retries.
	// Remember the return value of the invocation.  We'll use this
	// return value in the case of a retry.
	create_scal_srv_grp_state *st = new create_scal_srv_grp_state(retval);

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects: checkpoint routines on the secondary for checkpointing changes
//	to stickiness config for the named group.
//
// Parameters:
//	g_name:		Service group name
//	st:		Sticky config value
//
void
pdts_prov_impl_t::ckpt_config_sticky_srv_grp(
    const network::group_t		&g_name,	// IN
    const network::sticky_config_t	&st,		// IN
    Environment				&e)		// IN
{
	network::ret_value_t retval;

	NET_DBG(("pdts_prov_impl_t::ckpt_config_sticky_srv_grp"
	    "(%s, %d, %x)\n",
	    g_name.resource, st.st_timeout, st.st_flags));

	ASSERT(pdtsp != NULL);

	bool changed = pdtsp->config_sticky_srv_grp1(g_name, st, &retval);

	// Create a transaction state object in case of retries
	config_sticky_srv_grp_state *state = new
	    config_sticky_srv_grp_state(changed, retval);

	state->register_state(e);
	if (e.exception()) {
		//
		// No big deal, since config_sticky_srv_grp() is an
		// idempotent function itself. This could lead to
		// an unnecessary retry, if the primary fails here. But
		// that wouldn't be harmful.
		//
		e.clear();
	}
}

//
// Effects: checkpoint routines on the secondary for checkpointing changes
//	to RR config for the named group.
//
// Parameters:
//	g_name:		Service group name
//	st:		Sticky config value
//
void
pdts_prov_impl_t::ckpt_config_rr_srv_grp(
    const network::group_t		&g_name,	// IN
    const network::rr_config_t	&rr,			// IN
    Environment				&e)		// IN
{
	network::ret_value_t retval;

	NET_DBG(("pdts_prov_impl_t::ckpt_config_rr_srv_grp"
	    "(%s, %x, %d)\n",
	    g_name.resource, rr.rr_flags, rr.rr_conn_threshold));

	ASSERT(pdtsp != NULL);

	bool changed = pdtsp->config_rr_srv_grp1(g_name, rr, &retval);

	// Create a transaction state object in case of retries
	config_rr_srv_grp_state *state = new
	    config_rr_srv_grp_state(changed, retval);

	state->register_state(e);
	if (e.exception()) {
		e.clear();
	}
}

void
pdts_prov_impl_t::ckpt_delete_scal_srv_grp(const network::group_t &g_name,
		Environment &e)
{
	network::ret_value_t retval;

	NET_DBG(("pdts_prov_impl_t::ckpt_delete_scal_srv_grp()\n"));

	ASSERT(pdtsp != NULL);

	retval = pdtsp->delete_scal_srv_grp1(g_name);

	// Create a transaction state object in case of retries.
	// Remember the return value of the invocation.  We'll use this
	// return value in the case of a retry.
	delete_scal_srv_grp_state *st = new delete_scal_srv_grp_state(retval);

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects: Checkpoint change to the load balancer policy.
//
// Parameters:
//  - lb (IN):		Load balancer being updated.
//  - new_policy (IN):	New value for policy
//  - e (IN):		Environment variable for passing around exceptions.
//
void
pdts_prov_impl_t::ckpt_set_policy(network::lbobj_i_ptr,
	network::lb_specifier_t, Environment &)
{
	// This checkpoint is a stub for later expansion, when changing the
	// policy on the fly is permitted.
	CL_PANIC(!"unimplemented");
}

//
// Effects: Checkpoint change to the load balancer distribution.
//
// Parameters:
//  - lb (IN):		Load balancer being updated.
//  - dist (IN):	New value for distribution
//  - e (IN):		Environment variable for passing around exceptions.
//
void
pdts_prov_impl_t::ckpt_set_distribution(network::lbobj_i_ptr lb,
	const network::dist_info_t &dist, Environment &e)
{
	NET_DBG(("pdts_prov_impl_t::ckpt_set_distribution()\n"));

	lbobj *lb_ptr = lbobj::get_impl(lb);
	ASSERT(lb_ptr);
	lb_ptr->set_distribution_ii(dist, network::ckpt::_nil(), lb, false, e);
	e.clear();
}

//
// Effects: Checkpoint change to the load balancer distribution.
//
// Parameters:
//  - lb (IN):		Load balancer being updated.
//  - node (IN):	Node to update
//  - weight (IN):	New weight
//  - e (IN):		Environment variable for passing around exceptions.
//
void
pdts_prov_impl_t::ckpt_set_weight(network::weighted_lbobj_ptr lb,
	nodeid_t node, uint32_t weight, Environment &e)
{
	NET_DBG(("pdts_prov_impl_t::ckpt_set_weight(node=%d,wt=%d)\n",
		node, weight));

	//
	// lint gets confused between weighted_lbobj and the IDL interface
	// weighted_lbobj, so use ::weighted_lbobj to scope it.
	//

	::weighted_lbobj *lb_p = (::weighted_lbobj *)lbobj::get_impl(lb);

	lb_p->set_weight1(node, weight, false, e);
	e.clear();

	// Create a transaction state object in case of retries.
	lbobj_set_weight_state *st = new lbobj_set_weight_state();

	st->register_state(e);
	if (e.exception()) {
		e.exception()->print_exception("Failed to register state");
		e.clear();
	}
}

//
// Effects: Checkpoint change to the load balancer parameters.
//
// Parameters:
//  - lb (IN):		Load balancer being updated.
//  - parameters (IN):	New value for parameters.
//  - e (IN):		Environment variable for passing around exceptions.
//
void
pdts_prov_impl_t::ckpt_set_perf_mon_parameters(
	network::perf_mon_policy_ptr lb,
	const network::perf_mon_policy::perf_mon_parameters_t &parameters,
	Environment &)
{
	NET_DBG(("pdts_prov_impl_t::ckpt_set_perf_mon_parameters()\n"));
	perf_policy *lb_ptr = (perf_policy *)lbobj::get_impl(lb);
	lb_ptr->set_parameters_int(parameters, false);
}

//
// Effects: Checkpoint registration of a user load balancer.
//
// Parameters:
//  - lb (IN):		Load balancer being updated.
//  - user_lb (IN):	New load balancer being registered.
//  - e (IN):		Environment variable for passing around exceptions.
//
void
pdts_prov_impl_t::ckpt_user_policy(network::user_policy_ptr lb,
	network::lbobj_i_ptr user_lb, Environment &)
{
	NET_DBG(("pdts_prov_impl_t::ckpt_user_policy()\n"));
	user_lbobj *lb_ptr = (user_lbobj *)lbobj::get_impl(lb);
	lb_ptr->register_user_policy_int(user_lb, false);
}

// ************************************************************************
//
// Miscellaneous methods
//
// ************************************************************************
//
// Effects: This routine will only be invoked by "become_primary."
//
bool
pdts_prov_impl_t::create_service()
{
	NET_DBG(("pdts_prov_impl_t::create_service()\n"));

	// Create the PDT server replica object

	//
	// lint thinks this can be a memory leak, but it isn't because
	// create_service won't be called again without an intervening
	// call to become_spare() that will delete pdtsp
	//
	pdtsp = new pdts_impl_t(this);	//lint !e423
	if (pdtsp == 0) {
		cmn_err(CE_WARN, "Out of memory\n");
		return (false);
	}

	//
	// Create an object reference and make it part of the state of
	// this this provider object.  We do this to prevent a race
	// condition between add_secondary() (creates an object ref
	// and then releases it), which makes the object the target of
	// an _unreferenced) and mc_inet_init(), which gets the object ref
	// from the RM.
	//
	// Originally, we didn't create this "artificial" reference.
	// The implementation object had a reference count of 0.
	// When add_secondary() checkpointed the creation of the PDTS
	// implementation at a secondary, we created a reference for
	// the purpose of checkpointing, and then releasing it once
	// we were done; this caused the ref count to go to 1, and
	// then back down to 0.  This resulted in _unreferenced being
	// delivered to the PDTS implementation object and ultimately
	// destroying it.  Of course, this conflicted potentially with
	// other threads in the system trying to create a reference
	// while the object was being destroyed.  To solve this problem,
	// we decided that each HA replica provider object would hold
	// a single reference to the object when it was created, and
	// would be held forever.
	//
	pdts_v = pdtsp->get_objref();

	// Create the mcnet_node_array array of mcnet_node_impl_t objects.
	NET_DBG(("  call pdtsp->create_mcnet_node_array()\n"));

	int res = pdtsp->create_mcnet_node_array(this);
	if (res == -1) {
		cmn_err(CE_WARN, "pdts_prov_impl: Couldn't create the "
			"mcnet_node_array array\n");
		return (false);
	}

	return (true);
}

//
// Effects:  Return a pointer to the PDT server implementation object.
//
pdts_impl_t *
pdts_prov_impl_t::get_pdtsp()
{
	return (pdtsp);
}

//
// Effects: Returns true if this provider object is currently the
//   primary, else returns false.
//
bool
pdts_prov_impl_t::is_primary()
{
	return (prov_is_primary);
}

//
// Accessor function for the checkpoint object.
//
network::ckpt_ptr
pdts_prov_impl_t::get_checkpoint_network_ckpt()
{
	return (_ckpt_proxy);
}
