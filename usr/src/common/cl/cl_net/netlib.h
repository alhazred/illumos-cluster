/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NETLIB_H
#define	_NETLIB_H

#pragma ident	"@(#)netlib.h	1.102	08/05/20 SMI"

//
// Cluster networking library
//

#include <sys/stream.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <h/naming.h>
#include <h/streams.h>
#include <h/network.h>
#include <orb/object/adapter.h>
#include <sys/list_def.h>
#include <sys/mc_probe.h>

#include <cl_net/net_debug.h>

#if SOL_VERSION >= __s10
#ifdef _KERNEL
#define	SCTP_SUPPORT
#endif
#endif

//
// Some common V6 macros. Refer to inet/ip6.h (which is not installed
// on target systems, hence replicated here.)
//
#define	IPV6_VERSION	6
#define	IPV6_HDR_LEN	40
#define	V4_PART_OF_V6(v6)	v6.s6_addr32[3]

#define	SCALABLE_SERVICE_TAG	"Cluster.Networking"

#define	NULLNODE 0

//
// Macro to add up the 4 32-bit parts of a network::inaddr_t.
// This is used in hashing of addresses.
//
#define	SUMOF(x) (x.v6[0] ^ x.v6[1] ^ x.v6[2] ^ x.v6[3])

//
// Maximum number of times to retry sizing sequences
// for print_net_state ...
//
#define	GNS_MAX_RETRIES 5

#ifdef SCTP_SUPPORT
#define	ACTION_SCTP_BINDADDR_NOACTION 0
#define	ACTION_SCTP_BINDADDR_ADD 1
#define	ACTION_SCTP_BINDADDR_DEL 2
#endif

const uint32_t NODENAME_MAX = 50;

// Syslog for the networking module.
extern os::sc_syslog_msg *cl_net_syslog_msgp;

//
// Tunable parameter for hash table size.
// NB: this is not dynamically tunable, and must
// be set to the same value on all nodes or
// problems will be encountered.
//
// for example, in /etc/system:
//
//	set cl_net:pdt_hash_size = 24
//	set cl_net:bt_hash_size = 24
//	set cl_net:clnet_hastable_size = 100
//
extern uint32_t pdt_hash_size;
extern uint32_t bt_hash_size;
#ifdef SCTP_SUPPORT
extern uint32_t cn_hash_size;
#endif
extern uint32_t clnet_hashtable_size;

extern clock_t clnet_rr_timeout;

//
// Do not change this value. It is used for developement purpose.
//
extern uint32_t sticky_quiet_period;

//
// Common default stickiness values for both sl_servicegrp and
// ma_servicegrp -- timeout of 0, no udp, strong stickiness.
//
#define	INIT_STICKY_CONFIG(st) \
{				\
	st.st_timeout = 0;	\
	st.st_flags = 0;	\
}

//
// Common default RR values for sl_servicegrp and
// ma_servicegrp -- timeout of 0 minutes, no udp,rr disabled.
//

#define	INIT_RR_CONFIG(rr) \
{				\
	rr.rr_flags = 0;	\
	rr.rr_conn_threshold = 0;	\
}

//
// nodeid_array_t provides a compact way to store a set of nodeid's.
// It is a 32-bit quantity. If nodeid i is in this set, then the
// (i - 1)th bit position from the right is set.
//
typedef uint32_t nodeid_array_t;
#define	ISSET_NODEID(nid_array, nid) ((nid_array) & (1 << ((nid) - 1)))
#define	SET_NODEID(nid_array, nid) ((nid_array) |= (1 << ((nid) - 1)))
#define	CLR_NODEID(nid_array, nid) ((nid_array) &= ~(uint_t)(1 << ((nid) - 1)))
#define	ISLAST_NODEID(nid_array, nid) \
		(((nid_array)|(1 << ((nid) - 1))) == (1 << ((nid) - 1)))

//
// True iff address is either V6-unspecified (all zero) or V4-mapped
// INADDR_ANY.
//
inline bool addr6_is_inaddr_any(const network::inaddr_t &addr);

inline bool addr6_are_equal(const network::inaddr_t &addr1,
    const network::inaddr_t addr2);


extern uint32_t clnet_v6_mode;

extern void upgrade_inaddr(const network::inaddr_t &);
extern void upgrade_bindseq(const network::bind_t &);
extern void upgrade_fwdseq(const network::fwd_t &);

//
// Some basic stats concerning forwarding of packets
//
struct clnet_dbgstat {
	uint32_t	ndb_sendup;		// packets we do not care
	uint32_t	ndb_noaction;		// packets ever queued up
	uint32_t	ndb_drop;		// packets dropped
	uint32_t	ndb_local_forward;	// packets sent up locally
	uint32_t	ndb_remote_forward;	// packets sent remote
	uint32_t	ndb_remote_newunitdata;	// unitdata mblks created
	uint32_t	ndb_remote_newhdr;	// ethernet hdr mblks created
	uint32_t	ndb_remote_errors;	// failed to send remote
	uint32_t	ndb_rr_maxreached;	// RR max entries reached
};

//
// This class is a catchall for all routines that are accessible
// throughout a node.  Note that there there is exactly ONE instance
// of this object on each node of the cluster.
//
class netlib {
public:

	netlib() {};

	~netlib() {};

	network::PDTServer_ptr get_pdt_ref();

	char *protocol_to_string(uint8_t proto);

	char *ip4addr_to_string(uint32_t ip4addr);

	char *ipaddr_to_string(const network::inaddr_t &);

	char *lbpolicy_to_string(network::lb_policy_t policy);

	void get_nodename_from_nodeid(nodeid_t nodeid,
	    char nodename[NODENAME_MAX]);

private:
	//
	// This mutex protects the private variable following
	// "local_pdt_ref"
	//
	os::mutex_t pdt_ref_mutex;

	//
	// This variable holds an HA object reference to the current
	// primary PDT server.  This gives us a reference locally to the
	// current primary without having to talk to the name server
	// everytime we need a reference to the primary.
	//
	network::PDTServer_var local_pdt_ref;
};

extern netlib network_lib;

#define	DEF_PDT_HASH_SZ 1009	// Default PDT hash size
#define	MAX_PDT_HASH_SZ 1009 	// Maximum PDT hash size

#define	DEF_BT_HASH_SZ	1009	// Default binding table size
#define	MAX_BT_HASH_SZ	1009 	// Maximum binding table size
#define	MIN_BT_HASH_SZ	1 	// Minimum binding table size

#ifdef SCTP_SUPPORT
#define	DEF_CN_HASH_SZ	1009	// Default connection table size
#define	MAX_CN_HASH_SZ	1009 	// Maximum connection table size
#define	MIN_CN_HASH_SZ	1 	// Minimum connection table size
#endif  //  SCTP_SUPPORT

//
// TCP callbacks give us a pointer to a variable length address.
// Use the address family to decide how long the address is, and convert
// any V4 address to V4-mapped for subsequent processing.
// Result of conversion stored in dst.
//
inline void
copy_tcpaddr_to_addr(int af, const void *tcpaddr, network::inaddr_t *dst)
{
	if (af == AF_INET) {
		IN6_IPADDR_TO_V4MAPPED((*(uint32_t *)tcpaddr),
		    (in6_addr_t *)dst);
	} else if (af == AF_INET6) {
		*(in6_addr_t *)dst = *(in6_addr_t *)tcpaddr;
	} else {
		//
		// Some earlier checks should have prevented us from ever
		// getting here.
		//
		ASSERT(0);
	}
}

//
// Returns true if "policy" argument is either sticky or
// sticky wildcard, false otherwise.
//
inline bool
is_sticky(network::lb_specifier_t policy)
{
	return ((policy == network::LB_ST) || (policy == network::LB_SW));
}

//
// Returns true if "policy" argument is sticky wildcard,
// false otherwise.
//
inline bool
is_sticky_wildcard(network::lb_specifier_t policy)
{
	return (policy == network::LB_SW);
}

//
// Hash on the client source address/port, unless we're
// a sticky service, in which case hash only on the source
// address.
//
inline uint32_t
pdt_hash_func(const network::cid_t &cid, network::lb_specifier_t policy)
{
	if (is_sticky(policy))
		return (SUMOF(cid.ci_faddr) % pdt_hash_size);
	else
		return ((SUMOF(cid.ci_faddr) + cid.ci_fport)
		    % pdt_hash_size);
}

//
// PDT hash function used in code paths specific to sticky policies.
// NOTE: Must yield the same value with pdt_hash_func(cid, policy),
// where policy is some sticky policy, and cid.ci_faddr == faddr.
//
// This function is added to allow PDT hashing to be done for sticky
// services without having to pass in the load balancing policy.
//
inline uint32_t
pdt_faddr_hash_func(network::inaddr_t& faddr)
{
	return (SUMOF(faddr) % pdt_hash_size);
}



class pdte_hash_t {
public:
	pdte_hash_t();
	~pdte_hash_t();

	uint_t get_hashval();
	void set_hashval(uint_t hashval);
	uint_t get_seqnum();
	void set_seqnum(uint_t seqnum);

protected:
	unsigned int ph_seqnum;		// sequence number of bucket
	unsigned int ph_hashval;	// node signifying the instance
};


//
// Packet Dispatcher Table
// Basic PDT structure
//
class pdt: public _DList::ListElem {
public:

	pdt(uint_t n);
	virtual ~pdt();

	static unsigned long dump_pdtinfo(pdt *);
	static unsigned long dump_pdtinfo(const network::pdtinfo_t *);

	//
	// These flags are the possible seqnums when forwarding
	// list gets created.
	//
	enum {
		PDT_DROP_PKTS = -1,
		PDT_DROP_SYN = -2
	};

	//
	// These flags are used to specify the forwarding info collection
	// methods.
	//

	enum {
		PDT_COLLECT_SPECIFIC = 1,
		PDT_COLLECT_ALL = 2
	};

	int marshal_pdtinfo(network::pdtinfo_t& pdtinfo);
	int unmarshal_pdtinfo(const network::pdtinfo_t& pdtinfo);

	unsigned int pdt_gethashval(unsigned int index);
	void pdt_sethashval(unsigned int index, unsigned int hashval);

	int pdt_getbktseq(unsigned int index);
	void pdt_setbktseq(unsigned int index, int seqnum);

	void pdt_setflag(unsigned int on_flags);
	unsigned int pdt_getflag();

	int pdt_hashindex(network::cid_t& cid);
	unsigned int pdt_hash(network::cid_t& cid);

	//
	// size of PDT hash bucket
	//
	uint_t pdt_getnhash();

protected:
	uint_t	pdt_flag;		// Flag fields
	uint_t 	pdt_nhash;		// Number of hash entries
	pdte_hash_t	*pdte_hash;	// pointer to pdt array

	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	pdt(const pdt &);
	pdt &operator = (pdt &);
};

//
// Common PDT flag definitons
//
#define	PDT_INITIALIZED		0x1
#define	PDT_UNINITIALIZED	0x0

class frwd_cid : public _DList::ListElem {
public:
	frwd_cid(const network::cid_t& cid);
	~frwd_cid();
	network::cid_t fr_cid;
};

//
// frwd_info_t is the Object which encapsulates forwarding information
// associated with a collection of connection identifiers (cid).
// pdte_frwd_t object encapsulates per-bucket forwarding information.
// It contains 2 doubly linked list of frwd_info_t objects
// a) bucket-wide list of frwd_info_t objects
// b) per-node list of frwd_info_t objects which contains list of
//    of forwarding information objects to a specific node
//
//	pdte_frwd
//   -------------------
//  |			|
//  |			|
//  |-------------------
//  |			|
//  | pernodelists	|
//  |  -----------	|
//  | |		  |------> list of frwd_info_t maintained on a per-node basis
//  |  -----------	|
//  | |		  |------>
//  |  -----------	|
//  | |		  |------>
//  |  -----------	|
//  | |		  |------>
//  |  -----------	|
//  |			|
//   -------------------
//
//
typedef IntrList<frwd_cid, _DList> frwd_cid_list_t;

class frwd_info_t {
public:
	frwd_info_t();
	~frwd_info_t();

	void fr_dumpfrwd();

	bool fr_equal(const network::cid_t& cid, network::conn_state_t &state,
	    int change = 0);

	bool fr_setcid(const network::cid_t& cid);
	void fr_getcid(int ncids, network::cidseq_t& cids);

	void fr_setnode(nodeid_t node);
	nodeid_t fr_getnode();

	int fr_getncids();
	void fr_incrcids();

	bool  fr_delete(network::cid_t& cid);
	void clean_frwd_cid(const network::service_sap_t& ssap);

	// Accessors for list elements to put this frwd_info_t onto
	// multiple IntrLists
	_DList::ListElem	*get_pernode_elem();

	// mutex is used to lock the cid list.
	os::mutex_t frwd_cid_list_lock;

	frwd_cid_list_t frwd_cid_list;

	nodeid_t	fr_node;	// Node to forward requests to
	int		fr_ncids;

private:

	_DList::ListElem	pernode_elem;
};

typedef IntrList<frwd_info_t, _DList> frwd_info_list_t;

class pdte_frwd_t {
public:
	pdte_frwd_t();
	~pdte_frwd_t();

	//
	// Marhshal forwarding information associated with a pdte
	// bucket and for the previous entry in the bucket.
	void unmarshal_selective_frwd_info(const network::cid_t& cid,
	    nodeid_t node, int index);

	void clean_frwd_info(nodeid_t node);
	void clean_frwd_info_by_ssap(const network::service_sap_t& ssap);

	typedef int (*cbfunc_t)(nodeid_t node, network::service_sap_t& ssap,
	    nodeid_t gifnode, network::hashinfo_t& hashinfo,
	    network::fwd_t_var &fwdp, Environment& e);

	//
	// Method to search for an entry in the cid sequence
	//
	unsigned int lookup_change(const network::cid_t& cid,
	    network::conn_state_t &state, int change = 0);

	//
	// Method used to clear all usage bits. Required for RR
	//
	void resetall_usage_bit();

	//
	// Stop forwarding
	//
	int stop_forward(network::cid_t& cid, nodeid_t node);

	//
	// Boolean function to check if this frwding structure
	// is empty.
	//
	bool is_frwd_empty();

	frwd_info_list_t *pernodelists[NODEID_MAX+1];
	int nfrwd;	// Total number of entries in bucket list

	void pre_marshal_fwd(network::seqlong_t& cnt, nodeid_t node);
	void marshal_fwd(network::fwdseq_t& fwdinfo, nodeid_t node,
	    Environment& e);

private:

	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	pdte_frwd_t(const pdte_frwd_t &);
	pdte_frwd_t &operator = (pdte_frwd_t &);
};


//
// Basic service object definition
//
class servicegrp:public _DList::ListElem {
public:
	servicegrp(const network::group_t &group,
	    unsigned int nhash, network::lb_specifier_t policy_type =
	    network::LB_WEIGHTED);

	virtual ~servicegrp();

	//
	// size of PDT hash bucket
	//
	int getnhash();

	//
	// Load Balancing policy
	//
	network::lb_policy_t getpolicy();

	//
	// getssap to be moved to serviceobj if needed there
	// service sap
	//
	network::service_sap_t& getssap();

	//
	// Methods to access group name
	//
	bool is_group_equal(const network::group_t &group);
	network::group_t& getgroup();
	char *get_group_name();

protected:
	int			sr_nhash;	// size of PDT hash bucket
	network::lb_specifier_t	sr_policy;	// Load balancing policy
	network::group_t	sr_group;	// Holds the group names
};

//
// Basic service object definition
//
class serviceobj:public _DList::ListElem {
public:
	serviceobj(const network::service_sap_t &ssap);

	virtual ~serviceobj();

	bool equal(const network::service_sap_t &nsap);
	bool ip_equal(const network::service_sap_t &nsap);
	bool matches(const network::service_sap_t &nsap);
	bool ip_matches(const network::service_sap_t &nsap);

	//
	// service sap
	//
	network::service_sap_t& getssap();
	void set_gifnode(nodeid_t node);
	nodeid_t get_gifnode();

protected:
	network::service_sap_t	sr_sap;		// service sap
	nodeid_t		sr_gifnode;	// node that currently owns sap
};

class instanceobj:public _DList::ListElem {
public:
	instanceobj(nodeid_t node);

	nodeid_t get_instanceid();

	bool equal(nodeid_t node);

private:
	nodeid_t	ins_node;
};

//
// List of instance objects
//
typedef IntrList<instanceobj, _DList> instancelist_t;

#include <cl_net/netlib_in.h>

#ifndef _KERNEL

//
// When compiled for unode, strings.h is included and causes lint
// to complain about local "index" variables hiding the global symbol
// for index(3C).  We could rename all of them, but better to tell
// lint it is ok.
//
//lint -esym(578, index)
//

#endif	// _KERNEL

#endif	/* _NETLIB_H */
