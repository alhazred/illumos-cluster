//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)inet_port_mgmt.cc	1.112	09/02/13 SMI"

//
// Network Callback interfaces
//

extern "C" {

#include <sys/types.h>
#include <sys/errno.h>
#include <sys/disp.h>
#include <sys/sunddi.h>
#include <sys/socket.h>
#include <netinet/ip6.h>
#include <netinet/in.h>
#include <sys/disp.h>
#include <inet/led.h>
#ifdef _KERNEL
#include <sys/kstr.h>
#endif	// _KERNEL

}

#include <h/network.h>
#include <orb/infrastructure/orb_conf.h>
#include <nslib/ns.h>
#include <cl_net/netlib.h>
#include <cl_net/mcobj_impl.h>
#include <cl_net/muxq_impl.h>
#include <sys/mc_probe.h>
#include <sys/os.h>
#include <sys/rm_util.h>
#include <sys/sol_version.h>

#ifdef SCTP_SUPPORT
#include <cl_net/conn_table.h>

#define	MAX_ADDR_LIST_SIZE	(1024 * sizeof (network::inaddr_t))
#endif  // SCTP_SUPPORT

#if SOL_VERSION >= __s10
#include <sys/netstack.h>
#endif // SOL_VERSION >= __s10

//
// Globals available to the entire net module.
//
mcobj_impl_t			*mcobjp;
netlib				network_lib;
nodeid_t			mc_node;

#if (SOL_VERSION >= __s10) && defined(_KERNEL)

int clnet_version = 0;
int (*cl_tcp_walk_list_v0)(int (*callback)(cl_tcp_info_t *, void *), void *)
	= NULL;
int (*cl_tcp_walk_list_v1)(netstackid_t,
    int (*callback)(cl_tcp_info_t *, void *), void *) = NULL;

#endif // (SOL_VERSION >= __s10) && defined(_KERNEL)

extern "C" {

#if SOL_VERSION >= __s10
#ifdef _KERNEL

extern int (*cl_inet_connect2)(netstackid_t, uint8_t protocol,
	boolean_t is_outgoing, sa_family_t addr_family, uint8_t
	*laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport,
	void *);
extern void (*cl_inet_connect)(uint8_t protocol, sa_family_t
	addr_family, uint8_t *laddrp, in_port_t lport, uint8_t
	*faddrp, in_port_t fport);

#pragma	weak cl_inet_connect2
#pragma	weak cl_inet_connect

#else // _KERNEL

extern void (*cl_inet_bind)(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *);
extern void (*cl_inet_unbind)(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *);
extern void (*cl_inet_listen)(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *);
extern void (*cl_inet_unlisten)(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *);
extern int (*cl_inet_connect)(netstackid_t, uint8_t protocol, boolean_t
    is_outgoing, sa_family_t addr_family, uint8_t *laddrp, in_port_t lport,
    uint8_t *faddrp, in_port_t fport, void *);
extern void (*cl_inet_disconnect)(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, uint8_t *faddrp,
    in_port_t fport, void *);

#endif // _KERNEL

#else // SOL_VERSION >= __s10

extern void (*cl_inet_bind)(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport);
extern void (*cl_inet_unbind)(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport);
extern void (*cl_inet_listen)(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport);
extern void (*cl_inet_unlisten)(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport);
extern void (*cl_inet_connect)(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport);
extern void (*cl_inet_disconnect)(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport);

#endif // SOL_VERSION >= __s10
#ifdef SCTP_SUPPORT
//
// SCTP Callbacks
//
extern void (*cl_sctp_listen)(sa_family_t addr_family, uint8_t *laddrp,
    uint_t nladdrs, in_port_t lport);
extern void (*cl_sctp_unlisten)(sa_family_t addr_family, uint8_t *laddrp,
    uint_t nladdrs, in_port_t lport);
extern void (*cl_sctp_connect)(sa_family_t addr_family, uint8_t *laddrp,
    uint_t nladdrs, in_port_t lport, uint8_t *faddrp, uint_t nfaddrs,
    in_port_t fport, boolean_t can_block, cl_sctp_handle_t handle);
extern void (*cl_sctp_disconnect)(sa_family_t addr_family,
    cl_sctp_handle_t handle);
extern void (*cl_sctp_assoc_change)(sa_family_t addr_family, uint8_t *aaddrp,
    size_t asize, uint_t naaddrs, uint8_t *daddrp, size_t dsize,
    uint_t ndaddrs, int end, cl_sctp_handle_t handle);
extern void (*cl_sctp_check_addrs)(sa_family_t addr_family, in_port_t port,
    uint8_t **addrp, size_t size, uint_t *naddrs, boolean_t is_inaddr_any);

#pragma weak cl_sctp_listen
#pragma weak cl_sctp_unlisten
#pragma weak cl_sctp_connect
#pragma weak cl_sctp_disconnect
#pragma weak cl_sctp_assoc_change
#pragma weak cl_sctp_check_addrs

#endif /* SCTP_SUPPORT */

//
// Version 0 callback interfaces. These exist for backward compatibility
// and should be removed in the future.
//

//
// Store all the port numbers in host order and all the network addresses in
// network order.
//

void
cl_private_inet_bind_v0(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport)
{
	//
	// An optim. for TCP that may affect the addr_family check benignly.
	//
	if (protocol != IPPROTO_UDP) {
		return;
	}

	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}
	lport = ntohs(lport);
	network::service_sap_t ssap;
	ssap.protocol = protocol;
	copy_tcpaddr_to_addr(addr_family, laddrp, &ssap.laddr);
	ssap.lport = lport;

	int rc = mcobjp->isregistered(ssap);
	if (rc == 1) {
		MC_PROBE_1(cl_private_inet_bind, "mc net cl_inet", "",
			tnf_int, lport, lport);
		(void) mcobjp->mc_checknset(ssap, mcobj_impl_t::BIND);
		return;
	}
}

void
cl_private_inet_unbind_v0(uint8_t protocol, sa_family_t addr_family,
	uint8_t *laddrp, in_port_t lport)
{
	//
	// An optim. for TCP that may affect the addr_family check benignly.
	//
	if (protocol != IPPROTO_UDP) {
		return;
	}

	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}

	lport = ntohs(lport);
	network::service_sap_t ssap;
	ssap.protocol = protocol;
	copy_tcpaddr_to_addr(addr_family, laddrp, &ssap.laddr);
	ssap.lport = lport;
	MC_PROBE_1(cl_private_inet_unbind, "mc net cl_inet", "",
		tnf_int, lport, lport);
	int rc = mcobjp->isregistered(ssap);
	if (rc == 1) {
		MC_PROBE_0(cl_private_inet_unbind_deregister,
		    "mc net cl_inet", "");
		(void) mcobjp->mc_checknset(ssap, mcobj_impl_t::UNBIND);
		return;
	}
}

void
cl_private_inet_listen_v0(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport)
{
	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}

	lport = ntohs(lport);
	if (protocol == IPPROTO_TCP) {
		network::service_sap_t ssap;

		ssap.protocol = protocol;
		copy_tcpaddr_to_addr(addr_family, laddrp, &ssap.laddr);
		ssap.lport = lport;

		int rc = mcobjp->isregistered(ssap);
		if (rc == 1) {
			MC_PROBE_1(cl_private_inet_listen,
			    "mc net cl_inet", "",
				tnf_int, lport, lport);
			(void) mcobjp->mc_checknset(
				ssap, mcobj_impl_t::LISTEN);
			return;
		}
	}
}

void
cl_private_inet_unlisten_v0(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport)
{
	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}

	lport = ntohs(lport);
	if (protocol == IPPROTO_TCP) {
		network::service_sap_t ssap;
		ssap.protocol = protocol;
		copy_tcpaddr_to_addr(addr_family, laddrp, &ssap.laddr);
		ssap.lport = lport;
		MC_PROBE_1(cl_private_inet_unlisten, "mc net cl_inet", "",
			tnf_int, lport, lport);
		int rc = mcobjp->isregistered(ssap);
		if (rc == 1) {
			MC_PROBE_0(cl_private_inet_unlisten_deregister,
			    "mc net cl_inet", "");
			(void) mcobjp->mc_checknset(ssap,
				mcobj_impl_t::UNLISTEN);
			return;
		}
	}
}

void
cl_private_inet_connect_v0(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport)
{
	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}

	lport = ntohs(lport);
	fport = ntohs(fport);
	if (protocol == IPPROTO_TCP) {
		network::service_sap_t ssap;
		network::cid_t cid;

		cid.ci_protocol = ssap.protocol = protocol;
		copy_tcpaddr_to_addr(addr_family, laddrp, &cid.ci_laddr);
		copy_tcpaddr_to_addr(addr_family, laddrp, &ssap.laddr);
		cid.ci_lport = ssap.lport = lport;
		copy_tcpaddr_to_addr(addr_family, faddrp, &cid.ci_faddr);
		cid.ci_fport = fport;

		int rc = mcobjp->isregistered(ssap);
		if (rc == 1) {
			MC_PROBE_2(cl_private_inet_connect,
			    "mc net cl_inet", "",
			    tnf_string, faddr,
			    network_lib.ipaddr_to_string(cid.ci_faddr),
			    tnf_int, fport, fport);
			mcobjp->mc_openconn(ssap, cid);
			return;
		}
	}
}

void
cl_private_inet_disconnect_v0(uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport)
{

	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}

	lport = ntohs(lport);
	fport = ntohs(fport);
	if (protocol == IPPROTO_TCP) {
		network::service_sap_t ssap;
		network::cid_t cid;

		cid.ci_protocol = ssap.protocol = protocol;
		copy_tcpaddr_to_addr(addr_family, laddrp, &cid.ci_laddr);
		copy_tcpaddr_to_addr(addr_family, laddrp, &ssap.laddr);
		cid.ci_lport = ssap.lport = lport;
		copy_tcpaddr_to_addr(addr_family, faddrp, &cid.ci_faddr);
		cid.ci_fport = fport;

		int rc = mcobjp->isregistered(ssap);
		if (rc == 1) {
			MC_PROBE_2(cl_private_inet_disconnect,
			    "mc net cl_inet", "",
			    tnf_string, faddr,
			    network_lib.ipaddr_to_string(cid.ci_faddr),
			    tnf_int, fport, fport);
			mcobjp->mc_closeconn(ssap, cid);
			return;
		}
	}
}

#if SOL_VERSION >= __s10
//
// Version 1 callback functions introduced by the outgoing connections
// support changes.
void
cl_private_inet_bind_v1(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *)
{
	cl_private_inet_bind_v0(protocol, addr_family, laddrp, lport);
}

void
cl_private_inet_unbind_v1(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *)
{
	cl_private_inet_unbind_v0(protocol, addr_family, laddrp, lport);
}


void
cl_private_inet_listen_v1(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *)
{
	cl_private_inet_listen_v0(protocol, addr_family, laddrp, lport);
}

void
cl_private_inet_unlisten_v1(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, void *)
{
	cl_private_inet_unlisten_v0(protocol, addr_family, laddrp, lport);
}

int
cl_private_inet_connect_v1(netstackid_t, uint8_t protocol, boolean_t
    is_outgoing, sa_family_t addr_family, uint8_t *laddrp, in_port_t
    lport, uint8_t *faddrp, in_port_t fport, void *)
{
	if (!is_outgoing) {
		cl_private_inet_connect_v0(protocol, addr_family, laddrp,
		    lport, faddrp, fport);
	}
	return (0);
}

void
cl_private_inet_disconnect_v1(netstackid_t, uint8_t protocol, sa_family_t
    addr_family, uint8_t *laddrp, in_port_t lport, uint8_t *faddrp,
    in_port_t fport, void *)
{
	cl_private_inet_disconnect_v0(protocol, addr_family, laddrp,
	    lport, faddrp, fport);
}


#endif // SOL_VERSION >= __s10

#ifdef SCTP_SUPPORT

//
// SCTP listen callback implementation. Cluster code need to free memory
// allocated in laddrp array.
//
void
cl_private_sctp_listen(sa_family_t addr_family, uint8_t *laddrp,
    uint_t nladdrs, in_port_t lport)
{
	//
	// Ensure we face only ipv4 and ipv6 traffic.
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		kmem_free(laddrp, nladdrs * sizeof (in6_addr_t));
		return;
	}

	uint8_t protocol = IPPROTO_SCTP;
	uint8_t *waddrp = laddrp;
	lport = ntohs(lport);

	//
	// Addresses passed either V6 or V4MAPPED
	//
	addr_family = AF_INET6;

	network::service_sap_t ssap;
	int rc = 0;

	ssap.protocol = protocol;
	ssap.lport = lport;

	for (uint_t i = 0; i < nladdrs; i++) {
		copy_tcpaddr_to_addr(addr_family, waddrp, &ssap.laddr);
		rc = mcobjp->isregistered(ssap);
		if (rc == 1) {
			MC_PROBE_1(cl_private_sctp_listen,
			    "mc net cl_inet", "",
			    tnf_int, lport, lport);
			(void) mcobjp->mc_checknset(
			    ssap, mcobj_impl_t::LISTEN);
		}
		waddrp = waddrp + sizeof (in6_addr_t);
	}
	waddrp = NULL;
	kmem_free(laddrp, nladdrs * sizeof (in6_addr_t));
}

//
// SCTP unlisten callback implementation. Cluster code need to free memory
// allocated in laddrp array.
//
void
cl_private_sctp_unlisten(sa_family_t addr_family, uint8_t *laddrp,
    uint_t nladdrs, in_port_t lport)
{
	//
	// Ensure we face only ipv4 and ipv6 traffic.
	// Addresses passed are either V6 or V4MAPPED
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		kmem_free(laddrp, nladdrs * sizeof (in6_addr_t));
		return;
	}

	uint8_t protocol = IPPROTO_SCTP;
	uint8_t *waddrp = laddrp;
	lport = ntohs(lport);
	addr_family = AF_INET6;

	network::service_sap_t ssap;
	int rc = 0;

	ssap.protocol = protocol;
	ssap.lport = lport;

	for (uint_t i = 0; i < nladdrs; i++) {
		copy_tcpaddr_to_addr(addr_family, waddrp, &ssap.laddr);
		rc = mcobjp->isregistered(ssap);
		if (rc == 1) {
			MC_PROBE_1(cl_private_sctp_unlisten,
			    "mc net cl_inet", "",
			    tnf_int, lport, lport);
			(void) mcobjp->mc_checknset(
			    ssap, mcobj_impl_t::UNLISTEN);
		}
		waddrp = waddrp + sizeof (in6_addr_t);
	}
	waddrp = NULL;
	kmem_free(laddrp, nladdrs * sizeof (in6_addr_t));
}

//
// SCTP connect callback implementation
// Argument can_block is B_TRUE when called during initial phase
// of connection establishment when server node initiates connection.
// If can_block is B_TRUE the code can block and GIF node updation is
// done synchronously.
// can_block is B_FALSE when called during connection establishment
// when server node acts as server. In this case code cannot block
// and GIF node updation is done asynchronously.
//
// handle is used as the index to track SCTP connection information.
// SCTP connection information are stored in the connection table and
// handle is used as key to retrieve information from the table during
// disconnection and assoc_change.
//
// Sun Cluster code is responsible in freeing up the memory at laddrp/faddrp.
//
void
cl_private_sctp_connect(sa_family_t addr_family, uint8_t *laddrp,
    uint_t nladdrs, in_port_t lport, uint8_t *faddrp,
    uint_t nfaddrs, in_port_t fport, boolean_t can_block,
    cl_sctp_handle_t handle)
{
	//
	// Ensure we face only ipv4 and ipv6 traffic.
	// Addresses passes are either V6 or V4MAPPED
	//
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		kmem_free(laddrp, nladdrs * sizeof (in6_addr_t));
		kmem_free(faddrp, nfaddrs * sizeof (in6_addr_t));
		return;
	}

	addr_family = AF_INET6;
	uint8_t protocol = IPPROTO_SCTP;
	uint8_t *wladdrp = laddrp;
	int rc = 0;

	lport = ntohs(lport);
	fport = ntohs(fport);

	network::service_sap_t ssap;

	//
	// It will be sufficient to use any single local addr to lookup
	// service group as single server address is enough to uniquely
	// identify a service group. It is also OK to use any addr as local
	// address for connection entries in the forwarding lists
	// because inside a PDT, cid entry is identified by the
	// foreign address only.
	//
	ssap.protocol = protocol;
	ssap.lport = lport;

	//
	// Incase of scalable application connect, one of the local
	// endpoint (sap) of the connection should match (all may
	// not match, as endpoints may contain local non-shared
	// addresses as well, according to the policy adapted) one of
	// the registered ssap's. If none of the local endpoint
	// matches registered service, this will be considered as
	// a connection to a non-scalable application and we just
	// return. Note, we are not really doing any validation here.
	//
	for (uint_t i = 0; i < nladdrs; i++) {
		copy_tcpaddr_to_addr(addr_family, wladdrp, &ssap.laddr);

		if ((rc = mcobjp->isregistered(ssap)) == 1)
			break;
		wladdrp = wladdrp + sizeof (in6_addr_t);
	}
	wladdrp = NULL;
	kmem_free(laddrp, nladdrs * sizeof (in6_addr_t));
	if (rc != 0) {
		//
		// At this point we have one sap which maps
		// to a service group.
		//
		mcobjp->mc_multi_openconn(handle, addr_family, ssap,
		    nfaddrs, faddrp, fport, can_block, B_FALSE, (size_t)0);
	} else {
		kmem_free(faddrp, nfaddrs * sizeof (in6_addr_t));
	}
}

//
// SCTP disconnect callback implementation
// SCTP connection info is retrieved with handle from the connection
// table
//

void
cl_private_sctp_disconnect(sa_family_t addr_family, cl_sctp_handle_t handle)
{
	if (addr_family != AF_INET && addr_family != AF_INET6) {
		return;
	}

	conn_entry_t *entry;
	//
	// Extract connection info from connection table indexed by handle
	//
	entry = mcobjp->mc_conntab->lookup_entry(handle);
	if (entry == NULL) {
		//
		// No such connection registered in the connection table
		//
		return;
	}

	network::service_sap_t ssap;
	ssap = entry->cn_ssap;
	uint_t nfaddrs = entry->cn_nfaddrs;
	uint8_t *faddrp = entry->cn_faddrp;
	in_port_t fport = entry->cn_fport;

	if (mcobjp->isregistered(ssap) != 1) {
		return;
	}

	mcobjp->mc_multi_closeconn(ssap, nfaddrs, faddrp, fport, handle,
	    B_FALSE);
}

//
// SCTP assoc_change callback implementation
// This is called when association changes at either end wrt addresses
// used.
// aaddrp: pointer to addr set added
// asize: size of the address set array
// naaddrs: no. of addresses added
// daddrp: pointer to addr set deleted
// dsize: size of the address set array
// ndaddrs: no. of addresses deleted
// end: SCTP_CL_LADDR or SCTP_CL_FADDR
//
// Note: Suncluster code is responsibile in freeing up the memory at
// aaddrp/ daddrp
//
void cl_private_sctp_assoc_change(sa_family_t addr_family, uint8_t *aaddrp,
    size_t asize, uint_t naaddrs, uint8_t *daddrp, size_t dsize,
    uint_t ndaddrs, int end, cl_sctp_handle_t handle)
{
	//
	// Do nothing when address is change in at the server node end
	//
	if (end == SCTP_CL_LADDR) {
		if (asize > 0)
			kmem_free(aaddrp, asize);
		if (dsize > 0)
			kmem_free(daddrp, dsize);
		return;
	}

	if (addr_family != AF_INET && addr_family != AF_INET6) {
		if (asize > 0)
			kmem_free(aaddrp, asize);
		if (dsize > 0)
			kmem_free(daddrp, dsize);
		return;
	}

	//
	// Extract SCTP connection info
	//
	conn_entry_t *entry;
	entry = mcobjp->mc_conntab->lookup_entry(handle);

	//
	// Entry not in the connection table. Means not scalable service
	//
	if (entry == NULL) {
		if (asize > 0)
			kmem_free(aaddrp, asize);
		if (dsize > 0)
			kmem_free(daddrp, dsize);
		return;
	}

	network::service_sap_t ssap;
	ssap = entry->cn_ssap;

	//
	// Generally should be registered as we store only those entries in
	// the connection table which are registered.
	//
	if (mcobjp->isregistered(ssap) != 1) {
		if (asize > 0)
			kmem_free(aaddrp, asize);
		if (dsize > 0)
			kmem_free(daddrp, dsize);
		return;
	}

	addr_family = AF_INET6;

	if (naaddrs > 0) {
		mcobjp->mc_multi_openconn(handle, addr_family, ssap,
		    naaddrs, aaddrp, entry->cn_fport, B_FALSE, B_TRUE, asize);
	} else {
		if (asize > 0)
			kmem_free(aaddrp, asize);
	}

	if (ndaddrs > 0) {
		mcobjp->mc_multi_closeconn(ssap, ndaddrs, daddrp,
		    entry->cn_fport, handle, B_TRUE);
	}
	if (dsize > 0)
		kmem_free(daddrp, dsize);
}

//
// addrp is the pointer to the array of addresses the bind is called for.
// Incase of INADDR_ANY addrp will contain the full list of addresses
// available in the host. Note, this list may or may not contain the
// shared addresses depending on GIF or non-GIF node. The function is not
// called if function pointer is NULL. Note, the same callback function
// will be called incase of sctp_bindx() call with is_inaddr_any = false.
// This callback function will manipulate the address list and return the
// valid set of addresses through the same argument (addrp). The logic of
// address selection is as follows:
// 1. If service uses 'sticky wildcard' policy and there is no matching
//	service return the address list as is.
// 2. If the port is not-Loadbalanced, all shared addresses are removed
//	from the list
// 3. If the port is loadbalanced, find the set of shared addresses
//	configured for that port (set X)
//
//	3.1 If is_inaddr_any == false (means explicit bind), return the
//		intersection of set X with input addrp.
//	3.2 Else, return X
//

void
cl_private_sctp_check_addrs(sa_family_t addr_family, in_port_t port,
    uint8_t **addrp, size_t size, uint_t *naddrs,
    boolean_t is_inaddr_any)
{

	uint8_t			*waddrp = *addrp;
	uint_t			count = 0;
	struct sockaddr_in	*sin4 = NULL, *tsin4 = NULL;
	struct sockaddr_in6	*sin6 = NULL, *tsin6 = NULL;

	network::PDTServer_var	local_pdt_ref;
	network::inaddr_t	taddr;
	uint8_t			*paddrp;
	uint_t			npaddrs = 0;
	int			action = ACTION_SCTP_BINDADDR_NOACTION;
	int			wildcard = 0;
	size_t			max_addrs = 0;
	size_t			sock4_siz = 0;
	size_t			sock6_siz = 0;
	uint_t			tnaddrs = 0;

	port = ntohs(port);
	sock4_siz = sizeof (struct sockaddr_in);
	sock6_siz = sizeof (struct sockaddr_in6);

	// To get get rid of lint errors

	sin4 = (struct sockaddr_in *)waddrp;
	tsin4 = (struct sockaddr_in *)waddrp;
	sin6 = (struct sockaddr_in6 *)waddrp;
	tsin6 = (struct sockaddr_in6 *)waddrp;

	if (addr_family != AF_INET && addr_family != AF_INET6)
		return;

	paddrp = (uint8_t *)kmem_alloc(MAX_ADDR_LIST_SIZE, KM_SLEEP);
	//
	// Returns shared address list corresponding to a matching service
	// group if not found return all shared addresses. Returns ADD as
	// action if matches returns DEL otherwise
	//
	bool r = mcobjp->list_addr_against_port(port, (uint8_t **)&paddrp,
	    &npaddrs, MAX_ADDR_LIST_SIZE, &action, &wildcard);

	//
	// MAX_ADDR_LIST_SIZE was not enough to accomodate collection of
	// shared addresses (may contain redundant addresses) used by scalable
	// services.
	//
	if (!r) {
		//
		// SCMSGS
		// @explanation
		// The number of scalable services multiplied by the number of
		// shared addrsses exceeds the maximum allowed value of 1024.
		// @user_action
		// Either reduce the number of scalable applications in the
		// cluster or reduce the number of shared addresses that are
		// used.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Internal Error: Could not validate the SCTP bind"
		    " address list due to array overflow.");
		*naddrs = 0;
		goto done;
	}

	if (action == ACTION_SCTP_BINDADDR_NOACTION)
		goto done;

	//
	// No action incase of no matching port number found in the registered
	// services and also one or more services are having sticky wildcard
	// policy. We expect correct explicit bind from application if they
	// choose sticky wildcard policy for one of the services.
	//
	if (wildcard && action == ACTION_SCTP_BINDADDR_DEL) {
		goto done;
	}

	//
	// Returns same addr list as returned by list_addr_against_port
	//
	if (action == ACTION_SCTP_BINDADDR_ADD && is_inaddr_any) {
		if (addr_family == AF_INET) {
			max_addrs = size / sizeof (struct sockaddr_in);
			if (npaddrs > max_addrs) {
				//
				// SCMSGS
				// @explanation
				// Number of addresses that are configured in
				// the node has exceeds the maximum value.
				// @user_action
				// Reduce the number of addresses that are
				// plumbed in the system.
				//
				(void) cl_net_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "SCTP bind address validation failed "
				    "because the size of the address list "
				    "exceeds the expected value.");
				*naddrs = 0;
				goto done;
			}
			sin4 = (struct sockaddr_in *)waddrp;
			struct in6_addr *pin6 = (struct in6_addr *)paddrp;
			for (uint_t i = 0; i < npaddrs; i++) {
				sin4->sin_family = AF_INET;
				sin4->sin_port = port;
				IN6_V4MAPPED_TO_INADDR(pin6, &sin4->sin_addr);
				sin4 ++;
				pin6 ++;
			}
		} else {
			// Assuming AF_INET6

			max_addrs = size / sizeof (struct sockaddr_in6);
			if (npaddrs > max_addrs) {
				(void) cl_net_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "SCTP bind address validation failed "
				    "because the size of the address list "
				    "exceeds the expected value.");
				*naddrs = 0;
				goto done;
			}

			sin6 = (struct sockaddr_in6  *)waddrp;
			struct in6_addr *pin6 = (struct in6_addr *)paddrp;
			for (uint_t i = 0; i < npaddrs; i++) {
				sin6->sin6_family = AF_INET6;
				sin6->sin6_port = port;
				bcopy(pin6, &sin6->sin6_addr,
				    sizeof (struct in6_addr));
				sin6 ++;
				pin6 ++;
			}
		}
		*naddrs = npaddrs;
		goto done;
	}

	tnaddrs = *naddrs;
	*naddrs = 0;
	//
	// Returns initial addr set and list_addr_against_port addr set
	// intersection
	//
	if (action == ACTION_SCTP_BINDADDR_ADD && !is_inaddr_any) {
		if (addr_family == AF_INET) {
			sin4 = (struct sockaddr_in *)waddrp;
			tsin4 = (struct sockaddr_in *)waddrp;
		} else {
			sin6 = (struct sockaddr_in6 *)waddrp;
			tsin6 = (struct sockaddr_in6 *)waddrp;
		}

		network::inaddr_t *pin6;
		for (uint_t i = 0; i < tnaddrs; i++) {
			pin6 = (network::inaddr_t *) paddrp;
			for (uint_t j = 0; j < npaddrs; j++) {
				if (addr_family == AF_INET) {
					copy_tcpaddr_to_addr(addr_family,
					    &sin4->sin_addr, &taddr);
				} else {
					copy_tcpaddr_to_addr(addr_family,
					    &sin6->sin6_addr, &taddr);
				}
				if (addr6_are_equal(*pin6, taddr)) {
					if (addr_family == AF_INET) {
						if (tsin4 != sin4) {
							bcopy(sin4, tsin4,
							    sock4_siz);
						}
						tsin4 ++;
					} else {
						if (tsin6 != sin6) {
							bcopy(sin6, tsin6,
							    sock6_siz);
						}
						tsin6 ++;
					}
					(*naddrs) ++;
					break;
				}
				pin6 ++;
			}
			if (addr_family == AF_INET) {
				sin4 ++;
			} else {
				sin6 ++;
			}
		}
		goto done;
	}

	//
	// Returns initial addr set(A) - list_addr_against_port addr set(B)
	// - For each element in A, say 'a'.
	// - If 'a' does not belong to B, copy it over to the final
	// list, which in this case is basically 'A' again.
	// - If 'a' belongs to B, skip over it and do not copy it to the
	// final list and go to the next element in A. In this case, we
	// do not copy over 'a' to the final list, which is basically 'A'.
	// But we actually already have
	// it in there. And it will get overwritten at some point by another
	// element which does not belong to B or will be freed at the end.
	// That way it will no longer be a part of 'A'.
	//
	if (action == ACTION_SCTP_BINDADDR_DEL) {
		if (addr_family == AF_INET) {
			sin4 = (struct sockaddr_in *)waddrp;
			tsin4 = (struct sockaddr_in *)waddrp;
		} else {
			sin6 = (struct sockaddr_in6 *)waddrp;
			tsin6 = (struct sockaddr_in6 *)waddrp;
		}

		network::inaddr_t *pin6;
		bool found;
		for (uint_t i = 0; i < tnaddrs; i++) {
			pin6 = (network::inaddr_t *) paddrp;
			found = false;
			for (uint_t j = 0; j < npaddrs; j++) {
				if (addr_family == AF_INET) {
					copy_tcpaddr_to_addr(addr_family,
					    &sin4->sin_addr, &taddr);
				} else {
					copy_tcpaddr_to_addr(addr_family,
					    &sin6->sin6_addr, &taddr);
				}
				if (addr6_are_equal(*pin6, taddr)) {
					found = true;
					break;
				}
				pin6 ++;
			}
			if (!found) {
				if (addr_family == AF_INET) {
					if (tsin4 != sin4) {
						bcopy(sin4, tsin4,
						    sock4_siz);
					}
					tsin4 ++;
				} else {
					if (tsin6 != sin6) {
						bcopy(sin6, tsin6,
						    sock6_siz);
					}
					tsin6 ++;
				}
				(*naddrs) ++;
			}

			if (addr_family == AF_INET) {
				sin4 ++;
			} else {
				sin6 ++;
			}
		}
		goto done;
	}

done:
	if (paddrp) {
		kmem_free(paddrp, MAX_ADDR_LIST_SIZE);
	}
}

#endif // SCTP_SUPORT

#ifdef _KERNEL	// no fast path code for unode

void
cl_net_if()
{
	extern fpconf *fpc;

	fpc->fp_ns_if();
}

#endif	// _KERNEL

} // end extern "C"

int
mc_inet_init()
{
	network::PDTServer_var		local_pdt_ref;
	Environment			e;
	unsigned long			hook_enabled = 0L;

	mc_node = orb_conf::local_nodeid();

	//
	// Set the reference to the HA PDTServer object.
	//
	local_pdt_ref = network_lib.get_pdt_ref();
	if (CORBA::is_nil(local_pdt_ref)) {
		return (ENXIO);			// couldn't narrow
	}

	//
	// Create a mcobj object and attach it to PDT server.
	// This is used by the PDT server for callback functions.
	// The constructor for mcobj_impl_t also starts up the
	// dispatcher thread for registering/deregistering instances.
	//
	if ((mcobjp = new mcobj_impl_t()) == NULL)
		return (ENOMEM);

	//
	// Once we've performed a get_objref on the mcobj, we need
	// to coordinate with the unreferenced protocol to cleanup
	// properly.  For now, we panic.
	//
	if (mcobjp->init(local_pdt_ref) != NULL)
		CL_PANIC(!"mc_inet_init: Error initializing mcobj");

	//
	// Initialize tcp/ip hook function vectors
	//
#if SOL_VERSION >= __s10
#ifdef _KERNEL
	void *connect2ptr = (void *)modlookup("ip", "cl_inet_connect2");

	if (connect2ptr != NULL) {

		extern void (*cl_inet_bind)(netstackid_t, uint8_t protocol,
		    sa_family_t addr_family, uint8_t *laddrp, in_port_t lport,
		    void *);
		extern void (*cl_inet_unbind)(netstackid_t, uint8_t protocol,
		    sa_family_t addr_family, uint8_t *laddrp, in_port_t lport,
		    void *);
		extern void (*cl_inet_listen)(netstackid_t, uint8_t protocol,
		    sa_family_t addr_family, uint8_t *laddrp, in_port_t lport,
		    void *);
		extern void (*cl_inet_unlisten)(netstackid_t, uint8_t protocol,
		    sa_family_t addr_family, uint8_t *laddrp, in_port_t lport,
		    void *);
		extern void (*cl_inet_disconnect)(netstackid_t,
		    uint8_t protocol, sa_family_t addr_family,
		    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp,
		    in_port_t fport, void *);

		cl_inet_bind = cl_private_inet_bind_v1;
		cl_inet_unbind = cl_private_inet_unbind_v1;

		cl_inet_listen = cl_private_inet_listen_v1;
		cl_inet_unlisten = cl_private_inet_unlisten_v1;

		cl_inet_connect2 = cl_private_inet_connect_v1;
		cl_inet_disconnect = cl_private_inet_disconnect_v1;
		clnet_version = 1; // set version = 1

		cl_tcp_walk_list_v1 = (int (*)(netstackid_t,
		    int (*callback)(cl_tcp_info_t *, void *), void *))modlookup(
		    "ip", "cl_tcp_walk_list");
		CL_PANIC(cl_tcp_walk_list_v1 != NULL);
	} else {
		extern void (*cl_inet_bind)(uint8_t protocol, sa_family_t
		    addr_family, uint8_t *laddrp, in_port_t lport);
		extern void (*cl_inet_unbind)(uint8_t protocol, sa_family_t
		    addr_family, uint8_t *laddrp, in_port_t lport);
		extern void (*cl_inet_listen)(uint8_t protocol, sa_family_t
		    addr_family, uint8_t *laddrp, in_port_t lport);
		extern void (*cl_inet_unlisten)(uint8_t protocol, sa_family_t
		    addr_family, uint8_t *laddrp, in_port_t lport);
		extern void (*cl_inet_disconnect)(uint8_t protocol,
		    sa_family_t addr_family, uint8_t *laddrp, in_port_t lport,
		    uint8_t *faddrp, in_port_t fport);

		cl_inet_bind = cl_private_inet_bind_v0;
		cl_inet_unbind = cl_private_inet_unbind_v0;

		cl_inet_listen = cl_private_inet_listen_v0;
		cl_inet_unlisten = cl_private_inet_unlisten_v0;

		cl_inet_connect = cl_private_inet_connect_v0;
		cl_inet_disconnect = cl_private_inet_disconnect_v0;

		cl_tcp_walk_list_v0 = (int (*)(int (*callback)(cl_tcp_info_t *,
		    void *), void *))modlookup("ip", "cl_tcp_walk_list");
		CL_PANIC(cl_tcp_walk_list_v0 != NULL);
	}
#else // _KERNEL
	// Unode
	cl_inet_bind = cl_private_inet_bind_v1;
	cl_inet_unbind = cl_private_inet_unbind_v1;
	cl_inet_listen = cl_private_inet_listen_v1;
	cl_inet_unlisten = cl_private_inet_unlisten_v1;
	cl_inet_connect = cl_private_inet_connect_v1;
	cl_inet_disconnect = cl_private_inet_disconnect_v1;
#endif // _KERNEL
#else // SOL_VERSION >= __s10
	cl_inet_bind = cl_private_inet_bind_v0;
	cl_inet_unbind = cl_private_inet_unbind_v0;
	cl_inet_listen = cl_private_inet_listen_v0;
	cl_inet_unlisten = cl_private_inet_unlisten_v0;
	cl_inet_connect = cl_private_inet_connect_v0;
	cl_inet_disconnect = cl_private_inet_disconnect_v0;
#endif // SOL_VERSION >= __s10

#ifdef SCTP_SUPPORT
	// according to the C standard, a pointer is assumed to have
	// a non-zero value. So - it's ok for a 'correct' compiler
	// to optimize the check for (&cl_sctp_listen) to true
	// Hence moving it to a different variable to check the
	// value.
	hook_enabled = (unsigned long) &cl_sctp_listen;
	if (hook_enabled)
		cl_sctp_listen = cl_private_sctp_listen;
	hook_enabled = (unsigned long) &cl_sctp_unlisten;
	if (hook_enabled)
		cl_sctp_unlisten = cl_private_sctp_unlisten;
	hook_enabled = (unsigned long) &cl_sctp_connect;
	if (hook_enabled)
		cl_sctp_connect = cl_private_sctp_connect;
	hook_enabled = (unsigned long) &cl_sctp_disconnect;
	if (hook_enabled)
		cl_sctp_disconnect = cl_private_sctp_disconnect;
	hook_enabled = (unsigned long) &cl_sctp_assoc_change;
	if (hook_enabled)
		cl_sctp_assoc_change = cl_private_sctp_assoc_change;
	hook_enabled = (unsigned long) &cl_sctp_check_addrs;
	if (hook_enabled)
		cl_sctp_check_addrs = cl_private_sctp_check_addrs;
#endif  // SCTP_SUPPORT

	mcobjp->mc_register_all_services(e);
	if (e.exception()) {
		e.exception()->print_exception(
		    "Exception raised in mc_register_all_services()");
	}
	e.clear();

	return (0);
}

void
mc_inet_fini()
{
#if SOL_VERSION >= __s10
#ifdef _KERNEL
	extern void *cl_inet_bind;
	extern void *cl_inet_unbind;
	void *cl_inet_listen;
	void *cl_inet_unlisten;
	extern void *cl_inet_disconnect;

	cl_inet_bind = NULL;
	cl_inet_unbind = NULL;
	cl_inet_listen = NULL;
	cl_inet_unlisten = NULL;
	cl_inet_disconnect = NULL;

	if (clnet_version >= 1) {

		cl_inet_connect2 = NULL;
	} else {
		cl_inet_connect = NULL;
	}
#else // _KERNEL
	cl_inet_bind = NULL;
	cl_inet_unbind = NULL;
	cl_inet_listen = NULL;
	cl_inet_unlisten = NULL;
	cl_inet_connect = NULL;
	cl_inet_disconnect = NULL;
#endif // _KERNEL
#else	// SOL_VERSION >= __s10
	cl_inet_bind = NULL;
	cl_inet_unbind = NULL;
	cl_inet_listen = NULL;
	cl_inet_unlisten = NULL;
	cl_inet_connect = NULL;
	cl_inet_disconnect = NULL;
#endif // SOL_VERSION >= __s10

#ifdef SCTP_SUPPORT
	if (&cl_sctp_listen) {
		cl_sctp_listen = NULL;
	}
	if (&cl_sctp_unlisten) {
		cl_sctp_unlisten = NULL;
	}
	if (&cl_sctp_connect) {
		cl_sctp_connect = NULL;
	}
	if (&cl_sctp_disconnect) {
		cl_sctp_disconnect = NULL;
	}
	if (&cl_sctp_assoc_change) {
		cl_sctp_assoc_change = NULL;
	}
	if (&cl_sctp_check_addrs) {
		cl_sctp_check_addrs = NULL;
	}
#endif  // SCTP_SUPPORT
}
