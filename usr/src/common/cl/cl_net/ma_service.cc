/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1988 AT&T */
/*	  All Rights Reserved   */

/*
 * University Copyright- Copyright (c) 1982, 1986, 1988
 * The Regents of the University of California
 * All Rights Reserved
 *
 * University Acknowledgment- Portions of this document are derived from
 * software developed by the University of California, Berkeley, and its
 * contributors.
 */

#pragma ident	"@(#)ma_service.cc	1.176	08/05/20 SMI"

//
// Master service module for replicated services
//

#include <h/network.h>
#include <sys/os.h>

#include <cl_net/netlib.h>
#include <cl_net/ma_service.h>
#include <cl_net/ma_pdt.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/lbobj.h>
#include <cl_net/errprint.h>
#include <sys/mc_probe.h>

//
// We create a "permutation" table to map PDT buckets to so that we can
// populate the table it in a random order.
// From the PERM() users perspective the array is simply populated as
// a linear array.
//
static nodeid_t *permtab = NULL;

//
// If permtab has been created then use it, otherwise just
// default to a noop.
//
#define	PERM(bin) ((permtab) ? permtab[bin] : bin)

//
// Random function borrowed from libc/port/gen/rand.c
// (this probably belongs in os::). This doesn't pass FlexeLint,
// hence the directive (lint warns that right shifting a signed
// type is machine-dependent and can be zero-filled or sign-filled,
// but it doesn't matter here because of the "& 0x7fff").
//
static nodeid_t
rand(nodeid_t val)
{
	static int randx = 1;
	return ((((randx = randx * 1103515245 + 12345) >> 16) & //lint !e702
	    0x7fff) % val);
}

//
// Initialize the permutation table ... only done once, and until it's
// done the PERM() macro is a noop.
//
static void
perm_init() {
	if (permtab == NULL) {
		nodeid_t i, *p;
		static os::mutex_t perm_lock;

		// Atomically update permtab
		perm_lock.lock();

		// Check to see is permtab is NULL again, after acquiring
		// the mutex.
		if (permtab != NULL) {
			perm_lock.unlock();
			return;
		}

		//
		// We'll default to the old style mapping if there's no
		// memory for the mapping array.
		//
		// lint can't find a match for operator new with correct
		// argument count for some reason
		//
		//lint -save -e1703 -e1024 -e119
		if ((p = new (os::NO_SLEEP) nodeid_t[pdt_hash_size]) == NULL) {
			perm_lock.unlock();
			return;
		}
		//lint -restore

		//
		// Initialize the mapping to be the same.
		//
		for (i = 0; i < pdt_hash_size; i++) {
			p[i] = i;
		}

		//
		// Now swap buckets around in a random fashion.
		//
		for (i = 0; i < pdt_hash_size; i++) {
			//
			// j is a random number between i and
			// (pdt_hash_size - 1) inclusive.
			// Any easy way to get random numbers in the kernel???
			//
			nodeid_t j = rand(((nodeid_t)pdt_hash_size - i)) + i;

			//
			// t is the old value of the bucket, we swap it
			// with the value of the next bucket.
			//
			ASSERT(j < (nodeid_t)pdt_hash_size);
			nodeid_t t = p[j];
			p[j] = p[i];
			p[i] = t;
		}
		permtab = p;
		perm_lock.unlock();
	}
}

//
// Constructor for ma_servicegrp.
// On primary, the value nil is passed in for primary_lb;
// on secondary, a reference to the primary lb is passed in.
// If creation fails, okay is set to false.
//
ma_servicegrp::ma_servicegrp(const network::group_t &group_name,
    network::lb_specifier_t policy_type_in,
    network::lbobj_i_ptr primary_lb,
    bool &okay, unsigned int numhash = pdt_hash_size) :
    servicegrp(group_name, numhash, policy_type_in)
{
	uint32_t	i;

	NET_DBG(("ma_servicegrp::ma_servicegrp() constructor\n"));
	NET_DBG(("  group = %s\n", group_name.resource));

	// This is the pointer to the PDT table.
	pdts = NULL;

	//
	// Initialize an lbobj class and save pointer to it
	//
	lb = lbobj::lb_factory(this, policy_type_in, primary_lb);

	if (lb == NULL) {
		okay = false;
	} else {
		okay = true;
		//
		// Keep a reference to the load balancer,
		// so it will get unref when we're done.
		//
		if (CORBA::is_nil(primary_lb)) {
			// On primary, get the reference locally.
			lb_ref = get_lbobj();
		} else {
			// On secondary, use the reference passed in.
			lb_ref = network::lbobj_i::_duplicate(primary_lb);
		}
	}

	ninst = 0;
	nconfig_inst = 0;

	//
	// Create the load_dist arrays for load-balancing.
	// "nodes" contains the status of the instance on a node (where
	// node corresopnds to the index into the array).
	//
	load_dist.nodes.length(NODEID_MAX + 1);
	//
	// "weights" contains the weight for a node (where
	// node corresopnds to the index into the array).
	//
	load_dist.weights.length(NODEID_MAX + 1);

	//
	// Note that the arrays are of length NODEID_MAX + 1 because index
	// 0 is unused.  The indices of interest range from 1 to NODEID_MAX
	// inclusive: each index corresponds to a node in the cluster.  Using
	// the node id as an index makes it very easy to access a node's
	// information in both arrays.  Just for kicks, we'll make sure
	// index 0 is initialized just like all the others are.
	//
	for (i = 0; i < NODEID_MAX + 1; i++) {
		load_dist.nodes[i] = network::no_instance;
		load_dist.weights[i] = default_lb_weight;
		load_dist.initialized	= false;
	}

	//
	// The default load-balancing weight is 1.  This means that when
	// an instance starts up on a node that's in the config list it
	// has a weight of 1.  We do this for one reason.  It's
	// backwards compatible with the original implementation so
	// QA tests don't need to change.
	//
	// The load_dist array is of a fixed size. This is convenient
	// because we don't have to worry about growing and shrinking
	// the array as nodes enter and leave the config list.  But we
	// do have to worry about which nodes to set in the load_dist:
	// we only set those nodes that are currently in the config list.
	//

	// Initialize sticky config values
	INIT_STICKY_CONFIG(sticky_config);
	// Initialize  rr config values
	INIT_RR_CONFIG(rr_config);
}

ma_servicegrp::~ma_servicegrp()
{
	NET_DBG(("ma_servicegrp::~ma_servicegrp() destructor\n"));

	// Should have emptied the serviceobj list before
	// destroying the group.
	ASSERT(ma_serviceobj_list.empty());

	// Delete the instance lists.
	config_inst_list.dispose();
	instancelist.dispose();

	//
	// Tell the lbobj that this service is not using it.
	// This is necessary because multiple ma_servicegrp's may share a lbobj.
	// Note: until released, the load balancer may call set_distribution,
	// etc. on the ma_servicegrp.
	//
	if (lb) {
		lb->release_lbobj();
	}
	lb = NULL;		// Make lint happy

	delete pdts;		// delete the PDT table
	pdts = NULL;
}


ma_serviceobj::ma_serviceobj(ma_servicegrp &group,
    const network::service_sap_t &ssap) :
    serviceobj(ssap), servicegrp(&group)
{
}

ma_serviceobj::~ma_serviceobj()
{
	instancelist.dispose();
	servicegrp = NULL; 	// Make lint happy
}

void
ma_servicegrp::ma_dumpservice()
{
	for (nodeid_t node = 1; node <= NODEID_MAX; node++)
		(void) ma_dump_pdtinfo(node);
}


unsigned long
ma_servicegrp::ma_dump_pdtinfo(uint_t)
{
	ma_pdt *pdtp = pdts;

	if (pdtp == NULL) {
		return (0);
	}
	return (pdtp->dump_mapdt());
}

//
// Effects: Register a new instance.
//
void
ma_serviceobj::ma_register_instance(nodeid_t node)
{
	instanceobj	*instancep;

	NET_DBG(("  ma_serviceobj::ma_register_instance(%d)\n",
	    node));

	// Get a write lock on the instance list.
	instancelist_lock.wrlock();

	for (instancelist.atfirst();
		(instancep = instancelist.get_current()) != NULL;
		instancelist.advance()) {
		if (instancep->equal(node)) {
			//
			// Instance from the node is already active.
			// Do nothing.
			//
			instancelist_lock.unlock();
			return;
		}
	}

	// Create new instanceobj for "node" and append to list.
	instancep = new instanceobj(node);
	instancelist.append(instancep);

	// Release the write lock.
	instancelist_lock.unlock();
}

//
// Effects: Register a new instance.  Returns whether a new instance was
//   created for the group.
//
bool
ma_servicegrp::ma_register_instance(nodeid_t node,
    const network::service_sap_t& ssap)
{
	ma_serviceobj	*sobj;
	instanceobj	*instancep;

	instancelist_lock.rdlock();
	if (!ma_is_node_in_configlist(node)) {
		instancelist_lock.unlock();
		return (false);
	}
	instancelist_lock.unlock();

	NET_DBG(("  ma_servicegrp::ma_register_instance(%d)\n",
	    node));

	MC_PROBE_0(ma_register_instance, "mc net mainstance", "");

	//
	// Add the serviceobj instance to the serviceobj-specific
	// instance lists.
	//
	ma_serviceobj_list_lock.wrlock();
	for (ma_serviceobj_list.atfirst();
	    (sobj = ma_serviceobj_list.get_current()) != NULL;
	    ma_serviceobj_list.advance()) {
		if (sobj->matches(ssap)) {
			MC_PROBE_0(ma_register_instance_matched,
			    "mc net mainstance", "");
			sobj->ma_register_instance(node);
		}
	}
	MC_PROBE_0(ma_register_instance_adding_inst,
	    "mc net mainstance", "");

	//
	// We now have at least one service object instance active on the
	// specified node. Mark it in the service group list.
	//
	instancelist_lock.wrlock();
	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
	    instancelist.advance()) {
		if (instancep->equal(node)) {
			//
			// Instance from the node is already active.
			// Do nothing.
			//
			instancelist_lock.unlock();
			ma_serviceobj_list_lock.unlock();
			return (false);
		}
	}

	// Create new instanceobj for "node"
	instancep = new instanceobj(node);
	// Append to the GROUP instance list.
	instancelist.append(instancep);
	ninst++;

	// Mark the instance as up.
	load_dist.nodes[node] = network::instance_up;

	//
	// Note that we need not set the load-balancing weight for
	// this node.  Whatever value is in load_dist.weights[node]
	// is in force when this instance starts up.  If the instance
	// is starting up anew after registration, then the default
	// weight is 1 (default_lb_weight); please see the comments
	// in the constructor for ma_servicegrp.
	//

	//
	// Release the locks.
	//
	instancelist_lock.unlock();
	ma_serviceobj_list_lock.unlock();

	return (true);
}

bool
ma_servicegrp::is_strong_sticky()
{
	return (is_sticky(getpolicy()) &&
	    !(sticky_config.st_flags & network::ST_WEAK)?
	    true: false);
}

bool
ma_servicegrp::is_rr_enabled()
{
	return ((rr_config.rr_flags & network::LB_RR)? true:false);
}

bool
ma_servicegrp::is_generic_affinity()
{
	return (is_sticky(getpolicy()) &&
	    (sticky_config.st_flags & network::ST_GENERIC) ? true :false);
}

//
// Locking: ma_servicegrplist_lock.rdlock() protected
//
void
ma_servicegrp::ma_get_sticky_config(network::sticky_config_t &st)
{
	// Default bit-wise copy assignment operator is just fine
	st = sticky_config;
}

//
// Locking: ma_servicegrplist_lock.wrlock() protected
//
network::ret_value_t
ma_servicegrp::ma_config_sticky(const network::sticky_config_t& st)
{
	NET_DBG(("ma_servicegrp::ma_config_sticky()\n"));
	NET_DBG(("  gname = %s\n", get_group_name()));
	NET_DBG(("  timeout = %d, flags = %x\n",
	    st.st_timeout, st.st_flags));

	// Default bit-wise copy assignment operator is just fine
	sticky_config = st;

	return (network::SUCCESS);
}


//
// Effects: This routine adds the specified node to the configlist
//   for the specified service
//
network::ret_value_t
ma_servicegrp::ma_add_configlist_entry(nodeid_t node)
{
	instanceobj *instancep;

	NET_DBG(("ma_servicegrp::ma_add_configlist_entry(%d)\n", node));
	NET_DBG(("  gname = %s\n", get_group_name()));

	// Acquire write lock on instance list.
	instancelist_lock.wrlock();

	// Search for "node" in the instance list.
	for (config_inst_list.atfirst();
	    (instancep = config_inst_list.get_current()) != NULL;
	    config_inst_list.advance()) {
		if (instancep->equal(node)) {
			// node is already present in list.
			// Do nothing.
			instancelist_lock.unlock();
			return (network::NODE_EXISTS);
		}
	}
	instancep = new instanceobj(node);
	config_inst_list.append(instancep);
	nconfig_inst++;

	// Release lock on config instance list.
	instancelist_lock.unlock();

	return (network::SUCCESS);
}


//
// Requires: instancelist_lock MUST be held.
// Effects: Returns true if "node" is in the config list, false
//   otherwise.
//
bool
ma_servicegrp::ma_is_node_in_configlist(nodeid_t node)
{
	instanceobj *instancep;

	instancelist_t::ListIterator iter(config_inst_list);
	for (; (instancep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (instancep->equal(node)) {
			// Node is present. return true.
			return (true);
		}
	}
	// Node not present, return false
	return (false);
}

void
ma_serviceobj::ma_deregister_instance(nodeid_t node)
{
	instanceobj *instancep;

	NET_DBG(("  ma_serviceobj::ma_deregister_instance(%d)\n", node));

	//
	// In the case of deleting an instance from the list, note that
	// if we succeeded then upon a retry the instance will have already
	// been removed, and this loop has no effect.
	// We acquire the write lock on the service object
	//
	// Acquire write lock on object instance list.
	instancelist_lock.wrlock();

	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
	    instancelist.advance()) {
		if (instancep->equal(node)) {
			// Erase object from list.
			(void) instancelist.erase(instancep);
			// Delete the storage.
			delete instancep;
			break;
		}
	} // end for

	// Release lock.
	instancelist_lock.unlock();
}

bool
ma_serviceobj::ma_deregister_node(nodeid_t node)
{
	//
	// Part of 4230718: WARNING: mc_init_pdt Failed to find service
	//
	// If this is our gifnode then remove us it from our serviceobj
	// so it must be reattached (otherwise we may try to download the
	// PDT to the node when it has not been attached).
	//
	if (get_gifnode() == node)
		set_gifnode(NULLNODE);

	return (true);
}

bool
ma_servicegrp::ma_deregister_node(nodeid_t node)
{
	ma_serviceobj	*sobj;
	instanceobj	*instancep;

	NET_DBG(("  ma_servicegrp::ma_deregister_node(%d)\n", node));

	//
	// Remove the the node from all serviceobj instances.
	//
	// Acquire write lock on master service object list.
	ma_serviceobj_list_lock.wrlock();

	for (ma_serviceobj_list.atfirst();
	    (sobj = ma_serviceobj_list.get_current()) != NULL;
	    ma_serviceobj_list.advance()) {
		sobj->ma_deregister_instance(node);
		(void) sobj->ma_deregister_node(node);
	}
	// Release lock.
	ma_serviceobj_list_lock.unlock();

	//
	// In the case of deleting an instance from the list, note that
	// if we succeeded then upon a retry the instance will have already
	// been removed, and this loop has no effect.
	//

	// Acquire write lock on the group instance list.
	instancelist_lock.wrlock();
	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
	    instancelist.advance()) {
		if (instancep->equal(node)) {

			// Erase object from list.
			(void) instancelist.erase(instancep);
			// Destroy storage.
			delete instancep;
			--ninst;	// Decrement counter

			// Mark instance as down on "node."
			ASSERT(load_dist.nodes.length() > node);
			load_dist.nodes[node] = network::instance_down;
			ASSERT(lb);

			// Release lock.
			instancelist_lock.unlock();
			return (true);
		}
	}
	// Release lock.
	instancelist_lock.unlock();

	return (false);
}

bool
ma_servicegrp::ma_deregister_instance(nodeid_t node,
    const network::service_sap_t& ssap)
{
	ma_serviceobj	*sobj;
	instanceobj *instancep;
	bool	sobj_found = false;

	NET_DBG(("  ma_servicegrp::ma_deregister_instance(%d)\n", node));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Check if "node" is in the config list.  You can't deregister
	// an instance on a node that's unknown.
	//
	instancelist_lock.rdlock();
	if (!ma_is_node_in_configlist(node)) {
		instancelist_lock.unlock();
		return (false);
	}
	instancelist_lock.unlock();

	//
	// Remove the serviceobj instance from the serviceobj-specific
	// instance lists.
	//
	// Acquire write lock on master service object list.
	ma_serviceobj_list_lock.wrlock();

	for (ma_serviceobj_list.atfirst();
	    (sobj = ma_serviceobj_list.get_current()) != NULL;
	    ma_serviceobj_list.advance()) {
		//
		// Note that there may be more than one "matching" instance and
		// we must mark each down. If there are other "non-matching"
		// service objects we only need to note if one of them is up,
		// which is sufficient for us to know not to bring the service
		// group down on the node (see below).
		//
		if (sobj->matches(ssap)) {
			sobj->ma_deregister_instance(node);
		} else if (!sobj_found) {
			// Check if sobj has an instance running on the node.
			sobj->instancelist_lock.rdlock();
			instancelist_t::ListIterator iter(sobj->instancelist);
			for (; (instancep = iter.get_current()) != NULL;
			    iter.advance()) {
				if (instancep->equal(node)) {
					sobj_found = true;
					break;
				}
			}
			sobj->instancelist_lock.unlock();
		} // end if-then
	} // end for

	// Release lock.
	ma_serviceobj_list_lock.unlock();


	if (sobj_found) {
		// Don't bring down the node as long as some sobj instance
		// still running.
		return (false);
	}

	//
	// In the case of deleting an instance from the list, note that
	// if we succeeded then upon a retry the instance will have already
	// been removed, and this loop has no effect.
	//

	// Get a write lock on the group instance list.
	instancelist_lock.wrlock();
	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
	    instancelist.advance()) {
		if (instancep->equal(node)) {

			// Erase from list.
			(void) instancelist.erase(instancep);
			delete instancep;
			--ninst;

			// Mark instance as down on node "node."
			ASSERT(load_dist.nodes.length() > node);
			load_dist.nodes[node] = network::instance_down;
			ASSERT(lb);

			// Release lock on group instance list.
			instancelist_lock.unlock();
			return (true);
		}
	}
	// Release lock on group instance list.
	instancelist_lock.unlock();

	return (false);
}


//
// Effects: Return a copy of the group instance list.
//
instancelist_t *
ma_servicegrp::get_clist()
{
	instancelist_t	*newlist;
	instanceobj	*newinstance, *instancep;

	// Create new instance list.
	newlist = new instancelist_t();

	// Get read lock on instance list.
	instancelist_lock.rdlock();

	//
	// Iterate over config instance list and append a new
	// copy of the instanceobj to the new "newlist."
	//
	instancelist_t::ListIterator iter(config_inst_list);
	for (; (instancep = iter.get_current()) != NULL; iter.advance()) {
		newinstance = new instanceobj(instancep->get_instanceid());
		newlist->append(newinstance);
	} // end for

	// Release lock on instance list.
	instancelist_lock.unlock();

	return (newlist);
}

//
// Effects: This routine removes the node specified by the argument "node"
//   from the config_inst_list.
//
bool
ma_servicegrp::ma_remove_node_from_clist(nodeid_t node)
{
	instanceobj	*instancep;
	bool		found = false;

	NET_DBG(("  ma_servicegrp::ma_remove_node_from_clist"
		"(%d)\n", node));

	// Acquire write lock lock on the instance list.
	instancelist_lock.wrlock();

	//
	// Iterate over the config list, looking for the "node" in the
	// instance list.  If found, then erase from list and destroy
	// object and decrement counter.
	//
	for (config_inst_list.atfirst();
	    (instancep = config_inst_list.get_current()) != NULL;
	    config_inst_list.advance()) {
		if (instancep->equal(node)) {
			// Found the instance. Remove it.
			(void) config_inst_list.erase(instancep);
			nconfig_inst--;
			// Destroy the object.
			delete instancep;

			found = true;
			break;
		} // end if
	} // end for

	//
	// The reason we reset the weight to the default is as follows.
	// Suppose we didn't change the weight, which is set to 10.
	// This node won't participate in the PDT calculations because
	// the instance is down.  Suppose the node is added again at
	// a later date, and suppose that the weight remains unchanged.
	// Then when an instance starts, it will have whatever the last
	// weight was, which a user could find surprising.  Therefore,
	// we reset the weight to the default--the default it started
	// out with at the time of creation.
	//
	load_dist.weights[node] = default_lb_weight;

	// Release lock.
	instancelist_lock.unlock();

	return (found);
}

//
// Effects: Returns true if the node id "node" was removed from the
//   group instance list AND the instance was deregistered from each
//   instance list where an instance was running on that node.
//   Returns false if the node wasn't in the group's instance list.
//
bool
ma_servicegrp::ma_remove_node_from_ilist(nodeid_t node)
{
	instanceobj *instancep = NULL;
	ma_serviceobj *sobj = NULL;
	bool removed = false;

	NET_DBG(("  ma_servicegrp::ma_remove_node_from_ilist"
		"(%d)\n", node));

	// 1. Remove node id "node" from group instance list.
	// Acquire write lock on group instance list.
	instancelist_lock.wrlock();

	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
	    instancelist.advance()) {
		if (instancep->equal(node)) {
			// Remove from list.
			(void) instancelist.erase(instancep);
			// Destroy storage.
			delete instancep;
			// Decrement counter.
			ninst--;
			removed = true;

			//
			// For load-balancing, we mark the node as
			// instance_down on node "node."
			//
			ASSERT(load_dist.nodes.length() > node);
			load_dist.nodes[node] = network::instance_down;
			ASSERT(lb);

			break;
		} // end if
	} // end for

	// Release lock on group instance list.
	instancelist_lock.unlock();

	// 2. Remove node from instance list of each service object.

	// Acquire write lock on the master service object list.
	ma_serviceobj_list_lock.wrlock();

	//
	// Iterate over service object list, deregistering the instance
	// for each service object for "node."
	//
	for (ma_serviceobj_list.atfirst();
	    (sobj = ma_serviceobj_list.get_current()) != NULL;
	    ma_serviceobj_list.advance()) {
		// Remove instance from service object's instance list.
		sobj->ma_deregister_instance(node);
	} // end for

	// Release lock on master service object list.
	ma_serviceobj_list_lock.unlock();

	return (removed);
}

//
// Effects:  Dump the state of this object to the secondaries.
//
void
ma_serviceobj::dump_ma_serviceobj(network::group_t 	&group,
				replica::checkpoint_ptr sec_chkpt)
{
	network::seqlong_t	*inslist;
	uint_t			len;
	unsigned int		i = 0;
	instanceobj		*instancep;
	Environment		e;

	NET_DBG(("  ma_serviceobj::dump_ma_serviceobj()\n"));
	NET_DBG(("    group = %s\n", group.resource));

	// This checkpoint object sends checkpoint messages to a SPECIFIC
	// secondary, not all of them.
	network::ckpt_var ckpt = network::ckpt::_narrow(sec_chkpt);

	// Get the length of the instance list
	len = instancelist.count();

	// Create a new instance list of the IDL variety.
	inslist = new network::seqlong_t(len);
	network::seqlong_t &newinslist = *inslist;

	// convert instance list to IDL int sequence
	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
		instancelist.advance()) {
		uint_t id = instancep->get_instanceid();
		newinslist[i] = id;
		NET_DBG(("%d ", id));
		i = i + 1;
	} // end for
	NET_DBG(("\n"));

	// Checkpoint the master service object state to the secondaries.
	ckpt->ckpt_dump_ma_serviceobj_state(group, sr_sap, sr_gifnode,
		*inslist, e);

	delete inslist;

	//
	// Secondary failed.  Just log a warning and bail out.
	// Note that we check exceptions explicitly only for checkpoints
	// that are sent to a SINGLE secondary, as we do here.
	//
	if (e.exception() != NULL) {
		cmn_err(CE_WARN, "ckpt_dump_ma_serviceobj_state() "
		    "checkpoint failed\n");
		e.clear();
		return;
	}
}

//
// Effects: Divide the instances among the buckets according to
//   the input set of weights.  End up with num_nodes total instances
//   divided among the nhash buckets.
//   Return NULL if buckets are all 0, otherwise return int[NODEID_MAX].
//   result[nodeid] = # of hash buckets to assign to that instance.
//   sum of result[i] = nhash.
//
uint32_t *
ma_servicegrp::scale_buckets()
{
	uint32_t num_nodes = load_dist.nodes.length();
	uint32_t nhash = (uint32_t)getnhash();
	uint32_t *want_hash;
	uint32_t tot = 0;
	uint32_t excess = 0;
	uint32_t i;

	//
	// Figure out how many buckets should have the entry.
	//

	NET_DBG(("  ma_servicegrp::scale_buckets()\n"));

	ASSERT(nhash != 0);
	ASSERT(NODEID_MAX+1 >= num_nodes);

	// only kernel rwlock_t class has lock_held() method
#ifdef _KERNEL
	ASSERT(instancelist_lock.lock_held());
#endif

	//
	// Note that node 0 is not used.
	//
	for (i = 1; i < num_nodes; i++) {
		if (load_dist.nodes[i] == network::instance_up) {
			ASSERT(load_dist.weights[i] < 1000000);
			tot += load_dist.weights[i];
		}
	}
	if (tot == 0) {
		return (NULL);
	}

	//
	// Goal: divide up nhash bins among the instances, weighted
	// according to the weights.  Make sure that the sum of
	// all the want_hash == nhash, even if the weights don't
	// divide evenly.
	//
	want_hash = new uint32_t[NODEID_MAX+1];
	bzero(want_hash, sizeof (uint32_t) * (NODEID_MAX+1));
	for (i = 1; i < num_nodes; i++) {
		if (load_dist.nodes[i] == network::instance_up) {
			uint32_t part = (nhash * load_dist.weights[i]) + excess;
			uint32_t frac = part / tot;
			excess = part % tot;

			want_hash[i] = frac;
			//
			// Invariant: sum of want_hash up to i * tot + excess
			// == sum of weights * nhash
			// Invariant: excess < tot
			//
		}
	}
	ASSERT(excess == 0);
	//
	// At this point sum of weights == tot
	// So sum of wants == nhash, as desired
	//
#ifdef DEBUG
	// Verify the distribution
	tot = 0;
	for (i = 0; i <= NODEID_MAX; i++) {
		tot += want_hash[i];
	}
	ASSERT(tot == nhash);
#endif
	return (want_hash);
}

//
// Effects:  Update the hash table to correspond to the new buckets.
//   This is the main routine called after the load balancer returns
//   the weights, whenever the system configuration changes.
//   This routine does the updating of the PDT.
//
void
ma_servicegrp::update_buckets()
{
	NET_DBG(("  ma_servicegrp::update_buckets()\n"));

	if (pdts == NULL) {
		return;
	}

	perm_init();

	// Number of hash buckets.
	uint32_t nhash = (uint32_t)getnhash();

	if (pdts == NULL) {
		// pdt for this gif isn't initialized.
		// This could happen if mcnet doesn't get pushed, for instance.
		// We can't do anything, so just return.
		return;
	}

	if (!(pdts->pdt_getflag() & PDT_INITIALIZED)) {
		// Can't make new pdt right now
		return;
	}

	// Array to store how many hash buckets each node has assigned.
	uint32_t *have_hash = new uint32_t[NODEID_MAX+1];
	bzero(have_hash, sizeof (uint32_t) * (NODEID_MAX+1));

	// Convert the weights to bucket counts
	uint32_t *want_hash = scale_buckets();
	if (want_hash != NULL) {
		// Count the buckets.  This loop will put into
		// have_hash[node] the current number of hash buckets pointing
		// to that node.
		uint32_t bin;
		for (bin = 0; bin < nhash; bin++) {
			unsigned long node = pdts->pdt_gethashval(bin);
			have_hash[node]++;
		}

		// At this point, want_hash[node] indicates how many
		// entries we want to have for that node.
		// have_hash[node] indicates how many entries we currently
		// have for that node.
		// The following code steals buckets from nodes that have
		// too many entries (have_hash > want_hash), and gives the
		// buckets to nodes that don't have enough (have_hash <
		// want_hash).

		// Need_more_node keeps track of the first node that needs
		// more hash table entries.
		uint32_t need_more_node = 1;
		for (bin = 0; bin < nhash; bin++) {
			// Loop through each hash table entry, and see what
			// node it points to.  If that node has too many
			// buckets, re-assign that bucket to a node that
			// needs more buckets.  By the time we get to the
			// end of the table, buckets will be assigned to
			// each node according to its need.
			nodeid_t node = pdts->pdt_gethashval(PERM(bin));
			if (have_hash[node] > want_hash[node]) {
				// The invariant at this point is that
				// every node < need_more node has at least
				// enough buckets.
				while (have_hash[need_more_node] >=
						want_hash[need_more_node]) {
					need_more_node++;
					ASSERT(need_more_node <= NODEID_MAX);
				}
				// The invariant at this point is that
				// every node < need_more node has at least
				// enough buckets, and need_more_node is
				// the first node that needs more buckets.
				// So we assign the current bucket to that node.
				pdts->pdt_sethashval(PERM(bin), need_more_node);

				// Update have_hash to indicate that the
				// bucket has been taken away from node and
				// give to need_more_node
				have_hash[node]--;
				have_hash[need_more_node]++;
			}
		}
	} else {
		//
		// This is a special case to handle the situation when
		// all the load-balancing weights are set to 0.
		// want_hash is NULL,  So we iterate over the PDT itself
		// and change each bucket to 0.  The meaning of 0 is that
		// packets go nowhere.  Note that this is the state of
		// the PDT at creation time before any instances have
		// started.
		//
		for (uint32_t bucket = 0; bucket < nhash; bucket++) {
			pdts->pdt_sethashval(bucket, 0);
		} // end for
	}

	delete [] want_hash;
	delete [] have_hash;
}

//
// Effects: Return the current state of the gif/instance/weights.
//
network::dist_info_t *
ma_servicegrp::get_distribution(Environment &)
{
	return (&load_dist);
}

//
// Effects: Modify the current state of load-balancing weights (part 1).
// Exceptions:
//   - EINVAL:   If the lengths of argument "dist" and "load_dist" are
//		different, or any weight value of "dist" is way
//		too big.
//
void
ma_servicegrp::set_distribution1(const network::dist_info_t &dist,
	Environment &e)
{
	instanceobj	*instancep = NULL;
	nodeid_t	nid;

	NET_DBG(("  ma_servicegrp::set_distribution1()\n"));

	// Acquire read lock on group instance list.
	instancelist_lock.rdlock();

	// Sanity checks.
	//
	// Check the array lengths.  The invariant is that argument
	// "dist" and the load_dist arrays must have equal lengths.
	//
	if (load_dist.nodes.length() != dist.nodes.length() ||
	    load_dist.weights.length() != dist.weights.length()) {
		e.exception(new sol::op_e(EINVAL));
		// Release lock.
		instancelist_lock.unlock();
		return;
	}

	//
	// Iterate over the config list, checking the incoming weights
	// in "dist" for anything fishy, like a weight that's too big.
	//
	instancelist_t::ListIterator iter1(config_inst_list);
	for (; (instancep = iter1.get_current()) != NULL;
	    iter1.advance()) {
		nid = instancep->get_instanceid();

		// Mainly a debugging check to catch garbage.
		if (dist.weights[nid] >= TOOBIG) {
			e.exception(new sol::op_e(EINVAL));
			// Release lock.
			instancelist_lock.unlock();
			return;
		} // end if
	} // end for

	//
	// Set the weights in load_dist only for those nodes that are
	// in the config list.
	//
	instancelist_t::ListIterator iter2(config_inst_list);
	for (; (instancep = iter2.get_current()) != NULL;
	    iter2.advance()) {
		nid = instancep->get_instanceid();
		load_dist.weights[nid] = dist.weights[nid];
		load_dist.initialized = true;
	} // end for

	// Update the PDT buckets.
	update_buckets();

	// Release lock on group instance list.
	instancelist_lock.unlock();
}

//
// Effects: Modify the current state of load-balancing weights (part 2).
//   This part sends the new distribution to the slaves.
//
void
ma_servicegrp::set_distribution2(Environment &e)
{
	NET_DBG(("  ma_servicegrp::set_distribution2()\n"));

	// Send the changed table to the slave.
	ma_distribute_pdt(0, COLLECT_ALL, e);
}

//
// Effects: Set the weight "weight" for node "node."  Returns nothing.
// Parameters:
//  - node (IN):	node to set weight for. Legal values >=1 and <= 64.
//  - weight (IN):	load-balancing weight.  Legal values range from 0
//			to TOO_BIG.
//  - e (IN):		Environment variable for passing around exception.s
// Exceptions:
//  - EINVAL:		If "node" is not in config list.
//
void
ma_servicegrp::set_weight(nodeid_t node, uint32_t weight,
	Environment &e)
{
	char nodename[NODENAME_MAX];

	NET_DBG(("  ma_servicegrp::set_weight(node=%d,weight=%d)\n",
		node, weight));

	// Get the write lock in the group instance list.
	instancelist_lock.rdlock();

	//
	// You cannot set the weight of a node that's not in the
	// approved config list.
	//
	if (!ma_is_node_in_configlist(node)) {
		e.exception(new sol::op_e(EINVAL));
		instancelist_lock.unlock();
		return;
	}

	// Now really set the weight for node "node."
	load_dist.weights[node] = weight;
	load_dist.initialized	= true;

	// Now update the PDT table.
	update_buckets();

	// Release lock on group instance list.
	instancelist_lock.unlock();

	network_lib.get_nodename_from_nodeid((nodeid_t)node, nodename);

	//
	// SCMSGS
	// @explanation
	// This message indicates that the user has set a new weight for a
	// particular node from an old value.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, STATE_CHANGED,
	    "Load balancer for group '%s' setting weight for node %s to %d",
	    get_group_name(),
	    nodename,
	    weight);
}

//
// Effects: Dump state to the secondary identified by "sec_chkpt."  Returns
//    true if dump succeeds, else returns false if the secondary
//    fails before the dumping can complete.
//
// Note: Called only by the PRIMARY replica provider:
//  primary pdts_prov_impl_t::add_secondary() calls
//    primary pdts_impl_t::dump_maservicegrplist() calls
//	this routine
//
bool
ma_servicegrp::ma_dump_state(replica::checkpoint_ptr sec_chkpt)
{
	network::ckpt_var	ckpt;
	Environment		e;
	unsigned int		i;
	uint_t			len;
	network::seqlong_t	inslist;
	network::seqlong_t	configlist;
	instanceobj		*instancep;
	network::pdtinfo_t	pdtinfo;

	// Get the real checkpoint object to the right secondary.
	ckpt = network::ckpt::_narrow(sec_chkpt);

	NET_DBG(("           ma_servicegrp::ma_dump_state\n"));

	// Obtain write lock on the instancelist
	instancelist_lock.wrlock();

	// Get the length of the instance list.
	len = instancelist.count();
	i = 0;

	//
	// Instantiate IDL sequence of length "len."  "true" means that
	// the buffer will be released when the sequence goes away.
	//
	inslist.load(len, len, network::seqlong_t::allocbuf(len), true);

	NET_DBG(("                group instance list:"));

	// convert instance list to IDL int sequence
	for (instancelist.atfirst();
	    (instancep = instancelist.get_current()) != NULL;
		instancelist.advance()) {
		uint_t id = instancep->get_instanceid();
		inslist[i] = id;
		NET_DBG(("%d ", inslist[i]));
		i = i + 1;
	} // end for
	NET_DBG(("\n"));

	// Get length of the config instance list.
	len = config_inst_list.count();
	i = 0;

	//
	// Instantiate IDL sequence of length "len."  "true" means that
	// the buffer will be released when the sequence goes away.
	//
	configlist.load(len, len, network::seqlong_t::allocbuf(len), true);

	NET_DBG(("                group config list:"));

	// convert instance list to IDL int sequence
	for (config_inst_list.atfirst();
	    (instancep = config_inst_list.get_current()) != NULL;
		config_inst_list.advance()) {
		uint_t id = instancep->get_instanceid();
		configlist[i] = id;
		NET_DBG(("%d ", configlist[i]));
		i = i + 1;
	} // end for
	NET_DBG(("\n"));

	NET_DBG(("              call ckpt_dump_ma_service_state()\n"));
	NET_DBG(("                  nconfig_inst = %d\n", nconfig_inst));

	// Checkpoint the master service state to the secondaries.
	ckpt->ckpt_dump_ma_service_state(this->getgroup(), inslist, ninst, 0,
		load_dist, configlist, nconfig_inst, e);

	// Secondary failed.  Just log a warning and bail out.
	if (e.exception() != NULL) {
		cmn_err(CE_WARN, "ckpt_dump_ma_service_state() "
		    "checkpoint to specific secondary failed\n");
		e.clear();
		return (false);
	}

	ma_serviceobj *sobj;
	//
	// Dump the list of master service objects, each of which has
	// sap, gifnode, instancelist.
	//
	NET_DBG(("              Dump service object list\n"));
	//
	// Algorithm: walk the list, and for each service object,
	// dump the state of the object (dump_serviceobj_state()).
	//
	ma_serviceobj_list_lock.rdlock();
	ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		sobj->dump_ma_serviceobj(this->getgroup(), ckpt);
	}
	ma_serviceobj_list_lock.unlock();

	// Dump the master PDT state.
	NET_DBG(("              Dump master PDT\n"));

	if (pdts != NULL) {
		// If the table has been created but has not yet been
		// initialized we need to checkpoint creation of the
		// table to the new secondary.
		if (!(pdts->pdt_getflag() & PDT_INITIALIZED)) {
			// checkpoint creation of the empty table
			NET_DBG(("       ~PDT_INITIALIZED\n"));
			ckpt->ckpt_create_ma_pdt(this->getgroup(), e);
			// Secondary failed.  Just log a warning and bail out.
			if (e.exception() != NULL) {
			    cmn_err(CE_WARN, "ckpt_dump_create_ma_pdt() "
			    "checkpoint to specific secondary failed\n");
			    e.clear();
			    return (false);
			}
		} else {
			// Convert to PDT to canonical form only if PDT has
			// already been initialized.
			NET_DBG(("       PDT_INITIALIZED\n"));

			pdtinfo.group = sr_group;
			pdtinfo.policy = sr_policy;

			// Convert to canonical form.
			(void) pdts->marshal_pdtinfo(pdtinfo);

			// Checkpoint this pdtinfo to secondaries. Then
			// decanonicalize at the secondaries.
			NET_DBG(("        call ckpt_ma_pdtinfo()\n"));
			ckpt->ckpt_dump_ma_pdtinfo(this->getgroup(),
				pdtinfo, e);
			// Secondary failed.  Just log a warning and bail out.
			if (e.exception() != NULL) {
			    cmn_err(CE_WARN, "ckpt_dump_ma_pdtinfo() "
			    "checkpoint to specific secondary failed\n");
			    e.clear();
			    return (false);
			}
		} // end if
	} // end  if (pdts != NULL)

	ckpt->ckpt_dump_ma_sticky_config(getgroup(), sticky_config, e);

	ASSERT(!e.exception());
	e.clear();

	// Release the write lock on instancelist
	instancelist_lock.unlock();

	return (true);
}

//
// Effects: Tell this service about the number of active instances
//    "num_of_instances"
//
// Parameters:
//   - num_of_instances (IN): number of active instances
//
// Note: Called only by the SECONDARY replica provider.
//  primary ma_servicegrp::ma_dump_state calls
//    secondary pdts_prov_impl_t::ckpt_dump_ma_service_state calls
//	secondary pdts_impl_t::dump_ma_service_state() calls
//		this routine
//
void
ma_servicegrp::ma_set_ninst(int num_of_instances)		// IN
{
	NET_DBG(("  ma_servicegrp::ma_set_ninst(%d)\n", num_of_instances));

	ninst = num_of_instances;
}

//
// Effects:  Set the instance list in the master service object given
//	"inslist."
// Parameters:
//   - inslist (IN): sequence of instance node ids.
//
// Note: Called only by the SECONDARY replica provider.
//    primary ma_servicegrp::ma_dump_state calls
//	secondary pdts_prov_impl_t::ckpt_dump_ma_service_state calls
//	   secondary pdts_impl_t::dump_ma_service_state()
//
void
ma_servicegrp::ma_set_instancelist(const network::seqlong_t &inslist)	// IN
{
	unsigned int len, i;
	instanceobj *ins;

	NET_DBG(("  ma_servicegrp::ma_set_instancelist()\n"));

	len = inslist->length();

	for (i = 0; i < len; i++) {
		ins = new instanceobj(inslist[i]);
		instancelist.append(ins);
	}
}

//
// Effects:  Set the instance list in the master service object given
//	"inslist."
// Parameters:
//   - inslist (IN): sequence of instance node ids.
//
// Note: Called only by the SECONDARY replica provider.
//    primary ma_servicegrp::ma_dump_state calls
//	secondary pdts_prov_impl_t::ckpt_dump_ma_service_state calls
//	   secondary pdts_impl_t::dump_ma_serviceobj_state()
//
void
ma_serviceobj::ma_set_instancelist(const network::seqlong_t &inslist)	// IN
{
	unsigned int len, i;
	instanceobj *insobj;

	NET_DBG(("  ma_serviceobj::ma_set_instancelist()\n"));

	len = inslist->length();

	for (i = 0; i < len; i++) {
		insobj = new instanceobj(inslist[i]);
		instancelist.append(insobj);
	}
}

void
ma_servicegrp::ma_set_configlist(const network::seqlong_t &configlist)	// IN
{
	unsigned int len, i;
	instanceobj *ins;

	NET_DBG(("  ma_servicegrp::ma_set_configlist()\n"));

	len = configlist->length();

	for (i = 0; i < len; i++) {
		ins = new instanceobj(configlist[i]);
		config_inst_list.append(ins);
	}
}

void
ma_servicegrp::ma_set_nconfig_inst(int num_of_instances)		// IN
{
	NET_DBG(("  ma_servicegrp::ma_set_nconfig_inst(%d)\n",
		num_of_instances));

	nconfig_inst = num_of_instances;
}

//
// Effects: This routines sets the load distribution stuff "load_distribution"
//   in the master service object state.
//
// Parameters:
//   - load_distribution (IN):  stores the current instances and
//			distribution weights.
//
// Note: Called only by the SECONDARY replica provider.
//   primary ma_servicegrp::ma_dump_state calls
//	secondary pdts_prov_impl_t::ckpt_dump_ma_service_state calls
//	   secondary pdts_impl_t::dump_ma_service_state() calls
//		this routine
//
void
ma_servicegrp::ma_set_loaddist(const network::dist_info_t &load_distribution)
{
	load_dist = (network::dist_info_t)load_distribution;
}

//
// Effects:  Initialize the master PDT for this master service object
//   with the information in "info."
// Parameters:
//   - info (IN):  PDT table in canonical form.
//
// Note: Called only by the Secondary replica provider:
//  primary ma_servicegrp::ma_dump_state
//	secondary pdts_prov_impl_t::ckpt_dump_ma_pdtinfo() calls
//	   secondary  pdts_impl_t::create_pdt_state() calls
//		 this routine
//
void
ma_servicegrp::ma_set_pdtinfo(const network::pdtinfo_t &info)	// IN
{
	NET_DBG(("ma_servicegrp::ma_set_pdtinfo()\n"));

	if (pdts == NULL) {
		NET_DBG(("   creating new PDT\n"));
		ma_create_pdt();	// create new PDT
	}

	// Uncanonicalize the "info" argument into real PDT structure. This
	// routine also sets the state of the table to INITIALIZED.
	(void) pdts->unmarshal_pdtinfo(info);
}

//
// Effects:  For this master service object, this routine creates a new
//   master PDT table for node "node."
// Parameters:
//   - node (IN):  The node for which a master PDT will be created in
//		this service object.
//
// Note: Called only by the SECONDARY replica provider:
//  primary ma_servicegrp::ma_dump_state() calls
//	secondary pdts_prov_impl_t::ckpt_create_ma_pdt() calls
//	   secondary pdts_impl_t::create_ma_pdt() calls
//		this routine
//
void
ma_servicegrp::ma_create_pdt()
{
	NET_DBG(("    ma_servicegrp::ma_create_pdt()\n"));

	pdts = new ma_pdt((uint_t)getnhash());
}

//
// Effects:  This routine initializes a master PDT with the information
//    contained in "pdtinfo."
//
// Parameters:
//   - pdtinfo (IN): The canonical form of a PDT table.
//
// Note: Called only by the SECONDARY replica provider:
//  primary ma_servicegrp::ma_dump_state()
//	secondary pdts_prov_impl_t::ckpt_dump_ma_pdtinfo()
//	  secondary pdts_impl_t::create_pdt_state()
//
void
ma_servicegrp::ma_initialize_pdt(const network::pdtinfo_t& pdtinfo)	// IN
{
	NET_DBG(("    ma_servicegrp::ma_initialize_pdt()\n"));

	if (pdts == NULL) {
		NET_DBG(("      creating new PDT\n"));
		ma_create_pdt();	// create PDT
	}
	ASSERT(pdts != NULL);

	// PDT already created
	//
	// Uncanonicalize the "info" argument into real PDT
	// structure. This routine also sets the state of
	// the table to INITIALIZED.
	//
	(void) pdts->unmarshal_pdtinfo(pdtinfo);
}


//
// Detach a gifnode from this service, and delete
// the slave pdt.
//
// We do not delete the ma_pdt here since we may need it
// later (ie: if the service switches back to the old node).
//
void
ma_servicegrp::detach_gifnode(const network::service_sap_t &ssap,
    nodeid_t node, Environment &e)
{
	NET_DBG(("  ma_servicegrp::detach_gifnode(%d)\n", node));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	network::mcobj_ptr mcobjref = pdts_impl_t::mcnet_node_cbref(node);

	//
	// If there's no mcobj_impl_t for this node then just return.
	// (there's nothing we can do as the node isn't registered)
	//
	if (CORBA::is_nil(mcobjref)) {
		e.exception(new sol::op_e(ENOENT));
		return;
	}

	//
	// Perform appropriate deregistration on slave: delete the
	// the slave PDT.
	//
	mcobjref->mc_detach_gifnode(ssap, e);
}

//
// Attach a gifnode to this service, and propagate the pdtinfo to
// the slave pdt.
//
void
ma_servicegrp::attach_gifnode1(const network::service_sap_t& ssap,
    nodeid_t node, Environment &e)
{
	ma_serviceobj *sobj;

	NET_DBG(("  ma_servicegrp::attach_gifnode1(%d)\n",
		node));
	NET_DBG(("    group = %s\n", getgroup().resource));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// If the node is already attached to this service we're done.
	// XXX Neeed to reset nodeid when gifnode dies XXX
	//
	// if (ma_get_gifnode() == node) {
	//	return;
	// }

	//
	// Get the current service ma_pdt or create
	// a new one.
	//
	if (pdts == NULL) {
		NET_DBG(("     creating new PDT\n"));
		ma_create_pdt();
		if (pdts == NULL) {
			e.exception(new sol::op_e(ENOMEM));
			return;
		}
	}

	pdts->pdt_setflag(PDT_INITIALIZED);

	//
	// Update our notion of the gif node for this service
	//
	ma_serviceobj_list_lock.rdlock();
	ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);

	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->ip_equal(ssap))
			sobj->set_gifnode(node);
	}
	ma_serviceobj_list_lock.unlock();

	//
	// We must keep the instancelist from changing
	// out from underneath updatebuckets.
	//
	instancelist_lock.wrlock();
	update_buckets();
	instancelist_lock.unlock();
}

void
ma_servicegrp::attach_gifnode2(const network::service_sap_t& ssap,
    nodeid_t node, Environment &e)
{
	ASSERT(pdts != NULL);

	NET_DBG(("  ma_servicegrp::attach_gifnode2(), group '%s'\n",
	    getgroup().resource));

	//
	// Now that we have a new master PDT, send it off to the slaves
	// This is a drastic pdt change filtering down from
	// set_primary_gifnode() call. The entire pdt table will be blown
	// away.
	//
	// We should send the new gifnode information down to all the
	// slaves so they can optimize how stop_forwarding calls are
	// invoked (see mcobj_impl.cc::close_conn::execute).
	//
	for (uint_t i = 1; i <= NODEID_MAX; i++) {

		network::mcobj_ptr mcobjref =
		    pdts_impl_t::mcnet_node_cbref(i);

		if (CORBA::is_nil(mcobjref)) {
			continue;
		}

		mcobjref->mc_set_gifnode(ssap, node, e);

		e.clear();
	}

	ma_distribute_pdt(0, COLLECT_ALL, e);

	if (e.exception()) {
		e.exception()->print_exception("ma_distribute_pdt");
	}
}

//
// Return reference to the load balancer
//
network::lbobj_i_ptr
ma_servicegrp::get_lbobj()
{
	return (lb->get_objref_ii());
}

//
// Effects:  Returns the string that represents the type of forwarding
//   list action.
//
char *
ma_servicegrp::fwdinglist_to_string(uint_t type)
{
	switch (type) {
	case ma_servicegrp::DEREGISTER_PROXY:
		return ("DEREGISTER_PROXY");
	case ma_servicegrp::DEREGISTER_INSTANCE:
		return ("DEREGISTER_INSTANCE");
	case ma_servicegrp::COLLECT_ALL:
		return ("COLLECT_ALL");
	default:
		return ("NO SUCH FORWARDING LIST ACTION");
	}
}

void
ma_servicegrp::ma_distribute_pdt(nodeid_t nd, uint_t type, Environment &e)
{
	char gifnodes[NODEID_MAX + 1];
	ma_serviceobj *sobj;
	nodeid_t gnode;
	bool had_exception = false;

	NET_DBG(("  ma_servicegrp::ma_distribute_pdt(%d, %s)\n", nd,
		fwdinglist_to_string(type)));
	MC_PROBE_0(ma_distribute_pdt, "mc net mcfwd", "");

	if (pdts != NULL) {
		bzero(gifnodes, sizeof (gifnodes));

		//
		// Send the changed table to the slaves.
		//
		ma_serviceobj_list_lock.rdlock();

		//
		// Cycle through the entire list of serviceobj's
		// and send the PDT down to the slave.  We keep
		// a node array to ensure that we don't send it
		// down to the same node more than once.
		//
		ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);
		for (; (sobj = iter.get_current()) != NULL;
		    iter.advance()) {
			gnode = sobj->get_gifnode();
			//
			// If node is 0, then this service object has not
			// yet been attached to a gifnode, so we just
			// continue; a separate call to set_primary_gifnode()
			// for another gifnode will take care of this.
			//
			if (gnode == 0) {
				continue;
			}
			if (gifnodes[gnode] == 0) {
				(void) pdts->ma_init_pdt(gnode,
					(uint_t)getnhash(), is_rr_enabled(),
					getgroup(), load_dist, e);
				if (!e.exception()) {
					gifnodes[gnode] = 1;
				} else {
					//
					// Gifnode is dead. No point in
					// collecting and downloading info
					// to a dead gifnode.
					//
					NET_DBG(("     ma_init_pdt "
					    "signalled an exception for"
					    " gifnode %d\n", gnode));
					e.clear();
					had_exception = true;
					continue;
				}

			}
		}

		//
		// sobj is passed to the remote node to identify the
		// group. So we can pick any sobj we want.
		//
		iter.reinit(ma_serviceobj_list);
		sobj = iter.get_current();

		if (is_strong_sticky() && sticky_quiet_period != 0) {
			//
			// Pause some time for packets to reach proxy
			// nodes to help alleviate potential race conditions.
			//
			os::usecsleep((long)sticky_quiet_period);
		}

		for (gnode = 1; gnode <= NODEID_MAX; gnode++) {
			if (gifnodes[gnode] == 1) {
				NET_DBG(("ma_servicegrp::"
				    "ma_distribute_pdt(%d, %s)\n", nd,
				    fwdinglist_to_string(type)));

				//
				// In the following calls, 'this'
				// is passed into pdts, since it
				// doesn't have a pointer back to us, but
				// it needs to get certain info from us,
				// such as if a node is in configlist or
				// if the group is setup for stickiness.
				//
				if (type == COLLECT_ALL) {
					NET_DBG_FWD(("  Collect ALL forwarding"
					    " information\n"));
					(void) pdts->ma_new_gifnode(this,
					    gnode, sobj->getssap());
				} else if (type == DEREGISTER_PROXY) {
					NET_DBG_FWD(("  Clean forwarding"
					    " information for node %d\n",
					    nd));
					pdts->ma_clean_gifnode(nd, gnode, this,
					    sobj->getssap());
				} else if (type == DEREGISTER_INSTANCE) {
					NET_DBG_FWD(("  Collect forwarding"
					    " information from ONLY node %d\n",
					    nd));
					pdts->ma_collect_only(nd, gnode, this,
					    sobj->getssap());
				} else {
					ASSERT(0);
				}
			}
		}

		//
		// ma_init_complete() is pulled out of the above loop to
		// avoid the case of some GIFs having the new PDT while
		// others having the old PDT, *and* all these PDTs are
		// marked INITIALIZED. That is, we pull out
		// ma_init_complete(), which marks PDT INITIALIZED, so
		// that PDT downloads are atomic. This is crucial in order
		// to provide stickiness, where GIFs are supposed to have
		// the same PDT all the time.
		//
		for (gnode = 1; gnode <= NODEID_MAX; gnode++) {
			if (gifnodes[gnode] == 1) {
				pdts->ma_init_complete(gnode, sobj->getssap());
			}
		}

		ma_serviceobj_list_lock.unlock();
		if (had_exception)
			e.exception(new sol::op_e(EAGAIN));
	}
}

bool
ma_servicegrp::ip_equal(const network::service_sap_t &nsap)
{
	ma_serviceobj *sobj;

#ifdef _KERNEL
	ASSERT(ma_serviceobj_list_lock.lock_held());
#endif

	for (ma_serviceobj_list.atfirst();
	    (sobj = ma_serviceobj_list.get_current()) != NULL;
	    ma_serviceobj_list.advance()) {
		if (sobj->ip_equal(nsap)) {
			return (true);
		}
	}

	return (false);
}

bool
ma_servicegrp::equal(const network::service_sap_t &nsap)
{
	ma_serviceobj *sobj;

#ifdef _KERNEL
	ASSERT(ma_serviceobj_list_lock.lock_held());
#endif

	ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->equal(nsap)) {
			return (true);
		}
	}

	return (false);
}

bool
ma_servicegrp::ip_matches(const network::service_sap_t &nsap)
{
	ma_serviceobj *sobj;

	ma_serviceobj_list_lock.rdlock();

	ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);
	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->ip_matches(nsap)) {
			ma_serviceobj_list_lock.unlock();
			return (true);
		}
	}
	ma_serviceobj_list_lock.unlock();

	return (false);
}

bool
ma_servicegrp::matches(const network::service_sap_t &nsap)
{
	ma_serviceobj *sobj;

	ma_serviceobj_list_lock.rdlock();
	ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);

	for (; (sobj = iter.get_current()) != NULL; iter.advance()) {
		if (sobj->matches(nsap)) {
			ma_serviceobj_list_lock.unlock();
			return (true);
		}
	}
	ma_serviceobj_list_lock.unlock();

	return (false);
}

//
// Effects:  Helper routine.  Returns the master service object
//   identified by "ssap" in the master service object list
//   if it exists, else returns NULL.
//
// Parameters:
//   - ssap (IN):    service access point <protocol, ip addd, port>
//
ma_serviceobj *
ma_servicegrp::find_ma_serviceobj(const network::service_sap_t &ssap)
{
	ma_serviceobj *serviceobjp = NULL;

#ifdef _KERNEL
	ASSERT(ma_serviceobj_list_lock.lock_held());
#endif

	//
	// Now find the ma service object in the group given the "sap."
	//
	for (ma_serviceobj_list.atfirst();
	    (serviceobjp = ma_serviceobj_list.get_current()) != NULL;
	    ma_serviceobj_list.advance()) {
		if (serviceobjp->equal(ssap)) {	// found it!
			break;
		}
	}

	return (serviceobjp);
}

//
// Effects: Returns true if node id "nid" is in the instance list
//   of the service object, else returns false.
//
bool
ma_serviceobj::node_exists_in_ilist(nodeid_t nid)
{
	instanceobj *instancep;
	bool found = false;

	// Get a read lock on the object instance list.
	instancelist_lock.rdlock();

	//
	// Iterate over the object instance list.  If you don't find the
	// node id "nid", then return false, else return true.
	//
	instancelist_t::ListIterator iter(instancelist);
	for (; (instancep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (instancep->get_instanceid() == nid) {
			found = true;
			break;
		} // end if
	} // end for

	// Release lock on instance list.
	instancelist_lock.unlock();

	return (found);
}

//
// Effects: Returns true if node id "nid" exists in any service object,
//   else return false.
//
bool
ma_servicegrp::node_exists_in_sobj(nodeid_t nid)
{
	bool		found = false;
	ma_serviceobj	*serviceobjp = NULL;

#ifdef _KERNEL
	ASSERT(ma_serviceobj_list_lock.lock_held());
#endif

	//
	// Create an iterator.  Iterate over the service object list
	// and look for the node id "nid" in the object. If "nid" exists
	// then return true; otherwise, if it exists in no object, then
	// return false.
	//
	ma_serviceobjlist_t::ListIterator iter(ma_serviceobj_list);

	for (; (serviceobjp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (serviceobjp->node_exists_in_ilist(nid)) {
			found = true;
			break;
		}
	}
	return (found);
}

//
// Effects: This method removes the service object identified by "ssap"
//   from the service group.  We preserve the semantics of a service group,
//   that is, that a group is said to be up on a node if at least one
//   instance of some service object is running on that node.  Thus, if
//   the object being removed has the last instance running on some node,
//   then that node is removed the group's knowledge.
//   If the service object does not exist, then this method has no
//   effect.
//
bool
ma_servicegrp::ma_remove_service_object(const network::service_sap_t &ssap)
{
	ma_serviceobj	*sobjp = NULL;
	instanceobj	*sobj_instancep, *ginstancep;
	nodeid_t	node;
	bool		updated_ginslist = false;

	NET_DBG(("  ma_servicegrp::ma_remove_service_object()\n"));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Algorithm:
	// 1. Find the service object given the ssap.
	// 2. Remove the service object from the group list.
	//    But don't destroy object.
	// 3. For every instance id in this service object's instance list,
	//    check all other service objects in the group
	//    for that same instance id.
	//    a. If it exists in ANY other object, then continue;
	//    b. If it doesn't exist anywhere else, then remove it from
	//	 the group instance list.  Alter load_dist array to
	//	indicate the instance is down (it isn't but removing
	//	the service object effectively removes the instance).
	// 4. Destroy service object.

	//
	// Acquire the write lock on the service object list to prevent
	// anyone else from removing this object while we maninpulate it.
	//
	ma_serviceobj_list_lock.wrlock();

	// Look for the service object by "ssap."
	sobjp = find_ma_serviceobj(ssap);
	if (sobjp == NULL) {
		ma_serviceobj_list_lock.unlock();
		return (false);
	}

	//
	// Remove service object from master service object list.
	// But don't delete the object.
	//
	(void) ma_serviceobj_list.erase(sobjp);

	// Get write lock on the GROUP instance list.
	instancelist_lock.wrlock();

	//
	// Iterate over the object instance list.  For each node id,
	// check if node id exists in any other service object.
	// If yes, then continue iteration. If no, remove node id from
	// the group instance list.
	//
	instancelist_t::ListIterator iter1(sobjp->instancelist);
	for (; (sobj_instancep = iter1.get_current()) != NULL;
	    iter1.advance()) {
		node = sobj_instancep->get_instanceid();

		//
		// Does this node id exist in any remaining service
		// object in the group?  If yes, continue, else
		// remove from the group instance list and modify
		// load_dist to show that instance is "down."
		//
		if (node_exists_in_sobj(node)) {
			continue;
		} else {
			// Remove node from group instance list.
			instancelist_t::ListIterator iter2(instancelist);
			while ((ginstancep = iter2.get_current()) != NULL) {
				iter2.advance();
				if (ginstancep->equal(node)) {
					(void) instancelist.erase(ginstancep);
					delete ginstancep;
					--ninst;

					//
					// We removed at least one node
					// from the group instance list.
					//
					updated_ginslist = true;

					// Mark this instance as DOWN.
					ASSERT(load_dist.nodes.length() > node);
					load_dist.nodes[node] =
						network::instance_down;
					ASSERT(lb);
				}
			} // end while
		} // if (node_exists_in_sobj(node))
	} // end for

	//
	// If we removed a node id from the group instance list, then
	// call update buckets to modify the PDT.
	//
	if (updated_ginslist) {
		update_buckets();
	}

	// Release the lock on the group instance list.
	instancelist_lock.unlock();

	// Release the service group list lock.
	ma_serviceobj_list_lock.unlock();

	// Destroy storage for service object.
	delete sobjp;

	return (updated_ginslist);
}

void
ma_servicegrp::marshal_pdt(network::pdtinfo_t *pdtinfo, Environment&)
{
	//
	// Obtain write lock on the instancelist
	//
	instancelist_lock.rdlock();

	if (pdts != NULL && (pdts->pdt_getflag() & PDT_INITIALIZED)) {

		pdtinfo->group = sr_group;
		pdtinfo->policy = sr_policy;

		//
		// Convert to canonical form.
		//
		(void) pdts->marshal_pdtinfo(*pdtinfo);
	} else {
		pdtinfo->hashinfoseq.length(0);
		pdtinfo->nhash = 0;
	}

	//
	// Release the write lock on instancelist
	//
	instancelist_lock.unlock();
}

void
ma_servicegrp::marshal_sgrp(network::grpinfo_t *grpinfo, Environment& e)
{
	instanceobj *instp;
	ma_serviceobj *sobj;
	unsigned int j, cnt;
	nodeid_t num_nodes;

	grpinfo->srv_nhash = (unsigned int)getnhash();
	grpinfo->srv_group = getgroup();
	grpinfo->srv_policy = getpolicy();
	grpinfo->srv_pdt = has_pdt();

	ma_get_sticky_config(grpinfo->srv_stconfig);

	//
	// Attach the load balancing information for all nodes
	//
	num_nodes = load_dist.nodes.length();

	//
	// If the sequence we were given isn't big enough, ask
	// them to try again.
	//
	if (grpinfo->srv_lb_weights.length() < num_nodes) {
		e.exception(new sol::op_e(EAGAIN));
		return;
	}
	grpinfo->srv_lb_weights.length(num_nodes);

	if (num_nodes != 0) {
		for (uint_t i = 0; i < num_nodes; i++) {
			//
			// We only pass back the weights for instances
			// which are up.  There may be a case where an
			// instance has a weight, but is down.  We
			// currently do not pass back seperate information
			// to distinguish whether a node is up or not.
			//
			// The first element (node 0) isn't used by the
			// load balancer, so it probably contains garbage.
			// We pass it back just in case it contains
			// information in the future.  print_net_state
			// should ignore this element.
			//
			if (load_dist.nodes[i] == network::instance_up) {
				grpinfo->srv_lb_weights[i] =
				    load_dist.weights[i];
			} else {
				grpinfo->srv_lb_weights[i] = 0;
			}
		}
	}

	//
	// Attach the group instance list
	//
	// Acquire read lock on group instance list.
	instancelist_lock.rdlock();

	cnt = instancelist.count();

	//
	// If the srv_grpinst sequence is shorter than we need, tell
	// them to try again.  Otherwise, adjust the sequence so
	// that it is only as long as we need.
	//
	if (grpinfo->srv_grpinst.length() < cnt) {
		instancelist_lock.unlock();
		e.exception(new sol::op_e(EAGAIN));
		return;
	}
	grpinfo->srv_grpinst.length(cnt);

	instancelist_t::ListIterator iter(instancelist);
	for (j = 0; (instp = iter.get_current()) != NULL; iter.advance()) {
		grpinfo->srv_grpinst[j++] = instp->get_instanceid();
	}

	//
	// Attach the config instance list
	//
	cnt = config_inst_list.count();

	//
	// If the srv_config sequence is shorter than we need, tell
	// them to try again.  Otherwise, adjust the sequence so
	// that it is only as long as we need.
	//
	if (grpinfo->srv_config.length() < cnt) {
		instancelist_lock.unlock();
		e.exception(new sol::op_e(EAGAIN));
		return;
	}
	grpinfo->srv_config.length(cnt);

	instancelist_t::ListIterator iter2(config_inst_list);
	for (j = 0; (instp = iter2.get_current()) != NULL; iter2.advance()) {
		grpinfo->srv_config[j++] = instp->get_instanceid();
	}

	// Release lock on instance list.
	instancelist_lock.unlock();

	ma_serviceobj_list_lock.rdlock();
	cnt = ma_serviceobj_list.count();

	//
	// If the srv_obj sequence is shorter than we need, tell
	// them to try again.  Otherwise, adjust the sequence so
	// that it is only as long as we need.
	//
	if (grpinfo->srv_obj.length() < cnt) {
		ma_serviceobj_list_lock.unlock();
		e.exception(new sol::op_e(EAGAIN));
		return;
	}
	grpinfo->srv_obj.length(cnt);

	ma_serviceobjlist_t::ListIterator iter3(ma_serviceobj_list);
	for (j = 0; (sobj = iter3.get_current()) != NULL; iter3.advance()) {
		sobj->marshal_sobj(&grpinfo->srv_obj[j++], e);
		if (e.exception()) {
			ma_serviceobj_list_lock.unlock();
			return;
		}
	}
	ma_serviceobj_list_lock.unlock();
}

void
ma_serviceobj::marshal_sobj(network::srvinfo_t *srvinfo, Environment& e)
{
	instanceobj *instancep;
	unsigned int j, cnt;

	srvinfo->srv_gifnode = get_gifnode();
	srvinfo->srv_sap = getssap();

	//
	// Attach the service instance list
	//
	instancelist_lock.rdlock();
	cnt = instancelist.count();

	//
	// If the srv_inst sequence is shorter than we need, tell
	// them to try again.  Otherwise, adjust the sequence so
	// that it is only as long as we need.
	//
	if (srvinfo->srv_inst.length() < cnt) {
		instancelist_lock.unlock();
		e.exception(new sol::op_e(EAGAIN));
		return;
	}
	srvinfo->srv_inst.length(cnt);

	//
	// Marshal the instance list information.
	//
	instancelist_t::ListIterator iter(instancelist);
	for (j = 0; (instancep = iter.get_current()) != NULL; iter.advance()) {
		srvinfo->srv_inst[j++] = instancep->get_instanceid();
	}
	instancelist_lock.unlock();
}

bool
ma_servicegrp::has_pdt()
{
	bool result = false;

	if (pdts != NULL) {
		result = true;
	}
	return (result);
}

void
ma_servicegrp::ma_get_rr_config(network::rr_config_t &rr)
{
	// Default bit-wise copy assignment operator is just fine
	rr = rr_config;
}

//
// Locking: ma_servicegrplist_lock.wrlock() protected
//
network::ret_value_t
ma_servicegrp::ma_config_rr(const network::rr_config_t& rr)
{
	NET_DBG_RR(("ma_servicegrp::ma_config_rr()\n"));
	NET_DBG_RR(("  gname = %s\n", get_group_name()));
	NET_DBG_RR(("  flags = %x\n", rr.rr_flags));
	NET_DBG_RR(("  conn_thres = %d\n", rr.rr_conn_threshold));
	// Default bit-wise copy assignment operator is just fine

	rr_config = rr;

	return (network::SUCCESS);
}
