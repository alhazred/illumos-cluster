//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)net_unode.cc	1.10	09/02/13 SMI"

//
// Emulation routines and entry points for unode.
//

#include <stdio.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <cl_net/errprint.h>
#include <cl_net/sl_service.h>
#include <cl_net/mcobj_impl.h>
#include <cl_net/netlib.h>

#include <unode/output.h>

//
// Hack because inet/ip6.h is no longer public.
//
#define	IPV6_VERSION 6

//
// V6 unode support is not implemented yet.
// Anywhere address is used, we'll use only the least significant 32-bit
//
#define	IPV4(x) (x.v6[3])

//
// Convenience macro for initializing a struct that has a
// string field for the name of something and a field for the
// value of that thing.  This is used for #defined constants
// so a string can be looked up and associated with the
// constant's value.  For example init_value_string(TCPS_IDLE)
// is expanded by the preprocessor to '{ "TCPS_IDLE", -5 }'.
//

#define	init_value_string(x)	{ #x, x }

extern mcobj_impl_t *mcobjp;

//
// Emulate the kernel's cmn_err routine.  This was done so the
// code didn't need to change.  Alternatively the cmn_err calls
// could have been changed to os::warning, which has kernel
// and unode implementations, but os::warning displays differently
// so the kernel behavior is preserved by keeping the cmn_err
// calls and emulating it for unode.
//

void
cmn_err(int level, const char *fmt, ...)
{
	va_list adx;

	if (level < 0 || level >= CE_IGNORE)
		return;

	if (level == CE_NOTE)
		(void) printf("NOTICE: ");
	else if (level == CE_WARN)
		(void) printf("WARNING: ");

	va_start(adx, fmt);		//lint !e40
	(void) vprintf(fmt, adx);
	va_end(adx);

	if (level == CE_NOTE || level == CE_WARN)	// strange,
		(void) printf("\n");			//   but true
	else if (level == CE_PANIC)
		os::panic("");
}

//
// Base Solaris calls cl_net's hook functions through pointers when
// those pointers are non-NULL.  For unode (since we don't have Solaris)
// we have the same set of pointers that Solaris has in tcp.c and udp.c.
// They are initialized to non-NULL values in mc_inet_init(), although
// at the time of this writing cl_inet_connect is always NULL.
//

extern "C" {

// TCP hooks

void (*cl_inet_listen)(netstackid_t stack_id, uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, void *args) = NULL;
void (*cl_inet_unlisten)(netstackid_t stack_id, uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, void *args) = NULL;
void (*cl_inet_connect)(netstackid_t stack_id, uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport, void *args) = NULL;
void (*cl_inet_disconnect)(netstackid_t stack_id, uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, uint8_t *faddrp, in_port_t fport, void *args) = NULL;

// UDP hooks

void (*cl_inet_bind)(netstackid_t stack_id, uchar_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, void *args) = NULL;
void (*cl_inet_unbind)(netstackid_t stack_id, uint8_t protocol, sa_family_t addr_family,
    uint8_t *laddrp, in_port_t lport, void *args) = NULL;

}

//
// Emulation of base Solaris cl_tcp_walk_list.  sl_servicegrp_t::sl_conninfo
// calls cl_tcp_walk_list passing it a pointer to the routine get_conn_info,
// which is called back with info for each tcp connection the system
// knows about.  In a real cluster application, making system calls and
// incoming packets create and change the state of tcp connections.  For
// unode the tcp_* entry points add and change tcp connection info.
//

//
// We want a list of cl_tcp_info_t's, so we wrap one in this struct
// which inherits the list element thingie needed to string the
// cl_tcp_info_t's together.  Next we have a mutex for the list and
// the list itself.
//

struct tcp_conn_list_item : public _SList::ListElem {
	tcp_conn_list_item() : _SList::ListElem(this) {}
	cl_tcp_info_t info;
};

static os::mutex_t tcp_conn_list_mtx;
static IntrList<tcp_conn_list_item, _SList> tcp_conn_list;

//
// The emulation of cl_tcp_walk_list goes through the list of cl_tcp_info_t
// structs maintained by the tcp_* entry points and makes a callback for
// each.
//
#if SOL_VERSION >= __s10
extern "C" int
cl_tcp_walk_list(netstackid_t, int (*callback)(cl_tcp_info_t *, void *),
    void *arg)
#else	// SOL_VERSION >= __s10
extern "C" int
cl_tcp_walk_list(int (*callback)(cl_tcp_info_t *, void *), void *arg)
#endif // SOL_VERSION >= __s10
{
	SList<tcp_conn_list_item>::ListIterator iter(tcp_conn_list);
	tcp_conn_list_item *lip;
	int ret = 0;

	tcp_conn_list_mtx.lock();

	for (; (lip = iter.get_current()) != NULL; iter.advance()) {
		if (callback(&lip->info, arg) != 0) {
			ret = 1;
			break;
		}
	}

	tcp_conn_list_mtx.unlock();

	return (ret);
}

//
// Entry points and their support routines.  The entry points have
// similar forms in that they convert the input string arguments to
// the proper types, printing a usage message if the number or format
// of arguments is incorrect.  For TCP entry points the tcp list is
// updated and the hook function (normally called by base Solaris)
// called.
//

//
// Stub routine that can be given to unode_load during initial
// load, such as "unode_load foo 1 cl_net init".  unode_load
// wants to call something, so have it call the no-op init.
// The initial load of the unode module will first call unode_init
// in mcnet.cc.
//

int
init(int, char **)
{
	return (0);
}

//
// Convert string representation of a TCP state to the state.  Allow
// arbitrary integer too (for testing).
//

static bool
state_str_to_state(const char *ss, int *sp, FILE *fp)
{
	static struct state_str {
		const char *str;
		int32_t state;
	} state_strs[] = {
		init_value_string(TCPS_CLOSED),
		init_value_string(TCPS_IDLE),
		init_value_string(TCPS_BOUND),
		init_value_string(TCPS_LISTEN),
		init_value_string(TCPS_SYN_SENT),
		init_value_string(TCPS_SYN_RCVD),
		init_value_string(TCPS_ESTABLISHED),
		init_value_string(TCPS_CLOSE_WAIT),
		init_value_string(TCPS_FIN_WAIT_1),
		init_value_string(TCPS_CLOSING),
		init_value_string(TCPS_LAST_ACK),
		init_value_string(TCPS_FIN_WAIT_2),
		init_value_string(TCPS_TIME_WAIT),
	};
	uint_t i;

	// If string has only characters of an integer, use atoi.

	if (strspn(ss, "-0123456789") == strlen(ss)) {
		*sp = atoi(ss);
		return (true);
	}

	for (i = 0; i < sizeof (state_strs) / sizeof (state_strs[0]); i++)
		if (strcasecmp(state_strs[i].str, ss) == 0) {
			*sp = state_strs[i].state;
			return (true);
		}

	(void) fprintf(fp, "bad state: %s\n", ss);
	return (false);
}

//
// Convert a string representation of an IP address into an
// in6_addr_t in a form the kernel uses when it put things in
// the TCP connection list.  It would be nice to use the
// code in #ifdef FUTURE, but we have to compile and link on
// Solaris 7, which doesn't have inet_pton.  When we move
// completely away from Solaris 7 this code (and comment) can
// be udpated.
//
// We recognize IPv4 addresses and a few forms of IPv6 addresses,
// as well as strings for inaddr_any and loopback.  If the
// address is something like a.b.c.d it is converted to a mapped
// v6 address which would be written as ::FFFF:a.b.c.d.  An
// address like ::a.b.c.d is unmapped and only the v4 portion of
// the v6 address is set.  The double colon an also be used in
// forms like ::0 (the v6 inaddr_any) or ::1 (the v6 loopback).
//
// At present the cl_net code only looks at the v4 part of the
// address in get_conn_info, but we allow mapped addresses for
// future use.  Note that Solaris doesn't seem to consistently
// map v4 addresses; the cl_tcp_info_t passed to get_conn_info
// could have a local address of ::FFFF:10.146.84.72 and
// a foreign address of ::10.146.84.58.
//
// This code is correct for both big endian (SPARC) and little
// endian (Intel).
//

static bool
addr_str_to_addr(const char *astr, in6_addr_t *iap, FILE *fp)
{
#ifdef FUTURE
	const char *col, *dot;
	const char *pfx = "::FFFF:";
	const char *asl;
	char *tmp = NULL;
	size_t len;

	col = strchr(astr, ':');
	dot = strchr(astr, '.');

	if (col == NULL && dot == NULL) {
		if (strcasecmp(astr, "INADDR_ANY") == 0)
			astr = "0.0.0.0";
		else if (strcasecmp(astr, "INADDR_LOOPBACK") == 0)
			astr = "127.0.0.1";
		else {
			(void) fprintf(fp, "%s: bad address string\n", astr);
			return (false);
		}
	}

	if (col == NULL) {
		len = strlen(pfx) + strlen(astr) + 1;
		tmp = (char *)malloc(len);
		if (tmp == NULL) {
			(void) fprintf(fp, "malloc(%u) failed\n", len);
			return (false);
		}
		(void) strcpy(tmp, pfx);
		(void) strcat(tmp, astr);
		asl = tmp;
	} else
		asl = astr;

	if (!inet_pton(AF_INET6, asl, iap)) {
		(void) fprintf(fp, "%s: bad address string\n", astr);
		return (false);
	}

	if (asl == tmp)
		free(tmp);

	return (true);
#else
	char *cp;
	in_addr_t v4addr, *v4ap;
	ushort_t *usp;
	bool map;

	//
	// A v4 address is the upper four bytes of the 16-byte
	// IPv6 address, thus the offset of 12 into the IPv6
	// address for the start of the IPv4 address.
	//

	cp = (char *)iap;
	v4ap = (in_addr_t *)&cp[12];

	if (strcasecmp(astr, "INADDR_ANY") == 0)
		astr = "0.0.0.0";
	else if (strcasecmp(astr, "INADDR_LOOPBACK") == 0)
		astr = "127.0.0.1";

	if (astr[0] == ':' && astr[1] == ':') {
		map = false;
		astr += 2;
	} else
		map = true;

	//
	// At this point accept only dotted decimal forms.  Again,
	// we could do more sophisticated checking, but this is
	// only unode.
	//

	if (strspn(astr, ".0123456789") != strlen(astr)) {
		(void) fprintf(fp, "%s: bad address string\n", astr);
		return (false);
	}

	v4addr = inet_addr(astr);
	bzero(iap, sizeof (*iap));
	*v4ap = v4addr;
	if (map) {
		//
		// The v4 address starts at offset 12 (see above) so
		// having an unsigned short pointer set to offset 10
		// lets us write the two bytes of 1's before the v4
		// address to indicate a mapped address.
		//

		usp = (ushort_t *)&cp[10];
		*usp = 0xFFFF;
	}

	return (true);
#endif
}

//
// Convert string representation of protocol to the protocol.  Also
// allow arbitrary numeric value (for testing).
//

static bool
prot_str_to_prot(const char *ps, uint8_t *pp, FILE *fp)
{
	static struct prot_str {
		const char *str;
		int32_t prot;
	} prot_strs[] = {
		init_value_string(IPPROTO_IP),
		init_value_string(IPPROTO_HOPOPTS),
		init_value_string(IPPROTO_ICMP),
		init_value_string(IPPROTO_IGMP),
		init_value_string(IPPROTO_GGP),
		init_value_string(IPPROTO_ENCAP),
		init_value_string(IPPROTO_TCP),
		init_value_string(IPPROTO_EGP),
		init_value_string(IPPROTO_PUP),
		init_value_string(IPPROTO_UDP),
		init_value_string(IPPROTO_IDP),
		init_value_string(IPPROTO_IPV6),
		init_value_string(IPPROTO_ROUTING),
		init_value_string(IPPROTO_FRAGMENT),
		init_value_string(IPPROTO_RSVP),
		init_value_string(IPPROTO_ESP),
		init_value_string(IPPROTO_AH),
		init_value_string(IPPROTO_ICMPV6),
		init_value_string(IPPROTO_NONE),
		init_value_string(IPPROTO_DSTOPTS),
		init_value_string(IPPROTO_HELLO),
		init_value_string(IPPROTO_ND),
		init_value_string(IPPROTO_EON),
		init_value_string(IPPROTO_PIM),
		init_value_string(IPPROTO_RAW),
		init_value_string(IPPROTO_MAX),
	};
	uint_t i;

	if (strspn(ps, "0123456789") == strlen(ps)) {
		*pp = (uint8_t)atoi(ps);
		return (true);
	}

	for (i = 0; i < sizeof (prot_strs) / sizeof (prot_strs[0]); i++)
		if (strcasecmp(prot_strs[i].str, ps) == 0) {
			*pp = (uint8_t)prot_strs[i].prot;
			return (true);
		}

	(void) fprintf(fp, "bad protocol: %s\n", ps);
	return (false);
}

//
// Convert string representation of address family to
// address family.
//

static bool
af_str_to_af(const char *afs, sa_family_t *afp, FILE*fp)
{
	if (strcasecmp(afs, "AF_INET") == 0) {
		*afp = AF_INET;
		return (true);
	}
	if (strcasecmp(afs, "AF_INET6") == 0) {
		*afp = AF_INET6;
		return (true);
	}

	(void) fprintf(fp, "bad address family: %s\n", afs);
	return (false);
}

//
// Allocate a list item and initialize it by converting the
// arguments for address family, addresses, and ports, which
// are in string representation.  Like tcp_init() in Solaris,
// tcp_state is initialized to TCPS_IDLE.
//

static tcp_conn_list_item *
init_list_item(
	const char *afs,	// address family string
	const char *las,	// local address string
	const char *lps,	// local port
	const char *fas,	// foreign address
	const char *fps,	// foreign port
	FILE *fp)
{
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;
	sa_family_t af;

	lip = new tcp_conn_list_item;
	if (lip == NULL) {
		(void) fprintf(fp, "init_list_item: out of memory\n");
		return (NULL);
	}

	tip = &lip->info;

	if (!af_str_to_af(afs, &af, fp)) {
		delete lip;
		return (NULL);
	}

	tip->cl_tcpi_version = CL_TCPI_V1;
	tip->cl_tcpi_ipversion = (af == AF_INET ? IPV4_VERSION : IPV6_VERSION);
	tip->cl_tcpi_state = TCPS_IDLE;
	tip->cl_tcpi_lport = (in_port_t)atoi(lps);
	if (fps == NULL)
		tip->cl_tcpi_fport = 0;
	else
		tip->cl_tcpi_fport = (in_port_t)atoi(fps);
	if (!addr_str_to_addr(las, &tip->cl_tcpi_laddr_v6, fp))  {
		delete lip;
		return (NULL);
	}

	//
	// In cases where there is no foreign address, NULL is passed
	// for the foreign address string.  Check for that and zero
	// out the address if fas is NULL.
	//

	if (fas == NULL)
		bzero(&tip->cl_tcpi_faddr_v6, sizeof (tip->cl_tcpi_faddr_v6));
	else if (!addr_str_to_addr(fas, &tip->cl_tcpi_faddr_v6, fp))  {
		delete lip;
		return (NULL);
	}

	return (lip);
}

//
// Operations for the tcp list:
//
//	LIST_DELETE	delete if found, error if not found
//	LIST_APPEND	error if found, append otherwise
//	LIST_MODIFY	update item state if found, append otherwise
//

enum update_op_t { LIST_DELETE, LIST_APPEND, LIST_MODIFY };

//
// Update the tcp list maintained by the tcp_* entry points below.
// For error handling simplicity of the callers, this routine
// manages the storage of the list item, deleting it in all cases
// except when it is appended to the list.
//
// This routine can be used to update the state of an item already
// in the list, so the matching done when looking through the list
// includes everything except the state.
//
// If the operation is successful true is returned, false otherwise.
//

static bool
update_tcp_list(tcp_conn_list_item *lip, update_op_t op)
{
	tcp_conn_list_item *lip2;
	cl_tcp_info_t *tip, *tip2;
	SList<tcp_conn_list_item>::ListIterator iter(tcp_conn_list);
	bool ret = true;

	tip = &lip->info;

	tcp_conn_list_mtx.lock();

	for (; (lip2 = iter.get_current()) != NULL; iter.advance()) {
		tip2 = &lip2->info;

		// See if we already have the item.

		if (tip->cl_tcpi_version == tip2->cl_tcpi_version &&
		    tip->cl_tcpi_ipversion == tip2->cl_tcpi_ipversion &&
		    tip->cl_tcpi_lport == tip2->cl_tcpi_lport &&
		    tip->cl_tcpi_fport == tip2->cl_tcpi_fport &&
		    bcmp(&tip->cl_tcpi_laddr_v6, &tip2->cl_tcpi_laddr_v6,
		    sizeof (tip->cl_tcpi_laddr_v6)) == 0 &&
		    bcmp(&tip->cl_tcpi_faddr_v6, &tip2->cl_tcpi_faddr_v6,
		    sizeof (tip->cl_tcpi_faddr_v6)) == 0) {

			//
			// Have a match.  This is ok for delete or modify,
			// but is an error for the append operation.
			//

			if (op == LIST_DELETE) {
				(void) tcp_conn_list.erase(lip2);
				tcp_conn_list_mtx.unlock();
				delete lip2;
				delete lip;
				return (true);
			}

			if (op == LIST_APPEND) {
				tcp_conn_list_mtx.unlock();
				delete lip;
				return (false);
			}

			if (op == LIST_MODIFY) {

				//
				// Everything matched, without checking
				// the current state.  Update the state
				// and return, deleting the item passed
				// in because we found an item we could
				// update.
				//

				tip2->cl_tcpi_state = tip->cl_tcpi_state;
				tcp_conn_list_mtx.unlock();
				delete lip;
				return (true);
			}

			break;
		}
	}

	//
	// Don't have a match.  This is an error for delete, but ok
	// for modify or append.
	//

	if (lip2 == NULL) {
		if (op == LIST_DELETE) {
			delete lip;
			ret = false;
		} else if (op == LIST_APPEND || op == LIST_MODIFY)
			tcp_conn_list.append(lip);
	}

	tcp_conn_list_mtx.unlock();

	return (ret);
}

//
// TCP unode entry points.  Some of the entry points take a protocol
// argument.  The entry points that do are those that call one of
// cl_inet_* hook routines.  Base Solaris only calls the hooks with
// IPPROTO_TCP, but for flexibility in testing the hooks, these entry
// points get the protocol from a command line argument.
//
// As an example of how the entry points can be used to modify the
// tcp list and call the hook functions, the following sequence would
// simulate a TCP service starting up, accepting a client connection,
// the client closing the connection, and the service exiting:
//
//	tcp_listen IPPROTO_TCP AF_INET inaddr_any 80
//	tcp_connect IPPROTO_TCP AF_INET 10.146.84.64 80 10.146.84.58 39814
//	tcp_state AF_INET 10.146.84.64 80 10.146.84.58 39814 TCPS_SYN_RCVD
//	tcp_state AF_INET 10.146.84.64 80 10.146.84.58 39814 TCPS_ESTABLISHED
//	tcp_state AF_INET 10.146.84.64 80 10.146.84.58 39814 TCPS_CLOSE_WAIT
//	tcp_state AF_INET 10.146.84.64 80 10.146.84.58 39814 TCPS_LAST_ACK
//	tcp_disconnect IPPROTO_TCP AF_INET 10.146.84.64 80 10.146.84.58 39814
//	tcp_unlisten IPPROTO_TCP AF_INET inaddr_any 80
//
// Going through the various states of a connection with tcp_state may
// not be necessary unless the testing you're doing involves calls to
// cl_tcp_walk_list that want to see connections in the various states.
//

//
// Unode entry point.  Add the tcp list item in the listening state,
// or update an existing entry, then call the listen hook function.
// Base Solaris only calls the cl_inet_listen hook with IPPROTO_TCP,
// but for flexibility in testing the hook, this entry point gets the
// protocol from a command line argument.
//

int
tcp_listen(int argc, char **argv)
{
	output out(&argc, argv);
	uint8_t protocol;
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;
	ushort_t ipv;
	in_addr_t ia4;
	in6_addr_t ia6;
	in_port_t port;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: tcp_listen protocol addr-family "
		    "addr port\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);

	lip = init_list_item(argv[1], argv[2], argv[3], NULL, NULL, out);
	if (lip == NULL)
		return (-1);

	tip = &lip->info;
	tip->cl_tcpi_state = TCPS_LISTEN;

	//
	// If the item already exists, update_tcp_list() with LIST_MODIFY
	// will delete the item returned from init_list_item(), so we need
	// to save the info we'll need to pass to the hook fucntion.
	//

	ipv = tip->cl_tcpi_ipversion;
	if (ipv == IPV4_VERSION)
		ia4 = ((in_addr_t *)&tip->cl_tcpi_laddr_v6)[3];
	else
		ia6 = tip->cl_tcpi_laddr_v6;
	port = tip->cl_tcpi_lport;

	(void) update_tcp_list(lip, LIST_MODIFY);


	if (cl_inet_listen != NULL) {
		if (ipv == IPV4_VERSION) {
			(*cl_inet_listen)(0, protocol, AF_INET, (uint8_t *)&ia4,
			    port, NULL);
		} else
			(*cl_inet_listen)(0, protocol, AF_INET6, (uint8_t *)&ia6,
			    port, NULL);
	}

	return (0);
}

//
// Unode entry point.  Call the unlisten hook function then delete
// the item from the tcp list.  We should really make sure the item
// is in the tcp list before calling the hook, but the hook will
// ignore it as an unknown connection and we will still return -1
// indicating an error.
//

int
tcp_unlisten(int argc, char **argv)
{
	output out(&argc, argv);
	uint8_t protocol;
	in_addr_t *ia4p;
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: tcp_unlisten protocol addr-family "
		    "addr port\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);

	lip = init_list_item(argv[1], argv[2], argv[3], NULL, NULL, out);
	if (lip == NULL)
		return (-1);

	tip = &lip->info;

	if (cl_inet_unlisten != NULL) {
		if (tip->cl_tcpi_ipversion == IPV4_VERSION) {
			ia4p = &((in_addr_t *)&tip->cl_tcpi_laddr_v6)[3];

			(*cl_inet_unlisten)(0, protocol, AF_INET, (uint8_t *)ia4p,
			    tip->cl_tcpi_lport, NULL);
		} else
			(*cl_inet_unlisten)(0, protocol, AF_INET6,
			    (uint8_t *)&tip->cl_tcpi_laddr_v6,
			    tip->cl_tcpi_lport, NULL);
	}

	if (!update_tcp_list(lip, LIST_DELETE)) {
		(void) fprintf(out, "tcp_unlisten: connection not found\n");
		return (-1);
	}

	return (0);
}

//
// Unode entry point.  Add an item to the tcp list and call the
// connect hook if it is non-NULL.  (Currently mc_inet_init() does
// not set the hook, so it will not be called.)  The list item
// created is distinct from an item created earlier by tcp_listen
// because the new item has a non-zero foreign address and port.
// As with tcp_accept_comm() in Solaris, the new item is in the
// tcp list with state TCPS_IDLE when the hook is called and
// then set to TCPS_LISTEN.  With a real connection, the state would
// change to TCPS_SYN_RCVD and TCPS_ESTABLISHED as the handshake
// proceeded.  We leave the state as TCPS_LISTEN to allow greater
// testing flexibility.  The state changes can be made with tcp_state
// if desired.
//

int
tcp_connect(int argc, char **argv)
{
	output out(&argc, argv);
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;
	uint8_t protocol;
	in_addr_t *lia4p, *fia4p;

	argc--; argv++;

	if (argc != 6) {
		(void) fprintf(out, "usage: tcp_connect protocol addr-family "
		    "laddr lport faddr fport\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);

	lip = init_list_item(argv[1], argv[2], argv[3], argv[4], argv[5], out);
	if (lip == NULL)
		return (-1);

	if (!update_tcp_list(lip, LIST_APPEND)) {
		(void) fprintf(out, "tcp_connect: connection already exists\n");
		return (-1);
	}

	tip = &lip->info;

	if (cl_inet_connect != NULL) {
		if (tip->cl_tcpi_ipversion == IPV4_VERSION) {
			lia4p = &((in_addr_t *)&tip->cl_tcpi_laddr_v6)[3];
			fia4p = &((in_addr_t *)&tip->cl_tcpi_faddr_v6)[3];

			(*cl_inet_connect)(0, protocol, AF_INET, (uint8_t *)lia4p,
			    tip->cl_tcpi_lport,
			    (uint8_t *)fia4p, tip->cl_tcpi_fport, NULL);
		} else
			(*cl_inet_connect)(0, protocol, AF_INET6,
			    (uint8_t *)&tip->cl_tcpi_laddr_v6,
			    tip->cl_tcpi_lport,
			    (uint8_t *)&tip->cl_tcpi_faddr_v6,
			    tip->cl_tcpi_fport, NULL);
	}

	tip->cl_tcpi_state = TCPS_LISTEN;

	return (0);
}

//
// Unode entry point.  Remove the item from the tcp list then call
// the disconnect hook function.  This is the same order as Solaris.
//

int
tcp_disconnect(int argc, char **argv)
{
	output out(&argc, argv);
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;
	uint8_t protocol;
	ushort_t ipv;
	in_addr_t lia4, fia4;
	in6_addr_t lia6, fia6;
	in_port_t lport, fport;

	argc--; argv++;

	if (argc != 6) {
		(void) fprintf(out, "usage: tcp_disconnect protocol "
		    "addr-family laddr lport faddr fport\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);

	lip = init_list_item(argv[1], argv[2], argv[3], argv[4], argv[5], out);
	if (lip == NULL)
		return (-1);

	//
	// update_tcp_list() is going to delete the item returned
	// from init_list_item(), so we need to save the info we'll
	// need to pass to the hook fucntion.
	//

	tip = &lip->info;
	ipv = tip->cl_tcpi_ipversion;
	if (ipv == IPV4_VERSION) {
		lia4 = ((in_addr_t *)&tip->cl_tcpi_laddr_v6)[3];
		fia4 = ((in_addr_t *)&tip->cl_tcpi_faddr_v6)[3];
	} else {
		lia6 = tip->cl_tcpi_laddr_v6;
		fia6 = tip->cl_tcpi_faddr_v6;
	}
	lport = tip->cl_tcpi_lport;
	fport = tip->cl_tcpi_fport;

	if (!update_tcp_list(lip, LIST_DELETE)) {
		(void) fprintf(out, "tcp_disconnect: connection not found\n");
		return (-1);
	}

	if (cl_inet_disconnect != NULL) {
		if (ipv == IPV4_VERSION)
			(*cl_inet_disconnect)(0, protocol, AF_INET,
			    (uint8_t *)&lia4, lport, (uint8_t *)&fia4, fport, NULL);
		else
			(*cl_inet_disconnect)(0, protocol, AF_INET6,
			    (uint8_t *)&lia6, lport, (uint8_t *)&fia6, fport, NULL);
	}

	return (0);
}

//
// Unode entry point.  Create or update a list item with the
// given TCP state.
//

int
tcp_state(int argc, char **argv)
{
	output out(&argc, argv);
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;

	argc--; argv++;

	if (argc != 6) {
		(void) fprintf(out, "usage: tcp_state addr-family "
		    "laddr lport faddr fport tcp-state\n");
		return (-1);
	}

	lip = init_list_item(argv[0], argv[1], argv[2], argv[3], argv[4], out);
	if (lip == NULL)
		return (-1);

	tip = &lip->info;
	if (!state_str_to_state(argv[5], &tip->cl_tcpi_state, out)) {
		delete lip;
		return (-1);
	}

	(void) update_tcp_list(lip, LIST_MODIFY);

	return (0);
}

//
// Print out the tcp connection list.
//

static void
tcp_conn_list_print(FILE *fp)
{
	SList<tcp_conn_list_item>::ListIterator iter(tcp_conn_list);
	tcp_conn_list_item *lip;
	cl_tcp_info_t *ip;
	unsigned char *ucp;

	tcp_conn_list_mtx.lock();

	for (; (lip = iter.get_current()) != NULL; iter.advance()) {
		ip = &lip->info;

		(void) fprintf(fp, "v: %u ipv: %u s: %d lp: %u fp: %u\n",
		    ip->cl_tcpi_version, ip->cl_tcpi_ipversion,
		    ip->cl_tcpi_state,
		    ip->cl_tcpi_lport, ip->cl_tcpi_fport);
		ucp = (unsigned char *)&ip->cl_tcpi_laddr_v6;
		(void) fprintf(fp, "laddr: %u %u %u %u %u %u %u %u %u %u %u "
		    "%u %u %u %u %u\n",
		    ucp[0], ucp[1], ucp[2], ucp[3], ucp[4], ucp[5],
		    ucp[6], ucp[7], ucp[8], ucp[9], ucp[10], ucp[11], ucp[12],
		    ucp[13], ucp[14], ucp[15]);

		ucp = (unsigned char *)&ip->cl_tcpi_faddr_v6;
		(void) fprintf(fp, "faddr: %u %u %u %u %u %u %u %u %u %u %u "
		    "%u %u %u %u %u\n",
		    ucp[0], ucp[1], ucp[2], ucp[3], ucp[4], ucp[5],
		    ucp[6], ucp[7], ucp[8], ucp[9], ucp[10], ucp[11], ucp[12],
		    ucp[13], ucp[14], ucp[15]);
	}

	tcp_conn_list_mtx.unlock();
}

//
// Unode entry point.  tcp_conn_mod lets you add or update the tcp
// list with arbitrary values.  It can also print out the tcp list.
//
// The order of the arguments is the same as the order of the fields
// in cl_tcp_info_t.
//
// The special state "delete" indicates the item should be removed
// from the tcp list.
//

int
tcp_conn_mod(int argc, char **argv)
{
	output out(&argc, argv);
	bool list = false;
	ushort_t v, ipv;
	bool del;
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;

	argc--; argv++;

	if (argc >= 1) {
		list = (strcmp(argv[0], "list") == 0);
		if (list && argc != 1) {
			(void) fprintf(out, "usage: tcp_conn_mod list\n");
			return (-1);
		}
	}

	if (list) {
		tcp_conn_list_print(out);
		return (0);
	}

	if (argc != 7) {
		(void) fprintf(out, "usage: tcp_conn_mod version ip-version "
		    "state lport fport laddr faddr\n");
		return (-1);
	}

	v = (ushort_t)atoi(argv[0]);
	ipv = (ushort_t)atoi(argv[1]);
	del = (strcmp(argv[2], "delete") == 0);

	lip = init_list_item(ipv == 4 ? "af_inet" : "af_inet6", argv[5],
	    argv[3], argv[6], argv[4], out);
	if (lip == NULL)
		return (-1);

	tip = &lip->info;
	tip->cl_tcpi_version = v;

	if (del) {
		if (!update_tcp_list(lip, LIST_DELETE)) {
			(void) fprintf(out, "tcp_conn_mod delete failed: "
			    "connection not found\n");
			return (-1);
		}
	} else {
		if (!state_str_to_state(argv[2], &tip->cl_tcpi_state, out)) {
			delete lip;
			return (-1);
		}
		(void) update_tcp_list(lip, LIST_MODIFY);
	}

	return (0);
}

//
// UDP unode entry points.  Some of the entry points take a protocol
// argument.  The entry points that do are those that call one of
// cl_inet_* hook routines.  Base Solaris only calls the hooks with
// IPPROTO_UDP, but for flexibility in testing the hooks, these entry
// points get the protocol from a command line argument.
//

//
// Unode entry point.  Call the UDP bind hook function.
//

int
udp_bind(int argc, char **argv)
{
	output out(&argc, argv);
	uint8_t protocol;
	in_addr_t *ia4p;
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: udp_bind protocol addr-family "
		    "addr port\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);

	//
	// We're not putting anything on the list, but use
	// init_list_item to convert args for convenience.
	//

	lip = init_list_item(argv[1], argv[2], argv[3], NULL, NULL, out);
	if (lip == NULL)
		return (-1);

	tip = &lip->info;

	if (cl_inet_bind != NULL) {
		if (tip->cl_tcpi_ipversion == IPV4_VERSION) {
			ia4p = &((in_addr_t *)&tip->cl_tcpi_laddr_v6)[3];

			(*cl_inet_bind)(0, protocol, AF_INET, (uint8_t *)ia4p,
			    tip->cl_tcpi_lport, NULL);
		} else
			(*cl_inet_bind)(0, protocol, AF_INET6,
			    (uint8_t *)&tip->cl_tcpi_laddr_v6,
			    tip->cl_tcpi_lport, NULL);
	}

	delete lip;

	return (0);
}

//
// Unode entry point.  Call the UDP unbind hook function.
//

int
udp_unbind(int argc, char **argv)
{
	output out(&argc, argv);
	uint8_t protocol;
	in_addr_t *ia4p;
	tcp_conn_list_item *lip;
	cl_tcp_info_t *tip;

	argc--; argv++;

	if (argc != 4) {
		(void) fprintf(out, "usage: udp_unbind protocol addr-family "
		    "addr port\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);

	lip = init_list_item(argv[1], argv[2], argv[3], NULL, NULL, out);
	if (lip == NULL)
		return (-1);

	tip = &lip->info;

	if (cl_inet_unbind != NULL) {
		if (tip->cl_tcpi_ipversion == IPV4_VERSION) {
			ia4p = &((in_addr_t *)&tip->cl_tcpi_laddr_v6)[3];

			(*cl_inet_unbind)(0, protocol, AF_INET, (uint8_t *)ia4p,
			    tip->cl_tcpi_lport, NULL);
		} else
			(*cl_inet_unbind)(0, protocol, AF_INET6,
			    (uint8_t *)&tip->cl_tcpi_laddr_v6,
			    tip->cl_tcpi_lport, NULL);
	}

	delete lip;

	return (0);
}

//
// Unode entry point.  Calling this "packet" is a bit presumptuous as
// we're not really handling a packet, but for unode the main thing
// we want to know is how a packet will be directed.  On a real cluster,
// cl_net gets a packet in mcnet_rput, which calls demux_data, which
// calls naccept.  Look at those routines to see how things work for
// real.  Here we borrow from naccept the parts related to directing a
// packet based on protocol, addresses, and ports.
//
// After checking for appropriate protocol and if the packet
// belongs to a scalable service we call sl_input to have scalable
// services tell us where the packet would go and then print out
// whether the packet would be dropped, sent up, or directed to a
// node.
//

int
packet(int argc, char **argv)
{
	output out(&argc, argv);
	uint8_t protocol;
	in6_addr_t dst_ia6, src_ia6;
	in_port_t dst_port, src_port;
	network::service_sap_t ssap;
	sl_servicegrp_t *servicep;
	network::cid_t cid;
	nodeid_t node;
	network::cid_action_t action;
	uint32_t index;

	argc--; argv++;

	if (argc != 5) {
		(void) fprintf(out, "usage: packet protocol dst-addr dst-port "
		    "src-addr src-port\n");
		return (-1);
	}

	if (!prot_str_to_prot(argv[0], &protocol, out))
		return (-1);
	if (!(protocol == IPPROTO_TCP || protocol == IPPROTO_UDP)) {

		//
		// Not a protocol we handle, send up.  On a real
		// cluster ICMP is also handled - see naccept for details.
		//

		(void) fprintf(out, "CID_SENDUP\n");
		return (0);
	}

	if (!addr_str_to_addr(argv[1], &dst_ia6, out))
		return (-1);
	dst_port = (in_port_t)atoi(argv[2]);
	if (!addr_str_to_addr(argv[3], &src_ia6, out))
		return (-1);
	src_port = (in_port_t)atoi(argv[4]);

	ssap.protocol = protocol;
	IPV4(ssap.laddr) = ((in_addr_t *)&dst_ia6)[3];
	ssap.lport = dst_port;

	//
	// The code from here up to the check for node == 0
	// is adapted from naccept().  If naccept is changed,
	// make an analogous change here.
	//

	mcobjp->sl_servicegrplist_lock.rdlock();
	servicep = mcobjp->mc_match(ssap);
	if (servicep == NULL) {

		// Not for a scalable service, send up.

		mcobjp->sl_servicegrplist_lock.unlock();
		(void) fprintf(out, "CID_SENDUP\n");
		return (0);
	}

	cid.ci_protocol = ssap.protocol;
	IPV4(cid.ci_laddr) = IPV4(ssap.laddr);
	cid.ci_lport = ssap.lport;
	IPV4(cid.ci_faddr) = ((in_addr_t *)&src_ia6)[3];
	cid.ci_fport = src_port;

	//
	// Call sl_input to find out where the packet should go.
	// The last argument is a flag argument that, on a real
	// cluster, is 0 for non-TCP protocols and the th_flags
	// field of a TCP header for TCP.  TH_SYN is an example
	// of a bit that could be set in the flags.  We just pass
	// in 0 to keep it simple, but this code be extended to
	// take various th_flags bits as an argument and pass
	// the values to sl_input.
	//

	node = servicep->sl_input(cid, action, index, 0);

	mcobjp->sl_servicegrplist_lock.unlock();

	if (node == 0)
		action = network::CID_DROP;

	switch (action) {

	case network::CID_SENDUP:
		(void) fprintf(out, "CID_SENDUP\n");
		break;

	case network::CID_DROP:
		(void) fprintf(out, "CID_DROP\n");
		break;

	case network::CID_FORWARD:
		(void) fprintf(out, "CID_FORWARD to node %u\n", node);
		break;

	case network::CID_DISPATCH:
		(void) fprintf(out, "CID_DISPATCH to node %u\n", node);
		break;

	case network::CID_NOACTION:
		(void) fprintf(out, "CID_NOACTION\n");
		break;

	default:
		(void) fprintf(out, "unexpected action from sl_input\n");
		return (-1);
	}

	return (0);
}
