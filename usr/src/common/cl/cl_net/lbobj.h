/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _LBOBJ_H
#define	_LBOBJ_H

#pragma ident	"@(#)lbobj.h	1.37	08/05/20 SMI"

#include <h/network.h>
#include <cl_net/netlib.h>
#include <cl_net/ma_service.h>
#include <sys/list_def.h>

class ma_servicegrp;

// Set a maximum value for weights; mainly for debugging purposes
#define	TOOBIG ((unsigned)100000)

// This class provides the interface between the ma_servicegrp and the
// load balancer.
class lbobj {
public:

	lbobj();
	virtual	~lbobj();

	// The ma_service is done with the lbobj.
	// When all services are done with the lbobj, the lbobj is destroyed.
	void release_lbobj();

	// The factory is called to create a lbobj of the proper type.
	// If there is an error, the factory will return NULL.
	// lb_policy specifies the desired policy.
	// On the secondary, primary_lb references the primary lb.
	static lbobj *lb_factory(class ma_servicegrp *serviceptr, const
		network::lb_specifier_t &lb_policy,
		network::lbobj_i_ptr primary_lb);

	// The HA functions add_secondary_all, freeze_primary_all, and
	// unfreeze_primary_all act on all the load balancers, calling
	// add_secondary, freeze_primary, and unfreeze_primary on each
	// load balancer.

	// Dump the state to a new secondary
	static void add_secondary_all(network::ckpt_ptr ckpt, Environment &e);

	// Freeze the primaries
	static void freeze_primary_all(Environment &);

	// Unfreeze the primaries
	static void unfreeze_primary_all(Environment &);

	virtual void add_secondary(network::ckpt_ptr, Environment &) {}
	virtual void freeze_primary(Environment &) {}
	virtual void unfreeze_primary(Environment &) {}

	// Get the implementation from the CORBA reference
	static lbobj *get_impl(network::lbobj_i_ptr ref);

	// Internal implementation of get_distribution.
	// The CORBA implementation forwards the request to this.
	// This allows the code to be shared across implementations.
	// locked is true if the datalock is held.
	network::dist_info_t *get_distribution_ii(bool locked,
		CORBA::Environment &e);

	// Version of get_distribution_ii with the listlock held on entry.
	network::dist_info_t *get_distribution_ii_locked(CORBA::Environment &e);

	// Change the load to the specified distribution.
	void set_distribution_ii(const network::dist_info_t &dist,
		network::ckpt_ptr ckptp, network::lbobj_i_ptr this_lbobj,
		bool locked, Environment &_environment);

	// Internal get_objref call to get the lbobj_i reference.
	// This is needed because the implementation
	// inherits both from lbobj and network::lbobj_i
	virtual network::lbobj_i *get_objref_ii() = 0;

	// List of load balancers.
	typedef DList<lbobj> lbobj_list_t;

protected:

	// Global list of lbobjs.  This list is used for operations on all
	// the load balancers (add secondary, freeze, etc.)
	static lbobj_list_t	lblist;

	// Global lock to protect lblist.
	// If datalock is held, it must be held after listlock.
	static os::mutex_t	listlock;

	// ma_servicegrp associated with this lbobj.
	ma_servicegrp *servicegrp;

	// Lock for lbobj data structures
	os::mutex_t	datalock;

	// The current policy type.
	network::lb_specifier_t policy;

	// Access function to servicegrp and policy.
	void set_lb_info(ma_servicegrp *serviceptr,
	network::lb_specifier_t policy);
};

// "Weighted" load balancer

class weighted_lbobj : public lbobj, protected
	mc_replica_of<network::weighted_lbobj> {
public:
	// Primary constructor
	weighted_lbobj();
	// Secondary constructor
	weighted_lbobj(network::weighted_lbobj_ptr obj);

	virtual weighted_lbobj::~weighted_lbobj();

	// Return the current weight table.
	network::dist_info_t *get_distribution(CORBA::Environment &e);

	// Returns the load balancer policy
	network::lb_specifier_t get_policy(Environment &);

	// Change the load to the specified distribution.
	void set_distribution(const network::dist_info_t &dist,
		Environment &_environment);

	// Set one weight in the load distribution
	void set_weight(sol::nodeid_t node, uint32_t weight,
		Environment &_environment);

	// Internal call to set one weight
	void set_weight1(nodeid_t node, uint32_t weight, bool locked,
		Environment &_environment);

	void _unreferenced(unref_t);

	// Get the object reference
	network::lbobj_i *get_objref_ii();

	void add_secondary(network::ckpt_ptr, Environment &);

	// New checkpoint accessor function.
	network::ckpt_ptr	get_checkpoint();
};

// User load balancer
class user_lbobj : public lbobj, protected
	mc_replica_of<network::user_policy> {
public:
	// Primary constructor
	user_lbobj();
	// Secondary constructor
	user_lbobj(network::user_policy_ptr obj);

	// Return the current weight table.
	network::dist_info_t *get_distribution(CORBA::Environment &e);

	// Returns the load balancer policy
	network::lb_specifier_t get_policy(Environment &);

	// Change the load to the specified distribution.
	void set_distribution(const network::dist_info_t &dist,
		Environment &_environment);

	// Set one weight in the load distribution
	void set_weight(sol::nodeid_t node, uint32_t weight,
		Environment &_environment);

	// Link in the user module.
	void register_user_policy(network::lbobj_i_ptr policy,
		Environment &_environment);

	// Internal call to link in the user module.
	void register_user_policy_int(network::lbobj_i_ptr policy,
		bool locked);

	// Get the user module.
	network::lbobj_i_ptr get_user_policy(Environment &_environment);

	// Get the object reference
	network::lbobj_i *get_objref_ii();

	void _unreferenced(unref_t);

	void add_secondary(network::ckpt_ptr, Environment &);

	// New checkpoint accessor function.
	network::ckpt_ptr	get_checkpoint();

private:
	network::lbobj_i_var user_policy;
};

//
// Transaction state objects for HA
//
class lbobj_set_weight_state : public transaction_state {
public:
	lbobj_set_weight_state() {}

	~lbobj_set_weight_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};


#endif	/* _LBOBJ_H */
