#! /bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)dispatch.sh	1.3	08/05/20 SMI"

# dispatch - script for testing packet dispatch for a scalable
# service.

script=$0

if [ "$CODEMGR_WS" = "" ]; then
	echo $script: CODEMGR_WS must be set 1>&2
	exit 1
fi

# If you're looking at the .sh version of this script you'll see a
# string after DIR_SFX with the word replace in it.  The replace string
# will be updated with $INSTALL_DIR when the script is created by make
# (see Makefile.unode for more).  If you're looking at the created
# version you'll see either nothing after the equals sign or sparcv9/
# depending on whether it is the 32-bit or 64-bit version.

DIR_SFX=replace-with-install_dir

if [ "$DIR_SFX" = "" ]; then
	UNODE_BIN=$CODEMGR_WS/proto_unode/bin/
else
	UNODE_BIN=$CODEMGR_WS/proto_unode/bin/$DIR_SFX
fi

UNODE="${UNODE_BIN}unode"
UNODE_LOAD="${UNODE_BIN}unode_load"
UNODE_CLUSTER="${UNODE_BIN}unode_cluster"

progs="$UNODE $UNODE_LOAD $UNODE_CLUSTER"

for i in $progs; do
	if [ ! -x $i ]; then
		echo $script: $i not executable 1>&2
		exit 1
	fi
done

# Run a command saving its output and checking exit value.  If
# the command fails, print a message, show the output, and exit
# with an error.

run()
{
	if [ ! -d $tdir ]; then
		mkdir $tdir
	fi
	$* > $tdir/cmd.out 2>&1
	ret=$?
	if [ $ret -ne 0 ]; then
		echo $script: command \"$*\" failed with return value $ret 1>&2
		echo and output: 1>&2
		cat $tdir/cmd.out 1>&2
		err_exit
	fi
}

# Run a unode_load command saving the command output, the unode entry
# point output, and checking exit value.

run_ul()
{
	if [ ! -d $tdir ]; then
		mkdir $tdir
	fi
	$UNODE_LOAD $clname $* -o $tdir/ep.out > $tdir/cmd.out 2>&1
	ret=$?
	if [ $ret -ne 0 ]; then
		echo $script: command \"$*\" failed with return value $ret 1>&2
		echo and output: 1>&2
		cat $tdir/cmd.out 1>&2
		err_exit
	fi
}

# Clean up.  We use pkill instead of unode_kill because we just want
# to get rid of all the unode processes.  unode_kill would try to
# have the processes shut down the ORB and exit by themselves, which
# would trigger other processes detecting node failure, blah, blah, blah.

cleanup()
{
	pkill -xf "unode $clname .*"
	rm -rf $tdir
	exit $exval
}

# Exit with an error.  Print a note that the directory used by the
# unode processes is being left behind (for debugging), reset the
# exit trap handler, since otherwise it would remove the /tmp
# directory, and exit with 1.

err_exit()
{
	echo $tdir left behind 1>&2
	trap EXIT
	exit 1
}

clname=net_test.$$
tdir=/tmp/$clname
rm -rf $tdir

# Set up a trap handler to be called when the scripts exits or
# receives certain signals.  The cleanup routine exits with exval,
# so set it to 1 so ending with ctrl-C will exit with 1.  exval
# is set to 0 only when all the checks pass.

exval=1
trap cleanup EXIT HUP INT QUIT TERM

# Start up a three node cluster and load the cl_net module
# on nodes 1 and 2.

run $UNODE_CLUSTER $clname 3
run_ul 1 cl_net init
run_ul 2 cl_net init

# Register a service by making unode calls similar to what the
# RGM does followed by calls that act like base Solaris calling
# the net hook functions.

echo registering service

run_ul 3 ssmw create_scal_srv_grp apache LB_WEIGHTED
run_ul 3 ssmw add_scal_service apache 10.13.13.13 80 TCP
run_ul 3 ssmw add_nodeid apache 1
run_ul 3 ssmw add_nodeid apache 2
run_ul 3 ssmw set_primary_gifnode 10.13.13.13 80 TCP 1
run_ul 3 lb_op set_distribution apache 1@1,1@2

run_ul 1 cl_net tcp_listen ipproto_tcp af_inet inaddr_any 80
run_ul 2 cl_net tcp_listen ipproto_tcp af_inet inaddr_any 80

# Test the distribution of connections by seeing which nodes 100
# packets will be sent to using a source address of 10.13.13.17
# and source ports from 10000 to 10099.

echo checking connection distribution \(takes a minute\)

node1=0
node2=0
i=0
while [ $i -lt 100 ]; do
	port=`expr 10000 + $i`
	run $UNODE_LOAD -x EPRET $clname 1 cl_net packet ipproto_tcp \
		10.13.13.13 80 10.13.13.17 $port -o $tdir/ep.out
	if [ "`cat $tdir/cmd.out`" = "EPRET -1" ]; then
		echo FAIL: packet entry point failed with: 1>&2
		cat $tdir/ep.out 1>&2
		err_exit
	fi

	# Check for expected output from the entry point.

	disp_str=`cat $tdir/ep.out`
	len=`/usr/bin/expr "$disp_str" : '.*'`
	match=`/usr/bin/expr "$disp_str" : 'CID_DISPATCH to node [1-9][0-9]*'`
	if [ $len -ne $match ]; then
		echo FAIL: return string \"$disp_str\" does not match \
			expected '"CID_DISPATCH to node [1-9][0-9]*"' 1>&2
		err_exit
	fi

	# Keep tally of how many packets to each node.

	node=`echo $disp_str | awk '{ print $4 }'`
	if [ $node -eq 1 ]; then
		node1=`expr $node1 + 1`
	elif [ $node -eq 2 ]; then
		node2=`expr $node2 + 1`
	else
		echo FAIL: unexpected dispatch to node $node 1>&2
		err_exit
	fi

	i=`expr $i + 1`
done

echo connections dispatched to node 1: $node1
echo connections dispatched to node 2: $node2

# We can't expect a 50/50 distribution of packets because cl_net
# dispatch isn't round robin.  For this test we assume that each node
# getting between 40 and 60 packets is acceptable.  (For these ports
# and address the test currently gets 43/57).

if [ $node1 -lt 40 -o $node1 -gt 60 ]; then
	echo FAIL: packet distribution to node 1 of $node1 packets not \
		within range of 40 to 60 1>&2
	err_exit
fi
if [ $node2 -lt 40 -o $node2 -gt 60 ]; then
	echo FAIL: packet distribution to node 2 of $node2 packets not \
		within range of 40 to 60 1>&2
	err_exit
fi

# Reset the exit value that will be used by cleanup, which
# will be called when the script reaches its end.

exval=0
echo PASS
