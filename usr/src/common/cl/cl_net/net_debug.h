/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _NET_DEBUG_H
#define	_NET_DEBUG_H

#pragma ident	"@(#)net_debug.h	1.18	08/05/20 SMI"

#include <sys/dbg_printf.h>

#if SOL_VERSION >= __s10
#ifdef _KERNEL
#define	SCTP_SUPPORT
#endif
#endif

#if !defined(DBGBUFS) || defined(NO_DBGBUFS)

//
// Not setting DBGBUFS or setting NO_DBGBUFS are two ways to indicate
// debug buffers are not wanted (see dbg_printf.h).  For those cases
// set the debug macros to nothing.
//
#define	NET_DBG(a)
#define	NET_DBG_FWD(a)
#define	NET_DBG_BIND(x)
#define	NET_DBG_RR(a)

#ifdef SCTP_SUPPORT
#define	NET_DBG_CT(x)
#endif

#else	// defined(DBGBUFS) && !defined(NO_DBGBUFS)

extern dbg_print_buf net_dbg;
extern dbg_print_buf net_dbg_fwd;
extern dbg_print_buf net_dbg_bind;
extern dbg_print_buf net_dbg_rr;
#ifdef SCTP_SUPPORT
extern dbg_print_buf net_dbg_ct;
#endif

// 1 (on), 0 (off) flag.  Debug buffer for forwarding list info ONLY.
extern uint32_t clnet_net_dbg_fwd;
// 1 (on), 0 (off) flag.  Debug buffer for binding table info ONLY.
extern uint32_t clnet_net_dbg_bind;
// 1 (on), 0 (off) flag.  Debug buffer for connection table info ONLY.
#ifdef SCTP_SUPPORT
extern uint32_t clnet_net_dbg_ct;
#endif
// 1 (on), 0 (off) flag.  Debug buffer for all other networking info.
extern uint32_t clnet_net_dbg;
// 1 (on), 0 (off) flag.  Debug buffer for Round Robin info only
extern uint32_t clnet_net_dbg_rr;

//
// The reason for these odd-looking macro definitions is so that
// they can be used as statements in C/C++ language constructs.
// The do-while(0) means that the statement will be executed
// exactly once, which is the effect we want.  Suppose we got rid of
// the do-while and retained the if statement and used the
// macro in the following statement:
//	if (foobar) NET_DBG(("")) ;
//	else x = x + 1;
// the compiler would mysteriously complain about an else clause
// not being part of any if.  When the macro body is substituted
// you get
//	if (foobar) if (clnet_net_dbg == 1) net_dbg.dbprintf a ;;
//	else x = x + 1;
// Note the two semi-colons.  The second semi-colon is an empty
// statement.  The "else" following in fact does not belong to
// any if.
//
// Tell lint that "while (0)" is ok for these macros:
//
//lint -emacro(717, NET_DBG, NET_DBG_FWD, NET_DBG_BIND, NET_DBG_CT)
//lint -emacro(717, NET_DBG_RR)
//
#define	NET_DBG(a)				\
	do					\
		if (clnet_net_dbg == 1)		\
			net_dbg.dbprintf a; 	\
	while (0)

#define	NET_DBG_FWD(a)				\
	do					\
		if (clnet_net_dbg_fwd == 1)	\
			net_dbg_fwd.dbprintf a;	\
	while (0)

#define	NET_DBG_BIND(a)					\
	do						\
		if (clnet_net_dbg_bind == 1)		\
			net_dbg_bind.dbprintf a;	\
	while (0)

#define	NET_DBG_RR(a)					\
	do						\
		if (clnet_net_dbg_rr == 1)		\
			net_dbg_rr.dbprintf a;	\
	while (0)

#ifdef SCTP_SUPPORT
#define	NET_DBG_CT(a)					\
	do						\
		if (clnet_net_dbg_ct == 1)		\
			net_dbg_ct.dbprintf a;	\
	while (0)
#endif  // SCTP_SUPPORT

#endif	// defined(DBGBUFS) && !defined(NO_DBGBUFS)

#endif	/* _NET_DEBUG_H */
