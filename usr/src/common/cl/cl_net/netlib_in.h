/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _NETLIB_IN_H
#define	_NETLIB_IN_H

#pragma ident	"@(#)netlib_in.h	1.37	08/05/20 SMI"

#include <cl_net/netlib.h>

//
// Used when some nodes in the cluster is not V6 ready. Then all arguments
// received from those nodes will have the first 12 bytes of an address
// filled with junk. Ignore those bytes but create a V4-mapped V6 address
// based on the last 4 (sane) bytes. Need to do this when clnet_v6_mode == 0.
//
inline void
upgrade_inaddr(const network::inaddr_t& addr)
{
	if (!clnet_v6_mode) {
		//
		// XXX: Have to write into the "const" reference. Not a
		// nice thing to do, but it's the cleanest in terms of
		// code impact and avoiding making copies of the addr.
		//
		IN6_IPADDR_TO_V4MAPPED(addr.v6[3],
		    (in6_addr_t *)&addr.v6);
	}
}

//
// True iff address is either V6-unspecified (all zero) or V4-mapped
// INADDR_ANY.
//
inline bool
addr6_is_inaddr_any(const network::inaddr_t &addr)
{
	return (IN6_IS_ADDR_V4MAPPED_ANY((in6_addr_t *)&addr) ||
	    IN6_IS_ADDR_UNSPECIFIED((in6_addr_t *)&addr));
}

inline bool
addr6_are_equal(const network::inaddr_t &addr1,
    const network::inaddr_t addr2)
{
	return (addr1.v6[3] == addr2.v6[3] &&
	    addr1.v6[2] == addr2.v6[2] &&
	    addr1.v6[1] == addr2.v6[1] &&
	    addr1.v6[0] == addr2.v6[0]);
}

inline
pdte_hash_t::pdte_hash_t()
{
	ph_seqnum = 1; ph_hashval = 0;
}

inline
pdte_hash_t::~pdte_hash_t()
{
}

inline uint_t
pdte_hash_t::get_hashval()
{
	return (ph_hashval);
}

inline void
pdte_hash_t::set_hashval(uint_t hashval)
{
	ph_hashval = hashval;
}

inline uint_t
pdte_hash_t::get_seqnum()
{
	return (ph_seqnum);
}

inline void
pdte_hash_t::set_seqnum(uint_t seqnum)
{
	ph_seqnum = seqnum;
}

inline unsigned int
pdt::pdt_gethashval(unsigned int index)
{
	return (pdte_hash[index].get_hashval());
}

inline void
pdt::pdt_sethashval(unsigned int index, unsigned int hashval)
{
	pdte_hash[index].set_hashval(hashval);
}

inline int
pdt::pdt_getbktseq(unsigned int index)
{
	return (pdte_hash[index].get_seqnum());
}

inline void
pdt::pdt_setbktseq(unsigned int index, int seqnum)
{
	pdte_hash[index].set_seqnum(seqnum);
}

inline void
pdt::pdt_setflag(unsigned int on_flags)
{
	pdt_flag = on_flags;
}

inline unsigned int
pdt::pdt_getflag()
{
	return (pdt_flag);
}

inline uint_t
pdt::pdt_getnhash()
{
	return (pdt_nhash);
}

inline
frwd_cid::~frwd_cid()
{
}

inline
frwd_info_t::frwd_info_t() :
	pernode_elem(this)
{
	fr_node = 0; fr_ncids = 0;
}

inline _DList::ListElem *
frwd_info_t::get_pernode_elem()
{
	return (&pernode_elem);
}

//
// This method is for debugging only
//
inline void
frwd_info_t::fr_dumpfrwd()
{
}

inline void
frwd_info_t::fr_setnode(nodeid_t node)
{
	fr_node = node;
}

inline nodeid_t
frwd_info_t::fr_getnode()
{
	return (fr_node);
}

inline int
frwd_info_t::fr_getncids()
{
	return (fr_ncids);
}

inline void
frwd_info_t::fr_incrcids()
{
	fr_ncids++;
}

inline
servicegrp::~servicegrp()
{
}

inline
serviceobj::~serviceobj()
{
	//
	// Better zero out the sap, since it is used as a key in the fast
	// service lookup code. Not doing so might leave bad code working,
	// hiding bugs for ages to come!
	//
	bzero(&sr_sap, sizeof (sr_sap));
}

inline bool
serviceobj::ip_equal(const network::service_sap_t &nsap)
{
	return (addr6_are_equal(nsap.laddr, sr_sap.laddr));
}

inline bool
serviceobj::equal(const network::service_sap_t &nsap)
{
	return ((nsap.protocol == sr_sap.protocol) && ip_equal(nsap) &&
	    (nsap.lport == sr_sap.lport));
}

//
// Does the IP address in the given sap "match" this service object?
// True iff one of the following:
//	- addresses are identical
//	- our address is V6-unspecified (all zero)
//	- our address is V4-mapped INADDR_ANY, and the given address is
//	  V4-mapped
//
inline bool
serviceobj::ip_matches(const network::service_sap_t &nsap)
{
	return (addr6_are_equal(nsap.laddr, sr_sap.laddr) ||
	    IN6_IS_ADDR_UNSPECIFIED((in6_addr_t *)&nsap.laddr) ||
	    (IN6_IS_ADDR_V4MAPPED((in6_addr_t *)&sr_sap.laddr) &&
	    IN6_IS_ADDR_V4MAPPED_ANY((in6_addr_t *)&nsap.laddr)));
}

inline bool
serviceobj::matches(const network::service_sap_t &nsap)
{
	return ((nsap.protocol == sr_sap.protocol) && ip_matches(nsap) &&
	    (nsap.lport == sr_sap.lport));
}

//
// size of PDT hash bucket
//
inline int
servicegrp::getnhash()
{
	return (sr_nhash);
}

//
// Load Balancing policy
//
inline network::lb_policy_t
servicegrp::getpolicy()
{
	return (sr_policy);
}

inline network::service_sap_t&
serviceobj::getssap()
{
	return (sr_sap);
}

inline void
serviceobj::set_gifnode(nodeid_t node)
{
	sr_gifnode = node;
}

inline nodeid_t
serviceobj::get_gifnode()
{
	return (sr_gifnode);
}

inline char *
servicegrp::get_group_name()
{
	return ((char *)sr_group.resource);


}

inline network::group_t&
servicegrp::getgroup()
{
	return (sr_group);
}
inline
instanceobj::instanceobj(nodeid_t node) : _DList::ListElem(this)
{
	ins_node = node;
}

inline nodeid_t
instanceobj::get_instanceid()
{
	return (ins_node);
}

inline bool
instanceobj::equal(nodeid_t node)
{
	return (ins_node == node);
}

#endif	/* _NETLIB_IN_H */
