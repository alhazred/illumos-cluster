/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _PERF_POLICY_H
#define	_PERF_POLICY_H

#pragma ident	"@(#)perf_policy.h	1.23	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <cl_net/lbobj.h>

class perf_policy : public lbobj,
	protected mc_replica_of<network::perf_mon_policy> {
public:
	// Primary constructor
	perf_policy(bool &okay);
	// Secondary constructor
	perf_policy(bool &okay, network::perf_mon_policy_ptr lb);

	~perf_policy();

	// IDL methods

	// Return the current weight table.
	network::dist_info_t *get_distribution(CORBA::Environment &e);

	// Returns the load balancer policy
	network::lb_specifier_t get_policy(Environment &);

	// Get the object reference
	network::lbobj_i *get_objref_ii();

	// Modify the load-balancer parameters
	void set_parameters(const perf_mon_parameters_t &parameters,
		Environment &_environment);

	// Internal call to set parameters, with lock held already or not
	void set_parameters_int(const perf_mon_parameters_t &parameters,
		bool locked);

	// Get the load-balancer parameters.
	void get_parameters(perf_mon_parameters_t &result,
		Environment &_environment);

	void set_load_info(nodeid_t nodeid, const node_metrics &metrics,
		Environment &);

	void freeze_primary_prepare(Environment &e);

	// Start the primary threads
	void unfreeze_primary(Environment &e);

	// Stop the primary threads
	void freeze_primary(Environment &e);

	// Checkpoint to a new secondary
	void add_secondary(network::ckpt_ptr ckpt, Environment &e);

	// Checkpoint accessor function.
	network::ckpt_ptr	get_checkpoint();

private:
	// Initialize the object
	void initialize(bool &okay);

	// Process the data and re-balance.
	void balance_load();

	// Update frequency in seconds
	unsigned int		notify_frequency;
	// Scale rate.  Weight is reduced by 1 for each scale_rate load points.
	unsigned int		scale_rate;
	// Minimum weight
	unsigned int		min_weight;
	// Maximum weight
	unsigned int		max_weight;
	// Width of quiet zone
	unsigned int		quiet_width;
	// Don't decrease load below the floor
	unsigned int		floor;

	// Current up/down information returned by get_distribution
	network::dist_info_t	*curr_info;

	// CPU loads * 1000
	uint32_t		node_loads[NODEID_MAX+1];

	// Load balancing thread entry point
	static void		lb_thread(void *arg);

	// Balancing thread method in the object's context.
	void			balance_thread();

	// Condvar for thread to signal object
	os::condvar_t		thread_done_cv;
	// Condvar for thread to sleep on
	os::condvar_t		sleep_cv;
	// Set true when the object is ready to be destroyed.
	bool			obj_done;
	// Set true when the thread acknowledges obj_done.
	bool			thread_done;

	// Balancing thread
#ifdef _KERNEL
	klwp_id_t		balance_tid;
#else
	thread_t		balance_tid;
#endif

	void _unreferenced(unref_t);
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _PERF_POLICY_H */
