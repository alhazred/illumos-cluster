//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sl_rr.cc	1.5	08/05/20 SMI"

//
// RR load balancing pdt module
//

#include <sys/types.h>
#include <cl_net/sl_pdt.h>
#include <h/network.h>
#include <sys/os.h>

static uint8_t	gcd(uint8_t a, uint8_t b);

sl_rr::sl_rr(const network::pdtinfo_t& pdtinfo)
{
	uint8_t	num_nodes = 0;
	uint8_t	weights[NODEID_MAX+1];

	const network::hashinfoseq_t& hashinfoseq = pdtinfo.hashinfoseq;
	uint_t nent = hashinfoseq.length();

	total_weight = 0;
	assign_vec = NULL;

	ASSERT(nent <= NODEID_MAX + 1);

	weights[0] = 0;
	for (uint_t i = 1; i < nent; i++) {
		const network::hashinfo_t& hinfo = hashinfoseq[i];
		weights[i] = (hinfo.new_hashval > 100? 100: hinfo.new_hashval);
		if (weights[i] != 0) {
			num_nodes = i;
		}
	}
	NET_DBG_RR(("sl_rr constructor num of nodes is %d\n", num_nodes));
	if (num_nodes > 0) {
		total_weight = find_gcd(num_nodes, weights);
		int	usage[NODEID_MAX+1];
		int	conn[NODEID_MAX+1];

		assign_vec = new (os::NO_SLEEP) uint8_t[total_weight];
		if (assign_vec == NULL) {
			NET_DBG(("Memory allocation failure\n"));
		}

		int	pos = 0, min;

		bzero(&conn[0], sizeof (conn));
		while (pos != total_weight) {
			min = 1;
			for (uint8_t indx = 1; indx <= num_nodes; indx++) {
				if (weights[indx] <= 0) {
					continue;
				}
				usage[indx] = conn[indx] / weights[indx];
				if (usage[indx] < usage[min]) {
					min = indx;
				} else {
					if (usage[indx] == usage[min]) {
						if (weights[indx] >
						    weights[min]) {
							min = indx;
						}
					}
				}
			}
			assign_vec[pos] = min;
			NET_DBG_RR(("assign vec pos %d, val %d\n",
			    pos, assign_vec[pos]));
			conn[min] = conn[min] + 100;
			pos++;
		}
	}
	ptr = 0;
}

int
sl_rr::find_gcd(uint8_t num_elem, uint8_t *weight)
{
	uint8_t	indx, gc;
	int	total_wght = 0;

	gc = gcd(weight[1], weight[2]);

	for (indx = 3; indx <= num_elem; indx++) {
		gc = gcd(gc, weight[indx]);
		if (gc == 1) {
			break;
		}
	}

	for (indx = 1; indx <= num_elem; indx++) {
		weight[indx] = weight[indx]/gc;
		total_wght += weight[indx];
	}
	return (total_wght);
}

static uint8_t
gcd(uint8_t a, uint8_t b)
{
	uint8_t	res, tmp;

	if (a == 0) {
		return (b);
	}
	if (b == 0) {
		return (a);
	}
	if (a < b) {
		tmp = a;
		a = b;
		b = tmp;
	}

	while ((res = a % b) != 0) {
		a = b;
		b = res;
	}

	return (b);
}

sl_rr::~sl_rr()
{
	if (assign_vec != NULL) {
		delete[]assign_vec;
	}
	assign_vec = NULL;
}

bool
sl_rr::invalid_weights()
{
	return (total_weight == 0? true: false);
}

uint_t
sl_rr::next()
{
	uint_t	ret;

	rrlock.lock();

	ASSERT(total_weight != 0);

	ret = assign_vec[ptr];

	if (++ptr >= total_weight)
		ptr = 0;

	rrlock.unlock();
	return (ret);
}
