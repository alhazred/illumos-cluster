/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MCNET_H
#define	_MCNET_H

#pragma ident	"@(#)mcnet.h	1.3	08/05/20 SMI"

#include <orb/object/adapter.h>
#include <sys/vm_util.h>

#define	CL_NET_VP_NAME		"cl_net"
#define	CL_NET_VP_V6_MAJ	1
#define	CL_NET_VP_V6_MIN	1

class cl_net_upgrade_callback :
    public McServerof<version_manager::upgrade_callback>
{
public:

	void _unreferenced(unref_t unref);

	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &);
};

#endif	/* _MCNET_H */
