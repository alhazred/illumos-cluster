/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CONN_TABLE_IN_H
#define	_CONN_TABLE_IN_H

#pragma ident	"@(#)conn_table_in.h	1.3	08/05/20 SMI"

#include <cl_net/netlib.h>

inline
conn_entry_t::conn_entry_t(cl_sctp_handle_t handle, sa_family_t addr_family,
    network::service_sap_t& ssap, uint8_t nfaddrs,
    uint8_t *faddrp, in_port_t fport) : _DList::ListElem(this)
{
	cn_handle = handle;
	cn_addr_family = addr_family;
	cn_ssap = ssap;
	cn_nfaddrs = nfaddrs;
	cn_faddrp = faddrp;
	cn_fport = fport;
};

#endif	/* _CONN_TABLE_IN_H */
