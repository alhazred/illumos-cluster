/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _BIND_TABLE_IN_H
#define	_BIND_TABLE_IN_H

#pragma ident	"@(#)bind_table_in.h	1.7	08/05/20 SMI"

#include <cl_net/netlib.h>

inline
gif_bind_elem_t::gif_bind_elem_t(network::inaddr_t& faddr, nodeid_t node) :
	_DList::ListElem(this)
{
	bi_faddr = faddr;
	bi_nodeid = node;
	bi_state = network::BI_UNINITIALISED;
}

inline
bind_event::bind_event(bind_elem_t *e)
{
	elem = e;
	bt = NULL;
	elem_index = 0;
}

inline
bind_elem_t::bind_elem_t(network::inaddr_t& f, nodeid_t n) :
    _DList::ListElem(this),
    bi_timer(this), bi_event(this)
{
	bi_faddr = f;
	bi_nodeid = n;
	bi_count = 0;
	bi_udp_usage = 0;
	bi_interested = 0;
	bi_state = network::BI_CONNECTED;

	bzero(&bi_ssap, sizeof (bi_ssap));
}

inline
bind_elem_t::~bind_elem_t()
{
	//
	// Noop.
	//
}

inline void
proxy_bind_table_t::set_gifnodes(nodeid_array_t gnodes)
{
	bind_gifnodes = gnodes;
}

inline int
proxy_bind_table_t::get_timeout()
{
	return (bind_timeout);
}

inline
closeconn_timer::closeconn_timer(bind_elem_t *e) : _DList::ListElem(this)
{
	elem = e;
	elem_index = 0;
	timestamp = 0;
}

#endif	/* _BIND_TABLE_IN_H */
