//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ma_pdt.cc	1.69	08/05/20 SMI"

//
// ma_pdt - Master PDT maintained in the PDT server
//

#include <h/network.h>
#include <sys/os.h>

#include <cl_net/netlib.h>
#include <cl_net/ma_service.h>
#include <cl_net/ma_pdt.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/netlib.h>
#include <nslib/ns.h>

#include <sys/mc_probe.h>

#include <netinet/in.h>

ma_pdt::ma_pdt(uint_t nhashent) : pdt(nhashent)
{
	MC_PROBE_1(ma_pdt, "mc net pdt", "",
		tnf_uint, nhashent, nhashent);
}

ma_pdt::~ma_pdt() {
}

unsigned int
ma_pdt::dump_mapdt()
{
	(void) dump_pdtinfo((pdt *)this);

	return (0);
}

//
// This function assumes that the caller has the instance_list lock
// held so that new instances are not added or old ones deleted.
//
int
ma_pdt::ma_new_gifnode(ma_servicegrp *srgrp, nodeid_t gifnode,
    const network::service_sap_t& ssap)
{
	uint_t frwd_err = 0;
	uint_t bind_err = 0;
	uint_t bind_cnt = 0;
	Environment e;
	network::hashinfo_t hashinfo;
	network::mcobj_ptr gifnode_mcobjref;

	NET_DBG_FWD(("ma_pdt::ma_new_gifnode()\n"));
	NET_DBG_FWD(("  gifnode = %d, sap = [%s, %s, %d]\n",
	    gifnode,
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	ASSERT(gifnode != 0);

	gifnode_mcobjref = pdts_impl_t::mcnet_node_cbref(gifnode);

	if (CORBA::is_nil(gifnode_mcobjref)) {
		//
		// Return failure. There is nothing that can be done
		// now.
		//
		return (-1);
	}
	//
	// Add the pernodelst to the gif_bind_table so that we
	// do not lose affinity.
	//

	if (srgrp->is_generic_affinity()) {
		gifnode_mcobjref->mc_add_pernode_lst(ssap, bind_cnt++, e);
		if (e.exception()) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) cl_net_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "Cannot setup binding entries from "
			    "affinity list for GIF node %d",
			    gifnode);
			bind_err++;
			e.clear();
		}
	}

	hashinfo.index = 0;
	hashinfo.old_hashval = hashinfo.new_hashval = 0;
	for (uint_t i = 1; i <= NODEID_MAX; i++) {
		network::fwd_t_var fwdp;
		network::bind_t_var bindp;

		//
		// If the node is not in the config list for this
		// service group, then don't collect forwarding
		// information from that node.
		//
		if (srgrp->ma_is_node_in_configlist(i) == 0) {
			continue;
		}

		// Get the obj ref to the mcobj for node "i."
		network::mcobj_ptr mcobjref =
			pdts_impl_t::mcnet_node_cbref(i);
		if (CORBA::is_nil(mcobjref)) {
			continue;
		}


		//
		// Install binding entries on the new GIF node
		//
		if (srgrp->is_strong_sticky()) {
			mcobjref->mc_get_bindinfo(ssap, gifnode, bindp, e);
			if (e.exception()) {
				//
				// SCMSGS
				// @explanation
				// Failed to maintain client affinity for some
				// sticky services running on the named server
				// node. Connections from existing clients for
				// those services might go to a different
				// server node as a result.
				// @user_action
				// If client affinity is a requirement for
				// some of the sticky services, say due to
				// data integrity reasons, these services must
				// be brought offline on the named node, or
				// the node itself should be restarted.
				//
				(void) cl_net_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "Can't retrieve binding entries from "
				    "node %d for GIF node %d",
				    i, gifnode);
				bind_err++;
				e.clear();
			} else {
				//
				// When building a new gifnode, pass in
				// bind_cnt == 0 the first time so that
				// all existing entries on the GIF are
				// deleted. A non-zero bind_cnt tells
				// the GIF to just add, but don't delete.
				//
				gifnode_mcobjref->mc_add_bindinfo(ssap,
				    *bindp, bind_cnt++, e);
				if (e.exception()) {
					//
					// SCMSGS
					// @explanation
					// Failed to maintain client affinity
					// for some sticky services running on
					// the named server node due to a
					// problem on the named GIF node.
					// Connections from existing clients
					// for those services might go to a
					// different server node as a result.
					// @user_action
					// If client affinity is a requirement
					// for some of the sticky services,
					// say due to data integrity reasons,
					// switchover all global interfaces
					// (GIFs) from the named GIF node to
					// some other node.
					//
					(void) cl_net_syslog_msgp->log(
					    SC_SYSLOG_WARNING, MESSAGE,
					    "Can't setup binding entries from "
					    "node %d for GIF node %d",
					    i, gifnode);
					bind_err++;
					e.clear();
				}
			}

			// Skip the forwarding machinery
			continue;
		}

		//
		// Collect forwarding information. This forwarding information
		// will be the forwarding information corresponding to all
		// buckets. Sorting will have to be done by the new GIN.
		//
		// XXX Need to verify if it is ok to ignore return value.
		//
		(void) mcobjref->mc_pdte_conninfo(ssap, gifnode, hashinfo,
			fwdp, pdt::PDT_COLLECT_ALL, e);

		//
		// Either the proxynode is dead or there aren't any connections
		// for this service on this node.
		//
		if ((e.exception()) || (fwdp->fw_ncid == 0)) {
			if (e.exception()) {
				MC_PROBE_1(ma_new_gifnode, "mc net mcfwd", "",
					tnf_uint, node, i);
				NET_DBG_FWD(("  ex node %d\n", i));
				e.clear();
				frwd_err++;
			} else {
				MC_PROBE_2(ma_new_gifnode, "mc net mcfwd", "",
					tnf_uint, node, i,
					tnf_int, ncid, fwdp->fw_ncid);
				NET_DBG_FWD(("  ex node %d, ncid =%x\n", i,
				    fwdp->fw_ncid));
			}
		} else {
			//
			// Flag with DESTROY indicates the GIF node
			// corresponding fwd list should be detroyed
			// if already exists before adding the new
			// entries. NOFORCE signifies entries will
			// not be added to the fwd list if
			// forwarded node matches with the hash bucket's
			// corresponding forwarded node
			//
			gifnode_mcobjref->mc_add_frwd_info(ssap, *fwdp,
			    network::FWD_ADD_DESTROY_NOFORCE, e);
			if (e.exception()) {
				e.clear();
				frwd_err++;
			}
		}
	}
	return (frwd_err + bind_err? -1: 0);
}

//
// Remove forwarding and binding info for this node.
//
void
ma_pdt::ma_clean_gifnode(nodeid_t node, nodeid_t gifnode,
    ma_servicegrp *srgrp, const network::service_sap_t& ssap)
{
	Environment e;
	network::mcobj_ptr gifnode_mcobjref;

	NET_DBG_FWD(("ma_pdt::ma_clean_gifnode()\n"));
	NET_DBG_FWD(("  gifnode = %d, sap = [%s, %s, %d]\n",
	    gifnode,
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	ASSERT(gifnode != 0);
	ASSERT(node != 0);

	gifnode_mcobjref = pdts_impl_t::mcnet_node_cbref(gifnode);

	if (CORBA::is_nil(gifnode_mcobjref))
		return;

	if (srgrp->is_strong_sticky()) {
		//
		// Tell GIF node to remove bindings for this node
		//
		gifnode_mcobjref->mc_clean_bindinfo(node, ssap, e);
		if (e.exception()) {
			NET_DBG_BIND(("  Gifnode %d might be dead\n",
			    gifnode));
			e.clear();
		}
		return;
	}

	gifnode_mcobjref->mc_clean_frwd_info(node, ssap, e);
	if (e.exception()) {
		NET_DBG_FWD(("  Gifnode %d might be dead\n", gifnode));
		e.clear();
	}
}

//
// Collect forwarding and binding info from only this node.
//
void
ma_pdt::ma_collect_only(nodeid_t node, nodeid_t gifnode, ma_servicegrp *srgrp,
    const network::service_sap_t& ssap)
{
	network::fwd_t_var fwdp;
	network::bind_t_var bindp;
	Environment e;
	network::hashinfo_t hashinfo;
	network::mcobj_ptr gifnode_mcobjref;

	NET_DBG_FWD(("ma_pdt::ma_collect_only()\n"));
	NET_DBG_FWD(("  gifnode = %d, sap = [%s, %s, %d]\n",
	    gifnode,
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	if (srgrp->is_rr_enabled()) {
		//
		// Gifnode for rr always have the forwarding entries.
		// No need to collect and distribute them.
		//
		return;
	}

	ASSERT(gifnode != 0);
	gifnode_mcobjref = pdts_impl_t::mcnet_node_cbref(gifnode);
	if (CORBA::is_nil(gifnode_mcobjref))
		return;

	network::mcobj_ptr mcobjref = pdts_impl_t::mcnet_node_cbref(node);
	if (CORBA::is_nil(mcobjref))
		return;


	if (srgrp->is_strong_sticky()) {
		NET_DBG_BIND(("ma_pdt::ma_collect_only() on node %d\n", node));
		NET_DBG_BIND(("  gifnode = %d, sap = [%s, %s, %d]\n",
		    gifnode,
		    network_lib.protocol_to_string(ssap.protocol),
		    network_lib.ipaddr_to_string(ssap.laddr),
		    ssap.lport));

		mcobjref->mc_get_bindinfo(ssap, gifnode, bindp, e);
		if (e.exception()) {
			(void) cl_net_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "Can't retrieve binding entries from "
			    "node %d for GIF node %d",
			    node, gifnode);
			NET_DBG_BIND(("  failed on node %d\n", node));
			e.clear();
		} else {
			gifnode_mcobjref->mc_add_bindinfo(ssap, *bindp, 1, e);
			if (e.exception()) {
				(void) cl_net_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "Can't setup binding entries from "
				    "node %d for GIF node %d",
				    node, gifnode);
				NET_DBG_BIND(("  failed on gifnode\n"));
				e.clear();
			}
		}

		// Skip forwarding machinery
		return;
	}

	hashinfo.index = 0;
	hashinfo.old_hashval = hashinfo.new_hashval = 0;

	(void) mcobjref->mc_pdte_conninfo(ssap, gifnode, hashinfo,
	    fwdp, pdt::PDT_COLLECT_ALL, e);

	if ((e.exception()) || (fwdp->fw_ncid == 0)) {
		if (e.exception()) {
			NET_DBG_FWD(("  node %d died while collecting "
			    "frwd info\n", node));
		} else {
			NET_DBG_FWD(("  node %d has no forwarding "
			    "info\n", node));
		}
		e.clear();
		return;
	}

	//
	// Flag with DESTROY indicates the GIF node corresponding
	// fwd list should be detroyed if already exists
	// before adding the new entries. NOFORCE signifies
	// entries will not be added to the fwd list if
	// forwarded node matches with the hash bucket's
	// corresponding forwarded node
	//
	gifnode_mcobjref->mc_add_frwd_info(ssap, *fwdp,
	    network::FWD_ADD_DESTROY_NOFORCE, e);

	if (e.exception()) {
		NET_DBG_FWD(("  Gifnode %d died \n", gifnode));
		e.clear();
		return;
	}
}

//
// Method to marshal the weight distribution.
// For RR only the weights are used to build the node
// distribution.
//

void
ma_pdt::marshal_rrinfo(const network::dist_info_t& dist,
    network::pdtinfo_t& pdtinfo)
{
	uint_t num_nodes = dist.nodes.length();

	pdtinfo.hashinfoseq.length(num_nodes);

	for (uint_t i = 0; i < num_nodes; i++) {
		network::hashinfo_t& hashinfo = pdtinfo.hashinfoseq[i];

		hashinfo.seqnum = 0;
		hashinfo.index = 0;
		if (dist.nodes[i] == network::instance_up) {
			hashinfo.old_hashval =
			    hashinfo.new_hashval = dist.weights[i];
		} else {
			hashinfo.old_hashval = hashinfo.new_hashval = 0;
		}
	}
}

//
// Initialize pdt on the specified node
//
int
ma_pdt::ma_init_pdt(nodeid_t gifnode, uint32_t nhash, bool rr_enabled,
network::group_t &group, const network::dist_info_t& dist, Environment &e)
{
	NET_DBG(("      ma_pdt::ma_init_pdt(gifnode=%d), group '%s'\n",
	    gifnode, group.resource));

	ASSERT(gifnode != 0);
	if (mcnet_node_array[gifnode] == NULL) {
		e.exception(new sol::op_e(ENOENT));
		return (0);
	}

	network::mcobj_ptr mcobjref = mcnet_node_array[gifnode]->get_cbref();
	if (CORBA::is_nil(mcobjref)) {
		e.exception(new sol::op_e(ENOENT));
		return (0);
	}
	ASSERT(!CORBA::is_nil(mcobjref));

	// Construct the canonical form of the PDT
	network::pdtinfo_t pdtinfo;
	pdtinfo.nhash = nhash;
	pdtinfo.group = group;

	// For RR only marshal the weights and not the PDT
	if (rr_enabled)
		marshal_rrinfo(dist, pdtinfo);
	else
		(void) marshal_pdtinfo(pdtinfo);

	// Now call the mcobj to tell it about the PDT
	mcobjref->mc_init_pdt(pdtinfo, e);
	if (e.exception() != NULL) {
		return (0);
	}

	return (1);
}

//
// Mark the initialization as complete on the gifnode.
//
void
ma_pdt::ma_init_complete(nodeid_t gifnode, const network::service_sap_t& ssap)
{
	Environment e;
	network::mcobj_ptr gifnode_mcobjref;

	NET_DBG(("ma_pdt::ma_init_complete(gifnode=%d)\n", gifnode));
	ASSERT(gifnode != 0);

	gifnode_mcobjref = pdts_impl_t::mcnet_node_cbref(gifnode);

	//
	// If gifnode has died, nothing much can be done.
	//
	if (CORBA::is_nil(gifnode_mcobjref)) {
		return;
	}

	gifnode_mcobjref->mc_init_complete(ssap, e);

	if (e.exception()) {
		e.clear();
		return;
	}
}


#ifdef DEBUG
void
ma_pdt::get_pdt_summary(uint_t *comp_pdt)
{
	uint_t nodeid;

	for (uint_t bktnum = 0; bktnum < pdt_nhash; bktnum++) {
		nodeid = pdte_hash[bktnum].get_hashval();

		// Increment bktnum counter for that nodeid.
		if (nodeid != 0) {
			comp_pdt[nodeid] = comp_pdt[nodeid] + 1;
		}
	}
}
#endif
