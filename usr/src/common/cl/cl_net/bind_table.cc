//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)bind_table.cc	1.20	08/08/27 SMI"

//
// Slave service module
//

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>

#ifdef _KERNEL
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/dlpi.h>
#endif

#include <sys/conf.h>
#include <sys/modctl.h>

#include <h/network.h>
#include <sys/os.h>

#include <h/network.h>
#include <sys/os.h>
#include <sys/threadpool.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/clusterproc.h>

#include <cl_net/net_debug.h>
#include <cl_net/netlib.h>
#include <cl_net/bind_table.h>
#include <cl_net/sl_pdt.h>
#include <cl_net/sl_service.h>
#include <cl_net/mcobj_impl.h>

#include <sys/mc_probe.h>

extern threadpool close_conn_threadpool;

static ulong_t create_elem_errs = 0;

//lint -emacro(795, bind_hash)
#define	bind_hash(faddr) (SUMOF(faddr) % bind_hashsize)

uint_t
bt_hashsize_in_range(uint_t hashsize)
{
	if (hashsize < MIN_BT_HASH_SZ)
		return (MIN_BT_HASH_SZ);
	if (hashsize > MAX_BT_HASH_SZ)
		return (MAX_BT_HASH_SZ);

	return (hashsize);
}

void
bind_event::execute()
{
	bt->event_handler(elem, elem_index);
}

const char *
gif_bind_elem_t::to_string()
{
	static char elem_str_buf[50];

	(void) sprintf(elem_str_buf, "<%s, %d>",
	    network_lib.ipaddr_to_string(bi_faddr),
	    bi_nodeid);

	return (elem_str_buf);
}

//
// Locking: gif_bind_table is part of sl_pdt, and it expects to be
// protected by the same mechanism as for sl_pdt, that is, by
// sl_servicegrplist_lock.
//

gif_bind_table_t::gif_bind_table_t(uint_t hashsize)
{
	NET_DBG_BIND(("gif_bind_table_t::gif_bind_table_t\n"));

	bind_hashsize = bt_hashsize_in_range(hashsize);
	bind_list = new gif_bind_list_t[bind_hashsize];
	bind_list_lock = new os::rwlock_t[bind_hashsize];
}

gif_bind_table_t::~gif_bind_table_t()
{
	NET_DBG_BIND(("gif_bind_table_t::~gif_bind_table_t\n"));
	//
	// Remove all entries first.
	//
	clean_all();

	delete [] bind_list;
	bind_list = NULL;
	delete [] bind_list_lock;
	bind_list_lock = NULL;
}

//
// Caller must hold lock
//
gif_bind_elem_t *
gif_bind_table_t::lookup_elem(const network::inaddr_t& faddr)
{
	gif_bind_elem_t *elem;
	uint_t index = bind_hash(faddr);

#ifdef _KERNEL
	ASSERT(bind_list_lock[index].lock_held());
#endif

	gif_bind_list_t::ListIterator li(&bind_list[index]);
	while ((elem = li.get_current()) != NULL) {
		if (addr6_are_equal(elem->bi_faddr, faddr)) {
			break;
		}
		li.advance();
	}
	return (elem);
}


nodeid_t
gif_bind_table_t::lookup(network::inaddr_t& faddr,
    network::bind_state_t& state)
{
	gif_bind_elem_t *elem;
	nodeid_t node = 0;
	uint_t index = bind_hash(faddr);

	bind_list_lock[index].rdlock();
	if ((elem = lookup_elem(faddr)) != NULL) {
		node = elem->bi_nodeid;
		state = elem->bi_state;
	}
	bind_list_lock[index].unlock();
	return (node);
}

//
// Do not call in interrupt context.
//
bool
gif_bind_table_t::create(network::inaddr_t& faddr, nodeid_t node)
{
	bool created = false;
	gif_bind_elem_t *elem = NULL;
	uint_t index = bind_hash(faddr);

	bind_list_lock[index].wrlock();
	if ((elem = lookup_elem(faddr)) == NULL) {
		//
		// Cannot be called in interrupt context.
		// The GIF node never creates elems in the interrupt context.
		//
		elem = new (os::NO_SLEEP) gif_bind_elem_t(faddr, node);
		if (elem != NULL) {
			bind_list[index].append(elem);
			created = true;
			NET_DBG_BIND(("created %s\n", elem->to_string()));
		}
	} else {
		elem->bi_nodeid = node;
		NET_DBG_BIND(("updated %s\n", elem->to_string()));
	}
	bind_list_lock[index].unlock();
	return (created);
}

//
// Set state of the specified binding entry.
// If BI_CLOSED, entry will be deleted.
// If BI_DEPRECATED, entry is marked deprecated.
// If BI_CONNECTED, entry is marked connected.
// See sl_pdt::pdt_input for how these states are used.
//
bool
gif_bind_table_t::setstate(const network::inaddr_t& faddr,
    network::bind_state_t state)
{
	gif_bind_elem_t *elem;
	uint_t index = bind_hash(faddr);

	bind_list_lock[index].wrlock();
	if ((elem = lookup_elem(faddr)) != NULL) {
		switch (state) {
		case network::BI_CONNECTED:
			NET_DBG_BIND(("reconnect %s\n", elem->to_string()));
			elem->bi_state = state;
			break;
		case network::BI_DEPRECATED:
			NET_DBG_BIND(("deprecate %s\n", elem->to_string()));
			elem->bi_state = state;
			break;
		case network::BI_CLOSED:
			NET_DBG_BIND(("remove %s\n", elem->to_string()));
			(void) bind_list[index].erase(elem);
			delete elem;
			break;
		default:
			CL_PANIC(!"Unknown binding state");
		}
	}
	bind_list_lock[index].unlock();
	return (elem != NULL? true: false);
}

void
gif_bind_table_t::remove_node(nodeid_t node)
{
	gif_bind_elem_t *elem = NULL;
	uint_t index;

	NET_DBG_BIND(("gif_bind_table_t::remove_node %d\n", node));

	for (index = 0; index < bind_hashsize; index++) {
		bind_list_lock[index].wrlock();

		gif_bind_list_t::ListIterator li(&bind_list[index]);
		while ((elem = li.get_current()) != NULL) {
			li.advance();
			if (elem->bi_nodeid == node) {
				NET_DBG_BIND(("  remove %s\n",
				    elem->to_string()));

				(void) bind_list[index].erase(elem);
				delete elem;
			}
		}

		bind_list_lock[index].unlock();
	}
}

//
// Remove all elements. ma_new_gifnode() builds a new gif node by first
// removing all entries using this method.
//
void
gif_bind_table_t::clean_all()
{
	gif_bind_elem_t *elem = NULL;
	uint_t index = 0;

	NET_DBG_BIND(("gif_bind_table_t::clean_all\n"));

	for (index = 0; index < bind_hashsize; index++) {
		bind_list_lock[index].wrlock();
		while ((elem = bind_list[index].reapfirst()) != NULL) {
			NET_DBG_BIND(("  remove %s\n", elem->to_string()));
			delete elem;
		}
		bind_list_lock[index].unlock();
	}
}


void
gif_bind_table_t::gns_elems(network::gns_bindseq_t *bindp)
{
	gif_bind_elem_t *elem = NULL;
	uint_t i, count = 0;
	uint_t index;

	//
	// Need to hold all locks so that the number of elements can't
	// change, which if allowed, could overflow the buffer we allocate
	// based on the number of elements.
	//


	for (index = 0; index < bind_hashsize; index++) {
		bind_list_lock[index].rdlock();

		gif_bind_list_t::ListIterator li(&bind_list[index]);
		while ((elem = li.get_current()) != NULL) {
			count++;
			li.advance();
		}
	}
	bindp->length(count);

	i = 0;
	for (index = 0; index < bind_hashsize; index++) {
		gif_bind_list_t::ListIterator li(&bind_list[index]);
		while ((elem = li.get_current()) != NULL) {
			(*bindp)[i].gb_faddr = elem->bi_faddr;
			(*bindp)[i].gb_nodeid = elem->bi_nodeid;
			(*bindp)[i].gb_state = elem->bi_state;
			//
			// The remaining fields are not used on the GIF
			// node. Just fill in something. The caller would
			// ignore these values anyway.
			//
			(*bindp)[i].gb_count = 0;

			i++;
			li.advance();
		}
		bind_list_lock[index].unlock();
	}
	ASSERT(i == count);
}


const char *
bind_elem_t::to_string()
{
	static char elem_str_buf[50];

	(void) sprintf(elem_str_buf, "<%s, %d>",
	    network_lib.ipaddr_to_string(bi_faddr),
	    bi_nodeid);

	return (elem_str_buf);
}

//
// Locking: proxy_bind_table_t is part of sl_servicegrp_t, and is
// protected the same way sl_servicegrp_t's are, i.e. with the
// sl_servicegrplist_lock.
//

proxy_bind_table_t::proxy_bind_table_t(uint_t hashsize,
    const network::sticky_config_t &st, bool is_rr)
{
	NET_DBG_BIND(("proxy_bind_table_t::proxy_bind_table_t\n"));

	bind_hashsize = bt_hashsize_in_range(hashsize);
	bind_list = new bind_list_t[bind_hashsize];
	bind_list_lock = new os::rwlock_t[bind_hashsize];

	my_nodeid = orb_conf::local_nodeid();
	bind_gifnodes = 0;
	bind_timeout = st.st_timeout;

	rr_enabled = is_rr;

	//
	// XXX timerman is not needed for timeout == 0 or no timeout. This
	// is a potential place for future memory conservation work.
	//
	timerman = new closeconn_timer_manager(this);

	udp_tid = NULL;
	generic_affinity = (st.st_flags & network::ST_GENERIC)?true:false;
	if (generic_affinity) {
		NET_DBG_BIND(("Add udp sticky timer\n"));
		add_udpsticky_timer();
	}
}

proxy_bind_table_t::~proxy_bind_table_t()
{
	bind_elem_t *elem = NULL;
	uint_t index;

	NET_DBG_BIND(("proxy_bind_table_t::~proxy_bind_table_t\n"));
	//
	// Can't delete any data until timerman is deleted, because
	// timeout_handler() can be invoked while timerman is being
	// deleted. timeout_handler() will run fine if we don't touch
	// or reset any data until later.
	//
	delete timerman;
	timerman = NULL;

	if (generic_affinity) {
		NET_DBG_BIND(("Remove UDP sticky timer\n"));
		remove_udpsticky_timer();
	}

	//
	// Outstanding events must be drained first, since the event
	// handler accesses the elements, which are being deleted.
	// This call blocks until all deferred tasks are executed.
	// This call will return soon, since we are holding the global
	// sl_servicegrplist_lock, meaning that no new tasks can be added
	// while we are waiting.
	//
	close_conn_threadpool.quiesce(threadpool::QEMPTY);

	for (index = 0; index < bind_hashsize; index++) {
		while ((elem = bind_list[index].reapfirst()) != NULL) {
			NET_DBG_BIND(("  remove %s\n", elem->to_string()));
			delete elem;
		}
	}

	delete [] bind_list;
	bind_list = NULL;
	delete [] bind_list_lock;
	bind_list_lock = NULL;
} //lint !e1740


void
proxy_bind_table_t::config_rr(bool is_rr)
{
	rr_enabled = is_rr;
}

//
// Caller must hold lock.
//
bind_elem_t *
proxy_bind_table_t::lookup_elem(network::inaddr_t& faddr)
{
	bind_elem_t *elem = NULL;
	uint_t index = bind_hash(faddr);

#ifdef _KERNEL
	ASSERT(bind_list_lock[index].lock_held());
#endif

	bind_list_t::ListIterator li(&bind_list[index]);
	while ((elem = li.get_current()) != NULL) {
		if (addr6_are_equal(elem->bi_faddr, faddr)) {
			break;
		}
		li.advance();
	}
	return (elem);
}

//
// Caller must hold lock.
//
bind_elem_t *
proxy_bind_table_t::create_elem(network::inaddr_t& faddr, nodeid_t node)
{
	bind_elem_t *elem;
	uint_t index = bind_hash(faddr);

#ifdef _KERNEL
	ASSERT(bind_list_lock[index].lock_held());
#endif

	if ((elem = new(os::NO_SLEEP) bind_elem_t(faddr, node)) == NULL) {
		//
		// Caller should syslog this memory problem. We also
		// don't want to NET_DBG, since we'd be crowding out
		// the whole buffer with out of memory messages, which
		// is useless, given that the caller will syslog.
		//
		return (NULL);
	}

	//
	// prepend() will not result in memory allocation, since the list
	// is an IntrList. The element has the needed storage.
	//
	bind_list[index].prepend(elem);
	return (elem);
}

//
// Caller must hold lock.
//
void
proxy_bind_table_t::remove_elem(bind_elem_t *elem, uint_t index)
{
#ifdef _KERNEL
	ASSERT(bind_list_lock[index].lock_held());
#endif

	(void) bind_list[index].erase(elem);
	delete elem;
}

//
// Change timeout period.
//
//	From	To	Action
//	------------------------------------------------------------
//	0	Any	No action required, since no timers are running
//			and no bindings are in the CLOSED state
//	>0	-1	Stop all running timers.
//		0	Stop all running timers. Remove CLOSED bindings.
//		Other	Tell timer manager about the change
//	-1	0	Remove CLOSED bindings
//		>0	Start timers for CLOSED bindings
//	------------------------------------------------------------
//
void
proxy_bind_table_t::config_timeout(int new_timeout)
{
	bind_elem_t *elem = NULL;
	uint_t index;
	int old_bind_timeout;

	if (new_timeout < BIND_TIMEOUT_NONE ||
	    new_timeout == bind_timeout)
		return;

	NET_DBG_BIND(("old timeout %d, new timeout %d, generic_aff %d\n",
	    bind_timeout, new_timeout, generic_affinity));
	old_bind_timeout = bind_timeout;
	bind_timeout = new_timeout;
	timerman->config_timeout((time_t)bind_timeout);

	if (old_bind_timeout == 0) {
		//
		// There should be no running timer. And all bindings that
		// are in CLOSED state should have been removed. Start
		// the UDP sticky timer if generic affinity is enabled
		// else does not require any action.
		//
		NET_DBG_BIND(("Add udp sticky timer\n"));
		if ((bind_timeout > 0) && (generic_affinity)) {
			add_udpsticky_timer();
		}
		return;
	}

	if (old_bind_timeout > 0) {
		if (generic_affinity) {
			NET_DBG_BIND(("Remove UDP sticky timer\n"));
			remove_udpsticky_timer();
		}
		if (bind_timeout == 0 || bind_timeout == BIND_TIMEOUT_NONE) {
			//
			// Remove all running timers.
			// Our own timeout handler will be called for all
			// running timers. For BIND_TIMEOUT_NONE, the
			// handler simply ignores the event.
			//
			timerman->purge_all();
		} else {
			if (generic_affinity) {
				NET_DBG_BIND(("Add udp sticky timer\n"));
				add_udpsticky_timer();
			}
		}
		return;
	}

	if (old_bind_timeout == BIND_TIMEOUT_NONE) {
		for (index = 0; index < bind_hashsize; index++) {
			bind_list_lock[index].wrlock();

			bind_list_t::ListIterator li(&bind_list[index]);
			while ((elem = li.get_current()) != NULL) {
				li.advance();

				if (elem->bi_state != network::BI_CLOSED)
					continue;

				if (bind_timeout > 0) {
					add_closeconn_timer(elem);
				} else if (bind_timeout == 0) {
					add_closeconn_event(elem);
				}
			}

			bind_list_lock[index].unlock();
		}
		if (generic_affinity && (bind_timeout > 0)) {
			NET_DBG_BIND(("Add udp sticky timer\n"));
			add_udpsticky_timer();
		}
	}
}

//
// Return a list of elements to any GIF node interested. We remember which
// node has run this call, and put them in the "interested" list, so that
// when the entries are removed later on (due to timeouts), these nodes
// are properly notified.
//
void
proxy_bind_table_t::get_elems(nodeid_t new_gifnode, network::bind_t *bindp)
{
	bind_elem_t *elem = NULL;
	uint_t i = 0, count = 0;
	uint_t index;

	NET_DBG_BIND(("proxy_bind_table_t::get_elems\n"));

	//
	// If some memory errors occurred, then the list we are returning
	// is not complete. That is, the GIF nodes might be getting a
	// partial list, and so some clients might lose stickiness.
	//
	CL_PANIC(create_elem_errs == 0);

	//
	// All entries in binding table are returned, even those in
	// BI_CLOSED state -- these entries haven't timed out yet,
	// and so the new GIF should know about them.
	//
	for (index = 0; index < bind_hashsize; index++) {
		bind_list_lock[index].rdlock();
		if (!bind_list[index].empty()) {
			count += bind_list[index].count();

			bindp->bn_faddrseq.length(count);
			bindp->bn_nfaddr = count;

			bind_list_t::ListIterator li(&bind_list[index]);
			while ((elem = li.get_current()) != NULL) {
				(bindp->bn_faddrseq)[i++] = elem->bi_faddr;
				//
				// Add new_gifnode in the "interest list"
				// for this elem, so that we know we have
				// to notify it when the timer expires.
				//
				SET_NODEID(elem->bi_interested, new_gifnode);
				NET_DBG_BIND((" %s\n", elem->to_string()));
				li.advance();
			}
		}
		bind_list_lock[index].unlock();
	}
	ASSERT(i == count);
}

void
proxy_bind_table_t::gns_elems(network::gns_bindseq_t *bindp)
{
	bind_elem_t *elem = NULL;
	uint_t i, count = 0;
	uint_t index;

	//
	// Need to hold all locks so that the number of elements can't
	// change, which if allowed, could overflow the buffer we allocate
	// based on the number of elements.
	//

	for (index = 0; index < bind_hashsize; index++) {
		bind_list_lock[index].rdlock();

		bind_list_t::ListIterator li(&bind_list[index]);
		while ((elem = li.get_current()) != NULL) {
			count++;
			li.advance();
		}
	}
	bindp->length(count);

	i = 0;
	for (index = 0; index < bind_hashsize; index++) {
		bind_list_t::ListIterator li(&bind_list[index]);
		while ((elem = li.get_current()) != NULL) {
			(*bindp)[i].gb_faddr = elem->bi_faddr;
			(*bindp)[i].gb_nodeid = elem->bi_nodeid;
			(*bindp)[i].gb_count = elem->bi_count;
			(*bindp)[i].gb_state = elem->bi_state;

			i++;
			li.advance();
		}
		bind_list_lock[index].unlock();
	}
	ASSERT(i == count);
}

//
// Handler for client connection open hook.
// Algorithm:
//	If element for client IP address doesn't exist yet, create one.
//	Else if element is in CLOSED state, then cancel the running timer.
//	Mark element in CONNECTED state.
// Return Values:
//	0 -- created new element and added successfully
//	1 -- enabled existing elemet successfully
//	-1 - failed to update connection element
int
proxy_bind_table_t::conn_open(network::cid_t& cid, ushort_t udp_usage)
{
	bind_elem_t *elem;
	network::inaddr_t& faddr = cid.ci_faddr;
	uint_t index = bind_hash(faddr);
	int r = 1;

	bind_list_lock[index].wrlock();
	if ((elem = lookup_elem(faddr)) == NULL) {
		if ((elem = create_elem(faddr, my_nodeid)) == NULL) {
			//
			// Don't need to panic, until we are asked
			// for a list of all bindings in get_elems().
			//
			if (!create_elem_errs++) {
				NET_DBG_BIND(("  can't create elem\n"));
				//
				// SCMSGS
				// @explanation
				// Client affinity state on the node has
				// become incomplete due to unexpected memory
				// shortage. New connections from some clients
				// that have existing connections with this
				// node might go to a different node as a
				// result.
				// @user_action
				// If client affinity is a requirement for
				// some of the sticky services, say due to
				// data integrity reasons, these services must
				// be brought offline on the node, or the node
				// itself should be restarted.
				//
				(void) cl_net_syslog_msgp->log(
				    SC_SYSLOG_WARNING,
				    MESSAGE,
				    "Can't allocate binding element");
			}
			bind_list_lock[index].unlock();
			return (-1);
		}
		r = 0;
	}

	if (elem->bi_state == network::BI_CLOSED) {
		if (bind_timeout > 0) {
			//
			// New connection established. Stop the timer.
			// If timeout expires at this time, it'll have to
			// wait until we release the lock, at which point
			// it'll see that state is no longer in CLOSED and
			// quietly ignores it. See timeout_handler().
			//
			timerman->remove_timeout(&elem->bi_timer);
		}
		r = 1;
	}

	elem->bi_state = network::BI_CONNECTED;
	//
	// For TCP, the fields are set when the connection closes. Since
	// for UDP we do not have the concept of close handle it here.
	//
	if (udp_usage) {
		elem->bi_udp_usage = udp_usage;
		elem->bi_ssap.protocol = cid.ci_protocol;
		elem->bi_ssap.laddr = cid.ci_laddr;
		elem->bi_ssap.lport = cid.ci_lport;
	} else {
		elem->bi_count++;
	}
	NET_DBG_BIND(("conn_open <%s, %s:%d>\n",
	    network_lib.ipaddr_to_string(elem->bi_faddr),
	    network_lib.ipaddr_to_string(elem->bi_ssap.laddr),
	    elem->bi_ssap.lport));

	bind_list_lock[index].unlock();
	return (r);
}

//
// Handler for TCP connection close hook.
// Algorithm:
//	Update connection count.
//	If this is the last connection for the client, then
//		Mark element CLOSED
//		If timeout == 0, notify all interested GIFs.
//		If timeout > 0, start a timer for the element.
//
void
proxy_bind_table_t::conn_close(network::cid_t& cid)
{
	bind_elem_t *elem;
	network::inaddr_t& faddr = cid.ci_faddr;
	uint_t index = bind_hash(faddr);

	NET_DBG_BIND(("conn_close <%s, %s:%d>\n",
	    network_lib.ipaddr_to_string(cid.ci_faddr),
	    network_lib.ipaddr_to_string(cid.ci_laddr),
	    cid.ci_lport));
	bind_list_lock[index].wrlock();
	if ((elem = lookup_elem(faddr)) == NULL) {
		NET_DBG_BIND(("  no elem found\n"));
		bind_list_lock[index].unlock();
		return;
	}

	ASSERT(elem->bi_state == network::BI_CONNECTED);
	elem->bi_count--;

	//
	// Always remember what connection has closed. When we remove a
	// binding on GIF nodes, we pass the ssap part of it to the GIF
	// nodes so that they can identify the group quickly.
	//
	elem->bi_ssap.protocol = cid.ci_protocol;
	elem->bi_ssap.laddr = cid.ci_laddr;
	elem->bi_ssap.lport = cid.ci_lport;

	if ((elem->bi_count == 0) && (elem->bi_udp_usage == 0)) {
		elem->bi_state = network::BI_CLOSED;
		NET_DBG_BIND(("closed binding %s\n", elem->to_string()));

		if (bind_timeout > 0) {
			//
			// Start running the timer. We store cid in the
			// timer structure so that when timer expires, we
			// know which bind_list to lock before accessing
			// the element.
			//
			add_closeconn_timer(elem);
		} else if (bind_timeout == 0) {
			//
			// Timeout is zero. Tell GIF right away.
			// Create a closeconn event to notify the GIF.
			// Delete entry here, since processing of
			// the closeconn event does not involve the
			// entry anymore.
			//
			add_closeconn_event(elem);
		} else if (bind_timeout == BIND_TIMEOUT_NONE) {
			//
			// Entries remain on GIF till we die.
			// No need to tell the GIF about this close.
			// Don't remove entry either, since the new GIF
			// need to know about this entry after a failover.
			//
		} else {
			//
			// As a proxy node, we shouldn't be here.
			//
			ASSERT(0);
		}
	}
	bind_list_lock[index].unlock();
}

//
// Artificially trigger all timeouts. This is used when all listeners have
// died on this node, and we want all clients to be able to re-connect to
// a different node as soon as they closes all their connections. See
// sl_servicegrp_t::sl_local_deregistered().
//
void
proxy_bind_table_t::timeout_all()
{
	NET_DBG_BIND(("proxy_bind_table_t::timeout_all\n"));
	ASSERT(timerman != NULL);
	timerman->purge_all();
}


//
// Binding entry delete processing routine.
//
// We extract the set of interested GIF nodes and notify all of them about
// the entry removal in an atomic fashion. If a connection is established
// while we are in the middle of telling the GIF nodes about the removal,
// this routine ensures that none of them end up removing it, which is the
// correct behavior.
//
// Atomic removal from multiple GIF nodes is done in two phases:
//	1) Deprecate entry on all GIF nodes, so that they all drop any SYN
//	packets from the client.
//	2) Delete entry on all GIF nodes.
//
// Without the two-phase remove, it's possible that the binding is removed
// on some GIFs, but remain active on others. SYNs arriving at the GIFs
// with the entry already deleted would then be forwarded according to the
// PDT, whereas SYNs hitting the other GIFs would be forwarded with the
// binding table. Not a desirable senario.
//
// Note that the ACK in a 3-way handshake could be in transit and the
// connect() TCP callback is received after the GIF nodes are told to
// remove their bindings. A sleep (of some kind) could be used between the
// two phases to allow time for any packet in transit to trigger any TCP
// hook. We're not doing this though because:
//	1) The various remote calls in phase 1 are introducing some delay
// 	already. We don't want to add more sleep time.
//	2) In long-running tests, the problem doesn't seem to arise.
//
// Note that for optimization, the first phase is omitted for the last GIF
// node (since the second phase starts with the last GIF node). This also
// implies that in a 1-GIF setup, only the second phase is run.
//
void
proxy_bind_table_t::event_handler(bind_elem_t *elem, uint_t index)
{
	extern mcobj_impl_t *mcobjp;
	nodeid_array_t gifnodes, gifnodes_orig;
	network::inaddr_t faddr;
	network::service_sap_t ssap;
	network::bind_state_t target_state;
	Environment e;
	uint_t i;

	bind_list_lock[index].wrlock();

	NET_DBG_BIND(("proxy_bind_table_t::event_handler <%s, %s:%d>\n",
	    network_lib.ipaddr_to_string(elem->bi_faddr),
	    network_lib.ipaddr_to_string(elem->bi_ssap.laddr),
	    elem->bi_ssap.lport));

	if (elem->bi_state != network::BI_DEPRECATED) {
		NET_DBG_BIND(("  state != DEPRECATED: done\n"));
		//
		// elem has transitioned to a different state,
		// e.g. because a connection has come in right after the
		// deferred task is enqueued. Reset bt to indicate there
		// is no longer a deferred task pending.
		//
		elem->bi_event.bt = NULL;
		bind_list_lock[index].unlock();
		return;
	}

	//
	// Find out the gifnodes we need to contact to remove binding.
	// RR always create binding on gifnodes. So the bi_interested is
	// ignored.
	//

	gifnodes = bind_gifnodes;
	if (!rr_enabled) {
		gifnodes &= elem->bi_interested;
		if (gifnodes == 0) {
			NET_DBG_BIND((" gifnodes == 0: done\n"));
			remove_elem(elem, index);
			bind_list_lock[index].unlock();
			return;
		}
	}

	// Make copy of needed info, since lock will be dropped
	faddr = elem->bi_faddr;
	ssap = elem->bi_ssap;

	bind_list_lock[index].unlock();

	//
	// First phase: Deprecate the binding entry
	// Can't hold lock, since we're making a bunch of remove calls.
	//

	ASSERT(mcobjp != NULL);
	NET_DBG_BIND(("proxy_bind_table_t::mcobjp <%s, %s:%d>\n",
	    network_lib.ipaddr_to_string(elem->bi_faddr),
	    network_lib.ipaddr_to_string(elem->bi_ssap.laddr),
	    elem->bi_ssap.lport));

	gifnodes_orig = gifnodes;
	for (i = 1; i <= NODEID_MAX && !ISLAST_NODEID(gifnodes, i); i++) {
		if (!ISSET_NODEID(gifnodes, i))
			continue;

		if (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(i))) {
			network::mcobj_ptr gif = mcobjp->mc_node_to_mcobj(i);

			NET_DBG_BIND(("  DEPRECATE to node %d\n", i));
			gif->mc_setstate_binding(faddr, ssap, my_nodeid,
			    network::BI_DEPRECATED, e);

			//
			// GIF node failed. The donot_update exception
			// returned from GIF node indicates that the GIF
			// node has a pernodelst from the client that
			// has not been flushed to the proxy yet. So
			// move the client to CONNECTED state.
			//
			CORBA::Exception *ex;
			if ((ex = e.exception()) != NULL) {
				if (network::donot_update::_exnarrow(ex)) {
					e.clear();
					bind_list_lock[index].wrlock();
					elem->bi_state = network::BI_CONNECTED;
					bind_list_lock[i].unlock();
					break;
				}
				e.clear();
			}
		}

		//
		// unset nodeid bit so we can get out of the loop at the
		// earliest possible.
		//
		CLR_NODEID(gifnodes, i);
	}

	//
	// Check here. Point of no return.
	//

	bind_list_lock[index].wrlock();

	// If entry is no longer CLOSED, we need to undo the first phase.
	target_state = (elem->bi_state == network::BI_DEPRECATED?
	    network::BI_CLOSED:
	    network::BI_CONNECTED);

	if (target_state == network::BI_CLOSED) {
		remove_elem(elem, index);
	} else {
		//
		// State is back to CONNECTED. Need to keep elem around
		// instead of removing it.
		// Reset bt to enable future events.
		//

		elem->bi_event.bt = NULL;
	}

	bind_list_lock[index].unlock();

	//
	// Second phase. Either delete or reconnect the entry
	// Can't hold lock, since we're making a bunch of remote calls.
	//

	gifnodes = gifnodes_orig;
	for (; i > 0 && gifnodes != 0; i--) {
		if (!ISSET_NODEID(gifnodes, i))
			continue;

		if (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(i))) {
			network::mcobj_ptr gif = mcobjp->mc_node_to_mcobj(i);

			NET_DBG_BIND(("  %s to node %d\n",
			    (target_state == network::BI_CLOSED?
			    "CLOSE": "CONNECT"), i));
			gif->mc_setstate_binding(faddr, ssap, my_nodeid,
			    target_state, e);

			//
			// GIF node failed. Since bindings on it
			// are gone anyway, we can ignore any
			// exception here.
			//
			e.clear();
		}

		CLR_NODEID(gifnodes, i);
	}
}

//
// Timeout handler (called by closeconn_timer_manager) when an element has
// expired. Remove the element, and tell all interested GIF nodes.
//
void
proxy_bind_table_t::timeout_handler(closeconn_timer *tp)
{
	uint_t index;

	NET_DBG_BIND(("proxy_bind_table_t::timeout_handler\n"));

	if (bind_timeout == BIND_TIMEOUT_NONE) {
		//
		// timeout must have been changed since add_timeout() was
		// called. Since no timeout is set (BIND_TIMEOUT_NONE), we
		// are done.
		//
		return;
	}

	index = tp->elem_index;
	bind_list_lock[index].wrlock();
	if (tp->elem->bi_state == network::BI_CLOSED) {
		add_closeconn_event(tp->elem);
	}

	//
	// Reset timestamp to indicates we finish timeout processing. When
	// adding a timer, this value is ASSERT'ed 0 to make sure only one
	// outstanding timer per elem.
	//
	// Also, before elem is deleted by a deferred task, it can be
	// "re-claimed" if a connection comes in first. The elem can
	// therefore require another timer later when the connection
	// closes. At that time, timestamp must be 0.
	//
	tp->timestamp = 0;
	bind_list_lock[index].unlock();
}

//
// Internal routine to help create the deferred task that is used to
// notify GIF nodes about elements being removed.
//
void
proxy_bind_table_t::add_closeconn_event(bind_elem_t *elem)
{
	elem->bi_state = network::BI_DEPRECATED;
	//
	// We can enter BI_DEPRECATED multiple times, but there should
	// only be one outstanding deferred task, we have to use some
	// other variable to know if it's okay to create a deferred
	// task. bt is used by deferred task processing, so might as well
	// use it for this purpose.
	//
	if (elem->bi_event.bt == NULL) {
		//
		// Use defer task to notify the GIFs about the
		// removal. Need to defer it, since we should not make
		// remote invocations in interrupt context.
		//
		elem->bi_event.bt = this;
		elem->bi_event.elem_index = bind_hash(elem->bi_faddr);
		close_conn_threadpool.defer_processing(&elem->bi_event);
	}
}

void
proxy_bind_table_t::add_closeconn_timer(bind_elem_t *elem)
{
	elem->bi_timer.elem_index = bind_hash(elem->bi_faddr);
	timerman->add_timeout(&elem->bi_timer);
}
//
// Note on unode:
//
// There is currently no unode support for the timer management
// component of client affinity. Setting the sticky timeout period to
// a positive number would not work as expected, but to 0 or -1 should.
//

#ifndef _KERNEL
#define	ddi_get_time() (0)
#define	timeout(x, y, z) (NULL)
#define	untimeout(x) (0)
#define	delay(x) (void)sleep(1)
#endif

static void
closeconn_timer_manager_worker_dispatch(void *timerman)
{
	((closeconn_timer_manager *)timerman)->worker_thread();
}

//
// Helper C function to use in timeout(9F), so that we can re-establish a
// closeconn_timer_manager context.
//
void
closeconn_timeout_handler(void *timerman)
{
	((closeconn_timer_manager *)timerman)->timeout_handler();
}

//
// Helper C function to use in timeout(9F), so that we can establish the
// the usage of a bind element.
//
void
udp_closeconn_handler(void *arg)
{
	((proxy_bind_table_t *)arg)->udp_cleanup_handler();
}

void
proxy_bind_table_t::final_udp_cleanup_handler()
{
	network::gns_bindseq_t bindp;
	bind_elem_t *elem;
	network::inaddr_t faddr;
	uint_t  num_elem, index;

	gns_elems(&bindp);

	num_elem = bindp->length();

	//
	// If close event has been scheduled continue.
	//

	for (uint_t i = 0; i < num_elem; i++) {
		if ((bindp[i].gb_state == network::BI_DEPRECATED) ||
		    (bindp[i].gb_state == network::BI_CLOSED)) {
			continue;
		}
		faddr = bindp[i].gb_faddr;
		index = bind_hash(faddr);
		bind_list_lock[index].wrlock();
		if ((elem = lookup_elem(faddr)) == NULL) {
			bind_list_lock[index].unlock();
			continue;
		}
		if ((!elem->bi_count)) {
			elem->bi_state = network::BI_CLOSED;
			add_closeconn_timer(elem);
		} else {
			elem->bi_udp_usage = 0;
		}
		bind_list_lock[index].unlock();
	}
}
//
// Constructor for timer manager. Creates a worker thread.
// If worker thread can't be created, we keep going. Only panic when a
// timer is later added -- since without worker thread, we can't handle
// any timer. This "delayed panic" is needed, since we are created for all
// sticky timeout periods, but not all of them use us. Thus, we don't
// panic if we are not used.
//
closeconn_timer_manager::closeconn_timer_manager(proxy_bind_table_t *bt) :
    bindtab(bt), min_sleep(WORKER_MIN_SLEEP), timeout_period(0),
    worker_flags(0), tid(0), tid_expire(0)
{

#ifdef _KERNEL
	if (clnewlwp(
	    (void (*)(void *))closeconn_timer_manager_worker_dispatch,
	    this, MINCLSYSPRI, NULL, &worker_id) != 0) {
#else
	if (clnewlwp(closeconn_timer_manager_worker_dispatch,
	    this, 0, NULL, &worker_id) != 0) {
#endif
		NET_DBG_BIND(("worker thread create failed\n"));
		//
		// SCMSGS
		// @explanation
		// Failed to create a crucial kernel thread for client
		// affinity processing on the node.
		// @user_action
		// If client affinity is a requirement for some of the sticky
		// services, say due to data integrity reasons, the node
		// should be restarted.
		//
		(void) cl_net_syslog_msgp->log(
		    SC_SYSLOG_WARNING,
		    MESSAGE,
		    "Can't create kernel thread");
	} else {
		NET_DBG_BIND(("worker thread created\n"));
	}
}

closeconn_timer_manager::~closeconn_timer_manager()
{
	if (worker_id != NULL) {
		worker_lock.lock();
		worker_flags |= WORKER_EXIT;
		//
		// The signal can be lost if the worker thread is not
		// sleeping. But that's okay, since it will see that we
		// have asked it to exit (in worker_flags), and will then
		// exit.
		//
		worker_cv.signal();

		while (true) {
			if (!(worker_flags & WORKER_EXIT)) {
				//
				// By now, all outstanding timeouts are
				// cancelled, and we know that the worker
				// thread is on its way to exit.
				//
				worker_lock.unlock();
				break;
			}
			NET_DBG_BIND(("waiting on worker to exit\n"));
			worker_cv.wait(&worker_lock);
		}
		NET_DBG_BIND(("worker thread exited\n"));
	}

	//
	// Remove all timers from timerlist before it is destructed.
	// The timers themselves are deleted later as part of
	// bind_elem_t's destructor.
	//
	while (timerlist.reapfirst()) {
		// Nothing to do
	}

	bindtab = (proxy_bind_table_t *)0;
	tid = (timeout_id_t)NULL;
	worker_id = NULL;

} //lint !e1740

void
closeconn_timer_manager::config_timeout(time_t new_period)
{
	NET_DBG_BIND(("closeconn_timer_manager::config_timeout %d to %d\n",
	    timeout_period, new_period));

	if (new_period < 0)
		new_period = 0;

	worker_lock.lock();
	if (tid_expire != 0 && (new_period < timeout_period)) {
		//
		// A smaller timeout period means that the worker should
		// get up earlier. Signal it so that it can decide when to
		// get up next.
		//
		timeout_period = new_period;
		worker_flags |= WORKER_MORE_WORK;
		worker_cv.signal();
	} else {
		timeout_period = new_period;
	}
	worker_lock.unlock();
}

//
// Mark ''tp'' for expiration. We set the start time, and the worker
// thread will eventually expires the timer when enough time
// (timeout_period) has passed.
//
void
closeconn_timer_manager::add_timeout(closeconn_timer *tp)
{
	CL_PANIC(worker_id != NULL);

	worker_lock.lock();
	ASSERT(tp->timestamp == 0);
	tp->timestamp = ddi_get_time();
	//
	// Must do an append(), since timers are added in order of
	// expiration.
	//
	timerlist.append(tp);
	worker_flags |= WORKER_MORE_WORK;
	if (tid_expire == 0) {
		//
		// worker is planning to sleep forever. Must wake it up.
		// Otherwise, signal() is not needed. Worker thread will
		// wake up at some point and get to this new timeout.
		//
		worker_cv.signal();
	}
	worker_lock.unlock();
}

//
// Remove a timeout previously scheduled with add_timeout().
//
void
closeconn_timer_manager::remove_timeout(closeconn_timer *tp)
{
	worker_lock.lock();
	if (tp->timestamp != 0) {
		//
		// This is an active timer and so it should be part of the
		// list and we can remove it.
		//
		(void) timerlist.erase(tp);
		tp->timestamp = 0;
	}
	worker_lock.unlock();
}


//
// Schedule a kernel timeout with timeout(9F) to go off at time texpire.
// Assumes that there is no outstanding timeout already scheduled by us.
//
void
closeconn_timer_manager::schedule_timeout(time_t texpire)
{
	time_t curtime = ddi_get_time();
	time_t delta;

	worker_lock.lock();
	if (texpire > 0) {
		//
		// We should not be scheduling a timeout when one is
		// already scheduled.
		//
		if (tid != 0) {
			worker_lock.unlock();
			return;
		}

		if ((delta = texpire - curtime) < min_sleep) {
			//
			// Don't want to wake up too often, to avoid
			// context switching too frequently.
			//
			delta = min_sleep;
		}

		tid_expire = curtime + delta;
		tid = timeout(closeconn_timeout_handler, this,
		    (delta * drv_usectohz((ulong_t)1000000)));

		if (tid == 0) {
			//
			// SCMSGS
			// @explanation
			// Failed to maintain timeout state for client
			// affinity on the node.
			// @user_action
			// If client affinity is a requirement for some of the
			// sticky services, say due to data integrity reasons,
			// the node should be restarted.
			//
			(void) cl_net_syslog_msgp->log(
			    SC_SYSLOG_WARNING,
			    MESSAGE,
			    "Can't access kernel timeout facility");
		} else {
			NET_DBG_BIND(("schedule_timeout %x in %d secs\n",
			    tid, delta));
		}
	}
	worker_lock.unlock();
}

//
// Cancel any outstanding kernel timeout. A timeout that happens while
// this call is in progress should not be harmful.
//
void
closeconn_timer_manager::cancel_timeout()
{
	timeout_id_t old_tid;

	worker_lock.lock();
	old_tid = tid;
	tid = 0;
	tid_expire = 0;
	worker_lock.unlock();
	//
	// Must not hold lock here, since untimeout() will block until
	// some timeout in progress is done. If we hold a lock here that
	// the timeout handler needs, we could deadlock.
	//
	if (old_tid != 0) {
		NET_DBG_BIND(("untimeout %x\n", old_tid));
		(void) untimeout(old_tid);
	}
}

//
// Internal timeout handler for closeconn_timer_manager. We don't process
// the timeout in this routine, since we are in soft-interrupt context.
// Signal the worker to do the work.
//
void
closeconn_timer_manager::timeout_handler()
{
	worker_lock.lock();
	tid = 0;
	tid_expire = 0;
	worker_flags |= WORKER_TIMED_OUT;
	worker_cv.signal();
	worker_lock.unlock();
}

//
// Tell the worker to timeout all timers regardless of expiration.
// Since timeouts are delivered by a different thread (the worker),
// and this call is "asynchronous", the caller does not need to worry
// about deadlock.
//
void
closeconn_timer_manager::purge_all()
{
	worker_lock.lock();
	worker_flags |= WORKER_PURGE_ALL;
	worker_cv.signal();
	worker_lock.unlock();
}

//
// Internal timeout handler processing routine, run on the worker thread's
// context.
//
void
closeconn_timer_manager::process_timeouts()
{
	time_t curtime = ddi_get_time();
	closeconn_timer *tp;
	time_t next_expire = 0;

	NET_DBG_BIND(("  process_timeouts\n"));
	while (true) {
		worker_lock.lock();
		if (timerlist.empty()) {
			worker_lock.unlock();
			break;
		}
		//
		// After delivering a timeout, drop the lock and grab it
		// back, to avoid holding other threads (like
		// add_timeout()) up for too long.
		//
		timerlist.atfirst();
		tp = timerlist.get_current();
		if ((tp->timestamp + timeout_period) <= curtime) {
			//
			// timer has expired.
			//
			(void) timerlist.reapfirst();
			worker_lock.unlock();
			//
			// Deliver timeout w/o locks to avoid deadlock in
			// case the timeout_handler calls back into us.
			//
			bindtab->timeout_handler(tp);
			continue;
		}

		//
		// Here we are done with all expired timers. Find out when
		// the next one expires, and we are done for now.
		//
		next_expire = tp->timestamp + timeout_period;
		worker_lock.unlock();
		break;
	}

	if (next_expire > 0)
		schedule_timeout(next_expire);
}

//
// Deliver timeouts for all timers regardless of expiration.
//
void
closeconn_timer_manager::purge_all_timeouts()
{
	closeconn_timer *tp;

	NET_DBG_BIND(("  purge_all_timeouts\n"));

	while (true) {
		worker_lock.lock();
		if (timerlist.empty()) {
			worker_lock.unlock();
			break;
		}
		timerlist.atfirst();
		tp = timerlist.reapfirst();
		worker_lock.unlock();
		//
		// Deliver timeout w/o locks to avoid deadlock in
		// case the timeout_handler calls back into us.
		//
		bindtab->timeout_handler(tp);
	}
}

//
// Worker thread for closeconn_timer_manager. All methods result in waking
// this thread up to do the actual work, so that we're not in any
// interrupt context.
//
void
closeconn_timer_manager::worker_thread()
{
	while (true) {
		worker_lock.lock();
		while (worker_flags == 0)
			worker_cv.wait(&worker_lock);

		if (worker_flags & WORKER_EXIT) {
			NET_DBG_BIND(("worker on EXIT\n"));
			worker_lock.unlock();

			//
			// Stops all timers. This call won't return until
			// all timers are stopped, or if a timer
			// expiration is already in the way, until that
			// finishes.
			//
			cancel_timeout();

			//
			// We can only clear WORKER_EXIT *after* stopping
			// all timers. Or else, the destructor (which
			// waits on the flag to clear) could delete all
			// data when a timer expires.
			//
			worker_lock.lock();
			worker_flags &= ~WORKER_EXIT;
			worker_cv.signal();
			worker_lock.unlock();
			return;
		}
		if (worker_flags & (WORKER_TIMED_OUT | WORKER_MORE_WORK)) {
			NET_DBG_BIND(("worker on TIMED_OUT\n"));
			worker_flags &= ~(WORKER_TIMED_OUT | WORKER_MORE_WORK);
			worker_lock.unlock();
			process_timeouts();
			continue;
		}
		if (worker_flags & WORKER_PURGE_ALL) {
			NET_DBG_BIND(("worker on PURGE_ALL\n"));
			worker_flags &= ~WORKER_PURGE_ALL;
			worker_lock.unlock();
			purge_all_timeouts();
			continue;
		}
		ASSERT(0);
	}
}

// Starts the udp sticky timer.

void
proxy_bind_table_t::add_udpsticky_timer()
{
	udp_timeout_lock.lock();
	udp_tid = timeout(udp_closeconn_handler, this, (bind_timeout *
	    drv_usectohz((ulong_t)1000000)));
	udp_timeout_lock.unlock();
}

// Removes the timer previuosly scheduled by add_udpsticky_timer

void
proxy_bind_table_t::remove_udpsticky_timer()
{
	if (udp_tid != NULL) {
		udp_timeout_lock.lock();
		(void) untimeout(udp_tid);
		udp_timeout_lock.unlock();
		udp_tid = NULL;
	}
	final_udp_cleanup_handler();
}

//
// Handler function for generic affinity.
// Algorithm:
//	Updates the UDP usage field of the bind elements.
//	If there was no UDP usage of the element and no connections
//	from the client, mark the element as CLOSED and schedule
//	the timer to handle the event.

void
proxy_bind_table_t::udp_cleanup_handler()
{
	network::gns_bindseq_t bindp;
	bind_elem_t *elem;
	network::inaddr_t faddr;
	uint_t	num_elem, index;

	gns_elems(&bindp);

	num_elem = bindp->length();

	//
	// If there are existing connections from the client or the
	// close event has been scheduled continue.
	//

	for (uint_t i = 0; i < num_elem; i++) {
		if ((bindp[i].gb_count) ||
		    (bindp[i].gb_state == network::BI_DEPRECATED) ||
		    (bindp[i].gb_state == network::BI_CLOSED)) {
			continue;
		}
		faddr = bindp[i].gb_faddr;
		index = bind_hash(faddr);
		bind_list_lock[index].wrlock();
		if ((elem = lookup_elem(faddr)) == NULL) {
			bind_list_lock[index].unlock();
			continue;
		}
		if ((!elem->bi_count) && (!elem->bi_udp_usage)) {
			elem->bi_state = network::BI_CLOSED;
			add_closeconn_timer(elem);
		} else {
			elem->bi_udp_usage = 0;
		}
		bind_list_lock[index].unlock();
	}
	add_udpsticky_timer();

}

void
proxy_bind_table_t::set_generic_affinity(bool is_generic_aff)
{
	generic_affinity = is_generic_aff;
}
