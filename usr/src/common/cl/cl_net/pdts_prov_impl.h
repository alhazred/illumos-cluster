/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PDTS_PROV_IMPL_H
#define	_PDTS_PROV_IMPL_H

#pragma ident	"@(#)pdts_prov_impl.h	1.60	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <repl/service/replica_tmpl.h>
#include <cl_net/pdts_impl.h>

class pdts_impl_t;

class pdts_prov_impl_t : public repl_server<network::ckpt> {

public:
	// Constructor
	pdts_prov_impl_t(char *svc_id, const char *prov_id);

	// Destructor
	~pdts_prov_impl_t();

	// Assignment operator.  Not implemented.
	pdts_prov_impl_t& operator=(pdts_prov_impl_t& rhs);

	// Copy constructor.  Not implemented.
	pdts_prov_impl_t(pdts_prov_impl_t& s);

	//
	// IDL methods for checkpoint interface
	//

	//
	// The following set of checkpoint methods are used principally for
	// dumping state to a secondary from a frozen primary during the
	// "add_secondary() method on that primary.
	//
	void ckpt_create_pdts(network::PDTServer_ptr pdts,
			Environment &_environment);
	void ckpt_create_mcnet_node_array(Environment &_environment);
	void ckpt_create_mcnet_node_and_dump_state(network::mcnet_node_ptr serv,
		nodeid_t node,
		network::mcobj_ptr mcobj_ref, bool gif,
		Environment &_environment);
	void ckpt_create_ma_servicegrp(const network::group_t& gname,
		uint32_t nhash, network::lb_policy_t policy,
		network::lbobj_i_ptr lb,
		Environment &_environment);
	void ckpt_dump_ma_service_state(const network::group_t &group,
		const network::seqlong_t &inslist,
		int32_t ninst, nodeid_t gif_node,
		const network::dist_info_t &load_dist,
		const network::seqlong_t &configlist,
		int32_t nconfig_inst,
		Environment &_environment);
	void ckpt_dump_ma_serviceobj_state(
		const network::group_t		&group,
		const network::service_sap_t	&ssap,
		nodeid_t			gif_node,
		const network::seqlong_t	&inslist,
		Environment			&_environment);
	void ckpt_create_ma_pdt(const network::group_t &group,
		Environment &_environment);
	void ckpt_dump_ma_pdtinfo(const network::group_t &group,
		const network::pdtinfo_t &info,
		Environment &_environment);
	void ckpt_dump_ma_sticky_config(const network::group_t &group,
		const network::sticky_config_t &st,
		Environment &_environment);

	//
	// These routines are the remaining checkpoint routines.
	//
	void ckpt_attach_mcobj(nodeid_t node, network::mcobj_ptr mcobj_ref,
		network::mcnet_node_ptr mcnet_nodep,
		Environment &_environment);
	void ckpt_register_inst(const network::service_sap_t &ssap,
		nodeid_t node, Environment &_environment);
	void ckpt_deregister_inst(const network::service_sap_t &ssap,
		nodeid_t node, Environment &e);
	void ckpt_register_gifnode(nodeid_t node, Environment &_environment);
	void ckpt_set_primary_gifnode(const network::service_sap_t &ssap,
		nodeid_t node, Environment &_environment);
	void ckpt_attach_gifnode(const network::service_sap_t &ssap,
		nodeid_t node, Environment &_environment);
	void ckpt_set_ma_configlist(const network::group_t &gname,
		Environment &_environment);

	void ckpt_add_nodeid(const network::group_t &g_name,
	    nodeid_t node, Environment &_environment);

	void ckpt_remove_nodeid(const network::group_t &g_name,
	    nodeid_t node, Environment &_environment);

	void ckpt_add_scalable_service(const network::group_t &g_name,
		const network::service_sap_t &ssap,
		Environment &_environment);

	void ckpt_rem_scal_service(const network::group_t &gname,
		const network::service_sap_t &ssap, Environment &_environment);

	void ckpt_create_scal_srv_grp(const network::group_t &g_name,
	    network::lb_policy_t policykind,
	    network::lbobj_i_ptr primary_lb,
	    Environment &_environment);

	void ckpt_config_sticky_srv_grp(const network::group_t &g_name,
	    const network::sticky_config_t &st,
	    Environment &_environment);

	void ckpt_config_rr_srv_grp(const network::group_t &g_name,
	    const network::rr_config_t &rr,
	    Environment &_environment);

	void ckpt_delete_scal_srv_grp(const network::group_t &g_name,
		Environment &_environment);

	void ckpt_set_policy(network::lbobj_i_ptr lb,
		network::lb_specifier_t new_policy, Environment &_environment);

	void ckpt_set_distribution(network::lbobj_i_ptr lb,
		const network::dist_info_t &dist, Environment &_environment);
	void ckpt_set_weight(network::weighted_lbobj_ptr lb,
		nodeid_t node, uint32_t weight,
		Environment &_environment);

	void ckpt_set_perf_mon_parameters(network::perf_mon_policy_ptr lb,
		const network::perf_mon_policy::perf_mon_parameters_t &
		parameters, Environment &_environment);
	void ckpt_user_policy(network::user_policy_ptr lb,
		network::lbobj_i_ptr user_lb, Environment &_environment);

	//
	// IDL methods for replica::repl_prov methods.
	//
	//
	void become_primary(const replica::repl_name_seq &secondary_names,
				Environment &_environment);
	void become_secondary(Environment &_environment);
	void add_secondary(replica::checkpoint_ptr sec_chkpt,
				const char *secondary_name,
				Environment &_environment);
	void remove_secondary(const char *secondary_name,
				Environment &_environment);
	void freeze_primary_prepare(Environment &_environment);
	void freeze_primary(Environment &_environment);
	void unfreeze_primary(Environment &_environment);
	void become_spare(Environment &_environment);
	void become_active(Environment &_environment);
	void shutdown(Environment &_environment);
	CORBA::Object_ptr get_root_obj(Environment &_environment);

	//
	// Miscellaneous methods
	//
	bool create_service();
	pdts_impl_t *get_pdtsp();

	bool is_primary();

	network::ckpt_ptr get_checkpoint_network_ckpt();

private:
	//
	// This variable tells us if the pdts server object has been
	// created and completely initialized.
	//
	bool pdts_initialized;

	//
	// Need to keep track of whether this provider is the current
	// primary or not.  This information is needed in the
	// mcnet_node_impl_t object so that when the
	// unreferenced message is delivered upon
	// node failure the corresponding mcnet_node_impl_t
	// will clean things up differently depending on whether
	// the provider it's a part of is primary or secondary.
	//
	bool prov_is_primary;

	// Pointer to root replica PDT server object for this service.
	pdts_impl_t		*pdtsp;

	//
	// Create an object reference to prevent _unreferenced from being
	// delivered.
	//
	network::PDTServer_var	pdts_v;

	//
	// This is the proxy for the checkpoint interface.
	//
	network::ckpt_ptr	_ckpt_proxy;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _PDTS_PROV_IMPL_H */
