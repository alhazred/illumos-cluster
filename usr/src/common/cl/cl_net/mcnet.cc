//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mcnet.cc	1.95	08/05/20 SMI"

//
// mcnet Driver
//
// This file contains the driver entry points for the "cl_net"
// module. This module is modloaded by the shared IP resource init method in the
// RGM
//
// Unode notes: this file has entry points for module loading and
// streams that are only used by the kernel version of cl_net and
// so are surrounded by #ifdef _KERNEL.

#ifdef _KERNEL

#include <sys/types.h>
#include <sys/stream.h>
#include <sys/stropts.h>

#include <sys/errno.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/dlpi.h>

#include <sys/conf.h>
#include <sys/modctl.h>

#endif	// _KERNEL

#include <sys/vm_util.h>

#include <h/streams.h>
#include <h/network.h>
#include <cl_net/mcnet.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/netlib.h>
#include <cl_net/mcobj_impl.h>
#include <cl_net/muxq_impl.h>
#include <cl_net/errprint.h>

#include <nslib/ns.h>
#ifdef DEBUG
#include <sys/mc_probe.h>
#endif

extern int mc_inet_init(void);
extern void mc_inet_fini(void);
extern mcobj_impl_t *mcobjp;

//
// Global variables
//
pdts_prov_impl_t *mc_pdts_prov = NULL;

#define	pdtprovidernm "PDT_provider"

#define	CL_NET_TAG	"cl_net"
// Definition for cl_net's syslog

os::sc_syslog_msg *cl_net_syslog_msgp = new os::sc_syslog_msg(
	SCALABLE_SERVICE_TAG, CL_NET_TAG, NULL);

static void clnet_vm_init(void);
static int pdts_init(void);

#ifdef _KERNEL

extern "C" {

#define	MCNET_MODULE_NAME "mcnet"
char _depends_on[] =
	"misc/cl_runtime misc/cl_load misc/cl_orb misc/cl_haci "
	"misc/cl_quorum  misc/cl_comm drv/tcp drv/udp drv/ip misc/cl_dcs";

extern void _cplpl_init(void);
extern void _cplpl_fini(void);

#endif	// _KERNEL

static int mcnet_open(queue_t *, dev_t *, int, int, cred_t *);

#ifdef _KERNEL

static int mcnet_close(queue_t *, int, cred_t *);
static int mcnet_rput(queue_t *, mblk_t *);
static int mcnet_wput(queue_t *, mblk_t *);
static void mcnet_rsrv(queue_t *);

static struct module_info mcnet_info = {
	0xdeaf,			/* module id - random for now */
	MCNET_MODULE_NAME,	/* module name - mcnet */
	0,			/* min packet size */
	INFPSZ,			/* max packet size */
	64*1024,		/* hi-water mark */
	16*1024			/* lo-water mark */
};

static struct qinit mcnet_rint = {
	(int (*)(void))mcnet_rput,  /* put procedure */
	(int (*)(void))mcnet_rsrv, 	/* service procedure */
	(int (*)(void))mcnet_open,  /* open procedure */
	(int (*)(void))mcnet_close, /* close procedure */
	NULL,			/* qadmin procedure */
	&mcnet_info,		/* module info structure */
	NULL,			/* module statistics structure */
	NULL,			/* r/w proc. */
	NULL,			/* info. procedure. */
	0			/* uio type for struio(). */
};

static struct qinit mcnet_wint = {
	(int (*)(void))mcnet_wput,	/* put procedure */
	NULL, 			/* service procedure */
	NULL,			/* open procedure */
	NULL,			/* close procedure */
	NULL,			/* qadmin procedure */
	&mcnet_info,		/* module info structure */
	NULL,			/* module statistics structure */
	NULL,			/* r/w proc. */
	NULL,			/* info. procedure. */
	0			/* uio type for struio(). */
};

static struct streamtab mcnet_stream = {
	&mcnet_rint,		/* read queue init structure */
	&mcnet_wint,		/* write queue init structure */
	NULL,		/* mux read init structure */
	NULL		/* mux write init structure */
};

static struct fmodsw mcnet_fsw = {
	MCNET_MODULE_NAME,
	&mcnet_stream,
	D_MP,
};

/*
 * Module linkage information for the kernel.
 */

static struct modlstrmod modl_mcnet = {
	&mod_strmodops, /* Type of module.  This one is a Streams module */
	"Logical Network Device 'mcnet'",
	&mcnet_fsw	/* Module info */
};

static struct modlinkage modlinkage = {
	MODREV_1,
	{ (void *)&modl_mcnet, NULL, NULL, NULL }
};


int
_init(void)
{
	int error = 0;

	_cplpl_init();	// C++ initialization

	clnet_vm_init();

	if ((error = pdts_init()) != 0) {
		return (error);
	}

	//
	// Initialize all the mc net objects.
	//
	if ((error = mc_inet_init()) != 0) {
		//
		// can't find PDT server, abort
		//
		cmn_err(CE_WARN, "couldn't initialize PDT server(s), "
		    "aborting load of net module");
		mc_inet_fini();
		_cplpl_fini();
		return (error);
	}

	if ((error = mod_install(&modlinkage)) != 0) {
		mc_inet_fini();
		_cplpl_fini();
	}

	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}

int
_fini(void)
{
	//
	// We currently do not allow this module to be unloaded.
	//
	return (EBUSY);
}

#else	// !_KERNEL

// When unode_load loads the cl_net unode module it calls unode_init
// which is made look like _init() as much as possible to do similar
// things to what is done when the cl_net kernel module is loaded.
// The differences are we don't need to call _cplpl_init, _cplpl_fini
// or mod_install.  We also dummy up arguments to mcnet_open() and call
// it here to be like real cluster mode where after the kernel module
// is loaded, mcnet is pushed onto public network streams resulting in
// mcnet_open being called.

int
unode_init(void)
{
	int error = 0;

	if ((error = pdts_init()) != 0) {
		return (error);
	}

	//
	// Initialize all the mc net objects.
	//
	if ((error = mc_inet_init()) != 0) {
		//
		// can't find PDT server, abort
		//
		cmn_err(CE_WARN, "couldn't initialize PDT server(s), "
		    "aborting load of net module");
		mc_inet_fini();
		return (error);
	}

	// Set up arguments for mcnet_open only setting what
	// we know mcnet_open needs.

	queue_t rq;
	rq.q_ptr = NULL;
	error = mcnet_open(&rq, NULL, 0, MODOPEN, NULL);

	return (error);
}

#endif	// !_KERNEL

static int
mcnet_open(queue_t *rqp, dev_t *, int, int sflag, cred_t *)
{
	if (rqp->q_ptr) {
		//
		// This instance has already been opened.  Nothing to do.
		// Just return.
		//
		return (0);
	}

	if (sflag != MODOPEN) {
		return (ENXIO);
	}

	//
	// Our private data points to the muxq for this node.
	// set_muxq creates one if it doesn't already exist.
	//
	rqp->q_ptr = mcobjp->set_muxq();

#ifdef _KERNEL

	//
	// Turn our queue on ...
	//
	qprocson(rqp);

#endif	// _KERNEL

	return (0);
}

#ifdef _KERNEL

static int
mcnet_close(queue_t *rqp, int, cred_t *)
{
	//
	// Disable put and service procedures on this queue
	//
	qprocsoff(rqp);

	return (0);
}

//
// We would eliminate wput, but we need to provide a method for
// the STREAMS entry point ... so the simpler the better.
//
static int
mcnet_wput(queue_t *wqp, mblk_t *mp)
{
	if (mp->b_datap->db_type == M_FLUSH) {
		if (*mp->b_rptr & FLUSHW) {
			if (*mp->b_rptr & FLUSHBAND) {
				flushband(wqp, FLUSHDATA,
				    *(mp->b_rptr + 1));
			} else {
				flushq(wqp, FLUSHDATA);
			}
		}
	}
	putnext(wqp, mp);
	return (0);
}

static int
mcnet_rput(queue_t *rqp, mblk_t *mp)
{
	muxq_impl_t *muxq_ptr = (muxq_impl_t *)rqp->q_ptr;

	switch (mp->b_datap->db_type) {
		case M_DATA:
			//
			// q_ptr should never be null, since there
			// must be a muxq object created before
			// we're ever plumbed, but we shouldn't make
			// any assumptions.  If for some
			// reason it is null, something serious is
			// wrong and to avoid further damage we need
			// to bring the node down.
			//
			CL_PANIC(muxq_ptr != NULL);
			muxq_ptr->demux_data(rqp, mp);
			break;
		case M_FLUSH:
			if (*mp->b_rptr & FLUSHR) {
				if (*mp->b_rptr & FLUSHBAND) {
					flushband(rqp, FLUSHDATA,
					    *(mp->b_rptr + 1));
				} else {
					flushq(rqp, FLUSHDATA);
				}
			}
			putnext(rqp, mp);
			break;

		default:
			putnext(rqp, mp);
			break;
	}

	return (0);
}

// Read side service routine. This routine is used to handle
// only PCSIG messages which are put on the queue when the reassembly
// timer expires.

static void
mcnet_rsrv(queue_t *q)
{
	mblk_t  *mp;
	muxq_impl_t *muxq_ptr = (muxq_impl_t *)q->q_ptr;

	while ((mp = getq(q)) != NULL) {
		if (mp->b_datap->db_type == M_PCSIG) {
			CL_PANIC(muxq_ptr != NULL);
			muxq_ptr->frag_timer(q, mp);
		} else {
			// All other message types are handled by
			// the put procedure.
			ASSERT(0);
			putnext(q, mp);
		}
	}
}

}	// extern "C"

#endif	// _KERNEL

static int
pdts_init(void)
{
	Environment	e;

	//
	// sizeof(pdtprovidernm) takes care of the %s and null terminator.
	// The first 1 is for '.' and "sizeof(int) * 3 + 1 will hold
	// the longest decimal representation of an int on the theory
	// that a byte can be at most 3 characters in decimal so 3
	// times the number of bytes in an int plus 1 for a sign
	// character can holdest the longest int.
	//
	// The reason we go through all this complicated business is to
	// avoid a potential future problem if "pdtprovindernm" were
	// ever changed to be a longer string, and we forgot to change
	// provider_str.
	//
	char provider_str[sizeof (pdtprovidernm) + 1 + sizeof (int) * 3 + 1];

	//
	// Create the repl_server object (the provider) and register with
	// the HA Replica Manager (RM).  Assign to the global variable
	// mc_pdts_prov.
	//
	// Returns 0 if the provider object is created and successfully
	// registered with the Replica Manager (RM). Else returns 1.
	//
	// Create the repl_server object (the provider).
	// Name of provider is of the form: PDT_provider.nodeid
	//
	os::sprintf(provider_str, "%s.%d", pdtprovidernm,
	    orb_conf::local_nodeid());

	NET_DBG(("pdts_init: Create PDTS repl_server provider object '%s'\n",
	    provider_str));

	if (mc_pdts_prov == NULL) {
		mc_pdts_prov = new pdts_prov_impl_t(
		    (char *)network::PDTServer::NS_NAME,
		    (const char *)provider_str);
	}

	//
	// Register with the RM.  Note there are no service dependencies
	// (arg 2) and no provider dependencies (arg 3).
	//
	mc_pdts_prov->register_with_rm(1, NULL, NULL, true, e);
	if (e.exception()) {
		e.exception()->print_exception("register_with_rm");
		e.clear();
		cmn_err(CE_WARN, "pdts_init: "
		    "Failed to register PDTS provider with RM\n");
		//
		// The HA framework will delete the mc_pdts_prov object
		// if registration fails.
		//
		return (ESRCH);
	}

	NET_DBG(("Registered 'PDTService' with RM.\n"));

	return (0);
}


//
// Check out current version of cl_net and decide if we can start talking
// in V6 mode. If not, register with VM for an upgrade callback.
//
void
clnet_vm_init()
{
	version_manager::vp_version_t	vers;
	version_manager::vm_admin_var	vm_adm;
	Environment			e;

	vm_adm = vm_util::get_vm();
	if (CORBA::is_nil(vm_adm))
		CL_PANIC(!"cl_net: Could not get vm_admin object");

	vm_adm->get_running_version(CL_NET_VP_NAME, vers, e);
	if (e.exception()) {
		//
		// If we are running this code, we should get a running
		// version. But we want to allow the cl_net.vp file to
		// be removed and cause cl_net to run in 1.0 -- mostly
		// for rapid development and/or field support.
		//
		vers.major_num = 1;
		vers.minor_num = 0;
		e.exception()->print_exception("cl_net: "
		    "Failed get_running_version()");
		e.clear();
	}

	if (!(vers.major_num == 1 && vers.minor_num == 0)) {
		clnet_v6_mode = 1;
	} else {
		clnet_v6_mode = 0;
		//
		// Register with VM to get an upgrade callback once all
		// nodes are upgraded.
		//
		cl_net_upgrade_callback *cbp = new cl_net_upgrade_callback;
		version_manager::upgrade_callback_var cb_v = cbp->get_objref();

		//
		// No ordering requirment on cl_net callbacks, and so we
		// just set ucc to the vp name.
		//
		version_manager::ucc_seq_t ucc_seq(1, 1);
		ucc_seq[0].ucc_name = os::strdup(CL_NET_VP_NAME);
		ucc_seq[0].vp_name = os::strdup(CL_NET_VP_NAME);
		ucc_seq[0].freeze.length(1);
		ucc_seq[0].freeze[0] = os::strdup(network::PDTServer::NS_NAME);

		//
		// If cluster is running above or equal to 1.1, no
		// callback is needed and so none is registered.
		//
		version_manager::vp_version_t hi_vers;
		hi_vers.major_num = CL_NET_VP_V6_MAJ;
		hi_vers.minor_num = CL_NET_VP_V6_MIN;

		vm_adm->register_upgrade_callbacks(ucc_seq, cb_v,
		    hi_vers, vers, e);
		if (e.exception()) {
			e.exception()->print_exception("cl_net: "
			    "Failed register_upgrade_callbacks()");
			e.clear();
			cmn_err(CE_WARN, "cl_net: Unable to support IPv6");
		} else {
			NET_DBG(("register_upgrade_callbacks() succeeded\n"));
		}
	}
}

//
// Empty function to implement a pure virtual function.
// This object is deleted when VM has run the upgrade callback.
// No cleanup needed.
//
void
cl_net_upgrade_callback::_unreferenced(unref_t)
{
}

void
cl_net_upgrade_callback::do_callback(const char *,
    const version_manager::vp_version_t &new_version, Environment &)
{
	if (!(new_version.major_num == 1 && new_version.minor_num == 0)) {
		//
		// Running version != 1.0:
		//
		// Switch to talking V6. Since all nodes have upgraded by
		// now, we can safely assume the other nodes are only
		// communicating the V6 way now. That is, all V4 addresses
		// are V4-mapped when being passed as arguments around the
		// cluster.
		//
		// Locking not required. No config changes can be
		// happening, since the PDT server is frozen through the
		// duration of the callback. Other operations that are
		// ongoing with the assumption of clnet_v6_mode == 0 is
		// understood by all others anyway. Once those operations
		// complete, all arguments will be in V6 mode from now on.
		//
		clnet_v6_mode = 1;

		NET_DBG(("ipv6 enabled: cluster cl_net version %d.%d\n",
		    new_version.major_num, new_version.minor_num));
	}
}
