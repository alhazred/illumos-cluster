//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)bookkeep.cc	1.3	08/05/20 SMI"

#include <cl_net/net_debug.h>
#include <cl_net/sl_service.h>
#include <h/network.h>

extern "C" {    // can't do extern "C" static void, so surround whole thing

void	// kernel thread function returns void while
cleanup_thread(void *arg)
{
	sl_servicegrp_t	*sgrp = (sl_servicegrp_t *)arg;

	ASSERT(sgrp != NULL);

	sgrp->cleanup_post.rr_cleanup_lock.lock();
	while (true) {
		sgrp->cleanup_post.rr_cleanup_cv.wait(
		    &sgrp->cleanup_post.rr_cleanup_lock);
		switch (sgrp->cleanup_post.cleanup_state) {
			case CLEANUP_EXIT :
				sgrp->cleanup_post.cleanup_state =
				    CLEANUP_NONE;
				sgrp->cleanup_post.rr_cleanup_lock.unlock();
				return;
			case CLEANUP_START :
				sgrp->cleanup_post.cleanup_state =
				    CLEANUP_RUNNING;
				NET_DBG_RR(("Cleanup thread started\n"));
				sgrp->handle_cleanup();
				sgrp->cleanup_post.cleanup_state =
				    CLEANUP_CREATE;
			default :
				break;

		}
	}
}

void
cnct_disp(void *arg)
{


	sl_servicegrp_t *sgrp = (sl_servicegrp_t *)arg;

	while (true) {
		sgrp->connct_post.connct_lock.lock();
		sgrp->connct_post.connct_cv.wait(
		    &sgrp->connct_post.connct_lock);
		if (sgrp->connct_post.state == CLEANUP_EXIT) {
			sgrp->connct_post.state = CLEANUP_NONE;
			sgrp->connct_post.connct_lock.unlock();
			return;
		}
		sgrp->connect_call();
	}
}

void
timeout_func(void *arg)
{

	sl_servicegrp_t	*sgrp = (sl_servicegrp_t *)arg;

	ASSERT(sgrp != NULL);

	sgrp->handle_cleanup(1);
}

void
cnct_dispatch_func(void *arg)
{
	affinity_lst	*alst = (affinity_lst *)arg;

	ASSERT(alst != NULL);

	alst->handle_dispatch();
}
}
