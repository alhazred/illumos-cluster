//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mcobj_impl.cc	1.189	08/09/07 SMI"

//
// Module to handle mcobj object related services
//

#include <h/network.h>
#include <sys/os.h>
#include <orb/infrastructure/orb_conf.h>
#include <cl_net/netlib.h>
#include <cl_net/mcobj_impl.h>
#include <cl_net/errprint.h>

#ifdef SCTP_SUPPORT
#include <cl_net/conn_table.h>
#endif

#include <nslib/ns.h>
#include <sys/strsun.h>

#include <sys/mc_probe.h>
#include <orb/infrastructure/clusterproc.h>

threadpool close_conn_threadpool(true, 5, "close_conn", 30);

#ifdef SCTP_SUPPORT
threadpool open_conn_threadpool(true, 5, "open_conn", 30);
#endif

//
// Effects: Hash on the "key" modulo the table size "hsize" and
//   return the bucket number.
//
uint_t
vanilla_ht_hash_func(uint64_t key, uint_t hsize)
{
	//
	// lint doesn't like that we're trying to put a 64-bit value (the
	// key) into a 32-bit pointer (nkey when built for 32-bit), but
	// we know it is ok because we're only taking out the pointer
	// we put in earlier.
	//
	//lint -save -e511
	network::service_sap_t *nkey = (network::service_sap_t *)key;
	//lint -restore
	uint_t hashval;

	hashval = nkey->protocol + SUMOF(nkey->laddr) + nkey->lport;
	return (hashval % hsize);
}

//
// Effects: Compare keys "key1" and "key2."  Returns true if the state
//   of the keys is identical, else returns false.
//
bool
vanilla_ht_equal_func(uint64_t key1, uint64_t key2)
{
	//lint -save -e511
	network::service_sap_t *nkey1 = (network::service_sap_t *)key1;
	network::service_sap_t *nkey2 = (network::service_sap_t *)key2;
	//lint -restore

	if (nkey1->protocol == nkey2->protocol &&
		addr6_are_equal(nkey1->laddr, nkey2->laddr) &&
		nkey1->lport == nkey2->lport) {
		return (true);
	} else {
		return (false);
	}
}

//
// Effects: Free the argument in the "remove" and "dispose" methods
//   on the hash table "vanilla_ht."
//
void
vanilla_ht_free_func(uint64_t)
{
}

//
// Effects: This callback routine is a partial no-op for this hash table
//   because no key was allocated for keys that have real IP addresses,
//   and therefore there's nothing to free.  However, for the keys that
//   have INADDR_ANY as the IP address, we must allocate a key.
//
uint64_t
vanilla_ht_dup_func(uint64_t key)
{
	return (key);
}

//
// Effects: Hash on the "key" modulo the table size "hsize" and
//   return the bucket number.
//
uint_t
stickywildcard_ht_hash_func(uint64_t key, uint_t hsize)
{
	ourproto_ipaddr_t *newkey = (ourproto_ipaddr_t *)key;	//lint !e511
	return (SUMOF(newkey->laddr) % hsize);
}

//
// Effects: Compare keys "key1" and "key2."  Returns true if the state
//   of the keys is identical, else returns false.
//
bool
stickywildcard_ht_equal_func(uint64_t key1, uint64_t key2)
{
	ourproto_ipaddr_t *nkey1 = (ourproto_ipaddr_t *)key1;	//lint !e511
	ourproto_ipaddr_t *nkey2 = (ourproto_ipaddr_t *)key2;	//lint !e511

	if (addr6_are_equal(nkey1->laddr, nkey2->laddr)) {
		return (true);
	} else {
		return (false);
	}
}

//
// Effects:  Frees the storage in "key."
//
void
stickywildcard_ht_free_func(uint64_t key)
{
	ourproto_ipaddr_t *nkey1 = (ourproto_ipaddr_t *)key;	//lint !e511

	delete nkey1;
}

//
// Effects: Duplicates the "key" and returns it.
//
uint64_t
stickywildcard_ht_dup_func(uint64_t key)
{
	ourproto_ipaddr_t *nkey = (ourproto_ipaddr_t *)key;	//lint !e511

	// Allocate new key, and then copy the fields.
	ourproto_ipaddr_t *protokey = new ourproto_ipaddr_t;

	protokey->laddr = nkey->laddr;

	return ((uint64_t)protokey);
}


//
// Constructor
//
mcobj_impl_t::mcobj_impl_t() :
	vanilla_ht((uint_t)clnet_hashtable_size, // hash table initial size
	    vanilla_ht_hash_func,	// hash function
	    vanilla_ht_equal_func,	// key equality function
	    vanilla_ht_free_func,	// key free storage function
	    vanilla_ht_dup_func),	// key duplicate function
	stickywildcard_ht((uint_t)clnet_hashtable_size,
	    stickywildcard_ht_hash_func,
	    stickywildcard_ht_equal_func,
	    stickywildcard_ht_free_func,
	    stickywildcard_ht_dup_func),
	address_ht((uint_t)clnet_hashtable_size,
	    stickywildcard_ht_hash_func,
	    stickywildcard_ht_equal_func,
	    stickywildcard_ht_free_func,
	    stickywildcard_ht_dup_func),
	mcobj_muxq(NULL),
	mcobjs(NULL)
{
	NET_DBG(("vanilla_ht size = %d\n", clnet_hashtable_size));

	NET_DBG(("stickywildcard_ht size = %d\n", clnet_hashtable_size));

	disp_tid = NULL;
	disp_state = PDTS_DISP_NONE;
#ifdef SCTP_SUPPORT
	mc_conntab = NULL;
#endif
}

//
// Destructor
//
mcobj_impl_t::~mcobj_impl_t()
{
	NET_DBG(("mcobj_impl_t destructor\n"));

	//
	// If the dispatcher thread is running we need to
	// signal it to exit, which requires some non-trivial
	// synchronization.  We indicate we want it to exit by setting
	// the disp_tid to PDTS_DISP_EXIT, and signaling it to wakeup.
	// When it has exited the disp_tid should be set to NULL
	// so we know it's gone.
	//
	sl_post_mutex.lock();
	if (disp_state == PDTS_DISP_RUNNING) {
		disp_state = PDTS_DISP_EXIT;
		sl_post_cv.signal();
		sl_post_mutex.unlock();
		while (disp_state != PDTS_DISP_NONE) {
			//
			// The pdts_disp thread isn't quite done.
			//
			// It's not too important that we return
			// quickly here, as we're simply waiting
			// for the dispatcher thread to exit before
			// we cleanup.  Wait a bit so we don't spin
			// to tightly, then try again.
			//
			os::usecsleep(100000L);
			sl_post_mutex.lock();
			if (disp_state != PDTS_DISP_NONE)
				sl_post_cv.signal();
			sl_post_mutex.unlock();
		}
	} else {
		sl_post_mutex.unlock();
	}

	//
	// Now that the dispatcher thread is gone, we can get rid
	// of the thread id.
	//
	disp_tid = NULL;

	// Remove all the entries in vanilla_ht table.
	vanilla_ht.dispose();

	// Now destroy all entries in the stickywildcard hash table.
	stickywildcard_ht.dispose();

	//
	// lint expects to see these down here instead of at
	// the top of the function.
	//
	delete mcobj_muxq;
	mcobj_muxq = NULL;
	delete mcobjs;
	mcobjs = NULL;
#ifdef SCTP_SUPPORT
	delete mc_conntab;
	mc_conntab = NULL;
#endif
}

//
// An mcnet_node_impl_t object holds one reference to this mcobj
// that resides at a particular node.  All other nodes each have their
// own duplicated reference to this same mcobj; thus, there are
// n references for an n-node cluster to this mcobj.
//
void
#ifdef DEBUG
mcobj_impl_t::_unreferenced(unref_t cookie)
#else
mcobj_impl_t::_unreferenced(unref_t)
#endif
{
	NET_DBG(("mcobj_impl_t::_unreferenced()\n"));

	ASSERT(_last_unref(cookie));

	//
	// As it stands, this code will never be called.
	//
	ASSERT(0);

	//
	// Remove the service list entries and delete them.
	// The only time we should be getting an unreferenced
	// is when the pdt server is going away. If the pdt
	// server is dead, scalable services is finished until
	// next reboot.
	//
	sl_serviceobjlist_t *serviceobjlist;
	sl_servicegrp_t *servicegrpp;
	sl_serviceobj_t *serviceobjp;

	sl_servicegrplist_lock.wrlock();
	sl_servicegrplist.atfirst();

	while ((servicegrpp = sl_servicegrplist.get_current()) != NULL) {
		sl_servicegrplist.advance();

		serviceobjlist = &servicegrpp->sl_serviceobj_list;
		//
		// INVARIANT. First, remove the service objects in
		// each group before deleting the group.
		//
		for (serviceobjlist->atfirst();
		    (serviceobjp = serviceobjlist->get_current()) != NULL;
		    serviceobjlist->advance()) {
			// First, remove from the lookup tables.
			mc_remove_service_fastlookup(serviceobjp->getssap(),
			    servicegrpp->getpolicy());
			// Second, remove from list and delete.
			(void) serviceobjlist->erase(serviceobjp);
			// Third, remove from scalable address table
			mc_remove_service_address_ht(serviceobjp->getssap());
			// Finally, delete the service object
			delete serviceobjp;
		} // end for

		//
		// Second, remove the group from the list and delete
		// the group object itself.
		//
		(void) sl_servicegrplist.erase(servicegrpp);
		delete servicegrpp;
	}
	sl_servicegrplist_lock.unlock();

	//
	// Finally, delete the mcobj_impl_t object itself.  This will
	// also serve to delete the hash table objects too.
	//
	delete this;
}

//
// Static function used for our PDT dispatcher thread.
//

extern "C" {	// can't do extern "C" static void, so surround whole thing

static void
mc_pdts_disp(void *arg)
{
	static mcobj_impl_t *mcp = NULL;

	//
	// Currently we only support once instance of an mcobj
	// on the slaves ... so we'd better not be called more
	// than once.
	//
	ASSERT(mcp == NULL);

	mcp = (mcobj_impl_t *)arg;

	mcp->sl_post_mutex.lock();
	while (true) {
		mcp->sl_post_cv.wait(&(mcp->sl_post_mutex));
		//
		// If our thread id is PDTS_DISP_EXIT we're being
		// asked to exit ... set the threadid to NULL
		// to indicate our compliance.
		//
		if (mcp->disp_state == PDTS_DISP_EXIT) {
			mcp->disp_state = PDTS_DISP_NONE;
			mcp->sl_post_mutex.unlock();
			cmn_err(CE_WARN, "mc_pdts_disp exiting\n");
			return;
		}
		mcp->mc_locate_work();
	}
}

}	// extern "C"

static CORBA::Object_ptr	servref;

int
mcobj_impl_t::init(network::PDTServer_ptr pdt_server)
{
	Environment e;
	nodeid_t node = orb_conf::local_nodeid();

	//
	// We get a "smart" object ref and pass it to the IDL method;
	// we want the reference count to decrement when this init
	// routine returns.  Otherwise, we'd have a definite memory
	// leak if we didn't remember to release the reference
	// manually.
	//
	// Just create a reference to the mcobj and release it; if there
	// are no references outstanding, then unreferenced will be
	// delivered to the mcobj, and the object will eventually
	// self-destruct.
	//
	network::mcobj_var mcref = get_objref();
	//
	// There is a situation where the mcobj reference was never
	// created and never installed in a mcnet_node object. This
	// mcobj with reference count of 0 will still hang
	// around creating a memory leak. To remove the leak, we
	// must rely on the unreferenced mechanism to destroy this
	// C++ implementation object.  It is WRONG the destroy the
	// object explicitly because someone else could be handing
	// out references while the object is being deleted.
	// To handle this case, we do the following (sort of a hack).
	// Recall that the ref count's being 0 is insufficient for
	// the unreferenced stuff to kick in; it must go to at
	// least 1, and then be decremented.  To make this happen,
	// we created an artificial reference above, which we'll
	// never use. When we return and leave the scope of this
	// routine the reference count will decrement by 1.
	// At ref count 0, the ORB will deliver the unreferenced
	// message.  And the object will be destroyed.
	//

	//
	// In "attach_mcobj()" we send up our mcobj reference, and receive
	// back references to all existing mcobj's via a nested
	// invocation to mc_attach_mcobjs().
	//
	pdt_server->attach_mcobj(node, mcref, servref, e);

	if (e.exception() != NULL) {
		// Must be a system exception, but don't care which one.

		//
		// SCMSGS
		// @explanation
		// This means that we have lost communication with PDT server.
		// Scalable services will not work any more. Probably, the
		// nodes which are configured to be the primaries and
		// secondaries for the PDT server are down.
		// @user_action
		// Need to restart any of the nodes which are configured be
		// the primary or secondary for the PDT server.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to register with PDTserver");

		e.clear();
		return (ENXIO);
	}

	//
	// The dispatches thread takes care of managing bind/unbind,
	// listen/unlisten and connect/disconnect callbacks from our
	// tcp hooks.  This is performed asynchronously so our hooks
	// do not hold up the tcp layer processing unnecessarily.
	//
	// We create our dispatcher thread, with a default execution
	// stack, to execute the mc_pdts_disp() method on this object.
	// Set us runnable with minclsyspri thread priority.
	//
	sl_post_mutex.lock();

#ifdef _KERNEL
	if (clnewlwp((void (*)(void *))mc_pdts_disp,
	    this, MINCLSYSPRI, NULL, &disp_tid) != 0) {
#else
	// different thread creation call for unode

	if (clnewlwp(mc_pdts_disp, this, 0, NULL, &disp_tid) != 0) {
#endif
		//
		// SCMSGS
		// @explanation
		// The system has run out of resources that is required to
		// create a thread. The system could not create the connection
		// processing thread for scalable services.
		// @user_action
		// If cluster networking is required, add more resources (most
		// probably, memory) and reboot.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "No PDT Dispatcher thread.");
		sl_post_mutex.unlock();
		return (ENOMEM);
	}
	disp_state = PDTS_DISP_RUNNING;
	sl_post_mutex.unlock();

	// create sctp connection table
#ifdef SCTP_SUPPORT
	mc_create_conn_table();
#endif

	return (0);
}

void
mcobj_impl_t::mc_register_all_services(Environment&)
{
	uint_t				len;
	uint_t				index = 0;
	const network::srvinfo_t	*srvinfop;
	network::srvinfoseq_t_var	srvinfoseqp;
	Environment			e;
	network::PDTServer_var		local_pdt_ref;

	NET_DBG(("mcobj_impl_t::mc_register_all_services()\n"));

	MC_PROBE_0(mc_register_all_services_start, "mc net mcobj", "");

	//
	// Query the PDT server to get all the registered services and
	// information about which nodes are the gif nodes.
	//
	local_pdt_ref = network_lib.get_pdt_ref();
	if (CORBA::is_nil(local_pdt_ref)) {
		return;
	}
	NET_DBG(("  call pdts->get_all_registered_services()\n"));
	local_pdt_ref->get_all_registered_services(srvinfoseqp, e);

	if (e.exception() != NULL) {
		e.exception()->print_exception(
		    "Error in get_all_registered_services()");
		e.clear();
		return;
	}

	len = srvinfoseqp->length();

	MC_PROBE_1(mc_register_all_services, "mc net mcobj", "",
		tnf_uint, number_of_services, len);

	// Iterate over the sequence of service information.
	while (index < len) {
		srvinfop = &srvinfoseqp[index];
		mc_register_a_service(*srvinfop, e);
		index++;
	} // end while

	NET_DBG(("Scalable services enabled.\n"));

	MC_PROBE_0(mc_services_enabled, "mc net mcobj", "");
	MC_PROBE_0(mc_register_all_services_end, "mc net mcobj", "");
}

//
// Requires: sl_servicegrplist_lock must be held by caller.
//
// Effects: Insert the service group "servicegrpp" into the fast lookup
//   tables for the given "ssap" and the load-balancing policy "policy."
// Parameters:
//   ssap (IN):		service access point for service object
//   policy (IN):	load-balancing policy
//   servicegrpp (IN):  pointer to slave service group object
//
void
mcobj_impl_t::mc_insert_service_fastlookup(const network::service_sap_t& ssap,
    network::lb_specifier_t policy, sl_servicegrp_t *servicegrpp,
    ourproto_ipaddr_t *sticky_wc_key)
{
	NET_DBG(("mcobj_impl_t::mc_insert_service_fastlookup()\n"));

#ifdef _KERNEL
	// INVARIANT.  A write lock on the group list must be held.
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	//
	// Note that we need not any special locking for the hash tables
	// because the slave service group list is write-locked.
	//
	if (is_sticky_wildcard(policy)) {
		NET_DBG(("  sap = [ipaddr=%s]\n",
		    network_lib.ipaddr_to_string(sticky_wc_key->laddr)));

		//
		// Now insert this [key, servicegrpp] tuple into the
		// stickywildcard_ht hash table.
		//
		stickywildcard_ht.add(servicegrpp, sticky_wc_key);
	} else {
		//
		// Now try inserting the [ssap, servicegrpp] into the
		// the vanilla hash table.  Note that the pointer to the
		// "ssap" argument comes from the service object itself.
		// We are in effect pointing directly into that object.
		//
		NET_DBG(("  sap = [proto=%s,ipaddr=%s,port=%u]\n",
		    network_lib.protocol_to_string(ssap.protocol),
		    network_lib.ipaddr_to_string(ssap.laddr),
		    ssap.lport));

		vanilla_ht.add(servicegrpp, (network::service_sap_t *)&ssap);
	} // end if-then-else
}

//
// Create mc_conntab.
//

#ifdef SCTP_SUPPORT
void
mcobj_impl_t::mc_create_conn_table()
{
	NET_DBG(("mcobj_impl_t::mc_create_conn_table\n"));

	mc_conntab = new conn_table_t(cn_hash_size);
}
#endif // SCTP_SUPPORT

//
// Insert address of ssap to scalable address table (address_ht).
// The service group containing ssap should be passed in.
//
// address_ht provides a quick way to decide if a given address belongs to
// any scalable SAPs.
//
void
mcobj_impl_t::mc_insert_service_address_ht(const network::service_sap_t& ssap,
    sl_servicegrp_t *servicegrpp)
{
	NET_DBG(("mcobj_impl_t::mc_insert_service_address_ht([%s %s %d])\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

#ifdef _KERNEL
	// INVARIANT.  A write lock on the group list must be held.
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	ourproto_ipaddr_t addrkey(ssap.laddr);

	if (!address_ht.lookup(&addrkey)) {
		//
		// The service group pointer is added as the value, so
		// that a lookup will return non-NULL. The hash table does
		// not allow for a simple "key exists?" question. So we
		// have to associate the address (key) with some value
		// (the service group pointer).
		//
		address_ht.add(servicegrpp, &addrkey);
		NET_DBG(("  inserted ipaddr %s\n",
		    network_lib.ipaddr_to_string(ssap.laddr)));
	}
}

void
mcobj_impl_t::mc_register_a_service(const network::srvinfo_t& srvinfo,
    Environment&)
{
	uint_t		nhash;
	sl_servicegrp_t *servicegrpp = NULL;
	sl_servicegrp_t *new_servicegrpp = NULL;
	sl_serviceobj_t *new_serviceobjp = NULL;
	ourproto_ipaddr_t *sticky_wc_key = NULL;
	bool group_exists = false;

	MC_PROBE_0(mc_register_a_service_start, "mc net mcobj", "");

	const network::group_t& gname = srvinfo.srv_group;
	const network::service_sap_t& ssap = srvinfo.srv_sap;
	const network::lb_specifier_t policy = srvinfo.srv_policy;
	const network::sticky_config_t st = srvinfo.srv_stconfig;
	const nodeid_t	gifnode = srvinfo.srv_gifnode;
	nhash = srvinfo.srv_nhash;

	upgrade_inaddr(ssap.laddr);

	MC_PROBE_2(mc_register_a_service, "mc net mcobj", "",
		tnf_int, lport, ssap.lport,
		tnf_uint, node_number, orb_conf::local_nodeid());

	NET_DBG(("mcobj_impl_t::mc_register_a_service()\n"));
	NET_DBG(("  gname = %s\n", gname.resource));

	//
	// Obtain a read lock on the slave service group list.  Just
	// reading the items, so we don't a write lock.  We're checking
	// merely for existence.
	//
	sl_servicegrplist_lock.rdlock();

	//
	// Check to see if the group exists.
	//
	servicegrpp = find_sl_servicegrp(gname);
	if (servicegrpp != NULL) {
		group_exists = true;
	}

	//
	// Drop the lock here.  We do this because this method might
	// run potentially in interrupt context due to priority inversion,
	// and we cannot do blocking memory allocations.
	//
	sl_servicegrplist_lock.unlock();

	//
	// If the group doesn't exist then create a new one.  Always
	// create a service object; if some other thread added the object
	// while the lock was dropped, then we'll just delete that
	// newly created service object and return.
	//

	if (!group_exists) {
		new_servicegrpp = new sl_servicegrp_t(gname, nhash, policy);
		new_servicegrpp->sl_config_sticky(st);
		new_serviceobjp = new sl_serviceobj_t(*new_servicegrpp, ssap);
	} else {
		//
		// We create the service object without the back pointer
		// to the service group because it's possible that since
		// we dropped the lock someone blew away the object, and
		// we'll be hanging onto a dangling reference that points
		// to something deleted.
		//
		new_serviceobjp = new sl_serviceobj_t(ssap);
	}

	if (is_sticky_wildcard(policy)) {
		// Create a new key [ip address]
		sticky_wc_key = new ourproto_ipaddr_t(ssap.laddr);
	}

	// Obtain a write lock on the group list.
	sl_servicegrplist_lock.wrlock();

	//
	// Check to see that no one else created a service group while
	// we dropped the lock.
	//
	servicegrpp = find_sl_servicegrp(gname);
	if (servicegrpp == NULL) { // Didn't find it.
		servicegrpp = new_servicegrpp;
		// Append service to list of services
		if (is_sticky_wildcard(policy)) {
			sl_servicegrplist.append(new_servicegrpp);
		} else {
			sl_servicegrplist.prepend(new_servicegrpp);
		}
	} else {
		// Now set the service group in the service object.
		new_serviceobjp->set_service_grp(*servicegrpp);
	}

	//
	// Add the new ssap to this group if it doesn't already
	// have this one in its serviceobj list.
	//
	servicegrpp->sl_serviceobj_list_lock.wrlock();

	if (!servicegrpp->equal(ssap)) {
		servicegrpp->sl_serviceobj_list.append(new_serviceobjp);
	} else {
		//
		// Already added (possibly by another thread).
		// Delete service object created . Release locks and return.
		// At this point, we know that the service object already
		// exists, and therefore, already exists in the fast
		// service lookup tables.  We must return to avoid
		// adding this ssap to the lookup tables; note that
		// the table implementation allows the same key to be
		// entered more than once.
		//
		delete new_serviceobjp;
		servicegrpp->sl_serviceobj_list_lock.unlock();
		sl_servicegrplist_lock.unlock();
		return;
	}

	servicegrpp->sl_serviceobj_list_lock.unlock();


	//
	// From the service object we return a pointer to the sap field
	// in the super class and use that pointer to store in the
	// hash table entry for the key.  In this way we can avoid
	// allocating an object for the sap and using a pointer
	// to that object.
	//
	// Now associate the servicegrpp with the SAP in the fast
	// lookup tables.
	//
	// Because of the possibility of priority inversion
	// (this method could run at interrupt
	// priority where blocking allocations are disallowed), we must
	// create the sticky wildcard key _just in case_ we might need it.
	//
	mc_insert_service_fastlookup(new_serviceobjp->getssap(),
		policy, servicegrpp, sticky_wc_key);

	//
	// Add address to scalable address table
	//
	mc_insert_service_address_ht(new_serviceobjp->getssap(), servicegrpp);

	//
	// Propagate information about the gifnode attached to the serviceobj.
	//
	new_serviceobjp->set_gifnode(gifnode);

	// If not sticky wildcard policy, then delete the key object.
	if (!is_sticky_wildcard(policy)) {
		delete sticky_wc_key;
	}

	if (servicegrpp->is_strong_sticky()) {
		//
		// Re-create the binding table now that we added a new
		// service. This allows existing connections with the
		// newly added service to be recorded in the binding
		// table. See 4701492.
		//
		servicegrpp->sl_create_proxy_bind_table();
	}

	network::rr_config_t rr;

	// If UDP, set the flag. The flag  is used to start the
	// UDP timeout thread
	if (ssap.protocol == IPPROTO_UDP) {
		rr.rr_flags = network::LB_RR_UDP;
		rr.rr_conn_threshold = 0;
		servicegrpp->sl_config_rr(rr);
	}


	// Unlock the group list.
	sl_servicegrplist_lock.unlock();

	MC_PROBE_0(mc_register_a_service_end, "mc net mcobj", "");
}

//
// Effects:  Helper routine.  Returns an object identified by "ssap"
//   in the master service list if it exists, else returns NULL.
//
// Parameters:
//   - ssap (IN):    service access point <protocol, ip addd, port>
//
sl_serviceobj_t *
mcobj_impl_t::find_sl_serviceobj(sl_serviceobjlist_t &slsobjlist,
    const network::service_sap_t &sap)
{
	sl_serviceobj_t *serviceobjp = NULL;

	//
	// XXX This routine should be in sl_service.cc
	//
	sl_serviceobjlist_t::ListIterator iter(slsobjlist);

	// Now find the ma service object in the group given the "sap."
	for (; (serviceobjp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (serviceobjp->equal(sap)) {	// found it!
			break;
		}
	} // end for

	return (serviceobjp);
}

void
mcobj_impl_t::mc_register_grp(const network::group_t &g_name,
    uint32_t nhash, network::lb_specifier_t policy, Environment &)
{
	sl_servicegrp_t *servicegrpp = NULL;
	sl_servicegrp_t *new_servicegrpp = NULL;

	NET_DBG(("mcobj_impl_t::mc_register_grp()\n"));
	NET_DBG(("  group = %s\n", g_name.resource));

	//
	// Acquire read lock on slave service group list.  We just need
	// a read lock here to iterate over the list.
	//
	sl_servicegrplist_lock.rdlock();

	// Check to see whether service group "g_name" exists.
	servicegrpp = find_sl_servicegrp(g_name);

	// If group already exists, just return.
	if (servicegrpp != NULL) {
		sl_servicegrplist_lock.unlock();
		return;
	}

	// Release read lock.
	sl_servicegrplist_lock.unlock();

	//
	// We must create the new service group object without holding
	// the slave service group list lock; otherwise, we'd be doing
	// a blocking memory allocation potentially in interrupt context.
	// Then we must reacquire a lock, check to see if someone else
	// added the object in the meantime, and if not, add it.
	//
	new_servicegrpp = new sl_servicegrp_t(g_name, nhash, policy);

	// Try to acquire WRITE lock on slave service group list.
	sl_servicegrplist_lock.wrlock();

	//
	// Check to see if group exists, which means it was added
	// possibly by another thread.
	//
	servicegrpp = find_sl_servicegrp(g_name);
	if (servicegrpp != NULL) {
		delete new_servicegrpp;
		sl_servicegrplist_lock.unlock();
		return;
	}

	if (is_sticky_wildcard(new_servicegrpp->getpolicy()))
		sl_servicegrplist.append(new_servicegrpp);
	else
		sl_servicegrplist.prepend(new_servicegrpp);

	//
	// The slave pdt will be created when a gifnode
	// is attached to the service.
	//
	sl_servicegrplist_lock.unlock();
}

//
// Effects: Remove group from slave service group list.
//
void
mcobj_impl_t::mc_deregister_grp(const network::group_t &g_name, Environment &)
{
	sl_servicegrp_t *servicegrpp;

	sl_servicegrplist_lock.wrlock();
	servicegrpp = find_sl_servicegrp(g_name);

	// If servicegrpp is NULL, the group wasn't found. Remove
	// from list.
	if (servicegrpp != NULL) {
		(void) sl_servicegrplist.erase(servicegrpp);
	}

	sl_servicegrplist_lock.unlock();

	// Now, delete the object.
	delete servicegrpp;
}

//
// Configure stickiness parameters for the specified group.
// This function can be run on a non-sticky service group; it simply sets
// a bunch of values and really has no effect.
//
void
mcobj_impl_t::mc_config_sticky_grp(const network::group_t &g_name,
    const network::sticky_config_t &st, Environment &)
{
	sl_servicegrp_t *servicegrpp;

	sl_servicegrplist_lock.wrlock();

	servicegrpp = find_sl_servicegrp(g_name);
	if (servicegrpp != NULL) {
		servicegrpp->sl_config_sticky(st);
	}

	sl_servicegrplist_lock.unlock();
}

//
// Effects: This routine removes the service object identified by
//   argument "ssap" from the fast lookup tables, given the load-balancing
//   policy "policy."
// Parameters:
//   ssap (IN):	   Service access point identifies a service object.
//   policy (IN):  Load-balancing policy
//
void
mcobj_impl_t::mc_remove_service_fastlookup(const network::service_sap_t& ssap,
    network::lb_specifier_t policy)
{
	NET_DBG(("mcobj_impl_t::mc_remove_service_fastlookup()\n"));

	if (is_sticky_wildcard(policy)) {
		// Create new key [ip address]
		ourproto_ipaddr_t newkey(ssap.laddr);

		// Remove from sticky wildcard hash table.
		(void) stickywildcard_ht.remove(&newkey);
	} else {
		network::service_sap_t newkey;
		newkey.protocol = ssap.protocol;
		newkey.laddr = ssap.laddr;
		newkey.lport = ssap.lport;

		// Remove from the vanilla_ht hash table
		(void) vanilla_ht.remove(&newkey);
	} // end if-then-else
}

//
// Remove address of ssap from scalable address table (address_ht).
// It is only removed when no service of this and all other
// service groups uses it.
//
// address_ht provides a quick way to decide if a given address belongs to
// any scalable SAPs.
//
void
mcobj_impl_t::mc_remove_service_address_ht(const network::service_sap_t& ssap)
{
	NET_DBG(("mcobj_impl_t::mc_remove_service_address_ht([%s %s %d])\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

#ifdef _KERNEL
	// INVARIANT.  A write lock on the group list must be held.
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	ourproto_ipaddr_t addrkey(ssap.laddr);
	sl_servicegrp_t *servicep;

	if (address_ht.lookup(&addrkey) != NULL) {
		//
		// See if there is any remaining service group that has
		// the same address. If yes, point to it. If not, this
		// address is no longer scalable and this entry is
		// removed.
		//
		sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);
		for (; (servicep = iter.get_current()) != NULL;
		    iter.advance()) {
			if (servicep->ip_equal(ssap))
				break;
		}

		if (servicep == NULL) {
			//
			// No SAPs use this address. Remove it.
			//
			(void) address_ht.remove(&addrkey);
			NET_DBG(("  removed ipaddr %s\n",
			    network_lib.ipaddr_to_string(ssap.laddr)));
		} else {
			//
			// Address still used. Need to keep it around. We
			// also want to make sure the address is always
			// associated with a group that uses it. That's
			// why we have to remove-and-add here.
			//
			(void) address_ht.remove(&addrkey);
			address_ht.add(servicep, &addrkey);
		}
	}
}

void
mcobj_impl_t::mc_deregister_services(const network::service_sap_t &ssap,
	Environment &)
{
	sl_serviceobj_t	*serviceobjp = NULL;
	sl_servicegrp_t *servicegrpp;
	network::lb_policy_t	policy;

	upgrade_inaddr(ssap.laddr);

	NET_DBG(("mcobj_impl_t::mc_deregister_services()\n"));

	MC_PROBE_0(mc_deregister_services_start, "mc net mcobj", "");

	//
	// Since we may be listed for instance registration/deregistration
	// we need to hold the sl_post_mutex to avoid being accessed by
	// the pdts_disp thread.  To avoid deadlock with the dispatcher
	// thread, we acquire the post mutex first, then the servicegrp
	// list lock.  This same lock ordering is repeated by mc_checknset
	// as well.
	//
	sl_post_mutex.lock();

	//
	// Remove service from slave-service list
	//
	sl_servicegrplist_lock.wrlock();
	servicegrpp = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicegrpp != NULL) {
		//
		// Now find the slave service object.
		//
		// Read-lock the service object list
		servicegrpp->sl_serviceobj_list_lock.rdlock();

		serviceobjp = find_sl_serviceobj(
		    servicegrpp->sl_serviceobj_list, ssap);
		if (serviceobjp == NULL) {	// didn't find obj
			servicegrpp->sl_serviceobj_list_lock.unlock();
			sl_servicegrplist_lock.unlock();
			sl_post_mutex.unlock();
			return;
		}

		//
		// Erase from list
		//
		(void) servicegrpp->sl_serviceobj_list.erase(serviceobjp);

		//
		// Make sure we're removed from the service post list
		// so that we're not dereferenced when it's
		// traversed by the pdts_disp thread.
		//
		if (serviceobjp->sl_flags)
			(void) sl_post.erase(serviceobjp);

		//
		// Now remove entries (if there any) from the forwarding lists
		// in the PDT (that is associated with the service group) but
		// only for this service.
		//
		servicegrpp->sl_clean_frwd_info_by_ssap(ssap);

		//
		// Now remove entry from the fast lookup tables.
		//
		policy = servicegrpp->getpolicy();
		mc_remove_service_fastlookup(ssap, policy);

		// And remove from scalable address table
		mc_remove_service_address_ht(ssap);

		//
		// We wait till here to delete the object, so that the
		// mc_remove_service_fastlookup() call can
		// succeed, since the hash table entry key is actually
		// pointing to this object. See 4854323.
		//
		delete serviceobjp;

		servicegrpp->sl_serviceobj_list_lock.unlock();
	} // end if

	sl_servicegrplist_lock.unlock();

	MC_PROBE_0(mc_deregister_services_end, "mc net mcobj", "");

	//
	// Unlock the pdts_disp thread.
	//
	sl_post_mutex.unlock();
}

void
mcobj_impl_t::mc_detach_gifnode(const network::service_sap_t& ssap,
    Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);

	//
	// Remove gif from slave-service list for specified service
	//
	sl_servicegrplist_lock.rdlock();
	for (sl_servicegrplist.atfirst();
	    (servicep = sl_servicegrplist.get_current()) != NULL;
	    sl_servicegrplist.advance()) {
		if (servicep->ip_matches(ssap)) {
			//
			// We are deregistering as the GIF node. Push the
			// pernodelst to the respective proxies.
			//
			servicep->sl_dispatch_pernode_lst();
#ifdef _KERNEL
			if (sl_servicegrplist_lock.try_upgrade() == 0) {
				sl_servicegrplist_lock.unlock();
				sl_servicegrplist_lock.wrlock();
			}
#endif
			servicep->sl_deregister_gifnode();
		}
	}

	sl_servicegrplist_lock.unlock();
}

void
mcobj_impl_t::mc_set_gifnode(const network::service_sap_t& ssap,
    nodeid_t gifnode, Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);

	sl_servicegrplist_lock.rdlock();

	sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);
	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicep->ip_matches(ssap)) {
			servicep->sl_set_gifnode(ssap, gifnode);
		}
	}
	sl_servicegrplist_lock.unlock();
}



//
// Get connection information assciated with a service pdt entry
//
int
mcobj_impl_t::mc_pdte_conninfo(const network::service_sap_t& ssap,
    nodeid_t gifnode, const network::hashinfo_t& hashinfo,
    network::fwd_t_out fwdout, uint_t flag, Environment&)
{
	sl_servicegrp_t *servicep;
	int rc;

	upgrade_inaddr(ssap.laddr);

	//
	// Initialize return values
	//
	fwdout = new network::fwd_t();
	network::fwd_t *fwdp = fwdout;
	fwdp->fw_node = 0;
	fwdp->fw_ncid = 0;

	network::cidseq_t& cidseq = fwdp->fw_cidseq;
	cidseq.length(0);

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		//
		// Found a matching service - get forwarding information
		//
		rc = servicep->sl_conninfo(hashinfo, gifnode, fwdp, flag);

		sl_servicegrplist_lock.unlock();

		if (rc == 0) {
			MC_PROBE_3(mc_pdte_conninfo, "mc net mcobj", "",
				tnf_uint, fw_node, fwdp->fw_node,
				tnf_int, ncids, fwdp->fw_ncid,
				tnf_uint, length, (fwdp->fw_cidseq).length());
		}
		return (rc);
	}
	sl_servicegrplist_lock.unlock();
	return (-1);
}

//
// Add frwding info to the slave pdt.
//
// Flag with DESTROY indicates the GIF node sorresponding fwd list should be
// detroyed if already exists before adding the new entries. NODESTROY
// means the existing list should not be detroyed.
//
// FORCE signifies entries will be explicitly added to the fwd list even if
// forwarded node matches with the hash bucket's corresponding forwarded node.
// NOFORCE is the opposite
//
void
mcobj_impl_t::mc_add_frwd_info(const network::service_sap_t& ssap,
    const network::fwd_t& fwdp, uint_t flag, Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);
	upgrade_fwdseq(fwdp);

	MC_PROBE_0(mc_add_frwd_info, "mc net mcfwd", "");

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		servicep->sl_add_frwd_info(ssap, fwdp, flag);
	}

	sl_servicegrplist_lock.unlock();
}

//
// Removed forwarding info of this node.
//
void
mcobj_impl_t::mc_clean_frwd_info(nodeid_t node,
    const network::service_sap_t& ssap, Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		servicep->sl_clean_frwd_info(node);
	}
	sl_servicegrplist_lock.unlock();
}


//
// Stop forwarding
//
void
mcobj_impl_t::mc_stop_forwarding(const network::cid_t& cid,
    nodeid_t node, Environment&)
{
	sl_servicegrp_t *servicep;
	network::service_sap_t ssap;

	upgrade_inaddr(cid.ci_laddr);
	upgrade_inaddr(cid.ci_faddr);

	NET_DBG_FWD(("mcobj_impl_t::mc_stop_forwarding(node=%d)\n", node));

	ssap.protocol = cid.ci_protocol;
	ssap.laddr = cid.ci_laddr;
	ssap.lport = cid.ci_lport;

	sl_servicegrplist_lock.rdlock();

	sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);

	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (!(servicep->matches(ssap)))
			continue;
#ifdef DEBUG
		NET_DBG_FWD(("  Stop forwarding laddr=%s,lport=%d\n",
		    network_lib.ipaddr_to_string(cid.ci_laddr),
		    cid.ci_lport));
#endif

		//
		// Found a matching service - update switching information
		//
		servicep->sl_stop_forward(cid, node);
	}

	sl_servicegrplist_lock.unlock();
}

//
// Close SCTP multihomed connection
// We assume ssap is registered as it is validated before calling this function.
// This function is called by cl_private_sctp_disconnect and
// cl_private_sctp_assoc_change (when addresses are deleted)
//

#ifdef SCTP_SUPPORT
void
mcobj_impl_t::mc_multi_closeconn(network::service_sap_t& ssap, uint_t nfaddrs,
    uint8_t *faddrp, in_port_t fport, cl_sctp_handle_t handle,
    boolean_t assoc_ch) {
	network::cid_t cid;

	//
	// It will be sufficient to use any single local addr to lookup
	// service group as  we can uniquely identify one service group
	// with a sap. It is also OK to use single local
	// address for connection entries in the forwarding lists
	// because inside a PDT cid entry is identified by the
	// foreign address only.
	//

	cid.ci_protocol = ssap.protocol;
	cid.ci_laddr = ssap.laddr;
	cid.ci_lport = ssap.lport;
	cid.ci_fport = fport;

	uint8_t *wfaddrp = faddrp;
	for (uint_t i = 0; i < nfaddrs; i++) {
		copy_tcpaddr_to_addr(AF_INET6, wfaddrp,
		    &cid.ci_faddr);

		MC_PROBE_2(mc_multi_closeconn,
		    "mc net cl_inet", "",
		    tnf_string, faddr,
		    network_lib.ipaddr_to_string(cid.ci_faddr),
		    tnf_int, fport, fport);
		mc_closeconn(ssap, cid);
		wfaddrp = wfaddrp + sizeof (in6_addr_t);
	}
	if (!assoc_ch) {
		//
		// If called during association disconnect, remove the
		// connection entry
		//
		mc_conntab->remove_entry(handle);
	} else {
		//
		// If called due to association change remove the
		// particular address from the entry in connection table
		//
		conn_entry_t *entry;
		entry =  mc_conntab->lookup_entry(handle);
		mc_conntab->list_wrlock(handle);
		entry->deduct_addr(faddrp, nfaddrs);
		mc_conntab->list_unlock(handle);
	}
}
#endif  // SCTP_SUPPORT

//
// Connection close handler routine
//
void
mcobj_impl_t::mc_closeconn(network::service_sap_t& ssap, network::cid_t& cid)
{
	sl_servicegrp_t *servicep;

	sl_servicegrplist_lock.rdlock();

	//
	// Are we interested in this connection? First decide if the local
	// endpoint is a scalable sap, using mc_fastlookup_service().
	//
	// We also want to ignore connections that originate from a scalable
	// address to a scalable address. These entries are useless for
	// both forwarding entries and binding entries, since they are not
	// for clients external to the cluster.
	// What's worse is that TCP doesn't seem to give us a balanced
	// number of connect() and disconnect() callbacks on these
	// connections. These connections would therefore be harmful for
	// the client affinity connection counting mechanism.
	// See bug 4731235.
	//
	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL && !is_scalable_address(cid.ci_faddr)) {
		if (servicep->is_strong_sticky()) {
			//
			// If last connection is closed, start
			// a timer and send an event to the GIF nodes
			// when timer expires.
			//
			servicep->sl_closeconn(cid);

			// Skip the forwarding machinery
			sl_servicegrplist_lock.unlock();
			return;
		}

		//
		// Check if we are in the forwarding mode
		// or not. If we are not in forwarding mode,
		// then this function is a no-op. If we are
		// in a forwarding mode for this bucket, then
		// we need to call stop_forwarding() on the slave pdt
		// and the pdt server. For RR we are in the forwarding
		// mode and hence this function is no-op
		//

		if (!servicep->is_rr_enabled() &&
		    !servicep->sl_is_forwarded(cid)) {
			sl_servicegrplist_lock.unlock();
			return;
		}
		MC_PROBE_0(mc_closeconn, "mc net mcobj", "");

		nodeid_t node = servicep->sl_find_gifnode(ssap);

		sl_servicegrplist_lock.unlock();

		close_conn *clconnp;
		//
		// Need to have a close_conn structure. But cannot
		// do a new with SLEEP because this could be happening
		// in interrupt or streams context.
		//
		// We can't loop forever since the request may never
		// be satisified and we would hang.  If we don't send
		// back a connection closed notification back we will
		// incur a small memory loss at the gifnode, however,
		// that entry will be cleaned up if we go down, or the
		// gifnode fails over.
		//
		// Chances are if we are that short on memory the node
		// will probably drop out of membership shortly.
		// Unfortunately, we cannot preallocate enough structures
		// because we can never know how many connections would
		// simultaneously close.
		//
		for (int i = 0; i < CLOSE_CONN_RETRIES; i++) {
			clconnp = new (os::NO_SLEEP) close_conn(cid, node);
			if (clconnp)
				break;
		}

		if (clconnp) {
			close_conn_threadpool.defer_processing(clconnp);
		} else {
			//
			// SCMSGS
			// @explanation
			// The system has run out of resources that is
			// required to process connection terminations for a
			// scalable service.
			// @user_action
			// If cluster networking is required, add more
			// resources (most probably, memory) and reboot.
			//
			(void) cl_net_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "mc_closeconn failed to close connection");
		}

		return;
	}
	sl_servicegrplist_lock.unlock();
}

//
// Connection open handler routine
//
void
mcobj_impl_t::mc_openconn(network::service_sap_t& ssap, network::cid_t& cid)
{
	sl_servicegrp_t *servicep;

	sl_servicegrplist_lock.rdlock();

	//
	// Are we interested in this connection? First decide if the local
	// endpoint is a scalable sap, using mc_fastlookup_service().
	//
	// We also want to ignore connections that originate from a scalable
	// address to a scalable address. These entries are useless for
	// both forwarding entries and binding entries, since they are not
	// for clients external to the cluster.
	// What's worse is that TCP doesn't seem to give us a balanced
	// number of connect() and disconnect() callbacks on these
	// connections. These connections would therefore be harmful for
	// the client affinity connection counting mechanism.
	// See 4731235.
	//
	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL && !is_scalable_address(cid.ci_faddr)) {
		if (servicep->is_strong_sticky()) {
			//
			// Keep track of connections from this client so that
			// when the last one goes away, we can start the timer
			// to remove its binding entry on the GIF nodes.
			//
			(void) servicep->sl_openconn(cid);
		}
		//
		// Set the state of the connection to
		// Established. State used to cleanup
		// connection.
		if (servicep->is_rr_enabled()) {
			servicep->sl_setstate(cid);
		}
	}
	sl_servicegrplist_lock.unlock();

}


//
// Connection (with multi-endpoint) open handler routine; specially useful
// for SCTP connection (association) where one connection
// can have multiple cdi's.
// INPUT: ssap - one of the valid local sap (scalable) in the SCTP
//		association
//	nfaddrs - number of foreign addresses in the association
//	faddrs - pointer to the array of foreign addresses
//	fport - foreign port
//	can_block, assoc_ch - flags:
// This function is called during three types of events. When a connection
// is established it is called with can_block = B_FALSE and assoc_ch = B_FALSE.
// When server node initiates connection, during the early phase of the
// connection establishment it is called with can_block = B_TRUE and
// assoc_ch = B_FALSE. When association changes with addition of addresses
// for peer host, it is called with can_block = B_FALSE and assoc_ch = B_TRUE
//	size - 0 during connection establishment.
//		>0 during assoc_change
//
// ACTION: Create a fwd_t structure. And create a deferred task which pass the
//	the fwd_t structure to the GIF node to add it to the forwarding list.
//

#ifdef SCTP_SUPPORT
void
mcobj_impl_t::mc_multi_openconn(cl_sctp_handle_t handle,
    sa_family_t addr_family,
    network::service_sap_t& ssap, uint_t nfaddrs,
    uint8_t *faddrp, in_port_t fport, boolean_t can_block, boolean_t assoc_ch,
    size_t size)
{
	sl_servicegrp_t *servicep;

	sl_servicegrplist_lock.rdlock();

	//
	// Are we interested in this connection? First decide if the local
	// endpoint is a scalable sap, using mc_fastlookup_service().
	//
	// We also want to ignore connections that originate from a scalable
	// address to a scalable address. These entries are useless for
	// both forwarding entries and binding entries, since they are not
	// for clients external to the cluster.
	// What's worse is that TCP doesn't seem to give us a balanced
	// number of connect() and disconnect() callbacks on these
	// connections. These connections would therefore be harmful for
	// the client affinity connection counting mechanism.
	// See 4731235.
	//
	servicep = mc_fastlookup_service((network::service_sap_t)ssap);

	if (servicep == NULL) {
		sl_servicegrplist_lock.unlock();
		if (assoc_ch) {
			if (size > 0) {
				kmem_free(faddrp, size);
			}
		} else {
			kmem_free(faddrp, nfaddrs * sizeof (in6_addr_t));
		}
		return;
	}

	nodeid_t node = servicep->sl_find_gifnode(ssap);

	if (servicep->is_strong_sticky()) {
		sl_servicegrplist_lock.unlock();

		open_bconn *clbconnp;
		//
		// Need to have a open_bconn structure. But cannot
		// do a new with SLEEP because this could be happening
		// in interrupt or streams context. open_bconn structure
		// contains bind table address list for the newly created
		// SCTP connections.
		//
		// We can't loop forever since the request may never
		// be satisified and we would hang.
		//
		for (int i = 0; i < OPEN_CONN_RETRIES; i++) {
			clbconnp = new (os::NO_SLEEP) open_bconn(handle,
			    addr_family, ssap, nfaddrs, faddrp, fport, node,
			    assoc_ch, size);
			if (clbconnp)
				break;
		}

		//
		// If can_block is true i.e, cl_sctp_connect is called with
		// can_block, we will not defer the task and do the task
		// in the same execution flow.
		//
		if (clbconnp) {
			if (!can_block) {
				open_conn_threadpool.defer_processing(clbconnp);
			} else {
				// blocking call
				clbconnp->execute();
			}
		} else {
			if (assoc_ch) {
				if (size > 0) {
					kmem_free(faddrp, size);
				}
			} else {
				kmem_free(faddrp, nfaddrs *
				    sizeof (in6_addr_t));
			}
			//
			// SCMSGS
			// @explanation
			// Failed to allocate memory during connection
			// establishment for an SCTP sticky service.
			// @user_action
			// Install more memory, increase swap space, or reduce
			// peak memory consumption.
			//
			(void) cl_net_syslog_msgp->log(
			    SC_SYSLOG_WARNING, MESSAGE,
			    "mc_multi_openconn failed to open bindtab "
			    "connection.");
		}
		return;
	}
	sl_servicegrplist_lock.unlock();

	//
	// For non-sticky services update the forwarding list
	//
	open_conn *clconnp;

	//
	// Need to have a open_conn structure. But cannot
	// do a new with SLEEP because this could be happening
	// in interrupt or streams context.
	//
	// We can't loop forever since the request may never
	// be satisified and we would hang.
	//
	for (int i = 0; i < OPEN_CONN_RETRIES; i++) {
		clconnp = new (os::NO_SLEEP) open_conn(handle, addr_family,
		    ssap, nfaddrs, faddrp, fport, node, assoc_ch, size);
		if (clconnp)
			break;
	}

	if (clconnp) {
		if (!can_block) {
			open_conn_threadpool.defer_processing(clconnp);
		} else {
			// blocking call
			clconnp->execute();
		}
	} else {
		if (assoc_ch) {
			if (size > 0) {
				kmem_free(faddrp, size);
			}
		} else {
			kmem_free(faddrp, nfaddrs *
			    sizeof (in6_addr_t));
		}
		//
		// SCMSGS
		// @explanation
		// Failed to allocate memory during connection establishment
		// for an SCTP non-sticky service.
		// @user_action
		// Install more memory, increase swap space, or reduce peak
		// memory consumption.
		//
		(void) cl_net_syslog_msgp->log(
		    SC_SYSLOG_WARNING, MESSAGE,
		    "mc_multi_openconn failed to open connection.");
	}
}

//
// This is called by the cl_sctp_check_addrs callback function during
// SCTP bind.
// Returns shared address list corresponding to a matching service group
// if not found return all shared addresses. Returns ADD as action if matches
// returns DEL otherwise
//
// Algorithm:
//
// bool
// list_addr_against_port(IN port, OUT B, OUT Alen, OUT wildcard,
// OUT action, IN size) {
//    wildcard = false;
//    for each service group {
//	if (service grp is wildcard) {
//		wildcard = true;
//	}
//
//	// if Blen crosses size return false. This check will be everywhere
//	if port E (service grp port) { // E means belongs to
//		B = service grp addrs;
//		Blen = len(service grp addrs);
//		action = ADD;
//		break;
//	} else {
//		B = B + service grp addrs
//		Blen = Blen + len(service grp addrs);
//		action = DEL;
//	}
//    }
// }
//

bool
mcobj_impl_t::list_addr_against_port(in_port_t port,
    uint8_t **addrp, uint_t *naddrs, size_t size, int *action, int *wildcard)
{
	bool r = true;
	sl_servicegrp_t *servicegrpp;
	in6_addr_t tinadany = IN6ADDR_ANY_INIT;
	network::inaddr_t inadany;
	copy_tcpaddr_to_addr(AF_INET6, &tinadany, &inadany);

	network::inaddr_t *taddrp = (network::inaddr_t *)(*addrp);
	*wildcard = 0;

	//
	// Obtain the read locks for the pdts and the
	// master service list.
	//

	sl_servicegrplist_lock.rdlock();

	//
	// Traverse the service list and check if the specified
	// port exists and also populate the address list
	//

	sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_wildcard_sticky()) {
			*wildcard = 1;
		}

		//
		// ssap is populated with addr as 0 which matches with
		// any addresses. So port and protocol are the only matching
		// criteria.
		//
		network::service_sap_t ssap;
		ssap.protocol = IPPROTO_SCTP;
		ssap.lport = port;
		ssap.laddr = (network::inaddr_t) inadany;

		if (servicegrpp->matches(ssap)) {
			//
			// Populate the addr list with only the shared
			// addresses used by the matching service. Shared
			// collected from non-matching services will be
			// ignored.
			//
			r = servicegrpp->getaddrs(addrp, naddrs, size);
			*action = ACTION_SCTP_BINDADDR_ADD;
			break;
		} else {
			//
			// If no-match append the the shared addresses used
			// by the non-matching service to the addr list
			// so that at the end it constructs a list of all
			// shared addresses used in the system.
			//
			uint_t tnaddrs = 0;
			r = servicegrpp->getaddrs((uint8_t **)&taddrp,
			    &tnaddrs, (size_t)(size - ((uint8_t *)taddrp -
			    *addrp)));
			if (!r) {
				break;
			}

			taddrp += tnaddrs;
			(*naddrs) += tnaddrs;
			*action = ACTION_SCTP_BINDADDR_DEL;
		}
	}

	sl_servicegrplist_lock.unlock();

	return (r);

}
#endif  // SCTP_SUPPORT

//
// Get a list of binding entries (during GIF failover)
//
void
mcobj_impl_t::mc_get_bindinfo(const network::service_sap_t& ssap,
    nodeid_t new_gifnode, network::bind_t_out bindout,
    Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);

	//
	// Initialize return values
	//
	bindout = new network::bind_t();
	network::bind_t *bindp = bindout;
	bindp->bn_nodeid = 0;
	bindp->bn_nfaddr = 0;

	sl_servicegrplist_lock.rdlock();
	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		//
		// Found a matching service - get binding information
		//
		servicep->sl_get_bindinfo(new_gifnode, bindp);
	}
	sl_servicegrplist_lock.unlock();

	NET_DBG_BIND(("mc_get_bindinfo for %s on node %d\n",
	    network_lib.ipaddr_to_string(ssap.laddr),
	    bindp->bn_nodeid));

	for (uint_t index = 0; index < bindp->bn_nfaddr; index++) {
		NET_DBG_BIND(("client %s\n",
		    network_lib.ipaddr_to_string(bindp->bn_faddrseq[index])));
	}
}

//
// Add a list of binding entries (during GIF failovers)
//
void
mcobj_impl_t::mc_add_bindinfo(const network::service_sap_t& ssap,
    const network::bind_t& bindp, uint_t iter, Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);
	upgrade_bindseq(bindp);

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		ASSERT(servicep->is_strong_sticky());
		servicep->sl_add_bindinfo(bindp, iter);
	}

	sl_servicegrplist_lock.unlock();
}

//
// Creates GIF bind table entries for the entries in the pernodelst.
//

void
mcobj_impl_t::mc_add_pernode_lst(const network::service_sap_t& ssap,
    uint_t iter, Environment&)
{
	sl_servicegrp_t *servicep;

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		ASSERT(servicep->is_strong_sticky());
		servicep->sl_add_pernodelst(iter);
	}
	sl_servicegrplist_lock.unlock();

	NET_DBG_BIND(("mc_add_pernodelst for %s\n",
	    network_lib.ipaddr_to_string(ssap.laddr)));
}

//
// Remove all binding entries for a crashed proxy node
//
void
mcobj_impl_t::mc_clean_bindinfo(nodeid_t node,
    const network::service_sap_t& ssap, Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		ASSERT(servicep->is_strong_sticky());
		servicep->sl_clean_bindinfo(node);
	}

	sl_servicegrplist_lock.unlock();
}

void
mcobj_impl_t::mc_setstate_binding(const network::inaddr_t& faddr,
    const network::service_sap_t& ssap, const nodeid_t node,
    network::bind_state_t state,
    Environment& e)
{
	sl_servicegrp_t *servicep;
	bool		rcode = true;
	boolean_t	is_node_registered = B_TRUE;

	upgrade_inaddr(faddr);

	//
	// Check the state of the service on the remote node
	//
	if (is_generic_affinity((network::service_sap_t)ssap) &&
	    ((state == network::BI_DEPRECATED) ||
	    (state == network::BI_CLOSED))) {
		is_node_registered = mc_node_registered(ssap, node);
	}
	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		ASSERT(servicep->is_strong_sticky());
		rcode = servicep->sl_setstate_binding(faddr, node, state,
		    is_node_registered);
	}

	sl_servicegrplist_lock.unlock();
	if ((!rcode) && (state == network ::BI_DEPRECATED)) {
		network::donot_update *dnp(NULL);
		dnp = new network::donot_update;
		e.exception(dnp);
	}
}

void
mcobj_impl_t::mc_setstate_conn(const network::cid_t& cid,
    const network::conn_state_t state, Environment&)
{

	sl_servicegrp_t	*servicep;
	network::service_sap_t	ssap;

	ssap.protocol = cid.ci_protocol;
	ssap.laddr = cid.ci_laddr;
	ssap.lport = cid.ci_lport;

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		servicep->sl_setstate(cid);
	}

	sl_servicegrplist_lock.unlock();
}



//
// Effects: This method initializes a service group with name
//   pdtinfo.group with a new slave PDT table; it creates one
//   if necessary and populates it with data from "pdtinfo."
// PDT is set in the PDT_UNINITIALIZED state.
//
void
mcobj_impl_t::mc_init_pdt(const network::pdtinfo_t& pdtinfo, Environment&)
{
	sl_servicegrp_t		*servicegrpp = NULL;
	sl_pdt			*new_slpdt = NULL;
	bool			slpdt_exists = false;

	NET_DBG(("mcobj_impl_t::mc_init_pdt()\n"));
	NET_DBG(("  group = %s\n", pdtinfo.group.resource));

	//
	// Acquire the read lock on the slave service group list.
	// We just need to check for existence.  Yes, the group could
	// be blown away after we drop the lock, but that's okay because
	// we'll check for existence again.
	//
	sl_servicegrplist_lock.rdlock();

	//
	// Look for the service group given the name.  If we don't
	// find it, then print to common error.
	//
	servicegrpp = find_sl_servicegrp(pdtinfo.group);
	if (servicegrpp == NULL) {
		cmn_err(CE_WARN, "mc_init_pdt: Failed to find service\n");
		sl_servicegrplist_lock.unlock();
		return;
	}

	//
	// Is there a slave PDT created for the group?  If so, remember
	// that fact, else false.
	//
	if (servicegrpp->sl_pdt_exists()) {
		slpdt_exists = true;
	}
	int nhash = servicegrpp->getnhash();

	// Release the lock on the slave service group list.
	sl_servicegrplist_lock.unlock();

	//
	// If the slave PDT was not created, then create it now.  We
	// might have to destroy it later, if someone else beat us
	// to the punch and created it.
	//
	if (!slpdt_exists) {
		// Create slave PDT.
		new_slpdt = new sl_pdt((uint_t)nhash);
	}

	// Try to acquire the write lock on the slave service group list.
	sl_servicegrplist_lock.wrlock();

	// Look for the group again to make sure it still exists.
	servicegrpp = find_sl_servicegrp(pdtinfo.group);
	if (servicegrpp != NULL) {
		//
		// If the slave PDT doesn't exist, then install
		// it in the group object, but don't initialize it yet.
		//
		if (!servicegrpp->sl_pdt_exists()) {
			// Add this PDT table to the slave service group.
			servicegrpp->sl_create_pdt(new_slpdt);
		} else {
			// It does exist, so delete this copy.
			delete new_slpdt;
		}
		//
		// Initialize the slave PDT table (whether already existing)
		// or newly created with the data in "pdtinfo."
		//
		servicegrpp->sl_unmarshal_pdtinfo(pdtinfo);
	}

	// Release the lock.
	sl_servicegrplist_lock.unlock();
}

void
mcobj_impl_t::mc_init_complete(const network::service_sap_t& ssap,
    Environment&)
{
	sl_servicegrp_t *servicep;

	upgrade_inaddr(ssap.laddr);

	sl_servicegrplist_lock.wrlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		servicep->sl_init_complete();
	}

	sl_servicegrplist_lock.unlock();
}

//
// Internal method to check if a service is registered as a
// replicated service
//
int
mcobj_impl_t::isregistered(network::service_sap_t& ssap)
{
	sl_servicegrp_t *servicegrpp;

	// Acquire read lock on slave service group list.
	sl_servicegrplist_lock.rdlock();

	//
	// Check for INADDR_ANY.  This means that a server is either
	// binding/unbinding/listening/unlistening on INADDR_ANY.
	// We do a linear search of the slave service group list.
	//
	if (addr6_is_inaddr_any(ssap.laddr)) {
		sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);
		for (; (servicegrpp = iter.get_current()) != NULL;
			iter.advance()) {
		    if (servicegrpp->matches(ssap)) {
			MC_PROBE_0(isregistered, "mc net mcobj", "");
			sl_servicegrplist_lock.unlock();
			return (1);
		    }
		} // end for
	} else {
		//
		// Otherwise, it's service with a valid IP address.
		// We do a fast look up on this IP address.
		//
		servicegrpp = mc_fastlookup_service(ssap);
		if (servicegrpp != NULL) {
			MC_PROBE_0(isregistered, "mc net mcobj", "");
			sl_servicegrplist_lock.unlock();
			return (1);
		}
	}

	// Release lock.
	sl_servicegrplist_lock.unlock();
	return (0);
}

bool
mcobj_impl_t::is_scalable_address(const network::inaddr_t& dstaddr)
{
#ifdef _KERNEL
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	ourproto_ipaddr_t	partsap;

	partsap.laddr = dstaddr;
	if (address_ht.lookup(&partsap) != NULL)
		return (true);

	return (false);
}

//
// Requires: sl_servicegrplist_lock must be held.
//
// Effects: Returns a pointer to the slave service group object that
//   contains a service object that matches the argument "ssap." This
//   means that we've found a scalable service.  Otherwise, returns NULL,
//   which means that this "ssap" doesn't identify a scalable service.
// Parameters:
//   ssap (IN): service access point of service to look for.
//
sl_servicegrp_t*
mcobj_impl_t::mc_fastlookup_service(network::service_sap_t& ssap)
{
	sl_servicegrp_t		*servicegrpp = NULL;
	ourproto_ipaddr_t	partsap;

#ifdef _KERNEL
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	//
	// Look up the ssap in the ordinary services hash table..
	// If there, return the service group object, else look up
	// in the sticky wildcard hash table.
	//
	servicegrpp = (sl_servicegrp_t*)vanilla_ht.lookup(&ssap);
	if (servicegrpp == NULL) {
		//
		// It could be a sticky wildcard service.
		// Create new key [ip address]
		//
		partsap.laddr = ssap.laddr;

		//
		// Now look up the service group given the [proto, ipaddr].
		// If there, then return a pointer to it, else return NULL
		// because it doesn't represent a scalable service.
		//
		servicegrpp = (sl_servicegrp_t*)
			stickywildcard_ht.lookup(&partsap);
		if (servicegrpp == NULL) {
			// Still no object.  SAP does not identify a
			// scalable service. Send up the IP stack.
			return (NULL);
		} // end if
	} // end if

	return (servicegrpp);
}

//
// Requires: sl_servicegrp_list lock must be held.
//
// Effects: Internal method to find a servicegrp that matches the given
//   sap.  This will match specific services first, and wildcard sticky
//   services (those that service *all* ports) if a specific match isn't found.
//   Returns NULL if no service matches either case.
// Parameters:
//   ssap (IN): service access point of service to be searched for
//
sl_servicegrp_t *
mcobj_impl_t::mc_match(network::service_sap_t& ssap)
{
	sl_servicegrp_t *servicegrpp = NULL;

	MC_PROBE_0(mc_match_start, "mc net mcobj", "");

#ifdef _KERNEL
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	servicegrpp = mc_fastlookup_service(ssap);

	MC_PROBE_0(mc_match_end, "mc net mcobj", "");

	return (servicegrpp);
}

//
// Internal method to lookup a service given the service_sap.
// If present returns with the servicegrp structure locked
// else returns NULL.
//
sl_servicegrp_t *
mcobj_impl_t::mc_lookup(network::service_sap_t& ssap)
{
	sl_servicegrp_t *servicegrpp;

	MC_PROBE_0(mc_lookup_start, "mc net mcobj", "");

#ifdef _KERNEL
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	servicegrpp = mc_fastlookup_service(ssap);

	MC_PROBE_0(mc_lookup_end, "mc net mcobj", "");

	return (servicegrpp);
}

bool
mcobj_impl_t::mc_checknset(network::service_sap_t& ssap, mcop_t flag)
{
	sl_servicegrp_t *servicegrpp;

	sl_post_mutex.lock();
	sl_servicegrplist_lock.rdlock();

	//
	// Notice that we CANNOT use the mc_fastlookup_service() routine
	// here because we must examine EACH service group object to
	// check for a matching sap.  We must iterate over all groups
	// especially when services bind to INADDR_ANY, and there may
	// be more than one service in different groups.
	//
	sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (!servicegrpp->matches(ssap))
			continue;

		//
		// There could be multiple matching services especially
		// when the instance tries to bind to INADDR_ANY.
		//
		mc_setserviceflag(servicegrpp, ssap, flag);

		//
		// ssap may reside in multiple groups, so we need
		// to check them all.
		//
	}
	sl_servicegrplist_lock.unlock();

	//
	// We check the post service list here to see if
	// it's empty, then signal the pdts_disp thread
	// to handle the registration/deregistration of
	// the service instance.
	//
	if (!sl_post.empty()) {
		sl_post_cv.signal();
	}

	sl_post_mutex.unlock();

	return (true);
}

void
mcobj_impl_t::mc_setserviceflag(sl_servicegrp_t *srvp,
    network::service_sap_t& ssap, mcop_t flag)
{
	sl_serviceobj_t *sobj;

#ifdef _KERNEL
	ASSERT(sl_post_mutex.lock_held());
#endif

	//
	// Check each attached serviceobj to see if it matches
	// the sap passed in.  Usually there's only one, but we
	// do this for completeness.
	//
	srvp->sl_serviceobj_list_lock.wrlock();
	for (srvp->sl_serviceobj_list.atfirst();
	    (sobj = srvp->sl_serviceobj_list.get_current()) != NULL;
	    srvp->sl_serviceobj_list.advance()) {
		//
		// If we match, put us on the list of serviceobj's
		// which need servicing.
		//
		if (sobj->matches(ssap)) {
			//
			// If a listen/unlisten or bind/unbind
			// races, and ends up simultaneously being
			// posted for the serviceobj, do not
			// register/unregister the service.
			// Simply remove it from the list of services
			// to be handled as if it had never happened.
			//

			//
			// We must make sure we only append the serviceobj
			// once, and erase it only when the flags are
			// empty.  If we enqueue it twice, we may end up
			// erasing only one of them in the serviceobj
			// destructor, and then dereference garbage in
			// mc_locate_work later.
			//

			switch (flag) {
			case LISTEN:
				if (sobj->sl_flags & SL_UNLISTEN) {
					sobj->sl_flags &= ~SL_UNLISTEN;
					if (sobj->sl_flags == 0)
						(void) sl_post.erase(sobj);
				} else {
					if (sobj->sl_flags == 0)
						sl_post.append(sobj);
					sobj->sl_flags |= SL_LISTEN;
				}
				break;
			case UNLISTEN:
				if (sobj->sl_flags & SL_LISTEN) {
					sobj->sl_flags &= ~SL_LISTEN;
					if (sobj->sl_flags == 0)
						(void) sl_post.erase(sobj);
				} else {
					if (sobj->sl_flags == 0)
						sl_post.append(sobj);
					sobj->sl_flags |= SL_UNLISTEN;
				}
				break;
			case BIND:
				if (sobj->sl_flags & SL_UNBIND) {
					sobj->sl_flags &= ~SL_UNBIND;
					if (sobj->sl_flags == 0)
						(void) sl_post.erase(sobj);
				} else {
					if (sobj->sl_flags == 0)
						sl_post.append(sobj);
					sobj->sl_flags |= SL_BIND;
				}
				break;
			case UNBIND:
				if (sobj->sl_flags & SL_BIND) {
					sobj->sl_flags &= ~SL_BIND;
					if (sobj->sl_flags == 0)
						(void) sl_post.erase(sobj);
				} else {
					if (sobj->sl_flags == 0)
						sl_post.append(sobj);
					sobj->sl_flags |= SL_UNBIND;
				}
				break;
			case CONNECT:
			case DISCONNECT:
				break;
			default:
				break;

			}

			//
			// If the address is not INADDR_ANY, there will
			// not be another matching service in the
			// servicelist.  Better to break out rather
			// than loop on the list of all the services.
			//
			if (!addr6_is_inaddr_any(ssap.laddr))
				break;
		}
	}
	srvp->sl_serviceobj_list_lock.unlock();
}

void
mcobj_impl_t::mc_locate_work()
{
	network::service_sap_t ssap;
	nodeid_t node = orb_conf::local_nodeid();
	sl_serviceobj_t *sobj;
	Environment e;
	network::PDTServer_var local_pdt_ref;

	NET_DBG(("mcobj_impl_t::mc_locate_work()\n"));

	ASSERT(sl_post_mutex.lock_held());

	//
	// Lock the group list just to make sure we're not yanked out
	// while processing (we also cleanup properly in the serviceobj
	// destructor).
	//
	sl_servicegrplist_lock.rdlock();

	local_pdt_ref = network_lib.get_pdt_ref();
	if (CORBA::is_nil(local_pdt_ref)) {
		sl_servicegrplist_lock.unlock();
		return;
	}

	//
	// Traverse the posted serviceobj's and perform the flagged
	// registration/deregistration.
	//
	sl_post.atfirst();
	while ((sobj = sl_post.get_current()) != NULL) {

		ASSERT(sobj->sl_servicegrp != NULL);

		//
		// Make sure the serviceobj list doesn't change until
		// we've updated the flags and grabbed the SAP.  We
		// can drop this lock before performing the remote invocation
		// since we don't care about the instance state if there
		// is no serviceobj state there.
		//
		sobj->sl_servicegrp->sl_serviceobj_list_lock.wrlock();

		//
		// XXX - Bit flags used in case statement
		//	 For now it is okay since the values or
		//	 exclusive
		//
		switch (sobj->sl_flags) {
		case SL_LISTEN:
		case SL_BIND:
			//
			// The PDT server must be informed to
			// register the instance for the particular
			// node
			//
			sobj->sl_flags &= ~(SL_LISTEN | SL_BIND);

			//
			// If this is the last flag to be processed, remove
			// us from the list.
			//
			if (sobj->sl_flags == 0)
				(void) sl_post.erase(sobj);

			//
			// Mark listener up.
			//
			sobj->sl_local_state = SL_LOCAL_UP;
			sobj->sl_servicegrp->sl_local_registered(sobj);

			//
			// We can drop the servicelist lock after we've
			// gathered the SAP information since we don't really
			// care if the service is deregistered out from
			// underneath us (the PDT server should take care of
			// that).  We also don't want to be holding the lock
			// if this turns around into a local invocation
			// (i.e. via mc_pdte_conninfo())..
			//
			ssap = sobj->getssap();
			sobj->sl_servicegrp->sl_serviceobj_list_lock.unlock();
			sl_servicegrplist_lock.unlock();
			sl_post_mutex.unlock();

			//
			// we assume for now this HA op. does not fail.
			//

			local_pdt_ref->register_instance(ssap, node, e);

			if (e.exception()) {
				cmn_err(CE_WARN,
				    "Failed to register instance with PDT\n");
				e.clear();
			}

			sl_post_mutex.lock();
			sl_servicegrplist_lock.rdlock();
			break;

		case SL_UNLISTEN:
		case SL_UNBIND:
			//
			// The PDT server must be informed to
			// deregister the instance for the particular
			// node
			//
			sobj->sl_flags &= ~(SL_UNLISTEN | SL_UNBIND);

			//
			// If this is the last flag to be processed, remove
			// us from the list.
			//
			if (sobj->sl_flags == 0)
				(void) sl_post.erase(sobj);

			//
			// Mark listener down.
			//
			sobj->sl_local_state = SL_LOCAL_DOWN;
			sobj->sl_servicegrp->sl_local_deregistered(sobj);

			//
			// We can drop the servicelist lock after we've
			// gathered the SAP information since we don't really
			// care if the service is deregistered out from
			// underneath us (the PDT server should take care of
			// that).  We also don't want to be holding the lock
			// if this turns around into a local invocation
			// (i.e. via mc_pdte_conninfo())..
			//
			ssap = sobj->getssap();
			sobj->sl_servicegrp->sl_serviceobj_list_lock.unlock();
			sl_servicegrplist_lock.unlock();
			sl_post_mutex.unlock();

			local_pdt_ref->deregister_instance(ssap, node, e);

			if (e.exception()) {
				cmn_err(CE_WARN,
				    "Failed to deregister instance with PDT\n");
				e.clear();
			}

			sl_post_mutex.lock();
			sl_servicegrplist_lock.rdlock();
			break;

		case SL_CONNECT:
		case SL_DISCONNECT:
		default:
			sobj->sl_servicegrp->sl_serviceobj_list_lock.unlock();
			break;
		}
		sl_post.atfirst();
	}
	sl_servicegrplist_lock.unlock();
}

//
// ---- all implementations below this line should form a muxq object later.
//

//
// later this should return a pmx_impl_t *
//
muxq_impl_t *
mcobj_impl_t::set_muxq()
{
	//
	// muxq_create_lock prevents multiple threads from trying to
	// create muxq object at the same time and assigning it to
	// mcobj_muxq variable.
	//
	muxq_create_lock.lock();
	if (mcobj_muxq == NULL) {
		mcobj_muxq = new muxq_impl_t;
	}
	muxq_create_lock.unlock();
	return (mcobj_muxq);
}



//
// Effects: This method returns the CORBA object reference to mcobj_impl_t
//   for node "node" from the array mcobjs.
//   Else returns network::mcobj::_nil().
//
network::mcobj_ptr
mcobj_impl_t::mc_node_to_mcobj(nodeid_t node)
{
	return ((*mcobjs)[node]);
}

//
// Configure RR parameters for the specified group.
// The function simply sets a bunch of values and
// really has no effect.
//

void
mcobj_impl_t::mc_config_rr_grp(const network::group_t &g_name,
    const network::rr_config_t &rr, Environment &)
{
	sl_servicegrp_t *servicegrpp;
	sl_servicegrplist_lock.wrlock();

	servicegrpp = find_sl_servicegrp(g_name);
	if (servicegrpp != NULL)
		servicegrpp->sl_config_rr(rr);
	sl_servicegrplist_lock.unlock();
}

void
close_conn::execute()
{
	extern mcobj_impl_t *mcobjp;
	Environment e;

	ASSERT(mcobjp != NULL);

	if (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(gifnode))) {
		(mcobjp->mc_node_to_mcobj(gifnode))->mc_stop_forwarding(cid,
		    orb_conf::local_nodeid(), e);
		e.clear();
	}
	delete this;
}

//
// Execute deferred task of updating state of the connection table and
// PDT while a connection is established for non-sticky scalable service
//
#ifdef SCTP_SUPPORT
void
open_conn::execute()
{
	extern mcobj_impl_t *mcobjp;
	Environment e;
	network::inaddr_t taddr;
	network::fwd_t *fwdp = new network::fwd_t;
	sl_servicegrp_t *servicep;

	fwdp->fw_cidseq.length(nfaddrs);
	fwdp->fw_node = orb_conf::local_nodeid();
	fwdp->fw_ncid = 0;

	uint8_t *wfaddrp = faddrp;

	ASSERT(mcobjp != NULL);
	mcobjp->sl_servicegrplist_lock.rdlock();
	servicep = mcobjp->mc_fastlookup_service((network::service_sap_t)ssap);
	mcobjp->sl_servicegrplist_lock.unlock();

	if (servicep == NULL) {
		//
		// SCMSGS
		// @explanation
		// Failed to look up the internal data structure during
		// connection establishment.
		// @user_action
		// The connection involved might be disconnected. If it
		// happens repeatedly contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to look up the service group.");
		if (assoc_ch) {
			if (size > 0) {
				kmem_free(faddrp, size);
			}
		} else {
			kmem_free(faddrp, nfaddrs * sizeof (in6_addr_t));
		}
		return;
	}

	for (uint_t i = 0; i < nfaddrs; i++) {
		copy_tcpaddr_to_addr(addr_family, wfaddrp, &taddr);
		mcobjp->sl_servicegrplist_lock.rdlock();
		if (!mcobjp->is_scalable_address(taddr)) {
			mcobjp->sl_servicegrplist_lock.unlock();
			fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_protocol =
			    ssap.protocol;
			fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_lport =
			    ssap.lport;
			fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_laddr =
			    ssap.laddr;
			fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_fport =
			    fport;
			fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid].ci_faddr =
			    taddr;
			fwdp->fw_ncid++;
		} else {
			mcobjp->sl_servicegrplist_lock.unlock();
			if (assoc_ch) {
				if (size > 0) {
					kmem_free(faddrp, size);
				}
			} else {
				kmem_free(faddrp, nfaddrs *
				    sizeof (in6_addr_t));
			}
			return;
		}

		if (addr_family == AF_INET) {
			wfaddrp = wfaddrp + sizeof (uint32_t);
		} else {
			// Assuming AF_NET6
			wfaddrp = wfaddrp + sizeof (in6_addr_t);
		}
	}

	//
	// If the control reaches this point it is ensured that
	// none of the foreign addresses are scalable. Now we update
	// the sl_forwarded_count
	//
	for (uint_t i = 0; i < fwdp->fw_ncid; i++) {
		servicep->sl_update_frwd_count(fwdp->fw_cidseq[i]);
	}

	if (mcobjp->mc_conntab != NULL && !assoc_ch) {
		(void) mcobjp->mc_conntab->add_entry(handle, addr_family, ssap,
		    nfaddrs, faddrp, fport);
	}

	if (mcobjp->mc_conntab != NULL && assoc_ch) {
		conn_entry_t *entry;
		entry =  mcobjp->mc_conntab->lookup_entry(handle);
		mcobjp->mc_conntab->list_wrlock(handle);
		entry->append_addr(faddrp, nfaddrs);
		mcobjp->mc_conntab->list_unlock(handle);
	}

	network::fwd_t_var fwd_v = fwdp;
	if (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(gifnode))) {
		(mcobjp->mc_node_to_mcobj(gifnode))->mc_add_frwd_info(ssap,
		    *fwd_v, network::FWD_ADD_NODESTROY_NOFORCE, e);
		e.clear();
	}

	wfaddrp = NULL;

	delete this;
}

//
// Execute deferred task of updating state of the connection table and
// PDT while a connection is established for sticky scalable service
//
void
open_bconn::execute()
{
	extern mcobj_impl_t *mcobjp;
	Environment e;
	uint_t bind_cnt = 0;

	ASSERT(mcobjp != NULL);

	sl_servicegrp_t *servicep;
	network::bind_t *bindp = new network::bind_t;
	network::cid_t cid;
	network::inaddr_t taddr;

	bindp->bn_nfaddr = 0;
	bindp->bn_faddrseq.length(nfaddrs);
	uint8_t *wfaddrp = faddrp;

	mcobjp->sl_servicegrplist_lock.rdlock();
	servicep = mcobjp->mc_fastlookup_service((network::service_sap_t)ssap);
	mcobjp->sl_servicegrplist_lock.unlock();

	if (servicep == NULL) {
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING, MESSAGE,
		    "Failed to look up the service group.");
		if (assoc_ch) {
			if (size > 0) {
				kmem_free(faddrp, size);
			}
		} else {
			kmem_free(faddrp, nfaddrs * sizeof (in6_addr_t));
		}
		return;
	}

	cid.ci_protocol = ssap.protocol;
	cid.ci_laddr = ssap.laddr;
	cid.ci_lport = ssap.lport;
	for (int i = 0; i < nfaddrs; i++) {
		copy_tcpaddr_to_addr(addr_family, wfaddrp, &taddr);
		mcobjp->sl_servicegrplist_lock.rdlock();
		if (!mcobjp->is_scalable_address(taddr)) {
			mcobjp->sl_servicegrplist_lock.unlock();
			cid.ci_faddr = taddr;
			cid.ci_fport = fport;
			//
			// Keep track of connections from this client
			// so that when the last one goes away, we can
			// start the timer to remove its binding entry
			// on the GIF nodes.
			//
			// If the connection element is 'created'
			// in the proxy bind table, we add it in the
			// bindp list to get propagated to the GIF
			// bind table. We don't do this if the
			// element is already existing in the proxy
			// table, as it is also expected to be existing
			// in the GIF table incase of SCTP.
			//
			if (servicep->sl_openconn(cid) == 0) {
				bindp->bn_faddrseq[bindp->bn_nfaddr++]
				    = taddr;
			}
		} else {
			mcobjp->sl_servicegrplist_lock.unlock();
			delete bindp;
			if (assoc_ch) {
				if (size > 0) {
					kmem_free(faddrp, size);
				}
			} else {
				kmem_free(faddrp, nfaddrs *
				    sizeof (in6_addr_t));
			}
			return;
		}
		if (addr_family == AF_INET) {
			wfaddrp = wfaddrp + sizeof (uint32_t);
		} else {
			// Assuming AF_NET6
			wfaddrp = wfaddrp + sizeof (in6_addr_t);
		}
	}

	if (mcobjp->mc_conntab != NULL && !assoc_ch) {
		(void) mcobjp->mc_conntab->add_entry(handle, addr_family, ssap,
		    nfaddrs, faddrp, fport);
	}

	if (mcobjp->mc_conntab != NULL && assoc_ch) {
		conn_entry_t *entry;
		entry =  mcobjp->mc_conntab->lookup_entry(handle);
		mcobjp->mc_conntab->list_wrlock(handle);
		entry->append_addr(faddrp, nfaddrs);
		mcobjp->mc_conntab->list_unlock(handle);
	}

	if (bindp->bn_nfaddr == 0) {
		delete bindp;
		return;
	}

	bindp->bn_nodeid = orb_conf::local_nodeid();

	network::bind_t_var bind_v = bindp;
	if (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(gifnode))) {
		//
		// When building a new gifnode, pass in bind_cnt == 0 the first
		// time so that all existing entries on the GIF are
		// deleted. A non-zero bind_cnt tells the GIF to just add,
		// but don't delete.
		//
		bind_cnt = 1;
		(mcobjp->mc_node_to_mcobj(gifnode))->mc_add_bindinfo(ssap,
		    *bind_v, bind_cnt, e);
		e.clear();
	}

	wfaddrp = NULL;

	delete this;
}
#endif  // SCTP_SUPPORT

//
// Helper routine
// Effects: Returns the slave service group object given the group
// name "grp."
//
sl_servicegrp_t*
mcobj_impl_t::find_sl_servicegrp(const network::group_t &grp)  // IN
{
	sl_servicegrp_t *servicegrpp = NULL;

#ifdef _KERNEL
	ASSERT(sl_servicegrplist_lock.lock_held());
#endif

	sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_group_equal(grp)) { // found it!
			break;
		}
	}

	return (servicegrpp);
}

//
// Update a specific node mcobj reference.
//
void
mcobj_impl_t::mc_attach_mcobj(nodeid_t node, network::mcobj_ptr mcobjref,
    Environment& e)
{
	NET_DBG(("mcobj_impl_t::mc_attach_mcobj(node=%d)\n", node));

	if (!CORBA::is_nil(mcobjref)) {
		// The array element is a "smart" pointer.  The array
		// implementation will release the object reference
		// before assigning the new one.
		(*mcobjs)[node] = network::mcobj::_duplicate(mcobjref);
	} else {
		e.exception(new sol::op_e(ENOENT));
	}
}

//
// Effects: This method updates the cluster-wide mcobj references
//   given argument "mcobjsref".
//
void
mcobj_impl_t::mc_attach_mcobjs(const network::mcobjs_t &mcobjsref,
    Environment&)
{
	NET_DBG(("mcobj_impl_t::mc_attach_mcobjs()\n"));

	if (mcobjs != NULL) {
		// Delete the mcobjs array.  This has the side effect
		// of releasing the CORBA object references.
		delete mcobjs;
	}

	mcobjs = new network::mcobjs_t(mcobjsref);
}

void
mcobj_impl_t::mc_gns_bind(const network::group_t& gname, bool as_gif,
    network::gns_bindseq_t_out bindoutp, int32_t &st_timeout,
    Environment& e)
{
	network::gns_bindseq_t *bindp = NULL;
	sl_servicegrp_t *sgrp;
	int ret_timeout = 0;

	bindoutp = (network::gns_bindseq_t *)NULL;

	sl_servicegrplist_lock.rdlock();
	if ((sgrp = find_sl_servicegrp(gname)) == NULL) {
		sl_servicegrplist_lock.unlock();
		e.exception(new sol::op_e(ENOENT));
		return;
	}
	bindp = new network::gns_bindseq_t;
	sgrp->sl_gns_bind(as_gif, bindp, ret_timeout);
	sl_servicegrplist_lock.unlock();

	bindoutp = bindp;
	st_timeout = ret_timeout;
}

//
// Set of debugging interfaces for getting information from the mcobj
// to print out in print_net_state (gns stands for "get network state").
//
//	mc_gns_fwd	- get forwarding list information
//	mc_gns_pdt	- get pdt table information
//	mc_gns_service	- get service information
//	mc_gns_ht	- get fast lookup hash table information
//

void
mcobj_impl_t::mc_gns_fwd(network::fwdseq_t_out fwdinfop, nodeid_t node,
    const network::group_t& gname, Environment& e)
{
	network::fwdseq_t *fwdp;
	network::seqlong_t *cntseqp;
	sl_servicegrp_t *sgrp;
	uint_t i, retrycnt = 0;
	nodeid_t maxnodeid;

	//
	// Pre-initialize our out parameter
	//
	fwdinfop = (network::fwdseq_t *)NULL;

	//
	// Allocate our out parameter ...
	//
	fwdp = new (os::NO_SLEEP) network::fwdseq_t(NODEID_MAX);
	if (fwdp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		return;
	}
	network::fwdseq_t& fwdinfo = *fwdp;
	fwdinfo.length(NODEID_MAX);

	//
	// Initialize our cid sequences (empty)
	//
	for (i = 0; i < NODEID_MAX; i++) {
		fwdinfo[i].fw_cidseq.length(0);
		fwdinfo[i].fw_node = 0;
		fwdinfo[i].fw_ncid = 0;
	}

	//
	// cntseq is a temporary integer sequence used to hold the forwarding
	// list counts that are needed to size the fwdp fw_cidseq
	// sequences.
	//
	cntseqp = new (os::NO_SLEEP) network::seqlong_t(NODEID_MAX);
	if (cntseqp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		delete fwdp;
		return;
	}
	network::seqlong_t& cntseq = *cntseqp;
	cntseq.length(NODEID_MAX);

	//
	// Acquire read lock on sl_servicelist to prevent
	// the contents from changing.
	//
	sl_servicegrplist_lock.rdlock();

retry:
	if ((sgrp = find_sl_servicegrp(gname)) == NULL) {
		sl_servicegrplist_lock.unlock();
		e.exception(new sol::op_e(ENOENT));
		delete fwdp;
		delete cntseqp;
		return;
	}

	//
	// Find out how long the cid sequences need to be
	//
	for (i = 0; i < NODEID_MAX; i++) {
		cntseq[i] = 0;
	}
	sgrp->sl_pre_marshal_fwd(cntseqp, node);

	//
	// Drop the lock and resize our cid sequences
	//
	sl_servicegrplist_lock.unlock();
	for (i = 0; i < NODEID_MAX; i++) {
		//
		// Only INCREASE the length of the sequences.
		//
		if (fwdinfo[i].fw_cidseq.length() < cntseq[i]) {
			fwdinfo[i].fw_cidseq.length(cntseq[i]);
		}
	}

	//
	// Reacquire the lock, make sure the group still exists,
	// we can then marshall our forwarding information.
	//
	sl_servicegrplist_lock.rdlock();

	if ((sgrp = find_sl_servicegrp(gname)) == NULL) {
		sl_servicegrplist_lock.unlock();
		e.exception(new sol::op_e(ENOENT));
		delete fwdp;
		delete cntseqp;
		return;
	}
	sgrp->sl_marshal_fwd(fwdp, node, e);

	//
	// If we failed from EAGAIN, it was because the forwarding
	// lists became longer before we could marshal them.  Retry
	// sizing the lists and marshal again.
	//
	if (e.exception()) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op && (ex_op->error == EAGAIN)) {
			if (++retrycnt < GNS_MAX_RETRIES) {
				e.clear();
				goto retry;
			}
		}
		sl_servicegrplist_lock.unlock();
		delete fwdp;
		delete cntseqp;
		return;
	}

	sl_servicegrplist_lock.unlock();

	//
	// Now that we've finished, shrink the cid sequences down to
	// include only the forwarding information stored.
	//
	for (maxnodeid = 0, i = 0; i < NODEID_MAX; i++) {
		fwdinfo[i].fw_cidseq.length((uint_t)fwdinfo[i].fw_ncid);
		if ((fwdinfo[i].fw_ncid > 0) && (maxnodeid <= i))
			maxnodeid = i + 1;
	}
	fwdinfo.length(maxnodeid);

	delete cntseqp;
	fwdinfop = fwdp;
}

void
mcobj_impl_t::mc_gns_pdt(network::pdtinfo_t_out pdtinfop,
    const network::group_t& gname, Environment& e)
{
	network::pdtinfo_t *pdtp;
	sl_servicegrp_t *sgrp;

	//
	// Pre-initialize our out parameter
	//
	pdtinfop = (network::pdtinfo_t *)NULL;

	pdtp = new (os::NO_SLEEP) network::pdtinfo_t;
	if (pdtp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		return;
	}

	//
	// Acquire read lock on sl_servicelist to prevent
	// the contents from changing.
	//
	sl_servicegrplist_lock.rdlock();

	if ((sgrp = find_sl_servicegrp(gname)) == NULL) {
		sl_servicegrplist_lock.unlock();
		e.exception(new sol::op_e(ENOENT));
		delete pdtp;
		return;
	}

	sgrp->sl_marshal_pdt(pdtp, e);

	sl_servicegrplist_lock.unlock();

	if (e.exception()) {
		delete pdtp;
		return;
	}

	pdtinfop = pdtp;
}

//
//
void
mcobj_impl_t::mc_gns_service(network::grpinfoseq_t_out grpinfoseqp,
    const network::group_t& gname, Environment& e)
{
	network::grpinfoseq_t *grpp;
	network::seqlong_t sobjcnt;
	uint_t i, retrycnt = 0;
	sl_servicegrp_t *sgrp;
	uint_t grpcount = 1;

	//
	// Pre-initialize out our parameter
	//
	grpinfoseqp = (network::grpinfoseq_t *)NULL;

	//
	// Create the "grpinfoseqp" sequence for returning the service data.
	//
	grpp = new (os::NO_SLEEP) network::grpinfoseq_t;
	if (grpp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		return;
	}
	network::grpinfoseq_t& grpinfoseq = *grpp;

	//
	// Return here, without locks, if the servicegrp list length changes
	//
resize:

	//
	// We only have one service unless a null resource name is
	// passed ... in that case, find out how many service groups
	// are in the list, then drop locks and set our outparams
	// sequence length and temporary array.
	//
	// We have a separate serviceobj count sequence to contain
	// the service object counts for each service group.
	//
	if (*gname.resource == NULL) {
		sl_servicegrplist_lock.rdlock();
		grpcount = sl_servicegrplist.count();
		sl_servicegrplist_lock.unlock();
	} else {
		grpcount = 1;
	}

	grpinfoseq.length(grpcount);
	sobjcnt.length(grpcount);

	//
	// Now that we've set the group count, retrieve the number
	// of service objects in each group so that we can resize them.
	//
	sl_servicegrplist_lock.rdlock();

	if (*gname.resource == NULL) {
		//
		// Check to make sure that the number of groups
		// didn't change.
		//
		if (grpcount != sl_servicegrplist.count()) {
			sl_servicegrplist_lock.unlock();
			goto resize;
		}
		//
		// Count the number of service objects per group
		// so that we can size it after we've dropped locks.
		//
		sl_servicegrplist_t::ListIterator giter(sl_servicegrplist);

		for (i = 0; (sgrp = giter.get_current()) != NULL;
		    giter.advance()) {
			sobjcnt[i++] = sgrp->sl_serviceobj_list.count();
		}
	} else {
		sgrp = find_sl_servicegrp(gname);

		if (sgrp == NULL) {
			sl_servicegrplist_lock.unlock();
			e.exception(new sol::op_e(ENOENT));
			delete grpp;
			return;
		}
		sobjcnt[0] = sgrp->sl_serviceobj_list.count();
	}

	//
	// Drop locks and resize the serviceobj sequence in each
	// servicegrp.  We only make them longer here ... marshal_sgrp
	// will shorten them to reflect what really gets stored.
	//
	sl_servicegrplist_lock.unlock();

	if (*gname.resource == NULL) {
		for (i = 0; i < grpcount; i++) {
			if (grpinfoseq[i].srv_obj.length() < sobjcnt[i]) {
				grpinfoseq[i].srv_obj.length(sobjcnt[i]);
			}
		}
	} else {
		if (grpinfoseq[0].srv_obj.length() < sobjcnt[0]) {
			grpinfoseq[0].srv_obj.length(sobjcnt[0]);
		}
	}

	//
	// Now the grpinfoseq sequences are maxed out ... marshal
	// the service data into it, re-adjusting the sequence lengths
	// down if they're too big, and failing with EAGAIN if they
	// are too small.
	//

	//
	// Re-acquire our read lock on the maservicelist to prevent
	// the contents from changing.
	//
	sl_servicegrplist_lock.rdlock();

	//
	// A non-null resource name indicates that we want to see a
	// specific service, otherwise send back all services.
	//
	if (*gname.resource != NULL) {
		sgrp = find_sl_servicegrp(gname);

		if (sgrp == NULL) {
			sl_servicegrplist_lock.unlock();
			e.exception(new sol::op_e(ENOENT));
			delete grpp;
			return;
		}

		sgrp->sl_marshal_sgrp(&(grpinfoseq[0]), e);
		if (e.exception()) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			sl_servicegrplist_lock.unlock();

			//
			// The number of service objects changed
			// underneath us ... resize the sequences.
			//
			if (ex_op && (ex_op->error == EAGAIN)) {
				if (++retrycnt < GNS_MAX_RETRIES) {
					e.clear();
					goto resize;
				}
			}
			delete grpp;
			return;
		}

	} else {

		//
		// We're getting the entire list, check if the count changed
		// after we took it, if so, drop the lock and try again.
		//
		if (grpcount < sl_servicegrplist.count()) {
			sl_servicegrplist_lock.unlock();
			goto resize;
		}

		//
		// The grpinfoseq is allowed to be longer that needed,
		// we adjust it down to what we need.
		//
		ASSERT(grpcount >= sl_servicegrplist.count());
		grpinfoseq.length(grpcount);

		//
		// Iterate over the service group list.  For each service
		// group iterate over the list of service objects, extracting
		// relevant information from each object and inserting into
		// the grpinfoseqp sequence; this information will be used to
		// re-create the service group and its service objects.
		//
		sl_servicegrplist_t::ListIterator iter(sl_servicegrplist);

		for (i = 0; (sgrp = iter.get_current()) != NULL;
		    iter.advance()) {
			sgrp->sl_marshal_sgrp(&grpinfoseq[i++], e);
			if (e.exception()) {
				sol::op_e *ex_op =
				    sol::op_e::_exnarrow(e.exception());

				sl_servicegrplist_lock.unlock();
				if (ex_op && (ex_op->error == EAGAIN)) {
					if (++retrycnt < GNS_MAX_RETRIES) {
						e.clear();
						goto resize;
					}
				}
				delete grpp;
				return;
			}
		}
	}

	//
	// Release the locks.
	//
	sl_servicegrplist_lock.unlock();

	grpinfoseqp = grpp;
}

void
mcobj_impl_t::mc_gns_ht(network::seqlong_t_out vanillap,
    network::seqlong_t_out stickywildcardp,
    Environment& e)
{
	network::seqlong_t	*vanp, *stickyp;
	uint_t			size, i;

	//
	// Pre-initialize our out parameters
	//
	vanillap = (network::seqlong_t *)NULL;
	stickywildcardp = (network::seqlong_t *)NULL;

	//
	// Grab the number of buckets under the lock
	//
	sl_servicegrplist_lock.rdlock();
	size = vanilla_ht.get_number_of_buckets();

	//
	// Allocate/size the sequence OUTSIDE of the lock
	//
	sl_servicegrplist_lock.unlock();
	vanp = new (os::NO_SLEEP) network::seqlong_t(size);
	if (vanp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		return;
	}
	network::seqlong_t& vanilla = *vanp;
	vanilla.length(size);

	//
	// Populate the vanilla sequence under the lock
	//
	sl_servicegrplist_lock.rdlock();
	ASSERT(size == vanilla_ht.get_number_of_buckets());
	for (i = 0; i < size; i++) {
		vanilla[i] = vanilla_ht.get_number_chained(i);
	}

	//
	// Grab the number of buckets under the lock
	//
	size = stickywildcard_ht.get_number_of_buckets();

	//
	// Allocate/size the sequence OUTSIDE of the lock
	//
	sl_servicegrplist_lock.unlock();
	stickyp = new (os::NO_SLEEP) network::seqlong_t(size);
	if (stickyp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		delete vanp;
		return;
	}
	network::seqlong_t& stickywildcard = *stickyp;
	stickywildcard.length(size);

	//
	// Populate the stickwildcard sequence under the lock
	//
	sl_servicegrplist_lock.rdlock();
	ASSERT(size == stickywildcard_ht.get_number_of_buckets());
	for (i = 0; i < size; i++) {
		stickywildcard[i] = stickywildcard_ht.get_number_chained(i);
	}

	sl_servicegrplist_lock.unlock();

	vanillap = vanp;
	stickywildcardp = stickyp;
}

void
mcobj_impl_t::mc_setbind_table_entry(const network::fwd_t& fwdp, Environment& e)
{
	sl_servicegrp_t 	*servicep;
	network::service_sap_t  ssap;
	network::cid_t		cid;

	cid = fwdp.fw_cidseq[0];
	ssap.protocol = cid.ci_protocol;
	ssap.laddr = cid.ci_laddr;
	ssap.lport = cid.ci_lport;


	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		servicep->sl_create_proxy_bind_entry(fwdp);
	}
	sl_servicegrplist_lock.unlock();

}

int
mcobj_impl_t::is_generic_affinity(network::service_sap_t& ssap)
{
	sl_servicegrp_t 	*servicep;
	int			rcode = 0;

	sl_servicegrplist_lock.rdlock();

	servicep = mc_fastlookup_service((network::service_sap_t)ssap);
	if (servicep != NULL) {
		if (servicep->is_generic_affinity()) {
			rcode = 1;
		}
	}
	sl_servicegrplist_lock.unlock();
	return (rcode);
}

//
// Returns the state of the service identified by ssap
// on the remote node identified by nodeid.
// Refer to mc_setstate_binding for usage of this function
//
boolean_t
mcobj_impl_t::mc_node_registered(network::service_sap_t ssap,
    nodeid_t node)
{
	uint_t				len;
	uint_t				index = 0;
	const network::srvinfo_t	*srvinfop;
	network::srvinfoseq_t_var	srvinfoseqp;
	Environment			e;
	network::PDTServer_var		local_pdt_ref;


	local_pdt_ref = network_lib.get_pdt_ref();
	if (CORBA::is_nil(local_pdt_ref)) {
		return (B_FALSE);
	}

	local_pdt_ref->get_all_registered_services(srvinfoseqp, e);
	if (e.exception() != NULL) {
		e.exception()->print_exception(
		    "Error in get_all_registered_services()");
		e.clear();
		return (B_FALSE);
	}

	len = srvinfoseqp->length();
	while (index < len) {
		srvinfop = &srvinfoseqp[index];
		if (mc_sap_matches(srvinfop->srv_sap, ssap)) {
			int i;
			uint_t	instcount = srvinfop->srv_inst.length();
			for (i = 0; i < instcount; i++) {
				if (srvinfop->srv_inst[i] == node) {
					return (B_TRUE);
				}
			}
		}
		index++;
	}

	return (B_FALSE);
}

inline bool
mcobj_impl_t::mc_sap_matches(network::service_sap_t ssap,
network::service_sap_t nsap)
{
	return ((nsap.protocol == ssap.protocol) &&
	    (addr6_are_equal(nsap.laddr, ssap.laddr)) &&
	    (nsap.lport == ssap.lport));
}
