/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MCOBJ_IMPL_H
#define	_MCOBJ_IMPL_H

#pragma ident	"@(#)mcobj_impl.h	1.87	08/09/07 SMI"

//
// This file defines the implementation class routines that
// corresponds to the Service Access Point Server interface
// (interface network::SAPServer) described in
// interfaces/network.idl.
//

#include <sys/os.h>
#include <sys/threadpool.h>
#include <sys/hashtable.h>
#include <h/sol.h>
#include <h/network.h>
#include <orb/object/adapter.h>
#include <cl_net/sl_service.h>
#include <cl_net/muxq_impl.h>

#ifdef SCTP_SUPPORT
#include <cl_net/conn_table.h>

//
// The number of times we retry allocating a
// open_conn instance before failing.
//
#define	OPEN_CONN_RETRIES 5
#endif  //  SCTP_SUPPORT

//
// The number of times we retry allocating a
// close_conn instance before failing.
//
#define	CLOSE_CONN_RETRIES 5

//
// We use this value to indicate that the dispatcher
// thread has exited.
//
#define	PDTS_DISP_EXIT (-1)
#define	PDTS_DISP_RUNNING (1)
#define	PDTS_DISP_NONE (0)

class close_conn : public defer_task {
public:
	close_conn(network::cid_t& c, nodeid_t node)
	    {cid = c; gifnode = node; }
	~close_conn() {}
	void execute();
private:
	network::cid_t cid;
	nodeid_t gifnode;
};

#ifdef SCTP_SUPPORT
class open_conn : public defer_task {
public:
	open_conn(cl_sctp_handle_t phandle, sa_family_t paddr_family,
	    network::service_sap_t& pssap,
	    uint_t pnfaddrs, uint8_t *pfaddrp, in_port_t pfport,
	    nodeid_t node, boolean_t passoc_ch, size_t psize) {
		handle = phandle;
		addr_family = paddr_family;
		ssap = pssap;
		nfaddrs = pnfaddrs;
		faddrp = pfaddrp;
		fport = pfport;
		gifnode = node;
		assoc_ch = passoc_ch;
		size = psize;
	}
	~open_conn() {}
	void execute();
private:
	cl_sctp_handle_t handle;
	sa_family_t addr_family;
	network::service_sap_t ssap;
	uint_t nfaddrs;
	uint8_t *faddrp;
	in_port_t fport;
	nodeid_t gifnode;
	boolean_t assoc_ch;
	size_t size;
};

class open_bconn : public defer_task {
public:
	open_bconn(cl_sctp_handle_t phandle, sa_family_t paddr_family,
	    network::service_sap_t& pssap,
	    uint_t pnfaddrs, uint8_t *pfaddrp, in_port_t pfport,
	    nodeid_t node, boolean_t passoc_ch, size_t psize) {
		handle = phandle;
		addr_family = paddr_family;
		ssap = pssap;
		nfaddrs = pnfaddrs;
		faddrp = pfaddrp;
		fport = pfport;
		gifnode = node;
		assoc_ch = passoc_ch;
		size = psize;
	}
	~open_bconn() {}
	void execute();
private:
	cl_sctp_handle_t handle;
	sa_family_t addr_family;
	network::service_sap_t ssap;
	uint8_t nfaddrs;
	uint8_t *faddrp;
	in_port_t fport;
	nodeid_t gifnode;
	boolean_t assoc_ch;
	size_t size;
};
#endif  // SCTP_SUPPORT

//
// An object of this class represents the abstract object for the
// <protocol, ip address> pair.  This object is a key in the fast
// lookup mechanism using a hash table.  This object is allocated
// from the heap. It must be freed when the hash table is destroyed.
//
class ourproto_ipaddr_t {
public:
	network::inaddr_t laddr;

	ourproto_ipaddr_t() {}
	ourproto_ipaddr_t(network::inaddr_t ipaddr)
	{ laddr = ipaddr; }

	~ourproto_ipaddr_t() {}

	ourproto_ipaddr_t(ourproto_ipaddr_t const &rhs);
	ourproto_ipaddr_t &operator = (ourproto_ipaddr_t &rhs);
private:
};

class mcobj_impl_t: public McServerof<network::mcobj> {

public:
	typedef enum {
		NONE = 0,
		LISTEN,
		BIND,
		CONNECT,
		UNLISTEN,
		UNBIND,
		DISCONNECT
	} mcop_t;

	void _unreferenced(unref_t);

	// Constructor.
	mcobj_impl_t();

	// Destructor.
	~mcobj_impl_t();

	// Copy constructor.
	mcobj_impl_t(const mcobj_impl_t &);
	// Assignment operator.
	mcobj_impl_t &operator = (mcobj_impl_t &);

	int init(network::PDTServer_ptr pdt_server);

	network::mcobj_ptr getref() { return (get_objref()); }

	void putmsg(streams::msg_ptr, Environment& e);
	muxq_impl_t *set_muxq();

	void mc_register_a_service(const network::srvinfo_t& srvinfo,
	    Environment& e);

	void mc_register_all_services(Environment&);

	void mc_deregister_services(const network::service_sap_t& ssap,
	    Environment& e);

	void mc_register_grp(const network::group_t &g_name, uint32_t nhash,
	    network::lb_specifier_t    policy, Environment &e);

	void mc_deregister_grp(const network::group_t &g_name,
	    Environment &e);

	void mc_config_sticky_grp(const network::group_t &g_name,
	    const network::sticky_config_t &, Environment &);

	void mc_detach_gifnode(const network::service_sap_t& ssap,
	    Environment &e);

	void mc_set_gifnode(const network::service_sap_t& ssap,
	    nodeid_t node, Environment &e);

	int32_t mc_pdte_conninfo(const network::service_sap_t& ssap,
	    nodeid_t gifnode, const network::hashinfo_t& hashinfo,
	    network::fwd_t_out fwdp, uint_t flag, Environment &e);

	void mc_add_frwd_info(const network::service_sap_t& ssap,
	    const network::fwd_t& fwdp, uint_t flag, Environment& e);

	void mc_clean_frwd_info(nodeid_t node,
	    const network::service_sap_t& ssap, Environment& e);

	void mc_stop_forwarding(const network::cid_t& cid,
	    nodeid_t node, Environment& e);

	void mc_setstate_conn(const network::cid_t& cid,
	    network::conn_state_t state, Environment&);

	void mc_setbind_table_entry(const network::fwd_t& fwdp, Environment&);

	boolean_t mc_node_registered(network::service_sap_t ssap,
	    nodeid_t node);

	inline bool mc_sap_matches(network::service_sap_t nsap,
	    network::service_sap_t ssap);

	//
	// Proxy side routines for binding and forwarding entries
	//

	// Connection close handler routine
	void mc_closeconn(network::service_sap_t& ssap, network::cid_t& cid);

	// Connection open handler routine
	void mc_openconn(network::service_sap_t& ssap, network::cid_t& cid);

#ifdef SCTP_SUPPORT
	// Connection (with multi-endpoint) open handler routine;
	void mc_multi_openconn(cl_sctp_handle_t handle, sa_family_t addr_family,
	    network::service_sap_t& ssap, uint_t nfaddrs,
	    uint8_t *faddrp, in_port_t fport, boolean_t can_block,
	    boolean_t assoc_ch, size_t size);

	void mc_multi_closeconn(network::service_sap_t& ssap, uint_t nfaddrs,
	    uint8_t *faddrp, in_port_t fport, cl_sctp_handle_t handle,
	    boolean_t assoc_ch);

	// Manipulate SCTP address list during binding
	bool list_addr_against_port(in_port_t port, uint8_t **addrp,
	    uint_t *naddrs, size_t size, int *action, int *wildcard);
#endif  // SCTP_SUPPORT

	// Get a list of binding entries (during GIF failover)
	void mc_get_bindinfo(const network::service_sap_t& ssap,
	    nodeid_t new_gifnode,  network::bind_t_out bindp,
	    Environment &e);

	//
	// GIF side routines for binding and forwarding entries
	//

	// Add a list of binding entries (during GIF failovers)
	void mc_add_bindinfo(const network::service_sap_t& ssap,
	    const network::bind_t& bindp, uint_t iter, Environment& e);

	void mc_add_pernode_lst(const network::service_sap_t& ssap,
	    uint_t iter, Environment& e);

	// Remove all binding entries for a crashed proxy node
	void mc_clean_bindinfo(nodeid_t node,
	    const network::service_sap_t& ssap, Environment& e);

	// Set state of the specified binding entry
	void mc_setstate_binding(const network::inaddr_t& faddr,
	    const network::service_sap_t&, const nodeid_t node,
	    network::bind_state_t state,
	    Environment&);

	void mc_init_pdt(const network::pdtinfo_t& pdtinfo, Environment& e);

	void mc_init_complete(const network::service_sap_t& ssap,
	    Environment& e);

	//
	// INTERNAL METHODS
	//
	//
	// internal method to check if a service is registered as
	// a replicated service
	//
	int isregistered(network::service_sap_t& ssap);

	//
	// internal service to check if a service has generic affinity
	// enabled
	//
	int is_generic_affinity(network::service_sap_t& ssap);

	//
	// Determine if an address is a scalable address. Return true if
	// the address is part of a scalable service.
	// Hashed lookup is performed.
	//
	bool is_scalable_address(const network::inaddr_t& dstaddr);

	//
	// internal method to find a service given the service sap
	//
	sl_servicegrp_t *mc_lookup(network::service_sap_t& ssap);
	sl_servicegrp_t *mc_match(network::service_sap_t& ssap);

	sl_servicegrp_t *find_sl_servicegrp(const network::group_t &group);

	void mc_attach_mcobj(nodeid_t node, network::mcobj_ptr mcobjref,
	    Environment& e);

	void mc_attach_mcobjs(const network::mcobjs_t &mcobjsref,
	    Environment& e);

	//
	// mc_checknset is used by our tcp hooks to register an
	// instance bind/unbind/listen/unlisten.  mc_setserviceflag
	// is used internally to queue the event so that it can
	// be processed by the pdts_disp (mc_locate_work) thread later.
	//
	bool mc_checknset(network::service_sap_t& ssap, mcop_t flag);
	void mc_setserviceflag(sl_servicegrp_t *servicep,
	    network::service_sap_t& ssap, mcop_t flag);

	void mc_locate_work();

	//
	// Our dispatcher thread handle.  This thread is spawned by our
	// mcobj_impl_t constructor.
	//
	// We keep a state variable around to tell us if the thread is
	// running or not so we can shut it down properly in the destructor.
	//
#ifdef _KERNEL
	klwp_id_t disp_tid;
#else
	thread_t disp_tid;	// different thread id type for unode
#endif
	int disp_state;

	//
	// Global pointer to serviceobj's posted for instance processing
	// (protected by mc_mutex/mc_cv).  This list needs to be accessable
	// from the serviceobj destructor in order to make sure the obj is
	// removed if/when the serviceobj is destroyed.
	//
	// sl_post is appended in interrupt context, so the non-blocking
	// version of the list is used here. This list uses pointers in
	// the sl_serviceobj object itself, which does not conflict with
	// sl_servicegrp_t's list, which is a DList, which does not use
	// those pointers in the sl_serviceobj objects.
	//
	sl_serviceobjlist_intr_t	sl_post;
	os::condvar_t		sl_post_cv;	// synch with pdts_disp thread
	os::mutex_t		sl_post_mutex;	// lock to go with above.

#ifdef SCTP_SUPPORT
	// Connection table
	conn_table_t *mc_conntab;
#endif

	muxq_impl_t *get_muxq();

	sl_serviceobj_t *
	find_sl_serviceobj(sl_serviceobjlist_t &slsobjlist,
	    const network::service_sap_t &sap);

	// Lookup the [sap, servicegrpp] tuple in the fast lookup
	// tables and returns a pointer to the servicegrp object.
	sl_servicegrp_t*
	mc_fastlookup_service(network::service_sap_t& ssap);

	// Insert the [sap, servicegrpp] tuple into the fast lookup tables.
	void mc_insert_service_fastlookup(const network::service_sap_t& ssap,
	    network::lb_specifier_t policy, sl_servicegrp_t *servicegrpp,
	    ourproto_ipaddr_t *sticky_wc_key);

	// Remove the [sap, servicegrpp] tuple from the fast lookup tables.
	void mc_remove_service_fastlookup(const network::service_sap_t& ssap,
	    network::lb_specifier_t policy);

	network::mcobj_ptr mc_node_to_mcobj(nodeid_t node);

	void mc_config_rr_grp(const network::group_t &g_name,
	    const network::rr_config_t &, Environment &);

	// Slave service group linked list.
	sl_servicegrplist_t	sl_servicegrplist;

	// Read/write lock for slave service group list. Serializes access.
	os::rwlock_t		sl_servicegrplist_lock;

	// Hash table for fast lookup of ordinary services.
	hashtable_t<sl_servicegrp_t *, network::service_sap_t *> vanilla_ht;

	// Hash table for fast lookup of sticky wildcard services only.
	hashtable_t<sl_servicegrp_t *, ourproto_ipaddr_t *> stickywildcard_ht;

	// Hash table for fast lookup to determine if an address is scalable
	hashtable_t<sl_servicegrp_t *, ourproto_ipaddr_t *> address_ht;


	//
	// Debugging methods
	//
	void mc_gns_bind(const network::group_t& gname, bool as_gif,
	    network::gns_bindseq_t_out bindseq, int32_t &st_timeout,
	    Environment& e);
	void mc_gns_service(network::grpinfoseq_t_out srvinfoseqp,
	    const network::group_t& gname, Environment& e);
	void mc_gns_fwd(network::fwdseq_t_out fwdinfop, nodeid_t node,
	    const network::group_t& gname, Environment& e);
	void mc_gns_pdt(network::pdtinfo_t_out pdtinfop,
	    const network::group_t& gname, Environment& e);
	void mc_gns_ht(network::seqlong_t_out vanilla,
	    network::seqlong_t_out stickywildcard,
	    Environment& e);

private:
	// Insert address of ssap to scalable address table (address_ht).
	// The service group containing ssap should be passed in.
	void mc_insert_service_address_ht(const network::service_sap_t& ssap,
	    sl_servicegrp_t *servicegrpp);

	// Remove address of ssap from scalable address table (address_ht).
	// It is only removed when no service of this and all other
	// service groups uses it.
	void mc_remove_service_address_ht(const network::service_sap_t& ssap);

#ifdef SCTP_SUPPORT
	void mc_create_conn_table();
#endif

	//
	// To ensure that only one thread creates the muxq object
	// in set_muxq()
	//
	muxq_impl_t	*mcobj_muxq;
	os::mutex_t	muxq_create_lock;
	network::mcobjs_t	*mcobjs;
};

#endif	/* _MCOBJ_IMPL_H */
