/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MA_PDT_H
#define	_MA_PDT_H

#pragma ident	"@(#)ma_pdt.h	1.41	08/05/20 SMI"

//
// ma_pdt - Master PDT maintained in the PDT server
//

#include <h/network.h>
#include <sys/os.h>

#include <cl_net/netlib.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/ma_service.h>
#include <cl_net/netlib.h>
#include <nslib/ns.h>
#include <netinet/in.h>

class ma_servicegrp;

class ma_pdt: public pdt {

public:
	ma_pdt(uint_t nhashent);
	~ma_pdt();

	unsigned int dump_mapdt();

	//
	// Collect forwarding information for the new GIN
	//
	int ma_new_gifnode(ma_servicegrp *servicegrp, uint_t gifnode,
	    const network::service_sap_t& ssap);

	//
	// Remove forwarding info from pdt for this node.
	// Called from deregister_proxynode.
	//
	void ma_clean_gifnode(nodeid_t node, nodeid_t gifnode,
	    ma_servicegrp *, const network::service_sap_t& ssap);

	//
	// Collect forwarding info only for this node.
	// Called from deregister_instance
	//
	void ma_collect_only(nodeid_t node, nodeid_t gifnode,
	    ma_servicegrp *, const network::service_sap_t& ssap);

	//
	// Initialize pdt on specified node
	//
	int ma_init_pdt(nodeid_t node, uint32_t nhash, bool rr_enabled,
	    network::group_t &group, const network::dist_info_t&,
	    Environment& _environment);

	//
	// Mark the PDT initialization as complete on the gifnode.
	//
	void ma_init_complete(nodeid_t gifnode,
	    const network::service_sap_t& ssap);

#ifdef DEBUG
	void get_pdt_summary(uint_t *comp_pdt);
#endif

private:
	void marshal_rrinfo(const network::dist_info_t&,
		network::pdtinfo_t&);

	// We do not allow operator overloading.
	// Disallow assignments and call by value.
	ma_pdt(const ma_pdt &);
	ma_pdt &operator = (ma_pdt &);
};

#endif	/* _MA_PDT_H */
