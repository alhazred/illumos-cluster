/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CONN_TABLE_H
#define	_CONN_TABLE_H

#pragma ident	"@(#)conn_table.h	1.3	08/05/20 SMI"

#include <h/network.h>
#include <sys/os.h>

#undef nil // <inet/common.h> redefines nil

#include <netinet/in.h>
#include <inet/common.h>
#include <netinet/ip6.h>
#include <inet/ip.h>
#include <inet/sctp_itf.h>

typedef uintptr_t cl_sctp_handle_t;

class conn_entry_t : public _DList::ListElem {
public:
	conn_entry_t(cl_sctp_handle_t handle, sa_family_t addr_family,
	    network::service_sap_t& ssap, uint8_t nfaddrs,
	    uint8_t *faddrp, in_port_t fport);

	const char * to_string();
	void append_addr(uint8_t *addrp, uint_t naddrs);
	void deduct_addr(uint8_t *addrp, uint_t naddrs);

	cl_sctp_handle_t cn_handle;
	sa_family_t cn_addr_family;
	network::service_sap_t cn_ssap;
	uint_t cn_nfaddrs;
	uint8_t *cn_faddrp;
	in_port_t cn_fport;
};
typedef IntrList<conn_entry_t, _DList> conn_list_t;

class conn_table_t {
public:
	conn_table_t(uint_t hashsize);
	~conn_table_t();

	void list_rdlock(cl_sctp_handle_t handle);
	void list_wrlock(cl_sctp_handle_t handle);
	void list_unlock(cl_sctp_handle_t handle);
	conn_entry_t * lookup_entry(cl_sctp_handle_t handle);
	bool add_entry(cl_sctp_handle_t handle, sa_family_t addr_family,
	    network::service_sap_t& ssap, uint8_t nfaddrs,
	    uint8_t *faddrp, in_port_t fport);
	void remove_entry(cl_sctp_handle_t handle);
	void clean_all();

	uint_t			conn_hashsize;
	conn_list_t		*conn_list;
	os::rwlock_t		*conn_list_lock;
private:

	//
	// We dont allow the operators to be overloaded
	// Disallow assignments and pass by value
	//
	conn_table_t(const conn_table_t&);
	conn_table_t& operator = (conn_table_t&);

};

#include <cl_net/conn_table_in.h>

#endif	/* _CONN_TABLE_H */
