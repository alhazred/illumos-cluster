//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)lbobj.cc	1.76	08/05/20 SMI"

//
// Load Balance Object implementation
//

#include <h/network.h>
#include <sys/os.h>
#include <repl/service/replica_tmpl.h>
#include <cl_net/netlib.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/perf_policy.h>
#include <cl_net/lbobj.h>
#include <cl_net/errprint.h>
#include <nslib/ns.h>
#ifdef DEBUG
#include <sys/mc_probe.h>
#endif
#include <orb/fault/fault_injection.h>
#include <sys/clconf_int.h>

#include <sys/disp.h>

//
// Synchronization with other PDT Service methods *must* be done. The load
// balancer object holds a pointer to a ma_servicegrp. But to access this
// pointer properly, the ma_servicegrplist_lock in pdts_impl_t must be
// held. We achieve this by asking the provider for a pointer to the local
// pdts_impl_t (since the provider has the truth about this pointer) and
// then grab the lock from there. This synchronization is needed only for
// IDL methods exported that might come in conflict with other PDTServer
// methods.
//

// Static variables defined in lbobj.h

// Lock to protect the global lblist.
os::mutex_t	lbobj::listlock;

// Global list of lbobjs.
lbobj::lbobj_list_t	lbobj::lblist;

//
// Function to return the local network::PDTServer implementation.
// The network::lbobj IDL methods use this in order to acquire a lock that
// other methods in network::PDTServer uses.
//
// This function should only be called from the primary. Although it can
// work on secondaries, it is currently meant for primary use only.
//
static pdts_impl_t *
get_primary_pdtsp()
{
	pdts_impl_t *pdtsp;

	// mc_pdts_prov is initialized at mcnet load time.
	ASSERT(mc_pdts_prov != NULL);

	// This function is for use on the primary only
	ASSERT(mc_pdts_prov->is_primary());

	pdtsp = mc_pdts_prov->get_pdtsp();

	// Every node has a copy of the PDTServer, even on secondaries
	ASSERT(pdtsp != NULL);
	return (pdtsp);
}

//
// The factory returns a load balancer with the specified policy for the
// specified ma_servicegrp.
//
// The factory may be called to create either a primary or secondary, since
// the load balancer is HA.  On the secondary, a reference to the primary is
// passed in through primary_lb.
//
// Effects:
//	A new load balancer is created and added to lblist.
// Returns:
//	Returns pointer to the lbobj if successful.
//	Returns NULL if there is an error.
// Locks: acquires listlock.  Datalock indirectly acquired.
//
class lbobj *
lbobj::lb_factory(ma_servicegrp *serviceptr,
    const network::lb_specifier_t &lb_policy,
    network::lbobj_i_ptr primary_lb)
{
	bool okay = false;
	lbobj *lbptr = NULL;

	// Create a new load balancer of the appropriate type.
	// Sticky policies currently use the LB_WEIGHTED policy.
	if (lb_policy == network::LB_WEIGHTED || is_sticky(lb_policy)) {
		if (CORBA::is_nil(primary_lb)) {
			// Primary constructor
			lbptr = new weighted_lbobj();
		} else {
			// Secondary constructor
			network::weighted_lbobj_var nlb = network::
			    weighted_lbobj::_narrow(primary_lb);
			ASSERT(!CORBA::is_nil(nlb));
			lbptr = new weighted_lbobj(nlb);
		}
		okay = true;
	} else if (lb_policy == network::LB_USER) {
		if (CORBA::is_nil(primary_lb)) {
			// Primary constructor
			lbptr = new user_lbobj();
		} else {
			// Secondary constructor
			network::user_policy_var nlb = network::
			    user_policy::_narrow(primary_lb);
			ASSERT(!CORBA::is_nil(nlb));
			lbptr = new user_lbobj(nlb);
		}
		okay = true;
	} else if (lb_policy == network::LB_PERF) {
		if (CORBA::is_nil(primary_lb)) {
			// Primary constructor
			lbptr = new perf_policy(okay);
		} else {
			// Secondary constructor
			network::perf_mon_policy_var nlb = network::
			    perf_mon_policy::_narrow(primary_lb);
			ASSERT(!CORBA::is_nil(nlb));
			lbptr = new perf_policy(okay, nlb);
		}
	} else {
		// No valid policy specified
		lbptr = NULL;
		okay = false;
	}

	if (!okay) {
		// Invalid policy or constructor failed.
		cmn_err(CE_WARN, "Incorrect load-balancing policy\n");
		listlock.unlock();
		delete lbptr;
		return (NULL);
	}

	// At this point, a new load balancer has been successfully
	// created.

	lbptr->set_lb_info(serviceptr, lb_policy);
	// Add the load balancer to the list
	listlock.lock();
	lblist.append(lbptr);

	listlock.unlock();

	return (lbptr);
}

//
// Effects: Returns the implementation pointer from the CORBA object
//   reference "ref."  This is used by the checkpoint to get
//   the implementation pointer.
//
lbobj *
lbobj::get_impl(network::lbobj_i_ptr ref)
{
	return (lbobj *)ref->_handler()->get_cookie();
}

// Constructor for the load balancer base class.
lbobj::lbobj() : servicegrp(NULL), policy((network::lb_policy_t) 0)
{
	// Above are filled in by factory; initialized here just
	// to make lint happy.
}

//
// Effects:  This method sets the private variables to the arguments
//   "serviceptr" and policy key "policy_in."  It is an accessor
//   function used by the factory.  Access is serialized by a datalock.
//   Returns nothing.
//
void
lbobj::set_lb_info(ma_servicegrp *serviceptr,
	network::lb_specifier_t policy_in)
{
	datalock.lock();
	policy = policy_in;
	servicegrp = serviceptr;
	datalock.unlock();
}

//
// Effects:  This method releases the lbobj only when the
//   ma_servicegrp is done with it.  Releases means that it is merely
//   erased from the global list "lblist" but not destroyed.
//   The lbobj will be removed when unreferenced is called.
//   Returns nothing.  Access to the global list must be serialized.
//
void
lbobj::release_lbobj()
{
	//
	// SCMSGS
	// @explanation
	// This message indicates that the service group has been deleted.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		"load balancer for group '%s' released",
		servicegrp->get_group_name());
	listlock.lock();
	datalock.lock();
	servicegrp = NULL;
	(void) lblist.erase(this);
	datalock.unlock();
	listlock.unlock();
}

// Destructor for the base lbobj class.  Cleans up storage.
lbobj::~lbobj()
{
	ASSERT(servicegrp == NULL);
	//
	// SCMSGS
	// @explanation
	// This message indicates that the service group has been deleted.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
		"load balancer deleted");

	//
	// non-DEBUG lint wants to see servicegrp set to NULL here,
	// but it was by release_lbobj
	//
} //lint !e1740

//
// Effects:  Returns a pointer to a copy of the current load
//   distribution known to this object.  Typically it will be
//   called by a User Interface to get the current distribution.
//   Note that it is a shared implementation of the get_distribution code.
// Correctness:  Datalock must be acquired to serialize this method.
// Parameters:
//   locked (IN):  If true then the lock is already held, else lock
//		   is not held and must be acquired.
//   e (IN):	environment of call.
//
network::dist_info_t *
lbobj::get_distribution_ii(bool locked, Environment &e)
{
	if (!locked) {
		datalock.lock();
	}
	network::dist_info_t *distp = NULL;
	ASSERT(datalock.lock_held());
	if (servicegrp == NULL) {
		e.exception(new sol::op_e(EINVAL));
	} else {
		distp = new network::dist_info_t(*servicegrp->get_distribution(
		    e));
	}
	if (!locked) {
		datalock.unlock();
	}
	return (distp);
}

//
// Effects: Sets the current load distribution to the argument "dist"
//   in the master service group object. If this object is the primary
//   "this_lb" then it checkpoints the object using "ckptp" to the
//   secondaries and distributes the changes to the slaves.
//   Note that this is a shared implementation of the set_distribution code.
//   Called by the UI to set the current distribution.  Method must
//   be serialized with other load-balancing methods.
// Exceptions:
//   EINVAL -  if the "dist.weights[node]" is way too big.
//   weighted_lbobj::state_changed - if the new state of the load-balancing
//	weights is out of date with respect to the gifs or instances
//	after updating the weights.
// Parameters:
//	dist (IN):	new distribution weights
//	ckptp (IN):	checkpointing object
//	this_lb (IN):	pointer to lbobj (primary or secondary)
//	locked (IN):	true if datalock already held, else false and
//			must be acquired.
//
void
lbobj::set_distribution_ii(const network::dist_info_t &dist,
    network::ckpt_ptr ckptp, network::lbobj_i_ptr this_lb,
    bool locked, Environment &e)
{
	bool		state_changed = false;
	nodeid_t	nid;
	instanceobj	*instancep;

	ASSERT(this);

	// Check length of "dist" argument array.
	if (dist.weights.length() > NODEID_MAX + 1) {
		e.exception(new sol::op_e(EINVAL));
		return;
	}

	if (!locked) {
		datalock.lock();
	}

	ASSERT(datalock.lock_held());

	// Set the distribution in the master.
	servicegrp->set_distribution1(dist, e);

	if (e.exception()) {	// Exception was raised.
		if (sol::op_e::_exnarrow(e.exception())) {
			state_changed = true;
			e.clear();
		} else {
			e.exception()->print_exception(
			    "set_distribution");
		}
	}

	//
	// Print out to the console and syslog the weights for all nodes
	// in the config list
	//
	if (!e.exception()) {	// No exceptions.
		//
		// SCMSGS
		// @explanation
		// The load balancer is setting the distribution for the
		// specified service group.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, STATE_CHANGED,
		    "Load balancer setting distribution on %s:",
		    servicegrp->get_group_name());

		char nodename[NODENAME_MAX];

		//
		// Get a copy of the config list.
		// Iterate over this list, and print out the weights
		// only for those nodes in the config list, whether
		// the instance is up or not.
		//
		instancelist_t *clist = servicegrp->get_clist();

		for (clist->atfirst();
		    (instancep = clist->get_current()) != NULL;
		    clist->advance()) {

			nid = instancep->get_instanceid();
			network_lib.get_nodename_from_nodeid(
				(nodeid_t)nid, nodename);

			//
			// SCMSGS
			// @explanation
			// The load balancer set the specfied weight for the
			// specified node.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE,
			    STATE_CHANGED, "Node %s: weight %d",
			    nodename,
			    dist.weights[nid]);
		} // end for

		// Delete the elements of the instance list.
		clist->dispose();
		// Now delete the config list altogether.
		delete clist;

	} // end if (!e.exception())


	FAULTPT_NET(FAULTNUM_NET_SET_DIST_B, FaultFunctions::generic);

	//
	// If this is a primary, checkpoint the change and do the
	// second part of set_distribution1.
	//
	if (!CORBA::is_nil(ckptp) && !e.exception()) {
		ckptp->ckpt_set_distribution(this_lb, dist, e);
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_SET_DIST_A, FaultFunctions::generic);

		Environment env;
		servicegrp->set_distribution2(env);
		if (env.exception()) {
			env.exception()->print_exception(
			    "set_distribution2");
		}
		env.clear();
	}

	if (!locked) {
		datalock.unlock();
	}

	if (state_changed) {
		e.exception(new network::
		    weighted_lbobj::state_changed());
	}

	FAULTPT_NET(FAULTNUM_NET_SET_DIST_E, FaultFunctions::generic);
}

//
// The following methods are used to control state of the HA load balancer.
// These methods loop across all load balancers, calling the appropriate
// method on each load balancer.
// The key tasks are to start/stop balancer threads as appropriate and
// to dump parameters to any new secondaries.
// For load balancers that use a periodic balancing thread, the thread should
// only run on the active primary, so they should be
// started by unfreeze_primary and stopped by freeze_primary.
//

//
// PDTS is being checkpointed to a secondary.
// Effect: calls add_secondary on each load balancer.
// Locks: locks listlock.  May lock datalock indirectly.
//
void
lbobj::add_secondary_all(network::ckpt_ptr ckpt, Environment &e)
{
	lbobj *lbptr = NULL;
	listlock.lock();
	lbobj_list_t::ListIterator iter(lblist);
	for (; (lbptr = iter.get_current()) != NULL; iter.advance()) {
		lbptr->add_secondary(ckpt, e);
		e.clear();
	}
	listlock.unlock();
}

//
// PDTS is being frozen.
// Effect: calls freeze_primary on each load balancer.  Will stop threads.
// Locks: locks listlock.  May lock datalock indirectly.
//
void
lbobj::freeze_primary_all(Environment &e)
{
	lbobj *lbptr = NULL;

	listlock.lock();
	lbobj_list_t::ListIterator iter(lblist);
	for (; (lbptr = iter.get_current()) != NULL; iter.advance()) {
		lbptr->freeze_primary(e);
		e.clear();
	}
	listlock.unlock();
}

//
// PDTS is being unfrozen.
// Effect: calls unfreeze_primary on each load balancer.
// Start up balancing threads if necessay.
// Locks: locks listlock.  May lock datalock indirectly.
//
void
lbobj::unfreeze_primary_all(Environment &e)
{
	lbobj *lbptr = NULL;

	listlock.lock();
	lbobj_list_t::ListIterator iter(lblist);
	for (; (lbptr = iter.get_current()) != NULL; iter.advance()) {
		lbptr->unfreeze_primary(e);
		e.clear();
	}
	listlock.unlock();
}

//
// ----------------------------------------------------------------
// Weighted Load Balancer
//
//

// Primary constructor
weighted_lbobj::weighted_lbobj() :
    mc_replica_of<network::weighted_lbobj>(
    pdts_impl_t::get_ha_provider())
{
	// Set the HA cookie to point to the lbobj_i, for use by
	// the checkpoint.

	//
	// _handler is a virtual function and when you call a virtual
	// function from a constructor you get the version for the
	// constructor's class, not any derived class.  By scoping the
	// call as this->_handler we make it clear we are using this
	// class's virtual function and let lint know too so it
	// doesn't complain.  In this case there isn't a _handler
	// method for this class or any class that inherits this
	// class, so the call is actually to mc_replica_of<>::_handler.
	//
	this->_handler()->set_cookie((lbobj *)this);
}

// Secondary constructor
weighted_lbobj::weighted_lbobj(network::weighted_lbobj_ptr obj) :
    mc_replica_of<network::weighted_lbobj>(obj)
{
	// Set the HA cookie to point to the lbobj_i, for use by
	// the checkpoint.

	this->_handler()->set_cookie((lbobj *)this);
}

weighted_lbobj::~weighted_lbobj()
{
}

//
// If we can guarantee that no new object references
// to this object will ever be handed out once the reference
// count drops to zero, then it suffices to just assert that
// fact here.  And then it's safe to destroy the object.
//
// Observation:
//    The master service group object always holds one reference to
// the lbobj implementation object as part of its state.  When the
// group is destroyed, the reference to the associated lbobj object
// is released (because the state variable is declared as a
// "smart" var pointer), which decrements the reference count.
// Moreover, the lbobj is also removed from the global lblist.
// But the lboj object is not destroyed and its storage freed
// until the count drops to 0 and the unreferenced method is called.
//    The key point to note is that once the lbobj's reference count
// drops to 0, a new reference can NEVER be handed out
// to this lbobj because the enclosing group object is used
// to access the lbobj.  But since the group is gone, the access
// method "get_lbobj()" on the PDT server will fail.  That method
// is serialized with other PDT server IDL invocations using
// appropriate locks, and these methods obey the same locking
// order.  A new object reference CANNOT be handed out
// while an lbobj implementation object is being destroyed.
//
// Consider different scenarios:
// 1. Via pdts->get_lbobj() a remote client gets a new reference
// to an lbobj using the gname to identify the service group.
// There are now 2 references to this lbobj.
// a). If someone else destroys the service group while the client
// still holds a reference the reference count just drops to 1.
// The remote client can no longer do anything useful with the
// lbobj because the group is needed to perform operations, but
// the group has been destroyed.  So the remote client should
// release his reference, dropping the ref count of the lbobj to 0.
// A new reference to this lbobj can never be handed out.
// Now we can destroy this object.
//
// b). If the remote client's reference is released first, then
// the ref count drops to 1. Then if the service group is
// destroyed, the ref count drops to 0, and again, the lbobj may
// be destroyed because the enclosing service group object is
// gone.
//
void
#ifdef DEBUG
weighted_lbobj::_unreferenced(unref_t cookie)
#else
weighted_lbobj::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));

	delete this;
}

//
// Called by the UI to set the current distribution
// Effects: changes distribution and on each ma_servicegrp and checkpoints
// the change.
// Locks: acquires datalock.
//
void
weighted_lbobj::set_distribution(const network::dist_info_t &dist,
    CORBA::Environment &e)
{
	pdts_impl_t *pdtsp = get_primary_pdtsp(); // Return only if non-NULL

	//
	// Serialize with other PDT methods that access ma_servicegrplist
	//
	pdtsp->ma_servicegrplist_lock_wrlock();

	datalock.lock();

	network::ckpt_ptr ckptp = get_checkpoint();
	network::lbobj_i_var this_lb = get_objref();
	set_distribution_ii(dist, ckptp, this_lb, true, e);

	datalock.unlock();

	pdtsp->ma_servicegrplist_lock_unlock();
}

//
// Effects: Helper routine for set_weight.
//   Set one weight "weight" in the load distribution array for
//   node "node."
// Parameters:
//  - node (IN):	node id of node whose weight is to be changed
//  - weight (IN):	new weight (legal values are non-negative < 100,000)
//  - locked (IN):	true if datalock already locked, false otherwise
//  - e (IN):		Environment variable for passing around exceptions.
// Exceptions:
//  - state_changed
//
// Call sequences:
// 1. weighted_lbobj::set_weight(...)		Called at primary
//	weighted_lbobj::set_weight1(...)
// 2. pdts_prov_impl::ckpt_set_weight(...)	Called by secondary
//	weighted_lbobj::set_weight1(...)
//
void
weighted_lbobj::set_weight1(nodeid_t node, uint32_t weight, bool locked,
    Environment &e)
{
	NET_DBG(("  weighted_lbobj::set_weight1(node=%d, weight=%d)\n",
		node, weight));

	if (!locked) {
		datalock.lock();
	}

	// Set the weight in the master service group for node "node."
	servicegrp->set_weight(node, weight, e);
	if (e.exception()) {
		if (sol::op_e::_exnarrow(e.exception())) {
			// raise exception
			e.exception(new network::
				    weighted_lbobj::state_changed());
		} else {
			e.exception()->print_exception(
			    "weighted_lbobj::set_weight1");
		}
	}

	if (!locked) {
		datalock.unlock();
	}
}

//
// Effects: Set one weight in the load distribution array for node "node."
// Parameters:
//  - node (IN):	node id of node whose weight is to be changed
//  - weight (IN):	new weight (legal values are non-negative < 100,000)
//  - e (IN):		Environment variable for passing around exceptions.
// Exceptions:
//  - EINVAL:		if service group backpointer is NULL, or if
//			"weight" >= 100,000.
//
// Call sequence:
//    lb_op (src/rgm/rgm_lb.cc)
//	weighted_lbobj->set_weight((unsigned int)node,
//	    (unsigned int)arg->op_data.op_data_val[1], e);
void
weighted_lbobj::set_weight(sol::nodeid_t node, uint32_t weight,
    Environment &e)
{
	pdts_impl_t *pdtsp = get_primary_pdtsp(); // Return only if non-NULL

	//
	// Changes weight on each ma_servicegrp and checkpoints the change.
	// Locks: acquires datalock.
	//
	NET_DBG(("weighted_lbobj::set_weight(node=%d, weight=%d)\n",
		node, weight));


	if (weight >= TOOBIG) {
		e.exception(new sol::op_e(EINVAL));
		return;
	}

	//
	// Serialize with other PDT methods that access ma_servicegrplist
	//
	pdtsp->ma_servicegrplist_lock_wrlock();

	//
	// Acquire lock to serialize execution of this method with other
	// lbobj methods.
	//
	datalock.lock();

	//
	// The back pointer to the master service group could be NULL
	// if release_lbobj() has already been called on lbobj by the
	// destructor for the service group.  Recall that release_lbobj()
	// just sets servicegrp to NULL and removes the lbobj from the
	// master lbobj list.
	// So if the pointer is NULL, then this lbobj is useless; there's
	// no associated master service group object.  Raise exception and
	// bail out.
	//
	if (servicegrp == NULL) {
		e.exception(new sol::op_e(EINVAL));
		datalock.unlock();
		pdtsp->ma_servicegrplist_lock_unlock();
		return;
	}

	//
	// Get the primary's transaction context from the environment.
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	lbobj_set_weight_state *saved_state;

	//
	// Extract the saved state from the context.
	//
	saved_state = (lbobj_set_weight_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		//
		// Step 1. Set the weight for node "node" in the associated
		// master service group.
		//
		set_weight1(node, weight, true, e);
		if (e.exception() != NULL) {
			// The load-balancing state changed.  Pass
			// exception back to caller.
			// Release datalock.
			datalock.unlock();
			pdtsp->ma_servicegrplist_lock_unlock();
			return;
		} else {
			FAULTPT_NET(FAULTNUM_NET_SET_WGHT_B,
				    FaultFunctions::generic);

			network::ckpt_ptr ckptp = get_checkpoint();
			network::weighted_lbobj_var this_lb = get_objref();
			//
			// Checkpoint the setting of the weight to the
			// secondary lbobj objects.
			//
			ckptp->ckpt_set_weight(this_lb, node, weight, e);
			e.clear();

			//
			// If the primary then fails after the checkpoint,
			// the new primary that takes over will have the
			// same state.
			//
			FAULTPT_NET(FAULTNUM_NET_SET_WGHT_A,
				    FaultFunctions::generic);
		} // end if (!e.exception())
	}

	//
	// Step 2. Send the changed PDT table for the service group
	// to the slave.
	// HA: Note that this routine may be invoked again during a retry
	// without ill effect because it is idempotent.
	// Note that while we hold the exclusive datalock, no one can
	// destroy the master service group object 'servicegrp.'
	//
	NET_DBG(("  calling servicegrp->ma_distribute_pdt(...)\n"));

	Environment env;
	servicegrp->ma_distribute_pdt(0, ma_servicegrp::COLLECT_ALL, env);

	//
	// Note that the exception is set only if gifnode dies when
	// ma_distribute_pdt is in progress. This is all right,
	// from this call's point of view.  And therefore, it's okay
	// to just clear it.
	//
	env.clear();

	// Release datalock.
	datalock.unlock();

	pdtsp->ma_servicegrplist_lock_unlock();

	FAULTPT_NET(FAULTNUM_NET_SET_WGHT_E, FaultFunctions::generic);
}


//
// Effects: Called by the UI to get the current distribution
// Locks: acquires datalock indirectly
//
network::dist_info_t *
weighted_lbobj::get_distribution(Environment &e)
{
	network::dist_info_t *dist = get_distribution_ii(false, e);
	return (dist);
}

//
// Effects: This method returns the current load-balancing policy.
//
network::lb_specifier_t
weighted_lbobj::get_policy(Environment &)
{
	network::lb_specifier_t policy_out;
	datalock.lock();
	policy_out = policy;
	datalock.unlock();
	return (policy_out);
}

//
// Effects:  This method returns a reference to this object.
//   This function is needed because this class inherits both
//   from lbobj and lbobj_i.
//
network::lbobj_i *
weighted_lbobj::get_objref_ii() {
	return (get_objref());
}

//
// Requires: listlock held on entry.
// Effects: Checkpoint the changes to a new secondary
//   Acquires datalock indirectly.
//
void
weighted_lbobj::add_secondary(network::ckpt_ptr ckptp, Environment &e)
{
	ASSERT(listlock.lock_held());
	network::lbobj_i_var this_lb = get_objref();
	ASSERT(!CORBA::is_nil(this_lb));
	ASSERT(!CORBA::is_nil(ckptp));

	network::dist_info_t *dist = get_distribution_ii(false, e);
	ASSERT(dist != NULL);
	if (!e.exception() && dist != NULL) {
		// Checkpoint the current distribution
		ckptp->ckpt_set_distribution(this_lb, *dist, e);
		delete dist;
	}
}

//
// ----------------------------------------------------------------
// User Load Balancer
//
//

// Primary constructor
user_lbobj::user_lbobj() :
    mc_replica_of<network::user_policy>(
    pdts_impl_t::get_ha_provider())
{
	//
	// Set the HA cookie to point to the lbobj_i, for use by
	// the checkpoint.
	//

	this->_handler()->set_cookie((lbobj *)this);
}

// Secondary constructor
user_lbobj::user_lbobj(network::user_policy_ptr obj) :
    mc_replica_of<network::user_policy>(obj)
{
	//
	// Set the HA cookie to point to the lbobj_i, for use by
	// the checkpoint.
	//

	this->_handler()->set_cookie((lbobj *)this);
}

//
// Effects: Register a stacked load-balancer "lb."
// Parameters:
//   lb (IN):		Load-balancing CORBA object.
//   e (IN):		Environment for returning exceptions
//
void
user_lbobj::register_user_policy(network::lbobj_i_ptr lb, Environment &e)
{
	// Acquire lock to seralize execution
	datalock.lock();

	// "true" means that lock is already held.
	register_user_policy_int(lb, true);

	FAULTPT_NET(FAULTNUM_NET_REG_USRP_B, FaultFunctions::generic);

	network::ckpt_ptr ckptp = get_checkpoint();
	network::user_policy_var this_lb = get_objref();

	// Checkpoint the registration to the secondary lbobj objects.
	ckptp->ckpt_user_policy(this_lb, lb, e);
	e.clear();

	// Release lock
	datalock.unlock();

	FAULTPT_NET(FAULTNUM_NET_REG_USRP_A, FaultFunctions::generic);
}

//
// Effects: Register a stacked load-balancer "lb."
// Parameters:
//   lb (IN):		Load-balancing CORBA object.
//   locked (IN):  If true then the lock is already held, else lock
//		   is not held and must be acquired.
//
void
user_lbobj::register_user_policy_int(network::lbobj_i_ptr lb, bool locked)
{
	if (!locked) {
		datalock.lock();
	}
	user_policy = network::lbobj_i::_duplicate(lb);
	if (!locked) {
		datalock.unlock();
	}
}

//
// Effects:  This method returns a CORBA reference to the stacked load-balancer
//   object.
//
network::lbobj_i_ptr
user_lbobj::get_user_policy(Environment &)
{
	network::lbobj_i_ptr lbobj_ref;

	datalock.lock();	// Acquire lock to seralize execution

	lbobj_ref = network::lbobj_i::_duplicate(user_policy);

	datalock.unlock();	// Release lock

	return (lbobj_ref);
}

//
// Return a reference to this object.
//
network::lbobj_i *
user_lbobj::get_objref_ii() {
	return (get_objref());
}

void
#ifdef DEBUG
user_lbobj::_unreferenced(unref_t cookie)
#else
user_lbobj::_unreferenced(unref_t)
#endif
{
	//
	// For the rationale on why the following code is okay,
	// please see the comments above in the unreferenced
	// method of weighted_lbobj.
	//
	ASSERT(_last_unref(cookie));

	delete this;
}

//
// Effects: This method is called by the UI to set the current distribution.
// Parameters:
//    dist (IN):	Load distribution sequence.
//    e (IN):		Environment for returning exceptions.
//
void
user_lbobj::set_distribution(const network::dist_info_t &dist,
    Environment &e)
{
	pdts_impl_t *pdtsp = get_primary_pdtsp(); // Return only if non-NULL

	//
	// Serialize with other PDT methods that access ma_servicegrplist
	//
	pdtsp->ma_servicegrplist_lock_wrlock();

	datalock.lock();	// Acquire lock to serialize execution

	network::ckpt_ptr ckptp = get_checkpoint();
	network::lbobj_i_var this_lb = get_objref();
	set_distribution_ii(dist, ckptp, this_lb, true, e);

	datalock.unlock();	// Release lock

	pdtsp->ma_servicegrplist_lock_unlock();
}

//
// Effects:  This method is called by the UI to get the current distribution
// Locks: gets datalock indirectly.
//
network::dist_info_t *
user_lbobj::get_distribution(Environment &e)
{
	network::dist_info_t *dist = get_distribution_ii(false, e);
	return (dist);
}

//
// Effects: This method returns the current load-balancing policy.
// Locks: gets datalock.
//
network::lb_specifier_t
user_lbobj::get_policy(Environment &)
{
	network::lb_specifier_t policy_out;

	datalock.lock();	// Acquire lock to serialize execution

	policy_out = policy;
	datalock.unlock();	// Release lock

	return (policy_out);
}

void
user_lbobj::set_weight(sol::nodeid_t, uint32_t, Environment &)
{
	// XXX needs to be fixed.
	ASSERT(0);
}

//
// Effects: Checkpoints the lbobj state to a new secondary.
//
void
user_lbobj::add_secondary(network::ckpt_ptr ckptp, Environment &e)
{
	ASSERT(listlock.lock_held());
	network::user_policy_var this_lb = get_objref();

	// sends the policy object and distribution to the secondary.
	// Locks: listlock held on entry.  Locks datalock.

	datalock.lock();	// Acquire lock to serialize execution

	// Checkpoint the user policy object
	ckptp->ckpt_user_policy(this_lb, user_policy, e);
	if (e.exception()) {
		datalock.unlock();	// Release lock
		return;
	}

	network::dist_info_t *dist = get_distribution_ii(true, e);
	if (!e.exception() && dist != NULL) {
		// Checkpoint the current distribution
		ckptp->ckpt_set_distribution(this_lb, *dist, e);
		delete dist;
	}

	datalock.unlock();	// Release lock
}

//
// Checkpoint accessor function.
//
network::ckpt_ptr
weighted_lbobj::get_checkpoint()
{
	return ((pdts_prov_impl_t*)(get_provider()))->
	    get_checkpoint_network_ckpt();
}

//
// Checkpoint accessor function.
//
network::ckpt_ptr
user_lbobj::get_checkpoint()
{
	return ((pdts_prov_impl_t*)(get_provider()))->
	    get_checkpoint_network_ckpt();
}
