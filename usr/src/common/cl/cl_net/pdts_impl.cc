//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pdts_impl.cc	1.275	08/05/20 SMI"

//
// PDT server for Sun Cluster 3.0 product
//

#include <h/network.h>
#include <sys/os.h>
#include <cl_net/netlib.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/sl_service.h>
#include <cl_net/errprint.h>

#include <nslib/ns.h>

#include <sys/mc_probe.h>

#include <h/ccr.h>
#include <cl_net/lbobj.h>

#include <orb/fault/fault_injection.h>
#include <sys/clconf_int.h>

#include <netinet/in.h>
#include <sys/ddi.h>

#define	REGISTER_INS	1
#define	DEREGISTER_INS 	2

static void
print_dbg_msgs(const network::service_sap_t &ssap, nodeid_t node,
    int msgid)
{
	char nodename[NODENAME_MAX];

	network_lib.get_nodename_from_nodeid(node, nodename);

	switch (msgid) {
	case REGISTER_INS:
		NET_DBG(("Scalable service instance [%s,%s,%d] "
		    "registered on node %s.\n",
		    network_lib.protocol_to_string(ssap.protocol),
		    network_lib.ipaddr_to_string(ssap.laddr),
			ssap.lport, nodename));

		//
		// SCMSGS
		// @explanation
		// The specified scalable service has been registered on the
		// specified node. Now, the gif node can redirect packets for
		// the specified service to this node.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, STATE_CHANGED,
		    "Scalable service instance [%s,%s,%d] registered on "
		    "node %s.",
		    network_lib.protocol_to_string(ssap.protocol),
		    network_lib.ipaddr_to_string(ssap.laddr),
			ssap.lport, nodename);

		break;

	case DEREGISTER_INS:
		NET_DBG(("Scalable service instance [%s,%s,%d] "
		    "deregistered on node %s.\n",
		    network_lib.protocol_to_string(ssap.protocol),
		    network_lib.ipaddr_to_string(ssap.laddr),
			ssap.lport, nodename));

		//
		// SCMSGS
		// @explanation
		// The specified scalable service had been deregistered on the
		// specified node. Now, the gif node cannot redirect packets
		// for the specified service to this node.
		// @user_action
		// This is an informational message, no user action is needed.
		//
		(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, STATE_CHANGED,
		    "Scalable service instance [%s,%s,%d] deregistered "
		    "on node %s.",
		    network_lib.protocol_to_string(ssap.protocol),
		    network_lib.ipaddr_to_string(ssap.laddr),
			ssap.lport, nodename);

		break;

	default:
		break;
	}
}


//
// Allocate mcnet_node_array.
//
int
pdts_impl_t::create_mcnet_node_array(pdts_prov_impl_t *)
{
	unsigned int	i;

	NET_DBG(("pdts_impl_t::create_mcnet_node_array()\n"));

	// Create the array of mcnet_node_impl_t objects.
	mcnet_node_array = new mcnet_node_impl_t *[NODEID_MAX + 1];
	if (mcnet_node_array == NULL) {
		MC_PROBE_0(mcnet_node_array_alloc_failure,
			"mc net pdts", "");
		return (-1);
	}

	// Initialize the array elements to NULL.
	for (i = 1; i <= NODEID_MAX; i++) {
		mcnet_node_array[i] = NULL;
	}
	return (0);
}

void
pdts_impl_t::free_mcnet_node_array()
{
	NET_DBG(("pdts_impl_t::free_mcnet_node_array()\n"));

	if (mcnet_node_array != NULL) {
		for (int i = 1; i <= NODEID_MAX; i++) {
			delete mcnet_node_array[i];
		}
	}

	delete [] mcnet_node_array;
	mcnet_node_array = NULL;
}

//
// Locate mcobj server object for specific network-interface-node.
//
network::mcobj_ptr
pdts_impl_t::mcnet_node_cbref(nodeid_t node)
{
	if (mcnet_node_array != NULL && node != 0 &&
		mcnet_node_array[node] != NULL) {
		return (mcnet_node_array[node]->get_cbref());
	}

	return (NULL);
}

//
// Effects: Constructor for the primary.
//
pdts_impl_t::pdts_impl_t(pdts_prov_impl_t *server) :
    mc_replica_of<network::PDTServer>(server),
    my_provider(server)
{
	//
	// Initialize private state of PDT server
	//
	name[0] = '\0';
	mcnet_node_array = NULL;

	NET_DBG(("pdts_impl_t primary constructor\n"));
}

//
// Effects: Constructor for the secondary.
//
// Parameters:
//   ref (IN):    object reference to PDT server implementation object
//   server (IN): pointer to HA provider object.
//
pdts_impl_t::pdts_impl_t(network::PDTServer_ptr ref,
    pdts_prov_impl_t *server) :
    mc_replica_of<network::PDTServer>(ref), my_provider(server)
{
	//
	// Initialize private state of PDT server
	//
	name[0] = '\0';
	mcnet_node_array = NULL;

	NET_DBG(("pdts_impl_t secondary constructor\n"));
}


pdts_impl_t::~pdts_impl_t()
{
	ma_servicegrp *servicegrpp = NULL;

	NET_DBG(("pdts_impl_t destructor:\n"));

	// Obtain a write lock on the PDT server state.
	pdts_lock.wrlock();

	// Free the mcnet_node_array array and each object
	free_mcnet_node_array();

	ma_servicegrplist_lock.wrlock();	// Get write lock
	// Delete the master service group list
	//
	// We must delete each service object for an entire group before
	// we can delete the group itself.  The basic assumption is that
	// some other external entity (e.g., the RGM) will deregister all
	// the service objects for some group before deregistering the
	// group.
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	while ((servicegrpp = iter.get_current()) != NULL) {
		// This erases and deletes each element from the list.
		servicegrpp->ma_serviceobj_list.dispose();

		// Now erase the service group object from the list and
		// then delete the object itself.

		// Must advance the iterator _before_ erasing the object
		// from the list.
		iter.advance();
		(void) ma_servicegrplist.erase(servicegrpp);
		delete servicegrpp;
	} // end for

	my_provider = NULL;	// Make lint happy

	ma_servicegrplist_lock.unlock();	// Release write lock

	// Release the write lock.
	pdts_lock.unlock();
}

void
#ifdef DEBUG
pdts_impl_t::_unreferenced(unref_t cookie)
#else
pdts_impl_t::_unreferenced(unref_t)
#endif
{
	NET_DBG(("pdts_impl_t::_unreferenced\n"));

	ASSERT(_last_unref(cookie));

	delete this;
}


//
// Effects:  Attach the mcobj object "cbref" to the PDT server,
//   collect information about registered scalable services in
//   "srvinfoseq" and who the global network interfaces nodes are
//   in "gifinfo" and the reference to the mcnet_node_impl_t corresponding
//   to node "node."  This "cbref" object is the means by which
//   the PDT server communicates with each node of the cluster.
//
// Parameters:
//   - node (IN):	The node where the mcobj resides.
//   - cbref (IN):	The object reference to the mcobj.
//   - serv_ref (OUT):  The object reference to the mcnet_node_impl_t
//			for this node.
//   - e (IN):	Environment variable for passing around exceptions
//
void
pdts_impl_t::attach_mcobj(nodeid_t node, network::mcobj_ptr cbref,
    CORBA::Object_out serv_ref, Environment &e)
{
	network::mcobjs_t	*mcobjs;
	static os::mutex_t	attach_lock;
	mcnet_node_impl_t	*mcnet_nodep;
	network::mcnet_node_ptr mcnet_node_p;
	nodeid_t		i;

	NET_DBG(("pdts_impl_t::attach_mcobj(%d)\n", node));

	MC_PROBE_1(attach_mcobj, "mc net pdts", "",
		tnf_uint, node, node);

	if (node < 1 || node > NODEID_MAX) {
		e.exception(new sol::op_e(EINVAL));	// raise EINVAL
		mcobjs = (network::mcobjs_t *)NULL;
		return;
	}

	ASSERT(mcnet_node_array != NULL);

	//
	// Get write lock on the PDT server state to serialize
	// callers of attach_mcobj.
	//
	pdts_lock.rdlock();

	//
	// We obtain a local lock to serialize invocations of
	// attach_mcobj.
	//
	attach_lock.lock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	attach_mcobj_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (attach_mcobj_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		if (mcnet_node_array[node] == NULL) {
			// Not there?  Then create one!
			mcnet_node_array[node] =
				new mcnet_node_impl_t(my_provider);
		} // end if-then

		mcnet_nodep = mcnet_node_array[node];
		ASSERT(mcnet_nodep != NULL);

		mcnet_nodep->node = node;

		//
		// We need to check whether the mcnet_node
		// object at that index is NULL.  One might
		// think this is an error.  After all,
		// when node "node" failed, the mcnet_node object
		// should have deleted itself upon receiving the
		// unreferenced message.  But it is entirely possible
		// that such a message might be delayed considerably
		// because the node is heavily loaded.  The ORB just
		// guarantees that such a message will be delivered
		// eventually. And so the failed node could have had
		// time to rejoin the cluster and invoke attach_mcobj().
		// What we must do is to get another object reference,
		// which bumps the reference count to 1; this prevents
		// the unreferenced from deleting the object.  When the
		// mcnet_node object gets the unreferenced, it will
		// test whether there are any references; if so, it
		// does nothing; if not, then it will proceed with
		// deferring a task and deleting the object.  This
		// method is serialized with the unreferenced
		// method of mcnet_node.
		//

		//
		// Set the object reference to the mcobj "cbref" in the
		// mcnet_node_impl_t object corresponding to the node
		// "node."
		//
		mcnet_nodep->set_cbref(cbref);

		FAULTPT_NET(FAULTNUM_NET_ATACH_MCOB_B, FaultFunctions::generic);

		//
		// HA: Checkpoint the object reference to the mcobj to
		// the secondaries. Non-idempotent one-phase
		// mini-transaction. If a retry of this method
		// occurs, no harm is done
		// because the mcnet_node_impl_t holding the reference
		// will die along with the primary PDT server of which
		// it is a part, and the reference will be
		// released. The new one supersedes it, and
		// all is well.  We want to avoid the
		// overhead of checkpointing again.
		//
		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("    	ckptp->ckpt_attach_mcobj()\n"));
		NET_DBG(("          node = %d, cbref = 0x%x\n",
		    node, cbref));

		//
		// Create a reference. This is set in the
		// OUT parameter "serv_ref," which will set
		// the global variable at the node that is
		// attaching its mcobj. This ref will be the
		// only one pointing at the mcnet_node_impl_t,
		// which will get the unreferenced method when the
		// node fails. We also pass the reference in the
		// checkpoint message.
		//
		mcnet_node_p = mcnet_nodep->get_objref();

		//
		// Hand over the reference currently held on the
		// server object to the associated client.
		//
		// This static global variable "servref" on node "node"
		// (see definition in cl_net/inet_port_mgmt.cc) holds the
		// only reference to the mcnet_node_impl_t
		// corresponding to that node. When that node dies,
		// mcnet_node_impl_t will get the unreferenced
		// message delivered to it and it can take appropriate
		// cleanup action.
		//
		serv_ref = mcnet_node_p;

		ckptp->ckpt_attach_mcobj(node, cbref, mcnet_node_p, e);
		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_ATACH_MCOB_A, FaultFunctions::generic);
	} else {
		ASSERT(mcnet_node_array[node] != NULL);

		serv_ref = mcnet_node_array[node]->get_objref();
	}

	// Nested invocation requires its own environment
	Environment	env;

	//
	// Send the new mcobj reference down to all nodes except for
	// the one that's invoking attach_mcobj.  That node will receive
	// the entire list of references.
	//
	for (i = 1; i <= NODEID_MAX; i++) {
		if (i == node)
			continue;

		if (mcnet_node_array[i] == NULL)
			continue;

		network::mcobj_ptr mcobjref;
		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref))
			continue;

		mcobjref->mc_attach_mcobj(node, cbref, env);
		if (env.exception()) {
			//
			// Node probably died, just continue on.
			//
			env.clear();
			break;
		}
	}

	//
	// Gather the current list of mcobj references to send
	// back to the caller.
	//
	mcobjs = new network::mcobjs_t(NODEID_MAX + 1, NODEID_MAX + 1);
	for (i = 1; i <= NODEID_MAX; i++) {
		if (mcnet_node_array[i] != NULL &&
		    !CORBA::is_nil(mcnet_node_array[i]->get_cbref())) {
			(*mcobjs)[i] = network::mcobj::_duplicate(
			    mcnet_node_array[i]->get_cbref());
		} else {
			(*mcobjs)[i] = network::mcobj::_nil();
		}
	}

	cbref->mc_attach_mcobjs(*mcobjs, env);
	if (env.exception()) {
		//
		// Node probably died, so just continue on.
		//
		env.clear();
	}

	delete mcobjs;

	//
	// Release the attach_mcobj lock and the lock on the PDT server state.
	//
	attach_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_ATACH_MCOB_E, FaultFunctions::generic);
}

//
// Effects: Returns the total number of master service objects over
//   all master service groups.  Returns 0 if there are no such
//   objects.
//
uint_t
pdts_impl_t::count_scal_services()
{
	uint_t			count = 0;
	ma_servicegrp		*servicegrpp = NULL;
	ma_serviceobjlist_t	*masobjlist;

#ifdef _KERNEL
	ASSERT(ma_servicegrplist_lock.lock_held());	// lock is held?
#endif

	//
	// Iterate over the master group list, and then for each master
	// service group object iterate over the master service object
	// list, counting the number of service objects.
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	while ((servicegrpp = iter.get_current()) != NULL) {
		servicegrpp->ma_serviceobj_list_lock.wrlock();
		masobjlist = &servicegrpp->ma_serviceobj_list;
		count += masobjlist->count();
		servicegrpp->ma_serviceobj_list_lock.unlock();
		iter.advance();
	}

	return (count);
}

void
pdts_impl_t::gns_mcobjs(network::mcobjs_t_out mcobjs, Environment& e)
{
	//
	// Gather the current list of mcobj references to send
	// back to the caller.
	//
	pdts_lock.rdlock();
	mcobjs = new (os::NO_SLEEP)
	    network::mcobjs_t(NODEID_MAX + 1, NODEID_MAX + 1);
	if (mcobjs == NULL) {
		pdts_lock.unlock();
		e.exception(new sol::op_e(ENOMEM));
		return;
	}
	for (uint_t i = 1; i <= NODEID_MAX; i++) {
		if (mcnet_node_array[i] != NULL &&
		    !CORBA::is_nil(mcnet_node_array[i]->get_cbref())) {
			(*mcobjs)[i] = network::mcobj::_duplicate(
			    mcnet_node_array[i]->get_cbref());
		} else {
			(*mcobjs)[i] = network::mcobj::_nil();
		}
	}
	pdts_lock.unlock();
}

void
pdts_impl_t::gns_pdt(network::pdtinfo_t_out pdtinfop,
    const network::group_t& gname, Environment& e)
{
	network::pdtinfo_t *pdtp;
	ma_servicegrp *sgrp;

	//
	// Pre-initialize our out parameter
	//
	pdtinfop = (network::pdtinfo_t *)NULL;

	//
	// Allocate needed output parameters and set length
	// before acquiring locks to avoid priority inversion
	// and blocking memory allocations (i.e. length()).
	//
	// XXX We assume here that all pdt tables are created
	// XXX the same size as pdt_hash_size (or smaller).
	//
	pdtp = new (os::NO_SLEEP) network::pdtinfo_t;
	if (pdtp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		return;
	}
	pdtp->hashinfoseq.length(pdt_hash_size);

	//
	// Acquire read lock on PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Acquire read lock on maservicelist to prevent
	// the contents from changing.
	//
	ma_servicegrplist_lock.rdlock();

	if ((sgrp = find_ma_servicegrp(gname)) == NULL) {
		ma_servicegrplist_lock.unlock();
		pdts_lock.unlock();
		e.exception(new sol::op_e(ENOENT));
		delete pdtp;
		return;
	}

	sgrp->marshal_pdt(pdtp, e);

	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	pdtinfop = pdtp;
}

//
//
void
pdts_impl_t::gns_service(network::grpinfoseq_t_out grpinfoseqp,
    const network::group_t& gname, Environment& e)
{
	network::grpinfoseq_t *grpp;
	uint_t i, j, retrycnt = 0;
	ma_servicegrp *sgrp;
	network::seqlong_t sobjcnt;
	uint_t grpcount = 1;

	//
	// Pre-initialize our out parameter
	//
	grpinfoseqp = (network::grpinfoseq_t *)NULL;

	//
	// Create the "grpinfoseqp" sequence for returning the service data.
	//
	grpp = new (os::NO_SLEEP) network::grpinfoseq_t;
	if (grpp == NULL) {
		e.exception(new sol::op_e(ENOMEM));
		return;
	}
	network::grpinfoseq_t& grpinfoseq = *grpp;

	//
	// Return here, without locks, if the servicegrp list length changes
	//
resize:

	//
	// We only have one service unless a null resource name is
	// passed ... in that case, find out how many service groups
	// are in the list, then drop locks and set our outparams
	// sequence length and temporary array.
	//
	// We have a seperate serviceobj count sequence to contain
	// the service object counts for each service group.
	//
	if (*gname.resource == NULL) {
		pdts_lock.rdlock();
		ma_servicegrplist_lock.rdlock();
		grpcount = ma_servicegrplist.count();
		ma_servicegrplist_lock.unlock();
		pdts_lock.unlock();
	} else {
		grpcount = 1;
	}

	grpinfoseq.length(grpcount);
	sobjcnt.length(grpcount);

	//
	// Now that we've set the group count, retrieve the number
	// of service objects in each group so that we can resize them.
	//
	pdts_lock.rdlock();
	ma_servicegrplist_lock.rdlock();

	if (*gname.resource == NULL) {
		//
		// Check to make sure that the number of groups
		// didn't change.
		//
		if (grpcount != ma_servicegrplist.count()) {
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			goto resize;
		}
		//
		// Count the number of service objects per group
		// so that we can size it after we've dropped locks.
		//
		ma_servicegrplist_t::ListIterator giter(ma_servicegrplist);

		for (i = 0; (sgrp = giter.get_current()) != NULL;
		    giter.advance()) {
			sobjcnt[i++] = sgrp->ma_serviceobj_list.count();
		}
	} else {
		sgrp = find_ma_servicegrp(gname);

		if (sgrp == NULL) {
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			e.exception(new sol::op_e(ENOENT));
			delete grpp;
			return;
		}
		sobjcnt[0] = sgrp->ma_serviceobj_list.count();
	}

	//
	// Drop locks and resize the serviceobj sequence in each
	// servicegrp.  We only make them longer here ... marshal_sgrp
	// will shorten them to reflect what really gets stored.
	//
	// Max out the group/config/srv_inst instance list length(s)
	// as well as the load distribution list.
	//
	// The load_dist array contains a zero'th element that we could
	// eliminate, but to make things simple we simply send it down
	// with all the other node weights, hence the length can be
	// NODEID_MAX + 1 for the weights list (see comment in marshal_sgrp()).
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	if (*gname.resource == NULL) {
		for (i = 0; i < grpcount; i++) {
			if (grpinfoseq[i].srv_obj.length() < sobjcnt[i]) {
				grpinfoseq[i].srv_obj.length(sobjcnt[i]);
				for (j = 0; j < sobjcnt[i]; j++) {
					grpinfoseq[i].srv_obj[j].srv_inst.length
					    (NODEID_MAX);
				}
			}
			grpinfoseq[i].srv_grpinst.length(NODEID_MAX);
			grpinfoseq[i].srv_config.length(NODEID_MAX);
			grpinfoseq[i].srv_lb_weights.length(NODEID_MAX + 1);
		}
	} else {
		if (grpinfoseq[0].srv_obj.length() < sobjcnt[0]) {
			grpinfoseq[0].srv_obj.length(sobjcnt[0]);
			for (j = 0; j < sobjcnt[0]; j++) {
				grpinfoseq[0].srv_obj[j].srv_inst.length(
				    NODEID_MAX);
			}
		}
		grpinfoseq[0].srv_grpinst.length(NODEID_MAX);
		grpinfoseq[0].srv_config.length(NODEID_MAX);
		grpinfoseq[0].srv_lb_weights.length(NODEID_MAX + 1);
	}

	//
	// Now the grpinfoseq sequences are set ... marshal our
	// service data into it, re-adjusting the sequence lengths
	// down if they're too big, and failing with EAGAIN if they
	// are too small.
	//

	//
	// Re-acquire our read lock on the maservicelist to prevent
	// the contents from changing.
	//
	pdts_lock.rdlock();
	ma_servicegrplist_lock.rdlock();

	//
	// A non-null resource name indicates that we want to see a
	// specific service, otherwise send back all services.
	//
	if (*gname.resource != NULL) {
		sgrp = find_ma_servicegrp(gname);

		if (sgrp == NULL) {
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			e.exception(new sol::op_e(ENOENT));
			delete grpp;
			return;
		}

		sgrp->marshal_sgrp(&(grpinfoseq[0]), e);
		if (e.exception()) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();

			//
			// The number of service objects changed
			// underneath us ... resize the sequences.
			//
			if (ex_op && (ex_op->error == EAGAIN)) {
				if (++retrycnt < GNS_MAX_RETRIES) {
					e.clear();
					goto resize;
				}
			}
			delete grpp;
			return;
		}

	} else {

		//
		// We're getting the entire list, check if the count changed
		// after we took it, if so, drop the lock and try again.
		//
		if (grpcount < ma_servicegrplist.count()) {
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			goto resize;
		}

		//
		// The grpinfoseq is allowed to be longer that needed,
		// we adjust it down to what we need.
		//
		ASSERT(grpcount >= ma_servicegrplist.count());
		grpinfoseq.length(grpcount);

		//
		// Iterate over the service group list.  For each service
		// group iterate over the list of service objects, extracting
		// relevant information from each object and inserting into
		// the grpinfoseqp sequence; this information will be used to
		// re-create the service group and its service objects.
		//
		ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

		for (i = 0; (sgrp = iter.get_current()) != NULL;
		    iter.advance()) {
			sgrp->marshal_sgrp(&grpinfoseq[i++], e);
			if (e.exception()) {
				sol::op_e *ex_op =
				    sol::op_e::_exnarrow(e.exception());

				ma_servicegrplist_lock.unlock();
				pdts_lock.unlock();
				if (ex_op && (ex_op->error == EAGAIN)) {
					if (++retrycnt < GNS_MAX_RETRIES) {
						e.clear();
						goto resize;
					}
				}
				delete grpp;
				return;
			}
		}
	}

	//
	// Release the locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	grpinfoseqp = grpp;
}

//
// Effects:
//
// Parameters:
//   - srvinfoseqp (OUT):  Sequence of srvinfo data about each service obj.
//   - gifinfop (OUT):	Array of booleans indexed by nodeid.  1 means that
//			node is a gif node, 0 otherwise.
//			the time of method invocation.
//   - nodeid (IN):	Node where proxy queue resides.
//   - e (IN):		Environment variable for passing around exceptions
//
void
pdts_impl_t::get_all_registered_services(
    network::srvinfoseq_t_out	srvinfoseqp,		// OUT
    Environment			&)			// IN
{
	ma_servicegrp *servicegrpp;
	unsigned int i;

	NET_DBG(("pdts_impl_t::get_all_registered_services()\n"));

	//
	// Acquire read lock on PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Acquire read lock on maservicelist to prevent
	// the contents from changing.
	//
	ma_servicegrplist_lock.rdlock();
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	//
	// Create the sequence "srvinfoseqp" of service data.  We first count
	// the number of scalable services and set the length appropriately.
	//
	uint_t count = count_scal_services();
	srvinfoseqp = new network::srvinfoseq_t(count);
	network::srvinfoseq_t& srvinfoseq = *srvinfoseqp;
	srvinfoseq.length(count);

	MC_PROBE_1(get_all_registered_services, "mc net pdts", "",
		tnf_uint, number_services, count);
	//
	// Iterate over the master service group list.  For each service
	// group iterate over the list of service objects, extracting
	// relevant information from each object and inserting into
	// the srvinfoseqp sequence; this information will be used to
	// re-create the service group and its service objects at the
	// mcobj in its slave form.
	//
	for (i = 0; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		network::sticky_config_t st;
		ma_serviceobj *sobj;
		ma_serviceobjlist_t *masobjlist;

		// Retrieve per-group sticky config
		servicegrpp->ma_get_sticky_config(st);

		servicegrpp->ma_serviceobj_list_lock.wrlock();
		masobjlist = &servicegrpp->ma_serviceobj_list;
		for (masobjlist->atfirst();
		    (sobj = masobjlist->get_current()) != NULL;
		    servicegrpp->ma_serviceobj_list.advance(), i++) {
			ASSERT(i < count);
			srvinfoseq[i].srv_nhash =
			    (unsigned int)servicegrpp->getnhash();
			srvinfoseq[i].srv_group = servicegrpp->getgroup();
			srvinfoseq[i].srv_policy = servicegrpp->getpolicy();
			srvinfoseq[i].srv_gifnode = sobj->get_gifnode();
			srvinfoseq[i].srv_sap = sobj->getssap();
			srvinfoseq[i].srv_stconfig = st;
		}
		servicegrpp->ma_serviceobj_list_lock.unlock();
	}

	// Release the locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();
}

//
// Effects: This routine is invoked from "deregister_gifnode()" as well
//   as by the PDT server secondaries to change the state of the
//   master service objects, recording the fact that node "node"
//   is being deregistered.
//
// Parameters:
//   - node (IN): The node id of the global interface node that failed.
//
void
pdts_impl_t::deregister_gifnode1(nodeid_t node)
{
	ma_servicegrp	*servicep;

	NET_DBG(("  pdts_impl_t::deregister_gifnode1(%d)\n", node));

	//
	// Obtain a read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Get a read lock on the master service group list.
	//
	ma_servicegrplist_lock.rdlock();

	//
	// For every service deregister information about the gif node
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		//
		// We remove the instance from the instance list
		// and update the number of instances.
		//
		if (servicep->ma_deregister_node(node)) {
			servicep->instancelist_lock.wrlock();
			servicep->update_buckets();
			servicep->instancelist_lock.unlock();
		}
	} // end for

	//
	// Release locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();
}

//
// Effects: This routine deregisters the gif node "node" from all the
//   master service objects in the PDT server.
//
// Parameters:
//   - node (IN):  node id of the node that is deregistering its global network
//		due to node failure.
//   - e (IN):	Environment variable for passing around exceptions
//
// NOTE: This IDL method is called in a deferred task that is spawned
// by the unreferenced method in mcnet_node when a node fails.  This
// particular method will ONLY be called by the primary to deregister
// the gif node "node."  We don't care if it fails; it will never be
// retried because the deferred task--the caller--has also died as
// part of node failure.  Therefore, it need not be idempotent.
// The secondaries will have the same state because the unreferenced is
// also sent to them; they will call the helper routine--
// deregister_gifnode1--to do the same deregistration locally.
//
void
pdts_impl_t::deregister_gifnode(nodeid_t node, Environment&)
{
	ma_servicegrp *servicegrpp;

	NET_DBG(("pdts_impl_t::deregister_gifnode(%d)\n", node));

	MC_PROBE_0(deregister_gifnode, "mc net pdts", "");

	//
	// Note that we're not acquiring locks here because that's
	// done internally by this helper method.  This helper method
	// will be called directly by the secondaries.
	//
	deregister_gifnode1(node);

	FAULTPT_NET(FAULTNUM_NET_DEREG_GIFN_B, FaultFunctions::generic);

	//
	// Obtain a read lock on the PDT server state.
	//
	pdts_lock.rdlock();
	//
	// Acquire a read lock on the master service group list.
	//
	ma_servicegrplist_lock.rdlock();

	//
	// We've updated the master PDTs with whatever changes
	// were necessary to reflect the death of gif node "node."  Now
	// we must download these PDTs to the gif nodes.
	//
	Environment env;
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		servicegrpp->ma_distribute_pdt(node,
			ma_servicegrp::DEREGISTER_PROXY, env);
		env.clear();
	}


	// Release the locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_DEREG_GIFN_E, FaultFunctions::generic);
}


//
// Effects:  Helper function.  This routine tells all master service
//   objects to deregister all instances that were running at node "node,"
//   the failed node.
//
// Parameters:
//   - node (IN): node that failed.
//
void
pdts_impl_t::deregister_proxynode1(nodeid_t node)
{
	ma_servicegrp		*servicegrpp = NULL;

	NET_DBG(("  pdts_impl_t::deregister_proxynode1(%d)\n", node));

	// Acquire locks.
	pdts_lock.rdlock();
	ma_servicegrplist_lock.rdlock();

	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->ma_deregister_node(node)) {
			servicegrpp->instancelist_lock.wrlock();
			servicegrpp->update_buckets();
			servicegrpp->instancelist_lock.unlock();
		}
	}

	// Release locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();
}

//
// Effects: This method deregisters all instances that were running on the
//   failed node "node" from the PDT server.
//
// Parameters:
//   - node (IN): The node id of the cluster node whose proxy object has gone
//		  away due to node failure.
//   - e (IN):    Environment variable for passing around exceptions.
//
// NOTE: This IDL method is called in a deferred task that is spawned
// by the unreferenced method in mcnet_node when a node fails.  This
// particular method will ONLY be called by the primary to deregister
// the proxy node "node."  We don't care if it fails; it will never be
// retried because the deferred task--the caller--has also died as
// part of node failure.  Therefore, it need not be idempotent.
// The secondaries will have the same state because the unreferenced is
// also sent to them; they will call the helper routine--
// deregister_proxynode1--to do the same deregistration locally.
//
void
pdts_impl_t::deregister_proxynode(nodeid_t node, Environment &)
{
	ma_servicegrp *servicegrpp;

	NET_DBG(("pdts_impl_t::deregister_proxynode(%d)\n", node));

	//
	// Note that we're not acquiring locks here because that's
	// done internally by this helper method.  This helper method
	// will be called directly by the secondaries.
	//
	deregister_proxynode1(node);

	FAULTPT_NET(FAULTNUM_NET_DEREG_PXYN_A, FaultFunctions::generic);

	//
	// Acquire a read lock on the PDTS server state.
	//
	pdts_lock.rdlock();

	//
	// Acquire a read lock on the master service group list.
	//
	ma_servicegrplist_lock.rdlock();

	// Broadcast the deregistration to the slaves.
	Environment env;
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		servicegrpp->ma_distribute_pdt(node,
			ma_servicegrp::DEREGISTER_PROXY, env);
		//
		// Exception is set only if gifnode dies when
		// ma_distribute_pdt is in progress. This is ok,
		// from this call's point of view.
		//
		env.clear();
	}

	// Release the locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_DEREG_PXYN_E, FaultFunctions::generic);
}

//
// Effects:  Helper function.  This routine records various information
//   about the newly registered GIF node in the PDT server state,
//   and informs every master service object about the node.
//
// Parameters:
//   - node (IN):  The node id of the cluster node that has come up.
//
void
pdts_impl_t::register_gifnode1(nodeid_t node)
{
	mcnet_node_impl_t *client_nodep;

	NET_DBG(("  pdts_impl_t::register_gifnode1(%d)\n", node));

	//
	// mcnet_node_array should never be NULL unless someone else
	// clobbered the pointer.  This statement does _not_ panic
	// the system; it's merely an ASSERT that's valid in both debug
	// and non-debug kernels.
	//
	CL_PANIC(mcnet_node_array != NULL);

	client_nodep = mcnet_node_array[node];

	//
	// client_nodep should never be NULL, but just in case someone
	// else clobbers the pointer, let's check for not NULL.
	//
	CL_PANIC(client_nodep != NULL);

	//
	// Record in the mcnet_node that it corresponds to a gif node.
	// True means it does, false otherwise.
	//
	client_nodep->set_gifnode(true);
}

//
// Effects: Register information about global networking interfaces.
//   If registering global networking interface from a node for the
//   first time and replicated services have already been enabled
//   then we also update the node with switching information,
//   which may involve multiple callbacks to the client node.
//   Usually this is not expected to happen.
// Parameters:
//   - node (IN): The node id of the cluster node whose proxy object
//		has gone away due to node failure.
//   - e (IN):	Environment variable for passing around exceptions.
//
int32_t
pdts_impl_t::register_gifnode(nodeid_t node, Environment &e)
{
	NET_DBG(("pdts_impl_t::register_gifnode(%d)\n", node));

	MC_PROBE_1(register_gif, "mc net pdts", "",
		tnf_uint, node, node);

	if (node < 1 || node > NODEID_MAX) {
		e.exception(new sol::op_e(EINVAL));
		return (-1);
	}

	//
	// Get the write lock to prevent a race between this call
	// and register_pxqobj().
	//
	// XXX We no longer call register_pxqobj, so should this remain?
	//
	pdts_lock.wrlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	register_gifnode_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (register_gifnode_state *)pctxp->get_saved_state();

	// HA: If there's no saved state, then an ordinary method invocation or
	// a retry will take this path, cause state changes in the
	// mcnet_node_impl_t and master service objects,
	// and checkpoint to the secondaries
	// the *invocation* to cause those same state changes on the
	// secondaries without incurring the expense of sending state data.
	//
	if (saved_state == NULL) {
		register_gifnode1(node);

		FAULTPT_NET(FAULTNUM_NET_REG_GIFN_B, FaultFunctions::generic);

		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("   ckpt_register_gifnode(%d)\n", node));

		//
		// HA: Checkpoint the invocation (rather than state changes) to
		// the secondaries.  This will cause the state changes to
		// happen on the secondaries: set state in the mcnet_node
		// corresponding to node "node"
		// and register the gif with the master service objects.
		//
		ckptp->ckpt_register_gifnode(node, e);
		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_REG_GIFN_A, FaultFunctions::generic);
	}

	//
	// Release read lock on the PDT server state.
	//
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_REG_GIFN_E, FaultFunctions::generic);

	return (0);
}

//
// Effects:  This routine creates a new master service object with name
//   "ssap".  This object is appended to the service object list of the
//   master service group object identified by "g_name."
//
// Parameters:
//   - gname (IN):	Service group name.
//   - ssap (IN):	Service access point <protocol, ip addr, port>.
//			Names the service object uniquely.
//
void
pdts_impl_t::register_service1(const network::group_t &gname,
    const network::service_sap_t &ssap)
{
	ma_servicegrp		*servicep = NULL;

	NET_DBG(("  pdts_impl_t::register_service1()\n"));
	NET_DBG(("    group = %s\n", gname.resource));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	servicep = find_ma_servicegrp(gname);
	ASSERT(servicep != NULL);

	//
	// Get write lock on the service object list
	//
	servicep->ma_serviceobj_list_lock.wrlock();
	if (!servicep->equal(ssap)) {
		ma_serviceobj *sobj =
		    new ma_serviceobj(*servicep, ssap);
		servicep->ma_serviceobj_list.append(sobj);
	}
	servicep->ma_serviceobj_list_lock.unlock();
}

//
// Effects:  This helper routine takes a master service object "servicep"
//   an argument, the service access point information, and the type
//   of load-balancing policy.  It broadcasts to all nodes information
//   about this new services.
//
// Parameters:
//   - servicep (IN):	pointer to service group object.
//   - ssap (IN):	service access point <protocol, ip addd, port>
//
void
pdts_impl_t::register_service2(ma_servicegrp *servicep,
    const network::service_sap_t &ssap)
{
	Environment env;
	network::mcobj_ptr	mcobjref;
	network::srvinfo_t	srvinfo;

	NET_DBG(("  pdts_impl_t::register_service2()\n"));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	ASSERT(mcnet_node_array != NULL);

	srvinfo.srv_sap = ssap;
	srvinfo.srv_nhash = (unsigned int)servicep->getnhash();
	srvinfo.srv_policy = servicep->getpolicy();
	srvinfo.srv_group = servicep->getgroup();
	srvinfo.srv_gifnode = NULLNODE;

	//
	// Broadcast service information to all nodes
	//
	for (uint32_t i = 1; i <= NODEID_MAX; i++) {
		if (mcnet_node_array[i] == NULL) {
			continue;
		}
		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref)) {
			continue;
		}

		//
		// Update service information on clients
		//
		mcobjref->mc_register_a_service(srvinfo, env);
		if (env.exception()) {
			//
			// Failed to update switching
			// information on some node
			// Recovery depends on the type of
			// exception.For now we print the
			// warning and punt the issue.
			//
			env.clear();
			cmn_err(CE_WARN, "Failed to update switching "
			    "info on node %d\n", i);
		}
	} // end for
}

network::lbobj_i_ptr
pdts_impl_t::get_primary_lb(const network::group_t & gname, Environment &)
{
	ma_servicegrp *servicegrpp = find_ma_servicegrp(gname);
	return (servicegrpp->get_lbobj());
}

//
// Effects: Create a new scalable service group named "gname."
//   Returns network::SUCCESS if the group has been successfully added.
//
// Parameters:
//    - gname (IN): Service group name
//    - policykind (IN):   kind of load-balancing policy
//    - primary_lb (IN):  object reference to the primary's lbobj
//
// Exceptions:
//    - GROUP_EXISTS: if the group has already been created.
//
network::ret_value_t
pdts_impl_t::create_scal_srv_grp1(const network::group_t& gname,
    network::lb_policy_t policykind, network::lbobj_i_ptr primary_lb)
{
	bool		okay = true;

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	NET_DBG(("  pdts_impl_t::create_scal_srv_grp1()\n"));

	//
	// Find the group given the "gname."
	//
	ma_servicegrp *servicegrpp = find_ma_servicegrp(gname);
	if (servicegrpp != NULL) {
		//
		// Service already exists. Return error code.
		// Locks will be released by the caller.
		//
		return (network::GROUP_EXISTS);
	}

	//
	// Now create the service group object.
	//
	servicegrpp = new ma_servicegrp(gname, policykind,
		primary_lb, okay, pdt_hash_size);
	if (!okay) {
		NET_DBG(("create_scal_srv_grp1: error creating"
		    "ma_servicegrp object\n"));
		delete servicegrpp;
		return (network::INTERNAL_ERROR);
	}

	//
	// The group does not exist. So append it.
	//
	if (is_sticky_wildcard(servicegrpp->getpolicy())) {
		ma_servicegrplist.append(servicegrpp);
	} else {
		ma_servicegrplist.prepend(servicegrpp);
	}

	return (network::SUCCESS);
}

//
// Effects: Tell all the nodes to create this service group as part of their
//   state.
//
// Parameters:
//    - gname (IN): Service group name
//    - nhash (IN):  Size of the PDT hash table.
//    - policykind (IN):   kind of load-balancing policy
//    - e (IN):	Environment variable for passing around exceptions.
//
// Exceptions:
//
void
pdts_impl_t::create_scal_srv_grp2(const network::group_t& gname,
    uint32_t nhash, network::lb_policy_t policykind, Environment &e)
{
	network::mcobj_ptr	mcobjref;

	NET_DBG(("  pdts_impl_t::create_scal_srv_grp2()\n"));

	//
	// For each node, get the mcobj reference.
	//
	for (nodeid_t i = 1; i <= NODEID_MAX; i++) {
		if (mcnet_node_array[i] == NULL) {
			continue;
		}
		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref))
			continue;
		mcobjref->mc_register_grp(gname, nhash, policykind, e);
		e.clear();
	}
}

//
// Effects: Create a new scalable service group, called from the userland
//   SSM client.  Returns network::SUCCESS if the group has been successfully
//   added.
//
// Parameters:
//    - gname (IN): Service group name
//    - policykind (IN):   kind of load-balancing policy
//    - e (IN):	Environment variable for passing around exceptions.
//
// Exceptions:
//    - GROUP_EXISTS: if the group has already been created.
//
network::ret_value_t
pdts_impl_t::create_scal_srv_grp(const network::group_t& gname,
    network::lb_policy_t policykind, Environment &e)
{
	network::ret_value_t	retval = network::SUCCESS;

	NET_DBG(("pdts_impl_t::create_scal_srv_grp(group=%s)\n",
		gname.resource));

	//
	// Get a read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Get a write lock on the master service group list so we
	// can add a new group object to the list.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Get the primary's transaction context from the environment.
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	create_scal_srv_grp_state *saved_state;

	//
	// Extract the saved state from the context.
	//
	saved_state = (create_scal_srv_grp_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		// Create a nil lbobj object
		network::lbobj_i_var	primary_lb;
		primary_lb = network::lbobj_i::_nil();
		retval = create_scal_srv_grp1(gname, policykind, primary_lb);
		if (retval != network::SUCCESS) {
			//
			// Release the locks
			//
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return (retval);
		}

		FAULTPT_NET(FAULTNUM_NET_ADD_GRP_B, FaultFunctions::generic);

		//
		// HA: Now checkpoint the invocation of create_scal_srv_grp1()
		// at the secondaries.  This will have the effect of
		// creating a new master service group object along with the
		// newly created primary load balancer object, and append it
		// to the list.
		//
		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("  ckpt_create_scal_srv_grp()\n"));

		// Get the primary lbobj object created by create_scal_srv_grp1
		// and pass it in the checkpoint.
		primary_lb = get_primary_lb(gname, e);
		ckptp->ckpt_create_scal_srv_grp(gname, policykind,
			primary_lb, e);
		ASSERT(!e.exception());
		e.clear();
		FAULTPT_NET(FAULTNUM_NET_ADD_GRP_A, FaultFunctions::generic);
	} else {
		retval = saved_state->retval_state;
	}

	//
	// Update information on the slaves
	//
	Environment env;
	create_scal_srv_grp2(gname, pdt_hash_size, policykind, env);

	//
	// No exception is passed back from create_scal_srv_grp2(). Exception
	// is cleared before return. In case we do have an exception to be
	// passed back from create_scal_srv_grp2(), we need to remap the
	// exception and pass it back in e.
	//
	env.clear();

	//
	// Release the locks
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_ADD_GRP_E, FaultFunctions::generic);

	//
	// SCMSGS
	// @explanation
	// The service group by that name is now known by the scalable
	// services framework.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "Service group '%s' created", gname.resource);

	return (retval);
}

//
// Effects: Configure sticky parameters. This function has no effect on
//	non-sticky service groups.
//
// Parameters:
//	gname:		Service group name
//	st:		Sticky config value
//
// Exceptions:
//	NOSUCH_GROUP:	If gname can't be found as a group
//
network::ret_value_t
pdts_impl_t::config_sticky_srv_grp(const network::group_t &gname,
    const network::sticky_config_t &st, Environment &e)
{
	network::ret_value_t 	ret_value = network::SUCCESS;
	bool changed = false;

	NET_DBG(("pdts_impl_t::config_sticky_srv_grp()\n"));
	NET_DBG(("   group = %s, timeout = %d, flags = %x\n",
	    gname.resource, st.st_timeout, st.st_flags));

	ASSERT(st.st_timeout >= BIND_TIMEOUT_NONE);

	//
	// Obtain the read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Obtain the write lock on the master service group list.
	// We are not changing the list at all. We're just using this lock
	// to serialize updates to the sticky config of *one* service
	// group.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	config_sticky_srv_grp_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (config_sticky_srv_grp_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		changed = config_sticky_srv_grp1(gname, st, &ret_value);

		FAULTPT_NET(FAULTNUM_NET_CFG_ST_B, FaultFunctions::generic);

		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("   call ckpt_config_sticky_srv_grp(%d, %x)\n",
		    st.st_timeout, st.st_flags));

		ckptp->ckpt_config_sticky_srv_grp(gname, st, e);

		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_CFG_ST_A, FaultFunctions::generic);

	} else {
		ret_value = saved_state->retval_state;
		changed = saved_state->changed_state;
	}

	//
	// Broadcast the values to all nodes if the values have changed,
	// or if this is *not* a re-tried invocation. This would mean
	// duplicate invocations will result in multiple broadcasts of
	// the same values, but it's acceptable since the orphaned() call
	// is generally not implemented for PDT invocations. See 4802427.
	//
	if (changed || saved_state == NULL) {
		//
		// Download new sticky params to other nodes
		//
		config_sticky_srv_grp2(gname, st);
	}

	FAULTPT_NET(FAULTNUM_NET_CFG_ST_E, FaultFunctions::generic);

	// Release the locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	return (ret_value);
}

//
// Effects: Configures stickiness parameters in the local ma_servicegrp
//	object. This operation has no effect on non-sticky service groups.
//
// Exceptions:
//	NOSUCH_GROUP:	Can't find group
//
bool
pdts_impl_t::config_sticky_srv_grp1(const network::group_t &gname,
    const network::sticky_config_t &st,
    network::ret_value_t *retval)
{
	ma_servicegrp *servicegrpp = NULL;
	bool changed = false;
	network::ret_value_t ret_value = network::NOSUCH_GROUP;

	NET_DBG(("  pdts_impl_t::config_sticky_srv_grp1()\n"));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_group_equal(gname)) {
			network::sticky_config_t cur_st;
			servicegrpp->ma_get_sticky_config(cur_st);

			if (cur_st.st_timeout == st.st_timeout &&
			    cur_st.st_flags == st.st_flags) {
				changed = false;
				ret_value = network::SUCCESS;
			} else {
				changed = true;
				ret_value = servicegrpp->ma_config_sticky(st);
			}

			break;
		}
	}

	*retval = ret_value;
	return (changed);
}

//
// Effects: Download the new stickiness parameters to all other nodes.
//
void
pdts_impl_t::config_sticky_srv_grp2(const network::group_t &gname,
    const network::sticky_config_t &st)
{
	network::mcobj_ptr	mcobjref;
	Environment		e;

	NET_DBG(("  pdts_impl_t::config_sticky_srv_grp2()\n"));

	//
	// For each node, get the mcobj reference.
	// And "download" the sticky parameters to it.
	// Need to download to all nodes, since all nodes keep track of
	// all service groups and any changes to them.
	//
	for (int i = 1; i < NODEID_MAX; i++) {
		if (mcnet_node_array[i] == NULL) {
		    continue;
		}

		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref))
			continue;

		mcobjref->mc_config_sticky_grp(gname, st, e);

		//
		// The call shouldn't fail except for node crash. No
		// action required, since when node comes up later, it'll
		// get the latest config.
		//
		e.clear();
	}
}

//
// Effects: Configure Round robin parameters. This function has no effect
// service groups which have no RR configured
//
// Parameters:
//	gname:		Service group name
//	rr:		RR config value
//
// Exceptions:
//	NOSUCH_GROUP:	If gname can't be found as a group
//
network::ret_value_t
pdts_impl_t::config_rr_srv_grp(const network::group_t &gname,
    const network::rr_config_t &rr, Environment &e)
{
	network::ret_value_t 	ret_value = network::SUCCESS;
	bool changed = false;

	NET_DBG(("pdts_impl_t::config_rr_srv_grp()\n"));
	NET_DBG(("   group = %s, flags = %x, conn_threshold = %d\n",
	    gname.resource, rr.rr_flags, rr.rr_conn_threshold));

	//
	// Obtain the read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Obtain the write lock on the master service group list.
	// We are not changing the list at all. We're just using this lock
	// to serialize updates to the RR config of *one* service
	// group.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	config_rr_srv_grp_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (config_rr_srv_grp_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		changed = config_rr_srv_grp1(gname, rr, &ret_value);

		FAULTPT_NET(FAULTNUM_NET_CFG_ST_B, FaultFunctions::generic);

		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("   call ckpt_config_rr_srv_grp(%x)\n", rr.rr_flags));

		ckptp->ckpt_config_rr_srv_grp(gname, rr, e);

		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_CFG_ST_A, FaultFunctions::generic);

	} else {
		ret_value = saved_state->retval_state;
		changed = saved_state->changed_state;
	}

	//
	// Broadcast the values to all nodes if the values have changed.
	//
	if (changed || saved_state == NULL) {
		//
		// Download new round robin params to other nodes
		//
		config_rr_srv_grp2(gname, rr);
	}

	FAULTPT_NET(FAULTNUM_NET_CFG_ST_E, FaultFunctions::generic);

	// Release the locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	return (ret_value);
}

//
// Effects: Configures RR parameters in the local ma_servicegrp object.
// This operation has no effect on service groups which does not have RR
// enabled.
//
// Exceptions:
//	NOSUCH_GROUP:	Can't find group
//
bool
pdts_impl_t::config_rr_srv_grp1(const network::group_t &gname,
    const network::rr_config_t &rr, network::ret_value_t *retval)
{
	ma_servicegrp *servicegrpp = NULL;
	bool changed = false;
	network::ret_value_t ret_value = network::NOSUCH_GROUP;

	NET_DBG(("  pdts_impl_t::config_rr_srv_grp1()\n"));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_group_equal(gname)) {
			network::rr_config_t cur_st;
			servicegrpp->ma_get_rr_config(cur_st);
			if ((cur_st.rr_flags == rr.rr_flags) &&
			    cur_st.rr_conn_threshold == rr.rr_conn_threshold) {
				changed = false;
				ret_value = network::SUCCESS;
			} else {
				changed = true;
				ret_value = servicegrpp->ma_config_rr(rr);
			}

			break;
		}
	}

	*retval = ret_value;
	return (changed);
}

//
// Effects: Download the new RR parameters to all other nodes.
//
void
pdts_impl_t::config_rr_srv_grp2(const network::group_t &gname,
    const network::rr_config_t &rr)
{
	network::mcobj_ptr	mcobjref;
	Environment		e;

	NET_DBG(("  pdts_impl_t::config_rr_srv_grp2()\n"));
	NET_DBG(("flag = %x, conn = %d\n", rr.rr_flags,
	rr.rr_conn_threshold));

	//
	// For each node, get the mcobj reference.
	// And "download" the RR parameters to it.
	//
	for (int i = 1; i < NODEID_MAX; i++) {
		if (mcnet_node_array[i] == NULL) {
		    continue;
		}

		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref))
			continue;

		mcobjref->mc_config_rr_grp(gname, rr, e);

		//
		// The call shouldn't fail except for node crash. No
		// action required, since when node comes up later, it'll
		// get the latest config.
		//
		e.clear();
	}
}

//
// Effects: Delete a scalable service group, called from the userland
//   SSM client.  Returns network::SUCCESS if the group is successfully
//   removed.
// Parameters:
//    - gname (IN): Service group name.
//    - e (IN):	Environment variable for passing around exceptions.
// Exceptions:
//    - INVALID_SERVICE_GROUP: if the group cannot be found.
//    - SERVICE_EXISTS: if the service object list is non-empty.  Assume
//	that every master service object has already been removed by
//	separate invocation of rem_scal_service().
//
network::ret_value_t
pdts_impl_t::delete_scal_srv_grp(const network::group_t& gname, Environment &e)
{
	network::ret_value_t	retval = network::SUCCESS;

	NET_DBG(("pdts_impl_t::delete_scal_srv_grp(group=%s)\n",
		gname.resource));

	//
	// Get a read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Get a write lock on the master service group list;
	// we're going to be deleting group from the list.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Get the primary's transaction context from the environment.
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	delete_scal_srv_grp_state *saved_state;

	//
	// Extract the saved state from the context.
	//
	saved_state = (delete_scal_srv_grp_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		retval = delete_scal_srv_grp1(gname);
		if (retval != network::SUCCESS) {
			// Release the locks.
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return (retval);
		}

		FAULTPT_NET(FAULTNUM_NET_RM_GRP_B, FaultFunctions::generic);

		//
		// HA: Now checkpoint the invocation of delete_scal_srv_grp1()
		// at the secondaries.  This will have the effect of
		// deleting the master service group object from the list.
		//
		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("  ckpt_delete_scal_srv_grp()\n"));

		ckptp->ckpt_delete_scal_srv_grp(gname, e);
		ASSERT(!e.exception());
		e.clear();
		FAULTPT_NET(FAULTNUM_NET_RM_GRP_A, FaultFunctions::generic);
	} else {
		retval = saved_state->retval_state;
	}

	//
	// Remove the grp on the slave side
	//
	Environment env;
	delete_scal_srv_grp2(gname, env);

	//
	// No exception is passed back from delete_scal_srv_grp2(). Exception
	// is cleared before return. In case we do have an exception to be
	// passed back from delete_scal_srv_grp2(), we need to remap the
	// exception and pass it back in e.
	//
	env.clear();

	//
	// Release the locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_RM_GRP_E, FaultFunctions::generic);

	//
	// SCMSGS
	// @explanation
	// The service group by that name is no longer known by the scalable
	// services framework.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "Service group '%s' deleted", gname.resource);

	return (retval);

}

//
// Effects: Delete a scalable service group "gname" from the PDT service.
//   Returns network::SUCCESS if the group is successfully removed.
//   This routine is called only by delete_scal_srv_grp() and by
//   ckpt_delete_scal_srv_grp().
// Parameters:
//    - gname (IN): Service group name.
//
// Exceptions:
//    - INVALID_SERVICE_GROUP: if the group cannot be found.
//    - SERVICE_EXISTS: if the service object list is non-empty.  Assume
//	that every master service object has already been removed by
//	separate invocation of rem_scal_service().
//
network::ret_value_t
pdts_impl_t::delete_scal_srv_grp1(const network::group_t& gname)
{
	network::ret_value_t	retval = network::SUCCESS;
	ma_servicegrp *servicegrpp;

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	NET_DBG(("  pdts_impl_t::delete_scal_srv_grp1()\n"));

	servicegrpp = find_ma_servicegrp(gname);
	if (servicegrpp == NULL) {
		//
		// Service group does not exist.
		// Return error code.
		//
		return (network::INVALID_SERVICE_GROUP);
	}

	//
	// Assume that every master serviceobj belonging to the
	// group has already been deleted by separate calls to
	// to rem_scal_service().
	//
	if (!servicegrpp->ma_serviceobj_list.empty()) {
		return (network::SERVICE_EXISTS);
	}

	//
	// Remove the group from the list.
	//
	(void) ma_servicegrplist.erase(servicegrpp);
	delete servicegrpp;

	return (retval);
}

//
// Effects: Tell all the nodes to delete this scalable service group
//   identified by "gname"  from their innards.
//
// Parameters:
//    - gname (IN): Service group name.
//    - e (IN):	Environment variable for passing around exceptions.
//
// Exceptions:
//
void
pdts_impl_t::delete_scal_srv_grp2(const network::group_t& gname,
    Environment &e)
{
	network::mcobj_ptr	mcobjref;

	NET_DBG(("  pdts_impl_t::delete_scal_srv_grp2()\n"));

	//
	// For each node, get the mcobj reference.
	//
	for (int i = 1; i < NODEID_MAX; i++) {
		if (mcnet_node_array[i] == NULL) {
		    continue;
		}
		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref))
			continue;
		mcobjref->mc_deregister_grp(gname, e);
		e.clear();
	}
}

//
// Effects: Register a new scalable service, called from the userland
//   SSM client.  Returns network::SUCCESS if the service object was
//   successfully added to the group identified by "gname."  The service
//   object is identified by the triple <protocol, ip address, port>.
// Parameters:
//    - gname (IN): Service group name.
//    - sap (IN):    Service access point <protocol, ip addr, port>
//    - e (IN):	Environment variable for passing around exceptions.
// Exceptions:
//    - INVALID_SERVICE_GROUP: if "gname" doesn't name a service group.
//    - SERVICE_CONFLICTS: if the service already exists in any group.
//
network::ret_value_t
pdts_impl_t::add_scal_service(const network::group_t& gname,
    const network::service_sap_t& sap, Environment& e)
{
	ma_servicegrp			*servicegrpp = NULL;
	ma_servicegrp			*found_servicegrpp = NULL;

	upgrade_inaddr(sap.laddr);

	NET_DBG(("pdts_impl_t::add_scal_service()\n"));
	NET_DBG(("    group = %s\n", gname.resource));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(sap.protocol),
	    network_lib.ipaddr_to_string(sap.laddr),
	    sap.lport));

	pdts_lock.rdlock();			// read lock the PDT state
	ma_servicegrplist_lock.rdlock();	// read lock the group list

	//
	// Get the primary's transaction context from the environment.
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	add_scalable_service_state *saved_state;
	network::lb_policy_t policy;

	//
	// Extract the saved state from the context.
	//
	saved_state = (add_scalable_service_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		//
		// First, ensure that the group identified by "gname" is a
		// legitimate group.
		//
		found_servicegrpp = find_ma_servicegrp(gname);
		if (found_servicegrpp == NULL) {
			// Oops. We didn't find the group.
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return (network::INVALID_SERVICE_GROUP);
		} // end if-then

		//
		// Second, check if it's a duplicate request (the same SAP
		// being added to the same group again). If yes, return
		// SUCCESS. This effectively make this operation idempotent.
		//
		found_servicegrpp->ma_serviceobj_list_lock.rdlock();
		if (found_servicegrpp->equal(sap)) {
			found_servicegrpp->ma_serviceobj_list_lock.unlock();
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return (network::SUCCESS);
		}
		found_servicegrpp->ma_serviceobj_list_lock.unlock();
		policy = found_servicegrpp->getpolicy();

		//
		// Third, ensure that the same service is not
		// already contained in some other group.
		//
		ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
		for (; (servicegrpp = iter.get_current()) != NULL;
		    iter.advance()) {
			//
			// If we are a sticky wildcard service, make sure
			// there is no other sticky wildcard service on the
			// same IP address already in any other service group.
			// However if there is a sticky wildcard service
			// in the same group (gname) we can still go ahead
			// and add us (sticky wildcard service) to the group.
			// Or if the same SAP exists in another group then
			// we cannot add us to the group.
			//
			if (!servicegrpp->is_group_equal(gname)) {
			    if ((is_sticky_wildcard(policy) &&
				is_sticky_wildcard(servicegrpp->getpolicy()) &&
				servicegrpp->ip_matches(sap)) ||
				servicegrpp->matches(sap)) {
				ma_servicegrplist_lock.unlock();
				pdts_lock.unlock();
				return (network::SERVICE_CONFLICTS);
			    } // end if-then
			} // end if-then
		} // end for

		// Create the master service object identified by "sap"
		// and insert into service group named "gname."
		register_service1(gname, sap);

		FAULTPT_NET(FAULTNUM_NET_ADD_SS_B, FaultFunctions::generic);

		//
		// HA: Now checkpoint the invocation of register_service1()
		// at the secondaries.  This will have the effect of
		// creating a new master service object, append it to the
		// list of the group object identified by "gname."
		//
		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("  ckpt_add_scalable_service()\n"));

		ckptp->ckpt_add_scalable_service(gname, sap, e);

		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_ADD_SS_A, FaultFunctions::generic);

		//
		// Now distribute the new service to all nodes.
		//
		register_service2(found_servicegrpp, sap);
	} else {
		servicegrpp = find_ma_servicegrp(gname);
		ASSERT(servicegrpp != NULL);

		//
		// Now distribute the new service to all nodes.  This is
		// okay to do because this routine is idempotent.
		//
		register_service2(servicegrpp, sap);
	}

	//
	// Unlock the master service group list.
	//
	ma_servicegrplist_lock.unlock();

	//
	// Unlock the PDT server state.
	//
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_ADD_SS_E, FaultFunctions::generic);

	e.clear();	// XXX. Why do we clear this?

	//
	// SCMSGS
	// @explanation
	// A specific service known by its unique name SAP (service access
	// point), the three-tuple, has been created in the designated group.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "Service object [%s, %s, %d] created in group '%s'",
	    network_lib.protocol_to_string(sap.protocol),
	    network_lib.ipaddr_to_string(sap.laddr),
	    sap.lport, gname.resource);

	return (network::SUCCESS);
}

//
// Effects: Delete a scalable service object belonging to the specified group
//    identified by "gname."  The object is identified by its sap, defined by
//    <protokind, ip_addr, port> triple.
//    This routine is called from the SSM client.
// Parameters:
//    - gname (IN): Service group name.
//    - sap (IN):  Service access point <protocol, ip address, port>
//    - e (IN):	Environment variable for passing around exceptions.
//
network::ret_value_t
pdts_impl_t::rem_scal_service(const network::group_t& gname,
    const network::service_sap_t& sap, Environment& e)
{
	ma_servicegrp			*servicegrpp = NULL;
	network::service_sap_t		saved_sap;
	network::ret_value_t		saved_retval, retval;
	bool				saved_updated_pdt;
	bool				updated_pdt;

	upgrade_inaddr(sap.laddr);

	NET_DBG(("pdts_impl_t::rem_scal_service()\n"));
	NET_DBG(("    group = %s\n", gname.resource));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(sap.protocol),
	    network_lib.ipaddr_to_string(sap.laddr),
	    sap.lport));

	// Get a read lock on the PDT server.
	pdts_lock.rdlock();

	// Get a read lock on the master service group list.
	ma_servicegrplist_lock.rdlock();

	// Get the primary's transaction context from the environment.
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	rem_scal_service_state *saved_state;

	// Extract the saved state from the context.
	saved_state = (rem_scal_service_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		//
		// Check to see if the service group "gname" is
		// already in the master service group list.
		//
		servicegrpp = find_ma_servicegrp(gname);
		if (servicegrpp == NULL) {
			// The service group does not exist.
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return (network::INVALID_SERVICE_GROUP);
		}

		//
		// "updated_pdt" is a boolean that tells us whether the
		// service we deregistered resulted in changing the PDT too.
		// This is an optimization to avoid downloaded unchanged
		// PDTs unnecessarily.
		//
		retval = deregister_service1(gname, sap, updated_pdt);
		if (retval != network::SUCCESS) {
			// Unlock the master service group list.
			ma_servicegrplist_lock.unlock();

			// Unlock the PDT server state.
			pdts_lock.unlock();

			return (retval);
		}

		FAULTPT_NET(FAULTNUM_NET_RM_SS_B, FaultFunctions::generic);

		//
		// HA: Now checkpoint the invocation of deregister_service1()
		// at the secondaries.  This will have the effect of
		// deleting a master service object given the sap,
		// remove it from the list and decrement the global counter.
		//
		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("  ckpt_rem_scal_service()\n"));

		ckptp->ckpt_rem_scal_service(gname, sap, e);
		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_RM_SS_A, FaultFunctions::generic);

		//
		// This routine tells the GIF nodes to remove the
		// service from its knowledge.
		//
		Environment env;
		deregister_service2(sap, servicegrpp, updated_pdt, env);

		//
		// No exception is passed back from deregister_service2().
		// Exception is cleared before return. In case we do have
		// an exception to be passed back from deregister_service2(),
		// we need to remap the exception and pass it back in e.
		//
		env.clear();
	} else {
		// If there's saved state then get the return value and
		// and the sap.
		saved_retval = saved_state->retval_state;
		saved_sap = saved_state->sap_state;
		saved_updated_pdt = saved_state->updated_pdt_state;

		// Get the service group given the "gname."
		servicegrpp = find_ma_servicegrp(gname);
		ASSERT(servicegrpp != NULL);

		// Tell the GIF nodes to remove this service.
		Environment env;
		deregister_service2(saved_sap, servicegrpp,
		    saved_updated_pdt, env);

		//
		// No exception is passed back from deregister_service2().
		// Exception is cleared before return. In case we do have
		// an exception to be passed back from deregister_service2(),
		// we need to remap the exception and pass it back in e.
		//
		env.clear();

		// Unlock the master service group list.
		ma_servicegrplist_lock.unlock();

		// Unlock the PDT server state.
		pdts_lock.unlock();
		return (saved_retval);
	}

	// Unlock the master service group list.
	ma_servicegrplist_lock.unlock();

	// Unlock the PDT server state.
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_RM_SS_E, FaultFunctions::generic);

	//
	// SCMSGS
	// @explanation
	// A specific service known by its unique name SAP (service access
	// point), the three-tuple, has been deleted in the designated group.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE, MESSAGE,
	    "Service object [%s, %s, %d] removed from group '%s'",
	    network_lib.protocol_to_string(sap.protocol),
	    network_lib.ipaddr_to_string(sap.laddr),
	    sap.lport, gname.resource);

	return (network::SUCCESS);
}

//
// Effects: This routine is called by rem_scal_service() and by
//    ckpt_rem_scal_service() only.  It deregisters the service object
//    identified by "ssap" in the service group that contains that service
//    object.  Returns network::SUCCESS.
// Parameters:
//	- gname (IN): group name
//	- ssap (IN): Service access point <protocol, ip address, port>
//	- updated_pdt (OUT): true if we updated the PDT, false otherwise
// Exceptions:
//    -  network::NOSUCH_SERVICE
//	    If the object cannot be found.
//
network::ret_value_t
pdts_impl_t::deregister_service1(const network::group_t& gname,
	const network::service_sap_t &ssap, bool& updated_pdt)
{
	ma_servicegrp		*servicegrpp = NULL;
	network::ret_value_t	retval = network::SUCCESS;

	NET_DBG(("  pdts_impl_t::deregister_service1()\n"));
	NET_DBG(("    group = %s\n", gname.resource));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Now we remove the service object from the specific master
	// service group that has the service object identified
	// by the "ssap."
	//

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	// Find the master service group given the "gname."
	servicegrpp = find_ma_servicegrp(gname);
	ASSERT(servicegrpp != NULL);

	//
	// Now check for existence of the service object named by "ssap"
	// in the service group.
	//
	if (servicegrpp->matches(ssap)) {
		//
		// We've found the group that has a service object
		// named by "ssap."  Now remove the service object.
		// from the group's service object list. Modify the
		// PDT if necessary.
		//
		updated_pdt = servicegrpp->ma_remove_service_object(ssap);
	} else {
		//
		// Didn't find the service.  Just return to caller.
		//
		updated_pdt = false;
		return (network::NOSUCH_SERVICE);
	}

	return (retval);
}

//
// Effects: This routine is called by rem_scal_service() only.
//    It tells all the gif nodes to deregister the service identified by
//    "ssap." in the service group that contains that service
//    object.
// Parameters:
//    - ssap (IN): Service access point <protocol, ip address, port>
//    - servicegrpp (IN): Service group object pointer.
//    - updated_pdt (IN): Tells us whether the PDT for service group "gname"
//		was modified. True if modified, false otherwise.
//    - e (IN):	Environment variable for passing around exceptions.
// Exceptions:
//
void
pdts_impl_t::deregister_service2(const network::service_sap_t &ssap,
	ma_servicegrp *servicegrpp, bool updated_pdt, Environment &e)
{
	network::mcobj_ptr	mcobjref;

	NET_DBG(("  pdts_impl_t::deregister_service2()\n"));
	NET_DBG(("    sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	// Tell each mcobj to deregister this service.
	for (int i = 1; i < NODEID_MAX; i++) {
		if (mcnet_node_array[i] == NULL) {
			continue;
		}
		mcobjref = mcnet_node_array[i]->get_cbref();
		if (CORBA::is_nil(mcobjref))
			continue;

		mcobjref->mc_deregister_services(ssap, e);
		e.clear();
	}

	//
	// Distribute the PDT to the interested gif nodes.
	// If  "updated_pdt" is true (meaning that
	// we updated the PDT), then distribute the PDT.  Notice that
	// knowing the PDT was updated is an optimization to avoid
	// downloading PDTs unnecessarily.
	//
	if (updated_pdt) {
		servicegrpp->ma_distribute_pdt(0,
		    ma_servicegrp::COLLECT_ALL, e);
		e.clear();
	} // end if
}

//
// Effects: Returns true if the routine successfully adds the node id "node"
//	to the configuration list for a service group identified by "gname."
//	If true, returns in "retval" out parameter, the result of attempting
//	to add to the configuration list.
//  	This routine is called from the userland SSM client.
// Parameters:
//    - gname (IN):  structure containing the group names.
//    - node (IN): the node number which will be added to the
//		list.
//    - retval (OUT): The result of attempting to add to the config list.
//    - e (IN):	Environment variable for passing around exceptions.
//
bool
pdts_impl_t::add_nodeid1(const network::group_t &gname, nodeid_t node,
    network::ret_value_t *retval)
{
	ma_servicegrp *servicegrpp = NULL;
	bool found = false;
	network::ret_value_t ret_value = network::SUCCESS;

	NET_DBG(("  pdts_impl_t::add_nodeid1()\n"));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_group_equal(gname)) {
			found = true;
			ret_value =
			    servicegrpp->ma_add_configlist_entry(node);
		}
	}

	*retval = ret_value;
	return (found);
}

//
// Effects: Add a new service instance on node "node" to all
// 	scalable services belonging to the specified group.
//  	This routine is called from the userland SSM client.
// Parameters:
//    - gname (IN):  structure containing the group names.
//    - node (IN): the node number which will be added to the
//		list.
//    - e (IN):	Environment variable for passing around exceptions.
//
network::ret_value_t
pdts_impl_t::add_nodeid(const network::group_t &gname, nodeid_t node,
    Environment &e)
{
	//
	// Now add the server instance's node to the config_inst list
	// all the services belonging to this group.
	//

	network::ret_value_t 	ret_value;
	bool found = false;

	NET_DBG(("pdts_impl_t::add_nodeid(%d)\n", node));
	NET_DBG(("   group = %s\n", gname.resource));

	ASSERT(node >= 1 && node <= NODEID_MAX);

	//
	// Obtain the read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Obtain the write lock on the master service group list.
	//
	ma_servicegrplist_lock.rdlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	add_nodeid_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (add_nodeid_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		found = add_nodeid1(gname, node, &ret_value);

		FAULTPT_NET(FAULTNUM_NET_ADD_NODE_B, FaultFunctions::generic);

		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("   call ckpt_add_nodeid(%d)\n", node));
		ckptp->ckpt_add_nodeid(gname, node, e);
		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_ADD_NODE_A, FaultFunctions::generic);

	} else {
		ret_value = saved_state->retval_state;
		found = saved_state->found_state;
	}

	//
	// If found is false, the specified service group doesnt exist.
	// Set ret_value to NOSUCH_GROUP
	//
	if (!found) {
		ret_value = network::NOSUCH_GROUP;
	}

	// Release the locks.
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	return (ret_value);
}

//
// Effects: Removes the node "node" from the master service group object
//  named by "gname" and returns true if the group was found and false
//  otherwise; the out parameter "pdt_updated" returns true if the PDT
//  was updated, false otherwise.  The out parameter is an optimization
//  to tell the caller whether or not to download the PDT.
//
// Parameters:
//   - gname (IN):	Service group name.
//   - node (IN):	Node id
//   - pdt_updated (OUT): true if the PDT was updated, false otherwise.
//
// Exceptions:
//   - network::NO_NODEID - if "node" doesn't name a node id in the group
//				config list; returns NO
//   - network::NOSUCH_GROUP   - if "gname" doesn't name a legitimate group.
//
network::ret_value_t
pdts_impl_t::remove_nodeid1(const network::group_t &gname, nodeid_t node,
			    bool &pdt_updated)
{
	ma_servicegrp *servicegrpp = NULL;
	bool removed = false;
	bool found_group = false;
	network::ret_value_t ret_value = network::SUCCESS;

	NET_DBG(("  pdts_impl_t::remove_nodeid1()\n"));

	pdt_updated = false;

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	//
	// Iterate over the master service group list, looking for the
	// group whose name is exactly equal to "gname."  When found
	// remove that node from the group's config list; remove node
	// from instance lists of any service object where an instance
	// is running on that node.
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_group_equal(gname)) {
			found_group = true;

			// Try to remove from the group config list.
			removed = servicegrpp->ma_remove_node_from_clist(node);
			//
			// If we didn't remove anything from the group config
			// list, then raise exception NO_NODEID.
			//
			if (!removed) {
				return (network::NO_NODEID);
			}

			//
			// Try to remove the node id "node" from the group
			// instance list and from the instance lists of
			// each service.
			//
			removed = servicegrpp->ma_remove_node_from_ilist(node);
			if (removed) {
				// Now update the PDT with the new changes.
				servicegrpp->instancelist_lock.wrlock();
				servicegrpp->update_buckets();
				servicegrpp->instancelist_lock.unlock();
				pdt_updated = true;
			}
			//
			// If we didn't remove anything from the instance
			// lists, that's okay.  No instance was running on
			// on node "node" though it's configured that way.
			//
			break;
		} // end if
	} // end for

	// If we didn't find the group given "gname" then raise exception.
	if (!found_group) {
		ret_value = network::NOSUCH_GROUP;
	}

	return (ret_value);
}

void
pdts_impl_t::remove_nodeid2(const network::group_t &gname)
{
	ma_servicegrp *servicegrpp = NULL;
	Environment e;

	NET_DBG(("  pdts_impl_t::remove_nodeid2()\n"));

	//
	// Distribute the PDT to the interested gif nodes.
	// Find the right master service group whose PDT we wish to download.
	//
	servicegrpp = find_ma_servicegrp(gname);
	if (servicegrpp != NULL) {
		servicegrpp->ma_distribute_pdt(0, ma_servicegrp::COLLECT_ALL,
		    e);
		//
		// Gif node is dead.  There's nothing we can do so just
		// clear exception and bail out.
		//
		e.clear();
	} // end if
}

//
// Effects: Remove an existing service instance from node "node" an existing
//   scalable service.  This routine is called from the userland SSM client.
//
// Parameters:
//   - gname (IN):	Service group name.
//   - node (IN):	Node id
//   - e (IN):	Environment variable for passing around exceptions.
//
network::ret_value_t
pdts_impl_t::remove_nodeid(const network::group_t &gname, nodeid_t node,
    Environment &e)
{
	//
	// Now remove the server instance's node id from the
	// config_inst_list for all the services belonging to
	// the specified scalable service group.
	//

	NET_DBG(("pdts_impl_t::remove_nodeid(%d)\n", node));
	NET_DBG(("  group = %s\n", gname.resource));

	bool			pdt_updated = false;
	network::ret_value_t 	ret_value;

	ASSERT(node >= 1 && node <= NODEID_MAX);

	// Acquire locks
	//
	//
	pdts_lock.rdlock();
	ma_servicegrplist_lock.rdlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	remove_nodeid_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (remove_nodeid_state *)pctxp->get_saved_state();

	if (saved_state == NULL) {
		// 1. Remove node id from config list and instance lists.
		ret_value = remove_nodeid1(gname, node, pdt_updated);

		FAULTPT_NET(FAULTNUM_NET_RM_NODE_B, FaultFunctions::generic);

		network::ckpt_ptr ckptp = get_checkpoint();
		NET_DBG(("  call ckpt_remove_nodeid(%d)\n", node));

		// 2. Checkpoint invocation to secondaries.
		ckptp->ckpt_remove_nodeid(gname, node, e);
		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_RM_NODE_A, FaultFunctions::generic);

	} else {
		ret_value = saved_state->retval_state;
		pdt_updated = saved_state->pdt_updated_state;
	}

	//
	// 3. Distribute changes to slaves if the node was removed from
	// the group's config list.  Notice that this conditional is really
	// an optimization to avoid downloading the PDT if nothing changed.
	//
	if (pdt_updated) {
		NET_DBG(("  PDT was updated\n"));
		remove_nodeid2(gname);
	}

	//
	// Release locks
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	return (ret_value);
}

//
// Effects:  Query whether PDT server knows about scalable services as
//   represented by [ip address, port], in the specified group.
//   Returns true if the service is registered, else return false.
//
// Parameters:
//   - sap (IN):   Service access point triple <protocol, ip addr, port>
//   - e (IN):	Environment variable for passing around exceptions.
//
bool
pdts_impl_t::is_scal_service(const network::service_sap_t &sap, Environment&)
{
	bool			result;
	ma_servicegrp		*servicegrpp;

	upgrade_inaddr(sap.laddr);

	NET_DBG(("pdts_impl_t::is_scal_service()\n"));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(sap.protocol),
	    network_lib.ipaddr_to_string(sap.laddr),
	    sap.lport));

	result = false;

	//
	// Obtain the read locks for the pdts and the
	// master service list.
	//
	pdts_lock.rdlock();
	ma_servicegrplist_lock.rdlock();
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	//
	// Traverse the master service group list and check if the
	// specified sap exists.
	//
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->matches(sap)) {
			result = true;
			break;
		}
	}

	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	return (result);
}

//
// Effects:  Query whether PDT server knows about scalable services as
//   represented by [ip address, port], in the specified group.
//   Returns true if the service is registered, else return false.
//
// Parameters:
//   - group (IN):	Service group name
//   - e (IN):	Environment variable for passing around exceptions.
//
bool
pdts_impl_t::is_scal_service_group(const network::group_t &gname, Environment&)
{
	ma_servicegrp *servicegrpp;

	NET_DBG(("pdts_impl_t::is_scal_service_group()\n"));
	NET_DBG(("  group = %s\n", gname.resource));

	//
	// Obtain the read locks for the pdts and the
	// master service list.
	//
	pdts_lock.rdlock();
	ma_servicegrplist_lock.rdlock();

	//
	// Traverse the master service list and check if the specified
	// group exists.
	//

	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		//
		// Must check if specified sevice grp exists
		//
		if (servicegrpp->is_group_equal(gname)) {
			break;
		}
	}

	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	if (servicegrpp != NULL)
		return (true);

	return (false);
}

//
// Effects:  Returns true if we found the right master service object
//   identified by "ssap" and that the instance was registered with it.
//   Else returns false.
// Parameters:
//   - ssap (IN):    service access point <protocol, ip adrss, port>
//   - node (IN):    node where the instance has started up
//
bool
pdts_impl_t::register_instance1(const network::service_sap_t &ssap,
    nodeid_t node)
{
	ma_servicegrp		*servicegrpp = NULL;
	bool			found = false;
	char			nodename[NODENAME_MAX];

	NET_DBG(("  pdts_impl_t::register_instance1(%d)\n", node));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->matches(ssap)) {
			found = true;

			MC_PROBE_1(register_instance, "mc net pdts", "",
				tnf_uint, node, node);

			//
			// Call ma_register_instance() only if the node
			// exists in the config_inst_list. A node will be
			// added to the config_inst_list using the
			// add_nodeid() call.
			//
			if (servicegrpp->ma_is_node_in_configlist(node)) {
				if (servicegrpp->ma_register_instance(node,
				    ssap)) {
					servicegrpp->instancelist_lock.wrlock();
					servicegrpp->update_buckets();
					servicegrpp->instancelist_lock.unlock();
				}
				print_dbg_msgs(ssap, node, REGISTER_INS);
				MC_PROBE_3(register_instance,
				    "mc net pdts", "",
				    tnf_string, laddr,
				    network_lib.ipaddr_to_string(ssap.laddr),
				    tnf_int, lport, ssap.lport,
				    tnf_uint, nodeid, node);

				//
				// If the server instance binds to INADDR_ANY
				// and a specific port, then the entire
				// ma_servicegrp list will have to be walked.
				// This is because, the server instance can
				// handle packets belonging to any server
				// ip address, but specific port.
				//
				// Optimization to get out of the
				// loop if laddr is specific.
				//
				if (!addr6_is_inaddr_any(ssap.laddr)) {
					return (found);
				}
			} else {
				network_lib.get_nodename_from_nodeid(
				    node, nodename);
				//
				// SCMSGS
				// @explanation
				// The specified scalable service could not be
				// started on this node because the node is
				// not in the list of configured nodes for
				// this particluar service.
				// @user_action
				// If the specified service needs to be
				// started on this node, use clresourcegroup
				// to add the node to the list of configured
				// nodes for this service and then restart the
				// service.
				//
				(void) cl_net_syslog_msgp->log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "Node %s not in list of configured nodes",
				    nodename);
				return (false);
			}
		}
	}

	return (found);
}

//
// Effects:  This routine distributes the changed PDT to the gifnode for
//   the master service group that owns the service object identified by
//   the "ssap."  If the service object cannot be found, this routine has
//   no effect.
// Parameters:
//   - ssap (IN):    service access point <protocol, ip adrss, port>
//   - e (IN):	Environment variable for passing around exceptions.
//
void
pdts_impl_t::register_instance2(const network::service_sap_t &ssap,
    Environment &e)
{
	ma_servicegrp		*servicegrpp = NULL;

	NET_DBG(("  pdts_impl_t::register_instance2()\n"));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->matches(ssap)) {
			servicegrpp->ma_distribute_pdt(0,
			    ma_servicegrp::COLLECT_ALL, e);
			e.clear();
		}
	}
}

//
// Effects: Register a new instance with the PDT server.  The PDT server is
//   now aware of this instance and the instance may be the target of packet
//   forwarding. Registering the same instance more than once is equivalent
//   to exactly once.
// Parameters:
//   - ssap (IN):    service access point <protocol, ip adrss, port>
//   - node (IN):    node where the instance has started up
//   - e (IN):	Environment variable for passing around exceptions.
//
void
pdts_impl_t::register_instance(const network::service_sap_t &ssap,
    nodeid_t node, Environment &e)
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_impl_t::register_instance(%d)\n", node));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	ASSERT(node >= 1 && node <= NODEID_MAX);

	MC_PROBE_1(register_instance, "mc net pdts", "",
		tnf_uint, node, node);

	//
	// Get the primary's transaction context from the environment.
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	register_inst_state *saved_state;

	//
	// Extract the saved state from the context.
	//
	saved_state = (register_inst_state *)pctxp->get_saved_state();

	//
	// Acquire a read lock on the PDT server state.
	//
	pdts_lock.rdlock();
	//
	// Acquire a write lock on the master service list.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// HA strategy:
	//
	// The strategy here is to divide up this routine into two sections:
	// 1. Change of state LOCAL to the PDT server.
	// 2. Change of state EXTERNAL to the PDT server.
	// For section 1 we pull out the code into a separate routine;
	// invoke routine, and then checkpoint that invocation so it is
	// called on the secondaries.
	//

	//
	// HA: The lack of a state object merely means that the primary
	//   PDT server failed before the checkpoint.  There is no external
	//   communication with entities outside the immediate node, so
	//   we re-execute the entire method during the retry.
	//
	if (saved_state == NULL) {
		bool found = register_instance1(ssap, node);
		if (!found) {
			//
			// Release the locks on the state and return.
			//
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return;
		}

		//
		// HA: Note that if we fail here but before the checkpoint then
		// no state has changed on the secondaries, and we can redo
		// this invocation.
		//

		FAULTPT_NET(FAULTNUM_NET_REG_INST_B, FaultFunctions::generic);

		NET_DBG(("  call ckpt_register_inst(node=%d)\n", node));

		network::ckpt_ptr ckptp = get_checkpoint();

		//
		// HA: Checkpoint the invocation to the secondaries.  This
		// checkpoint causes state changes to occur at the
		// secondaries: register the instance with the
		// appropriate master service object.
		//
		ckptp->ckpt_register_inst(ssap, node, e);
		ASSERT(!e.exception());
		e.clear();

		//
		// HA: Note that if we fail after the checkpoint, everything is
		// peachy keen because we've successfully changed state on the
		// secondaries, and the new primary after the failover knows
		// all that the old primary knew.
		//

		FAULTPT_NET(FAULTNUM_NET_REG_INST_A, FaultFunctions::generic);
	}

	//
	// HA: The existence of saved state means that the state on the
	// secondaries was successfully changed and they now know about
	// the newly registered instance (reflected in the PDT tables).
	//

	//
	// Now tell the mcobj at the GIF node about the registration of
	// this instance.  We send the modified PDT table in its entirety
	// to the mcobj without distinguishing between modifying a few
	// buckets versus initializing the entire table.
	//
	// HA: Note that this routine may be invoked again during a retry
	// without ill effect because it is idempotent.
	//
	Environment env;
	register_instance2(ssap, env);

	//
	// No exception is passed back from register_instance2().
	// Exception is cleared before return. In case we do have
	// an exception to be passed back from register_instance2(),
	// we need to remap the exception and pass it back in e.
	//
	env.clear();

	//
	// Release the locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_REG_INST_E, FaultFunctions::generic);
}

//
// Effects: This helper routine returns true if the master service object
//   identified by "ssap" was found on the master service list and the
//    instance was deregistered with that object.
//
// Parameters:
//   - ssap (IN):    service access point <protocol, ip adrss, port>
//   - node (IN):    node where the instance used to exist.
//
bool
pdts_impl_t::deregister_instance1(const network::service_sap_t& ssap,
    nodeid_t node)
{
	ma_servicegrp			*servicegrpp;
	bool				found = false;

	NET_DBG(("   pdts_impl_t::deregister_instance1(%d)\n", node));

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->matches(ssap)) {
			if (servicegrpp->ma_deregister_instance(node, ssap)) {
				servicegrpp->instancelist_lock.wrlock();
				servicegrpp->update_buckets();
				servicegrpp->instancelist_lock.unlock();
			} // end if
			print_dbg_msgs(ssap, node, DEREGISTER_INS);
			MC_PROBE_3(deregister_instance,
			    "mc net pdts", "",
			    tnf_string, laddr,
			    network_lib.ipaddr_to_string(ssap.laddr),
			    tnf_int, lport, ssap.lport,
			    tnf_uint, nodeid, node);

			found = true;
			//
			// Comments in register_instance() holds good here too.
			//
			if (!addr6_is_inaddr_any(ssap.laddr)) {
				break;
			} // end if
		} // end if
	} // end for

	return (found);
}

void
pdts_impl_t::deregister_instance2(nodeid_t node,
    const network::service_sap_t& ssap, Environment &e)
{
	ma_servicegrp		*servicegrpp = NULL;

	NET_DBG(("   pdts_impl_t::deregister_instance2()\n"));

	//
	// Iterate over the master service object list, looking for the
	// object with sap "ssap."
	//

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->matches(ssap)) {
			servicegrpp->ma_distribute_pdt(node,
			    ma_servicegrp::DEREGISTER_INSTANCE, e);
			//
			// Exceptions are ok here.
			//
			if (e.exception()) {
				e.clear();
			}

			//
			// Comments in register_instance() holds good here too.
			//
			if (!addr6_is_inaddr_any(ssap.laddr)) {
				break;
			}
		}
	}
}

//
// Effects: Deregister an instance of the service from the PDT server.
//
// Parameters:
//   - ssap (IN):    service access point <protocol, ip adrss, port>
//   - node (IN):    node where the instance used to exist.
//   - e (IN):		Environment variable for passing around exceptions
//
// Notes.
//   1. This routine is currently called ONLY when a server instance
//   shuts down (or fails) and stops listening on a port.  Invariant:
//   the only instances that may be deregistered with the PDT server are
//   those that registered.
//

void
pdts_impl_t::deregister_instance(const network::service_sap_t& ssap,
    nodeid_t node, Environment &e)
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_impl_t::deregister_instance(%d)\n", node));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	ASSERT(node >= 1 && node <= NODEID_MAX);

	MC_PROBE_1(deregister_instance, "mc net pdts", "",
		tnf_uint, node, node);

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	deregister_inst_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (deregister_inst_state *)pctxp->get_saved_state();

	//
	// Get read lock on PDTS server state.
	//
	pdts_lock.rdlock();

	//
	// Acquire read lock on the master service list.
	//
	ma_servicegrplist_lock.rdlock();

	if (saved_state == NULL) {
		//
		// Call the helper function that does the work of traversing
		// the master service list.
		//
		bool found = deregister_instance1(ssap, node);
		if (!found) {
			//
			// Release locks.
			//
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return;
		}

		//
		// HA: Note that if we fail here but BEFORE the checkpoint then
		// no state has changed on the secondaries, and we can redo
		// this invocation.
		//

		FAULTPT_NET(FAULTNUM_NET_DEREG_INST_B,
		    FaultFunctions::generic);

		NET_DBG(("  call ckpt_deregister_instance(node=%d)\n", node));

		network::ckpt_ptr ckptp = get_checkpoint();

		//
		// HA: Checkpoint the invocation to the secondaries.
		// This causes state changes to occur at all the
		// secondaries: find the correct master service
		// object and deregister the instance (of course,
		// this modifies the PDT table).
		//
		ckptp->ckpt_deregister_inst(ssap, node, e);
		ASSERT(!e.exception());
		e.clear();

		//
		// HA: Note that if we fail AFTER the checkpoint, all is
		// well in the world because we've successfully changed
		// state on the secondaries, and the new primary after
		// the failover knows all that the old primary knew.
		//
		FAULTPT_NET(FAULTNUM_NET_DEREG_INST_A,
		    FaultFunctions::generic);
	}

	//
	// HA: If there's saved state, this means that the method invocation
	// was interrupted by failure, and a transaction object was created for
	// this mini-transaction.  The checkpoint checkpoints the invocation
	// that deregisters the instance at the secondaries; the last thing
	// done is to create the transaction state object  What we
	// don't know is if the mcobjs at the GIF nodes have been told.
	// It turns out that we don't care what exactly that saved state is.
	//
	// Now tell the mcobj at the GIF node about the deregistration of
	// this instance.
	// HA: Note that this routine may be invoked again during a retry
	// without ill effect.
	//
	Environment env;
	deregister_instance2(node, ssap, env);

	//
	// No exception is passed back from deregister_instance2().
	// Exception is cleared before return. In case we do have
	// an exception to be passed back from deregister_instance2(),
	// we need to remap the exception and pass it back in e.
	//
	env.clear();

	//
	// Release locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_DEREG_INST_E, FaultFunctions::generic);
}

//
// Look up the lbobj given the sap.
//
network::lbobj_i_ptr
pdts_impl_t::get_lbobj(const network::group_t &group_name, Environment &)
{
	ma_servicegrp	*servicegrpp = NULL;
	bool		found = false;

	//
	// Given sap, find the ma_servicegrp
	//
	pdts_lock.wrlock();
	ma_servicegrplist_lock.rdlock();
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicegrpp->is_group_equal(group_name)) {
			// found it; just break
			found = true;
			break;
		}
	}
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	//
	// Couldn't find service object. This is an error.
	//
	if (!found) {
		return (network::lbobj_i::_nil());
	}

	//
	// Given ma_servicegrp, get lbobj object
	//
	ASSERT(servicegrpp);
	return (servicegrpp->lb->get_objref_ii());
}

//
// Effects:  Helper routine.  Returns the master service group object
//   identified by "group" in the master service group list if
//   it exists, else returns NULL.
//
// Parameters:
//   - gname (IN):    Service group name
//
ma_servicegrp*
pdts_impl_t::find_ma_servicegrp(const network::group_t &gname)
{
	ma_servicegrp *servicep = NULL;

	//
	// Find the ma_service object
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicep->is_group_equal(gname)) {		// found it!
			break;
		}
	}

	return (servicep);
}

//
// Effects: Dump the state of mcnet_node array.  Create the array at the new
//   secondary and each individual mcnet_node_impl_t object;
//   each object corresponds to a node in the cluster.
//   Use the "sec_chkpt" as the means of communicating with
//   the new secondary.  Returns true if the dump
//   succeeded, else returns false if the secondary fails before
//   the dumping can complete.
// Note: This routine is called by replica provider object only when
//   adding a new secondary.  Also note that checkpoints fail ONLY during
//   add_secondary() calls of PDTS replica provider.
//
// Parameters:
//   -- sec_chkpt (IN):  checkpoint object for new secondary replica.
//
bool
pdts_impl_t::dump_mcnet_node_array(replica::checkpoint_ptr sec_chkpt)
{
	nodeid_t	node;
	Environment	e;
	bool		result = true;

	NET_DBG(("    pdts_impl_t::dump_mcnet_node_array()\n"));

	network::ckpt_var ckptp = network::ckpt::_narrow(sec_chkpt);

	//
	// Checkpoint creation of the mcnet_node_array array.
	//
	ckptp->ckpt_create_mcnet_node_array(e);
	// Secondary failed.  Just log a warning and bail out.
	if (e.exception() != NULL) {
		cmn_err(CE_WARN, "ckpt_create_pdts() checkpoint "
		    "to specific secondary failed\n");
		e.clear();
		return (false);
	}

	//
	// Create the individual mcnet_node and assign to array at the index
	// corresponding to a node's nodeid.
	//
	for (node = 1; node <= NODEID_MAX; node++) {
		//
		// Note: We MUST release this reference because there
		// must be only one object reference to the mcnet_node
		// from a node.  This is because a node failure
		// will cause the unreferenced method to be invoked
		// on the mcnet_node corresponding to the failed
		// node, and deregister the node from the PDT server.
		//
		if (mcnet_node_array[node] == NULL) {
			continue;
		}
		network::mcnet_node_var new_ref =
			mcnet_node_array[node]->get_objref();

		//
		// checkpoint creation of the mcnet_node and its state.
		//
		mcnet_node_impl_t *cur_obj = mcnet_node_array[node];

		ckptp->ckpt_create_mcnet_node_and_dump_state(new_ref, node,
		    cur_obj->get_cbref(), cur_obj->is_gifnode(), e);
		// Secondary failed.  Just log a warning and bail out.
		if (e.exception() != NULL) {
			cmn_err(CE_WARN,
			    "ckpt_create_mcnet_node_and_dump_state()"
			    " checkpoint to specific secondary failed\n");
			e.clear();
			result = false;
			break;
		}
	} // end for

	return (result);
}

//
// Effects: Dump the state of master service group list.  Create each
//   individual master service group object object and dump its state to
//   this new secondary.   Use the "sec_chkpt" as the means of
//   communicating with the new secondary.  Returns true if the dumping
//   succeeds completely, else returns false if secondary fails
//   before the dumping can complete.
// Note: This routine is called by replica provider object only when
//   adding a new secondary.
//
// Parameters:
//   -- sec_chkpt (IN):  checkpoint object for new secondary replica.
//
bool
pdts_impl_t::dump_maservicegrplist(replica::checkpoint_ptr sec_chkpt)
{
	ma_servicegrp			*servicegrpp;
	Environment			e;
	bool				result = true;

	NET_DBG(("    pdts_impl_t::dump_maservicegrplist()\n"));

	if (ma_servicegrplist.count() == 0) {
		return (result);	// succeeded since nothing to do.
	}

	//
	// This checkpoint object sends checkpoint messages to a SPECIFIC
	// secondary, not all of them.
	//
	network::ckpt_var ckptp = network::ckpt::_narrow(sec_chkpt);

	//
	// Iterate over the master service group list.
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicegrpp = iter.get_current()) != NULL;
	    iter.advance()) {
		NET_DBG(("     call ckpt_create_ma_servicegrp()\n"));

		network::lbobj_i_var parent_lb = servicegrpp->get_lbobj();

		// Checkpoint master service group object creation
		ckptp->ckpt_create_ma_servicegrp(servicegrpp->getgroup(),
			(unsigned int)servicegrpp->getnhash(),
			servicegrpp->getpolicy(),
			parent_lb,
			e);
		// Secondary failed.  Just log a warning and bail out.
		if (e.exception() != NULL) {
			cmn_err(CE_WARN, "ckpt_create_ma_servicegrp() "
			    "checkpoint to specific secondary failed\n");
			e.clear();
			result = false;
			break;
		}

		NET_DBG(("      dump_maservicegrplist: "
			"call servicegrpp->ma_dump_state()\n"));
		//
		// Checkpoint state of the master service object to this
		// secondary.
		//
		result = servicegrpp->ma_dump_state(sec_chkpt);
		if (!result) {
		    break;
		}
	}

	return (result);
}

// **************************************************************************
// The routines from here on are only called by the checkpoint routines
// the repl_server provider object pdts_prov_impl.
// **************************************************************************
//

//
// Effects:  This routine creates a new master service group object, appends
//   it to the master service group list and increments the global counter.
//
// Parameters:
//   - gname (IN):	   group name of service
//   - nhash (IN):	   number of hash buckets in the PDT table.
//   - policy_type (IN):   the type of policy.
//   - primary_lb (IN):    Object reference to the load-balancer associated
//			with group.
//
void
pdts_impl_t::dump_ma_servicegrp(const network::group_t &gname,
    uint32_t nhash, network::lb_specifier_t pdts_policy_type,
    network::lbobj_i_ptr primary_lb)
{
	ma_servicegrp	*servicep;
	bool		okay = true;

	NET_DBG(("pdts_impl_t::dump_ma_servicegrp()\n"));
	NET_DBG(("  group = %s\n", gname.resource));

	ma_servicegrplist_lock.wrlock();

	servicep = new ma_servicegrp(gname, pdts_policy_type,
	    primary_lb, okay, nhash);

	if (okay) {
		//
		// Wildcard sticky services must be appended to the list
		// so they are matched last.  This way, services that are tied
		// to a specific port are matched first, and correctly so,
		// before it is assumed that we are matching against a
		// wildcard sticky service (one that expects connections on
		// any available port, ie: passive ftp).
		//
		if (is_sticky_wildcard(servicep->getpolicy()))
			ma_servicegrplist.append(servicep);
		else
			ma_servicegrplist.prepend(servicep);
	} else {
		delete servicep;
		CL_PANIC(!"dump_ma_servicegrp: error creating"
		    "ma_servicegrp\n");
	}

	ma_servicegrplist_lock.unlock();
}

//
// Effects:  Dump the pertinent state of the master service object.
//
// Parameters:
//   - gname (IN):	Service group name
//   - inslist (IN):    The list of instances actually registered.
//   - ninst (IN):	The length of "inslist."
//   - gif_node (IN):   The global network interface node.
//   - load_dist (IN):  The current distribution of weights for the
//			load-balancer for this service object.
//   - configlist (IN):    The list of nodes where instances may run
//   - nconfig_inst (IN):  The length of "configlist."
//
void
pdts_impl_t::dump_ma_service_state(const network::group_t &gname,
    const network::seqlong_t &inslist, int32_t ninst,
    const network::dist_info_t &load_dist,
    const network::seqlong_t &configlist, int32_t nconfig_inst)
{
	ma_servicegrp   *servicegrpp = NULL;

	NET_DBG(("    pdts_impl_t::create_ma_service_state()\n"));
	NET_DBG(("      group = %s\n", gname.resource));

	//
	// Given the "group," find the master service group
	// object in the list.
	//
	ma_servicegrplist_lock.wrlock();
	for (ma_servicegrplist.atfirst();
	    (servicegrpp = ma_servicegrplist.get_current()) != NULL;
	    ma_servicegrplist.advance()) {
		if (servicegrpp->is_group_equal(gname)) {
			//
			// Set the number of instances.
			//
			servicegrpp->ma_set_ninst(ninst);

			//
			// Append elements of sequence to instancelist.
			//
			servicegrpp->ma_set_instancelist(inslist);

			//
			// Set the load distribution for load-balancing.
			//
			servicegrpp->ma_set_loaddist(load_dist);

			//
			// Append elements of sequence to configlist.
			//
			servicegrpp->ma_set_configlist(configlist);

			//
			// Set the number of instances in config list.
			//
			servicegrpp->ma_set_nconfig_inst(nconfig_inst);

			break;
		}
	}
	ma_servicegrplist_lock.unlock();
}

//
// Effects:  Dump the pertinent state of the master service object.
//
// Parameters:
//   - gname (IN):	Service group name
//   - ssap (IN):	Service access point--protocol, ip address, port
//   - gif_node (IN):   The global network interface node.
//   - inslist (IN):	The list of nodes where instances have started up.
//
// Called only on secondary.  Call sequence:
//	pdts_prov_impl_t::add_secondary()
//	    pdts_impl_t::dump_maservicegrplist()
//		ma_servicegrp::ma_dump_state()
//		    ma_serviceobj::dump_ma_serviceobj()
//			pdts_prov_impl_t::ckpt_dump_ma_serviceobj_state()
//
void
pdts_impl_t::dump_ma_serviceobj_state(const network::group_t &gname,
    const network::service_sap_t &ssap, nodeid_t gifnode,
    const network::seqlong_t &inslist)
{
	ma_servicegrp   *servicegrpp = NULL;
	ma_serviceobj	*sobj = NULL;

	NET_DBG(("    pdts_impl_t::dump_ma_serviceobj_state\n"));
	NET_DBG(("      group = %s\n", gname.resource));
	NET_DBG(("      sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Given the "group," find the master
	// Find the master service group in the list.
	// Given the "group," find the master service group object
	// in the list.
	//
	ma_servicegrplist_lock.wrlock();
	for (ma_servicegrplist.atfirst();
	    (servicegrpp = ma_servicegrplist.get_current()) != NULL;
	    ma_servicegrplist.advance()) {
		if (servicegrpp->is_group_equal(gname)) {
			//
			// Create a new master service object with the
			// data contained in the arguments to this method.
			// Append to master service object list.
			//
			servicegrpp->ma_serviceobj_list_lock.wrlock();
			sobj = new ma_serviceobj(*servicegrpp, ssap);
			sobj->set_gifnode(gifnode);
			sobj->ma_set_instancelist(inslist);
			servicegrpp->ma_serviceobj_list.append(sobj);
			servicegrpp->ma_serviceobj_list_lock.unlock();
		}
	}
	ma_servicegrplist_lock.unlock();
}

//
// Effects: For the master service group object identified by "group" set the
//   the PDT table with the data contained in "info."  If the master
//   service group object is not found, we do nothing.
//
// Parameters:
//   - group (IN):   Service group name
//   - info (IN):   The canonical form of a PDT table
//
void
pdts_impl_t::create_pdt_state(
#if defined(DBGBUFS) && !defined(NO_DBGBUFS)
    const network::group_t &gname,
#else
    const network::group_t &,
#endif
    const network::pdtinfo_t &info)
{
	ma_servicegrp   *servicegrpp = NULL;

	NET_DBG(("    pdts_impl_t::create_pdt_state()\n"));
	NET_DBG(("      group = %s\n", gname.resource));

	//
	// Find the ma_service object given the group name.
	//
	servicegrpp = find_ma_servicegrp(info.group);
	if (servicegrpp != NULL) {
		servicegrpp->ma_initialize_pdt(info);
	}
}

//
// Requires: This routine must only be invoked by the repl_server PDT
//   provider object's method ckpt_create_ma_pdt().
//
// Effects: Create a new master PDT for master service object identified
//   by "group."  If we can't find the master service object this routine
//   has no effect.
//
// Parameters:
//   - gname (IN):   Service group name.
//
void
pdts_impl_t::create_ma_pdt(const network::group_t &gname)
{
	ma_servicegrp *servicep = NULL;

	NET_DBG(("    pdts_impl_t::create_ma_pdt()\n"));
	NET_DBG(("      group = %s\n", gname.resource));

	//
	// Find the ma_service object given the group name.
	//
	servicep = find_ma_servicegrp(gname);
	if (servicep != NULL) {
		servicep->ma_create_pdt();
	}
}


//
// Effects:  This helper routine (will be called by the checkpoint
//   function for HA) sets the primary gifnode, for the services
//   associated with the "ssap" we've been passed, to the node "node"
//   specified.
//
// Parameters:
//   - ssap (IN):   Service access point <protocol, ip adrss, port>
//   - node (IN):   The node that is the new GIF node.
//   - e (IN):  Environment variable for passing around exceptions
//
void
pdts_impl_t::set_primary_gifnode1(const network::service_sap_t& ssap,
    nodeid_t node, Environment &e)
{
	ma_servicegrp	*servicep;
	int		found = 0;

	NET_DBG(("  pdts_impl_t::set_primary_gifnode1()\n"));

	//
	// Make this gifnode the only one attached to the services
	// that match this ssap.
	//

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicep->ip_matches(ssap)) {
			servicep->attach_gifnode1(ssap, node, e);
			found++;
		}
		if (e.exception()) {
			return;
		}
	}

	if (!found) {
		e.exception(new sol::op_e(ENOENT));
	}
}

//
// Effects:  This helper routine for set_primary_gifnode() sends the PDT
//   table off to the slave GIF node, updates buckets in the PDT if necessary,
//   and collect forwarding information.
//
// Parameters:
//   - ssap (IN):   Service access point <protocol, ip adrss, port>
//   - node (IN):   The node that is the new GIF node.
//   - e (IN):  Environment variable for passing around exceptions
//
void
pdts_impl_t::set_primary_gifnode2(const network::service_sap_t& ssap,
    nodeid_t node, Environment &e)
{
	ma_servicegrp *servicep;
	int found = 0;

	NET_DBG(("  pdts_impl_t::set_primary_gifnode2()\n"));

	//
	// Tell the mcobj about the new GIF node.
	//

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//

	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);
	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicep->ip_matches(ssap)) {
			//
			// Detach the gifnode from all other nodes
			//
			for (unsigned int i = 1; i <= NODEID_MAX; i++) {
				if (node == i) {
					continue;
				}
				servicep->detach_gifnode(ssap, i, e);
				e.clear();
			}
			//
			// Now, attach it to the requested primary.
			//
			servicep->attach_gifnode2(ssap, node, e);
			found++;
		}
		//
		// It is ok to return if there is an exception because
		// set_primary_gifnode failed for a particular servicegrp.
		// So, it is RGM's responsibility to do error recovery
		// because from scalable services point of view, gifnode
		// is dead.
		//
		if (e.exception()) {
			return;
		}
	}

	if (!found) {
		e.exception(new sol::op_e(ENOENT));
	}
}


//
// Set the primary gifnode, for the services associated with the sap
// we've been passed, to the node specified.
//
// This routine has the side effect of detaching the specified node,
// from any service (using the specified sap), that it's currently
// attached to.
//
// The intent is that the caller is expecting to migrate the GIF
// for this service to another node.
//
// Other strategies should consider using {attach,detach}_gifnode instead.
//
void
pdts_impl_t::set_primary_gifnode(const network::service_sap_t& ssap,
    nodeid_t node, Environment &e)
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_impl_t::set_primary_gifnode(%d)\n", node));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Acquire read lock on the PDT server state.
	//
	pdts_lock.rdlock();

	//
	// Acquire write lock on the master service list
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	set_primary_gifnode_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (set_primary_gifnode_state*)pctxp->get_saved_state();

	if (saved_state == NULL) {
		set_primary_gifnode1(ssap, node, e);

		if (e.exception()) {
			//
			// Release the locks.
			//
			ma_servicegrplist_lock.unlock();
			pdts_lock.unlock();
			return;
		}

		FAULTPT_NET(FAULTNUM_NET_SET_PGIFN_B, FaultFunctions::generic);

		NET_DBG(("  call ckpt_set_primary_gifnode(%d)\n", node));

		network::ckpt_ptr ckptp = get_checkpoint();
		ckptp->ckpt_set_primary_gifnode(ssap, node, e);
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_SET_PGIFN_A, FaultFunctions::generic);
	}

	Environment env;
	set_primary_gifnode2(ssap, node, env);

	//
	// User exception may be passed back from set_primary_gifnode2().
	// We need to pass it back in e.
	//
	CORBA::Exception	*ex;
	if ((ex = env.release_exception()) != NULL) {
		e.exception(ex);
	}
	env.clear();

	//
	// Release the locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_SET_PGIFN_E, FaultFunctions::generic);
}

//
// Requires: Locking must be done by the caller of this routine.
//
// Effects: This routine is a helper routine for the principal
//   attach_gifnode() method.  It finds the appropriate master service
//   object, creates a master PDT table if required, and records who
//   the gifnode is in the master service object.
//
void
pdts_impl_t::attach_gifnode1(const network::service_sap_t &ssap,
    nodeid_t gifnode, Environment &e)
{
	ma_servicegrp *servicep;
	int found = 0;

	NET_DBG(("  pdts_impl_t::attach_gifnode1()\n"));

	//
	// Find service associated with the ssap passed.
	//

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicep->ip_matches(ssap)) {
			servicep->attach_gifnode1(ssap, gifnode, e);
			if (e.exception()) {
				return;
			}
			found++;
		}
	}

	if (!found) {
		e.exception(new sol::op_e(ENOENT));
	}
}

//
// Requires: Locking must be done by the caller of this routine.
//
// Effects: This routine is a helper routine for the principal
//   attach_gifnode() method.  It finds the appropriate master service
//   object, gets the master PDT table, and sends it off to the slave
//   GIF node.
//
void
pdts_impl_t::attach_gifnode2(const network::service_sap_t& ssap,
    nodeid_t gifnode, Environment &e)
{
	ma_servicegrp *servicep;
	int found = 0;

	NET_DBG(("  pdts_impl_t::attach_gifnode2()\n"));

	//
	// Find service associated with the ssap passed.
	//

	//
	// Can't ASSERT here because the secondary doesn't hold
	// any locks when this is called.
	//
	// ASSERT(ma_servicegrplist_lock.lock_held());
	//
	ma_servicegrplist_t::ListIterator iter(ma_servicegrplist);

	for (; (servicep = iter.get_current()) != NULL;
	    iter.advance()) {
		if (servicep->ip_matches(ssap)) {
			servicep->attach_gifnode2(ssap, gifnode, e);
			if (e.exception()) {
				return;
			}
			found++;
		}
	}

	if (!found) {
		e.exception(new sol::op_e(ENOENT));
	}
}


//
// Effects: Attach the specified gifnode to services that are associated with
// this sap.
//
void
pdts_impl_t::attach_gifnode(const network::service_sap_t& ssap,
    nodeid_t gifnode, Environment &e)
{
	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_impl_t::attach_gifnode(%d)\n", gifnode));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// Acquire read lock on PDT server state
	//
	pdts_lock.rdlock();

	//
	// Prevent service list from being modified
	// by more than one thread.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Get the primary's transaction context from the environment
	//
	primary_ctx *pctxp = primary_ctx::extract_from(e);
	attach_gifnode_state *saved_state;

	//
	// Extract the saved state from the context
	//
	saved_state = (attach_gifnode_state*)pctxp->get_saved_state();

	if (saved_state == NULL) {
		//
		// This helper function, which will be called on the
		// secondaries to cause state changes, creates a master PDT
		// table if required and records who the GIF node is.
		//
		attach_gifnode1(ssap, gifnode, e);

		FAULTPT_NET(FAULTNUM_NET_ATACH_GIFN_B, FaultFunctions::generic);

		NET_DBG(("   call ckpt_attach_gifnode(%d)\n", gifnode));

		network::ckpt_ptr ckptp = get_checkpoint();
		ckptp->ckpt_attach_gifnode(ssap, gifnode, e);
		ASSERT(!e.exception());
		e.clear();

		FAULTPT_NET(FAULTNUM_NET_ATACH_GIFN_A, FaultFunctions::generic);
	}

	//
	// This routine finds the appropriate master service object, gets the
	// master PDT table, and sends it off to the slave GIF node.  For an
	// HA framework retry, it is okay if this routine is executed again.
	//
	Environment env;
	attach_gifnode2(ssap, gifnode, env);

	//
	// User exception may be passed back from attach_gifnode2().
	// We need to pass it back in e.
	//
	CORBA::Exception	*ex;
	if ((ex = env.release_exception()) != NULL) {
		e.exception(ex);
	}
	env.clear();

	//
	// Release the locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();

	FAULTPT_NET(FAULTNUM_NET_ATACH_GIFN_E, FaultFunctions::generic);
}


//
// Effects: Attach the specified gifnode to services that are associated with
// this sap.
//
void
pdts_impl_t::detach_gifnode(const network::service_sap_t& ssap,
    nodeid_t gifnode, Environment &e)
{
	ma_servicegrp	*servicep;
	int		found = 0;

	upgrade_inaddr(ssap.laddr);

	NET_DBG(("pdts_impl_t::detach_gifnode(%d)\n", gifnode));
	NET_DBG(("  sap = [%s, %s, %d]\n",
	    network_lib.protocol_to_string(ssap.protocol),
	    network_lib.ipaddr_to_string(ssap.laddr),
	    ssap.lport));

	//
	// HA strategy:
	//   This method is naturally idempotent, so nothing special need
	// be done.
	//

	//
	// Acquire read lock on PDT server state
	//
	pdts_lock.rdlock();

	//
	// Prevent service list from being modified
	// by more than one thread.
	//
	ma_servicegrplist_lock.wrlock();

	//
	// Find service associated with the ssap passed.
	//
	for (ma_servicegrplist.atfirst();
	    (servicep = ma_servicegrplist.get_current()) != NULL;
	    ma_servicegrplist.advance()) {
		if (servicep->ip_matches(ssap)) {
			servicep->detach_gifnode(ssap, gifnode, e);
			if (e.exception()) {
				goto error_exit;
			}
			found++;
		}
	}

	if (!found)
		e.exception(new sol::op_e(ENOENT));

error_exit:
	//
	// Release the locks.
	//
	ma_servicegrplist_lock.unlock();
	pdts_lock.unlock();
}


//
// Effects: Sets the index "nid" in the mcnet_node_array to NULL.
//   Called in mcnet_node_impl_t::_unreferenced().
//
void
pdts_impl_t::set_mcnet_node_to_null(nodeid_t nid)
{
	ASSERT(mcnet_node_array != NULL);

	mcnet_node_array[nid] = NULL;
}

//
// Effects: Sets the write lock on the pdts_lock.
//
void
pdts_impl_t::pdts_lock_wrlock()
{
	pdts_lock.wrlock();
}

//
// Effects: Sets the read lock on the pdts_lock.
//
void
pdts_impl_t::pdts_lock_rdlock()
{
	pdts_lock.rdlock();
}

//
// Effects: Release the lock on the pdts_lock.
//
void
pdts_impl_t::pdts_lock_unlock()
{
	pdts_lock.unlock();
}

//
// Checkpoint accessor function.
//
network::ckpt_ptr
pdts_impl_t::get_checkpoint()
{
	return ((pdts_prov_impl_t*)(get_provider()))->
	    get_checkpoint_network_ckpt();
}

//
// Grab read lock on the ma_servicegrplist
//
void
pdts_impl_t::ma_servicegrplist_lock_rdlock()
{
	ma_servicegrplist_lock.rdlock();
}

//
// Grab write lock on the ma_servicegrplist
//
void
pdts_impl_t::ma_servicegrplist_lock_wrlock()
{
	ma_servicegrplist_lock.wrlock();
}

//
// Unlock the ma_servicegrplist
//
void
pdts_impl_t::ma_servicegrplist_lock_unlock()
{
	ma_servicegrplist_lock.unlock();
}
