/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MCNET_NODE_IMPL_IN_H
#define	_MCNET_NODE_IMPL_IN_H

#pragma ident	"@(#)mcnet_node_impl_in.h	1.22	08/05/20 SMI"

//
// mcnet_node_impl_t inlined methods
//

inline void
mcnet_node_impl_t::set_cbref(network::mcobj_ptr &objref)
{
	cbref_v = network::mcobj::_duplicate(objref);
}

inline network::mcobj_ptr
mcnet_node_impl_t::get_cbref()
{
	return (cbref_v);
}

inline bool
mcnet_node_impl_t::is_gifnode()
{
	return (gif_node);
}

inline void
mcnet_node_impl_t::set_gifnode(bool gif)
{
	gif_node = gif;
}

inline network::ckpt*
mcnet_node_impl_t::get_checkpoint_network_ckpt()
{
	return ((pdts_prov_impl_t*)(get_provider()))->
	    get_checkpoint_network_ckpt();
}
#endif	/* _MCNET_NODE_IMPL_IN_H */
