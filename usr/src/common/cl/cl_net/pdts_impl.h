/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PDTS_IMPL_H
#define	_PDTS_IMPL_H

#pragma ident	"@(#)pdts_impl.h	1.125	08/05/20 SMI"

//
// This file defines the implementation class routines that
// corresponds to the Service Access Point Server interface
// (interface network::SAPServer) described in network.idl.
//

//
// Important: ma_servicegrplist_lock is the lock to serialize
// access to ma_services list and pdts_lock is the lock to
// serialize PDT server operations. Always obtain the
// pdts_lock before obtaining the ma_servicegrplist_lock.
//

#include <sys/ksynch.h>
#include <sys/os.h>

#include <h/network.h>
#include <orb/object/adapter.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/ma_service.h>
#include <cl_net/pdts_prov_impl.h>

class mcnet_node_impl_t;
class ma_servicegrp;
class ma_serviceobj;

template class IntrList<ma_servicegrp, _DList>;
typedef IntrList<ma_servicegrp, _DList> ma_servicegrplist_t;
typedef DList<ma_serviceobj> ma_serviceobjlist_t;

mcnet_node_impl_t **mcnet_node_array;

//
// External reference to global pointer to the pdts prov
//
extern class pdts_prov_impl_t *mc_pdts_prov;

class pdts_impl_t : public mc_replica_of < network::PDTServer > {

public:

	//
	// Constructor for primary
	//
	pdts_impl_t(pdts_prov_impl_t *server);

	//
	// Constructor for secondary
	//
	pdts_impl_t(network::PDTServer_ptr ref, pdts_prov_impl_t *server);

	static network::mcobj_ptr	mcnet_node_cbref(nodeid_t node);

	//
	// Destructor
	//
	~pdts_impl_t();

	void _unreferenced(unref_t);

	//
	// Return pointer to HA provider
	//
	static pdts_prov_impl_t *get_ha_provider() {
		ASSERT(mc_pdts_prov);
		return (mc_pdts_prov);
	}

	int create_mcnet_node_array(pdts_prov_impl_t *server);
	void free_mcnet_node_array();

	//
	// Effects:  Attach the mcobj object "cbref" to the PDT server,
	//   collect information about registered scalable services in
	//   "srvinfoseq" and who the global network interfaces nodes are
	//   in "gifinfo" and the reference to the servobj corresponding
	//   to node "node."
	//
	void attach_mcobj(nodeid_t node, network::mcobj_ptr cbref,
	    CORBA::Object_out serv_ref, Environment &e);

	//
	// This method is for debugging purposes
	//
	void gns_service(network::grpinfoseq_t_out grpinfoseqp,
	    const network::group_t &g_name, Environment &_environment);
	void gns_mcobjs(network::mcobjs_t_out mcobjs, Environment &e);
	void gns_pdt(network::pdtinfo_t_out pdtinfo,
	    const network::group_t& gname, Environment& e);


	// Effects:  This method registers global networking interfaces.
	int32_t register_gifnode(nodeid_t node, Environment &_environment);
	void register_gifnode1(nodeid_t node);

	// Effects: This method deregisters node as a gif supplier.
	void deregister_gifnode(nodeid_t node, Environment &_environment);
	void deregister_gifnode1(nodeid_t node);

	// Effects: This method deregisters the proxy node.
	void deregister_proxynode(nodeid_t node, Environment &_environment);
	void deregister_proxynode1(nodeid_t node);

	// Effects: This method enables scalable services
	void get_all_registered_services(network::srvinfoseq_t_out srvinfoseqp,
	    Environment &_environment);

	// Effects: Create a new scalable service group.
	network::ret_value_t
	create_scal_srv_grp(const network::group_t& group_name,
	    network::lb_specifier_t policy_type, Environment &_environment);

	network::ret_value_t
	create_scal_srv_grp1(const network::group_t& g_name,
	    network::lb_policy_t policykind, network::lbobj_i_ptr primary_lb);

	void
	create_scal_srv_grp2(const network::group_t &g_name, uint32_t nhash,
	    network::lb_specifier_t policy_type, Environment &_environment);


	network::ret_value_t
	config_sticky_srv_grp(const network::group_t& group_name,
	    const network::sticky_config_t &st,
	    Environment &_environment);

	bool config_sticky_srv_grp1(const network::group_t& g_name,
	    const network::sticky_config_t &st,
	    network::ret_value_t *retval);

	void config_sticky_srv_grp2(const network::group_t& g_name,
	    const network::sticky_config_t &st);

	network::ret_value_t
	config_rr_srv_grp(const network::group_t& group_name,
	    const network::rr_config_t &rr,
	    Environment &_environment);

	bool config_rr_srv_grp1(const network::group_t& g_name,
	    const network::rr_config_t &rr,
	    network::ret_value_t *retval);

	void config_rr_srv_grp2(const network::group_t& g_name,
	    const network::rr_config_t &st);

	// Effects: Delete a scalable service group.
	network::ret_value_t
	delete_scal_srv_grp(const network::group_t& group_name,
	    Environment &_environment);

	network::ret_value_t
	delete_scal_srv_grp1(const network::group_t& g_name);

	void
	delete_scal_srv_grp2(const network::group_t& g_name,
	    Environment &e);

	// Effects: Add a new scalable service to the specified group.
	network::ret_value_t
	add_scal_service(const network::group_t& group_name,
	    const network::service_sap_t& sap, Environment& e);

	// Effects: Remove a scalable service from the specified group.
	network::ret_value_t
	rem_scal_service(const network::group_t& group_name,
	    const network::service_sap_t& sap, Environment& e);

	// Effects:  This method adds an instance of a service.
	void register_instance(const network::service_sap_t& ssap,
	    nodeid_t node, Environment &_environment);

	// Effects: Helper method for register_instance().
	bool register_instance1(const network::service_sap_t& ssap,
	    nodeid_t node);

	//
	// Effects: Helper method for register_instance().  Distributes
	// changes to the slaves.
	//
	void register_instance2(const network::service_sap_t &ssap,
	    Environment &_environment);

	// Effects:  This method removes an instance of a service.
	void deregister_instance(const network::service_sap_t& ssap,
	    nodeid_t node, Environment &_environment);

	bool deregister_instance1(const network::service_sap_t& ssap,
	    nodeid_t node);

	void deregister_instance2(nodeid_t node,
	    const network::service_sap_t &ssap, Environment &e);

	//
	// Effects: Add a new service instance on node "node" for an
	// existing scalable service.
	//
	network::ret_value_t
	add_nodeid(const network::group_t& group_name,
	    nodeid_t node, Environment &_environment);

	//
	// Effects: Remove an existing service instance on node "node"
	// for an extant scalable service.
	//
	network::ret_value_t
	remove_nodeid(const network::group_t& group_name,
	    nodeid_t node, Environment &_environment);

	//
	// Effects: Query whether PDT server knows about a particular
	// scalable service identified by "sap."
	// Returns true if service is registered, else returns false.
	//
	bool is_scal_service(const network::service_sap_t& sap,
	    Environment& e);

	// Effects: Check if the specified service is a scalable service grp.
	bool is_scal_service_group(const network::group_t& group_name,
	    Environment &_environment);

	void set_primary_gifnode(const network::service_sap_t &ssap,
	    nodeid_t node, Environment &_environment);
	void set_primary_gifnode1(const network::service_sap_t& ssap,
	    nodeid_t nodeid, Environment &e);
	void set_primary_gifnode2(const network::service_sap_t& ssap,
	    nodeid_t nodeid, Environment &e);

	void attach_gifnode(const network::service_sap_t& ssap,
	    nodeid_t nodeid, Environment &e);
	void attach_gifnode1(const network::service_sap_t& ssap,
	    nodeid_t nodeid, Environment &e);
	void attach_gifnode2(const network::service_sap_t& ssap,
	    nodeid_t nodeid, Environment &e);

	void detach_gifnode(const network::service_sap_t& ssap,
	    nodeid_t nodeid, Environment &e);


	//
	// INTERNAL METHODS
	//

	//
	// Effects: Helper method to register a new service "ssap"
	//   in group "gname."  And to tell all slaves about it.
	//
	void
	register_service1(const network::group_t &g_name,
	    const network::service_sap_t &ssap);
	void register_service2(ma_servicegrp *servicep,
	    const network::service_sap_t &ssap);

	//
	// Effects: Helper method to deregister an existing service "ssap"
	//   in group "gname."  And to tell all slaves about it.
	//
	network::ret_value_t
	deregister_service1(const network::group_t &gname,
	    const network::service_sap_t &ssap, bool& updated_pdt);
	void deregister_service2(const network::service_sap_t &ssap,
	    ma_servicegrp *servicegrpp, bool updated_pdt, Environment &e);

	network::lbobj_i_ptr
	get_primary_lb(const network::group_t & g_name, Environment &e);

	bool add_nodeid1(const network::group_t &g_name,
	    nodeid_t node, network::ret_value_t *retval);

	network::ret_value_t remove_nodeid1(const network::group_t &g_name,
	    nodeid_t node, bool &pdt_updated);

	void remove_nodeid2(const network::group_t &gname);

	// Effects: Get the load balancing object, given the service sap.
	network::lbobj_i_ptr get_lbobj(const network::group_t &g_name,
	    Environment &e);

	//
	// Effects: Called at the secondary in ckpt_dump_ma_pdtinfo() method
	// to dump the state of a PDT table.
	//
	void create_pdt_state(const network::group_t &group,
	    const network::pdtinfo_t &info);

	//
	// Effects: Called at the secondary in ckpt_create_ma_pdt() method
	// to create a new master PDT.
	//
	void create_ma_pdt(const network::group_t &group);

	// Helper methods.
	uint_t count_scal_services();

	//
	// Effects: Returns a pointer to the group object given the
	// name "group" if the object is in the list, else returns NULL.
	//
	ma_servicegrp*
	find_ma_servicegrp(const network::group_t &group);

	// Effects: Sets index "nid" (node id) to NULL in mcnet_node_array.
	void set_mcnet_node_to_null(nodeid_t nid);

	// Exports the ability to set a write lock or read lock, and unlock
	// the pdts state.
	void pdts_lock_wrlock();
	void pdts_lock_rdlock();
	void pdts_lock_unlock();

	// Exports the ability to set a write lock or read lock, and unlock
	// the ma_servicegrp_list, so that lbobj's can serialize with the
	// rest of the PDT methods.
	void ma_servicegrplist_lock_rdlock();
	void ma_servicegrplist_lock_wrlock();
	void ma_servicegrplist_lock_unlock();

	//
	// The following methods are only called when the primary PDT
	// server is frozen and is dumping its state to a secondary.
	//
	bool dump_mcnet_node_array(replica::checkpoint_ptr sec_chkpt);

	bool dump_maservicegrplist(replica::checkpoint_ptr sec_chkpt);

	void dump_ma_servicegrp(const network::group_t &g_name,
	    uint32_t nhash, network::lb_specifier_t policy_type,
	    network::lbobj_i_ptr primary_lb);

	void dump_ma_service_state(const network::group_t &group,
	    const network::seqlong_t &inslist, int32_t ninst,
	    const network::dist_info_t &load_dist,
	    const network::seqlong_t &configlist, int32_t nconfig_inst);

	void dump_ma_serviceobj_state(const network::group_t &group,
	    const network::service_sap_t &ssap,
	    nodeid_t gif_node, const network::seqlong_t &inslist);

	// Checkpoint accessor functions.
	network::ckpt_ptr	get_checkpoint();

private:
	// Master service group list head.
	ma_servicegrplist_t	ma_servicegrplist;

	os::rwlock_t	ma_servicegrplist_lock;	// lock to serialize access to
						// ma_services list

	os::rwlock_t	pdts_lock; 	// to serialize PDT server operations
	char		name[32];	// name, e.g. "PDTServer"

	char *protocol_to_string(char proto);

	friend class mcnet_node_impl_t;

	//
	// Back pointer to the HA provider object of which this object
	// is a replica.
	//
	pdts_prov_impl_t *my_provider;
};

//
// Transaction state definitions.
//
class register_gifnode_state : public transaction_state {
public:
	register_gifnode_state() {}

	~register_gifnode_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class register_pxqobj_state : public transaction_state {
public:
	register_pxqobj_state() {}

	~register_pxqobj_state() {}

	void orphaned(Environment &) {}
	void committed() {}

};

class attach_mcobj_state : public transaction_state {
public:
	attach_mcobj_state() {}

	~attach_mcobj_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class deregister_inst_state : public transaction_state {
public:
	deregister_inst_state() {}

	~deregister_inst_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class register_inst_state : public transaction_state {
public:
	register_inst_state() {}

	~register_inst_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class set_primary_gifnode_state : public transaction_state {
public:
	set_primary_gifnode_state() {}

	~set_primary_gifnode_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class attach_gifnode_state : public transaction_state {
public:
	attach_gifnode_state() {}

	~attach_gifnode_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class add_scalable_service_state : public transaction_state {
public:
	add_scalable_service_state() {}

	~add_scalable_service_state() {}

	void orphaned(Environment &) {}
	void committed() {}
};

class add_nodeid_state : public transaction_state {
public:
	add_nodeid_state(bool found, network::ret_value_t retval) :
	    transaction_state(), found_state(found), retval_state(retval) {}

	~add_nodeid_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	bool found_state;
	network::ret_value_t retval_state;
};

class remove_nodeid_state : public transaction_state {
public:
	remove_nodeid_state(bool pdt_updated, network::ret_value_t retval) :
	    transaction_state(), pdt_updated_state(pdt_updated),
	    retval_state(retval) {}

	~remove_nodeid_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	bool pdt_updated_state;
	network::ret_value_t retval_state;
};

class rem_scal_service_state : public transaction_state {
public:
	rem_scal_service_state(network::ret_value_t retval,
		const network::service_sap_t &sap,
		bool updated_pdt): transaction_state(),
		retval_state(retval), sap_state(sap),
		updated_pdt_state(updated_pdt) {}

	~rem_scal_service_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	network::ret_value_t retval_state;
	network::service_sap_t sap_state;
	bool			updated_pdt_state;
};

class create_scal_srv_grp_state : public transaction_state {
public:
	create_scal_srv_grp_state(network::ret_value_t retval) :
		transaction_state(), retval_state(retval) {}

	~create_scal_srv_grp_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	network::ret_value_t retval_state;
};

class config_sticky_srv_grp_state : public transaction_state {
public:
	config_sticky_srv_grp_state(bool changed,
	    network::ret_value_t retval) :
	    transaction_state(),
	    changed_state(changed), retval_state(retval) {}

	~config_sticky_srv_grp_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	bool changed_state;
	network::ret_value_t retval_state;
};

class config_rr_srv_grp_state : public transaction_state {
public:
	config_rr_srv_grp_state(bool changed,
	    network::ret_value_t retval) :
	    transaction_state(),
	    changed_state(changed), retval_state(retval) {}

	~config_rr_srv_grp_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	bool changed_state;
	network::ret_value_t retval_state;
};


class delete_scal_srv_grp_state : public transaction_state {
public:
	delete_scal_srv_grp_state(network::ret_value_t retval) :
		transaction_state(), retval_state(retval) {}

	~delete_scal_srv_grp_state() {}

	void orphaned(Environment &) {}
	void committed() {}

	network::ret_value_t retval_state;
};

#endif	/* _PDTS_IMPL_H */
