//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.

#pragma ident	"@(#)sl_pdt.cc	1.74	08/09/07 SMI"

//
// Slave side pdt module
//

#include <h/network.h>
#include <sys/os.h>

#include <cl_net/netlib.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/netlib.h>
#include <cl_net/sl_service.h>
#include <cl_net/bind_table.h>
#include <cl_net/mcobj_impl.h>
#include <nslib/ns.h>
#include <netinet/in.h>
#ifdef SCTP_SUPPORT
#include <netinet/sctp.h>
#endif
#ifdef _KERNEL
#include <sys/tuneable.h>
#endif

#include <sys/mc_probe.h>

#define	MAX_CONN_CNT
#ifdef MAX_CONN_CNT
//
// Maximum number of connection that can be handled in
// the dispatch timeout time.
//
uint32_t	max_conn_cnt = 1000;
//
// Dispatch time. Time interval to push the pernodelst to
// the respective proxy node. Time specified in seconds.
//
clock_t		dispatch_time = 40;
#endif

extern struct clnet_dbgstat clndb;
extern  mcobj_impl_t *mcobjp;
int store_history = 0;

extern "C" {
void cnct_dispatch_func(void *arg);
}

threadpool dispatch_conn_threadpool(true, 5, "dispatch_conn", 20);

unsigned long
sl_pdt::dump_slpdt(sl_pdt *slpdt)
{
	(void) dump_pdtinfo((pdt *)slpdt);

	return (0);
}

sl_pdt::sl_pdt(uint_t nhashent) : pdt(nhashent)
{
	sl_pdte_frwd = new pdte_frwd_t*[nhashent];
	for (int i = 0; i < (int)nhashent; i++)
		sl_pdte_frwd[i] = NULL;

	pdt_bindtab = NULL;
	pdt_strong_sticky = false;
	pdt_udp_sticky = false;
	pdt_generic_sticky = false;
	pdt_rr_count = 0;
	INIT_RR_CONFIG(pdt_rr_config);

}

sl_pdt::~sl_pdt()
{
	for (int i = 0; i < (int)pdt_getnhash(); i++) {
		if (sl_pdte_frwd[i]) {
			delete sl_pdte_frwd[i];
			sl_pdte_frwd[i] = NULL;
		}
	}
	delete [] sl_pdte_frwd;
	sl_pdte_frwd = NULL;

	if (pdt_bindtab) {
		delete pdt_bindtab;
		pdt_bindtab = NULL;			//lint !e423
	}

}

void
sl_pdt::pdt_config_sticky(const network::sticky_config_t &st)
{
	pdt_strong_sticky = (st.st_flags & network::ST_WEAK? false: true);
	pdt_udp_sticky = (st.st_flags & network::ST_UDP? true: false);
	pdt_generic_sticky = (st.st_flags & network::ST_GENERIC? true: false);

	if (pdt_strong_sticky && pdt_bindtab == NULL) {
		pdt_bindtab = new gif_bind_table_t(bt_hash_size);
	} else if (!pdt_strong_sticky && pdt_bindtab != NULL) {
		delete pdt_bindtab;
		pdt_bindtab = NULL;			//lint !e423
	}
}

void
sl_pdt::set_rr_config(network::rr_config_t& rr_config)
{
	pdt_rr_config.rr_flags = rr_config.rr_flags;
	pdt_rr_config.rr_conn_threshold = rr_config.rr_conn_threshold;
}

//
// input processing for a network packet
//
unsigned int
sl_pdt::pdt_input(network::cid_t& cid, network::lb_specifier_t policy,
    network::cid_action_t& action, uint32_t& index, uint_t flag)
{
	nodeid_t	node = 0;

	if (!(pdt_flag & PDT_INITIALIZED)) {
		action = network::CID_DROP;
		return (0);
	}

	index = pdt_hash_func(cid, policy);
	int seqnum = pdt_getbktseq(index);
	//
	// seqnum can be set to PDT_DROP_PKTS when forwarding info
	// is being dumped onto slave pdt.
	// seqnum can be set to PDT_DROP_SYN when forwarding info
	// is being collected. This prevents new connnections from
	// being added to the tcp lists while it is being scanned.
	// The active connections should continue to work fine.
	// So the ideal state transition would be
	// normal ---> PDT_DROP_SYN ---> PDT_DROP_PKTS ---> normal
	//

#ifdef SCTP_SUPPORT
	if ((seqnum == PDT_DROP_PKTS) ||
	    ((cid.ci_protocol == IPPROTO_TCP) && (flag & TH_SYN) &&
	    (seqnum == PDT_DROP_SYN)) || ((cid.ci_protocol == IPPROTO_SCTP) &&
	    (flag == CHUNK_INIT) && (seqnum == PDT_DROP_SYN))) {
#else
	if ((seqnum == PDT_DROP_PKTS) ||
	    ((flag & TH_SYN) && (seqnum == PDT_DROP_SYN))) {
#endif
		MC_PROBE_1(pdt_input, "mc net slpdt", "",
			tnf_int, bucket, (int)index);

		action = network::CID_DROP;
		return (0);
	}

	//
	// If strong sticky, we consult the binding table, both for SYNs
	// and traffic for existing connections. Forwarding lists aren't
	// needed for strong sticky groups. UDP packets follow TCP
	// connections as well if UDP sticky is set.
	//
	// Otherwise,
	//
	// If it is a syn pkt, don't bother looking up the forwarding
	// list. This will not go to the forwarding node, and instead
	// will have to go to the node pointed to by the pdt table.
	// If it is a udp packet and RR set, we look up the forwarding table
	// else, there is no forwarding list.
	//
	network::conn_state_t	dummy_state;
#ifdef SCTP_SUPPORT
	if (pdt_strong_sticky && (cid.ci_protocol == IPPROTO_TCP ||
	    (cid.ci_protocol == IPPROTO_AH) ||
	    (cid.ci_protocol == IPPROTO_ESP) ||
	    (cid.ci_protocol == IPPROTO_SCTP) ||
	    (cid.ci_protocol == IPPROTO_UDP && pdt_udp_sticky) ||
	    (cid.ci_protocol == IPPROTO_UDP &&
	    (pdt_rr_config.rr_flags & network::LB_RR)) ||
	    pdt_generic_sticky)) {
#else
	if (pdt_strong_sticky && (cid.ci_protocol == IPPROTO_TCP ||
	    (cid.ci_protocol == IPPROTO_AH) ||
	    (cid.ci_protocol == IPPROTO_ESP) ||
	    (cid.ci_protocol == IPPROTO_UDP && pdt_udp_sticky) ||
	    (cid.ci_protocol == IPPROTO_UDP &&
	    (pdt_rr_config.rr_flags & network::LB_RR)) ||
	    pdt_generic_sticky)) {
#endif
		network::bind_state_t state;

		ASSERT(pdt_bindtab != NULL);

		if ((node = pdt_bindtab->lookup(cid.ci_faddr, state)) == 0) {
			//
			// Not found in binding table. Lookup PDT.
			//
			index = pdt_hash_func(cid, policy);
			node = pdt_gethashval(index);
			action = network::CID_DISPATCH;
			return (node);
		}

		// The right thing is to move the bind table entry to
		// BI_CONNECTED after the connect call. But since the messages
		// were getting passed very frequently has this optimization.
		//
		if ((state == network::BI_UNINITIALISED) && (flag & TH_ACK) &&
		    (cid.ci_protocol == IPPROTO_TCP)) {
			(void) pdt_bindtab->setstate(cid.ci_faddr,
			    network::BI_CONNECTED);
		}
#ifdef SCTP_SUPPORT
		if (((pdt_generic_sticky) ||
		    ((cid.ci_protocol == IPPROTO_TCP) && (flag & TH_SYN)) ||
		    ((cid.ci_protocol == IPPROTO_SCTP) &&
		    (flag == CHUNK_INIT))) &&
		    (state == network::BI_DEPRECATED)) {
#else
		if (((pdt_generic_sticky) ||
		    (flag & TH_SYN)) &&
		    (state == network::BI_DEPRECATED)) {
#endif
			//
			// Entry is in the middle of a two-phase
			// remove. Drop any packet from the client.
			//
			node = 0;
			action = network::CID_DROP;
		} else {
			//
			// Non-SYN, or in CONNECTED state
			// Since a binding entry exists, the action is
			// "forwarding".
			//

			action = network::CID_FORWARD;
		}

#ifdef SCTP_SUPPORT
	} else if ((flag != CHUNK_INIT) && (flag != CHUNK_COOKIE) &&
	    (cid.ci_protocol == IPPROTO_SCTP) &&
	    (sl_pdte_frwd[index] != NULL) &&
	    (node = sl_pdte_frwd[index]->lookup_change(cid,
	    dummy_state)) != 0) {
		action = network::CID_FORWARD;
#endif
	} else {
		network::conn_state_t	state;

		if ((sl_pdte_frwd[index] != NULL) &&
		    (node = sl_pdte_frwd[index]->lookup_change(cid, state)) !=
			0) {
			if ((pdt_rr_config.rr_flags & network::LB_RR) &&
			    (state == network::CONN_SYN_RCVD) &&
			    (cid.ci_protocol == IPPROTO_TCP)) {
				if (flag & TH_SYN) {
					// Drop any SYN for a SYN_RCVD
					// connection
					action = network::CID_DROP;
				}
			} else {
				action = network::CID_FORWARD;
			}
		} else {
			cid.ci_state = network::CONN_SYN_RCVD;
			index = pdt_hash_func(cid, policy);
			node = pdt_gethashval(index);
			action = network::CID_DISPATCH;
		}
	}
	return (node);
}

// Set the state of the connection in the forwarding table.
void
sl_pdt::setstate(const network::cid_t& cid, network::lb_specifier_t policy,
    network::conn_state_t state)
{
	uint32_t		index;

	index = pdt_hash_func(cid, policy);
	if (sl_pdte_frwd[index] != NULL) {
		(void) sl_pdte_frwd[index]-> lookup_change(cid, state, 1);
	}
}

void
sl_pdt::resetall_usage_bit()
{
	int	indx;

	for (indx = 0; indx < pdt_nhash; indx++) {
		if (sl_pdte_frwd[indx] != NULL) {
			sl_pdte_frwd[indx]->resetall_usage_bit();
		}
	}
}


// Insert the connection ID to the bind table
int
sl_pdt::insert_bind_info(const network::cid_t &cid, nodeid_t node)
{
	bool rcode;
	ASSERT(pdt_bindtab != NULL);

	network::inaddr_t faddr = cid.ci_faddr;

	rcode = pdt_bindtab->create(faddr, node);
	//
	// For generic affinity we do not want the GIF node to handle
	// the timeout. It is driven by the proxy node. Hence mark the
	// state as connected.
	//
	if (rcode && pdt_generic_sticky &&
	    cid.ci_state == network::CONN_ESTABLSHD) {
		(void) pdt_bindtab->setstate(faddr, network::BI_CONNECTED);
	}

	return ((rcode?0:-1));
}

//
// Add bindings that are retrieved from proxy nodes
//
void
sl_pdt::pdt_add_bindinfo(const network::bind_t& bindp, uint_t iter)
{
	uint32_t index;
	nodeid_t node = bindp.bn_nodeid;

	ASSERT(pdt_bindtab != NULL);
	if (iter == 0) {
		//
		// First iteration in a series of add_bindinfo.
		// We start with a clean slate in this case.
		// Note that we can't delete pdt_bindtab, since our caller
		// is not holding an exclusive lock.
		//
		pdt_bindtab->clean_all();
	}

	for (uint_t i = 0; i < bindp.bn_nfaddr; i++) {
		network::inaddr_t faddr = bindp.bn_faddrseq[i];
		index = pdt_faddr_hash_func(faddr);

			NET_DBG_BIND(("pdt_add_bindinfo <%s, %d, %d>\n",
			    network_lib.ipaddr_to_string(faddr),
			    node, index));
		if (pdt_gethashval(index) != node) {
			//
			// Ignore return: don't care if a new entry is
			// created, or an existing one is updated.
			//
			(void) pdt_bindtab->create(faddr, node);

			NET_DBG_BIND(("pdt_add_bindinfo <%s, %d>\n",
			    network_lib.ipaddr_to_string(faddr),
			    node));
		}
	}
}

//
// Remove bindings for the specified server node
//
void
sl_pdt::remove_bindinfo(nodeid_t node)
{
	ASSERT(pdt_bindtab != NULL);
	pdt_bindtab->remove_node(node);
}

void
sl_pdt::pdt_setstate_binding(const network::inaddr_t& faddr,
    network::bind_state_t state)
{
	ASSERT(pdt_bindtab != NULL);
	(void) pdt_bindtab->setstate(faddr, state);
	if ((pdt_rr_config.rr_flags & network::LB_RR) &&
	    (state == network::BI_CLOSED)) {
		decr_rr_count();
	}
}

//
// Return a list of existing bindings
//
void
sl_pdt::pdt_gns_bind(network::gns_bindseq_t *bindp)
{
	ASSERT(pdt_bindtab != NULL);
	pdt_bindtab->gns_elems(bindp);
}

void
sl_pdt::drop_syns(uint32_t index)
{
	pdt_setbktseq(index, PDT_DROP_SYN);
	NET_DBG(("sl_pdt::drop_syns--dropping SYNs for index %d\n", index));
}

// Insert the CID to the forwarding list
int
sl_pdt::insert_frwd_info(const network::cid_t& cid, nodeid_t node,
    uint32_t index)
{
	pdte_frwd_t *pdte_frwdp;

	if (sl_pdte_frwd[index] == NULL) {

#ifdef _KERNEL
		// The following check prevents the forwarding table to hog all
		// the available memory, which may cause other critical
		// operations to fail.

		// Though Solaris compares on tune.t_minarmem, we check for
		// twice the value. There is nothing wrong about being paranoid
		// on memory.

		// If we are  low on memory, the connection request will not
		// be accepted and the cleanup thread is invoked to manage
		// any cleanup.

		if (availrmem < btop(sizeof (pdte_frwd_t)) +
		    (2 *(uint_t)tune.t_minarmem)) {
			NET_DBG_FWD(("Low memory(%d)! Drop the"
			    "connection\n", availrmem));
			return (-1);
		}
#endif	// _KERNEL

		sl_pdte_frwd[index] = new (os::NO_SLEEP) pdte_frwd_t;
		//
		// Too bad if the memory allocation failed here.
		// Can't do much, and forwarding info for this
		// slot is lost.
		//
		if (sl_pdte_frwd[index] == NULL) {
			NET_DBG_FWD(("  Mem allocation for pdte_frwd_t"
			    "failed %d\n", index));
			return (-1);
		}
	}
	pdte_frwdp = sl_pdte_frwd[index];

	ASSERT(pdte_frwdp != NULL);

	NET_DBG_FWD(("  Doing unmarshal selective for index %d\n",
	    index));

	pdte_frwdp->unmarshal_selective_frwd_info(cid, node, index);

	if (sl_pdte_frwd[index]->is_frwd_empty()) {
		NET_DBG_FWD(("  Deleting pdte_frwd_t index"
		    "%d in add_frwd_info\n", index));
		delete sl_pdte_frwd[index];
		sl_pdte_frwd[index] = NULL;
		return (-1);
	}

	return (0);
}

//
// Assumes that sl_serviceobj_list_lock is held in write mode.
// Also, because of priority inversion, this routine can run at
// interrupt priority. So, all memory allocations have to be done
// with KM_NO_SLEEP turned on.
//
// fwdp_valid is a bool array parallel to fwdp. If fwdp_valid[i] is true,
// then the i-th forwarding entry in fwdp should be added -- because this
// node is the GIF node for the local address in the forwarding entry.
//
// Flag with DESTROY indicates the GIF node corres fwd list should be
// detroyed if already exists before adding the new entries. NODESTROY
// means the existing list should not be detroyed.
//
// FORCE signifies entries will be explicitly added to the fwd list even if
// forwarded node matches with the hash bucket's corresponding forwarded node.
// NOFORCE is the opposite
//
void
sl_pdt::add_frwd_info(const network::fwd_t& fwdp, bool *fwdp_valid,
    network::lb_specifier_t policy, uint_t flag)
{
	pdte_frwd_t *pdte_frwdp;
	nodeid_t node = fwdp.fw_node;
	int ncids = fwdp.fw_ncid;
	uint_t nhashent = pdt_getnhash();
	uint_t index, i;

	ASSERT((node > 0) && (node <= NODEID_MAX));

	NET_DBG_FWD(("sl_pdt::add_frwd_info()\n"));

	MC_PROBE_0(add_frwd_info, "mc net mcfwd", "");

#ifdef _KERNEL	// not emulated for unode

	//
	// The following check prevents the forwarding list from hogging
	// all the memory, which may cause other, more critical parts of the
	// system to fail. Note that this is just a workaround. The real fix
	// should be made in those places where resource availability are
	// critical, e.g. by reserving resources instead of dynamically
	// allocating.
	//
	// Solaris checks against tune.t_minarmem. To be a little conservative,
	// we double that.
	//
	if (availrmem < btop((uint_t)fwdp.fw_ncid * sizeof (frwd_cid)) +
	    (uint_t)tune.t_minarmem + (uint_t)tune.t_minarmem) {
		NET_DBG_FWD(("  Low memory (%d)! Drop the forwarding list\n",
		    availrmem));
		return;
	}

#endif	// _KERNEL

	if (flag == network::FWD_ADD_DESTROY_NOFORCE ||
	    flag == network::FWD_ADD_DESTROY_FORCE) {
		//
		// Clean up all the forwarding information associated
		// with this node. We have new forwarding information
		// from this node which will overwrite all the old
		// information, that we might have.
		//

		for (i = 0; i < (int)nhashent; i++) {
			//
			// Fix for 4381994: Even if node == hash value,
			// there can still be forwarding entries from before
			// pointing to 'node' that we need to clean up.
			// For example, if the bucket changes from 1 to 2, then
			// forwarding entries are legitimately created pointing
			// to node 1. Now if the value changes back from 2 to 1,
			// such entries need to be removed.
			//
			if (sl_pdte_frwd[i] != NULL) {
				sl_pdte_frwd[i]->clean_frwd_info(node);
				if (sl_pdte_frwd[i]->is_frwd_empty()) {
					NET_DBG_FWD(("  Deleting pdte_frwd_t"
					    "index %d in add_frwd_info\n", i));
					delete sl_pdte_frwd[i];
					sl_pdte_frwd[i] = NULL;
				}
			}
		}
	} // FWD_ADD_DESTROY_NOFORCE || FWD_ADD_DESTROY_FORCE

	//
	// If there are no cids, return.
	//

	if (ncids <= 0)
		return;

	//
	// For all the cids, find the bucket it corresponds to.
	// Check if the bucket is pointing to the same node as
	// the node if we got the forwarding information from.
	// Then check if pdte_frwd_t structure has been created or
	// not. Once the pdte_frwd_t is created, add the cid to
	// the forwarding list.
	//

	uint_t fflag = 0;
	if (flag == network::FWD_ADD_DESTROY_FORCE ||
	    flag == network::FWD_ADD_NODESTROY_FORCE)
		fflag = 1;

	for (i = 0; i < ncids; i++) {

		if (!fwdp_valid[i])
			continue;

		index = (int)pdt_hash_func(fwdp.fw_cidseq[(uint_t)i], policy);

		if (!fflag && node == pdt_gethashval((uint_t)index))
			continue;

		if (!incr_rr_count()) {
			NET_DBG_BIND(("increment of the count failed\n"));
		}

		if (insert_frwd_info(fwdp.fw_cidseq[i], node, index) != 0) {
			//
			// No point continuing. Out of memory.
			//
			break;
		}
	}
}

//
// Node died. Make sure that all the forwarding info for this node
// is removed from the forwarding lists since, all the connections
// dead now.
//
void
sl_pdt::remove_frwd_info(nodeid_t node)
{
	uint_t nhashent = pdt_getnhash();

	for (int index = 0; index < (int)nhashent; index++) {
		if (sl_pdte_frwd[index] == NULL)
			continue;
		int tmp =  sl_pdte_frwd[index]->nfrwd;
		sl_pdte_frwd[index]->clean_frwd_info(node);
		decr_rr_count(tmp - sl_pdte_frwd[index]->nfrwd);
		if (sl_pdte_frwd[index]->is_frwd_empty()) {
			NET_DBG_FWD(("deleting pdte_frwd_t for ind %d", index));
			delete sl_pdte_frwd[index];
			sl_pdte_frwd[index] = NULL;
		}
	}
}

//
// Service removed. Make sure that all the forwarding info for this service
// is removed from the forwarding lists of all nodes.
//
void
sl_pdt::remove_frwd_info_by_ssap(const network::service_sap_t& ssap)
{
	uint_t nhashent = pdt_getnhash();

	for (int index = 0; index < (int)nhashent; index++) {
		if (sl_pdte_frwd[index] == NULL)
			continue;
		int tmp = sl_pdte_frwd[index]->nfrwd;
		sl_pdte_frwd[index]->clean_frwd_info_by_ssap(ssap);
		decr_rr_count(tmp - sl_pdte_frwd[index]->nfrwd);
		if (sl_pdte_frwd[index]->is_frwd_empty()) {
			NET_DBG_FWD(("deleting pdte_frwd_t for bucket %d",
			    index));
			delete sl_pdte_frwd[index];
			sl_pdte_frwd[index] = NULL;
		}
	}
}


void
sl_pdt::stop_forward(const network::cid_t& cid, network::lb_specifier_t policy,
    nodeid_t node)
{
	int index;
	network::cid_t lcid = cid;

	NET_DBG_FWD(("sl_pdt::stop_forward()\n"));

	index = (int)pdt_hash_func(lcid, policy);

	if (sl_pdte_frwd[index] == NULL)
		return;

	int tmp = sl_pdte_frwd[index]->nfrwd;
	if (sl_pdte_frwd[index]->stop_forward(lcid, node)) {
		decr_rr_count(tmp - sl_pdte_frwd[index]->nfrwd);
		if (sl_pdte_frwd[index]->is_frwd_empty()) {
			NET_DBG_FWD(("  Deleting pdte_frwd_t index %d"
			    " in stop forward\n", index));
			delete sl_pdte_frwd[index];
			sl_pdte_frwd[index] = NULL;
		}
	}
}

void
sl_pdt::pre_marshal_fwd(network::seqlong_t& cntseq, nodeid_t node)
{
	for (uint_t i = 0; i < pdt_nhash; i++) {
		if (sl_pdte_frwd[i]) {
			sl_pdte_frwd[i]->pre_marshal_fwd(cntseq, node);
		}
	}
}

void
sl_pdt::marshal_fwd(network::fwdseq_t& fwdinfo, nodeid_t node, Environment& e)
{
	for (uint_t i = 0; i < pdt_nhash; i++) {
		if (sl_pdte_frwd[i]) {
			sl_pdte_frwd[i]->marshal_fwd(fwdinfo, node, e);
		}
	}
}

bool
sl_pdt::incr_rr_count()
{
	pdt_rr_lock.lock();

	if (pdt_rr_count == pdt_rr_config.rr_conn_threshold) {
		// The number of connectiona has reached the maximum. Start the
		// cleanup thread. Cannot handle the particular connection
		clndb.ndb_rr_maxreached++;
		pdt_rr_lock.unlock();

		return (false);
	}
	pdt_rr_count++;
	pdt_rr_lock.unlock();
	return (true);
}

void
sl_pdt::decr_rr_count(uint_t delta)
{

	pdt_rr_lock.lock();
	pdt_rr_count -= delta;
	ASSERT(pdt_rr_count >= 0);
	pdt_rr_lock.unlock();
}

cid_elem_t::cid_elem_t() :
    _DList::ListElem(this)
{
	bzero(&cid, sizeof (network::cid_t));
}

cid_elem_t::~cid_elem_t()
{
	bzero(&cid, sizeof (network::cid_t));
}

affinity_lst::affinity_lst()
{
	NET_DBG_BIND(("Affinity constructor invoked\n"));
	clnt_freelst_count = 0;
	cnct_dispatch_id = timeout(cnct_dispatch_func,
	    this, drv_usectohz(dispatch_time * 1000000));
}

affinity_lst::~affinity_lst()
{
	int i;

	NET_DBG(("Waiting for distpatch_conn_threadpool to quiesce\n"));
	dispatch_conn_threadpool.quiesce(threadpool::QEMPTY);
	stop_timeout();

	for (i = 1; i <= NODEID_MAX; i++) {
		pernodelst[i].cidlst_lock.wrlock();
		pernodelst[i].pernode_cidlst.dispose();
		pernodelst[i].cidlst_lock.unlock();
	}
	clnt_freelst_lock.lock();
	conn_freelst.dispose();
	clnt_freelst_lock.unlock();
}

void
affinity_lst::stop_timeout()
{
	NET_DBG_BIND(("stop timeout\n"));
	cnct_dispatch_lock.lock();
	if (cnct_dispatch_id != NULL) {
		(void) untimeout(cnct_dispatch_id);
		cnct_dispatch_id = NULL;
	}
	cnct_dispatch_lock.unlock();
}

cid_elem_t *
affinity_lst::get_cid_from_freelst()
{
	cid_elem_t *tmp = NULL;

	clnt_freelst_lock.lock();
	if (!conn_freelst.empty()) {
		tmp = conn_freelst.reapfirst();
		bzero((void *)&tmp->cid, sizeof (network::cid_t));
	} else {
		if (clnt_freelst_count < max_conn_cnt) {
			tmp = new (os::NO_SLEEP, os::ZERO) cid_elem_t;
			if (tmp == NULL) {
				//
				// No need to panic.
				// Log the message and continue.
				//
				NET_DBG(("memory cannot be allocated\n"));
				store_history++;
			} else {
				clnt_freelst_count++;
			}
		}
	}
	clnt_freelst_lock.unlock();
	return (tmp);
}

void
affinity_lst::append_lst(cid_elem_t *cid, nodeid_t node)
{
	pernodelst[node].cidlst_lock.wrlock();
	pernodelst[node].pernode_cidlst.prepend(cid);
	pernodelst[node].cidlst_lock.unlock();
}

// Hold pernodelst lock for the node

void
affinity_lst::add_to_freelist(nodeid_t node, network::fwd_t *fwdp)
{
	cid_elem_t 	*elem;
	uint_t		index, num_conn;

	num_conn = fwdp->fw_ncid;
	clnt_freelst_lock.lock();
	for (index = 0; index < num_conn; index++) {
		elem = lookup_cid(fwdp->fw_cidseq[index].ci_faddr, node);
		if (elem == NULL) {
			break;
		}
		pernodelst[node].cidlst_lock.wrlock();
		(void) pernodelst[node].pernode_cidlst.erase(elem);
		pernodelst[node].cidlst_lock.unlock();
		conn_freelst.prepend(elem);
	}
	clnt_freelst_lock.unlock();
}

cid_elem_t *
affinity_lst::lookup_cid(network::inaddr_t& faddr, nodeid_t node)
{
	cid_elem_t	*tmpcid = NULL;

	pernodelst[node].cidlst_lock.rdlock();
	cidlst_t::ListIterator iter(pernodelst[node].pernode_cidlst);
	while ((tmpcid = iter.get_current()) != NULL) {
		if (addr6_are_equal(tmpcid->cid.ci_faddr, faddr)) {
			break;
		}
		iter.advance();
	}
	pernodelst[node].cidlst_lock.unlock();
	return (tmpcid);
}

void
affinity_lst::get_pernode_lst(network::bind_t& bindp, nodeid_t node)
{
	cid_elem_t	*tmpcid = NULL;
	uint_t		i = 0;

	pernodelst[node].cidlst_lock.rdlock();
	bindp.bn_nodeid = node;
	bindp.bn_nfaddr =  pernodelst[node].pernode_cidlst.count();
	if (bindp.bn_nfaddr == 0) {
		pernodelst[node].cidlst_lock.unlock();
		return;
	}
	bindp.bn_faddrseq.length(bindp.bn_nfaddr);
	cidlst_t::ListIterator li(pernodelst[node].pernode_cidlst);
	while ((tmpcid = li.get_current()) != NULL) {
		(bindp.bn_faddrseq)[i] = tmpcid->cid.ci_faddr;
		i++;
		li.advance();
	}
	pernodelst[node].cidlst_lock.unlock();
}
//
// Handler to dispatch the pernodeconnlst to the proxy node
// Algorithm:
//	If the pernodelst is not empty, dispatches the deferred
//	task to handle.

void
affinity_lst::handle_dispatch(bool donot_free)
{
	cnct_dispatch_lock.lock();
	for (uint_t indx = 1; indx < NODEID_MAX+1; indx++) {
		pernodelst[indx].cidlst_lock.rdlock();
		if (pernodelst[indx].pernode_cidlst.empty()) {
			pernodelst[indx].cidlst_lock.unlock();
			continue;
		}
		pernodelst[indx].cidlst_lock.unlock();
		dispatch_conn *disp_conn;
		disp_conn = new(os::NO_SLEEP)dispatch_conn();
		if (disp_conn) {
			disp_conn->alst = this;
			disp_conn->node = indx;
			disp_conn->donot_free = donot_free;
			dispatch_conn_threadpool.defer_processing(disp_conn);
		} else {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) cl_net_syslog_msgp->log(SC_SYSLOG_WARNING,
			    MESSAGE,
			    "Failed to dispatch connection");
		}
	}

	//
	// We are being called by deregister function. Do not schedule
	// the timeout as we are transitioning to a new GIF node.
	//
	if (!donot_free) {
		cnct_dispatch_id = timeout(cnct_dispatch_func,
		    this, drv_usectohz(dispatch_time * 1000000));
	}
	cnct_dispatch_lock.unlock();

}

void
affinity_lst::clean_pernode_lst(nodeid_t node)
{
	cid_elem_t	*tmp = NULL;

	pernodelst[node].cidlst_lock.wrlock();
	clnt_freelst_lock.lock();
	while ((tmp = pernodelst[node].pernode_cidlst.reapfirst()) != NULL) {
		conn_freelst.prepend(tmp);
	}
	clnt_freelst_lock.unlock();
	pernodelst[node].cidlst_lock.unlock();
}

void
affinity_lst::dispatch_conn_node(nodeid_t node, bool free_list)
{
	cid_elem_t	*tmp = NULL;
	cidlst_t	free_lst;
	network::fwd_t *fwdp = NULL;
	Environment e;

	fwdp = new network::fwd_t;
	fwdp->fw_ncid = 0;

	pernodelst[node].cidlst_lock.wrlock();
	fwdp->fw_cidseq.length(pernodelst[node].pernode_cidlst.count());
	while ((tmp = pernodelst[node].pernode_cidlst.reapfirst()) != NULL) {
		fwdp->fw_cidseq[(uint_t)fwdp->fw_ncid] = tmp->cid;
		free_lst.prepend(tmp);
		fwdp->fw_ncid++;
	}

	pernodelst[node].cidlst_lock.unlock();
	fwdp->fw_node = orb_conf::local_nodeid();
	fwdp->fw_cidseq.length((uint_t)fwdp->fw_ncid);

	if ((fwdp->fw_ncid != 0) &&
	    (!CORBA::is_nil(mcobjp->mc_node_to_mcobj(node)))) {
		network::mcobj_ptr mcobjref = mcobjp->mc_node_to_mcobj(node);
		mcobjref->mc_setbind_table_entry(*fwdp, e);
		e.clear();
		if (!free_list) {
			clnt_freelst_lock.lock();
			while ((tmp = free_lst.reapfirst()) != NULL) {
				conn_freelst.prepend(tmp);
			}
			clnt_freelst_lock.unlock();
		} else {
			free_lst.dispose();
		}
	}

	delete fwdp;
}

dispatch_conn::dispatch_conn()
{
	alst = NULL;
	node = 0;
	donot_free = 0;
}

void
dispatch_conn::execute()
{
	alst->dispatch_conn_node(node, donot_free);
	delete this;
}
