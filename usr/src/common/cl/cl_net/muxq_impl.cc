//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.

#pragma ident	"@(#)muxq_impl.cc	1.127	08/05/20 SMI"

//
//
// Mux Queue Implementation
//

#include <sys/os.h>
#include <h/network.h>
#include <cl_net/muxq_impl.h>
#include <orb/ip/vlan.h>
#include <orb/ip/fpconf.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/transport/hb_threadpool.h>
#include <sys/mc_probe.h>
#include <cl_net/netlib.h>
#include <cl_net/sl_service.h>
#include <cl_net/mcobj_impl.h>
#include <cl_net/errprint.h>

#undef nil // <inet/common.h> redefines nil

#include <sys/disp.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <inet/common.h>
#include <inet/led.h>
#include <inet/ip.h>
#include <sys/dlpi.h>
#include <sys/sol_version.h>
#ifdef _KERNEL
#include <sys/kstr.h>
#endif

// Unode notes: most of this file deals with handling packets
// received from base Solaris, which of course we don't have
// with unode, so most things have _KERNEL around them and
// aren't used in unode.  The parts of the muxq_impl_t
// constructor that can be run in unode remain.
//
// For unode we come into the packet processing at sl_input (See
// net_unode.cc for more) and bypass the initial packet processing
// done here.  A more sophisticated setup could be done to better
// emulate how Solaris gives us packets to run more of this code
// in unode.  Maybe someday.

struct clnet_dbgstat clndb;

#ifdef _KERNEL

#include <inet/tcp.h>

#ifdef SCTP_SUPPORT
#include <netinet/sctp.h>
#endif

#include <sys/ethernet.h>

class fragment_manager;
fragment_manager	*fragment_managerp;

extern mcobj_impl_t *mcobjp;

extern fpconf *fpc;

#define	UDP_MIN_HEADER_LENGTH 8

#ifdef SCTP_SUPPORT
#define	SCTP_MIN_HEADER_LENGTH 16
#endif

#define	IP6_FRG_MIN_HEADER_LENGTH 8
#define	IP6_EXT_MIN_HEADER_LENGTH 8
#define	ICMP6_MIN_HEADER_LEN 8

#define	CL_IPH_HDR_VERSION(ipha)				\
	((int)(((ipha_t *)ipha)->ipha_version_and_hdr_length) >> 4)

time_t muxq_frag_timeout_period = 60;

// Number of fragment lists for hashed lookup of fragments
uint32_t frag_hash_size = DEF_FRAG_HASH_SIZE;

void
muxq_impl_t::demux_data(queue_t *rqp, mblk_t *mp)
{
	network::cid_action_t action;
	network::cid_t cid;
	uint_t flag;
	mblk_t *temp_mp;
	nodeid_t ret_node;

	ASSERT(mp->b_datap->db_type == M_DATA);

	//
	// Do optimized filtering first
	//
	action = naccept_main(rqp, mp, cid, flag, ret_node);

	switch (action) {
		case network::CID_DISPATCH:
		case network::CID_FORWARD:
			if (ret_node == mc_node) {
				if (!canputnext(rqp)) {
					while (mp) {
						temp_mp = mp->b_next;
						mp->b_next = NULL;
						freemsg(mp);
						mp = temp_mp;
					}
					break;
				}
				// b_next field has to be null for any
				// streams call. Since b_next field is
				// used to link up fragments which have
				// already arrived, make sure that streams
				// call handles one mp at a time.
				while (mp) {
					clndb.ndb_local_forward++;
					temp_mp = mp->b_next;
					mp->b_next = NULL;
					putnext(rqp, mp);
					mp = temp_mp;
				}
			} else {
				while (mp) {
					clndb.ndb_remote_forward++;
					temp_mp = mp->b_next;
					mp->b_next = NULL;
					send_packet(mp, ret_node, cid);
					mp = temp_mp;
				}
			}
			break;
		case network::CID_SENDUP:
			if (!canputnext(rqp)) {
				while (mp) {
					temp_mp = mp->b_next;
					mp->b_next = NULL;
					freemsg(mp);
					mp = temp_mp;
				}
				break;
			}
			while (mp) {
				clndb.ndb_sendup++;
				temp_mp = mp->b_next;
				mp->b_next = NULL;
				putnext(rqp, mp);
				mp = temp_mp;
			}
			break;
		case network::CID_NOACTION:
			clndb.ndb_noaction++;
			break;
		case network::CID_DROP:
			while (mp) {
				clndb.ndb_drop++;
				temp_mp = mp->b_next;
				mp->b_next = NULL;
				freemsg(mp);
				mp = temp_mp;
			}
			break;
		default:
			freemsg(mp);
			CL_PANIC(!"Unexpected action in muxq:demux_data\n");
	}
}

//
// This routine returns true if there is enough leading spaces to hold a data
// link header. It is called by muxq_impl_t::send_packet() before overwriting
// the spaces with the raw data link header for the fast path. It is quite
// possible that the data link header and the body of the IP message come in
// separate mblks. If this is the case, this routine might return false which
// prevents us from overwriting memory not belong to us. The mp argument points
// to the message we are intended to forward. The fplen argument is the size
// of the fast path header we got from the driver.
//
bool
muxq_impl_t::check_for_space(mblk_t *mp, size_t fplen)
{
	//
	// If data block is shared, do not overwrite.
	// DB_REF points to b_datap->db_ref, the reference count of the
	// data block. It is defined in /usr/include/sys/strsun.h.
	//
	if (DB_REF(mp) > 1)
		return (false);

	//
	// MBLKHEAD stores the offset where the data starts from the base
	// address of the data block. In another word, it is the size of the
	// leading spaces unused by the message body (it might be used by the
	// message to store hardware checksum or other private data). It is the
	// difference between b_rptr and b_datap->db_base (mp->b_rptr -
	// mp->b_datap->db_base). It is defined in /usr/include/sys/strsun.h.
	//
	if (MBLKHEAD(mp) >= (long)fplen) {
		return (true);
	} else {
		return (false);
	}
}

mblk_t *
muxq_impl_t::create_dlpi_hdr(size_t maclen)
{
	dl_unitdata_req_t *dl_udata;
	mblk_t *new_mp;

	if ((new_mp = allocb(sizeof (dl_unitdata_req_t) +
	    maclen, BPRI_HI)) == NULL) {
		NET_DBG_FWD(("muxq_impl_t::create_dlpi_hdr--Allocation "
		    "of dlpi hdr failed\n"));
		return (NULL);
	}

	new_mp->b_datap->db_type = M_PROTO;
	new_mp->b_rptr = new_mp->b_wptr;

	dl_udata = (dl_unitdata_req_t *)new_mp->b_wptr;
	new_mp->b_wptr += sizeof (dl_unitdata_req_t);
	dl_udata->dl_primitive = DL_UNITDATA_REQ;
	dl_udata->dl_dest_addr_offset = (unsigned)sizeof (dl_unitdata_req_t);

	//
	// IEEE user priority  : larger value is higher priority
	// DL unitdata request : lower value is higher priority
	// SC_SCAL_USR_PRI is guaranteed to be in [0 - MAX_8021D_UPRI].
	//
	dl_udata->dl_priority.dl_min = (MAX_8021D_UPRI - SC_SCAL_USR_PRI);
	dl_udata->dl_priority.dl_max = (MAX_8021D_UPRI - SC_SCAL_USR_PRI);

	return (new_mp);
}

void
muxq_impl_t::send_packet(mblk_t *mp, nodeid_t ret_node, network::cid_t & cid)
{
	ushort_t	sap;
	dl_unitdata_req_t *dl_udata;
	ret_data_t	data;
	mblk_t		*new_mp;

	//
	// Since we're re-creating the data link header, make sure the
	// proper SAP type is used for both IPv4 and IPv6 packets.
	//
	if (!IN6_IS_ADDR_V4MAPPED((in6_addr_t *)&cid.ci_laddr))
		sap = ETHERTYPE_IPV6;
	else
		sap = ETHERTYPE_IP;

	data.q = NULL;
	data.rmacaddr = NULL;
	data.size = 0;

	MC_PROBE_0(send_packet, "mc net muxq", "");

	//
	// Call into the path manager interface and increment the count
	// on the fp holder being used. This prevents the queue from being
	// closed when we are doing a putnext.
	//

	ASSERT(fpc);

	//
	// Done for the project to artificially inflate the size of the
	// msg to provide better bandwidth control.
	//
	size_t msz = msgdsize(mp);
	size_t msgsz = MAX(msz, 500);
	fpc->get_data_and_incr_count(sap, SC_SCAL_USR_PRI, ret_node, data,
	    msgsz, cid);


	//
	// If there are no active paths to this destination, or if no bandwidth
	// is available on the active paths, the ret_data_t structure can be
	// NULL. It is  GUARANTEED THAT THE refcount on the fp_holder has
	// not been incremented if the ret_data_t structure has been zeroed
	// out.
	//
	if (data.q == NULL) {
		clndb.ndb_remote_errors++;
		freemsg(mp);
		return;
	}
	ASSERT(data.q);
	ASSERT(data.rmacaddr);
	ASSERT(data.size);

	//
	// Hardware checksum are supported on ATM and Gigabit Ethernet
	// interfaces. For the upstream the b_ick_flag is set to
	// indicates that hardware checksum was done.  On the down
	// stream side the flag indicates that we want to do hardware
	// checksum. Since we wrote the raw data link header into the
	// spaces before the IP message starts, we might overwrite the
	// area where the hardware checksum related data is stored. This
	// may lead to data corrution. We need to turn off this flag.
	//
	// Note: This flag disappeard in Solaris 10. That's why this
	// if specifies versions of Solaris strictly earlier than S10.
	//
#if SOL_VERSION < __s10
	mp->b_ick_flag &= ~ICK_VALID;
#else
	//
	// From Solaris 10 onwards db_struioun.cksum.flags field in dblk_t
	// structure is set to non-zero by the adapter which verifies hardware
	// checksum for the upstream packets and if the same field is set to
	// non-zero for downstream packets, adapter generates the checksum
	// if it is capable. For upstream packet in public adapter if
	// the hardware checksum is done the flag will be set. We should ensure
	// to turn off this flag before forwarding the packet through the
	// transport adapter to restrict it from recalculating
	// checksum and possibly corrupting
	// the data, as other fields in dblk_t required for
	// calculating checksum (such as, db_cksumstart, db_cksumend) may
	// not be populated
	//
	mp->b_datap->db_struioun.cksum.flags = (uint16_t)0;
#endif

	if (data.fp_mp && check_for_space(mp, (size_t)MBLKL(data.fp_mp))) {
		//
		// Fastpath is supported and there are enough leading spaces
		// for a raw data link header.
		//
		new_mp = mp;

		//
		// Store the data link header in the leading spaces.
		//
		mp->b_rptr -= MBLKL(data.fp_mp);
		bcopy(data.fp_mp->b_rptr, mp->b_rptr,
		    (size_t)MBLKL(data.fp_mp));
	} else if (data.fp_mp) {
		clndb.ndb_remote_newhdr++;
		//
		// Fastpath supported, but no preceding space in dblk for us
		// to write in the new header or refcnt on dblk > 1.
		// So create a new mblk to contain the header.
		//

		if ((new_mp = dupb(data.fp_mp)) == NULL) {
			clndb.ndb_remote_errors++;
			fpc->decr_count(ret_node, data);
			freemsg(mp);
			return;
		}
		new_mp->b_cont = mp;
	} else {
		clndb.ndb_remote_newunitdata++;
		//
		// We are here because fastpath is not supported.
		//
		uchar_t *dlsap = NULL;

		new_mp = create_dlpi_hdr(data.size);

		if (new_mp == NULL) {
			clndb.ndb_remote_errors++;
			fpc->decr_count(ret_node, data);
			freemsg(mp);
			return;
		}
		//
		// The driver should use the dl_priority field of the unit
		// data request to compute the user priority to be stuffed in
		// the xmitted packet. To be doubly sure, we set the b_band
		// field of the M_DATA mblk to the right value.
		//
		mp->b_band = SC_SCAL_B_BAND;

		new_mp->b_cont = mp;

		dl_udata = (dl_unitdata_req_t *)new_mp->b_rptr;

		dlsap = new_mp->b_rptr + sizeof (dl_unitdata_req_t);
		dl_udata->dl_dest_addr_length = (unsigned)(data.size +
		    sizeof (ushort_t));
		bcopy(data.rmacaddr, dlsap, data.size);
		// Sap goes in the host order
		bcopy(&sap, dlsap + data.size, sizeof (ushort_t));
		new_mp->b_wptr += (data.size + sizeof (ushort_t));
	}

	if (canputnext(data.q)) {
		MC_PROBE_0(send_packet_putnext, "mc net muxq", "");
		//
		// put the pkt on the driver queue.
		//
		putnext(data.q, new_mp);
	} else {
		MC_PROBE_0(send_packet_putnext_failed, "mc net muxq", "");
		if (canput(data.q)) {
			//
			// Put the packet on the strhead.
			//
			(void) putq(data.q, new_mp);
		} else {
			//
			// Even the stream head is blocked.
			// So drop the packet.
			//
			clndb.ndb_remote_errors++;
			freemsg(new_mp);
		}
	}
	//
	// Drop the hold on the fp holder object. remove_path() can delete the
	// holder object now.
	//
	fpc->decr_count(ret_node, data);
}

bool
muxq_impl_t::is_address_scalable(const in6_addr_t *addrp)
{
	bool is_scalable;

	mcobjp->sl_servicegrplist_lock.rdlock();
	is_scalable = mcobjp->is_scalable_address(
	    *(network::inaddr_t *)addrp);
	mcobjp->sl_servicegrplist_lock.unlock();

	return (is_scalable);
}


network::cid_action_t
muxq_impl_t::naccept_main(queue_t *q, mblk_t *bp,
    network::cid_t& cid, uint_t& flag,
    nodeid_t &ret_node)
{
	size_t len;
	ipha_t *ipha;

	len = (size_t)(bp->b_wptr - bp->b_rptr);

	//
	// Check for 32-bit alignment of the header. Also, the header
	// must be at least that of a basic IPv4 header.
	//
	if ((!OK_32PTR(bp->b_rptr)) || (len < IP_SIMPLE_HDR_LENGTH)) {
		if (!pullupmsg(bp, (long)IP_SIMPLE_HDR_LENGTH)) {
			return (network::CID_DROP);
		}
	}

	ipha = (ipha_t *)(bp->b_rptr);

	if (CL_IPH_HDR_VERSION(ipha) == IPV4_VERSION)
		return (naccept_v4(q, bp, cid, flag, ret_node));
	else if (CL_IPH_HDR_VERSION(ipha) == IPV6_VERSION)
		return (naccept_v6(q, bp, cid, flag, ret_node));
	else
		return (network::CID_SENDUP);

}

network::cid_action_t
muxq_impl_t::naccept_v6(queue_t *q, mblk_t *mp,
    network::cid_t& cid, uint_t& flag,
    nodeid_t &ret_node)
{
	icmp6_t			*icmp6h = NULL;
	ip6_t			*ip6h;
	size_t			inner_ip6h_offset = 0;
	struct tcphdr		*tcph;
	struct udphdr		*udph;
#ifdef SCTP_SUPPORT
	sctp_hdr_t		*sctph;
	sctp_chunk_hdr_t	*chunk = NULL;
#endif
	ip6_frag_t		*ip6f = NULL;
	uint8_t			nexthdr;
	size_t			pkt_len, ip6_len;
	size_t			remlen, reqlen;
	in_port_t		srcport = 0, dstport = 0;
	in6_addr_t		*srcaddrp = NULL, *dstaddrp = NULL;
	uchar_t			*whereptr;
	uint32_t		index = 0;
	network::service_sap_t	ssap;
	network::cid_action_t	action = network::CID_DROP;
	sl_servicegrp_t		*servicep;
	fragment		*frag_holder = NULL;
	uint16_t		frag_offset = 0;
#ifdef SCTP_SUPPORT
	static unsigned long	hook_enabled = (unsigned long)
	    &cl_sctp_cookie_paddr;
#endif

	ip6h = (ip6_t *)mp->b_rptr;

	// Check for minimum header size
	if (mp->b_wptr - (uchar_t *)ip6h < IPV6_HDR_LEN) {
		if (!pullupmsg(mp, (size_t)IPV6_HDR_LEN)) {
			return (network::CID_DROP);
		}
		ip6h = (ip6_t *)mp->b_rptr;
	}

	//
	// This function won't be called with non-v6 packets.
	//
	ASSERT(CL_IPH_HDR_VERSION(ip6h) == IPV6_VERSION);

	//
	// Addresses that are "special" are not handled. These include the
	// unspecified address, loopback address, V4-mapped, and V4-compat
	// addresses. We could drop some of them (e.g. loopback and
	// V4-mapped), but the decision should be delegated to IP. We only
	// process packets that we might be interested in -- those with a
	// link-local, site-local, or global address.
	//
	if (((uint8_t *)&ip6h->ip6_src)[0] == 0 ||
	    ((uint8_t *)&ip6h->ip6_dst)[0] == 0) {
		return (network::CID_SENDUP);
	}

	//
	// Do not support multicast, both as src and dst. We will leave it
	// to the local IP to either drop it or process it.
	// The Solaris IN6_IS_ADDR_MULTICAST() macro gives 3 lint warnings
	// each time it's used in SC. So we're not using it here.
	//
	if (((uint8_t *)&ip6h->ip6_src)[0] == 0xff ||
	    ((uint8_t *)&ip6h->ip6_dst)[0] == 0xff) {
		return (network::CID_SENDUP);
	}

	//
	// Straighten out the size of the packet. If physical packet size
	// is too small, can't do anything but drop it. If physical size
	// is too large, we'll read only as much as the size specified in
	// the IP header.
	//
	if (mp->b_cont == NULL)
		pkt_len = (size_t)(mp->b_wptr - mp->b_rptr);
	else
		pkt_len = msgdsize(mp);
	ip6_len = ntohs(ip6h->ip6_plen) + IPV6_HDR_LEN;
	if (ip6_len != pkt_len) {
		ssize_t diff;

		if (ip6_len > pkt_len) {
			// Packet is too short
			return (network::CID_DROP);
		}
		diff = (ssize_t)(pkt_len - ip6_len);
		if (!adjmsg(mp, -diff)) {
			return (network::CID_DROP);
		}
		pkt_len = pkt_len - (size_t)diff;
	}

	nexthdr = ip6h->ip6_nxt;
	whereptr = ((uchar_t *)ip6h + IPV6_HDR_LEN);
	remlen = pkt_len - IPV6_HDR_LEN;

#define	PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen)			\
	if (remlen < reqlen)						\
		return (network::CID_DROP);				\
	if (mp->b_cont != NULL && (whereptr + reqlen > mp->b_wptr)) {	\
		if (!pullupmsg(mp, (ssize_t)(pkt_len - remlen + reqlen))) { \
			return (network::CID_DROP);			\
		}							\
		ip6h = (ip6_t *)mp->b_rptr;				\
		whereptr = ((uchar_t *)ip6h + (pkt_len - remlen));	\
	}

process_nexthdr:
	//
	// IPv6 header processing loop.
	// Go through the headers one by one in order to find out
	// <src-addr, src-port, dst-addr, dst-port, protocol>
	// We need this info to decide how to forward the packet.
	//
	// Keep reading until:
	// - the packet is exhausted
	// - we find a header for an un-supported protocol
	// - we find a TCP or UDP header
	// - we find an non-initial fragment for TCP/UDP
	//

	if (nexthdr == IPPROTO_TCP) {
		reqlen = TCP_MIN_HEADER_LENGTH;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		tcph = (struct tcphdr *)whereptr;

		flag = tcph->th_flags;
		srcport = ntohs(tcph->th_sport);
		dstport = ntohs(tcph->th_dport);

	} else if (nexthdr == IPPROTO_UDP) {
		reqlen = UDP_MIN_HEADER_LENGTH;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		udph = (struct udphdr *)whereptr;

		flag = 0;
		srcport = ntohs(udph->uh_sport);
		dstport = ntohs(udph->uh_dport);

#ifdef SCTP_SUPPORT
	} else if (nexthdr == IPPROTO_SCTP) {
		reqlen = SCTP_MIN_HEADER_LENGTH;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		sctph = (sctp_hdr_t *)whereptr;

		chunk = (sctp_chunk_hdr_t *)(sctph + 1);
		flag = chunk->sch_id;
		srcport = ntohs(sctph->sh_sport);
		dstport = ntohs(sctph->sh_dport);
#endif  //  SCTP_SUPPORT
	//
	// Treat IPSec protocols as upper layer protocol as the
	// the nexthdr is encapsulated and cannot be peeked.
	//
	} else if ((nexthdr == IPPROTO_AH) || (nexthdr == IPPROTO_ESP)) {
		srcport = 0;
		dstport = 0;

	} else if (nexthdr == IPPROTO_IPV6) {
		//
		// Encapsulation is not supported for scalable address.
		// Unless if this is part of an ICMP packet.
		//
		if (icmp6h == NULL)
			goto sendup_packet;

		reqlen = IPV6_HDR_LEN;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		inner_ip6h_offset = pkt_len - remlen;

		nexthdr = ((ip6_t *)whereptr)->ip6_nxt;
		remlen -= reqlen;
		whereptr += reqlen;

		goto process_nexthdr;

	} else if (nexthdr == IPPROTO_ICMPV6) {
		reqlen = ICMP6_MIN_HEADER_LEN;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		icmp6h = (icmp6_t *)whereptr;

		//
		// ICMP error messages must be forwarded to the cluster
		// node that triggered it. Info messages though can all be
		// handled by the GIF node.
		//
		if (icmp6h->icmp6_type & ICMP6_INFOMSG_MASK) {
			goto sendup_packet;
		}

		//
		// Process enclosed IPV6 header. The ICMP types that get
		// us here all have an embedded IP header.
		//
		nexthdr = IPPROTO_IPV6;
		remlen -= reqlen;
		whereptr += reqlen;

		goto process_nexthdr;

	} else if (nexthdr == IPPROTO_FRAGMENT) {
		//
		// Fragmented packet. Since fragment processing involve
		// allocation and processing of frag_holder, it is
		// costly. Avoid doing work if packet is meant for
		// non-scalable addresses.
		//
		if (!is_address_scalable(&ip6h->ip6_dst))
			goto sendup_packet;

		//
		// Fragment is for a scalable address
		//

		// Pull up the entire fragment header
		reqlen = IP6_FRG_MIN_HEADER_LENGTH;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		ip6f = (ip6_frag_t *)whereptr;

		frag_offset = ntohs(ip6f->ip6f_offlg & IP6F_OFF_MASK);
		uint32_t frag_ident = ip6f->ip6f_ident;
		uint16_t frag_extent = (uint16_t)(remlen - reqlen);
		uint16_t frag_more = (ip6f->ip6f_offlg & IP6F_MORE_FRAG);
		bool to_delete = false;

		frag_holder = fragment_managerp->fragment_compare_v6(q,
		    frag_ident, ip6h->ip6_src, ip6h->ip6_dst,
		    frag_offset, frag_extent, (bool)frag_more,
		    &to_delete);

		if (frag_holder == NULL)
			return (network::CID_DROP);

		if (frag_offset != 0) {
			//
			// Non-initial fragments. Queue up if the initial
			// one hasn't yet arrived. Otherwise forward to
			// the same node as the initial fragment.
			//
			if (frag_holder->valid_node()) {
				action = network::CID_DISPATCH;
				ret_node = frag_holder->get_nodeid(cid);
			} else {
				action = network::CID_NOACTION;
				if (!to_delete)
					frag_holder->add_mp_to_list(mp);
			}

			if (to_delete)
				delete frag_holder;
			else
				frag_holder->fragment_mutex.unlock();

			return (action);
		}

		//
		// Initial fragment. Continue to look for the upper layer
		// header.
		//
		ASSERT(frag_offset == 0);

		nexthdr = ip6f->ip6f_nxt;
		remlen -= reqlen;
		whereptr += reqlen;

		goto process_nexthdr;

	} else if (nexthdr == IPPROTO_HOPOPTS ||
	    nexthdr == IPPROTO_ROUTING ||
	    nexthdr == IPPROTO_DSTOPTS) {
		//
		// Extension headers to skip over. These options will be
		// processed by IP on the node that ultimately gets the
		// packet. Skip ahead so that we can find out which node
		// should get this packet based on subsequent headers.
		//

		// The header must be at least 8 bytes long
		reqlen = IP6_EXT_MIN_HEADER_LENGTH;
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		// Find out length of this header to know how much to skip
		// Doesn't matter what header format to use, just need to
		// extract Next Header and Ext Hdr Len.
		//
		struct ip6_hbh *hdr = (struct ip6_hbh *)whereptr;
		nexthdr = hdr->ip6h_nxt;
		reqlen = (hdr->ip6h_len << 3);
		PULLUPMSG(mp, ip6h, whereptr, remlen, reqlen);

		remlen -= (reqlen + IP6_EXT_MIN_HEADER_LENGTH);
		whereptr += (reqlen + IP6_EXT_MIN_HEADER_LENGTH);

		goto process_nexthdr;

	} else {

sendup_packet:

		if (frag_holder) {
			//
			// Not a TCP or UDP fragment. Mark all other
			// fragments of the same packet for sendup.
			//
			frag_holder->set_node(mc_node, mp);
		}
		return (network::CID_SENDUP);
	}

	//
	// The above loop can't have exited without encountering a UDP or
	// TCP header, even though such a header could have been embedded
	// in the ICMP payload. Fragments of non-UDP/TCP/SCTP should have been
	// sent up in the loop above.
	//

#ifdef SCTP_SUPPORT
	ASSERT(nexthdr == IPPROTO_TCP || nexthdr == IPPROTO_UDP ||
	    nexthdr == IPPROTO_SCTP || nexthdr == IPPROTO_AH ||
	    nexthdr == IPPROTO_ESP);
#else
	ASSERT(nexthdr == IPPROTO_TCP || nexthdr == IPPROTO_UDP ||
	nexthdr == IPPROTO_AH || nexthdr == IPPROTO_ESP);
#endif

	if (icmp6h == NULL) {
		// Non-ICMP
		srcaddrp = &ip6h->ip6_src;
#ifdef SCTP_SUPPORT
		if (nexthdr == IPPROTO_SCTP && flag == CHUNK_COOKIE) {
			// Extract primary src addr from cookie
			int r = 0;
			if (hook_enabled) {
				r = cl_sctp_cookie_paddr(chunk, srcaddrp);
			}
			if (r != 0) {
				//
				// SCMSGS
				// @explanation
				// Either the SCTP COOKIE-ACK packet does not
				// contain a cookie in the right position or
				// the cookie does not contain a primary
				// client IP address.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available.
				//
				(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE,
				    MESSAGE, "Unable to extract the primary "
				    "source IP address from the SCTP "
				    "Cookie chunk.");
			}
		}
#endif
		dstaddrp = &ip6h->ip6_dst;
	} else {
		// For ICMP, the parsing so far was done on the enclosed
		// header embedded in the ICMP message body. To find out
		// which node should get the packet, flip the source and
		// destination info before further processing.

		// If no embedded IP header was found, bad ICMP packet.
		if (!inner_ip6h_offset)
			return (network::CID_DROP);

		ip6_t *hdr = (ip6_t *)((uchar_t *)ip6h + inner_ip6h_offset);
		srcaddrp = &hdr->ip6_dst;
		dstaddrp = &hdr->ip6_src;

		in_port_t tmp_port = srcport;
		srcport = dstport;
		dstport = tmp_port;
	}

	//
	// src and dst address are crucial for subsquent processing. They
	// must have been set by now.
	//
	ASSERT(srcaddrp != NULL && dstaddrp != NULL);

	//
	// If a fragment gets here, it must be the initial fragment, since
	// non-initial fragments should have been queued up or sent up
	// above.
	//
	ASSERT(frag_holder == NULL || frag_offset == 0);

	ssap.protocol = nexthdr;
	ssap.laddr = *(network::inaddr_t *)dstaddrp;
	ssap.lport = dstport;

	mcobjp->sl_servicegrplist_lock.rdlock();

	servicep = mcobjp->mc_match(ssap);
	if (servicep == NULL) {
		//
		// Not a replicated service. Send the packet to the
		// local stream
		//
		// XXX - This may be right place to insert the hooks
		//	 for supporting dynamic filters.
		//

		if (frag_holder != NULL) {
			// set node will have to additionally
			// send up all the packets which have been
			// queued up in the fragment holder object
			// thru add_mp_to_list() call.
			// This is accomplished by attaching the mp_list
			// to the b_next field of the mp being passed
			// into the set_node call.
			// Later demux_data() will check for b_next field
			// and if present will forward the packets
			// individually.

			frag_holder->set_node(mc_node, mp);
		}
		mcobjp->sl_servicegrplist_lock.unlock();
		return (network::CID_SENDUP);
	}

	cid.ci_protocol = nexthdr;
	cid.ci_faddr = *(network::inaddr_t *)srcaddrp;
	cid.ci_fport = srcport;
	cid.ci_laddr = *(network::inaddr_t *)dstaddrp;
	cid.ci_lport = dstport;
	cid.ci_dbit = 1;
	// For TCP set the connection state to idle
	if (cid.ci_protocol == IPPROTO_TCP) {
		cid.ci_state = network::CONN_IDLE;
	} else {
		cid.ci_state = network::CONN_ESTABLSHD;
	}


	// Fragments other than the first one will not get forwarded
	// or dispatched until the destination node information is set.
	// So it is safe to update the information in the frag holder
	// object without holding locks.
	// It is necessary to store this information because fragments
	// other than the first one will not have the dstport & srcport
	// information.

	if (frag_holder != NULL) {
		frag_holder->update_info(cid.ci_protocol, srcport, dstport);
	}

	ret_node = servicep->sl_input(cid, action, index, flag);

	mcobjp->sl_servicegrplist_lock.unlock();

	if (frag_holder != NULL) {
		if (ret_node) {
			frag_holder->set_node(ret_node, mp);
		} else {
			//
			// This can happen when the node is
			// trying to boot up. The pdt table
			// is full of zeros.
			//
			action = network::CID_DROP;
			frag_holder->fragment_mutex.unlock();
		}
	}

	return (ret_node == 0? network::CID_DROP: action);
}


network::cid_action_t
muxq_impl_t::naccept_v4(queue_t *q, mblk_t *bp, network::cid_t& cid,
    uint_t& flag, nodeid_t &ret_node)
{
	ipha_t *ipha;
	network::inaddr_t srcaddr_v6;
	network::inaddr_t dstaddr_v6;
	in6_addr_t *srcaddrp = (in6_addr_t *)&srcaddr_v6;
	in6_addr_t *dstaddrp = (in6_addr_t *)&dstaddr_v6;
	uint16_t srcport;
	uint16_t dstport;
	uint16_t *up;
	long len;
	long reqlen;
	int iplen;
	nodeid_t node;
	network::cid_action_t action = network::CID_DROP;
	uint32_t index = 0;
	tcph_t	*tcph;
	icmph_t *icmph;
	network::service_sap_t ssap;
	sl_servicegrp_t	*servicep;
	uint32_t u1, u2;
	fragment *frag_holder = NULL;
#ifdef SCTP_SUPPORT
	static unsigned long hook_enabled = (unsigned long)
	    &cl_sctp_cookie_paddr;
#endif

	len = bp->b_wptr - bp->b_rptr;

	//
	// Check for minimum header size.
	//
	if (len < IP_SIMPLE_HDR_LENGTH) {
		if (!pullupmsg(bp, (long)IP_SIMPLE_HDR_LENGTH)) {
			return (network::CID_DROP);
		}
	}

	//
	// IP header complete in first mblock
	//
	ipha = (ipha_t *)(bp->b_rptr);

	//
	// This routine supports only IPv4 packets. This function
	// won't be called on a V6 packet.
	//
	ASSERT(CL_IPH_HDR_VERSION(ipha) == IPV4_VERSION);

	//
	// packet is multicast
	//
	if (CLASSD(ipha->ipha_dst))
		return (network::CID_SENDUP);

	//
	// Broadcast's are not taken care of now since we
	// do not have a way to identify the netmask from
	// mcnet yet.
	//
	if (ipha->ipha_protocol != IPPROTO_TCP &&
	    ipha->ipha_protocol != IPPROTO_UDP &&
	    ipha->ipha_protocol != IPPROTO_AH &&
	    ipha->ipha_protocol != IPPROTO_ESP &&
#ifdef SCTP_SUPPORT
	    ipha->ipha_protocol != IPPROTO_SCTP &&
#endif
	    ipha->ipha_protocol != IPPROTO_ICMP) {
		//
		// protocol not of interest to us
		// Send it up the local stream
		//
		return (network::CID_SENDUP);
	}

	IN6_IPADDR_TO_V4MAPPED(ipha->ipha_src, srcaddrp);
	IN6_IPADDR_TO_V4MAPPED(ipha->ipha_dst, dstaddrp);

	//
	// iplen contains the size of the IP header, len
	// contains the size of the mblk
	//
	iplen = (ipha->ipha_version_and_hdr_length & 0xF) << 2;
	len = bp->b_wptr - bp->b_rptr;

	//
	// Check to see if the packet is fragmented or not.
	//

	u2 = ntohs(ipha->ipha_fragment_offset_and_flags);
	u1 = u2 & (IPH_MF | IPH_OFFSET);

	if (u1) {
		//
		// Fragmented packet. Since fragment processing involve
		// allocation and processing of frag_holder, it is
		// costly. Avoid doing work if packet is meant for
		// non-scalable addresses.
		//
		if (!is_address_scalable(dstaddrp))
			return (network::CID_SENDUP);

		// We have a fragmented packet. Check to
		// see if we have the first frag which has the
		// tcp/udp/sctp headers or not. Then we can find out
		// the forwarding node info.

		uint16_t ident = ipha->ipha_ident;
		uint16_t offset = ((u1 & IPH_OFFSET) << 3);
		uint16_t extent = ipha->ipha_length - (uint16_t)iplen;
		bool mf = (u1 & IPH_MF? true: false);
		bool to_delete = false;

		// fragment_compare() will either find a holder object for the
		// fragment in the list or create a new one. To prevent a
		// race with the timeout thread, fragment_compare() will return
		// with the fragment_mutex held. It is the responsiblity
		// of the calling thread to give up the fragment_mutex.

		frag_holder = fragment_managerp->fragment_compare_v6(q, ident,
		    (in6_addr_t &)srcaddr_v6, (in6_addr_t &)dstaddr_v6,
		    offset, extent, mf,
		    &to_delete);

		if (frag_holder == NULL) {
			// No unlocking here since frag holder was not
			// created successfully.
			return (network::CID_DROP);
		}

		//
		// At this point, we should have a non-null holder object.
		//

		ASSERT(frag_holder);

		if (u2 & IPH_OFFSET) {
			// Not the first fragment.
			if (frag_holder->valid_node()) {
				// We already have the forwarding node
				// information. So just forward the fragment
				// to the same node.
				node = frag_holder->get_nodeid(cid);
				ret_node = node;
				frag_holder->fragment_mutex.unlock();
				// It is ok to set the seq_num to -1 since
				// the tcp header always goes in the
				// first fragment.
				flag = 0;
				action = network::CID_DISPATCH;
			} else {
				// The first fragment is yet to arrive.
				// Lets wait till the first one comes in.
				frag_holder->add_mp_to_list(bp);
				frag_holder->fragment_mutex.unlock();
				action = network::CID_NOACTION;
			}

			// Delete frag holder if all fragments have
			// arrived and have arrived in order. Note that if
			// PDT is all 0's, valid_node() returns false. We
			// don't care, as long as all fragments are here,
			// the frag holder is done!
			//
			if (to_delete)
				delete frag_holder;

			return (action);
		}

		//
		// We're here only if this is the first
		// fragment. frag_compare() should never have told us to
		// delete the first fragment.
		//
		ASSERT(!to_delete);
	}

	// Only the first fragment is allowed to proceed below
	// this point. This is necessary since only the first
	// fragment has a tcp/udp header.

	ASSERT(!(u2 & IPH_OFFSET));

	if ((u1) && (frag_holder->valid_node())) {
		// This is a duplicate copy of the first fragment.
		// So just discard the packet without processing
		// it further.
		frag_holder->fragment_mutex.unlock();
		return (network::CID_DROP);
	}

	// The required length of the message to be pulled
	// is the header size of IP + header size of the
	// protocol used (TCP, SCTP or UDP)

	if (ipha->ipha_protocol == IPPROTO_TCP) {
		reqlen = iplen + TCP_MIN_HEADER_LENGTH;
	} else if (ipha->ipha_protocol == IPPROTO_UDP) {
		reqlen = iplen + UDP_MIN_HEADER_LENGTH;
#ifdef SCTP_SUPPORT
	} else if (ipha->ipha_protocol == IPPROTO_SCTP) {
		reqlen = iplen + SCTP_MIN_HEADER_LENGTH;
#endif
	} else if (ipha->ipha_protocol == IPPROTO_ICMP) {
		reqlen = iplen + ICMPH_SIZE;
	} else {
		ASSERT((ipha->ipha_protocol == IPPROTO_AH) ||
		(ipha->ipha_protocol == IPPROTO_ESP));
		reqlen = iplen;
	}

	// Pull the message into the mblk bp

	if (len < reqlen) {
		if (!pullupmsg(bp, reqlen)) {
			if (u1)
				frag_holder->fragment_mutex.unlock();
			return (network::CID_DROP);
		}
		//
		// pullupmsg successful. so re-assign pointers.
		// important. cannot reuse the old pointer values
		// after pullupmsg.
		//
		ipha = (ipha_t *)bp->b_rptr;
	}

	//
	// If the size of the first fragment is less than the
	// size of iplen + TCP/SCTP/UDP_MIN_HEADER_LENGTH, we will
	// end up dropping the fragment and hence the entire packet
	// because we do not do reassembly like the ip does.
	//

	// Increment the read pointer by the size of the
	// IP header

	up = (uint16_t *)(bp->b_rptr + iplen);

	//
	// read the source port, destination port and
	// the flags (if TCP). In SCTP flag indicates the type of
	// the first chunk: CHUNK_INIT, CHUNK_COOKIE etc.
	//

	if (ipha->ipha_protocol == IPPROTO_TCP) {
		tcph = (tcph_t *)up;
		flag = tcph->th_flags[0];

		// The first two bytes of the TCP header
		// contains the src port, and the next
		// two bytes contains the destination port

		srcport = ntohs(*up++);
		dstport = ntohs(*up);

		// Set the cooresponding values in ssap

		ssap.protocol = IPPROTO_TCP;
		ssap.lport = dstport;
		ssap.laddr = dstaddr_v6;
#ifdef SCTP_SUPPORT
	} else if (ipha->ipha_protocol == IPPROTO_SCTP) {
		sctp_hdr_t *sctph = (sctp_hdr_t *)up;

		// The first two bytes of the TCP header
		// contains the src port, and the next
		// two bytes contains the destination port

		srcport = ntohs(*up++);
		dstport = ntohs(*up);

		sctp_chunk_hdr_t *chunk = (sctp_chunk_hdr_t *)(sctph + 1);
		flag = chunk->sch_id;

		//
		// If this is a cookie chunk replace srcaddrp with
		// cookie's primary address
		//
		if (flag == CHUNK_COOKIE) {
			// Extract primary src addr from cookie
			int r = 0;
			if (hook_enabled) {
				r = cl_sctp_cookie_paddr(chunk, srcaddrp);
			}
			if (r != 0) {
				(void) cl_net_syslog_msgp->log(SC_SYSLOG_NOTICE,
				    MESSAGE, "Unable to extract the primary "
				    "source IP address from the SCTP "
				    "Cookie chunk.");
			}
		}

		//
		// Set the cooresponding values in ssap
		//
		ssap.protocol = IPPROTO_SCTP;
		ssap.lport = dstport;
		ssap.laddr = dstaddr_v6;
#endif  // SCTP_SUPPORT
	} else if (ipha->ipha_protocol == IPPROTO_UDP) {
		flag = 0;

		// The first two bytes of the UDP header
		// contains the src port, and the next
		// two bytes contains the destination port
		// up contains the udp header
		srcport = ntohs(*up++);
		dstport = ntohs(*up);

		// Set the cooresponding values in ssap

		ssap.protocol = IPPROTO_UDP;
		ssap.lport = dstport;
		ssap.laddr = dstaddr_v6;
	} else if (ipha->ipha_protocol == IPPROTO_ICMP) {
		icmph = (icmph_t *)up;
		flag = 0;
		if (icmph->icmph_type == ICMP_DEST_UNREACHABLE ||
		    icmph->icmph_type == ICMP_SOURCE_QUENCH ||
		    icmph->icmph_type == ICMP_REDIRECT) {
			//
			// Scrounge around for the ip header.
			// and the following 8 bytes containing
			// atleast the first part of the tcp/udp
			// headers.
			//
			ipha_t *ipha_err = (ipha_t *)&icmph[1];
			if ((uchar_t *)&ipha_err[1] > bp->b_wptr) {
				if (!pullupmsg(bp,
				    (uchar_t *)&ipha_err[1] - bp->b_rptr)) {
				    if (u1)
					frag_holder->fragment_mutex.unlock();
				    return (network::CID_DROP);
				}
				//
				// Reset the pointers to point to the
				// new mblk after a successful pullupmsg()
				// This has to be done everywhere we do
				// a pullupmsg().
				//
				icmph = (icmph_t *)&bp->b_rptr[iplen];
				ipha_err = (ipha_t *)&icmph[1];
			}

			int total_len = iplen + ICMPH_SIZE + 8 +
			    ((ipha_err->ipha_version_and_hdr_length & 0xF)<< 2);
			//
			// Now that we have the ip header portion of icmp
			// msg is in the same mblk, try to ensure that the
			// following 8 byte data portion of the ip msg
			// is also in the same mblk.
			//
			if ((bp->b_wptr - bp->b_rptr) < total_len) {
				if (!pullupmsg(bp, (long)total_len)) {
				    if (u1)
					frag_holder->fragment_mutex.unlock();
				    return (network::CID_DROP);
				}
				icmph = (icmph_t *)&bp->b_rptr[iplen];
				ipha_err = (ipha_t *)&icmph[1];
			}

			//
			// Looking into the ip header included in the icmp
			// message is necessary because it gives an
			// indication about the actual tcp/udp msg for which
			// this icmp msg was generated. The protocol should
			// same as the protocol of the msg causing the icmp
			// error to be reported.
			//
			ssap.protocol = (uint16_t)ipha_err->ipha_protocol;
			up = (uint16_t *)(bp->b_rptr + total_len - 8);

			//
			// The destination and source port and addresses are
			// intentionally reversed for ICMP msgs since we are
			// looking at the portion of a  msg which originated
			// from a node in the cluster and not from the
			// client.
			//
			dstport = ntohs(*up++);
			srcport = ntohs(*up);
			IN6_IPADDR_TO_V4MAPPED(ipha->ipha_src, dstaddrp);
			IN6_IPADDR_TO_V4MAPPED(ipha->ipha_dst, srcaddrp);
			ssap.lport = dstport;
			ssap.laddr = dstaddr_v6;

		} else {
			if (u1) {
				//
				// This icmp msg is fragmented. Set
				// the frag holder to point to this
				// node so that the subsequent fragments
				// are forwarded to the same node.
				//
				frag_holder->set_node(mc_node, bp);
			}
			return (network::CID_SENDUP);
		}

	} else {

		//
		// Since IPSec is supported for Sticky wild service
		// treat IPPROTO_AH and IPPROTO_ESP same.
		// need to be changed later when we support sticky
		// services.
		//

		ASSERT((ipha->ipha_protocol == IPPROTO_AH) ||
		    (ipha->ipha_protocol == IPPROTO_ESP));

		ssap.protocol = ipha->ipha_protocol;
		ssap.laddr = dstaddr_v6;
		srcport = 0;
		dstport = 0;
	}

	//
	// Note: an adaptation of the code from here to the check
	// for node == 0 is in the packet() routine in net_unode.cc.
	// If you make changes here please check packet() and make
	// any necessary analogous changes.
	//

	// We require this lock to prevent the service group object from
	// being deleted.  Since the mc_match code does not obtain a lock
	// on the group object, there's nothing to ensure that no one
	// tries to delete it.
	mcobjp->sl_servicegrplist_lock.rdlock();

	servicep = mcobjp->mc_match(ssap);
	if (servicep == NULL) {
		//
		// Not a replicated service. Send the packet to the
		// local stream
		//
		// XXX - This may be right place to insert the hooks
		//	 for supporting dynamic filters.
		//

		if (u1) {
			// set node will have to additionally
			// send up all the packets which have been
			// queued up in the fragment holder object
			// thru add_mp_to_list() call.
			// This is accomplished by attaching the mp_list
			// to the b_next field of the mp being passed
			// into the set_node call.
			// Later demux_data() will check for b_next field
			// and if present will forward the packets
			// individually.

			frag_holder->set_node(mc_node, bp);
		}
		mcobjp->sl_servicegrplist_lock.unlock(); // Release lock
		return (network::CID_SENDUP);
	}

	cid.ci_protocol = ssap.protocol;
	cid.ci_laddr = dstaddr_v6;
	cid.ci_lport = dstport;
	cid.ci_faddr = srcaddr_v6;
	cid.ci_fport = srcport;
	cid.ci_dbit = 1;
	if (cid.ci_protocol == IPPROTO_TCP) {
		cid.ci_state = network::CONN_IDLE;
	} else {
		cid.ci_state = network::CONN_ESTABLSHD;
	}

	// Fragments other than the first one will not get forwarded
	// or dispatched until the destination node information is set.
	// So it is safe to update the information in the frag holder
	// object without holding locks.
	// It is necessary to store this information because fragments
	// other than the first one will not have the dstport & srcport
	// information.

	if (u1) {
		frag_holder->update_info(cid.ci_protocol, srcport, dstport);
	}

	//
	// Input processing
	//

	node = servicep->sl_input(cid, action, index, flag);
	ret_node = node;

	// Release lock on slave service group list at mcobj.
	mcobjp->sl_servicegrplist_lock.unlock();

	if (u1) {
		if (node) {
			frag_holder->set_node(node, bp);
		} else {
			//
			// This can happen when the node is
			// trying to boot up. The pdt table
			// is full of zeros.
			//
			action = network::CID_DROP;
			frag_holder->fragment_mutex.unlock();
		}
	}

	if (node == 0)
		action = network::CID_DROP;

	return (action);
}

//
// This routine gets called from the service routine (mcnet_rsrv)
// when the timer expires. All frag holder objects must be processed.
//

void
muxq_impl_t::frag_timer(queue_t *q, mblk_t *mp)
{

	NET_DBG_FWD(("muxq_impl_t::frag_timer()\n"));

	fragment_managerp->fragment_timer_expired(q, mp);

}

#endif	// _KERNEL

muxq_impl_t::muxq_impl_t()
{
	Environment		e;
	network::PDTServer_var	local_pdt_ref;

#ifdef _KERNEL
	fragment_managerp = new fragment_manager(frag_hash_size,
	    muxq_frag_timeout_period);
	ASSERT(fragment_managerp);
#endif // _KERNEL


	NET_DBG(("muxq_impl_t constructor\n"));

	local_pdt_ref = network_lib.get_pdt_ref();
	if (CORBA::is_nil(local_pdt_ref)) {
		return;
	}

	nodeid_t nid = orb_conf::local_nodeid();

	NET_DBG(("  Call pdts->register_gifnode(n=%d)\n", nid));

	(void) local_pdt_ref->register_gifnode(nid, e);

	if (e.exception()) {
		cmn_err(CE_WARN, "Failed to register gif on node %d "
		    "with PDT server \n", mc_node);
		e.clear();
		return;
		//
		// XXXX Need to Handle error from the constuctor
		//
	}
}

muxq_impl_t::~muxq_impl_t()
{
#ifdef _KERNEL
	delete fragment_managerp;
#endif	// _KERNEL
}
