/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MA_SERVICE_H
#define	_MA_SERVICE_H

#pragma ident	"@(#)ma_service.h	1.86	08/05/20 SMI"

//
// Master service object for scalable services
//

#include <h/network.h>
#include <cl_net/netlib.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/ma_pdt.h>
#include <cl_net/netlib.h>
#include <nslib/ns.h>
#include <sys/list_def.h>

class ma_serviceobj;
typedef DList<ma_serviceobj> ma_serviceobjlist_t;

const uint_t default_lb_weight = 1;

//
// Class definition of a master service object.
//
class ma_servicegrp:	public servicegrp {
public:
	// okay is set to false if the constructor failed.
	ma_servicegrp(const network::group_t &group_name,
	    network::lb_specifier_t policy_type,
	    network::lbobj_i_ptr primary_lb,
	    bool &okay, unsigned int nhash);

	// Destructor
	~ma_servicegrp();

	//
	// Forwarding list collection types.
	//
	enum {
		DEREGISTER_PROXY = 1,
		DEREGISTER_INSTANCE = 2,
		COLLECT_ALL = 3
	};



	bool equal(const network::service_sap_t &nsap);
	bool ip_equal(const network::service_sap_t &nsap);
	bool matches(const network::service_sap_t &nsap);
	bool ip_matches(const network::service_sap_t &nsap);

	void ma_dumpservice();
	unsigned long ma_dump_pdtinfo(nodeid_t node);
	void ma_distribute_pdt(nodeid_t node, uint_t type, Environment &e);

	bool is_strong_sticky();
	void ma_get_sticky_config(network::sticky_config_t &);
	// Routine returns if RR scheme is enabled for the service
	bool is_rr_enabled();
	bool is_generic_affinity();
	network::ret_value_t ma_config_sticky(const network::sticky_config_t&);

	network::ret_value_t ma_add_configlist_entry(nodeid_t node);

	bool ma_remove_node_from_clist(nodeid_t node);

	bool ma_remove_node_from_ilist(nodeid_t node);

	bool ma_is_node_in_configlist(nodeid_t node);

	bool ma_register_instance(nodeid_t node,
	    const network::service_sap_t& ssap);
	bool ma_deregister_instance(nodeid_t,
	    const network::service_sap_t& ssap);
	bool ma_deregister_node(nodeid_t);

	//
	// Return the number of nodes on which the instances
	// of this service can be run.
	//
	int ma_getnum_config_inst() { return (nconfig_inst); }

	void attach_gifnode1(const network::service_sap_t &ssap,
	    nodeid_t node, Environment &);
	void attach_gifnode2(const network::service_sap_t &ssap,
	    nodeid_t node, Environment &);
	void detach_gifnode(const network::service_sap_t &ssap,
	    nodeid_t node, Environment &);

	int ma_getnumins() { return (ninst); }

	// Return reference to the lbobj.
	network::lbobj_i_ptr get_lbobj();

	void ma_stop_forward(const network::cid_t& cid, nodeid_t node);

	//
	// The following methods are used exclusively by a master
	// service object at the secondary.
	//

	// Helper routine that dumps the state of a master service object
	// to a particular secondary that is being added.
	bool ma_dump_state(replica::checkpoint_ptr sec_chkpt);

	// Helper function that sets the number of active instances
	void ma_set_ninst(int num_of_instances);

	// Helper function that sets the instance list itself
	void ma_set_instancelist(const network::seqlong_t &inslist);

	// Helper function that sets the load distribution, which stores
	// the current instances and weights.
	void ma_set_loaddist(const network::dist_info_t &load_dist);

	// Helper function that sets the config list itself
	void ma_set_configlist(const network::seqlong_t &configlist);

	// Helper function that sets the number of configured instances
	void ma_set_nconfig_inst(int num_of_instances);

	// Initialize the master PDT for this master service object
	// with the information in "info."
	void ma_set_pdtinfo(const network::pdtinfo_t &info);

	// For this master service group object, this routine creates a new
	// master PDT table.
	void ma_create_pdt();

	// This routine initializes a master PDT with the information
	// contained in "pdtinfo."
	void ma_initialize_pdt(const network::pdtinfo_t& pdtinfo);

	network::dist_info_t *get_distribution(Environment &_environment);
	void set_distribution1(const network::dist_info_t &dist,
		Environment &_environment);
	void set_distribution2(Environment &_environment);
	void set_weight(nodeid_t node, uint32_t weight,
		Environment &_environment);

	ma_serviceobj *find_ma_serviceobj(const network::service_sap_t &ssap);
	bool ma_remove_service_object(const network::service_sap_t &ssap);
	bool node_exists_in_sobj(nodeid_t nid);

	//
	// instancelist is the list of nodes on which the service
	// is currently running, and instancelist_lock is the lock
	// protecting it.
	//
	instancelist_t	instancelist;	// instance list
	os::rwlock_t	instancelist_lock;	// instance list lock

	// config_inst_list the list of nodes on which the service
	// can be run, and config_inst_list_lock is the lock
	// protecting it.
	// instance_list is always a subset of config_inst_list.

	os::rwlock_t 	config_inst_list_lock;  // config inst list lock
	instancelist_t  config_inst_list;
	friend class lbobj;
	// Pointer to the load balancer implementation.
	lbobj		*lb;
	// Reference, so load balancer will get unreferenced when we're done.
	network::lbobj_i_var lb_ref;

	//
	// List of ma_serviceobjs associated with this ma_servicegrp
	//
	ma_serviceobjlist_t ma_serviceobj_list;
	os::rwlock_t 	ma_serviceobj_list_lock;

	//
	// Update the hash buckets with load balancing info.
	// This is the central function to change the PDT, after a
	// change in the weights or instances.
	//
	void	update_buckets();

	instancelist_t *get_clist();

	//
	// Gather service information for debugging
	//
	void marshal_pdt(network::pdtinfo_t *pdtinfo, Environment& e);
	void marshal_fwd(network::fwd_t *fwdinfo, Environment& e);
	void marshal_sgrp(network::grpinfo_t *grpinfo, Environment& e);

	// load_dist stores the current instances and distribution weights.
	network::dist_info_t  load_dist;
	bool has_pdt();
	//
	// For RR load balancing scheme
	//
	void ma_get_rr_config(network::rr_config_t &);
	network::ret_value_t ma_config_rr(const network::rr_config_t&);

private:
	// Helper function to scale the weights to the number of hash entries
	uint32_t	*ma_servicegrp::scale_buckets();

	// Node hosting the physical i/f hosting the srv_sap.
	nodeid_t	gif_node;
	int		nconfig_inst;	// Number of configured instances
	int		ninst;		// Number of active instances
	ma_pdt		*pdts;		// pdt table

	// config value for sticky service groups
	network::sticky_config_t sticky_config;

	// config info for RR enabled service groups

	network::rr_config_t rr_config;


	char *fwdinglist_to_string(uint_t type);
};

//
// Class definition of a master service object.
//
class ma_serviceobj:	public serviceobj {

public:
	ma_serviceobj(ma_servicegrp &group,
	    const network::service_sap_t &ssap);

	~ma_serviceobj();

	void ma_register_instance(nodeid_t node);
	void ma_deregister_instance(nodeid_t node);

	// Dump the state of this object to the secondaries
	void dump_ma_serviceobj(network::group_t &group,
		replica::checkpoint_ptr sec_chkpt);

	// Helper function that sets the instance list itself
	void ma_set_instancelist(const network::seqlong_t &inslist);

	//
	// Remove node from registry (this currently only deregisters gifnode)
	//
	bool ma_deregister_node(nodeid_t);

	bool node_exists_in_ilist(nodeid_t nid);

	int gname_set_add(network::group_t &group);

	//
	// instancelist is the list of nodes on which the service
	// is currently running, and instancelist_lock is the lock
	// protecting it.
	//
	instancelist_t	instancelist;	// instance list
	os::rwlock_t	instancelist_lock;	// instance list lock

	//
	// Gather service information for debugging
	//
	void marshal_sobj(network::srvinfo_t *srvinfo, Environment& e);

private:
	//
	// ma_servicegrp that references this ma_serviceobj.
	// XXX KCF - needed?
	//
	ma_servicegrp *servicegrp;
};

// Type def for list of master service group objects
typedef IntrList<ma_servicegrp, _DList> ma_servicegrplist_t;

// Type def for list of master service objects
typedef DList<ma_serviceobj> ma_serviceobjlist_t;

#endif	/* _MA_SERVICE_H */
