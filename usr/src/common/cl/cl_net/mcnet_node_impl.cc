//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident "@(#)mcnet_node_impl.cc 1.50       08/05/20    SMI"

//
// Service object implementation
//

#include <h/network.h>
#include <sys/os.h>

#include <cl_net/netlib.h>
#include <cl_net/mcnet_node_impl.h>
#include <cl_net/pdts_impl.h>
#include <cl_net/mcobj_impl.h>

#include <nslib/ns.h>
#include <netinet/in.h>

#include <orb/infrastructure/orbthreads.h>
#include <sys/rm_util.h>

//
// Effects: Constructor for primary.
//
mcnet_node_impl_t::mcnet_node_impl_t(pdts_prov_impl_t *server) :
	mc_replica_of<network::mcnet_node> (server),
	my_provider(server)
{
	cbref_v = network::mcobj::_nil();
	gif_node = false;
	node = 0;

	//
	// Since the ORB guidelines expressly forbid
	// memory allocations in the unreferenced thread,
	// we must create the deferred task object when
	// the mcnet_node object is created.  Then it's
	// just a matter of setting the fields in the
	// object with the current information at the time
	// of the unreferenced, and adding the object to the
	// deferred task queue.
	//
	// Notice that we're creating just one cleanup object here
	// in the constructor to serve dual purposes:
	// when the provider is a primary or when it's a secondary.
	// There are methods on the cleanup object to set the state
	// to either primary or secondary, so that in the execute
	// method we know what to do (since they do different things).
	// This is a storage optimization.  We could allocate two
	// different cleanup objects whose execute methods do different
	// things, but it's a waste of memory--even though this
	// mcnet_node_impl_t object is allocated once at the beginning
	// of time until the node fails.
	//
	mcnet_nodes_cleanup_task = new nodes_cleanup();
}

//
// Effects: Constructor for secondary.
//
mcnet_node_impl_t::mcnet_node_impl_t(network::mcnet_node_ptr ref,
				pdts_prov_impl_t *server) :
	mc_replica_of<network::mcnet_node>(ref),
	my_provider(server)
{
	cbref_v = network::mcobj::_nil();
	gif_node = false;
	node = 0;

	mcnet_nodes_cleanup_task = new nodes_cleanup();
}

//
// Destructor
//
mcnet_node_impl_t::~mcnet_node_impl_t()
{
	NET_DBG(("mcnet_node_impl_t destructor(node=%d)\n", node));

	cbref_v = network::mcobj::_nil();
	gif_node = false;
	node = 0;

	// lint, but only for unode, thinks the following line is
	// a memory leak, but it isn't because the task will delete
	// itself when it is done (see nodes_cleanup::execute())

	mcnet_nodes_cleanup_task = NULL;	//lint !e423
	my_provider = NULL;
}

void
mcnet_node_impl_t::_unreferenced(unref_t cookie)
{
	pdts_impl_t *pdtsp = NULL;

	NET_DBG(("mcnet_node_impl_::_unreferenced(node=%d)\n", node));

	//
	// Get the local PDTS implementation object for this provider.
	// We do NOT want the object reference since we're not
	// supposed to be making IDL invocations in an
	// unreferenced thread.
	//
	pdtsp = my_provider->get_pdtsp();
	ASSERT(pdtsp != NULL);

	//
	// Acquire a write lock here to synchronize access to this
	// object with the PDT server's method.  The PDT server is accessing
	// the C++ implementation object (CORBA) directly by indexing into
	// mcnet_node_array and then manipulating it, typically by
	// getting the cbref_v.  The PDT server's concurrently accessing
	// the object while it happens to be in the process of
	// being destroyed by this method could panic the node.
	// We access the mcnet_node_array quite a lot.
	//
	// So why not add yet another lock to synchronize
	// access to just the mcnet_node_array?  The PDT server follows
	// certain guidelines governing lock acquisition already. Adding
	// the new lock stuff would be tedious because it would have to
	// done everywhere we access the mcnet_node_array.  It is not
	// clear that a finer-grained lock confers any benefit; any
	// additional locks would affect performance. And then
	// this unreferenced method would have to obey the same rules,
	// which means importing those locks to this method.
	//
	pdtsp->pdts_lock_wrlock();

	if (_last_unref(cookie)) {
		//
		// The reason we care about whether we're a primary
		// or secondary has to do with how to handle
		// the unreferenced message.  At the primary when
		// this mcnet_node receives an unreferenced for the
		// node that failed, it is required by the ORB and the
		// HA framework that this HA object must be deleted
		// BEFORE the primary can become a secondary on
		// switchover to a new primary.  When the object is
		// deleted, an unreferenced is also sent to the
		// secondaries.  When the unreferenced is received we
		// do something different for a secondary than we do
		// for a primary.
		//
		if (my_provider->is_primary()) {
			NET_DBG(("  PRIMARY\n"));

			//
			// Set the state appropriately for the node
			// cleanup task object.
			//
			NET_DBG(("     putting task nodes_cleanup(%d)"
				" on queue\n", node));
			mcnet_nodes_cleanup_task->set_data(node,
				is_gifnode(), true);
		} else {
			//
			// What we're doing here is to deregister the
			// node ONLY at the secondary PDT server.
			// Since the deregistration at the primary
			// happens in a deferred task it is possible that
			// the task never completes if the primary fails
			// at an inopportune moment (like before
			// checkpointing).  So the node deregistration
			// never finishes, and the secondaries may never
			// find out about the deregistration.
			// To make sure that it happens we execute
			// the master deregistration code at each secondary
			// when it receives the unreferenced.
			// We do this as a deferred task in its own right.
			//
			//
			NET_DBG(("  SECONDARY\n"));
			NET_DBG(("     putting task "
			    "nodes_cleanup(%d) on queue\n", node));
			mcnet_nodes_cleanup_task->set_data(node, is_gifnode(),
				false);
			mcnet_nodes_cleanup_task->set_pdtsp(pdtsp);
		}

		//
		// For primary or secondary, the following code is executed.
		//

		// Enqueue deferred task object that does node cleanup.
		common_threadpool::the().defer_processing(
			mcnet_nodes_cleanup_task);

		//
		// Now set the mcnet_node_impl_t at index "node" in the
		// mcnet_node_array array to NULL.  This effectively means
		// that the node it represents has failed.
		//
		pdtsp->set_mcnet_node_to_null(node);

		// Now delete the mcnet_node_impl_t object.
		delete this;
	}

	// Release the pdts_lock.
	pdtsp->pdts_lock_unlock();
}

//
// Effects:  This method is the workhorse and the one that is invoked
//   by the deferred task execution stuff.
//
void
nodes_cleanup::execute()
{
	network::PDTServer_var	local_pdt_v;
	Environment		e;

	NET_DBG(("nodes_cleanup::execute(node=%d)\n", node));

	if (isprimary) {
		NET_DBG(("  nodes_cleanup::execute--PRIMARY\n"));
		//
		// Get an object reference to the current primary PDT server.
		// If the ref is NULL when the routine returns, then the
		// primary is dead, won't be coming up anytime soon, so
		// we just bail out.
		//
		local_pdt_v = network_lib.get_pdt_ref();
		if (CORBA::is_nil(local_pdt_v)) {
			delete this;
			return;
		}

		//
		// Note: If the primary is in the process of being initialized
		// then getting the reference will block until the primary
		// is initialized.  Even if it blocks forever (because the
		// primary never comes up), it is still correct to wait;
		// if the primary never comes up cluster networking is
		// hosed anyway, and we don't care if we can contact the
		// PDT server or not.
		//

		//
		// If we're hosting a global interface, deregister it.
		// Otherwise, deregister the proxy node.
		//
		if (isgif) {
			NET_DBG(("  calling IDL deregister_gifnode(%d)\n",
				node));
			local_pdt_v->deregister_gifnode(node, e);
		} else {
			NET_DBG(("  calling IDL deregister_proxynode(%d)\n",
				node));
			local_pdt_v->deregister_proxynode(node, e);
		}
		e.clear();
	} else {
		NET_DBG(("  nodes_cleanup::execute--SECONDARY\n"));
		if (isgif) {
			NET_DBG(("  calling deregister_gifnode1(%d)\n", node));
			local_pdtsp->deregister_gifnode1(node);
		} else {
			NET_DBG(("  calling deregister_proxynode1(%d)\n",
				node));
			local_pdtsp->deregister_proxynode1(node);
		} // end if-then-else
	} // end if-then-else

	// Now delete ourselves from the deferred queue.
	delete this;
}
