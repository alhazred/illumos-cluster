/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  updatable_copy_impl.h - updatable table object
 *
 */

#ifndef _UPDATABLE_COPY_IMPL_H
#define	_UPDATABLE_COPY_IMPL_H

#pragma ident	"@(#)updatable_copy_impl.h	1.20	08/12/17 SMI"

#include <h/ccr_data.h>
#include <ccr/persistent_table.h>
#include <ccr/data_server_impl.h>
#include <orb/object/adapter.h>
#include <sys/clconf_int.h>

//
// updatable_table_impl: object created dynamically by transaction manager
// in response to begin_transaction() request by user
// only one upd_table can exist at any time since writes are serialized
//
class updatable_copy_impl :
	public McServerof<ccr_data::updatable_table_copy>
{
public:
	//
	// Public interface
	//

	updatable_copy_impl();

	int initialize(const char *, const char *, const char *,
	    table_access *, data_server_impl *, bool);

	~updatable_copy_impl();

	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//

	void add_element(const char *, const char *, Environment &);
	void add_elements(const ccr::element_seq &, Environment &);

	void remove_element(const char *, Environment &);
	void remove_elements(const ccr::element_seq &, Environment &);
	void remove_all_elements(Environment &);
	void update_element(const char *, const char *, Environment &);

	void commit_update(Environment &);
	void rollback_update(Environment &);

	void commit_transaction(Environment &);
	void abort_transaction(Environment &);

	void clear_transaction(Environment &);

	void set_format_version(int32_t, Environment &);

	void lock(Environment &);
	void unlock(Environment &);

private:
	// private methods
	bool is_retry(ccr::update_type);
	int copy_table(char *, char *);
	void set_table_pathname(char **, const char *);
	void undo_transaction(Environment &);

#if	defined(WEAK_MEMBERSHIP) && defined(_KERNEL)
	bool weak_membership_is_used();
	bool is_split_brain();
#endif

	// do not use static strings (can overflow kernel stack)
	uint_t	cluster_id;
	char	*cluster_name;
	char	*table_name;
	char	*basepath;
	char	*origpath;
	char	*commitpath;
	char	*intentpath;
	char	*newtab_name;
	char	*newdir_name;
	char	*remtab_name;
	char	*remdir_name;
	data_server_impl *data_svr;
	table_access *tacc;

	// state information to handle retries
	enum trans_status_t {
		START, PREPARED, COMMITTED, DONE, UNLOCKED
	} transaction_status;
	ccr::update_type last_op;

	//
	// Protects every write operations so that we can check for
	// orphaned updates.
	//
	os::mutex_t lck;
};

#endif	/* _UPDATABLE_COPY_IMPL_H */
