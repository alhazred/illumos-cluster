//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// readonly_copy_impl.cc - readonly table object
//

#pragma ident	"@(#)readonly_copy_impl.cc	1.35	08/05/20 SMI"

#include <sys/os.h>
#include <nslib/ns.h>
#include <ccr/readonly_copy_impl.h>
#include <errno.h>

readonly_copy_impl::readonly_copy_impl():
	pathname(NULL),
	tacc(NULL),
	init_gennum(-1) {}


//
// readonly_copy_impl::initilaize()
//   Opens the file for reading. Records the gennum of the file
//   at this instant, to check for table_modified exceptions
//   for later read operations on this object.
//
int
readonly_copy_impl::initialize(char *path, table_access *ta)
{
	int retval;

	pathname = path;
	CL_PANIC(pathname);
	ASSERT(ta);
	tacc = ta;

	// Wait for writers to finish and block out future writers.
	tacc->begin_reader();
	// Record gennum for future reference. This will be used
	// to check whether the table has been modified when future
	// read ops are performed on this handle.
	init_gennum = tacc->rec.version;

	// Open the file.
	if ((retval = ptab.initialize(pathname)) != 0) {
		tacc->end_reader();
		return (retval);
	}

	// Unblock any waiting writers.
	tacc->end_reader();

	return (retval);
}

//
// readonly_copy_impl::verify_checksum()
//   Verifies that the ccr_checksum field in the table exists and
//   is correct.
//
int
readonly_copy_impl::verify_checksum()
{
	char reported_cksm[CKSM_HEX_LEN + 1], actual_cksm[CKSM_HEX_LEN + 1];
	uchar_t cksm_digest[DIGEST_LEN];
	table_element_t *next_el;
	int retval = 0;
	*reported_cksm = '\0';	// lint

	//
	// verify checksum is correct
	//

	// read checksum from file
	while ((next_el = ptab.next_element(retval)) != NULL) {
		if (!is_meta_row(next_el->key)) {
			// no checksum found
			delete next_el;
			return (1);
		}
		if (os::strcmp(next_el->key, "ccr_checksum") == 0) {
			if (os::strlen(next_el->data) != CKSM_HEX_LEN) {
				delete next_el;
				return (1);
			}
			(void) os::strcpy(reported_cksm, next_el->data);
			delete next_el;
			break;
		} else
			delete next_el;
	}

	// compute actual checksum for table
	if ((retval = ptab.compute_checksum(cksm_digest)) != 0) {
		return (retval);
	}

	// verify it matches reported checksum
	//
	// XXX is this still crashing?
	//
	encode_ccr_checksum(cksm_digest, actual_cksm);
	if (os::strcmp(reported_cksm, actual_cksm) != 0) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Table %s has invalid checksum field. Reported: %s,"
		    " actual: %s.", pathname, reported_cksm, actual_cksm);
		return (1);
	}

	return (0);
}

readonly_copy_impl::~readonly_copy_impl()
{
	ptab.close();
	pathname = NULL;
	ASSERT(tacc != NULL);
	tacc->rele();
	tacc = NULL;
}

void
#ifdef  DEBUG
readonly_copy_impl::_unreferenced(unref_t cookie)
#else
readonly_copy_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	delete this;
}


//
// IMPLEMENTATION OF IDL METHODS
//

//
// readonly_copy_impl::query_element()
//   Searches the table for the key and returns corresp data field.
//
char *
readonly_copy_impl::query_element(const char *qkey, Environment &e)
{
	char *data = NULL;
	table_element_t *tel = NULL;
	int errval;

	tacc->begin_reader();

	if (tacc->rec.version != init_gennum) {
		// table has been modified since opened
		e.exception(new ccr::table_modified());
		tacc->end_reader();
		return (NULL);
	}

	// read elements from table file
	ptab.atfirst();
	while ((tel = ptab.next_element(errval)) != NULL) {
		if (os::strcmp(tel->key, qkey) == 0) {
			tacc->end_reader();
			data = new char[os::strlen(tel->data) + 1];
			ASSERT(data);
			(void) os::strcpy(data, tel->data);
			delete tel;
			return (data);
		}
		delete tel;
	}

	tacc->end_reader();

	if (errval) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    errval));
	} else { // key not found
		e.exception(new ccr::no_such_key(qkey));
	}

	return (NULL);
}

int32_t
readonly_copy_impl::get_gennum(Environment &)
{
	return (init_gennum);
}

//
// readonly_copy_impl::atfirst()
//   Rewinds to the beginning of the table.
//
void
readonly_copy_impl::atfirst(Environment &e)
{
	tacc->begin_reader();

	if (tacc->rec.version != init_gennum) {
		// table has been modified since opened
		e.exception(new ccr::table_modified());
		tacc->end_reader();
		return;
	}

	ptab.atfirst();
	tacc->end_reader();
}

//
// readonly_copy_impl::next_element()
//
// Retrieve one element from the ccr table. If there's no more elements to
// return, an exception ccr::NoMoreElements will be returned because
// currently there is an ORB bug that prevents IDL interface from returning
// a NULL out parameter without an exception being raised.
// Note: caller needs to do proper checking of Exceptions.
//
// XXX This workaround should go away once the ORB bug is fixed.
//
void
readonly_copy_impl::next_element(ccr::table_element_out elem, Environment &e)
{
	table_element_t *tel;
	ccr::table_element *outtel;
	int errval = 0;

	tacc->begin_reader();

	if (tacc->rec.version != init_gennum) {
		// table has been modified since opened
		e.exception(new ccr::table_modified());
		tacc->end_reader();
		elem = NULL; //lint !e1514
		return;
	}

	while ((tel = ptab.next_element(errval)) != NULL) {
		// skip meta entries
		if (!is_meta_row(tel->key))
			break;
		else
			delete tel;
	}

	tacc->end_reader();

	if (errval) {
		//
		// if root filesystem is down, we might as well die too
		//
		if (errval == EIO) {
			//
			// SCMSGS
			// @explanation
			// The CCR failed to read repository due to root file
			// system failure on this node.
			// @user_action
			// The root file system needs to be replaced on the
			// offending node.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
			    "CCR: CCR unable to read root filesystem.");
		} else {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), errval));
			elem = NULL; //lint !e1514
			return;
		}
	}

	if (tel) {
		outtel = new ccr::table_element;
		ASSERT(outtel);
		outtel->key = os::strdup(tel->key);
		ASSERT((char *)outtel->key);
		outtel->data = os::strdup(tel->data);
		ASSERT((char *)outtel->data);

		elem = outtel;
		delete tel;
	} else {
		//
		// Due to an ORB bug that does not allow NULL out parameters
		// in non-exception paths, we are returning an exception here
		// in order to return an NULL out parameter.
		//
		e.exception(new ccr::NoMoreElements());
		elem = NULL; //lint !e1514
	}
}

//
// readonly_copy_impl::next_n_elements()
//
// Return how_many elements from the ccr table.
// Note: We don't return NoMoreElements exception here as we did in
// next_element because callers can find out this information easily
// from elems.length(). But they are responsible for deleting the elems
// even when the elems length is 0.
//
void
readonly_copy_impl::next_n_elements(uint32_t how_many,
				ccr::element_seq_out elems,
				Environment &e)
{
	table_element_t *tel;
	ccr::element_seq *out_elems;
	int errval = 0;
	uint32_t actual_len = 0;

	// Preallocate how_many size for out_elems, we can shrink it
	// later if needed.
	out_elems = new ccr::element_seq(how_many, how_many);
	ASSERT(out_elems);

	tacc->begin_reader();

	if (tacc->rec.version != init_gennum) {
		// table has been modified since opened
		e.exception(new ccr::table_modified());
		tacc->end_reader();
		delete out_elems;
		elems = NULL; //lint !e1514
		return;
	}

	while ((actual_len < how_many) &&
	    ((tel = ptab.next_element(errval)) != NULL)) {
		// skip meta entries
		if (!is_meta_row(tel->key)) {
			(*out_elems)[actual_len].key =
				os::strdup((char *)tel->key);
			ASSERT((char *)((*out_elems)[actual_len].key));
			(*out_elems)[actual_len].data =
				os::strdup((char *)tel->data);
			ASSERT((char *)((*out_elems)[actual_len].data));
			actual_len++;
		}
		delete tel;
	}

	tacc->end_reader();

	if (errval) {
		//
		// if root filesystem is down, we might as well die too
		//
		if (errval == EIO) {
			(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
			    "CCR: CCR unable to read root filesystem.");
		} else {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), errval));
			delete out_elems;
			elems = NULL; //lint !e1514
			return;
		}
	}

	// set the acutal size
	out_elems->length(actual_len);
	elems = out_elems;
}

//
// readonly_copy_impl::get_num_elements()
//   Returns the number of elements in the table.
//
int32_t
readonly_copy_impl::get_num_elements(Environment &e)
{
	table_element_t *tel;
	int errval;
	int32_t num_found = 0;

	tacc->begin_reader();

	if (tacc->rec.version != init_gennum) {
		// table has been modified since opened
		e.exception(new ccr::table_modified());
		tacc->end_reader();
		return (-1);
	}

	ptab.atfirst();
	while ((tel = ptab.next_element(errval)) != NULL) {
		// skip meta entries
		if (!is_meta_row(tel->key))
			num_found++;
		delete tel;
	}

	tacc->end_reader();

	if (errval) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    errval));
		return (-1);
	} else
		return (num_found);
}

//
// readonly_copy_impl::get_format_version()
//   Returns the table format version.
//   Format version is not yet implemented in the CCR, so returns
//   a zero.
//
int32_t
readonly_copy_impl::get_format_version(Environment &)
{
	return (0);
}

//
// readonly_copy_impl::register_callbacks()
//   Registers a set of callbacks with the specified table, which are
//   invoked when there are changes to this table.
//
void
readonly_copy_impl::register_callbacks(ccr::callback_ptr cb, Environment &e)
{
	if (! tacc->add_callback(cb))
		e.exception(new ccr::callback_exists);
}

//
// readonly_copy_impl::unregister_callbacks()
//   Unregisters the specified callbacks from the table.
//
void
readonly_copy_impl::unregister_callbacks(ccr::callback_ptr cb, Environment &e)
{
	if (! tacc->remove_callback(cb))
		e.exception(new ccr::no_such_callback);
}
