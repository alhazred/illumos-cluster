/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  ccr_ds.h - CCR data server header file
 *
 */

#ifndef _CCR_DS_H
#define	_CCR_DS_H

#pragma ident	"@(#)ccr_ds.h	1.7	08/05/20 SMI"

// for kernel and userland, directory path is hard coded
#define	CCR_DATA_PATH "/etc/cluster/ccr"

int ccr_data_server_initialize(char *);
int ccr_data_server_register();

#ifdef _KERNEL_ORB
int initialize_ccr_ds();
int register_ccr_ds();
#endif

#endif	/* _CCR_DS_H */
