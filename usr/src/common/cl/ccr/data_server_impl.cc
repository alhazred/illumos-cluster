//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// data_server_impl.cc - CCR data server
//

#pragma ident	"@(#)data_server_impl.cc	1.99	09/02/11 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/rm_util.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>
#include <ccr/data_server_impl.h>
#include <ccr/readonly_copy_impl.h>
#include <ccr/updatable_copy_impl.h>
#include <orb/fault/fault_injection.h>
#include <ccr/fault_ccr.h>

#include <nslib/naming_context_impl.h>

#if (SOL_VERSION >= __s10)
#include <sys/vc_int.h>
#endif // SOL_VERSION >= __s10

#if (SOL_VERSION >= __s10) && !defined(UNODE)
#include <cmm/membership_engine_impl.h>
#include <clconf/clconf_ccr.h>
#endif

data_server_impl *data_server_impl::the_data_server_implp = NULL;

data_server_impl::data_server_impl():
	handlep(NULL),
	epoch_num(0),
	dir_path(NULL),
	max_elems_expected(100),
	local_ccr_id(0),
	recovery_state(CCR_UNRECOVERED)
{
}

data_server_impl::~data_server_impl()
{
	if (handlep != nil) {
		CORBA::release(handlep);
		handlep = NULL;
	}
	delete [] dir_path;
	// XXX should empty tacc_hashtab
}

//
// static wrapper function to get the zone cluster id given the name
// name of the zone cluster.
//
int
data_server_impl::get_zcid(const char *zc_namep, uint32_t *clidp)
{
	Environment e;
	return (data_server_impl::the_data_server_implp->zc_getidbyname(
	    zc_namep, *clidp, e));
}

void
#ifdef DEBUG
data_server_impl:: _unreferenced(unref_t cookie)
#else
data_server_impl:: _unreferenced(unref_t)
#endif
{
	// This object does not support multiple 0->1 transitions
	ASSERT(_last_unref(cookie));

	// XXX Must fix this.
	// XXX stay around
	// delete this;
}

int
data_server_impl::initialize(char *path, quorum::ccr_id_t ccr_id)
{
	ccr::element_seq	*cluster_elems;
	ccr::element_seq	*elemsptr;
	table_access		*ta;
	Environment		e;
	uint_t			num_cz;
	ds_cluster_info		*clinfop;

	// set directory path
	dir_path = new char[os::strlen(path) + 1];
	ASSERT(dir_path);
	(void) os::strcpy(dir_path, path);

	local_ccr_id = ccr_id;

	//
	// read directory table contents into memory
	//
	// No locking is necessary to protect writing directory_elems
	// because the DS is just being initialized. The DS has not yet
	// instantiated the directory object, nor registered it with
	// the local name server. Therefore the clients can not yet
	// access the CCR even in read-only mode.
	//
	cluster_elems = get_table_data_in(DEFAULT_CLUSTER, CLUSTER_DIR, e);
	if (cluster_elems == NULL) {
		//
		// SCMSGS
		// @explanation
		// Reading the CCR metadata failed on this node during the CCR
		// data server initialization.
		// @user_action
		// There may be other related messages on this node, which may
		// help diagnose the problem. For example: If the root disk on
		// the afflicted node has failed, then it needs to be
		// replaced. If the cluster repository is corrupted, then boot
		// this node in -x mode to restore the cluster repository from
		// backup or other nodes in the cluster. The cluster
		// repository is located at /etc/cluster/ccr/. Contact
		// your authorized Sun service provider for assistance.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"CCR: Can't read CCR metadata.");
		CCR_DBG(("CCR DS: Reading directory error in "
		    "get_table_data_in\n"));
		e.clear();
		return (ENOENT);
	}

	clinfop = new ds_cluster_info(0, DEFAULT_CLUSTER, dir_path);
	ASSERT(clinfop);
	ta = new table_access;
	ASSERT(ta);
	ta->rec = clinfop->get_consis_in(CLUSTER_DIR, e);
	if (e.exception()) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"CCR: Can't read CCR metadata.");
		CCR_DBG(("CCR DS: reading directory error in get_consis_in\n"));
		e.clear();
		delete ta;
		return (ENOENT);
	}

	num_cz = cluster_elems->length();

	for (uint_t i = 0; i < num_cz; i++) {
		char *clusterp = (*cluster_elems)[i].key;
		char *cluster_idp = (*cluster_elems)[i].data;
		ASSERT(cluster_idp);
		ASSERT(clusterp);
		uint_t cluster_id = (uint_t)os::atoi(cluster_idp);
		elemsptr = get_table_data_in(clusterp, DIR, e);
		if (e.exception() != NULL) {
			//
			// SCMSGS
			// @explanation
			// Reading the CCR metadata failed on this node during
			// the CCR data server initialization.
			// @user_action
			// There may be other related messages on this node,
			// which may help diagnose the problem. For example:
			// If the root disk on the afflicted node has failed,
			// then it needs to be replaced. If the cluster
			// repository is corrupted, then boot this node in
			// -x mode to restore the cluster repository from
			// backup or other nodes in the cluster. The cluster
			// repository is located at /etc/cluster/ccr/.
			// Contact your authorized Sun service provider for
			// assistance.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Can't read CCR metadata for cluster %s.",
			    clusterp);
			CCR_DBG(("CCR DS: reading directory error in "
			    "get_table_data_in for cluster %s\n", clusterp));
			e.clear();
			return (ENOENT);
		}
		//
		// Add a new cluster to the global list of clusters.
		//
		if (is_default_cluster(clusterp)) {
			clinfop->tacc_hashtab.add(ta, CLUSTER_DIR);
			clinfop->directory_elems.seq = elemsptr;
			cluster_list.append(clinfop);
		} else {
			ds_cluster_info *new_cl =
			    new ds_cluster_info(cluster_id, clusterp,
			    dir_path);
			ASSERT(new_cl);
			char *cluster_namep = os::strdup(clusterp);
			ASSERT(cluster_namep);
			new_cl->directory_elems.seq = elemsptr;

			cl_mutex.lock();
			cluster_list.append(new_cl);
			cl_mutex.unlock();
			//
			// Update the hash tables
			//
			clid_lock.wrlock();
			clname_hashtab.add(cluster_id, cluster_namep);
			clid_hashtab.add(cluster_namep, cluster_id);
			clid_lock.unlock();
			//
			// Create a local context for the ZC
			//
			if (!ns::bind_cz_context(cluster_idp, LOCAL_NS,
			    true, NS_ALLOW_ALL_OPP_PERM, cluster_id)) {
				ASSERT(0);
			}
		}
	}

	//
	// Add records for user tables
	//
	refresh_cluster_info();

	CCR_DBG(("CCR DS: Data server with path %s initialized\n %p\n", path,
	    this));
	return (0);
}

void
data_server_impl::register_with_tm(Environment &e)
{
	CORBA::Object_var		obj_v;
	ccr_trans::manager_var		mgr_v;
	ccr_data::data_server_var	ds_v;
	Environment			env;
	CORBA::Exception		*ex;
	ccr_trans::comm_error		*ccr_ex;

	CCR_DBG(("CCR DS: in register_with_tm\n"));
	//
	// register myself with TM
	//

	// get ccr manager from rma
	replica::rm_admin_var rm_v = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_v));
	replica::service_admin_var control_v;
	control_v = rm_v->get_control("ccr_server", env);
	if (env.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR data server could not find the CCR transaction
		// manager in the cluster.
		// @user_action
		// Reboot the cluster. Also contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Could not find the CCR transaction manager.");
		CCR_DBG(("CCR DS: register_with_tm: tm_not_found exception\n"));
		env.clear();
		e.exception(new ccr_data::tm_not_found());
		return;
	}
	obj_v = control_v->get_root_obj(env);
	if ((ex = env.exception()) != NULL) {
		char	*err_msg = new char[15];
		ASSERT(err_msg);
		if (replica::deleted_service::_exnarrow(ex)) {
			os::sprintf(err_msg, "deleted");
		} else if (replica::uninitialized_service::_exnarrow(ex)) {
			os::sprintf(err_msg, "uninitialized");
		} else if (replica::failed_service::_exnarrow(ex)) {
			os::sprintf(err_msg, "failed");
		} else {
			CL_PANIC(0);
		}

		//
		// SCMSGS
		// @explanation
		// The CCR service is not available due to the indicated
		// failure.
		// @user_action
		// Reboot the cluster. Also contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: CCR service not available, service is %s.", err_msg);
		delete [] err_msg;
		env.clear();
		e.exception(new ccr_data::tm_not_found());
		return;
	} else {
		CL_PANIC(!CORBA::is_nil(obj_v));
	}
	mgr_v = ccr_trans::manager::_narrow(obj_v);

	ds_v = this->get_objref();
	for (; ; ) {
		handlep = mgr_v->register_data_server(ds_v, local_ccr_id, env);
		// check for retry flag
		if (((ex = env.release_exception()) != NULL) &&
		    ((ccr_ex = ccr_trans::comm_error::_exnarrow(ex)) !=
		    NULL) && ccr_ex->retry) {
			// try again because primary is frozen
			delete ex;

			// Sleep for a ms before retrying. This way, we
			// give up the CPU to other threads and prevent
			// entering an infinite loop on single-cpu nodes.
			os::usecsleep((os::usec_t)1000);
		} else {
#if defined(FAULT_INJECTION)
			// If we caused the system error for a test, reboot.
			if (fault_triggered(FAULTNUM_CCR_SYSTEM_ERROR_NODE,
			    NULL, NULL)) {
				fault_ccr::reboot_node(
				    FAULTNUM_CCR_SYSTEM_ERROR_NODE, NULL);
			}
#endif // FAULT_INJECTION

			// no other exceptions can be returned.
			CL_PANIC(ex == NULL);
			break;
		}
	}

	//
	// We are going to access the recovery_state that might be
	// changed by TM if there are other nodes trying to join.
	// So we need to use lock to protect reading the recovery_state.
	//
	recovery_mutex.lock();
	while (recovery_state != CCR_RECOVERED) {
		//
		// The do_recovery returns with no success. Possible causes
		// are no quorum when the node joins, loss of quorum during
		// do_recovery, no valid directory table, epoch on the
		// nodes that constitute a quorum or system_error on this node.
		//
		if (recovery_state == CCR_SYSTEM_ERROR) {
			//
			// we had some fatal ccr::system_error during
			// our or some other nodes's manager_impl::do_recovery
			// It means this node is bad, should go away. Otherwise,
			// it will prevent the do_recovery from success.
			//

			//
			// SCMSGS
			// @explanation
			// This node failed to access its cluster repository
			// when it first came up in cluster mode and tried to
			// synchronize its repository with other nodes in the
			// cluster.
			// @user_action
			// This is usually caused by an unrecoverable failure
			// such as disk failure. There may be other related
			// messages on this node, which may help diagnose the
			// problem. If the root disk on the afflicted node has
			// failed, then it needs to be replaced. If the root
			// disk is full on this node, boot the node into
			// non-cluster mode and free up some space by removing
			// unnecessary files. Contact your authorized
			// Sun service provider for assistance.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Failed to access cluster repository during "
			    "synchronization. ABORTING node.");
			e.exception(new ccr_data::register_failed());
			recovery_mutex.unlock();
			return;
		}

		//
		// We don't return error but goto sleep at this point
		// because we are hoping a new joining node would bring
		// us quorum or valid directory or epoch files. As soon
		// as the do_recovery succeeds, it will wake up all the
		// waiting nodes in the cluster.
		//
		// The DS could also be woken up when a new node is doing
		// do_recovery and detects some fatal ccr::system_error
		// on this node, which prevents the do_recovery from continuing.
		// So the do_recovery wakes up this offending node and asks
		// it to leave the cluster membership.
		//

		//
		// SCMSGS
		// @explanation
		// This node is waiting to finish the synchronization of its
		// repository with other nodes in the cluster before it can
		// join the cluster membership.
		// @user_action
		// This is an informational message, generally no user action
		// is needed. If all the nodes in the cluster are hanging at
		// this message for a long time, look for other messages. The
		// possible cause is the cluster hasn't obtained quorum, or
		// there is CCR metadata missing or invalid. If the cluster is
		// hanging due to missing or invalid metadata, the ccr
		// metadata needs to be recovered from backup. Contact your
		// authorized Sun service provider for assistance.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_NOTICE, MESSAGE,
		    "CCR: Waiting for repository synchronization to finish.");
		//
		// The recovery_mutex lock will be released in recovery_cv.wait,
		// after it is woken up, the recovery_mutex lock will be grabbed
		// again.
		//
		recovery_cv.wait(&recovery_mutex);
	}
	recovery_mutex.unlock();

	//
	// Check if there are any tables that need to be updated by this node.
	// It happens when this node carries a valid copy of a table with
	// override flag set, which was recovered as invalid in previous
	// do_recovery before this node joined. If so, do we need to update
	// the copies of the listed tables onto other nodes in cluster with
	// our valid copies.
	//
	//
	// In that case, we registered after do_recovery is done. During the
	// registration, we found that we carry one or more valid non directory
	// table files that are marked as invalid in the current cluster
	// membership. We need to update those table files to other nodes in
	// the cluster.
	//
	ds_cluster_info *clinfop;
	char		*tnamep;
	ccr_trans::manager_ptr	mgr_p = ccr_trans::manager::_duplicate(mgr_v);
	SList<char>	*local_cluster_listp;
	char		*clname;

	local_cluster_listp = new SList<char>;
	create_cluster_list(local_cluster_listp);

	while ((clname = local_cluster_listp->reapfirst()) != NULL) {
		clinfop = lookup_cluster_info(clname);
		ASSERT(clinfop);
		if (clinfop->update_table_list == NULL) {
			continue;
		}
		while ((tnamep = clinfop->update_table_list->reapfirst())
		    != NULL) {
			//
			// we don't care about failure because even if
			// we fail to update this table, the table will
			// still be treated as invalid as it was.
			//
			update_invalid_table(mgr_p, clinfop, tnamep);
			delete [] tnamep;
		}
		// delete this list as we will never use it again.
		delete clinfop->update_table_list;
		clinfop->update_table_list = NULL; //lint !e423
	}
	delete local_cluster_listp;
	CORBA::release(mgr_p);
}

//
// To update the copies of the specified table on other nodes in cluster
// with the valid copy on this node.
//
void
data_server_impl::update_invalid_table(ccr_trans::manager_ptr mgr_p,
    ds_cluster_info *clinfop, char *tname)
{
	readonly_persistent_table	*rdp;
	ccr::readonly_table_var		rd_v;
	ccr::updatable_table_var	upd_v;
	Environment			env;
	CORBA::Exception		*ex;
	ccr::element_seq		*seq;
	table_element_t			*elem;
	int				ret;
	uint_t				len = 0;
	char				*readpath;

	//
	// SCMSGS
	// @explanation
	// This joining node carries a valid copy of the indicated table with
	// override flag set while the current cluster memebership doesn't
	// have a valid copy of this table. This node will update its copy of
	// the indicated table to other nodes in the cluster.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) ccr_syslog_msg.log(SC_SYSLOG_NOTICE, MESSAGE,
	    "CCR: Updating invalid table %s in cluster %s.\n", tname,
	    clinfop->clusterp);
	//
	// The table is currently treated as invalid, so we need
	// to start the transaction as begin_transaction_invalid_ok.
	//
	upd_v = mgr_p->begin_transaction_invalid_ok_zc(clinfop->clusterp,
	    tname, env);
	if (env.exception() != NULL) {
		CCR_DBG(("CCR DS: exception in begin_transaction_invalid_ok "
		    "in update_invalid_table\n"));
		env.clear();
		return;
	}

	//
	// check to see if the table is still invalid. In case the
	// table has already been updated by another joining node,
	// we don't need to do it again.
	//
	// The table on this node is valid, but it has been marked as
	// invalid during register_with_tm(0 by the TM and once the
	// update_invalid method completes, the table will be come valid
	// for the entire cluster. By doing so, we can easily tell if
	// the table has been updated or not. And in case the update_invalid
	// fails, the table will remain invalid to be consistent with
	// other nodes.
	//
	rd_v = lookup_table(tname, clinfop, env);
	if ((ex = env.exception()) == NULL) {
		CCR_DBG(("CCR DS: table %s in cluster %s has already been "
		    "updated to valid.  Abort updating.\n", tname,
		    clinfop->clusterp));
		upd_v->abort_transaction(env);
		env.clear();
		return;
	} else if (ccr::table_invalid::_exnarrow(ex) == NULL) {
		//
		// either no_such_table or system_error exception can be
		// returned. Any exceptions other than system_error are
		// unexpected.
		//
		ccr::system_error	*err;
		CL_PANIC((err = ccr::system_error::_exnarrow(ex)) != NULL);
		CCR_DBG(("CCR DS: updating invalid table %s in cluster %s "
		    "encountered system error errno = %d during lookup.\n",
		    tname, clinfop->clusterp, err->errnum));
		upd_v->abort_transaction(env);
		env.clear();
		return;
	}
	env.clear();

	//
	// Get the full path of this table. No need to copy to a new file
	// for reading since we are the only one that is updating this table.
	//
	if (os::strcmp(tname, CLUSTER_DIR) == 0) {
		set_table_pathname(&readpath, ".", tname);
	} else {
		set_table_pathname(&readpath, clinfop->clusterp, tname);
	}

	// read the table file on this node
	rdp = new readonly_persistent_table;
	if (rdp->initialize(readpath) != 0) {
		CCR_DBG(("CCR DS: failed to read file %s in "
		    "update_invalid_table\n", readpath));
		rdp->close();
		delete rdp;
		delete [] readpath;
		upd_v->abort_transaction(env);
		return;
	}

	upd_v->remove_all_elements(env);
	if (env.exception()) {
		CCR_DBG(("CCR DS: exception in remove_all_elements in "
		    "update_invalid_table\n"));
		rdp->close();
		delete rdp;
		(void) os::file_unlink(readpath);
		delete [] readpath;
		upd_v->abort_transaction(env);
		env.clear();
		return;
	}

	rdp->atfirst();
	seq = new ccr::element_seq(len);
	while ((elem = rdp->next_element(ret)) != NULL) {
		if (!is_meta_row(elem->key)) {
			seq->length(len+1);
			(*seq)[len].key = elem->key;
			(*seq)[len].data = elem->data;
			len++;
		}
	}

	upd_v->add_elements(*seq, env);
	rdp->close();
	delete rdp;
	(void) os::file_unlink(readpath);
	delete [] readpath;
	delete(ccr::element_seq *)seq;
	if (env.exception()) {		// exception from add_elements
		CCR_DBG(("CCR DS: exception in add_elements in "
		    "update_invalid_table\n"));
		upd_v->abort_transaction(env);
		env.clear();
		return;
	}

	upd_v->commit_transaction(env);
	if (env.exception()) {
		CCR_DBG(("CCR DS: exception in commit_transaction in"
		    "update_invalid_table%\n"));
		upd_v->abort_transaction(env);
		env.clear();
		return;
	}
}

//
// Read epoch number from disk.
//
// If the read fails for some reason for the original EPOCHFILE
// file, try the OLDEPOCHFILE.
//
ccr_data::epoch_type
data_server_impl::get_epoch(Environment &e)
{
	ccr_data::epoch_type 	retval;

	//
	// First try to read the epoch number from EPOCHFILE.
	//
	retval = get_epoch_in(EPOCHFILE, e);
	if (e.exception()) {
		CCR_DBG(("CCR DS: Error %d in accessing epoch file %s/%s\n",
		    retval, dir_path, EPOCHFILE));
		// Error reading the EPOCHFILE. Try its backup OLDEPOCHFIL
		// after clearing the exception.
		e.clear();
		retval = get_epoch_in(OLDEPOCHFILE, e);

		if (e.exception()) {
			// Could not read either of the two epoch
			// files. This is really bad, and should
			// not happen. But we will let our caller
			// (CCR-TM) take appropriate action.
			CCR_DBG(("CCR DS: Error %d in accessing epoch file "
			    "%s/%s\n", retval, dir_path, OLDEPOCHFILE));
		}
		// For the case when there is no exception at this point,
		// the epoch file had access problems, But the backup did
		// not. Since the CCR-TM is going to update our epoch
		// file after computing the highest epoch number, there
		// is no need to copy the backup to the epoch file here.
	}
	return (retval);
}

//
// Writes the epoch number in the epoch file.
//
// Since the epoch file is one of the most critical files for
// CCR, be really paranoid about writing out its contents and
// maintaining its backup.
// The strategy followed here is the same one followed for
// the path_to_inst file in Solaris. The steps are:
//
// I.  write out the new contents in filename.tmp
// II. copy contents of filename to filename.bak.tmp
// III.rename filename.bak.tmp to filename.bak
// IV. rename filename.tmp to filename
//
// The above steps ensure that there will always be either
// a filename or a filename.bak file irrespective of where
// in the sequence of steps the system were to crash.
//
void
data_server_impl::set_epoch(ccr_data::epoch_type epnum, Environment &e)
{
	updatable_persistent_table 	tmp_table;
	readonly_persistent_table	epoch_table;
	char 				metastr[10];
	char 				*epoch_pathname		= NULL;
	char				*tmp_pathname		= NULL;
	char				*old_pathname		= NULL;
	char				*oldtmp_pathname	= NULL;
	int 				retval;

	// record epoch
	epoch_num = epnum;

	set_table_pathname(&epoch_pathname, DEFAULT_CLUSTER, EPOCHFILE);
	set_table_pathname(&old_pathname, DEFAULT_CLUSTER, OLDEPOCHFILE);

	// Create pathnames for the tmp epoch files.
	tmp_pathname = new char[os::strlen(epoch_pathname) +
	    os::strlen(TMPSUFFIX) + 1];
	os::sprintf(tmp_pathname, "%s%s", epoch_pathname, TMPSUFFIX);

	oldtmp_pathname = new char[os::strlen(old_pathname) +
	    os::strlen(TMPSUFFIX) + 1];
	os::sprintf(oldtmp_pathname, "%s%s", old_pathname, TMPSUFFIX);

	// I. Write out the epoch number in file epoch.tmp
	retval = tmp_table.initialize(tmp_pathname);
	if (retval != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		CCR_DBG(("CCR DS: Error %d in initializing epoch file %s\n",
		    retval, tmp_pathname));
		goto done;
	}

	// write epoch
	os::sprintf(metastr, "%ld", epnum);
	if ((retval = tmp_table.insert_element(EPOCHSTR, metastr)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		CCR_DBG(("CCR DS: Error %d in writing to epoch file %s\n",
		    retval, tmp_pathname));
		(void) tmp_table.close();
		goto done;
	}

	if ((retval = tmp_table.close()) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		CCR_DBG(("CCR DS: Error %d in closing epoch file %s\n", retval,
		    tmp_pathname));
		goto done;
	}

	//
	// Ok, we have the epoch number written in a temp epoch file. Now do the
	// somewhat tricky updating and renaming of the various files.
	//
	// II. Copy file epoch to epoch.bak.tmp
	//
	// Ie, create a temporary file to contain a backup copy of the original
	// epoch file. If epoch file does not exist, then simply rename the
	// epoch.tmp file to epoch and return.
	//
	if (epoch_table.initialize(epoch_pathname) != 0) {
		CCR_DBG(("CCR DS: Epoch file %s does not exist. Renaming %s to "
		    "it\n", epoch_pathname, tmp_pathname));
		if ((retval =
		    os::file_rename(tmp_pathname, epoch_pathname)) != 0) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			CCR_DBG(("CCR DS: Error %d in renaming epoch file %s "
			    "to %s\n", retval, tmp_pathname, epoch_pathname));
		}
		goto done;
	}

	epoch_table.close();

	// Copy the contents of epoch file into the bak backup.
	// Use the basic vn ops to do this copy chunk by chunk, rather than
	// using CCR data structures to copy element by element.
	retval = os::file_copy(epoch_pathname, oldtmp_pathname);
	if (retval != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		CCR_DBG(("CCR DS: Error %d in copying from epoch file %s to "
		    "backup %s.\n", retval, epoch_pathname, oldtmp_pathname));
		goto done;
	}


	// III. Rename epoch.old.tmp to epoch.bak
	if ((retval = os::file_rename(oldtmp_pathname, old_pathname)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		CCR_DBG(("CCR DS: Error %d in renaming epoch file %s to %s\n",
		    retval, oldtmp_pathname, old_pathname));
		goto done;
	}

	// IV. Rename epoch.tmp to epoch
	if ((retval = os::file_rename(tmp_pathname, epoch_pathname)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		CCR_DBG(("CCR DS: Error %d in renaming epoch file %s to %s\n",
		    retval, tmp_pathname, epoch_pathname));
	}

done:
	if (epoch_pathname)
		delete [] epoch_pathname;
	if (old_pathname)
		delete [] old_pathname;
	if (tmp_pathname) {
		(void) os::file_unlink(tmp_pathname);
		delete [] tmp_pathname;
	}
	if (oldtmp_pathname) {
		(void) os::file_unlink(oldtmp_pathname);
		delete [] oldtmp_pathname;
	}
}

//
// Get the consistency information of a table in a cluster.
//
void
data_server_impl::get_consis(const char *tname, ccr_data::consis &rec,
    Environment &env)
{
	ds_cluster_info	*clinfop;

	//
	// Get the cluster from the environment.
	//
	char *cluster = get_cluster_from_env(env);
	clinfop = lookup_cluster_info(cluster);
	if (clinfop == NULL) {
		env.exception(new ccr::no_such_zc());
		delete [] cluster;
		return;
	}
	rec = clinfop->get_consis_in(tname, env);
	delete [] cluster;
}

//
// Get the consistency information of a table in a given cluster.
//
void
data_server_impl::get_consis_zc(const char *zc_namep, const char *tname,
    ccr_data::consis &rec, Environment &env)
{
	ds_cluster_info *clinfop;
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, env)) {
		env.exception(new ccr::invalid_zc_access());
		return;
	}
	clinfop = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		env.exception(new ccr::no_such_zc());
		return;
	}
	rec = clinfop->get_consis_in(tname, env);
}

//
// Unused
//
ccr_data::consis_seq *
data_server_impl::get_all_consis(Environment &)
{
	// create empty sequence for now
	ccr_data::consis_seq *conslist = new ccr_data::consis_seq();
	CL_PANIC(conslist);
	return (conslist);
}

//
// Return the table information. If the  table is the cluster directory
// or a directory in a specific cluster, we return the information
// as a sequence of clusters (including the default global cluster) or
// list of tables in a cluster from the data structures in the Data server.
// Otherwise the table is read from the disk and the contents are returned
// back to the caller.
//
ccr::element_seq *
data_server_impl::get_table_data_zc(const char *zc_namep, const char *tname,
    Environment &e)
{
	ds_cluster_info	*clinfop;
	uint_t		num_cz;
	uint_t		num_tables;
	char		*key;
	char		*data;
	char		cl_id[65];

	//
	// Verify this request is coming from an appropriate zone
	//


	if (!check_cluster_access(zc_namep, e)) {
		e.exception(new ccr::invalid_zc_access());
		return (NULL);
	}
	//
	// If the table is CLUSTER_DIR or DIR, we have already read the
	// files during initialization, so just return as a
	// sequence
	//
	// No need to protect reads to the list of clusters or
	// list of tables in a cluster.
	// This function is called by TM either in the recovery case
	// or the node joining case where the TM guarantees that there
	// are no transactions in progress so it is not possible
	// to change the list of clusters or the list of tables in
	// the cluster.
	//
	CCR_DBG(("CCR DS: Table data: Cluster = %s tname =  %s\n", zc_namep,
	    tname));

	if (is_cluster_dir(tname)) {

		CL_PANIC(is_default_cluster(zc_namep));
		cl_mutex.lock();
		num_cz = cluster_list.count();
		CCR_DBG(("CCR DS: Number of clusters = %d\n", num_cz));
		//
		// Copy list of clusters.
		//
		ccr::element_seq *tels = new ccr::element_seq(num_cz, num_cz);

		uint_t indx = 0;
		for (cluster_list.atfirst(); (clinfop =
		    cluster_list.get_current()) != NULL;
		    cluster_list.advance()) {
			CL_PANIC(tels);
			(*tels)[indx].key = os::strdup(clinfop->clusterp);
			(void) os::itoa((int)clinfop->cluster_id, cl_id);
			(*tels)[indx].data = os::strdup(cl_id);
			indx++;
		}
		cl_mutex.unlock();
		return (tels);
	} else if (is_dir(tname)) {

		clinfop = lookup_cluster_info(zc_namep);
		if (clinfop == NULL) {
			e.exception(new ccr::no_such_zc());
			CCR_DBG(("CCR DS: No such cluster %s\n", zc_namep));
			return (NULL);
		}
		//
		// Check if there are any tables in the directory.
		//
		ccr::element_seq *tels = NULL;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
			tels = new ccr::element_seq(num_tables, num_tables);
			CL_PANIC(tels);
			for (uint_t indx = 0; indx < num_tables; indx++) {
				key = (*clinfop->directory_elems.seq)[indx].key;
				data = (*clinfop->directory_elems.seq)[indx].
				    data;
				ASSERT(key);
				ASSERT(data);
				(*tels)[indx].key = os::strdup(key);
				(*tels)[indx].data = os::strdup(data);
			}
		} else {
			tels = new ccr::element_seq(0, 0);
			CL_PANIC(tels);
		}
		return (tels);
	} else {
		//
		// This is a data table, will have to read the file.
		//
		return (get_table_data_in(zc_namep, tname, e));
	}
}

//
// Get the information in a table in the cluster from where the
// request originated.
//
ccr::element_seq *
data_server_impl::get_table_data(const char *tname, Environment &env)
{
	ccr::element_seq *elems;
	//
	// get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	elems = get_table_data_zc(clusterp, tname, env);
	delete [] clusterp;
	return (elems);
}

//
// Replace the table in the global cluster with a new sequence of elements.
//
void
data_server_impl::replace_table(const char *tname,
				const ccr::element_seq &elems,
				const ccr_data::consis &record,
				Environment &env)
{
	//
	// get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	replace_table_zc(clusterp, tname, elems, record, env);
	delete [] clusterp;
}

//
// Search and return the ds_cluster_info object for a cluster
// from the cluster list.
//
ds_cluster_info*
data_server_impl::lookup_cluster_info(const char *cluster)
{
	ds_cluster_info	*cltmp;
	ds_cluster_info	*clinfop = NULL;

	cl_mutex.lock();

	for (cluster_list.atfirst(); (cltmp = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		if (os::strcmp(cltmp->clusterp, cluster) == 0) {
			// Found match
			clinfop = cltmp;
			break;
		}
	}
	cl_mutex.unlock();
	return (clinfop);
}

//
// Updates the in-core list of all the clusters. For all the
// clusters that have been removed, the tables of the cluster
// and the directory are removed.
//
void
data_server_impl::cleanup_cluster_info(const ccr::element_seq &elems)
{
	uint_t 			indx;
	uint_t			nelems;
	ds_cluster_info		*clinfop;
	char			*orig_path;
	bool 			found;
	int			retval;
	Environment 		env;
	char			cluster_idp[30];

	nelems = elems.length();


	cl_mutex.lock();
	//
	// Walk through the list of clusters and remove the clusters
	// that have gone away.
	//
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		found = false;
		for (indx = 0; indx < nelems; indx++) {
			if (strcmp(elems[indx].key, clinfop->clusterp) == 0) {
				found = true;
				break;
			}
		}
		if (!found) {
			CCR_DBG(("CCR DS: Removing cluster %s from "
			    "cluster list\n", clinfop->clusterp));
			(void) cluster_list.erase(clinfop);
			//
			// Update the hash tables
			//
			clid_lock.wrlock();

			(void) clname_hashtab.remove((char *)clinfop->clusterp);
			(void) clid_hashtab.remove(clinfop->cluster_id);

			clid_lock.unlock();

			//
			// Remove the local context of the cz
			//
			os::sprintf(cluster_idp, "%d",
			    clinfop->cluster_id);
			if (!ns::unbind_cz_context(cluster_idp, LOCAL_NS)) {
				os::printf("data_server_impl::remove_cl_table"
				    " 1: Failed to delete local context"
				    " %s %d", (char *)clinfop->clusterp,
				    clinfop->cluster_id);
			}

			//
			// Removes all the tables in the cluster from
			// the filesystem.
			//
			retval = clinfop->remove_tables();
			if (retval != 0) {
				//
				// SCMSGS
				// @explanation
				// The error occurred while removing the CCR
				// tables for a removed cluster on this node.
				// The errno value indicates the nature of
				// the problem.
				// @user_action
				// There may be other related messages on
				// this node, which may help diagnose the
				// problem. The tables have to be manually
				// removed outside of CCR control. Contact
				// your authorized Sun service provider for
				// assistance.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: Could not remove the CCR "
				    "tables for cluster %s errno = %d.",
				    clinfop->clusterp, retval);
			}
			set_table_pathname(&orig_path, ".", clinfop->clusterp);
			//
			// It is ok, if we are not able to delete the
			// directory.
			//
			retval = os::rm_dir(orig_path);
			if (retval != 0) {
				//
				// SCMSGS
				// @explanation
				// The error occurred while removing the CCR
				// directory for a removed cluster on this
				// node. The errno value indicates the nature
				// of the problem.
				// @user_action
				// There may be other related messages on
				// this node, which may help diagnose the
				// problem. The directory has to be manually
				// removed outside of CCR control. Contact
				// your authorized Sun service provider for
				// assistance.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: Could not remove the CCR "
				    "directory for cluster %s errno = %d.",
				    clinfop->clusterp, retval);
			}
			delete clinfop;
			delete [] orig_path;
		}
	}
	cl_mutex.unlock();
}

//
// This routine is called when the cluster_directory table or the directory
// table of a cluster is being recovered. We cleanup stale entries or
// add new entries to the list of clusters or to the list of tables
// in a cluster.
//
void
data_server_impl::check_zone_cluster(const char *tname,
    ds_cluster_info *clinfop, const ccr::element_seq &elems, Environment &e)
{
	uint_t		num_elems;
	const char	*clnamep;
	char		*orig_path;
	int		retval;
	const char	*clidp;
	char		cl_id[65];
	uint_t		cluster_id;

	if (is_cluster_dir(tname)) {
		num_elems = elems.length();
		//
		// A new cluster may have got added or removed
		// at this point.
		//
		for (uint_t i = 0; i < num_elems; i++) {
			clnamep = (const char *)elems[i].key;
			clidp = (const char *)elems[i].data;
			cluster_id = (uint_t)os::atoi(clidp);
			CCR_DBG(("CCR DS: cluster = %s id = %s\n", clnamep,
			    clidp));

			//
			// Check if we already know about this
			// cluster.
			//
			if (is_default_cluster(clnamep)) {
				//
				// We already know about the
				// cluster global
				//
				continue;
			}
			ds_cluster_info *tmp_clinfop =
			    lookup_cluster_info(clnamep);

			if (!tmp_clinfop) {
				//
				// New cluster. Add the directory
				// table to the list of tables
				// for the cluster.
				//
				set_table_pathname(&orig_path, ".", clnamep);
				if ((retval = os::create_dir(orig_path)) != 0) {
					CCR_DBG(("CCR DS: Directory creation "
					    "failed with errno = %d\n",
					    retval));
					//
					// If the directory already exists in
					// the filesystem, it is probably
					// there due to failed transaction
					// to create a virtual cluster. It is
					// safe to ignore this error.
					//
					if (retval != EEXIST) {
						e.exception(
						    new ccr::system_error(
						    orb_conf::node_number(),
						    retval));
						delete [] orig_path;
						return;
					}
				}
				delete [] orig_path;
				tmp_clinfop = new ds_cluster_info(cluster_id,
				    clnamep, dir_path);
				ASSERT(tmp_clinfop);
				cl_mutex.lock();
				cluster_list.append(tmp_clinfop);
				//
				// Update the hash tables
				//
				char *cluster_namep = os::strdup(clnamep);
				ASSERT(cluster_namep);

				clid_lock.wrlock();
				clname_hashtab.add(cluster_id, cluster_namep);
				clid_hashtab.add(cluster_namep, cluster_id);
				clid_lock.unlock();
				//
				// Create the local context for the cz
				//
				if (!ns::bind_cz_context(clidp, LOCAL_NS,
					true, NS_ALLOW_ALL_OPP_PERM,
					cluster_id)) {
					ASSERT(0);
				}
				cl_mutex.unlock();
				CCR_DBG(("CCR DS: New cluster added %s\n",
				    clnamep));
			} else {
				CCR_DBG(("CCR DS: cluster = %s\n",
				    tmp_clinfop->clusterp));
				if (tmp_clinfop->directory_elems.seq != NULL) {
					CCR_DBG(("CCR DS: No tables in "
					    "cluster!\n"));
				}
			}
		}
		//
		// We added the clusters that we did not know about. Some we
		// knew about may have gone away. Need to cleanup those
		// clusters.
		//
		cleanup_cluster_info(elems);
		//
		// Here we refresh hash table with new table entries.We walk
		// through every cluster as new clusters may have been added.
		//
		refresh_cluster_info();
	} else {
		//
		// copy elems into in-core sequence
		//
		clinfop->cleanup_directory_info(elems);
		//
		// We dont have to worry about duplicate entries as we make
		// sure we flush out duplicates.
		//
		clinfop->directory_elems.add_elements(elems);
		//
		// Here we refresh hash table with new table entries.
		//
		clinfop->refresh_directory_info();
	}
}

//
// Replace the table in the cluster with a new sequence of elements.
//
void
data_server_impl::replace_table_zc(
	const char *cluster,
	const char *tname,
	const ccr::element_seq &elems,
	const ccr_data::consis &record,
	Environment &e)
{
	ds_cluster_info			*clinfop;
	char				metastr[CKSM_HEX_LEN + 1];
	char				*orig_path;
	char				*bak_path;
	char				*new_path;
	updatable_persistent_table	intent_table;
	table_access			*ta;
	int				retval;
	uint_t				num_elems;

	ASSERT(cluster);

	CCR_DBG(("CCR DS: in replace table. cluster = %s, name = %s "
	    "version = %d\n", cluster, tname, record.version));

	//
	// If table is invalid, ignore data entries after updating in-core
	// consis
	//
	if (!record.valid) {
		// leave table invalid so it can't be read
		return;
	}

	clinfop = lookup_cluster_info(cluster);
	ASSERT(clinfop);

	//
	// keep in-core consis info in sync with disk.
	//
	ta = clinfop->lookup_table(tname);
	ASSERT(ta);
	ta->rec = record;
	//
	// If the table is cluster_directory or a directory table of
	// a cluster, then we need to make changes to the list of
	// clusters or to the list of all the tables in the CCR.
	//
	if (is_cluster_dir(tname) || is_dir(tname)) {
		check_zone_cluster(tname, clinfop, elems, e);
		if (e.exception() != NULL) {
			return;
		}
	}

	if (is_cluster_dir(tname)) {
		set_table_pathname(&orig_path, ".", tname);
	} else {
		set_table_pathname(&orig_path, cluster, tname);
	}
	new_path = new char[os::strlen(orig_path) + 5];
	os::sprintf(new_path, "%s.new", orig_path);

	// open new temp file for writing
	if ((retval = intent_table.initialize(new_path)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		delete [] orig_path;
		delete [] new_path;
		ta->rele();
		return;
	}
	//
	// write meta entries
	//
	if (record.override) {
		//
		// get_consis_in sets the in-core version to -1 for tables
		// with override flag on. We need to write 0 to the persistent
		// data here. The in-core version will be updated to 0 in
		// recovery_done().
		//
		os::sprintf(metastr, "0");
	} else {
		os::sprintf(metastr, "%ld", record.version);
	}
	if ((retval = intent_table.insert_element("ccr_gennum", metastr))
			!= 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		(void) intent_table.close();
		delete [] orig_path;
		delete [] new_path;
		ta->rele();
		return;
	}
	CL_PANIC(os::strlen(record.checksum) == CKSM_HEX_LEN);
	(void) os::strcpy(metastr, record.checksum);
	if ((retval = intent_table.insert_element("ccr_checksum", metastr))
			!= 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		(void) intent_table.close();
		delete [] orig_path;
		delete [] new_path;
		ta->rele();
		return;
	}

	//
	// write elems into table file
	//
	num_elems = elems.length();
	for (uint_t i = 0; i < num_elems; i++) {
		if (is_dir(tname)) {
			retval = intent_table.insert_element(elems[i].key, "");
		} else {
			retval = intent_table.insert_element(elems[i].key,
			    elems[i].data);
		}
		if (retval != 0) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			(void) intent_table.close();
			delete [] orig_path;
			delete [] new_path;
			ta->rele();
			return;
		}
	}

	if ((retval = intent_table.close()) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		delete [] orig_path;
		delete [] new_path;
		ta->rele();
		return;
	}

	size_t len = strlen(orig_path) + strlen(BAKSUFFIX) + 1;
	bak_path = new char[len];
	(void) os::snprintf(bak_path, len, "%s%s", orig_path, BAKSUFFIX);
	retval = os::file_link(orig_path, bak_path);

	if ((retval != 0) && (retval != ENOENT)) {
		//
		// SCMSGS
		// @explanation
		// The indicated error occurred while backing up indicated CCR
		// table on this node.
		//
		// The errno value indicates the nature of the problem. errno
		// values are defined in the file /usr/include/sys/errno.h. An
		// errno value of 28(ENOSPC) indicates that the root file
		// system on the indicated node is full. Other values of errno
		// can be returned when the root disk has failed(EIO) or some
		// of the CCR tables have been deleted outside the control of
		// the cluster software(ENOENT).
		// @user_action
		// There may be other related messages on this node, which may
		// help diagnose the problem, for example: If the root file
		// system is full on the node, then free up some space by
		// removing unnecessary files. If the root disk on the
		// afflicted node has failed, then it needs to be replaced. If
		// the indicated CCR table was accidently deleted, then boot
		// this node in -x mode to restore the indicated CCR table
		// from other nodes in the cluster or backup. The CCR tables
		// are located at /etc/cluster/ccr/. Contact your authorized
		// Sun service provider for assistance.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Could not backup the CCR table %s errno = %d.",
		    orig_path, retval);
	}
	delete [] bak_path;

	// rename to original file
	if ((retval = os::file_rename(new_path, orig_path)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		delete [] orig_path;
		delete [] new_path;
		ta->rele();
		return;
	}
	delete [] orig_path;
	delete [] new_path;

	//
	// During table propagation, if a directory is propogated
	// along with the tables in the directory, but does not exist on
	// this data server, then we fetch the table consistency information
	// now as an earlier attempt would have failed since the table did
	// not exist.
	//
	ta->rec = clinfop->get_consis_in(tname, e);
	if (e.exception()) {
		CCR_DBG(("CCR DS: replace_table: could not read consis "
		    "info for %s table in cluster %s\n", tname, cluster));
		e.clear();
	}

	//
	// mark table as recovered, so clients can be notified
	// during recovery_done()
	//
	ta->flag_recovered(record);
	ta->rele();
}

void
data_server_impl::set_table_invalid(const char *tname, Environment &env)
{
	//
	// Get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	set_table_invalid_zc(clusterp, tname, env);
	delete [] clusterp;
}

//
// Set the table as invalid.
// Called by TM during node joining. If the joining node has a valid copy
// of the sepecified table while the current membership does not have a valid
// copy, the TM will set the table's rec on the joining node as invalid
// first.
//
void
data_server_impl::set_table_invalid_zc(const char *zc_namep, const char *tname,
    Environment &e)
{
	table_access *ta;
	ds_cluster_info *clinfop;

	CCR_DBG(("CCR DS: in set_table_invalid. cluster = %s name = %s\n",
	    zc_namep, tname));
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, e)) {
		e.exception(new ccr::invalid_zc_access());
	}
	clinfop = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		e.exception(new ccr::no_such_zc());
		return;
	}
	ta = clinfop->lookup_table(tname);
	ASSERT(ta);
	ta->rec.valid = false;
	ta->rele();
}

//
// Create a copy of the cluster list.
//
void
data_server_impl::create_cluster_list(SList<char> *local_cluster_listp)
{
	ds_cluster_info *clinfop;
	cl_mutex.lock();
	CL_PANIC(local_cluster_listp->empty());
	//
	// We shoudn't make a copy of each list element as it needs to be
	// shared.
	//
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		local_cluster_listp->append(clinfop->clusterp);
	}
	cl_mutex.unlock();
}

//
// recovery_done: invoked by TM on every DS to indicate the CCR is
// recovered and data can be read
//
void
data_server_impl::recovery_done(Environment &)
{
	naming::naming_context_var 	ctxp;
	CORBA::Object_var 		obj;
	quorum::quorum_algorithm_var 	quorum_obj;
	table_access 			*ta;
	table_access			*ta_cluster_dir;
	table_access			*ta_dir;
	ds_cluster_info			*global_clinfop;
	ds_cluster_info			*clinfop;
	SList<char>			*local_cluster_listp;
	uint_t 				num_tables;
	Environment			env;
	char				*clname;

	CCR_DBG(("CCR DS: in recovery done\n"));
	refresh_cluster_info();

	local_cluster_listp = new SList<char>;
	create_cluster_list(local_cluster_listp);
	//
	// Invoke did_recovery on registered clients
	//
	global_clinfop = lookup_cluster_info(DEFAULT_CLUSTER);
	ASSERT(global_clinfop);
	ta_cluster_dir = global_clinfop->lookup_table(CLUSTER_DIR);
	ASSERT(ta_cluster_dir);

	//
	// No need to protect the reading of the directory_elems since
	// the recovery_done is called at the end of recovery process in TM
	// when no other transactions are allowed to take place to make
	// changes to the directory_elems by adding or removing CCR tables.
	//
	while ((clname = local_cluster_listp->reapfirst()) != NULL) {
		if (is_default_cluster(clname)) {
			clinfop = global_clinfop;
		} else {
			clinfop = lookup_cluster_info(clname);
			ASSERT(clinfop);
		}
		if (clinfop->directory_elems.seq == NULL) {
			//
			// The cluster has no tables. continue with the next
			// cluster.
			//
			continue;
		}
		num_tables = clinfop->directory_elems.seq->length();
		for (uint_t indx  = 0; indx < num_tables; ++indx) {
			// get tacc for this table
			char *tname = (char *)(*clinfop->directory_elems.seq)
			    [indx].key;
			ASSERT(tname);
			ta = clinfop->lookup_table(tname);
			ASSERT(ta);

			if (ta->was_recovered()) {
				// Only valid table can be recovered
				CL_PANIC(ta->rec.valid);

				if (ta->rec.version == -1) {
					//
					// get_consis_in sets the in-core
					// version of a valid table with
					// override flag to -1. So here
					// if the table is recovered,
					// and its version is -1, it must
					// have override flag on.
					// In addition, we need to set the
					// in-core version to 0 here as
					// the recovery is sucessfully
					// done.
					//
					CL_PANIC(ta->rec.override);
					ta->rec.version = 0;
				}
				ta->rec.override = false;

				// invoke callback on every client of this table
				ta->call_did_recovery(clinfop->clusterp, tname);

				//
				// Invoke callback on any client registered for
				// the CCR repository for a cluster, which gets
				// implemented as attached to the directory.
				// If the table is a directory table, invoke
				// callback on the clients registered for the
				// CCR repository as a whole.
				//
				if (!is_dir(tname)) {
					ta_dir = clinfop->lookup_table(DIR);
					ASSERT(ta_dir);
					ta_dir->call_did_recovery(
					    clinfop->clusterp, tname);
					ta_dir->rele();
				} else {
					ta_cluster_dir->call_did_recovery(
					    clinfop->clusterp, tname);
				}
			} else if (ta->rec.version == -1) {
				ta->rec.valid = false;
				ta->rec.override = false;
			}
			ta->rele();
		}
	}
	delete local_cluster_listp;

	//
	// Perform any callbacks registered for the CCR repository as
	// a whole, which get implemented as attached to the directory.
	// If the directory had to be recovered, invoke callback.
	//
	if (ta_cluster_dir->was_recovered()) {
		ta_cluster_dir->call_did_recovery(DEFAULT_CLUSTER,
		    CLUSTER_DIR);
	}

	ta_cluster_dir->rele();

	//
	// notify quorum object that ccr is recovered
	//
	CCR_DBG(("CCR DS: calling ccr_refreshed\n"));
	quorum_obj = cmm_ns::get_quorum_algorithm(NULL);
	quorum_obj->ccr_refreshed(env);
	env.clear();

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// Notify clconf that CCR is recoverd.
	// clconf needs this callback to determine if one or more
	// zone clusters were added/removed while this node was down.
	// As part of this call, clconf updates its data structures
	// synchronously, if required.
	// This must happen before we notify membership that CCR is refreshed
	// because membership subsystem reads zone cluster data from clconf.
	//
	CCR_DBG(("CCR DS: telling clconf that ccr is refreshed\n"));
	clconf_ccr::the().ccr_is_refreshed_callback_for_clconf();

	//
	// Notify membership subsystem that CCR is recovered.
	//
	CCR_DBG(("CCR DS: telling membership that ccr is refreshed\n"));
	ccr_is_ready_callback_for_membership();
#endif

	// Set the DS recovery_state
	set_recovery_state(CCR_RECOVERED, env);

	env.clear();
}

//
// Set the recovery_state, called by CCR TM during DS registration.
// It will tell the waiting DS either it is recovered or it has
// system_error, should die.
//
void
data_server_impl::set_recovery_state(recovery_state_t new_state, Environment &)
{
	recovery_mutex.lock();
	recovery_state = new_state;
	// Wake up DS that is waiting at register_with_tm
	recovery_cv.signal();
	recovery_mutex.unlock();
}

//
// Called by CCR TM during DS registration. When this node joins after
// do_recovery is done, and this node carries a valid copy of a certain
// table with override flag set, which is recovered as invalid in the current
// cluster membership, the CCR TM will call this function to add that table
// in to the update_table_list maintained by this node. As soon as this node is
// done with DS registration, it will go through the list to update the listed
// tables with its own copies.
//
void
data_server_impl::add_update_table(const char *tname, Environment &env)
{
	//
	// Get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	add_update_table_zc(clusterp, tname, env);
	delete [] clusterp;
}

void
data_server_impl::add_update_table_zc(const char *zc_namep, const char *tname,
    Environment &env)
{
	ds_cluster_info	*clinfop;

	CCR_DBG(("CCR DS: in add_update_table.\n"));

	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, env)) {
		env.exception(new ccr::invalid_zc_access());
	}
	clinfop = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		env.exception(new ccr::no_such_zc());
		return;
	}
	//
	// Check recovery_state to make sure the DS has not yet been recovered.
	// At this point, there can be no other writers, the lock used here
	// is just for completeness.
	//
	recovery_mutex.lock();
	CL_PANIC(recovery_state == CCR_UNRECOVERED);
	recovery_mutex.unlock();

	clinfop->add_update_table(tname);
}

//
// Called by TM during propagate_table. If this DS is chosen as the truth
// DS to propagate data from and it encounters system_error during
// propagation, TM will call this method to kill this offending DS.
//
void
data_server_impl::die(Environment &)
{
#if defined(FAULT_INJECTION)
	// If the error was caused by fault injection, just reboot
	if (fault_triggered(FAULTNUM_CCR_SYSTEM_ERROR_NODE, NULL, NULL)) {
		fault_ccr::reboot_node(FAULTNUM_CCR_SYSTEM_ERROR_NODE, NULL);
	}
#endif // FAULT_INJECTION

	//
	// SCMSGS
	// @explanation
	// Some fatal error occured on this node during the synchronization of
	// cluster repository. This node will be killed to allow the
	// synchronization to continue.
	// @user_action
	// Look for other messages on this node that indicated the fatal error
	// occured on this node. For example, if the root disk on the
	// afflicted node has failed, then it needs to be replaced. Contact
	// your authorized Sun service provider for assistance.
	//
	(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE, "CCR: Fatal error: "
	    "Node will be killed.\n");
}

//
// Read all the elements in a table in a cluster.
//
ccr::element_seq *
data_server_impl::get_table_data_in(const char *cluster, const char *tname,
    Environment &e)
{
	char *tab_pathname;
	uint_t num_elems, cur_max;
	int retval;
	ccr::element_seq *tels;
	readonly_persistent_table ptab;
	table_element_t *next_el;

	if (is_cluster_dir(tname)) {
		ASSERT(is_default_cluster(cluster));
		set_table_pathname(&tab_pathname, ".", tname);
	} else {
		set_table_pathname(&tab_pathname, cluster, tname);
	}

	retval = ptab.initialize(tab_pathname);
	delete [] tab_pathname;
	if (retval) {
		//
		// There can be an error because there was a new virtual
		// cluster added and the new tables have not been recovered.
		// We shoudn't return with an exception for this case.
		//
		if ((retval != ENOENT) || is_default_cluster(cluster)) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
		}
		return (NULL);
	}

	// create sequence of elements
	tels = new ccr::element_seq(max_elems_expected, max_elems_expected);
	ASSERT(tels);
	cur_max = max_elems_expected;

	// read elements from table file
	num_elems = 0;
	while ((next_el = ptab.next_element(retval)) != NULL) {
		// skip meta entries
		if (is_meta_row(next_el->key)) {
			delete next_el;
			continue;
		}

		if (num_elems >= cur_max) {
			cur_max *= 2;
			tels->length(cur_max);
		}
		ccr::table_element &this_elem = (*tels)[num_elems];
		this_elem.key = new char[os::strlen(next_el->key) + 1];
		ASSERT((char *)this_elem.key);
		(void) os::strcpy(this_elem.key, next_el->key);
		this_elem.data = new char[os::strlen(next_el->data) + 1];
		ASSERT((char *)this_elem.data);
		(void) os::strcpy(this_elem.data, next_el->data);
		num_elems++;
		delete next_el;
	}

	// check for premature exit from loop
	if (retval) {
		delete tels;
		ptab.close();
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		return (NULL);
	}

	//
	// If the table is a directory and there are no directory entries,
	// we return NULL here.
	//
	if (is_dir(tname) && (num_elems == 0)) {
		delete tels;
		tels = NULL;
		if (is_default_cluster(cluster)) {
			//
			// We cant have no tables in the global cluster.
			//
			CCR_DBG(("CCR DS: No meta data in the global CCR\n"));
			e.exception(new ccr::database_invalid());
		}
	} else {
		// resize sequence to number read
		tels->length(num_elems);
	}
	ptab.close();
	return (tels);
}

//
// refresh_cluster_info:
// This traverses the list of all the tables in the CCR and
// adds the table access object to the hashtable of a cluster if the
// object does not exist in the hashtable for any table in the
// list for a cluster.
//
void
data_server_impl::refresh_cluster_info()
{
	ds_cluster_info	*clinfop;
	//
	// Fill hashtable with table_access entries
	//
	cl_mutex.lock();

	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		clinfop->refresh_directory_info();
	}
	cl_mutex.unlock();
}

//
// This function does a lookup of the table name for the given cluster
// name. It then checks for the validity of the table. If the table
// is valid, then returns a readonly_table_ptr object for the caller
// to do read operations on the table.
//
ccr::readonly_table_ptr
data_server_impl::lookup_zc(const char *zc_namep, const char *tname,
    Environment &e)
{
	ds_cluster_info		*clinfop;
	ccr::readonly_table_ptr rd_p = ccr::readonly_table::_nil();

	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, e)) {
		e.exception(new ccr::invalid_zc_access());
		return (ccr::readonly_table::_nil());
	}
	clinfop  = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		e.exception(new ccr::no_such_zc());
		return (ccr::readonly_table::_nil());
	}
	rd_p = lookup_table(tname, clinfop, e);
	//
	// Return the reference to the transaction object.
	//
	return (rd_p);
}

//
// Utility function that does the lookup of the table name. It takes
// the cluster information as an argument, it doesn not have to
// walk through the cluster list here.
//
ccr::readonly_table_ptr
data_server_impl::lookup_table(const char *tname, ds_cluster_info *clinfop,
    Environment &e) const
{
	int			retval;
	char			*tab_pathname	= NULL;
	readonly_copy_impl	*tcopy		= NULL;
	table_access		*ta;

	if (!is_dir(tname)) {
		// Check for the validity of the directory table first
		ta = clinfop->lookup_table(DIR);
		if (ta == NULL) {
			//
			// SCMSGS
			// @explanation
			// The CCR is unable to locate its metadata.
			// @user_action
			// Boot the offending node in -x mode to restore the
			// indicated table from backup or other nodes in the
			// cluster. The CCR tables are located at
			// /etc/cluster/ccr/. Contact your authorized Sun
			// service provider for assistance.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: CCR metadata not found.");
			e.exception(new ccr::no_such_table(DIR));
			return (ccr::readonly_table::_nil());
		}

		// check table is marked valid
		if (!ta->rec.valid) {
			e.exception(new ccr::table_invalid(DIR));
			ta->rele();
			return (ccr::readonly_table::_nil());
		}
		ta->rele();
	}

	// lookup table in tacc_hashtab
	ta = clinfop->lookup_table(tname);
	if (ta == NULL) {
		CCR_DBG(("CCR DS: No such table %s\n", tname));
		e.exception(new ccr::no_such_table(tname));
		return (ccr::readonly_table::_nil());
	}

	// check table is marked valid
	if (!ta->rec.valid) {
		CCR_DBG(("CCR DS: Invalid table %s\n", tname));
		e.exception(new ccr::table_invalid(tname));
		ta->rele();
		return (ccr::readonly_table::_nil());
	}

	clinfop->set_table_pathname(&tab_pathname, tname);
	tcopy = new readonly_copy_impl;
	ASSERT(tcopy);
	// initialize() will record the table_access value.
	retval = tcopy->initialize(tab_pathname, ta);
	delete [] tab_pathname;
	if (retval) {
		// destructor will release the table_access
		delete tcopy;
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		return (ccr::readonly_table::_nil());
	}

	// verify checksum of table file
	if ((retval = tcopy->verify_checksum()) != NULL) {
		// destructor will release the table_access.
		delete tcopy;
		e.exception(new ccr::table_invalid(tname));
		return (ccr::readonly_table::_nil());
	}

	// return reference to transaction object
	return (tcopy->get_objref());
}

//
// Lookup operation on a table in the default cluster.
//
ccr::readonly_table_ptr
data_server_impl::lookup(const char *tname, Environment &env) {
	ccr::readonly_table_ptr table_p;
	//
	// get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	table_p = lookup_zc(clusterp, tname, env);
	delete [] clusterp;
	return (table_p);
}

//
// Get the list of all the tables in a given cluster.
//
void
data_server_impl::get_table_names_zc(const char *zc_namep,
    CORBA::StringSeq_out tnames, Environment &env)
{
	ds_cluster_info	*clinfop;

	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, env)) {
		env.exception(new ccr::invalid_zc_access());
	}
	clinfop = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		env.exception(new ccr::no_such_zc());
		return;
	}
	clinfop->get_table_names(tnames);
}

//
// Get the list of all the tables in the cluster from where the request
// originated.
//
void
data_server_impl::get_table_names(CORBA::StringSeq_out tnames,
    Environment &env)
{
	//
	// Get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	get_table_names_zc(clusterp, tnames, env);
	delete [] clusterp;
}

//
// This routine retrieves a list of tables in a given cluster
// that are marked invalid.
//
void
data_server_impl::get_invalid_table_names_zc(const char *zc_namep,
    CORBA::StringSeq_out tnames, Environment &env)
{
	ds_cluster_info	*clinfop;
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, env)) {
		env.exception(new ccr::invalid_zc_access());
		return;
	}
	clinfop = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		env.exception(new ccr::no_such_zc());
		return;
	}
	clinfop->get_invalid_table_names(tnames);
}

//
// Retrieve a list of tables that are marked invalid in the default
// global cluster.
//
void
data_server_impl::get_invalid_table_names(CORBA::StringSeq_out tnames,
    Environment &env)
{
	//
	// Get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	get_invalid_table_names_zc(clusterp, tnames, env);
	delete [] clusterp;
}

//
// Initiate a transaction for a table in a given cluster.
//
ccr_data::updatable_table_copy_ptr
data_server_impl::begin_transaction_zc(const char *zc_namep, const char *tname,
    bool invalid_ok, Environment &e)
{
	updatable_copy_impl	*tcopy;
	table_access		*ta;
	ds_cluster_info		*clinfop;
	int			retval;

	CCR_DBG(("CCR DS: In ds::begin_transaction\n"));

	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(zc_namep, e)) {
		e.exception(new ccr::invalid_zc_access());
		return (ccr_data::updatable_table_copy::_nil());
	}
	// Check for orphaned.
	if (e.is_orphan()) {
		return (ccr_data::updatable_table_copy::_nil());
	}
	clinfop = lookup_cluster_info(zc_namep);
	if (clinfop == NULL) {
		e.exception(new ccr::no_such_zc());
		return (ccr_data::updatable_table_copy::_nil());
	}

	// lookup table in tacc_hashtab of the cluster.
	ta = clinfop->lookup_table(tname);
	if (ta == NULL) {
		e.exception(new ccr::no_such_table(tname));
		return (ccr_data::updatable_table_copy::_nil());
	}
	ta->hold();

	// create updatable table and return
	tcopy = new updatable_copy_impl;
	ASSERT(tcopy);
	if ((retval = tcopy->initialize(clinfop->dir_path, zc_namep, tname,
	    ta, this, invalid_ok)) != NULL) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		// this will do rele () on the ta
		delete tcopy;
		return (ccr_data::updatable_table_copy::_nil());
	}

	// return reference to object
	return (tcopy->get_objref());
}

//
// Initiate a transaction for a table in the cluster from where the
// request originated.
//
ccr_data::updatable_table_copy_ptr
data_server_impl::begin_transaction(const char *tname,
    bool invalid_ok, Environment &env)
{
	ccr_data::updatable_table_copy_ptr update_p;
	//
	// Get the cluster from the environment.
	//
	char *cluster = get_cluster_from_env(env);
	update_p = begin_transaction_zc(cluster, tname, invalid_ok, env);
	delete [] cluster;
	return (update_p);
}

//
// zc_getidbyname
// 	The first argument is potentially the name of a zone cluster.
//	The second argument is the zone cluster id that is returned
//	by the function.
// Return value
//	When the first argument is the name of a zone cluster,
//	other than 'global', the function :
//	Assigns the zone cluster ID of that zone cluster in the second argument
// 	of the function that is returned back to the caller.
//	Returns success (0).
//	Otherwise, the function returns -1.
//
int32_t
data_server_impl::zc_getidbyname(const char *zc_namep, uint32_t &zc_id,
    Environment &)
{
	uint32_t clid = 0;
	clid_lock.rdlock();
	clid = clname_hashtab.lookup(zc_namep);
	clid_lock.unlock();
	if (clid == 0) {
		return (-1);
	}
	zc_id = clid;
	return (0);
}

//
// Return the next available cluster id that is greater than
// the cluster id's of all the configured zone clusters.
//
uint32_t
data_server_impl::zc_get_maxid(Environment &)
{
#if SOL_VERSION >= __s10
	uint32_t	clid;
	uint32_t	clid_max = 1;
	ds_cluster_info	*clinfop;
	cl_mutex.lock();
	clid_lock.rdlock();
	for (cluster_list.atfirst();
	    (clinfop = cluster_list.get_current()) != NULL;
	    cluster_list.advance()) {
		clid = clname_hashtab.lookup(clinfop->clusterp);
		if (clid > clid_max) {
			clid_max = clid;
		}
	}
	clid_lock.unlock();
	if (clid_max < MIN_CLUSTER_ID) {
		clid_max = MIN_CLUSTER_ID;
	} else {
		clid_max++;
	}
	cl_mutex.unlock();
	return (clid_max);
#else
	ASSERT(0);
	return (0);
#endif // SOL_VERSION >= __s10
}

//
// Return the cluster name given a cluster id. If no cluster with the id,
// exists, a null value is returned.
//
char *
data_server_impl::zc_getnamebyid(uint32_t cluster_id, Environment &)
{
#if SOL_VERSION >= __s10
	char *clnamep = NULL;
	char *tmpclnamep = NULL;
	if ((cluster_id == BASE_CLUSTER_ID) ||
	    (cluster_id == BASE_NONGLOBAL_ID)) {
		clnamep = os::strdup(DEFAULT_CLUSTER);
	} else {
		clid_lock.rdlock();
		tmpclnamep = clid_hashtab.lookup(cluster_id);
		clid_lock.unlock();
		if (tmpclnamep != NULL) {
			clnamep = os::strdup(tmpclnamep);
			ASSERT(clnamep);
		}
	}
	return (clnamep);
#else
	ASSERT(0);
	return (NULL);
#endif // SOL_VERSION >= __s10
}

//
// Create a table in a cluster. Here we add the in-core information
// of the table. If the table being added is a directory, then we know that
// cluster is being added. The incore cluster information is also added
// in this case.
//
void
data_server_impl::create_table_zc(uint_t cluster_id, const char *cluster,
    const char *tname, Environment &env)
{
	ds_cluster_info	*clinfop;
	char cluster_idp[30];

	ASSERT(cluster);
	os::sprintf(cluster_idp, "%d", cluster_id);

	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, env)) {
		env.exception(new ccr::invalid_zc_access());
	}
	//
	// If a new directory is being added, then a new cluster is being
	// created. Add the cluster to the incore list of clusters.
	//
	if (is_dir(tname)) {
		char *cluster_namep = os::strdup(cluster);
		ASSERT(cluster_namep);
		clinfop = new ds_cluster_info(cluster_id, cluster, dir_path);
		ASSERT(clinfop);
		clinfop->add_incore_table(tname);
		cl_mutex.lock();
		cluster_list.append(clinfop);
		//
		// Update the hash tables
		//
		clid_lock.wrlock();
		clname_hashtab.add(cluster_id, cluster_namep);
		clid_hashtab.add(cluster_namep, cluster_id);
		clid_lock.unlock();
		//
		// Create a local context for the cz
		//
		if (!ns::bind_cz_context(cluster_idp, LOCAL_NS,
		    true, NS_ALLOW_ALL_OPP_PERM, cluster_id)) {
			ASSERT(0);
		}

		cl_mutex.unlock();
	} else {
		clinfop = lookup_cluster_info(cluster);
		if (clinfop == NULL) {
			env.exception(new ccr::no_such_zc());
			return;
		}
		clinfop->add_incore_table(tname);
	}
}

//
// Create a table in the cluster from where the request originated.
//
void
data_server_impl::create_table(const char *tname, Environment &env)
{
	//
	// Get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	//
	// We do not have to get the cluster id for the cluster as
	// it will not be used in this case. It is used only when
	// the cluster is being created which is not the case here.
	// It is ok to pass the cluster id of the physical cluster
	// here.
	//
	create_table_zc(0, clusterp, tname, env);
	delete [] clusterp;
}

//
// Remove a table from the given cluster.
// If the table being removed is a directory, then we know the cluster
// is being removed. The incore cluster information is also purged
// in this case.
//
void
data_server_impl::remove_table_zc(const char *cluster, const char *tname,
    Environment &env)
{
	char	cluster_idp[30];

	ds_cluster_info	*clinfop;
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, env)) {
		env.exception(new ccr::invalid_zc_access());
	}
	clinfop = lookup_cluster_info(cluster);
	if (clinfop == NULL) {
		env.exception(new ccr::no_such_zc());
		return;
	}
	ASSERT(clinfop);

	//
	// If a directory is being removed, then a cluster was
	// removed. Remove the cluster from the incore list of clusters.
	//
	if (is_dir(tname)) {
		// Cant remove global cluster
		ASSERT(!is_default_cluster(cluster));
		cl_mutex.lock();
		for (cluster_list.atfirst();
		    (clinfop = cluster_list.get_current()) != NULL;
		    cluster_list.advance()) {
			if (os::strcmp(clinfop->clusterp, cluster) == 0) {
				CCR_DBG(("CCR DS: Removing cluster %s from "
				    "cluster list\n", clinfop->clusterp));
				(void) cluster_list.erase(clinfop);
				//
				// Update the hash tables
				//
				clid_lock.wrlock();
				(void) clname_hashtab.remove(
				    (char *)clinfop->clusterp);
				(void) clid_hashtab.remove(clinfop->cluster_id);
				clid_lock.unlock();

				//
				// Remove the local context of the cz
				//
				os::sprintf(cluster_idp, "%d",
				    clinfop->cluster_id);
				if (!ns::unbind_cz_context(cluster_idp,
				    LOCAL_NS)) {
					os::printf(
					    "data_server_impl::remove_cl_table "
					    "2: Failed to delete local context "
					    "%s %d",
					    (char *)clinfop->clusterp,
					    clinfop->cluster_id);
				}

				delete clinfop;
				break;
			}
		}
		cl_mutex.unlock();
	} else {
		clinfop->remove_incore_table(tname);
	}
}

//
// Remove a table from the cluster from where the request originated.
//
void
data_server_impl::remove_table(const char *tname, Environment &env)
{
	//
	// Get the cluster from the environment.
	//
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	remove_table_zc(clusterp, tname, env);
	delete [] clusterp;
}

//
// Reads in epoch number from the file supplied.
//
ccr_data::epoch_type
data_server_impl::get_epoch_in(const char *epochfile, Environment &e)
{
	char 				*epoch_pathname = NULL;
	table_element_t			*telp		= NULL;
	readonly_persistent_table	epoch_tab;
	int 				retval;

	// open the epoch table supplied.
	set_table_pathname(&epoch_pathname, DEFAULT_CLUSTER, epochfile);
	retval = epoch_tab.initialize(epoch_pathname);
	delete [] epoch_pathname;

	if (retval) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		return (EINVAL);
	}

	// read epoch (should be only entry)
	telp = epoch_tab.next_element(retval);
	if (telp == NULL) {
		e.exception(new ccr::table_invalid("epoch"));
		epoch_tab.close();
		return (EINVAL);
	}

	epoch_tab.close();
	if (strcmp(telp->key, EPOCHSTR) != 0) {
		e.exception(new ccr::table_invalid("epoch"));
		delete telp;
		return (EINVAL);
	}
	epoch_num = os::atoi(telp->data);

	delete telp; // no use of telp anymore

	return (epoch_num);
}

//
// Builds and returns the absolute path of the CCR table.
//
void
data_server_impl::set_table_pathname(char **path, const char *zpath,
    const char *tname)
{
	size_t len = os::strlen(dir_path) + os::strlen(zpath) +
	    os::strlen(tname) + 3;
	*path = new char[len];
	CL_PANIC(*path);
	os::sprintf(*path, "%s/%s/%s", dir_path, zpath, tname);
}
