//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

//
// implementation of fault_ccr class. Used by ccr_fi tests.
//

#pragma ident	"@(#)fault_ccr.cc	1.4	08/05/20 SMI"

#include <sys/os.h>
#include <ccr/fault_ccr.h>
#include <orb/fault/fault_injection.h>
#include <orb/fault/fault_functions.h>
#include <orb/flow/resource_balancer.h>
#include <sys/node_order.h>
#include <sys/vnode.h>
#include <sys/stat.h>

//
// Generic test function. Calls another test function based on the
// operation (enum generic_op_t).
//
void
fault_ccr::generic_test_op(uint_t fault_num, void *argp, uint32_t argsize)
{
	ASSERT(argp != NULL);
	ASSERT(argsize == sizeof (ccr_arg_t));

	ccr_arg_t	*ccr_argp = (ccr_arg_t *)argp;

	switch (ccr_argp->op) {
	case REBOOT_NODE:
		reboot_node(fault_num, ccr_argp);
		break;
	case WAIT_FOR_REJOIN:
		wait_for_rejoin(ccr_argp);
		break;
	case SET_SYSTEM_ERROR:
		set_system_error(fault_num, ccr_argp);
		break;
	case RESTORE_TABLE:
	default:
		ASSERT(0);
	}
}

//
// set_system_error
// Sets the trigger for the system error fault point.
// 1 is interpreted as the current node.
// 2 is interpreted as a signal to clear the trigger.
// all other values cause all nodes to set the trigger.
//
void fault_ccr::set_system_error(uint_t fnum, ccr_arg_t *argp) {

	//
	// Bug 4657885 : Need to make a copy of the trigger before clearing it,
	// so that when we set the NodeTrigger later, we can use the same data.
	//
	fault_ccr::ccr_arg_t	arg_t;
	arg_t.op = argp->op;
	arg_t.nodenum = argp->nodenum;

	NodeTriggers::clear(fnum, TRIGGER_ALL_NODES);
	if (arg_t.nodenum == (nodeid_t)1) {
		os::printf("Setting system error for this node\n");
		NodeTriggers::add(FAULTNUM_CCR_SYSTEM_ERROR_NODE, &arg_t,
		    (unsigned int)sizeof (arg_t), (int)TRIGGER_THIS_NODE);
	} else if (arg_t.nodenum == (nodeid_t)2) {
		os::printf("Clearing system error for this node\n");
		NodeTriggers::clear(FAULTNUM_CCR_SYSTEM_ERROR_CLUSTER);
	} else {
		os::printf("Setting system error for all nodes\n");
		NodeTriggers::add(FAULTNUM_CCR_SYSTEM_ERROR_CLUSTER, &arg_t,
		    (unsigned int)sizeof (arg_t), TRIGGER_ALL_NODES);
	}

}

//
// reboot_node
// This is called when a fault point has been triggered to reboot
// a node.
//
void fault_ccr::reboot_node(uint_t fnum, ccr_arg_t *argp) {

	ccr_arg_t new_argp;

	if (!argp) {
		// Just reboot the current node
		ccr_arg_t	arg;
		arg.nodenum = orb_conf::local_nodeid();
		new_argp = arg;
		argp = &arg; //lint !e733
	}

	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_UNKNOWN;

	nodeid_t	local_node = orb_conf::local_nodeid();
	if (argp->nodenum != (nodeid_t)0) {
		wait_args.nodeid = argp->nodenum;
	} else {
		node_order	no;
		for (no.lowest(); no.valid(); no.higher()) {
			if (no.current() != local_node) {
				wait_args.nodeid = no.current();
				break;
			}
		}
	}

	os::printf("Setting reboot trigger for node %d, current node is %d\n",
	    wait_args.nodeid, local_node);

	//
	// First setup information to wait for rejoin if we later
	// decide to. Then, set the fault point to access it.
	//
	new_argp.nodenum = wait_args.nodeid;
	new_argp.op = WAIT_FOR_REJOIN;
	NodeTriggers::add(FAULTNUM_CCR_WAIT_FOR_REJOIN, &new_argp,
	    (unsigned int)sizeof (fault_ccr::ccr_arg_t), TRIGGER_ALL_NODES);

	NodeTriggers::clear(fnum, TRIGGER_ALL_NODES);

	FaultFunctions::reboot_once(fnum, &wait_args,
	    (unsigned int)sizeof (wait_args));
}

//
// wait_for_rejoin
// This is called from test code to cause a node which injected another
// node's reboot to wait until that node reboots before continuing.
//
void fault_ccr::wait_for_rejoin(ccr_arg_t *argp)
{
	FaultFunctions::wait_for_arg_t	wait_args;
	wait_args.op = FaultFunctions::WAIT_FOR_REJOIN;
	wait_args.nodeid = argp->nodenum;

	FaultFunctions::do_wait(&wait_args);
}
