//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// manager_impl.cc - CCR transaction manager
//

#pragma ident	"@(#)manager_impl.cc	1.71	08/11/24 SMI"

#include <sys/os.h>
#include <h/naming.h>
#include <cmm/cmm_ns.h>
#include <ccr/manager_impl.h>
#include <ccr/transaction_impl.h>
#include <orb/fault/fault_injection.h>
#include <orb/fault/fault_functions.h>
#include <ccr/fault_ccr.h>

// protos
extern ccr_repl_prov *the_service;

// Global
threadpool *the_ccr_threadpool;
const int ccr_threadpool_size = 2;

// constructor for primary
manager_impl::manager_impl(ccr_repl_prov *serverp):
	mc_replica_of<ccr_trans::manager>(serverp),
	we_are_primary(true)
{
	repl_svr = serverp;
	common_constructor();
}

// constructor for secondary
manager_impl::manager_impl(ccr_trans::manager_ptr objp):
	mc_replica_of<ccr_trans::manager>(objp),
	we_are_primary(false)
{
	repl_svr = NULL;
	common_constructor();
}

void
manager_impl::common_constructor()
{
	the_ccr_threadpool = new threadpool(true, 0, "CCR threadpool");
	if (the_ccr_threadpool == NULL) {
		CL_PANIC(!"Unable to allocate CCR threadpool");
	}
	if (the_ccr_threadpool->change_num_threads(ccr_threadpool_size)
	    == 0) {
		delete the_ccr_threadpool;
		CL_PANIC(!"Failed to allocate threads");
	}
	we_have_quorum = false;
	recovery_done = false;
	we_are_frozen = false;
	freeze_called = false;
	need_quorum_recheck = false;
	highest_epoch = -1;
	quorum_obj = NULL;
	CCR_DBG(("CCR TM: Manager obj = %p\n", this));
	reg_info = new regis_info(this);
}

// destructor
manager_impl::~manager_impl()
{
	CCR_DBG(("CCR TM: In manager_impl destructor\n"));
	delete reg_info;
}

void
manager_impl::_unreferenced(unref_t cookie)
{
	CCR_DBG(("CCR TM: In manager_impl _unreferenced\n"));
	//
	// No need to protect _last_unref because the manager_impl
	// persists throughout the life of a cluster node.
	//
	if (_last_unref(cookie))
		delete this;
}

void
manager_impl::set_repl_prov(ccr_repl_prov *svr)
{
	repl_svr = svr;
}

ccr::updatable_table_ptr
manager_impl::begin_transaction_invalid_ok(const char *table_name,
				Environment &env)
{
	ccr::updatable_table_ptr table_p;
	const char *clusterp = get_cluster_from_env(env);
	table_p = begin_transaction_common(clusterp, table_name, true, env);
	delete [] clusterp;
	return (table_p);
}

//
// This is same as begin_transaction() except that this would succeed
// even if the table is invalid.
//
ccr::updatable_table_ptr
manager_impl::begin_transaction_invalid_ok_zc(const char *cluster,
				const char *table_name,
				Environment &env)
{
	return (begin_transaction_common(cluster, table_name, true, env));
}

ccr::updatable_table_ptr
manager_impl::begin_transaction(const char *table_name,
				Environment &e)
{
	ccr::updatable_table_ptr upd_p;
	const char *cluster = get_cluster_from_env(e);
	upd_p = begin_transaction_common(cluster, table_name, false, e);
	delete [] cluster;
	return (upd_p);
}

//
// this is a retryable HA IDL method
//
ccr::updatable_table_ptr
manager_impl::begin_transaction_zc(const char *cluster,
				const char *table_name,
				Environment &e)
{
	return (begin_transaction_common(cluster, table_name, false, e));
}

//
// Common routine for begin_transaction and begin_transaction_invalid_ok
//
ccr::updatable_table_ptr
manager_impl::begin_transaction_common(const char *cluster,
				const char *table_name,
				bool invalid_ok,
				Environment &e)
{
	ccr::updatable_table_ptr trans_ref;
	table_trans_impl	*transp;
	table_info		*ti;
	Environment		env;
	CORBA::Exception	*ex;
	mgr_cluster_info	*clinfop;

	//
	// Check if recovery is complete.
	//
	if (!recovery_done) {
		// client should retry till recovery is complete.
		e.exception(new ccr_trans::comm_error(true));
		return (ccr::updatable_table::_nil());
	}

	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, e)) {
		e.exception(new ccr::invalid_zc_access());
		return (ccr::updatable_table::_nil());
	}

	clinfop = lookup_cluster_info(cluster);

	//
	// Check if cluster exists
	//
	if (clinfop == NULL) {
		e.exception(new ccr::no_such_zc(cluster));
		return (ccr::updatable_table::_nil());
	}

	//
	// Check for saved state (retry)
	//
	primary_ctx *ctxp = primary_ctx::extract_from(e);
	simple_state *saved_state = (simple_state *)ctxp->get_saved_state();
	if (saved_state) {
		// It is a retry
		ti = clinfop->lookup_table(table_name);
		ASSERT(ti);
		ASSERT(ti->running_trans);
		trans_ref = ti->running_trans->get_objref();
		ti->rele();
		return (trans_ref);
	}


	//
	// Quorum may need a recheck
	//
	if (need_quorum_recheck) {
		(void) check_quorum();
	}

	//
	// check if we have quorum
	//
	if (!we_have_quorum) {
		// client should retry till quorum is reached
		e.exception(new ccr_trans::comm_error(true));
		return (ccr::updatable_table::_nil());
	}

	//
	// check if table exists
	//
	ti = clinfop->lookup_table(table_name);
	if (ti == NULL) {
		e.exception(new ccr::no_such_table(table_name));
		return (ccr::updatable_table::_nil());
	}

	//
	// check if table is valid
	//
	if ((!invalid_ok) && (!ti->rec.valid)) {
		e.exception(new ccr::table_invalid(table_name));
		ti->rele();
		return (ccr::updatable_table::_nil());
	}

	//
	// flush out registrations and transactions on same table
	// if primary gets frozen return retry exception
	// (client handles retries)
	//

	// lock table to prevent concurrent updates to it.
	// it is unlocked in table_trans_impl::cleanup()
	if (!ti->lock_writes()) {
		// abort since primary is frozen
		e.exception(new ccr_trans::comm_error(true));
		ti->rele();
		return (ccr::updatable_table::_nil());
	}
	// Need to release the reference to ti from the lookup_
	// table before the above locking, in order to recheck
	// the existence of this table.
	ti->rele();

	//
	// Recheck for the existence of the table in question,
	// which may have been removed by an ongoing
	// transaction that this begin_transaction() has been
	// waiting to complete in the above if clause.
	//
	clinfop = lookup_cluster_info(cluster);

	//
	// Check if cluster exists
	//
	if (clinfop == NULL) {
		e.exception(new ccr::no_such_zc(cluster));
		return (ccr::updatable_table::_nil());
	}
	ti = clinfop->lookup_table(table_name);
	//
	// check if the table exists
	//
	if (ti == NULL) {
		e.exception(new ccr::no_such_table(table_name));
		return (ccr::updatable_table::_nil());
	}

	// block registration of data servers til this is done
	if (!reg_info->begin_transaction()) {
		// abort since primary is frozen
		e.exception(new ccr_trans::comm_error(true));
		ti->unlock_writes(false);
		ti->rele();
		return (ccr::updatable_table::_nil());
	}

	CCR_DBG(("CCR TM %p: In begin_transaction\n", this));

	//
	// create new table transaction
	// object will initiate transaction on every data server
	//
	transp = new table_trans_impl(repl_svr, this, cluster, table_name);
	ASSERT(transp);
	transp->startup(invalid_ok, env);
	if ((ex = env.release_exception()) != NULL) {
		CCR_DBG(("CCR TM: table_trans_impl->startup returns "
		    "exception\n"));
		delete transp;	// This will do the unlock_writes()
		if (CORBA::SystemException::_exnarrow(ex)) {
			delete ex;
			// mask system exception
			e.exception(new ccr_trans::comm_error(false));
		} else {
			e.exception(ex);
		}
		ti->rele();
		return (ccr::updatable_table::_nil());
	}

	// obtain reference
	trans_ref = transp->get_objref();

	// checkpoint creation to secondaries
	uint_t num_copies = transp->num_copies;
	ccr_data::updatable_copy_seq copies(num_copies, num_copies);
	for (uint_t i = 0; i < num_copies; i++)
		copies[i] = ccr_data::updatable_table_copy::_duplicate(
							transp->copies[i]);

	CCR_DBG(("CCR TM %p: Calling ckpt_create_transaction\n", this));
	// ckpt creates trans state
	if (is_default_cluster(cluster)) {
		get_checkpoint()->ckpt_create_transaction(trans_ref,
		    table_name, copies, transp->local_ccr_ids, false, e);
	} else {
		get_checkpoint()->ckpt_create_transaction_zc(trans_ref,
		    cluster, table_name, copies, transp->local_ccr_ids,
		    false, e);
	}
	CL_PANIC(e.exception() == NULL);

	CCR_DBG(("CCR TM: ti for table %s cluster %s = %p "
	    "running_trans = %p\n", table_name, cluster, ti, transp));

	// store transp in tinfo record
	ti->running_trans = transp;

	add_commit(e);

	ti->rele();
	return (trans_ref);
}

//
// what ckpt_create_transaction does on secondaries
// note: this method is called by both begin_transaction
// and dump_state, but does not create the transaction state
// in the latter (indicated by the lack of context)
//
void
manager_impl::ckptwork_create_transaction(ccr::updatable_table_ptr trans_ref,
				const char *cluster_name,
				const char *table_name,
				const ccr_data::updatable_copy_seq &copies,
				const quorum::ccr_id_list_t &ccr_ids,
				bool transaction_done,
				Environment &e)
{
	table_trans_impl	*transp;
	table_info		*tinfo;
	mgr_cluster_info	*clinfop;

	CCR_DBG(("CCR TM %p: in ckptwork_create_transaction\n", this));
	// create shadow copy of transaction object
	// transaction will failover to transp if primary dies
	transp = new table_trans_impl(trans_ref, this, cluster_name,
	    table_name, copies, ccr_ids);
	ASSERT(transp);

	// We only checkpoint completed transactions during dumpstate.
	CL_PANIC(!transaction_done || secondary_ctx::extract_from(e) == NULL);

	//
	// we should never be frozen here
	//
	bool ret = reg_info->begin_transaction();
	CL_PANIC(ret);

	clinfop = lookup_cluster_info(cluster_name);
	ASSERT(clinfop);

	// stick transp in the tinfo entry for this table
	tinfo = clinfop->lookup_table(table_name);
	ASSERT(tinfo);

	CCR_DBG(("CCR TM ckpt: ti for table %s cluster %s = %p "
	    "running_trans = %p\n", table_name, cluster_name, tinfo, transp));

	tinfo->running_trans = transp;
	// lock table for writing
	// no need for mutex since ckpts are serialized
	tinfo->in_use = true;
	tinfo->rele();

	// check if this is a mini-transaction checkpoint
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	if (ctxp) {
		// create ckpt state to handle retries
		simple_state *tx_state = new simple_state();
		ASSERT(tx_state);
		// register state object with framework
		tx_state->register_state(e);
	}

	if (transaction_done) {
		transp->cleanup(false);
	}
}

void
manager_impl::convert_spare_transactions()
{
	uint_t			indx;
	uint_t			num_tables;
	table_info		*ti;
	table_info		*ti_dir;
	char			*tname;
	mgr_cluster_info	*clinfop;

	CCR_DBG(("CCR TM: In convert spare transactions\n"));
	//
	// If recovery_done is not true, then there are
	// no transactions to cleanup, since trans are
	// ckpted only after recovery_done is true.
	//
	if (!recovery_done) {
		return;
	}

	//
	// Walk through the list of clusters and free up old transaction
	// objects  as well as running transaction for all the tables
	// in the cluster.
	//
	cl_mutex.lock();
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		num_tables = 0;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
		}
		for (indx = 0; indx < num_tables; indx++) {
			tname = (*clinfop->directory_elems.seq)[indx].key;
			ti = clinfop->lookup_table(tname);
			ASSERT(ti);

			//
			// Free up the old transaction objects as
			// well as the running one.
			//
			ti->old_trans_list.dispose();

			if (ti->running_trans) {
				delete ti->running_trans;
				ti->running_trans = NULL;
			}
			ti->rele();
		}
		// Directory table for each cluster.
		ti = clinfop->lookup_table(DIR);
		ASSERT(ti);
		ti->old_trans_list.dispose();

		if (ti->running_trans) {
			delete ti->running_trans;
			ti->running_trans = NULL;
		}
		ti->rele();
		if (is_default_cluster(clinfop->clusterp)) {
			// cluster_directory table
			ti_dir = clinfop->lookup_table(CLUSTER_DIR);
			ASSERT(ti_dir);
			ti_dir->old_trans_list.dispose();

			if (ti_dir->running_trans) {
				delete ti_dir->running_trans;
				ti_dir->running_trans = NULL;
			}
			ti_dir->rele();
		}
	}
	cl_mutex.unlock();
}

void
simple_state::committed()
{
	CCR_DBG(("CCR TM: In simple_state::committed\n"));
}

void
simple_state::orphaned(Environment &)
{
	CCR_DBG(("CCR TM: In simple_state::orphaned\n"));
}

table_info*
manager_impl::lookup_table(const char *cluster, const char *tname)
{
	mgr_cluster_info *clinfop;
	clinfop = lookup_cluster_info(cluster);
	ASSERT(clinfop);
	return (clinfop->lookup_table(tname));
}

//
// Walk through all the clusters and checkpoint information about each
// cluster.
//
void
manager_impl::checkpoint_cluster_info(Environment &e)
{
	table_info		*ti;
	mgr_cluster_info	*clinfop;
	uint_t			indx;
	char			*tname;
	uint_t			num_tables;

	//
	// Walk through all the clusters.
	//
	cl_mutex.lock();
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		num_tables = 0;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
		}
		//
		// Leave room for directory and cluster_directory in rec
		// sequence.
		//
		ccr_data::consis_seq rec_seq(num_tables + 2, num_tables + 2);
		for (indx = 0; indx < num_tables; indx++) {
			tname = (*clinfop->directory_elems.seq)[indx].key;
			ti = clinfop->lookup_table(tname);
			ASSERT(ti);
			rec_seq[indx] = ti->rec;
			ti->rele();
		}
		ti = clinfop->lookup_table(DIR);
		ASSERT(ti);
		rec_seq[num_tables] = ti->rec;
		//
		// Set checkpoint
		//
		if (is_default_cluster(clinfop->clusterp)) {
			if (ccr_trans::ckpt::_version(get_checkpoint())
			    >= 1) {
				ti = clinfop->lookup_table(CLUSTER_DIR);
				rec_seq[num_tables + 1] = ti->rec;
				ti->rele();
				get_checkpoint()->ckpt_recovery_done(
				    *(clinfop->directory_elems.seq),
				    highest_epoch, rec_seq, e);
			} else {
				//
				// We are sending checkpoint message to a
				// node running the old version. It does not
				// know about cluster_directory.
				//
				ccr_data::consis_seq rec_seq_old(
				    num_tables + 1, num_tables + 1);
				for (indx = 0; indx <= num_tables; indx++) {
					rec_seq_old[indx] = rec_seq[indx];
				}
				get_checkpoint()->ckpt_recovery_done(
				    *(clinfop->directory_elems.seq),
				    highest_epoch, rec_seq_old, e);
			}
		} else {
			if (num_tables != 0) {
				get_checkpoint()->ckpt_recovery_done_zc(
				    clinfop->cluster_id, clinfop->clusterp,
				    num_tables, *(clinfop->directory_elems.seq),
				    rec_seq, e);
			} else {
				ccr::element_seq dummy_seq(1, 1);
				get_checkpoint()->ckpt_recovery_done_zc(
				    clinfop->cluster_id, clinfop->clusterp,
				    num_tables, dummy_seq, rec_seq, e);
			}
		}
		//
		// Bail out of panic if the ccr_fi tests caused it.
		//
		FAULTPT_CCR(FAULTNUM_CCR_SYSTEM_ERROR_NODE,
		    fault_ccr::generic_test_op);
		CL_PANIC(e.exception() == NULL);
		//
		// Set the recovery_done flag for the cluster
		//
		clinfop->recovery_done = true;
	}
	cl_mutex.unlock();
	//
	// After all the ZC's are recovered, checkpoint the overall
	// recovery status.
	//
	if (ccr_trans::ckpt::_version(get_checkpoint()) >= 1) {
		get_checkpoint()->ckpt_all_zc_recovery_done(e);
	}
}

//
// this is a retryable HA IDL method
//
// called by data server to register itself
// if node crashes, handle gets _unreferenced which calls unregister
//
ccr_trans::handle_ptr
manager_impl::register_data_server(ccr_data::data_server_ptr dsp,
				quorum::ccr_id_t ccr_id,
				Environment &e)
{
	manhandle		*ds_handle;
	ccr_trans::handle_ptr	handlep = ccr_trans::handle::_nil();
	ds_info_t		*ds_infop;
	char			nodename[CL_MAX_LEN+1];
	CORBA::Exception	*ex;
	Environment		env;

	//
	// Check for saved state (retry)
	//
	primary_ctx *ctxp = primary_ctx::extract_from(e);
	reg_state *saved_state = (reg_state *)ctxp->get_saved_state();
	if (saved_state) {
		// no need to get a snapshot of ds_info_list here
		// since the ops on the sequence do not involve
		// remote invocations, so they can be done within
		// the ds_list_mutex lock-unlock block.
		ds_list_mutex.lock();
		// get handle from ds_info_list
		for (ds_info_list.atfirst(); (ds_infop =
		    ds_info_list.get_current()) != NULL;
		    ds_info_list.advance()) {
			if (dsp->_equiv(ds_infop->dsp)) {
				handlep = ds_infop->hand->get_objref();
				break;
			}
		}
		ds_list_mutex.unlock();

		//
		// The HA framework guarantees that an _unreferenced
		// will not be delivered while in the retry code, so
		// handlep must be non-nil.
		//
		CL_PANIC(!CORBA::is_nil(handlep));

		reg_info->progress_mutex.lock();
		if (reg_info->registration_pending) {
			//
			// There should be no transactions in progress when
			// the registration lock is held.
			//
			CL_PANIC(reg_info->num_transactions == 0);

			//
			// Since the registration lock is held, it means that
			// the old TM primary died in the middle of processing
			// this register_data_server() request. Need to redo
			// recovery/join steps.
			//
			reg_info->progress_mutex.unlock();
			goto retry;
		} else {
			//
			// If reg_info->registration_pending is false, it means
			// that the old TM primary died after having called
			// add_commit() but before returning from
			// register_data_server(). add_commit() calls
			// reg_info->end_registration() which sets the
			// reg_info->registration_pending flag to false on
			// TM secondaries. If there were waiting threads on
			// the old TM primary, all of them would retry their
			// requests on the new TM primary and whichever thread
			// gets the registration lock first would proceed and
			// wake up other waiting threads once it's done.
			//
			// So here we can simply return without doing anything.
			// See bug 4853796 for more details.
			//
			reg_info->progress_mutex.unlock();
			return (handlep);
		}
	}

	//
	// flush out registrations and transactions
	// if primary gets frozen return retry exception
	// (data server handles retries)
	//
	if (!reg_info->begin_registration()) {
		// abort since primary is frozen
		e.exception(new ccr_trans::comm_error(true));
		return (ccr_trans::handle::_nil());
	}

	CCR_DBG(("CCR TM: Registration request received\n"));

	// create handle to return to data server
	// handle will get unrefd if ds dies

	ds_handle = new manhandle(repl_svr, this, dsp);
	ASSERT(ds_handle);

	// add data server and ccr_id to list, by default, the DS is active
	if (!add_ds_info(dsp, ccr_id, ds_handle, false)) {
		CCR_DBG(("CCR TM: add_ds_info failed : ds_exists exception\n"));
		e.exception(new ccr_trans::ds_exists());
		delete ds_handle;
		reg_info->end_registration();
		return (ccr_trans::handle::_nil());
	}

	// No need to protect this get_objref() here since it is not
	// possible to get an _unreferenced for this object yet.
	handlep = ds_handle->get_objref();

	// checkpoint registration
	get_checkpoint()->ckpt_reg_data_server(dsp, ccr_id, handlep, false, e);
	CL_PANIC(e.exception() == NULL);

retry:
	if (recovery_done) {
		// bring joining node up to date
		if (!propagate_data(dsp)) {
			//
			// propagate_data to joiner failed
			// Either the joiner has system_error or is dead.
			// If the joiner is not dead, we need to tell the
			// joiner to throw itself out by marking the
			// SYSTEM_ERROR flag.
			//
			dsp->set_recovery_state(
			    ccr_data::data_server::CCR_SYSTEM_ERROR, env);
			env.clear();
			remove_ds_info(dsp, true);
			CORBA::release(handlep);
			handlep = nil;
			clconf_get_nodename(ccr_id, nodename);
			//
			// SCMSGS
			// @explanation
			// The indicated node failed to update its repository
			// with the ones in current membership. And it will
			// not be able to join the current membership.
			// @user_action
			// There may be other related messages on the
			// indicated node, which help diagnose the problem,
			// for example: If the root disk failed, it needs to
			// be replaced. If the root disk is full, remove some
			// unnecessary files to free up some space.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: The repository on the joining node %s could "
			    "not be recovered, join aborted.", nodename);
		} else {
			if (!we_have_quorum) {
				// means we lost quorum at some point after
				// recovery was done; check if we have it now
				if (check_quorum()) {
					//
					// SCMSGS
					// @explanation
					// The cluster lost quorum at sometime
					// after the cluster came up, and the
					// quorum is regained now.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) ccr_syslog_msg.log(
					    SC_SYSLOG_NOTICE, MESSAGE,
					    "CCR: Quorum regained.");
				}
			}
		}
	} else while (check_quorum() && !recovery_done) {
		// we have quorum
		do_recovery(env);
		if ((ex = env.release_exception()) != NULL) {
			if (ccr::table_invalid::_exnarrow(ex) != NULL) {
				//
				// This exception only corresponds to
				// an invalid epoch or directory, which
				// will prevent the node from coming up.
				//
				delete ex;
				break;
			}
			//
			// do_recovery failed because one of the nodes is dead
			// or needs to leave the cluster due to system_error .
			// We need to check if we still have quorum before
			// retrying the do_recovery.
			//
			delete ex;
		} else {
			recovery_done = true;
			//
			// Checkpoint information about all the clusters.
			//
			checkpoint_cluster_info(e);
		}
	}

	// commit mini-transaction before calling end_registration
	// to prevent deadlock on secondary
	add_commit(e);
	CL_PANIC(e.exception() == NULL);
	reg_info->end_registration();
	return (handlep);
}

//
// what ckpt_reg_data_server does on secondaries
// note: this method is called by both register_data_server
// and dump_state, but does not create the transaction state
// in the latter (indicated by the lack of context)
//
void
manager_impl::ckptwork_reg_data_server(ccr_data::data_server_ptr dsp,
				quorum::quorum_device_id_t ccr_id,
				ccr_trans::handle_ptr handlep,
				bool deactivated, Environment &e)
{
	CCR_DBG(("CCR TM %p: in ckptwork_reg_data_server\n", this));

	// create shadow copy of handle object
	manhandle *ds_handle = new manhandle(handlep, this, dsp);
	ASSERT(ds_handle);

	// begin_reg should not block since ckpt is being done
	// sequentially and nobody else would be registering at
	// this time. Also, the number of ongoing transactions
	// should be zero otherwise the data_server reg would not
	// succeed on the primary.
	CL_PANIC(!(reg_info->registration_pending) &&
	    reg_info->num_transactions == 0);
	(void) reg_info->begin_registration();
	(void) add_ds_info(dsp, ccr_id, ds_handle, deactivated);

	// check if this is a mini-transaction checkpoint
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	if (ctxp) {
		// create ckpt state to handle retries
		reg_state *tx_state = new reg_state(this);
		ASSERT(tx_state);
		// register state object with framework
		tx_state->register_state(e);
		// No need to do end_registration() here since
		// that is done in reg_state::committed().
	} else {
		reg_info->end_registration();
	}
}

void
manager_impl::convert_spare_data_servers(void)
{
	// Delete the ds_info_list list. No need
	// to protect the list with locks since the
	// HA framework guarantees that there will be
	// no accesses to this list.
	ds_info_list.dispose();
}

reg_state::reg_state(manager_impl *mgr):
	the_mgr(mgr)
{
}

reg_state::~reg_state()
{
	the_mgr = NULL;
}

void
reg_state::committed()
{
	CCR_DBG(("CCR TM: in reg_state::committed\n"));
	the_mgr->reg_info->end_registration();
}

void
reg_state::orphaned(Environment &)
{
	CCR_DBG(("CCR TM: In reg_state::orphaned\n"));
	the_mgr->reg_info->end_registration();
}

void manager_impl::ckptwork_recovery_done_zc(uint_t cluster_id,
    const char *cluster, uint_t num_tables, const ccr::element_seq &dir_elems,
    const ccr_data::consis_seq &recs)
{
	uint_t		i;
	table_info	*ti;
	mgr_cluster_info *clinfop;

	CCR_DBG(("CCR TM %p: in ckptwork_recovery_done_zc, cluster = %s\n",
	    this, cluster));

	clinfop = lookup_cluster_info(cluster);
	if (clinfop == NULL) {
		clinfop = new mgr_cluster_info(cluster_id, cluster);
		ASSERT(clinfop);
		//
		// Add the new cluster information to the list.
		//
		cl_mutex.lock();
		cluster_list.append(clinfop);
		cl_mutex.unlock();
	}
	if (num_tables == 0) {
		clinfop->directory_elems.seq = NULL;
	} else {
		clinfop->directory_elems.seq = new
		    ccr::element_seq(num_tables, num_tables);
	}
	//
	// cluster_directory hash entry
	//
	if (is_default_cluster(cluster)) {
		//
		// recs should have two more entries than dir_elems
		// for the DIR table and the CLUSTER_DIR table
		//
		ti = new table_info;
		ASSERT(ti);
		ti->rec = recs[num_tables + 1];
		clinfop->tinfo_hashtab.add(ti, CLUSTER_DIR);
	}
	//
	// directory hash entry
	//
	ti = new table_info; //lint !e423
	ASSERT(ti);
	ti->rec = recs[num_tables]; //lint !e423
	clinfop->tinfo_hashtab.add(ti, DIR);

	//
	// For each table, add directory and hashtab entries
	//
	for (i = 0; i < num_tables; i++) {
		const char *tname = (dir_elems)[i].key;
		ti = new table_info; //lint !e423
		ASSERT(ti);
		ti->rec = recs[i]; //lint !e423
		clinfop->tinfo_hashtab.add(ti, tname);
		// directory_elems entry
		(*clinfop->directory_elems.seq)[i] = dir_elems[i];
	}
	clinfop->recovery_done = true;
}

//
// Set the global recovery_done flag on the secondaries.
//
void
manager_impl::ckptwork_all_zc_recovery_done()
{
	recovery_done = true;
}

//
// what ckpt_recovery_done does on secondaries
// This function gets called for the global cluster.
//
void
manager_impl::ckptwork_recovery_done(
    const ccr::element_seq &dir_elems, ccr_data::epoch_type epoch_num,
    const ccr_data::consis_seq &recs)
{
	uint_t num_tables = dir_elems.length();
	ASSERT(num_tables > 0);
	highest_epoch = epoch_num;
	CL_PANIC(highest_epoch != -1);
	ckptwork_recovery_done_zc(0, DEFAULT_CLUSTER,
	    num_tables, dir_elems, recs);
}

void
manager_impl::convert_spare_recovery()
{
	//
	// If recovery_done is not true, then there are
	// no recovery_related data structures to free
	// up (since these would get ckpted only after
	// recovery_done is true).
	//
	if (!recovery_done) {
		return;
	}

	//
	// The dispose() method for the hashtable only deletes
	// pointers from the hashtable, so each entry pointed to
	// by these pointers need to be deleted/released.
	// Instead of iterating on the hashtable, and then deleting
	// each entry (which requires the corresp tname), start from
	// the directory_elems
	//
	table_info	*ti;
	char		*tname;
	uint_t		num_tables;
	uint_t		i;
	mgr_cluster_info	*clinfop;

	cl_mutex.lock();
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		num_tables = 0;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
		}
		for (i = 0; i < num_tables; i++) {
			tname = (*clinfop->directory_elems.seq)[i].key;
			ti = clinfop->tinfo_hashtab.remove(tname);
			ASSERT(ti);
			ti->rele();
		}

		ti = clinfop->tinfo_hashtab.remove(DIR);
		ASSERT(ti);
		ti->rele();
		if (is_default_cluster(clinfop->clusterp)) {
			ti = clinfop->tinfo_hashtab.remove(CLUSTER_DIR);
			ASSERT(ti);
			ti->rele();
		}
		delete clinfop->directory_elems.seq;
		clinfop->directory_elems.seq = NULL;
	}
	highest_epoch = -1;
	recovery_done = false;
	cl_mutex.unlock();
}

//
// called by table_trans_impl::commit_transaction (primary)
// and commit_state::committed() for create_table
// update internal table info structs
//
void
manager_impl::create_table_int(uint_t cluster_id, const char *cluster,
    const char *tname)
{
	mgr_cluster_info *clinfop;
	clinfop = lookup_cluster_info(cluster);

	//
	// If a new directory is being added, then a new cluster is being
	// created. Add the cluster to the incore list of clusters.
	// The cluster_id argument is needed only when a new cluster is
	// being created.
	//
	if (is_dir(tname)) {
		ASSERT(clinfop == NULL);
		clinfop = new mgr_cluster_info(cluster_id, cluster);
		clinfop->add_incore_table(tname);
		cl_mutex.lock();
		cluster_list.append(clinfop);
		cl_mutex.unlock();
	} else {
		ASSERT(clinfop);
		clinfop->add_incore_table(tname);
	}
}

//
// called by table_trans_impl::commit_transaction (primary)
// and commit_state::committed() for remove_table
// update internal table info structs
//
void
manager_impl::remove_table_int(const char *cluster, const char *tname)
{
	mgr_cluster_info *clinfop;
	clinfop = lookup_cluster_info(cluster);
	ASSERT(clinfop);

	//
	// If a directory is being removed, then the cluster is going away.
	// Remove the cluster information for the directory.
	//
	if (is_dir(tname)) {
		// Cant remove global cluster
		ASSERT(!is_default_cluster(cluster));
		cl_mutex.lock();
		for (cluster_list.atfirst();
		    (clinfop = cluster_list.get_current()) != NULL;
		    cluster_list.advance()) {
			if (os::strcmp(clinfop->clusterp, cluster) == 0) {
				CCR_DBG(("CCR TM: Removing cluster %s from "
				    "cluster list\n", clinfop->clusterp));
				(void) cluster_list.erase(clinfop);
				delete clinfop;
				break;
			}
		}
		cl_mutex.unlock();
	} else {
		CCR_DBG(("CCR TM: Removing table %s from cluster %s\n",
		    tname, cluster));
		clinfop->remove_incore_table(tname);
	}
}


// Called by table_trans_impl and by manager_impl to create a
// snapshot of ds_info_list. The list creation is done within
// locks. This method needed because an unreferenced may be
// called anytime and remove one of the entries of ds_info_list.
void
manager_impl::create_ds_snapshot(ds_info_list_t &local_list)
{
	ds_info_t *ds_infop, *new_ds_infop;
	ds_list_mutex.lock();
	CL_PANIC(local_list.empty());
	for (ds_info_list.atfirst();
	    (ds_infop = ds_info_list.get_current()) != NULL;
	    ds_info_list.advance()) {
		//
		// Make a copy of the "dsp" and "ccr_id" fields
		// in the list element. The dsp field needs
		// to be duplicated since in the event of a DS
		// dying, the original reference will be released
		// by the manhandle::_unreferenced() code. The
		// duplicating is done in the constructor of class
		// ds_info_t. In the destructor of ds_info_t, it
		// will do a release reference.
		//
		// Set the "hand" field for each to NULL since this
		// field should never be used from the local copy
		// (since it can become invalid if an _unreferenced
		// is called releasing the handle, leaving the
		// hand ptr invalid).
		//
		if (!ds_infop->deactivated) {
			new_ds_infop = new ds_info_t(ds_infop->dsp,
			    ds_infop->ccr_id, NULL, false);
			local_list.append(new_ds_infop);
		}
	}
	ds_list_mutex.unlock();
}

bool
manager_impl::check_quorum()
{
	Environment		e;
	quorum::ccr_id_list_t	ccr_ids;
	bool			retval;
	ds_info_list_t		local_ds_list;
	ds_info_t		*ds_infop;
	uint_t			i, num_ds;

	quorum_obj = cmm_ns::get_quorum_algorithm(NULL);
	create_ds_snapshot(local_ds_list);
	num_ds = local_ds_list.count();

	// pass sequence of ccr_ids to cmm
	ccr_ids.length(num_ds);
	for (local_ds_list.atfirst(), i = 0;
	    (ds_infop = local_ds_list.get_current()) != NULL;
	    local_ds_list.advance(), i++) {
		// ignore dsp marked as deactivated
		if (!ds_infop->deactivated) {
			ccr_ids[i] = ds_infop->ccr_id;
		}
	}
	retval = quorum_obj->check_ccr_quorum(ccr_ids, e);
	if (e.exception()) {
		e.clear();
		retval = false;
	}
	we_have_quorum = retval;
	// reset flag if it was set
	need_quorum_recheck = false;

	// Delete all the elments in local_ds_list
	local_ds_list.dispose();
	return (retval);
}

void
manager_impl::do_recovery(Environment &e)
{
	ccr_data::epoch_type	*epochs;
	uint_t			i, num_ds, num_tables;
	ds_info_list_t		local_ds_list;
	ds_info_t		*ds_infop;
	CORBA::Exception	*ex;
	Environment		env;
	ccr::system_error	*err = NULL;
	char			nodename[CL_MAX_LEN+1];
	mgr_cluster_info	*clinfop;

	CCR_DBG(("CCR TM: In do_recovery\n"));
	// create a local copy of ds_info_list.
	create_ds_snapshot(local_ds_list);
	num_ds = local_ds_list.count();

	//
	// Fault point for start of do_recovery.
	//
	FAULTPT_CCR(FAULTNUM_CCR_DO_RECOVERY_1,
	    fault_ccr::generic_test_op);

	//
	// get epoch num from each data server and select highest
	//
	highest_epoch = -1;
	epochs = new ccr_data::epoch_type[num_ds];
	for (local_ds_list.atfirst(), i = 0;
	    (ds_infop = local_ds_list.get_current()) != NULL;
	    local_ds_list.advance(), i++) {
		//
		// Fault point to allow reboots when querying for
		// CCR metadata.
		//
		FAULTPT_CCR(FAULTNUM_CCR_DO_RECOVERY_2,
		    fault_ccr::generic_test_op);

		epochs[i] = ds_infop->dsp->get_epoch(env);
		if ((ex = env.release_exception()) != NULL) {
			if (DS_UNREACHABLE(ex)) {
				remove_ds_info(ds_infop->dsp, true);
				CCR_DBG(("CCR TM: get_epoch returned "
				    "unreachable exception for ds %d\n", i));
				delete [] epochs;
				local_ds_list.dispose();
				e.exception(ex);
				return;
			} else if ((err = ccr::system_error::_exnarrow(ex))
			    != NULL) {
				if (err->errnum == ENOENT) {
					// epoch doesn't exist on that node,
					// we will treat it as invalid instead.
					delete ex;
					epochs[i] = -1;
				} else {
					e.exception(ex);
					ds_infop->dsp->set_recovery_state(
					    ccr_data::data_server::
						CCR_SYSTEM_ERROR, env);
					env.clear();
					remove_ds_info(ds_infop->dsp, true);
					delete [] epochs;
					local_ds_list.dispose();
					return;
				}
			} else {
				// epoch file doesn't have valid entry
				delete ex;
				epochs[i] = -1;
			}
		}
		if (epochs[i] > highest_epoch) {
			highest_epoch = epochs[i];
		}
	}

	if (highest_epoch < 0) {
		delete [] epochs;
		local_ds_list.dispose();
		//
		// SCMSGS
		// @explanation
		// The epoch indicates the number of times a cluster has come
		// up. It should not be less than 0. It could happen due to
		// corruption in the cluster repository.
		// @user_action
		// Boot the cluster in -x mode to restore the cluster
		// repository on all the members of the cluster from backup.
		// The cluster repository is located at /etc/cluster/ccr/.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE, "CCR: "
		    "Highest epoch is < 0, highest_epoch = %d.", highest_epoch);
		e.exception(new ccr::table_invalid("epoch"));
		return;
	}

	//
	// cluster_directory must be recovered or we hang
	// If cluster object has not yet been created, it will be created
	// within recover_table.
	//
	clinfop = lookup_cluster_info(DEFAULT_CLUSTER);
	recover_table(clinfop, DEFAULT_CLUSTER, CLUSTER_DIR, epochs, env);

	if ((ex = env.release_exception()) != NULL) {
		if (ccr::table_invalid::_exnarrow(ex) != NULL) {
			//
			// SCMSGS
			// @explanation
			// The CCR could not find valid metadata on all nodes
			// of the cluster.
			// @user_action
			// Boot the cluster in -x mode to restore the cluster
			// repository on all the nodes in the cluster from
			// backup. The cluster repository is located at
			// /etc/cluster/ccr/.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Invalid CCR metadata.");
		} else if ((err = ccr::system_error::_exnarrow(ex)) != NULL) {
			clconf_get_nodename(err->nodeid, nodename);
			//
			// SCMSGS
			// @explanation
			// The indicated error occurred when CCR is trying to
			// access the CCR metadata on the indicated node.
			//
			// The errno value indicates the nature of the
			// problem. errno values are defined in the file
			// /usr/include/sys/errno.h. An errno value of
			// 28(ENOSPC) indicates that the root files system on
			// the node is full. Other values of errno can be
			// returned when the root disk has failed(EIO).
			// @user_action
			// There may be other related messages on the node
			// where the failure occurred. These may help diagnose
			// the problem. If the root file system is full on the
			// node, then free up some space by removing
			// unnecessary files. If the root disk on the
			// afflicted node has failed, then it needs to be
			// replaced. If the cluster repository is corrupted,
			// boot the indicated node in -x mode to restore it
			// from backup. The cluster repository is located at
			// /etc/cluster/ccr/.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Can't access CCR metadata on node %s"
			    " errno = %d.", nodename, err->errnum);
		} else {
			CL_PANIC(DS_UNREACHABLE(ex));
		}
		e.exception(ex);
		delete [] epochs;
		local_ds_list.dispose();
		return;
	}

	//
	// Recover all the tables in all the configured clusters.
	//
	cl_mutex.lock();
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		num_tables = 0;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
		}
		//
		// Recover the directory table of the cluster.
		//
		recover_table(clinfop, clinfop->clusterp, DIR, epochs, env);

		if ((ex = env.release_exception()) != NULL) {
			if (ccr::table_invalid::_exnarrow(ex) != NULL) {
				//
				// SCMSGS
				// @explanation
				// CCR could not find a valid version of the
				// indicated table on the nodes in the
				// cluster.
				// @user_action
				// There may be other related messages on the
				// nodes where the failure occurred. They may
				// help diagnose the problem. If the indicated
				// table is unreadable due to disk failure,
				// the root disk on that node needs to be
				// replaced. If the table file is corrupted or
				// missing, boot the cluster in -x mode to
				// restore the indicated table from backup.
				// The CCR tables are located at
				// /etc/cluster/ccr/.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: Invalid CCR table : %s "
				    "cluster %s.", DIR, clinfop->clusterp);
				delete ex;
				continue;
			} else if ((err = ccr::system_error::_exnarrow(ex))
			    != NULL) {
				clconf_get_nodename(err->nodeid, nodename);
				//
				// SCMSGS
				// @explanation
				// The indicated error occurred when CCR was
				// tried to access the indicated table on the
				// nodes in the cluster.
				//
				// The errno value indicates the nature of the
				// problem. errno values are defined in the
				// file /usr/include/sys/errno.h. An errno
				// value of 28(ENOSPC) indicates that the root
				// files system on the node is full. Other
				// values of errno can be returned when the
				// root disk has failed(EIO).
				// @user_action
				// There may be other related messages on the
				// node where the failure occurred. They may
				// help diagnose the problem. If the root file
				// system is full on the node, then free up
				// some space by removing unnecessary files.
				// If the root disk on the afflicted node has
				// failed, then it needs to be replaced. If
				// the indicated table was accidently removed,
				// boot the indicated node in -x mode to
				// restore the indicated table from backup.
				// The CCR tables are located at
				// /etc/cluster/ccr/.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: Can't access "
				    "cluster %s table %s on node %s errno = "
				    "%d.", clinfop->clusterp, DIR, nodename,
				    err->errnum);
			} else {
				CL_PANIC(DS_UNREACHABLE(ex));
			}
			e.exception(ex);
			delete [] epochs;
			local_ds_list.dispose();
			cl_mutex.unlock();
			return;
		}
		//
		// Recover the tables of the cluster if any.
		//
		for (i = 0; i < num_tables; i++) {
			char *tname = (*clinfop->directory_elems.seq)[i].key;

			//
			// Fault point to cause changes while recovering
			// tables.
			//
			FAULTPT_CCR(FAULTNUM_CCR_DO_RECOVERY_3,
			    fault_ccr::generic_test_op);

			recover_table(clinfop, clinfop->clusterp, tname,
			    epochs, env);

			if ((ex = env.release_exception()) != NULL) {
				if (ccr::table_invalid::_exnarrow(ex) != NULL) {
					(void) ccr_syslog_msg.log(
					    SC_SYSLOG_WARNING, MESSAGE,
					    "CCR: Invalid CCR table : %s "
					    "cluster %s.", tname,
					    clinfop->clusterp);
					continue;
				} else if ((err =
				    ccr::system_error::_exnarrow(ex)) != NULL) {
					clconf_get_nodename(err->nodeid,
					    nodename);
					(void) ccr_syslog_msg.log(
					    SC_SYSLOG_WARNING,
					    MESSAGE, "CCR: Can't access "
					    "cluster %s table %s on node %s "
					    "errno = %d.", clinfop->clusterp,
					    tname, nodename, err->errnum);
				} else {
					CL_PANIC(DS_UNREACHABLE(ex));
				}
				e.exception(ex);
				delete [] epochs;
				local_ds_list.dispose();
				cl_mutex.unlock();
				return;
			}
		}
	}
	cl_mutex.unlock();

	// advance and set epoch on data servers
	highest_epoch++;
	for (local_ds_list.atfirst(); (ds_infop = local_ds_list.get_current())
	    != NULL; local_ds_list.advance()) {
		//
		// Fault point to change system while setting epoch.
		//
		FAULTPT_CCR(FAULTNUM_CCR_DO_RECOVERY_4,
		    fault_ccr::generic_test_op);

		ds_infop->dsp->set_epoch(highest_epoch, env);
		if ((ex = env.release_exception()) != NULL) {
			if ((ccr::system_error::_exnarrow(ex)) != NULL) {
				clconf_get_nodename(err->nodeid, nodename);
				//
				// SCMSGS
				// @explanation
				// The CCR was unable to set the epoch number
				// on the indicated node.
				//
				// The epoch was set by CCR to record the
				// number of times a cluster has come up. This
				// information is part of the CCR metadata.
				// @user_action
				// There may be other related messages on the
				// indicated node, which may help diagnose the
				// problem, for example: If the root file
				// system is full on the node, then free up
				// some space by removing unnecessary files.
				// If the root disk on the afflicted node has
				// failed, then it needs to be replaced.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE,
				    "CCR: Failed to set epoch on node %s "
				    "errno = %d.", nodename, err->errnum);
				ds_infop->dsp->set_recovery_state(
				    ccr_data::data_server::CCR_SYSTEM_ERROR,
				    env);
				env.clear();
			}
			delete ex;
		}
	}

	// XXX should pass highest_epoch here
	// inform data servers that recovery is complete
	for (local_ds_list.atfirst(); (ds_infop = local_ds_list.get_current())
	    != NULL; local_ds_list.advance()) {
		//
		// Fault point to change system while informing complete.
		//
		FAULTPT_CCR(FAULTNUM_CCR_DO_RECOVERY_5,
		    fault_ccr::generic_test_op);

		ds_infop->dsp->recovery_done(env);
		// The DS clears user exception before returning
		env.clear();
	}

	delete [] epochs;
	local_ds_list.dispose();
}

//
// Save the cluster information in the incore structure. If the
// cluster did not exist before it is added to the list of
// clusters.
//
void
manager_impl::save_cluster_info(ccr::element_seq *elems,
    const ds_info_t *truth_ds_infop, Environment &e)
{
	uint_t			num_clusters;
	ccr::element_seq	*cl_elems = NULL;
	Environment		env;
	CORBA::Exception	*exp;
	mgr_cluster_info	*clinfop;

	num_clusters = elems->length();
	for (uint_t indx = 0; indx < num_clusters; ++indx) {

		const char *cznamep = (*elems)[indx].key;
		const char *czidp = (*elems)[indx].data;
		uint_t cluster_id = (uint_t)os::atoi((char *)czidp);
		ASSERT(cznamep);
		ASSERT(czidp);
		if (is_default_cluster(cznamep)) {
			cl_elems = truth_ds_infop->dsp->
			    get_table_data(DIR, env);
		} else {
			cl_elems = truth_ds_infop->dsp->
			    get_table_data_zc((*elems)[indx].key, DIR, env);
		}
		if ((exp = env.release_exception()) != NULL) {
			//
			// DS unreachable or system error
			//
			CCR_DBG(("CCR TM: get_table_data returned "
			    "exception\n"));
			if (ccr::system_error::_exnarrow(exp) != NULL) {
				//
				// We need to tell the node to die
				// because it has some unrecoverable
				// system_error
				//
				truth_ds_infop->dsp->set_recovery_state(
				    ccr_data::data_server::CCR_SYSTEM_ERROR,
				    env);
				env.clear();
			}
			e.exception(exp);
			remove_ds_info(truth_ds_infop->dsp, true);
			// leave table invalid
			return;
		}
		clinfop = lookup_cluster_info(cznamep);
		if (clinfop == NULL) {
			ASSERT(!is_default_cluster(cznamep));
			CCR_DBG(("CCR TM: Create new cluster info for %s "
			    "with id = %s\n", cznamep, czidp));
			clinfop = new mgr_cluster_info(cluster_id, cznamep);
			cl_mutex.lock();
			cluster_list.append(clinfop);
			cl_mutex.unlock();
		}
		if (cl_elems == NULL) {
			CCR_DBG(("CCR TM: Cluster %s has no tables\n",
			    clinfop->clusterp));
		} else {
			clinfop->directory_elems.add_elements(*cl_elems);
		}
		delete cl_elems;
	}
}

//
// Walk through all the Data servers and retrieve the consistency record
// of a table.
//
ccr_data::consis *
manager_impl::get_table_consis(const char *cluster, const char *tname,
    ds_info_t **override_ds_infop, ds_info_list_t& local_ds_list,
    uint_t& override_ds_idx, Environment &e)
{
	uint_t			num_ds;
	uint_t			i;
	ds_info_t		*ds_infop;
	Environment		env;
	CORBA::Exception	*exp;
	ccr_data::consis	*rec = NULL;
	ccr::system_error	*err;
	char			nodename[CL_MAX_LEN+1];

	num_ds = local_ds_list.count();
	rec = new ccr_data::consis[num_ds];
	for (local_ds_list.atfirst(), i = 0; (ds_infop =
	    local_ds_list.get_current()) != NULL; local_ds_list.advance(),
	    i++) {
		if (is_default_cluster(cluster)) {
			if (is_cluster_dir(tname)) {
				//
				// For rolling upgrade support, we need
				// make sure the data server knows about
				// the cluster_directory table.
				//
				if (ccr_data::data_server::_version(
				    ds_infop->dsp) >= 1) {
					ds_infop->dsp->get_consis(tname,
					    rec[i], env);
				} else {
					CCR_DBG(("CCR TM: Set table %s invalid"
					    " for data server with version"
					    " %d\n", CLUSTER_DIR,
					    ccr_data::data_server::_version(
					    ds_infop->dsp)));
					rec[i].valid = false;
					continue;
				}
			} else {
				ds_infop->dsp->get_consis(tname, rec[i], env);
			}
		} else {
			ds_infop->dsp->get_consis_zc(cluster, tname,
			    rec[i], env);
		}
		if ((exp = env.release_exception()) != NULL) {
			if (DS_UNREACHABLE(exp)) {
				e.exception(exp);
				remove_ds_info(ds_infop->dsp, true);
				return (rec);
			} else if ((err = ccr::system_error::_exnarrow(exp))
			    != NULL) {
				if (err->errnum == ENOENT) {
					//
					// The table file doesn't exist on the
					// node, treat it as invalid.
					//
					rec[i].valid = false;
					delete exp;
				} else {
					e.exception(exp);
					clconf_get_nodename(ds_infop->ccr_id,
					    nodename);
					ds_infop->dsp->set_recovery_state(
					    ccr_data::data_server::
						CCR_SYSTEM_ERROR, env);
					env.clear();
					remove_ds_info(ds_infop->dsp, true);
					//
					// SCMSGS
					// @explanation
					// The CCR failed to read the
					// indicated table on the indicated
					// node. The CCR will attempt to
					// recover this table from other nodes
					// in the cluster.
					// @user_action
					// This is an informational message,
					// no user action is needed.
					//
					(void) ccr_syslog_msg.log(
					    SC_SYSLOG_WARNING, MESSAGE, "CCR: "
					    "Failed to read table %s on node "
					    "%s.", tname, nodename);
					CCR_DBG(("CCR TM: exception in "
					    "get_consis for table %s on "
					    "node %s\n", tname, nodename));
					return (rec);
				}
			} else {
				// ccr::table_invalid
				delete exp;
				rec[i].valid = false;
			}
		} else if (rec[i].override) {
			if (*override_ds_infop == NULL) {
				*override_ds_infop = ds_infop;
				override_ds_idx = i;
			} else if (strcmp(rec[override_ds_idx].checksum,
			    rec[i].checksum) != 0) {
				clconf_get_nodename(
				    (*override_ds_infop)->ccr_id, nodename);
				//
				// SCMSGS
				// @explanation
				// The override flag for a table indicates
				// that the CCR should use this copy as the
				// final version when the cluster is coming
				// up.
				//
				// In this case, the CCR detected multiple
				// nodes having the override flag set for the
				// indicated table. It chose the copy on the
				// indicated node as the final version.
				// @user_action
				// This is an informational message, no user
				// action is needed.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE,
				    "CCR: More than one data server has "
				    "override flag set for the table %s. "
				    "Using the table from node "
				    "%s.", tname, nodename);
			}
		} else if (rec[i].version == -1) {
			rec[i].valid = false;
		}
	}
	return (rec);
}

void
manager_impl::recover_table(
	mgr_cluster_info *clinfop,
	const char *cluster,
	const char *tname,
	const ccr_data::epoch_type *epochs,
	Environment &e)
{
	ccr_data::consis 	*rec;
	ccr::element_seq 	*elems;
	table_info 		*ti;
	int32_t 		truth_version;
	bool 			all_consistent;
	ds_info_list_t		local_ds_list;
	CORBA::Exception	*ex;
	Environment		env;
	ds_info_t		*ds_infop;
	ds_info_t		*override_ds_infop = NULL;
	ds_info_t		*truth_ds_infop = NULL;
	char			nodename[CL_MAX_LEN+1];
	uint_t 			i;
	uint_t			num_ds;
	uint_t			truth_idx = 0;
	uint_t			override_ds_idx = 0;
	bool			is_cluster_directory = false;
	bool			is_directory = false;

	CCR_DBG(("CCR TM %p: in recover_table cluster = %s, table = %s\n",
	    this, cluster, tname));

	if (is_cluster_dir(tname)) {
		is_cluster_directory = true;
	} else if (is_dir(tname)) {
		is_directory = true;
	}
	//
	// get tinfo hash entry for this table
	// tinfo->valid is initialized to false
	// we will validate the table iff recovery succeeds
	//
	if (clinfop == NULL) {
		if (is_default_cluster(cluster)) {
			//
			// The transaction manager does not know about this
			// cluster yet. The table being recovered must be
			// a cluster_directory and the cluster is global
			//
			CL_PANIC(is_cluster_directory);
			//
			// We go ahead and create the information about
			// the new cluster.
			//
			cl_mutex.lock();
			clinfop = new mgr_cluster_info(0, cluster);
			ASSERT(cluster_list.empty());
			cluster_list.append(clinfop);
			cl_mutex.unlock();
		} else {
			CL_PANIC(0);
		}
	}
	ti = clinfop->lookup_table(tname);

	//
	// Need to check for uniqueness before adding to the hashtable
	//
	if (ti == NULL) {
		ti = new table_info;
		ASSERT(ti);
		clinfop->tinfo_mutex.lock();
		clinfop->tinfo_hashtab.add(ti, tname);
		clinfop->tinfo_mutex.unlock();
		CCR_DBG(("CCR TM: Added %s %s\n", cluster, tname));
		//
		// Do an extra hold() on the ti, that way a rele()
		// can be done for it whenever the method returns.
		//
		ti->hold();
	} else {
		// tinfo entry already exists for this table
		ti->rec.valid = false;
	}

	//
	// Create a local ds info sequence.
	//
	create_ds_snapshot(local_ds_list);

	//
	// Get table consis from each data server
	//
	rec = get_table_consis(cluster, tname, &override_ds_infop,
	    local_ds_list, override_ds_idx, e);
	if (e.exception() != NULL) {
		local_ds_list.dispose();
		delete [] rec;
		ti->rele();
		return;
	}

	num_ds = local_ds_list.count();
	truth_version = -1;
	if (override_ds_infop == NULL) {
		//
		// determine which data server has latest version
		//
		for (i = 0; i < num_ds; i++) {
			// skip candidates without highest epoch or valid record
			if (epochs[i] != highest_epoch || !rec[i].valid)
				continue;
			if (rec[i].version > truth_version) {
				truth_idx = i;
				truth_version = rec[i].version;
			}
		}
		if (truth_version == -1) {
			// no valid version, leave table invalid
			delete [] rec;
			CCR_DBG(("CCR TM: No valid table %s on any data "
			    "server\n", tname));
			e.exception(new ccr::table_invalid(tname));
			ti->rele();
			local_ds_list.dispose();
			return;
		}

		all_consistent = true;
		for (i = 0; i < num_ds; i++) {
			//
			// A copy will be replaced if it is invalid, or has
			// different version or checksum as the truth copy.
			//
			if (!rec[i].valid || rec[i].version != truth_version ||
			    os::strcmp(rec[i].checksum,
			    rec[truth_idx].checksum) != 0) {
				all_consistent = false;
				break;
			}
		}

		// if every copy is uptodate and this is data table, we are done
		if (all_consistent && !is_directory && !is_cluster_directory) {
			// validate table with consis info
			ti->rec = rec[truth_idx];
			delete [] rec;
			ti->rele();
			local_ds_list.dispose();
			return;
		}
	} else {
		truth_idx = override_ds_idx;
		truth_ds_infop = override_ds_infop;
	}

	// get the truth_ds_info from the local_ds_list
	if (truth_ds_infop == NULL) {
		for (local_ds_list.atfirst(), i = 0;
		    (ds_infop = local_ds_list.get_current()) != NULL;
			local_ds_list.advance(), i++) {
			if (i == truth_idx) {
				truth_ds_infop = ds_infop;
				break;
			}
		}
	}

	ASSERT(truth_ds_infop != NULL);

	//
	// get table contents from truth copy
	//
	if (is_default_cluster(cluster)) {
		elems = truth_ds_infop->dsp->get_table_data(tname, env);
	} else {
		elems = truth_ds_infop->dsp->get_table_data_zc(cluster,
		    tname, env);
	}
	if ((ex = env.release_exception()) != NULL) {
		// ds unreachable or system error
		CCR_DBG(("CCR TM: get_table_data returned exception\n"));
		if (ccr::system_error::_exnarrow(ex) != NULL) {
			//
			// We need to tell the node to die because it
			// has some unrecoverable system_error
			//
			truth_ds_infop->dsp->set_recovery_state(
			    ccr_data::data_server::CCR_SYSTEM_ERROR, env);
			env.clear();
		}
		e.exception(ex);
		remove_ds_info(truth_ds_infop->dsp, true);
		// leave table invalid
		delete [] rec;
		ti->rele();
		local_ds_list.dispose();
		return;
	}

	//
	// For cluster directory, store table elements
	//
	if (is_cluster_directory) {
		save_cluster_info(elems, truth_ds_infop, e);
		if (e.exception() != NULL) {
			delete [] rec;
			ti->rele();
			local_ds_list.dispose();
			return;
		}
	}
	//
	// propagate contents to other copies
	// When override flag is set all the data servers, including the
	// one containing the overriding file will be replaced.
	// The original file needs to be replaced because it contains the
	// the gennum as -2 and that needs to be changed to 0.
	//
	for (local_ds_list.atfirst(), i = 0;
	    (ds_infop = local_ds_list.get_current()) != NULL;
	    local_ds_list.advance(), i++) {
		//
		// do not have to replace if the table not for override,
		// has same version as the truth version, the same checksum
		// as the truth checksum, the record is valid, the epoch is
		// same as highest epoch
		//
		if ((override_ds_infop == NULL) &&
		    (rec[i].version == truth_version) &&
		    (rec[i].valid) && (epochs[i] == highest_epoch)) {
			if (os::strcmp(rec[i].checksum,
			    rec[truth_idx].checksum) != 0) {
				clconf_get_nodename(truth_ds_infop->ccr_id,
				    nodename);
				//
				// SCMSGS
				// @explanation
				// The CCR detects that two valid copies of
				// the indicated table have the same version
				// but different contents. The copy on the
				// indicated node will be used by the CCR.
				// @user_action
				// This is an informational message, no user
				// action is needed.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: More than one copy "
				    "of table %s has the same version but "
				    "different checksums. Using the table "
				    "from node %s.", tname, nodename);
			} else {
				continue;
			}
		}

		if (is_default_cluster(cluster)) {
			//
			// For the table CLUSTER_DIR, we need to
			// pass in the list of all the clusters and not the
			// list of all the tables in the cluster repository.
			//
			if (is_cluster_dir(tname)) {
				if (ccr_data::data_server::_version(
				    ds_infop->dsp) >= 1) {
					ds_infop->dsp->replace_table(tname,
					    *elems, rec[truth_idx], env);
				} else {
					CCR_DBG(("CCR TM: Skipping replace "
					    "table %s for data server with "
					    "version %d\n", CLUSTER_DIR,
					    ccr_data::data_server::_version(
					    ds_infop->dsp)));
				}
			} else {
				ds_infop->dsp->replace_table(tname,
				    *elems, rec[truth_idx], env);
			}
		} else {
			ds_infop->dsp->replace_table_zc(cluster, tname, *elems,
			    rec[truth_idx], env);
		}

		if ((ex = env.release_exception()) != NULL) {
			// either ccr::system_error or ds_unreachable
			CCR_DBG(("CCR TM: replace_table returned exception\n"));
			if (ccr::system_error::_exnarrow(ex) != NULL) {
				ds_infop->dsp->set_recovery_state(
				    ccr_data::data_server::CCR_SYSTEM_ERROR,
				    env);
				env.clear();
			}
			remove_ds_info(ds_infop->dsp, true);
			e.exception(ex);
			// leave table invalid
			delete [] rec;
			delete elems;
			ti->rele();
			local_ds_list.dispose();
			return;
		}
	}

	// validate table with consis info
	ti->rec = rec[truth_idx];

	delete [] rec;
	delete elems;

	ti->rele();

	local_ds_list.dispose();
}

//
// Propagate the table files in the current membership to the joining node.
// Called by manager_impl::register_data_server(), when the do_recovery is
// done and a new node tries to join.
// return:
//	true	(successfully propagated data to the joiner)
//	false	(failed to propagate data to the joiner)
//
bool
manager_impl::propagate_data(ccr_data::data_server_ptr joiner)
{
	uint_t			num_tables;
	ds_info_list_t		local_ds_list;
	ds_info_t		*ds_infop;
	Environment		env;
	CORBA::Exception	*ex;
	bool			retry;
	bool			propagate_done = false;
	mgr_cluster_info	*clinfop;
	ccr_data::data_server_ptr truth_ds;

	CCR_DBG(("CCR TM : in propagate_data\n"));

	create_ds_snapshot(local_ds_list);

	//
	// in case the selected truth ds is bad or dead, we need to
	// continue the propagation by going to next available DS.
	//
	local_ds_list.atfirst();
	while ((ds_infop = local_ds_list.get_current()) != NULL) {
		if (joiner->_equiv(ds_infop->dsp)) {
			// skip joiner
			local_ds_list.advance();
			continue;
		}
		if (ccr_data::data_server::_version(joiner) < 1) {
			CCR_DBG(("CCR TM: Joiner DS version %d does not "
			    "know about %s\n",
			    ccr_data::data_server::_version(joiner),
			    CLUSTER_DIR));
			continue;
		}
		truth_ds = ds_infop->dsp;
		//
		// Fault point to allow node reboots after truth_ds
		// has been chosen.
		//
		FAULTPT_CCR(FAULTNUM_CCR_PROPAGATE_DATA_1,
		    fault_ccr::generic_test_op);

		if (propagate_table(truth_ds, joiner, DEFAULT_CLUSTER,
		    CLUSTER_DIR, retry)) {
			break;	// we are successfully done with this table
		} else {
			if (!retry) {
				CCR_DBG(("CCR TM Joiner failure during "
				    "propagate_table for cluster_"
				    "directory.\n"));
				// joiner has failure
				local_ds_list.dispose();
				return (propagate_done);
			} else {
				CCR_DBG(("CCR TM Truth DS failure during "
				    "propagate_table for "
				    "cluster_directory.\n"));
				//
				// the selected truth_ds has failure, need to
				// try next available ds.
				//
				local_ds_list.advance();
			}
		}
	}

	if (ds_infop == NULL) {
		// unable to propagate due to failure of all the available
		// truth DSs.
		local_ds_list.dispose();
		return (propagate_done);
	}

	//
	// For each table in directory, get elements from truth data server
	// and propagate them to joiner. We do this for all the clusters.
	//
	cl_mutex.lock();
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		//
		// Propagate the DIR table of the cluster
		//
		while ((ds_infop = local_ds_list.get_current()) != NULL) {
			if (joiner->_equiv(ds_infop->dsp)) {
				// skip joiner
				local_ds_list.advance();
				continue;
			}
			truth_ds = ds_infop->dsp;
			//
			// Fault point to allow node reboots after truth_ds
			// has been chosen.
			//
			FAULTPT_CCR(FAULTNUM_CCR_PROPAGATE_DATA_2,
			    fault_ccr::generic_test_op);

			if (propagate_table(truth_ds, joiner, clinfop->clusterp,
			    DIR, retry)) {
				break;	// we are successfully done
			} else {
				if (!retry) {
					// joiner has failure
					local_ds_list.dispose();
					cl_mutex.unlock();
					return (propagate_done);
				} else {
					// The selected truth ds has failure
					// need to go to next available DS
					local_ds_list.advance();
				}
			}
		}
		if (ds_infop == NULL) {
			// failed due to failure of all available truth DSs
			local_ds_list.dispose();
			cl_mutex.unlock();
			return (propagate_done);
		}
		num_tables = 0;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
		}
		for (uint_t i = 0; i < num_tables; i++) {
			char *tname = (*clinfop->directory_elems.seq)[i].key;
			while ((ds_infop = local_ds_list.get_current())
			    != NULL) {
				if (joiner->_equiv(ds_infop->dsp)) {
					// skip joiner
					local_ds_list.advance();
					continue;
				}
				truth_ds = ds_infop->dsp;
				//
				// Fault point to allow node reboots after
				// truth_ds has been chosen.
				//
				FAULTPT_CCR(FAULTNUM_CCR_PROPAGATE_DATA_2,
				    fault_ccr::generic_test_op);

				if (propagate_table(truth_ds, joiner,
				    clinfop->clusterp, tname, retry)) {
					break;	// we are successfully done
				} else {
					if (!retry) {
						// joiner has failure
						local_ds_list.dispose();
						cl_mutex.unlock();
						return (propagate_done);
					} else {
						//
						// The selected truth ds has
						// failure, need to go to next
						// available DS
						//
						local_ds_list.advance();
					}
				}
			}
			if (ds_infop == NULL) {
				//
				// failed due to failure of all available
				// truth DSs
				//
				local_ds_list.dispose();
				cl_mutex.unlock();
				return (propagate_done);
			}
		}
	}
	cl_mutex.unlock();

	local_ds_list.dispose();	// don't need this list anymore

	// give new epoch number to joiner
	joiner->set_epoch(highest_epoch, env);
	if (env.exception() != NULL) {
		// system_error or ds_unreachable on the joiner
		// joining failed
		env.clear();
		return (propagate_done);
	}

	joiner->recovery_done(env);
	if ((ex = env.exception()) != NULL) {
		// DS clears exceptions before returning
		CL_PANIC(DS_UNREACHABLE(ex));
		CCR_DBG(("CCR TM UNREACHABLE exception in"
		    " recovery done\n"));
		env.clear();
	} else {
		propagate_done = true;
	}

	return (propagate_done);
}

//
// To propagate the specified table to the joiner. Called by
// manager_impl::propagate_data()
//
// If the propagate_table fails due to the failure on the selected truth DS,
// such as ds dead or ccr::system_error, it will return false with the retry
// flag on. Therefore, the caller will pick the next truth DS to retry the
// propagate_table. We will kill the truth DS that has fatal system_error.
//
// If the propagate_table fails due to the failure on the joining node, it can
// not be retried.
//
// Return:
//	true	(successfully propagated the specified table or no need to
//		propagate)
//	false	(failed to propagate the specified table, might be retriable)
//
bool
manager_impl::propagate_table(ccr_data::data_server_ptr truth_ds,
			ccr_data::data_server_ptr joiner,
			const char *cluster, const char *tname,
			bool &retry)
{
	ccr_data::consis	joiner_rec, truth_rec;
	ccr::element_seq	*elems = NULL;
	CORBA::Exception	*ex;
	Environment		env;
	ccr::system_error	*err;
	ds_info_list_t		local_ds_list;
	ds_info_t		*ds_infop, *joiner_ds_infop = NULL;
	char			nodename[CL_MAX_LEN+1];

	retry = false;	// initialize as no retry

	CCR_DBG(("CCR TM: In propagate_table %s:%s\n", cluster, tname));

	//
	// Fault point to allow reboots during table propagation.
	//
	FAULTPT_CCR(FAULTNUM_CCR_PROPAGATE_TABLE_1,
	    fault_ccr::generic_test_op);

	//
	// Get truth consis rec.
	//
	if (is_default_cluster(cluster)) {
		truth_ds->get_consis(tname, truth_rec, env);
	} else {
		truth_ds->get_consis_zc(cluster, tname, truth_rec, env);
	}
	if ((ex = env.exception()) != NULL) {
		CCR_DBG(("CCR TM: get_consis failed on truth_ds.\n"));

		//
		// If get_consis() returns an exception, the table must
		// be invalid.
		//
		// If an IDL function returns an exception, the ORB doesn't
		// marshal its out-parameters and return value.
		//
		// truth_rec is used as an out-parameter in IDL call get_consis.
		// Since get_consis() returns an exception, we know that
		// truth_rec doesn't contain reliable data unless we explicitly
		// set it.
		//
		// We are setting truth_rec.valid here because it is the
		// only thing we will be using later.
		//
		truth_rec.valid = false;

		if ((err = ccr::system_error::_exnarrow(ex)) != NULL) {
			if (err->errnum == ENOENT) {
				//
				// Table file does not exist on the truth ds
				// treat it as invalid table.
				//
				env.clear();
			} else {
				env.clear();
				// truth_ds has system_error, it must die
				truth_ds->die(env);
				env.clear();
				retry = true;	// truth ds failure, need retry
				return (false);
			}
		} else if (ccr::table_invalid::_exnarrow(ex) != NULL) {
			env.clear();
		} else {
			CL_PANIC(DS_UNREACHABLE(ex));
			env.clear();
			retry = true;	// truth ds failure, need retry
			return (false);
		}
	} else if (truth_rec.version == -1) {
		//
		// The truth table should either be recovered or invalid.
		// So the override should be false.
		//
		CL_PANIC(!truth_rec.override);
		truth_rec.valid = false;
	}

	//
	// Fault point to reboot joiner after truth_ds does get_consis,
	// but before joiner does. Should cause joiner->get_consis to
	// fail.
	//
	FAULTPT_CCR(FAULTNUM_CCR_PROPAGATE_TABLE_2,
	    fault_ccr::generic_test_op);

	//
	// Get consis rec from the joiner node.
	//
	if (is_default_cluster(cluster)) {
		joiner->get_consis(tname, joiner_rec, env);
	} else {
		joiner->get_consis_zc(cluster, tname, joiner_rec, env);
	}
	if ((ex = env.exception()) != NULL) {
		CCR_DBG(("CCR TM: get_consis of table %s failed on joiner.\n",
		    tname));

		//
		// If get_consis() returns an exception, the table must
		// be invalid.
		//
		// If an IDL function returns an exception, the ORB doesn't
		// marshal its out-parameters and return value at all.
		//
		// joiner_rec is used as an out-parameter in IDL call
		// get_consis. Since get_consis() returns an exception, we know
		// that truth_rec doesn't contain reliable data unless we
		// explicitly set it.
		//
		// We are setting joiner_rec.valid here because it is the
		// only thing we will be using later.
		//
		joiner_rec.valid = false;

		if ((err = ccr::system_error::_exnarrow(ex)) != NULL) {
			if (err->errnum == ENOENT) {
				//
				// Table file does not exist on the joining
				// node, treat it as an invalid table.
				//
				env.clear();
			} else {
				//
				// The joining node is bad, it should not
				// be allowed to join. We return false here
				// so that the joining node will not be
				// able to register with TM.
				//
				CCR_DBG(("CCR TM: get_consis of table %s failed"
				    " on joiner due to system error errno "
				    "= %d.\n", tname, err->errnum));
				env.clear();
				return (false);
			}
		} else if (ccr::table_invalid::_exnarrow(ex) != NULL) {
			// table_invalid exception.
			env.clear();
		} else {
			// DS_UNREACHABLE
			CL_PANIC(DS_UNREACHABLE(ex));
			env.clear();
			return (false);	// joiner failure, no need retry
		}
	} else if ((joiner_rec.version == -1) && !joiner_rec.override) {
		joiner_rec.valid = false;
	}

	// Get local snapshot of DS info.
	create_ds_snapshot(local_ds_list);
	// Find out the joiner_ds_info from the local_ds_list
	for (local_ds_list.atfirst();
	    (ds_infop = local_ds_list.get_current()) != NULL;
	    local_ds_list.advance()) {
		if (joiner->_equiv(ds_infop->dsp)) {
			joiner_ds_infop = ds_infop;
			break;
		}
	}

	if (!truth_rec.valid) {
		if (is_dir(tname) || is_cluster_dir(tname)) {
			//
			// The directory table should be valid on the truth
			// ds, there must be something wrong on the selected
			// ds. Return false and try another truth ds.
			//
			retry = true;	// truth ds failure, need retry
			local_ds_list.dispose();
			return (false);
		}
		if (!joiner_rec.valid) {
			// No need to propagate this table.
			local_ds_list.dispose();
			return (true);
		} else {
			//
			// Set the joiner's table as invalid since the current
			// copy in cluster is invalid.
			//
			CCR_DBG(("CCR TM: set joiner's valid table to "
			    "invalid\n"));
			if (is_default_cluster(cluster)) {
				joiner->set_table_invalid(tname, env);
			} else {
				joiner->set_table_invalid_zc(cluster,
				    tname, env);
			}
			if ((ex = env.exception()) != NULL) {
				// only DS_UNREACHABLE is possible
				CL_PANIC(DS_UNREACHABLE(ex));
				env.clear();
				local_ds_list.dispose();
				// joiner failure
				return (false);
			}

			if (!joiner_rec.override) {
				//
				// joiner has valid table but no override
				// flag. Will treat as invalid. No need to
				// propagate.
				//
				local_ds_list.dispose();
				return (true);
			} else {
				//
				// Only if the truth table is invalid and the
				// joiner table is valid with override flag set,
				// will we replace the invalid table in the
				// current membership with the valid copy
				// carried by the joiner.
				//
				// However, we do not want to do it during
				// joiner registration because we want it to
				// be done as a full fledged transaction.
				// At this point, we just tell the joiner to
				// record this table into its update_table_list.
				// So that as soon as the joiner finishs the
				// CCR TM registration, it will go through the
				// update_table_list and update the those tables
				// by starting begin_transaction_invalid ok.
				//

				//
				// Add the specified table into joiner's
				// update_table_list.
				//
				if (is_default_cluster(cluster)) {
					joiner_ds_infop->dsp->add_update_table(
					    tname, env);
				} else {
					joiner_ds_infop->dsp->
					    add_update_table_zc(cluster,
					    tname, env);
				}
				env.clear();
				local_ds_list.dispose();
				return (true);	// no need to propagate
			}
		}
	}

	// ignore override field
	if (joiner_rec.valid && joiner_rec.override) {
		if (os::strcmp(joiner_rec.checksum, truth_rec.checksum) != 0) {
			clconf_get_nodename(joiner_ds_infop->ccr_id, nodename);
			//
			// SCMSGS
			// @explanation
			// The override flag for a table indicates that the
			// CCR should use this copy as the final version when
			// the cluster is coming up. If the cluster already
			// has a valid copy while the indicated node is
			// joining the cluster, then the override flag on the
			// joining node is ignored.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_NOTICE, MESSAGE,
			    "CCR: Ignoring override field for "
			    "table %s on joining node %s.", tname, nodename);
		}
		joiner_rec.override = false;
		joiner_rec.version = -1;
	}

	// no need to propagate if joiner is uptodate
	if (joiner_rec.valid && joiner_rec.version == truth_rec.version) {
		if (os::strcmp(joiner_rec.checksum, truth_rec.checksum) != 0) {
			clconf_get_nodename(joiner_ds_infop->ccr_id, nodename);
			//
			// SCMSGS
			// @explanation
			// The indicated table on the joining node has the
			// same version but different contents as the one in
			// the current membership. It will be replaced by the
			// one in the current membership.
			// @user_action
			// This is an informational message, no user action is
			// needed.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Table %s on the joining node %s has the "
			    "same version but different checksum as the copy "
			    "in the current membership. The table on the "
			    "joining node will be replaced by the one in the "
			    "current membership.", tname, nodename);
		} else {
			local_ds_list.dispose();
			return (true);	// no need to propagate this table
		}
	}

	if (is_default_cluster(cluster)) {
		elems = truth_ds->get_table_data(tname, env);
	} else {
		elems = truth_ds->get_table_data_zc(cluster, tname, env);
	}
	if ((ex = env.exception()) != NULL) {
		// ds_unreachable or system_error on truth DS.
		if (ccr::system_error::_exnarrow(ex) != NULL) {
			env.clear();
			// system_error on truth_ds, it must die
			truth_ds->die(env);
		}
		env.clear();
		local_ds_list.dispose();
		retry = true;	// need retry due to truth_ds failure
		return (false);
	}

	if (is_default_cluster(cluster)) {
		ASSERT(!is_cluster_dir(tname) ||
		    ccr_data::data_server::_version(joiner) >= 1);
		joiner->replace_table(tname, *elems, truth_rec, env);
	} else {
		joiner->replace_table_zc(cluster, tname, *elems, truth_rec,
		    env);
	}
	if (env.exception() != NULL) {
		// DS unreachable or system_error on joiner.
		// The joiner should go away either way.
		CCR_DBG(("CCR TM: exception in replace table\n"));
		env.clear();
		local_ds_list.dispose();
		return (false);	// joiner failure, no need to retry
	}

	delete elems;
	local_ds_list.dispose();
	return (true);	// we are done, no need retry
}

//
// Adding a ds_info_list element needs to be protected by
// ds_list_mutex lock-unlock ops on the primary. Not so
// on a secondary, but it is harmless.
//
bool
manager_impl::add_ds_info(ccr_data::data_server_ptr dsp,
			quorum::ccr_id_t ccr_id,
			manhandle *hand,
			bool deactivated)
{
	// This method can only be called when a DS is
	// registering.
	CL_PANIC(reg_info->registration_pending);

	ds_info_t	*ds_infop;

	ds_list_mutex.lock();

	// check if ds is already registered
	for (ds_info_list.atfirst(); (ds_infop = ds_info_list.get_current())
	    != NULL; ds_info_list.advance()) {
		if (dsp->_equiv(ds_infop->dsp)) {
			ds_list_mutex.unlock();
			return (false);
		}
	}

	ds_infop = new ds_info_t(dsp, ccr_id, hand, deactivated);
	ds_info_list.append(ds_infop);

	ds_list_mutex.unlock();
	return (true);
}

// Removing an element from the ds_info_list list must be
// protected by a ds_list_mutex lock-unlock construct for
// the primary. Not necessary for the secondaries.
//
// Remove the dsp in question from the ds_info_list.
// Modifications to the ds_info_list list need to be protected
// by the ds_list_mutex lock-unlock block. Since this lock
// is never held across remote invocations that may occur
// concurrently with the delivery of an _unreferenced, _unreferenced
// (which calls this method) does not block for an unreasonable
// time.
//
void
manager_impl::remove_ds_info(ccr_data::data_server_ptr dsp, bool mark)
{
	// This may be called by manhandle::_unreferenced, so
	// we can not block.

	ds_info_t *ds_infop;

	ds_list_mutex.lock();

	for (ds_info_list.atfirst(); (ds_infop = ds_info_list.get_current())
	    != NULL; ds_info_list.advance()) {
		if (dsp->_equiv(ds_infop->dsp)) {
			if (mark) {
				//
				// we know the node is dead or will die soon,
				// so we mark it first. The actual removal
				// will be done in manhandle::_unreferenced().
				// DS marked as deactived won't be counted
				// in check_quorum() and create_ds_snapshot().
				//
				ds_infop->deactivated = true;
			} else {
				(void) ds_info_list.erase(ds_infop);
				delete ds_infop;
			}
			ds_list_mutex.unlock();
			if (we_have_quorum) {
				//
				// set flag indicating we need to
				// recheck quorum
				//
				need_quorum_recheck = true;
			}
			return;
		}
	}
	// not found
	ds_list_mutex.unlock();

}

//
// routines called on the secondaries by ccr_repl_prov
// during checkpoints and service membership changes
//

// dump_state:
// checkpoint all HA objects to new secondary
void
manager_impl::dump_state(ccr_trans::ckpt_ptr sec_ckpt)
{
	uint_t			i;
	uint_t			num_tables;
	table_info		*ti;
	table_info		*ti_dir;
	ds_info_t		*ds_infop;
	mgr_cluster_info	*clinfop;
	table_info		*ti_cl_dir = NULL;
	Environment		e;
	CORBA::type_info_t	*typ;
	int			ccr_checkpoint_version;

	CCR_DBG(("CCR TM: in dump_state\n"));
	//
	// The exceptions, if any, from all the checkpoint methods below
	// are cleared before returning, because the only exception that
	// can be returned is COMM_FAILURE (XXX should assert on this)
	// because the secondary being added died. This can be ignored.
	//
	ccr_checkpoint_version = ccr_trans::ckpt::_version(sec_ckpt);
	CCR_DBG(("CCR TM: checkpoint version = %d\n", ccr_checkpoint_version));

	//
	// If IDL version of the manager does not have a corresponding
	// version of CCR checkpoint interface, this has to change.
	//
	typ = ccr_trans::manager::_get_type_info(ccr_checkpoint_version);

	// checkpoint manager
	ccr_trans::manager_var mgr_ref = this->get_objref(typ);
	sec_ckpt->ckpt_create_manager(mgr_ref, e);
	if (e.exception()) {
		e.clear();
		return;
	}

	//
	// A snapshot of the ds_info_list list is not necessary
	// here, since the primary is in frozen state, and that
	// guarantees that it will not receive any _unreferenced
	// and registrations, ie there can be no concurrent
	// changes to ds_info_list. Also, transaction operations
	// will also be blocked, so there can be no accesses
	// (even reads) for this list. So there is no need to
	// protect the following block with ds_list_mutex's
	// lock-unlock.
	// Since no _unreferenced can be delivered, get_objref()
	// below also does not need to be protected with locks.
	//
	for (ds_info_list.atfirst(); (ds_infop = ds_info_list.get_current())
	    != NULL; ds_info_list.advance()) {
		ccr_trans::handle_var handlep = ds_infop->hand->get_objref();
		sec_ckpt->ckpt_reg_data_server(ds_infop->dsp, ds_infop->ccr_id,
		    handlep, ds_infop->deactivated, e);
		if (e.exception()) {
			e.clear();
			return;
		}
	}

	// checkpoint directory entries and consis info
	if (recovery_done) {
		cl_mutex.lock();
		for (cluster_list.atfirst(); (clinfop =
		    cluster_list.get_current()) != NULL;
		    cluster_list.advance()) {
			num_tables = 0;
			//
			// Leave room for directory and cluster_directory
			// in rec sequence.
			//
			if (clinfop->directory_elems.seq != NULL) {
				num_tables =
				    clinfop->directory_elems.seq->length();
			}
			ccr_data::consis_seq	rec_seq(num_tables + 2,
			    num_tables + 2);
			for (i = 0; i < num_tables; i++) {
				char *tname =
				    (*clinfop->directory_elems.seq)[i].key;
				ti = clinfop->lookup_table(tname);
				ASSERT(ti);
				rec_seq[i] = ti->rec;
				ti->rele();
			}
			ti_dir = clinfop->lookup_table(DIR);
			ASSERT(ti_dir);
			rec_seq[num_tables] = ti_dir->rec;

			if (is_default_cluster(clinfop->clusterp)) {
				if (ccr_checkpoint_version >= 1) {
					ti_cl_dir =
					    clinfop->lookup_table(CLUSTER_DIR);
					ASSERT(ti_cl_dir);
					rec_seq[num_tables + 1] =
					    ti_cl_dir->rec;
					sec_ckpt->ckpt_recovery_done(
					    *(clinfop->directory_elems.seq),
					    highest_epoch, rec_seq, e);
				} else {
					//
					// We are sending checkpoint
					// message to a node running
					// the old version. It does not
					// know about cluster_directory.
					//
					ccr_data::consis_seq
					    rec_seq_old(num_tables + 1,
					    num_tables + 1);
					for (i = 0; i <= num_tables; i++) {
						rec_seq_old[i] = rec_seq[i];
					}
					sec_ckpt->ckpt_recovery_done(
					    *(clinfop->directory_elems.seq),
					    highest_epoch, rec_seq_old, e);
				}
			} else {
				ASSERT(ccr_checkpoint_version >= 1);
				if (num_tables > 0) {
					sec_ckpt->ckpt_recovery_done_zc(
					    clinfop->cluster_id,
					    clinfop->clusterp,
					    num_tables,
					    *(clinfop->directory_elems.seq),
					    rec_seq, e);
				} else {
					ccr::element_seq dummy_seq(1, 1);
					sec_ckpt->ckpt_recovery_done_zc(
					    clinfop->cluster_id,
					    clinfop->clusterp,
					    num_tables, dummy_seq, rec_seq,
					    e);
				}
			}
			if (e.exception()) {
				e.clear();
				ti_dir->rele();
				if (is_default_cluster(clinfop->clusterp)) {
					ti_cl_dir->rele();
				}
				cl_mutex.unlock();
				return;
			}
			//
			// checkpoint transactions in progress
			//
			for (i = 0; i < num_tables; i++) {
				char *tname =
				    (*clinfop->directory_elems.seq)[i].key;
				ti = clinfop->lookup_table(tname);
				ASSERT(ti);
				dump_trans(sec_ckpt, clinfop->clusterp, tname,
				    ti, e);
				ti->rele();
			}
			dump_trans(sec_ckpt, clinfop->clusterp, DIR, ti_dir, e);
			ti_dir->rele();

			if (is_default_cluster(clinfop->clusterp) &&
			    ccr_checkpoint_version >= 1) {
				dump_trans(sec_ckpt, DEFAULT_CLUSTER,
				    CLUSTER_DIR, ti_cl_dir, e);
				ti_cl_dir->rele();
			}
		}
		cl_mutex.unlock();
		//
		// Checkpoint the global recovery status of the CCR.
		//
		if (ccr_checkpoint_version >= 1) {
			sec_ckpt->ckpt_all_zc_recovery_done(e);
		}
	}
}

//
// utility function to dump transactions
//
void
manager_impl::dump_trans(ccr_trans::ckpt_ptr sec_ckpt, const char *cluster,
    const char *tname, table_info *ti, Environment& e)
{
	table_trans_impl	*transp;
	table_trans_impl	*old_tr;

	ASSERT(!is_cluster_dir(tname) || ccr_trans::ckpt::_version(sec_ckpt)
	    >= 1);
	//
	// Dump the old transaction objects if any. We don't need to lock since
	// we know that the service is frozen now and there is nothing
	// going on. Note that we need to be careful to dump the old
	// transactions first before dumping the current transaction.
	//
	SList<table_trans_impl>::ListIterator iter(ti->old_trans_list);
	while ((old_tr = iter.get_current()) != NULL) {
		dump_transaction(sec_ckpt, cluster, tname, old_tr, e);
		CL_PANIC(e.exception() == NULL);
		iter.advance();
	}

	if ((transp = ti->running_trans) == NULL) {
		return;
	}
	dump_transaction(sec_ckpt, cluster, tname, transp, e);
	if (e.exception()) {
		e.clear();
		return;
	}
	//
	// If this is a directory table, For directory operations, checkpoint
	// what table is being created or removed. A directory op can only be
	// an ADD or a REMOVE. Not an UPDATE or REMOVE_ALL since ccr idl
	// interface has methods only for create_table & remove_table.
	//
	if (transp->is_directory_create()) {
		ASSERT(is_dir(transp->table_name));
		if (is_default_cluster(transp->cluster_name)) {
			if (is_cluster_dir(tname)) {
				sec_ckpt->zc_ckpt_op(ccr::CCR_ADD,
				    transp->user_cluster_id,
				    transp->user_cluster, e);
			} else {
				sec_ckpt->ckpt_directory_op(ccr::CCR_ADD,
				    transp->user_table, e);
			}
		} else {
			sec_ckpt->ckpt_directory_op_zc(ccr::CCR_ADD,
			    transp->cluster_name, transp->user_table, e);
		}
	} else if (transp->is_directory_remove()) {
		ASSERT(is_dir(transp->table_name));
		if (is_default_cluster(transp->cluster_name)) {
			if (is_cluster_dir(tname)) {
				ASSERT(ccr_trans::ckpt::_version(sec_ckpt)
				    >= 1);
				sec_ckpt->zc_ckpt_op(ccr::CCR_REMOVE,
				    transp->user_cluster_id,
				    transp->user_cluster, e);
			} else {
				sec_ckpt->ckpt_directory_op(ccr::CCR_REMOVE,
				    transp->user_table, e);
			}
		} else {
			sec_ckpt->ckpt_directory_op_zc(ccr::CCR_REMOVE,
			    transp->cluster_name, transp->user_table, e);
		}
	}
	//
	// If the transaction's delete flag is set, that needs to be ckpted
	// also. This ckpt need not be done for the old (waiting to be deleted)
	// transactions since their cleanup work has been done. The delete flag
	// can be set on a directory table, which means that a cluster remove
	// operation is in progress.
	//
	if (transp->is_table_delete()) {
		if (is_default_cluster(transp->cluster_name)) {
			ASSERT(!is_cluster_dir(tname));
			sec_ckpt->ckpt_table_delete(transp->table_name, e);
		} else {
			sec_ckpt->ckpt_table_delete_zc(transp->cluster_name,
			    transp->table_name, e);
		}
	}
	CL_PANIC(e.exception() == NULL);
}

void
manager_impl::dump_transaction(ccr_trans::ckpt_ptr sec_ckpt,
				const char *cluster,
				const char *tname,
				table_trans_impl *transp,
				Environment &e)
{
	ccr::updatable_table_var ut_ref = transp->get_objref();
	uint_t num_utc = transp->num_copies;
	ccr_data::updatable_copy_seq utcs(num_utc, num_utc);
	for (uint_t j = 0; j < num_utc; j++)
		utcs[j] = ccr_data::updatable_table_copy::_duplicate(
		    transp->copies[j]);
	if (is_default_cluster(cluster)) {
		sec_ckpt->ckpt_create_transaction(ut_ref, tname, utcs,
		    transp->local_ccr_ids,
		    (transp->transaction_status ==
		    table_trans_impl::TRANS_END), e);
	} else {
		sec_ckpt->ckpt_create_transaction_zc(ut_ref, cluster, tname,
		    utcs, transp->local_ccr_ids,
		    (transp->transaction_status ==
		    table_trans_impl::TRANS_END), e);
	}
	CL_PANIC(e.exception() == NULL);
}

bool
manager_impl::is_primary()
{
	return (we_are_primary);
}

void
manager_impl::set_primary_status(bool val)
{
	we_are_primary = val;
}

// called by ccr_repl_prov::freeze_primary()
void
manager_impl::freeze()
{
	uint_t		num_tables = 0;
	table_info	*ti;
	mgr_cluster_info *clinfop;

	//
	// block incoming transactions
	//
	we_are_frozen = true;
	freeze_called = true;
	//
	// unblock threads waiting to start registration or transaction
	//
	reg_info->unblock_waiters();
	//
	// avoid race condition with recovery
	// if recovery not complete, tables can not be locked
	//
	if (!recovery_done) {
		//
		// We still need to quiesce the threadpool
		//
		ASSERT(the_ccr_threadpool);
		CCR_DBG(("CCR TM: Recovery not complete,"
		    "Quiesce the CCR threadpool\n"));
		the_ccr_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
		CCR_DBG(("CCR TM: CCR threadpool quiesced\n"));
		return;
	}
	//
	// Unblock threads waiting to lock tables
	// they will return with retry exception
	//
	cl_mutex.lock();
	for (cluster_list.atfirst(); (clinfop = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		num_tables = 0;
		if (clinfop->directory_elems.seq != NULL) {
			num_tables = clinfop->directory_elems.seq->length();
		}
		for (uint_t i = 0; i < num_tables; i++) {
			char *tname = (*clinfop->directory_elems.seq)[i].key;
			ti = clinfop->lookup_table(tname);
			ASSERT(ti);
			ti->unblock_waiters();
			ti->rele();
		}
		ti = clinfop->lookup_table(DIR);
		ASSERT(ti);
		ti->unblock_waiters();
		ti->rele();
		if (is_default_cluster(clinfop->clusterp)) {
			ti = clinfop->lookup_table(CLUSTER_DIR);
			ASSERT(ti);
			ti->unblock_waiters();
			ti->rele();
		}
	}
	cl_mutex.unlock();

	//
	// wait till the transactions on the threadpool are quiesced.
	//
	ASSERT(the_ccr_threadpool);
	CCR_DBG(("CCR TM: quiesce the CCR threadpool\n"));
	the_ccr_threadpool->quiesce(threadpool::QBLOCK_EMPTY);
	CCR_DBG(("CCR TM: CCR threadpool quiesced\n"));
}

//
// Return the cluster information from the list of clusters.
//
mgr_cluster_info*
manager_impl::lookup_cluster_info(const char *cluster)
{
	mgr_cluster_info	*clinfop = NULL;
	mgr_cluster_info	*cltmp = NULL;

	cl_mutex.lock();
	for (cluster_list.atfirst(); (cltmp = cluster_list.get_current())
	    != NULL; cluster_list.advance()) {
		if (os::strcmp(cluster, cltmp->clusterp) == 0) {
			// Match found
			clinfop = cltmp;
			break;
		}
	}
	cl_mutex.unlock();
	return (clinfop);
}

// called by ccr_repl_prov::unfreeze_primary()
void
manager_impl::unfreeze()
{
	//
	// Unblock incoming transactions
	//
	we_are_frozen = false;
	//
	// There was no preceding call to freeze, This has become the
	// the primary. Hence it is not required to resume the tasks on
	// the threadpool.
	//
	if (!freeze_called) {
		return;
	}
	//
	// Resume processing of tasks on the threadpool if there
	// are any.
	//
	ASSERT(the_ccr_threadpool);
	the_ccr_threadpool->unblock_processing();
	freeze_called = false;
}

bool
manager_impl::is_frozen()
{
	return (we_are_frozen);
}

// Checkpoint accessor function.
ccr_trans::ckpt_ptr
manager_impl::get_checkpoint()
{
	return ((ccr_repl_prov*)(get_provider()))->get_checkpoint_ccr_trans();
}


//
// manhandle methods
//

// constructor for primary
manhandle::manhandle(ccr_repl_prov *serverp, manager_impl *mgr,
			ccr_data::data_server_ptr dsp):
	mc_replica_of<ccr_trans::handle>(serverp),
	the_manager(mgr)
{
	the_dsp = ccr_data::data_server::_duplicate(dsp);
}

// constructor for secondary
manhandle::manhandle(ccr_trans::handle_ptr objp, manager_impl *mgr,
			ccr_data::data_server_ptr dsp):
	mc_replica_of<ccr_trans::handle>(objp),
	the_manager(mgr)
{
	the_dsp = ccr_data::data_server::_duplicate(dsp);
}

manhandle::~manhandle()
{
	CCR_DBG(("CCR TM: in manhandle destructor\n"));
	CORBA::release(the_dsp);
	the_dsp = ccr_data::data_server::_nil();
	the_manager = NULL;
}


//
// manhandle::_unreferenced
// Called when the data server holding the handle reference dies
// On the primary, calls remove_ds_info to remove the objref.
//
void
manhandle::_unreferenced(unref_t cookie)
{
	CCR_DBG(("CCR TM: in manhandle _unreferenced\n"));

	//
	// no need to protect last_unref since
	// only register_data_server() can call get_objref()
	// in its retry code, but the HA framework guarantees
	// that _unreferenced will not be called concurrently
	// with that.
	//
	// The if(_last_unref()) check is necessary because an object
	// reference may have been given out by dump_state()
	//
	if (!_last_unref(cookie))
		return;

	the_manager->remove_ds_info(the_dsp, false);
	delete this;
}

// Checkpoint accessor function.
ccr_trans::ckpt_ptr
manhandle::get_checkpoint()
{
	return ((ccr_repl_prov*)(get_provider()))->get_checkpoint_ccr_trans();
}

//
// ds_info_t methods
//
// If the ds_info_t is created inside the create_ds_snapshot(),
// the "hand" field should be set as NULL since this field
// should never be used from the local copy (since it can
// become invalid if an _unreferenced is called releasing
// the handle, leaving the hand ptr invalid).
//
ds_info_t::ds_info_t(ccr_data::data_server_ptr newdsp,
	quorum::ccr_id_t newccr_id, manhandle *newhand, bool is_deactivated):
	_SList::ListElem(this),
	ccr_id(newccr_id),
	hand(newhand),
	deactivated(is_deactivated)
{
	dsp = ccr_data::data_server::_duplicate(newdsp);
}

ds_info_t::~ds_info_t()
{
	CORBA::release(dsp);
	hand = NULL;
}

//
// Constructor
//
mgr_cluster_info::mgr_cluster_info(uint_t clid,
    const char *cluster_name) : _SList::ListElem(this)
{
	ASSERT(cluster_name);
	CCR_DBG(("CCR TM: create cluster info for %s with id %d\n",
	    cluster_name, clid));
	cluster_id = clid;
	clusterp = os::strdup((char *)cluster_name);
	directory_elems.seq = NULL;
	recovery_done = false;
}

//
// Destructor
//
mgr_cluster_info::~mgr_cluster_info()
{
	uint_t	indx;
	uint_t	num_tables;
	char	*tname;
	table_info	*ti;
	table_info	*ti_dir;

	if (directory_elems.seq != NULL) {
		num_tables = directory_elems.seq->length();
		for (indx = 0; indx < num_tables; indx++) {
			tname = (*directory_elems.seq)[indx].key;
			ti = tinfo_hashtab.remove(tname);
			ASSERT(ti);
			ti->rele();
		}
		delete directory_elems.seq;
	}
	ti_dir = tinfo_hashtab.remove(DIR);
	if (ti_dir != NULL) {
		ti_dir->rele();
	}
	delete [] clusterp;
}

//
// Return the table_info structure from the hashtble
//
table_info *
mgr_cluster_info::lookup_table(const char *tname)
{
	table_info *ti = NULL;
	tinfo_mutex.lock();
	ti = tinfo_hashtab.lookup(tname);
	if (ti != NULL) {
		ti->hold();
	}
	tinfo_mutex.unlock();
	return (ti);
}

//
// Add a new table to the cluster.
//
void
mgr_cluster_info::add_incore_table(const char *tname)
{
	// add tinfo entry to hashtab
	table_info *tinfo;
	tinfo = new table_info();
	ASSERT(tinfo);

	//
	// assert this will not block
	//
	tinfo_mutex.lock();
	tinfo->rec.valid = true;	// mark table valid for updates
	tinfo_hashtab.add(tinfo, tname);

	// add table to directory_elems
	directory_elems.add_element(tname);
	tinfo_mutex.unlock();
}

//
// Remove a table from the cluster.
//
void
mgr_cluster_info::remove_incore_table(const char *tname)
{
	table_info *ti;

	//
	// remove tinfo entry from hashtab
	//
	tinfo_mutex.lock();
	ti = tinfo_hashtab.remove(tname);
	ASSERT(ti);
	ti->rele();

	// remove entry from directory_elems
	directory_elems.remove_element(tname);
	tinfo_mutex.unlock();
}

//
// table_info methods
//
table_info::table_info():
	running_trans(NULL),
	in_use(false),
	to_remove(false),
	num_waiting(0)
{
	rec.valid = false;
}

table_info::~table_info()
{
	CCR_DBG(("CCR TM: in table_info destructor\n"));
}

bool
table_info::lock_writes()
{
	manager_impl *mgr = the_service->mgr;
	mutex.lock();
	// signal that a writer is waiting, so if primary is frozen,
	// manager will know to unblock us
	num_waiting++;
	while (in_use || mgr->is_frozen()) {
		if (mgr->is_frozen()) {
			num_waiting--;
			mutex.unlock();
			return (false);
		}
		cv.wait(&mutex);
	}
	//
	// If table is removed, we dont' want that it appears in use any
	// more. All threads waiting in this routine must exit with
	// exception in begin_transaction_common above.
	//
	if (!to_remove) {
		in_use = true;
	}
	num_waiting--;
	mutex.unlock();
	return (true);
}

void
table_info::unlock_writes(bool remove_table)
{
	mutex.lock();
	CL_PANIC(in_use);
	in_use = false;
	//
	// If table is removed, we want to wake up
	// all threads waiting in lock_writes().
	//
	if (remove_table) {
		to_remove = true;
		cv.broadcast();
	} else {
		cv.signal();
	}
	mutex.unlock();
}

void
table_info::unblock_waiters()
{
	mutex.lock();
	if (num_waiting > 0 && in_use)
		cv.broadcast();
	mutex.unlock();
}

void
table_info::transaction_done(table_trans_impl *tr)
{
	mutex.lock();
	CL_PANIC(tr != NULL);
	CL_PANIC(running_trans == NULL || running_trans == tr);
	old_trans_list.append(tr);
	running_trans = NULL;
	mutex.unlock();
}

void
table_info::delete_transaction(table_trans_impl *tr)
{
	mutex.lock();
#ifdef DEBUG
	bool ret = old_trans_list.erase(tr);
	CL_PANIC(ret);
#else
	(void) old_trans_list.erase(tr);
#endif
	mutex.unlock();
}

regis_info::regis_info(manager_impl *the_mgr):
	registration_pending(false),
	num_transactions(0),
	num_waiting(0),
	mgr(the_mgr) { }

regis_info::~regis_info()
{
	CCR_DBG(("in regis_info destructor\n"));
}

bool
regis_info::begin_transaction()
{
	progress_mutex.lock();
	// signal that we are waiting, so if primary is frozen,
	// manager will know to unblock us
	num_waiting++;
	// wait for registrations in progress to finish
	while (registration_pending || mgr->is_frozen()) {
		if (mgr->is_frozen()) {
			num_waiting--;
			progress_mutex.unlock();
			return (false);
		}
		progress_cv.wait(&progress_mutex);
	}
	// suspend incoming registrations
	num_transactions++;
	CCR_DBG(("CCR TM: regis_info::begin_trans, num = %d\n",
	    num_transactions));
	num_waiting--;
	progress_mutex.unlock();
	return (true);
}

void
regis_info::end_transaction()
{
	progress_mutex.lock();
	CL_PANIC(num_transactions > 0);
	num_transactions--;
	CCR_DBG(("CCR TM: regis_info::end_trans, num = %d\n",
	    num_transactions));
	// enable registrations if all transactions are done
	if ((num_transactions == 0) && (registration_pending)) {
		progress_cv.signal();
	}
	progress_mutex.unlock();
}

bool
regis_info::begin_registration()
{
	progress_mutex.lock();

	// signal that we are waiting, so if primary is frozen,
	// manager will know to unblock us
	num_waiting++;
	// wait for registrations in progress to finish
	// or abort if primary becomes frozen
	while (registration_pending || mgr->is_frozen()) {
		if (mgr->is_frozen()) {
			num_waiting--;
			progress_mutex.unlock();
			return (false);
		}
		progress_cv.wait(&progress_mutex);
	}
	// suspend incoming transactions and registrations
	registration_pending = true;

	// wait for outstanding transactions to finish
	// or abort if primary becomes frozen
	while (num_transactions > 0 || mgr->is_frozen()) {
		if (mgr->is_frozen()) {
			registration_pending = false;
			num_waiting--;
			progress_mutex.unlock();
			return (false);
		}
		progress_cv.wait(&progress_mutex);
	}
	num_waiting--;
	progress_mutex.unlock();
	return (true);
}

void
regis_info::end_registration()
{
	progress_mutex.lock();

	// enable transactions and registrations
	registration_pending = false;
	progress_cv.broadcast();
	progress_mutex.unlock();
}

void
regis_info::unblock_waiters()
{
	progress_mutex.lock();
	if (num_waiting > 0 && (registration_pending || num_transactions > 0))
		progress_cv.broadcast();
	progress_mutex.unlock();
}
