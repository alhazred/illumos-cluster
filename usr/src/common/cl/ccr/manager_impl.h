/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  manager_impl.h - CCR Transaction Manager
 *
 */

#ifndef _MANAGER_IMPL_H
#define	_MANAGER_IMPL_H

#pragma ident	"@(#)manager_impl.h	1.34	08/08/07 SMI"

#include <sys/os.h>
#include <sys/refcnt.h>
#include <sys/list_def.h>
#include <h/ccr_trans.h>
#include <ccr/ccr_repl_prov.h>
#include <ccr/common.h>
#include <repl/service/replica_tmpl.h>

// macro for determining if an exception indicates that the DS is gone
#define	DS_UNREACHABLE(ex) (CORBA::COMM_FAILURE::_exnarrow((ex)))

// forward decls
class table_info;
class regis_info;
class manager_impl;
class table_trans_impl;

//
// manhandle: one object per data server
// object is held onto by DS so that the Transaction Manager
// receives an _unreferenced if the DS dies
//

class manhandle :
	public mc_replica_of<ccr_trans::handle>
{
	friend class manager_impl;
public:
	// constructor for primary
	manhandle(ccr_repl_prov *, manager_impl *, ccr_data::data_server_ptr);
	// constructor for secondary
	manhandle(ccr_trans::handle_ptr, manager_impl *,
		ccr_data::data_server_ptr);
	~manhandle();

	void _unreferenced(unref_t);

	// Checkpoint accessor function.
	ccr_trans::ckpt_ptr	get_checkpoint();

protected:
	// store pointers to manager and data server
	manager_impl *the_manager;
	ccr_data::data_server_ptr the_dsp;
};

//
// ds_info: info associated with a registered data server. Used by
// manager_impl and table_trans_impl
//
class ds_info_t : public _SList::ListElem {
	friend class manager_impl;
	friend class table_trans_impl;
public:
	ds_info_t(ccr_data::data_server_ptr, quorum::ccr_id_t,
	    manhandle *, bool);
	~ds_info_t();

private:
	ccr_data::data_server_ptr	dsp;
	quorum::ccr_id_t		ccr_id;
	manhandle			*hand;
	// mark if the node is dead or will die soon
	bool				deactivated;
};

typedef IntrList<ds_info_t, _SList> ds_info_list_t;

//
// mgr_cluster_info: Information associated with a virtual cluster
// or the default global cluster.
//
class mgr_cluster_info : public _SList::ListElem {
	friend class manager_impl;
public:
	mgr_cluster_info(uint_t, const char *);
	~mgr_cluster_info();
	table_info *lookup_table(const char *);
	void add_incore_table(const char *);
	void remove_incore_table(const char *);
private:
	uint_t		cluster_id;
	char		*clusterp;

	// directory table elements.
	string_hashtable_t<table_info *>	tinfo_hashtab;
	dir_elem_seq				directory_elems;
	os::mutex_t				tinfo_mutex;
	bool					recovery_done;
};

typedef IntrList<mgr_cluster_info, _SList> cluster_list_t;

// Transaction Manager
// Responsible for serializing updates to tables
// One TM per cluster (HA replica server)
class manager_impl :
	public mc_replica_of<ccr_trans::manager>
{
	friend class ccr_repl_prov;
	friend class table_trans_impl;
	friend class update_state;
	friend class commit_state;
	friend class reg_state;
	friend class manhandle;
	friend class regis_info;

	//
	// Public interface
	//
public:
	// constructor for primary
	manager_impl(ccr_repl_prov *);
	// constructor for secondary
	manager_impl(ccr_trans::manager_ptr);

	~manager_impl();

	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//
	ccr::updatable_table_ptr
	cl_begin_transaction(const char *, const char *, Environment &);

	ccr::updatable_table_ptr
	begin_transaction(const char *, Environment &);

	ccr::updatable_table_ptr
	begin_transaction_zc(const char *, const char *, Environment &);

	ccr::updatable_table_ptr
	begin_transaction_invalid_ok(const char *, Environment &);

	ccr::updatable_table_ptr
	begin_transaction_invalid_ok_zc(const char *, const char *,
	    Environment &);

	ccr_trans::handle_ptr register_data_server(ccr_data::data_server_ptr,
					quorum::ccr_id_t,
					Environment &);
	bool is_frozen();

	// Checkpoint interface.
	ccr_trans::ckpt_ptr	get_checkpoint();

protected:
	table_info*	lookup_table(const char *cluster, const char *tname);
	bool add_ds_info(ccr_data::data_server_ptr, quorum::ccr_id_t,
				manhandle *, bool);
	void remove_ds_info(ccr_data::data_server_ptr, bool mark);
	void create_table_int(uint_t, const char *, const char *);
	void remove_table_int(const char *, const char *);

	void dump_state(ccr_trans::ckpt_ptr);
	void dump_transaction(ccr_trans::ckpt_ptr, const char *,
				const char *, table_trans_impl *,
				Environment &);
	void ckptwork_recovery_done(const ccr::element_seq &,
				ccr_data::epoch_type,
				const ccr_data::consis_seq &);
	void ckptwork_recovery_done_zc(uint_t, const char *,
				uint_t, const ccr::element_seq &,
				const ccr_data::consis_seq &);

	void ckptwork_all_zc_recovery_done();

	void ckptwork_reg_data_server(ccr_data::data_server_ptr,
				quorum::ccr_id_t,
				ccr_trans::handle_ptr,
				bool,
				Environment &);
	void ckptwork_create_transaction(ccr::updatable_table_ptr,
				const char *,
				const char *,
				const ccr_data::updatable_copy_seq &,
				const quorum::ccr_id_list_t &,
				bool,
				Environment &);
	void convert_spare_transactions(void);
	void convert_spare_recovery(void);
	void convert_spare_data_servers(void);

	void set_repl_prov(ccr_repl_prov *);
	bool is_primary();
	void set_primary_status(bool);
	// called by ccr_repl_prov during state transitions
	void freeze();
	void unfreeze();

	// make a "snapshot" copy of ds_info_seq.
	void create_ds_snapshot(ds_info_list_t &);

	// sequence of data server info
	ds_info_list_t			ds_info_list;

	regis_info			*reg_info;
	quorum::quorum_algorithm_var	quorum_obj;
	bool				recovery_done;

private:
	// private methods
	void common_constructor();
	bool check_quorum();
	void do_recovery(Environment &);
	void recover_table(mgr_cluster_info *, const char *, const char *,
	    const ccr_data::epoch_type *, Environment &);
	bool propagate_data(ccr_data::data_server_ptr);
	bool propagate_table(ccr_data::data_server_ptr,
			ccr_data::data_server_ptr, const char *, const char *,
			bool &);

	ccr::updatable_table_ptr *begin_transaction_impl(const char *,
	    Environment &);
	ccr::updatable_table_ptr begin_transaction_common(const char *,
	    const char *, bool, Environment &);
	mgr_cluster_info* lookup_cluster_info(const char *);
	void dump_trans(ccr_trans::ckpt_ptr, const char *, const char *,
	    table_info *, Environment&);
	void checkpoint_cluster_info(Environment &e);
	ccr_data::consis* get_table_consis(const char *, const char *,
	    ds_info_t **, ds_info_list_t&, uint_t&, Environment&);
	void save_cluster_info(ccr::element_seq *, const ds_info_t *,
	    Environment &);


	ccr_repl_prov		*repl_svr;
	ccr_data::epoch_type	highest_epoch;
	bool			we_have_quorum;
	bool			we_are_primary;
	bool			we_are_frozen;
	bool			need_quorum_recheck;

	// cluster list
	cluster_list_t		cluster_list;

	// Lock to protect cluster list.
	os::mutex_t		cl_mutex;

	// lock to protect ds_info_list
	os::mutex_t		ds_list_mutex;
	//
	// used to decide if unblock processing of the threadpool
	// is needed.
	//
	bool			freeze_called;
};

//
// table_info: a state object used by the TM to serialize updates to tables
// one table_info object per table
//
class table_info : public refcnt
{
	friend class manager_impl;
public:
	table_info();
	~table_info();
	bool lock_writes();
	void unlock_writes(bool remove_table);
	void unblock_waiters();

	// The current active transaction is done.
	void transaction_done(table_trans_impl *);
	// The specified transaction is being destroyed.
	void delete_transaction(table_trans_impl *);

	ccr_data::consis rec;	// consistency info
	// transaction in progress on this table (at most one)
	table_trans_impl *running_trans;

protected:
	bool		in_use;
	bool		to_remove;
private:
	uint_t		num_waiting;

	// The old transactions that are done and cleaned up, but not deleted
	// yet.
	SList<table_trans_impl> old_trans_list;

	os::mutex_t	mutex;
	os::condvar_t	cv;
};


//
// regis_info: a state object used by the TM to track how many transactions
// are in progress and how many DS registrations are pending
// one regis_info object per TM
//
class regis_info
{
	friend class manager_impl;
public:
	regis_info(manager_impl *);
	~regis_info();
	bool begin_transaction();
	void end_transaction();
	bool begin_registration();
	void end_registration();
	void unblock_waiters();

protected:
	bool registration_pending;
	uint_t num_transactions;

private:
	uint_t num_waiting;
	os::mutex_t progress_mutex;
	os::condvar_t progress_cv;
	manager_impl *mgr;
};

//
// transaction_state objects
//

class simple_state : public transaction_state {
public:
	void committed();
	void orphaned(Environment &);
};

class reg_state : public transaction_state {
public:
	reg_state(manager_impl *);
	~reg_state();
	void committed();
	void orphaned(Environment &);

	manager_impl *the_mgr;
};

#endif	/* _MANAGER_IMPL_H */
