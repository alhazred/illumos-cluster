/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  common.h - common code for DS and TM
 *
 */

#ifndef _CCR_COMMON_H
#define	_CCR_COMMON_H

#pragma ident	"@(#)common.h	1.17	08/07/24 SMI"

#include <h/ccr.h>
#include <ccr/ccr_checksum.h>
#include <orb/debug/haci_debug.h>

#include <orb/infrastructure/orb_conf.h>
#include <sys/clconf_int.h>

// Syslogging
extern os::sc_syslog_msg ccr_syslog_msg;

#if defined(HACI_DBGBUFS) && !defined(NO_CCR_DBGBUF)
#define	CCR_DBGBUF
#endif

#ifdef CCR_DBGBUF
extern dbg_print_buf ccr_dbg;
#define	CCR_DBG(args) HACI_DEBUG(ENABLE_CCR_DBG, ccr_dbg, args)
#else
#define	CCR_DBG(args)
#endif

#define	DEFAULT_CLUSTER		"global"
#define	CLUSTER_DIR		"cluster_directory"
#define	DIR			"directory"

//
// This data structure is used for building a table for supporting the
// mapping from version protocol spec file version number to the various
// IDL interface versions it represents. The table will be a two
// dimensional array indexed by major/minor vp version.
//
typedef struct {
	int data_server_vers; // Data server Interface ccr_data::data_server
	int ccr_manager_vers; // CCR Manager Interface ccr_trans::manager
	int ccr_ckpt_vers;	// CCR checkpoint Interface ccr_trans::ckpt
} ccr_version_map_t;

const int	CCR_VP_MAX_MAJOR = 2;
const int	CCR_VP_MAX_MINOR = 0;

//
// dir_elem_seq: sequence of table elements whose members can be
// added or removed dynamically
//
class dir_elem_seq {
	//
	// Public interface
	//
public:
	void add_element(const char *key);
	void remove_element(const char *key);
	void add_elements(const ccr::element_seq &elems);
	void remove_elements(ccr::element_seq *elems);

	ccr::element_seq *seq;
};

//
// Common function to check if cluster access is allowed.
//
bool check_cluster_access(const char *cluster, const CORBA::Environment &env);
char *get_cluster_from_env(const CORBA::Environment &env);

//
// Common functions.
//

//
// Check if the table is a cluster directory table.
//
inline bool
is_cluster_dir(const char *table_name)
{
	ASSERT(table_name);
	if (os::strcmp(table_name, CLUSTER_DIR) == 0) {
		return (true);
	}
	return (false);
}

//
// Check if the table is a directory table.
//
inline bool
is_dir(const char *table_name)
{
	ASSERT(table_name);
	if (os::strcmp(table_name, DIR) == 0) {
		return (true);
	}
	return (false);
}

//
// Check if the virtual cluster is actually the global cluster.
//
inline bool
is_default_cluster(const char *cluster)
{
	ASSERT(cluster);
	if (os::strcmp(cluster, DEFAULT_CLUSTER) == 0) {
		return (true);
	}
	return (false);
}

#endif	/* _CCR_COMMON_H */
