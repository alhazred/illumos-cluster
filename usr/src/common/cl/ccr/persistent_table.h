/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  persistent_table.h - persistent storage objects
 *
 */

#ifndef _PERSISTENT_TABLE_H
#define	_PERSISTENT_TABLE_H

#pragma ident	"@(#)persistent_table.h	1.18	08/12/17 SMI"

#ifndef _KERNEL
#include <stdio.h>
#else
#include <sys/kobj.h>
#include <sys/kobj_impl.h>
#include <sys/vnode.h>
#include <sys/file.h>
#endif // def KERNEL

#include <sys/param.h>
#include <ccr/ccr_checksum.h>
#include <sys/clconf_int.h>

// Syslog message
#define	CCR_SERVICE_TAG "Cluster.CCR"

#define	INIT_VERSION	"-1"
#define	OVRD_VERSION	"-2"

// Location of the split-brain CCR change flag/file
#define	SPLIT_BRAIN_CHANGE_FILE	"/etc/cluster/.split_brain_ccr_change"

#define	CLUSTER_DIRECTORY_TABLE "/etc/cluster/ccr/cluster_directory"

// useful macro
#define	is_meta_row(row) (bcmp((row), "ccr_", (size_t)4) == 0)

struct table_element_t {
	char *key;
	char *data;

	table_element_t();
	~table_element_t();
};

// readonly_persistent_table: class that does physical I/O
// used by readonly_table_impl
// exists in both the READONLY and READWRITE CCR modules
class readonly_persistent_table {
public:
	readonly_persistent_table();
	~readonly_persistent_table();
	int initialize(const char *);
	void atfirst();
	table_element_t *next_element(int &);
	void close();
	int compute_checksum(uchar_t *);

private:

	// private methods
	char *next_row(int &);
	int next_char(int &);

#ifndef _KERNEL
	FILE *ifs;
#else
	struct _buf *bpfile;
	uint_t offset;
#endif
	char *table_pathname;
	int eof_reached;
};

// updatable_persistent_table: class that does physical I/O
// used by updatable_table_impl
// does not exist in the READONLY CCR module

class updatable_persistent_table {
public:
	updatable_persistent_table();
	~updatable_persistent_table();
	int initialize(const char *);
	int insert_element(const char *, const char *);
	int insert_element(const table_element_t &);
	int insert_element(table_element_t *);
	// void insert_elements(const ccr::element_seq &, Environment &);
	int close();

private:
#ifndef _KERNEL
	FILE *ofs;
#else
	struct vnode *vp;
	offset_t offset;
#endif
	char *table_pathname;
};

class ccrlib {
public:
	// utility functions used by ccradm
	static int initialize_ccr_table(const char *, const char *);

#ifdef WEAK_MEMBERSHIP
	//
	// The following three functions are only used under the weak
	// membership model.
	//
#ifndef _KERNEL
	static int mark_ccr_as_winner();
	static int mark_ccr_as_loser();
#endif // _KERNEL
	static bool split_brain_ccr_change_exists();
#endif // WEAK_MEMBERSHIP
};
#endif	/* _PERSISTENT_TABLE_H */
