//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// table_access.cc - Implementation of methods of table_access class.
//

#pragma ident	"@(#)table_access.cc	1.5	08/07/17 SMI"

#include <ccr/common.h>
#include <ccr/table_access.h>

//
// Methods for data access class
//

table_access::table_access():
	num_readers(0),
	write_held(false),
	writer_waiting(false),
	recovered(false)
{
	// refcnt gets initialized to 1
	rec.valid = false;
}

table_access::~table_access()
{
	// clear list, releasing callback objrefs along the way
	ccr::callback_ptr cbptr;
	while ((cbptr = callbacks.reapfirst()) != nil) //lint !e64
		CORBA::release(cbptr);
}

void
table_access::begin_reader()
{
	mutex.lock();
	// wait for writer to finish
	while (writer_waiting || write_held)
		cv.wait(&mutex);
	num_readers++;
	mutex.unlock();
}

void
table_access::end_reader()
{
	mutex.lock();
	CL_PANIC(num_readers > 0);
	num_readers--;
	if (num_readers == 0 && writer_waiting)
		//
		// Unblock writer
		// Bug 4346043: There may also be
		// readers waiting on this table,
		// so need to do a broadcast instead
		// of a signal.
		//
		cv.broadcast();
	mutex.unlock();
}

void
table_access::begin_writer()
{
	mutex.lock();
	// cant have two writers
	CL_PANIC(!write_held && !writer_waiting);
	writer_waiting = true;
	// wait for readers to finish
	while (num_readers > 0) {
		cv.wait(&mutex);
	}
	writer_waiting = false;
	write_held = true;
	mutex.unlock();
}

void
table_access::end_writer()
{
	mutex.lock();
	write_held = false;
	// unblock readers
	cv.broadcast();
	mutex.unlock();
}

bool
table_access::add_callback(ccr::callback_ptr cbptr)
{
	ccr::callback_ptr cbdup;
	ccr::callback_ptr this_cb;

	cb_mutex.lock();
	// check if callback is already registered
	callbacks.atfirst();
	while ((this_cb = callbacks.get_current()) != nil) {
		if (cbptr->_equiv(this_cb)) {
			cb_mutex.unlock();
			return (false);
		}
		callbacks.advance();
	}
	// duplicate the passed objref and add to list
	cbdup = ccr::callback::_duplicate(cbptr);
	callbacks.append(cbdup);
	cb_mutex.unlock();
	return (true);
}

bool
table_access::remove_callback(ccr::callback_ptr cbptr)
{
	ccr::callback_ptr this_cb;
	bool was_found = false;

	// remove the passed objref from list
	cb_mutex.lock();
	callbacks.atfirst();
	while ((this_cb = callbacks.get_current()) != nil) {
		if (cbptr->_equiv(this_cb)) {
			CORBA::release(this_cb);
			(void) callbacks.erase(this_cb);
			was_found = true;
			//
			// No need to advance the list here as the erase()
			// already advances the _current pointer.
			//
		} else {
			callbacks.advance();
		}
	}
	cb_mutex.unlock();
	return (was_found);
}

ccr_data::callback_seq *
table_access::get_callbacks()
{
	// convert list to sequence
	ccr::callback_ptr this_cb;
	uint_t i, num_cbs;
	cb_mutex.lock();
	num_cbs = callbacks.count();
	ccr_data::callback_seq *cb_seq =
		new ccr_data::callback_seq(num_cbs, num_cbs);
	callbacks.atfirst();
	i = 0;
	while ((this_cb = callbacks.get_current()) != nil) {
		(*cb_seq)[i++] = ccr::callback::_duplicate(this_cb);
		callbacks.advance();
	}
	cb_mutex.unlock();
	return (cb_seq);
}

void
table_access::flag_recovered(const ccr_data::consis &truth_rec)
{
	mutex.lock();
	// sync local rec with the truth_rec
	rec.version = truth_rec.version;
	rec.override = truth_rec.override;
	rec.valid = truth_rec.valid;

	recovered = true;
	mutex.unlock();
}

bool
table_access::was_recovered()
{
	bool wasrec = false;
	mutex.lock();
	wasrec = recovered;
	mutex.unlock();
	return (wasrec);
}

void
table_access::call_did_recovery(const char *cluster, const char *tname)
{
	ccr::callback_ptr	cbptr;
	Environment		e;
	CORBA::Exception	*ex;

	cb_mutex.lock();
	CCR_DBG(("CCR DS : in call_did_recovery\n"));
	callbacks.atfirst();
	while ((cbptr = callbacks.get_current()) != nil) {
		if (is_default_cluster(cluster)) {
			cbptr->did_recovery(tname, ccr::CCR_RECOVERED, e);
		} else {
			cbptr->did_recovery_zc(cluster, tname,
			    ccr::CCR_RECOVERED, e);
		}
		if ((ex = e.exception()) != NULL) {
		// check if userland object has died
			if (CORBA::COMM_FAILURE::_exnarrow(ex) ||
			    CORBA::INV_OBJREF::_exnarrow(ex)) {
				CORBA::release(cbptr);
				(void) callbacks.erase(cbptr);
				e.clear();
				// erase() moves the current pointer to the
				// next element, no need to advance() at this
				// point.
				continue;
			} else {
				CCR_DBG(("CCR DS : did_recovery returned "
				    "exception\n"));
			}
			e.clear();
		}
		callbacks.advance();
	}
	cb_mutex.unlock();
}

void
table_access::call_did_update(const char *cluster, const char *tname,
    ccr::ccr_update_type ccr_op)
{
	ccr::callback_ptr	cbptr;
	Environment		e;
	CORBA::Exception	*ex;

	cb_mutex.lock();
	CCR_DBG(("CCR DS : in call_did_update\n"));
	callbacks.atfirst();
	while ((cbptr = callbacks.get_current()) != nil) {
		if (is_default_cluster(cluster)) {
			cbptr->did_update(tname, ccr_op, e);
		} else {
			cbptr->did_update_zc(cluster, tname, ccr_op, e);
		}
		if ((ex = e.exception()) != NULL) {
		// check if userland object has died
			if (CORBA::COMM_FAILURE::_exnarrow((ex)) ||
			    CORBA::INV_OBJREF::_exnarrow((ex))) {
				CORBA::release(cbptr);
				(void) callbacks.erase(cbptr);
				e.clear();
				// erase() moves the current pointer to the
				// next element. no need to advance() at
				// this point.
				continue;
			} else {
				CCR_DBG(("CCR DS : did_update returned "
				    "exception\n"));
			}
			e.clear();
		}
		callbacks.advance();
	}
	cb_mutex.unlock();
}
