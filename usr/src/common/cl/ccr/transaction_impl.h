/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  transaction_impl.h - CCR transaction object
 *
 */

#ifndef _TRANSACTION_IMPL_H
#define	_TRANSACTION_IMPL_H

#pragma ident	"@(#)transaction_impl.h	1.25	08/07/17 SMI"

#include <h/ccr_trans.h>
#include <ccr/ccr_repl_prov.h>
#include <ccr/manager_impl.h>
#include <repl/service/replica_tmpl.h>

// forward decls
class update_state;
class commit_state;
class abort_state;

// CCR threadpool
extern threadpool* the_ccr_threadpool;

// internal data structure
struct update_info {
	ccr::update_type op;
	const char *key;
	const char *data;
	const ccr::element_seq *seq;
};

//
// Transaction object for a table
// created on the fly by transaction manager when user
// invokes begin_transaction
//
class table_trans_impl :
	public mc_replica_of<ccr::updatable_table>
{
	//
	// class used for scheduling work in _unreferenced processing
	//
	class unref_task : public defer_task
	{
	public:
		unref_task(table_trans_impl *, bool);
		~unref_task();
		void execute();
	private:
		ccr::updatable_table_ptr transp;
		bool need_to_commit;
	};

	friend class manager_impl;
	friend class ccr_repl_prov;
	friend class update_state;
	friend class commit_state;
	friend class abort_state;
	friend class unref_task;

	//
	// Public interface
	//
public:
	// constructor for primary
	table_trans_impl(ccr_repl_prov *, manager_impl *, const char *,
	    const char *);
	// constructor for secondary
	table_trans_impl(ccr::updatable_table_ptr, manager_impl *,
	    const char *, const char *, const ccr_data::updatable_copy_seq &,
	    const quorum::ccr_id_list_t &);
	~table_trans_impl();
	void common_constructor(const char *, const char *);
	void _unreferenced(unref_t);
	void startup(bool, Environment &);
	bool is_directory_trans();
	bool is_directory_create();
	bool is_directory_remove();
	bool is_table_delete();
	void set_modified();
	void set_cluster_op(const ccr::update_type, uint_t,
	    const char *);
	void set_directory_op(const ccr::update_type, const char *);
	void set_table_delete();
	void cleanup(bool);

	//
	// Methods that implement the IDL interface
	//
	void add_element(const char *, const char *, Environment &);
	void add_elements(const ccr::element_seq &, Environment &);
	void update_element(const char *, const char *, Environment &);
	void remove_element(const char *, Environment &);
	void remove_elements(const ccr::element_seq &, Environment &);
	void remove_all_elements(Environment &);
	void commit_transaction(Environment &);
	void abort_transaction(Environment &);
	void set_format_version(int32_t, Environment &);

	// Checkpoint accessor function
	ccr_trans::ckpt_ptr	get_checkpoint();

protected:
	// array of tables to update
	uint_t num_copies;
	ccr_data::updatable_table_copy_var *copies;
	table_info *tinfo;
	char *table_name;
	char *cluster_name;
	char *user_table;	// table added or removed in directory
	uint_t user_cluster_id;
	char *user_cluster;

	//
	// checkpoint work routines
	//
	void ckptwork_start_update(Environment &);
	void ckptwork_start_commit(Environment &);
	void ckptwork_start_abort(Environment &);
	void ckptwork_trans_modified();
	// these are not instance-specific
	static void ckptwork_prepared_update(Environment &);
	static void ckptwork_rollback_update(Environment &);
	static void ckptwork_abort_update(Environment &);
	static void ckptwork_abort_transaction(Environment &);

private:
	//
	// private methods
	//
	void do_update(update_info *, Environment &);
	void prepare_update_copies(update_info *, Environment &);
	void rollback_update_copies(Environment &);
	void commit_update_copies(Environment &);
	void commit_transaction_copies(Environment &);
	void abort_transaction_copies(Environment &);
	void clear_transaction_copies(Environment &);
	void unlock_copies(Environment &);
	bool check_update_quorum();
	bool check_cluster_shutdown();

	// completion status used for sanity checking in case user commits
	// a transaction without doing updates, or invokes any operations
	// on a transaction that has already been committed or aborted
	enum trans_status_t {
		TRANS_START, TRANS_MODIFIED, TRANS_FAILED, TRANS_END
	} transaction_status;

	manager_impl *trans_mgr;
	bool *copy_avail;
	ccr::update_type directory_op;
	bool delete_flag;	// delete table on successful commit

	// local copy of DS ccr_ids at the time this transaction was started
	quorum::ccr_id_list_t local_ccr_ids;

	// Disallow assignments and pass by value
	table_trans_impl(const table_trans_impl &);
	table_trans_impl &operator = (table_trans_impl &);
};

//
// transaction_state objects
//

class update_state : public transaction_state {
public:
	enum tx_state { START, PREPARED, FAILED_PREPARE, FAILED_COMMIT,
			COMMITTED, ABORTED };

	update_state(table_trans_impl *);
	~update_state();

	void committed();
	void orphaned(Environment &);

	tx_state progress;
	table_trans_impl *transp;	// object of invocations
};

class commit_state : public transaction_state {
public:
	enum tx_state { START, FAILED, COMMITTED, ABORTED };

	commit_state(table_trans_impl *);
	~commit_state();

	void committed();
	void orphaned(Environment &);

	tx_state progress;
	table_trans_impl *transp;	// object of invocations
};

class abort_state : public transaction_state {
public:
	enum tx_state { START, ABORTED };

	abort_state(table_trans_impl *);
	~abort_state();

	void committed();
	void orphaned(Environment &);

	tx_state progress;
	table_trans_impl *transp;	// object of invocations
};
#endif	/* _TRANSACTION_IMPL_H */
