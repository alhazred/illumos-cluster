//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// updatable_copy_impl.cc - updatable table object
//

#pragma ident	"@(#)updatable_copy_impl.cc	1.59	08/12/17 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <h/naming.h>
#include <h/cmm.h>
#include <nslib/ns.h>
#include <ccr/updatable_copy_impl.h>
#include <ccr/ds_cluster_info.h>
#include <ccr/persistent_table.h>
#include <cmm/cmm_impl.h>
#include <cmm/cmm_ns.h>
#ifndef _KERNEL
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#include <orb/fault/fault_injection.h>

updatable_copy_impl::updatable_copy_impl():
	cluster_id(0),
	cluster_name(NULL),
	table_name(NULL),
	basepath(NULL),
	origpath(NULL),
	commitpath(NULL),
	intentpath(NULL),
	newtab_name(NULL),
	newdir_name(NULL),
	remtab_name(NULL),
	remdir_name(NULL),
	data_svr(nil),
	tacc(NULL),
	transaction_status(START),
	last_op(ccr::CCR_INVOP)
{
}

updatable_copy_impl::~updatable_copy_impl()
{
	delete [] cluster_name;
	delete [] table_name;
	delete [] origpath;
	delete [] commitpath;
	delete [] intentpath;
	delete [] newtab_name;
	delete [] newdir_name;
	delete [] remtab_name;
	delete [] remdir_name;
	delete [] basepath;
	data_svr = NULL;

	ASSERT(tacc != NULL);
	tacc->rele();
	tacc = NULL;
}

void
#ifdef  DEBUG
updatable_copy_impl::_unreferenced(unref_t cookie)
#else
updatable_copy_impl::_unreferenced(unref_t)
#endif
{
	ASSERT(_last_unref(cookie));
	CCR_DBG(("in updatable_copy_impl::_unreferenced\n"));
	// XXX need to keep track of state here
	// if primary TM dies midway through transaction,
	// secondary will retry
	delete this;
}

int
updatable_copy_impl::initialize(const char *dirpath, const char *czname,
				const char *tname, table_access *ta,
				data_server_impl *ds, bool invalid_ok)
{
	readonly_persistent_table orig_table;
	updatable_persistent_table commit_table;
	table_element_t *telp;
	int retval;

	if (is_cluster_dir(tname)) {
		basepath = os::strdup(dirpath);
	} else {
		basepath = new char[os::strlen(dirpath) + os::strlen(czname)
		    + 2];
		os::sprintf(basepath, "%s/%s", dirpath, czname);
	}
	table_name = new char[os::strlen(tname) + 1];
	(void) os::strcpy(table_name, tname);
	set_table_pathname(&origpath, tname);
	commitpath = new char[os::strlen(origpath) + 8];
	intentpath = new char[os::strlen(origpath) + 8];
	os::sprintf(commitpath, "%s.commit", origpath);
	os::sprintf(intentpath, "%s.intent", origpath);

	tacc = ta;
	data_svr = ds;
	cluster_id = 0;
	cluster_name = os::strdup(czname);

	// copy table to table.commit, omitting meta entries
	// get gennum from table

	if ((retval = commit_table.initialize(commitpath)) != 0) {
		return (retval);
	} else if ((retval = orig_table.initialize(origpath)) != 0) {
		if ((retval == ENOENT) && invalid_ok) {
			//
			// The table was deleted outside the control
			// of cluster software. We will create an
			// empty table instead since we lost the
			// original table. We still want this process
			// to be able to continue since it is usefull
			// when begin_transaction_invalid_ok tries to
			// recover some missing tables, for example:
			// data_server_impl::update_invalid_table.
			//
			if ((retval = commit_table.close()) != 0) {
				return (retval);
			}
			transaction_status = START;
			return (retval);
		} else {
			(void) commit_table.close();
			return (retval);
		}
	}

	// read all elements from src, write to dest
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// skip metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = commit_table.insert_element(telp)) != 0) {
				orig_table.close();
				(void) commit_table.close();
				delete telp;
				return (retval);
			}
		}
		delete telp;
	}
	orig_table.close();

	if ((retval = commit_table.close()) != 0) {
		return (retval);
	}

	transaction_status = START;

	return (retval);
}

void
updatable_copy_impl::add_element(const char *, const char *,
			Environment &)
{
	// This function shouldn't get called
	CL_PANIC(0);
}

void
updatable_copy_impl::add_elements(const ccr::element_seq &elements,
    Environment &e)
{
	//
	// Check for orphaned invocations. We need this check because
	// we need to be sure that we never process any request from
	// a dead TM after we process requests from the new TM. The lock
	// is used to make sure that once a check returns false, the
	// request will be processed to completion before any other
	// request is processed.
	//
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	table_element_t *telp = NULL;
	readonly_persistent_table commit_table;
	updatable_persistent_table intent_table;
	int retval;
	unsigned int i, j;
	const char *keyi;
	const char *keyj;

	CCR_DBG(("CCR DS: Adding %d key(s) to table %s\n", elements.length(),
	    table_name));

	FAULTPT_CCR(FAULTNUM_CCR_ADDELEMENT_BEGIN_1,
	    FaultFunctions::generic);

	if (is_retry(ccr::CCR_ADD)) {
		lck.unlock();
		return;
	}

	// Check to make sure there are no double keys passed in
	// Note that this is an O(N^2) test.  If this turns out to
	// be a performance problem for large tables, a couple solutions are:
	// a) Sort the list, O(N log N), to check for duplicates
	// b) Eliminate the test, and count on the caller to make sure
	// there are no duplicate entries (i.e. #ifdef DEBUG).
	for (i = 0; i < elements.length(); i++) {
		CCR_DBG(("CCR DS: key %s\n", (const char *)elements[i].key));
		for (j = i+1; j < elements.length(); j++) {
			//
			// Check if either key string
			// is NULL before passing them to strcmp.
			//
			keyi = elements[i].key;
			keyj = elements[j].key;
			if ((keyi == NULL) || (keyj == NULL)) {
				e.exception(new ccr::system_error(
				    orb_conf::node_number(), EINVAL));
				return;
			}

			if (os::strcmp(elements[i].key, elements[j].key) == 0) {
				CCR_DBG(("CCR DS: Double key %s\n",
				    (const char *)elements[i].key));
				e.exception(new ccr::key_exists(
				    (const char *)elements[i].key));
				lck.unlock();
				return;
			}
		}
	}

	// open rpt table.commit, upt table.intent
	if ((retval = commit_table.initialize(commitpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}
	if ((retval = intent_table.initialize(intentpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		commit_table.close();
		lck.unlock();
		return;
	}

	// read all elements from rpt, write to upt
	while ((telp = commit_table.next_element(retval)) != NULL) {
		if (is_meta_row(telp->key)) {
			// skip meta entries
			delete telp;
			continue;
		}
		// check for duplicate key
		for (i = 0; i < elements.length(); i++) {
			if (os::strcmp(telp->key, elements[i].key) == 0) {
				e.exception(new ccr::key_exists(
					(const char *)telp->key));
				delete telp;
				commit_table.close();
				(void) intent_table.close();
				lck.unlock();
				return;
			}
		}

		if ((retval = intent_table.insert_element(telp)) != 0) {
			delete telp;
			break;
		}
		delete telp;
	}

	commit_table.close();	// done reading

	// did we exit loop prematurely?
	if (retval) {
		// XXX remove intent and commit files
		// or maybe rollback_update will do this
		(void) intent_table.close();
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}

	// write new elements to upt
	for (i = 0; i < elements.length(); i++) {
		if ((retval = intent_table.insert_element(elements[i].key,
		    elements[i].data)) != 0) {
			(void) intent_table.close();
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			lck.unlock();
			return;
		}
	}

	// done writing
	if ((retval = intent_table.close()) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}

	//
	// if table_name == directory, create the new table
	// if table_name == cluster_directory, create the new cluster
	//
	if (is_dir(table_name) || is_cluster_dir(table_name)) {
		//
		// Adding multiple elements at once to directory poses
		// more atomicity problems than is worth dealing with at
		// this time.
		//
		CL_PANIC(elements.length() == 1);
		updatable_persistent_table newtab_table;
		char metastr[CKSM_HEX_LEN + 1];
		char *newtab_path;

		//
		// If this is the cluster_directory table, then we are adding
		// a new cluster. We then create a new directory
		// /etc/cluster/ccr/<cluster>. We then go ahead and create
		// an empty directory table for the virtual cluster.
		//
		if (is_cluster_dir(table_name)) {
			char *newdir_path;
			newtab_name = os::strdup(DIR);
			newdir_name = os::strdup((const char *)elements[0].key);
			cluster_id = os::atoi(elements[0].data);
			ASSERT(newdir_name);
			ASSERT(newtab_name);
			unsigned long len = os::strlen(basepath) +
			    os::strlen(elements[0].key) +
			    os::strlen(DIR) + 2;
			newtab_path = new char[len];
			ASSERT(newtab_path);
			os::sprintf(newtab_path, "%s/%s/%s", basepath,
			    newdir_name, DIR);
			set_table_pathname(&newdir_path, newdir_name);

			os::rdir zcdir;
			os::dirent_t *dp;
			if (!zcdir.open(newdir_path)) {
				zcdir.close();
				retval = os::create_dir(newdir_path);
				if (retval) {
					CCR_DBG(("CCR DS: Creating directory "
					    "for the zone cluster %s failed\n",
					    newdir_path));
					delete [] newdir_path;
					delete [] newtab_path;
					e.exception(new ccr::system_error(
					    orb_conf::node_number(), retval));
					lck.unlock();
					return;
				}
			} else {
				//
				// The directory for the zone cluster
				// already exists. We make sure here
				// that there are no files in this
				// directory.
				//
				char filepath[PATH_MAX];
				while ((dp = zcdir.read()) != NULL) {
					if (os::strcmp(dp->d_name, ".") == 0 ||
					    os::strcmp(dp->d_name, "..") == 0) {
						continue;
					}
					retval = os::snprintf(filepath,
					    (size_t)PATH_MAX, "%s/%s",
					    newdir_path, dp->d_name);
					if (retval > PATH_MAX) {
						// error.
						CCR_DBG(("CCR DS: retval = "
						    "%d > PATH_MAX\n", retval));
						delete [] newdir_path;
						delete [] newtab_path;
						e.exception(new
						    ccr::system_error(
						    orb_conf::node_number(),
						    retval));
						zcdir.close();
						lck.unlock();
						return;
					}
					retval = os::file_unlink(filepath);
					if (retval != 0) {
						// error.
						CCR_DBG(("CCR DS: Failed to "
						    "remove %s, ret = %d\n",
						    filepath, retval));
						delete [] newdir_path;
						delete [] newtab_path;
						// error
						e.exception(new
						    ccr::system_error(
						    orb_conf::node_number(),
						    retval));
						zcdir.close();
						lck.unlock();
						return;
					}
				}
			}
			delete [] newdir_path;
			zcdir.close();
		} else {
			newtab_name = new char[os::strlen(elements[0].key) + 1];
			ASSERT(newtab_name);
			(void) os::strcpy(newtab_name, elements[0].key);
			set_table_pathname(&newtab_path, elements[0].key);
		}

		retval = newtab_table.initialize(newtab_path);
		delete [] newtab_path;
		if (retval) {
			// XXX should roll back update
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			lck.unlock();
			return;
		}

		//
		// write meta entries
		//
		// generate checksum, version = 0
		if ((retval = newtab_table.insert_element("ccr_gennum", "0"))
		    != 0) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			(void) newtab_table.close();
			lck.unlock();
			return;
		}
		init_ccr_checksum(metastr);
		if ((retval = newtab_table.insert_element("ccr_checksum",
		    metastr)) != 0) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			(void) newtab_table.close();
			lck.unlock();
			return;
		}

		if ((retval = newtab_table.close()) != 0) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), retval));
			lck.unlock();
			return;
		}
	}

	transaction_status = PREPARED;
	last_op = ccr::CCR_ADD;

	lck.unlock();

	CCR_DBG(("CCR DS: Added key(s) to table %s\n", table_name));
}

void
updatable_copy_impl::remove_element(const char *, Environment &)
{
	CL_PANIC(0);
}

void
updatable_copy_impl::remove_elements(const ccr::element_seq &elements,
    Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	table_element_t *telp;
	readonly_persistent_table commit_table;
	updatable_persistent_table intent_table;
	bool key_was_found = false;
	unsigned int keys_found = 0;
	int retval;
	const char *keyi;

	FAULTPT_CCR(FAULTNUM_CCR_REMOVEELEMENT_BEGIN_1,
	    FaultFunctions::generic);

	if (is_retry(ccr::CCR_REMOVE)) {
		lck.unlock();
		return;
	}

	// open rpt table.commit, upt table.intent
	if ((retval = commit_table.initialize(commitpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}
	if ((retval = intent_table.initialize(intentpath)) != 0) {
		commit_table.close();
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}

	// read all elements from rpt, write to upt
	while ((telp = commit_table.next_element(retval)) != NULL) {
		if (is_meta_row(telp->key)) {
			// skip meta entries
			delete telp;
			continue;
		}
		key_was_found = false;
		for (unsigned int i = 0; i < elements.length(); i++) {
			//
			// look for matching key.
			// Check if either elements[i] or telp->key
			// is NULL before passing it to strcmp.
			//
			keyi = elements[i].key;
			if ((keyi == NULL) || (telp->key == NULL)) {
				e.exception(new ccr::system_error(
				    orb_conf::node_number(), EINVAL));
				return;
			}
			if (os::strcmp(telp->key, elements[i].key) == 0) {
				key_was_found = true;
				break;
			}
		}
		if (key_was_found) {
			// skip matching key
			keys_found++;
		} else {
			if ((retval = intent_table.insert_element(telp)) != 0) {
				delete telp;
				break;
			}
		}
		delete telp;
	}
	commit_table.close();
	if ((intent_table.close() != 0) || (retval != 0)) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}

	// did we find all keys to remove?
	if (keys_found != elements.length()) {
		// workaround for memory leak in 4236225
		// e.exception(new ccr::no_such_key(""));
		e.exception(new ccr::no_such_key());
		lck.unlock();
		return;
	}

	//
	// if table_name == directory, mark the table for removal
	//
	if (is_dir(table_name) || is_cluster_dir(table_name)) {
		//
		// Don't handle removal of multiple keys from directory
		// because of atomicity concerns.
		//
		CL_PANIC(elements.length() == 1);
		if (is_cluster_dir(table_name)) {
			remtab_name = os::strdup(DIR);
			remdir_name = os::strdup(elements[0].key);
			ASSERT(remdir_name);
		} else {
			remtab_name = os::strdup(elements[0].key);
		}
		ASSERT(remtab_name);
	}

	transaction_status = PREPARED;
	last_op = ccr::CCR_REMOVE;

	lck.unlock();

	CCR_DBG(("CCR DS: Removed keys from table %s\n", table_name));
}

void
updatable_copy_impl::update_element(const char *key, const char *new_data,
    Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	table_element_t *telp;
	readonly_persistent_table commit_table;
	updatable_persistent_table intent_table;
	int retval;
	bool key_found = false;

	FAULTPT_CCR(FAULTNUM_CCR_UPDATEELEMENT_BEGIN_1,
	    FaultFunctions::generic);

	if (is_retry(ccr::CCR_UPDATE)) {
		lck.unlock();
		return;
	}

	// open rpt table.commit, upt table.intent
	if ((retval = commit_table.initialize(commitpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}
	if ((retval = intent_table.initialize(intentpath)) != 0) {
		commit_table.close();
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}

	// read all elements from rpt, write to upt
	while ((telp = commit_table.next_element(retval)) != NULL) {
		if (is_meta_row(telp->key)) {
			// skip meta entries
			delete telp;
			continue;
		}
		//
		// replace data in matching key.
		// Check if either key or telp->key is NULL before
		// passing it to strcmp.
		//
		if ((key == NULL) || (telp->key == NULL)) {
			e.exception(new ccr::system_error(
			    orb_conf::node_number(), EINVAL));
			return;
			}

		if (os::strcmp(telp->key, key) == 0) {
			key_found = true;
			retval = intent_table.insert_element(telp->key,
			    new_data);
		} else {
			retval = intent_table.insert_element(telp);
		}
		delete telp;

		if (retval)
			break;
	}

	commit_table.close();
	if ((intent_table.close() != 0) || (retval != 0)) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	} else if (!key_found) {
		// workaround for memory leak in 4236225
		// e.exception(new ccr::no_such_key(key));
		e.exception(new ccr::no_such_key());
		lck.unlock();
		return;
	}

	transaction_status = PREPARED;
	last_op = ccr::CCR_UPDATE;

	lck.unlock();

	CCR_DBG(("CCR DS: Updated key %s in table %s\n",
		key, table_name));
}

void
updatable_copy_impl::remove_all_elements(Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	updatable_persistent_table intent_table;
	int retval;

	FAULTPT_CCR(FAULTNUM_CCR_REMOVEALLELEMENTS_BEGIN_1,
	    FaultFunctions::generic);

	if (is_retry(ccr::CCR_REMOVE_ALL)) {
		lck.unlock();
		return;
	}

	// create empty intent file
	if ((retval = intent_table.initialize(intentpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}
	if ((retval = intent_table.close()) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}

	transaction_status = PREPARED;
	last_op = ccr::CCR_REMOVE_ALL;

	lck.unlock();

	CCR_DBG(("CCR DS: Removed all keys from table %s\n",
		table_name));
}

void
updatable_copy_impl::commit_update(Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	readonly_persistent_table intent_table;
	updatable_persistent_table commit_table;
	int retval;
	uchar_t cksm_digest[DIGEST_LEN];
	char metastr[CKSM_HEX_LEN + 1];
	table_element_t *telp;

	FAULTPT_CCR(FAULTNUM_CCR_COMMITUPDATE_BEGIN_1,
	    FaultFunctions::generic);

	// handle retry
	if (transaction_status != PREPARED) {
		CL_PANIC(transaction_status == COMMITTED);
		lck.unlock();
		return;
	}

	if ((retval = intent_table.initialize(intentpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		lck.unlock();
		return;
	}
	if ((retval = commit_table.initialize(commitpath)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		intent_table.close();
		lck.unlock();
		return;
	}

	// write meta entries

	//
	// insert new gennum (cur gennum + 1)
	//
	os::sprintf(metastr, "%ld\0", tacc->rec.version + 1);
	if ((retval = commit_table.insert_element("ccr_gennum", metastr))
	    != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		intent_table.close();
		(void) commit_table.close();
		lck.unlock();
		return;
	}
	//
	// insert ccr_checksum
	//
	if ((retval = intent_table.compute_checksum(cksm_digest)) != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		intent_table.close();
		(void) commit_table.close();
		lck.unlock();
		return;
	}
	encode_ccr_checksum(cksm_digest, metastr);
	if ((retval = commit_table.insert_element("ccr_checksum", metastr))
	    != 0) {
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		intent_table.close();
		(void) commit_table.close();
		lck.unlock();
		return;
	}

	//
	// copy data entries
	//
	intent_table.atfirst();
	while ((telp = intent_table.next_element(retval)) != NULL) {
		if ((retval = commit_table.insert_element(telp)) != 0) {
			delete telp;
			break;
		}
		delete telp;
	}
	intent_table.close();

	if ((commit_table.close() != 0) || (retval != 0))
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));

	// remove intent table
	(void) os::file_unlink(intentpath);

	transaction_status = COMMITTED;
	lck.unlock();
}

void
updatable_copy_impl::rollback_update(Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	// remove intent file
	(void) os::file_unlink(intentpath);

	if (is_dir(table_name) || is_cluster_dir(table_name)) {
		//
		// Clean up whatever we started
		//
		if (newtab_name) {
			char *remtab_path;
			//
			// delete the new table file if it is already created
			//
			if (is_cluster_dir(table_name)) {
				ASSERT(newdir_name);
				unsigned long len = os::strlen(basepath) +
				    os::strlen(DIR) +
				    os::strlen(newdir_name) + 2;
				remtab_path = new char[len];
				ASSERT(remtab_path);
				os::sprintf(remtab_path, "%s/%s/%s", basepath,
				    newdir_name, DIR);
			} else {
				set_table_pathname(&remtab_path, newtab_name);
			}
			(void) os::file_unlink(remtab_path);
			if (is_cluster_dir(table_name)) {
				char *remdir_path;
				set_table_pathname(&remdir_path, newdir_name);
				(void) os::rm_dir(remdir_path);
				delete [] remdir_path;
				delete [] newdir_name;
				newdir_name = NULL;
			}
			delete [] remtab_path;
			delete [] newtab_name;
			newtab_name = NULL;
		} else if (remtab_name) {
			delete [] remtab_name;
			remtab_name = NULL;
		}
	}

	transaction_status = COMMITTED;
	lck.unlock();
}

void
updatable_copy_impl::commit_transaction(Environment &e)
{
	Environment	env;
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	CCR_DBG(("CCR DS: In commit trans %s %s\n", cluster_name, table_name));
	char *bakpath;
	size_t len;

	//
	// check state
	//
	switch (transaction_status) {
	case COMMITTED:		// normal case
		break;
	case DONE:
	case UNLOCKED:		// this is a retry
		lck.unlock();
		return;
	case START:
	case PREPARED:
	default:		// all other states are invalid
		CL_PANIC(0);
	}

	tacc->begin_writer();
	// backup the original file
	CL_PANIC(origpath != NULL);
	len = strlen(origpath) + strlen(BAKSUFFIX) + 1;
	bakpath = new char [len];
	(void) os::snprintf(bakpath, len, "%s%s", origpath, BAKSUFFIX);
	int retval = os::file_link(origpath, bakpath);
	if (retval != 0) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Could not backup the CCR table %s errno = %d.",
		    origpath, retval);
	}
	delete [] bakpath;

	// rename commit table to orig table
	// this makes the transaction persistent
	retval = os::file_rename(commitpath, origpath);

	// we do NOT call end_writer() here because the TM must know that
	// a quorum has committed before unlocking the table for reads

	if (retval) {
		//
		// SCMSGS
		// @explanation
		// The CCR data server failed to update the indicated table.
		// @user_action
		// There may be other related messages on this node, which may
		// help diagnose the problem. If the root file system is full
		// on the node, then free up some space by removing
		// unnecessary files. If the root disk on the afflicted node
		// has failed, then it needs to be replaced.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "CCR: Failed to update table %s.", table_name);
		CCR_DBG(("CCR DS: commit_transaction failed for table %s\n",
		    table_name));
	}

	// increment gennum so readers get invalidated
	++tacc->rec.version;
	if (!tacc->rec.valid) {
		// version should already be 0 when we incremented above
		// but make it 0 for sure.
		tacc->rec.version = 0;
		tacc->rec.valid = true;
	}

	if (is_dir(table_name) || is_cluster_dir(table_name)) {
		if (newtab_name) {
			// already created table file
			if (is_cluster_dir(table_name)) {
				data_svr->create_table_zc(cluster_id,
				    newdir_name, newtab_name, env);
			} else {
				if (is_default_cluster(cluster_name)) {
					data_svr->create_table(newtab_name,
					    env);
				} else {
					//
					// Creating a table.
					// Hence cluster id is -1
					//
					data_svr->create_table_zc(0,
					    cluster_name, newtab_name, env);
				}
			}
			env.clear();
		} else if (remtab_name) {
			//
			// remove the table file and table.bak file
			// table was locked by TM
			//
			char *remtab_path;
			char *remtabbak_path;
			unsigned long length;

			if (is_cluster_dir(table_name)) {
				length = os::strlen(basepath) +
				    os::strlen(DIR) +
				    os::strlen(remdir_name) + 2;
				remtab_path = new char[length];
				os::sprintf(remtab_path, "%s/%s/%s", basepath,
				    remdir_name, DIR);
				ASSERT(remtab_path);
			} else {
				set_table_pathname(&remtab_path, remtab_name);
			}
			length = strlen(remtab_path) + strlen(BAKSUFFIX) + 1;
			remtabbak_path = new char [length];
			(void) os::snprintf(remtabbak_path, length, "%s%s",
			    remtab_path, BAKSUFFIX);
			CCR_DBG(("CCR DS: Removing %s %s\n", remtab_path,
			    remtabbak_path));
			(void) os::file_unlink(remtab_path);
			(void) os::file_unlink(remtabbak_path);
			delete [] remtab_path;
			delete [] remtabbak_path;
			if (is_cluster_dir(table_name)) {
				data_svr->remove_table_zc(remdir_name,
				    remtab_name, env);
			} else {
				if (is_default_cluster(cluster_name)) {
					data_svr->remove_table(remtab_name,
					    env);
				} else {
					data_svr->remove_table_zc(cluster_name,
					    remtab_name, env);
				}
			}
			env.clear();
		}
	}
#if	defined(WEAK_MEMBERSHIP) && defined(_KERNEL)
	//
	// If the commit took place during split-brain under weak membership,
	// create the split-brain change file.
	//
	if (weak_membership_is_used() && is_split_brain()) {
		int error = os::file_create(SPLIT_BRAIN_CHANGE_FILE);
		if (error == 0 || error == EEXIST) {
			CCR_DBG(("CCR DS: Successful commit of table %s during"
			    " split brain\n", table_name));
		} else {
			CCR_DBG(("CCR DS: file_create returned %d while "
			    "committing %s\n", error, table_name));
			//
			// SCMSGS
			// @explanation
			// The CCR data server failed to create the file to
			// indicate that there has been a CCR update during
			// split brain under the weak-membership model.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
			    "CCR: Could not create split brain CCR change "
			    "flag.");
		}
	}
#endif // WEAK_MEMBERSHIP && _KERNEL

	transaction_status = DONE;
	lck.unlock();

	CCR_DBG(("CCR DS: committed transaction on table %s\n",
	    table_name));
}

// The only difference between abort_transaction() and clear_transaction()
// is the log msg, which is needed to avoid confusion during debugging.
// Could not just use a flag to abort_transaction, since this class inherits
// from the ccr::updatable_table whose abort_transaction() method does not
// allow another arg.
void
updatable_copy_impl::abort_transaction(Environment &e)
{
	undo_transaction(e);
	CCR_DBG(("CCR DS: aborted transaction on table %s\n",
	    table_name));
}

void
updatable_copy_impl::clear_transaction(Environment &e)
{
	undo_transaction(e);
	CCR_DBG(("CCR DS: committed transaction on table %s\n",
	    table_name));
}

void
updatable_copy_impl::undo_transaction(Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	// handle retry
	// maybe should assert here?
	if (transaction_status == UNLOCKED) {
		lck.unlock();
		return;
	}

	//
	// remove intent and commit files
	//
	(void) os::file_unlink(intentpath);
	(void) os::file_unlink(commitpath);

	if (is_dir(table_name) || is_cluster_dir(table_name)) {
		if (newtab_name) {
			// delete the new table file
			char *remtab_path;
			if (is_cluster_dir(table_name)) {
				char *remdir_path;
				ASSERT(newdir_name);
				unsigned long len = os::strlen(basepath) +
				    os::strlen(DIR) +
				    os::strlen(newdir_name) + 2;
				remtab_path = new char[len];
				ASSERT(remtab_path);
				os::sprintf(remtab_path, "%s/%s/%s", basepath,
				    newdir_name, DIR);
				set_table_pathname(&remdir_path, newdir_name);
				(void) os::rm_dir(remdir_path);
				delete [] remdir_path;
				(void) os::file_unlink(remtab_path);
			} else {
				set_table_pathname(&remtab_path, newtab_name);
				(void) os::file_unlink(remtab_path);
			}
			delete [] remtab_path;
		} else if (is_dir(table_name)) {
			//
			// The cluster is going away. Remove the cluster
			// directory.
			//
			(void) os::rm_dir(basepath);
		}
	}

	//
	// unlock table
	//
	tacc->end_writer();
	transaction_status = UNLOCKED;
	lck.unlock();
}

void
updatable_copy_impl::lock(Environment &)
{
	CCR_DBG(("CCR DS: lock invoked on %s\n", table_name));
}

void
updatable_copy_impl::unlock(Environment &e)
{
	lck.lock();
	if (e.is_orphan()) {
		lck.unlock();
		return;
	}

	//
	// check state
	//
	switch (transaction_status) {
	case DONE:		// normal case
		break;
	case UNLOCKED:		// this is a retry
		lck.unlock();
		return;
	case START:
	case PREPARED:
	case COMMITTED:
	default:		// all other states are invalid
		CL_PANIC(0);
	}

	tacc->end_writer();

	transaction_status = UNLOCKED;

	CCR_DBG(("CCR DS: unlocked table %s\n", table_name));

	// invoke did_update callback on every client of this table
	// XXX should spawn new thread for this

	//
	// If the table is not the DIR table then invoke did_update
	// on the directory also, since any client interested in global
	// changes to the directory must be informed of a non-directory
	// file change also.
	//
	if (!is_dir(table_name) && !is_cluster_dir(table_name)) {
		//
		// The table is not the directory. The operation type is
		// going to be ccr::CCR_TABLE_MODIFIED since for a table
		// to added/deleted, the directory would be the table
		// being modified.
		//
		table_access 		*ta_dir;
		ccr::ccr_update_type	ccr_op = ccr::CCR_TABLE_MODIFIED;

		// First invoke the callbacks on the table in question.
		tacc->call_did_update(cluster_name, table_name, ccr_op);

		ds_cluster_info *clinfo =
		    data_svr->lookup_cluster_info(cluster_name);
		ASSERT(cluster_name);
		// Then invoke the callbacks on the directory.
		ta_dir = clinfo->lookup_table(DIR);
		ASSERT(ta_dir);
		ta_dir->call_did_update(cluster_name, table_name, ccr_op);
		ta_dir->rele();
	} else {
		//
		// This is a directory operation, so it must be either
		// CCR_TABLE_ADDED or CCR_TABLE_REMOVED - determine which
		// one from the last_op field that got set. Since multiple
		// add/remove ops are not supported for the directory, ie,
		// only a single table add/delete can happen, the last_op
		// will always give the correct op.
		//
		CL_PANIC((last_op == ccr::CCR_ADD) ||
		    (last_op == ccr::CCR_REMOVE));
		if (is_cluster_dir(table_name)) {
			if (last_op == ccr::CCR_ADD) {
				tacc->call_did_update(cluster_name,
				    newdir_name, ccr::CCR_TABLE_ADDED);
				delete [] newdir_name;
				newdir_name = NULL;
			} else {
				tacc->call_did_update(cluster_name,
				    remdir_name, ccr::CCR_TABLE_REMOVED);
				delete [] remdir_name;
				remdir_name = NULL;
			}
		} else {
			ASSERT(is_dir(table_name));
			if (last_op == ccr::CCR_ADD) {
				tacc->call_did_update(cluster_name,
				    newtab_name, ccr::CCR_TABLE_ADDED);
				delete [] newtab_name;
				newtab_name = NULL;
			} else {
				tacc->call_did_update(cluster_name,
				    remtab_name, ccr::CCR_TABLE_REMOVED);
				delete [] remtab_name;
				remtab_name = NULL;
			}
		}
	}

	lck.unlock();
}

// private methods
bool
updatable_copy_impl::is_retry(ccr::update_type this_op)
{
	switch (transaction_status) {
	case START:
	case COMMITTED:
		break;
	case PREPARED:
		// make sure this is legitimate retry
		if (last_op != this_op)
			CL_PANIC(last_op == this_op);
		return (true);
	case DONE:
	case UNLOCKED:
	default:
		CCR_DBG(("CCR DS: invalid state transition requested by "
		    "TM: update operation on table file %s invoked in %d "
		    "state\n", table_name, transaction_status));
		//
		// SCMSGS
		// @explanation
		// CCR encountered an unrecoverable error while updating the
		// indicated table on this node.
		// @user_action
		// The node needs to be rebooted. Also contact your authorized
		// Sun service provider to determine whether a workaround or
		// patch is available.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "CCR: Unrecoverable failure while updating table "
		    "%s.", table_name);
	}
	return (false);
}

int
updatable_copy_impl::copy_table(char *src, char *dest)
{
	readonly_persistent_table src_table;
	updatable_persistent_table dest_table;
	table_element_t *telp;
	int retval = 0;

	if ((retval = src_table.initialize(src)) != 0)
		return (retval);

	if ((retval = dest_table.initialize(dest)) != 0) {
		src_table.close();
		return (retval);
	}

	// read all elements from src, write to dest
	while ((telp = src_table.next_element(retval)) != NULL) {
		// ignore metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = dest_table.insert_element(telp)) != 0) {
				delete telp;
				src_table.close();
				(void) dest_table.close();
				return (retval);
			}
		}
		delete telp;
	}

	src_table.close();
	retval = dest_table.close();
	return (retval);
}

// private utility func
void
updatable_copy_impl::set_table_pathname(char **path, const char *tname)
{
	unsigned long len = os::strlen(basepath) + os::strlen(tname) + 2;
	*path = new char[len];
	ASSERT(*path);
	os::sprintf(*path, "%s/%s", basepath, tname);
}

void
updatable_copy_impl::set_format_version(int32_t, Environment &)
{
	ASSERT(0);
}

#if	defined(WEAK_MEMBERSHIP) && defined(_KERNEL)
//
// Convenience function to check whether or not the cluster is running under
// the weak-membership model. We do not access the CCR directly, but obtain
// the information from the CMM instead, because this function is called
// while the CCR transaction lock is held.
//
bool
updatable_copy_impl::weak_membership_is_used()
{
	return (cmm_impl::the().are_multiple_partitions_allowed());
}

//
// Under the weak membership model, there can be just two nodes in the
// cluster. If one node is down, we're effectively in a split-brain.
//
bool
updatable_copy_impl::is_split_brain()
{
	// Return false if not under weak membership
	if (!weak_membership_is_used()) {
		return (false);
	}

	Environment e;
	CORBA::Exception *exp;
	int online_cnt = 0; // Number of online nodes
	cmm::cluster_state_t cstate;
	cmm::control_var cmm_cntl_v = cmm_ns::get_control();
	cmm_cntl_v->get_cluster_state(cstate, e);
	if ((exp = e.exception()) != NULL) {
		CCR_DBG(("CCR DS: get_cluster_state() returned exception %s\n",
		    exp->_name()));
		e.clear();
		return (false);
	}

	for (int i = 1; i < NODEID_MAX; i++) {
		if (cstate.members.members[i] != 0) {
			online_cnt++;
		}
	}

	ASSERT(online_cnt > 0);
	if (online_cnt < 2) {
		return (true);
	}
	return (false);
}
#endif // WEAK_MEMBERSHIP && _KERNEL
