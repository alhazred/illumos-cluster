//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// directory_impl.cc - CCR directory
// first point of contact for users of CCR
//

#pragma ident	"@(#)directory_impl.cc	1.41	08/11/24 SMI"

#include <sys/os.h>
#include <sys/rm_util.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <ccr/directory_impl.h>
#include <ccr/persistent_table.h>
#include <ccr/common.h>
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <sys/vc_int.h>
#endif // SOL_VERSION >= __s10

directory_impl::directory_impl(ccr_data::data_server_ptr local_data_svr):
	ccr_mgr(nil)
{
	// queries are forwarded to data server
	data_svr = ccr_data::data_server::_duplicate(local_data_svr);
}

directory_impl::~directory_impl()
{
	CORBA::release(data_svr);
	data_svr = nil;
	CORBA::release(ccr_mgr);
	ccr_mgr = nil;
}

void
#ifdef DEBUG
directory_impl::_unreferenced(unref_t cookie)
#else
directory_impl::_unreferenced(unref_t)
#endif
{
	// This object does not support multiple 0->1 transitions
	ASSERT(_last_unref(cookie));

	// XXX Must fix this.
	// XXX stay around
	// delete this;
}

//
// IMPLEMENTATION OF IDL METHODS
//

//
// directory_impl::get_cluster_names_and_ids()
// Gets the sequence of cluster names and cluster ids for
// the zone clusters configured.
//
// Note: Callers are responsible for freeing the CORBA::StringSeq_out structs
// that are returned.
//
#if (SOL_VERSION >= __s10)
void
directory_impl::zc_get_names_and_ids(
    CORBA::StringSeq_out cluster_names, CORBA::StringSeq_out cluster_ids,
    Environment &env)
{
	// Get the cluster from the environment.
	char *clusterp = get_cluster_from_env(env);
	if (!clusterp) {
		env.exception(new ccr::invalid_zc_access());
		return;
	}

	// Verify this request is coming from an appropriate zone
	if (!check_cluster_access(clusterp, env)) {
		env.exception(new ccr::invalid_zc_access());
	}

	// forward the request to the data server
	if (is_default_cluster(clusterp)) {
		ccr::element_seq *elemsp = NULL;
		Environment e;
		CORBA::Exception *exp = NULL;
		elemsp = data_svr->get_table_data(CLUSTER_DIR, e);
		if ((exp = e.release_exception()) != NULL) {
			env.exception(exp);
			delete [] clusterp;
			return;
		}

		uint_t num_clusters = elemsp->length();
		char **tmp_namesp = new char *[num_clusters];
		ASSERT(tmp_namesp);
		char **tmp_idsp = new char *[num_clusters];
		ASSERT(tmp_idsp);
		for (uint_t index = 0; index < num_clusters; index++) {
			char *cl_namep = (*elemsp)[index].key;
			tmp_namesp[index] =
			    new char[os::strlen((char *)cl_namep) + 1];
			ASSERT(tmp_namesp[index]);
			(void) os::strcpy(tmp_namesp[index], cl_namep);

			char *cl_idp = (*elemsp)[index].data;
			tmp_idsp[index] =
			    new char[os::strlen((char *)cl_idp) + 1];
			ASSERT(tmp_idsp[index]);
			(void) os::strcpy(tmp_idsp[index], cl_idp);
		}
		cluster_names = new CORBA::StringSeq(
		    num_clusters, num_clusters, tmp_namesp, true);
		cluster_ids = new CORBA::StringSeq(
		    num_clusters, num_clusters, tmp_idsp, true);
		delete elemsp;
	}
	delete [] clusterp;
}
#else	// (SOL_VERSION >= __s10)
void
directory_impl::zc_get_names_and_ids(
    CORBA::StringSeq_out, CORBA::StringSeq_out, Environment&)
{
	// Should not be used prior to S10
	ASSERT(0);
}
#endif	// (SOL_VERSION >= __s10)

//
// directory_impl::get_table_names()
//   Gets the sequence of names corresp to all the repository tables.
//
// Note: Callers are responsible for freeing the CORBA::StringSeq_out struct
// that is returned.
//
void
directory_impl::get_table_names(CORBA::StringSeq_out tabnames,
    Environment &env)
{
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);

	// forward the request to the data server
	if (is_default_cluster(clusterp)) {
		data_svr->get_table_names(tabnames, env);
	} else {
		data_svr->get_table_names_zc(clusterp, tabnames, env);
	}
	delete [] clusterp;
} //lint !e1746

//
// Gets the sequence of names corresp to all the repository tables
// in a specific cluster.
//
void
directory_impl::get_table_names_zc(const char *cluster,
	CORBA::StringSeq_out tabnames, Environment &env)
{
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, env)) {
		env.exception(new ccr::invalid_zc_access());
		return;
	}
	// forward the request to the data server
	if (is_default_cluster(cluster)) {
		data_svr->get_table_names(tabnames, env);
	} else {
		data_svr->get_table_names_zc(cluster, tabnames, env);
	}
} //lint !e1746

//
// directory_impl::get_invalid_table_names()
//   Gets the sequence of names corresp to all the repository tables that are
//   marked as invalid by the CCR..
//
// Note: Callers are responsible for freeing the CORBA::StringSeq_out struct
// that is returned.
//
void
directory_impl::get_invalid_table_names(CORBA::StringSeq_out invalid_tabnames,
	Environment &env)
{
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);

	// forward the request to the data server
	if (is_default_cluster(clusterp)) {
		data_svr->get_invalid_table_names(invalid_tabnames, env);
	} else {
		data_svr->get_invalid_table_names_zc(clusterp,
		    invalid_tabnames, env);
	}
	delete [] clusterp;
} //lint !e1746

void
directory_impl::get_invalid_table_names_zc(const char *cluster,
	CORBA::StringSeq_out invalid_tabnames, Environment &env)
{
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, env)) {
		env.exception(new ccr::invalid_zc_access());
		return;
	}
	// forward the request to the data server
	if (is_default_cluster(cluster)) {
		data_svr->get_invalid_table_names(invalid_tabnames, env);
	} else {
		data_svr->get_invalid_table_names_zc(cluster,
		    invalid_tabnames, env);
	}
} //lint !e1746

//
// directory_impl::lookup()
//   Returns a readonly handle for the table specified. The code
//   that gets called checks for the existence and validity of
//   the directory before looking for the specified table.
//
ccr::readonly_table_ptr
directory_impl::lookup(const char *name, Environment &e)
{
	Environment		env;
	CORBA::Exception	*exp;
	ccr::readonly_table_ptr	table_p = ccr::readonly_table::_nil();
	char			*clusterp = get_cluster_from_env(e);

	// forward request to data server
	if (is_default_cluster(clusterp)) {
		table_p = data_svr->lookup(name, env);
	} else {
		table_p = data_svr->lookup_zc(clusterp, name, env);
	}
	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		ASSERT(CORBA::is_nil(table_p));
		table_p = ccr::readonly_table::_nil();
	}
	delete [] clusterp;
	return (table_p);
}

ccr::readonly_table_ptr
directory_impl::lookup_zc(const char *cluster, const char *name,
    Environment &e)
{
	Environment		env;
	CORBA::Exception	*exp;
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, e)) {
		e.exception(new ccr::invalid_zc_access());
		return (nil);
	}
	// forward request to data server
	ccr::readonly_table_ptr rt_p = data_svr->lookup_zc(cluster, name, env);
	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		rt_p = ccr::readonly_table::_nil();
	}
	return (rt_p);
}

//
// directory_impl::zc_getidbyname()
// Returns the cluster id for a cluster. If no such cluster is configured,
// this will throw an exception no_such_zc.
//
uint32_t
directory_impl::zc_getidbyname(const char *cluster, Environment &e)
{
	uint32_t clid = 0;
	int ret;
#if (SOL_VERSION >= __s10) && !defined(UNODE)

	zoneid_t zid;
	zoneid_t caller_zid;
	zone_t *zonep;
	uint32_t cluster_id = e.get_cluster_id();

	// check if the cluster name is a valid name
	check_clustername(cluster, e);
	if (e.exception()) {
		return (0);
	}
	ret = data_svr->zc_getidbyname(cluster, clid, e);
	//
	// Verify this request is coming from an appropriate zone
	//
	if ((cluster_id >= MIN_CLUSTER_ID) && (cluster_id != clid)) {
		//
		// The request is from a zone cluster and it is trying
		// to access information about another zone cluster.
		//
		e.exception(new ccr::invalid_zc_access());
		return (0);
	}
	if ((clid >= MIN_CLUSTER_ID) && (cluster_id == BASE_NONGLOBAL_ID)) {
		//
		// The request is coming from a 1334 zone about
		// a zone cluster.
		//
		e.exception(new ccr::invalid_zc_access());
		return (0);
	}
	if (is_default_cluster(cluster)) {
		return (0);
	}
	//
	// We have a zonename that is not configured as a zone cluster.
	// For a native non-global (1334) zone, the zone cluster id
	// to be returned is BASE_NONGLOBAL_ID.
	//
	if (ret == -1) {
		zid = getzoneid();
		if (zid != 0) {
			//
			// We can execute in a non-global context only
			// when invoked via the cladm system call.
			//
			zonep = zone_find_by_id(zid);
			if (zonep == NULL) {
				// system call in zone context.
				CCR_DBG(("Got a null zone_t for the current"
				    " zone's context, zid = %d\n", zid));
				ASSERT(0);
				e.exception(new ccr::invalid_zc_access());
				return (0);
			}
			//
			// If the request came from a native non-global
			// zone for a zonename other than self or 'global',
			// then we shoudn't allow any information about
			// that zonename to leak.
			//
			if (os::strcmp(zonep->zone_name, cluster) == 0) {
				clid = BASE_NONGLOBAL_ID;
			} else {
				e.exception(new ccr::invalid_zc_access());
			}
			zone_rele(zonep);
			return (clid);
		} else {
			//
			// Direct invocation from client. Here we check the
			// zone context of the client. If the client is
			// executing in a native non-global zone, then
			// it can seek cluster id of the global zone
			// or self. This restriction is not there if the
			// client is executing in the global zone.
			//
			caller_zid = e.get_zone_id();
			if (caller_zid == 0) {
				zonep = zone_find_by_name((char *)cluster);
			} else {
				zonep = zone_find_by_id(caller_zid);
			}
			if (zonep == NULL) {
				e.exception(new ccr::no_such_zc());
				return (0);
			} else if (os::strcmp(zonep->zone_name, cluster)
			    == 0) {
				clid = BASE_NONGLOBAL_ID;
			} else {
				if (caller_zid == 0) {
					ASSERT(0);
				} else {
					e.exception(
					    new ccr::invalid_zc_access());
				}
			}
			zone_rele(zonep);
			return (clid);
		}
	}
#else
	ASSERT(0);
#endif // SOL_VERSION >= __S10 && !defined(UNODE)
	return (clid);
}

//
// directory_impl::zc_getnamebyid()
// Returns the name of the cluster, given the cluster id. If no cluster
// is configured with the given id, this will throw an exception
// no_such_zc.
//
char *
directory_impl::zc_getnamebyid(uint32_t cluster_id, Environment &e)
{
	char *clname = NULL;
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	uint32_t clid = e.get_cluster_id();
	//
	// If the invocation originated from a zone cluster, then
	// it can only query about self.
	//
	if ((clid >= MIN_CLUSTER_ID) && (clid != cluster_id)) {
		e.exception(new ccr::invalid_zc_access());
		return (NULL);
	}
	//
	// If the invocation originated from a native non-global zone
	// then it can query about self or base cluster.
	//
	if ((clid == BASE_NONGLOBAL_ID) && (cluster_id != BASE_CLUSTER_ID) &&
	    (cluster_id != BASE_NONGLOBAL_ID)) {
		e.exception(new ccr::invalid_zc_access());
		return (NULL);
	}
	//
	// For the base cluster or native non-global zone, the zone cluster
	// name defaults to 'global'
	//
	if ((cluster_id == BASE_CLUSTER_ID) ||
	    (cluster_id == BASE_NONGLOBAL_ID)) {
		clname = os::strdup(DEFAULT_CLUSTER);
	} else {
		clname = data_svr->zc_getnamebyid(cluster_id, e);
		if (clname == NULL) {
			e.exception(new ccr::no_such_zc());
		}
	}
#else
	ASSERT(0);
#endif // SOL_VERSION >= __S10 && !defined(UNODE)
	return (clname);
}

//
// directory_impl::zc_create()
// Creates a zone cluster. This creates an empty DIR table
// for the cluster. This involves trasaction on the CLUSTER_DIR.
//
void
directory_impl::zc_create(const char *cluster, Environment &e)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	ccr::updatable_table_var	transptr_v;
	CORBA::Exception		*exp;
	Environment			env;
	uint32_t			cluster_id;
	char				cl_id[65];

	//
	// Update reference to the manager object if it is not
	// already done.
	//
	if (update_tm_ref(e) != 0) {
		return;
	}

	//
	// Verify this request is coming from an appropriate zone
	//
	cluster_id = e.get_cluster_id();
	if (cluster_id != 0) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}

	// check if the cluster name is a valid name
	check_clustername(cluster, e);
	if (e.exception()) {
		return;
	}

	//
	// Lock out any other changes to the DEFAULT_CLUSTER.
	//
	transptr_v = begin_transaction_common(DEFAULT_CLUSTER, CLUSTER_DIR,
	    false, env);

	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		return;
	}

	//
	// Get the max unused cluster id. cluster id's 0, 1 and 2
	// are reserved. cluster id 0 is for the global cluster.
	// cluster id 1 is reserved for the use of the name server
	// and cluster id 2 is for the native non-global zone.
	//
	cluster_id = data_svr->zc_get_maxid(env);
	(void) os::itoa((int)cluster_id, cl_id);

	//
	// Add the specified cluster to the repository. This
	// also creates an "empty" (it has the ccr_gennum and
	// ccr_checksum fields initialized) DIR table
	// in the repository.
	//
	transptr_v->add_element(cluster, cl_id, env);

	//
	// Handle exception.
	//
	if ((exp = env.release_exception()) != NULL) {
		transptr_v->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: failed to abort the operation to"
			    " create cluster %s\n", cluster));
			env.clear();
		}

		//
		// check existence of cluster.
		//
		if (ccr::key_exists::_exnarrow(exp)) {
			delete exp;
			e.exception(new ccr::zc_exists(cluster));
		} else {
			//
			// SCMSGS
			// @explanation
			// The CCR failed to create the indicated cluster.
			// @user_action
			// The failure can happen due to many reasons, for
			// some of which no user action is required because
			// the CCR client in that case will handle the
			// failure. The cases for which user action is
			// required depends on other messages from CCR on the
			// node, and include: If it failed because the cluster
			// lost quorum, reboot the cluster. If the root file
			// system is full on the node, then free up some space
			// by removing unnecessary files. If the root disk on
			// the afflicted node has failed, then it needs to be
			// replaced. If the cluster repository is corrupted as
			// indicated by other CCR messages, then boot the
			// offending node(s) in -x mode to restore the cluster
			// repository backup. The cluster repository is
			// located at /etc/cluster/ccr/.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Create cluster %s failed.", cluster);
			CCR_DBG(("CCR: create cluster %s failed in "
			    "add_element\n", cluster));
			e.exception(exp);
		}
		return;
	}

	// this will unlock the directory table and make the create persistent.
	transptr_v->commit_transaction(env);

	//
	// Handle Exception.
	//
	if (env.exception()) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"CCR: Create cluster %s failed.", cluster);
		CCR_DBG(("CCR: create cluster %s failed in directory commit\n",
		    cluster));
		env.clear();
		return;
	}
#endif // SOL_VERSION >= __S10 && !defined(UNODE)
}

//
// directory_impl::zc_remove()
// Removes all the tables in the cluster. This involves transactions
// on the directory table as well as on the CLUSTER_DIR
// table. Each table in the directory is removed as a transaction.
//
void
directory_impl::zc_remove(const char *cluster, Environment &e)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	ccr::updatable_table_var	table_transptr_v;
	ccr::updatable_table_var	dir_transptr_v;
	ccr::updatable_table_var	cl_dir_transptr_v;
	CORBA::Exception		*exp;
	CORBA::StringSeq		*tabnames;
	Environment			env;
	uint_t				indx;
	uint_t				num_tables;
	uint32_t			cluster_id;

	//
	// Update reference to the manager object if it is not
	// already done.
	//
	if (update_tm_ref(e) != 0) {
		return;
	}

	//
	// Verify this request is coming from an appropriate zone
	//
	cluster_id = e.get_cluster_id();

	if (cluster_id != 0) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}

	// check if the cluster name is a valid name
	check_clustername(cluster, e);
	if (e.exception()) {
		return;
	}
	//
	// Get the list of all the tables in the cluster.
	//
	get_table_names_zc(cluster, tabnames, env);

	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		return;
	}

	num_tables = tabnames->length();

	//
	// Remove all the tables in the cluster.
	//
	for (indx = 0; indx < num_tables; ++indx) {

		remove_table_zc(cluster, (const char *)(*tabnames)[indx],
		    env);

		if ((exp = env.release_exception()) != NULL) {
			CCR_DBG(("Remove table %s %s\n failed", cluster,
			    (const char *)(*tabnames)[indx]));
			e.exception(exp);
			return;
		}
	}

	//
	// Lock out further updates to the directory.
	//
	dir_transptr_v = begin_transaction_common(cluster, DIR, false, env);

	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		return;
	}

	cl_dir_transptr_v = begin_transaction_common(DEFAULT_CLUSTER,
	    CLUSTER_DIR, false, env);

	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		dir_transptr_v->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: dir_transptr_v abort transaction "
			    "failed\n"));
			env.clear();
		}
		return;
	}

	//
	// Removing the directory table of a cluster is equivalent
	// to removing the cluster itself.
	//
	cl_dir_transptr_v->remove_element(cluster, env);

	if ((exp = env.release_exception()) != NULL) {
		e.exception(exp);
		cl_dir_transptr_v->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: cl_dir_transptr_v abort "
			    "transaction failed\n"));
			env.clear();
		}
		dir_transptr_v->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: dir_transptr_v abort transaction "
			    "failed\n"));
			env.clear();
		}
		return;
	}
	//
	// Unlock changes to the cluster_directory table.
	//
	cl_dir_transptr_v->commit_transaction(env);
	//
	// Handle Exception.
	//
	if (env.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR failed to remove the indicated cluster.
		// @user_action
		// The failure can happen due to many reasons, for some of
		// which no user action is required because the CCR client in
		// that case will handle the failure. The cases for which user
		// action is required depends on other messages from CCR on
		// the node, and include: If it failed because the cluster
		// lost quorum, reboot the cluster. If the root file system is
		// full on the node, then free up some space by removing
		// unnecessary files. If the root disk on the afflicted node
		// has failed, then it needs to be replaced. If the cluster
		// repository is corrupted as indicated by other CCR messages,
		// then boot the offending node(s) in -x mode to restore the
		// cluster repository from backup. The cluster repository is
		// located at /etc/cluster/ccr/.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Remove cluster %s failed.", cluster);
		CCR_DBG(("CCR: Remove cluster %s failed in "
		    "cluster directory commit", cluster));
		env.clear();
	}
	dir_transptr_v->commit_transaction(env);
	//
	// Handle Exception.
	//
	if (env.exception()) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Remove cluster %s failed.", cluster);
		CCR_DBG(("CCR: Remove cluster %s failed in "
		    "cluster directory commit", cluster));
		env.clear();
		return;
	}
#endif // SOL_VERSION >= __S10 && !defined(UNODE)
}

//
// directory_impl::create_table()
//   Creates a table with the specified name. Involves a transaction
//   on the directory, hence creates and removes to the repository
//   are serialized.
//
void
directory_impl::create_table(const char *table_name, Environment &env)
{
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	create_table_common_zc(clusterp, table_name, env);
	delete [] clusterp;
}


//
// Create a table with the specified name in a specific cluster.
//
void
directory_impl::create_table_zc(const char *cluster, const char *table_name,
    Environment &e)
{
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, e)) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}
	create_table_common_zc(cluster, table_name, e);
}

//
// Common function to create a CCR table for a specific table.
//
void
directory_impl::create_table_common_zc(const char *cluster,
    const char *table_name, Environment &e)
{
	ccr::updatable_table_var table_v;
	CORBA::Exception	*ex;
	Environment		env;

	// check if the table_name is a valid ccr table name
	check_tablename(table_name, e);
	if (e.exception()) {
		return;
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// The native non-global zone has 'read-only' access to
	// the CCR data of the global zone.
	//
	if (e.get_cluster_id() == BASE_NONGLOBAL_ID) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}
	//
	// Zone clusters can create CCR tables from within the
	// zone cluster only if the table name has a prefix "RW_"
	//
	if ((e.get_cluster_id() >= MIN_CLUSTER_ID) &&
	    (os::strstr(table_name, "RW_") != table_name)) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	//
	// Lock out any other changes to the DIR table.
	//
	table_v = begin_transaction_common(cluster, DIR, false, env);

	if ((ex = env.release_exception()) != NULL) {
		//
		// If there is a no_such_table or a table_invalid
		// exception on the DIR table, mask that as a
		// database_invalid exception.
		//
		if ((ccr::no_such_table::_exnarrow(ex) != NULL) ||
		    (ccr::table_invalid::_exnarrow(ex) != NULL)) {
			delete ex;
			CCR_DBG(("no_such_table or table_invalid"
			    " exception during begin_transaction\n"));
			e.exception(new ccr::database_invalid());
		} else {
			e.exception(ex);
		}
		return;
	}

	//
	// Add the specified table name to the repository.
	//
	table_v->add_element(table_name, "", env);
	// FI here
	if ((ex = env.release_exception()) != NULL) {
		table_v->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: failed to abort the operation to"
			    " create table %s\n", table_name));
			env.clear();
		}

		// check existence of table
		if (ccr::key_exists::_exnarrow(ex)) {
			delete ex;
			e.exception(new ccr::table_exists(table_name));
		} else {
			//
			// SCMSGS
			// @explanation
			// The CCR failed to create the indicated table.
			// @user_action
			// The failure can happen due to many reasons, for
			// some of which no user action is required because
			// the CCR client in that case will handle the
			// failure. The cases for which user action is
			// required depends on other messages from CCR on the
			// node, and include: If it failed because the cluster
			// lost quorum, reboot the cluster. If the root file
			// system is full on the node, then free up some space
			// by removing unnecessary files. If the root disk on
			// the afflicted node has failed, then it needs to be
			// replaced. If the cluster repository is corrupted as
			// indicated by other CCR messages, then boot the
			// offending node(s) in -x mode to restore the cluster
			// repository backup. The cluster repository is
			// located at /etc/cluster/ccr/.
			//
			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			    "CCR: Create table %s failed.", table_name);
			CCR_DBG(("CCR: create table %s failed in "
			    "add_element\n", table_name));
			e.exception(ex);
		}
		return;
	}

	// this will unlock the directory table and make the create persistent.
	table_v->commit_transaction(env);

	//
	// Handle Exceptions if any.
	//
	if (env.exception()) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"CCR: Create table %s failed.", table_name);
		CCR_DBG(("CCR: create table %s failed in directory commit\n",
		    table_name));
		env.clear();
		return;
	}
}

//
// directory_impl::remove_table()
//   Removes the specified table from the repository. Involves
//   transactions on the table as well as on the directory. Hence,
//   updates to this table are serialized with the remove_table op.
//   Also, creates and removes to the repository are serialized.
//
void
directory_impl::remove_table(const char *table_name, Environment &env)
{
	char *clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	remove_table_common_zc(clusterp, table_name, env);
	delete [] clusterp;
}

//
// Removes the specified table in the specified cluster from the repository.
//
void
directory_impl::remove_table_zc(const char *cluster, const char *table_name,
    Environment &e)
{
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, e)) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}
	remove_table_common_zc(cluster, table_name, e);
}

//
// Common function to remove a CCR table for a specific table.
//
void
directory_impl::remove_table_common_zc(const char *clusterp,
    const char *table_name, Environment &e)
{
	ccr::updatable_table_var table_transptr;
	ccr::updatable_table_var dir_transptr;
	CORBA::Exception	*ex;
	Environment		env;

	// check if the table_name is a valid ccr table name
	check_tablename(table_name, e);
	if (e.exception()) {
		return;
	}

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// The native non-global zone has 'read-only' access to
	// the CCR data of the global zone.
	//
	if (e.get_cluster_id() == BASE_NONGLOBAL_ID) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}
	//
	// Zone clusters can remove CCR tables from within the
	// zone cluster only if the table name has a prefix "RW_"
	//
	if ((e.get_cluster_id() >= MIN_CLUSTER_ID) &&
	    (os::strstr(table_name, "RW_") != table_name)) {
		e.exception(new ccr::invalid_zc_access());
		return;
	}
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	//
	// Since remove_table() will involve updates to the table
	// in question, as well as to the directory, obtain locks
	// on both the table and the directory. Lock out changes
	// to the table before doing the same on the directory,
	// that way the directory would not be unnecessarily
	// locked out if there was an already ongoing transaction
	// on the table. The unlocking sequence should be corres-
	// pondingly opposite, since the commit_transaction() on
	// the directory will set the delete_flag for the table
	// transaction, which is necessary for the table to get
	// removed from the TM's cache. If there were no delete_
	// flag to be set/used, and if the transaction on the
	// table committed before that on the directory (which
	// would also delete the table entry from the TM's cache),
	// then the window between the two commits would allow
	// an outstanding transaction on the table to proceed,
	// which would be incorrect, since the table would be
	// deleted in the second commit.
	//

	// Lock out any other updates to the specified table.
	table_transptr = begin_transaction_common(clusterp, table_name,
	    false, env);
	if ((ex = env.release_exception()) != NULL) {
		e.exception(ex);
		return;
	}

	//
	// Lock out any other updates to the directory.
	//
	dir_transptr = begin_transaction_common(clusterp, DIR, false, env);

	if ((ex = env.release_exception()) != NULL) {
		// If there is a no_such_table or a table_invalid
		// exception on the DIR table, mask that as a
		// database_invalid exception.
		if ((ccr::no_such_table::_exnarrow(ex) != NULL) ||
		    (ccr::table_invalid::_exnarrow(ex) != NULL)) {
			delete ex;
			e.exception(new ccr::database_invalid());
		} else {
			e.exception(ex);
		}
		table_transptr->abort_transaction(env);
		env.clear();
		return;
	}

	//
	// Remove the specified table from the repository - this
	// operation will be visible to the clients only after the
	// commit succeeds.
	// Removing the directory table of a cluster is equivalent
	// to removing the cluster itself.
	//
	dir_transptr->remove_element(table_name, env);
	// FI here
	if ((ex = env.release_exception()) != NULL) {
		// The table must have existed since otherwise
		// a begin_transaction() on it above would have
		// returned an exception.
		CL_PANIC(ccr::no_such_key::_exnarrow(ex) == NULL);

		dir_transptr->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: dir_transptr abort transaction "
			    "failed\n"));
			env.clear();
		}

		table_transptr->abort_transaction(env);
		if (env.exception()) {
			CCR_DBG(("CCR: table_transptr abort transaction"
			    " failed\n"));
			env.clear();
		}

		//
		// SCMSGS
		// @explanation
		// The CCR failed to remove the indicated table.
		// @user_action
		// The failure can happen due to many reasons, for some of
		// which no user action is required because the CCR client in
		// that case will handle the failure. The cases for which user
		// action is required depends on other messages from CCR on
		// the node, and include: If it failed because the cluster
		// lost quorum, reboot the cluster. If the root file system is
		// full on the node, then free up some space by removing
		// unnecessary files. If the root disk on the afflicted node
		// has failed, then it needs to be replaced. If the cluster
		// repository is corrupted as indicated by other CCR messages,
		// then boot the offending node(s) in -x mode to restore the
		// cluster repository from backup. The cluster repository is
		// located at /etc/cluster/ccr/.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Remove table %s failed.", table_name);
		e.exception(ex);
		return;
	}

	// Unlock changes to the directory table, and make the remove
	// visible to clients (by doing a file::unlink among other
	// things).
	dir_transptr->commit_transaction(env);
	// FI here
	if (env.exception()) {
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Remove table %s failed.", table_name);
		CCR_DBG(("CCR: Remove table %s failed in "
		    "directory commit", table_name));
		env.clear();
	}

	// Unlock changes to the specified filename.
	table_transptr->commit_transaction(env);

	// The commit on this transaction can not return an exception,
	// since there has been no update directly to this transaction,
	// which therefore has its state as START. This will cause the
	// commit_transaction() code to invoke clear_transaction() on
	// the distributed copies and to ignore any corresp. exceptions.
	CL_PANIC(env.exception() == NULL);
}

//
// Initiate a transaction for a table.
//
ccr::updatable_table_ptr
directory_impl::begin_transaction(const char *table_name, Environment &e)
{
	ccr::updatable_table_ptr trans_p = ccr::updatable_table::_nil();

	// check if the table_name is a valid ccr table name
	check_tablename(table_name, e);
	if (e.exception()) {
		return (ccr::updatable_table::_nil());
	}

	//
	// The cluster to which the table belongs to is retrieved from
	// the environment.
	//
	char *clusterp = get_cluster_from_env(e);
	ASSERT(clusterp);
	trans_p = begin_transaction_common(clusterp, table_name, false, e);
	delete [] clusterp;
	return (trans_p);
}

ccr::updatable_table_ptr
directory_impl::begin_transaction_zc(const char *cluster,
    const char *table_name, Environment &env)
{
	// check if the table_name is a valid ccr table name
	check_tablename(table_name, env);
	if (env.exception()) {
		return (ccr::updatable_table::_nil());
	}
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, env)) {
		env.exception(new ccr::invalid_zc_access());
		return (ccr::updatable_table::_nil());
	}
	return (begin_transaction_common(cluster, table_name, false, env));
}

ccr::updatable_table_ptr
directory_impl::begin_transaction_invalid_ok(const char *table_name,
    Environment &env)
{
	ccr::updatable_table_ptr trans_p = ccr::updatable_table::_nil();

	char	*clusterp = get_cluster_from_env(env);
	ASSERT(clusterp);
	trans_p  = begin_transaction_common(clusterp, table_name, true, env);
	delete [] clusterp;
	return (trans_p);
}

ccr::updatable_table_ptr
directory_impl::begin_transaction_invalid_ok_zc(const char *cluster,
    const char *table_name, Environment &e)
{
	//
	// Verify this request is coming from an appropriate zone
	//
	if (!check_cluster_access(cluster, e)) {
		e.exception(new ccr::invalid_zc_access());
		return (ccr::updatable_table::_nil());
	}
	return (begin_transaction_common(cluster, table_name, true, e));
}

//
// directory_impl::register_callback()
//  Registers a callback to be invoked whenever there is any change
//  to the repository related to the cluster, including any table being
//  added, removed or modified as well as when ccr is recovered.
//
void
directory_impl::register_callback(ccr::callback_ptr cb_p, Environment &e)
{
	char *clusterp = get_cluster_from_env(e);
	ASSERT(clusterp);
	register_callback_in(clusterp, DIR, cb_p, e);
	delete [] clusterp;
}


//
// directory_impl::unregister_callback()
//   Unregisters a callback from the directory table in the global cluster.
//
void
directory_impl::unregister_callback(ccr::callback_ptr cb_p, Environment &e)
{
	char *clusterp = get_cluster_from_env(e);
	ASSERT(clusterp);
	unregister_callback_in(clusterp, DIR, cb_p, e);
	delete [] clusterp;
}

//
// directory_impl::register_callback_zc()
//  Registers a callback to be invoked whenever there is any change
//  to the repository related to a specific cluster, including any table
//  being added, removed or modified as well as when ccr is recovered.
//
void
directory_impl::zc_register_callback(const char *cluster,
    ccr::callback_ptr cb_p, Environment &e)
{
	register_callback_in(cluster, DIR, cb_p, e);
}

//
// directory_impl::zc_unregister_callback()
//   Unregisters a callback from the directory table in a specific cluster.
//
void
directory_impl::zc_unregister_callback(const char *cluster,
    ccr::callback_ptr cb_p, Environment &e)
{
	unregister_callback_in(cluster, DIR, cb_p, e);
}

//
// directory_impl::register_cluster_callback()
//  Registers a callback to be invoked whenever there is any change
//  to the repository related to a specific cluster opeartion, including
//  any cluster being added, removed or when ccr is recovered.
//
void
directory_impl::register_cluster_callback(ccr::callback_ptr cb_p,
    Environment &e)
{
	//
	// This interface can be invoked only from a global zone.
	// This validation is performed later during the call.
	//
	register_callback_in(DEFAULT_CLUSTER, CLUSTER_DIR, cb_p, e);
}

//
// directory_impl::unregister_cluster_callback()
//   Unregisters a callback from the cluster directory table in
//   the global cluster.
//
void
directory_impl::unregister_cluster_callback(ccr::callback_ptr cb_p,
    Environment &e)
{
	//
	// This interface can be invoked only from a global zone.
	// This validation is performed later during the call.
	//
	unregister_callback_in(DEFAULT_CLUSTER, CLUSTER_DIR, cb_p, e);
}

//
// PRIVATE METHODS
//

//
// directory_impl::begin_transaction_common()
//   Common code for both types of begin_transaction() calls above.
//   This call blocks if there are outstanding transactions on the
//   the specified table, or if there is a data server registration
//   in progress.
//   This call returns prematurely if quorum and recovery are not
//   achieved or primary is frozen, upon which a retry is attempted.
//
ccr::updatable_table_ptr
directory_impl::begin_transaction_common(const char *cluster,
    const char *table_name, bool invalid_ok, Environment &e)
{
	CORBA::Exception 		*ex;
	ccr_trans::comm_error 		*ccr_ex;
	ccr::updatable_table_ptr 	transptr;
	bool				is_retry_needed;
	Environment			env;

	transptr = ccr::updatable_table::_nil();

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	//
	// The native non-global zone has 'read-only' access to
	// the CCR data of the global zone.
	//
	if (e.get_cluster_id() == BASE_NONGLOBAL_ID) {
		e.exception(new ccr::invalid_zc_access());
		return (ccr::updatable_table::_nil());
	}
	//
	// Zone clusters have 'read-only' access to their CCR
	// tables. However, if the table name has a prefix "RW_"
	// then, it does have write access to those tables.
	//
	if ((e.get_cluster_id() >= MIN_CLUSTER_ID) &&
	    (os::strstr(table_name, "RW_") != table_name)) {
		e.exception(new ccr::invalid_zc_access());
		return (ccr::updatable_table::_nil());
	}
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)

	//
	// Update reference to the manager object if it is not
	// already done.
	//
	if (update_tm_ref(e) != 0) {
		return (ccr::updatable_table::_nil());
	}

	is_retry_needed = true;
	while (is_retry_needed) {
		if (is_default_cluster(cluster) &&
		    !is_cluster_dir(table_name)) {
			transptr = (invalid_ok) ?
			    ccr_mgr->begin_transaction_invalid_ok(table_name,
			    env):
			    ccr_mgr->begin_transaction(table_name, env);
		} else {
			transptr = (invalid_ok) ?
			    ccr_mgr->begin_transaction_invalid_ok_zc(cluster,
			    table_name, env):
			    ccr_mgr->begin_transaction_zc(cluster,
			    table_name, env);
		}
		// check for retry flag
		if (((ex = env.release_exception()) != NULL) &&
		    ((ccr_ex = ccr_trans::comm_error::_exnarrow(ex)) !=
		    NULL)) {
			if (ccr_ex->retry) {
				// try again
				delete ex;

				// Sleep for a ms before retrying, to
				// allow the cluster state (source of
				// the retry return) to change. Sleeping
				// will yield the cpu to other threads,
				// otherwise this thread will enter an
				// an infinite loop on a single-CPU node,
				// since kernel threads can not be
				// preempted except by RT and interrupt
				// threads.
				os::usecsleep((os::usec_t)1000);
			} else {
				// Retry can only be false if there was
				// a CORBA system exception, which
				// can only happen if the cluster has
				// lost quorum.
				// XXX Am not sure if there can be system
				// exceptions denoting other types of
				// conditions for ccr.
				delete ex;
				e.exception(new ccr::lost_quorum());
				is_retry_needed = false;
				transptr = ccr::updatable_table::_nil();
			}
		} else {
			if (ex != NULL) {
				e.exception(ex);
				transptr = ccr::updatable_table::_nil();
			}
			is_retry_needed = false;
		}
	}
	return (transptr);
}

//
// directory_impl::lookup_tm()
//   Gets a reference to the CCR TM.
//
ccr_trans::manager_ptr
directory_impl::lookup_tm()
{
	CORBA::Object_var		obj_v;
	Environment			e;
	const uint_t			max_retries = 5;
	const uint_t			sleep_interval = 1;
	uint_t				retry_cnt = 0;
	CORBA::Exception		*exp = NULL;

	replica::rm_admin_var rm_ref_v = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref_v));

	replica::service_admin_var	control_v;

	//
	// If a valid reference to the CCR TM exists and its version
	// is the highest version supported by TM, there is no need
	// to do further work.
	//
	if (is_not_nil(ccr_mgr) &&
	    (ccr_trans::manager::_version(ccr_mgr) >= 1)) {
		return (ccr_mgr);
	}

	//
	// If the client of the CCR tries to initiate a transaction very
	// early during cluster boot, for example during the first CMM
	// reconfiguration, the CCR transaction manager may not be ready
	// to accept such requests. We resolve this by adding retries.
	//
	do {
		// Get CCR manager from RMA
		control_v = rm_ref_v->get_control("ccr_server", e);
		if ((exp = e.exception()) == NULL) {
			break;
		}
		if (retry_cnt < max_retries) {
			e.clear();
			retry_cnt++;
			CCR_DBG(("Could not find the CCR transaction manager, "
			    "will retry\n"));
			// Sleep for seconds equal to SLEEP_INTERVAL
			os::usecsleep((os::usec_t)sleep_interval*MICROSEC);
			continue;
		}
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Could not find the CCR transaction manager.");
		CCR_DBG(("tm_not_found in lookup_tm"));
		e.clear();
		return (nil);
	} while (1);
	obj_v = control_v->get_root_obj(e);
	if (e.exception()) {
		CCR_DBG(("CCR DIR: get_root_obj returned exception\n"));
		e.clear();
		return (nil);
	}
	ccr_mgr = ccr_trans::manager::_narrow(obj_v);
	return (ccr_mgr);
}

//
// Update the reference of the transaction manager object to the
// latest version. If the update has already happened, there is
// no further work to be done.
//
int
directory_impl::update_tm_ref(Environment &e)
{
	version_lock.lock();
	//
	// check and update the reference to TM if upgrade commit
	// has happened.
	//
	if (lookup_tm() == nil) {
		e.exception(new ccr::readonly_access());
		version_lock.unlock();
		return (1);
	}
	version_lock.unlock();
	return (0);
}

//
// directory_impl::check_clustername()
//   Called by remove_cluster() and create_cluster(), to prevent users from
//   accidentally passing an invalid cluster name or an empty string.
//
void
directory_impl::check_clustername(const char *clusterp, Environment &e) const
{
	// check if the size of cluster name is zero.
	if (clusterp == NULL || os::strlen(clusterp) == 0) {
		e.exception(new ccr::invalid_zc_name(clusterp));
		return;
	}

	// check if the cluster name starts with or contains an empty string
	if (os::strstr(clusterp, " ") != NULL) {
		e.exception(new ccr::invalid_zc_name(clusterp));
		return;
	}
}

//
// directory_impl::check_tablename()
//   Called by remove_table() and create_table(), to prevent users from
//   accidentally passing a CCR metadata table name or an empty string.
//
void
directory_impl::check_tablename(const char *table_name, Environment &e) const
{
	// check if the size of table_name is zero.
	if (os::strlen(table_name) == 0) {
		e.exception(new ccr::invalid_table_name(table_name));
		return;
	}

	// check if the table_name starts with or contains an empty string
	if (os::strstr(table_name, " ") != NULL) {
		e.exception(new ccr::invalid_table_name(table_name));
		return;
	}

	if (os::strcmp(CLUSTER_DIR, table_name) == 0) {
		e.exception(new ccr::is_metadata(table_name));
		return;
	}

	if (os::strcmp(DIR, table_name) == 0) {
		e.exception(new ccr::is_metadata(table_name));
		return;
	}

	if (os::strcmp("epoch", table_name) == 0) {
		e.exception(new ccr::is_metadata(table_name));
		return;
	}

	//
	// Since keyword starts with "ccr_" is reserved for ccr metadata, we
	// will not allow any table with name beginning with "ccr_".
	//
	if (is_meta_row(table_name)) {
		e.exception(new ccr::invalid_table_name(table_name));
	}
}

//
// Register callbacks for a specific table in a cluster.
//
void
directory_impl::register_callback_in(const char *cluster, const char *tname,
    ccr::callback_ptr cb_p, Environment &e)
{
	ccr::readonly_table_ptr	dir_p;
	Environment		env;
	CORBA::Exception	*ex;
	//
	// First get a readonly handle for the directory table.
	// The lookup will fail with invalid access exception
	// if the method is tried from a zone cluster
	// on a different cluster.
	//
	dir_p = lookup_zc(cluster, tname, env);
	if ((ex = env.release_exception()) != NULL) {
		// Problems accessing the directory file.
		e.exception(ex);
		return;
	}

	// Next register a callback with it.
	dir_p->register_callbacks(cb_p, env);
	if ((ex = env.release_exception()) != NULL) {
		e.exception(ex);
	}

	// Release the directory ptr before returning.
	CORBA::release(dir_p);
	dir_p = nil;
}

//
// unregister callbacks for a specific table in a cluster.
//
void
directory_impl::unregister_callback_in(const char *cluster,
    const char *tname, ccr::callback_ptr cb_p, Environment &e)
{
	ccr::readonly_table_ptr	dir_p;
	Environment		env;
	CORBA::Exception	*ex;
	//
	// First get a readonly handle for the directory table.
	// The lookup will fail with invalid access exception
	// if the method is tried from a zone cluster
	// on a different cluster.
	//
	dir_p = lookup_zc(cluster, tname, env);
	if ((ex = env.release_exception()) != NULL) {
		// Problems accessing the directory file.
		e.exception(ex);
		return;
	}

	// Unregister a callback with it.
	dir_p->unregister_callbacks(cb_p, env);
	if ((ex = env.release_exception()) != NULL) {
		e.exception(ex);
	}

	// Release the directory ptr before returning.
	CORBA::release(dir_p);
	dir_p = nil;
}
