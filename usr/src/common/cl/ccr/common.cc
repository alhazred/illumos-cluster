//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// common.cc - common code for DS and TM
//

#pragma ident	"@(#)common.cc	1.12	08/07/16 SMI"

#include <sys/os.h>
#include <ccr/data_server_impl.h>
#include <ccr/common.h>
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <sys/vc_int.h>
#endif // SOL_VERSION >=__s10

//
// Add an element to the CCR element sequence
//
void
dir_elem_seq::add_element(const char *tname)
{
	char	*key;
	char	*data;
	uint_t	num_tables = 0;

	ASSERT(tname != NULL);
	if (seq == NULL) {
		seq = new ccr::element_seq(1, 1);
		ASSERT(seq);
	} else {
		num_tables = seq->length();
	}
	seq->length(num_tables + 1);
	key = new char[os::strlen(tname) + 1];
	data = new char[1];
	ASSERT(key && data);
	(void) os::strcpy(key, tname);
	*data = '\0';
	(*seq)[num_tables].key = key;
	(*seq)[num_tables].data = data;
}

//
// Remove an element from the CCR element sequence
//
void
dir_elem_seq::remove_element(const char *tname)
{
	uint_t	num_tables = seq->length();
	uint_t	indx;
	uint_t	curr_indx;

	for (indx = 0; indx < num_tables; indx++) {
		if (os::strcmp(tname, (char *)(*seq)[indx].key) == 0) {
			break;
		}
	}
	CL_PANIC(indx < num_tables);

	//
	// compact rest of sequence
	//
	for (curr_indx = indx; curr_indx < num_tables - 1; curr_indx++) {
		(*seq)[curr_indx] = (*seq)[curr_indx + 1];
	}
	seq->length(num_tables - 1);
}

//
// Add multiple elements to the CCR element sequence.
// If an element already exists in the list it is discarded.
//
void
dir_elem_seq::add_elements(const ccr::element_seq& elems)
{
	uint_t		indx;
	uint_t		curr_indx = 0;
	uint_t		curr_length = 0;
	bool		found;
	const char	*key;
	const char	*curr_key;

	uint_t	num_tables = elems.length();

	if (seq == NULL) {
		for (indx = 0; indx < num_tables; ++indx) {
			key = (const char *)(elems)[indx].key;
			add_element(key);
		}
		return;
	}
	curr_length = seq->length();
	for (indx = 0; indx < num_tables; ++indx) {
		key = (const char *)(elems)[indx].key;
		ASSERT(key);
		found = false;
		//
		// Check if the element already exists in the list.
		//
		for (curr_indx = 0; curr_indx < curr_length; ++curr_indx) {
			curr_key = (const char *)(*seq)[curr_indx].key;
			ASSERT(curr_key);
			if (os::strcmp(key, curr_key) == 0) {
				found = true;
				break;
			}
		}
		if (!found) {
			add_element(key);
		}
	}
}

//
// Remove multiple elements from the CCR element sequence.
// The list of elements to be removed is passed as an agrument.
//
void
dir_elem_seq::remove_elements(ccr::element_seq *elems)
{
	uint_t	indx;
	char	*key;
	uint_t	num_tables = elems->length();

	for (indx = 0; indx < num_tables; indx++) {
		key = (char *)(*elems)[indx].key;
		remove_element(key);
	}
}

//
// Retrieve the cluster name from the environment
//
char *
get_cluster_from_env(const CORBA::Environment &env)
{
	char	*clusterp = NULL;
	Environment e;

#if (SOL_VERSION >= __s10) && !defined(UNODE)
	uint32_t cluster_id = env.get_cluster_id();
	clusterp = data_server_impl::the_data_server_implp->zc_getnamebyid(
	    cluster_id, e);
#else
	clusterp = os::strdup("global");
#endif // (SOL_VERSION >= __s10) && !defined(UNODE)
	return (clusterp);
}

//
// Check if the cluster access is appropriate
// The cluster access is permitted if the request originated from
// the global zone or from the zone that is part of the same cluster-wide
// zone. Also, it is permitted when the access is from a native non-global
// zone and the cluster happens to be the base cluster a.k.a global.
//
bool
check_cluster_access(const char *cluster, const CORBA::Environment& env)
{
#if (SOL_VERSION >= __s10) && !defined(UNODE)
	uint32_t cluster_id = env.get_cluster_id();
	uint32_t clid;
	int ret = data_server_impl::get_zcid(cluster, &clid);
	if (ret == -1) {
		clid = 0;
	}
	if ((cluster_id == 0) || (cluster_id == clid) ||
	    ((cluster_id == BASE_NONGLOBAL_ID) &&
	    (os::strcmp(cluster, "global") == 0))) {
		return (true);
	} else {
		return (false);
	}
#else
	//
	// Make sure only the cluster 'global' is being passed.
	//
	CL_PANIC(os::strcmp(cluster, "global") == 0);
	return (true);
#endif  // SOL_VERSION >= __S10 && !defined(UNODE)
}
