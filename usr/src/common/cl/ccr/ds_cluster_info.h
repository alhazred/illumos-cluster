//
//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ds_cluster_info.h - cluster information for DS.
//

#ifndef _DS_CLUSTER_INFO_H
#define	_DS_CLUSTER_INFO_H

#pragma ident	"@(#)ds_cluster_info.h	1.6	08/07/17 SMI"

#include <sys/os.h>
#include <sys/hashtable.h>
#include <sys/list_def.h>
#include <ccr/common.h>
#include <ccr/persistent_table.h>
#include <ccr/table_access.h>

//
// ds_cluster_info: Information specific to a cluster
// used by the data server.
//
class ds_cluster_info : public _SList::ListElem {
	friend class data_server_impl;
	//
	// Public Interface
	//
public:
	ds_cluster_info(uint_t, const char *, const char *);
	~ds_cluster_info();
	table_access *lookup_table(const char *);
	void remove_incore_table(const char *);
	void add_incore_table(const char *);
	void refresh_directory_info();
	void cleanup_directory_info(const ccr::element_seq &);
	void get_table_names(CORBA::StringSeq_out);
	void get_invalid_table_names(CORBA::StringSeq_out);
	void add_update_table(const char *);
	int remove_tables();
	void set_table_pathname(char **, const char *);
	ccr_data::consis get_consis_in(const char *, Environment&);

private:
	uint_t			cluster_id;	// cluster id
	char			*clusterp;	// cluster name
	char			*dir_path;	// Basepath for the CCR
						// for the cluster.
	dir_elem_seq		directory_elems; // List of tables in cluster.
						// Excludes directory table.
	os::mutex_t		tacc_mutex;
	SList<char>		*update_table_list;
	string_hashtable_t<table_access *>	tacc_hashtab;
};

#endif	/* _DS_CLUSTER_INFO_H */
