//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ccr_tm_main.cc - CCR Transaction Manager main routine

#pragma ident   "@(#)ccr_tm_init.cc 1.7     08/05/20 SMI"

#include <sys/os.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>
#include <ccr/ccr_repl_prov.h>

int
ccr_trans_mgr_initialize()
{
	ccr_repl_prov::initialize();
	return (0);
}

#ifdef _KERNEL_ORB
int
initialize_ccr_tm()
{
	return (ccr_trans_mgr_initialize());
}
#endif
