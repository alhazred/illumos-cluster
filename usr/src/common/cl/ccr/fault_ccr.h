/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * fault_ccr.h
 *
 */

#ifndef _FAULT_CCR_H
#define	_FAULT_CCR_H

#pragma ident	"@(#)fault_ccr.h	1.4	08/05/20 SMI"

//
// Used by src/orbtest/ccr_fi tests. Refer to
// src/fault_numbers/faultnum_ccr.h
//

#if defined(_FAULT_INJECTION)


#include <sys/os.h>
#include <orb/invo/common.h>

//
// fault_ccr class which provides some static methods for ccr_fi
//

class fault_ccr {
public:

	//
	// This is used to identify which operation to take when a certain
	// faultpoint is set. The generic_test uses these to process the
	// fault point.
	//
	enum op_t {
		RESTORE_TABLE = 1,
		REBOOT_NODE = 2,
		WAIT_FOR_REJOIN = 5,
		SET_SYSTEM_ERROR = 6
	};

	enum table_t {
		DIRECTORY = 1,
		EPOCH = 2,
		RECOVERY_TEST = 3,
		CCR_TEST = 4
	};

	// Argument for ccr_fi trigger
	struct ccr_arg_t {
		// Which operation to take
		op_t			op;

		// Which table to restore. See restore_file for list
		table_t			table;

		// Which node to reboot for REBOOT_NODE op
		nodeid_t		nodenum;
	};

	//
	// Generic test function which calls another test function based on
	// the operation..
	//
	static void	generic_test_op(uint_t fault_num, void *fault_argp,
			    uint32_t argsize);

	static void wait_for_rejoin(ccr_arg_t *argp);
	static void reboot_node(uint_t fnum, ccr_arg_t *argp);

private:
	// Disallow making instances of this class.
	fault_ccr();
	~fault_ccr();

	//
	// The following functions are real test functions that will force the
	// test ccr to go to the certain path. They are called by
	// above function.
	//

	static void set_system_error(uint_t fnum, ccr_arg_t *argp);
};

#endif // _FAULT_INJECTION

#endif	/* _FAULT_CCR_H */
