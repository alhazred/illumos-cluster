//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// transaction_impl.cc - CCR transaction object
//

#pragma ident	"@(#)transaction_impl.cc	1.59	08/08/04 SMI"

#include <sys/os.h>
#include <ccr/transaction_impl.h>
#include <ccr/fault_ccr.h>
#include <sys/clconf_int.h>

//
// table_trans_impl::table_trans_impl()
//   Constructor for primary
//
table_trans_impl::table_trans_impl(ccr_repl_prov *serverp,
				manager_impl *mgr,
				const char *clname,
				const char *tname):
	mc_replica_of<ccr::updatable_table>(serverp),
	num_copies(0),
	copies(NULL),
	tinfo(NULL),
	transaction_status(TRANS_START),
	trans_mgr(mgr)
{
	common_constructor(clname, tname);
}

//
// table_trans_impl::table_trans_impl()
//   Constructor for secondary,
//	called by ccr_repl_prov::ckpt_create_transaction()
//
table_trans_impl::table_trans_impl(ccr::updatable_table_ptr objp,
				manager_impl *mgr,
				const char *clname,
				const char *tname,
				const ccr_data::updatable_copy_seq &cops,
				const quorum::ccr_id_list_t &ccr_ids):
	mc_replica_of<ccr::updatable_table>(objp),
	num_copies(cops.length()),
	tinfo(NULL),
	transaction_status(TRANS_START),
	trans_mgr(mgr)
{
	uint_t i;

	common_constructor(clname, tname);

	copies = new ccr_data::updatable_table_copy_var[num_copies];
	ASSERT(copies);
	for (i = 0; i < num_copies; i++) {
		// obtain duplicate of utc object
		if (CORBA::is_nil(cops[i])) {
			copies[i] = nil;
		} else {
			copies[i] = ccr_data::updatable_table_copy::
				_duplicate(cops[i]);
		}
	}
	local_ccr_ids = ccr_ids;
}

//
// table_trans_impl::common_constructor()
//   Common code used by both the primary and secondary constructors.
//
void
table_trans_impl::common_constructor(const char *clname,
				const char *tname)
{
	cluster_name = os::strdup(clname);
	ASSERT(clname);
	table_name = os::strdup(tname);
	ASSERT(table_name);
	// init directory_op to something invalid
	directory_op = ccr::CCR_INVOP;
	delete_flag = false;
	user_table = NULL;
	user_cluster_id = 0;
	user_cluster = NULL;
	local_ccr_ids.length(0);
}

//
// table_trans_impl::startup()
//   This is called from manager_impl::begin_transaction_common().
//   It initiates the transaction on every data server, creating an
//   updatable table copy at each node.
//
void
table_trans_impl::startup(bool invalid_ok, Environment &e)
{
	uint_t		i;
	bool		any_node_dead = false;
	CORBA::Exception	*ex;
	Environment	e2;
	ds_info_list_t	local_ds_list;	// local copy
	ds_info_t	*ds_infop;
	char		nodename[CL_MAX_LEN+1];

	CCR_DBG(("CCR TM : In table_trans_impl::startup\n"));

	// store table and registration info
	tinfo = trans_mgr->lookup_table(cluster_name, table_name);

	//
	// Get a snapshot of the DS info and use that as
	// the local ds copy.
	//
	trans_mgr->create_ds_snapshot(local_ds_list);
	num_copies = local_ds_list.count();

	// Fault Point for ccr_fi
	FAULTPT_CCR(FAULTNUM_CCR_STARTUP_1,
	    fault_ccr::generic_test_op);

	//
	// From the local snapshot, extract just the ccr_ids
	// portion into the local_ccr_ids sequence to be used
	// whenever this trans object needs to check quorum.
	//
	local_ccr_ids.length(num_copies);
	for (local_ds_list.atfirst(), i = 0;
	    (ds_infop = local_ds_list.get_current()) != NULL;
	    local_ds_list.advance(), i++) {
		local_ccr_ids[i] = ds_infop->ccr_id;
	}

	// get updatable table copies from all data servers
	if (copies) {
		delete [] copies;
		copies = NULL;
	}
	copies = new ccr_data::updatable_table_copy_var[num_copies];
	ASSERT(copies);
	for (i = 0; i < num_copies; i++) {
		copies[i] = nil;
	}

	for (local_ds_list.atfirst(), i = 0;
	    (ds_infop = local_ds_list.get_current()) != NULL;
	    local_ds_list.advance(), i++) {
		ccr_data::data_server_ptr data_svr = ds_infop->dsp;
		ASSERT(!(CORBA::is_nil(data_svr)));
		if (is_default_cluster(cluster_name)) {
			copies[i] = data_svr->begin_transaction(table_name,
			    invalid_ok, e2);
		} else {
			copies[i] = data_svr->begin_transaction_zc(
			    cluster_name, table_name, invalid_ok, e2);
		}
		// FI here
		// Fault Point for ccr_fi
		FAULTPT_CCR(FAULTNUM_CCR_STARTUP_2,
		    fault_ccr::generic_test_op);

		if ((ex = e2.release_exception()) != NULL) {
			clconf_get_nodename(ds_infop->ccr_id, nodename);
			if (DS_UNREACHABLE(ex)) {
				//
				// SCMSGS
				// @explanation
				// While the TM was updating the indicated
				// table in the cluster, the specified node
				// went down and has become unreachable.
				// @user_action
				// The specified node needs to be rebooted.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: CCR data server on "
				    "node %s unreachable while updating "
				    "table %s in cluster %s.", nodename,
				    table_name, cluster_name);
				CCR_DBG(("CCR TM: DS unreachable while "
				    "invoking DS->begin_transaction on "
				    "node %s, cluster %s\n", nodename,
				    cluster_name));
				any_node_dead = true;
				delete ex;
				continue;
			} else {
				// something else went wrong on DS

				//
				// SCMSGS
				// @explanation
				// The operation to update the indicated table
				// failed to start on the indicated node.
				// @user_action
				// There may be other related messages on the
				// nodes where the failure occurred, which may
				// help diagnose the problem. If the root disk
				// failed, it needs to be replaced. If the
				// indicated table was deleted by accident,
				// boot the offending node(s) in -x mode to
				// restore the indicated table from other
				// nodes in the cluster. The CCR tables are
				// located at /etc/cluster/ccr/. If the root
				// disk is full, remove some unnecessary files
				// to free up some space.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE,
				    "CCR: Updating table %s failed to startup "
				    "on node %s for cluster %s.", table_name,
				    nodename, cluster_name);
				    e.exception(ex);
				break;
			}
		}
	}

	if (any_node_dead) {
		// check if remaining copies constitute quorum
		if (!(check_update_quorum())) {
			if (check_cluster_shutdown()) {
				// Set the lost_quorum exception to
				// abort the transaction
				e.exception(new ccr::lost_quorum());
			} else {
				//
				// SCMSGS
				// @explanation
				// The cluster lost quorum when CCR started to
				// update the indicated table.
				// @user_action
				// Reboot the cluster.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC,
				MESSAGE,
				"CCR: Lost quorum while starting to update "
				"table %s, cluster %s.", table_name,
				cluster_name);
				e.exception(new ccr::lost_quorum());
			}
		}
	}

	if (e.exception()) {
		// abort transaction
		abort_transaction_copies(e2);
		e2.clear();
	}
	// Release references to dsp's in the local ds list.
	local_ds_list.dispose();
}


//
// table_trans_impl::~table_trans_impl()
//   Destructor
//
table_trans_impl::~table_trans_impl()
{
	CCR_DBG(("in table_trans_impl destructor\n"));
	cleanup(false);		// idempotent

	// Delete the transaction from the transaction list in tinfo.
	ASSERT(tinfo != NULL);
	tinfo->delete_transaction(this);

	tinfo->rele();
	tinfo = NULL;
	trans_mgr = NULL;
	delete [] copies;
	if (user_cluster != NULL) {
		delete [] user_cluster;
	}
	delete [] table_name;
	delete [] cluster_name;
}

//
// table_trans_impl::_unreferenced()
//   This is called by the framework when the reference count for
//   this object goes down to zero. The unreferenced notification
//   is delivered at both the primary and the secondaries.
//   For the case of the transaction being incomplete at the primary,
//   a threadpool task needs to be created to process this unreferenced.
//   The recovery work can be posted to it.
//
void
table_trans_impl::_unreferenced(unref_t cookie)
{
	//
	// Note: here we don't need to protect last_unref since only
	// manager_impl::begin_transaction_common calls get_objref in
	// its retry code and the _unreferenced itself calls get_objref
	// in the constructor of unref_task, but the HA framework
	// guarantees that _unreferenced will not be called concurrently
	// with that.
	//
	// The if(_last_unref()) check is necessary because an object
	// reference may have been given out by dump_state()
	//
	if (_last_unref(cookie)) {
		CCR_DBG(("in table_trans_impl unreferenced\n"));
		// both primary and secondary get _unrefd
		// if we are primary and transaction is incomplete,
		// commit/abort it via new thread
		if (trans_mgr->is_primary() &&
		    transaction_status != TRANS_END) {
			bool need_to_commit;

			//
			// If the transaction's delete_flag has
			// been set, then the transaction needs
			// to be committed since the delete_flag
			// is only set to true by the successful
			// commit of the directory transaction
			// This is necessary for the TM's hashtable
			// to be correctly updated.
			//
			if (is_table_delete()) {
				// No do_update would have been called
				CL_PANIC(transaction_status == TRANS_START);
				need_to_commit = true;
			} else {
				need_to_commit = false;
			}

			// Add task to threadpool to end transaction.
			unref_task *ut = new unref_task(this,
			    need_to_commit);
			ASSERT(ut);
			ASSERT(the_ccr_threadpool);
			CCR_DBG(("Adding task to threadpool\n"));
			the_ccr_threadpool->defer_processing(ut);
		} else {
			delete this;
		}
	}
}

//
// table_trans_impl::is_directory_trans()
//   Checks if the transaction is on the directory.
//
bool
table_trans_impl::is_directory_trans()
{
	return (is_dir(table_name) || is_cluster_dir(table_name));
}

//
// table_trans_impl::is_directory_create()
//   Checks if a new table is being created in the repository
//   (i.e. the transaction is on the directory, and some variation of
//   add_element() has been called on it).
//
bool
table_trans_impl::is_directory_create()
{
	return (directory_op == ccr::CCR_ADD);
}

//
// table_trans_impl::is_directory_remove()
//   Checks fi a table is being removed from the repository
//   (i.e. the transaction is on the directory, and some variation of
//   remove_element() has been called on it.)
//
bool
table_trans_impl::is_directory_remove()
{
	return (directory_op == ccr::CCR_REMOVE);
}

//
// table_trans_impl::is_table_delete()
//   Checks if the table corresp to this transaction is flagged to
//   be removed on a successful commit.
//
bool
table_trans_impl::is_table_delete()
{
	return (delete_flag);
}

//
// table_trans_impl::set_modified()
//   After an update operation (add, remove, update) is successfully
//   executed as part of this transaction, this method is called both
//   on the primary and on the secondaries to mark the state of the
//   transaction as TRANS_MODIFIED.
//
void
table_trans_impl::set_modified()
{
	transaction_status = TRANS_MODIFIED;
}

//
// table_trans_impl::set_cluster_op()
//   If the transaction is on the cluster_directory, and an add/remove of
//   virtual cluster has been done, then this method is called to set the
//   directory_op, user_table, user_cluster_id and user_cluster fields of
//   this object.
//
void
table_trans_impl::set_cluster_op(const ccr::update_type op,
				uint_t cluster_id, const char *cluster)
{
	CL_PANIC(is_directory_trans());
	directory_op = op;
	user_table = os::strdup(DIR);
	ASSERT(user_table);
	if (cluster != NULL) {
		user_cluster_id = cluster_id;
		user_cluster = os::strdup(cluster);
		ASSERT(user_cluster);
	}
}

//
// table_trans_impl::set_directory_op()
//   If the transaction is on the directory, and an add/remove of
//   table has been done, then this method is called to set the
//   directory_op and user_table fields of this object.
//
void
table_trans_impl::set_directory_op(const ccr::update_type op,
				const char *tname)
{
	CL_PANIC(is_directory_trans());
	directory_op = op;
	user_table = new char[os::strlen(tname) + 1];
	ASSERT(user_table);
	(void) os::strcpy(user_table, tname);
}

//
// table_trans_impl::set_table_delete()
//	If the transaction is the outer of a nested (delete)
//	transaction set, then this method is called to flag
//	that the table be deleted.
//
void
table_trans_impl::set_table_delete()
{
	delete_flag = true;
}

//
// table_trans_impl::cleanup()
//   Cleanup utility, called on the
//	primary - by commit_transaction() and abort_transaction()
//	secondaries - by commit_state::committed()
//	both primary and secondaries - by the destructor.
//   Checks at the beg to ensure that the cleanup steps are executed
//   only once. The remove_table argument is used to do the cleanup
//   of the table in case this transaction is the outer of a remove
//   transactions set. If true, the corresp hashtable entry on the TM
//   needs to be deleted.
//
void
table_trans_impl::cleanup(bool remove_table)
{
	CCR_DBG(("cleanup: %s %s\n", cluster_name, table_name));
	// only do this once
	if (transaction_status == TRANS_END)
		return;
	else
		transaction_status = TRANS_END;

	CCR_DBG(("in table_trans_impl::cleanup\n"));

	for (uint_t i = 0; i < num_copies; i++) {
		copies[i] = nil;	// does CORBA::release()
	}
	// secondary may not have tinfo
	if (tinfo == NULL) {
		tinfo = trans_mgr->lookup_table(cluster_name, table_name);
		ASSERT(tinfo);
	}
	tinfo->transaction_done(this);

	// If remove_table is set to true, delete the incore hashtab
	// entry at the TM before unlocking writes to it.
	if (remove_table) {
		CL_PANIC(is_table_delete());
		CCR_DBG(("cleanup:Remove %s %s\n", cluster_name, table_name));
		trans_mgr->remove_table_int(cluster_name, table_name);
	}

	tinfo->unlock_writes(remove_table);
	ASSERT(trans_mgr && trans_mgr->reg_info);
	trans_mgr->reg_info->end_transaction();
}


//
// table_trans_impl::unref_task::unref_task()
//   Constructor for the deferred _unreferenced processing obj.
//
table_trans_impl::unref_task::unref_task(table_trans_impl *tp,
				    bool do_commit):
	transp(tp->get_objref()),
	need_to_commit(do_commit)
{
}

//
// table_trans_impl::unref_task::~unref_task()
//   Destructor.
//
table_trans_impl::unref_task::~unref_task()
{
	CORBA::release(transp);
}

//
// table_trans_impl::unref_task::execute()
//   Called by the deferred unref processing. Ends the transaction.
//
void
table_trans_impl::unref_task::execute()
{
	Environment e;

	if (need_to_commit)
		transp->commit_transaction(e);
	else
		transp->abort_transaction(e);
	CL_PANIC(e.exception() == NULL);
	delete this;
}


//
// IMPLEMENTATION OF IDL METHODS
//

//
// table_trans_impl::add_element()
//   Adds an element (key-data pair) to the table. Calls add_elements()
//   with one element in the the element_seq.
//
void
table_trans_impl::add_element(const char *key, const char *data,
			Environment &e)
{
	CCR_DBG(("CCR TM: Adding key %s to table %s\n", key, table_name));

	ccr::element_seq seq(1, 1);
	seq[0].key = key;
	seq[0].data = data;
	add_elements(seq, e);
}

//
// table_trans_impl::add_elements()
//   Adds the specified sequence of elements (key-data pairs) to
//   the table.
//
void
table_trans_impl::add_elements(const ccr::element_seq & seq,
			Environment &e)
{
	CCR_DBG(("CCR TM: Adding %d keys(s) to table %s\n", seq.length(),
		table_name));

	update_info ui = {ccr::CCR_ADD, NULL, NULL, &seq};

	do_update(&ui, e);
	if (e.exception())
		return;

	if (is_directory_trans()) {
		CL_PANIC(seq.length() == 1);
		if (is_cluster_dir(table_name)) {
			user_cluster = os::strdup(seq[0].key); //lint !e423
			user_cluster_id =
			    (uint_t)os::atoi(seq[0].data); //lint !e423
			set_directory_op(ccr::CCR_ADD, DIR);
			ASSERT(ccr_trans::ckpt::_version(get_checkpoint())
			    >= 1);
			get_checkpoint()->zc_ckpt_op(ccr::CCR_ADD,
			    os::atoi(seq[0].data), seq[0].key, e);
		} else {
			set_directory_op(ccr::CCR_ADD, seq[0].key);
			if (is_default_cluster(cluster_name)) {
				get_checkpoint()->ckpt_directory_op(
				    ccr::CCR_ADD, seq[0].key, e);
			} else {
				get_checkpoint()->ckpt_directory_op_zc(
				    ccr::CCR_ADD, cluster_name, seq[0].key,
				    e);
			}
		}
		CL_PANIC(e.exception() == NULL);
	}
}

//
// table_trans_impl::remove_element()
//   Removes the element specified by the given key from the table.
//   Calls remove_elements() with an 1-element sequence.
//
void
table_trans_impl::remove_element(const char *key, Environment &e)
{
	ccr::element_seq seq(1, 1);
	seq[0].key = key;
	seq[0].data = (const char *)"";
	remove_elements(seq, e);
}

//
// table_trans_impl::remove_elements()
//   Removes all the elements specified (the key is used for matching
//   against the actual elements) from the table.
//
void
table_trans_impl::remove_elements(const ccr::element_seq & seq, Environment &e)
{
	CCR_DBG(("CCR TM: Removing %d key(s) from table %s, cluster %s\n",
	    seq.length(), table_name, cluster_name));

	update_info ui = {ccr::CCR_REMOVE, NULL, NULL, &seq};

	do_update(&ui, e);
	if (e.exception()) {
		CCR_DBG(("CCR TM: do_update returned exception\n"));
		return;
	}

	if (is_directory_trans()) {
		CL_PANIC(seq.length() == 1);
		if (is_cluster_dir(table_name)) {
			user_cluster = os::strdup(seq[0].key); //lint !e672
			set_directory_op(ccr::CCR_REMOVE, DIR);
			ASSERT(ccr_trans::ckpt::_version(get_checkpoint())
			    >= 1);
			get_checkpoint()->zc_ckpt_op(ccr::CCR_REMOVE,
			    os::atoi(seq[0].data), seq[0].key, e);
		} else {
			set_directory_op(ccr::CCR_REMOVE, seq[0].key);
			if (is_default_cluster(cluster_name)) {
				get_checkpoint()->ckpt_directory_op(
				    ccr::CCR_REMOVE, seq[0].key, e);
			} else {
				get_checkpoint()->ckpt_directory_op_zc(
				    ccr::CCR_REMOVE, cluster_name, seq[0].key,
				    e);
			}
		}
		CL_PANIC(e.exception() == NULL);
	}
}

//
// table_trans_impl::update_element()
//   Changes the data field of the element (specified by the key)
//   to the specified value.
//
void
table_trans_impl::update_element(const char *key, const char *data,
			Environment &e)
{
	update_info ui = {ccr::CCR_UPDATE, key, data, NULL};

	do_update(&ui, e);
}

//
// table_trans_impl::remove_all_elements()
//   Removes all elements from the table (does not remove the table,
//   just empties its contents).
//
void
table_trans_impl::remove_all_elements(Environment &e)
{

	update_info ui = {ccr::CCR_REMOVE_ALL, NULL, NULL, NULL};

	do_update(&ui, e);
}

//
// table_trans_impl::set_format_version()
//  This feature not implemented yet.
//
void
table_trans_impl::set_format_version(int32_t, Environment &)
{
	// This feature of CCR is not implemented yet.
}

//
// table_trans_impl::commit_transaction()
//   Makes the updates, if any, of the transaction persistent, and
//   ends the transaction. In case of error conditions, aborts the
//   transaction.
//
// XXX DS must handle sequence of
// commit, unlocked, commit, unlocked
//
void
table_trans_impl::commit_transaction(Environment &e)
{
	CCR_DBG(("in table_trans_impl::commit_transaction\n"));

	table_info *ti;
	bool remove_table = false;

	if (transaction_status == TRANS_FAILED ||
	    transaction_status == TRANS_END) {
		e.exception(new ccr::out_of_service);
		return;
	}

	// Fault point for ccr_fi
	FAULTPT_CCR(FAULTNUM_CCR_COMMIT_TRANSACTION_1,
	    fault_ccr::generic_test_op);

	// Check to see if we have saved state
	primary_ctx *ctxp = primary_ctx::extract_from(e);
	commit_state *saved_state = (commit_state *)ctxp->get_saved_state();
	if (saved_state) {
		// this is a retry
		// need to refresh list of data servers from manager
		// XXX how do we know which copies are still valid?
		// states: START, FAILED, COMMITTED, ABORTED
		switch (saved_state->progress) {
		case commit_state::START:
			goto committing;
		case commit_state::FAILED:
			abort_transaction_copies(e);
			goto done;
		case commit_state::COMMITTED:
			return;
		case commit_state::ABORTED:
			e.exception(new ccr::out_of_service);
			goto done;
		default:
			CL_PANIC(0);
		}
	} else {
		if (is_default_cluster(cluster_name)) {
			get_checkpoint()->ckpt_start_commit(table_name, e);
		} else {
			get_checkpoint()->ckpt_start_commit_zc(cluster_name,
			    table_name, e);
		}
		CL_PANIC(e.exception() == NULL);
	}

committing:
	// sanity check, in case user does commit with no prior updates
	if (transaction_status == TRANS_START) {
		clear_transaction_copies(e);
		CL_PANIC(e.exception() == NULL);
		if (is_table_delete())
			remove_table = true;
		goto done;
	}

	// Fault point for ccr_fi
	FAULTPT_CCR(FAULTNUM_CCR_COMMIT_TRANSACTION_2,
	    fault_ccr::generic_test_op);

	commit_transaction_copies(e);	// make updates persistent
	if (e.exception()) {
		// a failure occured, but no one has committed yet
		// abort transaction on all remaining nodes
		// mask system exception
		e.clear();
		get_checkpoint()->ckpt_abort_transaction(e);
		CL_PANIC(e.exception() == NULL);
		//
		// XXX verify this is handled right on directory updates
		//
		abort_transaction_copies(e);
		CL_PANIC(e.exception() == NULL);
		// raise exception to user
		e.exception(new ccr::lost_quorum());
		goto done;
	}

	unlock_copies(e);	// unfreeze reads
	CL_PANIC(e.exception() == NULL);

	//
	// update incore state if this transaction is performed on an
	// invalid CCR table.
	//
	ti = trans_mgr->lookup_table(cluster_name, table_name);
	CL_PANIC(ti);
	if (!ti->rec.valid) {
		//
		// This transaction is initiated by begin_transaction_invalid_ok
		// to update an invalid CCR table in cluster. Since the
		// transaction has just successfully committed, it is safe to
		// mark the table in question as valid here.
		//
		ti->rec.valid = true;
	}
	ti->rele();

	//
	// update incore state for directory update
	//
	if (is_directory_trans()) {
		if (is_directory_create()) {
			if (is_cluster_dir(table_name)) {
				ASSERT(user_cluster);
				trans_mgr->create_table_int(
				    user_cluster_id, user_cluster,
				    user_table);
			} else {
				//
				// The cluster id will not be used when
				// a table is being created.
				// It is ok to pass the cluster id of the
				// global cluster.
				//
				trans_mgr->create_table_int(0,
				    cluster_name, user_table);
			}
		} else {
			CL_PANIC(is_directory_remove());
			//
			// Do not remove the user_table from the TM's
			// hashtable yet, since the commit on that
			// table will need to have that entry around.
			// However, flag the user_table for removal
			// during that later table commit.
			//
			if (is_cluster_dir(table_name)) {
				ASSERT(user_cluster);
				ti = trans_mgr->lookup_table(user_cluster,
				    user_table);
			} else {
				ti = trans_mgr->lookup_table(cluster_name,
				    user_table);
			}
			ASSERT(ti && ti->running_trans);
			ti->running_trans->set_table_delete();
			ti->rele();
		}
	} else if (is_table_delete()) {
		remove_table = true;
	}

done:
	CCR_DBG(("primary calling add_commit for table %s in cluster %s\n",
	    table_name, cluster_name));
	// call with clear environment
	CORBA::Exception *saved_ex = e.release_exception();
	//
	// secondary may not do this til later
	// committed is piggybacked onto next ckpt
	//
	add_commit(e);
	CL_PANIC(e.exception() == NULL);
	// restore exception, if any
	if (saved_ex)
		e.exception(saved_ex);

	cleanup(remove_table);
}

//
// table_trans_impl::abort_transaction()
//   Undoes any updates that have been made as part of the transaction,
//   in the intermediate ccr files, and ends this transaction.
//   A retryable HA IDL method.
//   XXX - but aren't all the IDL methods in this class retryable HA?
//
void
table_trans_impl::abort_transaction(Environment &e)
{
	// Will continue if transaction_status is TRANS_FAILED
	if (transaction_status == TRANS_END) {
		return;
	}

	CCR_DBG(("in table_trans_impl::abort_transaction\n"));

	// Check to see if we have saved state
	primary_ctx *ctxp = primary_ctx::extract_from(e);
	abort_state *saved_state = (abort_state *)ctxp->get_saved_state();
	if (saved_state) {
		// this is a retry
		// need to refresh list of data servers from manager
		// XXX how do we know which copies are still valid?
		// states: START, ABORTED
		switch (saved_state->progress) {
		case abort_state::START:
			goto aborting;
		case abort_state::ABORTED:
			goto done;
		default:
			CL_PANIC(0);
		}
	} else {
		if (is_default_cluster(cluster_name)) {
			get_checkpoint()->ckpt_start_abort(table_name, e);
		} else {
			get_checkpoint()->ckpt_start_abort_zc(cluster_name,
			    table_name, e);
		}
		CL_PANIC(e.exception() == NULL);
	}

aborting:
	abort_transaction_copies(e);
	CL_PANIC(e.exception() == NULL);

done:
	CCR_DBG(("primary calling add_commit for table %s in cluster %s\n",
	    table_name, cluster_name));
	//
	// secondary may not do this til later
	// committed is piggybacked onto next ckpt
	//
	add_commit(e);
	CL_PANIC(e.exception() == NULL);

	// unlock table and mark transaction done
	cleanup(false);
}

//
// PROTECTED METHODS
//

//
// table_trans_impl::ckptwork_start_update()
//   Called by do_update() to perform a checkpoint operation before
//   starting an update. Initializes the progress of update_state
//   to START.
//
void
table_trans_impl::ckptwork_start_update(Environment &e)
{
	// allocate new update state object, with progress set
	// to START and obj set to the invoked object
	// register state object with framework for future use
	update_state *tx_state = new update_state(this);
	ASSERT(tx_state);
	tx_state->register_state(e);
}

//
// table_trans_impl::ckptwork_prepared_update()
//   Called by do_update() to checkpoint the successful completion
//   of the distribution of update copies to the nodes (i.e. the
//   .intent files were successfully created with the changes).
//   Sets progress to update_state::PREPARED.
//
void
table_trans_impl::ckptwork_prepared_update(Environment &e)
{
	// get context from framework
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	CL_PANIC(e.exception() == NULL);
	// get state registered earlier, check sanity
	update_state *saved_state = (update_state *)ctxp->get_saved_state();
	ASSERT(saved_state != NULL);
	CL_PANIC(saved_state->progress == update_state::START);
	// perform state transition
	saved_state->progress = update_state::PREPARED;
}

//
// table_trans_impl::ckptwork_rollback_update()
//   Called by do_update() to checkpoint the rollback of an update,
//   which happens when update copies could not be propagated
//   successfully to the nodes.
//   Sets progress to update_state::FAILED_PREPARE.
//
void
table_trans_impl::ckptwork_rollback_update(Environment &e)
{
	// get context from framework
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	CL_PANIC(e.exception() == NULL);
	// get state registered earlier, check sanity
	update_state *saved_state = (update_state *) ctxp->get_saved_state();
	ASSERT(saved_state != NULL);
	CL_PANIC(saved_state->progress == update_state::START);
	// perform state transition
	saved_state->progress = update_state::FAILED_PREPARE;
}

//
// table_trans_impl::ckptwork_abort_update()
//   Called by do_update() when the updates could not be written
//   into the .commit files on all the nodes. Also called by
//   update_state::orphaned() if it finds the progress of the
//   update_state to be PREPARED.
//   Sets progress to update_state::FAILED_COMMIT.
//
void
table_trans_impl::ckptwork_abort_update(Environment &e)
{
	// get context from framework
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	CL_PANIC(e.exception() == NULL);
	// get state registered earlier, check sanity
	update_state *saved_state = (update_state *) ctxp->get_saved_state();
	ASSERT(saved_state != NULL);
	CL_PANIC(saved_state->progress == update_state::PREPARED);
	// perform state transition
	saved_state->progress = update_state::FAILED_COMMIT;
	saved_state->transp->transaction_status = TRANS_FAILED;
}

//
// table_trans_impl::ckptwork_trans_modified()
//   Called by do_update() after the .commit files are successfully
//   updated on all the nodes, to mark the transaction state as
//   TRANS_MODIFIED.
//
void
table_trans_impl::ckptwork_trans_modified()
{
	set_modified();
}


//
// table_trans_impl::ckptwork_start_commit()
//   Called by commit_transaction() to checkpoint the beginning of
//   the commit operation. Sets the progress of commit_state to START.
//
void
table_trans_impl::ckptwork_start_commit(Environment &e)
{
	// allocate new commit state object, with progress set
	// to START and obj set to the invoked object
	// register state object with framework for future use
	commit_state *tx_state = new commit_state(this);
	ASSERT(tx_state != NULL);
	tx_state->register_state(e);
}

//
// table_trans_impl::ckptwork_abort_transaction()
//   Called by commit_transaction() when the changes could not be
//   committed on any of the nodes.
//   Sets progress to commit_state:: FAILED.
//
void
table_trans_impl::ckptwork_abort_transaction(Environment &e)
{
	// get context from framework
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	CL_PANIC(e.exception() == NULL);
	// get state registered earlier, check sanity
	commit_state *saved_state = (commit_state *) ctxp->get_saved_state();
	ASSERT(saved_state != NULL);
	CL_PANIC(saved_state->progress == commit_state::START);
	// perform state transition
	saved_state->progress = commit_state::FAILED;
}

//
// table_trans_impl::ckptwork_start_abort()
//   Called by abort_transaction() to checkpoint the beginning of the
//   abort operation. Sets progress of abort_state to START.
//
void
table_trans_impl::ckptwork_start_abort(Environment &e)
{
	// allocate new abort state object, with progress set
	// to START and obj set to the invoked object
	// register state object with framework for future use
	abort_state *tx_state = new abort_state(this);
	ASSERT(tx_state != NULL);
	tx_state->register_state(e);
}


//
// PRIVATE METHODS
//

//
// table_trans_impl::do_update()
//   This is the generic, common method called by all types of updates
//   on a file. The update type together with the elements to be
//   updated are passed in a update_info structure. The successful
//   execution of all the steps results in a .commit file on all the
//   nodes that reflects the changes requested by this update.
//
//   private update method
//   non-idempotent one-phase mini transaction
void
table_trans_impl::do_update(update_info *ui, Environment &e)
{
	CORBA::Exception *saved_ex = NULL;


	// Check to see if we have saved state
	primary_ctx *ctxp = primary_ctx::extract_from(e);
	update_state *saved_state = (update_state *)ctxp->get_saved_state();
	if (saved_state) {
		// this is a retry
		// refresh list of data servers from manager
		switch (saved_state->progress) {
		case update_state::START:
			goto started;
		case update_state::PREPARED:
			goto prepared;
		case update_state::FAILED_PREPARE:
			// prepare_update failed, continue with rollback
			// and return exception
			rollback_update_copies(e);
			CL_PANIC(e.exception() == NULL);
			add_commit(e);
			CL_PANIC(e.exception() == NULL);
			e.exception(new ccr::out_of_service);
			return;
		case update_state::FAILED_COMMIT:
			//
			// Primary died after commit_update_copies failed.
			// We have to redo the whole procedure to abort
			// the transaction.
			//
			CL_PANIC(transaction_status == TRANS_FAILED);
			abort_transaction_copies(e);
			CL_PANIC(e.exception() == NULL);
			add_commit(e);
			CL_PANIC(e.exception() == NULL);
			e.exception(new ccr::out_of_service);
			return;
		case update_state::COMMITTED:
			return;
		case update_state::ABORTED:
			e.exception(new ccr::out_of_service);
			return;
		}
	} else {
		// sanity check
		if (transaction_status == TRANS_FAILED ||
		    transaction_status == TRANS_END) {
			e.exception(new ccr::out_of_service);
			return;
		}
		CCR_DBG(("TM calling ckpt_start_update\n"));
		if (is_default_cluster(cluster_name)) {
			get_checkpoint()->ckpt_start_update(table_name, e);
		} else {
			get_checkpoint()->ckpt_start_update_zc(cluster_name,
			    table_name, e);
		}
		CL_PANIC(e.exception() == NULL);
	}

started:

	prepare_update_copies(ui, e);

	if (e.exception()) {
		saved_ex = e.release_exception();
		// checkpoint to mark update failed
		// user may continue with transaction
		get_checkpoint()->ckpt_rollback_update(e);
		CL_PANIC(e.exception() == NULL);
		rollback_update_copies(e);
		CL_PANIC(e.exception() == NULL);

		add_commit(e);
		CL_PANIC(e.exception() == NULL);

		e.exception(saved_ex);
		return;
	}

	// checkpoint to mark update done
	CCR_DBG(("TM calling ckpt_prepared_update\n"));
	get_checkpoint()->ckpt_prepared_update(e);
	CL_PANIC(e.exception() == NULL);

prepared:
	// update succeeded, now try to commit
	commit_update_copies(e);
	if (e.exception()) {
		saved_ex = e.release_exception();

		//
		// some copies may have committed, so have
		// to abort entire transaction to keep
		// copies from having inconistent state
		//
		get_checkpoint()->ckpt_abort_update(e);
		CL_PANIC(e.exception() == NULL);

		CCR_DBG(("Call abort_transaction_copies after "
		    "commit_update_copies failed\n"));

		// abort_transaction_copies clears exceptions before returning
		abort_transaction_copies(e);
		CL_PANIC(e.exception() == NULL);

		// Finish this mini transaction
		add_commit(e);
		CL_PANIC(e.exception() == NULL);

		//
		// Set the transaction_status to TRANS_FAILED so that
		// subsequent updates and commit_transaction won't
		// be able to be initiated. After transaction_status
		// becomes TRANS_FAILED, only abort_transaction can be
		// initiated.  So even if client continues to call
		// do_update or commit_transaction, they will fail at the
		// sanity check at the very beginning
		//
		transaction_status = TRANS_FAILED;
		e.exception(saved_ex);
	} else {
		set_modified();
		get_checkpoint()->ckpt_trans_modified(e);
		CL_PANIC(e.exception() == NULL);
		// if primary dies before checkpointing
		// secondary will retry commit_update
		// okay since method is idempotent

		// all done
		CCR_DBG(("do_update calling add_commit\n"));
		add_commit(e);
		CL_PANIC(e.exception() == NULL);
	}
}

//
// table_trans_impl::prepare_update_copies()
//   Called by do_update() to prepare .intent files on all the nodes.
//   Calls methods on arrays of updatable_table_copies.
//
void
table_trans_impl::prepare_update_copies(update_info *ui, Environment &e)
{
	Environment		e2;
	CORBA::Exception	*ex;
	ccr::system_error	*err;
	char			nodename[CL_MAX_LEN+1];

	// Fault point for ccr_fi
	FAULTPT_CCR(FAULTNUM_CCR_PREPARE_UPDATE_COPIES,
	    fault_ccr::generic_test_op);

	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}

		switch (ui->op) {
		case ccr::CCR_ADD:
			CL_PANIC(ui->seq);
			copies[i]->add_elements(*(ui->seq), e2);
			break;
		case ccr::CCR_REMOVE:
			copies[i]->remove_elements(*(ui->seq), e2);
			break;
		case ccr::CCR_UPDATE:
			copies[i]->update_element(ui->key, ui->data, e2);
			break;
		case ccr::CCR_REMOVE_ALL:
			copies[i]->remove_all_elements(e2);
			break;
		case ccr::CCR_INVOP:
			// This should never happen.
			CL_PANIC(0);
			return;
		}
		if ((ex = e2.release_exception()) != NULL) {
			clconf_get_nodename(local_ccr_ids[i], nodename);
			e.exception(ex);
			if (DS_UNREACHABLE(ex)) {
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: CCR data server on "
				    "node %s unreachable while updating "
				    "table %s in cluster %s.", nodename,
				    table_name, cluster_name);
				CCR_DBG(("CCR TM: DS on node %s unreachable in "
				    "prepare_update_copies\n", nodename));
				copies[i] = nil;
				e.clear();

				// check if remaining copies have quorum
				if (check_update_quorum()) {
					continue;
				} else {
					// mask system exception
					e.exception(new
						ccr::lost_quorum());
					return;
				}
			} else if ((err = ccr::system_error::_exnarrow(ex)) !=
			    NULL) {
				//
				// SCMSGS
				// @explanation
				// The indicated error occurred while updating
				// the the indicated table on the indicated
				// node.
				//
				// The errno value indicates the nature of the
				// problem. errno values are defined in the
				// file /usr/include/sys/errno.h. An errno
				// value of 28 (ENOSPC) indicates that the
				// root file system on the node is full. Other
				// values of errno can be returned when the
				// root disk has failed (EIO) or some of the
				// CCR tables have been deleted outside the
				// control of the cluster software (ENOENT).
				// @user_action
				// There may be other related messages on the
				// node where the failure occurred. These may
				// help diagnose the problem. If the root file
				// system is full on the node, then free up
				// some space by removing unnecessary files.
				// If the indicated table was accidently
				// deleted, then boot the offending node in -x
				// mode to restore the indicated table from
				// other nodes in the cluster. The CCR tables
				// are located at /etc/cluster/ccr/. If the
				// root disk on the afflicted node has failed,
				// then it needs to be replaced.
				//
				(void) ccr_syslog_msg.log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "CCR: Can't access table %s (cluster %s)"
				    " while updating it on node %s, errno = "
				    "%d.", table_name, cluster_name, nodename,
				    err->errnum);
				CCR_DBG(("CCR TM: system error in "
				    "prepare_update_copies on "
				    "DS %s\n", nodename));
				return;
			} else {
				//
				// In this condition, the exception(such as
				// duplicated keys) should be able to be
				// handled by the client, hence no need
				// to syslog.
				//
				CCR_DBG(("CCR TM: update table %s (cluster %s)"
				    " invocation on DS %s failed\n",
				    table_name, cluster_name, nodename));
				return;
			}
		}
	}
}

//
// table_trans_impl::commit_update_copies()
//   Called by do_update() to copy the .intent files into the .commit
//   files on all the nodes.
//
void
table_trans_impl::commit_update_copies(Environment &e)
{
	Environment		e2;
	CORBA::Exception	*ex;
	ccr::system_error	*err;
	char			nodename[CL_MAX_LEN+1];

	// Fault point for ccr_fi
	FAULTPT_CCR(FAULTNUM_CCR_COMMIT_UPDATE_COPIES,
	    fault_ccr::generic_test_op);

	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}

		copies[i]->commit_update(e2);
		if ((ex = e2.release_exception()) != NULL) {
			clconf_get_nodename(local_ccr_ids[i], nodename);
			e.exception(ex);
			if (DS_UNREACHABLE(ex)) {
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE, "CCR: CCR data server on "
				    "node %s unreachable while updating "
				    "table %s in cluster %s.", nodename,
				    table_name, cluster_name);
				CCR_DBG(("CCR TM: DS on node %s unreachable in "
				    "commit_update\n", nodename));
				copies[i] = nil;
				e.clear();

				// check if remaining copies have quorum
				if (check_update_quorum()) {
					continue;
				} else {
					// mask system exception
					e.exception(new
						ccr::lost_quorum());
					return;
				}
			} else if ((err = ccr::system_error::_exnarrow(ex)) !=
			    NULL) {
				// something else went wrong on DS

				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE,
				    "CCR: Can't access table %s (cluster %s)"
				    " while updating it on node %s, errno = "
				    "%d.", table_name, cluster_name, nodename,
				    err->errnum);
				CCR_DBG(("CCR TM: system error in "
				    "commit_update on node %s\n", nodename));
				return;
			}
		}
	}
}

//
// table_trans_impl::rollback_update_copies()
//   Called by do_update() when a call to prepare_update_copies()
//   fails. Removes the .intent files on all the DSs.
//
void
table_trans_impl::rollback_update_copies(Environment &)
{
	Environment	env;
	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}
		copies[i]->rollback_update(env);
		// ignore exceptions for now
		env.clear();
	}
}

//
// table_trans_impl::commit_transaction_copies()
//   Called by commit_transaction() to make the updates persistent.
//   This results in incoming readers freezing until the commit
//   is complete, at the nodes. It also increments the version number
//   of the table at the DSs, thus invalidating any readers having
//   a handle to the original table.
//
void
table_trans_impl::commit_transaction_copies(Environment &e)
{
	Environment	e2;
	CORBA::Exception	*ex;
	bool		partial_commit = false;
	char		nodename[CL_MAX_LEN+1];

	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}

		// take a deep breath
		copies[i]->commit_transaction(e2);

		if ((ex = e2.release_exception()) != NULL) {
			e.exception(ex);
			clconf_get_nodename(local_ccr_ids[i], nodename);
			CL_PANIC(DS_UNREACHABLE(ex));

			(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
			    MESSAGE, "CCR: CCR data server on node %s "
			    "unreachable while updating table "
			    "%s in cluster %s.", nodename, table_name,
			    cluster_name);
			CCR_DBG(("CCR TM: DS unreachable on node %s in "
			    "commit_transaction\n", nodename));
			copies[i] = nil;

			// check if remaining copies have quorum
			if (check_update_quorum()) {
				e.clear();
				continue;
			} else if (!partial_commit) {
				// no one committed yet, safe to abort
				// leave exception in place
				return;
			} else if (check_cluster_shutdown()) {
				// Some copies have committed but
				// we have lost quorum because
				// the cluster is shuting down.
				// Stop the commit immediately.
				// It is not needed to panic the node since
				// shutdown is in progress

				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) ccr_syslog_msg.log(
				    SC_SYSLOG_WARNING, MESSAGE,
				    "CCR: Cluster has lost quorum while "
				    "updating table %s (cluster = %s), it "
				    "is possibly in an inconsistent state.",
				    table_name, cluster_name);
				return;
			} else {
				// some copies have committed but
				// we have lost quorum
				// need to abort cluster to prevent
				// data corruption

				//
				// SCMSGS
				// @explanation
				// The cluster lost quorum while the indicated
				// table was being changed, leading to
				// potential inconsistent copies on the nodes.
				// @user_action
				// Check if the indicated table are consistent
				// on all the nodes in the cluster, if not,
				// boot the cluster in -x mode to restore the
				// indicated table from backup. The CCR tables
				// are located at /etc/cluster/ccr/.
				//
				(void) ccr_syslog_msg.log(
				    SC_SYSLOG_PANIC, MESSAGE,
				    "CCR: Cluster has lost quorum while "
				    "updating table %s (cluster = %s), it"
				    " is possibly in an inconsistent state - "
				    "ABORTING.", table_name, cluster_name);
			}
		} else
			partial_commit = true;
	}
}

//
// table_trans_impl::unlock_copies()
//   This calls the unlock() method on the updatable_copy_impl
//   instance on each node, which releases the read-write lock
//   on the table at that node, thereby unblocking any read attempts
//   on the table. It also invokes callbacks on clients registered for
//   notification for updates to this table.
//
void
table_trans_impl::unlock_copies(Environment &)
{
	//
	// We need to use a new local env here. UTC will check is_orphan()
	// that is associated with the passed env. If the invocation is made
	// from the primary to the UTC on the primary, reusing the env could
	// pass some wrong orphan information that doesn't belong to this
	// invocation but previous invocation. Refer to bug #4316589 for more
	// details.
	//
	Environment	e;

	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}
		copies[i]->unlock(e);
		// ignore exceptions for now
		if (e.exception()) {
			e.clear();
		}
	}
}

//
// table_trans_impl::abort_transaction_copies()
//   This undoes the updates made in this transaction, removing the
//   intent and commit files at the nodes. Also, unfreezes any
//   readers blocked, waiting on the rwlock to the table.
//
void
table_trans_impl::abort_transaction_copies(Environment &)
{
	Environment	e;

	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}
		copies[i]->abort_transaction(e);
		// ignore exceptions for now
		if (e.exception()) {
			e.clear();
		}
	}
}

//
// table_trans_impl::clear_transaction_copies()
//   Called by commit_transaction() if there were no updates
//   made within the transaction. Except for the different
//   method called on the copies[], it is almost the same
//   as the abort_transaction_copies() method.
void
table_trans_impl::clear_transaction_copies(Environment &)
{
	Environment	e;

	for (uint_t i = 0; i < num_copies; i++) {
		if (CORBA::is_nil(copies[i])) {
			continue;
		}
		copies[i]->clear_transaction(e);
		// ignore exceptions for now
		if (e.exception()) {
			e.clear();
		}
	}
}

//
// table_trans_impl::check_cluster_shutdown()
//   Check a cluster shutdown is in progress
//
bool
table_trans_impl::check_cluster_shutdown()
{
	boolean_t cluster_shutdown_flag;
	nodeid_t node_id;
	int retval;

	node_id = clconf_get_local_nodeid();
	retval = clconf_get_cluster_shutdown(node_id, &cluster_shutdown_flag);
	CL_PANIC(!retval);
	return ((bool) cluster_shutdown_flag);
}

//
// table_trans_impl::check_update_quorum()
//   This is typically called after an invocation gets a system
//   exception. It checks if the remaining DSs constitute quorum.
//
bool
table_trans_impl::check_update_quorum()
{
	quorum::ccr_id_list_t ccr_ids;
	bool retval;
	Environment e;
	uint_t num_avail = 0;


	// Can not use the local_ccr_ids sequence directly
	// by removing elements that have nil copies[] counterparts
	// since the copies array is not dynamically changed and
	// this method assumes a 1-1 corresp between copies and
	// local_ccr_ids.

	CL_PANIC(!CORBA::is_nil(trans_mgr->quorum_obj));

	// pass sequence of ccr_ids to cmm
	ccr_ids.length(num_copies);	// gets destructed on return

	for (uint_t i = 0; i < num_copies; i++) {
		if (!CORBA::is_nil(copies[i])) {
			ccr_ids[num_avail++] = local_ccr_ids[i];
		}
	}
	if (num_avail < num_copies) {
		ccr_ids.length(num_avail);
	}
	retval = trans_mgr->quorum_obj->check_ccr_quorum(ccr_ids, e);
	if (e.exception()) {
		e.clear();
		return (false);
	}
	return (retval);
}

//
// Checkpoint accessor function.
//
ccr_trans::ckpt_ptr
table_trans_impl::get_checkpoint()
{
	return ((ccr_repl_prov*)(get_provider()))->get_checkpoint_ccr_trans();
}


//
//			transaction_state METHODS
//
// Each of the three transaction_state objects represent three diff.
// types of CCR transaction related mini-transactions and have the
// following methods defined below:
// (i)   a constructor, which essentially sets the progress field to
//	 START and transp to this transaction.
// (ii)  a destructor which sets transp to NULL.
// (iii) a committed() method which is called by the HA framework as
//	 a result of an add_commit() call by the transaction object.
// (iv)  an orphaned() method which is called by the HA framework as
//	 a result of both the client and the primary dying.
//

update_state::update_state(table_trans_impl *tp):
	transaction_state(),
	progress(START),
	transp(tp)
{
}

update_state::~update_state()
{
	CCR_DBG(("in update_state destructor\n"));
	transp = NULL;
}

void
update_state::committed()
{
	// called by framework as a result of add_commit()
	CCR_DBG(("update committed\n"));

	switch (progress) {
	case PREPARED:
		progress = COMMITTED;
		break;
	case FAILED_PREPARE:
		progress = ABORTED;
		break;
	case FAILED_COMMIT:
		// The primary has aborted the transaction due to
		// errors in commit_update_copies.
		CL_PANIC(transp->transaction_status ==
		    table_trans_impl::TRANS_FAILED);
		progress = ABORTED;
		break;
	case START:
		//
		// This can only happen if the mini-transaction becomes orphaned
		// (primary and client die) before prepare_update_copies()
		// finishes. In this case, we know that the orphaned() method on
		// the new primary has aborted the ccr transaction.
		//
		progress = ABORTED;
		break;
	case COMMITTED:
		// should not be committed twice
	case ABORTED:
		// should not be committed after being aborted
	default:
		CL_PANIC(0);
	}
}

void
update_state::orphaned(Environment &e)
{
	// called by framework when client and primary both die
	CCR_DBG(("transaction orphaned, aborting\n"));

	switch (progress) {
	case PREPARED:
	    // update needs to be backed out
	    transp->get_checkpoint()->ckpt_abort_update(e);
	    // fall thru
	case START:
	case FAILED_PREPARE:
	case FAILED_COMMIT:
	    transp->abort_transaction_copies(e);
	    CL_PANIC(e.exception() == NULL);
	    transp->cleanup(false);
	    break;
	case COMMITTED:
	case ABORTED:
	default: // nothing to do
	    break;
	}
}

commit_state::commit_state(table_trans_impl *tp):
	transaction_state(),
	progress(START),
	transp(tp)
{
}

commit_state::~commit_state()
{
	CCR_DBG(("in commit_state destructor\n"));
	transp = NULL;
}

void
commit_state::committed()
{
	// called by framework as a result of add_commit()
	CCR_DBG(("secondary in commit_state committed\n"));

	bool remove_table = false;

	if (progress == START) {
		progress = COMMITTED;
	} else {
		CL_PANIC(progress == FAILED);
		progress = ABORTED;
		transp->cleanup(remove_table);
		return;
	}

	table_info	*ti;
	manager_impl	*t_mgr = transp->trans_mgr;
	//
	// update incore state if this transaction is performed on an
	// invalid CCR table.
	//

	CL_PANIC(transp->cluster_name);
	CL_PANIC(transp->table_name);
	ti = t_mgr->lookup_table(transp->cluster_name, transp->table_name);
	CL_PANIC(ti);
	if (!ti->rec.valid) {
		//
		// This transaction is initiated by begin_transaction_invalid_ok
		// to update an invalid CCR table in cluster. Since the
		// transaction has just successfully committed, it is safe
		// to mark the table in question as valid here.
		//
		ti->rec.valid = true;
	}
	ti->rele();

	//
	// update incore state for directory update
	//
	if (transp->is_table_delete()) {
		CCR_DBG(("Table %s marked for delete\n", transp->table_name));
		remove_table = true;
	} else if (transp->is_directory_trans()) {
		char *user_table = transp->user_table;
		char *user_cluster = transp->user_cluster;
		uint_t user_cluster_id = transp->user_cluster_id;
		char *cluster_name = transp->cluster_name;
		CL_PANIC(cluster_name);
		CL_PANIC(user_table);
		if (transp->is_directory_create()) {
			if (is_cluster_dir(transp->table_name)) {
				t_mgr->create_table_int(user_cluster_id,
				    user_cluster, user_table);
			} else {
				//
				// The cluster id will not be used when
				// a table is being created. It is ok
				// to pass the cluster id of the global
				// cluster here.
				//
				t_mgr->create_table_int(0, cluster_name,
				    user_table);
			}
		} else {
			CL_PANIC(transp->is_directory_remove());
			//
			// Do not remove the hashtable entry for
			// the user_table yet, but mark it to be
			// removed during the commit of the
			// user_table transaction.
			//
			if (is_cluster_dir(transp->table_name)) {
				ti = t_mgr->lookup_table(user_cluster,
				    user_table);
			} else {
				ti = t_mgr->lookup_table(cluster_name,
				    user_table);
			}
			ti->running_trans->set_table_delete();
			CCR_DBG(("Table info ptr = %p\n", ti));
			ASSERT(ti && ti->running_trans);
			ti->rele();
		}
	}

	// unlock table for writing
	transp->cleanup(remove_table);
}

void
commit_state::orphaned(Environment &e)
{
	// called by framework when client and primary both die
	CCR_DBG(("transaction orphaned, aborting\n"));

	switch (progress) {
	case START:
	    // some copies may have committed
	    // retry commit on all copies
	    transp->commit_transaction(e);
	    // no one will receive exception anyway
	    e.clear();
	    break;
	case FAILED:
	    transp->abort_transaction_copies(e);
	    CL_PANIC(e.exception() == NULL);
	    transp->cleanup(false);
	    break;
	case COMMITTED:
	case ABORTED:
	default: // nothing to do
	    break;
	}
}

abort_state::abort_state(table_trans_impl *tp):
	transaction_state(),
	progress(START),
	transp(tp)
{
}

abort_state::~abort_state()
{
	CCR_DBG(("in abort_state destructor\n"));
	transp = NULL;
}

void
abort_state::committed()
{
	// called by framework as a result of add_commit()
	CCR_DBG(("secondary in abort_state committed\n"));

	CL_PANIC(progress == START);
	progress = ABORTED;

	// unlock table for writing
	transp->cleanup(false);
}

void
abort_state::orphaned(Environment &e)
{
	// called by framework when client and primary both die
	CCR_DBG(("transaction orphaned, aborting\n"));

	if (progress == START) {
		//
		// retry abort on all copies
		//
		// It is ok to invoke abort_transaction here rather than
		// abort_transaction_copies as we did in other orphaned
		// code because the state object remains the same.
		// So that it is just like a retry.
		//
		transp->abort_transaction(e);
		e.clear();
	}
}
