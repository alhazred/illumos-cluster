/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  ccr_repl_prov.h - HA service provider for CCR Transaction Manager
 */

#ifndef _CCR_REPL_PROV_H
#define	_CCR_REPL_PROV_H

#pragma ident	"@(#)ccr_repl_prov.h	1.33	08/08/07 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#include <h/ccr_trans.h>
#include <repl/service/replica_tmpl.h>
#include <repl/service/transaction_state.h>

// forward decls
class manager_impl;
class table_trans_impl;
class table_info;
class regis_info;

//
// The CCR is a HA service
//
class ccr_repl_prov : public repl_server<ccr_trans::ckpt>
{
	friend class manager_impl;
	friend class table_trans_impl;
	friend class table_info;
	friend class regis_info;
public:
	ccr_repl_prov(char *);
	~ccr_repl_prov();

	// checkpoint methods
	void ckpt_create_manager(ccr_trans::manager_ptr, Environment &);
	void ckpt_reg_data_server(ccr_data::data_server_ptr,
			quorum::quorum_device_id_t, ccr_trans::handle_ptr,
			bool deactivated, Environment &);
	void ckpt_unreg_data_server(ccr_data::data_server_ptr, Environment &);
	void ckpt_recovery_done(const ccr::element_seq &,
				ccr_data::epoch_type epoch_num,
				const ccr_data::consis_seq &,
				Environment &);

	void ckpt_recovery_done_zc(uint_t cluster_id,
				const char *cluster,
				uint_t num_tables,
				const ccr::element_seq &,
				const ccr_data::consis_seq &,
				Environment &);

	void ckpt_all_zc_recovery_done(Environment &);

	void ckpt_create_transaction(ccr::updatable_table_ptr, const char *,
			const ccr_data::updatable_copy_seq &,
			const quorum::ccr_id_list_t &, bool,
			Environment &);

	void ckpt_create_transaction_zc(ccr::updatable_table_ptr, const char *,
			const char *, const ccr_data::updatable_copy_seq &,
			const quorum::ccr_id_list_t &, bool,
			Environment &);

	void ckpt_start_update(const char *, Environment &);
	void ckpt_start_update_zc(const char *, const char *, Environment &);

	void zc_ckpt_op(ccr::update_type, uint_t, const char *,
	    Environment &);

	void ckpt_directory_op(ccr::update_type, const char *, Environment &);
	void ckpt_directory_op_zc(ccr::update_type, const char *, const char *,
				Environment &);
	void ckpt_table_delete(const char *, Environment &);
	void ckpt_table_delete_zc(const char *, const char *, Environment &);

	void ckpt_prepared_update(Environment &);
	void ckpt_trans_modified(Environment &);
	void ckpt_rollback_update(Environment &);
	void ckpt_abort_update(Environment &);
	void ckpt_abort_transaction(Environment &);

	void ckpt_start_commit(const char *, Environment &);
	void ckpt_start_commit_zc(const char *, const char *, Environment &);

	void ckpt_start_abort(const char *, Environment &);
	void ckpt_start_abort_zc(const char *, const char *, Environment &);
	void ckpt_service_version(unsigned short, unsigned short,
	    Environment &);

	// replica::repl_prov methods
	void become_secondary(Environment &e);
	void add_secondary(replica::checkpoint_ptr, const char *,
			Environment &);
	void remove_secondary(const char *, Environment &);
	void freeze_primary_prepare(Environment &);
	void freeze_primary(Environment &);
	void unfreeze_primary(Environment &);
	void become_spare(Environment &);
	void become_primary(const CORBA::StringSeq&, Environment &);
	CORBA::Object_ptr get_root_obj(Environment &);
	void shutdown(Environment &);
	static void initialize();
	void startup(Environment &);

	// Checkpoint accessor function.
	ccr_trans::ckpt_ptr	get_checkpoint_ccr_trans();

	void upgrade_callback(const version_manager::vp_version_t &,
	    Environment &);

	// Set the initial version number.
	void set_init_version(const version_manager::vp_version_t &);

protected:
	manager_impl *mgr;			// ccr manager

private:
	version_manager::vp_version_t	current_version;

	//
	// Provides locking between calls to upgrade callbacks and
	// become_primary, where we need to update the current
	// version.
	//
	os::rwlock_t	version_lock;

	ccr_trans::manager_ptr root_obj_ref;	// objref to mgr
	// Checkpoint proxy.
	ccr_trans::ckpt_ptr	_ckpt_proxy;
};

class ccr_upgrade_callback:
	public McServerof<version_manager::upgrade_callback>
{
public:
	ccr_upgrade_callback(ccr_repl_prov&);
	~ccr_upgrade_callback();

	void _unreferenced(unref_t unref);

	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version,
	    Environment &);
private:
	ccr_repl_prov  &prov;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif	/* _CCR_REPL_PROV_H */
