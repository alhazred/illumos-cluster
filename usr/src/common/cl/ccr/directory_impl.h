/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  directory_impl.h - CCR directory object
 *
 */

#ifndef _DIRECTORY_IMPL_H
#define	_DIRECTORY_IMPL_H

#pragma ident	"@(#)directory_impl.h	1.24	08/07/17 SMI"

#include <orb/object/adapter.h>
#include <h/ccr.h>
#include <h/ccr_trans.h>
#include <h/ccr_data.h>

// directory_impl: implements the CCR user interface
// one directory per cluster node

class directory_impl : public McServerof<ccr::directory> {
	//
	// Public interface
	//
public:
	directory_impl(ccr_data::data_server_ptr);
	~directory_impl();
	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//

	ccr::readonly_table_ptr lookup_zc(const char *, const char *,
	    Environment &);
	ccr::readonly_table_ptr lookup(const char *, Environment &);

	void zc_get_names_and_ids(
	    CORBA::StringSeq_out, CORBA::StringSeq_out, CORBA::Environment &);

	void get_table_names_zc(const char *, CORBA::StringSeq_out,
	    Environment &);
	void get_table_names(CORBA::StringSeq_out, Environment &);

	void get_invalid_table_names_zc(const char *, CORBA::StringSeq_out,
	    Environment &);
	void get_invalid_table_names(CORBA::StringSeq_out, Environment &);

	uint_t zc_getidbyname(const char *, Environment &);
	char *zc_getnamebyid(uint_t, Environment &);

	void zc_create(const char *, Environment &);

	void create_table_zc(const char *, const char *, Environment &);
	void create_table(const char *, Environment &);

	void zc_remove(const char *, Environment &);

	void remove_table_zc(const char *, const char *, Environment &);
	void remove_table(const char *, Environment &);

	ccr::updatable_table_ptr begin_transaction_zc(const char *,
						const char *,
						Environment &);
	ccr::updatable_table_ptr begin_transaction(const char *,
						Environment &);

	ccr::updatable_table_ptr begin_transaction_invalid_ok_zc(
						const char *,
						const char *,
						Environment &);
	ccr::updatable_table_ptr begin_transaction_invalid_ok(const char *,
						Environment &);

	void register_callback(ccr::callback_ptr, Environment &);
	void unregister_callback(ccr::callback_ptr, Environment &);

	void zc_register_callback(const char *, ccr::callback_ptr,
	    Environment &);
	void zc_unregister_callback(const char *, ccr::callback_ptr,
	    Environment &);

	void register_cluster_callback(ccr::callback_ptr,
	    Environment &);
	void unregister_cluster_callback(ccr::callback_ptr,
	    Environment &);
private:
	// private methods
	ccr_trans::manager_ptr lookup_tm();
	int update_tm_ref(Environment &);
	ccr::updatable_table_ptr begin_transaction_common(const char *,
	    const char *, bool, Environment &);
	//
	// Called by create_table and remove_table to make sure that
	// the user is not attempting to add or remove the CCR metadata.
	//
	// XXX in the future if more metadata files are added into CCR,
	// they should also be checked here.
	//
	void check_tablename(const char *, Environment &) const;
	//
	// Called by create_cluster and remove_cluster to make sure
	// that the user is not attempting to add or remove clusters
	// with invalid cluster names.
	//
	void check_clustername(const char *, Environment &) const;

	//
	// Utility functions for create table and remove table interfaces.
	//
	void create_table_common_zc(const char *, const char *,
	    Environment &);
	void remove_table_common_zc(const char *, const char *,
	    Environment &);

	//
	// utility routines to add or remove callbacks to a table in a cluster
	//
	void register_callback_in(const char *, const char *,
	    ccr::callback_ptr, Environment &);
	void unregister_callback_in(const char *, const char *,
	    ccr::callback_ptr, Environment &);

	os::mutex_t version_lock;
	// manager, local data server
	ccr_trans::manager_ptr ccr_mgr;
	ccr_data::data_server_ptr data_svr;
};

#endif	/* _DIRECTORY_IMPL_H */
