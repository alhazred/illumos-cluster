//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// table_access.h: synchronization class for table access used by DS.
//

#ifndef _TABLE_ACCESS_H
#define	_TABLE_ACCESS_H

#pragma ident	"@(#)table_access.h	1.5	08/07/17 SMI"

#include <sys/os.h>
#include <sys/refcnt.h>
#include <sys/list_def.h>
#include <h/ccr.h>
#include <h/ccr_data.h>

//
// table_access: synchronization class for table access
// one table_access object per table per data server
// similar to rwlock, permits multiple readers/single writer
//
class table_access : public refcnt {
	//
	// Public interface
	//
public:
	table_access();
	~table_access();
	void begin_reader();
	void end_reader();
	void begin_writer();
	void end_writer();
	bool add_callback(ccr::callback_ptr);
	bool remove_callback(ccr::callback_ptr);
	ccr_data::callback_seq *get_callbacks();
	void flag_recovered(const ccr_data::consis &);
	bool was_recovered();
	void call_did_recovery(const char *, const char *);
	void call_did_update(const char *, const char *, ccr::ccr_update_type);

	ccr_data::consis rec;	// consistency info
	// use rec.version to invalidate readers

private:
	//
	// Private methods
	//

	// use separate locks for read-write locking and callbacks
	// otherwise callbacks that read from modified table
	// would result in deadlock
	os::mutex_t mutex;
	os::mutex_t cb_mutex;
	os::condvar_t cv;
	uint_t num_readers;
	bool write_held;
	bool writer_waiting;

	// list of callbacks registered for this table
	SList<ccr::callback> callbacks;
	//
	// indicate if the table has been replaced during ccr recovery
	// and hence if callbacks registered for it should be invoked.
	//
	bool recovered;
};

#endif	/* _TABLE_ACCESS_H */
