//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// persistent_table.cc - persistent storage objects
//

#pragma ident	"@(#)persistent_table.cc	1.41	08/12/17 SMI"

#include <errno.h>
#include <sys/os.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ccr/persistent_table.h>
#include <ccr/common.h>
#include <orb/fault/fault_injection.h>

#ifndef _KERNEL
#include <unistd.h>
#endif

#include <md5.h>

// Syslog message
os::sc_syslog_msg ccr_syslog_msg(CCR_SERVICE_TAG, "", NULL);

// test for separator in ccr files
#define	SEP '\t'
#define	IS_SEP(x) ((x) == ' ' || (x) == SEP)

table_element_t::table_element_t():
	key(NULL),
	data(NULL) {}

table_element_t::~table_element_t()
{
	if (key)
		delete [] key;
	if (data)
		delete [] data;
}

readonly_persistent_table::readonly_persistent_table():
#ifndef _KERNEL
	ifs(NULL),
#else
	bpfile(NULL),
	offset(0),
#endif
	table_pathname(NULL),
	eof_reached(0)
{
}

readonly_persistent_table::~readonly_persistent_table()
{
	delete [] table_pathname;
#ifndef _KERNEL
	ASSERT(ifs == NULL);
	ifs = NULL;
#else
	ASSERT(bpfile == NULL);
	bpfile = NULL;	// For lint
#endif
}

int
readonly_persistent_table::initialize(const char *tab_pathname)
{
	table_pathname = new char[os::strlen(tab_pathname) + 1];
	ASSERT(table_pathname);
	(void) os::strcpy(table_pathname, tab_pathname);
#ifndef _KERNEL
	struct stat statbuf;
	if (stat(table_pathname, &statbuf) != 0) {
		return (errno);
	}
	ifs = fopen(table_pathname, "r");
	if (ifs == NULL) {
		return (errno);
	}
#else
#ifdef FAULT_INJECTION
	//
	// If we are causing a system error on the cluster, cause it on
	// this node and clear the trigger so that only this node has
	// the system error.
	//
	if (fault_triggered(FAULTNUM_CCR_SYSTEM_ERROR_CLUSTER, NULL, NULL)) {
		NodeTriggers::clear(FAULTNUM_CCR_SYSTEM_ERROR_CLUSTER,
		    TRIGGER_ALL_NODES);
		NodeTriggers::add(FAULTNUM_CCR_SYSTEM_ERROR_NODE, NULL, NULL);
		return (EACCES);
	}
#endif // FAULT_INJECTION

	bpfile = kobj_open_file((char *)table_pathname);
	if (bpfile == (struct _buf *)-1) {
		bpfile = NULL;
		return (ENOENT);
	}
	offset = 0;
#endif

	return (0);
}

int
readonly_persistent_table::compute_checksum(unsigned char *digest)
{
	// digest must be 16 bytes long
	MD5_CTX context;
	char *row;
	uint32_t rowlen;
	int retval = 0;

	MD5Init(&context);
	atfirst();
	while ((row = next_row(retval)) != NULL) {
		if (!is_meta_row(row)) {
			rowlen = (uint32_t)os::strlen(row);
			MD5Update(&context, (unsigned char *) row, rowlen);
		}
		delete [] row;
	}
	if (retval)
		return (retval);
	else {
		MD5Final(digest, &context);
		return (0);
	}
}

int
readonly_persistent_table::next_char(int &errval)
{
	int c;
#ifdef _KERNEL
	int num_read;
	char tmpbuf[2];
#endif
	errval = 0;

#ifdef _KERNEL
	num_read = kobj_read_file(bpfile, tmpbuf, 1, offset);
	if (num_read == 0) {
		eof_reached = 1;
		return (-1);
	} else if (num_read == -1) {
		errval = EIO;
		return (-1);
	}
	CL_PANIC(num_read == 1);
	offset++;
	c = *tmpbuf;
	return (c);
#else
	CL_PANIC(ifs);
	c = getc(ifs);
	if (c == EOF)
		eof_reached = 1;
	if (ferror(ifs))
		errval = errno;
	return (c);
#endif
}

char *
readonly_persistent_table::next_row(int &errval)
{
	char *rowbuf = NULL;
	uint_t rowbufsize, row_index = 0;
	uint_t incsize = 128;

	// States for skip_whitespace
	enum skip_state_t {COPY, SKIP, SKIPPED};
	skip_state_t skip_whitespace;

	errval = 0;
	// if we are at eof, nothing to do
	if (eof_reached)
		return (NULL);

	rowbufsize = incsize;	// start with reasonable size buffer
	rowbuf = new char[rowbufsize + 1];
	ASSERT(rowbuf);
	bzero(rowbuf, (size_t)rowbufsize + 1);

READ_A_ROW:
	skip_whitespace = COPY;
	while (!eof_reached) {
		char rowc = (char)next_char(errval);
		if (errval) {
			delete [] rowbuf;
			return (NULL);
		} else if (eof_reached)
			break;

		if (IS_SEP(rowc)) {
			if (skip_whitespace == COPY) {
				// Replace character with default separator
				rowc = SEP;
				// Skip any more sequential whitespace
				skip_whitespace = SKIP;
			} else if (skip_whitespace == SKIP) {
				// Skipping whitespace, so discard this
				continue;
			} // else output whitespace
		} else if (skip_whitespace == SKIP) {
			// If we were skipping whitespace, stop skipping.
			skip_whitespace = SKIPPED;
		}

		// reallocate rowbuf if necessary
		if (row_index >= rowbufsize) {
			// realloc rowbuf
			// XXX start rowbufsize at half initial value
			uint_t newsize = rowbufsize + incsize;
			char *tmpbuf = new char[newsize + 1];
			ASSERT(tmpbuf);
			bzero(tmpbuf, (size_t)newsize + 1);
			(void) os::strcpy(tmpbuf, rowbuf);
			delete [] rowbuf;
			rowbufsize = newsize;
			rowbuf = tmpbuf;
		}

		// strip off newline
		if (rowc == '\n')
			break;

		// copy char to rowbuf
		rowbuf[row_index++] = rowc;
	}

	// strip off trailing separator(s)
	// Note: for backwards compatibility with dcs_service files that
	// have trailing spaces, only tabs can be currently removed.
	// while (row_index > 0 && IS_SEP(rowbuf[row_index - 1])) {
	while (row_index > 0 && rowbuf[row_index - 1] == '\t') {
		row_index--;
	}

	if (row_index == 0) {
		if (eof_reached) {
			// End of file
			delete [] rowbuf;
			return (NULL);
		} else {
			// This is a blank row, skip to read next row
			goto READ_A_ROW;
		}
	} else {
		// null-terminate row
		rowbuf[row_index] = '\0';
		return (rowbuf);
	}
}

table_element_t *
readonly_persistent_table::next_element(int &errval)
{
	char *rowbuf;
	table_element_t *tel;
	uint_t rowlen, keylen, datalen;
	uint_t sep_index = 0;

	rowbuf = next_row(errval);
	if (errval) {
		// rowbuf is NULL if errval != 0
		return (NULL);
	}
	if (rowbuf == NULL)
		return (NULL);
	if (IS_SEP(*rowbuf)) {	// invalid format
		delete [] rowbuf;
		return (NULL);
	}

	rowlen = (uint_t)os::strlen(rowbuf);

	// find separator position
	for (uint_t i = 0; i < rowlen; i++) {
		if (IS_SEP(rowbuf[i])) {
			sep_index = i;
			break;
		}
	}

	if (sep_index == 0) {	// no separator, only key field
		keylen = rowlen;
		datalen = 0;
	} else {
		keylen = sep_index;
		datalen = rowlen - keylen - 1;
	}

	// create table element to return
	tel = new table_element_t;
	ASSERT(tel);
	tel->key = new char[keylen + 1];
	tel->data = new char[datalen + 1];
	ASSERT(tel->key && tel->data);

	// fill key and data from rowbuf
	(void) os::strncpy(tel->key, rowbuf, (size_t)keylen);
	tel->key[keylen] = '\0';
	if (datalen) {
		(void) os::strncpy(tel->data, &rowbuf[sep_index + 1],
			(size_t)datalen);
	}
	tel->data[datalen] = '\0';

	delete [] rowbuf;
	return (tel);
}

void
readonly_persistent_table::atfirst()
{
#ifndef _KERNEL
	// file pointer should be set
	CL_PANIC(ifs);
	rewind(ifs);
#else
	offset = 0;
#endif
	eof_reached = 0;
}

void
readonly_persistent_table::close()
{
	// file pointer may not necessarily be set,
	// particularly if the initialize() method
	// encountered a failure, and close() was
	// called thereafter.
#ifndef _KERNEL
	if (ifs) {
		(void) fclose(ifs);
		ifs = NULL;
	}
#else
	if (bpfile) {
		kobj_close_file(bpfile);
		bpfile = NULL;
	}
#endif
}

//
// checksum utilities
//
void
init_ccr_checksum(char *cksm_text)
{
	uchar_t cksm_digest[DIGEST_LEN];
	MD5_CTX context;
	MD5Init(&context);
	MD5Final(cksm_digest, &context);
	encode_ccr_checksum(cksm_digest, cksm_text);
}

void
encode_ccr_checksum(const uchar_t *cksm, char *text)
{
	int i;
	char *hexchars = "0123456789ABCDEF";

	for (i = 0; i < DIGEST_LEN; i++) {
		*text++ = hexchars[cksm[i] >> 4];	// upper half of byte
		*text++ = hexchars[cksm[i] & 0xF];	// lower half
	}
	*text = '\0';
}

static uint_t
hex2int(char *hextext)
{
	// hextext is 8 chars wide
	uint_t retval = 0, intval;

	for (uint_t i = 0; i < 8; i++) {
		char c = hextext[7 - i];
		if (c >= '0' && c <= '9') // isdigit
			//lint -e732
			intval = c - '0';
		else if (c >= 'A' && c <= 'F') { 	// isxdigit
			//lint -e732
			intval = 10 + c - 'A';
		} else {
			return (0);	// Need to return something
		}

		retval += (intval << (4 * i));
	}

	return (retval);
}

// XXX this may not be needed, TM could store octets rather than ints
void
decode_ccr_checksum(char *text, uint_t *cksm)
{
	// text is CKSM_HEX_LEN chars wide
	// cksm is array of 4 ints
	for (uint_t i = 0; i < 4; i++) {
		cksm[i] = hex2int(&text[8 * i]);
	}
}

updatable_persistent_table::updatable_persistent_table():
#ifndef _KERNEL
	ofs(NULL),
#else
	vp(NULL), offset(0),
#endif
	table_pathname(NULL)
{
}

updatable_persistent_table::~updatable_persistent_table()
{
	delete [] table_pathname;
#ifndef _KERNEL
	ofs = NULL;
#else
	vp = NULL;
#endif
}

int
updatable_persistent_table::initialize(const char *tab_pathname)
{
	table_pathname = new char[os::strlen(tab_pathname) + 1];
	ASSERT(table_pathname);
	(void) os::strcpy(table_pathname, tab_pathname);
#ifndef _KERNEL
	ofs = fopen(table_pathname, "w");
	if (ofs == NULL) {
		return (errno);
	}
#else
	int error;
	// remove file before opening in create mode
	error = vn_remove(table_pathname, UIO_SYSSPACE, RMFILE);
	if (error != 0 && error != ENOENT) {
		return (error);
	}
	// file permissions should be 600.
	int fmode = S_IFREG | S_IRUSR | S_IWUSR;
	if ((error = vn_open(table_pathname, UIO_SYSSPACE, FCREAT | FWRITE,
	    fmode, &vp, (enum create)0, 0)) != 0) {
		return (error);
	}
	offset = 0;
#endif

	return (0);
}

int
updatable_persistent_table::insert_element(const char *key, const char *data)
{
	int retval;
	char *sbuf;

	// CCR_DBG("CCR DS: insert_element %s %s\n", key, data);

	// key must not be null
	CL_PANIC(key);

	// write key, data to string buf
	if (data == NULL || *data == '\0') {
		sbuf = new char[os::strlen(key) + 2];
		ASSERT(sbuf);
		os::sprintf(sbuf, "%s\n", key);
	} else {
		sbuf = new char[os::strlen(key) + os::strlen(data) + 3];
		ASSERT(sbuf);
		os::sprintf(sbuf, "%s%c%s\n", key, SEP, data);
	}

#ifndef _KERNEL
	// file pointer was already set by caller
	CL_PANIC(ofs);

	// write and sync data to disk
	if (fprintf(ofs, "%s", sbuf) == -1)
		retval = errno;
	else
		retval = 0;
	(void) fflush(ofs);

#else
	ssize_t resid = 0;
	CL_PANIC(vp);

	// append buf to file
	retval = vn_rdwr(UIO_WRITE, vp, (caddr_t)sbuf,
	    (ssize_t)os::strlen(sbuf),
	    offset, UIO_SYSSPACE, FAPPEND,
	    (rlim64_t)MAXOFF32_T, kcred, &resid);
	offset += os::strlen(sbuf);
#endif
	delete [] sbuf;
	return (retval);
}

int
updatable_persistent_table::insert_element(const table_element_t &elem)
{
	const char *key = elem.key;
	const char *data = elem.data;
	return (insert_element(key, data));
}

int
updatable_persistent_table::insert_element(table_element_t *elemp)
{
	const char *key = elemp->key;
	const char *data = elemp->data;
	return (insert_element(key, data));
}

int
updatable_persistent_table::close()
{
	int retval = 0;

#ifndef _KERNEL
	// file pointer should be set
	CL_PANIC(ofs);
	if ((ofs) && (fclose(ofs) != 0))
		retval = errno;
#else
	CL_PANIC(vp);
#if	SOL_VERSION >= __s11
	if ((retval = VOP_FSYNC(vp, FSYNC, kcred, NULL)) != 0) {
#else
	if ((retval = VOP_FSYNC(vp, FSYNC, kcred)) != 0) {
#endif
		return (retval);
	}
	vn_rele(vp);
	vp = NULL;
#endif

	return (retval);
}

//
// initialize_ccr_table:
// utility used by unode and ccradm
// inserts metadata entries into a data table
//
int
ccrlib::initialize_ccr_table(const char *origpath, const char *gennumst)
{
	updatable_persistent_table new_table;
	readonly_persistent_table orig_table;
	table_element_t *telp;
	char *newpath, metastr[CKSM_HEX_LEN + 1];
	uchar_t cksm_digest[DIGEST_LEN];
	int retval;

	newpath = new char[os::strlen(origpath) + 5]; // room for suffix
	os::sprintf(newpath, "%s.new", origpath);
	// open input file and create empty new file
	if ((retval = orig_table.initialize(origpath)) != 0) {
		delete [] newpath;
		return (retval);
	}
	if ((retval = new_table.initialize(newpath)) != 0) {
		orig_table.close();
		delete [] newpath;
		return (retval);
	}

	//
	// write meta entries
	//

	// insert gennum = 0
	if ((retval = new_table.insert_element("ccr_gennum", gennumst)) != 0) {
		orig_table.close();
		(void) new_table.close();
		delete [] newpath;
		return (retval);
	}

	// insert ccr_checksum
	if ((retval = orig_table.compute_checksum(cksm_digest)) != 0) {
		orig_table.close();
		(void) new_table.close();
		delete [] newpath;
		return (retval);
	}
	encode_ccr_checksum(cksm_digest, metastr);
	if ((retval = new_table.insert_element("ccr_checksum", metastr)) != 0) {
		orig_table.close();
		(void) new_table.close();
		delete [] newpath;
		return (retval);
	}

	//
	// copy data entries
	//
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// ignore any stray metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = new_table.insert_element(telp)) != 0) {
				orig_table.close();
				(void) new_table.close();
				delete [] newpath;
				delete telp;
				return (retval);
			}
		}
		delete telp;
	}
	orig_table.close();
	if ((retval = new_table.close()) != 0) {
		delete [] newpath;
		return (retval);
	}

	// rename new table to orig table
	retval = os::file_rename(newpath, (char *)origpath);
	delete [] newpath;
	return (retval);
}

#ifdef WEAK_MEMBERSHIP
#ifndef _KERNEL
//
// Interface to select winner CCR copy after there are CCR changes after
// a split-brain under weak membership model. To select the winner, all we have
// to do is delete the SPLIT_BRAIN_CHANGE_FILE. The changed CCR tables will in
// effect be the truth copies.
//
// Returns 0 on success, non-zero otherwise.
//
int
ccrlib::mark_ccr_as_winner()
{
	//
	// Nothing to do if there's no split-brain CCR change
	//
	if (!split_brain_ccr_change_exists()) {
		return (0);
	}
	return (os::file_unlink(SPLIT_BRAIN_CHANGE_FILE));
}

//
// Interface to select loser CCR copy after CCR changes were made during
// a split-brain under weak membership model. To mark this node as the loser,
// all CCR tables on the node are "invalidated", that is, the generation
// number is set to -1. This ensures that the loser node cannot form a cluster
// on its own. When the loser node is booted, the CCR data server on the node
// will synchronize itself with the CCR copy on the winner node. Should the
// winner node be down, the loser node will just wait in the data server
// registration step until it can get the CCR truth copy from another node.
//
// Start from /etc/cluster/ccr, and invalidate every CCR table.
//
// SPLIT_BRAIN_CHANGE_FILE is deleted after all CCR tables have been
// invalidated.
//
// Returns 0 on success, non-zero otherwise.
//
int
ccrlib::mark_ccr_as_loser()
{
	readonly_persistent_table cluster_dir_tab; // for cluster_directory
	readonly_persistent_table	dir_tab; // for "directory"
	table_element_t		*clustp; // For elements of cluster_directory
	table_element_t		*telp; // For elements of "directory"
	int			retval = 0, err = 0;
	char			path[1024];

	//
	// Nothing to do if there's no split-brain CCR change
	//
	if (!split_brain_ccr_change_exists()) {
		return (0);
	}

	//
	// Here's the algorithm:
	//
	// 1. Read /etc/cluster/ccr/cluster_directory
	// 2. For each element "dir" in cluster_directory
	//    chdir(dir)
	//    Open "directory" table inside "dir"
	//    for each entry in "directory"
	//	invalidate entry
	//    Invalidate "directory"
	// 3. Invalidate cluster_directory
	//
	retval = cluster_dir_tab.initialize(CLUSTER_DIRECTORY_TABLE);
	if (retval != 0) {
		return (retval);
	}

	cluster_dir_tab.atfirst();
	while ((clustp = cluster_dir_tab.next_element(retval)) != NULL) {
		if (is_meta_row(clustp->key)) {
			continue;
		}

		//
		// The key is the name of a subdirectory (or cluster name) of
		// /etc/cluster/ccr. chdir(2) into that directory.
		//
		(void) os::snprintf(path, sizeof (path), "%s%s",
			"/etc/cluster/ccr/", clustp->key);
		if (chdir(path) != 0) {
			cluster_dir_tab.close();
			delete clustp;
			return (errno);
		}

		//
		// Open directory table and invalidate each entry
		//
		err = dir_tab.initialize("directory");
		if (err != 0) {
			cluster_dir_tab.close();
			delete clustp;
			return (err);
		}
		dir_tab.atfirst();
		while ((telp = dir_tab.next_element(err)) != NULL) {
			if (is_meta_row(telp->key)) {
				continue;
			}
			err = initialize_ccr_table(telp->key, INIT_VERSION);
			if (err != 0) {
				cluster_dir_tab.close();
				dir_tab.close();
				delete clustp;
				delete telp;
				return (err);
			}
			delete telp;
		}
		dir_tab.close();

		// Invalidate directory table itself
		if ((err = initialize_ccr_table("directory",
		    INIT_VERSION)) != 0) {
			cluster_dir_tab.close();
			delete clustp;
			return (err);
		}
		delete clustp;
	}
	cluster_dir_tab.close();

	// Invalidate cluster_directory itself
	if ((err = initialize_ccr_table(CLUSTER_DIRECTORY_TABLE,
	    INIT_VERSION)) != 0) {
		return (err);
	}
	return (os::file_unlink(SPLIT_BRAIN_CHANGE_FILE));
}

#endif // _KERNEL

bool
ccrlib::split_brain_ccr_change_exists()
{
	return (os::file_exists(SPLIT_BRAIN_CHANGE_FILE));
}

#endif // WEAK_MEMBERSHIP
