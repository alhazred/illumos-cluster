/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  data_server_impl.h - CCR data server
 *
 */

#ifndef _DATA_SERVER_IMPL_H
#define	_DATA_SERVER_IMPL_H

#pragma ident	"@(#)data_server_impl.h	1.43	09/02/11 SMI"

#include <sys/param.h>
#include <sys/os.h>
#include <sys/hashtable.h>
#include <sys/refcnt.h>
#include <h/quorum.h>
#include <h/ccr_data.h>
#include <h/ccr_trans.h>
#include <ccr/common.h>
#include <ccr/ds_cluster_info.h>
#include <ccr/table_access.h>
#include <orb/object/adapter.h>

#define	TM_NS_SLEEPTIME	100000	// .1 sec
#define	TM_NS_WHINETIME	5000000	// 5 sec

#define	EPOCHFILE	"epoch"
#define	OLDEPOCHFILE	"epoch.bak"
#define	TMPSUFFIX	".tmp"
#define	EPOCHSTR	"ccr_epoch"
#define	BAKSUFFIX	".bak"

typedef IntrList<ds_cluster_info, _SList> cluster_info_list_t;

//
// CCR Data Server
// Cluster has one data server per node
// Services I/O requests: reads from users and writes from
// the transaction manager
//
class data_server_impl : public McServerof<ccr_data::data_server> {
	//
	// Public interface
	//
public:
	friend class updatable_copy_impl;

	data_server_impl();
	~data_server_impl();
	int initialize(char *, quorum::ccr_id_t);
	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//

	// read
	ccr::readonly_table_ptr lookup_zc(const char *, const char *,
	    Environment &);
	ccr::readonly_table_ptr lookup(const char *, Environment &);
	void get_table_names_zc(const char *, CORBA::StringSeq_out,
	    Environment &);
	void get_table_names(CORBA::StringSeq_out, Environment &);
	void get_invalid_table_names_zc(const char *, CORBA::StringSeq_out,
	    Environment &);
	void get_invalid_table_names(CORBA::StringSeq_out, Environment &);

	//
	// Get zone cluster id and zone cluster name information for a
	// zone cluster.
	//
	int32_t zc_getidbyname(const char *, uint32_t &, Environment &);
	char *zc_getnamebyid(uint_t, Environment &);
	uint_t zc_get_maxid(Environment &);
	static int32_t	get_zcid(const char *, uint32_t *);

	// update
	void create_table_zc(uint_t, const char *, const char *,
	    Environment &);
	void create_table(const char *, Environment &);

	void remove_table_zc(const char *, const char *, Environment &);
	void remove_table(const char *, Environment &);

	ccr_data::updatable_table_copy_ptr begin_transaction_zc(const char *,
					const char *, bool, Environment &);
	ccr_data::updatable_table_copy_ptr begin_transaction(const char *,
	    bool, Environment &);

	// recovery
	void register_with_tm(Environment &);
	ccr_data::epoch_type get_epoch(Environment &);
	ccr_data::epoch_type read_epoch(Environment &);
	void set_epoch(ccr_data::epoch_type, Environment &);

	void get_consis_zc(const char *, const char *, ccr_data::consis &,
	    Environment &);
	void get_consis(const char *, ccr_data::consis &, Environment &);
	ccr_data::consis_seq *get_all_consis(Environment &);

	ccr::element_seq *get_table_data_zc(const char *, const char *,
	    Environment &);
	ccr::element_seq *get_table_data(const char *, Environment &);

	void replace_table_zc(const char *, const char *,
		const ccr::element_seq &,
		const ccr_data::consis &, Environment &);
	void replace_table(const char *, const ccr::element_seq &,
		const ccr_data::consis &, Environment &);

	void set_table_invalid_zc(const char *, const char *, Environment &);
	void set_table_invalid(const char *, Environment &);
	void recovery_done(Environment &);

	// Set the recovery state. Called by TM
	void set_recovery_state(recovery_state_t, Environment &);

	//
	// Add the table that needs to be updated outside of the recovery.
	// Called by TM during propagate_table.
	//
	void add_update_table_zc(const char *, const char *, Environment &);
	void add_update_table(const char *, Environment &);

	//
	// Called by TM to kill the offending DS that has system_error.
	//
	void die(Environment &);
private:
	//
	// private methods
	//
	void	cleanup_cluster_info(const ccr::element_seq &);
	void 	refresh_cluster_info();
	ds_cluster_info *lookup_cluster_info(const char *);

	void update_invalid_table(ccr_trans::manager_ptr, ds_cluster_info *,
	    char *);
	ccr::readonly_table_ptr lookup_table(const char *,
	    ds_cluster_info *, Environment &) const;
	void create_cluster_list(SList<char>*);

	ccr::element_seq *get_table_data_in(const char *, const char *,
	    Environment &);
	ccr_data::consis get_consis_in(const char *, const char *,
	    Environment &);
	void set_table_pathname(char **, const char *, const char *);
	ccr_data::epoch_type get_epoch_in(const char *,
	    Environment &);
	void check_zone_cluster(const char *, ds_cluster_info *,
	    const ccr::element_seq &,  Environment &);

public:
	static data_server_impl	*the_data_server_implp;

private:
	ccr_trans::handle_ptr	handlep;
	ccr_data::epoch_type	epoch_num;
	os::mutex_t		cl_mutex;
	cluster_info_list_t	cluster_list;	// List of clusters.

	// Hashtable to lookup zone cluster id from zone cluster name
	string_hashtable_t<uint_t>  clname_hashtab;

	// Hashtable to lookup zone cluster name from zone cluster_id
	hashtable_t<char *, uint_t>  clid_hashtab;

	// Lock to synchronize access to cluster id hash tables.
	os::rwlock_t		clid_lock;

	char			*dir_path;	// where files are located
	os::mutex_t		recovery_mutex;
	os::condvar_t		recovery_cv;
	const uint_t		max_elems_expected;
	quorum::ccr_id_t	local_ccr_id;
	//
	// The state of recovery. Since reading and writing of this variable
	// won't happen at the same time, we don't need use lock to protect
	// reading.
	//
	recovery_state_t	recovery_state;
};

#endif	/* _DATA_SERVER_IMPL_H */
