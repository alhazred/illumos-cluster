//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ccr_ds_main.cc - CCR data server main routine
//

#pragma ident	"@(#)ccr_ds_init.cc	1.26	08/07/17 SMI"

#include <sys/os.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <orb/invo/common.h>
#include <orb/invo/corba.h>
#include <orb/infrastructure/orb_conf.h>
#include <ccr/ccr_ds.h>
#include <ccr/data_server_impl.h>
#include <ccr/directory_impl.h>
#include <ccr/common.h>

// unode header
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <unode/unode_config.h>
#endif

//
// ccr_data_server_initialize:
// starts the CCR in readonly mode
//
int
ccr_data_server_initialize(char *dir_path)
{
	Environment e;
	int retval;
	data_server_impl *data_svr;
	directory_impl *dir_client;
	naming::naming_context_var ctxp;
	quorum::ccr_id_t ccr_id;
	ccr_data::data_server_var dsp;
	ccr::directory_var dirp;

	// The ccr_id of the CCR on a local disk is the same as the node id.
	ccr_id = (quorum::ccr_id_t) orb_conf::node_number();

	// create data server
	data_svr = new data_server_impl();
	data_server_impl::the_data_server_implp = data_svr;
	retval = data_svr->initialize(dir_path, ccr_id);
	if (retval) {
		//
		// SCMSGS
		// @explanation
		// The CCR data server could not initialize on this node. This
		// usually happens when the CCR is unable to read its metadata
		// entries on this node. There is a CCR data server per
		// cluster node.
		// @user_action
		// There may be other related messages on this node, which may
		// help diagnose this problem. If the root disk failed, it
		// needs to be replaced. If there was cluster repository
		// corruption, then the cluster repository needs to be
		// restored from backup or other nodes in the cluster. Boot
		// the offending node in -x mode to restore the repository.
		// The cluster repository is located at /etc/cluster/ccr/.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
			"CCR: Could not initialize CCR data server.");
		return (retval);
	}
	dsp = data_svr->get_objref();

	// create directory client read/write
	dir_client = new directory_impl(dsp);
	dirp = dir_client->get_objref();

	// bind client in local nameserver in all context
	ctxp = ns::local_nameserver_c1();
	if (CORBA::is_nil(ctxp)) {
		ASSERT(0);
	}
	ctxp->rebind("ccr_directory", dirp, e);
	CL_PANIC(!e.exception());

	// bind data server in local nameserver
	ctxp->rebind("ccr_ds", dsp, e);
	CL_PANIC(!e.exception());
	CCR_DBG(("Registered ccr_ds in local nameserver\n"));

	return (0);
}

//
// ccr_data_server_register:
// registers the local CCR DS with the TM
// to initiate recovery and read-write mode
//
int
ccr_data_server_register()
{
	naming::naming_context_var ctxp;
	CORBA::Object_var obj;
	ccr_data::data_server_ptr dsp;
	Environment e;

	// look up data server in local name server
	obj = ns::wait_resolve_local("ccr_ds", e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR data server could not be found in the local name
		// server.
		// @user_action
		// Reboot the node. Also contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: CCR data server not found.");
		CCR_DBG(("ccr_ds not found in local name server\n"));
		e.clear();
		return (ENOENT);
	}
	dsp = ccr_data::data_server::_narrow(obj);

	// register DS with TM
	dsp->register_with_tm(e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR data server on this node failed to join the
		// cluster, and can only serve readonly requests.
		// @user_action
		// There may be other related CCR messages on this and other
		// nodes in the cluster, which may help diagnose the problem.
		// It may be necessary to reboot this node or the entire
		// cluster.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: CCR data server failed to register with CCR "
		    "transaction manager.");
		e.clear();
		return (EINVAL);
	}

	// unbind ccr_ds
	ctxp = ns::local_nameserver_c1();
	if (CORBA::is_nil(ctxp)) {
		ASSERT(0);
	}
	ctxp->unbind("ccr_ds", e);
	e.clear();

	return (0);
}

#ifdef _KERNEL_ORB
int
initialize_ccr_ds()
{
	int retval;
	char *ccr_path = new char[MAXPATHLEN + 1];

#ifdef _KERNEL
	(void) os::strcpy(ccr_path, CCR_DATA_PATH);
#else  // unode
	(void) os::strcpy(ccr_path, unode_config::the().ccr_dir());
#endif
	retval = ccr_data_server_initialize(ccr_path);
	delete [] ccr_path;
	return (retval);
}

int
register_ccr_ds()
{
	return (ccr_data_server_register());
}
#endif
