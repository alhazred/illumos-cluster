//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ds_cluster_info.cc - cluster information in the Data server.
//

#pragma ident	"@(#)ds_cluster_info.cc	1.11	08/08/08 SMI"

#include <ccr/ds_cluster_info.h>
#include <orb/fault/fault_injection.h>
#include <ccr/fault_ccr.h>

//
// Methods for ds_cluster_info class.
//

//
// Constructor
//
ds_cluster_info::ds_cluster_info(
	uint_t id,
	const char *cluster_name,
	const char *dirpath) : _SList::ListElem(this)
{
	ASSERT(cluster_name);
	ASSERT(dirpath);
	CCR_DBG(("CCR DS: create cluster %s cluster_id %d\n",
	    cluster_name, id));
	cluster_id = id;
	clusterp = os::strdup(cluster_name);
	dir_path = os::strdup(dirpath);
	directory_elems.seq = NULL;
	update_table_list = NULL;
}

//
// Destructor
//
// Lint gives 'function may throw exception warning'.
//lint -e1551
//
ds_cluster_info::~ds_cluster_info()
{
	table_access	*ta;
	table_access	*ta_dir;

	CCR_DBG(("CCR DS: Destroy cluster %s\n", (char *)clusterp));
	if (update_table_list != NULL) {
		delete update_table_list;
	}
	if (directory_elems.seq != NULL) {
		uint_t num_tables = directory_elems.seq->length();
		for (uint_t indx = 0; indx < num_tables; indx++) {
			char *tname = (*directory_elems.seq)[indx].key;
			ta = tacc_hashtab.remove(tname);
			ASSERT(ta);
			ta->rele();
		}
		delete directory_elems.seq;
	}
	ta_dir = tacc_hashtab.remove(DIR);
	ASSERT(ta_dir);
	ta_dir->rele();
	delete [] clusterp;
	delete [] dir_path;
}
//lint +e1551

//
// Method to retrieve table access object for a table in the
// cluster. The object is retrieved from a hashtable of table
// access objects.
//
table_access *
ds_cluster_info::lookup_table(const char *tname)
{
	table_access *ta = NULL;
	tacc_mutex.lock();
	ta = tacc_hashtab.lookup(tname);
	if (ta != NULL) {
		ta->hold();
	}
	tacc_mutex.unlock();
	return (ta);
}


//
// Updates the hash table to reflect the new directory
// contents. Removes obsolete table access from the
// hash table.
void
ds_cluster_info::cleanup_directory_info(const ccr::element_seq &elems)
{
	ccr::element_seq	*local_elem_seq;
	uint_t 			indx;
	uint_t			cindx;
	uint_t			nelems;
	uint_t			num_elems;
	const char 		*tbname;
	bool 			found;
	Environment 		env;

	//
	// No need to protect the updates to the directory_elem
	// since this method is only called by the Transaction
	// manager in the recovery process when no other transactions
	// can take place to change the list of tables. The list of
	// tables get updated in refresh_directory_info.
	//
	//
	// Bug 4350167 - Make a copy of directory_elems and use the
	// copy for comparison with elems to determine those that are
	// no longer in the new sequence and need to be removed from
	// directory_elems. That way any change in the length and
	// content of directory_elems (resulting from remove_table())
	// does not affect the sequence used in the comparison below.
	//
	// No need to protect the reading of the directory_elems since
	// cleanup_directory_info is only called by
	// data_server_impl::replace_table when the recovery is in
	// progress and no other transactions can take place to change
	// directory_elems.
	//
	if (directory_elems.seq == NULL) {
		//
		// There is nothing to cleanup as there are no tables
		// in this cluster.
		//
		return;
	}
	num_elems = directory_elems.seq->length();
	local_elem_seq = new ccr::element_seq(num_elems, num_elems);
	*local_elem_seq = *(directory_elems.seq);

	nelems = elems.length();
	for (indx = 0; indx < num_elems; indx++) {
		tbname = (*local_elem_seq)[indx].key;
		found = false;
		for (cindx = 0; cindx < nelems; cindx++) {
			if (strcmp(elems[cindx].key, tbname) == 0) {
				found = true;
				break;
			}
		}
		if (!found) {
			CCR_DBG(("CCR DS: removing table %s from hashtab\n",
			    tbname));
			remove_incore_table(tbname);
		}
	}
	delete local_elem_seq;
}

//
// Retrieve the list of all the tables in the cluster.
//
void
ds_cluster_info::get_table_names(CORBA::StringSeq_out tnames)
{
	//
	// Reading of the directory_elems needs to be protected by a lock
	// because other processes could be changing the size of the
	// directory_elems by removing or adding CCR tables while the reading
	// is still in process. See Bug 4366881
	//
	tacc_mutex.lock();
	//
	// Return string sequence of table names. The cluster can also
	// be empty with zero tables.
	//
	if (directory_elems.seq != NULL) {
		uint_t num_tables = directory_elems.seq->length();
		char **tmp = new char *[num_tables];
		for (uint_t indx = 0; indx < num_tables; indx++) {
			char *tname = (*directory_elems.seq)[indx].key;
			tmp[indx] = new char[os::strlen((char *)tname) + 1];
			ASSERT(tmp[indx]);
			(void) os::strcpy(tmp[indx], tname);
		}
		tnames = new CORBA::StringSeq(num_tables, num_tables, tmp,
		    true);
	} else {
		tnames = new CORBA::StringSeq(0, 0, NULL, true);
	}
	tacc_mutex.unlock();
}

//
// Retrieve the list of all the tables marked invalid in the cluster.
//
void
ds_cluster_info::get_invalid_table_names(CORBA::StringSeq_out tnames)
{
	table_access	*ta = NULL;
	uint_t		num_tables = 0;
	uint_t		num_invalid_tables = 0;
	char		**tablenamep;
	uint_t		indx;
	char		*tname;

	//
	// Reading of the directory_elems needs to be protected by a lock
	// because other processes could be changing the size of the
	// directory_elems by removing or adding CCR tables while the reading
	// is still in process. See Bug 4366881
	//
	tacc_mutex.lock();
	//
	// Return string sequence of table names. The cluster can also
	// be empty with zero tables.
	//
	if (directory_elems.seq != NULL) {
		num_tables = directory_elems.seq->length();
		num_invalid_tables = 0;
		tablenamep = new char *[num_tables];
		for (indx = 0; indx < num_tables; indx++) {
			tname = (*directory_elems.seq)[indx].key;
			ta = tacc_hashtab.lookup(tname);
			ASSERT(ta);
			ta->hold();
			if (!ta->rec.valid) {
				// This is an invalid table.
				tablenamep[num_invalid_tables] = new
				    char[os::strlen(tname) + 1];
				ASSERT(tablenamep[num_invalid_tables]);
				(void) os::strcpy(
				    tablenamep[num_invalid_tables], tname);
				num_invalid_tables++;
			}
			ta->rele();
		}
		tnames = new CORBA::StringSeq(num_invalid_tables,
		    num_invalid_tables, tablenamep, true);
	} else {
		tnames = new CORBA::StringSeq(0, 0, NULL, true);
	}
	tacc_mutex.unlock();
}

//
// Add a table to the update table list for this cluster.
// This is called during data server registration and there is
// no need to lock protect this.
//
void
ds_cluster_info::add_update_table(const char *tname)
{
	//
	// Initialize the update_table_list that is to record
	// tables that are valid AND have the override flag set
	// on this joining node but are recovered as invalid in
	// previous do_recovery..
	//
	if (update_table_list == NULL) {
		update_table_list = new SList<char>;
	}
	update_table_list->append(os::strdup(tname));
}


//
// Remove the table from the incore structures.
//
void
ds_cluster_info::remove_incore_table(const char *tname)
{
	table_access *ta;

	tacc_mutex.lock();

	// remove tacc fromm hastab
	ta = tacc_hashtab.remove(tname);
	ASSERT(ta);
	ta->rele();

	//
	// Remove entry from directory_elems only if it is not the directory
	// table itself.
	//
	if (!is_dir(tname)) {
		directory_elems.remove_element(tname);
	}

	tacc_mutex.unlock();
}

//
// Add the table to the incore structures.
//
void
ds_cluster_info::add_incore_table(const char *tname)
{
	char		cksm_text[CKSM_HEX_LEN + 1];
	table_access	*ta;

	tacc_mutex.lock();

	// stick new tacc into hastab
	ta = new table_access;
	ASSERT(ta);
	ta->rec.valid = true;
	ta->rec.version = 0;
	init_ccr_checksum(cksm_text);
	CL_PANIC(os::strlen(cksm_text) == CKSM_HEX_LEN);
	(void) os::strcpy(ta->rec.checksum, cksm_text);

	tacc_hashtab.add(ta, tname);

	//
	// Add entry to directory_elems only if it is not the directory
	// table itself.
	//
	if (!is_dir(tname)) {
		directory_elems.add_element((char *)tname);
	}
	tacc_mutex.unlock();
}


//
// refresh_directory_info:
// This traverses the list of all the tables in the cluster and
// adds the table access object to the hashtable of the cluster if the
// object does not exist in the hashtable for any table in the
// list for any cluster.
//
void
ds_cluster_info::refresh_directory_info()
{
	uint_t		indx;
	table_access	*ta;
	char		*tname;
	Environment	env;
	uint_t		num_tables = 0;

	tacc_mutex.lock();

	if (directory_elems.seq != NULL) {
		num_tables = directory_elems.seq->length();
	}

	for (indx = 0; indx < num_tables; ++indx) {

		tname = (*directory_elems.seq)[indx].key;
		ASSERT(tname);
		//
		// check if hash entry already exists
		// (hashtable add does not check uniqueness)
		//
		if (tacc_hashtab.lookup(tname) == NULL) {
			ta = new table_access;
			ASSERT(ta);
			//
			// stick table consis info in ta entry
			//
			ta->rec = get_consis_in(tname, env);
			if (env.exception()) {
				CCR_DBG(("CCR DS: could not read consis "
				    "info for %s table in cluster %s\n",
				    tname, clusterp));
				env.clear();
			}
			tacc_hashtab.add(ta, tname);
		}
	}
	//
	// Also add the hash entry for the DIR table if it does
	// not exist.
	if ((ta = tacc_hashtab.lookup(DIR)) == NULL) { //lint !e423
		ta = new table_access;
		ASSERT(ta);
		//
		// stick table consis info in ta entry
		//
		ta->rec = get_consis_in(DIR, env);
		if (env.exception()) {
			CCR_DBG(("CCR DS: could not read consis info for %s "
			    "table in cluster %s\n", DIR, (char *)clusterp));
			env.clear();
		}
		tacc_hashtab.add(ta, DIR);
	}
	tacc_mutex.unlock();
}

//
// Retrieve the consistency of a table.
//
ccr_data::consis
ds_cluster_info::get_consis_in(const char *tname, Environment &e)
{
	ccr_data::consis		rec;
	char				*tab_pathname;
	int				retval;
	readonly_persistent_table	ptab;
	table_element_t			*next_el;
	bool				version_found = false;
	bool				checksum_found = false;
	char				reported_cksm[CKSM_HEX_LEN + 1];
	char				actual_cksm[CKSM_HEX_LEN + 1];
	uchar_t				cksm_digest[DIGEST_LEN];

	*reported_cksm = '\0';	// lint

	//
	// Initialize consis record
	//

	//
	// If rec is not valid other fields are ignored.
	//
	rec.valid = false;
	rec.override = false;
	rec.version = -1;

	//
	// Fault point to cause problems before reading any table data
	//
	FAULTPT_CCR(FAULTNUM_CCR_GET_CONSIS_IN_1,
	    fault_ccr::generic_test_op);

	//
	// Read from file, set some fields
	//
	set_table_pathname(&tab_pathname, tname);

	if ((retval = ptab.initialize(tab_pathname)) != NULL) {
		CCR_DBG(("CCR DS: Initialize failed for %s %s\n", clusterp,
		    tname));
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		delete [] tab_pathname;
		return (rec);
	}

	//
	// Fault point to cause problems after reading table
	// metadata but before getting elements
	//
	FAULTPT_CCR(FAULTNUM_CCR_GET_CONSIS_IN_2,
	    fault_ccr::generic_test_op);

	//
	// Read elements from table file
	//
	while ((next_el = ptab.next_element(retval)) != NULL) {
		// look for meta entries in header
		if (!is_meta_row(next_el->key)) {
			// reached data entries without finding
			// required metadata
			CCR_DBG(("CCR DS: Reached metadata ....\n"));
			e.exception(new ccr::table_invalid(tname));
			delete next_el;
			delete [] tab_pathname;
			ptab.close();
			return (rec);
		}
		if (os::strcmp(next_el->key, "ccr_gennum") == 0) {
			rec.version = os::atoi(next_el->data);
			version_found = true;
			if (rec.version == -2) {
				//
				// Set the version number to -1 for now.
				//
				// If the data server containing this copy
				// joins as joiner while this table is
				// currently marked as invalid (version == -1)
				// on all other recovered data servers, the
				// joining data server will propagate its copy
				// of table onto other data servers by
				// initiating a begin_transaction_invalid_ok().
				// At the end of transaction, each data server
				// will increase this table's version number
				// that is stored in its own cache by 1 before
				// writing to the persistent data. If we set
				// the version number of the joiner's copy to a
				// number other than -1, this table will end
				// up having inconsistent gennum across the
				// cluster nodes after the transaction finishes.
				// See bug 4487519 for more information. In
				// addition, before the propagation from joiner
				// to other data servers taking place, the table
				// is still considered as invalid in cluster,
				// so is the copy on the joiner.
				//
				// If the data server containing this copy
				// participates in the recovery procedure and
				// this copy is chosen as truth version, at the
				// time data_server_impl::replace_table, its
				// version number will be reset to 0.
				//
				rec.version = -1;
				rec.override = true;
			}
		} else if (os::strcmp(next_el->key, "ccr_checksum") == 0) {
			size_t cksm_len = os::strlen(next_el->data);
			if (cksm_len != CKSM_HEX_LEN) {
				//
				// SCMSGS
				// @explanation
				// The checksum of the indicated table has a
				// wrong size. This causes the consistency
				// check of the indicated table to fail.
				// @user_action
				// Boot the offending node in -x mode to
				// restore the indicated table from backup or
				// other nodes in the cluster. The CCR tables
				// are located at /etc/cluster/ccr/.
				//
				(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING,
				    MESSAGE,
				    "CCR: Invalid checksum length %d in "
				    "table %s, expected %d.", cksm_len,
				    tab_pathname, CKSM_HEX_LEN);
				e.exception(new ccr::table_invalid(tname));
				delete next_el;
				ptab.close();
				delete [] tab_pathname;
				return (rec);
			}
			(void) os::strcpy(reported_cksm, next_el->data);
			checksum_found = true;
		}

		delete next_el;
		if (version_found && checksum_found)
			break;
	}

	if (!(version_found && checksum_found)) {
		CCR_DBG(("CCR DS: version and checksum both not found\n"));
		e.exception(new ccr::table_invalid(tname));
		ptab.close();
		delete [] tab_pathname;
		return (rec);
	}

	// compute actual checksum for table
	if ((retval = ptab.compute_checksum(cksm_digest)) != 0) {
		ptab.close();
		delete [] tab_pathname;
		e.exception(new ccr::system_error(orb_conf::node_number(),
		    retval));
		return (rec);
	}
	ptab.close();

	// verify it matches reported checksum
	encode_ccr_checksum(cksm_digest, actual_cksm);
	if (os::strcmp(reported_cksm, actual_cksm) != 0) {
		//
		// SCMSGS
		// @explanation
		// The indicated table has an invalid checksum that does not
		// match the table contents. This causes the consistency check
		// on the indicated table to fail.
		// @user_action
		// Boot the offending node in -x mode to restore the indicated
		// table from backup or other nodes in the cluster. The CCR
		// tables are located at /etc/cluster/ccr/.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_WARNING, MESSAGE,
		    "CCR: Table %s has invalid checksum field. Reported: %s, "
		    "actual: %s.", tab_pathname, reported_cksm, actual_cksm);
		e.exception(new ccr::table_invalid(tname));
		delete [] tab_pathname;
		return (rec);
	}

	rec.valid = true;
	CL_PANIC(os::strlen(reported_cksm) == CKSM_HEX_LEN);
	(void) os::strcpy(rec.checksum, reported_cksm);
	delete [] tab_pathname;
	return (rec);
}

//
// Remove all the tables in /etc/cluster/ccr/<cluster_name>/
// utility routine used by the Data server to remove all the tables
// from the filesystem when the cluster has been removed.
//
int
ds_cluster_info::remove_tables()
{
	ccr::element_seq	*elementptr;
	char			*orig_path;
	char			*orig_bak_path;
	uint_t			num_tables;
	uint_t			indx;
	CORBA::Environment	env;
	int			retval = 0;

	CCR_DBG(("CCR DS: Remove all tables in cluster %s\n", clusterp));
	elementptr = directory_elems.seq;

	if (elementptr != NULL) {
		num_tables = elementptr->length();
		for (indx = 0; indx < num_tables; ++indx) {
			CCR_DBG(("CCR DS: Deleting table %s\n",
			    (char *)(*elementptr)[indx].key));
			set_table_pathname(&orig_path,
			    (char *)(*elementptr)[indx].key);
			if ((retval = os::file_unlink(orig_path)) != 0) {
				delete [] orig_path;
				return (retval);
			}
			orig_bak_path = new char[os::strlen(orig_path) + 5];
			CL_PANIC(orig_bak_path);
			os::sprintf(orig_bak_path, "%s.%s", orig_path, "bak");
			//
			// Ignore the return value as the .bak file may not
			// exist.
			//
			(void) os::file_unlink(orig_bak_path);
			delete [] orig_path;
			delete [] orig_bak_path;
		}
		set_table_pathname(&orig_path, DIR);
		if ((retval = os::file_unlink(orig_path)) != 0) {
			delete [] orig_path;
			return (retval);
		}
		orig_bak_path = new char[os::strlen(orig_path) + 5];
		CL_PANIC(orig_bak_path);
		os::sprintf(orig_bak_path, "%s.%s", orig_path, "bak");
		//
		// Ignore the return value as the .bak file may not
		// exist.
		//
		(void) os::file_unlink(orig_bak_path);
		delete [] orig_path;
		delete [] orig_bak_path;
	}
	return (retval);
}

//
// Builds the filesystem path of the table in the cluster.
//
void
ds_cluster_info::set_table_pathname(char **path, const char *tname)
{
	size_t len;
	//
	// If the cluster is global and the table is cluster_directory
	// the file exists at /<dir_path>/ as a special case
	// rather than /<dir_path>/<cluster>/ in all other cases.
	//
	if (is_default_cluster(clusterp) && is_cluster_dir(tname)) {
		len = os::strlen(dir_path) + os::strlen(tname) + 2;
		*path = new char[len];
		CL_PANIC(*path);
		os::sprintf(*path, "%s/%s", dir_path, tname);
		return;
	}
	len = os::strlen(dir_path) + os::strlen(clusterp) +
	    os::strlen(tname) + 3;
	*path = new char[len];
	CL_PANIC(*path);
	os::sprintf(*path, "%s/%s/%s", dir_path, clusterp, tname);
}
