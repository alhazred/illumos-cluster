/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  readonly_copy_impl.h - readonly table object
 *
 */

#ifndef _READONLY_COPY_IMPL_H
#define	_READONLY_COPY_IMPL_H

#pragma ident	"@(#)readonly_copy_impl.h	1.16	08/05/20 SMI"

#include <h/ccr.h>
#include <ccr/persistent_table.h>
#include <ccr/data_server_impl.h>
#include <orb/object/adapter.h>

// readonly_table_impl: object created dynamically by data server in
// response to lookup() request by user
// multiple ro_tables can exist simultaneously to permit multiple readers

class readonly_copy_impl : public McServerof<ccr::readonly_table> {
	//
	// Public interface
	//
public:
	readonly_copy_impl();
	int initialize(char *, table_access *);
	int verify_checksum();
	~readonly_copy_impl();
	void _unreferenced(unref_t);

	//
	// Methods that implement the IDL interface
	//

	char *query_element(const char *, Environment &);
	void atfirst(Environment &);
	void next_element(ccr::table_element_out, Environment &);
	void next_n_elements(uint32_t, ccr::element_seq_out, Environment &);

	int32_t get_num_elements(Environment &);
	int32_t get_format_version(Environment &);
	int32_t	get_gennum(Environment &);

	void register_callbacks(ccr::callback_ptr, Environment &);
	void unregister_callbacks(ccr::callback_ptr, Environment &);

private:
	char *pathname;
	readonly_persistent_table ptab;
	table_access *tacc;
	int32_t init_gennum;
};

#endif	/* _READONLY_COPY_IMPL_H */
