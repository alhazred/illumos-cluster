//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ccr_repl_prov.cc - HA service provider for CCR Transaction Manager

#pragma ident	"@(#)ccr_repl_prov.cc	1.50	08/08/07 SMI"

#include <sys/os.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <ccr/ccr_repl_prov.h>
#include <ccr/manager_impl.h>
#include <ccr/transaction_impl.h>
#include <sys/vm_util.h>

#ifdef CCR_DBGBUF
// Debugging buffer
uint32_t ccr_dbg_size = 10000;
dbg_print_buf ccr_dbg(ccr_dbg_size);
#endif

ccr_version_map_t
    ccr_vp_to_idl[CCR_VP_MAX_MAJOR + 1][CCR_VP_MAX_MINOR + 1] = {
	{ 0, 0, 0 },	// VP version 0.0 defined for indexing
	{ 0, 0, 0 },	// VP version 1.0
	{ 1, 1, 1 }	// VP version 2.0, layered CCR
};

ccr_repl_prov *the_service = NULL;

ccr_repl_prov::ccr_repl_prov(char *desc):
	repl_server<ccr_trans::ckpt>("ccr_server", desc),
	mgr(NULL), _ckpt_proxy(NULL)
{
	root_obj_ref = ccr_trans::manager::_nil();
	current_version.major_num = 1;
	current_version.major_num = 0;
}

// Lint override needed because lint doesn't know that _nil() is the same as
// NULL.
ccr_repl_prov::~ccr_repl_prov()
{
	CCR_DBG(("in ccr_repl_prov destructor\n"));
	mgr = NULL;
	root_obj_ref = ccr_trans::manager::_nil();
	_ckpt_proxy = NULL;
} //lint !e1740

void
ccr_repl_prov::initialize()
{
	Environment e;
	char desc[10];
	os::sprintf(desc, "%d", orb_conf::node_number());

	ASSERT(the_service == NULL);
	the_service = new ccr_repl_prov(desc);

	//
	// Register for Upgrade Callbacks.
	//

	//
	// Get a pointer to the local version manager.
	//
	version_manager::vm_admin_var vmgr_v = vm_util::get_vm(NODEID_UNKNOWN);
	if (CORBA::is_nil(vmgr_v)) {
		CL_PANIC(!"CCR: Could not get vm_admin object");
	}

	// Build a UCC for support of version upgrade callbacks
	version_manager::ucc_seq_t ucc_seq(1, 1);
	version_manager::string_seq_t freeze_seq(1, 1);

	ucc_seq[0].ucc_name = os::strdup("ccr");
	ucc_seq[0].vp_name = os::strdup("ccr");
	freeze_seq[0] = os::strdup("ccr_server");
	ucc_seq[0].freeze = freeze_seq;

	// Create a version upgrade callback object.
	ccr_upgrade_callback *cbp =
	    new ccr_upgrade_callback(*the_service);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();
	version_manager::vp_version_t	callback_limit;
	version_manager::vp_version_t	cur_version;

	cur_version.major_num = 1;
	cur_version.minor_num = 0;

	//
	// Register the callback object with the Version Manager. The
	// tmp_version will be returned.  The version lock is not held
	// since the replica is not yet registered with the HA framework
	// so there can not be a call to become_primary. The current version
	// is returned regardless.
	//
	// If the running version is less than the callback_limit
	// a callback will be registered, otherwise a callback is not
	// registered (currently no way to tell).  The current running
	// version is returned regardless.
	//
	callback_limit.major_num = 2;
	callback_limit.minor_num = 0;
	vmgr_v->register_upgrade_callbacks(ucc_seq, cb_v,
	    callback_limit, cur_version, e);
	if (e.exception()) {
		e.exception()->print_exception("CCR: "
		    "Failed register_upgrade_callbacks()");
		e.clear();
		CCR_DBG(("ccr_repl_prov::initialize: "
		    "Failed to register_upgrade_callbacks()\n"));
	}

	// Establish the current version in the provider
	the_service->set_init_version(cur_version);

	the_service->register_with_rm(1, true, e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR transaction manager failed to register with the
		// cluster HA framework.
		// @user_action
		// This is an unrecoverable error, and the cluster needs to be
		// rebooted. Also contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) ccr_syslog_msg.log(SC_SYSLOG_PANIC, MESSAGE,
		    "CCR: CCR transaction manager failed to register with "
		    "the cluster HA framework.");
	}
}

//
// startup
// called by become_primary()
//
void
ccr_repl_prov::startup(Environment &e)
{
	CORBA::type_info_t	*typ =
	    ccr_trans::manager::_get_type_info(
	    ccr_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].ccr_manager_vers);
	//
	// create the manager if not already created
	// else pass it ref to this repl_prov
	// mgr could have been created on ckpt if this is not
	// the first become_primary
	//
	if (mgr == NULL) {
		mgr = new manager_impl(this);
		CL_PANIC(CORBA::is_nil(root_obj_ref));
		root_obj_ref = mgr->get_objref(typ);
		mgr->get_checkpoint()->ckpt_create_manager(root_obj_ref, e);
		CL_PANIC(!e.exception());
	} else {
		mgr->set_repl_prov(this);
		mgr->set_primary_status(true);
	}

	if (CORBA::is_nil(root_obj_ref)) {
		//
		// get and store objref for use in get_root_obj
		// will be released on shutdown.
		//
		root_obj_ref = mgr->get_objref(typ);
	}
}

// become primary is called:
// - when service first starts up
// - when the old primary dies and service fails over to this node
// on failover case must initiate recovery

void
ccr_repl_prov::become_primary(const CORBA::StringSeq&, Environment &e)
{
	version_manager::vm_admin_var	vm_v;
	CORBA::Exception		*exp;
	replica::checkpoint_var		tmp_ckpt_v;
	CORBA::type_info_t		*typ = NULL;

	version_lock.rdlock();
	CCR_DBG(("ccr server::become_primary, current version is %d.%d.\n",
	    current_version.major_num, current_version.minor_num));

	// First, initialize the checkpoint proxy.
	typ = ccr_trans::ckpt::_get_type_info(
	    ccr_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].ccr_ckpt_vers);
	ASSERT(typ != NULL);

	tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = ccr_trans::ckpt::_narrow(tmp_ckpt_v);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	startup(e);
	version_lock.unlock();

	// this could be takeover, so check quorum
	(void) mgr->check_quorum();
}

CORBA::Object_ptr
ccr_repl_prov::get_root_obj(Environment &)
{
	ccr_trans::manager_ptr mgr_p;
	// should only be called on primary
	CL_PANIC(!CORBA::is_nil(root_obj_ref));
	version_lock.rdlock();
	mgr_p = ccr_trans::manager::_duplicate(root_obj_ref);
	version_lock.unlock();
	return (mgr_p);
}

// Note: become_secondary is not called on startup,
// only on switchover
void ccr_repl_prov::become_secondary(Environment &)
{
	CCR_DBG(("ccr server::become_secondary\n"));

	mgr->set_primary_status(false);
	// call unfreeze since freeze_primary was called before
	// allow checkpoints to be processed
	mgr->unfreeze();

	ASSERT(!CORBA::is_nil(_ckpt_proxy));

	// Release the reference to the multi_ckpt_handler.
	CORBA::release(_ckpt_proxy);
	_ckpt_proxy = nil;
}

void
ccr_repl_prov::add_secondary(replica::checkpoint_ptr sec, const char *,
			Environment &)
{
	CCR_DBG(("ccr server::add_secondary\n"));

	// narrow the ckpt ref to our ckpt type
	ccr_trans::ckpt_var sec_ckpt = ccr_trans::ckpt::_narrow(sec);

	// dump state to new secondary through its ckpt interface
	mgr->dump_state(sec_ckpt);

	// num_secondaries++;
}

void
ccr_repl_prov::remove_secondary(const char *, Environment &)
{
	CCR_DBG(("ccr server::remove_secondary\n"));

	// num_secondaries--;
	// table copies will be deleted when dummy object is unreferenced
}

void
ccr_repl_prov::freeze_primary_prepare(Environment &)
{
}

void
ccr_repl_prov::freeze_primary(Environment &)
{
	CCR_DBG(("ccr server::freeze_primary\n"));

	// we get this before adding secondary or new primary taking over
	// make sure no invocations to the service will block waiting for
	// another invocation or _unreferenced(), to prevent deadlock
	// we can wait for requests to complete and lock out new ones
	mgr->freeze();
}

void
ccr_repl_prov::unfreeze_primary(Environment &)
{
	CCR_DBG(("ccr server::unfreeze_primary\n"));

	// reset state
	// invocations and _unreferenced are enabled after this returns
	mgr->unfreeze();
}

// Convert to being a spare.
void
ccr_repl_prov::become_spare(Environment &)
{
	// If mgr is NULL, then this is a no-op since
	// since there are no data structs to free etc.
	if (!mgr)
		return;

	// Must not be primary to become_spare.
	CL_PANIC(!(mgr->is_primary()));

	CCR_DBG(("ccr server::become_spare\n"));
	// free all data structs that were created as a result
	// of dump_state()/ckpt_*, in reverse order to that
	// in dump_state.

	// First all the transactions.
	mgr->convert_spare_transactions();

	// Second any free'ing corres to the recovery_done hkpt.
	mgr->convert_spare_recovery();

	// Third all the DS handles.
	mgr->convert_spare_data_servers();

	// Finally the manager itself.
	// Lint override needed since lint is complaining about a
	// possible memory leak, which is obviously not true since
	// the object is being deleted before being set to NULL.
	delete mgr;
	mgr = NULL;	//lint !e423
}

void
ccr_repl_prov::shutdown(Environment &e)
{
	CCR_DBG(("ccr server::shutdown\n"));
	// We don't support shutdown method here
	e.exception(new replica::service_busy());

	//
	// Since we are returning an exception, don't release the
	// checkpoint proxy.
	//
}

//
// Set the initial version number.
//
void
ccr_repl_prov::set_init_version(const version_manager::vp_version_t
	&version)
{
	version_lock.wrlock();
	if (current_version.major_num < version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num < version.minor_num)) {
		current_version = version;
	}
	version_lock.unlock();
}

//
// Callback routine that gets invoked when the version manager
// commits the upgrade.
//
void
ccr_repl_prov::upgrade_callback(const version_manager::vp_version_t &version,
    Environment &e)
{
	CORBA::type_info_t *typ;

	//
	// Note that upgrade callbacks are not synchronized with
	// calls to become_primary(), add_secondary(), etc.
	// Getting this lock makes sure the replica state doesn't change.
	//
	version_lock.wrlock();
	if (current_version.major_num > version.major_num ||
	    (current_version.major_num == version.major_num &&
	    current_version.minor_num >= version.minor_num)) {
		// Version isn't changing so just return.
		version_lock.unlock();
		return;
	}
	//
	// Set the current version.
	//
	current_version = version;

	CCR_DBG(("upgrade_callback(%p): major = %d minor = %d ", this,
	    current_version.major_num, current_version.minor_num));

	//
	// If this is a secondary, then there is no more work to be done.
	//
	if (CORBA::is_nil(_ckpt_proxy)) {
		version_lock.unlock();
		return;
	}

	//
	// Switch the checkpoint interface to the new protocol. Save
	// the current _ckpt_proxy and release it after we get a new one
	//
	ccr_trans::ckpt_ptr old_ckpt_p = _ckpt_proxy;

	typ = ccr_trans::ckpt::_get_type_info(
	    ccr_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].ccr_ckpt_vers);

	replica::checkpoint_var tmp_ckpt_v = set_checkpoint(typ);
	_ckpt_proxy = ccr_trans::ckpt::_narrow(tmp_ckpt_v);
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	CORBA::release(old_ckpt_p);

	// Update the manager reference in the provider.
	ccr_trans::manager_ptr old_mgr_p = root_obj_ref;

	typ = ccr_trans::manager::_get_type_info(
	    ccr_vp_to_idl[current_version.major_num]
	    [current_version.minor_num].ccr_manager_vers);
	ASSERT(typ != NULL);
	root_obj_ref = mgr->get_objref(typ);
	ASSERT(!CORBA::is_nil(root_obj_ref));

	//
	// Create and add a primary context so the provider can send
	// checkpoints while the service is frozen.
	//
	primary_ctx ctx(NULL, primary_ctx::ADD_SECONDARY_CKPT, e);

	// Checkpoint the current version number.
	_ckpt_proxy->ckpt_service_version(
	    current_version.major_num,
	    current_version.minor_num, e);

	e.trans_ctxp = NULL;
	version_lock.unlock();
}

//
// Checkpoint methods
//

void
ccr_repl_prov::ckpt_create_manager(ccr_trans::manager_ptr mgr_ref,
				Environment &)
{
	CCR_DBG(("ccr ckpt_create_manager\n"));

	// create shadow copy of manager
	CL_PANIC(mgr == NULL);
	mgr = new manager_impl(mgr_ref);
}

void
ccr_repl_prov::ckpt_reg_data_server(ccr_data::data_server_ptr dsp,
				quorum::quorum_device_id_t qid,
				ccr_trans::handle_ptr handlep,
				bool deactivated,
				Environment &e)
{
	CCR_DBG(("ccr ckpt_reg_data_server\n"));

	// call down to manager routine
	ASSERT(mgr);
	mgr->ckptwork_reg_data_server(dsp, qid, handlep, deactivated, e);
}

void
ccr_repl_prov::ckpt_all_zc_recovery_done(Environment &)
{
	CCR_DBG(("ccr ckpt_all_zc_recovery_done\n"));
	// Call the Manager routine.
	ASSERT(mgr);
	mgr->ckptwork_all_zc_recovery_done();
}

void
ccr_repl_prov::ckpt_recovery_done(const ccr::element_seq &dir_elems,
				ccr_data::epoch_type epoch_num,
				const ccr_data::consis_seq &recs,
				Environment &)
{
	CCR_DBG(("ccr ckpt_recovery_done\n"));

	// call down to manager routine
	ASSERT(mgr);
	mgr->ckptwork_recovery_done(dir_elems, epoch_num, recs);
}

void
ccr_repl_prov::ckpt_recovery_done_zc(uint_t cluster_id,
				const char *cluster,
				uint_t num_tables,
				const ccr::element_seq &dir_elems,
				const ccr_data::consis_seq &recs,
				Environment &)
{
	CCR_DBG(("ccr ckpt_recovery_done_zc, cluster = %s\n", cluster));

	// call down to manager routine
	ASSERT(mgr);
	mgr->ckptwork_recovery_done_zc(cluster_id, cluster, num_tables,
	    dir_elems, recs);
}

void
ccr_repl_prov::ckpt_create_transaction(ccr::updatable_table_ptr trans_ref,
				const char *table_name,
				const ccr_data::updatable_copy_seq &copies,
				const quorum::ccr_id_list_t &ccr_ids,
				bool transaction_done,
				Environment &e)
{
	CCR_DBG(("in ckpt_create_transaction\n"));
	// call down to manager method
	ASSERT(mgr);
	mgr->ckptwork_create_transaction(trans_ref, DEFAULT_CLUSTER,
	    table_name, copies, ccr_ids, transaction_done, e);
}

void
ccr_repl_prov::ckpt_create_transaction_zc(ccr::updatable_table_ptr trans_ref,
				const char *cluster_name,
				const char *table_name,
				const ccr_data::updatable_copy_seq &copies,
				const quorum::ccr_id_list_t &ccr_ids,
				bool transaction_done,
				Environment &e)
{
	CCR_DBG(("in ckpt_create_transaction\n"));
	// call down to manager method
	ASSERT(mgr);
	mgr->ckptwork_create_transaction(trans_ref, cluster_name, table_name,
	    copies, ccr_ids, transaction_done, e);
}

void
ccr_repl_prov::ckpt_start_update(const char *table_name, Environment &e)
{
	CCR_DBG(("in ckpt_start_update\n"));
	// call down to transaction method
	ASSERT(mgr);
	table_info *tinfo = mgr->lookup_table(DEFAULT_CLUSTER, table_name);
	ASSERT(tinfo && tinfo->running_trans);
	tinfo->running_trans->ckptwork_start_update(e);
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_start_update_zc(const char *cluster_name,
    const char *table_name, Environment &e)
{
	CCR_DBG(("in ckpt_start_cl_update\n"));
	// call down to transaction method
	ASSERT(mgr);
	table_info *tinfo = mgr->lookup_table(cluster_name, table_name);
	ASSERT(tinfo && tinfo->running_trans);
	tinfo->running_trans->ckptwork_start_update(e);
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_prepared_update(Environment &e)
{
	CCR_DBG(("in ckpt_prepared_update\n"));
	// call down to static transaction method
	table_trans_impl::ckptwork_prepared_update(e);
}

void
ccr_repl_prov::ckpt_rollback_update(Environment &e)
{
	CCR_DBG(("in ckpt_rollback_update\n"));
	// call down to static transaction method
	table_trans_impl::ckptwork_rollback_update(e);
}

void
ccr_repl_prov::ckpt_abort_update(Environment &e)
{
	CCR_DBG(("in ckpt_abort_update\n"));
	// call down to static transaction method
	table_trans_impl::ckptwork_abort_update(e);
}

void
ccr_repl_prov::ckpt_trans_modified(Environment &e)
{
	CCR_DBG(("in ckpt_trans_modified\n"));

	// get context from framework
	secondary_ctx *ctxp = secondary_ctx::extract_from(e);
	CL_PANIC(e.exception() == NULL);
	// get state registered earlier, check sanity
	update_state *saved_state = (update_state *)ctxp->get_saved_state();
	ASSERT(saved_state && saved_state->transp);
	saved_state->transp->ckptwork_trans_modified();
}

//
// zc_ckpt_op:
// called by table_trans_impl after a cluster update, i.e. add or
// remove of a virtual cluster. There is no trasaction state when
// this called, since update_state has been committed and commit_state
// has not been initialized.
//
void
ccr_repl_prov::zc_ckpt_op(ccr::update_type op, uint_t cluster_id,
    const char *cluster_name, Environment &)
{
	table_info *tinfo;

	CCR_DBG(("in ccr ckpt_cluster_op\n"));
	tinfo = mgr->lookup_table(DEFAULT_CLUSTER, CLUSTER_DIR);
	ASSERT(tinfo->running_trans);
	tinfo->running_trans->set_cluster_op(op, cluster_id, cluster_name);
	tinfo->rele();
}

//
// ckpt_directory_op:
// called by table_trans_impl after a directory update
// informs secondaries what table is being created or removed
// note: there is no transaction state when this is called, since
// update_state has been committed and commit_state has not been initialized.
//
void
ccr_repl_prov::ckpt_directory_op(ccr::update_type op, const char *table_name,
    Environment &env)
{
	table_info *tinfo;

	CCR_DBG(("in ccr ckpt_directory_op\n"));

	ckpt_directory_op_zc(op, DEFAULT_CLUSTER, table_name, env);
}

void
ccr_repl_prov::ckpt_directory_op_zc(ccr::update_type op,
    const char *cluster_name, const char *table_name, Environment &)
{
	table_info *tinfo;

	CCR_DBG(("in ccr ckpt_directory_op\n"));

	tinfo = mgr->lookup_table(cluster_name, DIR);
	ASSERT(tinfo->running_trans);
	tinfo->running_trans->set_directory_op(op, table_name);
	tinfo->rele();
}

//
// ckpt_table_delete:
// Called by TM when dumping state of a transaction that has its
// delete_flag set.
//
void
ccr_repl_prov::ckpt_table_delete(const char *table_name, Environment &)
{
	table_info *tinfo;

	CCR_DBG(("in ccr ckpt_file_delete\n"));

	tinfo = mgr->lookup_table(DEFAULT_CLUSTER, table_name);
	ASSERT(tinfo->running_trans);
	tinfo->running_trans->set_table_delete();
}


void
ccr_repl_prov::ckpt_table_delete_zc(const char *cluster_name,
    const char *table_name, Environment &)
{
	table_info *tinfo;

	CCR_DBG(("in ccr ckpt_file_delete\n"));

	tinfo = mgr->lookup_table(cluster_name, table_name);
	ASSERT(tinfo->running_trans);
	tinfo->running_trans->set_table_delete();
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_start_commit(const char *table_name, Environment &e)
{
	CCR_DBG(("in ckpt_start_commit\n"));
	// call down to transaction method
	ASSERT(mgr);
	table_info *tinfo = mgr->lookup_table(DEFAULT_CLUSTER, table_name);
	ASSERT(tinfo && tinfo->running_trans);
	tinfo->running_trans->ckptwork_start_commit(e);
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_start_commit_zc(const char *cluster_name,
    const char *table_name, Environment &e)
{
	CCR_DBG(("in ckpt_start_commit\n"));
	// call down to transaction method
	ASSERT(mgr);
	table_info *tinfo = mgr->lookup_table(cluster_name, table_name);
	ASSERT(tinfo && tinfo->running_trans);
	tinfo->running_trans->ckptwork_start_commit(e);
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_start_abort(const char *table_name, Environment &e)
{
	CCR_DBG(("in ckpt_start_abort\n"));
	// call down to transaction method
	ASSERT(mgr);
	table_info *tinfo = mgr->lookup_table(DEFAULT_CLUSTER, table_name);
	ASSERT(tinfo && tinfo->running_trans);
	tinfo->running_trans->ckptwork_start_abort(e);
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_start_abort_zc(const char *cluster_name,
    const char *table_name, Environment &e)
{
	CCR_DBG(("in ckpt_start_abort\n"));
	// call down to transaction method
	ASSERT(mgr);
	table_info *tinfo = mgr->lookup_table(cluster_name, table_name);
	ASSERT(tinfo && tinfo->running_trans);
	tinfo->running_trans->ckptwork_start_abort(e);
	tinfo->rele();
}

void
ccr_repl_prov::ckpt_abort_transaction(Environment &e)
{
	CCR_DBG(("in ckpt_abort_transaction\n"));
	// call down to static transaction method
	table_trans_impl::ckptwork_abort_transaction(e);
}

//
// Checkpoint the new service version
//
void
ccr_repl_prov::ckpt_service_version(unsigned short new_major,
    unsigned short new_minor, Environment &)
{
	version_lock.wrlock();
	CCR_DBG(("in ckpt_service_version\n"));
	current_version.major_num = new_major;
	current_version.minor_num = new_minor;
	version_lock.unlock();
}

ccr_trans::ckpt_ptr
ccr_repl_prov::get_checkpoint_ccr_trans()
{
	ASSERT(!CORBA::is_nil(_ckpt_proxy));
	return (_ckpt_proxy);
}

//
// ccr_upgrade_callback class methods
//
ccr_upgrade_callback::ccr_upgrade_callback(ccr_repl_prov &ccr_prov) :
    prov(ccr_prov)
{
}

ccr_upgrade_callback::~ccr_upgrade_callback()
{
}

void
#ifdef DEBUG
ccr_upgrade_callback::_unreferenced(unref_t cookie)
#else
ccr_upgrade_callback::_unreferenced(unref_t)
#endif
{
	// This object does not support multiple 0->1 transitions.
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// Callback routine registered with version manager.
//
void
ccr_upgrade_callback::do_callback(const char *,
    const version_manager::vp_version_t &new_version, Environment &e)
{
	CCR_DBG(("ccr_upgrade_callback::do_callback().\n"));

	// Call the provider to update the version.
	prov.upgrade_callback(new_version, e);
}
