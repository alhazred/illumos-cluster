/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_QDM_H
#define	_FAULTNUM_QDM_H

#pragma ident	"@(#)faultnum_qdm.h	1.1	08/06/03 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the QDM fault point group.
 */

#ifdef FAULT_QDM
#define	FAULTPT_QDM(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_QDM(n, f)
#endif

/*
 * Fault numbers for the QDM fault point group.
 * For QDM, the FAULTBASE_QDM is equal to 1179648.
 */

#define	FAULTNUM_QD_BAD		(FAULTBASE_QDM | 0x0001)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_QDM_H */
