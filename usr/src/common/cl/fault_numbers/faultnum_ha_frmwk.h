/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_HA_FRMWK_H
#define	_FAULTNUM_HA_FRMWK_H

#pragma ident	"@(#)faultnum_ha_frmwk.h	1.41	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the HA framework group.
 */

#ifdef FAULT_HA_FRMWK
#define	FAULTPT_HA_FRMWK(n, f)		FAULT_POINT((n), (f))
#define	FAULTPT_HA_FRMWK_THIS_SVC(n, s, f)	\
		FaultFunctions::generic_this_svc((n), (s), (f))
#else
#define	FAULTPT_HA_FRMWK(n, f)
#define	FAULTPT_HA_FRMWK_THIS_SVC(n, s, f)
#endif

/*
 * Fault Point Number Definitions
 * Please refer to src/orbtest/repl_sample/faults/README.faults
 * For a complete description of each faultpoint
 */

/*
 * Naming Convention
 * For the HA faults I've used the following naming convention
 *
 * FILENAME_FUNCTION_NAME
 *
 * HXDOOR_RESERVE_RESOURCES_1
 * Will inject a fault before the client's invocation reaches the priamry.
 * The fault is located before the underlying xdoor is chosen.
 */

#define	FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_1 \
						(FAULTBASE_HA_FRMWK | 1)

/*
 * HXDOOR_RESERVE_RESOURCES_2
 * Will inject a faultl Before the client's invocation reaches the primary.
 * The fault is triggered after the framework has chosen a specific xdoor to
 * communicate with the target node through
 */

#define	FAULTNUM_HA_FRMWK_HXDOOR_RESERVE_RESOURCES_2 \
						(FAULTBASE_HA_FRMWK | 2)

/*
 * REPL_SAMPLE_GET_VALUE
 * Will trigger a fault at the beginning of the invocation on the ha service
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_GET_VALUE \
						(FAULTBASE_HA_FRMWK | 3)
/*
 * HXDOOR_SEND_REPLY
 * Will trigger a fault during the send_reply of an invocation.  (After it
 * has completed)
 */

#define	FAULTNUM_HA_FRMWK_HXDOOR_SEND_REPLY \
						(FAULTBASE_HA_FRMWK | 5)
/*
 * REPL_SERVICE_TRY_LOCAL_INVO
 * Will trigger a fault during a local invocation
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_TRY_LOCAL_INVO \
						(FAULTBASE_HA_FRMWK | 6)
/*
 * REPL_SAMPLE_SET_VALUE
 * Will trigger a fault at the beginning of the invocation on the ha service
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_VALUE \
						(FAULTBASE_HA_FRMWK | 7)
/*
 * CKPT_HANDLER_INVOKE_1
 * Will trigger a fault before a checkpoint is sent to a secondary
 */

#define	FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_1 \
						(FAULTBASE_HA_FRMWK | 8)

/*
 * CKPT_HANDLER_HANDLE_INCOMING_CALL
 * Will trigger a fault when a checkpoint is received by a secondary
 */

#define	FAULTNUM_HA_FRMWK_CKPT_HANDLER_HANDLE_INCOMING_CALL \
						(FAULTBASE_HA_FRMWK | 9)
/*
 * CKPT_HANDLER_INVOKE_2
 * Will trigger a fault during a checkpoint.  After it is sent to the secondary
 * and it has returned
 */

#define	FAULTNUM_HA_FRMWK_CKPT_HANDLER_INVOKE_2 \
						(FAULTBASE_HA_FRMWK | 10)

/*
 * REPL_SAMPLE_INC_VALUE
 * Will trigger a fault at the beginning of the invocation on the ha service
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE \
						(FAULTBASE_HA_FRMWK | 11)

/*
 * RECONFIGURE_1
 * The reconfigure faults are for fault-testing during switchovers
 */

#define	FAULTNUM_HA_FRMWK_RM_SERVICE_UP \
						(FAULTBASE_HA_FRMWK | 12)
#define	FAULTNUM_HA_FRMWK_RM_FREEZE_SERVICE	\
						(FAULTBASE_HA_FRMWK | 13)
#define	FAULTNUM_HA_FRMWK_RM_SERVICE_FROZEN_CLEAN \
						(FAULTBASE_HA_FRMWK | 14)
#define	FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_UNFREEZE_PRIMARY \
						(FAULTBASE_HA_FRMWK | 15)
#define	FAULTNUM_HA_FRMWK_RM_NEW_SECONDARY \
						(FAULTBASE_HA_FRMWK | 16)
#define	FAULTNUM_HA_FRMWK_RM_DISCONNECT \
						(FAULTBASE_HA_FRMWK | 17)
#define	FAULTNUM_HA_FRMWK_RM_SEC_WRAPUP \
						(FAULTBASE_HA_FRMWK | 18)
#define	FAULTNUM_HA_FRMWK_RM_NO_PRIMARY \
						(FAULTBASE_HA_FRMWK | 19)
#define	FAULTNUM_HA_FRMWK_RM_PRIMARY_PREPARE \
						(FAULTBASE_HA_FRMWK | 20)
#define	FAULTNUM_HA_FRMWK_RM_RECONNECT	\
						(FAULTBASE_HA_FRMWK | 21)
#define	FAULTNUM_HA_FRMWK_RM_START_PRIMARY \
						(FAULTBASE_HA_FRMWK | 22)
#define	FAULTNUM_HA_FRMWK_RM_UNFREEZE_PRIMARY \
						(FAULTBASE_HA_FRMWK | 23)
#define	FAULTNUM_HA_FRMWK_RM_UNFREEZE_SERVICE \
						(FAULTBASE_HA_FRMWK | 24)
#define	FAULTNUM_HA_FRMWK_RM_ROOT_OBJ_BCAST \
						(FAULTBASE_HA_FRMWK | 25)
#define	FAULTNUM_HA_FRMWK_RM_RECONFIG_EXIT \
						(FAULTBASE_HA_FRMWK | 26)
#define	FAULTNUM_HA_FRMWK_RM_BEFORE_MOVE_TO_SFD \
						(FAULTBASE_HA_FRMWK | 27)

/*
 * Fault within the sample service prior to the 1st checkpoint
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_2 \
						(FAULTBASE_HA_FRMWK | 28)

/*
 * Fault within the sample service after the 1st checkpoint
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_INC_VALUE_3 \
						(FAULTBASE_HA_FRMWK | 29)

/*
 * Fault during the action of the 2-phase mini-transaction
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_PERSIST_STORE_SET_VALUE \
						(FAULTBASE_HA_FRMWK | 30)

/*
 * Fault within the sample service after the action is taken
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SET_INC_VALUE \
						(FAULTBASE_HA_FRMWK | 31)

/*
 * Fault after add_commit() - inside of the commit call
 */
#define	FAULTNUM_HA_FRMWK_REPLICA_TMPL_ADD_COMMIT \
						(FAULTBASE_HA_FRMWK | 32)


/*
 * Fault withitn register_with_rm() before the service has been registered
 */
#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_1 \
						(FAULTBASE_HA_FRMWK | 33)

/*
 * Fault withitn register_with_rm() after the service has been registered
 * but before the prov is registerd.. causes the service to be in S_UNINIT
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_REGISTER_WITH_RM_2 \
						(FAULTBASE_HA_FRMWK | 34)

/*
 * Fault after the rm has recevied a add_rma and completed the transaction
 */

#define	FAULTNUM_HA_FRMWK_REPL_MGR_IMPL_ADD_RMA \
						(FAULTBASE_HA_FRMWK | 35)

/*
 * Return an exception during become_primary()
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_PRIMARY \
						(FAULTBASE_HA_FRMWK | 36)

/*
 * Return an exception during unfreeze_primary()
 */
#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_BECOME_SECONDARY \
						(FAULTBASE_HA_FRMWK | 37)

/*
 * Fault when the primary receives the shutdown command.
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_SHUTDOWN \
						(FAULTBASE_HA_FRMWK | 38)

/*
 * Fault when the primary receives the shutdown command.
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE \
						(FAULTBASE_HA_FRMWK | 39)

/*
 * Fault when the primary receives the shutdown command.
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY \
						(FAULTBASE_HA_FRMWK | 40)

/*
 * Shutdown repl_sample without waiting for unref
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_SHUTDOWN_W_O_WAITING_FOR_UNREF \
						(FAULTBASE_HA_FRMWK | 41)

/*
 * Fault point during add secondary with a partial dump complete
 */

#define	FAULTNUM_HA_FRMWK_REPL_SAMPLE_IMPL_ADD_SECONDARY \
						(FAULTBASE_HA_FRMWK | 42)

/*
 * Fault point during rm_repl_service::iterate_over
 */

#define	FAULTNUM_RM_REPL_SERVICE_ITERATE_OVER \
						(FAULTBASE_HA_FRMWK | 43)

/*
 * Fault point during rm_state_mach::connect_to_primary
 */

#define	FAULTNUM_RM_STATE_MACH_CONNECT_PRIMARY \
						(FAULTBASE_HA_FRMWK | 44)

/*
 * Fault point before the ckpt in repl_mgr_impl::register_service
 */

#define	FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_1 \
						(FAULTBASE_HA_FRMWK | 45)

/*
 * Fault point before the ckpt in repl_mgr_impl::add_rma
 */

#define	FAULTNUM_HA_RM_CKPT_ADD_RMA_1 \
						(FAULTBASE_HA_FRMWK | 46)

/*
 * Fault point before the ckpt in rm_repl_service::shutdown_service
 */

#define	FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_1 \
						(FAULTBASE_HA_FRMWK | 47)

/*
 * Fault point before the ckpt in rm_repl_service::register_rma_repl_prov
 */

#define	FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_1 \
						(FAULTBASE_HA_FRMWK | 48)

/*
 * Fault point before the ckpt in repl_mgr_impl::get_control
 */

#define	FAULTNUM_HA_RM_CKPT_GET_CONTROL_1 \
						(FAULTBASE_HA_FRMWK | 49)

/*
 * Fault point after the ckpt in repl_mgr_impl::register_service
 */

#define	FAULTNUM_HA_RM_CKPT_REGISTER_SERVICE_2 \
						(FAULTBASE_HA_FRMWK | 50)

/*
 * Fault point after the ckpt in repl_mgr_impl::add_rma
 */

#define	FAULTNUM_HA_RM_CKPT_ADD_RMA_2 \
						(FAULTBASE_HA_FRMWK | 51)

/*
 * Fault point after the ckpt in rm_repl_service::shutdown_service
 */

#define	FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_2 \
						(FAULTBASE_HA_FRMWK | 52)

/*
 * Fault point before the ckpt in rm_repl_service::register_rma_repl_prov
 */

#define	FAULTNUM_HA_RM_CKPT_REGISTER_REPL_PROV_2 \
						(FAULTBASE_HA_FRMWK | 53)

/*
 * Fault point after the ckpt in repl_mgr_impl::get_control
 */

#define	FAULTNUM_HA_RM_CKPT_GET_CONTROL_2 \
						(FAULTBASE_HA_FRMWK | 54)

/*
 * Fault point after the ckpt in rm_repl_service::shutdown_service
 */

#define	FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_3 \
						(FAULTBASE_HA_FRMWK | 55)

/*
 * Fault point after the ckpt in rm_repl_service::shutdown_service
 */

#define	FAULTNUM_HA_RM_CKPT_SHUTDOWN_SERVICE_4 \
						(FAULTBASE_HA_FRMWK | 56)

/*
 * Fault point for local to remote invocation
 */

#define	FAULTNUM_HA_FRMWK_LOCAL_REMOTE \
						(FAULTBASE_HA_FRMWK | 57)

/*
 * Fault point for remote to local invocation
 */

#define	FAULTNUM_HA_FRMWK_REMOTE_LOCAL \
						(FAULTBASE_HA_FRMWK | 58)

/*
 * Fault point to hold xdoor ack
 */

#define	FAULTNUM_HA_FRMWK_HOLD_XDOOR_ACK \
						(FAULTBASE_HA_FRMWK | 59)

/*
 * Fault point that causes node 2 to exit
 */

#define	FAULTNUM_HA_FRMWK_KILL_NODE_2	\
						(FAULTBASE_HA_FRMWK | 60)

/*
 * Fault point that transfer threads from unref_threadpool
 * to invocation threadpool. For bugId 4644079
 */

#define	FAULTNUM_HA_TRANSFER_TASK_TO_UNREF_THR	\
						(FAULTBASE_HA_FRMWK | 61)

/*
 * Fault when the primary is converted to a secondary
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SECONDARY	\
						(FAULTBASE_HA_FRMWK | 62)

/*
 * Fault when a new secondary is added to the service
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_ADD_SECONDARY	\
						(FAULTBASE_HA_FRMWK | 63)

/*
 * Fault when the spare is being made to become a secondary
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_COMMIT_SECONDARY	\
						(FAULTBASE_HA_FRMWK | 64)

/*
 * Fault when the secondary is being made to become the primary
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_PRIMARY	\
						(FAULTBASE_HA_FRMWK | 65)

/*
 * Fault when the primary is to remove a secondary
 */
#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_REMOVE_SECONDARY_2	\
						(FAULTBASE_HA_FRMWK | 66)

/*
 * Fault when the secondary is being made to become the spare
 */

#define	FAULTNUM_HA_FRMWK_REPL_SERVICE_BECOME_SPARE_2	\
						(FAULTBASE_HA_FRMWK | 67)

/*
 * Force become_primary to return fail exception
 */

#define	FAULTNUM_HA_RM_FAIL_START_NEW_PRIMARY	\
						(FAULTBASE_HA_FRMWK | 68)

/*
 * Fault point to test 4372770.
 */

#define	FAULTNUM_HA_FRMWK_RM_SVC_FRZN_CLN_SWITCHOVER \
						(FAULTBASE_HA_FRMWK | 69)
/*
 * Fault point before calling register_lca()
 */

#define	FAULTNUM_VM_LCA_BEFORE_REGISTER_LCA	\
						(FAULTBASE_HA_FRMWK | 70)

/*
 * Fault point after calling register_lca()
 */

#define	FAULTNUM_VM_LCA_AFTER_REGISTER_LCA	\
						(FAULTBASE_HA_FRMWK | 71)
/*
 * Fault point before calling ckpt_register_lca()
 */

#define	FAULTNUM_HA_RM_BEFORE_CKPT_REGISTER_LCA	\
						(FAULTBASE_HA_FRMWK | 72)

/*
 * Fault point after calling ckpt_register_lca()
 */

#define	FAULTNUM_HA_RM_AFTER_CKPT_REGISTER_LCA	\
						(FAULTBASE_HA_FRMWK | 73)

/*
 * Fault when service and its dependents are frozen
 */

#define	FAULTNUM_HA_FRMWK_SERVICE_FROZEN_FOR_UPGRADE	\
						(FAULTBASE_HA_FRMWK | 74)
/*
 * Fault primary after dependent service frozen
 */

#define	FAULTNUM_HA_FRMWK_KILL_PRI_OF_FROZEN_SERVICE	\
						(FAULTBASE_HA_FRMWK | 75)

/*
 * Fault point in rm_state_machine::add_secondary
 */

#define	FAULTNUM_HA_FRMWK_RM_ADD_SECONDARY_1		\
						(FAULTBASE_HA_FRMWK | 76)

/*
 * Fault point in rm_state_machine::add_secondary
 */

#define	FAULTNUM_HA_FRMWK_RM_ADD_SECONDARY_2		\
						(FAULTBASE_HA_FRMWK | 77)

/*
 * Fault point in rm_state_machine::commit_secondary
 */

#define	FAULTNUM_HA_FRMWK_RM_COMMIT_SECONDARY		\
						(FAULTBASE_HA_FRMWK | 78)

/*
 * Fault point in rm_state_machine::remove_secondaries
 */

#define	FAULTNUM_HA_FRMWK_RM_REMOVE_SECONDARIES_1	\
						(FAULTBASE_HA_FRMWK | 79)

/*
 * Fault point in rm_state_machine::remove_secondaries
 */

#define	FAULTNUM_HA_FRMWK_RM_REMOVE_SECONDARIES_2	\
						(FAULTBASE_HA_FRMWK | 80)

/*
 * Fault point in rm_state_machine::remove_secondaries
 */

#define	FAULTNUM_HA_FRMWK_RM_REMOVE_SECONDARIES_3	\
						(FAULTBASE_HA_FRMWK | 81)

/*
 * Fault point before ckpt freeze_for_upgrade
 */

#define	FAULTNUM_HA_RM_CKPT_FREEZE_FOR_UPGRADE_1	\
						(FAULTBASE_HA_FRMWK | 82)

/*
 * Fault point after ckpt freeze_for_upgrade
 */

#define	FAULTNUM_HA_RM_CKPT_FREEZE_FOR_UPGRADE_2	\
						(FAULTBASE_HA_FRMWK | 83)

/*
 * Fault point before ckpt unfreeze_after_upgrade
 */

#define	FAULTNUM_HA_RM_CKPT_UNFREEZE_AFTER_UPGRADE_1	\
						(FAULTBASE_HA_FRMWK | 84)

/*
 * Fault point after ckpt unfreeze_after_upgrade
 */

#define	FAULTNUM_HA_RM_CKPT_UNFREEZE_AFTER_UPGRADE_2	\
						(FAULTBASE_HA_FRMWK | 85)
#ifdef  __cplusplus
}
#endif

#endif /* _FAULTNUM_HA_FRMWK_H */
