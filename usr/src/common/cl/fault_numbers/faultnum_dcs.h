/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_DCS_H
#define	_FAULTNUM_DCS_H

#pragma ident	"@(#)faultnum_dcs.h	1.13	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the DCS fault point group.
 */

#ifdef FAULT_DCS
#define	FAULTPT_DCS(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_DCS(n, f)
#endif

/*
 * Fault numbers for the DCS fault point group.
 * Example:
 *
 *	#define	FAULTNUM_DCS_SEND	(FAULTBASE_DCS | 1)
 *
 * For DCS, the FAULTBASE_DCS is equal to 655360.
 *
 * The naming convention is: FAULTNUM_DCS_name_{C|S}_{B|A|E|O}
 * Where 'name' is the name of the operation.
 * 'C' means client.
 * 'S' means server.
 * 'B' means before the operation.
 *	For client, it means before calling server.
 *	For server, it means before doing the underlying file operation.
 * 'A' means after the operation.
 * 'E' means at the end of the operation (before returning).
 * 'O' means other.
 * 'LOCKED' means that some lock is held when the fault occurs.
 */

#define	FAULTNUM_DCS_MAPPER_UNREF_S_O		(FAULTBASE_DCS | 0x0001)

/*
 * Write to the CCR
 */

#define	FAULTNUM_DCS_WRITECCR_S_B		(FAULTBASE_DCS | 0x0002)
#define	FAULTNUM_DCS_WRITECCR_S_E		(FAULTBASE_DCS | 0x0003)
#define	FAULTNUM_DCS_WRITECCR_S_A		(FAULTBASE_DCS | 0x0004)

/*
 * Become the primary replica
 */

#define	FAULTNUM_DCS_CONVERT_PRIMARY_S_B	(FAULTBASE_DCS | 0x0005)
#define	FAULTNUM_DCS_CONVERT_PRIMARY_S_A	(FAULTBASE_DCS | 0x0006)
#define	FAULTNUM_DCS_CONVERT_PRIMARY_S_E	(FAULTBASE_DCS | 0x0007)

/*
 * Become a secondary replica
 */

#define	FAULTNUM_DCS_CONVERT_SECONDARY_S_B	(FAULTBASE_DCS | 0x0008)
#define	FAULTNUM_DCS_CONVERT_SECONDARY_S_A	(FAULTBASE_DCS | 0x0009)
#define	FAULTNUM_DCS_CONVERT_SECONDARY_S_E	(FAULTBASE_DCS | 0x000A)

/*
 * Add a secondary replica
 */

#define	FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_B	(FAULTBASE_DCS | 0x000B)
#define	FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_O_LOCKED	(FAULTBASE_DCS | 0x000C)
#define	FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_E	(FAULTBASE_DCS | 0x000D)
#define	FAULTNUM_DCS_NEWLY_JOINING_SECONDARY_S_A	(FAULTBASE_DCS | 0x000E)

/*
 * Register a new node's Device Service Manager
 */

#define	FAULTNUM_DCS_REGISTER_DSM_S_B		(FAULTBASE_DCS | 0x000F)
#define	FAULTNUM_DCS_REGISTER_DSM_S_A		(FAULTBASE_DCS | 0x0010)
#define	FAULTNUM_DCS_REGISTER_DSM_S_E		(FAULTBASE_DCS | 0x0011)

/*
 * Switchover from primary and secondary DCS replicas.
 */

#define	FAULTNUM_DCS_DO_SWITCHBACK_S_B		(FAULTBASE_DCS | 0x0012)
#define	FAULTNUM_DCS_DO_SWITCHBACK_S_A		(FAULTBASE_DCS | 0x0013)
#define	FAULTNUM_DCS_DO_SWITCHBACK_S_E		(FAULTBASE_DCS | 0x0014)

/*
 * Shutdown a particular DCS service, not the DCS itself
 */

#define	FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_B	(FAULTBASE_DCS | 0x0015)
#define	FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_A	(FAULTBASE_DCS | 0x0016)
#define	FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_E	(FAULTBASE_DCS | 0x0017)
#define	FAULTNUM_DCS_SHUTDOWN_DEVICE_SERVICE_S_O	(FAULTBASE_DCS | 0x0018)

/*
 * Switchover an individual device service (e.g. a particular metaset)
 * (e.g These tests are in src/pxfs/server/device_replica_impl.cc)
 */

#define	FAULTNUM_DCS_DEVICE_SERVICE_BECOME_PRIMARY_S_B	(FAULTBASE_DCS | 0x0019)
#define	FAULTNUM_DCS_DEVICE_SERVICE_BECOME_PRIMARY_S_A	(FAULTBASE_DCS | 0x001A)
#define	FAULTNUM_DCS_DEVICE_SERVICE_BECOME_PRIMARY_S_E	(FAULTBASE_DCS | 0x001B)

/*
 * Create a new device service. (e.g. create a new metaset)
 */

#define	FAULTNUM_DCS_CREATE_DEVICE_SERVICE_S_B	(FAULTBASE_DCS | 0x001C)
#define	FAULTNUM_DCS_CREATE_DEVICE_SERVICE_S_A	(FAULTBASE_DCS | 0x001D)
#define	FAULTNUM_DCS_CREATE_DEVICE_SERVICE_S_E	(FAULTBASE_DCS | 0x001E)

/*
 * Remove an individual device service (e.g. remove a metaset)
 */

#define	FAULTNUM_DCS_REMOVE_DEVICE_SERVICE_S_B	(FAULTBASE_DCS | 0x001F)
#define	FAULTNUM_DCS_REMOVE_DEVICE_SERVICE_S_A	(FAULTBASE_DCS | 0x0020)
#define	FAULTNUM_DCS_REMOVE_DEVICE_SERVICE_S_E	(FAULTBASE_DCS | 0x0021)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_DCS_H */
