/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_CCR_H
#define	_FAULTNUM_CCR_H

#pragma ident	"@(#)faultnum_ccr.h	1.15	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the CCR fault point group.
 */

#ifdef FAULT_CCR
#define	FAULTPT_CCR(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_CCR(n, f)
#endif

/*
 * Fault numbers for the CCR fault point group.
 * Example:
 *
 *	#define	FAULTNUM_CCR_SEND	(FAULTBASE_CCR | 1)
 *
 *
 * Symbols for the CCR fault points encode their location in the following
 * manner:
 *
 *	FAULTNUM_CCR_file_function
 */

#define	FAULTNUM_CCR_ADDELEMENT_BEGIN_1			(FAULTBASE_CCR | 1)
#define	FAULTNUM_CCR_REMOVEELEMENT_BEGIN_1		(FAULTBASE_CCR | 2)
#define	FAULTNUM_CCR_UPDATEELEMENT_BEGIN_1		(FAULTBASE_CCR | 3)
#define	FAULTNUM_CCR_REMOVEALLELEMENTS_BEGIN_1		(FAULTBASE_CCR | 4)
#define	FAULTNUM_CCR_COMMITUPDATE_BEGIN_1		(FAULTBASE_CCR | 5)
#define	FAULTNUM_CCR_DO_RECOVERY_1			(FAULTBASE_CCR | 6)
#define	FAULTNUM_CCR_DO_RECOVERY_2			(FAULTBASE_CCR | 7)
#define	FAULTNUM_CCR_DO_RECOVERY_3			(FAULTBASE_CCR | 8)
#define	FAULTNUM_CCR_DO_RECOVERY_4			(FAULTBASE_CCR | 9)
#define	FAULTNUM_CCR_DO_RECOVERY_5			(FAULTBASE_CCR | 10)
#define	FAULTNUM_CCR_RESTORE_TABLE			(FAULTBASE_CCR | 11)
#define	FAULTNUM_CCR_PROPAGATE_DATA_1		(FAULTBASE_CCR | 12)
#define	FAULTNUM_CCR_PROPAGATE_DATA_2		(FAULTBASE_CCR | 13)
#define	FAULTNUM_CCR_PROPAGATE_TABLE_1		(FAULTBASE_CCR | 14)
#define	FAULTNUM_CCR_PROPAGATE_TABLE_2		(FAULTBASE_CCR | 15)
#define	FAULTNUM_CCR_GET_CONSIS_IN_1		(FAULTBASE_CCR | 16)
#define	FAULTNUM_CCR_GET_CONSIS_IN_2		(FAULTBASE_CCR | 17)
#define	FAULTNUM_CCR_WAIT_FOR_REJOIN		(FAULTBASE_CCR | 18)
#define	FAULTNUM_CCR_STARTUP_1		(FAULTBASE_CCR | 19)
#define	FAULTNUM_CCR_STARTUP_2		(FAULTBASE_CCR | 20)
#define	FAULTNUM_CCR_PREPARE_UPDATE_COPIES		(FAULTBASE_CCR | 21)
#define	FAULTNUM_CCR_COMMIT_UPDATE_COPIES		(FAULTBASE_CCR | 22)
#define	FAULTNUM_CCR_COMMIT_TRANSACTION_1		(FAULTBASE_CCR | 23)
#define	FAULTNUM_CCR_COMMIT_TRANSACTION_2		(FAULTBASE_CCR | 25)
#define	FAULTNUM_CCR_SYSTEM_ERROR_NODE			(FAULTBASE_CCR | 26)
#define	FAULTNUM_CCR_SYSTEM_ERROR_CLUSTER		(FAULTBASE_CCR | 27)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_CCR_H */
