/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_UNREF_H
#define	_FAULTNUM_UNREF_H

#pragma ident	"@(#)faultnum_unref.h	1.12	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the UNREF fault point group.
 * Also refer to src/orb/fault_unref.h
 */

#ifdef	FAULT_UNREF
#define	FAULTPT_UNREF(n, stdp, fp) \
	    fault_unref::faultpt_unref((n), (stdp), (fp))
#else
#define	FAULTPT_UNREF(n, stdp, fp)
#endif

/*
 * set by server if there's any failure detected. since unreference
 * triggers are all global triggers, this is one way
 * for server to report failure to client.
 */

#define	FAULTNUM_UNREF_FAIL 			(FAULTBASE_UNREF | 0)

/*
 * Fault numbers for the UNREF fault point subgroup.
 * [UPPER CASE] means state, (lower case) means action
 */

/*
 * regular orb object
 */

/*
 * The following triggers are for different unref tests to cover different
 * unref paths.
 */

/*
 * Generic fault point for test setup of both FAULTNUM_UNREF_REGOBJ_PATH_1 and
 * FAULTNUM_UNREF_REGOBJ_PATH_2, called in
 * standard_handler_server:deliver_unreferenced()
 */

#define	FAULTNUM_UNREF_SETUP_DELIVER_UNREFERENCED	(FAULTBASE_UNREF | 1)


/*
 * Path 1:  [UNREF_SCHEDULED] -> (create ref)+ -> [UNREF_UNSCHEDULE] ->
 *	(release last ref) -> [UNREF_SCHEDULED]
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_1		(FAULTBASE_UNREF | 2)

/*
 * Path 2:  [UNREF_SCHEDULED] -> (create ref)+ -> [UNREF_UNSCHEDULE] ->
 *	(deliver_unreferenced) -> [ACTIVE_HANDLER] ->
 *	(release last ref) -> [UNREF_SCHEDULED]
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_2		(FAULTBASE_UNREF | 3)

/*
 * Generic fault point for test setup of both FAULTNUM_UNREF_REGOBJ_PATH_3 and
 * FAULTNUM_UNREF_REGOBJ_PATH_4, called in
 * standard_handler_server:last_unref()
 */

#define	FAULTNUM_UNREF_SETUP_LAST_UNREF	(FAULTBASE_UNREF | 4)

/*
 * Path 3: [UNREF_PROCESS] -> (create ref)+ -> [UNREF_NO_PROCESS] ->
 *	(release last ref) -> [UNREF_PROCESS] -> (last_unref) ->
 *	[UNREF_DONE]
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_3		(FAULTBASE_UNREF | 5)

/*
 * Path 4: [UNREF_PROCESS] -> (create ref)+ -> [UNREF_NO_PROCESS] ->
 *	(last_unref) -> [ACTIVE_HANDLER] -> (release last ref) ->
 *	[UNREF_SCHEDULED] -> (deliver_unreferenced) -> [UNREF_PROCESS] ->
 *	(last unref) -> [UNREF_DONE]
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_4		(FAULTBASE_UNREF | 6)

/*
 * Generic faultpoint for setup of both FAULTNUM_UNREF_REGOBJ_PATH_5 and
 * FAULTNUM_UNREF_REGOBJ_PATH_6, called in
 * standard_handler_server::begin_xdoor_unref().
 */

#define	FAULTNUM_UNREF_SETUP_BEG_XDOOR_UNREF	(FAULTBASE_UNREF | 7)

/*
 * Generic faultpoint for setup of both FAULTNUM_UNREF_REGOBJ_PATH_5 and
 * FAULTNUM_UNREF_REGOBJ_PATH_6, called in
 * standard_handler_server::deliver_unreferenced().
 */

#define	FAULTNUM_UNREF_SETUP_WAIT_DEL_UNREF	(FAULTBASE_UNREF | 8)

/*
 * Path 5
 * Xdoor->deliver_unreferenced(), handler nref = 0, [ACTIVE_HANDLER]
 * (create 1 ref)+ -> nref != 1, state remains [ACTIVE_HANDLER]
 * (release 1 ref) -> [UNREF_SCHEDULED].
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_5		(FAULTBASE_UNREF | 9)

/*
 * PATH 6
 * Path 5 -> Path 1
 * This tests handler states in deliver_unreferenced().
 * using FAULTNUM_UNREF_REGOBJ_PATH_1
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_6		(FAULTBASE_UNREF | 10)

/*
 * PATH 7
 * Path 5 -> Path 2
 * This tests handler states in deliver_unreferenced().
 * Uses FAULTNUM_UNREF_REGOBJ_PATH_2
 */

#define	FAULTNUM_UNREF_REGOBJ_PATH_7		(FAULTBASE_UNREF | 11)

/*
 * HA object
 *
 * Not available yet
 */

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_UNREF_H */
