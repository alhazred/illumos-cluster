/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FAULTNUM_DPM_H
#define	_FAULTNUM_DPM_H

#pragma ident	"@(#)faultnum_dpm.h	1.9	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * Fault point macro for the DPM fault point group.
 */

#ifdef FAULT_DPM
#define	FAULTPT_DPM(n, f)		FAULT_POINT((n), (f))
#else
#define	FAULTPT_DPM(n, f)
#endif

/*
 * Fault numbers for the DPM fault point group.
 * For DPM, the FAULTBASE_DPM is equal to 1048576.
 */

#define	FAULTNUM_DPM_PATH_BAD		(FAULTBASE_DPM | 0x0001)

#ifdef  __cplusplus
}
#endif

#endif	/* _FAULTNUM_DPM_H */
